/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelPWDetailDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-07-01
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudio.h"
#include "SmartPanelPWDetailDlg.h"
#include "afxdialogex.h"
#include "NXRemoteStudioDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define RECT_BAR	CRect(CPoint(114, 16), CSize(200, 165))

static const int bar_handler_start	= 128;
static const int bar_handler_start_NX1	= 50;
static const int bar_handler_gap_NX1	= 1;
static const int bar_handler_gap	= 20;
static const int bar_handler_color_gap	= 8;

// CSmartPanelPWDetailDlg dialog
IMPLEMENT_DYNAMIC(CSmartPanelPWDetailDlg, CDialogEx)

CSmartPanelPWDetailDlg::CSmartPanelPWDetailDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogST(CSmartPanelPWDetailDlg::IDD, pParent)
{
	m_nMouseIn = BTN_PW_NULL;	
	for(int i=0; i<BTN_PW_CNT - 1; i++)
	{
		m_nStatus[i] = BTN_PW_CNT;
		m_rectBtn[i] = CRect(0,0,0,0);
	}

	m_nPTPCode		= 0;
	m_nValueIndex	= 0;

	m_nColor		= 0;
	m_nSaturation	= 0;
	m_nSharpness	= 0;
	m_nContrast		= 0;
	m_nColor1st		= 0;
	m_nSaturation1st= 0;
	m_nSharpness1st	= 0;
	m_nContrast1st	= 0;

	m_bInit = FALSE;	

	m_nSelected = PW_LINE_FIRST;

	//NX1 dh0.seo 2014-09-03
	m_nRed = 0;
	m_nGreen = 0;
	m_nBlue = 0;
}

CSmartPanelPWDetailDlg::~CSmartPanelPWDetailDlg()
{
}

void CSmartPanelPWDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSmartPanelPWDetailDlg, CBkDialogST)
	ON_WM_PAINT()	
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()


BOOL CSmartPanelPWDetailDlg::OnInitDialog()
{
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	m_strModel = pMainWnd->GetModel();

	CRSChildDialog::OnInitDialog();		

	//SetBitmap(RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_bg.png")),RGB(255,255,255));
	SetItemPosition();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmartPanelPWDetailDlg::SetItemPosition()
{
	if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		m_rectBtn[BTN_PW_RED_PLUS	]	=	CRect(CPoint(320,35), CSize(20,20));
		m_rectBtn[BTN_PW_RED_MINUS	]	=	CRect(CPoint(20,35), CSize(20,20));
		m_rectBtn[BTN_PW_GREEN_PLUS	]	=	CRect(CPoint(320,85), CSize(20,20));
		m_rectBtn[BTN_PW_GREEN_MINUS]	=	CRect(CPoint(20,85), CSize(20,20));
		m_rectBtn[BTN_PW_BLUE_PLUS	]	=	CRect(CPoint(320,135), CSize(20,20));
		m_rectBtn[BTN_PW_BLUE_MINUS	]	=	CRect(CPoint(20,135), CSize(20,20));

		m_rectValue[PW_LINE_RED		] = CRect(CPoint(55,20), CSize(240,20));
		m_rectValue[PW_LINE_GREEN	] = CRect(CPoint(55,70), CSize(240,20));
		m_rectValue[PW_LINE_BLUE	] = CRect(CPoint(55,120), CSize(240,20));

		m_rectBtn[BTN_PW_RESET				]	=	CRect(CPoint(8,177), CSize(55,25));
		m_rectBtn[BTN_PW_OK				]	=	CRect(CPoint(294,177), CSize(55,25));
	}
	else
	{
		//-- Icon
		m_rectIcon[PW_LINE_FIRST	]	=	CRect(CPoint(16,26), CSize(24,24)) ;
		m_rectIcon[PW_LINE_SECOND	]	=	CRect(CPoint(16,62), CSize(24,24)) ;
		m_rectIcon[PW_LINE_THIRD	]	=	CRect(CPoint(16,98), CSize(24,24)) ;
		m_rectIcon[PW_LINE_FOURTH	]	=	CRect(CPoint(16,134), CSize(24,24)) ;

		//-- Value
		m_rectValue[PW_LINE_FIRST	]	=	CRect(CPoint(54,32), CSize(12,12)) ;
		m_rectValue[PW_LINE_SECOND	]	=	CRect(CPoint(40,62), CSize(35,24)) ;
		m_rectValue[PW_LINE_THIRD	]	=	CRect(CPoint(40,98), CSize(35,24)) ;
		m_rectValue[PW_LINE_FOURTH	]	=	CRect(CPoint(40,134), CSize(35,24)) ;

		//-- Bar
		m_rectBar[PW_LINE_FIRST		]	=	CRect(CPoint(114,30), CSize(197,16)) ;
		m_rectBar[PW_LINE_SECOND	]	=	CRect(CPoint(114,66), CSize(197,16)) ;
		m_rectBar[PW_LINE_THIRD		]	=	CRect(CPoint(114,102), CSize(197,16)) ;
		m_rectBar[PW_LINE_FOURTH	]	=	CRect(CPoint(114,138), CSize(197,16)) ;

		//-- Handle
		m_rectHandler[PW_LINE_FIRST	]	=	CRect(CPoint(bar_handler_start - 2,27), CSize(12,28)) ;
		m_rectHandler[PW_LINE_SECOND]	=	CRect(CPoint(bar_handler_start,63), CSize(12,28)) ;
		m_rectHandler[PW_LINE_THIRD	]	=	CRect(CPoint(bar_handler_start,99), CSize(12,28)) ;
		m_rectHandler[PW_LINE_FOURTH]	=	CRect(CPoint(bar_handler_start,135), CSize(12,28)) ;

		//-- Button
		m_rectBtn[BTN_PW_COLOR_LEFT		]	=	CRect(CPoint(80,26), CSize(25,24));
		m_rectBtn[BTN_PW_COLOR_RIGHT		]	=	CRect(CPoint(320,26), CSize(25,24));
		m_rectBtn[BTN_PW_SATURATION_LEFT	]	=	CRect(CPoint(80,62), CSize(25,24));
		m_rectBtn[BTN_PW_SATURATION_RIGHT	]	=	CRect(CPoint(320,62), CSize(25,24));
		m_rectBtn[BTN_PW_SHARPNESS_LEFT	]	=	CRect(CPoint(80,98), CSize(25,24));
		m_rectBtn[BTN_PW_SHARPNESS_RIGHT	]	=	CRect(CPoint(320,98), CSize(25,24));
		m_rectBtn[BTN_PW_CONTRAST_LEFT		]	=	CRect(CPoint(80,134), CSize(25,24));
		m_rectBtn[BTN_PW_CONTRAST_RIGHT	]	=	CRect(CPoint(320,134), CSize(25,24));
		m_rectBtn[BTN_PW_RESET				]	=	CRect(CPoint(8,177), CSize(55,25));
		m_rectBtn[BTN_PW_OK				]	=	CRect(CPoint(294,177), CSize(55,25));
	}
}

void CSmartPanelPWDetailDlg::OnPaint()
{	
	DrawBackground();	
	CRSChildDialog::OnPaint();
}

void CSmartPanelPWDetailDlg::CleanDC(CPaintDC* pDC)
{
	for(int i = 0; i < BTN_PW_CNT - 1; i++)
	{
		if(i < BTN_PW_CNT - 2)
		{
			CBrush brush = RGB_BLACK;
			pDC->FillRect(m_rectBtn[i], &brush);
		}
		else
		{
			CBrush brush = RGB_DEFAULT_DLG;
			pDC->FillRect(m_rectBtn[i], &brush);
		}
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::DrawBackground()
{
	CPaintDC dc(this);
	
	//CleanDC(&dc);

	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Round Dialog
	DrawRoundDialogs(&mDC, TRUE);
	DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_bg.png")),0, 0);
	
	if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		DrawLine(PW_LINE_RED, &mDC);
		DrawLine(PW_LINE_GREEN, &mDC);
		DrawLine(PW_LINE_BLUE, &mDC);

		DrawMenu(BTN_PW_RED_PLUS, &mDC);
		DrawMenu(BTN_PW_RED_MINUS, &mDC);
		DrawMenu(BTN_PW_GREEN_PLUS, &mDC);
		DrawMenu(BTN_PW_GREEN_MINUS, &mDC);
		DrawMenu(BTN_PW_BLUE_PLUS, &mDC);
		DrawMenu(BTN_PW_BLUE_MINUS, &mDC);

		DrawMenu(BTN_PW_RESET, &mDC);
		DrawMenu(BTN_PW_OK, &mDC);
	}
	else
	{
		//-- Draw Line
		DrawLine(PW_LINE_FIRST, &mDC);
		DrawLine(PW_LINE_SECOND, &mDC);
		DrawLine(PW_LINE_THIRD, &mDC);
		DrawLine(PW_LINE_FOURTH, &mDC);

		//-- Draw Menu
		DrawMenu(BTN_PW_COLOR_LEFT, &mDC);
		DrawMenu(BTN_PW_COLOR_RIGHT, &mDC);
		DrawMenu(BTN_PW_SATURATION_LEFT, &mDC);
		DrawMenu(BTN_PW_SATURATION_RIGHT, &mDC);
		DrawMenu(BTN_PW_SHARPNESS_LEFT, &mDC);
		DrawMenu(BTN_PW_SHARPNESS_RIGHT, &mDC);
		DrawMenu(BTN_PW_CONTRAST_LEFT, &mDC);
		DrawMenu(BTN_PW_CONTRAST_RIGHT, &mDC);
		DrawMenu(BTN_PW_RESET, &mDC);
		DrawMenu(BTN_PW_OK, &mDC);
	}
	

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawLine / DrawIcon / DrawValue / DrawBar / DrawBarHandler
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Main Camera / Name / Mode / Check Box
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::DrawLine(int nLine, CDC* pDC)
{
	DrawTitle(nLine, pDC);
	DrawIcon(nLine, pDC);
	DrawValue(nLine, pDC);
	DrawBar(nLine, pDC);
	DrawBarHandler(nLine, pDC);
}

void CSmartPanelPWDetailDlg::DrawTitle(int nLine, CDC* pDC)
{
	switch(nLine)
	{
	case PW_LINE_FIRST	:	DrawStr(pDC, STR_SIZE_TEXT_SMALL, _T("Color"), 120, 16, RGB(255,255,255));			break;
	case PW_LINE_SECOND	:	DrawStr(pDC, STR_SIZE_TEXT_SMALL, _T("Saturation"), 120,52, RGB(255,255,255));		break;
	case PW_LINE_THIRD	:	DrawStr(pDC, STR_SIZE_TEXT_SMALL, _T("Sharpness"), 120,88, RGB(255,255,255));		break;
	case PW_LINE_FOURTH	:	DrawStr(pDC, STR_SIZE_TEXT_SMALL, _T("Contrast"), 120,124, RGB(255,255,255));		break;
	case PW_LINE_RED	:	DrawStr(pDC, STR_SIZE_TEXT_SMALL, _T("Red"), 60, 25, RGB(255,255,255));			break;
	case PW_LINE_GREEN	:	DrawStr(pDC, STR_SIZE_TEXT_SMALL, _T("Green"), 60, 75, RGB(255,255,255));			break;
	case PW_LINE_BLUE	:	DrawStr(pDC, STR_SIZE_TEXT_SMALL, _T("Blue"), 60, 125, RGB(255,255,255));			break;
	}
}

void CSmartPanelPWDetailDlg::DrawIcon(int nLine, CDC* pDC)
{
	int nX, nY, nW, nH;
	nX		= m_rectIcon[nLine].left;	
	nY		= m_rectIcon[nLine].top;
	nW		= m_rectIcon[nLine].Width();
	nH		= m_rectIcon[nLine].Height();

	switch(nLine)
	{
	case PW_LINE_FIRST	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_icon_color.png")), nX, nY, nW, nH);			break;
	case PW_LINE_SECOND	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_icon_saturation.png")), nX, nY, nW, nH);	break;
	case PW_LINE_THIRD	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_icon_sharpness.png")), nX, nY, nW, nH);		break;
	case PW_LINE_FOURTH	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_icon_contrast.png")), nX, nY, nW, nH);		break;
	}
}

void CSmartPanelPWDetailDlg::DrawValue(int nLine, CDC* pDC)
{
	int nX, nY, nW, nH;
	nX		= m_rectValue[nLine].left;	
	nY		= m_rectValue[nLine].top;
	nW		= m_rectValue[nLine].Width();
	nH		= m_rectValue[nLine].Height();

	CString strValue = GetValues(nLine);
	
	switch(nLine)
	{
	case PW_LINE_FIRST	:	
		{
			DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_icon_box.png")), nX, nY, nW, nH);
			
			CRect rect = m_rectValue[PW_LINE_FIRST];

			CBrush brush = GetColorValues();
			pDC->FillRect(rect, &brush);
		}
		break;		
	case PW_LINE_SECOND	:	
	case PW_LINE_THIRD	:	
	case PW_LINE_FOURTH	:	DrawStr(pDC, STR_SIZE_TEXT_BOLD, strValue,m_rectValue[nLine],RGB(255,255,255));	break;
	}					
}

void CSmartPanelPWDetailDlg::DrawBar(int nLine, CDC* pDC)
{
	int nX, nY, nW, nH;
	nX		= m_rectBar[nLine].left;	
	nY		= m_rectBar[nLine].top;
	nW		= m_rectBar[nLine].Width();
	nH		= m_rectBar[nLine].Height();

	switch(nLine)
	{
	case PW_LINE_FIRST	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_bar_color.png")), nX, nY, nW, nH);			break;
	case PW_LINE_SECOND	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_bar_saturation.png")), nX, nY, nW, nH);		break;
	case PW_LINE_THIRD	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_bar_sharpness.png")), nX, nY, nW, nH);		break;
	case PW_LINE_FOURTH	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_bar_contrast.png")), nX, nY, nW, nH);		break;
	case PW_LINE_RED	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_bar_bg.png")), 60, 40, 240, 10);		break;
	case PW_LINE_GREEN	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_bar_bg.png")), 60, 90, 240, 10);		break;
	case PW_LINE_BLUE	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_bar_bg.png")), 60, 140, 240, 10);		break;
	}
	if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		if(m_nSelected == nLine)
			DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\03_quickpanel_progress_handler.png")), nX, nY, nW, nH);
	}
	else
	{
		if(m_nSelected == nLine)
			DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_pw_adjust_bar_focus.png")), nX, nY, nW, nH);
	}
}

void CSmartPanelPWDetailDlg::DrawBarHandler(int nLine, CDC* pDC)
{
	int nX, nY, nW, nH;
	nX		= m_rectHandler[nLine].left;	
	nY		= m_rectHandler[nLine].top;
	nW		= m_rectHandler[nLine].Width();
	nH		= m_rectHandler[nLine].Height();

	switch(nLine)
	{
	case PW_LINE_FIRST	:	
	case PW_LINE_SECOND	:	
	case PW_LINE_THIRD	:	
	case PW_LINE_FOURTH	:	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\03.Live View\\03_focus_progress_handle_nor.png")), nX, nY, nW, nH);		break;
	case PW_LINE_RED	:
	case PW_LINE_GREEN	:
	case PW_LINE_BLUE	:	Invalidate(FALSE);	DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\03_quickpanel_progress_handler.png")), nX, nY, nW, nH);		break;
	}
	if(m_strModel.Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		if(m_nSelected == nLine)
			DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\03_quickpanel_progress_handler.png")), nX, nY, nW, nH);
	}
	else
	{
		if(m_nSelected == nLine)
			DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\03.Live View\\03_focus_progress_handle_over.png")), nX, nY, nW, nH);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawMenu
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Draw Buttons
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::DrawMenu(int nItem, CDC* pDC)
{
	int nX, nY, nW, nH;
	nX	= m_rectBtn[nItem].left;	
	nY	= m_rectBtn[nItem].top;
	nW	= m_rectBtn[nItem].Width();
	nH	= m_rectBtn[nItem].Height();

	COLORREF color;

	switch(nItem)
	{
	case BTN_PW_COLOR_LEFT:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_focus.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_push.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_disable.png")),	nX, nY, nW, nH);	break;
		default	:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_normal.png")),		nX, nY, nW, nH);	break;
		}
		break;
	case BTN_PW_COLOR_RIGHT:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_focus.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_push.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_disable.png")),	nX, nY, nW, nH);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_normal.png")),	nX, nY, nW, nH);	break;
		}
		break;
	case BTN_PW_SATURATION_LEFT:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_focus.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_push.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_disable.png")),	nX, nY, nW, nH);	break;
		default	:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_normal.png")),		nX, nY, nW, nH);	break;
		}
		break;
	case BTN_PW_SATURATION_RIGHT:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_focus.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_push.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_disable.png")),	nX, nY, nW, nH);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_normal.png")),	nX, nY, nW, nH);	break;
		}
		break;
	case BTN_PW_SHARPNESS_LEFT:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_focus.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_push.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_disable.png")),	nX, nY, nW, nH);	break;
		default	:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_normal.png")),		nX, nY, nW, nH);	break;
		}
		break;
	case BTN_PW_SHARPNESS_RIGHT:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_focus.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_push.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_disable.png")),	nX, nY, nW, nH);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_normal.png")),	nX, nY, nW, nH);	break;
		}
		break;
	case BTN_PW_CONTRAST_LEFT:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_focus.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_push.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_disable.png")),	nX, nY, nW, nH);	break;
		default	:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_normal.png")),		nX, nY, nW, nH);	break;
		}
		break;
	case BTN_PW_CONTRAST_RIGHT:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_focus.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_push.png")),		nX, nY, nW, nH);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_disable.png")),	nX, nY, nW, nH);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_normal.png")),	nX, nY, nW, nH);	break;
		}
		break;
	case BTN_PW_RESET:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png")),	nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_push.png")),	nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_disable.png")),	nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png")),	nX, nY, nW, nH);	color = RGB(219,219,219);	break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Reset"),m_rectBtn[nItem],color);	
		break;
	case BTN_PW_OK:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_focus.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_push.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_disable.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn01_normal.png")),		nX, nY, nW, nH);	color = RGB(219,219,219);	break;
		}
		DrawStr(pDC, STR_SIZE_MIDDLE, _T("OK "),m_rectBtn[nItem],color);
		break;
	case BTN_PW_RED_PLUS:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_plus_sel.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_plus_nor.png")),		nX, nY, nW, nH);	color = RGB(219,219,219);	break;
		}
		break;
	case BTN_PW_RED_MINUS:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_minus_sel.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_minus_nor.png")),		nX, nY, nW, nH);	color = RGB(219,219,219);	break;
		}
		break;
	case BTN_PW_GREEN_PLUS:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_plus_sel.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_plus_nor.png")),		nX, nY, nW, nH);	color = RGB(219,219,219);	break;
		}
		break;
	case BTN_PW_GREEN_MINUS:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_minus_sel.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_minus_nor.png")),		nX, nY, nW, nH);	color = RGB(219,219,219);	break;
		}
		break;
	case BTN_PW_BLUE_PLUS:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_plus_sel.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_plus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_plus_nor.png")),		nX, nY, nW, nH);	color = RGB(219,219,219);	break;
		}
		break;
	case BTN_PW_BLUE_MINUS:
		switch(m_nStatus[nItem])
		{
		case PT_PW_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn2_minus_sel.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_PW_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\08_hotkey_pw_btn_minus_dim.png")),		nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\03_pz_bar_btn_minus_nor.png")),		nX, nY, nW, nH);	color = RGB(219,219,219);	break;
		}
		break;
	}
}

CString CSmartPanelPWDetailDlg::GetValues(int nLIne)
{
	CString strValue = _T("");

	switch(nLIne)
	{
	case PW_LINE_FIRST	:	break; 
	case PW_LINE_SECOND	:	strValue.Format(_T("%d"), m_nSaturation);	break;
	case PW_LINE_THIRD	:	strValue.Format(_T("%d"), m_nSharpness);	break;
	case PW_LINE_FOURTH	:	strValue.Format(_T("%d"), m_nContrast);		break;
	case PW_LINE_RED	:	strValue.Format(_T("%d"), m_nRed);			break;
	case PW_LINE_GREEN	:	strValue.Format(_T("%d"), m_nGreen);		break;
	case PW_LINE_BLUE	:	strValue.Format(_T("%d"), m_nBlue);			break;
	}

	return strValue;
}

 COLORREF CSmartPanelPWDetailDlg::GetColorValues()
 {
 	COLORREF rgb = RGB(255,255,255);
 
	switch(m_nColor)
	{
	case PW_COLOR_FF0B04	:	rgb = RGB(255,11,4	);		break;
	case PW_COLOR_FF6B0F	:	rgb = RGB(255,107,15);		break;
	case PW_COLOR_FDB61E	:	rgb = RGB(253,182,30);		break;
	case PW_COLOR_FCFD2D	:	rgb = RGB(252,253,45);		break;
	case PW_COLOR_CDFD2C	:	rgb = RGB(205,253,44);		break;
	case PW_COLOR_89FD2C	:	rgb = RGB(137,253,44);		break;
	case PW_COLOR_00FD2D	:	rgb = RGB(0,253,45	);		break;
	case PW_COLOR_00FE1F	:	rgb = RGB(0,253,51	);		break;
	case PW_COLOR_00FD92	:	rgb = RGB(0,253,79	);		break;
	case PW_COLOR_00FE9C	:	rgb = RGB(0,253,146	);		break;
	case PW_COLOR_00FDD9	:	rgb = RGB(0,253,217	);		break;
	case PW_COLOR_0DD2FD	:	rgb = RGB(13,210,253);		break;
	case PW_COLOR_1973FB	:	rgb = RGB(25,115,250);		break;
	case PW_COLOR_1C24FB	:	rgb = RGB(28,36,251	);		break;
	case PW_COLOR_2B1BFB	:	rgb = RGB(43,24,251	);		break;
	case PW_COLOR_7018FB	:	rgb = RGB(112,24,251);		break;
	case PW_COLOR_C51AFB	:	rgb = RGB(197,26,251);		break;
	case PW_COLOR_F01BFO	:	rgb = RGB(240,27,251);		break;
	case PW_COLOR_FF16D7	:	rgb = RGB(255,22,215);		break;
	case PW_COLOR_FF0A67	:	rgb = RGB(255,10,103);		break;
	case PW_COLOR_FF0834	:	rgb = RGB(255,8,52	);		break;
	}

	return rgb;
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CSmartPanelPWDetailDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CSmartPanelPWDetailDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_PW_CNT; i ++)
		ChangeStatus(i,PT_PW_NOR);
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	

	int x = 0 , y = 0;

	switch(nItem)
	{
	case BTN_PW_COLOR_LEFT			:		SetColorValue(FALSE);		break;	
	case BTN_PW_COLOR_RIGHT			:		SetColorValue(TRUE);		break;	
	case BTN_PW_SATURATION_LEFT		:		SetSaturationValue(FALSE);	break;	
	case BTN_PW_SATURATION_RIGHT	:		SetSaturationValue(TRUE);	break;	
	case BTN_PW_SHARPNESS_LEFT		:		SetSharpnessValue(FALSE);	break;	
	case BTN_PW_SHARPNESS_RIGHT		:		SetSharpnessValue(TRUE);	break;	
	case BTN_PW_CONTRAST_LEFT		:		SetContrastValue(FALSE);	break;	
	case BTN_PW_CONTRAST_RIGHT		:		SetContrastValue(TRUE);		break;
	case BTN_PW_RED_PLUS			:		SetRedValue(TRUE);			break;
	case BTN_PW_RED_MINUS			:		SetRedValue(FALSE);			break;
	case BTN_PW_GREEN_PLUS			:		SetGreenValue(TRUE);			break;
	case BTN_PW_GREEN_MINUS			:		SetGreenValue(FALSE);			break;
	case BTN_PW_BLUE_PLUS			:		SetBlueValue(TRUE);			break;
	case BTN_PW_BLUE_MINUS			:		SetBlueValue(FALSE);			break;
	case BTN_PW_RESET				:		ResetValue();	break;
	case BTN_PW_OK					:		((CSmartPanelChildDlg*)GetParent())->CloseSmartPanelChild();				return;
	default:								if(SetPosition(point))		SetDetailValue();		return;	//-- Check Enable Rect
	}

	//-- 2011-8-2 Lee JungTaek
	SetDetailValue();	

	CDialog::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CSmartPanelPWDetailDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_PW_IN_NOTHING;
	int nStart = 0;

	for(int i = nStart ; i < BTN_PW_CNT; i ++)
	{
		if(m_rectBtn[i].PtInRect(pt))
		{
			nReturn = i;
			if(bMove)
				ChangeStatus(i,PT_PW_INSIDE);
			else
				ChangeStatus(i,PT_PW_DOWN); 
		}
		else
			ChangeStatus(i,PT_PW_NOR); 
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		PtInBarRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CSmartPanelPWDetailDlg::PtInBarRect(CPoint pt)
{
	int nReturn = PW_LINE_NULL;
	int nStart = 0;

	for(int i = nStart ; i < PW_LINE_CNT; i ++)
	{
		if(m_rectBar[i].PtInRect(pt))
			nReturn = i;
	}

	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		CPaintDC dc(this);
		DrawMenu(nItem, &dc);
		InvalidateRect(m_rectBtn[nItem],FALSE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_PW_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_PW_INSIDE); 
		else
			ChangeStatus(i,PT_PW_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		SetColorValue
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Set Values (Min:0 / Max:21)
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::SetColorValue(BOOL bRight)
{
	if(bRight)
	{
		m_nColor++;
		if(m_nColor > 19)			m_nColor = 20;
	}
	else
	{
		m_nColor--;
		if(m_nColor < 0)			m_nColor = 0;
	}
	
	m_nSelected = PW_LINE_FIRST;
}

//------------------------------------------------------------------------------ 
//! @brief		SetSaturationValue
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Set Values (Min:-4 / Max:4)
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::SetSaturationValue(BOOL bRight)	
{
	if(bRight)
	{
		if(m_nSaturation > 3)			m_nSaturation = 4;
		else							m_nSaturation++;			
	}
	else
	{
		if(m_nSaturation < -3)			m_nSaturation = -4;
		else							m_nSaturation--;
	}

	m_nSelected = PW_LINE_SECOND;
}

//------------------------------------------------------------------------------ 
//! @brief		SetSharpnessValue
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Set Values (Min:-4 / Max:4)
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::SetSharpnessValue(BOOL bRight)	
{
	if(bRight)
	{
		if(m_nSharpness > 3)			m_nSharpness = 4;
		else							m_nSharpness++;			
	}
	else
	{
		if(m_nSharpness < -3)			m_nSharpness = -4;
		else							m_nSharpness--;
	}

	m_nSelected = PW_LINE_THIRD;
}

//------------------------------------------------------------------------------ 
//! @brief		SetContrastValue
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Set Values (Min:-4 / Max:4)
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::SetContrastValue(BOOL bRight)		
{
	if(bRight)
	{
		if(m_nContrast > 3)			m_nContrast = 4;
		else						m_nContrast++;			
	}
	else
	{
		if(m_nContrast < -3)			m_nContrast = -4;
		else						m_nContrast--;
	}

	m_nSelected = PW_LINE_FOURTH;
}

void CSmartPanelPWDetailDlg::SetRedValue(BOOL bPlus)
{
	if(bPlus)
	{
		if(m_nRed > 255)
			m_nRed = 255;
		else
			m_nRed++;
	}
	else
	{
		if(m_nRed < 0)
			m_nRed = 0;
		else
			m_nRed--;
	}
	m_nSelected = PW_LINE_RED;
}

void CSmartPanelPWDetailDlg::SetGreenValue(BOOL bPlus)
{
	if(bPlus)
	{
		if(m_nGreen > 255)
			m_nGreen = 255;
		else
			m_nGreen++;
	}
	else
	{
		if(m_nGreen < 0)
			m_nGreen = 0;
		else
			m_nGreen--;
	}
	m_nSelected = PW_LINE_GREEN;
}

void CSmartPanelPWDetailDlg::SetBlueValue(BOOL bPlus)
{
	if(bPlus)
	{
		if(m_nBlue > 255)
			m_nBlue = 255;
		else
			m_nBlue++;
	}
	else
	{
		if(m_nBlue < 0)
			m_nBlue = 0;
		else
			m_nBlue--;
	}
	m_nSelected = PW_LINE_BLUE;
}
//------------------------------------------------------------------------------ 
//! @brief		SetValueIndex
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Set First Values
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::SetValueIndex(int nValue)
{
	int nLeft = HIWORD(nValue) ;
	int nRight = LOWORD(nValue) ;
	
	//-- Get 1Byte
	char nColor = HIBYTE(nLeft);
	char nSaturation = LOBYTE(nLeft);
	char nSharpness = HIBYTE(nRight);
	char nContrast = LOBYTE(nRight);
	char nRed = 0;
	char nGreen = 0;
	char nBlue = 0;

	//-- Set Value
	m_nColor = nColor;		
	m_nSaturation = nSaturation;	
	m_nSharpness = nSharpness;	
	m_nContrast = nContrast;
	m_nRed = nRed;
	m_nGreen = nGreen;
	m_nBlue = nBlue;

	if(!m_bInit)
	{
		m_nColor1st	= nColor;
		m_nSaturation1st = nSaturation;
		m_nSharpness1st = nSharpness;
		m_nContrast1st = nContrast;

		m_bInit = TRUE;
	}

	//-- Set Handle Position And Redraw
	for(int i = 0; i < PW_LINE_CNT; i++)
	{
		InvalidateRect(m_rectValue[i], FALSE);	
		SetHandler(i);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		SetHandler
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	Set Bar Handler Position
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::SetHandler(int nLine)
{
	int nX = 0, nY = 0;

	switch(nLine)
	{
	case PW_LINE_FIRST	:	
		{
			nX = bar_handler_start + m_nColor * bar_handler_color_gap;	nY = 26;
			m_rectHandler[nLine] =  CRect(CPoint(nX,nY), CSize(12,28));
		}
		break;
	case PW_LINE_SECOND	:	
		{
			nX = bar_handler_start + (m_nSaturation + 4) * bar_handler_gap;	nY = 62;
			m_rectHandler[nLine] =  CRect(CPoint(nX,nY), CSize(12,28));
		}
		break;
	case PW_LINE_THIRD	:	
		{
			nX = bar_handler_start + (m_nSharpness + 4) * bar_handler_gap;	nY = 98;
			m_rectHandler[nLine] =  CRect(CPoint(nX,nY), CSize(12,28));
		}
		break;
	case PW_LINE_FOURTH :	
		{
			nX = bar_handler_start + (m_nContrast + 4) * bar_handler_gap;	nY = 134;
			m_rectHandler[nLine] =  CRect(CPoint(nX,nY), CSize(12,28));
		}
		break;
	case PW_LINE_RED :	
		{
			nX = bar_handler_start_NX1 + m_nRed;	nY = 35;
			m_rectHandler[nLine] =  CRect(CPoint(nX,nY), CSize(20,20));
		}
		break;
	case PW_LINE_GREEN :	
		{
			nX = bar_handler_start_NX1 + m_nGreen;	nY = 85;
			m_rectHandler[nLine] =  CRect(CPoint(nX,nY), CSize(20,20));
		}
		break;
	case PW_LINE_BLUE :	
		{
			nX = bar_handler_start_NX1 + m_nBlue;	nY = 135;
			m_rectHandler[nLine] =  CRect(CPoint(nX,nY), CSize(20,20));
		}
		break;
	}

	if(this->GetSafeHwnd())
	{
		Invalidate(FALSE);
		InvalidateRect(m_rectHandler, FALSE);
		//-- Update Image
		InvalidateRect(RECT_BAR, FALSE);
	}		
}	

//------------------------------------------------------------------------------ 
//! @brief		SetPosition
//! @date		2011-7-29
//! @author		Lee JungTaek
//! @note	 	Set Bar Event
//------------------------------------------------------------------------------ 
BOOL CSmartPanelPWDetailDlg::SetPosition(CPoint point)
{
	int nItem = PtInBarRect(point);

	//-- Check Boundary
	switch(nItem)
	{
	case PW_LINE_FIRST	:	
		{
			m_nColor = (point.x - bar_handler_start) / bar_handler_color_gap; 

			//-- Check Index
			if(m_nColor > 19)			m_nColor = 20;
			else if(m_nColor < 0)		m_nColor = 0;
		}
		break;
	case PW_LINE_SECOND	:	
		{
			m_nSaturation = (point.x - bar_handler_start) / bar_handler_gap - 4;

			//-- Check Index
			if(m_nSaturation > 3)			m_nSaturation = 4;
			else if(m_nSaturation < -3)		m_nSaturation = -4;
		}
		break;
	case PW_LINE_THIRD	:	
		{
			m_nSharpness = (point.x - bar_handler_start) / bar_handler_gap - 4;

			//-- Check Index
			if(m_nSharpness > 3)			m_nSharpness = 4;
			else if(m_nSharpness < -3)		m_nSharpness = -4;
		}
		break;
	case PW_LINE_FOURTH	:	
		{
			m_nContrast = (point.x - bar_handler_start) / bar_handler_gap - 4;

			//-- Check Index
			if(m_nContrast > 3)				m_nContrast = 4;
			else if(m_nContrast < -3)		m_nContrast = -4;
		}
		break;
	default:				return FALSE;
	}

	m_nSelected = nItem;
	//InvalidateRect(m_rectValue[nItem], FALSE);
	
	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief		ResetValue
//! @date		2011-7-29e
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::ResetValue()
{
	//-- 2011-8-1 Lee JungTaek
	m_nColor = 0;		
	m_nSaturation = 0;	
	m_nSharpness = 0;	
	m_nContrast = 0;	

	//-- 2011-8-2 Lee JungTaek
	m_nSelected = PW_LINE_FIRST;
	SetDetailValue();
}

//------------------------------------------------------------------------------ 
//! @brief		SetDetailValue
//! @date		2011-7-29
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::SetDetailValue()
{
  	UINT32 nValue = 0;
		
	nValue = (UINT8)m_nColor;
	nValue = nValue << 8;

	nValue = nValue + (UINT8)m_nSaturation;
	nValue = nValue << 8;

	nValue = nValue + (UINT8)m_nSharpness;
	nValue = nValue << 8;

	nValue = nValue + (UINT8)m_nContrast;

	//-- Set Detail Value
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	pMainWnd->SetPropertyValue(pMainWnd->GetSelectedItem(), m_nPTPCode, (int)nValue);
}

//------------------------------------------------------------------------------ 
//! @brief		GetDetailPropCode
//! @date		2011-7-28
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CSmartPanelPWDetailDlg::GetDetailPropCode(int nValueIndex)
{
	int nPropCode = 0;

	switch(nValueIndex)
	{
	case DSC_PW_DETAIL_STANDARD		:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_STANDARD		;	break;
	case DSC_PW_DETAIL_VIVID		:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_VIVID		;	break;
	case DSC_PW_DETAIL_PORTRAIT		:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_PORTRAIT		;	break;
	case DSC_PW_DETAIL_LANDSCAPE	:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_LANDSCAPE	;	break;
	case DSC_PW_DETAIL_FOREST		:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_FOREST		;	break;
	case DSC_PW_DETAIL_RETRO		:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_RETRO		;	break;
	case DSC_PW_DETAIL_COOL			:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_COOL			;	break;
	case DSC_PW_DETAIL_CALM			:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_CALM			;	break;
	case DSC_PW_DETAIL_CLASSIC		:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_CLASSIC		;	break;
	case DSC_PW_DETAIL_CUSTOM1		:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM1		;	break;
	case DSC_PW_DETAIL_CUSTOM2		:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM2		;	break;
	case DSC_PW_DETAIL_CUSTOM3		:	nPropCode = PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM3		;	break;
	}

	m_nPTPCode = nPropCode;
	m_nValueIndex = nValueIndex;

	return nPropCode;
}

//------------------------------------------------------------------------------ 
//! @brief		SetFirstValue
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelPWDetailDlg::SetFirstValue()
{
	CSmartPanelChildDlg *pSmartPanelChild = (CSmartPanelChildDlg*)GetParent();

	//-- Set First Values
	m_nColor		= m_nColor1st		;
	m_nSaturation	= m_nSaturation1st	;
	m_nSharpness	= m_nSharpness1st	;
	m_nContrast		= m_nContrast1st	;
		
	//-- Set Detail Value
	SetDetailValue();
	//-- Set 2 Depth Value
	int nValueIndex = pSmartPanelChild->GetSelected1stValue();
	pSmartPanelChild->SetSelectedPropValue(nValueIndex);
	//-- Close Dialog
	pSmartPanelChild->CloseSmartPanelChild();
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
BOOL CSmartPanelPWDetailDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_ESCAPE)
		{
			SetFirstValue();
			return TRUE;
		}
	}
	else if(pMsg->message == WM_MOUSEWHEEL)		return TRUE;

	return CBkDialogST::PreTranslateMessage(pMsg);
}