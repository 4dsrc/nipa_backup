/////////////////////////////////////////////////////////////////////////////
//
//  LiveviewDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Liveview.h"
#include "afxcmn.h"
#include "ThumbnailDlg.h"
#include "MultiviewDlg.h"
#include "LiveviewToolbarDlg.h"
#include "LiveviewFrmDlg.h"
#include "ZoomDlg.h"
#include "BkDialogST.h"
#include "NXRemoteStudioFunc.h"
#include "NXRemoteStudioDlg.h"

// CLiveviewDlg dialog
class CLiveviewDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CLiveviewDlg)	
public:
	CLiveviewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLiveviewDlg();

	enum
	{
		BTN_NOTHING = -1,
		BTN_SIDE_HANDLE,
	};

// Dialog Data
	enum { IDD = IDD_DLG_LIVEVIEW};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()
	LRESULT OnLiveviewMsg(WPARAM w, LPARAM l);
	//CMiLRe 20141118 Enter, ESC 키 누를 시 화면 사라지거나 종료되는 문제
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnBnClickedBtnMaximize();
	afx_msg void OnBnClickedBtnCancel();	
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnWindowPosChanged(WINDOWPOS* lpwndpos);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

public:
	CRect m_rectToolbar;
	
	CLiveview* GetLiveview()	{ return m_pDlgLiveviewFrm->GetLiveview(); }
	CLiveviewFrmDlg*			m_pDlgLiveviewFrm;	
	CLiveviewToolbarDlg*		m_pDlgToolbar;
	CThumbnailDlg*				m_pDlgThumbnail;
	CMultiviewDlg*				m_pDlgMultiview;		
	CZoomDlg*					m_pDlgZoom;
	int m_nRightSize;
	int m_nBottomSize;

	//-- 2011-06-22 hongsu.jung
	int GetGridOpt()					{ return m_nGridLine; }	
	int GetZoomOpt()					{ return m_nZoomOpt; }	

	//-- 2011-5-21 Lee JungTaek
	void ReloadThumbnailView(CString strFileName);
	//-- 2011-05-31 hongsu.jung
	BOOL SetMultiViewer(int );
	//-- 2011-06-08 hongsu.jung
	BOOL GetHideSeparate(int n)			{ return m_bHide[n];}
	void SetHideSeparate(int n, BOOL b) { m_bHide[n] = b; }
	int GetWindowShowStatus();
	void ChangeGridOpt(int );
	void ChangeZoomOpt(int );	

	void SetLiveview(BOOL );
	BOOL CLiveviewDlg::IsLiveviewOn()	{	return m_pDlgLiveviewFrm->GetLiveview()->IsLiveviewOn(); }
	void SetShow(int n)					{ m_nShow = n; }
	int GetShow()						{ return m_nShow; }

	void GetMonitorFullSize();
private:

	//-- 2011-06-22 hongsu.jung	
	int m_nGridLine;	
	int m_nZoomOpt;
	//-- 2011-07-05 hongsu.jung
	int m_nShow;
	CRect m_rectSideHandle;

//	CNXBitmapButton m_btnMax	;
	CBitmapButton m_btnMax		;
	CBitmapButton m_btnCancel	;
	BOOL m_bHide[2];		//- [1:TRUE] vertical, [0:FALSE] Horizon

	CRect m_CurrentRect;	// 현재 윈도우 Rect;

	//-- 2011-05-18 hongsu.jung
	void UpdateFrames();
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();

	void DrawBackGround(CDC* pDC);
	void DrawDialogs(CDC* pDC);	
	int PtInRect(CPoint pt);

	int m_nX;
	int m_nY;
	int m_nMaxX;
	int m_nMaxY;
	int m_nPointX;
	int m_nPointY;
	int m_nStatus;

public:	
	//CMiLRe 20141118 카메라 선택 시 프로퍼티 로드 중 창 컨트롤 막음
	BOOL m_bEnable;
	void EnableControl(BOOL bEnable);
	LRESULT OnEndResizeEvent(WPARAM w, LPARAM l);
	//dh0.seo
	CRect m_RectMax;
	CRect m_RectClose;
	BOOL m_bMaxState;
	BOOL m_bCloseState;
};