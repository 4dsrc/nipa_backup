/////////////////////////////////////////////////////////////////////////////
//
//  SeparateDlg.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "RSChildDialog.h"

// CSeparateDlg dialog
class CSeparateDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(CSeparateDlg)

public:
	CSeparateDlg(BOOL bVertical, CWnd* pParent = NULL);   // standard constructor
	virtual ~CSeparateDlg();

// Dialog Data
	enum { IDD = IDD_DLG_BOARD};
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	DECLARE_MESSAGE_MAP()

private:
	BOOL m_bVertical;	// 0: horizon 1:vertical	
	void DrawBackground();
public:	
	BOOL IsVertical()	{ return m_bVertical; }
};
