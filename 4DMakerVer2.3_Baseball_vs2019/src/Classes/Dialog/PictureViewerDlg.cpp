/////////////////////////////////////////////////////////////////////////////
//
//  PictureViewerDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PictureViewerDlg.h"
#include "SRSIndex.h"
//-- For Magnetic Windows
#include "NXRemoteStudioDlg.h"
//-- 2011-9-19 Lee JungTaek
#include "exif.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

enum
{
	SMART_STYLE_WIDTH = 0,
	SMART_STYLE_HEIGHT	 ,
};

int gMonLeft = 0;
int gMonRight = 0;
int gMonTop = 0;
int gMonBottom = 0;

BOOL CALLBACK MonitorPictureEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
{
	MONITORINFOEX mi;
	mi.cbSize = sizeof(MONITORINFOEX);
	GetMonitorInfo(hMonitor, &mi); 

	CPoint    pos;                
	GetCursorPos(&pos);        

	if(pos.x > mi.rcMonitor.left && pos.x < mi.rcMonitor.right)
	{
		gMonLeft = mi.rcMonitor.left;
		gMonRight = mi.rcMonitor.right;
		gMonTop = mi.rcMonitor.top;
		gMonBottom = mi.rcMonitor.bottom;
	}
	return TRUE;
}

void CPictureViewerDlg::GetFullSize()
{
	HDC hDC = GetDC()->GetSafeHdc();
	::EnumDisplayMonitors(NULL, NULL, MonitorPictureEnumProc, (LPARAM)hDC);
}

// CPictureViewerDlg dialog
IMPLEMENT_DYNAMIC(CPictureViewerDlg, CDialogEx)

	//------------------------------------------------------------------------------ 
	//! @brief		CPictureViewerDlg(CString strPath)
	//! @date		2010-05-23
	//! @author	hongsu.jung
	//------------------------------------------------------------------------------ 
	CPictureViewerDlg::CPictureViewerDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogST(CPictureViewerDlg::IDD, pParent)
{
	m_strImagePath = _T("");
	m_strImageName = _T("");
	m_bSmartStyle		= SMART_STYLE_WIDTH;
	m_nPictureCount = 0;
	m_nAutoViewSize = 0;
	m_nSizeCount = 0;
	m_nMaxCount = 0;
	m_bDrawPicture = FALSE;
}

CPictureViewerDlg::~CPictureViewerDlg() {}
void CPictureViewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CBkDialogST::DoDataExchange(pDX);	
	DDX_Control(pDX,  IDC_BTN_MAXIMIZE	,m_btnMax		);
	DDX_Control(pDX,  IDC_BTN_CANCEL		,m_btnCancel		);
}				

BEGIN_MESSAGE_MAP(CPictureViewerDlg, CBkDialogST)
	ON_WM_NCHITTEST()
	ON_WM_PAINT()
	ON_WM_SIZE()		
	ON_WM_ERASEBKGND()
	ON_WM_GETMINMAXINFO()
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BTN_CANCEL, OnBnClickedBtnCancel)	
	ON_BN_CLICKED(IDC_BTN_MAXIMIZE, OnBnClickedBtnMaximize)	
END_MESSAGE_MAP()

// CLiveview message handlers
BOOL CPictureViewerDlg::OnEraseBkgnd(CDC* pDC)	{	return FALSE;}

void CPictureViewerDlg::OnPaint()
{	
	UpdateFrames();
	CDialogEx::OnPaint();
}

BOOL CPictureViewerDlg::OnInitDialog()
{

	CBkDialogST::OnInitDialog();
	//-- 2011-05-30
	CreateFrames();
	//-- 2011-9-19 Lee JungTaek
//	GetClientRect(&m_rect);
	//-- 2011-05-18 hongsu.jung
	UpdateFrames();
	return TRUE; 	
}

void CPictureViewerDlg::OnSize(UINT nType, int cx, int cy)
{
	CBkDialogST::OnSize(nType, cx, cy);
	//-- 2011-9-19 Lee JungTaek
//	GetClientRect(&m_rect);
	//-- 2011-05-18 hongsu.jung
	UpdateFrames();
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CPictureViewerDlg::UpdateFrames()
{
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	if(pMainWnd)
		m_nAutoViewSize = pMainWnd->m_nAutoViewSelectSize;

	m_nPictureCount++;
	//-- Rounding Dialog
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(rect.left, rect.top, rect.Width(), rect.Height(), WHITENESS);
//	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	mDC.FillRect(rect, &CBrush(RGB(63,63,63)));

	//-- Draw Back Ground
	DrawBackGround(&mDC, rect);
	//-- Draw Dialogs
	DrawDialogs();
//	Invalidate(FALSE);
	//-- Draw Image
	DrawSelectedImage(&mDC, rect);
	Invalidate(FALSE);

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
//	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
	if(m_bDrawPicture == FALSE && m_nMaxCount < 11)
		dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);

	if(m_nAutoViewSize == 1)
	{
		if(m_nSizeCount < 2)
			m_nSizeCount++;
		if(m_nSizeCount == 1)
			Maximize();
	}
	else
	{
		if(m_nPictureCount <= 2)
		{
			MoveWindow(rect.left, rect.top, rect.Width(), rect.Height());
		}
	}

	Invalidate(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CPictureViewerDlg::CreateFrames()
{
	//-- Skin Button
	m_btnMax		.LoadBitmaps(IDR_IMG_MAXIMIZE	,NULL,NULL,NULL);
	m_btnCancel		.LoadBitmaps(IDR_IMG_CANCEL		,NULL,NULL,NULL);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackGround
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
static const int size_title_w				= 7;
static const int size_title_h				= 30;
static const int size_body_w			= 3;

void CPictureViewerDlg::DrawBackGround(CDC* pDC, CRect rect)
{
	//-- Draw Round Dialog
	DrawRoundDialogs(pDC);

	int nImgX, nImgY, nImgW, nImgH;
	CString strPath = RSGetRoot();
	CString strFullPath;

	//-- 1. Title Bar
	nImgY		= 0;
	nImgH	= size_title_h;

	nImgX		= 0;			
	nImgW	= size_title_w;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_left.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);

	nImgX		= size_title_w;
	nImgW	= rect.Width() -  2 * size_title_w ;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_center.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);

	nImgX		= rect.Width() - size_title_w;
	nImgW	= size_title_w;
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_liveview_title_right.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);

	//-- 2. Body	
/*	nImgY		= size_title_h;
	nImgH	= rect.Height()-size_title_h;			
	//-- 2.1 Top/Left
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_top_left.png"),strPath);
	DrawImage(pDC, strFullPath, 0, size_title_h, 8, 7);
	//-- 2.2 Top/Center
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_top_center.png"),strPath);
	DrawImage(pDC, strFullPath, 8, size_title_h, rect.Width() - (8+8), 7);
	//-- 2.3 Top/Right
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_top_right.png"),strPath);
	DrawImage(pDC, strFullPath, rect.Width()-8, size_title_h, 8, 7);

	//-- 2.4 Top/Left
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_medium_left.png"),strPath);
	DrawImage(pDC, strFullPath, 0, size_title_h+7, 8, rect.Height() - 7*2 - size_title_h);
	//-- 2.5 Top/Center
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_medium_right.png"),strPath);
	DrawImage(pDC, strFullPath, rect.Width()-8, size_title_h+7, 8, rect.Height() - 7*2 - size_title_h);

	//-- 2.6 Top/Left
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_bottom_left.png"),strPath);
	DrawImage(pDC, strFullPath, 0, rect.Height() - 7, 8, 7);
	//-- 2.7 Top/Center
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_bottom_center.png"),strPath);
	DrawImage(pDC, strFullPath, 8, rect.Height() - 7, rect.Width() - (8+8), 7);
	//-- 2.8 Top/Right
	strFullPath.Format(_T("%s\\img\\03.Live View\\03_qview_bg_bottom_right.png"),strPath);
	DrawImage(pDC, strFullPath, rect.Width()-8, rect.Height() - 7, 8, 7);

	//-- Draw Background	
	//-- 1. Boader
	nImgX		= rect.left + 5;
	nImgY		= rect.top + size_title_h + 5;
	nImgH	= rect.Height() - size_title_h - 10 - 100;	
	nImgW	= rect.Width() -10 - 100;
	strFullPath.Format(_T("%s\\img\\03.Live View\\liveview_bk2.png"),strPath);
	DrawImage(pDC, strFullPath, nImgX, nImgY, nImgW, nImgH);*/

	//-- 2011-05-31 hongsu.jung
	//-- 2011-8-11 Lee JungTaek
	//-- Modify
	CString strFileName = _T("");
	strFileName.Format(_T("Picture View - %s"), m_strImageName);
	DrawDlgTitle(pDC, strFileName);
}
//------------------------------------------------------------------------------ 
//! @brief		DrawDialogs
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------
static const int size_btn_size			= 16;
static const int btn_top_margin		= 6;
static const int liveview_top_margin	= 4;
static const int thumbnail_height		= 125;
static const int toolbar_height			= 58;
static const int multiview_width		= 155;
static const int resize_frame			= 15;

void CPictureViewerDlg::DrawDialogs()
{	
	if(!ISSet())
		return;

	//-- Top Title.
	CRect rectDialog, rectBtn, rectLiveview, rectToolbar, rectThumbnail, rectMultiview;
	GetClientRect(&rectDialog);

	{	//-- 1. Button
		rectBtn.top		= btn_top_margin;
		rectBtn.bottom	= btn_top_margin + size_btn_size;
		rectBtn.left		= rectDialog.Width() - size_btn_size - btn_top_margin;
		rectBtn.right		= rectDialog.Width() - btn_top_margin;	
		m_btnCancel		.MoveWindow(rectBtn);

		rectBtn.left		= rectBtn.left - size_btn_size - size_body_w;
		rectBtn.right		= rectBtn.right - size_btn_size - size_body_w;
		m_btnMax		.MoveWindow(rectBtn);
	}
}

void CPictureViewerDlg::OnBnClickedBtnCancel()	
{
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	if(pMainWnd)
		m_nAutoViewSize = pMainWnd->m_nAutoViewSelectSize;

	if(m_nAutoViewSize == 0)
	{
		MoveWindow(0, 0, 602, 476);
		m_btnMax		.LoadBitmaps(IDR_IMG_MAXIMIZE	,NULL,NULL,NULL);
		ShowWindow(SW_NORMAL);
	}
	else
	{
		int nMonitor = GetSystemMetrics(SM_CMONITORS);
		GetFullSize();

		m_btnMax		.LoadBitmaps(IDR_IMG_WINNORMAL,NULL,NULL,NULL);
		ShowWindow(SW_MAXIMIZE);

		MoveWindow(gMonLeft, gMonTop, abs(gMonLeft - gMonRight), abs(gMonTop - gMonBottom) );
	}

	SendMessage(WM_CLOSE);
}
void CPictureViewerDlg::OnBnClickedBtnMaximize()	
{ 
	WINDOWPLACEMENT wndpl;
	GetWindowPlacement(&wndpl);
	if( wndpl.showCmd == SW_MAXIMIZE)
	{
		m_btnMax		.LoadBitmaps(IDR_IMG_MAXIMIZE	,NULL,NULL,NULL);
		ShowWindow(SW_NORMAL);	
	}
	else
	{
		int nMonitor = GetSystemMetrics(SM_CMONITORS);
		GetFullSize();

		m_btnMax		.LoadBitmaps(IDR_IMG_WINNORMAL,NULL,NULL,NULL);
		ShowWindow(SW_MAXIMIZE);

		MoveWindow(gMonLeft, gMonTop, abs(gMonLeft - gMonRight), abs(gMonTop - gMonBottom) ); 
	}
}

void CPictureViewerDlg::Maximize()
{
	int nMonitor = GetSystemMetrics(SM_CMONITORS);
	GetFullSize();

	m_btnMax		.LoadBitmaps(IDR_IMG_WINNORMAL,NULL,NULL,NULL);
	ShowWindow(SW_MAXIMIZE);

	MoveWindow(gMonLeft, gMonTop, abs(gMonLeft - gMonRight), abs(gMonTop - gMonBottom) ); 
}

//------------------------------------------------------------------------------ 
//! @brief		DrawSelectedImage
//! @date		2011-05-21
//! @author		Lee JungTaek
//! @note	 	Draw Selected Image
//------------------------------------------------------------------------------ 
void CPictureViewerDlg::DrawSelectedImage(CDC* pDC, CRect rect)
{
	if(!m_strImagePath.GetLength())
		return;

	int nOrientation = 1;

	//-- 2011-9-19 Lee JungTaek
	//-- Check Exif File
	EXIFINFO exifinfo;
	m_nMaxCount = 0;
	m_bDrawPicture = FALSE;
TEST:
	FILE* hFile= _tfopen((LPCTSTR)m_strImagePath, _T("rb"));

	if (hFile)
	{
		memset(&exifinfo,0,sizeof(EXIFINFO));

		Cexif exif(&exifinfo);
		exif.DecodeExif(hFile);

		fclose(hFile);

		if(exifinfo.IsExif) nOrientation = exifinfo.Orientation;
		else	return;
	}
	else
	{
		if(m_nMaxCount > 10)
		{
			return;
		}
		else
		{
			m_nMaxCount++;
			if(m_nMaxCount > 11)
				m_bDrawPicture = TRUE;
			Sleep(100);
			goto TEST;
			return;
		}
	}

	//-- 2011-9-19 Lee JungTaek
	//-- Rotate Image
	Image Img(m_strImagePath);

	switch(nOrientation)
	{
	case 1:	
		if(&Img) Img.RotateFlip(RotateNoneFlipNone);	

		if(m_bSmartStyle != SMART_STYLE_WIDTH)
		{
			rect.right = m_rect.left + m_rect.Height();
			rect.bottom = m_rect.top + m_rect.Width();

			m_bSmartStyle = SMART_STYLE_WIDTH;
		}

		break;
	case 3:	
		if(&Img) Img.RotateFlip(Rotate180FlipNone);	

		if(m_bSmartStyle != SMART_STYLE_WIDTH)
		{
			rect.right = m_rect.left + m_rect.Height();
			rect.bottom = m_rect.top + m_rect.Width();

			m_bSmartStyle = SMART_STYLE_WIDTH;
		}

		break;
	case 6:	
		if(&Img) Img.RotateFlip(Rotate90FlipNone);	

		if(m_bSmartStyle != SMART_STYLE_WIDTH) //SMART_STYLE_HEIGHT)
		{
			rect.right = m_rect.left + m_rect.Height();
			rect.bottom = m_rect.top + m_rect.Width();

			m_bSmartStyle = SMART_STYLE_WIDTH; //SMART_STYLE_HEIGHT;
		}
		break;	
	case 8:	
		if(&Img) Img.RotateFlip(Rotate270FlipNone);	

		if(m_bSmartStyle != SMART_STYLE_HEIGHT)
		{
			rect.right = m_rect.left + m_rect.Height();
			rect.bottom = m_rect.top + m_rect.Width();

			m_bSmartStyle = SMART_STYLE_HEIGHT;
		}
		break;
	}

	//-- Set Draw Rect
	switch(nOrientation)
	{
	case 1:	
	case 3:	
		{
			int nRate = Img.GetWidth() / rect.Width();
			if (nRate == 0)
				nRate = 1;
			int nHeight = Img.GetHeight() / nRate;

//			rect.bottom = rect.top + nHeight;
		}
		break;
	case 6:		
	case 8:	
		{
			int nRate = Img.GetHeight() / rect.Height();
			int nWidth = Img.GetWidth() / nRate;

//			rect.right = rect.left + nWidth;
		}
	}

	double imageSize1 = (double)Img.GetWidth()/(double)Img.GetHeight();
	double imageSize2 = (double)(Img.GetHeight()*abs(gMonRight-gMonLeft))/(double)Img.GetWidth();
	Rect	DesRect;
	double monitorRate = (double)abs(gMonRight-gMonLeft)/(double)abs(gMonTop-gMonBottom);

	if((double)Img.GetWidth() >= (double)Img.GetHeight())
	{
		WINDOWPLACEMENT wndpl;
		GetWindowPlacement(&wndpl);
		if( wndpl.showCmd == SW_MAXIMIZE)
		{
			if(monitorRate <= 1.5)
			{
				if(imageSize1 == 1)
				{
					DesRect.X = (abs(gMonRight - gMonLeft) - abs(gMonBottom - gMonTop - size_title_h))/2;
					DesRect.Y = size_title_h;
					DesRect.Width = abs(gMonTop-gMonBottom) - size_title_h;
					DesRect.Height = abs(gMonTop-gMonBottom) - size_title_h;
				}
				else if(imageSize1 <= 1.5 && imageSize1 >1)
				{
					DesRect.X = 0;
					DesRect.Y = (abs(gMonBottom - gMonTop - size_title_h) - (abs(gMonRight - gMonLeft)/imageSize1))/2 + size_title_h;
					DesRect.Width = abs(gMonRight - gMonLeft);
					DesRect.Height = abs(gMonRight - gMonLeft)/imageSize1;
				}
				else
				{
					DesRect.X = 0;
					DesRect.Y = (abs(gMonBottom - gMonTop - size_title_h) - (abs(gMonRight - gMonLeft)/imageSize1))/2 + size_title_h;
					DesRect.Width = abs(gMonRight - gMonLeft);
					DesRect.Height = abs(gMonRight - gMonLeft)/imageSize1;
				}
			}
			else
			{
				if(imageSize1 <= 1.5)
				{

					DesRect.X = (abs(gMonRight-gMonLeft) - (gMonBottom * imageSize1))/2;
					DesRect.Y = size_title_h;
					DesRect.Width = (gMonBottom - size_title_h) * imageSize1;
					DesRect.Height = gMonBottom - size_title_h;
				}
				else
				{
					DesRect.X = (abs(gMonRight-gMonLeft) - (abs(imageSize2 - size_title_h) * imageSize1))/2;
					DesRect.Y = (abs(gMonBottom - imageSize2)/2) + size_title_h;
					DesRect.Width = abs(imageSize2 - size_title_h) * imageSize1;
					DesRect.Height = imageSize2 - size_title_h;
				}
			}
		}
		else
		{
			if(imageSize1 == 1)
			{
				DesRect.Width = (rect.bottom - rect.top - size_title_h);
				DesRect.Height = (rect.bottom - rect.top- size_title_h);
				DesRect.X = abs(rect.right - DesRect.Width)/2;
				DesRect.Y = rect.top + size_title_h;

			}
			else
			{
				DesRect.Width = rect.right;
				DesRect.Height = rect.right/imageSize1;
				DesRect.X = 0;
				DesRect.Y = abs((rect.bottom - DesRect.Height - size_title_h)/2)+30;
			}
		}
	}
	else
	{
		WINDOWPLACEMENT wndpl;
		GetWindowPlacement(&wndpl);
		if( wndpl.showCmd == SW_MAXIMIZE)
		{
			if(imageSize1 == 1)
			{
				DesRect.X = (abs(gMonRight - gMonLeft) - abs(gMonBottom - gMonTop - size_title_h))/2;
				DesRect.Y = size_title_h;
				DesRect.Width = abs(gMonTop-gMonBottom) - size_title_h;
				DesRect.Height = abs(gMonTop-gMonBottom) - size_title_h;
			}
			else
			{
				DesRect.Height = abs(gMonTop-gMonBottom) - size_title_h;
				DesRect.Width =  (double)Img.GetWidth()*((double)DesRect.Height/(double)Img.GetHeight());
				DesRect.X = (abs(gMonRight - gMonLeft) - (DesRect.Width))/2;
				DesRect.Y = size_title_h;
			}
		}
		else
		{
			if(imageSize1 == 1)
			{
				DesRect.Width = (rect.bottom - rect.top - size_title_h);
				DesRect.Height = (rect.bottom - rect.top- size_title_h);
				DesRect.X = abs(rect.right - DesRect.Width)/2;
				DesRect.Y = rect.top + size_title_h;
			}
			else
			{
				DesRect.Height = rect.bottom - rect.top - size_title_h;
				DesRect.Width = Img.GetWidth()/(Img.GetHeight()/DesRect.Height);
				DesRect.X = (rect.right - DesRect.Width)/2;
				DesRect.Y = rect.top + size_title_h;
			}
		}
	}

/*	Rect		DesRect;
	DesRect.X = rect.left;
	DesRect.Y = rect.top +  + size_title_h;
	DesRect.Width = (rect.right - rect.left);
	DesRect.Height = (rect.bottom - rect.top) - size_title_h; */

	Graphics gc(pDC->GetSafeHdc());
	gc.DrawImage(&Img, DesRect);
}

//static const int min_frame_size_width	= 400;
//static const int min_frame_size_height	= 300;

static const int min_frame_size_width	= 602;
static const int min_frame_size_height	= 476;

void CPictureViewerDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = min_frame_size_width;
	lpMMI->ptMinTrackSize.y = min_frame_size_height;

	RECT rcWorkArea;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWorkArea, SPIF_SENDCHANGE);

	lpMMI->ptMaxSize.x = rcWorkArea.right;
	lpMMI->ptMaxSize.y = rcWorkArea.bottom;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}

LRESULT CPictureViewerDlg::OnNcHitTest(CPoint point) 
{
	LRESULT nHitTest = CWnd::OnNcHitTest(point);
	//if( nHitTest == HTCLIENT)
	if(point.y > 0 && point.y < size_title_h)
		nHitTest = HTCAPTION;
	return nHitTest;
}

LRESULT CPictureViewerDlg::OnEndResizeEvent(WPARAM w, LPARAM l)
{
	int left = HIWORD(w);
	int right = LOWORD(w);
	int top = HIWORD(l);
	int bottom = LOWORD(l);

//	MoveWindow(left,top,right-left,bottom-top,1);
//	mouse_event( MOUSEEVENTF_LEFTUP,0,0,0,0);
	return 0L;
}

void CPictureViewerDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect rt;
	this->GetWindowRect(&rt);
	if(point.x<5 || point.y<10 || rt.right-5 <point.x || (rt.bottom-rt.top-10 < point.y) || (rt.right-rt.left-10<point.x))
	{
		// 		mouse_event(MOUSEEVENTF_LEFTUP,0,0,0,0);
		// 		CTransDlg dlg;
		// 		dlg.SetWindowRect(rt.left,rt.top,rt.right-rt.left  ,rt.bottom - rt.top);
		// 		dlg.SetRestrict(LIVEVIEW_DLG_WIDTH,LIVEVIEW_DLG_HEIGHT);
		// 		dlg.DoModal();
	}
	CBkDialogST::OnLButtonDown(nFlags, point);
}

BOOL CPictureViewerDlg::SetImageName(CString str)
{ 
	if(m_strImageName == str)
	{
		if(this->IsWindowVisible())
			return FALSE;
	}

	m_strImageName = str;
	return TRUE;
}
