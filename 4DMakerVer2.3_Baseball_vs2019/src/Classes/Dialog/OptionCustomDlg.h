/////////////////////////////////////////////////////////////////////////////
//
//  OptionCustomDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "OptionBaseDlg.h"
#include "NXRemoteStudioFunc.h"
#include "NXComboBox.h"

//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
#define SETDIALOG_ITEM_VIEW_SIZE 6

// COptionCustomDlg dialog
class COptionCustomDlg : public COptionBaseDlg
{
	DECLARE_DYNAMIC(COptionCustomDlg)	
public:
	COptionCustomDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionCustomDlg();

// Dialog Data
	enum { IDD = IDD_DLG_OPT_CUSTOM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSelchangeCmbISOStep();	
	afx_msg void OnSelchangeCmbAutoISORange();
	afx_msg void OnSelchangeCmbLDC();
	afx_msg void OnSelchangeCmbAFLamp();
	//CMiLRe 20140902 ISO Expansion 추가
	afx_msg void OnSelchangeCmbISOExpansion();
	//dh0.seo 2014-09-15 Auto Display Off
	afx_msg void OnSelchangeCmbAutoDisplayOff();

	// dh9.seo 2013-06-25 Quick View, Auto Display Off, Power Save
/*	afx_msg void OnSelChangeCmbQuickView();
	afx_msg void OnSelChangeCmbAutoDisplayOff();
	afx_msg void OnSelChangeCmbPowerSave(); */
public:
	
private:	
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();	
	void UpdateFrames();
	int m_nMode;

public:
	void InitProp(RSCustom& opt);
	void SaveProp(RSCustom& opt);

	//-- 2011-6-22 Lee JungTaek
	void SetOptionProp(int nPropIndex, int nPropValue, RSCustom& opt);
	void SetOptionISOStep(int nEnum);
	//CMiLRe 20140902 ISO Expansion 추가
	void SetOptionISOExpansion(int nEnum);
	void SetOptionAutoISORange(int nEnum);
	void SetOptionLDC(int nEnum);
	void SetOptionAFLamp(int nEnum);

	int ChangeIntValueToIndex(int nPropIndex, int nPropValue);
	void UpdateISOExpansion(int nEnum);
	void SetDropDownHeight(CNXComboBox* pMyComboBox, int itemsToShow);

	//dh0.seo 2014-10-17 추가
	void SetMode(int nMode) {m_nMode = nMode;}
	int GetMode(){return m_nMode;}
	void SetUpdate();

	//CMiLRe 20141114 IOS Auto Range가 Value에서 Enum으로 변경
	BOOL m_b3DMode;
	void SetLensOption3DMode(BOOL bValue) {m_b3DMode = bValue;}
	BOOL GetLensOption3DMode() {return m_b3DMode;}
	void Set3DISORangeShow();
public:	
	CNXComboBox	m_comboBox[6];
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	CStatic * m_staticLabel[SETDIALOG_ITEM_VIEW_SIZE];
	CStatic* m_staticLabelBar[SETDIALOG_ITEM_VIEW_SIZE];
	int m_nISOStep		;	
	int m_nAutoISORange	;
	int m_nLDC			;
	int m_nAFLamp		;
	//CMiLRe 20140902 ISO Expansion 추가
	int m_nISOExpansion	;
	//dh0.seo 2014-09-15 Auto Display Off
	int m_nAutoDisplay;
};