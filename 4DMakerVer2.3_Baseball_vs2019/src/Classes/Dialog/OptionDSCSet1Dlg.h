/////////////////////////////////////////////////////////////////////////////
//
//  OptionDSCSet1Dlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "OptionBaseDlg.h"
#include "PopupMsgDlg.h"
#include "NXRemoteStudioFunc.h"

#define SETDIALOG_ITEM_VIEW_SIZE 6

// COptionDSCSet1Dlg dialog
class COptionDSCSet1Dlg : public COptionBaseDlg
{
	DECLARE_DYNAMIC(COptionDSCSet1Dlg)	
public:
	COptionDSCSet1Dlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionDSCSet1Dlg();

// Dialog Data
	enum { IDD = IDD_DLG_OPT_DSC_SET1 };

protected:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnSelchangeCmbFileNmae();
	afx_msg void OnSelchangeCmbFileNumber();
	afx_msg void OnSelchangeCmbFolderType();
	afx_msg void OnSelchangeCmbReset();
	//dh0.seo 2014-09-12
	afx_msg void OnSelchangeCmbPowerSave();
	//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
	afx_msg void OnSelFormat();
	afx_msg void OnSelReset();	
		
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	DECLARE_MESSAGE_MAP()

private:	
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();	
	void UpdateFrames();	
	void ResetButton();
	BOOL PtInRect(CPoint pt, BOOL bMove);
	CRect m_rectBtn[2];
	int m_nStatus[2];

	CPopupMsgDlg* m_pPopupMsg;
	

public:
	CComboBox	m_comboBox[5];
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	CStatic * m_staticLabel[SETDIALOG_ITEM_VIEW_SIZE];
	CStatic* m_staticLabelBar[SETDIALOG_ITEM_VIEW_SIZE];
	int m_nFileName		;
	int m_nFileNumber	;
	int m_nFolderType	;
	int m_nPowerSave	;
	int m_nReset;
public:
	void InitProp(RSDSCSet1& opt);
	void SaveProp(RSDSCSet1& opt);
	
	//-- 2011-8-11 Lee JungTaek
	void LoadPopupMsg(int nType, CString strMsg, int nDelay, RSEvent* pMsg = NULL);

	void SetOptionProp(int nPropIndex, int nPropValue, RSDSCSet1& opt);
	int ChangeIntValueToIndex(int nPropIndex, int nPropValue);

	//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
	CButton  m_PushBtn[2];
};