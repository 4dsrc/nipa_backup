/////////////////////////////////////////////////////////////////////////////
//
//  TimerCaptureStausDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Lee JungTaek
// @Date	2011-07-16
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TimerCaptureStausDlg.h"
#include "OptionTimeLapseDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const int first_line_top		= 40;
static const int second_line_top	= 67;
static const int third_line_top		= 94;

static const int line_text_left		= 10;	
static const int line_value_left	= 190;

static const int text_width		= 180;	
static const int value_width	= 40;

static const int line_height		= 27;

// CTimerCaptureStausDlg dialog

IMPLEMENT_DYNAMIC(CTimerCaptureStausDlg,  CBkDialogST)

CTimerCaptureStausDlg::CTimerCaptureStausDlg(int nDelayTime, int nTimerCaptureTime, int nTimerCaptureCnt, CWnd* pParent /*=NULL*/)
	: CBkDialogST(CTimerCaptureStausDlg::IDD, pParent)
{
	m_nDealyTime			= nDelayTime;
	m_nTimerCaptureTime		= nTimerCaptureTime;
	m_nTimerCaptureCnt		= nTimerCaptureCnt;
}

CTimerCaptureStausDlg::~CTimerCaptureStausDlg()
{
}

void CTimerCaptureStausDlg::DoDataExchange(CDataExchange* pDX)
{
	CBkDialogST::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTimerCaptureStausDlg, CBkDialogST)
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

BOOL CTimerCaptureStausDlg::OnInitDialog()
{
	CBkDialogST::OnInitDialog();

	// TODO:  Add extra initialization here
	//SetBitmap(RSGetRoot(_T("img\\04.Option Popup\\TimerCaptureStatus_bk.png")), RGB(255,255,255));
	SetItemPosition();
	SetItemValue();	

	DrawBackground();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTimerCaptureStausDlg::SetItemPosition()
{
	m_rectTitle								= CRect(CPoint(50, 10), CSize(200, 30));
	m_rectStatusText[TCS_STATUS_LINE_FIRST] = CRect(CPoint(line_text_left, first_line_top), CSize(text_width, line_height));
	m_rectStatusValue[TCS_STATUS_LINE_FIRST]= CRect(CPoint(line_value_left, first_line_top), CSize(value_width, 20));
	m_rectStatusText[TCS_STATUS_LINE_SECOND] = CRect(CPoint(line_text_left, second_line_top), CSize(text_width, line_height));
	m_rectStatusValue[TCS_STATUS_LINE_SECOND]= CRect(CPoint(line_value_left, second_line_top), CSize(value_width, 20));
	m_rectStatusText[TCS_STATUS_LINE_TIRTH] = CRect(CPoint(line_text_left, third_line_top), CSize(text_width, line_height));
	m_rectStatusValue[TCS_STATUS_LINE_TIRTH]= CRect(CPoint(line_value_left, third_line_top), CSize(value_width, 20));
	Image img(RSGetRoot(_T("img\\04.Option Popup\\TimerCaptureStatus_bk.png")));
	m_rectBtn								= CRect(CPoint(img.GetWidth() / 2 - 37, img.GetHeight() - 31),  CSize(75,25));

	//-- Get Parent Rect
	CRect rectChild;
	GetWindowRect(&rectChild);

	CRect rectParent;
	GetWindowRect(&rectParent);

	MoveWindow((rectParent.left + rectParent.right) / 2 - img.GetWidth() / 2 ,  (rectParent.top + rectParent.bottom) / 2 - img.GetWidth() / 2 , img.GetWidth(), img.GetHeight());
	this->ShowWindow(SW_SHOW);
}

//------------------------------------------------------------------------------ 
//! @brief		SetItemValue
//! @date		2011-7-18
//! @author		Lee JungTaek
//! @note	 	Default Setting
//------------------------------------------------------------------------------ 
void CTimerCaptureStausDlg::SetItemValue()
{
	//-- Set Remain Time
	if(RSGetValueEnum(RS_OPT_TIMELAPSE_DELAYENABLE))
	{
		int nMin = m_nDealyTime / 60;

		if(nMin > 0)
			m_strRemainTime.Format(_T("%02d:%02d"), nMin, m_nDealyTime - 60);
		else
			m_strRemainTime.Format(_T("%02d:%02d"), nMin, m_nDealyTime);
	}
	else
	{
		int nMin = m_nTimerCaptureTime / 60;

		if(nMin > 0)
			m_strRemainTime.Format(_T("%02d:%02d"), nMin, m_nTimerCaptureTime - 60);
		else
			m_strRemainTime.Format(_T("%02d:%02d"), nMin, m_nTimerCaptureTime);
	}

	m_strAlreadyCnt.Format(_T("0"));
	m_strRemainCnt.Format(_T("%d"), m_nTimerCaptureCnt);
}

void CTimerCaptureStausDlg::OnPaint()
{	
	DrawBackground();	
	CBkDialogST::OnPaint();
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2011-7-9
//! @author		Lee JungTaek
//! @note	 	OnPaint
//------------------------------------------------------------------------------ 
void CTimerCaptureStausDlg::DrawBackground()
{
	CPaintDC dc(this);

	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Round Dialog
	DrawRoundDialogs(&mDC, TRUE);
	DrawImageBase(&mDC, RSGetRoot(_T("img\\04.Option Popup\\TimerCaptureStatus_bk.png")),0, 0);

	DrawTitle(&mDC);
	DrawLine(TCS_STATUS_LINE_FIRST, &mDC);
	DrawLine(TCS_STATUS_LINE_SECOND, &mDC);
	DrawLine(TCS_STATUS_LINE_TIRTH, &mDC);
	DrawMenu(BTN_TCS_STOP		  ,	&mDC);

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawTitle
//! @date		2011-7-16
//! @author		Lee JungTaek
//! @note	 	DrawTitle
//------------------------------------------------------------------------------ 
void CTimerCaptureStausDlg::DrawTitle(CDC* pDC)
{
	int nX, nY;
	nX		= m_rectTitle.left;	
	nY		= m_rectTitle.top;

	DrawStr(pDC, STR_SIZE_TEXT, _T("Timer Capture Status"), nX, nY, RGB(255,255,255));
}

//------------------------------------------------------------------------------ 
//! @brief		DrawStatusText / DrawStatusValue
//! @date		2011-7-9
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CTimerCaptureStausDlg::DrawLine(int nLine, CDC* pDC)
{
	DrawStatusText(nLine, pDC);
	DrawStatusValue(nLine, pDC);
}

void CTimerCaptureStausDlg::DrawStatusText(int nLine, CDC* pDC)
{
	int nX, nY;
	nX		= m_rectStatusText[nLine].left;	
	nY		= m_rectStatusText[nLine].top;

	switch(nLine)
	{
	case TCS_STATUS_LINE_FIRST	:	DrawStr(pDC, STR_SIZE_MIDDLE, _T("Time until next capture"), nX, nY, RGB(204,204,204));	break;
	case TCS_STATUS_LINE_SECOND	:	DrawStr(pDC, STR_SIZE_MIDDLE, _T("Already captured number"), nX, nY, RGB(204,204,204));	break;
	case TCS_STATUS_LINE_TIRTH	:	DrawStr(pDC, STR_SIZE_MIDDLE, _T("Remain capture number"), nX, nY, RGB(204,204,204));	break;
	}
}

void CTimerCaptureStausDlg::DrawStatusValue(int nLine, CDC* pDC)
{
	int nX, nY;
	nX		= m_rectStatusValue[nLine].left;	
	nY		= m_rectStatusValue[nLine].top;

	switch(nLine)
	{
	case TCS_STATUS_LINE_FIRST	:	DrawStr(pDC, STR_SIZE_MIDDLE, m_strRemainTime, nX, nY, RGB(204,204,204));	break;
	case TCS_STATUS_LINE_SECOND	:	DrawStr(pDC, STR_SIZE_MIDDLE, m_strAlreadyCnt, nX, nY, RGB(204,204,204));	break;
	case TCS_STATUS_LINE_TIRTH	:	DrawStr(pDC, STR_SIZE_MIDDLE, m_strRemainCnt, nX, nY, RGB(204,204,204));	break;
	}
}

void CTimerCaptureStausDlg::DrawMenu(int nItem, CDC* pDC)
{
	int nX, nY, nW, nH;
	nX	= m_rectBtn.left;	
	nY	= m_rectBtn.top;
	nW	= m_rectBtn.Width();
	nH	= m_rectBtn.Height();

	COLORREF color;

	switch(nItem)
	{
	case BTN_TCS_STOP	:	
		switch(m_nStatus)
		{
		case PT_TCS_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_focus.png")),	nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		case PT_TCS_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_push.png")),	nX, nY, nW, nH);	color = RGB(255,255,255);	break;
		default:				DrawGraphicsImage(pDC,  RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_btn02_normal.png")), nX, nY, nW, nH);	color = RGB(219,219,219);	break;		
		}

		DrawStr(pDC, STR_SIZE_MIDDLE, _T("Stop"),m_rectBtn[nItem],color);	
		break;	
	}
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CTimerCaptureStausDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CTimerCaptureStausDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CTimerCaptureStausDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_TCS_CNT; i ++)
		ChangeStatus(i,PT_TCS_NOR);
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CTimerCaptureStausDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
 	int nItem = PtInRect(point, FALSE);	
 
	switch(nItem)
	{
	case BTN_TCS_STOP	:	
		{
			//-- 2011-7-18 Lee JungTaek
			//-- Send Timer Capture Event
			RSEvent* pMsg		= new RSEvent;
			pMsg->message	= WM_RS_STOP_TIMER_CAPUTRE;
			::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
		}
		break;
			
 	}
	CDialog::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CTimerCaptureStausDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CTimerCaptureStausDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_TCS_IN_NOTHING;
	int nStart = 0;

	for(int i = nStart ; i < BTN_TCS_CNT; i ++)
	{
		if(m_rectBtn.PtInRect(pt))
		{
			nReturn = i;
			if(bMove)
				ChangeStatus(i,PT_TCS_INSIDE);
			else
				ChangeStatus(i,PT_TCS_DOWN); 
		}
		else
			ChangeStatus(i,PT_TCS_NOR); 
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CTimerCaptureStausDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus != nStatus)
	{
		m_nStatus = nStatus;
		CPaintDC dc(this);
		DrawMenu(nItem, &dc);
		InvalidateRect(m_rectBtn,FALSE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CTimerCaptureStausDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_TCS_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_TCS_INSIDE); 
		else
			ChangeStatus(i,PT_TCS_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		SetAlreadyCapturedCnt
//! @date		2011-7-18
//! @author		Lee JungTaek
//! @note	 	Default Value and String
//------------------------------------------------------------------------------ 
void CTimerCaptureStausDlg::UpdateCapturedCnt(int nCnt)
{
	if(this->GetSafeHwnd())
	{
		m_strAlreadyCnt.Format(_T("%d"), nCnt);
		m_strRemainCnt.Format(_T("%d"), m_nTimerCaptureCnt - nCnt);

		InvalidateRect(m_rectStatusValue[TCS_STATUS_LINE_SECOND], FALSE);
		InvalidateRect(m_rectStatusValue[TCS_STATUS_LINE_TIRTH], FALSE);

		TRACE(_T("m_strAlreadyCnt = %d	m_strRemainCnt = %d\n"), m_strAlreadyCnt, m_strRemainCnt);
	}	
}

void CTimerCaptureStausDlg::UpdateRemainTime(int nTime)
{
	int nMin = nTime / 60;

	if(nMin > 0)
		m_strRemainTime.Format(_T("%02d:%02d"), nMin, nTime - 60);
	else
		m_strRemainTime.Format(_T("%02d:%02d"), nMin, nTime);

	InvalidateRect(m_rectStatusValue[TCS_STATUS_LINE_FIRST], FALSE);
}