/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelBarometerChildDlg.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-09-15
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SmartPanelBarometerChildDlg.h"
#include "afxdialogex.h"
#include "SmartPanelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CSmartPanelBarometerChildDlg, CDialog)

CSmartPanelBarometerChildDlg::CSmartPanelBarometerChildDlg(CWnd* pParent /*=NULL*/)
	: CBkDialogST(CSmartPanelBarometerChildDlg::IDD, pParent)
{
	m_nMouseIn = BTN_BM_NULL;	
	for(int i=0; i<BTN_BM_CNT - 1; i++)
	{
		m_nStatus[i] = BTN_BM_CNT;
		m_rectBtn[i] = CRect(0,0,0,0);
	}

	m_bPosType = FALSE;
	m_pRSBarometer = NULL;

	//-- 2011-7-14 Lee JungTaek
	m_rectText[TEXT_POS_FIRST] = CRect(CPoint(40,18),CSize(35,30));
	m_rectText[TEXT_POS_SECOND] = CRect(CPoint(82,18),CSize(35,30));
	m_rectText[TEXT_POS_THIRD] = CRect(CPoint(124,14),CSize(45,30));
	m_rectText[TEXT_POS_FOURTH] = CRect(CPoint(180,18),CSize(35,30));
	m_rectText[TEXT_POS_FIFTH] = CRect(CPoint(222,18),CSize(35,30));

	m_rectBtn[BTN_BM_LEFT] = CRect(CPoint(9,10), CSize(25,24));
	m_rectBtn[BTN_BM_RIGHT] = CRect(CPoint(283,10), CSize(25,24));

	m_rectValue = CRect(CPoint(45,15), CSize(230,20));
}

CSmartPanelBarometerChildDlg::~CSmartPanelBarometerChildDlg()
{
}

void CSmartPanelBarometerChildDlg::DoDataExchange(CDataExchange* pDX)
{
	CBkDialogST::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSmartPanelBarometerChildDlg, CBkDialogST)
	ON_WM_PAINT()	
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

BOOL CSmartPanelBarometerChildDlg::OnInitDialog()
{
	CBkDialogST::OnInitDialog();		
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSmartPanelBarometerChildDlg::SetBGImageType()
{
	if(!m_bPosType)
		SetBitmap(RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_bg2_01.png")),RGB(255,255,255));
	else
		SetBitmap(RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_bg2_02.png")),RGB(255,255,255));
}

void CSmartPanelBarometerChildDlg::OnPaint()
{	
	DrawBackground();	
	CBkDialogST::OnPaint();
}

void CSmartPanelBarometerChildDlg::DrawBackground()
{
	CPaintDC dc(this);
	
	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- Draw Background
	if(!m_bPosType)
		DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_bg2_01.png")),0, 0);
	else
		DrawImageBase(&mDC, RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_bg2_02.png")),0, 0);

	//-- Draw Value
	DrawDlgValue(&mDC);
	InvalidateRect(m_rectValue, FALSE);
	
	//-- Draw Menu
	DrawMenu(BTN_BM_LEFT, &mDC);
	DrawMenu(BTN_BM_RIGHT, &mDC);	

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawMenu
//! @date		2011-8-1
//! @author		Lee JungTaek
//! @note	 	Draw Buttons
//------------------------------------------------------------------------------ 
void CSmartPanelBarometerChildDlg::DrawMenu(int nItem, CDC* pDC)
{
	int nX, nY, nW, nH;
	nX	= m_rectBtn[nItem].left;	
	nY	= m_rectBtn[nItem].top;
	nW	= m_rectBtn[nItem].Width();
	nH	= m_rectBtn[nItem].Height();

	switch(nItem)
	{
	case BTN_BM_LEFT:
		switch(m_nStatus[nItem])
		{
		case PT_BM_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_focus.png")),		nX, nY,nW, nH);	break;
		case PT_BM_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_push.png")),		nX, nY,nW, nH);	break;
		case PT_BM_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_disable.png")),	nX, nY,nW, nH);	break;
		default	:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_left_normal.png")),		nX, nY,nW, nH);	break;
		}
		break;
	case BTN_BM_RIGHT:
		switch(m_nStatus[nItem])
		{
		case PT_BM_INSIDE	:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_focus.png")),		nX, nY,nW, nH);	break;
		case PT_BM_DOWN		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_push.png")),		nX, nY,nW, nH);	break;
		case PT_BM_DIS		:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_disable.png")),	nX, nY,nW, nH);	break;
		default:				DrawGraphicsImage(pDC, RSGetRoot(_T("img\\02.Smart Panel\\02_wbfluorescent_move_right_normal.png")),	nX, nY,nW, nH);	break;
		}
		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawDlgValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelBarometerChildDlg::DrawDlgValue(CDC* pDC)
{
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_SMART					:		
	case DSC_MODE_P						:		
	case DSC_MODE_A						:		
	case DSC_MODE_S						:		
	case DSC_MODE_M						:
	case DSC_MODE_BULB					: //dh0.seo 2011-01-12 ��ġ �̵�
		switch(m_pRSBarometer->GetPropIndex())
		{
		case DSC_PASM_SHUTTERSPEED:		
		case DSC_PASM_APERTURE:			DrawCtrlText(pDC);			break;
		case DSC_PASM_ISO:				DrawCtrlValue(pDC);			break;
		}		
		break;
	case DSC_MODE_I					 	:		
	case DSC_MODE_CAPTURE_MOVIE		 	:		
	case DSC_MODE_MAGIC_MAGIC_FRAME		:
	case DSC_MODE_MAGIC_SMART_FILTER	:				break;
	case DSC_MODE_PANORAMA			 	:
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:		
	case DSC_MODE_SCENE_PORTRAIT		:		
	case DSC_MODE_SCENE_CHILDREN		:		
	case DSC_MODE_SCENE_SPORTS		 	:		
	case DSC_MODE_SCENE_CLOSE_UP		:		
	case DSC_MODE_SCENE_TEXT			:		
	case DSC_MODE_SCENE_SUNSET		 	:		
	case DSC_MODE_SCENE_DAWN			:		
	case DSC_MODE_SCENE_BACKLIGHT		:		
	case DSC_MODE_SCENE_FIREWORK		:		
	case DSC_MODE_SCENE_BEACH_SNOW	 	:		
	case DSC_MODE_SCENE_SOUND_PICTURE	:		
	case DSC_MODE_SCENE_3D			 	:
	case DSC_MODE_SCENE_BEST			:
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_ACTION			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
	case DSC_MODE_SCENE_CREATIVE		:
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
	case DSC_MODE_SCENE_PANORAMA		:
		switch(m_pRSBarometer->GetPropIndex())
		{
		case DSC_SCENE_BEAUTY_SHUTTERSPEED:		
		case DSC_SCENE_BEAUTY_APERTURE:			DrawCtrlText(pDC);			break;
		}
		break;
	case DSC_MODE_MOVIE				 	:		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawCtrlText
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Draw Text
//------------------------------------------------------------------------------ 
void CSmartPanelBarometerChildDlg::DrawCtrlText(CDC* pDC)
{
	CString strValue1 = m_pRSBarometer->GetPropStrValue(TEXT_POS_FIRST);
	CString strValue2 = m_pRSBarometer->GetPropStrValue(TEXT_POS_SECOND);
	CString strValue3 = m_pRSBarometer->GetPropStrValue(TEXT_POS_THIRD);
	CString strValue4 = m_pRSBarometer->GetPropStrValue(TEXT_POS_FOURTH);
	CString strValue5 = m_pRSBarometer->GetPropStrValue(TEXT_POS_FIFTH);

	Graphics gc(pDC->GetSafeHdc());
	CString strFontName = _T("SS_SJ_DTV_uni_20090209");
	CSize szStr;
	int x = 0, y = 0;

	//-- 1st Text
	Gdiplus::Font F1(strFontName, 11,FontStyleRegular,UnitPixel);
	szStr = pDC->GetOutputTextExtent(strValue1);
	x = (m_rectText[TEXT_POS_FIRST].left + m_rectText[TEXT_POS_FIRST].right) / 2 - (szStr.cx / 2) + 10;
	y = m_rectText[TEXT_POS_FIRST].top;
	PointF P1(x,y);

	SolidBrush B1(Color(50, 255,255,255));
	gc.DrawString(strValue1,-1,&F1,P1,&B1);
	
	//-- 2nd Text
	Gdiplus::Font F2(strFontName ,11,FontStyleRegular,UnitPixel);
	szStr = pDC->GetOutputTextExtent(strValue2);
	x = (m_rectText[TEXT_POS_SECOND].left + m_rectText[TEXT_POS_SECOND].right) / 2 - (szStr.cx / 2) + 10;
	y = m_rectText[TEXT_POS_SECOND].top;
	PointF P2(x,y);

	SolidBrush B2(Color(180, 255,255,255));
	gc.DrawString(strValue2,-1,&F2,P2,&B2);

	//-- 3rd Text (Setting Value)
	Gdiplus::Font F3(strFontName,14,FontStyleRegular,UnitPixel);
	szStr = pDC->GetOutputTextExtent(strValue3);
	x = (m_rectText[TEXT_POS_THIRD].left + m_rectText[TEXT_POS_THIRD].right) /2 - (szStr.cx / 2) + 10;
	y = m_rectText[TEXT_POS_THIRD].top;
	PointF P3(x,y);
	
	SolidBrush B3(Color(255,255,255));
	gc.DrawString(strValue3,-1,&F3,P3,&B3);
	
	//-- 4th Text
	Gdiplus::Font F4(strFontName,11,FontStyleRegular,UnitPixel);
	szStr = pDC->GetOutputTextExtent(strValue4);
	x = (m_rectText[TEXT_POS_FOURTH].left + m_rectText[TEXT_POS_FOURTH].right) / 2 - (szStr.cx / 2) + 10;
	y = m_rectText[TEXT_POS_FOURTH].top;
	PointF P4(x,y);
	
	SolidBrush B4(Color(180, 255,255,255));
	gc.DrawString(strValue4,-1,&F4,P4,&B4);
	
	//-- 5th Text
	Gdiplus::Font F5(strFontName,11,FontStyleRegular,UnitPixel);
	szStr = pDC->GetOutputTextExtent(strValue5);
	x = (m_rectText[TEXT_POS_FIFTH].left + m_rectText[TEXT_POS_FIFTH].right) / 2 - (szStr.cx / 2) + 10;
	y = m_rectText[TEXT_POS_FIFTH].top;
	PointF P5(x,y);

	SolidBrush B5(Color(50, 255,255,255));
	gc.DrawString(strValue5,-1,&F5,P5,&B5);	
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-13
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelBarometerChildDlg::DrawCtrlValue(CDC* pDC)
{
	//-- Set Alpha Color Matrix
	ColorMatrix matrix1 = {
		1,0,0,0,0,
		0,1,0,0,0,
		0,0,1,0,0,
		0,0,0,0.7f,0,
		0,0,0,0.0,1,
	};		
	ColorMatrix matrix2 = {
		1,0,0,0,0,
		0,1,0,0,0,
		0,0,1,0,0,
		0,0,0,0.2f,0,
		0,0,0,0.0,1,
	};

	ImageAttributes   IA1;   
	IA1.SetColorMatrix(&matrix1,ColorMatrixFlagsDefault,ColorAdjustTypeBitmap);

	ImageAttributes   IA2;   
	IA2.SetColorMatrix(&matrix2,ColorMatrixFlagsDefault,ColorAdjustTypeBitmap);

	//-- Set Rect
	Rect		DesRect;
	DesRect.X = 52;
	DesRect.Y = 12;
	DesRect.Width = 26;
	DesRect.Height = 20;

	Graphics gc(pDC->GetSafeHdc());

	//-- Value 1
	Image ImgValue1(m_pRSBarometer->GetPropStrValue(TEXT_POS_FIRST));
	gc.DrawImage(&ImgValue1, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA2);

	//-- Value 2
	Image ImgValue2(m_pRSBarometer->GetPropStrValue(TEXT_POS_SECOND));
	DesRect.X += 46;	
	gc.DrawImage(&ImgValue2, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);	

	//-- Value 3
	Image ImgValue3(m_pRSBarometer->GetPropStrValue(TEXT_POS_THIRD));
	DesRect.X += 46;
	gc.DrawImage(&ImgValue3, DesRect);

	//-- Value 4
	Image ImgValue4(m_pRSBarometer->GetPropStrValue(TEXT_POS_FOURTH));
	DesRect.X += 46;
	gc.DrawImage(&ImgValue4, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);		

	//-- Value 5
	Image ImgValue5(m_pRSBarometer->GetPropStrValue(TEXT_POS_FIFTH));
	DesRect.X += 46;
	gc.DrawImage(&ImgValue5, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA2);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelBarometerChildDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CSmartPanelBarometerChildDlg::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CSmartPanelBarometerChildDlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_BM_CNT; i ++)
		ChangeStatus(i,PT_BM_NOR);

	CPoint point;
	GetCursorPos(&point);
	ScreenToClient(&point);

	CRect rect;
	GetClientRect(&rect);

	//if(!rect.PtInRect(point)) 
		this->ShowWindow(SW_HIDE);

	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelBarometerChildDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	
	switch(nItem)
	{
	case BTN_BM_LEFT			:	m_pRSBarometer->SetPos(FALSE);				break;	
	case BTN_BM_RIGHT			:	m_pRSBarometer->SetPos(TRUE);				break;	
	default:						SetPropValueIndex(PtInRectValue(point));	break;	
	}

	//-- 2011-8-2 Lee JungTaek
	Invalidate(FALSE);

	CDialog::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CSmartPanelBarometerChildDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CSmartPanelBarometerChildDlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = PT_BM_IN_NOTHING;
	int nStart = 0;

	for(int i = nStart ; i < BTN_BM_CNT; i ++)
	{
		if(m_rectBtn[i].PtInRect(pt))
		{
			nReturn = i;
			if(bMove)
				ChangeStatus(i,PT_BM_INSIDE);
			else
				ChangeStatus(i,PT_BM_DOWN); 
		}
		else
			ChangeStatus(i,PT_BM_NOR); 
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelBarometerChildDlg::ChangeStatus(int nItem, int nStatus)
{
	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		CPaintDC dc(this);
		DrawMenu(nItem, &dc);
		InvalidateRect(m_rectBtn[nItem],FALSE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CSmartPanelBarometerChildDlg::ChangeStatusAll()
{
	int nStart = 0;
	for(int i = nStart; i < BTN_BM_CNT; i ++)
	{
		if(m_nMouseIn == i)
			ChangeStatus(i,PT_BM_INSIDE); 
		else
			ChangeStatus(i,PT_BM_NOR); 
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRectValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CSmartPanelBarometerChildDlg::PtInRectValue(CPoint point)
{
	for(int i = 0 ; i < TEXT_POS_CNT; i ++)
	{
		if(m_rectText[i].PtInRect(point))
		{
			if(i != TEXT_POS_THIRD)
			{
				ShowWindow(SW_SHOW);
				return i;
			}
		}
		else
		{
			ShowWindow(SW_HIDE);
		}
	}

	return TEXT_POS_NULL;
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropValueIndex
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CSmartPanelBarometerChildDlg::SetPropValueIndex(int nIndex)
{
	if(nIndex == TEXT_POS_NULL)		return;

	if(nIndex > TEXT_POS_THIRD)
	{
		if(m_pRSBarometer->GetPropIndexValue() + 1 == m_pRSBarometer->GetPropListCnt())
			return;

		//-- Just Change
		for(int i = TEXT_POS_THIRD; i < nIndex - 1; i++)
			m_pRSBarometer->SetPos(TRUE, FALSE);
		//-- Set Real
		m_pRSBarometer->SetPos(TRUE);
	}
	else
	{
		if(m_pRSBarometer->GetPropIndexValue() == 0)
			return;

		//-- Just Change
		for(int i = TEXT_POS_THIRD; i > nIndex + 1; i--)
			m_pRSBarometer->SetPos(FALSE, FALSE);
		//-- Set Real
		m_pRSBarometer->SetPos(FALSE);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-8-2
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
BOOL CSmartPanelBarometerChildDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_MOUSEWHEEL)
	{
		BOOL bMouseWheelUp;
		int delta = GET_WHEEL_DELTA_WPARAM(pMsg->wParam);

		if(delta < 0)		bMouseWheelUp = FALSE;
		else				bMouseWheelUp = TRUE;

		m_pRSBarometer->SetPos(bMouseWheelUp);
		Invalidate(FALSE);
		
		return TRUE;
	}		

	return CBkDialogST::PreTranslateMessage(pMsg);
}
