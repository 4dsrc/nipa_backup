/////////////////////////////////////////////////////////////////////////////
//
//  OptionDSCSet2Dlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "OptionBaseDlg.h"
#include "NXRemoteStudioFunc.h"

#define SETDIALOG_ITEM_VIEW_SIZE 6

// COptionDSCSet2Dlg dialog
class COptionDSCSet2Dlg : public COptionBaseDlg
{
	DECLARE_DYNAMIC(COptionDSCSet2Dlg)	
public:
	COptionDSCSet2Dlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionDSCSet2Dlg();

// Dialog Data
	enum { IDD = IDD_DLG_OPT_DSC_SET2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	DECLARE_MESSAGE_MAP()
	afx_msg void OnDeltaposSpinYear(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinMonth(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinDay(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinHour(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinMinute(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSelchangeCmbTimeZone();
	afx_msg void OnSelchangeCmbType();
	afx_msg void OnSelchangeCmbHourType();
	afx_msg void OnSelchangeCmbImprint();
	//CMiLRe 20141001 Date, Time 추가
	afx_msg void OnSelchangeCmbRefresh();	
	//dh0.seo 2014-09-15
	afx_msg void OnSelchangeCmbQuickView();
public:
	
private:	
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();	
	void UpdateFrames();
		
	CComboBox	m_comboBox[5];
	CSpinButtonCtrl m_spin[7];
	//CMiLRe 20141021 AM/PM 표시 및 12/24 변경 수정
	CEdit			m_edit[8];
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	CStatic * m_staticLabel[SETDIALOG_ITEM_VIEW_SIZE];
	CStatic* m_staticLabelBar[SETDIALOG_ITEM_VIEW_SIZE];
	//CMiLRe 20141001 Date, Time 추가
	CButton  m_PushRefreshBtn;

	CString m_strTimeZone;
	CString m_strDateTime;
	CString m_strType;
	int m_nHourType;
	int m_nImprint;
	int m_nDST;
	//dh0.seo 2014-09-15 Quick View
	int m_nQuickView;

	int m_nTimeZone;
	int m_nYear;
	int m_nMonth;
	int m_nDay;
	int m_nHour;
	int m_nMin;	

public:
	void InitProp(RSDSCSet2& opt);
	void SaveProp(RSDSCSet2& opt);

	void SetOptionProp(int nPropIndex, int nPropValue, RSDSCSet2& opt);
	void SetOptionProp(int nPropIndex, CString strPropValue, RSDSCSet2& opt);
	int ChangeIntValueToIndex(int nPropIndex, int nPropValue);
	int ChangeStringValueToIndex(int nPropIndex, CString strPropValue);

	void SetDateTime(CString strDateTime);
	CString GetDateTime();
	void ChangeDateTime();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};