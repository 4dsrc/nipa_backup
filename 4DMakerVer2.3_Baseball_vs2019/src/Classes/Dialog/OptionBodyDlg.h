/////////////////////////////////////////////////////////////////////////////
//
//  OptionBodyDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "RSChildDialog.h"
#include "OptionBaseDlg.h"
#include "RSOption.h"
#include "RSButton.h"
#include "NXRemoteStudioFunc.h"

enum
{
	OPT_TYPE_ENUM,
	OPT_TYPE_STR,
};
// COptionBodyDlg dialog
class COptionBodyDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(COptionBodyDlg)	
public:
	COptionBodyDlg(CRSOption * pOption, CWnd* pParent = NULL);   // standard constructor		
	virtual ~COptionBodyDlg();

// Dialog Data
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	enum { IDD = IDD_DLG_BOARD_OPT};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();	
	DECLARE_MESSAGE_MAP()
	
public:
	int m_nMainCamera;

	CRSOption*	m_pOption;

	void InitOptionProp();
	void SaveOptionProp(BOOL bTimerCapture = FALSE);
	void FileSave(CString strFilename);
	void SetActivate(int, int );

	//-- 2011-6-22 Lee JungTaek
	//-- Set Indicator
	void SetItem(int nPTPCode, CString strValue);
	void SetItem(int nPTPCode, int nPropValue);
	void SetMainCamera(int nMainCamera) {m_nMainCamera = nMainCamera;}

	//-- 2011-7-15 Lee JungTaek
	void SetDSCOptionProp(int nPTPCode, int nPropIndex);
	void SetDSCOptionProp(int nPTPCode, CString strPropValue);

	//-- 2011-7-18 Lee JungTaek
	void UpdateCapturedCnt(int nCnt);
	void UpdateRemainTime(int nTime);
	void CloseStatusDialog();

	//CMiLRe 20140902 DRIVER GET 설정
	CString GetModel(){return m_strModel;}
	void SetModel(CString strModel) {m_strModel = strModel;}
	void SetMode(int nMode);
	//CMiLRe 20141124 인터벌 캡쳐 디바이스에서 종료시 스타트 버튼 활성화
	COptionBaseDlg* GetDlgTimeLap();
	COptionBaseDlg* StartInterval();
private:	
	COptionBaseDlg* m_pOptDlg[OPT_TAB_CNT];
	//-- 2011-05-18 hongsu.jung
	void UpdateFrames();
	//-- 2011-05-30 hongsu.jung
	void CreateFrames();
	//CMiLRe 20140902 DRIVER GET 설정
	CString m_strModel;
	//CMiLRe 20140922 Picture Interval Capture 추가
	BOOL m_bChangedDP;
	int m_nMode;
};