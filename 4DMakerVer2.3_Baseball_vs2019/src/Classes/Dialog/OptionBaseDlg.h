/////////////////////////////////////////////////////////////////////////////
//
//  OptionBaseDlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "SRSIndex.h"
#include "DSCOptionIndex.h"
#include "RSChildDialog.h"
#include "NXRemoteStudioFunc.h"

// COptionBaseDlg dialog
class COptionBaseDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(COptionBaseDlg)	
public:
	COptionBaseDlg(CWnd* pParent = NULL);   // standard constructor
	COptionBaseDlg(UINT uResourceID, CWnd* pParent = NULL);
	virtual ~COptionBaseDlg();

// Dialog Data
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	enum { IDD = IDD_DLG_BOARD_OPT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();		
	afx_msg void OnPaint();		
	DECLARE_MESSAGE_MAP()

private:	
	virtual void UpdateFrames() {};
	virtual void CreateFrames()	{};

protected:
	void MouseEvent();

public:
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	COLORREF GetBKColor() {return RGB(0x50,0x50,0x50);}	
	COLORREF GetTextColor() {return RGB(0x0,0x0,0x0);}	
};