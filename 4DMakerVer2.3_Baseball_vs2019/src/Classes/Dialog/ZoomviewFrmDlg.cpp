/////////////////////////////////////////////////////////////////////////////
//
//  ZoomviewFrmDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ZoomviewFrmDlg.h"
#include "SRSIndex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CZoomviewFrmDlg dialog
IMPLEMENT_DYNAMIC(CZoomviewFrmDlg,  CRSChildDialog)

CZoomviewFrmDlg::CZoomviewFrmDlg(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CZoomviewFrmDlg::IDD, pParent)
{
	m_pZoomView	= NULL;		
	m_nRatio			= 0;
	//SetSrcSize(CSize(NX2000_SRC_WIDTH,NX2000_SRC_HEIGHT));
	SetSrcSize(CSize(NX1_SRC_WIDTH,NX1_SRC_HEIGHT));
}

CZoomviewFrmDlg::~CZoomviewFrmDlg()
{
	if(m_pZoomView)
	{
		delete m_pZoomView;
		m_pZoomView = NULL;
	}
}

void CZoomviewFrmDlg::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);	
}

BEGIN_MESSAGE_MAP(CZoomviewFrmDlg, CRSChildDialog)	
	ON_WM_SIZE()	
	ON_WM_PAINT()		
END_MESSAGE_MAP()

void CZoomviewFrmDlg::OnPaint()
{	
	//-- DrawZoomview
	DrawZoomview();
	CRSChildDialog::OnPaint();
}

// CZoomviewFrmDlg message handlers
BOOL CZoomviewFrmDlg::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();	
	
	m_pZoomView = new CZoomview();
	m_pZoomView->Create(NULL,_T(""),WS_VISIBLE|WS_CHILD,CRect(0,0,0,0),this,IDC_LIVEVIEWFRAME);	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CZoomviewFrmDlg::OnSize(UINT nType, int cx, int cy) 
{
    CDialog::OnSize(nType, cx, cy);
	//-- 2011-05-30 Draw Liveview
	DrawZoomview();
} 

//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CZoomviewFrmDlg::DrawZoomview()
{
	if(!m_pZoomView)
		return;

	CPaintDC dc(this);

	CRect rect;
	GetClientRect(&rect);
	int nImgX, nImgY, nImgW, nImgH;
	
	//-- Draw Background	
	//-- 1. Boader
	nImgX		= rect.left;	
	nImgY		= rect.top;
	nImgH	= rect.Height();			
	nImgW	= rect.Width();
	DrawImage(&dc, 
					RSGetRoot(_T("img\\03.Live View\\liveview_bk2.png"))
					,nImgX, nImgY, nImgW, nImgH);	

	//-- 2. Liveview
	GetZoomviewRect(&rect);
	m_pZoomView->MoveWindow(rect);
	m_pZoomView->Invalidate(FALSE);

	//-- 3. Set Ratio
	SetRatio(&rect);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
static const int size_margin	= 5;

void CZoomviewFrmDlg::GetZoomviewRect(LPRECT lpRect)
{
	CSize srcSize = GetSrcSize();
	int width	= srcSize .cx;
	int height	= srcSize .cy;	
	if(width == 0 || height == 0)
		return;

	//-- 2011-05-19
	//-- Move To Center
	int nW, nH;
	nW	= lpRect->right		- lpRect->left;
	nH		= lpRect->bottom	- lpRect->top;
	//-- guarantee Ratio
	int nRatioW, nRatioH;
	nRatioW = (int)(((float)width/(float)height)*(float)nH);
	nRatioH = (int)(((float)height/(float)width)*(float)nW);

	if( nW > nRatioW )		
	{
		lpRect->top		= size_margin;
		lpRect->left		= nW/2 - nRatioW/2;			
		lpRect->right		= nW/2 + nRatioW/2;
		lpRect->bottom = nH - size_margin;
	}
	else
	{	
		lpRect->left		= size_margin;
		lpRect->right		= nW - size_margin;
		lpRect->top		= nH/2 - nRatioH/2;
		lpRect->bottom	= nH/2 + nRatioH/2;
	}
}

void CZoomviewFrmDlg::SetRatio(LPRECT lpRect)
{
	float nSrcWidth;
	float nRatio;
	nSrcWidth	= (float) GetSrcSize().cx;
	if(nSrcWidth)
	{
		nRatio	= ( lpRect->right - lpRect->left ) / nSrcWidth * 100;
		m_nRatio = (int)nRatio;		
	}
	else
		m_nRatio = 0;	
}

void CZoomviewFrmDlg::RotateSrc(BOOL bRotate)
{ 
	int nTemp = m_size.cx; 
	if(bRotate && (m_size.cx > m_size.cy))
	{
		m_size.cx = m_size.cy; 
		m_size.cy = nTemp; 
	}
	else if(!bRotate && (m_size.cx < m_size.cy))		
	{
		m_size.cx = m_size.cy; 
		m_size.cy = nTemp; 
	}
}