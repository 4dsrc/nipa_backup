/////////////////////////////////////////////////////////////////////////////
//
//  OptionDSCSet1Dlg.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OptionDSCSet1Dlg.h"
#include "OptionBodyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// COptionDSCSet1Dlg dialog
IMPLEMENT_DYNAMIC(COptionDSCSet1Dlg, COptionBaseDlg)

//------------------------------------------------------------------------------ 
//! @brief		COptionDSCSet1Dlg(CString strPath)
//! @date		2010-05-23
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
COptionDSCSet1Dlg::COptionDSCSet1Dlg(CWnd* pParent /*=NULL*/)
	: COptionBaseDlg(COptionDSCSet1Dlg::IDD, pParent)
{
	m_nFileName		= 0;
	m_nFileNumber	= 0;
	m_nFolderType	= 0;
	m_nReset		= 0;

	m_nStatus[0] = BTN_NORMAL;
	m_nStatus[1] = BTN_NORMAL;

	m_pPopupMsg = NULL;
}

COptionDSCSet1Dlg::~COptionDSCSet1Dlg() 
{
	if(m_pPopupMsg)
	{
		delete m_pPopupMsg;
		m_pPopupMsg = NULL;
	}

	//CMiLRe 20141113 메모리 릭 제거
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		if(m_staticLabel[i])
		{
			delete m_staticLabel[i];
			m_staticLabel[i] = NULL;
		}
		
		if(m_staticLabelBar[i])
		{
			delete m_staticLabelBar[i];
			m_staticLabelBar[i] = NULL;
		}
	}
}

void COptionDSCSet1Dlg::DoDataExchange(CDataExchange* pDX)
{
	COptionBaseDlg::DoDataExchange(pDX);		
	DDX_Control(pDX, IDC_CMB_1, m_comboBox[0]);
	DDX_Control(pDX, IDC_CMB_2, m_comboBox[1]);
	DDX_Control(pDX, IDC_CMB_3, m_comboBox[2]);
	DDX_Control(pDX, IDC_CMB_4, m_comboBox[3]);
	DDX_Control(pDX, IDC_CMB_5, m_comboBox[4]);
	DDX_CBIndex(pDX, IDC_CMB_1, m_nFileName	);
	DDX_CBIndex(pDX, IDC_CMB_2, m_nFileNumber);
	DDX_CBIndex(pDX, IDC_CMB_3, m_nFolderType);
	DDX_CBIndex(pDX, IDC_CMB_5, m_nReset);
	//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
	DDX_Control(pDX, IDC_BUTTON1, m_PushBtn[0]);
	DDX_Control(pDX, IDC_BUTTON2, m_PushBtn[1]);

}				

BEGIN_MESSAGE_MAP(COptionDSCSet1Dlg, COptionBaseDlg)	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()	
	ON_WM_MOUSEMOVE()
	ON_CBN_SELCHANGE(IDC_CMB_1, OnSelchangeCmbFileNmae		)
	ON_CBN_SELCHANGE(IDC_CMB_2, OnSelchangeCmbFileNumber	)
	ON_CBN_SELCHANGE(IDC_CMB_3, OnSelchangeCmbFolderType	)
	ON_CBN_SELCHANGE(IDC_CMB_5, OnSelchangeCmbReset	)
	//dh0.seo 2014-09-12
	ON_CBN_SELCHANGE(IDC_CMB_4, OnSelchangeCmbPowerSave		)
	//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
	ON_BN_CLICKED(IDC_BUTTON1, OnSelFormat)
	ON_BN_CLICKED(IDC_BUTTON2, OnSelReset)
END_MESSAGE_MAP()

// CLiveview message handlers
//------------------------------------------------------------------------------ 
//! @brief		CreateFrame
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionDSCSet1Dlg::CreateFrames()
{
	m_comboBox[0].MoveWindow(160, 28*0 +	5,	150,22);
	m_comboBox[1].MoveWindow(160, 28*1 +	5,	150,22);
	m_comboBox[2].MoveWindow(160, 28*2 +	5,	150,22);
	m_comboBox[4].MoveWindow(160, 28*4 +	5,	150,22);
	//dh0.seo 2014-09-12
//	m_comboBox[3].MoveWindow(160, 28*5 +	5,	150,22);
	
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabel[i] = new CStatic();
		m_staticLabelBar[i] = new CStatic();
	}
	m_staticLabel[0]->Create(_T("File Name"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*0+10, 150, 31*1), this, IDC_STATIC_ETC_1);		
	m_staticLabel[1]->Create(_T("File Number"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*1+10, 150, 31*2), this, IDC_STATIC_ETC_2);		
	m_staticLabel[2]->Create(_T("Folder Type"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*2+10, 150, 31*3), this, IDC_STATIC_ETC_3);		
	m_staticLabel[3]->Create(_T("Format"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*3+10, 150, 31*4), this, IDC_STATIC_ETC_4);		
	m_staticLabel[4]->Create(_T("Reset"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*4+10, 150, 31*5), this, IDC_STATIC_ETC_5);		
//	m_staticLabel[5]->Create(_T("Power Save"),  WS_CHILD|WS_VISIBLE|SS_LEFT,  CRect(15,28*5+10, 150, 31*6), this, IDC_STATIC_ETC_6);		
	
	for(int i = 0 ; i < SETDIALOG_ITEM_VIEW_SIZE ; i++)
	{
		m_staticLabelBar[i]->Create(_T(""),  WS_CHILD|WS_VISIBLE|SS_BLACKRECT | SS_LEFT,  CRect(1,30+28*i, OPTION_DLG_WIDTH, 31+28*i), this );
	}

	//-- File Name
	m_comboBox[0].AddString(_T("Standard"));
	m_comboBox[0].AddString(_T("Date"));
	//-- File Number
	m_comboBox[1].AddString(_T("Reset"));
	m_comboBox[1].AddString(_T("Series"));
	//-- Folder Type
	m_comboBox[2].AddString(_T("Standard"));
	m_comboBox[2].AddString(_T("Date"));
	//dh0.seo 2014-09-12 Power Save
/*	m_comboBox[3].AddString(_T("30 sec"));
	m_comboBox[3].AddString(_T("1 min"));
	m_comboBox[3].AddString(_T("3 min"));
	m_comboBox[3].AddString(_T("5 min"));
	m_comboBox[3].AddString(_T("10 min"));
	m_comboBox[3].AddString(_T("30 min")); */


	m_comboBox[4].AddString(_T("Reset Selcet"));
	m_comboBox[4].AddString(_T("Camera Menu"));
	m_comboBox[4].AddString(_T("Key Mapping"));
	m_comboBox[4].AddString(_T("C1/C2 Mode"));
	m_comboBox[4].AddString(_T("Bluetooth/Wi-Fi"));
	m_comboBox[4].AddString(_T("All"));

	//-- Format / Reset
	m_rectBtn[0] = CRect(160, 28*3 +	5,	150,22);	
//	m_rectBtn[1] = CRect(160, 28*4 +	5,	150,22);

	//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
	m_PushBtn[0].MoveWindow(160, 28*3 +	5,	150,22);
	m_PushBtn[0].SetWindowText(_T("Start"));

//	m_PushBtn[1].MoveWindow(160, 28*4 +	5,	150,22);
//	m_PushBtn[1].SetWindowText(_T("Start"));

	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	this->SetBackgroundColor(GetBKColor());
}
//------------------------------------------------------------------------------ 
//! @brief		UpdateFrames
//! @date		2010-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionDSCSet1Dlg::UpdateFrames()
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	COLORREF color;
		
	CString strPath = RSGetRoot();
	CString strFullPath;
	
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_line.png"),strPath);
	//-- 2011-07-14 hongsu.jung
	color = GetTextColor();
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("File Name"	),15, 28*0 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*1, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("File Number"	),15, 28*1 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*2, rect.Width(),1);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Folder Type"	),15, 28*2 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*3, rect.Width(),1);
	//dh0.seo 2014-09-12
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Power Save"	),15, 28*5 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*6, rect.Width(),1);
	
	//-- Draw Background
	switch(m_nStatus[0])
	{
	case BTN_DOWN:
		color = RGB(0,0,0);
	case BTN_INSIDE:
		strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_focus bar.png"),strPath);
		DrawGraphicsImage(&dc, strFullPath, m_rectBtn[0]);
		break;
	}
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_line.png"),strPath);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Format"		),15, 28*3 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*4, rect.Width(),1);
	
	//-- Draw Background
	switch(m_nStatus[1])
	{
	case BTN_DOWN:
		color = RGB(0,0,0);
	case BTN_INSIDE:	
		strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_focus bar.png"),strPath);
		DrawGraphicsImage(&dc, strFullPath, m_rectBtn[1]);
		break;
	}
	strFullPath.Format(_T("%s\\img\\04.Option Popup\\04_line.png"),strPath);
	DrawStr(&dc, STR_SIZE_MIDDLE, _T("Reset"			),15, 28*4 +10,color);		DrawGraphicsImage(&dc, strFullPath, 1, 28*5, rect.Width(),1);
	Invalidate(FALSE);	
}

void COptionDSCSet1Dlg::SetOptionProp(int nPropIndex, int nPropValue, RSDSCSet1& opt)
{
	int nEnum = 0;

	nEnum = ChangeIntValueToIndex(nPropIndex, nPropValue);
	
	switch(nPropIndex)
	{
	case DSC_OPT_DSC1_FILENAME		:	m_nFileName		= nEnum;	break;	
	case DSC_OPT_DSC1_FILENUMBER	:	m_nFileNumber	= nEnum;	break;	
	case DSC_OPT_DSC1_FOLDERTYPE	:	m_nFolderType	= nEnum;	break;
	//dh0.seo 2014-09-12
	case DSC_OPT_DSC1_POWERSAVE		:	m_nPowerSave	= nEnum;	break;
	}

	if(((COptionBodyDlg*)GetParent())->GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
		m_comboBox[3].EnableWindow(FALSE);
	else
		m_comboBox[3].EnableWindow(TRUE);

	UpdateData(FALSE);
}

int COptionDSCSet1Dlg::ChangeIntValueToIndex(int nPropIndex, int nPropValue)
{
	int nEnum = 0;

	switch(nPropIndex)
	{
	case DSC_OPT_DSC1_FILENAME	:
		switch(nPropValue)
		{
		case eUCS_MENU_FILE_NAME_STANDARD	:		nEnum = DSC_MENU_FILENAME_OFF	;	break;
		case eUCS_MENU_FILE_NAME_DATE		:		nEnum =	DSC_MENU_FILENAME_ON	;	break;
		}
		break;
	case DSC_OPT_DSC1_FILENUMBER:									
		switch(nPropValue)
		{
		case eUCS_MENU_FILE_NO_RESET	:	nEnum = DSC_MENU_FILENUMBER_RESET;	break;
		case eUCS_MENU_FILE_NO_SERIES	:	nEnum = DSC_MENU_FILENUMBER_SERIES;	break;
		}
		break;
	case DSC_OPT_DSC1_FOLDERTYPE:
		switch(nPropValue)
		{
		case eUCS_MENU_FOLDER_TYPE_STANDARD	:	nEnum = DSC_MENU_FOLDERTYPE_STANDARD;	break;
		case eUCS_MENU_FOLDER_TYPE_DATE		:	nEnum = DSC_MENU_FOLDERTYPE_DATE;	break;
		}
		break;
	//dh0.seo 2014-09-12
	case DSC_OPT_DSC1_POWERSAVE:
		switch(nPropValue)
		{
		case eUCS_MENU_POWER_SAVE_30SEC		:	nEnum = DSC_MENU_POWERSAVE_30SEC;	break;
		case eUCS_MENU_POWER_SAVE_1MIN		:	nEnum = DSC_MENU_POWERSAVE_1MIN;	break;
		case eUCS_MENU_POWER_SAVE_3MIN		:	nEnum = DSC_MENU_POWERSAVE_3MIN;	break;
		case eUCS_MENU_POWER_SAVE_5MIN		:	nEnum = DSC_MENU_POWERSAVE_5MIN;	break;
		case eUCS_MENU_POWER_SAVE_10MIN		:	nEnum = DSC_MENU_POWERSAVE_10MIN;	break;
		case eUCS_MENU_POWER_SAVE_30MIN		:	nEnum = DSC_MENU_POWERSAVE_30MIN;	break;
		}
		break;
	}

	return nEnum;
}

void COptionDSCSet1Dlg::SaveProp(RSDSCSet1& opt)
{
	if(!UpdateData(TRUE))
		return;

	opt.nFileName	= m_nFileName		;
	opt.nFileNumber	= m_nFileNumber		;
	opt.nFolderType	= m_nFolderType		;
	//dh0.seo 2014-09-12
	opt.nPowerSave	= m_nPowerSave		;
}

//------------------------------------------------------------------------------ 
//! @brief		LoadPopupMsg
//! @date		2011-8-11
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void COptionDSCSet1Dlg::LoadPopupMsg(int nType, CString strMsg, int nDelay, RSEvent* pMsg)
{
	//-- 2011-8-10 Lee JungTaek
	//-- Add Popup Message
	if(m_pPopupMsg)
	{
		delete m_pPopupMsg;
		m_pPopupMsg = NULL;
	}

	m_pPopupMsg = new CPopupMsgDlg(nType, strMsg, nDelay);
	m_pPopupMsg->Create(CPopupMsgDlg::IDD, this);
	m_pPopupMsg->SetCommandMsg(pMsg);
 
 	CRect rect;
 	this->GetWindowRect(&rect);
 
	Image Img(RSGetRoot(_T("img\\02.Smart Panel\\02_popup_bg02.png")));

	int nX, nY, nWidth, nHeight;

	nWidth = Img.GetWidth();
	nHeight = Img.GetHeight();
	nX = (rect.left + rect.right - nWidth) / 2 - 3;
	nY = (rect.top + rect.bottom - nWidth) / 2 + 0;

	m_pPopupMsg->MoveWindow(nX, nY, Img.GetWidth() + 6, Img.GetHeight() + 8);
	m_pPopupMsg->ShowWindow(SW_SHOW);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int COptionDSCSet1Dlg::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = -1;
	for(int i = 0; i < 2; i ++)
	{
		if(m_rectBtn[i].PtInRect(pt))
		{
			nReturn = i;
			break;
		}
	}
	return nReturn;
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionDSCSet1Dlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	switch(nItem)
	{
		case 0:		//-- Format
			{
				m_nStatus[0] = BTN_DOWN;	
				InvalidateRect(m_rectBtn[0]);	

				//-- 2011-8-11 Lee JungTaek
				//-- Popup Dialog
				RSEvent* pMsg		= new RSEvent;
				pMsg->message	= WM_RS_OPT_FORMAT_DEVICE;
				LoadPopupMsg(POPUP_TYPE_WITH_2BTN, _T("Format ?"), POPUP_DELAY_NONE, pMsg);
			}
			break;
		case 1:		//-- Reset
			{
				m_nStatus[1] = BTN_DOWN;	
				InvalidateRect(m_rectBtn[1]);	

				//-- 2011-8-11 Lee JungTaek
				//-- Popup Dialog
				RSEvent* pMsg		= new RSEvent;
				pMsg->message	= WM_RS_OPT_RESET_DEVICE;
				LoadPopupMsg(POPUP_TYPE_WITH_2BTN, _T("Reset ?"), POPUP_DELAY_NONE, pMsg);
			}
			break;		
	}
	CDialog::OnLButtonDown(nFlags, point);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionDSCSet1Dlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	switch(nItem)
	{
		case 0:	if(m_nStatus[0] != BTN_INSIDE) { m_nStatus[0] = BTN_INSIDE;	InvalidateRect(m_rectBtn[0]);} break;
		case 1:	if(m_nStatus[1] != BTN_INSIDE) { m_nStatus[1] = BTN_INSIDE;	InvalidateRect(m_rectBtn[1]);}	break;
		default:	ResetButton();
	}
	CDialog::OnLButtonUp(nFlags, point);
}
//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void COptionDSCSet1Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);	
	switch(nItem)
	{
		case 0:	
			if(m_nStatus[0] != BTN_INSIDE) 
			{ 
				m_nStatus[0] = BTN_INSIDE;	
				InvalidateRect(m_rectBtn[0]);
				//-- Check
				if(m_nStatus[1] == BTN_INSIDE)
				{
					m_nStatus[1] = BTN_NORMAL; 
					InvalidateRect(m_rectBtn[1]);
				}
			}
			break;
		case 1:	
			if(m_nStatus[1] != BTN_INSIDE) 
			{ 
				m_nStatus[1] = BTN_INSIDE;	
				InvalidateRect(m_rectBtn[1]);
				//-- Check
				if(m_nStatus[0] == BTN_INSIDE)
				{
					m_nStatus[0] = BTN_NORMAL; 
					InvalidateRect(m_rectBtn[0]);
				}
			}
			break;
		default:	ResetButton();
	}
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseLeave
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT COptionDSCSet1Dlg::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	ResetButton();
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		ResetButton
//! @date		2011-07-14
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void COptionDSCSet1Dlg::ResetButton()
{
	if(m_nStatus[0] != BTN_NORMAL)
	{
		m_nStatus[0] = BTN_NORMAL;	
		InvalidateRect(m_rectBtn[0]);
	}

	if(m_nStatus[1] == BTN_DISABLE)
		return;
	if(m_nStatus[1] != BTN_NORMAL)
	{
		m_nStatus[1] = BTN_NORMAL;	
		InvalidateRect(m_rectBtn[1]);
	}
}
	
//------------------------------------------------------------------------------ 
//! @brief		OnSelchangeCmb
//! @date		2011-7-15
//! @author		Lee JungTaek
//! @note	 	Change Value
//------------------------------------------------------------------------------ 
void COptionDSCSet1Dlg::OnSelchangeCmbFileNmae() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_FILENAME, m_nFileName);
}
void COptionDSCSet1Dlg::OnSelchangeCmbFileNumber() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_FILENUMBER, m_nFileNumber);
}
void COptionDSCSet1Dlg::OnSelchangeCmbFolderType() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_FOLDERTYPE, m_nFolderType);
}
//dh0.seo 2014-09-12
void COptionDSCSet1Dlg::OnSelchangeCmbPowerSave() 
{
	UpdateData();
	((COptionBodyDlg*)GetParent())->SetDSCOptionProp(PTP_CODE_SAMSUNG_POWERSAVE, m_nPowerSave);
}

void COptionDSCSet1Dlg::OnSelchangeCmbReset()
{
	if(m_comboBox[4].GetCurSel() > 0)
	{
		UpdateData();
		RSEvent* pMsg		= new RSEvent;
		pMsg->message	= WM_RS_OPT_RESET_DEVICE;
		pMsg->nParam1	= m_comboBox[4].GetCurSel();
		LoadPopupMsg(POPUP_TYPE_WITH_2BTN, _T("Reset ?"), POPUP_DELAY_NONE, pMsg);
	}
}

//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
void COptionDSCSet1Dlg::OnSelFormat() 
{
	UpdateData();
	RSEvent* pMsg		= new RSEvent;
	pMsg->message	= WM_RS_OPT_FORMAT_DEVICE;
	LoadPopupMsg(POPUP_TYPE_WITH_2BTN, _T("Format ?"), POPUP_DELAY_NONE, pMsg);
}

//CMiLRe 20141114 Format, Rest, Sensor Cleaning 버튼으로 변경
void COptionDSCSet1Dlg::OnSelReset() 
{
	UpdateData();
	RSEvent* pMsg		= new RSEvent;
	pMsg->message	= WM_RS_OPT_RESET_DEVICE;
	LoadPopupMsg(POPUP_TYPE_WITH_2BTN, _T("Reset ?"), POPUP_DELAY_NONE, pMsg);
}
	