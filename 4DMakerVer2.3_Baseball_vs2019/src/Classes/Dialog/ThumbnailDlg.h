/////////////////////////////////////////////////////////////////////////////
//
//  ThumbNailDlg.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include <vector>
#include "PictureViewerDlg.h"
#include "ToolTipListCtrl.h"
//-- 2011-07-27 hongsu.jung
#include "ThumbNailList.h"
#include "NXRemoteStudioFunc.h"

// CThumbnailLoader
class CThumbnailLoader : public CWinThread
{
private:
	BOOL			m_bThreadFlag;
	BOOL			m_bLoadThumbnail;
	CThumbNailList *m_pThumbnailList;

public:
	CThumbnailLoader(void);
	~CThumbnailLoader(void);

	virtual BOOL InitInstance(){ return TRUE; }
	virtual int ExitInstance() { return CWinThread::ExitInstance(); }
	virtual int Run(void);
	void StopThread();

	void SetThumbnailList(CThumbNailList *pThumbnailList){ m_pThumbnailList = pThumbnailList; }
	//CMiLRe 20141124 섬네일 연사일 경우 여러자 못가지고 오는 버그 수정
	void LoadThumbnail(CString strFilename = _T("")){ m_bLoadThumbnail = TRUE; }
};

// CThumbNailDlg dialog

class CThumbnailDlg : public CRSChildDialog
{
	DECLARE_DYNAMIC(CThumbnailDlg)
public:
	CThumbnailDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CThumbnailDlg();
// Dialog Data
	enum { IDD = IDD_DLG_THUMBNAIL };
	enum {
		PRESSED_NOTHING = -1,
		PRESSED_LEFT,
		PRESSED_RIGHT,
	};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);		
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnReload(WPARAM , LPARAM );
	DECLARE_MESSAGE_MAP()	
	void MouseEvent();

private:	
	CRect			m_rect[3];
	int				m_nStatus[3];
	int				m_nPressed;
	//--2011-09-01 jeansu
	CThumbnailLoader *m_pThumbnailLoader;

private:
	void UpdateFrames();
	void CheckDisable();
	void CheckButton(int nIndex, int nStatus);
	void MoveThumbnail(BOOL bRight);
	//dh0.seo 2014-11-11
	int m_nIndex;
	//CMiLRe 20141124 섬네일 로드 쓰레드로 변경
	HANDLE m_hThread;
	CString m_strCameraID;
public:
	void LoadThumbnail(CString strFileName = _T(""));
	//dh0.seo 2014-11-11
	int GetCameraNum(int nIndex){m_nIndex = nIndex; return m_nIndex;}
	//CMiLRe 20141118 섬네일 프로그래스바 움직일때 버벅거리는 버그 수정(리스트에 이미지 새로 불러오는 것에서 파일 갯수로 비교)
	int GetThumnailFileCount(CString strFilePath = _T(""));
	int m_nThumnailCnt;
	//CMiLRe 20141124 섬네일 로드 쓰레드로 변경
	static DWORD WINAPI DrawRunner(IN void* param);
	int Run(void);
	BOOL m_bLoadThumbnail;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CThumbNailList* m_pDlgThumbnailList;
};