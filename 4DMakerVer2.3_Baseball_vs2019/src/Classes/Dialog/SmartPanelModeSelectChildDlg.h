/////////////////////////////////////////////////////////////////////////////
//
//  CSmartPanelModeSelectChildDlg.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-09-15
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "BKDialogST.h"
#include "RSModeSelectCtrl.h"
#include "NXRemoteStudioFunc.h"

enum
{
	BTN_MS_NULL = -1,
	BTN_MS_LEFT,
	BTN_MS_RIGHT,	
	BTN_MS_CNT	,
};

enum
{
	PT_MS_IN_NOTHING = -1,
	PT_MS_NOR,
	PT_MS_INSIDE,
	PT_MS_DOWN,
	PT_MS_DIS,
};

class CSmartPanelModeSelectChildDlg : public CBkDialogST
{
	DECLARE_DYNAMIC(CSmartPanelModeSelectChildDlg)

public:
	CSmartPanelModeSelectChildDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSmartPanelModeSelectChildDlg();

// Dialog Data
	enum { IDD = IDD_DLG_SMARTPANEL_BAROMETER_DETAIL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	//-- Item Info
	int	m_nMouseIn;
	int	m_nStatus[BTN_MS_CNT];
	CRect m_rectBtn[BTN_MS_CNT];
	CRect m_rectValeu[MODE_POS_CNT];
	CRect m_rectValue;

	//-- Draw Image
	void DrawBackground();	
	void DrawMenu(int nItem, CDC* pDC);

	//-- Mouse Function
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();

	//-- Draw Value
	void DrawDlgValue(CDC* pDC);
	void DrawCtrlValue(CDC* pDC);
	
	//-- Click
	int PtInRectValue(CPoint point);
	void SetPropValueIndex(int nIndex);

private:
	CString m_strModeSelectBKPath;

	//-- 2011-9-15 Lee JungTaek
	CRSModeSelectCtrl* m_pRSModeSelect;

public:
	//-- 2011-9-15 Lee JungTaek
	void SetCtrolInfo(CRSModeSelectCtrl* pRSModeSelect)	{m_pRSModeSelect = pRSModeSelect;}
};
