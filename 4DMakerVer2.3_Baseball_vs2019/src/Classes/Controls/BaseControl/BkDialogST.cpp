//
//	Class:		CBkDialogST
//
//	Author:		Davide Calabro'		davide_calabro@yahoo.com
//									http://www.softechsoftware.it
//
//	Disclaimer
//	----------
//	THIS SOFTWARE AND THE ACCOMPANYING FILES ARE DISTRIBUTED "AS IS" AND WITHOUT
//	ANY WARRANTIES WHETHER EXPRESSED OR IMPLIED. NO REPONSIBILITIES FOR POSSIBLE
//	DAMAGES OR EVEN FUNCTIONALITY CAN BE TAKEN. THE USER MUST ASSUME THE ENTIRE
//	RISK OF USING THIS SOFTWARE.
//
//	Terms of use
//	------------
//	THIS SOFTWARE IS FREE FOR PERSONAL USE OR FREEWARE APPLICATIONS.
//	IF YOU USE THIS SOFTWARE IN COMMERCIAL OR SHAREWARE APPLICATIONS YOU
//	ARE GENTLY ASKED TO DONATE 1$ (ONE U.S. DOLLAR) TO THE AUTHOR:
//
//		Davide Calabro'
//		P.O. Box 65
//		21019 Somma Lombardo (VA)
//		Italy
//
// @modified	JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//

#include "stdafx.h"
#include "BkDialogST.h"
//#include "cv.h"
//#include "highgui.h"
//#include "imgdecoder.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifndef WM_NCLBUTTONDOWN
#define WM_NCLBUTTONDOWN                0x00A1
#define	BKDLGST_DEFINES
#endif

#define    SC_SZLEFT           (0xF001)              // resize from left 
#define    SC_SZRIGHT          (0xF002)              // resize from right 
#define    SC_SZTOP            (0xF003)              // resize from top 
#define    SC_SZTOPLEFT        (0xF004)              // resize from top left 
#define    SC_SZTOPRIGHT       (0xF005)              // resize from top right 
#define    SC_SZBOTTOM         (0xF006)              // resize from bottom 
#define    SC_SZBOTTOMLEFT     (0xF007)              // resize from bottom left 
#define    SC_SZBOTTOMRIGHT    (0xF008)              // resize from bottom right 
#define    SC_DRAGMOVE         (0xF012)              // move by drag 

#define    RESIZE_FRAME_WIDTH        10
#define    RESIZE_FRAME_HEIGHT		 10

CBkDialogST::CBkDialogST(CWnd* pParent /*=NULL*/)
{
	//{{AFX_DATA_INIT(CBkDialogST)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_bSmartStyle = FALSE;
	Init();
}

CBkDialogST::CBkDialogST(UINT uResourceID, CWnd* pParent)
	: CRSChildDialog(uResourceID, pParent)
{
	Init();
}


CBkDialogST::CBkDialogST(LPCTSTR pszResourceID, CWnd* pParent)
	: CRSChildDialog(pszResourceID, pParent)
{
	Init();
}

CBkDialogST::~CBkDialogST()
{
	FreeResources();
}

void CBkDialogST::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBkDialogST)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CBkDialogST, CRSChildDialog)
	//{{AFX_MSG_MAP(CBkDialogST)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()	
	//}}AFX_MSG_MAP
	//ON_WM_NCHITTEST()
	ON_WM_LBUTTONDOWN()	
	ON_WM_SETCURSOR()		
	ON_WM_WINDOWPOSCHANGED()
	ON_WM_WINDOWPOSCHANGING()	
END_MESSAGE_MAP()

void CBkDialogST::Init()
{
	FreeResources(FALSE);

	// Default drawing bitmap mode
	m_byMode = BKDLGST_MODE_TILE;

	// No EasyMove mode
	m_bEasyMoveMode = FALSE;

	//-- 2011-05-16 hongsu.jung	
	m_rect = CRect(0,0,0,0);
	SetDockType(MAIN_NULL);

	//-- 2011-5-20 Lee JungTaek
	m_bMagenticWindow = TRUE;

} // End of Init

void CBkDialogST::FreeResources(BOOL bCheckForNULL)
{
	if (bCheckForNULL == TRUE)
	{
		// Destroy bitmap
		if (m_hBitmap)	::DeleteObject(m_hBitmap);
#ifndef UNDER_CE
		// Destroy region
		if (m_hRegion)
		{
			::SetWindowRgn(m_hWnd, NULL, FALSE);
			::DeleteObject(m_hRegion);
		} // if
#endif
	} // if

	m_hBitmap = NULL;
#ifndef UNDER_CE
	m_hRegion = NULL;
#endif
	m_dwWidth = 0;
	m_dwHeight = 0;
} // End of FreeResources

void CBkDialogST::OnSize(UINT nType, int cx, int cy) 
{
	CRSChildDialog::OnSize(nType, cx, cy);
	
	// If there is a bitmap loaded
	if (m_hBitmap != NULL)
	{
		Invalidate();
	} // if
} // End of OnSize

BOOL CBkDialogST::OnEraseBkgnd(CDC* pDC) 
{
	CRect		rWnd;
	int			nX			= 0;
	int			nY			= 0;

	BOOL	bRetValue;
	if(!m_bSmartStyle)
		bRetValue = CRSChildDialog::OnEraseBkgnd(pDC);
	else
		bRetValue = CDialogEx::OnEraseBkgnd(pDC);

	// If there is a bitmap loaded
	if (m_hBitmap)
	{
		GetClientRect(rWnd);

		CDC			dcMemoryDC;
		CBitmap		bmpMemoryBitmap;
		CBitmap*	pbmpOldMemoryBitmap = NULL;

		dcMemoryDC.CreateCompatibleDC(pDC);
		bmpMemoryBitmap.CreateCompatibleBitmap(pDC, rWnd.Width(), rWnd.Height());
		pbmpOldMemoryBitmap = (CBitmap*)dcMemoryDC.SelectObject(&bmpMemoryBitmap);

		// Fill background 
		dcMemoryDC.FillSolidRect(rWnd, pDC->GetBkColor());

		CDC			dcTempDC;
		HBITMAP		hbmpOldTempBitmap = NULL;

		dcTempDC.CreateCompatibleDC(pDC);
		hbmpOldTempBitmap = (HBITMAP)::SelectObject(dcTempDC.m_hDC, m_hBitmap);

		switch (m_byMode)
		{
			case BKDLGST_MODE_TILE:
				// Tile the bitmap
				while (nY < rWnd.Height()) 
				{
					while(nX < rWnd.Width()) 
					{
						dcMemoryDC.BitBlt(nX, nY, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
						nX += m_dwWidth;
					} // while
					nX = 0;
					nY += m_dwHeight;
				} // while
				break;
			case BKDLGST_MODE_CENTER:
				nX = ((rWnd.Width() - m_dwWidth)/2);
				nY = ((rWnd.Height() - m_dwHeight)/2);
				dcMemoryDC.BitBlt(nX, nY, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
				break;
			case BKDLGST_MODE_STRETCH:
				// Stretch the bitmap
				dcMemoryDC.StretchBlt(0, 0, rWnd.Width(), rWnd.Height(), &dcTempDC, 0, 0, m_dwWidth, m_dwHeight, SRCCOPY);
				break;
			case BKDLGST_MODE_TILETOP:
				while(nX < rWnd.Width()) 
				{
					dcMemoryDC.BitBlt(nX, 0, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
					nX += m_dwWidth;
				} // while
				break;
			case BKDLGST_MODE_TILEBOTTOM:
				while(nX < rWnd.Width()) 
				{
					dcMemoryDC.BitBlt(nX, rWnd.bottom - m_dwHeight, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
					nX += m_dwWidth;
				} // while
				break;
			case BKDLGST_MODE_TILELEFT:
				while (nY < rWnd.Height()) 
				{
					dcMemoryDC.BitBlt(0, nY, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
					nY += m_dwHeight;
				} // while
				break;
			case BKDLGST_MODE_TILERIGHT:
				while (nY < rWnd.Height()) 
				{
					dcMemoryDC.BitBlt(rWnd.right - m_dwWidth, nY, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
					nY += m_dwHeight;
				} // while
				break;
			case BKDLGST_MODE_TOPLEFT:
				dcMemoryDC.BitBlt(0, 0, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
				break;
			case BKDLGST_MODE_TOPRIGHT:
				dcMemoryDC.BitBlt(rWnd.right - m_dwWidth, 0, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
				break;
			case BKDLGST_MODE_TOPCENTER:
				nX = ((rWnd.Width() - m_dwWidth)/2);
				dcMemoryDC.BitBlt(nX, 0, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
				break;
			case BKDLGST_MODE_BOTTOMLEFT:
				dcMemoryDC.BitBlt(0, rWnd.bottom - m_dwHeight, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
				break;
			case BKDLGST_MODE_BOTTOMRIGHT:
				dcMemoryDC.BitBlt(rWnd.right - m_dwWidth, rWnd.bottom - m_dwHeight, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
				break;
			case BKDLGST_MODE_BOTTOMCENTER:
				nX = ((rWnd.Width() - m_dwWidth)/2);
				dcMemoryDC.BitBlt(nX, rWnd.bottom - m_dwHeight, m_dwWidth, m_dwHeight, &dcTempDC, 0, 0, SRCCOPY);
				break;
		} // switch

		pDC->BitBlt(0, 0, rWnd.Width(), rWnd.Height(), &dcMemoryDC, 0, 0, SRCCOPY);

		OnPostEraseBkgnd(&dcMemoryDC);

		::SelectObject(dcTempDC.m_hDC, hbmpOldTempBitmap);
		dcMemoryDC.SelectObject(pbmpOldMemoryBitmap);
	} // if

	return bRetValue;
} // End of OnEraseBkgnd

void CBkDialogST::OnPostEraseBkgnd(CDC* pDC)
{
} // End of OnPostEraseBkgnd

// -------------------------------------------------------------------------------------
// Scan a bitmap and return a perfect fit region.
// The caller must release the memory...
// (this method starts with a full region and excludes inside the loop)
// -------------------------------------------------------------------------------------
// Credits for this function go to Vander Nunes
//
#ifndef UNDER_CE
HRGN CBkDialogST::ScanRegion(HBITMAP hBitmap, BYTE byTransR, BYTE byTransG, BYTE byTransB)
{
	// bitmap width and height
	DWORD dwBmpWidth = 0, dwBmpHeight = 0;

	// the final region and a temporary region
	HRGN hRgn = NULL, hTmpRgn = NULL;

	// 24bit pixels from the bitmap
	LPBYTE lpbyPixels = Get24BitPixels(hBitmap, &dwBmpWidth, &dwBmpHeight);
	if (!lpbyPixels) return NULL;

	// create our working region
	hRgn = ::CreateRectRgn(0, 0, dwBmpWidth, dwBmpHeight);
	if (!hRgn) 
	{ 
		delete[] lpbyPixels; 
		return NULL; 
	} // if

	// ---------------------------------------------------------
	// scan the bitmap
	// ---------------------------------------------------------
	DWORD p=0;
	for (DWORD y=0; y<dwBmpHeight; y++)
	{
		for (DWORD x=0; x<dwBmpWidth; x++)
		{
			BYTE jRed   = lpbyPixels[p+2];
			BYTE jGreen = lpbyPixels[p+1];
			BYTE jBlue  = lpbyPixels[p+0];

			if (jRed == byTransR && jGreen == byTransG && jBlue == byTransB)
			{
				// remove transparent color from region
				hTmpRgn = ::CreateRectRgn(x,y,x+1,y+1);
				::CombineRgn(hRgn, hRgn, hTmpRgn, RGN_XOR);
				::DeleteObject(hTmpRgn);
			} // if

			// next pixel
			p+=3;
		} // for
	} // for

	// release pixels
	if(lpbyPixels)
	{
		delete[] lpbyPixels;
		lpbyPixels = NULL;
	}

	// return the region
	return hRgn;
} // End of ScanRegion
#endif

// -------------------------------------------------------------------------------------
// Return bitmap pixels in 24bits format.
// The caller must release the memory...
// -------------------------------------------------------------------------------------
// Credits for this function go to Vander Nunes
//
#ifndef UNDER_CE
LPBYTE CBkDialogST::Get24BitPixels(HBITMAP hBitmap, LPDWORD lpdwWidth, LPDWORD lpdwHeight)
{
	// a bitmap object just to get bitmap width and height
	BITMAP bmpBmp;

	// pointer to original bitmap info
	LPBITMAPINFO pbmiInfo;

	// bitmap info will hold the new 24bit bitmap info
	BITMAPINFO bmiInfo;

	// width and height of the bitmap
	DWORD dwBmpWidth = 0, dwBmpHeight = 0;

	// ---------------------------------------------------------
	// get some info from the bitmap
	// ---------------------------------------------------------
	int nCheck = ::GetObject(hBitmap, sizeof(bmpBmp),&bmpBmp);
	pbmiInfo   = (LPBITMAPINFO)&bmpBmp;

	// get width and height
	dwBmpWidth  = (DWORD)pbmiInfo->bmiHeader.biWidth;
	dwBmpWidth -= (dwBmpWidth%4);                       // width is 4 byte boundary aligned.
	dwBmpHeight = (DWORD)pbmiInfo->bmiHeader.biHeight;

	// copy to caller width and height parms
	*lpdwWidth  = dwBmpWidth;
	*lpdwHeight = dwBmpHeight;
	// ---------------------------------------------------------

	// allocate width * height * 24bits pixels
	LPBYTE lpbyPixels = new BYTE[dwBmpWidth * dwBmpHeight * 3];
	if (!lpbyPixels) return NULL;

	// get user desktop device context to get pixels from
	HDC hDC = ::GetWindowDC(NULL);

	// fill desired structure
	bmiInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmiInfo.bmiHeader.biWidth = dwBmpWidth;
	bmiInfo.bmiHeader.biHeight = 0 - (int)dwBmpHeight;
	bmiInfo.bmiHeader.biPlanes = 1;
	bmiInfo.bmiHeader.biBitCount = 24;
	bmiInfo.bmiHeader.biCompression = BI_RGB;
	bmiInfo.bmiHeader.biSizeImage = dwBmpWidth * dwBmpHeight * 3;
	bmiInfo.bmiHeader.biXPelsPerMeter = 0;
	bmiInfo.bmiHeader.biYPelsPerMeter = 0;
	bmiInfo.bmiHeader.biClrUsed = 0;
	bmiInfo.bmiHeader.biClrImportant = 0;

	// get pixels from the original bitmap converted to 24bits
	int iRes = ::GetDIBits(hDC,hBitmap, 0, dwBmpHeight, (LPVOID)lpbyPixels, &bmiInfo, DIB_RGB_COLORS);

	// release the device context
	::ReleaseDC(NULL,hDC);

	// if failed, cancel the operation.
	if (!iRes)
	{
		if(lpbyPixels)
		{
			delete[] lpbyPixels;
			lpbyPixels = NULL;
		}
		return NULL;
	} // if

	// return the pixel array
	return lpbyPixels;
} // End of Get24BitPixels
#endif

//
// Parameters:
//		[IN]	bActivate
//				TRUE if EasyMove mode must be activated.
//
// Return value:
//		BKDLGST_OK
//			Function executed successfully.
//
DWORD CBkDialogST::ActivateEasyMoveMode(BOOL bActivate)
{
	m_bEasyMoveMode = bActivate;

	return BKDLGST_OK;
} // End of ActivateEasyMoveMode

//
// Parameters:
//		[IN]	nBitmap
//				Resource ID of the bitmap to use as background.
//		[IN]	crTransColor
//				A COLORREF value indicating the color to "remove" from the
//				bitmap. When crTransColor is specified (different from -1L)
//				a region will be created using the supplied bitmap and applied
//				to the window.
//
// Return value:
//		BKDLGST_OK
//			Function executed successfully.
//		BKDLGST_INVALIDRESOURCE
//			The resource specified cannot be found or loaded.
//		BKDLGST_FAILEDREGION
//			Failed creating region
//
DWORD CBkDialogST::SetBitmap(int nBitmap, COLORREF crTransColor)
{
	HBITMAP		hBitmap			= NULL;
	HINSTANCE	hInstResource	= NULL;

	// Find correct resource handle
	hInstResource = AfxFindResourceHandle(MAKEINTRESOURCE(nBitmap), RT_BITMAP);

	// Load bitmap In
	hBitmap = (HBITMAP)::LoadImage(hInstResource, MAKEINTRESOURCE(nBitmap), IMAGE_BITMAP, 0, 0, 0);

	return SetBitmap(hBitmap, crTransColor);
} // End of SetBitmap

//-- Override
DWORD CBkDialogST::SetBitmap(CString strImagePathIn, COLORREF crTransColorIn)
{
	HBITMAP		hBitmap		= NULL;
	
	// Load bitmap Focus In
	//hBitmap = (HBITMAP)::LoadImage(NULL, strImagePathIn, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_DEFAULTSIZE);
	//-- 2011-5-20 Lee JungTaek
	Bitmap bitBK(strImagePathIn);
	bitBK.GetHBITMAP(RGB(255,255,255), &hBitmap);

	return SetBitmap(hBitmap, crTransColorIn);
} // End of SetBitmaps

//-- 2011-5-24 Lee JungTaek
//-- Alpha blending
DWORD CBkDialogST::SetTransBGImage(CString strImagePathIn)
{
	HBITMAP		hBitmap		= NULL;

 	Bitmap bitBK(strImagePathIn);
 	bitBK.GetHBITMAP(RGB(0,0,0), &hBitmap);
 
  	CBitmap cBitmap;
  	cBitmap.Attach(hBitmap);
	
	DoUpdateDummyDialog(cBitmap, 255);

	return TRUE;
}

void CBkDialogST::DoUpdateDummyDialog(CBitmap &bmp, BYTE SourceConstantAlpha)
{
	// make sure the window has the WS_EX_LAYERED style
	ModifyStyleEx(0, WS_EX_LAYERED);

	// Create/setup the DC's
	CDC dcScreen;
	CDC dcMemory;

	dcScreen.Attach(::GetDC(NULL));
	dcMemory.CreateCompatibleDC(&dcScreen);

	CBitmap *pOldBitmap= dcMemory.SelectObject(&bmp);

	// get the bitmap dimensions
	BITMAP bmpInfo;
	bmp.GetBitmap(&bmpInfo);

	// get the window rectangle (we are only interested in the top left position)
	CRect rectDlg;
	GetWindowRect(rectDlg);

	// calculate the new window position/size based on the bitmap size
	CPoint ptWindowScreenPosition(rectDlg.TopLeft());
	CSize szWindow(bmpInfo.bmWidth, bmpInfo.bmHeight);
	
	// Perform the alpha blend
	// setup the blend function
	BLENDFUNCTION blendPixelFunction= { AC_SRC_OVER, 0, SourceConstantAlpha, AC_SRC_ALPHA };
	CPoint ptSrc(0,0); // start point of the copy from dcMemory to dcScreen

	// perform the alpha blend
 	BOOL bRet= ::UpdateLayeredWindow(GetSafeHwnd(), dcScreen, &ptWindowScreenPosition, &szWindow, dcMemory,
 		&ptSrc, 0, &blendPixelFunction, ULW_ALPHA);
	
	//ASSERT(bRet);

	// clean up
	dcMemory.SelectObject(pOldBitmap);
}

//
// Parameters:
//		[IN]	hBitmap
//				Handle to the bitmap to use as background.
//		[IN]	crTransColor
//				A COLORREF value indicating the color to "remove" from the
//				bitmap. When crTransColor is specified (different from -1L)
//				a region will be created using the supplied bitmap and applied
//				to the window.
//
// Return value:
//		BKDLGST_OK
//			Function executed successfully.
//		BKDLGST_INVALIDRESOURCE
//			The resource specified cannot be found or loaded.
//		BKDLGST_FAILEDREGION
//			Failed creating region
//
DWORD CBkDialogST::SetBitmap(HBITMAP hBitmap, COLORREF crTransColor)
{
	int		nRetValue;
	BITMAP	csBitmapSize;

	// Free any loaded resource
	FreeResources();

	if (hBitmap)
	{
		m_hBitmap = hBitmap;

		// Get bitmap size
		nRetValue = ::GetObject(hBitmap, sizeof(csBitmapSize), &csBitmapSize);
		if (nRetValue == 0)
		{
			FreeResources();
			return BKDLGST_INVALIDRESOURCE;
		} // if
		m_dwWidth = (DWORD)csBitmapSize.bmWidth;
		m_dwHeight = (DWORD)csBitmapSize.bmHeight;

#ifndef UNDER_CE
		// Create region ?
		if (crTransColor != -1L)
		{
			m_hRegion = ScanRegion(m_hBitmap, GetRValue(crTransColor), GetGValue(crTransColor), GetBValue(crTransColor));
			if (m_hRegion == NULL)
			{
				FreeResources();
				return BKDLGST_FAILEDREGION;
			} // if

			::SetWindowRgn(m_hWnd, m_hRegion, FALSE);
		} // if
#endif
	} // if

	Invalidate();

	return BKDLGST_OK;
} // End of SetBitmap

//
// Parameters:
//		[IN]	byMode
//				Specifies how the bitmap will be placed in the dialog background.
//		[IN]	bRepaint
//				If TRUE the dialog will be repainted.
//
// Return value:
//		BKDLGST_OK
//			Function executed successfully.
//		BKDLGST_INVALIDMODE
//			Invalid mode.
//
DWORD CBkDialogST::SetMode(BYTE byMode, BOOL bRepaint)
{
	if (byMode >= BKDLGST_MAX_MODES)	return BKDLGST_INVALIDMODE;

	// Set new mode
	m_byMode = byMode;

	if (bRepaint == TRUE)	Invalidate();

	return BKDLGST_OK;
} // End of SetMode


void CBkDialogST::OnWindowPosChanged(WINDOWPOS* lpwndpos)
{
	CRSChildDialog::OnWindowPosChanged(lpwndpos);
}

void CBkDialogST::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	CRSChildDialog::OnWindowPosChanging(lpwndpos);
	//-- 2011-5-18 Lee JungTaek
	if(!GetMagneticWindow()) 
		return;

	//-- Get Parents Rect
	CRect rectParent;
	GetParent()->GetWindowRect(&rectParent);

	int nCx	= lpwndpos->x;	
	int nCy	= lpwndpos->y;
	int nCw	= lpwndpos->cx;
	int nCh	= lpwndpos->cy;
	int nCr	= nCx + nCw;	
	int nCb	= nCy + nCh;
	
	int nPx	= rectParent.left;
	int nPy	= rectParent.top;
	int nPw	= rectParent.Width();
	int nPh	= rectParent.Height();
	int nPr	= rectParent.right;	
	int nPb	= rectParent.bottom;

	//-- No Change
	if(!(nCx == 0 && nCy == 0 && nCw == 0 && nCh == 0)) 	
		SetDockType(MAIN_NULL);

	//-- Set Magnetic Type
	//-- Exist Parents Rect : x
	if(	
		(	nCx >= nPx			&& nCx <= nPr				) || 
		(	nCx < nPx			&& nCx + nPw > nPr		) ||
		(	nCx + nPw >= nPx && nCx + nPw <= nPr	)	
	) 
	{
		if((nCb >= nPy - MAGNET_ATTACH_RANGE) && (nCb <= nPy + MAGNET_ATTACH_RANGE)) 
		{
			//-- Parents Top
			//SetDockType(MAIN_TOP); //dh0.seo 2015-01-21 Smart Panel 오류 주석 MAIN_TOP -> MAIN_BOTTOM 으로 수정
			SetDockType(MAIN_BOTTOM);
			lpwndpos->y = nPy - lpwndpos->cy;
			//-- Magnetic X
			if((nCx >= nPx - MAGNET_ATTACH_RANGE) && (nCx <= nPx + MAGNET_ATTACH_RANGE))
				lpwndpos->x = nPx;
		}
		else if(nCy >= nPb - MAGNET_ATTACH_RANGE && nCy <= nPb + MAGNET_ATTACH_RANGE) 
		{
			//-- Parents Bottom
			SetDockType(MAIN_BOTTOM);			
			lpwndpos->y = nPb;
			//-- Magnetic X
			if((nCx >= nPx - MAGNET_ATTACH_RANGE) && (nCx <= nPx + MAGNET_ATTACH_RANGE))
				lpwndpos->x = nPx;
		}
	}		

	//-- Exist Parents Rect : y
	if((	nCy >= nPy		&&	nCy </*=*/ nPb	) || (	nCy < nPy		&&	nCy > nPb	) ||(	nCy >/*=*/ nPy		&&	nCy <= nPb))		
	{  
		if((nCx >= nPr - MAGNET_ATTACH_RANGE) && (nCx <= nPr + MAGNET_ATTACH_RANGE) && nCx != 0) 
		{
			//-- Parents Right
			SetDockType(MAIN_RIGHT);
			lpwndpos->x = nPr;
			//-- Magnetic X
			if((nCy >= nPy - MAGNET_ATTACH_RANGE) && (nCy <= nPy + MAGNET_ATTACH_RANGE))
				lpwndpos->y = nPy;
		}
		else if((nCx + nCw >= nPx - MAGNET_ATTACH_RANGE) && (nCx + nCw<= nPx + MAGNET_ATTACH_RANGE) && nCx != 0) 
		{
			//-- Parents Left
			SetDockType(MAIN_LEFT);
			lpwndpos->x	 = nPx - nCw;
			//-- Magnetic X
			if((nCy >= nPy - MAGNET_ATTACH_RANGE) && (nCy <= nPy + MAGNET_ATTACH_RANGE))
				lpwndpos->y = nPy;
		}
	}
}

#ifndef UNDER_CE
DWORD CBkDialogST::ShrinkToFit(BOOL bRepaint)
{
	CRect	rWnd;
	CRect	rClient;
	DWORD	dwDiffCX;
	DWORD	dwDiffCY;

	GetWindowRect(&rWnd);
	GetClientRect(&rClient);

	dwDiffCX = rWnd.Width() - rClient.Width();
	dwDiffCY = rWnd.Height() - rClient.Height();

	m_byMode = BKDLGST_MODE_CENTER;

	MoveWindow(rWnd.left, rWnd.top, dwDiffCX + m_dwWidth, dwDiffCY + m_dwHeight, bRepaint);

	return BKDLGST_OK;
} // End of ShrinkToFit
#endif

#ifdef	BKDLGST_DEFINES
#undef	BKDLGST_DEFINES
#undef	WM_NCLBUTTONDOWN
#endif

E_DECK_DRAG_STATE CBkDialogST::PtInResizeRect(CPoint point)
{
	CRect rtClient;
 
	//BOTTOM_RIGHT2
	GetClientRect(rtClient);
	rtClient.left = rtClient.right - 19;
	rtClient.top = rtClient.bottom - 21;
 
	if(rtClient.PtInRect(point))
		return eBOTTOMRIGHT_RESIZE;
 
	//LEFT:
	GetClientRect(rtClient);
	rtClient.right = rtClient.left + RESIZE_FRAME_WIDTH;
	rtClient.top += RESIZE_FRAME_WIDTH;
	rtClient.bottom -= RESIZE_FRAME_WIDTH;
 
	if(rtClient.PtInRect(point))
		return eLEFT_RESIZE;
 
	//RIGHT:
	GetClientRect(rtClient);
	rtClient.left = rtClient.right - RESIZE_FRAME_WIDTH;
	rtClient.top += RESIZE_FRAME_WIDTH;
	rtClient.bottom -= RESIZE_FRAME_WIDTH;
 
	if(rtClient.PtInRect(point))
		return eRIGHT_RESIZE;
 
	//TOP:
	GetClientRect(rtClient);
	rtClient.bottom = rtClient.top + RESIZE_FRAME_HEIGHT;
	rtClient.left += RESIZE_FRAME_HEIGHT;
	rtClient.right -= RESIZE_FRAME_HEIGHT;
 
	if(rtClient.PtInRect(point))
		return eTOP_RESIZE ;
 
	//BOTTOM:
	GetClientRect(rtClient);
	rtClient.top = rtClient.bottom - RESIZE_FRAME_HEIGHT;
	rtClient.left += RESIZE_FRAME_HEIGHT;
	rtClient.right -= RESIZE_FRAME_HEIGHT;
 
	if(rtClient.PtInRect(point))
		return eBOTTOM_RESIZE;
 
	//TOP_LEFT
	GetClientRect(rtClient);
	rtClient.right = rtClient.left + RESIZE_FRAME_WIDTH;
	rtClient.bottom = rtClient.top + RESIZE_FRAME_HEIGHT;
 
	if(rtClient.PtInRect(point))
		return eTOPLEFT_RESIZE;
 
	//TOP_RIGHT
	GetClientRect(rtClient);
	rtClient.left = rtClient.right - RESIZE_FRAME_WIDTH;
	rtClient.bottom = rtClient.top + RESIZE_FRAME_HEIGHT;
 
	if(rtClient.PtInRect(point))
		return eTOPRIGHT_RESIZE;
 
	//BOTTOM_LEFT
	GetClientRect(rtClient);
	rtClient.right = rtClient.left + RESIZE_FRAME_WIDTH;
	rtClient.top = rtClient.bottom - RESIZE_FRAME_HEIGHT;
 
	if(rtClient.PtInRect(point))
		return eBOTTOMLEFT_RESIZE;
 
	return eNONE;
 
}

BOOL CBkDialogST::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if(m_bSmartStyle)
		return CDialogEx::OnSetCursor(pWnd, nHitTest, message);

	CPoint point;
	GetCursorPos(&point);
	ScreenToClient(&point);
	
	switch(PtInResizeRect(point))
	{
	case eLEFT_RESIZE:
	case eRIGHT_RESIZE:
		SetCursor( LoadCursor(NULL, IDC_SIZEWE) );
		return TRUE;
	case eTOP_RESIZE:
	case eBOTTOM_RESIZE:
	case eSENDEDIT_RESIZE:
		SetCursor( LoadCursor(NULL, IDC_SIZENS) );
		return TRUE;
	case eTOPLEFT_RESIZE:
	case eBOTTOMRIGHT_RESIZE:
		SetCursor( LoadCursor(NULL, IDC_SIZENWSE) );
		return TRUE;
	case eTOPRIGHT_RESIZE:
	case eBOTTOMLEFT_RESIZE:
		SetCursor( LoadCursor(NULL, IDC_SIZENESW) );
		return TRUE;
	case eMOVE:
	case eNONE:
		SetCursor( LoadCursor(NULL, IDC_ARROW) );
	}

	return CRSChildDialog::OnSetCursor(pWnd, nHitTest, message);
}

void CBkDialogST::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// EasyMove mode
	if (m_bSmartStyle)		
	{
		if(m_bEasyMoveMode == TRUE)
			PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));
		return CDialogEx::OnLButtonDown(nFlags, point);
	}	

	switch(PtInResizeRect(point))
	{
		case eLEFT_RESIZE:
			ReleaseCapture(); 
			SendMessage(WM_SYSCOMMAND, SC_SZLEFT, 0); 
		break;
		case eRIGHT_RESIZE:
			ReleaseCapture(); 
			SendMessage(WM_SYSCOMMAND, SC_SZRIGHT, 0); 
		break;
		case eTOP_RESIZE:
			ReleaseCapture(); 
			SendMessage(WM_SYSCOMMAND, SC_SZTOP, 0); 
		break;
		case eBOTTOM_RESIZE:
			ReleaseCapture(); 
			SendMessage(WM_SYSCOMMAND, SC_SZBOTTOM, 0); 
		break;
		case eTOPLEFT_RESIZE:
			ReleaseCapture(); 
			SendMessage(WM_SYSCOMMAND, SC_SZTOPLEFT, 0); 
		break;
		case eTOPRIGHT_RESIZE:
			ReleaseCapture(); 
			SendMessage(WM_SYSCOMMAND, SC_SZTOPRIGHT, 0); 
		break;
		case eBOTTOMLEFT_RESIZE:
			ReleaseCapture(); 
			SendMessage(WM_SYSCOMMAND, SC_SZBOTTOMLEFT, 0); 
		break;
		case eBOTTOMRIGHT_RESIZE:
			ReleaseCapture(); 
			SendMessage(WM_SYSCOMMAND, SC_SZBOTTOMRIGHT, 0); 
		break;
		case eNONE:
		{
			CRect rtClient;
			GetClientRect(rtClient);
	   
			rtClient.bottom = rtClient.top + 30;
	   
			if(rtClient.PtInRect(point))
			SendMessage(WM_SYSCOMMAND, SC_DRAGMOVE, 0); 
		}
	}


	CRSChildDialog::OnLButtonDown(nFlags, point);
} // End of OnLButtonDown