#pragma once

#include "ResizeDialog.h"
#include "GlobalIndex.h"

// CTransDlg dialog

class CTransDlg : public CResizeDialog
{
	DECLARE_DYNAMIC(CTransDlg)

private :
private :
	int m_x;
	int m_y;
	int m_dx;
	int m_dy;
	
	int m_restrict_width;
	int m_restrict_height;

public:
	CTransDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTransDlg();

// Dialog Data
	enum { IDD = IDD_DLG_TRANSPARENT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	void n_SetDlgAlpha(int chAlpha);
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnExitSizeMove();
	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	
public:
	void SetWindowRect(int x, int y, int dx, int dy);
	void SetRestrict(int width, int height);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
};