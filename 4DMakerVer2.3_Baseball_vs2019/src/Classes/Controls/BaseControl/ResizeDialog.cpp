// ResizeDialog.cpp : implementation file
//

#include "stdafx.h"
#include "ResizeDialog.h"
#include "afxdialogex.h"

#define    SC_SZLEFT           (0xF001)              // resize from left 
#define    SC_SZRIGHT          (0xF002)              // resize from right 
#define    SC_SZTOP            (0xF003)              // resize from top 
#define    SC_SZTOPLEFT        (0xF004)              // resize from top left 
#define    SC_SZTOPRIGHT       (0xF005)              // resize from top right 
#define    SC_SZBOTTOM         (0xF006)              // resize from bottom 
#define    SC_SZBOTTOMLEFT     (0xF007)              // resize from bottom left 
#define    SC_SZBOTTOMRIGHT    (0xF008)              // resize from bottom right 
#define    SC_DRAGMOVE         (0xF012)              // move by drag 

#define    RESIZE_FRAME_WIDTH        10
#define    RESIZE_FRAME_HEIGHT		 10



// CResizeDialog dialog

IMPLEMENT_DYNAMIC(CResizeDialog, CDialogEx)

CResizeDialog::CResizeDialog(UINT nResourceID,CWnd* pParent /*=NULL*/)
	: CDialogEx(nResourceID, pParent)
{

}

CResizeDialog::~CResizeDialog()
{
}

void CResizeDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CResizeDialog, CDialogEx)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_SETCURSOR()
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()


// CResizeDialog message handlers


void CResizeDialog::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	 switch(PtInResizeRect(point))
	 {
	 case eLEFT_RESIZE:
	  ReleaseCapture(); 
	  SendMessage(WM_SYSCOMMAND, SC_SZLEFT, 0); 
	  break;
	 case eRIGHT_RESIZE:
	  ReleaseCapture(); 
	  SendMessage(WM_SYSCOMMAND, SC_SZRIGHT, 0); 
	  break;
	 case eTOP_RESIZE:
	  ReleaseCapture(); 
	  SendMessage(WM_SYSCOMMAND, SC_SZTOP, 0); 
	  break;
	 case eBOTTOM_RESIZE:
	  ReleaseCapture(); 
	  SendMessage(WM_SYSCOMMAND, SC_SZBOTTOM, 0); 
	  break;
	 case eTOPLEFT_RESIZE:
	  ReleaseCapture(); 
	  SendMessage(WM_SYSCOMMAND, SC_SZTOPLEFT, 0); 
	  break;
	 case eTOPRIGHT_RESIZE:
	  ReleaseCapture(); 
	  SendMessage(WM_SYSCOMMAND, SC_SZTOPRIGHT, 0); 
	  break;
	 case eBOTTOMLEFT_RESIZE:
	  ReleaseCapture(); 
	  SendMessage(WM_SYSCOMMAND, SC_SZBOTTOMLEFT, 0); 
	  break;
	 case eBOTTOMRIGHT_RESIZE:
	  ReleaseCapture(); 
	  SendMessage(WM_SYSCOMMAND, SC_SZBOTTOMRIGHT, 0); 
	  break;
	 case eNONE:
	  {
	   CRect rtClient;
	   GetClientRect(rtClient);
	   
	   rtClient.bottom = rtClient.top + 30;
	   
	   if(rtClient.PtInRect(point))
		SendMessage(WM_SYSCOMMAND, SC_DRAGMOVE, 0); 
	  }
	 }

	CDialogEx::OnLButtonDown(nFlags, point);
}


E_DECK_DRAG_STATE CResizeDialog::PtInResizeRect(CPoint point)
{
 CRect rtClient;
 
//BOTTOM_RIGHT2
 GetClientRect(rtClient);
 rtClient.left = rtClient.right - 19;
 rtClient.top = rtClient.bottom - 21;
 
 if(rtClient.PtInRect(point))
  return eBOTTOMRIGHT_RESIZE;
 
//LEFT:
 GetClientRect(rtClient);
 rtClient.right = rtClient.left + RESIZE_FRAME_WIDTH;
 rtClient.top += RESIZE_FRAME_WIDTH;
 rtClient.bottom -= RESIZE_FRAME_WIDTH;
 
 if(rtClient.PtInRect(point))
  return eLEFT_RESIZE;
 
//RIGHT:
 GetClientRect(rtClient);
 rtClient.left = rtClient.right - RESIZE_FRAME_WIDTH;
 rtClient.top += RESIZE_FRAME_WIDTH;
 rtClient.bottom -= RESIZE_FRAME_WIDTH;
 
 if(rtClient.PtInRect(point))
  return eRIGHT_RESIZE;
 
//TOP:
 GetClientRect(rtClient);
 rtClient.bottom = rtClient.top + RESIZE_FRAME_HEIGHT;
 rtClient.left += RESIZE_FRAME_HEIGHT;
 rtClient.right -= RESIZE_FRAME_HEIGHT;
 
 if(rtClient.PtInRect(point))
  return eTOP_RESIZE ;
 
//BOTTOM:
 GetClientRect(rtClient);
 rtClient.top = rtClient.bottom - RESIZE_FRAME_HEIGHT;
 rtClient.left += RESIZE_FRAME_HEIGHT;
 rtClient.right -= RESIZE_FRAME_HEIGHT;
 
 if(rtClient.PtInRect(point))
  return eBOTTOM_RESIZE;
 
//TOP_LEFT
 GetClientRect(rtClient);
 rtClient.right = rtClient.left + RESIZE_FRAME_WIDTH;
 rtClient.bottom = rtClient.top + RESIZE_FRAME_HEIGHT;
 
 if(rtClient.PtInRect(point))
  return eTOPLEFT_RESIZE;
 
//TOP_RIGHT
 GetClientRect(rtClient);
 rtClient.left = rtClient.right - RESIZE_FRAME_WIDTH;
 rtClient.bottom = rtClient.top + RESIZE_FRAME_HEIGHT;
 
 if(rtClient.PtInRect(point))
  return eTOPRIGHT_RESIZE;
 
//BOTTOM_LEFT
 GetClientRect(rtClient);
 rtClient.right = rtClient.left + RESIZE_FRAME_WIDTH;
 rtClient.top = rtClient.bottom - RESIZE_FRAME_HEIGHT;
 
 if(rtClient.PtInRect(point))
  return eBOTTOMLEFT_RESIZE;

 return eNONE;
 
}


void CResizeDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	CRect rtResize;
	PtInResizeRect(point);
	
	CDialogEx::OnMouseMove(nFlags, point);
}


BOOL CResizeDialog::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	// TODO: Add your message handler code here and/or call default

	CPoint point;
	GetCursorPos(&point);
	ScreenToClient(&point);
	
	switch(PtInResizeRect(point))
	{
	 case eLEFT_RESIZE:
	 case eRIGHT_RESIZE:
	  SetCursor( LoadCursor(NULL, IDC_SIZEWE) );
	  return TRUE;
	 case eTOP_RESIZE:
	 case eBOTTOM_RESIZE:
	 case eSENDEDIT_RESIZE:
	  SetCursor( LoadCursor(NULL, IDC_SIZENS) );
	  return TRUE;
	 case eTOPLEFT_RESIZE:
	 case eBOTTOMRIGHT_RESIZE:
	  SetCursor( LoadCursor(NULL, IDC_SIZENWSE) );
	  return TRUE;
	 case eTOPRIGHT_RESIZE:
	 case eBOTTOMLEFT_RESIZE:
	  SetCursor( LoadCursor(NULL, IDC_SIZENESW) );
	  return TRUE;
	 case eMOVE:
	 case eNONE:
	  SetCursor( LoadCursor(NULL, IDC_ARROW) );
	}

	return CDialogEx::OnSetCursor(pWnd, nHitTest, message);
}


void CResizeDialog::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: Add your message handler code here and/or call default
	 
	lpMMI->ptMinTrackSize.x = 305;
	lpMMI->ptMinTrackSize.y = 155;
	 
	RECT rcWorkArea;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWorkArea, SPIF_SENDCHANGE);
	 
	lpMMI->ptMaxSize.x = rcWorkArea.right;
	lpMMI->ptMaxSize.y = rcWorkArea.bottom;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}
