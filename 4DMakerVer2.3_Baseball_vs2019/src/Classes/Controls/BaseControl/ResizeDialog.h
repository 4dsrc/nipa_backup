#pragma once
#include "BkDialogST.h"
/*
enum E_DECK_DRAG_STATE { 
 eNONE , 
 eMOVE ,
 eFLOAT,
 eLEFT_RESIZE , 
 eRIGHT_RESIZE,
 eTOP_RESIZE , 
 eBOTTOM_RESIZE ,
 eTOPLEFT_RESIZE,
 eTOPRIGHT_RESIZE,
 eBOTTOMLEFT_RESIZE,
 eBOTTOMRIGHT_RESIZE,
 eSENDEDIT_RESIZE
};
*/
// CResizeDialog dialog

class CResizeDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CResizeDialog)

public:
	CResizeDialog(UINT nResourceID, CWnd* pParent = NULL); 
//	CResizeDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CResizeDialog();

// Dialog Data
	enum { IDD = IDD_DLG_BOARD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	E_DECK_DRAG_STATE PtInResizeRect(CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
};
