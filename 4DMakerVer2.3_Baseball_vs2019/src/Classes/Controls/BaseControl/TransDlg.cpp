// TransDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TransDlg.h"
#include "afxdialogex.h"

typedef BOOL (WINAPI *SetLayer)(HWND hWnd, COLORREF crKey, BYTE bAlpha, DWORD dwFlags);
#define LWA_COLORKEY            0x01
#define LWA_ALPHA               0x02

// CTransDlg dialog

IMPLEMENT_DYNAMIC(CTransDlg, CDialogEx)

CTransDlg::CTransDlg(CWnd* pParent /*=NULL*/)
	: CResizeDialog(CTransDlg::IDD, pParent)
{

}

CTransDlg::~CTransDlg()
{
}

void CTransDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTransDlg, CResizeDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_EXITSIZEMOVE()
	ON_WM_TIMER()
	ON_WM_LBUTTONUP()
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()


// CTransDlg message handlers
void CTransDlg::n_SetDlgAlpha(int chAlpha)
{
    HMODULE hUser32 = GetModuleHandle(_T("USER32.DLL"));
    SetLayer pSetLayer = (SetLayer)GetProcAddress(hUser32, "SetLayeredWindowAttributes");
    if(pSetLayer == NULL)
    {
//        MessageBox("win2000 �̻�");
        return;
    }
    SetWindowLong(this->m_hWnd, GWL_EXSTYLE, GetWindowLong(this->m_hWnd, GWL_EXSTYLE) | 0x80000);
    pSetLayer(this->m_hWnd, 0,chAlpha, LWA_ALPHA);
}

void CTransDlg::SetRestrict(int width, int height)
{
	m_restrict_width = width;
	m_restrict_height = height;
}


BOOL CTransDlg::OnInitDialog()
{
	CResizeDialog::OnInitDialog();

	// TODO:  Add extra initialization here
//	

	::SetWindowPos(this->GetSafeHwnd(), HWND_TOP, m_x,m_y,m_dx,m_dy, SWP_NOACTIVATE | SWP_NOOWNERZORDER);

	n_SetDlgAlpha(50);
	SetTimer(1,25,NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CTransDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CDialogEx::OnPaint() for painting messages
	CRect rt;

	CDC *pDC = this->GetWindowDC();

	this->GetWindowRect(&rt);

	CPen pen, *oldpen;
	pen.CreatePen(PS_INSIDEFRAME, 2, RGB(255,0,0)); // ���� ������ ���������� ����
	oldpen = (CPen*)pDC->SelectObject(&pen);
	pDC->Rectangle(0,0,rt.right-rt.left,rt.bottom-rt.top);
	pDC->SelectObject (oldpen);
	this->ReleaseDC(pDC);
}


void CTransDlg::OnSize(UINT nType, int cx, int cy)
{
	CResizeDialog::OnSize(nType, cx, cy);
	Invalidate(TRUE);
	// TODO: Add your message handler code here
}

void CTransDlg::OnExitSizeMove()
{
	// TODO: Add your message handler code here and/or call default

	CRect rt;
	this->GetWindowRect(&rt);
	WPARAM wParam;
	wParam=0;
	wParam = rt.left;
	wParam = wParam <<16;
	wParam = wParam | rt.right;
	
	LPARAM lParam;
	lParam=0;
	lParam = rt.top;
	lParam = lParam <<16;
	lParam = lParam | rt.bottom;
	mouse_event( MOUSEEVENTF_LEFTUP,0,0,0,0);
	OnOK();
	::SendMessage(this->GetParent()->m_hWnd,WM_END_RESIZE_EVENT,wParam,lParam);

	CResizeDialog::OnExitSizeMove();
}


void CTransDlg::SetWindowRect(int x, int y, int dx, int dy)
{
	 m_x = x;
	 m_y = y;
	 m_dx = dx;
	 m_dy = dy;
}

void CTransDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	switch(nIDEvent)
	{
		case 1:
			{
			 CRect rt;
			 this->GetWindowRect(&rt);
			 MoveWindow(m_x,m_y,m_dx,m_dy,0);
			 mouse_event( MOUSEEVENTF_LEFTDOWN,0,0,0,0);
//			 mouse_event( MOUSEEVENTF_MOVE ,0,0,0,0);
			 KillTimer(1);
			}
			break;
	}
	CResizeDialog::OnTimer(nIDEvent);
}


void CTransDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	OnOK();
	mouse_event( MOUSEEVENTF_LEFTUP,0,0,0,0);
	CResizeDialog::OnLButtonUp(nFlags, point);
}


void CTransDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: Add your message handler code here and/or call default
	lpMMI->ptMinTrackSize.x = m_restrict_width;
	lpMMI->ptMinTrackSize.y = m_restrict_height;
	 
	RECT rcWorkArea;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWorkArea, SPIF_SENDCHANGE);
	 
	lpMMI->ptMaxSize.x = rcWorkArea.right;
	lpMMI->ptMaxSize.y = rcWorkArea.bottom;

	CResizeDialog::OnGetMinMaxInfo(lpMMI);
}
