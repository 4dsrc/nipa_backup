////////////////////////////////////////////////////////////////////////////////
//
//	ESMCtrl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-24
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMCtrl.h"
#include "ESMFunc.h"

// #ifdef _DEBUG
// #define new DEBUG_NEW
// #undef THIS_FILE
// static char THIS_FILE[] = __FILE__;
// #endif

////////////////////////////////////////////////////////////
// Message maps

BEGIN_MESSAGE_MAP(CESMStatic, CStatic)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()	
END_MESSAGE_MAP()

BEGIN_MESSAGE_MAP(CESMEdit, CEdit)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_TIMER()	
END_MESSAGE_MAP()

BEGIN_MESSAGE_MAP(CESMBtn, CButton)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()	
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

BEGIN_MESSAGE_MAP(CESMCmb, CComboBox)
	ON_WM_MOUSEMOVE()
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()



////////////////////////////////////////////////////////////////////////////////////////////
// CESMCtrl

CESMCtrl::CESMCtrl()
{
	m_pBrush	= NULL;
	m_pHotBrush = NULL;
	m_pDisabledBrush = NULL;
	m_pSelectedBrush = NULL;
	m_pDeleteBrush	 = NULL;

	//SetDisabledColors(RGB(90,90,90), 0);
	SetDisabledColors(RGB_GRAYTEXT, RGB_BTNFACE);
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
	SetColors(	COLOR_BASIC_FG_3,
				COLOR_BASIC_BG_5,
				COLOR_WHITE,
				COLOR_BASIC_HBG);
#else
	SetColors(	COLOR_BASIC_FG_1,
				COLOR_BASIC_BG_1,
				//RGB(255,255,255),
				COLOR_BASIC_HFG,
				COLOR_BASIC_HBG);
#endif
	SetSelectedColors(COLOR_SELECT_FG, COLOR_SELECT_BG);
	SetDeleteColors(COLOR_DELETE_FG, COLOR_DELETE_BG);
	m_nStatus		= GRID_STATUS_NULL;
	m_nTimerID		= 1;
	m_iRolloverDelay = DSC_SELECT_ROW_DELAY;	

	//-- 2013-09-05 hongsu@esmlab.com
	//-- Font
	m_pFont = NULL;
	
}
CESMCtrl::~CESMCtrl()
{
	delete m_pBrush;
	m_pBrush = NULL;
	delete m_pHotBrush;
	m_pHotBrush = NULL;
	delete m_pDisabledBrush;
	m_pDisabledBrush = NULL;
	delete m_pSelectedBrush;
	m_pSelectedBrush = NULL;
	delete m_pDeleteBrush;
	m_pDeleteBrush = NULL;
	if(m_pFont)
	{
		delete m_pFont;
		m_pFont = NULL;
	}
}
void CESMCtrl::SetDisabledColors(	COLORREF DisabledFGColor, COLORREF DisabledBGColor)
{
	m_crDisabledFg = DisabledFGColor;
	m_crDisabledBg = DisabledBGColor;
	if(m_pDisabledBrush)
	{
		delete m_pDisabledBrush;
		m_pDisabledBrush = NULL;
	}

	//m_pDisabledBrush = new CBrush(DisabledBGColor);
}
void CESMCtrl::SetSelectedColors(const COLORREF FGColor, const COLORREF BGColor)
{
	m_crSelectFg = FGColor;
	m_crSelectBg = BGColor;
	if(m_pSelectedBrush)
	{
		delete m_pSelectedBrush;
		m_pSelectedBrush =NULL;
	}

	//m_pSelectedBrush = new CBrush(BGColor);
} 

void CESMCtrl::SetDeleteColors(const COLORREF FGColor, const COLORREF BGColor)
{
	m_crDeleteFg = FGColor;
	m_crDeleteBg = BGColor;
	if(m_pDeleteBrush)
	{
		delete m_pDeleteBrush;
		m_pDeleteBrush =NULL;
	}

	//m_pDeleteBrush = new CBrush(BGColor);
} 

void CESMCtrl::SetColors(const COLORREF FGColor, const COLORREF BGColor, const COLORREF HotFGColor, const COLORREF HotBGColor)
{
	m_crFg = FGColor;
	m_crBg = BGColor;
	m_crHotFg = HotFGColor;
	m_crHotBg = HotBGColor;
	if(m_pBrush)
	{
		delete m_pBrush;
		m_pBrush = NULL;
	}
	if(m_pHotBrush)
	{
		delete m_pHotBrush;
		m_pHotBrush = NULL;
	}

	//m_pBrush	= new CBrush(BGColor);
	//m_pHotBrush = new CBrush(HotBGColor);
}
void CESMCtrl::SetColorBg(COLORREF clr)
{
	SetColors(m_crFg, clr, m_crHotFg, m_crHotBg);
}
void CESMCtrl::SetColorFg(COLORREF clr)
{
	SetColors(clr, m_crBg, m_crHotFg, m_crHotBg);
}
void CESMCtrl::SetColorBgHot(COLORREF clr)
{
	SetColors(m_crFg, m_crBg, m_crHotFg, clr);
}
void CESMCtrl::SetColorFgHot(COLORREF clr)
{
	SetColors(m_crFg, m_crBg, clr, m_crHotBg);
}
void CESMCtrl::SetColorBgDisabled(COLORREF clr)
{
	SetDisabledColors(m_crDisabledFg, clr);
}
void CESMCtrl::SetColorFgDisabled(COLORREF clr)
{
	SetDisabledColors(clr, m_crDisabledBg);
}
//------------------------------------------------------------------------------
//! @function	CESMStatic
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
CESMStatic::CESMStatic(){}
CESMStatic::~CESMStatic(){}
HBRUSH CESMStatic::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	HBRUSH hbr;
	pDC->SetTextColor(m_crFg);
	pDC->SetBkColor(m_crBg);
	hbr = *m_pBrush;
	return hbr;	
}
void CESMStatic::ChangeFont(int nSize, CString strFont, int nFontType)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	SetFont(m_pFont);	
}

//------------------------------------------------------------------------------
//! @function	CESMEdit
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
CESMEdit::CESMEdit(){bFlag = FALSE;}
CESMEdit::~CESMEdit()
{
	if( m_pFont)
	{
		delete m_pFont;
		m_pFont = NULL;
	}
}
void CESMEdit::ChangeFont(int nSize, CString strFont, int nFontType, int nStrikeOut)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, nStrikeOut, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	SetFont(m_pFont);	
}
void CESMEdit::ReChangeFont(int nSize, CString strFont, int nFontType, int nStrikeOut)
{
	if (!m_pFont)
		return;

	if( m_pFont)
	{
		delete m_pFont;
		m_pFont = NULL;
	}

	m_pFont = new CFont();
 	m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, nStrikeOut, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
 		FIXED_PITCH|FF_MODERN, strFont);

	SetFont(m_pFont);	
}
void CESMEdit::ChangeFontItalic(int nSize, CString strFont, int nFontType,BOOL bItalic/* = FALSE*/)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, bItalic, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	SetFont(m_pFont);	
}
void CESMEdit::SetGridStatus(int nStatus)
{
	if(bFlag)
		m_nStatus = GRID_STATUS_DELETE;
	else
		m_nStatus = nStatus;

	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		//ESMLog(1,_T("Invalidate!!!"));
		Invalidate();	
	}
}

BOOL CESMEdit::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult) 
{
	if (!(message == WM_CTLCOLOREDIT || message == WM_CTLCOLORSTATIC))
		return CEdit::OnChildNotify(message,wParam,lParam,pLResult);

	HDC hdcChild = (HDC)wParam;
	if(!IsWindowEnabled()) // disabled
	{
		SetTextColor(hdcChild,	m_crDisabledFg);
		SetBkColor(hdcChild,	m_crDisabledBg);
		*pLResult = (LRESULT)(m_pDisabledBrush->GetSafeHandle());
	}
	else // normal
	{
		switch(m_nStatus)
		{
		case GRID_STATUS_OVER:
		case GRID_STATUS_OVER_SIBLING:
			{
				SetTextColor(hdcChild, m_crHotFg);
				SetBkColor(hdcChild, m_crHotBg);
				*pLResult = (LRESULT)(m_pHotBrush->GetSafeHandle());
				break;
			}
		case GRID_STATUS_SELECTED:
			{
				SetTextColor(hdcChild, m_crHotFg/*m_crSelectFg*/);
				SetBkColor(hdcChild, m_crHotBg/*m_crSelectBg*/);
				*pLResult = (LRESULT)(m_pSelectedBrush->GetSafeHandle());
				break;
			}
		case GRID_STATUS_DELETE:
			{
				SetTextColor(hdcChild, m_crDeleteFg);
				SetBkColor(hdcChild, m_crDeleteBg);
				*pLResult = (LRESULT)(m_pDeleteBrush->GetSafeHandle());
				break;
			}
		case GRID_STATUS_MKADD:
			{
				if(m_nIndex%2)
				{
					SetTextColor(hdcChild, COLOR_BASIC_FG_2);
					SetBkColor(hdcChild, COLOR_BASIC_BG_1);
				}
				else
				{
					SetTextColor(hdcChild, COLOR_BASIC_FG_2);
					SetBkColor(hdcChild, COLOR_BASIC_BG_1/*COLOR_BASIC_BG_2*/);
				}
				*pLResult = (LRESULT)(m_pDeleteBrush->GetSafeHandle());
				break;
			}
		default:
			{
				if(bFlag)
				{
					SetTextColor(hdcChild, m_crDeleteFg);
					SetBkColor(hdcChild, m_crDeleteBg);
					*pLResult = (LRESULT)(m_pDeleteBrush->GetSafeHandle());
					break;
				}
				else
				{
					SetTextColor(hdcChild, m_crFg);
					SetBkColor(hdcChild, m_crBg);
					*pLResult = (LRESULT)(m_pBrush->GetSafeHandle());
					break;
				}
				
			}
		}
	}

	return TRUE;
} 
void CESMEdit::OnMouseMove(UINT nFlags, CPoint point) 
{
    if (m_nStatus == GRID_STATUS_NULL)
    {
		m_nStatus = GRID_STATUS_OVER;
		Invalidate();
		SetTimer(m_nTimerID, m_iRolloverDelay, NULL);
		//-- 2013-09-06 hongsu@esmlab.com
		//-- Send Message To Sibling
		::SendMessage(GetParent()->GetSafeHwnd(), WM_DSC_INFO_OVER, m_ptGrid.y, (bool)true);
    }
	CEdit::OnMouseMove(nFlags, point);
}

void CESMEdit::OnTimer(UINT_PTR nIDEvent) 
{
	CPoint p(GetMessagePos());
	ScreenToClient(&p);
	CRect rect;
    GetClientRect(rect);

	if (m_nStatus == GRID_STATUS_OVER)
	{
		if (!rect.PtInRect(p))
		{
			m_nStatus = GRID_STATUS_NULL;
			KillTimer(m_nTimerID);
			Invalidate();
			//-- 2013-09-06 hongsu@esmlab.com
			//-- Send Message To Sibling
			::SendMessage(GetParent()->GetSafeHwnd(), WM_DSC_INFO_OVER, (int) m_ptGrid.y, (bool)false);
		}		
	}

	CEdit::OnTimer(nIDEvent);
}
void CESMEdit::OnLButtonUp(UINT nFlags, CPoint point)
{
	::SendMessage(GetParent()->GetSafeHwnd(), WM_DSC_INFO_GRID_CLICK, (int) m_ptGrid.x, (int) m_ptGrid.y);
	CEdit::OnLButtonUp(nFlags, point);
}

void CESMEdit::OnRButtonUp(UINT nFlags, CPoint point)
{
	::SendMessage(GetParent()->GetSafeHwnd(), WM_DSC_INFO_GRID_RCLICK, (int) m_ptGrid.x, (int) m_ptGrid.y);
	//CEdit::OnRButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------
//! @function	CESMBtn
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
CESMBtn::CESMBtn()
{
	// Initialize GDI+.
	GdiplusStartupInput gdiplusStartupInput;
	GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, NULL);
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
	SetGradientColors(	255, COLOR_BASIC_BG_5,	// default upper color
						255, COLOR_BASIC_BG_4,	// default lower color
						255, COLOR_BASIC_HBG);	// default lower HOT color
#else
	SetGradientColors(	255, RGB(28,28,28)/*RGB(0,0,0)*//*COLOR_BASIC_BG_3*/,	// default upper color
						255, RGB(28,28,28)/*COLOR_BASIC_BG_2*/,	// default lower color
						255, RGB(6,6,6)/*COLOR_BASIC_HBG*/);	// default lower HOT color
#endif
}
CESMBtn::~CESMBtn()
{
	GdiplusShutdown(m_gdiplusToken);
}
void CESMBtn::ChangeFont(int nSize, CString strFont, int nFontType)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	SetFont(m_pFont);	
}
void CESMBtn::SetGradientColors(
	BYTE alphaUpper,	COLORREF rgbUpper,
	BYTE alphaLower,	COLORREF rgbLower,
	BYTE alphaLowerHot, COLORREF rgbLowerHot)
{
	m_clrUpper = Color::MakeARGB(alphaUpper,
		GetRValue(rgbUpper),
		GetGValue(rgbUpper),
		GetBValue(rgbUpper));
	m_clrLower		= Color::MakeARGB(alphaLower,
		GetRValue(rgbLower),
		GetGValue(rgbLower),
		GetBValue(rgbLower));
	m_clrLowerHot	= Color::MakeARGB(alphaLowerHot,
		GetRValue(rgbLowerHot),
		GetGValue(rgbLowerHot),
		GetBValue(rgbLowerHot));
}
void CESMBtn::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	UINT				state(lpDIS->itemState);
	CString				btnText;
	Graphics			graphics(lpDIS->hDC);
	RECT				rctBgContent;
	Rect				clientRect;
	CRect				txtRect(0,0,0,0);
	LinearGradientBrush *plinGrBrush = NULL;

	UINT partID;
	UINT stateID;

	Color	clrDisabled = Color::MakeARGB(255,	
		GetRValue(m_crDisabledBg),
		GetGValue(m_crDisabledBg),
		GetBValue(m_crDisabledBg));
	Color	*pclrLower = NULL;

	partID	= DFC_BUTTON;
	stateID	= (state & ODS_SELECTED) ? DFCS_BUTTONPUSH|DFCS_FLAT|DFCS_PUSHED: DFCS_BUTTONPUSH|DFCS_FLAT;
	if(state & ODS_DISABLED) stateID = DFCS_BUTTONPUSH|DFCS_FLAT|DFCS_INACTIVE;
	DrawFrameControl(lpDIS->hDC, &lpDIS->rcItem, partID,stateID);
	memcpy(&rctBgContent, &lpDIS->rcItem, sizeof(RECT));
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Non Draw Round
	//InflateRect(&rctBgContent, -GetSystemMetrics(SM_CXEDGE), -GetSystemMetrics(SM_CYEDGE));

	clientRect.X = rctBgContent.left;
	clientRect.Y = rctBgContent.top;
	clientRect.Width = rctBgContent.right - rctBgContent.left;
	clientRect.Height = rctBgContent.bottom - rctBgContent.top;

	if (state & ODS_SELECTED)
	{
		if(m_nStatus == GRID_STATUS_OVER ) 
			pclrLower = &m_clrLowerHot;
		else 
			pclrLower = &m_clrLower;
		plinGrBrush = new LinearGradientBrush(clientRect, *pclrLower, m_clrUpper, 90);
	}
	else if( state & ODS_DISABLED )
	{
		pclrLower	= &clrDisabled;
		plinGrBrush = new LinearGradientBrush(clientRect, m_clrUpper, *pclrLower, 90);
	}
	else
	{
		if(m_nStatus == GRID_STATUS_OVER ) 
			pclrLower = &m_clrLowerHot ;
		else
			pclrLower = &m_clrLower;
		plinGrBrush = new LinearGradientBrush(clientRect, m_clrUpper, *pclrLower, 90);
	}

	graphics.FillRectangle(plinGrBrush, clientRect );

	// Draw the text
	GetWindowText(btnText);
	if(!btnText.IsEmpty())
	{
		COLORREF	clrTxt, clrOrg;
		int			bkModeOld;
		// select fg color
		if (state & ODS_DISABLED)
			clrTxt = m_crDisabledFg;
		else if (m_nStatus == GRID_STATUS_OVER)
			clrTxt = m_crHotFg;
		else
			clrTxt = m_crFg;

		// calculate bounding rect cause we obviously want to make
		// it multi lined
		DrawText(lpDIS->hDC, btnText, btnText.GetLength(), &txtRect, DT_CALCRECT | DT_CENTER);

		int txtWidth = txtRect.Width();
		int txtHight = txtRect.Height();

		txtRect.left	= clientRect.X + (clientRect.Width - txtWidth)/2;
		txtRect.right	= txtRect.left + txtWidth;
		txtRect.top		= clientRect.Y + (clientRect.Height - txtHight)/2;
		txtRect.bottom	= txtRect.top + txtHight;

		// discard parts not in clientRect
		txtRect.IntersectRect(&rctBgContent, &txtRect);
		// txt color
		clrOrg = SetTextColor(lpDIS->hDC, clrTxt);
		// transparent bg
		bkModeOld = SetBkMode(lpDIS->hDC, TRANSPARENT);

		// Draw text
		DrawText(lpDIS->hDC, btnText, btnText.GetLength(), &txtRect, DT_CENTER);
		SetBkMode(lpDIS->hDC, bkModeOld);
		SetTextColor(lpDIS->hDC, clrOrg);
	}
	delete plinGrBrush;
	plinGrBrush = NULL;
}
void CESMBtn::PreSubclassWindow() 
{
	ModifyStyle(0, BS_OWNERDRAW);	
	CButton::PreSubclassWindow();   
}
void CESMBtn::OnMouseMove(UINT nFlags, CPoint point) 
{
	int nID = GetDlgCtrlID();
	if(nID == IDC_BTN_SCROLL)
	{
		CPoint pos;
		GetCursorPos(&pos);
		::SendMessage(GetParent()->GetSafeHwnd(), WM_DSC_INFO_BTN_MOVE,  (int) nID, (int) pos.y);
	}
	else
	{
		CPoint p(GetMessagePos());
		ScreenToClient(&p);
		CRect rect;
		GetClientRect(rect);
		if (m_nStatus == GRID_STATUS_NULL)
		{
			m_nStatus = GRID_STATUS_OVER;
			Invalidate();
			SetTimer(m_nTimerID, m_iRolloverDelay, NULL);
		}
	}
	
	CButton::OnMouseMove(nFlags, point);
}
void CESMBtn::OnTimer(UINT_PTR nIDEvent) 
{
	CPoint p(GetMessagePos());
	ScreenToClient(&p);
	CRect rect;
	GetClientRect(rect);
	if (!rect.PtInRect(p))
	{
		m_nStatus = GRID_STATUS_NULL;
		KillTimer(m_nTimerID);
		Invalidate();
	}
	CButton::OnTimer(nIDEvent);
}
void CESMBtn::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// ownerdrawn buttons responds to doubleclicks rather than fast
	// singleclicks as the standard buttoms does, fix:
	PostMessage(WM_LBUTTONDOWN,0, MAKELPARAM(point.x, point.y));
}
void CESMBtn::OnLButtonUp(UINT nFlags, CPoint point)
{
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Send Message To Parent
	int nID = GetDlgCtrlID();
	::SendMessage(GetParent()->GetSafeHwnd(), WM_DSC_INFO_BTN_CLICK, (int) nID, NULL);
	CButton::OnLButtonUp(nFlags, point);
}


//------------------------------------------------------------------------------
//! @function	CESMCmb		
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
CESMCmb::CESMCmb(){}
CESMCmb::~CESMCmb(){}
HBRUSH CESMCmb::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CComboBox::OnCtlColor(pDC, pWnd, nCtlColor);
	if( nCtlColor == CTLCOLOR_LISTBOX || nCtlColor == CTLCOLOR_EDIT || nCtlColor == CTLCOLOR_MSGBOX )
    {
        if (m_nStatus == GRID_STATUS_OVER)
        {
                pDC->SetTextColor(m_crHotFg);
                pDC->SetBkColor(m_crHotBg);
                hbr = *m_pHotBrush;
        }
        else
        {
                pDC->SetTextColor(m_crFg);
                pDC->SetBkColor(m_crBg);
                hbr = *m_pBrush;
        }
    }
	return hbr;
}
BOOL CESMCmb::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult) 
{
    if (message != WM_CTLCOLOREDIT)
             return CComboBox::OnChildNotify(message,wParam,lParam,pLResult);

    HDC hdcChild = (HDC)wParam;
    if( m_nStatus == GRID_STATUS_OVER)
    {
        SetTextColor(hdcChild, m_crHotFg);
        SetBkColor(hdcChild, m_crHotBg);
        *pLResult = (LRESULT)(m_pHotBrush->GetSafeHandle());
    }
    else
    {
        SetTextColor(hdcChild, m_crFg);
        SetBkColor(hdcChild, m_crBg);
        *pLResult = (LRESULT)(m_pBrush->GetSafeHandle());
    }
    return TRUE;
}
void CESMCmb::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (m_nStatus != GRID_STATUS_OVER)
	{
		m_nStatus = GRID_STATUS_OVER;
		Invalidate();
		SetTimer(m_nTimerID, m_iRolloverDelay, NULL);
	}
	CComboBox::OnMouseMove(nFlags, point);
}
void CESMCmb::OnTimer(UINT_PTR nIDEvent) 
{
    CPoint p(GetMessagePos());
    ScreenToClient(&p);
    CRect rect;
    GetClientRect(rect);
    if (!rect.PtInRect(p))
    {
        m_nStatus = GRID_STATUS_NULL;
        KillTimer(m_nTimerID);
        Invalidate();
    }	
	CComboBox::OnTimer(nIDEvent);
}




void CESMBtn::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nID = GetDlgCtrlID();
	::SendMessage(GetParent()->GetSafeHwnd(), WM_DSC_INFO_BTN_DOWN, (int) nID, NULL);
	CButton::OnLButtonDown(nFlags, point);
}
