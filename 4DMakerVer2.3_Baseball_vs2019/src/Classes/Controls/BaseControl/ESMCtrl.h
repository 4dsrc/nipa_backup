////////////////////////////////////////////////////////////////////////////////
//
//	ESMCtrl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-24
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

/// MESSAGE
#define WM_DSC_INFO_OVER		WM_USER + 0x9000
#define WM_DSC_INFO_BTN_CLICK	WM_USER + 0x9001
#define WM_DSC_INFO_GRID_CLICK	WM_USER + 0x9002
#define WM_DSC_INFO_GRID_RCLICK	WM_USER + 0x9003
#define WM_DSC_INFO_BTN_DOWN	WM_USER + 0x9004
#define WM_DSC_INFO_BTN_MOVE	WM_USER + 0x9005

/// CONTROL ID
#define IDC_BTN_FOLDER_ADJ		0x9000
#define IDC_BTN_FOLDER_PROP		0x9001
#define IDC_BTN_HEADER_ID		0x9002
#define IDC_BTN_SCROLL			0x9003
#define IDC_BTN_TESTING			0X9004

/// VALUES
#define COLOR_WHITE			RGB(230,230,230)
#define COLOR_YELLOW		RGB(234,194, 70)
#define COLOR_GREEN			RGB(100,145, 76)
#define COLOR_BLUE			RGB( 30, 30,234)
#define COLOR_RED			RGB(234, 30, 30)
#define COLOR_BLACK			RGB(  0,  0,  0)
#define COLOR_BASIC_FG_1	RGB(185,185,185)
#define COLOR_BASIC_FG_2	RGB(180,180,180)
#define COLOR_BASIC_FG_3	RGB(210,210,210)
#define COLOR_BASIC_FG_4	RGB(215,215,215)
#define COLOR_BASIC_BG_1	RGB(0x2C,0x2C,0x2C)/*RGB( 89, 89, 89)*/
#define COLOR_BASIC_BG_2	RGB( 83, 83, 83)
#define COLOR_BASIC_BG_3	RGB(0x16,0x16,0x16)/*RGB(120,120,120)*/
#define COLOR_BASIC_BG_4	RGB(132,132,132)
#define COLOR_BASIC_BG_5	RGB(140,140,140)
#define COLOR_BASIC_BG_6	RGB(135,135,135)
#define COLOR_BASIC_BG_DARK RGB( 63, 63, 63)
#define COLOR_BASIC_HFG		RGB(255,210,210)
#define COLOR_BASIC_HBG		RGB(128,128,128)
#define COLOR_BASIC_RED		RGB(220, 40, 30)
#define COLOR_SELECT_FG		RGB(255, 255, 255)
#define COLOR_SELECT_BG		RGB( 48, 48, 48)
#define COLOR_DELETE_FG		RGB(255, 0, 0)
#define COLOR_DELETE_BG		RGB(0, 0, 0)

#define COLOR_TL_INFO_TOP	RGB( 77, 97, 95)
#define COLOR_TL_INFO_BODY	RGB(190,215,212)

// KEY VALUE
#define ESM_KEY_DEL					46
#define ESM_KEY_UP					38
#define ESM_KEY_DOWN				40
#define ESM_KEY_RIGHT				39
#define ESM_KEY_LEFT				37
#define ESM_KEY_ENTER				13

#define RGB_GRAYTEXT		::GetSysColor(COLOR_GRAYTEXT)
#define RGB_BTNFACE			::GetSysColor(COLOR_BTNFACE)

enum GRID_STATUS
{
	GRID_STATUS_NULL			=	0,
	GRID_STATUS_OVER			,
	GRID_STATUS_OVER_SIBLING	,
	GRID_STATUS_SELECTED		,
	GRID_STATUS_DELETE			,
	GRID_STATUS_MKADD			,

};


/// TIME
#define DSC_SELECT_ROW_DELAY		5


#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")

using namespace Gdiplus;

class CESMCtrl
{
public:
	CESMCtrl();
	virtual ~CESMCtrl();

protected:
	CFont *m_pFont;
	CPoint m_ptGrid;
	int m_nColumn;

public:
	//-- 2013-09-05 hongsu@esmlab.com
	virtual void ChangeFont(int nSize, CString strFont, int nFontType){};
	void SetPos(CPoint pt)			{ m_ptGrid = pt;}
	void ChangeRow(int nLine)	{ m_ptGrid.y = nLine;}
	int GetRow()				{ return m_ptGrid.y;}
	int GetStatus() {return m_nStatus;}

	void SetColors(const COLORREF FGColor, const COLORREF BGColor, const COLORREF HotFGColor, const COLORREF HotBGColor);
	void SetDisabledColors( const COLORREF DisabledFGColor = RGB_GRAYTEXT,
		const COLORREF DisabledBGColor = RGB_BTNFACE);
	void SetColorBg(COLORREF clr);
	void SetColorFg(COLORREF clr);
	void SetColorBgHot(COLORREF clr);
	void SetColorFgHot(COLORREF clr);
	void SetColorBgDisabled(COLORREF clr);
	void SetColorFgDisabled(COLORREF clr);
	void SetSelectedColors(const COLORREF FGColor, const COLORREF BGColor);
	void SetDeleteColors(const COLORREF FGColor, const COLORREF BGColor);
	void SetRolloverDelay( UINT mSeconds ) { m_iRolloverDelay = mSeconds; };


protected:		
	COLORREF	m_crFg, m_crBg, m_crHotFg, m_crHotBg;
	COLORREF	m_crSelectFg, m_crSelectBg;
	COLORREF	m_crDeleteFg, m_crDeleteBg;
	COLORREF	m_crDisabledFg, m_crDisabledBg;
	UINT		m_nTimerID, m_iRolloverDelay;
	int			m_nStatus;
	CBrush*		m_pBrush;
	CBrush*		m_pHotBrush;
	CBrush*		m_pDisabledBrush;
	CBrush*		m_pSelectedBrush;
	CBrush*		m_pDeleteBrush;
	bool		m_bEnableHot;
};

//------------------------------------------------------------------------------
//! @function	STATIC	
//------------------------------------------------------------------------------
class CESMStatic : public CStatic, public CESMCtrl
{	
public:
	CESMStatic();
	virtual ~CESMStatic();
	virtual void ChangeFont(int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);	
public:
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);	
	DECLARE_MESSAGE_MAP()
};

//------------------------------------------------------------------------------
//! @function	EDIT
//------------------------------------------------------------------------------
class CESMEdit : public CEdit, public CESMCtrl
{	
public:
	BOOL bFlag;
	int	m_nIndex;
public:
	CESMEdit();
	virtual ~CESMEdit();
	virtual void ChangeFont(int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL, int nStrikeOut=0);
	virtual void ReChangeFont(int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL, int nStrikeOut=0);
	virtual void ChangeFontItalic(int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL,BOOL bItalic = FALSE);

	virtual BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult);
	void SetGridStatus(int nStatus);

	BOOL IsOverControl();
protected:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()
};

//------------------------------------------------------------------------------
//! @function	BUTTON
//------------------------------------------------------------------------------
class CESMBtn : public CButton, public CESMCtrl
{
public:
	CESMBtn();
	virtual ~CESMBtn();
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
	virtual void ChangeFont(int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);
	void SetGradientColors(	BYTE alphaUpper,	COLORREF rgbUpper,
		BYTE alphaLower,	COLORREF rgbLower,
		BYTE alphaLowerHot,	COLORREF rgbLowerHot);

protected:	
	ULONG_PTR	m_gdiplusToken;	
	Color		m_clrUpper, m_clrLower, m_clrLowerHot;

	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	virtual void PreSubclassWindow();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};


//------------------------------------------------------------------------------
//! @function	COMBO
//------------------------------------------------------------------------------
class CESMCmb : public CComboBox, public CESMCtrl
{
public:
	CESMCmb();
	virtual ~CESMCmb();

	virtual BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult);
protected:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	DECLARE_MESSAGE_MAP()
};

