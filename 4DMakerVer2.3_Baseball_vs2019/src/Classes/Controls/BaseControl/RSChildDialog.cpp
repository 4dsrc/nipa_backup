//
//	Class:		CRSChildDialog

#include "stdafx.h"
#include "RSChildDialog.h"
#include "wingdi.h"
#include "ESMFunc.h"

#include "cv.h"
#include "highgui.h"

#ifdef _WIN64
//#include "CvvImage.h"
#endif


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const int text_margin = 5;

CRSChildDialog::CRSChildDialog(CWnd* pParent /*=NULL*/)																	{m_bDragMove = FALSE;	m_bLoaded = FALSE;}
CRSChildDialog::CRSChildDialog(UINT uResourceID, CWnd* pParent) : CDialogEx(uResourceID, pParent)				{m_bDragMove = FALSE;	m_bLoaded = FALSE;}
CRSChildDialog::CRSChildDialog(LPCTSTR pszResourceID, CWnd* pParent)	: CDialogEx(pszResourceID, pParent) {m_bDragMove = FALSE;	m_bLoaded = FALSE;}
CRSChildDialog::~CRSChildDialog()
{	
	TRACE(_T("Destroy:CRSChildDialog Start\n"));
	TRACE(_T("Destroy:CRSChildDialog End\n"));
}

void CRSChildDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);	
}

BEGIN_MESSAGE_MAP(CRSChildDialog, CDialogEx)
	//{{AFX_MSG_MAP(CRSChildDialog)
	ON_WM_ERASEBKGND()	
	//}}AFX_MSG_MAP	
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

BOOL CRSChildDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();		
	SetLoad(TRUE);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

BOOL CRSChildDialog::OnEraseBkgnd(CDC* pDC) { return FALSE;}
//------------------------------------------------------------------------------ 
//! @brief		DrawRoundDialogs
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CRSChildDialog::DrawRoundDialogs(CDC* pDC, BOOL bFull )
{
	CRect rect;
	GetClientRect(&rect);
	CRgn rgn;
	if(bFull)	rgn.CreateRoundRectRgn(0, 0, rect.right+1, rect.bottom-1, 13, 13);
	else		rgn.CreateRoundRectRgn(0, 0, rect.right+1, rect.bottom+3, 4, 4);
	SetWindowRgn(rgn, true);

	Invalidate(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawImage
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
static const int size_margin = 15;
void CRSChildDialog::DrawImage(CDC* pDC, CString strFile, CRect rect, BOOL bProtectRatio)
{
	DrawImage(pDC, strFile, rect.left, rect.top, rect.Width(), rect.Height(), bProtectRatio);
}

void CRSChildDialog::DrawImage(CDC* pDC, CString strFile, int x, int y, int width, int height, BOOL bProtectRatio)
{
	//CvvImage cImage;	
	IplImage *pImgSrc, *pImgResize;
	
	CStringA ansiString(strFile);	
	//pImgSrc = cvLoadImage(ansiString);

	//if(!pImgSrc)
	{
// 		CString strMsg;
// 		strMsg.Format(_T("Unload File [%s]"),strFile);
// 		ESMEvent* pMsg	= new ESMEvent;
// 		pMsg->message	= WM_RS_DESTROY;
// 		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
		//AfxMessageBox(strMsg);
		return;
	}

	//-- 2011-06-01 hongsu.jung
	if(bProtectRatio)
	{
		CRect rect;
		int nBaseW = pImgSrc->width;
		int nBaseH = pImgSrc->height;

		//-- guarantee Ratio
		int nRatioW, nRatioH;
		nRatioW = (int)(((float)nBaseW/(float)nBaseH)*(float)height);
		nRatioH = (int)(((float)nBaseH/(float)nBaseW)*(float)width);
		if( width > nRatioW )		
		{
			rect.top = y + size_margin;
			rect.left = x + width/2 - nRatioW/2;			
			rect.right = x + width/2 + nRatioW/2;
			rect.bottom = y + height - size_margin;
		}
		else
		{	
			rect.left	= size_margin;
			rect.right = x + width - size_margin;
			rect.top = y + height/2 - nRatioH/2;
			rect.bottom = y + height/2 + nRatioH/2;
		}

		x = rect.left;
		y = rect.top;
		width = rect.Width();
		height = rect.Height();
	}

	//pImgResize = cvCreateImage( cvSize(width, height), pImgSrc->depth, pImgSrc->nChannels );
	
	//cvResize(pImgSrc, pImgResize);
	//cImage.CopyOf(pImgResize ,8);
	//cImage.Show(pDC->GetSafeHdc(), x, y, width, height);

	//cvReleaseImage(&pImgSrc);
	//cvReleaseImage(&pImgResize);
	//cImage.Destroy();
}
//------------------------------------------------------------------------------ 
//! @brief		DrawImageBase
//! @date		2010-06-08
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CRSChildDialog::DrawImageBase(CDC* pDC, CString strFile, int x, int y)
{
	//CvvImage cImage;	
	//IplImage *pImgSrc;
	
	CStringA ansiString(strFile);	
	//pImgSrc = cvLoadImage(ansiString);

	//if(!pImgSrc)
	{
// 		CString strMsg;
// 		strMsg.Format(_T("Unload File [%s]"),strFile);
// 		RSEvent* pMsg	= new RSEvent;
// 		pMsg->message	= WM_RS_DESTROY;
// 		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
		//AfxMessageBox(strMsg);
		//return;
	}

	//int width = pImgSrc->width;
	//int height = pImgSrc->height;

	//cImage.CopyOf(pImgSrc ,8);
	//cImage.Show(pDC->GetSafeHdc(), x, y, width, height);
	//cvReleaseImage(&pImgSrc);
	//cImage.Destroy();
}
//------------------------------------------------------------------------------ 
//! @brief		DrawGraphicsImage
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CRSChildDialog::DrawGraphicsImage(CDC* pDC, CString strFile, Gdiplus::Rect rect)
{
	//-- Draw Dummy Image			
	//-- 2011-05-16 hongsu.jung		
	Image Img(strFile);
	Graphics gc(pDC->GetSafeHdc());
	gc.DrawImage(&Img, rect);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawGraphicsImage
//! @date		2010-05-30
//! @author	hongsu.jung	
//------------------------------------------------------------------------------ 
void CRSChildDialog::DrawGraphicsImage(CDC* pDC, CString strFile, int x, int y, int w, int h)
{
	Rect rect;
	rect.X = x;
	rect.Y = y;
	rect.Width = w;
	rect.Height = h;
	DrawGraphicsImage(pDC, strFile, rect);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawGraphicsImage
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CRSChildDialog::DrawGraphicsImage(CDC* pDC, CString strFile, CRect rect)
{
	Rect rt;
	rt.X = rect.left;
	rt.Y = rect.top;
	rt.Width = rect.Width();
	rt.Height = rect.Height();
	DrawGraphicsImage(pDC, strFile, rt);
}
//------------------------------------------------------------------------------ 
//! @brief		DrawDlgTitle
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
static const int title_x = 8;
static const int title_y = 7;

/*
#define GetRValue( rgb )    ( ( BYTE )( rgb ) )
#define GetGValue( rgb )    ( ( BYTE )( ( ( WORD )( rgb ) ) >> 8 ) )
#define GetBValue( rgb )    ( ( BYTE )( ( rgb ) >> 16 ) )
*/

void CRSChildDialog::DrawDlgTitle(CDC* pDC, CString strTitle, COLORREF color)
{
	Graphics gc(pDC->GetSafeHdc());
	Gdiplus::Font F1(_T("SS_SJ_DTV_uni_20090209"), 12, FontStyleBold, UnitPixel);
	PointF P1((Gdiplus::REAL)title_x, (Gdiplus::REAL)title_y);
	SolidBrush B1(Color(180, GetRValue(color),  GetGValue(color), GetBValue(color)));
	gc.DrawString(strTitle,-1,&F1,P1,&B1);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawDlgTitle
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CRSChildDialog::DrawStr(CDC* pDC, int nSize, CString strName, int x, int y, COLORREF color)
{
	int nPixelSize = 0, nStyle = 0, nAlpha = 0;

	switch(nSize)	
	{
	case STR_SIZE_SMALL:
		nAlpha = 255;
		nPixelSize = 10;
		nStyle = FontStyleRegular;
		break;
	case STR_SIZE_TOOLBAR:
		nAlpha = 180;
		nPixelSize = 12;
		nStyle = FontStyleBold;
		break;
	case STR_SIZE_MIDDLE:
		nAlpha = 192;
		nPixelSize = 12;
		nStyle = FontStyleRegular;
		break;
	case STR_SIZE_BUTTON:
		nAlpha = 192;
		nPixelSize = 10;
		nStyle = FontStyleBold;
		break;	
	case STR_SIZE_BIG:
		nAlpha = 64;
		nPixelSize = 12;
		nStyle = FontStyleBold;
		break;
	case STR_SIZE_TEXT:
		nAlpha = 192;
		nPixelSize = 14;
		nStyle = FontStyleRegular;
		break;
	case STR_SIZE_TEXT_BOLD:
		nAlpha = 192;
		nPixelSize = 14;
		nStyle = FontStyleBold;
		break;
	case STR_SIZE_TEXT_SMALL:
		nAlpha = 192;
		nPixelSize = 11;
		nStyle = FontStyleBold;
		break;
	case STR_SIZE_TEXT_POPUP:
		nAlpha = 192;
		nPixelSize = 16;
		nStyle = FontStyleRegular;
		break;
	}	
	Graphics gc(pDC->GetSafeHdc());
	Gdiplus::Font F1(_T("SS_SJ_DTV_uni_20090209"), (Gdiplus::REAL)nPixelSize, nStyle, UnitPixel);
	PointF P1((Gdiplus::REAL)x, (Gdiplus::REAL)y-2);
	SolidBrush B1(Color(nAlpha, GetRValue(color),  GetGValue(color), GetBValue(color) ));
	gc.DrawString(strName,-1,&F1,P1,&B1);
}


void CRSChildDialog::DrawStr(CDC* pDC, int nSize, CString strName, CRect rect, COLORREF color, BOOL bMiddle)
{
	int nPixelSize = 0, nStyle = 0, nAlpha = 0;

	switch(nSize)	
	{
	case STR_SIZE_SMALL:
		nAlpha = 255;
		nPixelSize = 10;
		nStyle = FontStyleRegular;
		break;
	case STR_SIZE_TOOLBAR:
		nAlpha = 180;
		nPixelSize = 12;
		nStyle = FontStyleBold;
		break;
	case STR_SIZE_MIDDLE:
		nAlpha = 192;
		nPixelSize = 12;
		nStyle = FontStyleRegular;
		break;
	case STR_SIZE_BUTTON:
		nAlpha = 192;
		nPixelSize = 10;
		nStyle = FontStyleBold;
		break;	
	case STR_SIZE_BIG:
		nAlpha = 64;
		nPixelSize = 12;
		nStyle = FontStyleBold;
		break;
	case STR_SIZE_TEXT:
		nAlpha = 192;
		nPixelSize = 14;
		nStyle = FontStyleRegular;
		break;
	case STR_SIZE_TEXT_BOLD:
		nAlpha = 192;
		nPixelSize = 14;
		nStyle = FontStyleBold;
		break;
	case STR_SIZE_TEXT_SMALL:
		nAlpha = 192;
		nPixelSize = 11;
		nStyle = FontStyleRegular;
		break;
	case STR_SIZE_TEXT_POPUP:
		nAlpha = 192;
		nPixelSize = 16;
		nStyle = FontStyleRegular;
		break;
	}	
	Graphics gc(pDC->GetSafeHdc());
	Gdiplus::Font F1(_T("SS_SJ_DTV_uni_20090209"), (Gdiplus::REAL)nPixelSize, nStyle, UnitPixel);
	CSize szStr;
	szStr = pDC->GetOutputTextExtent(strName);

	//-- Check String Size
	if(szStr.cx > rect.Width())
	{
		while(1)
		{
			strName = strName.Left(strName.GetLength()-1);
			szStr = pDC->GetOutputTextExtent(strName);
			if(szStr.cx <= rect.Width()-12)
			{
				strName += _T("...");
				szStr = pDC->GetOutputTextExtent(strName);
				break;
			}
		}
	}

	int x,y;
	if(bMiddle)		x = rect.left + (rect.Width() - szStr.cx)/2;
	else				x = rect.left + text_margin;	
	y = rect.top + (rect.Height() - szStr.cy)/2 + 1;

	PointF P1((Gdiplus::REAL)x, (Gdiplus::REAL)y);
	SolidBrush B1(Color(nAlpha, GetRValue(color),  GetGValue(color), GetBValue(color) ));
	gc.DrawString(strName,-1,&F1,P1,&B1);
}

void CRSChildDialog::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if(m_bDragMove)
	{
		CRect rtClient;
		GetClientRect(rtClient);	   
		rtClient.bottom = rtClient.top + 30;
		if(rtClient.PtInRect(point))
			SendMessage(WM_SYSCOMMAND, 0xF012, 0); 
	}
	CDialogEx::OnLButtonDown(nFlags, point);
} // End of OnLButtonDown

//------------------------------------------------------------------------------ 
//! @brief		CheckWindowRect
//! @date		2011-8-17
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSChildDialog::CheckWindowRect(int nX, int nY, int nW, int nH, CWnd * pWnd)
{
	HMONITOR hMon = MonitorFromPoint(CPoint(nX, nY), MONITOR_DEFAULTTONEAREST);
	MONITORINFO mi;
	mi.cbSize = sizeof(mi);
	GetMonitorInfo(hMon, &mi);

	CRect rectMon = mi.rcWork;
	
	int nX2 = nX + nW;
	int nY2 = nY + nH;

	//-- X Pos
	if(nX2 - nW < rectMon.left)			nX = rectMon.left;
	else if(nX + nW > rectMon.right)	nX = rectMon.right - nW;

	//-- Y Pos
	if(nY2 - nH < rectMon.top)			nY = rectMon.top;
	else if(nY + nH > rectMon.bottom)	nY = rectMon.bottom - nH;


	//-- 2013-03-09 hongsu.jung
	//-- 
	TRACE(_T("MoveWindow X:%d, Y:%d, W:%d H:%d\n"), nX, nY, nW, nH);

	if(pWnd)
		pWnd->MoveWindow(nX, nY, nW, nH);
	else
		MoveWindow(nX, nY, nW, nH);
}