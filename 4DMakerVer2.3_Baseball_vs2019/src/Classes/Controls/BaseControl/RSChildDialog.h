#pragma once

class CRSChildDialog : public CDialogEx
{
public:
	CRSChildDialog(CWnd* pParent = NULL);	
	CRSChildDialog(UINT uResourceID, CWnd* pParent = NULL);
	CRSChildDialog(LPCTSTR pszResourceID, CWnd* pParent = NULL);

protected:
	virtual ~CRSChildDialog();
	
	//{{AFX_DATA(CRSChildDialog)		
	//}}AFX_DATA
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRSChildDialog)
	//}}AFX_VIRTUAL

	enum
	{
		STR_SIZE_SMALL = 0,
		STR_SIZE_MIDDLE,
		STR_SIZE_BUTTON,
		STR_SIZE_TOOLBAR,
		STR_SIZE_BIG,
		STR_SIZE_TEXT,
		STR_SIZE_TEXT_BOLD,
		STR_SIZE_TEXT_SMALL,
		STR_SIZE_TEXT_POPUP,
	};

protected:
	BOOL m_bLoaded;		
	BOOL m_bDragMove;

	void SetLoad(BOOL b)		{ m_bLoaded = b; }
	BOOL ISSet()				{ return m_bLoaded ; }
	// Generated message map functions
	//{{AFX_MSG(CRSChildDialog)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()

public:
	void SetDragMove(BOOL b)	{ m_bDragMove =b; }
	//-- 2011-05-30 hongsu.jung
	void DrawRoundDialogs(CDC* pDC, BOOL bFull = FALSE);
	void DrawDlgTitle(CDC* pDC, CString strTitle, COLORREF color = RGB(255,255,255));
	void DrawStr(CDC* pDC, int nSize, CString strName, int x, int y, COLORREF color = RGB(240,240,240));
	void DrawStr(CDC* pDC, int nSize, CString strName, CRect rect, COLORREF color = RGB(240,240,240), BOOL bMiddle = TRUE);
	//-- 2011-05-30 hongsu.jung
	void DrawImage(CDC* pDC, CString strFile, int x, int y, int width, int height, BOOL bProtectRatio = FALSE);
	void DrawImage(CDC* pDC, CString strFile, CRect rect, BOOL bProtectRatio = FALSE);
	//-- 2011-06-08 hongsu.jung
	void DrawImageBase(CDC* pDC, CString strFile, int x, int y);
	void DrawImageBase(CDC* pDC, CString strFile, CPoint pt) { DrawImageBase(pDC, strFile, pt.x, pt.y); }
	//-- 2011-06-08 hongsu.jung
	void DrawGraphicsImage(CDC* pDC, CString strFile, int x, int y, int w, int h);
	void DrawGraphicsImage(CDC* pDC, CString strFile, CRect rect);
	void DrawGraphicsImage(CDC* pDC, CString strFile, Gdiplus::Rect rect);
	void DrawGraphicsImage(CDC* pDC, CString strFile, CPoint pt, CSize sz) { DrawGraphicsImage(pDC, strFile, pt.x, pt.y, sz.cx, sz.cy); }

	//-- 2011-8-17 Lee JungTaek
	//-- Check Window Rect
	void CheckWindowRect(int nX, int nY, int nW, int nH, CWnd * pWnd = NULL);
};
