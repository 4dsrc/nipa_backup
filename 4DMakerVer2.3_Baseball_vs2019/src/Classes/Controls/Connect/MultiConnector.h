#pragma once
#include "RSFunc.h"
#include "MultiPTPMgr.h"
#include "PTPMgr.h"
#include "globalindex.h"


// CMultiConnector

class CMultiConnector : public CWinThread
{
	DECLARE_DYNCREATE(CMultiConnector)

public:
	CMultiConnector();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CMultiConnector();

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	int AddMessage(RSEvent* pMsg) { return m_arMsg.Add((CObject*)pMsg); }

protected:
	DECLARE_MESSAGE_MAP()

//-- Value
private:
	BOOL m_bRun, m_bStop, m_bClear;
	CRSArray	 m_arMsg;

	CPTPMgr*    m_PTPMgr;

	int m_nCaptureIndex;
	int	m_nIndex;		// 멀티 커넥션 인덱스 번호
//------------------------------------------------------------------------------
//-- Date : 2010-3-4 
//-- Functions
//------------------------------------------------------------------------------
	virtual int Run(void);

public:
	void RemoveAll(BOOL bCheckConnect = FALSE);
	void SetPTPMgr(CPTPMgr* pPTP);
	CPTPMgr* GetPTPMgr() {return m_PTPMgr;}

//	int GetTotIndex() {return m_MultiPTPMgr.GetPTPCount();}

	DWORD	dwThreadId;
	HANDLE  g_Thread;
	HWND	pWnd;	

public:
	void SetIndex(int index){m_nIndex = index;}
	int  GetIndex(){return m_nIndex;}

public:
	//-- BASIC FUNCTIONS	
	void SetClear()		{ m_bClear = TRUE; if(IsNonData()) Sleep(1);}
	void ReleaseClear()	{ m_bClear = FALSE;}

private:
	void SetStop()		{ m_bStop  = TRUE; if(IsStop()) Sleep(1); }
	BOOL IsClear()		{ return m_bClear; }
	BOOL IsStop()		{ return m_bRun; }
	BOOL IsNonData()	{ if(m_arMsg.GetSize()) return FALSE; return TRUE; }

public:
	void OpenSession();
	void CloseSession();
	void CaptureImage();
	void TerminateSession();
	CString GetDeviceName();

};


