// MultiConnector.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "GlobalIndex.h"
#include "MultiConnector.h"

// CMultiConnector
// #pragma comment(lib, "PTP.lib")

IMPLEMENT_DYNCREATE(CMultiConnector, CWinThread)

CMultiConnector::CMultiConnector()
{
	m_nCaptureIndex = 0;
}

CMultiConnector::~CMultiConnector()
{
	// TerminateSession();
}

BOOL CMultiConnector::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	m_bRun	 = FALSE;
	m_bClear = FALSE;
	m_bStop	 = FALSE;
	return TRUE;
}

int CMultiConnector::ExitInstance()
{
	SetClear();
	SetStop();
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CMultiConnector, CWinThread)
END_MESSAGE_MAP()


// CMultiConnector 메시지 처리기입니다.

//--------------------------------------------------------------------------
//!@brief	RUN THREAD
//!@param   
//!@return	
//--------------------------------------------------------------------------
int CMultiConnector::Run(void)
{
	int nMsgIndex;	
	RSEvent* pMsg = NULL;
	m_bRun = TRUE;	
	while(1) 
	{
		nMsgIndex = m_arMsg.GetSize();
		while(nMsgIndex --)
		{
			pMsg = (RSEvent*)m_arMsg.GetAt(0);
			if(!pMsg)
			{
				TRACE("=====================No pMessage\r\n");
				continue;
			}
			
			if(IsClear())
			{
				goto clear;		
			}

			switch(pMsg->message)
			{
			case WM_RS_MC_OPEN_SESSION:
				OpenSession();
				break;
			case WM_RS_MC_CLOSE_SESSION:
				CloseSession();
				break;
			case WM_RS_MC_CAPTURE_IMAGE:
				CaptureImage();
				break;
			default:
				pMsg = NULL;
				TRACE("Unkone Message\r\n");
				break;
			}

			//-- 2010-7-23 hongsu.jung
			//-- Exception 
			if(pMsg)
			{
				delete pMsg;			
				pMsg = NULL;
			}
			m_arMsg.RemoveAt(0);			

			//-- 2010-3-12 hongsu.jung
			//-- Stop Testing. Reset
			if(IsClear())
			{clear:
				if(m_bStop)
				{	
					RemoveAll();
					TRACE("Stop\r\n");
					break;
				}
				else
				{
					//TGLog(2,"[CLEAR] Remove All Multi Command");
					RemoveAll(TRUE);
					nMsgIndex = m_arMsg.GetSize();
					TRACE("Go\r\n");
				}
			}
		}
		Sleep(1);
	}

	m_bRun = FALSE;
	return 0;
}

void CMultiConnector::RemoveAll(BOOL bCheckConnect)
{
	int nAll = m_arMsg.GetSize();
	while(nAll--)
	{
		RSEvent* pMsg = (RSEvent*)m_arMsg.GetAt(nAll);
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
			m_arMsg.RemoveAt(nAll);
		}		
		else
		{
			m_arMsg.RemoveAll();
			nAll = 0;
		}
	}	
}

//-- OpenSession
void CMultiConnector::OpenSession()
{
	BOOL bConnect = TRUE;
    if (m_PTPMgr->PTP_OpenSession() != PTPErrNone)
		bConnect = FALSE;
}

//-- CloseSession
void CMultiConnector::CloseSession()
{
	m_PTPMgr->PTP_CloseSession();
}

void CMultiConnector::TerminateSession()
{
	m_PTPMgr->PTP_TerminateSession();
}

//-- CaptureImage
void CMultiConnector::CaptureImage()
{
	int ret;	
	// chlee jpg 저장
	CString strFileName = _T("");
	strFileName.Format(_T("..\\..\\pictures\\Image%d_#NX200_%d.jpg"), m_nCaptureIndex, m_nIndex+1);
	m_nCaptureIndex++;
	ret = m_PTPMgr->PTP_SendCapture(strFileName);
	// chlee
	// ShellExecute(NULL, _T("open"), strFileName, NULL, NULL, SW_SHOW);
}

CString CMultiConnector::GetDeviceName()
{
	if(m_PTPMgr)
		return m_PTPMgr->GetDeviceVendor();	
	return _T("");
}

void CMultiConnector::SetPTPMgr(CPTPMgr* pPTP)
{
	m_PTPMgr = pPTP;
}
