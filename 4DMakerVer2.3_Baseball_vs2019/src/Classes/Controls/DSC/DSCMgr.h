////////////////////////////////////////////////////////////////////////////////
//
//	DSCMgr.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>

#include "ESMFunc.h"
#include "ESMIndex.h"
#include "ESMDefine.h"
#include "ESMm3u8.h"
#include "ESMEncoderForReferee.h"
// 4DMakerDSCMgr
class CDSCViewer;
class CDSCItem;
class CSdiSingleMgr;

//-- 2013-04-22 hongsu.jung
//-- Thread Function
//void _OpenSession(void *param);
void _DoConvertFrame(void *param);

struct stSensorData
{
	stSensorData()
	{
		nStartTime = 0;
		nTimeStamp = 0;
	}
	CString strDscId;
	UINT nStartTime;
	UINT nTimeStamp;
};

class CDSCMgr : public CWinThread
{
	DECLARE_DYNCREATE(CDSCMgr)

public:
	CDSCMgr(void) {};
	CDSCMgr(CDSCViewer* pParent);
	
	virtual ~CDSCMgr();

public:
	void DoThreadStop()	{ m_bThreadStop = TRUE; Sleep(10); }
public:
	CDSCViewer* m_pDSCViewer;
	HWND		m_hParentWnd;	// AfxGetMainWnd()->GetSafeHwnd();
	BOOL		m_bThreadRun;	
	BOOL		m_bThreadStop;
private:
	//	Thread Handle
	CRITICAL_SECTION m_csLock;
	HANDLE		m_hOpenSession;
	HANDLE		m_hSetProperty;
	HANDLE		m_hConvertFile;
	HANDLE		m_hDelThread;

	CESMArray	m_arMsg;
	void Init();
	virtual BOOL InitInstance();	
	virtual int Run(void);

	BOOL m_bRecording;
	int m_nCheckFileCount;
public:
	void AddMsg(ESMEvent* pMsg);
	void RemoveAll();
	void StopThread();
	virtual int ExitInstance();
	void CamConnectInfo(int nRemoteId);
	CDSCViewer* GetDscViewer(){ return m_pDSCViewer; }

	//------------------------------------------------------
	//-- DSC (4DMakerDSCMgrDSC.cpp)
	//------------------------------------------------------
	int CheckConnect();
	void OnDscOpenSession(ESMEvent* pMsg);
	void OnDscSetProperty(ESMEvent* pMsg);
	
	//------------------------------------------------------
	//-- EVENT (4DMakerDSCMgrEVent.cpp)
	//------------------------------------------------------
	void OnEvnetErrMsg(ESMEvent* pMsg);
	void OnEventOpenSession(ESMEvent* pMsg);
	void OnEventLiveviewReceive(ESMEvent* pMsg);
	void OnEventCaptureDone(ESMEvent* pMsg);
	void OnEventSaveDone(ESMEvent* pMsg);
	void OnEventAfDetect(ESMEvent* pMsg);
	void OnEventPropertyChange(ESMEvent* pMsg);
	void OnEventGetCaptureImageRecord(ESMEvent* pMsg);
	void OnEventGetProperty(ESMEvent* pMsg);
	void OnEventErrorOccured(ESMEvent* pMsg);
	void OnEventLiveviewErrorOccured(ESMEvent* pMsg);	
	void OnEventGetFocusValue(ESMEvent* pMsg);
	void OnEventGetSenSorTime(ESMEvent* pMsg);					//-- 2014-09-15 hongsu@esmlab.com
	void OnEventGetErrorFrame(ESMEvent* pMsg);					//-- 2014-09-17 hongsu@esmlab.com

	void OnEventGetAddFrame(ESMEvent* pMsg);					//-- 2014-09-17 hongsu@esmlab.com
	void OnEventGetSkipFrame(ESMEvent* pMsg);					//-- 2014-09-17 hongsu@esmlab.com
	
	void OnEventGetTickResult(ESMEvent* pMsg);
	

	//-- 2013-07-18 hongsu.jung
	void OnEventMovieDone(ESMEvent* pMsg);
	void OnEventFileTransfer(ESMEvent* pMsg);
	void OnEventCapture(ESMEvent* pMsg);
	void OnEventUsbConnectFail(ESMEvent* pMsg);
	void OnEventRcvSyncResult(ESMEvent* pMsg);
	void OnEventRcvSyncCheck(ESMEvent* pMsg);
	

	void OnEventRecordStop(ESMEvent* pMsg);

	//------------------------------------------------------
	//-- EVENT (User)
	//------------------------------------------------------
	void DoS1Press();
	void DoS1Press(CDSCItem* pItem);
	void DoS1Release();
	void DoS1Release(CDSCItem* pItem);
	void DoMinFocus();

	void DoTakePicture();	
	void DoMovieCapture(BOOL bRecordStatus);
	void DoMovieCancel();
	void DeleteDirectory(CString strDelPath);
	static unsigned WINAPI DelDirThread(LPVOID param);
	//------------------------------------------------------
	//-- Remote Control (User)
	//------------------------------------------------------
	void OnEventNetStatus(ESMEvent* pMsg);
	void OnEventConvertFinish(ESMEvent* pMsg);
	void OnEventNetCapture(ESMEvent* pMsg);
	static unsigned WINAPI OnEventNetCaptureThread(LPVOID param);
	void OnEventNetMovieCancel(ESMEvent* pMsg);
	void OnEventNetConnect(ESMEvent* pMsg);
	void OnEventNetPropGet(ESMEvent* pMsg);
	void OnEventNetPropSet(ESMEvent* pMsg);
	void OnEventSetFocusMove(ESMEvent* pMsg);
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	void OnEventSetFocusValue(ESMEvent* pMsg);
	void OnEventNetLiveview(ESMEvent* pMsg);
	void OnEventNetPreRecInitGH5(ESMEvent* pMsg);
	void OnEventNetPreRecInitGH5Result(ESMEvent* pMsg);
	void OnEventNetGetTick(ESMEvent* pMsg);
	void OnEventNetGetTickCmd(ESMEvent* pMsg);
	void OnEventRcvGetTickCmdResult(ESMEvent* pMsg);
	static unsigned WINAPI OnEventNetGetTickThread(LPVOID param);
	void OnEventSetMovieSaveCheck(ESMEvent* pMsg);
	void OnEventSetFocus(ESMEvent* pMsg);
	void OnEventFileTransferFail(ESMEvent* pMsg);
	void OnEventFileTransferInfo(ESMEvent* pMsg);
	void OnEventSendCaptureFail(ESMEvent* pMsg);
	void OnEventSensorSyncOut(ESMEvent* pMsg);	
	void OnEventCaptureDelay(ESMEvent* pMsg);	
	void OnEventNetGetItemFocus(ESMEvent* pMsg);	

	//------------------------------------------------------
	//-- LIST Command (4DMakerDSCMgrList.cpp)
	//------------------------------------------------------
	void OnEventAddUsbID(CString strUsbID);

	//------------------------------------------------------
	//-- LIST Command (4DMakerDSCMgrList.cpp)
	//------------------------------------------------------
	void OnEventSetSize(ESMEvent* pMsg);

	//------------------------------------------------------
	//-- Convert File from mp4 to jpg
	//------------------------------------------------------
	void OnEventConvertFile(ESMEvent* pMsg);

	//------------------------------------------------------
	//-- Copy Record File
	//------------------------------------------------------
	static unsigned WINAPI CopyRecordFile(LPVOID param);

	//------------------------------------------------------
	//-- 2014-09-17 hongsu@esmlab.com
	//-- RESET VALID ERROR FRAME
	//------------------------------------------------------
	void OnEventErrorFrame(ESMEvent* pMsg);
	CObArray m_arCorrectFrame;
	void AddValidFrame(ValidFrame* pValidFrame);	
	ValidFrame* GetPrevFrame(CString strID, int nIndex);
	int GetValidFrameCount()			{ return m_arCorrectFrame.GetCount(); }
	ValidFrame* GetValidFrame(int n)	{ return (ValidFrame*)m_arCorrectFrame.GetAt(n); }

	void OnEventAddFrame(ESMEvent* pMsg);
	void OnEventSkipFrame(ESMEvent* pMsg);
	void OnEventValidSkip(ESMEvent* pMsg);
	void OnEventRecordFinish(ESMEvent* pMsg);
	void OnEventCopyToServer(ESMEvent* pMsg);

	//------------------------------------------------------
	//-- 2014-09-1 kcd
	//-- Exception DSC
	//------------------------------------------------------
	void OnEventExceptionDsc(ESMEvent* pMsg);
	void OnEventInformSensorOK(ESMEvent*pMsg);
	int m_nCaptureTime;
	int GetCaptureTime() {return m_nCaptureTime;}
	void SetCaptureTime(int nCaptureTime) { m_nCaptureTime = nCaptureTime; }

	int m_nRecordTime;
	int GetRecordTime() {return m_nRecordTime;}
	void SetRecordTime(int nRecordTime) { m_nRecordTime = nRecordTime; }
	void ResetErrorFrame();

	vector<CString> m_ArrSyncFailDsc;
	multimap<CString, stSensorData> m_mapSyncFailDsc;
	void OnEventSyncDataSet();
	void SyncDataInsert(ESMEvent* pMsg);
	void SyncDataDelete(CString strRecProfile);

	HANDLE m_hRecStateThread;
	BOOL   m_bRecThreadStop;
	static unsigned WINAPI OpserverRecState(LPVOID param);
	static unsigned WINAPI OpenSessionThread(LPVOID param);

	void SetEditerRecording(BOOL bValue) {m_bRecording = bValue;}
	BOOL GetEditerRecording() {return m_bRecording;}
	void SetTimeLineReset();

	CRITICAL_SECTION m_csDataLock;

	void FileWriteDone(CString csFilePath);
	CString GetCamID(CString strFilePath);

	//190129 hjcho
	int GetDSCIndexFromCameraList(CString strConnectDSCID);
	CStringList m_strListConnectVendorId;
	void RemoveAllConnectVendorId();

private:
	map<CString, CESMm3u8 *> m_M3u8;
public:
	void MakeM3u8(CString strKey);
	CESMm3u8 * GetM3u8(CString strKey);
	void DeleteM3u8();
	void ResetM3u8();
};


struct stDSCMgrThreadData
{
	stDSCMgrThreadData()
	{
		pDscMgr = NULL;
		pMsg = NULL;
	}
	CDSCMgr* pDscMgr;
	ESMEvent* pMsg;
};