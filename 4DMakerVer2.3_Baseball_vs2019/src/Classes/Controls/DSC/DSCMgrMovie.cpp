////////////////////////////////////////////////////////////////////////////////
//
//	DSCViewerCapture.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCListViewer.h"
#include "DSCViewer.h"
#include "DSCItem.h"
#include "DSCMgr.h"
#include "ESMImgMgr.h"
#include "ESMProcessControl.h"
#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CDSCMgr::DoMovieCapture(BOOL bRecordStatus)
{
	int nAll = m_pDSCViewer->GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection"));
		return;
	}

	if(!(m_pDSCViewer->m_pFrameSelector->GetDSCRecStatus() == DSC_REC_ON))
	{
		TRACE(_T("===========START MOVIE CAPTURE==============\n"));

		//-- Reset TimeLine Editor
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_VIEW_TIMELINE_RESET;		
		::SendMessage(m_hParentWnd , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

		CString strFolder;
		BOOL bResult = FALSE;
		strFolder.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord());	
		bResult = CreateDirectory(strFolder, NULL);
		if(!bResult)
		{
			ESMLog(1, _T("Creat Folder Fail!!"));
		}
	}
	else
	{
		if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_BOTH)
		{
			CString strFailDsc;
			CDSCItem* pItem = NULL;
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);
			//초기화
			for( int i =0 ;i < arDSCList.GetSize(); i++)
			{
			 	pItem = (CDSCItem*)arDSCList.GetAt(i);
			 	pItem->SetSensorSync(TRUE);
			}
			ESMLog(5, _T("SensorSync Init"));
			for( int nFailIndex = 0; nFailIndex < m_ArrSyncFailDsc.size(); nFailIndex++)
			{
			 	strFailDsc = m_ArrSyncFailDsc.at(nFailIndex);
			 	for( int i =0 ;i < arDSCList.GetSize(); i++)
			 	{
			 		pItem = (CDSCItem*)arDSCList.GetAt(i);
			 		if(pItem)
			 		{
			 			if( pItem->GetDeviceDSCID() == strFailDsc)
			 			{
							ESMLog(5, _T("SyncOut [%s]"), strFailDsc);
			 				pItem->SetSensorSync(FALSE);
			 				break;
			 			}
			 		}
			 	}
			}
			m_ArrSyncFailDsc.clear();
		}
	}

	//-- Get Sync Time
	int nTickTime = ESMGetTick();
	SetCaptureTime(nTickTime);
	TRACE(_T("[Sync Exe] [Server] 1.Get Base Time %d\n"),nTickTime);

	CDSCGroup* pGroup = NULL;
	BOOL bMakingInfo = FALSE;

	//-- Load Making Info // NotUse
	//ESMEvent* pMsg	= new ESMEvent;
	//pMsg->message	= WM_ESM_MOVIE_SET_MAKING_INFO;		
	//::SendMessage(m_hParentWnd , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	

	
	BOOL bSend = FALSE;
	while(nAll--)
	{

		pGroup = m_pDSCViewer->GetGroup(nAll);
		if(!pGroup)
			continue;
		if(pGroup->GetSync())
		{
			//2017.02.17 joonho.kim
			// 4DP Makin info
			if(!ESMGetMaingInfoFlag())
			{
				pGroup->DoMakingInfo();
				bMakingInfo = TRUE;
			}
			if(bSend == FALSE)
			{
				pGroup->SendToDeleteDSC();
				bSend = TRUE;
			}
			
			pGroup->DoMovieGroupCapture(nTickTime, bRecordStatus);
		}  
	}

	if(bMakingInfo)
		ESMSetMaingInfoFlag(TRUE);

	if(!(m_pDSCViewer->m_pFrameSelector->GetDSCRecStatus() == DSC_REC_ON))
	{
		if(m_pDSCViewer->m_bPauseMode == TRUE)
		{
			SetRecordTime(0);
			m_pDSCViewer->m_bRecordStep = ESM_PAUSESTEP_PAUSEREADY;
			while(true)
			{
				if(m_pDSCViewer->m_bRecordStep == ESM_PAUSESTEP_RESUME)
					break;

				Sleep(1);
			}
		}
		else
		{
			int nRecTime = nTickTime+ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN);
			nTickTime = nTickTime + ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN) + sync_frame_time_after_sensor_on + ESMGetValue(ESM_VALUE_SENSORONTIME);
			SetRecordTime(nTickTime);
			while(true)
			{
				if(nRecTime <= ESMGetTick())
					break;

				Sleep(1);
			}
		}
		ESMSetRecordingInfoint(_T("RecFlag"), SDI_STATUS_REC);
	}
	else
	{
		ESMSetRecordingInfoint(_T("RecFlag"), SDI_STATUS_REC_FINISH);
		int nRecTime = GetDscViewer()->m_pFrameSelector->GetRecTime();
		ESMSetRecordingInfoint(_T("RecTime"), nRecTime);
	}

	m_pDSCViewer->m_pFrameSelector->SetRecStatus(DSC_REC_BTN);
}

void CDSCMgr::DoMovieCancel()
{
	int nAll = m_pDSCViewer->GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection"));
		return;
	}

	ESMLog(5,_T("Movie Cancel"));

	CDSCGroup* pGroup = NULL;
	while(nAll--)
	{
		pGroup = m_pDSCViewer->GetGroup(nAll);
		if(!pGroup)
			continue;
		pGroup->DoMovieCancel();
	}
}

void CDSCMgr::DeleteDirectory(CString strDelPath)
{
	if (m_hDelThread)
	{
		CloseHandle(m_hDelThread);
		m_hDelThread = NULL;
	}
	m_hDelThread = (HANDLE) _beginthreadex(NULL, 0, DelDirThread, (void *)this, 0, NULL);
}
unsigned WINAPI CDSCMgr::DelDirThread(LPVOID param)
{
	CString* pStrPath = (CString*)param;
	CString strPath;
 	CESMFileOperation fo;
 	strPath.Format(_T("%s\\"), *pStrPath);	// SERVER
 	fo.Delete(strPath, FALSE);
	return 0;
}
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-04-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @revision		
//------------------------------------------------------------------------------
void CDSCMgr::OnEventConvertFile(ESMEvent* pMsg)
{
	CDSCItem* pItem = (CDSCItem*)pMsg->pDest;
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	m_hConvertFile = (HANDLE) _beginthread( _DoConvertFrame, 0, (void*)pItem); // create thread
}

void _DoConvertFrame(void *param)
{
	CDSCItem* pItem		= (CDSCItem*)param;
	if(!pItem)
		return;

	//------------------------------------------------------------------------------
	//
	//! @brief		Using ffmpeg.exe
	//! 1. video generation
	//! => ffmpeg -i frame%d.jpg output.mp4
	//! => ffmpeg -i frame%d.jpg output.ts
	//!
	//! 2. frame extraction
	//! => ffmpeg -i input.mp4 frame%d.jpg
	//! 
	//! 3. video crop
	//!    from 30 sec to 150 sec (120 sec)
	//! => ffmpeg -i input.mp4 -ss 30 -t 120 -vcodec copy -acodec copy out_cut.mp4
	//! -ss : starting point (second)
	//! -t  : recoding time (second)
	//! 
	//! 4. video merge
	//! => ffmpeg -i input1.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate1.ts
	//! => ffmpeg -i input2.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate2.ts
	//! => ffmpeg -i "concat:intermediate1.ts|intermediate2.ts" -c copy -bsf:a aac_adtstoasc output.mp4
	//
	//------------------------------------------------------------------------------
	{
		//-- Remove Previous Files
		CESMFileOperation fo;
		SHELLEXECUTEINFO lpExecInfo;
		CString strPath;
		CString strDSCID = pItem->GetDeviceDSCID();

		//-- 2013-10-09 yongmin 
		//-- agent 일때 frame 추출 완료후 server 로 전송
		CString strSubFolder;
		CString strSubFolder2;
		

		//-- 2014-01-09 hjjang
		//Home Path로 변경하여 생성
		strSubFolder.Format(_T("%s\\record\\files\\%s"),ESMGetPath(ESM_PATH_HOME), ESMGetFrameRecord());

		// Server Path
		//strSubFolder.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_RECORD_FILE), ESMGetFrameRecord());	// SERVER
		

		strPath.Format(_T("%s\\%s\\"),strSubFolder,strDSCID);	// SERVER
		fo.Delete(strPath, FALSE);
		//-- 2013-09-26 hongsu@esmlab.com
		//-- Create Path
		CreateDirectory(strPath, NULL);

		CString strMovie;
		//-- 2013-10-09 yongmin 
		//-- agent 일때 frame 추출 완료후 server 로 전송
		//if(!ESMGetValue(ESM_VALUE_NET_MODE))	
			strMovie.Format(_T("%s\\%s.mp4"),ESMGetPath(ESM_PATH_MOVIE),pItem->GetDeviceDSCID());
		//else									
		//	strMovie.Format(_T("%s\\%s.mp4"),strSubFolder,pItem->GetDeviceDSCID());

		//2013-12-18 kcd
		// 영상 파일 복사시간 Wait.
		for( int i =0 ;i < 100; i++)
		{
			if(!fo.IsFileExist(strMovie))
				Sleep(50);
			else
				break;
		}
		
		ESMLog(1,_T("[Movie] Convert To Picture from [%s]"),strMovie);

		//-- 2013-09-25 hongsu@esmlab.com		
		//-- Create Still Images
		CString strCmd;
		strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));

		//! 2. frame extraction
		//! => ffmpeg -i input.mp4 frame%d.jpg

		CString strImg;
		//-- 2013-10-09 yongmin 
		//-- agent 일때 frame 추출 완료후 server 로 전송
		strImg.Format(_T("%s\\%s\\"), strSubFolder,strDSCID);
		strImg.Append(_T("frame_%d.jpg"));

		CString strCreateImages;
		strCreateImages.Format(_T("-i \"%s\" -q:v 1 \"%s\""),
			strMovie, //strTempMovie,									
			strImg);	 

		lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
		lpExecInfo.lpFile = strCmd;
		lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		lpExecInfo.hwnd = NULL;  
		lpExecInfo.lpVerb = L"open";
		lpExecInfo.lpParameters = strCreateImages;
		lpExecInfo.lpDirectory = NULL;
		lpExecInfo.nShow = SW_HIDE; // hide shell during execution
		lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
		ShellExecuteEx(&lpExecInfo);

		// wait until the process is finished
		if (lpExecInfo.hProcess != NULL)
		{
			::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
			::CloseHandle(lpExecInfo.hProcess);
		}
		

		//------------------------------------------------------------------------------		
		//! @brief		Correct Function
		//! @date		2013-10-15
		//! @owner		Hongsu Jung (hongsu@esmlab.com)		
		//! @revision	Change For Temporary
		//------------------------------------------------------------------------------				
		//-- Change File Names to Time
		CString strSrc, strDst;
		int nIndex = 1;
		int nMilliSec = 0;
		int nSec = 0;
		//-- Get Previous
		while(1)
		{
			//-- 2013-10-01 hongsu@esmlab.com
			//-- Check 0.990
			if( nMilliSec > 960)
			{
				nMilliSec = 0;
				nSec++;
			}								

			strSrc.Format(_T("%s\\frame_%d.jpg"),strPath, nIndex);
			strDst.Format(_T("%s\\%d.%03d.jpg"),strPath, nSec, nMilliSec);
			if(!fo.Rename(strSrc, strDst))
			break;
			nMilliSec += movie_next_frame_time;
			nIndex++;
		}

		//-- 2013-09-29 hongsu@esmlab.com
		//-- Set Last File Time 
		int nSaveFile = nSec*1000 + nMilliSec;
		pItem->SetSavedLastTime(nSaveFile);
		//ShellExecute(NULL, NULL, strCmd,  strCreateImages, NULL, NULL);
		ESMLog(1,_T("[Movie] Finish Creating Frames [%s][%d ms]"),strDSCID,nSaveFile);

		//-- 2013-10-09 yongmin 
		//-- agent 일때 frame 추출 완료후 server 로 전송
		/*
		if(ESMGetValue(ESM_VALUE_NET_MODE))
		{
			CString strServerPath;

			strServerPath.Format(_T("%s\\%s.mp4"),ESMGetPath(ESM_PATH_MOVIE),strDSCID);
			fo.Delete(strServerPath, FALSE);
			
			//  [10/14/2013 Administrator]
			//  Check 
			strServerPath.Replace(_T("\\\\"), _T("\\"));
			strServerPath.Insert(0,_T("\\"));
			ESMLog(5,_T("Copy Distribute File [%s][%s==>%s]"),strDSCID,strMovie,strServerPath);
			fo.Copy(strMovie,strServerPath,TRUE);


			strServerPath.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_FRAME),strDSCID);
			fo.Delete(strServerPath, FALSE);
			strServerPath.Format(_T("%s\\"),ESMGetPath(ESM_PATH_FRAME));

			strServerPath.Replace(_T("\\\\"), _T("\\"));
			strServerPath.Insert(0,_T("\\"));
			ESMLog(5,_T("Copy Distribute File [%s][%s==>%s]"),strDSCID,strPath,strServerPath);
			fo.Copy(strPath,strServerPath,TRUE);
		}
		*/

		//-- 2013-09-25 hongsu@esmlab.com
		//-- Send Message To Frame
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_FRAME_CONVERT_FINISH;
		pMsg->pDest		= (LPARAM)pItem;
		::SendMessage(pItem->m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);
	}
	_endthread();
}

