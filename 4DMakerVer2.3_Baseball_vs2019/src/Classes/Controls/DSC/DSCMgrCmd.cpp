////////////////////////////////////////////////////////////////////////////////
//
//	4DMakerDSCMgr.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-23
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCMgr.h"
#include "DSCViewer.h"
#include "ESMUtil.h"
#include "SdiSingleMgr.h"
#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//----------------------------------------------------------------
//-- 2013-04-22 hongsu.jung
//-- Thread Function
//----------------------------------------------------------------
// void _OpenSession(void *param)
// {
// 	HWND hwnd;
// 	CString strConnect;
// 	ESMEvent* pEvent = (ESMEvent*)param;
// 
// 	//-- Remove Char pointer in Function
// 	strConnect = ESMUtil::CharToCString((char*)pEvent->pParam, TRUE);
// 	hwnd = (HWND)pEvent->nParam1;
// 
// 	//-- 2013-09-10 hongsu@esmlab.com
// 	//-- Get Main View
// 	CDSCViewer* pDSCViewer = (CDSCViewer*)pEvent->pDest;	
// 	//-- remove Event Pointer
// 	if(pEvent)
// 	{
// 		delete pEvent;
// 		pEvent = NULL;;
// 	}
// 
// 	//-- 2013-02-06 hongsu.jung
// 	//-- Create Single Manager
// 	CSdiSingleMgr* pSDIMgr = NULL;
// 	//-- 2013-10-25 hongsu.jung
// 	//pSDIMgr = new CSdiSingleMgr(0, strConnect, _T(""));
// 	
// 	LARGE_INTEGER swFreq, swStart;
// 	ESMGetTickInfo(swFreq, swStart);
// 	pSDIMgr = new CSdiSingleMgr(0, strConnect, _T(""),swFreq, swStart);
// 
// 	pSDIMgr->SdiSetReceiveHandler(hwnd);
// 	if(!pSDIMgr)
// 	{
// 		ESMLog(0,_T("[PTP] Set Handle"));
// 		//2013.12.18 kcd
// 		// pSDIMgr 삭제 의미 없음.
// 		//delete pSDIMgr;
// 		_endthread();
// 		return;
// 	}
// 
// 	//-- 2011-05-24 hongsu.jung
// 	//-- Init Session
// 	int nRet = pSDIMgr->PTP_InitSession(strConnect);
// 	if(nRet < 1)
// 	{
// 		ESMLog(0,_T("[PTP] Dsc Iintial"));
// 		if(pSDIMgr)
// 		{
// 			delete pSDIMgr;
// 			pSDIMgr = NULL;
// 		}
// 		_endthread();
// 		return;
// 	}
// 	
// 	//-- 2011-05-24 hongsu.jung
// 	//-- Open Session
// 	ESMLog(3,_T("[PTP] OpenSession... [USB|%s]"),strConnect);
// 
// 	if(pSDIMgr->PTP_OpenSession() < 0)
// 	{
// 		// ESMLog(0,_T("[PTP] Get UniqueID... [USB|%s]"),strConnect);
// 		if(pSDIMgr)
// 		{
// 			delete pSDIMgr;
// 			pSDIMgr = NULL;
// 		}
// 		//_endthread();
// 		return;
// 	}
// 	ESMLog(3,_T("[PTP] Get UniqueID [%s][%s]"), pSDIMgr->GetDeviceUniqueID(),strConnect);
// 
// 	// [12/5/2013 Administrator]
// 	// Check Connection
// #ifdef NX3000_TEST
// 	if(!pSDIMgr->CheckGetFile())
// 	{
// 		//AfxMessageBox(_T("FAIL CONNECTION"));
// 		ESMLog(3, _T("FAIL CONNECTION"));
// 	}
// #endif
// 
// 	//-- 2013-09-10 hongsu@esmlab.com
// 	//-- Find CDSCItem / Update Data
// 	int nAll = pDSCViewer->GetItemCount();
// 	CDSCItem* pItem = NULL;
// 	while(nAll--)
// 	{
// 		pItem = pDSCViewer->GetItemData(nAll);
// 		if(pItem->GetDSCInfo(DSC_INFO_ID) == pSDIMgr->GetDeviceDSCID())
// 		{
// 			pItem->SetSDIMgr(pSDIMgr);
// 			//-- 2013-09-13 hongsu@esmlab.com
// 			//-- Set Select Item
// 			//pDSCViewer->m_pListView->SetSelectItem(nAll, pItem);
// 			pDSCViewer->m_pListView->SetGridStatus(pItem, GRID_STATUS_NULL);
// 
// 			
// 			//-- 2013-10-23 hongsu.jung
// 			//-- Get Tick Count
// #ifdef NX3000_TEST
// 			int tPC;
// 			double tDSC;
// 			pItem->GetTickCount(tPC, tDSC);
// #endif
// 			//-- 2013-10-02 hongsu@esmlab.com
// 			//-- Load Item Exist File
// 			/*
// 			ESMEvent* pMsg = new ESMEvent();
// 			pMsg->message	= WM_RS_MOVIE_FILE_CONVERT;
// 			pMsg->pDest		= (LPARAM)pItem;				
// 			::SendMessage(hwnd, WM_ESM, (WPARAM)WM_ESM_DSC, (LPARAM)pMsg);		
// 			*/
// 
// 			//_endthread();
// 			if(pSDIMgr)
// 			{
// 				delete pSDIMgr;
// 				pSDIMgr = NULL;
// 			}
// 			return ;
// 		}
// 	}
// 
// 	//-- 2011-05-24 hongsu.jung
// 	//-- Create Thread
// 	pSDIMgr->CreateThread();
// 
// 	//-- 2013-12-14 yongmin
// 	//-- Load List Not Camera
// 	pDSCViewer->AddDSC(pSDIMgr->GetDeviceDSCID());
// 	// Set Dsc Count
// 	ESMEvent* pMsg	= new ESMEvent;
// 	pMsg->message	= WM_ESM_MOVIE_SET_MOVIECOUNT;
// 	pMsg->nParam1	= pDSCViewer->m_arDSCItem.GetSize();
// 	::SendMessage(hwnd, WM_ESM, WM_ESM_MOVIE, (LPARAM)pMsg);
// 	pItem = pDSCViewer->GetItemData(pSDIMgr->GetDeviceDSCID());
// 	if(pItem)
// 	{
// 		pItem->SetSDIMgr(pSDIMgr);
// 		//-- 2013-09-13 hongsu@esmlab.com
// 		//-- Set Select Item
// 		//pDSCViewer->m_pListView->SetSelectItem(nAll, pItem);
// 		pDSCViewer->m_pListView->SetGridStatus(pItem, GRID_STATUS_NULL);
// 			
// 		//-- 2013-10-23 hongsu.jung
// 		//-- Get Tick Count
// #ifdef NX3000_TEST
// 		int tPC;
// 		double tDSC;
// 		pItem->GetTickCount(tPC, tDSC);
// #endif
// 	}
// 
// //	ESMEvent * pSdiMsg = new ESMEvent;
// // 	pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
// // 	pSdiMsg->nParam1 = HIDDEN_COMMAND_CHANGE_MOV_VOICE;
// // 	pSdiMsg->nParam2 = 0;		//0 : Disable, 1: Use
// // 	pItem->SdiAddMsg(pSdiMsg);	
// 
// 
// 	//_endthread();
// 	return ;
// }

unsigned WINAPI CDSCMgr::OpenSessionThread(LPVOID param)
{	
	HWND hwnd;
	CString strConnect;
	ESMEvent* pEvent = (ESMEvent*)param;
	 
	//-- Remove Char pointer in Function
	strConnect = ESMUtil::CharToCString((char*)pEvent->pParam, TRUE);
	hwnd = (HWND)pEvent->nParam1;
	 
	//-- 2013-09-10 hongsu@esmlab.com
	//-- Get Main View
	CDSCViewer* pDSCViewer = (CDSCViewer*)pEvent->pDest;	
	//-- remove Event Pointer
	if(pEvent)
	{
	 	delete pEvent;
	 	pEvent = NULL;;
	}
	 
	//-- 2013-02-06 hongsu.jung
	//-- Create Single Manager
	CSdiSingleMgr* pSDIMgr = NULL;
	 	
	LARGE_INTEGER swFreq, swStart;
	ESMGetTickInfo(swFreq, swStart);
	pSDIMgr = new CSdiSingleMgr(0, strConnect, _T(""),swFreq, swStart);
	 
	pSDIMgr->SdiSetReceiveHandler(hwnd);
	if(!pSDIMgr)
	{
	 	ESMLog(0,_T("[PTP] Set Handle"));
	 	return 0;
	}
	 
	//-- 2011-05-24 hongsu.jung
	//-- Init Session
	int nRet = pSDIMgr->PTP_InitSession(strConnect);
	CString strModel = pSDIMgr->GetDeviceModel();
	if(nRet < 1)
	{
	 	ESMLog(0,_T("[PTP] Dsc Iintial"));
	 	if(pSDIMgr)
	 	{
	 		delete pSDIMgr;
	 		pSDIMgr = NULL;
	 	}
	 	return 0;
	}
	 	
	//-- 2011-05-24 hongsu.jung
	//-- Open Session
	ESMLog(3,_T("[PTP] OpenSession... [USB|%s]"),strConnect);	

	/*if(strModel.CompareNoCase(_T("GH5")) == 0)
	{
		if(pSDIMgr->PTP_ExOpenSession() < 0)
		{
			// ESMLog(0,_T("[PTP] Get UniqueID... [USB|%s]"),strConnect);
			if(pSDIMgr)
			{
				delete pSDIMgr;
				pSDIMgr = NULL;
			}
			return 0;
		}
	}
	else*/
	{
		if(pSDIMgr->PTP_OpenSession() < 0)
		{
			// ESMLog(0,_T("[PTP] Get UniqueID... [USB|%s]"),strConnect);
			if(pSDIMgr)
			{
				delete pSDIMgr;
				pSDIMgr = NULL;
			}
			return 0;
		}
	}
	
	ESMLog(3,_T("[PTP] Get UniqueID [%s][%s]"), pSDIMgr->GetDeviceUniqueID(),strConnect);
	 
	// [12/5/2013 Administrator]
	// Check Connection
	#ifdef NX3000_TEST
	if(!pSDIMgr->CheckGetFile())
	{
	 	//AfxMessageBox(_T("FAIL CONNECTION"));
	 	ESMLog(3, _T("FAIL CONNECTION"));
	}
	#endif
	 
	//-- 2013-09-10 hongsu@esmlab.com
	EnterCriticalSection (&(pDSCViewer->m_CriSection));
	int nAll = pDSCViewer->GetItemCount();
	CDSCItem* pItem = NULL;
	while(nAll--)
	{
	 	pItem = pDSCViewer->GetItemData(nAll);
	 	if(pItem->GetDSCInfo(DSC_INFO_ID) == pSDIMgr->GetDeviceDSCID())
	 	{
			if( pItem->m_pSdi == NULL)
				break;

	 		if(pSDIMgr)
	 		{
				ESMLog(3,_T("[PTP] delete pSDIMgr; (CSdiSingleMgr) [%s][%s]"), pSDIMgr->GetDeviceUniqueID(),strConnect);
	 			delete pSDIMgr;
	 			pSDIMgr = NULL;
	 		}
			LeaveCriticalSection (&(pDSCViewer->m_CriSection));
	 		return 0;
	 	}
	}
	 
	//-- 2011-05-24 hongsu.jung
	//-- Create Thread
	pSDIMgr->CreateThread();
	 
	//-- 2013-12-14 yongmin
	//-- Load List Not Camera
	if(pSDIMgr->GetDeviceDSCID().IsEmpty())
	{
		pSDIMgr->SetDeviceDSCID(_T("10101"));
	}

	pDSCViewer->AddDSC(pSDIMgr->GetDeviceDSCID());

	if(ESMGetGPUMakeFile())
	{
		if(ESMGetValue(ESM_VALUE_RTSP))
		{
			int nIdx = pDSCViewer->m_pDSCMgr->GetDSCIndexFromCameraList(pSDIMgr->GetDeviceDSCID());
			/*for(int i = 0 ; i < 10 ; i++)
				ESMLog(5,_T("[%d] camera index!!!!"),nIdx);*/
			ESMLog(5,_T("[%d] 4DP Method"),ESMGetValue(ESM_VALUE_4DPMETHOD));
			int nFrameRate = 30;
			if(ESMGetValue(ESM_VALUE_4DPMETHOD) == 0 || ESMGetValue(ESM_VALUE_4DPMETHOD) == 2)
				nFrameRate = 25;

#ifdef DEV_LGU
			//nIdx += 30;
			//190320 hjcho - LGU+ 임시 카메라 순번 지정
			if(nIdx == 0 || nIdx == 60)
			{
				nIdx = 60;
			}
			/*else if(nIdx < 60)
			{
				nIdx = 60 - nIdx;
			}*/
#endif

			ESMMovieRTMovie(_T(""),pSDIMgr->GetDeviceDSCID(),nIdx,nFrameRate);
		}
		else
		{
			if(ESMGetValue(ESM_VALUE_REFEREEREAD) == FALSE)
				ESMSet4DPMgr(pSDIMgr->GetDeviceDSCID());
		}
	}

	//jhhan 190208
	if(ESMGetValue(ESM_VALUE_REFEREEREAD))
	{
		CString strKey = pSDIMgr->GetDeviceDSCID();
		//jhhan m3u8
		pDSCViewer->m_pDSCMgr->MakeM3u8(strKey);
		ESMCreateM3u8(strKey);
		
	}
	

	LeaveCriticalSection (&(pDSCViewer->m_CriSection));


	// Set Dsc Count
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_MOVIE_SET_MOVIECOUNT;
	pMsg->nParam1	= pDSCViewer->m_arDSCItem.GetSize();
	::SendMessage(hwnd, WM_ESM, WM_ESM_MOVIE, (LPARAM)pMsg);

	pItem = pDSCViewer->GetItemData(pSDIMgr->GetDeviceDSCID());
	if(pItem)
	{
	 	pItem->SetSDIMgr(pSDIMgr);
	 	//-- 2013-09-13 hongsu@esmlab.com
	 	//-- Set Select Item
	 	//pDSCViewer->m_pListView->SetSelectItem(nAll, pItem);
	 	pDSCViewer->m_pListView->SetGridStatus(pItem, GRID_STATUS_NULL);
	 			
	 	//-- 2013-10-23 hongsu.jung
	 	//-- Get Tick Count
	#ifdef NX3000_TEST
	 	int tPC, nCheckCnt, nCheckTime;
	 	double tDSC;
	 	pItem->DSCGetTickCount(tPC, tDSC, nCheckCnt, nCheckTime, ESMGetValue(ESM_VALUE_DSC_SYNC_TIME));
	#endif
	}
	 
	return 0;
}


unsigned WINAPI CDSCMgr::CopyRecordFile(LPVOID param)
{
	//파일크기 확인해서 진행
	CDSCItem* pDSCItem = (CDSCItem*)param;
	CString strMoviePath, strRamFile, strRamSeFile, strFile, strSavePath;
	strRamFile.Format(_T("%s\\%s.mp4"), ESMGetClientRamPath(), pDSCItem->GetDeviceDSCID());
	strRamSeFile.Format(_T("%s\\%s.mp4"), ESMGetClientBackupRamPath(), pDSCItem->GetDeviceDSCID());

	CESMFileOperation fo;
	if( !fo.CheckPath(ESMGetClientBackupRamPath()))
		fo.CreateFolder(ESMGetClientBackupRamPath());

	int nCount = 0;
	while(1)
	{
		HANDLE hFile = CreateFile( strRamFile, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL );
		int nFileSize = GetFileSize( hFile, NULL );
		if(pDSCItem->GetMovieSize() == nFileSize && pDSCItem->GetMovieFileClose())
		{
			CloseHandle( hFile );
			break;
		}
		nCount++;
		CloseHandle( hFile );
		if( nCount > 3000)
		{
			ESMLog(5, _T("Copy Record Done Fail ImageSize[%d]ImageSize[%d] CloseState[%d]"), pDSCItem->GetMovieSize(), nFileSize, pDSCItem->GetMovieFileClose(), pDSCItem->GetMovieFileClose());
			return 0;
		}
		Sleep(2);
	}

	if ( ESMGetExecuteMode() )
	{
		strMoviePath.Format(_T("%s"),ESMGetClientRamPath());

		if(ESMGetValue(ESM_VALUE_MOVIESAVELOCATION))
			strSavePath.Format(_T("%s\\%s\\%s.mp4"),ESMGetPath(ESM_PATH_LOCALMOVIE), ESMGetFrameRecord(), pDSCItem->GetDeviceDSCID());
		else
		{
			strSavePath.Format(_T("%s\\%s\\%s.mp4"),ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pDSCItem->GetDeviceDSCID());		
			ESMEvent* pMsg= new ESMEvent();
			pMsg->pDest		= (LPARAM)pDSCItem;
			pMsg->message	= WM_ESM_NET_COPYTOSERVER;
			::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pMsg);
		}
	}
	else
		strMoviePath.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord());

	strFile.Format(_T("%s\\%s.mp4"), strMoviePath, pDSCItem->GetDeviceDSCID());
	CopyFile(strRamFile, strRamSeFile, false);

	CString strTp;
	strTp.Format(_T("%s@%s"), strRamSeFile, strRamFile);
	TCHAR* cPath = new TCHAR[1+strTp.GetLength()];
	_tcscpy(cPath, strTp);
	TCHAR* strId = new TCHAR[pDSCItem->GetDeviceDSCID().GetLength() + 1];
	_tcscpy(strId, pDSCItem->GetDeviceDSCID());

// 	ESMEvent* pMsgBuffer	= new ESMEvent();
// 	pMsgBuffer->message	= WM_ESM_MOVIE_SET_MOVIEDATA;
// 	pMsgBuffer->pParam	= (LPARAM)cPath;
// 	pMsgBuffer->pDest	= (LPARAM)strId;
// 	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsgBuffer);
// 	ESMLog(5, _T("Send Record Finish!!"));
// 	ESMEvent* pMsg= new ESMEvent();
// 	pMsg->pDest		= (LPARAM)pDSCItem;
// 	pMsg->message	= WM_ESM_NET_RECORDFINISH;
// 	::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pMsg);

	// File Copy to Movie Path

	CopyFile(strRamFile, strSavePath, false);
	return 0;
}
int CDSCMgr::GetDSCIndexFromCameraList(CString strConnectDSCID)
{
	CString strPath;
	strPath.Format(_T("%s\\CameraList.csv"),ESMGetPath(ESM_PATH_SETUP));

	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\CameraList.csv"), ESMGetPath(ESM_PATH_SETUP));

	if(!ReadFile.Open(strInputData, CFile::modeRead))
	{
		ESMLog(0,_T("Load CameraList.csv ERROR!!!!!!!"));
		return 0;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if(iLength == 0)
	{
		ESMLog(0,_T("Load CameraList.csv ERROR!!!!!!!"));
		return 0;
	}

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0, i = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;

	CString strNum, strDSCIP, strDSCID,strUsedFlag, strMakeFlag, strName, strReverse, strGroup, strVertical, strDetect;

	int nCnt = 1;
	int nIdx = 0;

	while(1)
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;

		AfxExtractSubString(strDSCID, sLine, 0, ',');

		strDSCID.Trim();

		if(strDSCID != _T(""))
		{
			if(strDSCID == strConnectDSCID)
			{
				nIdx = nCnt;
				break;
			}
		}
		else
			break;

		nCnt++;
	}

	//ReadFile.Close();
	return nIdx;
}