////////////////////////////////////////////////////////////////////////////////
//
//	DSCMgrDSC.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCMgr.h"
#include "DSCViewer.h"

#include "ESMUtil.h"
#include "SdiSingleMgr.h"
#include "ESMFileOperation.h"
#include <random>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ESM_FRAME_SYNC_MARGIN	10

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-04-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @revision		
//------------------------------------------------------------------------------

int CDSCMgr::CheckConnect()
{
	//-- 2013-02-12 hongsu.jung
	//-- Get Connection List
	SdiStrArray arList;
	CSdiSingleMgr::SdiGetDeviceHandleList(&arList);

	//-- 2013-02-06 hongsu.jung
	//-- Get Connected List
	int nAll = (int)arList.GetSize();

	ESMLog(5, _T("CheckConnect Device Handle Count[%d]"), nAll);

	//-- 2014-09-08 hongsu@esmlab.com
	//-- Check Disconnected
	if(!m_pDSCViewer)
	{
		arList.RemoveAll();
		ESMLog(0,_T("CheckConnect::m_pDSCViewer non exist"));
		return SDI_ERR_SDK_NOT_READY;
	}

	m_pDSCViewer->CheckDisconnet(&arList);

	if(!nAll)
		return SDI_ERR_OK;

	// 2017-08-31 yongmin 디바이스 핸들 리스트 카운트 와 아이템 카운트 만으로 새로운 연결을 판단 할수 없음!
	//if( arList.GetSize() == m_pDSCViewer->GetItemCount())
	//	return SDI_ERR_OK;

	SdiString strConnect;
	bool bMsgFlag = false;
	while(nAll--)	
	{
		//-- 2013-02-13 hongsu.jung
		//-- Wait Create Thread
		ESMSleep();
		strConnect = arList.GetAt(nAll);
		
		//-- 2013-02-12 hongsu.jung
		//-- Check Exist
		//-- 2014-01-21 changdo kim 
		//-- 재접속 변경 .
		if(m_pDSCViewer->IsExist(strConnect))
			continue;

		POSITION pos = m_strListConnectVendorId.Find(strConnect);
		if(pos != NULL)
		{
			ESMLog(5,_T("CheckConnect return- exist strConnect: %s"), strConnect);
			continue;
		}
		m_strListConnectVendorId.AddTail(strConnect);

		bMsgFlag = true;
		ESMEvent* pMsg = new ESMEvent();		
		pMsg->message = WM_ESM_DSC_OPEN_SESSION;
		pMsg->pParam = (LPARAM)ESMUtil::GetCharString(strConnect);
		AddMsg(pMsg);		
	}

	arList.RemoveAll();

	if(bMsgFlag)
	{
		ESMEvent* pMsg	= NULL;
		//-- 2013-09-11 hongsu@esmlab.com
		//-- Check List
		/*
		pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_VIEW_SORT;		
		::PostMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		*/

		//-- 2013-10-05 hongsu@esmlab.com
		//-- Organize Number Field 
		pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_VIEW_ORGANIZE_INDEX;		
		::PostMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	}

	return SDI_ERR_OK;
}

void CDSCMgr::OnDscOpenSession(ESMEvent* pMsg)	
{
	//-- Add To List with USB ID
	//OnEventAddUsbID(ESMUtil::CharToCString((char*)pMsg->pParam));

	//-- Open Session Thread
	ESMLog(5,_T("[Thread] OpenSession [%s]"),ESMUtil::CharToCString((char*)pMsg->pParam));
	pMsg->nParam1	= (UINT)AfxGetMainWnd()->GetSafeHwnd();
	pMsg->pDest		= (LPARAM)m_pDSCViewer;

//	m_hOpenSession = (HANDLE) _beginthread( _OpenSession, 0, (void*)pMsg); // create thread

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, OpenSessionThread, (void *)pMsg, 0, NULL);
	CloseHandle(hSyncTime);	
}

void CDSCMgr::OnEventNetStatus(ESMEvent* pMsg)	
{
	int nStatus = pMsg->nParam1;
	//if( nStatus == SDI_STATUS_REC && ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		if(ESMGetRecordSync() == FALSE)
			return;
	}
	if( nStatus == SDI_STATUS_CONNECTED && ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
	{
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		return;
	}
	
	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pItem = NULL;
	CString* pDSCID = (CString*)pMsg->pDest;
	if( pDSCID != NULL)
	{
		while(nAll--)
		{
			pItem = m_pDSCViewer->GetItemData(nAll);
			if(pItem->GetDeviceDSCID() == pDSCID->GetString())
			{
				pItem->SetDSCStatus(pMsg->nParam1);
				CamConnectInfo(pItem->m_nRemoteID);
				break;
			}
		}
	}
	if(pMsg->nParam1 == SDI_STATUS_REC) //jhhan 181022
	{
		m_pDSCViewer->m_pFrameSelector->SetInitRecTime();
		m_pDSCViewer->m_pFrameSelector->InitLine();

		//ESMLoadSquarePointData();
	}
		
	//}
	//  2013-10-17 Ryumin
	//	DSCFrameSelector Draw Sync
 	//if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER && ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING)
 	//{
 	//	int nStatus = pMsg->nParam1;
 	//	if(nStatus == SDI_STATUS_REC || nStatus == SDI_STATUS_REC_FINISH)
 	//	{
 	//		int nRecFlag = ESMGetRecordingInfoInt(_T("RecFlag"), -1);
 	//		if(nRecFlag != nStatus)
 	//		{
 	//			/*if(nStatus == SDI_STATUS_REC)
 	//			{
 	//				Sleep((ESMGetValue((ESM_VALUE_SYNC_TIME_MARGIN))) + 800);
 	//			}*/
 	//			//ESMSetRecordingInfoint(_T("RecFlag"), nStatus);
 	//			//ESMLog(5, _T("##################FileRead RecFlag : %d, Network RecFlag : %d"), nRecFlag, nStatus);
 	//			m_pDSCViewer->m_pFrameSelector->SetRecStatus(DSC_REC_BTN);
 
 	//			CString strRecProfile = ESMGetRecordingInfoString(_T("RecProfile"), _T(""));
 	//			if(strRecProfile.GetLength())
 	//				ESMSetFrameRecord(strRecProfile);
 	//		}
 	//	}
 	//}
	m_pDSCViewer->m_pFrameSelector->DrawEditor();

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}
}

unsigned WINAPI CDSCMgr::OpserverRecState(LPVOID param)
{
	CDSCMgr* pDSCMgr = (CDSCMgr*)param;
	int nRecState = SDI_STATUS_NORMAL;
	CDSCItem* pExist = NULL;
	pDSCMgr->SetEditerRecording(FALSE);
	int nStatus = SDI_STATUS_NORMAL;
	int nPoint = 0;
	CString strPoint;
	while(1)
	{
		Sleep(10);
		
		int nRecFlag = ESMGetRecordingInfoInt(_T("RecFlag"), SDI_STATUS_NORMAL);
		nStatus = nRecFlag;
		if(nRecFlag != nRecState && nRecFlag != SDI_STATUS_NORMAL)
		{
			if(ESMGetRecordSync() == FALSE)
			{
				//ESMLog(5, _T("RecFlag :%d"), nRecFlag);
				continue;
			}

			//pDSCMgr->m_pDSCViewer->m_pFrameSelector->SetRecStatus(DSC_REC_BTN);
			if( nRecFlag == SDI_STATUS_REC)  // 녹화 명령시에
			{
				//if (!ESMGetFrameRecord().GetLength())
				{
					CString strRecProfile = ESMGetRecordingInfoString(_T("RecProfile"), _T(""));
					if(strRecProfile.GetLength())
					{
						CString strPrevRecProFile = ESMGetFrameRecord();
						if (strPrevRecProFile.GetLength())
						{
							pDSCMgr->SyncDataDelete(strPrevRecProFile);
						}

						ESMSetFrameRecord(strRecProfile);
						{
							CString strRecordFolder = ESMGetPath(ESM_PATH_MOVIE);
							CString strHome = ESMGetPath(ESM_PATH_FILESERVER);
							if(strRecordFolder.Find(_T("F:")) != -1)
							{
								strHome.Replace(_T("4DMaker"), _T(""));
								strRecordFolder.Replace(_T("F:\\"),strHome);
								strPoint.Format(_T("%s\\%s.sp"),strRecordFolder, strRecProfile);
							}
						}
					}
				}
				
				
				//ESMEvent* pMsg	= new ESMEvent;
				//pMsg->message	= WM_ESM_VIEW_TIMELINE_RESET;		
				//::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

				//CObArray arDSCList;
				//ESMGetDSCList(&arDSCList);
				//CDSCItem* pItem = NULL;
				//for( int i =0 ;i < arDSCList.GetSize(); i++)
				//{
				//	pItem = (CDSCItem*)arDSCList.GetAt(i);
				//	//pItem->SetDSCStatus(FALSE);
				//}
				
				if(!pDSCMgr->GetEditerRecording())
				{
					//ESMLog(5, _T("SDI_STATUS_REC"));
					pDSCMgr->m_pDSCViewer->m_pFrameSelector->SetRecStatusEditer(DSC_REC_ON);
					pDSCMgr->SetEditerRecording(TRUE);
				}
			}
			else // 녹화 종료시에
			{
				
				pDSCMgr->m_pDSCViewer->m_pFrameSelector->SetRecStatusEditer(DSC_REC_STOP);
				//CString strRecProfile = ESMGetRecordingInfoString(_T("RecProfile"), _T(""));
				//if(strRecProfile.GetLength())
				//{
				//	CString strPrevRecProFile = ESMGetFrameRecord();
				//	//if (strPrevRecProFile != strRecProfile)
				//	//	pDSCMgr->SyncDataDelete(strPrevRecProFile);

				//	ESMSetFrameRecord(strRecProfile);
				//}

				ESMEvent* pMsg = NULL;
				pMsg	= new ESMEvent;
				pMsg->message	= WM_ESM_VIEW_RECORDDONE;
				::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
				// 화면을 빨간색에서 변경

				CString* pDSCID = (CString*)pMsg->pDest;
				int nAll = pDSCMgr->m_pDSCViewer->GetItemCount();
				CDSCItem* pExist = NULL;
				while(nAll--)
				{
					pExist = pDSCMgr->m_pDSCViewer->GetItemData(nAll);
					if(pExist)
					{
						pExist->SetDSCStatus(SDI_STATUS_REC);
						//ESMLog(5, _T("RecState Change [%s]"), pExist->GetDeviceDSCID());
					}
				}

				pDSCMgr->GetDscViewer()->m_pFrameSelector->SetRecTime(0);
				pDSCMgr->GetDscViewer()->m_pFrameSelector->SetTimeMax(0);

				int nRecTime = ESMGetRecordingInfoInt(_T("RecTime"), 0);
				pDSCMgr->GetDscViewer()->m_pFrameSelector->SetRecTime(nRecTime);
				pDSCMgr->GetDscViewer()->m_pFrameSelector->SetTimeMax(nRecTime);

				CObArray arDSCList;
				ESMGetDSCList(&arDSCList);
				CDSCItem* pItem = NULL;
				CString strFailDsc;
				for( int i =0 ;i < arDSCList.GetSize(); i++)
				{
					pItem = (CDSCItem*)arDSCList.GetAt(i);
					pItem->SetSensorSync(FALSE);
					if(pItem)
					{
						pItem->SetSensorSync(TRUE);
						for( int nFailIndex = 0; nFailIndex < pDSCMgr->m_ArrSyncFailDsc.size(); nFailIndex++)
						{
							strFailDsc = pDSCMgr->m_ArrSyncFailDsc.at(nFailIndex);
							if( pItem->GetDeviceDSCID() == strFailDsc)
							{
								ESMLog(5, _T("Sync Out Dsc [%s]"), strFailDsc);
								pItem->SetSensorSync(FALSE);
								break;
							}
						}
					}
				}
				pDSCMgr->m_ArrSyncFailDsc.clear();

				ESMLog(5, _T("Set Sync Data") );
				pDSCMgr->SetEditerRecording(FALSE);
				strPoint.Empty();
				nPoint = 0;
			}
			nRecState = nRecFlag;
		}
		else if(nRecFlag == nRecState && nRecFlag == SDI_STATUS_REC)
		{
			if(ESMGetRecordSync() == FALSE)
			{
				//ESMLog(5, _T("RecFlag :%d"), nRecFlag);
				continue;
			}

			if(!pDSCMgr->GetEditerRecording())
			{
				//CString strRecProfile = ESMGetRecordingInfoString(_T("RecProfile"), _T(""));
				//if(strRecProfile.GetLength())
				//{
				//	CString strPrevRecProFile = ESMGetFrameRecord();
				//	//if (strPrevRecProFile != strRecProfile)
				//	//	pDSCMgr->SyncDataDelete(strPrevRecProFile);

				//	ESMSetFrameRecord(strRecProfile);
				//}

				int nRecTime = ESMGetRecordingInfoInt(_T("RecTime"), 0);
				pDSCMgr->m_pDSCViewer->m_pFrameSelector->SetRecTime(nRecTime);
				pDSCMgr->m_pDSCViewer->m_pFrameSelector->SetRecStatusEditer(DSC_REC_ON);
				pDSCMgr->SetEditerRecording(TRUE);
			}

			if(strPoint.IsEmpty() != TRUE)
			{
				if(ESMLoadSelectPoint(nPoint, strPoint) != 0)
					nPoint++;
			}
		}
		if( pDSCMgr->m_bRecThreadStop ) 
			break;
	}
	return 0 ;

}

void CDSCMgr::SetTimeLineReset()	
{
	int nRecTime =  REC_TIMER_DRAW_TIME;		
	//-- Check Total Size
	m_pDSCViewer->m_pFrameSelector->SetRecTime(0);
	m_pDSCViewer->m_pFrameSelector->SetTimeMax(nRecTime);
	m_pDSCViewer->m_pFrameSelector->DrawRecBar();
	m_pDSCViewer->m_pFrameSelector->ReflashSelectTimeLine();
}

void CDSCMgr::OnEventConvertFinish(ESMEvent* pMsg)	
{
	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pExist = NULL;
	CString* pDSCID = (CString*)pMsg->pDest;

	while(nAll--)
	{
		pExist = m_pDSCViewer->GetItemData(nAll);
		if(pExist->GetDeviceDSCID() == pDSCID->GetString())
		{
			pExist->SetSavedLastTime(pMsg->nParam1);
			m_pDSCViewer->m_pFrameSelector->SetSavedLastTime(pMsg->nParam1);
			break;
		}
	}
	//  2013-10-17 Ryumin
	//	DSCFrameSelector Draw Sync
	m_pDSCViewer->m_pFrameSelector->DrawEditor();

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}
}

void CDSCMgr::OnEventNetPropGet(ESMEvent* pMsg)	
{
	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pExist = NULL;
	CString* pDSCID = (CString*)pMsg->pDest;
	ESMEvent* pDuplicate = NULL;

	if(pDSCID->GetLength())
	{
		while(nAll--)
		{
			pExist = m_pDSCViewer->GetItemData(nAll);
			if(pExist->GetDeviceDSCID() == pDSCID->GetString())
			{
				//-- 2014/09/06 hongsu
				//-- Debug
				pDuplicate = new ESMEvent;
				pDuplicate->message = WM_SDI_OP_GET_PROPERTY_DESC;
				pDuplicate->nParam1 = pMsg->nParam1;
				pExist->SdiAddMsg(pDuplicate);

				if(pMsg)
				{
					delete pMsg;
					pMsg = NULL;
				}

				if(pDSCID)
				{
					delete pDSCID;
					pDSCID = NULL;
				}

				return;
			}
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}
}

void CDSCMgr::OnEventSetFocusMove(ESMEvent* pMsg)
{
	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pExist = NULL;
	CString* pDSCID = (CString*)pMsg->pDest;
	ESMEvent* pDuplicate = NULL;

	while(nAll--)
	{
		pExist = m_pDSCViewer->GetItemData(nAll);
		if(pExist->GetDeviceDSCID() == pDSCID->GetString())
		{
			ESMEvent* pSnedMsg = new ESMEvent();
			pSnedMsg->message = WM_SDI_OP_SET_FOCUS_VALUE;
			pSnedMsg->nParam1 = pMsg->nParam1;
			pExist->SdiAddMsg(pSnedMsg);
			break;
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}
}

//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
void CDSCMgr::OnEventSetFocusValue(ESMEvent* pMsg)
{
	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pExist = NULL;
	CString* pDSCID = (CString*)pMsg->pDest;
	ESMEvent* pDuplicate = NULL;

	while(nAll--)
	{
		pExist = m_pDSCViewer->GetItemData(nAll);
		if(pExist->GetDeviceDSCID() == pDSCID->GetString())
		{
			ESMEvent* pSnedMsg = new ESMEvent();
			pSnedMsg->message = WM_SDI_OP_SET_FOCUS_VALUE;
			pSnedMsg->nParam1 = pMsg->nParam1;
			pExist->SdiAddMsg(pSnedMsg);
			break;
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}
}

void CDSCMgr::OnEventNetPropSet(ESMEvent* pMsg)	
{
	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pExist = NULL;
	CString* pDSCID = (CString*)pMsg->pDest;
	ESMEvent* pDuplicate = NULL;

	while(nAll--)
	{
		//-- 2014/09/06 hongsu
		//-- Debug
		pDuplicate = new ESMEvent;
		pDuplicate->message = WM_SDI_OP_GET_PROPERTY_DESC;
		pDuplicate->nParam1 = pMsg->nParam1;
		pDuplicate->nParam2 = pMsg->nParam2;

		pExist = m_pDSCViewer->GetItemData(nAll);
		if(pExist->GetDeviceDSCID() == pDSCID->GetString())
		{

			if( pMsg->nParam1 == HIDDEN_COMMAND_BANK_SAVE ||
				pMsg->nParam1 == HIDDEN_COMMAND_LCD_ON||
				pMsg->nParam1 == HIDDEN_COMMAND_LCD_OFF || 
				pMsg->nParam1 == HIDDEN_COMMAND_SAVEFOCUSZOOM)
			{
				if( pMsg->nParam2 == PTP_VALUE_NONE )
					pDuplicate->message = WM_SDI_OP_HIDDEN_COMMAND;
			}
			else if( pMsg->nParam1 == HIDDEN_COMMAND_CHANGE_MODE ||
				pMsg->nParam1 == HIDDEN_COMMAND_CHANGE_MOV_SIZE ||
				pMsg->nParam1 == HIDDEN_COMMAND_CHANGE_MOV_VOICE  ||
				pMsg->nParam1 == HIDDEN_COMMAND_MOVIEANDJPG ||
				pMsg->nParam1 == HIDDEN_COMMAND_NONENCORDING)
			{
				pDuplicate->message = WM_SDI_OP_HIDDEN_COMMAND;
				pDuplicate->pParam = (LPARAM)pMsg->nParam2;
			}
			else
			{
				/*if(pDuplicate->nParam1 == 0x5003)
					TRACE(_T(""));*/

				pDuplicate->message = WM_SDI_OP_SET_PROPERTY_VALUE;
				pDuplicate->pParam = pMsg->pParam;
			}

			pExist->SdiAddMsg(pDuplicate);

			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}

			if(pDSCID)
			{
				delete pDSCID;
				pDSCID = NULL;
			}

			return;
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}
}

void CDSCMgr::OnEventNetConnect(ESMEvent* pMsg)	
{
	ESMEvent* pCopyMsg = NULL;

	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pExist = NULL;

	while(nAll--)
	{
		pExist = m_pDSCViewer->GetItemData(nAll);
		if(pExist->GetDeviceDSCID().GetLength() == 5)
		{
			pCopyMsg = new ESMEvent();
			pCopyMsg->message	= WM_RS_RC_NETWORK_CONNECT;
			pCopyMsg->pDest	= (LPARAM)pExist;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pCopyMsg);
		}
		Sleep(10);
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

}

void CDSCMgr::OnEventNetPreRecInitGH5( ESMEvent* pMsg )
{
	int nAll = m_pDSCViewer->GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 21"));
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = m_pDSCViewer->GetGroup(nAll);
		if(!pGroup)
			continue;

		pGroup->SetPreRecInitGH5Group();
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventNetPreRecInitGH5Result( ESMEvent* pMsg )
{		
	int nResult = (int)pMsg->nParam1;

	if( ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		CString* pDSCID = (CString*)pMsg->pDest;
		CString strID = pDSCID->GetString();

		CObArray arDSCList;
		ESMGetDSCList(&arDSCList);
		CDSCItem* pItem = NULL;
		for( int i =0 ;i < arDSCList.GetSize(); i++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(i);
			if (pItem->GetDeviceDSCID() != strID)
				continue;

			if (nResult == SDI_ERR_OK)
			{
				pItem->SetInitPreRec(TRUE);
			}
			else
			{
				pItem->SetInitPreRec(FALSE);
				ESMLog(1, _T("##### Pre Rec InitGH5Result fail!! [%s], result: [0x%X]"), strID, nResult);
			}
		}

		if(pDSCID)
		{
			delete pDSCID;
			pDSCID = NULL;
		}

	}
	else if( ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
	{
		CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
		ESMLog(1, _T("OnEventNetPreRecInitGH5Result [%s], result: [0x%X]"), pDSCItem->GetDeviceDSCID(), nResult);

		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message	= WM_RS_RC_PRE_REC_INIT_GH5_RESULT;
		pMsg->nParam1	= nResult;
		pMsg->pDest	= (LPARAM)pDSCItem;
		::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventNetGetTick(ESMEvent* pMsg)
{
	int nAll = m_pDSCViewer->GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 21"));
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = m_pDSCViewer->GetGroup(nAll);
		if(!pGroup)
			continue;

		pGroup->SetSync(FALSE);
		pGroup->GetTickGroup();
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventNetGetTickCmd(ESMEvent* pMsg)
{
	int nAll = m_pDSCViewer->GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 21 ---"));
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		return;
	}

	CDSCGroup* pGroup = NULL;
	
	//ESMLog(5,_T("########### OnEventNetGetTickCmd  ---  gettick count:%d"), pMsg->nParam1);

	while(nAll--)
	{
		pGroup = m_pDSCViewer->GetGroup(nAll);
		if(!pGroup)
			continue;
		
		pGroup->GetTickCmdGroup(pMsg->nParam1);
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventRcvGetTickCmdResult(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;	

	int nCount = 0;
	CString strDSCID;
	//strDSCID.Format(_T("%d"), pMsg->nParam1);
	//BOOL bGetTickResult = pMsg->nParam2;
	strDSCID = pDSCID->GetString();

	//ESMLog(5, _T("##### OnEventRcvGetTickCmdResult... ID[%d], result[%d], pDSCID:%s"), pMsg->nParam1, pMsg->nParam2, strDSCID);

	BOOL bGetTickFinish = TRUE;
	int nAll = m_pDSCViewer->GetItemCount();
	nCount = nAll;

	while(nAll--)
	{
		CDSCItem* pDscItem = m_pDSCViewer->GetItemData(nAll);

		if(pDscItem->GetDeviceDSCID() == strDSCID || pDscItem->m_bDelete == TRUE || (pDscItem->GetDSCStatus() != SDI_STATUS_REC))
		{
			//pDscItem->SetReSyncReady(bGetTickResult);
			pDscItem->SetReSyncGetTickReady(TRUE);

			//ESMLog(5, _T("##### OnEventRcvGetTickCmdResult... ID[%s], result[%d]"), strDSCID, pMsg->nParam2);
		}
	}

	while(nCount--)
	{
		CDSCItem* pDscItem = m_pDSCViewer->GetItemData(nCount);
		if (pDscItem->GetReSyncGetTickReady() == FALSE)
		{
			//ESMLog(5, _T("##### OnEventRcvGetTickCmdResult... ID[%s], status[%s, %d]"), pDscItem->GetDeviceDSCID(), pDscItem->GetStatusString(), pDscItem->GetDSCStatus());
			bGetTickFinish = FALSE;
			break;
		}
	}
	
	if (bGetTickFinish)
	{
		ESMLog(5, _T("############### Finish GetTick result !! "));
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventSetMovieSaveCheck(ESMEvent* pMsg)
{
	m_pDSCViewer->SetMovieSaveCheck(pMsg->nParam1);
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventSetFocus(ESMEvent* pMsg)
{
	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pDscItem = NULL;
	CString* pDSCID = (CString*)pMsg->pDest;
	FocusPosition* pFocus = (FocusPosition*)pMsg->pParam;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	BOOL bSave = pMsg->nParam3;
	int nCurFocus = pFocus->current;
	int nMaxFocus = pFocus->max;
	int nMinFocus = pFocus->min;
	delete pFocus;
	pFocus = NULL;
	while(nAll--)
	{
		pDscItem = m_pDSCViewer->GetItemData(nAll);
		if(pDscItem->GetDeviceDSCID() == pDSCID->GetString())
		{
#if 0
			if( m_pDSCViewer->GetFocusState() == FALSE)		// Focus Set
				pDscItem->SetDSCStatus(SDI_STATUS_FOCUS_OK);
			else											// Focus Get 및 데이터 저장	
#endif
				//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
				//CMiLRe 20160204 Focus All Setting 시 Focus 저장되는 버그 수정
				if(!bSave)
				{
					pDscItem->LoadFocus();
					int nGap = abs(nCurFocus - pDscItem->m_nCurFocus);
					if(nGap > 2)
					{
						ESMLog(5, _T("<< [%s] Reference Focus : %d,     Camera Focus : %d    >>>>>>>>>GAP  = %d "), pDscItem->GetDeviceDSCID(), pDscItem->m_nCurFocus,	nCurFocus,	pDscItem->m_nCurFocus-nCurFocus);	
					}
					/*else
					{
						ESMLog(5, _T("<< [%s] Focus Setting (Focus Valus = %d/%d) Succeed!!! >>>>>>>>>>"), pDscItem->GetDeviceDSCID(), nCurFocus, pDscItem->m_nCurFocus);
					}*/
				}
				else
				{
					pDscItem->SetFocusData(nCurFocus, nMaxFocus, nMinFocus);
				}		
		}
	}
	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::CamConnectInfo(int nRemoteId)
{
	CDSCGroup* pGroup = m_pDSCViewer->GetGroupData(nRemoteId);
	if( pGroup == NULL)
		return ;
	int nCamCount = pGroup->GetCount();
	CDSCItem* pDSCitem;
	int nStatus = -1;
	int nAllStatus = -1;
	int nIndex =0;
	for( nIndex =0 ; nIndex < nCamCount; nIndex++)
	{
		pDSCitem = pGroup->GetDSC(nIndex);
		nStatus = pDSCitem->GetDSCStatus();
		if( nStatus != SDI_STATUS_CONNECTED)
			break;
	}
	if( nIndex == nCamCount)
		nAllStatus = 1;
	else
		nAllStatus = 0;

	if( nAllStatus == 1)
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message	= WM_ESM_NET_DSCCONSTATUS;
		pMsg->nParam1	= (LPARAM)nRemoteId;
		::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}
}

void CDSCMgr::OnEventFileTransferFail(ESMEvent* pMsg)
{
// 	CDSCItem* pDSCitem = (CDSCItem*)pMsg->pParent;
// 	ESMLog(1, _T("Camera[%s] File Trancfer Error!!!!"), pDSCitem->GetDeviceDSCID());


// 	ESMEvent* pMsg = NULL;
// 	pMsg = new ESMEvent();
// 	pMsg->message	= WM_ESM_NET_DSCCONSTATUS;
// 	pMsg->nParam1	= (LPARAM)nRemoteId;
// 	::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

//	if(pMsg)
//	{
//		delete pMsg;
//		pMsg = NULL;
//	}
}

void CDSCMgr::OnEventFileTransferInfo(ESMEvent* pMsg)
{
	CDSCItem* pDSCitem = (CDSCItem*)pMsg->pParent;
	if(pDSCitem)
	{
		pDSCitem->AddMovieSize(pMsg->nParam1);
		pDSCitem->SetMovieFileClose(pMsg->nParam2);
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

//-- 2014-09-15 changdo
//-- Set Capture Exception Status
void CDSCMgr::OnEventSendCaptureFail(ESMEvent* pMsg)
{
	CDSCItem* pDSCitem = (CDSCItem*)pMsg->pParent;
	if(pDSCitem)
	{
		pDSCitem->SetCaptureExcept(TRUE);

		//-- Exception Dsc
		//-- Send To Server
		ESMEvent* pNewMsg	= new ESMEvent();
		pNewMsg->pDest		= (LPARAM)pDSCitem;
		pNewMsg->message	= WM_ESM_NET_EXCEPTIONDSC;
		::SendMessage(m_hParentWnd, WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventSensorSyncOut(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;
	CString strID = pDSCID->GetString();
	
	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}

	if( pMsg->nParam1 == -1)	// case : Frame Index 오류 :리스트 삭제
	{
		CString strRecProfile;
		if (strID)
		{
			strRecProfile = ESMGetRecordingInfoString(_T("RecProfile"), _T(""));
			EnterCriticalSection(&m_csDataLock);
			pair< multimap<CString,stSensorData>::iterator, multimap<CString,stSensorData>::iterator > iter_pair;
			iter_pair = m_mapSyncFailDsc.equal_range(strRecProfile);
			multimap <CString,stSensorData>::iterator iter;
			for(iter=iter_pair.first ; iter!=iter_pair.second ;)
			{
				if( strID == iter->second.strDscId)
					m_mapSyncFailDsc.erase(iter++);
				else
					iter++;
			}
			LeaveCriticalSection(&m_csDataLock);
		}

		ESMLog(0, _T("[%s] Sensor Sync Frmae Error : Expected Frame [%d],  Real Frame [%d]"), strID, pMsg->nParam2,  pMsg->nParam3);
	}      
	else // case : SensorSync Error : 로그만 표시
	{
		//ESMLog(0, _T("[%s] Sensor Sync Gap [%.1lf]ms --> expected [%.1lf]ms : CaptureTime[%.1lf], Interval [%.1lf]ms"), strID, (double)pMsg->nParam1 * 0.1, (double)sync_frame_time_after_sensor_on * 0.1, (double)pMsg->nParam2 * 0.1, (double)pMsg->nParam3 * 0.1);
		ESMLog(0, _T("[%s] Sensor Sync Gap [%.1lf]ms --> expected [%.1lf]ms : CaptureTime[%.1lf], Interval [%.1lf]ms"), strID, (double)pMsg->nParam1, (double)sync_frame_time_after_sensor_on * 0.1, (double)pMsg->nParam2 , (double)pMsg->nParam3);
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventSyncDataSet()
{
	CString strFailDsc;
	if(!m_pDSCViewer)
		return;

	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pItem = NULL;

	pair< multimap<CString,stSensorData>::iterator, multimap<CString,stSensorData>::iterator > iter_pair;
	multimap <CString,stSensorData>::iterator iter;

	CString strRecProfile = ESMGetFrameRecord();
	iter_pair = m_mapSyncFailDsc.equal_range(strRecProfile);

	if(m_mapSyncFailDsc.size() <= 0)
		return;

	EnterCriticalSection(&m_csDataLock);
	// 데이터 평균 및 중간 데이터 확인후 Sensor Out 확정
	vector<int> arrrSensorData;
	int nGap = 0, nTotalNum = 0;
	for(iter=iter_pair.first ; iter!=iter_pair.second ; ++iter)
	{
		nGap = iter->second.nTimeStamp - iter->second.nStartTime;
		arrrSensorData.push_back(nGap);
	}
	sort(arrrSensorData.begin(), arrrSensorData.end());
	int nSelectNum = 0, nSelectCount = 0,nMaxNum = 0, nMaxCount = 0;
	for ( vector<int>::iterator iter = arrrSensorData.begin(); iter != arrrSensorData.end(); ++iter)  // i++ 증가 연산자가 일부러 없습니다.
	{
		if( *iter == nSelectNum)
			nSelectCount++;
		else
		{
			if( nMaxCount < nSelectCount)
			{
				nMaxNum = nSelectNum;
				nMaxCount = nSelectCount;
			}

			nSelectNum = *iter;
			nSelectCount = 1;
		}
	}
	if( nMaxCount <= nSelectCount)
	{
		nMaxNum = nSelectNum;
		nMaxCount = nSelectCount;
	}

	//ESMLog(5, _T("nSelectedTimeStamp [%d], nMaxCount [%d]\n"), nMaxNum, nMaxCount);
	// 크게 틀어진 데이터를 솎아 낸다.
	for ( vector<int>::iterator iter = arrrSensorData.begin(); iter != arrrSensorData.end();)  // i++ 증가 연산자가 일부러 없습니다.
	{
		if (  abs( *iter - nMaxNum ) > 3 )
		{
			TRACE(_T("arrrSensorData.erase[%d]\n"), *iter);
			iter = arrrSensorData.erase(iter);  // erase의 반환값을 i에 저장해서 유효성을 유지합니다.
		}
		else
			++iter;
	}

	map<CString, int> nArrAvgGap;
	nTotalNum = 0;
	if( arrrSensorData.size() > 0)
	{
		for( int i =0 ;i < arrrSensorData.size(); i++)
			nTotalNum += arrrSensorData.at(i);

		int nSize = arrrSensorData.size();
		int nAvgNum = 0;
		nAvgNum = nTotalNum / nSize;

		//초기화
		for( int i =0 ;i < nAll; i++)
		{
			pItem = m_pDSCViewer->GetItemData(i);
			if(pItem)
				pItem->SetSensorSync(FALSE);
		}

		//jhhan 190819 Sync
		int nSensorTime = 2;
		if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
			nSensorTime = ESMConvertTime(2);
		else if(pItem->GetDeviceModel().CompareNoCase(_T("NX1")) == 0)
			nSensorTime = 2;
		else
		{
			if(ESMGetDevice() == SDI_MODEL_NX1)
				nSensorTime = 2;
			else if(ESMGetDevice() == SDI_MODEL_GH5)
				nSensorTime = ESMConvertTime(2);
			else
			{
				int nDevice = 0;
				nDevice = MessageBoxEx(NULL, _T("Select your camera.\n\nYes : GH5, No : NX1"), _T("WARNING"), MB_YESNO|MB_ICONWARNING, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US));
				if(nDevice == IDYES)
				{
					ESMSetDevice(SDI_MODEL_GH5);
					nSensorTime = ESMConvertTime(2);
				}
				else
				{
					ESMSetDevice(SDI_MODEL_NX1);
					nSensorTime = 2;
				}
			}
		}


		//Test
		/*for( int i =0 ;i < nAll; i++)
		{
			pItem = m_pDSCViewer->GetItemData(i);
			BOOL bCheck = pItem->GetSensorSync();
			if(bCheck)
			{
				TRACE(_T("Error"));
			}
		}*/

		// SensorData Set
		for(iter=iter_pair.first ; iter!=iter_pair.second ; ++iter)
		{
			nGap = iter->second.nTimeStamp - iter->second.nStartTime;
			//if( abs(nGap - nAvgNum) < sync_decision_sensor_on )
			//if( abs(nGap - nAvgNum) <= 1 )
			if( abs(nGap - nAvgNum) <= nSensorTime ) //2ms over sensor out
			{
				//SensorOk
				for( int i =0 ;i < nAll; i++)
				{
					pItem = m_pDSCViewer->GetItemData(i);
					if(pItem && pItem->GetDeviceDSCID() == iter->second.strDscId)
						pItem->SetSensorSync(TRUE);
				}
			}
		}

		for( int i =0 ;i < nAll; i++)
		{
			pItem = m_pDSCViewer->GetItemData(i);
			if(pItem)
			{
				if(!pItem->GetSensorSync())
				{
					for(iter=iter_pair.first; iter!=iter_pair.second; ++iter)
					{
						if(iter->second.strDscId == pItem->GetDeviceDSCID())
						{
							nGap = iter->second.nTimeStamp - iter->second.nStartTime;
							ESMLog(5, _T("SensorOut [%s], nStartTime[%u], nTimeStamp[%u] Gap[%d] Avg[%d] AvgGap[%d]"), 
								pItem->GetDeviceDSCID(), 
								iter->second.nStartTime, 
								iter->second.nTimeStamp, 
								iter->second.nTimeStamp - iter->second.nStartTime, 
								nAvgNum, 
								nAvgNum-nGap);
							pItem->SetDSCStatus(SDI_STATUS_ERROR);	//jhhan 180829
						}
					}
				}
				else
				{
					//ESMLog(5,_T("[TC5] Start"));
					for(iter=iter_pair.first; iter!=iter_pair.second; ++iter)
					{
						if(iter->second.strDscId == pItem->GetDeviceDSCID())
						{
							nGap = iter->second.nTimeStamp - iter->second.nStartTime;
							if(ESMGetValue(ESM_VALUE_TEST_MODE)== TRUE)
							{
								int nAvgGap = nAvgNum-nGap;
								if(nAvgGap > 4)
								{
									iter->second.nStartTime = iter->second.nTimeStamp+4+abs(nAvgNum);
									nAvgGap = 4;
								}
								else if(nAvgGap < -4)
								{
									iter->second.nStartTime = iter->second.nTimeStamp-4+abs(nAvgNum);
									nAvgGap = -4;
								}
							}
						}
					}
				}
			}			
		}
		if(ESMGetValue(ESM_VALUE_TEST_MODE) == TRUE)
			ESMLog(5,_T("[TC5] Start"));
		for( int i =0 ;i < nAll; i++)
		{
			pItem = m_pDSCViewer->GetItemData(i);
			if(pItem)
			{
				if(!pItem->GetSensorSync())
				{
					for(iter=iter_pair.first; iter!=iter_pair.second; ++iter)
					{
						if(iter->second.strDscId == pItem->GetDeviceDSCID())
						{
							nGap = iter->second.nTimeStamp - iter->second.nStartTime;
							ESMLog(5, _T("SensorOut [%s], nStartTime[%u], nTimeStamp[%u] Gap[%d] Avg[%d] AvgGap[%d]"), 
								pItem->GetDeviceDSCID(), 
								iter->second.nStartTime, 
								iter->second.nTimeStamp, 
								iter->second.nTimeStamp - iter->second.nStartTime, 
								nAvgNum, 
								nAvgNum-nGap);
							pItem->SetDSCStatus(SDI_STATUS_ERROR);	//jhhan 180829
						}
					}
				}
				else
				{
					
					for(iter=iter_pair.first; iter!=iter_pair.second; ++iter)
					{
						if(iter->second.strDscId == pItem->GetDeviceDSCID())
						{
							nGap = iter->second.nTimeStamp - iter->second.nStartTime;
							if(ESMGetValue(ESM_VALUE_TEST_MODE)== TRUE)
							{
								ESMLog(5, _T("SensorIn [%s], nStartTime[%u]ms, nTimeStamp[%u]ms Gap[%d]ms Avg[%d]ms AvgGap[%d]ms"), 
								pItem->GetDeviceDSCID(), 
								iter->second.nStartTime, 
								iter->second.nTimeStamp, 
								iter->second.nTimeStamp - iter->second.nStartTime, 
								nAvgNum, 
								nAvgNum-nGap);

								nArrAvgGap.insert( pair<CString, int>(
									m_pDSCViewer->GetItemData(i)->GetDeviceDSCID(), nAvgNum - nGap
									));	
							}
						}
					}
				}
			}			
		}
		if(ESMGetValue(ESM_VALUE_TEST_MODE) == TRUE)
			ESMLog(5,_T("[TC5] End"));
		
		ESMSetArrGapAvg(nArrAvgGap);
	}

	LeaveCriticalSection(&m_csDataLock);
}

void CDSCMgr::SyncDataInsert(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;
	CString strID = pDSCID->GetString();
	CString strRecProfile;
	stSensorData SensorData;
	if (strID)
	{
		strRecProfile = ESMGetRecordingInfoString(_T("RecProfile"), _T(""));
		EnterCriticalSection(&m_csDataLock);
		SensorData.strDscId = strID;
		SensorData.nStartTime = pMsg->nParam1;
		SensorData.nTimeStamp = pMsg->nParam2;
		m_mapSyncFailDsc.insert( make_pair(strRecProfile,SensorData) );
		LeaveCriticalSection(&m_csDataLock);
		
		//jhhan 16-11-22 SensorData Check
		//ESMLog(5, _T("SyncDataInsert [%s][%d][%d]"), strID, pMsg->nParam1, pMsg->nParam2);
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}
}

void CDSCMgr::SyncDataDelete(CString strRecProfile)
{
	EnterCriticalSection(&m_csDataLock);
	m_mapSyncFailDsc.erase(strRecProfile);
	LeaveCriticalSection(&m_csDataLock);
	ESMLog(5, _T("SyncDataDelete [%s]"), strRecProfile);
}

void CDSCMgr::OnEventCaptureDelay(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;
	CString strID = pDSCID->GetString();
	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}
	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pDscItem = NULL;

	//-- Find DSC from ID 
	while(nAll--)
	{
		pDscItem = m_pDSCViewer->GetItemData(nAll);
		if(pDscItem->GetDeviceDSCID() == strID)
		{
			pDscItem->SetDelayTime(pMsg->nParam1);
			pDscItem->SetSensorOnDesignation(pMsg->nParam2);
			pDscItem->SetPauseMode(FALSE);//(CAMREVISION)
			pDscItem->SetRecordStep(pMsg->nParam3);
			break;
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventNetGetItemFocus(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;
	CString strID = pDSCID->GetString();
	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}
	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pExist = NULL;

	while(nAll--)
	{
		pExist = m_pDSCViewer->GetItemData(nAll);
		if( strID == pExist->GetDeviceDSCID())
		{
			ESMEvent* pSendMsg = NULL;
			pSendMsg = new ESMEvent;
			pSendMsg->message = WM_SDI_OP_GET_FOCUS_VALUE;
			pExist->SdiAddMsg(pSendMsg);
			break;
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-09-17
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCMgr::OnEventErrorFrame(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;

	if(pDSCID == NULL)
	{
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		return;
	}

	CString strID = pDSCID->GetString();
	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}

	int nIndex = pMsg->nParam1;
	int nInterval = pMsg->nParam2;
	float	nTimeStampInterval = (float)(nInterval)/(float)1000;
	ValidFrame* pValidFrameExcept = NULL;
	ESMEvent* pMsgExcept = NULL;	
	float nTime1, nTime2;

	nTime1 = ESMGetFrameTime(nIndex);	

	ESMLog(5, _T("[%s] Error Frame cnt:[%d][%.3f], Interval:[%.3f]"), 
		strID,		
		nIndex,
		(float)(nTime1/(float)1000), 
		nTimeStampInterval
		);

	

	//-- 2014/09/20 hongsu
	//-- Check Wrong Interval (micro second)
	if(nInterval> 1000)
	{
		//ESMLog(5,_T("##[SERVER][%s][%.3f] Intreval Between TickCount and Timestamp [%d] ms"),strID, (float)(nTime1/(float)1000),  nInterval/1000);
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		return;
	}

	if(nInterval< 10)
	{
		//ESMLog(5,_T("##[SERVER][%s][%.3f] Check Time Interval [%d] ms"),strID, (float)(nTime1/(float)1000)		, nInterval);
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		return;
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

// 	int nSkipCnt;	// Frame Skip Count
// 	nSkipCnt = (nInterval - movie_next_frame_time_margin) /movie_next_frame_time;
// 	nSkipCnt+=2;
// 
// 	int nAllSkipCnt = nSkipCnt;
// 	
// 	//	Error Frame
// 	while(nSkipCnt--)
// 	{			
// 		pValidFrameExcept = new ValidFrame;
// 		pValidFrameExcept->strDSC = strID;
// 		pValidFrameExcept->nCurrentIndex	= nIndex-nSkipCnt;
// 		pValidFrameExcept->nCorrectIndex	= -1;
// 		//-- TRACE
// 		nTime1 = ESMGetFrameTime(pValidFrameExcept->nCurrentIndex);	
// 		ESMLog(5,_T("[SERVER][%s][%.03f] Except Frame Interval [%d]ms [%d/%d]"),
// 			pValidFrameExcept->strDSC			,
// 			(float)(nTime1/(float)1000)		, 
// 			//pValidFrameExcept->nCurrentIndex,
// 			nInterval,
// 			nSkipCnt+1,
// 			nAllSkipCnt);
// 
// 		AddValidFrame(pValidFrameExcept);
// 	}
// 
// 	
// 	//-- Check Error Frame Info (39,40,41)
// 	if( (nInterval < movie_next_frame_time + movie_next_frame_time_margin) &&
// 		(nInterval >= movie_next_frame_time - movie_next_frame_time_margin) )
// 	{		
// 		ValidFrame* pValidFrameCorrect = new ValidFrame;
// 		pValidFrameCorrect->strDSC =strID;
// 		pValidFrameCorrect->nCurrentIndex	= nIndex;
// 		pValidFrameCorrect->nCorrectIndex	= nIndex-1;
// 		//-- TRACE
// 		nTime1 = ESMGetFrameTime(pValidFrameCorrect->nCurrentIndex);
// 		nTime2 = ESMGetFrameTime(pValidFrameCorrect->nCorrectIndex);
// 		ESMLog(5,_T("[SERVER][%s][%.03f]->[%.03f]  Change Frame"),			
// 			pValidFrameCorrect->strDSC			,
// 			(float)(nTime1/(float)1000)		, 
// 			(float)(nTime2/(float)1000)		
// 			//pValidFrameCorrect->nCurrentIndex	,
// 			//pValidFrameCorrect->nCorrectIndex	);
// 			);
// 
// 		AddValidFrame(pValidFrameCorrect);
// 	}	
}

void CDSCMgr::OnEventAddFrame(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;
	CString strID = pDSCID->GetString();
	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}

	int nIndex = pMsg->nParam1;
	int nInterval = pMsg->nParam2;

	float nTime;
	//-- TRACE
	nTime = ESMGetFrameTime(nIndex);	

	ESMLog(5, _T(">> [%s] Add Frame Time:[%d][%.3f] Interval[%d]"), 
		strID,		
		nIndex,
		(float)(nTime/(float)1000),
		nInterval);	

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

}

void CDSCMgr::OnEventSkipFrame(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;
	CString strID = pDSCID->GetString();
	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}

	int nIndex = pMsg->nParam1;
	int nInterval = pMsg->nParam2;

	float nTime;
	//-- TRACE
	nTime = ESMGetFrameTime(nIndex);	

	ESMLog(5, _T("<< [%s] Skip Frame Time:[%d][%.3f] Interval[%d]"), 
		strID,		
		nIndex,
		(float)(nTime/(float)1000),
		nInterval);	

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}
void CDSCMgr::OnEventValidSkip(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;
	CString strID = pDSCID->GetString();
	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}

	int nIndex = pMsg->nParam1;

	float nTime;
	//-- TRACE
	nTime = ESMGetFrameTime(nIndex);	

	ESMLog(5, _T("<< [%s] Skip Frame Time:[%d][%.3f]"), 
		strID,		
		nIndex,
		(float)(nTime/(float)1000));

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}
void CDSCMgr::OnEventRecordFinish(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;
	CString strID = pDSCID->GetString();
	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}

	//ESMLog(5, _T("########### Record Finish %s"), strID);

	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pExist = NULL;
	while(nAll--)
	{
		pExist = m_pDSCViewer->GetItemData(nAll);
		if( strID == pExist->GetDeviceDSCID())
		{
			pExist->SetRecordReady(TRUE);
			break;
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventCopyToServer(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;
	CString strID = pDSCID->GetString();
	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
	//ESMLog(5, _T("Copy To Server[%s]"), strID);
}

void CDSCMgr::OnEventExceptionDsc(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;

	if(pDSCID == NULL)
		return;

	CString strID = pDSCID->GetString();
	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}

	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pExist = NULL;

	while(nAll--)
	{
		pExist = m_pDSCViewer->GetItemData(nAll);
		if( strID == pExist->GetDeviceDSCID())
		{
			pExist->SetCaptureExcept(TRUE);
			break;
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}
void CDSCMgr::OnEventInformSensorOK(ESMEvent*pMsg)
{
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pDest;
	CString strMovieID = pDSCItem->GetDeviceDSCID();

	int nAll = m_pDSCViewer->GetItemCount();
	while(nAll--)
	{
		pDSCItem = m_pDSCViewer->GetItemData(nAll);
		if( strMovieID == pDSCItem->GetDeviceDSCID())
		{

			pDSCItem->SetSensorSync(TRUE);

			ESMEvent* pNewMsg	= new ESMEvent();
			pNewMsg->pDest		= (LPARAM)pDSCItem;
			pNewMsg->nParam1	= pMsg->nParam1;
			pNewMsg->nParam2	= pMsg->nParam2;
			pNewMsg->nParam3	= pMsg->nParam3;
			pNewMsg->message	= WM_ESM_NET_SENSORSYNCTIME;
			::SendMessage(m_hParentWnd, WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
			break;
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}