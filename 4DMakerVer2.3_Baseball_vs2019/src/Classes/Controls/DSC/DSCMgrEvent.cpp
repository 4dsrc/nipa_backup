////////////////////////////////////////////////////////////////////////////////
//
//	DSCMgrEvent.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCMgr.h"
#include "DSCViewer.h"
#include "ESMUtil.h"
#include "ESMIni.h"
#include "SdiSingleMgr.h"
#include "ESMFileOperation.h"
#include <sys/timeb.h>
#include <time.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//----------------------------------------------------------------
//-- 2013-04-22 hongsu.jung
//-- Get Event
//----------------------------------------------------------------
void CDSCMgr::OnEventOpenSession(ESMEvent* pMsg)
{
	CDSCItem* pItem = (CDSCItem*)pMsg->pParent;
	if(pItem)
	{
		switch(pItem->GetType())
		{
		case DSC_LOCAL	:	pItem->SetDSCStatus(SDI_STATUS_CONNECTED);	break;
		case DSC_REMOTE	:	break;
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventCapture(ESMEvent* pMsg)
{
	CDSCItem* pItem = (CDSCItem*)pMsg->pDest;

	switch(pItem->GetType())
	{
	case DSC_LOCAL	:	
		{
			if(!pItem->m_pSdi)
			{
				delete pMsg;
				pMsg = NULL;
				return;
			}
			pItem->SdiAddMsg(pMsg);	
			break;
		}
	case DSC_REMOTE	:	
		break;
	}	
}

void CDSCMgr::OnEventLiveviewReceive(ESMEvent* pMsg)
{
	ESMEvent* pMsgBuffer	= new ESMEvent();
	pMsgBuffer->message	= WM_ESM_VIEW_LIVEVIEW_BUFFER;
	pMsgBuffer->nParam2	= pMsg->nParam2;
	pMsgBuffer->pParam	= pMsg->pParam;
	::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsgBuffer);

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEvnetErrMsg(ESMEvent* pMsg)
{
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	if(pDSCItem)
	{
		ESMEvent* pSendMsg = new ESMEvent();

		if(pMsg->message == WM_SDI_EVENT_ERR_MSG)
			pSendMsg->message	= WM_ESM_NET_ERROR_MSG;
		else
			pSendMsg->message	= WM_ESM_NET_ERROR_MSG_GH5;

		pSendMsg->nParam1	= pMsg->nParam1;
		pSendMsg->nParam2	= pMsg->nParam2;
		pSendMsg->nParam3	= pMsg->nParam3;
		pSendMsg->pDest		= (LPARAM)pDSCItem;

		::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pSendMsg);

		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
	}
}

void CDSCMgr::OnEventCaptureDone(ESMEvent* pMsg)
{
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	//-- 2013-02-15 hongsu.jung
	//-- Get SDI Manager
	if(pDSCItem)
	{
		CString strFolder, strDate = _T(""), strPath, strBackupPath;

		//Create Rampath Folder
		CreateDirectory(ESMGetClientRamPath(), NULL);
		strFolder.Format(_T("%s\\"),ESMGetClientRamPath());
		
		int nCount = 0;
		while(1)
		{
			strDate.Format(_T("%s"), ESMGetFrameRecord());
			if(strDate.IsEmpty())
			{
				ESMLog(1, _T("#########Empty ESMGetFrameRecord()"));
				nCount++;
				if(nCount > 100)
				{
					ESMLog(0,_T("Empty ESMGetFrameRecord()"));
					break;
				}
				Sleep(10);
			}
			break;
		}
		
		strFolder.Append(strDate);
		CreateDirectory(strFolder, NULL);
		//Create Local Folder
		strFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_FILE));
		while(1)
		{
			strDate.Format(_T("%s"), ESMGetFrameRecord());
			if(strDate.IsEmpty())
			{
				ESMLog(1, _T("#########Empty ESMGetFrameRecord()"));
				nCount++;
				if(nCount > 100)
				{
					ESMLog(0,_T("Empty ESMGetFrameRecord()"));
					break;
				}
				Sleep(10);
			}
			break;
		}
		strFolder.Append(strDate);
		CreateDirectory(strFolder, NULL);

		strPath.Format(_T("%s\\%s\\%s"),ESMGetClientRamPath(), ESMGetFrameRecord(),pDSCItem->GetDeviceDSCID());
		strBackupPath.Format(_T("%s\\%s\\%s"),ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), pDSCItem->GetDeviceDSCID());

		strPath.Append(_T("@"));
		strPath.Append(strBackupPath);

		//strPath.Format(_T("%s\\%s\\%s"),ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), pDSCItem->GetDeviceDSCID());
		//strPath.Format(_T("%s\\%s\\%s"),ESMGetClientRamPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), pDSCItem->GetDeviceDSCID());
		//-- Save Capture Image
		
		TCHAR* cPath = new TCHAR[1+strPath.GetLength()];
		_tcscpy(cPath, strPath);

		ESMEvent* pSendMsg = new ESMEvent();
		pSendMsg->message = WM_SDI_OP_CAPTURE_IMAGE_RECEIVE;
		pSendMsg->pParam = (LPARAM)cPath;
		pSendMsg->nParam1 = TRUE;
		if(pDSCItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		{
			pSendMsg->nParam2 = 0;
			pSendMsg->nParam3 = (int)pSendMsg->pParam;
		}
		else
		{
			pSendMsg->nParam2 = pMsg->nParam2;
			pSendMsg->nParam3 = pMsg->nParam3;
		}
		pDSCItem->SdiAddMsg(pSendMsg);

		pSendMsg = new ESMEvent();
		pSendMsg->message	= WM_SDI_OP_IMAGERECEIVECOMPLETE;
		pDSCItem->SdiAddMsg(pSendMsg);

		//-- Change Status
		pDSCItem->SetDSCStatus(SDI_STATUS_GET_FILE);
		ESMLog(5,_T("Start Save Picture [%s][%s], Param2[%d], Param3[%d]"),pDSCItem->GetDeviceDSCID(),strPath, pMsg->nParam2, pMsg->nParam3);
		//-- Update MovieEditor Bar
		m_pDSCViewer->m_pFrameSelector->DrawEditor();
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventRecordStop(ESMEvent* pMsg)
{
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	if(pDSCItem)
	{
		//pDSCItem->m_nFileIndex = 0;
		//-- Change Status
		pDSCItem->SetDSCStatus(SDI_STATUS_REC_FINISH);

		ESMLog(5, _T("Send Record Finish!!"));

		ESMEvent* pMsg= new ESMEvent();
		pMsg->pDest		= (LPARAM)pDSCItem;
		pMsg->message	= WM_ESM_NET_RECORDFINISH;
		::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)pMsg);


	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}
void CDSCMgr::OnEventFileTransfer(ESMEvent* pMsg)
{
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;

	//-- 2013-02-15 hongsu.jung
	//-- Get SDI Manager
	
	if(pDSCItem)
	{
		if(pDSCItem->m_bMovieCancel)
		{
			pDSCItem->SetDSCStatus(SDI_STATUS_REC_FINISH);
			m_pDSCViewer->m_pFrameSelector->DrawEditor();
		}
		else if(pDSCItem->m_bMovieSaveCheck)
		{
			if( pDSCItem->m_strPrevRecordProfile != ESMGetFrameRecord())		// 촬영 시작 
			{
				pDSCItem->m_nFileIndex = 0;
			}
			pDSCItem->m_strPrevRecordProfile = ESMGetFrameRecord();
			CString strPath, strFile, strRemovePath;
			
			//jhhan File Temp

#ifdef _FILE_TEMP
			if(ESMGetRecordFileSplit())
				strPath.Format(_T("%s\\%s\\%s"),ESMGetClientRamPath(), ESMGetFrameRecord(), _T("Temp"));
			else
				strPath.Format(_T("%s\\%s"),ESMGetClientRamPath(), ESMGetFrameRecord());
#else
			strPath.Format(_T("%s\\%s"),ESMGetClientRamPath(), ESMGetFrameRecord());
#endif
			CESMFileOperation fo;
			if( !fo.CheckPath(strPath))
				fo.CreateFolder(strPath);

#if 1 //--2015-10-22 joonho.kim GOP
			strFile.Format(_T("%s\\%s_%d.mp4"), strPath, pDSCItem->GetDeviceDSCID(), pDSCItem->m_nFileIndex);
			strRemovePath = strPath;
#else
			strFile.Format(_T("%s\\%s.mp4"), strPath, pDSCItem->GetDeviceDSCID());
#endif

			strPath = strFile;
			//-- Save Capture Image
			CString strSavePath;
			strSavePath.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_LOCALMOVIE), ESMGetFrameRecord());
			

			if( !fo.CheckPath(strSavePath))
				fo.CreateFolder(strSavePath);
			CString strFullPath;
			strFullPath.Format(_T("%s\\%s_%d.mp4"), strSavePath, pDSCItem->GetDeviceDSCID(), pDSCItem->m_nFileIndex);
			strPath.Append(_T("@"));
			strPath.Append(strFullPath);

			

			//경로 추가
			CString strRTSP = _T("0");
			if(ESMGetValue(ESM_VALUE_RTSP))
				strRTSP = _T("1");

			strPath.Append(_T("@"));
			strPath.Append(strRTSP);
			
//			CString str4DASaveAs = ESMGet4DAPath(pDSCItem->GetDeviceDSCID());
//			if(str4DASaveAs.IsEmpty() != TRUE)
//			{
//				CString str4DA;
//#if _SAME_DEFAULT_PATH
//				str4DA.Format(_T("\\\\%s\\%s"), str4DASaveAs, strSavePath.Mid(2, strSavePath.GetLength()));
//				if( !fo.CheckPath(str4DA))
//					fo.CreateFolder(str4DA);
//				str4DA.Format(_T("\\\\%s\\%s\\%s_%d.mp4"), str4DASaveAs, strSavePath.Mid(2, strSavePath.GetLength()), pDSCItem->GetDeviceDSCID(), pDSCItem->m_nFileIndex);
//#else
//				str4DA.Format(_T("\\\\%s\\%s"), str4DASaveAs, _T("Movie"));
//				if( !fo.CheckPath(str4DA))
//					fo.CreateFolder(str4DA);
//				str4DA.Format(_T("\\\\%s\\%s\\%s_%d.mp4"), str4DASaveAs, _T("Movie"), pDSCItem->GetDeviceDSCID(), pDSCItem->m_nFileIndex);
//#endif
//				strPath.Append(_T("@"));
//				strPath.Append(str4DA);
//
//				/*CString strMovie;
//				strMovie.Format(_T("M:\\Movie\\%s_%d.mp4"), pDSCItem->GetDeviceDSCID(), pDSCItem->m_nFileIndex);
//
//				strPath.Append(_T("@"));
//				strPath.Append(strMovie);*/
//
//			}
			
			int nFileIndex = pDSCItem->m_nFileIndex;

			pDSCItem->m_nFileIndex++;

		
			//Making Info
			if(!ESMGetValue(ESM_VALUE_MOVIESAVELOCATION))
			{
				BOOL bCopyFlag = FALSE;
				making_info* info = ESMGetMakingInfo();
				CString strFirst, strEnd;
				strFirst = (LPCSTR)info->strFirstDSC;
				strEnd = (LPCSTR)info->strEndDSC;

				CString str4DPPath = _T("");
				CString strFrontIP = ESMGetFrontIP();
				if(pDSCItem->GetDeviceDSCID().CompareNoCase(strFirst) == 0 || pDSCItem->GetDeviceDSCID().CompareNoCase(strEnd) == 0)
				{
					//4DP 경로
					if(info->nProcessorCnt > 0)
					{
						int nFrame = 0;
						int nIndex = 0;
						if(nFileIndex > 0)
						{
							while(1)
							{
								if(nIndex >= info->nProcessorCnt)
								{
									nIndex = 0;
								}

								if(nFrame == nFileIndex)
								{
									nFrame = nIndex;
									break;
								}

								nIndex++;
								nFrame++;
							}
						}
						//str4DPPath.Format(_T("\\\\192.168.0.%d\\movie\\%s"), info->nProcessorIP[nFrame], ESMGetFrameRecord());
						str4DPPath.Format(_T("\\\\%s%d\\movie\\%s"), strFrontIP, info->nProcessorIP[nFrame], ESMGetFrameRecord());
						if(!fo.CheckPath(str4DPPath))
						{
							CString strDir;
							//strDir.Format(_T("\\\\192.168.0.%d\\movie"), info->nProcessorIP[nFrame]);
							strDir.Format(_T("\\\\%s%d\\movie"), strFrontIP, info->nProcessorIP[nFrame]);
							//fo.DeleteDirectroyFiles(strDir);
							//ESMLog(1,_T("###Delete Folder %s"), strDir);
							fo.Delete(strDir,FALSE);
							fo.CreateFolder(str4DPPath);
						}

						CString str;
						str.Format(_T("\\%s_%d.mp4"), pDSCItem->GetDeviceDSCID(), nFileIndex);
						str4DPPath.Append(str);
						ESMLog(1, _T("#########Save Path %s"), str4DPPath);
						bCopyFlag = TRUE;
					}
				}

				if(!str4DPPath.IsEmpty())
				{
					strPath.Append(_T("@"));
					strPath.Append(str4DPPath);
				}
			}
			

			TCHAR* cPath = new TCHAR[1+strPath.GetLength()];
			_tcscpy(cPath, strPath);
			//File  remove
			if(pDSCItem->m_nFileIndex == 0)
				m_nCheckFileCount = 0;
			else
			{
				//jhhan File Temp
#ifdef _FILE_TEMP
				if(ESMGetRecordFileSplit())
				{
					if(pDSCItem->m_nFileIndex >= ESMGetValue(ESM_VALUE_DELETE_COUNT)/2)
					{
						m_nCheckFileCount = pDSCItem->m_nFileIndex - ESMGetValue(ESM_VALUE_DELETE_COUNT)/2;
						strFile.Format(_T("%s\\%s_%d.mp4"), strRemovePath, pDSCItem->GetDeviceDSCID(), m_nCheckFileCount);
						CESMFileOperation fo;
						fo.Delete(strFile);

						//wgkim						
						{
							CString strTempPath = strFile;
							CString strTok, strTokWithExtension;
							strTempPath.Replace(_T("\\Temp"), _T(""));

							int length = strTempPath.GetLength(), find_count = 0;
							for(int i = 0; i < length; i++)							 
								if(strTempPath[i] == '_')
									find_count++;								
							
							int nStrCount = find_count;
							AfxExtractSubString(strTokWithExtension, strTempPath, nStrCount, '_');
							AfxExtractSubString(strTok, strTokWithExtension, 0, '.');

							int nStrLength = strTok.GetLength();

							for(int i = 0; i < 2; i++)
							{									
								CString strNumber;
								int nNumber = _ttoi(strTok);
								nNumber = nNumber * 2 + i;
								strNumber.Format(_T("%d"), nNumber);

								strTempPath = strFile;
								CString strReplace = strNumber+_T(".mp4");
								strTempPath.Replace(strTokWithExtension, strReplace);							
								strTempPath.Replace(_T("\\Temp"), _T(""));

								fo.Delete(strTempPath);
							}
						}
					}
				}
#endif
				

				//jhhan File Temp
				if(pDSCItem->m_nFileIndex >= ESMGetValue(ESM_VALUE_DELETE_COUNT))
				{
					int nIdx = pDSCItem->m_nFileIndex - ESMGetValue(ESM_VALUE_DELETE_COUNT);
					strFile.Format(_T("%s\\%s_%d.mp4"), strRemovePath, pDSCItem->GetDeviceDSCID(), nIdx);
					CESMFileOperation fo;
#ifdef _FILE_TEMP
					if(ESMGetRecordFileSplit())
						strFile.Replace(_T("\\Temp"), _T(""));
#endif			
					fo.Delete(strFile);
				}
			}

			//ESMEvent* 
			ESMEvent* pSendMsg = new ESMEvent();
			memset(pSendMsg, 0, sizeof(ESMEvent));
			pSendMsg->message = WM_SDI_OP_FILE_TRANSFER;
			pSendMsg->pDest = (LPARAM)pMsg->nParam1;
			pSendMsg->pParam = (LPARAM)cPath;
			pSendMsg->nParam1 = pMsg->nParam2;
			pSendMsg->nParam2 = pMsg->nParam3;
			pSendMsg->nParam3 = (int)pMsg->pParam;

			pDSCItem->SdiAddMsg(pSendMsg);
		}
		else
			pDSCItem->m_bMovieSaveCheck = TRUE;
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}


void CDSCMgr::OnEventUsbConnectFail(ESMEvent* pMsg)
{
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;

	ESMLog(1, _T("OnEventUsbConnectFail [%s]"), pDSCItem->GetDeviceDSCID());
	if( pDSCItem->GetType() == DSC_REMOTE)
	{
		ESMEvent* pMsg = new ESMEvent();
		pMsg->message	= WM_RS_RC_USBCONNECTFAIL;
		pMsg->pDest		= (LPARAM)pDSCItem;
		::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

//------------------------------------------
void CDSCMgr::OnEventMovieDone(ESMEvent* pMsg)
{
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	int nfilenum = pMsg->nParam2;
	int nClose = pMsg->nParam3;
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	//-- 2013-02-15 hongsu.jung
	//-- Get SDI Manager
	if(pDSCItem)
	{
		if(pDSCItem->m_bMovieCancel)
		{
			pDSCItem->SetDSCStatus(SDI_STATUS_REC_FINISH);
			m_pDSCViewer->m_pFrameSelector->DrawEditor();
		}
		else if(pDSCItem->m_bMovieSaveCheck)
		{
			CESMFileOperation fo;
			CString strPath, strFile;
			strPath.Format(_T("%s"),ESMGetClientRamPath());

			strFile.Format(_T("%s\\%s"), strPath, pDSCItem->GetDeviceDSCID());
			strPath = strFile;
			//-- Save Capture Image
			TCHAR* cPath = new TCHAR[1+strPath.GetLength()];
			_tcscpy(cPath, strPath);

			//ESMEvent* 
			pMsg = new ESMEvent();
			memset(pMsg, 0, sizeof(ESMEvent));
			pMsg->message = WM_SDI_OP_MOVIE_IMAGE_RECEIVE;
			pMsg->pParam = (LPARAM)cPath;
			pMsg->nParam1 = nfilenum;
			pMsg->nParam2 = nClose;
			pDSCItem->SdiAddMsg(pMsg);
		
			//-- Change Status
			pDSCItem->SetDSCStatus(SDI_STATUS_REC_FINISH);
			ESMLog(5,_T(":: OnEventMovieDone::Start Save Movie [%s][%s] : %d"),pDSCItem->GetDeviceDSCID(),strPath, nClose);
			//-- Update MovieEditor Bar
			m_pDSCViewer->m_pFrameSelector->DrawEditor();

			//_beginthreadex(NULL, 0, CopyRecordFile, (void *)pDSCItem, 0, NULL);
		}
		else
			pDSCItem->m_bMovieSaveCheck = TRUE;

	}
}

void CDSCMgr::OnEventSaveDone(ESMEvent* pMsg)
{
	//-- Change Status
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	if(pDSCItem)
	{
		//-- 2013-09-25 hongsu@esmlab.com
		//-- Picture
		if(pMsg->nParam2)
		{
			ESMLog(5,_T("Finish Save Picture [%s]"),pDSCItem->GetDeviceDSCID());		
			pDSCItem->SetDSCStatus(SDI_STATUS_CAPTURE_OK);
			//-- Update Draw Editor Color
			//m_pDSCViewer->m_pFrameSelector->DrawEditor();
		}
		else//-- Movie
		{
			ESMLog(5,_T("Finish Save Movie [%s]"),pDSCItem->GetDeviceDSCID());				
			//-- Change Status
			pDSCItem->SetDSCStatus(SDI_STATUS_REC_FINISH);				
			//-- Update Draw Editor Color
			m_pDSCViewer->m_pFrameSelector->DrawEditor();

			//MovieMgr Ver
			CString strRamFile;
			//strRamFile.Format(_T("%s\\%s.mp4"), ESMGetClientRamPath(), pDSCItem->GetDeviceDSCID());
			strRamFile.Format(_T("%s\\%s.mp4"), ESMGetClientRamPath(), pDSCItem->GetDeviceDSCID());

			CESMFileOperation fo;
			if( !fo.CheckPath(strRamFile))
				fo.CreateFolder(strRamFile);
			
			TCHAR* cPath = new TCHAR[1+strRamFile.GetLength()];
			_tcscpy(cPath, strRamFile);
			TCHAR* strId = new TCHAR[pDSCItem->GetDeviceDSCID().GetLength() + 1];
			_tcscpy(strId, pDSCItem->GetDeviceDSCID());
			ESMEvent* pMsgBuffer	= new ESMEvent();
			pMsgBuffer->message	= WM_ESM_MOVIE_SET_MOVIEDATA;
			pMsgBuffer->pParam	= (LPARAM)cPath;
			pMsgBuffer->pDest	= (LPARAM)strId;
			//::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsgBuffer);

			// File Copy to Movie Path
			//_beginthreadex(NULL, 0, CopyRecordFile, (void *)pDSCItem, 0, NULL);
		}
		
		//-- 2013-09-13 hongsu@esmlab.com
		//-- Redraw ImageEditor
		m_pDSCViewer->m_pFrameSelector->Invalidate();
	}
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventAfDetect(ESMEvent* pMsg)
{
	int nStatus = SDI_STATUS_UNKNOWN;
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	int nFocus = (int)pMsg->nParam2;
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	if(!pDSCItem)
		return;

	if(pDSCItem->GetDSCStatus() == SDI_STATUS_REC || pDSCItem->GetDSCStatus() == SDI_STATUS_CAPTURE)
		return;
	
	switch(nFocus)
	{
	case AF_DETECT	:	nStatus = SDI_STATUS_FOCUS_OK	;	break;
	case AF_FAIL	:	nStatus = SDI_STATUS_FOCUS_FAIL	;	break;	
	case AF_CANCEL	:	
	default			:	return;
	}
	
	/*
	//-- Send Message S1 Release
	pMsg = new ESMEvent();
	pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4;			
	pDSCItem->SdiAddMsg((CObject*)pMsg);
	*/
	
	//-- Change Status
	pDSCItem->SetDSCStatus(nStatus);

}

void CDSCMgr::OnEventPropertyChange(ESMEvent* pMsg)
{
	//-- 2013-04-27 hongsu@esmlab.com
	//-- Check Liveview Size
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	
	if(pDSCItem)
	{
		/*
		//-- Check Liveview On
		CString strSelectedID = m_pDSCViewer->GetSelectedItemID();
		if(strSelectedID == _T(""))
			return;
		if(pSdiMgr->GetDeviceDSCID() != strSelectedID)
			return;
		*/


		//Code 검증 필요
		/*if(pMsg->nParam2 == PTP_CODE_IMAGESIZE)
		{
			//-- Send Liveview Info
			ESMEvent* pMsgSize = NULL;
			pMsgSize = new ESMEvent();
			pMsgSize->message = WM_RS_MC_LIVEVIEW_INFO;			
			pDSCItem->SdiAddMsg(pMsgSize);
		}*/
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventGetCaptureImageRecord(ESMEvent* pMsg)
{
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	if(pDSCItem)
	{
		ESMEvent* _pMsg= new ESMEvent();
		_pMsg->pDest		= (LPARAM)pDSCItem;
		_pMsg->nParam1		= (int)pMsg->nParam1;		// result;
		_pMsg->nParam2		= (int)pMsg->nParam2;		// gh5 init rec result;
		_pMsg->nParam3		= (BOOL)pMsg->nParam3;		// PTP_SendCapture_Require complted;
		_pMsg->message	= WM_ESM_NET_RESULT_IMAGE_RECORD_TOSERVER;
		::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_NET, (LPARAM)_pMsg);
	}
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

//------------------------------------------------------------------------------
//! @function	Get Property & Set Property View
//! @brief				
//! @date		2013-05-04
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCMgr::OnEventGetProperty(ESMEvent* pMsg)
{
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventGetFocusValue(ESMEvent* pMsg)
{
	CESMIni ini;
	CString strConfig;
	strConfig.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONFIG);

	if(!strConfig.GetLength())
		return;

	if(!ini.SetIniFilename (strConfig))
		return;

	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	if(pDSCItem)
	{
		SdiFocusPosition* pFocus = (SdiFocusPosition*)pMsg->pParam;
		if(pFocus)
		{
			if(pFocus->current)
			{
				pDSCItem->m_nFocus = pFocus->current;
				ini.WriteInt	(INFO_SECTION_FOCUS, pDSCItem->GetDeviceDSCID()	, pDSCItem->m_nFocus);
			}
			if(pFocus)
			{
				delete pFocus;
				pFocus = NULL;
			}
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventErrorOccured(ESMEvent* pMsg)
{
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	//if( pDSCItem )
	//	TRACE(_T("Driver return code (initiating) [OnEventErrorOccured]: 0x800700AA [%s]\r\n"), pDSCItem->GetDeviceDSCID());
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventLiveviewErrorOccured(ESMEvent* pMsg)
{
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventRcvSyncCheck(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;

	CDSCGroup* pGroup = m_pDSCViewer->GetGroupData(pMsg->nParam1);
	if(pGroup && pDSCID)
	{
		for(int i = 0; i < pGroup->GetCount(); i++)
		{
			CDSCItem* pItem = pGroup->GetDSC(i);
			CString str = pItem->GetDeviceDSCID();

			//if(pMsg->nParam2 == _ttoi(pItem->GetDeviceDSCID()))
			if (pDSCID->GetString() == pItem->GetDeviceDSCID())
			{
				//joonho.kim 17.06.22 Sync Test
				pItem->m_nTesting[TESTING_PROP_AC_CNT]++;
				pItem->m_nCheckTime += pMsg->nParam3;
				pItem->m_nTesting[TESTING_PROP_AC_AVG] = pItem->m_nCheckTime/pItem->m_nTesting[TESTING_PROP_AC_CNT];

				if(pItem->m_nTesting[TESTING_PROP_AC_MIN] == 0)
					pItem->m_nTesting[TESTING_PROP_AC_MIN] = pMsg->nParam3;
				if(pItem->m_nTesting[TESTING_PROP_AC_MIN] > pMsg->nParam3)
					pItem->m_nTesting[TESTING_PROP_AC_MIN] = pMsg->nParam3;
				if(pItem->m_nTesting[TESTING_PROP_AC_MAX] < pMsg->nParam3)
					pItem->m_nTesting[TESTING_PROP_AC_MAX] = pMsg->nParam3;



				/*pItem->m_nSyncCount++;
				pItem->m_nCheckTime += pMsg->nParam3;

				if(pItem->m_nCheckMin == 0)
					pItem->m_nCheckMin = pMsg->nParam3;
				if(pItem->m_nCheckMin < pMsg->nParam3)
					pItem->m_nCheckMin = pMsg->nParam3;
				if(pItem->m_nCheckMax < pMsg->nParam3)
					pItem->m_nCheckMax = pMsg->nParam3;*/
				/////////////

				pItem->SetTickStatus(TRUE);
			}
		}
	}
}


void CDSCMgr::OnEventRcvSyncResult(ESMEvent* pMsg)
{
	CDSCGroup* pGroup = m_pDSCViewer->GetGroupData(pMsg->nParam1);
	if (pGroup)
	{
		pGroup->SetSync(TRUE);
		ESMLog(1, _T("Group Sync Seccess : %d"), pGroup->m_nIP);
	}
	else
		ESMLog(0, _T("Non DSC Group : %d"), pMsg->nParam1);

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-09-15
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCMgr::OnEventGetSenSorTime(ESMEvent* pMsg)
{
// 	//-- 2014-09-15 hongsu@esmlab.com
// 	//-- Get DSCItem 
// 	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
// 	if(pDSCItem)
// 	{
// 		//pDSCItem->m_bGetStartTime = TRUE;
// 		//-- Sensor Start Time
// 		int nExecuteTime;
// 		int nSensorTime = pMsg->nParam3 *10;
// 		//int nSensorTime1 = pMsg->nParam3;
// 		nExecuteTime = pDSCItem->GetExecuteTime();
// 		
// 		//-- 2014-09-15 hongsu@esmlab.com
// 		//-- Check Sensor Time Sync
// 		int nGap = 0, nAbsGap = 0;
// 		if( pDSCItem->GetSensorOnDesignation() == 0 )
// 		{
// 			nAbsGap = abs(nSensorTime - nExecuteTime - sync_frame_time_after_sensor_on - ESMGetValue(ESM_VALUE_SENSORONTIME));
// 			nGap = nSensorTime - nExecuteTime - sync_frame_time_after_sensor_on - ESMGetValue(ESM_VALUE_SENSORONTIME);
// 		}
// 		else
// 		{
// 			nAbsGap = abs(nSensorTime - nExecuteTime - sync_frame_time_after_sensor_on - pDSCItem->GetSensorOnDesignation());
// 			nGap = nSensorTime - nExecuteTime - sync_frame_time_after_sensor_on - pDSCItem->GetSensorOnDesignation();
// 			ESMLog(5, _T("DSC[%s]"), pDSCItem->GetDeviceDSCID(), pDSCItem->GetSensorOnDesignation());
// 		}
// 
// 		ESMLog(5,_T("[%s] Sensor Start Time [%.1lf] Execute Time [%.1lf] Interval[%.1lf] AbsGap[%.1lf], Gap[%.1lf]"),
// 						pDSCItem->GetDeviceDSCID(),
// 						(double)nSensorTime * 0.1, 
// 						(double)nExecuteTime * 0.1, 
// 						(double)(nSensorTime - nExecuteTime) * 0.1,
// 						(double)nAbsGap * 0.1,
// 						(double)nGap * 0.1);
// 
//  		if(nAbsGap > sync_decision_sensor_on)
//  		{
// 			ESMLog(0,_T("[%s] Sensor Black Out"), pDSCItem->GetDeviceDSCID());
// 			pDSCItem->SetSensorSync(FALSE);
// 
// 			ESMEvent* pNewMsg	= new ESMEvent();
// 			pNewMsg->pDest		= (LPARAM)pDSCItem;
// 			pNewMsg->nParam1	= nAbsGap;
// 			pNewMsg->nParam2	= nSensorTime - nExecuteTime;
// 			pNewMsg->nParam3	= nGap;
// 			pNewMsg->message	= WM_ESM_NET_SENSORSYNCOUT;
// 			::SendMessage(m_hParentWnd, WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
// 		}
// 		else
// 		{
// 			pDSCItem->SetSensorSync(TRUE);
// 
// 			ESMEvent* pNewMsg	= new ESMEvent();
// 			pNewMsg->pDest		= (LPARAM)pDSCItem;
// 			pNewMsg->nParam1	= nAbsGap;
// 			pNewMsg->nParam2	= nSensorTime - nExecuteTime;
// 			pNewMsg->nParam3	= nGap;
// 			pNewMsg->message	= WM_ESM_NET_SENSORSYNCTIME;
// 			::SendMessage(m_hParentWnd, WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
// 		}
// 	}

// 	m_nStartTime *= 0.1;
// 	m_nIntervalSensorOn *= 0.1;
// 	int nFrameTimeAferSensorOn = sync_frame_time_after_sensor_on * 0.1;

	CString* pstr = (CString*)pMsg->pParam;
	CString strMsg;
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	
	if(pDSCItem)
	{
	 	//-- Sensor Start Time
	 	UINT nExecuteTime;
	 	UINT nSensorTime = pMsg->nParam3 ;
	 	//int nSensorTime1 = pMsg->nParam3;
		if(pDSCItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		{
			nExecuteTime = pDSCItem->GetExecuteTime();
			strMsg.Format(_T("[%s] [EVENT] PTP_EC_CUSTOM_StartTime t1 = %u, t2 = %u"), *pstr , pMsg->nParam2, pMsg->nParam3);
		}
		else //NX Series
		{
			nExecuteTime = pDSCItem->GetExecuteTime();
			nExecuteTime = nExecuteTime * 0.1;

			strMsg.Format(_T("[%s] [EVENT] PTP_EC_CUSTOM_StartTime t1 = %u, t2 = %u"), *pstr , pMsg->nParam3, pMsg->nParam2);
		}
	 	
		ESMLog(5,strMsg);

// 		TCHAR* cPath = {0,};
// 		cPath = new TCHAR[_tcslen(strPath) + 1];
// 		_tcscpy( cPath, strPath );

		HWND hHwnd = AfxGetMainWnd()->GetSafeHwnd();

		RSEvent* pNewMsg	= new RSEvent();
		pNewMsg->pDest		= (LPARAM)pDSCItem;
		//pNewMsg->pParam		= (LPARAM)cPath; 
		pNewMsg->nParam1	= nExecuteTime;
		pNewMsg->nParam2	= nSensorTime;
		pNewMsg->message	= WM_RS_RC_INFORMSENSOROK;
		::SendMessage(hHwnd, WM_SDI, (WPARAM)WM_RS_RC_INFORMSENSOROK, (LPARAM)pNewMsg);
	}

	if(pstr)
	{
		delete pstr;
		pMsg->pParam = NULL;
	}

	//-- 2014-09-15 hongsu@esmlab.com
	//-- Remove Message 
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-09-16
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCMgr::OnEventGetErrorFrame(ESMEvent* pMsg)
{
	//-- 2014-09-15 hongsu@esmlab.com
	//-- Get DSCItem 
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	if(pDSCItem)
	{
		int		nFrameCnt		= pMsg->nParam2;
		float	nFrameTime		= (float)ESMGetFrameTime(nFrameCnt)/(float)1000;
		//float	nIntervalTime	= (float)pMsg->nParam3/(float)1000;		
		int	nIntervalTime	= pMsg->nParam3;		
		//-- 2014-09-21 hongsu@esmlab.com
		//-- Check Another Info
		float	nTimeStampInterval = (float)((int)pMsg->pDest)/(float)1000; // 100um
		ESMLog(5,_T("[AGENT][%s][%.03f] /ERROR FRAME Interval[%.03f]"),
										pDSCItem->GetDeviceDSCID(), 
										nFrameTime, 
										(float)nTimeStampInterval);

		//-- 2014-09-15 hongsu@esmlab.com		
		//-- Send To Movie Manager
		pDSCItem->AddErrorFrame(nFrameCnt,nIntervalTime);

		//-- Error Frame Info
		//-- Send To Server
		ESMEvent* pNewMsg	= new ESMEvent();
		pNewMsg->pDest		= (LPARAM)pDSCItem;
		pNewMsg->nParam1	= nFrameCnt;
		pNewMsg->nParam2	= nIntervalTime;
		pNewMsg->nParam3	= nTimeStampInterval;
		pNewMsg->message	= WM_ESM_NET_ERRORFRAME;
		::SendMessage(m_hParentWnd, WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
	}

	//-- 2014-09-15 hongsu@esmlab.com
	//-- Remove Message 
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}


void CDSCMgr::OnEventGetAddFrame(ESMEvent* pMsg)
{
	//-- 2014-09-15 hongsu@esmlab.com
	//-- Get DSCItem 
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	if(pDSCItem)
	{
		int		nFrameCnt		= pMsg->nParam2;
		float	nFrameTime		= (float)ESMGetFrameTime(nFrameCnt)/(float)1000;
		float	nIntervalTime	= (float)pMsg->nParam3/(float)1000;
		ESMLog(5,_T("[AGENT][%s][%.03f] /Add Time/ Interval time[%.03f]ms"),pDSCItem->GetDeviceDSCID(), nFrameTime, nIntervalTime);	


// 		//-- 2014-09-21 hongsu@esmlab.com
// 		//-- Check Another Info
// 		float	nTimeStampInterval = (float)((int)pMsg->pDest)/(float)1000;
// 		ESMLog(5,_T("[AGENT][%s][%.03f] /Add Frame/ Interval [%.03f]ms Time Tick[%.03f]ms"),
// 			pDSCItem->GetDeviceDSCID(), 
// 			nFrameTime, 
// 			nIntervalTime,
// 			nTimeStampInterval);

		//-- 2014-09-15 hongsu@esmlab.com		
		//-- Send To Movie Manager
		pDSCItem->AddErrorFrame(nFrameCnt,nIntervalTime);


		//-- Check Infoaof Add Frame
		ESMEvent* pNewMsg	= new ESMEvent();
		pNewMsg->pDest		= (LPARAM)pDSCItem;
		pNewMsg->nParam1	= nFrameCnt;
		pNewMsg->nParam2	= nIntervalTime;
		//pNewMsg->message	= WM_ESM_NET_ADDFRAME;
		pNewMsg->message	= WM_ESM_NET_ERRORFRAME;
		::SendMessage(m_hParentWnd, WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
	}

	//-- 2014-09-15 hongsu@esmlab.com
	//-- Remove Message 
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventGetSkipFrame(ESMEvent* pMsg)
{
	//-- 2014-09-15 hongsu@esmlab.com
	//-- Get DSCItem 
	CDSCItem* pDSCItem = (CDSCItem*)pMsg->pParent;
	if(pDSCItem)
	{
		int		nFrameCnt		= pMsg->nParam2;
		float	nFrameTime		= (float)ESMGetFrameTime(nFrameCnt)/(float)1000;
		float	nIntervalTime	= (float)pMsg->nParam3/(float)1000;
		ESMLog(5,_T("[AGENT][%s][%.03f] /Skip Time/ Interval time[%.03f]ms"),pDSCItem->GetDeviceDSCID(), nFrameTime, nIntervalTime);		

		//-- Check Infoaof Add Frame
		ESMEvent* pNewMsg	= new ESMEvent();
		pNewMsg->pDest		= (LPARAM)pDSCItem;
		pNewMsg->nParam1	= nFrameCnt;
		pNewMsg->nParam2	= nIntervalTime;
		pNewMsg->message	= WM_ESM_NET_SKIPFRAME;
		::SendMessage(m_hParentWnd, WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
	}

	//-- 2014-09-15 hongsu@esmlab.com
	//-- Remove Message 
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-09-17
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCMgr::ResetErrorFrame()
{
	//-- 2014-09-17 hongsu@esmlab.com
	//-- Remove All ValidFrame 
	ValidFrame* pExist = NULL;
	//-- Check Valid Items
	int nAll = m_arCorrectFrame.GetCount();
	while(nAll--)
	{
		pExist = (ValidFrame*)m_arCorrectFrame.GetAt(nAll);
		if(pExist)
		{
			delete pExist;
			pExist = NULL;
		}
	}
	m_arCorrectFrame.RemoveAll();	
}


ValidFrame* CDSCMgr::GetPrevFrame(CString strID, int nIndex)
{
	ValidFrame* pExist = NULL;
	int nAll = m_arCorrectFrame.GetCount();
	while(nAll--)
	{
		pExist = (ValidFrame*)m_arCorrectFrame.GetAt(nAll);
		if( (pExist->strDSC			== strID	) &&
			(pExist->nCurrentIndex	== nIndex	))
		{
			return pExist;
		}
	}
	return NULL;
}

void CDSCMgr::AddValidFrame(ValidFrame* pValidFrame)
{
	ValidFrame* pExist = NULL;
	ValidFrame* pExistPrev = NULL;

	CString strID	= pValidFrame->strDSC;
	int nIndex		= pValidFrame->nCurrentIndex;

	//-- 2014-09-17 hongsu@esmlab.com
	//-- Check Valid Items
	int nAll = m_arCorrectFrame.GetCount();
	while(nAll--)
	{
		pExist = (ValidFrame*)m_arCorrectFrame.GetAt(nAll);
		if( (pExist->strDSC			== strID	) &&
			(pExist->nCurrentIndex	== nIndex	))
		{
			//-- 2014-09-17 hongsu@esmlab.com
			//-- Change to Correct
			if(pExist->nCorrectIndex == -1)
			{
				pExistPrev = GetPrevFrame(strID,nIndex-1);
				//-- 2014-09-21 hongsu@esmlab.com
				//-- Check Previous Error Frame 
				if(pExistPrev)
				{
					pExist->nCorrectIndex = pValidFrame->nCorrectIndex;
				}
			}

			//-- 2014-09-17 hongsu@esmlab.com
			//-- break
			if(pValidFrame)
			{
				delete pValidFrame;
				pValidFrame = NULL;
			}
			return;			
		}
	}

	//-- 2014-09-17 hongsu@esmlab.com
	//-- Non Exist Frame Info
	m_arCorrectFrame.Add((CObject*)pValidFrame);
}

void CDSCMgr::OnEventGetTickResult(ESMEvent* pMsg)
{
	CString* pDSCID = (CString*)pMsg->pDest;
	CString strID = pDSCID->GetString();	

	CDSCItem* pItem = m_pDSCViewer->GetItemData(strID, TRUE);

	if (pItem)
	{
		ESMEvent* pNewMsg	= new ESMEvent();
		pNewMsg->pDest		= (LPARAM)pItem;
		pNewMsg->nParam1	= pMsg->nParam1;
		pNewMsg->nParam2	= pMsg->nParam2;
		pNewMsg->nParam3	= pMsg->nParam3;
		pNewMsg->pParam		= pMsg->pParam;
		pNewMsg->message	= WM_ESM_NET_RESULT_GET_TICK_TOSERVER;
		::SendMessage(m_hParentWnd, WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);
	}	

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}