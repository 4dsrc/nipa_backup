////////////////////////////////////////////////////////////////////////////////
//
//	DSCMgrLive.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCMgr.h"
#include "DSCViewer.h"
#include "ESMUtil.h"
#include "SdiSingleMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-04-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @revision		
//------------------------------------------------------------------------------
void CDSCMgr::OnEventSetSize(ESMEvent* pMsg)
{
	//m_pDSCViewer->m_ImgWidth = nSrcWidth;
	//m_pDSCViewer->m_ImgHeight = nSrcHeight;
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}

void CDSCMgr::OnEventNetLiveview(ESMEvent* pMsg)
{
	int nAll = m_pDSCViewer->GetItemCount();
	CDSCItem* pExist = NULL;
	CString* pDSCID = (CString*)pMsg->pDest;

	while(nAll--)
	{
		pExist = m_pDSCViewer->GetItemData(nAll);
		if(pExist->GetDeviceDSCID() == pDSCID->GetString())
		{		
			pExist->SetLiveview(pExist, pMsg->nParam1);
			//break;	// 두개 이상의 영상이 스위칭되게 보이는 버그가 있음.
		}
		else
		{
			pExist->SetLiveview(pExist, FALSE);
		}
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

	if(pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}
}

