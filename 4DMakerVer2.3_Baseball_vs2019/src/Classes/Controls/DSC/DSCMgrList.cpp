////////////////////////////////////////////////////////////////////////////////
//
//	4DMakerDSCMgrList.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-27
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCMgr.h"
#include "DSCViewer.h"
#include "ESMUtil.h"
#include "SdiSingleMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-04-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @revision		
//------------------------------------------------------------------------------
void CDSCMgr::OnEventAddUsbID(CString strUsbID)
{
	//-- 2013-12-14 yongmin
	//-- Add Not USB ID (Only DSC ID) 
	//m_pDSCViewer->AddDSC(strUsbID);
}
