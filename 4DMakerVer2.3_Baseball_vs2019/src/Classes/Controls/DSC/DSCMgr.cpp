////////////////////////////////////////////////////////////////////////////////
//
//	DSCMgr.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCMgr.h"
#include "DSCViewer.h"
#include "ESMUtil.h"
#include "SdiSingleMgr.h"
#include "ESMIni.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CDSCMgr, CWinThread)


// CDSCMgr
CDSCMgr::CDSCMgr(CDSCViewer* pParent)
{
	m_pDSCViewer	= pParent;
	m_hOpenSession	= NULL;
	m_hSetProperty	= NULL;
	m_hConvertFile	= NULL;
	m_hDelThread	= NULL;
	m_bThreadRun	= FALSE;
	m_bThreadStop	= FALSE;
	m_bRecThreadStop = FALSE;
	m_hParentWnd = AfxGetMainWnd()->GetSafeHwnd();
	m_hRecStateThread = NULL;
	m_nCaptureTime = 0;
	m_nRecordTime = 0;
	InitializeCriticalSection (&m_csDataLock);

	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		ESMSetRecordingInfoint(_T("RecFlag"), SDI_STATUS_NORMAL);
		m_hRecStateThread = (HANDLE) _beginthreadex(NULL, 0, OpserverRecState, (void *)this, 0, NULL);
	}
}

CDSCMgr::~CDSCMgr()
{
	m_bRecThreadStop = TRUE;
	DeleteCriticalSection(&m_csDataLock);
	if( m_hRecStateThread)
	{
		WaitForSingleObject(m_hRecStateThread, INFINITE);
		CloseHandle(m_hRecStateThread);
	}

	ExitInstance();
}

BOOL CDSCMgr::InitInstance() 
{ 	
	return TRUE; 
}

int CDSCMgr::ExitInstance()				
{
	//-- Stop Thread
	StopThread();
	// 2014-09-10 kcd
	// _beginthread 는 내부적으로 CloseHandle 수행
	// _beginthreadex 경우는 해주어야한다.
// 	if (m_hOpenSession)	CloseHandle(m_hOpenSession);
// 	if (m_hSetProperty)	CloseHandle(m_hSetProperty);
// 	if (m_hConvertFile)	CloseHandle(m_hConvertFile);

	WaitForSingleObject(m_hDelThread, 5000);
	if (m_hDelThread)	
	{
		CloseHandle(m_hDelThread);
		m_hDelThread = NULL;
	}

	return CWinThread::ExitInstance();
}

//------------------------------------------------------------------------------ 
//! @brief		StopThread
//! @date		2010-06-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CDSCMgr::StopThread()
{
	//-- 2013-04-25 hongsu.jung
	//-- Thread Stop
	DoThreadStop();
	while(m_bThreadRun)
		Sleep(1);
}

void CDSCMgr::AddMsg(ESMEvent* pMsg)
{
	if(!m_bThreadStop)
	{
		m_arMsg.Add((CObject*)pMsg);
	}
}

void CDSCMgr::RemoveAll()
{
	ESMEvent* pMsg = NULL;
	int nAll = (int)m_arMsg.GetCount();
	while(nAll--)
	{
		pMsg = (ESMEvent*)m_arMsg.GetAt(nAll);
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
		m_arMsg.RemoveAt(nAll);
	}
	m_arMsg.RemoveAll();
}
//------------------------------------------------------------------------------ 
//! @brief		Run
//! @date		2010-05-30
//! @author	hongsu.jung
//! @note	 	get message
//------------------------------------------------------------------------------ 
int CDSCMgr::Run(void)
{
	m_bThreadRun	= TRUE;
	m_bThreadStop	= FALSE;
	ESMEvent* pMsg	= NULL;
	int nMessage = 0;
	while(!m_bThreadStop)
	{
		//-- yield thread
		Sleep(1);
		int nAll = m_arMsg.GetSize();
		while(nAll--)
		{
			//-- yield thread
			Sleep(1);
			pMsg = (ESMEvent*)m_arMsg.GetAt(0);
			// 2013-01-28 kcd
			// 버그성 코드로 Debudebugg 요.

 			if(!pMsg)
 			{
 				m_arMsg.RemoveAt(0);
 				continue;
 			}
			nMessage =  pMsg->message;
			switch(pMsg->message)
			{
			case WM_CAPTURE_FILE_WRITE_ERROR:
				{
					void* pBuf = (void*)pMsg->pDest;
					CString* pStrData = (CString*)pMsg->pParam;
					CFile file;
					if(file.Open(*pStrData, CFile::modeNoTruncate | CFile::modeWrite | CFile::modeCreate ,NULL))
					{
						file.Write(pBuf, pMsg->nParam1);

						char* pmdat;
						if(pMsg->nParam2 > 0)
						{
							file.Seek(pMsg->nParam3 , CFile::begin); // mdat Write
							pmdat = (char*)&pMsg->nParam2;

							file.Write(&pmdat[3], 1);
							file.Write(&pmdat[2], 1);
							file.Write(&pmdat[1], 1);
							file.Write(&pmdat[0], 1);
						}

						file.Close();

						TRACE(_T("WM_CAPTURE_FILE_WRITE_ERROR ##File Write OK!!\n"));
					}
					else
						TRACE(_T("WM_CAPTURE_FILE_WRITE_ERROR ##File Write fail!!\n"));

					if(pStrData)
						delete pStrData;

					if(pMsg->pDest)
						delete pBuf;
				}

				
				break;
			case WM_CAPTURE_FILE_WRITE_DONE:
				{
					//ESMLog(1, _T("############ MakeMovieFile Time %d - %s"), pMsg->nParam1,*(CString*)pMsg->pParam);
					if(ESMGetGPUMakeFile())
					{
#if 0/*_4DA*/
						ESMEvent* pMsg1	= new ESMEvent;
						pMsg1->message = WM_MAKEMOVIE_GPU_FILE_ADD_MSG;
						pMsg1->pParam	= (LPARAM)pMsg->pParam;
						HWND phanle = AfxGetMainWnd()->GetSafeHwnd();

						::SendMessage(AfxGetMainWnd()->GetSafeHwnd(),WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg1 );
#else	//4DP Making Only
						/*CString* strFile = (CString*)pMsg->pParam;
						CString StrData;
						StrData.Format(_T("%s"), *strFile);

						FileWriteDone(StrData);*/
#endif

						//if(ESMGetValue(ESM_VALUE_RTSP))
						{
							CString strFile;
							strFile.Format(_T("%s"), *(CString*)pMsg->pParam);
							//ESMLog(5, strFile);
							ESMSet4DPTransmit(strFile);
						}
					}
					//jhhan 190208
					if(ESMGetValue(ESM_VALUE_REFEREEREAD))
					{
						CString strFile;
						strFile.Format(_T("%s"), *(CString*)pMsg->pParam);

						//jhhan 190129 m3u8

						CString strText;
						strText = ESMGetDSCIDFromPath(strFile);
						//ESMLog(5, _T("DONE : %s"), strText);

						vector<CString> _vtLive;
						_vtLive = ESMGetLiveList();

						for(int i=0; i < _vtLive.size(); i++)
						{
							CString strTxt;
							strTxt = _vtLive.at(i);
							if(strText == strTxt)
							{
								CESMm3u8 *_hd = NULL;
								_hd = GetM3u8(strText);

								//OnHDTranscoding(strFile);
								ESMTranscoding(strFile, 0, (void *)_hd);

								//jhhan m3u8
								/*CESMm3u8 *pData = NULL;
								pData = GetM3u8(strText);
								if(pData)
								{
								ESMLog(5, _T("Write : %s"), strFile);
								pData->Write(strFile);
								}*/
								break;
							}
						}

					}
					//jhhan 170329 Log
					/*CString* strFile = (CString*)pMsg->pParam;
					CString strData;
					strData.Format(_T("WM_CAPTURE_FILE_WRITE_DONE : %s"), *strFile);
					ESMLog(5, strData);*/

					
				}
				break;
			//-------------------------------------------------
			//--
			//-- Command Messasge
			//-- 
			//-------------------------------------------------
			case WM_ESM_DSC_OPEN_SESSION	: 
				OnDscOpenSession(pMsg);	
				break;
			
			//-------------------------------------------------
			//--
			//-- Get Event from SdiSingleMgr
			//-- 
			//-------------------------------------------------	
			case WM_SDI_EVENT_ERR_MSG:
			case WM_SDI_EVENT_ERR_MSG_GH5:
				OnEvnetErrMsg(pMsg);
				break;
			case WM_SDI_EVENT_CAPTURE_DONE	:
				OnEventCaptureDone(pMsg);				
				break;

			case WM_SDI_EVENT_SAVE_DONE		: 
				OnEventSaveDone(pMsg);	
				break;

			case WM_SDI_EVENT_AF_DETECT		: 
				OnEventAfDetect(pMsg);	
				break;

			case WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED	: 
				OnEventPropertyChange(pMsg);
				break;

			case WM_SDI_EVENT_LIVEVIEW_RECEIVED			: 
				OnEventLiveviewReceive(pMsg);		
				break;

			case WM_SDI_EVENT_EVENT_ERROR_OCCURED		: 
				OnEventErrorOccured(pMsg);
				break;

			case WM_SDI_EVENT_LIVEVIEW_ERROR_OCCURED	:
				OnEventLiveviewErrorOccured(pMsg);	
				break;		

			//-- 2013-07-18
			case WM_SDI_EVENT_MOVIE_DONE				: 
				OnEventMovieDone(pMsg);
				break;

			case WM_SDI_EVENT_FILE_TRANSFER				:
				{
					CSdiSingleMgr* pMain = (CSdiSingleMgr*)pMsg->pDest;
					ESMLog(1, _T(", [%s] File Transfer"), pMain->GetDeviceDSCID());	
				}
				
				OnEventFileTransfer(pMsg);
				break;

			case WM_SDI_EVENT_RECORD_STOP				:
				OnEventRecordStop(pMsg);
				break;

			case WM_SDI_EVENT_USBCONNECTFAIL			:
				OnEventUsbConnectFail(pMsg);
				break;

			case WM_SDI_RESULT_OPEN_SESSION				: 
				OnEventOpenSession(pMsg);
				break;
			case WM_SDI_RESULT_GH5_INITIALIZE			:
				OnEventNetPreRecInitGH5Result(pMsg);
				break;
			case WM_SDI_RESULT_CLOSE_SESSION			:
			case WM_SDI_RESULT_CAPTURE_IMAGE_RECEIVE	:
			case WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_1	:
			case WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_2	:
			case WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_3	:
			case WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_4	:
			case WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_5	:
			case WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_6	:
			case WM_SDI_RESULT_CAPTURE_IMAGE_ONCE		:		
				break;					

			case WM_SDI_RESULT_CAPTURE_IMAGE_RECORD		:
				OnEventGetCaptureImageRecord(pMsg);
				break;

			case WM_SDI_RESULT_GET_PROPERTY_DESC		: 
				OnEventGetProperty(pMsg);	
				break;

			case WM_SDI_RESULT_SET_PROPERTY_VALUE		:
			case WM_SDI_RESULT_LIVEVIEW_INFO			:
				break;

			case WM_SDI_RESULT_FOCUS					: 
				OnEventGetFocusValue(pMsg);	
				break;

			case WM_SDI_RESULT_RESET					:
			case WM_SDI_RESULT_FORMAT					:	
			case WM_SDI_RESULT_FWUPDATE					:
				break;

			//-- 2014-09-15 hongsu@esmlab.com
			//-- Get Event : Sensor Start And Save Frame Time 
			case WM_SDI_EVENT_SENSOR_TIME				:
				OnEventGetSenSorTime(pMsg);
				break;
			case WM_SDI_EVENT_SENSOR_ERROR_TIME:
				{
					CString strMSG;
					strMSG.Format(_T("SENSOR_TIME ID= %d, StartTime = %d, TimeStamp = %d", pMsg->nParam1, pMsg->nParam2, pMsg->nParam3));
					ESMLog(5,strMSG);
				}
				break;
			//-- 2014-09-17 hongsu@esmlab.com
			//-- Get Event : Error Frame Event (Occurred Frame Count & Interval Time )
			case WM_SDI_EVENT_ERROR_FRAME				:
				OnEventGetErrorFrame(pMsg);
				break;

			case WM_SDI_EVENT_ADD_FRAME:
				OnEventGetAddFrame(pMsg);
				break;
			case WM_SDI_EVENT_SKIP_FRAME:
				OnEventGetSkipFrame(pMsg);
				break;
			case WM_SDI_RESULT_GET_TICK:
				OnEventGetTickResult(pMsg);
				break;


			//------------------------------------------------------------------------------
			//! @brief		RS Message				
			//! @owner		Hongsu Jung (hongsu@esmlab.com)
			//! @return			
			//! @revision		
			//------------------------------------------------------------------------------			

			//-- 2013-09-10 hongsu@esmlab.com
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1		: 
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2		: 
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_3		: 
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4		: 
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5		:
			case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_6		: 
				OnEventCapture(pMsg);	
				break;
			//-- 2013-09-29 hongsu@esmlab.com
			//-- Get Message Inside 
			case WM_RS_LV_UPDATE_TUMBNAIL				:		
				break;
			case WM_RS_LV_GET_INFO						: 
				OnEventSetSize(pMsg);		
				break;
			case WM_RS_UPDATE_PROPERTY_VALUE			:
			case WM_RS_LIVEVIEW_GET_FOCUSPOSITION		:	
				break;
			case WM_RS_MOVIE_FILE_CONVERT				: OnEventConvertFile(pMsg);	
				break;
			//-- 2013-10-01 yongmin@esmlab.com
			//-- Remote Contol Message 
			case WM_RS_RC_NETWORK_CONNECT				: 
				OnEventNetConnect(pMsg);	
				break;
			case WM_RS_RC_CAPTURE_IMAGE_REQUIRE			: 
				OnEventNetCapture(pMsg);	
				break;
			case WM_RS_RC_CAPTURE_CANCEL				: 
				OnEventNetMovieCancel(pMsg);	
				break;
			case WM_RS_RC_STATUS_CHANGE					: 
				OnEventNetStatus(pMsg);
				break;
			case WM_RS_RC_PRE_REC_INIT_GH5_RESULT		:
				OnEventNetPreRecInitGH5Result(pMsg);
				break;
			case WM_RS_RC_CONVERT_FINISH				: 
				OnEventConvertFinish(pMsg);	
				break;
			case WM_RS_RC_GET_PROPERTY					:
				OnEventNetPropGet(pMsg);
				break;
			case WM_ESM_LIST_GETITEMFOCUS				:
				OnEventNetGetItemFocus(pMsg);
				break;
			case WM_RS_RC_SET_PROPERTY					: 
				/*if(pMsg->nParam1 == 0x5003)
					TRACE(_T("TEST"));*/

				OnEventNetPropSet(pMsg);	
				break;
			case WM_RCPCMD_SET_FOCUS					:
				OnEventSetFocusMove(pMsg);	
				break;
			//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
			case WM_RCPCMD_SET_FOCUS_VALUE:
				OnEventSetFocusValue(pMsg);	
				break;
			//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
			case WM_RCPCMD_SET_FOCUS_ALL					:
				OnEventSetFocusMove(pMsg);	
				break;
			case WM_RS_RC_LIVEVIEW						:
				OnEventNetLiveview(pMsg);		
				break;
			case WM_RS_RC_PRE_REC_INIT_GH5				:
				OnEventNetPreRecInitGH5(pMsg);
				break;
			case WM_RS_RC_CAMERA_SYNC					:
				OnEventNetGetTick(pMsg);	
				break;
			case WM_RS_RC_GET_TICK_CMD					:
				OnEventNetGetTickCmd(pMsg);		// resync 사용하기위해
				break;
			case WM_ESM_DSC_GET_TICK_RESULT_CMD			:
				OnEventRcvGetTickCmdResult(pMsg);
				break;
			case WM_ESM_DSC_SYNC_CHECK					:
				OnEventRcvSyncCheck(pMsg);
				break;
			case WM_ESM_DSC_SYNC_RESULT					:
				OnEventRcvSyncResult(pMsg);
				break;
			case WM_RS_RC_MOVIESAVE_CHECK				:
				OnEventSetMovieSaveCheck(pMsg);	
				break;
			case WM_RS_RC_GET_FOCUS						:
				OnEventSetFocus(pMsg);	
				break;
// 			case WM_SDI_RESULT_FILE_TRANSFERFAIL		:
// 				{
// 					OnEventFileTransferFail(pMsg);
// 					break;
// 				}
			case WM_SDI_RESULT_FILE_TRANSFER		:
				OnEventFileTransferInfo(pMsg);
				break;
			case WM_SDI_RESULT_SENDCAPTUREFAIL		:
				OnEventSendCaptureFail(pMsg);
				break;
			case WM_RS_RC_SENSORSYNCOUT:
				OnEventSensorSyncOut(pMsg);
				break;
			case WM_RS_RC_CAPTUREDELAY:
				OnEventCaptureDelay(pMsg);
				break;
			case WM_RS_RC_ERRORFRAME:
				OnEventErrorFrame(pMsg);
				break;
			case WM_RS_RC_ADDFRAME:
				OnEventAddFrame(pMsg);
				break;
			case WM_RS_RC_SKIPFRAME:
				OnEventSkipFrame(pMsg);
				break;
			case WM_RS_RC_VALIDSKIP:
				OnEventValidSkip(pMsg);
				break;
			case WM_RS_RC_RECORDFINISH:
				OnEventRecordFinish(pMsg);
				break;
			case WM_RS_RC_COPYTOSERVER:
				OnEventCopyToServer(pMsg);
				break;
			case WM_RS_RC_EXCEPTIONDSC:
				OnEventExceptionDsc(pMsg);
				break;		
			case WM_RS_RC_SETSENSORTIME:
				SyncDataInsert(pMsg);
				break;
			case WM_RS_RC_SYNC_DATASET:
				{
					OnEventSyncDataSet();
					if(pMsg)
					{
						delete pMsg;
						pMsg = NULL;
					}
				}
				break;
			case WM_RS_RC_INFORMSENSOROK	:
				OnEventInformSensorOK(pMsg);
				break;
			default:
				break;
			}						
			//-- Thread Lock Release
			m_arMsg.RemoveAt(0);		
		}		
	}
	//-- 2013-04-24 hongsu.jung
	//-- Remove All Message
	RemoveAll();
	m_bThreadRun = FALSE;
	//-- 2013-04-25 hongsu.jung
	//-- Thread Remove by self
	m_bAutoDelete = FALSE;

	_endthread();

	return 0;
}

void CDSCMgr::FileWriteDone(CString csFilePath)
{
	CString strFile;
	CString strCamID = GetCamID(csFilePath);

#ifdef _INI_4DA_LOCAL
	//네트워크 접근 경로 - 4DA ini 저장
	strFile.Format(_T("%s\\%s.ini"),_T("M:\\movie"),strCamID);
#endif
#ifdef _INI_4DP_NET_RAM
	//로컬 경로 - 4DP ini 저장
	strFile.Format(_T("\\\\%s\\movie\\%s.ini"),ESMGet4DAPath(strCamID),strCamID);
#endif
#ifdef _INI_4DP_NET_RT
	//로컬 경로 - 4DP RT
	strFile.Format(_T("\\\\%s\\4DMaker\\RT\\RT.ini"), ESMGet4DAPath(strCamID));
#endif
	
	CESMIni ini;
	if(ini.SetIniFilename (strFile))
	{
#ifdef _ALL_TIME	//확인 후 NEXT 진행
		//ini.DeleteSection(strCamID);
		while(1)
		{
			CString strFlag;
			strFlag = ini.GetString(strCamID, _T("FLAG"));
			if(strFlag.IsEmpty())
				break;
			Sleep(10);
		}
		CString strFlag, strPath;
		{
			ini.WriteString	(strCamID, _T("FLAG")	, _T("1"));
			ini.WriteString	(strCamID, _T("PATH")	, csFilePath);
			ESMLog(5, _T("[%s] %s"), strCamID, csFilePath);
		}
#else	//무조건 NEXT 진행
	#ifdef _SAVE_ORIGINAL
			ini.DeleteSection(strCamID);
			ini.WriteString	(strCamID, _T("FLAG")	, _T("1"));
			ini.WriteString	(strCamID, _T("PATH")	, csFilePath);
			ESMLog(5, _T("[%s] %s"), strCamID, csFilePath);
	#else
			ini.DeleteSection(_T("RT"));
			ini.WriteString	(_T("RT"), _T("FLAG")	, _T("1"));
			ini.WriteString	(_T("RT"), _T("PATH")	, csFilePath);
			ESMLog(5, _T("[%s] %s"), _T("RT"), csFilePath);
	#endif
#endif
	}else
	{
		ESMLog(5, _T("[%s] NOT %s"), strCamID, csFilePath);
	}
}

CString CDSCMgr::GetCamID(CString strFilePath)
{
#if 0
	CString strDscId;
	CString strFramePath = strFilePath;
	int nIndex = strFramePath.ReverseFind('\\') + 1;
	CString strTpPath = strFramePath.Right(strFramePath.GetLength() - nIndex);
	strDscId = strTpPath.Left(5);
	CString strCamID = strDscId;
#else
	int nFind = strFilePath.ReverseFind('\\');
	CString csFile = strFilePath.Right(strFilePath.GetLength()-nFind);
	csFile.Trim();
	csFile.Trim(_T("\\"));
	nFind = csFile.Find('.');
	CString csCam = csFile.Left(nFind);
	nFind = csFile.Find('_');
	
	CString strCamID = csFile.Mid(0, nFind);
#endif

	return strCamID;
}

void CDSCMgr::RemoveAllConnectVendorId()
{
	m_strListConnectVendorId.RemoveAll();
}

void CDSCMgr::MakeM3u8(CString strKey)
{
	CESMm3u8 *pData = NULL;

	map<CString, CESMm3u8 *>::iterator itList = m_M3u8.find(strKey);
	if(itList != m_M3u8.end())
	{
		pData = itList->second;
	}

	if(pData == NULL)
	{
		CString strTxt;
		strTxt.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("hd"));
		ESMCreateAllDirectories(strTxt);
		ESMLog(5, _T("m3u8 : %s"), strTxt);

		pData = new CESMm3u8();
		strTxt.Format(_T("%s\\%s\\%s.m3u8"), ESMGetPath(ESM_PATH_MOVIE_WEB), _T("hd"), strKey);
		pData->SetFile(strTxt);
		//pData->SetInfo(MOVIE_FPS_UHD_60P);
		//ESMLog(5, _T("MakeM3u8 : %s"),strKey);

		m_M3u8.insert(make_pair(strKey, pData));
	}
}

CESMm3u8 * CDSCMgr::GetM3u8(CString strKey)
{
	//ESMLog(5, _T("GetM3u8 : %s"), strKey);
	CESMm3u8 *pData = NULL;
	map<CString, CESMm3u8 *>::iterator itList = m_M3u8.find(strKey);
	if(itList != m_M3u8.end())
	{
		pData = itList->second;
	}
	return pData;
}

void CDSCMgr::DeleteM3u8()
{
	for(auto it = m_M3u8.begin(); it != m_M3u8.end(); it++)
	{
		CESMm3u8 *pData = it->second;
		if(pData)
		{
			delete pData;
			pData = NULL;
		}
	}

	m_M3u8.clear();
}
void CDSCMgr::ResetM3u8()
{
	ESMLog(5, _T("ResetM3u8"));

	for(auto it = m_M3u8.begin(); it != m_M3u8.end(); it++)
	{
		CESMm3u8 *pData = it->second;
		if(pData)
		{
			pData->ResetList();
		}
	}
}