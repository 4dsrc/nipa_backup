////////////////////////////////////////////////////////////////////////////////
//
//	DSCViewerCapture.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCListViewer.h"
#include "ESMFileOperation.h"
#include "DSCViewer.h"
#include "DSCItem.h"
#include "DSCMgr.h"
#include "ESMImgMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CDSCMgr::DoTakePicture()
{
		//-- Get Sync Time
	int nTickTime = ESMGetTick();
	TRACE(_T("[Sync Exe] [Server] 1.Get Base Time %d\n"),nTickTime);

	int nAll = m_pDSCViewer->GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 18"));
		return;
	}

	CESMFileOperation fo;
	CString strFolder;
	strFolder.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord());	
	fo.CreateFolder(strFolder);

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = m_pDSCViewer->GetGroup(nAll);
		if(!pGroup)
			continue;

		pGroup->DoTakeGroupPicture(nTickTime);
	}
}

void CDSCMgr::DoS1Press()
{
	int nAll = m_pDSCViewer->GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 19"));
		return;
	}

	CDSCGroup* pGroup = NULL;
	while(nAll--)
	{
		pGroup = m_pDSCViewer->GetGroup(nAll);
		if(!pGroup)
			continue;
		pGroup->DoGroupS1Press();
	}	
}

void CDSCMgr::DoMinFocus()
{
	int nAll = m_pDSCViewer->GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 19"));
		return;
	}

	CDSCGroup* pGroup = NULL;
	while(nAll--)
	{
		pGroup = m_pDSCViewer->GetGroup(nAll);
		if(!pGroup)
			continue;
		pGroup->DoMinFocus();
	}	
}

void CDSCMgr::DoS1Press(CDSCItem* pItem)
{
	ESMLog(5,_T("S1 Press [%s]"),pItem->GetDeviceDSCID());

	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		int nAll = m_pDSCViewer->GetGroupCount();	
		if(!nAll)
		{
			ESMLog(5,_T("Non DSC Conection 19"));
			return;
		}

		CDSCGroup* pGroup = NULL;
		while(nAll--)
		{
			pGroup = m_pDSCViewer->GetGroup(nAll);
			if(!pGroup)
				continue;
			for (int i=0;  i<pGroup->GetCount(); i++)
			{
				CDSCItem* pTempItem = pGroup->GetDSC(i);
				if (pTempItem == NULL)
					continue;

				if (pTempItem->GetDeviceDSCID() != pItem->GetDeviceDSCID())
					continue;

				pGroup->DoGroupS1Press(pItem);

				break;
			}		
		}	
	}
	else
	{
		ESMEvent* pMsg = new ESMEvent();
		
		pItem->SetDSCStatus(SDI_STATUS_NORMAL);
		pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1;
		pMsg->pDest		= (LPARAM)pItem;
		AddMsg(pMsg);

		Sleep(2000);

		DoS1Release(pItem);
	}
}

void CDSCMgr::DoS1Release()
{
	int nAll = m_pDSCViewer->GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 20"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = m_pDSCViewer->GetGroup(nAll);
		if(!pGroup)
			continue;
		pGroup->DoGroupS1Release();
	}
}

void CDSCMgr::DoS1Release(CDSCItem* pItem)
{
	ESMEvent* pMsg = new ESMEvent();
	//-- Release Focusing
	pItem->SetDSCStatus(SDI_STATUS_NORMAL);
	pMsg->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4;
	pMsg->pDest		= (LPARAM)pItem;
	AddMsg(pMsg);
	ESMLog(5,_T("S1 Release [%s]"),pItem->GetDeviceDSCID());
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-04-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @revision		
//------------------------------------------------------------------------------
void CDSCMgr::OnEventNetCapture(ESMEvent* pMsg)
{
	stDSCMgrThreadData* stDscData = new stDSCMgrThreadData;
	stDscData->pDscMgr = this;
	stDscData->pMsg = pMsg;

	//-- 2016-11-13 ymlee
	// Set Rec State
	m_pDSCViewer->m_pFrameSelector->SetRecStatus(DSC_REC_BTN);
	//ESMSendFrameDataInfo();

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, OnEventNetCaptureThread, (void *)stDscData, 0, NULL);
	CloseHandle(hSyncTime);

// 
// 	switch(pMsg->nParam1)
// 	{
// 	//-- Focussing
// 	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1		: DoS1Press();					break;
// 	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2		: 
// 		{				
// 			CDSCGroup* pGroup = m_pDSCViewer->GetDSCBaseGroup();
// 			if(pGroup)
// 				pGroup->DoTakePictureLocal((int)pMsg->nParam2);
// 			break;
// 		}
// 	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_3		: break;
// 	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4		: DoS1Release();	break;	
// 	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5		: 
// 		{
// 			ESMLog(1, _T("Recieve MovieSignal"));
// 			CDSCGroup* pGroup = m_pDSCViewer->GetDSCBaseGroup();
// 			if(pGroup)
// 				pGroup->DoMovieCaptureLocal((int)pMsg->nParam2);
// 			break;
// 		}
// 	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_6		:
// 		break;
// 	default:
// 		ESMLog(0, _T("OnEventNetCapture() Unknown message [%d]\n"),pMsg->message);
// 		break;
// 	}
}
unsigned WINAPI CDSCMgr::OnEventNetCaptureThread(LPVOID param)
{	
	stDSCMgrThreadData* stDscData = (stDSCMgrThreadData*)param;
	CDSCMgr* pDSCMgr = stDscData->pDscMgr;
	ESMEvent* pMsg = stDscData->pMsg;	

	switch(pMsg->nParam1)
	{
		//-- Focussing
	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1		: 
		{
			pDSCMgr->DoS1Press();					
		}
		break;
	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_7		:
		{
			CDSCItem* pDestItem = NULL;
			pDestItem = (CDSCItem*)pMsg->pDest;

			CDSCGroup* pGroup = pDSCMgr->m_pDSCViewer->GetDSCBaseGroup();
			if (pGroup == NULL)
				break;

			for (int i = 0; i < pGroup->GetCount() ; i++)
			{
				CDSCItem* pItem = pGroup->GetDSC(i);
				if (!pItem)
					continue;

				if (!pDestItem)
					break;

				if (pDestItem->GetDeviceDSCID() != pItem->GetDeviceDSCID())
					continue;

				//if (pMsg->nParam2 != _ttoi(pItem->GetDeviceDSCID()))
				//	continue;

				pDSCMgr->DoS1Press(pItem);		
				break;
			}
		}
		break;
	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2		: 
		{				
			CDSCGroup* pGroup = pDSCMgr->m_pDSCViewer->GetDSCBaseGroup();
			if(pGroup)
				pGroup->DoTakePictureLocal((int)pMsg->nParam2);
			break;
		}
	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_3		: break;
	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4		: pDSCMgr->DoS1Release();	break;	
	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5		: 
		{
			if(ESMGetRecordStatus() == ESMGetNextRecordStatus())
			{
				ESMLog(0, _T("Record Status Error %d %d"), ESMGetRecordStatus(), ESMGetNextRecordStatus());
				break;
			}
			else
				ESMSetRecordStatus(ESMGetNextRecordStatus());

			ESMLog(1, _T("Recieve MovieSignal"));
			CDSCGroup* pGroup = pDSCMgr->m_pDSCViewer->GetDSCBaseGroup();
			if(pGroup)
				pGroup->DoMovieCaptureLocal((int)pMsg->nParam2);
			break;
		}
	case WM_RS_MC_CAPTURE_IMAGE_REQUIRE_6		:
		break;
	default:
		ESMLog(0, _T("OnEventNetCapture() Unknown message [%d]\n"),pMsg->message);
		break;
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
	if( stDscData)
	{
		delete stDscData;
		stDscData = NULL;
	}
	return 0;
}

void CDSCMgr::OnEventNetMovieCancel(ESMEvent* pMsg)
{
	DoMovieCancel();

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
}