/////////////////////////////////////////////////////////////////////////////
//
// Registry.h: implementation of the Registry class.
//
//
// Copyright (c) 2003 Samsung SDS, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of Samsung SDS, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2003-11-11
//
/////////////////////////////////////////////////////////////////////////////

#ifndef __REGISTRY_H_
#define __REGISTRY_H_

#include <winreg.h>

#define REG_RECT	0x0001
#define REG_POINT	0x0002

//UNICODE 빌드시 Read() 함수에서 비정상적으로 읽혀
//확인하기 위해 사용안하는 함수는 임시로 주석처리함.
//추후 UNICODE 빌드시 사용하기 위해 확인하고 사용하면 됨.

class CRegistry : public CObject
{
// Construction
public:
	CRegistry(HKEY hKeyRoot = HKEY_LOCAL_MACHINE);
	virtual ~CRegistry();

	//struct REGINFO
	//{
	//	LONG lMessage;
	//	DWORD dwType;
	//	DWORD dwSize;
	//} m_Info;

// Operations
public:

	HKEY 	m_hKey;
	void Close();

	BOOL VerifyKey (HKEY hKeyRoot, LPCTSTR pszPath);
	//BOOL VerifyKey (LPCTSTR pszPath);
	//BOOL VerifyValue (LPCTSTR pszValue);
	BOOL CreateKey (HKEY hKeyRoot, LPCTSTR pszPath);
	BOOL Open (HKEY hKeyRoot, LPCTSTR pszPath);

	//BOOL DeleteValue (LPCTSTR pszValue);
	//BOOL DeleteValueKey (HKEY hKeyRoot, LPCTSTR pszPath);

	BOOL Write (LPCTSTR pszKey, int iVal);
	BOOL Write (LPCTSTR pszKey, LPCTSTR pszVal);

	BOOL Read (LPCTSTR pszKey, CString& sVal);	
	BOOL Read (LPCTSTR pszKey, UINT& iVal);
	//-- 2011-06-07 hongsu.jung
	BOOL Read (LPCTSTR pszKey, int& iVal);

	//** DISCOVERY
	//CString GetEventLogLists(HKEY hKeyRoot, LPCTSTR pszPath);
	//** 2003-11-11
	//** DISCOVERY - SERVICE
	//CString GetServiceList();
	//CString GetDispName(CString strService, int& nType);

	//** GET VALUES
	//BOOL GetServiceItem(int nIndex, CString& strRegistry, CString& strDisplay, int& nType);

protected:	
	//CString m_sPath;
};

#endif // !defined (__REGISTRY_H_)

