////////////////////////////////////////////////////////////////////////////////
//
//	rsfunc.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2011-05-31
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "SRSIndex.h"

//-- 2011-05-31 hongsu.jung
extern	void RSInit();
extern	CString RSGetRoot();
extern	CString RSGetRoot(CString strPath);
//-- 2011-6-16 Lee JungTaek
extern void CreateAllDirectories(CString csPath);

//-- 2011-6-20 Lee JungTaek
extern int RSGetValueEnum(int nOption);
extern CString RSGetValueStr(int nOption);
extern void RSReloadOption();
extern CString GetDateTime();
extern CString GetDate();
extern CString GetTime();

extern BOOL RSGetFocus();
extern void RSSetFocus(BOOL bFocus);
extern BOOL	RSGetMFFocus();
extern void RSSetMFFocus(BOOL bFocus);


class CRSArray : public CObArray
{
public:
	CRSArray()	{ InitializeCriticalSection (&_RSArray);		}
	~CRSArray()	{ DeleteCriticalSection (&_RSArray);		}

private:
	CRITICAL_SECTION _RSArray;

public:
	int Add(CObject* p)
	{
		int nReturn;
		EnterCriticalSection (&_RSArray);
		nReturn = (int)CObArray::Add(p);
		LeaveCriticalSection (&_RSArray);
		return nReturn;
	}

	int GetSize()
	{
		int nReturn = 0;

		if(&_RSArray)
		{
			EnterCriticalSection (&_RSArray);
			nReturn = (int)CObArray::GetSize();
			LeaveCriticalSection (&_RSArray);
		}
		return nReturn;
	}

	CObject* GetAt(int nIndex)
	{
		int nAll = GetSize();
		CObject * pObject = NULL;
		if(nIndex > nAll - 1 || nIndex < 0 )
			return NULL;
		EnterCriticalSection (&_RSArray);
		pObject = CObArray::GetAt(nIndex);
		LeaveCriticalSection (&_RSArray);
		return pObject;
	}

	int Insert(int nIndex, CObject* p)
	{
		int nReturn = 0;
		EnterCriticalSection (&_RSArray);
		CObArray::InsertAt(nIndex, p);
		LeaveCriticalSection (&_RSArray);
		return nReturn;
	}
};