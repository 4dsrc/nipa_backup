
#include "StdAfx.h"
#include "RSFunc.h"
#include "NXRemoteStudioDlg.h"
//-- 2011-06-14
//-- time measure
#include <sys/timeb.h>
#include <time.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CNXRemoteStudioDlg* g_pMainDlgWnd = NULL;	

//------------------------------------------------------------------------------ 
//! @brief		void TGInit()
//! @date		
//! @attention	none
//! @note	 	//-- 2009-05-25
//------------------------------------------------------------------------------ 
void RSInit()
{
	//-- GET MAINFRAME POINT
	g_pMainDlgWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();

}

//------------------------------------------------------------------------------ 
//! @brief		RSGetRoot
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
CString RSGetRoot()
{
	if(!g_pMainDlgWnd)
		return _T("");	
	return g_pMainDlgWnd->GetRoot();
}

BOOL RSGetFocus()
{
	return g_pMainDlgWnd->GetEnableFocus();
}

void RSSetFocus(BOOL bFocus)
{
	g_pMainDlgWnd->SetEnableFocus(bFocus);
}

BOOL RSGetMFFocus()
{
	return g_pMainDlgWnd->GetEnableMFFocus();
}

void RSSetMFFocus(BOOL bFocus)
{
	g_pMainDlgWnd->SetEnableMFFocus(bFocus);
}

//------------------------------------------------------------------------------ 
//! @brief		RSGetRoot
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------  
CString RSGetRoot(CString strPath)
{
	if(!g_pMainDlgWnd)
		return _T("");	
	return g_pMainDlgWnd->GetRoot(strPath);
}

//------------------------------------------------------------------------------ 
//! @brief		GetDateTime
//! @date		2011-05-21
//! @author	Lee JungTaek
//! @revision	2011-05-23
//! @revisor	Hongsu Jung
//! @note	 	Capture Image
//------------------------------------------------------------------------------ 
CString GetDateTime()
{
	CString strDate = _T("");
	CTime time  = CTime::GetCurrentTime();
	int nYear = time.GetYear();
	int nMonth = time.GetMonth();
	int nDay = time.GetDay();
	int nHour = time.GetHour();
	int nMin = time.GetMinute();
	int nSec = time.GetSecond();

	strDate.Format(_T("%d%02d%02d_%02d%02d%02d"), nYear, nMonth, nDay, nHour, nMin ,nSec);
	return strDate;
}

//------------------------------------------------------------------------------ 
//! @brief		GetTime
//! @date		2011-7-18
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString GetDate()
{
	CString strDate = _T("");
	CTime time  = CTime::GetCurrentTime();
	int nYear = time.GetYear();
	int nMonth = time.GetMonth();
	int nDay = time.GetDay();

	strDate.Format(_T("%d%02d%02d"), nYear, nMonth, nDay);
	return strDate;
}

//------------------------------------------------------------------------------ 
//! @brief		GetTime
//! @date		2011-7-18
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString GetTime()
{
	struct _timeb milli_time;
	_ftime( &milli_time );	

	CString strTime = _T("");
	CTime time  = CTime::GetCurrentTime();
	int nHour = time.GetHour();
	int nMin = time.GetMinute();
	int nSec = time.GetSecond();
	int nMilliSec = milli_time.millitm;

	strTime.Format(_T("%02d%02d%02d.%03d"), nHour, nMin, nSec, nMilliSec);
					   
	return strTime;
}

//---------------------------------------------------------
//! @brief		RemoveSlash
//! @date		2010-5-6 keunbae.song
//! @attention	none
//! @note	 	
//---------------------------------------------------------
CString RemoveSlash(LPCTSTR Path)
{
	CString cs = Path;
	::PathRemoveBackslash(cs.GetBuffer(_MAX_PATH));
	cs.ReleaseBuffer(-1);
	return cs;
}

//---------------------------------------------------------
//! @brief		FileExists
//! @date		2010-5-6 keunbae.song
//! @attention	none
//! @note	 	
//---------------------------------------------------------
BOOL FileExists(LPCTSTR Path)
{
	return (::PathFileExists(Path));
}

//---------------------------------------------------------
//! @brief		CreateAllDirectories
//! @date		2010-5-6 keunbae.song
//! @attention	none
//! @note	 	
//---------------------------------------------------------
void CreateAllDirectories(CString csPath)
{
	if(csPath.IsEmpty())
		return;

	// Remove ending / if exists
	RemoveSlash(csPath);

	//If this folder already exists no need to create it
	if(FileExists(csPath))
		return;

	// Recursive call, one fewer folders
	int nFound = csPath.ReverseFind(_T('\\'));
	CreateAllDirectories(csPath.Left(nFound));

	// Actually create a folder
	CreateDirectory(csPath,NULL);
}

//------------------------------------------------------------------------------ 
//! @brief		RSGetValueEnum
//! @date		2011-6-16
//! @author		Lee JungTaek
//! @note	 	Get Option Value
//------------------------------------------------------------------------------ 
int RSGetValueEnum(int nOption)
{
	int nRet = -1;
	if(!g_pMainDlgWnd)
		return nRet;

	if(!g_pMainDlgWnd->m_pOptionDlg)
		return nRet;

	switch(nOption)
	{
	case RS_OPT_TIMELAPSE_DELAYENABLE:
		nRet = g_pMainDlgWnd->m_pOptionDlg->m_pOptionBody->m_pOption->m_TimeLapse.bDelaySet;
		break;
	case RS_OPT_TIMELAPSE_TIMERENABLE:
		nRet = g_pMainDlgWnd->m_pOptionDlg->m_pOptionBody->m_pOption->m_TimeLapse.bTimerCaptureSet;
		break;
	case RS_OPT_TIMELAPSE_DELAYTIME:
		nRet = g_pMainDlgWnd->m_pOptionDlg->m_RSOption.m_TimeLapse.nDeleyTime;
		break;
	case RS_OPT_TIMELAPSE_TIMERCAPTUREINTERVAL:
		nRet = g_pMainDlgWnd->m_pOptionDlg->m_RSOption.m_TimeLapse.nTimerTime;
		break;
	case RS_OPT_TIMELAPSE_TIMERCAPTURECNT:
		nRet = g_pMainDlgWnd->m_pOptionDlg->m_RSOption.m_TimeLapse.nCaptureCount;
		break;
	case RS_OPT_PCSET2_FILENAME_TYPE:
		nRet = g_pMainDlgWnd->m_pOptionDlg->m_RSOption.m_PCSet2.nFileNameType;
		break;
	case RS_OPT_PCSET2_CHECKVIEW:
		nRet = g_pMainDlgWnd->m_pOptionDlg->m_RSOption.m_PCSet2.nCheckView;
		break;
	case RS_OPT_PCSET2_SELECTVIEW:
		nRet = g_pMainDlgWnd->m_pOptionDlg->m_RSOption.m_PCSet2.nSelectView;
	case RS_OPT_PCSET2_SELECTVIEWSIZE:
		nRet = g_pMainDlgWnd->m_pOptionDlg->m_RSOption.m_PCSet2.nSelectViewSize;
		break;
	}

	return nRet;
}

//------------------------------------------------------------------------------ 
//! @brief		RSGetValueStr
//! @date		2011-6-30
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
extern CString RSGetValueStr(int nOption)
{
	CString strValue = _T("");
	if(!g_pMainDlgWnd-> m_pOptionDlg)
		return strValue;

	switch(nOption)
	{
	case RS_OPT_PCSET1_PICTUREPATH:		strValue = g_pMainDlgWnd-> m_pOptionDlg->m_RSOption.m_PCSet1.strPictureRootPath;		break;
	case RS_OPT_PCSET1_BANKPATH:		strValue = g_pMainDlgWnd->m_pOptionDlg->m_RSOption.m_PCSet1.strBankPath;		break;
	case RS_OPT_PCSET2_PREFIX:				strValue = g_pMainDlgWnd->m_pOptionDlg->m_RSOption.m_PCSet2.strPrefix;		break;
	default:
		strValue = _T("C:\\picture");
	}

	return strValue;
}

//------------------------------------------------------------------------------ 
//! @brief		RSReloadOption()
//! @date		2011-6-20
//! @author		Lee JungTaek
//! @note	 	Reload Option
//------------------------------------------------------------------------------ 
void RSReloadOption()
{
	g_pMainDlgWnd->m_pOptionDlg->LoadOptionInfo();
}