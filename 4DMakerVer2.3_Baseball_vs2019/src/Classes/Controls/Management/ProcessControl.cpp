/////////////////////////////////////////////////////////////////////////////
//
// ProcessControl.cpp: implementation of the Registry class.
//
//
// Copyright (c) 2008 Samsung SDS, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of Samsung SDS, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2008-06-26
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ProcessControl.h"
#include "Tlhelp32.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ProcessControl::ProcessControl() {}
ProcessControl::~ProcessControl() {}


BOOL ProcessControl::KillProcess(CString strProcess)
{
	HANDLE hndl = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
	DWORD dwsma = GetLastError();

	DWORD dwExitCode = 0;

	PROCESSENTRY32  procEntry={0};
	procEntry.dwSize = sizeof( PROCESSENTRY32 );

	//-- Check SRS
	CString strExist;
	Process32First(hndl,&procEntry);	
	int nCnt = 0;
	do
	{
		strExist.Format(_T("%s"),procEntry.szExeFile);
		if(strExist == strProcess)
			nCnt++;
	}while(Process32Next(hndl,&procEntry));	

	//-- Kill Process
	Process32First(hndl,&procEntry);	
	do
	{
		strExist.Format(_T("%s"),procEntry.szExeFile);
		if(strExist == strProcess)
		{
			nCnt--;
			if(nCnt >  0)
			{
				//prevent
				HANDLE hHandle = ::OpenProcess(PROCESS_ALL_ACCESS,0,procEntry.th32ProcessID);

				//-- 2011-8-4 Lee JungTaek
				//-- Code Sonar
				if(hHandle)
				{
					::GetExitCodeProcess(hHandle,&dwExitCode);
					::TerminateProcess(hHandle,dwExitCode);

					delete hHandle;
					hHandle = NULL;
				}				
			}
		}		
	}while(Process32Next(hndl,&procEntry));		
	return FALSE;
}

//----------------------------------------------------
//--
//-- GET WINDOWS HANDLE
//-- 2008-07-14
//--
//----------------------------------------------------

typedef struct tagWNDINFO { 
    DWORD   dwProcessID; 
    HWND    hWnd; 
} WNDINFO; 

BOOL CALLBACK EnumWindowsProc(HWND hWnd, LPARAM lParam)
{ 
	WNDINFO*    pWndInfo = (WNDINFO*)(lParam); 
	DWORD       dwProcessID; 

	::GetWindowThreadProcessId(hWnd, &dwProcessID);
	if (dwProcessID == pWndInfo->dwProcessID) 
	{ 
		pWndInfo->hWnd = hWnd;
		return(FALSE);
	} 
	return(TRUE);
} 

BOOL ProcessControl::GetWindowHandelFromPID(DWORD pid)
{
	BOOL brc; 
	WNDINFO wi;
	//LRESULT lResult;

	wi.dwProcessID = pid; 
	wi.hWnd = 0; 
	brc = EnumWindows( EnumWindowsProc, (LPARAM) &wi); 	

	if (wi.hWnd) 
	{ 
		TRACE("Window found \n");
		DWORD fromId = GetCurrentThreadId(); 
		DWORD toId = GetWindowThreadProcessId(GetForegroundWindow(), NULL); 
		bool succeeded; 
	    AttachThreadInput(fromId, toId, TRUE); 
	    succeeded = (bool)SetForegroundWindow(wi.hWnd); 
	    AttachThreadInput(fromId, toId, FALSE); 
		return succeeded;
	} 
	else 
		TRACE("Window not found\n");
	return FALSE;
}

