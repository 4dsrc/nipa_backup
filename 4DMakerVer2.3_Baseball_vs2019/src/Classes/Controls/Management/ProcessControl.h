/////////////////////////////////////////////////////////////////////////////
//
// ProcessControl.h: implementation of the Registry class.
//
//
// Copyright (c) 2008 Samsung SDS, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of Samsung SDS, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2008-06-26
//
/////////////////////////////////////////////////////////////////////////////

#pragma once


class ProcessControl
{
public:
	ProcessControl();
	virtual ~ProcessControl();

public:
	//-- Kill Process
	BOOL KillProcess(CString strProcess);
	BOOL GetWindowHandelFromPID(DWORD pid);
};


