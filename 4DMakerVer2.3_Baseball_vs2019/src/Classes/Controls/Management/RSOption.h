/////////////////////////////////////////////////////////////////////////////
//
//	TGOption.h: main header file for the TestGuarantee application
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2009-02-01
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "SRSIndex.h"

class CRSOption
{
public:
	CRSOption();
	virtual ~CRSOption();

	// page data struct
	RSTimeLapse			m_TimeLapse;
	RSCustom			m_Custom;
	RSDSCSet1			m_DSCSet1;
	RSDSCSet2			m_DSCSet2;
	RSDSCSet3			m_DSCSet3;
	RSPCSet1			m_PCSet1;
	RSPCSet2			m_PCSet2;
	RSAbout				m_About;
	//CMiLRe 20140918 OPTION Tab �߰� & VScroll �߰�
	RSDSCSetEtc			m_DSCSetEtc;

public:
	//-- LOAD
	void Load(CString strFile);
	//-- Create Path Directory
	void CreateDir();

private:
	// Get Data from File
 	void LoadTimeLapse(BOOL, int, int , BOOL, int, int, int);
	void LoadPCSet1(CString, CString);
	void LoadPCSet2(CString, int, int, int, int);
// 	void LoadCustom(BOOL, BOOL , BOOL);
// 	void LoadDSCSet1(BOOL, BOOL , BOOL);
// 	void LoadDSCSet2(BOOL, BOOL , BOOL);
// 	void LoadPCSet1(BOOL, BOOL , BOOL);
// 	void LoadPCSet2(BOOL, BOOL , BOOL);
// 	void LoadAbout(BOOL, BOOL , BOOL);
};
