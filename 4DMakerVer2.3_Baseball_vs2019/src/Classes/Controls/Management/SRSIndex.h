/////////////////////////////////////////////////////////////////////////////
//
//  ptpIndex.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-09
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ptpIndex.h"

///////////////////////////////////////////////////////
///					Define							///
///////////////////////////////////////////////////////
#define ARR_SIZE(A)	(sizeof(A) / sizeof(A[0]))
#define MAGNET_ATTACH_RANGE 50

#define MAIN_WND_WIDTH			304
#define MAIN_WND_HEIGHT			219 + 41//219 + 31
#define SMART_PANEL_HEIGHT		290
#define LIVEVIEW_DLG_WIDTH		635
#define LIVEVIEW_DLG_HEIGHT		660
#define OPTION_DLG_WIDTH_2000		398
//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
#define OPTION_DLG_WIDTH		444
#define OPTION_DLG_HEIGHT		240
#define OPTION_TAB_HEIGHT		39

//EVF
//#define NX1_SRC_WIDTH		720	//1024
//#define NX1_SRC_HEIGHT		480 //-- 2013-6-20 yongmin.lee
#define NX1_SRC_WIDTH		1024	//1024
#define NX1_SRC_HEIGHT		684 //-- 2013-6-20 yongmin.lee

#define NX2000_SRC_WIDTH		800	//1024
#define NX2000_SRC_HEIGHT		482 //-- 2013-6-20 yongmin.lee
#define SRC_WIDTH				640	//1024
#define SRC_HEIGHT				424 //-- 2013-6-20 yongmin.lee
#define NX500_SRC_WIDTH			720
#define NX500_SRC_HEIGHT		480
#define GH5_SRC_WIDTH			640
#define GH5_SRC_HEIGHT			360
#define CLIPING(data)  ( (data) < 0 ? 0 : ( (data) > 255 ) ? 255 : ( data ) )
#define SELECT_NOTHING			-1
#define MAIN_LIVEVIEW			-1
#define MULTILIVEVIEW_CNT		4
#define STR_NOT_CONNECTED		_T("NOT CONNECTED")
///////////////////////////////////////////////////////
///					Define - ini Info				///
///////////////////////////////////////////////////////
//-- SmartPanel
#define INFO_LAYOUT_TYPE		_T("Type")
#define INFO_LAYOUT_X			_T("PosX")
#define INFO_LAYOUT_Y			_T("PosY")
#define INFO_LAYOUT_W			_T("Width")
#define INFO_LAYOUT_H			_T("Height")
#define INFO_LAYOUT_SEP_V		_T("Vertical")
#define INFO_LAYOUT_SEP_H		_T("Horizon")
#define INFO_LAYOUT_SHOW		_T("Show")

#define INFO_PROPERTY_VALUE_COUNT	_T("ITEM_COUNT")
#define INFO_PROPERTY_VALUE_NAME	_T("VALUE_NAME")
#define INFO_PROPERTY_IMAGE_NORMAL	_T("IMAGE_NORMAL")
#define INFO_PROPERTY_IMAGE_SELECT	_T("IMAGE_SELECT")
#define INFO_PROPERTY_IMAGE_DISABLE	_T("IMAGE_DISABLE")

//-- Registry
#define REG_RS_ROOT				_T("SOFTWARE\\Samsung\\SRS")
#define REG_RS_ROOT_64			_T("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Samsung Remote Studio")
#define REG_RS_ROOT_32			_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Samsung Remote Studio")
#define REG_RS_POS				_T("position")
//#define REG_RS_ROOT_PATH		_T("directory")
#define REG_RS_ROOT_PATH		_T("programFolder")
#define REG_RS_DSC_NAME			_T("dscname")
#define REG_RS_UPDATE			_T("update")
#define REG_RS_VERSION			_T("version")
#define REG_RS_OPENPROGRAM		_T("openprogram")
#define REG_RAW_ROOT			_T("SOFTWARE\\Samsung\\Samsung RAW Converter 4")
#define REG_RAW_DIR				_T("DIRECTORY")


#define REG_RS_MAINWIN			_T("MainWindow")
#define REG_RS_LIVEVIEW			_T("Liveview")
#define REG_RS_SMARTPANEL		_T("SmartPanel")
#define REG_RS_PICTUREVIEWER	_T("PictureViewer")
#define REG_RS_ZOOMER			_T("Zoomer")

//-- Option
#define INFO_OPT_TIMELAPSE		_T("TIME LAPSE")
#define INFO_OPT_CUSTOM			_T("CUSTOM")
#define INFO_OPT_DSCSET1		_T("DSC_SET1")
#define INFO_OPT_DSCSET2		_T("DSC_SET2")
#define INFO_OPT_PCSET1_32		_T("PC_SET1_32")
#define INFO_OPT_PCSET1_64		_T("PC_SET1_64")
#define INFO_OPT_PCSET2			_T("PC_SET2")
#define INFO_OPT_ABOUT			_T("ABOUT")

//TimeLapse
#define INFO_TIMELAPSE_DELAYENABLE	_T("DelayEnable")
#define INFO_TIMELAPSE_DELAYMIN		_T("DelayMin")
#define INFO_TIMELAPSE_DELAYSEC		_T("DelaySec")
#define INFO_TIMELAPSE_TIMERENABLE	_T("TimerEnable")
#define INFO_TIMELAPSE_TIMERMIN		_T("TimerMin")
#define INFO_TIMELAPSE_TIMERSEC		_T("TimerSec")
#define INFO_TIMELAPSE_TIMERCOUNT	_T("CaptureNumber")


//PC Set1
#define INFO_PCSET1_PICTUREPATH		_T("Picture")
#define INFO_PCSET1_BANKPATH		_T("Bank")

//PC Set2
#define INFO_PCSET2_PREFIX			_T("Prefix")
#define INFO_PCSET2_FILENAME_TYPE	_T("FileNameType")
#define INFO_PCSET2_CHECKVIEW		_T("CheckView")
#define INFO_PCSET2_SELECTVIEW		_T("SelectView")
#define INFO_PCSET2_SELECTVIEWSIZE	_T("ViewSize")

#define BANK_FILE_NAME				_T("spb")

#define RGB_DEFAULT_DLG				RGB(58,58,58)
#define RGB_BLACK					RGB(0,0,0)
#define RGB_WHITE					RGB(255,255,255)

///////////////////////////////////////////////////////
///					WM_MESSAGE
///////////////////////////////////////////////////////

#define WM_RS									WM_USER+0x9000
//-- Multi Control
#define WM_RS_MC								WM_USER+1500
#define WM_RS_MC_OPEN_SESSION					WM_USER+1501
#define WM_RS_MC_CLOSE_SESSION					WM_USER+1502
#define WM_RS_MC_GET_PTP_DEVICEINFO				WM_USER+1503
#define WM_RS_MC_CAPTURE_IMAGE					WM_USER+1504
#define WM_RS_MC_LIVEVIEW_IMAGE_EVENT			WM_USER+1506
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1		WM_USER+1507		// chlee Add
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2		WM_USER+1508		// chlee Add
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_3		WM_USER+1509		// chlee Add
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4		WM_USER+1510		// chlee Add
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5		WM_USER+1511		// yongmin.lee
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_6		WM_USER+1512		// yongmin.lee
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_7		WM_USER+1513		
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_ONCE		WM_USER+1514		// chlee Add
#define WM_RS_MC_RECORD_MOVIE					WM_USER+1515
#define WM_RS_MC_RECORD_CANCLE					WM_USER+1516
#define WM_RS_MC_GET_RECORD_STATUS				WM_USER+1517
#define WM_RS_MC_GET_CAPTURE_COUNT				WM_USER+1518
#define WM_RS_MC_CAPTURE_IMAGE_RECORD			WM_USER+1519


//-- Smart Panel
#define WM_RS_SP_SET_PROPERTY_VALUE				WM_USER+1551
#define WM_RS_SP_UPDATE_PROPERTY_VALUE			WM_USER+1552
#define WM_RS_LV_UPDATE_TUMBNAIL				WM_USER+1553
//-- LiveView Dialog
#define WM_RS_LV_CHANGE_DEVICE					WM_USER+1554
#define WM_RS_LV_GET_INFO						WM_USER+1555
//-- Get Property
#define WM_RS_GET_PROPERTY_INFO					WM_USER+1600
#define WM_RS_SET_PROPERTY_INFO					WM_USER+1601
#define WM_RS_UPDATE_PROPERTY_VALUE				WM_USER+1602
//-- 2011-6-16 Lee JungTaek
#define WM_RS_GET_UNIQUE_ID						WM_USER+1604
//-- 2011-7-8 Lee JungTaek
#define WM_RS_SET_DSC_NAME						WM_USER+1606
#define WM_RS_SAVE_BANK							WM_USER+1607
#define WM_RS_LOAD_BANK							WM_USER+1608
//-- 2011-07-14 hongsu.jung
#define WM_RS_SET_CHANGE_LIVEVIEW_SIZE			WM_USER+1609
//-- 2011-7-18 Lee JungTaek
#define WM_RS_START_TIMER_CAPUTRE				WM_USER+1610
#define WM_RS_STOP_TIMER_CAPUTRE				WM_USER+1611
//CMiLRe 20140922 Picture Setting Interval Capture - Capture 
#define WM_RS_START_INTERVAL_CAPUTRE      WM_USER+1612
//-- 2011-7-18 Lee JungTaek
#define WM_RS_OPT_FORMAT_DEVICE					WM_USER+1615
#define WM_RS_OPT_RESET_DEVICE					WM_USER+1616
//CMiLRe NX2000 FirmWare Update CMD 추가
#define WM_RS_OPT_FW_UPDATE						WM_USER+1617

//CMiLRe 20141001 Date, Time 추가
#define WM_RS_OPT_DATE_N_TIME						WM_USER+1618
#define WM_RS_OPT_DATE_N_TIME_UPDATE						WM_USER+1619

//-- 2013-10-23 hongsu
#define WM_RS_SET_TICK							WM_USER+1620
#define WM_RS_SLEEP								WM_USER+1621

//dh0.seo 2014-11-04
#define WM_RS_OPT_SENSOR_CLEANING_DEVICE		WM_USER+1622
//CMiLRe 20141113 IntervalCapture Stop 추가
#define WM_RS_STOP_INTERVAL_CAPUTRE		WM_USER+1623
//CMiLRe 20141127 Display Save Mode 에서 Wake up
#define WM_RS_DISPLAY_SAVE_MODE_WAKE_UP		WM_USER+1624

#define WM_RS_GET_RECORD_STATUS					WM_USER+1625
#define WM_RS_GET_CAPTURE_COUNT					WM_USER+1626
#define WM_RS_SET_RESUME						WM_USER+1627
#define WM_RS_SET_PAUSE							WM_USER+1628

#define	WM_RS_CONTROL_TOUCH_AF					WM_USER+1630
#define WM_RS_GET_INTERVAL_CAPTURE				WM_USER+1631
#define WM_RS_GET_INTERVAL_CAPTURE_VALUE		WM_USER+1632
#define WM_RS_GET_FD_BOX_INFO1					WM_USER+1633
#define WM_RS_GET_FD_BOX_INFO2					WM_USER+1634
#define WM_RS_GET_TRACKING_BOX_INFO1			WM_USER+1635
#define WM_RS_GET_TRACKING_BOX_INFO2			WM_USER+1636
#define WM_RS_GET_IMAGE_FILE_NAME				WM_USER+1637
#define WM_RS_TRACKING_AF_STOP					WM_USER+1638

//-- 2013-10-01 yongmin.lee
//-- Remote Control
#define WM_RS_RC_CAPTURE_IMAGE_REQUIRE			WM_USER+1701
#define WM_RS_RC_STATUS_CHANGE					WM_USER+1702
#define WM_RS_RC_NETWORK_CONNECT				WM_USER+1703
#define WM_RS_RC_GET_PROPERTY					WM_USER+1704
#define WM_RS_RC_SET_PROPERTY					WM_USER+1705
#define WM_RS_RC_LIVEVIEW						WM_USER+1706
#define WM_RS_RC_CAMERA_SYNC					WM_USER+1707
#define WM_RS_RC_MOVIESAVE_CHECK				WM_USER+1708
#define WM_RS_RC_NETWORK_STATUS					WM_USER+1709
#define WM_RS_RC_CONVERT_FINISH					WM_USER+1710
#define WM_RS_RC_GET_FOCUS						WM_USER+1711
#define WM_RS_RC_CAPTURE_CANCEL					WM_USER+1712
#define WM_RS_RC_CAMERA_SYNC_RESULT				WM_USER+1713
#define WM_RS_RC_USBCONNECTFAIL					WM_USER+1714
#define WM_RS_RC_SENSORSYNCOUT					WM_USER+1715
#define WM_RS_RC_ADDFRAME						WM_USER+1716
#define WM_RS_RC_SKIPFRAME						WM_USER+1717
#define WM_RS_RC_ERRORFRAME						WM_USER+1718
#define WM_RS_RC_EXCEPTIONDSC					WM_USER+1719
#define WM_RS_RC_CAPTUREDELAY					WM_USER+1720

#define WM_RS_OPT_DEVICE_REBOOT					WM_USER+1721
#define WM_RS_OPT_FW_DOWNLOAD					WM_USER+1722
#define WM_RS_OPT_DEVICE_POWER_OFF				WM_USER+1723
#define WM_RS_RC_VALIDSKIP						WM_USER+1724
#define WM_RS_RC_RECORDFINISH					WM_USER+1725
#define WM_RS_RC_COPYTOSERVER					WM_USER+1726
#define WM_RS_RC_SETFRAMECOUNT					WM_USER+1727
#define WM_RS_RC_SETSENSORTIME					WM_USER+1728
#define WM_RS_RC_INFORMSENSORNG					WM_USER+1729
#define WM_RS_RC_INFORMSENSOROK					WM_USER+1730
#define WM_RS_RC_CAMERA_SYNCSTOP				WM_USER+1731
#define WM_RS_RC_SYNC_DATASET					WM_USER+1732

#define WM_RS_RC_CAMERA_SYNC_CHECK				WM_USER+1733
#define WM_RS_RC_RECORD_FILE_SPLIT				WM_USER+1734

//jhhan 16-09-23
#define WM_RS_RC_SET_FRAME_RECORD				WM_USER+1735
//joonho.kim 17-02-17
#define WM_RS_RC_SET_MAKING_INFO				WM_USER+1736
//joonho.kim 17-08-31
#define WM_RS_RC_SEND_USBDEVICE_CHECK			WM_USER+1737

#define WM_RS_RC_CAPTURE_IMAGE_REQUIRE_GH5		WM_USER+1738

#define WM_RS_RC_PRE_REC_INIT_GH5				WM_USER+1739
#define WM_RS_RC_PRE_REC_INIT_GH5_RESULT		WM_USER+1740

#define WM_RS_RC_GET_TICK_CMD					WM_USER+1741
#define WM_RS_RC_GET_TICK_CMD_RESULT			WM_USER+1742

#define WM_RS_EVENT_LIVEVIEW_IMAGE				WM_USER+2001
#define WM_RS_MC_LIVEVIEW_INFO					WM_USER+2002
#define WM_RS_MC_LIVEVIEW_OPT					WM_USER+2003
#define WM_RCPCMD_SET_FOCUS						WM_USER+2004
//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
#define WM_RCPCMD_SET_FOCUS_ALL					WM_USER+2005
#define WM_RCPCMD_SET_FOCUS_VALUE				WM_USER+2006

//-- 2013-09-29 hongsu@esmlab.com
//-- Movie File Convert from mp4 to jpg
#define WM_RS_MOVIE_FILE_CONVERT				WM_USER+2100

#define WM_LIVEVIEW								WM_USER+3001
#define WM_LIVEVIEW_EVENT_SHIFT					WM_USER+3002
#define WM_LIVEVIEW_EVENT_FULL_SCREEN			WM_USER+3004
#define WM_LIVEVIEW_EVENT_SEPARATOR				WM_USER+3005
#define WM_LIVEVIEW_GRID_CHANGE					WM_USER+3006
#define WM_LIVEVIEW_ZOOM_WINDOW					WM_USER+3007
//-- 2011-6-24 Lee JungTaek
#define WM_LIVEVIEW_EVENT_SET_FOCUS				WM_USER+3008
#define WM_LIVEVIEW_EVENT_SIZE					WM_USER+3009
#define WM_RS_LIVEVIEW_GET_FOCUSPOSITION		WM_USER+3010
#define WM_RS_LIVEVIEW_SET_FOCUSPOSITION		WM_USER+3011
#define WM_RS_SET_FOCUS_VALUE					WM_USER+3012
#define WM_RECORD_POPUP_DLG						WM_USER+3013
#define WM_RECORD_STATUS_CHECK					WM_USER+3014
#define WM_RS_SET_FOCUS_MODE					WM_USER+3015
#define WM_RS_FILE_TRANSFER_COMPLETE			WM_USER+3016
//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
#define WM_LIVEVIEW_EVENT_SET_FOCUS_VALUE					WM_USER+3017
#define WM_CAPTURE_FILE_WRITE_DONE				WM_USER+3018
#define WM_RS_DESTROY							WM_USER+3019
#define WM_LIVE_EVF_ON_OFF						WM_USER+3020
#define WM_RS_SET_SELECT_AF_POS					WM_USER+3021
#define WM_RS_SET_AF_FOCUS						WM_USER+3022
#define WM_RS_SET_AF_FOCUS_COLOR				WM_USER+3023
#define WM_RS_GET_IMAGE_PATH					WM_USER+3024
#define WM_RS_SET_TRACKING_AF_STOP				WM_USER+3025
#define WM_CAPTURE_FILE_WRITE_ERROR				WM_USER+3026

#define WM_LOAD_THUMBNAIL						WM_USER+4001
///////////////////////////////////////////////////////
///	-- TransParent Dialog							///
///////////////////////////////////////////////////////
#define	WM_END_RESIZE_EVENT						WM_USER+5010

#define TAB_CHANGE								WM_USER+5000
#define TAB_BALLOON								WM_USER+5001
#define WM_LOG_MSG								WM_USER+5002



///////////////////////////////////////////////////////
///					Define - String					///
///////////////////////////////////////////////////////
#define STR_CONFIG_PATH							_T("config")
#define STR_CONFIG_LAYOUT_PATH					_T("config\\Layout")
#define STR_CONFIG_ENUMVALUE_PATH				_T("config\\EnumValue")
#define STR_IMAGE_COMMON_ICON_PATH				_T("img\\00.Common")
#define STR_IMAGE_LIGHTSTUDIO_ICON_PATH			_T("img\\01.Smart Panel")
#define STR_IMAGE_SMARTPANNEL_ICON_PATH			_T("img\\02.Smart Panel\\List Icon")

///////////////////////////////////////////////////////
///					enum							///
///////////////////////////////////////////////////////
enum MAIN_MAGNET 
{ 
	MAIN_NULL = 0, 
	MAIN_TOP, 
	MAIN_RIGHT, 
	MAIN_LEFT, 
	MAIN_BOTTOM 
};

#ifndef BUTTON_STATUS
enum BUTTON_STATUS
{
	BTN_NOTHING = -1,
	BTN_NORMAL,
	BTN_INSIDE,
	BTN_DOWN,
	BTN_DISABLE,
};
#endif //BUTTON_STATUS

enum BUTTON_COMMAND
{
	BTN_CMD_NULL = -1,
	BTN_MF_BAR,
	BTN_MF_BAR_HANDLE,
	BTN_CMD_REW,
	BTN_CMD_PREV ,
	BTN_CMD_NEXT ,
	BTN_CMD_FF ,
	BTN_CMD_OPT ,
	BTN_CMD_LIVE ,
	BTN_CMD_MULTI,
	BTN_CMD_SHUT ,
	BTN_CMD_AFMF,
	//NX1 dh0.seo 2014-08-27
	BTN_CMD_RECORD,
	BTN_MF_NEAR,
	BTN_MF_FAR,
	BTN_RECORD_START,
	BTN_RECORD_STOP,
	//dh0.seo 2014-11-19
	BTN_SAVE_BANK,
	BTN_LOAD_BANK,
	BTN_MF_AF,
	BTN_RIGHT,
	BTN_LEFT,
	BTN_DRAG,
	BTN_BAR,
	BTN_CAMERA1,
	BTN_CAMERA2,
	BTN_MOVIE,
	BTN_SETTINGS,
	BTN_TITLE,
	BTN_CHECK,
	BTN_ADJUST,
	BTN_EDIT,
	BTN_FORMAT,
	BTN_RESET,
	BTN_LIVEVIEW,
	BTN_QUICKVIEW,
	BTN_THUMBNAIL_LEFT,
	BTN_THUMBNAIL_RIGHT,
	BTN_THUMBNAIL_DRAG,
	BTN_ROTATE_LEFT,
	BTN_ROTATE_RIGHT,
	BTN_ROTATE_GRID,
	BTN_AF_POINT,
	BTN_CENTER,
	BTN_ZOOM_LEFT,
	BTN_ZOOM_RIGHT,
	BTN_ZOOM_HANDLE,
	BTN_CAPTURE,
	BTN_RECORD,
	BTN_PAUSE,
	BTN_CUSTOM_PLUS,
	BTN_CUSTOM_MINUS,
	BTN_SETTING,
	BTN_MIN,
	BTN_MAX,
	BTN_CLOSE,
	BTN_CMD_CNT,
	//CMiLRe 20141118 Lens Focus 조정, 슬라이드 바형식, 좌우 조절
	BTN_CMD_MF_CHANGE = 0xFE,
};
//-- 2011-7-7 chlee
enum OS_VERSION 
{
	OS_UNKNOWN = 0,
	OS_WINDOWS_XP,
	OS_WINDOWS_VISTA,
	OS_WINDOWS_SEVEN,
	OS_WINDOWS_EIGHT
};

enum IMAGE_MULTIAPPLY_TYPE
{
	IMAGE_MUTLIAPPY_MAINCAMERA = 0,
	IMAGE_MUTLIAPPY_DISABLE,
	IMAGE_MUTLIAPPY_UNCHECK,
	IMAGE_MUTLIAPPY_CHECK,
};

enum CAMERA_BTN_1ST_BK_STATUS
{
	BTN_BK_1ST_STATUS_NULL = -1,
	BTN_BK_1ST_STATUS_NORMAL = 0,
	BTN_BK_1ST_STATUS_FOCUS,
	BTN_BK_1ST_STATUS_PUSH,
	BTN_BK_1ST_STATUS_DISABLE,
};

enum CAMERA_BTN_TYPE
{
	BTN_BK_TYPE_1ST,
	BTN_BK_TYPE_2ND,
};

enum CAMERA_BTN_2ND_BK_STATUS
{
	BTN_BK_2ND_STATUS_NULL	= -1,
	BTN_BK_2ND_STATUS_NORMAL,
	//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
	BTN_BK_2ND_STATUS_SELECT,
	BTN_BK_2ND_STATUS_FOCUS,
};

enum CAMERA_BTN_2ND_BK_POS_STATUS
{
	BTN_BK_2ND_POS_NULL = -1,
	BTN_BK_2ND_POS_NORMAL	,
	BTN_BK_2ND_POS_LEFT		,
	BTN_BK_2ND_POS_RIGHT	,
	BTN_BK_2ND_POS_TOP_LEFT,
	BTN_BK_2ND_POS_TOP_RIGHT,
	BTN_BK_2ND_POS_BOTTOM_LEFT,
	BTN_BK_2ND_POS_BOTTOM_RIGHT,
};

enum OPTION_TAB
{
	OPT_TAB_NOTHING = -1,
	OPT_TAB_TIMELAPSE	= 0,
	OPT_TAB_CUSTOM		,
	OPT_TAB_DSC_SET1	,
	OPT_TAB_DSC_SET2	,
	OPT_TAB_DSC_SET3	,
	OPT_TAB_PC_SET1		,
	OPT_TAB_PC_SET2		,
	OPT_TAB_DSC_SET_ETC	,
	OPT_TAB_ABOUT		,
	//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
	OPT_TAB_CNT,
};

// CLiveview
enum SHIFT_COMMAND
{ 
	SHIFT_LEFT	= 0, 
	SHIFT_RIGHT, 
};

// CLiveview
enum SHIFT_POSITION
{ 
	SHIFT_0	= 0, 
	SHIFT_90, 
	SHIFT_180, 
	SHIFT_270, 	
};

enum GRID_OPT
{
	GRID_NULL	= 0,
	GRID_2X2		,
	GRID_3X3		,
	GRID_7X7		,
	GRID_PLUS	,
	GRID_X		,
	GRID_CNT,
};

enum ZOOM_OPT
{
	ZOOM_OFF	= 0,
	ZOOM_1_2	,
	ZOOM_1_3	,
	ZOOM_1_4	,	
	ZOOM_CNT,
};

enum AF_FOCUS_OPT
{
	AF_NULL	= 0,
	AF_DETECT	,
	AF_FAIL		,
	AF_CANCEL	,
	AF_FAIL_CONTINOUSAF,
};


enum LIST_OPT
{
	CMB_MULTI	= 0,
	CMB_ZOOM	,
	CMB_GRID,
	CMB_BANK,		//-- 2011-7-14 Lee JungTaek
	CMB_CNT,
};

enum CONNECTION_CHANGE
{
	CONNECT_INFO_ERR = 0,
	CONNECT_INFO_ADD,
	CONNECT_INFO_REMOVE_THIS,
	CONNECT_INFO_REMOVE_BEFORE,
	CONNECT_INFO_REMOVE_AFTER,
	CONNECT_INFO_NULL,	
};

enum RS_OPT_PROPERTY
{
	RS_OPT_TIMELAPSE_DELAYENABLE,
	RS_OPT_TIMELAPSE_TIMERENABLE,
	RS_OPT_TIMELAPSE_DELAYTIME,
	RS_OPT_TIMELAPSE_TIMERCAPTUREINTERVAL,
	RS_OPT_TIMELAPSE_TIMERCAPTURECNT,
	RS_OPT_PCSET1_PICTUREPATH,
	RS_OPT_PCSET1_BANKPATH,
	RS_OPT_PCSET2_PREFIX,
	RS_OPT_PCSET2_FILENAME_TYPE,
	RS_OPT_PCSET2_CHECKVIEW,
	RS_OPT_PCSET2_SELECTVIEW,
	RS_OPT_PCSET2_SELECTVIEWSIZE,
};

enum RS_OPT_FILENAME_TYPE
{
	RS_OPT_FILENAME_TYPE_PREFIX		= 0,
	RS_OPT_FILENAME_TYPE_DATE		, 
	RS_OPT_FILENAME_TYPE_CAMERANAME	,
};

//-- ASCII CODE
enum RS_ASCII
{
	RS_INDICATOR_ASCII_QUOTE	= 34,
	RS_INDICATOR_ASCII_DOT		= 46,
	RS_INDICATOR_ASCII_SLASH	= 47,
	RS_INDICATOR_ASCII_0		= 48,
	RS_INDICATOR_ASCII_1		= 49,
	RS_INDICATOR_ASCII_2		= 50,
	RS_INDICATOR_ASCII_3		= 51,
	RS_INDICATOR_ASCII_4		= 52,
	RS_INDICATOR_ASCII_5		= 53,
	RS_INDICATOR_ASCII_6		= 54,
	RS_INDICATOR_ASCII_7		= 55,
	RS_INDICATOR_ASCII_8		= 56,
	RS_INDICATOR_ASCII_9		= 57,
	RS_INDICATOR_ASCII_A		= 65,
	RS_INDICATOR_ASCII_B		= 66,
	RS_INDICATOR_ASCII_C		= 67,
	RS_INDICATOR_ASCII_D		= 68,
	RS_INDICATOR_ASCII_E		= 69,
	RS_INDICATOR_ASCII_F		= 70,
	RS_INDICATOR_ASCII_G		= 71,
	RS_INDICATOR_ASCII_H		= 72,
	RS_INDICATOR_ASCII_I		= 73,
	RS_INDICATOR_ASCII_J		= 74,
	RS_INDICATOR_ASCII_K		= 75,
	RS_INDICATOR_ASCII_L		= 76,
	RS_INDICATOR_ASCII_M		= 77,
	RS_INDICATOR_ASCII_N		= 78,
	RS_INDICATOR_ASCII_O		= 79,
	RS_INDICATOR_ASCII_P		= 80,
	RS_INDICATOR_ASCII_Q		= 81,
	RS_INDICATOR_ASCII_R		= 82,
	RS_INDICATOR_ASCII_S		= 83,
	RS_INDICATOR_ASCII_T		= 84,
	RS_INDICATOR_ASCII_U		= 85,
	RS_INDICATOR_ASCII_V		= 86,
	RS_INDICATOR_ASCII_W		= 87,
	RS_INDICATOR_ASCII_X		= 88,
	RS_INDICATOR_ASCII_Y		= 89,
	RS_INDICATOR_ASCII_Z		= 90,
	RS_INDICATOR_ASCII_UNDERBAR	= 95,
};

//-- 2011-7-18 Lee JungTaek
//-- Option
enum RS_FILENAME_TYP
{
	RS_FILENAME_PRE_NAME_DATE_TIME,
	RS_FILENAME_DATE_PRE_NAME_TIME,
	RS_FILENAME_NAME_PRE_DATE_TIME,
};

enum RS_PROP_DETAIL_TYPE
{
	PROP_DETAIL_TYPE_NULL = -1,
	PROP_DETAIL_TYPE_WB,
	PROP_DETAIL_TYPE_PW,
	PROP_DETAIL_TYPE_DRIVE,
	PROP_DETAIL_TYPE_FLASH,
	PROP_DETAIL_TYPE_METERING,
	PROP_DETAIL_TYPE_AF_MODE,
	PROP_DETAIL_TYPE_AF_AREA,
	PROP_DETAIL_TYPE_PHOTO_SIZE,
	PROP_DETAIL_TYPE_QUALITY,
	PROP_DETAIL_TYPE_SF,
	PROP_DETAIL_TYPE_OIS,
	PROP_DETAIL_TYPE_ETC,
};

enum RS_QUALITY_TYPE
{
	RS_QUALITY_TYPE_NULL	= -1,
	RS_QUALITY_TYPE_JPG,
	RS_QUALITY_TYPE_RAW,
	RS_QUALITY_TYPE_JPGRAW,
};

///////////////////////////////////////////////////////
///					Structure						///
///////////////////////////////////////////////////////

typedef struct tagLISTEVENTMSG {
	UINT	message;
	UINT	nParam1;
	UINT	nParam2;
	UINT	nParam3;
	LPARAM	pParam;	
	LPARAM	pDest;
	LPARAM	pParent;
} RSEvent;

typedef struct tagCONTROLTYPEINFO {
	int nType;
	CObject * pRSCtrl;
} ControlTypeInfo;

//-- 2011-6-16 Lee JungTaek
//-- Option Dialog Structure
struct RSTimeLapse	
{
	BOOL	bDelaySet;
	int		nDelayMin;
	int		nDelaySec;
	BOOL	bTimerCaptureSet;
	int		nTimerMin;
	int		nTimerSec;
	int		nCaptureCount;
	int		nDeleyTime;
	int		nTimerTime;
	int nIntervalCapture_Hour;
	int nIntervalCapture_Minute;
	int nIntervalCapture_Second;
	int nIntervalCapture_Count;	
	int nIntervalCapture_Start_Hour;
	int nIntervalCapture_Start_Minute;
	int nIntervalCapture_Start_AMPM;
	int nIntervalCapture_Time_Lapse;
	BOOL bStartTime;
};

struct RSCustom						
{									
	int nISOStep; 
	//CMiLRe 20140902 ISO Expansion 추가
	int nISOExpansion;
	int nAutoISORange;
	int nLDC;
	int nAFLamp;
	//dh0.seo 2014-09-15
	int nAutoDisplay;
};

struct RSDSCSet1	
{
	int nFileName	;
	int nFileNumber	;
	int nFolderType	;
	//dh0.seo 2014-09-12
	int nPowerSave	;
};
struct RSDSCSet2	
{
	CString strTimeZone;
	CString	strDateTime;
	CString	strType;
	int		nDST;	
	int		nHourType;
	int		nImprint;
	int		nQuickView;
};
struct RSDSCSet3	
{
	int nSystemVol	;
	int nAFSound	;
	int nBtnSound	;
	//CMiLRe 20140901 E Shutter Sound 추가
	int nESHSound;
	//CMiLRe 20140910 Movie Voice&Size 추가
	int nVoiceVolume;
	int nMovieSize;
};
struct RSPCSet1
{
	CString strPictureRootPath;
	CString strBankPath;
};
struct RSPCSet2	
{
	CString strPrefix;
	int nFileNameType;
	int nCheckView;
	int nSelectView;
	int nSelectViewSize;
};
struct RSAbout
{

};
//CMiLRe 20140918 OPTION Tab 추가 & VScroll 추가
struct RSDSCSetEtc	
{
	int nOLED_Color;
	int nMF_Assist;
	int nFocus_Peaking_Level;
	int nFocus_Peaking_Color;
	int nBrightness_Adjust_Guide;
	int nFraming_Mode;
	int nDynamic_Range;
	int nLink_Ae_to_Af;
	int nOver_Exposure;
	//CMiLRe 20140918 Picture Setting 추가
	int nFlashUse_Wireless;
	int nFlash_Channel;
	int nGroupFlashGroup_A_Attl;
	int nGroupFlashGroup_A_Manual;
	int nGroupFlashGroup_A;
	int nGroupFlashGroup_B_Attl;
	int nGroupFlashGroup_B_Manual;
	int nGroupFlashGroup_B;
	int nGroupFlashGroup_C_Attl;
	int nGroupFlashGroup_C_Manual;
	int nGroupFlashGroup_C;

	int nExtFlashUse_Wireless;
	int nExtFlash_Channel;
	int nExtGroupFlashGroup_A_Attl;
	int nExtGroupFlashGroup_A_Manual;
	int nExtGroupFlashGroup_A;
	int nExtGroupFlashGroup_B_Attl;
	int nExtGroupFlashGroup_B_Manual;
	int nExtGroupFlashGroup_B;
	int nExtGroupFlashGroup_C_Attl;
	int nExtGroupFlashGroup_C_Manual;
	int nExtGroupFlashGroup_C; 

	//CMiLRe 20140918 Movie Setting 추가
	int nMov_Qualty;
	int nMov_Multi_Motion;
	int nMov_Fader;
	int nWind_Cut;
	int nMic_Manual_Value;
	int nMovie_Smart_Range;
	//CMiLRe 20140929 Custom 추가
	int nDispBrightLevel;
	int nDispAutoBright;
	int nDispColorDetail_XY;
	int nVideoOut;
	int nAnynet;
	int nHdmi;
	int n3D_Hdmi;
	int nLanguage;
	int nMode_Help_Guide;
	int nFunction_Help_Guide;
	int nTouch_Operation;
	int nAF_Release_Proprity;
	int nEVF_Button_Interaction;
	int nGrid_Line;
	int nNoise_High_ISO;
	int nNoise_Long_Term;
	int nBluetooth;
	int nBluetooth_Auto_Time;
	int nMySmartphone;
	int nWifi_Privacy_lock;
	int nBual_Band_Mobile_AP;
	int nSensor_Cleaning_Start_Up;
	int nSensor_Cleaning_Shut_Down;
	int nColor_Space;
	int nDistortion_Correct;
	int nWifi_Sending_Size;
	int nEfs;
	//CMiLRe 20141001 Setting Battery Selection 추가
	int nBatterySelection;
	//CMiLRe 20141021 Setting Battery Selection 추가 - Internal, External Battry Level 추가
	int nBatteryLevel;
	int nBatteryLevelExt;
	//CMiLRe 20141008 Bracket Settings (AE,WB)
	int nDispUserIcon;
	int nDispUserDateNTime;
	int nDispUserButtons;
	int nI_FN_FNO;
	int nI_FN_Shutter_Sheed;
	int nI_FN_EV;
	int nI_FN_ISO;
	int nI_FN_WB;
	int nI_FN_I_Zoom;
	//dh0.seo 2014-10-02 Key Mapping
	int nKeyMappingPreview;
	int nKeyMappingAEL;
	int nKeyMappingAFON;
	int nKeyMappingCustom1;
	int nKeyMappingCustom2;
	int nKeyMappingCustom3;
	int nKeyMappingWheel;
	int nKeyMappingDial;
	//CMiLRe 20141030 DMF 추가
	int nDMF;
	int nMinShutter;
	int nDepthBrk;
	int nMarkerDis;
	int nMarkerSize;
	int nMarkerTran;
	int nTouchAF;
public:
};
//-- 2011-9-16 Lee JungTaek
//-- Exif Info
typedef struct _ImageFileInfo
{
	CString strFileName;
	CString strTypeComment;

	CString strEquipMake;
	CString strEquipModel;
	CString	strLensType;
	CString strXResolution;			
	CString strYResolution;
	CString strResolutionUnit;

	CString strDate;
	CString strTime;
	CString strFNumber;
	CString strFocalLength;
	CString strISOSpeed;
	CString strExposureBias;
	CString strMaxAperture;
	CString strFlash;
	CString strMeteringMode;
	CString strExposureProg;


	CString strJPEGQuality;
	CString strEXIFVersion;
	CString strShutterSpeed;
	CString strEXIFAperture;
	CString strBrightness;
	CString strFlashpix;		
	CString strColorSpace;	
	CString strCustomRendered;		
	CString strExposureMode;		
	CString strSceneCaptureType;
	CString strSFR;

	//-- 2010-7-13 keunbae.song
	//-- modify
	//int		nWidth;
	//int		nHeight;
	CString strWidth;
	CString strHeight;

} ImageFileInfo;


typedef enum
{
	/*   0 */ MSG_INVALID = 0,
	/*   0 */ MSG_CATURE_PROCESS = 0,
	/*   1 */ MSG_CARD_ERROR,
	/*   2 */ MSG_NO_CARD,
	/*   3 */ MSG_FILE_ERROR,
	/*   4 */ MSG_DCF_FULL_ERROR,
	/*   5 */ MSG_MEMORY_FULL,
	/*   6 */ MSG_LOW_BATTERY,
	/*   7 */ MSG_CARD_LOCKED,
	/*   8 */ MSG_FORMATTING,
	/*   9 */ MSG_FORMAT_COMPLETED,
	/*  10 */ MSG_FORMAT_ERROR,
	/*  11 */ MSG_RESETTING,
	/*  12 */ MSG_RESET_COMPLETED,
	/*  13 */ MSG_TURN_OFF_AND_ON_AGAIN,
	/*  14 */ MSG_PROCESSING,
	/*  15 */ MSG_BLINK_SHOT,
	/*  16 */ MSG_OUT_OF_NUMBER,
	/*  17 */ MSG_NOW_PRINTING,
	/*  18 */ MSG_PRINTING_COMPLETED,
	/*  19 */ MSG_WARNING,
	/*  20 */ MSG_ERROR,
	/*  21 */ MSG_PAPER_ERROR,
	/*  22 */ MSG_CHECK_PAPER,
	/*  23 */ MSG_INK_ERROR,
	/*  24 */ MSG_CHECK_INK,
	/*  25 */ MSG_CANCELING,
	/*  26 */ MSG_OPERATION_ERROR,
	/*  27 */ MSG_CONNECTING_USB,
	/*  28 */ MSG_CONNECTING_COMPUTER,
	/*  29 */ MSG_CONNECTING_PRINTER,
	/*  30 */ MSG_CONNECTING_ERROR,
	/*  31 */ MSG_CONNECTED_COMPUTER,
	/*  32 */ MSG_CONNECTED_PRINTER,
	/*  33 */ MSG_NOT_SUPPORTED,
	/*  34 */ MSG_CAPTURING,
	/*  35 */ MSG_TRIMMING,
	/*  36 */ MSG_PROTECTED_FILE,
	/*  37 */ MSG_NO_IMAGE,
	/*  38 */ MSG_DELETING,
	/*  39 */ MSG_MAKING_NEW_IMAGE,
	/*  40 */ MSG_FILTERING,
	/*  41 */ MSG_PROTECTING,
	/*  42 */ MSG_DELETE_COMPLETED,
	/*  43 */ MSG_IMAGE_ADJUSTING,
	/*  44 */ MSG_NO_RESTORATION_FILE,
	/*  45 */ MSG_RESTORATION_ON,
	/*  46 */ MSG_COPYING,
	/*  47 */ MSG_RESTORING_PICTURES,
	/*  48 */ MSG_COPY_COMPLETED,
	/*  49 */ MSG_COMPLETED,
	/*  50 */ MSG_SMART_ALBUM_INSUFFICIENT_MEM,
	/*  51 */ MSG_SMART_ALBUM_CARD_ERROR,
	/*  52 */ MSG_SMART_ALBUM_CARD_ERROR_1,
	/*  53 */ MSG_ROTATE_THE_LENS_COVER,
	/*  54 */ MSG_FR_SELECT_A_FACE_TO_REGISTER,
	/*  55 */ MSG_FR_INVALID,
	/*  56 */ MSG_FR_REGISTERED,
	/*  57 */ MSG_FR_ANGLE,
	/*  58 */ MSG_FR_RECOGNITION,
	/*  59 */ MSG_USER_REGISTRATION,
	/*  60 */ MSG_SMART_ALBUM_CREAT_ERROR,
	/*  61 */ MSG_FR_FACE_NOT_DETECTED,
	/*  62 */ MSG_SAMPLE_CAMERA,
	/*  63 */ MSG_WARNING_EXPOSURE,
	/*  64 */ MSG_NO_FR_MY_STAR,
	/*  65 */ MSG_LOCK_UNLOCKING,
	/*  66 */ MSG_NOT_RED_EYE_FIX,
	/*  67 */ MSG_MMC_CARD_ERR,
	/*  68 */ MSG_NOT_SUPPORT_SYSTEM,
	/*  69 */ MSG_NOT_SUPPORT_DOWN,
	/*  70 */ MSG_TOUCH_INFO_GUIDE,
	/*  71 */ MSG_CALIBRATION_COMPLETE,
	/*  72 */ MSG_CALIBRATION_EXIT,
	/*  73 */ MSG_SHOOTING_PREVIOUS_CWB_SET,
	/*  74 */ MSG_SHOOTING_MEASURE_WB,
	/*  75 */ MSG_SHOOTING_NEW_CWB_SET,
	/*  76 */ MSG_NOT_SUPPORT_THIS_MODE,
	/*  77 */ MSG_CONVERTER_LENS_ON,
	/*  78 */ MSG_OPEN_THE_LENS_CAP,
	/*  79 */ MSG_REMOVE_CONVERTER_LENS,
	/*  80 */ MSG_ZOOM_ERROR,
	/*  81 */ MSG_CANCEL_SAVE,
	/*  82 */ MSG_NOT_ENOUGH_BATTERY,
	/*  83 */ MSG_BATTERY_REPLACE_BATTERY,
	/*  84 */ MSG_NOT_SMART_RANGE,
	/*  85 */ MSG_PANORAMA_NG_CAPTURE,		/* Panorama Shot 촬영 속도가 빠르거나 화면의 물체가 가까워서 움직임을 감지하기 어렵습니다. 천천히 촬영해 주시기 바랍니다 */
	/*  86 */ MSG_PANORAMA_NG_DIRECTION,	/* Panorama Shot 촬영 방향이 변경되어 촬영을 종료합니다 */
	/*  87 */ MSG_PANORAMA_NG_MOVEMENT,		/* Panoarama NG No Movement */
	/*  88 */ MSG_PANORAMA_NOT_SUPPORT_DIRECTION,	 /* Panorama 3D Mode - Vertical Capture Not Support */
	/*  89 */ MSG_NO_FILE,
	/*  90 */ MSG_LENS_DETACHED,
	/*  91 */ MSG_LENS_ATTACHED,
	/*  92 */ MSG_LENS_FOCUS_LOCKED,
	/*  93 */ MSG_ERROR_00,
	/*  94 */ MSG_ERROR_01,
	/*  95 */ MSG_ERROR_02,
	/*  96 */ MSG_ERROR_CIS_TEMPERATURE,
	/*  97 */ MSG_CHECK_THE_LENS,
	/*  98 */ MSG_NO_LENS,
	/*  99 */ MSG_PW_BRACKET_ERROR,
	/* 100 */ MSG_IFN_CUSTOM_ERROR,
	/* 101 */ MSG_WRONG_FIRMWARE,
	/* 102 */ MSG_FIRMWAREUPDATING,
	/* 103 */ MSG_FIRMWARE_COMPLETED,
	/* 104 */ MSG_RVF_END,  //"종료중 입니다."
	/* 105 */ MSG_NO_TRANSFER_SHARE_WIFI,
	/* 106 */ MSG_WIFI_SHARE_FILE_COUNT_OVER,
	/* 107 */ MSG_WIFI_SHARE_FILE_SIZE_OVER,
	/* 108 */ MSG_WIFI_SHARE_ONLY_PHOTO,
	/* 109 */ MSG_WIFI_SHARE_ONLY_VIDEO,
	/* 110 */ MSG_WIFI_SHARE_ONLY_VIDEO_320X240,
	/* 111 */ MSG_WIFI_SHARE_NOT_SUPPORT,
	/* 112 */ MSG_WIFI_SHARE_FULL_TRANS_BUFFER,
	/* 113 */ MSG_WIFI_RUNNGING,
	/* 114 */ MSG_CHECK_CRC,
	/* 115 */ MSG_CHECK_CRC_COMPLETE,
	/* 116 */ MSG_CHECK_CRC_FAILED,
	/* 117 */ MSG_MEMORY_LOW,
	/* 118 */ MSG_ACCELERATION_ADJUST_COMPLETE,
	/* 119 */ MSG_CHECK_FW_UPDATING,
	/* 120 */ MSG_CES_DEMO_VERSION,
	/* 121 */ MSG_CANCLE_SAVE,	//STR_MSG_CANCLE_SAVE : Recording a Movie is not available because the memory card is too slow
	/* 122 */ MSG_WIFI_SHARE_FILE_COUNT_OVER_WEIBO,
	/* 123 */ MSG_WIFI_AUTOSHARE_TRANSMITION_WAIT,
	/* 124 */ MSG_BODY_FIRMWARE_UPDATE_COMPLETE,
	/* 125 */ MSG_LOCKING,
	/* 126 */ MSG_UNLOCKING,
	/* 127 */ MSG_3D_REC_LOW_LIGHT_WARNING,
	/* 128 */ MSG_CARD_NOT_SUPPORTED,
	/* 129 */ MSG_ERROR_81,
	/* 130 */ MSG_ERROR_91,
	/* 131 */ MSG_USB_ERROR,
	/* 132 */ MSG_RESETTING_CUSTOM_MODE0,
	/* 133 */ MSG_RESETTING_CUSTOM_MODE1,
	/* 134 */ MSG_RESETTING_CUSTOM_MODE2,
	/* 135 */ MSG_RESETTING_CUSTOM_MODE3,
	/* 136 */ MSG_RESETTING_CUSTOM_MODE4,
	/* 137 */ MSG_RESETTING_CUSTOM_MODE5,
	/* 138 */ MSG_RESETTING_CUSTOM_MODE6,
	/* 139 */ MSG_RESETTING_CUSTOM_MODE7,
	/* 140 */ MSG_RESETTING_CUSTOM_MODE8,
	/* 141 */ MSG_RESETTING_CUSTOM_MODE9,
	/* 142 */ MSG_RESET_CUSTOM_MODE0_COMPLETED,
	/* 143 */ MSG_RESET_CUSTOM_MODE1_COMPLETED,
	/* 144 */ MSG_RESET_CUSTOM_MODE2_COMPLETED,
	/* 145 */ MSG_RESET_CUSTOM_MODE3_COMPLETED,
	/* 146 */ MSG_RESET_CUSTOM_MODE4_COMPLETED,
	/* 147 */ MSG_RESET_CUSTOM_MODE5_COMPLETED,
	/* 148 */ MSG_RESET_CUSTOM_MODE6_COMPLETED,
	/* 149 */ MSG_RESET_CUSTOM_MODE7_COMPLETED,
	/* 150 */ MSG_RESET_CUSTOM_MODE8_COMPLETED,
	/* 151 */ MSG_RESET_CUSTOM_MODE9_COMPLETED,
	/* 152 */ MSG_ALREADY_RUNNING,
	/* 153 */ MSG_NO_CARD_AUTO_BACKUP,
	/* 154 */ MSG_CRITICALLY_LOW_BATTERY,
	/* 155 */ MSG_AUTOBACKUP_NO_CARD,
	/* 156 */ MSG_CUSTOM_EVF_KEYBOARD_WARNING,  /* evf로는 keyboard 사용을 할수 없음 경고 */
	/* 157 */ MSG_CUSTOM_SAVE_COMPLETE,
	/* 158 */ MSG_CUSTOM_SAVE_LIST_FULL,
	/* 159 */ MSG_CUSTOM_SAVE_FAIL_SAME_NAME,
	/* 160 */ MSG_CUSTOM_DELETE_LIST_DELETING,
	/* 161 */ MSG_CUSTOM_DELETE_LIST_COMPLETE,
	/* 162 */ MSG_CUSTOM_INPUTTEXT_FULL,
	/* 163 */ MSG_HDMI_ENABLE_SIZE_1080,
	/* 164 */ MSG_EVF_DISABLE,					/* 해당 기능은 evf를 사용할 수 없습니다. */
	/* 165 */ MSG_NW_BM_MSG_CONNECTED_AP,
	/* 166 */ MSG_WIFI_EXIT,
	/* 167 */ MSG_FIRMWARE_UPDATE_WARNING,		/* 펌웨어 업데이트는 배터리 잔량이 70% 이상일 경우 진행 가능합니다. */
	/* 168 */ MSG_SENDING_FILE,                 /* 파일 전송 중입니다. */
	/* 169 */ MSG_UPLOADED_PHOTO,               /* Photo has already been sent. */
	/* 170 */ MSG_UPLOADED_VIDEO,               /* Video has already been sent. */
	/* 171 */ MSG_NEED_FW_UPGRADE_FOR_FLASH,    /* flahs 기능을 사용하기 위해 최신 fw로 update바랍니다.. */
	/* 172 */ MSG_WIFI_SHARE_ERROR_FILE,
	/* 173 */ MSG_NOT_OPERATE_VIEWFINDER,
	/* 174 */ MSG_CONVERTING,   /* convert to jpg msg*/
	/* 175 */ MSG_TIMELAPS_COUNT_LIMIT,   /* MAX 600장 초과시 */
	/* 176 */ MSG_MOBILE_WAITING,	/* 잠시만 기다려 주세요 */
	/* 177 */ MSG_STOP_INTERVAL_CAPTURE, /* 인터벌 촬영 종료시 */
	/* 178 */ MSG_FW_READY,
	/* 179 */ MSG_NFC_TAG_WAITING, /* 스마트폰과 연결 중입니다. 다양한 기능을 사용해 보세요.*/
	/* 180 */ MSG_NOT_SUPPORT_Q_TRANSFER_BY_QUALITY, /* Quick Transfer를 지원하지 않는 형식의 파일입니다. 이 기능을 사용 하시려면, 파일 형식을 변경 하셔야 합니다 */
	/* 181 */ MSG_NFC_OTHER_PHONE_ADD, /* 이미 등록된 스마트폰이 있습니다. 미등록된 스마트 폰을 연결하려면 메뉴 > '설정' > '내 스마트폰' 에서 스마트폰을 등록하세요. */
	/* 182 */ MSG_HIGH_VOLUME_GUIDE, /* 동영상 촬영 또는 movie stby시 팝업 */
	/* 183 */ MSG_MOBILE_BT_RECONNECT, /* 블루 투스 연결이 근겻습니다. 게속 진행하시려면 다시 블루투스를 연결하세요. */
	/* 184 */ MSG_TIME_LAPSE_LIMIT_COUNT_TIME, /* "The interval count and time have been changed to use the Time Lapse feature." */
	/* 185 */ MSG_TIME_LAPSE_LIMIT_TIME, /* "The interval time has been changed to use the Time Lapse feature." */
	/* 186 */ MSG_NO_CARD_I_LAUNCHER,

	NUM_OF_MSG,
	MSG_ERASE,						/* message erase */
}MSG_TYPE;