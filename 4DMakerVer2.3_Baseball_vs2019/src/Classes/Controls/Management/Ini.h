/////////////////////////////////////////////////////////////////////////////
//
// Ini.h: implementation of the CIni class.
//
//
// Copyright (c) 2003 Samsung SDS, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of Samsung SDS, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2003-07-28
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(__INI_H_)
#define __INI_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// Ini-file wrapper class
class CIni : public CObject  
{
public:
   CIni();
   CIni( LPCTSTR IniFilename );
   virtual ~CIni();

// Methods
public:
   // Sets the current Ini-file to use.
   BOOL SetIniFilename(LPCTSTR IniFilename, BOOL bReset = FALSE);
   //
   // Reads an integer from the ini-file.
   UINT GetInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault=0);
   //
   UINT GetIntValuetoKey(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault=0);
   // Reads a boolean value from the ini-file.
   BOOL GetBoolean(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bDefault=FALSE);
   // Reads a string from the ini-file.
   CString GetString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszDefault=NULL);
   //
   CString GetStringValuetoKey(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszDefault=NULL);
   //
   CString GetStringSection(LPCTSTR lpszSection);
   //BY_0625
   UINT GetInt3(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault=0);
   BOOL WriteInt3(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nValue);

   // Writes an integer to the ini-file.
   BOOL WriteInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nValue);
   // Writes a boolean value to the ini-file.
   BOOL WriteBoolean(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bValue);

   //-- 2009-03-31
   //-- For NX Project
   BOOL WriteTrueFalse(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bValue);

   // Writes a string to the ini-file.
   BOOL WriteString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszValue);
   
   // Removes an item from the current ini-file.
   BOOL DeleteKey(LPCTSTR lpszSection, LPCTSTR lpszEntry);
   // Removes a complete section from the ini-file.
   BOOL DeleteSection(LPCTSTR lpszSection);

// Variables
protected:
   CString m_IniFilename; // The current ini-file used.
};

#endif // !defined(__INI_H_)
