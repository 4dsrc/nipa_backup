/////////////////////////////////////////////////////////////////////////////
//
//	TGOption.cpp: main header file for the TestGuarantee application
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2009-02-01
//
/////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "RSOption.h"
#include "Ini.h"
#include "NXRemoteStudioFunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//==============================================================================
// Constructor/Destructor
//==============================================================================

CRSOption::CRSOption(){}

CRSOption::~CRSOption(){}

//==============================================================================
// Public
//==============================================================================

//------------------------------------------------------------------------------ 
//! @brief		Load
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CRSOption::Load(CString strFile)
{
	CIni ini;
	//-- Load Config File
	if(!ini.SetIniFilename (strFile))
		return;
	
	BOOL bDelaySet, bTimerCaptureSet;
	int	nDelayMin, nDelaySec, nTimerMin, nTimerSec, nCaptureCount;	

	bDelaySet			= ini.GetBoolean(INFO_OPT_TIMELAPSE, INFO_TIMELAPSE_DELAYENABLE	);
	nDelayMin			= ini.GetInt(INFO_OPT_TIMELAPSE, INFO_TIMELAPSE_DELAYMIN		);
	nDelaySec			= ini.GetInt(INFO_OPT_TIMELAPSE, INFO_TIMELAPSE_DELAYSEC		);
	bTimerCaptureSet	= ini.GetBoolean(INFO_OPT_TIMELAPSE, INFO_TIMELAPSE_TIMERENABLE	);
	nTimerMin			= ini.GetInt(INFO_OPT_TIMELAPSE, INFO_TIMELAPSE_TIMERMIN		);
	nTimerSec			= ini.GetInt(INFO_OPT_TIMELAPSE, INFO_TIMELAPSE_TIMERSEC	);
	nCaptureCount		= ini.GetInt(INFO_OPT_TIMELAPSE, INFO_TIMELAPSE_TIMERCOUNT	);

	LoadTimeLapse(bDelaySet, nDelayMin, nDelaySec, bTimerCaptureSet, nTimerMin, nTimerSec, nCaptureCount);

	//-- PC Set1
	CString strPictureRootPath, strBankPath;
	CString strInfo;

	if(IsWow64())
		strInfo = INFO_OPT_PCSET1_64;
	else
		strInfo = INFO_OPT_PCSET1_32;

	strPictureRootPath	= ini.GetString(strInfo, INFO_PCSET1_PICTUREPATH	);

	if(!strPictureRootPath.GetLength())
		strPictureRootPath.Format(_T("%s\\picture"),RSGetRoot());
	strBankPath			= ini.GetString(strInfo, INFO_PCSET1_BANKPATH		);
	if(!strBankPath.GetLength())
		strBankPath.Format(_T("%s\\config\\bank"),RSGetRoot());

	LoadPCSet1(strPictureRootPath, strBankPath);

	//-- PC Set2
	CString strPrefix;
	int nFileNameType;
	int nCheckView;
	int nSelectView;
	int nSelectViewSize;

	strPrefix = ini.GetString(INFO_OPT_PCSET2	,  INFO_PCSET2_PREFIX		);
	nFileNameType = ini.GetInt(INFO_OPT_PCSET2	,  INFO_PCSET2_FILENAME_TYPE);
	nCheckView = ini.GetInt(INFO_OPT_PCSET2	,  INFO_PCSET2_CHECKVIEW);
	nSelectView = ini.GetInt(INFO_OPT_PCSET2	,  INFO_PCSET2_SELECTVIEW);
	nSelectViewSize = ini.GetInt(INFO_OPT_PCSET2	,  INFO_PCSET2_SELECTVIEWSIZE);

	LoadPCSet2(strPrefix, nFileNameType, nCheckView, nSelectView, nSelectViewSize);

	//-- Create Directory
	CreateDir();
}

//------------------------------------------------------------------------------ 
//! @brief		LoadOption
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CRSOption::LoadTimeLapse(BOOL bDelaySet, int nDelayMin, int nDelaySec, BOOL bTimerCaptureSet, int nTimerMin, int nTimerSec, int nCaptureCount)
{
	m_TimeLapse.bDelaySet			=	bDelaySet		;
	m_TimeLapse.bTimerCaptureSet	=	bTimerCaptureSet;
	m_TimeLapse.nDelayMin			=	nDelayMin		;
	m_TimeLapse.nDelaySec			=	nDelaySec		;
	m_TimeLapse.nTimerMin			=	nTimerMin		;
	m_TimeLapse.nTimerSec			=	nTimerSec		;
	m_TimeLapse.nCaptureCount		=	nCaptureCount	;
}

//------------------------------------------------------------------------------ 
//! @brief		LoadPCSet1
//! @date		2011-6-30
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSOption::LoadPCSet1(CString strPictureRootPath, CString strBankPath)
{
	m_PCSet1.strPictureRootPath	= strPictureRootPath;
	m_PCSet1.strBankPath		= strBankPath;
}

//------------------------------------------------------------------------------ 
//! @brief		LoadPCSet2
//! @date		2011-6-30
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSOption::LoadPCSet2(CString strPrefix, int nFileNameType, int nCheckView, int nSelectView, int nSelectViewSize)
{
	m_PCSet2.strPrefix	=	strPrefix;
	m_PCSet2.nFileNameType	=	nFileNameType;
	m_PCSet2.nCheckView	=	nCheckView;
	m_PCSet2.nSelectView	=	nSelectView;
	m_PCSet2.nSelectViewSize	=	nSelectViewSize;
}

//------------------------------------------------------------------------------ 
//! @brief		CreateDir
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------
void CRSOption::CreateDir()
{
#ifdef	IFA
	CreateAllDirectories(RSGetValueStr(0));
#else
	CreateAllDirectories(RSGetValueStr(RS_OPT_PCSET1_PICTUREPATH));
#endif
	CreateAllDirectories(RSGetValueStr(RS_OPT_PCSET1_BANKPATH));
}