/////////////////////////////////////////////////////////////////////////////
//
// Ini.cpp: implementation of the CIni class.
//
//
// Copyright (c) 2003 Samsung SDS, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of Samsung SDS, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2003-07-28
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Ini.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIni::CIni()
{
   m_IniFilename.Empty();
}

CIni::CIni(LPCTSTR IniFilename)
{
   SetIniFilename( IniFilename );
}

CIni::~CIni()
{
   // Flush .ini file
   ::WritePrivateProfileString( NULL, NULL, NULL, m_IniFilename );
}


//////////////////////////////////////////////////////////////////////
// Methods
//////////////////////////////////////////////////////////////////////

//#define MAX_INI_BUFFER 1048576   // Defines the maximum number of chars we can
								// read from the ini file 
#define MAX_INI_BUFFER 256

//--
//-- DELETE PREVIOUS FILE
//-- 2008-07-24
//-- 
#include "FileOperations.h"

BOOL CIni::SetIniFilename(LPCTSTR IniFilename, BOOL bReset)
{
   ASSERT(AfxIsValidString(IniFilename));
   m_IniFilename = IniFilename;
   if( m_IniFilename.IsEmpty() ) 
	   return FALSE;

   //--
   //-- DELETE PREVIOUS FILE
   //-- 2008-07-24
   //-- 
   if(bReset)
   {
	   CFileOperation fo;
	   fo.Delete(IniFilename);
   }

   return TRUE;
};

UINT CIni::GetInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   if( m_IniFilename.IsEmpty() ) return 0; // error
   CString sDefault;
   sDefault.Format( _T("%d"), nDefault );
   CString s = GetString( lpszSection, lpszEntry, sDefault );
   return _ttol( s );
};

UINT CIni::GetIntValuetoKey(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   if( m_IniFilename.IsEmpty() ) return 0; // error
   CString sDefault;
   sDefault.Format( _T("%d"), nDefault );
   CString s = GetStringValuetoKey( lpszSection, lpszEntry, sDefault);
   return _ttol( s );
};


CString CIni::GetString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszDefault)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   if( m_IniFilename.IsEmpty() ) return CString();
   CString s;
   long ret = ::GetPrivateProfileString( lpszSection, lpszEntry, lpszDefault, s.GetBuffer( MAX_INI_BUFFER ), MAX_INI_BUFFER, m_IniFilename );
   s.ReleaseBuffer();
   if( ret==0 ) return CString(lpszDefault);
   return s;
};

CString CIni::GetStringValuetoKey(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszDefault)
{
	int nPos = 0;
	CString strSection;
	CString strToken;
	CString strEntry;
	CString strTemp;

	ASSERT(AfxIsValidString(lpszSection));
	ASSERT(AfxIsValidString(lpszEntry));

	strSection = GetStringSection(lpszSection);

	strEntry = _T("=");
	strEntry.Append(lpszEntry);
	
	while((strToken = strSection.Tokenize(_T("~"),nPos)) != "")
	{
		if(strToken.Find(strEntry) >= 0)
		{
			strToken.Replace(strEntry,_T(""));
			return strToken;
		}
	}
	
	strToken.Empty();
	return CString(lpszDefault);
};

CString CIni::GetStringSection(LPCTSTR lpszSection)
{
	TCHAR buff[10240]={0};
	
	memset(buff, 0x00, 10240);
	ASSERT(AfxIsValidString(lpszSection));
	if( m_IniFilename.IsEmpty() ) return CString();
	CString s;
	CString strTemp;

	long ret = ::GetPrivateProfileSection(lpszSection, (LPTSTR)buff, 10239, m_IniFilename );
	
	if( ret==0 ) return CString(lpszSection);

	// buffer out of access
	//for(int i=0 ; i<10239 ; i++)
	for(int i=0 ; i<10237 ; i++)
	{
		if(buff[i] == NULL){
			s.Append(_T("~"));
		}else{
			strTemp.Format(_T("%c"),buff[i]);
			s.Append(strTemp);
		}

		if((buff[i+1]=='=' && buff[i+2]=='=') || (buff[i+1] == NULL && buff[i+2] == NULL))
		{
			return s;
		}
	}

	return s;
};

BOOL CIni::GetBoolean(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bDefault)
{
   CString s = GetString(lpszSection,lpszEntry);
   if( s.IsEmpty() ) return bDefault;
   TCHAR c = (TCHAR)_totupper( s[0] );
   switch( c ) {
   case _T('Y'): // YES
   case _T('1'): // 1 (binary)
   case _T('O'): // OK
      return TRUE;
   default:
      return FALSE;
   };
};


BOOL CIni::WriteInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nValue)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   CString s;
   s.Format( _T("%d"), nValue );
   return WriteString( lpszSection, lpszEntry, s );
};

BOOL CIni::WriteBoolean(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bValue)
{
   CString s;
   bValue ? s=_T("Y") : s=_T("N");
   return WriteString( lpszSection, lpszEntry, s );
};

BOOL CIni::WriteTrueFalse(LPCTSTR lpszSection, LPCTSTR lpszEntry, BOOL bValue)
{
   CString s;
   bValue ? s=_T("True") : s=_T("False");
   return WriteString( lpszSection, lpszEntry, s );
};

BOOL CIni::WriteString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszValue)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   if( m_IniFilename.IsEmpty() ) 
	   return FALSE;
   return ::WritePrivateProfileString( lpszSection, lpszEntry, lpszValue, m_IniFilename );
};


BOOL CIni::DeleteKey(LPCTSTR lpszSection, LPCTSTR lpszEntry)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   if( m_IniFilename.IsEmpty() ) 
	   return FALSE;
   return ::WritePrivateProfileString( lpszSection, lpszEntry, NULL, m_IniFilename );
};

BOOL CIni::DeleteSection(LPCTSTR lpszSection)
{
   ASSERT(AfxIsValidString(lpszSection));
   if( m_IniFilename.IsEmpty() ) return FALSE;
   return ::WritePrivateProfileString( lpszSection, NULL, NULL, m_IniFilename );
};

