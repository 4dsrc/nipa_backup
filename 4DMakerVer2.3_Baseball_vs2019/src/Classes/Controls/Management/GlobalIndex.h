/////////////////////////////////////////////////////////////////////////////
//
//  ptpIndex.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-09
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ptpIndex.h"

///////////////////////////////////////////////////////
///					Define							///
///////////////////////////////////////////////////////
#define ARR_SIZE(A)	(sizeof(A) / sizeof(A[0]))
#define MAGNET_ATTACH_RANGE 50

#define MAIN_WND_WIDTH		304
#define MAIN_WND_HEIGHT		219 + 31
#define SMART_PANEL_HEIGHT	251
#define LIVEVIEW_DLG_WIDTH	660
#define LIVEVIEW_DLG_HEIGHT	660
#define OPTION_DLG_WIDTH		500
#define OPTION_DLG_HEIGHT		250
#define OPTION_TAB_HEIGHT		36

#define SRC_WIDTH				640	//1024
#define SRC_HEIGHT				424
#define CLIPING(data)  ( (data) < 0 ? 0 : ( (data) > 255 ) ? 255 : ( data ) )
#define SELECT_NOTHING			-1
#define MAIN_LIVEVIEW			-1
#define MULTILIVEVIEW_CNT		4

///////////////////////////////////////////////////////
///					Define - ini Info				///
///////////////////////////////////////////////////////
//-- SmartPanel
#define INFO_LAYOUT_TYPE	_T("Type")
#define INFO_LAYOUT_X		_T("PosX")
#define INFO_LAYOUT_Y		_T("PosY")
#define INFO_LAYOUT_W		_T("Width")
#define INFO_LAYOUT_H		_T("Height")
#define INFO_LAYOUT_SEP_V	_T("Vertical")
#define INFO_LAYOUT_SEP_H	_T("Horizon")
#define INFO_LAYOUT_SHOW	_T("Show")

#define INFO_PROPERTY_VALUE_COUNT	_T("ITEM_COUNT")
#define INFO_PROPERTY_VALUE_NAME	_T("VALUE_NAME")
#define INFO_PROPERTY_IMAGE_NORMAL	_T("IMAGE_NORMAL")
#define INFO_PROPERTY_IMAGE_SELECT	_T("IMAGE_SELECT")
#define INFO_PROPERTY_IMAGE_DISABLE	_T("IMAGE_DISABLE")

//-- Registry
#define REG_RS_ROOT				_T("SOFTWARE\\Samsung\\RemoteStudio")
#define REG_RS_POS				_T("position")
#define REG_RS_ROOT_PATH		_T("directory")

#define REG_RS_MAINWIN		_T("MainWindow")
#define REG_RS_LIVEVIEW			_T("Liveview")
#define REG_RS_SMARTPANEL	_T("SmartPanel")
#define REG_RS_PICTUREVIEWER	_T("PictureViewer")
#define REG_RS_ZOOMER			_T("Zoomer")

//-- Option
#define INFO_OPT_TIMELAPSE	_T("TIME LAPSE")
#define INFO_OPT_CUSTOM		_T("CUSTOM")
#define INFO_OPT_DSCSET1	_T("DSC_SET1")
#define INFO_OPT_DSCSET2	_T("DSC_SET2")
#define INFO_OPT_PCSET1		_T("PC_SET1")
#define INFO_OPT_PCSET2		_T("PC_SET2")
#define INFO_OPT_ABOUT		_T("ABOUT")

//TimeLapse
#define INFO_TIMELAPSE_ENABLE		_T("Enable")
#define INFO_TIMELAPSE_HOUR			_T("Hour")
#define INFO_TIMELAPSE_MIN			_T("Min")
#define INFO_TIMELAPSE_SEC			_T("Sec")
#define INFO_TIMELAPSE_CAPTIME		_T("Capture Time")
#define INFO_TIMELAPSE_INTERVAL		_T("Interval")

//PC Set1
#define INFO_PCSET1_PICTUREPATH		_T("Picture")
#define INFO_PCSET1_BANKPATH		_T("Bank")

//PC Set2
#define INFO_PCSET2_PREFIX			_T("Prefix")
#define INFO_PCSET2_FILENAME_TYPE	_T("FileNameType")


#define RGB_DEFAULT_DLG				RGB(58,58,58)
#define RGB_BLACK					RGB(0,0,0)
#define RGB_WHITE					RGB(255,255,255)

///////////////////////////////////////////////////////
///					WM_MESSAGE
///////////////////////////////////////////////////////

#define WM_RS									WM_USER+1000
//-- Multi Control
#define WM_RS_MC								WM_USER+1500
#define WM_RS_MC_OPEN_SESSION					WM_USER+1501
#define WM_RS_MC_CLOSE_SESSION					WM_USER+1502
#define WM_RS_MC_CAPTURE_IMAGE					WM_USER+1504
#define WM_RS_MC_LIVEVIEW_IMAGE_EVENT			WM_USER+1506
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1		WM_USER+1507		
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2		WM_USER+1508		
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_3		WM_USER+1509		
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4		WM_USER+1510		
#define WM_RS_MC_CAPTURE_IMAGE_REQUIRE_ONCE		WM_USER+1511		
//-- Smart Panel
#define WM_RS_SP_SET_PROPERTY_VALUE				WM_USER+1551
#define WM_RS_SP_UPDATE_PROPERTY_VALUE			WM_USER+1552
#define WM_RS_LV_UPDATE_TUMBNAIL				WM_USER+1553
//-- LiveView Dialog
#define WM_RS_LV_CHANGE_DEVICE					WM_USER+1554
#define WM_RS_LV_GET_INFO						WM_USER+1555
//-- Get Property
#define WM_RS_GET_PROPERTY_INFO					WM_USER+1600
#define WM_RS_SET_PROPERTY_INFO					WM_USER+1601
#define WM_RS_UPDATE_PROPERTY_VALUE				WM_USER+1602
#define WM_RS_GET_PROPERTY_CHANGE				WM_USER+1603
//-- 2011-6-16 Lee JungTaek
#define WM_RS_SET_INDICATOR_INTERVAL			WM_USER+1604
#define WM_RS_GET_UNIQUE_ID						WM_USER+1605

#define WM_RS_EVENT_LIVEVIEW_IMAGE				WM_USER+2001
#define WM_RS_MC_LIVEVIEW_INFO					WM_USER+2002
#define WM_LIVEVIEW								WM_USER+3001
#define WM_LIVEVIEW_EVENT_SHIFT					WM_USER+3002
#define WM_LIVEVIEW_SEND_MESSAGE				WM_USER+3003
#define WM_LIVEVIEW_EVENT_FULL_SCREEN			WM_USER+3004
#define WM_LIVEVIEW_EVENT_SEPARATOR				WM_USER+3005
#define WM_LIVEVIEW_GRID_CHANGE					WM_USER+3006
#define WM_LIVEVIEW_ZOOM_WINDOW					WM_USER+3007
//-- 2011-6-24 Lee JungTaek
#define WM_LIVEVIEW_EVENT_SET_FOCUS				WM_USER+3008
//#define WM_LIVEVIEW_EVENT_GET_FOCUS			WM_USER+3009
#define WM_RS_LIVEVIEW_GET_FOCUSPOSITION		WM_USER+3010
#define WM_RS_LIVEVIEW_SET_FOCUSPOSITION		WM_USER+3011
///////////////////////////////////////////////////////
///	-- TransParent Dialog							///
///////////////////////////////////////////////////////
#define	WM_END_RESIZE_EVENT						WM_USER+5010

// chlee 캡쳐명령후 받아오는 이벤트 메시지
#define WM_CAPTURE_RECEIVE					WM_USER+3500
#define TAB_CHANGE								WM_USER+5000


///////////////////////////////////////////////////////
///					Define - String					///
///////////////////////////////////////////////////////
#define STR_CONFIG_PATH					_T("config")
#define STR_CONFIG_LAYOUT_PATH			_T("config\\Layout")
#define STR_IMAGE_COMMON_ICON_PATH		_T("img\\00.Common")
#define STR_IMAGE_LIGHTSTUDIO_ICON_PATH	_T("img\\01.Smart Panel")
#define STR_IMAGE_SMARTPANNEL_ICON_PATH	_T("img\\02.Smart Panel\\List Icon")

///////////////////////////////////////////////////////
///					enum							///
///////////////////////////////////////////////////////
enum MAIN_MAGNET 
{ 
	MAIN_NULL = 0, 
	MAIN_TOP, 
	MAIN_RIGHT, 
	MAIN_LEFT, 
	MAIN_BOTTOM 
};

enum IMAGE_MULTIAPPLY_TYPE
{
	IMAGE_MUTLIAPPY_MAINCAMERA = 0,
	IMAGE_MUTLIAPPY_DISABLE,
	IMAGE_MUTLIAPPY_UNCHECK,
	IMAGE_MUTLIAPPY_CHECK,
};



enum CAMERA_BTN_BK_STATUS
{
	BTN_BK_STATUS_NOIMAGE = -1,
	BTN_BK_1ST_STATUS_NORMAL = 0,
	BTN_BK_1ST_STATUS_FOCUS,
	BTN_BK_1ST_STATUS_PUSH,
	BTN_BK_1ST_STATUS_DISABLE,
	BTN_BK_2ND_STATUS_FOCUS,
};

enum OPTION_TAB
{
	OPT_TAB_NOTHING = -1,
	OPT_TAB_TIMELAPSE	= 0,
	OPT_TAB_CUSTOM		,
	OPT_TAB_DSC_SET1	,
	OPT_TAB_DSC_SET2	,
	OPT_TAB_DSC_SET3	,
	OPT_TAB_PC_SET1		,
	OPT_TAB_PC_SET2		,
	OPT_TAB_ABOUT		,
	OPT_TAB_CNT,
};

// CLiveview
enum SHIFT_COMMAND
{ 
	SHIFT_LEFT	= 0, 
	SHIFT_RIGHT, 
};

// CLiveview
enum SHIFT_POSITION
{ 
	SHIFT_0	= 0, 
	SHIFT_90, 
	SHIFT_180, 
	SHIFT_270, 	
};

enum GRID_OPT
{
	GRID_NULL	= 0,
	GRID_2X2		,
	GRID_3X3		,
	GRID_7X7		,
	GRID_PLUS	,
	GRID_X		,
	GRID_CNT,
};

enum ZOOM_OPT
{
	ZOOM_OFF	= 0,
	ZOOM_1_2	,
	ZOOM_1_3	,
	ZOOM_1_4	,	
	ZOOM_CNT,
};



enum LIST_OPT
{
	CMB_MULTI	= 0,
	CMB_ZOOM	,
	CMB_GRID,
	CMB_CNT,
};



enum CONNECTION_CHANGE
{
	CONNECT_INFO_ERR = 0,
	CONNECT_INFO_ADD,
	CONNECT_INFO_REMOVE,
	CONNECT_INFO_NULL,	
};



enum RS_OPT_PROPERTY
{
	RS_OPT_TIMELAPSE_ENABLE,
	RS_OPT_TIMELAPSE_RESERV_TIME,
	RS_OPT_TIMELAPSE_CAPTURE_TIME,
	RS_OPT_TIMELAPSE_INTERVAL,
	RS_OPT_PCSET1_PICTUREPATH,
	RS_OPT_PCSET1_BANKPATH,
	RS_OPT_PCSET2_PREFIX,
	RS_OPT_PCSET2_FILENAME_TYPE,
};

enum RS_OPT_FILENAME_TYPE
{
	RS_OPT_FILENAME_TYPE_PREFIX		= 0,
	RS_OPT_FILENAME_TYPE_DATE		, 
	RS_OPT_FILENAME_TYPE_CAMERANAME	,
};

//-- ASCII CODE
enum RS_ASCII
{
	RS_INDICATOR_ASCII_DOT		= 46,
	RS_INDICATOR_ASCII_SLASH	= 47,
	RS_INDICATOR_ASCII_0		= 48,
	RS_INDICATOR_ASCII_1		= 49,
	RS_INDICATOR_ASCII_2		= 50,
	RS_INDICATOR_ASCII_3		= 51,
	RS_INDICATOR_ASCII_4		= 52,
	RS_INDICATOR_ASCII_5		= 53,
	RS_INDICATOR_ASCII_6		= 54,
	RS_INDICATOR_ASCII_7		= 55,
	RS_INDICATOR_ASCII_8		= 56,
	RS_INDICATOR_ASCII_9		= 57,
	RS_INDICATOR_ASCII_A		= 65,
	RS_INDICATOR_ASCII_B		= 66,
	RS_INDICATOR_ASCII_C		= 67,
	RS_INDICATOR_ASCII_D		= 68,
	RS_INDICATOR_ASCII_E		= 69,
	RS_INDICATOR_ASCII_F		= 70,
	RS_INDICATOR_ASCII_G		= 71,
	RS_INDICATOR_ASCII_H		= 72,
	RS_INDICATOR_ASCII_I		= 73,
	RS_INDICATOR_ASCII_J		= 74,
	RS_INDICATOR_ASCII_K		= 75,
	RS_INDICATOR_ASCII_L		= 76,
	RS_INDICATOR_ASCII_M		= 77,
	RS_INDICATOR_ASCII_N		= 78,
	RS_INDICATOR_ASCII_O		= 79,
	RS_INDICATOR_ASCII_P		= 80,
	RS_INDICATOR_ASCII_Q		= 81,
	RS_INDICATOR_ASCII_R		= 82,
	RS_INDICATOR_ASCII_S		= 83,
	RS_INDICATOR_ASCII_T		= 84,
	RS_INDICATOR_ASCII_U		= 85,
	RS_INDICATOR_ASCII_V		= 86,
	RS_INDICATOR_ASCII_W		= 87,
	RS_INDICATOR_ASCII_X		= 88,
	RS_INDICATOR_ASCII_Y		= 89,
	RS_INDICATOR_ASCII_Z		= 90,
	RS_INDICATOR_ASCII_UNDERBAR	= 95,
};

///////////////////////////////////////////////////////
///					Structure						///
///////////////////////////////////////////////////////
typedef struct tagLISTEVENTMSG {
	UINT	message;
	UINT	nParam;
	UINT	nParam2;
	LPARAM	pParam;	
	BOOL	bParam;
	DWORD   time;
} RSEvent;

typedef struct tagCONTROLTYPEINFO {
	int nType;
	CObject * pRSCtrl;
} ControlTypeInfo;

//-- 2011-6-16 Lee JungTaek
//-- Option Dialog Structure
struct RSTimeLapse	
{
	BOOL bTimeLapse;
	int nReservHour;
	int nReservMin;
	int nReservSec;
	int nCaptureTime;
	int nInterval;
	int nReservTime;
};

struct RSCustom						
{									
	int nISOStep; 
	int nISOExpansion;
	int nAutoISORange;
	int nLDC;
	int nAFLamp;
};

struct RSDSCSet1	
{
	int nFileName	;
	int nFileNumber	;
	int nFolderType	;
};
struct RSDSCSet2	
{
	CString strTimeZone;
	CString	strDateTime;
	CString	strType;
	int		nDST;	
	int		nHourType;
	int		nImprint;
};
struct RSDSCSet3	
{
	int nSystemVol	;
	int nAFSound	;
	int nBtnSound	;
};
struct RSPCSet1
{
	CString strPictureRootPath;
	CString strBankPath;
};
struct RSPCSet2	
{
	CString strPrefix;
	int nFileNameType;
};
