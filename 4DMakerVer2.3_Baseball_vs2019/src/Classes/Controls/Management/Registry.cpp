/////////////////////////////////////////////////////////////////////////////
//
// Registry.cpp: implementation of the Registry class.
//
//
// Copyright (c) 2003 Samsung SDS, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of Samsung SDS, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2003-11-11
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Registry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRegistry::CRegistry(HKEY hKeyRoot)
{
	m_hKey = hKeyRoot;
}

CRegistry::~CRegistry()
{
	Close();
}


BOOL CRegistry::VerifyKey (HKEY hKeyRoot, LPCTSTR pszPath)
{
	ASSERT (hKeyRoot);
	ASSERT (pszPath);

	LONG ReturnValue = RegOpenKeyEx (hKeyRoot, pszPath, 0L,
		KEY_ALL_ACCESS, &m_hKey);
	if(ReturnValue == ERROR_SUCCESS)
		return TRUE;
	
	//m_Info.lMessage = ReturnValue;
	//m_Info.dwSize = 0L;
	//m_Info.dwType = 0L;

	return FALSE;
}

//BOOL CRegistry::VerifyKey (LPCTSTR pszPath)
//{
//	ASSERT (m_hKey);
//
//	LONG ReturnValue = RegOpenKeyEx (m_hKey, pszPath, 0L,
//		KEY_ALL_ACCESS, &m_hKey);
//	
//	m_Info.lMessage = ReturnValue;
//	m_Info.dwSize = 0L;
//	m_Info.dwType = 0L;
//
//	if(ReturnValue == ERROR_SUCCESS)
//		return TRUE;
//	
//	return FALSE;
//}

//BOOL CRegistry::VerifyValue (LPCTSTR pszValue)
//{
//	ASSERT(m_hKey);
//	LONG lReturn = RegQueryValueEx(m_hKey, pszValue, NULL,
//		NULL, NULL, NULL);
//
//	m_Info.lMessage = lReturn;
//	m_Info.dwSize = 0L;
//	m_Info.dwType = 0L;
//
//	if(lReturn == ERROR_SUCCESS)
//		return TRUE;
//
//	return FALSE;
//}

BOOL CRegistry::CreateKey (HKEY hKeyRoot, LPCTSTR pszPath)
{
	DWORD dw;

	LONG ReturnValue = RegCreateKeyEx (hKeyRoot, pszPath, 0L, NULL,
		REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, 
		&m_hKey, &dw);

	//m_Info.lMessage = ReturnValue;
	//m_Info.dwSize = 0L;
	//m_Info.dwType = 0L;

	if(ReturnValue == ERROR_SUCCESS)
		return TRUE;

	return FALSE;
}

BOOL CRegistry::Open (HKEY hKeyRoot, LPCTSTR pszPath)
{
	//m_sPath = pszPath;

	LONG ReturnValue = RegOpenKeyEx (hKeyRoot, pszPath, 0L,
		KEY_ALL_ACCESS, &m_hKey);

	//m_Info.lMessage = ReturnValue;
	//m_Info.dwSize = 0L;
	//m_Info.dwType = 0L;

	if(ReturnValue == ERROR_SUCCESS)
		return TRUE;

	return FALSE;
}

BOOL CRegistry::Read(LPCTSTR pszKey, int& iVal)
{
	ASSERT(m_hKey);
	ASSERT(pszKey);

	DWORD dwType;
	DWORD dwSize = sizeof (DWORD);
	DWORD dwDest;

	LONG lReturn = RegQueryValueEx (m_hKey, (LPTSTR) pszKey, NULL,
		&dwType, (BYTE *) &dwDest, &dwSize);
		
	if(lReturn == ERROR_SUCCESS)
	{
		iVal = (int)dwDest;
		return TRUE;
	}
	return FALSE;
}

BOOL CRegistry::Read(LPCTSTR pszKey, UINT& iVal)
{
	ASSERT(m_hKey);
	ASSERT(pszKey);

	DWORD dwType;
	DWORD dwSize = sizeof (DWORD);
	DWORD dwDest;

	LONG lReturn = RegQueryValueEx (m_hKey, (LPTSTR) pszKey, NULL,
		&dwType, (BYTE *) &dwDest, &dwSize);

	//m_Info.lMessage = lReturn;
	//m_Info.dwType = dwType;
	//m_Info.dwSize = dwSize;

	if(lReturn == ERROR_SUCCESS)
	{
		iVal = (UINT)dwDest;
		return TRUE;
	}

	return FALSE;
}

void CRegistry::Close()
{
	if (m_hKey)
	{
		RegCloseKey (m_hKey);
		m_hKey = NULL;
	}
}

BOOL CRegistry::Write (LPCTSTR pszKey, int iVal)
{
	DWORD dwValue;

	ASSERT(m_hKey);
	ASSERT(pszKey);
	
	dwValue = (DWORD)iVal;
	LONG ReturnValue = RegSetValueEx (m_hKey, pszKey, 0L, REG_DWORD,
		(CONST BYTE*) &dwValue, sizeof(DWORD));

//	m_Info.lMessage = ReturnValue;
//	m_Info.dwSize = sizeof(DWORD);
//	m_Info.dwType = REG_DWORD;

	if(ReturnValue == ERROR_SUCCESS)
		return TRUE;
	
	return FALSE;
}

BOOL CRegistry::Write (LPCTSTR pszKey, LPCTSTR pszData)
{
	ASSERT(m_hKey);
	ASSERT(pszKey);
	ASSERT(pszData);
	ASSERT(AfxIsValidAddress(pszData, _tcslen(pszData), FALSE));

	LONG ReturnValue = RegSetValueEx (m_hKey, pszKey, 0L, REG_SZ,
		(CONST BYTE*) pszData, (DWORD)_tcslen(pszData)*2);

//	m_Info.lMessage = ReturnValue;
//	m_Info.dwSize = (DWORD)(_tcslen(pszData) + 1);
//	m_Info.dwType = REG_SZ;

	if(ReturnValue == ERROR_SUCCESS)
		return TRUE;
	
	return FALSE;
}

//BOOL CRegistry::Write (LPCTSTR pszKey, DWORD dwVal)
//{
//	ASSERT(m_hKey);
//	ASSERT(pszKey);
//	return RegSetValueEx (m_hKey, pszKey, 0L, REG_DWORD,
//		(CONST BYTE*) &dwVal, sizeof(DWORD));
//}


//BOOL CRegistry::Write (LPCTSTR pszKey, CStringList& scStringList)
//{
//	ASSERT(m_hKey);
//	ASSERT(pszKey);
//	const int iMaxChars = 4096;
//	BYTE* byData = (BYTE*)::calloc(iMaxChars, sizeof(TCHAR));
//	ASSERT(byData);
//
//	CMemFile file(byData, iMaxChars, 16);
//	CArchive ar(&file, CArchive::store);
//	ASSERT(scStringList.IsSerializable());
//	scStringList.Serialize(ar);
//	ar.Close();
//	const DWORD dwLen = (DWORD)file.GetLength();
//	ASSERT(dwLen < iMaxChars);
//	LONG lReturn = RegSetValueEx(m_hKey, pszKey, 0, REG_BINARY,
//		file.Detach(), dwLen);
//	
//	m_Info.lMessage = lReturn;
//	m_Info.dwSize = dwLen;
//	m_Info.dwType = REG_BINARY;
//
//	if(byData)
//	{
//		free(byData);
//		byData = NULL;
//	}
//
//	if(lReturn == ERROR_SUCCESS)
//		return TRUE;
//	
//	return FALSE;
//}

//BOOL CRegistry::Write (LPCTSTR pszKey, CByteArray& bcArray)
//{
//	ASSERT(m_hKey);
//	ASSERT(pszKey);
//	const int iMaxChars = 4096;
//	BYTE* byData = (BYTE*)::calloc(iMaxChars, sizeof(TCHAR));
//	ASSERT(byData);
//
//	CMemFile file(byData, iMaxChars, 16);
//	CArchive ar(&file, CArchive::store);
//	ASSERT(bcArray.IsSerializable());
//	bcArray.Serialize(ar);
//	ar.Close();
//	const DWORD dwLen = (DWORD)file.GetLength();
//	ASSERT(dwLen < iMaxChars);
//	LONG lReturn = RegSetValueEx(m_hKey, pszKey, 0, REG_BINARY,
//		file.Detach(), dwLen);
//	
//	m_Info.lMessage = lReturn;
//	m_Info.dwSize = dwLen;
//	m_Info.dwType = REG_BINARY;
//
//	if(byData)
//	{
//		free(byData);
//		byData = NULL;
//	}
//
//	if(lReturn == ERROR_SUCCESS)
//		return TRUE;
//	
//	return FALSE;
//}

//BOOL CRegistry::Write (LPCTSTR pszKey, CDWordArray& dwcArray)
//{
//	ASSERT(m_hKey);
//	ASSERT(pszKey);
//	const int iMaxChars = 4096;
//	BYTE* byData = (BYTE*)::calloc(iMaxChars, sizeof(TCHAR));
//	ASSERT(byData);
//
//	CMemFile file(byData, iMaxChars, 16);
//	CArchive ar(&file, CArchive::store);
//	ASSERT(dwcArray.IsSerializable());
//	dwcArray.Serialize(ar);
//	ar.Close();
//	const DWORD dwLen = (DWORD)file.GetLength();
//	ASSERT(dwLen < iMaxChars);
//	LONG lReturn = RegSetValueEx(m_hKey, pszKey, 0, REG_BINARY,
//		file.Detach(), dwLen);
//	
//	m_Info.lMessage = lReturn;
//	m_Info.dwSize = dwLen;
//	m_Info.dwType = REG_BINARY;
//
//	if(byData)
//	{
//		free(byData);
//		byData = NULL;
//	}
//
//	if(lReturn == ERROR_SUCCESS)
//		return TRUE;
//	
//	return FALSE;
//}

//BOOL CRegistry::Write (LPCTSTR pszKey, CWordArray& wcArray)
//{
//	ASSERT(m_hKey);
//	ASSERT(pszKey);
//	const int iMaxChars = 4096;
//	BYTE* byData = (BYTE*)::calloc(iMaxChars, sizeof(TCHAR));
//	ASSERT(byData);
//
//	CMemFile file(byData, iMaxChars, 16);
//	CArchive ar(&file, CArchive::store);
//	ASSERT(wcArray.IsSerializable());
//	wcArray.Serialize(ar);
//	ar.Close();
//	const DWORD dwLen = (DWORD)file.GetLength();
//	ASSERT(dwLen < iMaxChars);
//	LONG lReturn = RegSetValueEx(m_hKey, pszKey, 0, REG_BINARY,
//		file.Detach(), dwLen);
//	
//	m_Info.lMessage = lReturn;
//	m_Info.dwSize = dwLen;
//	m_Info.dwType = REG_BINARY;
//
//	if(byData)
//	{
//		free(byData);
//		byData = NULL;
//	}
//
//	if(lReturn == ERROR_SUCCESS)
//		return TRUE;
//	
//	return FALSE;
//}

//BOOL CRegistry::Write (LPCTSTR pszKey, CStringArray& scArray)
//{
//	ASSERT(m_hKey);
//	ASSERT(pszKey);
//	const int iMaxChars = 4096;
//	BYTE* byData = (BYTE*)::calloc(iMaxChars, sizeof(TCHAR));
//	ASSERT(byData);
//
//	CMemFile file(byData, iMaxChars, 16);
//	CArchive ar(&file, CArchive::store);
//	ASSERT(scArray.IsSerializable());
//	scArray.Serialize(ar);
//	ar.Close();
//	const DWORD dwLen = (DWORD)file.GetLength();
//	ASSERT(dwLen < iMaxChars);
//	LONG lReturn = RegSetValueEx(m_hKey, pszKey, 0, REG_BINARY,
//		file.Detach(), dwLen);
//	
//	m_Info.lMessage = lReturn;
//	m_Info.dwSize = dwLen;
//	m_Info.dwType = REG_BINARY;
//
//	if(byData)
//	{
//		free(byData);
//		byData = NULL;
//	}
//
//	if(lReturn == ERROR_SUCCESS)
//		return TRUE;
//	
//	return FALSE;
//}

//BOOL CRegistry::Write(LPCTSTR pszKey, LPCRECT rcRect)
//{
//	ASSERT(m_hKey);
//	ASSERT(pszKey);
//	const int iMaxChars = 30;
//	CDWordArray dwcArray;
//	BYTE* byData = (BYTE*)::calloc(iMaxChars, sizeof(TCHAR));
//	ASSERT(byData);
//
//	dwcArray.SetSize(5);
//	dwcArray.SetAt(0, rcRect->top);
//	dwcArray.SetAt(1, rcRect->bottom);
//	dwcArray.SetAt(2, rcRect->left);
//	dwcArray.SetAt(3, rcRect->right);
//
//	CMemFile file(byData, iMaxChars, 16);
//	CArchive ar(&file, CArchive::store);
//	ASSERT(dwcArray.IsSerializable());
//	dwcArray.Serialize(ar);
//	ar.Close();
//	const DWORD dwLen = (DWORD)file.GetLength();
//	ASSERT(dwLen < iMaxChars);
//	LONG lReturn = RegSetValueEx(m_hKey, pszKey, 0, REG_BINARY,
//		file.Detach(), dwLen);
//	
//	m_Info.lMessage = lReturn;
//	m_Info.dwSize = dwLen;
//	m_Info.dwType = REG_RECT;
//
//	if(byData)
//	{
//		free(byData);
//		byData = NULL;
//	}
//
//	if(lReturn == ERROR_SUCCESS)
//		return TRUE;
//	
//	return FALSE;
//}

//BOOL CRegistry::Write(LPCTSTR pszKey, LPPOINT& lpPoint)
//{
//	ASSERT(m_hKey);
//	ASSERT(pszKey);
//	const int iMaxChars = 20;
//	CDWordArray dwcArray;
//	BYTE* byData = (BYTE*)::calloc(iMaxChars, sizeof(TCHAR));
//	ASSERT(byData);
//
//	dwcArray.SetSize(5);
//	dwcArray.SetAt(0, lpPoint->x);
//	dwcArray.SetAt(1, lpPoint->y);
//
//	CMemFile file(byData, iMaxChars, 16);
//	CArchive ar(&file, CArchive::store);
//	ASSERT(dwcArray.IsSerializable());
//	dwcArray.Serialize(ar);
//	ar.Close();
//	const DWORD dwLen = (DWORD)file.GetLength();
//	ASSERT(dwLen < iMaxChars);
//	LONG lReturn = RegSetValueEx(m_hKey, pszKey, 0, REG_BINARY,
//		file.Detach(), dwLen);
//	
//	m_Info.lMessage = lReturn;
//	m_Info.dwSize = dwLen;
//	m_Info.dwType = REG_POINT;
//
//	if(byData)
//	{
//		free(byData);
//		byData = NULL;
//	}
//
//	if(lReturn == ERROR_SUCCESS)
//		return TRUE;
//	
//	return FALSE;
//}

//BOOL CRegistry::Read (LPCTSTR pszKey, DWORD& dwVal)
//{
//	ASSERT(m_hKey);
//	ASSERT(pszKey);
//
//	DWORD dwType;
//	DWORD dwSize = sizeof (DWORD);
//	DWORD dwDest;
//
//	LONG lReturn = RegQueryValueEx (m_hKey, (LPTSTR) pszKey, NULL, 
//		&dwType, (BYTE *) &dwDest, &dwSize);
//
//	m_Info.lMessage = lReturn;
//	m_Info.dwType = dwType;
//	m_Info.dwSize = dwSize;
//
//	if(lReturn == ERROR_SUCCESS)
//	{
//		dwVal = dwDest;
//		return TRUE;
//	}
//
//	return FALSE;
//}

BOOL CRegistry::Read (LPCTSTR pszKey, CString& sVal)
{
	ASSERT(m_hKey);
	ASSERT(pszKey);

	DWORD dwType;
	DWORD dwSize = 200;
	TCHAR  szString[255];

	LONG lReturn = RegQueryValueEx (m_hKey, (LPTSTR) pszKey, NULL,
		&dwType, (BYTE *) szString, &dwSize);

	//m_Info.lMessage = lReturn;
	//m_Info.dwType = dwType;
	//m_Info.dwSize = dwSize;

	if(lReturn == ERROR_SUCCESS)
	{
		sVal = szString;
		return TRUE;
	}

	return FALSE;
}

//BOOL CRegistry::DeleteValue (LPCTSTR pszValue)
//{
//	ASSERT(m_hKey);
//	LONG lReturn = RegDeleteValue(m_hKey, pszValue);
//
//
//	m_Info.lMessage = lReturn;
//	m_Info.dwType = 0L;
//	m_Info.dwSize = 0L;
//
//	if(lReturn == ERROR_SUCCESS)
//		return TRUE;
//
//	return FALSE;
//}

//BOOL CRegistry::DeleteValueKey (HKEY hKeyRoot, LPCTSTR pszPath)
//{
//	ASSERT(pszPath);
//	ASSERT(hKeyRoot);
//
//	LONG lReturn = RegDeleteKey(hKeyRoot, pszPath);
//
//	m_Info.lMessage = lReturn;
//	m_Info.dwType = 0L;
//	m_Info.dwSize = 0L;
//
//	if(lReturn == ERROR_SUCCESS)
//		return TRUE;
//
//	return FALSE;
//}

//CString CRegistry::GetEventLogLists(HKEY hKeyRoot, LPCTSTR pszPath)
//{
//	if(!VerifyKey(hKeyRoot, pszPath))  
//		return _T("");	
//	//** open Registry value
//	Open(hKeyRoot,pszPath);
//
//	CString strKeys;
//	DWORD dwIndex=0;
//	//char ValueName[1024]="";
//	TCHAR ValueName[1024] = {0};
//	DWORD dwValueNameSize=sizeof(ValueName);
//	DWORD dwType=REG_NONE;
//	BYTE Data[1024]="";
//	DWORD dwDataSize=sizeof(Data);
//	LONG Result=RegEnumKey(m_hKey, dwIndex, ValueName, dwValueNameSize);	
//	while (Result!=ERROR_NO_MORE_ITEMS)
//	{
//		//** ADD KEY NAME
//		strKeys += ValueName;
//		strKeys += _T(", ");
//
//		dwType=REG_NONE;
//		dwValueNameSize=sizeof(ValueName);
//		dwDataSize=sizeof(Data);
//		ZeroMemory(ValueName,dwValueNameSize);
//		ZeroMemory(Data,dwDataSize);
//
//		dwIndex++;
//		Result=RegEnumKey(m_hKey, dwIndex, ValueName, dwValueNameSize);	
//	}	
//
//	//** CLOSE
//	Close();
//
//	int nLenghth = strKeys.GetLength ();
//	if(nLenghth>2)
//		strKeys = strKeys.Left(nLenghth-2);
//	return strKeys;
//}


//** 2003-11-11
//** DISCOVERY - SERVICE

#define REG_PATH_SERVICE		_T("SYSTEM\\CurrentControlSet\\Services")
//CString CRegistry::GetServiceList()
//{
//	if(!VerifyKey(HKEY_LOCAL_MACHINE, REG_PATH_SERVICE))  
//		return _T("");	
//	//** OPEN REGISTRY VALUE
//	Open(HKEY_LOCAL_MACHINE,REG_PATH_SERVICE);
//
//	CString strKeys,strDisp;
//	DWORD dwIndex=0;
//	//char ValueName[1024]="";
//	TCHAR ValueName[1024] = {0};
//	DWORD dwValueNameSize=sizeof(ValueName);
////	DWORD dwType=REG_NONE;
//	BYTE Data[1024]="";
//	DWORD dwDataSize=sizeof(Data);	
//	LONG Result;
//	ZeroMemory(ValueName,dwValueNameSize);
//	ZeroMemory(Data,dwDataSize);
//	int nType;
//	while(1)
//	{
//		//** OPEN REGISTRY VALUE
//		Open(HKEY_LOCAL_MACHINE,REG_PATH_SERVICE);
//		Result=RegEnumKey(m_hKey, dwIndex, ValueName, dwValueNameSize);
//		//** CLOSE
//		Close();
//
//		if(Result == ERROR_NO_MORE_ITEMS)
//			break;
//		//** CHECK DISPLAY NAME , OBJECT NAME
//		strDisp = GetDispName(ValueName, nType);
//		if(!strDisp.IsEmpty())
//		{
//			//** ADD KEY NAME
//			strKeys += strDisp;
//			strKeys += _T(", ");
//		}
//		dwIndex++;		
//	}
//
//	//** CLOSE
//	Close();
//	int nLenghth = strKeys.GetLength ();
//	if(nLenghth>2)
//		strKeys = strKeys.Left(nLenghth-2);
//	return strKeys;
//}

//BOOL CRegistry::GetServiceItem(int nIndex, CString& strRegistry, CString& strDisplay, int& nType)
//{
//	if(!VerifyKey(HKEY_LOCAL_MACHINE, REG_PATH_SERVICE))  
//		return FALSE;	
//	
//	CString strKeys;
////	DWORD dwIndex=0;
//	//char ValueName[1024]="";
//	TCHAR ValueName[1024] = {0};
//	DWORD dwValueNameSize=sizeof(ValueName);
////	DWORD dwType=REG_NONE;
//	BYTE Data[1024]="";
//	DWORD dwDataSize=sizeof(Data);	
//	LONG Result;
//	ZeroMemory(ValueName,dwValueNameSize);
//	ZeroMemory(Data,dwDataSize);
//
//	//** OPEN REGISTRY VALUE
//	Open(HKEY_LOCAL_MACHINE,REG_PATH_SERVICE);
//	Result=RegEnumKey(m_hKey, nIndex, ValueName, dwValueNameSize);
//	//** CLOSE
//	Close();
//
//	if(Result == ERROR_NO_MORE_ITEMS)
//		return FALSE;
//	//** CHECK DISPLAY NAME , OBJECT NAME
//	strDisplay = GetDispName(ValueName,nType);
//	if(strDisplay.IsEmpty())
//		return TRUE;
//
//	nType--;	//** 1:Auto 2: Manual 3: Not Use
//	strRegistry = ValueName;	
//	return TRUE;
//}

//CString CRegistry::GetDispName(CString strService, int& nType)
//{
//	UINT n;
//	CString strPath,strTemp;
//	strPath.Format(_T("%s\\%s"),REG_PATH_SERVICE,strService);
//
//	if(!VerifyKey(HKEY_LOCAL_MACHINE, strPath))  
//		return _T("");	
//	//** OPEN REGISTRY VALUE
//	if(!Open(HKEY_LOCAL_MACHINE,strPath))
//		return _T("");	
//	//** GET OBJECTNAME
//	Read(_T("ObjectName"),strTemp);
//	if(strTemp.IsEmpty())
//		return  _T("");
//	//** GET TYPE
//	Read(_T("Start"),n);
//	nType = (int)n;
//	//** GET DISPLAY NAME
//	Read(_T("DisplayName"),strTemp);
//	//** CLOSE
//	Close();
//	
//	return strTemp;	
//}
