/////////////////////////////////////////////////////////////////////////////
//
//  Zoomview.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudioDlg.h"
#include "SRSIndex.h"
#include "Zoomview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CZoomview, CView)

CZoomview::CZoomview()
{
	m_pImgZoom = NULL;
}
CZoomview::~CZoomview(){}
BEGIN_MESSAGE_MAP(CZoomview, CView)	
	ON_WM_PAINT()
	ON_WM_SIZE()		
END_MESSAGE_MAP()

// CZoomview diagnostics
#ifdef _DEBUG
void CZoomview::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CZoomview::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG

void CZoomview::OnPaint()
{	
	DrawZoomview();	
	CView::OnPaint();
}

void CZoomview::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);		
}

void CZoomview::CopyImage(IplImage *pImgZoom)
{
	CRect rect;
	GetClientRect(&rect);
	if(m_pImgZoom)
		cvReleaseImage(&m_pImgZoom);
	m_pImgZoom = cvCreateImage( cvSize(rect.Width(),rect.Height()),pImgZoom->depth, pImgZoom->nChannels );
	cvResize(pImgZoom, m_pImgZoom );

	DrawZoomview();	
	Invalidate(FALSE);	
}
//------------------------------------------------------------------------------ 
//! @brief		DrawZoomview
//! @date		2010-05-19
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CZoomview::DrawZoomview()
{
	if(!m_pImgZoom)	
		return;
	
	//-- GetRect
	CRect rect;
	GetClientRect(&rect);

	//-- 2011-05-16 hongsu.jung		
	CPaintDC dc(this);	
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);


	//-- Draw Liveview
	CvvImage cImage;		
	cImage.CopyOf(m_pImgZoom,8);
	
	cImage.Show(mDC.GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height() );	
	cImage.Destroy();

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}