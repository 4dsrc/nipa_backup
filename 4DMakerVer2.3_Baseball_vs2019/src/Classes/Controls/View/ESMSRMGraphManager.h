////////////////////////////////////////////////////////////////////////////////
//
//	TGSRMGraphManager.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-10-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once



class CTGSRMGraphManager : public CWinThread
{
	DECLARE_DYNCREATE(CTGSRMGraphManager)

public:	
	CTGSRMGraphManager();           
	virtual ~CTGSRMGraphManager();

public:
	//-- BASIC FUNCTIONS	
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run(void);
	int AddMessage(TGEvent* pEvent);
	int GetMessageCount();
	BOOL IsRun()	{ return m_bRun; }

private:
	CRITICAL_SECTION _TGSRMGraphMgr;

	CTGArray	m_arMsg;	
	BOOL		m_bRun;
	BOOL		m_bStop;
	BOOL		m_bError;	

public :	
	
protected:
	
	DECLARE_MESSAGE_MAP()

private:
};


