////////////////////////////////////////////////////////////////////////////////
//
//	4DMakerMainGrid.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

class CESMSplashWnd;
class C4DMakerDSCMgr;
#include "DSCInfo.h"

class C4DMakerMainGrid : public CESMAdvGrid
{
public:
	C4DMakerMainGrid(void);
	~C4DMakerMainGrid(void);

private:
	CRITICAL_SECTION _DSCGrid;

public:
	CESMSplashWnd* m_pSplash;

	BOOL ClearGrid();
	void Init(void);
	void InitGridColumn();
	void ShowGroupArea();
	void ShowColumnChooser();

protected:
	DECLARE_MESSAGE_MAP()	
		

	//==================================================================
	//== DSC
	//== Date	 : 2013-04-23
	//== Owner	 : hongsu.jung
	//== Comment : DSC Control
	//==================================================================
public:
	C4DMakerDSCMgr* m_pDSCMgr;
	BOOL InitDSCMgr();
	void DisConnect();
	BOOL IsExist(CString srtUniqueID);
	BOOL AddDSC(CSdiSingleMgr* pDSC);
};
