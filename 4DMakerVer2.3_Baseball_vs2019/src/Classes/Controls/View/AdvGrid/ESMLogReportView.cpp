////////////////////////////////////////////////////////////////////////////////
//
//	TGLogReportView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-24
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GlobalIndex.h"
#include "TG.h"
#include "TGLogReportView.h"


// CTGLogReportView 대화 상자입니다.
CTGLogReportView::CTGLogReportView(CWnd* pParent /*=NULL*/)
: CDialog(CTGLogReportView::IDD, pParent){}
CTGLogReportView::~CTGLogReportView(){}

void CTGLogReportView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTGLogReportView)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
}

BEGIN_MESSAGE_MAP(CTGLogReportView, CDialog)
	//{{AFX_MSG_MAP(CTGLogReportView)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()	
	ON_COMMAND(ID_IMAGE_REPORT_GROUP			,OnReportGroup			)	
	ON_COMMAND(ID_IMAGE_REPORT_CHOOSER			,OnReportChooser		)		
END_MESSAGE_MAP()

BOOL CTGLogReportView::OnInitDialog() 
{
	if(!CDialog::OnInitDialog())
	{
		ASSERT( FALSE );
		return FALSE;
	}	

	if(	! m_LogGrid.Create(
		this,
		CRect( 0, 0, 0, 0 ),
		AFX_IDW_PANE_FIRST
		)
		)
	{
		TGLog(0,_T("Failed to create view window"));
		return -1;
	}

	//-- 2012-07-24 hongsu@esmlab.com
	//-- Init
	m_LogGrid.InitReport();
	return TRUE;
}

void CTGLogReportView::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	m_rcClientFrame.top	= 30;
	RepositionBars(0,0xFFFF,0);

	//-- RESIZE CONTROL
	m_LogGrid.MoveWindow(m_rcClientFrame);
}

void CTGLogReportView::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_REPORT_GROUP	,
		ID_IMAGE_REPORT_CHOOSER	,
	};

	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	

	m_wndToolBar.ShowWindow(SW_SHOW);
}

void CTGLogReportView::OnReportGroup()
{
	m_LogGrid.ShowGroupArea();
}

void CTGLogReportView::OnReportChooser()
{
	m_LogGrid.ShowColumnChooser();
}

BOOL CTGLogReportView::AddLog(CObject* pReceiveObject)
{
	return m_LogGrid.AddLog(pReceiveObject);
}

BOOL CTGLogReportView::ClearGrid()
{
	return m_LogGrid.ClearGrid();
}

BOOL CTGLogReportView::LogCheck(CObject* pCompare)
{
	return m_LogGrid.LogCheck(pCompare);
}



//------------------------------------------------------------------------------
//! @brief		Log Report Manager
//! @date		2012-07-12
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
