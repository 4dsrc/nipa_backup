////////////////////////////////////////////////////////////////////////////////
//
//	DSCInfo.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ESMFunc.h"
#include "ESMAdvGrid.h"
#include "SdiSingleMgr.h"

class CESMSplashWnd;

enum 
{
	DSC_COLUMN_UNIQUE		= 0,
	DSC_COLUMN_STATUS		,
	DSC_COLUMN_INTERVAL		,
	DSC_COLUMN_ADJUST_X		,
	DSC_COLUMN_ADJUST_Y		,
	DSC_COLUMN_PICTURE		,
};

enum
{
	DSC_STATUS_UNKNOWN		= 0,
	DSC_STATUS_NORMAL		,
	DSC_STATUS_FOCUSSED		,
};


const CString g_arDSCColumn[] = 
{
	_T("Unique ID"),
	_T("Status"),
	_T("Interval"),
	_T("Adjust X"),	
	_T("Adjust Y"),		
	_T("Picture"),	
};


class CDSCInfo : public CObject
{
public:
	CDSCInfo();
	~CDSCInfo();

	CString m_strUniqieID		;
	int m_nStatus				;
	int m_nInterval				;
	int m_nAdjustX				;		
	int m_nAdjustY				;

	//-- 2013-04-22 hongsu.jung
	//-- Picture
	CExtBitmap* m_pBmpThumbnail	;	
	//-- DSC Connection Infomation
	CSdiSingleMgr* m_pDSCSingleMgr;
};


