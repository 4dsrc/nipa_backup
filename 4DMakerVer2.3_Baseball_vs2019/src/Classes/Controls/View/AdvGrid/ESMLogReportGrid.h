////////////////////////////////////////////////////////////////////////////////
//
//	TGLogReportGrid.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-24
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "TGAdvGrid.h"

enum 
{
	LOG_REPORT_TIME		= 0,
	LOG_REPORT_IP		,
	LOG_REPORT_KEY		,
	LOG_REPORT_CONTENT	,
};

const CString g_arLogReportColumn[] = 
{
	_T("Time"	),
	_T("IP"		),
	_T("Key"	),	
	_T("Content"),		
};


class CLogReport : public CObject
{
public:
	CLogReport() {};
	~CLogReport() {};

	CString m_strTime		;
	CString m_strIP			;		
	CString m_strKey		;
	CString m_strContent	;		
};


class CTGLogReportGrid : public CTGAdvGrid //CListCtrl
{
public:
	CTGLogReportGrid(void);
public:
	~CTGLogReportGrid(void);

public:
	BOOL AddLog(CObject* pReceiveObject);
	BOOL GetLog(CString strIP, CString strLog);	
	BOOL AddLogLine(CString strIP, CString strLine);
	BOOL AddLogLines();
	BOOL ClearGrid();
	BOOL LogCheck(CObject* pCompare);

	void InitReport(void);
	void InitGridColumn();
	void ShowGroupArea();
	void ShowColumnChooser();

	int  GetLineCount();
	CLogReport* GetLogRoport(int nIndex);
	void RemoveLine();
	void AddLogLine(CLogReport* pLogReport);

protected:

	DECLARE_MESSAGE_MAP()
	CTGArray m_arLine;
	int GetDestColumn(CString strDest);
	BOOL GetCheckGroup(CString strGroup);
	void ShowInfo(CLogReport* pLogReport);

};
