////////////////////////////////////////////////////////////////////////////////
//
//	TGLogReportView.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-24
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "TGLogReportGrid.h"

class CTGOption;


// CTGLogReportView 대화 상자입니다.

class CTGLogReportView : public CDialog 
{ 
	class CInnerToolControlBar:public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

public:
	CTGLogReportView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTGLogReportView();

	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CTGLogReportView)
	enum { IDD = IDD_GRAPH_WITH_TOOLBAR };
	//}}AFX_DATA

private:
	//-- To Toolbar
	CRect m_rcClientFrame;	
	CInnerToolControlBar m_wndToolBar;	
	CTGLogReportGrid m_LogGrid;	

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg void OnReportGroup();	
	afx_msg void OnReportChooser();	
	DECLARE_MESSAGE_MAP()

public:

	BOOL AddLog(CObject* pReceiveObject);
	BOOL ClearGrid();
	BOOL LogCheck(CObject* pCompare);

	LRESULT AnalysisLogAdd(CObject* pReceiveObject)	{ return AddLog(pReceiveObject);}
	LRESULT AnalysisLogClear()						{ return ClearGrid();		}
	LRESULT AnalysisLogCheck(CObject* pCompare)		{ return LogCheck(pCompare);}


};
