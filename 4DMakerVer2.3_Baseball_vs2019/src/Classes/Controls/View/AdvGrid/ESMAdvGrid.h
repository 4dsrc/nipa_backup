////////////////////////////////////////////////////////////////////////////////
//
//	TGAdvGrid.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-23
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Prof-UIS.h>

#define ID_RG_SHOW_COLUMN_CHOOSER						2401
#define ID_RG_SHOW_GROUP_AREA							2402	// Group Box
#define ID_RG_COLUMN_CHOOSER_LOCATION_FLOATING			2403
#define ID_RG_COLUMN_CHOOSER_LOCATION_TOP				2404
#define ID_RG_COLUMN_CHOOSER_LOCATION_BOTTOM			2405
#define ID_RG_COLUMN_CHOOSER_LOCATION_LEFT				2406
#define ID_RG_COLUMN_CHOOSER_LOCATION_RIGHT				2407
#define ID_RG_AUTO_SIZE_COLUMNS							2408
#define ID_RG_RESIZE_COLUMN_DYNAMICALLY					2409
#define ID_RG_USE_MULTI_ROW_SELECTION					2410
#define ID_RG_SUBTRACT_SEL_AREAS						2411
#define ID_RG_USE_COLUMN_BASED_FOCUS					2412
#define ID_RG_ENABLE_CELL_EDITING						2413
#define ID_RG_ENABLE_CELL_AUTO_EDITING					2414
#define ID_RG_AUTO_PREVIEW_MODE							2415
#define ID_RG_GRID_LINES_HORIZONTAL						2416
#define ID_RG_GRID_LINES_VERTICAL						2417
#define ID_RG_USE_PM									2418
#define ID_RG_USE_LAYOUT_AND_STYLE_2003					2419
#define ID_RG_COLLAPSE_GROUPS_AFTER_REGROUPING			2420
#define ID_RG_ENABLE_COLUMN_CONTEXT_MENU				2421
#define ID_RG_ENABLE_FIELD_LIST_COLUMN_CONTEXT_MENU		2422
#define ID_RG_USE_BOLD_GROUPS							2423
#define ID_RG_RESET_SORT_ORDER							2424
#define ID_RG_EXTENDED_LISTBOX_MULTI_SELECTION_STYLE	2425
#define ID_RG_SHOW_BUTTONS_IN_ALL_ROWS					2426
#define ID_RG_SHOW_BUTTONS_IN_FOCUSED_ROW				2427
#define ID_RG_SHOW_BUTTONS_IN_SELECTED_ROWS				2428
#define ID_RG_SHOW_BUTTONS_NEVER						2429


#define __ENABLE_CELLEDITING_BSE_STYLES__ \
	(__EGWS_BSE_EDIT_SINGLE_LCLICK \
	|__EGWS_BSE_EDIT_SINGLE_FOCUSED_ONLY \
	|__EGWS_BSE_EDIT_DOUBLE_LCLICK \
	|__EGWS_BSE_EDIT_RETURN_CLICK \
	|__EGWS_BSE_EDIT_CELLS_INNER \
	)
/////////////////////////////////////////////////////////////////////////////
// CESMAdvGrid window

class CESMAdvGrid : public CExtReportGridWnd
{
// Construction
public:
	CESMAdvGrid();

	void InitGridColumn();
	void AddSampleContent();

// Attributes
public:
	
protected:
	
	CTypedPtrList < CPtrList, CExtReportGridColumn * > m_listColumns;
	INT m_nBoldFontIndex;

// Operations
public:

	int  GetLineCount();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMAdvGrid)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CESMAdvGrid();

	// Generated message map functions
protected:
	//{{AFX_MSG(CESMAdvGrid)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnRgShowColumnChooser();
	afx_msg void OnRgShowGroupArea();
	afx_msg void OnUpdateRgShowColumnChooser(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRgShowGroupArea(CCmdUI* pCmdUI);
	afx_msg void OnRgColumnChooserLocationFloating();
	afx_msg void OnUpdateRgColumnChooserLocationFloating(CCmdUI* pCmdUI);
	afx_msg void OnRgColumnChooserLocationTop();
	afx_msg void OnUpdateRgColumnChooserLocationTop(CCmdUI* pCmdUI);
	afx_msg void OnRgColumnChooserLocationBottom();
	afx_msg void OnUpdateRgColumnChooserLocationBottom(CCmdUI* pCmdUI);
	afx_msg void OnRgColumnChooserLocationLeft();
	afx_msg void OnUpdateRgColumnChooserLocationLeft(CCmdUI* pCmdUI);
	afx_msg void OnRgColumnChooserLocationRight();
	afx_msg void OnUpdateRgColumnChooserLocationRight(CCmdUI* pCmdUI);
	afx_msg void OnRgAutoSizeColumns();
	afx_msg void OnUpdateRgAutoSizeColumns(CCmdUI* pCmdUI);
	afx_msg void OnRgResizeColumnDynamically();
	afx_msg void OnUpdateRgResizeColumnDynamically(CCmdUI* pCmdUI);
	afx_msg void OnRgUseMultiRowSelection();
	afx_msg void OnUpdateRgUseMultiRowSelection(CCmdUI* pCmdUI);
	afx_msg void OnRgSubtractSelAreas();
	afx_msg void OnUpdateRgSubtractSelAreas(CCmdUI* pCmdUI);
	afx_msg void OnEditClear();
	afx_msg void OnUpdateEditClear(CCmdUI* pCmdUI);
	afx_msg void OnEditClearAll();
	afx_msg void OnUpdateEditClearAll(CCmdUI* pCmdUI);
	afx_msg void OnEditSelectAll();
	afx_msg void OnUpdateEditSelectAll(CCmdUI* pCmdUI);
	afx_msg void OnRgUseColumnBasedFocus();
	afx_msg void OnUpdateRgUseColumnBasedFocus(CCmdUI* pCmdUI);
	afx_msg void OnRgEnableCellEditing();
	afx_msg void OnUpdateRgEnableCellEditing(CCmdUI* pCmdUI);
	afx_msg void OnRgEnableCellAutoEditing();
	afx_msg void OnUpdateRgEnableCellAutoEditing(CCmdUI* pCmdUI);
	afx_msg void OnRgAutoPreviewMode();
	afx_msg void OnUpdateRgAutoPreviewMode(CCmdUI* pCmdUI);
	afx_msg void OnRgGridLinesHorizontal();
	afx_msg void OnUpdateRgGridLinesHorizontal(CCmdUI* pCmdUI);
	afx_msg void OnRgGridLinesVertical();
	afx_msg void OnUpdateRgGridLinesVertical(CCmdUI* pCmdUI);
	afx_msg void OnRgUsePM();
	afx_msg void OnUpdateRgUsePM(CCmdUI* pCmdUI);
	afx_msg void OnRgUseLayoutAndStyle2003();
	afx_msg void OnUpdateRgUseLayoutAndStyle2003(CCmdUI* pCmdUI);
	afx_msg void OnRgCollapseGroupsAfterRegrouping();
	afx_msg void OnUpdateRgCollapseGroupsAfterRegrouping(CCmdUI* pCmdUI);
	afx_msg void OnRgEnableColumnContextMenu();
	afx_msg void OnUpdateRgEnableColumnContextMenu(CCmdUI* pCmdUI);
	afx_msg void OnRgEnableFieldListColumnContextMenu();
	afx_msg void OnUpdateRgEnableFieldListColumnContextMenu(CCmdUI* pCmdUI);
	afx_msg void OnRgUseBoldGroups();
	afx_msg void OnUpdateRgUseBoldGroups(CCmdUI* pCmdUI);
	afx_msg void OnRgResetSortOrder();
	afx_msg void OnUpdateRgResetSortOrder(CCmdUI* pCmdUI);
	afx_msg void OnRgExtendedListboxMultiSelectionStyle();
	afx_msg void OnUpdateRgExtendedListboxMultiSelectionStyle(CCmdUI* pCmdUI);
	afx_msg void OnRgShowButtonsInAllRows();
	afx_msg void OnUpdateRgShowButtonsInAllRows(CCmdUI* pCmdUI);
	afx_msg void OnRgShowButtonsInFocusedRow();
	afx_msg void OnUpdateRgShowButtonsInFocusedRow(CCmdUI* pCmdUI);
	afx_msg void OnRgShowButtonsInSelectedRows();
	afx_msg void OnUpdateRgShowButtonsInSelectedRows(CCmdUI* pCmdUI);
	afx_msg void OnRgShowButtonsNever();
	afx_msg void OnUpdateRgShowButtonsNever(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnGridCellInputComplete(
		CExtGridCell & _cell,
		LONG nColNo,
		LONG nRowNo,
		INT nColType,
		INT nRowType,
		HWND hWndInputControl = NULL
		);
protected:
	void _AdjustRowBoldState(
		LONG nRowNo,
		bool bRedraw = true
		);
	void _AdjustRowBoldState(
		CExtReportGridItem * pRGI,
		bool bRedraw = true
		);
};
