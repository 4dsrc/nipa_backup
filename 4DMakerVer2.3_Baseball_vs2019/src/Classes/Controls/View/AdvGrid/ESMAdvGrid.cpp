////////////////////////////////////////////////////////////////////////////////
//
//	TGAdvGrid.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-23
// 

//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "ESMAdvGrid.h"

/////////////////////////////////////////////////////////////////////////////
// CESMAdvGrid

CESMAdvGrid::CESMAdvGrid()
	: m_nBoldFontIndex( -1 )
{
}

CESMAdvGrid::~CESMAdvGrid()
{
}


BEGIN_MESSAGE_MAP( CESMAdvGrid, CExtReportGridWnd )
	//{{AFX_MSG_MAP(CESMAdvGrid)
	ON_WM_CREATE()
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CLEAR, OnUpdateEditClear)
	ON_COMMAND(ID_EDIT_CLEAR_ALL, OnEditClearAll)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CLEAR_ALL, OnUpdateEditClearAll)
	ON_COMMAND(ID_EDIT_SELECT_ALL, OnEditSelectAll)
	ON_UPDATE_COMMAND_UI(ID_EDIT_SELECT_ALL, OnUpdateEditSelectAll)

	ON_COMMAND(ID_RG_SHOW_COLUMN_CHOOSER, OnRgShowColumnChooser)
	ON_COMMAND(ID_RG_SHOW_GROUP_AREA, OnRgShowGroupArea)
	ON_UPDATE_COMMAND_UI(ID_RG_SHOW_COLUMN_CHOOSER, OnUpdateRgShowColumnChooser)
	ON_UPDATE_COMMAND_UI(ID_RG_SHOW_GROUP_AREA, OnUpdateRgShowGroupArea)
	ON_COMMAND(ID_RG_COLUMN_CHOOSER_LOCATION_FLOATING, OnRgColumnChooserLocationFloating)
	ON_UPDATE_COMMAND_UI(ID_RG_COLUMN_CHOOSER_LOCATION_FLOATING, OnUpdateRgColumnChooserLocationFloating)
	ON_COMMAND(ID_RG_COLUMN_CHOOSER_LOCATION_TOP, OnRgColumnChooserLocationTop)
	ON_UPDATE_COMMAND_UI(ID_RG_COLUMN_CHOOSER_LOCATION_TOP, OnUpdateRgColumnChooserLocationTop)
	ON_COMMAND(ID_RG_COLUMN_CHOOSER_LOCATION_BOTTOM, OnRgColumnChooserLocationBottom)
	ON_UPDATE_COMMAND_UI(ID_RG_COLUMN_CHOOSER_LOCATION_BOTTOM, OnUpdateRgColumnChooserLocationBottom)
	ON_COMMAND(ID_RG_COLUMN_CHOOSER_LOCATION_LEFT, OnRgColumnChooserLocationLeft)
	ON_UPDATE_COMMAND_UI(ID_RG_COLUMN_CHOOSER_LOCATION_LEFT, OnUpdateRgColumnChooserLocationLeft)
	ON_COMMAND(ID_RG_COLUMN_CHOOSER_LOCATION_RIGHT, OnRgColumnChooserLocationRight)
	ON_UPDATE_COMMAND_UI(ID_RG_COLUMN_CHOOSER_LOCATION_RIGHT, OnUpdateRgColumnChooserLocationRight)
	ON_COMMAND(ID_RG_AUTO_SIZE_COLUMNS, OnRgAutoSizeColumns)
	ON_UPDATE_COMMAND_UI(ID_RG_AUTO_SIZE_COLUMNS, OnUpdateRgAutoSizeColumns)
	ON_COMMAND(ID_RG_RESIZE_COLUMN_DYNAMICALLY, OnRgResizeColumnDynamically)
	ON_UPDATE_COMMAND_UI(ID_RG_RESIZE_COLUMN_DYNAMICALLY, OnUpdateRgResizeColumnDynamically)
	ON_COMMAND(ID_RG_USE_MULTI_ROW_SELECTION, OnRgUseMultiRowSelection)
	ON_UPDATE_COMMAND_UI(ID_RG_USE_MULTI_ROW_SELECTION, OnUpdateRgUseMultiRowSelection)
	ON_COMMAND(ID_RG_SUBTRACT_SEL_AREAS, OnRgSubtractSelAreas)
	ON_UPDATE_COMMAND_UI(ID_RG_SUBTRACT_SEL_AREAS, OnUpdateRgSubtractSelAreas)	
	ON_COMMAND(ID_RG_USE_COLUMN_BASED_FOCUS, OnRgUseColumnBasedFocus)
	ON_UPDATE_COMMAND_UI(ID_RG_USE_COLUMN_BASED_FOCUS, OnUpdateRgUseColumnBasedFocus)
	ON_COMMAND(ID_RG_ENABLE_CELL_EDITING, OnRgEnableCellEditing)
	ON_UPDATE_COMMAND_UI(ID_RG_ENABLE_CELL_EDITING, OnUpdateRgEnableCellEditing)
	ON_COMMAND(ID_RG_ENABLE_CELL_AUTO_EDITING, OnRgEnableCellAutoEditing)
	ON_UPDATE_COMMAND_UI(ID_RG_ENABLE_CELL_AUTO_EDITING, OnUpdateRgEnableCellAutoEditing)
	ON_COMMAND(ID_RG_AUTO_PREVIEW_MODE, OnRgAutoPreviewMode)
	ON_UPDATE_COMMAND_UI(ID_RG_AUTO_PREVIEW_MODE, OnUpdateRgAutoPreviewMode)
	ON_COMMAND(ID_RG_GRID_LINES_HORIZONTAL, OnRgGridLinesHorizontal)
	ON_UPDATE_COMMAND_UI(ID_RG_GRID_LINES_HORIZONTAL, OnUpdateRgGridLinesHorizontal)
	ON_COMMAND(ID_RG_GRID_LINES_VERTICAL, OnRgGridLinesVertical)
	ON_UPDATE_COMMAND_UI(ID_RG_GRID_LINES_VERTICAL, OnUpdateRgGridLinesVertical)
	ON_COMMAND(ID_RG_USE_PM, OnRgUsePM)
	ON_UPDATE_COMMAND_UI(ID_RG_USE_PM, OnUpdateRgUsePM)
	ON_COMMAND(ID_RG_USE_LAYOUT_AND_STYLE_2003, OnRgUseLayoutAndStyle2003)
	ON_UPDATE_COMMAND_UI(ID_RG_USE_LAYOUT_AND_STYLE_2003, OnUpdateRgUseLayoutAndStyle2003)
	ON_COMMAND(ID_RG_COLLAPSE_GROUPS_AFTER_REGROUPING, OnRgCollapseGroupsAfterRegrouping)
	ON_UPDATE_COMMAND_UI(ID_RG_COLLAPSE_GROUPS_AFTER_REGROUPING, OnUpdateRgCollapseGroupsAfterRegrouping)
	ON_COMMAND(ID_RG_ENABLE_COLUMN_CONTEXT_MENU, OnRgEnableColumnContextMenu)
	ON_UPDATE_COMMAND_UI(ID_RG_ENABLE_COLUMN_CONTEXT_MENU, OnUpdateRgEnableColumnContextMenu)
	ON_COMMAND(ID_RG_ENABLE_FIELD_LIST_COLUMN_CONTEXT_MENU, OnRgEnableFieldListColumnContextMenu)
	ON_UPDATE_COMMAND_UI(ID_RG_ENABLE_FIELD_LIST_COLUMN_CONTEXT_MENU, OnUpdateRgEnableFieldListColumnContextMenu)
	ON_COMMAND(ID_RG_USE_BOLD_GROUPS, OnRgUseBoldGroups)
	ON_UPDATE_COMMAND_UI(ID_RG_USE_BOLD_GROUPS, OnUpdateRgUseBoldGroups)
	ON_COMMAND(ID_RG_RESET_SORT_ORDER, OnRgResetSortOrder)
	ON_UPDATE_COMMAND_UI(ID_RG_RESET_SORT_ORDER, OnUpdateRgResetSortOrder)
	ON_COMMAND(ID_RG_EXTENDED_LISTBOX_MULTI_SELECTION_STYLE, OnRgExtendedListboxMultiSelectionStyle)
	ON_UPDATE_COMMAND_UI(ID_RG_EXTENDED_LISTBOX_MULTI_SELECTION_STYLE, OnUpdateRgExtendedListboxMultiSelectionStyle)
	ON_COMMAND(ID_RG_SHOW_BUTTONS_IN_ALL_ROWS, OnRgShowButtonsInAllRows)
	ON_UPDATE_COMMAND_UI(ID_RG_SHOW_BUTTONS_IN_ALL_ROWS, OnUpdateRgShowButtonsInAllRows)
	ON_COMMAND(ID_RG_SHOW_BUTTONS_IN_FOCUSED_ROW, OnRgShowButtonsInFocusedRow)
	ON_UPDATE_COMMAND_UI(ID_RG_SHOW_BUTTONS_IN_FOCUSED_ROW, OnUpdateRgShowButtonsInFocusedRow)
	ON_COMMAND(ID_RG_SHOW_BUTTONS_IN_SELECTED_ROWS, OnRgShowButtonsInSelectedRows)
	ON_UPDATE_COMMAND_UI(ID_RG_SHOW_BUTTONS_IN_SELECTED_ROWS, OnUpdateRgShowButtonsInSelectedRows)
	ON_COMMAND(ID_RG_SHOW_BUTTONS_NEVER, OnRgShowButtonsNever)
	ON_UPDATE_COMMAND_UI(ID_RG_SHOW_BUTTONS_NEVER, OnUpdateRgShowButtonsNever)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CESMAdvGrid::InitGridColumn()
{
	/*
	CRect rcOutput( 0, 0, 0, 0 );
	ReportGroupAreaShow();
	ReportAutoPreviewModeSet( true, false );

	m_nBoldFontIndex = GridFontInsert( g_PaintManager->m_FontBold, -1, true );

	INT nInitialNormal = 100;	
	CExtReportGridColumn * pRGC = NULL;

	// Time
	pRGC =	ReportColumnRegister( _T("Time"), _T("Information"), true, false );
	ASSERT_VALID( pRGC );
	pRGC->ExtentSet( nInitialNormal );
	m_listColumns.AddTail( pRGC );

	// IP
	pRGC = ReportColumnRegister( _T("IP"), _T("Information"), true, false );
	ASSERT_VALID( pRGC );
	pRGC->ExtentSet( nInitialNormal );
	m_listColumns.AddTail( pRGC );

	// Key
	pRGC = ReportColumnRegister( _T("Key"), _T("Information"), true, false );
	ASSERT_VALID( pRGC );
	pRGC->ExtentSet( nInitialNormal );
	m_listColumns.AddTail( pRGC );

	// Contents
	pRGC = ReportColumnRegister( _T("Contents"), _T("Information"), true, false );
	ASSERT_VALID( pRGC );
	pRGC->ExtentSet( 5*nInitialNormal );
	m_listColumns.AddTail( pRGC );
	*/
}

void CESMAdvGrid::AddSampleContent()
{
	/*
	CExtReportGridColumn* pRGC	= NULL;
	CExtReportGridItem	* pRGI	= NULL;
	CExtGridCell		* pCell = NULL;
	POSITION pos = NULL;
	LONG nRow, nRowCount = LONG( sizeof(g_arrDataRowsInit) / sizeof(g_arrDataRowsInit[0]) );
	
	CTypedPtrArray < CPtrArray, CExtReportGridItem * > arrRegisteredItems;
	arrRegisteredItems.SetSize( nRowCount );
	VERIFY( ReportItemRegister( arrRegisteredItems ) );
	for( nRow = 0; nRow < nRowCount; nRow ++ )
	{
		sample_data & _dr = g_arrDataRowsInit[ nRow ];
		pRGI = arrRegisteredItems[ nRow ];
		ASSERT_VALID( pRGI );

		pos = m_listColumns.GetHeadPosition();
		USES_CONVERSION;

		// Time
		pRGC = m_listColumns.GetNext( pos );
		pCell = ReportItemGetCell( pRGC, pRGI, RUNTIME_CLASS(CExtGridCellStringDM) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.m_strTime) );

		// IP Address
		pRGC = m_listColumns.GetNext( pos );
		pCell = ReportItemGetCell( pRGC, pRGI, RUNTIME_CLASS(CExtGridCellStringDM) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.m_strIP) );

		// Key
		pRGC = m_listColumns.GetNext( pos );
		pCell = ReportItemGetCell( pRGC, pRGI, RUNTIME_CLASS(CExtGridCellStringDM) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.m_strKey) );

		// Contents
		pRGC = m_listColumns.GetNext( pos );
		pCell = ReportItemGetCell( pRGC, pRGI, RUNTIME_CLASS(CExtGridCellStringDM) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(_dr.m_strContent) );
	} 
	*/
}

/////////////////////////////////////////////////////////////////////////////
// CESMAdvGrid message handlers

int CESMAdvGrid::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if( CExtReportGridWnd::OnCreate( lpCreateStruct ) == -1 )
		return -1;
	return 0;
}

void CESMAdvGrid::OnRgShowColumnChooser() 
{
	ReportColumnChooserShow( ! ReportColumnChooserIsVisible() );
}
void CESMAdvGrid::OnUpdateRgShowColumnChooser(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( ReportColumnChooserIsVisible() ? TRUE : FALSE );
}

void CESMAdvGrid::OnRgShowGroupArea() 
{
	ReportGroupAreaShow( ! ReportGroupAreaIsVisible() );
}

void CESMAdvGrid::OnUpdateRgShowGroupArea(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( ReportGroupAreaIsVisible() ? TRUE : FALSE );
}

void CESMAdvGrid::OnRgColumnChooserLocationFloating() 
{
	ReportColumnChooserGet()->OrientationSet(
		AFX_IDW_DOCKBAR_FLOAT );
}

void CESMAdvGrid::OnUpdateRgColumnChooserLocationFloating(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetRadio(
		( ReportColumnChooserGet()->OrientationGet()
			== AFX_IDW_DOCKBAR_FLOAT )
				? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgColumnChooserLocationTop() 
{
	ReportColumnChooserGet()->OrientationSet(
		AFX_IDW_DOCKBAR_TOP );
}

void CESMAdvGrid::OnUpdateRgColumnChooserLocationTop(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetRadio(
		( ReportColumnChooserGet()->OrientationGet()
			== AFX_IDW_DOCKBAR_TOP )
				? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgColumnChooserLocationBottom() 
{
	ReportColumnChooserGet()->OrientationSet(
		AFX_IDW_DOCKBAR_BOTTOM );
}

void CESMAdvGrid::OnUpdateRgColumnChooserLocationBottom(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetRadio(
		( ReportColumnChooserGet()->OrientationGet()
			== AFX_IDW_DOCKBAR_BOTTOM )
				? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgColumnChooserLocationLeft() 
{
	ReportColumnChooserGet()->OrientationSet(
		AFX_IDW_DOCKBAR_LEFT );
}

void CESMAdvGrid::OnUpdateRgColumnChooserLocationLeft(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetRadio(
		( ReportColumnChooserGet()->OrientationGet()
			== AFX_IDW_DOCKBAR_LEFT )
				? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgColumnChooserLocationRight() 
{
	ReportColumnChooserGet()->OrientationSet(
		AFX_IDW_DOCKBAR_RIGHT );
}

void CESMAdvGrid::OnUpdateRgColumnChooserLocationRight(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetRadio(
		( ReportColumnChooserGet()->OrientationGet()
			== AFX_IDW_DOCKBAR_RIGHT )
				? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgAutoSizeColumns() 
{
bool bUseProportionalResizing = ReportColumnProportionalResizingGet();
	ReportColumnProportionalResizingSet(
		! bUseProportionalResizing
		);
}

void CESMAdvGrid::OnUpdateRgAutoSizeColumns(CCmdUI* pCmdUI) 
{
bool bUseProportionalResizing = ReportColumnProportionalResizingGet();
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		bUseProportionalResizing ? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgResizeColumnDynamically() 
{
	if( (SiwGetStyle()&__EGBS_DYNAMIC_RESIZING_H) != 0 )
		SiwModifyStyle( 0, __EGBS_DYNAMIC_RESIZING_H, false );
	else
		SiwModifyStyle( __EGBS_DYNAMIC_RESIZING_H, 0, false );
}

void CESMAdvGrid::OnUpdateRgResizeColumnDynamically(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (SiwGetStyle()&__EGBS_DYNAMIC_RESIZING_H) != 0 ) ? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgUseMultiRowSelection() 
{
	if( (SiwGetStyle()&__EGBS_SFM_ROWS) != 0 )
	{
		SiwModifyStyle( 0, __EGBS_SFM_ROWS|__EGBS_MULTI_AREA_SELECTION, false );
		SelectionUnset();
	}
	else
		SiwModifyStyle( __EGBS_SFM_ROWS|__EGBS_MULTI_AREA_SELECTION, 0, false );
}

void CESMAdvGrid::OnUpdateRgUseMultiRowSelection(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (SiwGetStyle()&__EGBS_SFM_ROWS) != 0 ) ? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgSubtractSelAreas() 
{
	if( (SiwGetStyle()&__EGBS_SUBTRACT_SEL_AREAS) != 0 )
		SiwModifyStyle( 0, __EGBS_SUBTRACT_SEL_AREAS, false );
	else
		SiwModifyStyle( __EGBS_SUBTRACT_SEL_AREAS, 0, false );
	OnSwDoRedraw();
}

void CESMAdvGrid::OnUpdateRgSubtractSelAreas(CCmdUI* pCmdUI) 
{
	if( (SiwGetStyle()&__EGBS_SFM_ROWS) == 0 )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetCheck( 0 );
		return;
	}
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (SiwGetStyle()&__EGBS_SUBTRACT_SEL_AREAS) != 0 ) ? TRUE : FALSE
		);
}

void CESMAdvGrid::OnEditClear() 
{
	if( RowCountGet() == 0 || ColumnCountGet() == 0 )
		return;
	DWORD dwSiwStyle = SiwGetStyle();
	if( (dwSiwStyle&__EGBS_SFM_ROWS) == 0 )
	{ // if single row selection
		CExtReportGridItem * pFocusedRGI = ReportItemFocusGet();
		if( pFocusedRGI == NULL )
			return;
		ASSERT_VALID( pFocusedRGI );
		LPCTSTR strMessage = _T("Delete focused item?");
		if( ::AfxMessageBox(
				LPCTSTR(strMessage),
				MB_ICONQUESTION|MB_YESNO
				) != IDYES
			)
			return;
		VERIFY( ReportItemUnRegister( pFocusedRGI ) );
	} // if single row selection
	else
	{ // if multi row selection
		if(		ReportItemModifySelectionForUnRegistering( true )
			&&	(! SelectionIsEmpty() )
			)
		{
			LPCTSTR strMessage = _T("Delete selection?");
			if( ::AfxMessageBox(
					LPCTSTR(strMessage),
					MB_ICONQUESTION|MB_YESNO
					) != IDYES
				)
				return;
			ReportItemUnRegisterSelection();
		}
	} // if multi row selection
}

void CESMAdvGrid::OnUpdateEditClear(CCmdUI* pCmdUI) 
{
	if( RowCountGet() == 0 || ColumnCountGet() == 0 )
	{
		pCmdUI->Enable( FALSE );
		return;
	}
	
	DWORD dwSiwStyle = SiwGetStyle();
	if( (dwSiwStyle&__EGBS_SFM_ROWS) == 0 )
	{ // if single row selection
		CExtReportGridItem * pFocusedRGI = ReportItemFocusGet();
		pCmdUI->Enable( ( pFocusedRGI != NULL ) ? TRUE : FALSE );
	} // if single row selection
	else
	{ // if multi row selection
		pCmdUI->Enable( SelectionIsEmpty() ? FALSE : TRUE );
	} // if multi row selection
}

void CESMAdvGrid::OnEditClearAll() 
{
	LONG nColCount = ColumnCountGet();
	LONG nRowCount = RowCountGet();
	if( nRowCount == 0 || nColCount == 0 )
		return;
	SelectionUnset( false, false );
	SelectionInsertAt( 0, CRect( 0, 0, nColCount - 1, nRowCount - 1 ), false );
	ReportItemUnRegisterSelection();
}

void CESMAdvGrid::OnUpdateEditClearAll(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( ( RowCountGet() > 0 && ColumnCountGet() > 0 ) ? TRUE : FALSE );
}

void CESMAdvGrid::OnEditSelectAll() 
{
	LONG nColCount = ColumnCountGet();
	LONG nRowCount = RowCountGet();
	if( nRowCount == 0 || nColCount == 0 )
		return;
	SelectionUnset( false, false );
	SelectionInsertAt( 0, CRect( 0, 0, nColCount - 1, nRowCount - 1 ), false );
	FocusSet( CPoint( nColCount - 1, nRowCount - 1 ), true, true, false, true );
}

void CESMAdvGrid::OnUpdateEditSelectAll(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( ( RowCountGet() > 0 && ColumnCountGet() > 0 ) ? TRUE : FALSE );
}

void CESMAdvGrid::OnRgUseColumnBasedFocus() 
{
	if( (SiwGetStyle()&__EGBS_SFB_MASK) == __EGBS_SFB_CELLS )
	{
		SiwModifyStyle( 0, __EGBS_SFB_MASK, false );
		SiwModifyStyle( __EGBS_SFB_FULL_ROWS, 0, true );
	}
	else
	{
		SiwModifyStyle( 0, __EGBS_SFB_MASK, false );
		SiwModifyStyle( __EGBS_SFB_CELLS, 0, true );
	}
}

void CESMAdvGrid::OnUpdateRgUseColumnBasedFocus(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (SiwGetStyle()&__EGBS_SFB_MASK) == __EGBS_SFB_CELLS )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgEnableCellEditing() 
{
	if( (BseGetStyle()&__ENABLE_CELLEDITING_BSE_STYLES__) == 0 )
		BseModifyStyle( __ENABLE_CELLEDITING_BSE_STYLES__ );
	else
		BseModifyStyle( 0, __ENABLE_CELLEDITING_BSE_STYLES__ );
}

void CESMAdvGrid::OnUpdateRgEnableCellEditing(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (BseGetStyle()&__ENABLE_CELLEDITING_BSE_STYLES__) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgEnableCellAutoEditing() 
{
	if( (BseGetStyle()&__EGWS_BSE_EDIT_AUTO) == 0 )
		BseModifyStyle( __EGWS_BSE_EDIT_AUTO );
	else
		BseModifyStyle( 0, __EGWS_BSE_EDIT_AUTO );
}

void CESMAdvGrid::OnUpdateRgEnableCellAutoEditing(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(
		( (BseGetStyle()&__ENABLE_CELLEDITING_BSE_STYLES__) != 0 )
			? TRUE : FALSE
		);
	pCmdUI->SetCheck(
		( (BseGetStyle()&__EGWS_BSE_EDIT_AUTO) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgAutoPreviewMode() 
{
	ReportAutoPreviewModeSet( ! ReportAutoPreviewModeGet() );
}

void CESMAdvGrid::OnUpdateRgAutoPreviewMode(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck( ReportAutoPreviewModeGet() ? TRUE : FALSE );
}


void CESMAdvGrid::OnRgGridLinesHorizontal() 
{
	if( (SiwGetStyle()&__EGBS_GRIDLINES_H) != 0 )
		SiwModifyStyle(	0,	__EGBS_GRIDLINES_H, true );
	else
		SiwModifyStyle( __EGBS_GRIDLINES_H, 0, true );
}

void CESMAdvGrid::OnUpdateRgGridLinesHorizontal(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (SiwGetStyle()&__EGBS_GRIDLINES_H) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgGridLinesVertical() 
{
	if( (SiwGetStyle()&__EGBS_GRIDLINES_V) != 0 )
		SiwModifyStyle(
			0,
			__EGBS_GRIDLINES_V,
			true
			);
	else
		SiwModifyStyle(
			__EGBS_GRIDLINES_V,
			0,
			true
			);
}

void CESMAdvGrid::OnUpdateRgGridLinesVertical(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (SiwGetStyle()&__EGBS_GRIDLINES_V) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgUsePM() 
{
	if( (SiwGetStyleEx()&__EGWS_EX_PM_COLORS) != 0 )
		SiwModifyStyleEx(
			0,
			__EGWS_EX_PM_COLORS,
			false
			);
	else
		SiwModifyStyleEx(
			__EGWS_EX_PM_COLORS,
			0,
			false
			);
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

void CESMAdvGrid::OnUpdateRgUsePM(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (SiwGetStyleEx()&__EGWS_EX_PM_COLORS) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgUseLayoutAndStyle2003() 
{
	if( (ReportGridGetStyle()&__ERGS_LAYOUT_AND_STYLE_2003) != 0 )
		ReportGridModifyStyle(
			0,
			__ERGS_LAYOUT_AND_STYLE_2003,
			false
			);
	else
		ReportGridModifyStyle(
			__ERGS_LAYOUT_AND_STYLE_2003,
			0,
			false
			);
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

void CESMAdvGrid::OnUpdateRgUseLayoutAndStyle2003(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (ReportGridGetStyle()&__ERGS_LAYOUT_AND_STYLE_2003) != 0 )
			? TRUE : FALSE
		);
}


void CESMAdvGrid::OnRgCollapseGroupsAfterRegrouping() 
{
	if( (ReportGridGetStyle()&__ERGS_COLLAPSE_AFTER_REGROUPING) != 0 )
		ReportGridModifyStyle(
			0,
			__ERGS_COLLAPSE_AFTER_REGROUPING,
			false
			);
	else
		ReportGridModifyStyle(
			__ERGS_COLLAPSE_AFTER_REGROUPING,
			0,
			false
			);
}

void CESMAdvGrid::OnUpdateRgCollapseGroupsAfterRegrouping(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (ReportGridGetStyle()&__ERGS_COLLAPSE_AFTER_REGROUPING) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgEnableColumnContextMenu() 
{
	if( (ReportGridGetStyle()&__ERGS_ENABLE_COLUMN_CTX_MENU) != 0 )
		ReportGridModifyStyle(
			0,
			__ERGS_ENABLE_COLUMN_CTX_MENU,
			false
			);
	else
		ReportGridModifyStyle(
			__ERGS_ENABLE_COLUMN_CTX_MENU,
			0,
			false
			);
}

void CESMAdvGrid::OnUpdateRgEnableColumnContextMenu(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (ReportGridGetStyle()&__ERGS_ENABLE_COLUMN_CTX_MENU) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgEnableFieldListColumnContextMenu() 
{
	if( (ReportGridGetStyle()&__ERGS_ENABLE_COLUMN_LIST_IN_CTX_MENU) != 0 )
		ReportGridModifyStyle(
			0,
			__ERGS_ENABLE_COLUMN_LIST_IN_CTX_MENU,
			false
			);
	else
		ReportGridModifyStyle(
			__ERGS_ENABLE_COLUMN_LIST_IN_CTX_MENU,
			0,
			false
			);
}

void CESMAdvGrid::OnUpdateRgEnableFieldListColumnContextMenu(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (ReportGridGetStyle()&__ERGS_ENABLE_COLUMN_LIST_IN_CTX_MENU) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgUseBoldGroups() 
{
	if( (ReportGridGetStyle()&__ERGS_BOLD_GROUPS) != 0 )
		ReportGridModifyStyle(
			0,
			__ERGS_BOLD_GROUPS,
			false
			);
	else
		ReportGridModifyStyle(
			__ERGS_BOLD_GROUPS,
			0,
			false
			);
	OnSwRecalcLayout( true );
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

void CESMAdvGrid::OnUpdateRgUseBoldGroups(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (ReportGridGetStyle()&__ERGS_BOLD_GROUPS) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgResetSortOrder() 
{
	const CExtReportGridSortOrder & _rgsoCurrent = ReportSortOrderGet();
	if( _rgsoCurrent.ColumnCountGet() == 0 )
		return;
	CExtReportGridSortOrder _rgsoNew;
	ReportSortOrderSet( _rgsoNew, true );
}

void CESMAdvGrid::OnUpdateRgResetSortOrder(CCmdUI* pCmdUI) 
{
	const CExtReportGridSortOrder & _rgsoCurrent = ReportSortOrderGet();
	pCmdUI->Enable(	( _rgsoCurrent.ColumnCountGet() == 0 ) ? FALSE : TRUE );
}

void CESMAdvGrid::OnRgExtendedListboxMultiSelectionStyle() 
{
	if( (SiwGetStyle()&__EGBS_LBEXT_SELECTION) != 0 )
		SiwModifyStyle(
			0,
			__EGBS_LBEXT_SELECTION,
			false
			);
	else
		SiwModifyStyle(
			__EGBS_LBEXT_SELECTION,
			0,
			false
			);
}

void CESMAdvGrid::OnUpdateRgExtendedListboxMultiSelectionStyle(CCmdUI* pCmdUI) 
{
	if( (SiwGetStyle()&__EGBS_SFM_ROWS) == 0 )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetCheck( 0 );
		return;
	}
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (SiwGetStyle()&__EGBS_LBEXT_SELECTION) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgShowButtonsInAllRows() 
{
	if( (BseGetStyle()&(__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS)) == 0 )
		return;
	if( (BseGetStyle()&__EGWS_BSE_BUTTONS_PERSISTENT) != 0 )
		return;
	BseModifyStyle(
		__EGWS_BSE_BUTTONS_PERSISTENT,
		__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS,
		false
		);
	OnSwRecalcLayout( true );
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

void CESMAdvGrid::OnUpdateRgShowButtonsInAllRows(CCmdUI* pCmdUI) 
{
	if( (BseGetStyle()&(__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS)) == 0 )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetRadio( 0 );
		return;
	}
	pCmdUI->Enable();
	pCmdUI->SetRadio(
		( (BseGetStyle()&__EGWS_BSE_BUTTONS_PERSISTENT) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgShowButtonsInFocusedRow() 
{
	if( (BseGetStyle()&(__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS)) == 0 )
		return;
	if( (BseGetStyle()&__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW) != 0 )
		return;
	BseModifyStyle(
		__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW,
		__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS,
		false
		);
	OnSwRecalcLayout( true );
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

void CESMAdvGrid::OnUpdateRgShowButtonsInFocusedRow(CCmdUI* pCmdUI) 
{
	if( (BseGetStyle()&(__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS)) == 0 )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetRadio( 0 );
		return;
	}
	pCmdUI->Enable();
	pCmdUI->SetRadio(
		( (BseGetStyle()&__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgShowButtonsInSelectedRows() 
{
	if( (BseGetStyle()&(__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS)) == 0 )
		return;
	if( (BseGetStyle()&__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS) != 0 )
		return;
	BseModifyStyle(
		__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS,
		__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW,
		false
		);
	OnSwRecalcLayout( true );
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

void CESMAdvGrid::OnUpdateRgShowButtonsInSelectedRows(CCmdUI* pCmdUI) 
{
	if( (BseGetStyle()&(__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS)) == 0 )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetRadio( 0 );
		return;
	}
	pCmdUI->Enable();
	pCmdUI->SetRadio(
		( (BseGetStyle()&__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS) != 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnRgShowButtonsNever() 
{
	if( (BseGetStyle()&(__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS)) == 0 )
		BseModifyStyle(
			__EGWS_BSE_BUTTONS_PERSISTENT,
			0,
			false
			);
	else
		BseModifyStyle(
			0,
			__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS,
			false
			);
	OnSwRecalcLayout( true );
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

void CESMAdvGrid::OnUpdateRgShowButtonsNever(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(
		( (BseGetStyle()&(__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS)) == 0 )
			? TRUE : FALSE
		);
}

void CESMAdvGrid::OnDestroy() 
{
	CWinApp * pApp = ::AfxGetApp();
	ASSERT( pApp != NULL );
	ASSERT( pApp->m_pszRegistryKey != NULL );
	ASSERT( pApp->m_pszRegistryKey[0] != _T('\0') );
	ASSERT( pApp->m_pszProfileName != NULL );
	ASSERT( pApp->m_pszProfileName[0] != _T('\0') );
	ASSERT( pApp->m_pszProfileName != NULL );
	ReportGridStateSave(
		_T("TG-ReportGridView"),
		pApp->m_pszRegistryKey,
		pApp->m_pszProfileName,
		pApp->m_pszProfileName
		);
	CExtReportGridWnd ::OnDestroy();
}

void CESMAdvGrid::OnGridCellInputComplete(
	CExtGridCell & _cell,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	HWND hWndInputControl // = NULL
	)
{
	ASSERT_VALID( this );
	HWND hWndOwn = m_hWnd;
	CExtReportGridWnd::OnGridCellInputComplete(
		_cell,
		nColNo,
		nRowNo,
		nColType,
		nRowType,
		hWndInputControl
		);
	if(		hWndOwn == NULL
		||	(! IsWindow(hWndOwn) )
		||	CWnd::FromHandlePermanent( hWndOwn ) != this
		)
		return;
	if( nColType != 0 || nRowType != 0 )
		return;
	const CExtReportGridColumn * pRGC =
		STATIC_DOWNCAST(
			CExtReportGridColumn,
			GridCellGetOuterAtTop( nColNo, 0L )
			);
	ASSERT_VALID( pRGC );
	ASSERT( ReportColumnIsRegistered( pRGC ) );
	CExtSafeString strColumnName = pRGC->ColumnNameGet();
	if( strColumnName == _T("Bold") )
		_AdjustRowBoldState( nRowNo );
}

void CESMAdvGrid::_AdjustRowBoldState(
	LONG nRowNo,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT( 0L <= nRowNo && nRowNo < RowCountGet() );
	HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
	ASSERT( hTreeItem );
	CExtReportGridItem * pRGI = ReportItemFromTreeItem( hTreeItem );
	ASSERT_VALID( pRGI );
	_AdjustRowBoldState( pRGI, bRedraw );
}

void CESMAdvGrid::_AdjustRowBoldState(
	CExtReportGridItem * pRGI,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGI );
	CExtReportGridColumn * pRGC =
		ReportColumnGet(
			_T("Bold"),
			_T("Other Fields")
			);
	ASSERT_VALID( pRGC );
	CExtGridCell * pCellBoldCheckBox = ReportItemGetCell( pRGC, pRGI );
	ASSERT_VALID( pCellBoldCheckBox );
	ASSERT_KINDOF( CExtGridCellCheckBox, pCellBoldCheckBox );
	bool bBold =
		( ((CExtGridCellCheckBox*)pCellBoldCheckBox)->GetCheck() != 0 )
			? true : false;
	bool bAtLeastOneColumnActive = false;
	POSITION pos = ReportColumnGetStartPosition();
	for( ; pos != NULL; )
	{
		pRGC = ReportColumnGetNext( pos );
		ASSERT_VALID( pRGC );
		if( bRedraw && (! pRGC->ColumnIsActive() ) )
			bAtLeastOneColumnActive = true;
		CExtGridCell * pCell = ReportItemGetCell( pRGC, pRGI );
		if( pCell == NULL )
			continue;
		ASSERT_VALID( pCell );
		pCell->FontIndexSet( bBold ? m_nBoldFontIndex : -1 );
	} // for( ; pos != NULL; )
	if( (! bRedraw ) || ! bAtLeastOneColumnActive )
		return;
	LONG nRowNo = ItemGetVisibleIndexOf( *pRGI );
	if( nRowNo < 0 )
		return;
	CRect rcCellExtra;
	if( ! GridCellRectsGet( 0, nRowNo, 0, 0, NULL, &rcCellExtra ) )
		return;
	CRect rcClient = OnSwGetClientRect();
	CRect rcRowExtra(
		rcClient.left,
		rcCellExtra.top,
		rcClient.right,
		rcCellExtra.bottom
		);
	InvalidateRect( &rcRowExtra );
}
