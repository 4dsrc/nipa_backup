////////////////////////////////////////////////////////////////////////////////
//
//	DSCInfo.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "DSCInfo.h"

CDSCInfo::CDSCInfo(void)
{
	m_strUniqieID	= _T("");
	m_nStatus		= DSC_STATUS_UNKNOWN	;
	m_nInterval		= 0	;
	m_nAdjustX		= 0	;		
	m_nAdjustY		= 0	;
	m_nAdjustX			;

	//-- 2013-04-22 hongsu.jung
	//-- Picture
	m_pBmpThumbnail		= NULL;	
	//-- DSC Connection Infomation
	m_pDSCSingleMgr		= NULL;
}

CDSCInfo::~CDSCInfo(void)
{
	if(m_pBmpThumbnail)
		delete m_pBmpThumbnail;

	if(m_pDSCSingleMgr)
	{
		m_pDSCSingleMgr->Delete();
		delete m_pDSCSingleMgr;
	}
}
