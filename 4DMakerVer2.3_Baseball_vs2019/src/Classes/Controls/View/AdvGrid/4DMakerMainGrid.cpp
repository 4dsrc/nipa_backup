////////////////////////////////////////////////////////////////////////////////
//
//	4DMakerMainGrid.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-23
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "4DMakerMainGrid.h"
#include "ESMSplashWnd.h"

C4DMakerMainGrid::C4DMakerMainGrid(void)
{
	InitializeCriticalSection (&_DSCGrid);
	m_pDSCMgr = NULL;
}

C4DMakerMainGrid::~C4DMakerMainGrid(void)
{
	ClearGrid();
	DeleteCriticalSection (&_DSCGrid);
}

BEGIN_MESSAGE_MAP(C4DMakerMainGrid, CESMAdvGrid)	
END_MESSAGE_MAP()


void C4DMakerMainGrid::Init(void)
{
	if( m_pSplash->GetSafeHwnd() != NULL )
		m_pSplash->AddTextLine( _T("Init Main Grid...") );
	
	//-- 2012-07-24 hongsu@esmlab.com
	//-- Set Chooser Left 
	ReportColumnChooserGet()->OrientationSet(AFX_IDW_DOCKBAR_LEFT);
	//-- Previews Mode
	ReportAutoPreviewModeSet(TRUE);
	//-- Non Multi Selection
	SiwModifyStyle( 0, __EGBS_SFM_ROWS|__EGBS_MULTI_AREA_SELECTION, false );
	SelectionUnset();
	//-- Non Select Mask
	SiwModifyStyle( 0, __EGBS_SFB_MASK, false );
	SiwModifyStyle( __EGBS_SFB_FULL_ROWS, 0, true );
	//-- Non Auto Sizing
	ReportColumnProportionalResizingSet(FALSE);
	//-- Non Editable
	BseModifyStyle( 0, __ENABLE_CELLEDITING_BSE_STYLES__ );
	//-- UI 
	SiwModifyStyleEx(__EGWS_EX_PM_COLORS, 0, false );
	//-- 2003 Style
	ReportGridModifyStyle(__ERGS_LAYOUT_AND_STYLE_2003,0,false);
	//-- Bold Group
	ReportGridModifyStyle(__ERGS_BOLD_GROUPS,0,false);
	//-- Line : Horizontal (O)/ Vertical (X)
	SiwModifyStyle( __EGBS_GRIDLINES_H, 0, true );
	SiwModifyStyle(0,__EGBS_GRIDLINES_V,true);
	//-- Hide All Buttons
	BseModifyStyle(0,__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS,false);

	//-- 2012-07-24 hongsu@esmlab.com
	//-- Init Grid Column 
	InitGridColumn();

	//-- 2013-04-23 hongsu.jung
	//-- Create DSC Mgr Thread
	InitDSCMgr();
}

void C4DMakerMainGrid::ShowGroupArea()		{ OnRgShowGroupArea();		}
void C4DMakerMainGrid::ShowColumnChooser()	{ OnRgShowColumnChooser();	}
void C4DMakerMainGrid::InitGridColumn()
{
	CRect rcOutput( 0, 0, 0, 0 );
	//-- 2013-04-22 hongsu.jung
	//-- Disable Group Box for Filterling
	ReportGroupAreaShow(false);
	ReportAutoPreviewModeSet( true, false );

	m_nBoldFontIndex = GridFontInsert( g_PaintManager->m_FontBold, -1, true );
	CExtReportGridColumn * pRGC = NULL;

	// Unique ID
	pRGC = ReportColumnRegister( g_arDSCColumn[DSC_COLUMN_UNIQUE], _T("Information"), true, false );
	ASSERT_VALID(pRGC);
	pRGC->ExtentSet(160);
	m_listColumns.AddTail( pRGC );

	// Status
	pRGC = ReportColumnRegister( g_arDSCColumn[DSC_COLUMN_STATUS],	_T("Information"), true, false );
	ASSERT_VALID(pRGC);
	pRGC->ExtentSet(100);
	m_listColumns.AddTail( pRGC );

	// Interval
	pRGC = ReportColumnRegister( g_arDSCColumn[DSC_COLUMN_INTERVAL], _T("Information"), true, false );
	ASSERT_VALID(pRGC);
	pRGC->ExtentSet(60);
	m_listColumns.AddTail( pRGC );

	// Adjust X
	pRGC = ReportColumnRegister( g_arDSCColumn[DSC_COLUMN_ADJUST_X], _T("Information"), true, false );
	ASSERT_VALID(pRGC);
	pRGC->ExtentSet(60);
	m_listColumns.AddTail( pRGC );

	
	// Adjust Y
	pRGC = ReportColumnRegister( g_arDSCColumn[DSC_COLUMN_ADJUST_Y], _T("Information"), true, false );
	ASSERT_VALID(pRGC);
	pRGC->ExtentSet(60);
	m_listColumns.AddTail( pRGC );

	// Picture
	pRGC = ReportColumnRegister( g_arDSCColumn[DSC_COLUMN_PICTURE], _T("Information"), true, false );
	ASSERT_VALID(pRGC);
	pRGC->ExtentSet(200);
	m_listColumns.AddTail( pRGC );
}

BOOL C4DMakerMainGrid::ClearGrid()
{
	//-- Disconnect All DSC
	DisConnect();
	//-- 2012-07-24 hongsu@esmlab.com
	//-- Remove Grid 
	OnEditClearAll();
	return TRUE;
}

void C4DMakerMainGrid::DisConnect()
{

}