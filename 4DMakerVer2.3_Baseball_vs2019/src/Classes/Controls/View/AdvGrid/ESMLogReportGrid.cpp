////////////////////////////////////////////////////////////////////////////////
//
//	TGLogReportGrid.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-24
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TGLogReportGrid.h"
//-- 2012-07-24 hongsu@esmlab.com
//-- Get Received Buffer
#include "HTTPBase.h"
//-- 2012-07-24 hongsu@esmlab.com
//-- Compare 
#include "TGCompare.h"



CTGLogReportGrid::CTGLogReportGrid(void){}
CTGLogReportGrid::~CTGLogReportGrid(void)
{
	ClearGrid();
}

BEGIN_MESSAGE_MAP(CTGLogReportGrid, CTGAdvGrid)	
END_MESSAGE_MAP()


void CTGLogReportGrid::InitReport(void)
{
	//-- Design up to the MainFrame Paint
	//-> g_PaintManager.InstallPaintManager(RUNTIME_CLASS(CExtPaintManagerOffice2007_R1));

	//-- 2012-07-24 hongsu@esmlab.com
	//-- Set Chooser Left 
	ReportColumnChooserGet()->OrientationSet(AFX_IDW_DOCKBAR_LEFT);
	//-- Previews Mode
	ReportAutoPreviewModeSet(TRUE);
	//-- Non Multi Selection
	SiwModifyStyle( 0, __EGBS_SFM_ROWS|__EGBS_MULTI_AREA_SELECTION, false );
	SelectionUnset();
	//-- Non Select Mask
	SiwModifyStyle( 0, __EGBS_SFB_MASK, false );
	SiwModifyStyle( __EGBS_SFB_FULL_ROWS, 0, true );
	//-- Non Auto Sizing
	ReportColumnProportionalResizingSet(FALSE);
	//-- Non Editable
	BseModifyStyle( 0, __ENABLE_CELLEDITING_BSE_STYLES__ );
	//-- UI 
	SiwModifyStyleEx(__EGWS_EX_PM_COLORS, 0, false );
	//-- 2003 Style
	ReportGridModifyStyle(__ERGS_LAYOUT_AND_STYLE_2003,0,false);
	//-- Bold Group
	ReportGridModifyStyle(__ERGS_BOLD_GROUPS,0,false);
	//-- Line : Horizontal (O)/ Vertical (X)
	SiwModifyStyle( __EGBS_GRIDLINES_H, 0, true );
	SiwModifyStyle(0,__EGBS_GRIDLINES_V,true);
	//-- Hide All Buttons
	BseModifyStyle(0,__EGWS_BSE_BUTTONS_PERSISTENT|__EGWS_BSE_BUTTONS_IN_FOCUSED_ROW|__EGWS_BSE_BUTTONS_IN_SELECTED_CELLS,false);

	//-- 2012-07-24 hongsu@esmlab.com
	//-- Init Grid Column 
	InitGridColumn();
}

void CTGLogReportGrid::ShowGroupArea()		{ OnRgShowGroupArea();		}
void CTGLogReportGrid::ShowColumnChooser()	{ OnRgShowColumnChooser();	}
void CTGLogReportGrid::InitGridColumn()
{
	CRect rcOutput( 0, 0, 0, 0 );
	ReportGroupAreaShow();
	ReportAutoPreviewModeSet( true, false );

	m_nBoldFontIndex = GridFontInsert( g_PaintManager->m_FontBold, -1, true );
	CExtReportGridColumn * pRGC = NULL;

	// Time
	pRGC = ReportColumnRegister( g_arLogReportColumn[LOG_REPORT_TIME], _T("Information"), true, false );
	ASSERT_VALID(pRGC);
	pRGC->ExtentSet(160);
	m_listColumns.AddTail( pRGC );

	// IP
	pRGC = ReportColumnRegister( g_arLogReportColumn[LOG_REPORT_IP],	_T("Information"), true, false );
	ASSERT_VALID(pRGC);
	pRGC->ExtentSet(100);
	m_listColumns.AddTail( pRGC );

	// Key
	pRGC = ReportColumnRegister( g_arLogReportColumn[LOG_REPORT_KEY], _T("Information"), true, false );
	ASSERT_VALID(pRGC);
	pRGC->ExtentSet(150);
	m_listColumns.AddTail( pRGC );

	// Contents
	pRGC = ReportColumnRegister( g_arLogReportColumn[LOG_REPORT_CONTENT], _T("Information"), true, false );
	ASSERT_VALID(pRGC);
	pRGC->ExtentSet(500);
	m_listColumns.AddTail( pRGC );
}

BOOL CTGLogReportGrid::ClearGrid()
{
	//-- 2012-07-24 hongsu@esmlab.com
	//-- Remove Line Array 
	RemoveLine();

	//-- 2012-07-24 hongsu@esmlab.com
	//-- Remove Grid 
	OnEditClearAll();
	return TRUE;
}

int CTGLogReportGrid::GetLineCount()
{
	return m_arLine.GetSize();
}

CLogReport* CTGLogReportGrid::GetLogRoport(int nIndex)
{
	return (CLogReport*)m_arLine.GetAt(nIndex);
}

void CTGLogReportGrid::AddLogLine(CLogReport* pLogReport)
{
	m_arLine.Add((CObject*)pLogReport);
}

void CTGLogReportGrid::RemoveLine()
{
	CLogReport* pLog = NULL;
	int nAll = GetLineCount();
	while(nAll--)
	{
		pLog = GetLogRoport(nAll);
		if(pLog)
		{
			delete pLog;
			pLog = NULL;
		}
		m_arLine.RemoveAt(nAll);
	}
	m_arLine.RemoveAll();
}


//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2012-07-24
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
BOOL CTGLogReportGrid::AddLog(CObject* pReceiveObject)
{
	CHTTPBase* pHTTP = (CHTTPBase*)pReceiveObject;
	if(!pHTTP)
		return FALSE;

	CString strIP = pHTTP->GetIPAddress();
	if(!GetLog(strIP, pHTTP->GetReceiveBuffer()))
		return FALSE;
	
	return AddLogLines();
}

BOOL CTGLogReportGrid::GetLog(CString strIP, CString strLog)
{
	//-- 2012-07-24 hongsu@esmlab.com
	//-- parsing get Line
	CString strLine;
	CString strToken, strDes;
	BOOL bRet = TRUE;
	int nPos = 0;

	//-- Set Destination
	strDes = strLog;
	while(strDes != _T(""))
	{
		nPos = strDes.Find(_T('\n'));
		if(nPos == -1)
		{
			if(strDes.GetLength())
				if(!AddLogLine(strIP, strDes))
					bRet = FALSE;
			break;
		}

		//-- Send the left string
		strToken = strDes.Left(nPos-1);
		if(!AddLogLine(strIP, strToken))
			bRet = FALSE;	
		strDes = strDes.Mid(nPos+1);
	}
	return bRet;
}

BOOL CTGLogReportGrid::AddLogLine(CString strIP, CString strLine)
{
	int nPos = 0;
	CLogReport* pLogReport = NULL;
	pLogReport = new CLogReport();
	if(!pLogReport)
		return FALSE;

	TGLog(5,_T("Add Line:[%s] %s"),strIP,strLine);
	//-- 2012-07-24 hongsu@esmlab.com
	//-- parsing parameter
	pLogReport->m_strIP = strIP;
	
	nPos = strLine.Find(_T(']'));
	if(nPos == -1)
	{
		delete pLogReport;
		return FALSE;	
	}

	//-- Get Time
	pLogReport->m_strTime = strLine.Left(nPos+1);
	strLine = strLine.Mid(nPos+2);

	nPos = strLine.Find(_T(']'));
	if(nPos == -1)
	{
		pLogReport->m_strKey		= strLine;
		pLogReport->m_strContent	= _T("");
	}
	else
	{
		pLogReport->m_strKey		= strLine.Left(nPos+1);
		pLogReport->m_strContent	= strLine.Mid(nPos+2);
	}

	AddLogLine(pLogReport);
	return TRUE;
}

BOOL CTGLogReportGrid::AddLogLines()
{
	CExtReportGridColumn* pRGC	= NULL;
	CExtReportGridItem	* pRGI	= NULL;
	CExtGridCell		* pCell = NULL;
	CLogReport			* pLogReport = NULL;
	POSITION pos = NULL;
	LONG nRow, nRowCount = GetLineCount();
	if(!nRowCount)
		return FALSE;

	//-- 2012-07-24 hongsu@esmlab.com
	//-- Add Report Line 
	TGLog(5,_T("AddLogLines [%d]"),nRowCount);

	CTypedPtrArray < CPtrArray, CExtReportGridItem * > arrRegisteredItems;
	arrRegisteredItems.SetSize( nRowCount );
	VERIFY( ReportItemRegister( arrRegisteredItems ) );
	for( nRow = 0; nRow < nRowCount; nRow ++ )
	{
		pLogReport = GetLogRoport(nRow);		
		pRGI = arrRegisteredItems[nRow];
		ASSERT_VALID( pRGI );

		pos = m_listColumns.GetHeadPosition();
		USES_CONVERSION;

		// Time
		pRGC = m_listColumns.GetNext( pos );
		pCell = ReportItemGetCell( pRGC, pRGI, RUNTIME_CLASS(CExtGridCellStringDM) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(pLogReport->m_strTime) );

		// IP Address
		pRGC = m_listColumns.GetNext( pos );
		pCell = ReportItemGetCell( pRGC, pRGI, RUNTIME_CLASS(CExtGridCellStringDM) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(pLogReport->m_strIP) );

		// Key
		pRGC = m_listColumns.GetNext( pos );
		pCell = ReportItemGetCell( pRGC, pRGI, RUNTIME_CLASS(CExtGridCellStringDM) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		//-- 2012-07-24 hongsu@esmlab.com
		//-- TEST
		if(!pLogReport->m_strKey.GetLength())
			pCell->TextSet( OLE2CT(pLogReport->m_strContent) );
		pCell->TextSet( OLE2CT(pLogReport->m_strKey) );

		// Contents
		pRGC = m_listColumns.GetNext( pos );
		pCell = ReportItemGetCell( pRGC, pRGI, RUNTIME_CLASS(CExtGridCellStringDM) );
		ASSERT_VALID( pCell );
		pCell->ModifyStyle( __EGCS_TEXT_ELLIPSIS );
		pCell->ModifyStyleEx( __EGCS_EX_NO_INPLACE_CONTROL_BORDER );
		pCell->TextSet( OLE2CT(pLogReport->m_strContent) );
	} 

	VERIFY( ReportSortOrderUpdate() );

	//-- 2012-07-24 hongsu@esmlab.com
	//-- Remove Information
	RemoveLine();
	return TRUE;
}

BOOL CTGLogReportGrid::LogCheck(CObject* pCompare)
{
	BOOL bRet = TRUE;
	CTGCompare* pComp = (CTGCompare*)pCompare;
	if(!pComp)
		return FALSE;

	//-- 2012-07-24 hongsu@esmlab.com
	//-- Find Column
	CString strExist;
	CExtGridCell* pCell = NULL;
	LONG nGetCol = GetDestColumn(pComp->m_strDest);
	LONG nRowCount = RowCountGet();
	LONG nColCount = ColumnCountGet();
	BOOL bCheckIP = GetCheckGroup(pComp->m_strGroup);
	LONG nSumRow = 0;

	for(int nRow = 0; nRow < nRowCount ; nRow ++ )
	{
		if( nGetCol != TG_UNKNOWN)
		{
			pCell = GridCellGet(nGetCol,nRow);
			if(!pCell)
				continue;
			pCell->TextGet(strExist);
			//-- Compare 
			if(pComp->DoCompare(strExist))
			{
				CLogReport report;
				pCell = GridCellGet(LOG_REPORT_TIME		,nRow);	pCell->TextGet(report.m_strTime		);
				pCell = GridCellGet(LOG_REPORT_IP		,nRow);	pCell->TextGet(report.m_strIP		);
				pCell = GridCellGet(LOG_REPORT_KEY		,nRow);	pCell->TextGet(report.m_strKey		);
				pCell = GridCellGet(LOG_REPORT_CONTENT	,nRow);	pCell->TextGet(report.m_strContent	);

				ShowInfo(&report);	

				for(int nCol = 0; nCol < nColCount; nCol ++)
				{
					pCell = GridCellGet(nCol,nRow);
					GridCellSet( nCol, nSumRow, pCell );
					pCell->Empty();
				}
				nSumRow++;
			}
			else
			{
				for(int nCol = 0; nCol < nColCount; nCol ++)
				{
					pCell = GridCellGet(nCol,nRow);
					pCell->Empty();
				}
			}
		}
		
	/*	if(bCheckIP)
		{
			//-----------------------------------
			//-- IP Address
			pCell = GridCellGet(1,nRow);
			if(!pCell)
				continue;
			pCell->TextGet(strExist);
			//-- Check IP Address 
			if(pComp->m_strGroup != strExist)
				continue;
		}

		//-----------------------------------
		//-- Check
		//-- 2012-09-10 joonho.kim
		if( nCol == TG_UNKNOWN)
			nCol = 0;
		pCell = GridCellGet(nCol,nRow);
		if(!pCell)
			continue;
		pCell->TextGet(strExist);
		//-- Compare 
		if(pComp->DoCompare(strExist))
		{
			CLogReport report;
			pCell = GridCellGet(LOG_REPORT_TIME		,nRow);	pCell->TextGet(report.m_strTime		);
			pCell = GridCellGet(LOG_REPORT_IP		,nRow);	pCell->TextGet(report.m_strIP		);
			pCell = GridCellGet(LOG_REPORT_KEY		,nRow);	pCell->TextGet(report.m_strKey		);
			pCell = GridCellGet(LOG_REPORT_CONTENT	,nRow);	pCell->TextGet(report.m_strContent	);

			ShowInfo(&report);	
			return pComp->DoReturn(); 
		}*/
	}
	Invalidate();
	return bRet;
}

int CTGLogReportGrid::GetDestColumn(CString strDest)
{
	for (int i = 0 ; i < ARRAY_SIZE(g_arLogReportColumn) ; i ++)
	{
		if(g_arLogReportColumn[i] == strDest)
			return i;
	}
	return TG_UNKNOWN;
}

BOOL CTGLogReportGrid::GetCheckGroup(CString strGroup)
{
	strGroup.MakeUpper();
	if(strGroup == DEF_STR_ALL)
		return FALSE;
	return TRUE;
}

void CTGLogReportGrid::ShowInfo(CLogReport* pReport)
{
	TGLog(1,_T("[Check Detection] Time:%s"	),pReport->m_strTime	);
	TGLog(1,_T("[Check Detection] IP:%s"		),pReport->m_strIP		);
	TGLog(1,_T("[Check Detection] Key:%s"		),pReport->m_strKey		);
	TGLog(1,_T("[Check Detection] Content:%s"	),pReport->m_strContent	);
}