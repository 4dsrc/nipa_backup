// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_EDIT_H)
	#include <ExtEdit.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include <../Src/ExtMemoryDC.h>
#endif

#if (!defined __EXT_POPUP_MENU_WND_H)
	#include <ExtPopupMenuWnd.h>
#endif

#if (!defined __EXT_TOOLCONTROLBAR_H)
	#include <ExtToolControlBar.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE( CExtEditBase, CEdit );
IMPLEMENT_CExtPmBridge_MEMBERS( CExtEditBase );

CExtEditBase::CExtEditBase()
	: m_bHandleCtxMenus( true )
	, m_clrBack( COLORREF(-1L) )
	, m_clrBackPrev( COLORREF(-1L) )
	, m_clrText( COLORREF(-1L) )
{
	EnableToolTips();

	PmBridge_Install();
}

CExtEditBase::~CExtEditBase()
{
	PmBridge_Uninstall();

	if( m_brBack.GetSafeHandle() != NULL )
		m_brBack.DeleteObject();
}

BEGIN_MESSAGE_MAP( CExtEditBase, CEdit )
	//{{AFX_MSG_MAP(CExtEditBase)
	ON_WM_CONTEXTMENU()
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CExtEditBase::OnContextMenu(CWnd* pWnd,CPoint pos )
{
	if( ! m_bHandleCtxMenus )
	{
		CEdit::OnContextMenu( pWnd, pos );
		return;
	} // if( ! m_bHandleCtxMenus )
LPCTSTR strProfileName =
		g_CmdManager->ProfileNameFromWnd( GetSafeHwnd() );
	if( strProfileName == NULL ){
		CEdit::OnContextMenu( pWnd, pos );
		return;
	}
CExtPopupMenuWnd * pPopup = new CExtPopupMenuWnd;
	if( !pPopup->CreatePopupMenu( GetSafeHwnd() ) )
	{
		ASSERT( FALSE );
		delete pPopup;
		CEdit::OnContextMenu( pWnd, pos );
		return;
	}
static struct
{
	UINT m_nCmdID;
	LPCTSTR m_sMenuText;
} arrCmds[] =
{
	{ ID_EDIT_UNDO, _T("&Undo") },
//	{ ID_EDIT_REDO, _T("&Redo") },
	{ ID_SEPARATOR, NULL },
	{ ID_EDIT_CUT, _T("Cu&t") },
	{ ID_EDIT_COPY, _T("&Copy") },
	{ ID_EDIT_PASTE, _T("&Paste") },
	{ ID_EDIT_CLEAR, _T("Cl&ear") },
	{ ID_SEPARATOR, NULL },
	{ ID_EDIT_SELECT_ALL, _T("Select &All") },
};
	for( int i = 0; i < sizeof(arrCmds)/sizeof(arrCmds[0]); i++ )
	{
		if( arrCmds[i].m_nCmdID == ID_SEPARATOR )
		{
			VERIFY( pPopup->ItemInsert( ID_SEPARATOR ) );
			continue;
		} // if( arrCmds[i].m_nCmdID == ID_SEPARATOR )
		CExtCmdItem * pCmdItem =
			g_CmdManager->CmdGetPtr(
				strProfileName,
				arrCmds[i].m_nCmdID
				);
		if( pCmdItem == NULL )
			pCmdItem =
				g_CmdManager->CmdAllocPtr(
					strProfileName,
					arrCmds[i].m_nCmdID
					);
		ASSERT( pCmdItem != NULL );
		if( pCmdItem == NULL )
			continue;
		if( pCmdItem->m_sMenuText.IsEmpty() )
		{
			CExtSafeString sText;
			if( g_ResourceManager->LoadString( sText, arrCmds[i].m_nCmdID ) )
			{
				sText.Replace( _T("\t"), _T(" ") );
				sText.Replace( _T("\r"), _T("") );
				sText.TrimLeft();
				sText.TrimRight();
				if( ! sText.IsEmpty() )
				{
					int nSep = sText.ReverseFind('\n');
					if( nSep < 0 )
					{
						pCmdItem->m_sMenuText = sText;
					} // if( nSep < 0 )
					else
					{
						int nLen = sText.GetLength();
						pCmdItem->m_sMenuText = sText.Right( nLen - nSep );
						pCmdItem->m_sMenuText.TrimLeft();
						pCmdItem->m_sMenuText.TrimRight();
					} // else from if( nSep < 0 )
				} // if( ! sText.IsEmpty() )
			}
			if( pCmdItem->m_sMenuText.IsEmpty() )
				pCmdItem->m_sMenuText = arrCmds[i].m_sMenuText;
		} // if( pCmdItem->m_sMenuText.IsEmpty() )
		pCmdItem->StateSetBasic();
		VERIFY( pPopup->ItemInsert( arrCmds[i].m_nCmdID ) );
	} // for( int i = 0; i < sizeof(arrCmds)/sizeof(arrCmds[0]); i++ )
CRect rcClient;
	GetClientRect( &rcClient );
	ClientToScreen( &rcClient );
	if( !rcClient.PtInRect( pos ) )
		pos = rcClient.CenterPoint();
	SetFocus();
	
HWND hWndOwn = m_hWnd;
	if(	! pPopup->TrackPopupMenu(
			TPMX_OWNERDRAW_FIXED|TPMX_DO_MESSAGE_LOOP,
			pos.x, 
			pos.y,
			NULL,
			this,
			NULL,
			NULL,
			true
			) 
		)
	{
//		ASSERT( FALSE );
		delete pPopup;
		if( ! ::IsWindow( hWndOwn ) )
			return;
		CEdit::OnContextMenu( pWnd, pos );
		return;
	}
	else
	{
		if( ! ::IsWindow( hWndOwn ) )
			return;
		VERIFY(
			RedrawWindow(
				NULL,
				NULL,
				RDW_INVALIDATE|RDW_UPDATENOW|RDW_ERASE|RDW_ERASENOW
				|RDW_FRAME|RDW_ALLCHILDREN
				)
			);
	}
}

__EXT_MFC_INT_PTR CExtEditBase::OnToolHitTest(
	CPoint point,
	TOOLINFO * pTI
	) const
{
	__PROF_UIS_MANAGE_STATE;
	__EXT_MFC_IMPLEMENT_TT_REDIR_OnToolHitTest( CExtEditBase, CExtToolControlBar );
	return CEdit::OnToolHitTest( point, pTI );
}

void CExtEditBase::InitToolTip()
{
	if( m_wndToolTip.m_hWnd == NULL )
	{
		m_wndToolTip.Create(this);
		m_wndToolTip.Activate(FALSE);
	}
}

int CExtEditBase::OnQueryMaxTipWidth( 
	__EXT_MFC_SAFE_LPCTSTR lpszText 
	)
{
	lpszText;
	return 250;
}

void CExtEditBase::ActivateTooltip(BOOL bActivate)
{
	if( m_wndToolTip.GetToolCount() == 0 )
		return;
	m_wndToolTip.Activate(bActivate);
}

void CExtEditBase::SetTooltipText(
	CExtSafeString * spText,
	BOOL bActivate // = TRUE
	)
{
	if( spText == NULL )
		return;
	InitToolTip();
	if( m_wndToolTip.GetToolCount() == 0 )
	{
		CRect rectBtn; 
		GetClientRect(rectBtn);
		m_wndToolTip.AddTool( this, *spText, rectBtn, 1 );
	}
	CWnd::CancelToolTips();
	m_wndToolTip.UpdateTipText( *spText, this, 1 );
	m_wndToolTip.SetMaxTipWidth( -1 );
	if( spText->Find( _T("\r") ) >= 0 )
		m_wndToolTip.SetMaxTipWidth( OnQueryMaxTipWidth( *spText ) );
	m_wndToolTip.Activate(bActivate);
}

void CExtEditBase::SetTooltipText(
	CExtSafeString & sText,
	BOOL bActivate // = TRUE
	)
{
	if( sText.IsEmpty() )
		return;
	InitToolTip();
	if( m_wndToolTip.GetToolCount() == 0 )
	{
		CRect rectBtn; 
		GetClientRect(rectBtn);
		m_wndToolTip.AddTool( this, sText, rectBtn, 1 );
	}
	CWnd::CancelToolTips();
	m_wndToolTip.UpdateTipText( sText, this, 1 );
	m_wndToolTip.SetMaxTipWidth( -1 );
	if( sText.Find( _T("\r") ) >= 0 )
		m_wndToolTip.SetMaxTipWidth( OnQueryMaxTipWidth( sText ) );
	m_wndToolTip.Activate( bActivate );
}

void CExtEditBase::SetTooltipText(
	__EXT_MFC_SAFE_LPCTSTR sText,
	BOOL bActivate // = TRUE
	)
{
	if(		sText == NULL
		|| _tcslen( sText ) == 0
		)
		return;
	InitToolTip();
	if( m_wndToolTip.GetToolCount() == 0 )
	{
		CRect rectBtn; 
		GetClientRect(rectBtn);
		m_wndToolTip.AddTool(this,sText,rectBtn,1);
	}
	CWnd::CancelToolTips();
	m_wndToolTip.UpdateTipText(sText,this,1);
	m_wndToolTip.SetMaxTipWidth( -1 );
	CString strText( sText );
	if( strText.Find( _T("\r") ) >= 0 )
		m_wndToolTip.SetMaxTipWidth( OnQueryMaxTipWidth( sText ) );
	m_wndToolTip.Activate(bActivate);
}

void CExtEditBase::SetTooltipText(
	int nId,
	BOOL bActivate // = TRUE
	)
{
CExtSafeString sText;
	g_ResourceManager->LoadString( sText, nId );
	if( ! sText.IsEmpty() )
		SetTooltipText( &sText, bActivate );
}

LRESULT CExtEditBase::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if(		message == WM_NOTIFY
		&&	m_wndToolTip.GetSafeHwnd() != NULL
		&&	IsWindow( m_wndToolTip.GetSafeHwnd() )
		&&	((LPNMHDR)lParam) != NULL
		&&	((LPNMHDR)lParam)->hwndFrom == m_wndToolTip.GetSafeHwnd()
		&&	((LPNMHDR)lParam)->code == TTN_SHOW
		)
		::SetWindowPos(
			m_wndToolTip.GetSafeHwnd(),
			HWND_TOP,
			0,0,0,0,
			SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE
			);
LRESULT lResult =
		CEdit::WindowProc( message, wParam, lParam );
	return lResult;
}

BOOL CExtEditBase::PreTranslateMessage( MSG * pMsg )
{
	if( !CExtPopupMenuWnd::IsMenuTracking() )
	{
		InitToolTip();
		m_wndToolTip.RelayEvent(pMsg);
	}

	return CEdit::PreTranslateMessage( pMsg );
}

BOOL CExtEditBase::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
	if(		m_bHandleCtxMenus
		&&	(	nCode == CN_UPDATE_COMMAND_UI
			||	nCode == CN_COMMAND
			)
		)
	{
		bool bReadOnly = ( (GetStyle() & ES_READONLY) != 0 ) ? true : false;
		DWORD nSelection = GetSel();
		if( nID == ID_EDIT_UNDO )
		{
			if( nCode == CN_UPDATE_COMMAND_UI )
			{
				CCmdUI * pCmdUI = (CCmdUI *)pExtra;
				ASSERT( pCmdUI != NULL );
				pCmdUI->Enable( (CanUndo() && (!bReadOnly)) ? TRUE : FALSE );
			} // if( nCode == CN_UPDATE_COMMAND_UI )
			else
				Undo();
			return TRUE;
		} // if( nID == ID_EDIT_UNDO )

//		if( nID == ID_EDIT_REDO )
//		{
//			if( nCode == CN_UPDATE_COMMAND_UI )
//			{
//				CCmdUI * pCmdUI = (CCmdUI *)pExtra;
//				ASSERT( pCmdUI != NULL );
//				pCmdUI->Enable(
//					(BOOL) (SendMessage( EM_CANREDO ) && (!bReadOnly)
//					);
//			} // if( nCode == CN_UPDATE_COMMAND_UI )
//			else
//				SendMessage( EM_REDO );
//			return TRUE;
//		} // if( nID == ID_EDIT_REDO )
		if( nID == ID_EDIT_CUT )
		{
			if( nCode == CN_UPDATE_COMMAND_UI )
			{
				CCmdUI * pCmdUI = (CCmdUI *)pExtra;
				ASSERT( pCmdUI != NULL );
				pCmdUI->Enable(
						(LOWORD(nSelection)) != (HIWORD(nSelection))
					&&	(!bReadOnly)
					);
			} // if( nCode == CN_UPDATE_COMMAND_UI )
			else
				SendMessage( WM_CUT );
			return TRUE;
		} // if( nID == ID_EDIT_CUT )
		if( nID == ID_EDIT_COPY )
		{
			if( nCode == CN_UPDATE_COMMAND_UI )
			{
				CCmdUI * pCmdUI = (CCmdUI *)pExtra;
				ASSERT( pCmdUI != NULL );
				pCmdUI->Enable(
					(LOWORD(nSelection)) != (HIWORD(nSelection))
					);
			} // if( nCode == CN_UPDATE_COMMAND_UI )
			else
				SendMessage( WM_COPY );
			return TRUE;
		} // if( nID == ID_EDIT_COPY )
		if( nID == ID_EDIT_PASTE )
		{
			if( nCode == CN_UPDATE_COMMAND_UI )
			{
				CCmdUI * pCmdUI = (CCmdUI *)pExtra;
				ASSERT( pCmdUI != NULL );
				pCmdUI->Enable(
						IsClipboardFormatAvailable(CF_TEXT) 
					&&	(!bReadOnly)
					);
			} // if( nCode == CN_UPDATE_COMMAND_UI )
			else
				SendMessage( WM_PASTE );
			return TRUE;
		} // if( nID == ID_EDIT_PASTE )
		if( nID == ID_EDIT_CLEAR )
		{
			if( nCode == CN_UPDATE_COMMAND_UI )
			{
				CCmdUI * pCmdUI = (CCmdUI *)pExtra;
				ASSERT( pCmdUI != NULL );
				pCmdUI->Enable(
						(LOWORD(nSelection)) != (HIWORD(nSelection))
					&&	(!bReadOnly)
					);
			} // if( nCode == CN_UPDATE_COMMAND_UI )
			else
				SendMessage( WM_CLEAR );
			return TRUE;
		} // if( nID == ID_EDIT_CLEAR )
		if( nID == ID_EDIT_SELECT_ALL )
		{
			if( nCode == CN_UPDATE_COMMAND_UI )
			{
				CCmdUI * pCmdUI = (CCmdUI *)pExtra;
				ASSERT( pCmdUI != NULL );
				
				int nLength = GetWindowTextLength();
				pCmdUI->Enable(
						nLength > 0
					&&	(!(		(LOWORD(nSelection)) == 0 
							&&	(HIWORD(nSelection)) == nLength
						))
					);
			} // if( nCode == CN_UPDATE_COMMAND_UI )
			else
				SetSel( 0, -1 );
			return TRUE;
		} // if( nID == ID_EDIT_SELECT_ALL )
	} // if( m_bHandleCtxMenus ...
	return CEdit::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

HBRUSH CExtEditBase::CtlColor( CDC* pDC, UINT nCtlColor )
{
	ASSERT_VALID( this );
	if(		nCtlColor == CTLCOLOR_EDIT 
		||	nCtlColor == CTLCOLOR_MSGBOX
		||	nCtlColor == CTLCOLOR_STATIC
		)
	{
		bool bReadOnly = (GetStyle()&ES_READONLY) != 0;
		bool bDisabled = OnQueryWindowEnabledState() ? false : true;
		COLORREF clrText = GetTextColor();
		if( clrText == COLORREF(-1L) )
		{
			COLORREF clrSysText =
				PmBridge_GetPM()->GetColor(
					bDisabled 
						? COLOR_GRAYTEXT 
						: COLOR_WINDOWTEXT,
					this
					);
			clrText = clrSysText;
		}
		COLORREF clrBk = GetBkColor();
		if( clrBk == COLORREF(-1L) )
		{
			COLORREF clrSysBk =
				PmBridge_GetPM()->GetColor( 
					( bReadOnly || bDisabled ) 
						? COLOR_3DFACE 
						: COLOR_WINDOW,
					this 
					);
			clrBk = clrSysBk;
		}
		pDC->SetBkColor( clrBk );
		pDC->SetTextColor( clrText );
		if( m_clrBackPrev != clrBk )
		{
			if( m_brBack.GetSafeHandle() != NULL )
				m_brBack.DeleteObject();
			m_brBack.CreateSolidBrush( clrBk );
			m_clrBackPrev = clrBk;
		}
		return m_brBack;
	}
	else
		return (HBRUSH)Default();
}

/////////////////////////////////////////////////////////////////////////////
// CExtEdit

#define UPDATE_TIMER		1
#define UPDATE_TIMER_PERIOD	10

IMPLEMENT_DYNCREATE( CExtEdit, CExtEditBase );

CExtEdit::CExtEdit()
	: m_bMouseOver( false )
{
}

CExtEdit::~CExtEdit()
{
}

BEGIN_MESSAGE_MAP(CExtEdit, CExtEditBase)
	//{{AFX_MSG_MAP(CExtEdit)
	ON_WM_TIMER()
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_WM_NCPAINT()
	ON_WM_SETCURSOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtEdit message handlers

BOOL CExtEdit::OnSetCursor( CWnd * pWnd, UINT nHitTest, UINT message )
{
	if( message == WM_MOUSEMOVE )
	{
		CRect rcWnd;
		GetWindowRect( &rcWnd );
		if( rcWnd.PtInRect( GetCurrentMessage()->pt ) )
		{
			SetTimer( UPDATE_TIMER, UPDATE_TIMER_PERIOD, NULL );
			OnTimer( UPDATE_TIMER );
		}
	}
	return CExtEditBase::OnSetCursor(pWnd, nHitTest, message);
}

void CExtEdit::OnTimer( __EXT_MFC_UINT_PTR nIDEvent ) 
{
	if( nIDEvent != UPDATE_TIMER )
	{
		CExtEditBase::OnTimer( nIDEvent );
		return;
	}
POINT pt;
	if( ! ::GetCursorPos(&pt) )
		return;
	if( IsWindowVisible() )
	{
		bool bOldMouseOver = m_bMouseOver;
		CRect rectItem;
		GetWindowRect( &rectItem );
		if( !rectItem.PtInRect(pt) )
		{
			KillTimer( UPDATE_TIMER );
			m_bMouseOver = false;
		}
		else 
			if(	::WindowFromPoint( pt ) == m_hWnd )
			m_bMouseOver = true;
		if( bOldMouseOver != m_bMouseOver )
			_PostRedraw();
	} // if( IsWindowVisible() )
}

void CExtEdit::_PostRedraw()
{
	VERIFY(
		RedrawWindow(
			NULL,
			NULL,
			RDW_INVALIDATE | RDW_UPDATENOW
				| RDW_ERASE | RDW_ERASENOW
				| RDW_FRAME | RDW_ALLCHILDREN
			)
		);
}

void CExtEdit::_DrawEditImpl(
	CRect rectClient,
	CDC * pDC // = NULL
	)
{
bool bNeedReleaseDC = false;
	if( pDC == NULL )
	{
		pDC = GetDC();
		bNeedReleaseDC = true;
	}
	ASSERT_VALID( pDC );
bool bFocus = false;
	if(	GetFocus()->GetSafeHwnd() == m_hWnd
		&& (!CExtPopupMenuWnd::IsMenuTracking())
		)
		bFocus = true;

bool bEnabled = OnQueryWindowEnabledState();
bool bReadOnly = ( (GetStyle() & ES_READONLY) != 0 ) ? true : false;

CExtPaintManager::PAINTCONTROLFRAMEDATA _pcfd(
		this,
		rectClient,
		true,
		m_bMouseOver,
		bEnabled,
		bFocus,
		bReadOnly
		);
	PmBridge_GetPM()->PaintControlFrame( *pDC, _pcfd );

	if( bNeedReleaseDC )
		ReleaseDC(pDC);
}

void CExtEdit::OnSetFocus(CWnd* pOldWnd) 
{
	CExtEditBase::OnSetFocus(pOldWnd);
	_PostRedraw();
}

void CExtEdit::OnKillFocus(CWnd* pNewWnd) 
{
	CExtEditBase::OnKillFocus(pNewWnd);
	_PostRedraw();
}

void CExtEdit::OnNcPaint() 
{
	ASSERT_VALID( this );
	CWindowDC dc( this );
	_DoPaintNC( &dc );
}

void CExtEdit::_DoPaintNC( CDC * pDC )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC );
	ASSERT( pDC->GetSafeHdc() != NULL );

	CRect rcInBarWnd, rcInBarClient;
    GetWindowRect( &rcInBarWnd );
    GetClientRect( &rcInBarClient );
    ClientToScreen( &rcInBarClient );
    if( rcInBarWnd == rcInBarClient )
        return;

    CPoint ptDevOffset = -rcInBarWnd.TopLeft();
    rcInBarWnd.OffsetRect( ptDevOffset );
    rcInBarClient.OffsetRect( ptDevOffset );

    const INT cx = ::GetSystemMetrics(SM_CXVSCROLL);
    const INT cy = ::GetSystemMetrics(SM_CYHSCROLL);

	DWORD dwStyle = GetStyle();
    bool bHasVerticalSB = ( (dwStyle&WS_VSCROLL) != 0 ) ? true : false;
    bool bHasHorizontalSB = ( (dwStyle&WS_HSCROLL) != 0 ) ? true : false;
		
    if( bHasVerticalSB && bHasHorizontalSB )
    {
		INT nNcX = rcInBarWnd.right - rcInBarClient.right - cx;
		INT nNcY = rcInBarWnd.bottom - rcInBarClient.bottom - cy;
        pDC->FillSolidRect(
            rcInBarWnd.right - cx - nNcX,
            rcInBarWnd.bottom - cy - nNcY,
            cx,
            cy,
            PmBridge_GetPM()->GetColor( COLOR_WINDOW, this )
            );
    }

    CRect rcExclude( rcInBarClient );
    if( bHasVerticalSB )
        rcExclude.right += cx;
    if( bHasHorizontalSB )
        rcExclude.bottom += cy;
	
#ifndef __EXT_MFC_NO_SPINWND

	CWnd * pWndParent = GetParent();
	ASSERT_VALID( pWndParent );
	bool bBuddyWithSpin = false;
	bool bSpinAlignRight = false;
	bool bSpinAlignLeft = false;
	CWnd * pWnd = this->GetWindow( GW_HWNDNEXT );
	if( pWnd != NULL )
	{
		ASSERT_VALID( pWnd );
		static const TCHAR szSpin[] = _T("msctls_updown32");
		TCHAR szCompare[ sizeof(szSpin)/sizeof(szSpin[0]) + 1 ] = _T("");
		::GetClassName( 
			pWnd->GetSafeHwnd(), 
			szCompare, 
			sizeof(szCompare)/sizeof(szCompare[0]) 
			);
		if( !lstrcmpi( szCompare, szSpin ) )
		{
			DWORD dwStyle = pWnd->GetStyle();
			bSpinAlignRight = ( (dwStyle&UDS_ALIGNRIGHT) != 0 ) ? true : false;
			bSpinAlignLeft = ( (dwStyle&UDS_ALIGNLEFT) != 0 ) ? true : false;
			bBuddyWithSpin = 
				( (dwStyle&UDS_AUTOBUDDY) != 0 && (bSpinAlignRight || bSpinAlignLeft) ) ? true : false;
		}
	}
	if( bBuddyWithSpin )
	{
		CRect rcSpin;
		pWnd->GetClientRect( &rcSpin );
		if( bSpinAlignRight )
		{
			rcInBarWnd.right += rcSpin.Width();
			rcExclude.right += rcSpin.Width();
		}
		else if( bSpinAlignLeft )
		{
			rcInBarWnd.left -= rcSpin.Width();
			rcExclude.left -= rcSpin.Width();
		}
	}
	
#endif // __EXT_MFC_NO_SPINWND

	pDC->ExcludeClipRect( &rcExclude );

    if( bHasVerticalSB || bHasHorizontalSB )
		Default();
   
	bool bReadOnly = ( (GetStyle() & ES_READONLY) != 0 ) ? true : false;
	bool bDisabled = OnQueryWindowEnabledState() ? false : true;
	COLORREF clrSysBk =
		PmBridge_GetPM()->GetColor( 
			( bReadOnly || bDisabled ) 
				? COLOR_3DFACE 
				: COLOR_WINDOW,
			this
			);
//	2.54
// 	static const TCHAR szEditBox[] = _T("EDIT");
//     TCHAR szCompare[sizeof(szEditBox)/sizeof(szEditBox[0])+1];
//     ::GetClassName( 
//         GetSafeHwnd(), 
//         szCompare, 
//         sizeof(szCompare)/sizeof(szCompare[0]) 
// 		);
//     if( !lstrcmpi( szCompare, szEditBox ) )
//         pDC->FillSolidRect(
//             &rcInBarWnd, 
//             clrSysBk
//             );

	rcInBarWnd.DeflateRect(3,3);
	_DrawEditImpl( rcInBarWnd, pDC );
	pDC->SelectClipRgn( NULL );

#ifndef __EXT_MFC_NO_SPINWND

	if( bBuddyWithSpin )
	{
		CRect rcSpinWnd, rcSpinClient;
		pWnd->GetWindowRect( &rcSpinWnd );
		pWnd->GetClientRect( &rcSpinClient );
		pWnd->ClientToScreen( &rcSpinClient );

		CPoint ptDevOffset = -rcSpinWnd.TopLeft();
		rcSpinWnd.OffsetRect( ptDevOffset );
		rcSpinClient.OffsetRect( ptDevOffset );

		DWORD dwExStyle = ::GetWindowLong( pWnd->GetSafeHwnd(), GWL_EXSTYLE );
		bool bRTL = ( (dwExStyle & WS_EX_LAYOUTRTL) != 0 ) ? true : false;

		if( bRTL )
			rcSpinClient.OffsetRect( -3, 0 );

		CWindowDC dcSpin( pWnd );
		dcSpin.ExcludeClipRect( &rcSpinClient );

        dcSpin.FillSolidRect(
            &rcSpinWnd, 
            clrSysBk
            );

		if( bSpinAlignRight )
			rcSpinWnd.left -= rcInBarClient.Width();
		else if( bSpinAlignLeft )
			rcSpinWnd.right += rcInBarClient.Width();
		rcSpinWnd.DeflateRect(3,3);
		_DrawEditImpl( rcSpinWnd, &dcSpin );

		dcSpin.SelectClipRgn( NULL );
	}

#endif // __EXT_MFC_NO_SPINWND
}

LRESULT CExtEdit::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if(		message == WM_PRINT 
		||	message == WM_PRINTCLIENT
		)
	{
		CDC * pDC = CDC::FromHandle( (HDC) wParam );
		CRect rcWnd;
		GetWindowRect( &rcWnd );
		rcWnd.OffsetRect( -rcWnd.TopLeft() );
		CExtMemoryDC dc(
			pDC,
			&rcWnd
			);
		if( (lParam&PRF_NONCLIENT) != 0 )
			_DoPaintNC( &dc );
		if( (lParam&PRF_CHILDREN) != 0 )
			CExtPaintManager::stat_PrintChildren(
				m_hWnd,
				message,
				dc.GetSafeHdc(),
				lParam,
				false
				);
		return (!0);
	}
	return CExtEditBase::WindowProc(message, wParam, lParam);
}