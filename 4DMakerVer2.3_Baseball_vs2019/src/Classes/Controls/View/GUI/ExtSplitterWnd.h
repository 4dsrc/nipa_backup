// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#if (!defined __EXT_SPLITTER_WND_H)
#define __EXT_SPLITTER_WND_H

#if (!defined __EXT_MFC_NO_SPLITTER_WND)

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if (!defined __EXT_MFC_DEF_H)
	#include <ExtMfcDef.h>
#endif // __EXT_MFC_DEF_H

class __PROF_UIS_API CExtSplitterWnd
	: public CSplitterWnd
	, public CExtPmBridge
{
public:
	DECLARE_DYNCREATE( CExtSplitterWnd );
	DECLARE_CExtPmBridge_MEMBERS( CExtSplitterWnd );

	CExtSplitterWnd();
	virtual ~CExtSplitterWnd();

protected:
	virtual void DrawAllSplitBars(
		CDC * pDC,
		int cxInside,
		int cyInside
		);
	virtual void OnDrawSplitter(
		CDC* pDC,
		ESplitType nType,
		const CRect & rectArg
		);
	virtual void OnInvertTracker(
		const CRect & rect
		);
public:
	virtual BOOL CreateScrollBarCtrl( DWORD dwStyle, UINT nID );

	//{{AFX_VIRTUAL(CExtSplitterWnd)
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CExtSplitterWnd)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
}; // class CExtSplitterWnd

#endif // (!defined __EXT_MFC_NO_SPLITTER_WND)

#endif // !defined(__EXT_SPLITTER_WND_H)
