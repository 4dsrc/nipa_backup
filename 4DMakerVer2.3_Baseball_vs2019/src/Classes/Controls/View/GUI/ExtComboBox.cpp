// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_COMBO_BOX_H)
	#include <ExtComboBox.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include <../Src/ExtMemoryDC.h>
#endif

#if (!defined __EXT_POPUP_MENU_WND_H)
	#include <ExtPopupMenuWnd.h>
#endif

#if (!defined __EXT_TOOLCONTROLBAR_H)
	#include <ExtToolControlBar.h>
#endif

#if (!defined __AFXPRIV_H__)
	#include <AfxPriv.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//////////////////////////////////////////////////////////////////////////
// CExtComboBoxFilterPopupListBox
//////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC( CExtComboBoxFilterPopupListBox, CListBox );

BEGIN_MESSAGE_MAP(CExtComboBoxFilterPopupListBox, CListBox)
	//{{AFX_MSG_MAP(CExtComboBoxFilterPopupListBox)
	ON_WM_MOUSEACTIVATE()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_NCLBUTTONDOWN()
	ON_WM_SETCURSOR()
	ON_WM_CAPTURECHANGED()
	ON_WM_CANCELMODE()
	ON_WM_NCLBUTTONUP()
	ON_WM_NCCALCSIZE()
	ON_WM_NCPAINT()
	ON_WM_NCLBUTTONDBLCLK()
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
	ON_WM_ACTIVATEAPP()
	__EXT_MFC_ON_WM_NCHITTEST()
END_MESSAGE_MAP()

CExtComboBoxFilterPopupListBox::CExtComboBoxFilterPopupListBox(
	CExtComboBoxBase * pCB
	)
	: m_pCB( pCB )
	, m_hCursorSizeWE( NULL )
	, m_hCursorSizeNS( NULL )
	, m_hCursorSizeNWSE( NULL )
	, m_bResizingX( false )
	, m_bResizingY( false )
	, m_ptCursorPosLast( 0, 0 )
{
	ASSERT( m_pCB != NULL );
	m_hCursorSizeWE = ::LoadCursor( NULL, IDC_SIZEWE );
	ASSERT( m_hCursorSizeWE != NULL );
	m_hCursorSizeNS = ::LoadCursor( NULL, IDC_SIZENS );
	ASSERT( m_hCursorSizeNS != NULL );
	m_hCursorSizeNWSE = ::LoadCursor( NULL, IDC_SIZENWSE );
	ASSERT( m_hCursorSizeNWSE != NULL );
}

CExtComboBoxFilterPopupListBox::~CExtComboBoxFilterPopupListBox()
{
	if( m_hCursorSizeWE != NULL )
	{
		::DestroyCursor( m_hCursorSizeWE );
		m_hCursorSizeWE = NULL;
	}
	if( m_hCursorSizeNS != NULL )
	{
		::DestroyCursor( m_hCursorSizeNS );
		m_hCursorSizeNS = NULL;
	}
	if( m_hCursorSizeNWSE != NULL )
	{
		::DestroyCursor( m_hCursorSizeNWSE );
		m_hCursorSizeNWSE = NULL;
	}
}

void CExtComboBoxFilterPopupListBox::DrawItem( LPDRAWITEMSTRUCT lpDIS ) 
{
	ASSERT_VALID( this );
	ASSERT( lpDIS != NULL && lpDIS->hDC != NULL );

	if(		(GetStyle() & CBS_HASSTRINGS) == 0 
		&&	(GetStyle() & LBS_HASSTRINGS) == 0 
		)
		return;
	
	INT nItem = lpDIS->itemID;
	if( nItem < 0 || nItem >= GetCount() )
		return;

DWORD dwItemData = DWORD( GetItemData( nItem ) );
	if( dwItemData != 0 )
	{
		CExtComboBoxBase::LB_ITEM * pItemDataExt = (CExtComboBoxBase::LB_ITEM *) dwItemData;
		ASSERT( pItemDataExt != NULL );
		if(		pItemDataExt != NULL
			&&	::AfxIsValidAddress(
					static_cast < LPVOID > ( pItemDataExt ),
					sizeof( CExtComboBoxBase::LB_ITEM )
					)
			)
		{
			CDC dc;
			dc.Attach( lpDIS->hDC );
			if( lpDIS->itemAction & (ODA_DRAWENTIRE | ODA_SELECT) )
			{
				CRect rcErase( lpDIS->rcItem );
				if( nItem == (GetCount()-1) )
				{
					CRect rcClient;
					GetClientRect( &rcClient );
					if( rcErase.bottom < rcClient.bottom )
						rcErase.bottom = rcClient.bottom;
				}
				dc.FillSolidRect(
					rcErase, 
					::GetSysColor( COLOR_WINDOW )
					);
			}
			if( lpDIS->itemID >= 0 )
				m_pCB->OnPopupListBoxDrawItem( 
					dc,
					lpDIS->rcItem,
					m_pCB->OnPopupListBoxCalcItemExtraSizes( lpDIS->itemID ),
					lpDIS->itemState,
					pItemDataExt 
					);
			dc.Detach();
		}
	} // if( lResult != CB_ERR )
}

void CExtComboBoxFilterPopupListBox::MeasureItem( LPMEASUREITEMSTRUCT lpMIS ) 
{
	m_pCB->OnPopupListBoxMeasureItem( lpMIS );
}

LRESULT CExtComboBoxFilterPopupListBox::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	switch( message )
	{
	case WM_ERASEBKGND:
		if( 	(GetStyle() & LBS_OWNERDRAWFIXED) != 0 
			||	(GetStyle() & LBS_OWNERDRAWVARIABLE) != 0 
			)
			return TRUE;
		break;
	case WM_PAINT:
		if( 	(GetStyle() & LBS_OWNERDRAWFIXED) != 0 
			||	(GetStyle() & LBS_OWNERDRAWVARIABLE) != 0 
			)
		{
			CPaintDC dcPaint( this );
			CRect rcClient;
			GetClientRect( &rcClient );
			CExtMemoryDC dc(
				&dcPaint,
				&rcClient
				);
			dc.FillSolidRect(
				&rcClient,
				::GetSysColor( COLOR_WINDOW )
				);

			CFont * pFont = GetFont();
			ASSERT( pFont != NULL );
			CFont * pOldFont = dc.SelectObject( pFont );

			INT nCount = GetCount();
			for( INT nItem = GetTopIndex(); nItem < nCount; nItem++ )
			{
				DRAWITEMSTRUCT dis;
				::memset( &dis, 0, sizeof( DRAWITEMSTRUCT ) );
				dis.CtlType = ODT_LISTBOX;
				dis.CtlID = GetDlgCtrlID();
				dis.itemID = nItem;
				dis.hDC = dc.GetSafeHdc();
				GetItemRect( nItem, &dis.rcItem );
				dis.itemAction = ODA_DRAWENTIRE;
				dis.hwndItem = GetSafeHwnd();

				if( rcClient.bottom < dis.rcItem.top )
					break;

				if( GetSel( nItem ) > 0 )
					dis.itemState |= ODS_SELECTED;
				if( GetCurSel() == nItem )
					dis.itemState |= ODS_FOCUS;

				SendMessage( 
					WM_DRAWITEM, 
					(WPARAM)GetDlgCtrlID(), 
					(LPARAM)&dis 
					);
			}

			dc.SelectObject( pOldFont );

			return TRUE;
		}
		break;
	}
	return CListBox::WindowProc( message, wParam, lParam );
}

LONG CExtComboBoxFilterPopupListBox::HitTest( 
	const POINT & ptClient
	) const
{
	ASSERT_VALID( this );
	if(		GetSafeHwnd() == NULL
		||	( ! ::IsWindow( GetSafeHwnd() ) )
		)
		return HTNOWHERE;

CPoint ptWnd( ptClient );
	ClientToScreen( &ptWnd );
CRect rcWnd;
	GetWindowRect( &rcWnd );

CRect rcGrip( rcWnd );
	rcGrip.top = rcGrip.bottom - ::GetSystemMetrics( SM_CYVSCROLL );
	rcGrip.left = rcGrip.right - ::GetSystemMetrics( SM_CXVSCROLL );
	if(	rcGrip.PtInRect( ptWnd ) )
		return HTBOTTOMRIGHT;
	
CRect rcBottom( rcWnd );
	rcBottom.top = rcBottom.bottom - ::GetSystemMetrics( SM_CYEDGE );
	if( rcBottom.PtInRect( ptWnd ) )
		return HTBOTTOM;

CRect rcRight( rcWnd );
	rcRight.left = rcRight.right - ::GetSystemMetrics( SM_CXEDGE );
	if( rcRight.PtInRect( ptWnd ) )
		return HTRIGHT;

	if( rcWnd.PtInRect( ptWnd ) )
		return HTCLIENT;

	return HTNOWHERE;
}

#if _MFC_VER < 0x700
void CExtComboBoxFilterPopupListBox::OnActivateApp(BOOL bActive, HTASK hTask) 
#else
void CExtComboBoxFilterPopupListBox::OnActivateApp(BOOL bActive, DWORD hTask) 
#endif
{
	CListBox::OnActivateApp(bActive, hTask);
	if( ! bActive )
		SendMessage( WM_CANCELMODE );
}

void CExtComboBoxFilterPopupListBox::OnCancelMode() 
{
	CListBox::OnCancelMode();
	
	if( CExtMouseCaptureSink::GetCapture() == GetSafeHwnd() )
		CExtMouseCaptureSink::ReleaseCapture();

	m_bResizingX = false;
	m_bResizingY = false;
	m_ptCursorPosLast = CPoint( 0, 0 );

	Invalidate();
	UpdateWindow();
}

void CExtComboBoxFilterPopupListBox::OnCaptureChanged(CWnd *pWnd) 
{
	CListBox::OnCaptureChanged(pWnd);
	if( CExtMouseCaptureSink::GetCapture() != m_hWnd )
		SendMessage( WM_CANCELMODE );
}

void CExtComboBoxFilterPopupListBox::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, true, MK_LBUTTON ) )
		CListBox::OnLButtonDown(nFlags, point);
}

void CExtComboBoxFilterPopupListBox::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, false, MK_LBUTTON ) )
		CListBox::OnLButtonUp(nFlags, point);
}

void CExtComboBoxFilterPopupListBox::OnNcLButtonDblClk(UINT nHitTest, CPoint point) 
{
	if( !_ProcessMouseClick( point, true, MK_LBUTTON ) )
		CListBox::OnNcLButtonDblClk(nHitTest, point);
}

void CExtComboBoxFilterPopupListBox::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, true, MK_LBUTTON ) )
		CListBox::OnLButtonDblClk(nFlags, point);
}

void CExtComboBoxFilterPopupListBox::OnNcLButtonDown(UINT nHitTest, CPoint point) 
{
	if( !_ProcessMouseClick( point, true, MK_LBUTTON ) )
		CListBox::OnNcLButtonDown(nHitTest, point);
}

void CExtComboBoxFilterPopupListBox::OnNcLButtonUp(UINT nHitTest, CPoint point) 
{
	if( !_ProcessMouseClick( point, false, MK_LBUTTON ) )
		CListBox::OnNcLButtonUp(nHitTest, point);
}

void CExtComboBoxFilterPopupListBox::OnMouseMove(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseMove( point ) )
		CListBox::OnMouseMove(nFlags, point);
}


bool CExtComboBoxFilterPopupListBox::_ProcessMouseClick(
	CPoint point,
	bool bButtonPressed,
	INT nMouseButton // MK_... values
	)
{
	ASSERT_VALID( this );

	// process only left mouse button clicks
	if( nMouseButton != MK_LBUTTON )
		return false;

	// select item
	if(		(! bButtonPressed)
		&&  (! m_bResizingX)
		&&  (! m_bResizingY)
		)
	{
		INT nCount = GetCount();
		if( nCount > 0 )
		{
			BOOL bOutside = TRUE;
			INT nHitTest = (INT) ItemFromPoint( point, bOutside );
			nHitTest;
			if( !bOutside )
			{
				SetCurSel( nHitTest );
				m_pCB->OnFilterPopupListSelEndOK();
			}
		}
		return true;
	}

	LONG nHT = HitTest( point );
	
	m_bResizingX = false;
	m_bResizingY = false;
	m_ptCursorPosLast = CPoint( 0, 0 );
	
	if( bButtonPressed )
	{
		if( nHT == HTBOTTOMRIGHT )
			m_bResizingX = m_bResizingY = true;
		if( nHT == HTRIGHT )
			m_bResizingX = true;
		if( nHT == HTBOTTOM )
			m_bResizingY = true;
		if( m_bResizingX || m_bResizingY )
		{
			m_ptCursorPosLast = point;
			if( CExtMouseCaptureSink::GetCapture() != GetSafeHwnd() )
				CExtMouseCaptureSink::SetCapture( GetSafeHwnd() );
			return true;
		}
	}
	else
	{
		if( CExtMouseCaptureSink::GetCapture() == GetSafeHwnd() )
			CExtMouseCaptureSink::ReleaseCapture();
	}
	
	if( nHT == HTCLIENT )
		return true;

	return false;
}

bool CExtComboBoxFilterPopupListBox::_ProcessMouseMove( CPoint point )
{
	ASSERT_VALID( this );
	if(		CExtPopupMenuWnd::IsMenuTracking() 
		||	(! CExtPopupMenuWnd::TestHoverEnabledFromActiveHWND( GetSafeHwnd() ) )
		)
		return false;

	LONG nHT = HitTest( point );
	if(		nHT == HTCLIENT 
		&&	!(m_bResizingX || m_bResizingY)
		&&	m_ptCursorPosLast != point
		)
	{
		INT nCount = GetCount();
		if( nCount == 0 )
			return true;

		BOOL bOutside = TRUE;
		INT nHitTest = (INT) ItemFromPoint( point, bOutside );
		if( bOutside )
		{
			INT nTop = GetTopIndex();
			if( nHitTest <= nTop )
			{
				if( nTop > 0 )
					nHitTest--;
			}
			else
			{
				if( nCount > 0 && nHitTest >= (nCount-2) )
					nHitTest++;
			}
		} // if( bOutside )

		m_ptCursorPosLast = point;

		INT nCurSel = GetCurSel();
		if( nCurSel == nHitTest )
			return true;
		SetCurSel( nHitTest );
	
		Invalidate();
		UpdateWindow();

		return true;
	}
	
	// resize window
	if(		( m_bResizingX || m_bResizingY )
		&&	m_ptCursorPosLast != point
		)
	{
		CRect rcWindow;
		GetWindowRect( &rcWindow );
		CRect rcWindowNew = rcWindow;

		CRect rcClient;
		GetClientRect( &rcClient );

		CSize szMin = m_pCB->OnFilterPopupListQueryMinSize();
		CSize szMax = m_pCB->OnFilterPopupListQueryMaxSize();

		if(		m_bResizingX 
			&&	point.x > szMin.cx 
			&&	( point.x < szMax.cx || szMax.cx < 0 )
			)
		{
			INT nOffsetX = point.x - m_ptCursorPosLast.x;
			rcWindowNew.right += nOffsetX;
			m_ptCursorPosLast.x += nOffsetX;
		}
		if(		m_bResizingY 
			&&	point.y > szMin.cy 
			&&	( point.y < szMax.cy || szMax.cy < 0 )
			)
		{
			INT nOffsetY = point.y - m_ptCursorPosLast.y;
			rcWindowNew.bottom += nOffsetY;
			m_ptCursorPosLast.y += nOffsetY;
		}

		if( rcWindowNew != rcWindow )
		{
			SetWindowPos(
				NULL,
				0,0,
				rcWindowNew.Width(), rcWindowNew.Height(),
				SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE 
					| SWP_NOACTIVATE | SWP_NOSENDCHANGING
				);
			Invalidate( FALSE );
			UpdateWindow();
		}
	}
		
	return true;
}

BOOL CExtComboBoxFilterPopupListBox::OnSetCursor( CWnd * pWnd, UINT nHitTest, UINT message ) 
{
	ASSERT_VALID( this );

	CPoint ptClient;
	::GetCursorPos( &ptClient );
	ScreenToClient( &ptClient );
	LONG nHT = HitTest( ptClient );

	if(		nHT == HTBOTTOMRIGHT 
		&&	m_hCursorSizeNWSE != NULL
		)
	{
		SetCursor( m_hCursorSizeNWSE );
		return TRUE;
	}
	if(		nHT == HTRIGHT 
		&&	m_hCursorSizeWE != NULL
		)
	{
		SetCursor( m_hCursorSizeWE );
		return TRUE;
	}
	if(		nHT == HTBOTTOM
		&&	m_hCursorSizeNS != NULL
		)
	{
		SetCursor( m_hCursorSizeNS );
		return TRUE;
	}
	return CListBox::OnSetCursor(pWnd, nHitTest, message);
}

int CExtComboBoxFilterPopupListBox::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message) 
{
	if( (GetStyle()&WS_TABSTOP) == 0 )
		return MA_NOACTIVATE;
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

UINT CExtComboBoxFilterPopupListBox::OnNcHitTest( CPoint point ) 
{
	CPoint ptClient = point;
	ScreenToClient( &ptClient );
	LONG nHT = HitTest( ptClient );
	if( nHT != HTCLIENT )
		return HTCLIENT;
	return UINT( CListBox::OnNcHitTest(point) );
}

void CExtComboBoxFilterPopupListBox::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp) 
{
	CRect & rcNcRect =
		reinterpret_cast < CRect & > ( lpncsp->rgrc[0] );
	rcNcRect.bottom -= 14;

	CListBox::OnNcCalcSize(bCalcValidRects, lpncsp);
}

void CExtComboBoxFilterPopupListBox::OnNcPaint() 
{
	ASSERT_VALID( this );
	
	Default();
	
CRect rcInBarWnd, rcInBarClient;
    GetWindowRect( &rcInBarWnd );
    GetClientRect( &rcInBarClient );
    ClientToScreen( &rcInBarClient );
    if( rcInBarWnd == rcInBarClient )
        return;

CPoint ptDevOffset = -rcInBarWnd.TopLeft();
    rcInBarWnd.OffsetRect( ptDevOffset );
    rcInBarClient.OffsetRect( ptDevOffset );
   
CWindowDC dc( this );

const INT cx = ::GetSystemMetrics(SM_CXVSCROLL);
const INT cy = ::GetSystemMetrics(SM_CYHSCROLL);

DWORD dwStyle = GetStyle();
bool bHasVerticalSB = ( (dwStyle&WS_VSCROLL) != 0 ) ? true : false;
bool bHasHorizontalSB = ( (dwStyle&WS_HSCROLL) != 0 ) ? true : false;
		
    if( bHasVerticalSB && bHasHorizontalSB )
    {
		INT nNcX = rcInBarWnd.right - rcInBarClient.right - cx;
		INT nNcY = rcInBarWnd.bottom - rcInBarClient.bottom - cy;
        dc.FillSolidRect(
            rcInBarWnd.right - cx - nNcX,
            rcInBarWnd.bottom - cy - nNcY,
            cx,
            cy,
            g_PaintManager->GetColor( COLOR_WINDOW, this )
            );
    }

CRect rcExclude( rcInBarClient );
    if( bHasVerticalSB )
        rcExclude.right += cx;
    if( bHasHorizontalSB )
        rcExclude.bottom += cy;

	dc.ExcludeClipRect( &rcExclude );

	rcInBarWnd.DeflateRect( 1, 1 );

    dc.FillSolidRect(
        &rcInBarWnd, 
        ::GetSysColor( COLOR_WINDOW )
        );	

CRect rcGrip( rcInBarWnd );
	g_PaintManager->PaintResizingGripper(
		dc,
		rcGrip,
		this
		);
	
	dc.SelectClipRgn( NULL );
}

/////////////////////////////////////////////////////////////////////////////
// CExtComboEditCtrlHook
/////////////////////////////////////////////////////////////////////////////

CExtComboEditCtrlHook::CExtComboEditCtrlHook()
{
}

CExtComboEditCtrlHook::~CExtComboEditCtrlHook()
{
}

CExtComboBoxBase * CExtComboEditCtrlHook::OnQueryComboBox() const
{
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( ::IsWindow(GetSafeHwnd()) );
HWND hWndParent = ::GetParent( GetSafeHwnd() );
	ASSERT( hWndParent != NULL );
	ASSERT( ::IsWindow(hWndParent) );
CExtComboBoxBase * pCombo = 
		DYNAMIC_DOWNCAST(
			CExtComboBoxBase,
			FromHandlePermanent(hWndParent)
			);
	ASSERT( pCombo != NULL );
	return pCombo;
}

IMPLEMENT_DYNCREATE( CExtComboEditCtrlHook, CExtEditBase );

BEGIN_MESSAGE_MAP(CExtComboEditCtrlHook, CExtEditBase)
	//{{AFX_MSG_MAP(CExtComboEditCtrlHook)
	ON_WM_CONTEXTMENU()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

LRESULT CExtComboEditCtrlHook::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
bool bFlushAutoComplete = false;

	switch( message )
	{
	case WM_CUT:
	case WM_COPY:
	case WM_PASTE:
	case WM_CLEAR:
	case WM_UNDO:
		bFlushAutoComplete = true;
		break;
	case WM_COMMAND:
		switch( LOWORD(wParam) )
		{
		case ID_EDIT_CLEAR:
		case ID_EDIT_CLEAR_ALL:
		case ID_EDIT_COPY:
		case ID_EDIT_CUT:
		case ID_EDIT_FIND:
		case ID_EDIT_PASTE:
		case ID_EDIT_PASTE_LINK:
		case ID_EDIT_PASTE_SPECIAL:
		case ID_EDIT_REPEAT:
		case ID_EDIT_REPLACE:
		case ID_EDIT_SELECT_ALL:
		case ID_EDIT_UNDO:
		case ID_EDIT_REDO:
			bFlushAutoComplete = true;
		break;
		} // switch( LOWORD(wParam) )
		break;
	} // switch( message )
	
	if( bFlushAutoComplete )
		OnQueryComboBox()->m_bAutoComplete = false;

	return CExtEditBase::WindowProc(message,wParam,lParam);
}

__EXT_MFC_INT_PTR CExtComboEditCtrlHook::OnToolHitTest(
	CPoint point,
	TOOLINFO * pTI
	) const
{
	__PROF_UIS_MANAGE_STATE;
	__EXT_MFC_IMPLEMENT_TT_REDIR_OnToolHitTest_EX( CExtComboEditCtrlHook, CExtToolControlBar, m_wndToolTip, point, pTI, 2 )
	return CExtEditBase::OnToolHitTest( point, pTI );
}

void CExtComboEditCtrlHook::OnContextMenu(CWnd* pWnd,CPoint pos )
{
	if( ! m_bHandleCtxMenus )
	{
		CExtEditBase::OnContextMenu( pWnd, pos );
		return;
	} // if( ! m_bHandleCtxMenus )

CExtComboBoxBase * pCombo = OnQueryComboBox();
	if( pCombo == NULL )
		return;
	m_bHandleCtxMenus = pCombo->m_bHandleCtxMenus;
	SetFocus();

		CExtEditBase::OnContextMenu( pWnd, pos );

	VERIFY(
		pCombo->RedrawWindow(
		NULL,
		NULL,
		RDW_INVALIDATE|RDW_UPDATENOW|RDW_ERASE|RDW_ERASENOW
		|RDW_FRAME|RDW_ALLCHILDREN
		)
		);
}

void CExtComboEditCtrlHook::OnDestroy() 
{
	CExtComboBoxBase * pCB = OnQueryComboBox();
	ASSERT( pCB != NULL );
	if( pCB != NULL )
	{
		ASSERT_VALID( pCB );
		pCB->OnFilterPopupListDestroy();
	}
	CExtEditBase::OnDestroy();
}

/////////////////////////////////////////////////////////////////////////////
// CExtComboBoxBase::LB_ITEM

CExtComboBoxBase::LB_ITEM::CELL::CELL(
	__EXT_MFC_SAFE_LPCTSTR sItemText // = NULL
	)
{
	m_sItemText = (sItemText != NULL) ? sItemText : _T("");
	m_nLParam = 0L;
	m_clrText = COLORREF( -1L );
	m_clrBack = COLORREF( -1L );
}

CExtComboBoxBase::LB_ITEM::CELL::~CELL()
{
}

/////////////////////////////////////////////////////////////////////////////
// CExtComboBoxBase::LB_ITEM

CExtComboBoxBase::LB_ITEM::LB_ITEM()
{
	m_dwUserData = 0;
}

CExtComboBoxBase::LB_ITEM::~LB_ITEM()
{
	while( m_arrItemCells.GetSize() > 0 )
	{
		CELL * pItemCell = m_arrItemCells[0];
		if( pItemCell != NULL ) 
		{
			delete pItemCell;
			pItemCell = NULL;
		}
		m_arrItemCells.RemoveAt(0);
	}
}

INT CExtComboBoxBase::LB_ITEM::LbItemCellInsert(
	__EXT_MFC_SAFE_LPCTSTR sItemText, // = NULL
	INT nPos // = -1 // append
	)
{
INT nCount = INT( m_arrItemCells.GetSize() );
	if( nPos < 0 || nPos > nCount )
		nPos = nCount; // append
	
CELL * pItemCell = new CELL( sItemText );
	ASSERT( pItemCell != NULL );
	m_arrItemCells.InsertAt( nPos, pItemCell );

	return nPos;
}

bool CExtComboBoxBase::LB_ITEM::LbItemCellRemove(
	LONG nItem
	)
{
INT nCount = INT( m_arrItemCells.GetSize() );
	if( nItem < 0 || nItem >= nCount )
		return false;
	m_arrItemCells.RemoveAt( nItem );
	return true;
}

CExtComboBoxBase::LB_ITEM::CELL * CExtComboBoxBase::LB_ITEM::LbItemCellGet( 
	LONG nItem 
	) const
{
INT nCount = INT( m_arrItemCells.GetSize() );
	if( nItem < 0 || nItem >= nCount )
		return NULL;

CELL * pItemCell = m_arrItemCells.GetAt( nItem );
	ASSERT( pItemCell != NULL );
	return pItemCell;
}

INT CExtComboBoxBase::LB_ITEM::LbItemCellGetIndexOf( 
	const CELL * pItemCell 
	) const
{
INT nCount = INT( m_arrItemCells.GetSize() );
	for( INT nItem = 0; nItem < nCount; nItem++ )
	{
		CELL * pItemCell2 = m_arrItemCells.GetAt( nItem );
		ASSERT( pItemCell2 != NULL );
		if( pItemCell2 == pItemCell )
			return nItem;
	}
	return -1;
}

INT CExtComboBoxBase::LB_ITEM::LbItemCellGetCount() const
{
INT nCount = INT( m_arrItemCells.GetSize() );
	return nCount;
}

DWORD CExtComboBoxBase::LB_ITEM::DataGet() const
{
	return m_dwUserData;
}

void CExtComboBoxBase::LB_ITEM::DataSet( 
	DWORD dwData 
	)
{
	m_dwUserData = dwData;
}
	
/////////////////////////////////////////////////////////////////////////////
// CExtComboBoxBase

IMPLEMENT_DYNCREATE( CExtComboBoxBase, CComboBox );

CExtComboBoxBase::CExtComboBoxBase()
	: m_bAutoComplete( true )
	, m_bHandleCtxMenus( true )
	, m_bEnableAutoComplete( true )
	, m_clrBack( COLORREF(-1L) )
	, m_clrBackPrev( COLORREF(-1L) )
	, m_clrText( COLORREF(-1L) )
	, m_pPopupListWnd( NULL )
	, m_szFilterPopupListBox( -1, -1 )
	, m_bEnableAutoFilter( false )
{
	EnableToolTips();
}

CExtComboBoxBase::~CExtComboBoxBase()
{
CExtAnimationSite * pAcAS = AnimationClient_SiteGet();
	if( pAcAS != NULL )
		pAcAS->AnimationSite_ClientRemove( this );
}

BEGIN_MESSAGE_MAP(CExtComboBoxBase, CComboBox)
	//{{AFX_MSG_MAP(CExtComboBoxBase)
	ON_MESSAGE(CB_ADDSTRING, OnCBAddString)
	ON_MESSAGE(CB_INSERTSTRING, OnCBInsertString)
	ON_MESSAGE(CB_DELETESTRING, OnCBDeleteString)
	ON_MESSAGE(CB_RESETCONTENT, OnCBResetContent)
	ON_MESSAGE(CB_GETITEMDATA, OnCBGetItemData)
	ON_MESSAGE(CB_SETITEMDATA, OnCBSetItemData)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_CONTROL_REFLECT( CBN_EDITUPDATE, OnEditCtrlUpdate )
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtComboBoxBase message handlers

CSize CExtComboBoxBase::OnFilterPopupListQueryMinSize()
{
	return CSize( 40, 20 );
}

CSize CExtComboBoxBase::OnFilterPopupListQueryMaxSize()
{
	return CSize( -1, -1 );
}

bool CExtComboBoxBase::IsFilterPopupListVisible() const
{
	ASSERT_VALID( this );
	if(		m_pPopupListWnd != NULL 
		&&	m_pPopupListWnd->GetSafeHwnd() != NULL
		)
		return m_pPopupListWnd->IsWindowVisible() ? true : false;
	return false;
}

CSize CExtComboBoxBase::GetFilterPopupListSize() const
{
	return m_szFilterPopupListBox;
}

CSize CExtComboBoxBase::SetFilterPopupListSize( CSize szSize )
{
	CSize szFilterPopupListBoxOld = m_szFilterPopupListBox;
	m_szFilterPopupListBox = szSize;
	return szFilterPopupListBoxOld;
}

bool CExtComboBoxBase::OnHookWndMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	UINT nMessage,
	WPARAM & wParam,
	LPARAM & lParam
	)
{
	if( GetSafeHwnd() != NULL )
	{
		if(		nMessage == WM_SETFOCUS 
			||	nMessage == WM_KILLFOCUS
			||	nMessage == WM_NEXTDLGCTL
			||	(	(	nMessage == WM_WINDOWPOSCHANGING
					||	nMessage == WM_WINDOWPOSCHANGED
					)
					&& ( ((LPWINDOWPOS)lParam)->flags&(SWP_NOMOVE|SWP_NOSIZE) ) == 0
				)
			||	nMessage == WM_SIZE
			||	nMessage == WM_ACTIVATEAPP
			||	nMessage == WM_CANCELMODE
			||	nMessage == WM_STYLECHANGING
			||	nMessage == WM_STYLECHANGED
			||	nMessage == WM_SYSCOMMAND
			||	nMessage == WM_DESTROY
			||	nMessage == WM_NCDESTROY
			||	nMessage == WM_ENABLE
			||	nMessage == CExtPopupMenuWnd::g_nMsgPrepareMenu
			||	(	(	nMessage == WM_MOUSEACTIVATE
					||	nMessage == WM_ACTIVATE
					)
				&&	(! ::IsChild( m_hWnd, hWndHooked ) )
				)
			|| CExtPopupMenuWnd::IsMenuTracking()
			|| CExtControlBar::_DraggingGetBar() != NULL
			)
		{
			OnFilterPopupListDestroy();
		}
	}
	return
		CExtHookSink::OnHookWndMsg(
			lResult,
			hWndHooked,
			nMessage,
			wParam,
			lParam
			);
}

HWND CExtComboBoxBase::AnimationSite_GetSafeHWND() const
{
__PROF_UIS_MANAGE_STATE;
HWND hWnd = GetSafeHwnd();
	return hWnd;
}

void CExtComboBoxBase::ShowFilterPopupList( 
	CExtSafeString & sFilterText 
	)
{
	ASSERT_VALID( this );

	if( m_pPopupListWnd == NULL )
		m_pPopupListWnd = OnFilterPopupListCreate();
	if( m_pPopupListWnd == NULL )
	{
		ASSERT( FALSE );
		return;
	}
	ASSERT_VALID( m_pPopupListWnd );
	
	// find nearest top lever popup window
	CWnd * pWndParent = this;
	for( ; pWndParent != NULL; pWndParent = pWndParent->GetParent() )
		if( (pWndParent->GetStyle()&WS_CHILD) == 0 )
			break;
	VERIFY(
		SetupHookWndSink(
			pWndParent->GetSafeHwnd(),
			false,
			false
			)
		);
	VERIFY(
		SetupHookWndSink(
			m_hWnd,
			false,
			false
			)
		);
HWND hWndEditor = GetInnerEditCtrl()->GetSafeHwnd();
	if( hWndEditor )
		VERIFY(
			SetupHookWndSink(
				hWndEditor,
				false,
				false
				)
			);

	OnFilterPopupListSyncContents( sFilterText );

	if(		sFilterText.IsEmpty()
		||	m_pPopupListWnd->GetCount() == 0 
		)
		OnFilterPopupListDestroy();
	else
		if( !IsFilterPopupListVisible() )
			m_pPopupListWnd->SetWindowPos(
				NULL,
				0,0,0,0,
				SWP_SHOWWINDOW | SWP_NOZORDER | SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE
				);
}

CExtComboBoxFilterPopupListBox * CExtComboBoxBase::OnFilterPopupListCreate()
{
	ASSERT_VALID( this );

	OnFilterPopupListDestroy();

	CExtComboBoxFilterPopupListBox * pPopupListWnd =
		new CExtComboBoxFilterPopupListBox (
			this
			);
	if( pPopupListWnd == NULL )
	{
		ASSERT( FALSE );
		return NULL;
	}
	ASSERT_VALID( pPopupListWnd );

	CRect rcWnd;
	GetWindowRect( &rcWnd );
	
	CSize szMax = OnFilterPopupListQueryMaxSize();
	szMax.cx = szMax.cx > 0 ? szMax.cx : 150;
	szMax.cy = szMax.cy > 0 ? szMax.cy : 100;
	if( m_szFilterPopupListBox.cx < 0 )
		m_szFilterPopupListBox.cx = max( rcWnd.Width(), szMax.cx );
	if( m_szFilterPopupListBox.cy < 0 )
		m_szFilterPopupListBox.cy = max( ::MulDiv( rcWnd.Width(), 2, 3 ), szMax.cy );

	CRect rcPopupListWnd(
		rcWnd.left,
		rcWnd.bottom,
		rcWnd.left + m_szFilterPopupListBox.cx,
		rcWnd.bottom + m_szFilterPopupListBox.cy
		);
	rcPopupListWnd.OffsetRect( 1, 1 );

	DWORD dwPopupListWndStyle = 
		WS_POPUP | WS_BORDER | WS_VSCROLL | LBS_NOINTEGRALHEIGHT | LBS_HASSTRINGS | LBS_OWNERDRAWVARIABLE;

	if( !pPopupListWnd->CreateEx(
			WS_EX_TOPMOST | WS_EX_WINDOWEDGE,
			_T("LISTBOX"),
			_T(""),
			dwPopupListWndStyle,
			rcPopupListWnd,
			this,
			UINT(0)
			)
		)
	{
		ASSERT( FALSE );
		return NULL;
	}
	pPopupListWnd->SetFont( 
		CFont::FromHandle( (HFONT)::GetStockObject(DEFAULT_GUI_FONT) ) 
		);

	return pPopupListWnd;
}

void CExtComboBoxBase::OnFilterPopupListDestroy()
{
	ASSERT_VALID( this );
CExtComboBoxFilterPopupListBox * pLB = m_pPopupListWnd;
	if( pLB != NULL )
	{
		m_pPopupListWnd = NULL;
		if( pLB->GetSafeHwnd() != NULL )
		{
			RemoveAllWndHooks();
			CRect rcWnd;
			pLB->GetWindowRect( &rcWnd );
			m_szFilterPopupListBox = rcWnd.Size();
			pLB->DestroyWindow();
		}
		delete pLB;
	}
}

void CExtComboBoxBase::OnFilterPopupListSyncContents(
	CExtSafeString & sFilterText
	)
{
	ASSERT_VALID( this );
	if(		m_pPopupListWnd == NULL 
		||	m_pPopupListWnd->GetSafeHwnd() == NULL 
		)
		return;

	m_pPopupListWnd->SetRedraw( FALSE );

	m_pPopupListWnd->ResetContent();
	if( !sFilterText.IsEmpty() )
	{
		CStringArray arrTextToRemove; 
		arrTextToRemove.Add( _T("http://") );
		arrTextToRemove.Add( _T("http://www.") );
		arrTextToRemove.Add( _T("ftp://") );
		arrTextToRemove.Add( _T("ftp://www.") );
		arrTextToRemove.Add( _T("file://") );
		arrTextToRemove.Add( _T("www.") );

		INT nLenght = sFilterText.GetLength();
		CString sItemText;

		for( LONG nItem = 0; nItem < GetCount(); nItem++ )
		{
			sItemText.Empty();
			GetLBText( nItem, sItemText );

			CString sItemTextToCompare = sItemText;
			
			for( INT nIndex = 0; nIndex < arrTextToRemove.GetSize(); nIndex++ ) 
			{
				CString sTextToRemove = arrTextToRemove[ nIndex ];
				INT nLenght = sTextToRemove.GetLength();
				CString sItemTextPart = sItemTextToCompare.Left( nLenght );
				if( sItemTextPart.CompareNoCase( sTextToRemove ) == 0 )
					sItemTextToCompare.Delete( 0, nLenght );
			}

			if(	sFilterText.CompareNoCase( sItemTextToCompare.Left( nLenght ) ) == 0 )
			{
				INT nItemAdded = m_pPopupListWnd->AddString( sItemText );
				LB_ITEM * pLbItem = LbItemGet( nItem );
				m_pPopupListWnd->SetItemData( 
					nItemAdded, 
					(DWORD) pLbItem 
					);
			}
		}
	} // if( !sFilterText.IsEmpty() )

	m_pPopupListWnd->SetRedraw( TRUE );
	m_pPopupListWnd->Invalidate();
	m_pPopupListWnd->UpdateWindow();
}

BOOL CExtComboBoxBase::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult) 
{
	ASSERT_VALID( this );
	if( message == WM_COMMAND )
	{
		INT nCode = HIWORD(wParam);
		if( nCode == CBN_DROPDOWN )
			OnFilterPopupListDestroy();
	}
	return CComboBox::OnChildNotify(message, wParam, lParam, pLResult);
}

LONG CExtComboBoxBase::FindItemExact(
	__EXT_MFC_SAFE_LPCTSTR lpszString,
	LONG nIndexStart // = -1
	) const
{
	ASSERT_VALID( this );
	LONG nCount = GetCount();
	if(		(!(nIndexStart < 0 || nIndexStart >= nCount))
		&&	nIndexStart != -1 
		)
	{
		ASSERT( FALSE );
		return CB_ERR;
	}
	LONG nItemsProcessed = 0;
	LONG nItem = nIndexStart;
	while( nItemsProcessed < nCount )
	{
		if( nItem == -1 )
			nItem = 0;
		else if( nItem < nCount - 1 )
			nItem++;
		else
			nItem = 0;

		CString sText;
		GetLBText( nItem, sText );
		if( sText.Compare( lpszString ) == 0 )
			return nItem;

		nItemsProcessed++;
	}
	return CB_ERR;
}

void CExtComboBoxBase::OnFilterPopupListSyncSelection( 
	bool bSetEditSel // = true
	)
{
	ASSERT_VALID( this );
	if(		m_pPopupListWnd == NULL 
		||	m_pPopupListWnd->GetSafeHwnd() == NULL 
		)
		return;
	
	LB_ITEM * pLbItemNew = NULL;
	INT nItem = m_pPopupListWnd->GetCurSel();

	LB_ITEM * pItemDataExt = 
		(LB_ITEM *) m_pPopupListWnd->GetItemData( nItem );
	if(		pItemDataExt != NULL
		&&	::AfxIsValidAddress(
				static_cast < LPVOID > ( pItemDataExt ),
				sizeof( LB_ITEM )
				)
		)
		pLbItemNew = pItemDataExt;

	for( LONG nIndex = 0; nIndex < GetCount(); nIndex++ )
	{
		LB_ITEM * pLbItem = LbItemGet( nIndex );
		if( pLbItem == pLbItemNew )
		{
			CEdit * pEdit = GetInnerEditCtrl();
			ASSERT( pEdit != NULL );
			ASSERT_VALID( pEdit );
			if( pEdit != NULL )
				pEdit->SetRedraw( FALSE );
			SetCurSel( nIndex );
			// make text non-selected
			if( !bSetEditSel )
				SetEditSel( -1, -1 );
			if( pEdit != NULL )
			{
				pEdit->SetRedraw( TRUE );
				pEdit->Invalidate();
				pEdit->UpdateWindow();
			}
			break;
		}
	}
}

void CExtComboBoxBase::OnFilterPopupListSelChanged()
{
	ASSERT_VALID( this );
	if( m_pPopupListWnd != NULL )
	{
		m_pPopupListWnd->Invalidate();
		m_pPopupListWnd->UpdateWindow();
	}
	OnFilterPopupListSyncSelection( false );
}

void CExtComboBoxBase::OnFilterPopupListSelEndOK()
{
	ASSERT_VALID( this );
	if(		m_pPopupListWnd == NULL 
		||	m_pPopupListWnd->GetSafeHwnd() == NULL 
		)
		return;
	
	OnFilterPopupListSyncSelection( true );

	OnFilterPopupListDestroy();

	// send the CBN_SELENDOK notification
	GetParent()->SendMessage( 
		WM_COMMAND,
		(WPARAM) MAKELONG( GetDlgCtrlID(), CBN_SELENDOK ),
		(LPARAM) GetSafeHwnd()
		);
}

BOOL CExtComboBoxBase::PreTranslateMessage(MSG* pMsg) 
{
	if( !CExtPopupMenuWnd::IsMenuTracking() )
	{
		InitToolTip();
		m_wndToolTip.RelayEvent(pMsg);
	}

	if(		m_pPopupListWnd != NULL
		&&	m_pPopupListWnd->GetSafeHwnd() != NULL
		)
	{
		if(		pMsg->message == WM_KEYDOWN 
			&&	( pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_RETURN )
			)
		{
			HWND hWndFocus = ::GetFocus();
			if(		hWndFocus != NULL
				&&	(	m_hWnd == hWndFocus
					||	::IsChild( m_hWnd, hWndFocus )
					)
				)
			{
				if( pMsg->wParam == VK_ESCAPE )
					OnFilterPopupListDestroy();
				else if( pMsg->wParam == VK_RETURN )
				{
					INT nItem = m_pPopupListWnd->GetCurSel();
					if(		nItem == LB_ERR 
						&&	m_pPopupListWnd->GetCount() > 0
						) 
						m_pPopupListWnd->SetCurSel( 0 );
					OnFilterPopupListSelEndOK();
				}
				return TRUE;
			}
		}
		if(	( pMsg->message == WM_KEYDOWN 
				&&	(	pMsg->wParam == VK_DOWN 
					||	pMsg->wParam == VK_UP 
//					||	pMsg->wParam == VK_HOME 
//					||	pMsg->wParam == VK_END 
//					||	pMsg->wParam == VK_LEFT 
//					||	pMsg->wParam == VK_RIGHT
					||	pMsg->wParam == VK_PRIOR 
					||	pMsg->wParam == VK_NEXT
					)
				)
			|| pMsg->message == WM_MOUSEWHEEL
			)
		{
			INT nCurSelOld = m_pPopupListWnd->GetCurSel();
			m_pPopupListWnd->SendMessage( 
				pMsg->message,
				pMsg->wParam,
				pMsg->lParam
				);
			INT nCurSelNew = m_pPopupListWnd->GetCurSel();
			if( nCurSelOld != nCurSelNew )
				OnFilterPopupListSelChanged();
			return TRUE;
		}
	}
	
	if( m_bEnableAutoComplete )
	{
		if( pMsg->message == WM_SYSCHAR )
			return TRUE;
		if( pMsg->message == WM_KEYDOWN )
		{
			m_bAutoComplete = true;
			int nVirtKey = (int) pMsg->wParam;
			if(nVirtKey == VK_DELETE || nVirtKey == VK_BACK)
				m_bAutoComplete = false;
		} // if( pMsg->message == WM_KEYDOWN )
	} // if( m_bEnableAutoComplete )
	else
	{
		m_bAutoComplete = false;
	} // else from if( m_bEnableAutoComplete )

	return CComboBox::PreTranslateMessage(pMsg);
}

void CExtComboBoxBase::OnEditCtrlUpdate() 
{
	if( m_bEnableAutoFilter )
	{
		CEdit * pEdit = GetInnerEditCtrl();
		ASSERT( pEdit != NULL );
		ASSERT_VALID( pEdit );
		if( pEdit != NULL )
		{
			CString sFilterText;
			pEdit->GetWindowText( sFilterText );
			INT nStartChar = 0;
			INT nEndChar = 0;
			pEdit->GetSel(
				nStartChar,
				nEndChar 
				) ;
			if( nStartChar < nEndChar )
				sFilterText.Delete( nStartChar, nEndChar - nStartChar );
			sFilterText.TrimLeft();
			if( ! GetDroppedState() )
			{
				CExtSafeString _sFilterText = LPCTSTR(sFilterText);
				ShowFilterPopupList( _sFilterText );
			}
		}
	}
	
	if( !m_bEnableAutoComplete )
	{
		Default();
		return;
	}
	if( !m_bAutoComplete ) 
		return;
CString str;
	GetWindowText( str );
int nLength = str.GetLength();
DWORD dwCurSel = GetEditSel();
WORD dStart = LOWORD( dwCurSel );
WORD dEnd   = HIWORD( dwCurSel );
int nSelectStringResult = SelectString( -1, str );
	if( nSelectStringResult == CB_ERR )
	{
		SetWindowText( str );
		if( dwCurSel != CB_ERR )
			SetEditSel( dStart, dEnd );
	} // if( nSelectStringResult == CB_ERR )
	if( dEnd < nLength && dwCurSel != CB_ERR )
		SetEditSel( dStart, dEnd );
	else
		SetEditSel( nLength, -1 );
}

void CExtComboBoxBase::OnSize(UINT nType, int cx, int cy) 
{
	AnimationSite_ClientProgressStop( this );
	AnimationClient_StateGet( false ).Empty();
	AnimationClient_StateGet( true ).Empty();
CRect rcClient;
	GetClientRect( &rcClient );
	AnimationClient_TargetRectSet( rcClient );

	CComboBox::OnSize( nType, cx, cy );
}

void CExtComboBoxBase::OnDestroy() 
{
	for( LONG nItem = 0; nItem < GetCount(); nItem++ )
	{
		LRESULT lResult = DefWindowProc( CB_GETITEMDATA, nItem, 0 );
		if( lResult != CB_ERR )		
		{
			LB_ITEM * pItemDataExt = (LB_ITEM *) lResult;
			// ASSERT( pItemDataExt != NULL );
			if(		pItemDataExt != NULL
				&&	::AfxIsValidAddress(
						static_cast < LPVOID > ( pItemDataExt ),
						sizeof( LB_ITEM )
						)
				)
			{
				delete pItemDataExt;
				pItemDataExt = NULL;
			}
		}
	}
	CComboBox::OnDestroy();
}

LRESULT CExtComboBoxBase::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	switch( message )
	{
	case WM_NOTIFY:
		if(		m_wndToolTip.GetSafeHwnd() != NULL
			&&	IsWindow( m_wndToolTip.GetSafeHwnd() )
			&&	((LPNMHDR)lParam) != NULL
			&&	((LPNMHDR)lParam)->hwndFrom == m_wndToolTip.GetSafeHwnd()
			&&	((LPNMHDR)lParam)->code == TTN_SHOW
			)
			::SetWindowPos(
				m_wndToolTip.GetSafeHwnd(),
				HWND_TOP,
				0,0,0,0,
				SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE
				);
		break;
	case WM_ENABLE:
		{
			HWND hWndEdit =
				::GetWindow( GetSafeHwnd(), GW_CHILD );
			if( hWndEdit == NULL || !::IsWindow(hWndEdit) )
				break;
			::EnableWindow( hWndEdit, TRUE );
			::SendMessage( hWndEdit, EM_SETREADONLY, !wParam, 0L );
			Invalidate();
		}
		break;

	case WM_CREATE:
		{
			LRESULT lResult =
				CComboBox::WindowProc(message, wParam, lParam);;
			OnSubclassInnerEdit();
			return lResult;
		}
	} // switch( message )

	return CComboBox::WindowProc(message, wParam, lParam);
}

void CExtComboBoxBase::PreSubclassWindow() 
{
	CComboBox::PreSubclassWindow();

	OnSubclassInnerEdit();

DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle&WS_CLIPCHILDREN) == 0 )
		ModifyStyle( 0, WS_CLIPCHILDREN, SWP_FRAMECHANGED );

// 	for( LONG nItem = 0; nItem < GetCount(); nItem++ )
// 	{
// 		bool bInit = false;
// 		LRESULT lResult = DefWindowProc( CB_GETITEMDATA, nItem, 0 );
// 		if( lResult != CB_ERR )		
// 		{
// 			LB_ITEM * pItemDataExt = (LB_ITEM *) lResult;
// 			if(		pItemDataExt != NULL
// 				&&	::AfxIsValidAddress(
// 						static_cast < LPVOID > ( pItemDataExt ),
// 						sizeof( LB_ITEM )
// 						)
// 				)
// 				bInit = true;
// 		}
// 		if( ! bInit )
// 		{
// 			CString sString;
// 			GetLBText( nItem, sString );
// 
// 			LB_ITEM * pItemDataExt = new LB_ITEM;
// 			ASSERT( pItemDataExt != NULL );
// 			pItemDataExt->DataSet( lResult );
// 			
// 			INT nItem = pItemDataExt->LbItemCellInsert( sString );
// 			LB_ITEM::CELL * pItemCell = 
// 				pItemDataExt->LbItemCellGet( nItem );
// 			ASSERT( pItemCell != NULL );
// 			
// 			DefWindowProc( CB_SETITEMDATA, nItem, (LPARAM) pItemDataExt );
// 		}
// 	}
 
	AnimationSite_ClientProgressStop( this );
	AnimationClient_StateGet( false ).Empty();
	AnimationClient_StateGet( true ).Empty();
CRect rectButton;
	GetClientRect( &rectButton );
	AnimationClient_TargetRectSet( rectButton );
}

void CExtComboBoxBase::OnSubclassInnerEdit()
{
	if( m_wndInnerEditHook.GetSafeHwnd() == NULL )
	{
		HWND hWndEdit = ::GetWindow( GetSafeHwnd(), GW_CHILD );
		if( hWndEdit != NULL && ::IsWindow(hWndEdit) )
		{
			m_wndInnerEditHook.m_bHandleCtxMenus = m_bHandleCtxMenus;
			VERIFY( m_wndInnerEditHook.SubclassWindow( hWndEdit ) );
		} // if( hWndEdit != NULL && ::IsWindow(hWndEdit) )
	} // if( m_wndInnerEditHook.GetSafeHwnd() == NULL )
}

__EXT_MFC_INT_PTR CExtComboBoxBase::OnToolHitTest(
	CPoint point,
	TOOLINFO * pTI
	) const
{
	__PROF_UIS_MANAGE_STATE;
	__EXT_MFC_IMPLEMENT_TT_REDIR_OnToolHitTest( CExtComboBoxBase, CExtToolControlBar );
	return CComboBox::OnToolHitTest( point, pTI );
}

void CExtComboBoxBase::InitToolTip()
{
	if( m_wndToolTip.m_hWnd == NULL )
	{
		m_wndToolTip.Create(this);
		m_wndToolTip.Activate(FALSE);
	}
}

void CExtComboBoxBase::ActivateTooltip(BOOL bActivate)
{
	if( m_wndToolTip.GetToolCount() == 0 )
		return;
	m_wndToolTip.Activate(bActivate);
}

int CExtComboBoxBase::OnQueryMaxTipWidth( 
	__EXT_MFC_SAFE_LPCTSTR lpszText 
	)
{
	lpszText;
	return 250;
}

void CExtComboBoxBase::SetTooltipText(
	CExtSafeString * spText,
	BOOL bActivate // = TRUE
	)
{
	if( spText == NULL )
		return;
	InitToolTip();
	if( m_wndToolTip.GetToolCount() == 0 )
	{
		CRect rectBtn; 
		GetClientRect(rectBtn);
		m_wndToolTip.AddTool( this, *spText, rectBtn, 1 );
	}
	CWnd::CancelToolTips();
	m_wndToolTip.UpdateTipText( *spText, this, 1 );
	m_wndToolTip.SetMaxTipWidth( -1 );
	if( spText->Find( _T("\r") ) >= 0 )
		m_wndToolTip.SetMaxTipWidth( OnQueryMaxTipWidth( *spText ) );
	m_wndToolTip.Activate(bActivate);
}

void CExtComboBoxBase::SetTooltipText(
	CExtSafeString & sText,
	BOOL bActivate // = TRUE
	)
{
	if( sText.IsEmpty() )
		return;
	InitToolTip();
	if( m_wndToolTip.GetToolCount() == 0 )
	{
		CRect rectBtn; 
		GetClientRect(rectBtn);
		m_wndToolTip.AddTool( this, sText, rectBtn, 1 );
	}
	CWnd::CancelToolTips();
	m_wndToolTip.UpdateTipText( sText, this, 1 );
	m_wndToolTip.SetMaxTipWidth( -1 );
	if( sText.Find( _T("\r") ) >= 0 )
		m_wndToolTip.SetMaxTipWidth( OnQueryMaxTipWidth( sText ) );
	m_wndToolTip.Activate( bActivate );
}

void CExtComboBoxBase::SetTooltipText(
	__EXT_MFC_SAFE_LPCTSTR sText,
	BOOL bActivate // = TRUE
	)
{
	if(		sText == NULL
		|| _tcslen( sText ) == 0
		)
		return;
	InitToolTip();
	if( m_wndToolTip.GetToolCount() == 0 )
	{
		CRect rectBtn; 
		GetClientRect(rectBtn);
		m_wndToolTip.AddTool(this,sText,rectBtn,1);
	}
	CWnd::CancelToolTips();
	m_wndToolTip.UpdateTipText(sText,this,1);
	m_wndToolTip.SetMaxTipWidth( -1 );
	CString strText( sText );
	if( strText.Find( _T("\r") ) >= 0 )
		m_wndToolTip.SetMaxTipWidth( OnQueryMaxTipWidth( sText ) );
	m_wndToolTip.Activate(bActivate);
}

void CExtComboBoxBase::SetTooltipText(
	int nId,
	BOOL bActivate // = TRUE
	)
{
CExtSafeString sText;
	g_ResourceManager->LoadString( sText, nId );
	if( ! sText.IsEmpty() )
		SetTooltipText( &sText, bActivate );
}

void CExtComboBoxBase::SetBkColor( 
	COLORREF clrBk 
	)
{ 
	ASSERT_VALID( this );
	m_clrBack = clrBk; 
	if( GetSafeHwnd() != NULL )
		Invalidate();
}

COLORREF CExtComboBoxBase::GetBkColor() const
{ 
	ASSERT_VALID( this );
	return m_clrBack; 
}

void CExtComboBoxBase::SetTextColor( 
	COLORREF clrText 
	)
{ 
	ASSERT_VALID( this );
	m_clrText = clrText; 
	if( GetSafeHwnd() != NULL )
		Invalidate();
}

COLORREF CExtComboBoxBase::GetTextColor() const
{ 
	ASSERT_VALID( this );
	return m_clrText; 
}

HBRUSH CExtComboBoxBase::CtlColor( CDC* pDC, UINT nCtlColor )
{
	ASSERT_VALID( this );
	if(		nCtlColor == CTLCOLOR_EDIT 
		||	nCtlColor == CTLCOLOR_MSGBOX
		||	nCtlColor == CTLCOLOR_STATIC
		)
	{
		COLORREF clrText = GetTextColor();
		if( clrText == COLORREF(-1L) )
		{
			COLORREF clrSysText =
				PmBridge_GetPM()->GetColor(
					( ! IsWindowEnabled() )
						? COLOR_GRAYTEXT
						: COLOR_WINDOWTEXT
					);
			clrText = clrSysText;
		} 
		COLORREF clrBk = GetBkColor();
		if( clrBk == COLORREF(-1L) )
		{
			COLORREF clrSysBk =
				PmBridge_GetPM()->GetColor( 
					( ! IsWindowEnabled() )
						? COLOR_3DFACE
						: COLOR_WINDOW,
					this
				);
			clrBk = clrSysBk;
		}
		pDC->SetBkColor( clrBk );
		pDC->SetTextColor( clrText );
		if( m_clrBackPrev != clrBk )
		{
			if( m_brBack.GetSafeHandle() != NULL )
				m_brBack.DeleteObject();
			m_brBack.CreateSolidBrush( clrBk );
			m_clrBackPrev = clrBk;
		}
		return m_brBack;
	}
	else
		return (HBRUSH)Default();
}

CEdit * CExtComboBoxBase::OnQueryInnerEditCtrl() const
{
	ASSERT_VALID( this );
	CEdit * pEdit = NULL;
	DWORD dwWndStyle = GetStyle();
	DWORD dwComboBoxType = dwWndStyle & 0x0003L;
	if(		dwComboBoxType != CBS_DROPDOWNLIST 
		&&	m_wndInnerEditHook.m_hWnd != NULL
		&&	::IsWindow( m_wndInnerEditHook.m_hWnd )
		)
		pEdit = (CEdit *)&m_wndInnerEditHook;
	return pEdit;
}

CEdit * CExtComboBoxBase::GetInnerEditCtrl() const
{
	ASSERT_VALID( this );
	return OnQueryInnerEditCtrl();
}

INT CExtComboBoxBase::AddStringUnique( 
	__EXT_MFC_SAFE_LPCTSTR lpszString 
	)
{
	ASSERT_VALID( this );
	if( CB_ERR == FindStringExact( 0, lpszString ) )
		return AddString( lpszString );
	return CB_ERR;
}

LRESULT CExtComboBoxBase::OnCBAddString( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
LRESULT lResult = DefWindowProc( CB_ADDSTRING, wParam, lParam );
	if( lResult != CB_ERR )		
	{
		LPCTSTR lpszString = LPCTSTR ( lParam );
		
		LB_ITEM * pItemDataExt = new LB_ITEM;
		ASSERT( pItemDataExt != NULL );
		
		INT nItem = pItemDataExt->LbItemCellInsert( lpszString );
		LB_ITEM::CELL * pItemCell = 
			pItemDataExt->LbItemCellGet( nItem );
		ASSERT( pItemCell != NULL );
		
		DefWindowProc( CB_SETITEMDATA, lResult, (LPARAM) pItemDataExt );
	}
	return lResult;
}

LRESULT CExtComboBoxBase::OnCBInsertString( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
LRESULT lResult = DefWindowProc( CB_INSERTSTRING, wParam, lParam );
	if( lResult != CB_ERR )		
	{
		LPCTSTR lpszString = LPCTSTR ( lParam );
		
		LB_ITEM * pItemDataExt = new LB_ITEM;
		ASSERT( pItemDataExt != NULL );
		
		INT nItem = pItemDataExt->LbItemCellInsert( lpszString );
		LB_ITEM::CELL * pItemCell = 
			pItemDataExt->LbItemCellGet( nItem );
		ASSERT( pItemCell != NULL );
		
		DefWindowProc( CB_SETITEMDATA, lResult, (LPARAM) pItemDataExt );
	}
	return lResult;
}

LRESULT CExtComboBoxBase::OnCBDeleteString( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
LRESULT lResult = DefWindowProc( CB_GETITEMDATA, wParam, 0 );
	if( lResult != CB_ERR )		
	{
		LB_ITEM * pItemDataExt = (LB_ITEM *) lResult;
		ASSERT( pItemDataExt != NULL );
		if(		pItemDataExt != NULL
			&&	::AfxIsValidAddress(
					static_cast < LPVOID > ( pItemDataExt ),
					sizeof( LB_ITEM )
					)
			)
		{
			delete pItemDataExt;
			pItemDataExt = NULL;
		}
	}
	return DefWindowProc( CB_DELETESTRING, wParam, lParam );
}

LRESULT CExtComboBoxBase::OnCBResetContent( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < GetCount(); nItem++ )
	{
		LRESULT lResult = DefWindowProc( CB_GETITEMDATA, nItem, 0 );
		if( lResult != CB_ERR )		
		{
			LB_ITEM * pItemDataExt = (LB_ITEM *) lResult;
			ASSERT( pItemDataExt != NULL );
			if(		pItemDataExt != NULL
				&&	::AfxIsValidAddress(
						static_cast < LPVOID > ( pItemDataExt ),
						sizeof( LB_ITEM )
						)
				)
			{
				delete pItemDataExt;
				pItemDataExt = NULL;
			}
		}
	}
	return DefWindowProc( CB_RESETCONTENT, wParam, lParam );
}

LRESULT CExtComboBoxBase::OnCBGetItemData( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
LRESULT lResult = DefWindowProc( CB_GETITEMDATA, wParam, lParam );
	if( lResult != CB_ERR )		
	{
		LB_ITEM * pItemDataExt = (LB_ITEM *) lResult;
		ASSERT( pItemDataExt != NULL );
		if(		pItemDataExt != NULL
			&&	::AfxIsValidAddress(
					static_cast < LPVOID > ( pItemDataExt ),
					sizeof( LB_ITEM )
					)
			)
			lResult = pItemDataExt->DataGet();
		else
			lResult = 0L;
	}
	return lResult;
}

LRESULT CExtComboBoxBase::OnCBSetItemData( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
LRESULT lResult = DefWindowProc( CB_GETITEMDATA, wParam, 0 );
	if( lResult != CB_ERR )		
	{
		LB_ITEM * pItemDataExt = (LB_ITEM *) lResult;
		ASSERT( pItemDataExt != NULL );
		if(		pItemDataExt != NULL
			&&	::AfxIsValidAddress(
					static_cast < LPVOID > ( pItemDataExt ),
					sizeof( LB_ITEM )
					)
			)
			pItemDataExt->DataSet( (DWORD)lParam );
	}
	return 0L;
}

void CExtComboBoxBase::DeleteItem( LPDELETEITEMSTRUCT lpDIS ) 
{
	ASSERT_VALID( this );

// DELETEITEMSTRUCT deleteItem;
// 	::memcpy( &deleteItem, lpDIS, sizeof(DELETEITEMSTRUCT) );
// 
// 	// WINBUG: The following if block is required because Windows NT
// 	// version 3.51 does not properly fill out the LPDELETEITEMSTRUCT.
// 	if( deleteItem.itemData == 0 )
// 	{
// 		LRESULT lResult = DefWindowProc( CB_GETITEMDATA, deleteItem.itemID, 0 );
// 		if( lResult != CB_ERR )
// 			deleteItem.itemData = (UINT)lResult;
// 	}
// 
// 	if(		deleteItem.itemData != 0 
// 		&&	deleteItem.itemData != CB_ERR
// 		)
// 	{
// 		LB_ITEM * pItemDataExt = (LB_ITEM *) deleteItem.itemData;
// 		ASSERT( pItemDataExt != NULL );
// 		if(		pItemDataExt != NULL
// 			&&	::AfxIsValidAddress(
// 					static_cast < LPVOID > ( pItemDataExt ),
// 					sizeof( LB_ITEM )
// 					)
// 			)
// 		{
// 			deleteItem.itemData = pItemDataExt->DataGet();
// 			delete pItemDataExt;
// 			pItemDataExt = NULL;
// 		}		
// 	}
// 
// 	CComboBox::DeleteItem( &deleteItem );

	CComboBox::DeleteItem( lpDIS );
}

CSize CExtComboBoxBase::OnPopupListBoxCalcItemExtraSizes(
	LONG nItem
	) const
{
	ASSERT_VALID( this );
	nItem;
	return CSize( 2, 0 );
}

CSize CExtComboBoxBase::OnPopupListBoxCalcItemCellExtraSizes() const
{
	ASSERT_VALID( this );
	return CSize( 4, 0 );
}

LONG CExtComboBoxBase::OnPopupListBoxQueryColumnWidth(
	LONG nColNo
	) const
{
	ASSERT_VALID( this );
LONG nColWidth = LbColumnWidthGet( nColNo );
	if( nColWidth != 0L )
		nColWidth = max( 50, nColWidth ); // minimum width
	return nColWidth;
}

void CExtComboBoxBase::DrawItem( LPDRAWITEMSTRUCT lpDIS ) 
{
	ASSERT_VALID( this );
	ASSERT( lpDIS != NULL && lpDIS->hDC != NULL );

	if(		(GetStyle() & CBS_HASSTRINGS) == 0 
		&&	(GetStyle() & LBS_HASSTRINGS) == 0 
		)
		return;

	INT nItem = lpDIS->itemID;
	if( nItem < 0 || nItem >= GetCount() )
		return;

LRESULT lResult = DefWindowProc( CB_GETITEMDATA, nItem, 0 );
	if( lResult != CB_ERR )
	{
		CDC dc;
		dc.Attach( lpDIS->hDC );
		if( lpDIS->itemAction & (ODA_DRAWENTIRE | ODA_SELECT) )
		{
			CRect rcErase( lpDIS->rcItem );
			if( nItem == (GetCount()-1) )
			{
				CRect rcClient;
				GetClientRect( &rcClient );
				if( rcErase.bottom < rcClient.bottom )
					rcErase.bottom = rcClient.bottom;
			}
			dc.FillSolidRect(
				rcErase, 
				::GetSysColor( COLOR_WINDOW )
				);
		}
		if( lpDIS->itemID >= 0 )
		{
			LB_ITEM * pItemDataExt = (LB_ITEM *) lResult;
			if(		pItemDataExt != NULL
				&&	::AfxIsValidAddress(
						static_cast < LPVOID > ( pItemDataExt ),
						sizeof( LB_ITEM )
						)
				)
			{
				OnPopupListBoxDrawItem( 
					dc,
					lpDIS->rcItem,
					OnPopupListBoxCalcItemExtraSizes( lpDIS->itemID ),
					lpDIS->itemState,
					pItemDataExt 
					);
			}
			else
			{
				CRect rcDrawItem( lpDIS->rcItem );
				bool bSelected = ( lpDIS->itemState & ODS_SELECTED ) ? true : false;
				if( bSelected )
					dc.FillSolidRect(
						&rcDrawItem,
						::GetSysColor( COLOR_HIGHLIGHT )
						);

				CString sText;
				GetLBText( nItem, sText );
				if( ! sText.IsEmpty() )
				{
					COLORREF clrTextOld =
						dc.SetTextColor(
							::GetSysColor( bSelected ? COLOR_HIGHLIGHTTEXT : COLOR_BTNTEXT )
							);
					int nOldBkMode = dc.SetBkMode( TRANSPARENT );
					CRect rcText( rcDrawItem );
					rcText.DeflateRect( 2, 0 );

					UINT nFormat = 
							DT_SINGLELINE
						|	DT_LEFT
						|	DT_VCENTER
						|	DT_NOPREFIX
						;
					if( sText.Find( _T('\t') ) != -1 ) // do tabs expanding
						nFormat |= DT_EXPANDTABS;

					dc.DrawText(
						sText,
						&rcText,
						nFormat
						);

					dc.SetBkMode( nOldBkMode );
					dc.SetTextColor( clrTextOld );
				} // if( ! sText.IsEmpty() )

				if( bSelected )
				{
					COLORREF clrTextOld = 
						dc.SetTextColor( 
							::GetSysColor( COLOR_HIGHLIGHTTEXT ) 
							);
					COLORREF clrBkOld = dc.SetBkColor( RGB(0,0,0) );
					dc.DrawFocusRect( &rcDrawItem );
					dc.SetBkColor( clrBkOld );
					dc.SetTextColor( clrTextOld );
				}	
			}
		}
		dc.Detach();
	} // if( lResult != CB_ERR )
}

void CExtComboBoxBase::MeasureItem( LPMEASUREITEMSTRUCT lpMIS ) 
{
	ASSERT_VALID( this );
	ASSERT( lpMIS != NULL );

	if(		(GetStyle() & CBS_HASSTRINGS) == 0 
		&&	(GetStyle() & LBS_HASSTRINGS) == 0 
		)
		return;
	
	lpMIS->itemWidth = lpMIS->itemHeight = 16;
	INT nItem = lpMIS->itemID;
	if( nItem < 0 || nItem >= GetCount() )
		return;

	OnPopupListBoxMeasureItem( lpMIS );
}

void CExtComboBoxBase::OnPopupListBoxDrawItem( 
	CDC & dc,
	const RECT & rcItem,
	const CSize & szExtra,
	UINT nState,
	const CExtComboBoxBase::LB_ITEM * pLbItem
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

	if(		(GetStyle() & CBS_HASSTRINGS) == 0 
		&&	(GetStyle() & LBS_HASSTRINGS) == 0 
		)
		return;

CRect rcDrawItem( rcItem );

bool bSelected = ( nState & ODS_SELECTED ) ? true : false;
	if( bSelected )
		dc.FillSolidRect(
			&rcDrawItem,
			::GetSysColor( COLOR_HIGHLIGHT )
			);

INT nOldBkMode = dc.SetBkMode( TRANSPARENT );

CRect rcText( rcDrawItem );
	rcText.DeflateRect( szExtra );

	// draw item cells
	if( pLbItem != NULL )
	{
		INT nCount = pLbItem->LbItemCellGetCount();
		for( INT nCellItem = 0; nCellItem < nCount; nCellItem++ )
		{
			LB_ITEM::CELL * pItemCell = pLbItem->LbItemCellGet( nCellItem );
			ASSERT( pItemCell != NULL );
			
			LONG nColWidth = OnPopupListBoxQueryColumnWidth( nCellItem );
			if( nColWidth > 0L )
			{
				if(		pItemCell->m_clrBack != COLORREF(-1L) 
					&&	(!bSelected)
					)
					dc.FillSolidRect(
						&rcText,
						pItemCell->m_clrBack
						);
				
				CString sText = pItemCell->m_sItemText;
				if( !sText.IsEmpty() )
				{
					COLORREF clrText = 
						(pItemCell->m_clrText != COLORREF(-1L) && (!bSelected))
							? pItemCell->m_clrText
							: ::GetSysColor( bSelected ? COLOR_HIGHLIGHTTEXT : COLOR_BTNTEXT );
					COLORREF clrTextOld = dc.SetTextColor( clrText );
					
					UINT nFormat = 
						DT_SINGLELINE
						|	DT_LEFT
						|	DT_VCENTER
						|	DT_NOPREFIX
						;
					if( sText.Find( _T('\t') ) != -1 ) // do tabs expanding
						nFormat |= DT_EXPANDTABS;
					
					CRect rcItemText( rcText );
					if( nColWidth > 0 )
						rcItemText.right = rcItemText.left + nColWidth;
					
					dc.DrawText(
						sText,
						&rcItemText,
						(nFormat|DT_END_ELLIPSIS)
						);
					
					dc.SetTextColor( clrTextOld );
					
				} // if( ! sText.IsEmpty() )
				
				if( nColWidth > 0 )
					rcText.left += nColWidth;
			}

		} // for( ...
	} // if( pLbItem != NULL )

	dc.SetBkMode( nOldBkMode );

	if( bSelected )
	{
		COLORREF clrTextOld = 
			dc.SetTextColor( 
				::GetSysColor( COLOR_HIGHLIGHTTEXT ) 
				);
		COLORREF clrBkOld = dc.SetBkColor( RGB(0,0,0) );
		::DrawFocusRect( dc, &rcDrawItem );
		dc.SetBkColor( clrBkOld );
		dc.SetTextColor( clrTextOld );
	}
}

void CExtComboBoxBase::OnPopupListBoxMeasureItem( LPMEASUREITEMSTRUCT lpMIS )
{
	ASSERT_VALID( this );
	ASSERT( lpMIS != NULL );

	if(		(GetStyle() & CBS_HASSTRINGS) == 0 
		&&	(GetStyle() & LBS_HASSTRINGS) == 0 
		)
		return;
	
	lpMIS->itemWidth = lpMIS->itemHeight = 16;
	INT nItem = lpMIS->itemID;
	if( nItem < 0 || nItem >= GetCount() )
		return;

HFONT hFont = OnQueryFont();
	ASSERT( hFont != NULL );

INT nHeight = 0;
INT nWidth = 0;

LB_ITEM * pLbItem = LbItemGet( nItem );
	if( pLbItem != NULL )
	{
		INT nCount = pLbItem->LbItemCellGetCount();
		for( INT nCellItem = 0; nCellItem < nCount; nCellItem++ )
		{
			LB_ITEM::CELL * pItemCell = pLbItem->LbItemCellGet( nCellItem );
			if( pItemCell != NULL )
			{
				CExtSafeString sText = pItemCell->m_sItemText;
				if( !sText.IsEmpty() )
				{
					UINT nFormat = 
							DT_SINGLELINE
						|	DT_LEFT
						|	DT_VCENTER
						|	DT_NOPREFIX
						;
					if( sText.Find( _T('\t') ) != -1 ) // do tabs expanding
						nFormat |= DT_EXPANDTABS;

					CWindowDC dc( NULL );
					CRect rcMeasure =
						CExtPaintManager::stat_CalcTextDimension( 
							dc, 
							*CFont::FromHandle( hFont ), 
							sText,
							nFormat
							);

					nHeight = max( nHeight, rcMeasure.Height() );
					nWidth += rcMeasure.Width();
					CSize szExtra = 
						OnPopupListBoxCalcItemCellExtraSizes();
					nWidth += szExtra.cx;
				}
			} // if( pItemCell != NULL )
		}
	}
	else
	{
		CExtSafeString sMeasure;
		GetLBText( nItem, sMeasure );
		if( sMeasure.IsEmpty() )
			return;
		CWindowDC dc( NULL );
		CRect rcMeasure =
			CExtPaintManager::stat_CalcTextDimension( 
				dc, 
				*CFont::FromHandle( hFont ), 
				sMeasure,
				DT_SINGLELINE | DT_LEFT | DT_NOPREFIX
				);
		
		nWidth = rcMeasure.Width();
		nHeight = rcMeasure.Height();
	}
	
	lpMIS->itemWidth = nWidth;
	lpMIS->itemHeight = max( nHeight, (INT)lpMIS->itemHeight );

	CSize szExtra = 
		OnPopupListBoxCalcItemExtraSizes( nItem );
	lpMIS->itemWidth += szExtra.cx * 2;
	lpMIS->itemHeight += szExtra.cy * 2;
}

HFONT CExtComboBoxBase::OnQueryFont() const
{
	ASSERT_VALID( this );
	HFONT hFont = (HFONT)
		::SendMessage( m_hWnd, WM_GETFONT, 0L, 0L );
	if( hFont == NULL )
	{
		HWND hWndParent = ::GetParent( m_hWnd );
		if( hWndParent != NULL )
			hFont = (HFONT)
				::SendMessage( hWndParent, WM_GETFONT, 0L, 0L );
	} // if( hFont == NULL )
	if( hFont == NULL )
	{
		hFont = (HFONT)::GetStockObject( DEFAULT_GUI_FONT );
		if( hFont == NULL )
			hFont = (HFONT)::GetStockObject( SYSTEM_FONT );
	} // if( hFont == NULL )
	return hFont;
}

CExtComboBoxBase::LB_ITEM * CExtComboBoxBase::LbItemGet( 
	LONG nItem
	) const
{
	ASSERT_VALID( this );
	if(		nItem < 0 
		||	nItem >= GetCount() 
		)
		return NULL;
CExtComboBoxBase * pThis =
		const_cast < CExtComboBoxBase * > ( this );
LRESULT lResult = pThis->DefWindowProc( CB_GETITEMDATA, nItem, 0 );
	if( lResult != CB_ERR )		
	{
		LB_ITEM * pItemDataExt = (LB_ITEM *) lResult;
		// ASSERT( pItemDataExt != NULL );
		if(		pItemDataExt != NULL
			&&	::AfxIsValidAddress(
					static_cast < LPVOID > ( pItemDataExt ),
					sizeof( LB_ITEM )
					)
			)
			return pItemDataExt;
	}
	return NULL;
}

LONG CExtComboBoxBase::LbColumnWidthGet( 
	LONG nColNo
	) const
{
	ASSERT_VALID( this );
LONG nWidth = -1L;
	if( ! m_mapColumnsWidth.Lookup( nColNo, nWidth ) )
		return -1L;
	return nWidth;
}

void CExtComboBoxBase::LbColumnWidthSet( 
	LONG nColNo,
	LONG nWidth
	)
{
	ASSERT_VALID( this );
	m_mapColumnsWidth.SetAt( nColNo, nWidth );
}

#define UPDATE_TIMER		1
#define UPDATE_TIMER_PERIOD	10

/////////////////////////////////////////////////////////////////////////////
// CExtComboBox

IMPLEMENT_DYNCREATE( CExtComboBox, CExtComboBoxBase );

CExtComboBox::CExtComboBox()
	: CExtComboBoxBase()
	, m_bLButtonDown( false )
	, m_bPainted( false )
	, m_dwLastStateCode( 0 )
{
}

CExtComboBox::~CExtComboBox()
{
CExtAnimationSite * pAcAS = AnimationClient_SiteGet();
	if( pAcAS != NULL )
		pAcAS->AnimationSite_ClientRemove( this );
}

BEGIN_MESSAGE_MAP(CExtComboBox, CExtComboBoxBase)
	//{{AFX_MSG_MAP(CExtComboBox)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_SETCURSOR()
	ON_WM_ERASEBKGND()
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_CONTROL_REFLECT(CBN_CLOSEUP, OnCloseup)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtComboBox message handlers

BOOL CExtComboBox::OnSetCursor( CWnd * pWnd, UINT nHitTest, UINT message )
{
	if( message == WM_MOUSEMOVE )
	{
		CRect rcWnd;
		GetWindowRect( &rcWnd );
		if( rcWnd.PtInRect( GetCurrentMessage()->pt ) )
		{
			SetTimer( UPDATE_TIMER, UPDATE_TIMER_PERIOD, NULL );
			OnTimer( UPDATE_TIMER );
		}
	}
	return CExtComboBoxBase::OnSetCursor(pWnd, nHitTest, message);
}

void CExtComboBox::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_bLButtonDown = true;
	CExtComboBoxBase::OnLButtonDown(nFlags, point);
}

void CExtComboBox::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_bLButtonDown = false;
	Invalidate();
	CExtComboBoxBase::OnLButtonUp(nFlags, point);
}

bool CExtComboBox::IsFocused() const
{
bool bFocused = false;
HWND hWndFocus = ::GetFocus();
	if(		hWndFocus == GetSafeHwnd()
		||	::IsChild( GetSafeHwnd(), hWndFocus )
		)
		bFocused = true;
	return bFocused;
}

bool CExtComboBox::IsHovered() const
{
CPoint ptCursorPos( 0, 0 );
	::GetCursorPos( &ptCursorPos );
CRect rcItem;
	GetWindowRect( &rcItem );
bool bHovered = false;
	if( rcItem.PtInRect(ptCursorPos) )
		bHovered = true;
	return bHovered;
}

void CExtComboBox::OnSetFocus( CWnd * pOldWnd ) 
{
	CExtComboBoxBase::OnSetFocus( pOldWnd );
	Invalidate();	
}

void CExtComboBox::OnKillFocus( CWnd * pNewWnd ) 
{
	CExtComboBoxBase::OnKillFocus( pNewWnd );
	Invalidate();	
}

void CExtComboBox::OnCloseup()
{
	Invalidate();
}

void CExtComboBox::OnTimer( __EXT_MFC_UINT_PTR nIDEvent )
{
	if( UPDATE_TIMER == nIDEvent )
	{
		CClientDC dc( this );
		if( m_bLButtonDown )
		{
			KillTimer( UPDATE_TIMER );
			if( m_bPainted )
			{
				_OnDrawComboImpl( 
					GetDroppedState() ? true : false, 
					IsHovered(), 
					&dc 
					);
				m_bPainted = false;
			}
		}
		else if( !IsHovered() && !IsFocused() )
		{
			KillTimer( UPDATE_TIMER );
			if( m_bPainted == TRUE )
			{
				_OnDrawComboImpl( 
					false, 
					false, 
					&dc 
					);
				m_bPainted = false;
			}
		}
		else
		{
			if( !m_bPainted )
			{
				m_bPainted = true;
				_OnDrawComboImpl( 
					GetDroppedState() ? true : false, 
					true, 
					&dc 
					);
			}
			else
				return;
		}
		return;
	}
	if( AnimationSite_OnHookTimer( UINT(nIDEvent) ) )
		return;
	CExtComboBoxBase::OnTimer( nIDEvent );
}

BOOL CExtComboBox::OnEraseBkgnd(CDC* pDC)
{
	pDC;
	return TRUE;
}

void CExtComboBox::OnPaint()
{
	if( ( GetExStyle() & (WS_EX_DLGMODALFRAME|WS_EX_CLIENTEDGE|WS_EX_STATICEDGE) ) != 0 )
		ModifyStyleEx(
			WS_EX_DLGMODALFRAME|WS_EX_CLIENTEDGE|WS_EX_STATICEDGE,
			0,
			SWP_FRAMECHANGED
			);
	
DWORD dwWndStyle = GetStyle();
	if(		(dwWndStyle&(CBS_OWNERDRAWFIXED|CBS_OWNERDRAWVARIABLE)) != 0 
		&&	(dwWndStyle & 0x0003L) == CBS_DROPDOWNLIST
		)
	{
		Default();
		CClientDC dc( this );
		_OnDrawComboImpl( 
			GetDroppedState() ? true : false, 
			IsHovered(), 
			&dc 
			);
		return;
	}

CPaintDC dcPaint(this);
	CExtPaintManager::stat_ExcludeChildAreas(
		dcPaint.GetSafeHdc(),
		GetSafeHwnd()
		);
CRect rcClient;
	GetClientRect( &rcClient );
CExtMemoryDC dc(
		&dcPaint,
		&rcClient
		);
	DefWindowProc( WM_PAINT, (WPARAM)dc.GetSafeHdc(), (LPARAM)0 );

DWORD dwStyle = GetStyle();
	if(		(dwStyle&CBS_SIMPLE) == CBS_SIMPLE 
		&&	(dwStyle&CBS_DROPDOWN) != CBS_DROPDOWN
		&&	(dwStyle&CBS_DROPDOWNLIST) != CBS_DROPDOWNLIST
		)
	{
		if(		(! PmBridge_GetPM()->GetCb2DbTransparentMode(this) )
			||	(! PmBridge_GetPM()->PaintDockerBkgnd( true, dc, this ) )
			)
			dc.FillSolidRect(
				&rcClient,
				PmBridge_GetPM()->GetColor(
					CExtPaintManager::CLR_3DFACE_OUT, this
					)
				);
	}

	_OnDrawComboImpl( 
		GetDroppedState() ? true : false, 
		IsHovered(), 
		&dc 
		);

}

void CExtComboBox::_OnDrawComboImpl(
	bool bPressed,
	bool bHover,
	CDC * pDC // = NULL
	)
{
	if( AnimationClient_StatePaint( *pDC ) )
		return;

DWORD dwStyle = GetStyle();
	if(		(dwStyle&CBS_SIMPLE) == CBS_SIMPLE 
		&&	(dwStyle&CBS_DROPDOWN) != CBS_DROPDOWN
		&&	(dwStyle&CBS_DROPDOWNLIST) != CBS_DROPDOWNLIST
		)
		return;

CRect rcClient;
	GetClientRect( &rcClient );
bool bCallReleaseDC = false;
	if( pDC == NULL )
	{
		pDC = GetDC();
		ASSERT( pDC != NULL );
		bCallReleaseDC = true;
	}

bool bEnabled = IsWindowEnabled() ? true : false;
bool bPushed = ( bPressed || GetDroppedState() ) ? true : false;
	if( !bEnabled )
	{
		bPushed = false;
		bHover = false;
	}
	if( CExtPopupMenuWnd::IsMenuTracking() )
		bHover = false;
	else if( !IsHovered() )
	{
		if( IsFocused() )
			bHover = true;
	}
	
	// erase are of the button that contains the drop-down arrow
CRect rcDDButton( rcClient );
	rcDDButton.left = rcDDButton.right - ::GetSystemMetrics( SM_CXHTHUMB );
	rcDDButton.left -= 2;
	if( rcDDButton.left > rcDDButton.right )
		rcDDButton.left = rcDDButton.right;

//	struct __EXT_MFC_COMBOBOXINFO
//	{
//		DWORD cbSize;
//		RECT  rcItem;
//		RECT  rcButton;
//		DWORD stateButton;
//		HWND  hwndCombo;
//		HWND  hwndItem;
//		HWND  hwndList;
//	};
//
//	__EXT_MFC_COMBOBOXINFO pcbi;
//	::memset( &pcbi, 0, sizeof( __EXT_MFC_COMBOBOXINFO ) ) ;
//	pcbi.cbSize = sizeof( __EXT_MFC_COMBOBOXINFO );
//
//#if (defined CB_GETCOMBOBOXINFO)	
//	ASSERT( CB_GETCOMBOBOXINFO ==  0x0164 );
//#endif
//
//	if( SendMessage(
//			(UINT)  0x0164,
//			(WPARAM) 0L,
//			(LPARAM) &pcbi
//			) > 0L 
//		)
//	{
//		rcDDButton = pcbi.rcButton;
//	}

COLORREF clrBk = GetBkColor();
	if( clrBk == COLORREF(-1L) )
	{
		COLORREF clrSysBk =
			PmBridge_GetPM()->GetColor( 
				( ! IsWindowEnabled() )
					? COLOR_3DFACE
					: COLOR_WINDOW,
				this
			);
		clrBk = clrSysBk;
	}
	pDC->FillSolidRect( 
		&rcDDButton, 
		clrBk
		);

	// fix for disabled edit border inside combo box
	if(		(!bEnabled)
		&&	(dwStyle&CBS_DROPDOWN) == CBS_DROPDOWN 
		)
	{
		CWnd * pWndChild = GetWindow( GW_CHILD );
		if( pWndChild != NULL )
		{
			ASSERT_VALID( pWndChild );
			static const TCHAR szEdit[] = _T("edit");
			TCHAR szCompare[ sizeof(szEdit)/sizeof(szEdit[0]) + 1 ] = _T("");
			::GetClassName( 
				pWndChild->GetSafeHwnd(), 
				szCompare, 
				sizeof(szCompare)/sizeof(szCompare[0]) 
				);
			if( !lstrcmpi( szCompare, szEdit ) )
			{
				CRect rcChild;
				pWndChild->GetWindowRect( &rcChild );
				ScreenToClient( rcChild );
				rcChild.InflateRect(1,1);
				pDC->Draw3dRect( 
					&rcChild, 
					clrBk,
					clrBk
					);
			}
		}
	}

CExtPaintManager::PAINTCOMBOFRAMEDATA _pcfd(
		this,
		rcClient,
		bHover,
		bPushed,
		bEnabled
		);
	PmBridge_GetPM()->PaintComboFrame( *pDC, _pcfd );

DWORD dwLastStateCode = 0;
	if( m_bLButtonDown )
		dwLastStateCode |= 0x01;
	if( IsHovered() )
		dwLastStateCode |= 0x02;
	if( IsFocused() )
		dwLastStateCode |= 0x04;
	if( bPressed )
		dwLastStateCode |= 0x08;
	if( m_dwLastStateCode != dwLastStateCode )
	{
		if( AnimationClient_StateGet( true ).FromSurface( pDC->m_hDC, rcClient ) )
		{
			if( AnimationClient_StateGet( false ).IsEmpty() )
				AnimationClient_StateGet( false ) = AnimationClient_StateGet( true );
			else
				AnimationSite_OnClientStateChanged( this, 0 );
		}
		m_dwLastStateCode = dwLastStateCode;
	}

	if( bCallReleaseDC )
		ReleaseDC( pDC );
}

LRESULT CExtComboBox::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if( message == WM_PRINT || message == WM_PRINTCLIENT )
	{
		CDC * pDC = CDC::FromHandle( (HDC)wParam );
		
		CRect rcClient;
		GetClientRect( &rcClient );

		CExtMemoryDC dc(
			pDC,
			&rcClient
			);

		DWORD dwStyle = GetStyle();
		if(		(dwStyle&CBS_SIMPLE) == CBS_SIMPLE 
			&&	(dwStyle&CBS_DROPDOWN) != CBS_DROPDOWN
			&&	(dwStyle&CBS_DROPDOWNLIST) != CBS_DROPDOWNLIST
			)
		{
			if(		(! PmBridge_GetPM()->GetCb2DbTransparentMode(this) )
				||	(! PmBridge_GetPM()->PaintDockerBkgnd( true, dc, this ) )
				)
				dc.FillSolidRect(
					&rcClient,
					PmBridge_GetPM()->GetColor(
						CExtPaintManager::CLR_3DFACE_OUT, this
						)
					);
		}
		
		_OnDrawComboImpl( 
			GetDroppedState() ? true : false, 
			IsHovered(), 
			&dc 
			);

		if( (lParam&PRF_CHILDREN) != 0 )
			CExtPaintManager::stat_PrintChildren(
				m_hWnd,
				message,
				dc.GetSafeHdc(),
				lParam,
				false
				);

		return (!0);
	}

	return CExtComboBoxBase::WindowProc(message, wParam, lParam);
}

void CExtComboBox::AnimationSite_OnProgressShutdownTimer( UINT nTimerID )
{
	ASSERT_VALID( this );
	CExtComboBoxBase::AnimationSite_OnProgressShutdownTimer( nTimerID );
}

const CExtAnimationParameters *
	CExtComboBox::AnimationClient_OnQueryAnimationParameters(
		INT eAPT // __EAPT_*** anumation type
		) const
{
	ASSERT_VALID( this );
	eAPT;
	return (&g_PaintManager->g_DefAnimationParametersEmpty);
// const CExtAnimationParameters * pAnimationParameters =
// 		g_PaintManager->Animation_GetParameters(
// 			eAPT,
// 			(CObject*)this,
// 			this
// 			);
// 	return pAnimationParameters;
}

//bool CExtComboBox::AnimationClient_CacheNextState(
//	CDC & dc,
//	const RECT & rcAcAnimationTarget,
//	bool bAnimate,
//	INT eAPT // __EAPT_*** anumation type
//	)
//{
//	ASSERT_VALID( this );
//	ASSERT_VALID( (&dc) );
//	ASSERT( dc.GetSafeHdc() != NULL );
//	ASSERT( AnimationClient_CacheGeneratorIsLocked() );
//
//DWORD dwWndStyle = GetStyle();
//	if(		(dwWndStyle&(CBS_OWNERDRAWFIXED|CBS_OWNERDRAWVARIABLE)) != 0 
//		&&	(dwWndStyle & 0x0003L) == CBS_DROPDOWNLIST
//		)
//	{
//		DefWindowProc( WM_PAINT, (WPARAM)dc.GetSafeHdc(), (LPARAM)0 );
//		_OnDrawComboImpl( 
//			GetDroppedState() ? true : false, 
//			IsHovered(), 
//			&dc 
//			);
//		return
//			CExtComboBoxBase::AnimationClient_CacheNextState(
//				dc,
//				rcAcAnimationTarget,
//				bAnimate,
//				eAPT
//				);
//	}
//
//	CExtPaintManager::stat_ExcludeChildAreas(
//		dc.GetSafeHdc(),
//		GetSafeHwnd()
//		);
//CRect rcClient;
//	GetClientRect( &rcClient );
//	DefWindowProc( WM_PAINT, (WPARAM)dc.GetSafeHdc(), (LPARAM)0 );
//
//DWORD dwStyle = GetStyle();
//	if(		(dwStyle&CBS_SIMPLE) == CBS_SIMPLE 
//		&&	(dwStyle&CBS_DROPDOWN) != CBS_DROPDOWN
//		&&	(dwStyle&CBS_DROPDOWNLIST) != CBS_DROPDOWNLIST
//		)
//	{
//		if(		(! PmBridge_GetPM()->GetCb2DbTransparentMode(this) )
//			||	(! PmBridge_GetPM()->PaintDockerBkgnd( true, dc, this ) )
//			)
//			dc.FillSolidRect(
//				&rcClient,
//				PmBridge_GetPM()->GetColor(
//					CExtPaintManager::CLR_3DFACE_OUT, this
//					)
//				);
//	}
//
//	_OnDrawComboImpl( 
//		GetDroppedState() ? true : false, 
//		IsHovered(), 
//		&dc 
//		);
//
//	return
//		CExtComboBoxBase::AnimationClient_CacheNextState(
//			dc,
//			rcAcAnimationTarget,
//			bAnimate,
//			eAPT
//			);
//}



