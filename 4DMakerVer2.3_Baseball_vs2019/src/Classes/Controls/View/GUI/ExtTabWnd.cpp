// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_TAB_CTRL)

	#if (!defined __AFXPRIV_H__)
		#include <AfxPriv.h>
	#endif 

	#if _MFC_VER < 0x700
		#include <../src/AfxImpl.h>
	#else
		#include <../src/mfc/AfxImpl.h>
	#endif

	#if (!defined __EXT_TABWND_H)
		#include <ExtTabWnd.h>
	#endif

	#if (!defined __EXT_PAINT_MANAGER_H)
		#include <ExtPaintManager.h>
	#endif

	#if (!defined __EXT_MEMORY_DC_H)
		#include <../Src/ExtMemoryDC.h>
	#endif

	#if (!defined __EXT_CONTROLBAR_H)
		#include <ExtControlBar.h>
	#endif

	#if (!defined __EXT_POPUP_MENU_WND_H)
		#include <ExtPopupMenuWnd.h>
	#endif

	#if (!defined __EXT_LOCALIZATION_H)
		#include <ExtLocalization.h>
	#endif

	#include <Resources/Resource.h>

	#if (!defined __EXT_CONTROLBAR_H)
		#include <ExtControlBar.h>
	#endif

#endif // (!defined __EXT_MFC_NO_TAB_CTRL )

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
	#if (!defined __EXTCUSTOMIZE_H)
		#include <ExtCustomize.h>
	#endif
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#if (!defined __EXT_MFC_NO_TAB_CTRL)

/////////////////////////////////////////////////////////////////////////////
// CExtTabWnd

CExtTabWnd::TAB_ITEM_INFO::TAB_ITEM_INFO(
	CExtTabWnd * pWndTab,  // = NULL
	TAB_ITEM_INFO * pPrev, // = NULL
	TAB_ITEM_INFO * pNext, // = NULL
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL
	const CExtCmdIcon * pIcon, // = NULL // CHANGED 2.53
	DWORD dwStyle, // = 0 // visible & non-group
	LPARAM lParam, // = 0
	__EXT_MFC_SAFE_LPCTSTR sTooltipText // = NULL
	)
	: m_pWndTab( pWndTab )
	, m_pPrev( pPrev )
	, m_pNext( pNext )
	, m_sText( ( sText == NULL ) ? _T("") : sText )
	, m_dwItemStyle( dwStyle )
	, m_dwItemStyleEx( 0 )
	, m_sizeLastMeasuredItem( __EXTTAB_MIN_HORZ_WIDTH, __EXTTAB_MIN_HORZ_HEIGHT )
	, m_sizeLastMeasuredText( 0, 0 )
	, m_sizeLastMeasuredIcon( 0, 0 )
	, m_rcItem( 0, 0, __EXTTAB_MIN_HORZ_WIDTH, __EXTTAB_MIN_HORZ_HEIGHT )
	, m_rcCloseButton( 0, 0, 0, 0 )
	, m_lParam( lParam )
	, m_sTooltipText( ( sTooltipText == NULL ) ? _T("") : sTooltipText )
{
	if( pIcon != NULL && ( ! pIcon->IsEmpty() ) )
		m_icon = (*pIcon);
	if( m_pPrev != NULL )
		m_pPrev->m_pNext = this;
	if( m_pNext != NULL )
		m_pNext->m_pPrev = this;
}

CExtTabWnd::TAB_ITEM_INFO::TAB_ITEM_INFO(
	const TAB_ITEM_INFO & other
	)
	: m_pWndTab( NULL )
	, m_pPrev( NULL )
	, m_pNext( NULL )
	, m_sText( _T("") )
	, m_dwItemStyle( 0 )
	, m_dwItemStyleEx( 0 )
	, m_sizeLastMeasuredItem( __EXTTAB_MIN_HORZ_WIDTH, __EXTTAB_MIN_HORZ_HEIGHT )
	, m_sizeLastMeasuredText( 0, 0 )
	, m_sizeLastMeasuredIcon( 0, 0 )
	, m_rcItem( 0, 0, __EXTTAB_MIN_HORZ_WIDTH, __EXTTAB_MIN_HORZ_HEIGHT )
	, m_rcCloseButton( 0, 0, 0, 0 )
	, m_lParam( 0 )
	, m_bHelperToolTipAvail( false )
	, m_bMultiRowWrap( false )
	, m_sTooltipText( _T("") )
{
	_AssignFromOther( other );
}

CExtTabWnd::TAB_ITEM_INFO::~TAB_ITEM_INFO()
{
}

void CExtTabWnd::TAB_ITEM_INFO::_AssignFromOther(
	const TAB_ITEM_INFO & other
	)
{
	ASSERT_VALID( (&other) );
	m_pWndTab = other.m_pWndTab;
	m_dwItemStyle = other.m_dwItemStyle;
	m_dwItemStyleEx = other.m_dwItemStyleEx;
	m_sText = other.m_sText;
	m_sTooltipText = other.m_sTooltipText;
	m_icon = other.m_icon;
	m_pPrev = other.m_pPrev;
	m_pNext = other.m_pNext;
	m_sizeLastMeasuredItem = other.m_sizeLastMeasuredItem;
	m_sizeLastMeasuredText = other.m_sizeLastMeasuredText;
	m_sizeLastMeasuredIcon = other.m_sizeLastMeasuredIcon;
	m_rcItem = other.m_rcItem;
	m_rcCloseButton = other.m_rcCloseButton;
	m_lParam = other.m_lParam;
	m_bHelperToolTipAvail = other.m_bHelperToolTipAvail;
	m_bMultiRowWrap = other.m_bMultiRowWrap;
	ASSERT_VALID( this );
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::TAB_ITEM_INFO::TextGet() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );

__EXT_MFC_SAFE_LPCTSTR sExternItemText =
		m_pWndTab->OnTabWndQueryItemText( this );
	if( sExternItemText != NULL )
		return sExternItemText;

	return m_sText;
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::TAB_ITEM_INFO::TooltipTextGet() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
__EXT_MFC_SAFE_LPCTSTR sTooltipItemText =
		m_pWndTab->OnTabWndQueryItemTooltipText( this );
	if(		sTooltipItemText != NULL
		&&	_tcslen(sTooltipItemText) > 0
		)
		return sTooltipItemText;
	return m_sTooltipText;
}

CSize CExtTabWnd::TAB_ITEM_INFO::IconGetSize() const
{
	ASSERT_VALID( this );

CExtCmdIcon * pExternIcon = m_pWndTab->OnTabWndQueryItemIcon( this );
	if( pExternIcon != NULL && (! pExternIcon->IsEmpty()) )
	{
		CSize _sizeIcon = pExternIcon->GetSize();
		return _sizeIcon;
	}

	return m_icon.GetSize();
}

CExtCmdIcon * CExtTabWnd::TAB_ITEM_INFO::IconGetPtr() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );

CExtCmdIcon * pExternIcon = m_pWndTab->OnTabWndQueryItemIcon( this );
	if( pExternIcon != NULL )
		return pExternIcon;

	return (CExtCmdIcon *)&m_icon;
}

CSize CExtTabWnd::TAB_ITEM_INFO::Measure(
	CDC * pDcMeasure // = NULL
	)
{
	ASSERT_VALID( this );

CDC * pDC = pDcMeasure;
	if( pDcMeasure == NULL )
	{
		pDC = new CWindowDC( NULL );
		ASSERT( pDC != NULL );
	}
	ASSERT( pDC->GetSafeHdc() != NULL );

CExtTabWnd * pWndTab = GetTabWnd();
	ASSERT_VALID( pWndTab );

bool bSelected = SelectedGet();
//bool bSelected = pWndTab->SelectionBoldGet();

CFont & font =
		pWndTab->_GetTabWndFont(
			bSelected,
			__ETWS_ORIENT_TOP
			);
	ASSERT( font.GetSafeHandle() != NULL );

CExtSafeString sText = TextGet();
CRect rcMeasureText =
		CExtPaintManager::stat_CalcTextDimension(
			*pDC,
			font,
			sText
			);
	m_sizeLastMeasuredText = rcMeasureText.Size();
	if( pWndTab->OnTabWndQueryAlignFontBasedDifference() )
	{
		CFont & fontC =
			pWndTab->_GetTabWndFont(
				!bSelected,
				__ETWS_ORIENT_TOP
				);
		ASSERT( font.GetSafeHandle() != NULL );
		CRect rcMeasureText =
			CExtPaintManager::stat_CalcTextDimension(
				*pDC,
				fontC,
				sText
				);
		CSize _size = rcMeasureText.Size();
		m_sizeLastMeasuredText.cx =
			max( _size.cx, m_sizeLastMeasuredText.cx );
		m_sizeLastMeasuredText.cy =
			max( _size.cy, m_sizeLastMeasuredText.cy );
	}
	m_sizeLastMeasuredIcon = IconGetSize();

CSize _sizeTab( 0, 0 );
bool bVertical = pWndTab->OrientationIsVertical();
	if( bVertical )
	{
		m_sizeLastMeasuredText.cx += 10;
		m_sizeLastMeasuredText.cy += 2;
		_sizeTab.cx =
			max(  m_sizeLastMeasuredIcon.cx, m_sizeLastMeasuredText.cy );
		_sizeTab.cy =
			m_sizeLastMeasuredIcon.cy
			+ ((m_sizeLastMeasuredIcon.cy > 0) ? __EXTTAB_MARGIN_ICON2TEXT_Y : 0)
			+ m_sizeLastMeasuredText.cx
			+ __EXTTAB_SEPARATOR_AREA_Y
			;

		_sizeTab.cx += __EXTTAB_MARGIN_BORDER_VX * 2;
		_sizeTab.cy += __EXTTAB_MARGIN_BORDER_VY * 2;

		CSize _sizeCloseButton =
			pWndTab->OnTabWndQueryItemCloseButtonSize( this );
		if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )
		{
			_sizeTab.cx =
				max( _sizeTab.cx, _sizeCloseButton.cx );
			_sizeTab.cy += _sizeCloseButton.cy + __EXTTAB_MARGIN_ICON2TEXT_Y;
		} // if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )

		_sizeTab.cx =
			max( _sizeTab.cx, __EXTTAB_MIN_VERT_WIDTH );
		_sizeTab.cy =
			max( _sizeTab.cy, __EXTTAB_MIN_VERT_HEIGHT );
	} // if( bVertical )
	else
	{
		_sizeTab.cx =
			m_sizeLastMeasuredIcon.cx
			+ ((m_sizeLastMeasuredIcon.cx > 0) ? __EXTTAB_MARGIN_ICON2TEXT_X : 0)
			+ m_sizeLastMeasuredText.cx
			+ __EXTTAB_SEPARATOR_AREA_X
			;
		_sizeTab.cy =
			max(  m_sizeLastMeasuredIcon.cy, m_sizeLastMeasuredText.cy );

		_sizeTab.cx += __EXTTAB_MARGIN_BORDER_HX * 2;
		_sizeTab.cy += __EXTTAB_MARGIN_BORDER_HY * 2;

		CSize _sizeCloseButton =
			pWndTab->OnTabWndQueryItemCloseButtonSize( this );
		if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )
		{
			_sizeTab.cx += _sizeCloseButton.cx + __EXTTAB_MARGIN_ICON2TEXT_X;
			_sizeTab.cy =
				max( _sizeTab.cy, _sizeCloseButton.cy );
		} // if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )

		_sizeTab.cx =
			max( _sizeTab.cx, __EXTTAB_MIN_HORZ_WIDTH );
		_sizeTab.cy =
			max( _sizeTab.cy, __EXTTAB_MIN_HORZ_HEIGHT );
	} // else from if( bVertical )

	pWndTab->OnTabWndUpdateItemMeasure(
		this,
		*pDC,
		_sizeTab
		);

	if( pDcMeasure == NULL )
	{
		ASSERT( pDC != NULL );
		delete pDC;
	}

	m_sizeLastMeasuredItem = _sizeTab;
	return _sizeTab;
}

bool CExtTabWnd::TAB_ITEM_INFO::HitTest(
	const POINT & ptClient
	) const
{
	if( !VisibleGet() )
		return false;
const CRect & rcItem = ItemRectGet();
	if( rcItem.IsRectEmpty() )
		return false;
	if( !rcItem.PtInRect(ptClient) )
		return false;
	return true;
}

CExtTabWnd * CExtTabWnd::TAB_ITEM_INFO::GetTabWnd()
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return m_pWndTab;
}

const CExtTabWnd * CExtTabWnd::TAB_ITEM_INFO::GetTabWnd() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return m_pWndTab;
}

void CExtTabWnd::TAB_ITEM_INFO::TextSet(
	__EXT_MFC_SAFE_LPCTSTR sText // = NULL
	)
{
	ASSERT_VALID( this );
	m_sText = ( sText == NULL ) ? _T("") : sText;
}

void CExtTabWnd::TAB_ITEM_INFO::TooltipTextSet(
	__EXT_MFC_SAFE_LPCTSTR sTooltipText // = NULL
	)
{
	ASSERT_VALID( this );
	m_sTooltipText = ( sTooltipText == NULL ) ? _T("") : sTooltipText;
}

void CExtTabWnd::TAB_ITEM_INFO::IconSet(
	const CExtCmdIcon & _icon
	)
{
	ASSERT_VALID( this );
	m_icon = _icon;
}

void CExtTabWnd::TAB_ITEM_INFO::IconSet(
	HICON hIcon, // = NULL
	bool bCopyIcon // = true
	)
{
	ASSERT_VALID( this );
	if( hIcon == NULL )
		m_icon.Empty();
	else
		m_icon.AssignFromHICON( hIcon, bCopyIcon );
}

DWORD CExtTabWnd::TAB_ITEM_INFO::GetItemStyle() const
{
	ASSERT_VALID( this );
DWORD dwItemStyle = m_dwItemStyle;
	if( m_pWndTab != NULL )
	{
		ASSERT_VALID( m_pWndTab );
		m_pWndTab->_OnTabItemHook_GetItemStyle( *this, dwItemStyle );
	}
	return dwItemStyle;
}

DWORD CExtTabWnd::TAB_ITEM_INFO::ModifyItemStyle(
	DWORD dwRemove,
	DWORD dwAdd // = 0
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );

	if( m_pWndTab != NULL )
	{
		ASSERT_VALID( m_pWndTab );
		m_pWndTab->_OnTabItemHook_ModifyItemStyle( *this, m_dwItemStyle, dwRemove, dwAdd );
	}

DWORD dwOldStyle = m_dwItemStyle;
	m_dwItemStyle &= ~dwRemove;
	m_dwItemStyle |= dwAdd;

bool bWasVisible =
		( dwOldStyle & __ETWI_INVISIBLE )
			? false
			: true;
bool bNowVisible =
		( m_dwItemStyle & __ETWI_INVISIBLE )
			? false
			: true;
	if( bWasVisible != bNowVisible )
	{
		ASSERT(
				m_pWndTab->m_nVisibleItemCount >= 0
			&&	m_pWndTab->m_nVisibleItemCount <= m_pWndTab->m_arrItems.GetSize()
			);

		if( bNowVisible )
			m_pWndTab->m_nVisibleItemCount ++;
		else
			m_pWndTab->m_nVisibleItemCount --;
		
		ASSERT(
				m_pWndTab->m_nVisibleItemCount >= 0
			&&	m_pWndTab->m_nVisibleItemCount <= m_pWndTab->m_arrItems.GetSize()
			);
	} // if( bWasVisible != bNowVisible )

	bool bNowSelected =
		( m_dwItemStyle & __ETWI_SELECTED )
			? true
			: false;
	if( bNowSelected )
		m_dwItemStyle |= __ETWI_IN_GROUP_ACTIVE;

	bool bWasInGroupActive =
		( dwOldStyle & __ETWI_IN_GROUP_ACTIVE )
			? true
			: false;
	bool bNowInGroupActive =
		( m_dwItemStyle & __ETWI_IN_GROUP_ACTIVE )
			? true
			: false;

	if( bWasInGroupActive != bNowInGroupActive
		&& (m_pPrev != NULL || m_pNext != NULL)
		&& bNowInGroupActive
		)
	{
		bool bInGroupActiveWasFoundBeforeThis = false;
		if( (m_dwItemStyle & __ETWI_GROUP_START) == 0 )
		{
			for(	TAB_ITEM_INFO * pTii = m_pPrev;
					pTii != NULL;
					pTii = pTii->m_pPrev
					)
			{ // find active in group item before this
				if( pTii->m_dwItemStyle & __ETWI_IN_GROUP_ACTIVE )
				{
					pTii->m_dwItemStyle &= ~__ETWI_IN_GROUP_ACTIVE;
					bInGroupActiveWasFoundBeforeThis = true;
					break;
				}
				if( pTii->m_dwItemStyle & __ETWI_GROUP_START )
					break;
			} // find active in group item before this
		}
		if( !bInGroupActiveWasFoundBeforeThis )
		{
			for(	TAB_ITEM_INFO * pTii = m_pNext;
					pTii != NULL;
					pTii = pTii->m_pNext
					)
			{ // find active in group item after this
				if( pTii->m_dwItemStyle & __ETWI_GROUP_START )
					break;
				if( pTii->m_dwItemStyle & __ETWI_IN_GROUP_ACTIVE )
				{
					pTii->m_dwItemStyle &= ~__ETWI_IN_GROUP_ACTIVE;
					break;
				}
			} // find active in group item after this
		} // if( !bInGroupActiveWasFoundBeforeThis )
	} // if( bWasInGroupActive != bNowInGroupActive )

	return dwOldStyle;
}

DWORD CExtTabWnd::TAB_ITEM_INFO::GetItemStyleEx() const
{
	ASSERT_VALID( this );
DWORD dwItemStyleEx = m_dwItemStyleEx;
	if( m_pWndTab != NULL )
	{
		ASSERT_VALID( m_pWndTab );
		m_pWndTab->_OnTabItemHook_GetItemStyleEx( *this, dwItemStyleEx );
	}
	return dwItemStyleEx;
}

DWORD CExtTabWnd::TAB_ITEM_INFO::ModifyItemStyleEx(
	DWORD dwRemove,
	DWORD dwAdd // = 0
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	if( m_pWndTab != NULL )
	{
		ASSERT_VALID( m_pWndTab );
		m_pWndTab->_OnTabItemHook_ModifyItemStyleEx( *this, m_dwItemStyleEx, dwRemove, dwAdd );
	}
DWORD dwOldStyleEx = m_dwItemStyleEx;
	m_dwItemStyleEx &= ~dwRemove;
	m_dwItemStyleEx |= dwAdd;
	return dwOldStyleEx;
}

const CSize & CExtTabWnd::TAB_ITEM_INFO::GetLastMeasuredItemSize() const
{
	return m_sizeLastMeasuredItem;
}

const CSize & CExtTabWnd::TAB_ITEM_INFO::GetLastMeasuredTextSize() const
{
	return m_sizeLastMeasuredText;
}

const CSize & CExtTabWnd::TAB_ITEM_INFO::GetLastMeasuredIconSize() const
{
	return m_sizeLastMeasuredIcon;
}

const CRect & CExtTabWnd::TAB_ITEM_INFO::ItemRectGet() const
{
	return m_rcItem;
}

void CExtTabWnd::TAB_ITEM_INFO::ItemRectSet( const RECT & rcItem )
{
	m_rcItem = rcItem;
}

const CRect & CExtTabWnd::TAB_ITEM_INFO::CloseButtonRectGet() const
{
	return m_rcCloseButton;
}

void CExtTabWnd::TAB_ITEM_INFO::CloseButtonRectSet( const RECT & rcCloseButton )
{
	m_rcCloseButton = rcCloseButton;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetPrev()
{
	ASSERT_VALID( this );
	return m_pPrev;
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetPrev() const
{
	ASSERT_VALID( this );
	return m_pPrev;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetNext()
{
	ASSERT_VALID( this );
	return m_pNext;
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetNext() const
{
	ASSERT_VALID( this );
	return m_pNext;
}

bool CExtTabWnd::TAB_ITEM_INFO::IsFirst() const
{
	return (GetPrev() == NULL) ? true : false;
}

bool CExtTabWnd::TAB_ITEM_INFO::IsLast() const
{
	return (GetNext() == NULL) ? true : false;
}

bool CExtTabWnd::TAB_ITEM_INFO::VisibleGet() const
{
	bool bVisible =
		( (GetItemStyle() & __ETWI_INVISIBLE) != 0 )
			? false : true;
	return bVisible;
}

bool CExtTabWnd::TAB_ITEM_INFO::VisibleSet( 
	bool bVisible // = true 
	)
{
	bool bWasVisible = VisibleGet();
	ModifyItemStyle(
		bVisible ? __ETWI_INVISIBLE : 0,
		bVisible ? 0 : __ETWI_INVISIBLE
		);
	return bWasVisible;
}

bool CExtTabWnd::TAB_ITEM_INFO::EnabledGet() const
{
	bool bEnable =
		( (GetItemStyle() & __ETWI_DISABLED) != 0 )
			? false : true;
	return bEnable;
}

bool CExtTabWnd::TAB_ITEM_INFO::EnabledSet( 
	bool bEnable // = true 
	)
{
	bool bEnablePrev = EnabledGet();
	ModifyItemStyle(
		bEnable ? __ETWI_DISABLED : 0,
		bEnable ? 0 : __ETWI_DISABLED
		);
	return bEnablePrev;
}

bool CExtTabWnd::TAB_ITEM_INFO::SelectedGet() const
{
	bool bSelected =
		( (GetItemStyle() & __ETWI_SELECTED) != 0 )
			? true : false;
	return bSelected;
}

bool CExtTabWnd::TAB_ITEM_INFO::SelectedSet( 
	bool bSelected // = true 
	)
{
	bool bWasSelected = SelectedGet();
	ModifyItemStyle(
		bSelected ? 0 : __ETWI_SELECTED,
		bSelected ? __ETWI_SELECTED : 0
		);
	return bWasSelected;
}

bool CExtTabWnd::TAB_ITEM_INFO::GroupStartGet() const
{
	bool bGroupStart =
		( (GetItemStyle() & __ETWI_GROUP_START) != 0 )
			? true : false;
	return bGroupStart;
}

bool CExtTabWnd::TAB_ITEM_INFO::GroupStartSet( 
	bool bGroupStart // = true 
	)
{
	bool bWasGroupStart = GroupStartGet();
	ModifyItemStyle(
		bGroupStart ? 0 : __ETWI_GROUP_START,
		bGroupStart ? __ETWI_GROUP_START : 0
		);
	return bWasGroupStart;
}

bool CExtTabWnd::TAB_ITEM_INFO::InGroupActiveGet() const
{
	bool bInGroupActive =
		( (GetItemStyle() & __ETWI_IN_GROUP_ACTIVE) != 0 )
			? true : false;
	return bInGroupActive;
}

bool CExtTabWnd::TAB_ITEM_INFO::InGroupActiveSet( 
	bool bInGroupActive // = true 
	)
{
	bool bWasInGroupActive = InGroupActiveGet();
	ModifyItemStyle(
		bInGroupActive ? 0 : __ETWI_IN_GROUP_ACTIVE,
		bInGroupActive ? __ETWI_IN_GROUP_ACTIVE : 0
		);
	return bWasInGroupActive;
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetInGroupFirst() const
{
	ASSERT_VALID( this );
	const TAB_ITEM_INFO * pTii = this;
	for( ; pTii != NULL; pTii = pTii->GetPrev() )
	{
		ASSERT_VALID( pTii );
		if(		pTii->GroupStartGet()
			||	pTii->GetPrev() == NULL
			)
			return pTii;
	}
	ASSERT( FALSE );
	return NULL;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetInGroupFirst()
{
	const TAB_ITEM_INFO * pTii = this;
	pTii = pTii->GetInGroupFirst();
	return
		const_cast < TAB_ITEM_INFO * > ( pTii );
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetInGroupLast() const
{
	ASSERT_VALID( this );
	const TAB_ITEM_INFO * pTii = GetNext();
	const TAB_ITEM_INFO * pTii2 = this;
	for( ; pTii != NULL; )
	{
		ASSERT_VALID( pTii );
		if( pTii->GroupStartGet() )
			return pTii2;
		pTii2 = pTii;
		pTii = pTii->GetNext();
	}
	return pTii2;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::TAB_ITEM_INFO::GetInGroupLast()
{
	const TAB_ITEM_INFO * pTii = this;
	pTii = pTii->GetInGroupLast();
	return
		const_cast < TAB_ITEM_INFO * > ( pTii );
}

LONG CExtTabWnd::TAB_ITEM_INFO::GetIndexOf() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return m_pWndTab->ItemGetIndexOf( this );
}

LPARAM CExtTabWnd::TAB_ITEM_INFO::LParamGet() const
{
	return m_lParam;
}

LPARAM CExtTabWnd::TAB_ITEM_INFO::LParamSet( 
	LPARAM lParam // = 0 
	)
{
	LPARAM lParamOld = m_lParam;
	m_lParam = lParam;
	return lParamOld;
}

bool CExtTabWnd::TAB_ITEM_INFO::IsToolTipAvailByExtent() const
{
	return m_bHelperToolTipAvail;
}

bool CExtTabWnd::TAB_ITEM_INFO::MultiRowWrapGet() const
{
	return m_bMultiRowWrap;
}

void CExtTabWnd::TAB_ITEM_INFO::MultiRowWrapSet(
	bool bMultiRowWrap // = true
	)
{
	m_bMultiRowWrap = bMultiRowWrap;
}

#ifdef _DEBUG

void CExtTabWnd::TAB_ITEM_INFO::AssertValid() const
{
	CObject::AssertValid();

	ASSERT( m_pWndTab != NULL );

	if( m_pPrev != NULL )
	{
		ASSERT( m_pPrev->m_pNext == this );
		ASSERT( m_pPrev->m_pWndTab == m_pWndTab );
	}
	
	if( m_pNext != NULL )
	{
		ASSERT( m_pNext->m_pPrev == this );
		ASSERT( m_pNext->m_pWndTab == m_pWndTab );
	}

}

void CExtTabWnd::TAB_ITEM_INFO::Dump(CDumpContext& dc) const
{
	CObject::Dump( dc );
}

#endif

/////////////////////////////////////////////////////////////////////////////
// CExtTabWnd window

bool CExtTabWnd::g_bEnableOnIdleCalls = false;

IMPLEMENT_DYNCREATE( CExtTabWnd, CWnd );
IMPLEMENT_CExtPmBridge_MEMBERS_GENERIC( CExtTabWnd );

CExtTabWnd::CExtTabWnd()
	: m_bDirectCreateCall( false )
	, m_bDelayRecalcLayout( true )
	, m_bReflectQueryRepositionCalcEffect( false )
	, m_bReflectParentSizing( true )
	, m_bTrackingButtonPushed( false )
	, m_bTrackingButtonHover( false )
	, m_dwTabWndStyle( 0 )
	, m_dwTabWndStyleEx( 0 )
	, m_nSelIndex( -1 )
	, m_nDelayedSelIndex( -1 )
	, m_nIndexVisFirst( 0 )
	, m_nIndexVisLast( 0 )
	, m_nVisibleItemCount( 0 )
	, m_rcTabItemsArea( 0, 0, 0, 0 )
	, m_rcTabNearBorderArea( 0, 0, 0, 0 )
	, m_rcBtnUp( 0, 0, 0, 0 )
	, m_rcBtnDown( 0, 0, 0, 0 )
	, m_rcBtnScrollHome( 0, 0, 0, 0 )
	, m_rcBtnScrollEnd( 0, 0, 0, 0 )
	, m_rcBtnClose( 0, 0, 0, 0 )
	, m_rcBtnHelp( 0, 0, 0, 0 )
	, m_rcBtnTabList( 0, 0, 0, 0 )
	, m_nPushedTrackingButton( (-1L) )
	, m_nPushedTrackingHitTest( __ETWH_NOWHERE )
	, m_bPushedTrackingCloseButton( false )
	, m_nHoverTrackingHitTest( __ETWH_NOWHERE )
	, m_nItemsExtent( 0 )
	, m_nScrollPos( 0 )
	, m_nScrollMaxPos( 0 )
	, m_nScrollDirectionRest( 0 )
	, m_rcRecalcLayout( 0, 0, 0, 0 )
	, m_ptStartDrag( -1, -1 )
	, m_bEnableTrackToolTips( false )
	, m_bPushedUp( false )
	, m_bPushedDown( false )
	, m_bPushedScrollHome( false )
	, m_bPushedScrollEnd( false )
	, m_bPushedHelp( false )
	, m_bPushedClose( false )
	, m_bPushedTabList( false )
	, m_pHelperLastPaintManagerRTC( NULL )
	, m_rcLastMovedItemRect( 0, 0, 0, 0 )
	, m_bDragging( false )
	, m_nHoverTrackedIndex( -1L )
{
	VERIFY( RegisterTabWndClass() );

	PmBridge_Install();
}

CExtTabWnd::~CExtTabWnd()
{
	PmBridge_Uninstall();

	_RemoveAllItemsImpl();
}

bool CExtTabWnd::_IsMdiTabCtrl() const
{
	return false;
}

bool CExtTabWnd::_IsCustomLayoutTabWnd() const
{
	return false;
}

BEGIN_MESSAGE_MAP(CExtTabWnd, CWnd)
	//{{AFX_MSG_MAP(CExtTabWnd)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_SIZE()
	ON_WM_WINDOWPOSCHANGED()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDBLCLK()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEACTIVATE()
	ON_WM_SETFOCUS()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MBUTTONDBLCLK()
	ON_WM_CAPTURECHANGED()
	ON_WM_CANCELMODE()
	ON_WM_TIMER()
	ON_WM_SHOWWINDOW()
	ON_WM_SETCURSOR()
	//}}AFX_MSG_MAP
	ON_MESSAGE( WM_SIZEPARENT, OnSizeParent )
	ON_WM_SYSCOLORCHANGE()
	__EXT_MFC_SAFE_ON_WM_SETTINGCHANGE()
	ON_MESSAGE( WM_DISPLAYCHANGE, OnDisplayChange )
	ON_MESSAGE( __ExtMfc_WM_THEMECHANGED, OnThemeChanged )
	ON_REGISTERED_MESSAGE(
		CExtControlBar::g_nMsgQueryRepositionCalcEffect,
		_OnQueryRepositionCalcEffect
		)
END_MESSAGE_MAP()


#ifdef _DEBUG

void CExtTabWnd::AssertValid() const
{
	CWnd::AssertValid();
__EXT_MFC_INT_PTR nItemCount = m_arrItems.GetSize();
	ASSERT(
			m_nVisibleItemCount >= 0
		&&	m_nVisibleItemCount <= nItemCount
		);
}

void CExtTabWnd::Dump(CDumpContext& dc) const
{
	CWnd::Dump( dc );
}

#endif

LONG CExtTabWnd::GetHoverTrackingItem() const
{
	ASSERT_VALID( this );
	return m_nHoverTrackingHitTest;
}

LONG CExtTabWnd::GetPushedTrackingItem() const
{
	ASSERT_VALID( this );
	return m_nPushedTrackingButton;
}

CFont & CExtTabWnd::_GetTabWndFont(
	bool bSelected,
	DWORD dwOrientation // = DWORD(-1) // default orientation
	) const
{
	ASSERT_VALID( this );
	if( dwOrientation == DWORD(-1) )
		dwOrientation = OrientationGet();
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
bool bBold =
		( bSelected && SelectionBoldGet() )
			? true
			: false
			;
	if( dwOrientation == __ETWS_ORIENT_TOP
		|| dwOrientation == __ETWS_ORIENT_BOTTOM
		)
		return
			bBold
				? pPM->m_FontBold
				: pPM->m_FontNormal
				;
	if( GetTabWndStyle() & __ETWS_INVERT_VERT_FONT )
		return
			bBold
				? pPM->m_FontBoldVertX
				: pPM->m_FontNormalVertX
				;
	return
			bBold
				? pPM->m_FontBoldVert
				: pPM->m_FontNormalVert
				;
}

void CExtTabWnd::_RemoveAllItemsImpl()
{
	ASSERT_VALID( this );

LONG nCount = (LONG)m_arrItems.GetSize();
	
	if( nCount > 0 )
	{
		OnTabWndRemoveAllItems( true );

		for( LONG nItem = 0; nItem < nCount; nItem ++ )
		{
			TAB_ITEM_INFO * pTII = m_arrItems[ nItem ];
			delete pTII;
		}
		m_arrItems.RemoveAll();
		m_arrItems.FreeExtra();

		m_nVisibleItemCount = 0;
		m_nSelIndex = -1;
		m_bDelayRecalcLayout = true;

		ASSERT_VALID( this );

		OnTabWndRemoveAllItems( false );
	} // if( nCount > 0 )
}

void CExtTabWnd::_OnTabItemHook_GetItemStyle(
	const CExtTabWnd::TAB_ITEM_INFO & _TII,
	DWORD & dwItemStyle
	) const
{
	ASSERT_VALID( this );
	_TII;
	dwItemStyle;
}

void CExtTabWnd::_OnTabItemHook_ModifyItemStyle(
	CExtTabWnd::TAB_ITEM_INFO & _TII,
	DWORD dwItemStyle,
	DWORD & dwRemove,
	DWORD & dwAdd
	)
{
	ASSERT_VALID( this );
	_TII;
	dwItemStyle;
	dwRemove;
	dwAdd;
}

void CExtTabWnd::_OnTabItemHook_GetItemStyleEx(
	const CExtTabWnd::TAB_ITEM_INFO & _TII,
	DWORD & dwItemStyleEx
	) const
{
	ASSERT_VALID( this );
	_TII;
	dwItemStyleEx;
}

void CExtTabWnd::_OnTabItemHook_ModifyItemStyleEx(
	CExtTabWnd::TAB_ITEM_INFO & _TII,
	DWORD dwItemStyleEx,
	DWORD & dwRemove,
	DWORD & dwAdd
	)
{
	ASSERT_VALID( this );
	_TII;
	dwItemStyleEx;
	dwRemove;
	dwAdd;
}

DWORD CExtTabWnd::GetTabWndStyle() const
{
	ASSERT_VALID( this );
	return m_dwTabWndStyle;
}

DWORD CExtTabWnd::ModifyTabWndStyle(
	DWORD dwRemove,
	DWORD dwAdd, // = 0,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
DWORD dwOldStyle = m_dwTabWndStyle;
	m_dwTabWndStyle &= ~dwRemove;
	m_dwTabWndStyle |= dwAdd;
	if( m_dwTabWndStyle & __ETWS_EQUAL_WIDTHS )
		m_nScrollPos = 0;
	UpdateTabWnd( bUpdateTabWnd );
	return dwOldStyle;
}

DWORD CExtTabWnd::GetTabWndStyleEx() const
{
	ASSERT_VALID( this );
	return m_dwTabWndStyleEx;
}

DWORD CExtTabWnd::ModifyTabWndStyleEx(
	DWORD dwRemove,
	DWORD dwAdd, // = 0,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
DWORD dwOldStyleEx = m_dwTabWndStyleEx;
	m_dwTabWndStyleEx &= ~dwRemove;
	m_dwTabWndStyleEx |= dwAdd;
	UpdateTabWnd( bUpdateTabWnd );
	return dwOldStyleEx;
}

DWORD CExtTabWnd::OrientationGet() const
{
	return GetTabWndStyle() & __ETWS_ORIENT_MASK;
}

DWORD CExtTabWnd::OrientationSet(
	DWORD dwOrientation,
	bool bUpdateTabWnd // = false
	)
{
	DWORD dwOldOrientation =
		GetTabWndStyle() & __ETWS_ORIENT_MASK;
	ModifyTabWndStyle(
		__ETWS_ORIENT_MASK,
		dwOrientation & __ETWS_ORIENT_MASK
		);
	UpdateTabWnd( bUpdateTabWnd );
	return dwOldOrientation;
}

bool CExtTabWnd::OrientationIsHorizontal() const
{
	DWORD dwOrientation = OrientationGet();
	if( dwOrientation == __ETWS_ORIENT_TOP
		|| dwOrientation == __ETWS_ORIENT_BOTTOM
		)
		return true;
	return false;
}

bool CExtTabWnd::OrientationIsVertical() const
{
	return !OrientationIsHorizontal();
}

bool CExtTabWnd::OrientationIsTopLeft() const
{
	DWORD dwOrientation = OrientationGet();
	if( dwOrientation == __ETWS_ORIENT_TOP
		|| dwOrientation == __ETWS_ORIENT_LEFT
		)
		return true;
	return false;
}

HWND CExtTabWnd::PmBridge_GetSafeHwnd() const
{
__PROF_UIS_MANAGE_STATE;
HWND hWnd = GetSafeHwnd();
	return hWnd;
}

void CExtTabWnd::PmBridge_OnPaintManagerChanged(
	CExtPaintManager * pGlobalPM
	)
{
LONG nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		TAB_ITEM_INFO * pTII = ItemGet( nIndex );
		if( pTII != NULL )
		{
			CExtCmdIcon * pIcon = pTII->IconGetPtr();
			if( pIcon != NULL )
				pIcon->OnEmptyGeneratedBitmaps();
		}
	}
	CExtPmBridge::PmBridge_OnPaintManagerChanged( pGlobalPM );
}

LONG CExtTabWnd::ItemGetCount() const
{
	ASSERT_VALID( this );
	return (LONG)m_arrItems.GetSize();
}

LONG CExtTabWnd::ItemGetVisibleCount() const
{
	ASSERT_VALID( this );
	return m_nVisibleItemCount;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::ItemGet( LONG nIndex )
{
	ASSERT_VALID( this );
	LONG nCount = ItemGetCount();
	if( nIndex < 0 || nIndex >= nCount )
	{
		ASSERT( FALSE );
		return NULL;
	}
	TAB_ITEM_INFO * pTii = m_arrItems[ nIndex ];
	ASSERT_VALID( pTii );
	ASSERT( pTii->m_pWndTab == this );
	return pTii;
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::ItemGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	return
		(const_cast < CExtTabWnd * > ( this ))->ItemGet( nIndex );
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::ItemTextGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	return ItemGet(nIndex)->TextGet();
}

void CExtTabWnd::ItemTextSet(
	LONG nIndex,
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	ItemGet(nIndex)->TextSet( sText );
	UpdateTabWnd( bUpdateTabWnd );
}

bool CExtTabWnd::ItemEnabledGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	return ItemGet( nIndex )->EnabledGet();
}

void CExtTabWnd::ItemEnabledSet(
	LONG nIndex,
	bool bEnabled, // = true
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	ItemGet( nIndex )->EnabledSet( bEnabled );
	UpdateTabWnd( bUpdateTabWnd );
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::ItemTooltipTextGet( LONG nIndex ) const
{
	return ItemGet(nIndex)->TooltipTextGet();
}

void CExtTabWnd::ItemTooltipTextSet(
	LONG nIndex,
	__EXT_MFC_SAFE_LPCTSTR sTooltipText, // = NULL
	bool bUpdateTabWnd // = false
	)
{
	ItemGet(nIndex)->TooltipTextSet( sTooltipText );
	UpdateTabWnd( bUpdateTabWnd );
}

LPARAM CExtTabWnd::ItemLParamGet( LONG nIndex ) const
{
	return ItemGet(nIndex)->LParamGet();
}

void CExtTabWnd::ItemLParamSet(
	LONG nIndex,
	LPARAM lParam // = 0
	)
{
	ItemGet(nIndex)->LParamSet( lParam );
}

CExtCmdIcon & CExtTabWnd::ItemIconGet( LONG nIndex )
{
CExtCmdIcon * pIcon =
		ItemGet(nIndex)->IconGetPtr();
	if( pIcon == NULL )
	{
		static CExtCmdIcon g_EmptyIcon;
		return g_EmptyIcon;
	}
	return *pIcon;
}

const CExtCmdIcon & CExtTabWnd::ItemIconGet( LONG nIndex ) const
{
	return
		( const_cast < CExtTabWnd * > ( this ) )
		-> ItemIconGet( nIndex );
}

void CExtTabWnd::ItemIconSet( // ADDED 2.53
	LONG nIndex,
	const CExtCmdIcon & _icon,
	bool bUpdateTabWnd // = false
	)
{
	ItemGet(nIndex)->IconSet( _icon );
	UpdateTabWnd( bUpdateTabWnd );
}

void CExtTabWnd::ItemIconSet(
	LONG nIndex,
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true,
	bool bUpdateTabWnd // = false
	)
{
	ItemGet(nIndex)->IconSet( hIcon, bCopyIcon );
	UpdateTabWnd( bUpdateTabWnd );
}

DWORD CExtTabWnd::ItemStyleGet( LONG nIndex )
{
	return ItemGet(nIndex)->GetItemStyle();
}

DWORD CExtTabWnd::ItemStyleModify(
	LONG nIndex,
	DWORD dwRemove,
	DWORD dwAdd, // = 0
	bool bUpdateTabWnd // = false
	)
{
	DWORD dwOldItemStyle =
		ItemGet(nIndex)->ModifyItemStyle(dwRemove,dwAdd);
	UpdateTabWnd( bUpdateTabWnd );
	return dwOldItemStyle;
}

LONG CExtTabWnd::ItemGetIndexOf( const CExtTabWnd::TAB_ITEM_INFO * pTii ) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	LONG nCount = ItemGetCount();
	for( LONG nIndex = 0; nIndex < nCount; nIndex++ )
	{
		const TAB_ITEM_INFO * pTii2 = ItemGet( nIndex );
		if( pTii2 == pTii )
			return nIndex;
	} // for( LONG nIndex = 0; nIndex < nCount; nIndex++ )
	ASSERT( FALSE );
	return -1;
}

LONG CExtTabWnd::SelectionGet() const
{
	ASSERT_VALID( this );
	return m_nSelIndex;
}

const CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::SelectionGetPtr() const
{
	ASSERT_VALID( this );
	if( m_nSelIndex < 0 || ItemGetCount() == 0 )
		return NULL;
	return ItemGet( m_nSelIndex );
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::SelectionGetPtr()
{
	ASSERT_VALID( this );
	LONG nCount = ItemGetCount();
	if( m_nSelIndex < 0 || nCount == 0 )
		return NULL;
	return ItemGet( m_nSelIndex );
}

bool CExtTabWnd::SelectionBoldGet() const
{
	bool bSelectionBold =
		(GetTabWndStyle() & __ETWS_BOLD_SELECTION) ? true : false;
	if( bSelectionBold
		&& (GetTabWndStyle() & __ETWS_GROUPED)
		)
		bSelectionBold = false;
	return bSelectionBold;
}

void CExtTabWnd::SelectionBoldSet(
	bool bBold, // = true
	bool bUpdateTabWnd // = false
	)
{
	ModifyTabWndStyle(
		bBold ? 0 : __ETWS_BOLD_SELECTION,
		bBold ? __ETWS_BOLD_SELECTION : 0,
		bUpdateTabWnd
		);
}

void CExtTabWnd::ItemGetGroupRange(
	LONG nIndex,
	LONG & nGroupStart,
	LONG & nGroupEnd,
	TAB_ITEM_INFO ** ppTiiStart, // = NULL
	TAB_ITEM_INFO ** ppTiiEnd // = NULL
	)
{
	ASSERT_VALID( this );
	TAB_ITEM_INFO * pTii = ItemGet( nIndex );
	ASSERT_VALID( pTii );
	TAB_ITEM_INFO * pTiiStart = pTii->GetInGroupFirst();
	ASSERT_VALID( pTiiStart );
	TAB_ITEM_INFO * pTiiEnd = pTii->GetInGroupLast();
	ASSERT_VALID( pTiiEnd );
	nGroupStart = ItemGetIndexOf( pTiiStart );
	nGroupEnd = ItemGetIndexOf( pTiiEnd );
	if( ppTiiStart != NULL )
		*ppTiiStart = pTiiStart;
	if( ppTiiEnd != NULL )
		*ppTiiEnd = pTiiEnd;
}

void CExtTabWnd::ItemGetGroupRange(
	LONG nIndex,
	LONG & nGroupStart,
	LONG & nGroupEnd,
	const TAB_ITEM_INFO ** ppTiiStart, // = NULL
	const TAB_ITEM_INFO ** ppTiiEnd // = NULL
	) const
{
	ASSERT_VALID( this );
	const TAB_ITEM_INFO * pTii = ItemGet( nIndex );
	ASSERT_VALID( pTii );
	const TAB_ITEM_INFO * pTiiStart = pTii->GetInGroupFirst();
	ASSERT_VALID( pTiiStart );
	const TAB_ITEM_INFO * pTiiEnd = pTii->GetInGroupFirst();
	ASSERT_VALID( pTiiEnd );
	nGroupStart = ItemGetIndexOf( pTiiStart );
	nGroupEnd = ItemGetIndexOf( pTiiEnd );
	if( ppTiiStart != NULL )
		*ppTiiStart = pTiiStart;
	if( ppTiiEnd != NULL )
		*ppTiiEnd = pTiiEnd;
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::ItemInsert(
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true
	DWORD dwItemStyle, // = 0 // visible & non-group
	LONG nIndex, // = -1 // default - append
	LPARAM lParam, // = 0
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
CExtCmdIcon _icon;
	if( hIcon != NULL )
		_icon.AssignFromHICON( hIcon, bCopyIcon );
	return
		ItemInsert(
			sText,
			_icon,
			dwItemStyle,
			nIndex,
			lParam,
			bUpdateTabWnd
			);
}

CExtTabWnd::TAB_ITEM_INFO * CExtTabWnd::ItemInsert(
	__EXT_MFC_SAFE_LPCTSTR sText,
	const CExtCmdIcon & _icon,
	DWORD dwItemStyle, // = 0 // visible & non-group
	LONG nIndex, // = -1 // default - append
	LPARAM lParam, // = 0
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if( nIndex < 0 || nIndex > nCount )
		nIndex = nCount;

TAB_ITEM_INFO * pTiiPrev = NULL;
	if( nIndex > 0 )
	{
		pTiiPrev = ItemGet( nIndex - 1 );
		ASSERT_VALID( pTiiPrev );
	}

TAB_ITEM_INFO * pTiiNext = NULL;
	if( nIndex < nCount )
	{
		pTiiNext = ItemGet( nIndex );
		ASSERT_VALID( pTiiNext );
	}

TAB_ITEM_INFO * pTii =
		new TAB_ITEM_INFO(
			this,
			pTiiPrev,
			pTiiNext,
			sText,
			&_icon,
			dwItemStyle,
			lParam
			);
	m_arrItems.InsertAt( nIndex, pTii );
	ASSERT_VALID( pTii );
	if( m_nSelIndex >= nIndex ) 
		m_nSelIndex++;

	if( pTii->VisibleGet() )
		m_nVisibleItemCount ++;

	ASSERT_VALID( this );

	OnTabWndItemInsert(
		nIndex,
		pTii
		);

	UpdateTabWnd( bUpdateTabWnd );
	return pTii;
}

LONG CExtTabWnd::ItemRemove( // returns count of removed items
	LONG nIndex,
	LONG nCountToRemove, // = 1
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );

	if( nCountToRemove > 0 )
	{
		LONG nCount = ItemGetCount();
		if( nCount > 0 )
		{
			ASSERT( nIndex >= 0 && nIndex < nCount );
			LONG nAvailToRemove = nCount - nIndex;
			if( nCountToRemove > nAvailToRemove )
				nCountToRemove = nAvailToRemove;

			TAB_ITEM_INFO * pTiiPrev = NULL;
			TAB_ITEM_INFO * pTiiNext = NULL;

			if( m_nSelIndex >= (nIndex+nCountToRemove) )
				m_nSelIndex -= nCountToRemove;

			for( LONG n = 0; n < nCountToRemove; n++ )
			{
				TAB_ITEM_INFO * pTii = ItemGet( nIndex );
				if( n == 0 )
					pTiiPrev = pTii->GetPrev();
				if( n == ( nCountToRemove - 1  ) )
					pTiiNext = pTii->GetNext();
				if( pTii->VisibleGet() )
					m_nVisibleItemCount --;
				if( pTii->SelectedGet() )
					m_nSelIndex = -1;
				delete pTii;
			}

			
			OnTabWndRemoveItem( nIndex, nCountToRemove, true );
			
			m_arrItems.RemoveAt( nIndex, nCountToRemove );

			if( pTiiPrev != NULL )
				pTiiPrev->m_pNext = pTiiNext;
			if( pTiiNext != NULL )
				pTiiNext->m_pPrev = pTiiPrev;
			
			OnTabWndRemoveItem( nIndex, nCountToRemove, false );
			
		} // if( nCount > 0 )
		else
			nCountToRemove = 0;
	}

	ASSERT_VALID( this );

	UpdateTabWnd( bUpdateTabWnd );
	return nCountToRemove;
}

void CExtTabWnd::ItemRemoveGroup(
	LONG nIndex,
	LONG * p_nGroupStart, // = NULL
	LONG * p_nCountInGroup, // = NULL
	bool bUpdateTabWnd // = false
	)
{
	LONG nGroupStart;
	LONG nGroupEnd;
	ItemGetGroupRange(
		nIndex,
		nGroupStart,
		nGroupEnd
		);
	ASSERT( nGroupEnd >= nGroupStart );
	LONG nCountToRemove = nGroupEnd - nGroupStart + 1;
	ItemRemove(
		nGroupStart,
		nCountToRemove,
		bUpdateTabWnd
		);
	if( p_nGroupStart != NULL )
		*p_nGroupStart = nGroupStart;
	if( p_nCountInGroup != NULL )
		*p_nCountInGroup = nCountToRemove;
}

void CExtTabWnd::ItemRemoveAll(
	bool bUpdateTabWnd // = false
	)
{
	_RemoveAllItemsImpl();
	UpdateTabWnd( bUpdateTabWnd );
}

LONG CExtTabWnd::ItemHitTest(
	const POINT & ptClient
	) const
{
	ASSERT_VALID( this );
	if(	GetSafeHwnd() == NULL
		||	( ! ::IsWindow( GetSafeHwnd() ) )
		)
		return __ETWH_NOWHERE;
CRect rcClient;
	GetClientRect( &rcClient );
	if( rcClient.IsRectEmpty()
		|| (! rcClient.PtInRect(ptClient) )
		)
		return __ETWH_NOWHERE;
	
	if(		(! m_rcTabNearBorderArea.IsRectEmpty() )
		&&	m_rcTabNearBorderArea.PtInRect(ptClient)
		)
		return __ETWH_BORDER_AREA;

	if(		(! m_rcBtnUp.IsRectEmpty() )
		&&	m_rcBtnUp.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_LEFTUP;
	if(		(! m_rcBtnDown.IsRectEmpty() )
		&&	m_rcBtnDown.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_RIGHTDOWN;
	if(		(! m_rcBtnScrollHome.IsRectEmpty() )
		&&	m_rcBtnScrollHome.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_SCROLL_HOME;
	if(		(! m_rcBtnScrollEnd.IsRectEmpty() )
		&&	m_rcBtnScrollEnd.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_SCROLL_END;
	if(		(! m_rcBtnHelp.IsRectEmpty() )
		&&	m_rcBtnHelp.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_HELP;
	if(		(! m_rcBtnClose.IsRectEmpty() )
		&&	m_rcBtnClose.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_CLOSE;
	if(		(! m_rcBtnTabList.IsRectEmpty() )
		&&	m_rcBtnTabList.PtInRect(ptClient)
		)
		return __ETWH_BUTTON_TAB_LIST;
	if(		m_rcTabItemsArea.IsRectEmpty()
		||	(! m_rcTabItemsArea.PtInRect(ptClient) )
		)
		return __ETWH_NOWHERE;

	if( ItemGetVisibleCount() == 0 )
		return __ETWH_ITEMS_AREA;

bool bHorz = OrientationIsHorizontal();

//CPoint ptClientTest(
//		ptClient.x + ( bHorz ? m_nScrollPos : 0 ),
//		ptClient.y + ( bHorz ? 0 : m_nScrollPos )
//		);
CPoint ptClientTest(
		ptClient.x +
			( bHorz
				? (m_nScrollPos - m_rcTabItemsArea.left)
				: 0
			),
		ptClient.y +
			( bHorz
				? 0
				: (m_nScrollPos - m_rcTabItemsArea.top)
			)
		);

LONG nItemCount = ItemGetCount();
LONG nVisCount = ItemGetVisibleCount();
	if(		nItemCount > 0
		&&	nVisCount > 0
		&&	m_nIndexVisFirst >= 0
		&&	m_nIndexVisLast >= 0
		&&	m_nIndexVisFirst < nItemCount
		&&	m_nIndexVisLast < nItemCount
		)
	{
		ASSERT( m_nIndexVisFirst <= m_nIndexVisLast );
		ASSERT( 0 <= m_nIndexVisFirst && m_nIndexVisFirst < nItemCount );
		ASSERT( 0 <= m_nIndexVisLast && m_nIndexVisLast < nItemCount );
		for( LONG nIndex = m_nIndexVisFirst; nIndex <= m_nIndexVisLast; nIndex++ )
		{
			const TAB_ITEM_INFO * pTii = ItemGet( nIndex );
			ASSERT_VALID( pTii );
			if( !pTii->VisibleGet() )
				continue;
			if( pTii->HitTest( ptClientTest ) )
				//return nIndex;
				return pTii->GetIndexOf();
			
		} // for( LONG nIndex = m_nIndexVisFirst; nIndex <= m_nIndexVisLast; nIndex++ )
	} // if( nItemCount > 0 && nVisCount > 0 )

	return __ETWH_ITEMS_AREA;
}

bool CExtTabWnd::ItemEnsureVisible(
	INT nItemIndex,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	ASSERT( nItemIndex >= 0 && nItemIndex < ItemGetCount() );
bool bRetVal = false;
	if( _IsScrollAvail() )
	{
		TAB_ITEM_INFO * pTii = ItemGet( nItemIndex );
		ASSERT_VALID( pTii );
		if( pTii->VisibleGet() )
		{
			bool bHorz = OrientationIsHorizontal();
			
			CRect rcItem = pTii->ItemRectGet();
			rcItem.OffsetRect(
				bHorz ? (-m_nScrollPos) : 0,
				bHorz ? 0 : (-m_nScrollPos)
				);
			
			INT nItemExtentMinVal =
				bHorz ? rcItem.left : rcItem.top;
			
			INT nItemExtentMaxVal =
				bHorz ? rcItem.right : rcItem.bottom;
			
			INT nAreaExtentMinVal = 0;
			INT nAreaExtentMaxVal =
				bHorz
					? (m_rcTabItemsArea.right - m_rcTabItemsArea.left)
					: (m_rcTabItemsArea.bottom - m_rcTabItemsArea.top)
					;
				//bHorz ? m_rcTabItemsArea.right : m_rcTabItemsArea.bottom;

			//nAreaExtentMaxVal += __EXTTAB_ADD_END_SCROLL_SPACE + 2;
			nAreaExtentMaxVal += __EXTTAB_ADD_END_SCROLL_SPACE;

			if( nItemExtentMaxVal > nAreaExtentMaxVal  )
			{
				INT nShift = nItemExtentMaxVal - nAreaExtentMaxVal;
				ASSERT( nShift > 0 );
				m_nScrollPos += nShift;
				
				INT nScrollRest = m_nScrollMaxPos - m_nScrollPos;
				nScrollRest = min( nScrollRest, __EXTTAB_ADD_END_SCROLL_SPACE );
				m_nScrollPos += nScrollRest;
				
				ASSERT( m_nScrollPos >= 0 && m_nScrollPos <= m_nScrollMaxPos );
			}
			if( nItemExtentMinVal < nAreaExtentMinVal  )
			{
				INT nShift = nAreaExtentMinVal - nItemExtentMinVal;
				ASSERT( nShift > 0 );
				m_nScrollPos -= nShift;

				INT nScrollRest = min( m_nScrollPos, __EXTTAB_ADD_END_SCROLL_SPACE );
				m_nScrollPos -= nScrollRest;
				
				ASSERT( m_nScrollPos >= 0 && m_nScrollPos <= m_nScrollMaxPos );
			}
			
			if( m_nScrollPos > m_nScrollMaxPos )
				m_nScrollPos = m_nScrollMaxPos;
			
			bRetVal = true;
		} // if( pTii->VisibleGet() )
	} // if( _IsScrollAvail() )
	else
		bRetVal = true;
	UpdateTabWnd( bUpdateTabWnd );
	return bRetVal;
}

bool CExtTabWnd::g_bTabWndClassRegistered = false;

bool CExtTabWnd::RegisterTabWndClass()
{
	if( g_bTabWndClassRegistered )
		return true;

WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo(
			hInst,
			__EXT_TAB_WND_CLASS_NAME,
			&_wndClassInfo
			)
		)
	{
		_wndClassInfo.style = CS_GLOBALCLASS|CS_DBLCLKS;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor =
			( g_hCursor != NULL )
				? g_hCursor
				: ::LoadCursor(
					NULL, //hInst,
					IDC_ARROW
					)
				;
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_TAB_WND_CLASS_NAME;
		if( ! ::AfxRegisterClass( &_wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}

	g_bTabWndClassRegistered = true;
	return true;
}

CRect CExtTabWnd::CalcPreviewLayout( const CRect & rcAvail )
{
	ASSERT_VALID( this );
	m_rcRecalcLayout = rcAvail;
	_RecalcLayoutImpl();
	m_rcRecalcLayout.SetRect( 0, 0, 0, 0 );
	
	CRect rcResult;
	if( m_rcTabItemsArea.IsRectEmpty() )
		GetWindowRect( &rcResult );
	else
		rcResult = m_rcTabItemsArea;

	m_bDelayRecalcLayout = true;
	return rcResult;
}

BOOL CExtTabWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( IDC_STATIC )
	DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS
	DWORD dwTabStyle, // = __ETWS_DEFAULT
	CCreateContext * pContext // = NULL
	)
{
	if( ! RegisterTabWndClass() )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	m_bDirectCreateCall = true;
	m_dwTabWndStyle = dwTabStyle;
	if( ! CWnd::Create(
			__EXT_TAB_WND_CLASS_NAME,
			NULL,
			dwWindowStyle,
			rcWnd,
			pParentWnd,
			nDlgCtrlID,
			pContext
			)
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		::AfxThrowMemoryException();
	}
	return TRUE;
}

bool CExtTabWnd::_CreateHelper()
{
	EnableToolTips( TRUE );

	if( OnTabWndToolTipQueryEnabled() )
	{ // tooltips enabled at all?
		if( ! m_wndToolTip.Create( this ) )
		{
			ASSERT( FALSE );
			return false;
		}
		m_wndToolTip.Activate( TRUE );
	}

	UpdateTabWnd( false );
	return true;
}

void CExtTabWnd::PreSubclassWindow() 
{
	CWnd::PreSubclassWindow();

	if( m_bDirectCreateCall )
		return;

DWORD dwStyle = ::GetWindowLong( m_hWnd, GWL_STYLE );
	m_dwTabWndStyle = dwStyle & __EXTMFC_ALL_FORM_MOVABLE_WND_STYLES;
	::SetWindowLong( m_hWnd, GWL_STYLE, dwStyle & (~__EXTMFC_ALL_FORM_MOVABLE_WND_STYLES) );
	
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		::AfxThrowMemoryException();
	}
}

LRESULT CExtTabWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if( message == WM_PRINT || message == WM_PRINTCLIENT )
	{
		CDC * pDC = CDC::FromHandle( (HDC)wParam );
		CRect rcClient;
		GetClientRect( &rcClient );
		DoPaint( pDC, rcClient );
		return (!0);
	}	
	if(		message == WM_NOTIFY
		&&	m_wndToolTip.GetSafeHwnd() != NULL
		&&	IsWindow( m_wndToolTip.GetSafeHwnd() )
		&&	((LPNMHDR)lParam) != NULL
		&&	((LPNMHDR)lParam)->hwndFrom == m_wndToolTip.GetSafeHwnd()
		&&	((LPNMHDR)lParam)->code == TTN_SHOW
		)
		::SetWindowPos(
			m_wndToolTip.GetSafeHwnd(),
			HWND_TOP,
			0,0,0,0,
			SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE
			);
	return CWnd::WindowProc(message, wParam, lParam);
}

BOOL CExtTabWnd::PreTranslateMessage(MSG* pMsg) 
{
	if(		OnTabWndToolTipQueryEnabled()
		&&	OnAdvancedPopupMenuTipWndGet() == NULL
		&&	m_wndToolTip.GetSafeHwnd() != NULL
		)
		m_wndToolTip.RelayEvent( pMsg );

	return CWnd::PreTranslateMessage(pMsg);
}

CExtPopupMenuTipWnd * CExtTabWnd::OnAdvancedPopupMenuTipWndGet() const
{
	if( ! CExtControlBar::g_bUseAdvancedToolTips )
		return NULL;
	return (&( CExtPopupMenuSite::g_DefPopupMenuSite.GetTip() ));
}

void CExtTabWnd::OnAdvancedPopupMenuTipWndDisplay(
	CExtPopupMenuTipWnd & _ATTW,
	const RECT & rcExcludeArea,
	__EXT_MFC_SAFE_LPCTSTR strTipText
	) const
{
	ASSERT_VALID( this );
	ASSERT( strTipText != NULL && _tcslen( strTipText ) > 0 );
	_ATTW.SetTipStyle( CExtPopupMenuTipWnd::__ETS_RECTANGLE_NO_ICON );
	_ATTW.SetText( strTipText );
	_ATTW.Show( (CWnd*)this, rcExcludeArea );
}

BOOL CExtTabWnd::PreCreateWindow(CREATESTRUCT& cs) 
{
	if(		( ! RegisterTabWndClass() )
		||	( ! CWnd::PreCreateWindow( cs ) )
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	cs.lpszClass = __EXT_TAB_WND_CLASS_NAME;
	return TRUE;
}

bool CExtTabWnd::_ProcessMouseClick(
	CPoint point,
	bool bButtonPressed,
	INT nMouseButton, // MK_... values
	UINT nMouseEventFlags
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return false;
	if( bButtonPressed )
	{
		CWnd * pWndTestChildFrame = GetParentFrame();
		if(		pWndTestChildFrame != NULL
			&&	pWndTestChildFrame->IsKindOf( RUNTIME_CLASS( CMDIChildWnd ) )
			)
		{
			CFrameWnd * pWndFrame =
				pWndTestChildFrame->GetParentFrame();
			if( pWndFrame != NULL )
			{
				CMDIFrameWnd * pWndMDIFrame =
					DYNAMIC_DOWNCAST( CMDIFrameWnd, pWndFrame );
				if( pWndMDIFrame != NULL )
				{
					CMDIChildWnd * pActive =
						pWndMDIFrame->MDIGetActive();
					if( pWndTestChildFrame != pActive )
						((CMDIChildWnd*)pWndTestChildFrame)->MDIActivate();
				}
			}
		}
	} // if( bButtonPressed )

LONG nHitTest = ItemHitTest( point );
	switch( nHitTest )
	{
	case __ETWH_BUTTON_LEFTUP:
	case __ETWH_BUTTON_RIGHTDOWN:
	case __ETWH_BUTTON_SCROLL_HOME:
	case __ETWH_BUTTON_SCROLL_END:
	case __ETWH_BUTTON_HELP:
	case __ETWH_BUTTON_CLOSE:
	case __ETWH_BUTTON_TAB_LIST:
		return
			OnTabWndClickedButton(
				nHitTest,
				bButtonPressed,
				nMouseButton,
				nMouseEventFlags
				);
	default:
		if( nHitTest < __ETWH_TAB_FIRST )
		{
			OnTabWndMouseTrackingPushedStop();
			Invalidate();
			UpdateWindow();
			return false; //true;
		}
	break;
	}
	ASSERT( nHitTest >= 0 && nHitTest < ItemGetCount() );
	if( bButtonPressed )
		m_ptStartDrag = point;
	else
	{
		if(		m_nPushedTrackingButton >= 0
			&&	m_bPushedTrackingCloseButton
			)
		{
			CPoint ptCursor;
			if( ::GetCursorPos( &ptCursor ) )
			{
				ScreenToClient( &ptCursor );
				if( ItemGet( m_nPushedTrackingButton )->CloseButtonRectGet().PtInRect( ptCursor ) )
					OnTabWndClickedItemCloseButton( m_nPushedTrackingButton );
			}
			return true;
		}
	}
	return
		OnTabWndClickedItem(
			nHitTest,
			bButtonPressed,
			nMouseButton,
			nMouseEventFlags
			);
}

void CExtTabWnd::UpdateTabWnd(
	bool bImmediate // = true
	)
{
	ASSERT_VALID( this );
	
	m_bDelayRecalcLayout = true;

	if(		GetSafeHwnd() == NULL
		||	( ! ::IsWindow(GetSafeHwnd()) )
		)
		return;

	if( bImmediate
		&& ( (GetStyle() & WS_VISIBLE) != 0 )
		)
	{
		_RecalcLayoutImpl();
		Invalidate();
		UpdateWindow();
	}
}

int CExtTabWnd::OnTabWndQueryMultiRowColumnDistance() const
{
	ASSERT_VALID( this );
	return 0;
}

bool CExtTabWnd::OnTabWndQueryAlignFontBasedDifference() const
{
	ASSERT_VALID( this );
DWORD dwTabWndStyle = GetTabWndStyle();
	if( (dwTabWndStyle&__ETWS_ALIGN_FONT_DIFF) != 0 )
		return true;
	return false;
}

bool CExtTabWnd::OnTabWndQueryMultiRowColumnLayout() const
{
	ASSERT_VALID( this );
DWORD dwTabWndStyle = GetTabWndStyle();
	if( (dwTabWndStyle&__ETWS_MULTI_ROW_COLUMN) != 0 )
		return true;
	return false;
}

void CExtTabWnd::OnTabWndSyncVisibility()
{
	ASSERT_VALID( this );
}

void CExtTabWnd::OnTabWndButtonsRecalcLayout(
	CRect &rcButtonsArea,
	INT nButtonExtent,
	INT nBetweenButtonExtent
	)
{
	ASSERT_VALID( this );

	m_rcBtnUp.SetRectEmpty();
	m_rcBtnDown.SetRectEmpty();
	m_rcBtnScrollHome.SetRectEmpty();
	m_rcBtnScrollEnd.SetRectEmpty();
	m_rcBtnHelp.SetRectEmpty();
	m_rcBtnClose.SetRectEmpty();
	m_rcBtnTabList.SetRectEmpty();

DWORD dwTabWndStyle = GetTabWndStyle();
bool bShowHelp = (dwTabWndStyle&__ETWS_SHOW_BTN_HELP) ? true : false;
bool bShowClose = (dwTabWndStyle&__ETWS_SHOW_BTN_CLOSE) ? true : false;
bool bShowTabList = (dwTabWndStyle&__ETWS_SHOW_BTN_TAB_LIST) ? true : false;
bool bShowScrollHome = (dwTabWndStyle&__ETWS_SHOW_BTN_SCROLL_HOME) ? true : false;
bool bShowScrollEnd = (dwTabWndStyle&__ETWS_SHOW_BTN_SCROLL_END) ? true : false;
bool bEqualWidth = (dwTabWndStyle&__ETWS_EQUAL_WIDTHS) ? true : false;
	if( OnTabWndQueryMultiRowColumnLayout() )
		bShowScrollHome = bShowScrollEnd = false;

bool bHorz = OrientationIsHorizontal();

INT nButtonShift = - nBetweenButtonExtent - nButtonExtent;

	if( bShowClose )
	{
		m_rcBtnClose = rcButtonsArea;
		rcButtonsArea.OffsetRect(
			bHorz ? nButtonShift : 0,
			bHorz ? 0 : nButtonShift
			);
	} // if( bShowClose )
	if( bShowTabList )
	{
		m_rcBtnTabList = rcButtonsArea;
		rcButtonsArea.OffsetRect(
			bHorz ? nButtonShift : 0,
			bHorz ? 0 : nButtonShift
			);
	} // if( bShowTabList )
	if( bShowHelp )
	{
		m_rcBtnHelp = rcButtonsArea;
		rcButtonsArea.OffsetRect(
			bHorz ? nButtonShift : 0,
			bHorz ? 0 : nButtonShift
			);
	} // if( bShowHelp )

	// auto enable/disable scroll buttons
	m_dwTabWndStyle &= ~(__ETWS_ENABLED_BTN_UP|__ETWS_ENABLED_BTN_DOWN);
	m_dwTabWndStyle &= ~(__ETWS_ENABLED_BTN_SCROLL_HOME|__ETWS_ENABLED_BTN_SCROLL_END);
bool bMultiRowColumn = OnTabWndQueryMultiRowColumnLayout();

	if(		(! bEqualWidth )
		&&	(	_IsScrollAvail()
			||	(	(m_dwTabWndStyle & __ETWS_AUTOHIDE_SCROLL) == 0
				&&	(! bMultiRowColumn )
				)
			)
		)
	{
		if( bShowScrollEnd )
	{
			m_rcBtnScrollEnd = rcButtonsArea;
			rcButtonsArea.OffsetRect(
				bHorz ? nButtonShift : 0,
				bHorz ? 0 : nButtonShift
				);
		} // if( bShowScrollEnd )

		m_rcBtnDown = rcButtonsArea;
		rcButtonsArea.OffsetRect(
			bHorz ? nButtonShift : 0,
			bHorz ? 0 : nButtonShift
			);
		m_rcBtnUp = rcButtonsArea;
		rcButtonsArea.OffsetRect(
			bHorz ? nButtonShift : 0,
			bHorz ? 0 : nButtonShift
			);

		if( bShowScrollHome )
		{
			m_rcBtnScrollHome = rcButtonsArea;
			rcButtonsArea.OffsetRect(
				bHorz ? nButtonShift : 0,
				bHorz ? 0 : nButtonShift
				);
		} // if( bShowScrollHome )

		if( _IsScrollAvail() && m_nScrollPos > 0 )
			m_dwTabWndStyle |= (__ETWS_ENABLED_BTN_UP|__ETWS_ENABLED_BTN_SCROLL_HOME);
		if( _IsScrollAvail() && m_nScrollPos < m_nScrollMaxPos )
			m_dwTabWndStyle |= (__ETWS_ENABLED_BTN_DOWN|__ETWS_ENABLED_BTN_SCROLL_END);
	}
}

INT CExtTabWnd::OnTabWndButtonsCalcWidth(
	INT nButtonExtent,
	INT nBetweenButtonExtent
	)
{
	ASSERT_VALID( this );

DWORD dwTabWndStyle = GetTabWndStyle();
bool bShowHelp = (dwTabWndStyle&__ETWS_SHOW_BTN_HELP) ? true : false;
bool bShowClose = (dwTabWndStyle&__ETWS_SHOW_BTN_CLOSE) ? true : false;
bool bShowTabList = (dwTabWndStyle&__ETWS_SHOW_BTN_TAB_LIST) ? true : false;
bool bShowScrollHome = (dwTabWndStyle&__ETWS_SHOW_BTN_SCROLL_HOME) ? true : false;
bool bShowScrollEnd = (dwTabWndStyle&__ETWS_SHOW_BTN_SCROLL_END) ? true : false;
bool bEqualWidth = (dwTabWndStyle&__ETWS_EQUAL_WIDTHS) ? true : false;
	if( OnTabWndQueryMultiRowColumnLayout() )
		bShowScrollHome = bShowScrollEnd = false;

INT nWidth = 0;
bool bFirstShiftPassed = false;
	if( bShowClose )
	{
		if( ! bFirstShiftPassed )
		{
			nWidth += nBetweenButtonExtent;
			bFirstShiftPassed = true;
		}
		nWidth += nButtonExtent + nBetweenButtonExtent;
	} // if( bShowClose )
	if( bShowHelp )
	{
		if( ! bFirstShiftPassed )
		{
			nWidth += nBetweenButtonExtent;
			bFirstShiftPassed = true;
		}
		nWidth += nButtonExtent + nBetweenButtonExtent;
	} // if( bShowHelp )

	if( bShowTabList )
	{
		if( ! bFirstShiftPassed )
		{
			nWidth += nBetweenButtonExtent;
			bFirstShiftPassed = true;
		}
		nWidth += nButtonExtent + nBetweenButtonExtent;
	} // if( bShowTabList )

bool bMultiRowColumn = OnTabWndQueryMultiRowColumnLayout();
	if( (! bEqualWidth ) && (! bMultiRowColumn ) )
	{
		if( bShowScrollHome )
		{
			if( ! bFirstShiftPassed )
			{
				nWidth += nBetweenButtonExtent;
				bFirstShiftPassed = true;
			}
			nWidth += nButtonExtent + nBetweenButtonExtent;
		} // if( bShowScrollHome )

		if( ! bFirstShiftPassed )
		{
			nWidth += nBetweenButtonExtent;
			bFirstShiftPassed = true;
		}
		nWidth += (nButtonExtent + nBetweenButtonExtent)*2;

		if( bShowScrollEnd )
		{
			if( ! bFirstShiftPassed )
			{
				nWidth += nBetweenButtonExtent;
				bFirstShiftPassed = true;
			}
			nWidth += nButtonExtent + nBetweenButtonExtent;
		} // if( bShowScrollEnd )
	} // if( (! bEqualWidth ) && (! bMultiRowColumn ) )

	return nWidth;
}

void CExtTabWnd::_RecalcLayoutImpl()
{
	ASSERT_VALID( this );

	if(		GetSafeHwnd() == NULL
		||	( ! ::IsWindow(GetSafeHwnd()) )
		)
		return;

	if( ! m_bDelayRecalcLayout )
		return;
	m_bDelayRecalcLayout = false;

	m_rcTabItemsArea.SetRectEmpty();
	m_rcTabNearBorderArea.SetRectEmpty();
	m_nIndexVisFirst = m_nIndexVisLast = -1;

CRect rcClient;
	_RecalcLayout_GetClientRect( rcClient );
	if(		rcClient.IsRectEmpty()
		||	rcClient.right <= rcClient.left
		||	rcClient.bottom <=  rcClient.top
		)
		return;

	m_rcTabItemsArea = rcClient;
DWORD dwOrientation = OrientationGet();
bool bHorz = OrientationIsHorizontal();
CWindowDC dcMeasure( this );
LONG nTabAreaMetric = 0;
bool bMultiRowColumn = OnTabWndQueryMultiRowColumnLayout();
LONG nItemCount = ItemGetCount();
DWORD dwTabWndStyle = GetTabWndStyle();
bool bEqualWidth = (dwTabWndStyle&__ETWS_EQUAL_WIDTHS) ? true : false;
bool bEnableScrollButtons = ! bEqualWidth;
bool bGrouped = (dwTabWndStyle&__ETWS_GROUPED) ? true : false;
bool bGroupedExpandItems = (dwTabWndStyle&__ETWS_GROUPED_EXPAND_ITEMS) ? true : false;

int nPartExtent = 0;
int nPartCrossExtent = 0;
int nMaxAvailPartExtent = 0;
int nPartDistance = 0;
	if( bMultiRowColumn )
	{
		nPartDistance =
			OnTabWndQueryMultiRowColumnDistance();
		CSize _sizeButton =
				OnTabWndCalcButtonSize(
					dcMeasure,
					20
					);
		ASSERT( _sizeButton.cx > 0 && _sizeButton.cy > 0 );
		INT nButtonExtent = bHorz ? _sizeButton.cx : _sizeButton.cy;
		INT nBetweenButtonExtent = bHorz ? __EXTTAB_BETWEEN_BTN_GAP_DX : __EXTTAB_BETWEEN_BTN_GAP_DY;

		int nButtonsPreCalcExtent = 
			OnTabWndButtonsCalcWidth(
				nButtonExtent,
				nBetweenButtonExtent
				);

		bEnableScrollButtons = false;
		m_nItemsExtent = m_nScrollMaxPos = 0;
		switch( dwOrientation )
		{
		case __ETWS_ORIENT_TOP:
		case __ETWS_ORIENT_BOTTOM:
			nMaxAvailPartExtent = rcClient.Width();
			m_rcTabItemsArea.bottom = m_rcTabItemsArea.top;
			break;
		case __ETWS_ORIENT_LEFT:
		case __ETWS_ORIENT_RIGHT:
			nMaxAvailPartExtent = rcClient.Height();
			m_rcTabItemsArea.right = m_rcTabItemsArea.left;
			break;
#ifdef _DEBUG
		default:
			ASSERT( FALSE );
		break;
#endif // _DEBUG
		} // switch( dwOrientation )
		nMaxAvailPartExtent -= nButtonsPreCalcExtent;
	} // if( bMultiRowColumn )
	else
	{
		if( bGrouped )
			m_nItemsExtent = m_nScrollMaxPos = 10;
		else
			m_nItemsExtent = m_nScrollMaxPos = 0;
	} // else from if( bMultiRowColumn )

	if( bGrouped )
		bEqualWidth = false;

INT nVisibleNo = 0;
LONG nIndex = 0;
TAB_ITEM_INFO * pTiiLV = NULL;
	for( nIndex = 0; nIndex < nItemCount; )
	{ // compute extent of items
		TAB_ITEM_INFO * pTii = ItemGet( nIndex );
		ASSERT_VALID( pTii );
		pTii->MultiRowWrapSet( false );
		if( ! pTii->VisibleGet() )
		{
			nIndex++;
			continue;
		}
		
		if( bGrouped )
		{
			if( ! bGroupedExpandItems )
			{
				INT nMaxInGroupExtent = 0;
				INT nNormalGroupItemExtent = 0;
				INT nGroupStartIndex = nIndex;
				INT nCountInGroup = 0;
				for( nCountInGroup = 0; true ; )
				{
					CSize _size = pTii->Measure( &dcMeasure );
					CSize _sizeText = pTii->GetLastMeasuredTextSize();
					CSize _sizeIcon = pTii->GetLastMeasuredIconSize();
					bool bGroupStart =
						(nVisibleNo == 0)
						||
						pTii->GroupStartGet();

					switch( dwOrientation )
					{
					case __ETWS_ORIENT_TOP:
					case __ETWS_ORIENT_BOTTOM:
					{
						nMaxInGroupExtent =
							max( nMaxInGroupExtent, _size.cx );
						INT nGroupItemExtent =
							_size.cx
							- _sizeText.cx
							- ((_sizeIcon.cx > 0) ? __EXTTAB_MARGIN_ICON2TEXT_X : 0)
							;
						nNormalGroupItemExtent =
							max( nNormalGroupItemExtent, nGroupItemExtent );
						//
						if( nVisibleNo != 0
							&& bGroupStart
							)
							m_nItemsExtent += __EXTTAB_BETWEEN_GROUP_GAP_DX;
						nTabAreaMetric =
							max( nTabAreaMetric, _size.cy );
					}
					break;
					case __ETWS_ORIENT_LEFT:
					case __ETWS_ORIENT_RIGHT:
					{
						nMaxInGroupExtent =
							max( nMaxInGroupExtent, _size.cy );
						INT nGroupItemExtent =
							_size.cy
							- _sizeText.cx
							- ((_sizeIcon.cy > 0) ? __EXTTAB_MARGIN_ICON2TEXT_Y : 0)
							;
						nNormalGroupItemExtent =
							max( nNormalGroupItemExtent, nGroupItemExtent );
						//
						if( nVisibleNo != 0
							&& bGroupStart
							)
							m_nItemsExtent += __EXTTAB_BETWEEN_GROUP_GAP_DY;
						nTabAreaMetric =
							max( nTabAreaMetric, _size.cx );
					}
					break;
				#ifdef _DEBUG
					default:
						ASSERT( FALSE );
					break;
				#endif // _DEBUG
					} // switch( dwOrientation )

					nIndex++;
					nVisibleNo++;
					nCountInGroup++;
					ASSERT( nIndex <= nItemCount );
					if( nIndex == nItemCount )
						break;
					pTii = ItemGet( nIndex );
					ASSERT_VALID( pTii );
					if( pTii->GroupStartGet() )
						break;
				} // for( nCountInGroup = 0; true ; )

				ASSERT( nCountInGroup >= 1 );
				INT nGroupExtent =
					nMaxInGroupExtent
					+ nNormalGroupItemExtent * (nCountInGroup-1);
				m_nItemsExtent += nGroupExtent;

				for( INT n = 0; n < nCountInGroup; n++ )
				{ // reset in group minimal sizes
					pTii = ItemGet( nGroupStartIndex + n );
					ASSERT_VALID( pTii );
					bool bInGroupActive = pTii->InGroupActiveGet();

					switch( dwOrientation )
					{
					case __ETWS_ORIENT_TOP:
					case __ETWS_ORIENT_BOTTOM:
						if( bInGroupActive )
							pTii->m_sizeLastMeasuredItem.cx = nMaxInGroupExtent;
						else
							pTii->m_sizeLastMeasuredItem.cx = nNormalGroupItemExtent;
					break;
					case __ETWS_ORIENT_LEFT:
					case __ETWS_ORIENT_RIGHT:
						if( bInGroupActive )
							pTii->m_sizeLastMeasuredItem.cy = nMaxInGroupExtent;
						else
							pTii->m_sizeLastMeasuredItem.cy = nNormalGroupItemExtent;
					break;
#ifdef _DEBUG
					default:
						ASSERT( FALSE );
					break;
#endif // _DEBUG
					} // switch( dwOrientation )
				} // reset in group minimal sizes

			} // if( ! bGroupedExpandItems )
			else
			{
				CSize _size = pTii->Measure( &dcMeasure );
				bool bGroupStart =
						(nVisibleNo == 0)
					||	pTii->GroupStartGet()
					;
				switch( dwOrientation )
				{
				case __ETWS_ORIENT_TOP:
				case __ETWS_ORIENT_BOTTOM:
					m_nItemsExtent += _size.cx;
					nTabAreaMetric =
						max( nTabAreaMetric, _size.cy );
					if( nVisibleNo != 0
						&& bGroupStart
						)
						m_nItemsExtent += __EXTTAB_BETWEEN_GROUP_GAP_DX;
				break;
				case __ETWS_ORIENT_LEFT:
				case __ETWS_ORIENT_RIGHT:
					m_nItemsExtent += _size.cy;
					nTabAreaMetric =
						max( nTabAreaMetric, _size.cx );
					if( nVisibleNo != 0
						&& bGroupStart
						)
						m_nItemsExtent += __EXTTAB_BETWEEN_GROUP_GAP_DY;
				break;
#ifdef _DEBUG
				default:
					ASSERT( FALSE );
				break;
#endif // _DEBUG
				} // switch( dwOrientation )

				nIndex++;
				nVisibleNo++;
			} // else from if( ! bGroupedExpandItems )
		} // if( bGrouped )
		else
		{
			CSize _size = pTii->Measure( &dcMeasure );
			switch( dwOrientation )
			{
			case __ETWS_ORIENT_TOP:
			case __ETWS_ORIENT_BOTTOM:
				if( bMultiRowColumn )
				{
					int nTestPartExtent = nPartExtent + _size.cx;
					if( nTestPartExtent > nMaxAvailPartExtent )
					{
						nPartCrossExtent = max( nPartCrossExtent, _size.cy );
						nPartCrossExtent += nPartDistance;
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
						m_rcTabItemsArea.bottom += nPartCrossExtent;
						nPartExtent = _size.cx;
						nPartCrossExtent = _size.cy;
						if( pTiiLV != NULL )
							pTiiLV->MultiRowWrapSet();
						else
							pTii->MultiRowWrapSet();
					} // if( nTestPartExtent > nMaxAvailPartExtent )
					else
					{
						nPartExtent += _size.cx;
						nPartCrossExtent = max( nPartCrossExtent, _size.cy );
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
					} // else from if( nTestPartExtent > nMaxAvailPartExtent )
					if( nIndex == (nItemCount-1) )
					{
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
						m_rcTabItemsArea.bottom += nPartCrossExtent;
					}
				} // if( bMultiRowColumn )
				else
				{
					m_nItemsExtent += _size.cx;
					nTabAreaMetric =
						max( nTabAreaMetric, _size.cy );
				} // else from if( bMultiRowColumn )
			break;
			case __ETWS_ORIENT_LEFT:
			case __ETWS_ORIENT_RIGHT:
				if( bMultiRowColumn )
				{
					int nTestPartExtent = nPartExtent + _size.cy;
					if( nTestPartExtent > nMaxAvailPartExtent )
					{
						nPartCrossExtent = max( nPartCrossExtent, _size.cx );
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
						nPartCrossExtent += nPartDistance;
						m_rcTabItemsArea.right += nPartCrossExtent;
						nPartExtent = _size.cy;
						nPartCrossExtent = _size.cx;
						if( pTiiLV != NULL )
							pTiiLV->MultiRowWrapSet();
						else
							pTii->MultiRowWrapSet();
					} // if( nTestPartExtent > nMaxAvailPartExtent )
					else
					{
						nPartExtent += _size.cy;
						nPartCrossExtent = max( nPartCrossExtent, _size.cx );
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
					} // else from if( nTestPartExtent > nMaxAvailPartExtent )
					if( nIndex == (nItemCount-1) )
					{
						m_nItemsExtent = max( m_nItemsExtent, nPartCrossExtent );
						m_rcTabItemsArea.right += nPartCrossExtent;
					}
				} // if( bMultiRowColumn )
				else
				{
					m_nItemsExtent += _size.cy;
					nTabAreaMetric =
						max( nTabAreaMetric, _size.cx );
				} // else from if( bMultiRowColumn )
			break;
#ifdef _DEBUG
			default:
				ASSERT( FALSE );
			break;
#endif // _DEBUG
			} // switch( dwOrientation )

			nIndex++;
			nVisibleNo++;
		} // else from if( bGrouped )
		pTiiLV = pTii;
	} // compute extent of items

LONG nSpaceBefore = 0, nSpaceAfter = 0, nSpaceOver = 0;
	OnTabWndMeasureItemAreaMargins(
		nSpaceBefore,
		nSpaceAfter,
		nSpaceOver
		);
LONG nAddShiftForBtns = nSpaceAfter;
	// pre-compute button size
CSize _sizeButton =
		OnTabWndCalcButtonSize(
			dcMeasure,
			nTabAreaMetric
			);
	ASSERT( _sizeButton.cx > 0 && _sizeButton.cy > 0 );
INT nButtonExtent = bHorz ? _sizeButton.cx : _sizeButton.cy;
INT nBetweenButtonExtent = bHorz ? __EXTTAB_BETWEEN_BTN_GAP_DX : __EXTTAB_BETWEEN_BTN_GAP_DY;

	nSpaceAfter += 
		OnTabWndButtonsCalcWidth(
			nButtonExtent,
			nBetweenButtonExtent
			);

LONG nItemRectStartPos = 0;
LONG nItemRectOffs = 0;

	// compute tab window areas
	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:
		nTabAreaMetric =
			max( nTabAreaMetric, __EXTTAB_MIN_HORZ_HEIGHT );
		if( ! bMultiRowColumn )
		{
			m_rcTabItemsArea.top += nSpaceOver;
			m_rcTabItemsArea.bottom =
				m_rcTabItemsArea.top + nTabAreaMetric;
			m_rcTabItemsArea.bottom =
				min( m_rcTabItemsArea.bottom, rcClient.bottom );
			if( m_rcTabItemsArea.bottom < rcClient.bottom )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.top = m_rcTabItemsArea.bottom;
			}
			m_rcTabItemsArea.left += nSpaceBefore;
			m_rcTabItemsArea.right -= nSpaceAfter;
			nItemRectOffs += m_rcTabItemsArea.top;
			m_nScrollMaxPos = m_nItemsExtent - m_rcTabItemsArea.Width();
			if( m_nScrollMaxPos < 0 )
				m_nScrollMaxPos = 0;
		}
		else
		{
			m_rcTabItemsArea.left += nSpaceBefore;
			m_rcTabItemsArea.right -= nSpaceAfter;
			nItemRectOffs += nSpaceOver;
		}
	break;
	case __ETWS_ORIENT_BOTTOM:
		nTabAreaMetric =
			max( nTabAreaMetric, __EXTTAB_MIN_HORZ_HEIGHT );
		if( ! bMultiRowColumn )
		{
			m_rcTabItemsArea.bottom -= nSpaceOver;
			m_rcTabItemsArea.top =
				m_rcTabItemsArea.bottom - nTabAreaMetric;
			m_rcTabItemsArea.top =
				max( m_rcTabItemsArea.top, rcClient.top );
			if( m_rcTabItemsArea.top > rcClient.top )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.bottom = m_rcTabItemsArea.top;
			}
			m_rcTabItemsArea.left += nSpaceBefore;
			m_rcTabItemsArea.right -= nSpaceAfter;
			nItemRectOffs += m_rcTabItemsArea.top;
			m_nScrollMaxPos = m_nItemsExtent - m_rcTabItemsArea.Width();
			if( m_nScrollMaxPos < 0 )
				m_nScrollMaxPos = 0;
		}
		else
		{
			m_rcTabItemsArea.left += nSpaceBefore;
			m_rcTabItemsArea.right -= nSpaceAfter;
			nItemRectOffs += nSpaceOver;
		}
	break;
	case __ETWS_ORIENT_LEFT:
		nTabAreaMetric =
			max( nTabAreaMetric, __EXTTAB_MIN_VERT_WIDTH );
		if( ! bMultiRowColumn )
		{
			m_rcTabItemsArea.left += nSpaceOver;
			m_rcTabItemsArea.right =
				m_rcTabItemsArea.left + nTabAreaMetric;
			m_rcTabItemsArea.right =
				min( m_rcTabItemsArea.right, rcClient.right );
			if( m_rcTabItemsArea.right < rcClient.right )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.left = m_rcTabItemsArea.right;
			}
			m_rcTabItemsArea.top += nSpaceBefore;
			m_rcTabItemsArea.bottom -= nSpaceAfter;
			nItemRectOffs += m_rcTabItemsArea.left;
			m_nScrollMaxPos = m_nItemsExtent - m_rcTabItemsArea.Height();
			if( m_nScrollMaxPos < 0 )
				m_nScrollMaxPos = 0;
		}
		else
		{
			m_rcTabItemsArea.top += nSpaceBefore;
			m_rcTabItemsArea.bottom -= nSpaceAfter;
			nItemRectOffs += nSpaceOver;
		}
	break;
	case __ETWS_ORIENT_RIGHT:
		nTabAreaMetric =
			max( nTabAreaMetric, __EXTTAB_MIN_VERT_WIDTH );
		if( ! bMultiRowColumn )
		{
			m_rcTabItemsArea.right -= nSpaceOver;
			m_rcTabItemsArea.left =
				m_rcTabItemsArea.right - nTabAreaMetric;
			m_rcTabItemsArea.left =
				max( m_rcTabItemsArea.left, rcClient.left );
			if( m_rcTabItemsArea.left > rcClient.left )
			{
				m_rcTabNearBorderArea = rcClient;
				m_rcTabNearBorderArea.right = m_rcTabItemsArea.left;
			}
			m_rcTabItemsArea.top += nSpaceBefore;
			m_rcTabItemsArea.bottom -= nSpaceAfter;
			nItemRectOffs += m_rcTabItemsArea.left;
			m_nScrollMaxPos = m_nItemsExtent - m_rcTabItemsArea.Height();
			if( m_nScrollMaxPos < 0 )
				m_nScrollMaxPos = 0;
		}
		else
		{
			m_rcTabItemsArea.top += nSpaceBefore;
			m_rcTabItemsArea.bottom -= nSpaceAfter;
			nItemRectOffs += nSpaceOver;
		}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( dwOrientation )

	if( ! bMultiRowColumn )
	{
		ASSERT( m_nScrollPos >= 0 );
		ASSERT( m_nScrollMaxPos >= 0 );
		if( m_nScrollPos > m_nScrollMaxPos )
			m_nScrollPos = m_nScrollMaxPos;
	}
	else
		m_nScrollPos = m_nScrollMaxPos = 0;

LONG nEqualItemExtent =
		bHorz
			? __EXTTAB_MIN_HORZ_WIDTH
			: __EXTTAB_MIN_VERT_HEIGHT
			;
bool bCancelEqualWidth = false;
	if( bEqualWidth && m_nVisibleItemCount > 0 )
	{
		if( ! _IsScrollAvail() )
		{
			if( (m_dwTabWndStyle & __ETWS_FULL_WIDTH) == 0 )
				bCancelEqualWidth = true;
		}
		if( ! bCancelEqualWidth )
		{
			INT nTestExtent = bHorz
				? m_rcTabItemsArea.Width()
				: m_rcTabItemsArea.Height();
			nTestExtent /= m_nVisibleItemCount;
			nEqualItemExtent = max( nTestExtent, nEqualItemExtent );
		}
	} // if( bEqualWidth && m_nVisibleItemCount > 0 )

CRect rcButton( m_rcTabItemsArea );
	if( bHorz )
		rcButton.right = rcClient.right - nAddShiftForBtns;
	else
		rcButton.bottom = rcClient.bottom - nAddShiftForBtns;
	rcButton.OffsetRect(
		bHorz ? 0 : (m_rcTabItemsArea.Width() - _sizeButton.cx) / 2,
		bHorz ? (m_rcTabItemsArea.Height() - _sizeButton.cy) / 2 : 0
		);
	rcButton.OffsetRect(
		bHorz ? (-nBetweenButtonExtent) : 0,
		bHorz ? 0 : (-nBetweenButtonExtent)
		);
	if( bHorz )
	{
		rcButton.left = rcButton.right - _sizeButton.cx;
		rcButton.bottom = rcButton.top + _sizeButton.cy;
	}
	else
	{
		rcButton.right = rcButton.left + _sizeButton.cx;
		rcButton.top = rcButton.bottom - _sizeButton.cy;
	}

	OnTabWndButtonsRecalcLayout(
		rcButton,
		nButtonExtent,
		nBetweenButtonExtent
		);

	m_nIndexVisFirst = -1;
	m_nIndexVisLast = -1;

	nVisibleNo = 0;
	m_bEnableTrackToolTips = false;

	if( bMultiRowColumn )
	{
		int nPartStart = 0, nPartCrossExtent = 0;
		for( nIndex = 0; nIndex < nItemCount; )
		{ // setup item rectangles
			TAB_ITEM_INFO * pTii = ItemGet( nIndex );
			ASSERT_VALID( pTii );
			bool bMultiRowWrap =
				(	( nIndex == ( nItemCount - 1 ) )
				||	pTii->MultiRowWrapGet()
				) ? true : false;

			if( ! bMultiRowWrap )
			{
				if( pTii->VisibleGet() )
				{
					const CSize & _size = pTii->GetLastMeasuredItemSize();
					switch( dwOrientation )
					{
					case __ETWS_ORIENT_TOP:
					case __ETWS_ORIENT_BOTTOM:
						nPartCrossExtent = max( nPartCrossExtent, _size.cy );
					break;
					case __ETWS_ORIENT_LEFT:
					case __ETWS_ORIENT_RIGHT:
						nPartCrossExtent = max( nPartCrossExtent, _size.cx );
					break;
#ifdef _DEBUG
					default:
						ASSERT( FALSE );
					break;
#endif // _DEBUG
					} // switch( dwOrientation )
				} // if( pTii->VisibleGet() )
				nIndex++;
				continue;
			} // if( ! bMultiRowWrap )
			int nReviewIndex = nPartStart;
			for( ; nReviewIndex <= nIndex; nReviewIndex ++ )
			{
				TAB_ITEM_INFO * pTii = ItemGet( nReviewIndex );
				ASSERT_VALID( pTii );
				if( ! pTii->VisibleGet() )
					continue;
				pTii->m_bHelperToolTipAvail = false;
				const CSize & _size = pTii->GetLastMeasuredItemSize();
				CRect rcItem( 0, 0, _size.cx, _size.cy );
//bool bGroupStart = bGrouped && pTii->GroupStartGet();
				switch( dwOrientation )
				{
				case __ETWS_ORIENT_TOP:
				case __ETWS_ORIENT_BOTTOM:
					nPartCrossExtent = max( nPartCrossExtent, _size.cy );
					rcItem.OffsetRect( nItemRectStartPos, nItemRectOffs );
					nItemRectStartPos += _size.cx;
				break;
				case __ETWS_ORIENT_LEFT:
				case __ETWS_ORIENT_RIGHT:
					nPartCrossExtent = max( nPartCrossExtent, _size.cx );
					rcItem.OffsetRect( nItemRectOffs, nItemRectStartPos );
					nItemRectStartPos += _size.cy;
				break;
#ifdef _DEBUG
				default:
					ASSERT( FALSE );
				break;
#endif // _DEBUG
				} // switch( dwOrientation )
				pTii->ItemRectSet( rcItem );
				if( m_nIndexVisFirst < 0 )
					m_nIndexVisFirst = nReviewIndex;
				if( m_nIndexVisLast < 0 || m_nIndexVisLast < nReviewIndex )
					m_nIndexVisLast = nReviewIndex;
				nVisibleNo++;
			} // for( ; nReviewIndex <= nIndex; nReviewIndex ++ )
			nIndex++;
			nPartStart = nIndex;
			nItemRectStartPos = 0;
			nPartCrossExtent += nPartDistance;
			nItemRectOffs += nPartCrossExtent;
			nPartCrossExtent = 0;
		} // setup item rectangles
	} // if( bMultiRowColumn )
	else
	{
		for( nIndex = 0; nIndex < nItemCount; nIndex++ )
		{ // setup item rectangles
			TAB_ITEM_INFO * pTii = ItemGet( nIndex );
			ASSERT_VALID( pTii );
			if( ! pTii->VisibleGet() )
				continue;

			pTii->m_bHelperToolTipAvail = false;
			if( (! bMultiRowColumn ) && bEqualWidth && (! bCancelEqualWidth ) )
			{
				if( bHorz )
				{
					if( pTii->m_sizeLastMeasuredItem.cx > nEqualItemExtent )
					{
						m_bEnableTrackToolTips = true;
						pTii->m_bHelperToolTipAvail = true;
					}
					pTii->m_sizeLastMeasuredItem.cx = nEqualItemExtent;
				}
				else
				{
					if( pTii->m_sizeLastMeasuredItem.cy > nEqualItemExtent )
					{
						m_bEnableTrackToolTips = true;
						pTii->m_bHelperToolTipAvail = true;
					}
					pTii->m_sizeLastMeasuredItem.cy = nEqualItemExtent;
				}
			}
			
			const CSize & _size = pTii->GetLastMeasuredItemSize();
			CRect rcItem( 0, 0, _size.cx, _size.cy );

			bool bGroupStart = bGrouped && pTii->GroupStartGet();
			
			switch( dwOrientation )
			{
			case __ETWS_ORIENT_TOP:
			case __ETWS_ORIENT_BOTTOM:
				if(		nVisibleNo != 0
					&&	bGroupStart
					)
					nItemRectStartPos += __EXTTAB_BETWEEN_GROUP_GAP_DX;
				rcItem.OffsetRect( nItemRectStartPos, nItemRectOffs );
				nItemRectStartPos += _size.cx;
			break;
			case __ETWS_ORIENT_LEFT:
			case __ETWS_ORIENT_RIGHT:
				if(		nVisibleNo != 0
					&&	bGroupStart
					)
					nItemRectStartPos += __EXTTAB_BETWEEN_GROUP_GAP_DY;
				rcItem.OffsetRect( nItemRectOffs, nItemRectStartPos );
				nItemRectStartPos += _size.cy;
			break;
#ifdef _DEBUG
			default:
				ASSERT( FALSE );
			break;
#endif // _DEBUG
			} // switch( dwOrientation )

			pTii->ItemRectSet( rcItem );
			rcItem.OffsetRect(
				bHorz
					? (m_rcTabItemsArea.left - m_nScrollPos)
					: 0
				,
				bHorz
					? 0
					: (m_rcTabItemsArea.top - m_nScrollPos)
				);
			CSize _sizeCloseButton =
				OnTabWndQueryItemCloseButtonSize( pTii );
			if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )
			{
				CRect rcTabItemCloseButton = rcItem;
				if( bHorz )
				{
					rcTabItemCloseButton.left = rcTabItemCloseButton.right - _sizeCloseButton.cx;
					rcTabItemCloseButton.bottom = rcTabItemCloseButton.top + _sizeCloseButton.cy;
					rcTabItemCloseButton.OffsetRect(
						- __EXTTAB_MARGIN_ICON2TEXT_X,
						( rcItem.Height() - rcTabItemCloseButton.Height() ) / 2
						);
				} // if( bHorz )
				else
				{
					rcTabItemCloseButton.right = rcTabItemCloseButton.left + _sizeCloseButton.cx;
					rcTabItemCloseButton.top = rcTabItemCloseButton.bottom - _sizeCloseButton.cy;
					rcTabItemCloseButton.OffsetRect(
						( rcItem.Width() - rcTabItemCloseButton.Width() ) / 2,
						- __EXTTAB_MARGIN_ICON2TEXT_Y
						);
				} // else from if( bHorz )
				pTii->CloseButtonRectSet( rcTabItemCloseButton );
			} // if( _sizeCloseButton.cx > 0 && _sizeCloseButton.cy > 0 )
			INT nItemExtentMinVal =
				bHorz ? rcItem.left : rcItem.top;
			
			INT nItemExtentMaxVal =
				bHorz ? rcItem.right : rcItem.bottom;
			
			INT nAreaExtentMinVal = 0;
			INT nAreaExtentMaxVal =
				bHorz
					? (m_rcTabItemsArea.right - m_rcTabItemsArea.left)
					: (m_rcTabItemsArea.bottom - m_rcTabItemsArea.top)
					;
			nAreaExtentMaxVal += __EXTTAB_ADD_END_SCROLL_SPACE + 2;
			if( m_nIndexVisFirst < 0 )
			{
				if( nItemExtentMaxVal >= nAreaExtentMinVal )
					m_nIndexVisFirst = nIndex;
			}
			if( nItemExtentMinVal <= nAreaExtentMaxVal )
				m_nIndexVisLast = nIndex;

			nVisibleNo++;
		} // setup item rectangles
	} // else from if( bMultiRowColumn )
	ASSERT( nVisibleNo == m_nVisibleItemCount );

	if( m_nIndexVisFirst < 0 || m_nIndexVisLast < 0 )
	{
		m_nIndexVisFirst = m_nIndexVisLast = -1;
		bEnableScrollButtons = false;
	}
	else
	{
		if( m_nIndexVisFirst > m_nIndexVisLast )
			m_nIndexVisLast = m_nIndexVisFirst;
		ASSERT( m_nIndexVisFirst <= m_nIndexVisLast );
		ASSERT( 0 <= m_nIndexVisFirst && m_nIndexVisFirst < nItemCount );
		ASSERT( 0 <= m_nIndexVisLast && m_nIndexVisLast < nItemCount );
	}

	ASSERT_VALID( this );
}

void CExtTabWnd::_RecalcLayout_GetClientRect(
	CRect & rcClient
	)
{
	ASSERT_VALID( this );
	if( ! m_rcRecalcLayout.IsRectEmpty() )
		rcClient = m_rcRecalcLayout;
	else
	{
		ASSERT( GetSafeHwnd() != NULL );
		ASSERT( ::IsWindow(GetSafeHwnd()) );
		GetClientRect( &rcClient );
	}
}

void CExtTabWnd::DoPaint( 
	CDC * pDC,
	CRect & rcClient
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC );

CRuntimeClass * pPaintManagerRTC =
		PmBridge_GetPM()->GetRuntimeClass();
	if( m_pHelperLastPaintManagerRTC != pPaintManagerRTC )
	{
		m_pHelperLastPaintManagerRTC = pPaintManagerRTC;
		m_bDelayRecalcLayout = true;
	}

	// recalc delayed layout first
	_RecalcLayoutImpl();

CExtMemoryDC dc(
		pDC,
		&rcClient
		);
	OnTabWndDrawEntire( dc, rcClient );

	PmBridge_GetPM()->OnPaintSessionComplete( this );
}

/////////////////////////////////////////////////////////////////////////////
// CExtTabWnd message handlers

void CExtTabWnd::OnPaint() 
{
	ASSERT_VALID( this );
CPaintDC dcPaint( this );
CRect rcClient;
	GetClientRect( &rcClient );
	if( rcClient.IsRectEmpty() )
		return;
	DoPaint( &dcPaint, rcClient );
}


void CExtTabWnd::OnTabWndMeasureItemAreaMargins(
	LONG & nSpaceBefore,
	LONG & nSpaceAfter,
	LONG & nSpaceOver
	)
{
	ASSERT_VALID( this );
	PmBridge_GetPM()->TabWnd_MeasureItemAreaMargins(
		this,
		nSpaceBefore,
		nSpaceAfter,
		nSpaceOver
		);
}

void CExtTabWnd::_DrawItem(
	CDC & dc,
	LONG nItemIndex
	)
{
TAB_ITEM_INFO * pTii = ItemGet( nItemIndex );
	ASSERT_VALID( pTii );
	if( !pTii->VisibleGet() )
		return;

bool bHorz = OrientationIsHorizontal();
bool bTopLeft = OrientationIsTopLeft();
bool bCenteredText =
	(GetTabWndStyle() & __ETWS_CENTERED_TEXT) ? true : false;
bool bInvertedVerticalMode = 
	(GetTabWndStyle() & __ETWS_INVERT_VERT_FONT) ? true : false;
bool bGroupedMode =
	(GetTabWndStyle() & __ETWS_GROUPED) ? true : false;

CRect rcItem = pTii->ItemRectGet();
	rcItem.OffsetRect(
		bHorz
			? (m_rcTabItemsArea.left - m_nScrollPos)
			: 0
		,
		bHorz
			? 0
			: (m_rcTabItemsArea.top - m_nScrollPos)
		);

	if( !dc.RectVisible( &rcItem ) )
		return;

CSize _sizeTextMeasured = pTii->GetLastMeasuredTextSize();
bool bSelected = pTii->SelectedGet();
CFont & font = _GetTabWndFont( bSelected );
CFont * pFont = &font;
CExtSafeString sText = pTii->TextGet();
CExtCmdIcon * pIcon = pTii->IconGetPtr();
bool bInGroupActive = pTii->InGroupActiveGet();

	OnTabWndDrawItem(
		dc,
		m_rcTabItemsArea,
		nItemIndex,
		pTii,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcItem,
		_sizeTextMeasured,
		pFont,
		sText,
		pIcon
		);
}

void CExtTabWnd::OnTabWndDrawEntire(
	CDC & dc,
	CRect & rcClient
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

bool bGroupedMode =
		(GetTabWndStyle() & __ETWS_GROUPED) ? true : false;

	OnTabWndEraseClientArea(
		dc,
		rcClient,
		m_rcTabItemsArea,
		m_rcTabNearBorderArea,
		OrientationGet(),
		bGroupedMode
		);

LONG nItemCount = ItemGetCount();
LONG nVisCount = ItemGetVisibleCount();
	if( nItemCount > 0 && nVisCount > 0 && m_nIndexVisFirst >= 0 )
	{
		ASSERT( m_nIndexVisFirst <= m_nIndexVisLast );
		ASSERT( 0 <= m_nIndexVisFirst && m_nIndexVisFirst < nItemCount );
		ASSERT( 0 <= m_nIndexVisLast && m_nIndexVisLast < nItemCount );

		bool bHorz = OrientationIsHorizontal();
		bool bTopLeft = OrientationIsTopLeft();

		CRect rcSetMargins(
			( (!bHorz) && (!bTopLeft) ) ? 1 : 0,
			(   bHorz  && (!bTopLeft) ) ? 1 : 0,
			( (!bHorz) &&   bTopLeft  ) ? 1 : 0,
			(   bHorz  &&   bTopLeft  ) ? 1 : 0
			);
		CRect rcPaintItems( m_rcTabItemsArea );
		rcPaintItems.InflateRect(
			bHorz ? 1 : 0,
			bHorz ? 0 : 1
			);
		if( bGroupedMode )
			rcPaintItems.InflateRect(
				bHorz ? 0 : 1,
				bHorz ? 1 : 0
				);
		rcPaintItems.InflateRect(
			rcSetMargins.left,
			rcSetMargins.top,
			rcSetMargins.right,
			rcSetMargins.bottom
			);
		CRgn rgnPaint;
		if( !rgnPaint.CreateRectRgnIndirect(&rcPaintItems) )
		{
			ASSERT( FALSE );
			return;
		}
		dc.SelectClipRgn( &rgnPaint );

		LONG nIndexSelected = -1L;
		for( LONG nIndex = m_nIndexVisFirst; nIndex <= m_nIndexVisLast; nIndex++ )
		{ 
			if( SelectionGet() != nIndex )
			{
				_DrawItem(
					dc,
					nIndex
					);
				dc.SelectClipRgn( &rgnPaint );
			}
			else
				nIndexSelected = nIndex;
		}

		if( nIndexSelected >= 0L )
			_DrawItem(
				dc,
				nIndexSelected
				);

		dc.SelectClipRgn( NULL );
	} // if( nItemCount > 0 && nVisCount > 0 && m_nIndexVisFirst >= 0 )

//bool bAnyButtonTracked =
//			m_nPushedTrackingButton == __ETWH_BUTTON_LEFTUP
//		||	m_nPushedTrackingButton == __ETWH_BUTTON_RIGHTDOWN
//		||	m_nPushedTrackingButton == __ETWH_BUTTON_HELP
//		||	m_nPushedTrackingButton == __ETWH_BUTTON_CLOSE
//		;
//bool bAnyMouseButtonPressed = false;
//	if( bAnyButtonTracked )
//		bAnyMouseButtonPressed =
//			CExtPopupMenuWnd::IsKeyPressed(VK_LBUTTON)
//		||	CExtPopupMenuWnd::IsKeyPressed(VK_MBUTTON)
//		||	CExtPopupMenuWnd::IsKeyPressed(VK_RBUTTON)
//		;

CPoint ptCursor( 0, 0 );
	::GetCursorPos( &ptCursor );
	ScreenToClient( &ptCursor );

bool bTopLeft = OrientationIsTopLeft();
bool bHorz = OrientationIsHorizontal();

	if(		(! m_rcBtnUp.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnUp )
		)
	{
//		bool bEnabled =
//			_IsScrollAvail() && m_nScrollPos > 0;
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_UP) ? true : false;
		bool bHover = bEnabled && m_rcBtnUp.PtInRect(ptCursor) ? true : false;
		//bool bPushed = bEnabled && bHover && bAnyButtonTracked;
		OnTabWndDrawButton(
			dc,
			m_rcBtnUp,
			__ETWH_BUTTON_LEFTUP,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedUp,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnDown.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnDown )
		)
	{
//		bool bEnabled =
//			_IsScrollAvail() && m_nScrollPos < m_nScrollMaxPos;
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_DOWN) ? true : false;
		bool bHover = bEnabled && m_rcBtnDown.PtInRect(ptCursor) ? true : false;
		//bool bPushed = bEnabled && bHover && bAnyButtonTracked;
		OnTabWndDrawButton(
			dc,
			m_rcBtnDown,
			__ETWH_BUTTON_RIGHTDOWN,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedDown,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnHelp.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnHelp )
		)
	{
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_HELP) ? true : false;
		bool bHover = bEnabled && m_rcBtnHelp.PtInRect(ptCursor) ? true : false;
		//bool bPushed = bEnabled && bHover && bAnyButtonTracked;
		OnTabWndDrawButton(
			dc,
			m_rcBtnHelp,
			__ETWH_BUTTON_HELP,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedHelp,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnClose.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnClose )
		)
	{
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_CLOSE) ? true : false;
		bool bHover = bEnabled && m_rcBtnClose.PtInRect(ptCursor) ? true : false;
		//bool bPushed = bEnabled && bHover && bAnyButtonTracked;
		OnTabWndDrawButton(
			dc,
			m_rcBtnClose,
			__ETWH_BUTTON_CLOSE,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedClose,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnTabList.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnTabList )
		)
	{
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_TAB_LIST) ? true : false;
		bEnabled = (bEnabled && ItemGetCount() > 0 );
		bool bHover = bEnabled && m_rcBtnTabList.PtInRect(ptCursor) ? true : false;
		OnTabWndDrawButton(
			dc,
			m_rcBtnTabList,
			__ETWH_BUTTON_TAB_LIST,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedTabList,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnScrollHome.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnScrollHome )
		)
	{
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_HOME) ? true : false;
		bool bHover = bEnabled && m_rcBtnScrollHome.PtInRect(ptCursor) ? true : false;
		OnTabWndDrawButton(
			dc,
			m_rcBtnScrollHome,
			__ETWH_BUTTON_SCROLL_HOME,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedScrollHome,
			bGroupedMode
			);
	}
	if(		(! m_rcBtnScrollEnd.IsRectEmpty() )
		&&	dc.RectVisible( &m_rcBtnScrollEnd )
		)
	{
		bool bEnabled = (GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_END) ? true : false;
		bool bHover = bEnabled && m_rcBtnScrollEnd.PtInRect(ptCursor) ? true : false;
		OnTabWndDrawButton(
			dc,
			m_rcBtnScrollEnd,
			__ETWH_BUTTON_SCROLL_END,
			bTopLeft,
			bHorz,
			bEnabled,
			bHover,
			m_bPushedScrollEnd,
			bGroupedMode
			);
	}
}

void CExtTabWnd::OnTabWndDrawButton(
	CDC & dc,
	CRect & rcButton,
	LONG nHitTest,
	bool bTopLeft,
	bool bHorz,
	bool bEnabled,
	bool bHover,
	bool bPushed,
	bool bGroupedMode
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

	PmBridge_GetPM()->PaintTabButton(
		dc,
		rcButton,
		nHitTest,
		bTopLeft,
		bHorz,
		bEnabled,
		bHover,
		bPushed,
		bGroupedMode,
		this
		);
}

void CExtTabWnd::OnTabWndEraseClientArea(
	CDC & dc,
	CRect & rcClient,
	CRect & rcTabItemsArea,
	CRect & rcTabNearBorderArea,
	DWORD dwOrientation,
	bool bGroupedMode
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintTabClientArea(
		dc,
		rcClient,
		rcTabItemsArea,
		rcTabNearBorderArea,
		dwOrientation,
		bGroupedMode,
		this
		);
}

void CExtTabWnd::OnTabWndDrawItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	LONG nItemIndex,
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );

bool bEnabled = pTii->EnabledGet();
bool bHover = (m_nHoverTrackingHitTest == nItemIndex) ? true : false;

	bEnabled;
	bHover;

	if( (pTii->GetItemStyle() & __ETWI_CENTERED_TEXT) != 0 )
		bCenteredText = true;

	PmBridge_GetPM()->PaintTabItem(
		dc,
		rcTabItemsArea,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcEntireItem,
		sizeTextMeasured,
		pFont,
		sText,
		pIcon,
		this,
		nItemIndex
		);
}

bool CExtTabWnd::OnTabWndMouseTrackingPushedStart(
	INT nMouseButton, // MK_... values
	LONG nHitTest
	)
{
	ASSERT_VALID( this );

	if(		m_nPushedTrackingHitTest != __ETWH_NOWHERE
		&&	m_nPushedTrackingButton != (-1L)
		)
	{
		if(		m_nPushedTrackingHitTest != nMouseButton
			||	m_nPushedTrackingButton != nHitTest
			)
			return false;
	}

	m_bPushedUp = false;
	m_bPushedDown = false;
	m_bPushedScrollHome = false;
	m_bPushedScrollEnd = false;
	m_bPushedHelp = false;
	m_bPushedClose = false;
	m_bPushedTabList = false;

	switch( nHitTest )
	{
	case __ETWH_BUTTON_LEFTUP:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_UP) == 0 )
			return false;
		m_bPushedUp = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_RIGHTDOWN:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_DOWN) == 0 )
			return false;
		m_bPushedDown = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_SCROLL_HOME:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_HOME) == 0 )
			return false;
		m_bPushedScrollHome = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_SCROLL_END:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_END) == 0 )
			return false;
		m_bPushedScrollEnd = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_HELP:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_HELP) == 0 )
			return false;
		m_bPushedHelp = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_CLOSE:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if( (GetTabWndStyle() & __ETWS_ENABLED_BTN_CLOSE) == 0 )
			return false;
		m_bPushedClose = true;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_TAB_LIST:
		if( nMouseButton != MK_LBUTTON )
			return false;
		if(		(GetTabWndStyle() & __ETWS_ENABLED_BTN_TAB_LIST) == 0 
			|| 	(ItemGetCount() <= 0)
			)
			return false;
		m_bPushedTabList = true;
		UpdateTabWnd( true );
		break;
	} // switch( nHitTest )

	m_nPushedTrackingHitTest = nMouseButton;
	m_nPushedTrackingButton = nHitTest;
	m_bTrackingButtonPushed = true;
	m_bPushedTrackingCloseButton = false;
	if( m_nPushedTrackingButton >= 0 )
	{
		CPoint ptCursor;
		if( GetCursorPos( &ptCursor ) )
		{
			ScreenToClient( &ptCursor );
			TAB_ITEM_INFO * pTII = ItemGet( m_nPushedTrackingButton );
			if( pTII->CloseButtonRectGet().PtInRect( ptCursor ) )
			{
				if(		(GetTabWndStyleEx()&__ETWS_EX_CLOSE_ON_SELECTED_ONLY) == 0
					||	SelectionGet() == m_nPushedTrackingButton
					)
					m_bPushedTrackingCloseButton = true;
			}
		} // if( GetCursorPos( &ptCursor ) )
	} // if( m_nPushedTrackingButton >= 0 )

	if( CExtMouseCaptureSink::GetCapture() != m_hWnd )
		CExtMouseCaptureSink::SetCapture( m_hWnd );

	return true;
}

void CExtTabWnd::OnTabWndMouseTrackingPushedStop(
	bool bEnableReleaseCapture // = true
	)
{
	ASSERT_VALID( this );

	if( (GetTabWndStyle() & __ETWS_SCROLL_BY_PAGE) == 0
		&&	m_nScrollDirectionRest != 0
		)
		SendMessage( WM_CANCELMODE );

	m_ptStartDrag.x = m_ptStartDrag.y = -1;
	switch( m_nPushedTrackingButton )
	{
	case __ETWH_BUTTON_LEFTUP:
		m_bPushedUp = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_RIGHTDOWN:
		m_bPushedDown = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_SCROLL_HOME:
		m_bPushedScrollHome = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_SCROLL_END:
		m_bPushedScrollEnd = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_HELP:
		m_bPushedHelp = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_CLOSE:
		m_bPushedClose = false;
		UpdateTabWnd( true );
		break;
	case __ETWH_BUTTON_TAB_LIST:
		m_bPushedTabList = false;
		UpdateTabWnd( true );
		break;
	} // switch( m_nPushedTrackingButton )

	m_nPushedTrackingHitTest = __ETWH_NOWHERE;
	m_bPushedTrackingCloseButton = false;
	m_nPushedTrackingButton = (-1L);
	m_bTrackingButtonPushed = false;
	if( bEnableReleaseCapture && CExtMouseCaptureSink::GetCapture() == GetSafeHwnd() )
		CExtMouseCaptureSink::ReleaseCapture();
}

bool CExtTabWnd::OnTabWndStartDrag( LONG nIndex )
{
	ASSERT_VALID( this );
	ASSERT( ItemGetCount() > 0 );
	ASSERT(
		nIndex >= 0
		&& nIndex < ItemGetCount()
		);

	if(		nIndex >= 0
		&&	nIndex < ItemGetCount()
		&&	(GetTabWndStyle()&__ETWS_ITEM_DRAGGING) != 0
		&&	(GetTabWndStyle()&__ETWS_GROUPED) == 0
		)
		return true;
	
	return false;
}

bool CExtTabWnd::OnTabWndClickedButton(
	LONG nHitTest,
	bool bButtonPressed,
	INT nMouseButton, // MK_... values
	UINT nMouseEventFlags
	)
{
	ASSERT_VALID( this );
	nMouseEventFlags;

	if( bButtonPressed )
	{
		if( OnTabWndMouseTrackingPushedStart( nMouseButton, nHitTest ) )
		{
			Invalidate();
			UpdateWindow();
			if(		nHitTest == __ETWH_BUTTON_LEFTUP
				||	nHitTest == __ETWH_BUTTON_RIGHTDOWN
				)
			{
				OnTabWndDoScroll(
					(nHitTest == __ETWH_BUTTON_LEFTUP) ? -1 : +1,
					true
					);
			}
		}
	}
	else
	{
		OnTabWndMouseTrackingPushedStop();

		if(		nMouseButton == MK_LBUTTON 
			&&	(ItemGetCount() > 0)
			&&	(
					(	nHitTest == __ETWH_BUTTON_SCROLL_HOME
					&&	(GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_HOME) != 0
					)
				||	(	nHitTest == __ETWH_BUTTON_SCROLL_END
					&&	(GetTabWndStyle() & __ETWS_ENABLED_BTN_SCROLL_END) != 0
					)
				)
			)
		{
			LONG nItem = 
				nHitTest == __ETWH_BUTTON_SCROLL_HOME
					? 0
					: ItemGetCount() - 1;
			ASSERT( nItem >= 0 && nItem < ItemGetCount() );
			ItemEnsureVisible( nItem, true );
		}
		
		if(		nMouseButton == MK_LBUTTON 
			&&	nHitTest == __ETWH_BUTTON_TAB_LIST
			&&	(GetTabWndStyle() & __ETWS_ENABLED_BTN_TAB_LIST) != 0
			&&	(ItemGetCount() > 0)
			)
		{
			// show tab list popup menu
			CExtPopupMenuWnd * pPopup = new CExtPopupMenuWnd;
			if( pPopup->CreatePopupMenu( GetSafeHwnd() ) )
			{
				UINT nStartID = 0xffff;
				for( LONG nIndex = 0; nIndex < m_arrItems.GetSize(); nIndex++ )
				{
					TAB_ITEM_INFO * pTii = ItemGet( nIndex );
					if( pTii != NULL ) 
					{
						bool bGrouped = (GetTabWndStyle()&__ETWS_GROUPED) ? true : false;
						if(		bGrouped 
							&&	nIndex != 0 
							)
						{
							LONG nGroupStart;
							LONG nGroupEnd;
							ItemGetGroupRange(
								nIndex,
								nGroupStart,
								nGroupEnd
								);
							if( nGroupStart == nIndex )
								VERIFY( pPopup->ItemInsertCommand( CExtPopupMenuWnd::TYPE_SEPARATOR, -1 ) );
						}

						if( !pTii->VisibleGet() )
							continue;

						CExtCmdIcon * pCmdIcon = OnTabWndQueryItemIcon( pTii );
						static CExtCmdIcon g_EmptyIcon;
						CExtCmdIcon & _icon = (pCmdIcon != NULL) ? (*pCmdIcon) : g_EmptyIcon;
						VERIFY( 
							pPopup->ItemInsertCommand(
								nStartID + nIndex,
								-1,
								pTii->TextGet(),
								NULL,
								_icon,
								( SelectionGet() == nIndex )
								) 
							);
						
						INT nItemsCount = pPopup->ItemGetCount();
						pPopup->ItemEnabledSet( 
							nItemsCount - 1, 
							pTii->EnabledGet()
							);
					}	
				}
				CPoint ptTrack( m_rcBtnTabList.left, m_rcBtnTabList.bottom );
				ClientToScreen( &ptTrack );
				CRect rcExcludeArea( m_rcBtnTabList );
				ClientToScreen( &rcExcludeArea );
				
				DWORD nAlign = TPMX_LEFTALIGN; 
				switch( __ETWS_ORIENT_MASK & OrientationGet() ) 
				{
				case __ETWS_ORIENT_TOP:
					nAlign = TPMX_TOPALIGN;
					break;
				case __ETWS_ORIENT_BOTTOM:
					nAlign = TPMX_BOTTOMALIGN;
					break;
				case __ETWS_ORIENT_LEFT:
					nAlign = TPMX_LEFTALIGN;
					break;
				case __ETWS_ORIENT_RIGHT:
					nAlign = TPMX_RIGHTALIGN;
					break;
				}

				UINT nCmdRetVal = 0;
				HWND hWndOwn = m_hWnd;
				if(	pPopup->TrackPopupMenu(
						TPMX_OWNERDRAW_FIXED
							| nAlign
							| TPMX_COMBINE_DEFAULT
							| TPMX_DO_MESSAGE_LOOP
							| TPMX_NO_WM_COMMAND
							| TPMX_NO_CMD_UI
							| TPMX_NO_HIDE_RARELY,
						ptTrack.x, 
						ptTrack.y,
						&rcExcludeArea,
						pPopup, // NULL,
						_CbPaintCombinedTabListBtnContent,
						&nCmdRetVal,
						true
						)
					)
				{
					if( ! ::IsWindow(hWndOwn) )
						return true;
					// user selected the nCmdRetVal item
					if( nCmdRetVal != 0 )
						SelectionSet( nCmdRetVal - nStartID , true, true );
					
					Invalidate();
				}
				else
				{
					ASSERT( FALSE );
					delete pPopup;
				}
				if( ! ::IsWindow(hWndOwn) )
					return true;
			}
			else
			{
				ASSERT( FALSE );
				delete pPopup;
			}
		}
	}

	return true;
}

void CExtTabWnd::OnTabWndClickedItemCloseButton(
	LONG nItemIndex
	)
{
	ASSERT_VALID( this );
	nItemIndex;
}

void CExtTabWnd::_CbPaintCombinedTabListBtnContent(
	LPVOID pCookie,
	CDC & dc,
	const CWnd & refWndMenu,
	const CRect & rcExcludeArea, // in screen coords
	int eCombineAlign // CExtPopupMenuWnd::e_combine_align_t values
	)
{
	dc;
	refWndMenu;
	rcExcludeArea;
	eCombineAlign;
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( refWndMenu.GetSafeHwnd() != NULL );
	ASSERT( eCombineAlign != CExtPopupMenuWnd::__CMBA_NONE );
	
	if( rcExcludeArea.IsRectEmpty() )
		return;
	
	CRect rcExcludeAreaX(rcExcludeArea);
	refWndMenu.ScreenToClient( &rcExcludeAreaX );

CExtPopupMenuWnd * pPopup = (CExtPopupMenuWnd*)pCookie;
	ASSERT_VALID( pPopup );
	ASSERT_KINDOF( CExtPopupMenuWnd, pPopup );
	pPopup->PmBridge_GetPM()->PaintTabButton(
		dc,
		rcExcludeAreaX,
		__ETWH_BUTTON_TAB_LIST,
		true,	// bTopLeft
		false,	// bHorz
		true,	// bEnabled,
		false,	// bHover,
		false,	// bPushed,
		false,	// bGroupedMode
		(CObject*)&refWndMenu
		);
}

bool CExtTabWnd::OnTabWndClickedItem(
	LONG nItemIndex,
	bool bButtonPressed,
	INT nMouseButton, // MK_... values
	UINT nMouseEventFlags
	)
{
	ASSERT_VALID( this );
	nMouseEventFlags;

	if( bButtonPressed )
	{
		ASSERT( ItemGetCount() > 0 );
		ASSERT( nItemIndex >= 0 && nItemIndex < ItemGetCount() );
		if( OnTabWndMouseTrackingPushedStart( nMouseButton, nItemIndex ) )
		{
			SelectionSet( nItemIndex, true, true );
			return ( nMouseButton == MK_RBUTTON ) ? false : true;
		}
	}
	OnTabWndMouseTrackingPushedStop();
	return ( nMouseButton == MK_RBUTTON ) ? false : true;
}

void CExtTabWnd::OnTabWndDoScroll(
	LONG nStep,
	bool bSmoothScroll // = true
	)
{
	ASSERT_VALID( this );
	ASSERT( nStep != 0 );
	
	if( !_IsScrollAvail() )
		return;
//	if( _IsScrolling() )
//		return;
	if(		m_rcTabItemsArea.left >= m_rcTabItemsArea.right
		||	m_rcTabItemsArea.top >= m_rcTabItemsArea.bottom
		||	ItemGetCount() == 0
		||	ItemGetVisibleCount() == 0
		)
		return;

bool bHorz = OrientationIsHorizontal();
INT nScrollRange = bHorz
		? m_rcTabItemsArea.right - m_rcTabItemsArea.left
		: m_rcTabItemsArea.bottom - m_rcTabItemsArea.top
		;
	ASSERT( nScrollRange > 0 );

	if( nStep < 0 )
	{ // up
		if( m_nScrollPos == 0 )
			return;
		ASSERT( (GetTabWndStyle() & __ETWS_ENABLED_BTN_UP) != 0 );

		if( bSmoothScroll )
		{
			if( GetTabWndStyle() & __ETWS_SCROLL_BY_PAGE )
				m_nScrollDirectionRest = -nScrollRange;
			else
				m_nScrollDirectionRest = -m_nScrollPos;
			SetTimer(
				__EXTTAB_SCROLL_TIMER_ID,
				__EXTTAB_SCROLL_TIMER_PERIOD,
				NULL
				);
			return;
		}

		//m_nScrollPos -= nScrollRange;
		//m_nScrollPos--;
		m_nScrollPos += nStep;
		if( m_nScrollPos < 0 )
			m_nScrollPos = 0;

	} // up
	else
	{ // down
		if( m_nScrollPos == m_nScrollMaxPos )
			return;
		ASSERT( (GetTabWndStyle() & __ETWS_ENABLED_BTN_DOWN) != 0 );

		if( bSmoothScroll )
		{
			if( GetTabWndStyle() & __ETWS_SCROLL_BY_PAGE )
				m_nScrollDirectionRest = nScrollRange;
			else
				m_nScrollDirectionRest = m_nScrollMaxPos - m_nScrollPos;
			SetTimer(
				__EXTTAB_SCROLL_TIMER_ID,
				__EXTTAB_SCROLL_TIMER_PERIOD,
				NULL
				);
			return;
		}

		//m_nScrollPos += nScrollRange;
		//m_nScrollPos++;
		m_nScrollPos += nStep;
		if( m_nScrollPos > m_nScrollMaxPos )
			m_nScrollPos = m_nScrollMaxPos;
	} // down

	UpdateTabWnd( true );
}

LONG CExtTabWnd::SelectionSet(
	LONG nSelIndex,
	bool bEnsureVisible, // = false
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
LONG nOldSelection = m_nSelIndex;
	if( ! OnTabWndSelectionChange( m_nSelIndex, nSelIndex, true ) )
		return m_nSelIndex;
LONG nCount = ItemGetCount();
	if( nSelIndex < 0 )
		m_nSelIndex = -1;
	else
	{
		ASSERT( nSelIndex < nCount );
		m_nSelIndex = nSelIndex;
	} // else from if( nSelIndex < 0 )
	if( nOldSelection >= 0 )
	{
		ASSERT( nOldSelection < nCount );
		
		TAB_ITEM_INFO * pTii = ItemGet( nOldSelection );
		pTii->ModifyItemStyle(
			__ETWI_SELECTED,
			0
			);
	}
	if( m_nSelIndex >= 0 )
	{
		ASSERT( m_nSelIndex < nCount );

		TAB_ITEM_INFO * pTii = ItemGet( m_nSelIndex );
		ASSERT_VALID( pTii );
		//ASSERT( pTii->VisibleGet() );
		
		pTii->ModifyItemStyle(
			0,
			__ETWI_SELECTED
			);
	}
	if( ! OnTabWndSelectionChange( nOldSelection, m_nSelIndex, false ) )
		return m_nSelIndex;

	if( bEnsureVisible && m_nSelIndex >= 0 && ItemGetCount() > 0 )
	{
		ASSERT( m_nSelIndex < ItemGetCount() );
		// item should not have "invisible" style
		VERIFY(
			ItemEnsureVisible(
				m_nSelIndex,
				bUpdateTabWnd
				)
			);
	} // if( bEnsureVisible )
	else
		UpdateTabWnd( bUpdateTabWnd );
	return nOldSelection;
}

LONG CExtTabWnd::SelectionDelayedGet() const
{
	ASSERT_VALID( this );
	return m_nDelayedSelIndex;
}

bool CExtTabWnd::SelectionDelay(
	LONG nSelIndex, // = -1, // -1 - to cancel
	DWORD dwMilliseconds // = 0 // should be > 0 if nSelIndex >= 0
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( ::IsWindow(GetSafeHwnd()) );
	if( nSelIndex < 0 )
	{
		if( m_nDelayedSelIndex >= 0 )
		{
			KillTimer( __EXTTAB_SELECTION_DELAY_TIMER_ID );
			m_nDelayedSelIndex = -1;
		}
		return true;
	}

	if( m_nDelayedSelIndex >= 0 )
	{
		KillTimer( __EXTTAB_SELECTION_DELAY_TIMER_ID );
		m_nDelayedSelIndex = -1;
	}

INT nVisibleItemCount = ItemGetVisibleCount();
	if( nVisibleItemCount == 0 )
		return false;
INT nItemCount = ItemGetCount();
	if( nSelIndex >= nItemCount )
	{
		ASSERT( FALSE );
		return false;
	}
	if( ! (ItemGet( nSelIndex )->VisibleGet()) )
	{
		ASSERT( FALSE );
		return false;
	}
	ASSERT( dwMilliseconds > 0 );
	m_nDelayedSelIndex = nSelIndex;
	SetTimer(
		__EXTTAB_SELECTION_DELAY_TIMER_ID,
		dwMilliseconds,
		NULL
		);
	return true;
}

LONG CExtTabWnd::ItemFindByLParam(
	LPARAM lParam,
	LONG nIndexStartSearch, // = -1
	bool bIncludeVisible, // = true,
	bool bIncludeInvisible // = false
	) const
{
	ASSERT_VALID( this );
LONG nIndex = ( nIndexStartSearch < 0 )
		? 0
		: (nIndexStartSearch + 1)
		;
LONG nCount = ItemGetCount();
	for( ; nIndex < nCount; nIndex++ )
	{
		const TAB_ITEM_INFO * pTii = ItemGet( nIndex );
		ASSERT_VALID( pTii );
		bool bItemVisible = pTii->VisibleGet();
		if( bIncludeVisible  && !bIncludeInvisible && !bItemVisible )
			continue;
		if( !bIncludeVisible && bIncludeInvisible  && bItemVisible )
			continue;
		LPARAM lParam2 = pTii->LParamGet();
		if( lParam == lParam2 )
			return nIndex;
	} // for( ; nIndex < nCount; nIndex++ )
	return -1;
}

LONG CExtTabWnd::ItemFindByStyle(
	DWORD dwItemStyleInclude,
	DWORD dwItemStyleExclude, // = __ETWI_INVISIBLE
	LONG nIndexStartSearch // = -1
	) const
{
	ASSERT_VALID( this );
LONG nIndex = ( nIndexStartSearch < 0 )
		? 0
		: (nIndexStartSearch + 1)
		;
LONG nCount = ItemGetCount();
	for( ; nIndex < nCount; nIndex++ )
	{
		const TAB_ITEM_INFO * pTii = ItemGet( nIndex );
		ASSERT_VALID( pTii );
		DWORD dwItemStyle = pTii->GetItemStyle();
		if( dwItemStyleInclude != 0 )
		{
			DWORD dwItemStylesReallyIncluded = dwItemStyle & dwItemStyleInclude;
			if( dwItemStylesReallyIncluded == 0 )
				continue;
		}
		if( dwItemStyleExclude != 0 )
		{
			DWORD dwItemStylesReallyExculded = dwItemStyle & dwItemStyleExclude;
			if( dwItemStylesReallyExculded != 0 )
				continue;
		}
		return nIndex;
	} // for( ; nIndex < nCount; nIndex++ )
	return -1;
}

bool CExtTabWnd::OnTabWndSelectionChange(
	LONG nOldItemIndex,
	LONG nNewItemIndex,
	bool bPreSelectionTest
	)
{
	ASSERT_VALID( this );
	nOldItemIndex;
	if( bPreSelectionTest )
	{
		if( nNewItemIndex >= 0 )
		{
			TAB_ITEM_INFO * pTII = ItemGet( nNewItemIndex );
			ASSERT_VALID( pTII );
			if( ! pTII->EnabledGet() )
				return false;
		}
	}   
	return true;
}

CExtCmdIcon * CExtTabWnd::OnTabWndQueryItemIcon(
	const CExtTabWnd::TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	if( pTii != NULL ) 
		return (CExtCmdIcon*)&pTii->m_icon;
	return NULL;
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::OnTabWndQueryItemText(
	const CExtTabWnd::TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	if( pTii != NULL ) 
		return pTii->m_sText;
	return NULL;
}

__EXT_MFC_SAFE_LPCTSTR CExtTabWnd::OnTabWndQueryItemTooltipText(
	const CExtTabWnd::TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	if( pTii != NULL ) 
		return pTii->m_sTooltipText;
	return NULL;
}

CSize CExtTabWnd::OnTabWndQueryItemCloseButtonSize(
	const TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
CExtCmdIcon * pCmdIcon = OnTabWndQueryItemCloseButtonShape( pTii );
	if( pCmdIcon == NULL || pCmdIcon->IsEmpty() )
		return CSize( 0, 0 );
CSize _sizeCloseButton = pCmdIcon->GetSize();
CExtPaintManager * pPM = PmBridge_GetPM();
	ASSERT_VALID( pPM );
	_sizeCloseButton.cx = pPM->UiScalingDo( _sizeCloseButton.cx, CExtPaintManager::__EUIST_X );
	_sizeCloseButton.cy = pPM->UiScalingDo( _sizeCloseButton.cy, CExtPaintManager::__EUIST_Y );
	return _sizeCloseButton;
}

CExtCmdIcon * CExtTabWnd::OnTabWndQueryItemCloseButtonShape(
	const TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
bool bCloseButtonOnThisTab =
			(	( (GetTabWndStyle()&__ETWS_GROUPED) == 0 )
		&&	( (GetTabWndStyleEx()&__ETWS_EX_CLOSE_ON_TABS) != 0 )
		&&	( (pTii->GetItemStyleEx()&__ETWI_EX_NO_CLOSE_ON_TAB) == 0 )
		) ? true : false;
	if( ! bCloseButtonOnThisTab )
		return NULL;
	if( ! m_iconTabItemCloseButton.IsEmpty() )
		return (&m_iconTabItemCloseButton);
CExtBitmap _bmp;
	if( ! _bmp.LoadBMP_Resource( MAKEINTRESOURCE(IDB_EXT_TAB_CLOSE_BTN_DEFAULT) ) )
		return NULL;
	_bmp.Make32();
	_bmp.AlphaColor( RGB(255,0,255), RGB(0,0,0), BYTE(0) );
CRect rc( 0, 0, 16, 16 );
	m_iconTabItemCloseButton.m_bmpDisabled.FromBitmap( _bmp, rc );
	m_iconTabItemCloseButton.m_bmpDisabled.AdjustHLS( COLORREF(-1L), COLORREF(-1L), 0.0, 0.0, -1.0 );
	m_iconTabItemCloseButton.m_bmpDisabled.AdjustAlpha( -0.25 );
	rc.OffsetRect( 16, 0 );
	m_iconTabItemCloseButton.m_bmpNormal.FromBitmap( _bmp, rc );
	m_iconTabItemCloseButton.m_bmpNormal.AdjustHLS( COLORREF(-1L), COLORREF(-1L), 0.0, 0.0, -0.25 );
	//m_iconTabItemCloseButton.m_bmpNormal.AdjustAlpha( -0.25 );
	rc.OffsetRect( 16, 0 );
	m_iconTabItemCloseButton.m_bmpHover.FromBitmap( _bmp, rc );
	rc.OffsetRect( 16, 0 );
	m_iconTabItemCloseButton.m_bmpPressed.FromBitmap( _bmp, rc );
	m_iconTabItemCloseButton.m_dwFlags =
		  __EXT_ICON_PERSISTENT_BITMAP_DISABLED
		| __EXT_ICON_PERSISTENT_BITMAP_HOVER
		| __EXT_ICON_PERSISTENT_BITMAP_PRESSED;
	return (&m_iconTabItemCloseButton);
}

INT CExtTabWnd::OnTabWndQueryItemCloseButtonPaintState(
	const TAB_ITEM_INFO * pTii
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	if( (GetTabWndStyleEx()&__ETWS_EX_CLOSE_ON_SELECTED_ONLY) != 0 )
	{
		if( SelectionGetPtr() != pTii )
			return INT( CExtCmdIcon::__PAINT_INVISIBLE );
	}
	if( ! pTii->EnabledGet() )
		return INT( CExtCmdIcon::__PAINT_DISABLED );
LONG nPushedIdx = GetPushedTrackingItem();
	if( nPushedIdx >= 0 )
	{
		if( nPushedIdx == ItemGetIndexOf( pTii ) )
		{
			if( m_bPushedTrackingCloseButton )
				return INT( CExtCmdIcon::__PAINT_PRESSED );
		}
	}
LONG nHoverIdx = GetHoverTrackingItem();
	if( nHoverIdx >= 0 )
	{
		if( nHoverIdx == ItemGetIndexOf( pTii ) )
			return INT( CExtCmdIcon::__PAINT_HOVER );
	}
	if( SelectionGetPtr() != pTii )
		return INT( CExtCmdIcon::__PAINT_DISABLED );
	return INT( CExtCmdIcon::__PAINT_NORMAL );
}

void CExtTabWnd::OnTabWndUpdateItemMeasure(
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	CDC & dcMeasure,
	CSize & sizePreCalc
	)
{
	ASSERT_VALID( this );
	PmBridge_GetPM()->TabWnd_UpdateItemMeasure(
		this,
		pTii,
		dcMeasure,
		sizePreCalc
		);
}

void CExtTabWnd::OnTabWndItemInsert(
	LONG nItemIndex,
	CExtTabWnd::TAB_ITEM_INFO * pTii
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	ASSERT( ItemGetCount() > 0 );
	ASSERT( nItemIndex >= 0 && nItemIndex < ItemGetCount() );
	ASSERT( ItemGetIndexOf( pTii ) == nItemIndex );
	pTii;
	nItemIndex;
}

void CExtTabWnd::OnTabWndRemoveItem(
	LONG nItemIndex,
	LONG nCount,
	bool bPreRemove
	)
{
	ASSERT_VALID( this );
	nItemIndex;
	nCount;
	bPreRemove;
}

void CExtTabWnd::OnTabWndRemoveAllItems(
	bool bPreRemove
	)
{
	ASSERT_VALID( this );
	bPreRemove;
}

CSize CExtTabWnd::OnTabWndCalcButtonSize(
	CDC & dcMeasure,
	LONG nTabAreaMetric // vertical max width or horizontal max heights of all tabs
	)
{
	ASSERT_VALID( this );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	dcMeasure;
	nTabAreaMetric;
CSize _sizeTmp(
		::MulDiv( ::GetSystemMetrics( SM_CXSMSIZE ), 3, 4 ),
		::MulDiv( ::GetSystemMetrics( SM_CYSMSIZE ), 3, 4 )
		);
int nMetric =
	min( _sizeTmp.cx, _sizeTmp.cy ) + 1;
CSize _sizeButton(
		max( nMetric, __EXTTAB_BTN_MIN_DX ),
		max( nMetric, __EXTTAB_BTN_MIN_DY )
		);
	return _sizeButton;
}

void CExtTabWnd::OnSysColorChange() 
{
	ASSERT_VALID( this );
	CWnd::OnSysColorChange();
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnSysColorChange( this );
	g_CmdManager.OnSysColorChange( pPM, this );
	Invalidate();
}

LRESULT CExtTabWnd::OnDisplayChange( WPARAM wParam, LPARAM lParam )
{
LRESULT lResult = CWnd::OnDisplayChange( wParam, lParam );
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnDisplayChange( this, (INT)wParam, CPoint(lParam) );
	g_CmdManager.OnDisplayChange( pPM, this, (INT)wParam, CPoint(lParam) );
	return lResult;
}

LRESULT CExtTabWnd::OnThemeChanged( WPARAM wParam, LPARAM lParam )
{
LRESULT lResult = Default();
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnThemeChanged( this, wParam, lParam );
	g_CmdManager.OnThemeChanged( pPM, this, wParam, lParam );
	return lResult;
}

void CExtTabWnd::OnSettingChange(UINT uFlags, __EXT_MFC_SAFE_LPCTSTR lpszSection) 
{
	ASSERT_VALID( this );
	CWnd::OnSettingChange(uFlags, lpszSection);
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnSettingChange( this, uFlags, lpszSection );
	g_CmdManager.OnSettingChange( pPM, this, uFlags, lpszSection );
	Invalidate();
}

LRESULT CExtTabWnd::_OnQueryRepositionCalcEffect(WPARAM wParam, LPARAM lParam)
{
	ASSERT_VALID( this );
	lParam;
	if( !m_bReflectQueryRepositionCalcEffect )
		return 0L;
CExtControlBar::QUERY_REPOSITION_CALC_EFFECT_DATA * p_qrced =
		(CExtControlBar::QUERY_REPOSITION_CALC_EFFECT_DATA *)
			(wParam);
	ASSERT( p_qrced != NULL );
	ASSERT_VALID( p_qrced->m_pWndToReposChilds );
	ASSERT( p_qrced->m_pWndToReposChilds->GetSafeHwnd() != NULL );
	ASSERT( ::IsWindow( p_qrced->m_pWndToReposChilds->GetSafeHwnd() ) );
	if( p_qrced->IsQueryReposQuery() )
	{
		if( p_qrced->m_pWndToReposChilds->IsKindOf(
				RUNTIME_CLASS(CFrameWnd)
				)
			)
			p_qrced->ExcludeFromCenterSet();
	} // if( p_qrced->IsQueryReposQuery() )
	return 0L;
}

void CExtTabWnd::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	ASSERT_VALID( this );
	pWnd;
	point;
}

void CExtTabWnd::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);

DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle & WS_VISIBLE) == 0 )
		return;
CExtPopupMenuTipWnd * pATTW =
		OnAdvancedPopupMenuTipWndGet();
	if( pATTW != NULL )
	{
		if(		pATTW->GetSafeHwnd() != NULL
			&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
			)
			pATTW->Hide();
	}
	if(		m_wndToolTip.GetSafeHwnd() != NULL
		&&	::IsWindow( m_wndToolTip.GetSafeHwnd() )
		)
		m_wndToolTip.DelTool( this, 1 );
	CWnd::CancelToolTips();

	UpdateTabWnd( true );
}

void CExtTabWnd::OnWindowPosChanged(WINDOWPOS FAR* lpwndpos) 
{
	CWnd::OnWindowPosChanged(lpwndpos);

DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle & WS_VISIBLE) == 0 )
		return;

CExtPopupMenuTipWnd * pATTW =
		OnAdvancedPopupMenuTipWndGet();
	if( pATTW != NULL )
	{
		if(		pATTW->GetSafeHwnd() != NULL
			&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
			)
			pATTW->Hide();
	}
	if(		m_wndToolTip.GetSafeHwnd() != NULL
		&&	::IsWindow( m_wndToolTip.GetSafeHwnd() )
		)
		m_wndToolTip.DelTool( this, 1 );
	CWnd::CancelToolTips();

	UpdateTabWnd( true );
}

void CExtTabWnd::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CWnd::OnShowWindow(bShow, nStatus);
	
DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle & WS_VISIBLE) == 0 )
		return;

	UpdateTabWnd( true );
}

void CExtTabWnd::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
//	CWnd::OnLButtonDblClk(nFlags, point);
	nFlags;
	point;
}
void CExtTabWnd::OnRButtonDblClk(UINT nFlags, CPoint point) 
{
//	CWnd::OnRButtonDblClk(nFlags, point);
	nFlags;
	point;
}
void CExtTabWnd::OnMButtonDblClk(UINT nFlags, CPoint point) 
{
//	CWnd::OnMButtonDblClk(nFlags, point);
	nFlags;
	point;
}
int CExtTabWnd::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message) 
{
//	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
	pDesktopWnd;
	nHitTest;
	message;

bool bTopParentActive = false;
HWND hWndForeground = ::GetForegroundWindow();
CWnd * pWndForeground =
		(hWndForeground == NULL)
			? NULL
			: CWnd::FromHandlePermanent(hWndForeground)
			;
	if( pWndForeground != NULL )
	{
		CWnd * pWndTopParent =
			GetTopLevelParent();
		if( pWndTopParent != NULL )
		{
			HWND hWndLastActivePopup = ::GetLastActivePopup( pWndTopParent->m_hWnd );
			CWnd * pWndLastActivePopup =
				(hWndLastActivePopup == NULL)
					? NULL
					: CWnd::FromHandlePermanent(hWndLastActivePopup)
					;
			if( pWndForeground == pWndLastActivePopup )
				bTopParentActive = true;
		}
	}

	if( ! bTopParentActive )
	{
		HWND hWndParent = ::GetParent( GetSafeHwnd() );
		if( hWndParent != NULL )
			::SetFocus( hWndParent );
	}

	return MA_NOACTIVATE;
}
void CExtTabWnd::OnSetFocus(CWnd* pOldWnd) 
{
//	CWnd::OnSetFocus(pOldWnd);
	pOldWnd;
HWND hWndParent = ::GetParent( GetSafeHwnd() );
	if( hWndParent != NULL )
		::SetFocus( hWndParent );
}

void CExtTabWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, true, MK_LBUTTON, nFlags ) )
		CWnd::OnLButtonDown(nFlags, point);
}
void CExtTabWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, false, MK_LBUTTON, nFlags ) )
		CWnd::OnLButtonUp(nFlags, point);
}
void CExtTabWnd::OnRButtonDown(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, true, MK_RBUTTON, nFlags ) )
		CWnd::OnRButtonDown(nFlags, point);
}
void CExtTabWnd::OnRButtonUp(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, false, MK_RBUTTON, nFlags ) )
		CWnd::OnRButtonUp(nFlags, point);
}
void CExtTabWnd::OnMButtonDown(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, true, MK_MBUTTON, nFlags ) )
		CWnd::OnMButtonDown(nFlags, point);
}
void CExtTabWnd::OnMButtonUp(UINT nFlags, CPoint point) 
{
	if( !_ProcessMouseClick( point, false, MK_MBUTTON, nFlags ) )
		CWnd::OnMButtonUp(nFlags, point);
}

HCURSOR CExtTabWnd::g_hCursor = ::LoadCursor( NULL, IDC_ARROW );

BOOL CExtTabWnd::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite =
		CExtCustomizeSite::GetCustomizeSite( m_hWnd );
	if(		pSite != NULL
		&&	pSite->IsCustomizeMode()
		)
		return FALSE;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
	if( CExtControlBar::FindHelpMode(this) )
		return FALSE;
	if( g_hCursor != NULL )
	{
//		HCURSOR hCursor = GetCursor();
//		if( hCursor != g_hCursor )
//		{
//			CPoint ptRealCursorPos;
//			::GetCursorPos( &ptRealCursorPos );
//			ScreenToClient( &ptRealCursorPos );
//			CRect rcClient;
//			GetClientRect( &rcClient );
//			if( rcClient.PtInRect(ptRealCursorPos) )
				::SetCursor( g_hCursor );
//		} // if( hCursor != g_hCursor )
		return TRUE;
	} // if( g_hCursor != NULL )
	
	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

void CExtTabWnd::_ProcessMouseMove(
	UINT nFlags, 
	CPoint point
	)
{
	ASSERT_VALID( this );
	nFlags;
CExtPopupMenuTipWnd * pATTW =
		OnAdvancedPopupMenuTipWndGet();
	if( m_nPushedTrackingHitTest >= 0 )
	{
		LONG nIndexToStartDrag = m_nPushedTrackingButton;
		LONG nItemCount = ItemGetCount();
		if(		nItemCount == 0
			||	nIndexToStartDrag < 0
			||	nIndexToStartDrag >= nItemCount
			)
		{
			if( pATTW != NULL )
			{
				if(		pATTW->GetSafeHwnd() != NULL
					&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
					)
					pATTW->Hide();
			}
			if(		m_wndToolTip.GetSafeHwnd() != NULL
				&&	::IsWindow( m_wndToolTip.GetSafeHwnd() )
				)
				m_wndToolTip.DelTool( this, 1 );
			CWnd::CancelToolTips();
			bool bCancelMode = true;
			if(		m_bPushedUp
				||	m_bPushedDown
				||	m_bPushedScrollHome
				||	m_bPushedScrollEnd
				||	m_bPushedHelp
				||	m_bPushedClose
				||	m_bPushedTabList
				|| _IsScrolling()
				)
				bCancelMode = false;
			if( bCancelMode )
				SendMessage( WM_CANCELMODE );
			return;
		}
		if(		(abs(point.x-m_ptStartDrag.x)) > __EXTTAB_DRAG_START_DX
			||	(abs(point.y-m_ptStartDrag.y)) > __EXTTAB_DRAG_START_DY
			)
		{
			if(		(! m_bPushedTrackingCloseButton )
				&&	OnTabWndStartDrag(nIndexToStartDrag)
				)
			{
				if( pATTW != NULL )
				{
					if(		pATTW->GetSafeHwnd() != NULL
						&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
						)
						pATTW->Hide();
				}
				if(		m_wndToolTip.GetSafeHwnd() != NULL
					&&	::IsWindow( m_wndToolTip.GetSafeHwnd() )
					)
					m_wndToolTip.DelTool( this, 1 );
				CWnd::CancelToolTips();

				if( m_hWnd != NULL && ::IsWindow( m_hWnd ) )
					OnTabWndProcessDrag( nIndexToStartDrag );

				return;
			}
		}
	}

	if( m_bTrackingButtonPushed )
	{
		if( pATTW != NULL )
		{
			if(		pATTW->GetSafeHwnd() != NULL
				&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
				)
				pATTW->Hide();
		}
		if(		m_wndToolTip.GetSafeHwnd() != NULL
			&&	::IsWindow( m_wndToolTip.GetSafeHwnd() )
			)
			m_wndToolTip.DelTool( this, 1 );
		CWnd::CancelToolTips();
		return;
	}

	if( ! CExtPopupMenuWnd::TestHoverEnabledFromActiveHWND(
			GetSafeHwnd()
			)
		)
		return;

LONG nHitTest = ItemHitTest( point );
bool bCancelTips = true;
	switch( nHitTest )
	{
	case __ETWH_BUTTON_LEFTUP:
	case __ETWH_BUTTON_RIGHTDOWN:
	case __ETWH_BUTTON_SCROLL_HOME:
	case __ETWH_BUTTON_SCROLL_END:
	case __ETWH_BUTTON_HELP:
	case __ETWH_BUTTON_CLOSE:
	case __ETWH_BUTTON_TAB_LIST:
		if( m_nHoverTrackedIndex != nHitTest )
		{
			if( OnTabWndMouseTrackingHoverStart( nHitTest ) )
				bCancelTips = false;
		} // if( m_nHoverTrackedIndex != nHitTest )
		else
			bCancelTips = false;
		break;
	default:
		{
			bool bRedraw = true;
			if(	nHitTest >= 0 )
			{
				if( m_nHoverTrackedIndex != nHitTest )
				{
					// if mouse is really on items
					if( OnTabWndMouseTrackingHoverStart( nHitTest ) )
					{
						bRedraw = OnTabWndQueryHoverChangingRedraw();
					}
				} // if( m_nHoverTrackedIndex != nHitTest )
				else
					bRedraw = false;
			}
			else
			{
				// if mouse is really NOT on items
				OnTabWndMouseTrackingHoverStop( true );
			}

			if( bRedraw )
			{
				Invalidate();
				UpdateWindow();
			}

			if(		ItemGetVisibleCount() > 0
				&&	nHitTest >= 0
				)
			{
				TAB_ITEM_INFO * pTii = ItemGet(nHitTest);
				ASSERT_VALID( pTii );
				ASSERT( pTii->VisibleGet() );
				CExtPopupMenuTipWnd * pATTW =
					OnAdvancedPopupMenuTipWndGet();

				if(		(GetTabWndStyle() & __ETWS_HOVER_FOCUS)
					&&	m_nDelayedSelIndex != nHitTest
					)
				{
					if( m_nDelayedSelIndex >= 0 )
					{
						VERIFY( SelectionDelay() );
					}
					VERIFY(
						SelectionDelay(
							nHitTest,
							__EXTTAB_SELECTION_DELAY_TIMER_PERIOD
							)
						);
					ASSERT( m_nDelayedSelIndex == nHitTest );
				}
				else if(
						OnTabWndToolTipQueryEnabled()
					&&	(	pATTW != NULL
						||	m_wndToolTip.GetSafeHwnd() != NULL
						)
					)
				{
					CString sItemTooltipText = 
						pTii->TooltipTextGet();
					if(		sItemTooltipText.IsEmpty() 
						&&	m_bEnableTrackToolTips
						)
					{
						ASSERT( GetTabWndStyle() & __ETWS_EQUAL_WIDTHS );
						if( pTii->IsToolTipAvailByExtent() )
							sItemTooltipText = pTii->TextGet();
					}
					if( ! sItemTooltipText.IsEmpty() )
					{
						bCancelTips = false;
						CRect rcItem = pTii->ItemRectGet();
						if( _IsScrollAvail() )
						{
							if( OrientationIsHorizontal() )
								rcItem.OffsetRect( -m_nScrollPos, 0 );
							else
								rcItem.OffsetRect( 0, -m_nScrollPos );
						}
						if( pATTW != NULL )
						{
							CRect rcArea( rcItem );
							rcArea.InflateRect( 4, 4 );
							ClientToScreen( &rcArea );
							OnAdvancedPopupMenuTipWndDisplay(
								*pATTW,
								rcArea,
								__EXT_MFC_SAFE_LPCTSTR(sItemTooltipText)
								);
						}
						else
							m_wndToolTip.AddTool(
								this,
								sItemTooltipText,
								&rcItem,
								1
								);
					}
				}
			}
		}
		break;
	} // switch( nHitTest )

	if( bCancelTips )
	{
		CExtPopupMenuTipWnd * pATTW =
			OnAdvancedPopupMenuTipWndGet();
		if( pATTW != NULL )
		{
			if(		pATTW->GetSafeHwnd() != NULL
				&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
				)
				pATTW->Hide();
		}
		if(		m_wndToolTip.GetSafeHwnd() != NULL
			&&	::IsWindow( m_wndToolTip.GetSafeHwnd() )
			)
			m_wndToolTip.DelTool( this, 1 );
		CWnd::CancelToolTips();
	}
}

void CExtTabWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
//	CWnd::OnMouseMove(nFlags, point);
	_ProcessMouseMove( nFlags, point );
}

void CExtTabWnd::OnCaptureChanged(CWnd *pWnd) 
{
	CWnd::OnCaptureChanged(pWnd);
	if(		pWnd != NULL
		&&	pWnd != this
		)
	{
		OnTabWndMouseTrackingPushedStop( false );
		OnTabWndMouseTrackingHoverStop( false );
	}
	else if( m_bTrackingButtonHover )
		SendMessage( WM_CANCELMODE );
}

void CExtTabWnd::OnCancelMode() 
{
	CWnd::OnCancelMode();

	if( m_nScrollDirectionRest != 0 )
	{
		m_nScrollDirectionRest = 0;
		KillTimer( __EXTTAB_SCROLL_TIMER_ID );
	}

	OnTabWndMouseTrackingPushedStop( true );
	OnTabWndMouseTrackingHoverStop( true );
}

bool CExtTabWnd::OnTabWndMouseTrackingHoverStart(
	LONG nHitTest
	)
{
	ASSERT_VALID( this );

	if( m_nHoverTrackedIndex == nHitTest )
		return true;

	if( ! CExtPopupMenuWnd::TestHoverEnabledFromActiveHWND( GetSafeHwnd() ) )
		return false;

	m_nHoverTrackingHitTest = nHitTest;
	m_bTrackingButtonHover = true;
	Invalidate();
	UpdateWindow();
	if( CExtMouseCaptureSink::GetCapture() != m_hWnd )
		CExtMouseCaptureSink::SetCapture( m_hWnd );
	m_nHoverTrackedIndex = nHitTest;
CExtPopupMenuTipWnd * pATTW =
		OnAdvancedPopupMenuTipWndGet();

	if(		OnTabWndToolTipQueryEnabled()
		&&	(	pATTW != NULL
			||	m_wndToolTip.GetSafeHwnd() != NULL
			)
		)
	{
		CRect rcTipTrack( 0, 0, 0, 0 );
		CExtSafeString sTooltipText;

#if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)
		CExtLocalResourceHelper _LRH;
#endif
		switch( nHitTest )
		{
		case __ETWH_BUTTON_LEFTUP:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_LEFTUP ) )
				sTooltipText = _T("Previous");
			rcTipTrack = m_rcBtnUp;
			break;

		case __ETWH_BUTTON_RIGHTDOWN:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_RIGHTDOWN ) )
				sTooltipText = _T("Next");
			rcTipTrack = m_rcBtnDown;
			break;
		case __ETWH_BUTTON_SCROLL_HOME:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_SCROLL_HOME ) )
				sTooltipText = _T("Home");
			rcTipTrack = m_rcBtnScrollHome;
			break;
		case __ETWH_BUTTON_SCROLL_END:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_SCROLL_END ) )
				sTooltipText = _T("End");
			rcTipTrack = m_rcBtnScrollEnd;
			break;		case __ETWH_BUTTON_HELP:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_HELP ) )
				sTooltipText = _T("Help");
			rcTipTrack = m_rcBtnHelp;
			break;
		case __ETWH_BUTTON_CLOSE:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_CLOSE ) )
				sTooltipText = _T("");
			rcTipTrack = m_rcBtnClose;
			break;
		case __ETWH_BUTTON_TAB_LIST:
			if( ! g_ResourceManager->LoadString( sTooltipText, IDS_EXTTABWND_BTN_TAB_LIST ) )
				sTooltipText = _T("");
			rcTipTrack = m_rcBtnTabList;
			break;
		} // switch( nHitTest )
		if( ! sTooltipText.IsEmpty() )
		{
			if( pATTW != NULL )
			{
				CRect rcArea( rcTipTrack );
				ClientToScreen( &rcArea );
				OnAdvancedPopupMenuTipWndDisplay(
					*pATTW,
					rcArea,
					sTooltipText
					);
			}
			else
				m_wndToolTip.AddTool(
					this,
					sTooltipText,
					&rcTipTrack,
					1
					);
		}
		else
		{
			if( pATTW != NULL )
			{
				if(		pATTW->GetSafeHwnd() != NULL
					&&	(pATTW->GetStyle()&WS_VISIBLE) != 0
					)
					pATTW->Hide();
			}
			m_wndToolTip.DelTool( this, 1 );
			CWnd::CancelToolTips();
		}
	} // if( OnTabWndToolTipQueryEnabled() ...

	return true;
}

void CExtTabWnd::OnTabWndMouseTrackingHoverStop(
	bool bEnableReleaseCapture // = true
	)
{
	ASSERT_VALID( this );

	m_nHoverTrackedIndex = -1L;

	if(! m_bTrackingButtonHover )
		return;
	m_bTrackingButtonHover = false;
	m_nHoverTrackingHitTest = __ETWH_NOWHERE;
	Invalidate();
	if( bEnableReleaseCapture )
		CExtMouseCaptureSink::ReleaseCapture();
}

bool CExtTabWnd::OnTabWndQueryHoverChangingRedraw() const
{
	ASSERT_VALID( this );
	if( (GetTabWndStyleEx()&__ETWS_EX_CLOSE_ON_TABS) != 0 )
		return true;
	return PmBridge_GetPM()->QueryTabWndHoverChangingRedraw( this );
}

void CExtTabWnd::OnTimer(__EXT_MFC_UINT_PTR nIDEvent) 
{
	switch( nIDEvent )
	{
	case __EXTTAB_SCROLL_TIMER_ID:
	{
		if( m_nScrollDirectionRest != 0 )
		{
			OnTabWndDoScroll(
				(m_nScrollDirectionRest < 0)
					? - ( min(__EXTTAB_SCROLL_STEP,(-m_nScrollDirectionRest)) )
					: + ( min(__EXTTAB_SCROLL_STEP, m_nScrollDirectionRest  ) )
				,
				false
				);
			if( m_nScrollDirectionRest < 0 )
			{
				//m_nScrollDirectionRest++;
				m_nScrollDirectionRest += __EXTTAB_SCROLL_STEP;
				if( m_nScrollDirectionRest > 0 )
					m_nScrollDirectionRest = 0;
			}
			else
			{
				//m_nScrollDirectionRest--;
				m_nScrollDirectionRest -= __EXTTAB_SCROLL_STEP;
				if( m_nScrollDirectionRest < 0 )
					m_nScrollDirectionRest = 0;
			}
		} // if( m_nScrollDirectionRest != 0 )

		if( m_nScrollDirectionRest == 0 )
			KillTimer( __EXTTAB_SCROLL_TIMER_ID );
	}
	break; // case __EXTTAB_SCROLL_TIMER_ID
	case __EXTTAB_SELECTION_DELAY_TIMER_ID:
	{
		KillTimer( __EXTTAB_SELECTION_DELAY_TIMER_ID );
		if( m_nSelIndex != m_nDelayedSelIndex
			&& m_nDelayedSelIndex >= 0
			&& m_nDelayedSelIndex < ItemGetCount()
			&& ItemGetVisibleCount() > 0
			&& ItemGet(m_nDelayedSelIndex)->VisibleGet()
			)
		{
			CPoint ptCursor( 0, 0 );
			if( ! ::GetCursorPos( &ptCursor ) )
				return;
			ScreenToClient( &ptCursor );
			LONG nHitTest = ItemHitTest( ptCursor );
			if( nHitTest == m_nDelayedSelIndex )
				SelectionSet( m_nDelayedSelIndex, true, true );
		} // if( m_nSelIndex != m_nDelayedSelIndex )
		m_nDelayedSelIndex = -1;
	}
	break; // case __EXTTAB_SELECTION_DELAY_TIMER_ID
	default:
	{
		CWnd::OnTimer(nIDEvent);
	}
	break;
	} // switch( nIDEvent )
}

INT CExtTabWnd::OnTabWndGetParentSizingMargin(
	DWORD dwOrientation
	) const
{
	ASSERT_VALID( this );
INT nMargin = 0;
	if( PmBridge_GetPM()->TabWnd_GetParentSizingMargin(
			nMargin,
			dwOrientation,
			const_cast < CExtTabWnd * > ( this )
			)
		)
	{
		ASSERT( nMargin >= 0 );
		return nMargin;
	}
	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:
		return __EXTTAB_IN_FRAME_GAP_TOP_DY;
	case __ETWS_ORIENT_BOTTOM:
		return __EXTTAB_IN_FRAME_GAP_BOTTOM_DY;
	case __ETWS_ORIENT_LEFT:
		return __EXTTAB_IN_FRAME_GAP_LEFT_DX;
	case __ETWS_ORIENT_RIGHT:
		return __EXTTAB_IN_FRAME_GAP_RIGHT_DX;
	default:
		ASSERT( FALSE );
		return 0;
	} // switch( dwOrientation )
}

LRESULT CExtTabWnd::OnSizeParent( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	wParam;
	
	if( ! m_bReflectParentSizing )
		return 0;

AFX_SIZEPARENTPARAMS * lpLayout =
		(AFX_SIZEPARENTPARAMS *) lParam;
	ASSERT( lpLayout != NULL );
CRect rcFrameRest = &lpLayout->rect;
	if(		rcFrameRest.left >= rcFrameRest.right
		||	rcFrameRest.top >= rcFrameRest.bottom
		)
	{
		if( lpLayout->hDWP == NULL )
			return 0;
		::SetWindowPos(
			m_hWnd,
			NULL, 0, 0, 0, 0,
			SWP_NOSIZE|SWP_NOMOVE
				|SWP_NOZORDER|SWP_NOOWNERZORDER
				|SWP_HIDEWINDOW
			);
		return 0;
	}

	OnTabWndSyncVisibility();

DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle&WS_VISIBLE) == 0 )
		return 0;

	m_rcRecalcLayout = rcFrameRest;
	m_bDelayRecalcLayout = true;
	_RecalcLayoutImpl();
	m_rcRecalcLayout.SetRect( 0, 0, 0, 0 );
	
	if( m_rcTabItemsArea.IsRectEmpty() )
		return 0;

CSize _sizeNeeded = m_rcTabItemsArea.Size();
	if( _sizeNeeded.cx <= 0 || _sizeNeeded.cy <= 0 )
		return 0;

CRect rcOwnLayout( rcFrameRest );
DWORD dwOrientation = OrientationGet();
	if(		dwOrientation == __ETWS_ORIENT_LEFT
		||	dwOrientation == __ETWS_ORIENT_RIGHT
		)
	{
		HWND hWndParent = ::GetParent( m_hWnd );
		ASSERT( hWndParent != NULL && ::IsWindow(hWndParent) );
		DWORD dwStyleEx = (DWORD)::GetWindowLong( hWndParent, GWL_EXSTYLE );
		bool bParentRTL = ( (dwStyleEx&WS_EX_LAYOUTRTL) != 0 ) ? true : false;
		if( bParentRTL )
		{
			if( dwOrientation == __ETWS_ORIENT_LEFT )
				dwOrientation = __ETWS_ORIENT_RIGHT;
			else
				dwOrientation = __ETWS_ORIENT_LEFT;
		} // if( bParentRTL )
	}

	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:
	{
		ASSERT( _sizeNeeded.cy > 0 );
		//_sizeNeeded.cy += __EXTTAB_IN_FRAME_GAP_TOP_DY; // + 3;
		_sizeNeeded.cy += OnTabWndGetParentSizingMargin( dwOrientation );
		lpLayout->rect.top += _sizeNeeded.cy;
		rcOwnLayout.bottom = rcOwnLayout.top + _sizeNeeded.cy;
		lpLayout->sizeTotal.cy += _sizeNeeded.cy;
	}
	break;
	case __ETWS_ORIENT_BOTTOM:
	{
		ASSERT( _sizeNeeded.cy > 0 );
		_sizeNeeded.cy ++;
		_sizeNeeded.cy ++;
		//_sizeNeeded.cy += __EXTTAB_IN_FRAME_GAP_BOTTOM_DY;
		_sizeNeeded.cy += OnTabWndGetParentSizingMargin( dwOrientation );
		lpLayout->rect.bottom -= _sizeNeeded.cy;
		rcOwnLayout.top = rcOwnLayout.bottom - _sizeNeeded.cy;
		lpLayout->sizeTotal.cy += _sizeNeeded.cy;
	}
	break;
	case __ETWS_ORIENT_LEFT:
	{
		ASSERT( _sizeNeeded.cx > 0 );
		//_sizeNeeded.cx += __EXTTAB_IN_FRAME_GAP_LEFT_DX; // + 3;
		_sizeNeeded.cx += OnTabWndGetParentSizingMargin( dwOrientation );
		lpLayout->rect.left += _sizeNeeded.cx;
		rcOwnLayout.right = rcOwnLayout.left + _sizeNeeded.cx;
		lpLayout->sizeTotal.cx += _sizeNeeded.cx;
	}
	break;
	case __ETWS_ORIENT_RIGHT:
	{
		ASSERT( _sizeNeeded.cx > 0 );
		_sizeNeeded.cx ++;
		_sizeNeeded.cx ++;
		//_sizeNeeded.cx += __EXTTAB_IN_FRAME_GAP_RIGHT_DX;
		_sizeNeeded.cx += OnTabWndGetParentSizingMargin( dwOrientation );
		lpLayout->rect.right -= _sizeNeeded.cx;
		rcOwnLayout.left = rcOwnLayout.right - _sizeNeeded.cx;
		lpLayout->sizeTotal.cx += _sizeNeeded.cx;
	}
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( dwOrientation )

	ASSERT( ! rcOwnLayout.IsRectEmpty() );
	if( lpLayout->hDWP != NULL )
	{
		::AfxRepositionWindow(
			lpLayout,
			m_hWnd,
			&rcOwnLayout
			);
		::SetWindowPos(
			m_hWnd,
			NULL, 0, 0, 0, 0,
			SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER
				|SWP_FRAMECHANGED
			);
		UpdateTabWnd( true );
	} // if( lpLayout->hDWP != NULL )
	return 0;
}

bool CExtTabWnd::OnTabWndToolTipQueryEnabled() const
{
	return true;
}

bool CExtTabWnd::OnTabWndProcessDrag( LONG nIndex )
{
HWND hWndOwn = GetSafeHwnd();
	ASSERT( hWndOwn != NULL && ::IsWindow(hWndOwn) );

	if( CExtMouseCaptureSink::GetCapture() != hWndOwn )
		CExtMouseCaptureSink::SetCapture( hWndOwn );
	m_bDragging = true;

	for( bool bStopFlag = false; (!bStopFlag) && ::IsWindow(hWndOwn); )
	{
		// Process all the messages in the message queue
		if( !::WaitMessage() )
			break;

		MSG msg;
		while( ::IsWindow(hWndOwn) && PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
		{
			switch( msg.message )
			{
			case WM_RBUTTONDOWN:
			case WM_RBUTTONUP:
			case WM_RBUTTONDBLCLK:
			case WM_MBUTTONDOWN:
			case WM_MBUTTONUP:
			case WM_MBUTTONDBLCLK:
			case WM_NCLBUTTONDOWN:
			case WM_NCLBUTTONUP:
			case WM_NCLBUTTONDBLCLK:
			case WM_NCRBUTTONDOWN:
			case WM_NCRBUTTONUP:
			case WM_NCRBUTTONDBLCLK:
			case WM_NCMBUTTONDOWN:
			case WM_NCMBUTTONUP:
			case WM_NCMBUTTONDBLCLK:
			case WM_MOUSEWHEEL:
			case WM_CONTEXTMENU:
			case WM_CANCELMODE:
			case WM_ACTIVATEAPP:
			case WM_SETTINGCHANGE:
			case WM_SYSCOLORCHANGE:
			case WM_LBUTTONUP:
				bStopFlag = true;
				break;
			case WM_CAPTURECHANGED:
				if( (HWND)msg.wParam != hWndOwn )
					bStopFlag = true;
				break;
			case WM_MOUSEMOVE:
				{
					PeekMessage(&msg, NULL, WM_MOUSEMOVE, WM_MOUSEMOVE, PM_REMOVE);
					for( ; PeekMessage(&msg, NULL, WM_MOUSEMOVE, WM_MOUSEMOVE, PM_NOREMOVE); )
						PeekMessage(&msg, NULL, WM_MOUSEMOVE, WM_MOUSEMOVE, PM_REMOVE);

					CPoint pt( (DWORD)msg.lParam );
					if( msg.hwnd != m_hWnd )
					{
						::ClientToScreen( msg.hwnd, &pt );
						::ScreenToClient( m_hWnd, &pt );
					}
					LONG nHitTest = ItemHitTest( pt );
					LONG nItemsCount = ItemGetCount();

					CPoint ptCursor;
					GetCursorPos( &ptCursor );
					CRect rcWindow;
					GetWindowRect( &rcWindow );

					bool bHorz = OrientationIsHorizontal();
					
					if( bHorz && ( ptCursor.x > rcWindow.right || ptCursor.x < rcWindow.left ) )
					{
						if( m_nScrollDirectionRest == 0 )
							OnTabWndDoScroll(
								(ptCursor.x < rcWindow.left) ? -1 : +1,
								true
								);
					}
					else if( !bHorz && ( ptCursor.y > rcWindow.bottom || ptCursor.y < rcWindow.top ) )
					{
						if( m_nScrollDirectionRest == 0 )
							OnTabWndDoScroll(
								(ptCursor.y < rcWindow.top) ? -1 : +1,
								true
								);
					}
					else
					{
						m_nScrollDirectionRest = 0;
						KillTimer( __EXTTAB_SCROLL_TIMER_ID );
						
						if(		nItemsCount != 0
							&&	nIndex >= 0
							&&	nIndex < nItemsCount
							&&	nIndex != nHitTest
							&&	nHitTest >= 0
							&&	nHitTest < nItemsCount
							&&	(!m_rcLastMovedItemRect.PtInRect( pt ) || abs(nIndex - nHitTest) > 1 )
							)
						{
							m_rcLastMovedItemRect = ItemGet( nHitTest )->ItemRectGet();
							LONG nSpaceBefore = 0, nSpaceAfter = 0, nSpaceOver = 0;
								OnTabWndMeasureItemAreaMargins(
									nSpaceBefore,
									nSpaceAfter,
									nSpaceOver
									);
							m_rcLastMovedItemRect.OffsetRect( 
								bHorz ? nSpaceBefore - m_nScrollPos : 0,
								bHorz ? 0 : nSpaceBefore - m_nScrollPos
								);

							// move item into the new position
							if( !ItemMove( nIndex, nHitTest, true ) )
								m_rcLastMovedItemRect = CRect(0,0,0,0);
							else nIndex = nHitTest;
						}
					}
					continue;
				}
				break;
			default:
				if(	CExtMouseCaptureSink::GetCapture() != GetSafeHwnd()	)
					bStopFlag = true;
				break;
			} // switch( msg.message )

			if( !::IsWindow(hWndOwn) )
			{
				bStopFlag = false;
				break;
			}

			if( !AfxGetThread()->PumpMessage() )
			{
				PostQuitMessage(0);
				break; // Signal WM_QUIT received
			}

			if( bStopFlag )
				break;

		} // while( ::IsWindow(hWndOwn) && PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
		
		if( bStopFlag )
			break;

		if( CExtTabWnd::g_bEnableOnIdleCalls )
		{
			for(	LONG nIdleCounter = 0L;
					::AfxGetThread()->OnIdle(nIdleCounter);
					nIdleCounter ++
					);
		}
	} // for( bool bStopFlag = false; (!bStopFlag) && ::IsWindow(hWndOwn) ; )

	if( ::IsWindow(hWndOwn) )
	{
		if( CExtMouseCaptureSink::GetCapture() == hWndOwn )
			CExtMouseCaptureSink::ReleaseCapture();
		m_nPushedTrackingButton = (-1L);
		m_nScrollDirectionRest = 0;
		m_rcLastMovedItemRect = CRect(0,0,0,0);
		KillTimer( __EXTTAB_SCROLL_TIMER_ID );
		UpdateTabWnd();
	} // if( ::IsWindow(hWndOwn) )	

	m_bDragging = false;

	return true;
}

bool CExtTabWnd::_ItemMoveImpl( // move item into the new position
	LONG nIndex,
	LONG nIndexNew
	)
{
	ASSERT_VALID( this );

	int nDirection = 0;
	nDirection = ( nIndex < nIndexNew ) ? +1 : -1;
	if( nDirection == 0 )
		return false;

TAB_ITEM_INFO *pTii		= ItemGet( nIndex );
CExtSafeString sText	= pTii->TextGet();
CExtCmdIcon * pIconTmp	= OnTabWndQueryItemIcon( pTii );
CExtCmdIcon _iconEmpty;
CExtCmdIcon & _icon = ( pIconTmp == NULL ) ? _iconEmpty : (*pIconTmp);
DWORD dwItemStyle		= pTii->GetItemStyle();
LPARAM lParam			= pTii->LParamGet();

	ItemEnsureVisible( nIndexNew, false );

	ItemInsert(
		sText,
		_icon,
		dwItemStyle,
		(nDirection > 0) ? nIndexNew + 1 : nIndexNew,
		lParam,
		false
		);
	ItemRemove( 
		(nDirection > 0) ? nIndex  : nIndex + 1, 
		1, 
		false 
		);
	SelectionSet( nIndexNew, false, false );
	
	return true;
}

bool CExtTabWnd::ItemMove(
	LONG nIndex,
	LONG nIndexNew,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );

	LONG nItemsCount = ItemGetCount();
	if(!(	nItemsCount != 0
		&&	nIndex >= 0
		&&	nIndex < nItemsCount
		&&	nIndexNew >= 0
		&&	nIndexNew < nItemsCount
		&&	nIndexNew != nIndex
		)
		)
		return false;

	if( !OnTabWndItemPosChanging( nIndex, nIndexNew ) )
		return false;
	
	_ItemMoveImpl(
		nIndex,
		nIndexNew
		);

	UpdateTabWnd( bUpdateTabWnd );
	OnTabWndItemPosChanged( nIndex, nIndexNew );
	return true;
}

bool CExtTabWnd::OnTabWndItemPosChanging(
	LONG nItemIndex,
	LONG nItemNewIndex
	)
{
	ASSERT_VALID( this );
	nItemIndex;
	nItemNewIndex;
	return true;
}

void CExtTabWnd::OnTabWndItemPosChanged(
	LONG nItemIndex,
	LONG nItemNewIndex
	)
{
	ASSERT_VALID( this );
	nItemIndex;
	nItemNewIndex;
}
 
#if (!defined __EXT_MFC_NO_TABMDI_CTRL )

/////////////////////////////////////////////////////////////////////////////
// CExtTabMdiWnd window

IMPLEMENT_DYNCREATE( CExtTabMdiWnd, CExtTabWnd );

CExtTabMdiWnd::CExtTabMdiWnd()
{
}

CExtTabMdiWnd::~CExtTabMdiWnd()
{
}

BEGIN_MESSAGE_MAP(CExtTabMdiWnd, CExtTabWnd)
	//{{AFX_MSG_MAP(CExtTabMdiWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG

void CExtTabMdiWnd::AssertValid() const
{
	CExtTabWnd::AssertValid();
}

void CExtTabMdiWnd::Dump(CDumpContext& dc) const
{
	CExtTabWnd::Dump( dc );
}

#endif

#endif // (!defined __EXT_MFC_NO_TABMDI_CTRL )


/////////////////////////////////////////////////////////////////////////////
// CExtTabOneNoteWnd

#ifndef __EXT_MFC_NO_TAB_ONENOTE_CTRL

IMPLEMENT_DYNCREATE( CExtTabOneNoteWnd, CExtTabWnd );

#define __EXTTAB_ONENOTE_INDENT_TOP		1
#define __EXTTAB_ONENOTE_INDENT_BOTTOM	1
#define __EXTTAB_ONENOTE_INDENT_LEFT	2
#define __EXTTAB_ONENOTE_INDENT_RIGHT	1

CExtTabOneNoteWnd::CExtTabOneNoteWnd()
	: m_nNextColorIndex( 0 )
{
}

CExtTabOneNoteWnd::~CExtTabOneNoteWnd()
{
}

bool CExtTabOneNoteWnd::_IsCustomLayoutTabWnd() const
{
	return true;
}

BOOL CExtTabOneNoteWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( IDC_STATIC )
	DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS
	DWORD dwTabStyle, // = __ETWS_ONENOTE_DEFAULT
	CCreateContext * pContext // = NULL
	)
{
	ASSERT( pParentWnd != NULL );
	ASSERT( pParentWnd->GetSafeHwnd() != NULL );
	ASSERT( ::IsWindow(pParentWnd->GetSafeHwnd()) );
	
	if(	! CExtTabWnd::Create(
			pParentWnd,
			rcWnd,
			nDlgCtrlID,
			dwWindowStyle,
			dwTabStyle,
			pContext
			)
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	return TRUE;
}

void CExtTabOneNoteWnd::PreSubclassWindow() 
{
	CExtTabWnd::PreSubclassWindow();

	if( m_bDirectCreateCall )
		return;

	ModifyTabWndStyle( 
		0xFFFFFFFF, 
		__ETWS_ONENOTE_DEFAULT
		);
}

CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::ItemInsert(
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true
	DWORD dwItemStyle, // = 0 // visible & non-group
	LONG nIndex, // = -1 // default - append
	LPARAM lParam, // = 0
	COLORREF clrBkLight, // = (COLORREF)(-1L),
	COLORREF clrBkDark, // = (COLORREF)(-1L),
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
CExtCmdIcon _icon;
	if( hIcon != NULL )
		_icon.AssignFromHICON( hIcon, bCopyIcon );
	return
		ItemInsert(
			sText,
			_icon,
			dwItemStyle,
			nIndex,
			lParam,
			clrBkLight,
			clrBkDark,
			bUpdateTabWnd
			);
}

CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::ItemInsert(
	__EXT_MFC_SAFE_LPCTSTR sText,
	const CExtCmdIcon & _icon,
	DWORD dwItemStyle, // = 0 // visible & non-group
	LONG nIndex, // = -1 // default - append
	LPARAM lParam, // = 0
	COLORREF clrBkLight, // = (COLORREF)(-1L),
	COLORREF clrBkDark, // = (COLORREF)(-1L),
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if( nIndex < 0 || nIndex > nCount )
		nIndex = nCount;

TAB_ITEM_INFO * pTiiPrev = NULL;
	if( nIndex > 0 )
	{
		pTiiPrev = ItemGet( nIndex - 1 );
		ASSERT_VALID( pTiiPrev );
	}

TAB_ITEM_INFO * pTiiNext = NULL;
	if( nIndex < nCount )
	{
		pTiiNext = ItemGet( nIndex );
		ASSERT_VALID( pTiiNext );
	}

	if(		clrBkLight == (COLORREF)(-1L)
		||	clrBkDark == (COLORREF)(-1L)
		)
	{
		GenerateItemBkColors(
			m_nNextColorIndex++,
			&clrBkLight,
			&clrBkDark
		);
		ASSERT( clrBkLight != (COLORREF)(-1L) 
			&&	clrBkDark != (COLORREF)(-1L) 
			);
	}

TAB_ITEM_INFO_ONENOTE * pTiiON =
		new TAB_ITEM_INFO_ONENOTE(
			this,
			pTiiPrev,
			pTiiNext,
			sText,
			&_icon,
			dwItemStyle,
			lParam,
			clrBkLight,
			clrBkDark
			);
TAB_ITEM_INFO * pTii = static_cast < TAB_ITEM_INFO * > ( pTiiON );

	m_arrItems.InsertAt( nIndex, pTii );
	ASSERT_VALID( pTii );
	if( m_nSelIndex >= nIndex ) 
		m_nSelIndex++;

	if( pTii->VisibleGet() )
		m_nVisibleItemCount ++;

	ASSERT_VALID( this );

	OnTabWndItemInsert(
		nIndex,
		pTii
		);
	
	UpdateTabWnd( bUpdateTabWnd );
	return pTiiON;
}

bool CExtTabOneNoteWnd::_ItemMoveImpl( // move item into the new position
	LONG nIndex,
	LONG nIndexNew
	)
{
	ASSERT_VALID( this );

	int nDirection = 0;
	nDirection = ( nIndex < nIndexNew ) ? +1 : -1;
	if( nDirection == 0 )
		return false;

TAB_ITEM_INFO_ONENOTE *pTii = 
		static_cast < TAB_ITEM_INFO_ONENOTE * > ( ItemGet( nIndex ) );
CExtSafeString sText		= pTii->TextGet();
CExtCmdIcon * pIconTmp	= OnTabWndQueryItemIcon( pTii );
CExtCmdIcon _iconEmpty;
CExtCmdIcon & _icon = ( pIconTmp == NULL ) ? _iconEmpty : (*pIconTmp);
DWORD dwItemStyle	= pTii->GetItemStyle();
LPARAM lParam		= pTii->LParamGet();
COLORREF clrBkLight = pTii->GetColorBkLight();
COLORREF clrBkDark  = pTii->GetColorBkDark();

	ItemEnsureVisible( nIndexNew, false );

	ItemInsert(
		sText,
		_icon,
		dwItemStyle,
		(nDirection > 0) ? nIndexNew + 1 : nIndexNew,
		lParam,
		clrBkLight,
		clrBkDark,
		false
		);
	ItemRemove( 
		(nDirection > 0) ? nIndex  : nIndex + 1, 
		1, 
		false 
		);
	SelectionSet( nIndexNew, false, false );

	return true;
}

CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::ItemGet( LONG nIndex )
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if( nIndex < 0 || nIndex >= nCount )
	{
		ASSERT( FALSE );
		return NULL;
	}
TAB_ITEM_INFO_ONENOTE * pTii = 
		static_cast
			< TAB_ITEM_INFO_ONENOTE * >
			( m_arrItems[ nIndex ] );
	ASSERT_VALID( pTii );
	ASSERT( pTii->GetTabWnd() == this );
	return pTii;
}

const CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::ItemGet( LONG nIndex ) const
{
	return
		( const_cast
			< CExtTabOneNoteWnd * >
			( this )
		) -> ItemGet( nIndex );
}

const CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::SelectionGetPtr() const
{
	ASSERT_VALID( this );
	if( m_nSelIndex < 0 || ItemGetCount() == 0 )
		return NULL;
	return ItemGet( m_nSelIndex );
}

CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * CExtTabOneNoteWnd::SelectionGetPtr()
{
	ASSERT_VALID( this );
	LONG nCount = ItemGetCount();
	if( m_nSelIndex < 0 || nCount == 0 )
		return NULL;
	return ItemGet( m_nSelIndex );
}	

bool CExtTabOneNoteWnd::OnTabWndQueryHoverChangingRedraw() const
{
	ASSERT_VALID( this );
	return true;
}

void CExtTabOneNoteWnd::OnTabWndMeasureItemAreaMargins(
	LONG & nSpaceBefore,
	LONG & nSpaceAfter,
	LONG & nSpaceOver
	)
{
	ASSERT_VALID( this );
	nSpaceBefore	= 1;
	nSpaceAfter		= 2;
	nSpaceOver		= 3;
}

void CExtTabOneNoteWnd::OnTabWndUpdateItemMeasure(
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	CDC & dcMeasure,
	CSize & sizePreCalc
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	ASSERT( pTii->GetTabWnd() == this );
	pTii;
	dcMeasure;
bool bHorz = OrientationIsHorizontal();
	if( bHorz )
	{
		sizePreCalc.cx += (__EXTTAB_ONENOTE_INDENT_LEFT + __EXTTAB_ONENOTE_INDENT_RIGHT);
		if( pTii->GetIndexOf() == 0 )
			sizePreCalc.cx += 16;
		sizePreCalc.cy = 19;
	}
	else
	{
		sizePreCalc.cy += (__EXTTAB_ONENOTE_INDENT_LEFT + __EXTTAB_ONENOTE_INDENT_RIGHT);
		if( pTii->GetIndexOf() == 0 )
			sizePreCalc.cy += 16;
		sizePreCalc.cx = 19;
	}
}

void CExtTabOneNoteWnd::OnTabWndEraseClientArea(
	CDC & dc,
	CRect & rcClient,
	CRect & rcTabItemsArea,
	CRect & rcTabNearBorderArea,
	DWORD dwOrientation,
	bool bGroupedMode
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	rcTabItemsArea;
	bGroupedMode;

	if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )
	{
		bool bHorz = OrientationIsHorizontal();
		bool bTopLeft = OrientationIsTopLeft();
		COLORREF clrDark = PmBridge_GetPM()->GetColor( COLOR_3DLIGHT, this );
		COLORREF clrLight = PmBridge_GetPM()->GetColor( COLOR_3DHILIGHT, this );
		PmBridge_GetPM()->stat_PaintGradientRect(
			dc,
			&rcClient,
			bHorz 
				? ( bTopLeft ? clrLight : clrDark )
				: ( bTopLeft ? clrDark : clrLight ),
			bHorz
				? ( bTopLeft ? clrDark : clrLight )
				: ( bTopLeft ? clrLight : clrDark ),
			bHorz
		);
	} // if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )
	else 
		dc.FillSolidRect( 
			&rcClient, 
			PmBridge_GetPM()->GetColor( COLOR_3DFACE, this ) 
			);
	if( ! rcTabNearBorderArea.IsRectEmpty() )
	{
		CRect rcTabNearMargin( rcTabNearBorderArea ); // prepare tab border margin rect
		CRect rcColorLine( rcClient );

		switch( dwOrientation )
		{
		case __ETWS_ORIENT_TOP:
			rcTabNearMargin.bottom = rcTabNearMargin.top + 1;
			rcTabNearMargin.OffsetRect(0,-1);
			rcColorLine.top = rcTabNearMargin.bottom;
		break;
		case __ETWS_ORIENT_BOTTOM:
			rcTabNearMargin.top = rcTabNearMargin.bottom - 1;
			rcTabNearMargin.OffsetRect(0,1);
			rcColorLine.bottom = rcTabNearMargin.top; 
		break;
		case __ETWS_ORIENT_LEFT:
			rcTabNearMargin.right = rcTabNearMargin.left + 1;
			rcTabNearMargin.OffsetRect(-1,0);
			rcColorLine.left = rcTabNearMargin.right;
		break;
		case __ETWS_ORIENT_RIGHT:
			rcTabNearMargin.left = rcTabNearMargin.right - 1;
			rcTabNearMargin.OffsetRect(1,0);
			rcColorLine.right = rcTabNearMargin.left;
		break;
		default:
			ASSERT( FALSE );
		break;
		} // switch( dwOrientation )

		// paint tab border margin
		dc.FillSolidRect(
			&rcTabNearMargin,
			PmBridge_GetPM()->GetColor( COLOR_3DSHADOW, this )
			);

		if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )
		{
			COLORREF clrBkDark = COLORREF( -1L );
			LONG nSelIndex = SelectionGet();
			if( nSelIndex >= 0L )
			{
				OnTabWndQueryItemColors(
					nSelIndex,
					false,
					false,
					ItemEnabledGet( nSelIndex ),
					NULL,
					NULL,
					NULL,
					&clrBkDark,
					NULL
					);
			}
			dc.FillSolidRect( 
				&rcColorLine, 
				clrBkDark != COLORREF( -1L )
					? clrBkDark
					: PmBridge_GetPM()->GetColor( COLOR_3DFACE, this )
				);
		}
		else 
		{
			dc.FillSolidRect( 
				&rcColorLine, 
				PmBridge_GetPM()->GetColor(
					SelectionGet() >= 0
						? COLOR_WINDOW
						: COLOR_3DFACE
						,
					this
					)
				);
		}
	} // if( ! rcTabNearBorderArea.IsRectEmpty() )
}

void CExtTabOneNoteWnd::GenerateItemBkColors(
	LONG nIndex,
	COLORREF * pclrBkLight, // = NULL,
	COLORREF * pclrBkDark // = NULL
	)
{
	ASSERT_VALID( this );
COLORREF clrBkLight = (COLORREF)(-1L);
COLORREF clrBkDark = (COLORREF)(-1L);
	if( nIndex >= 0 )
	{
		int nIndexBase = nIndex % 7;
		switch( nIndexBase ) 
		{
		case 0:
			clrBkDark = RGB(138, 168, 228);
			clrBkLight = RGB(221, 230, 247);
			break;
		case 1:
			clrBkDark = RGB(255, 216, 105);
			clrBkLight = RGB(255, 244, 213);
			break;
		case 2:
			clrBkDark = RGB(183, 201, 151);
			clrBkLight = RGB(234, 240, 226);
			break;
		case 3:
			clrBkDark = RGB(238, 149, 151);
			clrBkLight = RGB(249, 225, 226);
			break;
		case 4:
			clrBkDark = RGB(180, 158, 222);
			clrBkLight = RGB(234, 227, 245);
			break;
		case 5:
			clrBkDark = RGB(145, 186, 174);
			clrBkLight = RGB(224, 236, 232);
			break;
		case 6:
			clrBkDark = RGB(246, 176, 120);
			clrBkLight = RGB(252, 233, 217);
			break;
		case 7:
			clrBkDark = RGB(213, 164, 187);
			clrBkLight = RGB(243, 229, 236);
			break;
#ifdef _DEBUG
		default:
			{
				ASSERT( FALSE );
			}
			break;
#endif // _DEBUG
		} // switch( nIndexBase )
	} // if( nIndex >= 0 )
	else
	{
		clrBkDark =
			clrBkLight = 
			PmBridge_GetPM()->GetColor( COLOR_3DFACE, this );
	}
	if( pclrBkLight != NULL )
		*pclrBkLight = clrBkLight;
	if( pclrBkDark != NULL )
		*pclrBkDark = clrBkDark;
}

void CExtTabOneNoteWnd::OnTabWndQueryItemColors(
	LONG nItemIndex,
	bool bSelected,
	bool bHover,
	bool bEnabled,
	COLORREF * pclrBorderLight, // = NULL,
	COLORREF * pclrBorderDark, // = NULL,
	COLORREF * pclrBkLight, // = NULL,
	COLORREF * pclrBkDark, // = NULL,
	COLORREF * pclrText // = NULL
	)
{
	ASSERT_VALID( this );

COLORREF clrBorderLight = (COLORREF)(-1L);
COLORREF clrBorderDark = (COLORREF)(-1L);
COLORREF clrBkLight = (COLORREF)(-1L);
COLORREF clrBkDark = (COLORREF)(-1L);
COLORREF clrText = (COLORREF)(-1L);

	PmBridge_GetPM()->GetTabOneNoteItemColors(
		bSelected,
		bHover,
		bEnabled,
		clrBorderLight,
		clrBorderDark,
		clrBkLight,
		clrBkDark,
		clrText
		);

	if(		( (!bHover) || (!bEnabled) ) 
		&&	nItemIndex >= 0 
		)
	{
		TAB_ITEM_INFO_ONENOTE * pTii = 
			static_cast
				< TAB_ITEM_INFO_ONENOTE * >
				( ItemGet( nItemIndex ) );
		clrBkLight = pTii->GetColorBkLight();
		clrBkDark  = pTii->GetColorBkDark();
	}

	if( pclrBorderLight != NULL )
		*pclrBorderLight = clrBorderLight;
	if( pclrBorderDark != NULL )
		*pclrBorderDark = clrBorderDark;
	if( pclrBkLight != NULL )
		*pclrBkLight = clrBkLight;
	if( pclrBkDark != NULL )
		*pclrBkDark = clrBkDark;
	if( pclrText != NULL )
		*pclrText = clrText;
}

void CExtTabOneNoteWnd::OnTabWndDrawItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	LONG nItemIndex,
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );
	rcTabItemsArea;

CExtCmdIcon * pIconTabItemCloseButton =
		OnTabWndQueryItemCloseButtonShape( pTii );
CExtCmdIcon::e_paint_type_t ePaintStateITICB =
		(CExtCmdIcon::e_paint_type_t)
			OnTabWndQueryItemCloseButtonPaintState( pTii );
CRect rcTabItemCloseButton( 0, 0, 0, 0 );
	if( pIconTabItemCloseButton != NULL )
		rcTabItemCloseButton = pTii->CloseButtonRectGet();

bool bEnabled = pTii->EnabledGet();
bool bHover = (m_nHoverTrackingHitTest == nItemIndex) ? true : false;

	if( (pTii->GetItemStyle() & __ETWI_CENTERED_TEXT) != 0 )
		bCenteredText = true;

bool bInGroupFirst = false;
	if( bGroupedMode )
	{
		TAB_ITEM_INFO * pTiiStart = pTii->GetInGroupFirst();
		ASSERT_VALID( pTiiStart );
		bInGroupFirst = ( nItemIndex == ItemGetIndexOf( pTiiStart ) );
	}
bool bFirstItem =
		(	nItemIndex == 0
		||	(	bInGroupFirst
			&&	nItemIndex != SelectionGet()
			)
		);

CRect rcItem( rcEntireItem );
CRect rcItemRgn( rcItem );
POINT arrPointsBorders[11] =
		{	{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 },
		};
POINT arrPointsInnerArea[10] = 
		{	{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 },
		};
POINT arrPointsClipArea[13] =
		{	{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 },
		};

	if( bHorz )
	{
		if( bTopLeft )
		{
			rcItemRgn.InflateRect( nItemIndex == 0 ? 0 : -1, 0, 1, 0 );

			arrPointsBorders[0] = CPoint( rcItemRgn.right,		rcItemRgn.bottom );
			arrPointsBorders[1] = CPoint( rcItemRgn.right,		rcItemRgn.top + 2 );
			arrPointsBorders[2] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top );
			arrPointsBorders[3] = CPoint( rcItemRgn.left + 5,	rcItemRgn.top );
			arrPointsBorders[4] = CPoint( rcItemRgn.left + 4,	rcItemRgn.top + 1 );
			arrPointsBorders[5] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top + 1 );
			arrPointsBorders[6] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 2 );
			arrPointsBorders[7] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top + 2 );
			arrPointsBorders[8] = CPoint( rcItemRgn.left,		rcItemRgn.top + 3 );
			if( bSelected || bFirstItem )
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.left - 15, rcItemRgn.bottom - 1 );
				arrPointsBorders[10] = CPoint( rcItemRgn.left - 16, rcItemRgn.bottom - 1 );
			}
			else
			{
				arrPointsBorders[9]= CPoint( rcItemRgn.left, rcItemRgn.bottom - 1 );
				arrPointsBorders[10]= arrPointsBorders[9];
			}
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].x += 16;
			
			arrPointsInnerArea[0] = CPoint( rcItemRgn.right - 1,	rcItemRgn.bottom );
			arrPointsInnerArea[1] = CPoint( rcItemRgn.right - 1,	rcItemRgn.top + 2 );
			arrPointsInnerArea[2] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 1 );
			arrPointsInnerArea[3] = CPoint( rcItemRgn.left + 5,		rcItemRgn.top + 1 );
			arrPointsInnerArea[4] = CPoint( rcItemRgn.left + 4,		rcItemRgn.top + 2 );
			arrPointsInnerArea[5] = CPoint( rcItemRgn.left + 3,		rcItemRgn.top + 2 );
			arrPointsInnerArea[6] = CPoint( rcItemRgn.left + 2,		rcItemRgn.top + 3 );
			arrPointsInnerArea[7] = CPoint( rcItemRgn.left + 1,		rcItemRgn.top + 3 );
			arrPointsInnerArea[8] = CPoint( rcItemRgn.left,			rcItemRgn.top + 4 );
			if( bSelected || bFirstItem )
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left - 14, rcItemRgn.bottom - 1 );
			else
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left, rcItemRgn.bottom - 1 );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsInnerArea)/sizeof(arrPointsInnerArea[0]); i++ )
					arrPointsInnerArea[i].x += 16;

			arrPointsClipArea[0] = CPoint( rcItemRgn.right - 2,		rcItemRgn.bottom );
			arrPointsClipArea[1] = CPoint( rcItemRgn.right - 2,		rcItemRgn.top + 2 );
			arrPointsClipArea[2] = CPoint( rcItemRgn.left + 5,		rcItemRgn.top + 2 );
			arrPointsClipArea[3] = CPoint( rcItemRgn.left + 4,		rcItemRgn.top + 3 );
			arrPointsClipArea[4] = CPoint( rcItemRgn.left + 3,		rcItemRgn.top + 3 );
			arrPointsClipArea[5] = CPoint( rcItemRgn.left + 2,		rcItemRgn.top + 4 );
			arrPointsClipArea[6] = CPoint( rcItemRgn.left + 1,		rcItemRgn.top + 4 );
			arrPointsClipArea[7] = CPoint( rcItemRgn.left,			rcItemRgn.top + 5 );
			if( bSelected || bFirstItem )
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left - 14,	rcItemRgn.bottom );
				arrPointsClipArea[9] = CPoint( rcItemRgn.left - 16,	rcItemRgn.bottom );
				arrPointsClipArea[10]= CPoint( rcItemRgn.left - 16,	rcItemRgn.bottom );
				arrPointsClipArea[11]= CPoint( rcItemRgn.right,		rcItemRgn.bottom );
				if( bSelected )
				{
					arrPointsClipArea[10].y++;
					arrPointsClipArea[11].y++;
				}
			}
			else
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left,	rcItemRgn.bottom );
				arrPointsClipArea[11]= CPoint( rcItemRgn.right - 2, rcItemRgn.bottom );
				arrPointsClipArea[10] = arrPointsClipArea[9] = arrPointsClipArea[8];
			}
			arrPointsClipArea[12] = CPoint( rcItemRgn.right, rcItemRgn.bottom - 1 );
			if( nItemIndex == 0 )
				for( int i = 2 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]) - 3; i++ )
					arrPointsClipArea[i].x += 16;
	
		} // if( bTopLeft )
		else
		{
			rcItemRgn.InflateRect( nItemIndex == 0 ? 0 : -1, 0, 0, 0 );

			arrPointsBorders[0] = CPoint( rcItemRgn.right,		rcItemRgn.top );
			arrPointsBorders[1] = CPoint( rcItemRgn.right,		rcItemRgn.bottom - 3 );
			arrPointsBorders[2] = CPoint( rcItemRgn.right - 3,	rcItemRgn.bottom );
			arrPointsBorders[3] = CPoint( rcItemRgn.left + 5,	rcItemRgn.bottom );
			arrPointsBorders[4] = CPoint( rcItemRgn.left + 4,	rcItemRgn.bottom - 1 );
			arrPointsBorders[5] = CPoint( rcItemRgn.left + 3,	rcItemRgn.bottom - 1 );
			arrPointsBorders[6] = CPoint( rcItemRgn.left + 2,	rcItemRgn.bottom - 2 );
			arrPointsBorders[7] = CPoint( rcItemRgn.left + 1,	rcItemRgn.bottom - 2 );
			arrPointsBorders[8] = CPoint( rcItemRgn.left - 1,	rcItemRgn.bottom - 4 );
			if( bSelected || bFirstItem )
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.left - 16, rcItemRgn.top + 1 );
				arrPointsBorders[10] = CPoint( rcItemRgn.left - 17, rcItemRgn.top + 1 );
			}
			else
			{
				arrPointsBorders[9]= CPoint( rcItemRgn.left - 1, rcItemRgn.top + 1 );
				arrPointsBorders[10]= arrPointsBorders[9];
			}
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].x += 16;
			
			arrPointsInnerArea[0] = CPoint( rcItemRgn.right - 1,	rcItemRgn.top );
			arrPointsInnerArea[1] = CPoint( rcItemRgn.right - 1,	rcItemRgn.bottom - 3 );
			arrPointsInnerArea[2] = CPoint( rcItemRgn.right - 3,	rcItemRgn.bottom - 1 );
			arrPointsInnerArea[3] = CPoint( rcItemRgn.left + 5,		rcItemRgn.bottom - 1 );
			arrPointsInnerArea[4] = CPoint( rcItemRgn.left + 4,		rcItemRgn.bottom - 2 );
			arrPointsInnerArea[5] = CPoint( rcItemRgn.left + 3,		rcItemRgn.bottom - 2 );
			arrPointsInnerArea[6] = CPoint( rcItemRgn.left + 2,		rcItemRgn.bottom - 3 );
			arrPointsInnerArea[7] = CPoint( rcItemRgn.left + 1,		rcItemRgn.bottom - 3 );
			arrPointsInnerArea[8] = CPoint( rcItemRgn.left - 1,		rcItemRgn.bottom - 5 );
			if( bSelected || bFirstItem )
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left - 14, rcItemRgn.top + 1 );
			else
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left - 1, rcItemRgn.top + 1 );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsInnerArea)/sizeof(arrPointsInnerArea[0]); i++ )
					arrPointsInnerArea[i].x += 16;

			arrPointsClipArea[0] = CPoint( rcItemRgn.right - 2,		rcItemRgn.top );
			arrPointsClipArea[1] = CPoint( rcItemRgn.right - 2,		rcItemRgn.bottom - 2 );
			arrPointsClipArea[2] = CPoint( rcItemRgn.left + 5,		rcItemRgn.bottom - 2 );
			arrPointsClipArea[3] = CPoint( rcItemRgn.left + 4,		rcItemRgn.bottom - 3 );
			arrPointsClipArea[4] = CPoint( rcItemRgn.left + 3,		rcItemRgn.bottom - 3 );
			arrPointsClipArea[5] = CPoint( rcItemRgn.left + 2,		rcItemRgn.bottom - 4 );
			arrPointsClipArea[6] = CPoint( rcItemRgn.left + 1,		rcItemRgn.bottom - 4 );
			arrPointsClipArea[7] = CPoint( rcItemRgn.left - 1,		rcItemRgn.bottom - 6 );
			if( bSelected || bFirstItem )
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left - 15,	rcItemRgn.top );
				arrPointsClipArea[9] = CPoint( rcItemRgn.left - 16,	rcItemRgn.top + 1 );
				arrPointsClipArea[10]= CPoint( rcItemRgn.left - 16,	rcItemRgn.top + 1 );
				arrPointsClipArea[11]= CPoint( rcItemRgn.right,		rcItemRgn.top + 1 );
				if( bSelected )
				{
					arrPointsClipArea[10].y -= 1;
					arrPointsClipArea[11].y -= 1;
				}
			}
			else
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left - 1, rcItemRgn.top + 1 );
				arrPointsClipArea[11]= CPoint( rcItemRgn.right, rcItemRgn.top + 1 );
				arrPointsClipArea[10] = arrPointsClipArea[9] = arrPointsClipArea[8];
			}
			arrPointsClipArea[12] = CPoint( rcItemRgn.right, rcItemRgn.top + 1 );
			if( nItemIndex == 0 )
				for( int i = 2 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]) - 3; i++ )
					arrPointsClipArea[i].x += 16;

		} // else if( bTopLeft )
	} // if( bHorz )
	else
	{
		if( bTopLeft )
		{
			rcItemRgn.InflateRect( 0, nItemIndex == 0 ? 0 : -1, 0, 1 );

			arrPointsBorders[0] = CPoint( rcItemRgn.right,		rcItemRgn.bottom );
			arrPointsBorders[1] = CPoint( rcItemRgn.left + 3,	rcItemRgn.bottom );
			arrPointsBorders[2] = CPoint( rcItemRgn.left,		rcItemRgn.bottom - 3 );
			arrPointsBorders[3] = CPoint( rcItemRgn.left,		rcItemRgn.top + 5 );
			arrPointsBorders[4] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top + 4 );
			arrPointsBorders[5] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top + 3 );
			arrPointsBorders[6] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 2 );
			arrPointsBorders[7] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 1 );
			arrPointsBorders[8] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top );
			if( bSelected || bFirstItem )
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.right - 1, rcItemRgn.top - 16 );
				arrPointsBorders[10] = CPoint( rcItemRgn.right - 1, rcItemRgn.top - 17 );
			}
			else
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.left + 3, rcItemRgn.top );
				arrPointsBorders[10]= CPoint( rcItemRgn.right, rcItemRgn.top );
			}
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].y += 16;
			
			arrPointsInnerArea[0] = CPoint( rcItemRgn.right,	rcItemRgn.bottom - 1 );
			arrPointsInnerArea[1] = CPoint( rcItemRgn.left + 3,	rcItemRgn.bottom - 1 );
			arrPointsInnerArea[2] = CPoint( rcItemRgn.left + 1,	rcItemRgn.bottom - 3 );
			arrPointsInnerArea[3] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top + 5 );
			arrPointsInnerArea[4] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 4 );
			arrPointsInnerArea[5] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 3);
			arrPointsInnerArea[6] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top + 2 );
			arrPointsInnerArea[7] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top + 1 );
			arrPointsInnerArea[8] = CPoint( rcItemRgn.left + 4,	rcItemRgn.top );
			if( bSelected || bFirstItem )
				arrPointsInnerArea[9] = CPoint( rcItemRgn.right, rcItemRgn.top - 15 );
			else
				arrPointsInnerArea[9] = CPoint( rcItemRgn.right, rcItemRgn.top );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsInnerArea)/sizeof(arrPointsInnerArea[0]); i++ )
					arrPointsInnerArea[i].y += 16;

			arrPointsClipArea[0] = CPoint( rcItemRgn.right,		rcItemRgn.bottom - 2 );
			arrPointsClipArea[1] = CPoint( rcItemRgn.left + 3,	rcItemRgn.bottom - 2 );
			arrPointsClipArea[2] = CPoint( rcItemRgn.left + 2,	rcItemRgn.bottom - 3 );
			arrPointsClipArea[3] = CPoint( rcItemRgn.left + 2,	rcItemRgn.top + 5 );
			arrPointsClipArea[4] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top + 4 );
			arrPointsClipArea[5] = CPoint( rcItemRgn.left + 3,	rcItemRgn.top + 3);
			arrPointsClipArea[6] = CPoint( rcItemRgn.left + 4,	rcItemRgn.top + 2 );
			arrPointsClipArea[7] = CPoint( rcItemRgn.left + 4,	rcItemRgn.top + 1 );
			if( bSelected || bFirstItem )
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.right,		rcItemRgn.top - 15 );
				arrPointsClipArea[9] = CPoint( rcItemRgn.right,		rcItemRgn.top - 16 );
				arrPointsClipArea[10]= CPoint( rcItemRgn.right,		rcItemRgn.top - 16 );
				arrPointsClipArea[11]= CPoint( rcItemRgn.right - 1,	rcItemRgn.bottom );
				if( bSelected )
				{
					arrPointsClipArea[10].x += 1;
					arrPointsClipArea[11].x += 1;
				}
	  			arrPointsClipArea[12]= arrPointsClipArea[11];
			}
			else
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left + 5,	rcItemRgn.top );
				arrPointsClipArea[9] = CPoint( rcItemRgn.right,		rcItemRgn.top );
				arrPointsClipArea[10]= CPoint( rcItemRgn.right,		rcItemRgn.bottom );
				arrPointsClipArea[12]= 
					arrPointsClipArea[11] = 
					arrPointsClipArea[10];
			}
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]) - 3; i++ )
					arrPointsClipArea[i].y += 16;

		} // if( bTopLeft )
		else
		{
			//rcItemRgn.InflateRect( 0, nItemIndex == 0 ? 0 : -1, 0, 0 );

			arrPointsBorders[0] = CPoint( rcItemRgn.left,		rcItemRgn.bottom );
			arrPointsBorders[1] = CPoint( rcItemRgn.right - 3,	rcItemRgn.bottom );
			arrPointsBorders[2] = CPoint( rcItemRgn.right,		rcItemRgn.bottom - 3 );
			arrPointsBorders[3] = CPoint( rcItemRgn.right,		rcItemRgn.top + 5 );
			arrPointsBorders[4] = CPoint( rcItemRgn.right - 1,	rcItemRgn.top + 4 );
			arrPointsBorders[5] = CPoint( rcItemRgn.right - 1,	rcItemRgn.top + 3 );
			arrPointsBorders[6] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 2 );
			arrPointsBorders[7] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 1 );
			arrPointsBorders[8] = CPoint( rcItemRgn.right - 3,	rcItemRgn.top );
			if( bSelected || bFirstItem )
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top - 16 );
				arrPointsBorders[10]= CPoint( rcItemRgn.left + 1,	rcItemRgn.top - 17 );
			}
			else
			{
				arrPointsBorders[9] = CPoint( rcItemRgn.right - 3, rcItemRgn.top );
				arrPointsBorders[10]= CPoint( rcItemRgn.left, rcItemRgn.top );
			}
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].y += 16;

			arrPointsInnerArea[0] = CPoint( rcItemRgn.left,			rcItemRgn.bottom - 1 );
			arrPointsInnerArea[1] = CPoint( rcItemRgn.right - 3,	rcItemRgn.bottom - 1 );
			arrPointsInnerArea[2] = CPoint( rcItemRgn.right - 1,	rcItemRgn.bottom - 3 );
			arrPointsInnerArea[3] = CPoint( rcItemRgn.right - 1,	rcItemRgn.top + 5 );
			arrPointsInnerArea[4] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 4 );
			arrPointsInnerArea[5] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 3);
			arrPointsInnerArea[6] = CPoint( rcItemRgn.right - 3,	rcItemRgn.top + 2 );
			arrPointsInnerArea[7] = CPoint( rcItemRgn.right - 3,	rcItemRgn.top + 1 );
			arrPointsInnerArea[8] = CPoint( rcItemRgn.right - 4,	rcItemRgn.top );
			if( bSelected || bFirstItem )
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left + 1, rcItemRgn.top - 15 );
			else
				arrPointsInnerArea[9] = CPoint( rcItemRgn.left + 1, rcItemRgn.top );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsInnerArea)/sizeof(arrPointsInnerArea[0]); i++ )
					arrPointsInnerArea[i].y += 16;

			arrPointsClipArea[0] = CPoint( rcItemRgn.left + 1,	rcItemRgn.bottom - 2 );
			arrPointsClipArea[1] = CPoint( rcItemRgn.right - 3,	rcItemRgn.bottom - 2 );
			arrPointsClipArea[2] = CPoint( rcItemRgn.right - 2,	rcItemRgn.bottom - 3 );
			arrPointsClipArea[3] = CPoint( rcItemRgn.right - 2,	rcItemRgn.top + 5 );
			arrPointsClipArea[4] = CPoint( rcItemRgn.right - 3,	rcItemRgn.top + 4 );
			arrPointsClipArea[5] = CPoint( rcItemRgn.right - 3,	rcItemRgn.top + 3);
			arrPointsClipArea[6] = CPoint( rcItemRgn.right - 4,	rcItemRgn.top + 2 );
			arrPointsClipArea[7] = CPoint( rcItemRgn.right - 4,	rcItemRgn.top + 1 );

			if( bSelected || bFirstItem )
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top - 14 );
				arrPointsClipArea[9] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top - 15 );
				arrPointsClipArea[10]= CPoint( rcItemRgn.left + 1,	rcItemRgn.top - 15 );
				arrPointsClipArea[11]= CPoint( rcItemRgn.left + 1,	rcItemRgn.bottom );
				if( bSelected )
				{
					arrPointsClipArea[10].x -= 1;
					arrPointsClipArea[11].x -= 1;
				}
	  			arrPointsClipArea[12]= arrPointsClipArea[11];
			}
			else
			{
				arrPointsClipArea[8] = CPoint( rcItemRgn.right - 5,	rcItemRgn.top );
				arrPointsClipArea[9] = CPoint( rcItemRgn.left + 1,	rcItemRgn.top );
				arrPointsClipArea[10]= CPoint( rcItemRgn.left + 1,  rcItemRgn.bottom );
				arrPointsClipArea[12]= 
					arrPointsClipArea[11] = 
					arrPointsClipArea[10];
			}
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]) - 3; i++ )
					arrPointsClipArea[i].y += 16;

		} // else if( bTopLeft )

	} // else if( bHorz )

COLORREF clrBorderLight = (COLORREF)(-1L);
COLORREF clrBorderDark = (COLORREF)(-1L);
COLORREF clrBkLight = (COLORREF)(-1L);
COLORREF clrBkDark = (COLORREF)(-1L);
COLORREF clrText = (COLORREF)(-1L);

	OnTabWndQueryItemColors(
		nItemIndex,
		bSelected,
		bHover,
		bEnabled,
		&clrBorderLight,
		&clrBorderDark,
		&clrBkLight,
		&clrBkDark,
		&clrText
		);

// draw item border 
CBrush brushBorders( clrBorderDark );
CBrush brushBordersInnerArea( clrBorderLight );
CRgn rgnBorders, rgnBordersInnerArea, rgnClipArea;
	VERIFY( rgnBorders.CreatePolygonRgn( arrPointsBorders, 11, ALTERNATE ) );
	VERIFY( rgnBordersInnerArea.CreatePolygonRgn( arrPointsInnerArea, 10, ALTERNATE ) );
	VERIFY( rgnClipArea.CreatePolygonRgn( arrPointsClipArea, 13, ALTERNATE ) );
	dc.FrameRgn( &rgnBorders, &brushBorders, 1, 1 );
	dc.FrameRgn( &rgnBordersInnerArea, &brushBordersInnerArea, 1, 1 );

	// fill item background 
CRect rcFill( rcItemRgn );
	rcFill.InflateRect(
		bHorz ? 20 :  0,
		bHorz ?  0 : 20,
		bHorz ? 20 :  0,
		bHorz ?  0 : 20
		);
	if( bTopLeft )
		rcFill.InflateRect(
			0,
			0,
			bHorz ? 0 : 1,
			bHorz ? 1 : 0
			);

	dc.SelectClipRgn( &rgnClipArea, RGN_AND );
	if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )
	{
		CExtPaintManager::stat_PaintGradientRect(
			dc,
			&rcFill,
			bHorz 
				? ( bTopLeft ? clrBkDark : clrBkLight ) 
				: ( bTopLeft ? clrBkLight : clrBkDark ),
			bHorz 
				? ( bTopLeft ? clrBkLight : clrBkDark ) 
				: ( bTopLeft ? clrBkDark : clrBkLight ),
			bHorz
		);
	} // if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )
	else 
	{
		dc.FillSolidRect( 
			&rcFill, 
			bSelected 
				? PmBridge_GetPM()->GetColor( COLOR_WINDOW, this )
				: PmBridge_GetPM()->GetColor( COLOR_3DFACE, this )
			);
	} // else from if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )

	rcItem.DeflateRect(
		bHorz 
			? __EXTTAB_ONENOTE_INDENT_LEFT 
			: ( bTopLeft ? __EXTTAB_ONENOTE_INDENT_TOP * 2 : __EXTTAB_ONENOTE_INDENT_BOTTOM ),
		bHorz 
			? ( bTopLeft ? __EXTTAB_ONENOTE_INDENT_TOP : 0 ) 
			: __EXTTAB_ONENOTE_INDENT_LEFT,
		bHorz 
			? __EXTTAB_ONENOTE_INDENT_RIGHT 
			: 0,
		bHorz 
			? 0 
			: __EXTTAB_ONENOTE_INDENT_RIGHT
		);

	// first item indent
	if( nItemIndex == 0 )
		rcItem.DeflateRect(
			bHorz ? 16 : 0,
			(!bHorz) ? 16 : 0,
			0,
			0
			);

bool bDrawIcon =
		(	pIcon != NULL 
		&&	(!pIcon->IsEmpty()) 
		&&	(GetTabWndStyle()&__ETWS_HIDE_ICONS) == 0 
		) ? true : false;

	// if tab has no icons - increase left indent
	if( (!bDrawIcon) && (GetTabWndStyle()&__ETWS_CENTERED_TEXT) == 0 )
		rcItem.DeflateRect(
			bHorz ? 4 : 0,
			0,
			0,
			0
			);

CSize _sizeIcon( 0, 0 );
	if( bDrawIcon )
	{
		_sizeIcon = pIcon->GetSize();
		ASSERT( _sizeIcon.cx > 0 && _sizeIcon.cy > 0 );
	}
CRect rcItemForIcon( rcItem );
	if(		bDrawIcon
		&&	_sizeIcon.cx > 0
		&&	_sizeIcon.cy > 0
		)
	{
		rcItemForIcon.right = rcItemForIcon.left + _sizeIcon.cx;
		rcItemForIcon.bottom = rcItemForIcon.top + _sizeIcon.cy;
		rcItemForIcon.OffsetRect(
			bHorz ? 0 : ((rcItem.Width() - _sizeIcon.cx) / 2),
			bHorz ? ((rcItem.Height() - _sizeIcon.cy) / 2) : 0
			);
		if( rcItemForIcon.left < rcItem.left )
			rcItemForIcon.left = rcItem.left;
		if( rcItemForIcon.right > rcItem.right )
			rcItemForIcon.right = rcItem.right;
		if( rcItemForIcon.top < rcItem.top )
			rcItemForIcon.top = rcItem.top;
		if( rcItemForIcon.bottom > rcItem.bottom )
			rcItemForIcon.bottom = rcItem.bottom;
	}

	CExtSafeString sItemText( (sText == NULL) ? _T("") : sText );

CRect rcText(
		rcItem.left
			+	(	bHorz
					? (_sizeIcon.cx +
						((_sizeIcon.cx > 0) ? 2*__EXTTAB_MARGIN_ICON2TEXT_X : 0)
						)
					: 0
				),
		rcItem.top 
			+	(	bHorz
					? 0
					: (_sizeIcon.cy +
						((_sizeIcon.cy > 0) ? __EXTTAB_MARGIN_ICON2TEXT_Y : 0)
						)
				),
		rcItem.right,
		rcItem.bottom
		);
	if( !bHorz )
	{
		if( pIconTabItemCloseButton != NULL )
			rcText.bottom = min( rcText.bottom, rcTabItemCloseButton.top );
		int nWidth0 = rcText.Width();
		int nWidth1 = rcItem.Width() + __EXTTAB_MARGIN_ICON2TEXT_X*2;
		if( nWidth1 > nWidth0 )
		{
			if( bInvertedVerticalMode )
			{
				rcText.left = rcText.right - nWidth1;
			} // if( bInvertedVerticalMode )
			else
			{
				rcText.right = rcText.left + nWidth1;
			} // else from if( bInvertedVerticalMode )
		} // if( nWidth1 > nWidth0 )
	} // if( !bHorz )
	else
	{
		if( pIconTabItemCloseButton != NULL )
			rcText.right = min( rcText.right, rcTabItemCloseButton.left );
	}

CSize _sizeText = rcText.Size();
bool bDrawText = false;
	if(		( bHorz		&& _sizeText.cx >= ( max(16,_sizeIcon.cx) ) )
		||	( (!bHorz)	&& _sizeText.cy >= ( max(16,_sizeIcon.cy) ) )
		)
		bDrawText = true;

	if( (!bDrawText) && (! ( bGroupedMode && (!bInGroupActive)) ) )
	{
		rcItemForIcon.OffsetRect(
			bHorz ? (rcItem.Width() - _sizeIcon.cx) / 2 : 0,
			bHorz ? 0 : (rcItem.Height() - _sizeIcon.cy) / 2
			);
	}

	if( bDrawIcon )
	{
		if(		(bHorz && rcItemForIcon.Width() >= _sizeIcon.cx )
			||	(!bHorz && rcItemForIcon.Height() >= _sizeIcon.cy)
			)
		{
			CRect rcTmpText( 0, 0, 0, 0 );
			CExtSafeString sText( bDrawText ? sItemText : _T("") );
			PmBridge_GetPM()->PaintIcon(
				dc,
				bHorz,
				sText,
				pIcon,
				rcItemForIcon,
				rcTmpText,
				false,
				bEnabled,
				false,
				0
				);
		}
	}

	if( bDrawText )
	{ // if we have sense to paint text on tab item
		ASSERT( pFont != NULL );
		ASSERT( pFont->GetSafeHandle() != NULL );
		COLORREF clrOldText = dc.SetTextColor( clrText );
		INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
		CFont * pOldFont = dc.SelectObject( pFont );
		if( !bHorz )
		{
			if( bCenteredText )
			{
				UINT nOldTA = dc.SetTextAlign(
					TA_CENTER | TA_BASELINE
					);
				rcText.OffsetRect(
					bInvertedVerticalMode
						?   sizeTextMeasured.cy/2
						: - sizeTextMeasured.cy/2
						,
					0
					);
				CPoint ptCenter = rcText.CenterPoint();
				dc.ExtTextOut(
					ptCenter.x,
					ptCenter.y,
					ETO_CLIPPED,
					&rcText,
					sItemText,
					sItemText.GetLength(),
					NULL
					);
				dc.SetTextAlign( nOldTA );
			} // if( bCenteredText )
			else
			{
				UINT nOldTA = dc.SetTextAlign(
					TA_TOP | TA_BASELINE
					);
				rcText.OffsetRect(
					bInvertedVerticalMode
						?   sizeTextMeasured.cy/2
						: - sizeTextMeasured.cy/2
						,
					0
					);
				CPoint ptCenter = rcText.CenterPoint();
				if( bInvertedVerticalMode )
					ptCenter.y =
						rcText.bottom - 4
						- (rcText.Height() - sizeTextMeasured.cx)
						;
				else
					ptCenter.y =
						rcText.top + 4
						;
				dc.ExtTextOut(
					ptCenter.x,
					ptCenter.y,
					ETO_CLIPPED,
					&rcText,
					sItemText,
					sItemText.GetLength(),
					NULL
					);
				dc.SetTextAlign( nOldTA );
			} // else from if( bCenteredText )
		} // if( !bHorz )
		else
		{
			UINT nFormat =
				DT_SINGLELINE|DT_VCENTER|DT_END_ELLIPSIS;
			if( bCenteredText )
				nFormat |= DT_CENTER;
			else
				nFormat |= DT_LEFT;

			dc.DrawText(
				sItemText,
				sItemText.GetLength(),
				rcText,
				nFormat
				);
		}
		dc.SelectObject( pOldFont );
		dc.SetBkMode( nOldBkMode );
		dc.SetTextColor( clrOldText );
	} // if we have sense to paint text on tab item
	
	if( pIconTabItemCloseButton != NULL )
	{
		ASSERT( ! pIconTabItemCloseButton->IsEmpty() );
		if( dc.RectVisible( &rcTabItemCloseButton ) )
		{
			CRect _rcTabItemCloseButton = rcTabItemCloseButton;
			if( bHorz )
			{
				if( bTopLeft )
					_rcTabItemCloseButton.OffsetRect( 0, 1 );
			}
			else
			{
				if( bTopLeft )
					_rcTabItemCloseButton.OffsetRect( 1, 0 );
			}
			pIconTabItemCloseButton->Paint(
				g_PaintManager.GetPM(),
				dc.m_hDC,
				_rcTabItemCloseButton,
				ePaintStateITICB
				);
		}
	} // if( pIconTabItemCloseButton != NULL )

	dc.SelectClipRgn( NULL );
}

#if (!defined __EXT_MFC_NO_TABMDI_CTRL )

/////////////////////////////////////////////////////////////////////////////
// CExtTabMdiOneNoteWnd window

IMPLEMENT_DYNCREATE( CExtTabMdiOneNoteWnd, CExtTabOneNoteWnd );

CExtTabMdiOneNoteWnd::CExtTabMdiOneNoteWnd()
{
}

CExtTabMdiOneNoteWnd::~CExtTabMdiOneNoteWnd()
{
	ASSERT_VALID( this );
}

BEGIN_MESSAGE_MAP(CExtTabMdiOneNoteWnd, CExtTabOneNoteWnd)
	//{{AFX_MSG_MAP(CExtTabMdiOneNoteWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG

void CExtTabMdiOneNoteWnd::AssertValid() const
{
	CExtTabOneNoteWnd::AssertValid();
}

void CExtTabMdiOneNoteWnd::Dump(CDumpContext& dc) const
{
	CExtTabOneNoteWnd::Dump( dc );
}

#endif

void CExtTabMdiOneNoteWnd::OnMdiTabImplAdjustBorderSpaces()
{
	ASSERT_VALID( this );
DWORD dwWndStyle = GetStyle();
bool bInvisibleTab =
		( (dwWndStyle & WS_VISIBLE) == 0 ) ? true : false;
DWORD dwOrientation = OrientationGet();
	m_rcReservedBorderSpace.SetRect(
		(bInvisibleTab || dwOrientation == __ETWS_ORIENT_LEFT) ? 0 : __EXTTAB_MDI_NC_AREA_GAP_DX_L,
		(bInvisibleTab || dwOrientation == __ETWS_ORIENT_TOP) ? 0 : __EXTTAB_MDI_NC_AREA_GAP_DY_T,
		(bInvisibleTab || dwOrientation == __ETWS_ORIENT_RIGHT) ? 0 : __EXTTAB_MDI_NC_AREA_GAP_DX_R,
		(bInvisibleTab || dwOrientation == __ETWS_ORIENT_BOTTOM) ? 0 : __EXTTAB_MDI_NC_AREA_GAP_DY_B
		);
}

void CExtTabMdiOneNoteWnd::OnMdiTabImplDrawOuterBorder(
	CDC & dc,
	const CRect & rcOuterBorder,
	const CRect & rcMdiAreaClient,
	const CRect & rcMdiAreaWnd
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	rcMdiAreaWnd;
	if( rcOuterBorder == rcMdiAreaClient )
		return;
CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * pTII =
		SelectionGetPtr();
	if( pTII == NULL )
		return;
	dc.FillSolidRect(
		&rcOuterBorder,
		pTII->GetColorBkDark()
		);
}

#endif // (!defined __EXT_MFC_NO_TABMDI_CTRL )

#endif // __EXT_MFC_NO_TAB_ONENOTE_CTRL


/////////////////////////////////////////////////////////////////////////////
// CExtTabWhidbeyWnd

#ifndef __EXT_MFC_NO_TAB_WHIDBEY_CTRL

IMPLEMENT_DYNCREATE( CExtTabWhidbeyWnd, CExtTabWnd );

#define __EXTTAB_WHIDBEY_INDENT_TOP		1
#define __EXTTAB_WHIDBEY_INDENT_BOTTOM	1
#define __EXTTAB_WHIDBEY_INDENT_LEFT	3
#define __EXTTAB_WHIDBEY_INDENT_RIGHT	4

CExtTabWhidbeyWnd::CExtTabWhidbeyWnd()
{
}

CExtTabWhidbeyWnd::~CExtTabWhidbeyWnd()
{
}

bool CExtTabWhidbeyWnd::_IsCustomLayoutTabWnd() const
{
	return true;
}

BOOL CExtTabWhidbeyWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( IDC_STATIC )
	DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS
	DWORD dwTabStyle, // = __ETWS_WHIDBEY_DEFAULT
	CCreateContext * pContext // = NULL
	)
{
	ASSERT( pParentWnd != NULL );
	ASSERT( pParentWnd->GetSafeHwnd() != NULL );
	ASSERT( ::IsWindow(pParentWnd->GetSafeHwnd()) );
	
	if(	!CExtTabWnd::Create(
			pParentWnd,
			rcWnd,
			nDlgCtrlID,
			dwWindowStyle,
			dwTabStyle,
			pContext
			)
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	return TRUE;
}

void CExtTabWhidbeyWnd::PreSubclassWindow() 
{
	CExtTabWnd::PreSubclassWindow();

	if( m_bDirectCreateCall )
		return;

	ModifyTabWndStyle( 
		0xFFFFFFFF, 
		__ETWS_WHIDBEY_DEFAULT
		);
}

void CExtTabWhidbeyWnd::OnTabWndMeasureItemAreaMargins(
	LONG & nSpaceBefore,
	LONG & nSpaceAfter,
	LONG & nSpaceOver
	)
{
	ASSERT_VALID( this );
	nSpaceBefore	= 1;
	nSpaceAfter		= 2;
	nSpaceOver		= 3;
}

void CExtTabWhidbeyWnd::OnTabWndUpdateItemMeasure(
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	CDC & dcMeasure,
	CSize & sizePreCalc
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dcMeasure.GetSafeHdc() != NULL );
	ASSERT( pTii->GetTabWnd() == this );
	dcMeasure;

	bool bHorz = OrientationIsHorizontal();
	if( bHorz )
	{
		sizePreCalc.cx += (__EXTTAB_WHIDBEY_INDENT_LEFT + __EXTTAB_WHIDBEY_INDENT_RIGHT);
		if( pTii->GetIndexOf() == 0 )
			sizePreCalc.cx += 9;
		sizePreCalc.cy = 17;
	}
	else
	{
		sizePreCalc.cy += (__EXTTAB_WHIDBEY_INDENT_LEFT + __EXTTAB_WHIDBEY_INDENT_RIGHT);
		if( pTii->GetIndexOf() == 0 )
			sizePreCalc.cy += 9;
		sizePreCalc.cx = 17;
	}
}

void CExtTabWhidbeyWnd::OnTabWndEraseClientArea(
	CDC & dc,
	CRect & rcClient,
	CRect & rcTabItemsArea,
	CRect & rcTabNearBorderArea,
	DWORD dwOrientation,
	bool bGroupedMode
	)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	rcTabItemsArea;
	bGroupedMode;
	dc.FillSolidRect( 
		&rcClient, 
		PmBridge_GetPM()->GetColor( COLOR_3DLIGHT, this ) 
		);
	if( !rcTabNearBorderArea.IsRectEmpty() )
	{
		CRect rcTabNearMargin( rcTabNearBorderArea ); // prepare tab border margin rect
		switch( dwOrientation )
		{
			case __ETWS_ORIENT_TOP:
				rcTabNearMargin.bottom = rcTabNearMargin.top + 1;
				rcTabNearMargin.OffsetRect(0,-1);
			break;
			case __ETWS_ORIENT_BOTTOM:
				rcTabNearMargin.top = rcTabNearMargin.bottom - 1;
				rcTabNearMargin.OffsetRect(0,1);
			break;
			case __ETWS_ORIENT_LEFT:
				rcTabNearMargin.right = rcTabNearMargin.left + 1;
			break;
			case __ETWS_ORIENT_RIGHT:
				rcTabNearMargin.left = rcTabNearMargin.right - 1;
			break;
			default:
				ASSERT( FALSE );
			break;
		} // switch( dwOrientation )
		// paint tab border margin
		dc.FillSolidRect(
			&rcTabNearMargin,
			PmBridge_GetPM()->GetColor( COLOR_3DSHADOW, this )
			);
	} // if( !rcTabNearBorderArea.IsRectEmpty() )
}

void CExtTabWhidbeyWnd::OnTabWndDrawItem(
	CDC & dc,
	CRect & rcTabItemsArea,
	LONG nItemIndex,
	CExtTabWnd::TAB_ITEM_INFO * pTii,
	bool bTopLeft,
	bool bHorz,
	bool bSelected,
	bool bCenteredText,
	bool bGroupedMode,
	bool bInGroupActive,
	bool bInvertedVerticalMode,
	const CRect & rcEntireItem,
	CSize sizeTextMeasured,
	CFont * pFont,
	__EXT_MFC_SAFE_LPCTSTR sText,
	CExtCmdIcon * pIcon
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );

bool bEnabled = pTii->EnabledGet();
bool bHover = (m_nHoverTrackingHitTest == nItemIndex) ? true : false;

	if( (pTii->GetItemStyle() & __ETWI_CENTERED_TEXT) != 0 )
		bCenteredText = true;

bool bInGroupFirst = false;
	if( bGroupedMode )
	{
		TAB_ITEM_INFO * pTiiStart = pTii->GetInGroupFirst();
		ASSERT_VALID( pTiiStart );
		bInGroupFirst = ( nItemIndex == ItemGetIndexOf( pTiiStart ) );
	}
bool bFirstItem =
		(	nItemIndex == 0
		||	(	bInGroupFirst
			&&	nItemIndex != SelectionGet()
			)
		);

CExtCmdIcon * pIconTabItemCloseButton =
		OnTabWndQueryItemCloseButtonShape( pTii );
CExtCmdIcon::e_paint_type_t ePaintStateITICB =
		(CExtCmdIcon::e_paint_type_t)
			OnTabWndQueryItemCloseButtonPaintState( pTii );
CRect rcTabItemCloseButton( 0, 0, 0, 0 );
	if( pIconTabItemCloseButton != NULL )
		rcTabItemCloseButton = pTii->CloseButtonRectGet();

CRect rcItem( rcEntireItem );
POINT arrPointsBorders[11] =
		{	{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 },
		};
POINT arrPointsClipArea[11] =
		{	{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
			{ 0, 0 }, { 0, 0 }, { 0, 0 },
		};

	if( bHorz )
	{
		if( bTopLeft )
		{
			rcItem.OffsetRect( 0, 2 );

			arrPointsBorders[0] = CPoint( rcItem.right,		rcItem.bottom );
			arrPointsBorders[1] = CPoint( rcItem.right,		rcItem.top + 2 );
			arrPointsBorders[2] = CPoint( rcItem.right - 2,	rcItem.top );
			arrPointsBorders[3] = CPoint( rcItem.left + 9,	rcItem.top );
			arrPointsBorders[4] = CPoint( rcItem.left + 8,	rcItem.top + 1 );
			arrPointsBorders[5] = CPoint( rcItem.left + 7,	rcItem.top + 1 );
			arrPointsBorders[6] = CPoint( rcItem.left + 6,	rcItem.top + 2 );
			arrPointsBorders[7] = CPoint( rcItem.left + 5,	rcItem.top + 2 );
			arrPointsBorders[8] = CPoint( rcItem.left + 4,	rcItem.top + 3 );
			arrPointsBorders[9] = CPoint( rcItem.left,		rcItem.top + 7 );
			if( bSelected || bFirstItem )
				arrPointsBorders[10] = CPoint( rcItem.left - 10, rcItem.bottom );
			else
				arrPointsBorders[10] = CPoint( rcItem.left, rcItem.bottom );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].x += 9;

			arrPointsClipArea[0] = CPoint( rcItem.right,	rcItem.bottom );
			arrPointsClipArea[1] = CPoint( rcItem.right,	rcItem.top + 2 );
			arrPointsClipArea[2] = CPoint( rcItem.right - 2,rcItem.top );
			arrPointsClipArea[3] = CPoint( rcItem.left + 9,	rcItem.top );
			arrPointsClipArea[4] = CPoint( rcItem.left + 8,	rcItem.top + 2 );
			arrPointsClipArea[5] = CPoint( rcItem.left + 7,	rcItem.top + 2 );
			arrPointsClipArea[6] = CPoint( rcItem.left + 6,	rcItem.top + 3 );
			arrPointsClipArea[7] = CPoint( rcItem.left + 5,	rcItem.top + 3 );
			arrPointsClipArea[8] = CPoint( rcItem.left + 4,	rcItem.top + 4 );
			arrPointsClipArea[9] = CPoint( rcItem.left + 1,	rcItem.top + 7 );
			if( bSelected || bFirstItem )
				arrPointsClipArea[10] = CPoint( rcItem.left - 10, rcItem.bottom );
			else
				arrPointsClipArea[10] = CPoint( rcItem.left + 1, rcItem.bottom );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]); i++ )
					arrPointsClipArea[i].x += 9;
			if( bSelected )
			{
				arrPointsClipArea[0].y += 1;
				arrPointsClipArea[10].y += 1;
			}
		} // if( bTopLeft )
		else
		{
			arrPointsBorders[0] = CPoint( rcItem.right,		rcItem.top );
			arrPointsBorders[1] = CPoint( rcItem.right,		rcItem.bottom - 2 );
			arrPointsBorders[2] = CPoint( rcItem.right - 2,	rcItem.bottom );
			arrPointsBorders[3] = CPoint( rcItem.left + 9,	rcItem.bottom );
			arrPointsBorders[4] = CPoint( rcItem.left + 8,	rcItem.bottom - 1 );
			arrPointsBorders[5] = CPoint( rcItem.left + 7,	rcItem.bottom - 1 );
			arrPointsBorders[6] = CPoint( rcItem.left + 6,	rcItem.bottom - 2 );
			arrPointsBorders[7] = CPoint( rcItem.left + 5,	rcItem.bottom - 2 );
			arrPointsBorders[8] = CPoint( rcItem.left + 4,	rcItem.bottom - 3 );
			arrPointsBorders[9] = CPoint( rcItem.left,		rcItem.bottom - 7 );
			if( bSelected || bFirstItem )
				arrPointsBorders[10] = CPoint( rcItem.left - 10, rcItem.top );
			else
				arrPointsBorders[10] = CPoint( rcItem.left, rcItem.top );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].x += 9;
			
			arrPointsClipArea[0] = CPoint( rcItem.right,	rcItem.top + 1 );
			arrPointsClipArea[1] = CPoint( rcItem.right,	rcItem.bottom - 2 );
			arrPointsClipArea[2] = CPoint( rcItem.right - 2,rcItem.bottom );
			arrPointsClipArea[3] = CPoint( rcItem.left + 10,rcItem.bottom );
			arrPointsClipArea[4] = CPoint( rcItem.left + 9,	rcItem.bottom - 1 );
			arrPointsClipArea[5] = CPoint( rcItem.left + 8,	rcItem.bottom - 1 );
			arrPointsClipArea[6] = CPoint( rcItem.left + 7,	rcItem.bottom - 2 );
			arrPointsClipArea[7] = CPoint( rcItem.left + 6,	rcItem.bottom - 2 );
			arrPointsClipArea[8] = CPoint( rcItem.left + 5,	rcItem.bottom - 3 );
			arrPointsClipArea[9] = CPoint( rcItem.left + 1,	rcItem.bottom - 7 );
			if( bSelected || bFirstItem )
				arrPointsClipArea[10] = CPoint( rcItem.left - 9, rcItem.top + 1 );
			else
				arrPointsClipArea[10] = CPoint( rcItem.left + 1, rcItem.top + 1 );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]); i++ )
					arrPointsClipArea[i].x += 9;
			if( bSelected )
			{
				arrPointsClipArea[0].y -= 1;
				arrPointsClipArea[10].y -= 1;
			}
			if( !bSelected && bFirstItem )
				arrPointsClipArea[10].x += 1;

		} // else if( bTopLeft )
	} // if( bHorz )
	else
	{
		if( bTopLeft )
		{
			rcItem.OffsetRect( 3, 0 );

			arrPointsBorders[0] = CPoint( rcItem.right,		rcItem.bottom );
			arrPointsBorders[1] = CPoint( rcItem.left + 2,	rcItem.bottom );
			arrPointsBorders[2] = CPoint( rcItem.left,		rcItem.bottom - 2 );
			arrPointsBorders[3] = CPoint( rcItem.left,		rcItem.top + 9 );
			arrPointsBorders[4] = CPoint( rcItem.left + 1,	rcItem.top + 8 );
			arrPointsBorders[5] = CPoint( rcItem.left + 1,	rcItem.top + 7 );
			arrPointsBorders[6] = CPoint( rcItem.left + 2,	rcItem.top + 6 );
			arrPointsBorders[7] = CPoint( rcItem.left + 2,	rcItem.top + 5 );
			arrPointsBorders[8] = CPoint( rcItem.left + 3,	rcItem.top + 4 );
			arrPointsBorders[9] = CPoint( rcItem.left + 7,	rcItem.top );
			if( bSelected || bFirstItem )
				arrPointsBorders[10] = CPoint( rcItem.right, rcItem.top - 10 );
			else
				arrPointsBorders[10] = CPoint( rcItem.right, rcItem.top );

			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].y += 9;

			arrPointsClipArea[0] = CPoint( rcItem.right,	rcItem.bottom );
			arrPointsClipArea[1] = CPoint( rcItem.left + 2,	rcItem.bottom );
			arrPointsClipArea[2] = CPoint( rcItem.left + 1,	rcItem.bottom - 2 );
			arrPointsClipArea[3] = CPoint( rcItem.left + 1,	rcItem.top + 9 );
			arrPointsClipArea[4] = CPoint( rcItem.left + 2,	rcItem.top + 8 );
			arrPointsClipArea[5] = CPoint( rcItem.left + 2,	rcItem.top + 7 );
			arrPointsClipArea[6] = CPoint( rcItem.left + 3,	rcItem.top + 6 );
			arrPointsClipArea[7] = CPoint( rcItem.left + 3,	rcItem.top + 5 );
			arrPointsClipArea[8] = CPoint( rcItem.left + 4,	rcItem.top + 4 );
			arrPointsClipArea[9] = CPoint( rcItem.left + 7,	rcItem.top + 1 );
			if( bSelected || bFirstItem )
				arrPointsClipArea[10] = CPoint( rcItem.right, rcItem.top - 10 );
			else
				arrPointsClipArea[10] = CPoint( rcItem.right, rcItem.top );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]); i++ )
					arrPointsClipArea[i].y += 9;
			if( bSelected )
			{
				arrPointsClipArea[0].x += 1;
				arrPointsClipArea[10].x += 1;
			}
		
		} // if( bTopLeft )
		else
		{
			rcItem.OffsetRect( -1, 0 );

			arrPointsBorders[0] = CPoint( rcItem.left,		rcItem.bottom );
			arrPointsBorders[1] = CPoint( rcItem.right - 2,	rcItem.bottom );
			arrPointsBorders[2] = CPoint( rcItem.right,		rcItem.bottom - 2 );
			arrPointsBorders[3] = CPoint( rcItem.right,		rcItem.top + 9 );
			arrPointsBorders[4] = CPoint( rcItem.right - 1,	rcItem.top + 8 );
			arrPointsBorders[5] = CPoint( rcItem.right - 1,	rcItem.top + 7 );
			arrPointsBorders[6] = CPoint( rcItem.right - 2,	rcItem.top + 6 );
			arrPointsBorders[7] = CPoint( rcItem.right - 2,	rcItem.top + 5 );
			arrPointsBorders[8] = CPoint( rcItem.right - 3,	rcItem.top + 4 );
			arrPointsBorders[9] = CPoint( rcItem.right - 7,	rcItem.top );
			if( bSelected || bFirstItem )
				arrPointsBorders[10] = CPoint( rcItem.left, rcItem.top - 10 );
			else
				arrPointsBorders[10] = CPoint( rcItem.left, rcItem.top );

			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]); i++ )
					arrPointsBorders[i].y += 9;

			arrPointsClipArea[0] = CPoint( rcItem.left,		rcItem.bottom );
			arrPointsClipArea[1] = CPoint( rcItem.right - 2,rcItem.bottom );
			arrPointsClipArea[2] = CPoint( rcItem.right,	rcItem.bottom - 2 );
			arrPointsClipArea[3] = CPoint( rcItem.right,	rcItem.top + 9 );
			arrPointsClipArea[4] = CPoint( rcItem.right - 1,	rcItem.top + 8 );
			arrPointsClipArea[5] = CPoint( rcItem.right - 1,	rcItem.top + 7 );
			arrPointsClipArea[6] = CPoint( rcItem.right - 2,	rcItem.top + 6 );
			arrPointsClipArea[7] = CPoint( rcItem.right - 2,	rcItem.top + 5 );
			arrPointsClipArea[8] = CPoint( rcItem.right - 3,	rcItem.top + 4 );
			arrPointsClipArea[9] = CPoint( rcItem.right - 6,	rcItem.top + 1 );
			if( bSelected || bFirstItem )
				arrPointsClipArea[10] = CPoint( rcItem.left + 1, rcItem.top - 10 );
			else
				arrPointsClipArea[10] = CPoint( rcItem.left + 1, rcItem.top );
			if( nItemIndex == 0 )
				for( int i = 3 ; i < sizeof(arrPointsClipArea)/sizeof(arrPointsClipArea[0]); i++ )
					arrPointsClipArea[i].y += 9;
			if( bSelected )
			{
				arrPointsClipArea[0].x -= 1;
				arrPointsClipArea[10].x -= 1;
			}		
			if( !bSelected && bFirstItem )
				arrPointsClipArea[10].y += 1;
		} // else if( bTopLeft )

	} // else if( bHorz )

COLORREF clrBorder = (COLORREF)(-1L);
COLORREF clrBkLight = (COLORREF)(-1L);
COLORREF clrBkDark = (COLORREF)(-1L);
COLORREF clrText = (COLORREF)(-1L);

	OnTabWndQueryItemColors(
		nItemIndex,
		bSelected,
		bHover,
		bEnabled,
		&clrBorder,
		&clrBkLight,
		&clrBkDark,
		&clrText
		);

	// draw item border 
CPen pen(PS_SOLID, 1, clrBorder);
CPen * pOldPen = dc.SelectObject( &pen );
	for( int i = 0 ; i < sizeof(arrPointsBorders)/sizeof(arrPointsBorders[0]) - 1; i++ )
	{
		dc.MoveTo( arrPointsBorders[i] );
		dc.LineTo( arrPointsBorders[i + 1] );
	}
	if( !bSelected )
	{
		dc.MoveTo( arrPointsBorders[10] );
		dc.LineTo( arrPointsBorders[0] );
	}
	dc.SelectObject( pOldPen );

CRgn rgnClipArea;
	VERIFY( rgnClipArea.CreatePolygonRgn( arrPointsClipArea, 11, ALTERNATE ) );

	// fill item background 
CRect rcFill( rcTabItemsArea );
	rcFill.DeflateRect(
		(!bHorz) ? ( bTopLeft ?  0 : -1 ) : -1,
		  bHorz  ? ( bTopLeft ?  3 :  0 ) : -1,
		(!bHorz) ? ( bTopLeft ? -1 :  4 ) : -1,
		  bHorz  ? ( bTopLeft ?  0 :  3 ) : -1
		);

	dc.SelectClipRgn( &rgnClipArea, RGN_AND );
	if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )
	{
		CExtPaintManager::stat_PaintGradientRect(
			dc,
			&rcFill,
			bHorz 
				? ( bTopLeft ? clrBkDark : clrBkLight ) 
				: ( bTopLeft ? clrBkLight : clrBkDark ),
			bHorz 
				? ( bTopLeft ? clrBkLight : clrBkDark ) 
				: ( bTopLeft ? clrBkDark : clrBkLight ),
			bHorz
		);
	}
	else 
	{
		dc.FillSolidRect( 
			&rcFill, 
			bSelected 
				? PmBridge_GetPM()->GetColor( COLOR_WINDOW, this )
				: PmBridge_GetPM()->GetColor( COLOR_3DFACE, this )
			);
	}

	rcItem.DeflateRect(
		bHorz 
			? __EXTTAB_WHIDBEY_INDENT_LEFT 
			: ( bTopLeft ? __EXTTAB_WHIDBEY_INDENT_TOP * 2 : 0 ),
		bHorz 
			? ( bTopLeft ? __EXTTAB_WHIDBEY_INDENT_TOP : 0 ) 
			: __EXTTAB_WHIDBEY_INDENT_LEFT,
		bHorz 
			? __EXTTAB_WHIDBEY_INDENT_RIGHT 
			: __EXTTAB_WHIDBEY_INDENT_TOP,
		bHorz 
			? __EXTTAB_WHIDBEY_INDENT_BOTTOM 
			: __EXTTAB_WHIDBEY_INDENT_RIGHT
		);

	// first item indent
	if( nItemIndex == 0 )
		rcItem.DeflateRect(
			bHorz ? 9 : 0,
			!bHorz ? 9 : 0,
			0,
			0
			);

bool bDrawIcon = (		
			pIcon != NULL 
		&&	(!pIcon->IsEmpty()) 
		&&	(GetTabWndStyle()&__ETWS_HIDE_ICONS) == 0 
		);

	// if tab has no icons - increase left indent
	if( !bDrawIcon && (GetTabWndStyle()&__ETWS_CENTERED_TEXT) == 0 )
		rcItem.DeflateRect(
			bHorz ? 6 : 0,
			0,
			0,
			0
			);

	CSize _sizeIcon( 0, 0 );
	if( bDrawIcon )
	{
		_sizeIcon = pIcon->GetSize();
		ASSERT( _sizeIcon.cx > 0 && _sizeIcon.cy > 0 );
	}
	CRect rcItemForIcon( rcItem );
	if(		bDrawIcon
		&&	_sizeIcon.cx > 0
		&&	_sizeIcon.cy > 0
		)
	{
		rcItemForIcon.right = rcItemForIcon.left + _sizeIcon.cx;
		rcItemForIcon.bottom = rcItemForIcon.top + _sizeIcon.cy;
		rcItemForIcon.OffsetRect(
			bHorz ? 0 : ((rcItem.Width() - _sizeIcon.cx) / 2),
			bHorz ? ((rcItem.Height() - _sizeIcon.cy) / 2) : 0
			);
		if( rcItemForIcon.left < rcItem.left )
			rcItemForIcon.left = rcItem.left;
		if( rcItemForIcon.right > rcItem.right )
			rcItemForIcon.right = rcItem.right;
		if( rcItemForIcon.top < rcItem.top )
			rcItemForIcon.top = rcItem.top;
		if( rcItemForIcon.bottom > rcItem.bottom )
			rcItemForIcon.bottom = rcItem.bottom;
	}

	CExtSafeString sItemText( (sText == NULL) ? _T("") : sText );

	// IMPORTANT:  the rcText calculation fixed by Genka
	CRect rcText(
		rcItem.left
			+	(	bHorz
					? (_sizeIcon.cx +
						((_sizeIcon.cx > 0) ? 2*__EXTTAB_MARGIN_ICON2TEXT_X : 0)
						)
					: 0
				),
		rcItem.top 
			+	(	bHorz
					? 0
					: (_sizeIcon.cy +
						((_sizeIcon.cy > 0) ? __EXTTAB_MARGIN_ICON2TEXT_Y : 0)
						)
				),
		rcItem.right,
		rcItem.bottom
		);
	if( !bHorz )
	{
		if( pIconTabItemCloseButton != NULL )
			rcText.bottom = min( rcText.bottom, rcTabItemCloseButton.top );
		int nWidth0 = rcText.Width();
		int nWidth1 = rcItem.Width() + __EXTTAB_MARGIN_ICON2TEXT_X*2;
		if( nWidth1 > nWidth0 )
		{
			if( bInvertedVerticalMode )
			{
				rcText.left = rcText.right - nWidth1;
			} // if( bInvertedVerticalMode )
			else
			{
				rcText.right = rcText.left + nWidth1;
			} // else from if( bInvertedVerticalMode )
		} // if( nWidth1 > nWidth0 )
	} // if( !bHorz )
	else
	{
		if( pIconTabItemCloseButton != NULL )
			rcText.right = min( rcText.right, rcTabItemCloseButton.left );
	}

	CSize _sizeText = rcText.Size();

	bool bDrawText = false;
	if(		( bHorz		&& _sizeText.cx >= ( max(16,_sizeIcon.cx) ) )
		||	( (!bHorz)	&& _sizeText.cy >= ( max(16,_sizeIcon.cy) ) )
		)
		bDrawText = true;

	if( !bDrawText && !( bGroupedMode && ! bInGroupActive ) )
	{
		rcItemForIcon.OffsetRect(
			bHorz ? (rcItem.Width() - _sizeIcon.cx) / 2 : 0,
			bHorz ? 0 : (rcItem.Height() - _sizeIcon.cy) / 2
			);
	}

	if( bDrawIcon )
	{
		if(		(bHorz && rcItemForIcon.Width() >= _sizeIcon.cx )
			||	(!bHorz && rcItemForIcon.Height() >= _sizeIcon.cy)
			)
		{
			CRect rcTmpText( 0, 0, 0, 0 );
			CExtSafeString sText( bDrawText ? sItemText : _T("") );
			PmBridge_GetPM()->PaintIcon(
				dc,
				bHorz,
				sText,
				pIcon,
				rcItemForIcon,
				rcTmpText,
				false,
				bEnabled,
				false,
				0
				);
		}
	}

	if( bDrawText )
	{ // if we have sense to paint text on tab item
		ASSERT( pFont != NULL );
		ASSERT( pFont->GetSafeHandle() != NULL );
		COLORREF clrOldText = dc.SetTextColor( clrText );
		INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
		CFont * pOldFont = dc.SelectObject( pFont );
		if( !bHorz )
		{
			if( bCenteredText )
			{
				UINT nOldTA = dc.SetTextAlign(
					TA_CENTER | TA_BASELINE
					);
				rcText.OffsetRect(
					bInvertedVerticalMode
						?   sizeTextMeasured.cy/2
						: - sizeTextMeasured.cy/2
						,
					0
					);
				CPoint ptCenter = rcText.CenterPoint();
				dc.ExtTextOut(
					ptCenter.x,
					ptCenter.y,
					ETO_CLIPPED,
					&rcText,
					sItemText,
					sItemText.GetLength(),
					NULL
					);
				dc.SetTextAlign( nOldTA );
			} // if( bCenteredText )
			else
			{
				UINT nOldTA = dc.SetTextAlign(
					TA_TOP | TA_BASELINE
					);
				rcText.OffsetRect(
					bInvertedVerticalMode
						?   sizeTextMeasured.cy/2
						: - sizeTextMeasured.cy/2
						,
					0
					);
				CPoint ptCenter = rcText.CenterPoint();
				if( bInvertedVerticalMode )
					ptCenter.y =
						rcText.bottom - 4
						- (rcText.Height() - sizeTextMeasured.cx)
						;
				else
					ptCenter.y =
						rcText.top + 4
						;
				dc.ExtTextOut(
					ptCenter.x,
					ptCenter.y,
					ETO_CLIPPED,
					&rcText,
					sItemText,
					sItemText.GetLength(),
					NULL
					);
				dc.SetTextAlign( nOldTA );
			} // else from if( bCenteredText )
		} // if( !bHorz )
		else
		{
			UINT nFormat =
				DT_SINGLELINE|DT_VCENTER|DT_END_ELLIPSIS;
			if( bCenteredText )
				nFormat |= DT_CENTER;
			else
				nFormat |= DT_LEFT;

			dc.DrawText(
				sItemText,
				sItemText.GetLength(),
				rcText,
				nFormat
				);
		}
		dc.SelectObject( pOldFont );
		dc.SetBkMode( nOldBkMode );
		dc.SetTextColor( clrOldText );
	} // if we have sense to paint text on tab item

	if( pIconTabItemCloseButton != NULL )
	{
		ASSERT( ! pIconTabItemCloseButton->IsEmpty() );
		if( dc.RectVisible( &rcTabItemCloseButton ) )
		{
			CRect _rcTabItemCloseButton = rcTabItemCloseButton;
			if( bHorz )
			{
				if( bTopLeft )
					_rcTabItemCloseButton.OffsetRect( 0, 3 );
			}
			else
			{
				if( bTopLeft )
					_rcTabItemCloseButton.OffsetRect( 4, 0 );
			}
			pIconTabItemCloseButton->Paint(
				g_PaintManager.GetPM(),
				dc.m_hDC,
				_rcTabItemCloseButton,
				ePaintStateITICB
				);
		}
	} // if( pIconTabItemCloseButton != NULL )

	dc.SelectClipRgn( NULL );
}

void CExtTabWhidbeyWnd::OnTabWndQueryItemColors(
	LONG nItemIndex,
	bool bSelected,
	bool bHover,
	bool bEnabled,
	COLORREF * pclrBorder, // = NULL,
	COLORREF * pclrBkLight, // = NULL,
	COLORREF * pclrBkDark, // = NULL,
	COLORREF * pclrText // = NULL
	)
{
	ASSERT_VALID( this );
	bHover;
	nItemIndex;

	COLORREF clrBorder = (COLORREF)(-1L);
	COLORREF clrBkLight = (COLORREF)(-1L);
	COLORREF clrBkDark = (COLORREF)(-1L);
	COLORREF clrText = (COLORREF)(-1L);

	PmBridge_GetPM()->GetTabWhidbeyItemColors(
		bSelected,
		bHover,
		bEnabled,
		clrBorder,
		clrBkLight,
		clrBkDark,
		clrText
	);
	
	if( pclrBorder != NULL )
		*pclrBorder = clrBorder;
	if( pclrBkLight != NULL )
		*pclrBkLight = clrBkLight;
	if( pclrBkDark != NULL )
		*pclrBkDark = clrBkDark;
	if( pclrText != NULL )
		*pclrText = clrText;
}


#if (!defined __EXT_MFC_NO_TABMDI_CTRL )

/////////////////////////////////////////////////////////////////////////////
// CExtTabMdiWhidbeyWnd window

IMPLEMENT_DYNCREATE( CExtTabMdiWhidbeyWnd, CExtTabWhidbeyWnd );

CExtTabMdiWhidbeyWnd::CExtTabMdiWhidbeyWnd()
{
}

CExtTabMdiWhidbeyWnd::~CExtTabMdiWhidbeyWnd()
{
}

BEGIN_MESSAGE_MAP(CExtTabMdiWhidbeyWnd, CExtTabWhidbeyWnd)
	//{{AFX_MSG_MAP(CExtTabMdiWhidbeyWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG

void CExtTabMdiWhidbeyWnd::AssertValid() const
{
	CExtTabWhidbeyWnd::AssertValid();
}

void CExtTabMdiWhidbeyWnd::Dump(CDumpContext& dc) const
{
	CExtTabWhidbeyWnd::Dump( dc );
}

#endif

#endif // (!defined __EXT_MFC_NO_TABMDI_CTRL )

#endif // __EXT_MFC_NO_TAB_WHIDBEY_CTRL




#endif // (!defined __EXT_MFC_NO_TAB_CTRL )
