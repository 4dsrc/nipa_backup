// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_LABEL_H)
	#include <ExtLabel.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include <../Src/ExtMemoryDC.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtLabel

IMPLEMENT_DYNCREATE( CExtLabel, CStatic );
IMPLEMENT_CExtPmBridge_MEMBERS( CExtLabel );

CExtLabel::CExtLabel()
	: m_bFontBold(false)
	, m_bFontItalic(false)
	, m_bFontUnderline(false)
	, m_bFontStrikeOut(false)
	, m_clrTextNormal( COLORREF(-1L) )
	, m_clrTextDisabled( COLORREF(-1L) )
	, m_clrBackground( COLORREF(-1L) )
	, m_bInitText(false)
	, m_sText( _T("") )
{
	PmBridge_Install();
}

CExtLabel::~CExtLabel()
{
	PmBridge_Uninstall();
}

BEGIN_MESSAGE_MAP(CExtLabel, CStatic)
	//{{AFX_MSG_MAP(CExtLabel)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CExtLabel::OnEraseBkgnd(CDC* pDC) 
{
	pDC;
	return TRUE;
}

void CExtLabel::DoPaint( 
	CDC * pDC,
	CRect & rcClient
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC );
CExtMemoryDC dc(
		pDC,
		&rcClient
		);
CRgn rgnClient;
	if( rgnClient.CreateRectRgnIndirect( &rcClient ) )
		dc.SelectClipRgn( &rgnClient );

	OnEraseBackground( dc, rcClient );

DWORD dwWndStyle = GetStyle();
DWORD dwWndType = (dwWndStyle&SS_TYPEMASK);
	if( dwWndType == SS_ICON )
	{
		HICON hIcon = GetIcon();
		if( hIcon != NULL )
		{
			CExtCmdIcon _icon;
			_icon.AssignFromHICON( hIcon, true );
			CSize szIcon = _icon.GetSize();
			bool bCenterImage = ( (dwWndStyle&SS_CENTERIMAGE) != 0 );
			int nOffsetX = bCenterImage ? (rcClient.Width() - szIcon.cx) / 2 : 0;
			int nOffsetY = bCenterImage ? (rcClient.Height() - szIcon.cy) / 2 : 0;
			_icon.Paint(
				PmBridge_GetPM(),
				dc.GetSafeHdc(),
 				rcClient.left + nOffsetX,
 				rcClient.top + nOffsetY,
				-1,
				-1
				);
		}	
	}
	else
	{
		CExtSafeString strText;
		int nTextLen = GetWindowTextLength();
		if( nTextLen > 0 )
		{
			GetWindowText( strText.GetBuffer( nTextLen + 2 ), nTextLen + 1 );
			strText.ReleaseBuffer();
		} // if( nTextLen > 0 )
		if( strText.GetLength() > 0 )
		{
			DWORD dwDrawTextFlags = 0;
				switch( dwWndType )
			{
			case SS_RIGHT: 
				dwDrawTextFlags = DT_RIGHT|DT_WORDBREAK; 
				break; 
			case SS_CENTER: 
				dwDrawTextFlags = DT_CENTER|DT_WORDBREAK;
				break;
			case SS_LEFTNOWORDWRAP: 
				dwDrawTextFlags = DT_LEFT; 
				break;
			default: // all the other types assumed as left
			case SS_LEFT: 
				dwDrawTextFlags = DT_LEFT|DT_WORDBREAK; 
				break;
				} // switch( dwWndType )

			if( strText.Find( _T('\t') ) != -1 ) // do tabs expanding
				dwDrawTextFlags |= DT_EXPANDTABS;

			if( (dwWndStyle&SS_CENTERIMAGE) != 0 )
			{ // center vertically
				// dwDrawTextFlags = DT_CENTER; // - 2.28
				// DT_VCENTER is for DT_SINGLELINE only
				if( strText.Find( _T("\r\n") ) == -1 )
					dwDrawTextFlags |= DT_VCENTER|DT_SINGLELINE;
			} // center vertically

			dwDrawTextFlags |= (DT_VCENTER|DT_END_ELLIPSIS); 

			bool bEnabled = IsWindowEnabled() ? true : false;
			OnDrawLabelText(
				dc,
				rcClient,
				strText,
				dwDrawTextFlags,
				bEnabled
				);
		} // if( strText.GetLength() > 0 )
	}
	PmBridge_GetPM()->OnPaintSessionComplete( this );
	if( rgnClient.GetSafeHandle() != NULL )
		dc.SelectClipRgn( &rgnClient );	
}

void CExtLabel::OnPaint() 
{
	ASSERT_VALID( this );
DWORD dwWndStyle = GetStyle();
DWORD dwWndType = (dwWndStyle&SS_TYPEMASK);
	if(		dwWndType == SS_BLACKRECT 
		||	dwWndType == SS_GRAYRECT 
		||	dwWndType == SS_WHITERECT 
		||	dwWndType == SS_BLACKFRAME 
		||	dwWndType == SS_GRAYFRAME 
		||	dwWndType == SS_WHITEFRAME 
		||	dwWndType == SS_USERITEM 
		||	dwWndType == SS_OWNERDRAW 
		||	dwWndType == SS_BITMAP 
		||	dwWndType == SS_ENHMETAFILE 
		||	dwWndType == SS_ETCHEDHORZ 
		||	dwWndType == SS_ETCHEDVERT 
		||	dwWndType == SS_ETCHEDFRAME 
		)
	{
		Default();
		return;
	}
CPaintDC dcPaint( this );
CRect rcClient;
	GetClientRect( &rcClient );
	if( rcClient.IsRectEmpty() )
		return;
	DoPaint( &dcPaint, rcClient );
}

void CExtLabel::OnEraseBackground(
	CDC & dc,
	const CRect & rcClient
	)
{
	ASSERT_VALID( this );
COLORREF clrBackground = GetBkColor();
bool bTransparent = false;
	if(		PmBridge_GetPM()->GetCb2DbTransparentMode(this)
		&&	( clrBackground == COLORREF(-1L) )
		)
	{
		CExtPaintManager::stat_ExcludeChildAreas(
			dc,
			GetSafeHwnd(),
			CExtPaintManager::stat_DefExcludeChildAreaCallback
			);
		if( PmBridge_GetPM()->PaintDockerBkgnd( true, dc, this ) )
			bTransparent = true;
	}
	if( ! bTransparent )
		dc.FillSolidRect(
			&rcClient,
			(clrBackground != COLORREF(-1L)) 
				? clrBackground 
				: PmBridge_GetPM()->GetColor( CExtPaintManager::CLR_3DFACE_OUT, this ) 
			);	
}

void CExtLabel::OnDrawLabelText(
	CDC & dc,
	const RECT & rcText,
	__EXT_MFC_SAFE_LPCTSTR strText,
	DWORD dwDrawTextFlags,
	bool bEnabled
	)
{
HFONT hFont = (HFONT)
		::SendMessage( m_hWnd, WM_GETFONT, 0L, 0L );
	if( hFont == NULL )
	{
		HWND hWndParent = ::GetParent( m_hWnd );
		if( hWndParent != NULL )
			hFont = (HFONT)
				::SendMessage( hWndParent, WM_GETFONT, 0L, 0L );
	} // if( hFont == NULL )
	if( hFont == NULL )
	{
		hFont = (HFONT)::GetStockObject( DEFAULT_GUI_FONT );
		if( hFont == NULL )
			hFont = (HFONT)::GetStockObject( SYSTEM_FONT );
	} // if( hFont == NULL )

LOGFONT lf;
	::GetObject( 
		(HGDIOBJ) hFont, 
        sizeof( lf ), 
        (LPVOID) & lf 
		);
	hFont = NULL;
	
	if( m_bFontBold )
		lf.lfWeight = 
			(lf.lfWeight > FW_BOLD) 
				? lf.lfWeight 
				: FW_BOLD;
	lf.lfItalic = (BYTE)( m_bFontItalic ? 1 : 0 );
	lf.lfUnderline = (BYTE)( m_bFontUnderline ? 1 : 0 );
	lf.lfStrikeOut = (BYTE)( m_bFontStrikeOut ? 1 : 0 );
	
	hFont = ::CreateFontIndirect( &lf );
	ASSERT( hFont != NULL );
CFont _fontDestructor;
	_fontDestructor.Attach( hFont );
	
HGDIOBJ hOldFont = NULL;
	if( hFont != NULL )
		hOldFont = ::SelectObject( dc, (HGDIOBJ)hFont );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );
COLORREF clrOldText =
		dc.SetTextColor( OnQueryTextColor( bEnabled ) );
			
CRect rc( rcText );
	dc.DrawText(
		LPCTSTR(strText),
		int(_tcslen(strText)),
		rc,
		dwDrawTextFlags
		);
	dc.SetTextColor( clrOldText );
	dc.SetBkMode( nOldBkMode );
	if( hFont != NULL )
		::SelectObject( dc, hOldFont );
}

COLORREF CExtLabel::OnQueryTextColor(
	bool bEnabled
	) const
{
	ASSERT_VALID( this );
COLORREF clrText = GetTextColor( bEnabled );
	if( clrText != COLORREF(-1L) )
		return clrText;
CWindowDC dcFake( NULL );
	clrText =
		PmBridge_GetPM()->QueryObjectTextColor(
			dcFake,
			bEnabled,
			false,
			false,
			false,
			(CObject*)this
			);
	if( clrText != COLORREF(-1L) )
		return clrText;
	clrText =
		PmBridge_GetPM()->GetColor(
			bEnabled
				? COLOR_BTNTEXT
				: COLOR_3DSHADOW
				,
			(CObject*)this
			);
	return clrText;
}

LRESULT CExtLabel::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
#if (defined WM_UPDATEUISTATE)	
	ASSERT( WM_UPDATEUISTATE == 0x0128 );
#endif
	// WM_UPDATEUISTATE causes repaint without WM_PAINT, so we eat it
	if( message == 0x0128 )
		return 0;
	if(	message == WM_ENABLE )
	{
		LRESULT lResult = CStatic::WindowProc(message, wParam, lParam);
		Invalidate();
		UpdateWindow();
		return lResult;
	}
	if( message == WM_PRINT || message == WM_PRINTCLIENT )
	{
		CDC * pDC = CDC::FromHandle( (HDC)wParam );
		CRect rcClient;
		GetClientRect( &rcClient );
		DoPaint( pDC, rcClient );
		return (!0);
	}	
	if(		message == WM_SETTEXT 
		||	message == WM_GETTEXT 
		||	message == WM_GETTEXTLENGTH 
		)
	{
		DWORD dwWndStyle = GetStyle();
		DWORD dwWndType = (dwWndStyle&SS_TYPEMASK);
		if(		dwWndType == SS_BLACKRECT 
			||	dwWndType == SS_GRAYRECT 
			||	dwWndType == SS_WHITERECT 
			||	dwWndType == SS_BLACKFRAME 
			||	dwWndType == SS_GRAYFRAME 
			||	dwWndType == SS_WHITEFRAME 
			||	dwWndType == SS_USERITEM 
			||	dwWndType == SS_OWNERDRAW 
			||	dwWndType == SS_BITMAP 
			||	dwWndType == SS_ICON
			||	dwWndType == SS_ENHMETAFILE 
			||	dwWndType == SS_ETCHEDHORZ 
			||	dwWndType == SS_ETCHEDVERT 
			||	dwWndType == SS_ETCHEDFRAME 
			)
			return CStatic::WindowProc( message, wParam, lParam );

		LRESULT lResult = FALSE;
		if(		(!m_bInitText)
			&&	(message == WM_GETTEXT || message == WM_GETTEXTLENGTH)
			)
		{
			lResult = CStatic::WindowProc( message, wParam, lParam );
			
			INT nMaxLength = 0;
			if( message != WM_GETTEXTLENGTH )
			{
				WPARAM wParamLocal = 0L;
				LPARAM lParamLocal = 0L; 
				nMaxLength = (INT)
					CStatic::WindowProc( 
						WM_GETTEXTLENGTH, 
						wParamLocal, 
						lParamLocal 
						);
			}
			else
				nMaxLength = (INT)lResult;
			CString sTextInit;
			CStatic::WindowProc( 
				WM_GETTEXT, 
				nMaxLength + 1, 
				(LPARAM)sTextInit.GetBuffer( nMaxLength + 1 ) 
				);
			sTextInit.ReleaseBuffer();
			m_sText = sTextInit;

			m_bInitText = true;
			return lResult;
		}

		if( message == WM_SETTEXT )
		{
			LPCTSTR lpszText = (LPCTSTR)lParam;
			m_sText = lpszText;
			m_bInitText = true;
			RedrawWindow(
				NULL, NULL,
				RDW_INVALIDATE | RDW_ERASE | RDW_UPDATENOW | RDW_ERASENOW
				);
			lResult = TRUE;
		}
		else if( message == WM_GETTEXT )
		{
			TCHAR * lpszText = (TCHAR *)lParam;
			// 2.55
			// __EXT_MFC_STRCPY(
			// 	lpszText, 
			// 	wParam, 
			// 	m_sText
			// 	);
			::memset( lpszText, 0, wParam );
			__EXT_MFC_STRNCPY(
				lpszText,
				wParam,
				m_sText,
				wParam - 1
				);
			lpszText[ wParam - 1 ] = _T('\0');
			lResult = TRUE;
		}
		else if( message == WM_GETTEXTLENGTH )
		{
			lResult = m_sText.GetLength();
		}
 		return lResult;
	}

	return CStatic::WindowProc(message, wParam, lParam);
}

void CExtLabel::PreSubclassWindow()
{
	m_bInitText = false;
	CStatic::PreSubclassWindow();
}

void CExtLabel::SetFontBold( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	m_bFontBold = bSet;
	if( GetSafeHwnd() != NULL )
		Invalidate();
}

void CExtLabel::SetFontItalic( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	m_bFontItalic = bSet;
	if( GetSafeHwnd() != NULL )
		Invalidate();
}

void CExtLabel::SetFontUnderline( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	m_bFontUnderline = bSet;
	if( GetSafeHwnd() != NULL )
		Invalidate();
}

void CExtLabel::SetFontStrikeOut( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	m_bFontStrikeOut = bSet;
	if( GetSafeHwnd() != NULL )
		Invalidate();
}

void CExtLabel::SetBkColor( COLORREF clrBk )
{ 
	ASSERT_VALID( this );
	m_clrBackground = clrBk; 
	if( GetSafeHwnd() != NULL )
		Invalidate();
}

void CExtLabel::SetTextColor(
	bool bEnabled,
	COLORREF clrText // = COLORREF(-1L)
	)
{
	ASSERT_VALID( this );
	if( bEnabled )
		m_clrTextNormal = clrText;
	else
		m_clrTextDisabled = clrText;
	if( GetSafeHwnd() != NULL )
		Invalidate();
}
	
bool CExtLabel::GetFontBold()
{
	ASSERT_VALID( this );
	return m_bFontBold;
}

bool CExtLabel::GetFontItalic()
{
	ASSERT_VALID( this );
	return m_bFontItalic;
}

bool CExtLabel::GetFontUnderline()
{
	ASSERT_VALID( this );
	return m_bFontUnderline;
}

bool CExtLabel::GetFontStrikeOut()
{
	ASSERT_VALID( this );
	return m_bFontStrikeOut;
}

COLORREF CExtLabel::GetBkColor() const
{ 
	ASSERT_VALID( this );
	return m_clrBackground; 
}

COLORREF CExtLabel::GetTextColor(
	bool bEnabled
	) const
{
	ASSERT_VALID( this );
COLORREF clrText =
		bEnabled
			? m_clrTextNormal
			: m_clrTextDisabled
			;
	return clrText;
}
