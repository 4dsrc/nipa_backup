// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "StdAfx.h"

#if (!defined __EXT_HOOK_H)
	#include "ExtHook.h"
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

struct __PROF_UIS_API CExtHookSink::HookSinkArray_t :
	public CArray< CExtHookSink *, CExtHookSink * >
{
	INT Find( const CExtHookSink * pHookSink ) const
	{
		ASSERT( pHookSink != NULL );
		for( INT nSinkIdx = 0; nSinkIdx < GetSize(); nSinkIdx++ )
		{
			const CExtHookSink * pHookSinkExamine =
				GetAt( nSinkIdx );
			ASSERT( pHookSink != NULL );
			if( pHookSinkExamine == pHookSink )
				return nSinkIdx;
		}
		return -1;
	}
	void AddHead( CExtHookSink * pHookSink )
	{
		ASSERT( pHookSink != NULL );
		InsertAt( 0, pHookSink );
	}
	void AddTail( CExtHookSink * pHookSink )
	{
		ASSERT( pHookSink != NULL );
		InsertAt( GetSize(), pHookSink );
	}
}; // struct HookSinkArray_t

struct __PROF_UIS_API CExtHookSink::HookChains_t
{
	bool m_bEatNcDestroy:1;
	HookSinkArray_t m_HookSinkArray;
	HWND m_hWndHooked;
	WNDPROC m_pWNDPROC;

	static LRESULT CALLBACK g_HookWndProc(
		HWND hWnd,
		UINT nMessage,
		WPARAM wParam,
		LPARAM lParam
		);

	HookChains_t(
		HWND hWndHooked
		)
		: m_hWndHooked( hWndHooked )
		, m_bEatNcDestroy( false )
	{
		ASSERT( m_hWndHooked != NULL );
		ASSERT( ::IsWindow(m_hWndHooked) );

#if _MFC_VER >= 0x800
		m_pWNDPROC = (WNDPROC)
			::SetWindowLongPtr(
				m_hWndHooked,
				__EXT_MFC_GWL_WNDPROC,
				(DWORD)g_HookWndProc
				);
#else
		m_pWNDPROC = (WNDPROC)
			::SetWindowLong(
				m_hWndHooked,
				__EXT_MFC_GWL_WNDPROC,
				(DWORD)g_HookWndProc
				);
#endif
//		ASSERT( m_pWNDPROC != NULL );
	};

	~HookChains_t()
	{
		DestroyChains( false );
	};

	void DestroyChains( bool bDelete )
	{
		for( int nSinkIdx=0; nSinkIdx < m_HookSinkArray.GetSize(); nSinkIdx++ )
		{
			CExtHookSink * pHookSink =
				m_HookSinkArray[ nSinkIdx ];
			ASSERT( pHookSink != NULL );
			if( pHookSink->IsAutoDeleteHookWndSink() )
				delete pHookSink;
		} // for( int nSinkIdx=0; nSinkIdx < m_HookSinkArray.GetSize(); nSinkIdx++ )
		m_HookSinkArray.RemoveAll();

		ASSERT( m_hWndHooked != NULL );
		ASSERT( ::IsWindow(m_hWndHooked) );
		ASSERT( m_pWNDPROC != NULL );

#if _MFC_VER >= 0x800
		::SetWindowLongPtr(
			m_hWndHooked,
			__EXT_MFC_GWL_WNDPROC,
			(DWORD)m_pWNDPROC
			);
#else
		::SetWindowLong(
			m_hWndHooked,
			__EXT_MFC_GWL_WNDPROC,
			(DWORD)m_pWNDPROC
			);
#endif

		if( bDelete )
			delete this;
	};

	LRESULT HookChainsWindowProc(
		UINT nMessage,
		WPARAM & wParam,
		LPARAM & lParam
		)
	{
		ASSERT( m_hWndHooked != NULL );
		ASSERT( ::IsWindow(m_hWndHooked) );
		ASSERT( m_pWNDPROC != NULL );
//		CWnd * pWndPermanent = NULL;
//		{ // BLOCK: state managing
//			__PROF_UIS_MANAGE_STATE;
//			pWndPermanent = 
//				CWnd::FromHandlePermanent( m_hWndHooked );
//		} // BLOCK: state managing
		bool bEatNcDestroy = m_bEatNcDestroy;
		int nSinkIdx, nSinkCount = (int)m_HookSinkArray.GetSize();
		for( nSinkIdx = 0; nSinkIdx < nSinkCount; nSinkIdx++ )
		{
			CExtHookSink * pHookSink =
				m_HookSinkArray[ nSinkIdx ];
			ASSERT( pHookSink != NULL );
			if( nMessage == WM_NCDESTROY ) 
			{
				if(		(! bEatNcDestroy )
					&&	pHookSink->m_bEatNcDestroy
					)
					bEatNcDestroy = true;
				pHookSink->OnHookWndNcDestroy();
				continue;
			} // if( nMessage == WM_NCDESTROY ) 
			LRESULT lResult = 0;
			if(	pHookSink->OnHookWndMsg(
					lResult,
					m_hWndHooked,
					nMessage,
					wParam,
					lParam
					)
				)
				return lResult;
		} // for( nSinkIdx = 0; nSinkIdx < nSinkCount; nSinkIdx++ )
		WNDPROC pWNDPROC = m_pWNDPROC;
		HWND hWndHooked = m_hWndHooked;
		if( nMessage == WM_NCDESTROY ) 
		{
			DestroyChains( true );
//			if( pWndPermanent != NULL )
//			{
//				__PROF_UIS_MANAGE_STATE;
//				CWnd * pWndExamine =
//					CWnd::FromHandlePermanent( hWndHooked );
//				if( pWndExamine != pWndPermanent )
//					return 0L;
//			} // if( pWndPermanent != NULL )
			if( bEatNcDestroy )
				return 0L;
		} // if( nMessage == WM_NCDESTROY ) 
		LRESULT lResult =
			::CallWindowProc(
				pWNDPROC,
				hWndHooked,
				nMessage,
				wParam,
				lParam
				);
		return lResult;
	};

}; // struct CExtHookSink::HookChains_t

typedef
	CMap < HWND, HWND,
		CExtHookSink::HookChains_t *, CExtHookSink::HookChains_t * >
	HookChainsMap_t;

static HookChainsMap_t g_HookChainsMap;

LRESULT CALLBACK CExtHookSink::HookChains_t::g_HookWndProc(
	HWND hWnd,
	UINT nMessage,
	WPARAM wParam,
	LPARAM lParam
	)
{

LRESULT lResult = 0;

MSG & refMsgMfcCurr = AfxGetThreadState()->m_lastSentMsg;
MSG msgMfcSaved( refMsgMfcCurr );
	refMsgMfcCurr.hwnd    = hWnd;
	refMsgMfcCurr.message = nMessage;
	refMsgMfcCurr.wParam  = wParam;
	refMsgMfcCurr.lParam  = lParam;

CExtHookSink::HookChains_t * pHookChains = NULL;
	if( g_HookChainsMap.Lookup( hWnd, pHookChains ) )
	{
		ASSERT( pHookChains != NULL );
		ASSERT( pHookChains->m_hWndHooked == hWnd );
		lResult =
			pHookChains->HookChainsWindowProc(
				nMessage,
				wParam,
				lParam
				);  
		if( nMessage == WM_NCDESTROY ) 
			g_HookChainsMap.RemoveKey( hWnd );
	} // if( g_HookChainsMap.Lookup( hWnd, pHookChains ) )

	refMsgMfcCurr = msgMfcSaved;
	
	return lResult;
}

CExtHookSink::CExtHookSink(
	bool bEnableDetailedWndHooks // = true
	)
	: m_bEnableDetailedWndHooks( bEnableDetailedWndHooks )
	, m_bEatNcDestroy( false )
{
}

CExtHookSink::~CExtHookSink()
{
}

LRESULT CExtHookSink::OnHookWndMsgNextProcInvoke(
	UINT nMessage,
	WPARAM wParam,
	LPARAM lParam
	)
{
MSG & msgCurrMfc = AfxGetThreadState()->m_lastSentMsg;
	ASSERT( msgCurrMfc.hwnd != NULL );
	ASSERT( ::IsWindow( msgCurrMfc.hwnd ) );

CExtHookSink::HookChains_t * pHookChains = NULL;
	VERIFY(
		g_HookChainsMap.Lookup(
			msgCurrMfc.hwnd,
			pHookChains
			)
		);
	ASSERT( pHookChains != NULL );
	ASSERT( pHookChains->m_hWndHooked == msgCurrMfc.hwnd );
	ASSERT( pHookChains->m_pWNDPROC != NULL );

	return
		::CallWindowProc(
			pHookChains->m_pWNDPROC,
			msgCurrMfc.hwnd,
			nMessage,
			wParam,
			lParam
			);
}

LRESULT CExtHookSink::OnHookWndMsgNextProcCurrent(
	WPARAM wParam,
	LPARAM lParam
	)
{
MSG & msgCurrMfc = AfxGetThreadState()->m_lastSentMsg;
	ASSERT( msgCurrMfc.hwnd != NULL );
	ASSERT( ::IsWindow( msgCurrMfc.hwnd ) );

CExtHookSink::HookChains_t * pHookChains = NULL;
	VERIFY(
		g_HookChainsMap.Lookup(
			msgCurrMfc.hwnd,
			pHookChains
			)
		);
	ASSERT( pHookChains != NULL );
	ASSERT( pHookChains->m_hWndHooked == msgCurrMfc.hwnd );
	ASSERT( pHookChains->m_pWNDPROC != NULL );

	return
		::CallWindowProc(
			pHookChains->m_pWNDPROC,
			msgCurrMfc.hwnd,
			msgCurrMfc.message,
			wParam,
			lParam
			);
}

LRESULT CExtHookSink::OnHookWndMsgDefault()
{
MSG & msgCurrMfc = AfxGetThreadState()->m_lastSentMsg;
	ASSERT( msgCurrMfc.hwnd != NULL );
	ASSERT( ::IsWindow( msgCurrMfc.hwnd ) );

CExtHookSink::HookChains_t * pHookChains = NULL;
	VERIFY(
		g_HookChainsMap.Lookup(
			msgCurrMfc.hwnd,
			pHookChains
			)
		);
	ASSERT( pHookChains != NULL );
	ASSERT( pHookChains->m_hWndHooked == msgCurrMfc.hwnd );
	ASSERT( pHookChains->m_pWNDPROC != NULL );

	return
		::CallWindowProc(
			pHookChains->m_pWNDPROC,
			msgCurrMfc.hwnd,
			msgCurrMfc.message,
			msgCurrMfc.wParam,
			msgCurrMfc.lParam
			);
}

bool CExtHookSink::OnHookWndMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	UINT nMessage,
	WPARAM & wParam,
	LPARAM & lParam
	)
{
	lResult;
	hWndHooked;
	nMessage;
	wParam;
	lParam;

	if( !m_bEnableDetailedWndHooks )
		return false;

	switch( nMessage )
	{
	case WM_COMMAND:
		return
			OnHookCmdMsg(
				lResult,
				hWndHooked,
				HIWORD(wParam),
				LOWORD(wParam),
				(HWND)lParam
				);
	case WM_NOTIFY:
		return
			OnHookNotifyMsg(
				lResult,
				hWndHooked,
				(INT)wParam,
				(LPNMHDR)lParam
				);
	case WM_PAINT:
		return
			OnHookPaintMsg(
				lResult,
				hWndHooked,
				(HDC)wParam
				);
	case WM_ERASEBKGND:
		return
			OnHookEraseBackgroundMsg(
				lResult,
				hWndHooked,
				(HDC)wParam
				);
	case WM_PRINT:
	case WM_PRINTCLIENT:
		return
			OnHookPrintMsg(
				lResult,
				hWndHooked,
				(HDC)wParam
				);
	case WM_NCPAINT:
		return
			OnHookNcPaintMsg(
				lResult,
				hWndHooked,
				(HRGN)wParam
				);
	} // switch( nMessage )

	return false;
}

bool CExtHookSink::OnHookCmdMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	WORD wNotifyCode,
	WORD wID,
	HWND hWndCtrl
	)
{
	lResult;
	hWndHooked;
	wNotifyCode;
	wID;
	hWndCtrl;
	
	return false;
}

bool CExtHookSink::OnHookNotifyMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	INT nIdCtrl,
	LPNMHDR lpnmhdr
	)
{
	lResult;
	hWndHooked;
	nIdCtrl;
	lpnmhdr;
	
	return false;
}

bool CExtHookSink::OnHookPaintMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	HDC hDC
	)
{
	lResult;
	hWndHooked;
	hDC;
	
	return false;
}

bool CExtHookSink::OnHookEraseBackgroundMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	HDC hDC
	)
{
	lResult;
	hWndHooked;
	hDC;
	
	return false;
}

bool CExtHookSink::OnHookPrintMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	HDC hDC
	)
{
	lResult;
	hWndHooked;
	hDC;
	
	return false;
}

bool CExtHookSink::OnHookNcPaintMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	HRGN hRgnUpdate
	)
{
	lResult;
	hWndHooked;
	hRgnUpdate;
	
	return false;
}

void CExtHookSink::OnHookWndNcDestroy()
{
}

void CExtHookSink::OnHookWndAttach( HWND hWnd )
{
	ASSERT( hWnd != NULL );
	hWnd;
}

void CExtHookSink::OnHookWndDetach( HWND hWnd )
{
	ASSERT( hWnd != NULL );
	hWnd;
}

bool CExtHookSink::IsAutoDeleteHookWndSink()
{
	return false;
}

bool CExtHookSink::SetupHookWndSink(
	HWND hWnd,
	bool bRemove, // = false
	bool bAddToHead // = false
	)
{
	ASSERT( hWnd != NULL );
	if( hWnd == NULL )
		return false;

	ASSERT( bRemove || (!bRemove && ::IsWindow(hWnd)) );
	if( (!bRemove) && (!::IsWindow(hWnd) ) )
		return false;

CExtHookSink::HookChains_t * pHookChains = NULL;
	if( !g_HookChainsMap.Lookup( hWnd, pHookChains ) )
	{
		ASSERT( pHookChains == NULL );
	}
	else
	{
		ASSERT( pHookChains != NULL );
	}
	
	if( bRemove )
	{
		if( pHookChains == NULL )
			return true;
		INT pos =
			pHookChains->m_HookSinkArray.Find( this );
		if( pos < 0 )
			return true;

		OnHookWndDetach( hWnd );

		pHookChains->m_HookSinkArray.RemoveAt( pos );
		if( IsAutoDeleteHookWndSink() )
			delete this;
		
//if( pHookChains->m_HookSinkArray.GetSize() == 0 )
//	pHookChains->DestroyChains( true );

		return true;
	} // if( bRemove )
	
	if( pHookChains == NULL )
	{
		pHookChains =
			new CExtHookSink::HookChains_t( hWnd );
		g_HookChainsMap.SetAt( hWnd, pHookChains );
	} // if( pHookChains == NULL )
	else
	{
		INT pos =
			pHookChains->m_HookSinkArray.Find( this );
		if( pos >= 0 )
			return true;
	} // else from if( pHookChains == NULL )

	if( bAddToHead )
		pHookChains->m_HookSinkArray.AddHead( this );
	else
		pHookChains->m_HookSinkArray.AddTail( this );

	OnHookWndAttach( hWnd );

	return true;
}

ULONG CExtHookSink::SetupHookWndSinkToChilds(
	HWND hWnd,
	UINT * pDlgCtrlIDs, // = NULL
	ULONG nCountOfDlgCtrlIDs, // = 0
	bool bDeep // = false
	)
{
	ASSERT( hWnd != NULL );
	if( hWnd == NULL )
		return 0;

	ASSERT( ::IsWindow(hWnd) );
	if( !::IsWindow(hWnd) )
		return 0;
ULONG nCountOfHooks = 0;
	hWnd = ::GetWindow( hWnd, GW_CHILD );
	for( ; hWnd != NULL; hWnd = ::GetWindow( hWnd, GW_HWNDNEXT ) )
	{
		ASSERT(
			(nCountOfDlgCtrlIDs == 0 && pDlgCtrlIDs == NULL)
			|| (nCountOfDlgCtrlIDs > 0 && pDlgCtrlIDs != NULL)
			);
		bool bSetupHook = true;
		if( nCountOfDlgCtrlIDs > 0 && pDlgCtrlIDs != NULL )
		{
			bSetupHook = false;
			UINT nDlgCtrlID = ::GetDlgCtrlID( hWnd );
			for( ULONG i=0; i<nCountOfDlgCtrlIDs; i++ )
			{
				if( pDlgCtrlIDs[i] == nDlgCtrlID )
				{
					bSetupHook = true;
					break;
				}
			} // for( ULONG i=0; i<nCountOfDlgCtrlIDs; i++ )
		} // if( nCountOfDlgCtrlIDs > 0 && pDlgCtrlIDs != NULL )
		if( bSetupHook )
		{
			if(	SetupHookWndSink( hWnd ) )
				nCountOfHooks++;
			else
			{
				ASSERT( FALSE );
			}
		} // if( bSetupHook )
		if( bDeep )
			nCountOfHooks +=
				SetupHookWndSinkToChilds(
					hWnd,
					pDlgCtrlIDs,
					nCountOfDlgCtrlIDs,
					bDeep
					);
	} // for( ; hWnd != NULL; hWnd = ::GetWindow( hWnd, GW_HWNDNEXT ) )
	return nCountOfHooks;
}

void CExtHookSink::RemoveAllWndHooks()
{
HookedWndList_t _list;
	GetHookedWindows( _list );
POSITION pos = _list.GetHeadPosition();
	for( int nHwndIdx = 0; nHwndIdx < _list.GetCount(); nHwndIdx++ )
	{
		HWND hWndHooked = _list.GetNext( pos );
		VERIFY( SetupHookWndSink( hWndHooked, true ) );
	} // for( int nHwndIdx = 0; nHwndIdx < _list.GetCount(); nHwndIdx++ )
}

void CExtHookSink::GetHookedWindows( HookedWndList_t & _list )
{
	_list.RemoveAll();
POSITION posChains = g_HookChainsMap.GetStartPosition();
	for( ; posChains != NULL; )
	{
		CExtHookSink::HookChains_t * pHookChains = NULL;
		HWND hWndHooked = NULL;
		g_HookChainsMap.GetNextAssoc(
			posChains, hWndHooked, pHookChains );
		ASSERT( hWndHooked != NULL );
		ASSERT( pHookChains != NULL );
		ASSERT( pHookChains->m_hWndHooked == hWndHooked );
		if( pHookChains->m_HookSinkArray.Find(this) < 0 )
			continue;
		ASSERT( _list.Find(pHookChains->m_hWndHooked) == NULL );
		_list.AddTail( pHookChains->m_hWndHooked );
	} // for( ; pos != NULL; )
}

bool CExtHookSink::IsHookedWindow( HWND hWnd )
{
CExtHookSink::HookChains_t * pHookChains = NULL;
	if( !g_HookChainsMap.Lookup( hWnd, pHookChains ) )
		return false;
	ASSERT( pHookChains != NULL );
	ASSERT( pHookChains->m_hWndHooked == hWnd );
	if( pHookChains->m_HookSinkArray.Find(this) >= 0 )
		return true;
	return false;
}

CTypedPtrList < CPtrList, CExtHookSpy * > CExtHookSpy::g_listListeners;
HHOOK CExtHookSpy::g_hHookMouse = NULL;
HHOOK CExtHookSpy::g_hHookKeyboard = NULL;
bool CExtHookSpy::g_bListenersChanged = false;

CExtHookSpy::CExtHookSpy(
	bool bAutoRegister // = false
	)
{
	if( bAutoRegister )
		HookSpyRegister();
}

CExtHookSpy::~CExtHookSpy()
{
	HookSpyUnregister();
}

void CExtHookSpy::SHS_Hook( bool bHook )
{
	if( bHook )
	{
		if( g_hHookMouse == NULL )
		{
			g_hHookMouse =
				::SetWindowsHookEx(
					WH_MOUSE,
					SHS_HookMouseProc, 
					0,
					::GetCurrentThreadId()
					);
			ASSERT( g_hHookMouse != NULL );
		}
		if( g_hHookKeyboard == NULL )
		{
			g_hHookKeyboard =
				::SetWindowsHookEx(
					WH_KEYBOARD,
					SHS_HookKeyboardProc, 
					0,
					::GetCurrentThreadId()
					);
			ASSERT( g_hHookKeyboard != NULL );
		}
	} // if( bHook )
	else
	{
		if( g_hHookMouse != NULL )
		{
			::UnhookWindowsHookEx( g_hHookMouse );
			g_hHookMouse = NULL;
		}
		if( g_hHookKeyboard != NULL )
		{
			::UnhookWindowsHookEx( g_hHookKeyboard );
			g_hHookKeyboard = NULL;
		}
	} // else from if( bHook )
}

LRESULT CALLBACK CExtHookSpy::SHS_HookMouseProc(
	int nCode,      // hook code
	WPARAM wParam,  // message identifier
	LPARAM lParam   // mouse coordinates
	)
{
__PROF_UIS_MANAGE_STATE;
MOUSEHOOKSTRUCT * lpMS = (MOUSEHOOKSTRUCT*)lParam;
	ASSERT( lpMS != NULL );
	if( nCode == HC_ACTION )
	{
		POSITION pos = g_listListeners.GetHeadPosition();
		for( g_bListenersChanged = false; pos != NULL; )
		{
			CExtHookSpy * pHS = g_listListeners.GetNext( pos );
			ASSERT( pHS != NULL );
			switch( wParam )
			{
			case WM_MOUSEMOVE:
				if(	pHS->
						HSLL_OnMouseMove(
							lpMS->hwnd,
							0, // wParam,
							lpMS->pt // screen coordinates
							)
					)
					return 1; // eat!
			break;

			case WM_MOUSEWHEEL:
				if(	pHS->
						HSLL_OnMouseWheel(
							wParam,
							lParam
							)
					)
					return 1; // eat!
			break;

			case WM_NCLBUTTONDOWN:
			case WM_NCRBUTTONDOWN:
			case WM_NCMBUTTONDOWN:
			case WM_LBUTTONDOWN:
			case WM_RBUTTONDOWN:
			case WM_MBUTTONDOWN:

			case WM_NCLBUTTONUP:
			case WM_NCRBUTTONUP:
			case WM_NCMBUTTONUP:
			case WM_LBUTTONUP:
			case WM_RBUTTONUP:
			case WM_MBUTTONUP:
				if(	pHS->
						HSLL_OnMouseClick(
							lpMS->hwnd,
							UINT(wParam),
							0,
							lpMS->pt // screen coordinates
							)
					)
					return 1; // eat!
			break;
			} // switch( wParam )
			if( g_bListenersChanged )
			{
				g_bListenersChanged = false;
				pos = g_listListeners.GetHeadPosition();
			} // if( g_bListenersChanged )
		} // for( g_bListenersChanged = false; pos != NULL; )
		g_bListenersChanged = false;
	} // if( nCode == HC_ACTION )
	return
		::CallNextHookEx(
			g_hHookMouse,
			nCode,
			wParam,
			lParam
			);
}

LRESULT CALLBACK CExtHookSpy::SHS_HookKeyboardProc(
	int nCode,      // hook code
	WPARAM wParam,  // virtual-key code
	LPARAM lParam   // keystroke-message information
	)
{
__PROF_UIS_MANAGE_STATE;
	if( nCode == HC_ACTION )
	{
		POSITION pos = g_listListeners.GetHeadPosition();
		for( g_bListenersChanged = false; pos != NULL; )
		{
			CExtHookSpy * pHS = g_listListeners.GetNext( pos );
			ASSERT( pHS != NULL );
			if(	pHS->
					HSLL_OnKeyDown(
						UINT(wParam),
						LOWORD(lParam),
						HIWORD(lParam)
						)
				)
				return 1; // eat!
			if( g_bListenersChanged )
			{
				g_bListenersChanged = false;
				pos = g_listListeners.GetHeadPosition();
			} // if( g_bListenersChanged )
		} // for( g_bListenersChanged = false; pos != NULL; )
		g_bListenersChanged = false;
	} // if( nCode == HC_ACTION )
	return
		::CallNextHookEx(
			g_hHookKeyboard,
			nCode,
			wParam,
			lParam
			);
}

bool CExtHookSpy::HSLL_OnMouseWheel(
	WPARAM wParam,
	LPARAM lParam
	)
{
	wParam;
	if( ! g_PaintManager.m_bIsWin2000orLater )
		return false;
struct __SAME_AS_MOUSEHOOKSTRUCTEX
{
	MOUSEHOOKSTRUCT mhs;
	DWORD mouseData;
};
__SAME_AS_MOUSEHOOKSTRUCTEX * pMHEX =
		reinterpret_cast
			< __SAME_AS_MOUSEHOOKSTRUCTEX * >
				( lParam );
	ASSERT( pMHEX != NULL );
DWORD dwWheelDeltaAndZeroFlags =
		DWORD( pMHEX->mouseData ) & 0xFFFF0000;
MSG _msg;
	::memset( &_msg, 0, sizeof(MSG) );
	_msg.hwnd = pMHEX->mhs.hwnd;
	_msg.wParam = WPARAM(dwWheelDeltaAndZeroFlags);
	_msg.lParam = MAKELPARAM(pMHEX->mhs.pt.x,pMHEX->mhs.pt.y);
	_msg.message = WM_MOUSEWHEEL;
//	TRACE1( "wheel_msg = %x\n", _msg.message );
	if( HSLL_PreTranslateMessage( &_msg ) )
		return true;
	if( OnHookSpyPreTranslateMessage( &_msg ) )
		return true;
	if( OnHookSpyMouseWheelMsg( &_msg ) )
		return true;
	if( OnHookSpyPostProcessMessage( &_msg ) )
		return true;
	return false;
}

bool CExtHookSpy::HSLL_OnMouseMove(
	HWND hWnd,
	UINT nFlags,
	CPoint point
	)
{
	if(		hWnd == NULL
		||	(! ::IsWindow(hWnd) )
		||	(! ::GetCursorPos(&point) ) 
		)
		return false;
	::ScreenToClient( hWnd, &point );
MSG _msg;
	::memset( &_msg, 0, sizeof(MSG) );
	_msg.hwnd = hWnd;
	_msg.wParam = WPARAM(nFlags);
	_msg.lParam = MAKELPARAM( point.x, point.y );
	_msg.message = WM_MOUSEMOVE;
	_msg.pt = point;
	if( HSLL_PreTranslateMessage( &_msg ) )
		return true;
	if( ! HookSpyIsRegistered( this ) )
		return false;
	if( OnHookSpyPreTranslateMessage( &_msg ) )
		return true;
	if( ! HookSpyIsRegistered( this ) )
		return false;
	if( OnHookSpyMouseMoveMsg( &_msg ) )
		return true;
	if( ! HookSpyIsRegistered( this ) )
		return false;
	if( OnHookSpyPostProcessMessage( &_msg ) )
		return true;
	return false;
}

bool CExtHookSpy::HSLL_OnMouseClick(
	HWND hWnd,
	UINT nMessage,
	UINT nFlags,
	CPoint point
	)
{
MSG _msg;
	::memset( &_msg, 0, sizeof(MSG) );
	_msg.hwnd = hWnd;
	_msg.wParam = WPARAM(nFlags);
	_msg.lParam = MAKELPARAM(point.x,point.y);
	_msg.message = nMessage;
::ScreenToClient( hWnd, &point );
	_msg.pt = point;
	if( HSLL_PreTranslateMessage( &_msg ) )
		return true;
	if( ! HookSpyIsRegistered( this ) )
		return false;
	if( OnHookSpyPreTranslateMessage( &_msg ) )
		return true;
	if( ! HookSpyIsRegistered( this ) )
		return false;
	if( OnHookSpyMouseClickMsg( &_msg ) )
		return true;
	if( ! HookSpyIsRegistered( this ) )
		return false;
	if( OnHookSpyPostProcessMessage( &_msg ) )
		return true;
	return false;
}

bool CExtHookSpy::HSLL_OnKeyDown(
	UINT nChar,
	UINT nRepCnt,
	UINT nFlags
	)
{
MSG _msg;
	::memset( &_msg, 0, sizeof(MSG) );
	_msg.hwnd = ::GetFocus();
	_msg.wParam = WPARAM(nChar);
	_msg.lParam = MAKELPARAM(nRepCnt,nFlags);
	_msg.message =
		( ( nFlags & (KF_UP) ) != 0 )
			? WM_KEYUP
			: WM_KEYDOWN
			;
//	TRACE3(
//		"key_msg = %x (%s), flags = 0x%04X\n",
//		_msg.message,
//		(_msg.message == WM_KEYUP) ? "WM_KEYUP" : "WM_KEYDOWN",
//		nFlags
//		);
	if( HSLL_PreTranslateMessage( &_msg ) )
		return true;
	if( ! HookSpyIsRegistered( this ) )
		return false;
	if( OnHookSpyPreTranslateMessage( &_msg ) )
		return true;
	if( ! HookSpyIsRegistered( this ) )
		return false;
	if( OnHookSpyKeyMsg( &_msg ) )
		return true;
	if( ! HookSpyIsRegistered( this ) )
		return false;
	if( OnHookSpyPostProcessMessage( &_msg ) )
		return true;
	return false;
}

bool CExtHookSpy::HSLL_PreTranslateMessage(
	MSG * pMSG
	)
{
	ASSERT( pMSG != NULL );
	pMSG;
	return false;
}

bool CExtHookSpy::OnHookSpyPreTranslateMessage(
	MSG * pMSG
	)
{
	ASSERT( pMSG != NULL );
	pMSG;
	return false;
}

bool CExtHookSpy::OnHookSpyPostProcessMessage(
	MSG * pMSG
	)
{
	ASSERT( pMSG != NULL );
	pMSG;
	return false;
}

bool CExtHookSpy::OnHookSpyMouseWheelMsg(
	MSG * pMSG
	)
{
	ASSERT( pMSG != NULL );
	pMSG;
	return false;
}

bool CExtHookSpy::OnHookSpyMouseMoveMsg(
	MSG * pMSG
	)
{
	ASSERT( pMSG != NULL );
	pMSG;
	return false;
}

bool CExtHookSpy::OnHookSpyMouseClickMsg(
	MSG * pMSG
	)
{
	ASSERT( pMSG != NULL );
	pMSG;
	return false;
}

bool CExtHookSpy::OnHookSpyKeyMsg(
	MSG * pMSG
	)
{
	ASSERT( pMSG != NULL );
	pMSG;
	return false;
}

void CExtHookSpy::HookSpyRegister()
{
	HookSpyRegister( this );
}

void CExtHookSpy::HookSpyRegister( CExtHookSpy * pHS )
{
__PROF_UIS_MANAGE_STATE;
	if( pHS == NULL )
		return;
POSITION pos = g_listListeners.Find( pHS );
	if( pos != NULL )
		return;
INT nRegisteredCount = INT( g_listListeners.GetCount() );
	if( nRegisteredCount == 0 )
		SHS_Hook( true );
	g_listListeners.AddHead( pHS );
	g_bListenersChanged = true;
	ASSERT( (nRegisteredCount+1) == INT( g_listListeners.GetCount() ) );
}

void CExtHookSpy::HookSpyUnregister()
{
	HookSpyUnregister( this );
}

void CExtHookSpy::HookSpyUnregister( CExtHookSpy * pHS )
{
__PROF_UIS_MANAGE_STATE;
	if( pHS == NULL )
		return;
POSITION pos = g_listListeners.Find( pHS );
	if( pos == NULL )
		return;
	g_listListeners.RemoveAt( pos );
	g_bListenersChanged = true;
INT nRegisteredCount = INT( g_listListeners.GetCount() );
	if( nRegisteredCount == 0 )
		SHS_Hook( false );
}

bool CExtHookSpy::HookSpyIsRegistered() const
{
	return HookSpyIsRegistered( this );
}

bool CExtHookSpy::HookSpyIsRegistered( const CExtHookSpy * pHS )
{
	if( pHS == NULL )
		return false;
__PROF_UIS_MANAGE_STATE;
POSITION pos = g_listListeners.Find( (void*)pHS );
	if( pos == NULL )
		return false;
	return true;
}
