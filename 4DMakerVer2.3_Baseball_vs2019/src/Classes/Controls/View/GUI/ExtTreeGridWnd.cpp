// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_TREEGRIDWND)

#if (!defined __EXTTREEGRIDWND_H)
	#include <ExtTreeGridWnd.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

IMPLEMENT_DYNCREATE( CExtTreeGridCellNode, CExtGridCell );

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtTreeGridCellNode

CExtTreeGridCellNode::CExtTreeGridCellNode(
	CExtGridDataProvider * pDataProvider // = NULL
	)
	: CExtGCE < CExtGridCell > ( pDataProvider )
	, m_pNodeParent( NULL )
	, m_pNodeNext( NULL )
	, m_pNodePrev( NULL )
	, m_nIndentPx( ULONG(-1L) )
	, m_nContentWeightAll( 0 )
	, m_nContentWeightExpanded( 0 )
	, m_bExpanded( false )
	, m_nOptIndex( 0 )
{
}

CExtTreeGridCellNode::~CExtTreeGridCellNode()
{
CExtGridDataProvider * pDP = DataProviderGet();
	if( pDP != NULL && pDP->m_pOuterDataProvider != NULL )
	{
		CExtTreeGridDataProvider * pTreeDP =
			DYNAMIC_DOWNCAST( CExtTreeGridDataProvider, pDP->m_pOuterDataProvider );
		if( pTreeDP != NULL )
			pTreeDP->OnNodeRemoved( this );
	} // if( pDP != NULL && pDP->m_pOuterDataProvider != NULL )
}

ULONG CExtTreeGridCellNode::_ContentWeight_Get(
	bool bExpandedOnly
	) const
{
	ASSERT_VALID( this );
	ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
	return (bExpandedOnly ? m_nContentWeightExpanded : m_nContentWeightAll);
}

void CExtTreeGridCellNode::_ContentWeight_Increment(
	ULONG nWeight,
	bool bExpandedOnly
	)
{
	ASSERT_VALID( this );
	ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
	m_nContentWeightExpanded += nWeight;
	if( ! bExpandedOnly )
		m_nContentWeightAll += nWeight;
	ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if(		pNodeParent != NULL
		&&	pNodeParent->TreeNodeIsExpanded()
		)
		pNodeParent->_ContentWeight_Increment(
			nWeight,
			bExpandedOnly
			);
}

void CExtTreeGridCellNode::_ContentWeight_Decrement(
	ULONG nWeight,
	bool bExpandedOnly
	)
{
	ASSERT_VALID( this );
	ASSERT( m_nContentWeightExpanded >= nWeight );
	ASSERT( m_nContentWeightAll >= nWeight );
	ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
	m_nContentWeightExpanded -= nWeight;
	if( ! bExpandedOnly )
		m_nContentWeightAll -= nWeight;
	ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if(		pNodeParent != NULL
		&&	pNodeParent->TreeNodeIsExpanded()
		)
		pNodeParent->_ContentWeight_Decrement(
			nWeight,
			bExpandedOnly
			);
}

void CExtTreeGridCellNode::_ContentWeight_IncrementNonExpanded(
	ULONG nWeight
	)
{
	ASSERT_VALID( this );
	ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
	m_nContentWeightAll += nWeight;
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if( pNodeParent != NULL )
		pNodeParent->_ContentWeight_IncrementNonExpanded( nWeight );
}

void CExtTreeGridCellNode::_ContentWeight_DecrementNonExpanded(
	ULONG nWeight
	)
{
	ASSERT_VALID( this );
//	ASSERT( m_nContentWeightExpanded >= nWeight );
	ASSERT( m_nContentWeightAll >= nWeight );
//	ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
	m_nContentWeightAll -= nWeight;
//	ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if( pNodeParent != NULL )
		pNodeParent->_ContentWeight_DecrementNonExpanded( nWeight );
}

void CExtTreeGridCellNode::_ContentWeight_Adjust()
{
	ASSERT_VALID( this );
	if( m_arrChildren.GetSize() == 0 )
	{
		m_nContentWeightAll = m_nContentWeightExpanded = 0;
		if( TreeNodeGetParent() != NULL )
			m_bExpanded = false;
	}
	else
	{
		if( TreeNodeIsExpanded() )
		{
			m_nContentWeightExpanded = _ContentWeight_CalcVisible();
			ASSERT( m_nContentWeightExpanded <= m_nContentWeightAll );
		}
		else
			m_nContentWeightExpanded = 0;
	}
	if( m_pNodeParent != NULL )
		m_pNodeParent->_ContentWeight_Adjust();
}

ULONG CExtTreeGridCellNode::_ContentWeight_CalcVisible() const
{
	ASSERT_VALID( this );
	//ASSERT( TreeNodeIsExpanded() );
ULONG nCount = TreeNodeGetChildCount();
ULONG nCalcVal = nCount;
	for( ULONG nIdx = 0; nIdx < nCount; nIdx++ )
	{
		const CExtTreeGridCellNode * pNode =
			TreeNodeGetChildAt( nIdx );
		ASSERT_VALID( pNode );
		if( pNode->TreeNodeIsExpanded() )
			nCalcVal += pNode->_ContentWeight_CalcVisible();
	} // for( ULONG nIdx = 0; nIdx < nCount; nIdx++ )
	return nCalcVal;
}

void CExtTreeGridCellNode::_Content_FillVisibleArray(
	CExtTreeGridCellNode::NodeArr_t & arr,
	ULONG & nOffset
	)
{
	ASSERT_VALID( this );
ULONG nCount = TreeNodeGetChildCount();
	ASSERT( (nOffset+nCount) <= ULONG(arr.GetSize()) );
	for( ULONG nIdx = 0; nIdx < nCount; nIdx++ )
	{
		CExtTreeGridCellNode * pNode =
			TreeNodeGetChildAt( nIdx );
		ASSERT_VALID( pNode );
		arr.SetAt( nOffset++, pNode );
		if( pNode->TreeNodeIsExpanded() )
			pNode->_Content_FillVisibleArray( arr, nOffset );
	} // for( ULONG nIdx = 0; nIdx < nCount; nIdx++ )
}

CExtTreeGridCellNode::operator HTREEITEM () const
{
#ifdef _DEBUG
	if( this != NULL )
	{
		ASSERT_VALID( this );
	}
#endif // _DEBUG
	return reinterpret_cast < HTREEITEM > ( LPVOID(this) );
}

CExtTreeGridCellNode * CExtTreeGridCellNode::FromHTREEITEM(
	HTREEITEM hTreeItem
	)
{
	if( hTreeItem == NULL )
		return NULL;
CExtTreeGridCellNode * pCell =
		reinterpret_cast < CExtTreeGridCellNode * > ( hTreeItem );
	ASSERT_VALID( pCell );
	ASSERT_KINDOF( CExtTreeGridCellNode, pCell );
	return pCell;
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetRoot()
{
	ASSERT_VALID( this );
CExtTreeGridCellNode * pNode = this;
	for( ; true; )
	{
		CExtTreeGridCellNode * pNodeParent = pNode->TreeNodeGetParent();
		if( pNodeParent == NULL )
			break;
		pNode = pNodeParent;
	} // for( ; true; )
	return pNode;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetRoot() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridCellNode * > ( this ) )
		-> TreeNodeGetRoot();
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetParent()
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pNodeParent != NULL )
	{
		ASSERT_VALID( m_pNodeParent );
	}
#endif // _DEBUG
	return m_pNodeParent;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetParent() const
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pNodeParent != NULL )
	{
		ASSERT_VALID( m_pNodeParent );
	}
#endif // _DEBUG
	return m_pNodeParent;
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetNext(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper
	)
{
	ASSERT_VALID( this );
	if( (! bSiblingOnly )
		&& bWalkDeeper
		&& TreeNodeGetChildCount() > 0
		&& ( (! bExpandedWalk ) || TreeNodeIsExpanded() )
		)
		return TreeNodeGetChildAt( 0 );
	if( m_pNodeNext != NULL )
	{
		ASSERT_VALID( m_pNodeNext );
		return m_pNodeNext;
	}
	if( bSiblingOnly )
		return NULL;
	if( ! bExpandedWalk )
		return NULL;
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if(		pNodeParent == NULL
		||	pNodeParent->TreeNodeGetParent() == NULL
		)
		return NULL;
CExtTreeGridCellNode * pNode = pNodeParent;
	for( ; pNode != NULL; )
	{
		CExtTreeGridCellNode * pNodeX = pNode->TreeNodeGetNext( true, false, false );
		if( pNodeX != NULL )
			return pNodeX;
		pNode = pNode->TreeNodeGetParent();
		if(		pNodeParent == NULL
//			||	pNodeParent->TreeNodeGetParent() == NULL
			)
			return NULL;
	}
	return NULL;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetNext(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridCellNode * > ( this ) )
		-> TreeNodeGetNext( bSiblingOnly, bExpandedWalk, bWalkDeeper );
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetPrev(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper
	)
{
	ASSERT_VALID( this );
	bWalkDeeper;
	if( m_pNodePrev != NULL )
	{
		ASSERT_VALID( m_pNodePrev );
		if( bSiblingOnly )
			return m_pNodePrev;
		CExtTreeGridCellNode * pNode = m_pNodePrev;
		for( ; pNode->TreeNodeGetChildCount() > 0; )
		{
			if( bExpandedWalk && (! pNode->TreeNodeIsExpanded() )  )
				return pNode;
			pNode = pNode->TreeNodeGetChildAt( pNode->TreeNodeGetChildCount() - 1 );
			ASSERT_VALID( pNode );
		}
		return pNode;
	}
	if( bSiblingOnly )
		return NULL;
CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if(		pNodeParent == NULL
		||	pNodeParent->TreeNodeGetParent() == NULL
		)
		return NULL;
	return pNodeParent;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetPrev(
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridCellNode * > ( this ) )
		-> TreeNodeGetPrev( bSiblingOnly, bExpandedWalk, bWalkDeeper );
}

ULONG CExtTreeGridCellNode::TreeNodeGetSiblingIndex() const
{
	ASSERT_VALID( this );
const CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if( pNodeParent == NULL )
		return 0;
//ULONG nCount = pNodeParent->TreeNodeGetChildCount();
//	for( ULONG nIdx = 0; nIdx < nCount; nIdx ++ )
//	{
//		const CExtTreeGridCellNode * pNode =
//			pNodeParent->TreeNodeGetChildAt( nIdx );
//		ASSERT_VALID( pNode );
//		if( pNode == this )
//			return nIdx;
//	} // for( ULONG nIdx = 0; nIdx < nCount; nIdx ++ )
//	ASSERT( FALSE );
//	return 0;
	return m_nOptIndex;
}

ULONG CExtTreeGridCellNode::TreeNodeGetChildCount() const
{
	ASSERT_VALID( this );
	return ULONG(m_arrChildren.GetSize());
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetChildAt( ULONG nPos )
{
	ASSERT_VALID( this );
	ASSERT( nPos < ULONG(m_arrChildren.GetSize()) );
CExtTreeGridCellNode * pNode = m_arrChildren.GetAt( nPos );
	ASSERT_VALID( pNode );
	ASSERT( pNode->m_nOptIndex == nPos );
	return pNode;
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetChildAt( ULONG nPos ) const
{
	ASSERT_VALID( this );
	ASSERT( nPos < ULONG(m_arrChildren.GetSize()) );
CExtTreeGridCellNode * pNode = m_arrChildren.GetAt( nPos );
	ASSERT_VALID( pNode );
	ASSERT( pNode->m_nOptIndex == nPos );
	return pNode;
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetFirstSibling()
{
	ASSERT_VALID( this );
	if( m_pNodeParent == NULL )
		return NULL;
	ASSERT_VALID( m_pNodeParent );
	return m_pNodeParent->TreeNodeGetFirstChild();
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetFirstSibling() const
{
	ASSERT_VALID( this );
	if( m_pNodeParent == NULL )
		return NULL;
	ASSERT_VALID( m_pNodeParent );
	return m_pNodeParent->TreeNodeGetFirstChild();
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetLastSibling()
{
	ASSERT_VALID( this );
	if( m_pNodeParent == NULL )
		return NULL;
	ASSERT_VALID( m_pNodeParent );
	return m_pNodeParent->TreeNodeGetLastChild();
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetLastSibling() const
{
	ASSERT_VALID( this );
	if( m_pNodeParent == NULL )
		return NULL;
	ASSERT_VALID( m_pNodeParent );
	return m_pNodeParent->TreeNodeGetLastChild();
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetFirstChild()
{
	ASSERT_VALID( this );
	return TreeNodeGetChildAt( 0 );
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetFirstChild() const
{
	ASSERT_VALID( this );
	return TreeNodeGetChildAt( 0 );
}

CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetLastChild()
{
	ASSERT_VALID( this );
	return TreeNodeGetChildAt( TreeNodeGetChildCount() - 1 );
}

const CExtTreeGridCellNode * CExtTreeGridCellNode::TreeNodeGetLastChild() const
{
	ASSERT_VALID( this );
	return TreeNodeGetChildAt( TreeNodeGetChildCount() - 1 );
}

ULONG CExtTreeGridCellNode::TreeNodeIndentCompute() const
{
	ASSERT_VALID( this );
ULONG nCalcIndent = 0;
const CExtTreeGridCellNode * pNode = TreeNodeGetParent();
	for( ; pNode != NULL; pNode = pNode->TreeNodeGetParent() )
	{
		ASSERT_VALID( pNode );
		nCalcIndent ++;
	} // for( ; pNode != NULL; pNode = pNode->TreeNodeGetParent() )
	return nCalcIndent;
}

ULONG CExtTreeGridCellNode::TreeNodeIndentPxGet() const
{
	ASSERT_VALID( this );
	return m_nIndentPx;
}

void CExtTreeGridCellNode::TreeNodeIndentPxSet(
	ULONG nIndentPx
	)
{
	ASSERT_VALID( this );
	m_nIndentPx = nIndentPx;
}

CExtTreeGridCellNode::e_expand_box_shape_t CExtTreeGridCellNode::TreeNodeGetExpandBoxShape() const
{
	ASSERT_VALID( this );
	if( TreeNodeGetChildCount() == 0 )
		return __EEBS_NONE;
	if( TreeNodeGetParent() == NULL )
		__EEBS_EXPANDED;
	return m_bExpanded ? __EEBS_EXPANDED : __EEBS_COLLAPSED;
}

bool CExtTreeGridCellNode::TreeNodeIsDisplayed() const
{
	ASSERT_VALID( this );
const CExtTreeGridCellNode * pNode = this;
	for( ; true; )
	{
		const CExtTreeGridCellNode * pNodeParent =
			pNode->TreeNodeGetParent();
		if( pNodeParent == NULL )
			return true;
		ASSERT_VALID( pNodeParent );
		pNode = pNodeParent;
		if( ! pNode->TreeNodeIsExpanded() )
			return false;
	} // for( ; true; )
	return false;
}

void CExtTreeGridCellNode::TreeNodeMarkExpanded(
	bool bExpand
	)
{
	ASSERT_VALID( this );
	ASSERT( TreeNodeGetParent() != NULL || bExpand );
	m_bExpanded = bExpand;
}

bool CExtTreeGridCellNode::TreeNodeIsExpanded() const
{
	ASSERT_VALID( this );
	if( TreeNodeGetParent() == NULL )
		true;
	return m_bExpanded;
}

ULONG CExtTreeGridCellNode::TreeNodeCalcOffset(
	bool bExpandedOnly
	) const
{
	ASSERT_VALID( this );
const CExtTreeGridCellNode * pNodeParent = TreeNodeGetParent();
	if( pNodeParent == NULL )
		return 0;
ULONG nOffset = 0;
const CExtTreeGridCellNode * pNodeCompute = this;
	for( ;	pNodeCompute != NULL; )
	{
		const CExtTreeGridCellNode * pNode =
			pNodeCompute->TreeNodeGetPrev( true, false, false );
		for(	; pNode != NULL;
				pNode = pNode->TreeNodeGetPrev( true, false, false )
			)
		{
			ASSERT_VALID( pNode );
			nOffset ++;
			ULONG nContentWeight = pNode->_ContentWeight_Get( bExpandedOnly );
			nOffset += nContentWeight;
		}
		pNodeParent = pNodeCompute->TreeNodeGetParent();
		ASSERT_VALID( pNodeParent );
		if( pNodeParent->TreeNodeGetParent() == NULL )
			break;
		nOffset ++;
		pNodeCompute = pNodeParent;
	}
	return nOffset;
}

/////////////////////////////////////////////////////////////////////////////
// CExtTreeGridDataProvider

IMPLEMENT_DYNCREATE( CExtTreeGridDataProvider, CExtGridDataProvider );

CExtTreeGridDataProvider::CExtTreeGridDataProvider()
	: m_pCellRoot( NULL )
	, m_pTreeNodeDefaultRTC( RUNTIME_CLASS( CExtTreeGridCellNode ) )
	, m_nIndentPxDefault( __EXT_MGC_TREE_GRID_DEFAULT_LEVEL_INDENT )
	, m_nReservedColCount( 1L )
	, m_nReservedRowCount( 1L )
	, m_pTreeGridWnd( NULL )
{
	m_DP.m_pOuterDataProvider = this;
}

CExtTreeGridDataProvider::~CExtTreeGridDataProvider()
{
	m_DP.m_pOuterDataProvider = NULL;
	m_pTreeGridWnd = NULL;
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::TreeNodeGetRoot()
{
	ASSERT_VALID( this );
	return _Tree_NodeGetRoot();
}

const CExtTreeGridCellNode * CExtTreeGridDataProvider::TreeNodeGetRoot() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridDataProvider * > ( this ) )
		-> TreeNodeGetRoot();
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::TreeNodeGetByVisibleRowIndex( ULONG nRowNo )
{
	ASSERT_VALID( this );
	return _Tree_NodeGetByVisibleRowIndex( nRowNo );
}

const CExtTreeGridCellNode * CExtTreeGridDataProvider::TreeNodeGetByVisibleRowIndex( ULONG nRowNo ) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridDataProvider * > ( this ) )
		-> TreeNodeGetByVisibleRowIndex( nRowNo );
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::_Tree_NodeGetByVisibleRowIndex( ULONG nRowNo )
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	nRowNo = _Tree_MapRowToCache( nRowNo );
//ULONG nReservedRowCount = 0;
//	_DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
//	nRowNo += nReservedRowCount - 1;
CExtTreeGridCellNode * pNode =
		STATIC_DOWNCAST(
			CExtTreeGridCellNode,
			_DP.CellGet( 0, nRowNo )
			);
	ASSERT_VALID( pNode );
	return pNode;
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::_Tree_NodeGetRoot()
{
	ASSERT_VALID( this );
	if( m_pCellRoot == NULL )
		_Tree_GetCacheDP(); // init
	ASSERT_VALID( m_pCellRoot );
	return m_pCellRoot;
}

ULONG CExtTreeGridDataProvider::TreeGetDisplayedCount() const
{
	ASSERT_VALID( this );
	return _Tree_GetDisplayedCount();
}

void CExtTreeGridDataProvider::OnNodeRemoved(
	CExtTreeGridCellNode * pNode
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pNode );
	if( m_pTreeGridWnd != NULL )
	{
		ASSERT_VALID( m_pTreeGridWnd );
		m_pTreeGridWnd->OnTreeGridNodeRemoved( pNode );
	} // if( m_pTreeGridWnd != NULL )
}

ULONG CExtTreeGridDataProvider::TreeGetRowIndentPx( ULONG nRowNo ) const
{
	ASSERT_VALID( this );
//ULONG nReservedRowCount = 0;
//	CacheReservedCountsGet( NULL, &nReservedRowCount );
//	nRowNo += nReservedRowCount;
	return
		( const_cast < CExtTreeGridDataProvider * > ( this ) )
		-> _Tree_GetRowIndentPx( nRowNo );
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::TreeNodeInsert(
	CExtTreeGridCellNode * pNodeParent, // = NULL // if NULL - root
	ULONG nIdxInsertBefore, // = (ULONG(-1)) // if (ULONG(-1)) - insert to end
	ULONG nInsertCount // = 1
	) // returns pointer to first inserted
{
	ASSERT_VALID( this );
	return
		_Tree_NodeInsert(
			pNodeParent,
			nIdxInsertBefore,
			nInsertCount
			);
}

ULONG CExtTreeGridDataProvider::_Tree_NodeRemove(
	CExtTreeGridCellNode * pNode,
	bool bChildrenOnly // = false
	) // returns count of removed items
{
	ASSERT_VALID( this );
	ASSERT_VALID( pNode );
	ASSERT_VALID( m_pCellRoot );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nReservedRowCount = 0;
	 _DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
ULONG nAdjustOffset = 1;
	if( LPVOID(pNode) == LPVOID(m_pCellRoot) )
	{
		bChildrenOnly = true;
		nAdjustOffset = 0;
	} // if( LPVOID(pNode) == LPVOID(m_pCellRoot) )
ULONG	nCountRemoved = 0,
		nIdx,
		nChildrenCount = pNode->TreeNodeGetChildCount();
	for( nIdx = 0; nIdx < nChildrenCount; nIdx++ )
	{
		CExtTreeGridCellNode * pChildNode =
			pNode->TreeNodeGetChildAt( nIdx );
		ASSERT_VALID( pChildNode );
		ULONG nSubCountRemoved =
			_Tree_NodeRemove(
				pChildNode,
				true
				);
		nCountRemoved += nSubCountRemoved;
	} // for( nIdx = 0; nIdx < nChildrenCount; nIdx++ )
ULONG nContentWeight = pNode->_ContentWeight_Get( false );
ULONG nOffset = pNode->TreeNodeCalcOffset( false );
bool bDisplayed = pNode->TreeNodeIsDisplayed();
bool bExpanded = pNode->TreeNodeIsExpanded();
ULONG nVisibleOffset = 0;
	if( bDisplayed )
		nVisibleOffset = pNode->TreeNodeCalcOffset( true );
	if( nContentWeight > 0 )
	{
// #ifdef _DEBUG
// 		if( LPVOID(pNode) != LPVOID(m_pCellRoot) )
// 		{
// 			CExtTreeGridCellNode * pDebugTestNode = m_arrGridRef.GetAt( nOffset );
// 			ASSERT_VALID( pDebugTestNode );
// 			ASSERT( LPCVOID(pDebugTestNode) == LPCVOID(pNode) );
// 		} // if( LPVOID(pNode) == LPVOID(m_pCellRoot) )
// #endif // _DEBUG
		m_arrGridRef.RemoveAt(
			nOffset + nAdjustOffset,
			nContentWeight
			);
		if( bDisplayed && bExpanded )
		{
#ifdef _DEBUG
			if( LPVOID(pNode) != LPVOID(m_pCellRoot) )
			{
				CExtTreeGridCellNode * pDebugTestNode = m_arrGridVis.GetAt( nVisibleOffset );
				ASSERT_VALID( pDebugTestNode );
				ASSERT( LPCVOID(pDebugTestNode) == LPCVOID(pNode) );
			} // if( LPVOID(pNode) == LPVOID(m_pCellRoot) )
#endif // _DEBUG
			ULONG nVisibleContentWeight =
				pNode->_ContentWeight_CalcVisible();
			m_arrGridVis.RemoveAt(
				nVisibleOffset + nAdjustOffset,
				nVisibleContentWeight
				);
			pNode->_ContentWeight_Decrement( nVisibleContentWeight, false );
			pNode->m_arrChildren.RemoveAll();
		} // if( bDisplayed && bExpanded )
		else
		{
			pNode->m_arrChildren.RemoveAll();
			pNode->_ContentWeight_DecrementNonExpanded( nContentWeight );
			pNode->_ContentWeight_Adjust();
		} // else from if( bDisplayed && bExpanded )
		ASSERT( pNode->_ContentWeight_Get(true) == 0 );
		ASSERT( pNode->_ContentWeight_Get(false) == 0 );
		nCountRemoved += nContentWeight;
		VERIFY( _DP.RowRemove( nOffset + nAdjustOffset + nReservedRowCount, nContentWeight ) );
	} // if( nContentWeight > 0 )
	if( ! bChildrenOnly )
	{
		CExtTreeGridCellNode * pNodeNext = pNode->m_pNodeNext;
		CExtTreeGridCellNode * pNodePrev = pNode->m_pNodePrev;
		CExtTreeGridCellNode * pNodeParent = pNode->m_pNodeParent;
		ASSERT_VALID( pNodeParent );
		ULONG nSiblingIdx = pNode->TreeNodeGetSiblingIndex();
		if( pNodeNext != NULL )
		{
			ASSERT_VALID( pNodeNext );
			ASSERT( LPCVOID(pNodeNext->m_pNodePrev) == LPCVOID(pNode) );
			pNodeNext->m_pNodePrev = pNodePrev;
		} // if( pNodeNext != NULL )
		if( pNodePrev != NULL )
		{
			ASSERT_VALID( pNodePrev );
			ASSERT( LPCVOID(pNodePrev->m_pNodeNext) == LPCVOID(pNode) );
			pNodePrev->m_pNodeNext = pNodeNext;
		} // if( pNodePrev != NULL )
		pNodeParent->m_arrChildren.RemoveAt( nSiblingIdx );
		ULONG nResetIdx = nSiblingIdx, nResetCnt = ULONG( pNodeParent->m_arrChildren.GetSize() );
		for( ; nResetIdx < nResetCnt; nResetIdx++ )
		{
			CExtTreeGridCellNode * pNode = (CExtTreeGridCellNode *)
				pNodeParent->m_arrChildren.GetAt( nResetIdx );
			ASSERT_VALID( pNode );
			ASSERT_KINDOF( CExtTreeGridCellNode, pNode );
			pNode->m_nOptIndex = nResetIdx;
		}
		if( bDisplayed )
		{
#ifdef _DEBUG
			if( LPVOID(pNode) != LPVOID(m_pCellRoot) )
			{
				CExtTreeGridCellNode * pDebugTestNode = m_arrGridVis.GetAt( nVisibleOffset );
				ASSERT_VALID( pDebugTestNode );
				ASSERT( LPCVOID(pDebugTestNode) == LPCVOID(pNode) );
			} // if( LPVOID(pNode) == LPVOID(m_pCellRoot) )
#endif // _DEBUG
			m_arrGridVis.RemoveAt( nVisibleOffset );
			pNodeParent->_ContentWeight_Decrement( 1, false );
		} // if( bDisplayed && bExpanded )
		else
		{
			pNodeParent->_ContentWeight_DecrementNonExpanded( 1 );
			pNodeParent->_ContentWeight_Adjust();
		} // else from if( bDisplayed && bExpanded )
		VERIFY( _DP.RowRemove( nOffset + nReservedRowCount ) );
	} // if( ! bChildrenOnly )
	return nCountRemoved;
}

CRuntimeClass * CExtTreeGridDataProvider::_Tree_NodeGetRTC()
{
	ASSERT_VALID( this );
	return m_pTreeNodeDefaultRTC;
}

void CExtTreeGridDataProvider::_Tree_NodeAdjustProps(
	CExtTreeGridCellNode * pNode
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pNode );
	ASSERT_KINDOF( CExtTreeGridCellNode, pNode );
	//pNode->TreeNodeIndentPxSet( m_nIndentPxDefault );
	pNode;
}

ULONG CExtTreeGridDataProvider::TreeNodeRemove(
	CExtTreeGridCellNode * pNode,
	bool bChildrenOnly // = false
	) // returns count of removed items
{
	ASSERT_VALID( this );
	ASSERT_VALID( pNode );
	return
		_Tree_NodeRemove(
			pNode,
			bChildrenOnly
			);
}

bool CExtTreeGridDataProvider::TreeNodeExpand(
	CExtTreeGridCellNode * pNode,
	INT nActionTVE // = TVE_TOGGLE // TVE_COLLAPSE, TVE_EXPAND, TVE_TOGGLE only
	)
{
	ASSERT_VALID( this );
	ASSERT(
			nActionTVE == TVE_COLLAPSE
		||	nActionTVE == TVE_EXPAND
		||	nActionTVE == TVE_TOGGLE
		);
	return
		_Tree_NodeExpand(
			pNode,
			nActionTVE
			);
}

ULONG CExtTreeGridDataProvider::_Tree_GetRowIndentPx( ULONG nRowNo )
{
	ASSERT_VALID( this );
	ASSERT( nRowNo < RowCountGet() );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nRowCountReservedForTree = _Tree_ReservedRowsGet();
//	nRowNo += nRowCountReservedForTree;
ULONG nReservedRowCount = 0;
	_DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
	nRowNo += nReservedRowCount - nRowCountReservedForTree;
	nRowNo = _Tree_MapRowToCache( nRowNo );
//	nRowNo += nRowCountReservedForTree;
CExtTreeGridCellNode * pNode =
		STATIC_DOWNCAST(
			CExtTreeGridCellNode,
			_DP.CellGet( 0, nRowNo )
			);
	ASSERT_VALID( pNode );
CExtTreeGridCellNode * pNodeRoot = _Tree_NodeGetRoot();
	ASSERT_VALID( pNodeRoot );
ULONG nIndentPx = pNode->TreeNodeIndentPxGet();
	if( nIndentPx == ULONG(-1L) )
		nIndentPx = m_nIndentPxDefault;
	for( ; true; )
	{
		pNode = pNode->TreeNodeGetParent();
		ASSERT_VALID( pNode );
		if( LPCVOID(pNodeRoot) == LPCVOID(pNode) )
			break;
		ULONG nLevel = pNode->TreeNodeIndentPxGet();
		if( nLevel == ULONG(-1L) )
			nLevel = m_nIndentPxDefault;
		nIndentPx += nLevel;
	} // for( ; true; )
	return nIndentPx;
}

CExtTreeGridCellNode * CExtTreeGridDataProvider::_Tree_NodeInsert(
	CExtTreeGridCellNode * pNodeParent,
	ULONG nIdxInsertBefore, // = (ULONG(-1)) // if (ULONG(-1)) - insert to end
	ULONG nInsertCount // = 1
	) // returns pointer to first inserted
{
	ASSERT_VALID( this );
	if( nInsertCount == 0 )
		return NULL;
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
CExtTreeGridCellNode * pNodeRoot = _Tree_NodeGetRoot();
	if( pNodeParent == NULL )
	{
		pNodeParent = pNodeRoot;
		ASSERT_VALID( pNodeParent );
	} // if( pNodeParent == NULL )
	else
	{
		ASSERT_VALID( pNodeParent );
		ASSERT( LPCVOID(pNodeParent->DataProviderGet()) == LPCVOID(&_DP) );
	} // else from if( pNodeParent == NULL )
ULONG nParentChildCount = pNodeParent->TreeNodeGetChildCount();
	if(		nIdxInsertBefore == ULONG(-1)
		||	nIdxInsertBefore > nParentChildCount
		)
		nIdxInsertBefore = nParentChildCount;

ULONG nReservedRowCount = 0;
	 _DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
ULONG nAdditionalOffsetTotal = 0;
ULONG nAdditionalOffsetVisible = 0;
ULONG nVisibleOffset = 0;
ULONG nInsertOffset = nReservedRowCount;
CExtTreeGridCellNode * pNodeCompute = pNodeParent;
	if( nIdxInsertBefore != 0 )
	{
		ASSERT( nIdxInsertBefore <= nParentChildCount );
		if( nIdxInsertBefore == nParentChildCount )
		{
			nAdditionalOffsetTotal =
				pNodeParent->_ContentWeight_Get( false );
			nAdditionalOffsetVisible =
				pNodeParent->_ContentWeight_Get( true );
			if( pNodeParent != pNodeRoot )
			{
				nAdditionalOffsetTotal ++;
				nAdditionalOffsetVisible ++;
			}
		}
		else
		{
			pNodeCompute =
				pNodeParent->TreeNodeGetChildAt(
					nIdxInsertBefore
					);
		}
	}
	else
	{
		if( pNodeParent != pNodeRoot )
		{
			nAdditionalOffsetTotal = 1;
			nAdditionalOffsetVisible = 1;
		}
	}
	nInsertOffset += pNodeCompute->TreeNodeCalcOffset( false );
	if(		pNodeParent->TreeNodeIsDisplayed()
		&&	pNodeParent->TreeNodeIsExpanded()
		)
		nVisibleOffset +=
			pNodeCompute->TreeNodeCalcOffset( true )
			+ nAdditionalOffsetVisible
			;
	else
		nVisibleOffset = ULONG(-1);
	nInsertOffset += nAdditionalOffsetTotal;


	_DP.RowInsert( nInsertOffset, nInsertCount );
CExtTreeGridCellNode * pNodeRetVal = NULL;
ULONG nIdx = 0;
	for( ; nIdx < nInsertCount; nIdx++ )
	{
		ULONG nEffectiveIdx = nInsertOffset + nIdx;
		CExtTreeGridCellNode * pNodeCurr =
			STATIC_DOWNCAST(
				CExtTreeGridCellNode,
				_DP.CellGet(
					0,
					nEffectiveIdx,
					_Tree_NodeGetRTC()
					)
				);
		ASSERT_VALID( pNodeCurr );
		_Tree_NodeAdjustProps( pNodeCurr );
		m_arrGridRef.InsertAt(
			nEffectiveIdx - nReservedRowCount,
			pNodeCurr
			);
		if( nVisibleOffset != ULONG(-1) )
			m_arrGridVis.InsertAt(
				nVisibleOffset + nIdx,
				pNodeCurr
				);
		pNodeCurr->m_pNodeParent = pNodeParent;
		ULONG nSiblingIdx = nIdxInsertBefore + nIdx;
		pNodeParent->m_arrChildren.InsertAt(
			nSiblingIdx,
			pNodeCurr,
			1
			);
		pNodeCurr->m_nOptIndex = nSiblingIdx;
		if( nIdx == 0 )
			pNodeRetVal = pNodeCurr;
		CExtTreeGridCellNode * pNodePrev = NULL, * pNodeNext = NULL;
		if( nSiblingIdx > 0 )
		{
			pNodePrev = pNodeParent->m_arrChildren.GetAt( nSiblingIdx - 1 );
			ASSERT_VALID( pNodePrev );
		} // if( nSiblingIdx > 0 )
		if( nSiblingIdx < ULONG(pNodeParent->m_arrChildren.GetSize()-1) )
		{
			pNodeNext = pNodeParent->m_arrChildren.GetAt( nSiblingIdx + 1 );
			ASSERT_VALID( pNodeNext );
		} // if( nSiblingIdx < ULONG(pNodeParent->m_arrChildren.GetSize()-1) )
		if( pNodePrev != NULL )
		{
			pNodePrev->m_pNodeNext = pNodeCurr;
			pNodeCurr->m_pNodePrev = pNodePrev;
		} // if( pNodePrev != NULL )
		if( pNodeNext != NULL )
		{
			pNodeNext->m_pNodePrev = pNodeCurr;
			pNodeCurr->m_pNodeNext = pNodeNext;
		} // if( pNodeNext != NULL )
	} // for( ; nIdx < nInsertCount; nIdx++ )

ULONG nResetIdx, nResetCnt = ULONG(pNodeParent->m_arrChildren.GetSize());
	for( nResetIdx = nIdxInsertBefore; nResetIdx < nResetCnt; nResetIdx++ )
	{
		CExtTreeGridCellNode * pNode = (CExtTreeGridCellNode *)
			pNodeParent->m_arrChildren.GetAt( nResetIdx );
		ASSERT_VALID( pNode );
		ASSERT_KINDOF( CExtTreeGridCellNode, pNode );
		pNode->m_nOptIndex = nResetIdx;
	} // for( nResetIdx = nIdxInsertBefore; nResetIdx < nResetCnt; nResetIdx++ )
	
	if( nVisibleOffset != ULONG(-1) )
		pNodeParent->_ContentWeight_Increment( nInsertCount, false );
	else
		pNodeParent->_ContentWeight_IncrementNonExpanded( nInsertCount );
	return pNodeRetVal;
}

bool CExtTreeGridDataProvider::_Tree_NodeExpand(
	CExtTreeGridCellNode * pNode,
	INT nActionTVE // = TVE_TOGGLE // TVE_COLLAPSE, TVE_EXPAND, TVE_TOGGLE only
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pNode );
	ASSERT(
			nActionTVE == TVE_COLLAPSE
		||	nActionTVE == TVE_EXPAND
		||	nActionTVE == TVE_TOGGLE
		);
bool bExpanded = pNode->TreeNodeIsExpanded();
	if( nActionTVE == TVE_TOGGLE )
	{
		if( bExpanded )
			nActionTVE = TVE_COLLAPSE;
		else
			nActionTVE = TVE_EXPAND;
	} // if( nActionTVE == TVE_TOGGLE )
	if( nActionTVE == TVE_EXPAND )
	{
		if( bExpanded )
			return false;
		pNode->TreeNodeMarkExpanded( true );
		ULONG nWeightVisile = pNode->_ContentWeight_CalcVisible();
		if( pNode->TreeNodeIsDisplayed() )
		{
			ULONG nOffset = pNode->TreeNodeCalcOffset( true );
			ASSERT( nWeightVisile >= pNode->TreeNodeGetChildCount() );
			if( nWeightVisile != 0 )
			{
				m_arrGridVis.InsertAt( ++ nOffset, NULL, nWeightVisile );
				pNode->_Content_FillVisibleArray( m_arrGridVis, nOffset );
			} // if( nWeightVisile != 0 )
		} // if( pNode->TreeNodeIsDisplayed() )
		pNode->_ContentWeight_Increment( nWeightVisile, true );
	} // if( nActionTVE == TVE_EXPAND )
	else
	{
		if( ! bExpanded )
			return false;
		pNode->TreeNodeMarkExpanded( false );
		ULONG nWeightVisile =
			//pNode->_ContentWeight_Get( true );
			pNode->_ContentWeight_CalcVisible();
		if( pNode->TreeNodeIsDisplayed() )
		{
			ULONG nOffset = pNode->TreeNodeCalcOffset( true );
			if( nWeightVisile != 0 )
			{
				m_arrGridVis.RemoveAt( nOffset + 1, nWeightVisile );
			} // if( nWeightVisile != 0 )
		} // if( pNode->TreeNodeIsDisplayed() )
		pNode->_ContentWeight_Decrement( nWeightVisile, true );
	} // else from if( nActionTVE == TVE_EXPAND )
	return true;
}

ULONG CExtTreeGridDataProvider::_Tree_GetDisplayedCount() const
{
	ASSERT_VALID( this );
	return ULONG( m_arrGridVis.GetSize() );
}

ULONG CExtTreeGridDataProvider::_Tree_MapColToCache( ULONG nColNo ) const
{
	ASSERT_VALID( this );
	return nColNo + _Tree_ReservedColumnsGet();
}

ULONG CExtTreeGridDataProvider::_Tree_MapRowToCache( ULONG nRowNo ) const
{
	ASSERT_VALID( this );
const CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nReservedRowCount = 0;
	 _DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
ULONG nRowCountReservedForTree = _Tree_ReservedRowsGet();
	ASSERT( nReservedRowCount >= nRowCountReservedForTree );
	nReservedRowCount -= nRowCountReservedForTree;
ULONG nOffset = 0;
	if( nRowNo >= nReservedRowCount )
	{
		nRowNo -= nReservedRowCount;
		ASSERT( nRowNo < ULONG(m_arrGridVis.GetSize()) );
		CExtTreeGridCellNode * pNode = m_arrGridVis.GetAt( nRowNo );
		ASSERT_VALID( pNode );
		nOffset = pNode->TreeNodeCalcOffset( false );
		nOffset += nReservedRowCount;
	} // if( nRowNo >= nReservedRowCount )
	return nOffset + nRowCountReservedForTree;
}

ULONG CExtTreeGridDataProvider::_Tree_ReservedColumnsGet() const
{
	ASSERT_VALID( this );
	return m_nReservedColCount;
}

bool CExtTreeGridDataProvider::_Tree_ReservedColumnsSet( ULONG nReservedCount )
{
	ASSERT_VALID( this );
	ASSERT( nReservedCount >= 1 ); // including tree structure column
	if( nReservedCount == 0 )
		return false;
	if( m_pCellRoot == NULL )
		return false; // not initialized yet
	if( m_nReservedColCount == nReservedCount )
		return true;
	ASSERT_VALID( m_pCellRoot );
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	m_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
	if( m_nReservedColCount > nReservedCount )
	{
		ULONG nCountToRemove = m_nReservedColCount - nReservedCount;
		if( ! m_DP.ColumnRemove( nReservedCount, nCountToRemove ) )
			return false;
		m_DP.CacheReserveForOuterCells(
			nReservedColumnCount - nCountToRemove,
			nReservedRowCount
			);
	} // if( m_nReservedColCount > nReservedCount )
	else
	{
		ULONG nCountToInsert = nReservedCount - m_nReservedColCount;
		if( ! m_DP.ColumnInsert( m_nReservedColCount, nCountToInsert ) )
			return false;
		m_DP.CacheReserveForOuterCells(
			nReservedColumnCount + nCountToInsert,
			nReservedRowCount
			);
	} // else from if( m_nReservedColCount > nReservedCount )
	m_nReservedColCount = nReservedCount;
	return true;
}

bool CExtTreeGridDataProvider::_Tree_ReservedColumnsMoveToReserve(
	ULONG nSrcIdx,
	ULONG nSrcCount,
	ULONG nDstIdx
	)
{
	ASSERT_VALID( this );
	ASSERT( nDstIdx >= 1 );
	if( nDstIdx == 0 )
		return false;
ULONG nReservedColCount = _Tree_ReservedColumnsGet();
	ASSERT( nReservedColCount >= 1 ); // including tree structure
	ASSERT( nDstIdx <= nReservedColCount );
	if( nDstIdx > nReservedColCount )
		return false;
ULONG nPublishedColCount = ColumnCountGet();
	ASSERT( nSrcIdx < nPublishedColCount );
	if( nSrcIdx >= nPublishedColCount )
		return false;
	ASSERT( ( nSrcIdx + nSrcCount ) <= nPublishedColCount );
	if( ( nSrcIdx + nSrcCount ) > nPublishedColCount )
		return false;
	if( nSrcCount == 0 )
		return true;
ULONG nSrcIndex = nSrcIdx + nReservedColCount, nDstIndex = nDstIdx, nIdx = 0;
	for( ; nIdx < nSrcCount; nSrcIndex ++, nDstIndex ++, nIdx++ )
		m_DP.SwapDroppedSeries(
			true,
			nSrcIndex,
			nDstIndex,
			NULL
			);
	m_nReservedColCount += nSrcCount;
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	m_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
	m_DP.CacheReserveForOuterCells(
		nReservedColumnCount + nSrcCount,
		nReservedRowCount
		);
	return true;
}

bool CExtTreeGridDataProvider::_Tree_ReservedColumnsMoveFromReserve(
	ULONG nSrcIdx,
	ULONG nSrcCount,
	ULONG nDstIdx
	)
{
	ASSERT_VALID( this );
	ASSERT( nSrcIdx >= 1 );
	if( nSrcIdx == 0 )
		return false;
ULONG nReservedColCount = _Tree_ReservedColumnsGet();
	ASSERT( nReservedColCount >= 1 ); // including tree structure
	ASSERT( nSrcIdx < nReservedColCount );
	if( nSrcIdx >= nReservedColCount )
		return false;
	ASSERT( ( nSrcIdx + nSrcCount ) <= nReservedColCount );
	if( ( nSrcIdx + nSrcCount ) > nReservedColCount )
		return false;
	if( nSrcCount == 0 )
		return true;
ULONG nPublishedColCount = ColumnCountGet();
	ASSERT( nDstIdx <= nPublishedColCount );
	if( nDstIdx > nPublishedColCount )
		return false;
ULONG nSrcIndex = nSrcIdx, nDstIndex = nDstIdx + nReservedColCount, nIdx = 0;
	for( ; nIdx < nSrcCount; /*nSrcIndex ++, nDstIndex ++,*/ nIdx++ )
		m_DP.SwapDroppedSeries(
			true,
			nSrcIndex,
			nDstIndex,
			NULL
			);
	m_nReservedColCount -= nSrcCount;
	ASSERT( m_nReservedColCount >= 1 );
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	m_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
	m_DP.CacheReserveForOuterCells(
		nReservedColumnCount - nSrcCount,
		nReservedRowCount
		);
	return true;
}

ULONG CExtTreeGridDataProvider::_Tree_ReservedRowsGet() const
{
	ASSERT_VALID( this );
	return m_nReservedRowCount;
}

bool CExtTreeGridDataProvider::_Tree_ReservedRowsSet( ULONG nReservedCount )
{
	ASSERT_VALID( this );
	ASSERT( nReservedCount >= 1 ); // including tree structure row
	if( nReservedCount == 0 )
		return false;
	if( m_pCellRoot == NULL )
		return false; // not initialized yet
	if( m_nReservedRowCount == nReservedCount )
		return true;
	ASSERT_VALID( m_pCellRoot );
	if( m_nReservedRowCount > nReservedCount )
	{
		ULONG nCountToRemove = m_nReservedRowCount - nReservedCount;
		if( ! m_DP.RowRemove( nReservedCount, nCountToRemove ) )
			return false;
	} // if( m_nReservedRowCount > nReservedCount )
	else
	{
		ULONG nCountToInsert = nReservedCount - m_nReservedRowCount;
		if( ! m_DP.RowInsert( m_nReservedRowCount, nCountToInsert ) )
			return false;
	} // else from if( m_nReservedRowCount > nReservedCount )
	m_nReservedRowCount = nReservedCount;
	return true;
}

CExtGridDataProvider & CExtTreeGridDataProvider::_Tree_GetCacheDP()
{
	ASSERT_VALID( this );
	if( m_pCellRoot == NULL )
	{
		// reserve range for root cell and tree structure
		ULONG nReservedColCount = _Tree_ReservedColumnsGet();
		ULONG nReservedRowCount = _Tree_ReservedRowsGet();
		VERIFY( m_DP.ColumnInsert( 0, nReservedColCount ) );
		VERIFY( m_DP.RowInsert( 0, nReservedRowCount ) );
		m_DP.CacheReserveForOuterCells(
			nReservedColCount,
			nReservedRowCount
			);
		m_pCellRoot =
			STATIC_DOWNCAST(
				CExtTreeGridCellNode,
				m_DP.CellGet(
					0,
					0,
					_Tree_NodeGetRTC()
					)
				);
		ASSERT_VALID( m_pCellRoot );
		ASSERT_KINDOF( CExtTreeGridCellNode, m_pCellRoot );
		m_pCellRoot->TreeNodeMarkExpanded( true );
		_Tree_NodeAdjustProps( m_pCellRoot );
	} // if( m_pCellRoot == NULL )
	return m_DP;
}

const CExtGridDataProvider & CExtTreeGridDataProvider::_Tree_GetCacheDP() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridDataProvider * > ( this ) )
		-> _Tree_GetCacheDP();
}

#ifdef _DEBUG

void CExtTreeGridDataProvider::AssertValid() const
{
	CExtGridDataProvider::AssertValid();
	m_DP.AssertValid();
	ASSERT( m_nReservedColCount >= 1L );
	ASSERT( m_nReservedRowCount >= 1L );
}

void CExtTreeGridDataProvider::Dump( CDumpContext & dc ) const
{
	CExtGridDataProvider::Dump( dc );
	_Tree_GetCacheDP().Dump( dc );
}

#endif // _DEBUG

__EXT_MFC_SAFE_LPTSTR CExtTreeGridDataProvider::StringAlloc(
	INT nCharCountIncZT
	)
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.StringAlloc( nCharCountIncZT );
}

void CExtTreeGridDataProvider::StringFree(
	__EXT_MFC_SAFE_LPTSTR strToFree
	)
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	_DP.StringFree( strToFree );
}

bool CExtTreeGridDataProvider::ColumnInsert(
	ULONG nColNo,
	ULONG nInsertCount // = 1
	)
{
	ASSERT_VALID( this );
	ASSERT( nColNo >= 0 );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.ColumnInsert( _Tree_MapColToCache( nColNo ), nInsertCount );
}

bool CExtTreeGridDataProvider::RowInsert(
	ULONG nRowNo,
	ULONG nInsertCount // = 1
	)
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nRowCountReservedInCache = 0;
	_DP.CacheReservedCountsGet( NULL, &nRowCountReservedInCache );
	if( nRowNo <= nRowCountReservedInCache )
		return _DP.RowInsert( nRowNo, nInsertCount );
	// this method must never be invoked in other case
	ASSERT( FALSE );
	return false;
}

ULONG CExtTreeGridDataProvider::ColumnCountGet() const
{
	ASSERT_VALID( this );
const CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.ColumnCountGet() - _Tree_ReservedColumnsGet();
}

ULONG CExtTreeGridDataProvider::RowCountGet() const
{
	ASSERT_VALID( this );
const CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nRowCountReservedInCache = 0;
	_DP.CacheReservedCountsGet( NULL, &nRowCountReservedInCache );
ULONG nRowCountReservedForTree = _Tree_ReservedRowsGet();
	ASSERT( nRowCountReservedInCache >= nRowCountReservedForTree );
ULONG nRowCountDisplayed = _Tree_GetDisplayedCount();
ULONG nRowCountGridOriented =
		nRowCountReservedInCache
		- nRowCountReservedForTree
		+ nRowCountDisplayed;
	return nRowCountGridOriented;
}

bool CExtTreeGridDataProvider::ColumnRemove(
	ULONG nColNo,
	ULONG nRemoveCount // = 1
	)
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.ColumnRemove( _Tree_MapColToCache( nColNo ), nRemoveCount );
}

bool CExtTreeGridDataProvider::RowRemove(
	ULONG nRowNo,
	ULONG nRemoveCount // = 1
	)
{
	ASSERT_VALID( this );
	nRowNo;
	nRemoveCount;
	// this method must never be invoked
	ASSERT( FALSE );
	return false;
}

void CExtTreeGridDataProvider::MinimizeMemoryUsage()
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	_DP.MinimizeMemoryUsage();
}

bool CExtTreeGridDataProvider::RowDefaultValueBind(
	ULONG nRowNo, // = (ULONG(-1)) // if (ULONG(-1)) - default value for all rows
	CExtGridCell * pCell // = NULL // if NULL - remove default value
	)
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	if( nRowNo != (ULONG(-1)) )
		nRowNo = _Tree_MapRowToCache( nRowNo );
	return _DP.RowDefaultValueBind( nRowNo, pCell );
}

bool CExtTreeGridDataProvider::ColumnDefaultValueBind(
	ULONG nColNo, // = (ULONG(-1)) // if (ULONG(-1)) - default value for all columns
	CExtGridCell * pCell // = NULL // if NULL - remove default value
	)
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	if( nColNo != (ULONG(-1)) )
		nColNo = _Tree_MapColToCache( nColNo );
	return _DP.ColumnDefaultValueBind( nColNo, pCell );
}

void CExtTreeGridDataProvider::RowDefaultValueUnbindAll()
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	_DP.RowDefaultValueUnbindAll();
}

void CExtTreeGridDataProvider::ColumnDefaultValueUnbindAll()
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	_DP.ColumnDefaultValueUnbindAll();
}

CExtGridCell * CExtTreeGridDataProvider::RowDefaultValueGet(
	ULONG nRowNo
	)
{
	ASSERT_VALID( this );
	ASSERT( nRowNo >= 0 );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.RowDefaultValueGet( _Tree_MapRowToCache( nRowNo ) );
}

CExtGridCell * CExtTreeGridDataProvider::ColumnDefaultValueGet(
	ULONG nColNo
	)
{
	ASSERT_VALID( this );
	ASSERT( nColNo >= 0 );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.ColumnDefaultValueGet( _Tree_MapColToCache( nColNo ) );
}

CExtGridCell * CExtTreeGridDataProvider::ExtractGridCell(
	CExtTreeGridCellNode * pNode,
	ULONG nColNo,
	CRuntimeClass * pInitRTC, // = NULL
	bool bAutoFindValue, // = true
	bool bUseColumnDefaultValue // = true
	)
{
	ASSERT_VALID( this );
	ASSERT( nColNo >= 0 );
	if(		pNode == NULL
		||	pNode == TreeNodeGetRoot()
		)
		return NULL;
	ASSERT_VALID( pNode );
	ASSERT( pNode->TreeNodeGetParent() != NULL );
ULONG nRowNo = pNode->TreeNodeCalcOffset( false );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
	nColNo += nReservedColumnCount;
	nRowNo += nReservedRowCount;
	return
		_DP.CellGet(
			nColNo,
			nRowNo,
			pInitRTC,
			bAutoFindValue,
			bUseColumnDefaultValue
			);
}

CExtGridCell * CExtTreeGridDataProvider::CellGet(
	ULONG nColNo,
	ULONG nRowNo,
	CRuntimeClass * pInitRTC, // = NULL
	bool bAutoFindValue, // = true // auto find row/column default value (only when pInitRTC=NULL)
	bool bUseColumnDefaultValue // = true // false - use row default value (only when bAutoFindValue=true)
	)
{
	ASSERT_VALID( this );
	ASSERT( nColNo >= 0 );
	ASSERT( nRowNo >= 0 );
	nColNo = _Tree_MapColToCache( nColNo );
	nRowNo = _Tree_MapRowToCache( nRowNo );

//	if( nColNo == 2 )
//	{
//		TRACE( "---------------------------------\n" );
//		for( int i = 0; i < m_arrGridVis.GetSize(); i++ )
//		{
//			CExtTreeGridCellNode * pNode = m_arrGridVis[i];
//			CString s;
//			s.Format(
//				_T("%2d) %d %d %d %d\n"),
//				i,
//				pNode->_ContentWeight_Get( true ),
//				pNode->_ContentWeight_Get( false ),
//				pNode->TreeNodeCalcOffset( true ),
//				pNode->TreeNodeCalcOffset( false ),
//				pNode->TreeNodeGetChildCount()
//				);
//			TRACE( LPCTSTR(s) );
//		}
//	}

CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return
		_DP.CellGet(
			nColNo,
			nRowNo,
			pInitRTC,
			bAutoFindValue,
			bUseColumnDefaultValue
			);
}

bool CExtTreeGridDataProvider::CellRangeSet(
	ULONG nColNo,
	ULONG nRowNo,
	ULONG nColCount, // = 1L
	ULONG nRowCount, // = 1L
	const CExtGridCell * pCellNewValue, // = NULL // if NULL - empty existing cell values
	bool bReplace, // = false // false - assign to existing cell instances or column/row type created cells, true - create new cloned copies of pCellNewValue
	CRuntimeClass * pInitRTC, // = NULL // runtime class for new cell instance (used if bReplace=false)
	bool bAutoFindValue, // = true // auto find row/column default value (only when pInitRTC=NULL)
	bool bUseColumnDefaultValue, // = true // false - use row default value (only when bAutoFindValue=true)
	ULONG * p_nUpdatedCellCount // = NULL // returns count of really updated cells (zero also may be treated as success)
	)
{
	ASSERT_VALID( this );
	ASSERT( nColNo >= 0 );
	ASSERT( nRowNo >= 0 );
	ASSERT( nColCount == 1 );
	ASSERT( nRowCount == 1 );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return
		_DP.CellRangeSet(
			_Tree_MapColToCache( nColNo ),
			_Tree_MapRowToCache( nRowNo ),
			nColCount,
			nRowCount,
			pCellNewValue,
			bReplace,
			pInitRTC,
			bAutoFindValue,
			bUseColumnDefaultValue,
			p_nUpdatedCellCount
			);
}

bool CExtTreeGridDataProvider::CacheReserveForOuterCells(
	ULONG nColCount,
	ULONG nRowCount
	)
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return
		_DP.CacheReserveForOuterCells(
			nColCount + _Tree_ReservedColumnsGet(),
			nRowCount + _Tree_ReservedRowsGet()
			);
}

void CExtTreeGridDataProvider::CacheReservedCountsGet(
	ULONG * p_nColCount,
	ULONG * p_nRowCount
	) const
{
	ASSERT_VALID( this );
const CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	_DP.CacheReservedCountsGet(
		p_nColCount,
		p_nRowCount
		);
	if( p_nColCount != NULL )
		(*p_nColCount) -= _Tree_ReservedColumnsGet();
	if( p_nRowCount != NULL )
		(*p_nRowCount) -= _Tree_ReservedRowsGet();
}

bool CExtTreeGridDataProvider::CacheData(
	const CExtScrollItemCacheInfo & _sciNew,
	const CExtScrollItemCacheInfo & _sciOld
	)
{
	ASSERT_VALID( this );
	_sciNew;
	_sciOld;
	return true;
}

bool CExtTreeGridDataProvider::CacheIsVisibleFirstRecord( bool bHorz )
{
	ASSERT_VALID( this );
	bHorz;
	return true;
}

bool CExtTreeGridDataProvider::CacheIsVisibleLastRecord( bool bHorz )
{
	ASSERT_VALID( this );
	bHorz;
	return true;
}

ULONG CExtTreeGridDataProvider::CacheColumnCountGet()
{
	ASSERT_VALID( this );
	return ColumnCountGet();
}

ULONG CExtTreeGridDataProvider::CacheRowCountGet()
{
	ASSERT_VALID( this );
	return RowCountGet();
}

INT CExtTreeGridDataProvider::IconGetCount()
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.IconGetCount();
}

CExtCmdIcon * CExtTreeGridDataProvider::IconGetAt( INT nIdx )
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.IconGetAt( nIdx );
}

INT CExtTreeGridDataProvider::IconInsert( // returns index or -1
	CExtCmdIcon * pIcon,
	INT nIdx, // = -1 // append
	bool bCopyIcon // = true
	)
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.IconInsert( pIcon, nIdx, bCopyIcon );
}

INT CExtTreeGridDataProvider::IconRemove(
	INT nIdx, // = 0
	INT nCountToRemove // = -1 // all
	)
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.IconRemove( nIdx, nCountToRemove );
}

INT CExtTreeGridDataProvider::FontGetCount()
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.FontGetCount();
}

HFONT CExtTreeGridDataProvider::FontGetAt( INT nIdx )
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.FontGetAt( nIdx );
}

INT CExtTreeGridDataProvider::FontInsert( // returns index or -1
	HFONT hFont,
	INT nIdx, // = -1 // append
	bool bCopyFont // = true
	)
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.FontInsert( hFont, nIdx, bCopyFont );
}

INT CExtTreeGridDataProvider::FontRemove(
	INT nIdx, // = 0
	INT nCountToRemove // = -1 // all
	)
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
	return _DP.FontRemove( nIdx, nCountToRemove );
}

bool CExtTreeGridDataProvider::SortOrderUpdate(
	bool bColumns, // true = sort order for columns, false - for rows
	IDataProviderEvents * pDPE // = NULL
	)
{
	ASSERT_VALID( this );
	bColumns;
	pDPE;
	// this method must never be invoked
//	ASSERT( FALSE );
	return true;
}

bool CExtTreeGridDataProvider::SortOrderSet(
	const CExtGridDataSortOrder & _gdso,
	bool bColumns, // true = sort order for columns, false - for rows
	IDataProviderEvents * pDPE // = NULL
	)
{
	ASSERT_VALID( this );
	_gdso;
	bColumns;
	pDPE;
	// this method must never be invoked
	ASSERT( FALSE );
	return true;
}

bool CExtTreeGridDataProvider::SortOrderGet(
	CExtGridDataSortOrder & _gdso,
	bool bColumns // true = sort order for columns, false - for rows
	) const
{
	ASSERT_VALID( this );
	_gdso;
	bColumns;
	// this method must never be invoked
	ASSERT( FALSE );
	return true;
}

bool CExtTreeGridDataProvider::SwapDroppedSeries(
	bool bColumns, // true = swap columns, false - rows
	ULONG nRowColNoSrc,
	ULONG nRowColNoDropBefore,
	IDataProviderEvents * pDPE // = NULL
	)
{
	ASSERT_VALID( this );
	bColumns;
	nRowColNoSrc;
	nRowColNoDropBefore;
	pDPE;
	// this method must never be invoked
	ASSERT( FALSE );
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtTreeGridWnd

IMPLEMENT_DYNCREATE( CExtTreeGridWnd, CExtGridWnd );

CExtTreeGridWnd::CExtTreeGridWnd()
	: m_nGbwAdjustRectsLock( 0 )
	, m_bDrawFilledExpandGlyphs( true )
	, m_bAdustNullIndent( false )
{
//	if( m_iconTreeBoxExpanded.IsEmpty() )
//		PmBridge_GetPM()->LoadWinXpTreeBox(
//			m_iconTreeBoxExpanded,
//			true
//			);
//	if( m_iconTreeBoxCollapsed.IsEmpty() )
//		PmBridge_GetPM()->LoadWinXpTreeBox(
//			m_iconTreeBoxCollapsed,
//			false
//			);
}

CExtTreeGridWnd::~CExtTreeGridWnd()
{
}

CExtGridDataProvider & CExtTreeGridWnd::OnGridQueryDataProvider()
{
	ASSERT_VALID( this );
	if( m_pDataProvider != NULL )
	{
		ASSERT_VALID( m_pDataProvider );
		return (*m_pDataProvider);
	} // if( m_pDataProvider != NULL )
CExtTreeGridDataProvider * pTreeDP = new CExtTreeGridDataProvider;
	pTreeDP->m_pTreeGridWnd = this;
	m_pDataProvider = pTreeDP;
	ASSERT_VALID( m_pDataProvider );
	return (*m_pDataProvider);
}

CExtTreeGridDataProvider & CExtTreeGridWnd::GetTreeData()
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = OnGridQueryDataProvider();
	return (*(STATIC_DOWNCAST(CExtTreeGridDataProvider,(&_DP))));
}

const CExtTreeGridDataProvider & CExtTreeGridWnd::GetTreeData() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridWnd * > ( this ) )
			-> GetTreeData();
}

CExtTreeGridDataProvider & CExtTreeGridWnd::_GetTreeData() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridWnd * > ( this ) )
			-> GetTreeData();
}

HTREEITEM CExtTreeGridWnd::ItemGetRoot() const
{
	ASSERT_VALID( this );
//	if(		OuterRowCountTopGet() == 0
//		||	OuterColumnCountRightGet() == 0
//		)
//		return NULL;
CExtTreeGridDataProvider & _DP = _GetTreeData();
	return (*_DP.TreeNodeGetRoot());
}

HTREEITEM CExtTreeGridWnd::ItemGetByVisibleRowIndex( LONG nRowNo ) const
{
	ASSERT_VALID( this );
CExtTreeGridDataProvider & _DP = _GetTreeData();
ULONG nReservedRowCount = 0;
	_DP.CacheReservedCountsGet( NULL, &nReservedRowCount );
	nRowNo += nReservedRowCount;
	return (*_DP.TreeNodeGetByVisibleRowIndex(ULONG(nRowNo)));
}

HTREEITEM CExtTreeGridWnd::ItemGetParent( HTREEITEM hTreeItem ) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetParent()));
}

HTREEITEM CExtTreeGridWnd::ItemGetNext(
	HTREEITEM hTreeItem,
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper
	) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetNext( bSiblingOnly, bExpandedWalk, bWalkDeeper )));
}

HTREEITEM CExtTreeGridWnd::ItemGetPrev(
	HTREEITEM hTreeItem,
	bool bSiblingOnly,
	bool bExpandedWalk,
	bool bWalkDeeper
	) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetPrev( bSiblingOnly, bExpandedWalk, bWalkDeeper )));
}

LONG CExtTreeGridWnd::ItemGetChildCount( HTREEITEM hTreeItem ) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetChildCount();
}

HTREEITEM CExtTreeGridWnd::ItemGetChildAt( HTREEITEM hTreeItem, LONG nPos ) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetChildAt(nPos)));
}

HTREEITEM CExtTreeGridWnd::ItemGetFirstSibling( HTREEITEM hTreeItem ) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetFirstSibling()));
}

HTREEITEM CExtTreeGridWnd::ItemGetLastSibling( HTREEITEM hTreeItem ) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetLastSibling()));
}

HTREEITEM CExtTreeGridWnd::ItemGetFirstChild( HTREEITEM hTreeItem ) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetFirstChild()));
}

HTREEITEM CExtTreeGridWnd::ItemGetLastChild( HTREEITEM hTreeItem ) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
	return (*(CExtTreeGridCellNode::FromHTREEITEM(hTreeItem)->TreeNodeGetLastChild()));
}

HTREEITEM CExtTreeGridWnd::ItemInsert(
	HTREEITEM hTreeItemParent, // if NULL - root
	ULONG nIdxInsertBefore, // = (ULONG(-1)) // if (ULONG(-1)) - insert to end
	ULONG nInsertCount, // = 1
	bool bRedraw // = true
	) // returns handle of first inserted
{
	ASSERT_VALID( this );
HTREEITEM hTreeItem =
		(*(	_GetTreeData().TreeNodeInsert(
				( hTreeItemParent != NULL )
					? CExtTreeGridCellNode::FromHTREEITEM(hTreeItemParent)
					: NULL,
				nIdxInsertBefore,
				nInsertCount
				)
			) );
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	}
	return hTreeItem;
}

LONG CExtTreeGridWnd::ItemRemove(
	HTREEITEM hTreeItem,
	bool bChildrenOnly, // = false
	bool bRedraw // = true
	) // returns count of removed items
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return 0;
	ItemFocusSet( NULL );
LONG nCountRemoved = (LONG)
		_GetTreeData().TreeNodeRemove(
			CExtTreeGridCellNode::FromHTREEITEM(hTreeItem),
			bChildrenOnly
			);
	OnSwUpdateScrollBars();
	if( bRedraw )
		OnSwDoRedraw();
	return nCountRemoved;
}

CExtGridCell * CExtTreeGridWnd::ItemGetCell(
	HTREEITEM hTreeItem,
	LONG nColNo,
	INT nColType, // = 0
	CRuntimeClass * pInitRTC, // = NULL
	bool bAutoFindValue, // = true
	bool bUseColumnDefaultValue // = true
	)
{
	ASSERT_VALID( this );
	if(		hTreeItem == NULL
		||	hTreeItem == ItemGetRoot()
		)
		return NULL;
LONG nEffectiveColNo = nColNo;
	if( nColType < 0 )
	{
		LONG nOuterColCountLeft = CExtGridBaseWnd::OuterColumnCountLeftGet();
		if( nColNo >= nOuterColCountLeft )
		{
			// invalid cell position
			ASSERT( FALSE );
			return NULL;
		}
		LONG nOuterColCountRight = CExtGridBaseWnd::OuterColumnCountRightGet();
		LONG nEffectiveShift = nOuterColCountLeft + nOuterColCountRight;
		nEffectiveColNo -= nEffectiveShift;
	} // if( nColType < 0 )
	else if( nColType > 0 )
	{
		LONG nOuterColCountRight = CExtGridBaseWnd::OuterColumnCountRightGet();
		if( nColNo >= nOuterColCountRight )
		{
			// invalid cell position
			ASSERT( FALSE );
			return NULL;
		}
//		LONG nOuterColCountLeft = CExtGridBaseWnd::OuterColumnCountLeftGet();
//		nEffectiveColNo += nOuterColCountLeft;
		nEffectiveColNo -= nOuterColCountRight;
	} // else if( nColType > 0 )
	else
	{
		LONG nInnerColCount = ColumnCountGet();
		if( nColNo >= nInnerColCount )
		{
			// invalid cell position
//			ASSERT( FALSE );
			return NULL;
		} // if( nColNo >= nInnerColCount )
//		LONG nOuterColCountLeft = CExtGridBaseWnd::OuterColumnCountLeftGet();
//		LONG nOuterColCountRight = CExtGridBaseWnd::OuterColumnCountRightGet();
//		LONG nEffectiveShift = nOuterColCountLeft + nOuterColCountRight;
//		nEffectiveColNo += nEffectiveShift;
	} // else from else if( nColType > 0 )
CExtGridCell * pCell =
		_GetTreeData().ExtractGridCell(
			CExtTreeGridCellNode::FromHTREEITEM(hTreeItem),
			(ULONG)nEffectiveColNo,
			pInitRTC,
			bAutoFindValue,
			bUseColumnDefaultValue
			);
	return pCell;
}

const CExtGridCell * CExtTreeGridWnd::ItemGetCell(
	HTREEITEM hTreeItem,
	LONG nColNo,
	INT nColType, // = 0
	CRuntimeClass * pInitRTC, // = NULL
	bool bAutoFindValue, // = true
	bool bUseColumnDefaultValue // = true
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTreeGridWnd * > ( this ) ) ->
			ItemGetCell(
				hTreeItem,
				nColNo,
				nColType,
				pInitRTC,
				bAutoFindValue,
				bUseColumnDefaultValue
				);
}

bool CExtTreeGridWnd::ItemIsExpanded(
	HTREEITEM hTreeItem
	) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
	{
		ASSERT( FALSE );
		return false;
	}
	return
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItem )
			-> TreeNodeIsExpanded();
}

CExtTreeGridCellNode::e_expand_box_shape_t CExtTreeGridWnd::ItemGetExpandBoxShape(
	HTREEITEM hTreeItem
	) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
	{
		ASSERT( FALSE );
		return CExtTreeGridCellNode::__EEBS_NONE;
	}
	return
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItem )
			-> TreeNodeGetExpandBoxShape();
}

void CExtTreeGridWnd::ItemExpand(
	HTREEITEM hTreeItem,
	INT nActionTVE, // = TVE_TOGGLE // TVE_COLLAPSE, TVE_EXPAND, TVE_TOGGLE only
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT(
			nActionTVE == TVE_COLLAPSE
		||	nActionTVE == TVE_EXPAND
		||	nActionTVE == TVE_TOGGLE
		);
	if( hTreeItem == NULL )
		return;
	if( _GetTreeData().TreeNodeExpand(
			CExtTreeGridCellNode::FromHTREEITEM(hTreeItem),
			nActionTVE
			)
		)
	{
		CPoint ptFocus = FocusGet();
		if( ptFocus.y >= 0 )
		{
 			LONG i = ItemGetVisibleIndexOf( hTreeItem );
			if( i >= 0 )
			{
				ptFocus.y = i;
				FocusSet( ptFocus, false, false, true, true, false );
			} // if( i >= 0 )
			else
				ItemFocusSet( hTreeItem, bRedraw );
		} // if( ptFocus.y >= 0 )
		else
			ItemFocusSet( hTreeItem, bRedraw );
		if( bRedraw && GetSafeHwnd() != NULL )
		{
			OnSwUpdateScrollBars();
			OnSwDoRedraw();
		}
	}
}

void CExtTreeGridWnd::ItemExpandAll(
	HTREEITEM hTreeItem,
	INT nActionTVE, // = TVE_TOGGLE // TVE_COLLAPSE, TVE_EXPAND, TVE_TOGGLE only
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT(
			nActionTVE == TVE_COLLAPSE
		||	nActionTVE == TVE_EXPAND
		||	nActionTVE == TVE_TOGGLE
		);
	if( hTreeItem == NULL )
		return;
LONG nChildIndex, nChildCount = ItemGetChildCount( hTreeItem );
	for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
	{
		HTREEITEM htiChild = ItemGetChildAt( hTreeItem, nChildIndex );
		ASSERT( htiChild != NULL );
		ItemExpandAll( htiChild, nActionTVE, false );
	}
	if( hTreeItem != ItemGetRoot() )
		ItemExpand( hTreeItem, nActionTVE, bRedraw );
	else if( bRedraw && GetSafeHwnd() != NULL )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	}
}

HTREEITEM CExtTreeGridWnd::ItemFocusGet() const
{
	ASSERT_VALID( this );
CPoint ptFocus = FocusGet();
	if( ptFocus.y < 0 )
		return NULL;
HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( ptFocus.y );
	return hTreeItem;
}

bool CExtTreeGridWnd::ItemFocusSet(
	HTREEITEM hTreeItem,
	LONG nColNo,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
CPoint ptFocus = FocusGet();
	if( ptFocus.y < 0 )
	{
		if( hTreeItem == NULL )
			return false;
	} // if( ptFocus.y < 0 )
	else
	{
		if( hTreeItem == NULL )
		{
			SelectionUnset( false, false );
			FocusUnset( bRedraw );
			return true;
		}
		HTREEITEM htiFocus = ItemGetByVisibleRowIndex( ptFocus.y );
		ASSERT( htiFocus != NULL );
		if( htiFocus == hTreeItem && nColNo == ptFocus.x )
			return false;
	} // else from if( ptFocus.y < 0 )
	if( ! ItemEnsureExpanded( hTreeItem, false ) )
		return false;
	ptFocus.x = nColNo;
	ptFocus.y = ItemGetVisibleIndexOf( hTreeItem );
	ASSERT( ptFocus.y >= 0 );
	if( bRedraw )
		OnSwUpdateScrollBars();
	FocusSet( ptFocus, bRedraw, true, true, bRedraw );
	return true;
}

bool CExtTreeGridWnd::ItemFocusSet(
	HTREEITEM hTreeItem,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	return ItemFocusSet( hTreeItem, FocusGet().x, bRedraw );
}

bool CExtTreeGridWnd::ItemEnsureExpanded(
	HTREEITEM hTreeItem,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return false;
CExtTreeGridDataProvider & _DP = _GetTreeData();
CExtTreeGridCellNode * pNodeRoot =
		_DP.TreeNodeGetRoot();
	ASSERT_VALID( pNodeRoot );
CExtTreeGridCellNode * pNode =
		CExtTreeGridCellNode::FromHTREEITEM(hTreeItem);
	ASSERT_VALID( pNode );
	if( LPCVOID(pNodeRoot) != LPCVOID(pNode) )
	{
		pNode = pNode->TreeNodeGetParent();
		ASSERT_VALID( pNode );
		for( ; LPCVOID(pNodeRoot) != LPCVOID(pNode); )
		{
			_DP.TreeNodeExpand(
				pNode,
				TVE_EXPAND
				);
			pNode = pNode->TreeNodeGetParent();
			ASSERT_VALID( pNode );
		} // for( ; LPCVOID(pNodeRoot) == LPCVOID(pNode); )
	} // if( LPCVOID(pNodeRoot) != LPCVOID(pNode) )
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	}
	return true;
}

LONG CExtTreeGridWnd::ItemGetSiblingIndexOf(
	HTREEITEM hTreeItem
	) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return -1;
CExtTreeGridCellNode * pNode =
		CExtTreeGridCellNode::FromHTREEITEM(hTreeItem);
	ASSERT_VALID( pNode );
LONG i = (LONG)pNode->TreeNodeGetSiblingIndex();
	return i;
}

LONG CExtTreeGridWnd::ItemGetVisibleIndexOf(
	HTREEITEM hTreeItem
	) const
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return -1;
CExtTreeGridCellNode * pNode =
		CExtTreeGridCellNode::FromHTREEITEM(hTreeItem);
	ASSERT_VALID( pNode );
	if( ! pNode->TreeNodeIsDisplayed() )
		return -1;
LONG i = (LONG)pNode->TreeNodeCalcOffset( true );
	return i;
}

bool CExtTreeGridWnd::_IsTreeGridWndInitialized() const
{
	ASSERT_VALID( this );
	return ( ItemGetRoot() != NULL ) ? true : false;
}

void CExtTreeGridWnd::_InitTreeGridWnd()
{
	ASSERT_VALID( this );
	ItemGetRoot();
}

void CExtTreeGridWnd::PreSubclassWindow() 
{
	CExtGridWnd::PreSubclassWindow();
	_InitTreeGridWnd();
}

CExtGridHitTestInfo & CExtTreeGridWnd::HitTest(
	CExtGridHitTestInfo & htInfo,
	bool bReAlignCellResizing,
	bool bSupressZeroTopCellResizing,
	bool bComputeOuterDropAfterState // = false
	) const
{
	ASSERT_VALID( this );
	if( bReAlignCellResizing || bComputeOuterDropAfterState )
		m_nGbwAdjustRectsLock ++;
CExtGridHitTestInfo & htInfoRetVal =
		CExtGridWnd::HitTest(
			htInfo,
			bReAlignCellResizing,
			bSupressZeroTopCellResizing,
			bComputeOuterDropAfterState
			);
	if(		(htInfoRetVal.m_dwAreaFlags&__EGBWA_NEAR_CELL_BORDER_SIDE_MASK) == 0
		&&	(htInfoRetVal.m_dwAreaFlags&__EGBWA_INNER_CELLS) != 0
		&&	OnTreeGridQueryColumnOutline(htInfoRetVal.m_nColNo)
		)
	{ // if hit testing inside inner cell and not near the cell's border
		CRect rcOutlineArea(
			htInfoRetVal.m_rcExtra.left,
			htInfoRetVal.m_rcExtra.top,
			min(htInfoRetVal.m_rcItem.left,htInfoRetVal.m_rcItem.right),
			htInfoRetVal.m_rcExtra.bottom
			);
		if( rcOutlineArea.PtInRect(htInfoRetVal.m_ptClient) )
		{ // if inside outline area
			htInfoRetVal.m_dwAreaFlags |= __EGBWA_TREE_OUTLINE_AREA;
			HTREEITEM hTreeItemHT = ItemGetByVisibleRowIndex( htInfoRetVal.m_nRowNo );
			ASSERT( hTreeItemHT != NULL );
			CExtTreeGridCellNode * pNode =
				CExtTreeGridCellNode::FromHTREEITEM( hTreeItemHT );
			ASSERT_VALID( pNode );
			CExtTreeGridCellNode::e_expand_box_shape_t eEBS =
				pNode->TreeNodeGetExpandBoxShape();
			if( eEBS != CExtTreeGridCellNode::__EEBS_NONE )
			{
				CRect rcIndent(
					htInfoRetVal.m_rcItem.left, htInfoRetVal.m_rcItem.top,
					htInfoRetVal.m_rcItem.left, htInfoRetVal.m_rcItem.bottom
					);
				INT nPxIndent = (INT)pNode->TreeNodeIndentPxGet();
				if( nPxIndent == ULONG(-1L) )
					nPxIndent = GetTreeData().m_nIndentPxDefault;
				rcIndent.right = rcIndent.left;
				rcIndent.left -= nPxIndent;
				if(		rcIndent.left == rcIndent.right
					&&	m_bAdustNullIndent
					)
				{
					INT nPxIndentPaint = nPxIndent;
					if( nPxIndentPaint == 0 )
						nPxIndentPaint = GetTreeData().m_nIndentPxDefault;
					rcIndent.left -= nPxIndentPaint;
				}
				if( rcIndent.PtInRect(htInfoRetVal.m_ptClient) )
				{
					htInfoRetVal.m_dwAreaFlags |= __EGBWA_TREE_BOX;
					if( eEBS != CExtTreeGridCellNode::__EEBS_EXPANDED )
						htInfoRetVal.m_dwAreaFlags |= __EGBWA_TREE_BOX_IS_EXPANDED;
				} // if( rcIndent.PtInRect(htInfoRetVal.m_ptClient) )
			} // if( eEBS != CExtTreeGridCellNode::__EEBS_NONE )
		} // if inside outline area
	} // if hit testing inside inner cell and not near the cell's border
	if( bReAlignCellResizing || bComputeOuterDropAfterState )
		m_nGbwAdjustRectsLock --;
	return htInfoRetVal;
}

void CExtTreeGridWnd::OnGbwResizingStateUpdate(
	bool bInitial,
	bool bFinal,
	const CPoint * p_ptClient // = NULL
	)
{
	ASSERT_VALID( this );
	m_nGbwAdjustRectsLock ++;
	CExtGridWnd::OnGbwResizingStateUpdate(
		bInitial,
		bFinal,
		p_ptClient
		);
	m_nGbwAdjustRectsLock --;
	if( m_nGbwAdjustRectsLock == 0 )
	{
		OnSwRecalcLayout( true );
		OnSwDoRedraw();
	} // if( m_nGbwAdjustRectsLock == 0 )
}

void CExtTreeGridWnd::OnGbwResizingStateApply(
	bool bHorz,
	LONG nItemNo,
	INT nItemExtent
	)
{
	ASSERT_VALID( this );
	m_nGbwAdjustRectsLock ++;
	CExtGridWnd::OnGbwResizingStateApply(
		bHorz,
		nItemNo,
		nItemExtent
		);
	m_nGbwAdjustRectsLock --;
	if( m_nGbwAdjustRectsLock == 0 )
	{
		OnSwRecalcLayout( true );
		OnSwDoRedraw();
	} // if( m_nGbwAdjustRectsLock == 0 )
}

bool CExtTreeGridWnd::OnTreeGridQueryDrawOutline(
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( nVisibleColNo >= 0 );
	ASSERT( nVisibleRowNo >= 0 );
	ASSERT( nColNo >= 0 );
	ASSERT( nRowNo >= 0 );
	dc;
	nVisibleColNo;
	nVisibleRowNo;
	nColNo;
	nRowNo;
	rcCellExtra;
	rcCell;
	rcVisibleRange;
	dwAreaFlags;
	dwHelperPaintFlags;
	return true;
}

void CExtTreeGridWnd::OnTreeGridNodeRemoved(
	CExtTreeGridCellNode * pNode
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pNode );
	pNode;
}

void CExtTreeGridWnd::OnTreeGridPaintExpandButton(
	CDC & dc,
	HTREEITEM hTreeItem,
	bool bExpanded,
	const CRect & rcIndent
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( hTreeItem != NULL );
	hTreeItem;
	if(		( ! m_iconTreeBoxExpanded.IsEmpty() )
		&&	( ! m_iconTreeBoxCollapsed.IsEmpty() )
		)
	{
		const CExtCmdIcon * pIcon =
			bExpanded
				? (&m_iconTreeBoxExpanded)
				: (&m_iconTreeBoxCollapsed)
				;
		if( ! pIcon->IsEmpty() )
		{
			CSize _sizeIcon = pIcon->GetSize();
			CPoint ptOutput =
				rcIndent.TopLeft()
				+ CSize(
					( rcIndent.Width()  - _sizeIcon.cx ) / 2,
					( rcIndent.Height() - _sizeIcon.cy ) / 2
					)
				;
			pIcon->Paint(
				PmBridge_GetPM(),
				dc.m_hDC,
				ptOutput.x,
				ptOutput.y,
				-1,
				-1
				);
			return;
		} // if( ! pIcon->IsEmpty() )
	}
CExtPaintManager::glyph_t * pGlyph =
		CExtPaintManager::g_DockingCaptionGlyphs[
				bExpanded
					? (	m_bDrawFilledExpandGlyphs
							? CExtPaintManager::__DCBT_TREE_MINUS_FILLED
							: CExtPaintManager::__DCBT_TREE_MINUS
						)
					: (	m_bDrawFilledExpandGlyphs
							? CExtPaintManager::__DCBT_TREE_PLUS_FILLED
							: CExtPaintManager::__DCBT_TREE_PLUS
						)
				];
COLORREF ColorValues[] =
{
	RGB(0,0,0),
	PmBridge_GetPM()->GetColor(
		CExtPaintManager::CLR_TEXT_OUT,
		(CObject*)this
		),
	PmBridge_GetPM()->GetColor(
		COLOR_WINDOW,
		(CObject*)this
		),
};
CPoint ptGlyph = rcIndent.TopLeft();
	ptGlyph.x += ( rcIndent.Width() - pGlyph->Size().cx ) / 2;
	ptGlyph.y += ( rcIndent.Height() - pGlyph->Size().cy ) / 2;
	PmBridge_GetPM()->PaintGlyph(
		dc,
		ptGlyph,
		*pGlyph,
		ColorValues
		);
}

void CExtTreeGridWnd::OnGbwPaintCell(
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( nVisibleColNo >= 0 );
	ASSERT( nVisibleRowNo >= 0 );
	ASSERT( nColNo >= 0 );
	ASSERT( nRowNo >= 0 );
	CExtGridWnd::OnGbwPaintCell(
		dc,
		nVisibleColNo,
		nVisibleRowNo,
		nColNo,
		nRowNo,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);
	if(		( dwAreaFlags & __EGBWA_INNER_CELLS ) != 0
		&&	OnTreeGridQueryColumnOutline( nColNo )
		)
	{
		CRect rcOutlineArea(
			rcCellExtra.left,
			rcCellExtra.top,
			min(rcCell.left,rcCell.right),
			rcCellExtra.bottom
			);
		CDC & dcOutline = dc;
// 		CExtMemoryDC dcOutline(
// 			&dc,
// 			&rcOutlineArea,
//  			CExtMemoryDC::MDCOPT_TO_MEMORY //|CExtMemoryDC::MDCOPT_FORCE_DIB
//  				|CExtMemoryDC::MDCOPT_FILL_BITS
// 			);
		COLORREF clrDots =
			PmBridge_GetPM()->GetColor(
				CExtPaintManager::CLR_3DSHADOW_OUT,
				(CObject*)this
				);
		HTREEITEM hRootItem = ItemGetRoot();
		ASSERT( hRootItem != NULL );
		HTREEITEM hTreeItemFirst = ItemGetByVisibleRowIndex( nRowNo );
		ASSERT( hTreeItemFirst != NULL );
		CRect rcIndent(
			rcCell.left, rcCell.top,
			rcCell.left, rcCell.bottom
			);
		HTREEITEM hTreeItem = hTreeItemFirst;
		bool bDrawOutline =
			OnTreeGridQueryDrawOutline(
				dcOutline,
				nVisibleColNo,
				nVisibleRowNo,
				nColNo,
				nRowNo,
				rcCellExtra,
				rcCell,
				rcVisibleRange,
				dwAreaFlags,
				dwHelperPaintFlags
				);
		for( ; hTreeItem != hRootItem; hTreeItem = ItemGetParent(hTreeItem) )
		{
			ASSERT( hTreeItem != NULL );
			CExtTreeGridCellNode * pNode =
				CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
			ASSERT_VALID( pNode );
			INT nPxIndent = (INT)pNode->TreeNodeIndentPxGet();
			if( nPxIndent == ULONG(-1L) )
				nPxIndent = GetTreeData().m_nIndentPxDefault;
			rcIndent.right = rcIndent.left;
			rcIndent.left -= nPxIndent;
			INT nMetricH = rcIndent.Width();
			INT nMetricH2 = nMetricH / 2;
			INT nMetricV = rcIndent.Height();
			INT nMetricV2 = min( nMetricH, nMetricV );
			nMetricV2 /= 2;
			bool bDrawUpperLineV = false;
			if( bDrawOutline && hTreeItemFirst == hTreeItem )
			{
				CExtPaintManager::stat_DrawDotLineH(
					dcOutline,
					rcIndent.left + nMetricH2,
					rcIndent.right,
					rcIndent.top + nMetricV2,
					clrDots
					);
				bDrawUpperLineV = true;
			}
			HTREEITEM hTreeItemNext = ItemGetNext( hTreeItem, true, false, false );
			if( bDrawOutline && hTreeItemNext != NULL )
			{
				CExtPaintManager::stat_DrawDotLineV(
					dcOutline,
					rcIndent.left + nMetricH2,
					rcIndent.top + nMetricV2,
					rcIndent.bottom,
					clrDots
					);
				bDrawUpperLineV = true;
			}
			if( bDrawOutline && bDrawUpperLineV )
				CExtPaintManager::stat_DrawDotLineV(
					dcOutline,
					rcIndent.left + nMetricH2,
					rcIndent.top,
					rcIndent.top + nMetricV2,
					clrDots
					);
			if( hTreeItemFirst == hTreeItem )
			{
				CExtTreeGridCellNode::e_expand_box_shape_t eEBS =
					pNode->TreeNodeGetExpandBoxShape();
				if( eEBS != CExtTreeGridCellNode::__EEBS_NONE )
				{
					CRect rcIndentPaint = rcIndent;
					if(		rcIndentPaint.left == rcIndentPaint.right
						&&	m_bAdustNullIndent
						)
					{
						INT nPxIndentPaint = nPxIndent;
						if( nPxIndentPaint == 0 )
							nPxIndentPaint = GetTreeData().m_nIndentPxDefault;
						rcIndentPaint.left -= nPxIndentPaint;
					}
					if( rcIndentPaint.left < rcIndentPaint.right )
						OnTreeGridPaintExpandButton(
							dcOutline,
							hTreeItem,
							( eEBS == CExtTreeGridCellNode::__EEBS_EXPANDED )
								? true : false,
							rcIndentPaint
							);
				}
			} // if( hTreeItemFirst == hTreeItem )
		} // for( ; hTreeItem != hRootItem; hTreeItem = ItemGetParent(hTreeItem) )
		//dcOutline.__Flush();
	} // if( ( dwAreaFlags & __EGBWA_INNER_CELLS ) != 0 ...
}

bool CExtTreeGridWnd::OnTreeGridQueryColumnOutline(
	LONG nColNo
	) const
{
	ASSERT_VALID( this );
	return (nColNo == 0) ? true : false;
}

void CExtTreeGridWnd::OnTreeGridToggleItemExpandedState(
	LONG nRowNo,
	CExtGridHitTestInfo * pHtInfo, // = NULL
	INT nActionTVE // = TVE_TOGGLE // TVE_COLLAPSE, TVE_EXPAND, TVE_TOGGLE only
	)
{
	ASSERT_VALID( this );
	ASSERT(
			nActionTVE == TVE_COLLAPSE
		||	nActionTVE == TVE_EXPAND
		||	nActionTVE == TVE_TOGGLE
		);
	pHtInfo;
HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
	if( hTreeItem == NULL )
		return;
	ItemExpand( hTreeItem, nActionTVE );
}

bool CExtTreeGridWnd::OnGbwBeginEdit(
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcInplaceControl,
	bool bContinueMsgLoop, // = true
	__EXT_MFC_SAFE_LPCTSTR strStartEditText // = NULL
	)
{
	ASSERT_VALID( this );
bool bRetVal =
		CExtGridWnd::OnGbwBeginEdit(
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcInplaceControl,
			bContinueMsgLoop,
			strStartEditText
			);
	return bRetVal;
}

bool CExtTreeGridWnd::OnGbwAnalyzeCellMouseClickEvent(
	UINT nChar, // VK_LBUTTON, VK_RBUTTON or VK_MBUTTON only
	UINT nRepCnt, // 0 - button up, 1 - single click, 2 - double click, 3 - post single click & begin editing
	UINT nFlags, // mouse event flags
	CPoint point // mouse pointer in client coordinates
	)
{
	ASSERT_VALID( this );
	ASSERT( 0 <= nRepCnt && nRepCnt <= 3 );
	if( nChar == VK_LBUTTON )
	{
		CExtGridHitTestInfo htInfo( point );
		HitTest( htInfo, false, true );
		if(		htInfo.IsHoverEmpty()
			||	(! htInfo.IsValidRect() )
			)
			return false;
		INT nColType = htInfo.GetInnerOuterTypeOfColumn();
		INT nRowType = htInfo.GetInnerOuterTypeOfRow();
		if(		nColType == 0
			&&	nRowType == 0
			)
		{
			if( nRepCnt == 1 || nRepCnt == 2 )
			{
				if(		(	(	nRepCnt == 1
							&&	OnTreeGridQueryColumnOutline( htInfo.m_nColNo )
							&&	(htInfo.m_dwAreaFlags&__EGBWA_TREE_OUTLINE_AREA) != 0
							)
						||	nRepCnt == 2
						)
					&&	(htInfo.m_dwAreaFlags&(__EGBWA_CELL_BUTTON|__EGBWA_CELL_CHECKBOX)) == 0
					)
				{
					HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( htInfo.m_nRowNo );
					if(		(	nRepCnt == 2
							&&	hTreeItem != NULL
							&&	ItemGetChildCount( hTreeItem ) > 0
							)
						||	(htInfo.m_dwAreaFlags&__EGBWA_TREE_BOX) != 0
						)
					{
						OnTreeGridToggleItemExpandedState(
							htInfo.m_nRowNo,
							&htInfo
							);
						return true;
					}
				}
			} // if( nRepCnt == 1 || nRepCnt == 2 )
		}
	} // if( nChar == VK_LBUTTON && ( nRepCnt == 1 || nRepCnt == 2 ) )
	if( CExtGridWnd::OnGbwAnalyzeCellMouseClickEvent(
			nChar,
			nRepCnt,
			nFlags,
			point
			)
		)
		return true;
	return false;
}

bool CExtTreeGridWnd::OnTreeGridQueryKbCollapseEnabled(
	HTREEITEM hTreeItem,
	LONG nColNo
	)
{
	ASSERT_VALID( this );
	ASSERT( hTreeItem != NULL );
	hTreeItem;
	nColNo;
	return true;
}

bool CExtTreeGridWnd::OnTreeGridQueryKbExpandEnabled(
	HTREEITEM hTreeItem,
	LONG nColNo
	)
{
	ASSERT_VALID( this );
	ASSERT( hTreeItem != NULL );
	hTreeItem;
	nColNo;
	return true;
}

bool CExtTreeGridWnd::OnTreeGridQueryKbJumpParentEnabled(
	HTREEITEM hTreeItem,
	LONG nColNo
	)
{
	ASSERT_VALID( this );
	ASSERT( hTreeItem != NULL );
	hTreeItem;
	nColNo;
	return true;
}

bool CExtTreeGridWnd::OnGbwAnalyzeCellKeyEvent(
	bool bKeyDownEvent, // true - key-down event, false - key-up event
	UINT nChar, // virtual key code
	UINT nRepCnt, // key-down/key-up press count
	UINT nFlags // key-down/key-up event flags
	)
{
	ASSERT_VALID( this );
	if( bKeyDownEvent )
	{
		if( nChar == VK_LEFT )
		{
			HTREEITEM hTreeItem = ItemFocusGet();
			if( hTreeItem == NULL )
				return true;
			CExtTreeGridCellNode::e_expand_box_shape_t eEBT =
				ItemGetExpandBoxShape( hTreeItem );
			if( eEBT == CExtTreeGridCellNode::__EEBS_EXPANDED )
			{
				if( ! OnTreeGridQueryKbCollapseEnabled( hTreeItem, FocusGet().x ) )
					return
						CExtGridWnd::OnGbwAnalyzeCellKeyEvent(
							bKeyDownEvent,
							nChar,
							nRepCnt,
							nFlags
							);
				ItemExpand( hTreeItem, TVE_COLLAPSE );
				return true;
			} // if( eEBT == CExtTreeGridCellNode::__EEBS_EXPANDED )
			HTREEITEM htiParent = ItemGetParent( hTreeItem );
			if(		htiParent == NULL
				||	htiParent == ItemGetRoot()
				)
				//return true;
				return
					CExtGridWnd::OnGbwAnalyzeCellKeyEvent(
						bKeyDownEvent,
						nChar,
						nRepCnt,
						nFlags
						);
			if( ! OnTreeGridQueryKbJumpParentEnabled( hTreeItem, FocusGet().x ) )
				return
					CExtGridWnd::OnGbwAnalyzeCellKeyEvent(
						bKeyDownEvent,
						nChar,
						nRepCnt,
						nFlags
						);
			ItemFocusSet( htiParent );
			return true;
		} // if( nChar == VK_LEFT )
		if( nChar == VK_RIGHT )
		{
			HTREEITEM hTreeItem = ItemFocusGet();
			if( hTreeItem == NULL )
				return true;
			CExtTreeGridCellNode::e_expand_box_shape_t eEBT =
				ItemGetExpandBoxShape( hTreeItem );
			if( eEBT != CExtTreeGridCellNode::__EEBS_COLLAPSED )
				//return true;
				return
					CExtGridWnd::OnGbwAnalyzeCellKeyEvent(
						bKeyDownEvent,
						nChar,
						nRepCnt,
						nFlags
						);
			if( ! OnTreeGridQueryKbExpandEnabled( hTreeItem, FocusGet().x ) )
				return
					CExtGridWnd::OnGbwAnalyzeCellKeyEvent(
						bKeyDownEvent,
						nChar,
						nRepCnt,
						nFlags
						);
			ItemExpand( hTreeItem, TVE_EXPAND );
			return true;
		} // if( nChar == VK_RIGHT )
		if( nChar == VK_SUBTRACT )
		{
			HTREEITEM hTreeItem = ItemFocusGet();
			if( hTreeItem == NULL )
				return true;
			CExtTreeGridCellNode::e_expand_box_shape_t eEBT =
				ItemGetExpandBoxShape( hTreeItem );
			if( eEBT != CExtTreeGridCellNode::__EEBS_EXPANDED )
				return true;
			ItemExpand( hTreeItem, TVE_COLLAPSE );
			return true;
		} // if( nChar == VK_SUBTRACT )
		if( nChar == VK_ADD )
		{
			HTREEITEM hTreeItem = ItemFocusGet();
			if( hTreeItem == NULL )
				return true;
			CExtTreeGridCellNode::e_expand_box_shape_t eEBT =
				ItemGetExpandBoxShape( hTreeItem );
			if( eEBT != CExtTreeGridCellNode::__EEBS_COLLAPSED )
				return true;
			ItemExpand( hTreeItem, TVE_EXPAND );
			return true;
		} // if( nChar == VK_ADD )
	} // if( bKeyDownEvent )

bool bRetVal =
		CExtGridWnd::OnGbwAnalyzeCellKeyEvent(
			bKeyDownEvent,
			nChar,
			nRepCnt,
			nFlags
			);
	return bRetVal;
}

void CExtTreeGridWnd::OnGbwAdjustRects(
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	RECT & rcCellExtraA,
	RECT & rcCellA
	) const
{
	ASSERT_VALID( this );
	rcCellExtraA;
	if( 	m_nGbwAdjustRectsLock == 0
		&&	nRowType == 0
		&&	nColType == 0
		&&	OnTreeGridQueryColumnOutline( nColNo )
		)
	{
		ULONG nRowIndentPx = _GetTreeData().TreeGetRowIndentPx( ULONG(nRowNo) );
		rcCellA.left += nRowIndentPx;
	} // if( m_nGbwAdjustRectsLock == 0 ...
}

void CExtTreeGridWnd::OnSwUpdateWindow()
{
	ASSERT_VALID( this );
	if( m_nGbwAdjustRectsLock != 0 )
		return;
	CExtGridWnd::OnSwUpdateWindow();
}

LONG CExtTreeGridWnd::RowCountGet() const
{
	(*((LONG*)(&m_nCountOfRows))) =
		LONG( _GetTreeData().TreeGetDisplayedCount() );
	return m_nCountOfRows;
}

BEGIN_MESSAGE_MAP( CExtTreeGridWnd, CExtGridWnd )
	//{{AFX_MSG_MAP(CExtTreeGridWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

INT CExtTreeGridWnd::OnSiwQueryItemExtentH(
	LONG nColNo,
	INT * p_nExtraSpaceBefore, // = NULL
	INT * p_nExtraSpaceAfter   // = NULL
	) const
{
	ASSERT_VALID( this );
	return
		CExtGridWnd::OnSiwQueryItemExtentH(
			nColNo,
			p_nExtraSpaceBefore,
			p_nExtraSpaceAfter
			);
}

INT CExtTreeGridWnd::OnSiwQueryItemExtentV(
	LONG nRowNo,
	INT * p_nExtraSpaceBefore, // = NULL
	INT * p_nExtraSpaceAfter   // = NULL
	) const
{
	ASSERT_VALID( this );
	ASSERT( nRowNo >= 0 );
	if( p_nExtraSpaceBefore != NULL )
		(*p_nExtraSpaceBefore) = 0;
	if( p_nExtraSpaceAfter != NULL )
		(*p_nExtraSpaceAfter) = 0;
	if( FixedSizeRowsGet() )
		return
			CExtGridWnd::OnSiwQueryItemExtentV(
				nRowNo,
				p_nExtraSpaceBefore,
				p_nExtraSpaceAfter
				);
HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
	if( hTreeItem == NULL )
		return
			CExtGridWnd::OnSiwQueryItemExtentV(
				nRowNo,
				p_nExtraSpaceBefore,
				p_nExtraSpaceAfter
				);
CExtTreeGridCellNode * pCell =
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
	ASSERT_VALID( pCell );
INT nItemExtent, nExtraSpaceAfter = 0, nExtraSpaceBefore = 0;
	if( ! pCell->ExtentGet( nItemExtent, 0 ) )
		return
			CExtGridWnd::OnSiwQueryItemExtentV(
				nRowNo,
				p_nExtraSpaceBefore,
				p_nExtraSpaceAfter
				);
	pCell->ExtraSpaceGet( nExtraSpaceAfter, true );
	pCell->ExtraSpaceGet( nExtraSpaceBefore, false );
	ASSERT( nItemExtent >= 0 );
	ASSERT( nExtraSpaceAfter >= 0 );
	ASSERT( nExtraSpaceBefore >= 0 );
	if( p_nExtraSpaceBefore != NULL )
		(*p_nExtraSpaceBefore) = nExtraSpaceBefore;
	if( p_nExtraSpaceAfter != NULL )
		(*p_nExtraSpaceAfter) = nExtraSpaceAfter;
	return (nItemExtent+nExtraSpaceAfter+nExtraSpaceBefore);
}

bool CExtTreeGridWnd::OnGbwCanResizeColumn(
	LONG nColNo,
	INT * p_nExtentMin, // = NULL
	INT * p_nExtentMax  // = NULL
	)
{
	ASSERT_VALID( this );
	return
		CExtGridWnd::OnGbwCanResizeColumn(
			nColNo,
			p_nExtentMin,
			p_nExtentMax
			);
}

bool CExtTreeGridWnd::OnGbwCanResizeRow(
	LONG nRowNo,
	INT * p_nExtentMin, // = NULL
	INT * p_nExtentMax  // = NULL
	)
{
	ASSERT_VALID( this );
	if( FixedSizeRowsGet() )
		return
			CExtGridWnd::OnGbwCanResizeRow(
				nRowNo,
				p_nExtentMin,
				p_nExtentMax
				);
INT nExtentMin = 0, nExtentMax = 32767;
	ASSERT( nRowNo >= 0 );
HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
	if( hTreeItem == NULL )
		return
			CExtGridWnd::OnGbwCanResizeRow(
				nRowNo,
				p_nExtentMin,
				p_nExtentMax
				);
CExtTreeGridCellNode * pCell =
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
	ASSERT_VALID( pCell );
	if( pCell->ExtentGet( nExtentMin, -1 ) )
	{
		VERIFY( pCell->ExtentGet( nExtentMax, 1 ) );
	}
	ASSERT(
			nExtentMin >=  0
		&&	nExtentMax >= 0
		&&	nExtentMin <= nExtentMax
		);
	if( p_nExtentMin != NULL )
		(*p_nExtentMin) = nExtentMin;
	if( p_nExtentMax != NULL )
		(*p_nExtentMax) = nExtentMax;
bool bRetVal = (nExtentMin != nExtentMax) ? true : false;
	return bRetVal;
}

void CExtTreeGridWnd::OnDataProviderSortEnter(
	bool bColumns // true = sorting/swapping columns, false - rows
	)
{
	ASSERT_VALID( this );
	bColumns;
}

void CExtTreeGridWnd::OnDataProviderSortLeave(
	bool bColumns // true = sorting/swapping columns, false - rows
	)
{
	ASSERT_VALID( this );
	bColumns;
}

void CExtTreeGridWnd::OnDataProviderSwapSeries(
	bool bColumns, // true = sorting/swapping columns, false - rows
	LONG nRowColNo1,
	LONG nRowColNo2,
	LONG nSwapCounter
	)
{
	ASSERT_VALID( this );
	bColumns;
	nRowColNo1;
	nRowColNo2;
	nSwapCounter;
}

void CExtTreeGridWnd::OnDataProviderSwapDroppedSeries(
	bool bColumns, // true = swapping columns, false - rows
	LONG nRowColNoSrc,
	LONG nRowColNoDropBefore
	)
{
	ASSERT_VALID( this );
	bColumns;
	nRowColNoSrc;
	nRowColNoDropBefore;
}

#endif // (!defined __EXT_MFC_NO_TREEGRIDWND)


