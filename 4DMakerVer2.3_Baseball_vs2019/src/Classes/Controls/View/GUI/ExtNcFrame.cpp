// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_NC_FRAME_H)
	#include <ExtNcFrame.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if (!defined __AFXPRIV_H__)
	#include <AfxPriv.h>
#endif 

#if _MFC_VER < 0x700
	#include <../src/AfxImpl.h>
#else
	#ifndef __AFXSTATE_H__
		#include <../src/mfc/afxstat_.h>
	#endif
	#include <../src/mfc/AfxImpl.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if (!defined __EXT_MFC_NO_NC_FRAME )

IMPLEMENT_CExtPmBridge_MEMBERS_GENERIC( CExtPmBridgeNC )

CExtPmBridgeNC::CExtPmBridgeNC(
	CExtNcFrameImpl * pNcFrameImpl // = NULL
	)
	: m_pNcFrameImpl( pNcFrameImpl )
{
	PmBridge_Install();
}

CExtPmBridgeNC::~CExtPmBridgeNC()
{
	PmBridge_Uninstall();
}

const CExtNcFrameImpl * CExtPmBridgeNC::NcFrameImpl_Get() const
{
	return m_pNcFrameImpl;
}

CExtNcFrameImpl * CExtPmBridgeNC::NcFrameImpl_Get()
{
	return m_pNcFrameImpl;
}

void CExtPmBridgeNC::NcFrameImpl_Set(
	CExtNcFrameImpl * pNcFrameImpl
	)
{
	m_pNcFrameImpl = pNcFrameImpl;
}

HWND CExtPmBridgeNC::PmBridge_GetSafeHwnd() const
{
	if( m_pNcFrameImpl != NULL )
	{
		HWND hWnd =
			m_pNcFrameImpl->NcFrameImpl_OnQueryHWND();
		return hWnd;
	}
	return NULL;
}

void CExtPmBridgeNC::PmBridge_OnPaintManagerChanged(
	CExtPaintManager * pGlobalPM
	)
{
	_AdjustThemeSettings();
	CExtPmBridge::PmBridge_OnPaintManagerChanged( pGlobalPM );
}

void CExtPmBridgeNC::PmBridge_OnDisplayChange(
	CExtPaintManager * pGlobalPM,
	CWnd * pWndNotifySrc,
	INT nDepthBPP,
	CPoint ptSizes
	)
{
	CExtPmBridge::PmBridge_OnDisplayChange(
		pGlobalPM,
		pWndNotifySrc,
		nDepthBPP,
		ptSizes
		);
	if( g_PaintManager.m_bIsWinVistaOrLater )
		Sleep( 3000 );
	_AdjustThemeSettings();
}

void CExtPmBridgeNC::PmBridge_OnThemeChanged(
	CExtPaintManager * pGlobalPM,
	CWnd * pWndNotifySrc,
	WPARAM wParam,
	LPARAM lParam
	)
{
	CExtPmBridge::PmBridge_OnThemeChanged(
		pGlobalPM,
		pWndNotifySrc,
		wParam,
		lParam
		);
	if( g_PaintManager.m_bIsWinVistaOrLater )
		Sleep( 5000 );
	_AdjustThemeSettings();
}

void CExtPmBridgeNC::_AdjustThemeSettings()
{
	if( m_pNcFrameImpl == NULL )
		return;
HWND hWndOwn = PmBridge_GetSafeHwnd();
	if( hWndOwn == NULL )
		return;
	ASSERT( ::IsWindow( hWndOwn ) );
WINDOWPLACEMENT _wp;
	::memset( &_wp, 0, sizeof(WINDOWPLACEMENT) );
	_wp.length = sizeof(WINDOWPLACEMENT);
	::GetWindowPlacement( hWndOwn, &_wp );
DWORD dwInitialStyle = m_pNcFrameImpl->NcFrameImpl_GetInitialStyle();
DWORD dwCurrentStyle = (DWORD) ::GetWindowLong( hWndOwn, GWL_STYLE );
HWND hWndSurface = NULL;
	if( (dwCurrentStyle&(WS_CHILD|WS_VISIBLE)) == WS_VISIBLE )
	{
		CRect rcSurface;
		::GetWindowRect( hWndOwn, &rcSurface );
		
//		bool bRestoreSurface = false;
//		CDC dcSurface;
//		CBitmap bmpSurface, * pBmpOld = NULL;
//		if( dcSurface.CreateCompatibleDC( NULL ) )
//		{
//			CWindowDC dcDesktop( NULL );
//			if( bmpSurface.CreateCompatibleBitmap( &dcDesktop, rcSurface.Width(), rcSurface.Height() ) )
//			{
//				bRestoreSurface = true;
//				pBmpOld = dcSurface.SelectObject( &bmpSurface );
//				::SendMessage(
//					hWndOwn,
//					WM_PRINT,
//					WPARAM( dcSurface.m_hDC ),
//					PRF_CHECKVISIBLE|PRF_CHILDREN|PRF_CLIENT
//						|PRF_ERASEBKGND|PRF_NONCLIENT|PRF_OWNED 
//					);
//			} // if( bmpSurface.CreateCompatibleBitmap( &dcDesktop, rcSurface.Width(), rcSurface.Height() ) )
//		} // if( dcSurface.CreateCompatibleDC( NULL ) )

		hWndSurface =
			::CreateWindowEx(
				0,
				_T("Static"),
				_T(""),
				WS_POPUP,
				rcSurface.left,
				rcSurface.top,
				rcSurface.Width()
					+ (		(	_wp.showCmd == SW_SHOWNORMAL
							&&	g_PaintManager.m_bIsWinXPorLater
							) ? 1 : 0  // +1 for recomputing Window HRGN on WindowsXP or Later
						),
				rcSurface.Height(),
				hWndOwn,
				(HMENU)NULL,
				::AfxGetInstanceHandle(),
				NULL
				);
		if( hWndSurface != NULL )
		{
			::EnableWindow( hWndSurface, FALSE );
			::ShowWindow( hWndSurface, SW_SHOWNOACTIVATE );
		} // if( hWndSurface != NULL )

//		if( bRestoreSurface )
//		{
//			if( hWndSurface != NULL )
//			{
//				CWindowDC dcDst( CWnd::FromHandle(hWndSurface) );
//				dcDst.BitBlt(
//					0,
//					0,
//					rcSurface.Width(),
//					rcSurface.Height(),
//					&dcSurface,
//					rcSurface.Width(),
//					rcSurface.Height(),
//					SRCCOPY
//					);
//			} // if( hWndSurface != NULL )
//			dcSurface.SelectObject( pBmpOld );
//		} // if( bRestoreSurface )

	} // if( (dwCurrentStyle&(WS_CHILD|WS_VISIBLE)) == WS_VISIBLE )


CExtNcFrameImpl::NcLock _NcLock( * m_pNcFrameImpl );
	::SetWindowRgn( hWndOwn, NULL, TRUE );
	if( (dwInitialStyle&WS_BORDER) != 0 )
		dwCurrentStyle |= WS_BORDER;
	else
		dwCurrentStyle &= ~(WS_BORDER);
	if( (dwInitialStyle&WS_CAPTION) != 0 )
		dwCurrentStyle |= WS_CAPTION;
	else
		dwCurrentStyle &= ~(WS_CAPTION);
	::SetWindowLong( hWndOwn, GWL_STYLE, LONG(dwCurrentStyle) );
//	CExtPmBridge::PmBridge_OnPaintManagerChanged( pGlobalPM );
	if( ! ::IsWindow( hWndOwn ) )
		return;
	if( m_pNcFrameImpl->NcFrameImpl_IsSupported() )
	{
		DWORD dwStyle = (DWORD) ::GetWindowLong( hWndOwn, GWL_STYLE );
		if( (dwStyle&WS_BORDER) != 0 )
		{
			if( m_pNcFrameImpl->NcFrameImpl_IsDwmBased() )
			{
// 				if( m_pNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement() )
// 					dwStyle &= ~(WS_CAPTION);
			}
			else
				dwStyle &= ~WS_BORDER;
			::SetWindowLong( hWndOwn, GWL_STYLE, LONG(dwStyle) );
		} // if( (dwStyle&WS_BORDER) != 0 )
	} // if( m_pNcFrameImpl->NcFrameImpl_IsSupported() )
		//m_pNcFrameImpl->NcFrameImpl_RecalcNcFrame();
	if( m_pNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement() )
	{
		m_pNcFrameImpl->NcFrameImpl_AdjustVistaDwmCompatibilityIssues();
		m_pNcFrameImpl->NcFrameImpl_RecalcNcFrame();
	}
	else
		m_pNcFrameImpl->NcFrameImpl_DelayRgnAdjustment();
	_NcLock.UnlockNow();
	if(		m_pNcFrameImpl->NcFrameImpl_IsSupported()
		&&	( ! m_pNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement() )
		)
	{
		m_pNcFrameImpl->NcFrameImpl_SetupRgn( NULL );
		::RedrawWindow(
			hWndOwn, NULL, NULL,
			RDW_INVALIDATE|RDW_ERASE
				|RDW_FRAME
				|RDW_ALLCHILDREN
			);
	} // if( m_pNcFrameImpl->NcFrameImpl_IsSupported() ...
	else
	{
		m_pNcFrameImpl->NcFrameImpl_RecalcNcFrame();
		if( (dwCurrentStyle&(WS_CHILD|WS_VISIBLE)) == WS_VISIBLE )
		{
			switch( _wp.showCmd )
			{
			case SW_SHOWNORMAL:
				{
					if( g_PaintManager.m_bIsWinXPorLater )
					{
						CRect rc = _wp.rcNormalPosition;
						rc.right++;
						MoveWindow( hWndOwn, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top, FALSE );
						rc.right--;
						MoveWindow( hWndOwn, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top, FALSE );
					} // if( g_PaintManager.m_bIsWinXPorLater )
//					CSize _size(
//						_wp.rcNormalPosition.right - _wp.rcNormalPosition.left,
//						_wp.rcNormalPosition.bottom - _wp.rcNormalPosition.top
//						);
//					::SendMessage( hWndOwn, WM_SIZE, SIZE_RESTORED, MAKELPARAM(_size.cx,_size.cy) );
				}
			break;
//			case SW_SHOWMAXIMIZED:
//				{
//					CSize _size(
//						_wp.rcNormalPosition.right - _wp.rcNormalPosition.left,
//						_wp.rcNormalPosition.bottom - _wp.rcNormalPosition.top
//						);
//					::SendMessage( hWndOwn, WM_SIZE, SIZE_MAXIMIZED, MAKELPARAM(_size.cx,_size.cy) );
//				}
//			break;
			} // switch( _wp.showCmd )
		} // if( (dwCurrentStyle&(WS_CHILD|WS_VISIBLE)) == WS_VISIBLE )
	} // else from if( m_pNcFrameImpl->NcFrameImpl_IsSupported() ...
	if( m_pNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement() )
		m_pNcFrameImpl->NcFrameImpl_RecalcNcFrame();
bool bContinueRecalc = true;
	while( bContinueRecalc )
	{
		bContinueRecalc = false;
		MSG _msg;
		while( ::PeekMessage( &_msg, NULL, WM_WINDOWPOSCHANGING, WM_WINDOWPOSCHANGED, PM_NOREMOVE ) )
		{
			if( ! ::GetMessage( &_msg, NULL, WM_WINDOWPOSCHANGING, WM_WINDOWPOSCHANGED ) )
				break;
			::DispatchMessage( &_msg );
			bContinueRecalc = true;
		}
		while( ::PeekMessage( &_msg, NULL, WM_NCCALCSIZE, WM_NCCALCSIZE, PM_NOREMOVE ) )
		{
			if( ! ::GetMessage( &_msg, NULL, WM_NCCALCSIZE, WM_NCCALCSIZE ) )
				break;
			::DispatchMessage( &_msg );
			bContinueRecalc = true;
		}
		while( ::PeekMessage( &_msg, NULL, WM_WINDOWPOSCHANGING, WM_WINDOWPOSCHANGED, PM_NOREMOVE ) )
		{
			if( ! ::GetMessage( &_msg, NULL, WM_WINDOWPOSCHANGING, WM_WINDOWPOSCHANGED ) )
				break;
			::DispatchMessage( &_msg );
			bContinueRecalc = true;
		}
		while( ::PeekMessage( &_msg, NULL, WM_SIZE, WM_SIZE, PM_NOREMOVE ) )
		{
			if( ! ::GetMessage( &_msg, NULL, WM_SIZE, WM_SIZE ) )
				break;
			::DispatchMessage( &_msg );
			bContinueRecalc = true;
		}
	} // while( bContinueRecalc )
	if( hWndSurface != NULL )
		::DestroyWindow( hWndSurface );
	CExtPaintManager::stat_PassPaintMessages();
}

CExtNcFrameImpl::NcLock::NcLock( const CExtNcFrameImpl & _NcFrameImpl )
	: m_NcFrameImpl( _NcFrameImpl )
	, m_bLocked( true )
{
	m_NcFrameImpl.NcFrameImpl_NcLock( true );
}

CExtNcFrameImpl::NcLock::~NcLock()
{
	UnlockNow();
}

void CExtNcFrameImpl::NcLock::UnlockNow()
{
	if( ! m_bLocked )
		return;
	m_bLocked = false;
	m_NcFrameImpl.NcFrameImpl_NcLock( false );
}

const UINT CExtNcFrameImpl::g_nMsgFindExtNcFrameImpl =
	::RegisterWindowMessage(
		_T("CExtNcFrameImpl::g_nMsgFindExtNcFrameImpl")
		);

CExtNcFrameImpl::CExtNcFrameImpl()
	: m_bNcFrameImpl_IsActive( false )
	, m_bNcFrameImpl_RgnSet( m_bNcFrameImpl_RgnSet )
	, m_bNcFrameImpl_RestoreBorder( false )
	, m_bNcFrameImpl_DelatayedFrameRecalc( false )
	, m_bNcFrameImpl_IsEnabled( true )
	, m_bNcFrameImpl_IsDwmBased( true )
	, m_bNcFrameImpl_Resizing( false )
	, m_bNcFrameImpl_QuickWindowPlacement( false )
	, m_rcNcFrameImpl_ScClose( 0, 0, 0, 0 )
	, m_rcNcFrameImpl_ScMaximize( 0, 0, 0, 0 )
	, m_rcNcFrameImpl_ScMinimize( 0, 0, 0, 0 )
	, m_rcNcFrameImpl_ScHelp( 0, 0, 0, 0 )
	, m_rcNcFrameImpl_Text( 0, 0, 0, 0 )
	, m_rcNcFrameImpl_Icon( 0, 0, 0, 0 )
	, m_nNcFrameImpl_ScTrackedButtonHover( 0 )
	, m_nNcFrameImpl_ScTrackedButtonPressed( 0 )
	, m_strNcFrameImpl_TipMinimize( _T("Minimize") )
	, m_strNcFrameImpl_TipMaximize( _T("Maximize") )
	, m_strNcFrameImpl_TipRestore( _T("Restore") )
	, m_strNcFrameImpl_TipClose( _T("Close") )
	, m_dwNcFrameImpl_StyleInitial( 0 )
	, m_dwNcFrameImpl_StyleExInitial( 0 )
	, m_nNcFrameImpl_Lock( 0 )
	, m_nNcFrameImpl_LastCheckCursorHT( HTNOWHERE )
	, m_ptNcFrameImpl_LastCheckCursor( -32767, -32767 )
	, m_pNcFrameImplBridge( NULL )
	, m_bNcFrameImpl_HelperInsideNcHitTest( false )
{
	m_BridgeNC.NcFrameImpl_Set( this );
	m_wndNcFrameImpl_Tip.SetTipStyle( CExtPopupMenuTipWnd::__ETS_RECTANGLE_NO_ICON );
	m_wndNcFrameImpl_Tip.SetShadowSize( 0 );
}

CExtNcFrameImpl::~CExtNcFrameImpl()
{
	m_BridgeNC.NcFrameImpl_Set( NULL );
	NcFrameImpl_MapHtRects_Clean();
}

void CExtNcFrameImpl::NcFrameImpl_MapHtRects_Clean()
{
POSITION pos = m_mapNcFrameImpl_HtRects.GetStartPosition();
	for( ; pos != NULL; )
	{
		LPVOID p1, p2;
		m_mapNcFrameImpl_HtRects.GetNextAssoc( pos, p1, p2 );
		ASSERT( p2 != NULL );
		RECT * pRect = ( RECT * ) p2;
		delete pRect;
	} // for( ; pos != NULL; )
	m_mapNcFrameImpl_HtRects.RemoveAll();
}

void CExtNcFrameImpl::NcFrameImpl_MapHtRects_SetAt( LONG nHT, const RECT & rc ) const
{
LPVOID p;
	if( m_mapNcFrameImpl_HtRects.Lookup( LPVOID(nHT), p ) )
	{
		ASSERT( p != NULL );
		RECT * pRect = ( RECT * ) p;
		pRect->left = rc.left;
		pRect->top = rc.top;
		pRect->right = rc.right;
		pRect->bottom = rc.bottom;
	}
	else
	{
		RECT * pRect = new RECT;
		pRect->left = rc.left;
		pRect->top = rc.top;
		pRect->right = rc.right;
		pRect->bottom = rc.bottom;
		m_mapNcFrameImpl_HtRects.SetAt( LPVOID(nHT), pRect );
	}
}

bool CExtNcFrameImpl::NcFrameImpl_MapHtRects_GetAt( LONG nHT, RECT & rc ) const
{
LPVOID p;
	if( m_mapNcFrameImpl_HtRects.Lookup( LPVOID(nHT), p ) )
	{
		ASSERT( p != NULL );
		RECT * pRect = ( RECT * ) p;
		rc.left = pRect->left;
		rc.top = pRect->top;
		rc.right = pRect->right;
		rc.bottom = pRect->bottom;
		return true;
	}
	return false;
}

void CExtNcFrameImpl::NcFrameImpl_NcLock( bool bLock ) const
{
	ASSERT( m_nNcFrameImpl_Lock >= 0 );
	if( bLock )
		m_nNcFrameImpl_Lock ++;
	else
		m_nNcFrameImpl_Lock --;
	ASSERT( m_nNcFrameImpl_Lock >= 0 );
}

DWORD CExtNcFrameImpl::NcFrameImpl_GetInitialStyle() const
{
	return m_dwNcFrameImpl_StyleInitial;
}

DWORD CExtNcFrameImpl::NcFrameImpl_GetInitialStyleEx() const
{
	return m_dwNcFrameImpl_StyleExInitial;
}

HWND CExtNcFrameImpl::NcFrameImpl_OnQueryHWND() const
{
	return ( const_cast < CExtNcFrameImpl * > (this) ) -> NcFrameImpl_OnQueryHWND();
}

CWnd * CExtNcFrameImpl::NcFrameImpl_GetFrameWindow()
{
HWND hWnd = NcFrameImpl_OnQueryHWND();
	if( hWnd == NULL )
		return NULL;
	ASSERT( ::IsWindow( hWnd ) );
	return CWnd::FromHandle( hWnd );
}

const CWnd * CExtNcFrameImpl::NcFrameImpl_GetFrameWindow() const
{
	return ( const_cast < CExtNcFrameImpl * > (this) ) -> NcFrameImpl_GetFrameWindow();
}

CRect CExtNcFrameImpl::NcFrameImpl_GetNcHtRect(
	UINT nHT,
	LPMINMAXINFO pMinMaxInfo,
	LPCRECT pRectWnd // = NULL
	) const
{
	return
		NcFrameImpl_GetNcHtRect(
			nHT,
			true,
			false,
			false,
			pMinMaxInfo,
			pRectWnd
			);
}

CRect CExtNcFrameImpl::NcFrameImpl_GetNcHtRect(
	UINT nHT,
	bool bScreenMapping, // = true
	bool bLayoutBordersH, // = false
	bool bLayoutBordersV, // = false
	LPMINMAXINFO pMinMaxInfo, // = NULL
	LPCRECT pRectWnd // = NULL
	) const
{
	if( m_pNcFrameImplBridge != NULL )
	{
		CRect rc;
		if( m_pNcFrameImplBridge->
				NcFrameImpl_GetNcHtRect(
					rc,
					nHT,
					bScreenMapping,
					bLayoutBordersH,
					bLayoutBordersV,
					pMinMaxInfo,
					pRectWnd
					)
			)
			return rc;
	}
//CWnd * pWndFrameImpl = (CWnd *)NcFrameImpl_GetFrameWindow();
//	ASSERT_VALID( pWndFrameImpl );
//NcLock _NcLock( * this );
//DWORD dwResetStyles = m_dwNcFrameImpl_StyleInitial&WS_BORDER;
//	if( dwResetStyles != 0 )
//		pWndFrameImpl->ModifyStyle( 0, dwResetStyles );

RECT rc = { 0, 0, 0, 0 };
	if( NcFrameImpl_MapHtRects_GetAt( LONG(nHT), rc ) )
	{
		if( bScreenMapping )
		{
			CRect rcWnd( 0, 0, 0, 0 );
			if( pRectWnd != NULL )
				rcWnd = (*pRectWnd);
			else
			{
 				CWnd * pWndFrameImpl = (CWnd *)NcFrameImpl_GetFrameWindow();
 				ASSERT_VALID( pWndFrameImpl );
				pWndFrameImpl->GetWindowRect( &rcWnd );
			} // else from if( pRectWnd != NULL )
			::OffsetRect( &rc, rcWnd.left, rcWnd.top );
		} // if( bScreenMapping )
	} // if( NcFrameImpl_MapHtRects_GetAt( LONG(nHT), rc ) )
	else
	{
		CExtCmdIcon _icon;
		NcFrameImpl_GetIcon( _icon );
		CWnd * pWndFrameImpl = (CWnd *)NcFrameImpl_GetFrameWindow();
		ASSERT_VALID( pWndFrameImpl );
		rc =
			NcFrameImpl_GetPM()->NcFrame_GetHtRect(
				nHT,
				bScreenMapping,
				bLayoutBordersH,
				bLayoutBordersV,
				&_icon,
				pWndFrameImpl,
				pMinMaxInfo
				);
		CRect rc2 = rc;
		if( bScreenMapping )
		{
			CRect rcWnd( 0, 0, 0, 0 );
			if( pRectWnd != NULL )
				rcWnd = (*pRectWnd);
			else
			{
 				CWnd * pWndFrameImpl = (CWnd *)NcFrameImpl_GetFrameWindow();
 				ASSERT_VALID( pWndFrameImpl );
				pWndFrameImpl->GetWindowRect( &rcWnd );
			} // else from if( pRectWnd != NULL )
			::OffsetRect( &rc, -rcWnd.left, -rcWnd.top );
		} // if( bScreenMapping )
		NcFrameImpl_MapHtRects_SetAt( LONG(nHT), rc2 );
	} // else from if( NcFrameImpl_MapHtRects_GetAt( LONG(nHT), rc1 ) )

//	if( dwResetStyles != 0 )
//		pWndFrameImpl->ModifyStyle( dwResetStyles, 0 );
	return rc;
}

CRect CExtNcFrameImpl::NcFrameImpl_GetNcScRect( UINT nSC ) const
{
	if( m_rcNcFrameImpl_ScClose.IsRectEmpty() )
		NcFrameImpl_ReCacheScButtonRects();
	if( m_pNcFrameImplBridge != NULL )
	{
		CRect rc;
		if( m_pNcFrameImplBridge->NcFrameImplBridge_GetNcScRect( nSC, rc ) )
		{
			switch( nSC )
			{
			case SC_CLOSE:
				m_rcNcFrameImpl_ScClose = rc;
			case SC_MAXIMIZE:
			case SC_RESTORE:
				m_rcNcFrameImpl_ScMaximize = rc;
			case SC_MINIMIZE:
				m_rcNcFrameImpl_ScMinimize = rc;
			case SC_CONTEXTHELP:
				m_rcNcFrameImpl_ScHelp = rc;
			} // switch( nSC )
			return rc;
		}
	}
	switch( nSC )
	{
	case SC_CLOSE:
		return m_rcNcFrameImpl_ScClose;
	case SC_MAXIMIZE:
	case SC_RESTORE:
		return m_rcNcFrameImpl_ScMaximize;
	case SC_MINIMIZE:
		return m_rcNcFrameImpl_ScMinimize;
	case SC_CONTEXTHELP:
		return m_rcNcFrameImpl_ScHelp;
	} // switch( nSC )
	return CRect( 0, 0, 0, 0 );
}

void CExtNcFrameImpl::NcFrameImpl_GetIcon( CExtCmdIcon & _icon ) const
{
	_icon.Empty();
	if( ! m_iconNcFrameImpl_QueryCache.IsEmpty() )
	{
		_icon = m_iconNcFrameImpl_QueryCache;
		return;
	}
const CWnd * pWndFrameImpl = NcFrameImpl_GetFrameWindow();
HWND hWnd = (HWND)pWndFrameImpl->GetSafeHwnd();
	if( hWnd == NULL )
		return;
HICON hIcon = (HICON)
		::SendMessage(
			hWnd,
			WM_GETICON,
			WPARAM(ICON_SMALL),
			0
			);
	if( hIcon == NULL )
	{
		hIcon = (HICON)
			::SendMessage(
				hWnd,
				WM_GETICON,
				WPARAM(ICON_BIG),
				0
				);
		if( hIcon == NULL )
		{
			hIcon = (HICON)
#if _MFC_VER >= 0x700
				(INT_PTR)
#endif
					::GetClassLong(
						hWnd,
						__EXT_MFC_GCL_HICONSM
						);
			if( hIcon == NULL )
			{
				hIcon = (HICON)
#if _MFC_VER >= 0x700
					(INT_PTR)
#endif
						::GetClassLong(
							hWnd,
							__EXT_MFC_GCL_HICON
							);
			} // if( hIcon == NULL )
		} // if( hIcon == NULL )
	} // if( hIcon == NULL )
	if( hIcon == NULL )
		return;
	_icon.AssignFromHICON( hIcon, true );
CSize _sizeIcon = _icon.GetSize();
	if( _sizeIcon.cx != 16 || _sizeIcon.cy != 16 )
		_icon.Scale( CSize(16,16) );
	m_iconNcFrameImpl_QueryCache = _icon;
}

void CExtNcFrameImpl::NcFrameImpl_ReCacheScButtonRects() const
{
CWnd * pWndFrameImpl = (CWnd *)NcFrameImpl_GetFrameWindow();
	ASSERT_VALID( pWndFrameImpl );
	CExtCmdIcon _icon;
	NcFrameImpl_GetIcon( _icon );
CString strCaption;
	pWndFrameImpl->GetWindowText( strCaption );
//NcLock _NcLock( * this );
//DWORD dwResetStyles = m_dwNcFrameImpl_StyleInitial&WS_BORDER;
//	if( dwResetStyles != 0 )
//		pWndFrameImpl->ModifyStyle( 0, dwResetStyles );

MINMAXINFO _mmi, * pMinMaxInfo = NULL;
	::memset( &_mmi, 0, sizeof(MINMAXINFO) );
	CExtPaintManager::monitor_parms_t _mp;
	CExtPaintManager::stat_GetMonitorParms( _mp, pWndFrameImpl );
	_mmi.ptMaxPosition.x = _mp.m_rcWorkArea.left;
	_mmi.ptMaxPosition.y = _mp.m_rcWorkArea.top;
	_mmi.ptMaxTrackSize.x = _mp.m_rcWorkArea.Width(); // ::GetSystemMetrics( SM_CXMAXTRACK );
	_mmi.ptMaxTrackSize.y = _mp.m_rcWorkArea.Height(); // ::GetSystemMetrics( SM_CYMAXTRACK );
	_mmi.ptMinTrackSize.x = ::GetSystemMetrics( SM_CXMINTRACK );
	_mmi.ptMinTrackSize.y = ::GetSystemMetrics( SM_CYMINTRACK );
	_mmi.ptMaxSize.x = _mmi.ptMaxTrackSize.x;
	_mmi.ptMaxSize.y = _mmi.ptMaxTrackSize.y;
	if( pWndFrameImpl->SendMessage( WM_GETMINMAXINFO, 0, LPARAM(&_mmi) ) == 0 )
		pMinMaxInfo = &_mmi;

	NcFrameImpl_GetPM()->NcFrame_GetRects(
		&_icon,
		LPCTSTR(strCaption),
		DT_LEFT|DT_VCENTER,
		m_rcNcFrameImpl_Icon,
		m_rcNcFrameImpl_Text,
		m_rcNcFrameImpl_ScHelp,
		m_rcNcFrameImpl_ScMinimize,
		m_rcNcFrameImpl_ScMaximize,
		m_rcNcFrameImpl_ScClose,
		pWndFrameImpl,
		pMinMaxInfo
		);
	if( m_pNcFrameImplBridge != NULL )
	{
		m_pNcFrameImplBridge->NcFrameImplBridge_GetNcScRect( SC_CLOSE, m_rcNcFrameImpl_ScClose );
		m_pNcFrameImplBridge->NcFrameImplBridge_GetNcScRect( SC_MAXIMIZE, m_rcNcFrameImpl_ScMaximize );
		m_pNcFrameImplBridge->NcFrameImplBridge_GetNcScRect( SC_MINIMIZE, m_rcNcFrameImpl_ScMinimize );
		m_pNcFrameImplBridge->NcFrameImplBridge_GetNcScRect( SC_CONTEXTHELP, m_rcNcFrameImpl_ScHelp );
	} // if( m_pNcFrameImplBridge != NULL )

//	if( dwResetStyles != 0 )
//		pWndFrameImpl->ModifyStyle( dwResetStyles, 0 );
}

void CExtNcFrameImpl::NcFrameImpl_DelayRgnAdjustment()
{
	if( NcFrameImpl_IsDwmBased() )
		return;
	m_bNcFrameImpl_RgnSet = false;
}

void CExtNcFrameImpl::NcFrameImpl_SetupRgn(
	WINDOWPOS * pWndPos // = NULL
	)
{
CWnd * pWndFrameImpl = NcFrameImpl_GetFrameWindow();
	ASSERT_VALID( pWndFrameImpl );
	if( NcFrameImpl_IsDwmBased() )
	{
		pWndFrameImpl->SetWindowRgn( NULL, TRUE );
		return;
	}
	if( m_nNcFrameImpl_Lock != 0 )
		return;
	//NcFrameImpl_MapHtRects_Clean();
	m_nNcFrameImpl_LastCheckCursorHT = HTNOWHERE;
	m_ptNcFrameImpl_LastCheckCursor.x = m_ptNcFrameImpl_LastCheckCursor.y = -32767;
	m_bNcFrameImpl_RgnSet = true;
DWORD dwStyle = m_dwNcFrameImpl_StyleInitial; // pWndFrameImpl->GetStyle();
	if( (dwStyle&WS_BORDER) == 0 )
		return;
	CRect rcWnd( 0, 0, 0, 0 );
	if( pWndPos != NULL )
	{
		if( ( pWndPos->flags & SWP_NOSIZE) !=  0 )
			return;
		rcWnd.right = rcWnd.left + pWndPos->cx;
		rcWnd.bottom = rcWnd.top + pWndPos->cy;
	}
	else
	{
		pWndFrameImpl->GetWindowRect( &rcWnd );
		rcWnd.OffsetRect( -rcWnd.TopLeft() );
	}
	m_rcNcFrameImpl_ScClose.SetRectEmpty();
	m_rcNcFrameImpl_ScMaximize.SetRectEmpty();
	m_rcNcFrameImpl_ScMinimize.SetRectEmpty();
	m_rcNcFrameImpl_ScHelp.SetRectEmpty();
HRGN hRgn =
		NcFrameImpl_GetPM()->NcFrame_GenerateSkinFrameRGN(
			rcWnd,
			pWndFrameImpl
			);
	if( m_pNcFrameImplBridge != NULL )
	{

		HRGN hRgnExclude = m_pNcFrameImplBridge->NcFrameImplBridge_GetNcExcludeHRGN();
		if( hRgnExclude != NULL )
		{
			CRect rcBox( 0, 0, 0, 0 ); 
			GetRgnBox( hRgnExclude, &rcBox );
			HRGN hRgnBox = ::CreateRectRgnIndirect( &rcBox );
			::CombineRgn( hRgn, hRgn, hRgnBox, RGN_OR );
			::DeleteObject( hRgnBox );
			::CombineRgn( hRgn, hRgn, hRgnExclude, RGN_XOR );
		}
	}
	pWndFrameImpl->SetWindowRgn( hRgn, TRUE );
}

bool CExtNcFrameImpl::NcFrameImpl_GetMinMaxInfo(
	LPMINMAXINFO pMMI
	) const
{
	ASSERT( pMMI != NULL );
	if( ! NcFrameImpl_IsSupported() )
		return false;
	if( NcFrameImpl_IsDwmBased() )
		return true;
CWnd * pWndFrameImpl = (CWnd *)NcFrameImpl_GetFrameWindow();
	ASSERT_VALID( pWndFrameImpl );
CExtPaintManager::monitor_parms_t _mp;
CExtPaintManager::stat_GetMonitorParms( _mp, pMMI->ptMaxPosition );
CExtPaintManager::monitor_parms_t _mpPrimary, _mpCurrent;
CExtPaintManager::stat_GetMonitorParms( _mpPrimary );
CExtPaintManager::stat_GetMonitorParms( _mpCurrent, pWndFrameImpl );
	if( _mpPrimary.m_rcWorkArea != _mpCurrent.m_rcWorkArea )
		return true;
CSize _maxSize = _mp.m_rcWorkArea.Size();
	if( _mp.m_rcMonitor == _mp.m_rcWorkArea )
	{
		if( pMMI->ptMaxPosition.x < _mp.m_rcWorkArea.left )
			pMMI->ptMaxPosition.x = _mp.m_rcWorkArea.left;
		if( pMMI->ptMaxPosition.y < _mp.m_rcWorkArea.top )
			pMMI->ptMaxPosition.y = _mp.m_rcWorkArea.top;
 		//_maxSize.cx -= 1;
 		_maxSize.cy -= 1;
	} // if( _mp.m_rcMonitor == _mp.m_rcWorkArea )
	if( NcFrameImpl_IsForceEmpty() )
	{
		if( ! NcFrameImpl_IsForceEmptyNcBorderEmpty() )
		{
			CRect rc = NcFrameImpl_GetForceEmptyNcBorder();
			_maxSize.cx += rc.left + rc.right;
			_maxSize.cy += rc.top + rc.bottom;
		} // if( ! NcFrameImpl_IsForceEmptyNcBorderEmpty() )
	} // if( NcFrameImpl_IsForceEmpty() )
	else
	{
		CRect rcNcBorders, rcThemePadding;
		NcFrameImpl_GetPM()->NcFrame_GetMetrics(
			rcNcBorders,
			rcThemePadding,
			pWndFrameImpl
			);
		_maxSize.cx += rcNcBorders.left + rcNcBorders.right;
		_maxSize.cy += rcNcBorders.top + rcNcBorders.bottom;
	} // else from if( NcFrameImpl_IsForceEmpty() )
	if( pMMI->ptMaxSize.x > _maxSize.cx )
		pMMI->ptMaxSize.x = _maxSize.cx;
	if( pMMI->ptMaxSize.y > _maxSize.cy )
		pMMI->ptMaxSize.y = _maxSize.cy;
	return true;
}

__EXT_MFC_SAFE_LPCTSTR CExtNcFrameImpl::NcFrameImpl_GetScTipText( UINT nSC ) const
{
const CWnd * pWndFrameImpl = NcFrameImpl_GetFrameWindow();
	ASSERT_VALID( pWndFrameImpl );
	if( m_rcNcFrameImpl_ScClose.IsRectEmpty() )
		NcFrameImpl_ReCacheScButtonRects();
	switch( nSC )
	{
	case SC_CLOSE:
		return LPCTSTR( m_strNcFrameImpl_TipClose );
	case SC_MAXIMIZE:
	case SC_RESTORE:
		if( pWndFrameImpl->IsZoomed() )
			return LPCTSTR( m_strNcFrameImpl_TipRestore );
		else
			return LPCTSTR( m_strNcFrameImpl_TipMaximize );
	case SC_MINIMIZE:
		return LPCTSTR( m_strNcFrameImpl_TipMinimize );
	} // switch( nSC )
	return NULL;
}

bool CExtNcFrameImpl::NcFrameImpl_IsSupported() const
{
	if( ! m_bNcFrameImpl_IsEnabled )
		return false;
const CWnd * pWndFrameImpl = NcFrameImpl_GetFrameWindow();
	if( pWndFrameImpl->GetSafeHwnd() == NULL )
		return false;
	if( ! NcFrameImpl_GetPM()->NcFrame_IsSupported( pWndFrameImpl ) )
		return false;
CFrameWnd * pFrameWnd = DYNAMIC_DOWNCAST( CFrameWnd, pWndFrameImpl );
	if(		pFrameWnd != NULL
		&&	CExtControlBar::IsOleIpObjActive( pFrameWnd )
		)
	{
		if( m_bNcFrameImpl_RgnSet )
		{
			(((CExtNcFrameImpl*)this)->m_bNcFrameImpl_RgnSet) = false;
			::SetWindowRgn( pWndFrameImpl->m_hWnd, NULL, FALSE );
		} // if( m_bNcFrameImpl_RgnSet )
		return false;
	}
	return true;
}

bool CExtNcFrameImpl::NcFrameImpl_IsDwmBased() const
{
	if( ! NcFrameImpl_IsSupported() )
		return false;
bool bDwmMode = g_PaintManager.m_DWM.IsCompositionEnabled() ? true : false;
	if( ! bDwmMode )
		return false;
	return m_bNcFrameImpl_IsDwmBased;
}

bool CExtNcFrameImpl::NcFrameImpl_IsDwmCaptionReplacement() const
{
	if( ! NcFrameImpl_IsDwmBased() )
		return false;
	if( m_pNcFrameImplBridge != NULL )
	{
		INT nDwmCaptionHeight =
			m_pNcFrameImplBridge->
				NcFrameImplBridge_GetDwmEmbeddedCaptionHeight();
		ASSERT( nDwmCaptionHeight >= 0 );
		if( nDwmCaptionHeight > 0 )
			return true;
	} // if( m_pNcFrameImplBridge != NULL )
	return false;
}

bool CExtNcFrameImpl::NcFrameImpl_IsForceEmpty() const
{
	if( NcFrameImpl_IsDwmBased() )
		return true;
	return false;
}

CRect CExtNcFrameImpl::NcFrameImpl_GetForceEmptyNcBorder() const
{
	return CRect( 0, 0, 0, 0 );
}

bool CExtNcFrameImpl::NcFrameImpl_IsForceEmptyNcBorderEmpty() const
{
	if( NcFrameImpl_IsDwmBased() )
		return true;
CRect rc = NcFrameImpl_GetForceEmptyNcBorder();
	ASSERT( rc.left >= 0 );
	ASSERT( rc.top >= 0 );
	ASSERT( rc.right >= 0 );
	ASSERT( rc.bottom >= 0 );
	if(		rc.left > 0
		||	rc.top > 0
		||	rc.right > 0
		||	rc.bottom > 0
		)
		return false;
	return true;
}

bool CExtNcFrameImpl::NcFrameImpl_IsActive()
{
HWND hWndOwn = NcFrameImpl_OnQueryHWND();
	if( hWndOwn == NULL )
		return false;
	if( m_bNcFrameImpl_IsActive )
		return true;
HWND hWndActive = ::GetActiveWindow();
	if( hWndActive == NULL )
		return false;
	if( hWndActive == hWndOwn )
		return true;
	if( ::IsChild( hWndOwn, hWndActive ) )
		return true;
HWND hWnd = ::GetParent( hWndActive );
	for( ; hWnd != NULL; hWnd = ::GetParent( hWnd ) )
	{
		if( hWndOwn == hWndOwn )
			return true;
	}
	return false;
}

void CExtNcFrameImpl::NcFrameImpl_OnNcPaint(
	CDC & dcPaint,
	bool bOuterEmulationMode // = false
	)
{
	bOuterEmulationMode;
CExtPaintManager * pPM = NcFrameImpl_GetPM();
	ASSERT_VALID( pPM );
CWnd * pWndFrameImpl = NcFrameImpl_GetFrameWindow();
CRect rcNcBorders, rcThemePadding;
	pPM->NcFrame_GetMetrics(
		rcNcBorders,
		rcThemePadding,
		pWndFrameImpl
		);
bool bFrameActive = NcFrameImpl_IsActive();
INT nAdjustCaptionHeight = 0;
	if(		m_pNcFrameImplBridge != NULL
		&&	m_pNcFrameImplBridge->NcFrameImplBridge_OnQueryCaptionMergeMode()
		)
	{
		nAdjustCaptionHeight =
			pPM->NcFrame_GetCaptionHeight(
				bFrameActive,
				pWndFrameImpl
				);
		rcNcBorders.top += nAdjustCaptionHeight;
	}
CRect rcWnd, rcClient;
	pWndFrameImpl->GetWindowRect( &rcWnd );
	pWndFrameImpl->GetClientRect( &rcClient );
CRect rcSavedClient = rcClient;
	pWndFrameImpl->ClientToScreen( &rcClient );
	rcClient.OffsetRect( -rcWnd.TopLeft() );
CRect rcRealClient = rcClient;
	rcClient.top += nAdjustCaptionHeight;
	rcWnd.OffsetRect( -rcWnd.TopLeft() );
DWORD dwOldLayout = dcPaint.SetLayout( LAYOUT_LTR ); // skinned RTL layout currently not supported

	if( bOuterEmulationMode  )
	{
//		dcPaint.ExcludeClipRect( &rcClient );
	}
	else
	{
		dcPaint.ExcludeClipRect( &rcRealClient );
		if( m_pNcFrameImplBridge != NULL && nAdjustCaptionHeight > 0 )
		{
			HWND hWnd = m_pNcFrameImplBridge->NcFrameImplBridge_GetSafeHwnd();
			if( hWnd != NULL )
			{
				CRect rc = rcSavedClient;
				rc.bottom = rc.top + nAdjustCaptionHeight;
				pWndFrameImpl->ClientToScreen( &rc );
				::ScreenToClient( hWnd, (LPPOINT)&rc );
				::ScreenToClient( hWnd, ((LPPOINT)&rc)+1 );
				::InvalidateRect( hWnd, &rc, TRUE );
				::UpdateWindow( hWnd );
			}
		}
	}

	{ // BLOCK: memory DC
		CExtMemoryDC dc( &dcPaint, &rcWnd );
		NcFrameImpl_ReCacheScButtonRects();
		bool bFrameEnabled = pWndFrameImpl->IsWindowEnabled() ? true : false;
		bool bFrameMaximized = pWndFrameImpl->IsZoomed() ? true : false;
		CExtPaintManager::e_nc_button_state_t
			eStateButtonHelp = CExtPaintManager::__ENCBS_DISABLED,
			eStateButtonMinimize = CExtPaintManager::__ENCBS_DISABLED,
			eStateButtonMaximizeRestore = CExtPaintManager::__ENCBS_DISABLED,
			eStateButtonClose = CExtPaintManager::__ENCBS_DISABLED;
		if( ! m_rcNcFrameImpl_ScClose.IsRectEmpty() )
		{
			if( NcFrameImpl_OnQuerySystemCommandEnabled( SC_CLOSE ) )
				eStateButtonClose =
					( m_nNcFrameImpl_ScTrackedButtonPressed == SC_CLOSE )
						? CExtPaintManager::__ENCBS_PRESSED
						: ( ( m_nNcFrameImpl_ScTrackedButtonHover == SC_CLOSE
							&& m_nNcFrameImpl_ScTrackedButtonPressed == 0 )
							? CExtPaintManager::__ENCBS_HOVER
							: CExtPaintManager::__ENCBS_NORMAL )
						;
		}
		if( ! m_rcNcFrameImpl_ScMaximize.IsRectEmpty() )
		{
			if( bFrameMaximized )
			{
				if(		NcFrameImpl_OnQuerySystemCommandEnabled( SC_RESTORE )
					||	NcFrameImpl_OnQuerySystemCommandEnabled( SC_MAXIMIZE )
					)
					eStateButtonMaximizeRestore =
						( m_nNcFrameImpl_ScTrackedButtonPressed == SC_MAXIMIZE )
							? CExtPaintManager::__ENCBS_PRESSED
							: ( ( m_nNcFrameImpl_ScTrackedButtonHover == SC_MAXIMIZE
								&& m_nNcFrameImpl_ScTrackedButtonPressed == 0 )
								? CExtPaintManager::__ENCBS_HOVER
								: CExtPaintManager::__ENCBS_NORMAL )
							;
			}
			else
			{
				if(		NcFrameImpl_OnQuerySystemCommandEnabled( SC_RESTORE )
					||	NcFrameImpl_OnQuerySystemCommandEnabled( SC_MAXIMIZE )
					)
					eStateButtonMaximizeRestore =
						( m_nNcFrameImpl_ScTrackedButtonPressed == SC_MAXIMIZE )
							? CExtPaintManager::__ENCBS_PRESSED
							: ( ( m_nNcFrameImpl_ScTrackedButtonHover == SC_MAXIMIZE
								&& m_nNcFrameImpl_ScTrackedButtonPressed == 0 )
								? CExtPaintManager::__ENCBS_HOVER
								: CExtPaintManager::__ENCBS_NORMAL )
							;
			}
		}
		if( ! m_rcNcFrameImpl_ScMinimize.IsRectEmpty() )
		{
			if( NcFrameImpl_OnQuerySystemCommandEnabled( SC_MINIMIZE ) )
				eStateButtonMinimize =
					( m_nNcFrameImpl_ScTrackedButtonPressed == SC_MINIMIZE )
						? CExtPaintManager::__ENCBS_PRESSED
						: ( ( m_nNcFrameImpl_ScTrackedButtonHover == SC_MINIMIZE
							&& m_nNcFrameImpl_ScTrackedButtonPressed == 0 )
							? CExtPaintManager::__ENCBS_HOVER
							: CExtPaintManager::__ENCBS_NORMAL )
						;
		}
		if( ! m_rcNcFrameImpl_ScHelp.IsRectEmpty() )
		{
			if( NcFrameImpl_OnQuerySystemCommandEnabled( SC_CONTEXTHELP ) )
				eStateButtonHelp =
					( m_nNcFrameImpl_ScTrackedButtonPressed == SC_CONTEXTHELP )
						? CExtPaintManager::__ENCBS_PRESSED
						: ( ( m_nNcFrameImpl_ScTrackedButtonHover == SC_CONTEXTHELP
							&& m_nNcFrameImpl_ScTrackedButtonPressed == 0 )
							? CExtPaintManager::__ENCBS_HOVER
							: CExtPaintManager::__ENCBS_NORMAL )
						;
		}
		bool bDrawIcon = true, bDrawCaptionText = true;
		CExtCmdIcon _icon;
		CString strCaption;
		CRect rcDrawIcon = m_rcNcFrameImpl_Icon;
		CRect rcDrawText = m_rcNcFrameImpl_Text;
		UINT nDtFlags = DT_SINGLELINE|DT_LEFT|DT_VCENTER;
		if( m_pNcFrameImplBridge != NULL )
		{
			bDrawIcon =
				m_pNcFrameImplBridge->NcFrameImplBridge_OnQueryDrawIcon(
					rcDrawIcon
					);
			bDrawCaptionText =
				m_pNcFrameImplBridge->NcFrameImplBridge_OnQueryDrawCaptionText(
					rcDrawText,
					nDtFlags
					);
		} // if( m_pNcFrameImplBridge != NULL )
		if( bDrawIcon )
			NcFrameImpl_GetIcon( _icon );
		pWndFrameImpl->GetWindowText( strCaption );
		pPM->NcFrame_Paint(
			dc,
			bDrawIcon ? (&_icon) : NULL,
			bDrawCaptionText ? LPCTSTR(strCaption) : _T(""),
			nDtFlags,
			rcWnd,
			rcClient,
			rcDrawIcon,
			rcDrawText,
			m_rcNcFrameImpl_ScHelp,
			m_rcNcFrameImpl_ScMinimize,
			m_rcNcFrameImpl_ScMaximize,
			m_rcNcFrameImpl_ScClose,
			bFrameActive,
			bFrameEnabled,
			bFrameMaximized,
			eStateButtonHelp,
			eStateButtonMinimize,
			eStateButtonMaximizeRestore,
			eStateButtonClose,
			pWndFrameImpl
			);
		if( m_pNcFrameImplBridge != NULL )
		{
			if( ! bOuterEmulationMode )
				m_pNcFrameImplBridge->NcFrameImplBridge_OnOverPaint(
					dc,
					rcWnd,
					rcClient
					);
			if( ! bDrawCaptionText )
				m_pNcFrameImplBridge->NcFrameImplBridge_OnDrawCaptionText(
					dc,
					LPCTSTR(strCaption),
					rcDrawText
					);
		}
	} // BLOCK: memory DC
	dcPaint.SetLayout( dwOldLayout );
}

void CExtNcFrameImpl::NcFrameImpl_AdjustVistaDwmCompatibilityIssues()
{
bool bDwmMode = g_PaintManager.m_DWM.IsCompositionEnabled() ? true : false;
	if( ! bDwmMode )
		return;
HWND hWndOwn = NcFrameImpl_OnQueryHWND();
	if( hWndOwn == NULL || ( ! ::IsWindow( hWndOwn ) ) )
		return;

bool bDwmArea = NcFrameImpl_IsDwmCaptionReplacement();
	if( ! bDwmArea )
		return;
bool bCustomNcArea = false;

CExtDWM::__EXT_DWMNCRENDERINGPOLICY ncrp =
		bCustomNcArea
			? CExtDWM::__EXT_DWMNCRP_DISABLED
			: CExtDWM::__EXT_DWMNCRP_ENABLED // CExtDWM::__EXT_DWMNCRP_USEWINDOWSTYLE
			;
HRESULT hr = 
		g_PaintManager.m_DWM.DwmSetWindowAttribute(
			hWndOwn,
			CExtDWM::__EXT_DWMWA_NCRENDERING_POLICY,
			&ncrp,
			sizeof(ncrp)
			);
//	ASSERT( hr == S_OK );
BOOL bAllow = bDwmArea ? TRUE : FALSE;
	hr = 
		g_PaintManager.m_DWM.DwmSetWindowAttribute(
			hWndOwn,
			CExtDWM::__EXT_DWMWA_ALLOW_NCPAINT,
			&bAllow,
			sizeof(bAllow)
			);
//	ASSERT( hr == S_OK );
	hr;

//	NcFrameImpl_RecalcNcFrame();
}

bool CExtNcFrameImpl::NcFrameImpl_PreWindowProc( LRESULT & lResult, UINT message, WPARAM wParam, LPARAM lParam )
{

	m_bNcFrameImpl_RestoreBorder = false;
CWnd * pWndFrameImpl = NcFrameImpl_GetFrameWindow();
	switch( message )
	{
	case WM_SETICON:
		if( wParam == ICON_SMALL && ((HICON)lParam) != NULL )
		{
			m_iconNcFrameImpl_QueryCache.AssignFromHICON( ((HICON)lParam), true );
			m_iconNcFrameImpl_QueryCache.Scale( CSize(16,16) );
		} // if( wParam == ICON_SMALL && ((HICON)lParam) != NULL )
		break;
	case WM_NCACTIVATE:
		if( NcFrameImpl_IsDwmCaptionReplacement() )
		{
			HWND hWnd = m_pNcFrameImplBridge->NcFrameImplBridge_GetSafeHwnd();
			CExtControlBar * pBar = DYNAMIC_DOWNCAST( CExtControlBar, CWnd::FromHandlePermanent( hWnd ) );
			if( pBar != NULL )
			{
				INT nIndex, nCount = pBar->AnimationSite_ClientGetCount();
				for( nIndex = 0; nIndex < nCount; nIndex ++ )
				{
					CExtAnimationClient * pAC = pBar->AnimationSite_ClientGetAt( nIndex );
					ASSERT( pAC != NULL );
					pBar->AnimationSite_ClientProgressStop( pAC );
					pAC->AnimationClient_StateGet( false ).Empty();
					pAC->AnimationClient_StateGet( true ).Empty();
				} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
			} // if( pBar != NULL )
			if( m_pNcFrameImplBridge != NULL )
			{
				if( hWnd != NULL && ::IsWindow( hWnd ) )
					::RedrawWindow(
						hWnd,
						NULL,
						NULL,
						RDW_INVALIDATE|RDW_UPDATENOW|RDW_ERASE|RDW_ERASENOW|RDW_ALLCHILDREN
						);
			}
			HWND hWndOwn = NcFrameImpl_OnQueryHWND();
			ASSERT( hWndOwn != NULL && ::IsWindow(hWndOwn) );
			::SendMessage( hWndOwn, WM_NCPAINT, 0L, 0L );
			break;
		} // if( NcFrameImpl_IsDwmCaptionReplacement() )
		if( NcFrameImpl_IsDwmBased() )
			break;
 		lResult = 1;
 		return true;
	case WM_CREATE:
		NcFrameImpl_MapHtRects_Clean();
		NcFrameImpl_AdjustVistaDwmCompatibilityIssues();
		if( NcFrameImpl_IsSupported() )
		{
			HWND hWndOwn = NcFrameImpl_OnQueryHWND();
			ASSERT( hWndOwn != NULL && ::IsWindow(hWndOwn) );
			DWORD dwStyle = m_dwNcFrameImpl_StyleInitial;
			if( NcFrameImpl_IsDwmBased() )
			{
// 				if( NcFrameImpl_IsDwmCaptionReplacement() )
// 					dwStyle &= ~(WS_CAPTION);
			}
			else
			{
				if( (dwStyle&WS_BORDER) != 0 )
					dwStyle &= ~(WS_BORDER);
			}
			if( dwStyle != m_dwNcFrameImpl_StyleInitial )
				::SetWindowLong( hWndOwn, GWL_STYLE, LONG(dwStyle) );
			NcFrameImpl_RecalcNcFrame();
		}
	break;
	case WM_INITDIALOG:
		NcFrameImpl_AdjustVistaDwmCompatibilityIssues();
		//if( ! m_bNcFrameImpl_RgnSet )
			NcFrameImpl_SetupRgn();
	break;
	case WM_ACTIVATEAPP:
		if( wParam == 0 )
			m_bNcFrameImpl_IsActive = false;
		else
			m_bNcFrameImpl_IsActive = NcFrameImpl_IsActive();
		if(		(! NcFrameImpl_IsForceEmpty() )
			||	(! NcFrameImpl_IsForceEmptyNcBorderEmpty() )
			)
			pWndFrameImpl->SendMessage( WM_NCPAINT );
		else
		{
			if( NcFrameImpl_IsDwmCaptionReplacement() )
			{
				if( m_pNcFrameImplBridge != NULL )
				{
					HWND hWnd = m_pNcFrameImplBridge->NcFrameImplBridge_GetSafeHwnd();
					if( hWnd != NULL && ::IsWindow( hWnd ) )
						::RedrawWindow(
							hWnd,
							NULL,
							NULL,
							RDW_INVALIDATE|RDW_UPDATENOW|RDW_ERASE|RDW_ERASENOW|RDW_ALLCHILDREN
							);
				}
				HWND hWndOwn = NcFrameImpl_OnQueryHWND();
				ASSERT( hWndOwn != NULL && ::IsWindow(hWndOwn) );
				::SendMessage( hWndOwn, WM_NCPAINT, 0L, 0L );
				break;
			} // if( NcFrameImpl_IsDwmCaptionReplacement() )
		}
	break;
	case WM_ACTIVATE:
		m_bNcFrameImpl_IsActive = ( LOWORD(wParam) == WA_INACTIVE ) ? false : true;
		if(		(! NcFrameImpl_IsForceEmpty() )
			||	(! NcFrameImpl_IsForceEmptyNcBorderEmpty() )
			)
		{
			NcFrameImpl_DelayRgnAdjustment();
			NcFrameImpl_SetupRgn();
			pWndFrameImpl->SendMessage( WM_NCPAINT );
		}
		else
		{
			if( NcFrameImpl_IsDwmCaptionReplacement() )
			{
				if( m_pNcFrameImplBridge != NULL )
				{
					HWND hWnd = m_pNcFrameImplBridge->NcFrameImplBridge_GetSafeHwnd();
					if( hWnd != NULL && ::IsWindow( hWnd ) )
						::RedrawWindow(
							hWnd,
							NULL,
							NULL,
							RDW_INVALIDATE|RDW_UPDATENOW|RDW_ERASE|RDW_ERASENOW|RDW_ALLCHILDREN
							);
				}
				HWND hWndOwn = NcFrameImpl_OnQueryHWND();
				ASSERT( hWndOwn != NULL && ::IsWindow(hWndOwn) );
				::SendMessage( hWndOwn, WM_NCPAINT, 0L, 0L );
				break;
			} // if( NcFrameImpl_IsDwmCaptionReplacement() )
		}
	break;
	case WM_NCLBUTTONDBLCLK:
		if( NcFrameImpl_IsForceEmpty() )
			break;
		if( wParam == HTCAPTION )
		{
			HWND hWndOwn = NcFrameImpl_OnQueryHWND();
			ASSERT( hWndOwn != NULL && ::IsWindow(hWndOwn) );
			bool bZoomed = ( ::IsZoomed( hWndOwn ) ) ? true : false;
			UINT nSC = bZoomed ? SC_RESTORE : SC_MAXIMIZE;
			if(		NcFrameImpl_OnQuerySystemCommandEnabled( SC_RESTORE )
				||	NcFrameImpl_OnQuerySystemCommandEnabled( SC_MAXIMIZE )
				)
			{
				::SendMessage(
					hWndOwn,
					WM_SYSCOMMAND,
					WPARAM(nSC),
					lParam
					);
			}
			lResult = 0L;
			return true;
		} // if( wParam == HTCAPTION )
		else if( wParam == HTSYSMENU )
		{
			if( NcFrameImpl_OnQuerySystemCommandEnabled( SC_CLOSE ) )
			{
				HWND hWndOwn = pWndFrameImpl->GetSafeHwnd();
				pWndFrameImpl->ModifyStyle( 0, WS_BORDER );
				::SendMessage(
					hWndOwn,
					WM_SYSCOMMAND,
					SC_CLOSE,
					lParam
					);
				if( ::IsWindow( hWndOwn ) )
				{
					pWndFrameImpl->ModifyStyle( WS_BORDER, 0 );
					pWndFrameImpl->SendMessage( WM_NCPAINT );
				} // if( ::IsWindow( hWndOwn ) )
				lResult = 0L;
				return true;
			} // if( NcFrameImpl_OnQuerySystemCommandEnabled( SC_CLOSE ) )
		} // else if( wParam == HTSYSMENU )
		if( (pWndFrameImpl->GetStyle()&WS_BORDER) != 0 )
		{
			pWndFrameImpl->ModifyStyle( 0, WS_BORDER );
			m_bNcFrameImpl_RestoreBorder = true;
		}
	break;
	case WM_NCHITTEST:
		if( ! NcFrameImpl_IsForceEmpty() )
		{
			if( m_bNcFrameImpl_HelperInsideNcHitTest )
			{
				lResult = HTNOWHERE;
				return true;
			}
			m_bNcFrameImpl_HelperInsideNcHitTest = true;
			CRect rcWnd;
			pWndFrameImpl->GetWindowRect( &rcWnd );
			CPoint pointScreen, pointWnd( -32767, -32767 );
			if( ::GetCursorPos( &pointScreen ) )
			{
				pointWnd = pointScreen;
				pointWnd -= rcWnd.TopLeft();
			}
			if( m_pNcFrameImplBridge != NULL )
			{
				HRGN hRgn = m_pNcFrameImplBridge->NcFrameImplBridge_GetNcResizingHRGN();
				if(		hRgn != NULL
					&&	::PtInRegion( hRgn, pointWnd.x, pointWnd.y )
					)
				{
					lResult = HTTOPLEFT;
					//NcFrameImpl_CheckCursor( pointScreen, LPARAM(lResult) );
					::SetCursor(
						::LoadCursor( NULL, IDC_SIZENWSE )
						);
					m_bNcFrameImpl_HelperInsideNcHitTest = false;
					return true;
				}
			}
			if( NcFrameImpl_GetNcScRect(SC_CLOSE).PtInRect(pointWnd) )
			{
				lResult = HTCAPTION;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(HTCLOSE) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcScRect(SC_MINIMIZE).PtInRect(pointWnd) )
			{
				lResult = HTCAPTION;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(HTMINBUTTON) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcScRect(SC_MAXIMIZE).PtInRect(pointWnd) )
			{
				lResult = HTCAPTION;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(HTMAXBUTTON) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcScRect(SC_CONTEXTHELP).PtInRect(pointWnd) )
			{
				lResult = HTCAPTION;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(HTHELP) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}

			MINMAXINFO _mmi, * pMinMaxInfo = NULL;
			::memset( &_mmi, 0, sizeof(MINMAXINFO) );
			CExtPaintManager::monitor_parms_t _mp;
			CExtPaintManager::stat_GetMonitorParms( _mp, pWndFrameImpl );
			_mmi.ptMaxPosition.x = _mp.m_rcWorkArea.left;
			_mmi.ptMaxPosition.y = _mp.m_rcWorkArea.top;
			_mmi.ptMaxTrackSize.x = _mp.m_rcWorkArea.Width(); // ::GetSystemMetrics( SM_CXMAXTRACK );
			_mmi.ptMaxTrackSize.y = _mp.m_rcWorkArea.Height(); // ::GetSystemMetrics( SM_CYMAXTRACK );
			_mmi.ptMinTrackSize.x = ::GetSystemMetrics( SM_CXMINTRACK );
			_mmi.ptMinTrackSize.y = ::GetSystemMetrics( SM_CYMINTRACK );
			_mmi.ptMaxSize.x = _mmi.ptMaxTrackSize.x;
			_mmi.ptMaxSize.y = _mmi.ptMaxTrackSize.y;
			if( pWndFrameImpl->SendMessage( WM_GETMINMAXINFO, 0, LPARAM(&_mmi) ) == 0 )
				pMinMaxInfo = &_mmi;

			Sleep( 1 ); // this line of code magically fixes 50-100% CPU eating problem when moving mouse over skinable NC areas

			if( NcFrameImpl_GetNcHtRect(HTTOPLEFT,false,false,false,pMinMaxInfo,&rcWnd).PtInRect(pointWnd) )
			{
				lResult = HTTOPLEFT;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(lResult) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcHtRect(HTTOPRIGHT,false,false,false,pMinMaxInfo,&rcWnd).PtInRect(pointWnd) )
			{
				lResult = HTTOPRIGHT;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(lResult) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcHtRect(HTBOTTOMLEFT,false,false,false,pMinMaxInfo,&rcWnd).PtInRect(pointWnd) )
			{
				lResult = HTBOTTOMLEFT;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(lResult) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcHtRect(HTBOTTOMRIGHT,false,false,false,pMinMaxInfo,&rcWnd).PtInRect(pointWnd) )
			{
				lResult = HTBOTTOMRIGHT;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(lResult) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcHtRect(HTTOP,false,false,false,pMinMaxInfo,&rcWnd).PtInRect(pointWnd) )
			{
				lResult = HTTOP;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(lResult) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcHtRect(HTBOTTOM,false,false,false,pMinMaxInfo,&rcWnd).PtInRect(pointWnd) )
			{
				lResult = HTBOTTOM;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(lResult) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcHtRect(HTLEFT,false,false,false,pMinMaxInfo,&rcWnd).PtInRect(pointWnd) )
			{
				lResult = HTLEFT;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(lResult) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcHtRect(HTRIGHT,false,false,false,pMinMaxInfo,&rcWnd).PtInRect(pointWnd) )
			{
				lResult = HTRIGHT;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(lResult) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcHtRect(HTSYSMENU,false,false,false,pMinMaxInfo,&rcWnd).PtInRect(pointWnd) )
			{
				lResult = HTSYSMENU;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(lResult) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			if( NcFrameImpl_GetNcHtRect(HTCAPTION,false,false,false,pMinMaxInfo,&rcWnd).PtInRect(pointWnd) )
			{
				lResult = HTCAPTION;
				NcFrameImpl_CheckCursor( pointScreen, LPARAM(lResult) );
				m_bNcFrameImpl_HelperInsideNcHitTest = false;
				return true;
			}
			m_bNcFrameImpl_HelperInsideNcHitTest = false;
		} // if( ! NcFrameImpl_IsForceEmpty() )
		//lResult = HTCLIENT;
		//return true;
		break;
	case WM_NCCALCSIZE:
		//NcFrameImpl_MapHtRects_Clean();
		if( NcFrameImpl_IsDwmBased() )
			break;
		if( m_nNcFrameImpl_Lock == 0 )
		{
			NCCALCSIZE_PARAMS * pNCCSP =
				reinterpret_cast < NCCALCSIZE_PARAMS * > ( lParam );
			ASSERT( pNCCSP != NULL );
			if( NcFrameImpl_IsForceEmpty() )
			{
				if( ! NcFrameImpl_IsForceEmptyNcBorderEmpty() )
				{
					CRect rc = NcFrameImpl_GetForceEmptyNcBorder();
					pNCCSP->rgrc[0].top += rc.top;
					pNCCSP->rgrc[0].left += rc.left;
					pNCCSP->rgrc[0].right -= rc.right;
					pNCCSP->rgrc[0].bottom -= rc.bottom;
				} // if( ! NcFrameImpl_IsForceEmptyNcBorderEmpty() )
			} // if( NcFrameImpl_IsForceEmpty() )
			else
			{
				CRect rcNcBorders, rcThemePadding;
				NcFrameImpl_GetPM()->NcFrame_GetMetrics(
					rcNcBorders,
					rcThemePadding,
					pWndFrameImpl
					);
				pNCCSP->rgrc[0].top += rcThemePadding.top;
				pNCCSP->rgrc[0].left += rcThemePadding.left;
				pNCCSP->rgrc[0].right -= rcThemePadding.right;
				pNCCSP->rgrc[0].bottom -= rcThemePadding.bottom;
				if(		m_pNcFrameImplBridge != NULL
					&&	m_pNcFrameImplBridge->NcFrameImplBridge_OnQueryCaptionMergeMode()
					)
					pNCCSP->rgrc[0].top -=
						NcFrameImpl_GetPM() -> 
							NcFrame_GetCaptionHeight(
								NcFrameImpl_IsActive(),
								NcFrameImpl_GetFrameWindow()
								);
			} // else from if( NcFrameImpl_IsForceEmpty() )
		}
		lResult = 0;
		return true;
	case WM_STYLECHANGED:
		NcFrameImpl_MapHtRects_Clean();
		if( NcFrameImpl_IsSupported() && ( ! NcFrameImpl_IsDwmBased() ) )
		{
			if( wParam == GWL_EXSTYLE )
			{
				STYLESTRUCT * pSS = (STYLESTRUCT *)lParam;
				if(		pSS != NULL
					&&	(pSS->styleNew&WS_EX_LAYOUT_RTL|WS_EX_RTLREADING) != 0
					)
				{
					pWndFrameImpl->ModifyStyleEx( WS_EX_LAYOUT_RTL|WS_EX_RTLREADING, 0, SWP_FRAMECHANGED );
				}
			}
		}
		break;
	case WM_NCPAINT:
		if( NcFrameImpl_IsDwmBased() )
		{
		} // if( NcFrameImpl_IsDwmBased() )
		else
		{
			if( ! NcFrameImpl_IsForceEmpty() )
			{
				CWindowDC dcPaint( pWndFrameImpl );
				NcFrameImpl_OnNcPaint( dcPaint );
				lResult = 0;
				return true;
			}
		} // else from if( NcFrameImpl_IsDwmBased() )
		break;
	case WM_NCLBUTTONUP:
			if( NcFrameImpl_IsDwmCaptionReplacement() && m_pNcFrameImplBridge != NULL )
			{
				if( wParam == HTCAPTION || wParam == HTSYSMENU )
				{
					HWND hWnd = m_pNcFrameImplBridge->NcFrameImplBridge_GetSafeHwnd();
					if( hWnd != NULL && ::IsWindow( hWnd ) )
					{
						CPoint ptCursor;
						::GetCursorPos( &ptCursor );
						::ScreenToClient( hWnd, &ptCursor );
						CExtControlBar * pBar = DYNAMIC_DOWNCAST( CExtControlBar, CWnd::FromHandlePermanent( hWnd ) );
						if(		pBar != NULL
							&&	pBar->_OnMouseMoveMsg( 0, ptCursor )
							)
						{
							lResult = ::SendMessage( hWnd, WM_LBUTTONUP, 0, MAKELPARAM( ptCursor.x, ptCursor.y ) );
							return true;
						}
					} // if( hWnd != NULL && ::IsWindow( hWnd ) )
				} // if( wParam == HTCAPTION || wParam == HTSYSMENU )
				break;
			} // if( NcFrameImpl_IsDwmCaptionReplacement() && m_pNcFrameImplBridge != NULL )
		break;
	case WM_NCLBUTTONDOWN:
		{
			if( NcFrameImpl_IsDwmCaptionReplacement() && m_pNcFrameImplBridge != NULL )
			{
				if( wParam == HTCAPTION || wParam == HTSYSMENU )
				{
					HWND hWnd = m_pNcFrameImplBridge->NcFrameImplBridge_GetSafeHwnd();
					if( hWnd != NULL && ::IsWindow( hWnd ) )
					{
						CPoint ptCursor;
						::GetCursorPos( &ptCursor );
						::ScreenToClient( hWnd, &ptCursor );
						CExtControlBar * pBar = DYNAMIC_DOWNCAST( CExtControlBar, CWnd::FromHandlePermanent( hWnd ) );
						if(		pBar != NULL
							&&	pBar->_OnMouseMoveMsg( 0, ptCursor )
							)
						{
							lResult = ::SendMessage( hWnd, WM_LBUTTONDOWN, 0, MAKELPARAM( ptCursor.x, ptCursor.y ) );
							return true;
						}
					} // if( hWnd != NULL && ::IsWindow( hWnd ) )
				} // if( wParam == HTCAPTION || wParam == HTSYSMENU )
				break;
			} // if( NcFrameImpl_IsDwmCaptionReplacement() && m_pNcFrameImplBridge != NULL )
			if( NcFrameImpl_IsForceEmpty() )
				break;
			m_nNcFrameImpl_ScTrackedButtonPressed = 0;
			CRect rcWnd;
			pWndFrameImpl->GetWindowRect( &rcWnd );
			CPoint pointWnd;
			::GetCursorPos( &pointWnd );
			pointWnd -= rcWnd.TopLeft();
			if( m_rcNcFrameImpl_ScClose.PtInRect( pointWnd ) )
				m_nNcFrameImpl_ScTrackedButtonPressed = SC_CLOSE;
			else if( m_rcNcFrameImpl_ScMaximize.PtInRect( pointWnd ) )
				m_nNcFrameImpl_ScTrackedButtonPressed = SC_MAXIMIZE;
			else if( m_rcNcFrameImpl_ScMinimize.PtInRect( pointWnd ) )
				m_nNcFrameImpl_ScTrackedButtonPressed = SC_MINIMIZE;
			if( m_nNcFrameImpl_ScTrackedButtonPressed != 0 )
			{
				pWndFrameImpl->SendMessage( WM_NCPAINT );
				if(		m_nNcFrameImpl_ScTrackedButtonPressed != 0
					&&	CWnd::GetCapture() != pWndFrameImpl
					)
					pWndFrameImpl->SetCapture();
			} // if( m_nNcFrameImpl_ScTrackedButtonPressed != 0 )
			else if( m_bNcFrameImpl_Resizing )
			{
				if(		wParam == HTLEFT
					||	wParam == HTRIGHT
					||	wParam == HTTOP
					||	wParam == HTBOTTOM
					||	wParam == HTTOPLEFT
					||	wParam == HTBOTTOMLEFT
					||	wParam == HTTOPRIGHT
					||	wParam == HTBOTTOMRIGHT
					)
				{ // track resizing loop
					HWND hWnd = pWndFrameImpl->GetSafeHwnd();
					::SetCapture( hWnd );
					CPoint ptStart( LOWORD(lParam), HIWORD(lParam) );
					CRect rcWndStart;
					::GetWindowRect( hWnd, rcWndStart );
					bool bStop = false;
					for( ; ! bStop ; )
					{
						::WaitMessage();
						MSG msg;
						// Process all the messages in the message queue
						while( PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) )
						{
							switch( msg.message )
							{
							case WM_ACTIVATEAPP:
							case WM_CANCELMODE:
							case WM_LBUTTONDOWN:
							case WM_LBUTTONUP:
							case WM_LBUTTONDBLCLK:
							case WM_MBUTTONDOWN:
							case WM_MBUTTONUP:
							case WM_MBUTTONDBLCLK:
							case WM_RBUTTONDOWN:
							case WM_RBUTTONUP:
							case WM_RBUTTONDBLCLK:
							case WM_NCLBUTTONDOWN:
							case WM_NCLBUTTONUP:
							case WM_NCLBUTTONDBLCLK:
							case WM_NCRBUTTONDOWN:
							case WM_NCRBUTTONUP:
							case WM_NCRBUTTONDBLCLK:
							case WM_NCMBUTTONDOWN:
							case WM_NCMBUTTONUP:
							case WM_NCMBUTTONDBLCLK:
							case WM_CONTEXTMENU:
								bStop = true;
							break;
							case WM_CAPTURECHANGED:
								if( hWnd != ((HWND)lParam) )
									bStop = true;
							break;
							case WM_SIZE:
							case WM_WINDOWPOSCHANGING:
							case WM_WINDOWPOSCHANGED:
							case WM_ERASEBKGND:
							case WM_NCHITTEST:
							case WM_NCMOUSEMOVE:
								if( ::IsWindow( hWnd ) )
								{
									::PeekMessage( &msg, NULL, 0, 0, PM_REMOVE );
									continue;
								}
								bStop = true;
							break;
							case WM_MOUSEMOVE:
								::PeekMessage( &msg, NULL, 0, 0, PM_REMOVE );
								if( msg.hwnd == hWnd )
								{
									while( ::PeekMessage( &msg, hWnd, WM_MOUSEMOVE, WM_MOUSEMOVE, PM_REMOVE ) );
									CPoint pt;
									if( ! ::GetCursorPos( &pt ) )
									{
										bStop = true;
										break;
									}
									CRect rcWnd = rcWndStart;
									if(		wParam == HTLEFT
										||	wParam == HTTOPLEFT
										||	wParam == HTBOTTOMLEFT
										)
										rcWnd.left = rcWndStart.left + pt.x - ptStart.x;
									if(		wParam == HTRIGHT
										||	wParam == HTTOPRIGHT
										||	wParam == HTBOTTOMRIGHT
										)
										rcWnd.right = rcWndStart.right + pt.x - ptStart.x;
									if(		wParam == HTTOP
										||	wParam == HTTOPLEFT
										||	wParam == HTTOPRIGHT
										)
										rcWnd.top = rcWndStart.top + pt.y - ptStart.y;
									if(		wParam == HTBOTTOM
										||	wParam == HTBOTTOMLEFT
										||	wParam == HTBOTTOMRIGHT
										)
										rcWnd.bottom = rcWndStart.bottom + pt.y - ptStart.y;
									CRect rcWndCurrent;
									::GetWindowRect( hWnd, &rcWndCurrent );
									if( rcWndCurrent != rcWnd )
									{
										// ::MoveWindow(
										// 	hWnd,
										// 	rcWnd.left,
										// 	rcWnd.top,
										// 	rcWnd.Width(),
										// 	rcWnd.Height(),
										// 	TRUE
										// 	);
										// ::SetWindowPos(
										// 	hWnd,
										// 	NULL,
										// 	rcWnd.left,
										// 	rcWnd.top,
										// 	rcWnd.Width(),
										// 	rcWnd.Height(),
										// 	SWP_NOACTIVATE|SWP_NOZORDER|SWP_NOOWNERZORDER
										// 		//|SWP_NOSENDCHANGING|SWP_NOREDRAW
										// 		//|SWP_FRAMECHANGED
										// 	);
										// //NcFrameImpl_SetupRgn( NULL );
										// CWnd::FromHandle(hWnd)->RepositionBars( 0, 0xFFFF, AFX_IDW_PANE_FIRST );
										// SendMessage( hWnd, WM_NCPAINT, 0, 0 );
										// CExtPaintManager::stat_PassPaintMessages();

										UINT nIDFirst = 0, nIDLast = 0xFFFF, nIDLeftOver = AFX_IDW_PANE_FIRST;
										AFX_SIZEPARENTPARAMS layout = { NULL, { 0, 0, 0, 0 }, { 0, 0 }, FALSE };
										HWND hWndLeftOver = NULL;
										layout.bStretch = TRUE;
										CRect rcClientCurrent;
										::GetClientRect( hWnd, &rcClientCurrent );
										layout.rect.right = rcClientCurrent.right + rcWnd.Width() - rcWndCurrent.Width();
										layout.rect.bottom = rcClientCurrent.bottom + rcWnd.Height() - rcWndCurrent.Height();
										//::SendMessage( hWnd, WM_SETREDRAW, 0, 0 );
										layout.hDWP = ::BeginDeferWindowPos( 8 );
										HWND hWndChild = ::GetTopWindow( hWnd );
										for( ; hWndChild != NULL; hWndChild = ::GetNextWindow( hWndChild, GW_HWNDNEXT ) )
										{
											UINT nIDC = _AfxGetDlgCtrlID( hWndChild );
											//CWnd * pWnd = CWnd::FromHandlePermanent( hWndChild );
											if( nIDC == nIDLeftOver )
												hWndLeftOver = hWndChild;
											else
												if(		nIDC >= nIDFirst
													&&	nIDC <= nIDLast
													//&&	pWnd != NULL
													)
													::SendMessage( hWndChild, WM_SIZEPARENT, 0, (LPARAM)&layout );
										}
										if( nIDLeftOver != 0 && hWndLeftOver != NULL )
										{
											CWnd * pLeftOver = CWnd::FromHandle( hWndLeftOver );
											pLeftOver->CalcWindowRect( &layout.rect );
											::AfxRepositionWindow( &layout, hWndLeftOver, &layout.rect );
										}
										// 	layout.hDWP =
										// 		::DeferWindowPos(
										// 			layout.hDWP,
										// 			hWnd,
										// 			NULL,
										// 			rcWnd.left,
										// 			rcWnd.top,
										// 			rcWnd.Width(),
										// 			rcWnd.Height(),
										// 			SWP_NOACTIVATE|SWP_NOZORDER
										// 			);
										if(		layout.hDWP == NULL
											||	( ! ::EndDeferWindowPos( layout.hDWP ) )
											)
										{
											TRACE0("Warning: DeferWindowPos failed - low system resources.\n");
										}
										//::SendMessage( hWnd, WM_SETREDRAW, 1, 0 );
										//::RedrawWindow( hWnd, NULL, NULL, RDW_INVALIDATE|RDW_ERASE|RDW_ALLCHILDREN );
										::MoveWindow(
											hWnd,
											rcWnd.left,
											rcWnd.top,
											rcWnd.Width(),
											rcWnd.Height(),
											TRUE
											);
										SendMessage( hWnd, WM_NCPAINT, 0, 0 );
										CExtPaintManager::stat_PassPaintMessages();
									} // if( rcWndCurrent != rcWnd )
								} // if( msg.hwnd == hWnd )
 								if( ::IsWindow( hWnd ) )
 									continue;
								bStop = true;
							break;
							default:
								if(	WM_KEYFIRST <= msg.message
									&& msg.message <= WM_KEYLAST
									)
									bStop = true;
							break;
							} // switch( msg.message )
							if( ! ::IsWindow( hWnd ) )
								bStop = true;
							if( bStop )
								break;
							if( ! AfxGetThread()->PumpMessage() )
							{
								PostQuitMessage(0);
								break; // Signal WM_QUIT received
							} // if( !AfxGetThread()->PumpMessage() )
						} // while( PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) )
						if( bStop )
							break;
						for(	LONG nIdleCounter = 0L;
								::AfxGetThread()->OnIdle(nIdleCounter);
								nIdleCounter ++
								);
					} // for( ; ! bStop ; )
					if(		::IsWindow( hWnd )
						&&	::GetCapture() == hWnd
						)
						::ReleaseCapture();
					lResult = 0;
					return true;
				} // track resizing loop
			} // else if( m_bNcFrameImpl_Resizing )
		}
	break;
	case WM_LBUTTONDOWN:
		{
			if( NcFrameImpl_IsForceEmpty() )
				break;
			m_nNcFrameImpl_ScTrackedButtonPressed = 0;
			CRect rcWnd;
			pWndFrameImpl->GetWindowRect( &rcWnd );
			CPoint pointWnd;
			::GetCursorPos( &pointWnd );
			pointWnd -= rcWnd.TopLeft();
			if( m_rcNcFrameImpl_ScClose.PtInRect( pointWnd ) )
				m_nNcFrameImpl_ScTrackedButtonPressed = SC_CLOSE;
			else if( m_rcNcFrameImpl_ScMaximize.PtInRect( pointWnd ) )
				m_nNcFrameImpl_ScTrackedButtonPressed = SC_MAXIMIZE;
			else if( m_rcNcFrameImpl_ScMinimize.PtInRect( pointWnd ) )
				m_nNcFrameImpl_ScTrackedButtonPressed = SC_MINIMIZE;
			if( m_nNcFrameImpl_ScTrackedButtonPressed != 0 )
			{
				pWndFrameImpl->SendMessage( WM_NCPAINT );
				if(		m_nNcFrameImpl_ScTrackedButtonPressed != 0
					&&	CWnd::GetCapture() != pWndFrameImpl
					)
					pWndFrameImpl->SetCapture();
			} // if( m_nNcFrameImpl_ScTrackedButtonPressed != 0 )
		}
	break;
	case WM_LBUTTONUP:
		{
			if( NcFrameImpl_IsForceEmpty() )
				break;
			if( m_nNcFrameImpl_ScTrackedButtonPressed == 0 )
				break;
			UINT nSC = m_nNcFrameImpl_ScTrackedButtonPressed;
			if( nSC == SC_MAXIMIZE && pWndFrameImpl->IsZoomed() )
				nSC = SC_RESTORE;
			m_nNcFrameImpl_ScTrackedButtonHover = m_nNcFrameImpl_ScTrackedButtonPressed = 0;
			m_wndNcFrameImpl_Tip.Hide();
			if( CWnd::GetCapture() == pWndFrameImpl )
				::ReleaseCapture();
			//if( nSC != SC_CLOSE )
				pWndFrameImpl->SendMessage( WM_NCPAINT );
			CRect rcButton = NcFrameImpl_GetNcScRect( nSC );
			CRect rcWnd;
			pWndFrameImpl->GetWindowRect( &rcWnd );
			CPoint pointWnd;
			::GetCursorPos( &pointWnd );
			pointWnd -= rcWnd.TopLeft();
			if( rcButton.PtInRect(pointWnd) )
			{
				HWND hWndOwn = pWndFrameImpl->GetSafeHwnd();
				ASSERT( hWndOwn != NULL && ::IsWindow(hWndOwn) );
				bool bEnabled = NcFrameImpl_OnQuerySystemCommandEnabled( nSC );
				if( ! bEnabled )
				{
					if( nSC == SC_MAXIMIZE )
						bEnabled = NcFrameImpl_OnQuerySystemCommandEnabled( SC_RESTORE );
					else if( nSC == SC_RESTORE )
						bEnabled = NcFrameImpl_OnQuerySystemCommandEnabled( SC_MAXIMIZE );
				}
				if( bEnabled )
				{
					pWndFrameImpl->ModifyStyle( 0, WS_BORDER );

//					CExtPaintManager::monitor_parms_t _mp;
//					CExtPaintManager::stat_GetMonitorParms( _mp, pWndFrameImpl );
//					WINDOWPLACEMENT _wp;
//					::memset( &_wp, 0, sizeof(WINDOWPLACEMENT) );
//					if( pWndFrameImpl->GetWindowPlacement( &_wp ) )
//					{
//						_wp.ptMaxPosition.x = _mp.m_rcWorkArea.left;
//						_wp.ptMaxPosition.x = _mp.m_rcWorkArea.top;
//						pWndFrameImpl->SetWindowPlacement( &_wp );
//					}

					pWndFrameImpl->SendMessage( WM_SYSCOMMAND, nSC );

// 					if( nSC == SC_MAXIMIZE )
// 						pWndFrameImpl->ShowWindow( SW_SHOWMAXIMIZED );
// 					else if( nSC == SC_RESTORE )
// 						pWndFrameImpl->ShowWindow( SW_RESTORE );

					if( ::IsWindow( hWndOwn ) )
					{
						pWndFrameImpl->ModifyStyle( WS_BORDER, 0 );
						if( nSC == SC_CLOSE )
							pWndFrameImpl->SendMessage( WM_NCPAINT );
					} // if( ::IsWindow( hWndOwn ) )
				}
			} // if( rcButton.PtInRect(pointWnd) )
		}
		lResult = 0;
		return true;
	case WM_MOUSEMOVE:
		{
			if( NcFrameImpl_IsForceEmpty() )
				break;
			if( m_nNcFrameImpl_ScTrackedButtonHover == 0 )
				break;
			CRect rcWnd;
			pWndFrameImpl->GetWindowRect( &rcWnd );
			CPoint pointWnd;
			if( ::GetCursorPos( &pointWnd ) )
			{
				pWndFrameImpl->PostMessage( WM_NCHITTEST, 0, MAKELPARAM( pointWnd.x, pointWnd.y ) );
				pointWnd -= rcWnd.TopLeft();
			}
			else
				pointWnd.x = pointWnd.y = -32767;
			UINT nScTrackedButtonHover = 0;
			if( m_rcNcFrameImpl_ScClose.PtInRect( pointWnd ) )
				nScTrackedButtonHover = SC_CLOSE;
			else if( m_rcNcFrameImpl_ScMaximize.PtInRect( pointWnd ) )
				nScTrackedButtonHover = SC_MAXIMIZE;
			else if( m_rcNcFrameImpl_ScMinimize.PtInRect( pointWnd ) )
				nScTrackedButtonHover = SC_MINIMIZE;
			if( m_nNcFrameImpl_ScTrackedButtonHover != nScTrackedButtonHover )
			{
				m_nNcFrameImpl_ScTrackedButtonHover = nScTrackedButtonHover;
				pWndFrameImpl->SendMessage( WM_NCPAINT );
				if(		m_nNcFrameImpl_ScTrackedButtonHover == 0
					&&	m_nNcFrameImpl_ScTrackedButtonPressed == 0
					)
				{
					if( CWnd::GetCapture() == pWndFrameImpl )
						::ReleaseCapture();
					m_wndNcFrameImpl_Tip.Hide();
				}
				else
				{
					LPCTSTR strTipText = NcFrameImpl_GetScTipText( m_nNcFrameImpl_ScTrackedButtonHover );
					if( strTipText == NULL || _tcslen(strTipText) == 0 )
						m_wndNcFrameImpl_Tip.Hide();
					else
					{
						CRect rcItem = NcFrameImpl_GetNcScRect( m_nNcFrameImpl_ScTrackedButtonHover );
						rcItem.OffsetRect( rcWnd.TopLeft() );
						m_wndNcFrameImpl_Tip.SetText( strTipText );
						m_wndNcFrameImpl_Tip.Show( pWndFrameImpl, rcItem );
					}
				}
				lResult = 0;
				return true;
			} // if( m_nNcFrameImpl_ScTrackedButtonHover != nScTrackedButtonHover )
		}
		lResult = 0;
		return true;
	case WM_NCMOUSEMOVE:
		{
			if( NcFrameImpl_IsDwmCaptionReplacement() && m_pNcFrameImplBridge != NULL )
			{
				if( wParam == HTCAPTION || wParam == HTSYSMENU )
				{
					HWND hWnd = m_pNcFrameImplBridge->NcFrameImplBridge_GetSafeHwnd();
					if( hWnd != NULL && ::IsWindow( hWnd ) )
					{
						CPoint ptCursor;
						::GetCursorPos( &ptCursor );
						::ScreenToClient( hWnd, &ptCursor );
						CExtControlBar * pBar = DYNAMIC_DOWNCAST( CExtControlBar, CWnd::FromHandlePermanent( hWnd ) );
						if( pBar != NULL )
						{
							if( pBar->_OnMouseMoveMsg( 0, ptCursor ) )
							{
								lResult = 0L;
								return true;
							}
						}
						else
						{
							lResult = ::SendMessage( hWnd, WM_MOUSEMOVE, 0, MAKELPARAM( ptCursor.x, ptCursor.y ) );
							return true;
						}
					} // if( hWnd != NULL && ::IsWindow( hWnd ) )
				} // if( wParam == HTCAPTION || wParam == HTSYSMENU )
				break;
			} // if( NcFrameImpl_IsDwmCaptionReplacement() && m_pNcFrameImplBridge != NULL )
			if( NcFrameImpl_IsForceEmpty() )
				break;
			if( m_rcNcFrameImpl_ScClose.IsRectEmpty() )
				NcFrameImpl_ReCacheScButtonRects();
			CRect rcWnd;
			pWndFrameImpl->GetWindowRect( &rcWnd );
			CPoint pointWnd;
			if( ::GetCursorPos( &pointWnd ) )
			{
				pWndFrameImpl->PostMessage( WM_NCHITTEST, 0, MAKELPARAM( pointWnd.x, pointWnd.y ) );
				pointWnd -= rcWnd.TopLeft();
			}
			else
				pointWnd.x = pointWnd.y = -32767;
			UINT nScTrackedButtonHover = 0;
			if( m_rcNcFrameImpl_ScClose.PtInRect( pointWnd ) )
				nScTrackedButtonHover = SC_CLOSE;
			else if( m_rcNcFrameImpl_ScMaximize.PtInRect( pointWnd ) )
				nScTrackedButtonHover = SC_MAXIMIZE;
			else if( m_rcNcFrameImpl_ScMinimize.PtInRect( pointWnd ) )
				nScTrackedButtonHover = SC_MINIMIZE;
			if( m_nNcFrameImpl_ScTrackedButtonHover != nScTrackedButtonHover )
			{
				m_nNcFrameImpl_ScTrackedButtonHover = nScTrackedButtonHover;
				pWndFrameImpl->SendMessage( WM_NCPAINT );
				if( m_nNcFrameImpl_ScTrackedButtonHover != 0 )
				{
					if( CWnd::GetCapture() != pWndFrameImpl )
						pWndFrameImpl->SetCapture();
					LPCTSTR strTipText = NcFrameImpl_GetScTipText( m_nNcFrameImpl_ScTrackedButtonHover );
					if( strTipText == NULL || _tcslen(strTipText) == 0 )
						m_wndNcFrameImpl_Tip.Hide();
					else
					{
						CRect rcItem = NcFrameImpl_GetNcScRect( m_nNcFrameImpl_ScTrackedButtonHover );
						rcItem.OffsetRect( rcWnd.TopLeft() );
						m_wndNcFrameImpl_Tip.SetText( strTipText );
						m_wndNcFrameImpl_Tip.Show( pWndFrameImpl, rcItem );
					}
				}
				else
					m_wndNcFrameImpl_Tip.Hide();
				lResult = 0;
				return true;
			} // if( m_nNcFrameImpl_ScTrackedButtonHover != nScTrackedButtonHover )
		}
	break;
	case WM_SYSCOMMAND:
		{
			UINT nSC = UINT( wParam );
			if( ! NcFrameImpl_OnQuerySystemCommandEnabled( nSC ) )
			{
				lResult = 0;
				return true;
			}
			if( NcFrameImpl_OnQueryQuickWindowPlacement() )
			{
				if(		nSC == SC_MAXIMIZE
					||	nSC == SC_MINIMIZE
					||	nSC == SC_RESTORE
					)
				{
					WINDOWPLACEMENT _wp;
					::memset( &_wp, 0, sizeof(WINDOWPLACEMENT) );
					HWND hWndOwn = NcFrameImpl_OnQueryHWND();
					ASSERT( hWndOwn != NULL && ::IsWindow(hWndOwn) );
					if( ::GetWindowPlacement( hWndOwn, &_wp ) )
					{
						switch( nSC )
						{
						case SC_MAXIMIZE:
							_wp.showCmd = SW_SHOWMAXIMIZED;
						break;
						case SC_MINIMIZE:
							_wp.showCmd = SW_SHOWMINIMIZED;
						break;
						case SC_RESTORE:
							_wp.showCmd = SW_SHOWNORMAL;
						break;
#ifdef _DEBUG
						default:
							ASSERT( FALSE );
						break;
#endif // _DEBUG
						} // switch( nSC )
						::SetWindowPlacement( hWndOwn, &_wp );
					} // if( ::GetWindowPlacement( hWndOwn, &_wp ) )
					lResult = 0;
					return true;
				}
			} // if( NcFrameImpl_OnQueryQuickWindowPlacement() )
		}
	break;
	case WM_SETTINGCHANGE:
	case WM_DISPLAYCHANGE:
	case WM_DESTROY:
	case WM_NCDESTROY:
		NcFrameImpl_MapHtRects_Clean();
		break;
	default:
		if( message == g_nMsgFindExtNcFrameImpl )
		{
			CExtNcFrameImpl ** ppExtNcFrameImpl = (CExtNcFrameImpl **)wParam;
			(*ppExtNcFrameImpl) = this;
			m_pNcFrameImplBridge = (IExtNcFrameImplBridge*)lParam;
			return true;
		} // if( message == g_nMsgFindExtNcFrameImpl )
	break;
	} // switch( message )
	return false;
}

bool CExtNcFrameImpl::NcFrameImpl_OnQuerySystemCommandEnabled(
	UINT nSystemCommandID
	)
{
CWnd * pWndFrameImpl = NcFrameImpl_GetFrameWindow();
	if( pWndFrameImpl == NULL )
		return false;
	if(		nSystemCommandID == SC_CLOSE
// 		||	nSystemCommandID == SC_MINIMIZE
// 		||	nSystemCommandID == SC_MAXIMIZE
// 		||	nSystemCommandID == SC_RESTORE
// 		||	nSystemCommandID == SC_SIZE
// 		||	nSystemCommandID == SC_MOVE
		)
	{
		CMenu * pSysMenu = pWndFrameImpl->GetSystemMenu( FALSE );
		if( pSysMenu == NULL )
			return false;
		UINT _nSystemCommandID = nSystemCommandID;
// 		if( _nSystemCommandID == SC_RESTORE )
//  			_nSystemCommandID = SC_MAXIMIZE;
		UINT nMenuItemState =
			pSysMenu->GetMenuState( _nSystemCommandID, MF_BYCOMMAND );
		if( nMenuItemState == 0xFFFFFFFF )
			return false;
		if( (nMenuItemState&(MF_DISABLED|MF_GRAYED)) != 0 )
			return false;
	}
DWORD dwStyle = pWndFrameImpl->GetStyle();
	if( nSystemCommandID == SC_MINIMIZE )
	{
		if( (dwStyle&WS_MINIMIZEBOX) == 0 )
			return false;
	}
	if( nSystemCommandID == SC_MAXIMIZE )
	{
		if( (dwStyle&WS_MAXIMIZEBOX) == 0 )
			return false;
	}
	
	if( nSystemCommandID == SC_RESTORE || nSystemCommandID == SC_MAXIMIZE )
	{
		MINMAXINFO _mmi;
		::memset( &_mmi, 0, sizeof(MINMAXINFO) );
		CExtPaintManager::monitor_parms_t _mp;
		CExtPaintManager::stat_GetMonitorParms( _mp, pWndFrameImpl );
		_mmi.ptMaxPosition.x = _mp.m_rcWorkArea.left;
		_mmi.ptMaxPosition.y = _mp.m_rcWorkArea.top;
		_mmi.ptMaxTrackSize.x = _mp.m_rcWorkArea.Width(); // ::GetSystemMetrics( SM_CXMAXTRACK );
		_mmi.ptMaxTrackSize.y = _mp.m_rcWorkArea.Height(); // ::GetSystemMetrics( SM_CYMAXTRACK );
		_mmi.ptMinTrackSize.x = ::GetSystemMetrics( SM_CXMINTRACK );
		_mmi.ptMinTrackSize.y = ::GetSystemMetrics( SM_CYMINTRACK );
		_mmi.ptMaxSize.x = _mmi.ptMaxTrackSize.x;
		_mmi.ptMaxSize.y = _mmi.ptMaxTrackSize.y;
		if( pWndFrameImpl->SendMessage( WM_GETMINMAXINFO, 0, LPARAM(&_mmi) ) == 0 )
		{
			if( nSystemCommandID == SC_RESTORE )
			{
				WINDOWPLACEMENT _wp;
				::memset( &_wp, 0, sizeof(WINDOWPLACEMENT) );
				_wp.length = sizeof(WINDOWPLACEMENT);
				pWndFrameImpl->GetWindowPlacement( &_wp );
				if(		_wp.showCmd == SW_SHOWMINIMIZED
					||	_wp.showCmd == SW_SHOWMINNOACTIVE
					||	_wp.showCmd == SW_RESTORE
					)
					return true;
			}
			if(		_mmi.ptMinTrackSize.x >= _mmi.ptMaxTrackSize.x
				||	_mmi.ptMinTrackSize.y >= _mmi.ptMaxTrackSize.y
				)
				return false;
		}
	}
	return true;
}

CExtPaintManager * CExtNcFrameImpl::NcFrameImpl_GetPM()
{
	return m_BridgeNC.PmBridge_GetPM();
}

CExtPaintManager * CExtNcFrameImpl::NcFrameImpl_GetPM() const
{
	return
		( const_cast < CExtNcFrameImpl * > ( this ) )
			-> NcFrameImpl_GetPM();
}

CExtNcFrameImpl * CExtNcFrameImpl::NcFrameImpl_FindInstance(
	HWND hWnd,
	IExtNcFrameImplBridge * pNcFrameImplBridge // = NULL
	)
{
	for( ; hWnd != NULL && ::IsWindow( hWnd ); hWnd = ::GetParent( hWnd ) )
	{
		if( ( ::GetWindowLong( hWnd, GWL_STYLE ) & WS_CHILD ) != 0 )
			continue;
		CExtNcFrameImpl * pExtNcFrameImpl = NULL;
		::SendMessage(
			hWnd,
			g_nMsgFindExtNcFrameImpl,
			WPARAM(&pExtNcFrameImpl),
			LPARAM(pNcFrameImplBridge)
			);
		if( pExtNcFrameImpl != NULL )
			return pExtNcFrameImpl;
	} // for( ; hWnd != NULL && ::IsWindow( hWnd ); hWnd = ::GetParent( hWnd ) )
	return NULL;
}

void CExtNcFrameImpl::NcFrameImpl_CheckCursor(
	CPoint pointScreen,
	LPARAM nHT,
	bool bCheckWindowFromPoint // = true
	)
{
	if(		(! NcFrameImpl_IsSupported() )
		||	NcFrameImpl_IsDwmBased()
		||	(! ::GetCursorPos(&pointScreen) ) // reset
		)
	{
		m_nNcFrameImpl_LastCheckCursorHT = HTNOWHERE;
		m_ptNcFrameImpl_LastCheckCursor.x = m_ptNcFrameImpl_LastCheckCursor.y = -32767;
		return;
	}
HWND hWndOwn = NcFrameImpl_OnQueryHWND();
	if(		hWndOwn == NULL
		||	(	bCheckWindowFromPoint
			&&	::WindowFromPoint( pointScreen ) != hWndOwn
			)
		)
	{
		m_nNcFrameImpl_LastCheckCursorHT = HTNOWHERE;
		m_ptNcFrameImpl_LastCheckCursor.x = m_ptNcFrameImpl_LastCheckCursor.y = -32767;
		return;
	}
	if(		m_ptNcFrameImpl_LastCheckCursor == pointScreen
		&&	m_nNcFrameImpl_LastCheckCursorHT == nHT
		)
		return;
	m_ptNcFrameImpl_LastCheckCursor = pointScreen;
	m_nNcFrameImpl_LastCheckCursorHT = nHT;
	::SendMessage( hWndOwn, WM_SETCURSOR, WPARAM(hWndOwn), nHT );
}

void CExtNcFrameImpl::NcFrameImpl_PostWindowProc( LRESULT & lResult, UINT message, WPARAM wParam, LPARAM lParam )
{
	lResult;
	switch( message )
	{
 	case WM_NCPAINT:
		if( NcFrameImpl_IsDwmCaptionReplacement() && m_pNcFrameImplBridge != NULL )
		{
			HWND hWnd = m_pNcFrameImplBridge->NcFrameImplBridge_GetSafeHwnd();
			if( hWnd != NULL && ::IsWindow( hWnd ) )
			{
 				CWnd * pWndFrameImpl = (CWnd *)NcFrameImpl_GetFrameWindow();
 				ASSERT_VALID( pWndFrameImpl );
 				CWindowDC dcWnd( pWndFrameImpl );
				//dcWnd.SelectClipRgn( NULL );
				CRect rcWnd, rcClient;
				pWndFrameImpl->GetWindowRect( &rcWnd );
				pWndFrameImpl->GetClientRect( &rcClient );
				pWndFrameImpl->ClientToScreen( &rcClient );
				rcClient.OffsetRect( -rcWnd.TopLeft() );
				rcWnd.OffsetRect( -rcWnd.TopLeft() );
				dcWnd.ExcludeClipRect( &rcClient );
				CExtMemoryDC dc( &dcWnd, &rcWnd );
				dc.FillSolidRect( &rcWnd, RGB(0,0,0) );
				CPoint ptViewPortOffset(
					::GetSystemMetrics( SM_CXSIZEFRAME ),
					::GetSystemMetrics( SM_CYCAPTION )
						- NcFrameImpl_GetPM()->NcFrame_GetCaptionHeight( true, pWndFrameImpl )
						+ ::GetSystemMetrics( SM_CYSIZEFRAME )
					);
				dc.OffsetViewportOrg( ptViewPortOffset.x, ptViewPortOffset.y );
				::SendMessage( hWnd, WM_PRINTCLIENT, WPARAM(dc.m_hDC), PRF_CLIENT );
				dc.OffsetViewportOrg( -ptViewPortOffset.x, -ptViewPortOffset.y );
			} // if( hWnd != NULL && ::IsWindow( hWnd ) )
		} // if( NcFrameImpl_IsDwmCaptionReplacement() && m_pNcFrameImplBridge != NULL )
 	break;
	case WM_PRINT:
	case WM_PRINTCLIENT:
		{
			CWnd * pWndFrameImpl = (CWnd *)NcFrameImpl_GetFrameWindow();
			ASSERT_VALID( pWndFrameImpl );

			CDC dc;
			dc.Attach( (HDC)wParam );

			CRect rcRgnWnd, rcRgnClient;
			pWndFrameImpl->GetWindowRect( &rcRgnWnd );
			pWndFrameImpl->GetClientRect( &rcRgnClient );

			if(		(lParam&PRF_NONCLIENT) != 0
				&&	NcFrameImpl_IsSupported()
				&&	( ! NcFrameImpl_IsDwmBased() )
				)
			{
				CRect rcWnd = rcRgnWnd, rcClient = rcRgnClient;
				pWndFrameImpl->ClientToScreen( &rcClient );
				rcClient.OffsetRect( -rcWnd.TopLeft() );
				rcWnd.OffsetRect( -rcWnd.TopLeft() );
				CRgn rgnWnd;
				if( rgnWnd.CreateRectRgnIndirect(&rcWnd) )
					dc.SelectClipRgn( &rgnWnd );
				dc.ExcludeClipRect( &rcClient );
				NcFrameImpl_OnNcPaint( dc );
				dc.SelectClipRgn( NULL );
			}
			if( (lParam&PRF_CHILDREN) != 0 )
				CExtPaintManager::stat_PrintChildren(
					pWndFrameImpl->m_hWnd,
					message,
					dc.GetSafeHdc(),
					lParam,
					false
					);
			dc.Detach();
		} // if( NcFrameImpl_IsSupported() && ( ! NcFrameImpl_IsDwmBased() ) )
	break;
	case WM_NCLBUTTONDBLCLK:
		if( NcFrameImpl_IsDwmBased() )
			break;
		if( m_bNcFrameImpl_RestoreBorder )
		{
			m_bNcFrameImpl_RestoreBorder = false;
			CWnd * pWndFrameImpl = NcFrameImpl_GetFrameWindow();
			pWndFrameImpl->ModifyStyle( WS_BORDER, 0 );
		}
	break;
	case WM_WINDOWPOSCHANGING:
		if( NcFrameImpl_IsDwmBased() )
			break;
		{
			LPWINDOWPOS lpWindowPos = 
				reinterpret_cast < LPWINDOWPOS > (lParam);
			ASSERT( lpWindowPos != NULL );
			//lpWindowPos->flags |= SWP_FRAMECHANGED;
			m_bNcFrameImpl_DelatayedFrameRecalc =
				( ( lpWindowPos->flags & SWP_FRAMECHANGED ) == 0 )
					? true : false;
			NcFrameImpl_SetupRgn( (WINDOWPOS *)lParam );
		}
	break;
	case WM_WINDOWPOSCHANGED:
		if( NcFrameImpl_IsDwmBased() )
			break;
//		if( m_bNcFrameImpl_DelatayedFrameRecalc )
//		{
		m_bNcFrameImpl_DelatayedFrameRecalc = false;
//			NcFrameImpl_RecalcNcFrame();
//		}
	break;
	case WM_SIZE:
		NcFrameImpl_MapHtRects_Clean();
		if( NcFrameImpl_IsDwmCaptionReplacement() )
			NcFrameImpl_RecalcNcFrame();
		else if( ! NcFrameImpl_IsDwmBased() )
		{
			CWnd * pWndFrameImpl = NcFrameImpl_GetFrameWindow();
			if( (pWndFrameImpl->GetExStyle()&WS_EX_LAYOUTRTL) != 0 )
			{
				m_bNcFrameImpl_DelatayedFrameRecalc = false;
				NcFrameImpl_SetupRgn( NULL );
			}
		}
	break;
	case WM_GETMINMAXINFO:
		if( NcFrameImpl_IsSupported() )
		{
			LPMINMAXINFO pMMI = (LPMINMAXINFO)lParam;
			ASSERT( pMMI != NULL );
			VERIFY( NcFrameImpl_GetMinMaxInfo( pMMI ) );
		} // if( NcFrameImpl_IsSupported() )
	break;
	} // switch( message )
}

bool CExtNcFrameImpl::NcFrameImpl_OnQueryQuickWindowPlacement() const
{
	return m_bNcFrameImpl_QuickWindowPlacement;
}

bool CExtNcFrameImpl::NcFrameImpl_RecalcNcFrame()
{
	if( m_nNcFrameImpl_Lock != 0 )
		return false;
HWND hWndOwn = NcFrameImpl_OnQueryHWND();
	if( hWndOwn == NULL )
		return false;
	ASSERT( ::IsWindow( hWndOwn ) );
	::SetWindowPos(
		hWndOwn, NULL, 0, 0, 0, 0,
		SWP_FRAMECHANGED
			|SWP_NOACTIVATE
			|SWP_NOZORDER
			|SWP_NOOWNERZORDER
			|SWP_NOMOVE
			|SWP_NOSIZE
		);
	return true;
}

void CExtNcFrameImpl::PreSubclassWindow()
{
HWND hWndOwn = NcFrameImpl_OnQueryHWND();
	ASSERT( hWndOwn != NULL && ::IsWindow( hWndOwn ) );
	m_dwNcFrameImpl_StyleInitial = (DWORD)::GetWindowLong( hWndOwn, GWL_STYLE );
	m_dwNcFrameImpl_StyleExInitial = (DWORD)::GetWindowLong( hWndOwn, GWL_EXSTYLE );
	if(		(m_dwNcFrameImpl_StyleInitial&(WS_BORDER|WS_CAPTION)) != 0
		&&	NcFrameImpl_IsSupported()
		)
	{
		NcFrameImpl_DelayRgnAdjustment();
		DWORD dwStyle = m_dwNcFrameImpl_StyleInitial;
		if( NcFrameImpl_IsDwmBased() )
		{
// 			if( NcFrameImpl_IsDwmCaptionReplacement() )
// 				dwStyle &= ~(WS_CAPTION);
		}
		else
		{
			if( (dwStyle&WS_BORDER) != 0 )
				dwStyle &= ~(WS_BORDER);
		}
		if( dwStyle != m_dwNcFrameImpl_StyleInitial )
		{
			::SetWindowLong( hWndOwn, GWL_STYLE, LONG(dwStyle) );
		}
	}
	NcFrameImpl_MapHtRects_Clean();
	NcFrameImpl_RecalcNcFrame();
}

void CExtNcFrameImpl::PostNcDestroy()
{
	m_pNcFrameImplBridge = NULL;
	NcFrameImpl_DelayRgnAdjustment();
	m_nNcFrameImpl_LastCheckCursorHT = HTNOWHERE;
	m_ptNcFrameImpl_LastCheckCursor.x = m_ptNcFrameImpl_LastCheckCursor.y = -32767;
	m_iconNcFrameImpl_QueryCache.Empty();
}

IMPLEMENT_CExtPmBridge_MEMBERS( CExtNcFrameWatchMDI );

CExtNcFrameWatchMDI::CExtNcFrameWatchMDI()
	: m_pNcFrameImpl( NULL )
	, m_nMsgDelayedRecalc(
		::RegisterWindowMessage(
			_T("CExtNcFrameWatchMDI::m_nMsgDelayedRecalc")
			)
		)
{
	PmBridge_Install();
}

CExtNcFrameWatchMDI::~CExtNcFrameWatchMDI()
{
	PmBridge_Uninstall();
}

void CExtNcFrameWatchMDI::PostSync()
{
HWND hWnd = m_pNcFrameImpl->NcFrameImpl_OnQueryHWND();
	if( hWnd == NULL )
		return;
	ASSERT( ::IsWindow( hWnd ) );
HWND hWndMdiClient = ::GetParent( hWnd );
	hWnd = hWndMdiClient;
	ASSERT( hWnd != NULL && ::IsWindow( hWnd ) );
	for(	hWnd = ::GetWindow( hWnd, GW_CHILD );
			hWnd != NULL;
			hWnd = ::GetWindow( hWnd, GW_HWNDNEXT )
			)
		::PostMessage(
			hWnd,
			m_nMsgDelayedRecalc,
			0,
			0
			);
//BOOL bMax = FALSE;
//	hWnd = (HWND)
//		::SendMessage(
//			hWndMdiClient,
//			WM_MDIGETACTIVE,
//			0,
//			(LPARAM)&bMax
//			);
//	if( hWnd != NULL && bMax )
//	{
//		CWnd * pWndMdiClient = CWnd::FromHandle( hWndMdiClient );
//		CRect rcAdjust;
//		pWndMdiClient->GetClientRect( &rcAdjust );
//		CWnd * pWnd = CWnd::FromHandle( hWnd );
//		CRect rcClient, rcWnd;
//		pWnd->GetWindowRect( &rcWnd );
//		CRect rcReal = rcWnd;
//		pWndMdiClient->ScreenToClient( &rcReal );
//		pWnd->GetClientRect( &rcClient );
//		pWnd->ScreenToClient( &rcWnd );
//		rcAdjust.left -= rcClient.left - rcWnd.left;
//		rcAdjust.top -= rcClient.top - rcWnd.top;
//		rcAdjust.right += rcWnd.right - rcClient.right;
//		rcAdjust.bottom += rcWnd.bottom - rcClient.bottom;
//		if( rcReal != rcAdjust )
//			pWnd->MoveWindow( &rcAdjust );
//	} // if( hWnd != NULL && bMax )
}

bool CExtNcFrameWatchMDI::OnHookWndMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	UINT nMessage,
	WPARAM & wParam,
	LPARAM & lParam
	)
{
	if( m_pNcFrameImpl != NULL )
	{
		switch( nMessage )
		{
		case WM_MDIMAXIMIZE:
		case WM_MDIRESTORE:
		case WM_MDITILE:
		case WM_MDICASCADE:
		case WM_MDIACTIVATE:
		case WM_MDIDESTROY:
			PostSync();
		break;
		} // switch( nMessage )
//		if( nMessage == WM_MDIACTIVATE )
//		{
//			HWND hWndNewActive = (HWND)wParam;
//			BOOL bMax = FALSE;
//			HWND hWndActiveMdiChild = (HWND)
//				::SendMessage(
//					hWndHooked,
//					WM_MDIGETACTIVE,
//					0,
//					(LPARAM)&bMax
//					);
//			if(		hWndActiveMdiChild == NULL
//				||	hWndNewActive == NULL
//				||	(! ::IsWindow(hWndNewActive) )
//				||	( ::GetParent(hWndNewActive) != hWndHooked )
//				)
//				bMax = FALSE;
//			if( bMax )
//				::SendMessage( hWndNewActive, WM_SETREDRAW, FALSE, 0 );
//			bool bRetVal =
//				CExtHookSink::OnHookWndMsg(
//					lResult,
//					hWndHooked,
//					nMessage,
//					wParam,
//					lParam
//					);
//			if( bMax )
//			{
//				::SendMessage( hWndNewActive, WM_SETREDRAW, TRUE, 0 );
//				::RedrawWindow(
//					hWndNewActive,
//					NULL,
//					NULL,
//					RDW_INVALIDATE|RDW_UPDATENOW
//						|RDW_ERASE|RDW_ERASENOW
//						|RDW_ALLCHILDREN
//					);
//			} // if( bMax )
//			return bRetVal;
//		} // if( nMessage == WM_MDIACTIVATE )
	} // if( m_pNcFrameImpl != NULL )
	return
		CExtHookSink::OnHookWndMsg(
			lResult,
			hWndHooked,
			nMessage,
			wParam,
			lParam
			);
}

void CExtNcFrameWatchMDI::PmBridge_OnPaintManagerChanged(
	CExtPaintManager * pGlobalPM
	)
{
	CExtPmBridge::PmBridge_OnPaintManagerChanged(
		pGlobalPM
		);
	if( m_pNcFrameImpl != NULL )
		PostSync();
}

#endif // (!defined __EXT_MFC_NO_NC_FRAME )


