// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_CTRL)

#if (! defined __EXT_TAB_PAGE_CONTAINER_WND_H)
	#include <ExtTabPageContainerWnd.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include <../Src/ExtMemoryDC.h>
#endif

#if (!defined __EXT_POPUP_MENU_WND_H)
	#include <ExtPopupMenuWnd.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerWnd

IMPLEMENT_DYNCREATE(CExtTabPageContainerWnd, CWnd)
IMPLEMENT_CExtPmBridge_MEMBERS( CExtTabPageContainerWnd );

CExtTabPageContainerWnd::CExtTabPageContainerWnd()
	: m_bDirectCreateCall( false )
	, m_pWndTab( NULL )
{
	VERIFY( RegisterTabCtrlWndClass() );

	PmBridge_Install();
}

CExtTabPageContainerWnd::~CExtTabPageContainerWnd()
{
	PmBridge_Uninstall();
}

BEGIN_MESSAGE_MAP(CExtTabPageContainerWnd, CWnd)
	//{{AFX_MSG_MAP(CExtTabPageContainerWnd)
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CExtTabWnd * CExtTabPageContainerWnd::GetSafeTabWindow()
{
	ASSERT_VALID( this );
	if( m_pWndTab == NULL )
		return NULL;
	ASSERT_VALID( m_pWndTab );
	if( m_pWndTab->GetSafeHwnd() == NULL )
		return NULL;
	return m_pWndTab;
}

const CExtTabWnd * CExtTabPageContainerWnd::GetSafeTabWindow() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTabPageContainerWnd * > ( this ) )
			-> GetSafeTabWindow();
}

/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerWnd message handlers

HCURSOR CExtTabPageContainerWnd::g_hCursor = ::LoadCursor( NULL, IDC_ARROW );
bool CExtTabPageContainerWnd::g_bTabCtrlWndClassRegistered = false;

bool CExtTabPageContainerWnd::RegisterTabCtrlWndClass()
{
	if( g_bTabCtrlWndClassRegistered )
		return true;
	
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo(
			hInst,
			__EXT_TAB_PAGE_CONTAINER_CLASS_NAME,
			&_wndClassInfo
			)
		)
	{
		_wndClassInfo.style = CS_GLOBALCLASS|CS_DBLCLKS;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor =
			( g_hCursor != NULL )
			? g_hCursor
			: ::LoadCursor(
			NULL, //hInst,
			IDC_ARROW
			)
			;
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_TAB_PAGE_CONTAINER_CLASS_NAME;
		if( !::AfxRegisterClass( &_wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
	
	g_bTabCtrlWndClassRegistered = true;
	return true;
}

BOOL CExtTabPageContainerWnd::Create(
						CWnd * pParentWnd,
						const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
						UINT nDlgCtrlID, // = UINT( IDC_STATIC )
						DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS
						CCreateContext * pContext // = NULL
						)
{
	if( !RegisterTabCtrlWndClass() )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	m_bDirectCreateCall = true;
	if( ! CWnd::Create(
		__EXT_TAB_PAGE_CONTAINER_CLASS_NAME,
		NULL,
		dwWindowStyle,
		rcWnd,
		pParentWnd,
		nDlgCtrlID,
		pContext
		)
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}

	if( m_pWndTab == NULL )
		m_pWndTab = OnTabWndGetTabImpl();
	ASSERT_VALID(m_pWndTab);
	if( m_pWndTab != NULL && m_pWndTab->m_hWnd == NULL )
	{
		VERIFY(
			m_pWndTab->Create(
				this,
				CRect( 0, 0, 0, 0 ),
				0x100,
				WS_CHILD|WS_VISIBLE|WS_TABSTOP,
				__ETWS_DEFAULT,
				NULL
				)
			);	
	} // if( m_pWndTab != NULL && m_pWndTab->m_hWnd == NULL )
	
	_RepositionBarsImpl();
	return TRUE;
}

void CExtTabPageContainerWnd::DoPaint( CDC * pDC )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pDC );
CRect rcClient;
	GetClientRect( &rcClient );
CExtPaintManager::stat_ExcludeChildAreas(
		pDC->GetSafeHdc(),
		GetSafeHwnd()
		);
	if(		(! PmBridge_GetPM()->GetCb2DbTransparentMode(this) )
		||	(! PmBridge_GetPM()->PaintDockerBkgnd( true, *pDC, this ) )
		)
		pDC->FillSolidRect(
			&rcClient,
			PmBridge_GetPM()->GetColor(
				CExtPaintManager::CLR_3DFACE_OUT, this
				)
			);
}

LRESULT CExtTabPageContainerWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if(		message == WM_PRINT
		||	message == WM_PRINTCLIENT
		)
	{
		CDC * pDC = CDC::FromHandle( (HDC) wParam );
		
		CRect rcWnd, rcClient;
		GetWindowRect( &rcWnd );
		GetClientRect( &rcClient );
		ClientToScreen( rcClient );
		rcClient.OffsetRect( -rcWnd.TopLeft() );
		rcWnd.OffsetRect( -rcWnd.TopLeft() );

		if( (lParam&PRF_NONCLIENT) != 0 )
		{
			pDC->ExcludeClipRect(rcClient);
			PmBridge_GetPM()->PaintResizableBarChildNcAreaRect(
				*pDC,
				rcWnd,
				this
				);
			pDC->SelectClipRgn( NULL );
		}
	
		if( (lParam&(PRF_CLIENT|PRF_ERASEBKGND)) != 0 )
		{
			CPoint ptVpOffset( 0, 0 );
			if( (lParam&PRF_NONCLIENT) != 0 )
			{
				ptVpOffset.x = rcWnd.left - rcClient.left;
				ptVpOffset.y = rcWnd.top - rcClient.top;
			}
			if(		ptVpOffset.x != 0
				||	ptVpOffset.y != 0
				)
				pDC->OffsetViewportOrg(
					-ptVpOffset.x,
					-ptVpOffset.y
					);
			DoPaint( pDC );
			if(		ptVpOffset.x != 0
				||	ptVpOffset.y != 0
				)
				pDC->OffsetViewportOrg(
					ptVpOffset.x,
					ptVpOffset.y
					);
		} // if( (lParam&(PRF_CLIENT|PRF_ERASEBKGND)) != 0 )
		
		if( (lParam&PRF_CHILDREN) != 0 )
			CExtPaintManager::stat_PrintChildren(
				m_hWnd,
				message,
				pDC->GetSafeHdc(),
				lParam,
				false
				);
		return (!0);
	}

	switch( message )
	{
	case WM_CLOSE:
		if( (GetStyle() & WS_CHILD) != 0 )
			return 0L;
	break;

	case WM_PAINT:
		{
			CPaintDC dcPaint( this );
			DoPaint( &dcPaint );
		}
		return TRUE;
		
	case WM_ERASEBKGND:
		return FALSE;
		
	case WM_SIZE:
	case WM_WINDOWPOSCHANGED:
		_RepositionBarsImpl();
		break;
	case WM_CREATE:
			if( m_pWndTab == NULL )
				m_pWndTab = OnTabWndGetTabImpl();
			ASSERT_VALID(m_pWndTab);
			if( m_pWndTab != NULL && m_pWndTab->m_hWnd == NULL )
			{
				VERIFY(
					m_pWndTab->Create(
						this,
						CRect( 0, 0, 0, 0 ),
						0x100,
						WS_CHILD|WS_VISIBLE|WS_TABSTOP,
						__ETWS_ORIENT_BOTTOM,
						NULL
						)
					);	
			} // if( m_pWndTab != NULL && m_pWndTab->m_hWnd == NULL )
		break;
	case WM_DESTROY:
		m_pWndTab = NULL;
		break;
	} // switch( message )
	
	return CWnd::WindowProc(message, wParam, lParam);
}

BOOL CExtTabPageContainerWnd::PreTranslateMessage(MSG* pMsg) 
{
	if(		pMsg->message == WM_KEYDOWN
		&&	(	pMsg->wParam == VK_PRIOR
				||	pMsg->wParam == VK_NEXT
		
		)
		&&	CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL )
		&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_SHIFT ) )
		&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_MENU ) )
		)
	{
		PagesNavigate((pMsg->wParam == VK_NEXT));
		return TRUE;
	}
	return CWnd::PreTranslateMessage( pMsg );
}

BOOL CExtTabPageContainerWnd::PageInsert(
	HWND hWnd,
	__EXT_MFC_SAFE_LPCTSTR sItemText/* = NULL*/,
	HICON hIcon/* = NULL*/,
	bool bCopyIcon/* = true*/,
	int nPos/* = -1*/, // append
	bool bSelect/* = false*/
	)
{
CExtCmdIcon pageIcon;
	pageIcon.AssignFromHICON( hIcon, bCopyIcon );
	return
		PageInsert(
			hWnd,
			pageIcon,
			sItemText,
			nPos,
			bSelect
			);
}

BOOL CExtTabPageContainerWnd::PageInsert(
	HWND hWnd,
	const CExtCmdIcon & pageIcon,
	__EXT_MFC_SAFE_LPCTSTR sItemText/* = NULL*/,
	int nPos/* = -1*/, // append
	bool bSelect/* = false*/
	)
{
	ASSERT_VALID( this );
	if(		hWnd   == NULL || (! ::IsWindow(hWnd)   )
		||	m_hWnd == NULL || (! ::IsWindow(m_hWnd) )
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	int nCount = m_pWndTab->ItemGetCount();
	if( nCount == 0 )
		bSelect = true;
	if( nPos < 0 || nPos > nCount )
		nPos = nCount; // append
	if( ::GetParent(hWnd) != m_hWnd )
		::SetParent( hWnd, m_hWnd );
	if( bSelect )
	{
		if( nCount > 0 )
		{
			int i = (int)m_pWndTab->SelectionGet();
			if( i >= 0 )
				::ShowWindow(
					(HWND)m_pWndTab->ItemLParamGet(i),
					SW_HIDE
					);
		} // if( nCount > 0 )
		if( ( ::GetWindowLong( hWnd, GWL_STYLE ) & WS_VISIBLE ) == 0 )
			::ShowWindow( hWnd, SW_SHOW );
	} // if( bSelect )
	else
	{
		if( ( ::GetWindowLong( hWnd, GWL_STYLE ) & WS_VISIBLE ) != 0 )
			::ShowWindow( hWnd, SW_HIDE );
	} // else from if( bSelect )
	
	m_pWndTab->ItemInsert(
		sItemText,
		pageIcon,
		0,
		(LONG)nPos,
		(LPARAM)hWnd,
		false
		);
	
	nCount++;
	for( int i = nPos; i < nCount; i++ )
		::SetWindowLong(
			(HWND)m_pWndTab->ItemLParamGet(i),
			GWL_ID,
			0x101 + i
			);
	if( bSelect )
		m_pWndTab->SelectionSet( nPos, true, true );
	else
		m_pWndTab->UpdateTabWnd( true );

	_RepositionBarsImpl();
				
	return true;
}

BOOL CExtTabPageContainerWnd::PageInsert(
	CWnd * pWnd,
	__EXT_MFC_SAFE_LPCTSTR sItemText,// = NULL,
	HICON hIcon,// = NULL,
	bool bCopyIcon,// = true,
	int nPos,//= -1, // append
	bool bSelect/* = false */
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pWnd );
	return
		PageInsert(
			pWnd->GetSafeHwnd(),
			sItemText,
			hIcon,
			bCopyIcon,
			nPos,
			bSelect
			);
}

BOOL CExtTabPageContainerWnd::PageInsert(
	CWnd * pWnd,
	const CExtCmdIcon & pageIcon,
	__EXT_MFC_SAFE_LPCTSTR sItemText,// = NULL,
	int nPos,//= -1, // append
	bool bSelect/* = false */
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pWnd );
	return
		PageInsert(
			pWnd->GetSafeHwnd(),
			pageIcon,
			sItemText,
			nPos,
			bSelect
			);
}

LONG CExtTabPageContainerWnd::PageRemove( 
	LONG nIndex,
	LONG nCountToRemove, // = 1
	bool bDestroyPageWnd // = true
	)
{
	// returns count of removed items
	ASSERT_VALID( this );
	if( nIndex > m_pWndTab->ItemGetCount()-1)
	{
		return 0;
	}

HWND hWnd = (HWND)m_pWndTab->ItemLParamGet(nIndex);
	::ShowWindow( hWnd, SW_HIDE );
	if(	bDestroyPageWnd )
		::DestroyWindow(hWnd);
LONG lRet = m_pWndTab->ItemRemove(nIndex,nCountToRemove,true);

	int nCount = m_pWndTab->ItemGetCount();
	int i = 0;
	for( ; i < nCount; i++ )
		::SetWindowLong(
		(HWND)m_pWndTab->ItemLParamGet(i),
		GWL_ID,
		0x101 + i
		);

	// select previous page
	if( nCount > 0 )
	{
		if(nIndex > 0)
			nIndex--;
		PageSelectionSet(nIndex);
	}

	RemoveAllWndHooks();
	_RepositionBarsImpl();
	return lRet;
}

LONG CExtTabPageContainerWnd::PageRemoveAll( 
	bool bDestroyPageWnd // = true 
	)
{
	// returns count of removed items
	ASSERT_VALID( this );
	int nRemovedCount = 0;
	while ( PageRemove(0,1,bDestroyPageWnd) > 0 ) 
		nRemovedCount++;
	return nRemovedCount;
}

HWND CExtTabPageContainerWnd::PageHwndSet(
	LONG nIndex,
	HWND hWndNew
	)
{
	ASSERT_VALID( this );
	ASSERT( hWndNew != NULL && ::IsWindow(hWndNew) );
	HWND hWndOld = PageHwndGetSafe(nIndex);
	ASSERT( hWndOld != NULL && ::IsWindow(hWndOld) );
	if( hWndNew == hWndOld )
		return hWndNew;
	if( ::GetParent(hWndNew) != m_hWnd )
		::SetParent( hWndNew, m_hWnd );
	m_pWndTab->ItemLParamSet( nIndex, LPARAM(hWndNew) );
	int nSelIdx = PageSelectionGet();
	if( nSelIdx == nIndex )
	{
		if( ( ::GetWindowLong( hWndNew, GWL_STYLE ) & WS_VISIBLE ) == 0 )
			::ShowWindow( hWndNew, SW_SHOW );
	} // if( nSelIdx == nIndex )
	else
	{
		if( ( ::GetWindowLong( hWndNew, GWL_STYLE ) & WS_VISIBLE ) != 0 )
			::ShowWindow( hWndNew, SW_HIDE );
	} // else from if( nSelIdx == nIndex )
	::SetWindowLong(
		hWndNew,
		GWL_ID,
		::GetWindowLong( hWndOld, GWL_ID )
		);
	_RepositionBarsImpl();
	m_pWndTab->UpdateTabWnd( true );
	return hWndOld;
}

HWND CExtTabPageContainerWnd::PageHwndGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	LPARAM lParam = m_pWndTab->ItemLParamGet( nIndex );
	HWND hWnd = (HWND)lParam;
	return hWnd;
}

HWND CExtTabPageContainerWnd::PageHwndGetSafe( LONG nIndex ) const
{
	ASSERT_VALID( this );
	HWND hWnd = PageHwndGet(nIndex);
	if( hWnd != NULL )
	{
		if( ! ::IsWindow(hWnd) )
			hWnd = NULL;
	}
	return hWnd;
}

const CWnd * CExtTabPageContainerWnd::PagePermanentWndGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	HWND hWnd = PageHwndGetSafe( nIndex );
	if( hWnd == NULL )
		return NULL;
	CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
	return pWnd;
}

CWnd * CExtTabPageContainerWnd::PagePermanentWndGet( LONG nIndex )
{
	ASSERT_VALID( this );
	HWND hWnd = PageHwndGetSafe( nIndex );
	if( hWnd == NULL )
		return NULL;
	CWnd * pWnd = CWnd::FromHandlePermanent( hWnd );
	return pWnd;
}

bool CExtTabPageContainerWnd::PageMove(
	LONG nIndex,
	LONG nNewIndex,
	bool bUpdateTabWnd // = false
	)
{
	 // move page into the new position
	ASSERT_VALID( this );
	bool bRet = m_pWndTab->ItemMove( nIndex, nNewIndex, bUpdateTabWnd );
	CWnd::RepositionBars( 0, 0xFFFF, 0x101 + nIndex );		
	return bRet;
}

int CExtTabPageContainerWnd::PageGetCount() const
{
	ASSERT_VALID( this );
	return m_pWndTab->ItemGetCount();
}

int CExtTabPageContainerWnd::PageSelectionGet() const
{
	ASSERT_VALID( this );
	return m_pWndTab->SelectionGet();
}

void CExtTabPageContainerWnd::PageSelectionSet( int nPos )
{
	ASSERT_VALID( this );
	m_pWndTab->SelectionSet( nPos, true, true );
}

CExtCmdIcon & CExtTabPageContainerWnd::PageIconGet( LONG nIndex )
{
	ASSERT_VALID( this );
	return m_pWndTab->ItemIconGet( nIndex );
}

const CExtCmdIcon & CExtTabPageContainerWnd::PageIconGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	return m_pWndTab->ItemIconGet( nIndex );
}

void CExtTabPageContainerWnd::PageIconSet(
	LONG nIndex,
	const CExtCmdIcon & _icon,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	m_pWndTab->ItemIconSet(
		nIndex,
		_icon,
		bUpdateTabWnd
		);
}

void CExtTabPageContainerWnd::PageIconSet(
	LONG nIndex,
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	m_pWndTab->ItemIconSet(
		nIndex,
		hIcon,
		bCopyIcon,
		bUpdateTabWnd
		);
}

void CExtTabPageContainerWnd::PageCenterTextSet( 
	LONG nIndex,
	bool bSet, // = true 
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	m_pWndTab->ItemStyleModify( 
		nIndex,
		bSet ? 0 : __ETWI_CENTERED_TEXT,
		bSet ? __ETWI_CENTERED_TEXT : 0,
		bUpdateTabWnd
		);
}

bool CExtTabPageContainerWnd::PageCenterTextGet(
	LONG nIndex
	) const
{
	ASSERT_VALID( this );
	bool bCenteredText = false;
	if( (m_pWndTab->ItemStyleGet( nIndex ) & __ETWI_CENTERED_TEXT) != 0 )
		bCenteredText = true;
	return bCenteredText;
}

__EXT_MFC_SAFE_LPCTSTR CExtTabPageContainerWnd::PageTextGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	return m_pWndTab->ItemTextGet( nIndex );
}

void CExtTabPageContainerWnd::PageTextSet(
	LONG nIndex,
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	m_pWndTab->ItemTextSet( nIndex, sText, bUpdateTabWnd );
}

__EXT_MFC_SAFE_LPCTSTR CExtTabPageContainerWnd::PageTooltipTextGet( LONG nIndex ) const
{
	ASSERT_VALID( this );
	return m_pWndTab->ItemTooltipTextGet( nIndex );
}

void CExtTabPageContainerWnd::PageTooltipTextSet(
	LONG nIndex,
	__EXT_MFC_SAFE_LPCTSTR sTooltipText, // = NULL
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	m_pWndTab->ItemTooltipTextSet( nIndex, sTooltipText, bUpdateTabWnd );
}

void CExtTabPageContainerWnd::PreSubclassWindow() 
{
	if( m_bDirectCreateCall )
		return;
	CWnd::PreSubclassWindow();
	if( m_pWndTab == NULL )
		m_pWndTab = OnTabWndGetTabImpl();
	ASSERT_VALID(m_pWndTab);
	if( m_pWndTab != NULL && m_pWndTab->m_hWnd == NULL )
	{
		VERIFY(
			m_pWndTab->Create(
				this,
				CRect( 0, 0, 0, 0 ),
				0x100,
				WS_CHILD|WS_VISIBLE|WS_TABSTOP,
				__ETWS_ORIENT_BOTTOM,
				NULL
				)
			);	
	} // if( m_pWndTab != NULL && m_pWndTab->m_hWnd == NULL )
}

void CExtTabPageContainerWnd::_ResetAllPageIdentifiersImpl()
{
	int i, nCount = m_pWndTab->ItemGetCount();
	for( i = 0; i < nCount; i++ )
		::SetWindowLong(
			(HWND)m_pWndTab->ItemLParamGet(i),
			GWL_ID,
			0x101+i
			);
}
void CExtTabPageContainerWnd::_RepositionBarsImpl()
{
int nPos = 0, nAdjustPos = -1;
	if( m_pWndTab->GetSafeHwnd() != NULL )
	{
		nPos = (int)m_pWndTab->SelectionGet();
		if( nPos < 0 )
			nPos = 0;
		else
			nAdjustPos = nPos;
	} // if( m_pWndTab->GetSafeHwnd() != NULL )
	CWnd::RepositionBars( 0, 0xFFFF, 0x101 + nPos );
	if( nAdjustPos >= 0 )
	{
		HWND hWnd = (HWND) m_pWndTab->ItemLParamGet( nAdjustPos );
		if(		hWnd != NULL
			&&	::IsWindow( hWnd )
			&&	( ::GetWindowLong( hWnd, GWL_STYLE ) & WS_VISIBLE ) == 0
			)
			::ShowWindow( hWnd, SW_SHOW );
	} // if( nAdjustPos >= 0 )
}
void CExtTabPageContainerWnd::_RealignAllImpl()
{
	if( m_pWndTab->GetSafeHwnd() == NULL )
		return;
	int nCount = m_pWndTab->ItemGetCount();
	if( nCount == 0 )
		return;
	int nSel = (int)m_pWndTab->SelectionGet();
	if( nSel < 0 )
		return;
	_RepositionBarsImpl();
	CRect rcAlign;
	CWnd * pWnd = GetDlgItem( 0x101 + nSel );
	if( pWnd )
	{
		pWnd->GetWindowRect( &rcAlign );
		ScreenToClient( &rcAlign );
	}
	int nPos;
	for( nPos = 0; nPos < nCount; nPos++ )
	{
		if( nPos == nSel )
			continue;
		CWnd * pWnd = GetDlgItem( 0x101 + nPos );
		if( pWnd )
		{
			CRect rcItem;
			pWnd->GetWindowRect( &rcItem );
			ScreenToClient( &rcItem );
			if( rcItem == rcAlign )
				continue;
			pWnd->MoveWindow( &rcAlign );
		}
	}
}

bool CExtTabPageContainerWnd::OnHookWndMsg(
	LRESULT & lResult,
	HWND hWndHooked,
	UINT nMessage,
	WPARAM & wParam,
	LPARAM & lParam
	)
{
	__PROF_UIS_MANAGE_STATE;

	ASSERT_VALID( this );
	
	if(		nMessage == WM_KEYDOWN
		&&	(	wParam == VK_PRIOR || wParam == VK_NEXT	)
		&&	CExtPopupMenuWnd::IsKeyPressed( VK_CONTROL )
		&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_SHIFT ) )
		&&	(! CExtPopupMenuWnd::IsKeyPressed( VK_MENU ) )
		)
	{
		PagesNavigate((wParam == VK_NEXT));
		return true;
	}
	
	return
		CExtHookSink::OnHookWndMsg(
		lResult,
		hWndHooked,
		nMessage,
		wParam,
		lParam
		);
}

BOOL CExtTabPageContainerWnd::PagesNavigate(BOOL bNext)
{
	int nCount =
		(m_pWndTab->GetSafeHwnd() != NULL)
			? m_pWndTab->ItemGetCount()
			: 0;
	if( nCount > 1 )
	{
		HWND hWndFocus = ::GetFocus();
		if( hWndFocus != NULL && ::IsChild(m_hWnd,hWndFocus) )
		{
			LONG nCurSel = m_pWndTab->SelectionGet();
			CExtTabWnd::TAB_ITEM_INFO * pTII = NULL;
			if( bNext )
			{
				LONG nCurSelPrev = nCurSel;
				do 
				{
					nCurSel ++;
					if( nCurSel >= nCount )
						nCurSel = 0;
					pTII = m_pWndTab->ItemGet( nCurSel );
					ASSERT( pTII != NULL );
					ASSERT_VALID( pTII );
				} 
				while( (!pTII->VisibleGet() || !pTII->EnabledGet()) && nCurSelPrev != nCurSel );
			}
			else
			{
				LONG nCurSelPrev = nCurSel;
				do 
				{
					nCurSel --;
					if( nCurSel < 0 )
				nCurSel = nCount - 1;
					pTII = m_pWndTab->ItemGet( nCurSel );
					ASSERT( pTII != NULL );
					ASSERT_VALID( pTII );
				} 
				while( (!pTII->VisibleGet() || !pTII->EnabledGet()) && nCurSelPrev != nCurSel );
			}
			m_pWndTab->SelectionSet( nCurSel, true, true );
			return TRUE;
		} // if( hWndFocus != NULL && ::IsChild(m_hWnd,hWndFocus) )
	}
	return FALSE;
}

ULONG CExtTabPageContainerWnd::SetupHookWndSinkToChilds(
	 HWND hWnd,
	 UINT * pDlgCtrlIDs, // = NULL
	 ULONG nCountOfDlgCtrlIDs, // = 0
	 bool bDeep // = false
	 )
{
	ASSERT( hWnd != NULL );
	if( hWnd == NULL )
		return 0;
	
	ASSERT( ::IsWindow(hWnd) );
	if( !::IsWindow(hWnd) )
		return 0;
	ULONG nCountOfHooks = 0;
	hWnd = ::GetWindow( hWnd, GW_CHILD );
	for( ; hWnd != NULL; hWnd = ::GetWindow( hWnd, GW_HWNDNEXT ) )
	{
		// skip 
		CWnd * pWndTestDowncast = CWnd::FromHandle( hWnd );
		ASSERT_VALID( pWndTestDowncast );
		if(	pWndTestDowncast->IsKindOf(RUNTIME_CLASS( CExtTabPageContainerWnd ))){
			continue;
		}

		ASSERT(
			(nCountOfDlgCtrlIDs == 0 && pDlgCtrlIDs == NULL)
			|| (nCountOfDlgCtrlIDs > 0 && pDlgCtrlIDs != NULL)
			);
		bool bSetupHook = true;
		if( nCountOfDlgCtrlIDs > 0 && pDlgCtrlIDs != NULL )
		{
			bSetupHook = false;
			UINT nDlgCtrlID = ::GetDlgCtrlID( hWnd );
			for( ULONG i=0; i<nCountOfDlgCtrlIDs; i++ )
			{
				if( pDlgCtrlIDs[i] == nDlgCtrlID )
				{
					bSetupHook = true;
					break;
				}
			} // for( ULONG i=0; i<nCountOfDlgCtrlIDs; i++ )
		} // if( nCountOfDlgCtrlIDs > 0 && pDlgCtrlIDs != NULL )

		if( bSetupHook )
		{
			if(	SetupHookWndSink( hWnd ) )
				nCountOfHooks++;
			else
			{
				ASSERT( FALSE );
			}
		} // if( bSetupHook )

		
		if( bDeep )
			nCountOfHooks +=
			SetupHookWndSinkToChilds(
			hWnd,
			pDlgCtrlIDs,
			nCountOfDlgCtrlIDs,
			bDeep
			);
	} // for( ; hWnd != NULL; hWnd = ::GetWindow( hWnd, GW_HWNDNEXT ) )
	return nCountOfHooks;
}

bool CExtTabPageContainerWnd::OnTabWndClickedButton(
	LONG nHitTest,
	bool bButtonPressed,
	INT nMouseButton, // MK_... values
	UINT nMouseEventFlags
	)
{
	ASSERT_VALID( this );
	bButtonPressed;
	nMouseButton;
	nMouseEventFlags;
	nHitTest;
#if (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE)
	if(		(GetStyle()&(WS_CHILD|WS_VISIBLE)) == (WS_CHILD|WS_VISIBLE)
		&&	GetDlgCtrlID() == AFX_IDW_PANE_FIRST
		)
	{
		CFrameWnd * pFrame =
			DYNAMIC_DOWNCAST(
				CFrameWnd,
				GetParent()
				);
		if(		pFrame != NULL
			&&	(! pFrame->IsKindOf(RUNTIME_CLASS(CMDIFrameWnd)) )
			&&	(! pFrame->IsKindOf(RUNTIME_CLASS(CMiniFrameWnd)) )
			)
		{ // if SDI frame
			CExtDynamicBarSite * pDBS = 
				CExtDynamicBarSite::FindBarSite( this );
			if(		pDBS != NULL
				&&	pDBS->GetTabPageContainer() == this
				)
				return
					pDBS->OnTabPageContainerClickedButton(
						nHitTest,
						bButtonPressed,
						nMouseButton,
						nMouseEventFlags
						);
		} // if SDI frame
	} // if( (GetStyle()&(WS_CHILD|WS_VISIBLE)) == (WS_CHILD|WS_VISIBLE) ...
#endif // (!defined __EXT_MFC_NO_DYNAMIC_BAR_SITE)
	return true;
}

void CExtTabPageContainerWnd::OnTabWndClickedItemCloseButton(
	LONG nItemIndex
	)
{
	ASSERT_VALID( this );
	nItemIndex;
}

void CExtTabPageContainerWnd::OnTabWndItemPosChanged(
	LONG nItemIndex,
	LONG nItemNewIndex
	)
{
	ASSERT_VALID( this );
	nItemIndex;
	nItemNewIndex;
	_ResetAllPageIdentifiersImpl();
}

bool CExtTabPageContainerWnd::OnTabWndSelectionChange(
	LONG nOldItemIndex,
	LONG nNewItemIndex,
	bool bPreSelectionTest
	)
{
	ASSERT_VALID( this );
	nOldItemIndex;
	if( ! bPreSelectionTest )
	{
		//_RepositionBarsImpl();
		_RealignAllImpl();
		RemoveAllWndHooks();
		if( nNewItemIndex >= 0 )
		{
			HWND hWndNew = (HWND)m_pWndTab->ItemLParamGet(nNewItemIndex);
			SetupHookWndSinkToChilds(
				hWndNew,
				NULL,
				0,
				true
				);
		} // if( nNewItemIndex >= 0 )
	} // if( ! bPreSelectionTest )
	return true;
}

void CExtTabPageContainerWnd::OnSetFocus(CWnd* pOldWnd) 
{
	CWnd::OnSetFocus(pOldWnd);
	if( GetSafeTabWindow() == NULL )
		return; // destruction mode entered
int nIndex = PageSelectionGet();
	if( nIndex != -1 )
	{
		HWND hWnd = (HWND)m_pWndTab->ItemLParamGet( PageSelectionGet() );
		::SetFocus( hWnd );
	}
}

DWORD CExtTabPageContainerWnd::ModifyTabStyle( DWORD dwTabWndStyle, bool bSet)
{
	ASSERT_VALID( this );
	DWORD dwTabWndStyleRemove = dwTabWndStyle;
	DWORD dwTabWndStyleAdd = dwTabWndStyle;
	if( bSet )	
		dwTabWndStyleRemove = 0;
	else		
		dwTabWndStyleAdd = 0;
	DWORD dwRet = 
		m_pWndTab->ModifyTabWndStyle( dwTabWndStyleRemove, dwTabWndStyleAdd, true);
	_RepositionBarsImpl();
	return dwRet;
}

LONG CExtTabPageContainerWnd::PageFindByHWND(
	HWND hWnd,
	LONG nIndexStartSearch, // = -1
	bool bIncludeVisible, // = true
	bool bIncludeInvisible // = false
	) const
{
	ASSERT_VALID( this );
	return
		m_pWndTab->ItemFindByLParam(
			(LPARAM)hWnd,
			nIndexStartSearch,
			bIncludeVisible,
			bIncludeInvisible
			);
}

bool CExtTabPageContainerWnd::PageEnsureVisible(
	INT nItemIndex,
	bool bUpdateTabWnd // = false
	)
{
	ASSERT_VALID( this );
	return
		m_pWndTab->ItemEnsureVisible(
			nItemIndex,
			bUpdateTabWnd
			);
}

bool CExtTabPageContainerWnd::PageEnabledGet(
	INT nItemIndex
	) const
{
	ASSERT_VALID( this );
	CExtTabWnd::TAB_ITEM_INFO * pTII = 
		m_pWndTab->ItemGet( nItemIndex );
	ASSERT( pTII != NULL );
	ASSERT_VALID( pTII );
	return
		pTII->EnabledGet();
}

bool CExtTabPageContainerWnd::PageEnabledSet( 
	INT nItemIndex,
	bool bEnable // = true 
	)
{
	ASSERT_VALID( this );
CExtTabWnd::TAB_ITEM_INFO * pTII = 
		m_pWndTab->ItemGet( nItemIndex );
	ASSERT( pTII != NULL );
	ASSERT_VALID( pTII );
bool bRet = 
		pTII->EnabledSet( bEnable );
HWND hWnd = PageHwndGetSafe( nItemIndex );
	if( hWnd != NULL )
	{
		if( bEnable && (!::IsWindowEnabled(hWnd)) )
			::EnableWindow( hWnd, TRUE );
		if( (!bEnable) && ::IsWindowEnabled(hWnd) )
			::EnableWindow( hWnd, FALSE );
	}
	m_pWndTab->Invalidate();
	m_pWndTab->UpdateWindow();
	return bRet;
}

bool CExtTabPageContainerWnd::PageVisibleGet( 
	INT nItemIndex 
	) const
{
	ASSERT_VALID( this );
	CExtTabWnd::TAB_ITEM_INFO * pTII = 
		m_pWndTab->ItemGet( nItemIndex );
	ASSERT( pTII != NULL );
	ASSERT_VALID( pTII );
	return
		pTII->VisibleGet();
}

bool CExtTabPageContainerWnd::PageVisibleSet( 
	INT nItemIndex, 
	bool bVisible // = true 
	)
{
	ASSERT_VALID( this );
	CExtTabWnd::TAB_ITEM_INFO * pTII = 
		m_pWndTab->ItemGet( nItemIndex );
	ASSERT( pTII != NULL );
	ASSERT_VALID( pTII );
	bool bRet = 
		pTII->VisibleSet( bVisible );
	if( !bVisible ) 
	{
		HWND hWnd = PageHwndGetSafe( nItemIndex );
		ASSERT( hWnd != NULL && ::IsWindow( hWnd ) );
		if( hWnd != NULL )
			::ShowWindow( hWnd, SW_HIDE );
	}
	_RepositionBarsImpl();
	return bRet;	
}

DWORD CExtTabPageContainerWnd::CenterTextSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_CENTERED_TEXT, bSet);
}

bool CExtTabPageContainerWnd::CenterTextGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_CENTERED_TEXT) ? true : false;
}

DWORD CExtTabPageContainerWnd::AutoHideScrollSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_AUTOHIDE_SCROLL, bSet);
}

bool CExtTabPageContainerWnd::AutoHideScrollGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_AUTOHIDE_SCROLL) ? true : false;
}

DWORD CExtTabPageContainerWnd::ShowBtnScrollHomeSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_SHOW_BTN_SCROLL_HOME, bSet);
}

bool CExtTabPageContainerWnd::ShowBtnScrollHomeGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_SHOW_BTN_SCROLL_HOME) ? true : false;
}

DWORD CExtTabPageContainerWnd::ShowBtnScrollEndSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_SHOW_BTN_SCROLL_END, bSet);
}

bool CExtTabPageContainerWnd::ShowBtnScrollEndGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_SHOW_BTN_SCROLL_END) ? true : false;
}

DWORD CExtTabPageContainerWnd::ShowBtnCloseSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_SHOW_BTN_CLOSE, bSet);
}

bool CExtTabPageContainerWnd::ShowBtnCloseGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_SHOW_BTN_CLOSE) ? true : false;
}

DWORD CExtTabPageContainerWnd::EnabledBtnCloseSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_ENABLED_BTN_CLOSE, bSet);
}

bool CExtTabPageContainerWnd::EnabledBtnCloseGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_ENABLED_BTN_CLOSE) ? true : false;
}

DWORD CExtTabPageContainerWnd::ShowBtnHelpSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_SHOW_BTN_HELP, bSet);
}

bool CExtTabPageContainerWnd::ShowBtnHelpGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_SHOW_BTN_HELP) ? true : false;
}

DWORD CExtTabPageContainerWnd::ShowBtnTabListSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_SHOW_BTN_TAB_LIST, bSet);
}

bool CExtTabPageContainerWnd::ShowBtnTabListGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_SHOW_BTN_TAB_LIST) ? true : false;
}

DWORD CExtTabPageContainerWnd::EnabledBtnTabListSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_ENABLED_BTN_TAB_LIST, bSet);
}

bool CExtTabPageContainerWnd::EnabledBtnTabListGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_ENABLED_BTN_TAB_LIST) ? true : false;
}

DWORD CExtTabPageContainerWnd::EnabledBtnHelpSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_ENABLED_BTN_HELP, bSet);
}

bool CExtTabPageContainerWnd::EnabledBtnHelpGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_ENABLED_BTN_HELP) ? true : false;
}

DWORD CExtTabPageContainerWnd::EqualWidthsSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_EQUAL_WIDTHS, bSet);
}

bool CExtTabPageContainerWnd::EqualWidthsGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_EQUAL_WIDTHS) ? true : false;
}

DWORD CExtTabPageContainerWnd::FullWidthSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_FULL_WIDTH, bSet);
}

bool CExtTabPageContainerWnd::FullWidthGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_FULL_WIDTH) ? true : false;
}

DWORD CExtTabPageContainerWnd::HoverFocusSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_HOVER_FOCUS, bSet);
}

bool CExtTabPageContainerWnd::HoverFocusGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_HOVER_FOCUS) ? true : false;
}

DWORD CExtTabPageContainerWnd::ItemDraggingSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_ITEM_DRAGGING, bSet);
}

bool CExtTabPageContainerWnd::ItemDraggingGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_ITEM_DRAGGING) ? true : false;
}

DWORD CExtTabPageContainerWnd::InvertVertFontSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	return ModifyTabStyle(__ETWS_INVERT_VERT_FONT, bSet);
}

bool CExtTabPageContainerWnd::InvertVertFontGet() const
{
	ASSERT_VALID( this );
	return (m_pWndTab->GetTabWndStyle() & __ETWS_INVERT_VERT_FONT) ? true : false;
}

DWORD CExtTabPageContainerWnd::OrientationGet() const
{
	ASSERT_VALID( this );
	return m_pWndTab->OrientationGet();
}

DWORD CExtTabPageContainerWnd::OrientationSet( DWORD dwOrientation )
{
	ASSERT_VALID( this );
	DWORD dwRet = m_pWndTab->OrientationSet(dwOrientation,true);
	_RepositionBarsImpl();
	if( m_pWndTab->ItemGetCount() > 0 )
	{
		LONG i = m_pWndTab->SelectionGet();
		if( i >= 0 )
			m_pWndTab->ItemEnsureVisible( i, true );
	} // if( m_pWndTab->ItemGetCount() > 0 )
	return dwRet;		
}

bool CExtTabPageContainerWnd::OrientationIsHorizontal() const
{
	ASSERT_VALID( this );
	return m_pWndTab->OrientationIsHorizontal();
}

bool CExtTabPageContainerWnd::OrientationIsVertical() const
{
	ASSERT_VALID( this );
	return m_pWndTab->OrientationIsVertical();
}

bool CExtTabPageContainerWnd::OrientationIsTopLeft() const
{
	ASSERT_VALID( this );
	return m_pWndTab->OrientationIsTopLeft();
}

bool CExtTabPageContainerWnd::SelectionBoldGet() const
{
	ASSERT_VALID( this );
	return m_pWndTab->SelectionBoldGet();
}

void CExtTabPageContainerWnd::SelectionBoldSet( 
	bool bBold // = true 
	)
{
	ASSERT_VALID( this );
	m_pWndTab->SelectionBoldSet(bBold);
	_RepositionBarsImpl();
}

#if (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_FLAT_CTRL)

/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerFlatWnd
/////////////////////////////////////////////////////////////////////////////

CExtTabPageContainerFlatWnd::CExtTabPageContainerFlatWnd()
	: m_bRenderConsistentPageBackground( false )
{
}

CExtTabPageContainerFlatWnd::~CExtTabPageContainerFlatWnd()
{
}

IMPLEMENT_DYNCREATE(CExtTabPageContainerFlatWnd, CExtTabPageContainerWnd)

BEGIN_MESSAGE_MAP(CExtTabPageContainerFlatWnd, CExtTabPageContainerWnd)
	//{{AFX_MSG_MAP(CExtTabPageContainerFlatWnd)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerFlatWnd message handlers

BOOL CExtTabPageContainerFlatWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd,
	UINT nDlgCtrlID,
	DWORD dwWindowStyle,
	CCreateContext * pContext
	)
{
	BOOL bRet = CExtTabPageContainerWnd::Create(
		pParentWnd,
		rcWnd,
		nDlgCtrlID,
		dwWindowStyle,
		pContext);

	CenterTextSet();
	AutoHideScrollSet();

	return bRet;
}

bool CExtTabPageContainerFlatWnd::ItemsHasInclineGet( 
	bool bBefore 
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return ((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineGet(bBefore);
}

bool CExtTabPageContainerFlatWnd::ItemsHasInclineBeforeGet() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return ((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineBeforeGet();
}

bool CExtTabPageContainerFlatWnd::ItemsHasInclineAfterGet() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	return ((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineAfterGet();
}

void CExtTabPageContainerFlatWnd::ItemsHasInclineSet( 
	bool bBefore, 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineSet( bBefore, bSet );
}

void CExtTabPageContainerFlatWnd::ItemsHasInclineBeforeSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineBeforeSet( bSet);
}

void CExtTabPageContainerFlatWnd::ItemsHasInclineAfterSet( 
	bool bSet // = true 
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pWndTab );
	((CExtTabFlatWnd*)m_pWndTab)->ItemsHasInclineAfterSet( bSet );
}

LRESULT CExtTabPageContainerFlatWnd::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	if(		message == CExtPaintManager::g_nMsgPaintInheritedBackground
		&&	m_bRenderConsistentPageBackground
		)
	{
		if( PageGetCount() == 0 )
			return 0;
		LONG nSelIndex = PageSelectionGet();
		if( nSelIndex < 0 )
			return 0;
		CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA * pPIBD =
			CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA::FromWPARAM( wParam );
		ASSERT( pPIBD != NULL );
		if(		pPIBD->m_pWnd == this
			||	pPIBD->m_pWnd == GetSafeTabWindow()
			)
			return 0;
		pPIBD->m_bBackgroundDrawn = true;
		CExtTabFlatWnd * pWndTab =
			STATIC_DOWNCAST( CExtTabFlatWnd, GetSafeTabWindow() );
		ASSERT_VALID( pWndTab );
		bool bEnabled = 
			pWndTab->ItemEnabledGet( nSelIndex );
		COLORREF clrLight = COLORREF(-1L);
		COLORREF clrShadow = COLORREF(-1L);
		COLORREF clrDkShadow = COLORREF(-1L);
		COLORREF clrTabBk = COLORREF(-1L);
		COLORREF clrText = COLORREF(-1L);
		pWndTab->OnFlatTabWndGetItemColors(
			nSelIndex,
			true,
			false,
			bEnabled,
			clrLight,
			clrShadow,
			clrDkShadow, 
			clrTabBk, 
			clrText
			);
		CRect rc = pPIBD->GetRenderingRect();
		pPIBD->m_dc.FillSolidRect( &rc, clrTabBk );
		return 0;
	}
	return CExtTabPageContainerWnd::WindowProc( message, wParam, lParam );
}

#endif // (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_FLAT_CTRL)

#if (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_BUTTONS_CTRL)

/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerButtonsWnd
/////////////////////////////////////////////////////////////////////////////

CExtTabPageContainerButtonsWnd::CExtTabPageContainerButtonsWnd()
{
}

CExtTabPageContainerButtonsWnd::~CExtTabPageContainerButtonsWnd()
{
}

IMPLEMENT_DYNCREATE(CExtTabPageContainerButtonsWnd, CExtTabPageContainerFlatWnd)

BEGIN_MESSAGE_MAP(CExtTabPageContainerButtonsWnd, CExtTabPageContainerFlatWnd)
	//{{AFX_MSG_MAP(CExtTabPageContainerButtonsWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#endif // (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_BUTTONS_CTRL)

/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerOneNoteWnd
/////////////////////////////////////////////////////////////////////////////

#if (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_ONENOTE_CTRL)

CExtTabPageContainerOneNoteWnd::CExtTabPageContainerOneNoteWnd()
	: m_bRenderConsistentPageBackground( false )
	, m_bRenderGradientInheritance( false )
{
}

CExtTabPageContainerOneNoteWnd::~CExtTabPageContainerOneNoteWnd()
{
}

BOOL CExtTabPageContainerOneNoteWnd::PageInsert(
	HWND hWnd,
	__EXT_MFC_SAFE_LPCTSTR sItemText, // = NULL`
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true
	int nPos, // = -1 // append
	bool bSelect, // = false
	COLORREF clrBkLight, // = (COLORREF)(-1L)
	COLORREF clrBkDark // = (COLORREF)(-1L)
	)
{
	ASSERT_VALID( this );
CExtCmdIcon pageIcon;
	pageIcon.AssignFromHICON( hIcon, bCopyIcon );
	return
		PageInsert(
			hWnd,
			pageIcon,
			sItemText,
			nPos,
			bSelect,
			clrBkLight,
			clrBkDark
			);
}

BOOL CExtTabPageContainerOneNoteWnd::PageInsert(
	HWND hWnd,
	const CExtCmdIcon & pageIcon,
	__EXT_MFC_SAFE_LPCTSTR sItemText, // = NULL`
	int nPos, // = -1 // append
	bool bSelect, // = false
	COLORREF clrBkLight, // = (COLORREF)(-1L)
	COLORREF clrBkDark // = (COLORREF)(-1L)
	)
{
	ASSERT_VALID( this );
	if(		hWnd   == NULL || (! ::IsWindow(hWnd)   )
		||	m_hWnd == NULL || (! ::IsWindow(m_hWnd) )
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	CExtTabOneNoteWnd * pWndTab = 
		DYNAMIC_DOWNCAST( CExtTabOneNoteWnd, m_pWndTab );
	ASSERT( pWndTab != NULL );
	int nCount = pWndTab->ItemGetCount();
	if( nCount == 0 )
		bSelect = true;
	if( nPos < 0 || nPos > nCount )
		nPos = nCount; // append
	if( ::GetParent(hWnd) != m_hWnd )
		::SetParent( hWnd, m_hWnd );
	if( bSelect )
	{
		if( nCount > 0 )
		{
			int i = (int)pWndTab->SelectionGet();
			if( i >= 0 )
				::ShowWindow(
					(HWND)pWndTab->ItemLParamGet(i),
					SW_HIDE
					);
		} // if( nCount > 0 )
		if( ( ::GetWindowLong( hWnd, GWL_STYLE ) & WS_VISIBLE ) == 0 )
			::ShowWindow( hWnd, SW_SHOW );
	} // if( bSelect )
	else
	{
		if( ( ::GetWindowLong( hWnd, GWL_STYLE ) & WS_VISIBLE ) != 0 )
			::ShowWindow( hWnd, SW_HIDE );
	} // else from if( bSelect )
	
	pWndTab->ItemInsert(
		sItemText,
		pageIcon,
		0,
		(LONG)nPos,
		(LPARAM)hWnd,
		clrBkLight,
		clrBkDark,
		false
		);
	
	nCount++;
	for( int i = nPos; i < nCount; i++ )
		::SetWindowLong(
			(HWND)m_pWndTab->ItemLParamGet(i),
			GWL_ID,
			0x101 + i
			);
	if( bSelect )
		m_pWndTab->SelectionSet( nPos, true, true );
	else
		m_pWndTab->UpdateTabWnd( true );

	_RepositionBarsImpl();
				
	return true;
}

BOOL CExtTabPageContainerOneNoteWnd::PageInsert(
	CWnd * pWnd,
	__EXT_MFC_SAFE_LPCTSTR sItemText, // = NULL
	HICON hIcon, // = NULL,
	bool bCopyIcon, // = true
	int nPos, //= -1, // append
	bool bSelect, // = false
	COLORREF clrBkLight, // = (COLORREF)(-1L)
	COLORREF clrBkDark // = (COLORREF)(-1L)
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pWnd );
	return
		PageInsert(
			pWnd->GetSafeHwnd(),
			sItemText,
			hIcon,
			bCopyIcon,
			nPos,
			bSelect,
			clrBkLight,
			clrBkDark
			);
}

BOOL CExtTabPageContainerOneNoteWnd::PageInsert(
	CWnd * pWnd,
		const CExtCmdIcon & pageIcon,
	__EXT_MFC_SAFE_LPCTSTR sItemText, // = NULL
	int nPos, //= -1, // append
	bool bSelect, // = false
	COLORREF clrBkLight, // = (COLORREF)(-1L)
	COLORREF clrBkDark // = (COLORREF)(-1L)
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pWnd );
	return
		PageInsert(
			pWnd->GetSafeHwnd(),
			pageIcon,
			sItemText,
			nPos,
			bSelect,
			clrBkLight,
			clrBkDark
			);
}

IMPLEMENT_DYNCREATE(CExtTabPageContainerOneNoteWnd, CExtTabPageContainerWnd)

LRESULT CExtTabPageContainerOneNoteWnd::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	if(		message == CExtPaintManager::g_nMsgPaintInheritedBackground
		&&	m_bRenderConsistentPageBackground
		)
	{
		if( PageGetCount() == 0 )
			return 0;
		LONG nSelIndex = PageSelectionGet();
		if( nSelIndex < 0 )
			return 0;
		CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA * pPIBD =
			CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA::FromWPARAM( wParam );
		ASSERT( pPIBD != NULL );
		if(		pPIBD->m_pWnd == this
			||	pPIBD->m_pWnd == GetSafeTabWindow()
			)
			return 0;
		pPIBD->m_bBackgroundDrawn = true;
		CExtTabOneNoteWnd * pWndTab =
			STATIC_DOWNCAST( CExtTabOneNoteWnd, GetSafeTabWindow() );
		ASSERT_VALID( pWndTab );
		bool bEnabled = 
			pWndTab->ItemEnabledGet( nSelIndex );
		if( m_bRenderGradientInheritance )
		{
			COLORREF clrLight, clrDark;
			pWndTab->OnTabWndQueryItemColors(
				nSelIndex,
				true,
				false,
				bEnabled,
				NULL,
				NULL,
				&clrLight,
				&clrDark
				);
			DWORD dwOrientaton = OrientationGet();
			CRect rc;
			GetClientRect( &rc );

			if(		dwOrientaton == __ETWS_ORIENT_TOP
				||	dwOrientaton == __ETWS_ORIENT_LEFT
				) // TO FIX: condition
				RepositionBars( 0, 0xFFFF, 0x101 + nSelIndex, CWnd::reposQuery, &rc, &rc );
			
			ClientToScreen( &rc );
			if( pPIBD->m_bClientMapping )
				pPIBD->m_pWnd->ScreenToClient( &rc );
			else
			{
				CRect rcTarget;
				pPIBD->m_pWnd->GetWindowRect( &rcTarget );
				rc.OffsetRect( - rcTarget.TopLeft() );
			}
			COLORREF clrTmp;
			bool bHorz = false;
			switch( dwOrientaton)
			{
			case __ETWS_ORIENT_TOP:
				bHorz = true;
				clrTmp = clrDark;
				clrDark = clrLight;
				clrLight = clrTmp;
			break;
			case __ETWS_ORIENT_BOTTOM:
				bHorz = true;
			break;
			case __ETWS_ORIENT_LEFT:
			break;
			case __ETWS_ORIENT_RIGHT:
				clrTmp = clrDark;
				clrDark = clrLight;
				clrLight = clrTmp;
			break;
#ifdef _DEBUG
			default:
				ASSERT( FALSE );
			break;
#endif // _DEBUG
			} // switch( dwOrientaton)
			CExtPaintManager::stat_PaintGradientRect(
				pPIBD->m_dc,
				&rc,
				clrDark,
				clrLight,
				bHorz
				);
		} // if( m_bRenderGradientInheritance )
		else
		{
			CRect rc = pPIBD->GetRenderingRect();
			COLORREF clr;
			pWndTab->OnTabWndQueryItemColors(
				nSelIndex,
				true,
				false,
				bEnabled,
				NULL,
				NULL,
				NULL,
				&clr
				);
			pPIBD->m_dc.FillSolidRect( &rc, clr );
		} // else from if( m_bRenderGradientInheritance )
		return 0;
	}
	return CExtTabPageContainerWnd::WindowProc( message, wParam, lParam );
}


#endif // (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_ONENOTE_CTRL)


/////////////////////////////////////////////////////////////////////////////
// CExtTabPageContainerWhidbeyWnd
/////////////////////////////////////////////////////////////////////////////

#if (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_WHIDBEY_CTRL)

CExtTabPageContainerWhidbeyWnd::CExtTabPageContainerWhidbeyWnd()
	: m_bRenderConsistentPageBackground( false )
{
}

CExtTabPageContainerWhidbeyWnd::~CExtTabPageContainerWhidbeyWnd()
{
}

IMPLEMENT_DYNCREATE(CExtTabPageContainerWhidbeyWnd, CExtTabPageContainerWnd)

LRESULT CExtTabPageContainerWhidbeyWnd::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	if(		message == CExtPaintManager::g_nMsgPaintInheritedBackground
		&&	m_bRenderConsistentPageBackground
		)
	{
		if( PageGetCount() == 0 )
			return 0;
		LONG nSelIndex = PageSelectionGet();
		if( nSelIndex < 0 )
			return 0;
		CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA * pPIBD =
			CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA::FromWPARAM( wParam );
		ASSERT( pPIBD != NULL );
		if(		pPIBD->m_pWnd == this
			||	pPIBD->m_pWnd == GetSafeTabWindow()
			)
			return 0;
		pPIBD->m_bBackgroundDrawn = true;
		CExtTabWhidbeyWnd * pWndTab =
			STATIC_DOWNCAST( CExtTabWhidbeyWnd, GetSafeTabWindow() );
		ASSERT_VALID( pWndTab );
		bool bEnabled = 
			pWndTab->ItemEnabledGet( nSelIndex );
		COLORREF clr = COLORREF( -1L );
		pWndTab->OnTabWndQueryItemColors(
			nSelIndex,
			true,
			false,
			bEnabled,
			NULL,
			NULL,
			&clr,
			NULL
			);
		CRect rc = pPIBD->GetRenderingRect();
		pPIBD->m_dc.FillSolidRect( &rc, clr );
		return 0;
	}
	return CExtTabPageContainerWnd::WindowProc( message, wParam, lParam );
}

#endif // (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_WHIDBEY_CTRL)


#endif // (!defined __EXT_MFC_NO_TAB_PAGECONTAINER_CTRL)
