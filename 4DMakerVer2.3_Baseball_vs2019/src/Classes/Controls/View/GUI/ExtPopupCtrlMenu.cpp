// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_POPUP_CTRL_MENU_H)
	#include <ExtPopupCtrlMenu.h>
#endif

#if (!defined __EXT_MENUCONTROLBAR_H)
	#include <ExtMenuControlBar.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include <../Src/ExtMemoryDC.h>
#endif

#if (!defined __EXT_LOCALIZATION_H)
	#include <ExtLocalization.h>
#endif

#if (!defined __EXT_MFC_NO_RIBBON_BAR)
	#if (!defined __EXT_RIBBON_BAR_H)
		#include <ExtRibbonBar.h>
	#endif // (!defined __EXT_RIBBON_BAR_H)
#endif

#include <math.h>

#include <Resources/Resource.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtPopupControlMenuWnd

IMPLEMENT_DYNCREATE(CExtPopupControlMenuWnd, CExtPopupMenuWnd)

BEGIN_MESSAGE_MAP(CExtPopupControlMenuWnd, CExtPopupMenuWnd)
	//{{AFX_MSG_MAP(CExtPopupControlMenuWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

UINT CExtPopupControlMenuWnd::g_nMsgControlInputRetranslate =
	::RegisterWindowMessage(
		_T("CExtPopupControlMenuWnd::g_nMsgControlInputRetranslate")
		);
bool CExtPopupControlMenuWnd::g_bControlMenuWithShadows = true;

CExtPopupControlMenuWnd::CExtPopupControlMenuWnd()
	: m_sizeChildControl( 150, 100 )
	, m_sizeChildControlMin( -1, -1 )
	, m_sizeChildControlMax( -1, -1 )
	, m_rcChildControl( 0, 0, 0, 0 )
	, m_hWndChildControl( NULL )
	, m_bStdEscProcessing( true )
{
}

CExtPopupControlMenuWnd::~CExtPopupControlMenuWnd()
{
}

void CExtPopupControlMenuWnd::_RecalcMinMaxResizingSizes()
{
	ASSERT_VALID( this );
int nMenuShadowSize = OnQueryMenuShadowSize();
CRect rcMB = CExtPopupMenuWnd::OnQueryMenuBorderMetrics();
CSize _size = _CalcTrackSize();
CSize _sizeDiff(
		_size.cx - m_sizeChildControl.cx - nMenuShadowSize - rcMB.left - rcMB.right,
		_size.cy - m_sizeChildControl.cy - nMenuShadowSize - rcMB.top - rcMB.bottom
		);
	if( m_sizeChildControlMin.cx >= 0 && m_sizeChildControlMin.cy >= 0 )
	{
		CSize _sizeMinOld = ResizingMinSizeGet();
		CSize _sizeMinNew = _sizeMinOld;
		CSize _sizeCalcMin(
				_sizeDiff.cx + m_sizeChildControlMin.cx,
				_sizeDiff.cy + m_sizeChildControlMin.cy
				);
		if( m_sizeChildControlMin.cx >= 0 )
			_sizeMinNew.cx = max( _sizeMinNew.cx, _sizeCalcMin.cx ) - nMenuShadowSize - rcMB.left - rcMB.right;
		if( m_sizeChildControlMin.cy >= 0 )
			_sizeMinNew.cy = max( _sizeMinNew.cy, _sizeCalcMin.cy ) - nMenuShadowSize - rcMB.top - rcMB.bottom;
		if( _sizeMinNew != _sizeMinOld )
			ResizingMinSizeSet( _sizeMinNew );
	} // if( m_sizeChildControlMin.cx >= 0 && m_sizeChildControlMin.cy >= 0 )
	if( m_sizeChildControlMax.cx >= 0 && m_sizeChildControlMax.cy >= 0 )
	{
		CSize _sizeMaxOld = ResizingMaxSizeGet();
		CSize _sizeMaxNew = _sizeMaxOld;
		CSize _sizeCalcMax(
				_sizeDiff.cx + m_sizeChildControlMax.cx,
				_sizeDiff.cy + m_sizeChildControlMax.cy
				);
		if( m_sizeChildControlMax.cx >= 0 )
			_sizeMaxNew.cx = min( _sizeMaxNew.cx, _sizeCalcMax.cx ) - nMenuShadowSize - rcMB.left - rcMB.right;
		if( m_sizeChildControlMax.cy >= 0 )
			_sizeMaxNew.cy = min( _sizeMaxNew.cy, _sizeCalcMax.cy ) - nMenuShadowSize - rcMB.top - rcMB.bottom;
		if( _sizeMaxNew != _sizeMaxOld )
			ResizingMaxSizeSet( _sizeMaxNew );
	} // if( m_sizeChildControlMax.cx >= 0 && m_sizeChildControlMax.cy >= 0 )
}

bool CExtPopupControlMenuWnd::IsAllItemsRarelyUsed() const
{
	ASSERT_VALID( this );
	return false;
}

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
// CExtCustomizeSite::ICustomizeDropTarget
DROPEFFECT CExtPopupControlMenuWnd::OnCustomizeTargetOver(
	CExtCustomizeSite::CCmdDragInfo & _dragInfo,
	CPoint point,
	DWORD dwKeyState
	)
{
	ASSERT_VALID( this );
	ASSERT( !_dragInfo.IsEmpty() );
	_dragInfo;
	point;
	dwKeyState;
	return DROPEFFECT_NONE;
}
void CExtPopupControlMenuWnd::OnCustomizeTargetLeave()
{
	ASSERT_VALID( this );
}
bool CExtPopupControlMenuWnd::OnCustomizeTargetDrop(
	CExtCustomizeSite::CCmdDragInfo & _dragInfo,
	CPoint point,
	DROPEFFECT de
	)
{
	ASSERT_VALID( this );
	ASSERT( !_dragInfo.IsEmpty() );;
	_dragInfo;
	point;
	de;
	return false;
}
// CExtCustomizeSite::ICustomizeDropSource
void CExtPopupControlMenuWnd::OnCustomizeSourceDragComplete(
	DROPEFFECT de,
	bool bCanceled,
	bool * p_bNoResetActiveItem
	)
{
	ASSERT_VALID( this );
	ASSERT( p_bNoResetActiveItem != NULL );
	de;
	bCanceled;
	p_bNoResetActiveItem;
}
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

bool CExtPopupControlMenuWnd::_CanStartLevelTracking()
{
	ASSERT_VALID( this );
	if( _FindHelpMode() )
		return false;
	if( _FindCustomizeMode() )
		return false;
	return true;
}

bool CExtPopupControlMenuWnd::_OnMouseWheel(
	WPARAM wParam,
	LPARAM lParam,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );
	wParam;
	lParam;
	bNoEat;
	if( GetSafeHwnd() == NULL )
		return true;

	if( _IsResizingMode() )
	{
		bNoEat = false;
		return true;
	}

CExtPopupMenuSite & _site = GetSite();
	if(		_site.GetAnimated() != NULL
		||	_site.IsShutdownMode()
		||	_site.IsEmpty()
		||	_site.GetAnimated() != NULL
		)
		return true;

TranslateMouseWheelEventData_t _td( this, wParam, lParam, bNoEat );
	if( _td.Notify() )
	{
		bNoEat = _td.m_bNoEat;
		return true;
	}

	if(		m_hWndChildControl != NULL
		&&	::IsWindow( m_hWndChildControl )
		)
	{
		CONTROLINPUTRETRANSLATEINFO _ciri(
			this,
			wParam,
			lParam,
			bNoEat
			);
		bool bRetVal = 
			( ::SendMessage(
				m_hWndChildControl,
				g_nMsgControlInputRetranslate,
				(WPARAM)(&_ciri),
				(LPARAM)0L
				) != 0 )
			? true : false;
		return bRetVal;
	}

CPoint ptScreenClick;
	if( ! ::GetCursorPos( &ptScreenClick ) )
		return true;
HWND hWndFromPoint = ::WindowFromPoint( ptScreenClick );
	if(		hWndFromPoint != NULL
		&&	(::GetWindowLong(hWndFromPoint,GWL_STYLE)&WS_CHILD) != 0
		&&	(	m_hWndChildControl == hWndFromPoint
			||	::IsChild( m_hWndChildControl, hWndFromPoint )
			)
		)
	{
		bNoEat = true;
		return false;
	}
HWND hWndFocus = ::GetFocus();
	if(		hWndFocus != NULL
		&&	(::GetWindowLong(hWndFocus,GWL_STYLE)&WS_CHILD) != 0
		&&	(	m_hWndChildControl == hWndFocus
			||	::IsChild( m_hWndChildControl, hWndFocus )
			)
		)
	{
		bNoEat = true;
		return false;
	}
HWND hWndCapture = ::GetCapture();
	if(		hWndCapture != NULL
		&&	(::GetWindowLong(hWndCapture,GWL_STYLE)&WS_CHILD) != 0
		&&	(	m_hWndChildControl == hWndCapture
			||	::IsChild( m_hWndChildControl, hWndCapture )
			)
		)
	{
		bNoEat = true;
		return false;
	}
	return true;
}

bool CExtPopupControlMenuWnd::_OnMouseMove(
	UINT nFlags,
	CPoint point,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );

	if( GetSafeHwnd() == NULL )
		return false;

	if( GetSite().GetAnimated() != NULL )
		return true;

	if( _IsResizingMode() )
	{
		bNoEat = true;
		return false;
	}

CExtPopupMenuSite & _site = GetSite();
	if(	_site.IsShutdownMode()
		|| _site.IsEmpty()
		|| _site.GetAnimated() != NULL
		)
		return true;

TranslateMouseMoveEventData_t _td( this, nFlags, point, bNoEat );
	if( _td.Notify() )
	{
		bNoEat = _td.m_bNoEat;
		return true;
	}

CPoint ptScreenClick( point );
	ClientToScreen( &ptScreenClick );

bool bRetVal = false;
	if(		m_hWndChildControl != NULL
		&&	::IsWindow( m_hWndChildControl )
		)
	{
		CONTROLINPUTRETRANSLATEINFO _ciri(
			this,
			true,
			nFlags,
			point,
			bNoEat
			);
		bRetVal = 
			( ::SendMessage(
				m_hWndChildControl,
				g_nMsgControlInputRetranslate,
				(WPARAM)(&_ciri),
				(LPARAM)0L
				) != 0 )
			? true : false;
	}

HWND hWndFromPoint = ::WindowFromPoint( ptScreenClick );
	if(		hWndFromPoint != NULL
		&&	(::GetWindowLong(hWndFromPoint,GWL_STYLE)&WS_CHILD) != 0
		&&	(	m_hWndChildControl == hWndFromPoint
			||	::IsChild( m_hWndChildControl, hWndFromPoint )
			)
		)
	{
		bNoEat = true;
		return false;
	}
HWND hWndFocus = ::GetFocus();
	if(		hWndFocus != NULL
		&&	(::GetWindowLong(hWndFocus,GWL_STYLE)&WS_CHILD) != 0
		&&	(	m_hWndChildControl == hWndFocus
			||	::IsChild( m_hWndChildControl, hWndFocus )
			)
		)
	{
		bNoEat = true;
		return false;
	}
HWND hWndCapture = ::GetCapture();
	if(		hWndCapture != NULL
		&&	(::GetWindowLong(hWndCapture,GWL_STYLE)&WS_CHILD) != 0
		&&	(	m_hWndChildControl == hWndCapture
			||	::IsChild( m_hWndChildControl, hWndCapture )
			)
		)
	{
		bNoEat = true;
		return false;
	}

	if( (! bRetVal ) && (! _PtInWndArea( point ) ) )
	{
		if( _CoolTipIsVisible() )
		{
			_CoolTipHide( false );
			Invalidate();
		}
		if( m_pWndParentMenu != NULL
			&& m_pWndParentMenu->GetSafeHwnd() != NULL
			)
		{
			ASSERT_VALID( m_pWndParentMenu );
			ClientToScreen( &point );
			m_pWndParentMenu->ScreenToClient( &point );
			if( m_pWndParentMenu->_OnMouseMove(
					nFlags,
					point,
					bNoEat
					)
				)
			{
				if( bNoEat )
					return false;
				_OnCancelMode();
				return true;
			}
		}
		return false;
	} // if( (! bRetVal ) && (! _PtInWndArea( point ) ) )
	else
	{
		if( _IsTearOff() )
		{
			if( CExtPopupMenuWnd::_HitTest(point) != IDX_TEAROFF )
				Invalidate();
		}
	} // else from if( (! bRetVal ) && (! _PtInWndArea( point ) ) )

	if( _IsTearOff() )
	{
		if( CExtPopupMenuWnd::_HitTest(point) == IDX_TEAROFF )
		{
			HWND hWndOwn = m_hWnd;
			_ItemFocusCancel( TRUE, FALSE );
			if( ! ::IsWindow( hWndOwn ) )
				return true;
			_SetCapture();
			HCURSOR hCursor = ::LoadCursor( NULL, IDC_SIZEALL );
			ASSERT( hCursor != NULL );
			::SetCursor( hCursor );
			if(		g_bMenuShowCoolTips
				&&	GetSite().GetCapture() == this
				)
			{
				CRect rcItem;
				_GetItemRect(IDX_TEAROFF,rcItem);
				ClientToScreen( &rcItem );
				bool bShowTip = true;
				CExtPopupMenuTipWnd & _tipWnd = GetTip();
				if( _tipWnd.GetSafeHwnd() != NULL )
				{
					CRect rcExcludeArea = _tipWnd.GetExcludeArea();
					if( rcExcludeArea == rcItem )
						bShowTip = false;
				}
				if( bShowTip )
				{
					CExtSafeString sTipText;

#if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)
					CExtLocalResourceHelper _LRH;
#endif

					g_ResourceManager->LoadString( sTipText, ID_EXT_TEAR_OFF_MENU_TIP );
					if( sTipText.IsEmpty() )
						sTipText = _T("Drag to make this menu float");

					_tipWnd.SetText( sTipText );
					VERIFY(
						_tipWnd.Show(
							this,
							rcItem
							)
						);
				}
			}
			return true;
		}
		_CoolTipHide( false );
		HCURSOR hCursor = ::LoadCursor( NULL, IDC_ARROW );
		ASSERT( hCursor != NULL );
		::SetCursor( hCursor );
		Invalidate();
	}

	return bRetVal;
}

bool CExtPopupControlMenuWnd::_OnMouseClick(
	UINT nFlags,
	CPoint point,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );
	bNoEat;

	if( GetSafeHwnd() == NULL )
		return false;

	if( GetSite().GetAnimated() != NULL )
		return true;

CExtPopupMenuSite & _site = GetSite();
	if(	_site.IsShutdownMode()
		|| _site.IsEmpty()
		|| _site.GetAnimated() != NULL
		)
		return true;

TranslateMouseClickEventData_t _td( this, nFlags, point, bNoEat );
	if( _td.Notify() )
	{
		bNoEat = _td.m_bNoEat;
		return true;
	}

bool bLButtonUpCall =
		(nFlags==WM_LBUTTONUP || nFlags==WM_NCLBUTTONUP)
			? true : false;
CPoint ptScreenClick( point );
	ClientToScreen( &ptScreenClick );
	if( (TrackFlagsGet()&TPMX_RIBBON_RESIZING) != 0 )
	{
		UINT nHT = (UINT) ::SendMessage( m_hWnd, WM_NCHITTEST, 0L, MAKELPARAM(ptScreenClick.x,ptScreenClick.y) );
		switch( nHT )
		{
		case HTLEFT:
		case HTRIGHT:
		case HTTOP:
		case HTBOTTOM:
		case HTTOPLEFT:
		case HTTOPRIGHT:
		case HTBOTTOMLEFT:
		case HTBOTTOMRIGHT:
			if( ! bLButtonUpCall )
			{
				m_bHelperResizingMode = true;
				_DoResizing(
					ptScreenClick,
					( (TrackFlagsGet()&TPMX_RIBBON_RESIZING_VERTICAL_ONLY) != 0 ) ? true : false
					);
			} // if( ! bLButtonUpCall )
			else
				m_bHelperResizingMode = false;
			bNoEat = true;
			return false;
		} // switch( nHT )
		m_bHelperResizingMode = false;
	} // if( (TrackFlagsGet()&TPMX_RIBBON_RESIZING) != 0 )

	if( _IsTearOff() && CExtPopupMenuWnd::_HitTest(point) == IDX_TEAROFF )
	{
		if( ! bLButtonUpCall )
			_DoTearOff();
		return true;
	}
HWND hWndFromPoint = ::WindowFromPoint( ptScreenClick );
	if(		hWndFromPoint != NULL
		&&	(::GetWindowLong(hWndFromPoint,GWL_STYLE)&WS_CHILD) != 0
		&&	(	m_hWndChildControl == hWndFromPoint
			||	::IsChild( m_hWndChildControl, hWndFromPoint )
			)
		)
	{
		bNoEat = true;
		return false;
	}
HWND hWndFocus = ::GetFocus();
	if(		hWndFocus != NULL
		&&	(::GetWindowLong(hWndFocus,GWL_STYLE)&WS_CHILD) != 0
		&&	(	m_hWndChildControl == hWndFocus
			||	::IsChild( m_hWndChildControl, hWndFocus )
			)
		)
	{
		bNoEat = true;
		return false;
	}
HWND hWndCapture = ::GetCapture();
	if(		hWndCapture != NULL
		&&	(::GetWindowLong(hWndCapture,GWL_STYLE)&WS_CHILD) != 0
		&&	(	m_hWndChildControl == hWndCapture
			||	::IsChild( m_hWndChildControl, hWndCapture )
			)
		)
	{
		bNoEat = true;
		return false;
	}

bool bPtInWndArea = _PtInWndArea( point );
	if( bPtInWndArea )
	{
		if( m_bExcludeAreaSpec )
		{
			CRect rc = m_rcExcludeArea;
			ScreenToClient( &rc );
			if( rc.PtInRect( point ) )
			{
				bPtInWndArea = false;
				bool bLButtonDownCall =
					( nFlags == WM_LBUTTONDOWN || nFlags==WM_NCLBUTTONDOWN )
						? true : false;
				if( bLButtonDownCall )
				{
					_OnCancelMode();
					return false;
				}
			}
		}
	}
	if( ! bPtInWndArea )
	{
		if( m_pWndParentMenu != NULL
			&& m_pWndParentMenu->GetSafeHwnd() != NULL
			)
		{
			ASSERT_VALID( m_pWndParentMenu );
			ClientToScreen( &point );
			m_pWndParentMenu->ScreenToClient( &point );

			HWND hWndOwn = GetSafeHwnd();
			ASSERT( hWndOwn != NULL );
			ASSERT( ::IsWindow(hWndOwn) );
			CExtPopupMenuWnd * pWndParentMenu = m_pWndParentMenu;

			bool bInplaceControlArea = false, bInplaceDropDownArea = false;
			int nHtTemp =
				pWndParentMenu->_HitTest(
					point,
					&bInplaceControlArea,
					&bInplaceDropDownArea
					);
			if( nHtTemp >= 0 )
			{
				MENUITEMDATA & mi = pWndParentMenu->ItemGetInfo( nHtTemp );
				if(		mi.IsPopup()
					&&	mi.GetPopup() == this
					)
				{
					if( bInplaceControlArea )
					{
						pWndParentMenu->_SetCapture();
						pWndParentMenu->_ItemFocusCancel( FALSE );
						pWndParentMenu->_ItemFocusSet( nHtTemp, FALSE, TRUE );
						if( mi.IsAllowInplaceEditActivation() )
						{
							CWnd * pWndInplace = mi.GetInplaceEditPtr();
							if( pWndInplace != NULL )
							{
								ASSERT_VALID( pWndInplace );
								ASSERT( pWndInplace->GetSafeHwnd() != NULL && (::IsWindow(pWndInplace->GetSafeHwnd())) );
								if( (pWndInplace->GetStyle() & WS_VISIBLE) == 0 )
								{
									CRect rcInplaceEdit;
									pWndParentMenu->_GetItemRect( nHtTemp, rcInplaceEdit );
									rcInplaceEdit =
										mi.AdjustInplaceEditRect(
											rcInplaceEdit,
											OnQueryLayoutRTL()
											);
									pWndInplace->SetWindowPos(
										NULL,
										rcInplaceEdit.left, rcInplaceEdit.top,
										rcInplaceEdit.Width(), rcInplaceEdit.Height(),
										SWP_NOZORDER|SWP_NOOWNERZORDER
											|SWP_NOACTIVATE|SWP_SHOWWINDOW
										);
								} // if( (pWndInplace->GetStyle() & WS_VISIBLE) == 0 )
								pWndInplace->SetFocus();
							} // if( pWndInplace != NULL )
						} // if( mi.IsAllowInplaceEditActivation() )
					} // if( bInplaceControlArea )
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
					CExtCustomizeCmdTreeNode * pNode = mi.GetCmdNode();
					if( pNode == NULL )
						return true;
					if( (pNode->GetFlags()&__ECTN_TBB_SEPARATED_DROPDOWN) )
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
						return true;
				}
			} // if( nHtTemp >= 0 )

			if(	pWndParentMenu->_OnMouseClick(
					nFlags,
					point,
					bNoEat
					)
				)
			{
				if( bNoEat )
					return false;
				if( ::IsWindow(hWndOwn) )
				{
					//_OnCancelMode();
					CancelMenuTracking();
				} // CancelMenuTracking();
				return true;
			}
			return false;
		}

		// fixed in v. 2.20
		if(		nFlags == WM_RBUTTONUP
			||	nFlags == WM_LBUTTONUP
			)
		{
			return true;
		}

		_OnCancelMode();
		return false;
	} // if( ! bPtInWndArea )

bool bRetVal = false;
	if(		m_hWndChildControl != NULL
		&&	::IsWindow( m_hWndChildControl )
		)
	{
		CONTROLINPUTRETRANSLATEINFO _ciri(
			this,
			false,
			nFlags,
			point,
			bNoEat
			);
		bRetVal = 
			( ::SendMessage(
				m_hWndChildControl,
				g_nMsgControlInputRetranslate,
				(WPARAM)(&_ciri),
				(LPARAM)0L
				) != 0 )
			? true : false;
	}

	return bRetVal;
}

bool CExtPopupControlMenuWnd::_OnKeyDown(
	UINT nChar,
	UINT nRepCnt,
	UINT nFlags,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );

	if( GetSafeHwnd() == NULL )
		return true; //false;

	if( GetSite().GetAnimated() != NULL )
		return true;

	if( _IsResizingMode() )
	{
		bNoEat = true;
		return false;
	}

TranslateKeyboardEventData_t _td( this, nChar, nRepCnt, nFlags, bNoEat );
	if( _td.Notify() )
	{
		bNoEat = _td.m_bNoEat;
		return true;
	}

	if( m_bStdEscProcessing && nChar == VK_ESCAPE )
	{
		bNoEat = false;
		if( m_bTopLevel )
			_EndSequence();
		else
		{
			int nParentCurIndex =
				m_pWndParentMenu->_GetCurIndex();
			ASSERT(
				nParentCurIndex >= 0
				&&
				nParentCurIndex <=
					m_pWndParentMenu->ItemGetCount()
				);
			HWND hWndOwn = m_hWnd;
			m_pWndParentMenu->_SetCapture();
			m_pWndParentMenu->_ItemFocusCancel(
				FALSE
				);
			m_pWndParentMenu->_ItemFocusSet(
				nParentCurIndex,
				FALSE,
				TRUE
				);
			if( ! ::IsWindow( hWndOwn ) )
				return true;
			m_pWndParentMenu->_SetCapture();
		}
		return true;
	}

bool bRetVal = false;
	if(		m_hWndChildControl != NULL
		&&	::IsWindow( m_hWndChildControl )
		)
	{
		CONTROLINPUTRETRANSLATEINFO _ciri(
			this,
			nChar,
			nRepCnt,
			nFlags,
			bNoEat
			);
		bRetVal =
			( ::SendMessage(
				m_hWndChildControl,
				g_nMsgControlInputRetranslate,
				(WPARAM)(&_ciri),
				(LPARAM)0L
				) != 0 )
			? true : false;
	}

	return bRetVal;
}

CRect CExtPopupControlMenuWnd::_CalcTrackRect()
{
	ASSERT_VALID( this );
	return CExtPopupMenuWnd::_CalcTrackRect();
}

CSize CExtPopupControlMenuWnd::_CalcTrackSize()
{
	ASSERT_VALID( this );
CRect rcMB = OnQueryMenuBorderMetrics();
int nMenuShadowSize = OnQueryMenuShadowSize();
CSize _size(
		m_sizeChildControl.cx + nMenuShadowSize + rcMB.left + rcMB.right,
		m_sizeChildControl.cy + nMenuShadowSize + rcMB.top + rcMB.bottom
		);
	_size.cx += m_nLeftAreaWidth;
	return _size;
}

bool CExtPopupControlMenuWnd::_IsPopupWithShadows() const
{
	if( ! g_bControlMenuWithShadows )
		return false;
	return CExtPopupMenuWnd::_IsPopupWithShadows();
}

bool CExtPopupControlMenuWnd::_IsPopupWithShadowsDynamic() const
{
	if( ! g_bControlMenuWithShadows )
		return false;
	return CExtPopupMenuWnd::_IsPopupWithShadowsDynamic();
}

void CExtPopupControlMenuWnd::_AdjustAnimation(
	CExtPopupBaseWnd::e_animation_type_t & eAT
	)
{
	ASSERT_VALID( this );
	eAT = __AT_CONTENT_DISPLAY;
}

bool CExtPopupControlMenuWnd::_CreateHelper(
	CWnd * pWndCmdReceiver
	)
{
	ASSERT_VALID( this );

	if( ! CExtPopupMenuWnd::_CreateHelper( pWndCmdReceiver ) )
		return false;

	m_rcChildControl = _RecalcControlRect();
	ASSERT( m_hWndChildControl == NULL );
	m_hWndChildControl = OnCreateChildControl( m_rcChildControl );
/*
	if( m_hWndChildControl == NULL || (! ::IsWindow(m_hWndChildControl) ) )
	{
		ASSERT( FALSE );
		return false;
	}
*/
	return true;
}

void CExtPopupControlMenuWnd::_DoAdjustControlMetrics(
	CSize _sizeAdjust
	)
{
	ASSERT_VALID( this );
	m_sizeChildControl += _sizeAdjust;
}

CRect CExtPopupControlMenuWnd::_RecalcControlRect()
{
	ASSERT_VALID( this );
CRect rcClient;
	_GetClientRect( &rcClient );
CRect rcControl;
	rcControl.left = rcControl.top = 0;
	rcControl.right = m_sizeChildControl.cx;
	rcControl.bottom = m_sizeChildControl.cy;
	rcControl.OffsetRect( rcClient.TopLeft() );
	rcControl.OffsetRect( m_nLeftAreaWidth, 0 );
CRect rcMB = OnQueryMenuBorderMetrics();
	rcControl.OffsetRect( rcMB.left, rcMB.top );
	if( _IsTearOff() )
	{
		int nTearOffCaptionHeight =
			_GetTearOffCaptionHeight();
		rcControl.OffsetRect( 0, nTearOffCaptionHeight );
	}
	return rcControl;
}

void CExtPopupControlMenuWnd::_RecalcLayoutImpl()
{
	ASSERT_VALID( this );
	CExtPopupMenuWnd::_RecalcLayoutImpl();
	if( m_hWndChildControl == NULL )
		return;

	ASSERT_VALID( this );

//	ASSERT( ::IsWindow(m_hWndChildControl) );

CRect rcControl = _RecalcControlRect();
	if( rcControl == m_rcChildControl )
		return;
	m_rcChildControl = rcControl;
	::SetWindowPos(
		m_hWndChildControl, NULL,
		m_rcChildControl.left, m_rcChildControl.top,
		m_rcChildControl.Width(), m_rcChildControl.Height(),
		SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOACTIVATE
		);
	if( IsWindowVisible() )
	{
		CClientDC dc(this);
		_DoPaint(dc);
	}
}

HWND CExtPopupControlMenuWnd::OnCreateChildControl(
	const RECT & rcChildControl
	)
{
	ASSERT_VALID( this );
	rcChildControl;
	return NULL;
}

LRESULT CExtPopupControlMenuWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if(		message == WM_CLOSE
		||	message == WM_DESTROY
		)
		m_hWndChildControl = NULL;
	return CExtPopupMenuWnd::WindowProc(message,wParam,lParam);
}

/////////////////////////////////////////////////////////////////////////////
// CExtPopupInplaceListBox

IMPLEMENT_DYNCREATE(CExtPopupInplaceListBox, CListBox)

BEGIN_MESSAGE_MAP(CExtPopupInplaceListBox, CListBox)
	//{{AFX_MSG_MAP(CExtPopupInplaceListBox)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

UINT CExtPopupInplaceListBox::g_nMsgPopupListBoxInitContent =
	::RegisterWindowMessage(
		_T("CExtPopupInplaceListBox::g_nMsgPopupListBoxInitContent")
		);
UINT CExtPopupInplaceListBox::g_nMsgPopupListBoxItemMeasure =
	::RegisterWindowMessage(
		_T("CExtPopupInplaceListBox::g_nMsgPopupListBoxItemMeasure")
		);
UINT CExtPopupInplaceListBox::g_nMsgPopupListBoxItemDraw =
	::RegisterWindowMessage(
		_T("CExtPopupInplaceListBox::g_nMsgPopupListBoxItemDraw")
		);
UINT CExtPopupInplaceListBox::g_nMsgPopupListBoxSelEndOK =
	::RegisterWindowMessage(
		_T("CExtPopupInplaceListBox::g_nMsgPopupListBoxSelEndOK")
		);
UINT CExtPopupInplaceListBox::g_nMsgPopupListBoxSelEndCancel =
	::RegisterWindowMessage(
		_T("CExtPopupInplaceListBox::g_nMsgPopupListBoxSelEndCancel")
		);
UINT CExtPopupInplaceListBox::g_nMsgPopupListBoxSelChange =
	::RegisterWindowMessage(
		_T("CExtPopupInplaceListBox::g_nMsgPopupListBoxSelChange")
		);
UINT CExtPopupInplaceListBox::g_nMsgPopupListBoxItemClick =
	::RegisterWindowMessage(
		_T("CExtPopupInplaceListBox::g_nMsgPopupListBoxItemClick")
		);

CExtPopupInplaceListBox::CExtPopupInplaceListBox()
	: m_pCbInitListBoxContent( NULL )
	, m_pInitListBoxCookie( NULL )
	, m_pCbListBoxSelection( NULL )
	, m_pSelectionCookie( NULL )
	, m_pCbListBoxItemClick( NULL )
	, m_pItemClickCookie( NULL )
	, m_pCbListBoxItemDraw( NULL )
	, m_pCbListBoxItemMeasure( NULL )
	, m_pListBoxItemCookie( NULL )
	, m_lParamCookie( 0L )
	, m_bSelEndNotificationPassed( false )
{
}

CExtPopupInplaceListBox::~CExtPopupInplaceListBox()
{
}

void CExtPopupInplaceListBox::DrawItem(LPDRAWITEMSTRUCT pDIS)
{
	ASSERT_VALID( this );
	ASSERT( pDIS != NULL );
	ASSERT( pDIS->hDC != NULL );
	if(		m_pCbListBoxItemDraw != NULL
		&&	m_pCbListBoxItemDraw(
				*this,
				m_pListBoxItemCookie,
				pDIS
				)
		)
		return;
CExtPopupControlMenuWnd * pPopup =
		STATIC_DOWNCAST( CExtPopupControlMenuWnd, GetParent() );
	ASSERT_VALID( pPopup );
	ASSERT( pPopup->m_hWnd != NULL && ::IsWindow(pPopup->m_hWnd) );
HWND hWndCmdReceiver = pPopup->GetCmdReceiverHWND();
	ASSERT( hWndCmdReceiver != NULL && ::IsWindow(hWndCmdReceiver) );
POPUPLISTBOXITEMDRAWINFO plbdii(
		this,
		pDIS
		);
	::SendMessage(
		hWndCmdReceiver,
		g_nMsgPopupListBoxItemDraw,
		(WPARAM)(&plbdii),
		(LPARAM)m_lParamCookie
		);
}

void CExtPopupInplaceListBox::MeasureItem(LPMEASUREITEMSTRUCT pMIS)
{
	ASSERT_VALID( this );
	ASSERT( pMIS != NULL );
	if(		m_pCbListBoxItemMeasure != NULL
		&&	m_pCbListBoxItemMeasure(
				*this,
				m_pListBoxItemCookie,
				pMIS
				)
		)
		return;
CExtPopupControlMenuWnd * pPopup =
		STATIC_DOWNCAST( CExtPopupControlMenuWnd, GetParent() );
	ASSERT_VALID( pPopup );
	ASSERT( pPopup->m_hWnd != NULL && ::IsWindow(pPopup->m_hWnd) );
HWND hWndCmdReceiver = pPopup->GetCmdReceiverHWND();
	ASSERT( hWndCmdReceiver != NULL && ::IsWindow(hWndCmdReceiver) );
POPUPLISTBOXITEMMEASUREINFO plbmii(
		this,
		pMIS
		);
	::SendMessage(
		hWndCmdReceiver,
		g_nMsgPopupListBoxItemMeasure,
		(WPARAM)(&plbmii),
		(LPARAM)m_lParamCookie
		);
}

void CExtPopupInplaceListBox::PostNcDestroy()
{
	delete this;
}

LRESULT CExtPopupInplaceListBox::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if( 	(GetStyle() & LBS_OWNERDRAWFIXED) != 0 
		||	(GetStyle() & LBS_OWNERDRAWVARIABLE) != 0 
		)
	{
		if( message == WM_ERASEBKGND )
			return 0L;
		if( message == WM_PAINT )
		{
			ASSERT_VALID( this );
			CPaintDC dcPaint( this );
			CRect rcClient;
			GetClientRect( &rcClient );
			CExtMemoryDC dc( &dcPaint, &rcClient );

			dc.FillSolidRect(
				&rcClient,
				::GetSysColor( COLOR_WINDOW ) 
				);

			CFont * pFont = GetFont();
			ASSERT( pFont != NULL );
			CFont * pOldFont = dc.SelectObject( pFont );

			INT nCount = GetCount();
			for( INT nItem = GetTopIndex(); nItem < nCount; nItem++ )
			{
				DRAWITEMSTRUCT dis;
				::memset( &dis, 0, sizeof( DRAWITEMSTRUCT ) );
				dis.CtlType = ODT_LISTBOX;
				dis.CtlID = GetDlgCtrlID();
				dis.itemID = nItem;
				dis.hDC = dc.GetSafeHdc();
				GetItemRect( nItem, &dis.rcItem );
				dis.itemAction = ODA_DRAWENTIRE;
				dis.hwndItem = GetSafeHwnd();

				if( rcClient.bottom < dis.rcItem.top )
					break;

				if( GetSel( nItem ) > 0 )
					dis.itemState |= ODS_SELECTED;
				if( GetCurSel() == nItem )
					dis.itemState |= ODS_FOCUS;

				SendMessage( 
					WM_DRAWITEM, 
					(WPARAM)GetDlgCtrlID(), 
					(LPARAM)&dis 
					);
			}

			dc.SelectObject( pOldFont );
			
			return 0L;
		} // if( message == WM_PAINT )
	} 
	
	if( message == WM_CREATE )
	{
		CExtPopupControlMenuWnd * pPopup =
			STATIC_DOWNCAST( CExtPopupControlMenuWnd, GetParent() );
		m_bSelEndNotificationPassed = false;
		LRESULT lResult = CListBox::WindowProc(message,wParam,lParam);
		SetFont( & pPopup->PmBridge_GetPM()->m_FontNormal );
		bool bCbInitSucceeded = false;
		if( m_pCbInitListBoxContent != NULL )
			bCbInitSucceeded =
				m_pCbInitListBoxContent(
					*this,
					m_pInitListBoxCookie
					);
		if( !bCbInitSucceeded )
		{
			ASSERT_VALID( pPopup );
			ASSERT( pPopup->m_hWnd != NULL && ::IsWindow(pPopup->m_hWnd) );
			HWND hWndCmdReceiver = pPopup->GetCmdReceiverHWND();
			ASSERT( hWndCmdReceiver != NULL && ::IsWindow(hWndCmdReceiver) );
			::SendMessage(
				hWndCmdReceiver,
				g_nMsgPopupListBoxInitContent,
				(WPARAM)this,
				(LPARAM)m_lParamCookie
				);
		} // if( !bCbInitSucceeded )
		return lResult;
	} // if( message == WM_CREATE )
	if( message == WM_DESTROY )
	{
		_DoSelEndCancel( true );
		return CListBox::WindowProc(message,wParam,lParam);
	} // if( message == WM_DESTROY )
	if( message == WM_MOUSEACTIVATE )
		return MA_NOACTIVATE;
	if( message == WM_MOUSEMOVE )
	{
		INT nCount = CListBox::GetCount();
		if( nCount == 0 )
			return 0;
		BOOL bOutside = TRUE;
		CPoint point( (DWORD)lParam );
		INT nHitTest = (INT)CListBox::ItemFromPoint( point, bOutside );
		if( bOutside )
		{
			if( GetCapture() != this )
				return 0;
			INT nTop = CListBox::GetTopIndex();
			if( nHitTest <= nTop )
			{
				if( nTop > 0 )
					nHitTest--;
			} // if( nHitTest <= nTop )
			else
			{
				if( nCount > 0 && nHitTest >= (nCount-2) )
					nHitTest++;
			} // else from if( nHitTest <= nTop )
		} // if( bOutside )
		INT nCurSel = CListBox::GetCurSel();
		if( nCurSel == nHitTest )
			return 0;
		CListBox::SetCurSel( nHitTest );
		_DoSelChange();
		return 0;
	} // if( message == WM_MOUSEMOVE )
	if(		message == WM_RBUTTONUP
		||	message == WM_RBUTTONDOWN
		||	message == WM_RBUTTONDBLCLK
		||	message == WM_MBUTTONUP
		||	message == WM_MBUTTONDOWN
		||	message == WM_MBUTTONDOWN
//		||	message == WM_LBUTTONUP
//		||	message == WM_LBUTTONDOWN
		||	message == WM_LBUTTONDBLCLK
		)
		return 0;
	if( message == WM_LBUTTONDOWN )
	{
		INT nCount = CListBox::GetCount();
		if( nCount == 0 )
			return 0;
		SetCapture();
		return 0;
	}
	if( message == WM_LBUTTONUP )
	{
		if( GetCapture() == this )
			ReleaseCapture();
		INT nCount = CListBox::GetCount();
		if( nCount > 0 )
		{
			BOOL bOutside = TRUE;
			CPoint point( (DWORD)lParam );
			INT nHitTest = (INT)CListBox::ItemFromPoint( point, bOutside );
			nHitTest;
			if( !bOutside )
			{
				if( !_DoItemClick() )
					_DoSelEndOK();
			}
		} // if( nCount > 0 )
		return 0;
	} // if( message == WM_LBUTTONUP )
	if( message == WM_CANCELMODE )
	{
		if( GetCapture() == this )
			ReleaseCapture();
		_DoSelEndCancel();
		return 0;
	} // if( message == WM_CANCELMODE )
	if( message == CExtPopupControlMenuWnd::g_nMsgControlInputRetranslate )
	{
		ASSERT( wParam != 0 );
		CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO & _ciri =
			*((CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO*)wParam);
		if( _ciri.m_eRTT == CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO::__ERTT_MOUSE_WHEEL )
		{
			if( ! g_PaintManager.m_bIsWin2000orLater )
				return 1;
			DWORD dwWndStyle = GetStyle();
			if( (dwWndStyle & WS_VSCROLL) == 0 )
				return 1; // only vertical scrolling is supported
			int nItemCount = CListBox::GetCount();
			if( nItemCount == 0 )
				return 1;
			struct __SAME_AS_MOUSEHOOKSTRUCTEX
			{
				MOUSEHOOKSTRUCT mhs;
				DWORD mouseData;
			};
			__SAME_AS_MOUSEHOOKSTRUCTEX * pMHEX =
				reinterpret_cast
					< __SAME_AS_MOUSEHOOKSTRUCTEX * >
						( _ciri.m_lParam );
			ASSERT( pMHEX != NULL );
			DWORD dwWheelDeltaAndZeroFlags =
				DWORD( pMHEX->mouseData ) & 0xFFFF0000;
			if( dwWheelDeltaAndZeroFlags == 0 )
				return 1;
			int yAmount =
				( int(short(dwWheelDeltaAndZeroFlags>>16)) > 0 )
					? (-1) : 1;
			int nMouseWheelScrollLines =
				(int)g_PaintManager.GetMouseWheelScrollLines();
			if( nMouseWheelScrollLines > 2 )
				nMouseWheelScrollLines--; // offset is 1 less
			yAmount *= nMouseWheelScrollLines;
			int nTopIdxSrc = CListBox::GetTopIndex();
			int nTopIdxDst = nTopIdxSrc + yAmount;
			if( nTopIdxDst < 0 )
				nTopIdxDst = 0;
			else if( nTopIdxDst >= nItemCount )
				nTopIdxDst = nItemCount - 1;
			if( nTopIdxDst == nTopIdxSrc )
				return 0;
			CListBox::SetTopIndex( nTopIdxDst );
			_ciri.m_bNoEat = true;
			return 0;
		} // if( _ciri.m_eRTT == CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO::__ERTT_MOUSE_WHEEL )
		if( _ciri.m_eRTT == CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO::__ERTT_MOUSE_MOVE )
		{
			CPoint ptScreen = _ciri.m_point;
			ClientToScreen( &ptScreen );
			if( ::WindowFromPoint( ptScreen ) != m_hWnd )
				return 0;
			_ciri.m_bNoEat = false;
			CRect rcClient;
			GetClientRect( &rcClient );
			if( rcClient.PtInRect( _ciri.m_point ) )
				SendMessage( WM_MOUSEMOVE, 0L, MAKELPARAM( _ciri.m_point.x, _ciri.m_point.y ) );
			else
				SendMessage( WM_NCMOUSEMOVE, HTVSCROLL, MAKELPARAM( ptScreen.x, ptScreen.y ) );
			return 1;
		}
		if( _ciri.m_eRTT != CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO::__ERTT_KEYBOARD )
			return 0;
		bool bAlt =
			( (::GetAsyncKeyState(VK_MENU)&0x8000) != 0 )
				? true : false;
		if( bAlt )
		{
			_DoSelEndCancel();
			return 1;
		}
		bool bCtrl =
			( (::GetAsyncKeyState(VK_CONTROL)&0x8000) != 0 )
				? true : false;
		bool bShift =
			( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 )
				? true : false;
		if( bAlt || bCtrl || ( bShift && _ciri.m_nChar != VK_TAB ) )
			return 0;
		if( _ciri.m_nChar == VK_ESCAPE )
		{
			_DoSelEndCancel();
			return 1;
		} // if( _ciri.m_nChar == VK_ESCAPE )
		if( _ciri.m_nChar == VK_RETURN )
		{
			_DoSelEndOK();
			return 1;
		} // if( _ciri.m_nChar == VK_ESCAPE )
		if( _ciri.m_nChar == VK_SPACE )
		{
			_DoItemClick();
			return 1;
		} // if( _ciri.m_nChar == VK_ESCAPE )
		if( _ciri.m_nChar == VK_RIGHT )
			return 1;
		if( _ciri.m_nChar == VK_LEFT )
		{
			CExtPopupControlMenuWnd * pPopup =
				STATIC_DOWNCAST( CExtPopupControlMenuWnd, GetParent() );
			ASSERT_VALID( pPopup );
			ASSERT( pPopup->m_hWnd != NULL && ::IsWindow(pPopup->m_hWnd) );
			CExtPopupMenuWnd * pWndParentMenu = pPopup->GetParentMenuWnd();
			if( pWndParentMenu == NULL )
				return 1;
			int nParentCurIndex = pWndParentMenu->_GetCurIndex();
			ASSERT(
					nParentCurIndex >= 0
				&&	nParentCurIndex <= pWndParentMenu->ItemGetCount()
				);
			pWndParentMenu->_ItemFocusCancel( FALSE );
			pWndParentMenu->_ItemFocusSet(
				nParentCurIndex,
				FALSE,
				TRUE
				);
			pWndParentMenu->_SetCapture();
			return 1;
		} // if( _ciri.m_nChar == VK_LEFT )
		INT nCurSelOld = CListBox::GetCurSel();
		UINT nChar = _ciri.m_nChar;
		if( nChar == VK_TAB )
			nChar = bShift ? VK_UP : VK_DOWN; 
		DefWindowProc( WM_KEYDOWN, nChar, _ciri.m_nFlags );
		INT nCurSelNew = CListBox::GetCurSel();
		if( nCurSelOld != nCurSelNew )
			_DoSelChange();
		return 0;
	} // if( message == CExtPopupControlMenuWnd::g_nMsgControlInputRetranslate )
	return CListBox::WindowProc(message,wParam,lParam);
}

void CExtPopupInplaceListBox::_DoSelEndOK()
{
	ASSERT_VALID( this );
	if( m_bSelEndNotificationPassed )
		return;
	m_bSelEndNotificationPassed = true;
HWND hWndThis = m_hWnd;
	ASSERT( hWndThis != NULL && ::IsWindow(hWndThis) );
	if(		m_pCbListBoxSelection != NULL
		&&	m_pCbListBoxSelection(
				*this,
				m_pSelectionCookie,
				__SAT_SELENDOK
				)
		)
		return;
	if( ::IsWindow( hWndThis ) )
	{
		CExtPopupControlMenuWnd * pPopup =
			STATIC_DOWNCAST( CExtPopupControlMenuWnd, GetParent() );
		ASSERT_VALID( pPopup );
		ASSERT( pPopup->m_hWnd != NULL && ::IsWindow(pPopup->m_hWnd) );
		HWND hWndCmdReceiver = pPopup->GetCmdReceiverHWND();
		ASSERT( hWndCmdReceiver != NULL && ::IsWindow(hWndCmdReceiver) );
		POPUPLISTBOXITEMSELENDINFO _plbsei( this );
		::SendMessage(
			hWndCmdReceiver,
			g_nMsgPopupListBoxSelEndOK,
			(WPARAM)(&_plbsei),
			(LPARAM)m_lParamCookie
			);
		if( pPopup->_IsFadeOutAnimation() )
			pPopup->DestroyWindow();
		else
			pPopup->SendMessage( WM_CANCELMODE );
	} // if( ::IsWindow( hWndThis ) )
	CExtToolControlBar::_CloseTrackingMenus();
}

void CExtPopupInplaceListBox::_DoSelEndCancel(
	bool bFinalDestroyMode // = false
	)
{
	ASSERT_VALID( this );
	if( m_bSelEndNotificationPassed )
		return;
	m_bSelEndNotificationPassed = true;
	if( bFinalDestroyMode )
	{
		if( m_pCbListBoxSelection != NULL )
			m_pCbListBoxSelection(
				*this,
				m_pSelectionCookie,
				__SAT_CLOSE
				);
		return;
	}
HWND hWndThis = m_hWnd;
	ASSERT( hWndThis != NULL && ::IsWindow(hWndThis) );
	if(		m_pCbListBoxSelection != NULL
		&&	m_pCbListBoxSelection(
				*this,
				m_pSelectionCookie,
				__SAT_SELENDCANCEL
				)
		)
		return;
	if( ::IsWindow( hWndThis ) )
	{
		CExtPopupControlMenuWnd * pPopup =
			STATIC_DOWNCAST( CExtPopupControlMenuWnd, GetParent() );
		ASSERT_VALID( pPopup );
		ASSERT( pPopup->m_hWnd != NULL && ::IsWindow(pPopup->m_hWnd) );
		HWND hWndCmdReceiver = pPopup->GetCmdReceiverHWND();
		ASSERT( hWndCmdReceiver != NULL && ::IsWindow(hWndCmdReceiver) );
		POPUPLISTBOXITEMSELENDINFO _plbsei( this );
		::SendMessage(
			hWndCmdReceiver,
			g_nMsgPopupListBoxSelEndCancel,
			(WPARAM)(&_plbsei),
			(LPARAM)m_lParamCookie
			);
		pPopup->SendMessage( WM_CANCELMODE );
	} // if( ::IsWindow( hWndThis ) )
	CExtToolControlBar::_CloseTrackingMenus();
}

void CExtPopupInplaceListBox::_DoSelChange()
{
	ASSERT_VALID( this );
HWND hWndThis = m_hWnd;
	ASSERT( hWndThis != NULL && ::IsWindow(hWndThis) );
	if(		m_pCbListBoxSelection != NULL
		&&	m_pCbListBoxSelection(
				*this,
				m_pSelectionCookie,
				__SAT_SELCHANGE
				)
		)
		return;
	if( ! ::IsWindow( hWndThis ) )
		return;
CExtPopupControlMenuWnd * pPopup =
		STATIC_DOWNCAST( CExtPopupControlMenuWnd, GetParent() );
	ASSERT_VALID( pPopup );
	ASSERT( pPopup->m_hWnd != NULL && ::IsWindow(pPopup->m_hWnd) );
CExtPopupMenuWnd * pWndParentMenu = pPopup->GetParentMenuWnd();
	if( pWndParentMenu != NULL )
	{
		int nParentCurIndex = pWndParentMenu->_GetCurIndex();
		ASSERT(
				nParentCurIndex >= 0
			&&	nParentCurIndex <= pWndParentMenu->ItemGetCount()
			);
		CExtPopupMenuWnd::MENUITEMDATA & mi =
			pWndParentMenu->ItemGetInfo( nParentCurIndex );
		bool bResetTempSelText = true;
		if( GetStyle() & LBS_HASSTRINGS )
		{
			INT nCurSel = CListBox::GetCurSel();
			if(		CListBox::GetCount() > 0
				&&	nCurSel >= 0
				)
			{
				CExtSafeString sTempSelText;
				CListBox::GetText( nCurSel, *((CString*)&sTempSelText) );
				if( !sTempSelText.IsEmpty() )
				{
					mi.SetTempSelectedInplaceEditText( sTempSelText );
					bResetTempSelText = false;
				}
			}
		} // if( GetStyle() & LBS_HASSTRINGS )
		if( bResetTempSelText )
			mi.SetTempSelectedInplaceEditText( NULL );
		CClientDC dc( pWndParentMenu );
		pWndParentMenu->_DoPaint( dc );
	} // if( pWndParentMenu != NULL )

HWND hWndCmdReceiver = pPopup->GetCmdReceiverHWND();
	ASSERT( hWndCmdReceiver != NULL && ::IsWindow(hWndCmdReceiver) );
POPUPLISTBOXITEMSELENDINFO _plbsei( this );
	::SendMessage(
		hWndCmdReceiver,
		g_nMsgPopupListBoxSelChange,
		(WPARAM)(&_plbsei),
		(LPARAM)m_lParamCookie
		);
}

bool CExtPopupInplaceListBox::_DoItemClick()
{
	ASSERT_VALID( this );
HWND hWndThis = m_hWnd;
	ASSERT( hWndThis != NULL && ::IsWindow(hWndThis) );
	if( m_pCbListBoxItemClick != NULL )
		return
			m_pCbListBoxItemClick(
				*this,
				m_pItemClickCookie
				);
	if( ! ::IsWindow( hWndThis ) )
		return false;
CExtPopupControlMenuWnd * pPopup =
		STATIC_DOWNCAST( CExtPopupControlMenuWnd, GetParent() );
	ASSERT_VALID( pPopup );
	ASSERT( pPopup->m_hWnd != NULL && ::IsWindow(pPopup->m_hWnd) );

HWND hWndCmdReceiver = pPopup->GetCmdReceiverHWND();
	ASSERT( hWndCmdReceiver != NULL && ::IsWindow(hWndCmdReceiver) );
POPUPLISTBOXITEMCLICKINFO _plbici( this );
LRESULT lRes = 
		::SendMessage(
			hWndCmdReceiver,
			g_nMsgPopupListBoxItemClick,
			(WPARAM)(&_plbici),
			(LPARAM)m_lParamCookie
			);
	return (lRes > 0L) ? true : false;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPopupListBoxMenuWnd

IMPLEMENT_DYNCREATE(CExtPopupListBoxMenuWnd, CExtPopupControlMenuWnd)

BEGIN_MESSAGE_MAP(CExtPopupListBoxMenuWnd, CExtPopupControlMenuWnd)
	//{{AFX_MSG_MAP(CExtPopupListBoxMenuWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CExtPopupListBoxMenuWnd::CExtPopupListBoxMenuWnd(
	LPARAM lParamListCookie, // = 0L
	DWORD dwListBoxStyles // = WS_CHILD|WS_VISIBLE|WS_VSCROLL|LBS_NOINTEGRALHEIGHT|LBS_HASSTRINGS|LBS_OWNERDRAWVARIABLE
	)
	: m_lParamListCookie( lParamListCookie )
	, m_dwListBoxStyles( dwListBoxStyles )
	, m_pCbInitListBoxContent( NULL )
	, m_pInitListBoxCookie( NULL )
	, m_pCbListBoxSelection( NULL )
	, m_pSelectionCookie( NULL )
	, m_pCbListBoxItemClick( NULL )
	, m_pItemClickCookie( NULL )
	, m_pCbListBoxItemDraw( NULL )
	, m_pCbListBoxItemMeasure( NULL )
	, m_pListBoxItemCookie( NULL )
{
}

CExtPopupListBoxMenuWnd::~CExtPopupListBoxMenuWnd()
{
}

bool CExtPopupListBoxMenuWnd::_OnKeyDown(
	UINT nChar,
	UINT nRepCnt,
	UINT nFlags,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return true; //false;
	if( GetSite().GetAnimated() != NULL )
		return true;

	if( _IsResizingMode() )
	{
		bNoEat = true;
		return false;
	}

TranslateKeyboardEventData_t _td( this, nChar, nRepCnt, nFlags, bNoEat );
	if( _td.Notify() )
	{
		bNoEat = _td.m_bNoEat;
		return true;
	}

	if( nChar == VK_F4 )
	{
		bNoEat = false;
		_EndSequence();
		return true;
	}
	return
		CExtPopupControlMenuWnd::_OnKeyDown(
			nChar,
			nRepCnt,
			nFlags,
			bNoEat
			);
}

HWND CExtPopupListBoxMenuWnd::OnCreateChildControl(
	const RECT & rcChildControl
	)
{
	ASSERT_VALID( this );
CExtPopupInplaceListBox * pListBox = new CExtPopupInplaceListBox;
	pListBox->m_pCbListBoxSelection = m_pCbListBoxSelection;
	pListBox->m_pSelectionCookie = m_pSelectionCookie;
	pListBox->m_pCbListBoxItemClick = m_pCbListBoxItemClick;
	pListBox->m_pItemClickCookie = m_pItemClickCookie;
	pListBox->m_pCbInitListBoxContent = m_pCbInitListBoxContent;
	pListBox->m_pInitListBoxCookie = m_pInitListBoxCookie;
	pListBox->m_lParamCookie = m_lParamListCookie;
	pListBox->m_pCbListBoxItemDraw = m_pCbListBoxItemDraw;
	pListBox->m_pCbListBoxItemMeasure = m_pCbListBoxItemMeasure;
	pListBox->m_pListBoxItemCookie = m_pListBoxItemCookie;
	if(	! pListBox->Create(
			m_dwListBoxStyles,
			rcChildControl,
			this,
			(UINT)(IDC_STATIC)
			)
		)
	{
		ASSERT( FALSE );
		delete pListBox;
		return NULL;
	}
bool bRTL = OnQueryLayoutRTL();
	if( bRTL )
		pListBox->ModifyStyleEx( 0, WS_EX_LAYOUTRTL, SWP_FRAMECHANGED );
	return pListBox->m_hWnd;
}

#if (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)

/////////////////////////////////////////////////////////////////////////////
// CExtBarTextFieldButton::CInPlaceEditWnd

CExtBarTextFieldButton::CInPlaceEditWnd::CInPlaceEditWnd(
	CExtBarTextFieldButton * pTextFieldTBB,
	CExtSafeString * pStr,
	CExtBarTextFieldButton::pCbVerifyTextInput pCbVerify, // = NULL
	CExtBarTextFieldButton::pCbInplaceEditWndProc pCbWndProc, // = NULL
	LPVOID pCbCookie // = NULL
	)
	: m_pTbbTextField( pTextFieldTBB )
	, m_pStr( pStr )
	, m_pCbVerifyTextInput( pCbVerify )
	, m_pCbWndProc( pCbWndProc )
	, m_pCbCookie( pCbCookie )
	, m_bCanceling( false )
{
	ASSERT_VALID( m_pTbbTextField );
	ASSERT( m_pStr != NULL );
	m_pBar = m_pTbbTextField->GetBar();
	ASSERT_VALID( m_pBar );
	m_pBtnRTC = m_pTbbTextField->GetRuntimeClass();
	g_pWndInplaceEditor = this;
}

CExtBarTextFieldButton::CInPlaceEditWnd::~CInPlaceEditWnd()
{
	if( g_pWndInplaceEditor == this )
		g_pWndInplaceEditor = NULL;
}

#ifdef _DEBUG
void CExtBarTextFieldButton::CInPlaceEditWnd::AssertValid() const
{
	CEdit::AssertValid();
	ASSERT_VALID( m_pTbbTextField );
	ASSERT( m_pStr != NULL );
}
#endif // _DEBUG

bool CExtBarTextFieldButton::CInPlaceEditWnd::Create(
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pTbbTextField );
CExtToolControlBar * pBar = m_pTbbTextField->GetBar();
	ASSERT_VALID( pBar );
	ASSERT( pBar->GetSafeHwnd() != NULL && ::IsWindow(pBar->GetSafeHwnd()) );
CRect rc = m_pTbbTextField->OnInplaceControlCalcRect( m_pTbbTextField->Rect() );
bool bPopupMode = false;
CWnd * pEditParentWnd = pBar;
#if (!defined __EXT_MFC_NO_RIBBON_BAR)
	if( pBar->m_pDockBar == NULL )
	{
		CExtRibbonBar * pRibbonBar = DYNAMIC_DOWNCAST( CExtRibbonBar, pBar );
		if(		pRibbonBar != NULL
			&&	pRibbonBar->m_pExtNcFrameImpl != NULL
			&&	pRibbonBar->m_pExtNcFrameImpl->NcFrameImpl_IsDwmCaptionReplacement()
			)
		{
			CWnd * pWndParent = pBar->GetParent();
			if( pWndParent != NULL )
			{
				CRect _rectTest = rc;
				pBar->ClientToScreen( &_rectTest );
				CRect _rectPopup = _rectTest;
				pWndParent->ScreenToClient( &_rectTest );
				if( _rectTest.top <= 0 )
				{
					pEditParentWnd = pWndParent;
					bPopupMode = true;
					rc = _rectPopup;
				}
			} // if( pWndParent != NULL )
		}
	} // if( pBar->m_pDockBar == NULL )
#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)

UINT nDlgCtrlID = m_pTbbTextField->GetCmdID( false );
// 	if( ! CEdit::Create(
// 			( bPopupMode ? WS_POPUP : WS_CHILD )
// 				|WS_VISIBLE|ES_LEFT|ES_AUTOHSCROLL,
// 			rc,
// 			pBar,
// 			nDlgCtrlID
// 			)
// 		)
	if( ! CWnd::CreateEx(
			0,
			_T("EDIT"),
			_T(""),
			( bPopupMode ? WS_POPUP : WS_CHILD )
 					|WS_VISIBLE|WS_CLIPSIBLINGS|WS_CLIPCHILDREN
					|ES_LEFT|ES_AUTOHSCROLL,
			rc.left,
			rc.top,
			rc.Width(),
			rc.Height(),
			pEditParentWnd->GetSafeHwnd(),
			bPopupMode ? NULL : ((HMENU)nDlgCtrlID),
			NULL
			)
		)
	{
		ASSERT( FALSE );
		return false;
	}
	SetFont( & pBar->PmBridge_GetPM()->m_FontNormal );
	SetWindowText( *m_pStr );
	SetSel( 0, -1 ); // SetSel( 0, 0 );
	SetFocus();
bool bRTL = ( (g_ResourceManager->OnQueryLangLayout()&LAYOUT_RTL) != 0 ) ? true : false;
	if( bRTL )
		ModifyStyleEx( 0, WS_EX_LAYOUTRTL, SWP_FRAMECHANGED );
	UpdateWindow();
	return true;
}

bool CExtBarTextFieldButton::CInPlaceEditWnd::_IsValidState()
{
	ASSERT_VALID( m_pBar );
int nBtnIdx = m_pBar->_GetIndexOf( m_pTbbTextField );
	if( nBtnIdx < 0 )
		return false;
CExtBarButton * pTBB = m_pBar->GetButton( nBtnIdx );
	if( ! pTBB->IsKindOf( m_pBtnRTC ) )
		return false;
	return true;
}

BOOL CExtBarTextFieldButton::CInPlaceEditWnd::PreTranslateMessage( MSG * pMsg )
{
	ASSERT_VALID( m_pBar );
	if( ! _IsValidState() )
		return CEdit::PreTranslateMessage( pMsg );
	if( !m_bCanceling )
	{
		if( pMsg->message == WM_KEYDOWN )
		{
			bool bAlt =
				( (::GetAsyncKeyState(VK_MENU)&0x8000) != 0 )
					? true : false;
			if( !bAlt )
			{
				bool bCtrl =
					( (::GetAsyncKeyState(VK_CONTROL)&0x8000) != 0 )
						? true : false;
				bool bShift =
					( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 )
						? true : false;
				if(		bCtrl
					&&	(!bShift)
					&&	(	int(pMsg->wParam) == VK_INSERT
						||	int(pMsg->wParam) == int( _T('C') )
						)
					)
				{
					SendMessage( WM_COPY, 0, 0 );
					return TRUE;
				} 
				if(		( bCtrl && (!bShift) && int(pMsg->wParam) == int( _T('V') ) )
					||	( (!bCtrl) && bShift && int(pMsg->wParam) == VK_INSERT )
					)
				{
					SendMessage( WM_PASTE, 0, 0 );
					return TRUE;
				} 
				if(		( bCtrl && (!bShift) && int(pMsg->wParam) == int( _T('X') ) )
					||	( (!bCtrl) && bShift && int(pMsg->wParam) == VK_DELETE )
					)
				{
					SendMessage( WM_CUT, 0, 0 );
					return TRUE;
				} 
				if(	bCtrl && (!bShift) && int(pMsg->wParam) == int( _T('A') ) ) 
				{
					SetSel( 0, -1 );
					return TRUE;
				}
			} // if( !bAlt )
		} // if( pMsg->message == WM_KEYDOWN )
		else if(
				pMsg->message == WM_LBUTTONDOWN
			||	pMsg->message == WM_MBUTTONDOWN
			||	pMsg->message == WM_RBUTTONDOWN
			)
		{
			if( pMsg->hwnd != m_hWnd )
			{
				m_bCanceling = true;
				PostMessage( (WM_USER+0x666) );
			}
		}
	} // if( !m_bCanceling )
	
	// HASH Added START
	// (Allows single key accelerators)
	if(		(	pMsg->message == WM_KEYDOWN
			||	pMsg->message == WM_CHAR
			||	pMsg->message == WM_SYSKEYDOWN
			||	pMsg->message == WM_SYSKEYUP
			)
		&&	pMsg->hwnd == m_hWnd
		)
	{
		::TranslateMessage( pMsg );
		::DispatchMessage( pMsg );
		return TRUE;
	}
	// HASH Added END
	
	return CEdit::PreTranslateMessage( pMsg );
}

LRESULT CExtBarTextFieldButton::CInPlaceEditWnd::WindowProc(
	UINT message,
	WPARAM wParam,
	LPARAM lParam
	)
{
	ASSERT_VALID( m_pBar );
	if( ! _IsValidState() )
		return CEdit::WindowProc( message, wParam, lParam );
	if( m_pCbWndProc != NULL )
	{
		LRESULT lResult = 0L;
		if( m_pCbWndProc(
				lResult,
				message,
				wParam,
				lParam,
				*this,
				m_pCbCookie
				)
			)
			return lResult;
	} // if( m_pCbWndProc != NULL )
	if( !m_bCanceling )
	{
		if(		CExtPopupMenuWnd::IsMenuTracking()
			||	CExtControlBar::_DraggingGetBar() != NULL
			)
		{
			m_bCanceling = true;
			PostMessage( (WM_USER+0x666) );
		}
	}
	if( message == (WM_USER+0x666) )
	{
		GetBarTextFieldButton()->OnInplaceControlSessionCancel();
		return 0;
	}
	if( message == WM_NCCALCSIZE )
	{
		NCCALCSIZE_PARAMS * pNCCSP =
			reinterpret_cast < NCCALCSIZE_PARAMS * > ( lParam );
		ASSERT( pNCCSP != NULL );
		CRect rcInBarWnd( pNCCSP->rgrc[0] );
		rcInBarWnd.DeflateRect( 2, 2, 0, 2 );
		::CopyRect( &(pNCCSP->rgrc[0]), rcInBarWnd );
		return 0;
	} // if( message == WM_NCCALCSIZE )
	if( message == WM_NCPAINT )
	{
		CRect rcInBarWnd, rcInBarClient;
		GetWindowRect( &rcInBarWnd );
		GetClientRect( &rcInBarClient );
		ClientToScreen( &rcInBarClient );
		if( rcInBarWnd == rcInBarClient )
			return 0;
		CPoint ptDevOffset = -rcInBarWnd.TopLeft();
		rcInBarWnd.OffsetRect( ptDevOffset );
		rcInBarClient.OffsetRect( ptDevOffset );
		CWindowDC dc( this );
		ASSERT( dc.GetSafeHdc() != NULL );
		dc.ExcludeClipRect( &rcInBarClient );
		dc.FillSolidRect(
			rcInBarWnd,
			m_pBar->PmBridge_GetPM()->GetColor( COLOR_WINDOW, this )
			);
		return 0;
	} // if( message == WM_NCPAINT )
	if( message == WM_GETDLGCODE )
		return DLGC_WANTALLKEYS|DLGC_WANTCHARS|DLGC_WANTTAB|DLGC_HASSETSEL;
	if(		message == WM_RBUTTONDOWN
		||	message == WM_RBUTTONUP
		||	message == WM_RBUTTONDBLCLK
		||	message == WM_CONTEXTMENU
		)
		return 0;
	
	if( message == WM_KEYDOWN )
	{
		if(		int(wParam) == VK_MENU
			||	int(wParam) == VK_ESCAPE
			)
		{
         // STAR BugFix
     		bool bCtrl =
			   ( (::GetAsyncKeyState(VK_CONTROL)&0x8000) != 0 )
				   ? true : false;

         if( ! bCtrl ) 
         {
         // STAR BugFix -- end
         
			   GetBarTextFieldButton()->OnInplaceControlSessionCancel();
			   return 0;
         }
		}
		if(		int(wParam) == VK_DOWN
			||	int(wParam) == VK_UP
			||	int(wParam) == VK_F4
			)
		{
			CExtBarTextFieldButton * pTextFieldTBB =
				GetBarTextFieldButton();
			if( pTextFieldTBB->IsComboTextField() )
			{
				pTextFieldTBB->OnInplaceControlSessionEnd();
				pTextFieldTBB->OnTrackPopup( CPoint(0,0), true, false );
				return 0;
			}
		}

		if( int(wParam) == VK_RETURN )
		{
			ASSERT( m_pStr != NULL );
			CExtSafeString sText;
			int nTextLength = GetWindowTextLength();
			if( nTextLength > 0 )
			{
				GetWindowText( sText.GetBuffer(nTextLength+2), nTextLength+1 );
				sText.ReleaseBuffer();
			}
			if( m_pCbVerifyTextInput != NULL )
			{
				if(	m_pCbVerifyTextInput(
						*this,
						m_pCbCookie,
						sText.IsEmpty() ? _T("") : sText,
						sText.IsEmpty() ? _T("") : sText
						)
					)
					*m_pStr = sText;
			} // if( m_pCbVerifyTextInput != NULL )
			else
				*m_pStr = sText;
			GetBarTextFieldButton()->OnInplaceControlSessionEnd();
			return 0;
		}
		bool bAlt =
			( (::GetAsyncKeyState(VK_MENU)&0x8000) != 0 )
				? true : false;
		if( bAlt )
		{
         // STAR BugFix
     		bool bCtrl =
			   ( (::GetAsyncKeyState(VK_CONTROL)&0x8000) != 0 )
				   ? true : false;

         if( ! bCtrl ) 
         // STAR BugFix -- end
         {
			   GetBarTextFieldButton()->OnInplaceControlSessionCancel();
			   return 0;
         }
		}

		ASSERT( m_pStr != NULL );
		CString sTextOld;
		GetWindowText( sTextOld );
		DWORD dwSelSaved = CEdit::GetSel();
		CEdit::SetRedraw( FALSE );
		LRESULT lResult = CEdit::WindowProc( message, wParam, lParam );
		CString sTextNew;
		GetWindowText( sTextNew );
		if( m_pCbVerifyTextInput != NULL )
		{
			if(	m_pCbVerifyTextInput(
					*this,
					m_pCbCookie,
					sTextOld.IsEmpty() ? _T("") : (LPCTSTR)sTextOld,
					sTextNew.IsEmpty() ? _T("") : (LPCTSTR)sTextNew
					)
				)
				*m_pStr = sTextNew;
			else
			{
				CEdit::SetSel( 0, -1 );
				CEdit::ReplaceSel( sTextOld );
				CEdit::SetSel( dwSelSaved );
			}
		} // if( m_pCbVerifyTextInput != NULL )
		else
			*m_pStr = sTextNew;
		CEdit::SetRedraw( TRUE );
		Invalidate();
		UpdateWindow();
		
		return lResult;
	} // if( message == WM_KEYDOWN )
	else if( message == WM_CHAR )
	{
		ASSERT( m_pStr != NULL );
		CString sTextOld;
		GetWindowText( sTextOld );
		DWORD dwSelSaved = CEdit::GetSel();
		CEdit::SetRedraw( FALSE );
		LRESULT lResult = CEdit::WindowProc( message, wParam, lParam );
		CString sTextNew;
		GetWindowText( sTextNew );
		if( m_pCbVerifyTextInput != NULL )
		{
			if(	m_pCbVerifyTextInput(
					*this,
					m_pCbCookie,
					sTextOld.IsEmpty() ? _T("") : (LPCTSTR)sTextOld,
					sTextNew.IsEmpty() ? _T("") : (LPCTSTR)sTextNew
					)
				)
				*m_pStr = sTextNew;
			else
			{
				CEdit::SetSel( 0, -1 );
				CEdit::ReplaceSel( sTextOld );
				CEdit::SetSel( dwSelSaved );
			}
		} // if( m_pCbVerifyTextInput != NULL )
		else
			*m_pStr = sTextNew;
		CEdit::SetRedraw( TRUE );
		Invalidate();
		UpdateWindow();

		return lResult;
	} // else if( message == WM_CHAR )
	else if( message == WM_KILLFOCUS || message == WM_CANCELMODE )
	{
		GetBarTextFieldButton()->OnInplaceControlSessionCancel();
		return 0;
	} // else if( message == WM_KILLFOCUS || message == WM_CANCELMODE )

	return CEdit::WindowProc( message, wParam, lParam );
}

void CExtBarTextFieldButton::CInPlaceEditWnd::PostNcDestroy()
{
	ASSERT_VALID( this );
	delete this;
}

/////////////////////////////////////////////////////////////////////////////
// CExtBarTextFieldButton

IMPLEMENT_DYNCREATE(CExtBarTextFieldButton, CExtBarButton)

CExtBarTextFieldButton::CExtBarTextFieldButton(
	bool bComboField, // = false
	INT nTextFieldWidth, // = __EXT_MENU_DEF_INPLACE_EDIT_WIDTH
	CExtToolControlBar * pBar, // = NULL
	UINT nCmdID, // = ID_SEPARATOR
	UINT nStyle // = 0
	)
	: CExtBarButton( pBar, nCmdID, nStyle )
	, m_nTextFieldWidth( nTextFieldWidth )
	, m_bComboField( bComboField )
	, m_bComboPopupDropped( false )
	, m_lParamCookie( 0L )
	, m_dwListBoxStyles( WS_CHILD|WS_VISIBLE|WS_VSCROLL|LBS_NOINTEGRALHEIGHT|LBS_HASSTRINGS|LBS_OWNERDRAWVARIABLE )
	, m_bHelperFindListInitialItem( false )
	, m_nDropDownWidth( -2 ) // (-1) - auto calc, (-2) - same as button area
	, m_nDropDownHeightMax( 250 )
	, m_bTextFieldIsNotEditable( false )
	, m_bRunInplaceControlAfterPopupClosed( false )
{
	ASSERT( m_nTextFieldWidth >= 0 );
}

CExtBarTextFieldButton::~CExtBarTextFieldButton()
{
//	OnInplaceControlSessionCancel();
}

CExtBarTextFieldButton::CInPlaceEditWnd *
	CExtBarTextFieldButton::CInPlaceEditWnd::g_pWndInplaceEditor = NULL;

void CExtBarTextFieldButton::_CancelInplaceEditor()
{
	if( CExtBarTextFieldButton::CInPlaceEditWnd::g_pWndInplaceEditor->GetSafeHwnd() != NULL )
	{
		ASSERT_VALID( CExtBarTextFieldButton::CInPlaceEditWnd::g_pWndInplaceEditor );
		CExtBarTextFieldButton::CInPlaceEditWnd::g_pWndInplaceEditor->
			GetBarTextFieldButton()->OnInplaceControlSessionEnd();
		if( CExtBarTextFieldButton::CInPlaceEditWnd::g_pWndInplaceEditor->GetSafeHwnd() != NULL )
			CExtBarTextFieldButton::CInPlaceEditWnd::g_pWndInplaceEditor->DestroyWindow();
	} // if( CExtBarTextFieldButton::CInPlaceEditWnd::g_pWndInplaceEditor->GetSafeHwnd() != NULL )
}

INT CExtBarTextFieldButton::GetTextFieldWidth() const
{
	ASSERT_VALID( this );
	ASSERT( m_nTextFieldWidth > 0 );
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeCmdTreeNode * pNode = ((CExtBarTextFieldButton*)this)->GetCmdNode( false );
	if( pNode != NULL )
	{
		ASSERT_VALID( pNode );
		return pNode->TextFieldWidthGet();
	} // if( pNode != NULL )
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
	return m_nTextFieldWidth;
}

INT CExtBarTextFieldButton::SetTextFieldWidth( INT nTextFieldWidth )
{
	ASSERT_VALID( this );
	ASSERT( nTextFieldWidth > 0 );
	OnInplaceControlSessionCancel();
INT nTextFieldWidthOld = GetTextFieldWidth();
	m_nTextFieldWidth = nTextFieldWidth;
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeCmdTreeNode * pNode = GetCmdNode( false );
	if( pNode != NULL )
	{
		ASSERT_VALID( pNode );
		pNode->TextFieldWidthSet( nTextFieldWidth );
	} // if( pNode != NULL )
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
	return nTextFieldWidthOld;
}

__EXT_MFC_SAFE_LPCTSTR CExtBarTextFieldButton::GetFieldText() const
{
	ASSERT_VALID( this );
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if( pSite != NULL )
	{
		pSite->OnTextFieldInplaceTextGet(
			this,
			((CExtBarButton *)this)->GetCmdNode( false ),
			*( (CExtSafeString *)&m_sTextField ) // emulate C++ property mutability
			);
		CExtCustomizeCmdTreeNode * pNode =
			((CExtBarButton*)this)->GetCmdNode( false );
		if( pNode != NULL )
		{
			ASSERT_VALID( pNode );
			pNode->m_sDefInplaceEditBuffer = m_sTextField;
		} // m_sTextField
	} // if( pSite != NULL )
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
	return m_sTextField;
}

bool CExtBarTextFieldButton::SetFieldText(
	__EXT_MFC_SAFE_LPCTSTR sText,
	bool bVerify // = true
	)
{
	ASSERT_VALID( this );
	if(		bVerify
		&&	(!	OnInplaceControlVerifyTextInput(
					NULL,
					( GetFieldText() == NULL ) ? _T("") : GetFieldText(),
					( sText == NULL ) ? _T("") : sText
				)
			)
		)
		return false;
LPCTSTR sNewText = ( sText == NULL ) ? _T("") : sText;
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if( pSite != NULL )
	{
		CExtCustomizeCmdTreeNode * pNode = GetCmdNode( false );
		if( pNode != NULL )
		{
			ASSERT_VALID( pNode );
			pNode->m_sDefInplaceEditBuffer = m_sTextField;
		} // m_sTextField
		pSite->OnTextFieldInplaceTextSet(
			this,
			GetCmdNode( false ),
			m_sTextField,
			sNewText
			);
	}
	else
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
		m_sTextField = sNewText;
	return true;
}

bool CExtBarTextFieldButton::AnimationClient_OnQueryEnabledState(
	INT eAPT // __EAPT_*** anumation type
	) const
{
	ASSERT_VALID( this );
	if( IsComboTextField() && IsComboPopupDropped() )
		return false;
	return CExtBarButton::AnimationClient_OnQueryEnabledState( eAPT );
}

CRect CExtBarTextFieldButton::OnCalcComboDropRect( const RECT & rcBtnArea ) const
{
	ASSERT_VALID( this );
	if( !IsComboTextField() )
		return CRect( 0, 0, 0, 0 );
CRect rcComboDropRect( rcBtnArea );
	rcComboDropRect.left = 
		rcComboDropRect.right - GetBar()->PmBridge_GetPM()->GetDropDownButtonWidth( (CObject*)this );
	return rcComboDropRect;
}

CRect CExtBarTextFieldButton::OnInplaceControlCalcRect( const RECT & rcBtnArea ) const
{
	ASSERT_VALID( this );
CRect rcCtrl( rcBtnArea );
CRect rcControlMarginSizes = OnInplaceControlCalcMarginSizes();
	rcCtrl.DeflateRect(
		rcControlMarginSizes.left,
		rcControlMarginSizes.top,
		rcControlMarginSizes.right,
		rcControlMarginSizes.bottom
		);
	if( IsComboTextField() )
		rcCtrl.right -= GetBar()->PmBridge_GetPM()->GetDropDownButtonWidth( (CObject*)this );
	return rcCtrl;
}

CRect CExtBarTextFieldButton::OnInplaceControlCalcMarginSizes() const
{
	ASSERT_VALID( this );
	return CRect( 2, 2, 2, 2 );
}

CWnd * CExtBarTextFieldButton::OnInplaceControlCreate()
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
CInPlaceEditWnd * pEdit =
		new CInPlaceEditWnd(
			this,
			&m_sTextInplaceBuffer,
			(CExtBarTextFieldButton::pCbVerifyTextInput)stat_CbVerifyTextInput,
			(CExtBarTextFieldButton::pCbInplaceEditWndProc)stat_CbInplaceEditWndProc,
			this
			);
	if( ! pEdit->Create() )
	{
		ASSERT( FALSE );
		m_sTextInplaceBuffer.Empty();
		delete pEdit;
		return NULL;
	}
bool bRTL = ( (g_ResourceManager->OnQueryLangLayout()&LAYOUT_RTL) != 0 ) ? true : false;
	if( bRTL )
		pEdit->ModifyStyleEx( 0, WS_EX_LAYOUTRTL, SWP_FRAMECHANGED );
	return pEdit;
}

CExtBarTextFieldButton * CExtBarTextFieldButton::g_pBtnEditing = NULL;

void CExtBarTextFieldButton::OnInplaceControlRun()
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
CExtToolControlBar * pBar = m_pBar;
CExtBarTextFieldButton * pThis = this;
	OnInplaceControlSessionCancel();
__EXT_MFC_SAFE_LPCTSTR sText = GetFieldText();
	m_sTextInplaceBuffer = (sText == NULL) ? _T("") : sText;
CWnd * pEdit = OnInplaceControlCreate();
	if( pEdit == NULL )
		return;
	ASSERT_VALID( pEdit );
	ASSERT(
			pEdit->GetSafeHwnd() != NULL
		&&	::IsWindow( pEdit->GetSafeHwnd() )
		);
	CtrlSet( pEdit, true );
	RedrawButton();
HWND hWndCtrl = pEdit->m_hWnd;
	g_pBtnEditing = this;
	for( MSG msg; ::IsWindow(hWndCtrl); )
	{
		// Process all the messages in the message queue
		while(
				PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE)
			&&	::IsWindow(hWndCtrl)
			)
		{
			bool bStop = false;
			switch( msg.message )
			{
			//case WM_CONTEXTMENU:
			case WM_ACTIVATEAPP:
			case WM_CANCELMODE:
				bStop = true;
			break;
			case WM_NCLBUTTONDOWN:
			case WM_NCRBUTTONDOWN:
			case WM_NCMBUTTONDOWN:
			case WM_LBUTTONDOWN:
			case WM_RBUTTONDOWN:
			case WM_MBUTTONDOWN:
				if( msg.hwnd != hWndCtrl )
					bStop = true;
			break;
			default:
				if( msg.hwnd != hWndCtrl )
				{
					if(		WM_KEYFIRST <= msg.message
						&&	msg.message <= WM_KEYLAST
						)
					{
						bStop = true;
						break;
					}
					if(		WM_MOUSEFIRST <= msg.message
						&&	msg.message <= WM_MOUSELAST
						)
					{
						PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);
						continue;
					}
				} // if( msg.hwnd != hWndCtrl )
			} // switch( msg.message )
			if( bStop )
			{
				if( pBar->_GetIndexOf(pThis) < 0 )
					break;
				OnInplaceControlSessionCancel();
				break;
			} // if( bStop )
			if( !AfxGetThread()->PumpMessage() )
			{
				PostQuitMessage(0);
				break; // Signal WM_QUIT received
			} // if( !AfxGetThread()->PumpMessage() )
		} // while( PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) ...
	} // for( MSG msg; ::IsWindow(hWndCtrl); )
	if( g_pBtnEditing == this )
		g_pBtnEditing = NULL;
}

void CExtBarTextFieldButton::OnInplaceControlSessionEnd()
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
CExtToolControlBar * pBar = m_pBar;
CExtBarTextFieldButton * pThis = this;
//CExtSafeString strCurrentFieldText = GetFieldText();
//	if( strCurrentFieldText != m_sTextInplaceBuffer )
		SetFieldText( m_sTextInplaceBuffer );
	if( pBar->_GetIndexOf(pThis) < 0 )
		return;
	CtrlSet( NULL, true );
	m_sTextInplaceBuffer.Empty();
	if(		m_pBar->m_hWnd != NULL
		&&	::IsWindow( m_pBar->m_hWnd )
		)
		RedrawButton();
	if( m_pBar->m_pDockSite->GetSafeHwnd() != NULL )
		m_pBar->m_pDockSite->SetFocus();
	else
		m_pBar->GetParent()->SetFocus();
}

void CExtBarTextFieldButton::OnInplaceControlSessionCancel()
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	CtrlSet( NULL, true );
	m_sTextInplaceBuffer.Empty();
	if(		m_pBar->m_hWnd != NULL
		&&	::IsWindow( m_pBar->m_hWnd )
		)
		RedrawButton();
	if( m_pBar->m_pDockSite->GetSafeHwnd() != NULL )
		m_pBar->m_pDockSite->SetFocus();
	else
		m_pBar->GetParent()->SetFocus();
}

bool CExtBarTextFieldButton::OnInplaceControlVerifyTextInput(
	CEdit * pEdit,
	__EXT_MFC_SAFE_LPCTSTR sTextOld,
	__EXT_MFC_SAFE_LPCTSTR sTextNew
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	ASSERT( sTextOld != NULL );
	ASSERT( sTextNew != NULL );
	pEdit;
	sTextOld;
	sTextNew;

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if( pSite != NULL )
		return
			pSite->OnTextFieldVerify(
				this,
				GetCmdNode( false ),
				sTextOld,
				sTextNew
				);
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	return true;
}

void CExtBarTextFieldButton::OnInplaceControlPutTextInputResult(
	CEdit * pEdit,
	__EXT_MFC_SAFE_LPCTSTR sTextNew
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	ASSERT( sTextNew != NULL );
	pEdit;
	VERIFY( SetFieldText( sTextNew, false ) );
}

bool CExtBarTextFieldButton::OnInplaceControlWndProcCall(
	LRESULT & lResult,
	UINT message,
	WPARAM wParam,
	LPARAM lParam,
	CEdit & wndEdit
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if( pSite != NULL )
		return
			pSite->OnTextFieldWndProcHook(
				lResult,
				message,
				wParam,
				lParam,
				wndEdit,
				this,
				GetCmdNode( false )
				);
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
	lResult;
	message;
	wParam;
	lParam;
	wndEdit;
	return false;
}

void CExtBarTextFieldButton::_UpdateCtrl()
{
	ASSERT_VALID( this );
//	CExtBarButton::_UpdateCtrl();
CWnd * pWndInplaceControl = CtrlGet();
	if( pWndInplaceControl == NULL )
		return;
	ASSERT_VALID( pWndInplaceControl );
CRect rcButton = Rect();
CRect rcCtrl = OnInplaceControlCalcRect( rcButton );
	pWndInplaceControl->SetWindowPos(
		NULL,
		rcCtrl.left, rcCtrl.top,
		rcCtrl.Width(), rcCtrl.Height(),
		SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOACTIVATE
			|	(	(	IsVisible()
					&&	(GetStyle() & TBBS_HIDDEN) == 0
					&&	(!m_bVertDocked || GetCtrlVisibleVertically())
					)
					? SWP_SHOWWINDOW
					: SWP_HIDEWINDOW
				)
			|	(	( pWndInplaceControl->GetParent() == GetBar() )
						? 0
						: (SWP_NOMOVE|SWP_NOSIZE)
				)
		);
	pWndInplaceControl->EnableWindow( IsEnabled() );
	if( pWndInplaceControl->IsKindOf(RUNTIME_CLASS(CButton)) )
	{
		int nCheck = 0;
		if( IsPressed() )
			nCheck = 1;
		else if( IsIndeterminate() )
			nCheck = 2;
		((CButton *)(pWndInplaceControl))->SetCheck( nCheck );
	}
//	if( m_bVisible && m_bHover )
//		pWndInplaceControl->SetFocus();
}

CExtSafeString CExtBarTextFieldButton::GetText() const
{
	ASSERT_VALID( this );
	//return CExtSafeString( _T("") );
	return CExtBarButton::GetText();
}

void CExtBarTextFieldButton::SetMenu(
	HMENU hMenu,
	bool bPopupMenu,
	bool bAutoDestroyMenu
	)
{
	ASSERT_VALID( this );
	ASSERT( FALSE ); // should not be used with text-field button
	hMenu;
	bPopupMenu;
	bAutoDestroyMenu;
}

HMENU CExtBarTextFieldButton::GetMenu()
{
	ASSERT_VALID( this );
	return NULL;
}

//bool CExtBarTextFieldButton::IsLargeIcon() const
//{
//	ASSERT_VALID( this );
//	return false;
//}
//
//bool CExtBarTextFieldButton::IsDisplayScreenTip() const
//{
//	ASSERT_VALID( this );
//	return false;
//}
//
//bool CExtBarTextFieldButton::IsDisplayShortcutKeysOnScreenTip() const
//{
//	ASSERT_VALID( this );
//	return false;
//}

void CExtBarTextFieldButton::SetRect( const RECT & rectButton )
{
	ASSERT_VALID( this );
CExtToolControlBar * pBar = GetBar();
	ASSERT_VALID( pBar );
	pBar->AnimationSite_ClientProgressStop( this );
	AnimationClient_StateGet( false ).Empty();
	AnimationClient_StateGet( true ).Empty();
	AnimationClient_TargetRectSet( rectButton );
	m_ActiveRect = rectButton;
CWnd * pWndInplaceControl = CtrlGet();
	if( (pWndInplaceControl != NULL) && (!m_bVertDocked || GetCtrlVisibleVertically()) )
	{
		ASSERT_VALID( pWndInplaceControl );
		CRect rcCtrl = OnInplaceControlCalcRect( rectButton );
		if( pWndInplaceControl->GetParent() == pBar )
			pWndInplaceControl->MoveWindow( &rcCtrl, FALSE );
	} // if( (pWndInplaceControl != NULL) && (!m_bVertDocked || GetCtrlVisibleVertically()) )
	_UpdateCtrl();
}

bool CExtBarTextFieldButton::CanBePressedInDisabledState()
{
	return false;
}

bool CExtBarTextFieldButton::IsAbleToTrackMenu(
	bool bCustomizeMode // = false
	) const
{
	ASSERT_VALID( this );
	if( bCustomizeMode )
		return false;
const CExtToolControlBar * pBar = GetBar();
	if( pBar->GetSafeHwnd() == NULL )
		return false;
bool bHorz = ((((CExtToolControlBar*)pBar)->GetBarStyle()&CBRS_ORIENT_HORZ)!=0) ? true : false;
	if( ! bHorz )
	{
		if( ! GetNoRotateVerticalLayout() )
			return false;
	}
	return true;
}

bool CExtBarTextFieldButton::IsContainerOfPopupLikeMenu()
{
	ASSERT_VALID( this );
	return false;
}

//CExtCmdIcon * CExtBarTextFieldButton::GetIconPtr()
//{
//	ASSERT_VALID( this );
//	return NULL; // no icon at all for text-fields
//}

CSize CExtBarTextFieldButton::CalculateLayout(
	CDC & dc,
	CSize sizePreCalc,
	BOOL bHorz
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	ASSERT_VALID( (&dc) );

	if(		(! bHorz )
		&&	(! GetCtrlVisibleVertically() )
		)
		return CExtBarButton::CalculateLayout( dc, sizePreCalc, bHorz );

static CExtSafeString g_sTestText( _T("AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789;[]{}\\/=+-_*&^%$#@!~") );
CRect rcTestText =
		CExtPaintManager::stat_CalcTextDimension(
			dc,
			GetBar()->PmBridge_GetPM()->m_FontNormal,
			g_sTestText
			);
	m_ActiveSize.cx = GetTextFieldWidth();
	m_ActiveSize.cy = rcTestText.Height() + 4;
CRect rcControlMarginSizes = OnInplaceControlCalcMarginSizes();
	m_ActiveSize.cx +=
		rcControlMarginSizes.left + rcControlMarginSizes.right;
	m_ActiveSize.cy +=
		rcControlMarginSizes.top + rcControlMarginSizes.bottom;
	return m_ActiveSize;
}

#if (!defined __EXT_MFC_NO_RIBBON_BAR)

CSize CExtBarTextFieldButton::RibbonILV_CalcSize(
	CDC & dc,
	INT nILV // = -1 // -1 use current visual level
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	nILV;
CSize _sizePreCalc( 0, 0 );
	if( ! IsVisible() )
		return _sizePreCalc;
	if( (GetStyle()&TBBS_HIDDEN) != 0 )
		return _sizePreCalc;
CSize _size =
		( const_cast < CExtBarTextFieldButton * > ( this ) )
		-> CalculateLayout( dc, _sizePreCalc, true );
	_size.cy --;
	return _size;
}

#endif // (!defined __EXT_MFC_NO_RIBBON_BAR)

BOOL CExtBarTextFieldButton::PutToPopupMenu(
	CExtPopupMenuWnd * pPopup
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );

	OnInplaceControlSessionCancel();
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
	if( IsComboTextField() )
	{
		CExtPopupMenuWnd * pSub = OnCreateDropPopup( true );
		if( pSub == NULL )
		{
			ASSERT( FALSE );
			return FALSE;
		}
		ASSERT_VALID( pSub );
		CExtCmdItem * pCmdItem =
			g_CmdManager->CmdGetPtr(
				g_CmdManager->ProfileNameFromWnd( m_pBar->GetSafeHwnd() ),
				GetCmdID( false )
				);
		ASSERT( pCmdItem != NULL );
		CExtSafeString sText = pCmdItem->m_sMenuText;
		if( sText.IsEmpty() )
			sText = pCmdItem->m_sToolbarText;
// added in 2.55
		const CExtCmdIcon & _icon = GetIcon();
// commented in 2.55
// 		const CExtCmdIcon & _icon =
// 			g_CmdManager->CmdGetIcon(
// 				g_CmdManager->ProfileNameFromWnd( pBar->GetSafeHwnd() ),
// 				GetCmdID( false )
// 				);
		if( ! pPopup->ItemInsertSpecPopup( pSub, -1, sText, _icon ) )
		{
			ASSERT( FALSE );
			return FALSE;
		}
		CExtCustomizeCmdTreeNode * pNode = GetCmdNode();
		if( pNode != NULL )
		{
			ASSERT_VALID( pNode );
			pPopup->ItemGetInfo(
				pPopup->ItemGetCount() - 1
				).SetCmdNode( pNode );
		} // if( pNode != NULL )
	} // if( IsComboTextField() )
	else
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
	{
		if( !pPopup->ItemInsert( GetCmdID(false) ) )
		{
			ASSERT( FALSE );
			return FALSE;
		}
	} // else from if( IsComboTextField() )
CExtPopupMenuWnd::MENUITEMDATA & mi =
		pPopup->ItemGetInfo( pPopup->ItemGetCount() - 1 );
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
//CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
//	if( pSite != NULL )
//		pSite->OnTextFieldInplaceTextGet(
//			this,
//			GetCmdNode( false ),
//			m_sTextField
//			);
	ASSERT_VALID( GetBar()->m_pDockSite );
	mi.UpdateFromCmdTree(
		GetBar()->m_pDockSite->m_hWnd,
		GetCmdNode(),
		GetCmdNode()->GetOwnIndex(),
		pPopup
		);
#else
int nInplaceEditWidth = m_nTextFieldWidth;
	mi.SetInplaceEdit(
		&m_sTextField,
		(CExtPopupMenuWnd::pCbVerifyTextInput)stat_CbVerifyTextInput,
		(CExtPopupMenuWnd::pCbPutTextInputResult)stat_CbPutTextInputResult,
		(CExtPopupMenuWnd::pCbInplaceEditWndProc)stat_CbInplaceEditWndProc,
		this,
		nInplaceEditWidth
		);
bool bEnabled = IsDisabled() ? false : true;
	mi.Enable( bEnabled );
	mi.AllowInplaceEditActivation( (!m_bTextFieldIsNotEditable) && bEnabled );
	mi.MeasureItem( NULL );
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
	pPopup->_SyncItems();

	
	return TRUE;
}

bool CExtBarTextFieldButton::stat_CbVerifyTextInput(
	CInPlaceEditWnd & edit,
	CExtBarTextFieldButton * pTextFieldTBB,
	__EXT_MFC_SAFE_LPCTSTR sTextOld,
	__EXT_MFC_SAFE_LPCTSTR sTextNew
	)
{
	ASSERT_VALID( pTextFieldTBB );
	ASSERT_KINDOF( CExtBarTextFieldButton, pTextFieldTBB );
	return
		pTextFieldTBB->OnInplaceControlVerifyTextInput(
			&edit,
			sTextOld,
			sTextNew
			);
}

void CExtBarTextFieldButton::stat_CbPutTextInputResult(
	CInPlaceEditWnd & edit,
	CExtBarTextFieldButton * pTextFieldTBB,
	__EXT_MFC_SAFE_LPCTSTR sTextNew
	)
{
	ASSERT_VALID( pTextFieldTBB );
	ASSERT_KINDOF( CExtBarTextFieldButton, pTextFieldTBB );
	ASSERT( sTextNew != NULL );
	pTextFieldTBB->OnInplaceControlPutTextInputResult(
		&edit,
		sTextNew
		);
}

bool CExtBarTextFieldButton::stat_CbInplaceEditWndProc(
	LRESULT & lResult,
	UINT message,
	WPARAM wParam,
	LPARAM lParam,
	CEdit & wndEdit,
	CExtBarTextFieldButton * pTextFieldTBB
	)
{
	ASSERT_VALID( pTextFieldTBB );
	ASSERT_KINDOF( CExtBarTextFieldButton, pTextFieldTBB );
	return
		pTextFieldTBB->OnInplaceControlWndProcCall(
			lResult,
			message,
			wParam,
			lParam,
			wndEdit
			);
}

bool CExtBarTextFieldButton::stat_CbInitListBoxContent(
	CExtPopupInplaceListBox & wndListBox,
	CExtBarTextFieldButton * pTextFieldTBB
	)
{
	ASSERT_VALID( pTextFieldTBB );
	ASSERT_KINDOF( CExtBarTextFieldButton, pTextFieldTBB );
	return
		pTextFieldTBB->OnPopupListBoxInitContent(
			wndListBox
			);
}

bool CExtBarTextFieldButton::stat_CbListBoxSelection(
	CExtPopupInplaceListBox & wndListBox,
	CExtBarTextFieldButton * pTextFieldTBB,
	int eSAT // CExtPopupInplaceListBox::e_sel_action_t
	)
{
	ASSERT_VALID( pTextFieldTBB );
	ASSERT_KINDOF( CExtBarTextFieldButton, pTextFieldTBB );
	switch( ((CExtPopupInplaceListBox::e_sel_action_t)eSAT) )
	{
	case CExtPopupInplaceListBox::__SAT_SELCHANGE:
		return
			pTextFieldTBB->OnPopupListBoxSelChange(
				wndListBox
				);
	case CExtPopupInplaceListBox::__SAT_SELENDOK:
		return
			pTextFieldTBB->OnPopupListBoxSelEndOK(
				wndListBox
				);
	case CExtPopupInplaceListBox::__SAT_SELENDCANCEL:
		return
			pTextFieldTBB->OnPopupListBoxSelEndCancel(
				wndListBox
				);
	case CExtPopupInplaceListBox::__SAT_CLOSE:
		return
			pTextFieldTBB->OnPopupListBoxClose(
				wndListBox
				);
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( ((CExtPopupInplaceListBox::e_sel_action_t)eSAT) )
	return false;
}

bool CExtBarTextFieldButton::stat_CbListBoxItemDraw(
	CExtPopupInplaceListBox & wndListBox,
	CExtBarTextFieldButton * pTextFieldTBB,
	LPDRAWITEMSTRUCT pDIS
	)
{
	ASSERT_VALID( pTextFieldTBB );
	ASSERT_KINDOF( CExtBarTextFieldButton, pTextFieldTBB );
	ASSERT( pDIS != NULL && pDIS->hDC != NULL );
	return
		pTextFieldTBB->OnPopupListBoxItemDraw(
			wndListBox,
			pDIS
			);
}

bool CExtBarTextFieldButton::stat_CbListBoxItemMeasure(
	CExtPopupInplaceListBox & wndListBox,
	CExtBarTextFieldButton * pTextFieldTBB,
	LPMEASUREITEMSTRUCT pMIS
	)
{
	ASSERT_VALID( pTextFieldTBB );
	ASSERT_KINDOF( CExtBarTextFieldButton, pTextFieldTBB );
	ASSERT( pMIS != NULL );
	return
		pTextFieldTBB->OnPopupListBoxItemMeasure(
			wndListBox,
			pMIS
			);
}

UINT CExtBarTextFieldButton::OnTrackPopup(
	CPoint point,
	bool bSelectAny,
	bool bForceNoAnimation
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	point;
bool bDockSiteCustomizeMode =
		m_pBar->_IsDockSiteCustomizeMode();
	if( bDockSiteCustomizeMode )
		return UINT(-1L);
	if( !IsEnabled() )
		return UINT(-1L);
	if( !IsComboTextField() )
		return UINT(-1L);
	if(		m_bVertDocked
		&&	(! GetCtrlVisibleVertically() )
		)
		return UINT(-1L);
	if( IsComboPopupDropped() )
		return UINT(-1L);
	CExtToolControlBar::_CloseTrackingMenus();
//	CExtPopupMenuWnd::CancelMenuTracking();
	OnInplaceControlSessionCancel();
	if( m_pBar->IsFloating() )
	{
		m_pBar->ActivateTopParent();
		CFrameWnd * pFrame =
			m_pBar->GetDockingFrame();
		ASSERT_VALID( pFrame );
		pFrame->BringWindowToTop();
	}
//CWnd * pWndCmdTarget = GetCmdTargetWnd();
//	ASSERT_VALID( pWndCmdTarget );

CExtPopupMenuWnd * pPopup = OnCreateDropPopup( false );
	if( pPopup == NULL )
	{
		ASSERT( FALSE );
		return UINT(-1L);
	}
	ASSERT_VALID( pPopup );
CRect rcButton = OnInplaceControlCalcRect( Rect() );
CRect rcMB = pPopup->OnQueryMenuBorderMetrics();
	rcButton.InflateRect( rcMB.left, rcMB.top, rcMB.right, rcMB.bottom );
	rcButton.right +=
		GetBar()->PmBridge_GetPM()->GetDropDownButtonWidth( this ) - rcMB.left;
DWORD dwTrackFlags =
		OnGetTrackPopupFlags()
		| TPMX_COMBINE_NONE
		| TPMX_OWNERDRAW_FIXED
		| TPMX_DO_MESSAGE_LOOP
		;
	if( bForceNoAnimation )
		dwTrackFlags |= TPMX_FORCE_NO_ANIMATION;
	m_pBar->ClientToScreen( &rcButton );
	m_pBar->ClientToScreen( &point );
	ModifyStyle( TBBS_PRESSED, 0 );
	m_bComboPopupDropped = true;
	m_sTextInplaceBuffer = GetFieldText();
	RedrawButton( true );
	m_bHelperFindListInitialItem = bSelectAny;

CExtToolControlBar * pBar = m_pBar;
HWND hWndBar = pBar->m_hWnd;
	ASSERT( hWndBar != NULL && ::IsWindow(hWndBar) );
CExtBarTextFieldButton * pThis = this;
	pPopup->m_hWndNotifyMenuClosed = pBar->GetSafeHwnd();
	if( ! pPopup->TrackPopupMenu(
			dwTrackFlags,
			point.x,point.y,
			&rcButton,
			NULL, //m_pBar
			NULL, //CExtToolControlBar::_CbPaintCombinedContent
			NULL,
			false // true
			)
		)
	{
		delete pPopup;
//		CExtToolControlBar::_CloseTrackingMenus();
//		return UINT(-1L);
	}
	if(		(! ::IsWindow(hWndBar) )
		||	CWnd::FromHandlePermanent(hWndBar) != pBar
		||	pBar->_GetIndexOf( pThis ) < 0
		||	(! pThis->IsKindOf(RUNTIME_CLASS(CExtBarTextFieldButton)) )
		)
		return UINT(-1L);

	ModifyStyle( 0, TBBS_PRESSED );
	m_bComboPopupDropped = false;
	RedrawButton( false );

	if(		(! m_bTextFieldIsNotEditable)
		&&	m_bRunInplaceControlAfterPopupClosed
		)
		OnInplaceControlRun();

//	CExtToolControlBar::g_bMenuTracking = true;
//	m_pBar->_SwitchMenuTrackingIndex(
//		m_pBar->_GetIndexOf( this )
//		);
	return UINT(-1L);
}

CExtPopupMenuWnd * CExtBarTextFieldButton::OnCreateDropPopup(
	bool bContentExpand // true - inserting to content expand menu, false - tracking dropped popup
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	bContentExpand;

CWnd * pWndCmdTarget = GetCmdTargetWnd();
	ASSERT_VALID( pWndCmdTarget );

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if( pSite != NULL )
	{
		CExtPopupControlMenuWnd * pExtCrtPopup =
			pSite->OnTextFieldCreateDropPopup(
				this,
				GetCmdNode( false ),
				pWndCmdTarget->m_hWnd,
				bContentExpand
				);
		if( pExtCrtPopup != NULL )
			return pExtCrtPopup;
	}
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

CExtPopupListBoxMenuWnd * pPopup = new CExtPopupListBoxMenuWnd;
	
	pPopup->m_lParamListCookie = m_lParamCookie;

	pPopup->m_pCbListBoxSelection = 
		(CExtPopupInplaceListBox::pCbListBoxSelection)
			stat_CbListBoxSelection;
	pPopup->m_pSelectionCookie = (LPVOID)this;
	
	pPopup->m_pCbInitListBoxContent =
		(CExtPopupInplaceListBox::pCbInitListBoxContent)
			stat_CbInitListBoxContent;
	pPopup->m_pInitListBoxCookie = (LPVOID)this;

	pPopup->m_pCbListBoxItemDraw =
		(CExtPopupInplaceListBox::pCbListBoxItemDraw)
			stat_CbListBoxItemDraw;
	pPopup->m_pCbListBoxItemMeasure =
		(CExtPopupInplaceListBox::pCbListBoxItemMeasure)
			stat_CbListBoxItemMeasure;
	pPopup->m_pListBoxItemCookie = (LPVOID)this;

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
	if(		pSite == NULL
		||	(!	pSite->OnPopupListBoxGetStyles(
					this,
					GetCmdNode( false ),
					pPopup->m_dwListBoxStyles
					)
			)
		)
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
		pPopup->m_dwListBoxStyles = m_dwListBoxStyles;

CSize sizeMeasure = OnPopupListBoxMeasureTrackSize();
	pPopup->m_sizeChildControl = sizeMeasure;

	VERIFY( pPopup->CreatePopupMenu( pWndCmdTarget->m_hWnd ) );
	return pPopup;
}

CSize CExtBarTextFieldButton::OnPopupListBoxMeasureTrackSize() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if( pSite != NULL )
	{
		CSize _size =
			pSite->OnPopupListBoxMeasureTrackSize(
				this,
				((CExtBarButton*)this)->GetCmdNode( false )
				);
		if( _size.cx > 0 && _size.cy > 0 )
			return _size;
	} // if( pSite != NULL )
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

int nCount = (int)m_arrLbItems.GetSize();
	if(		(m_dwListBoxStyles & LBS_HASSTRINGS) == 0
		||	nCount == 0
		)
	{
		CSize sizeCalc( 150, 100 );
		if( m_nDropDownWidth > 0 )
			sizeCalc.cx = m_nDropDownWidth;
		if( m_nDropDownHeightMax > 0 )
			sizeCalc.cy = m_nDropDownHeightMax;
//		if( m_bComboField )
//			sizeCalc.cx +=
//				//::GetSystemMetrics( SM_CXSIZEFRAME )
//				+ GetBar()->PmBridge_GetPM()->GetDropDownButtonWidth( this )
//				;
		return sizeCalc;
	}

CWindowDC dc( NULL );
CSize sizeCalc( 0, 0 );
	for( int i = 0; i < nCount; i++ )
	{
		const CExtSafeString & sItem =
			((CExtSafeStringArray*)&m_arrLbItems)->ElementAt( i );
		CExtSafeString sMeasure(
			sItem.IsEmpty() ? _T("AaWwPpQqRrWwZz") : sItem
			);
		CRect rcMeasure =
			CExtPaintManager::stat_CalcTextDimension(
				dc,
				GetBar()->PmBridge_GetPM()->m_FontNormal,
				sMeasure
				);
		if( m_dwListBoxStyles & (LBS_OWNERDRAWVARIABLE|LBS_OWNERDRAWFIXED) )
			rcMeasure.InflateRect( OnPopupListBoxCalcItemExtraSizes() );
		CSize sizeMeasure = rcMeasure.Size();
		if( m_nDropDownWidth == -1 )
			sizeCalc.cx = max( sizeCalc.cx, sizeMeasure.cx );
		sizeCalc.cy += sizeMeasure.cy;
	} // for( int i = 0; i < nCount; i++ )

INT nDD = GetBar()->PmBridge_GetPM()->GetDropDownButtonWidth( (CObject*)this );
	if( m_nDropDownWidth == -1 )
		sizeCalc.cx += ::GetSystemMetrics( SM_CXSIZEFRAME )*2;
	sizeCalc.cy += 2;
	ASSERT( m_nDropDownHeightMax > 0 );
	if( sizeCalc.cy > m_nDropDownHeightMax )
	{
		sizeCalc.cy = m_nDropDownHeightMax;
		if( m_nDropDownWidth == -1 )
			sizeCalc.cx +=
				::GetSystemMetrics( SM_CXSIZEFRAME )
				+ nDD
				;
	} // if( sizeCalc.cy > m_nDropDownHeightMax )
	else if( m_dwListBoxStyles & LBS_DISABLENOSCROLL )
	{
		if( m_nDropDownWidth == -1 )
			sizeCalc.cx +=
				::GetSystemMetrics( SM_CXSIZEFRAME )
				+ nDD
				;
	} // else if( m_dwListBoxStyles & LBS_DISABLENOSCROLL )

	if( m_nDropDownWidth == -2 )
	{
//		CRect rcButton = Rect();
//		CRect rcTextField = OnInplaceControlCalcRect( rcButton );
//		sizeCalc.cx = rcTextField.Width();
		sizeCalc.cx = m_nTextFieldWidth;
		sizeCalc.cx += nDD;
	}
	else if( m_nDropDownWidth > 0 )
		sizeCalc.cx = m_nDropDownWidth;

	return sizeCalc;
}

bool CExtBarTextFieldButton::OnPopupListBoxInitContent(
	CExtPopupInplaceListBox & wndListBox
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if(		pSite != NULL
		&&	pSite->OnPopupListBoxInitContent(
				this,
				GetCmdNode( false ),
				wndListBox
				)
		)
		return true;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	if( (wndListBox.GetStyle() & LBS_HASSTRINGS) != 0 )
	{
		int nCount = (int)m_arrLbItems.GetSize();
		for( int i = 0; i < nCount; i++ )
		{
			const CExtSafeString & sItem = m_arrLbItems.ElementAt( i );
			wndListBox.AddString(
				sItem.IsEmpty() ? _T("") : sItem
				);
		} // for( int i = 0; i < nCount; i++ )
		if(		m_bHelperFindListInitialItem
			&&	wndListBox.GetCount() > 0
			)
		{
			__EXT_MFC_SAFE_LPCTSTR sText = GetFieldText();
			if(		sText != NULL
				&&	_tcslen( sText ) > 0
				)
				wndListBox.SelectString( -1, sText );
		} // if( m_bHelperFindListInitialItem ...
	} // if( (wndListBox.GetStyle() & LBS_HASSTRINGS) != 0 )

	return true;
}

bool CExtBarTextFieldButton::OnPopupListBoxSelChange(
	CExtPopupInplaceListBox & wndListBox
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if(		pSite != NULL
		&&	pSite->OnPopupListBoxSelChange(
				this,
				GetCmdNode( false ),
				wndListBox
				)
		)
		return true;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	if(		(wndListBox.GetStyle() & LBS_HASSTRINGS) != 0
		&&	wndListBox.GetCount() > 0
		)
	{
		int nCurSel = wndListBox.GetCurSel();
		if( nCurSel >= 0 )
		{
			wndListBox.GetText( nCurSel, *((CString*)&m_sTextInplaceBuffer) );
			RedrawButton( true );
		}
	}
	return false;
}

bool CExtBarTextFieldButton::OnPopupListBoxSelEndOK(
	CExtPopupInplaceListBox & wndListBox
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
HWND hWndThis = wndListBox.m_hWnd;
	ASSERT( hWndThis != NULL && ::IsWindow(hWndThis) );
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if(		pSite != NULL
		&&	pSite->OnPopupListBoxSelEndOK(
				this,
				GetCmdNode( false ),
				wndListBox
				)
		)
		return true;
	if( ! ::IsWindow( hWndThis ) )
		return false;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	if(		(wndListBox.GetStyle() & LBS_HASSTRINGS) != 0
		&&	wndListBox.GetCount() > 0
		)
	{
		int nCurSel = wndListBox.GetCurSel();
		if( nCurSel >= 0 )
		{
			wndListBox.GetText( nCurSel, *((CString*)&m_sTextInplaceBuffer) );
			SetFieldText( m_sTextInplaceBuffer );
#if (!defined __EXT_MFC_NO_CUSTOMIZE)
			if( ! ::IsWindow( hWndThis ) )
				return false;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
			RedrawButton( false );
		}
	}
	return false;
}

bool CExtBarTextFieldButton::OnPopupListBoxSelEndCancel(
	CExtPopupInplaceListBox & wndListBox
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	wndListBox;

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if(		pSite != NULL
		&&	pSite->OnPopupListBoxSelEndCancel(
				this,
				GetCmdNode( false ),
				wndListBox
				)
		)
		return true;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	RedrawButton( false );
	return false;
}

bool CExtBarTextFieldButton::OnPopupListBoxClose(
	CExtPopupInplaceListBox & wndListBox
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	wndListBox;

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if(		pSite != NULL
		&&	pSite->OnPopupListBoxClose(
				this,
				GetCmdNode( false ),
				wndListBox
				)
		)
		return true;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	return false;
}

bool CExtBarTextFieldButton::OnPopupListBoxItemDraw(
	CExtPopupInplaceListBox & wndListBox,
	LPDRAWITEMSTRUCT pDIS
	)
{
	ASSERT_VALID( this );
	ASSERT( pDIS != NULL && pDIS->hDC != NULL );

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if(		pSite != NULL
		&&	pSite->OnPopupListBoxItemDraw(
				this,
				GetCmdNode( false ),
				wndListBox,
				pDIS
				)
		)
		return true;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	if( (wndListBox.GetStyle() & LBS_HASSTRINGS) == 0 )
		return false;
	if( ((INT)pDIS->itemID) < 0 || ((INT)pDIS->itemID) >= wndListBox.GetCount() )
		return true;
CDC dc;
	dc.Attach( pDIS->hDC );

	if( pDIS->itemAction & (ODA_DRAWENTIRE | ODA_SELECT) )
	{
		CRect rcErase( pDIS->rcItem );
		if( INT(pDIS->itemID) == (wndListBox.GetCount()-1) )
		{
			CRect rcClient;
			wndListBox.GetClientRect( &rcClient );
			if( rcErase.bottom < rcClient.bottom )
				rcErase.bottom = rcClient.bottom;
		}
		dc.FillSolidRect( 
			rcErase, 
			GetBar()->PmBridge_GetPM()->GetColor( COLOR_WINDOW )
			);
		dc.FillSolidRect(
			&pDIS->rcItem,
			GetBar()->PmBridge_GetPM()->GetColor(
				( pDIS->itemState & ODS_SELECTED )
					? COLOR_HIGHLIGHT
					: COLOR_WINDOW
					,
				this
				)
			);
	}

	if( INT(pDIS->itemID) >= 0 )
	{
		CString sText;
		wndListBox.GetText( (INT)pDIS->itemID, sText );
		if( !sText.IsEmpty() )
		{
			COLORREF clrTextOld =
				dc.SetTextColor(
					GetBar()->PmBridge_GetPM()->GetColor(
						( pDIS->itemState & ODS_SELECTED )
							? COLOR_HIGHLIGHTTEXT
							: COLOR_BTNTEXT
							,
						this
						)
					);
			int nOldBkMode = dc.SetBkMode( TRANSPARENT );
			CRect rcText( pDIS->rcItem );
			rcText.DeflateRect( OnPopupListBoxCalcItemExtraSizes() );
			dc.DrawText(
				sText,
				&rcText,
				DT_SINGLELINE|DT_LEFT|DT_VCENTER|DT_NOPREFIX
				);
			dc.SetBkMode( nOldBkMode );
			dc.SetTextColor( clrTextOld );
		} // if( !sText.IsEmpty() )
	} // if( INT(pDIS->itemID) >= 0 )

	if( pDIS->itemState & ODS_SELECTED )
		dc.DrawFocusRect( &pDIS->rcItem );

	dc.Detach();
	return true;
}

bool CExtBarTextFieldButton::OnPopupListBoxItemMeasure(
	CExtPopupInplaceListBox & wndListBox,
	LPMEASUREITEMSTRUCT pMIS
	)
{
	ASSERT_VALID( this );
	ASSERT( pMIS != NULL );

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if(		pSite != NULL
		&&	pSite->OnPopupListBoxItemMeasure(
				this,
				GetCmdNode( false ),
				wndListBox,
				pMIS
				)
		)
		return true;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	if( (wndListBox.GetStyle() & LBS_HASSTRINGS) == 0 )
		return false;
	pMIS->itemWidth = pMIS->itemHeight = 10;
	if( ((INT)pMIS->itemID) < 0 || ((INT)pMIS->itemID) >= wndListBox.GetCount() )
		return true;
CExtSafeString sMeasure;
	wndListBox.GetText( (INT)pMIS->itemID, *((CString*)&sMeasure) );
	if( sMeasure.IsEmpty() )
		return true;
CWindowDC dc( NULL );
CRect rcMeasure =
		CExtPaintManager::stat_CalcTextDimension(
			dc,
			GetBar()->PmBridge_GetPM()->m_FontNormal,
			sMeasure
			);
	rcMeasure.InflateRect( OnPopupListBoxCalcItemExtraSizes() );
	pMIS->itemWidth = rcMeasure.Width();
	pMIS->itemHeight = rcMeasure.Height();
	return true;
}

CSize CExtBarTextFieldButton::OnPopupListBoxCalcItemExtraSizes() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if( pSite != NULL )
	{
		CSize _size =
			pSite->OnPopupListBoxCalcItemExtraSizes(
				this,
				((CExtBarButton*)this)->GetCmdNode( false )
				);
		if( _size.cx > 0 && _size.cy > 0 ) 
			return _size;
	} // if( pSite != NULL )
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	return CSize( 2, 1 );
}

void CExtBarTextFieldButton::OnHover(
	CPoint point,
	bool bOn,
	bool & bSuspendTips
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	point;
	bSuspendTips = false;

bool bDockSiteCustomizeMode =
		m_pBar->_IsDockSiteCustomizeMode();
	if( bDockSiteCustomizeMode )
		return;
	if(		GetBar()->m_pDockSite == NULL
		||	(! GetBar()->m_pDockSite->m_bHelpMode )
		)
	{
		CExtPopupMenuWnd::ITEMCOVERINGNOTIFICATON _icn(
			NULL,
			this,
			bOn
				? CExtPopupMenuWnd::ITEMCOVERINGNOTIFICATON::__EICN_SET
				: CExtPopupMenuWnd::ITEMCOVERINGNOTIFICATON::__EICN_CANCEL
			);
		HWND hWndOwn = GetBar()->GetSafeHwnd();
		_icn.Notify();
		bSuspendTips = _icn.m_bSuspendTips;
		if( ! ::IsWindow( hWndOwn ) )
			return;
	}

	if( bOn )
	{
		if( CExtToolControlBar::g_bMenuTracking )
		{
//			if( ! OnQueryHoverBasedMenuTracking() )
//				return;
//			OnTrackPopup( point, false, false );
		}
		else
		{
			CExtControlBar::stat_SetMessageString(
				GetCmdTargetWnd(),
				(UINT)(
					(	//	( ! m_pBar->IsKindOf(RUNTIME_CLASS(CExtMenuControlBar)) )
							( ! m_pBar->_IsSimplifiedDropDownButtons() )
						&&	( ! IsKindOf(RUNTIME_CLASS(CExtBarContentExpandButton)) )
						&&	CExtCmdManager::IsCommand( GetCmdID(false) )
					)
						? GetCmdID(true)
						: AFX_IDS_IDLEMESSAGE
					)
				);
		}
		CWnd * pCtrl = CtrlGet();
		if( pCtrl == NULL
			|| (pCtrl->GetStyle() & WS_VISIBLE) == 0
			)
			((CExtMouseCaptureSink *)m_pBar)->SetCapture( m_pBar->GetSafeHwnd() );
	} // if( bOn )
	else
	{
		CExtControlBar::stat_SetMessageString( GetCmdTargetWnd() );
		CExtMouseCaptureSink::ReleaseCapture();
	} // else from if( bOn )
}

void CExtBarTextFieldButton::OnClick(
	CPoint point,
	bool bDown
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );

bool bDockSiteCustomizeMode =
		m_pBar->_IsDockSiteCustomizeMode();
	if( bDockSiteCustomizeMode )
		return;

	if(		m_bVertDocked
		&&	(! GetCtrlVisibleVertically() )
		)
	{
		CExtBarButton::OnClick( point, bDown );
		return;
	}

	if( bDown )
	{
		CExtToolControlBar::_CloseTrackingMenus();

		CExtControlBar::stat_SetMessageString(
			GetCmdTargetWnd(),
			(UINT)GetCmdID(false)
			);

		return;
	} // if( bDown )

	CExtControlBar::stat_SetMessageString( GetCmdTargetWnd() );

CRect rcButton = Rect();
	if( !m_bTextFieldIsNotEditable )
	{
		CRect rcTextField = OnInplaceControlCalcRect( rcButton );
		if( rcTextField.PtInRect(point) )
		{
			OnInplaceControlRun();
			return;
		}
	} // if( !m_bTextFieldIsNotEditable )
	if( !IsComboTextField() )
		return;
bool bTrackComboPopup = m_bTextFieldIsNotEditable;
	if( !bTrackComboPopup )
	{
		CRect rcComboDropRect = OnCalcComboDropRect( rcButton );
		if( rcComboDropRect.PtInRect(point) )
			bTrackComboPopup = true;
	}
	if( bTrackComboPopup )
		OnTrackPopup( point, false, false );
}

void CExtBarTextFieldButton::OnDeliverCmd()
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	CExtBarButton::OnDeliverCmd();
}

__EXT_MFC_INT_PTR CExtBarTextFieldButton::OnToolHitTest(
	CPoint point,
	TOOLINFO * pTI
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	return CExtBarButton::OnToolHitTest( point, pTI );
}

LRESULT CExtBarTextFieldButton::OnHelpHitTest(
	CPoint point
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	return CExtBarButton::OnHelpHitTest( point );
}

void CExtBarTextFieldButton::OnUpdateCmdUI(
	CWnd * pTarget,
	BOOL bDisableIfNoHndler,
	int nIndex
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	CExtBarButton::OnUpdateCmdUI(
		pTarget,
		bDisableIfNoHndler,
		nIndex
		);
}

bool CExtBarTextFieldButton::OnSetCursor( CPoint point )
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	if( m_pBar->_IsDockSiteCustomizeMode() )
		return CExtBarButton::OnSetCursor( point );
	if(		IsDisabled()
		||	m_bTextFieldIsNotEditable
		)
		return false;
	if(		m_bVertDocked
		&&	(! GetCtrlVisibleVertically() )
		)
		return false;
CRect rcButton = Rect();
CRect rcTextField = OnInplaceControlCalcRect( rcButton );
	if( !rcTextField.PtInRect(point) )
		return false;
	::SetCursor( ::LoadCursor(NULL,IDC_IBEAM) );
	return true;
}

void CExtBarTextFieldButton::PaintCompound(
	CDC & dc,
	bool bPaintParentChain,
	bool bPaintChildren,
	bool bPaintOneNearestChildrenLevelOnly
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( GetBar() );

bool bHorz = IsHorzBarOrientation();
	if( ! bHorz )
	{
		CExtBarButton::PaintCompound( dc, false, bPaintChildren, bPaintOneNearestChildrenLevelOnly );
		return;
	}

	if( ! IsPaintAble( dc ) )
		return;
	if( AnimationClient_StatePaint( dc ) )
		return;
	if( bPaintParentChain )
		PaintParentChain( dc );

CRect rcButton = Rect();
CRect rcTextField = OnInplaceControlCalcRect( rcButton );
bool bDockSiteCustomizeMode =
		m_pBar->_IsDockSiteCustomizeMode();
bool bPushed =
		(	(!bDockSiteCustomizeMode)
		&&	(	IsComboPopupDropped()
			||	(		CtrlGet() != NULL
				&&	( ! IsComboTextField() )
				)
			)
		)
			? true : false;
bool bEnabled =
		( IsDisabled() && (!bDockSiteCustomizeMode) )
			? false : true;
bool bHover =
		(	(!bDockSiteCustomizeMode)
		&&	(	IsHover()
			||	CtrlGet() != NULL
			||	IsComboPopupDropped()
			)
		)
			? true : false;
	if(		(! bEnabled )
		||	(	CExtPopupMenuWnd::IsMenuTracking()
			&&	(! IsComboPopupDropped() )
			)
		)
		bPushed = bHover = false;

CExtSafeString sText;
	if( !IsComboPopupDropped() )
		sText = (GetFieldText() == NULL) ? _T("") : GetFieldText();
	else
		sText = m_sTextInplaceBuffer;

CExtToolControlBar * pBar = GetBar();
	if( bEnabled )
	{
		CExtMenuControlBar * pMenuBar = DYNAMIC_DOWNCAST( CExtMenuControlBar, pBar );
		if( pMenuBar != NULL )
		{
			int nFlatTrackingIndex = pMenuBar->_FlatTrackingIndexGet();
			if( nFlatTrackingIndex >= 0 )
			{
				int nOwnIndex = pBar->_GetIndexOf( this );
				if( nOwnIndex == nFlatTrackingIndex )
					bHover = true;
			} // if( nFlatTrackingIndex >= 0 )
		} // if( pMenuBar != NULL )
	} // if( bEnabled )

CExtPaintManager::PAINTTOOLBARTEXTFIELDDATA _ptbtfd(
		this,
		sText,
		rcButton,
		rcTextField,
		IsComboTextField(),
		bHover,
		bPushed,
		bEnabled
		);
	pBar->PmBridge_GetPM()->PaintToolbarTextField( dc, _ptbtfd );

	if( bPaintChildren )
		PaintChildren( dc, bPaintOneNearestChildrenLevelOnly );
}

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
int CExtBarTextFieldButton::GetInitialResizingStateH( // -1 - left side resizing, 1 - right side resizing, 0 - no resizing at specified point (in bar's client coord)
	CPoint point,
	int * p_nWidthMin, // = NULL
	int * p_nWidthMax // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
CRect rcButton = Rect();
	if( !rcButton.PtInRect(point) )
		return 0;
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if( pSite == NULL )
		return 0;
CExtCustomizeCmdTreeNode * pNode = GetCmdNode( false );
	if( pNode == NULL )
		return 0;
	ASSERT_VALID( pNode );
	if( (pNode->GetFlags() & __ECTN_TBB_RESIZABLE) == 0 )
		return 0;
int nDdWidth = GetBar()->PmBridge_GetPM()->GetDropDividerMerics().cx / 2;
	if( nDdWidth < 2 )
		nDdWidth = 2;
int nRetVal = 0;
CRect rcH( rcButton );
	rcH.right = rcH.left + nDdWidth;
	if( rcH.PtInRect(point) )
		nRetVal = -1;
	else
	{
		rcH = rcButton;
		rcH.left = rcH.right - nDdWidth;
		if( rcH.PtInRect(point) )
			nRetVal = 1;
	} // else from if( rcH.PtInRect(point) )
	if( nRetVal != 0 )
		pSite->OnGetCmdItemMinMaxSizeH(
			this,
			pNode,
			p_nWidthMin,
			p_nWidthMax
			);
	return nRetVal;
}
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

#ifndef __EXT_MFC_NO_BUILTIN_DATEFIELD

/////////////////////////////////////////////////////////////////////////////
// CExtBarDateFieldButton

IMPLEMENT_DYNCREATE(CExtBarDateFieldButton, CExtBarTextFieldButton)

CExtBarDateFieldButton::CExtBarDateFieldButton(
	INT nTextFieldWidth, // = __EXT_MENU_DEF_INPLACE_EDIT_WIDTH
	CExtToolControlBar * pBar, // = NULL
	UINT nCmdID, // = ID_SEPARATOR
	UINT nStyle // = 0
	)
	: CExtBarTextFieldButton( true, nTextFieldWidth, pBar, nCmdID, nStyle )
{
}

CExtBarDateFieldButton::~CExtBarDateFieldButton()
{
}

CExtPopupMenuWnd * CExtBarDateFieldButton::OnCreateDropPopup(
	bool bContentExpand // true - inserting to content expand menu, false - tracking dropped popup
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	bContentExpand;

CWnd * pWndCmdTarget = GetCmdTargetWnd();
	ASSERT_VALID( pWndCmdTarget );

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if( pSite != NULL )
	{
		CExtPopupControlMenuWnd * pExtCrtPopup =
			pSite->OnTextFieldCreateDropPopup(
				this,
				GetCmdNode( false ),
				pWndCmdTarget->m_hWnd,
				bContentExpand
				);
		if( pExtCrtPopup != NULL )
			return pExtCrtPopup;
	}
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)
	
	CSize szCalendarDimensions = CSize(1,1);
	DWORD dwDatePickerWindowStyle = WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN;
	DWORD dwDatePickerStyle = __EDPWS_DEFAULT;

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
	if(	pSite != NULL )
		pSite->OnPopupDatePickerGetStyles(
			this,
			GetCmdNode( false ),
			szCalendarDimensions,
			dwDatePickerStyle
		);
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	CExtPopupDatePickerMenuWnd * pPopup = 
		new CExtPopupDatePickerMenuWnd
		(
			m_lParamCookie,
			szCalendarDimensions,
			dwDatePickerWindowStyle,
			dwDatePickerStyle
		);

	pPopup->m_pCbDatePickerSelection = 
		(CExtPopupInplaceDatePicker::pCbDatePickerSelection)
			stat_CbDatePickerSelection;
	pPopup->m_pDatePickerSelectionCookie = (LPVOID)this;
	
	pPopup->m_pCbInitDatePickerContent =
		(CExtPopupInplaceDatePicker::pCbInitDatePickerContent)
			stat_CbInitDatePickerContent;
	pPopup->m_pInitDatePickerCookie = (LPVOID)this;

	VERIFY( pPopup->CreatePopupMenu( pWndCmdTarget->m_hWnd ) );
	return pPopup;
}

bool CExtBarDateFieldButton::stat_CbInitDatePickerContent(
	CExtDatePickerWnd & wndDatePicker,
	CExtBarDateFieldButton * pDateFieldTBB
	)
{
	ASSERT_VALID( pDateFieldTBB );
	ASSERT_KINDOF( CExtBarDateFieldButton, pDateFieldTBB );
	return
		pDateFieldTBB->OnPopupDatePickerInitContent(
			wndDatePicker
			);
}

bool CExtBarDateFieldButton::stat_CbDatePickerSelection(
	LPVOID pSelectionNotification,
	CExtBarDateFieldButton * pDateFieldTBB
	)
{
	ASSERT_VALID( pDateFieldTBB );
	ASSERT_KINDOF( CExtBarDateFieldButton, pDateFieldTBB );
	return
		pDateFieldTBB->OnPopupDatePickerSelChange(
			pSelectionNotification
			);
}

bool CExtBarDateFieldButton::OnPopupDatePickerInitContent(
	CExtDatePickerWnd & wndDatePicker
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	wndDatePicker;

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if(		pSite != NULL
		&&	pSite->OnPopupDatePickerInitContent(
				this,
				GetCmdNode( false ),
				wndDatePicker
				)
		)
		return true;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	return true;
}

bool CExtBarDateFieldButton::OnPopupDatePickerSelChange(
	LPVOID pSelectionNotification
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pBar );
	pSelectionNotification;

#if (!defined __EXT_MFC_NO_CUSTOMIZE)
CExtCustomizeSite * pSite = m_pBar->GetCustomizeSite();
	if(		pSite != NULL
		&&	pSite->OnPopupDatePickerSelChange(
				this,
				GetCmdNode( false ),
				pSelectionNotification
				)
		)
		return true;
#endif // (!defined __EXT_MFC_NO_CUSTOMIZE)

	return false;
}

#endif // __EXT_MFC_NO_BUILTIN_DATEFIELD

#endif // (!defined __EXT_MFC_NO_BUILTIN_TEXTFIELD)

#if (!defined __EXT_MFC_NO_DATE_PICKER_POPUP)

/////////////////////////////////////////////////////////////////////////////
// CExtPopupInplaceDatePicker

IMPLEMENT_DYNCREATE(CExtPopupInplaceDatePicker, CExtDatePickerWnd)

BEGIN_MESSAGE_MAP(CExtPopupInplaceDatePicker, CExtDatePickerWnd)
	//{{AFX_MSG_MAP(CExtPopupInplaceDatePicker)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

UINT CExtPopupInplaceDatePicker::g_nMsgPopupDatePickerInitContent =
	::RegisterWindowMessage(
		_T("CExtPopupInplaceDatePicker::g_nMsgPopupDatePickerInitContent")
		);

CExtPopupInplaceDatePicker::CExtPopupInplaceDatePicker()
	: m_pCbInitDatePickerContent( NULL )
	, m_pInitDatePickerCookie( NULL )
	, m_pCbDatePickerSelection( NULL )
	, m_pDatePickerSelectionCookie( NULL )
	, m_lParamCookie( 0L )
{
}

CExtPopupInplaceDatePicker::~CExtPopupInplaceDatePicker()
{
}

LRESULT CExtPopupInplaceDatePicker::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if( message == WM_CREATE )
	{
		LRESULT lResult = CExtDatePickerWnd::WindowProc(message,wParam,lParam);
		CExtPopupControlMenuWnd * pPopup =
			STATIC_DOWNCAST( CExtPopupControlMenuWnd, GetParent() );
		ASSERT_VALID( pPopup );
		ASSERT( pPopup->m_hWnd != NULL && ::IsWindow(pPopup->m_hWnd) );
		SetFont( & pPopup->PmBridge_GetPM()->m_FontNormal );
		bool bCbInitSucceeded = false;
		if( m_pCbInitDatePickerContent != NULL )
			bCbInitSucceeded =
				m_pCbInitDatePickerContent(
					*this,
					m_pInitDatePickerCookie
					);
		if( !bCbInitSucceeded )
		{
			HWND hWndCmdReceiver = pPopup->GetCmdReceiverHWND();
			ASSERT( hWndCmdReceiver != NULL && ::IsWindow(hWndCmdReceiver) );
			::SendMessage(
				hWndCmdReceiver,
				g_nMsgPopupDatePickerInitContent,
				(WPARAM)this,
				(LPARAM)m_lParamCookie
				);
		} // if( !bCbInitSucceeded )
		return lResult;
	} // if( message == WM_CREATE )

	if( message == WM_MOUSEACTIVATE )
		return MA_NOACTIVATE;

	return CExtDatePickerWnd::WindowProc(message,wParam,lParam);
}

/////////////////////////////////////////////////////////////////////////////
// CExtPopupDatePickerMenuWnd

IMPLEMENT_DYNCREATE(CExtPopupDatePickerMenuWnd, CExtPopupControlMenuWnd)

BEGIN_MESSAGE_MAP(CExtPopupDatePickerMenuWnd, CExtPopupControlMenuWnd)
	//{{AFX_MSG_MAP(CExtPopupDatePickerMenuWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CExtPopupDatePickerMenuWnd::CExtPopupDatePickerMenuWnd(
	LPARAM lParamCookie, // = 0L
	CSize sizeCalendarDimensions, // = CSize(1,1)
	DWORD dwDatePickerWindowStyle, // = WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN
	DWORD dwDatePickerStyle // = __EDPWS_DEFAULT
	)
	: m_lParamCookie( lParamCookie )
	, m_sizeCalendarDimensions( sizeCalendarDimensions )
	, m_dwDatePickerWindowStyle( dwDatePickerWindowStyle )
	, m_dwDatePickerStyle( dwDatePickerStyle )
	, m_pExternalSelectionInfo( NULL )
	, m_pCbDatePickerSelection( NULL )
	, m_pDatePickerSelectionCookie( NULL )
	, m_pCbInitDatePickerContent( NULL )
	, m_pInitDatePickerCookie( NULL )
{
	ASSERT( (m_dwDatePickerWindowStyle&WS_CHILD) != 0 );
	ASSERT( (m_dwDatePickerWindowStyle&WS_POPUP) == 0 );
	ASSERT( (m_dwDatePickerWindowStyle&WS_TABSTOP) == 0 );
	ASSERT( m_sizeCalendarDimensions.cx >= 1 );
	ASSERT( m_sizeCalendarDimensions.cy >= 1 );
CExtDatePickerWnd wndTemp;
	wndTemp.DimSet( 
		m_sizeCalendarDimensions,
		m_sizeCalendarDimensions,
		false
		);
	wndTemp.ModifyDatePickerStyle(
		0xFFFFFFFF,
		m_dwDatePickerStyle,
		false
		);
	m_sizeChildControl =
		wndTemp.OnDatePickerCalcContentSize( m_sizeCalendarDimensions );
}

CExtPopupDatePickerMenuWnd::~CExtPopupDatePickerMenuWnd()
{
}

CRect CExtPopupDatePickerMenuWnd::OnQueryMenuBorderMetrics() const
{
	ASSERT_VALID( this );
	return CRect( 0, 0, 0, 0 );
}

HWND CExtPopupDatePickerMenuWnd::OnCreateChildControl(
	const RECT & rcChildControl
	)
{
	ASSERT_VALID( this );
	ASSERT( (m_dwDatePickerWindowStyle&WS_CHILD) != 0 );
	ASSERT( (m_dwDatePickerWindowStyle&WS_POPUP) == 0 );
	ASSERT( (m_dwDatePickerWindowStyle&WS_TABSTOP) == 0 );
	ASSERT( m_sizeCalendarDimensions.cx >= 1 );
	ASSERT( m_sizeCalendarDimensions.cy >= 1 );
CExtPopupInplaceDatePicker * pWnd = new CExtPopupInplaceDatePicker;
	pWnd->m_pExternalSelectionInfo = m_pExternalSelectionInfo;

	pWnd->m_pCbDatePickerSelection = m_pCbDatePickerSelection;
	pWnd->m_pDatePickerSelectionCookie = m_pDatePickerSelectionCookie;
	pWnd->m_pCbInitDatePickerContent = m_pCbInitDatePickerContent;
	pWnd->m_pInitDatePickerCookie = m_pInitDatePickerCookie;
	pWnd->m_lParamCookie = m_lParamCookie;

	if( ! pWnd->Create(
			this,
			rcChildControl,
			UINT( IDC_STATIC ),
			m_sizeCalendarDimensions,
			m_sizeCalendarDimensions,
			m_dwDatePickerWindowStyle,
			m_dwDatePickerStyle,
			NULL
			)
		)
	{
		ASSERT( FALSE );
		return NULL;
	}
	pWnd->m_bAutoDeleteWindow = true;
bool bRTL = OnQueryLayoutRTL();
	if( bRTL )
		pWnd->ModifyStyleEx( 0, WS_EX_LAYOUTRTL, SWP_FRAMECHANGED );
	return pWnd->m_hWnd;
}

bool CExtPopupDatePickerMenuWnd::_OnMouseWheel(
	WPARAM wParam,
	LPARAM lParam,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );

	if( _IsResizingMode() )
	{
		bNoEat = true;
		return false;
	}

TranslateMouseWheelEventData_t _td( this, wParam, lParam, bNoEat );
	if( _td.Notify() )
	{
		bNoEat = _td.m_bNoEat;
		return true;
	}

	bNoEat = false;
	return true;
}

bool CExtPopupDatePickerMenuWnd::_OnMouseMove(
	UINT nFlags,
	CPoint point,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );

	if( GetSafeHwnd() == NULL )
		return false;

	if( GetSite().GetAnimated() != NULL )
		return true;

	if( _IsResizingMode() )
	{
		bNoEat = true;
		return false;
	}

TranslateMouseMoveEventData_t _td( this, nFlags, point, bNoEat );
	if( _td.Notify() )
	{
		bNoEat = _td.m_bNoEat;
		return true;
	}

CExtPopupMenuSite & _site = GetSite();
	if(	_site.IsShutdownMode()
		|| _site.IsEmpty()
		|| _site.GetAnimated() != NULL
		)
		return true;

CPoint ptScreenClick( point );
	ClientToScreen( &ptScreenClick );
	
HWND hWndFromPoint = ::WindowFromPoint( ptScreenClick );
	if( hWndFromPoint != NULL )
	{
		if( (::GetWindowLong(hWndFromPoint,GWL_STYLE)&WS_CHILD) != 0 )
		{
			if(		(	m_hWndChildControl == hWndFromPoint
					||	::IsChild( m_hWndChildControl, hWndFromPoint )
					)
				)
			{
				bNoEat = true;
				return false;
			}
		} // if( (::GetWindowLong(hWndFromPoint,GWL_STYLE)&WS_CHILD) != 0 )
		else
		{
			CWnd * pWnd = CWnd::FromHandlePermanent( hWndFromPoint );
			if(		pWnd != NULL
				&&	pWnd->IsKindOf( RUNTIME_CLASS(CExtDatePickerHeaderPopupWnd) )
				)
			{
				bNoEat = true;
				return false;
			}
		} // else from if( (::GetWindowLong(hWndFromPoint,GWL_STYLE)&WS_CHILD) != 0 )
	} // if( hWndFromPoint != NULL )

	return
		CExtPopupControlMenuWnd::_OnMouseMove(
			nFlags,
			point,
			bNoEat
			);
}

bool CExtPopupDatePickerMenuWnd::_OnMouseClick(
	UINT nFlags,
	CPoint point,
	bool & bNoEat
	)
{
	ASSERT_VALID( this );

	if( GetSafeHwnd() == NULL )
		return false;

	if( GetSite().GetAnimated() != NULL )
		return true;

CExtPopupMenuSite & _site = GetSite();
	if(	_site.IsShutdownMode()
		|| _site.IsEmpty()
		|| _site.GetAnimated() != NULL
		)
		return true;

TranslateMouseClickEventData_t _td( this, nFlags, point, bNoEat );
	if( _td.Notify() )
	{
		bNoEat = _td.m_bNoEat;
		return true;
	}

bool bLButtonUpCall =
		(nFlags==WM_LBUTTONUP || nFlags==WM_NCLBUTTONUP)
			? true : false;
CPoint ptScreenClick( point );
	ClientToScreen( &ptScreenClick );
	if( (TrackFlagsGet()&TPMX_RIBBON_RESIZING) != 0 )
	{
		UINT nHT = (UINT) ::SendMessage( m_hWnd, WM_NCHITTEST, 0L, MAKELPARAM(ptScreenClick.x,ptScreenClick.y) );
		switch( nHT )
		{
		case HTLEFT:
		case HTRIGHT:
		case HTTOP:
		case HTBOTTOM:
		case HTTOPLEFT:
		case HTTOPRIGHT:
		case HTBOTTOMLEFT:
		case HTBOTTOMRIGHT:
			if( ! bLButtonUpCall )
			{
				m_bHelperResizingMode = true;
				_DoResizing(
					ptScreenClick,
					( (TrackFlagsGet()&TPMX_RIBBON_RESIZING_VERTICAL_ONLY) != 0 ) ? true : false
					);
			} // if( ! bLButtonUpCall )
			else
				m_bHelperResizingMode = false;
			bNoEat = true;
			return false;
		} // switch( nHT )
		m_bHelperResizingMode = false;
	} // if( (TrackFlagsGet()&TPMX_RIBBON_RESIZING) != 0 )

HWND hWndFromPoint = ::WindowFromPoint( ptScreenClick );
	if( hWndFromPoint != NULL )
	{
		if( (::GetWindowLong(hWndFromPoint,GWL_STYLE)&WS_CHILD) != 0 )
		{
			if(		(	m_hWndChildControl == hWndFromPoint
					||	::IsChild( m_hWndChildControl, hWndFromPoint )
					)
				)
			{
				bNoEat = true;
				return false;
			}
		} // if( (::GetWindowLong(hWndFromPoint,GWL_STYLE)&WS_CHILD) != 0 )
		else
		{
			CWnd * pWnd = CWnd::FromHandlePermanent( hWndFromPoint );
			if(		pWnd != NULL
				&&	pWnd->IsKindOf( RUNTIME_CLASS(CExtDatePickerHeaderPopupWnd) )
				)
			{
				bNoEat = true;
				return false;
			}
		} // else from if( (::GetWindowLong(hWndFromPoint,GWL_STYLE)&WS_CHILD) != 0 )
	} // if( hWndFromPoint != NULL )

	return
		CExtPopupControlMenuWnd::_OnMouseClick(
			nFlags,
			point,
			bNoEat
			);
}

LRESULT CExtPopupDatePickerMenuWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if( message == CExtDatePickerWnd::g_nMsgSelectionNotification )
	{
		ASSERT(	m_hWndCmdReceiver != NULL && ::IsWindow( m_hWndCmdReceiver ) );

		if(		m_pCbDatePickerSelection != NULL
			&&	m_pCbDatePickerSelection(
					(LPVOID)wParam,
					m_pDatePickerSelectionCookie
				)
			)
			return TRUE;

		return
			::SendMessage(
				m_hWndCmdReceiver,
				message,
				wParam,
				lParam
				);
	} // if( message == CExtDatePickerWnd::g_nMsgSelectionNotification )
	return CExtPopupControlMenuWnd::WindowProc(message,wParam,lParam);
}

#endif // (!defined __EXT_MFC_NO_DATE_PICKER_POPUP)

#ifndef __EXT_MFC_NO_UNDO_REDO_POPUP

/////////////////////////////////////////////////////////////////////////////
// CExtPopupUndoRedoListBox

IMPLEMENT_DYNCREATE(CExtPopupUndoRedoListBox, CExtPopupInplaceListBox)

BEGIN_MESSAGE_MAP(CExtPopupUndoRedoListBox, CExtPopupInplaceListBox)
	//{{AFX_MSG_MAP(CExtPopupUndoRedoListBox)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

LRESULT CExtPopupUndoRedoListBox::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if( message == WM_ERASEBKGND )
		return (!0);
	
	if( message == WM_PAINT )
	{
		CExtPopupMenuWnd * pPopup =
			STATIC_DOWNCAST( CExtPopupMenuWnd, GetParent() );
		CRect rcClient;
		GetClientRect( &rcClient );
		CPaintDC dcPaint( this );
		CExtMemoryDC dc( &dcPaint, &rcClient );
		dc.FillSolidRect(
			&rcClient,
			pPopup->PmBridge_GetPM()->GetColor( COLOR_WINDOW, this )
			);
		DefWindowProc(
			WM_PAINT,
			WPARAM( dc.GetSafeHdc() ),
			LPARAM(0)
			);
		pPopup->PmBridge_GetPM()->OnPaintSessionComplete( this );
		return 0;
	}
	else if( message == WM_MOUSEMOVE )
	{
		INT nCount = CListBox::GetCount();
		if( nCount == 0 )
			return 0;
		BOOL bOutside = TRUE;
		CPoint point( (DWORD)lParam );
		INT nHitTest = (INT)CListBox::ItemFromPoint( point, bOutside );
		if(		!bOutside 
			&&	(nHitTest + 1) != CListBox::GetSelCount()
			)
			SetSelection( nHitTest );

		if( m_bLButtonPushed ) 
		{
			POINT point;
			if( ::GetCursorPos(&point) )
			{
				ScreenToClient( &point );
				CRect rcClient;
				GetClientRect( &rcClient );
				if( point.y > 0 && point.y < rcClient.bottom )
				{
					StopScrolling();
				}
				else
				{
					if( point.y < 0 )
					{
						UINT nElapse = GetScrollingSpeed( abs(0 - point.y) );
						bool bNeedToResetTimer =
							(m_nLastElapseTimerUp != (UINT)nElapse);
						if( (!m_bScrollingUp) || bNeedToResetTimer )
						{
							if( bNeedToResetTimer )
								KillTimer( __EDPW_SCROLL_UNDO_REDO_UP_TIMER_ID );
							SetTimer( 
								__EDPW_SCROLL_UNDO_REDO_UP_TIMER_ID, 
								nElapse, 
								NULL 
								);
							m_nLastElapseTimerUp = nElapse;
							m_bScrollingUp = true;
						} // if( (!m_bScrollingUp) || bNeedToResetTimer )
					} // if( point.y < 0 )
					if( point.y > 0 )
					{
						UINT nElapse = GetScrollingSpeed( abs( point.y - rcClient.bottom ) );
						bool bNeedToResetTimer = (m_nLastElapseTimerDown != (UINT)nElapse);
						if( (!m_bScrollingDown) || bNeedToResetTimer )
						{
							if( bNeedToResetTimer )
								KillTimer( __EDPW_SCROLL_UNDO_REDO_UP_TIMER_ID );
							SetTimer( 
								__EDPW_SCROLL_UNDO_REDO_DOWN_TIMER_ID, 
								nElapse, 
								NULL 
								);
							m_nLastElapseTimerDown = nElapse;
							m_bScrollingDown = true;
						} // if( (!m_bScrollingDown) || bNeedToResetTimer )
					} // if( point.y > 0 m_bScrollingDown)
				}
			} // if( ::GetCursorPos(&point) )
		} // if( m_bLButtonPushed ) 
		return 0;
	}
	else if(	message == WM_VSCROLL 
			||	(	message == CExtPopupControlMenuWnd::g_nMsgControlInputRetranslate 
				&&	((CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO*)wParam)->m_eRTT == CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO::__ERTT_MOUSE_WHEEL
				)
			)
	{
		UINT nSBCode = (UINT)wParam;
		if( nSBCode == SB_ENDSCROLL )
			CExtPopupInplaceListBox::WindowProc( message, wParam, lParam );

		int nScrollPos1 = GetScrollPos( SB_VERT );
		SetRedraw( FALSE );
		LRESULT lRes = CExtPopupInplaceListBox::WindowProc( message, wParam, lParam );
		int nScrollPos2 = GetScrollPos( SB_VERT );

		INT nScrollDirection = 0; // -1: top, +1: bottom
		switch( nSBCode ) 
		{
		case SB_LINEUP:		// Scroll one line up
		case SB_PAGEUP:		// Scroll one page up
		case SB_TOP:		// Scroll to top
			nScrollDirection = -1;
			break;
		case SB_LINEDOWN:	// Scroll one line down
		case SB_PAGEDOWN:	// Scroll one page down
		case SB_BOTTOM:		// Scroll to bottom
			nScrollDirection = +1;
			break;
		default:
			if( nScrollPos1 != nScrollPos2 )
				nScrollDirection = 
					( nScrollPos1 > nScrollPos2 ) ? -1 : +1;
		}

		INT nCount = CListBox::GetCount();
		if( nScrollDirection == -1 )
		{	// up
			INT nTopItem = CListBox::GetTopIndex();
			if( (nTopItem + 1) != CListBox::GetSelCount() )
				SetSelection( nTopItem );
		}
		else if( nScrollDirection == +1 )
		{	// down
			INT nTopItem = CListBox::GetTopIndex();
			CRect rcClient;
			GetClientRect( &rcClient );
			INT nBottomItem = nTopItem;
			for( ; nBottomItem < nCount - 1; )
			{
				CRect rcItem;
				GetItemRect( nBottomItem, &rcItem );
				if( rcItem.bottom >= rcClient.bottom )
					break;
				nBottomItem++;
			}
			if( (nBottomItem + 1) != CListBox::GetSelCount() )
				SetSelection( nBottomItem );
		}
	
		SetRedraw( TRUE );
		return lRes;
	}
	else if( message == CExtPopupControlMenuWnd::g_nMsgControlInputRetranslate )
	{
		ASSERT( wParam != 0 );
		CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO & _ciri =
			*((CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO*)wParam);
		if(		_ciri.m_eRTT == CExtPopupControlMenuWnd::CONTROLINPUTRETRANSLATEINFO::__ERTT_KEYBOARD 
			&&	(	_ciri.m_nChar == VK_UP
				||	_ciri.m_nChar == VK_DOWN
				||	_ciri.m_nChar == VK_HOME
				||	_ciri.m_nChar == VK_END
				||	_ciri.m_nChar == VK_PRIOR
				||	_ciri.m_nChar == VK_NEXT
				)
			)
		{
			INT nCount = CListBox::GetCount();
			INT nSelCount = CListBox::GetSelCount();
			INT nTopItem = CListBox::GetTopIndex();
			CRect rcClient;
			GetClientRect( &rcClient );
			INT nBottomItem = nTopItem;
			for( ; nBottomItem < nCount - 1; )
			{
				CRect rcItem;
				GetItemRect( nBottomItem, &rcItem );
				if( rcItem.bottom >= rcClient.bottom )
					break;
				nBottomItem++;
			}

			if( _ciri.m_nChar == VK_UP )
			{
				INT nSelItem = (nSelCount - 1) - 1;
				if( nSelItem >= 0 )
				{
					SetSelection( nSelItem );
					if( nSelItem < nTopItem )
						CListBox::SetTopIndex( nSelItem );
				}
			}
			else if( _ciri.m_nChar == VK_DOWN )
			{
				INT nSelItem = (nSelCount - 1) + 1;
				if( nSelItem < nCount )
				{
					SetSelection( nSelItem );
					if( nSelItem > nBottomItem )
						CListBox::SetTopIndex( nTopItem + nSelItem - nBottomItem );
				}
			}
			else if( _ciri.m_nChar == VK_HOME )
			{
				SetSelection( 0 );
				CListBox::SetTopIndex( 0 );
			}
			else if( _ciri.m_nChar == VK_END )
			{
				if( nSelCount < nCount )
				{
					SetSelection( nCount - 1 );
					CListBox::SetTopIndex( nCount - 1 );
				}
			}
			else if( _ciri.m_nChar == VK_PRIOR )
			{
				if( nSelCount > 1 )
				{
					int nPageSize = nBottomItem - nTopItem + 1;
					INT nSelItem = (nSelCount - 1) - (nPageSize - 1);
					if( nSelItem < 0 )
						nSelItem = 0;
					SetSelection( nSelItem );
					if( nSelItem < nTopItem )
						CListBox::SetTopIndex( nSelItem );
				}
			}
			else if( _ciri.m_nChar == VK_NEXT )
			{
				if( nSelCount < nCount )
				{
					int nPageSize = nBottomItem - nTopItem + 1;
					INT nSelItem = (nSelCount - 1) + (nPageSize - 1);
					if( nSelItem > nCount - 1 )
						nSelItem = nCount - 1;
					SetSelection( nSelItem );
					if( nSelItem > nBottomItem )
						CListBox::SetTopIndex( nSelItem - nPageSize + 1 );
				}
			}
			return 0;
		}
	}
	else if( message == WM_LBUTTONDOWN )
	{
		INT nCount = CListBox::GetCount();
		if( nCount != 0 )
		{
			SetCapture();
			m_bLButtonPushed = true;
		}
		return 0;
	} 
	else if( message == WM_LBUTTONUP )
	{
		StopScrolling();
		if( GetCapture() == this )
			ReleaseCapture();
		INT nCount = CListBox::GetCount();
		if( nCount != 0 )
		{
			if( m_bLButtonPushed )
				_DoSelEndOK();
			m_bLButtonPushed = false;
		}
		return 0;
	} // if( message == WM_LBUTTONUP )
	else if( message == WM_TIMER )
	{
		UINT nIDEvent = (UINT)wParam;
		switch( nIDEvent ) 
		{
		case __EDPW_SCROLL_UNDO_REDO_UP_TIMER_ID:
			{
				INT nTopItem = CListBox::GetTopIndex();
				nTopItem -= 1;
				if(	nTopItem >= 0 )
				{
					SetSelection( nTopItem );
					CListBox::SetTopIndex( nTopItem );
				}
			}
			return 0;
		case __EDPW_SCROLL_UNDO_REDO_DOWN_TIMER_ID:
			{
				INT nTopItem = CListBox::GetTopIndex();
				CRect rcClient;
				GetClientRect( &rcClient );
				INT nBottomItem = nTopItem;
				INT nCount = CListBox::GetCount();
				for( ; nBottomItem < nCount - 1; )
				{
					CRect rcItem;
					GetItemRect( nBottomItem, &rcItem );
					if( rcItem.bottom >= rcClient.bottom )
						break;
					nBottomItem++;
				}
				nBottomItem += 1;
				if(	nBottomItem < nCount )
				{
					SetSelection( nBottomItem );
					CListBox::SetTopIndex( nTopItem + 1 );
				}			
			}
			return 0;
		default:
			return CExtPopupInplaceListBox::WindowProc(message,wParam,lParam);
		}
	} // if( message == WM_TIMER )
	else if( message == WM_CANCELMODE )
	{
		StopScrolling();
		if( GetCapture() == this )
			ReleaseCapture();
		_DoSelEndCancel();
		return 0;
	} // if( message == WM_CANCELMODE )
	else if( message == WM_DESTROY )
	{
		StopScrolling();
		_DoSelEndCancel( true );
		return CExtPopupInplaceListBox::WindowProc(message,wParam,lParam);
	} // if( message == WM_DESTROY )


	return CExtPopupInplaceListBox::WindowProc( message, wParam, lParam );
}

void CExtPopupUndoRedoListBox::SetSelection( INT nSelCount )
{
	INT nCount = CListBox::GetCount();
	CListBox::SelItemRange( FALSE, 0, nCount - 1 );
	if( nSelCount == 0 )
		CListBox::SetSel( nSelCount, TRUE );
	else
		CListBox::SelItemRange( TRUE, 0, nSelCount );
	_DoSelChange();
}

void CExtPopupUndoRedoListBox::StopScrolling()
{
 	if( m_bScrollingDown )
	{
		if( ::IsWindow(m_hWnd) )
			::KillTimer( m_hWnd, __EDPW_SCROLL_UNDO_REDO_DOWN_TIMER_ID );
		m_bScrollingDown = false;
	}
	if( m_bScrollingUp )
	{
		if( ::IsWindow(m_hWnd) )
			::KillTimer( m_hWnd, __EDPW_SCROLL_UNDO_REDO_UP_TIMER_ID );
		m_bScrollingUp = false;
	}
}

UINT CExtPopupUndoRedoListBox::GetScrollingSpeed( int nPixels )
{
	ASSERT_VALID( this );
double dAcceleration = floor( double(nPixels)/double(__EDPW_SCROLL_UNDO_REDO_ACCELERATION_STEP) );
INT nAcceleration = (INT)dAcceleration;
UINT nElapse = 0;
	switch( nAcceleration ) 
	{
	case 0:  nElapse = __EDPW_SCROLL_UNDO_REDO_TIMER_ELAPSE1; break;
	case 1:  nElapse = __EDPW_SCROLL_UNDO_REDO_TIMER_ELAPSE2; break;
	case 2:  nElapse = __EDPW_SCROLL_UNDO_REDO_TIMER_ELAPSE3; break;
	case 3:  nElapse = __EDPW_SCROLL_UNDO_REDO_TIMER_ELAPSE4; break;
	case 4:  nElapse = __EDPW_SCROLL_UNDO_REDO_TIMER_ELAPSE5; break;
	default: nElapse = __EDPW_SCROLL_UNDO_REDO_TIMER_ELAPSE5; break;
	}
	return nElapse;
}

void CExtPopupUndoRedoListBox::_DoSelChange()
{
	ASSERT_VALID( this );
HWND hWndThis = m_hWnd;
	ASSERT( hWndThis != NULL && ::IsWindow(hWndThis) );
	if(		m_pCbListBoxSelection != NULL
		&&	m_pCbListBoxSelection(
				*this,
				m_pSelectionCookie,
				__SAT_SELCHANGE
				)
		)
		return;
	if( ! ::IsWindow( hWndThis ) )
		return;

CExtPopupUndoRedoMenuWnd * pPopup =
		STATIC_DOWNCAST( CExtPopupUndoRedoMenuWnd, GetParent() );
	ASSERT_VALID( pPopup );
	ASSERT( pPopup->m_hWnd != NULL && ::IsWindow(pPopup->m_hWnd) );
	pPopup->SetActionsCount( CListBox::GetSelCount() );

HWND hWndCmdReceiver = pPopup->GetCmdReceiverHWND();
	ASSERT( hWndCmdReceiver != NULL && ::IsWindow(hWndCmdReceiver) );
POPUPLISTBOXITEMSELENDINFO _plbsei( this );
	::SendMessage(
		hWndCmdReceiver,
		g_nMsgPopupListBoxSelChange,
		(WPARAM)(&_plbsei),
		(LPARAM)m_lParamCookie
		);
}

/////////////////////////////////////////////////////////////////////////////
// CExtPopupUndoRedoMenuWnd

IMPLEMENT_DYNCREATE(CExtPopupUndoRedoMenuWnd, CExtPopupControlMenuWnd)

BEGIN_MESSAGE_MAP(CExtPopupUndoRedoMenuWnd, CExtPopupControlMenuWnd)
	//{{AFX_MSG_MAP(CExtPopupUndoRedoMenuWnd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CExtPopupUndoRedoMenuWnd::CExtPopupUndoRedoMenuWnd(
	LPARAM lParamCookie, // = 0L
	DWORD dwListBoxStyles // = WS_CHILD|WS_VISIBLE|WS_VSCROLL|LBS_MULTIPLESEL|LBS_HASSTRINGS|LBS_OWNERDRAWVARIABLE
	)
	: m_lParamCookie( lParamCookie )
	, m_dwListBoxStyles( dwListBoxStyles )
	, m_pCbFormatCaption( NULL )
	, m_pFormatCaptionCookie( NULL )
	, m_pCbInitListBoxContent( NULL )
	, m_pInitListBoxCookie( NULL )
	, m_pCbListBoxSelection( NULL )
	, m_pSelectionCookie( NULL )
	, m_pCbListBoxItemDraw( NULL )
	, m_pCbListBoxItemMeasure( NULL )
	, m_pListBoxItemCookie( NULL )
	, m_nActionsCount( 0 )
{
}

CExtPopupUndoRedoMenuWnd::~CExtPopupUndoRedoMenuWnd()
{
}

HWND CExtPopupUndoRedoMenuWnd::OnCreateChildControl(
	const RECT & rcChildControl
	)
{
	ASSERT_VALID( this );
CExtPopupUndoRedoListBox * pListBox = new CExtPopupUndoRedoListBox;
	pListBox->m_pCbListBoxSelection = m_pCbListBoxSelection;
	pListBox->m_pSelectionCookie = m_pSelectionCookie;
	pListBox->m_pCbInitListBoxContent = m_pCbInitListBoxContent;
	pListBox->m_pInitListBoxCookie = m_pInitListBoxCookie;
	pListBox->m_lParamCookie = m_lParamCookie;
	pListBox->m_pCbListBoxItemDraw = m_pCbListBoxItemDraw;
	pListBox->m_pCbListBoxItemMeasure = m_pCbListBoxItemMeasure;
	pListBox->m_pListBoxItemCookie = m_pListBoxItemCookie;
	if(	! pListBox->Create(
			m_dwListBoxStyles,
			rcChildControl,
				this,
			(UINT)(IDC_STATIC)
				)
		)
	{
		ASSERT( FALSE );
		delete pListBox;
		return NULL;
	}
	SetActionsCount( pListBox->GetSelCount() );
bool bRTL = OnQueryLayoutRTL();
	if( bRTL )
		pListBox->ModifyStyleEx( 0, WS_EX_LAYOUTRTL, SWP_FRAMECHANGED );
	return pListBox->m_hWnd;
}

int CExtPopupUndoRedoMenuWnd::OnQueryTextAreaHeight() const
{
	return 24;
}

CRect CExtPopupUndoRedoMenuWnd::_RecalcControlRect()
{
	ASSERT_VALID( this );
CRect rcControl = CExtPopupControlMenuWnd::_RecalcControlRect();
	rcControl.OffsetRect( 1, 1 );
	return rcControl;
}

CSize CExtPopupUndoRedoMenuWnd::_CalcTrackSize()
{
	ASSERT_VALID( this );
	CSize _size = CExtPopupControlMenuWnd::_CalcTrackSize();
	_size.cy += OnQueryTextAreaHeight();
	_size.cy += 2;
	_size.cx += 2;
	return _size;
}

void CExtPopupUndoRedoMenuWnd::_RecalcLayoutImpl()
{
	ASSERT_VALID( this );
	CExtPopupMenuWnd::_RecalcLayoutImpl();

	CRect rcControl = _RecalcControlRect();
	if( rcControl == m_rcChildControl )
		return;
	m_rcChildControl = rcControl;
	m_rcTextArea.SetRect(
		m_rcChildControl.left,
		m_rcChildControl.bottom,
		m_rcChildControl.right,
		m_rcChildControl.bottom + OnQueryTextAreaHeight()
		);

	if( m_hWndChildControl == NULL )
		return;

	::SetWindowPos(
		m_hWndChildControl, NULL,
		m_rcChildControl.left, m_rcChildControl.top,
		m_rcChildControl.Width(), m_rcChildControl.Height(),
		SWP_NOZORDER|SWP_NOOWNERZORDER|SWP_NOACTIVATE
		);
	if( IsWindowVisible() )
	{
		CClientDC dc(this);
		_DoPaint(dc);
	}
}

void CExtPopupUndoRedoMenuWnd::OnPaintUndoRedoCaption(
	CDC & dc,
	CRect rcItem
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&dc) );
	ASSERT( dc.GetSafeHdc() != NULL );
	
	CExtSafeString sCaption;
	if(	!(	m_pCbFormatCaption != NULL
		&&	m_pCbFormatCaption(
				sCaption,
				this,
				m_pFormatCaptionCookie
				)
			)
		)
	{
		sCaption.Format( 
			_T("Undo %d Actions"), 
			GetActionsCount() 
			);
	}

	PmBridge_GetPM()->PaintUndoRedoCaption( 
		dc, 
		rcItem, 
		sCaption, 
		this 
		);
}

void CExtPopupUndoRedoMenuWnd::_DoPaint( CDC & dcPaint, bool bUseBackBuffer /*= true*/ )
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&dcPaint) );
	ASSERT( dcPaint.GetSafeHdc() != NULL );
	CExtPopupControlMenuWnd::_DoPaint( dcPaint, bUseBackBuffer );

	OnPaintUndoRedoCaption( 
		dcPaint, 
		m_rcTextArea
		);
}

void CExtPopupUndoRedoMenuWnd::SetActionsCount( INT nActionsCount )
{
	ASSERT_VALID( this );
	m_nActionsCount = nActionsCount;
	CClientDC dc(this);
	OnPaintUndoRedoCaption( 
		dc, 
		m_rcTextArea
		);
}

INT CExtPopupUndoRedoMenuWnd::GetActionsCount() const
{
	ASSERT_VALID( this );
	return m_nActionsCount;
}

#endif // __EXT_MFC_NO_UNDO_REDO_POPUP
