// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "StdAfx.h"

#if (!defined __EXT_MFC_NO_SPLITTER_WND)

#if (!defined __EXT_SPLITTER_WND_H)
	#include <ExtSplitterWnd.h>
#endif

#if (!defined __EXT_SCROLLWND_H)
	#include <ExtScrollWnd.h>
#endif 

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if _MFC_VER < 0x700
	#include <../src/AfxImpl.h>
#else
	#include <../src/mfc/AfxImpl.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE( CExtSplitterWnd, CSplitterWnd );
IMPLEMENT_CExtPmBridge_MEMBERS( CExtSplitterWnd );

CExtSplitterWnd::CExtSplitterWnd()
{
	PmBridge_Install();
}

CExtSplitterWnd::~CExtSplitterWnd()
{
	PmBridge_Uninstall();
}

BEGIN_MESSAGE_MAP(CExtSplitterWnd, CSplitterWnd)
	//{{AFX_MSG_MAP(CExtSplitterWnd)
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CExtSplitterWnd::DrawAllSplitBars(
	CDC * pDC,
	int cxInside,
	int cyInside
	)
{
	if( pDC == NULL )
	{
		RedrawWindow( NULL, NULL, RDW_INVALIDATE|RDW_NOCHILDREN );
		return;
	}
	ASSERT_VALID( pDC );
	if( (GetStyle()&WS_CLIPCHILDREN) == 0 )
		ModifyStyle( 0, WS_CLIPCHILDREN );
	if( (GetStyle()&WS_CLIPSIBLINGS) == 0 )
		ModifyStyle( 0, WS_CLIPSIBLINGS );
CRect rcClient;
	GetClientRect( &rcClient );
CFrameWnd * pInnerFrame = GetParentFrame();
	ASSERT_VALID( pInnerFrame );
CFrameWnd * pMainFrame = pInnerFrame->GetParentFrame();
	if( pMainFrame == NULL )
		pMainFrame = pInnerFrame;
#if (!defined __EXT_MFC_NO_TAB_ONENOTE_CTRL)
bool bDrawDefault = true;
CExtTabMdiOneNoteWnd * pOneNoteTabs = NULL;
CWnd * pWnd = pMainFrame->GetWindow( GW_CHILD );
	for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
	{
		pOneNoteTabs =
			DYNAMIC_DOWNCAST(
				CExtTabMdiOneNoteWnd,
				pWnd
				);
		if( pOneNoteTabs != NULL )
			break;
	} // for( ; pWnd != NULL; pWnd = pWnd->GetWindow( GW_HWNDNEXT ) )
	if( pOneNoteTabs != NULL )
	{
		bDrawDefault = false;
		LONG nIndex = pOneNoteTabs->ItemFindByHWND( pInnerFrame->m_hWnd );
		// ASSERT( nIndex >= 0 );
		if( nIndex >= 0 )
		{
			CExtTabOneNoteWnd::TAB_ITEM_INFO_ONENOTE * pTII =
				pOneNoteTabs->ItemGet( nIndex );
			COLORREF clrFill = pTII->GetColorBkDark();
			pDC->FillSolidRect( rcClient, clrFill );
		}
	} // if( pOneNoteTabs != NULL )
	if( bDrawDefault )
#endif
	{ // block
		CExtPaintManager * pPM = PmBridge_GetPM();
		ASSERT_VALID( pPM );
		CRect rcWnd;
		pMainFrame->GetWindowRect( &rcWnd );
		ScreenToClient( &rcWnd );
		if( pPM->m_clrForceSplitterBk != COLORREF(-1L) )
		{
			pDC->FillSolidRect( rcClient, pPM->m_clrForceSplitterBk );
			return;
		}
		else if(	(! pPM->GetCb2DbTransparentMode(this) )
				||	(! pPM->PaintDockerBkgnd(
						true,
						*pDC,
						rcClient,
						rcWnd
						)
					)
			)
		{
			COLORREF clrFill =
				pPM->GetColor( CExtPaintManager::CLR_3DFACE_OUT );
			pDC->FillSolidRect( rcClient, clrFill );
		}
// 		else
// 			return;
	} // block
	CSplitterWnd::DrawAllSplitBars(
		pDC,
		cxInside,
		cyInside
		);
}

void CExtSplitterWnd::OnDrawSplitter(
	CDC* pDC,
	ESplitType nType,
	const CRect & rectArg
	)
{
	if( pDC == NULL )
	{
		RedrawWindow( rectArg, NULL, RDW_INVALIDATE|RDW_NOCHILDREN );
		return;
	}
	ASSERT_VALID( pDC );
CRect rect = rectArg;
	switch( nType )
	{
	case splitBorder:
		{
			CExtPaintManager * pPM = PmBridge_GetPM();
			ASSERT_VALID( pPM );
			if( pPM->m_clrForceSplitterBk != COLORREF(-1L) )
				pDC->FillSolidRect( rect, pPM->m_clrForceSplitterBk );

			rect.InflateRect( -CX_BORDER, -CY_BORDER );
			PmBridge_GetPM()->PaintResizableBarChildNcAreaRect(
				*pDC,
				rect,
				this
				);
		}
		return;
	case splitIntersection:
	case splitBox:
	case splitBar:
		{
			CExtPaintManager * pPM = PmBridge_GetPM();
			ASSERT_VALID( pPM );
			if( pPM->m_clrForceSplitterBk != COLORREF(-1L) )
				pDC->FillSolidRect( rect, pPM->m_clrForceSplitterBk );
		}
		return;
	default:
			ASSERT(FALSE);  // unknown splitter type
		break;
	} // switch( nType )
}

void CExtSplitterWnd::OnInvertTracker(
	const CRect & rect
	)
{
	ASSERT_VALID( this );
	ASSERT( ! rect.IsRectEmpty() );
CRect rc = rect;
CWnd * pWndParent = GetParent();
	ClientToScreen( &rc );
	pWndParent->ScreenToClient( &rc );
//CDC * pDC = pWndParent->GetDC();
CDC * pDC = pWndParent->GetDCEx( NULL, DCX_CACHE | DCX_LOCKWINDOWUPDATE | DCX_CLIPSIBLINGS );
CBrush * pBrush = CDC::GetHalftoneBrush();
HBRUSH hOldBrush = NULL;
	if( pBrush != NULL )
		hOldBrush = (HBRUSH)SelectObject( pDC->m_hDC, pBrush->m_hObject );
	pDC->PatBlt( rc.left, rc.top, rc.Width(), rc.Height(), PATINVERT );
	if( hOldBrush != NULL )
		SelectObject( pDC->m_hDC, hOldBrush );
	ReleaseDC( pDC );
}

BOOL CExtSplitterWnd::CreateScrollBarCtrl( DWORD dwStyle, UINT nID )
{
	ASSERT_VALID( this );
	ASSERT( m_hWnd != NULL );
	if( (dwStyle&SBS_SIZEBOX) != 0 )
		return CSplitterWnd::CreateScrollBarCtrl( dwStyle, nID );
CExtScrollBar * pExtScrollBar = new CExtScrollBar;
	pExtScrollBar->m_bAutoDeleteOnPostNcDestroy = true;
	pExtScrollBar->m_bReflectParentSizing = false;
pExtScrollBar->m_bHelperLightAccent = false;
	if( ! pExtScrollBar->Create(
			dwStyle|WS_VISIBLE|WS_CHILD,
			CRect(0,0,1,1),
			this,
			nID
			)
		)
		return FALSE;
HWND hWnd = pExtScrollBar->GetSafeHwnd();
//HWND hWnd =
//		::CreateWindow(
//			_T("SCROLLBAR"), NULL,
//			dwStyle|WS_VISIBLE|WS_CHILD,
//			0,
//			0,
//			1,
//			1,
//			m_hWnd, 
//			(HMENU)nID,
//			AfxGetInstanceHandle(),
//			NULL
//			);
#ifdef _DEBUG
	if( hWnd == NULL )
		TRACE1(
			"Warning: Window creation failed: GetLastError returns 0x%8.8X\n",
			GetLastError()
			);
#endif
	return ( hWnd != NULL ) ? TRUE : FALSE;
}

void CExtSplitterWnd::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	ASSERT( pScrollBar != NULL );
	if( ! pScrollBar->IsKindOf( RUNTIME_CLASS(CExtScrollBar) ) )
	{
		CSplitterWnd::OnHScroll( nSBCode, nPos, pScrollBar );
		return;
	}
int col = _AfxGetDlgCtrlID( pScrollBar->m_hWnd ) - AFX_IDW_HSCROLL_FIRST;
	ASSERT( col >= 0 && col < m_nMaxCols );
	ASSERT( m_nRows > 0 );
int nOldPos = pScrollBar->GetScrollPos();
#ifdef _DEBUG
int nNewPos;
#endif
	for( int row = 0; row < m_nRows; row++ )
	{
		GetPane( row, col ) ->
			SendMessage(
				WM_HSCROLL,
				MAKELONG( nSBCode, nPos ),
				(LPARAM)pScrollBar->m_hWnd
				);
#ifdef _DEBUG
		if( row == 0 )
		{
			nNewPos = pScrollBar->GetScrollPos();
			if( pScrollBar->GetScrollPos() != nNewPos )
			{
				TRACE0( "Warning: scroll panes setting different scroll positions.\n" );
			} // if( pScrollBar->GetScrollPos() != nNewPos )
		} // if( row == 0 )
#endif //_DEBUG
		if( row < ( m_nRows - 1 ) )
			pScrollBar->SetScrollPos( nOldPos, FALSE );
			//((CExtScrollBar*)pScrollBar)->_SetScrollPos( nOldPos, false, false );
	} // for( int row = 0; row < m_nRows; row++ )
}

void CExtSplitterWnd::OnVScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	ASSERT( pScrollBar != NULL );
	if( ! pScrollBar->IsKindOf( RUNTIME_CLASS(CExtScrollBar) ) )
	{
		CSplitterWnd::OnHScroll( nSBCode, nPos, pScrollBar );
		return;
	}
int row = _AfxGetDlgCtrlID( pScrollBar->m_hWnd ) - AFX_IDW_VSCROLL_FIRST;
	ASSERT( row >= 0 && row < m_nMaxRows );
	ASSERT( m_nCols > 0 );
int nOldPos = pScrollBar->GetScrollPos();
#ifdef _DEBUG
int nNewPos;
#endif
	for( int col = 0; col < m_nCols; col++ )
	{
		GetPane( row, col ) ->
			SendMessage(
				WM_VSCROLL,
				MAKELONG( nSBCode, nPos ),
				(LPARAM)pScrollBar->m_hWnd
				);
#ifdef _DEBUG
		if( col == 0 )
		{
			nNewPos = pScrollBar->GetScrollPos();
			if( pScrollBar->GetScrollPos() != nNewPos )
			{
				TRACE0("Warning: scroll panes setting different scroll positions.\n");
			} // if( pScrollBar->GetScrollPos() != nNewPos )
		} // if( col == 0 )
#endif //_DEBUG
		if( col < ( m_nCols - 1 ) )
			pScrollBar->SetScrollPos( nOldPos, FALSE );
			//((CExtScrollBar*)pScrollBar)->_SetScrollPos( nOldPos, false, false );
	} // for( int col = 0; col < m_nCols; col++ )
}

#endif // (!defined __EXT_MFC_NO_SPLITTER_WND)


