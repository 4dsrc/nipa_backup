// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#if (! defined __EXT_TAB_FLAT_WND_H)
#define __EXT_TAB_FLAT_WND_H

#if (!defined __EXT_MFC_NO_TABFLAT_CTRL)

#if (!defined __EXT_MFC_DEF_H)
	#include <ExtMfcDef.h>
#endif // __EXT_MFC_DEF_H

#if (!defined __EXT_HOOK_H)
	#include "ExtHook.h"
#endif

#if (!defined __EXT_MOUSECAPTURESINK_H)
	#include <ExtMouseCaptureSink.h>
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExtTabFlatWnd.h : header file
//

#ifndef __AFXTEMPL_H__
	#include <AfxTempl.h>
#endif

#if (!defined __EXT_CMD_ICON_H)
	#include <ExtCmdIcon.h>
#endif

#if (!defined __EXT_TABWND_H)
	#include <ExtTabWnd.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtTabFlatWnd window

class __PROF_UIS_API CExtTabFlatWnd : public CExtTabWnd
{
// Construction
public:
	DECLARE_DYNCREATE( CExtTabFlatWnd );
	CExtTabFlatWnd();
	virtual bool _IsCustomLayoutTabWnd() const;

// Attributes
public:

	bool ItemsHasInclineGet( bool bBefore ) const
	{
		ASSERT_VALID( this );
		DWORD dwTabWndStyle = GetTabWndStyle();
		DWORD dwTestMask = bBefore
			? __ETWS_FT_NO_ITEMS_INCLINE_BEFORE
			: __ETWS_FT_NO_ITEMS_INCLINE_AFTER
			;

		bool bRetVal = ( (dwTabWndStyle&dwTestMask) != 0 ) ? false : true;
		return bRetVal;
	}
	bool ItemsHasInclineBeforeGet() const
	{
		ASSERT_VALID( this );
		return ItemsHasInclineGet( true );
	}
	bool ItemsHasInclineAfterGet() const
	{
		ASSERT_VALID( this );
		return ItemsHasInclineGet( false );
	}
	void ItemsHasInclineSet( bool bBefore, bool bSet = true )
	{
		ASSERT_VALID( this );
		DWORD dwAddRemoveMask = bBefore
			? __ETWS_FT_NO_ITEMS_INCLINE_BEFORE
			: __ETWS_FT_NO_ITEMS_INCLINE_AFTER
			;
		ModifyTabWndStyle(
			bSet ? dwAddRemoveMask : 0,
			bSet ? 0 : dwAddRemoveMask,
			(GetSafeHwnd() != NULL) ? true : false
			);
	}
	void ItemsHasInclineBeforeSet( bool bSet = true )
	{
		ASSERT_VALID( this );
		ItemsHasInclineSet( true, bSet );
	}
	void ItemsHasInclineAfterSet( bool bSet = true )
	{
		ASSERT_VALID( this );
		ItemsHasInclineSet( false, bSet );
	}

	// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExtTabFlatWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CExtTabFlatWnd();

protected:
	virtual int _CalcRgnShift( bool bHorz, const CRect & rc )
	{
		int nShift = ::MulDiv( bHorz ? rc.Height() : rc.Width(), 1, 4);
		return nShift;
	}
	
	void OnTabWndDrawItem(
		CDC & dc,
		CRect & rcTabItemsArea,
		LONG nItemIndex,
		TAB_ITEM_INFO * pTii,
		bool bTopLeft,
		bool bHorz,
		bool bSelected,
		bool bCenteredText,
		bool bGroupedMode,
		bool bInGroupActive,
		bool bInvertedVerticalMode,
		const CRect & rcEntireItem,
		CSize sizeTextMeasured,
		CFont * pFont,
		__EXT_MFC_SAFE_LPCTSTR sText,
		CExtCmdIcon * pIcon
		);
	
	void OnTabWndMeasureItemAreaMargins(
		LONG & nSpaceBefore,
		LONG & nSpaceAfter,
		LONG & nSpaceOver
		);

	virtual void OnTabWndUpdateItemMeasure(
		TAB_ITEM_INFO * pTii,
		CDC & dcMeasure,
		CSize & sizePreCalc
		);
	
	virtual void OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		);
	virtual void OnTabWndDrawEntire(
		CDC & dc,
		CRect & rcClient
		);

	virtual void OnTabWndDrawButton(
		CDC & dc,
		CRect & rcButton,
		LONG nHitTest,
		bool bTopLeft,
		bool bHorz,
		bool bEnabled,
		bool bHover,
		bool bPushed,
		bool bGroupedMode
		);
	
	virtual CSize OnTabWndCalcButtonSize(
		CDC & dcMeasure,
		LONG nTabAreaMetric // vertical max width or horizontal max heights of all tabs
		);
	
public:
	virtual void OnFlatTabWndGetItemColors(
		LONG nItemIndex,
		bool bSelected,
		bool bHover,
		bool bEnabled,
		COLORREF &clrLight,
		COLORREF &clrShadow,
		COLORREF &clrDkShadow, 
		COLORREF &clrTabBk, 
		COLORREF &clrText
		)
	{
		ASSERT_VALID( this );
		nItemIndex;
		bSelected;
		bHover;
		clrLight = 
			PmBridge_GetPM()->GetColor( COLOR_3DHIGHLIGHT, this );
		clrShadow =  
			PmBridge_GetPM()->GetColor( COLOR_3DSHADOW, this );
		clrDkShadow = 
			PmBridge_GetPM()->GetColor( COLOR_3DDKSHADOW, this );
		clrTabBk = 
			bSelected
				? RGB(255,255,255)
				: PmBridge_GetPM()->GetColor( CExtPaintManager::CLR_3DFACE_OUT, this )
				;
		clrText = 
			PmBridge_GetPM()->GetColor( bEnabled ? COLOR_BTNTEXT : COLOR_3DSHADOW, this );
	}

	virtual void OnFlatTabWndGetMarginColors( COLORREF & clrMargin )
	{
		ASSERT_VALID( this );
		clrMargin = 
			PmBridge_GetPM()->GetColor( COLOR_3DDKSHADOW, this );
	}
protected:
	virtual void OnFlatTabWndQueryItemInclines(
		LONG nItemIndex,
		bool bSelected,
		bool * p_bItemHasInclineBefore,
		bool * p_bItemHasInclineAfter
		)
	{
		ASSERT_VALID( this );
		nItemIndex;
		bSelected;
		if( p_bItemHasInclineBefore != NULL )
		{
			*p_bItemHasInclineBefore = ItemsHasInclineBeforeGet();
		}
		if( p_bItemHasInclineAfter != NULL )
		{
			*p_bItemHasInclineAfter = ItemsHasInclineAfterGet();
		}
	}
	
	virtual int OnFlatTabWndGetSize( bool bHorz )
	{
		ASSERT_VALID( this );
		int nSize = 0;
		if(bHorz) 
			nSize = 16;//::GetSystemMetrics( SM_CXHSCROLL );
		else
			nSize = 16;//::GetSystemMetrics( SM_CYHSCROLL );

		return nSize;
	}
	
	// Generated message map functions
	//{{AFX_MSG(CExtTabFlatWnd)
	//}}AFX_MSG
	afx_msg LRESULT OnSizeParent( WPARAM wParam, LPARAM lParam );
	DECLARE_MESSAGE_MAP()
}; // class CExtTabFlatWnd

#define __EXTTAB_FLAT_GAP_X 2
#define __EXTTAB_FLAT_GAP_Y 2

#if (!defined __EXT_MFC_NO_TABMDI_CTRL )

/////////////////////////////////////////////////////////////////////////////
// CExtTabMdiFlatWnd window

class __PROF_UIS_API CExtTabMdiFlatWnd : public CExtTMWI < CExtTabFlatWnd >
{
public:
	DECLARE_DYNCREATE( CExtTabMdiFlatWnd );
	CExtTabMdiFlatWnd();
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExtTabMdiFlatWnd)
	protected:
	//}}AFX_VIRTUAL
	// Implementation
public:
	virtual ~CExtTabMdiFlatWnd();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	// Generated message map functions
protected:
	//{{AFX_MSG(CExtTabMdiFlatWnd)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
}; // class CExtTabMdiFlatWnd

#endif // (!defined __EXT_MFC_NO_TABMDI_CTRL )

#if (!defined __EXT_MFC_NO_TABBUTTONS_CTRL)

class __PROF_UIS_API CExtTabButtonsWnd : public CExtTabFlatWnd
{
// Construction
public:
	DECLARE_DYNCREATE( CExtTabButtonsWnd );
	CExtTabButtonsWnd();

// Attributes
public:

	// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExtTabButtonsWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CExtTabButtonsWnd();

protected:
	void OnTabWndDrawItem(
		CDC & dc,
		CRect & rcTabItemsArea,
		LONG nItemIndex,
		TAB_ITEM_INFO * pTii,
		bool bTopLeft,
		bool bHorz,
		bool bSelected,
		bool bCenteredText,
		bool bGroupedMode,
		bool bInGroupActive,
		bool bInvertedVerticalMode,
		const CRect & rcEntireItem,
		CSize sizeTextMeasured,
		CFont * pFont,
		__EXT_MFC_SAFE_LPCTSTR sText,
		CExtCmdIcon * pIcon
		);
	void OnTabWndMeasureItemAreaMargins(
		LONG & nSpaceBefore,
		LONG & nSpaceAfter,
		LONG & nSpaceOver
		);
	virtual void OnTabWndUpdateItemMeasure(
		TAB_ITEM_INFO * pTii,
		CDC & dcMeasure,
		CSize & sizePreCalc
		);
	virtual void OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		);
	virtual void OnTabWndDrawEntire(
		CDC & dc,
		CRect & rcClient
		);
	virtual INT OnTabWndGetParentSizingMargin(
		DWORD dwOrientation
		) const;
	virtual void OnFlatTabWndQueryItemInclines(
		LONG nItemIndex,
		bool bSelected,
		bool * p_bItemHasInclineBefore,
		bool * p_bItemHasInclineAfter
		)
	{
		ASSERT_VALID( this );
		nItemIndex;
		bSelected;
		if( p_bItemHasInclineBefore != NULL )
		{
			*p_bItemHasInclineBefore = 0; // ItemsHasInclineBeforeGet();
		}
		if( p_bItemHasInclineAfter != NULL )
		{
			*p_bItemHasInclineAfter = 0; // ItemsHasInclineAfterGet();
		}
	}
	
	// Generated message map functions
	//{{AFX_MSG(CExtTabButtonsWnd)
	//}}AFX_MSG
	afx_msg LRESULT OnSizeParent( WPARAM wParam, LPARAM lParam );
	DECLARE_MESSAGE_MAP()
}; // class CExtTabButtonsWnd

#if (!defined __EXT_MFC_NO_TABMDI_CTRL )

/////////////////////////////////////////////////////////////////////////////
// CExtTabMdiButtonsWnd window

class __PROF_UIS_API CExtTabMdiButtonsWnd : public CExtTMWI < CExtTabButtonsWnd >
{
public:
	DECLARE_DYNCREATE( CExtTabMdiButtonsWnd );
	CExtTabMdiButtonsWnd();
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExtTabMdiButtonsWnd)
	protected:
	//}}AFX_VIRTUAL
	// Implementation
public:
	virtual ~CExtTabMdiButtonsWnd();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	// Generated message map functions
protected:
	//{{AFX_MSG(CExtTabMdiButtonsWnd)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
}; // class CExtTabMdiButtonsWnd

#endif // (!defined __EXT_MFC_NO_TABMDI_CTRL )

#endif // (!defined __EXT_MFC_NO_TABBUTTONS_CTRL)

#endif // (!defined __EXT_MFC_NO_TABFLAT_CTRL)

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(__EXT_TAB_FLAT_WND_H)
