// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_PAGE_NAVIGATOR)

#if (!defined __EXT_PAGE_NAVIGATOR_H)
	#include <ExtPageNavigatorWnd.h>
#endif // (!defined __EXT_PAGE_NAVIGATOR_H)

#if (!defined __ExtCmdManager_H)
	#include <ExtCmdManager.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include <ExtMemoryDC.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if (!defined __EXT_POPUP_MENU_WND_H)
	#include <ExtPopupMenuWnd.h>
#endif

#if (!defined __EXT_CHECK_LIST_H)
	#include <ExtCheckListWnd.h>
#endif

#if (!defined __EXT_TEMPL_H)
	#include <ExtTempl.h>
#endif

#if (!defined __EXT_REGISTRY_H)
	#include <ExtRegistry.h>
#endif

#include <Resources/Resource.h>

#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CExtPageNavigatorOptionsDlg dialog

#if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)
#if (!defined __EXT_MFC_NO_CHECK_LIST)

class CExtPageNavigatorOptionsDlg : public CExtResDlg
{
// Construction
public:
	CExtPageNavigatorOptionsDlg(
		UINT nIdDlgResource,
		CExtPageNavigatorWnd * pPageNavigatorWnd
		); 

// Dialog Data
	//{{AFX_DATA(CExtPageNavigatorOptionsDlg)
	CButton m_wndMoveUp;
	CButton m_wndMoveDown;
	CButton m_wndReset;
	//}}AFX_DATA

	class CPagesListWnd : public CExtCheckListWnd
	{
	public:
		virtual CRect OnQueryItemMargins( INT nItem ) const
		{
			ASSERT_VALID( this );
			if( !(nItem >= 0 && nItem < GetCount()) )
			{
				ASSERT( FALSE );
				return CRect( 0, 0, 0, 0 );
			}
			return CRect( 2, 1, 2, 0 );
		}	
		virtual CRect OnQueryItemCheckMargins( INT nItem ) const
		{
			ASSERT_VALID( this );
			if( !(nItem >= 0 && nItem < GetCount()) )
			{
				ASSERT( FALSE );
				return CRect( 0, 0, 0, 0 );
			}
			return CRect( 0, 0, 2, 0 );
		}	
	}; // class CPagesListWnd

 	CExtWFF < CPagesListWnd > m_wndListPages;

	bool MoveLBItem( INT nIndex, bool bUp );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExtPageNavigatorOptionsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CExtPageNavigatorWnd * m_pPageNavigatorWnd;

	virtual bool OnQueryAutomaticRTLTransform() const
	{
		return true;
	}

	// Generated message map functions
	//{{AFX_MSG(CExtPageNavigatorOptionsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeListPages();
	afx_msg void OnMoveUp();
	afx_msg void OnMoveDown();
	afx_msg void OnReset();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CExtPageNavigatorOptionsDlg::CExtPageNavigatorOptionsDlg(
	UINT nIdDlgResource,
	CExtPageNavigatorWnd * pPageNavigatorWnd
	)
	: CExtResDlg( nIdDlgResource, pPageNavigatorWnd) 
	, m_pPageNavigatorWnd( pPageNavigatorWnd )
{
	ASSERT( m_pPageNavigatorWnd != NULL && ::IsWindow( m_pPageNavigatorWnd->m_hWnd ) );
	//{{AFX_DATA_INIT(CExtPageNavigatorOptionsDlg)
	//}}AFX_DATA_INIT
}

void CExtPageNavigatorOptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CExtResDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExtPageNavigatorOptionsDlg)
	DDX_Control(pDX, IDC_EXT_PN_PAGES_LIST, m_wndListPages);
	DDX_Control(pDX, IDC_EXT_PN_MOVE_UP, m_wndMoveUp);
	DDX_Control(pDX, IDC_EXT_PN_MOVE_DOWN, m_wndMoveDown);
	DDX_Control(pDX, IDC_EXT_PN_RESET, m_wndReset);
	//}}AFX_DATA_MAP
}

 
BEGIN_MESSAGE_MAP(CExtPageNavigatorOptionsDlg, CExtResDlg)
	//{{AFX_MSG_MAP(CExtPageNavigatorOptionsDlg)
	ON_LBN_SELCHANGE(IDC_EXT_PN_PAGES_LIST, OnSelchangeListPages)
	ON_BN_CLICKED(IDC_EXT_PN_MOVE_UP, OnMoveUp)
	ON_BN_CLICKED(IDC_EXT_PN_MOVE_DOWN, OnMoveDown)
	ON_BN_CLICKED(IDC_EXT_PN_RESET, OnReset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtPageNavigatorOptionsDlg message handlers

BOOL CExtPageNavigatorOptionsDlg::OnInitDialog() 
{
	CExtResDlg::OnInitDialog();

	m_wndListPages.SetCheckStyle( BS_CHECKBOX );
	for( LONG nItem = 0; nItem < m_pPageNavigatorWnd->ItemGetCount(); nItem ++ )
	{
		CExtPageNavigatorWnd::PAGE_ITEM_INFO * pPII = 
			m_pPageNavigatorWnd->ItemGetInfo( nItem );
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if( pPII != NULL )
		{
			INT nRow = m_wndListPages.AddString( pPII->TextGet() );
			m_wndListPages.SetCheck( 
				nRow, 
				pPII->EnabledGet() ? 1 : 0 
				);
			m_wndListPages.SetItemData( 
				nRow, 
				nItem
				);
		}
	}
	
	OnSelchangeListPages();

	return TRUE;
}

void CExtPageNavigatorOptionsDlg::OnSelchangeListPages() 
{
	INT nRow = m_wndListPages.GetCurSel();
	BOOL bEnabledMoveUp = FALSE;
	BOOL bEnabledMoveDown = FALSE;
	if( nRow != LB_ERR )
	{
		if( nRow > 0 )
			bEnabledMoveUp = TRUE;
		if( nRow < m_wndListPages.GetCount() - 1 )
			bEnabledMoveDown = TRUE;
	}
	if( m_wndMoveUp.IsWindowEnabled() != bEnabledMoveUp )
		m_wndMoveUp.EnableWindow( bEnabledMoveUp );
	if( m_wndMoveDown.IsWindowEnabled() != bEnabledMoveDown )
		m_wndMoveDown.EnableWindow( bEnabledMoveDown );
}

bool CExtPageNavigatorOptionsDlg::MoveLBItem( INT nIndex, bool bUp )
{
	if(		nIndex != LB_ERR 
		&&	(nIndex > 0 || !bUp)
		&&	(nIndex < m_wndListPages.GetCount() - 1 || bUp)
		)
	{
		bool bSelected = (m_wndListPages.GetCurSel() == nIndex) ? true : false;
		DWORD dwItemData = (DWORD)m_wndListPages.GetItemData( nIndex );
		INT nChecked = m_wndListPages.GetCheck( nIndex );
		CString sText;
		m_wndListPages.GetText( nIndex, sText );
		m_wndListPages.DeleteString( nIndex );

		nIndex = m_wndListPages.InsertString( nIndex - (bUp ? 1 : -1), sText );
		m_wndListPages.SetCheck( nIndex, nChecked ? 1 : 0 );
		m_wndListPages.SetItemData( nIndex, dwItemData );
		if( bSelected )
		m_wndListPages.SetCurSel( nIndex );

		OnSelchangeListPages();
		
		return true;
	}
	return false;
}

void CExtPageNavigatorOptionsDlg::OnMoveUp() 
{
	INT nRow = m_wndListPages.GetCurSel();
	if(		nRow != LB_ERR 
		&&	nRow > 0
		)
	{
		MoveLBItem( nRow, true );
	}
}

void CExtPageNavigatorOptionsDlg::OnMoveDown() 
{
	INT nRow = m_wndListPages.GetCurSel();
	if(		nRow != LB_ERR 
		&&	nRow < m_wndListPages.GetCount() - 1
		)
	{
		MoveLBItem( nRow, false );
	}
}

void CExtPageNavigatorOptionsDlg::OnReset() 
{
	m_wndListPages.SetRedraw( FALSE );

	bool bStopSorting = true;
	do
	{
		bStopSorting = true;
		for( LONG nRow = 0; nRow < m_wndListPages.GetCount() - 1; nRow ++ )
		{
			LONG nItem1 = (LONG)m_wndListPages.GetItemData( nRow );
			CExtPageNavigatorWnd::PAGE_ITEM_INFO * pPII1 = 
				m_pPageNavigatorWnd->ItemGetInfo( nItem1 );
			ASSERT( pPII1 != NULL );
			ASSERT_VALID( pPII1 );

			LONG nItem2 = (LONG)m_wndListPages.GetItemData( nRow + 1 );
			CExtPageNavigatorWnd::PAGE_ITEM_INFO * pPII2 = 
				m_pPageNavigatorWnd->ItemGetInfo( nItem2 );
			ASSERT( pPII2 != NULL );
			ASSERT_VALID( pPII2 );

			if( pPII1->m_nIndexOriginal > pPII2->m_nIndexOriginal )
			{
				MoveLBItem( nRow, false );
				bStopSorting = false;
				break;
			}
		}
	} 
	while( !bStopSorting );

	for( LONG nRow = 0; nRow < m_wndListPages.GetCount(); nRow ++ )
	{
		LONG nItem = (LONG)m_wndListPages.GetItemData( nRow );
		CExtPageNavigatorWnd::PAGE_ITEM_INFO * pPII = 
			m_pPageNavigatorWnd->ItemGetInfo( nItem );
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if( pPII != NULL )
		{
			m_wndListPages.SetCheck( 
				nRow,
				pPII->m_bEnabledOriginal ? 1 : 0
				);
		}
	}

	m_wndListPages.SetRedraw( TRUE );
	m_wndListPages.Invalidate();
	m_wndListPages.UpdateWindow();
}

void CExtPageNavigatorOptionsDlg::OnOK() 
{
	if( m_wndListPages.GetCount() != m_pPageNavigatorWnd->ItemGetCount() )
	{
		ASSERT( FALSE );
		return;
	}

	LONG nRow = 0;

	// sync items enable status
	for( nRow = 0; nRow < m_wndListPages.GetCount(); nRow ++ )
	{
		LONG nItem = (LONG)m_wndListPages.GetItemData( nRow );
		CExtPageNavigatorWnd::PAGE_ITEM_INFO * pPII = 
			m_pPageNavigatorWnd->ItemGetInfo( nItem );
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if( pPII != NULL )
		{
			INT nChecked = m_wndListPages.GetCheck( nRow );
			bool bEnabled = (nChecked != 0) ? true : false;
			if( pPII->EnabledGet() != bEnabled )
				pPII->EnabledSet( bEnabled, false );
		}
	}

	// sync items order
	CExtPageNavigatorWnd::PageItemsArr_t arrItemsOld;
	for( LONG nItem = 0; nItem < m_pPageNavigatorWnd->ItemGetCount(); nItem ++ )
	{
		CExtPageNavigatorWnd::PAGE_ITEM_INFO * pPII = 
			m_pPageNavigatorWnd->ItemGetInfo( nItem );
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if( pPII != NULL )
			arrItemsOld.Add( pPII );
	}
	for( nRow = 0; nRow < m_wndListPages.GetCount(); nRow ++ )
	{
		LONG nItem = (LONG)m_wndListPages.GetItemData( nRow );
		m_pPageNavigatorWnd->ItemSetInfo( 
			nRow, 
			arrItemsOld[ nItem ] 
			);
	}
	arrItemsOld.RemoveAll();

	m_pPageNavigatorWnd->UpdatePageNavigatorWnd( true );

	CExtResDlg::OnOK();
}

#endif // #if (!defined __EXT_MFC_NO_CHECK_LIST)
#endif // #if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)

/////////////////////////////////////////////////////////////////////////////
// PAGE_ITEM_INFO
/////////////////////////////////////////////////////////////////////////////

CExtPageNavigatorWnd::PAGE_ITEM_INFO::PAGE_ITEM_INFO(
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL
	const CExtCmdIcon * pIconExpanded, // = NULL
	const CExtCmdIcon * pIconCollapsed, // = NULL
	DWORD dwData, // = 0,
	CExtPageNavigatorWnd *pPageNavigatorWnd // = NULL
	)
	: m_sText( ( sText == NULL ) ? _T("") : sText )
	, m_sTooltip( ( sText == NULL ) ? _T("") : sText )
	, m_lParam( dwData )
	, m_bEnabled( true )
	, m_bEnabledOriginal( true )
	, m_nIndexOriginal( 0L )
	, m_bVisible( false )
	, m_bExpanded( false )
	, m_bHover( false )
	, m_bSelected( false )
	, m_bPressed( false )
	, m_bConfigButton( false )
	, m_rcRect( 0, 0, 0, 0 )
	, m_rcActivePage( 0, 0, 0, 0 )
	, m_pPageNavigatorWnd( pPageNavigatorWnd )
{
	ASSERT_VALID( this );
	if( pIconExpanded != NULL )
		m_IconExpanded.AssignFromOther( *pIconExpanded );
	if( pIconCollapsed != NULL )
		m_IconCollapsed.AssignFromOther( *pIconCollapsed );
}

CExtPageNavigatorWnd::PAGE_ITEM_INFO::PAGE_ITEM_INFO(
	CExtPageNavigatorWnd *pPageNavigatorWnd
	)
	: m_sText( _T("") )
	, m_sTooltip( _T("") )
	, m_lParam( 0 )
	, m_bEnabled( true )
	, m_bEnabledOriginal( true )
	, m_nIndexOriginal( 0L )
	, m_bVisible( true )
	, m_bExpanded( false )
	, m_bHover( false )
	, m_bSelected( false )
	, m_bPressed( false )
	, m_bConfigButton( true )
	, m_rcRect( 0, 0, 0, 0 )
	, m_rcActivePage( 0, 0, 0, 0 )
	, m_pPageNavigatorWnd( pPageNavigatorWnd )
{
	ASSERT_VALID( this );
}

CExtPageNavigatorWnd::PAGE_ITEM_INFO::PAGE_ITEM_INFO(
	const PAGE_ITEM_INFO & other
	)
{
	_AssignFromOther( other );
	ASSERT_VALID( this );
}

CExtPageNavigatorWnd::PAGE_ITEM_INFO::~PAGE_ITEM_INFO()
{
	_PaneRemoveAllImpl();
}

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::_AssignFromOther(
	const PAGE_ITEM_INFO & other
	)
{
	ASSERT_VALID( (&other) );

	m_sText = other.m_sText;
	m_sTooltip = other.m_sTooltip;
	m_IconExpanded = other.m_IconExpanded;
	m_IconCollapsed = other.m_IconCollapsed;
	m_lParam = other.m_lParam;
	m_bEnabled = other.m_bEnabled;
	m_bEnabledOriginal = other.m_bEnabledOriginal;
	m_nIndexOriginal = m_pPageNavigatorWnd->ItemGetIndexOf( this );
	m_bVisible = other.m_bVisible;
	m_bExpanded = other.m_bExpanded;
	m_bHover = other.m_bHover;
	m_bSelected = other.m_bSelected;
	m_bPressed = other.m_bPressed;
	m_bConfigButton = other.m_bConfigButton;
	m_rcRect = other.m_rcRect;
	m_rcActivePage = other.m_rcActivePage;
	m_pPageNavigatorWnd = other.m_pPageNavigatorWnd;

	ASSERT_VALID( this );
}

#ifdef _DEBUG
void CExtPageNavigatorWnd::PAGE_ITEM_INFO::AssertValid() const
{
	CObject::AssertValid();
	ASSERT( m_pPageNavigatorWnd != NULL && ::IsWindow( m_pPageNavigatorWnd->m_hWnd ) );

LONG nCount = 0;
LONG nItem = 0; 
	for( nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
		ASSERT( pIPI != NULL );
		ASSERT_VALID( pIPI );
		if(		pIPI != NULL 
			&&	pIPI->HeightGet() < 0 
			)
			nCount++;
	}
	ASSERT( nCount <= 1 );

	// page should have only one pressed pane caption
	nCount = 0;
	for( nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
		ASSERT( pIPI != NULL );
		ASSERT_VALID( pIPI );
		if(		pIPI != NULL 
			&&	pIPI->m_bPressed 
			)
			nCount++;
	}
	ASSERT( nCount <= 1 );

	// page should have only one hovered pane caption
	nCount = 0;
	for( nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
		ASSERT( pIPI != NULL );
		ASSERT_VALID( pIPI );
		if(		pIPI != NULL 
			&&	pIPI->m_bHover 
			)
			nCount++;
	}
	ASSERT( nCount <= 1 );
}
void CExtPageNavigatorWnd::PAGE_ITEM_INFO::Dump(CDumpContext& dc) const
{
	CObject::Dump( dc );
}
#endif

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::Serialize( 
	CArchive & ar,
	bool bPersistent // = true
	)
{
	CObject::Serialize( ar );
	if( ar.IsStoring() )
	{
		ar << BYTE(m_bEnabled);
	}
	else
	{
		BYTE bTmp;
		ar >> bTmp;
		EnabledSet( 
			( bTmp != 0 ) ? true : false, 
			bPersistent 
			);
	}
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
		ASSERT( pIPI != NULL );
		ASSERT_VALID( pIPI );
		if( pIPI != NULL )
			pIPI->Serialize( ar );
	}
}

CSize CExtPageNavigatorWnd::PAGE_ITEM_INFO::IconGetSize( 
	bool bExpanded 
	) const
{
	ASSERT_VALID( this );
	return bExpanded 
			? m_IconExpanded.GetSize() 
			: m_IconCollapsed.GetSize();
}

CExtCmdIcon * CExtPageNavigatorWnd::PAGE_ITEM_INFO::IconGetPtr( 
	bool bExpanded 
	) const
{
	ASSERT_VALID( this );
	CExtCmdIcon *pCmdIcon = 
		bExpanded 
			? (CExtCmdIcon *)&m_IconExpanded 
			: (CExtCmdIcon *)&m_IconCollapsed;
	if(		pCmdIcon == NULL
		||	pCmdIcon->IsEmpty()
		)
		return NULL;
	return pCmdIcon;
}

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::IconSet(
	HICON hIcon,
	bool bExpanded
	)
{
	ASSERT_VALID( this );
	if( hIcon == NULL )
		bExpanded 
			? m_IconExpanded.Empty() 
			: m_IconCollapsed.Empty();
	else
		bExpanded 
			? m_IconExpanded.AssignFromHICON( hIcon, true ) 
			: m_IconCollapsed.AssignFromHICON( hIcon, true );
}

__EXT_MFC_SAFE_LPCTSTR CExtPageNavigatorWnd::PAGE_ITEM_INFO::TextGet() const
{
	ASSERT_VALID( this );
	return __EXT_MFC_SAFE_LPCTSTR(m_sText);
}

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::TextSet(
	__EXT_MFC_SAFE_LPCTSTR sText // = NULL 
	)
{
	ASSERT_VALID( this );
	m_sText = ( sText == NULL ) ? _T("") : sText;
}

LPARAM CExtPageNavigatorWnd::PAGE_ITEM_INFO::LParamGet() const
{
	ASSERT_VALID( this );
	return m_lParam;
}

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::LParamSet( 
	LPARAM lParam // = 0 
	)
{
	ASSERT_VALID( this );
	m_lParam = lParam;
}

__EXT_MFC_SAFE_LPCTSTR CExtPageNavigatorWnd::PAGE_ITEM_INFO::TooltipGet() const
{
	ASSERT_VALID( this );
	return m_sTooltip;
}

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::TooltipSet( 
	__EXT_MFC_SAFE_LPCTSTR sTooltip // = NULL 
	)
{
	ASSERT_VALID( this );
	m_sTooltip = ( sTooltip == NULL ) ? _T("") : sTooltip;
}

bool CExtPageNavigatorWnd::PAGE_ITEM_INFO::EnabledGet() const
{
	ASSERT_VALID( this );
	return m_bEnabled;
}

bool CExtPageNavigatorWnd::PAGE_ITEM_INFO::EnabledSet(
	bool bEnabled, // = true
	bool bPersistent // = true
	)
{
	ASSERT_VALID( this );
	bool bEnabledOld = m_bEnabled;
	m_bEnabled = bEnabled;
	if( bPersistent )
		m_bEnabledOriginal = m_bEnabled;
	return bEnabledOld;
}

CExtPageNavigatorWnd::ITEM_PANE_INFO * CExtPageNavigatorWnd::PAGE_ITEM_INFO::HitTestPane( 
	const POINT & ptClient 
	) const
{
	ASSERT_VALID( this );
ITEM_PANE_INFO * pIPI = NULL;
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPITmp = m_arrItems[ nItem ];
		ASSERT( pIPITmp != NULL );
		ASSERT_VALID( pIPITmp );

		CRect rcPane = pIPITmp->ItemRectGet();
		CRect rcPaneCaption = pIPITmp->CaptionRectGet();
		
		if(		(	( !rcPane.IsRectEmpty() )
				&&	rcPane.PtInRect(ptClient) 
				)
			||	(	( !rcPaneCaption.IsRectEmpty() )
				&&	rcPaneCaption.PtInRect(ptClient) 
				)
			)
		{
			pIPI = pIPITmp;
			break;
		}
	}
	return pIPI;	
}

//////////////////////////////////////////////////////////////////////////

CExtPageNavigatorWnd::ITEM_PANE_INFO * CExtPageNavigatorWnd::PAGE_ITEM_INFO::PaneInsert(
	HWND hWnd,
	LONG nIndex, // = -1 
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL, 
	INT nHeight, // = -1,
	bool bUpdate // = false
	)
{
	ASSERT_VALID( this );
	LONG nCount = PaneGetCount();
	if( nIndex < 0 || nIndex > nCount )
		nIndex = nCount;
	
	if( ::GetParent( hWnd ) != m_pPageNavigatorWnd->GetSafeHwnd() )
		::SetParent( hWnd, m_pPageNavigatorWnd->GetSafeHwnd() );

	ITEM_PANE_INFO * pIPI =
		new ITEM_PANE_INFO(
			hWnd,
			sText,
			nHeight,
			m_pPageNavigatorWnd
			);
	
	ASSERT( pIPI != NULL );
	ASSERT_VALID( pIPI );
	m_arrItems.InsertAt( nIndex, pIPI );

	if( m_pPageNavigatorWnd->m_pCurrentItem == this )
		Show();
	else
		Hide();

	m_pPageNavigatorWnd->UpdatePageNavigatorWnd( bUpdate );
	return pIPI;
}

//////////////////////////////////////////////////////////////////////////

bool CExtPageNavigatorWnd::PAGE_ITEM_INFO::PaneRemove( 
	LONG nIndex,
	bool bUpdate // = false 
	)
{
	ASSERT_VALID( this );
	LONG nCount = PaneGetCount();
	ASSERT( nIndex >= 0 && nIndex < nCount );
	if( nIndex < 0 || nIndex >= nCount )
	{
		ASSERT( FALSE );
		return false;
	}
ITEM_PANE_INFO * pIPI = PaneGetInfo( nIndex );
	m_arrItems.RemoveAt( nIndex );
	ASSERT( pIPI != NULL );
	ASSERT_VALID( pIPI );
	delete pIPI;
	pIPI = NULL;
	m_pPageNavigatorWnd->UpdatePageNavigatorWnd( bUpdate );
	return true;
}

//////////////////////////////////////////////////////////////////////////

LONG CExtPageNavigatorWnd::PAGE_ITEM_INFO::PaneRemoveAll(
   bool bUpdate // = false
   )
{
	ASSERT_VALID( this );
LONG nRetVal = _PaneRemoveAllImpl();
	ASSERT_VALID( this );
	m_pPageNavigatorWnd->UpdatePageNavigatorWnd( bUpdate );
	return nRetVal;
}

//////////////////////////////////////////////////////////////////////////

LONG CExtPageNavigatorWnd::PAGE_ITEM_INFO::_PaneRemoveAllImpl()
{
LONG nRetVal = (LONG)m_arrItems.GetSize();
	while( m_arrItems.GetSize() > 0 )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[0];
		m_arrItems.RemoveAt(0);
		delete pIPI;
		pIPI = NULL;
	}
	return nRetVal;
}

//////////////////////////////////////////////////////////////////////////

CExtPageNavigatorWnd::ITEM_PANE_INFO * CExtPageNavigatorWnd::PAGE_ITEM_INFO::PaneGetInfo(
	LONG nIndex
	)
{
	ASSERT_VALID( this );
LONG nCount = PaneGetCount();
	ASSERT( nIndex >= 0 && nIndex < nCount );
	if( nIndex < 0 || nIndex >= nCount )
	{
		ASSERT( FALSE );
		return NULL;
	}
	ITEM_PANE_INFO * pIPI = m_arrItems[ nIndex ];
	ASSERT( pIPI != NULL );
	ASSERT_VALID( pIPI );
	return pIPI;
}

const CExtPageNavigatorWnd::ITEM_PANE_INFO * CExtPageNavigatorWnd::PAGE_ITEM_INFO::PaneGetInfo(
	LONG nIndex
	) const
{
	ASSERT_VALID( this );
const ITEM_PANE_INFO * ptr =
		( const_cast < CExtPageNavigatorWnd::PAGE_ITEM_INFO * > ( this ) )
			-> PaneGetInfo( nIndex );
	return ptr;
}

//////////////////////////////////////////////////////////////////////////

bool CExtPageNavigatorWnd::PAGE_ITEM_INFO::PaneExpand( 
	LONG nIndex,
	ITEM_PANE_INFO::e_ExpandType_t eExpandType,
	bool bUpdate // = false
	)
{
	ASSERT_VALID( this );
ITEM_PANE_INFO * pIPI = PaneGetInfo( nIndex );
	ASSERT( pIPI != NULL );
	ASSERT_VALID( pIPI );
	return pIPI->Expand( eExpandType, bUpdate );
}

bool CExtPageNavigatorWnd::PAGE_ITEM_INFO::PaneIsExpanded(
	LONG nIndex
	) const
{
	ASSERT_VALID( this );
const ITEM_PANE_INFO * pIPI = PaneGetInfo( nIndex );
	ASSERT( pIPI != NULL );
	ASSERT_VALID( pIPI );
	return pIPI->IsExpanded();
}

bool CExtPageNavigatorWnd::PAGE_ITEM_INFO::PaneExpandableGet(
	LONG nIndex
	) const
{
	ASSERT_VALID( this );
const ITEM_PANE_INFO * pIPI = PaneGetInfo( nIndex );
	ASSERT( pIPI != NULL );
	ASSERT_VALID( pIPI );
	return pIPI->ExpandableGet();
}

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::PaneExpandableSet(
	LONG nIndex,
	bool bSet // = true
	)
{
	ASSERT_VALID( this );
ITEM_PANE_INFO * pIPI = PaneGetInfo( nIndex );
	ASSERT( pIPI != NULL );
	ASSERT_VALID( pIPI );
	pIPI->ExpandableSet( bSet );
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::ActivePageRectSet( 
	LPCRECT rcActivePage 
	)
{
	ASSERT_VALID( this );
	ASSERT( rcActivePage != NULL );
	m_rcActivePage = rcActivePage;

	// recalc layout of the panes
	INT nItemPageCaptionHeight = m_pPageNavigatorWnd->OnQueryItemPageCaptionHeight();

	CRect rcActivePageRest( m_rcActivePage );

	bool bAutoSizeExists = false;

	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
		ASSERT( pIPI != NULL );
		ASSERT_VALID( pIPI );
		if( pIPI != NULL )
		{
			pIPI->ItemRectSetEmpty();
			pIPI->CaptionRectSetEmpty();

			if( rcActivePageRest.top < rcActivePageRest.bottom )
			{
				bool bExpanded = pIPI->IsExpanded();

				if(		pIPI->HeightGet() < 0 
					&&	bExpanded
					)
				{
					bAutoSizeExists = true;
					continue;
				}

				// pane area
				INT nBottom = 0;
				INT nTop = 0;
				INT nCurItemCaptionHeight = 
					( pIPI->TextGetRef().IsEmpty() ? 0 : nItemPageCaptionHeight );

				if(	((!bAutoSizeExists) && nItem == (m_arrItems.GetSize() - 1) ) )
				{
					// last item and item with autosize is not exists
					nTop = rcActivePageRest.top;
					nBottom = rcActivePageRest.bottom;
				}
				else if( bAutoSizeExists )
				{
					nBottom = rcActivePageRest.bottom;
					nTop = nBottom - (bExpanded ? pIPI->HeightGet() : nCurItemCaptionHeight);
				}
				else
				{
					// item with autosize is not reached yet
					nTop = rcActivePageRest.top;
					nBottom = nTop + (bExpanded ? pIPI->HeightGet() : nCurItemCaptionHeight);
				}

				// normalize rect
				if( nTop > rcActivePageRest.bottom )
					nTop = rcActivePageRest.bottom;
				else
					if( nTop < rcActivePageRest.top )
						nTop = rcActivePageRest.top;

				if( nBottom > rcActivePageRest.bottom )
					nBottom = rcActivePageRest.bottom;

				// pane window area
				CRect rcItem( 
					rcActivePageRest.left, 
					nBottom, 
					rcActivePageRest.right, 
					nBottom 
					);
				if( bExpanded )
				{
					rcItem.SetRect(
						rcActivePageRest.left,
						nTop + nCurItemCaptionHeight,
						rcActivePageRest.right,
						nBottom
						);
					pIPI->ItemRectSet( rcItem );
				}

				// pane caption area
				if( nCurItemCaptionHeight > 0 )
					pIPI->CaptionRectSet(
						rcActivePageRest.left,
						rcItem.top - nCurItemCaptionHeight,
						rcActivePageRest.right,
						rcItem.top
						);

				if(		pIPI->HeightGet() < 0 
					&&	(!bExpanded)
					&&	nItem > 0
					)
				{
					ITEM_PANE_INFO * pIPIPrev = m_arrItems[ nItem - 1 ];
					ASSERT( pIPIPrev != NULL );
					ASSERT_VALID( pIPIPrev );
					if( pIPIPrev != NULL )
						pIPIPrev->m_rcRect.bottom = pIPI->CaptionRectGet().top;
				}

				if( bAutoSizeExists )
				{
					rcActivePageRest.bottom -= pIPI->CaptionRectGet().Height();
					rcActivePageRest.bottom -= pIPI->ItemRectGet().Height();
				}
				else
				{
					if( pIPI->HeightGet() >= 0 )
					{
						rcActivePageRest.top += pIPI->CaptionRectGet().Height();
						rcActivePageRest.top += pIPI->ItemRectGet().Height();
					}
					else
					{
						rcActivePageRest.top = pIPI->CaptionRectGet().bottom;
					}
				}
			}
		}
	}
	if( bAutoSizeExists )
	{
		for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
		{
			ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
			ASSERT( pIPI != NULL );
			ASSERT_VALID( pIPI );
			if(		pIPI != NULL 
				&&	pIPI->HeightGet() < 0
				)
			{
				pIPI->ItemRectSetEmpty();
				pIPI->CaptionRectSetEmpty();

				if( rcActivePageRest.top < rcActivePageRest.bottom )
				{
					INT nCurItemCaptionHeight = 
						( pIPI->TextGetRef().IsEmpty() ? 0 : nItemPageCaptionHeight );
	
					INT nTop = rcActivePageRest.top;
					INT nBottom = rcActivePageRest.bottom;

					// normalize rect
					if( nTop > rcActivePageRest.bottom )
						nTop = rcActivePageRest.bottom;
					else
						if( nTop < rcActivePageRest.top )
							nTop = rcActivePageRest.top;

					if( nBottom > rcActivePageRest.bottom )
						nBottom = rcActivePageRest.bottom;

					// pane window area
					CRect rcItem( 
						rcActivePageRest.left, 
						nBottom, 
						rcActivePageRest.right, 
						nBottom 
						);
					if( pIPI->IsExpanded() )
					{
						rcItem.SetRect(
							rcActivePageRest.left,
							nTop + nCurItemCaptionHeight,
							rcActivePageRest.right,
							nBottom
							);
						pIPI->ItemRectSet( rcItem );
					}

					// pane caption area
					if( nCurItemCaptionHeight > 0 )
					{
						pIPI->CaptionRectSet(
							rcActivePageRest.left,
							rcItem.top - nCurItemCaptionHeight,
							rcActivePageRest.right,
							rcItem.top
							);
					}
				}
				break;
			}
		}
	}
}

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::_ClearPressedFlag()
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
		ASSERT( pIPI != NULL );
		ASSERT_VALID( pIPI );
		if(	pIPI != NULL )
			pIPI->m_bPressed = false;
	}
}

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::_ClearHoverFlag()
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
		ASSERT( pIPI != NULL );
		ASSERT_VALID( pIPI );
		if(	pIPI != NULL )
			pIPI->m_bHover = false;
	}
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::OnPageItemDrawEntire(
	CDC & dc
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
		ASSERT( pIPI != NULL );
		ASSERT_VALID( pIPI );
		if( pIPI != NULL )
			m_pPageNavigatorWnd->OnPageItemPaneDrawCaption( 
				dc,
				pIPI->CaptionRectGet(),
				pIPI->TextGetRef(),
				(pIPI->m_bHover && pIPI->m_bExpandable),
				(pIPI->m_bPressed && pIPI->m_bExpandable),
				pIPI->m_bExpandable,
				pIPI->m_bExpanded,
				( nItem == 0 ) ? false : true
				);
	}
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::Show()
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
		ASSERT( pIPI != NULL );
		ASSERT_VALID( pIPI );
		if( pIPI != NULL )
		{
			Resize();
			if( pIPI->IsExpanded() )
				::ShowWindow( pIPI->GetSafeHwnd(), SW_SHOW );
			if( ::GetFocus() != NULL )
				::SetFocus( pIPI->GetSafeHwnd() );
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::Hide()
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
		ASSERT( pIPI != NULL );
		ASSERT_VALID( pIPI );
		if( pIPI != NULL )
			::ShowWindow( pIPI->GetSafeHwnd(), SW_HIDE );
	}
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::PAGE_ITEM_INFO::Resize()
{
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		ITEM_PANE_INFO * pIPI = m_arrItems[ nItem ];
		ASSERT( pIPI != NULL );
		ASSERT_VALID( pIPI );
		if( pIPI != NULL )
		{
			CRect rc = pIPI->ItemRectGet();
			::MoveWindow( 
				pIPI->GetSafeHwnd(), 
				rc.left,
				rc.top,
				rc.Width(),
				rc.Height(),
				TRUE
				);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CExtPageNavigatorWnd::ITEM_PANE_INFO
/////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
void CExtPageNavigatorWnd::ITEM_PANE_INFO::AssertValid() const
{
	CObject::AssertValid();
}
void CExtPageNavigatorWnd::ITEM_PANE_INFO::Dump(CDumpContext& dc) const
{
	CObject::Dump( dc );
}
#endif

void CExtPageNavigatorWnd::ITEM_PANE_INFO::Serialize( CArchive & ar )
{
	CObject::Serialize( ar );
	if( ar.IsStoring() )
	{
		// ar << BYTE(m_bExpandable);
		ar << BYTE(m_bExpanded);
	}
	else
	{
		BYTE bTmp;
		// ar >> bTmp;
		// m_bExpandable = ( bTmp != 0 ) ? true : false;
		ar >> bTmp;
		m_bExpanded = ( bTmp != 0 ) ? true : false;
	}
}

HWND CExtPageNavigatorWnd::ITEM_PANE_INFO::GetSafeHwnd()
{
	ASSERT_VALID( this );
	if(		m_hWnd == NULL
		||	(! ::IsWindow(m_hWnd) )
		)
		return NULL;
	return m_hWnd;
}

__EXT_MFC_SAFE_LPCTSTR CExtPageNavigatorWnd::ITEM_PANE_INFO::TextGet() const
{
	ASSERT_VALID( this );
	return ( m_sText.IsEmpty() ? _T("") : __EXT_MFC_SAFE_LPCTSTR(m_sText) );
}

void CExtPageNavigatorWnd::ITEM_PANE_INFO::TextSet( 
	__EXT_MFC_SAFE_LPCTSTR sText 
	)
{
	ASSERT_VALID( this );
	m_sText = (sText == NULL) ? _T("") : sText;
}

const CExtSafeString & CExtPageNavigatorWnd::ITEM_PANE_INFO::TextGetRef() const
{
	ASSERT_VALID( this );
	return m_sText;
}

bool CExtPageNavigatorWnd::ITEM_PANE_INFO::Expand( 
	e_ExpandType_t eExpandType,
	bool bUpdate // = false
	)
{
	ASSERT_VALID( this );
bool bExpand = true;
	switch( eExpandType )
	{
	case __ET_COLLAPSE:
		bExpand = false;
		break;
	case __ET_EXPAND:
		bExpand = true;
	    break;
	case __ET_TOGGLE:
		bExpand = !m_bExpanded;
	    break;
	default:
		ASSERT( FALSE );
		return false;
	}
HWND hWnd = GetSafeHwnd();
	if( hWnd != NULL )
	{
		m_bExpanded = bExpand;
		m_pPageNavigatorWnd->UpdatePageNavigatorWnd( bUpdate );
		::ShowWindow( hWnd, bExpand ? SW_SHOW : SW_HIDE );
// 		if(		bExpand
// 			&&	::GetFocus() != NULL 
// 			)
// 			::SetFocus( hWnd );
		return true;
	}
	return false;
}

bool CExtPageNavigatorWnd::ITEM_PANE_INFO::ExpandableGet() const
{
	ASSERT_VALID( this );
	return m_bExpandable;
}

void CExtPageNavigatorWnd::ITEM_PANE_INFO::ExpandableSet(
	bool bSet // = true
	)
{
	ASSERT_VALID( this );
	m_bExpandable = bSet;
}

bool CExtPageNavigatorWnd::ITEM_PANE_INFO::IsExpanded() const
{
	ASSERT_VALID( this );
	return m_bExpanded;
}

CExtSafeString & CExtPageNavigatorWnd::ITEM_PANE_INFO::TextGetRef()
{
	ASSERT_VALID( this );
	return m_sText;
}

CRect CExtPageNavigatorWnd::ITEM_PANE_INFO::ItemRectGet() const
{
	ASSERT_VALID( this );
	return m_rcRect;
}

void CExtPageNavigatorWnd::ITEM_PANE_INFO::ItemRectSet( 
	const RECT & rc 
	)
{
	ASSERT_VALID( this );
	m_rcRect = rc;
}

void CExtPageNavigatorWnd::ITEM_PANE_INFO::ItemRectSet( 
	int x1, 
	int y1, 
	int x2, 
	int y2 
	)
{
	ASSERT_VALID( this );
	m_rcRect.SetRect( x1, y1, x2, y2 );
}

void CExtPageNavigatorWnd::ITEM_PANE_INFO::ItemRectSetEmpty()
{
	ASSERT_VALID( this );
	m_rcRect.SetRectEmpty();
}

CRect CExtPageNavigatorWnd::ITEM_PANE_INFO::CaptionRectGet() const
{
	ASSERT_VALID( this );
	return m_rcCaption;
}

void CExtPageNavigatorWnd::ITEM_PANE_INFO::CaptionRectSet( 
	const RECT & rc 
	)
{
	ASSERT_VALID( this );
	m_rcCaption = rc;
}

void CExtPageNavigatorWnd::ITEM_PANE_INFO::CaptionRectSet( 
	int x1, 
	int y1, 
	int x2, 
	int y2 
	)
{
	ASSERT_VALID( this );
	m_rcCaption.SetRect( x1, y1, x2, y2 );
}

void CExtPageNavigatorWnd::ITEM_PANE_INFO::CaptionRectSetEmpty()
{
	ASSERT_VALID( this );
	m_rcCaption.SetRectEmpty();
}

INT CExtPageNavigatorWnd::ITEM_PANE_INFO::HeightGet() const
{
	ASSERT_VALID( this );
	return m_nHeight;
}

void CExtPageNavigatorWnd::ITEM_PANE_INFO::HeightSet( 
	INT nHeight 
	)
{
	ASSERT_VALID( this );
	m_nHeight = nHeight;
}

LPARAM CExtPageNavigatorWnd::ITEM_PANE_INFO::LParamGet() const
{
	ASSERT_VALID( this );
	return m_lParam;
}

void CExtPageNavigatorWnd::ITEM_PANE_INFO::LParamSet( 
	LPARAM lParam // = 0 
	)
{
	ASSERT_VALID( this );
	m_lParam = lParam;
}

/////////////////////////////////////////////////////////////////////////////
// CExtPageNavigatorWnd
/////////////////////////////////////////////////////////////////////////////

BYTE CExtPageNavigatorWnd::g_arrBmpDataShowMoreButtons[] = 
{	
	0x42,0x4D,0x36,0x03,0x00,0x00,0x00,0x00,
	0x00,0x00,0x36,0x00,0x00,0x00,0x28,0x00,
	0x00,0x00,0x10,0x00,0x00,0x00,0x10,0x00,
	0x00,0x00,0x01,0x00,0x18,0x00,0x00,0x00,
	0x00,0x00,0x00,0x03,0x00,0x00,0xC4,0x0E,
	0x00,0x00,0xC4,0x0E,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xD0,0x78,0x50,
	0xC0,0x70,0x40,0xB0,0x68,0x40,0xB0,0x60,
	0x40,0xA0,0x58,0x30,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xD0,0x78,0x50,
	0xF0,0x70,0x30,0xF0,0x70,0x30,0xF0,0x70,
	0x30,0xA0,0x58,0x30,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xD3,0xB7,0xAB,0xE0,0x88,0x50,0xD0,
	0x80,0x50,0xD0,0x78,0x50,0xD0,0x78,0x50,
	0xFF,0x80,0x40,0xF0,0x70,0x30,0xF0,0x70,
	0x30,0xA0,0x58,0x30,0xA0,0x50,0x30,0x90,
	0x50,0x30,0x90,0x48,0x20,0xC7,0x98,0x86,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xD5,0xB7,0xAB,0xE0,
	0x88,0x60,0xFF,0xC0,0xA0,0xFF,0x98,0x70,
	0xFF,0x88,0x50,0xFF,0x80,0x40,0xF0,0x70,
	0x30,0xF0,0x68,0x20,0xF0,0x68,0x20,0xD0,
	0x68,0x30,0xC7,0x9A,0x86,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xD7,
	0xB4,0xA5,0xE0,0x88,0x60,0xFF,0xC0,0xA0,
	0xFF,0xB0,0x80,0xFF,0xA0,0x70,0xF0,0x88,
	0x50,0xF0,0x70,0x30,0xD0,0x60,0x30,0xC5,
	0x9F,0x90,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xD5,0xB1,0xA0,0xE0,0x88,0x60,
	0xFF,0xC0,0xA0,0xFF,0xB0,0x90,0xFF,0xA0,
	0x60,0xD0,0x68,0x30,0xC9,0xA1,0x90,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xD5,0xB1,0xA0,
	0xE0,0x88,0x50,0xFF,0xC0,0xA0,0xE0,0x70,
	0x40,0xC7,0x9A,0x86,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xD5,0xB1,0xA0,0xE0,0x88,0x50,0xC7,0x9A,
	0x86,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xD1,0xA9,0x98,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF
};

BYTE CExtPageNavigatorWnd::g_arrBmpDataShowFewerButtons[] = 
{	
	0x42,0x4D,0x36,0x03,0x00,0x00,0x00,0x00,
	0x00,0x00,0x36,0x00,0x00,0x00,0x28,0x00,
	0x00,0x00,0x10,0x00,0x00,0x00,0x10,0x00,
	0x00,0x00,0x01,0x00,0x18,0x00,0x00,0x00,
	0x00,0x00,0x00,0x03,0x00,0x00,0xC4,0x0E,
	0x00,0x00,0xC4,0x0E,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xD1,0xA0,0x89,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xD5,0xB1,0xA0,0xA0,0x58,0x30,0xC7,0x9A,
	0x86,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xD5,0xB1,0xA0,
	0xD0,0x70,0x40,0xD0,0x58,0x20,0x90,0x48,
	0x20,0xC7,0x9A,0x86,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xD5,0xB1,0xA0,0xD0,0x78,0x40,
	0xFF,0x98,0x60,0xF0,0x78,0x30,0xD0,0x58,
	0x20,0x90,0x48,0x20,0xC9,0xA1,0x90,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xD7,
	0xB4,0xA5,0xDF,0x83,0x56,0xFF,0xB8,0x90,
	0xFF,0xA8,0x70,0xFF,0x98,0x60,0xF0,0x78,
	0x30,0xD0,0x58,0x20,0x90,0x48,0x20,0xC5,
	0x9F,0x90,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xD5,0xB7,0xAB,0xE0,
	0x88,0x60,0xFF,0xC0,0xA0,0xFF,0xC0,0xA0,
	0xFF,0xB8,0x90,0xFF,0xA8,0x70,0xFF,0x98,
	0x60,0xE0,0x88,0x50,0xD0,0x70,0x40,0x90,
	0x48,0x20,0xC7,0x9A,0x86,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xD3,0xB7,0xAB,0xE0,0x98,0x70,0xE0,
	0x98,0x70,0xE0,0x98,0x70,0xE0,0x90,0x60,
	0xFF,0xC0,0xA0,0xFF,0xB8,0x90,0xFF,0xA8,
	0x70,0xD0,0x58,0x20,0xD0,0x60,0x20,0xE0,
	0x68,0x30,0xE0,0x68,0x30,0xC7,0x98,0x86,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xF0,0xA8,0x80,
	0xFF,0xC0,0xA0,0xFF,0xC0,0xA0,0xFF,0xB8,
	0x90,0xE0,0x70,0x30,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xF0,0xB0,0x90,
	0xE0,0xA0,0x70,0xE0,0x90,0x60,0xE0,0x80,
	0x50,0xE0,0x70,0x40,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,
	0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF,
	0x00,0xFF,0xFF,0x00,0xFF,0xFF,0x00,0xFF,
	0xFF,0x00,0xFF,0xFF,0x00,0xFF,0xFF
};

CExtPageNavigatorWnd::SELECTION_NOTIFICATION::SELECTION_NOTIFICATION(
	bool bChangedFinally,
	LONG nPageOld,
	LONG nPageNew,
	LPARAM lParamCookie
	)
	: m_bChangedFinally( bChangedFinally )
	, m_lParamCookie( lParamCookie )
	, m_nPageOld( nPageOld )
	, m_nPageNew( nPageNew )
{
}

CExtPageNavigatorWnd::SELECTION_NOTIFICATION::operator WPARAM() const
{
WPARAM wParam = reinterpret_cast < WPARAM > ( this );
	return wParam;
}

const CExtPageNavigatorWnd::SELECTION_NOTIFICATION *
	CExtPageNavigatorWnd::SELECTION_NOTIFICATION::FromWPARAM( WPARAM wParam )
{
CExtPageNavigatorWnd::SELECTION_NOTIFICATION * pSN =
		reinterpret_cast < CExtPageNavigatorWnd::SELECTION_NOTIFICATION * > ( wParam );
	ASSERT( pSN != NULL );
	return pSN;
}

LRESULT CExtPageNavigatorWnd::SELECTION_NOTIFICATION::Notify(
	HWND hWndNotify
	) const
{
	ASSERT(
			hWndNotify != NULL
		&&	::IsWindow( hWndNotify )
		);
	return 
		::SendMessage(
			hWndNotify,
			CExtPageNavigatorWnd::g_nMsgSelectionNotification,
			*this,
			m_lParamCookie
			);
}

//////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE( CExtPageNavigatorWnd, CWnd );
IMPLEMENT_CExtPmBridge_MEMBERS( CExtPageNavigatorWnd );

CExtPageNavigatorWnd::CExtPageNavigatorWnd()
	: m_bDirectCreateCall( false )
	, m_bInitialized( false )
	, m_bUpdatingLayout( false )
	, m_bAutoDeleteWindow( false )
	, m_rcPageNavigatorClient( 0, 0, 0, 0 )
	, m_rcPageNavigatorInnerArea( 0, 0, 0, 0 )
	, m_rcItemCaption( 0, 0, 0, 0 )
	, m_rcCollapsedItemsArea( 0, 0, 0, 0 )
	, m_rcExpandedItemsArea( 0, 0, 0, 0 )
	, m_rcSplitter( 0, 0, 0, 0 )
	, m_bSplitterMouseTracking( false )
	, m_bSplitterMouseHover( false )
	, m_bCanceling( false )
	, m_bItemsMouseHover( false )
	, m_nMinPageAreaHeight( 200 )
	, m_nExpandedItemsDesired( 0 )
	, m_bSplitterVisible( true )
	, m_bSplitterEnabled( true )
	, m_hSizeCursor( NULL )
	, m_hHandCursor( NULL )
	, m_pCurrentItem( NULL )
	, m_lParamCookie( 0 )
	, m_hWndNotificationReceiver( NULL )
{
	VERIFY( RegisterWndClass() );
	m_sCommandProfile.Format(
		_T("PageNavigator-%d-%d-%d"),
		int( this ),
		int( ::GetCurrentThreadId() ),
		int( ::GetCurrentProcessId() )
		);

	PmBridge_Install();
}

CExtPageNavigatorWnd::~CExtPageNavigatorWnd()
{
	PmBridge_Uninstall();

	_ItemRemoveAllImpl( true );
	if( m_hSizeCursor != NULL )
	{
		::DestroyCursor( m_hSizeCursor );
		m_hSizeCursor = NULL;
	}
	if( m_hHandCursor != NULL )
	{
		::DestroyCursor( m_hHandCursor );
		m_hHandCursor = NULL;
	}
}

BEGIN_MESSAGE_MAP(CExtPageNavigatorWnd, CWnd)
	//{{AFX_MSG_MAP(CExtPageNavigatorWnd)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	ON_WM_ACTIVATEAPP()
	ON_WM_SYSCOLORCHANGE()
	__EXT_MFC_SAFE_ON_WM_SETTINGCHANGE()
	ON_MESSAGE(WM_DISPLAYCHANGE, OnDisplayChange)
	ON_MESSAGE(__ExtMfc_WM_THEMECHANGED, OnThemeChanged)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtPageNavigatorWnd message handlers

HCURSOR CExtPageNavigatorWnd::g_hCursor = ::LoadCursor( NULL, IDC_ARROW );
bool CExtPageNavigatorWnd::g_bWndClassRegistered = false;
const UINT CExtPageNavigatorWnd::g_nMsgSelectionNotification =
	::RegisterWindowMessage(
		_T("CExtPageNavigatorWnd::g_nMsgSelectionNotification")
		);

bool CExtPageNavigatorWnd::RegisterWndClass()
{
	if( g_bWndClassRegistered )
		return true;
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo(
			hInst,
			__EXT_PAGE_NAVIGATOR_CLASS_NAME,
			&_wndClassInfo
			)
		)
	{
		_wndClassInfo.style = CS_GLOBALCLASS|CS_DBLCLKS;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor =
			( g_hCursor != NULL )
				? g_hCursor
				: ::LoadCursor( NULL, IDC_ARROW )
				;
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_PAGE_NAVIGATOR_CLASS_NAME;
		if( !::AfxRegisterClass( & _wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
	g_bWndClassRegistered = true;
	return true;
}

#ifdef _DEBUG
void CExtPageNavigatorWnd::AssertValid() const
{
	CWnd::AssertValid();

	INT i = 0;
	INT nCount = 0;

	// the expand button should be exists
	nCount = 0;
	for( i = 0; i < m_arrItems.GetSize(); i++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[i];
		ASSERT( pPII != NULL );
		if(	pPII->m_bConfigButton ) 
			nCount++;
	}
	ASSERT( 0 <= nCount && nCount <= 1 );

	// configuration button cannot be currently selected
	if( m_pCurrentItem != NULL )
	{
		ASSERT( !m_pCurrentItem->m_bConfigButton );
	}

	// control should have only one pressed item
	nCount = 0;
	for( i = 0; i < m_arrItems.GetSize(); i++ )
		if(	m_arrItems[i]->m_bPressed ) 
			nCount++;
	ASSERT( nCount <= 1 );

	// control should have only one hovered item
	nCount = 0;
	for( i = 0; i < m_arrItems.GetSize(); i++ )
		if(	m_arrItems[i]->m_bHover) 
			nCount++;
	ASSERT( nCount <= 1 );

	ASSERT( m_nExpandedItemsDesired >= 0 );
}
void CExtPageNavigatorWnd::Dump(CDumpContext& dc) const
{
	CWnd::Dump( dc );
}
#endif // _DEBUG

//////////////////////////////////////////////////////////////////////////

bool CExtPageNavigatorWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( IDC_STATIC )
	DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN
	CCreateContext * pContext // = NULL
	)
{
	ASSERT_VALID( this );
	if( ! RegisterWndClass() )
	{
		ASSERT( FALSE );
		return false;
	}
	m_bDirectCreateCall = true;
	if( ! CWnd::Create(
		__EXT_PAGE_NAVIGATOR_CLASS_NAME,
		NULL,
		dwWindowStyle,
		rcWnd,
		pParentWnd,
		nDlgCtrlID,
		pContext
		)
		)
	{
		ASSERT( FALSE );
		return false;
	}
	m_bInitialized = true;
	return true;
}

//////////////////////////////////////////////////////////////////////////

bool CExtPageNavigatorWnd::_CreateHelper()
{
	ASSERT_VALID( this );
	if( m_bInitialized )
		return true;
	m_hSizeCursor = AfxGetApp()->LoadStandardCursor(MAKEINTRESOURCE(IDC_SIZENS));

INT nCursorID = 
#if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)
	IDC_EXT_HAND;
#else
	32649;
#endif // (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)

HINSTANCE hInstResource =
		AfxFindResourceHandle(
			MAKEINTRESOURCE(nCursorID),
			RT_GROUP_CURSOR
			);
	m_hHandCursor =
		(HCURSOR)::LoadImage(
			hInstResource,
			MAKEINTRESOURCE(nCursorID),
			IMAGE_CURSOR,
			0,
			0,
			0
			);
	if( m_hHandCursor == NULL )
		m_hHandCursor =
			::LoadCursor(
				NULL,
				MAKEINTRESOURCE(nCursorID)
				);

	// add config buttton
PAGE_ITEM_INFO * pPII =
		new PAGE_ITEM_INFO( this );
	ASSERT_VALID( pPII );

CExtSafeString sTip;
	if( ! g_ResourceManager->LoadString( sTip, IDS_EXT_PN_CONFIG_BUTTON_TIP ) )
		sTip = _T("Configure buttons");
	pPII->TooltipSet( sTip );
	m_arrItems.InsertAt( 0, pPII );

	ASSERT( ! m_sCommandProfile.IsEmpty() );
	VERIFY( g_CmdManager->ProfileSetup( m_sCommandProfile, m_hWnd ) );	

	_InitToolTip( false );
	UpdatePageNavigatorWnd( false );
	return true;
}

//////////////////////////////////////////////////////////////////////////

bool CExtPageNavigatorWnd::_InitToolTip( 
	bool bActivate // = false 
		)
{
	if( m_wndToolTip.m_hWnd == NULL )
	{
		// enabling tooltips
		EnableToolTips( TRUE );
		if( !m_wndToolTip.Create(this) )
	{
		ASSERT( FALSE );
		return false;
	}
		m_wndToolTip.SetMaxTipWidth( 200 ); // enable multiline tooltips
		m_wndToolTip.Activate( bActivate ? TRUE : FALSE );
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////

CExtPageNavigatorWnd::PAGE_ITEM_INFO * CExtPageNavigatorWnd::_GetConfigButton()
{
	ASSERT_VALID( this );
	PAGE_ITEM_INFO * pPIIConfigBtn = NULL;
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(		pPII != NULL 
			&&	pPII->m_bConfigButton
			)
		{
			pPIIConfigBtn = pPII;
			break;
		}
	}
	return pPIIConfigBtn;
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::_ClearExpandedFlag()
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(	pPII != NULL )
			pPII->m_bExpanded = false;
	}
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::_ClearPressedFlag()
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(	pPII != NULL )
			pPII->m_bPressed = false;
	}
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::_ClearPanesPressedFlag()
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(	pPII != NULL )
			pPII->_ClearPressedFlag();
	}
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::_ClearHoverFlag()
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(	pPII != NULL )
			pPII->m_bHover = false;
	}
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::_ClearPanesHoverFlag()
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(	pPII != NULL )
			pPII->_ClearHoverFlag();
	}
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::_ClearVisibleFlag()
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(	pPII != NULL )
			pPII->m_bVisible = false;
	}
}

//////////////////////////////////////////////////////////////////////////

INT CExtPageNavigatorWnd::_CalcMaxExpandedItemsCount()
{
	ASSERT_VALID( this );
	INT nItemHeight = OnQueryItemHeight();
	INT nSplitterHeight = OnQuerySplitterHeight();
	INT nCollapsedAreaHeight = OnQueryCollapsedAreaHeight();

	CRect rcClient;
	GetClientRect( &rcClient );
	INT nClientHeight = rcClient.Height();

	// client height without collapsed and separator areas
	INT nUsefulHeight = (nClientHeight - nCollapsedAreaHeight - nSplitterHeight );
	if( nUsefulHeight < 0 )
		nUsefulHeight = 0;
	INT nMaxExpandedItemsCount = 0;
	if( m_arrItems.GetSize() > 0 )
	{
		// area in which the items can be expanded
		INT nHeightForExpand = nUsefulHeight - m_nMinPageAreaHeight;
		if( nHeightForExpand < 0 )
			nHeightForExpand = 0;
		// calculate maximum allowed expanded items count
		nMaxExpandedItemsCount = nHeightForExpand / nItemHeight;
	}
	return nMaxExpandedItemsCount;
}

//////////////////////////////////////////////////////////////////////////

LRESULT CExtPageNavigatorWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if( message == WM_PRINT || message == WM_PRINTCLIENT )
	{
		CDC * pDC = CDC::FromHandle( (HDC)wParam );
		CRect rcClient;
		GetClientRect( &rcClient );
		OnPageNavigatorDrawEntire( *pDC, rcClient );
		if( (lParam&PRF_CHILDREN) != 0 )
			CExtPaintManager::stat_PrintChildren(
				m_hWnd,
				message,
				pDC->GetSafeHdc(),
				lParam,
				false
				);
		return (!0);
	}
	switch( message ) 
	{
	case WM_ERASEBKGND:
		return (!0);
	case WM_PAINT:
	{
		CPaintDC dcPaint( this );
		CRect rcClient;
		GetClientRect( &rcClient );
		if( rcClient.IsRectEmpty() )
			break;
		CExtMemoryDC dc(
			&dcPaint,
			&rcClient
			);
		OnPageNavigatorDrawEntire( dc, rcClient );
		PmBridge_GetPM()->OnPaintSessionComplete( this );
		return 0;
	}
	break;
	case WM_SIZE:
	{
		LRESULT lResult = CWnd::WindowProc(message, wParam, lParam);
		UpdatePageNavigatorWnd( true );
		return lResult;
	}
	case WM_SETCURSOR:
	{
		if(		m_bSplitterMouseTracking
			||	m_bSplitterMouseHover 
			&&	( ItemGetCount() > 0 )
			)
		{
			::SetCursor( m_hSizeCursor );
			return 0;
		}

		POINT point;
		if( ::GetCursorPos(&point) )
		{
			ScreenToClient( &point );
			LONG nHitTest = HitTest( point );

			if(		nHitTest == __EPNWH_COLLAPSED_ITEMS_AREA 
				||	nHitTest == __EPNWH_EXPANDED_ITEMS_AREA 
				||	nHitTest == __EPNWH_ITEM_PAGE
				)
			{
				PAGE_ITEM_INFO * pPII = HitTestItem( point );
				if( pPII != NULL )
				{
					bool bSetCursor = false;
					if( nHitTest == __EPNWH_ITEM_PAGE )
					{
						ITEM_PANE_INFO * pIPI = pPII->HitTestPane( point );
						if(		pIPI != NULL 
							&&	pIPI->ExpandableGet()
							)
						{
							CRect rcPaneCaption = pIPI->CaptionRectGet();
							if(		( !rcPaneCaption.IsRectEmpty() )
								&&	rcPaneCaption.PtInRect(point) 
								)
								bSetCursor = true;
						}
					}
					else
						bSetCursor = true;

					if( bSetCursor )
					{
						::SetCursor( m_hHandCursor );
						return 0;
					}
				}					
			}
		}
	}
	break;
	case WM_LBUTTONDOWN:
	case WM_LBUTTONDBLCLK:
	{
		POINT point;
		if( ::GetCursorPos(&point) )
		{
			ScreenToClient( &point );
			if( _ProcessMouseClick( point, true, MK_LBUTTON ) )
				return 0;
		}
	}
	break;
	case WM_LBUTTONUP:
	{
		POINT point;
		if( ::GetCursorPos(&point) )
		{
			ScreenToClient( &point );
			if( _ProcessMouseClick( point, false, MK_LBUTTON ) )
				return 0;
		}
	}
	break;
	case WM_RBUTTONDOWN:
	case WM_RBUTTONDBLCLK:
	{
		POINT point;
		if( ::GetCursorPos(&point) )
		{
			ScreenToClient( &point );
			if( _ProcessMouseClick( point, true, MK_RBUTTON ) )
				return 0;
		}
	}
	break;
	case WM_RBUTTONUP:
	{
		POINT point;
		if( ::GetCursorPos(&point) )
		{
			ScreenToClient( &point );
			if( _ProcessMouseClick( point, false, MK_RBUTTON ) )
				return 0;
		}
	}
	break;
	case WM_MOUSEMOVE:
	{
		POINT point;
		if( ::GetCursorPos(&point) )
		{
			ScreenToClient( &point );
			if( _ProcessMouseMove( point ) )
				return 0;
		}
	}
	break;
	case WM_CANCELMODE:
	{

		LRESULT lResult = CWnd::WindowProc(message, wParam, lParam);
		if( m_bCanceling )
			return lResult;
		m_bCanceling = true;
		m_bSplitterMouseTracking = false;
		if( CExtMouseCaptureSink::GetCapture() == GetSafeHwnd() )
			CExtMouseCaptureSink::ReleaseCapture();

		_ClearHoverFlag();
		_ClearPressedFlag();
		_ClearPanesHoverFlag();
		_ClearPanesPressedFlag();

		Invalidate( FALSE );
		
		m_bCanceling = false;
		return lResult;
	}
	break;
	case WM_CAPTURECHANGED:
	{
		LRESULT lResult = CWnd::WindowProc(message, wParam, lParam);
		if(		CExtMouseCaptureSink::GetCapture() != GetSafeHwnd() 
			&&	m_bSplitterMouseTracking
			)
			SendMessage( WM_CANCELMODE );
		return lResult;
	}
	break;
	case WM_NOTIFY:
	if(		m_wndToolTip.GetSafeHwnd() != NULL
		&&	IsWindow( m_wndToolTip.GetSafeHwnd() )
		&&	((LPNMHDR)lParam) != NULL
		&&	((LPNMHDR)lParam)->hwndFrom == m_wndToolTip.GetSafeHwnd()
		&&	((LPNMHDR)lParam)->code == TTN_SHOW
		)
	{
		::SetWindowPos(
			m_wndToolTip.GetSafeHwnd(),
			HWND_TOP,
			0,0,0,0,
			SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE
			);
		return 0;
	}
	if( ((LPNMHDR)lParam)->code == TTN_NEEDTEXT  )
	{
		UINT nID = UINT( ((LPNMHDR)lParam)->idFrom );
		if(	(nID - 1) >= 0 && nID <= (UINT)m_arrItems.GetSize() )
		{
			PAGE_ITEM_INFO * pPII = m_arrItems[ nID - 1 ];
			ASSERT( pPII != NULL );
			ASSERT_VALID( pPII );
			if(	pPII != NULL )
			{
				TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)((LPNMHDR)lParam);
				lstrcpy( pTTT->lpszText, pPII->TooltipGet() );
				return 0;
			}
		}
	}
	break;
	case WM_COMMAND:
	{
		LONG nCmd = (UINT)wParam;
		LONG nItemCount = ItemGetCount();
		if(		nCmd > 0 
			&&	nCmd <= 2*nItemCount 
			)
		{
			if( nCmd > nItemCount )
			{
				// enabled/disabled items
				nCmd = nCmd - nItemCount;
				PAGE_ITEM_INFO * pPII = m_arrItems[ nCmd ];
				ASSERT( pPII != NULL );
				ASSERT_VALID( pPII );
				if(	pPII != NULL )
				{
					pPII->EnabledSet( 
						!pPII->EnabledGet(), 
						false 
						);
					UpdatePageNavigatorWnd( true );
				}
			}
			else
			{
				// invisible items
				PAGE_ITEM_INFO * pPII = m_arrItems[ nCmd ];
				ASSERT( pPII != NULL );
				ASSERT_VALID( pPII );
				if(		pPII != NULL 
					&&	m_pCurrentItem != pPII
					)
					SelectionSet( pPII );
			}
			return 0;
		}
	}
	break;
	case WM_TIMER:
	if( ! CExtPopupMenuWnd::IsMenuTracking() )
	{
		KillTimer( 0x0001 );
		Invalidate( FALSE );
		return 0;
	}
	break;
	case WM_DESTROY:
	{
		VERIFY( 
			g_CmdManager->ProfileWndRemove( m_hWnd, true ) 
			);
		return CWnd::WindowProc(message, wParam, lParam);
	}
	} // switch( message ) 
	if( message == CExtPopupMenuWnd::g_nMsgNotifyMenuClosed )
	{
		PAGE_ITEM_INFO * pPII = _GetConfigButton();
		pPII->m_bHover = false;
		Invalidate( FALSE );
		// delayed repaint, if page navigator is placed into floating control bar
		// we should manually repaint self when popup menu will be completely closed
		SetTimer( 0x0001, 10, NULL );
		return 0;
	} // if( message == CExtPopupMenuWnd::g_nMsgNotifyMenuClosed )
	else if( message == CExtPopupMenuWnd::g_nMsgPrepareMenu )
	{
		CExtPopupMenuWnd::MsgPrepareMenuData_t * pData =
			reinterpret_cast
				< CExtPopupMenuWnd::MsgPrepareMenuData_t * >
					( wParam );
		ASSERT( pData != NULL );
		CExtPopupMenuWnd * pPopup = pData->m_pPopup;
		ASSERT( pPopup != NULL );
		INT nItemPos =
			pPopup->ItemFindPosForCmdID(
				IDC_EXT_PN_ADD_OR_REMOVE_BUTTONS
				);
		if( nItemPos > 0 )
		{
			// enabled/disabled items
			CExtPopupMenuWnd * pNewPopup = new CExtPopupMenuWnd;
			if( pNewPopup->CreatePopupMenu( GetSafeHwnd() ) )
			{
				for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
				{
					PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
					ASSERT( pPII != NULL );
					ASSERT_VALID( pPII );
					if(		pPII != NULL 
						&&	!pPII->m_bConfigButton
						)
					{
						CExtSafeString sText = pPII->m_sText;
						sText = sText.SpanExcluding(_T("\r\n"));
						pNewPopup->ItemInsertCommand(
							nItem + ItemGetCount(),
							-1,
							sText,
							NULL,
							pPII->m_IconCollapsed,
							pPII->EnabledGet(),
							GetSafeHwnd()
							);
					}
				}
				VERIFY(
					pPopup->ItemInsertSpecPopup(
						pNewPopup,
						nItemPos + 1,
						pPopup->ItemGetText(nItemPos),
						pPopup->ItemGetIcon(nItemPos)
						)
					);
			} // if( pNewPopup->CreatePopupMenu( GetSafeHwnd() ) )
			else
				delete pNewPopup;
			VERIFY( pPopup->ItemRemove(nItemPos) );

			// invisible items
			if( ItemGetVisibleCount() < ItemGetEnabledCount() )
			{
				VERIFY(
					pPopup->ItemInsert(
						CExtPopupMenuWnd::TYPE_SEPARATOR,
						-1
						)
					);
				for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
				{
					PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
					ASSERT( pPII != NULL );
					ASSERT_VALID( pPII );
					if(		pPII != NULL 
						&&	pPII->EnabledGet()
						&&	!pPII->m_bVisible
						&&	!pPII->m_bConfigButton
						)
					{
						CExtSafeString sText = pPII->m_sText;
						sText = sText.SpanExcluding(_T("\r\n"));
						pPopup->ItemInsertCommand(
							nItem,
							-1,
							sText,
							NULL,
							pPII->m_IconCollapsed,
							(pPII == m_pCurrentItem) ? 1 : 0,
							GetSafeHwnd()
							);
					}
				}
			}

		} // if( nItemPos > 0 )
		return 0;
	} // else if( message == CExtPopupMenuWnd::g_nMsgPrepareMenu )
	return CWnd::WindowProc(message, wParam, lParam);
}

//////////////////////////////////////////////////////////////////////////

BOOL CExtPageNavigatorWnd::PreTranslateMessage(MSG* pMsg) 
{
	if(	m_wndToolTip.GetSafeHwnd() != NULL )
		m_wndToolTip.RelayEvent( pMsg );
	return CWnd::PreTranslateMessage(pMsg);
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::PreSubclassWindow() 
{
	CWnd::PreSubclassWindow();
	if( m_bDirectCreateCall )
		return;
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		AfxThrowMemoryException();
	} // if( ! _CreateHelper() )
	m_bInitialized = true;
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::PostNcDestroy() 
{
	ASSERT( ! m_sCommandProfile.IsEmpty() );
	g_CmdManager->ProfileDestroy( m_sCommandProfile, true );
	if( m_bAutoDeleteWindow )
		delete this;
}

//////////////////////////////////////////////////////////////////////////

#if _MFC_VER < 0x700
void CExtPageNavigatorWnd::OnActivateApp(BOOL bActive, HTASK hTask) 
#else
void CExtPageNavigatorWnd::OnActivateApp(BOOL bActive, DWORD hTask) 
#endif
{
	CWnd::OnActivateApp(bActive, hTask);
	if( ! bActive )
		SendMessage( WM_CANCELMODE );
}

void CExtPageNavigatorWnd::OnSysColorChange() 
{
	ASSERT_VALID( this );
	CWnd::OnSysColorChange();
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnSysColorChange( this );
	g_CmdManager.OnSysColorChange( pPM, this );
	UpdatePageNavigatorWnd();	
}

LRESULT CExtPageNavigatorWnd::OnDisplayChange( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
LRESULT lResult = CWnd::OnDisplayChange( wParam, lParam );
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnDisplayChange( this, (INT)wParam, CPoint(lParam) );
	g_CmdManager.OnDisplayChange( pPM, this, (INT)wParam, CPoint(lParam) );
	return lResult;
}

LRESULT CExtPageNavigatorWnd::OnThemeChanged( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
LRESULT lResult = Default();
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnThemeChanged( this, wParam, lParam );
	g_CmdManager.OnThemeChanged( pPM, this, wParam, lParam );
	return lResult;
}

void CExtPageNavigatorWnd::OnSettingChange(UINT uFlags, __EXT_MFC_SAFE_LPCTSTR lpszSection) 
{
	ASSERT_VALID( this );
	CWnd::OnSettingChange(uFlags, lpszSection);
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnSettingChange( this, uFlags, lpszSection );
	g_CmdManager.OnSettingChange( pPM, this, uFlags, lpszSection );
}

//////////////////////////////////////////////////////////////////////////

HWND CExtPageNavigatorWnd::OnPageNavigatorGetNotificationReceiver() const
{
	ASSERT_VALID( this );
	if(		m_hWndNotificationReceiver != NULL
		&&	::IsWindow( m_hWndNotificationReceiver )
		)
		return m_hWndNotificationReceiver;
	if( GetSafeHwnd() == NULL )
		return NULL;
HWND hWndNotificationReceiver = ::GetParent( m_hWnd );
	return hWndNotificationReceiver;
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::OnPageNavigatorEraseClientArea(
	CDC & dc,
	const CRect & rcClient
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintPageNavigatorClientArea(
		dc,
		rcClient,
		(CObject*)this
		);
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::OnPageNavigatorDrawBorder(
	CDC & dc,
	const CRect & rcClient
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintPageNavigatorBorder(
		dc,
		rcClient
		);
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::OnPageNavigatorDrawItemCaptionArea(
	CDC & dc,
	const CRect & rcItemCaptionArea,
	__EXT_MFC_SAFE_LPCTSTR sText
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
CExtSafeString sCaption = sText;
	sCaption = sCaption.SpanExcluding(_T("\r\n"));
	PmBridge_GetPM()->PaintPageNavigatorItemCaption(
		dc,
		rcItemCaptionArea,
		sCaption
		);
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::OnPageNavigatorDrawCollapsedItemsArea(
	CDC & dc,
	const CRect & rcCollapsedItemsArea
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

	// collapsed area background
	PmBridge_GetPM()->PaintPageNavigatorItem(
		dc,
		rcCollapsedItemsArea,
		NULL,
		NULL,
		true,
		false,
		false
		);	

	// collapsed items
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(		pPII != NULL 
			&&  pPII->EnabledGet()
			&&  pPII->m_bVisible
			&& !pPII->m_bExpanded
			)
		{
			PmBridge_GetPM()->PaintPageNavigatorItem(
				dc,
				pPII->m_rcRect,
				pPII->TextGet(),
				&pPII->m_IconCollapsed,
				false,
				pPII->m_bPressed 
					&& pPII->m_bHover
					|| (m_pCurrentItem == pPII ),
				pPII->m_bHover 
					&& ( ItemGetPressed() == NULL || ItemGetPressed() == pPII )
				);	

			if( pPII->m_bConfigButton )
				PmBridge_GetPM()->PaintPageNavigatorConfigButton(
					dc,
					pPII->m_rcRect,
					pPII->m_bPressed,
					pPII->m_bHover
					);	
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::OnPageNavigatorDrawSplitter(
	CDC & dc,
	const CRect & rcSplitter,
	bool bDrawDots // = true
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintPageNavigatorSplitter(
		dc,
		rcSplitter,
		bDrawDots
		);
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::OnPageNavigatorDrawExpandedItemsArea(
	CDC & dc,
	const CRect & rcExpandedItemsArea
	) const
{
	rcExpandedItemsArea;
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(		pPII != NULL 
			&&  pPII->EnabledGet()
			&&	pPII->m_bExpanded
			&& !pPII->m_bConfigButton
			)
		{
			PmBridge_GetPM()->PaintPageNavigatorItem(
				dc,
				pPII->m_rcRect,
				pPII->TextGet(),
				&pPII->m_IconExpanded,
				true,
				pPII->m_bPressed 
					&& pPII->m_bHover
					|| (m_pCurrentItem == pPII ),
				pPII->m_bHover 
					&& ( ItemGetPressed() == NULL || ItemGetPressed() == pPII )
				);	
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::OnPageItemPaneDrawCaption(
	CDC & dc,
	const CRect & rcCaption,
	__EXT_MFC_SAFE_LPCTSTR strCaption,
	bool bHover,
	bool bPressed,
	bool bExpandable,
	bool bExpanded,
	bool bDrawTopLine // = true
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintPageNavigatorItemPaneCaption(
		dc,
		rcCaption,
		strCaption,
		bHover,
		bPressed,
		bExpandable,
		bExpanded,
		bDrawTopLine
		);
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::UpdatePageNavigatorWnd(
	bool bUpdate // = false
	)
{
	ASSERT_VALID( this );
	if( ! m_bInitialized )
		return;
	if(		GetSafeHwnd() == NULL
		||	( ! ::IsWindow( m_hWnd ) )
		)
		return;
	
	_RecalcLayout();

	if( m_pCurrentItem != NULL )
	{
		ASSERT_VALID( m_pCurrentItem );
		m_pCurrentItem->Resize();
	}
	
	if( bUpdate )
		RedrawWindow(
			NULL,
			NULL,
			RDW_INVALIDATE | RDW_UPDATENOW 
			| RDW_ERASE | RDW_ERASENOW
			| RDW_FRAME | RDW_ALLCHILDREN
			);
}

//////////////////////////////////////////////////////////////////////////

INT CExtPageNavigatorWnd::OnQueryItemPageCaptionHeight() const
{
	ASSERT_VALID( this );
	return PmBridge_GetPM()->PageNavigator_GetItemPaneCaptionHeight();
}

INT CExtPageNavigatorWnd::OnQueryItemCaptionHeight() const
{
	ASSERT_VALID( this );
	return PmBridge_GetPM()->PageNavigator_GetItemCaptionHeight();
}

INT CExtPageNavigatorWnd::OnQuerySplitterHeight() const
{
	ASSERT_VALID( this );
	if( IsSplitterVisible() )
		return PmBridge_GetPM()->PageNavigator_GetSplitterHeight();
	return 0;
}

INT CExtPageNavigatorWnd::OnQueryItemHeight() const
{
	ASSERT_VALID( this );
	return 32;
}

INT CExtPageNavigatorWnd::OnQueryCollapsedAreaHeight() const
{
	ASSERT_VALID( this );
	return 32;
}

CSize CExtPageNavigatorWnd::OnQueryCollapsedItemSize() const
{
	ASSERT_VALID( this );
	return CSize( 22, 30 );
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::OnPageNavigatorDrawEntire(
	CDC & dc,
	const CRect & rcClient
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

	// draw background
	OnPageNavigatorEraseClientArea( 
		dc, 
		rcClient 
		);

	// draw border
	OnPageNavigatorDrawBorder( 
		dc, 
		m_rcPageNavigatorClient 
		);

	if( m_pCurrentItem != NULL )
	{
		ASSERT_VALID( m_pCurrentItem );
	
		// draw page panes captions
		m_pCurrentItem->OnPageItemDrawEntire( dc );

		// draw current item's caption
		OnPageNavigatorDrawItemCaptionArea(
			dc, 
			m_rcItemCaption,
			m_pCurrentItem->TextGet()
			);
	}

	// draw collapsed items area
	OnPageNavigatorDrawCollapsedItemsArea( 
		dc, 
		m_rcCollapsedItemsArea 
		);

	// draw expanded items area
	OnPageNavigatorDrawExpandedItemsArea( 
		dc, 
		m_rcExpandedItemsArea 
		);

	// draw splitter
	if( IsSplitterVisible() )
		OnPageNavigatorDrawSplitter( 
			dc, 
			m_rcSplitter,
			( ItemGetCount() > 0 && IsSplitterEnabled() ) 
				? true 
				: false
			);
}

//////////////////////////////////////////////////////////////////////////

LONG CExtPageNavigatorWnd::HitTest( const POINT & ptClient ) const
{
	ASSERT_VALID( this );
CRect rcClient;
	GetClientRect( &rcClient );
	if(		rcClient.IsRectEmpty()
		||	(! rcClient.PtInRect(ptClient) )
		)
		return __EPNWH_NOWHERE;
	if(		(! m_rcItemCaption.IsRectEmpty() )
		&&	m_rcItemCaption.PtInRect(ptClient)
		)
		return __EPNWH_ITEM_CAPTION;
	if(		(! m_rcCollapsedItemsArea.IsRectEmpty() )
		&&	m_rcCollapsedItemsArea.PtInRect(ptClient)
		)
		return __EPNWH_COLLAPSED_ITEMS_AREA;
	if(		(! m_rcExpandedItemsArea.IsRectEmpty() )
		&&	m_rcExpandedItemsArea.PtInRect(ptClient)
		)
		return __EPNWH_EXPANDED_ITEMS_AREA;
	if(		(! m_rcSplitter.IsRectEmpty() )
		&&	m_rcSplitter.PtInRect(ptClient)
		)
		return __EPNWH_SPLITTER;
	if( m_pCurrentItem != NULL )
	{
		if(		(! m_pCurrentItem->m_rcActivePage.IsRectEmpty() )
			&&	m_pCurrentItem->m_rcActivePage.PtInRect(ptClient)
			)
			return __EPNWH_ITEM_PAGE;
	}
	return __EPNWH_NOWHERE;
}

//////////////////////////////////////////////////////////////////////////

CExtPageNavigatorWnd::PAGE_ITEM_INFO * CExtPageNavigatorWnd::HitTestItem( const POINT & ptClient ) const
{
PAGE_ITEM_INFO * pPII = NULL;
LONG nHitTest = HitTest( ptClient );

	if(		nHitTest == __EPNWH_COLLAPSED_ITEMS_AREA
		||	nHitTest == __EPNWH_EXPANDED_ITEMS_AREA
		)
	{
		for( INT i = 0; i < m_arrItems.GetSize(); i++ )
		{
			PAGE_ITEM_INFO * pPIITmp = m_arrItems[i];
			ASSERT( pPIITmp != NULL );
			ASSERT_VALID( pPIITmp );

			if(		( !pPIITmp->m_rcRect.IsRectEmpty() )
				&&	pPIITmp->m_rcRect.PtInRect(ptClient)
				)
			{
				pPII = pPIITmp;
				break;
			}
		}
	}
	else if( nHitTest == __EPNWH_ITEM_PAGE )
	{
		if( m_pCurrentItem != NULL )
		{
			if(		( !m_pCurrentItem->m_rcActivePage.IsRectEmpty() )
				&&	m_pCurrentItem->m_rcActivePage.PtInRect(ptClient)
				)
				pPII = m_pCurrentItem;
		}
	}
	return pPII;
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::_ActivateOnClick()
{
	ASSERT_VALID( this );
HWND hWndOwn = GetSafeHwnd();
	if(		hWndOwn == NULL
		||	( ! ::IsWindow(hWndOwn) )
		)
		return;

HWND hWndActivate = ::GetParent( hWndOwn );
	for( ; hWndActivate != NULL; hWndActivate = ::GetParent(hWndActivate) )
	{
		DWORD dwStyle = (DWORD)
			::GetWindowLong( hWndActivate, GWL_STYLE );
		if( (dwStyle&WS_CHILD) == 0 )
			break;
	}

	if( hWndActivate != NULL )
	{
		HWND hWndFocus = ::GetFocus();
		if(	hWndFocus == NULL )
			::SetFocus( hWndActivate );
		else if(	hWndActivate != hWndFocus
					|| ( !::IsChild(hWndActivate, hWndFocus) )
					)
		{
			if( ! CExtPopupMenuWnd::TestHoverEnabledFromActiveHWND(hWndOwn) )
				::SetFocus( hWndActivate );
		}
	}
	else
		::SetFocus( hWndOwn );
}

bool CExtPageNavigatorWnd::_ProcessMouseMove( CPoint point )
{
	ASSERT_VALID( this );

	if( CExtPopupMenuWnd::IsMenuTracking() )
		return false;

	if( m_bSplitterMouseTracking )
	{
		CRect rcClient;
		GetClientRect( &rcClient );

		INT nItemHeight = OnQueryItemHeight();
		INT nSplitterHeight = OnQuerySplitterHeight();
		INT nCollapsedAreaHeight = OnQueryCollapsedAreaHeight();

		double dExpandedItemsDesired = 
			double(rcClient.Height() - point.y - nSplitterHeight - nCollapsedAreaHeight) / double(nItemHeight);

		INT nExpandedItemsDesired = 
			( m_rcSplitter.top < point.y )
			? (INT)ceil( dExpandedItemsDesired )
			: (INT)floor( dExpandedItemsDesired );

		if(		nExpandedItemsDesired != ItemGetExpandedCount() 
			&&	nExpandedItemsDesired <= _CalcMaxExpandedItemsCount()
			&&	nExpandedItemsDesired >= 0
			) 
		{
			m_nExpandedItemsDesired = nExpandedItemsDesired;
			UpdatePageNavigatorWnd( true );
		}
		return true;
	}

LONG nHitTest = HitTest( point );
bool bNeedRedraw = false;
bool bPrevItemsMouseHover = m_bItemsMouseHover;
	m_bItemsMouseHover = false;

bool bReleaseCapture = false;
	if(	nHitTest == __EPNWH_ITEM_PAGE )
	{
		_ClearHoverFlag();

		PAGE_ITEM_INFO * pPII = HitTestItem( point );
		if(		pPII != NULL 
// 			&&	( ItemGetPressed() == NULL )
// 			&&	( ItemPaneGetPressed() == NULL )
			)
		{
			ITEM_PANE_INFO * pIPI = pPII->HitTestPane( point );
			if(		pIPI != NULL 
				&&	pIPI->ExpandableGet()
				)
			{
				CRect rcPaneCaption = pIPI->CaptionRectGet();
				if(		( !rcPaneCaption.IsRectEmpty() )
					&&	rcPaneCaption.PtInRect(point) 
					)
				{
					// if mouse is really on items
					if(		CExtMouseCaptureSink::GetCapture() != GetSafeHwnd() 
						&&	( !m_bSplitterMouseTracking ) 
						)
						CExtMouseCaptureSink::SetCapture( GetSafeHwnd() );

					if( !pIPI->m_bHover )
					{
						pPII->_ClearHoverFlag();
						pIPI->m_bHover = true;
						bNeedRedraw = true;
					}
					m_bItemsMouseHover = true;
				}
				else
				{
					pPII->_ClearHoverFlag();
					if( bPrevItemsMouseHover != m_bItemsMouseHover )
						bNeedRedraw = true;
					bReleaseCapture = true;
				}
			}
			else
			{
				pPII->_ClearHoverFlag();
				bNeedRedraw = true;
				bReleaseCapture = true;
			}
		}
 	}
	else if(	nHitTest == __EPNWH_COLLAPSED_ITEMS_AREA
			||	nHitTest == __EPNWH_EXPANDED_ITEMS_AREA
			)
	{
		_ClearPanesHoverFlag();
		
		PAGE_ITEM_INFO * pPII = HitTestItem( point );
		if(	pPII != NULL )
		{
			// if mouse is really on items
			if(		CExtMouseCaptureSink::GetCapture() != GetSafeHwnd() 
				&&	( !m_bSplitterMouseTracking ) 
				)
				CExtMouseCaptureSink::SetCapture( GetSafeHwnd() );

			if( !pPII->m_bHover )
			{
				_ClearHoverFlag();
				pPII->m_bHover = true;
				bNeedRedraw = true;
			}
			m_bItemsMouseHover = true;
		}
		else
		{
			_ClearHoverFlag();
			if( bPrevItemsMouseHover != m_bItemsMouseHover )
				bNeedRedraw = true;
			bReleaseCapture = true;
		}
	}
	else
	{
		// if mouse is really NOT on items
		if(		CExtMouseCaptureSink::GetCapture() == GetSafeHwnd() 
			&&	( !m_bSplitterMouseTracking ) 
			)
			bReleaseCapture = true;

		_ClearHoverFlag();
		_ClearPanesHoverFlag();

		bNeedRedraw = true;
	}
	
	if(		bReleaseCapture 
		&&	( ItemGetPressed() == NULL )
		&&	( ItemPaneGetPressed() == NULL )
		)
	{
		if( CExtMouseCaptureSink::GetCapture() == GetSafeHwnd() )
			CExtMouseCaptureSink::ReleaseCapture();
	}
	
bool bPrevSplitterMouseHover = m_bSplitterMouseHover;
	m_bSplitterMouseHover = 
		( nHitTest == __EPNWH_SPLITTER && IsSplitterEnabled() ) 
			? true
			: false;
	if(		bPrevSplitterMouseHover != m_bSplitterMouseHover 
		||	bPrevItemsMouseHover != m_bItemsMouseHover
		)
		SendMessage( WM_SETCURSOR );

	if( bNeedRedraw )
		Invalidate( FALSE );
	
	return true;
}

//////////////////////////////////////////////////////////////////////////

bool CExtPageNavigatorWnd::_ProcessMouseClick(
	CPoint point,
	bool bButtonPressed,
	INT nMouseButton // MK_... values
	)
{
	ASSERT_VALID( this );

	if( bButtonPressed )
		_ActivateOnClick();

	if(		nMouseButton == MK_RBUTTON 
		&&	!bButtonPressed
		)
	{
#if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)
#if (!defined __EXT_MFC_NO_CHECK_LIST)
		// show popup menu
		CExtPopupMenuWnd * pPopup = new CExtPopupMenuWnd;
		pPopup->SetOwner( this );
		if( pPopup->CreatePopupMenu( GetSafeHwnd() ) )
		{
			CExtCmdItem * pCmdItem =
				g_CmdManager->CmdGetPtr(
					g_CmdManager->ProfileNameFromWnd( m_hWnd ),
					IDC_EXT_PN_OPTIONS
					);
			if( pCmdItem == NULL )
				pCmdItem =
					g_CmdManager->CmdAllocPtr(
						g_CmdManager->ProfileNameFromWnd( m_hWnd ),
						IDC_EXT_PN_OPTIONS
						);
			ASSERT( pCmdItem != NULL );
			if( pCmdItem != NULL )
			{
				CExtSafeString sMenuText;
				if( !g_ResourceManager->LoadString( sMenuText, IDS_EXT_PN_OPTIONS ) )
					sMenuText = _T("Na&vigation Pane Options...");
				pCmdItem->m_sMenuText = 
					pCmdItem->m_sTipStatus = 
					pCmdItem->m_sTipTool = sMenuText;
				pCmdItem->StateSetBasic();

				VERIFY( pPopup->ItemInsert( IDC_EXT_PN_OPTIONS ) );
			}

			DWORD dwExStyle = ::GetWindowLong( m_hWnd, GWL_EXSTYLE );
			bool bRTL = ( (dwExStyle & WS_EX_LAYOUTRTL) != 0 ) ? true : false;

			UINT nTrackFlags = TPMX_NO_HIDE_RARELY;
			nTrackFlags |= bRTL ? TPMX_RIGHTALIGN : TPMX_LEFTALIGN;
							
			CPoint ptTrack( point );
			ClientToScreen( &ptTrack );
			CRect rcExclude( ptTrack, ptTrack );
			if( pPopup->TrackPopupMenu(
					nTrackFlags,
					ptTrack.x, 
					ptTrack.y,
					rcExclude,
					this,
					NULL,
					NULL,
					true
					)
				)
				return true;
		}
		else
		{
			ASSERT( FALSE );
			delete pPopup;
		}		
		return false;
#endif // #if (!defined __EXT_MFC_NO_CHECK_LIST)
#endif // #if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)
	}

	// process only left mouse button clicks
	if( nMouseButton != MK_LBUTTON )
		return false;

	if( (GetStyle() & WS_TABSTOP) != 0 )
		SetFocus();

	// cancel any kind of clicked mouse tracking
	m_bSplitterMouseTracking = false;
	if( CExtMouseCaptureSink::GetCapture() == GetSafeHwnd() )
		CExtMouseCaptureSink::ReleaseCapture();

HWND hWndOwn = GetSafeHwnd();
	ASSERT( hWndOwn != NULL	&& ::IsWindow(hWndOwn) );
				
LONG nHitTest = HitTest( point );
	switch( nHitTest )
	{
	case __EPNWH_ITEM_PAGE:
		{
			_ClearHoverFlag();
			_ClearPressedFlag();

			PAGE_ITEM_INFO * pPII = HitTestItem( point );
			if( pPII != NULL )
			{
				ITEM_PANE_INFO * pIPI = pPII->HitTestPane( point );
				if(		pIPI != NULL 
					&&	pIPI->ExpandableGet()
					)
				{
					CRect rcPaneCaption = pIPI->CaptionRectGet();
					if(		( !rcPaneCaption.IsRectEmpty() )
						&&	rcPaneCaption.PtInRect(point) 
						)
					{
						if( bButtonPressed )
						{
							// if left mouse button pressed
							if( CExtMouseCaptureSink::GetCapture() != GetSafeHwnd() )
								CExtMouseCaptureSink::SetCapture( GetSafeHwnd() );
							
							pPII->_ClearPressedFlag();
							pIPI->m_bPressed = true;
						}
						else
						{
							if(		pIPI->m_bPressed 
								&&	pIPI->ExpandableGet()
								)
								pIPI->Expand( ITEM_PANE_INFO::__ET_TOGGLE, true );
							pPII->_ClearPressedFlag();
							pPII->_ClearHoverFlag();
						}
					}
				}
				else
				{
					pPII->_ClearPressedFlag();
				}
				Invalidate();
			}
		}
		break;
	
	case __EPNWH_SPLITTER:
		{
			_ClearHoverFlag();
			_ClearPressedFlag();
			_ClearPanesHoverFlag();
			_ClearPanesPressedFlag();

			if(		bButtonPressed 
				&&	IsSplitterEnabled()
				)
			{
				if( CExtMouseCaptureSink::GetCapture() != GetSafeHwnd() )
					CExtMouseCaptureSink::SetCapture( GetSafeHwnd() );
				m_bSplitterMouseTracking = true;
			}
		}
		break;

	case __EPNWH_COLLAPSED_ITEMS_AREA:
	case __EPNWH_EXPANDED_ITEMS_AREA:
		{
			_ClearPanesPressedFlag();
			_ClearPanesHoverFlag();

			PAGE_ITEM_INFO * pPII = HitTestItem( point );
			if( pPII != NULL )
			{
				if( bButtonPressed )
				{
					// if left mouse button pressed
					if( CExtMouseCaptureSink::GetCapture() != GetSafeHwnd() )
						CExtMouseCaptureSink::SetCapture( GetSafeHwnd() );

					_ClearPressedFlag();
					pPII->m_bPressed = true;
					if( m_pCurrentItem != pPII )
						Invalidate();
				}
				else
				{
					if( pPII->m_bConfigButton )
					{
						// show config popup menu
						CExtPopupMenuWnd * pPopup = new CExtPopupMenuWnd;
						pPopup->SetOwner( this );
						if( pPopup->CreatePopupMenu( GetSafeHwnd() ) )
						{
							static struct
							{
								UINT m_nCmdID;
								UINT m_nMenuTextResourceID;
								LPCTSTR m_sMenuText;
								BYTE * m_pBitmapBits;
								LONG m_nBitmapBitsSize;
							} arrCmds[] =
							{
								{	IDC_EXT_PN_SHOW_MORE_BUTTONS, 
									IDS_EXT_PN_SHOW_MORE_BUTTONS, 
									_T("Show &More Buttons"),
									CExtPageNavigatorWnd::g_arrBmpDataShowMoreButtons,
									sizeof( CExtPageNavigatorWnd::g_arrBmpDataShowMoreButtons ) / sizeof( CExtPageNavigatorWnd::g_arrBmpDataShowMoreButtons[0] ),
								},
								{ 
									IDC_EXT_PN_SHOW_FEWER_BUTTONS, 
									IDS_EXT_PN_SHOW_FEWER_BUTTONS, 
									_T("Show &Fewer Buttons"),
									CExtPageNavigatorWnd::g_arrBmpDataShowFewerButtons,
									sizeof( CExtPageNavigatorWnd::g_arrBmpDataShowFewerButtons ) / sizeof( CExtPageNavigatorWnd::g_arrBmpDataShowFewerButtons[0] ),
								},
#if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)
#if (!defined __EXT_MFC_NO_CHECK_LIST)
								{ 
									IDC_EXT_PN_OPTIONS, 
									IDS_EXT_PN_OPTIONS, 
									_T("Na&vigation Pane Options..."),
									NULL,
									0,
								},
#endif // #if (!defined __EXT_MFC_NO_CHECK_LIST)
#endif // #if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)
								{ 
									IDC_EXT_PN_ADD_OR_REMOVE_BUTTONS, 
									IDS_EXT_PN_ADD_OR_REMOVE_BUTTONS, 
									_T("&Add or Remove Buttons"),
									NULL,
									0,
								}
							};

							for( int i = 0; i < sizeof(arrCmds)/sizeof(arrCmds[0]); i++ )
							{
								CExtCmdItem * pCmdItem =
									g_CmdManager->CmdGetPtr(
										g_CmdManager->ProfileNameFromWnd( m_hWnd ),
										arrCmds[i].m_nCmdID
										);
								if( pCmdItem == NULL )
									pCmdItem =
										g_CmdManager->CmdAllocPtr(
											g_CmdManager->ProfileNameFromWnd( m_hWnd ),
											arrCmds[i].m_nCmdID
											);
								ASSERT( pCmdItem != NULL );
								if( pCmdItem == NULL )
									continue;

								CExtSafeString sMenuText;
								if( !g_ResourceManager->LoadString( sMenuText, arrCmds[i].m_nMenuTextResourceID ) )
									sMenuText = arrCmds[i].m_sMenuText;
								pCmdItem->m_sMenuText = 
									pCmdItem->m_sTipStatus = 
									pCmdItem->m_sTipTool = sMenuText;
								pCmdItem->StateSetBasic();
								
								if( arrCmds[i].m_pBitmapBits != NULL )
								{
									CExtBitmap _bmp;
									_bmp.LoadBMP_Buffer(
										arrCmds[i].m_pBitmapBits, 
										arrCmds[i].m_nBitmapBitsSize
										);
									pCmdItem->m_pProfile->CmdSetIcon(
										arrCmds[i].m_nCmdID,
										_bmp, 
										RGB(255,0,255)
										);
								}

								VERIFY( pPopup->ItemInsert( arrCmds[i].m_nCmdID ) );

							} // for( int i = 0; i < sizeof(arrCmds)/sizeof(arrCmds[0]); i++ )

							CPoint ptTrack( 
								pPII->m_rcRect.right + 1, 
								pPII->m_rcRect.top + (pPII->m_rcRect.Width() / 2)
								);
							ClientToScreen( &ptTrack );
							
							pPII->m_bPressed = false;
							pPII->m_bHover = true;
							Invalidate( FALSE );

							DWORD dwExStyle = ::GetWindowLong( GetSafeHwnd(), GWL_EXSTYLE );
							bool bRTL = ( (dwExStyle & WS_EX_LAYOUTRTL) != 0 ) ? true : false;

							UINT nTrackFlags = TPMX_NO_HIDE_RARELY;
							nTrackFlags |= bRTL ? TPMX_RIGHTALIGN : TPMX_LEFTALIGN;
							
							CRect rcExclude( ptTrack, ptTrack );
							pPopup->TrackPopupMenu(
									nTrackFlags,
									ptTrack.x, 
									ptTrack.y,
									rcExclude,
									this,
									NULL,
									NULL,
									true
									);
						}
						else
						{
							ASSERT( FALSE );
							delete pPopup;
						}
					}
					else
					{
						if(		m_pCurrentItem != pPII
							&&	pPII->m_bPressed 
							)
							SelectionSet( pPII );
					}
					_ClearPressedFlag();
					UpdatePageNavigatorWnd( true );
				}
			}
			else
			{
				_ClearPressedFlag();
				Invalidate();
			}
		}
		break;

	default:
		_ClearHoverFlag();
		_ClearPressedFlag();
		_ClearPanesHoverFlag();
		_ClearPanesPressedFlag();
		Invalidate();
		return false; 
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////

CExtPageNavigatorWnd::PAGE_ITEM_INFO * CExtPageNavigatorWnd::ItemInsert(
	LONG nIndex, // = -1, 
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL, 
	HICON hIconExpanded, // = NULL,
	HICON hIconCollapsed, // = NULL,
	DWORD dwData, // = 0,
	bool bUpdate // = false
	)
{
	ASSERT_VALID( this );
CExtCmdIcon _iconExpanded;
CExtCmdIcon _iconCollapsed;
	if( hIconExpanded != NULL )
		_iconExpanded.AssignFromHICON( hIconExpanded, true );
	if( hIconCollapsed != NULL )
		_iconCollapsed.AssignFromHICON( hIconCollapsed, true );
	return 
		ItemInsert(
			nIndex,
			sText,
			(hIconExpanded == NULL) ? NULL : &_iconExpanded,
			(hIconCollapsed == NULL) ? NULL : &_iconCollapsed,
			dwData,
			bUpdate
			);
}

CExtPageNavigatorWnd::PAGE_ITEM_INFO * CExtPageNavigatorWnd::ItemInsert(
	LONG nIndex, // = -1, 
	__EXT_MFC_SAFE_LPCTSTR sText, // = NULL, 
	const CExtCmdIcon * pIconExpanded, // = NULL
	const CExtCmdIcon * pIconCollapsed, // = NULL
	DWORD dwData, // = 0,
	bool bUpdate // = false
	)
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if( nIndex < 0 || nIndex > nCount )
		nIndex = nCount;
PAGE_ITEM_INFO * pPII =
		new PAGE_ITEM_INFO(
			sText,
			pIconExpanded,
			pIconCollapsed,
			dwData,
			this
			);
	ASSERT_VALID( pPII );
	pPII->m_nIndexOriginal = nIndex;

	nIndex++;
	m_arrItems.InsertAt( nIndex, pPII );
	if(		ItemGetCount() == 1 
		&&	m_pCurrentItem == NULL
		)
		SelectionSet( pPII ); // m_pCurrentItem = pPII;
	UpdatePageNavigatorWnd( bUpdate );
	return pPII;
}

//////////////////////////////////////////////////////////////////////////

LONG CExtPageNavigatorWnd::_ItemRemoveAllImpl( 
	bool bRemoveExpandButton // = false 
	)
{	
LONG nCount = (LONG)m_arrItems.GetSize();
LONG nRetVal = nCount;
	if( nCount > 0 )
	{
		while( m_arrItems.GetSize() > 1 )
		{
			PAGE_ITEM_INFO * pPII = m_arrItems[0];
			if(	pPII->m_bConfigButton )
			{
				pPII = m_arrItems[1];
				m_arrItems.RemoveAt(1);
			}
			else
				m_arrItems.RemoveAt(0);
			delete pPII;
			pPII = NULL;
		}
		if( bRemoveExpandButton )
		{
			PAGE_ITEM_INFO * pPII = m_arrItems[0];
			m_arrItems.RemoveAt(0);
			delete pPII;
			pPII = NULL;
		}
	} // if( nCount > 0 )
	return nRetVal;
}

bool CExtPageNavigatorWnd::_ItemRemoveImpl(
	LONG nIndex
	)
{
	ASSERT_VALID( this );
LONG nCount = (LONG)m_arrItems.GetSize();
	if( nIndex < 0 || nIndex >= nCount )
	{
		ASSERT( FALSE );
		return false;
	}

PAGE_ITEM_INFO * pPII = _ItemGetInfoImpl( nIndex );
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
	ASSERT( pPII->m_bConfigButton == false );

	// if removed item is currently selected  - remove selection
	if( pPII == m_pCurrentItem )
		SelectionSet( (PAGE_ITEM_INFO*)NULL );
	
	m_arrItems.RemoveAt( nIndex );
	delete pPII;
	pPII = NULL;
	return true;
}

bool CExtPageNavigatorWnd::ItemRemove( 
	LONG nIndex,
	bool bUpdate // = false 
	)
{
	ASSERT_VALID( this );
	bool bRes = _ItemRemoveImpl( nIndex + 1 );
	UpdatePageNavigatorWnd( bUpdate );
	return bRes;
}

LONG CExtPageNavigatorWnd::ItemGetCount() const
{
	ASSERT_VALID( this );
	INT nCount = 0;
	for( INT i = 0; i < m_arrItems.GetSize(); i++ )
		if(	!m_arrItems[i]->m_bConfigButton ) 
			nCount++;
	return nCount;
}

INT CExtPageNavigatorWnd::ItemGetExpandedCount() const
{
	ASSERT_VALID( this );
	INT nCount = 0;
	for( INT i = 0; i < m_arrItems.GetSize(); i++ )
		if(		m_arrItems[i]->m_bExpanded 
			&&	m_arrItems[i]->m_bVisible
			&&	m_arrItems[i]->EnabledGet()
			&&(!m_arrItems[i]->m_bConfigButton)
			) 
			nCount++;
	return nCount;
}
INT CExtPageNavigatorWnd::ItemGetCollapsedCount() const
{
	ASSERT_VALID( this );
	INT nCount = 0;
	for( INT i = 0; i < m_arrItems.GetSize(); i++ )
		if(	  (!m_arrItems[i]->m_bExpanded) 
			&&	m_arrItems[i]->m_bVisible
			&&	m_arrItems[i]->EnabledGet()
			&&(!m_arrItems[i]->m_bConfigButton)
			) 
			nCount++;
	return nCount;
}

INT CExtPageNavigatorWnd::ItemGetVisibleCount() const
{
	ASSERT_VALID( this );
	INT nCount = 0;
	for( INT i = 0; i < m_arrItems.GetSize(); i++ )
		if(		m_arrItems[i]->m_bVisible 
			&&	m_arrItems[i]->EnabledGet()
			&&(!m_arrItems[i]->m_bConfigButton)
			) 
			nCount++;
	return nCount;
}

INT CExtPageNavigatorWnd::ItemGetEnabledCount() const
{
	ASSERT_VALID( this );
	INT nCount = 0;
	for( INT i = 0; i < m_arrItems.GetSize(); i++ )
		if(		m_arrItems[i]->EnabledGet() 
			&&(!m_arrItems[i]->m_bConfigButton)
			) 
			nCount++;
	return nCount;
}

void CExtPageNavigatorWnd::ItemSetExpandedCount( 
	INT nCount 
	)
{
	ASSERT_VALID( this );
	m_nExpandedItemsDesired = nCount;
	UpdatePageNavigatorWnd( true );
}

CExtPageNavigatorWnd::PAGE_ITEM_INFO * CExtPageNavigatorWnd::ItemGetPressed() const
{
	ASSERT_VALID( this );
PAGE_ITEM_INFO * pPII = NULL;
	for( INT i = 0; i < m_arrItems.GetSize(); i++ )
	{
		if(	m_arrItems[i]->m_bPressed ) 
		{
			pPII = m_arrItems[i];
			break;
		}
	}
	return pPII;
}

CExtPageNavigatorWnd::ITEM_PANE_INFO * CExtPageNavigatorWnd::ItemPaneGetPressed() const
{
	ASSERT_VALID( this );
	if( m_pCurrentItem != NULL )
	{
		for( LONG nIndex = 0; nIndex < m_pCurrentItem->PaneGetCount(); nIndex++ )
		{
			ITEM_PANE_INFO * pIPI = m_pCurrentItem->PaneGetInfo( nIndex );
			if(	pIPI->m_bPressed ) 
				return pIPI;
		}
	}
	return NULL;
}

bool CExtPageNavigatorWnd::ItemEnabledGet( 
	LONG nIndex
	) const
{
	ASSERT_VALID( this );
	if( nIndex < 0 || nIndex >= m_arrItems.GetSize() )
	{
		ASSERT( FALSE );
		return false;
	}
const PAGE_ITEM_INFO * pPII = ItemGetInfo( nIndex );
	if( pPII != NULL )
		return pPII->EnabledGet();
	return false;
}

bool CExtPageNavigatorWnd::ItemEnabledSet( 
	LONG nIndex,
	bool bEnabled, // = true
	bool bPersistent, // = true
	bool bUpdate // = false 
	)
{
	ASSERT_VALID( this );
	if( nIndex < 0 || nIndex >= m_arrItems.GetSize() )
	{
		ASSERT( FALSE );
		return false;
	}
PAGE_ITEM_INFO * pPII = ItemGetInfo( nIndex );
bool bEnabledOld = false;
	if( pPII != NULL )
		bEnabledOld = pPII->EnabledSet( bEnabled, bPersistent );
	UpdatePageNavigatorWnd( bUpdate );
	return bEnabledOld;
}

INT CExtPageNavigatorWnd::MinPageAreaHeightGet() const
{
	ASSERT_VALID( this );
	return m_nMinPageAreaHeight;
}

void CExtPageNavigatorWnd::MinPageAreaHeightSet( 
	INT nHeight 
	)
{
	ASSERT_VALID( this );
	m_nMinPageAreaHeight = nHeight;
	UpdatePageNavigatorWnd( true );
}

LONG CExtPageNavigatorWnd::ItemRemoveAll(
	bool bUpdate // = false
	)
{
	ASSERT_VALID( this );
LONG nRetVal =
		_ItemRemoveAllImpl( false );
	SelectionSet( ( PAGE_ITEM_INFO * ) NULL );
	ASSERT_VALID( this );
	UpdatePageNavigatorWnd( bUpdate );
	return nRetVal;
}

CExtPageNavigatorWnd::PAGE_ITEM_INFO * CExtPageNavigatorWnd::_ItemGetInfoImpl(
	LONG nIndex
	)
{
LONG nCount = (LONG)m_arrItems.GetSize();
	if( nIndex < 0 || nIndex >= nCount )
	{
		ASSERT( FALSE );
		return NULL;
	}
	return m_arrItems[ nIndex ];
}

CExtPageNavigatorWnd::PAGE_ITEM_INFO * CExtPageNavigatorWnd::ItemGetInfo(
	LONG nIndex
	)
{
	ASSERT_VALID( this );
	PAGE_ITEM_INFO * pPII = _ItemGetInfoImpl( nIndex + 1 );
	ASSERT( pPII != NULL );
	ASSERT_VALID( pPII );
	ASSERT( pPII->m_bConfigButton != true );
	return pPII;
}

const CExtPageNavigatorWnd::PAGE_ITEM_INFO * CExtPageNavigatorWnd::ItemGetInfo(
	LONG nIndex
	) const
{
	ASSERT_VALID( this );
PAGE_ITEM_INFO * ptr =
		( const_cast < CExtPageNavigatorWnd * > ( this ) )
			-> ItemGetInfo( nIndex );
	return ptr;
}

bool CExtPageNavigatorWnd::ItemSetInfo(
	LONG nIndex,
	PAGE_ITEM_INFO * pPII
	)
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if( nIndex < 0 || nIndex >= nCount || pPII == NULL )
	{
		ASSERT( FALSE );
		return false;
	}
	ASSERT_VALID( pPII );
	m_arrItems.SetAt( nIndex + 1, pPII );
	return true;
}

//////////////////////////////////////////////////////////////////////////

LONG CExtPageNavigatorWnd::ItemGetIndexOf( 
	const CExtPageNavigatorWnd::PAGE_ITEM_INFO * pPII 
	) const
{
	ASSERT_VALID( this );
LONG nIndex = -1;
	for( LONG i = 0; i < m_arrItems.GetSize(); i++ )
	{
		const CExtPageNavigatorWnd::PAGE_ITEM_INFO * pPIICurrent = 
			m_arrItems[i];
		ASSERT( pPIICurrent != NULL );
		ASSERT_VALID( pPIICurrent );
		if(	pPII == pPIICurrent )
		{
			nIndex = i;
			break;
		}
	}
	if( nIndex > 0 )
		nIndex--; // exclude config button
	return nIndex;
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::SelectionSet( 
	LONG nIndex 
	)
{
	ASSERT_VALID( this );
	if( (nIndex < 0 || nIndex >= m_arrItems.GetSize()) && nIndex != -1 )
	{
		ASSERT( FALSE );
		return;
	}
PAGE_ITEM_INFO * pPII = NULL;
	if( nIndex != -1 )
		pPII = ItemGetInfo( nIndex );
	SelectionSet( pPII );
}

//////////////////////////////////////////////////////////////////////////

void CExtPageNavigatorWnd::SelectionSet( 
	PAGE_ITEM_INFO * pPII 
	)
{
	ASSERT_VALID( this );

	_ClearPressedFlag();
	_ClearPanesPressedFlag();

LONG nPageOld = ItemGetIndexOf( m_pCurrentItem );
LONG nPageNew = ItemGetIndexOf( pPII );
HWND hWndNotificationReceiver =
		OnPageNavigatorGetNotificationReceiver();
	if( hWndNotificationReceiver != NULL )
	{
		SELECTION_NOTIFICATION _SN( 
			false,
			nPageOld, 
			nPageNew,
			m_lParamCookie
			);
		LRESULT lResult = _SN.Notify( hWndNotificationReceiver );
		if( lResult == -1 )
			return;
	} // if( hWndNotificationReceiver != NULL )
	if( m_pCurrentItem != NULL )
		m_pCurrentItem->Hide();
	m_pCurrentItem = pPII;
	if( m_pCurrentItem != NULL )
		m_pCurrentItem->Show();
	UpdatePageNavigatorWnd( true );
	if( hWndNotificationReceiver != NULL )
	{
		SELECTION_NOTIFICATION _SN( 
			true,
			nPageOld, 
			nPageNew,
			m_lParamCookie
			);
		_SN.Notify( hWndNotificationReceiver );
	} // if( hWndNotificationReceiver != NULL )
}

LONG CExtPageNavigatorWnd::_SelectionGetImpl() const
{
LONG nIndex = -1;
	for( LONG i = 0; i < m_arrItems.GetSize(); i++ )
	{
		if(		(! m_arrItems[i]->m_bConfigButton )
			&&	m_arrItems[i]->EnabledGet()
			&&	m_arrItems[i] == m_pCurrentItem
			)
		{
			nIndex = i;
			break;
		}
	}
	return nIndex;
}

LONG CExtPageNavigatorWnd::SelectionGet() const
{
	ASSERT_VALID( this );
	LONG nIndex = _SelectionGetImpl();
	return (nIndex - 1);
}

void CExtPageNavigatorWnd::PmBridge_OnPaintManagerChanged(
	CExtPaintManager * pGlobalPM
	)
{
LONG nIndex, nCount = ItemGetCount();
	for( nIndex = 0; nIndex < nCount; nIndex++ )
	{
		PAGE_ITEM_INFO * pPII = ItemGetInfo( nIndex );
		if( pPII != NULL )
		{
			CExtCmdIcon * pIconCollapsed = pPII->IconGetPtr( false );
			if( pIconCollapsed != NULL )
				pIconCollapsed->OnEmptyGeneratedBitmaps();
			CExtCmdIcon * pIconExpanded = pPII->IconGetPtr( true );
			if( pIconExpanded != NULL )
				pIconExpanded->OnEmptyGeneratedBitmaps();
		}
	}
	CExtPmBridge::PmBridge_OnPaintManagerChanged( pGlobalPM );
}


void CExtPageNavigatorWnd::_RecalcLayout()
{
	ASSERT_VALID( this );
	if(		m_bUpdatingLayout
		||	GetSafeHwnd() == NULL
		||	( ! ::IsWindow( GetSafeHwnd() ) )
		)
		return;

	m_bUpdatingLayout = true;

	m_rcPageNavigatorClient.SetRectEmpty();
	m_rcPageNavigatorInnerArea.SetRectEmpty();
	m_rcItemCaption.SetRectEmpty();
	m_rcCollapsedItemsArea.SetRectEmpty();
	m_rcExpandedItemsArea.SetRectEmpty();
	m_rcSplitter.SetRectEmpty();

INT nItemCaptionHeight = OnQueryItemCaptionHeight();
INT nItemHeight = OnQueryItemHeight();
INT nSplitterHeight = OnQuerySplitterHeight();
INT nCollapsedAreaHeight = OnQueryCollapsedAreaHeight();
LONG nItemsCount = ItemGetEnabledCount();
CSize szCollapsedItemSize = OnQueryCollapsedItemSize();
LONG nItem = 0;
CRect rcClient;
	GetClientRect( &rcClient );

	_ClearExpandedFlag();
	_ClearVisibleFlag();
	
	// clear items rect
	for( nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if( pPII != NULL )
			pPII->m_rcRect.SetRectEmpty();
	}

	//////////////////////////////////////////////////////////////////////////

	// expanded items
	if( nItemsCount > 0 )
	{
		INT nMaxExpandedItemsCount = _CalcMaxExpandedItemsCount();
		INT nExpandedItemsCount = 
			nMaxExpandedItemsCount > m_nExpandedItemsDesired
			? m_nExpandedItemsDesired
			: nMaxExpandedItemsCount;

		INT nSetExpandedOk = 0;
		for( nItem = 0; nItem < m_arrItems.GetSize() && nExpandedItemsCount > nSetExpandedOk; nItem ++ )
		{
			PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
			ASSERT( pPII != NULL );
			ASSERT_VALID( pPII );
			if(		pPII != NULL 
				&&	pPII->EnabledGet()
				&& !pPII->m_bConfigButton
				)
			{
				pPII->m_bExpanded = true;
				pPII->m_bVisible = true;
				nSetExpandedOk++;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

	// inner client area including border area
	m_rcPageNavigatorClient.CopyRect( &rcClient );

	// inner client area excluding border area
	m_rcPageNavigatorInnerArea.CopyRect( &m_rcPageNavigatorClient );
	m_rcPageNavigatorInnerArea.DeflateRect(1,1);
	
	// current item's caption area
	m_rcItemCaption.SetRect(
		m_rcPageNavigatorInnerArea.left,
		m_rcPageNavigatorInnerArea.top,
		m_rcPageNavigatorInnerArea.right,
		m_rcPageNavigatorInnerArea.top + nItemCaptionHeight
		);

	// collapsed items area
	m_rcCollapsedItemsArea.SetRect(
		m_rcPageNavigatorClient.left,
		m_rcPageNavigatorClient.bottom - nCollapsedAreaHeight,
		m_rcPageNavigatorClient.right,
		m_rcPageNavigatorClient.bottom
		);
	
	// expanded items area
	m_rcExpandedItemsArea.SetRect(
		m_rcPageNavigatorClient.left,
		m_rcCollapsedItemsArea.top - ItemGetExpandedCount()*nItemHeight,
		m_rcPageNavigatorClient.right,
		m_rcCollapsedItemsArea.top
		);

	// Splitter
	m_rcSplitter.SetRect(
		m_rcPageNavigatorInnerArea.left,
		m_rcExpandedItemsArea.top - nSplitterHeight,
		m_rcPageNavigatorInnerArea.right,
		m_rcExpandedItemsArea.top
		);

	// expanded items
INT nExpandedItems = 0;
	for( nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(		pPII != NULL 
			&&	pPII->m_bExpanded
			&& !pPII->m_bConfigButton
			)
		{
			pPII->m_bExpanded = true;
			pPII->m_rcRect.SetRect(
				m_rcExpandedItemsArea.left,
				m_rcExpandedItemsArea.top + nItemHeight*nExpandedItems,
				m_rcExpandedItemsArea.right,
				m_rcExpandedItemsArea.top + nItemHeight*nExpandedItems + nItemHeight + 1
				);
			nExpandedItems++;
		}
	}

	// collapsed items
	INT nMaxCollapsedItemsCount = 
		m_rcPageNavigatorInnerArea.Width() / szCollapsedItemSize.cx;
	nMaxCollapsedItemsCount--; // config button
	
	if(		nItemsCount > 0 
		&&	nMaxCollapsedItemsCount > 0
		)
	{
		INT nCollapsedItemsCount = 
			( nItemsCount - nExpandedItems ) < nMaxCollapsedItemsCount
			? ( nItemsCount - nExpandedItems )
			: nMaxCollapsedItemsCount;

		INT nCurItem = nExpandedItems;
		for(	nItem = nCurItem; 
					nItem < m_arrItems.GetSize()
				&&	nCurItem < ( nExpandedItems + nMaxCollapsedItemsCount ); 
				nItem ++ 
			)
		{
			PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
			ASSERT( pPII != NULL );
			ASSERT_VALID( pPII );
			if(		pPII != NULL 
				&&	pPII->EnabledGet()
				&& !pPII->m_bExpanded
				&& !pPII->m_bConfigButton
				)
			{
				pPII->m_bVisible = true;
				pPII->m_rcRect.SetRect(
					m_rcCollapsedItemsArea.right - 
						szCollapsedItemSize.cx * ( nCollapsedItemsCount - (nCurItem - nExpandedItems - 1)),
					m_rcCollapsedItemsArea.top,
					m_rcCollapsedItemsArea.right - 
						szCollapsedItemSize.cx * ( nCollapsedItemsCount - (nCurItem - nExpandedItems - 1) - 1),
					m_rcCollapsedItemsArea.top + szCollapsedItemSize.cy
					);
				pPII->m_rcRect.OffsetRect( -1, 1 );
				nCurItem ++;
			}
		}
	}

	// config button
	for( nItem = 0; nItem < m_arrItems.GetSize() && ItemGetCount() > 0 ; nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(		pPII != NULL 
			&&	pPII->m_bConfigButton
			)
		{
			pPII->m_bVisible = true;
			pPII->m_rcRect.SetRect(
				m_rcCollapsedItemsArea.right - szCollapsedItemSize.cx,
				m_rcCollapsedItemsArea.top,
				m_rcCollapsedItemsArea.right,
				m_rcCollapsedItemsArea.top + szCollapsedItemSize.cy
				);
			pPII->m_rcRect.OffsetRect( -1, 1 );
			break;
		}
	}

	// active page area
CRect rcActivePage;
	rcActivePage.SetRect(
		m_rcPageNavigatorInnerArea.left,
		m_rcItemCaption.bottom,
		m_rcPageNavigatorInnerArea.right,
		m_rcSplitter.top
		);
	if( rcActivePage.top > rcActivePage.bottom )
		rcActivePage.top = rcActivePage.bottom;
	for( nItem = 0; nItem < m_arrItems.GetSize(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = m_arrItems[ nItem ];
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(		pPII != NULL 
			&&	!pPII->m_bConfigButton
			)
			pPII->ActivePageRectSet( &rcActivePage );
	}
	
	// initialize tooltips
	if( m_wndToolTip.m_hWnd != NULL )
	{
		INT nToolCount = m_wndToolTip.GetToolCount();
		for( INT i = 1; i <= nToolCount; i++ )
			m_wndToolTip.DelTool( this, i );

		for( INT nItem = 1; nItem <= m_arrItems.GetSize(); nItem ++ )
		{
			PAGE_ITEM_INFO * pPII = m_arrItems[ nItem - 1 ];
			ASSERT( pPII != NULL );
			ASSERT_VALID( pPII );
			if(		pPII != NULL 
				&&	!pPII->m_bExpanded
				&&	pPII->m_bVisible
				)
				m_wndToolTip.AddTool(
					this,
					LPSTR_TEXTCALLBACK,
					pPII->m_rcRect,
					nItem
					);
		}		
		m_wndToolTip.Activate( TRUE );
	}

	m_bUpdatingLayout = false;	
}

//////////////////////////////////////////////////////////////////////////

BOOL CExtPageNavigatorWnd::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
	if( nCode == CN_UPDATE_COMMAND_UI )
	{
		CCmdUI * pCmdUI = (CCmdUI *)pExtra;
		ASSERT( pCmdUI != NULL );
		if( nID == IDC_EXT_PN_SHOW_FEWER_BUTTONS )
		{
			pCmdUI->Enable( 
				ItemGetExpandedCount() > 0
				);
			return TRUE;
		}
		else if( nID == IDC_EXT_PN_SHOW_MORE_BUTTONS )
		{
			pCmdUI->Enable( 
					ItemGetExpandedCount() < ItemGetCount()
				&&	ItemGetVisibleCount() > 0
				);
			return TRUE;
		}
		else if( nID == IDC_EXT_PN_OPTIONS )
		{
			pCmdUI->Enable( ItemGetCount() > 0 );
			return TRUE;
		}
	}
	else if( nCode == CN_COMMAND )
	{
		if( nID == IDC_EXT_PN_SHOW_FEWER_BUTTONS )
		{
			m_nExpandedItemsDesired--;
			UpdatePageNavigatorWnd( true );
			return TRUE;
		}
		else if( nID == IDC_EXT_PN_SHOW_MORE_BUTTONS )
		{
			m_nExpandedItemsDesired++;
			UpdatePageNavigatorWnd( true );
			return TRUE;
		}
		else if( nID == IDC_EXT_PN_OPTIONS )
		{
#if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)
#if (!defined __EXT_MFC_NO_CHECK_LIST)
			CExtPageNavigatorOptionsDlg dlg( IDD_EXT_PN_OPTIONS, this );
			if( dlg.DoModal() == IDOK )
			{
			}
			return TRUE;
#endif // #if (!defined __EXT_MFC_NO_CHECK_LIST)
#endif // #if (!defined __EXT_PROFUIS_STATIC_LINK || defined __EXT_PROFUIS_STATIC_LINK_WITH_RESOURCES)
		}
	}

	return CWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

int CExtPageNavigatorWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if( CWnd::OnCreate(lpCreateStruct) == -1 )
		return -1;
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		AfxThrowMemoryException();
	}
	return 0;
}

bool CExtPageNavigatorWnd::ItemMove( 
	LONG nIndex1,
	LONG nIndex2,
	bool bPersistent, // = true
	bool bUpdate // = false 
	)
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if(		nIndex1 < 0 || nIndex1 > nCount 
		||	nIndex2 < 0 || nIndex2 > nCount
		)
	{
		ASSERT( FALSE );
		return false;
	}
	if( nIndex1 == nIndex2 )
		return false;

PAGE_ITEM_INFO * pPII1 = ItemGetInfo( nIndex1 );
	ASSERT( pPII1 != NULL );
	ASSERT_VALID( pPII1 );

	m_arrItems.RemoveAt( nIndex1 + 1 );
	m_arrItems.InsertAt( nIndex2 + 1, pPII1 );

	if( bPersistent )
		pPII1->m_nIndexOriginal = nIndex2;

	UpdatePageNavigatorWnd( bUpdate );
	
	return true;
}

bool CExtPageNavigatorWnd::ItemsSwap( 
	LONG nIndex1,
	LONG nIndex2,
	bool bPersistent, // = true
	bool bUpdate // = false 
	)
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if(		nIndex1 < 0 || nIndex1 > nCount 
		||	nIndex2 < 0 || nIndex2 > nCount
		)
	{
		ASSERT( FALSE );
		return false;
	}
	if( nIndex1 == nIndex2 )
		return false;

PAGE_ITEM_INFO * pPII1 = m_arrItems[ nIndex1 + 1 ];
	ASSERT( pPII1 != NULL );
	ASSERT_VALID( pPII1 );
PAGE_ITEM_INFO * pPII2 = m_arrItems[ nIndex2 + 1 ];
	ASSERT( pPII2 != NULL );
	ASSERT_VALID( pPII2 );

	m_arrItems.SetAt( nIndex1 + 1, pPII2 );
	m_arrItems.SetAt( nIndex2 + 1, pPII1 );

	if( bPersistent )
	{
		pPII1->m_nIndexOriginal = nIndex2 + 1;
		pPII2->m_nIndexOriginal = nIndex1 + 1;
	}

	UpdatePageNavigatorWnd( bUpdate );
	
	return true;
}

LONG CExtPageNavigatorWnd::ItemFind(
	LPARAM lParam,
	LONG nIndexStart // = -1
	) const
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if(		(nIndexStart < 0 || nIndexStart >= nCount)
		&&	nIndexStart != -1
		)
	{
		ASSERT( FALSE );
		return -1;
	}
	LONG nItemsProcessed = 0;
	LONG nItem = nIndexStart;
	while( nItemsProcessed < nCount )
	{
		if( nItem == -1 )
			nItem = 0;
		else if( nItem < nCount - 1 )
			nItem++;
		else
			nItem = 0;

		const PAGE_ITEM_INFO * pPII = ItemGetInfo( nItem );
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(		pPII != NULL 
			&&	pPII->LParamGet() == lParam
			)
			return nItem;
	
		nItemsProcessed++;
	}
	return -1;
}

LONG CExtPageNavigatorWnd::ItemFind(
	__EXT_MFC_SAFE_LPCTSTR sText,
	LONG nIndexStart // = -1
	) const
{
	ASSERT_VALID( this );
LONG nCount = ItemGetCount();
	if(		(nIndexStart < 0 || nIndexStart >= nCount)
		&&	nIndexStart != -1
		)
	{
		ASSERT( FALSE );
		return -1;
	}
	LONG nItemsProcessed = 0;
	LONG nItem = nIndexStart;
	while( nItemsProcessed < nCount )
	{
		if( nItem == -1 )
			nItem = 0;
		else if( nItem < nCount - 1 )
			nItem++;
		else
			nItem = 0;

		const PAGE_ITEM_INFO * pPII = ItemGetInfo( nItem );
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if(		pPII != NULL 
			&&	_tcscmp( pPII->TextGet(), sText ) == 0
			)
			return nItem;
	
		nItemsProcessed++;
	}
	return -1;
}

#define __REG_VAR_ITEMS_EXPANDED_COUNT	_T("ItemsExpandedCount")
#define __REG_VAR_ITEMS_ORDER			_T("ItemsOrder")
#define __REG_VAR_ITEMS_PROPERTIES		_T("ItemsProperties")

bool CExtPageNavigatorWnd::StateLoad(
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProfile,
	bool bPersistent, // = true
	HKEY hKeyRoot // = HKEY_CURRENT_USER
	)
{
	ASSERT_VALID( this );
	ASSERT( sSectionNameCompany != NULL );
	ASSERT( sSectionNameProduct != NULL );
	ASSERT( sSectionNameProfile != NULL );

CExtSafeString sRegKeyPath;
	sRegKeyPath.Format(
		_T("Software\\%s\\%s\\%s\\%s"),
		sSectionNameCompany,
		sSectionNameProduct,
		__PROF_UIS_REG_SECTION,
		sSectionNameProfile
		);

	try
	{
		CExtRegistry reg;
		if( !reg.Create(
				hKeyRoot,
				sRegKeyPath,
				KEY_READ
				)
			)
			return false;

		// load expanded items count
		DWORD nExpandedCount = 0;
		if(	!reg.LoadNumber(
				__REG_VAR_ITEMS_EXPANDED_COUNT,
				&nExpandedCount
				)
			)
			return false;
		ItemSetExpandedCount( nExpandedCount );

		// load items order
		{
			LPBYTE lpbData = NULL;
			DWORD dwDataSize;
			if(	!reg.LoadNewBinary(
					__REG_VAR_ITEMS_ORDER,
					&lpbData,
					&dwDataSize
					)
				)
				return false;

			CMemFile _file( lpbData, dwDataSize );
			CArchive ar(
				&_file, 
				CArchive::load
				);
			CDWordArray arrItemsOrder;
			arrItemsOrder.Serialize( ar );
			if( arrItemsOrder.GetSize() != ItemGetCount() )
			{
				ASSERT( FALSE );
				delete[] lpbData;
				return false;
			}

			PageItemsArr_t arrItemsOld;
			LONG nItem;
			for( nItem = 0; nItem < ItemGetCount(); nItem ++ )
			{
				PAGE_ITEM_INFO * pPII = ItemGetInfo( nItem );
				ASSERT( pPII != NULL );
				ASSERT_VALID( pPII );
				if( pPII != NULL )
					arrItemsOld.Add( pPII );
			}
			for( nItem = 0; nItem < arrItemsOrder.GetSize(); nItem++ )
			{
				LONG nSavedIndex = arrItemsOrder[ nItem ];
				if( bPersistent )
					arrItemsOld[ nSavedIndex ]->m_nIndexOriginal = nItem;
				ItemSetInfo(
					nItem,
					arrItemsOld[ nSavedIndex ] 
					);
			}
			arrItemsOld.RemoveAll();

			delete[] lpbData;
			lpbData = NULL;
			ar.Close();
		}

		// load items enabled status
		{
			LPBYTE lpbData = NULL;
			DWORD dwDataSize;
			if(	!reg.LoadNewBinary(
					__REG_VAR_ITEMS_PROPERTIES,
					&lpbData,
					&dwDataSize
					)
				)
				return false;

			CMemFile _file( lpbData, dwDataSize );
			CArchive ar(
				&_file, 
				CArchive::load
				);

			for( LONG nItem = 0; nItem < ItemGetCount(); nItem++ )
			{
				PAGE_ITEM_INFO * pPII = ItemGetInfo( nItem );
				ASSERT( pPII != NULL );
				ASSERT_VALID( pPII );
				if( pPII != NULL )
					pPII->Serialize( ar, bPersistent );
			}

			delete[] lpbData;
			lpbData = NULL;
			ar.Close();
		}

		reg.Close();

		return true;
	} // try
	catch( CException * pXept )
	{
		pXept->Delete();
		ASSERT( FALSE );
	} // catch( CException * pXept )
	catch( ... )
	{
		ASSERT( FALSE );
	} // catch( ... )
	return false;
}

bool CExtPageNavigatorWnd::StateSave(
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProfile,
	HKEY hKeyRoot // = HKEY_CURRENT_USER
	)
{
	ASSERT_VALID( this );
	ASSERT( sSectionNameCompany != NULL );
	ASSERT( sSectionNameProduct != NULL );
	ASSERT( sSectionNameProfile != NULL );

CExtSafeString sRegKeyPath;
	sRegKeyPath.Format(
		_T("Software\\%s\\%s\\%s\\%s"),
		sSectionNameCompany,
		sSectionNameProduct,
		__PROF_UIS_REG_SECTION,
		sSectionNameProfile
		);

	try
	{
		CExtRegistry reg;
		if( !reg.Create(
				hKeyRoot,
				sRegKeyPath,
				KEY_WRITE
				)
			)
			return false;

		// save expanded items count
		INT nExpandedCount = ItemGetExpandedCount();
		if(	!reg.SaveNumber(
				__REG_VAR_ITEMS_EXPANDED_COUNT,
				nExpandedCount
				)
			)
			return false;

		// save items order
		{
			CDWordArray arrItemsOrder;
			for( LONG nItem = 0; nItem < ItemGetCount(); nItem ++ )
			{
				PAGE_ITEM_INFO * pPII = ItemGetInfo( nItem );
				ASSERT( pPII != NULL );
				ASSERT_VALID( pPII );
				if( pPII != NULL )
					arrItemsOrder.Add( pPII->m_nIndexOriginal );
			}

			CMemFile _file;
			CArchive ar(
				&_file,
				CArchive::store
				);
			arrItemsOrder.Serialize( ar );
			ar.Flush();
			ar.Close();

			DWORD dwDataSize = (DWORD)_file.GetLength();
			LPBYTE lpbData = _file.Detach();
			ASSERT( lpbData != NULL );
			if(	!reg.SaveBinary(
					__REG_VAR_ITEMS_ORDER,
					lpbData,
					dwDataSize
					)
				)
				return false;
			free( lpbData );
			_file.Close();
		}

		// save items properties
		{
			CMemFile _file;
			CArchive ar(
				&_file,
				CArchive::store
				);

			for( LONG nItem = 0; nItem < ItemGetCount(); nItem ++ )
			{
				PAGE_ITEM_INFO * pPII = ItemGetInfo( nItem );
				ASSERT( pPII != NULL );
				ASSERT_VALID( pPII );
				if( pPII != NULL )
					pPII->Serialize( ar );
			}
			ar.Flush();
			ar.Close();

			DWORD dwDataSize = (DWORD)_file.GetLength();
			LPBYTE lpbData = _file.Detach();
			ASSERT( lpbData != NULL );
			if(	!reg.SaveBinary(
					__REG_VAR_ITEMS_PROPERTIES,
					lpbData,
					dwDataSize
					)
				)
				return false;

			free( lpbData );
			_file.Close();
		}

		reg.Close();

		return true;
	} // try
	catch( CException * pXept )
	{
		pXept->Delete();
		ASSERT( FALSE );
	} // catch( CException * pXept )
	catch( ... )
	{
		ASSERT( FALSE );
	} // catch( ... )
	return false;
}

void CExtPageNavigatorWnd::StateReset(
	bool bPersistent // = true
	)
{
	ASSERT_VALID( this );

	// reset items order
	bool bStopSorting = true;
	do
	{
		bStopSorting = true;
		for( LONG nItem = 0; nItem < ItemGetCount() - 1; nItem ++ )
		{
			PAGE_ITEM_INFO * pPII1 = ItemGetInfo( nItem );
			ASSERT( pPII1 != NULL );
			ASSERT_VALID( pPII1 );

			PAGE_ITEM_INFO * pPII2 = ItemGetInfo( nItem + 1 );
			ASSERT( pPII2 != NULL );
			ASSERT_VALID( pPII2 );
		
			if( pPII1->m_nIndexOriginal > pPII2->m_nIndexOriginal )
			{
				ItemMove( nItem, nItem + 1, false );
				bStopSorting = false;
				break;
			}
		}
	} 
	while( !bStopSorting );
	if( bPersistent )
	{
		for( LONG nItem = 0; nItem < ItemGetCount(); nItem ++ )
		{
			PAGE_ITEM_INFO * pPII = ItemGetInfo( nItem );
			ASSERT( pPII != NULL );
			ASSERT_VALID( pPII );
			pPII->m_nIndexOriginal = nItem;
		}
	}

	// reset items enable status
	for( LONG nItem = 0; nItem < ItemGetCount(); nItem ++ )
	{
		PAGE_ITEM_INFO * pPII = ItemGetInfo( nItem );
		ASSERT( pPII != NULL );
		ASSERT_VALID( pPII );
		if( pPII != NULL )
		{
			if( pPII->EnabledGet() != pPII->m_bEnabledOriginal )
				pPII->EnabledSet( pPII->m_bEnabledOriginal, bPersistent );
		}
	}

	UpdatePageNavigatorWnd( true );
}

bool CExtPageNavigatorWnd::ShowSplitter( 
	bool bShow, // = true 
	bool bUpdate // = false
	)
{
	ASSERT_VALID( this );
bool bSplitterVisibleOld = m_bSplitterVisible;
	m_bSplitterVisible = bShow;
	UpdatePageNavigatorWnd( bUpdate );
	return bSplitterVisibleOld;
}

bool CExtPageNavigatorWnd::IsSplitterVisible() const
{
	ASSERT_VALID( this );
	return m_bSplitterVisible;
}

bool CExtPageNavigatorWnd::EnableSplitter( 
	bool bEnabled, // = true 
	bool bUpdate // = false
	)
{
	ASSERT_VALID( this );
bool bSplitterEnabledOld = m_bSplitterEnabled;
	m_bSplitterEnabled = bEnabled;
	UpdatePageNavigatorWnd( bUpdate );
	return bSplitterEnabledOld;
}

bool CExtPageNavigatorWnd::IsSplitterEnabled() const
{
	ASSERT_VALID( this );
	return m_bSplitterEnabled;
}

#endif // (!defined __EXT_MFC_NO_PAGE_NAVIGATOR)

