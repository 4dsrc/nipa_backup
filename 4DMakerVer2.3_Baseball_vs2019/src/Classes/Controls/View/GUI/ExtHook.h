// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#if (!defined __EXT_HOOK_H)
#define __EXT_HOOK_H

#if (!defined __EXT_MFC_DEF_H)
	#include <ExtMfcDef.h>
#endif // __EXT_MFC_DEF_H

class __PROF_UIS_API CExtHookSink
{
public:

	struct HookSinkArray_t;
	struct HookChains_t;
	friend struct HookChains_t;


	bool m_bEnableDetailedWndHooks:1, m_bEatNcDestroy:1;

	typedef
		CList < HWND, HWND >
		HookedWndList_t;

	CExtHookSink(
		bool bEnableDetailedWndHooks = true
		);
	virtual ~CExtHookSink();

	virtual bool IsAutoDeleteHookWndSink();

	virtual LRESULT OnHookWndMsgNextProcCurrent(
		WPARAM wParam,
		LPARAM lParam
		);
	virtual LRESULT OnHookWndMsgNextProcInvoke(
		UINT nMessage,
		WPARAM wParam,
		LPARAM lParam
		);
	virtual LRESULT OnHookWndMsgDefault();
	virtual bool OnHookWndMsg(
		LRESULT & lResult,
		HWND hWndHooked,
		UINT nMessage,
		WPARAM & wParam,
		LPARAM & lParam
		);
	virtual bool OnHookCmdMsg(
		LRESULT & lResult,
		HWND hWndHooked,
		WORD wNotifyCode,
		WORD wID,
		HWND hWndCtrl
		);
	virtual bool OnHookNotifyMsg(
		LRESULT & lResult,
		HWND hWndHooked,
		INT nIdCtrl,
		LPNMHDR lpnmhdr
		);
	virtual bool OnHookPaintMsg(
		LRESULT & lResult,
		HWND hWndHooked,
		HDC hDC
		);
	virtual bool OnHookEraseBackgroundMsg(
		LRESULT & lResult,
		HWND hWndHooked,
		HDC hDC
		);
	virtual bool OnHookPrintMsg(
		LRESULT & lResult,
		HWND hWndHooked,
		HDC hDC
		);
	virtual bool OnHookNcPaintMsg(
		LRESULT & lResult,
		HWND hWndHooked,
		HRGN hRgnUpdate
		);

	virtual void OnHookWndNcDestroy();

	virtual void OnHookWndAttach( HWND hWnd );
	virtual void OnHookWndDetach( HWND hWnd );

	virtual bool SetupHookWndSink(
		HWND hWnd,
		bool bRemove = false,
		bool bAddToHead = false
		);

	ULONG SetupHookWndSinkToChilds(
		HWND hWnd,
		UINT * pDlgCtrlIDs = NULL,
		ULONG nCountOfDlgCtrlIDs = 0,
		bool bDeep = false
		);

	virtual void RemoveAllWndHooks();

	void GetHookedWindows( HookedWndList_t & _list );
	bool IsHookedWindow( HWND hWnd );

}; // class CExtHookSink

class __PROF_UIS_API CExtHookSpy
{
	static CTypedPtrList < CPtrList, CExtHookSpy * > g_listListeners;
	static HHOOK g_hHookMouse;
	static HHOOK g_hHookKeyboard;
	static bool g_bListenersChanged;
	static void SHS_Hook( bool bHook );
	static LRESULT CALLBACK SHS_HookMouseProc(
		int nCode,      // hook code
		WPARAM wParam,  // message identifier
		LPARAM lParam   // mouse coordinates
		);
	static LRESULT CALLBACK SHS_HookKeyboardProc(
		int nCode,      // hook code
		WPARAM wParam,  // virtual-key code
		LPARAM lParam   // keystroke-message information
		);
public:
	CExtHookSpy(
		bool bAutoRegister = false
		);
	virtual ~CExtHookSpy();
	void HookSpyRegister();
	static void HookSpyRegister( CExtHookSpy * pHS );
	void HookSpyUnregister();
	static void HookSpyUnregister( CExtHookSpy * pHS );
	bool HookSpyIsRegistered() const;
	static bool HookSpyIsRegistered( const CExtHookSpy * pHS );
protected:
	virtual bool HSLL_OnMouseWheel(
		WPARAM wParam,
		LPARAM lParam
		);
	virtual bool HSLL_OnMouseMove(
		HWND hWnd,
		UINT nFlags,
		CPoint point
		);
	virtual bool HSLL_OnMouseClick(
		HWND hWnd,
		UINT nMessage,
		UINT nFlags,
		CPoint point
		);
	virtual bool HSLL_OnKeyDown(
		UINT nChar,
		UINT nRepCnt,
		UINT nFlags
		);
	virtual bool HSLL_PreTranslateMessage(
		MSG * pMSG
		);
public:
	virtual bool OnHookSpyPreTranslateMessage(
		MSG * pMSG
		);
	virtual bool OnHookSpyPostProcessMessage(
		MSG * pMSG
		);
	virtual bool OnHookSpyMouseWheelMsg(
		MSG * pMSG
		);
	virtual bool OnHookSpyMouseMoveMsg(
		MSG * pMSG
		);
	virtual bool OnHookSpyMouseClickMsg(
		MSG * pMSG
		);
	virtual bool OnHookSpyKeyMsg(
		MSG * pMSG
		);
};

#endif // __EXT_HOOK_H
