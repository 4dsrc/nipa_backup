// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __AFXPRIV_H__)
	#include <AfxPriv.h>
#endif 

#if (!defined __EXT_TABBEDTOOLCONTROLBAR_H)
	#include <ExtTabbedToolControlBar.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if (!defined __EXT_MFC_NO_TABBED_TOOLBAR)

/////////////////////////////////////////////////////////////////////////////
// CExtTabbedToolControlBar window

IMPLEMENT_DYNCREATE( CExtTabbedToolControlBar, CExtToolControlBar );

CExtTabbedToolControlBar::CExtTabbedToolControlBar()
	: m_pWndTabPageContainer( NULL )
	, m_bChangingDockState( false )
	, m_dwTabsOrientationAtLeft( __ETWS_ORIENT_LEFT )
	, m_dwTabsOrientationAtRight( __ETWS_ORIENT_RIGHT )
	, m_dwTabsOrientationAtTop( __ETWS_ORIENT_TOP )
	, m_dwTabsOrientationAtBottom( __ETWS_ORIENT_BOTTOM )
	, m_dwTabsOrientationFloating( __ETWS_ORIENT_TOP )
	, m_nFloatingWidth( 500 )
{
	m_bForceBalloonGradientInDialogs = false;
	m_bForceNoBalloonWhenRedockable = false;
	m_bPaletteMode = true;
}

CExtTabbedToolControlBar::~CExtTabbedToolControlBar()
{
	if( m_pWndTabPageContainer != NULL )
		delete m_pWndTabPageContainer;
}

CExtTabPageContainerWnd * CExtTabbedToolControlBar::GetTabPageContainer()
{
	ASSERT_VALID( this );
	if(		GetSafeHwnd() == NULL
		||	m_pWndTabPageContainer->GetSafeHwnd() == NULL
		)
		return NULL;
	return m_pWndTabPageContainer;
}

const CExtTabPageContainerWnd * CExtTabbedToolControlBar::GetTabPageContainer() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTabbedToolControlBar * > ( this ) )
			-> GetTabPageContainer();
}

DWORD CExtTabbedToolControlBar::TabsOrientationAtLeftGet()
{
	ASSERT_VALID( this );
	ASSERT(
			m_dwTabsOrientationAtLeft == __ETWS_ORIENT_LEFT
		||	m_dwTabsOrientationAtLeft == __ETWS_ORIENT_RIGHT
		);
	return m_dwTabsOrientationAtLeft;
}

void CExtTabbedToolControlBar::TabsOrientationAtLeftSet( DWORD dwOrientation )
{
	ASSERT_VALID( this );
	ASSERT(
			dwOrientation == __ETWS_ORIENT_LEFT
		||	dwOrientation == __ETWS_ORIENT_RIGHT
		);
	m_dwTabsOrientationAtLeft = dwOrientation;
}

DWORD CExtTabbedToolControlBar::TabsOrientationAtRightGet()
{
	ASSERT_VALID( this );
	ASSERT(
			m_dwTabsOrientationAtRight == __ETWS_ORIENT_LEFT
		||	m_dwTabsOrientationAtRight == __ETWS_ORIENT_RIGHT
		);
	return m_dwTabsOrientationAtRight;
}

void CExtTabbedToolControlBar::TabsOrientationAtRightSet( DWORD dwOrientation )
{
	ASSERT_VALID( this );
	ASSERT(
			dwOrientation == __ETWS_ORIENT_LEFT
		||	dwOrientation == __ETWS_ORIENT_RIGHT
		);
	m_dwTabsOrientationAtRight = dwOrientation;
}

DWORD CExtTabbedToolControlBar::TabsOrientationAtTopGet()
{
	ASSERT_VALID( this );
	ASSERT(
			m_dwTabsOrientationAtTop == __ETWS_ORIENT_TOP
		||	m_dwTabsOrientationAtTop == __ETWS_ORIENT_BOTTOM
		);
	return m_dwTabsOrientationAtTop;
}

void CExtTabbedToolControlBar::TabsOrientationAtTopSet( DWORD dwOrientation )
{
	ASSERT_VALID( this );
	ASSERT(
			dwOrientation == __ETWS_ORIENT_TOP
		||	dwOrientation == __ETWS_ORIENT_BOTTOM
		);
	m_dwTabsOrientationAtTop = dwOrientation;
}

DWORD CExtTabbedToolControlBar::TabsOrientationAtBottomGet()
{
	ASSERT_VALID( this );
	ASSERT(
			m_dwTabsOrientationAtBottom == __ETWS_ORIENT_TOP
		||	m_dwTabsOrientationAtBottom == __ETWS_ORIENT_BOTTOM
		);
	return m_dwTabsOrientationAtBottom;
}

void CExtTabbedToolControlBar::TabsOrientationAtBottomSet( DWORD dwOrientation )
{
	ASSERT_VALID( this );
	ASSERT(
			dwOrientation == __ETWS_ORIENT_TOP
		||	dwOrientation == __ETWS_ORIENT_BOTTOM
		);
	m_dwTabsOrientationAtBottom = dwOrientation;
}

DWORD CExtTabbedToolControlBar::TabsOrientationFloatingGet()
{
	ASSERT_VALID( this );
	ASSERT(
			m_dwTabsOrientationFloating == __ETWS_ORIENT_TOP
		||	m_dwTabsOrientationFloating == __ETWS_ORIENT_BOTTOM
		);
	return m_dwTabsOrientationFloating;
}

void CExtTabbedToolControlBar::TabsOrientationFloatingSet( DWORD dwOrientation )
{
	ASSERT_VALID( this );
	ASSERT(
			dwOrientation == __ETWS_ORIENT_TOP
		||	dwOrientation == __ETWS_ORIENT_BOTTOM
		);
	m_dwTabsOrientationFloating = dwOrientation;
}

void CExtTabbedToolControlBar::TabsOrientationSetDockedOuter()
{
	ASSERT_VALID( this );
	m_dwTabsOrientationAtLeft = __ETWS_ORIENT_LEFT;
	m_dwTabsOrientationAtRight = __ETWS_ORIENT_RIGHT;
	m_dwTabsOrientationAtTop = __ETWS_ORIENT_TOP;
	m_dwTabsOrientationAtBottom = __ETWS_ORIENT_BOTTOM;
}

void CExtTabbedToolControlBar::TabsOrientationSetDockedInner()
{
	ASSERT_VALID( this );
	m_dwTabsOrientationAtLeft = __ETWS_ORIENT_RIGHT;
	m_dwTabsOrientationAtRight = __ETWS_ORIENT_LEFT;
	m_dwTabsOrientationAtTop = __ETWS_ORIENT_BOTTOM;
	m_dwTabsOrientationAtBottom = __ETWS_ORIENT_TOP;
}

void CExtTabbedToolControlBar::TabsOrientationSetDockedLeftTop()
{
	ASSERT_VALID( this );
	m_dwTabsOrientationAtLeft = __ETWS_ORIENT_LEFT;
	m_dwTabsOrientationAtRight = __ETWS_ORIENT_LEFT;
	m_dwTabsOrientationAtTop = __ETWS_ORIENT_TOP;
	m_dwTabsOrientationAtBottom = __ETWS_ORIENT_TOP;
}

void CExtTabbedToolControlBar::TabsOrientationSetDockedRightBottom()
{
	ASSERT_VALID( this );
	m_dwTabsOrientationAtLeft = __ETWS_ORIENT_RIGHT;
	m_dwTabsOrientationAtRight = __ETWS_ORIENT_RIGHT;
	m_dwTabsOrientationAtTop = __ETWS_ORIENT_BOTTOM;
	m_dwTabsOrientationAtBottom = __ETWS_ORIENT_BOTTOM;
}

CExtToolControlBar * CExtTabbedToolControlBar::BarInsert(
	__EXT_MFC_SAFE_LPCTSTR strCaption,
	CWnd * pToolBarOwnerWnd, // = NULL
	INT nTabIndex, // = -1
	HICON hIcon, // = NULL
	bool bCopyIcon, // = true
	bool bSelect // = false
	)
{
	ASSERT_VALID( this );
CExtCmdIcon icon;
	if( hIcon != NULL )
		icon.AssignFromHICON( hIcon, bCopyIcon );
	return
		BarInsert(
			strCaption,
			icon,
			pToolBarOwnerWnd,
			nTabIndex,
			bSelect
			);
}

CExtToolControlBar * CExtTabbedToolControlBar::BarInsert(
	__EXT_MFC_SAFE_LPCTSTR strCaption,
	const CExtCmdIcon & icon,
	CWnd * pToolBarOwnerWnd, // = NULL
	INT nTabIndex, // = -1
	bool bSelect // = false
	)
{
	ASSERT_VALID( this );
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	if( pWndTabPageContainer == NULL )
	{
		ASSERT( FALSE );
		return NULL;
	}
int nCountOfBars = BarGetCount();
	if( nTabIndex < 0 || nTabIndex > nCountOfBars )
		nTabIndex = nCountOfBars;
CExtToolControlBar * pToolBar =
		OnBarCreate(
			strCaption,
			pToolBarOwnerWnd
			);
	if( pToolBar == NULL )
	{
		ASSERT( FALSE );
		return NULL;
	}
CExtTabPageContainerOneNoteWnd * pTabPageContainerOneNoteWnd =
		DYNAMIC_DOWNCAST(
			CExtTabPageContainerOneNoteWnd,
			pWndTabPageContainer
			);
	if( pTabPageContainerOneNoteWnd != NULL )
	{
		VERIFY(
			pTabPageContainerOneNoteWnd->PageInsert(
				pToolBar,
				icon,
				( strCaption == NULL ) ? _T("") : strCaption,
				nTabIndex,
				bSelect
				)
			);
		pTabPageContainerOneNoteWnd->PageCenterTextSet( 
			nTabIndex,
			icon.IsEmpty() ? true : false,
			true
			);
	}
	else
	{
		VERIFY(
			pWndTabPageContainer->PageInsert(
				pToolBar,
				icon,
				( strCaption == NULL ) ? _T("") : strCaption,
				nTabIndex,
				bSelect
				)
			);
		pWndTabPageContainer->PageCenterTextSet( 
			nTabIndex,
			icon.IsEmpty() ? true : false,
			true
			);
	}
	pWndTabPageContainer->GetSafeTabWindow()
		->ItemEnsureVisible( nTabIndex );
	OnBarInserted( nTabIndex );
	return pToolBar;
}

INT CExtTabbedToolControlBar::BarGetCount() const
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return 0;
const CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	if( pWndTabPageContainer == NULL )
		return NULL;
INT nBarCount = pWndTabPageContainer->PageGetCount();
	return nBarCount;
}

CExtToolControlBar * CExtTabbedToolControlBar::BarGetAt( INT nTabIndex )
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL || nTabIndex < 0 || nTabIndex >= BarGetCount() )
		return NULL;
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	if( pWndTabPageContainer == NULL )
		return NULL;
CExtToolControlBar * pToolBar =
		STATIC_DOWNCAST(
			CExtToolControlBar,
			pWndTabPageContainer->PagePermanentWndGet(
				nTabIndex
				)
			);
	ASSERT_VALID( pToolBar );
	ASSERT( pToolBar->GetSafeHwnd() != NULL );
	return pToolBar;
}

const CExtToolControlBar * CExtTabbedToolControlBar::BarGetAt( INT nTabIndex ) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtTabbedToolControlBar * > ( this ) )
			-> BarGetAt( nTabIndex );
}

INT CExtTabbedToolControlBar::BarGetIndexOf(
	const CExtToolControlBar * pToolBar
	) const
{
	ASSERT_VALID( this );
	if(		pToolBar == NULL
		||	GetSafeHwnd() == NULL
		)
		return 0;
	ASSERT_VALID( this );
	if( pToolBar->GetSafeHwnd() == NULL )
		return 0;
INT nTabIndex, nTabCount = BarGetCount();
	for( nTabIndex = 0; nTabIndex < nTabCount; nTabIndex ++ )
	{
		const CExtToolControlBar * pToolBar2 =
			BarGetAt( nTabIndex );
		ASSERT_VALID( pToolBar2 );
		if( LPCVOID(pToolBar) == LPCVOID(pToolBar2) )
			return nTabIndex;
	}
	return -1;
}

INT CExtTabbedToolControlBar::BarRemove(
	INT nTabIndex,
	INT nCountToRemove, // = 1
	bool bRecalcLayout // = true
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return 0;
	if( nCountToRemove == 0 || nTabIndex < 0 )
		return 0;
INT nBarCount = BarGetCount();
	if( nBarCount == 0 || nTabIndex >= nBarCount )
		return 0;
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	if( pWndTabPageContainer->GetSafeHwnd() == NULL )
		return 0;
	if( nCountToRemove < 0 )
		nCountToRemove = nBarCount;
INT nMaxCountToRemove = nBarCount - nTabIndex;
	if( nCountToRemove > nMaxCountToRemove )
		nCountToRemove = nMaxCountToRemove;
INT nCountRemoved = 0;
	for( nCountToRemove ++; nCountToRemove > 0; nCountToRemove --)
	{
		if( ! OnBarRemoving( nTabIndex ) )
		{
			continue;
		}
		pWndTabPageContainer->PageRemove( nTabIndex );
		OnBarRemoved( nTabIndex );
		nCountRemoved ++;
	}
	if(		bRecalcLayout
		&&	m_pDockSite->GetSafeHwnd() != NULL
		)
	{
		ASSERT_VALID( m_pDockSite );
		CFrameWnd * pParentFrame = GetParentFrame();
		ASSERT_VALID( pParentFrame );
		pParentFrame->RecalcLayout();
	}
	return nCountRemoved;
}

INT CExtTabbedToolControlBar::BarRemoveAll(
	bool bRecalcLayout // = true
	)
{
	ASSERT_VALID( this );
	return BarRemove( 0, BarGetCount(), bRecalcLayout );
}

void CExtTabbedToolControlBar::OnBarInserted( INT nTabIndex )
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( 0 <= nTabIndex && nTabIndex < BarGetCount() );
	nTabIndex;
}

bool CExtTabbedToolControlBar::OnBarRemoving( INT nTabIndex )
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( 0 <= nTabIndex && nTabIndex < BarGetCount() );
	nTabIndex;
	return true;
}

void CExtTabbedToolControlBar::OnBarRemoved( INT nTabIndex )
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( 0 <= nTabIndex && nTabIndex <= BarGetCount() );
	nTabIndex;
}

bool CExtTabbedToolControlBar::OnBarEraseBk(
	CDC & dc,
	CExtToolControlBar * pToolBar
	)
{
#ifdef _DEBUG
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
const CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
	ASSERT_VALID( pToolBar );
	ASSERT( pToolBar->GetSafeHwnd() != NULL );
	ASSERT( LPCVOID(pToolBar->GetParent()) == LPCVOID(pWndTabPageContainer) );
#endif // _DEBUG
CRect rcClient;	
	pToolBar->GetClientRect( &rcClient );
//COLORREF clr =
//		PmBridge_GetPM()->GetColor(
//			CExtPaintManager::CLR_3DFACE_OUT
//			);
COLORREF clr;
	PmBridge_GetPM()->GetThemeAccentTabColors(
		NULL,
		&clr
		);
	dc.FillSolidRect( &rcClient, clr );
	return true;
}

CSize CExtTabbedToolControlBar::GetButtonAdjustment(
	CExtToolControlBar * pBar
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pBar );
	pBar;
	return CSize( 0, 1 );
}

LRESULT CExtTabbedToolControlBar::WindowProc(
	UINT message, WPARAM wParam, LPARAM lParam )
{
LRESULT lResult =
		CExtToolControlBar::WindowProc( message, wParam, lParam );
	switch( message )
	{
	case WM_CREATE:
	{
		m_pWndTabPageContainer =
			OnTabPageContainerCreate();
#ifdef _DEBUG
		if( m_pWndTabPageContainer != NULL )
		{
			ASSERT_VALID( m_pWndTabPageContainer );
			ASSERT( m_pWndTabPageContainer->GetSafeHwnd() != NULL );
		} // if( m_pWndTabPageContainer != NULL )
#endif // _DEBUG
	}
	break;
	case WM_TIMER:
		if( wParam == 940 )
		{
			KillTimer( 940 );
			if( m_bChangingDockState )
			{
				m_bChangingDockState = false;			
				GetParentFrame()->RecalcLayout();
			}
		}
		return 0;
	case WM_WINDOWPOSCHANGED:
	case WM_SIZE:
	{
		CExtTabPageContainerWnd * pWndTabPageContainer =
			GetTabPageContainer();
		if( pWndTabPageContainer != NULL )
		{
			CRect rcMargins = OnGetTabPageContainerAreaMargins();
			CRect rc;
			GetClientRect( &rc );
			rc.left   += rcMargins.left;
			rc.right  -= rcMargins.right;
			rc.top    += rcMargins.top;
			rc.bottom -= rcMargins.bottom;
			pWndTabPageContainer->MoveWindow( &rc );
			OnAdjustTabbedLayout( OnGetDockBarDlgCtrlID() );
			CExtTabWnd * pWndTab =
				pWndTabPageContainer->GetSafeTabWindow();
			ASSERT_VALID( pWndTab );
			LONG nSelIndex = pWndTab->SelectionGet();
			if( nSelIndex >= 0 )
				pWndTab->ItemEnsureVisible( nSelIndex );
		}
	}
	break;
	default:
		if( message == CExtPaintManager::g_nMsgPaintInheritedBackground )
		{
			CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA * pPIBD =
				CExtPaintManager::PAINTINHERITEDBACKGROUNDDATA::FromWPARAM( wParam );
			ASSERT( pPIBD != NULL );
			if(		pPIBD->m_pWnd->GetSafeHwnd() != NULL
				&&	pPIBD->m_pWnd != this
				&&	(! m_bForceNoBalloonWhenRedockable )
				)
			{
				pPIBD->m_bSequenceCanceled = true;
				return 0;
			}
		} // if( message == CExtPaintManager::g_nMsgPaintInheritedBackground )
	break;
	} // switch( message )
	return lResult;
}

CExtTabPageContainerWnd * CExtTabbedToolControlBar::OnTabPageContainerCreateObject()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pTabPageContainerWnd = NULL;
	try
	{
		pTabPageContainerWnd =
			new LocalTabPageContainerWnd;
	} // try
	catch( CException * pException )
	{
		ASSERT( FALSE );
		pException->Delete();
		pTabPageContainerWnd = NULL;
	} // catch( CException * pException )
	return pTabPageContainerWnd;
}

CExtTabPageContainerWnd * CExtTabbedToolControlBar::OnTabPageContainerCreate()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pTabPageContainerWnd =
		OnTabPageContainerCreateObject();
	if( pTabPageContainerWnd == NULL )
	{
		ASSERT( FALSE );
		return NULL;
	}
CRect rcClient;
	GetClientRect( &rcClient );
	if( ! pTabPageContainerWnd->Create(
			this,
			rcClient,
			UINT(IDC_STATIC),
			WS_CHILD|WS_VISIBLE
				|WS_CLIPCHILDREN|WS_CLIPSIBLINGS
			)
		)
	{
		ASSERT( FALSE );
		delete pTabPageContainerWnd;
		return NULL;
	}
	pTabPageContainerWnd->OrientationSet( __ETWS_ORIENT_TOP );
	return pTabPageContainerWnd;
}

CExtToolControlBar * CExtTabbedToolControlBar::OnBarCreateObject()
{
	ASSERT_VALID( this );
CExtToolControlBar * pToolBar = NULL;
	try
	{
		pToolBar =
			new LocalNoReflectedToolBar;
	} // try
	catch( CException * pException )
	{
		ASSERT( FALSE );
		pException->Delete();
		pToolBar = NULL;
	} // catch( CException * pException )
	return pToolBar;
}

CExtToolControlBar * CExtTabbedToolControlBar::OnBarCreate(
	__EXT_MFC_SAFE_LPCTSTR strCaption,
	CWnd * pToolBarOwnerWnd
	)
{
	ASSERT_VALID( this );
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	if( pWndTabPageContainer->GetSafeHwnd() == NULL )
	{
		ASSERT( FALSE );
		return NULL;
	}
CExtToolControlBar * pToolBar =
		OnBarCreateObject();
	if( pToolBar == NULL )
	{
		ASSERT( FALSE );
		return NULL;
	}
	pToolBar->m_bAutoDelete = TRUE;
	pToolBar->m_bPresubclassDialogMode = true;
	if( ! pToolBar->Create(
			( strCaption == NULL ) ? _T("") : strCaption,
			pWndTabPageContainer,
			AFX_IDW_TOOLBAR,
			WS_CHILD|WS_VISIBLE
				|WS_CLIPCHILDREN|WS_CLIPSIBLINGS
				|CBRS_ALIGN_TOP|CBRS_TOOLTIPS
				|CBRS_FLYBY|CBRS_SIZE_DYNAMIC
			)
		)
	{
		ASSERT( FALSE );
		return NULL;
	}
	pToolBar->SetOwner(
		( pToolBarOwnerWnd->GetSafeHwnd() != NULL )
			? pToolBarOwnerWnd
			: GetParent()
		);
	return pToolBar;
}

CRect CExtTabbedToolControlBar::OnGetTabPageContainerAreaMargins() const
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	if( m_pDockSite != NULL && IsFloating() )
		return CRect( 1, 1, 5, 5 );
const CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
DWORD dwOrientation = pWndTabPageContainer->OrientationGet();
	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:
		return CRect( 4, 0, 4, 4 );
	case __ETWS_ORIENT_BOTTOM:
		return CRect( 4, 2, 4, 2 );
	case __ETWS_ORIENT_LEFT:
		return CRect( 0, 4, 6, 4 );
	case __ETWS_ORIENT_RIGHT:
		return CRect( 4, 4, 2, 4 );
	default:
		ASSERT( FALSE );
		return CRect( 0, 0, 0, 0 );
	} // switch( dwOrientation )
}

bool CExtTabbedToolControlBar::_GetFullRowMode() const
{
	ASSERT_VALID( this );
	return true;
}

bool CExtTabbedToolControlBar::_AffixmentIsAlignedHW() const
{
	ASSERT_VALID( this );
	return false;
}

bool CExtTabbedToolControlBar::OnQueryMultiRowLayout() const
{
	ASSERT_VALID( this );
	return false;
}

CSize CExtTabbedToolControlBar::CalcDynamicLayout(
	int nLength,
	DWORD dwMode
	)
{
	ASSERT_VALID( this );
	if(	(nLength == -1)
		&& !(dwMode & (LM_MRUWIDTH|LM_COMMIT))
		&&  (dwMode & (LM_HORZDOCK|LM_VERTDOCK))
		)
		return
			CalcFixedLayout(
				dwMode & LM_STRETCH,
				dwMode & LM_HORZDOCK
				);
	ASSERT(
		(dwMode&(LM_HORZ|LM_HORZDOCK))
		||
		(!(dwMode&LM_HORZDOCK))
		);
CSize sizeCalcLayout = _CalcLayout( dwMode, nLength );
	return sizeCalcLayout;
}

CSize CExtTabbedToolControlBar::CalcFixedLayout(
	BOOL bStretch,
	BOOL bHorz
	)
{
	ASSERT_VALID( this );
	DWORD dwMode = bStretch ? LM_STRETCH : 0;
	dwMode |= bHorz ? LM_HORZ : 0;
	ASSERT(
		(dwMode&(LM_HORZ|LM_HORZDOCK))
		||
		(!(dwMode&LM_HORZDOCK))
		);
CSize sizeCalcLayout = _CalcLayout( dwMode );
	_RecalcNcArea();
	return sizeCalcLayout;
}

void CExtTabbedToolControlBar::_DraggingUpdateState(
	const CPoint & point,
	bool bForceFloatMode
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( m_pDockSite );
	m_bChangingDockState = true;
	GetParentFrame()->DelayRecalcLayout();
	CExtToolControlBar::_DraggingUpdateState(
		point,
		bForceFloatMode
		);
	SetTimer( 940, 1, NULL );
	m_bChangingDockState = false;
}

void CExtTabbedToolControlBar::DoEraseBk( CDC * pDC )
{
	CExtToolControlBar::DoEraseBk( pDC );
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	if( pWndTabPageContainer != NULL )
	{
		CRect rcClient, rcTabPageContainer;
		GetClientRect( &rcClient );
		pWndTabPageContainer->GetWindowRect( &rcTabPageContainer );
		ScreenToClient( &rcTabPageContainer );
		OnPaintBackground(
			*pDC,
			rcClient,
			rcTabPageContainer
			);
	}
}

void CExtTabbedToolControlBar::OnPaintBackground(
	CDC & dc,
	const CRect & rcClient,
	const CRect & rcTabPageContainer
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( dc.GetSafeHdc() != NULL );
	rcClient;
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
CRect rcBorder( rcTabPageContainer );
	rcBorder.InflateRect( 1, 1 );
CRect rcTabItemsArea =
	pWndTabPageContainer->GetSafeTabWindow()->
		GetRectTabItemsArea();
	pWndTabPageContainer->GetSafeTabWindow()->
		ClientToScreen( &rcTabItemsArea ); 
	ScreenToClient( &rcTabItemsArea ); 
DWORD dwOrientation = pWndTabPageContainer->OrientationGet();
	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:
		rcBorder.top = rcTabItemsArea.bottom;
	break;
	case __ETWS_ORIENT_BOTTOM:
		rcBorder.bottom = rcTabItemsArea.top;
		break;
	case __ETWS_ORIENT_LEFT:
		rcBorder.left = rcTabItemsArea.right;
	break;
	case __ETWS_ORIENT_RIGHT:
		rcBorder.right = rcTabItemsArea.left;
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
		return;
#endif // _DEBUG
	} // switch( dwOrientation )
//COLORREF clrLight = PmBridge_GetPM()->GetColor( COLOR_3DHIGHLIGHT );
//COLORREF clrDark = PmBridge_GetPM()->GetColor( COLOR_3DSHADOW );
COLORREF clrLight, clrDark;
	PmBridge_GetPM()->GetThemeAccentTabColors(
		NULL,
		NULL,
		&clrLight,
		&clrDark
		);
	dc.Draw3dRect( &rcBorder, clrLight, clrDark );
}

DWORD CExtTabbedToolControlBar::OnQueryTabOrientation(
	UINT nDockBarDlgCtrlID
	)
{
	ASSERT_VALID( this );
	ASSERT(
			nDockBarDlgCtrlID == AFX_IDW_DOCKBAR_LEFT
		||	nDockBarDlgCtrlID == AFX_IDW_DOCKBAR_RIGHT
		||	nDockBarDlgCtrlID == AFX_IDW_DOCKBAR_TOP
		||	nDockBarDlgCtrlID == AFX_IDW_DOCKBAR_BOTTOM
		||	nDockBarDlgCtrlID == AFX_IDW_DOCKBAR_FLOAT
		);
	switch( nDockBarDlgCtrlID )
	{
	case AFX_IDW_DOCKBAR_LEFT:
	{
		DWORD dwOrientation = TabsOrientationAtLeftGet();
		ASSERT(
				dwOrientation == __ETWS_ORIENT_LEFT
			||	dwOrientation == __ETWS_ORIENT_RIGHT
			);
		return dwOrientation;
	}
	case AFX_IDW_DOCKBAR_RIGHT:
	{
		DWORD dwOrientation = TabsOrientationAtRightGet();
		ASSERT(
				dwOrientation == __ETWS_ORIENT_LEFT
			||	dwOrientation == __ETWS_ORIENT_RIGHT
			);
		return dwOrientation;
	}
	case AFX_IDW_DOCKBAR_TOP:
	{
		DWORD dwOrientation = TabsOrientationAtTopGet();
		ASSERT(
				dwOrientation == __ETWS_ORIENT_TOP
			||	dwOrientation == __ETWS_ORIENT_BOTTOM
			);
		return dwOrientation;
	}
	case AFX_IDW_DOCKBAR_BOTTOM:
	{
		DWORD dwOrientation = TabsOrientationAtBottomGet();
		ASSERT(
				dwOrientation == __ETWS_ORIENT_TOP
			||	dwOrientation == __ETWS_ORIENT_BOTTOM
			);
		return dwOrientation;
	}
	case AFX_IDW_DOCKBAR_FLOAT:
	{
		DWORD dwOrientation = TabsOrientationFloatingGet();
		ASSERT(
				dwOrientation == __ETWS_ORIENT_TOP
			||	dwOrientation == __ETWS_ORIENT_BOTTOM
			);
		return dwOrientation;
	}
	} // switch( nDockBarDlgCtrlID )
	ASSERT( FALSE );
	return __ETWS_ORIENT_TOP;
}

void CExtTabbedToolControlBar::OnAdjustTabbedLayout(
	UINT nDockBarDlgCtrlID
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
DWORD dwOrientationOld =
		pWndTabPageContainer->OrientationGet();
DWORD dwOrientationNew =
		OnQueryTabOrientation(
			nDockBarDlgCtrlID
			);
	ASSERT(
			dwOrientationNew == __ETWS_ORIENT_LEFT
		||	dwOrientationNew == __ETWS_ORIENT_RIGHT
		||	dwOrientationNew == __ETWS_ORIENT_TOP
		||	dwOrientationNew == __ETWS_ORIENT_BOTTOM
		);
	if( dwOrientationOld != dwOrientationNew )
		pWndTabPageContainer->OrientationSet(
			dwOrientationNew
			);
	pWndTabPageContainer->RepositionBars( 0, 0xFFFF, 0 );
INT nTabIndex, nTabCount = BarGetCount();
	for( nTabIndex = 0; nTabIndex < nTabCount; nTabIndex ++ )
	{
		CExtToolControlBar * pToolBar =
			BarGetAt( nTabIndex );
		ASSERT_VALID( pToolBar );
		DWORD dwBarStyle = pToolBar->GetBarStyle();
		DWORD dwBarAlign = dwBarStyle & CBRS_ALIGN_ANY;
		switch( nDockBarDlgCtrlID )
		{
		case AFX_IDW_DOCKBAR_LEFT:
		case AFX_IDW_DOCKBAR_RIGHT:
			if( dwBarAlign != CBRS_ALIGN_LEFT )
				pToolBar->SetBarStyle(
					( dwBarStyle & (~CBRS_ALIGN_ANY) )
					| CBRS_ALIGN_LEFT
					);
		break;
		case AFX_IDW_DOCKBAR_FLOAT:
		case AFX_IDW_DOCKBAR_TOP:
		case AFX_IDW_DOCKBAR_BOTTOM:
			if( dwBarAlign != CBRS_ALIGN_TOP )
				pToolBar->SetBarStyle(
					( dwBarStyle & (~CBRS_ALIGN_ANY) )
					| CBRS_ALIGN_TOP
					);
		break;
#ifdef _DEBUG
		default:
		{
			ASSERT( FALSE );
		}
		break;
#endif // _DEBUG
		} // switch( nDockBarDlgCtrlID )
	}
}

int CExtTabbedToolControlBar::OnCalcMaxToolBarWidth()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
int nMaxWidth = 0;
INT nTabIndex, nTabCount = BarGetCount();
	for( nTabIndex = 0; nTabIndex < nTabCount; nTabIndex ++ )
	{
		CExtToolControlBar * pToolBar =
			BarGetAt( nTabIndex );
		ASSERT_VALID( pToolBar );
		CRect rcClient, rcWnd;
		pToolBar->GetClientRect( &rcClient );
		pToolBar->GetWindowRect( &rcWnd );
		CSize sizeNC = rcWnd.Size() - rcClient.Size();
		int nMaxInBarWidth = 0;
		int nBtnIdx, nCountOfButtons = pToolBar->GetButtonsCount();
		for( nBtnIdx = 0; nBtnIdx < nCountOfButtons; nBtnIdx++ )
		{
			CExtBarButton * pTBB =
				pToolBar->GetButton( nBtnIdx );
			ASSERT_VALID( pTBB );
			if(		pTBB->IsSeparator()
				||	pTBB->IsKindOf(
						RUNTIME_CLASS(
							CExtBarContentExpandButton
							)
						)
				)
				continue;
			CSize sizeBtn = pTBB->Rect().Size();
			nMaxInBarWidth = max( nMaxInBarWidth, sizeBtn.cx );
		}
		nMaxInBarWidth += sizeNC.cx + 2;
		nMaxWidth = max( nMaxWidth, nMaxInBarWidth );
	}
	return nMaxWidth;
}

int CExtTabbedToolControlBar::OnCalcMaxToolBarHeight()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
int nMaxHeight = 0;
INT nTabIndex, nTabCount = BarGetCount();
	for( nTabIndex = 0; nTabIndex < nTabCount; nTabIndex ++ )
	{
		CExtToolControlBar * pToolBar =
			BarGetAt( nTabIndex );
		ASSERT_VALID( pToolBar );
		CRect rcClient, rcWnd;
		pToolBar->GetClientRect( &rcClient );
		pToolBar->GetWindowRect( &rcWnd );
		CSize sizeNC = rcWnd.Size() - rcClient.Size();
		int nMaxInBarHeight = 0;
		int nBtnIdx, nCountOfButtons = pToolBar->GetButtonsCount();
		for( nBtnIdx = 0; nBtnIdx < nCountOfButtons; nBtnIdx++ )
		{
			CExtBarButton * pTBB =
				pToolBar->GetButton( nBtnIdx );
			ASSERT_VALID( pTBB );
			if(		pTBB->IsSeparator()
				||	pTBB->IsKindOf(
						RUNTIME_CLASS(
							CExtBarContentExpandButton
							)
						)
				)
				continue;
			CSize sizeBtn = pTBB->Rect().Size();
			nMaxInBarHeight = max( nMaxInBarHeight, sizeBtn.cy );
		}
		nMaxInBarHeight += sizeNC.cy + 2;
		nMaxHeight = max( nMaxHeight, nMaxInBarHeight );
	}
	return nMaxHeight;
}

CSize CExtTabbedToolControlBar::OnCalcTabPageContainerSize(
	UINT nDockBarDlgCtrlID
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
CRect rcClient;
	pWndTabPageContainer->GetClientRect( &rcClient );
CRect rcCalc( rcClient );
	pWndTabPageContainer->RepositionBars(
		0, 0xFFFF, 0, CWnd::reposQuery,
		&rcCalc, &rcCalc
		);
CSize sizeCalc = rcClient.Size() - rcCalc.Size();
	switch( nDockBarDlgCtrlID )
	{
	case AFX_IDW_DOCKBAR_LEFT:
	case AFX_IDW_DOCKBAR_RIGHT:
		sizeCalc.cx += OnCalcMaxToolBarWidth();
		break;
	case AFX_IDW_DOCKBAR_FLOAT:
	case AFX_IDW_DOCKBAR_TOP:
	case AFX_IDW_DOCKBAR_BOTTOM:
		sizeCalc.cy += OnCalcMaxToolBarHeight();
		break;
	default:
		return CSize( 0, 0 );
	} // switch( nDockBarDlgCtrlID )
	return sizeCalc;
}

DWORD CExtTabbedToolControlBar::OnGetDockBarDlgCtrlID()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return AFX_IDW_DOCKBAR_FLOAT;
	CWnd * pWnd = GetParent();
	if( pWnd->GetSafeHwnd() == NULL )
		return AFX_IDW_DOCKBAR_FLOAT;
CControlBar * pBar =
		DYNAMIC_DOWNCAST( CControlBar, pWnd );
	if( pBar == NULL || (! pBar->IsDockBar() ) )
		return AFX_IDW_DOCKBAR_FLOAT;
	return UINT(pBar->GetDlgCtrlID());
}

CSize CExtTabbedToolControlBar::_CalcLayout(
	DWORD dwMode,
	int nLength // = -1
	)
{
	ASSERT_VALID( this );
	dwMode;
	nLength;
CSize sizeCalcLayout =
		CExtToolControlBar::_CalcLayout(
			dwMode,
			nLength
			);
CRect rcClient, rcWnd;
	GetClientRect( &rcClient );
	GetWindowRect( &rcWnd );
CSize sizeNC = rcWnd.Size() - rcClient.Size();
	if( IsFloating() )
	{
		UINT nDockBarDlgCtrlID =
			AFX_IDW_DOCKBAR_FLOAT;
		//OnAdjustTabbedLayout( nDockBarDlgCtrlID );
		sizeCalcLayout.cx = m_nFloatingWidth;
		sizeCalcLayout.cy =
			sizeNC.cy +
			OnCalcTabPageContainerSize(
				nDockBarDlgCtrlID
				).cy;
	}
	else if( IsDockedHorizontally() )
	{
		UINT nDockBarDlgCtrlID =
			OnGetDockBarDlgCtrlID();
		if(		nDockBarDlgCtrlID != AFX_IDW_DOCKBAR_TOP
			&&  nDockBarDlgCtrlID != AFX_IDW_DOCKBAR_BOTTOM
			)
			nDockBarDlgCtrlID = AFX_IDW_DOCKBAR_TOP;
		//OnAdjustTabbedLayout( nDockBarDlgCtrlID );
		sizeCalcLayout.cy =
			sizeNC.cy +
			OnCalcTabPageContainerSize(
				nDockBarDlgCtrlID
				).cy;
	}
	else if( IsDockedVertically() )
	{
		UINT nDockBarDlgCtrlID =
			OnGetDockBarDlgCtrlID();
		if(		nDockBarDlgCtrlID != AFX_IDW_DOCKBAR_LEFT
			&&  nDockBarDlgCtrlID != AFX_IDW_DOCKBAR_RIGHT
			)
			nDockBarDlgCtrlID = AFX_IDW_DOCKBAR_RIGHT;
		//OnAdjustTabbedLayout( nDockBarDlgCtrlID );
		sizeCalcLayout.cx =
			sizeNC.cx +
			OnCalcTabPageContainerSize(
				nDockBarDlgCtrlID
				).cx;
	}
	else
	{
		sizeCalcLayout += CSize( 20, 20 );
	}
CRect rcMargins = OnGetTabPageContainerAreaMargins();
	sizeCalcLayout.cx += rcMargins.left + rcMargins.right;
	sizeCalcLayout.cy += rcMargins.top + rcMargins.bottom;
	if( m_pDockSite == NULL )
	{ // specific for dialog mode
		if( IsDockedHorizontally() )
			sizeCalcLayout.cy += 4;
		else
			sizeCalcLayout.cx += 4;
	} // specific for dialog mode
	return sizeCalcLayout;
}

void CExtTabbedToolControlBar::OnUpdateCmdUI(
	CFrameWnd * pTarget,
	BOOL bDisableIfNoHndler
	)
{
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetTabPageContainer();
	if( pWndTabPageContainer == NULL )
		return;
INT nTabIndex, nTabCount = BarGetCount();
	for( nTabIndex = 0; nTabIndex < nTabCount; nTabIndex ++ )
	{
		CExtToolControlBar * pToolBar =
			BarGetAt( nTabIndex );
		ASSERT_VALID( pToolBar );
		int nBtnIdx, nCountOfButtons = pToolBar->GetButtonsCount();
		for( nBtnIdx = 0; nBtnIdx < nCountOfButtons; nBtnIdx++ )
		{
			CExtBarButton * pTBB =
				pToolBar->GetButton( nBtnIdx );
			ASSERT_VALID( pTBB );
			pTBB->OnUpdateCmdUI(
				pTarget,
				bDisableIfNoHndler,
				nBtnIdx
				);
		}
		pToolBar->UpdateDialogControls( pTarget, bDisableIfNoHndler );
	}
}

class CExtTabbedToolControlBar::LocalTabWnd
	: public CExtTWPC < CExtTabWnd >
{
protected:
	virtual INT OnTabWndGetParentSizingMargin(
		DWORD dwOrientation
		) const;
	virtual void OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		);
	virtual void OnTabWndDrawItem(
		CDC & dc,
		CRect & rcTabItemsArea,
		LONG nItemIndex,
		TAB_ITEM_INFO * pTii,
		bool bTopLeft,
		bool bHorz,
		bool bSelected,
		bool bCenteredText,
		bool bGroupedMode,
		bool bInGroupActive,
		bool bInvertedVerticalMode,
		const CRect & rcEntireItem,
		CSize sizeTextMeasured,
		CFont * pFont,
		__EXT_MFC_SAFE_LPCTSTR sText,
		CExtCmdIcon * pIcon
		);
}; // class CExtTabbedToolControlBar::LocalTabWnd

INT CExtTabbedToolControlBar::LocalTabWnd::
	OnTabWndGetParentSizingMargin(
		DWORD dwOrientation
		) const
{
	ASSERT_VALID( this );
	if(		dwOrientation == __ETWS_ORIENT_RIGHT
		||	dwOrientation == __ETWS_ORIENT_BOTTOM
		)
		return 1;
	return 3;
}

void CExtTabbedToolControlBar::LocalTabWnd::
	OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		)
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	PmBridge_GetPM()->PaintTabbedTabClientArea(
		dc,
		rcClient,
		rcTabItemsArea,
		rcTabNearBorderArea,
		dwOrientation,
		bGroupedMode,
		this
		);
}

void CExtTabbedToolControlBar::LocalTabWnd::
	OnTabWndDrawItem(
		CDC & dc,
		CRect & rcTabItemsArea,
		LONG nItemIndex,
		CExtTabWnd::TAB_ITEM_INFO * pTii,
		bool bTopLeft,
		bool bHorz,
		bool bSelected,
		bool bCenteredText,
		bool bGroupedMode,
		bool bInGroupActive,
		bool bInvertedVerticalMode,
		const CRect & rcEntireItem,
		CSize sizeTextMeasured,
		CFont * pFont,
		__EXT_MFC_SAFE_LPCTSTR sText,
		CExtCmdIcon * pIcon
		)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTii );
	pTii;
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( pFont != NULL );
	ASSERT( pFont->GetSafeHandle() != NULL );
	
	if( (pTii->GetItemStyle() & __ETWI_CENTERED_TEXT) != 0 )
		bCenteredText = true;
	
	PmBridge_GetPM()->PaintTabItem(
		dc,
		rcTabItemsArea,
		bTopLeft,
		bHorz,
		bSelected,
		bCenteredText,
		bGroupedMode,
		bInGroupActive,
		bInvertedVerticalMode,
		rcEntireItem,
		sizeTextMeasured,
		pFont,
		sText,
		pIcon,
		this,
		nItemIndex,
		// PmBridge_GetPM()->GetColor( CExtPaintManager::CLR_TEXT_OUT ) - v.2.55
		PmBridge_GetPM()->GetColor( COLOR_BTNTEXT )
		);
}

CExtTabWnd * CExtTabbedToolControlBar::LocalTabPageContainerWnd::
	OnTabWndGetTabImpl()
{
	ASSERT_VALID( this );
	return new LocalTabWnd;
}

CExtTabbedToolControlBar::LocalNoReflectedToolBar::
	LocalNoReflectedToolBar()
		: m_bFakeMultirowLayout( false )
{
	m_bAutoDelete = TRUE;
	m_cxLeftBorder = 0;
	m_cxRightBorder = 0;
	m_cyTopBorder = 0;
	m_cyBottomBorder = 0;
}

CExtTabPageContainerWnd *
	CExtTabbedToolControlBar::LocalNoReflectedToolBar::
		GetContainer()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pContainer =
		STATIC_DOWNCAST(
			CExtTabPageContainerWnd,
			GetParent()
			);
	return pContainer;
}

CExtTabbedToolControlBar *
	CExtTabbedToolControlBar::LocalNoReflectedToolBar::
		GetContainerBar()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabbedToolControlBar * pContainerBar =
		STATIC_DOWNCAST(
			CExtTabbedToolControlBar,
			GetContainer()->GetParent()
			);
	return pContainerBar;
}

CSize CExtTabbedToolControlBar::LocalNoReflectedToolBar::
	CalcDynamicLayout(
		int nLength,
		DWORD dwMode
		)
{
	ASSERT_VALID( this );
	if(	(nLength == -1)
		&& !(dwMode & (LM_MRUWIDTH|LM_COMMIT))
		&&  (dwMode & (LM_HORZDOCK|LM_VERTDOCK))
		)
		return
			CalcFixedLayout(
				dwMode & LM_STRETCH,
				dwMode & LM_HORZDOCK
				);
	ASSERT(
		(dwMode&(LM_HORZ|LM_HORZDOCK))
		||
		(!(dwMode&LM_HORZDOCK))
		);
CSize sizeCalcLayout = _CalcLayout( dwMode, nLength );
	return sizeCalcLayout;
}

CSize CExtTabbedToolControlBar::LocalNoReflectedToolBar::
	CalcFixedLayout(
		BOOL bStretch,
		BOOL bHorz
		)
{
	ASSERT_VALID( this );
	DWORD dwMode = bStretch ? LM_STRETCH : 0;
	dwMode |= bHorz ? LM_HORZ : 0;
	ASSERT(
		(dwMode&(LM_HORZ|LM_HORZDOCK))
		||
		(!(dwMode&LM_HORZDOCK))
		);
CSize sizeCalcLayout = _CalcLayout( dwMode );
	_RecalcNcArea();
	return sizeCalcLayout;
}

CSize CExtTabbedToolControlBar::LocalNoReflectedToolBar::
	_CalcLayout(
		DWORD dwMode,
		int nLength // = -1
		)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CSize sizeCalcLayout =
		CExtToolControlBar::_CalcLayout( dwMode, nLength );
	return sizeCalcLayout;
}

LRESULT CExtTabbedToolControlBar::LocalNoReflectedToolBar::
	WindowProc( UINT message, WPARAM wParam, LPARAM lParam ) 
{
	if( message == WM_SIZEPARENT )
		return 0;
	if( message == WM_NCCALCSIZE )
		return 0;
	LRESULT lResult = CExtToolControlBar::WindowProc( message, wParam, lParam );
	return lResult;
}

bool CExtTabbedToolControlBar::LocalNoReflectedToolBar::
	OnQueryMultiRowLayout() const
{
	ASSERT_VALID( this );
	return m_bFakeMultirowLayout;
}

void CExtTabbedToolControlBar::LocalNoReflectedToolBar::
	_RecalcPositionsImpl()
{
	ASSERT_VALID( this );
	CExtToolControlBar::_RecalcPositionsImpl();
int nBtnIdx, nBtnCount = GetButtonsCount();
CSize _sizeOffset = GetContainerBar()->GetButtonAdjustment( this );
	for( nBtnIdx = 0; nBtnIdx < nBtnCount; nBtnIdx++ )
	{
		CExtBarButton * pTBB = GetButton( nBtnIdx );
		ASSERT_VALID( pTBB );
		if( pTBB->IsKindOf(RUNTIME_CLASS(CExtBarContentExpandButton)) )
			break;
		if( ! pTBB->IsVisible() )
			continue;
		if( (pTBB->GetStyle()&TBBS_HIDDEN) != 0 )
			continue;
		CRect rcTBB = pTBB->Rect();
		rcTBB.OffsetRect( _sizeOffset );
		pTBB->SetRect( rcTBB );
	} // for( nBtnIdx = 0; nBtnIdx < nBtnCount; nBtnIdx++ )
}

void CExtTabbedToolControlBar::LocalNoReflectedToolBar::
	_RecalcLayoutImpl()
{
	ASSERT_VALID( this );
	CExtToolControlBar::_RecalcLayoutImpl();
}

void CExtTabbedToolControlBar::LocalNoReflectedToolBar::
	DoPaint( CDC * pDC )
{
	ASSERT_VALID( this );
	m_bFakeMultirowLayout = true;
	CExtToolControlBar::DoPaint( pDC );
	m_bFakeMultirowLayout = false;
}

void CExtTabbedToolControlBar::LocalNoReflectedToolBar::
	DoEraseBk( CDC * pDC )
{
	ASSERT_VALID( this );
CExtTabbedToolControlBar * pContainerBar =
		GetContainerBar();
	if(		pContainerBar->GetSafeHwnd() != NULL
		&&  (! pContainerBar->OnBarEraseBk( *pDC, this ) )
		)
		CExtToolControlBar::DoEraseBk( pDC );
}

#if (!defined __EXT_MFC_NO_TABBED_TOOLBAR_FLAT)

/////////////////////////////////////////////////////////////////////////////
// CExtTabbedToolControlBarFlat window

IMPLEMENT_DYNCREATE( CExtTabbedToolControlBarFlat, CExtTabbedToolControlBar );

CExtTabbedToolControlBarFlat::CExtTabbedToolControlBarFlat()
{
}

CExtTabbedToolControlBarFlat::~CExtTabbedToolControlBarFlat()
{
}

CExtTabPageContainerWnd *
	CExtTabbedToolControlBarFlat::OnTabPageContainerCreateObject()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pTabPageContainerWnd = NULL;
	try
	{
		pTabPageContainerWnd =
			new LocalTabPageContainerWnd;
	} // try
	catch( CException * pException )
	{
		ASSERT( FALSE );
		pException->Delete();
		pTabPageContainerWnd = NULL;
	} // catch( CException * pException )
	return pTabPageContainerWnd;
}

COLORREF CExtTabbedToolControlBarFlat::LocalToolBar::
	OnQueryCustomAccentEffectForIcon(
		CDC & dc,
		CExtBarButton * pTBB
		)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	pTBB;
	if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) <= 8 )
		return COLORREF(-1L);
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetContainer();
	if( pWndTabPageContainer == NULL )
		return COLORREF(-1L);
CExtTabbedToolControlBar * pContainerBar =
		GetContainerBar();
	if( pContainerBar == NULL )
		return COLORREF(-1L);
INT nTabIndex = pContainerBar->BarGetIndexOf( this );
	ASSERT( 0 <= nTabIndex && nTabIndex < pContainerBar->BarGetCount() );
CExtTabFlatWnd * pWndTab =
	STATIC_DOWNCAST(
		CExtTabFlatWnd,
		pWndTabPageContainer->GetSafeTabWindow()
		);
	ASSERT_VALID( pWndTab );

bool bEnabled = 
	pWndTab->ItemEnabledGet( nTabIndex );

COLORREF clrLight = COLORREF(-1L);
COLORREF clrShadow = COLORREF(-1L);
COLORREF clrDkShadow = COLORREF(-1L);
COLORREF clrTabBk = COLORREF(-1L);
COLORREF clrText = COLORREF(-1L);

	pWndTab->OnFlatTabWndGetItemColors(
		nTabIndex,
		true,
		false,
		bEnabled,
		clrLight,
		clrShadow,
		clrDkShadow, 
		clrTabBk, 
		clrText
		);
	return clrTabBk;
}

class CExtTabbedToolControlBarFlat::LocalTabWnd
	: public CExtTWPC < CExtTabFlatWnd >
{
protected:
	virtual void OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		);
}; // class CExtTabbedToolControlBarFlat::LocalTabWnd

void CExtTabbedToolControlBarFlat::LocalTabWnd::
	OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		)
{
	ASSERT_VALID( this );
	rcClient;
	rcTabItemsArea;
	rcTabNearBorderArea;
	dwOrientation;
	bGroupedMode;
	if( ! PmBridge_GetPM()->PaintDockerBkgnd(
			true,
			dc,
			this
			)
		)
		dc.FillSolidRect(
			&rcClient,
			PmBridge_GetPM()->GetColor(
				CExtPaintManager::CLR_3DFACE_OUT
				)
			);
bool bHorz = OrientationIsHorizontal();
bool bTopLeft = OrientationIsTopLeft();
CRect rcMargin( rcClient );
	if( bHorz )
	{
		if( bTopLeft )
		{
			rcMargin.top = rcMargin.bottom - 1;
		} // if( bTopLeft )
		else
		{
			rcMargin.bottom = rcMargin.top + 1;
		} // else from if( bTopLeft )
	} // if( bHorz )
	else
	{
		if( bTopLeft )
		{
			rcMargin.left = rcMargin.right - 1;
		} // if( bTopLeft )
		else
		{
			rcMargin.right = rcMargin.left + 1;
		} // else from if( bTopLeft )
	} // else from if( bHorz )
COLORREF clrMargin;
	OnFlatTabWndGetMarginColors( clrMargin );
	dc.FillSolidRect( &rcMargin, clrMargin );
}

CExtTabWnd * CExtTabbedToolControlBarFlat::LocalTabPageContainerWnd::
	OnTabWndGetTabImpl()
{
	ASSERT_VALID( this );
	return new LocalTabWnd;
}

CExtTabPageContainerFlatWnd * CExtTabbedToolControlBarFlat::
	GetFlatTabPageContainer()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pWndTabPageContainer = GetTabPageContainer();
	if( pWndTabPageContainer == NULL )
		return NULL;
	ASSERT_VALID( pWndTabPageContainer );
CExtTabPageContainerFlatWnd * pWndTabPageContainerFlat =
		STATIC_DOWNCAST(
			CExtTabPageContainerFlatWnd,
			pWndTabPageContainer
			);
	return pWndTabPageContainerFlat;
}

const CExtTabPageContainerFlatWnd * CExtTabbedToolControlBarFlat::
	GetFlatTabPageContainer() const
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	return
		( const_cast < CExtTabbedToolControlBarFlat * > ( this ) )
			-> GetFlatTabPageContainer();
}

bool CExtTabbedToolControlBarFlat::OnBarEraseBk(
	CDC & dc,
	CExtToolControlBar * pToolBar
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
const CExtTabPageContainerFlatWnd * pWndTabPageContainer =
		GetFlatTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
	ASSERT_VALID( pToolBar );
	ASSERT( pToolBar->GetSafeHwnd() != NULL );
	ASSERT( LPCVOID(pToolBar->GetParent()) == LPCVOID(pWndTabPageContainer) );
INT nTabIndex = BarGetIndexOf( pToolBar );
	ASSERT( 0 <= nTabIndex && nTabIndex < BarGetCount() );
CExtTabFlatWnd * pWndTab =
		STATIC_DOWNCAST(
			CExtTabFlatWnd,
			((CExtTabWnd*)pWndTabPageContainer->GetSafeTabWindow())
			);
	ASSERT_VALID( pWndTab );

bool bEnabled = 
	pWndTab->ItemEnabledGet( nTabIndex );

COLORREF clrLight = COLORREF(-1L);
COLORREF clrShadow = COLORREF(-1L);
COLORREF clrDkShadow = COLORREF(-1L);
COLORREF clrTabBk = COLORREF(-1L);
COLORREF clrText = COLORREF(-1L);

	pWndTab->OnFlatTabWndGetItemColors(
		nTabIndex,
		true,
		false,
		bEnabled,
		clrLight,
		clrShadow,
		clrDkShadow, 
		clrTabBk, 
		clrText
		);

CRect rcClient;
	pToolBar->GetClientRect( &rcClient );
	dc.FillSolidRect( &rcClient, clrTabBk );
	return true;
}

CSize CExtTabbedToolControlBarFlat::GetButtonAdjustment(
	CExtToolControlBar * pBar
	) const
{
	ASSERT_VALID( this );
	return
		CExtTabbedToolControlBar::
			GetButtonAdjustment( pBar );
}

CExtToolControlBar * CExtTabbedToolControlBarFlat::OnBarCreateObject()
{
	ASSERT_VALID( this );
CExtToolControlBar * pToolBar = NULL;
	try
	{
		pToolBar =
			new LocalToolBar;
	} // try
	catch( CException * pException )
	{
		ASSERT( FALSE );
		pException->Delete();
		pToolBar = NULL;
	} // catch( CException * pException )
	return pToolBar;
}

void CExtTabbedToolControlBarFlat::OnPaintBackground(
	CDC & dc,
	const CRect & rcClient,
	const CRect & rcTabPageContainer
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( dc.GetSafeHdc() != NULL );
	rcClient;
CExtTabPageContainerFlatWnd * pWndTabPageContainer =
		GetFlatTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
CRect rcBorder( rcTabPageContainer );
	rcBorder.InflateRect( 1, 1 );
CRect rcTabItemsArea =
	pWndTabPageContainer->GetSafeTabWindow()->
		GetRectTabItemsArea();
	pWndTabPageContainer->GetSafeTabWindow()->
		ClientToScreen( &rcTabItemsArea ); 
	ScreenToClient( &rcTabItemsArea ); 
DWORD dwOrientation = pWndTabPageContainer->OrientationGet();
	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:
		rcBorder.top = rcTabItemsArea.bottom - 1;
	break;
	case __ETWS_ORIENT_BOTTOM:
		rcBorder.bottom = rcTabItemsArea.top;
		break;
	case __ETWS_ORIENT_LEFT:
		rcBorder.left = rcTabItemsArea.right - 1;
	break;
	case __ETWS_ORIENT_RIGHT:
		rcBorder.right = rcTabItemsArea.left;
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
		return;
#endif // _DEBUG
	} // switch( dwOrientation )
CExtTabFlatWnd * pWndTab =
	STATIC_DOWNCAST(
		CExtTabFlatWnd,
		pWndTabPageContainer->GetSafeTabWindow()
		);
COLORREF clrMargin;
	pWndTab->OnFlatTabWndGetMarginColors( clrMargin );
	dc.Draw3dRect( &rcBorder, clrMargin, clrMargin );
}

#endif // __EXT_MFC_NO_TABBED_TOOLBAR_FLAT

#if (!defined __EXT_MFC_NO_TABBED_TOOLBAR_BUTTONS)

/////////////////////////////////////////////////////////////////////////////
// CExtTabbedToolControlBarButtons window

IMPLEMENT_DYNCREATE( CExtTabbedToolControlBarButtons, CExtTabbedToolControlBar );

CExtTabbedToolControlBarButtons::CExtTabbedToolControlBarButtons()
{
}

CExtTabbedToolControlBarButtons::~CExtTabbedToolControlBarButtons()
{
}

CExtTabPageContainerWnd *
	CExtTabbedToolControlBarButtons::OnTabPageContainerCreateObject()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pTabPageContainerWnd = NULL;
	try
	{
		pTabPageContainerWnd =
			new LocalTabPageContainerWnd;
	} // try
	catch( CException * pException )
	{
		ASSERT( FALSE );
		pException->Delete();
		pTabPageContainerWnd = NULL;
	} // catch( CException * pException )
	return pTabPageContainerWnd;
}

COLORREF CExtTabbedToolControlBarButtons::LocalToolBar::
	OnQueryCustomAccentEffectForIcon(
		CDC & dc,
		CExtBarButton * pTBB
		)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	pTBB;
	if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) <= 8 )
		return COLORREF(-1L);
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetContainer();
	if( pWndTabPageContainer == NULL )
		return COLORREF(-1L);
CExtTabbedToolControlBar * pContainerBar =
		GetContainerBar();
	if( pContainerBar == NULL )
		return COLORREF(-1L);
INT nTabIndex = pContainerBar->BarGetIndexOf( this );
	ASSERT( 0 <= nTabIndex && nTabIndex < pContainerBar->BarGetCount() );
CExtTabButtonsWnd * pWndTab =
	STATIC_DOWNCAST(
		CExtTabButtonsWnd,
		pWndTabPageContainer->GetSafeTabWindow()
		);
	ASSERT_VALID( pWndTab );

bool bEnabled = 
	pWndTab->ItemEnabledGet( nTabIndex );

COLORREF clrLight = COLORREF(-1L);
COLORREF clrShadow = COLORREF(-1L);
COLORREF clrDkShadow = COLORREF(-1L);
COLORREF clrTabBk = COLORREF(-1L);
COLORREF clrText = COLORREF(-1L);

	pWndTab->OnFlatTabWndGetItemColors(
		nTabIndex,
		true,
		false,
		bEnabled,
		clrLight,
		clrShadow,
		clrDkShadow, 
		clrTabBk, 
		clrText
		);
	return clrTabBk;
}

class CExtTabbedToolControlBarButtons::LocalTabWnd
	: public CExtTWPC < CExtTabButtonsWnd >
{
protected:
	virtual void OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		);
}; // class CExtTabbedToolControlBarButtons::LocalTabWnd

void CExtTabbedToolControlBarButtons::LocalTabWnd::
	OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		)
{
	ASSERT_VALID( this );
	rcClient;
	rcTabItemsArea;
	rcTabNearBorderArea;
	dwOrientation;
	bGroupedMode;
	if( ! PmBridge_GetPM()->PaintDockerBkgnd(
			true,
			dc,
			this
			)
		)
		dc.FillSolidRect(
			&rcClient,
			PmBridge_GetPM()->GetColor(
				CExtPaintManager::CLR_3DFACE_OUT
				)
			);
//bool bHorz = OrientationIsHorizontal();
//bool bTopLeft = OrientationIsTopLeft();
//CRect rcMargin( rcClient );
//	if( bHorz )
//	{
//		if( bTopLeft )
//		{
//			rcMargin.top = rcMargin.bottom - 1;
//		} // if( bTopLeft )
//		else
//		{
//			rcMargin.bottom = rcMargin.top + 1;
//		} // else from if( bTopLeft )
//	} // if( bHorz )
//	else
//	{
//		if( bTopLeft )
//		{
//			rcMargin.left = rcMargin.right - 1;
//		} // if( bTopLeft )
//		else
//		{
//			rcMargin.right = rcMargin.left + 1;
//		} // else from if( bTopLeft )
//	} // else from if( bHorz )
//COLORREF clrMargin;
//	OnFlatTabWndGetMarginColors( clrMargin );
//	dc.FillSolidRect( &rcMargin, clrMargin );
}

CExtTabWnd * CExtTabbedToolControlBarButtons::LocalTabPageContainerWnd::
	OnTabWndGetTabImpl()
{
	ASSERT_VALID( this );
	return new LocalTabWnd;
}

CExtTabPageContainerButtonsWnd * CExtTabbedToolControlBarButtons::
	GetButtonsTabPageContainer()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pWndTabPageContainer = GetTabPageContainer();
	if( pWndTabPageContainer == NULL )
		return NULL;
	ASSERT_VALID( pWndTabPageContainer );
CExtTabPageContainerButtonsWnd * pWndTabPageContainerButtons =
		STATIC_DOWNCAST(
			CExtTabPageContainerButtonsWnd,
			pWndTabPageContainer
			);
	return pWndTabPageContainerButtons;
}

const CExtTabPageContainerButtonsWnd * CExtTabbedToolControlBarButtons::
	GetButtonsTabPageContainer() const
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	return
		( const_cast < CExtTabbedToolControlBarButtons * > ( this ) )
			-> GetButtonsTabPageContainer();
}

bool CExtTabbedToolControlBarButtons::OnBarEraseBk(
	CDC & dc,
	CExtToolControlBar * pToolBar
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	dc;
	pToolBar;
	return false;
}

CSize CExtTabbedToolControlBarButtons::GetButtonAdjustment(
	CExtToolControlBar * pBar
	) const
{
	ASSERT_VALID( this );
	return
		CExtTabbedToolControlBar::
			GetButtonAdjustment( pBar );
}

CExtToolControlBar * CExtTabbedToolControlBarButtons::OnBarCreateObject()
{
	ASSERT_VALID( this );
CExtToolControlBar * pToolBar = NULL;
	try
	{
		pToolBar =
			new LocalToolBar;
	} // try
	catch( CException * pException )
	{
		ASSERT( FALSE );
		pException->Delete();
		pToolBar = NULL;
	} // catch( CException * pException )
	return pToolBar;
}

void CExtTabbedToolControlBarButtons::OnPaintBackground(
	CDC & dc,
	const CRect & rcClient,
	const CRect & rcTabPageContainer
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( dc.GetSafeHdc() != NULL );
	dc;
	rcClient;
	rcTabPageContainer;
}

#endif // __EXT_MFC_NO_TABBED_TOOLBAR_BUTTONS

#if (!defined __EXT_MFC_NO_TABBED_TOOLBAR_WHIDBEY)

/////////////////////////////////////////////////////////////////////////////
// CExtTabbedToolControlBarWhidbey window

IMPLEMENT_DYNCREATE( CExtTabbedToolControlBarWhidbey, CExtTabbedToolControlBar );

CExtTabbedToolControlBarWhidbey::CExtTabbedToolControlBarWhidbey()
{
}

CExtTabbedToolControlBarWhidbey::~CExtTabbedToolControlBarWhidbey()
{
}

CExtTabPageContainerWnd *
	CExtTabbedToolControlBarWhidbey::OnTabPageContainerCreateObject()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pTabPageContainerWnd = NULL;
	try
	{
		pTabPageContainerWnd =
			new LocalTabPageContainerWnd;
	} // try
	catch( CException * pException )
	{
		ASSERT( FALSE );
		pException->Delete();
		pTabPageContainerWnd = NULL;
	} // catch( CException * pException )
	return pTabPageContainerWnd;
}

COLORREF CExtTabbedToolControlBarWhidbey::LocalToolBar::
	OnQueryCustomAccentEffectForIcon(
		CDC & dc,
		CExtBarButton * pTBB
		)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	pTBB;
	if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) <= 8 )
		return COLORREF(-1L);
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetContainer();
	if( pWndTabPageContainer == NULL )
		return COLORREF(-1L);
CExtTabbedToolControlBar * pContainerBar =
		GetContainerBar();
	if( pContainerBar == NULL )
		return COLORREF(-1L);
INT nTabIndex = pContainerBar->BarGetIndexOf( this );
	ASSERT( 0 <= nTabIndex && nTabIndex < pContainerBar->BarGetCount() );
CExtTabWhidbeyWnd * pWndTab =
	STATIC_DOWNCAST(
		CExtTabWhidbeyWnd,
		pWndTabPageContainer->GetSafeTabWindow()
		);
	ASSERT_VALID( pWndTab );
bool bEnabled = 
	pWndTab->ItemEnabledGet( nTabIndex );
COLORREF clr = COLORREF( -1L );
	pWndTab->OnTabWndQueryItemColors(
		nTabIndex,
		true,
		false,
		bEnabled,
		NULL,
		NULL,
		&clr
		);
	return clr;
}

class CExtTabbedToolControlBarWhidbey::LocalTabWnd
	: public CExtTWPC < CExtTabWhidbeyWnd >
{
protected:
	virtual INT OnTabWndGetParentSizingMargin(
		DWORD dwOrientation
		) const;
	virtual void OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		);
}; // class CExtTabbedToolControlBarWhidbey::LocalTabWnd

INT CExtTabbedToolControlBarWhidbey::LocalTabWnd::
	OnTabWndGetParentSizingMargin(
		DWORD dwOrientation
		) const
{
	ASSERT_VALID( this );
	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:
		return 3;
	case __ETWS_ORIENT_BOTTOM:
		return 1;
	case __ETWS_ORIENT_LEFT:
		return 4;
	case __ETWS_ORIENT_RIGHT:
		return 2;
	default:
		ASSERT( FALSE );
		return 0;
	} // switch( dwOrientation )
}

void CExtTabbedToolControlBarWhidbey::LocalTabWnd::
	OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		)
{
	ASSERT_VALID( this );
	rcClient;
	rcTabItemsArea;
	rcTabNearBorderArea;
	dwOrientation;
	bGroupedMode;
	if( ! PmBridge_GetPM()->PaintDockerBkgnd(
			true,
			dc,
			this
			)
		)
		dc.FillSolidRect(
			&rcClient,
			PmBridge_GetPM()->GetColor(
				CExtPaintManager::CLR_3DFACE_OUT
				)
			);
CRect rcTabNearMargin( rcTabItemsArea );
	switch( dwOrientation )
	{
		case __ETWS_ORIENT_TOP:
			rcTabNearMargin.top = rcTabNearMargin.bottom - 1;
			rcTabNearMargin.left = rcClient.left;
			rcTabNearMargin.right = rcClient.right;
		break;
		case __ETWS_ORIENT_BOTTOM:
			rcTabNearMargin.bottom = rcTabNearMargin.top + 1;
			rcTabNearMargin.left = rcClient.left;
			rcTabNearMargin.right = rcClient.right;
		break;
		case __ETWS_ORIENT_LEFT:
			rcTabNearMargin.left = rcTabNearMargin.right - 1;
			rcTabNearMargin.top = rcClient.top;
			rcTabNearMargin.bottom = rcClient.bottom;
			rcTabNearMargin.OffsetRect( 1, 0 );
		break;
		case __ETWS_ORIENT_RIGHT:
			rcTabNearMargin.right = rcTabNearMargin.left + 1;
			rcTabNearMargin.top = rcClient.top;
			rcTabNearMargin.bottom = rcClient.bottom;
			rcTabNearMargin.OffsetRect( -1, 0 );
		break;
		default:
			ASSERT( FALSE );
		break;
	} // switch( dwOrientation )
	// paint tab border margin
	dc.FillSolidRect(
		&rcTabNearMargin,
		PmBridge_GetPM()->GetColor( COLOR_3DSHADOW, this )
		);
}

CExtTabWnd * CExtTabbedToolControlBarWhidbey::LocalTabPageContainerWnd::
	OnTabWndGetTabImpl()
{
	return new LocalTabWnd;
}

CExtTabPageContainerWhidbeyWnd * CExtTabbedToolControlBarWhidbey::
	GetWhidbeyTabPageContainer()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pWndTabPageContainer = GetTabPageContainer();
	if( pWndTabPageContainer == NULL )
		return NULL;
	ASSERT_VALID( pWndTabPageContainer );
CExtTabPageContainerWhidbeyWnd * pWndTabPageContainerWhidbey =
		STATIC_DOWNCAST(
			CExtTabPageContainerWhidbeyWnd,
			pWndTabPageContainer
			);
	return pWndTabPageContainerWhidbey;
}

const CExtTabPageContainerWhidbeyWnd * CExtTabbedToolControlBarWhidbey::
	GetWhidbeyTabPageContainer() const
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	return
		( const_cast < CExtTabbedToolControlBarWhidbey * > ( this ) )
			-> GetWhidbeyTabPageContainer();
}

bool CExtTabbedToolControlBarWhidbey::OnBarEraseBk(
	CDC & dc,
	CExtToolControlBar * pToolBar
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
const CExtTabPageContainerWhidbeyWnd * pWndTabPageContainer =
		GetWhidbeyTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
	ASSERT_VALID( pToolBar );
	ASSERT( pToolBar->GetSafeHwnd() != NULL );
	ASSERT( LPCVOID(pToolBar->GetParent()) == LPCVOID(pWndTabPageContainer) );
INT nTabIndex = BarGetIndexOf( pToolBar );
	ASSERT( 0 <= nTabIndex && nTabIndex < BarGetCount() );
CExtTabWhidbeyWnd * pWndTab =
		STATIC_DOWNCAST(
			CExtTabWhidbeyWnd,
			((CExtTabWnd*)pWndTabPageContainer->GetSafeTabWindow())
			);
	ASSERT_VALID( pWndTab );
bool bEnabled = 
	pWndTab->ItemEnabledGet( nTabIndex );
COLORREF clr = COLORREF( -1L );
	pWndTab->OnTabWndQueryItemColors(
		nTabIndex,
		true,
		false,
		bEnabled,
		NULL,
		NULL,
		&clr
		);
CRect rcClient;
	pToolBar->GetClientRect( &rcClient );
	dc.FillSolidRect( &rcClient, clr );
	return true;
}

CSize CExtTabbedToolControlBarWhidbey::GetButtonAdjustment(
	CExtToolControlBar * pBar
	) const
{
	ASSERT_VALID( this );
	return
		CExtTabbedToolControlBar::
			GetButtonAdjustment( pBar );
}

CExtToolControlBar * CExtTabbedToolControlBarWhidbey::OnBarCreateObject()
{
	ASSERT_VALID( this );
CExtToolControlBar * pToolBar = NULL;
	try
	{
		pToolBar =
			new LocalToolBar;
	} // try
	catch( CException * pException )
	{
		ASSERT( FALSE );
		pException->Delete();
		pToolBar = NULL;
	} // catch( CException * pException )
	return pToolBar;
}

void CExtTabbedToolControlBarWhidbey::OnPaintBackground(
	CDC & dc,
	const CRect & rcClient,
	const CRect & rcTabPageContainer
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( dc.GetSafeHdc() != NULL );
	rcClient;
CExtTabPageContainerWhidbeyWnd * pWndTabPageContainer =
		GetWhidbeyTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
CRect rcBorder( rcTabPageContainer );
	rcBorder.InflateRect( 1, 1 );
CRect rcTabItemsArea =
	pWndTabPageContainer->GetSafeTabWindow()->
		GetRectTabItemsArea();
	pWndTabPageContainer->GetSafeTabWindow()->
		ClientToScreen( &rcTabItemsArea ); 
	ScreenToClient( &rcTabItemsArea ); 
DWORD dwOrientation = pWndTabPageContainer->OrientationGet();
	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:
		rcBorder.top = rcTabItemsArea.bottom - 1;
	break;
	case __ETWS_ORIENT_BOTTOM:
		rcBorder.bottom = rcTabItemsArea.top + 1;
		break;
	case __ETWS_ORIENT_LEFT:
		rcBorder.left = rcTabItemsArea.right + 1;
	break;
	case __ETWS_ORIENT_RIGHT:
		rcBorder.right = rcTabItemsArea.left;
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
		return;
#endif // _DEBUG
	} // switch( dwOrientation )
COLORREF clrMargin = PmBridge_GetPM()->GetColor( COLOR_3DSHADOW );
	dc.Draw3dRect( &rcBorder, clrMargin, clrMargin );
}

#endif // __EXT_MFC_NO_TABBED_TOOLBAR_WHIDBEY

#if (!defined __EXT_MFC_NO_TABBED_TOOLBAR_ONENOTE)

/////////////////////////////////////////////////////////////////////////////
// CExtTabbedToolControlBarOneNote window

IMPLEMENT_DYNCREATE( CExtTabbedToolControlBarOneNote, CExtTabbedToolControlBar );

CExtTabbedToolControlBarOneNote::CExtTabbedToolControlBarOneNote()
{
}

CExtTabbedToolControlBarOneNote::~CExtTabbedToolControlBarOneNote()
{
}

CExtTabPageContainerWnd *
	CExtTabbedToolControlBarOneNote::OnTabPageContainerCreateObject()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pTabPageContainerWnd = NULL;
	try
	{
		pTabPageContainerWnd =
			new LocalTabPageContainerWnd;
	} // try
	catch( CException * pException )
	{
		ASSERT( FALSE );
		pException->Delete();
		pTabPageContainerWnd = NULL;
	} // catch( CException * pException )
	return pTabPageContainerWnd;
}

COLORREF CExtTabbedToolControlBarOneNote::LocalToolBar::
	OnQueryCustomAccentEffectForIcon(
		CDC & dc,
		CExtBarButton * pTBB
		)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pTBB );
	pTBB;
	if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) <= 8 )
		return COLORREF(-1L);
CExtTabPageContainerWnd * pWndTabPageContainer =
		GetContainer();
	if( pWndTabPageContainer == NULL )
		return COLORREF(-1L);
CExtTabbedToolControlBar * pContainerBar =
		GetContainerBar();
	if( pContainerBar == NULL )
		return COLORREF(-1L);
INT nTabIndex = pContainerBar->BarGetIndexOf( this );
	ASSERT( 0 <= nTabIndex && nTabIndex < pContainerBar->BarGetCount() );
CExtTabOneNoteWnd * pWndTab =
	STATIC_DOWNCAST(
		CExtTabOneNoteWnd,
		pWndTabPageContainer->GetSafeTabWindow()
		);
	ASSERT_VALID( pWndTab );
bool bEnabled = 
	pWndTab->ItemEnabledGet( nTabIndex );
COLORREF clr = COLORREF( -1L );
	pWndTab->OnTabWndQueryItemColors(
		nTabIndex,
		true,
		false,
		bEnabled,		
		NULL,
		NULL,
		NULL,
		&clr
		);
	return clr;
}

class CExtTabbedToolControlBarOneNote::LocalTabWnd
	: public CExtTWPC < CExtTabOneNoteWnd >
{
protected:
	virtual INT OnTabWndGetParentSizingMargin(
		DWORD dwOrientation
		) const;
	virtual void OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		);
}; // class CExtTabbedToolControlBarOneNote::LocalTabWnd

INT CExtTabbedToolControlBarOneNote::LocalTabWnd::
	OnTabWndGetParentSizingMargin(
		DWORD dwOrientation
		) const
{
	ASSERT_VALID( this );
	dwOrientation;
	return 4;
}

void CExtTabbedToolControlBarOneNote::LocalTabWnd::
	OnTabWndEraseClientArea(
		CDC & dc,
		CRect & rcClient,
		CRect & rcTabItemsArea,
		CRect & rcTabNearBorderArea,
		DWORD dwOrientation,
		bool bGroupedMode
		)
{
	ASSERT_VALID( this );
	rcClient;
	rcTabItemsArea;
	rcTabNearBorderArea;
	dwOrientation;
	bGroupedMode;
	if( ! PmBridge_GetPM()->PaintDockerBkgnd(
			true,
			dc,
			this
			)
		)
		dc.FillSolidRect(
			&rcClient,
			PmBridge_GetPM()->GetColor(
				CExtPaintManager::CLR_3DFACE_OUT
				)
			);
	if( ! rcTabNearBorderArea.IsRectEmpty() )
	{
		CRect rcTabNearMargin( rcTabNearBorderArea ); // prepare tab border margin rect
		CRect rcColorLine( rcClient );

		switch( dwOrientation )
		{
		case __ETWS_ORIENT_TOP:
			rcTabNearMargin.bottom = rcTabNearMargin.top + 1;
			rcTabNearMargin.OffsetRect(0,-1);
			rcColorLine.top = rcTabNearMargin.bottom;
		break;
		case __ETWS_ORIENT_BOTTOM:
			rcTabNearMargin.top = rcTabNearMargin.bottom - 1;
			rcTabNearMargin.OffsetRect(0,1);
			rcColorLine.bottom = rcTabNearMargin.top; 
		break;
		case __ETWS_ORIENT_LEFT:
			rcTabNearMargin.right = rcTabNearMargin.left + 1;
			rcTabNearMargin.OffsetRect(-1,0);
			rcColorLine.left = rcTabNearMargin.right;
		break;
		case __ETWS_ORIENT_RIGHT:
			rcTabNearMargin.left = rcTabNearMargin.right - 1;
			rcTabNearMargin.OffsetRect(1,0);
			rcColorLine.right = rcTabNearMargin.left;
		break;
#ifdef _DEBUG
		default:
			ASSERT( FALSE );
		break;
#endif // _DEBUG
		} // switch( dwOrientation )

		// paint tab border margin
		dc.FillSolidRect(
			&rcTabNearMargin,
			PmBridge_GetPM()->GetColor( COLOR_3DSHADOW, this )
			);

		if( ::GetDeviceCaps( dc.m_hDC, BITSPIXEL ) > 8 )
		{
			LONG nSelIndex = SelectionGet();
			bool bEnabled = 
				ItemEnabledGet( nSelIndex );
			COLORREF clrBkDark = COLORREF( -1L );
			OnTabWndQueryItemColors(
				nSelIndex,
				false,
				false,
				bEnabled,
				NULL,
				NULL,
				NULL,
				&clrBkDark,
				NULL
				);
			dc.FillSolidRect(
				&rcColorLine,
				clrBkDark
				);
		}
		else 
		{
			dc.FillSolidRect( 
				&rcColorLine, 
				PmBridge_GetPM()->GetColor(
					SelectionGet() >= 0
						? COLOR_WINDOW
						: COLOR_3DFACE
						,
					this
					)
				);
		}
	} // if( ! rcTabNearBorderArea.IsRectEmpty() )
}

CExtTabWnd * CExtTabbedToolControlBarOneNote::LocalTabPageContainerWnd::
	OnTabWndGetTabImpl()
{
	ASSERT_VALID( this );
	return new LocalTabWnd;
}

CExtTabPageContainerOneNoteWnd * CExtTabbedToolControlBarOneNote::
	GetOneNoteTabPageContainer()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
CExtTabPageContainerWnd * pWndTabPageContainer = GetTabPageContainer();
	if( pWndTabPageContainer == NULL )
		return NULL;
	ASSERT_VALID( pWndTabPageContainer );
CExtTabPageContainerOneNoteWnd * pWndTabPageContainerOneNote =
		STATIC_DOWNCAST(
			CExtTabPageContainerOneNoteWnd,
			pWndTabPageContainer
			);
	return pWndTabPageContainerOneNote;
}

const CExtTabPageContainerOneNoteWnd * CExtTabbedToolControlBarOneNote::
	GetOneNoteTabPageContainer() const
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	return
		( const_cast < CExtTabbedToolControlBarOneNote * > ( this ) )
			-> GetOneNoteTabPageContainer();
}

bool CExtTabbedToolControlBarOneNote::OnBarEraseBk(
	CDC & dc,
	CExtToolControlBar * pToolBar
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
const CExtTabPageContainerOneNoteWnd * pWndTabPageContainer =
		GetOneNoteTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
	ASSERT_VALID( pToolBar );
	ASSERT( pToolBar->GetSafeHwnd() != NULL );
	ASSERT( LPCVOID(pToolBar->GetParent()) == LPCVOID(pWndTabPageContainer) );
INT nTabIndex = BarGetIndexOf( pToolBar );
	ASSERT( 0 <= nTabIndex && nTabIndex < BarGetCount() );
CExtTabOneNoteWnd * pWndTab =
		STATIC_DOWNCAST(
			CExtTabOneNoteWnd,
			((CExtTabWnd*)pWndTabPageContainer->GetSafeTabWindow())
			);
	ASSERT_VALID( pWndTab );
bool bEnabled = 
	pWndTab->ItemEnabledGet( nTabIndex );
COLORREF clr = COLORREF( -1L );
	pWndTab->OnTabWndQueryItemColors(
		nTabIndex,
		true,
		false,
		bEnabled,
		NULL,
		NULL,
		NULL,
		&clr
		);
CRect rcClient;
	pToolBar->GetClientRect( &rcClient );
	dc.FillSolidRect( &rcClient, clr );
	return true;
}

CSize CExtTabbedToolControlBarOneNote::GetButtonAdjustment(
	CExtToolControlBar * pBar
	) const
{
	ASSERT_VALID( this );
	return
		CExtTabbedToolControlBar::
			GetButtonAdjustment( pBar );
}

CExtToolControlBar * CExtTabbedToolControlBarOneNote::OnBarCreateObject()
{
	ASSERT_VALID( this );
CExtToolControlBar * pToolBar = NULL;
	try
	{
		pToolBar =
			new LocalToolBar;
	} // try
	catch( CException * pException )
	{
		ASSERT( FALSE );
		pException->Delete();
		pToolBar = NULL;
	} // catch( CException * pException )
	return pToolBar;
}

void CExtTabbedToolControlBarOneNote::OnPaintBackground(
	CDC & dc,
	const CRect & rcClient,
	const CRect & rcTabPageContainer
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( dc.GetSafeHdc() != NULL );
	rcClient;
CExtTabPageContainerOneNoteWnd * pWndTabPageContainer =
		GetOneNoteTabPageContainer();
	ASSERT_VALID( pWndTabPageContainer );
	ASSERT( pWndTabPageContainer->GetSafeHwnd() != NULL );
	pWndTabPageContainer;
CRect rcBorder( rcTabPageContainer );
	rcBorder.InflateRect( 1, 1 );
CRect rcTabItemsArea =
	pWndTabPageContainer->GetSafeTabWindow()->
		GetRectTabItemsArea();
	pWndTabPageContainer->GetSafeTabWindow()->
		ClientToScreen( &rcTabItemsArea ); 
	ScreenToClient( &rcTabItemsArea ); 
DWORD dwOrientation = pWndTabPageContainer->OrientationGet();
	switch( dwOrientation )
	{
	case __ETWS_ORIENT_TOP:
		rcBorder.top = rcTabItemsArea.bottom;
	break;
	case __ETWS_ORIENT_BOTTOM:
		rcBorder.bottom = rcTabItemsArea.top + 1;
		break;
	case __ETWS_ORIENT_LEFT:
		rcBorder.left = rcTabItemsArea.right;
	break;
	case __ETWS_ORIENT_RIGHT:
		rcBorder.right = rcTabItemsArea.left;
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
		return;
#endif // _DEBUG
	} // switch( dwOrientation )
COLORREF clrMargin = PmBridge_GetPM()->GetColor( COLOR_3DSHADOW );
	dc.Draw3dRect( &rcBorder, clrMargin, clrMargin );
}

#endif // __EXT_MFC_NO_TABBED_TOOLBAR_ONENOTE

#endif // __EXT_MFC_NO_TABBED_TOOLBAR
