// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_MFC_NO_DATE_PICKER_POPUP)
	#if (!defined __EXT_POPUP_CTRL_MENU_H)
		#include <ExtPopupCtrlMenu.h>
	#endif // (!defined __EXT_POPUP_CTRL_MENU_H)
#endif // (!defined __EXT_MFC_NO_DATE_PICKER_POPUP)

#if (!defined __EXT_MFC_NO_DATETIMEWND)
	#if (!defined __EXT_DATE_PICKER_H)
		#include <ExtDatePicker.h>
	#endif (!defined __EXT_DATE_PICKER_H)
#endif // (!defined __EXT_MFC_NO_DATETIMEWND)

#if (!defined __EXT_MFC_NO_DURATIONWND)

#if (!defined __EXT_MEMORY_DC_H)
	#include "ExtMemoryDC.h"
#endif

#if (!defined __EXT_DURATIONWND_H)
	#include "ExtDurationWnd.h"
#endif // (!defined __EXT_DURATIONWND_H)

#include <Resources/Resource.h>

#include <stdlib.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define __EXT_DURATIONWND_SPIN_BTN_ID		0x123
#define __EXT_DURATIONWND_DROPDOWN_BTN_ID	0x124

//////////////////////////////////////////////////////////////////////////
// CExtDurationWnd::CHANGING_NOTIFICATION

CExtDurationWnd::CHANGING_NOTIFICATION::CHANGING_NOTIFICATION(
	bool bChangedFinally,
	const COleDateTimeSpan & dtSpanOld,
	const COleDateTimeSpan & dtSpanNew,
	LPARAM lParamCookie
	)
	: m_bChangedFinally( bChangedFinally )
	, m_lParamCookie( lParamCookie )
	, m_dtSpanOld( dtSpanOld )
	, m_dtSpanNew( dtSpanNew )
{
}

CExtDurationWnd::CHANGING_NOTIFICATION::operator WPARAM() const
{
	WPARAM wParam = reinterpret_cast < WPARAM > ( this );
	return wParam;
}

const CExtDurationWnd::CHANGING_NOTIFICATION *
	CExtDurationWnd::CHANGING_NOTIFICATION::FromWPARAM( WPARAM wParam )
{
	CExtDurationWnd::CHANGING_NOTIFICATION * pSN =
		reinterpret_cast < CExtDurationWnd::CHANGING_NOTIFICATION * > ( wParam );
	ASSERT( pSN != NULL );
	return pSN;
}

LRESULT CExtDurationWnd::CHANGING_NOTIFICATION::Notify( HWND hWndNotify ) const
{
	ASSERT( hWndNotify != NULL && ::IsWindow( hWndNotify ) );
	return 
		::SendMessage(
			hWndNotify,
			CExtDurationWnd::g_nMsgChangingNotification,
			*this,
			m_lParamCookie
			);
}

//////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE( CExtDurationWnd, CWnd );
IMPLEMENT_CExtPmBridge_MEMBERS( CExtDurationWnd );

CExtDurationWnd::CExtDurationWnd()
	: m_bDirectCreateCall( false )
	, m_bInitialized( false )
	, m_bUpdatingLayout( false )
	, m_bAutoDeleteWindow( false )
	, m_bSpinButtonVisible( true )
	, m_bReadOnly( false )
	, m_bDropDownButtonVisible( true )
	, m_bContinuousScrolling( false )
	, m_rcSpin( 0, 0, 0, 0 )
	, m_rcDropDown( 0, 0, 0, 0 )
	, m_pSelectedItem( NULL )
	, m_pLastInputItem( NULL )
	, m_clrBackground( COLORREF(-1L) ) 
	, m_clrText( COLORREF(-1L) )
	, m_lParamCookie( 0 )
	, m_hWndNotificationReceiver( NULL )
{
	m_dtSpan = 0.0;
	m_eStatus = CExtDurationWnd::valid;
	m_eAlign = CExtDurationWnd::left;
	VERIFY( RegisterWndClass() );

	PmBridge_Install();
}

CExtDurationWnd::~CExtDurationWnd()
{
	PmBridge_Uninstall();

	while( m_arrItems.GetSize() > 0 )
	{
		ITEM_INFO * pII = m_arrItems[0];
		m_arrItems.RemoveAt(0);
		delete pII;
		pII = NULL;
	}
}

BEGIN_MESSAGE_MAP(CExtDurationWnd, CWnd)
	//{{AFX_MSG_MAP(CExtDurationWnd)
	ON_WM_GETDLGCODE()
	ON_WM_LBUTTONDOWN()
	ON_WM_KEYDOWN()
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_WM_NCCALCSIZE()
	ON_WM_NCPAINT()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_MOUSEWHEEL()
	ON_WM_ENABLE()
	ON_BN_CLICKED(__EXT_DURATIONWND_DROPDOWN_BTN_ID, OnDropDownButtonClicked)
	//}}AFX_MSG_MAP
	ON_WM_ACTIVATEAPP()
	ON_WM_SYSCOLORCHANGE()
	__EXT_MFC_SAFE_ON_WM_SETTINGCHANGE()
	ON_MESSAGE(WM_DISPLAYCHANGE, OnDisplayChange)
	ON_MESSAGE(__ExtMfc_WM_THEMECHANGED, OnThemeChanged)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtDurationWnd message handlers

HCURSOR CExtDurationWnd::g_hCursor = ::LoadCursor( NULL, IDC_ARROW );
bool CExtDurationWnd::g_bWndClassRegistered = false;
const UINT CExtDurationWnd::g_nMsgChangingNotification =
	::RegisterWindowMessage(
		_T("CExtDurationWnd::g_nMsgChangingNotification")
		);

bool CExtDurationWnd::RegisterWndClass()
{
	if( g_bWndClassRegistered )
		return true;
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo(
			hInst,
			__EXT_DURATIONWND_CLASS_NAME,
			&_wndClassInfo
			)
		)
	{
		_wndClassInfo.style = CS_GLOBALCLASS|CS_DBLCLKS;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor =
			( g_hCursor != NULL )
				? g_hCursor
				: ::LoadCursor( NULL, IDC_ARROW )
				;
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_DURATIONWND_CLASS_NAME;
		if( !::AfxRegisterClass( & _wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
	g_bWndClassRegistered = true;
	return true;
}

#ifdef _DEBUG
void CExtDurationWnd::AssertValid() const
{
	CWnd::AssertValid();
	ASSERT( !m_bInitialized || m_wndSpin.GetSafeHwnd() != NULL );
	ASSERT( !m_bInitialized || m_wndDropDown.GetSafeHwnd() != NULL );
}
void CExtDurationWnd::Dump(CDumpContext& dc) const
{
	CWnd::Dump( dc );
}
#endif // _DEBUG

bool CExtDurationWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( IDC_STATIC )
	DWORD dwWindowStyle, // = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN
	CCreateContext * pContext // = NULL
	)
{
	ASSERT_VALID( this );
	if( ! RegisterWndClass() )
	{
		ASSERT( FALSE );
		return false;
	}
	m_bDirectCreateCall = true;
	if( ! CWnd::Create(
		__EXT_DURATIONWND_CLASS_NAME,
		NULL,
		dwWindowStyle,
		rcWnd,
		pParentWnd,
		nDlgCtrlID,
		pContext
		)
		)
	{
		ASSERT( FALSE );
		return false;
	}
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		AfxThrowMemoryException();
	} // if( ! _CreateHelper() )
	m_bInitialized = true;
	UpdateDurationWnd( true, false );
	return true;
}

bool CExtDurationWnd::_CreateHelper()
{
	ASSERT_VALID( this );
	if( m_bInitialized )
		return true;
	OnInitializeItemsArray();
	VERIFY( OnCreateSpinButton() );
	VERIFY( OnCreateDropDownButton() );
	UpdateDurationWnd( true, false );
	return true;
}

void CExtDurationWnd::PostNcDestroy() 
{
	if( m_bAutoDeleteWindow )
		delete this;
}

void CExtDurationWnd::PreSubclassWindow() 
{
	CWnd::PreSubclassWindow();
	if( m_bDirectCreateCall )
		return;
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		AfxThrowMemoryException();
	} // if( ! _CreateHelper() )
	m_bInitialized = true;
	UpdateDurationWnd( true, false );
}

BOOL CExtDurationWnd::PreCreateWindow(CREATESTRUCT& cs)
{
	ASSERT_VALID( this );
	if( !CWnd::PreCreateWindow(cs) )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	cs.style &= ~(WS_BORDER);
	return TRUE;
}

#if _MFC_VER < 0x700
void CExtDurationWnd::OnActivateApp(BOOL bActive, HTASK hTask) 
#else
void CExtDurationWnd::OnActivateApp(BOOL bActive, DWORD hTask) 
#endif
{
	CWnd::OnActivateApp(bActive, hTask);
	if( ! bActive )
		SendMessage( WM_CANCELMODE );
}

void CExtDurationWnd::OnSysColorChange() 
{
	ASSERT_VALID( this );
	CWnd::OnSysColorChange();
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnSysColorChange( this );
	g_CmdManager.OnSysColorChange( pPM, this );
	UpdateDurationWnd( true, true );
}

LRESULT CExtDurationWnd::OnDisplayChange( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
LRESULT lResult = CWnd::OnDisplayChange( wParam, lParam );
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnDisplayChange( this, (INT)wParam, CPoint(lParam) );
	g_CmdManager.OnDisplayChange( pPM, this, (INT)wParam, CPoint(lParam) );
	UpdateDurationWnd( true, true );
	return lResult;
}

LRESULT CExtDurationWnd::OnThemeChanged( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
LRESULT lResult = Default();
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnThemeChanged( this, wParam, lParam );
	g_CmdManager.OnThemeChanged( pPM, this, wParam, lParam );
	UpdateDurationWnd( true, true );
	return lResult;
}

void CExtDurationWnd::OnSettingChange(UINT uFlags, __EXT_MFC_SAFE_LPCTSTR lpszSection) 
{
	ASSERT_VALID( this );
	CWnd::OnSettingChange(uFlags, lpszSection);
CExtPaintManager * pPM = PmBridge_GetPM();
	g_PaintManager.OnSettingChange( this, uFlags, lpszSection );
	g_CmdManager.OnSettingChange( pPM, this, uFlags, lpszSection );
	UpdateDurationWnd( true, true );
}

UINT CExtDurationWnd::OnGetDlgCode() 
{
	return DLGC_WANTARROWS | DLGC_WANTCHARS | DLGC_WANTALLKEYS;
}

void CExtDurationWnd::UpdateDurationWnd(
	bool bRecalcLayout, // = true,
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
	if(		GetSafeHwnd() == NULL
		||	( ! ::IsWindow( m_hWnd ) )
		||	( !m_bInitialized )
		)
		return;
	if( bRecalcLayout )
	{
		_RecalcDuration(); // should be before _RecalcLayout() !!!
		_RecalcLayout();
	}
	if(		bUpdate
		&&	GetSafeHwnd() != NULL
		&&	(GetStyle() & WS_VISIBLE) != 0
		)
	{
		RedrawWindow( 
			NULL, 
			NULL, 
			RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_FRAME | RDW_UPDATENOW | RDW_ERASENOW
			);

// 		Invalidate();
// 		UpdateWindow();
// 		if(		m_wndDropDown.GetSafeHwnd() != NULL
// 			&&	(m_wndDropDown.GetStyle() & WS_VISIBLE) != 0
// 			)
// 			m_wndDropDown.RedrawWindow( 
// 				NULL, 
// 				NULL, 
// 				RDW_INVALIDATE | RDW_NOCHILDREN | RDW_FRAME | RDW_UPDATENOW | RDW_ERASENOW
// 				);
// 		if(		m_wndSpin.GetSafeHwnd() != NULL
// 			&&	(m_wndSpin.GetStyle() & WS_VISIBLE) != 0
// 			)
// 			m_wndSpin.RedrawWindow( 
// 				NULL, 
// 				NULL, 
// 				RDW_INVALIDATE | RDW_NOCHILDREN | RDW_FRAME | RDW_UPDATENOW | RDW_ERASENOW
// 				);
	}
}

CSize CExtDurationWnd::OnQueryBorderSize() const
{
	ASSERT_VALID( this );
	return CSize( 1, 1 );
}

INT CExtDurationWnd::OnQuerySpinButtonWidth() const
{
	ASSERT_VALID( this );
	return ::GetSystemMetrics( SM_CYVTHUMB );
}

INT CExtDurationWnd::OnQueryDropDownButtonWidth() const
{
	ASSERT_VALID( this );
	return ::GetSystemMetrics( SM_CYVTHUMB );
}

BOOL CExtDurationWnd::OnCreateSpinButton()
{
	ASSERT_VALID( this );
	if( m_wndSpin.GetSafeHwnd() != NULL )
		return TRUE;

	CRect rcSpin(0,0,1,1);
	BOOL bRet = m_wndSpin.Create(
		WS_CHILD | UDS_ALIGNRIGHT | UDS_AUTOBUDDY,
		rcSpin,
		this,
		UINT( __EXT_DURATIONWND_SPIN_BTN_ID )
		);

	if( !bRet )
		TRACE(_T("CExtDurationWnd: Can not create spin button!\n"));

	return bRet;
}

BOOL CExtDurationWnd::OnCreateDropDownButton()
{
	ASSERT_VALID( this );
	if( m_wndDropDown.GetSafeHwnd() != NULL )
		return TRUE;

	CRect rcDropDown(0,0,1,1);
	BOOL bRet = m_wndDropDown.Create(
		_T(""),
		WS_CHILD | BS_PUSHBUTTON,
		rcDropDown,
		this,
		UINT( __EXT_DURATIONWND_DROPDOWN_BTN_ID )
		);

	if( !bRet )
		TRACE(_T("CExtDurationWnd: Can not create dropdown button!\n"));

	return bRet;
}

bool CExtDurationWnd::OnShowDropDownMenu()
{
	ASSERT_VALID( this );
	return false;
}

void CExtDurationWnd::OnSpinButtonSyncVisibility()
{
	ASSERT_VALID( this );
	eStatus_t eStatus = GetStatus();
	m_wndSpin.ShowWindow(
		IsSpinButtonVisible() && eStatus == CExtDurationWnd::valid
			? SW_SHOW 
			: SW_HIDE
		);	
}

void CExtDurationWnd::OnDropDownButtonSyncVisibility()
{
	ASSERT_VALID( this );
	m_wndDropDown.ShowWindow(
		IsDropDownButtonVisible() ? SW_SHOW : SW_HIDE
		);	
}

void CExtDurationWnd::OnSize(UINT nType, int cx, int cy) 
{
	ASSERT_VALID( this );
	CWnd::OnSize(nType, cx, cy);
	UpdateDurationWnd( true, true );
}

BOOL CExtDurationWnd::OnEraseBkgnd(CDC* pDC) 
{
	ASSERT_VALID( this );
	pDC;
	return TRUE;
}

void CExtDurationWnd::OnPaint() 
{
	ASSERT_VALID( this );
	CPaintDC dcPaint( this );
	CRect rcClient;
	GetClientRect( &rcClient );
	if( rcClient.IsRectEmpty() )
		return;
CExtMemoryDC dc(
		&dcPaint //,
//		&rcClient
		);
	OnDurationDrawEntire( dc, rcClient );
	PmBridge_GetPM()->OnPaintSessionComplete( this );
}

HFONT CExtDurationWnd::OnQueryFont() const
{
	ASSERT_VALID( this );
	HFONT hFont = (HFONT)
		::SendMessage( m_hWnd, WM_GETFONT, 0L, 0L );
	if( hFont == NULL )
	{
		HWND hWndParent = ::GetParent( m_hWnd );
		if( hWndParent != NULL )
			hFont = (HFONT)
				::SendMessage( hWndParent, WM_GETFONT, 0L, 0L );
	} // if( hFont == NULL )
	if( hFont == NULL )
	{
		hFont = (HFONT)::GetStockObject( DEFAULT_GUI_FONT );
		if( hFont == NULL )
			hFont = (HFONT)::GetStockObject( SYSTEM_FONT );
	} // if( hFont == NULL )
	return hFont;
}

COLORREF CExtDurationWnd::OnQueryItemBkColor( const ITEM_INFO * pII ) const
{
	ASSERT_VALID( this );
	ASSERT( pII != NULL );
	const CWnd* pWndThis = this;
	if(		pII != NULL 
		&&	GetFocus() == pWndThis
		&&	pII == m_pSelectedItem
		)
		return PmBridge_GetPM()->GetColor( COLOR_HIGHLIGHT, (CObject*)this );
	return COLORREF(-1L);
}

COLORREF CExtDurationWnd::OnQueryItemTextColor( const ITEM_INFO * pII ) const
{
	ASSERT_VALID( this );
	ASSERT( pII != NULL );
	COLORREF clrText = GetTextColor();
	if( clrText == COLORREF(-1L) )
	{
		const CWnd * pWndThis = this;
		if(		pII != NULL 
			&&	GetFocus() == pWndThis
			&&	pII == m_pSelectedItem
			)
			clrText = PmBridge_GetPM()->GetColor( IsWindowEnabled() ? COLOR_HIGHLIGHTTEXT : COLOR_GRAYTEXT, (CObject*)this );
		else
			clrText = PmBridge_GetPM()->GetColor( IsWindowEnabled() ? COLOR_BTNTEXT : COLOR_GRAYTEXT, (CObject*)this );
	}
	return clrText;
}

void CExtDurationWnd::OnDurationDrawEntire(
	CDC & dc,
	const CRect & rcClient
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );

HFONT hFont = OnQueryFont();
	ASSERT( hFont != NULL );
HGDIOBJ hOldFont = NULL;
	if( hFont != NULL )
		hOldFont = ::SelectObject( dc, (HGDIOBJ)hFont );
INT nOldBkMode = dc.SetBkMode( TRANSPARENT );

	// draw background
	OnDurationEraseClientArea( 
		dc, 
		rcClient 
		);
	
	// draw border
	CSize szBorder = OnQueryBorderSize();
	if( szBorder.cx > 0 || szBorder.cy > 0 )
		OnDurationDrawBorder( 
			dc, 
			rcClient 
			);

	for( LONG nItem = LONG(m_arrItems.GetSize()) - 1; nItem >= 0; nItem-- )
	{
		ITEM_INFO * pII = m_arrItems[ nItem ];
		ASSERT( pII != NULL );
		if(		pII != NULL 
			&&	pII->m_bVisible
			&&	(!pII->m_rcRect.IsRectEmpty())
			)
		{
			COLORREF clrBackground = OnQueryItemBkColor( pII );
			COLORREF clrText = OnQueryItemTextColor( pII );

			if( clrBackground != COLORREF( -1L ) )
				dc.FillSolidRect(
					pII->m_rcRect, 
					clrBackground
					);

			COLORREF clrOldText = dc.SetTextColor( clrText );
			
			CExtSafeString sText = OnQueryItemText( pII );
			dc.DrawText(
				sText,
				sText.GetLength(),
				&pII->m_rcRect,
				DT_SINGLELINE | DT_CENTER | DT_VCENTER
				);

			dc.SetTextColor( clrOldText );
		}
	}

	dc.SetBkMode( nOldBkMode );
	if( hFont != NULL )
		::SelectObject( dc, hOldFont );
}

void CExtDurationWnd::OnDurationEraseClientArea(
	CDC & dc,
	const CRect & rcClient
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( ! dc.RectVisible(&rcClient) )
		return;

	COLORREF clrBackground = GetBkColor();

	if( clrBackground == COLORREF(-1L) )
		dc.FillSolidRect(
			&rcClient,
			IsWindowEnabled() 
				? PmBridge_GetPM()->GetColor( COLOR_WINDOW, (CObject*)this )
				: ::GetSysColor( COLOR_3DFACE )
			);
	else
		dc.FillSolidRect(
			&rcClient,
			clrBackground
			);
}

void CExtDurationWnd::OnDurationDrawBorder(
	CDC & dc,
	const CRect & rcClient
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
    PmBridge_GetPM()->PaintResizableBarChildNcAreaRect(
        dc,
        rcClient,
        (CObject*)this
        );
}

void CExtDurationWnd::ShowSpinButton( 
	bool bShow // = true 
	)
{
	ASSERT_VALID( this );
	m_bSpinButtonVisible = bShow;
	UpdateDurationWnd( true, true );
}

bool CExtDurationWnd::IsSpinButtonVisible() const
{
	ASSERT_VALID( this );
	return m_bSpinButtonVisible;
}

void CExtDurationWnd::ShowDropDownButton( 
	bool bShow // = true 
	)
{
	ASSERT_VALID( this );
	m_bDropDownButtonVisible = bShow;
	UpdateDurationWnd( true, true );
}

bool CExtDurationWnd::IsDropDownButtonVisible() const
{
	ASSERT_VALID( this );
	return false;
}

void CExtDurationWnd::SetReadOnly( 
	bool bReadOnly // = true 
	)
{
	ASSERT_VALID( this );
	m_bReadOnly = bReadOnly;
	UpdateDurationWnd( true, true );
}

bool CExtDurationWnd::IsReadOnly() const
{
	ASSERT_VALID( this );
	return m_bReadOnly;	
}

BOOL CExtDurationWnd::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	ASSERT_VALID( this );
	if(		wParam == __EXT_DURATIONWND_SPIN_BTN_ID 
		&&	(!IsReadOnly())
		)
	{
		NM_UPDOWN * pNM = (NM_UPDOWN*)lParam;
		ASSERT( pNM != NULL );
		if( pNM->hdr.code == UDN_DELTAPOS )
		{
			ScrollCurrentItem( pNM->iDelta < 0 ? +1 : -1 );
			SetFocus();
		}
	} 
	return CWnd::OnNotify(wParam, lParam, pResult);
}

CExtDurationWnd::ITEM_INFO * CExtDurationWnd::OnInitializeItemYear(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	lpszTextBefore;
	lpszTextAfter;
	return NULL;
}

CExtDurationWnd::ITEM_INFO * CExtDurationWnd::OnInitializeItemMonth(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	lpszTextBefore;
	lpszTextAfter;
	return NULL;
}

CExtDurationWnd::ITEM_INFO * CExtDurationWnd::OnInitializeItemDay(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	ITEM_INFO * pDayII = 
		new ITEM_INFO
		(
			CExtDurationWnd::day,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
		);
	ASSERT( pDayII != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pDayII->AddDependentItem( pII );
	}
	m_arrItems.Add( pDayII );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pDayII->AddDependentItem( pII );
	}
	return pDayII;
}

CExtDurationWnd::ITEM_INFO * CExtDurationWnd::OnInitializeItemHour(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	ITEM_INFO * pHourII = 
		new ITEM_INFO
		(
			CExtDurationWnd::hour,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
		);
	ASSERT( pHourII != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pHourII->AddDependentItem( pII );
	}
	m_arrItems.Add( pHourII );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pHourII->AddDependentItem( pII );
	}
	return pHourII;
}

CExtDurationWnd::ITEM_INFO * CExtDurationWnd::OnInitializeItemMinute(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	ITEM_INFO * pMinuteII = 
		new ITEM_INFO
		(
			CExtDurationWnd::minute,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
		);
	ASSERT( pMinuteII != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pMinuteII->AddDependentItem( pII );
	}
	m_arrItems.Add( pMinuteII );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pMinuteII->AddDependentItem( pII );
	}	
	return pMinuteII;
}

CExtDurationWnd::ITEM_INFO * CExtDurationWnd::OnInitializeItemSecond(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	CExtSafeString sTimeSeparator = OnQueryTimeSeparator();
	ITEM_INFO * pSecondII = 
		new ITEM_INFO
		(
			CExtDurationWnd::second,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
		);
	ASSERT( pSecondII != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pSecondII->AddDependentItem( pII );
	}
	m_arrItems.Add( pSecondII );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pSecondII->AddDependentItem( pII );
	}
	return pSecondII;
}

CExtDurationWnd::ITEM_INFO * CExtDurationWnd::OnInitializeItemDesignator(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	lpszTextBefore;
	lpszTextAfter;
	return NULL;
}

void CExtDurationWnd::OnInitializeItemsArray()
{
	ASSERT_VALID( this );
	CExtSafeString sTimeSeparator = OnQueryTimeSeparator();
	// day
	CString sDays;
	if( ! g_ResourceManager->LoadString( sDays, IDS_EXT_DURATION_DAYS ) )
		sDays = _T(" Days ");
	OnInitializeItemDay( NULL, sDays );
	// hour
	OnInitializeItemHour( NULL, sTimeSeparator );
	// minute
	OnInitializeItemMinute( NULL, sTimeSeparator );
	// second
	OnInitializeItemSecond( NULL, NULL );
}

void CExtDurationWnd::SetDuration( const COleDateTimeSpan & dtSpan )
{
	ASSERT_VALID( this );
COleDateTimeSpan dtSpanTmp( dtSpan );
	if( double( dtSpanTmp ) < 0.0 )
		return;
//	{
//		ASSERT( FALSE );
//		dtSpanTmp = double( 0.0 );
//	}
COleDateTimeSpan dtSpanPrev = m_dtSpan;
	if( OnValueChanging( dtSpanPrev, dtSpanTmp ) )
	{
		m_dtSpan = dtSpanTmp;
		OnValueChanged( dtSpanPrev, m_dtSpan );
		UpdateDurationWnd( true, true );
	}
}

void CExtDurationWnd::SetDuration( double dSpan )
{
	ASSERT_VALID( this );
	SetDuration( COleDateTimeSpan( dSpan ) );
}

void CExtDurationWnd::SetDuration( LONG nDays, INT nHours, INT nMins, INT nSecs )
{
	ASSERT_VALID( this );
	SetDuration( COleDateTimeSpan( nDays, nHours, nMins, nSecs ) );
}

void CExtDurationWnd::SetDuration( LONG nTotalSeconds )
{
	ASSERT_VALID( this );
	SetDuration( COleDateTimeSpan( 0, 0, 0, nTotalSeconds ) );
}

COleDateTimeSpan CExtDurationWnd::GetDuration() const
{
	ASSERT_VALID( this );
	return m_dtSpan;
}

CRect CExtDurationWnd::OnQueryItemMargins( const ITEM_INFO * pII ) const
{
	ASSERT_VALID( this );
	ASSERT( pII != NULL );
	CRect rcItemMargins( 1, 1, 1, 1 );
	if( pII->m_eItemType == CExtDurationWnd::label )
		rcItemMargins.SetRect( 0, 1, 0, 1 );
	return rcItemMargins;
}

LONG CExtDurationWnd::_ItemGetIndexOf(
	const ITEM_INFO * pII
	) const
{
	ASSERT_VALID( this );
	if( pII == NULL )
		return -1L;
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
	{
		const ITEM_INFO * pII2 = m_arrItems[ nItem ];
		ASSERT( pII2 != NULL );
		if( pII2 == pII )
			return nItem;
	}
	return -1L;
}

CExtDurationWnd::ITEM_INFO * CExtDurationWnd::ItemGet( 
	eItem_t eItem 
	) const
{
	// ASSERT_VALID( this );
	ASSERT( eItem != CExtDurationWnd::label );
	ITEM_INFO * pII = NULL;
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
	{
		pII = m_arrItems[ nItem ];
		ASSERT( pII != NULL );
		if(		pII != NULL 
			&&	pII->m_eItemType != CExtDurationWnd::label
			&&	pII->m_eItemType == eItem
			)
			break;
	}
	return pII;
}

void CExtDurationWnd::_RecalcDuration()
{
	ASSERT_VALID( this );

	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
	{
		ITEM_INFO * pII = m_arrItems[ nItem ];
		ASSERT( pII != NULL );
		if( pII != NULL )
			pII->m_nValue = 0;
	}

	ITEM_INFO * pII = NULL;

	INT nDays = m_dtSpan.GetDays();
	INT nHours = m_dtSpan.GetHours();
	INT nMinutes = m_dtSpan.GetMinutes();
	INT nSeconds = m_dtSpan.GetSeconds();

	pII = ItemGet( CExtDurationWnd::day );
	if(		pII != NULL 
		&&	pII->m_bVisible
		)
	{
		pII->m_nValue = nDays;
		nDays = 0;
	}

	pII = ItemGet( CExtDurationWnd::hour );
	if(		pII != NULL 
		&&	pII->m_bVisible
		)
	{
		pII->m_nValue = 
			nHours + 
			(nDays * 24);
		nDays = 0;
		nHours = 0;
	}

	pII = ItemGet( CExtDurationWnd::minute );
	if(		pII != NULL 
		&&	pII->m_bVisible
		)
	{
		pII->m_nValue = 
			nMinutes + 
			(nDays * 60 * 24) + 
			(nHours * 60);
		nDays = 0;
		nHours = 0;
		nMinutes = 0;
	}

	pII = ItemGet( CExtDurationWnd::second );
	if(		pII != NULL 
		&&	pII->m_bVisible
		)
	{
		pII->m_nValue = 
			nSeconds +
			(nDays * 60 * 60 * 24) + 
			(nHours * 60 * 60) +
			(nMinutes * 60);
		nDays = 0;
		nHours = 0;
		nMinutes = 0;
		nSeconds = 0;
	}
}

bool CExtDurationWnd::IsItemVisible( eItem_t eItem ) const
{
	ASSERT_VALID( this );
	ASSERT( eItem != CExtDurationWnd::label );
	ITEM_INFO * pII = ItemGet( eItem );
	ASSERT( pII != NULL );
	if( pII == NULL )
		return false;
	return pII->m_bVisible;
}

void CExtDurationWnd::SetShowItem( 
	eItem_t eItem,
	bool bShow, // = true 
	bool bUpdate // = true 
	)
{
	ASSERT_VALID( this );
	ASSERT( eItem != CExtDurationWnd::label );
	ITEM_INFO * pII = ItemGet( eItem );
	ASSERT( pII != NULL );
	if(		pII != NULL 
		&&	pII->m_bVisible != bShow
		)
	{
		pII->m_bVisible = bShow;
//		for( LONG nItem = 0; nItem < pII->m_arrDependentItems.GetSize(); nItem++ )
//		{
//			ITEM_INFO * pDependentII = pII->m_arrDependentItems[ nItem ];
//			ASSERT( pDependentII != NULL );
//			pDependentII->m_bVisible = bShow;
//		}
		UpdateDurationWnd( true, bUpdate );
	}
}

void CExtDurationWnd::OnItemSyncVisibility( ITEM_INFO * pII )
{
	ASSERT_VALID( this );
	ASSERT( pII != NULL );
	if( pII == NULL )
		return;
	if( pII->m_eItemType == CExtDurationWnd::label )
	{
		// search the owner item
		ITEM_INFO * pIIOwner = NULL;
		for( LONG nItem1 = 0; nItem1 < m_arrItems.GetSize() && pIIOwner == NULL; nItem1++ )
		{
			ITEM_INFO * pII1 = m_arrItems[ nItem1 ];
			ASSERT( pII1 != NULL );
			if( pII1 != NULL )
			{
				for( LONG nItem2 = 0; nItem2 < pII1->m_arrDependentItems.GetSize() && pIIOwner == NULL; nItem2++ )
				{
					ITEM_INFO * pDependentII = pII1->m_arrDependentItems[ nItem2 ];
					ASSERT( pDependentII != NULL );
					if( pDependentII == pII )
					{
						pIIOwner = pII1;
						break;
					}
				}
			}
		} // for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
		if( pIIOwner != NULL )
		{
			CExtSafeString sTimeSeparator = OnQueryTimeSeparator();
			CExtSafeString sDateSeparator = OnQueryDateSeparator();
			if( !pIIOwner->m_bVisible )
				pII->m_bVisible = false;
			else if( IsFirstVisibleItem( pIIOwner ) && pII->m_bBefore && ( pII->m_sText == sTimeSeparator || pII->m_sText == sDateSeparator ) )
				pII->m_bVisible = false;
			else if( IsLastVisibleItem( pIIOwner ) && !pII->m_bBefore && ( pII->m_sText == sTimeSeparator || pII->m_sText == sDateSeparator ) )
				pII->m_bVisible = false;
			else
				pII->m_bVisible = true;
		}
	} // if( pII->m_eItemType == CExtDurationWnd::label )
}

void CExtDurationWnd::_RecalcLayout()
{
	ASSERT_VALID( this );
	if(		m_bUpdatingLayout
		||	GetSafeHwnd() == NULL
		||	( ! ::IsWindow( GetSafeHwnd() ) )
		)
		return;

	m_bUpdatingLayout = true;

	m_rcSpin.SetRectEmpty();
	m_rcDropDown.SetRectEmpty();

LONG nItem = 0;
	for( nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
	{
		ITEM_INFO * pII = m_arrItems[ nItem ];
		ASSERT( pII != NULL );
		if( pII != NULL )
		{
			pII->m_rcRect.SetRectEmpty();
			OnItemSyncVisibility( pII );
		}
	}

	CRect rcClient;
	GetClientRect( &rcClient );

	// inner client area excluding border area
	CRect rcDurationInnerArea( rcClient );
	CSize szBorder = OnQueryBorderSize();
	if( szBorder.cx > 0 || szBorder.cy > 0 )
		rcDurationInnerArea.DeflateRect( szBorder );

	// dropdown button
	if( IsDropDownButtonVisible() )
	{
		INT nDropDownButtonWidth = 
			OnQueryDropDownButtonWidth();
		m_rcDropDown.CopyRect( &rcDurationInnerArea );
		m_rcDropDown.left = m_rcDropDown.right - nDropDownButtonWidth;
		rcDurationInnerArea.DeflateRect( 0, 0, m_rcDropDown.Width(), 0 );
		
		// Adjust dropdown button:
		m_wndDropDown.SetWindowPos(
			NULL, 
			m_rcDropDown.left,
			m_rcDropDown.top,
			m_rcDropDown.Width(),
			m_rcDropDown.Height(),
			SWP_NOZORDER | SWP_NOACTIVATE
			);
		m_wndDropDown.EnableWindow( 
				IsWindowEnabled() 
			&&	(!IsReadOnly())
			);
	}
	OnDropDownButtonSyncVisibility();

	// spin button
	if( IsSpinButtonVisible() )
	{
		INT nSpinButtonWidth = 
			OnQuerySpinButtonWidth();
		m_rcSpin.CopyRect( &rcDurationInnerArea );
		m_rcSpin.left = m_rcSpin.right - nSpinButtonWidth;
		rcDurationInnerArea.DeflateRect( 0, 0, m_rcSpin.Width(), 0 );
		
		// Adjust spin button:
		m_wndSpin.SetWindowPos(
			NULL, 
			m_rcSpin.left,
			m_rcSpin.top,
			m_rcSpin.Width(),
			m_rcSpin.Height(),
			SWP_NOZORDER | SWP_NOACTIVATE
			);
		m_wndSpin.EnableWindow( 
				IsWindowEnabled() 
			&&	(!IsReadOnly())
			);
	}
	OnSpinButtonSyncVisibility();

	rcDurationInnerArea.DeflateRect( 
		0, 0, 
		( IsDropDownButtonVisible() || IsSpinButtonVisible() ) ? 1 : 2, 0 
		);

	eStatus_t eStatus = GetStatus();
	if( eStatus == CExtDurationWnd::valid )
	{
		CSize szContent = CSize(0,0);
		INT nMostLeft = rcDurationInnerArea.right;

		// items rect
		for( nItem = INT(m_arrItems.GetSize()) - 1; nItem >= 0; nItem-- )
		{
			ITEM_INFO * pII = m_arrItems[ nItem ];
			ASSERT( pII != NULL );
			if(		pII != NULL 
				&&	pII->m_bVisible
				)
			{
				CExtSafeString sText = OnQueryItemText( pII );
				CRect rcText(0,0,0,0);
				if( !sText.IsEmpty() )
				{
					CClientDC dcScreen( NULL );
					HFONT hFont = OnQueryFont();
					ASSERT( hFont != NULL );
					HGDIOBJ hOldFont = NULL;
					if( hFont != NULL )
						hOldFont = ::SelectObject( dcScreen, (HGDIOBJ)hFont );
					dcScreen.DrawText(
						sText,
						sText.GetLength(),
						&rcText,
						DT_SINGLELINE | DT_LEFT | DT_CALCRECT
						);
					if( hOldFont != NULL )
						dcScreen.SelectObject( hOldFont );
					CRect rcMargins = OnQueryItemMargins( pII );
					rcText.InflateRect( 
						0,
						0,
						rcMargins.left + rcMargins.right,
						rcMargins.top + rcMargins.bottom
						);
				} // if( !sText.IsEmpty() )
			
				pII->m_rcRect.CopyRect( &rcDurationInnerArea );
				pII->m_rcRect.left = pII->m_rcRect.right - rcText.Width();
				pII->m_rcRect.DeflateRect( 
					0, 
					(pII->m_rcRect.Height() - rcText.Height()) / 2 
					);
				rcDurationInnerArea.DeflateRect( 0, 0, pII->m_rcRect.Width(), 0 );
	
					nMostLeft = min( nMostLeft, pII->m_rcRect.left );
					szContent.cx += pII->m_rcRect.Width();
					szContent.cy = max( pII->m_rcRect.Height(), szContent.cy );
			}
		} // for( nItem = m_arrItems.GetSize() - 1; nItem >= 0; nItem-- )

		eAlign_t eAlign = GetAlign();
		if( eAlign != right )
		{
			INT nDiff = nMostLeft - rcDurationInnerArea.left;
			if( szBorder.cx > 0  )
				nDiff -= 2;
			if( eAlign == center )
				nDiff = ::MulDiv( nDiff, 1, 2 );
			for( nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
			{
				ITEM_INFO * pII = m_arrItems[ nItem ];
				ASSERT( pII != NULL );
				if(		pII != NULL 
					&&	pII->m_bVisible
					)
					pII->m_rcRect.OffsetRect( -nDiff, 0 );
			}	
		}
	
	} // if( eStatus == CExtDurationWnd::valid )

	m_bUpdatingLayout = false;	
}

void CExtDurationWnd::OnSetFocus(CWnd* pOldWnd) 
{
	ASSERT_VALID( this );
	CWnd::OnSetFocus(pOldWnd);
	UpdateDurationWnd( false, true );
}

void CExtDurationWnd::OnKillFocus(CWnd* pNewWnd) 
{
	ASSERT_VALID( this );
	if( m_pLastInputItem != NULL )
		OnItemFinishInput();
	CWnd::OnKillFocus(pNewWnd);
	UpdateDurationWnd( false, true );
}

void CExtDurationWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	ASSERT_VALID( this );
	CWnd::OnLButtonDown(nFlags, point);
	ITEM_INFO * pII = HitTestItem( point );
	if( pII != NULL )
		OnSelectItem( pII );
	SetFocus();
}

void CExtDurationWnd::OnSelectItem( ITEM_INFO * pII )
{
	ASSERT_VALID( this );
	if(		m_pLastInputItem != NULL 
		&&	m_pLastInputItem != pII
		)
		OnItemFinishInput();
	if( pII == NULL )
		m_pSelectedItem = NULL;
	else if( pII->m_eItemType != CExtDurationWnd::label )
		m_pSelectedItem = pII;
	else
	{
		for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
		{
			ITEM_INFO * pII2 = m_arrItems[ nItem ];
			ASSERT( pII2 != NULL );
			if(		pII2 != NULL 
				&&	pII2->m_bVisible
				&&	pII2->m_eItemType != CExtDurationWnd::label
				)
			{
				for( LONG nItem2 = 0; nItem2 < pII2->m_arrDependentItems.GetSize(); nItem2++ )
				{
					ITEM_INFO * pDependentII = pII2->m_arrDependentItems[ nItem2 ];
					ASSERT( pDependentII != NULL );
					if( pDependentII == pII )
					{
						m_pSelectedItem = pII2;
						break;
					}
				}
			}
		}
	}
	m_pLastInputItem = NULL;
	UpdateDurationWnd( false, true );
	OnItemSelectionChanged();
}

void CExtDurationWnd::OnItemFinishInput()
{
	ASSERT_VALID( this );
}

void CExtDurationWnd::OnAutoSelectNextItem( const ITEM_INFO * pII )
{
	ASSERT_VALID( this );
	SelectNextItem( pII );
}

void CExtDurationWnd::OnItemSelectionChanged()
{
	ASSERT_VALID( this );
}

bool CExtDurationWnd::SelectItem( eItem_t eItem )
{
	ASSERT_VALID( this );
	ASSERT( eItem != CExtDurationWnd::label );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
	{
		ITEM_INFO * pII = m_arrItems[ nItem ];
		ASSERT( pII != NULL );
		if(		pII != NULL 
			&&	pII->m_bVisible
			&&	pII->m_eItemType != CExtDurationWnd::label
			&&	pII->m_eItemType == eItem
			)
		{
			OnSelectItem( pII );
			return true;
		}
	}
	return false;
}

CExtDurationWnd::ITEM_INFO * CExtDurationWnd::SelectionGet() const
{
	ASSERT_VALID( this );
	return m_pSelectedItem;
}

bool CExtDurationWnd::SelectNextItem( const ITEM_INFO * pII )
{
	ASSERT_VALID( this );
LONG nStartIndex = _ItemGetIndexOf( pII );
LONG nItem = nStartIndex;
// 	if( nStartIndex < 0 )
// 		nStartIndex = 0;
	do 
	{
		nItem += 1;
		if( nItem >= m_arrItems.GetSize() )
			nItem = 0;
		ITEM_INFO * pII2 = m_arrItems[ nItem ];
		ASSERT( pII2 != NULL );
		if(		pII2 != NULL 
			&&	pII2->m_bVisible
			&&	pII2->m_eItemType != CExtDurationWnd::label
			)
		{
			OnSelectItem( pII2 );
			return true;
		}
	} while( nItem != nStartIndex );
	return false;
}

bool CExtDurationWnd::SelectPrevItem( const ITEM_INFO * pII )
{
	ASSERT_VALID( this );
LONG nStartIndex = _ItemGetIndexOf( pII );
	if( nStartIndex < 0 )
		nStartIndex = (LONG)m_arrItems.GetSize();
LONG nItem = nStartIndex;
	do 
	{
		nItem -= 1;
		if( nItem < 0 )
			nItem = LONG(m_arrItems.GetSize()) - 1;
		ITEM_INFO * pII2 = m_arrItems[ nItem ];
		ASSERT( pII2 != NULL );
		if(		pII2 != NULL 
			&&	pII2->m_bVisible
			&&	pII2->m_eItemType != CExtDurationWnd::label
			)
		{
			OnSelectItem( pII2 );
			return true;
		}
	} while( nItem != nStartIndex );
	return false;
}

bool CExtDurationWnd::SelectFirsItem()
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
	{
		ITEM_INFO * pII = m_arrItems[ nItem ];
		ASSERT( pII != NULL );
		if(		pII != NULL 
			&&	pII->m_bVisible
			&&	pII->m_eItemType != CExtDurationWnd::label
			)
		{
			OnSelectItem( pII );
			return true;
		}
	}
	return false;
}

bool CExtDurationWnd::SelectLastItem()
{
	ASSERT_VALID( this );
	for( LONG nItem = LONG(m_arrItems.GetSize()) - 1; nItem >= 0; nItem-- )
	{
		ITEM_INFO * pII = m_arrItems[ nItem ];
		ASSERT( pII != NULL );
		if(		pII != NULL 
			&&	pII->m_bVisible
			&&	pII->m_eItemType != CExtDurationWnd::label
			)
		{
			OnSelectItem( pII );
			return true;
		}
	}
	return false;
}

bool CExtDurationWnd::IsFirstVisibleItem( const ITEM_INFO * pII ) const
{
	ASSERT_VALID( this );
	ASSERT( pII != NULL );
	if( pII == NULL )
		return false;
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
	{
		ITEM_INFO * pII2 = m_arrItems[ nItem ];
		ASSERT( pII2 != NULL );
		if(		pII2 != NULL 
			&&	pII2->m_bVisible
			&&	pII2->m_eItemType != CExtDurationWnd::label
			)
		{
			return (pII == pII2) ? true : false;
		}
	}
	return false;
}

bool CExtDurationWnd::IsLastVisibleItem( const ITEM_INFO * pII ) const
{
	ASSERT_VALID( this );
	ASSERT( pII != NULL );
	if( pII == NULL )
		return false;
	for( LONG nItem = LONG(m_arrItems.GetSize()) - 1; nItem >= 0; nItem-- )
	{
		ITEM_INFO * pII2 = m_arrItems[ nItem ];
		ASSERT( pII2 != NULL );
		if(		pII2 != NULL 
			&&	pII2->m_bVisible
			&&	pII2->m_eItemType != CExtDurationWnd::label
			)
		{
			return (pII == pII2) ? true : false;
		}
	}
	return false;
}

CSize CExtDurationWnd::OnCalcContentSize() const
{
	ASSERT_VALID( this );
	CSize szContent = CSize(0,0);
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
	{
		ITEM_INFO * pII = m_arrItems[ nItem ];
		ASSERT( pII != NULL );
		if(		pII != NULL 
			&&	pII->m_bVisible
			)
		{
			szContent.cx += pII->m_rcRect.Width();
			szContent.cy = max( pII->m_rcRect.Height(), szContent.cy );
		}
	}
	if( IsSpinButtonVisible() )
	{
		INT nSpinButtonWidth = OnQuerySpinButtonWidth();
		szContent.cx += nSpinButtonWidth;
	}
	if( IsDropDownButtonVisible() )
	{
		INT nDropDownButtonWidth = OnQueryDropDownButtonWidth();
		szContent.cx += nDropDownButtonWidth;
	}
	CSize szBorder = OnQueryBorderSize();
	szContent += szBorder;
	szContent.cx += 4;
	szContent.cy = max( 20, szContent.cy );
	return szContent;
}

void CExtDurationWnd::AdjustSize()
{
	ASSERT_VALID( this );
	if(		GetSafeHwnd() == NULL
		||	( ! ::IsWindow( GetSafeHwnd() ) )
		)
		return;
CSize szContent = OnCalcContentSize();
CRect rcWnd;
	GetWindowRect( &rcWnd );
	if( rcWnd.Size() == szContent )
		return;
	rcWnd.right = rcWnd.left + szContent.cx;
	rcWnd.bottom = rcWnd.top + szContent.cy;
	if( (GetStyle() & WS_CHILD) )
		GetParent()->ScreenToClient( &rcWnd );
	MoveWindow( &rcWnd, FALSE );
	UpdateDurationWnd( false, true );
}

bool CExtDurationWnd::OnValueChanging( 
	const COleDateTimeSpan & dtSpanOld,
	const COleDateTimeSpan & dtSpanNew
	)
{
	ASSERT_VALID( this );
	HWND hWndNotificationReceiver = OnQueryNotificationReceiver();
	if( hWndNotificationReceiver != NULL )
	{
		CHANGING_NOTIFICATION _SN( 
			false,
			dtSpanOld, 
			dtSpanNew,
			m_lParamCookie
			);
		LRESULT lResult = _SN.Notify( hWndNotificationReceiver );
		if( lResult < 0 )
			return false;
	} // if( hWndNotificationReceiver != NULL )
	return true;
}

bool CExtDurationWnd::OnValueChanged( 
	const COleDateTimeSpan & dtSpanOld,
	const COleDateTimeSpan & dtSpanNew
	)
{
	ASSERT_VALID( this );
	HWND hWndNotificationReceiver = OnQueryNotificationReceiver();
	if( hWndNotificationReceiver != NULL )
	{
		CHANGING_NOTIFICATION _SN( 
			true,
			dtSpanOld, 
			dtSpanNew,
			m_lParamCookie
			);
		LRESULT lResult = _SN.Notify( hWndNotificationReceiver );
		if( lResult < 0 )
			return false;
	} // if( hWndNotificationReceiver != NULL )
	return true;
}

void CExtDurationWnd::SetContinuousScrolling( 
	bool bSet // = true
	)
{
	ASSERT_VALID( this );
	m_bContinuousScrolling = bSet;
}

bool CExtDurationWnd::GetContinuousScrolling() const
{
	ASSERT_VALID( this );
	return m_bContinuousScrolling;
}

void CExtDurationWnd::ScrollCurrentItem( INT nDelta )
{
	ASSERT_VALID( this );
	ITEM_INFO * pII = SelectionGet();
	if( pII != NULL )
	{
		double dTotalSeconds = m_dtSpan.GetTotalSeconds();
		switch( pII->m_eItemType ) 
		{
		case CExtDurationWnd::day:
			if( m_bContinuousScrolling )
				dTotalSeconds += ( nDelta * 60 * 60 * 24 );
			else
			{
				double dValue = pII->m_nValue;
				dValue += nDelta;
				if( dValue < 0 )
					dValue = 0;
				dTotalSeconds -= ( pII->m_nValue * 60 * 60 * 24 );
				dTotalSeconds += ( dValue * 60 * 60 * 24 );
			}
			break;
		case CExtDurationWnd::hour:
			if( m_bContinuousScrolling )
				dTotalSeconds += ( nDelta * 60 * 60 );
			else
			{
				double dValue = pII->m_nValue;
				dValue += nDelta;
				if( dValue > 23 && !IsFirstVisibleItem( pII ) )
					dValue = 0;
				if( dValue < 0 )
					dValue = 23;
				dTotalSeconds -= ( pII->m_nValue * 60 * 60 );
				dTotalSeconds += ( dValue * 60 * 60 );
			}
			break;
		case CExtDurationWnd::minute:
			if( m_bContinuousScrolling )
				dTotalSeconds += ( nDelta * 60 );
			else
			{
				double dValue = pII->m_nValue;
				dValue += nDelta;
				if( dValue > 59 && !IsFirstVisibleItem( pII ) )
					dValue = 0;
				if( dValue < 0 )
					dValue = 59;
				dTotalSeconds -= ( pII->m_nValue * 60 );
				dTotalSeconds += ( dValue * 60 );
			}
			break;
		case CExtDurationWnd::second:
			if( m_bContinuousScrolling )
				dTotalSeconds += nDelta;
			else
			{
				double dValue = pII->m_nValue;
				dValue += nDelta;
				if( dValue > 59 && !IsFirstVisibleItem( pII ) )
					dValue = 0;
				if( dValue < 0 )
					dValue = 59;
				dTotalSeconds -= ( pII->m_nValue );
				dTotalSeconds += ( dValue );
			}
			break;
		default:
			ASSERT( FALSE );
		}
		if( dTotalSeconds < LONG_MAX )
			SetDuration( (LONG) dTotalSeconds );
	} // if( pII != NULL )
}

CExtSafeString CExtDurationWnd::OnQueryItemText( const ITEM_INFO * pII ) const
{
	ASSERT_VALID( this );
	ASSERT( pII != NULL );
	if( pII == NULL )
		return _T("");

	CExtSafeString sText;
	switch( pII->m_eItemType ) 
	{
	case CExtDurationWnd::label:
		sText.Format( _T("%s"), pII->m_sText );
		break;
	case CExtDurationWnd::day:
		sText.Format( _T("%d"), pII->m_nValue );
		break;
	case CExtDurationWnd::hour:
	case CExtDurationWnd::minute:
	case CExtDurationWnd::second:
		sText.Format( _T("%02d"), pII->m_nValue );
		break;
	}	
	return sText;
}

BOOL CExtDurationWnd::OnMouseWheel(UINT fFlags, short zDelta, CPoint point)
{
	ASSERT_VALID( this );
	fFlags;
	point;
	if( IsReadOnly() )
		return false;
	if( zDelta > 0 )
		ScrollCurrentItem( +1 );
	else if( zDelta < 0 )
		ScrollCurrentItem( -1 );
	else
		return false;
	return true;
}

void CExtDurationWnd::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	ASSERT_VALID( this );
	switch( nChar )
	{
	case VK_TAB:
		{
			HWND hWndParent = GetParent()->GetSafeHwnd();
			if( hWndParent == NULL )
				return;
			bool bShift = ( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 ) ? true : false;
			::SendMessage(
				hWndParent,
				WM_NEXTDLGCTL,
				bShift ? (!0) : 0,
				0
				);
		}
		break;

	case VK_F4:
		if( ! IsReadOnly() )
			OnShowDropDownMenu();
		break;

	case VK_RIGHT:
	case 110: // . (num part of keyboard)
	case 111: // / (num part of keyboard)
	case 188: // .
	case 189: // -
	case 190: // ,
	case 191: // /
		SelectNextItem( m_pSelectedItem );
		break;

	case VK_LEFT:
		SelectPrevItem( m_pSelectedItem );
		break;

	case VK_HOME:
		SelectFirsItem();
		break;

	case VK_END:
		SelectLastItem();
		break;

	case VK_DOWN:
		if(		(!IsReadOnly())
			&&	GetStatus() == CExtDurationWnd::valid 
			)
			ScrollCurrentItem( -1 );
		break;

	case VK_UP:
		if(		(!IsReadOnly())
			&&	GetStatus() == CExtDurationWnd::valid 
			)
			ScrollCurrentItem( +1 );
		break;

	case 67: // C
	case 86: // V
	case VK_INSERT:
		{
			ITEM_INFO * pII = SelectionGet();
			if( pII != NULL )
			{
				bool bCtrl = ( (::GetAsyncKeyState(VK_CONTROL)&0x8000) != 0 ) ? true : false;
				bool bShift = ( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 ) ? true : false;
				if(		( nChar == 67 && bCtrl ) 
					||	( nChar == VK_INSERT && bCtrl )  
					)
				{
					CExtSafeString sText = OnQueryItemText( pII );
					// copy to clipboard
					if( OpenClipboard() )
					{
						if( EmptyClipboard() )
						{
							HGLOBAL hGlobal =
								::GlobalAlloc( 
									GMEM_DDESHARE, 
									sText.GetLength() + 1 
									);
							ASSERT( hGlobal != NULL );
							if( hGlobal != NULL )
							{
								LPTSTR lpszBuffer = 
									(LPTSTR) ::GlobalLock( hGlobal );
								__EXT_MFC_STRCPY( 
									lpszBuffer, 
									sText.GetLength() + 1,
									LPCTSTR(sText)
									);
								::GlobalUnlock( hGlobal );
								::SetClipboardData( 
									CF_TEXT, 
									hGlobal 
									);
							} // if( hGlobal != NULL )
						} // if( EmptyClipboard() )
						::CloseClipboard();
					} // if( OpenClipboard() )
				}
				else 
					if( ! IsReadOnly() )
					{
						if(		( nChar == 86 && bCtrl ) 
							||	( nChar == VK_INSERT && bShift ) 
							)
						{
							// paste from clipboard
							if( OpenClipboard() )
							{
								LPTSTR lpszBuffer = 
									(LPTSTR) ::GetClipboardData( CF_TEXT );
								if( lpszBuffer != NULL )
								{
									double dValue = 
										(double)_ttol( lpszBuffer );
									double dTotalSeconds = 
										m_dtSpan.GetTotalSeconds();
									switch( pII->m_eItemType ) 
									{
									case CExtDurationWnd::day:
										if(	dValue > 0 )
										{
											dTotalSeconds -= ( pII->m_nValue * 60 * 60 * 24 );
											dTotalSeconds += ( dValue * 60 * 60 * 24 );
										}
										break;
									case CExtDurationWnd::hour:
										if(		dValue > 0 
											&&	( dValue <= 23 || IsFirstVisibleItem( pII ) )
											)
										{
											dTotalSeconds -= ( pII->m_nValue * 60 * 60 );
											dTotalSeconds += ( dValue * 60 * 60 );
										}
										break;
									case CExtDurationWnd::minute:
										if(		dValue > 0 
											&&	( dValue <= 59 || IsFirstVisibleItem( pII ) )
											)
										{
											dTotalSeconds -= ( pII->m_nValue * 60 );
											dTotalSeconds += ( dValue * 60 );
										}
										break;
									case CExtDurationWnd::second:
										if(		dValue > 0 
											&&	( dValue <= 59 || IsFirstVisibleItem( pII ) )
											)
										{
											dTotalSeconds -= ( pII->m_nValue );
											dTotalSeconds += ( dValue );
										}
										break;
									default:
										ASSERT( FALSE );
									}
									if( dTotalSeconds < LONG_MAX )
										SetDuration( (LONG)dTotalSeconds );
								}
								::CloseClipboard();
							} // if( OpenClipboard() )
						}
					} // if( ! IsReadOnly() )
			} // if( pII != NULL )
		}
		break;

	default:
		if( ! IsReadOnly() )
		{
			if(		nChar >= VK_NUMPAD0 
				&&	nChar <= VK_NUMPAD9
				)
				OnDigitPressed( nChar - VK_NUMPAD0 );
			else 
				if( isdigit( nChar ) )
					OnDigitPressed( nChar - _T('0') );
				else
					CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		}
		else
			CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		return;
	} // switch( nChar )
}

void CExtDurationWnd::OnDigitPressed(
	UINT nDigit
	)
{
	ASSERT_VALID( this );
	if( IsReadOnly() )
		return;
	ITEM_INFO * pII = SelectionGet();
	if( pII != NULL )
	{
		double dValue = 0;
		if( pII == m_pLastInputItem )
			dValue = pII->m_nValue * 10 + nDigit;
		else
			dValue = nDigit;

		double dTotalSeconds = m_dtSpan.GetTotalSeconds();
		switch( pII->m_eItemType ) 
		{
		case CExtDurationWnd::day:
			if(	dValue < 0 )
				dValue = nDigit;
			dTotalSeconds -= ( pII->m_nValue * 60 * 60 * 24 );
			dTotalSeconds += ( dValue * 60 * 60 * 24 );
			break;
		case CExtDurationWnd::hour:
			if(		dValue < 0 
				||	( dValue > 23 && !IsFirstVisibleItem( pII ) )
				)
				dValue = nDigit;
			dTotalSeconds -= ( pII->m_nValue * 60 * 60 );
			dTotalSeconds += ( dValue * 60 * 60 );
			break;
		case CExtDurationWnd::minute:
			if(		dValue < 0 
				||	( dValue > 59 && !IsFirstVisibleItem( pII ) )
				)
				dValue = nDigit;
			dTotalSeconds -= ( pII->m_nValue * 60 );
			dTotalSeconds += ( dValue * 60 );
			break;
		case CExtDurationWnd::second:
			if(		dValue < 0 
				||	( dValue > 59 && !IsFirstVisibleItem( pII ) )
				)
				dValue = nDigit;
			dTotalSeconds -= ( pII->m_nValue );
			dTotalSeconds += ( dValue );
			break;
		default:
			ASSERT( FALSE );
		}
		m_pLastInputItem = pII;
		if( dTotalSeconds < LONG_MAX )
			SetDuration( (LONG) dTotalSeconds );
	}
}

CExtDurationWnd::ITEM_INFO * CExtDurationWnd::HitTestItem( 
	const POINT & ptClient 
	) const
{
	ASSERT_VALID( this );
	for( LONG nItem = 0; nItem < m_arrItems.GetSize(); nItem++ )
	{
		ITEM_INFO * pII = m_arrItems[ nItem ];
		ASSERT( pII != NULL );
		if(		pII != NULL 
			&&	pII->m_rcRect.PtInRect( ptClient )
			&&	pII->m_bVisible
			)
			return pII;
	}
	return NULL;
}

void CExtDurationWnd::OnDropDownButtonClicked()
{
	ASSERT_VALID( this );
}

CExtSafeString CExtDurationWnd::OnQueryTimeSeparator() const
{
	ASSERT_VALID( this );
	// Characters used for the time separator. 
	LPTSTR lpszBuf = (LPTSTR)::LocalAlloc(LPTR, 10);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_STIME,
			lpszBuf,
			10
			) != 0
		);
	CExtSafeString s = (LPTSTR)lpszBuf;
	::LocalFree( lpszBuf );
	return s;
}

CExtSafeString CExtDurationWnd::OnQueryDateSeparator() const
{
	ASSERT_VALID( this );
	// Characters used for the date separator.
	LPTSTR lpszBuf = (LPTSTR)::LocalAlloc(LPTR, 10);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_SDATE,
			lpszBuf,
			10
			) != 0
		);
	CExtSafeString s = (LPTSTR)lpszBuf;
	::LocalFree( lpszBuf );
	return s;
}

CExtSafeString CExtDurationWnd::OnQueryHourLeadingZeros() const
{
	ASSERT_VALID( this );
	// Whether to use leading zeros in time fields. 
    // 0 - No leading zeros.
    // 1 - Leading zeros for hours.
	LPTSTR lpszBuf = (LPTSTR)::LocalAlloc(LPTR, 10);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_ITLZERO,
			lpszBuf,
			10
			) != 0
		);
	CExtSafeString s = (LPTSTR)lpszBuf;
	::LocalFree( lpszBuf );
	return s;
}

CExtSafeString CExtDurationWnd::OnQueryDayLeadingZeros() const
{
	ASSERT_VALID( this );
	// Whether to use leading zeros in day fields. 
	LPTSTR lpszBuf = (LPTSTR)::LocalAlloc(LPTR, 10);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_IDAYLZERO,
			lpszBuf,
			10
			) != 0
		);
	CExtSafeString s = (LPTSTR)lpszBuf;
	::LocalFree( lpszBuf );
	return s;
}

CExtSafeString CExtDurationWnd::OnQueryMonthLeadingZeros() const
{
	ASSERT_VALID( this );
	// Whether to use leading zeros in month fields. 
	LPTSTR lpszBuf = (LPTSTR)::LocalAlloc(LPTR, 10);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_IMONLZERO,
			lpszBuf,
			10
			) != 0
		);
	CExtSafeString s = (LPTSTR)lpszBuf;
	::LocalFree( lpszBuf );
	return s;
}

CExtSafeString CExtDurationWnd::OnQueryYearFormat() const
{
	ASSERT_VALID( this );
	// Whether to use full 4-digit century. 
    // 0 - Two digit.
    // 1 - Full century.
	LPTSTR lpszBuf = (LPTSTR)::LocalAlloc(LPTR, 10);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_ICENTURY,
			lpszBuf,
			10
			) != 0
		);
	CExtSafeString s = (LPTSTR)lpszBuf;
	::LocalFree( lpszBuf );
	return s;
}

CExtSafeString CExtDurationWnd::OnQueryTimeDesignatorAM() const
{
	ASSERT_VALID( this );
	// String for the AM designator. 
	LPTSTR lpszBuf = (LPTSTR)::LocalAlloc(LPTR, 10);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_S1159,
			lpszBuf,
			10
			) != 0
		);
	CExtSafeString s = (LPTSTR)lpszBuf;
	::LocalFree( lpszBuf );
	return s;
}

CExtSafeString CExtDurationWnd::OnQueryTimeDesignatorPM() const
{
	ASSERT_VALID( this );
	// String for the PM designator. 
	LPTSTR lpszBuf = (LPTSTR)::LocalAlloc(LPTR, 10);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_S2359,
			lpszBuf,
			10
			) != 0
		);
	CExtSafeString s = (LPTSTR)lpszBuf;
	::LocalFree( lpszBuf );
	return s;
}

CExtSafeString CExtDurationWnd::OnQueryTimeDesignatorPosition() const
{
	ASSERT_VALID( this );
	// Whether the time marker string (AM|PM) precedes or follows the time string. 
	// 0 - Suffix (9:15 AM). 
	// 1 - Prefix (AM 9:15).
	LPTSTR lpszBuf = (LPTSTR)::LocalAlloc(LPTR, 10);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_ITIMEMARKPOSN,
			lpszBuf,
			10
			) != 0
		);
	CExtSafeString s = (LPTSTR)lpszBuf;
	::LocalFree( lpszBuf );
	return s;
}

LRESULT CExtDurationWnd::CExtDropDownButton::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
#ifndef __EXT_MFC_NO_SPIN
	if( message == WM_NCCALCSIZE )
	{
		NCCALCSIZE_PARAMS * pNCCSP =
			reinterpret_cast < NCCALCSIZE_PARAMS * > ( lParam );
		ASSERT( pNCCSP != NULL );
		CRect rcInBarWnd( pNCCSP->rgrc[0] );
		rcInBarWnd.DeflateRect( 1, 1, 1, 1 );
		::CopyRect( &(pNCCSP->rgrc[0]), rcInBarWnd );
		return 0;
	}
	if( message == WM_NCPAINT )
	{
		CRect rcWnd, rcClient;
		GetWindowRect( &rcWnd );
		GetClientRect( &rcClient );
		ClientToScreen( &rcClient );
		if( rcWnd == rcClient )
			return 0;
		CPoint ptDevOffset = -rcWnd.TopLeft();
		rcWnd.OffsetRect( ptDevOffset );
		rcClient.OffsetRect( ptDevOffset );
		
		CWindowDC dc( this );
		ASSERT( dc.GetSafeHdc() != NULL );
		dc.ExcludeClipRect( &rcClient );
		CExtDurationWnd * pWndParent = (CExtDurationWnd *)GetParent();
		ASSERT_VALID( pWndParent );
		COLORREF clrBk = pWndParent->GetBkColor();
		dc.FillSolidRect( 
			&rcWnd, 
			(clrBk != COLORREF( -1L ))
				? clrBk 
				: ( pWndParent->IsWindowEnabled() 
						? PmBridge_GetPM()->GetColor( COLOR_WINDOW, this ) 
						: ::GetSysColor( COLOR_3DFACE ) 
						)
			);
		return 0;
	}
#endif // __EXT_MFC_NO_SPIN
	
	return CExtButton::WindowProc( message, wParam, lParam );
}

void CExtDurationWnd::CExtDropDownButton::_RenderImpl(
	CDC & dc,
	bool bTransparent, // = false
	bool bCombinedContent // = false
	)
{
	ASSERT_VALID( this );
	bTransparent;
	bCombinedContent;
CRect rcClient;
	GetClientRect( &rcClient );
bool bPushed = m_bPushed ? true : false;
bool bEnabled = OnQueryWindowEnabledState();
bool bHover =
		( m_bMouseOver && !CExtPopupMenuWnd::IsMenuTracking() )
			? true 
			: false;

#ifndef __EXT_MFC_NO_SPIN

CExtPaintManager * pPM = PmBridge_GetPM();
CExtDurationWnd * pWndParent = (CExtDurationWnd *)GetParent();
	ASSERT_VALID( pWndParent );
COLORREF clrBk = pWndParent->GetBkColor();
	dc.FillSolidRect( 
		&rcClient, 
		(clrBk != COLORREF( -1L ))
			? clrBk 
			: ( pWndParent->IsWindowEnabled() 
					? PmBridge_GetPM()->GetColor( COLOR_WINDOW, this ) 
					: ::GetSysColor( COLOR_3DFACE ) 
					)
		);
	pPM->PaintSpinButton(
		dc,
		rcClient,
		false,
		false,
		bEnabled,
		false,
		false,
		bPushed,
		bHover,
		false,
		this
		);

#else

	bool bDefaultDrawing = true;
	if(		g_PaintManager.m_UxTheme.IsControlsThemed()
		&&	g_PaintManager.m_UxTheme.OpenThemeData( m_hWnd, L"COMBOBOX" ) != NULL
		)
	{
			INT nState = 
				(!bEnabled) 
					? CBXS_DISABLED 
					: bPushed 
						? CBXS_PRESSED 
						: bHover 
							? CBXS_HOT 
							: CBXS_NORMAL;

		if( g_PaintManager.m_UxTheme.DrawThemeBackground(
					dc.GetSafeHdc(), 
					CP_DROPDOWNBUTTON, 
					nState, 
					&rcClient, 
					0
				) == S_OK
			)
			bDefaultDrawing = false;
		g_PaintManager.m_UxTheme.CloseThemeData();
	}
	if( bDefaultDrawing )
	{
		INT nState = 
			(!bEnabled) 
				? DFCS_INACTIVE 
				: bPushed 
					? (DFCS_PUSHED|DFCS_FLAT)
					: 0;
		dc.DrawFrameControl(
			&rcClient, 
			DFC_SCROLL,
			DFCS_SCROLLDOWN|nState
			);
	} // if( bDefaultDrawing )

#endif // __EXT_MFC_NO_SPIN

}

#ifndef __EXT_MFC_NO_SPIN

LRESULT CExtDurationWnd::CExtSpinButton::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	if( message == WM_NCCALCSIZE )
	{
		NCCALCSIZE_PARAMS * pNCCSP =
			reinterpret_cast < NCCALCSIZE_PARAMS * > ( lParam );
		ASSERT( pNCCSP != NULL );
		CRect rcInBarWnd( pNCCSP->rgrc[0] );
		rcInBarWnd.DeflateRect( 1, 1, 1, 1 );
		::CopyRect( &(pNCCSP->rgrc[0]), rcInBarWnd );
		return 0;
	}
	if( message == WM_NCPAINT )
	{
		CRect rcWnd, rcClient;
		GetWindowRect( &rcWnd );
		GetClientRect( &rcClient );
		ClientToScreen( &rcClient );
		if( rcWnd == rcClient )
			return 0;
		CPoint ptDevOffset = -rcWnd.TopLeft();
		rcWnd.OffsetRect( ptDevOffset );
		rcClient.OffsetRect( ptDevOffset );
		
		CWindowDC dc( this );
		ASSERT( dc.GetSafeHdc() != NULL );
		dc.ExcludeClipRect( &rcClient );
		CExtDurationWnd * pWndParent = (CExtDurationWnd *)GetParent();
		ASSERT_VALID( pWndParent );
		COLORREF clrBk = pWndParent->GetBkColor();
		dc.FillSolidRect( 
			&rcWnd, 
			(clrBk != COLORREF( -1L ))
				? clrBk 
				: ( pWndParent->IsWindowEnabled() 
						? PmBridge_GetPM()->GetColor( COLOR_WINDOW, this ) 
						: ::GetSysColor( COLOR_3DFACE ) 
						)
			);
		return 0;
	}
	return CExtSpinWnd::WindowProc( message, wParam, lParam );
}

COLORREF CExtDurationWnd::CExtSpinButton::OnQueryBkColor() const
{
	ASSERT_VALID( this );
CExtDurationWnd * pWndParent = (CExtDurationWnd *)GetParent();
	ASSERT_VALID( pWndParent );
COLORREF clrBk = pWndParent->GetBkColor();
	return 	
		(clrBk != COLORREF( -1L ))
			? clrBk 
			: ( pWndParent->IsWindowEnabled() 
					? PmBridge_GetPM()->GetColor( COLOR_WINDOW, pWndParent ) 
					: ::GetSysColor( COLOR_3DFACE ) 
					);
}

#endif // __EXT_MFC_NO_SPIN

void CExtDurationWnd::SetStatus( eStatus_t eStatus )
{
	ASSERT_VALID( this );
	m_eStatus = eStatus;
	UpdateDurationWnd( true, true );
}

CExtDurationWnd::eStatus_t CExtDurationWnd::GetStatus() const
{
	ASSERT_VALID( this );
	return m_eStatus;
}

void CExtDurationWnd::SetBkColor( 
	COLORREF clrBk // = COLORREF(-1L) 
	)
{ 
	ASSERT_VALID( this );
	m_clrBackground = clrBk; 
	UpdateDurationWnd( false, true );
}

COLORREF CExtDurationWnd::GetBkColor() const
{ 
	ASSERT_VALID( this );
	return m_clrBackground; 
}

void CExtDurationWnd::SetTextColor( 
	COLORREF clrText // = COLORREF(-1L) 
	)
{
	ASSERT_VALID( this );
	m_clrText = clrText; 
	UpdateDurationWnd( false, true );
}

COLORREF CExtDurationWnd::GetTextColor() const
{
	ASSERT_VALID( this );
	return m_clrText; 
}

void CExtDurationWnd::SetAlign( eAlign_t eAlign )
{
	ASSERT_VALID( this );
	m_eAlign = eAlign;
	UpdateDurationWnd( true, true );
}

CExtDurationWnd::eAlign_t CExtDurationWnd::GetAlign() const
{
	ASSERT_VALID( this );
	return m_eAlign;
}

HWND CExtDurationWnd::OnQueryNotificationReceiver() const
{
	ASSERT_VALID( this );
	if(		m_hWndNotificationReceiver != NULL
		&&	::IsWindow( m_hWndNotificationReceiver )
		)
		return m_hWndNotificationReceiver;
	if( GetSafeHwnd() == NULL )
		return NULL;
HWND hWndNotificationReceiver = ::GetParent( m_hWnd );
	return hWndNotificationReceiver;
}

void CExtDurationWnd::OnEnable( BOOL bEnable )
{
	ASSERT_VALID( this );
BOOL bReadOnly = IsReadOnly();
	m_wndDropDown.EnableWindow( bEnable && (!bReadOnly) );
	m_wndSpin.EnableWindow( bEnable && (!bReadOnly) );
	UpdateDurationWnd( false, true );
}

LRESULT CExtDurationWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{ 
	if( message == WM_PRINT || message == WM_PRINTCLIENT )
	{
		CDC * pDC = CDC::FromHandle( (HDC)wParam );
		CRect rcClient;
		GetClientRect( &rcClient );
		OnDurationDrawEntire( *pDC, rcClient );
		return (!0);
	}	
	return CWnd::WindowProc(message, wParam, lParam);
}

#if (!defined __EXT_MFC_NO_DATETIMEWND)

//////////////////////////////////////////////////////////////////////////
// CExtDateTimeWnd::CHANGING_NOTIFICATION

CExtDateTimeWnd::CHANGING_NOTIFICATION::CHANGING_NOTIFICATION(
	bool bChangedFinally,
	COleDateTime dtDateTimeOld,
	COleDateTime dtDateTimeNew,
	LPARAM lParamCookie
	)
	: m_bChangedFinally( bChangedFinally )
	, m_lParamCookie( lParamCookie )
	, m_dtDateTimeOld( dtDateTimeOld )
	, m_dtDateTimeNew( dtDateTimeNew )
{
}

CExtDateTimeWnd::CHANGING_NOTIFICATION::operator WPARAM() const
{
	WPARAM wParam = reinterpret_cast < WPARAM > ( this );
	return wParam;
}

const CExtDateTimeWnd::CHANGING_NOTIFICATION *
	CExtDateTimeWnd::CHANGING_NOTIFICATION::FromWPARAM( WPARAM wParam )
{
	CExtDateTimeWnd::CHANGING_NOTIFICATION * pSN =
		reinterpret_cast < CExtDateTimeWnd::CHANGING_NOTIFICATION * > ( wParam );
	ASSERT( pSN != NULL );
	return pSN;
}

LRESULT CExtDateTimeWnd::CHANGING_NOTIFICATION::Notify( HWND hWndNotify ) const
{
	ASSERT( hWndNotify != NULL && ::IsWindow( hWndNotify ) );
	return 
		::SendMessage(
			hWndNotify,
			CExtDateTimeWnd::g_nMsgChangingNotification,
			*this,
			m_lParamCookie
			);
}

/////////////////////////////////////////////////////////////////////////////
// CExtDateTimeWnd window

IMPLEMENT_DYNCREATE( CExtDateTimeWnd, CExtDurationWnd );

CExtDateTimeWnd::CExtDateTimeWnd()
	: CExtDurationWnd()
	, m_bBlankYear( false )
	, m_bBlankMonth( false )
	, m_bBlankDay( false )
	, m_bBlankHour( false )
	, m_bBlankMinute( false )
	, m_bBlankSecond( false )
	, m_bBlankDesignator( false )
	, m_bAutoSelectNext( false )
{
	m_eMode = CExtDateTimeWnd::all;
	m_eTimeFormat = CExtDateTimeWnd::automatic;

	m_dtDate = COleDateTime::GetCurrentTime();

	m_dtRangeMin.SetStatus( COleDateTime::null );
	m_dtRangeMax.SetStatus( COleDateTime::null );

	SetStatus( CExtDurationWnd::null );
}

CExtDateTimeWnd::~CExtDateTimeWnd()
{
}

BEGIN_MESSAGE_MAP(CExtDateTimeWnd, CExtDurationWnd)
	//{{AFX_MSG_MAP(CExtDateTimeWnd)
	ON_WM_KILLFOCUS()
	ON_WM_KEYDOWN()
	ON_BN_CLICKED(__EXT_DURATIONWND_DROPDOWN_BTN_ID, OnDropDownButtonClicked)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(
		CExtDatePickerWnd::g_nMsgSelectionNotification,
		OnDropDownCalendarSelChanged
	)
	ON_REGISTERED_MESSAGE(
		CExtPopupInplaceDatePicker::g_nMsgPopupDatePickerInitContent,
		OnDropDownCalendarInit
	)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtDateTimeWnd message handlers

const UINT CExtDateTimeWnd::g_nMsgChangingNotification =
	::RegisterWindowMessage(
		_T("CExtDateTimeWnd::g_nMsgChangingNotification")
		);

#ifdef _DEBUG
void CExtDateTimeWnd::AssertValid() const
{
	CExtDurationWnd::AssertValid();
}
void CExtDateTimeWnd::Dump(CDumpContext& dc) const
{
	CExtDurationWnd::Dump( dc );
}
#endif // _DEBUG

BOOL CExtDateTimeWnd::_UpdateDurationFromOleDateTime(
	bool bUpdate // = false
	)
{
	ASSERT_VALID( this );

	ITEM_INFO * pYearII = ItemGet( CExtDurationWnd::year );
	ASSERT( pYearII != NULL );
	ITEM_INFO * pMonthII = ItemGet( CExtDurationWnd::month );
	ASSERT( pMonthII != NULL );
	ITEM_INFO * pDayII = ItemGet( CExtDurationWnd::day );
	ASSERT( pDayII != NULL );

	ITEM_INFO * pHourII = ItemGet( CExtDurationWnd::hour );
	ASSERT( pHourII != NULL );
	ITEM_INFO * pMinuteII = ItemGet( CExtDurationWnd::minute );
	ASSERT( pMinuteII != NULL );
	ITEM_INFO * pSecondII = ItemGet( CExtDurationWnd::second );
	ASSERT( pSecondII != NULL );
	ITEM_INFO * pDesignatorII = ItemGet( CExtDurationWnd::designator );
	ASSERT( pDesignatorII != NULL );

	if(		pYearII == NULL
		||	pMonthII == NULL
		||	pDayII == NULL
		||	pHourII == NULL
		||	pMinuteII == NULL
		||	pSecondII == NULL
		||	pDesignatorII == NULL
		)
		return FALSE;

	pYearII->m_nValue = m_dtDate.GetYear();
	pMonthII->m_nValue = m_dtDate.GetMonth();
	pDayII->m_nValue = m_dtDate.GetDay();
	pHourII->m_nValue = m_dtDate.GetHour();
	pMinuteII->m_nValue = m_dtDate.GetMinute();
	pSecondII->m_nValue = m_dtDate.GetSecond();

	if(		!OnQueryTimeFormat24Hours()
		&&	( pHourII->m_nValue > 12 || pHourII->m_nValue == 0 )
		)
	{
		if( pHourII->m_nValue == 0 )
			pHourII->m_nValue = 12;
		else
			pHourII->m_nValue -= 12;
		ASSERT( pHourII->m_nValue > 0 && pHourII->m_nValue <= 12 );
	}

	m_eStatus = (CExtDurationWnd::eStatus_t)m_dtDate.GetStatus();

	UpdateDurationWnd( true, bUpdate );

	return TRUE;
}

void CExtDateTimeWnd::SetBlank(
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
	m_dtDate = COleDateTime::GetCurrentTime();
	m_bBlankYear = true;
	m_bBlankMonth = true;
	m_bBlankDay = true;
	m_bBlankHour = true;
	m_bBlankMinute = true;
	m_bBlankSecond = true;
	m_bBlankDesignator = true;
	UpdateDurationWnd( true, bUpdate );
}

void CExtDateTimeWnd::SetStatus( eStatus_t eStatus )
{
	ASSERT_VALID( this );
	m_dtDate.SetStatus( (COleDateTime::DateTimeStatus) eStatus );
	CExtDurationWnd::SetStatus( eStatus );
}

CExtDurationWnd::eStatus_t CExtDateTimeWnd::GetStatus() const
{
	ASSERT_VALID( this );
	return (CExtDurationWnd::eStatus_t)m_dtDate.GetStatus();
}

void CExtDateTimeWnd::SetMode( eMode_t eMode )
{
	ASSERT_VALID( this );
	m_eMode = eMode;

	bool bDatePart = ( m_eMode == date || m_eMode == all ) ? true : false;
	bool bTimePart = ( m_eMode == time || m_eMode == all ) ? true : false;
	
	SetShowItem( year, bDatePart, false );
	SetShowItem( month, bDatePart, false );
	SetShowItem( day, bDatePart, false );

	SetShowItem( hour, bTimePart, false );
	SetShowItem( minute, bTimePart, false );
	SetShowItem( second, bTimePart, false );
	SetShowItem( designator, bTimePart, false );

	UpdateDurationWnd( true, true );
}

CExtDateTimeWnd::eMode_t CExtDateTimeWnd::GetMode() const
{
	ASSERT_VALID( this );
	return m_eMode;
}

void CExtDateTimeWnd::SetTimeFormat( eTimeFormat_t eTimeFormat )
{
	ASSERT_VALID( this );
	m_eTimeFormat = eTimeFormat;
	VERIFY( _UpdateDurationFromOleDateTime( true ) );
	UpdateDurationWnd( true, true );
}

CExtDateTimeWnd::eTimeFormat_t CExtDateTimeWnd::GetTimeFormat() const
{
	ASSERT_VALID( this );
	return m_eTimeFormat;
}

void CExtDateTimeWnd::SetDateTime( 
	COleDateTime dateSrc, 
	bool bResetBlankMode, // = true
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
COleDateTime dtDatePrev = m_dtDate;
COleDateTime dtRangeMin, dtRangeMax;
	OnQueryRange(
		&dtRangeMin,
		&dtRangeMax
		);
COleDateTime dtDate = dateSrc;
	if( dtDate.GetStatus() == COleDateTime::valid )
	{
// 		if( dtDate < dtRangeMin )
// 			dtDate = dtRangeMin;
		if( dtDate.m_dt < dtRangeMin.m_dt )
			dtDate = dtRangeMin;
		if( dtDate > dtRangeMax )
			dtDate = dtRangeMax;
		if(		dtDate != dateSrc 
			&&	m_dtDate == dtDate
			)
			return;
	}
	if( OnValueChanging( dtDatePrev, dateSrc ) )
	{

		m_dtDate = dtDate;
		if( bResetBlankMode )
			m_bBlankYear = m_bBlankMonth = m_bBlankDay = m_bBlankHour = 
				m_bBlankMinute = m_bBlankSecond = m_bBlankDesignator = false;
		VERIFY( _UpdateDurationFromOleDateTime( bUpdate ) );
		OnValueChanged( dtDatePrev, m_dtDate );
	}
}

void CExtDateTimeWnd::SetDateTime( 
	VARIANT varSrc,
	bool bResetBlankMode, // = true
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
	SetDateTime( COleDateTime(varSrc), bResetBlankMode, bUpdate );
}

void CExtDateTimeWnd::SetDateTime( 
	DATE dtSrc,
	bool bResetBlankMode, // = true
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
	SetDateTime( COleDateTime(dtSrc), bResetBlankMode, bUpdate );
}

void CExtDateTimeWnd::SetDateTime( 
	time_t timeSrc,
	bool bResetBlankMode, // = true
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
	SetDateTime( COleDateTime(timeSrc), bResetBlankMode, bUpdate );
}

void CExtDateTimeWnd::SetDateTime( 
	SYSTEMTIME systimeSrc,
	bool bResetBlankMode, // = true
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
	SetDateTime( COleDateTime(systimeSrc), bResetBlankMode, bUpdate );
}

void CExtDateTimeWnd::SetDateTime( 
	FILETIME filetimeSrc,
	bool bResetBlankMode, // = true
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
	SetDateTime( COleDateTime(filetimeSrc), bResetBlankMode, bUpdate );
}

INT CExtDateTimeWnd::SetDateTime(
	INT nYear, INT nMonth, INT nDay,
	INT nHour, INT nMin, INT nSec,
	bool bResetBlankMode, // = true
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
COleDateTime dtDate;
INT nRet =  
	dtDate.SetDateTime(
		nYear, nMonth, nDay,
		nHour, nMin, nSec
		);
	if( (COleDateTime::DateTimeStatus)nRet != COleDateTime::valid )
		return nRet;

	SetDateTime( dtDate, bResetBlankMode, bUpdate );

	return COleDateTime::valid;
}

INT CExtDateTimeWnd::SetDate(
	INT nYear, INT nMonth, INT nDay,
	bool bResetBlankMode, // = true
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
	INT nRet = 0; 
	COleDateTime dtDatePrev = m_dtDate;
	if( OnValueChanging( dtDatePrev, COleDateTime( nYear, nMonth, nDay, 0, 0, 0 ) ) )
	{
		COleDateTime dtDate;
		nRet =  
			dtDate.SetDate(
				nYear, nMonth, nDay
				);
		if( (COleDateTime::DateTimeStatus)nRet == COleDateTime::valid )
		{
			m_dtDate = dtDate;
			if( bResetBlankMode )
				m_bBlankYear = m_bBlankMonth = m_bBlankDay = m_bBlankHour = 
					m_bBlankMinute = m_bBlankSecond = m_bBlankDesignator = false;
			VERIFY( _UpdateDurationFromOleDateTime( bUpdate ) );
			OnValueChanged( dtDatePrev, m_dtDate );
		}
	}
	return nRet;
}

INT CExtDateTimeWnd::SetTime(
	INT nHour, INT nMin, INT nSec,
	bool bResetBlankMode, // = true
	bool bUpdate // = true
	)
{
	ASSERT_VALID( this );
	INT nRet = 0; 
	COleDateTime dtDatePrev = m_dtDate;
	if( OnValueChanging( dtDatePrev, COleDateTime( 1899, 12, 30, nHour, nMin, nSec ) ) )
	{
		COleDateTime dtDate;
		nRet =  
			dtDate.SetTime(
				nHour, nMin, nSec
				);
		if( (COleDateTime::DateTimeStatus)nRet == COleDateTime::valid )
		{
			m_dtDate = dtDate;
			if( bResetBlankMode )
				m_bBlankYear = m_bBlankMonth = m_bBlankDay = m_bBlankHour = 
					m_bBlankMinute = m_bBlankSecond = m_bBlankDesignator = false;
			VERIFY( _UpdateDurationFromOleDateTime( bUpdate ) );
			OnValueChanged( dtDatePrev, m_dtDate );
		}
	}
	return nRet;
}

const CExtDateTimeWnd & CExtDateTimeWnd::operator = (const COleDateTime & dtSrc)
{
	ASSERT_VALID( this );
	SetDateTime( dtSrc, true, true );
	return *this;
}

const CExtDateTimeWnd & CExtDateTimeWnd::operator = (const VARIANT & varSrc)
{
	ASSERT_VALID( this );
	SetDateTime( varSrc, true, true );
	return *this;
}

const CExtDateTimeWnd & CExtDateTimeWnd::operator = (DATE dtSrc)
{
	ASSERT_VALID( this );
	SetDateTime( dtSrc, true, true );
	return *this;
}

const CExtDateTimeWnd & CExtDateTimeWnd::operator = (const time_t & timeSrc)
{
	ASSERT_VALID( this );
	SetDateTime( timeSrc, true, true );
	return *this;
}

const CExtDateTimeWnd & CExtDateTimeWnd::operator = (const SYSTEMTIME & systimeSrc)
{
	ASSERT_VALID( this );
	SetDateTime( systimeSrc, true, true );
	return *this;
}

const CExtDateTimeWnd & CExtDateTimeWnd::operator = (const FILETIME & filetimeSrc)
{
	ASSERT_VALID( this );
	SetDateTime( filetimeSrc, true, true );
	return *this;
}

BOOL CExtDateTimeWnd::operator < (const CExtDateTimeWnd & other) const
{
	ASSERT_VALID( this );
	return (m_dtDate < other.m_dtDate);
}

BOOL CExtDateTimeWnd::operator > (const CExtDateTimeWnd & other) const
{   
	ASSERT_VALID( this );
	return (m_dtDate > other.m_dtDate);
}

BOOL CExtDateTimeWnd::operator <= (const CExtDateTimeWnd & other) const
{
	ASSERT_VALID( this );
	return (m_dtDate <= other.m_dtDate);
}

BOOL CExtDateTimeWnd::operator >= (const CExtDateTimeWnd & other) const
{
	ASSERT_VALID( this );
	return (m_dtDate >= other.m_dtDate);
}

BOOL CExtDateTimeWnd::operator == (const CExtDateTimeWnd & other) const
{
	ASSERT_VALID( this );
	return (m_dtDate == other.m_dtDate);
}

BOOL CExtDateTimeWnd::operator != (const CExtDateTimeWnd & other) const
{
	ASSERT_VALID( this );
	return (m_dtDate != other.m_dtDate);
}

COleDateTime CExtDateTimeWnd::GetDateTime() const
{
	ASSERT_VALID( this );
	if(		m_bBlankYear 
		||	m_bBlankMonth
		||	m_bBlankDay
		)
	{
		COleDateTime dtDate;
		dtDate.SetStatus( COleDateTime::invalid );
		return dtDate;
	}
	return m_dtDate;
}

BOOL CExtDateTimeWnd::GetAsSystemTime(SYSTEMTIME& sysTime) const
{
	ASSERT_VALID( this );
	if(		m_bBlankYear 
		||	m_bBlankMonth
		||	m_bBlankDay
		)
		return FALSE;
	BOOL bRetVal = m_dtDate.GetAsSystemTime( sysTime );
	return bRetVal;
}

INT CExtDateTimeWnd::GetYear() const
{
	ASSERT_VALID( this );
	if( m_bBlankYear )
		return -1;
	return m_dtDate.GetYear();
}

INT CExtDateTimeWnd::GetMonth() const
{
	ASSERT_VALID( this );
	if( m_bBlankMonth )
		return -1;
	return m_dtDate.GetMonth();
}

INT CExtDateTimeWnd::GetDay() const
{
	ASSERT_VALID( this );
	if( m_bBlankDay )
		return -1;
	return m_dtDate.GetDay();
}

INT CExtDateTimeWnd::GetHour() const
{
	ASSERT_VALID( this );
	return m_dtDate.GetHour();
}

INT CExtDateTimeWnd::GetMinute() const
{
	ASSERT_VALID( this );
	return m_dtDate.GetMinute();
}

INT CExtDateTimeWnd::GetSecond() const
{
	ASSERT_VALID( this );
	return m_dtDate.GetSecond();
}

INT CExtDateTimeWnd::GetDayOfWeek() const
{
	ASSERT_VALID( this );
	if( m_bBlankDay )
		return -1;
	return m_dtDate.GetDayOfWeek();
}

INT CExtDateTimeWnd::GetDayOfYear() const
{
	ASSERT_VALID( this );
	if( m_bBlankDay )
		return -1;
	return m_dtDate.GetDayOfYear();
}

bool CExtDateTimeWnd::IsDropDownButtonVisible() const
{
	ASSERT_VALID( this );
	return m_bDropDownButtonVisible;
}

CExtDateTimeWnd::ITEM_INFO * CExtDateTimeWnd::OnInitializeItemYear(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	ITEM_INFO * pYearII = 
		new ITEM_INFO
		(
			CExtDurationWnd::year,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
		);
	ASSERT( pYearII != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pYearII->AddDependentItem( pII );
	}
	m_arrItems.Add( pYearII );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pYearII->AddDependentItem( pII );
	}
	return pYearII;
}

CExtDateTimeWnd::ITEM_INFO * CExtDateTimeWnd::OnInitializeItemMonth(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	ITEM_INFO * pMonthII = 
		new ITEM_INFO
		(
			CExtDurationWnd::month,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
		);
	ASSERT( pMonthII != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pMonthII->AddDependentItem( pII );
	}
	m_arrItems.Add( pMonthII );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pMonthII->AddDependentItem( pII );
	}	
	return pMonthII;
}

CExtDateTimeWnd::ITEM_INFO * CExtDateTimeWnd::OnInitializeItemDesignator(
	__EXT_MFC_SAFE_LPCTSTR lpszTextBefore, // = NULL
	__EXT_MFC_SAFE_LPCTSTR lpszTextAfter // = NULL 
	)
{
	ASSERT_VALID( this );
	ITEM_INFO * pDesignatorII = 
		new ITEM_INFO
		(
			CExtDurationWnd::designator,
			_T(""),
			0,
			CRect(0,0,0,0),
			true
		);
	ASSERT( pDesignatorII != NULL );

	if( lpszTextBefore != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextBefore,
				0,
				CRect(0,0,0,0),
				true,
				true // before
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pDesignatorII->AddDependentItem( pII );
	}
	m_arrItems.Add( pDesignatorII );

	if( lpszTextAfter != NULL )
	{
		ITEM_INFO * pII = 
			new ITEM_INFO
			(
				CExtDurationWnd::label,
				lpszTextAfter,
				0,
				CRect(0,0,0,0),
				true,
				false // after
			);
		ASSERT( pII != NULL );
		m_arrItems.Add( pII );
		pDesignatorII->AddDependentItem( pII );
	}	
	return pDesignatorII;
}

void CExtDateTimeWnd::OnInitializeItemsArray()
{
	ASSERT_VALID( this );

CExtSafeString sTimeDesignatorPos = OnQueryTimeDesignatorPosition();
CExtSafeString sTimeSeparator = OnQueryTimeSeparator();
CExtSafeString sDateSeparator = OnQueryDateSeparator();

// Short Date format-ordering specifier. 
// 0 - Month - Day - Year
// 1 - Day - Month - Year
// 2 - Year - Month - Day
LPTSTR lpszBuf = (LPTSTR)::LocalAlloc(LPTR, 50);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_IDATE,
			lpszBuf,
			50
			) != 0
		);
	CExtSafeString sDate = (LPTSTR)lpszBuf;
	ASSERT( !sDate.IsEmpty() );
	if( sDate.IsEmpty() )
		sDate = _T("0");
	::LocalFree( lpszBuf );

	switch( sDate[0] )
	{
	case '0':	// Month-Day-Year
	default:
		OnInitializeItemMonth( NULL, NULL );
		OnInitializeItemDay( sDateSeparator, NULL );
		OnInitializeItemYear( sDateSeparator, NULL );
	break;
	case '1':	// Day-Month-Year
		OnInitializeItemDay( NULL, NULL );
		OnInitializeItemMonth( sDateSeparator, NULL );
		OnInitializeItemYear( sDateSeparator, NULL );
	break;
	case '2':	// Year-Month-Day
		OnInitializeItemYear( NULL, NULL );
		OnInitializeItemMonth( sDateSeparator, NULL );
		OnInitializeItemDay( sDateSeparator, NULL );
	break;
	} // switch( sDate[0] )

		// designator
BOOL bFirstDesignator = ( _tcscmp( sTimeDesignatorPos, _T("1") ) == 0 );
	if( bFirstDesignator )
		OnInitializeItemDesignator( _T(" "), NULL );

	// hour
	OnInitializeItemHour( _T(" "), NULL );
	// minute
	OnInitializeItemMinute( sTimeSeparator, NULL );
	// second
	OnInitializeItemSecond( sTimeSeparator, NULL );
		
	// designator
	if( !bFirstDesignator )
		OnInitializeItemDesignator( _T(" "), NULL );
}

bool CExtDateTimeWnd::OnQueryTimeFormat24Hours() const
{
	eTimeFormat_t eTimeFormat = GetTimeFormat();

	if( eTimeFormat == h12 )
		return false;
	else if( eTimeFormat == h24 )
		return true;

	// Time format specifier. 
    // 0 - AM/PM 12-hour format.
    // 1 - 24-hour format.
	ASSERT_VALID( this );
	LPTSTR lpszBuf = (LPTSTR)::LocalAlloc(LPTR, 10);
	VERIFY(
		g_ResourceManager->GetLocaleInfo(
			LOCALE_ITIME,
			lpszBuf,
			10
			) != 0
		);
CExtSafeString s = (LPTSTR)lpszBuf;
	::LocalFree( lpszBuf );
	return ( _tcscmp( s, _T("0") ) == 0) ? false : true;
}

CExtSafeString CExtDateTimeWnd::OnQueryItemText( 
	const ITEM_INFO * pII 
	) const
{
	ASSERT_VALID( this );
	ASSERT( pII != NULL );
	if( pII == NULL )
		return _T("");
	CExtSafeString sText;
	switch( pII->m_eItemType ) 
	{
	case CExtDurationWnd::label:
		sText.Format( _T("%s"), pII->m_sText );
		break;
	case CExtDurationWnd::day:
		{
			CExtSafeString s = OnQueryDayLeadingZeros();
			CExtSafeString sFormatString = _T("%d");
			if( _tcscmp( s, _T("1") ) == 0 )
				sFormatString = _T("%02d");
			sText.Format( LPCTSTR(sFormatString), pII->m_nValue );
		}
		break;
	case CExtDurationWnd::month:
		{
			CExtSafeString s = OnQueryMonthLeadingZeros();
			CExtSafeString sFormatString = _T("%d");
			if( _tcscmp( s, _T("1") ) == 0 )
				sFormatString = _T("%02d");
			sText.Format( LPCTSTR(sFormatString), pII->m_nValue );
		}
		break;
	case CExtDurationWnd::year:
		{
			CExtSafeString s = OnQueryYearFormat();
			CExtSafeString sFormatString = _T("%04d");
			INT nValue = pII->m_nValue;
			if( _tcscmp( s, _T("0") ) == 0 )
			{
				nValue = nValue % 100;
				sFormatString = _T("%02d");
			}
			sText.Format( LPCTSTR(sFormatString), nValue );
		}
		break;
	case CExtDurationWnd::hour:
		{
			CExtSafeString s = OnQueryHourLeadingZeros();
			CExtSafeString sFormatString = _T("%d");
			if( _tcscmp( s, _T("1") ) == 0 )
				sFormatString = _T("%02d");
			sText.Format( LPCTSTR(sFormatString), pII->m_nValue );
		}
		break;
	case CExtDurationWnd::minute:
	case CExtDurationWnd::second:
		sText.Format( _T("%02d"), pII->m_nValue );
		break;
	case CExtDurationWnd::designator:
		if( !OnQueryTimeFormat24Hours() )
		{
			sText = 
				m_dtDate.GetHour() < 12
					? OnQueryTimeDesignatorAM()
					: OnQueryTimeDesignatorPM();
		}
		break;
	}	

CExtSafeString sTextBlank = sText;
	for( INT i = 0; i < sTextBlank.GetLength(); i++ )
		sTextBlank.SetAt( i, _T(' ') );
	switch( pII->m_eItemType ) 
	{
	case CExtDurationWnd::day:
		if( m_bBlankDay )
			sText = sTextBlank;
		break;
	case CExtDurationWnd::month:
		if( m_bBlankMonth )
			sText = sTextBlank;
		break;
	case CExtDurationWnd::year:
		if( m_bBlankYear )
			sText = sTextBlank;
		break;
	case CExtDurationWnd::hour:
		if( m_bBlankHour )
			sText = sTextBlank;
		break;
	case CExtDurationWnd::minute:
		if( m_bBlankMinute )
			sText = sTextBlank;
		break;
	case CExtDurationWnd::second:
		if( m_bBlankSecond )
			sText = sTextBlank;
		break;
	case CExtDurationWnd::designator:
		if( m_bBlankDesignator )
			sText = sTextBlank;
		break;
	}	
	
	return sText;
}

bool CExtDateTimeWnd::_CreateHelper()
{
	ASSERT_VALID( this );
	bool bRet = CExtDurationWnd::_CreateHelper();
	VERIFY( _UpdateDurationFromOleDateTime( true ) );
	UpdateDurationWnd( true, false );
	return bRet;
}

void CExtDateTimeWnd::_RecalcDuration()
{
	ASSERT_VALID( this );
	SetShowItem( 
		CExtDurationWnd::designator,
		!OnQueryTimeFormat24Hours() && ( m_eMode == time || m_eMode == all )
		);
}

bool CExtDateTimeWnd::OnShowDropDownMenu()
{
	ASSERT_VALID( this );

	VERIFY( _UpdateDurationFromOleDateTime( true ) );

	CExtPopupDatePickerMenuWnd * pPopup = 
		new CExtPopupDatePickerMenuWnd;
	if( !pPopup->CreatePopupMenu( GetSafeHwnd() ) )
	{
		ASSERT( FALSE );
		delete pPopup;
		return false;
	}
	pPopup->m_dwDatePickerStyle = 
			__EDPWS_BORDER
		| __EDPWS_SHOW_NON_MONTH_DAYS
		| __EDPWS_HIDE_INNER_NON_MONTH_DAYS
		| __EDPWS_SHOW_TODAY_HIGHLIGHT
		| __EDPWS_SHOW_MONTH_LIST_SHADOW
		| __EDPWS_BUTTON_TODAY
		| __EDPWS_BUTTON_NONE
		| __EDPWS_BUTTON_SEPARATOR
		;
	CRect rcClient;
	GetClientRect( &rcClient );
	ClientToScreen( &rcClient );
	CPoint ptTrack(
		rcClient.right, 
		rcClient.bottom
		);
	ptTrack.x += 
		pPopup->OnQueryMenuShadowSize();
	CRect rcExclude( ptTrack, ptTrack );
	if(	! pPopup->TrackPopupMenu(
			TPMX_RIGHTALIGN,
			ptTrack.x, 
			ptTrack.y,
			rcExclude,
			this,
			NULL,
			NULL,
			true
			) 
		)
	{
		ASSERT( FALSE );
		delete pPopup;
		return false;
	}
	return true;
}

void CExtDateTimeWnd::OnDropDownButtonClicked()
{
	ASSERT_VALID( this );
	if( IsReadOnly() )
		return;
	OnShowDropDownMenu();
}

LRESULT CExtDateTimeWnd::OnDropDownCalendarSelChanged( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;
	const CExtDatePickerWnd::SELECTION_NOTIFICATION * pSN =
		CExtDatePickerWnd::SELECTION_NOTIFICATION::FromWPARAM( wParam );
	ASSERT( pSN != NULL );
	if(		pSN->m_bFinalSelectionChanging 
		&&	(!IsReadOnly())
		)
	{
		if(		pSN->m_dtBegin.GetStatus() == COleDateTime::valid 
			&&	GetStatus() == CExtDateTimeWnd::valid
			)
			SetDateTime( 
				pSN->m_dtBegin.GetYear(),
				pSN->m_dtBegin.GetMonth(),
				pSN->m_dtBegin.GetDay(),
				m_dtDate.GetHour(),
				m_dtDate.GetMinute(),
				m_dtDate.GetSecond(),
				true, true
				);
		else
			SetDateTime( 
				pSN->m_dtBegin,
				true, true
				);
	}
	return 0L;
}

LRESULT CExtDateTimeWnd::OnDropDownCalendarInit( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	lParam;
	CExtDatePickerWnd * pDatePickerWnd =
		STATIC_DOWNCAST( CExtDatePickerWnd, (CWnd*) wParam );
	ASSERT( pDatePickerWnd != NULL );
	ASSERT_VALID( pDatePickerWnd );
	eStatus_t eStatus = GetStatus();
	if( eStatus == CExtDateTimeWnd::valid )
	{
		pDatePickerWnd->SelectionSet( m_dtDate, m_dtDate );
		pDatePickerWnd->EnsureVisible( m_dtDate );
	}
	return 0L;
}

INT CExtDateTimeWnd::stat_GetDaysInMonth( INT nYear, INT nMonth )
{
	if( nYear <= __EXT_DATE_YEAR_MIN )
		nYear += 28;
	else if( nYear >= __EXT_DATE_YEAR_MAX )
		nYear -= 28;
	COleDateTime dt1( nYear, nMonth, 1, 0, 0, 0 );
	ASSERT( dt1.GetStatus() == COleDateTime::valid 
		||	dt1.GetStatus() == COleDateTime::null 
		);
	if( dt1.GetStatus() != COleDateTime::valid )
		return 0;
	INT nNextYear = nYear;
	INT nNextMonth = nMonth + 1;
	if( nNextMonth > 12 )
	{
		nNextMonth = 1;
		nNextYear += 1;
	}
	else if( nNextMonth < 1 )
	{
		nNextMonth = 12;
		nNextYear -= 1;
	}
	COleDateTime dt2( 
		nNextYear,
		nNextMonth,
		1,
		0,0,0
		);
	COleDateTimeSpan dtDays = (dt2 - dt1);
	return (INT)dtDays.GetTotalDays();
}

bool CExtDateTimeWnd::OnValueChanging(
	const COleDateTime & dtDateTimeOld,
	const COleDateTime & dtDateTimeNew
	) const
{
	ASSERT_VALID( this );
	HWND hWndNotificationReceiver = OnQueryNotificationReceiver();
	if( hWndNotificationReceiver != NULL )
	{
		CHANGING_NOTIFICATION _SN( 
			false,
			dtDateTimeOld, 
			dtDateTimeNew,
			m_lParamCookie
			);
		LRESULT lResult = _SN.Notify( hWndNotificationReceiver );
		if( lResult < 0 )
			return false;
	} // if( hWndNotificationReceiver != NULL )
	return true;
}

bool CExtDateTimeWnd::OnValueChanged(
	const COleDateTime & dtDateTimeOld,
	const COleDateTime &dtDateTimeNew
	) const
{
	ASSERT_VALID( this );
	HWND hWndNotificationReceiver = OnQueryNotificationReceiver();
	if( hWndNotificationReceiver != NULL )
	{
		CHANGING_NOTIFICATION _SN( 
			true,
			dtDateTimeOld, 
			dtDateTimeNew,
			m_lParamCookie
			);
		LRESULT lResult = _SN.Notify( hWndNotificationReceiver );
		if( lResult < 0 )
			return false;
	} // if( hWndNotificationReceiver != NULL )
	return true;
}

void CExtDateTimeWnd::OnItemFinishInput()
{
	ASSERT_VALID( this );
	CExtDurationWnd::OnItemFinishInput();

	if(		m_pLastInputItem != NULL 
		&&	m_pLastInputItem->m_eItemType == CExtDateTimeWnd::year
		)
	{
		INT nYear = m_pLastInputItem->m_nValue;
		if( nYear >= 0 && nYear <= 29 )
			nYear += 2000;
		else if( nYear >= 30 && nYear <= 99 )
			nYear += 1900;

		if( m_pLastInputItem->m_nValue != nYear )
		{
			INT nMonth	= m_dtDate.GetMonth();
			INT nDay	= m_dtDate.GetDay();
			INT nHour	= m_dtDate.GetHour();
			INT nMinute = m_dtDate.GetMinute();
			INT nSecond = m_dtDate.GetSecond();

			COleDateTime dtTest( 
				nYear,
				nMonth,
				nDay,
				nHour,
				nMinute,
				nSecond 
				);
			if( dtTest.GetStatus() == COleDateTime::valid )
			{
				m_bBlankYear = false;
				
				if(		nMonth != m_dtDate.GetMonth() 
					&&	m_bBlankMonth
					)
					m_bBlankMonth = false;
				if(		nDay != m_dtDate.GetDay() 
					&&	m_bBlankDay
					)
					m_bBlankDay = false;
				if(		nHour != m_dtDate.GetHour() 
					&&	m_bBlankHour
					)
					m_bBlankHour = false;
				if(		nMinute != m_dtDate.GetMinute() 
					&&	m_bBlankMinute
					)
					m_bBlankMinute = false;
				if(		nSecond != m_dtDate.GetSecond() 
					&&	m_bBlankSecond
					)
					m_bBlankSecond = false;

				SetDateTime(
					nYear,
					nMonth,
					nDay,
					nHour,
					nMinute,
					nSecond,
					false, true
					);
			}
		} // if( m_pLastInputItem->m_nValue != nYear )
	}
}

void CExtDateTimeWnd::OnItemSelectionChanged()
{
	ASSERT_VALID( this );
	VERIFY( _UpdateDurationFromOleDateTime( true ) );
}

void CExtDateTimeWnd::OnKillFocus(CWnd* pNewWnd) 
{
	ASSERT_VALID( this );
	CExtDurationWnd::OnKillFocus(pNewWnd);
	VERIFY( _UpdateDurationFromOleDateTime( true ) );
}

void CExtDateTimeWnd::ScrollCurrentItem( INT nDelta )
{
	ASSERT_VALID( this );
COleDateTime dtRangeMin, dtRangeMax;
	OnQueryRange(
		&dtRangeMin,
		&dtRangeMax
		);
ITEM_INFO * pII = SelectionGet();
	if( pII != NULL )
	{
		switch( pII->m_eItemType ) 
		{
		case CExtDurationWnd::day:
			if( m_bContinuousScrolling )
			{
				COleDateTimeSpan dtSpan( nDelta, 0, 0, 0 );
				SetDateTime( 
					m_dtDate + dtSpan,
					false, true
					);
			}
			else
			{
				INT nDaysInMonth = 
					CExtDateTimeWnd::stat_GetDaysInMonth( 
						m_dtDate.GetYear(),
						m_dtDate.GetMonth()
						);
				INT nValue = pII->m_nValue;
				nValue += nDelta;
				if( nValue > nDaysInMonth )
					nValue = 1;
				if( nValue < 1 )
					nValue = nDaysInMonth;
				SetDateTime(
					m_dtDate.GetYear(),
					m_dtDate.GetMonth(),
					nValue,
					m_dtDate.GetHour(),
					m_dtDate.GetMinute(),
					m_dtDate.GetSecond(),
					false, true
					);
			}
			m_bBlankDay = false;
			break;
		case CExtDurationWnd::month:
			{
				INT nNextYear = m_dtDate.GetYear();
				INT nNextMonth = m_dtDate.GetMonth() + nDelta;
				if( nNextMonth > 12 )
				{
					nNextMonth = 1;
					if( m_bContinuousScrolling )
					{
						nNextYear += 1;
						m_bBlankYear = false;
					}
				}
				else if( nNextMonth < 1 )
				{
					nNextMonth = 12;
					if( m_bContinuousScrolling )
					{
						nNextYear -= 1;
						m_bBlankYear = false;
					}
				}		
				INT nDaysInMonth = 
					CExtDateTimeWnd::stat_GetDaysInMonth( 
						nNextYear,
						nNextMonth
						);
				INT nNextDay = m_dtDate.GetDay();
				if( nNextDay > nDaysInMonth )
				{
					nNextDay = nDaysInMonth;
					m_bBlankDay = false;
				}
				SetDateTime(
					nNextYear,
					nNextMonth,
					nNextDay,
					m_dtDate.GetHour(),
					m_dtDate.GetMinute(),
					m_dtDate.GetSecond(),
					false, true
					);
				m_bBlankMonth = false;
			}
			break;
		case CExtDurationWnd::year:
			{
				INT nNextYear = pII->m_nValue;
				nNextYear += nDelta;
				if( nNextYear > dtRangeMax.GetYear() )
					nNextYear = dtRangeMin.GetYear();
				if( nNextYear < dtRangeMin.GetYear() )
					nNextYear = dtRangeMax.GetYear();
				INT nDaysInMonth = 
					CExtDateTimeWnd::stat_GetDaysInMonth( 
						nNextYear,
						m_dtDate.GetMonth()
						);
				INT nNextDay = m_dtDate.GetDay();
				if( nNextDay > nDaysInMonth )
				{
					nNextDay = nDaysInMonth;
					m_bBlankDay = false;
				}
				SetDateTime(
					nNextYear,
					m_dtDate.GetMonth(),
					nNextDay,
					m_dtDate.GetHour(),
					m_dtDate.GetMinute(),
					m_dtDate.GetSecond(),
					false, true
					);
				m_bBlankYear = false;
			}
			break;
		case CExtDurationWnd::hour:
			{
				COleDateTime dtDate = m_dtDate;
				INT nNextHour = m_dtDate.GetHour() + nDelta;
				if( nNextHour > 23 )
				{
					nNextHour = 0;
					if( m_bContinuousScrolling )
						dtDate += COleDateTimeSpan( 1, 0, 0, 0 );
				}
				else if( nNextHour < 0 )
				{
					nNextHour = 23;
					if( m_bContinuousScrolling )
						dtDate -= COleDateTimeSpan( 1, 0, 0, 0 );
				}	
				
				if(		dtDate.GetYear() != m_dtDate.GetYear() 
					&&	m_bBlankYear
					)
					m_bBlankYear = false;
				if(		dtDate.GetMonth() != m_dtDate.GetMonth() 
					&&	m_bBlankMonth
					)
					m_bBlankMonth = false;
				if(		dtDate.GetDay() != m_dtDate.GetDay() 
					&&	m_bBlankDay
					)
					m_bBlankDay = false;

				m_bBlankHour = false;

				SetDateTime(
					dtDate.GetYear(),
					dtDate.GetMonth(),
					dtDate.GetDay(),
					nNextHour,
					dtDate.GetMinute(),
					dtDate.GetSecond(),
					false, true
					);
			}
			break;
		case CExtDurationWnd::minute:
			{
				COleDateTime dtDate = m_dtDate;
				INT nNextMinute = m_dtDate.GetMinute() + nDelta;
				if( nNextMinute > 59 )
				{
					nNextMinute = 0;
					if( m_bContinuousScrolling )
						dtDate += COleDateTimeSpan( 0, 1, 0, 0 );
				}
				else if( nNextMinute < 0 )
				{
					nNextMinute = 59;
					if( m_bContinuousScrolling )
						dtDate -= COleDateTimeSpan( 0, 1, 0, 0 );
				}	

				if(		dtDate.GetYear() != m_dtDate.GetYear() 
					&&	m_bBlankYear
					)
					m_bBlankYear = false;
				if(		dtDate.GetMonth() != m_dtDate.GetMonth() 
					&&	m_bBlankMonth
					)
					m_bBlankMonth = false;
				if(		dtDate.GetDay() != m_dtDate.GetDay() 
					&&	m_bBlankDay
					)
					m_bBlankDay = false;
				if(		dtDate.GetHour() != m_dtDate.GetHour() 
					&&	m_bBlankHour
					)
					m_bBlankHour = false;

				m_bBlankMinute = false;

				SetDateTime(
					dtDate.GetYear(),
					dtDate.GetMonth(),
					dtDate.GetDay(),
					dtDate.GetHour(),
					nNextMinute,
					dtDate.GetSecond(),
					false, true
					);
			}
			break;
		case CExtDurationWnd::second:
			{
				COleDateTime dtDate = m_dtDate;
				INT nNextSecond = m_dtDate.GetSecond() + nDelta;
				if( nNextSecond > 59 )
				{
					nNextSecond = 0;
					if( m_bContinuousScrolling )
						dtDate += COleDateTimeSpan( 0, 0, 1, 0 );
				}
				else if( nNextSecond < 0 )
				{
					nNextSecond = 59;
					if( m_bContinuousScrolling )
						dtDate -= COleDateTimeSpan( 0, 0, 1, 0 );
				}	
				
				if(		dtDate.GetYear() != m_dtDate.GetYear() 
					&&	m_bBlankYear
					)
					m_bBlankYear = false;
				if(		dtDate.GetMonth() != m_dtDate.GetMonth() 
					&&	m_bBlankMonth
					)
					m_bBlankMonth = false;
				if(		dtDate.GetDay() != m_dtDate.GetDay() 
					&&	m_bBlankDay
					)
					m_bBlankDay = false;
				if(		dtDate.GetHour() != m_dtDate.GetHour() 
					&&	m_bBlankHour
					)
					m_bBlankHour = false;
				if(		dtDate.GetMinute() != m_dtDate.GetMinute() 
					&&	m_bBlankMinute
					)
					m_bBlankMinute = false;
			
				m_bBlankSecond = false;

				SetDateTime(
					dtDate.GetYear(),
					dtDate.GetMonth(),
					dtDate.GetDay(),
					dtDate.GetHour(),
					dtDate.GetMinute(),
					nNextSecond,
					false, true
					);
			}
			break;
		case CExtDurationWnd::designator:
			if( !OnQueryTimeFormat24Hours() )
			{
				INT nHour = m_dtDate.GetHour();
				nHour += (nHour < 12) ? +12 : -12;
				SetDateTime(
					m_dtDate.GetYear(),
					m_dtDate.GetMonth(),
					m_dtDate.GetDay(),
					nHour,
					m_dtDate.GetMinute(),
					m_dtDate.GetSecond(),
					false, true
					);
				m_bBlankDesignator = false;
			}
			break;
		default:
			ASSERT( FALSE );
		}
		UpdateDurationWnd( true, true );
	} // if( pII != NULL )
}

void CExtDateTimeWnd::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	ASSERT_VALID( this );
COleDateTime dtRangeMin, dtRangeMax;
	OnQueryRange(
		&dtRangeMin,
		&dtRangeMax
		);
	switch( nChar )
	{
	case VK_RETURN:
		if(		GetStatus() != CExtDateTimeWnd::valid 
			&&	(!IsReadOnly()) 
			)
			SetDateTime( COleDateTime::GetCurrentTime(), true, true );
		break;
	case VK_HOME:
	case VK_END:
		if( ! IsReadOnly() )
		{
			ITEM_INFO * pII = SelectionGet();
			if( pII != NULL )
			{
				switch( pII->m_eItemType ) 
				{
				case CExtDurationWnd::day:
					{
						INT nDaysInMonth = 
							CExtDateTimeWnd::stat_GetDaysInMonth( 
								m_dtDate.GetYear(),
								m_dtDate.GetMonth()
								);
						SetDateTime(
							m_dtDate.GetYear(),
							m_dtDate.GetMonth(),
							nChar == VK_HOME ? 1 : nDaysInMonth,
							m_dtDate.GetHour(),
							m_dtDate.GetMinute(),
							m_dtDate.GetSecond(),
							false, true
							);
						m_bBlankDay = false;
					}
					break;
				case CExtDurationWnd::month:
					{
						INT nMonthNew = (nChar == VK_HOME) ? 1 : 12;
						INT nDaysInMonth = 
							CExtDateTimeWnd::stat_GetDaysInMonth( 
								m_dtDate.GetYear(),
								nMonthNew
								);
						INT nDayNew = m_dtDate.GetDay();
						if( nDayNew > nDaysInMonth )
						{
							nDayNew = nDaysInMonth;
							m_bBlankDay = false;
						}
						SetDateTime(
							m_dtDate.GetYear(),
							nMonthNew,
							nDayNew,
							m_dtDate.GetHour(),
							m_dtDate.GetMinute(),
							m_dtDate.GetSecond(),
							false, true
							);
						m_bBlankMonth = false;
					}
					break;
				case CExtDurationWnd::year:
					{
						INT nYearNew =
							(nChar == VK_HOME) 
								? dtRangeMin.GetYear() 
								: dtRangeMax.GetYear();
						INT nDaysInMonth = 
							CExtDateTimeWnd::stat_GetDaysInMonth( 
								nYearNew,
								m_dtDate.GetMonth()
								);
						INT nDayNew = m_dtDate.GetDay();
						if( nDayNew > nDaysInMonth )
						{
							nDayNew = nDaysInMonth;
							m_bBlankDay = false;
						}
						SetDateTime(
							nYearNew,
							m_dtDate.GetMonth(),
							nDayNew,
							m_dtDate.GetHour(),
							m_dtDate.GetMinute(),
							m_dtDate.GetSecond(),
							false, true
							);
						m_bBlankYear = false;
					}
					break;
				case CExtDurationWnd::hour:
					SetDateTime(
						m_dtDate.GetYear(),
						m_dtDate.GetMonth(),
						m_dtDate.GetDay(),
						nChar == VK_HOME ? 0 : 23,
						m_dtDate.GetMinute(),
						m_dtDate.GetSecond(),
						false, true
						);
					m_bBlankHour = false;
					break;
				case CExtDurationWnd::minute:
					SetDateTime(
						m_dtDate.GetYear(),
						m_dtDate.GetMonth(),
						m_dtDate.GetDay(),
						m_dtDate.GetHour(),
						nChar == VK_HOME ? 0 : 59,
						m_dtDate.GetSecond(),
						false, true
						);
					m_bBlankMinute = false;
					break;
				case CExtDurationWnd::second:
					SetDateTime(
						m_dtDate.GetYear(),
						m_dtDate.GetMonth(),
						m_dtDate.GetDay(),
						m_dtDate.GetHour(),
						m_dtDate.GetMinute(),
						nChar == VK_HOME ? 0 : 59,
						false, true
						);
					m_bBlankSecond = false;
					break;
				case CExtDurationWnd::designator:
					if( !OnQueryTimeFormat24Hours() )
					{
						INT nHour = m_dtDate.GetHour();
						if( nChar == VK_HOME && nHour > 12 )
							nHour -= 12;
						else if( nChar == VK_END && nHour < 12 )
							nHour += 12;
						SetDateTime(
							m_dtDate.GetYear(),
							m_dtDate.GetMonth(),
							m_dtDate.GetDay(),
							nHour,
							m_dtDate.GetMinute(),
							m_dtDate.GetSecond(),
							false, true
							);
						m_bBlankDesignator = false;
					}
				break;
				default:
					ASSERT( FALSE );
				}
			} // if( pII != NULL )
		}
		break;
	case 67: // C
	case 86: // V
	case VK_INSERT:
		{
			ITEM_INFO * pII = SelectionGet();
			if( pII != NULL )
			{
				bool bCtrl = ( (::GetAsyncKeyState(VK_CONTROL)&0x8000) != 0 ) ? true : false;
				bool bShift = ( (::GetAsyncKeyState(VK_SHIFT)&0x8000) != 0 ) ? true : false;
				if(		( nChar == 67 && bCtrl ) 
					||	( nChar == VK_INSERT && bCtrl )  
					)
				{
					CExtSafeString sText = OnQueryItemText( pII );
					sText.Remove( _T(' ') );

					// copy to clipboard
					if( OpenClipboard() )
					{
						if( EmptyClipboard() )
						{
							HGLOBAL hGlobal =
								::GlobalAlloc( 
									GMEM_DDESHARE, 
									sText.GetLength() + 1 
									);
							ASSERT( hGlobal != NULL );
							if( hGlobal != NULL )
							{
								LPTSTR lpszBuffer = 
									(LPTSTR) ::GlobalLock( hGlobal );
								__EXT_MFC_STRCPY( 
									lpszBuffer, 
									sText.GetLength() + 1,
									LPCTSTR(sText)
									);
								::GlobalUnlock( hGlobal );
								::SetClipboardData( 
									CF_TEXT, 
									hGlobal 
									);
							} // if( hGlobal != NULL )
						} // if( EmptyClipboard() )
						::CloseClipboard();
					} // if( OpenClipboard() )
				}
				else 
					if( ! IsReadOnly() )
					{
						if(		( nChar == 86 && bCtrl ) 
							||	( nChar == VK_INSERT && bShift ) 
							)
						{
							// paste from clipboard
							if( OpenClipboard() )
							{
								LPTSTR lpszBuffer = 
									(LPTSTR) ::GetClipboardData( CF_TEXT );
								if( lpszBuffer != NULL )
								{
									LONG nValue = 
										_ttol( lpszBuffer );
									INT nYear	= m_dtDate.GetYear();
									INT nMonth	= m_dtDate.GetMonth();
									INT nDay	= m_dtDate.GetDay();
									INT nHour	= m_dtDate.GetHour();
									INT nMinute = m_dtDate.GetMinute();
									INT nSecond = m_dtDate.GetSecond();
									switch( pII->m_eItemType ) 
									{
									case CExtDurationWnd::day:
										{
											INT nDaysInMonth = 
												CExtDateTimeWnd::stat_GetDaysInMonth( 
													nYear,
													nMonth
													);
											if( nValue < 1 )
												nDay = 1;
											else if( nValue > nDaysInMonth )
												nDay = nDaysInMonth;
											else
												nDay = nValue;
										}
										break;
									case CExtDurationWnd::month:
										{
											if( nValue < 1 )
												nMonth = 1;
											else if( nValue > 12 )
												nMonth = 12;
											else
												nMonth = nValue;
											INT nDaysInMonth = 
												CExtDateTimeWnd::stat_GetDaysInMonth( 
													nYear,
													nMonth
													);
											if( nDay > nDaysInMonth )
												nDay = nDaysInMonth;
										}								
										break;
									case CExtDurationWnd::year:
										{
											if( nValue < dtRangeMin.GetYear() )
												nYear = dtRangeMin.GetYear();
											else if( nValue > dtRangeMax.GetYear() )
												nYear = dtRangeMax.GetYear();
											else
												nYear = nValue;
											INT nDaysInMonth = 
												CExtDateTimeWnd::stat_GetDaysInMonth( 
													nYear,
													nMonth
													);
											if( nDay > nDaysInMonth )
												nDay = nDaysInMonth;
										}
										break;
									case CExtDurationWnd::hour:
										{
											if( nValue < 0 )
												nHour = 0;
											else if( nValue > 23 )
												nHour = 23;
											else
												nHour = nValue;
										}								
										break;
									case CExtDurationWnd::minute:
										{
											if( nValue < 0 )
												nMinute = 0;
											else if( nValue > 59 )
												nMinute = 59;
											else
												nMinute = nValue;
										}								
										break;
									case CExtDurationWnd::second:
										{
											if( nValue < 0 )
												nSecond = 0;
											else if( nValue > 59 )
												nSecond = 59;
											else
												nSecond = nValue;
										}								
										break;
									case CExtDurationWnd::designator:
										{
											CString sAM = OnQueryTimeDesignatorAM();
											CString sPM = OnQueryTimeDesignatorPM();
											if(	!OnQueryTimeFormat24Hours() )
											{
												if(	!sAM.CompareNoCase( lpszBuffer ) )
												{
													if( nHour > 12 )
														nHour -= 12;
													m_bBlankDesignator = false;
												}
												else if( !sPM.CompareNoCase( lpszBuffer ) )
												{
													if( nHour < 12 )
														nHour += 12;
													m_bBlankDesignator = false;
												}
											}
										}
										break;
									default:
										ASSERT( FALSE );
									} // switch( pII->m_eItemType ) 

									if(		nYear	!= m_dtDate.GetYear()
										||	nMonth	!= m_dtDate.GetMonth()
										||	nDay	!= m_dtDate.GetDay()
										||	nHour	!= m_dtDate.GetHour()
										||	nMinute != m_dtDate.GetMinute()
										||	nSecond != m_dtDate.GetSecond()
										)
									{
										if(		nYear != m_dtDate.GetYear() 
											&&	m_bBlankYear
											)
											m_bBlankYear = false;
										if(		nMonth != m_dtDate.GetMonth() 
											&&	m_bBlankMonth
											)
											m_bBlankMonth = false;
										if(		nDay != m_dtDate.GetDay() 
											&&	m_bBlankDay
											)
											m_bBlankDay = false;
										if(		nHour != m_dtDate.GetHour() 
											&&	m_bBlankHour
											)
											m_bBlankHour = false;
										if(		nMinute != m_dtDate.GetMinute() 
											&&	m_bBlankMinute
											)
											m_bBlankMinute = false;
										if(		nSecond != m_dtDate.GetSecond() 
											&&	m_bBlankSecond
											)
											m_bBlankSecond = false;

										SetDateTime(
											nYear,
											nMonth,
											nDay,
											nHour,
											nMinute,
											nSecond,
											false, true
											);
									}
								} // if( lpszBuffer != NULL )
								::CloseClipboard();
							} // if( OpenClipboard() )
						}
					} // if( ! IsReadOnly() )
			} // if( pII != NULL )
		}
		break;
	default:
		if( ! IsReadOnly() )
		{
			CString sAM = OnQueryTimeDesignatorAM();
			CString sPM = OnQueryTimeDesignatorPM();
			if(		!sAM.IsEmpty() 
				&&	sAM[0] == (TCHAR)nChar 
				&&	!OnQueryTimeFormat24Hours() 
				)
			{
				INT nHour = m_dtDate.GetHour();
				if( nHour > 12 )
				{
					SetDateTime(
						m_dtDate.GetYear(),
						m_dtDate.GetMonth(),
						m_dtDate.GetDay(),
						nHour - 12,
						m_dtDate.GetMinute(),
						m_dtDate.GetSecond(),
						false, true
						);
					m_bBlankHour = false;
				}
			}
			else if(	!sPM.IsEmpty() 
					&&	sPM[0] == (TCHAR)nChar 
					&&	!OnQueryTimeFormat24Hours() 
					)
			{
				INT nHour = m_dtDate.GetHour();
				if( nHour < 12 )
				{
					SetDateTime(
						m_dtDate.GetYear(),
						m_dtDate.GetMonth(),
						m_dtDate.GetDay(),
						nHour + 12,
						m_dtDate.GetMinute(),
						m_dtDate.GetSecond(),
						false, true
						);
					m_bBlankHour = false;
				}
			}
			else
			{	
				CExtDurationWnd::OnKeyDown(nChar, nRepCnt, nFlags);
				return;
			}
		}
		else
		{	
			CExtDurationWnd::OnKeyDown(nChar, nRepCnt, nFlags);
			return;
		}
	} // switch( nChar )
	UpdateDurationWnd( true, true );
}

void CExtDateTimeWnd::OnDigitPressed(
	UINT nDigit
	)
{
	ASSERT_VALID( this );
	if( IsReadOnly() )
		return;
COleDateTime dtRangeMin, dtRangeMax;
	OnQueryRange(
		&dtRangeMin,
		&dtRangeMax
		);
ITEM_INFO * pII = SelectionGet();
	if( pII != NULL )
	{
		INT nValue = 0;
		if( pII == m_pLastInputItem )
			nValue = pII->m_nValue * 10 + nDigit;
		else
			nValue = nDigit;

		INT nYear	= m_dtDate.GetYear();
		INT nMonth	= m_dtDate.GetMonth();
		INT nDay	= m_dtDate.GetDay();
		INT nHour	= m_dtDate.GetHour();
		INT nMinute = m_dtDate.GetMinute();
		INT nSecond = m_dtDate.GetSecond();

		switch( pII->m_eItemType ) 
		{
		case CExtDurationWnd::day:
			{
				INT nDaysInMonth = 
					CExtDateTimeWnd::stat_GetDaysInMonth( 
						nYear,
						nMonth
						);
				if(	nValue < 0 || nValue > nDaysInMonth )
					nValue = nDigit;
				if( nValue > nDaysInMonth )
					nValue = nDaysInMonth;
				pII->m_nValue = nValue;
				if( nValue > 0 && nValue <= nDaysInMonth )
					nDay = nValue;
				m_bBlankDay = false;
			}
			break;
		case CExtDurationWnd::month:
			{
				if( nValue < 0 || nValue > 12 )
					nValue = nDigit;
				pII->m_nValue = nValue;
				if( nValue >= 0 && nValue <= 12 )
				{
					nMonth = nValue;
					if( nValue > 0 )
					{
						INT nDaysInMonth = 
							CExtDateTimeWnd::stat_GetDaysInMonth( 
								nYear,
								nMonth
								);
						if( nDay > nDaysInMonth )
							nDay = nDaysInMonth;
					}
				}
				m_bBlankMonth = false;
			}
			break;
		case CExtDurationWnd::year:
			{
				if(		nValue < 0 
					||	nValue > dtRangeMax.GetYear() 
					)
					nValue = nDigit;
				pII->m_nValue = nValue;
				if(		nValue >= dtRangeMin.GetYear() 
					&&	nValue <= dtRangeMax.GetYear() 
					||	nValue == 0
					)
				{
					nYear = nValue;
					if( nValue > 0 )
					{
						INT nDaysInMonth = 
							CExtDateTimeWnd::stat_GetDaysInMonth( 
								nYear,
								nMonth
								);
						if( nDay > nDaysInMonth )
							nDay = nDaysInMonth;
					}
				}
				m_bBlankYear = false;
			}
			break;
		case CExtDurationWnd::hour:
			{
				bool b24Hours = OnQueryTimeFormat24Hours();
				if( nValue < 0 || nValue > 23 )
					nValue = nDigit;
				pII->m_nValue = nValue;
				if( !b24Hours )
				{
					if( nHour >= 12 && nValue < 12 )
						nValue += 12;
					if( pII->m_nValue > 12 )
						pII->m_nValue -= 12;
				}
				ASSERT( pII->m_nValue >= 0 
				&&	(	(pII->m_nValue <= 23 && b24Hours) 
					||	(pII->m_nValue <= 12 && !b24Hours) )
				);
				ASSERT( nValue >= 0 && nValue <= 23 );
				nHour = nValue;
				m_bBlankHour = false;
			}
			break;
		case CExtDurationWnd::minute:
			if(	nValue < 0 || nValue > 59 )
				nValue = nDigit;
			nMinute = nValue;
			m_bBlankMinute = false;
			break;
		case CExtDurationWnd::second:
			if(	nValue < 0 || nValue > 59 )
				nValue = nDigit;
			nSecond = nValue;
			m_bBlankSecond = false;
			break;
		case CExtDurationWnd::designator:
			break;
		default:
			ASSERT( FALSE );
		}

		bool bSelectNextItem = false;

		if(		m_bAutoSelectNext
			&&	m_pLastInputItem == pII 
			)
		{
			if( pII->m_eItemType == CExtDurationWnd::year )
			{
				// 0 - Two digit, 1 - Full century
				CExtSafeString s = OnQueryYearFormat();
				CString sYear;
				sYear.Format( _T("%d"), nValue );
				INT nYearLength = sYear.GetLength();
				if(		( _tcscmp( s, _T("0") ) == 0 && nYearLength >= 2 ) 
					||	( _tcscmp( s, _T("1") ) == 0 && nYearLength >= 4 ) 
					)
					bSelectNextItem = true;
			}
			else
				bSelectNextItem = true;
		}

		m_pLastInputItem = pII;

		COleDateTime dtTest( 
			nYear,
			nMonth,
			nDay,
			nHour,
			nMinute,
			nSecond 
			);
		if(	dtTest.GetStatus() == COleDateTime::valid &&
				(	(	nYear	!= m_dtDate.GetYear() 
					&&	nYear >= dtRangeMin.GetYear() 
					&&	nYear <= dtRangeMax.GetYear() 
					)
				||	nMonth	!= m_dtDate.GetMonth()
				||	nDay	!= m_dtDate.GetDay()
				||	nHour	!= m_dtDate.GetHour()
				||	nMinute != m_dtDate.GetMinute()
				||	nSecond != m_dtDate.GetSecond()
				)
			)
			SetDateTime(
				nYear,
				nMonth,
				nDay,
				nHour,
				nMinute,
				nSecond,
				false, false
				);

		if( bSelectNextItem )
			OnAutoSelectNextItem( pII );
		
		UpdateDurationWnd( true, true );
	}
}

void CExtDateTimeWnd::SetAutoSelectNext( 
	bool bSet // = true
	)
{
	ASSERT_VALID( this );
	m_bAutoSelectNext = bSet;
}

bool CExtDateTimeWnd::GetAutoSelectNext() const
{
	ASSERT_VALID( this );
	return m_bAutoSelectNext;
}

bool CExtDateTimeWnd::ShowDropDown()
{
	ASSERT_VALID( this );
	return OnShowDropDownMenu();
}

void CExtDateTimeWnd::SetRange(
	const COleDateTime * pdtRangeMin,
	const COleDateTime * pdtRangeMax 
	)
{
	ASSERT_VALID( this );

	m_dtRangeMin.SetStatus( COleDateTime::null );
	m_dtRangeMax.SetStatus( COleDateTime::null );
	
	if( pdtRangeMin != NULL )
		m_dtRangeMin = *pdtRangeMin;
	if( pdtRangeMax != NULL )
		m_dtRangeMax = *pdtRangeMax;
}

void CExtDateTimeWnd::SetRange(
	const COleDateTime & dtRangeMin,
	const COleDateTime & dtRangeMax 
	)
{
	ASSERT_VALID( this );
	SetRange(
		&dtRangeMin,
		&dtRangeMax 
		);
}

void CExtDateTimeWnd::SetRange(
	const CTime * pdtRangeMin,
	const CTime * pdtRangeMax 
	)
{
	ASSERT_VALID( this );

	m_dtRangeMin.SetStatus( COleDateTime::null );
	m_dtRangeMax.SetStatus( COleDateTime::null );
	
	if( pdtRangeMin != NULL )
		m_dtRangeMin = pdtRangeMin->GetTime();
	if( pdtRangeMax != NULL )
		m_dtRangeMax = pdtRangeMax->GetTime();
}

void CExtDateTimeWnd::SetRange(
	const CTime & dtRangeMin,
	const CTime & dtRangeMax 
	)
{
	ASSERT_VALID( this );
	SetRange(
		&dtRangeMin,
		&dtRangeMax 
		);
}

void CExtDateTimeWnd::GetRange(
	COleDateTime * pdtRangeMin,
	COleDateTime * pdtRangeMax 
	) const
{
	ASSERT_VALID( this );
	if( pdtRangeMin != NULL )
		(*pdtRangeMin) = m_dtRangeMin;
	if( pdtRangeMax != NULL )
		(*pdtRangeMax) = m_dtRangeMax;
}

void CExtDateTimeWnd::GetRange(
	CTime * pdtRangeMin,
	CTime * pdtRangeMax 
	) const
{
	ASSERT_VALID( this );
SYSTEMTIME sys_time;
	::memset( &sys_time, 0, sizeof(SYSTEMTIME) );
	if( pdtRangeMin != NULL )
	{
		m_dtRangeMin.GetAsSystemTime( sys_time );
		(*pdtRangeMin) = sys_time;
	}
	if( pdtRangeMax != NULL )
	{
		m_dtRangeMax.GetAsSystemTime( sys_time );
		(*pdtRangeMax) = sys_time;
	}
}

void CExtDateTimeWnd::OnQueryRange(
	COleDateTime * pdtRangeMin,
	COleDateTime * pdtRangeMax 
	) const
{
	ASSERT_VALID( this );
	if( pdtRangeMin != NULL )
	{
		if( m_dtRangeMin.GetStatus() == COleDateTime::valid )
			(*pdtRangeMin) = m_dtRangeMin;
		else
			(*pdtRangeMin) = COleDateTime( __EXT_DATE_YEAR_MIN, 1, 1, 0, 0, 0 );
	}
	if( pdtRangeMax != NULL )
	{
		if( m_dtRangeMax.GetStatus() == COleDateTime::valid )
			(*pdtRangeMax) = m_dtRangeMax;
		else
			(*pdtRangeMax) = COleDateTime( __EXT_DATE_YEAR_MAX, 12, 31, 0, 0, 0 );
	}
}

#endif // (!defined __EXT_MFC_NO_DATETIMEWND)

#endif // (!defined __EXT_MFC_NO_DURATIONWND)
