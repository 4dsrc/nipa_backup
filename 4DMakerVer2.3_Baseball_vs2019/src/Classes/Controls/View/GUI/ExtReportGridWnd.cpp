// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#include "stdafx.h"

#if (!defined __EXT_REPORT_GRID_WND_H)
	#include <ExtReportGridWnd.h>
#endif

#if (!defined __EXT_MEMORY_DC_H)
	#include <../Src/ExtMemoryDC.h>
#endif

#if (!defined __EXT_PAINT_MANAGER_H)
	#include <ExtPaintManager.h>
#endif

#if (!defined __AFXPRIV_H__)
	#include <AfxPriv.h>
#endif 

#include <Resources/Resource.h>

#if (!defined __EXT_MFC_NO_REPORTGRIDWND)
	IMPLEMENT_SERIAL( CExtReportGridColumn, CExtGridCellHeader, VERSIONABLE_SCHEMA|1 );
	IMPLEMENT_SERIAL( CExtReportGridItem, CExtTreeGridCellNode, VERSIONABLE_SCHEMA|1 );
#endif // (!defined __EXT_MFC_NO_REPORTGRIDWND)

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if (!defined __EXT_MFC_NO_REPORTGRIDWND)

/////////////////////////////////////////////////////////////////////////////
// CExtReportGridCategoryComboBox window

IMPLEMENT_DYNCREATE( CExtReportGridCategoryComboBox, CExtComboBoxBase );

CExtReportGridCategoryComboBox::CExtReportGridCategoryComboBox(
	CExtReportGridWnd * pRGW // = NULL
	)
	: m_pRGW( pRGW )
{
#ifdef _DEBUG
	if( m_pRGW != NULL )
	{
		ASSERT_VALID( m_pRGW );
	}
#endif // _DEBUG
}

CExtReportGridCategoryComboBox::~CExtReportGridCategoryComboBox()
{
}

#ifdef _DEBUG

void CExtReportGridCategoryComboBox::AssertValid() const
{
	CExtComboBoxBase::AssertValid();
	if( m_pRGW != NULL )
	{
		ASSERT_VALID( m_pRGW );
	}
}

void CExtReportGridCategoryComboBox::Dump( CDumpContext & dc ) const
{
	CExtComboBoxBase::Dump( dc );
}

#endif

BEGIN_MESSAGE_MAP( CExtReportGridCategoryComboBox, CExtComboBoxBase )
    //{{AFX_MSG_MAP(CExtReportGridCategoryComboBox)
	//}}AFX_MSG_MAP
	ON_CONTROL_REFLECT( CBN_SELENDOK, OnSelEndOK )
	ON_MESSAGE( WM_SIZEPARENT, OnSizeParent )
END_MESSAGE_MAP()

void CExtReportGridCategoryComboBox::OnSynchronizeCurSel()
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	if( m_pRGW->GetSafeHwnd() == NULL )
		return;
	ASSERT_VALID( m_pRGW );
CExtReportGridColumnChooserWnd * pCCW =
		m_pRGW->ReportColumnChooserGet();
	if( pCCW->GetSafeHwnd() == NULL )
		return;
	ASSERT_VALID( pCCW );
CString strCategoryName;
int nCurSel = -1;
	if(		GetCount() > 0
		&&	( nCurSel = GetCurSel() ) >= 0
		)
	{
		GetLBText( nCurSel, strCategoryName );
		pCCW->SetContentFromCategory(
			strCategoryName.IsEmpty()
				? _T("")
				: LPCTSTR(strCategoryName)
			);
	}
	else
		pCCW->SetContentFromCategory();
}

void CExtReportGridCategoryComboBox::OnSelEndOK()
{
	ASSERT_VALID( this );
	Default();
	OnSynchronizeCurSel();
}

LRESULT CExtReportGridCategoryComboBox::OnSizeParent( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	wParam;
AFX_SIZEPARENTPARAMS * lpLayout =
		(AFX_SIZEPARENTPARAMS *) lParam;
	ASSERT( lpLayout != NULL );
CRect rcFrameRest = &lpLayout->rect;
//	if(		rcFrameRest.left >= rcFrameRest.right
//		||	rcFrameRest.top >= rcFrameRest.bottom
//		)
//	{
//		if( lpLayout->hDWP == NULL )
//			return 0;
//		::SetWindowPos(
//			m_hWnd,
//			NULL, 0, 0, 0, 0,
//			SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOOWNERZORDER
//				|SWP_HIDEWINDOW
//			);
//		return 0;
//	}
DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle&WS_VISIBLE) == 0 )
		return 0;
CRect rcWnd;
	GetWindowRect( &rcWnd );
int nHeight = rcWnd.Height();
CRect rcOwnLayout( rcFrameRest );
	lpLayout->rect.top += nHeight;
	rcOwnLayout.bottom = rcOwnLayout.top + nHeight;
	lpLayout->sizeTotal.cy += nHeight;
//	ASSERT( ! rcOwnLayout.IsRectEmpty() );
	if( lpLayout->hDWP != NULL )
	{
		::AfxRepositionWindow(
			lpLayout,
			m_hWnd,
			&rcOwnLayout
			);
	} // if( lpLayout->hDWP != NULL )
	return 0;
}

bool CExtReportGridCategoryComboBox::Create(
	CWnd * pWndParent, 
	bool bVisible // = true
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() == NULL );
	ASSERT_VALID( pWndParent );
	ASSERT( pWndParent->GetSafeHwnd() != NULL );
CRect rc( 0, 0, 0, 400 );
	if(	! CExtComboBoxBase::Create(
			WS_CHILD
				| ( bVisible ? WS_VISIBLE : 0 )
				|WS_TABSTOP
				|WS_CLIPCHILDREN|WS_CLIPSIBLINGS
				|CBS_DROPDOWNLIST
				|CBS_HASSTRINGS
				|CBS_SORT,
			rc,
			pWndParent,
			0x101
			)
		)
	{
		ASSERT( FALSE );
		return false;
	}
	SetFont(
		CFont::FromHandle(
			(HFONT)::GetStockObject(DEFAULT_GUI_FONT)
			)
		);
	::SetWindowPos(
		m_hWnd, HWND_TOP, 0, 0, 0, 0,
		SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE
		);
	return true;
}

void CExtReportGridCategoryComboBox::PostNcDestroy()
{
	delete this;
}

/////////////////////////////////////////////////////////////////////////////
// CExtReportGridColumnChooserWnd

IMPLEMENT_DYNAMIC( CExtReportGridColumnChooserMiniFrameWnd, CMiniFrameWnd );		

CExtReportGridColumnChooserMiniFrameWnd::CExtReportGridColumnChooserMiniFrameWnd(
	CExtReportGridWnd & _RGW
	)
	: m_RGW( _RGW )
{
	ASSERT_VALID( (&m_RGW) );
}

CExtReportGridColumnChooserMiniFrameWnd::~CExtReportGridColumnChooserMiniFrameWnd()
{
}

CExtReportGridWnd & CExtReportGridColumnChooserMiniFrameWnd::GetReportGridWnd()
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&m_RGW) );
	return m_RGW;
}

const CExtReportGridWnd & CExtReportGridColumnChooserMiniFrameWnd::GetReportGridWnd() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&m_RGW) );
	return m_RGW;
}

bool CExtReportGridColumnChooserMiniFrameWnd::Create()
{
	ASSERT_VALID( this );
	ASSERT( m_hWnd == NULL );
CExtReportGridWnd & _RGW = GetReportGridWnd();
	if( _RGW.GetSafeHwnd() == NULL )
		return false;
CExtReportGridColumnChooserWnd * pCCW =
		_RGW.ReportColumnChooserGet();
	if( pCCW == NULL )
		return false;
	ASSERT_VALID( pCCW );
	if( pCCW->GetSafeHwnd() == NULL )
		return false;
WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo(
			hInst,
			__EXT_COLUMN_CHOOSER_MINI_FRAME_CLASS_NAME,
			&_wndClassInfo
			)
		)
	{
		_wndClassInfo.style =
			CS_GLOBALCLASS
			|CS_DBLCLKS
			|CS_SAVEBITS
			|CS_HREDRAW
			|CS_VREDRAW
			;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor = ::LoadCursor( NULL, IDC_ARROW ) ;
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_COLUMN_CHOOSER_MINI_FRAME_CLASS_NAME;
		if( !::AfxRegisterClass( &_wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
CExtSafeString strCaption;
	_RGW.OnReportGridColumnChooserQueryCaption( strCaption );
	if( ! CWnd::CreateEx(
			0,
			__EXT_COLUMN_CHOOSER_MINI_FRAME_CLASS_NAME,
			strCaption.IsEmpty() ? _T("") : LPCTSTR(strCaption),
			WS_POPUP
				| ( _RGW.ReportColumnChooserIsVisible() ? WS_VISIBLE : 0 )
				|WS_CLIPSIBLINGS|WS_CLIPCHILDREN
				|WS_SYSMENU
				|WS_BORDER|WS_THICKFRAME|WS_CAPTION
				,
			200, 200, 200, 300,
			_RGW.GetSafeHwnd(),
			(HMENU)NULL,
			(LPVOID)NULL
			)
		)
		return false;
CMenu * pSysMenu = GetSystemMenu( FALSE );
	if( pSysMenu->GetSafeHmenu() != NULL )
	{
		UINT nIndex, nCount = pSysMenu->GetMenuItemCount();
		for( nIndex = 0; nIndex < nCount; )
		{
			UINT nID = pSysMenu->GetMenuItemID( int(nIndex) );
			if(		nID == SC_MOVE
				||	nID == SC_SIZE
				||	nID == SC_CLOSE
				)
			{
				nIndex ++;
				continue;
			}
			pSysMenu->DeleteMenu( nIndex, MF_BYPOSITION );
			nCount --;
		}
	} // if( pSysMenu->GetSafeHmenu() != NULL )
	pCCW->SetParent( this );
	RecalcLayout();
	_RGW.OnSwRecalcLayout( true );
	return true;
}

BEGIN_MESSAGE_MAP( CExtReportGridColumnChooserMiniFrameWnd, CMiniFrameWnd )
	//{{AFX_MSG_MAP(CExtReportGridColumnChooserMiniFrameWnd)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	__EXT_MFC_ON_WM_NCHITTEST()
END_MESSAGE_MAP()

void CExtReportGridColumnChooserMiniFrameWnd::PreSubclassWindow() 
{
	CMiniFrameWnd::PreSubclassWindow();
}

void CExtReportGridColumnChooserMiniFrameWnd::PostNcDestroy() 
{
	CMiniFrameWnd::PostNcDestroy();
}

void CExtReportGridColumnChooserMiniFrameWnd::RecalcLayout(
	BOOL bNotify // = TRUE
	)
{
CExtReportGridWnd & _RGW = GetReportGridWnd();
	if( _RGW.GetSafeHwnd() == NULL )
	{
		CMiniFrameWnd::RecalcLayout( bNotify );
		return;
	}
CExtReportGridColumnChooserWnd * pCCW =
		_RGW.ReportColumnChooserGet();
	if( pCCW == NULL )
	{
		CMiniFrameWnd::RecalcLayout( bNotify );
		return;
	}
	ASSERT_VALID( pCCW );
	if( pCCW->GetSafeHwnd() == NULL )
	{
		CMiniFrameWnd::RecalcLayout( bNotify );
		return;
	}
CFrameWnd * pLocationFrame = pCCW->GetParentFrame();
	if( pLocationFrame != this )
	{
		CMiniFrameWnd::RecalcLayout( bNotify );
		return;
	}
UINT nDlgCtrlID = UINT( pCCW->GetDlgCtrlID() );
	if( nDlgCtrlID != AFX_IDW_PANE_FIRST )
		pCCW->SetDlgCtrlID( AFX_IDW_PANE_FIRST );
	CMiniFrameWnd::RecalcLayout( bNotify );
	if( nDlgCtrlID != AFX_IDW_PANE_FIRST )
		pCCW->SetDlgCtrlID( int(nDlgCtrlID) );
}

void CExtReportGridColumnChooserMiniFrameWnd::OnClose() 
{
	ASSERT_VALID( this );
CExtReportGridWnd & _RGW = GetReportGridWnd();
	_RGW.ReportColumnChooserShow( false );
}

UINT CExtReportGridColumnChooserMiniFrameWnd::OnNcHitTest(CPoint point)
{
	return UINT( CWnd::OnNcHitTest( point ) );
}

/////////////////////////////////////////////////////////////////////////////
// CExtReportGridColumnChooserWnd

IMPLEMENT_DYNAMIC( CExtReportGridColumnChooserWnd, CExtGridWnd );		

CExtReportGridColumnChooserWnd::CExtReportGridColumnChooserWnd(
	CExtReportGridWnd & _RGW
	)
	: m_RGW( _RGW )
	, m_rcExtents( 150, 150, 150, 150 )
	, m_nOrientation( AFX_IDW_DOCKBAR_FLOAT )
{
	ASSERT_VALID( (&m_RGW) );
}

CExtReportGridColumnChooserWnd::~CExtReportGridColumnChooserWnd()
{
}

CExtReportGridWnd & CExtReportGridColumnChooserWnd::GetReportGridWnd()
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&m_RGW) );
	return m_RGW;
}

const CExtReportGridWnd & CExtReportGridColumnChooserWnd::GetReportGridWnd() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&m_RGW) );
	return m_RGW;
}

CRect CExtReportGridColumnChooserWnd::ExtentsGet() const
{
	ASSERT_VALID( this );
	return m_rcExtents;
}

void CExtReportGridColumnChooserWnd::ExtentsSet(
	const RECT & rcExtents
	)
{
	ASSERT_VALID( this );
	m_rcExtents = rcExtents;
	if( m_rcExtents.left < 1 )
		m_rcExtents.left = 1;
	if( m_rcExtents.top < 1 )
		m_rcExtents.top = 1;
	if( m_rcExtents.right < 1 )
		m_rcExtents.right = 1;
	if( m_rcExtents.bottom < 1 )
		m_rcExtents.bottom = 1;
}

UINT CExtReportGridColumnChooserWnd::OrientationGet() const
{
	ASSERT_VALID( this );
	ASSERT(
			m_nOrientation == AFX_IDW_DOCKBAR_FLOAT
		||	m_nOrientation == AFX_IDW_DOCKBAR_LEFT
		||	m_nOrientation == AFX_IDW_DOCKBAR_RIGHT
		||	m_nOrientation == AFX_IDW_DOCKBAR_TOP
		||	m_nOrientation == AFX_IDW_DOCKBAR_BOTTOM
		);
	return m_nOrientation;
}

void CExtReportGridColumnChooserWnd::OrientationSet(
	UINT nOrientation
	)
{
	ASSERT_VALID( this );
	ASSERT(
			nOrientation == AFX_IDW_DOCKBAR_FLOAT
		||	nOrientation == AFX_IDW_DOCKBAR_LEFT
		||	nOrientation == AFX_IDW_DOCKBAR_RIGHT
		||	nOrientation == AFX_IDW_DOCKBAR_TOP
		||	nOrientation == AFX_IDW_DOCKBAR_BOTTOM
		);
	if( m_nOrientation == nOrientation )
		return;
	m_nOrientation = nOrientation;
	m_RGW.OnReportGridColumnChooserAdjustLocation();
}

void CExtReportGridColumnChooserWnd::SetContentFromCategory(
	__EXT_MFC_SAFE_LPCTSTR strCategoryName // = __EXT_MFC_SAFE_LPCTSTR(NULL) // NULL - empty content
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	RowRemoveAll( false );
	if( strCategoryName != NULL )
	{
		CTypedPtrArray < CPtrArray, CExtReportGridColumn * > arrCategoryColumns;
		m_RGW.ReportColumnGetContent(
			strCategoryName,
			arrCategoryColumns,
			false,
			true
			);
		LONG nColNo, nColCount = LONG( arrCategoryColumns.GetSize() );
		RowAdd( nColCount, false );
		for( nColNo = 0; nColNo < nColCount; nColNo ++ )
		{
			CExtReportGridColumn * pRGC =
				arrCategoryColumns[ nColNo ];
			ASSERT_VALID( pRGC );
			ASSERT( ! pRGC->ColumnIsActive() );
			CExtGridCell * pCell =
				GridCellGet(
					0,
					nColNo,
					0,
					0,
					RUNTIME_CLASS(CExtReportGridColumn)
					);
			ASSERT_VALID( pCell );
			pCell->Assign( *pRGC );
			pCell->ModifyStyle(
				0,
				__EGCS_SORT_ARROW // no sort arrow in column chooser
				);
		} // for( nColNo = 0; nColNo < nColCount; nColNo ++ )
	} // if( strCategoryName != NULL )
CExtGridDataSortOrder::ITEM_INFO _soii( 0 );
CExtGridDataSortOrder _gdso;
	_gdso.m_arrItems.Add( _soii );
	GridSortOrderSetup( false, _gdso, false, false, false );
CExtGridDataProvider & _DataProvider = OnGridQueryDataProvider();
	VERIFY( _DataProvider.SortOrderUpdate( false, this ) );
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

INT CExtReportGridColumnChooserWnd::OnSiwQueryItemExtentH(
	LONG nColNo,
	INT * p_nExtraSpaceBefore, // = NULL
	INT * p_nExtraSpaceAfter // = NULL
	) const
{
	ASSERT_VALID( this );
	nColNo;
	if( p_nExtraSpaceBefore != NULL )
		(*p_nExtraSpaceBefore) = 0;
	if( p_nExtraSpaceAfter != NULL )
		(*p_nExtraSpaceAfter) = 0;
CRect rcClient = OnSwGetClientRect();
	return rcClient.Width();
}

bool CExtReportGridColumnChooserWnd::GridCellRectsGet(
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	RECT * pRectCell,
	RECT * pRectCellExtra, // = NULL
	RECT * pRectText, // = NULL
	RECT * pRectTextArea, // = NULL
	RECT * pRectIcon, // = NULL
	RECT * pRectCheck, // = NULL
	RECT * pRectButtonEllipsis, // = NULL
	RECT * pRectButtonDropdown, // = NULL
	RECT * pRectButtonUp, // = NULL
	RECT * pRectButtonDown, // = NULL
	RECT * pRectFocusArrow, // = NULL
	RECT * pRectSortArrow // = NULL
	) const
{
	ASSERT_VALID( this );
	if( ! CExtGridWnd::GridCellRectsGet(
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			pRectCell,
			pRectCellExtra,
			pRectText,
			pRectTextArea,
			pRectIcon,
			pRectCheck,
			pRectButtonEllipsis,
			pRectButtonDropdown,
			pRectButtonUp,
			pRectButtonDown,
			pRectFocusArrow,
			pRectSortArrow
			)
		)
		return false;
	if( pRectCell == NULL && pRectCellExtra == NULL )
		return true;
CRect rcClient = OnSwGetClientRect();
	::SetRect(
		pRectCell,
		rcClient.left,
		pRectCell->top,
		rcClient.right,
		pRectCell->bottom
		);
	::SetRect(
		pRectCellExtra,
		rcClient.left,
		pRectCellExtra->top,
		rcClient.right,
		pRectCellExtra->bottom
		);
	return true;
}

bool CExtReportGridColumnChooserWnd::OnGbwDataDndIsAllowed() const
{
	ASSERT_VALID( this );
	return true;
}

CPoint CExtReportGridColumnChooserWnd::OnGbwDataDndGetStartOffset() const
{
	ASSERT_VALID( this );
	return CExtGridWnd::OnGbwDataDndGetStartOffset();
}

bool CExtReportGridColumnChooserWnd::OnGbwDataDndCanStart(
	const CExtGridHitTestInfo & htInfo
	)
{
	ASSERT_VALID( this );
	if(		htInfo.IsHoverEmpty()
		||	htInfo.GetInnerOuterTypeOfColumn() != 0
		||	htInfo.GetInnerOuterTypeOfRow() != 0
		||	htInfo.m_nColNo != 0
		)
		return false;
	m_htInfoDataDnd = htInfo;
	return true;
}

void CExtReportGridColumnChooserWnd::OnGbwDataDndDo(
	const CExtGridHitTestInfo & htInfo
	)
{
	ASSERT_VALID( this );
	if(		htInfo.GetInnerOuterTypeOfColumn() != 0
		||	htInfo.GetInnerOuterTypeOfRow() != 0
		||	htInfo.m_nColNo != 0
		)
		return;
	if( m_RGW.GetSafeHwnd() == NULL )
		return;
static CExtGridCell * g_pTrackCell = NULL;
	if( g_pTrackCell != NULL )
		return;
CExtGridCell * pCellHT = GridCellGet( htInfo );
	if( pCellHT == NULL )
		return;
	ASSERT_VALID( pCellHT );
CExtReportGridColumn * pRGC =
		DYNAMIC_DOWNCAST( CExtReportGridColumn, pCellHT );
	if( pRGC == NULL )
		return;
	if( ! pRGC->DragDropEnabledGet() )
		return;
__EXT_MFC_SAFE_LPCTSTR strColumnName = pRGC->ColumnNameGet();
__EXT_MFC_SAFE_LPCTSTR strCategoryName = pRGC->CategoryNameGet();
CExtReportGridColumn * pRealRGC =
		m_RGW.ReportColumnGet(
			strColumnName,
			strCategoryName
			);
	if( pRealRGC == NULL )
		return;
CExtReportGridGroupAreaWnd * pGAW = NULL;
	if( m_RGW.ReportGroupAreaIsVisible() )
	{
		pGAW = m_RGW.ReportGroupAreaGet();
		if( pGAW != NULL )
		{
			ASSERT_VALID( pGAW );
			if( pGAW->GetSafeHwnd() == NULL )
				pGAW = NULL;
		} // if( pGAW != NULL )
	} // if( m_RGW.ReportGroupAreaIsVisible() )
//bool bCenteredDndAlignment = m_RGW.OnGridQueryCenteredDndAlignment();
CRect rcDND( htInfo.m_rcItem );
INT nRealExtent = 0;
	pRealRGC->ExtentGet( nRealExtent, 0 );
CClientDC dcMeasure( &m_RGW );
CSize _sizeMeasure = 
		pRealRGC->MeasureCell( 
			&m_RGW,
			dcMeasure,
			htInfo.m_nVisibleColNo,
			htInfo.m_nVisibleRowNo,
			htInfo.m_nColNo,
			htInfo.m_nRowNo,
			0,
			0
			);
	nRealExtent = max( nRealExtent, _sizeMeasure.cx );
	if( nRealExtent > 0 )
	{
		CPoint pt = htInfo.m_ptClient;
//		if( ! bCenteredDndAlignment )
			rcDND.left = pt.x - nRealExtent / 2;
		rcDND.right = rcDND.left + nRealExtent;
	} // if( nRealExtent > 0 )
//	if( bCenteredDndAlignment )
//		rcDND.OffsetRect(
//			htInfo.m_ptClient.x - htInfo.m_rcItem.left - htInfo.m_rcItem.Width()/2,
//			htInfo.m_ptClient.y - htInfo.m_rcItem.bottom + 4
//			);
	m_htInfoCellPressing = htInfo;
CExtContentExpandWnd wndDND, wndArrows;
	if( ! wndDND.Activate(
			rcDND,
			this,
			__ECWAF_DEF_EXPANDED_ITEM_PAINTER
				|__ECWAF_NO_CAPTURE
				|__ECWAF_REDIRECT_MOUSE
				|__ECWAF_REDIRECT_NO_DEACTIVATE
				|__ECWAF_REDIRECT_AND_HANDLE
				|__ECWAF_HANDLE_MOUSE_ACTIVATE
				|__ECWAF_MA_NOACTIVATE
			)
		)
	{
		ASSERT( FALSE );
		return;
	}
CExtGridHitTestInfo htInfoDrop;
CPoint ptShiftLast( 0, 0 );
CRect rcInitialWndDND;
	wndDND.GetWindowRect( &rcInitialWndDND );
CRect rcVisibleRange = m_RGW.OnSiwGetVisibleRange();
HWND hWndGrid = m_hWnd;
	g_pTrackCell = pRGC;
	CExtMouseCaptureSink::SetCapture( hWndGrid );
bool bStopFlag = false;
bool bEventDropIn = false, bEventDropOut = false;
LONG nGroupAreaDropHT = -1L;
	ASSERT( m_RGW.m_hCursorOuterDragOK != NULL );
	ASSERT( m_RGW.m_hCursorOuterDragCancel != NULL );
	::SetCursor( m_RGW.m_hCursorOuterDragOK );
	for(	MSG msg;
				::IsWindow( hWndGrid )
			&&	(!bStopFlag)
			&&	(g_pTrackCell != NULL)
			;
		)
	{ // main message loop
		if( ! ::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
		{
			if(		( ! ::IsWindow( hWndGrid ) )
				||	bStopFlag
				||	g_pTrackCell == NULL
				)
				break;
			if( CExtGridWnd::g_bEnableOnIdleCalls )
			{
				for(	LONG nIdleCounter = 0L;
						::AfxGetThread()->OnIdle(nIdleCounter);
						nIdleCounter ++
						);
			}
			::WaitMessage();
			continue;
		} // if( ! ::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
		switch( msg.message )
		{
		case WM_KILLFOCUS:
			if( msg.hwnd == hWndGrid )
				bStopFlag = true;
		break;
		case WM_CANCELMODE:
		case WM_ACTIVATEAPP:
		case WM_SYSCOMMAND:
		case WM_SETTINGCHANGE:
		case WM_SYSCOLORCHANGE:
			bStopFlag = true;
		break;
		case WM_COMMAND:
			if(		(HIWORD(msg.wParam)) == 0
				||	(HIWORD(msg.wParam)) == 1
				)
				bStopFlag = true;
		break;
		case WM_CAPTURECHANGED:
			if( (HWND)msg.wParam != hWndGrid )
				bStopFlag = true;
		break;
		case WM_MOUSEWHEEL:
			if( msg.hwnd != hWndGrid )
				bStopFlag = true;
			else
			{
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
				continue;
			} // else from if( msg.hwnd != hWndGrid )
		break;
		case WM_MOUSEMOVE:
			if( msg.hwnd != hWndGrid )
				bStopFlag = true;
			else
			{
				ASSERT_VALID( this );
				ASSERT_VALID( g_pTrackCell );
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
				CPoint point;
				point = DWORD( msg.lParam );
				CPoint ptShift = point - htInfo.m_ptClient;
				if( ptShift != ptShiftLast )
				{
					CExtGridHitTestInfo htInfoDropNew;
					CPoint ptRGW = point;
					ClientToScreen( &ptRGW );
					m_RGW.ScreenToClient( &ptRGW );
					CExtGridHitTestInfo htInfoRGW = htInfo;
					htInfoRGW.m_dwAreaFlags = __EGBWA_OUTER_TOP|__EGBWA_OUTER_CELLS;
					htInfoRGW.m_nRowNo = 0;
					htInfoRGW.m_nColNo = 32767;
					m_RGW.OnGridCalcOuterDropTarget(
						htInfoRGW, // htInfo
						htInfoDropNew,
						ptRGW // point
						);
					bool bSelfCancelDropEffect = false;
					if( ! htInfoDropNew.IsHoverEmpty() )
					{
						m_RGW.ClientToScreen( & htInfoDropNew.m_rcItem );
						m_RGW.ClientToScreen( & htInfoDropNew.m_rcExtra );
						m_RGW.ClientToScreen( & htInfoDropNew.m_rcPart );
						ScreenToClient( & htInfoDropNew.m_rcItem );
						ScreenToClient( & htInfoDropNew.m_rcExtra );
						ScreenToClient( & htInfoDropNew.m_rcPart );
						if(		(! htInfoDropNew.IsHoverEqual(htInfoDrop) )
							||	wndArrows.GetSafeHwnd() == NULL
							)
						{
							bEventDropIn = false;
							bEventDropOut = true;
							htInfoDrop = htInfoDropNew;
							bool bOuterDropAfterState =
								( (htInfoDrop.m_dwAreaFlags&__EGBWA_OUTER_DROP_AFTER) != 0 )
									? true : false;
							if( ! bSelfCancelDropEffect )
							{
								nGroupAreaDropHT = -1L;
								bEventDropIn = true;
								bEventDropOut = false;
								CRect rcArrows(
									bOuterDropAfterState
										? htInfoDrop.m_rcItem.right
										: htInfoDrop.m_rcItem.left,
									htInfoDrop.m_rcItem.top,
									bOuterDropAfterState
										? htInfoDrop.m_rcItem.right
										: htInfoDrop.m_rcItem.left,
									htInfoDrop.m_rcItem.bottom
									);
								VERIFY(
									wndArrows.Activate(
										rcArrows,
										this,
										__ECWAF_DRAW_RED_ARROWS
											|__ECWAF_TRANSPARENT_ITEM
											|__ECWAF_NO_CAPTURE
											|__ECWAF_REDIRECT_MOUSE
											|__ECWAF_REDIRECT_NO_DEACTIVATE
											|__ECWAF_REDIRECT_AND_HANDLE
											|__ECWAF_HANDLE_MOUSE_ACTIVATE
											|__ECWAF_MA_NOACTIVATE
										)
									);
								if( wndArrows.GetSafeHwnd() != NULL )
									wndArrows.SetWindowPos(
										&wndDND, 0, 0, 0, 0,
										SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE
										);
							} // if( ! bSelfCancelDropEffect )
							if( bEventDropIn )
							{
								nGroupAreaDropHT = -1L;
								bEventDropOut = false;
							} // if( bEventDropIn )
						} // if( ! htInfoDropNew.IsHoverEqual(htInfoDrop) ...
					} // if( ! htInfoDropNew.IsHoverEmpty() )
					else
					{
						LONG nNewGroupAreaDropHT = -1L;
						if( pGAW != NULL )
						{
							CPoint ptGAW = point;
							ClientToScreen( &ptGAW );
							pGAW->ScreenToClient( &ptGAW );
							if( ( nNewGroupAreaDropHT = pGAW->ItemDropHitTest(ptGAW) ) >= 0 )
							{
								CPoint ptTop( 0, 0 );
								INT nHeight = 0;
								if(		nNewGroupAreaDropHT != nGroupAreaDropHT
									&&	pGAW->ItemDropMarkerGet(
											nNewGroupAreaDropHT,
											ptTop,
											nHeight
											)
									)
								{
									nGroupAreaDropHT = nNewGroupAreaDropHT;
									CRect rcArrows(
										ptTop.x,
										ptTop.y,
										ptTop.x,
										ptTop.y + nHeight
										);
									pGAW->ClientToScreen( &rcArrows );
									ScreenToClient( &rcArrows );
									VERIFY(
										wndArrows.Activate(
											rcArrows,
											this,
											__ECWAF_DRAW_RED_ARROWS
												|__ECWAF_TRANSPARENT_ITEM
												|__ECWAF_NO_CAPTURE
												|__ECWAF_REDIRECT_MOUSE
												|__ECWAF_REDIRECT_NO_DEACTIVATE
												|__ECWAF_REDIRECT_AND_HANDLE
												|__ECWAF_HANDLE_MOUSE_ACTIVATE
												|__ECWAF_MA_NOACTIVATE
											)
										);
									if( wndArrows.GetSafeHwnd() != NULL )
										wndArrows.SetWindowPos(
											&wndDND, 0, 0, 0, 0,
											SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE
											);
									bEventDropIn = false;
									bEventDropOut = false;
									bSelfCancelDropEffect = false;
								} // if( nNewGroupAreaDropHT != nGroupAreaDropHT ...
							} // if( ( nNewGroupAreaDropHT = pGAW->ItemDropHitTest(ptGAW) ) >= 0 )
							else
								nGroupAreaDropHT = -1L;
						} // if( pGAW != NULL )
						if( nGroupAreaDropHT < 0 )
						{
							bEventDropIn = false;
							bEventDropOut = true;
							bSelfCancelDropEffect = true;
						} // if( nGroupAreaDropHT < 0 )
					} // else from if( ! htInfoDropNew.IsHoverEmpty() )
					if( bSelfCancelDropEffect )
						wndArrows.Deactivate();
					if( bEventDropOut )
					{
						//::SetCursor( m_RGW.m_hCursorOuterDragCancel );
						::SetCursor( ::LoadCursor(NULL,IDC_NO) );
					}
					else if( bEventDropIn || nGroupAreaDropHT >= 0 )
						::SetCursor( m_RGW.m_hCursorOuterDragOK );
					ptShiftLast = ptShift;
					CRect rcWnd( rcInitialWndDND );
					rcWnd.OffsetRect( ptShift );
					wndDND.MoveWindow( &rcWnd );
					wndDND.Invalidate();
					wndDND.UpdateWindow();
					if( wndArrows.GetSafeHwnd() != NULL )
						wndArrows.UpdateWindow();
					OnSwUpdateWindow();
					if( pGAW->GetSafeHwnd() != NULL )
						pGAW->UpdateWindow();
					m_RGW.OnSwUpdateWindow();
					CExtPaintManager::stat_PassPaintMessages();
				} // if( ptShift != ptShiftLast )
				continue;
			} // else from if( msg.hwnd != hWndGrid )
		break;
		case WM_LBUTTONUP:
		case WM_RBUTTONUP:
		case WM_MBUTTONUP:
			bStopFlag = true;
			if( msg.hwnd == hWndGrid )
			{
				ASSERT_VALID( this );
				ASSERT_VALID( g_pTrackCell );
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
			} // if( msg.hwnd == hWndGrid )
			break;
		case WM_LBUTTONDBLCLK:
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDBLCLK:
		case WM_RBUTTONDOWN:
		case WM_MBUTTONDBLCLK:
		case WM_MBUTTONDOWN:
		case WM_CONTEXTMENU:
		case WM_NCLBUTTONUP:
		case WM_NCLBUTTONDBLCLK:
		case WM_NCLBUTTONDOWN:
		case WM_NCRBUTTONUP:
		case WM_NCRBUTTONDBLCLK:
		case WM_NCRBUTTONDOWN:
		case WM_NCMBUTTONUP:
		case WM_NCMBUTTONDBLCLK:
		case WM_NCMBUTTONDOWN:
			bStopFlag = true;
		break;
		default:
			if(	   WM_KEYFIRST <= msg.message
				&& msg.message <= WM_KEYLAST
				)
			{
				if( msg.message == WM_KEYDOWN || msg.message == WM_KEYUP )
				{
					if( msg.wParam == VK_SHIFT || msg.wParam == VK_CONTROL )
					{
						::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
						continue;
					} // if( msg.wParam == VK_SHIFT || msg.wParam == VK_CONTROL )
				} // if( msg.message == WM_KEYDOWN || msg.message == WM_KEYUP )
				bStopFlag = true;
				bEventDropIn = false;
				bEventDropOut = false;
			}
		break;
		} // switch( msg.message )
		if( bStopFlag )
			break;
		if( ! ::AfxGetThread() -> PumpMessage() )
			break;
	} // message loop
	m_htInfoCellPressing.Empty();
	g_pTrackCell = NULL;
	_DoSetCursor();
	wndDND.Deactivate();
	wndArrows.Deactivate();
	if( ! ::IsWindow( hWndGrid ) )
		return;
	if( bEventDropIn )
	{
		ASSERT( ! htInfoDrop.IsHoverEmpty() );
		//m_RGW.OnGridOuterDragComplete( htInfo, htInfoDrop );
		bool bOuterDropAfterState =
			( (htInfoDrop.m_dwAreaFlags&__EGBWA_OUTER_DROP_AFTER) != 0 )
				? true : false;
		VERIFY(
			m_RGW.ReportColumnActivate(
				strColumnName,
				strCategoryName,
				true,
				htInfoDrop.m_nColNo + ( bOuterDropAfterState ? 1 : 0 ),
				true
				)
			);
	} // if( bEventDropIn )
	else if( nGroupAreaDropHT >= 0 )
	{
		bool bAscending = true;
		CExtReportGridSortOrder & _rgsoSrc =
			m_RGW.ReportSortOrderGet();
		CExtReportGridSortOrder _rgso = _rgsoSrc;
		LONG nColumnGroupCount = _rgso.ColumnGroupCountGet();
		ASSERT( nGroupAreaDropHT <= nColumnGroupCount );
		LONG nExistingIndex = _rgso.ColumnGetIndexOf( pRealRGC );
		if( nExistingIndex >= 0 )
		{
			if( nExistingIndex < nColumnGroupCount )
			{
				if( nGroupAreaDropHT > nExistingIndex )
					nGroupAreaDropHT --;
				nColumnGroupCount --;
				_rgso.ColumnGroupCountSet( nColumnGroupCount );
#ifdef _DEBUG
				CExtReportGridColumn * pExistingRGC =
					_rgso.ColumnGetAt( nExistingIndex );
				ASSERT_VALID( pExistingRGC );
				ASSERT( LPVOID(pRealRGC) == LPVOID(pRealRGC) );
#endif // _DEBUG
			} // if( nExistingIndex < nColumnGroupCount )
			VERIFY( _rgso.ColumnRemove( nExistingIndex, 1 ) );
			bAscending = pRealRGC->SortingAscendingGet();
		} // if( nExistingIndex >= 0 )
			bAscending = pRealRGC->SortingAscendingGet();
		_rgso.ColumnInsert(
			pRealRGC,
			nGroupAreaDropHT,
			bAscending
			);
		nColumnGroupCount ++;
		_rgso.ColumnGroupCountSet( nColumnGroupCount );
		m_RGW.ReportSortOrderSet( _rgso, true );
	} // else if( nGroupAreaDropHT >= 0 )
	else if( bEventDropOut )
	{
		//m_RGW.OnGridOuterDragOut( htInfo );
	} // else if( bEventDropOut )
	OnGbwHoverRecalc();
	OnSwDoRedraw();
}

void CExtReportGridColumnChooserWnd::OnSiwPaintForeground(
	CDC & dc,
	bool bFocusedControl
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	CExtGridWnd::OnSiwPaintForeground(
		dc,
		bFocusedControl
		);
	if( RowCountGet() == 0 )
	{
		CRect rcPaintEmptyDataMessage = OnSwGetClientRect();
		rcPaintEmptyDataMessage.DeflateRect( 5, 5 );
		if(		rcPaintEmptyDataMessage.left < rcPaintEmptyDataMessage.right
			&&	rcPaintEmptyDataMessage.top < rcPaintEmptyDataMessage.bottom
			)
		{ // if reasonable rect for painting text message
			CExtSafeString strEmptyDataMessage;
			m_RGW.OnReportGridColumnChooserNoFieldsAvailableLabel( strEmptyDataMessage );
			if( ! strEmptyDataMessage.IsEmpty() )
			{
				int nOldBkMode = dc.SetBkMode( TRANSPARENT );
				COLORREF clrOldTextColor = dc.SetTextColor( OnSiwGetSysColor( COLOR_3DSHADOW ) );
				CFont * pOldFont = dc.SelectObject( &(OnSiwGetDefaultFont()) );
				dc.DrawText(
					LPCTSTR(strEmptyDataMessage),
					strEmptyDataMessage.GetLength(),
					rcPaintEmptyDataMessage,
					DT_SINGLELINE|DT_CENTER|DT_VCENTER|DT_END_ELLIPSIS
					);
				dc.SelectObject( pOldFont );
				dc.SetTextColor( clrOldTextColor );
				dc.SetBkMode( nOldBkMode );
			} // if( ! strEmptyDataMessage.IsEmpty() )
		} // if reasonable rect for painting text message
	} // if( RowCountGet() == 0 ...
}

void CExtReportGridColumnChooserWnd::OnGbwEraseArea(
	CDC & dc,
	const RECT & rcArea,
	DWORD dwAreaFlags
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( dwAreaFlags == __EGBWA_INNER_CELLS )
	{
		dc.FillSolidRect( &rcArea, ::GetSysColor( COLOR_3DFACE ) );
		return;
	}
	CExtGridWnd::OnGbwEraseArea(
		dc,
		rcArea,
		dwAreaFlags
		);
}

void CExtReportGridColumnChooserWnd::OnGbwPaintCell(
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
// CRect rcClient = OnSwGetClientRect();
// CRect rcCellBk(
// 		rcClient.left,
// 		rcCell.top,
// 		rcClient.right,
// 		rcCell.bottom
// 		);
// 	dc.FillSolidRect(
// 		&rcCellBk,
// 		::GetSysColor( COLOR_3DFACE )
// 		);
// 	dc.Draw3dRect(
// 		&rcCellBk,
// 		::GetSysColor( COLOR_3DHIGHLIGHT ),
// 		::GetSysColor( COLOR_3DSHADOW )
// 		);
	CExtGridWnd::OnGbwPaintCell(
		dc,
		nVisibleColNo,
		nVisibleRowNo,
		nColNo,
		nRowNo,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);
}

bool CExtReportGridColumnChooserWnd::_CreateHelper()
{
	if( ! CExtGridWnd::_CreateHelper() )
		return false;
	SiwModifyStyle(
		__ESIS_STH_NONE
			|__ESIS_STV_PIXEL
			| __EGBS_SFB_NONE
			//|__EGBS_RESIZING_CELLS_OUTER
			//|__EGBS_MULTI_AREA_SELECTION|__EGBS_NO_HIDE_SELECTION
			//|__EGBS_GRIDLINES //|__EGBS_LBEXT_SELECTION
			//|__EGBS_SUBTRACT_SEL_AREAS
			|__EGBS_FIXED_SIZE_ROWS
		,
		0,
		false
		);
	SiwModifyStyleEx(
		__EGBS_EX_CELL_EXPANDING_INNER,
		0,
		false
		);
	BseModifyStyle(
		0,
		__EGWS_BSE_DEFAULT,
		false
		);
	BseModifyStyleEx(
		0,
		__EGWS_BSE_EX_DEFAULT,
		false
		);
	m_wndScrollBarV.m_bCompleteRepaint = false;
	m_wndScrollBarV.m_eSO = CExtScrollBar::__ESO_RIGHT;
	if( ! m_wndScrollBarV.Create(
			WS_CHILD|WS_VISIBLE|SBS_VERT|SBS_RIGHTALIGN,
			CRect(0,0,0,0),
			this,
			1
			)
		)
	{
		ASSERT( FALSE );
		return false;
	}
	ColumnAdd();
	return true;
}

CScrollBar* CExtReportGridColumnChooserWnd::GetScrollBarCtrl(int nBar) const
{
	ASSERT_VALID( this );
	if( m_hWnd == NULL || (! ::IsWindow(m_hWnd) ) )
		return NULL;
	ASSERT( nBar == SB_HORZ || nBar == SB_VERT );
	if( nBar == SB_VERT )
	{
		if( m_wndScrollBarV.GetSafeHwnd() != NULL )
			return ( const_cast < CExtScrollBar * > ( &m_wndScrollBarV ) );
	} // if( nBar == SB_VERT )
	return NULL;
}

BEGIN_MESSAGE_MAP( CExtReportGridColumnChooserWnd, CExtGridWnd )
	//{{AFX_MSG_MAP(CExtReportGridColumnChooserWnd)
	//}}AFX_MSG_MAP
	ON_MESSAGE( WM_SIZEPARENT, OnSizeParent )
END_MESSAGE_MAP()

void CExtReportGridColumnChooserWnd::PreSubclassWindow() 
{
	CExtGridWnd::PreSubclassWindow();
}

void CExtReportGridColumnChooserWnd::PostNcDestroy() 
{
	CExtGridWnd::PostNcDestroy();
	delete this;
}

LRESULT CExtReportGridColumnChooserWnd::OnSizeParent( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	wParam;
UINT nOrientation = OrientationGet();
	if( nOrientation == AFX_IDW_DOCKBAR_FLOAT )
		return 0;
DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle&WS_VISIBLE) == 0 )
		return 0;
AFX_SIZEPARENTPARAMS * lpLayout =
		(AFX_SIZEPARENTPARAMS *) lParam;
	ASSERT( lpLayout != NULL );
CRect rcOwnLayout = lpLayout->rect;
CRect rcExtents = ExtentsGet();
	switch( nOrientation )
	{
	case AFX_IDW_DOCKBAR_LEFT:
		lpLayout->rect.left += rcExtents.left;
		lpLayout->sizeTotal.cx += rcExtents.left;
		rcOwnLayout.right = rcOwnLayout.left + rcExtents.left;
	break;
	case AFX_IDW_DOCKBAR_TOP:
		lpLayout->rect.top += rcExtents.top;
		lpLayout->sizeTotal.cy += rcExtents.top;
		rcOwnLayout.bottom = rcOwnLayout.top + rcExtents.top;
	break;
	case AFX_IDW_DOCKBAR_RIGHT:
		lpLayout->rect.right -= rcExtents.right;
		lpLayout->sizeTotal.cx += rcExtents.right;
		rcOwnLayout.left = rcOwnLayout.right - rcExtents.right;
	break;
	case AFX_IDW_DOCKBAR_BOTTOM:
		lpLayout->rect.bottom -= rcExtents.bottom;
		lpLayout->sizeTotal.cy += rcExtents.bottom;
		rcOwnLayout.top = rcOwnLayout.bottom - rcExtents.bottom;
	break;
#ifdef _DEBUG
	default:
		ASSERT( FALSE );
	break;
#endif // _DEBUG
	} // switch( nOrientation )
	if( lpLayout->hDWP != NULL )
		::AfxRepositionWindow(
			lpLayout,
			m_hWnd,
			&rcOwnLayout
			);
	return 0L;
}

/////////////////////////////////////////////////////////////////////////////
// CExtReportGridSortOrder

IMPLEMENT_DYNCREATE( CExtReportGridSortOrder, CObject );

CExtReportGridSortOrder::CExtReportGridSortOrder()
	: m_nGroupCount( 0L )
{
}

CExtReportGridSortOrder::CExtReportGridSortOrder( const CExtReportGridSortOrder & other )
	: m_nGroupCount( 0L )
{
	Assign( other );
}

CExtReportGridSortOrder::~CExtReportGridSortOrder()
{
	Empty();
}

CExtReportGridSortOrder & CExtReportGridSortOrder::operator = ( const CExtReportGridSortOrder & other )
{
	ASSERT_VALID( this );
	Assign( other );
	return (*this);
}

void CExtReportGridSortOrder::Assign( const CExtReportGridSortOrder & other )
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&other) );
	m_arrColumns.RemoveAll();
	m_arrAscendingFlags.RemoveAll();
	m_arrColumns.Copy( other.m_arrColumns );
	m_arrAscendingFlags.Copy( other.m_arrAscendingFlags );
	m_nGroupCount = other.m_nGroupCount;
	ASSERT_VALID( this );
}

bool CExtReportGridSortOrder :: operator == ( const CExtReportGridSortOrder & other ) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&other) );
bool bIsEqual = IsEqual( other );
	return bIsEqual;
}

bool CExtReportGridSortOrder :: operator != ( const CExtReportGridSortOrder & other ) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&other) );
bool bIsEqual = IsEqual( other );
	return (!bIsEqual);
}

bool CExtReportGridSortOrder::IsEqual( const CExtReportGridSortOrder & other ) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&other) );
LONG nColumnCountThis = ColumnCountGet();
LONG nColumnCountOther = other.ColumnCountGet();
	if( nColumnCountThis != nColumnCountOther )
		return false;
	if( nColumnCountThis == 0 )
		return true;
LONG nColumnGroupCountThis = ColumnGroupCountGet();
LONG nColumnGroupCountOther = other.ColumnGroupCountGet();
	if( nColumnGroupCountThis != nColumnGroupCountOther )
		return false;
LONG nIndex;
	for( nIndex = 0; nIndex < nColumnCountThis; nIndex ++ )
	{
		bool bAscendingThis = false, bAscendingOther = false;
		const CExtReportGridColumn * pThisRGC =
			ColumnGetAt( nIndex, &bAscendingThis );
		ASSERT_VALID( pThisRGC );
		const CExtReportGridColumn * pOtherRGC =
			other.ColumnGetAt( nIndex, &bAscendingOther );
		ASSERT_VALID( pOtherRGC );
		if( LPCVOID(pThisRGC) != LPCVOID(pOtherRGC) )
			return false;
		if( bAscendingThis != bAscendingOther )
			return false;
	} // for( nIndex = 0; nIndex < nColumnCountThis; nIndex ++ )
	return true;
}

void CExtReportGridSortOrder::Empty()
{
	ASSERT_VALID( this );
	m_arrAscendingFlags.RemoveAll();
	m_arrColumns.RemoveAll();
	m_nGroupCount = 0;
	ASSERT_VALID( this );
}

bool CExtReportGridSortOrder::IsEmpty() const
{
	ASSERT_VALID( this );
LONG nColumnCount = ColumnCountGet();
	return ( nColumnCount == 0 ) ? true : false;
}

bool CExtReportGridSortOrder::_VerifyOwner( const CExtReportGridWnd * pRGW ) const // if not NULL - verify all column pointers are owned by report grid
{
	if( pRGW == NULL )
		return true;
LONG nCountColumns = LONG( m_arrColumns.GetSize() );
#ifdef _DEBUG
	ASSERT( nCountColumns >= 0 );
LONG nCountAscendingFlags = LONG( m_arrAscendingFlags.GetSize() );
	ASSERT( nCountAscendingFlags >= 0 );
	ASSERT( nCountColumns == nCountAscendingFlags );
	ASSERT( m_nGroupCount >= 0 );
	ASSERT( m_nGroupCount <= nCountColumns );
#endif // _DEBUG
LONG nIndex;
	for( nIndex = 0; nIndex < nCountColumns; nIndex ++ )
	{
		const CExtReportGridColumn * pRGC =
			m_arrColumns[ nIndex ];
		ASSERT_VALID( pRGC );
		CExtReportGridWnd * pTestRGW = pRGC->GetReportGrid();
		if( LPVOID(pTestRGW) != LPVOID(pRGW) )
			return false;
	} // for( nIndex = 0; nIndex < nCountColumns; nIndex ++ )
	return true;
}

bool CExtReportGridSortOrder::_VerifySortImpl() const // verify all column pointers are marked as sort-enabled
{
// LONG nCountColumns = LONG( m_arrColumns.GetSize() );
// #ifdef _DEBUG
// 	ASSERT( nCountColumns >= 0 );
// LONG nCountAscendingFlags = LONG( m_arrAscendingFlags.GetSize() );
// 	ASSERT( nCountAscendingFlags >= 0 );
// 	ASSERT( nCountColumns == nCountAscendingFlags );
// 	ASSERT( m_nGroupCount >= 0 );
// 	ASSERT( m_nGroupCount <= nCountColumns );
// #endif // _DEBUG
// LONG nIndex;
// 	for( nIndex = 0; nIndex < nCountColumns; nIndex ++ )
// 	{
// 		const CExtReportGridColumn * pRGC =
// 			m_arrColumns[ nIndex ];
// 		ASSERT_VALID( pRGC );
// 		ASSERT( pRGC->SortingEnabledGet() );
// 		bool bAsc1 = pRGC->SortingAscendingGet();
// 		bool bAsc2 = m_arrAscendingFlags[ nIndex ];
// 		if(		( bAsc1 && bAsc2 )
// 			||	( (!bAsc1) && (!bAsc2) )
// 			)
// 			continue;
// 		return false;
// 	} // for( nIndex = 0; nIndex < nCountColumns; nIndex ++ )
	return true;
}

bool CExtReportGridSortOrder::_VerifyUniqueImpl() const // verify all column pointers are unique
{
LONG nCountColumns = LONG( m_arrColumns.GetSize() );
#ifdef _DEBUG
	ASSERT( nCountColumns >= 0 );
LONG nCountAscendingFlags = LONG( m_arrAscendingFlags.GetSize() );
	ASSERT( nCountAscendingFlags >= 0 );
	ASSERT( nCountColumns == nCountAscendingFlags );
	ASSERT( m_nGroupCount >= 0 );
	ASSERT( m_nGroupCount <= nCountColumns );
#endif // _DEBUG
LONG nIndex1, nIndex2;
	for( nIndex1 = 0; nIndex1 < ( nCountColumns - 1 ); nIndex1 ++ )
	{
		const CExtReportGridColumn * pRGC1 =
			m_arrColumns[ nIndex1 ];
		ASSERT_VALID( pRGC1 );
		for( nIndex2 = ( nIndex1 + 1 ); nIndex2 < nCountColumns; nIndex2 ++ )
		{
			const CExtReportGridColumn * pRGC2 =
				m_arrColumns[ nIndex2 ];
			ASSERT_VALID( pRGC2 );
			if( LPCVOID( pRGC1 ) == LPCVOID( pRGC2 ) )
				return false;
		} // for( nIndex2 = ( nIndex1 + 1 ); nIndex2 < nCountColumns; nIndex2 ++ )
	} // for( nIndex1 = 0; nIndex1 < ( nCountColumns - 1 ); nIndex1 ++ )
	return true;
}

#ifdef _DEBUG

void CExtReportGridSortOrder::AssertValid() const
{
	CObject::AssertValid();
	ASSERT( _VerifySortImpl() ); // verify all column pointers are marked as sort-enabled
	ASSERT( _VerifyUniqueImpl() ); // verify all column pointers are unique
}

void CExtReportGridSortOrder::Dump( CDumpContext & dc ) const
{
	CObject::Dump( dc );
}

#endif

LONG CExtReportGridSortOrder::ColumnCountGet() const
{
	ASSERT_VALID( this );
LONG nColumnCount = LONG( m_arrColumns.GetSize() );
	return nColumnCount;
}

LONG CExtReportGridSortOrder::ColumnGroupCountGet() const
{
	ASSERT_VALID( this );
	return m_nGroupCount;
}

bool CExtReportGridSortOrder::ColumnGroupCountSet( LONG nGroupCount )
{
	ASSERT_VALID( this );
	if( nGroupCount < 0 )
		return false;
LONG nColumnCount = ColumnCountGet();
	if( nGroupCount > nColumnCount )
		return false;
	m_nGroupCount = nGroupCount;
	return true;
}

CExtReportGridColumn * CExtReportGridSortOrder::ColumnGetAt(
	LONG nIndex,
	bool * p_bAscending // = NULL
	)
{
	ASSERT_VALID( this );
	if( nIndex < 0 )
		return NULL;
LONG nColumnCount = ColumnCountGet();
	if( nIndex >= nColumnCount )
		return NULL;
	if( p_bAscending != NULL )
	{
		bool bAscending = m_arrAscendingFlags[ nIndex ];
		(*p_bAscending) = bAscending;
	} // if( p_bAscending != NULL )
CExtReportGridColumn * pRGC = m_arrColumns[ nIndex ];
	ASSERT_VALID( pRGC );
	return pRGC;
}

const CExtReportGridColumn * CExtReportGridSortOrder::ColumnGetAt(
	LONG nIndex,
	bool * p_bAscending // = NULL
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridSortOrder * > ( this ) )
		-> ColumnGetAt( nIndex, p_bAscending );
}

bool CExtReportGridSortOrder::ColumnSetAt(
	CExtReportGridColumn * pRGC,
	LONG nIndex,
	bool bAscending
	)
{
	ASSERT_VALID( this );
	if( pRGC == NULL )
		return false;
	ASSERT_VALID( pRGC );
	if( nIndex < 0 )
		return false;
LONG nColumnCount = ColumnCountGet();
	if( nIndex >= nColumnCount )
		return false;
LONG nExistingIndex = ColumnGetIndexOf( pRGC );
	if(		nExistingIndex >= 0
		&&	nExistingIndex != nIndex
		)
		return false;
	m_arrColumns.SetAt( nIndex, pRGC );
	m_arrAscendingFlags.SetAt( nIndex, bAscending );
	pRGC->SortingAscendingSet( bAscending );
	ASSERT_VALID( this );
	return true;
}

bool CExtReportGridSortOrder::ColumnInsert(
	CExtReportGridColumn * pRGC,
	LONG nIndex, // -1 - append
	bool bAscending
	)
{
	ASSERT_VALID( this );
	if( pRGC == NULL )
		return false;
	ASSERT_VALID( pRGC );
LONG nExistingIndex = ColumnGetIndexOf( pRGC );
	if( nExistingIndex >= 0 )
		return false;
LONG nColumnCount = ColumnCountGet();
	if(		nIndex < 0
		||	nIndex > nColumnCount
		)
		nIndex = nColumnCount;
	m_arrColumns.InsertAt( nIndex, pRGC, 1 );
	pRGC->SortingAscendingSet( bAscending );
	m_arrAscendingFlags.InsertAt( nIndex, bAscending, 1 );
	ASSERT_VALID( this );
	return true;
}

bool CExtReportGridSortOrder::ColumnRemove(
	LONG nIndex,
	LONG nCountToRemove // = 1
	)
{
	ASSERT_VALID( this );
	if( nCountToRemove == 0 )
		return true;
	if( nIndex < 0 || nCountToRemove < 0 )
		return false;
// LONG nGroupCount = ColumnGroupCountGet();
LONG nColumnCount = ColumnCountGet();
	if( nIndex >= nColumnCount )
		return false;
	if( nCountToRemove > (nColumnCount - nIndex ) )
		return false;
// LONG nNewColumnCount = nColumnCount - nCountToRemove;
// 	if( nGroupCount > nNewColumnCount )
// 	{
// 		//VERIFY( ColumnGroupCountSet( nNewColumnCount ) );
// 		return false;
// 	}
	m_arrColumns.RemoveAt( nIndex, nCountToRemove );
	m_arrAscendingFlags.RemoveAt( nIndex, nCountToRemove );
	ASSERT_VALID( this );
	return true;
}

bool CExtReportGridSortOrder::ColumnRemove(
	const CExtReportGridColumn * pRGC,
	LONG nCountToRemove // = 1
	)
{
	ASSERT_VALID( this );
LONG nIndex = ColumnGetIndexOf( pRGC );
	if( nIndex < 0 )
		return false;
bool bRetVal = ColumnRemove( nIndex, nCountToRemove );
	return bRetVal;
}

LONG CExtReportGridSortOrder::ColumnGetIndexOf(
	const CExtReportGridColumn * pRGC
	) const
{
	ASSERT_VALID( this );
	if( pRGC == NULL )
		return (-1L);
	ASSERT_VALID( pRGC );
LONG nIndex, nColumnCount = ColumnCountGet();
	for( nIndex = 0; nIndex < nColumnCount; nIndex ++ )
	{
		const CExtReportGridColumn * pTestRGC =
			ColumnGetAt( nIndex );
		if( LPCVOID(pTestRGC) == LPCVOID(pRGC) )
			return nIndex;
	}
	return (-1L);
}

bool CExtReportGridSortOrder::VerifyColumns(
	const CExtReportGridWnd * pRGW, // = NULL // if not NULL - verify all column pointers are owned by report grid
	bool bVerifyUnique, // = true // verify all column pointers are unique
	bool bVerifySort // = true // verify all column pointers are marked as sort-enabled
	) const
{
	ASSERT_VALID( this );
	if( pRGW != NULL )
	{
		ASSERT_VALID( pRGW );
		if( ! _VerifyOwner( pRGW ) )
			return false;
	} // if( pRGW != NULL )
	if( bVerifyUnique )
	{
		if(  ! _VerifyUniqueImpl() )
			return false;
	} // if( bVerifyUnique )
	if( bVerifySort )
	{
		if( ! _VerifySortImpl() )
			return false;
	} // if( bVerifySort )
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CExtReportGridGroupAreaWnd window

bool CExtReportGridGroupAreaWnd::g_bReportGroupAreaWndClassRegistered = false;

IMPLEMENT_DYNAMIC( CExtReportGridGroupAreaWnd, CWnd );		

CExtReportGridGroupAreaWnd::CExtReportGridGroupAreaWnd(
	CExtReportGridWnd & _RGW
	)
	: m_RGW( _RGW )
	, m_bDirectCreateCall( false )
	, m_nMinimalHeight( 34 )
	, m_nMaxItemWidth( 200 )
	, m_rcLayoutIndent( 10, 8, 10, 7 )
	, m_pHelperProcessedRGC( NULL )
{
	ASSERT_VALID( (&m_RGW) );
	VERIFY( RegisterReportGroupAreaWndClass() );
}

CExtReportGridGroupAreaWnd::~CExtReportGridGroupAreaWnd()
{
}

CExtReportGridWnd & CExtReportGridGroupAreaWnd::GetReportGridWnd()
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&m_RGW) );
	return m_RGW;
}

const CExtReportGridWnd & CExtReportGridGroupAreaWnd::GetReportGridWnd() const
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&m_RGW) );
	return m_RGW;
}

bool CExtReportGridGroupAreaWnd::RegisterReportGroupAreaWndClass()
{
	if( g_bReportGroupAreaWndClassRegistered )
		return true;

WNDCLASS _wndClassInfo;
HINSTANCE hInst = ::AfxGetInstanceHandle();
	if( ! ::GetClassInfo(
			hInst,
			__EXT_REPORT_GROUP_AREA_WND_CLASS_NAME,
			&_wndClassInfo
			)
		)
	{
		_wndClassInfo.style = CS_GLOBALCLASS|CS_DBLCLKS;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor = ::LoadCursor( NULL, IDC_ARROW );
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __EXT_REPORT_GROUP_AREA_WND_CLASS_NAME;
		if( ! ::AfxRegisterClass( &_wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}

	g_bReportGroupAreaWndClassRegistered = true;
	return true;
}

bool CExtReportGridGroupAreaWnd::Create(
	CWnd * pParentWnd,
	const RECT & rcWnd, // = CRect( 0, 0, 0, 0 )
	UINT nDlgCtrlID, // = UINT( IDC_STATIC )
	DWORD dwWindowStyle // = WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS
	)
{
	if( ! RegisterReportGroupAreaWndClass() )
	{
		ASSERT( FALSE );
		return false;
	}
	m_bDirectCreateCall = true;
	if( ! CWnd::Create(
			__EXT_REPORT_GROUP_AREA_WND_CLASS_NAME,
			NULL,
			dwWindowStyle,
			rcWnd,
			pParentWnd,
			nDlgCtrlID
			)
		)
	{
		m_bDirectCreateCall = false;
		ASSERT( FALSE );
		return false;
	}
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		::AfxThrowMemoryException();
	}
	m_bDirectCreateCall = false;
	return true;
}

bool CExtReportGridGroupAreaWnd::_CreateHelper()
{
	// EnableToolTips( TRUE );
	return true;
}

INT CExtReportGridGroupAreaWnd::MinimalHeightGet() const
{
	ASSERT_VALID( this );
	return m_nMinimalHeight;
}

void CExtReportGridGroupAreaWnd::MinimalHeightSet( INT nMinimalHeight )
{
	ASSERT_VALID( this );
	if( nMinimalHeight < 1 )
		nMinimalHeight = 1;
	m_nMinimalHeight = nMinimalHeight;
}

INT CExtReportGridGroupAreaWnd::MaxItemWidthGet() const
{
	ASSERT_VALID( this );
	return m_nMaxItemWidth;
}

void CExtReportGridGroupAreaWnd::MaxItemWidthSet( INT nMaxItemWidth )
{
	ASSERT_VALID( this );
	if( nMaxItemWidth < 1 )
		nMaxItemWidth = 1;
	m_nMaxItemWidth = nMaxItemWidth;
}

CRect CExtReportGridGroupAreaWnd::LayoutIndentGet() const
{
	ASSERT_VALID( this );
	return m_rcLayoutIndent;
}

void CExtReportGridGroupAreaWnd::LayoutIndentSet( const RECT & rcLayoutIndent )
{
	ASSERT_VALID( this );
	m_rcLayoutIndent = rcLayoutIndent;
	if( m_rcLayoutIndent.left < 0 )
		m_rcLayoutIndent.left = 0;
	if( m_rcLayoutIndent.top < 0 )
		m_rcLayoutIndent.top = 0;
	if( m_rcLayoutIndent.right < 0 )
		m_rcLayoutIndent.right = 0;
	if( m_rcLayoutIndent.bottom < 0 )
		m_rcLayoutIndent.bottom = 0;
}

CExtReportGridSortOrder * CExtReportGridGroupAreaWnd::OnQueryReportGridSortOrder()
{
	ASSERT_VALID( this );
	if( m_RGW.GetSafeHwnd() == NULL )
		return NULL;
	return ( & ( m_RGW.ReportSortOrderGet() ) );
}

CExtReportGridSortOrder * CExtReportGridGroupAreaWnd::GetReportGridSortOrder()
{
	ASSERT_VALID( this );
	return OnQueryReportGridSortOrder();
}

const CExtReportGridSortOrder * CExtReportGridGroupAreaWnd::GetReportGridSortOrder() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridGroupAreaWnd * > ( this ) )
		-> GetReportGridSortOrder();
}

BEGIN_MESSAGE_MAP(CExtReportGridGroupAreaWnd, CWnd)
	//{{AFX_MSG_MAP(CExtReportGridGroupAreaWnd)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
	ON_MESSAGE( WM_SIZEPARENT, OnSizeParent )
	ON_REGISTERED_MESSAGE(
		CExtContentExpandWnd::g_nMsgPaintItemContent,
		_OnPaintExpandedItemContent
		)
END_MESSAGE_MAP()

BOOL CExtReportGridGroupAreaWnd::PreCreateWindow( CREATESTRUCT & cs ) 
{
	if(		( ! RegisterReportGroupAreaWndClass() )
		||	( ! CWnd::PreCreateWindow( cs ) )
		)
	{
		ASSERT( FALSE );
		return FALSE;
	}
	cs.lpszClass = __EXT_REPORT_GROUP_AREA_WND_CLASS_NAME;
	return TRUE;
}

void CExtReportGridGroupAreaWnd::PreSubclassWindow() 
{
	CWnd::PreSubclassWindow();
	if( m_bDirectCreateCall )
		return;
	if( ! _CreateHelper() )
	{
		ASSERT( FALSE );
		::AfxThrowMemoryException();
	}
}

void CExtReportGridGroupAreaWnd::PostNcDestroy() 
{
	CWnd::PostNcDestroy();
	delete this;
}

CSize CExtReportGridGroupAreaWnd::OnCalcLayoutSize() const
{
	ASSERT_VALID( this );
CSize sizeLayout( 0, 0 );
const CExtReportGridSortOrder * pRGSO = GetReportGridSortOrder();
	if( pRGSO != NULL )
	{
		ASSERT_VALID( pRGSO );
		LONG nColumnGroupCount = pRGSO->ColumnGroupCountGet();
		if( nColumnGroupCount > 0 )
		{
			CSize sizeItemOffset = OnQueryItemOffset();
			ASSERT( sizeItemOffset.cx >= 0 && sizeItemOffset.cy >= 0 );
			LONG nIndex;
			CSize sizePrevItem( 0, 0 );
			for( nIndex = 0; nIndex < nColumnGroupCount; nIndex ++ )
			{
				const CExtReportGridColumn * pRGC =
					pRGSO->ColumnGetAt( nIndex );
				ASSERT_VALID( pRGC );
				CSize sizeItem = OnCalcItemSize( pRGC );
				sizeLayout.cx += sizeItem.cx;
				if( nIndex > 0 )
				{
					sizeLayout.cx += sizeItemOffset.cx;
					sizeLayout.cy +=
						  sizeItem.cy
						+ sizeItemOffset.cy
						- sizePrevItem.cy
						;
				} // if( nIndex > 0 )
				else
				{
					sizeLayout.cy += sizeItem.cy;
				} // else from if( nIndex > 0 )
				sizePrevItem = sizeItem;
			} // for( nIndex = 0; nIndex < nColumnGroupCount; nIndex ++ )
		} // if( nColumnGroupCount > 0 )
	} // if( pRGSO != NULL )
CRect rcLayoutIndent = LayoutIndentGet();
	ASSERT( rcLayoutIndent.left >= 0 );
	ASSERT( rcLayoutIndent.top >= 0 );
	ASSERT( rcLayoutIndent.right >= 0 );
	ASSERT( rcLayoutIndent.bottom >= 0 );
	sizeLayout.cx += rcLayoutIndent.left + rcLayoutIndent.right;
	sizeLayout.cy += rcLayoutIndent.top + rcLayoutIndent.bottom;
INT nMinimalHeight = MinimalHeightGet();
	if( sizeLayout.cy < nMinimalHeight )
		sizeLayout.cy = nMinimalHeight;
	return sizeLayout;
}

CSize CExtReportGridGroupAreaWnd::OnCalcItemSize(
 	const CExtReportGridColumn * pRGC
 	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGC );
CSize _size = pRGC->OnCalcSizeInGroupArea();
INT nMaxItemWidth = MaxItemWidthGet();
	if( _size.cx > nMaxItemWidth )
		_size.cx = nMaxItemWidth;
	return _size;
}

CSize CExtReportGridGroupAreaWnd::OnQueryItemOffset() const
{
	ASSERT_VALID( this );
	return CSize( 4, 9 );
}

void CExtReportGridGroupAreaWnd::OnQueryConnectorInfo(
	CPen * pPenToCreate,
	INT & nHorzDistanceFromRight,
	INT & nVertDistanceFromBottom
	) const
{
	ASSERT_VALID( this );
	if( pPenToCreate != NULL )
	{
		if( pPenToCreate->GetSafeHandle() != NULL )
			pPenToCreate->DeleteObject();
		VERIFY(
			pPenToCreate->CreatePen(
				PS_SOLID,
				1,
				::GetSysColor( COLOR_BTNTEXT )
				)
			);
	} // if( pPenToCreate != NULL )
	nHorzDistanceFromRight = 6;
	nVertDistanceFromBottom = 3;
}

LONG CExtReportGridGroupAreaWnd::ItemHitTest(
	const POINT & ptClient,
	RECT * pRectItem // = NULL
	) const
{
	ASSERT_VALID( this );
	if( pRectItem != NULL )
		::SetRect( pRectItem, 0, 0, 0, 0 );
	if( GetSafeHwnd() == NULL )
		return -1L;
	if( m_RGW.GetSafeHwnd() == NULL )
		return -1L;
	if( ! m_RGW.ReportGroupAreaIsVisible() )
		return -1L;
CRect rcClient;
	GetClientRect( &rcClient );
	if( ! rcClient.PtInRect( ptClient ) )
		return -1L;
const CExtReportGridSortOrder * pRGSO = GetReportGridSortOrder();
	if( pRGSO == NULL )
		return -1L;
	ASSERT_VALID( pRGSO );
LONG nColumnGroupCount = pRGSO->ColumnGroupCountGet();
	if( nColumnGroupCount == 0 )
		return -1L;
CRect rcLayoutIndent = LayoutIndentGet();
	ASSERT( rcLayoutIndent.left >= 0 );
	ASSERT( rcLayoutIndent.top >= 0 );
	ASSERT( rcLayoutIndent.right >= 0 );
	ASSERT( rcLayoutIndent.bottom >= 0 );
CPoint ptWalk = rcLayoutIndent.TopLeft();
CSize sizeItemOffset = OnQueryItemOffset();
	ASSERT( sizeItemOffset.cx >= 0 && sizeItemOffset.cy >= 0 );
LONG nIndex;
	for( nIndex = 0; nIndex < nColumnGroupCount; nIndex ++ )
	{
		const CExtReportGridColumn * pRGC =
			pRGSO->ColumnGetAt( nIndex );
		ASSERT_VALID( pRGC );
		CSize sizeItem = OnCalcItemSize( pRGC );
		CRect rcItem( ptWalk, sizeItem );
		if( rcItem.PtInRect( ptClient ) )
		{
			if( pRectItem != NULL )
				::CopyRect( pRectItem, &rcItem );
			return nIndex;
		}
		ptWalk.x += sizeItem.cx + sizeItemOffset.cx;
		ptWalk.y += sizeItemOffset.cy;
	} // for( nIndex = 0; nIndex < nColumnGroupCount; nIndex ++ )
	return -1L;
}

LONG CExtReportGridGroupAreaWnd::ItemDropHitTest( // returns index of item before which ptClient drops new item or -1
	const POINT & ptClient
	) const
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return -1L;
	if( m_RGW.GetSafeHwnd() == NULL )
		return -1L;
	if( ! m_RGW.ReportGroupAreaIsVisible() )
		return -1L;
CRect rcClient;
	GetClientRect( &rcClient );
	if( ! rcClient.PtInRect( ptClient ) )
		return -1L;
const CExtReportGridSortOrder * pRGSO = GetReportGridSortOrder();
	if( pRGSO == NULL )
		return -1L;
	ASSERT_VALID( pRGSO );
LONG nColumnGroupCount = pRGSO->ColumnGroupCountGet();
	if( nColumnGroupCount == 0 )
		return 0; // drop first into sort order
CRect rcLayoutIndent = LayoutIndentGet();
	ASSERT( rcLayoutIndent.left >= 0 );
	ASSERT( rcLayoutIndent.top >= 0 );
	ASSERT( rcLayoutIndent.right >= 0 );
	ASSERT( rcLayoutIndent.bottom >= 0 );
CPoint ptWalk = rcLayoutIndent.TopLeft();
CSize sizeItemOffset = OnQueryItemOffset();
	ASSERT( sizeItemOffset.cx >= 0 && sizeItemOffset.cy >= 0 );
LONG nIndex, nHT = nColumnGroupCount;
	for( nIndex = 0; nIndex < nColumnGroupCount; nIndex ++ )
	{
		const CExtReportGridColumn * pRGC =
			pRGSO->ColumnGetAt( nIndex );
		ASSERT_VALID( pRGC );
		CSize sizeItem = OnCalcItemSize( pRGC );
		//CRect rcItem( ptWalk, sizeItem );
		INT nMidleX = ptWalk.x + sizeItem.cx / 2;
		if( ptClient.x < nMidleX )
		{
			nHT = nIndex;
			break;
		} // if( ptClient.x < nMidleX )
		ptWalk.x += sizeItem.cx + sizeItemOffset.cx;
		ptWalk.y += sizeItemOffset.cy;
	} // for( nIndex = 0; nIndex < nColumnGroupCount; nIndex ++ )
	return nHT;
}

bool CExtReportGridGroupAreaWnd::ItemDropMarkerGet(
	LONG nHT,
	POINT & ptTop,
	INT & nHeight
	) const
{
	ASSERT_VALID( this );
	if( nHT < 0 )
		return false;
	if( GetSafeHwnd() == NULL )
		return false;
	if( m_RGW.GetSafeHwnd() == NULL )
		return false;
	if( ! m_RGW.ReportGroupAreaIsVisible() )
		return false;
const CExtReportGridSortOrder * pRGSO = GetReportGridSortOrder();
	if( pRGSO == NULL )
		return false;
	ASSERT_VALID( pRGSO );
LONG nColumnGroupCount = pRGSO->ColumnGroupCountGet();
	if( nHT > nColumnGroupCount )
		return false;
CRect rcLayoutIndent = LayoutIndentGet();
	ASSERT( rcLayoutIndent.left >= 0 );
	ASSERT( rcLayoutIndent.top >= 0 );
	ASSERT( rcLayoutIndent.right >= 0 );
	ASSERT( rcLayoutIndent.bottom >= 0 );
	if( nColumnGroupCount == 0 )
	{ // drop first into sort order
		CRect rcClient;
		GetClientRect( &rcClient );
		ptTop.x = rcClient.left + rcLayoutIndent.left;
		ptTop.y = rcClient.top + rcLayoutIndent.top;
		nHeight = rcClient.Height() - rcLayoutIndent.top - rcLayoutIndent.bottom;
		if( nHeight < 10 )
			nHeight = 10;
		return true;
	} // drop first into sort order
CPoint ptWalk = rcLayoutIndent.TopLeft();
CSize sizeItemOffset = OnQueryItemOffset();
	ASSERT( sizeItemOffset.cx >= 0 && sizeItemOffset.cy >= 0 );
LONG nIndex;
	for( nIndex = 0; nIndex <= nHT; nIndex ++ )
	{
		if( nIndex == nColumnGroupCount )
			break;
		const CExtReportGridColumn * pRGC =
			pRGSO->ColumnGetAt( nIndex );
		ASSERT_VALID( pRGC );
		CSize sizeItem = OnCalcItemSize( pRGC );
		//CRect rcItem( ptWalk, sizeItem );
		ptTop.x = ptWalk.x;
		ptTop.y = ptWalk.y;
		nHeight = sizeItem.cy;
		if( nHT == nIndex )
			break;
		ptWalk.x += sizeItem.cx;
		ptTop.x = ptWalk.x;
		ptWalk.x += sizeItemOffset.cx;
		ptWalk.y += sizeItemOffset.cy;
	} // for( nIndex = 0; nIndex <= nHT; nIndex ++ )
	return true;
}

void CExtReportGridGroupAreaWnd::OnRButtonDown(UINT nFlags, CPoint point)
{
	ASSERT_VALID( this );
CRect rcItem( 0, 0, 0, 0 );
LONG nHT = ItemHitTest( point, &rcItem );
	if( nHT >= 0 )
	{
		CRect rcItemScreen = rcItem;
		ClientToScreen( &rcItemScreen );
		CPoint ptScreen = point;
		ClientToScreen( &ptScreen );
		CExtReportGridSortOrder * pRGSO = GetReportGridSortOrder();
		ASSERT_VALID( pRGSO );
#ifdef _DEBUG
		LONG nColumnGroupCount = pRGSO->ColumnGroupCountGet();
		ASSERT( nColumnGroupCount > 0 && nHT < nColumnGroupCount );
#endif // _DEBUG
		CExtReportGridColumn * pRGC = pRGSO->ColumnGetAt( nHT );
		ASSERT_VALID( pRGC );
		m_RGW.OnReportGridColumnCtxMenuTrack(
			this,
			pRGC,
			ptScreen,
			rcItemScreen,
			rcItemScreen
			);
		return;
	} // if( nHT >= 0 )
	CWnd::OnLButtonDown( nFlags, point );
}

void CExtReportGridGroupAreaWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	ASSERT_VALID( this );
static CExtGridCell * g_pTrackCell = NULL;
	if( g_pTrackCell != NULL )
	{
		CWnd::OnLButtonDown( nFlags, point );
		return;
	} // if( g_pTrackCell != NULL )

CRect rcItem( 0, 0, 0, 0 );
LONG nStartHT = ItemHitTest( point, &rcItem );
	if( nStartHT < 0 )
	{
		CWnd::OnLButtonDown( nFlags, point );
		return;
	} // if( nStartHT < 0 )
	ASSERT( m_RGW.GetSafeHwnd() != NULL );
	ASSERT( m_RGW.ReportGroupAreaIsVisible() );
CRect rcClient;
	GetClientRect( &rcClient );
	ASSERT( rcClient.PtInRect( point ) );
CExtReportGridSortOrder * pRGSO = GetReportGridSortOrder();
	ASSERT_VALID( pRGSO );
LONG nColumnGroupCount = pRGSO->ColumnGroupCountGet();
	ASSERT( nColumnGroupCount > 0 && nStartHT < nColumnGroupCount );
CExtReportGridColumn * pRealRGC = pRGSO->ColumnGetAt( nStartHT );
	ASSERT_VALID( pRealRGC );
	if( ! pRealRGC->DragDropEnabledGet() )
		return;
__EXT_MFC_SAFE_LPCTSTR strColumnName = pRealRGC->ColumnNameGet();
__EXT_MFC_SAFE_LPCTSTR strCategoryName = pRealRGC->CategoryNameGet();

bool bCenteredDndAlignment = m_RGW.OnGridQueryCenteredDndAlignment();
CRect rcDND = rcItem;
INT nRealExtent = 0;
	pRealRGC->ExtentGet( nRealExtent, 0 );
CSize _sizeMeasure = pRealRGC->MeasureCell( &m_RGW );
	nRealExtent = max( nRealExtent, _sizeMeasure.cx );
	if( nRealExtent > 0 )
	{
		CPoint pt = point;
		if( ! bCenteredDndAlignment )
			rcDND.left = pt.x - nRealExtent / 2;
		rcDND.right = rcDND.left + nRealExtent;
	} // if( nRealExtent > 0 )
	if( bCenteredDndAlignment )
		rcDND.OffsetRect(
			point.x - rcItem.left - rcItem.Width()/2,
			point.y - rcItem.bottom + 4
			);
	m_pHelperProcessedRGC = pRealRGC;
CExtContentExpandWnd wndDND, wndArrows;
CExtGridHitTestInfo htInfoDrop;
CPoint ptShiftLast( 0, 0 );
CRect rcVisibleRange = m_RGW.OnSiwGetVisibleRange();
HWND hWndGrid = m_hWnd;
	g_pTrackCell = pRealRGC;
	CExtMouseCaptureSink::SetCapture( hWndGrid );
bool bStopFlag = false;
bool bDND = true, bClick = false;
bool bEventDropIn = false, bEventDropOut = false, bEventDropSame = false;
LONG nGroupAreaDropHT = -1L;
	ASSERT( m_RGW.m_hCursorOuterDragOK != NULL );
	ASSERT( m_RGW.m_hCursorOuterDragCancel != NULL );
	::SetCursor( m_RGW.m_hCursorOuterDragOK );
MSG msg;
	for(	;
				::IsWindow( hWndGrid )
			&&	bDND
			&&	(! bClick )
			&&	(g_pTrackCell != NULL)
			;
		)
	{ // detect drag-n-drop loop
		bool bDndStartDetected = false;
		if( ! ::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
		{
			if(		( ! ::IsWindow( hWndGrid ) )
				||	(! bDND )
				||	g_pTrackCell == NULL
				)
				break;
			if( CExtGridWnd::g_bEnableOnIdleCalls )
			{
				for(	LONG nIdleCounter = 0L;
						::AfxGetThread()->OnIdle(nIdleCounter);
						nIdleCounter ++
						);
			}
			::WaitMessage();
			continue;
		} // if( ! ::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
		switch( msg.message )
		{
		case WM_KILLFOCUS:
			if( msg.hwnd == hWndGrid )
				bDND = false;
		break;
		case WM_CANCELMODE:
		case WM_ACTIVATEAPP:
		case WM_SYSCOMMAND:
		case WM_SETTINGCHANGE:
		case WM_SYSCOLORCHANGE:
			bDND = false;
		break;
		case WM_COMMAND:
			if(		(HIWORD(msg.wParam)) == 0
				||	(HIWORD(msg.wParam)) == 1
				)
				bDND = false;
		break;
		case WM_CAPTURECHANGED:
			if( (HWND)msg.wParam != hWndGrid )
				bDND = false;
		break;
		case WM_MOUSEWHEEL:
			if( msg.hwnd != hWndGrid )
				bDND = false;
			else
			{
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
				continue;
			} // else from if( msg.hwnd != hWndGrid )
		break;
		case WM_MOUSEMOVE:
			if( msg.hwnd != hWndGrid )
				bDND = false;
			else
			{
				ASSERT_VALID( this );
				ASSERT_VALID( g_pTrackCell );
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
				CPoint point2;
				point2 = DWORD( msg.lParam );
				CPoint ptShift = point2 - point;
				if( ptShift.x < 0 )
					ptShift.x = - ptShift.x;
				if( ptShift.y < 0 )
					ptShift.y = - ptShift.y;
				CSize _sizeDragOffset = m_RGW.OnGridQueryStartDragOffset();
				if(		ptShift.x >= _sizeDragOffset.cx
					||	ptShift.y >= _sizeDragOffset.cy
					)
					bDndStartDetected = true;
			} // else from if( msg.hwnd != hWndGrid )
		break;
		case WM_LBUTTONUP:
			bDND = bClick = false;
			if( msg.hwnd == hWndGrid )
			{
				CPoint ptClient;
				ptClient = DWORD( msg.lParam );
				LONG nOwnHT = ItemHitTest( ptClient );
				if( nOwnHT >= 0 )
				{
					ASSERT( nOwnHT < nColumnGroupCount );
					CExtReportGridColumn * pRgcHT =
						pRGSO->ColumnGetAt( nOwnHT );
					ASSERT_VALID( pRgcHT );
					if( LPCVOID(pRgcHT) == LPCVOID(pRealRGC) )
						bClick = true;
				}
			} // if( msg.hwnd == hWndGrid )
			break;
		case WM_RBUTTONUP:
		case WM_MBUTTONUP:
			bDND = false;
			if( msg.hwnd == hWndGrid )
			{
				ASSERT_VALID( this );
				ASSERT_VALID( g_pTrackCell );
				::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
			} // if( msg.hwnd == hWndGrid )
			break;
		case WM_LBUTTONDBLCLK:
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDBLCLK:
		case WM_RBUTTONDOWN:
		case WM_MBUTTONDBLCLK:
		case WM_MBUTTONDOWN:
		case WM_CONTEXTMENU:
		case WM_NCLBUTTONUP:
		case WM_NCLBUTTONDBLCLK:
		case WM_NCLBUTTONDOWN:
		case WM_NCRBUTTONUP:
		case WM_NCRBUTTONDBLCLK:
		case WM_NCRBUTTONDOWN:
		case WM_NCMBUTTONUP:
		case WM_NCMBUTTONDBLCLK:
		case WM_NCMBUTTONDOWN:
			bDND = false;
		break;
		default:
			if(	   WM_KEYFIRST <= msg.message
				&& msg.message <= WM_KEYLAST
				)
			{
				if( msg.message == WM_KEYDOWN || msg.message == WM_KEYUP )
				{
					if( msg.wParam == VK_SHIFT || msg.wParam == VK_CONTROL )
					{
						::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
						continue;
					} // if( msg.wParam == VK_SHIFT || msg.wParam == VK_CONTROL )
				} // if( msg.message == WM_KEYDOWN || msg.message == WM_KEYUP )
				bStopFlag = true;
			}
		break;
		} // switch( msg.message )
		if( ! bDND )
			break;
		if( bDndStartDetected )
			break;
		if( bClick )
			break;
		if( ! ::AfxGetThread() -> PumpMessage() )
			break;
	} // detect drag-n-drop loop
	if(		bDND
		&&	wndDND.Activate(
				rcDND,
				this,
				__ECWAF_DEF_EXPANDED_ITEM_PAINTER
					|__ECWAF_NO_CAPTURE
					|__ECWAF_REDIRECT_MOUSE
					|__ECWAF_REDIRECT_NO_DEACTIVATE
					|__ECWAF_REDIRECT_AND_HANDLE
					|__ECWAF_HANDLE_MOUSE_ACTIVATE
					|__ECWAF_MA_NOACTIVATE
				)
		)
	{
		CRect rcInitialWndDND;
		wndDND.GetWindowRect( &rcInitialWndDND );
		for(	;
					::IsWindow( hWndGrid )
				&&	(!bStopFlag)
				&&	(g_pTrackCell != NULL)
				;
			)
		{ // main message loop
			if( ! ::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
			{
				if(		( ! ::IsWindow( hWndGrid ) )
					||	bStopFlag
					||	g_pTrackCell == NULL
					)
					break;
				if( CExtGridWnd::g_bEnableOnIdleCalls )
				{
					for(	LONG nIdleCounter = 0L;
							::AfxGetThread()->OnIdle(nIdleCounter);
							nIdleCounter ++
							);
				}
				::WaitMessage();
				continue;
			} // if( ! ::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
			switch( msg.message )
			{
			case WM_KILLFOCUS:
				if( msg.hwnd == hWndGrid )
					bStopFlag = true;
			break;
			case WM_CANCELMODE:
			case WM_ACTIVATEAPP:
			case WM_SYSCOMMAND:
			case WM_SETTINGCHANGE:
			case WM_SYSCOLORCHANGE:
				bStopFlag = true;
			break;
			case WM_COMMAND:
				if(		(HIWORD(msg.wParam)) == 0
					||	(HIWORD(msg.wParam)) == 1
					)
					bStopFlag = true;
			break;
			case WM_CAPTURECHANGED:
				if( (HWND)msg.wParam != hWndGrid )
					bStopFlag = true;
			break;
			case WM_MOUSEWHEEL:
				if( msg.hwnd != hWndGrid )
					bStopFlag = true;
				else
				{
					::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
					continue;
				} // else from if( msg.hwnd != hWndGrid )
			break;
			case WM_MOUSEMOVE:
				if( msg.hwnd != hWndGrid )
					bStopFlag = true;
				else
				{
					bEventDropSame = false;
					ASSERT_VALID( this );
					ASSERT_VALID( g_pTrackCell );
					::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
					CPoint point2;
					point2 = DWORD( msg.lParam );
					CPoint ptShift = point2 - point;
					if( ptShift != ptShiftLast )
					{
						CExtGridHitTestInfo htInfoDropNew;
						CPoint ptRGW = point2;
						ClientToScreen( &ptRGW );
						m_RGW.ScreenToClient( &ptRGW );
						CExtGridHitTestInfo htInfoRGW; // = htInfo;
						htInfoRGW.m_ptClient = point2;
						ClientToScreen( &htInfoRGW.m_ptClient );
						m_RGW.ScreenToClient( &htInfoRGW.m_ptClient );
						htInfoRGW.m_dwAreaFlags = __EGBWA_OUTER_TOP|__EGBWA_OUTER_CELLS;
						htInfoRGW.m_nRowNo = 0;
						htInfoRGW.m_nColNo = 32767;
						m_RGW.OnGridCalcOuterDropTarget(
							htInfoRGW, // htInfo
							htInfoDropNew,
							ptRGW // point2
							);
						bool bSelfCancelDropEffect = false;
						if( ! htInfoDropNew.IsHoverEmpty() )
						{
							if(		(! htInfoDropNew.IsHoverEqual(htInfoDrop) )
								||	wndArrows.GetSafeHwnd() == NULL
								)
							{
								bEventDropIn = false;
								bEventDropOut = true;
								htInfoDrop = htInfoDropNew;
								bool bOuterDropAfterState =
									( (htInfoDrop.m_dwAreaFlags&__EGBWA_OUTER_DROP_AFTER) != 0 )
										? true : false;
								if( ! bSelfCancelDropEffect )
								{
									nGroupAreaDropHT = -1L;
									bEventDropIn = true;
									bEventDropOut = false;
									CRect rcArrows(
										bOuterDropAfterState
											? htInfoDrop.m_rcItem.right
											: htInfoDrop.m_rcItem.left,
										htInfoDrop.m_rcItem.top,
										bOuterDropAfterState
											? htInfoDrop.m_rcItem.right
											: htInfoDrop.m_rcItem.left,
										htInfoDrop.m_rcItem.bottom
										);
									m_RGW.ClientToScreen( &rcArrows );
									ScreenToClient( &rcArrows );
									VERIFY(
										wndArrows.Activate(
											rcArrows,
											this,
											__ECWAF_DRAW_RED_ARROWS
												|__ECWAF_TRANSPARENT_ITEM
												|__ECWAF_NO_CAPTURE
												|__ECWAF_REDIRECT_MOUSE
												|__ECWAF_REDIRECT_NO_DEACTIVATE
												|__ECWAF_REDIRECT_AND_HANDLE
												|__ECWAF_HANDLE_MOUSE_ACTIVATE
												|__ECWAF_MA_NOACTIVATE
											)
										);
									if( wndArrows.GetSafeHwnd() != NULL )
										wndArrows.SetWindowPos(
											&wndDND, 0, 0, 0, 0,
											SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE
											);
								} // if( ! bSelfCancelDropEffect )
								if( bEventDropIn )
								{
									nGroupAreaDropHT = -1L;
									bEventDropOut = false;
								} // if( bEventDropIn )
							} // if( ! htInfoDropNew.IsHoverEqual(htInfoDrop) ...
						} // if( ! htInfoDropNew.IsHoverEmpty() )
						else
						{
							CPoint ptGAW = point2;
							LONG nNewGroupAreaDropHT = ItemDropHitTest( ptGAW );
							if( nNewGroupAreaDropHT >= 0 )
							{
								if(		nNewGroupAreaDropHT == nStartHT
									||	nNewGroupAreaDropHT == ( nStartHT + 1 )
									)
									bEventDropSame = true;
								CPoint ptTop( 0, 0 );
								INT nHeight = 0;
								if(		nNewGroupAreaDropHT != nGroupAreaDropHT
									&&	ItemDropMarkerGet(
											nNewGroupAreaDropHT,
											ptTop,
											nHeight
											)
									)
								{
									nGroupAreaDropHT = nNewGroupAreaDropHT;
									if( bEventDropSame )
									{
										wndArrows.Deactivate();
									} // if( bEventDropSame )
									else
									{
										CRect rcArrows(
											ptTop.x,
											ptTop.y,
											ptTop.x,
											ptTop.y + nHeight
											);
										VERIFY(
											wndArrows.Activate(
												rcArrows,
												this,
												__ECWAF_DRAW_RED_ARROWS
													|__ECWAF_TRANSPARENT_ITEM
													|__ECWAF_NO_CAPTURE
													|__ECWAF_REDIRECT_MOUSE
													|__ECWAF_REDIRECT_NO_DEACTIVATE
													|__ECWAF_REDIRECT_AND_HANDLE
													|__ECWAF_HANDLE_MOUSE_ACTIVATE
													|__ECWAF_MA_NOACTIVATE
												)
											);
										if( wndArrows.GetSafeHwnd() != NULL )
											wndArrows.SetWindowPos(
												&wndDND, 0, 0, 0, 0,
												SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE
												);
									} // else from if( bEventDropSame )
									bEventDropIn = false;
									bEventDropOut = false;
									bSelfCancelDropEffect = false;
								} // if( nNewGroupAreaDropHT != nGroupAreaDropHT ...
							} // if( ( nNewGroupAreaDropHT = ItemDropHitTest(ptGAW) ) >= 0 )
							else
								nGroupAreaDropHT = -1L;
							if( nGroupAreaDropHT < 0 )
							{
								bEventDropIn = false;
								bEventDropOut = true;
								bSelfCancelDropEffect = true;
							} // if( nGroupAreaDropHT < 0 )
						} // else from if( ! htInfoDropNew.IsHoverEmpty() )
						if( bSelfCancelDropEffect )
							wndArrows.Deactivate();
						if( bEventDropOut )
							::SetCursor( m_RGW.m_hCursorOuterDragCancel );
						else if( bEventDropIn || nGroupAreaDropHT >= 0 )
							::SetCursor( m_RGW.m_hCursorOuterDragOK );
						ptShiftLast = ptShift;
						CRect rcWnd( rcInitialWndDND );
						rcWnd.OffsetRect( ptShift );
						wndDND.MoveWindow( &rcWnd );
						wndDND.Invalidate();
						wndDND.UpdateWindow();
						if( wndArrows.GetSafeHwnd() != NULL )
							wndArrows.UpdateWindow();
						UpdateWindow();
						m_RGW.OnSwUpdateWindow();
						CExtPaintManager::stat_PassPaintMessages();
					} // if( ptShift != ptShiftLast )
					continue;
				} // else from if( msg.hwnd != hWndGrid )
			break;
			case WM_LBUTTONUP:
			case WM_RBUTTONUP:
			case WM_MBUTTONUP:
				bStopFlag = true;
				if( msg.hwnd == hWndGrid )
				{
					ASSERT_VALID( this );
					ASSERT_VALID( g_pTrackCell );
					::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
				} // if( msg.hwnd == hWndGrid )
				break;
			case WM_LBUTTONDBLCLK:
			case WM_LBUTTONDOWN:
			case WM_RBUTTONDBLCLK:
			case WM_RBUTTONDOWN:
			case WM_MBUTTONDBLCLK:
			case WM_MBUTTONDOWN:
			case WM_CONTEXTMENU:
			case WM_NCLBUTTONUP:
			case WM_NCLBUTTONDBLCLK:
			case WM_NCLBUTTONDOWN:
			case WM_NCRBUTTONUP:
			case WM_NCRBUTTONDBLCLK:
			case WM_NCRBUTTONDOWN:
			case WM_NCMBUTTONUP:
			case WM_NCMBUTTONDBLCLK:
			case WM_NCMBUTTONDOWN:
				bStopFlag = true;
			break;
			default:
				if(	   WM_KEYFIRST <= msg.message
					&& msg.message <= WM_KEYLAST
					)
				{
					if( msg.message == WM_KEYDOWN || msg.message == WM_KEYUP )
					{
						if( msg.wParam == VK_SHIFT || msg.wParam == VK_CONTROL )
						{
							::PeekMessage(&msg,NULL,msg.message,msg.message,PM_REMOVE);
							continue;
						} // if( msg.wParam == VK_SHIFT || msg.wParam == VK_CONTROL )
					} // if( msg.message == WM_KEYDOWN || msg.message == WM_KEYUP )
					bStopFlag = true;
					bEventDropIn = false;
					bEventDropOut = false;
					bDND = false;
				}
			break;
			} // switch( msg.message )
			if( bStopFlag )
				break;
			if( ! ::AfxGetThread() -> PumpMessage() )
				break;
		} // message loop
	} // if( bDND ...
	g_pTrackCell = NULL;
	::ReleaseCapture();
	::SetCursor( m_RGW.m_hCursorOuterDragOK );
	wndDND.Deactivate();
	wndArrows.Deactivate();
	if( ! ::IsWindow( hWndGrid ) )
		return;
	m_pHelperProcessedRGC = NULL;
	if( bDND )
	{
		if( bEventDropIn )
		{
			ASSERT( ! htInfoDrop.IsHoverEmpty() );
			//m_RGW.OnGridOuterDragComplete( htInfo, htInfoDrop );
			bool bOuterDropAfterState =
				( (htInfoDrop.m_dwAreaFlags&__EGBWA_OUTER_DROP_AFTER) != 0 )
					? true : false;
			// activate column
			if(	m_RGW.ReportColumnActivate(
					strColumnName,
					strCategoryName,
					true,
					htInfoDrop.m_nColNo + ( bOuterDropAfterState ? 1 : 0 ),
					true
					)
				)
			{ // if succeeded to activate column, then remove it from sort order
				CExtReportGridSortOrder _rgso = *pRGSO;
				nColumnGroupCount--;
				ASSERT( nColumnGroupCount >= 0 );
				_rgso.ColumnGroupCountSet( nColumnGroupCount );
				VERIFY( _rgso.ColumnRemove( pRealRGC ) );
				m_RGW.ReportSortOrderSet( _rgso, true );
			} // if succeeded to activate column, then remove it from sort order
#ifdef _DEBUG
			else
			{ // if failed to activate column
				ASSERT( FALSE );
			} // if failed to activate column
#endif // _DEBUG
		} // if( bEventDropIn )
		else if( nGroupAreaDropHT >= 0 && (! bEventDropSame ) )
		{
			bool bAscending = true;
			CExtReportGridSortOrder _rgso = *pRGSO;
			ASSERT( nGroupAreaDropHT <= nColumnGroupCount );
			LONG nExistingIndex = _rgso.ColumnGetIndexOf( pRealRGC );
			if( nExistingIndex >= 0 )
			{
				if( nExistingIndex < nColumnGroupCount )
				{
					if( nGroupAreaDropHT > nExistingIndex )
						nGroupAreaDropHT --;
					nColumnGroupCount --;
					_rgso.ColumnGroupCountSet( nColumnGroupCount );
#ifdef _DEBUG
					CExtReportGridColumn * pExistingRGC =
						_rgso.ColumnGetAt( nExistingIndex );
					ASSERT_VALID( pExistingRGC );
					ASSERT( LPVOID(pRealRGC) == LPVOID(pRealRGC) );
#endif // _DEBUG
				} // if( nExistingIndex < nColumnGroupCount )
				VERIFY( _rgso.ColumnRemove( nExistingIndex, 1 ) );
				bAscending = pRealRGC->SortingAscendingGet();
			} // if( nExistingIndex >= 0 )
				bAscending = pRealRGC->SortingAscendingGet();
			_rgso.ColumnInsert(
				pRealRGC,
				nGroupAreaDropHT,
				bAscending
				);
			nColumnGroupCount ++;
			_rgso.ColumnGroupCountSet( nColumnGroupCount );
			const CExtReportGridSortOrder & _rgsoCurrent = m_RGW.ReportSortOrderGet();
			if( _rgsoCurrent != _rgso )
				m_RGW.ReportSortOrderSet( _rgso, true );
		} // else else if( nGroupAreaDropHT >= 0 && (! bEventDropSame ) )
		else if( bEventDropOut && (! bEventDropSame ) )
		{
			// deactivate column
			if(	m_RGW.ReportColumnActivate(
					strColumnName,
					strCategoryName,
					false,
					0,
					true
					)
				)
			{ // if succeeded to deactivate column, then remove it from sort order
				CExtReportGridSortOrder _rgso = *pRGSO;
				nColumnGroupCount--;
				ASSERT( nColumnGroupCount >= 0 );
				_rgso.ColumnGroupCountSet( nColumnGroupCount );
				VERIFY( _rgso.ColumnRemove( pRealRGC ) );
				m_RGW.ReportSortOrderSet( _rgso, true );
			} // if succeeded to deactivate column, then remove it from sort order
#ifdef _DEBUG
			else
			{ // if failed to deactivate column
				ASSERT( FALSE );
			} // if failed to deactivate column
#endif // _DEBUG
		} // else else if( bEventDropOut && (! bEventDropSame ) )
		Invalidate();
		UpdateWindow();
		return;
	} // if( bDND )
	if( ! bClick )
		return;
CExtReportGridSortOrder _rgso = m_RGW.ReportSortOrderGet();
LONG nExistingIndex = _rgso.ColumnGetIndexOf( pRealRGC );
	ASSERT( nExistingIndex >= 0 );
bool bAscending = pRealRGC->SortingAscendingGet();
	_rgso.ColumnSetAt( pRealRGC, nExistingIndex, !bAscending );
	m_RGW.ReportSortOrderSet( _rgso, true );
}

LRESULT CExtReportGridGroupAreaWnd::_OnPaintExpandedItemContent(WPARAM wParam, LPARAM lParam)
{
	ASSERT_VALID( this );
	lParam;
CExtContentExpandWnd::PAINT_ITEM_CONTENT_DATA * p_picd =
		(CExtContentExpandWnd::PAINT_ITEM_CONTENT_DATA *)wParam;
	ASSERT( p_picd != NULL );
	ASSERT( p_picd->m_dc.GetSafeHdc() != NULL );
	if( m_pHelperProcessedRGC == NULL )
		return 0;
	ASSERT_VALID( m_pHelperProcessedRGC );
COLORREF clrText = m_RGW.OnSiwGetSysColor( COLOR_BTNTEXT );
int nOldBkMode = p_picd->m_dc.SetBkMode( TRANSPARENT );
COLORREF clrTextOld = p_picd->m_dc.SetTextColor( clrText );
CFont * pOldFont = p_picd->m_dc.SelectObject( &( m_RGW.OnSiwGetDefaultFont() ) );
CRect rcVisibleRange( 0, 0, 0, 0 );
	m_pHelperProcessedRGC->OnPaintBackground(
		m_RGW,
		p_picd->m_dc,
		0L,
		0L,
		0L,
		0L,
		0,
		-1,
		p_picd->m_rcItem, // extra-space
		p_picd->m_rcItem,
		rcVisibleRange,
		__EGBWA_OUTER_TOP|__EGBWA_OUTER_CELLS,
		__EGCPF_OUTER_DND
		);
	m_pHelperProcessedRGC->OnPaintForeground(
		m_RGW,
		p_picd->m_dc,
		0L,
		0L,
		0L,
		0L,
		0,
		-1,
		p_picd->m_rcItem, // extra-space
		p_picd->m_rcItem,
		rcVisibleRange,
		__EGBWA_OUTER_TOP|__EGBWA_OUTER_CELLS,
		__EGCPF_OUTER_DND
		);
	p_picd->m_dc.SelectObject( pOldFont );
	p_picd->m_dc.SetTextColor( clrTextOld );
	p_picd->m_dc.SetBkMode( nOldBkMode );
	return (!0);
}

LRESULT CExtReportGridGroupAreaWnd::OnSizeParent( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	wParam;
DWORD dwWndStyle = GetStyle();
	if( (dwWndStyle&WS_VISIBLE) == 0 )
		return 0;
AFX_SIZEPARENTPARAMS * lpLayout =
		(AFX_SIZEPARENTPARAMS *) lParam;
	ASSERT( lpLayout != NULL );
CRect rcOwnLayout = lpLayout->rect;
CSize sizeLayout = OnCalcLayoutSize();
	lpLayout->rect.top += sizeLayout.cy;
	lpLayout->sizeTotal.cy += sizeLayout.cy;
	rcOwnLayout.bottom = rcOwnLayout.top + sizeLayout.cy;
	if( lpLayout->hDWP != NULL )
		::AfxRepositionWindow(
			lpLayout,
			m_hWnd,
			&rcOwnLayout
			);
	return 0L;
}

void CExtReportGridGroupAreaWnd::OnSize( UINT nType, int cx, int cy ) 
{
	CWnd::OnSize( nType, cx, cy );
}

BOOL CExtReportGridGroupAreaWnd::OnEraseBkgnd( CDC * pDC ) 
{
	pDC;
	return TRUE;
}

void CExtReportGridGroupAreaWnd::OnPaint() 
{
	m_RGW.m_bHelperRenderingGroupArea = true;
CRect rcClient;
	GetClientRect( &rcClient );
CPaintDC dcPaint( this );
CExtMemoryDC dc( &dcPaint, &rcClient );
	dc.FillSolidRect(
		&rcClient,
		::GetSysColor( COLOR_3DSHADOW )
		);
COLORREF clrText = m_RGW.OnSiwGetSysColor( COLOR_BTNTEXT );
int nOldBkMode = dc.SetBkMode( TRANSPARENT );
COLORREF clrTextOld = dc.SetTextColor( clrText );
CFont * pDrawFont = &( m_RGW.OnSiwGetDefaultFont() );
CFont * pOldFont = dc.SelectObject( pDrawFont );
CRect rcLayoutIndent = LayoutIndentGet();
	ASSERT( rcLayoutIndent.left >= 0 );
	ASSERT( rcLayoutIndent.top >= 0 );
	ASSERT( rcLayoutIndent.right >= 0 );
	ASSERT( rcLayoutIndent.bottom >= 0 );
bool bNonEmptyGroupAreaDrawn = false;
CExtSafeString strCaption;
	m_RGW.OnReportGridGroupAreaQueryEmptyText( strCaption );
	if( m_RGW.GetSafeHwnd() != NULL )
	{
		const CExtReportGridSortOrder * pRGSO = GetReportGridSortOrder();
#ifdef _DEBUG
		if( pRGSO != NULL )
		{
			ASSERT_VALID( pRGSO );
		} // if( pRGSO != NULL )
#endif // _DEBUG
		if( (m_RGW.SiwGetStyleEx()&__EGWS_EX_PM_COLORS) != 0 )
		{
			if( m_RGW.PmBridge_GetPM()->ReportGrid_PaintGroupAreaBackground(
					dc,
					rcClient,
					( pRGSO != NULL && pRGSO->ColumnGroupCountGet() == 0 )
						? LPCTSTR( strCaption )
						: NULL
						,
					(CObject*)this
					)
				)
			{
				bNonEmptyGroupAreaDrawn = true;
			}
		} // if( (m_RGW.SiwGetStyleEx()&__EGWS_EX_PM_COLORS) != 0 )
		if( pRGSO != NULL )
		{
			ASSERT_VALID( pRGSO );
			LONG nColumnGroupCount = pRGSO->ColumnGroupCountGet();
			if( nColumnGroupCount > 0 )
			{
				bNonEmptyGroupAreaDrawn = true;
				CPoint ptWalk = rcLayoutIndent.TopLeft();
				CSize sizeItemOffset = OnQueryItemOffset();
				ASSERT( sizeItemOffset.cx >= 0 && sizeItemOffset.cy >= 0 );
				LONG nIndex;
				CRect rcVisibleRange( 0, 0, nColumnGroupCount, 0 );
				DWORD dwAreaFlags = 0; // __EGBWA_OUTER_CELLS;
				DWORD dwHelperPaintFlags = 0;
				CPen penToCreate, * pOldPen = NULL;
				INT nHorzDistanceFromRight = 0, nVertDistanceFromBottom = 0;
				OnQueryConnectorInfo(
					&penToCreate,
					nHorzDistanceFromRight,
					nVertDistanceFromBottom
					);
				if( penToCreate.GetSafeHandle() != NULL )
					pOldPen = dc.SelectObject( &penToCreate );
				for( nIndex = 0; nIndex < nColumnGroupCount; nIndex ++ )
				{
					const CExtReportGridColumn * pRGC =
						pRGSO->ColumnGetAt( nIndex );
					ASSERT_VALID( pRGC );
					CSize sizeItem = OnCalcItemSize( pRGC );
					CRect rcItem( ptWalk, sizeItem );
					if( nIndex < ( nColumnGroupCount - 1 ) )
					{
						POINT arrPtConnector[ 3 ] =
						{
							{
								rcItem.right - nHorzDistanceFromRight,
								rcItem.bottom
							},
							{
								rcItem.right - nHorzDistanceFromRight,
								rcItem.bottom + nVertDistanceFromBottom
							},
							{
								rcItem.right + sizeItemOffset.cx,
								rcItem.bottom + nVertDistanceFromBottom
							},
						};
						dc.MoveTo( arrPtConnector[ 0 ] );
						dc.LineTo( arrPtConnector[ 1 ] );
						dc.LineTo( arrPtConnector[ 2 ] );
					} // if( nIndex < ( nColumnGroupCount - 1 ) )
					pRGC->OnPaintBackground(
						m_RGW,
						dc,
						nIndex,
						0L,
						nIndex,
						0L,
						0,
						-1,
						rcItem,
						rcItem,
						rcVisibleRange,
						dwAreaFlags,
						dwHelperPaintFlags
						);
					pRGC->OnPaintForeground(
						m_RGW,
						dc,
						nIndex,
						0L,
						nIndex,
						0L,
						0,
						-1,
						rcItem,
						rcItem,
						rcVisibleRange,
						dwAreaFlags,
						dwHelperPaintFlags
						);
					ptWalk.x += sizeItem.cx + sizeItemOffset.cx;
					ptWalk.y += sizeItemOffset.cy;
				} // for( nIndex = 0; nIndex < nColumnGroupCount; nIndex ++ )
				if( penToCreate.GetSafeHandle() != NULL )
					dc.SelectObject( pOldPen );
			} // if( nColumnGroupCount > 0 )
		} // if( pRGSO != NULL )
	} // if( m_RGW.GetSafeHwnd() != NULL )
	if( ! bNonEmptyGroupAreaDrawn )
	{
		if( ! strCaption.IsEmpty() )
		{
			CRect rcCaption = rcClient;
			rcCaption.DeflateRect(
				rcLayoutIndent.left,
				rcLayoutIndent.top,
				rcLayoutIndent.right,
				rcLayoutIndent.bottom
				);
			CRect rcText =
				CExtPaintManager::stat_CalcTextDimension(
					dc,
					*pDrawFont,
					strCaption
					);
			rcText.OffsetRect( rcCaption.TopLeft() );
			rcText.right += 8;
			rcCaption.right = min( rcCaption.right, rcText.right );
			dc.FillSolidRect(
				&rcCaption,
				::GetSysColor( COLOR_3DFACE )
				);
			dc.SetTextColor( ::GetSysColor(COLOR_3DSHADOW) );
			rcCaption.DeflateRect( 4, 0 );
			dc.DrawText(
				LPCTSTR(strCaption),
				strCaption.GetLength(),
				&rcCaption,
				DT_SINGLELINE|DT_LEFT|DT_VCENTER|DT_END_ELLIPSIS
				);
		} // if( ! strCaption.IsEmpty() )
	} // if( ! bNonEmptyGroupAreaDrawn )
	dc.SelectObject( pOldFont );
	dc.SetTextColor( clrTextOld );
	dc.SetBkMode( nOldBkMode );
	m_RGW.m_bHelperRenderingGroupArea = false;
}

/////////////////////////////////////////////////////////////////////////////
// CExtReportGridColumn cell

CExtReportGridColumn::CExtReportGridColumn(
	CExtGridDataProvider * pDataProvider // = NULL
	)
	: CExtGridCellHeader( pDataProvider )
	, m_pRGW( NULL )
	, m_bHelperIsActive( true )
	, m_bGroupingEnabled( true )
	, m_bSortingAscending( true )
	, m_bCtxMenuVisible( true )
	, m_bCtxMenuAlignmentCommandsEnabled( true )
	, m_nColumnListMenuID( 0 )
{
	ExtentSet( 10, -1 );
	ExtentSet( 10, 0 );
}

CExtReportGridColumn::CExtReportGridColumn( const CExtGridCell & other )
	: CExtGridCellHeader( other )
	, m_pRGW( NULL )
	, m_bHelperIsActive( true )
	, m_bGroupingEnabled( true )
	, m_bSortingAscending( true )
	, m_nColumnListMenuID( 0 )
{
}

CExtReportGridColumn::~CExtReportGridColumn()
{
}

#ifdef _DEBUG

void CExtReportGridColumn::AssertValid() const
{
	CExtGridCellHeader::AssertValid();
}

void CExtReportGridColumn::Dump( CDumpContext & dc ) const
{
	CExtGridCellHeader::Dump( dc );
}

#endif // _DEBUG

void CExtReportGridColumn::Assign( const CExtGridCell & other )
{
	CExtGridCellHeader::Assign( other );
CExtReportGridColumn * pCell =
		DYNAMIC_DOWNCAST(
			CExtReportGridColumn,
			( const_cast < CExtGridCell * > ( &other ) )
			);
	if( pCell != NULL )
	{
// 		if( m_pRGW != NULL )
// 		{
// 			ASSERT_VALID( m_pRGW );
// 			ColumnNameSet( pCell->ColumnNameGet() );
// 			CategoryNameSet( pCell->CategoryNameGet() );
// 		} // if( m_pRGW != NULL )
		m_strCategoryName = pCell->m_strCategoryName;
		m_bHelperIsActive = pCell->m_bHelperIsActive;
		m_bGroupingEnabled = pCell->m_bGroupingEnabled;
		m_bSortingAscending = pCell->m_bSortingAscending;
		m_bCtxMenuVisible = pCell->m_bCtxMenuVisible;
		m_bCtxMenuAlignmentCommandsEnabled = pCell->m_bCtxMenuAlignmentCommandsEnabled;
		m_nColumnListMenuID = pCell->m_nColumnListMenuID;
	} // if( pCell != NULL )
	else
	{
		m_strCategoryName = _T("");
		m_bHelperIsActive = true;
		m_bGroupingEnabled = true;
		m_bSortingAscending = true;
		m_bCtxMenuVisible = true;
		m_bCtxMenuAlignmentCommandsEnabled = true;
		m_nColumnListMenuID = 0;
	} // else from if( pCell != NULL )
}

void CExtReportGridColumn::Serialize( CArchive & ar )
{
	CExtGridCellHeader::Serialize( ar );
DWORD dwFlags = 0;
	if( ar.IsStoring() )
	{
		ar << m_strCategoryName;
		if( m_bHelperIsActive )
			dwFlags |= 0x00000001;
		if( m_bGroupingEnabled )
			dwFlags |= 0x00000002;
		if( m_bSortingAscending )
			dwFlags |= 0x00000004;
		if( m_bCtxMenuVisible )
			dwFlags |= 0x00000008;
		if( m_bCtxMenuAlignmentCommandsEnabled )
			dwFlags |= 0x00000010;
		ar << dwFlags;
	} // if( ar.IsStoring() )
	else
	{
		m_nColumnListMenuID = 0;
		ar >> m_strCategoryName;
		ar >> dwFlags;
		m_bHelperIsActive =
			( ( dwFlags & 0x00000001 ) != 0 ) ? true : false;
		m_bGroupingEnabled =
			( ( dwFlags & 0x00000002 ) != 0 ) ? true : false;
		m_bSortingAscending =
			( ( dwFlags & 0x00000004 ) != 0 ) ? true : false;
		m_bCtxMenuVisible =
			( ( dwFlags & 0x00000008 ) != 0 ) ? true : false;
		m_bCtxMenuAlignmentCommandsEnabled =
			( ( dwFlags & 0x00000010 ) != 0 ) ? true : false;
	} // else from if( ar.IsStoring() )
}

__EXT_MFC_SAFE_LPCTSTR CExtReportGridColumn::ColumnNameGet() const
{
	ASSERT_VALID( this );
	//ASSERT_VALID( m_pRGW );
__EXT_MFC_SAFE_LPCTSTR strColumnName = GetTextBuffer();
	return ( strColumnName == NULL ) ? _T("") : strColumnName;
}

void CExtReportGridColumn::TextSet(
	__EXT_MFC_SAFE_LPCTSTR str, // = __EXT_MFC_SAFE_LPCTSTR(NULL), // empty text
	bool bAllowChangeDataType // = false
	)
{
	ASSERT_VALID( this );
	//ASSERT_VALID( m_pRGW );
	if( str == NULL )
		str	= _T("");
	if( m_pRGW != NULL )
	{
		ASSERT_VALID( m_pRGW );
		CExtSafeString strColumnNameOld = ColumnNameGet();
		CExtSafeString strCategoryNameOld = CategoryNameGet();
		m_pRGW->ReportColumnRename(
			LPCTSTR(strColumnNameOld),
			LPCTSTR(strCategoryNameOld),
			str,
			CategoryNameGet()
			);
	} // if( m_pRGW != NULL )
	CExtGridCellHeader::TextSet( str, bAllowChangeDataType );
}

void CExtReportGridColumn::ColumnNameSet(
	__EXT_MFC_SAFE_LPCTSTR strColumnName
	)
{
	ASSERT_VALID( this );
	//ASSERT_VALID( m_pRGW );
	if( strColumnName == NULL )
		strColumnName = _T("");
	TextSet( strColumnName );
}

__EXT_MFC_SAFE_LPCTSTR CExtReportGridColumn::CategoryNameGet() const
{
	ASSERT_VALID( this );
	//ASSERT_VALID( m_pRGW );
	if( m_strCategoryName.IsEmpty() )
		return _T("");
	return LPCTSTR(m_strCategoryName);
}

void CExtReportGridColumn::CategoryNameSet(
	__EXT_MFC_SAFE_LPCTSTR strCategoryName
	)
{
	ASSERT_VALID( this );
	//ASSERT_VALID( m_pRGW );
	if( strCategoryName == NULL )
		strCategoryName = _T("");
	if( m_pRGW != NULL )
	{
		ASSERT_VALID( m_pRGW );
		CExtSafeString strColumnNameOld = ColumnNameGet();
		CExtSafeString strCategoryNameOld = CategoryNameGet();
		m_pRGW->ReportColumnRename(
			LPCTSTR(strColumnNameOld),
			LPCTSTR(strCategoryNameOld),
			ColumnNameGet(),
			strCategoryName
			);
	} // if( m_pRGW != NULL )
	m_strCategoryName = strCategoryName;
}

bool CExtReportGridColumn::ColumnIsActive() const
{
	ASSERT_VALID( this );
	return m_bHelperIsActive;
}

void CExtReportGridColumn::InitNames(
	__EXT_MFC_SAFE_LPCTSTR strColumnName,
	__EXT_MFC_SAFE_LPCTSTR strCategoryName
	)
{
	ASSERT_VALID( this );
	if( strColumnName == NULL )
		strColumnName = _T("");
	if( strCategoryName == NULL )
		strCategoryName = _T("");
	CExtGridCellHeader::TextSet( strColumnName );
	m_strCategoryName = strCategoryName;
}

CExtReportGridWnd * CExtReportGridColumn::GetReportGrid() const
{
	ASSERT_VALID( this );
	return m_pRGW;
}

bool CExtReportGridColumn::CtxMenuVisibleGet() const
{
	ASSERT_VALID( this );
	return m_bCtxMenuVisible;
}
void CExtReportGridColumn::CtxMenuVisibleSet(
	bool bSet // = true
	)
{
	ASSERT_VALID( this );
	m_bCtxMenuVisible = bSet;
}

bool CExtReportGridColumn::CtxMenuAlignmentCommandsEnabledGet() const
{
	ASSERT_VALID( this );
	return m_bCtxMenuAlignmentCommandsEnabled;
}

void CExtReportGridColumn::CtxMenuAlignmentCommandsEnabledSet(
	bool bSet // = true
	)
{
	ASSERT_VALID( this );
	m_bCtxMenuAlignmentCommandsEnabled = bSet;
}

bool CExtReportGridColumn::GroupingEnabledGet() const
{
	ASSERT_VALID( this );
	if( ! SortingEnabledGet() )
		return false;
	return m_bGroupingEnabled;
}

void CExtReportGridColumn::GroupingEnabledSet(
	bool bGroupingEnabled // = true
	)
{
	ASSERT_VALID( this );
	if(		( m_bGroupingEnabled && bGroupingEnabled )
		||	( (!m_bGroupingEnabled) && (!bGroupingEnabled) )
		)
		return;
	m_bGroupingEnabled = bGroupingEnabled;
}

bool CExtReportGridColumn::SortingEnabledGet() const
{
	ASSERT_VALID( this );
	if( (GetStyleEx()&__EGCS_EX_DISABLE_SORT) == 0 )
		return true;
	return false;
}

void CExtReportGridColumn::SortingEnabledSet(
	bool bSortingEnabled // = true
	)
{
	ASSERT_VALID( this );
	if( bSortingEnabled )
		ModifyStyleEx( 0, __EGCS_EX_DISABLE_SORT );
	else
		ModifyStyleEx( __EGCS_EX_DISABLE_SORT );
}

bool CExtReportGridColumn::DragDropEnabledGet() const
{
	ASSERT_VALID( this );
	if( (GetStyleEx()&__EGCS_EX_DISABLE_START_DRAG) == 0 )
		return true;
	return false;
}

void CExtReportGridColumn::DragDropEnabledSet(
	bool bDragDropEnabled // = true
	)
{
	ASSERT_VALID( this );
	if( bDragDropEnabled )
		ModifyStyleEx( 0, __EGCS_EX_DISABLE_START_DRAG );
	else
		ModifyStyleEx( __EGCS_EX_DISABLE_START_DRAG );
}

bool CExtReportGridColumn::SortingAscendingGet() const
{
	ASSERT_VALID( this );
	return m_bSortingAscending;
}

void CExtReportGridColumn::SortingAscendingSet(
	bool bSortingAscending // = true
	)
{
	ASSERT_VALID( this );
// 	if(		( m_bSortingAscending && bSortingAscending )
// 		||	( (!m_bSortingAscending) && (!bSortingAscending) )
// 		)
// 		return;
	m_bSortingAscending = bSortingAscending;
}

bool CExtReportGridColumn::OnCellPressingLeave(
	CExtGridWnd & wndGrid,
	const CExtGridHitTestInfo & htInfo,
	UINT nChar, // VK_LBUTTON, VK_RBUTTON or VK_MBUTTON only
	UINT nFlags, // mouse event flags
	bool bPressedEvent,
	bool bStartDragEvent
	)
{
	ASSERT_VALID( this );
	if( bStartDragEvent )
		return false;
	if(		bStartDragEvent
		||	(! bPressedEvent)
		||	nChar != VK_LBUTTON
		||	(nFlags&MK_CONTROL) != 0
		||	CExtPopupMenuWnd::IsKeyPressed( VK_MENU )
		||	htInfo.GetInnerOuterTypeOfColumn() != 0
		||	htInfo.GetInnerOuterTypeOfRow() != -1
		||	(! wndGrid.IsKindOf( RUNTIME_CLASS( CExtReportGridWnd ) ) )
//		||	(! SortingEnabledGet() )
		)
		return true;
CExtReportGridWnd * pRGW =
		STATIC_DOWNCAST( CExtReportGridWnd, (&wndGrid) );
	ASSERT_VALID( pRGW );
bool bShift = ( (nFlags&MK_SHIFT) != 0 ) ? true : false;
CExtReportGridSortOrder _rgso = pRGW->ReportSortOrderGet();
LONG nExistingIndex = _rgso.ColumnGetIndexOf( this );
	if( ! bShift )
	{
		CExtReportGridSortOrder _rgsoCompletelyNew;
		LONG nColumnGroupCount = _rgso.ColumnGroupCountGet();
		if( nColumnGroupCount > 0 )
		{
			_rgsoCompletelyNew = _rgso;
			LONG nColumnCount = _rgso.ColumnCountGet();
			ASSERT( nColumnCount >= nColumnGroupCount );
			LONG nCountToRemove = nColumnCount - nColumnGroupCount;
			if( nCountToRemove > 0 )
			{
				VERIFY(
					_rgsoCompletelyNew.ColumnRemove(
						nColumnGroupCount,
						nCountToRemove
						)
					);
			} // if( nCountToRemove > 0 )
		} // if( nColumnGroupCount > 0 )
		bool bAscending = true;
		if( nExistingIndex >= 0 )
			bAscending = ! SortingAscendingGet();
		_rgsoCompletelyNew.ColumnInsert( this, -1, bAscending );
		pRGW->ReportSortOrderSet( _rgsoCompletelyNew, true );
		return true;
	} // if( ! bShift )
	if( nExistingIndex < 0 )
		_rgso.ColumnInsert( this, -1, true );
	else
		_rgso.ColumnSetAt( this, nExistingIndex, ! SortingAscendingGet() );
	pRGW->ReportSortOrderSet( _rgso, true );
	return true;
}

CSize CExtReportGridColumn::OnCalcSizeInGroupArea() const
{
	ASSERT_VALID( this );
INT nItemExtent = 0;
	if( ! ExtentGet( nItemExtent, 0 ) )
		nItemExtent = 75;
CSize _sizeMeasure = MeasureCell( m_pRGW );
	nItemExtent = max( nItemExtent, _sizeMeasure.cx );
INT nItemHeight = 0;
	if( m_pRGW != NULL )
	{
		ASSERT_VALID( m_pRGW );
		nItemHeight = m_pRGW->OuterRowHeightGet( true, 0L, false );
	} // if( m_pRGW != NULL )
	else
		nItemHeight = 23;
	return CSize( nItemExtent, nItemHeight );
}

void CExtReportGridColumn::OnPaintSortArrow(
	const RECT & rcSortArrow,
	const CExtGridWnd & wndGrid,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&wndGrid) );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( ! dc.RectVisible(&rcSortArrow) )
		return;
	nVisibleColNo;
	nVisibleRowNo;
	nColNo;
	nRowNo;
	nColType;
	nRowType;
	rcCellExtra;
	rcCell;
	rcVisibleRange;
	dwAreaFlags;
	if( (dwHelperPaintFlags&__EGCPF_OUTER_DND) != 0 )
		return;
	if( nColType == 0 && nRowType == 0 )
		return;
DWORD dwCellStyle = GetStyle();
	if( (dwCellStyle&__EGCS_SORT_ARROW) == 0 )
		return;
DWORD dwCellStyleEx = GetStyleEx();
	if(		(dwCellStyleEx&__EGCS_EX_DO_NOT_PAINT_SORT_ARROW) != 0
		&&	(dwAreaFlags&__EGBWA_OUTER_CELLS) != 0
		)
		return;
CExtPaintManager * pPM = wndGrid.PmBridge_GetPM();
	if( (wndGrid.SiwGetStyleEx()&__EGWS_EX_PM_COLORS) != 0 )
	{
		if( pPM->ReportGrid_PaintHeaderSortArrow(
				dc,
				rcSortArrow,
				( (dwCellStyle&__EGCS_SORT_DESCENDING) != 0 ) ? false : true,
				(CObject*)this
				)
			)
			return;
	} // if( (wndGrid.SiwGetStyleEx()&__EGWS_EX_PM_COLORS) != 0 )
CExtPaintManager::glyph_t * pSortArrowGlyph = NULL;
	if( (dwCellStyle&__EGCS_SORT_DESCENDING) != 0 )
	{
		pSortArrowGlyph =
			CExtPaintManager::g_DockingCaptionGlyphs[
				(INT) CExtPaintManager::__DCBT_ARROW_SORT_DOWN
				];
	} // if( (dwCellStyle&__EGCS_SORT_DESCENDING) != 0 )
	else
	{
		pSortArrowGlyph =
			CExtPaintManager::g_DockingCaptionGlyphs[
				(INT) CExtPaintManager::__DCBT_ARROW_SORT_UP
				];
	} // else from if( (dwCellStyle&__EGCS_SORT_DESCENDING) != 0 )
	ASSERT( pSortArrowGlyph != NULL );
COLORREF ColorValues[4] =
{
	RGB(0,0,0),
	::GetSysColor( COLOR_3DSHADOW ),
	::GetSysColor( COLOR_3DHIGHLIGHT ),
	::GetSysColor( COLOR_3DDKSHADOW ),
};
	pPM->PaintGlyphCentered(
		dc,
		rcSortArrow,
		*pSortArrowGlyph,
		ColorValues
		);
}

bool CExtReportGridColumn::OnPaintBackground(
	const CExtGridWnd & wndGrid,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
bool bBackgroundDrawn = false;
	if(		m_pRGW != NULL
		&&	( (dwHelperPaintFlags&__EGCPF_HIGHLIGHTED_BY_PRESSED_COLUMN) == 0 )
		)
	{
		if( (dwHelperPaintFlags&__EGCPF_OUTER_DND) != 0 )
			m_pRGW->m_bHelperRenderingOuterDND = true;
		if( m_pRGW->OnReportGridPaintHeaderRowBackground(
				dc,
				rcCell,
				(CObject*)this,
				LPARAM(dwHelperPaintFlags)
				)
			)
		{
			m_pRGW->m_bHelperRenderingOuterDND = false;
			return true;
		}
		m_pRGW->m_bHelperRenderingOuterDND = false;
		if( ( m_pRGW->ReportGridGetStyle() & __ERGS_LAYOUT_AND_STYLE_2003 ) != 0 )
		{
			bBackgroundDrawn = true;
			LONG nColCount = m_pRGW->ColumnCountGet();
			if( nColCount > 0 )
			{
				if( nColNo < ( nColCount - 1 ) )
				{
					CRect rc(
						rcCell.right - 1,
						rcCell.top + 2,
						rcCell.right,
						rcCell.bottom - 4
						);
					dc.FillSolidRect(
						&rc,
						dc.GetNearestColor( RGB(172,168,153) )
						);
				}
				else if( (m_pRGW->BseGetStyleEx()&__EGBS_BSE_EX_PROPORTIONAL_COLUMN_WIDTHS) == 0 )
				{
					CRect rc(
						rcCell.right - 1,
						rcCell.top + 2,
						rcCell.right,
						rcCell.bottom - 4
						);
					dc.FillSolidRect(
						&rc,
						dc.GetNearestColor( RGB(172,168,153) )
						);
					rc.OffsetRect( 1, 0 );
					dc.FillSolidRect(
						&rc,
						dc.GetNearestColor( RGB(255,255,255) )
						);
				}
				if( nColNo >= 0 )
				{
					CRect rc(
						rcCell.left,
						rcCell.top + 2,
						rcCell.left + 1,
						rcCell.bottom - 4
						);
					dc.FillSolidRect(
						&rc,
						dc.GetNearestColor( RGB(255,255,255) )
						);
				}
			}
		} // if( ( m_pRGW->ReportGridGetStyle() & __ERGS_LAYOUT_AND_STYLE_2003 ) != 0 )
	} // if( m_pRGW != NULL )
	if( ! bBackgroundDrawn )
	{
		dc.FillSolidRect(
			&rcCell,
			::GetSysColor( COLOR_3DFACE )
			);
		dc.Draw3dRect(
			&rcCell,
			::GetSysColor( COLOR_3DHIGHLIGHT ),
			::GetSysColor( COLOR_3DSHADOW )
			);
	} // if( ! bBackgroundDrawn )
bool bRetVal =
		CExtGridCellHeader::OnPaintBackground(
			wndGrid,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
	return bRetVal;
}

COLORREF CExtReportGridColumn::OnQueryTextColor(
	const CExtGridWnd & wndGrid,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
	if( (wndGrid.SiwGetStyleEx()&__EGWS_EX_PM_COLORS) != 0 )
	{
		COLORREF clr =
			wndGrid.PmBridge_GetPM()->
				ReportGrid_GetHeaderTextColor(
					(CObject*)this,
					LPARAM(dwHelperPaintFlags)
					);
		if( clr != COLORREF(-1L) )
			return clr;
	} // if( (wndGrid.SiwGetStyleEx()&__EGWS_EX_PM_COLORS) != 0 )
COLORREF clr =
		CExtGridCellHeader::OnQueryTextColor(
			wndGrid,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			dwAreaFlags,
			dwHelperPaintFlags
			);
	return clr;
}

bool CExtReportGridColumn::OnClick(
	CExtGridWnd & wndGrid,
	const CExtGridHitTestInfo & htInfo,
	UINT nChar, // VK_LBUTTON, VK_RBUTTON or VK_MBUTTON only
	UINT nRepCnt, // 0 - button up, 1 - single click, 2 - double click, 3 - post single click & begin editing
	UINT nFlags // mouse event flags
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&wndGrid) );
	ASSERT( ! htInfo.IsHoverEmpty() );
	ASSERT( htInfo.IsValidRect() );
	ASSERT( nChar == VK_LBUTTON || nChar == VK_RBUTTON || nChar == VK_MBUTTON );
	ASSERT( 0 <= nRepCnt && nRepCnt <= 3 );
	if( CExtGridCellHeader::OnClick(
			wndGrid,
			htInfo,
			nChar,
			nRepCnt,
			nFlags
			)
		)
		return true;
	if(		m_pRGW != NULL
		&&	nChar == VK_RBUTTON
		&&	( nRepCnt == 1 || nRepCnt == 2 )
		&&	( nFlags & ( MK_SHIFT | MK_CONTROL ) ) == 0
		)
	{
		ASSERT( LPVOID(m_pRGW) == LPVOID(&wndGrid) );
		CRect rcItemScreen = htInfo.m_rcItem;
		CRect rcItemExtraScreen = htInfo.m_rcExtra;
		CPoint ptScreen = htInfo.m_ptClient;
		wndGrid.ClientToScreen( &ptScreen );
		wndGrid.ClientToScreen( &rcItemScreen );
		wndGrid.ClientToScreen( &rcItemExtraScreen );
		if( m_pRGW->OnReportGridColumnCtxMenuTrack(
				m_pRGW,
				this,
				ptScreen,
				rcItemScreen,
				rcItemExtraScreen
				)
			)
			return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// CExtReportGridItem cell

CExtReportGridItem::CExtReportGridItem(
	CExtGridDataProvider * pDataProvider // = NULL
	)
	: CExtTreeGridCellNode( pDataProvider )
	, m_pRGW( NULL )
	, m_pCellPreviewArea( NULL )
{
}

//CExtReportGridItem::CExtReportGridItem( const CExtGridCell & other )
//	: CExtTreeGridCellNode( other )
//{
//}

CExtReportGridItem::~CExtReportGridItem()
{
	PreviewAreaDestroy();
}

#ifdef _DEBUG

void CExtReportGridItem::AssertValid() const
{
	CExtTreeGridCellNode::AssertValid();
}

void CExtReportGridItem::Dump( CDumpContext & dc ) const
{
	CExtTreeGridCellNode::Dump( dc );
}

#endif // _DEBUG

void CExtReportGridItem::Assign( const CExtGridCell & other )
{
	PreviewAreaDestroy();
	CExtTreeGridCellNode::Assign( other );
CExtReportGridItem * pCell =
		DYNAMIC_DOWNCAST(
			CExtReportGridItem,
			( const_cast < CExtGridCell * > ( &other ) )
			);
	if( pCell != NULL )
	{
		CExtGridCell * pCellPreviewAreaOther =
			pCell->PreviewAreaGet();
		if( pCellPreviewAreaOther != NULL )
		{
			ASSERT_VALID( pCellPreviewAreaOther );
			ASSERT_VALID( m_pRGW );
			CExtGridDataProvider & _TmpDP = m_pRGW->GetReportData()._Tree_GetCacheDP();
			CExtGridDataProviderMemory * pMemDP =
			STATIC_DOWNCAST( CExtGridDataProviderMemory, (&_TmpDP) );
			IMalloc * pMalloc = pMemDP->_DVM_GetCellAllocator();
			ASSERT( pMalloc != NULL );
			m_pCellPreviewArea =
				pCellPreviewAreaOther->Clone( pMalloc );
			ASSERT_VALID( m_pCellPreviewArea );
		}
	} // if( pCell != NULL )
}

void CExtReportGridItem::Serialize( CArchive & ar )
{
	CExtTreeGridCellNode::Serialize( ar );
DWORD dwTmp = 0;
	if( ar.IsStoring() )
	{
		if( m_pCellPreviewArea != NULL )
		{
			dwTmp |= 0x01;
			ar << dwTmp;
			VERIFY(
				CExtGridCell::InstanceSave(
					*m_pCellPreviewArea,
					ar,
					true
					)
				);
		}
		else
			ar << dwTmp;
	} // if( ar.IsStoring() )
	else
	{
		PreviewAreaDestroy();
		ar >> dwTmp;
		if( (dwTmp&0x01) != 0 )
		{
			ASSERT_VALID( m_pRGW );
			CExtGridDataProvider & _TmpDP = m_pRGW->GetReportData()._Tree_GetCacheDP();
			CExtGridDataProviderMemory * pMemDP =
			STATIC_DOWNCAST( CExtGridDataProviderMemory, (&_TmpDP) );
			IMalloc * pMalloc = pMemDP->_DVM_GetCellAllocator();
			ASSERT( pMalloc != NULL );
			m_pCellPreviewArea =
				CExtGridCell::InstanceLoad(
					ar,
					pMalloc,
					true
					);
			ASSERT_VALID( m_pCellPreviewArea );
		} // if( (dwTmp&0x01) != 0 )
	} // else from if( ar.IsStoring() )
}

ULONG CExtReportGridItem::TreeNodeCalcOffset(
	bool bExpandedOnly
	) const
{
	ASSERT_VALID( this );
	if( m_pRGW != NULL )
	{
		ASSERT_VALID( m_pRGW );
		const CExtReportGridSortOrder & _rgso = m_pRGW->ReportSortOrderGet();
		if( _rgso.ColumnGroupCountGet() == 0 )
			return TreeNodeGetSiblingIndex();
	} // if( m_pRGW != NULL )
	return CExtTreeGridCellNode::TreeNodeCalcOffset( bExpandedOnly );
}

void CExtReportGridItem::PreviewAreaDestroy()
{
	ASSERT_VALID( this );
	if( m_pCellPreviewArea == NULL )
		return;
	if( m_pRGW == NULL )
	{
		ASSERT( m_pCellPreviewArea == NULL );
		return;
	}
	ASSERT_VALID( m_pRGW );
CExtGridDataProvider * pCDP =
		m_pCellPreviewArea->DataProviderGet();
	ASSERT_VALID( pCDP );
CExtGridDataProviderMemory * pMemDP =
		STATIC_DOWNCAST( CExtGridDataProviderMemory, pCDP );
IMalloc * pMalloc = pMemDP->_DVM_GetCellAllocator();
	ASSERT( pMalloc != NULL );
CExtGridCell * pCellPreviewArea = m_pCellPreviewArea;
	m_pCellPreviewArea = NULL;
	CExtGridCell::InstanceDestroy(
		pCellPreviewArea,
		pMalloc,
		false
		);
}

CExtGridCell * CExtReportGridItem::PreviewAreaGet(
	CRuntimeClass * pInitRTC // = NULL
	)
{
	ASSERT_VALID( this );
	if(		m_pCellPreviewArea == NULL
		&&	pInitRTC == NULL
		)
		return NULL;
	ASSERT_VALID( m_pRGW );
	if( m_pCellPreviewArea != NULL )
	{
		ASSERT_VALID( m_pCellPreviewArea );
		return m_pCellPreviewArea;
	}
	if( pInitRTC == NULL )
		return NULL;
CExtGridDataProvider & _TmpDP = m_pRGW->GetReportData()._Tree_GetCacheDP();
CExtGridDataProviderMemory * pMemDP =
		STATIC_DOWNCAST( CExtGridDataProviderMemory, (&_TmpDP) );
IMalloc * pMalloc = pMemDP->_DVM_GetCellAllocator();
	ASSERT( pMalloc != NULL );
	m_pCellPreviewArea =
		CExtGridCell::InstanceCreate(
			pInitRTC,
			pMalloc,
			false
			);
	if( m_pCellPreviewArea != NULL )
	{
		ASSERT_VALID( m_pCellPreviewArea );
		m_pCellPreviewArea->DataProviderSet( pMemDP );
	}
	m_pCellPreviewArea->ModifyStyleEx( 0, __EGCS_EX_EMPTY );
	return m_pCellPreviewArea;
}

bool CExtReportGridItem::OnQueryExtraSpace(
	INT & nItemExtraSpace,
	bool bGet,
	bool bAfter // true - extra space after, false - after before
	)
{
	ASSERT_VALID( this );
	nItemExtraSpace = 0;
	if( ! bGet )
		return false;
	if( m_pRGW == NULL )
		return false;
	ASSERT_VALID( m_pRGW );
	nItemExtraSpace = m_pRGW->OnReportGridQueryItemExtraSpace( this, bAfter );
	return true;
//	CExtTreeGridCellNode::OnQueryExtraSpace(
//		nItemExtraSpace,
//		bGet,
//		bAfter
//		);
}

/////////////////////////////////////////////////////////////////////////////
// CExtReportGridDataProvider

IMPLEMENT_DYNCREATE( CExtReportGridDataProvider, CExtTreeGridDataProvider );

CExtReportGridDataProvider::CExtReportGridDataProvider()
{
	m_nTreeReservedColumns = _Tree_ReservedColumnsGet();
	ASSERT( m_nTreeReservedColumns >= 1 );
	m_nTreeReservedRows = _Tree_ReservedRowsGet();
	ASSERT( m_nTreeReservedRows >= 1 );
	m_pTreeNodeDefaultRTC = RUNTIME_CLASS( CExtReportGridItem );
}

CExtReportGridDataProvider::~CExtReportGridDataProvider()
{
}

#ifdef _DEBUG

void CExtReportGridDataProvider::AssertValid() const
{
	CExtTreeGridDataProvider::AssertValid();
	ASSERT( m_nTreeReservedColumns >= 1 );
	ASSERT( m_nTreeReservedRows >= 1 );
}

void CExtReportGridDataProvider::Dump( CDumpContext & dc ) const
{
	CExtTreeGridDataProvider::Dump( dc );
}

#endif // _DEBUG

CExtReportGridSortOrder & CExtReportGridDataProvider::ReportSortOrderGet()
{
	ASSERT_VALID( this );
	return m_rgso;
}

bool CExtReportGridDataProvider::ReportSortOrderSet(
	const CExtReportGridSortOrder & _rgso,
	bool bUpdateSortOrder,
	CExtReportGridWnd * pRGW
	)
{
	ASSERT_VALID( this );
	if( LPCVOID(&m_rgso) == LPCVOID(&_rgso) )
		return true;
	if( ! ReportSortOrderVerify( _rgso, pRGW ) )
		return false;
	if( m_rgso == _rgso )
		return true;
	m_rgso = _rgso;
	if( ! bUpdateSortOrder )
		return true;
bool bRetVal = ReportSortOrderUpdate( NULL );
	return bRetVal;
}

bool CExtReportGridDataProvider::ReportSortOrderUpdate(
	CExtReportGridWnd * pRGW
	)
{
	ASSERT_VALID( this );
	// update sort arrow cell styles
ULONG nRgsoColumnCount = ULONG( m_rgso.ColumnCountGet() );
ULONG nIndex, nColumnCountActive = ActiveColumnCountGet();
	for( nIndex = 0; nIndex < nColumnCountActive; nIndex ++ )
	{
		CExtReportGridColumn * pRGC = ActiveColumnGet( nIndex );
		ASSERT_VALID( pRGC );
		LONG nSortIndex = -1L;
		if( nRgsoColumnCount > 0 )
			nSortIndex = m_rgso.ColumnGetIndexOf( pRGC );
		if( nSortIndex >= 0 )
		{
			pRGC->ModifyStyle( __EGCS_SORT_ARROW );
			bool bAscending = false;
			m_rgso.ColumnGetAt( nSortIndex, &bAscending );
			if( bAscending )
				pRGC->ModifyStyle( 0, __EGCS_SORT_DESCENDING );
			else
				pRGC->ModifyStyle( __EGCS_SORT_DESCENDING );
		} // if( nSortIndex >= 0 )
		else
		{
			pRGC->ModifyStyle( 0, __EGCS_SORT_ARROW );
		} // else from if( nSortIndex >= 0 )
	} // for( nIndex = 0; nIndex < nColumnCountActive; nIndex ++ )
ULONG nColumnCountInactive = InactiveColumnCountGet();
	for( nIndex = 0; nIndex < nColumnCountInactive; nIndex ++ )
	{
		CExtReportGridColumn * pRGC = InactiveColumnGet( nIndex );
		ASSERT_VALID( pRGC );
		LONG nSortIndex = -1L;
		if( nRgsoColumnCount > 0 )
			nSortIndex = m_rgso.ColumnGetIndexOf( pRGC );
		if( nSortIndex >= 0 )
		{
			pRGC->ModifyStyle( __EGCS_SORT_ARROW );
			bool bAscending = false;
			m_rgso.ColumnGetAt( nSortIndex, &bAscending );
			if( bAscending )
				pRGC->ModifyStyle( 0, __EGCS_SORT_DESCENDING );
			else
				pRGC->ModifyStyle( __EGCS_SORT_DESCENDING );
		} // if( nSortIndex >= 0 )
		else
		{
			pRGC->ModifyStyle( 0, __EGCS_SORT_ARROW );
		} // else from if( nSortIndex >= 0 )
	} // for( nIndex = 0; nIndex < nColumnCountInactive; nIndex ++ )


// CExtGridDataProvider::IDataProviderEvents * pDPE = NULL;
// bool bColumns = false;
// 	if( pDPE != NULL )
// 		pDPE->OnDataProviderSortEnter( bColumns );
// LONG nSwapCounter = 0L;
// bool bRetVal =
// 		m_DP._SortStep(
// 			bColumns,
// 			pDPE,
// 			_gdso,
// 			nSwapCounter,
// 			nLow,
// 			nHigh
// 			);
// 	if( pDPE != NULL )
// 		pDPE->OnDataProviderSortLeave( bColumns );
//	return bRetVal;

CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
CExtTreeGridCellNode * pNodeRoot = TreeNodeGetRoot();
	ASSERT_VALID( pNodeRoot );
	// generate the plain matrix
CExtTreeGridCellNode * pNodePrev = NULL;
ULONG nRowNo = nReservedRowCount, nRowCount = _DP.RowCountGet();
ULONG nContentWeight = nRowCount - nRowNo;
ULONG nContentWeightInitial = nContentWeight;
	pNodeRoot->m_arrChildren.RemoveAll();
	m_arrGridRef.RemoveAll();
	m_arrGridVis.RemoveAll();
	pNodeRoot->m_arrChildren.SetSize( nContentWeight );
	m_arrGridRef.SetSize( nContentWeight );
	m_arrGridVis.SetSize( nContentWeight );
	for( ; nRowNo < nRowCount; )
	{
		CExtGridCell * pCellTmp =
			_DP.CellGet(
				m_nTreeReservedColumns - 1,
				nRowNo,
				NULL,
				false,
				false
				);
		ASSERT_VALID( pCellTmp );
		ASSERT_KINDOF( CExtReportGridItem, pCellTmp );
		CExtReportGridItem * pNode =
			STATIC_DOWNCAST( CExtReportGridItem, pCellTmp );
		if( pNode->TreeNodeGetChildCount() > 0 )
		{
			nRowCount --;
			nContentWeight --;
			_DP.RowRemove( nRowNo, 1 );
			continue;
		} // if( pNode->TreeNodeGetChildCount() > 0 )
		pNode->m_nOptIndex = ULONG( nRowNo - nReservedRowCount );
		pNodeRoot->m_arrChildren.SetAt(
			int( pNode->m_nOptIndex ),
			pNode
			);
		m_arrGridRef.SetAt(
			int( nRowNo - nReservedRowCount ),
			pNode
			);
		m_arrGridVis.SetAt(
			int( nRowNo - nReservedRowCount ),
			pNode
			);
		pNode->m_arrChildren.RemoveAll();
		pNode->m_pNodeParent = pNodeRoot;
		pNode->m_pNodeNext = NULL;
		pNode->m_pNodePrev = pNodePrev;
		pNode->m_nContentWeightAll = 0;
		pNode->m_nContentWeightExpanded = 0;
		if( pNodePrev != NULL )
			pNodePrev->m_pNodeNext = pNode;
		pNodePrev = pNode;
		nRowNo ++;
	} // for( ; nRowNo < nRowCount; )
	pNodeRoot->m_nContentWeightAll
		= pNodeRoot->m_nContentWeightExpanded
		= nContentWeight;
	ASSERT( nContentWeightInitial >= nContentWeight );
ULONG nCountToRemove = nContentWeightInitial - nContentWeight;
	if( nCountToRemove > 0 )
	{
		pNodeRoot->m_arrChildren.RemoveAt( nContentWeight, nCountToRemove );
		m_arrGridRef.RemoveAt( nContentWeight, nCountToRemove );
		m_arrGridVis.RemoveAt( nContentWeight, nCountToRemove );
	} // if( nCountToRemove > 0 )

	if( nRgsoColumnCount > 0 )
	{
		LONG nColumnGroupCount = m_rgso.ColumnGroupCountGet();
		CArray < ULONG, ULONG > arrGroupIndices;
		arrGroupIndices.SetSize( int( nColumnGroupCount ) );
		// sort the plain matrix
		CExtGridDataSortOrder _gdso, _gdsoEmpty;
		ULONG nRgsoIndex;
		for( nRgsoIndex = 0; nRgsoIndex < nRgsoColumnCount; nRgsoIndex ++ )
		{
			bool bAscending = false;
			const CExtReportGridColumn * pRGC =
				m_rgso.ColumnGetAt(
					LONG(nRgsoIndex),
					&bAscending
					);
			ASSERT_VALID( pRGC );
			bool bActive = pRGC->ColumnIsActive();
			ULONG nColNo = 0;
			if( bActive )
			{
				nColNo = ActiveIndexGet( pRGC );
				if( nColNo == ULONG(-1L) )
					return NULL;
				ULONG nColCountInactive = InactiveColumnCountGet();
				nColNo += nColCountInactive;
			} // if( bActive )
			else
			{
				nColNo = InactiveIndexGet( pRGC );
				if( nColNo == ULONG(-1L) )
					return NULL;
			} // else from if( bActive )
			nColNo += m_nTreeReservedColumns;
			CExtGridDataSortOrder::ITEM_INFO _soii(
				nColNo,
				bAscending
				);
			_gdso.m_arrItems.Add( _soii );
			if( nRgsoIndex < ULONG(nColumnGroupCount) )
				arrGroupIndices.SetAt( ULONG( nRgsoIndex ), ULONG( nColNo ) ); 
		} // for( nRgsoIndex = 0; nRgsoIndex < nRgsoColumnCount; nRgsoIndex ++ )
		_DP.CacheReserveForOuterCells( 0, nReservedRowCount );
		_DP.SortOrderSet( _gdso, false, NULL );
		_DP.SortOrderSet( _gdsoEmpty, false, NULL ); // reset inner sort order to empty
		_DP.CacheReserveForOuterCells( nReservedColumnCount, nReservedRowCount );
		// reset node states (all root children)
		pNodePrev = NULL;
		nRowNo = nReservedRowCount;
		nRowCount = _DP.RowCountGet();
		nContentWeight = nRowCount - nRowNo;
		pNodeRoot->m_nContentWeightAll
			= pNodeRoot->m_nContentWeightExpanded
			= nContentWeight;
		pNodeRoot->m_arrChildren.RemoveAll();
		m_arrGridRef.RemoveAll();
		m_arrGridVis.RemoveAll();
		pNodeRoot->m_arrChildren.SetSize( nContentWeight );
		m_arrGridRef.SetSize( nContentWeight );
		m_arrGridVis.SetSize( nContentWeight );
		for( ; nRowNo < nRowCount; nRowNo ++ )
		{
			CExtGridCell * pCellTmp =
				_DP.CellGet(
					m_nTreeReservedColumns - 1,
					nRowNo,
					NULL,
					false,
					false
					);
			ASSERT_VALID( pCellTmp );
			ASSERT_KINDOF( CExtReportGridItem, pCellTmp );
			CExtReportGridItem * pNode =
				STATIC_DOWNCAST( CExtReportGridItem, pCellTmp );
			pNode->m_nOptIndex = ULONG( nRowNo - nReservedRowCount );
			pNodeRoot->m_arrChildren.SetAt(
				int( pNode->m_nOptIndex ),
				pNode
				);
			m_arrGridRef.SetAt(
				int( nRowNo - nReservedRowCount ),
				pNode
				);
			m_arrGridVis.SetAt(
				int( nRowNo - nReservedRowCount ),
				pNode
				);
			pNode->m_arrChildren.RemoveAll();
			pNode->m_pNodeParent = pNodeRoot;
			pNode->m_pNodeNext = NULL;
			pNode->m_pNodePrev = pNodePrev;
			pNode->m_nContentWeightAll = 0;
			pNode->m_nContentWeightExpanded = 0;
			if( pNodePrev != NULL )
				pNodePrev->m_pNodeNext = pNode;
			pNodePrev = pNode;
		} // for( ; nRowNo < nRowCount; nRowNo ++ )
		if( nColumnGroupCount >= 0 )
		{ // build the tree
			CExtTreeGridCellNode * pDNL = NULL, * pCW = NULL;
			CExtTreeGridCellNode * pPN = pNodeRoot; // parent navigator
			pNodeRoot->m_nContentWeightAll = 0;
			pNodeRoot->m_nContentWeightExpanded = 0;
			pNodeRoot->m_arrChildren.RemoveAll();
			ULONG nInitIndex;
			CTypedPtrArray < CPtrArray, CExtGridCell * > arrLevels;
			arrLevels.SetSize( int( nColumnGroupCount ) );
			nRowNo = nReservedRowCount;
			for( ; nRowNo < nRowCount; )
			{
				CExtGridCell * pCellTmp =
					_DP.CellGet(
						m_nTreeReservedColumns - 1,
						nRowNo,
						NULL,
						false,
						false
						);
				ASSERT_VALID( pCellTmp );
				ASSERT_KINDOF( CExtReportGridItem, pCellTmp );
				CExtReportGridItem * pNode =
					STATIC_DOWNCAST( CExtReportGridItem, pCellTmp );
				pNode->m_nContentWeightAll = 0;
				pNode->m_nContentWeightExpanded = 0;
				pNode->m_arrChildren.RemoveAll();
				if( nRowNo == nReservedRowCount )
				{ // init arrLevels, init first tree branch
					CExtTreeGridCellNode * pNodeParent = pNodeRoot;
					for( nInitIndex = 0; nInitIndex < ULONG(nColumnGroupCount); nInitIndex ++ )
					{
						ULONG nMappedColumnIndex = arrGroupIndices[ nInitIndex ];
						CExtGridCell * pCell =
							_DP.CellGet(
								nMappedColumnIndex,
								nRowNo,
								RUNTIME_CLASS(CExtGridCell), //NULL,
								false,
								false
								);
						ASSERT_VALID( pCell );
						arrLevels.SetAt( int( nInitIndex ), pCell );
						_DP.RowInsert( nRowNo, 1L );
						CExtReportGridItem * pCellInitNode =
							STATIC_DOWNCAST(
								CExtReportGridItem,
								_DP.CellGet(
									m_nTreeReservedColumns - 1,
									nRowNo,
									RUNTIME_CLASS(CExtReportGridItem),
									false,
									false
									)
								);
						pCellInitNode->m_pRGW = pRGW;
						m_arrGridVis.InsertAt( int( nRowNo - nReservedRowCount ), pCellInitNode );
						m_arrGridRef.InsertAt( int( nRowNo - nReservedRowCount ), pCellInitNode );
						pCellInitNode->m_pNodeParent = pNodeParent;
						pCellInitNode->m_pNodeNext = NULL;
						pCellInitNode->m_pNodePrev = NULL;

						pCellInitNode->m_bExpanded = true;
						pCW = pCellInitNode->m_pNodeParent;
						pCellInitNode->m_nOptIndex = ULONG( pCW->m_arrChildren.GetSize() );
						pCW->m_arrChildren.Add( pCellInitNode );
						for( ; pCW != NULL; pCW = pCW->m_pNodeParent )
						{
							ASSERT_VALID( pCW );
							pCW->m_nContentWeightAll ++;
							pCW->m_nContentWeightExpanded ++;
						}

						nRowNo ++;
						nRowCount ++;
						pNodeParent = pCellInitNode;
					} // for( nInitIndex = 0; nInitIndex < ULONG(nColumnGroupCount); nInitIndex ++ )
					pNode->m_pNodeParent = pPN = pNodeParent;
					pNode->m_pNodeNext = NULL;
					pNode->m_pNodePrev = NULL;
					
					pCW = pNode->m_pNodeParent;
					pNode->m_nOptIndex = ULONG( pCW->m_arrChildren.GetSize() );
					pCW->m_arrChildren.Add( pNode );
					for( ; pCW != NULL; pCW = pCW->m_pNodeParent )
					{
						ASSERT_VALID( pCW );
						pCW->m_nContentWeightAll ++;
						pCW->m_nContentWeightExpanded ++;
					}

					pDNL = pNode;
					nRowNo ++;
					continue;
				} // init arrLevels, init first tree branch

				// compute the matching level
				ULONG nMatchingLevel = 0;
				for( nInitIndex = 0; nInitIndex < ULONG(nColumnGroupCount); nInitIndex ++ )
				{
					ULONG nMappedColumnIndex = arrGroupIndices[ nInitIndex ];
					CExtGridCell * pCell =
						_DP.CellGet(
							nMappedColumnIndex,
							nRowNo,
							RUNTIME_CLASS(CExtGridCell), //NULL,
							false,
							false
							);
					ASSERT_VALID( pCell );
					CExtGridCell * pCellCmp = arrLevels[ nInitIndex ];
					ASSERT_VALID( pCellCmp );
					int nCmpResult = pCellCmp->Compare( *pCell );
					if( nCmpResult != 0 )
						break;
					nMatchingLevel ++;
				} // for( nInitIndex = 0; nInitIndex < ULONG(nColumnGroupCount); nInitIndex ++ )

				// analyze the matching level
				ASSERT( nMatchingLevel <= ULONG(nColumnGroupCount) );
				if( nMatchingLevel == ULONG(nColumnGroupCount) )
				{
					pNode->m_pNodeParent = pPN;
					pNode->m_pNodeNext = NULL;
					pNode->m_pNodePrev = pDNL;
					pDNL->m_pNodeNext = pNode;
					pDNL = pNode;

					pCW = pNode->m_pNodeParent;
					pNode->m_nOptIndex = ULONG( pCW->m_arrChildren.GetSize() );
					pCW->m_arrChildren.Add( pNode );
					for( ; pCW != NULL; pCW = pCW->m_pNodeParent )
					{
						ASSERT_VALID( pCW );
						pCW->m_nContentWeightAll ++;
						pCW->m_nContentWeightExpanded ++;
					}

					nRowNo ++;
					continue;
				} // if( nMatchingLevel == nColumnGroupCount )

				// jump pPN to matching level
				ULONG nDiffML = nColumnGroupCount - nMatchingLevel;
				for( nInitIndex = 0; nInitIndex < nDiffML; nInitIndex ++ )
				{
					pPN = pPN->m_pNodeParent;
					ASSERT_VALID( pPN );
				} // for( nInitIndex = 0; nInitIndex < nDiffML; nInitIndex ++ )

				// insert required branch
				for( nInitIndex = nMatchingLevel; nInitIndex < ULONG(nColumnGroupCount); nInitIndex ++ )
				{
					ULONG nMappedColumnIndex = arrGroupIndices[ nInitIndex ];
					CExtGridCell * pCell =
						_DP.CellGet(
							nMappedColumnIndex,
							nRowNo,
							RUNTIME_CLASS(CExtGridCell), //NULL,
							false,
							false
							);
					ASSERT_VALID( pCell );
					arrLevels.SetAt( int( nInitIndex ), pCell );
					_DP.RowInsert( nRowNo, 1L );
					CExtReportGridItem * pCellInitNode =
						STATIC_DOWNCAST(
							CExtReportGridItem,
							_DP.CellGet(
								m_nTreeReservedColumns - 1,
								nRowNo,
								RUNTIME_CLASS(CExtReportGridItem),
								false,
								false
								)
							);
					pCellInitNode->m_pRGW = pRGW;
					m_arrGridVis.InsertAt( int( nRowNo - nReservedRowCount ), pCellInitNode );
					m_arrGridRef.InsertAt( int( nRowNo - nReservedRowCount ), pCellInitNode );
					pCellInitNode->m_pNodeParent = pPN;
					pCellInitNode->m_pNodeNext = NULL;
					pCellInitNode->m_pNodePrev = NULL;

					if( nInitIndex == nMatchingLevel )
					{
						LONG nCC = LONG( pPN->m_arrChildren.GetSize() );
						if( nCC > 0 )
						{
							CExtTreeGridCellNode * pPrev = pPN->m_arrChildren[ nCC - 1 ];
							ASSERT_VALID( pPrev );
							pPrev->m_pNodeNext = pCellInitNode;
							pCellInitNode->m_pNodePrev = pPrev;
						}
					}

					pCellInitNode->m_bExpanded = true;
					pCW = pCellInitNode->m_pNodeParent;
					pCellInitNode->m_nOptIndex = ULONG( pCW->m_arrChildren.GetSize() );
					pCW->m_arrChildren.Add( pCellInitNode );
					for( ; pCW != NULL; pCW = pCW->m_pNodeParent )
					{
						ASSERT_VALID( pCW );
						pCW->m_nContentWeightAll ++;
						pCW->m_nContentWeightExpanded ++;
					}

					nRowNo ++;
					nRowCount ++;
					pPN = pCellInitNode;
				} // for( nInitIndex = nMatchingLevel; nInitIndex < ULONG(nColumnGroupCount); nInitIndex ++ )
				pNode->m_pNodeParent = pPN;
				pNode->m_pNodeNext = NULL;
				pNode->m_pNodePrev = NULL;

				pCW = pNode->m_pNodeParent;
				pNode->m_nOptIndex = ULONG( pCW->m_arrChildren.GetSize() );
				pCW->m_arrChildren.Add( pNode );
				for( ; pCW != NULL; pCW = pCW->m_pNodeParent )
				{
					ASSERT_VALID( pCW );
					pCW->m_nContentWeightAll ++;
					pCW->m_nContentWeightExpanded ++;
				}

				pDNL = pNode;
				nRowNo ++;
			} // for( ; nRowNo < nRowCount; )
		} // build the tree
	} // if( nRgsoColumnCount > 0 )
	return true;
}

bool CExtReportGridDataProvider::ReportSortOrderVerify(
	const CExtReportGridSortOrder & _rgso,
	CExtReportGridWnd * pMsgRGW
	) const
{
	ASSERT_VALID( this );
LONG nIndex, nColumnCount = _rgso.ColumnCountGet(),
		nColumnGroupCount = _rgso.ColumnGroupCountGet();
	for( nIndex = 0; nIndex < nColumnCount; nIndex ++ )
	{
		const CExtReportGridColumn * pRGC = _rgso.ColumnGetAt( nIndex );
		ASSERT_VALID( pRGC );
		if( nIndex < nColumnGroupCount )
		{
			if( ! pRGC->GroupingEnabledGet() )
			{
				if( pMsgRGW != NULL )
				{
					ASSERT_VALID( pMsgRGW );
					ASSERT( pMsgRGW->GetSafeHwnd() );
					pMsgRGW->OnReportGridSortOrderVerificationFailed(
						_rgso,
						nIndex,
						true
						);
				}
				return false;
			}
		}
		if( ! pRGC->SortingEnabledGet() )
		{
			if( ! pRGC->GroupingEnabledGet() )
			{
				if( pMsgRGW != NULL )
				{
					ASSERT_VALID( pMsgRGW );
					ASSERT( pMsgRGW->GetSafeHwnd() );
					pMsgRGW->OnReportGridSortOrderVerificationFailed(
						_rgso,
						nIndex,
						false
						);
				}
				return false;
			}
			return false;
		}
		bool bActive = pRGC->ColumnIsActive();
		if( bActive )
		{
			ULONG nDpIndex = ActiveIndexGet( pRGC );
			if( nDpIndex == ULONG(-1L) )
				return false;
		} // if( bActive )
		else
		{
			ULONG nDpIndex = InactiveIndexGet( pRGC );
			if( nDpIndex == ULONG(-1L) )
				return false;
		} // else from if( bActive )
	}  // for( nIndex = 0; nIndex < nColumnCount; nIndex ++ )
	return true;
}

bool CExtReportGridDataProvider::SortOrderGet(
	CExtGridDataSortOrder & _gdso,
	bool bColumns // true = sort order for columns, false - for rows
	) const
{
	ASSERT_VALID( this );
	if( ! bColumns )
		return false;
	_gdso.Empty();
	return true;
}

bool CExtReportGridDataProvider::SwapDroppedSeries(
	bool bColumns, // true = swap columns, false - rows
	ULONG nRowColNoSrc,
	ULONG nRowColNoDropBefore,
	CExtGridDataProvider::IDataProviderEvents * pDPE // = NULL
	)
{
	ASSERT_VALID( this );
	if( ! bColumns )
		return false;
 	nRowColNoSrc = _Tree_MapColToCache( nRowColNoSrc );
 	nRowColNoDropBefore = _Tree_MapColToCache( nRowColNoDropBefore );
	return
		m_DP.SwapDroppedSeries(
			bColumns, // true = swap columns, false - rows
			nRowColNoSrc,
			nRowColNoDropBefore,
			pDPE
			);
}

ULONG CExtReportGridDataProvider::InactiveColumnCountGet() const
{
	ASSERT_VALID( this );
ULONG nInactiveColumnCount = _Tree_ReservedColumnsGet();
	ASSERT( nInactiveColumnCount >= m_nTreeReservedColumns );
	nInactiveColumnCount -= m_nTreeReservedColumns;
	return nInactiveColumnCount;
}

ULONG CExtReportGridDataProvider::ActiveColumnCountGet() const
{
	ASSERT_VALID( this );
ULONG nActiveColumnCount = ColumnCountGet();
	return nActiveColumnCount;
}

bool CExtReportGridDataProvider::ActivateColumn(
	ULONG nIndexInactive,
	ULONG nIndexActive // = ULONG(-1L) // ULONG(-1L) - last position
	)
{
	ASSERT_VALID( this );
ULONG nActiveColumnCount = ActiveColumnCountGet();
	if( nIndexActive == ULONG(-1L) )
		nIndexActive = nActiveColumnCount;
	else if( nIndexActive > nActiveColumnCount )
		return false;
ULONG nInactiveColumnCount = InactiveColumnCountGet();
	if( nIndexInactive >= nInactiveColumnCount )
		return false;
bool bRetVal =
		_Tree_ReservedColumnsMoveFromReserve(
			nIndexInactive + m_nTreeReservedColumns,
			1,
			nIndexActive
			);
	return bRetVal;
}

void CExtReportGridDataProvider::RemoveReportColumn( CExtReportGridColumn * pRGC )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGC );
CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
LONG nRgcIndex = _rgso.ColumnGetIndexOf( pRGC );
	if( nRgcIndex >= 0 )
	{
		LONG nRgcGroupCount = _rgso.ColumnGroupCountGet();
		if( nRgcGroupCount > 0 && nRgcIndex < nRgcGroupCount )
			_rgso.ColumnGroupCountSet( nRgcGroupCount - 1 );
		_rgso.ColumnRemove( pRGC );
	} // if( nRgcIndex >= 0 )
ULONG nIndexActive = ActiveIndexGet( pRGC );
ULONG nIndexInactive = InactiveIndexGet( pRGC );
	ASSERT(
			( nIndexActive != ULONG(-1L) && nIndexInactive == ULONG(-1L) )
		||	( nIndexActive == ULONG(-1L) && nIndexInactive != ULONG(-1L) )
		);
	if( nIndexActive != ULONG(-1L) )
	{ // removing active column
		ULONG nColCountInactive = InactiveColumnCountGet();
		VERIFY(
			m_DP.ColumnRemove(
				m_nTreeReservedColumns
					+ nColCountInactive
					+ nIndexActive
				)
			);
	} // removing active column
	else
	{ // removing inactive column
		ActivateColumn( nIndexInactive, 0 );
#ifdef _DEBUG
		nIndexActive = ActiveIndexGet( pRGC );
		nIndexInactive = InactiveIndexGet( pRGC );
		ASSERT(
			( nIndexActive == 0L && nIndexInactive == ULONG(-1L) )
			);
#endif // _DEBUG
		ULONG nColCountInactive = InactiveColumnCountGet();
		VERIFY(
			m_DP.ColumnRemove(
				m_nTreeReservedColumns
					+ nColCountInactive
				)
			);
	} // removing inactive column
}

bool CExtReportGridDataProvider::DeactivateColumn(
	ULONG nIndexActive,
	ULONG nIndexInactive // = ULONG(-1L) // ULONG(-1L) - last position
	)
{
	ASSERT_VALID( this );
ULONG nActiveColumnCount = ActiveColumnCountGet();
	if( nIndexActive >= nActiveColumnCount )
		return false;
ULONG nInactiveColumnCount = InactiveColumnCountGet();
	if( nIndexInactive == ULONG(-1L) )
		nIndexInactive = nInactiveColumnCount;
	else if( nIndexInactive > nInactiveColumnCount )
		return false;
bool bRetVal =
		_Tree_ReservedColumnsMoveToReserve(
			nIndexActive,
			1,
			nIndexInactive + m_nTreeReservedColumns
			);
	return bRetVal;
}

ULONG CExtReportGridDataProvider::ActiveIndexGet(
	const CExtReportGridColumn * pRGC
	) const
{
	ASSERT_VALID( this );
	if( pRGC == NULL )
		return ULONG(-1L);
	ASSERT_VALID( pRGC );
ULONG nColCountActive = ActiveColumnCountGet();
ULONG nColCountInactive = InactiveColumnCountGet();
ULONG nRowNo = _Tree_MapRowToCache( 0L );
ULONG nColNo;
	for( nColNo = 0L; nColNo < nColCountActive; nColNo ++ )
	{
		const CExtGridCell * pCell =
			( (CExtGridDataProvider *) ( &m_DP ) ) -> CellGet(
				nColNo + m_nTreeReservedColumns + nColCountInactive,
				nRowNo
				);
		if( LPVOID(pCell) == LPVOID(pRGC) )
			return nColNo;
	} // for( nColNo = 0L; nColNo < nColCountActive; nColNo ++ )
	return ULONG(-1L);
}

ULONG CExtReportGridDataProvider::InactiveIndexGet(
	const CExtReportGridColumn * pRGC
	) const
{
	ASSERT_VALID( this );
	if( pRGC == NULL )
		return ULONG(-1L);
	ASSERT_VALID( pRGC );
ULONG nColCountInactive = InactiveColumnCountGet();
ULONG nRowNo = _Tree_MapRowToCache( 0L );
ULONG nColNo;
	for( nColNo = 0L; nColNo < nColCountInactive; nColNo ++ )
	{
		const CExtGridCell * pCell =
			( (CExtGridDataProvider *) ( &m_DP ) ) -> CellGet(
				nColNo + m_nTreeReservedColumns,
				nRowNo
				);
		if( LPVOID(pCell) == LPVOID(pRGC) )
			return nColNo;
	} // for( nColNo = 0L; nColNo < nColCountInactive; nColNo ++ )
	return ULONG(-1L);
}

CExtReportGridColumn * CExtReportGridDataProvider::ActiveColumnGet(
	ULONG nIndex
	)
{
	ASSERT_VALID( this );
ULONG nColCountActive = ActiveColumnCountGet();
	if( nIndex >= nColCountActive )
		return NULL;
ULONG nColCountInactive = InactiveColumnCountGet();
ULONG nRowNo = _Tree_MapRowToCache( 0L );
CExtGridCell * pCell =
		m_DP.CellGet(
			nIndex + m_nTreeReservedColumns + nColCountInactive,
			nRowNo
			);
	ASSERT_VALID( pCell );
CExtReportGridColumn * pRGC =
		STATIC_DOWNCAST( CExtReportGridColumn, pCell );
	return pRGC;
}

const CExtReportGridColumn * CExtReportGridDataProvider::ActiveColumnGet(
	ULONG nIndex
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridDataProvider * > ( this ) )
		-> ActiveColumnGet( nIndex );
}

CExtReportGridColumn * CExtReportGridDataProvider::InactiveColumnGet(
	ULONG nIndex
	)
{
	ASSERT_VALID( this );
ULONG nColCountInactive = InactiveColumnCountGet();
	if( nIndex >= nColCountInactive )
		return NULL;
ULONG nRowNo = _Tree_MapRowToCache( 0L );
CExtGridCell * pCell =
		m_DP.CellGet(
			nIndex + m_nTreeReservedColumns,
			nRowNo
			);
	ASSERT_VALID( pCell );
CExtReportGridColumn * pRGC =
		STATIC_DOWNCAST( CExtReportGridColumn, pCell );
	return pRGC;
}

const CExtReportGridColumn * CExtReportGridDataProvider::InactiveColumnGet(
	ULONG nIndex
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridDataProvider * > ( this ) )
		-> InactiveColumnGet( nIndex );
}

CExtGridCell * CExtReportGridDataProvider::ReportItemGetCell(
	const CExtReportGridColumn * pRGC,
	const CExtReportGridItem * pRGI,
	CRuntimeClass * pRTC // = NULL
	)
{
	ASSERT_VALID( this );
	if( pRGC == NULL || pRGI == NULL )
		return NULL;
	ASSERT_VALID( pRGC );
	ASSERT_VALID( pRGI );

bool bActive = pRGC->ColumnIsActive();
ULONG nColNo = 0;
	if( bActive )
	{
		nColNo = ActiveIndexGet( pRGC );
		if( nColNo == ULONG(-1L) )
			return NULL;
		ULONG nColCountInactive = InactiveColumnCountGet();
		nColNo += nColCountInactive;
	} // if( bActive )
	else
	{
		nColNo = InactiveIndexGet( pRGC );
		if( nColNo == ULONG(-1L) )
			return NULL;
	} // else from if( bActive )

	ASSERT( pRGI->TreeNodeGetParent() != NULL );
ULONG nRowNo = pRGI->TreeNodeCalcOffset( false );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
//	nColNo += nReservedColumnCount;
	nRowNo += nReservedRowCount;
	nColNo += m_nTreeReservedColumns;
//	nRowNo += m_nTreeReservedRows;
	return
		_DP.CellGet(
			nColNo,
			nRowNo,
			pRTC,
			false, // bAutoFindValue
			false  // bUseColumnDefaultValue
			);
}

const CExtGridCell * CExtReportGridDataProvider::ReportItemGetCell(
	const CExtReportGridColumn * pRGC,
	const CExtReportGridItem * pRGI,
	CRuntimeClass * pRTC // = NULL
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridDataProvider * > ( this ) )
		-> ReportItemGetCell(
			pRGC,
			pRGI,
			pRTC
			);
}

bool CExtReportGridDataProvider::ReportItemSetCell(
	const CExtReportGridColumn * pRGC,
	const CExtReportGridItem * pRGI,
	const CExtGridCell * pCellNewValue, // = NULL // if NULL - empty existing cell values
	bool bReplace, // = false // false - assign to existing cell instances or column/row type created cells, true - create new cloned copies of pCellNewValue
	CRuntimeClass * pInitRTC // = NULL // runtime class for new cell instance (used if bReplace=false)
	)
{
	ASSERT_VALID( this );
	if( pRGC == NULL || pRGI == NULL )
		return false;
	ASSERT_VALID( pRGC );
	ASSERT_VALID( pRGI );

bool bActive = pRGC->ColumnIsActive();
ULONG nColNo = 0;
	if( bActive )
	{
		nColNo = ActiveIndexGet( pRGC );
		if( nColNo == ULONG(-1L) )
			return NULL;
		ULONG nColCountInactive = InactiveColumnCountGet();
		nColNo += nColCountInactive;
	} // if( bActive )
	else
	{
		nColNo = InactiveIndexGet( pRGC );
		if( nColNo == ULONG(-1L) )
			return NULL;
	} // else from if( bActive )

	ASSERT( pRGI->TreeNodeGetParent() != NULL );
ULONG nRowNo = pRGI->TreeNodeCalcOffset( false );
CExtGridDataProvider & _DP = _Tree_GetCacheDP();
ULONG nReservedColumnCount = 0, nReservedRowCount = 0;
	_DP.CacheReservedCountsGet( &nReservedColumnCount, &nReservedRowCount );
//	nColNo += nReservedColumnCount;
	nRowNo += nReservedRowCount;
	nColNo += m_nTreeReservedColumns;
//	nRowNo += m_nTreeReservedRows;
	return
		_DP.CellRangeSet(
			nColNo,
			nRowNo,
			1L,
			1L,
			pCellNewValue,
			bReplace,
			pInitRTC,
			false, // bAutoFindValue
			false  // bUseColumnDefaultValue
			);
}

/////////////////////////////////////////////////////////////////////////////
// CExtReportGridWnd window

CExtBitmap CExtReportGridWnd::g_bmpHeaderAreaBk;
RECT CExtReportGridWnd::g_rcHeaderAreaPadding = { 0, 0, 0, 4 };

IMPLEMENT_DYNCREATE( CExtReportGridWnd, CExtTreeGridWnd );		

CExtReportGridWnd::CExtReportGridWnd()
	: m_pCAB( NULL )
	, m_pCCW( NULL )
	, m_pGAW( NULL )
	, m_bCreateHelperInvoked( false )
	, m_nHelerLockSiwDrawFocusRect( 0L )
	, m_nHelerLockOnSwUpdateScrollBars( 0L )
	, m_nHelerLockDrawHL( 0L )
	, m_nHelerLockEnsureVisibleColumn( 0L )
	, m_nHelerLockEnsureVisibleRow( 0L )
	, m_dwReportGridStyle( __ERGS_DEFAULT )
	, m_dwReportGridStyleEx( __ERGS_EX_DEFAULT )
	, m_pCtxMenuRGC( NULL )
	, m_nMaxColCountForGrouping( 32767 )
	, m_nColumnListMenuStartID( 15000 )
	, m_bHelperUpdateAfterMenuClosed( false )
	, m_bHelperRenderingGroupArea( false )
	, m_bHelperRenderingOuterDND( false )
{
	m_strLocalCommandProfileName.Format(
		_T("Prof-UIS-Report-Grid-%p"),
		LPVOID(this)
		);
	if( g_bmpHeaderAreaBk.IsEmpty() )
	{
		static const BYTE g_arrHeaderAreaBk[]=
		{
			0x42,0x4D,0x46,0x02,0x00,0x00,0x00,0x00,0x00,0x00,0x36,0x00,0x00,0x00,0x28,0x00,0x00,0x00,0x08,0x00,0x00,0x00,0x16,0x00,
			0x00,0x00,0x01,0x00,0x18,0x00,0x00,0x00,0x00,0x00,0x10,0x02,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0xB2,0xC2,0xC5,0xB2,0xC2,0xC5,0xB2,0xC2,0xC5,0xB2,0xC2,0xC5,0xB2,0xC2,0xC5,0xB2,0xC2,0xC5,
			0xB2,0xC2,0xC5,0xB2,0xC2,0xC5,0xBE,0xCF,0xD2,0xBE,0xCF,0xD2,0xBE,0xCF,0xD2,0xBE,0xCF,0xD2,0xBE,0xCF,0xD2,0xBE,0xCF,0xD2,
			0xBE,0xCF,0xD2,0xBE,0xCF,0xD2,0xC8,0xD8,0xDC,0xC8,0xD8,0xDC,0xC8,0xD8,0xDC,0xC8,0xD8,0xDC,0xC8,0xD8,0xDC,0xC8,0xD8,0xDC,
			0xC8,0xD8,0xDC,0xC8,0xD8,0xDC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,
			0xD8,0xE9,0xEC,0xD8,0xE9,0xEC,0xEC
		};
		VERIFY(
			g_bmpHeaderAreaBk.LoadBMP_Buffer(
				g_arrHeaderAreaBk,
				sizeof(g_arrHeaderAreaBk)/sizeof(g_arrHeaderAreaBk[0])
				)
			);
	}
}

CExtReportGridWnd::~CExtReportGridWnd()
{
	ReportColumnUnRegister();
}

DWORD CExtReportGridWnd::ReportGridGetStyle() const
{
	ASSERT_VALID( this );
	return m_dwReportGridStyle;
}

DWORD CExtReportGridWnd::ReportGridModifyStyle(
	DWORD dwStyleAdd,
	DWORD dwStyleRemove, // = 0L
	bool bRedraw // = true
	)
{
	ASSERT( this != NULL );
	if( bRedraw )
	{
		if(		m_hWnd == NULL
			||	( ! ::IsWindow(m_hWnd) )
			)
			bRedraw = false;
	}
	if( bRedraw )
		SendMessage( WM_CANCELMODE );
DWORD dwReportGridStyleOld = m_dwReportGridStyle;
	m_dwReportGridStyle &= ~dwStyleRemove;
	m_dwReportGridStyle |= dwStyleAdd;
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw )
	return dwReportGridStyleOld;
}

DWORD CExtReportGridWnd::ReportGridGetStyleEx() const
{
	ASSERT_VALID( this );
	return m_dwReportGridStyleEx;
}

DWORD CExtReportGridWnd::ReportGridModifyStyleEx(
	DWORD dwStyleExAdd,
	DWORD dwStyleExRemove, // = 0L
	bool bRedraw // = true
	)
{
	ASSERT( this != NULL );
	if( bRedraw )
	{
		if(		m_hWnd == NULL
			||	( ! ::IsWindow(m_hWnd) )
			)
			bRedraw = false;
	}
	if( bRedraw )
		SendMessage( WM_CANCELMODE );
DWORD dwReportGridStyleExOld = m_dwReportGridStyleEx;
	m_dwReportGridStyleEx &= ~dwStyleExRemove;
	m_dwReportGridStyleEx |= dwStyleExAdd;
	if( bRedraw )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw )
	return dwReportGridStyleExOld;
}

CExtGridDataProvider & CExtReportGridWnd::OnGridQueryDataProvider()
{
	ASSERT_VALID( this );
	if( m_pDataProvider != NULL )
	{
		ASSERT_VALID( m_pDataProvider );
		return (*m_pDataProvider);
	} // if( m_pDataProvider != NULL )
	m_pDataProvider = new CExtReportGridDataProvider;
	ASSERT_VALID( m_pDataProvider );
	return (*m_pDataProvider);
}

CExtReportGridDataProvider & CExtReportGridWnd::GetReportData()
{
	ASSERT_VALID( this );
CExtGridDataProvider & _DP = OnGridQueryDataProvider();
	return (*(STATIC_DOWNCAST(CExtReportGridDataProvider,(&_DP))));
}

const CExtReportGridDataProvider & CExtReportGridWnd::GetReportData() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
			-> GetReportData();
}

CExtReportGridDataProvider & CExtReportGridWnd::_GetReportData() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
			-> GetReportData();
}

BEGIN_MESSAGE_MAP(CExtReportGridWnd, CExtTreeGridWnd)
	//{{AFX_MSG_MAP(CExtReportGridWnd)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE(
		CExtPopupBaseWnd::g_nMsgNotifyMenuClosed,
		_OnPopupMenuClosed
		)
	ON_COMMAND( ID_EXT_RG_CTX_SORT_ASCENDING, _OnRgColumnCtxCmdSortAscending )
	ON_UPDATE_COMMAND_UI( ID_EXT_RG_CTX_SORT_ASCENDING, _OnRgUpdateColumnCtxCmdSortAscending )
	ON_COMMAND( ID_EXT_RG_CTX_SORT_DESCENDING, _OnRgColumnCtxCmdSortDescending )
	ON_UPDATE_COMMAND_UI( ID_EXT_RG_CTX_SORT_DESCENDING, _OnRgUpdateColumnCtxCmdSortDescending )
	ON_COMMAND( ID_EXT_RG_CTX_GROUP_BY_CONTEXT_FIELD, _OnRgColumnCtxCmdGroupByContextField )
	ON_UPDATE_COMMAND_UI( ID_EXT_RG_CTX_GROUP_BY_CONTEXT_FIELD, _OnRgUpdateColumnCtxCmdGroupByContextField )
	ON_COMMAND( ID_EXT_RG_CTX_SHOW_GROUP_BY_BOX, _OnRgColumnCtxCmdShowHideGroupByBox )
	ON_UPDATE_COMMAND_UI( ID_EXT_RG_CTX_SHOW_GROUP_BY_BOX, _OnRgUpdateColumnCtxCmdShowHideGroupByBox )
	ON_COMMAND( ID_EXT_RG_CTX_REMOVE_COLUMN, _OnRgColumnCtxCmdRemoveColumn )
	ON_UPDATE_COMMAND_UI( ID_EXT_RG_CTX_REMOVE_COLUMN, _OnRgUpdateColumnCtxCmdRemoveColumn )
	ON_COMMAND( ID_EXT_RG_CTX_FIELD_CHOOSER, _OnRgColumnCtxCmdShowHideFieldChooser )
	ON_UPDATE_COMMAND_UI( ID_EXT_RG_CTX_FIELD_CHOOSER, _OnRgUpdateColumnCtxCmdShowHideFieldChooser )
	ON_COMMAND( ID_EXT_RG_CTX_BEST_FIT_CONTEXT_FIELD, _OnRgColumnCtxCmdBestFitContextField )
	ON_UPDATE_COMMAND_UI( ID_EXT_RG_CTX_BEST_FIT_CONTEXT_FIELD, _OnRgUpdateColumnCtxCmdBestFitContextField )
	ON_COMMAND( ID_EXT_RG_CTX_ALIGNMENT_LEFT, _OnRgColumnCtxCmdColumnAlignmentLeft )
	ON_UPDATE_COMMAND_UI( ID_EXT_RG_CTX_ALIGNMENT_LEFT, _OnRgUpdateColumnCtxCmdColumnAlignmentLeft )
	ON_COMMAND( ID_EXT_RG_CTX_ALIGNMENT_CENTER, _OnRgColumnCtxCmdColumnAlignmentCenter )
	ON_UPDATE_COMMAND_UI( ID_EXT_RG_CTX_ALIGNMENT_CENTER, _OnRgUpdateColumnCtxCmdColumnAlignmentCenter )
	ON_COMMAND( ID_EXT_RG_CTX_ALIGNMENT_RIGHT, _OnRgColumnCtxCmdColumnAlignmentRight )
	ON_UPDATE_COMMAND_UI( ID_EXT_RG_CTX_ALIGNMENT_RIGHT, _OnRgUpdateColumnCtxCmdColumnAlignmentRight )
	ON_COMMAND( ID_EXT_RG_CTX_ALIGNMENT_BY_TYPE, _OnRgColumnCtxCmdColumnAlignmentByType )
	ON_UPDATE_COMMAND_UI( ID_EXT_RG_CTX_ALIGNMENT_BY_TYPE, _OnRgUpdateColumnCtxCmdColumnAlignmentByType )
END_MESSAGE_MAP()

LONG CExtReportGridWnd::OnGridHookOuterCellDND(
	bool bFinal,
	LONG nExternalHT,
	const POINT & point,
	CExtGridCell * pCell,
	const CExtGridHitTestInfo & htInfo,
	CExtContentExpandWnd & wndArrows,
	CExtContentExpandWnd & wndDND
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pCell );
	if(		htInfo.IsHoverEmpty()
		||	htInfo.GetInnerOuterTypeOfColumn() != 0
		||	htInfo.GetInnerOuterTypeOfRow() != -1
		||	(! pCell->IsKindOf( RUNTIME_CLASS( CExtReportGridColumn ) ) )
		||	(! ReportGroupAreaIsVisible() )
		)
	{
		wndArrows.Deactivate();
		return -1L;
	}
CExtReportGridColumn * pRealRGC =
		STATIC_DOWNCAST( CExtReportGridColumn, pCell );
CExtReportGridGroupAreaWnd * pGAW = ReportGroupAreaGet();
	if( pGAW == NULL )
	{
		wndArrows.Deactivate();
		return -1L;
	}
	ASSERT_VALID( pGAW );
	if( pGAW->GetSafeHwnd() == NULL )
	{
		wndArrows.Deactivate();
		return -1L;
	}
	if( bFinal )
	{
		wndArrows.Deactivate();
		if( nExternalHT < 0 )
		{
			wndArrows.Deactivate();
			return -1L;
		}
		__EXT_MFC_SAFE_LPCTSTR strColumnName = pRealRGC->ColumnNameGet();
		__EXT_MFC_SAFE_LPCTSTR strCategoryName = pRealRGC->CategoryNameGet();
		bool bAscending = true;
		CExtReportGridSortOrder & _rgsoSrc =
			ReportSortOrderGet();
		CExtReportGridSortOrder _rgso = _rgsoSrc;
		LONG nColumnGroupCount = _rgso.ColumnGroupCountGet();
		ASSERT( nExternalHT <= nColumnGroupCount );
		LONG nExistingIndex = _rgso.ColumnGetIndexOf( pRealRGC );
		if( nExistingIndex >= 0 )
		{
			if( nExistingIndex < nColumnGroupCount )
			{
				if( nExternalHT > nExistingIndex )
					nExternalHT --;
				nColumnGroupCount --;
				_rgso.ColumnGroupCountSet( nColumnGroupCount );
#ifdef _DEBUG
				CExtReportGridColumn * pExistingRGC =
					_rgso.ColumnGetAt( nExistingIndex );
				ASSERT_VALID( pExistingRGC );
				ASSERT( LPVOID(pRealRGC) == LPVOID(pRealRGC) );
#endif // _DEBUG
			} // if( nExistingIndex < nColumnGroupCount )
			VERIFY( _rgso.ColumnRemove( nExistingIndex, 1 ) );
			bAscending = pRealRGC->SortingAscendingGet();
		} // if( nExistingIndex >= 0 )
		bAscending = pRealRGC->SortingAscendingGet();
		_rgso.ColumnInsert(
			pRealRGC,
			nExternalHT,
			bAscending
			);
		nColumnGroupCount ++;
		_rgso.ColumnGroupCountSet( nColumnGroupCount );
		if( ReportSortOrderSet( _rgso, true, false ) )
		{
			ReportColumnActivate(
				strColumnName,
				strCategoryName,
				false,
				0,
				true
				);
		}
	} // if( bFinal )
	else
	{
		CPoint ptGAW = point;
		ClientToScreen( &ptGAW );
		pGAW->ScreenToClient( &ptGAW );
		LONG nHT = pGAW->ItemDropHitTest( ptGAW );
		if( nHT == nExternalHT )
			return nHT;
		if( nHT < 0 )
		{
			wndArrows.Deactivate();
			return -1L;
		}
		CPoint ptTop( 0, 0 );
		INT nHeight = 0;
		if(	! pGAW->ItemDropMarkerGet(
				nHT,
				ptTop,
				nHeight
				)
			)
		{
			wndArrows.Deactivate();
			return -1L;
		}
		CRect rcArrows(
			ptTop.x,
			ptTop.y,
			ptTop.x,
			ptTop.y + nHeight
			);
		pGAW->ClientToScreen( &rcArrows );
		ScreenToClient( &rcArrows );
		VERIFY(
			wndArrows.Activate(
				rcArrows,
				this,
				__ECWAF_DRAW_RED_ARROWS
					|__ECWAF_TRANSPARENT_ITEM
					|__ECWAF_NO_CAPTURE
					|__ECWAF_REDIRECT_MOUSE
					|__ECWAF_REDIRECT_NO_DEACTIVATE
					|__ECWAF_REDIRECT_AND_HANDLE
					|__ECWAF_HANDLE_MOUSE_ACTIVATE
					|__ECWAF_MA_NOACTIVATE
				)
			);
		if( wndArrows.GetSafeHwnd() != NULL )
			wndArrows.SetWindowPos(
				&wndDND, 0, 0, 0, 0,
				SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE
				);
		return nHT;
	} // else from if( bFinal )
	return -1L;
}

bool CExtReportGridWnd::_CreateHelper()
{
	if( m_bCreateHelperInvoked )
		return true;
	m_bCreateHelperInvoked = true;
	if( ! CExtTreeGridWnd::_CreateHelper() )
		return false;
CExtReportGridColumnChooserWnd * pCCW = ReportColumnChooserGet();
	ASSERT_VALID( pCCW );
	pCCW;
CExtReportGridCategoryComboBox * pCAB = ReportCategoryComboBoxGet();
	ASSERT_VALID( pCAB );
	pCAB;
CExtReportGridGroupAreaWnd * pGAW = ReportGroupAreaGet();
	ASSERT_VALID( pGAW );
	pGAW;
	SiwModifyStyle(
		__ESIS_STH_NONE // __ESIS_STH_PIXEL
			|__ESIS_STV_ITEM
			|__EGBS_SFM_FULL_ROWS //|__EGBS_SFM_CELLS_V
				|__EGBS_MULTI_AREA_SELECTION
			|__EGBS_SUBTRACT_SEL_AREAS
			|__EGBS_NO_HIDE_SELECTION
			|__EGBS_RESIZING_CELLS_OUTER   //|__EGBS_DYNAMIC_RESIZING_H
			|__EGBS_GRIDLINES_H //|__EGBS_GRIDLINES
			//|__EGBS_LBEXT_SELECTION
			//|__ESIS_DISABLE_AUTOHIDE_SB
		,
		0,
		false
		);
	SiwModifyStyleEx(
		__EGBS_EX_CELL_TOOLTIPS_OUTER
			|__EGBS_EX_CELL_EXPANDING_INNER
			|__EGBS_EX_HVO_EVENT_CELLS|__EGBS_EX_HVO_HIGHLIGHT_CELL
			|__EGWS_EX_PM_COLORS,
		0,
		false
		);
	BseModifyStyle(
		__EGWS_BSE_SORT_COLUMNS|__EGWS_BSE_BUTTONS_PERSISTENT,
		__EGWS_BSE_DEFAULT,
		false
		);
	BseModifyStyleEx(
		__EGBS_BSE_EX_DRAG_COLUMNS|__EGBS_BSE_EX_DRAG_REMOVE_COLUMNS
			|__EGBS_BSE_EX_PROPORTIONAL_COLUMN_WIDTHS
			|__EGBS_BSE_EX_DBLCLK_BEST_FIT_COLUMN
			|__EGBS_BSE_EX_HIGHLIGHT_PRESSING_COLUMNS_OUTER|__EGBS_BSE_EX_HIGHLIGHT_PRESSING_STAY_COLUMNS,
		__EGWS_BSE_EX_DEFAULT,
		false
		);
	CExtTreeGridWnd::OuterRowCountTopSet( 1L, false );
	CExtTreeGridWnd::OuterRowHeightSet( true, 0, OuterRowHeightGet( true, 0, false ) + 3 );
	m_wndScrollBarH.m_eSO = CExtScrollBar::__ESO_BOTTOM;
	m_wndScrollBarV.m_eSO = CExtScrollBar::__ESO_RIGHT;
	if( ! m_wndScrollBarV.Create(
			WS_CHILD|WS_VISIBLE|SBS_VERT|SBS_RIGHTALIGN,
			CRect(0,0,0,0),
			this,
			1
			)
		)
	{
		ASSERT( FALSE );
		return false;
	}
	if( ! m_wndScrollBarH.Create(
			WS_CHILD|WS_VISIBLE|SBS_HORZ|SBS_BOTTOMALIGN,
			CRect(0,0,0,0),
			this,
			2
			)
		)
	{
		ASSERT( FALSE );
		return false;
	}
	m_wndScrollBarH.SyncReservedSpace( &m_wndScrollBarV );
	m_wndScrollBarV.SyncReservedSpace( &m_wndScrollBarH );
	OnSwRecalcLayout( true );
	return true;
}

CScrollBar* CExtReportGridWnd::GetScrollBarCtrl(int nBar) const
{
	ASSERT_VALID( this );
	if( m_hWnd == NULL || (! ::IsWindow(m_hWnd) ) )
		return NULL;
	ASSERT( nBar == SB_HORZ || nBar == SB_VERT );
	if( nBar == SB_HORZ )
	{
		if( m_wndScrollBarH.GetSafeHwnd() != NULL )
			return ( const_cast < CExtScrollBar * > ( &m_wndScrollBarH ) );
	} // if( nBar == SB_HORZ )
	else
	{
		if( m_wndScrollBarV.GetSafeHwnd() != NULL )
			return ( const_cast < CExtScrollBar * > ( &m_wndScrollBarV ) );
	} // else from if( nBar == SB_HORZ )
	return NULL;
}

void CExtReportGridWnd::PreSubclassWindow() 
{
	CExtTreeGridWnd::PreSubclassWindow();
	m_bHelperUpdateAfterMenuClosed = false;
	OnReportGridCommandProfileInit();
}

void CExtReportGridWnd::PostNcDestroy() 
{
	m_bHelperUpdateAfterMenuClosed = false;
	OnReportGridCommandProfileShutdown();
	m_bCreateHelperInvoked = false;
	m_pCAB = NULL; // must be self-deleting implementation
	m_pCCW = NULL; // must be self-deleting implementation
	m_pGAW = NULL; // must be self-deleting implementation
	CExtTreeGridWnd::PostNcDestroy();
}

LRESULT CExtReportGridWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if( message == WM_CREATE )
		if( ! _CreateHelper() )
			return 0;
LRESULT lResult = CExtTreeGridWnd::WindowProc( message, wParam, lParam );
	return lResult;
}

LONG CExtReportGridWnd::ReportSortOrderMaxGroupColumnCountGet() const
{
	ASSERT_VALID( this );
	ASSERT( m_nMaxColCountForGrouping >= 0 );
	return m_nMaxColCountForGrouping;
}

void CExtReportGridWnd::ReportSortOrderMaxGroupColumnCountSet(
	LONG nMaxColCount,
//	bool bAdjustSortOrderIfExceeded, // = true
	bool bRedrawIfAdjusted // = true
	)
{
	ASSERT_VALID( this );
	ASSERT( nMaxColCount >= 0 );
	m_nMaxColCountForGrouping = nMaxColCount;
//	if( ! bAdjustSortOrderIfExceeded )
//		return;
CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
LONG nColumnGroupCount = _rgso.ColumnGroupCountGet();
	if( nColumnGroupCount <= m_nMaxColCountForGrouping )
		return;
	_rgso.ColumnGroupCountSet( m_nMaxColCountForGrouping );
	if( bRedrawIfAdjusted && GetSafeHwnd() == NULL )
		bRedrawIfAdjusted = false;
	ReportSortOrderUpdate( bRedrawIfAdjusted );
}

void CExtReportGridWnd::OnReportMaxGroupColumnCountReached()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
LONG nMaxColCount = ReportSortOrderMaxGroupColumnCountGet();
CExtSafeString strMsg, strFormat;
	if( ! g_ResourceManager->LoadString(
			strFormat,
			IDS_EXT_RG_MAX_GROUP_COUNT_EXCEEDED_MSG_FORMAT
			)
		)
		strFormat = _T("You cannot group items by more than %d fields.");
	strMsg.Format(
		LPCTSTR(strFormat),
		INT(nMaxColCount)
		);
	::AfxMessageBox( LPCTSTR(strMsg), MB_OK|MB_ICONINFORMATION );
}

CExtReportGridSortOrder & CExtReportGridWnd::ReportSortOrderGet()
{
	ASSERT_VALID( this );
CExtReportGridDataProvider & _DP = _GetReportData();
CExtReportGridSortOrder & _rgso = _DP.ReportSortOrderGet();
	return _rgso;
}

const CExtReportGridSortOrder & CExtReportGridWnd::ReportSortOrderGet() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
		-> ReportSortOrderGet();
}

bool CExtReportGridWnd::ReportSortOrderSet(
	const CExtReportGridSortOrder & _rgso,
	bool bShowMsgBoxesIfFailed,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	if( ! ReportSortOrderVerify(
			_rgso,
			bShowMsgBoxesIfFailed
			)
		)
		return false;
LONG nIndex, nColumnGroupCount = _rgso.ColumnGroupCountGet();
	for( nIndex = 0; nIndex < nColumnGroupCount; nIndex ++ )
	{
		const CExtReportGridColumn * pRGC =
			_rgso.ColumnGetAt( nIndex );
		ASSERT_VALID( pRGC );
		ASSERT( pRGC->SortingEnabledGet() );
		if( pRGC->ColumnIsActive() )
		{
			__EXT_MFC_SAFE_LPCTSTR strColumnName = pRGC->ColumnNameGet();
			__EXT_MFC_SAFE_LPCTSTR strCategoryName = pRGC->CategoryNameGet();
			if( ! ReportColumnActivate(
					strColumnName,
					strCategoryName,
					false,
					-1L,
					false
					)
				)
				return false;
		} // if( pRGC->ColumnIsActive() )
	} // for( nIndex = 0; nIndex < nColumnGroupCount; nIndex ++ )
CExtReportGridDataProvider & _DP = _GetReportData();
	if( ! _DP.ReportSortOrderSet(
			_rgso,
			false,
			( bShowMsgBoxesIfFailed && GetSafeHwnd() != NULL ) ? this : NULL
			)
		)
		return false;
bool bRetVal = ReportSortOrderUpdate( bRedraw );
	return bRetVal;
}

bool CExtReportGridWnd::ReportSortOrderUpdate(
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	m_nHelerLockOnSwUpdateScrollBars ++;
	m_nHelerLockEnsureVisibleColumn ++;
	m_nHelerLockEnsureVisibleRow ++;
CExtReportGridDataProvider & _DP = _GetReportData();
HTREEITEM htiFocus = ItemFocusGet();
	SelectionUnset( false, false );
CPoint ptFocus = FocusGet();
	FocusUnset( false );
	if( htiFocus != NULL )
	{
		if( ItemGetChildCount(htiFocus) > 0 )
			htiFocus = NULL;
	}
	if( ! _DP.ReportSortOrderUpdate( this ) )
	{
		m_nHelerLockOnSwUpdateScrollBars --;
		m_nHelerLockEnsureVisibleColumn --;
		m_nHelerLockEnsureVisibleRow --;
		return false;
	}
	OnReportGridReportSortOrderUpdateComplete();
	if( GetSafeHwnd() == NULL )
	{
		m_nHelerLockOnSwUpdateScrollBars --;
		m_nHelerLockEnsureVisibleColumn --;
		m_nHelerLockEnsureVisibleRow --;
		return true;
	}
	if( htiFocus != NULL )
	{
		ItemFocusSet( htiFocus, ptFocus.x, false );
		SelectionUnset( true, false );
	}
	if( ! bRedraw )
	{
		m_nHelerLockOnSwUpdateScrollBars --;
		m_nHelerLockEnsureVisibleColumn --;
		m_nHelerLockEnsureVisibleRow --;
		return true;
	}
	if( htiFocus != NULL )
		ItemEnsureExpanded( htiFocus );
	m_nHelerLockOnSwUpdateScrollBars --;
	m_nHelerLockEnsureVisibleColumn --;
	m_nHelerLockEnsureVisibleRow --;
	if( htiFocus != NULL )
	{
		LONG i = ItemGetVisibleIndexOf( htiFocus );
		EnsureVisibleRow( i, false );
	}
	OnSwRecalcLayout( true );
	OnSwUpdateScrollBars();
//	ptFocus = FocusGet();
	if( ptFocus.x >= 0 )
		EnsureVisibleColumn( ptFocus.x, false );
// 	if( ptFocus.y >= 0 )
// 		EnsureVisibleRow( ptFocus.y, false );

CExtPopupMenuTipWnd * pATTW =
		OnAdvancedPopupMenuTipWndGet();
	if( pATTW != NULL )
		pATTW->Hide();
	if(		m_wndToolTip.GetSafeHwnd() != NULL
		&&	::IsWindow( m_wndToolTip.GetSafeHwnd() )
		)
		m_wndToolTip.DelTool( this, 1 );
	CWnd::CancelToolTips();

	OnSwDoRedraw();
	if( ReportGroupAreaIsVisible() )
	{
		CExtReportGridGroupAreaWnd * pGAW = ReportGroupAreaGet();
		if( pGAW != NULL )
		{
			ASSERT_VALID( pGAW );
			if( pGAW->GetSafeHwnd() != NULL )
			{
				pGAW->Invalidate();
			}
		} // if( pGAW != NULL )
	} // if( ReportGroupAreaIsVisible() )
	return true;
}

void CExtReportGridWnd::OnSwUpdateScrollBars()
{
	ASSERT_VALID( this );
	if( m_nHelerLockOnSwUpdateScrollBars != 0 )
		return;
	CExtTreeGridWnd::OnSwUpdateScrollBars();
}

bool CExtReportGridWnd::EnsureVisibleColumn(
	LONG nColNo,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	if( m_nHelerLockEnsureVisibleColumn != 0 )
		return false;
	return CExtTreeGridWnd::EnsureVisibleColumn( nColNo, bRedraw );
}

bool CExtReportGridWnd::EnsureVisibleRow(
	LONG nRowNo,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	if( m_nHelerLockEnsureVisibleRow != 0 )
		return false;
	return CExtTreeGridWnd::EnsureVisibleRow( nRowNo, bRedraw );
}

void CExtReportGridWnd::OnReportGridReportSortOrderUpdateComplete()
{
	ASSERT_VALID( this );
	if( (ReportGridGetStyle()&__ERGS_COLLAPSE_AFTER_REGROUPING) != 0 )
		ItemExpandAll( ItemGetRoot(), TVE_COLLAPSE, false );
}

bool CExtReportGridWnd::ReportSortOrderVerify(
	const CExtReportGridSortOrder & _rgso,
	bool bShowMsgBoxesIfFailed
	) const
{
	ASSERT_VALID( this );
CExtReportGridDataProvider & _DP = _GetReportData();
	if( ! _rgso.VerifyColumns( this ) )
		return false;
	if( ! _DP.ReportSortOrderVerify(
			_rgso,
			( bShowMsgBoxesIfFailed && GetSafeHwnd() != NULL )
				? ( const_cast < CExtReportGridWnd * > ( this ) )
				: NULL
			)
		)
		return false;
LONG nMaxColCount = ReportSortOrderMaxGroupColumnCountGet();
LONG nColumnGroupCount = _rgso.ColumnGroupCountGet();
	if( nColumnGroupCount >= nMaxColCount )
	{
		if( bShowMsgBoxesIfFailed && GetSafeHwnd() != NULL )
		{
			( const_cast < CExtReportGridWnd * > ( this ) )
				-> OnReportMaxGroupColumnCountReached();
		}
		return false;
	}
	return true;
}

CExtReportGridCategoryComboBox * CExtReportGridWnd::ReportCategoryComboBoxGet()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return NULL;
	if( m_pCAB != NULL )
	{
		ASSERT_VALID( m_pCAB );
		ASSERT( m_pCAB->GetSafeHwnd() != NULL );
		return m_pCAB;
	} // if( m_pCAB != NULL )
	m_pCAB = OnReportGridCreateCategoryComboBox();
	if( m_pCAB == NULL )
		return NULL;
	ASSERT_VALID( m_pCAB );
	ASSERT( m_pCAB->GetSafeHwnd() != NULL );
	return m_pCAB;
}

const CExtReportGridCategoryComboBox * CExtReportGridWnd::ReportCategoryComboBoxGet() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
		-> ReportCategoryComboBoxGet();
}

CExtReportGridColumnChooserWnd * CExtReportGridWnd::ReportColumnChooserGet()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return NULL;
	if( m_pCCW != NULL )
	{
		ASSERT_VALID( m_pCCW );
		ASSERT( m_pCCW->GetSafeHwnd() != NULL );
		return m_pCCW;
	} // if( m_pCCW != NULL )
	m_pCCW = OnReportGridCreateColumnChooser();
	if( m_pCCW == NULL )
		return NULL;
	OnReportGridColumnChooserAdjustLocation();
	ASSERT_VALID( m_pCCW );
	ASSERT( m_pCCW->GetSafeHwnd() != NULL );
	return m_pCCW;
}

const CExtReportGridColumnChooserWnd * CExtReportGridWnd::ReportColumnChooserGet() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
		-> ReportColumnChooserGet();
}
CExtReportGridGroupAreaWnd * CExtReportGridWnd::ReportGroupAreaGet()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return NULL;
	if( m_pGAW != NULL )
	{
		ASSERT_VALID( m_pGAW );
		ASSERT( m_pGAW->GetSafeHwnd() != NULL );
		return m_pGAW;
	} // if( m_pGAW != NULL )
	m_pGAW = OnReportGridCreateGroupArea();
	if( m_pGAW == NULL )
		return NULL;
	ASSERT_VALID( m_pGAW );
	ASSERT( m_pGAW->GetSafeHwnd() != NULL );
	return m_pGAW;
}

bool CExtReportGridWnd::ReportColumnChooserIsVisible() const
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return false;
const CExtReportGridColumnChooserWnd * pCCW = ReportColumnChooserGet();
	if( pCCW->GetSafeHwnd() == NULL )
		return false;
DWORD dwStyle = pCCW->GetStyle();
	if( (dwStyle&WS_VISIBLE) == 0 )
		return false;
CWnd * pWnd = pCCW->GetParent();
	if( LPVOID(pWnd) == LPCVOID(this) )
		return true;
	dwStyle = pWnd->GetStyle();
	if( (dwStyle&WS_VISIBLE) == 0 )
		return false;
	return true;
}

void CExtReportGridWnd::ReportCategoryComboBoxShow(
	bool bShow // = true
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	if( bShow )
	{
		if( ReportCategoryComboBoxIsVisible() )
			return;
	}
	else
	{
		if( ! ReportCategoryComboBoxIsVisible() )
			return;
	}
CExtReportGridCategoryComboBox * pCAB = ReportCategoryComboBoxGet();
	if( pCAB->GetSafeHwnd() == NULL )
		return;
CWnd * pWnd = pCAB->GetParent();
	if( LPVOID(pWnd) == LPCVOID(this) )
	{
		pCAB->ShowWindow( bShow ? SW_SHOWNA : SW_HIDE );
		OnSwRecalcLayout( true );
		return;
	}
	pCAB->ShowWindow( bShow ? SW_SHOWNOACTIVATE : SW_HIDE );
}

bool CExtReportGridWnd::ReportCategoryComboBoxIsVisible() const
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return false;
const CExtReportGridCategoryComboBox * pCAB = ReportCategoryComboBoxGet();
	if( pCAB->GetSafeHwnd() == NULL )
		return false;
DWORD dwStyle = pCAB->GetStyle();
	if( (dwStyle&WS_VISIBLE) == 0 )
		return false;
CWnd * pWnd = pCAB->GetParent();
	if( LPVOID(pWnd) == LPCVOID(this) )
		return true;
	dwStyle = pWnd->GetStyle();
	if( (dwStyle&WS_VISIBLE) == 0 )
		return false;
	return true;
}

void CExtReportGridWnd::ReportColumnChooserShow(
	bool bShow // = true
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	if( bShow )
	{
		if( ReportColumnChooserIsVisible() )
			return;
	}
	else
	{
		if( ! ReportColumnChooserIsVisible() )
			return;
	}
CExtReportGridColumnChooserWnd * pCCW = ReportColumnChooserGet();
	if( pCCW->GetSafeHwnd() == NULL )
		return;
CWnd * pWnd = pCCW->GetParent();
	if( LPVOID(pWnd) == LPCVOID(this) )
	{
		//m_rcClient.SetRect( 0, 0, 0, 0 );
		pCCW->ShowWindow( bShow ? SW_SHOWNA : SW_HIDE );
		OnSwRecalcLayout( true );
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
		return;
	}
	pCCW->ShowWindow( bShow ? SW_SHOWNOACTIVATE : SW_HIDE );
	pWnd->ShowWindow( bShow ? SW_SHOWNOACTIVATE : SW_HIDE );
}

const CExtReportGridGroupAreaWnd * CExtReportGridWnd::ReportGroupAreaGet() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
		-> ReportGroupAreaGet();
}

bool CExtReportGridWnd::ReportGroupAreaIsVisible() const
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return false;
const CExtReportGridGroupAreaWnd * pGAW = ReportGroupAreaGet();
	if( pGAW->GetSafeHwnd() == NULL )
		return false;
DWORD dwStyle = pGAW->GetStyle();
	if( (dwStyle&WS_VISIBLE) == 0 )
		return false;
CWnd * pWnd = pGAW->GetParent();
	if( LPVOID(pWnd) == LPCVOID(this) )
		return true;
	dwStyle = pWnd->GetStyle();
	if( (dwStyle&WS_VISIBLE) == 0 )
		return false;
	return true;
}

void CExtReportGridWnd::ReportGroupAreaShow(
	bool bShow // = true
	)
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
	if( bShow )
	{
		if( ReportGroupAreaIsVisible() )
			return;
	}
	else
	{
		if( ! ReportGroupAreaIsVisible() )
			return;
	}
CExtReportGridGroupAreaWnd * pGAW = ReportGroupAreaGet();
	if( pGAW->GetSafeHwnd() == NULL )
		return;
CWnd * pWnd = pGAW->GetParent();
	if( LPVOID(pWnd) == LPCVOID(this) )
	{
		pGAW->ShowWindow( bShow ? SW_SHOWNA : SW_HIDE );
		OnSwRecalcLayout( true );
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
		return;
	}
	pGAW->ShowWindow( bShow ? SW_SHOWNOACTIVATE : SW_HIDE );
	OnSwRecalcLayout( true );
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

void CExtReportGridWnd::OnGridOuterDragOut(
	const CExtGridHitTestInfo & htInfo
	)
{
	ASSERT_VALID( this );
	ASSERT( ! htInfo.IsHoverEmpty() );
	ASSERT( htInfo.GetInnerOuterTypeOfColumn() == 0 );
	ASSERT( htInfo.GetInnerOuterTypeOfRow() == (-1) );
CExtGridCell * pCellTmp =
		GridCellGet( htInfo );
	ASSERT_VALID( pCellTmp );
CExtReportGridColumn * pRGC =
		STATIC_DOWNCAST( CExtReportGridColumn, pCellTmp );
	VERIFY( ReportColumnActivate( pRGC, false ) );
}

void CExtReportGridWnd::OnReportGridColumnChooserAdjustLocation()
{
	ASSERT_VALID( this );
	if( GetSafeHwnd() == NULL )
		return;
CExtReportGridColumnChooserWnd * pCCW = ReportColumnChooserGet();
	if( pCCW == NULL )
		return;
	ASSERT_VALID( pCCW );
	if( pCCW->GetSafeHwnd() == NULL )
		return;
	//m_rcClient.SetRect( 0, 0, 0, 0 );
UINT nOrientation = pCCW->OrientationGet();
CWnd * pWndParent = pCCW->GetParent();
	if( nOrientation != AFX_IDW_DOCKBAR_FLOAT )
	{
		if( pWndParent == this )
		{
			OnSwRecalcLayout( true );
			OnSwUpdateScrollBars();
			OnSwDoRedraw();
			return;
		}
		pWndParent = pCCW->GetParentFrame();
		ASSERT_VALID( pWndParent );
		ASSERT( pWndParent != GetParentFrame() );
		pCCW->SetParent( this );
		pWndParent->DestroyWindow(); // must be self-destroying implementation
		OnSwRecalcLayout( true );
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
		return;
	} // if( nOrientation != AFX_IDW_DOCKBAR_FLOAT )
	if( pWndParent != this )
	{
		OnSwRecalcLayout( true );
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
		return;
	}
CExtReportGridColumnChooserMiniFrameWnd * pMiniFrame =
		new CExtReportGridColumnChooserMiniFrameWnd( *this );
	VERIFY( pMiniFrame->Create() );
	OnSwRecalcLayout( true );
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

CExtReportGridColumnChooserWnd * CExtReportGridWnd::OnReportGridCreateColumnChooser()
{
	ASSERT_VALID( this );
CExtReportGridColumnChooserWnd * pCCW =
		new CExtReportGridColumnChooserWnd( *this );
	if( ! pCCW->Create(
			this,
			CRect( 0, 0, 0, 0 ),
			7000,
			0L,
			WS_CHILD|WS_CLIPCHILDREN
				|WS_CLIPCHILDREN|WS_CLIPSIBLINGS
				|WS_TABSTOP
				//|WS_VISIBLE
			)
		)
		return NULL;
	ModifyStyleEx( 0, WS_EX_CONTROLPARENT );
	return pCCW;
}

CExtReportGridCategoryComboBox * CExtReportGridWnd::OnReportGridCreateCategoryComboBox()
{
	ASSERT_VALID( this );
CExtReportGridColumnChooserWnd * pCCW =
		ReportColumnChooserGet();
	if( pCCW->GetSafeHwnd() == NULL )
		return false;
CExtReportGridCategoryComboBox * pCAB =
		new CExtReportGridCategoryComboBox( this );
	if( ! pCAB->Create( pCCW, true ) )
		return NULL;
	pCCW->ModifyStyleEx( 0, WS_EX_CONTROLPARENT );
	return pCAB;
}

CExtReportGridGroupAreaWnd * CExtReportGridWnd::OnReportGridCreateGroupArea()
{
	ASSERT_VALID( this );
CExtReportGridGroupAreaWnd * pGAW = new CExtReportGridGroupAreaWnd( *this );
	if( ! pGAW->Create(
			this,
			CRect( 0, 0, 0, 0 ),
			7001,
			WS_CHILD|WS_CLIPCHILDREN
				|WS_CLIPCHILDREN|WS_CLIPSIBLINGS
				|WS_TABSTOP
				//|WS_VISIBLE
			)
		)
		return NULL;
	return pGAW;
}

CExtReportGridColumn * CExtReportGridWnd::ReportColumnRegister(
	__EXT_MFC_SAFE_LPCTSTR strColumnName, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strCategoryName, // = NULL
	bool bActivate, // = true
	bool bRedraw, // = true
	CRuntimeClass * pRTC // = NULL // should be kind of CExtReportGridColumn
	)
{
	ASSERT_VALID( this );
	if( pRTC != NULL )
	{
		if( ! pRTC->IsDerivedFrom( RUNTIME_CLASS( CExtReportGridColumn ) ) )
		{
			ASSERT( FALSE );
			return NULL;
		}
	} // if( pRTC != NULL )
	else
		pRTC = RUNTIME_CLASS( CExtReportGridColumn );
	if( strColumnName == NULL )
		strColumnName = _T("");
	if( strCategoryName == NULL )
		strCategoryName = _T("");
	if( ! ColumnAdd( 1L, false ) )
		return NULL;
LONG nColCount = ColumnCountGet();
CExtGridCell * pCellAllocated =
		GridCellGetOuterAtTop(
			nColCount - 1L,
			0,
			pRTC,
			false,
			false
			);
	if( pCellAllocated == NULL )
		return false;
	ASSERT_VALID( pCellAllocated );
CExtReportGridColumn * pRGC =
		STATIC_DOWNCAST( CExtReportGridColumn, pCellAllocated );
	pRGC->m_pRGW = this;
// 	pRGC->ColumnNameSet( strColumnName );
// 	pRGC->CategoryNameSet( strCategoryName );
	pRGC->InitNames(
		strColumnName,
		strCategoryName
		);
CTypedPtrArray < CPtrArray, CExtReportGridColumn * >
		* pArrCategoryColumns = NULL;
	if(	! m_mapColumnCategories.Lookup(
			strCategoryName,
			(void*&)pArrCategoryColumns
			)
		)
	{
		pArrCategoryColumns = new
			CTypedPtrArray < CPtrArray, CExtReportGridColumn * >;
		m_mapColumnCategories.SetAt( strCategoryName, pArrCategoryColumns );
		OnReportGridColumnCategoryAdded( strCategoryName );
	}
#ifdef _DEBUG
	else
	{
		ASSERT( pArrCategoryColumns != NULL );
	}
#endif // _DEBUG
	pArrCategoryColumns->Add( pRGC );
	m_mapColumns.SetAt( pRGC, pRGC );
	if( ! bActivate )
	{
// 		CExtReportGridDataProvider & _DP = _GetReportData();
// 		VERIFY( _DP.DeactivateColumn( nColCount - 1L ) );
		VERIFY(
			ReportColumnActivate(
				strColumnName,
				strCategoryName,
				false,
				-1L,
				false
				)
			);
	} // if( ! bActivate )
	if( bRedraw && GetSafeHwnd() != NULL )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw && GetSafeHwnd() != NULL )
	return pRGC;
}

bool CExtReportGridWnd::ReportColumnActivate(
	CExtReportGridColumn * pRGC,
	bool bActivate, // = true
	LONG nColNo, // = -1L // -1L - activate last
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	if( pRGC == NULL )
		return false;
	ASSERT_VALID( pRGC );
	if( ! ReportColumnIsRegistered( pRGC ) )
		return false;
__EXT_MFC_SAFE_LPCTSTR strColumnName = pRGC->ColumnNameGet();
__EXT_MFC_SAFE_LPCTSTR strCategoryName = pRGC->CategoryNameGet();
	return
		ReportColumnActivate(
			strColumnName,
			strCategoryName,
			bActivate,
			nColNo,
			bRedraw
			);
}

bool CExtReportGridWnd::ReportColumnActivate( // default parameters will activate/deactivate all the columns
	__EXT_MFC_SAFE_LPCTSTR strColumnName, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strCategoryName, // = NULL
	bool bActivate, // = true
	LONG nColNo, // = -1L // -1L - activate last
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	if( bActivate )
	{
		CExtReportGridDataProvider & _DP = _GetReportData();
		LONG nDislayedColumnCount = LONG( _DP.ActiveColumnCountGet() );
		if( nColNo < 0 || nColNo > nDislayedColumnCount )
			nColNo = nDislayedColumnCount;
	} // if( bActivate )
bool bRetVal = true;
	if( strColumnName == NULL )
	{
		if( strCategoryName == NULL )
		{
			// activate/deactivate all the columns in all categories
			POSITION pos = m_mapColumns.GetStartPosition();
			for( ; pos != NULL; )
			{
				CExtReportGridColumn * pRGC = NULL;
				LPVOID ptr = NULL;
				m_mapColumns.GetNextAssoc( pos, (void*&)pRGC, ptr );
				ASSERT_VALID( pRGC );
				ASSERT( pRGC->m_pRGW == this );
				if( ! ReportColumnActivate( pRGC, bActivate, nColNo, false ) )
					bRetVal = false;
				else if( bActivate )
					nColNo ++;
			} // for( ; pos != NULL; )
		} // if( strCategoryName == NULL )
		else
		{
			// activate/deactivate all the columns in the strCategoryName category
			CTypedPtrArray < CPtrArray, CExtReportGridColumn * > arrCategoryColumns;
			ReportColumnGetContent( strCategoryName, arrCategoryColumns );
			LONG nColumnCount = LONG( arrCategoryColumns.GetSize() );
			if( nColumnCount == 0L )
				bRetVal = false;
			else
			{
				LONG nColumnIndex;
				for( nColumnIndex = 0; nColumnIndex < nColumnCount; nColumnIndex ++ )
				{
					CExtReportGridColumn * pRGC = arrCategoryColumns[ nColumnIndex ];
					ASSERT_VALID( pRGC );
					if( ! ReportColumnActivate(
							pRGC,
							bActivate,
							nColNo,
							false
							)
						)
						bRetVal = false;
					else if( bActivate )
						nColNo++;
				} // for( nColumnIndex = 0; nColumnIndex < nColumnCount; nColumnIndex ++ )
			} // else from if( nColumnCount == 0L )
		} // else from if( strCategoryName == NULL )
	} // if( strColumnName == NULL )
	else
	{
		// activate/deactivate all one column
		if( strCategoryName == NULL )
			strCategoryName = _T("");
		CExtReportGridColumn * pRGC =
			ReportColumnGet( strColumnName, strCategoryName );
		if( pRGC != NULL )
		{
			ASSERT_VALID( pRGC );
			if( bActivate )
			{
				if( ! pRGC->ColumnIsActive() )
				{
					CExtReportGridDataProvider & _DP = _GetReportData();
					ULONG nCurrentColNo = _DP.InactiveIndexGet( pRGC );
					if( nCurrentColNo != ULONG(-1L) )
					{
						bRetVal = _DP.ActivateColumn( nCurrentColNo, ULONG(nColNo) );
						ASSERT( bRetVal );
						if( bRetVal )
							pRGC->m_bHelperIsActive = true;
					} // if( nCurrentColNo != ULONG(-1L) )
					else
					{
						ASSERT( FALSE );
						bRetVal = false;
					} // else from if( nCurrentColNo != ULONG(-1L) )
				} // if( ! pRGC->ColumnIsActive() )
				else
				{
					CExtReportGridDataProvider & _DP = _GetReportData();
					ULONG nCurrentColNo = _DP.ActiveIndexGet( pRGC );
					if( LONG(nCurrentColNo) != nColNo )
					{
						// RE-ACTIVATE COLUMN AT NEW INDEX
						bRetVal = _DP.DeactivateColumn( LONG(nCurrentColNo), 0 );
						ASSERT( bRetVal );
						if( bRetVal )
						{
							if( nColNo > LONG(nCurrentColNo) )
								nColNo --;
							pRGC->m_bHelperIsActive = false;
							bRetVal = _DP.ActivateColumn( 0, ULONG(nColNo) );
							ASSERT( bRetVal );
							if( bRetVal )
								pRGC->m_bHelperIsActive = true;
						} // if( bRetVal )
					} // if( LONG(nCurrentColNo) != nColNo )
				} // else if( ! pRGC->ColumnIsActive() )
			} // if( bActivate )
			else
			{
				if( pRGC->ColumnIsActive() )
				{
					CExtReportGridDataProvider & _DP = _GetReportData();
					ULONG nCurrentColNo = _DP.ActiveIndexGet( pRGC );
					if( nCurrentColNo != ULONG(-1L) )
					{
						bRetVal = _DP.DeactivateColumn( nCurrentColNo, LONG(nColNo) );
						ASSERT( bRetVal );
						if( bRetVal )
							pRGC->m_bHelperIsActive = false;
					} // if( nCurrentColNo != ULONG(-1L) )
					else
					{
						ASSERT( FALSE );
						bRetVal = false;
					} // else from if( nCurrentColNo != ULONG(-1L) )
				} // if( pRGC->ColumnIsActive() )
			} // else from if( bActivate )
			if( bRetVal )
			{
				int nCurSel = -1;
				CExtReportGridCategoryComboBox * pCAB = ReportCategoryComboBoxGet();
				if(		pCAB->GetSafeHwnd() != NULL
					&&	pCAB->GetCount() > 0
					&&	( nCurSel = pCAB->GetCurSel() ) >= 0
					)
				{
					CString strCategorySel;
					pCAB->GetLBText( nCurSel, strCategorySel );
					if( strCategorySel == strCategoryName )
						pCAB->OnSynchronizeCurSel();
				} // if( pCAB->GetSafeHwnd() != NULL ...
			} // if( bRetVal )
		} // if( pRGC != NULL )
		else
			bRetVal = false;
	} // else from if( strColumnName == NULL )
	if( ! bRetVal )
		return false;
	if( bRedraw && GetSafeHwnd() != NULL )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw && GetSafeHwnd() != NULL )
	return true;
}

bool CExtReportGridWnd::ReportColumnIsActive(
	const CExtReportGridColumn * pRGC
	) const
{
	ASSERT_VALID( this );
	if( pRGC == NULL )
		return false;
	ASSERT_VALID( pRGC );
	if( ! ReportColumnIsRegistered( pRGC ) )
		return false;
__EXT_MFC_SAFE_LPCTSTR strColumnName = pRGC->ColumnNameGet();
__EXT_MFC_SAFE_LPCTSTR strCategoryName = pRGC->CategoryNameGet();
	return
		ReportColumnIsActive(
			strColumnName,
			strCategoryName
			);
}

bool CExtReportGridWnd::ReportColumnIsActive(
	__EXT_MFC_SAFE_LPCTSTR strColumnName,
	__EXT_MFC_SAFE_LPCTSTR strCategoryName
	) const
{
	ASSERT_VALID( this );
	if( strColumnName == NULL )
		strColumnName = _T("");
	if( strCategoryName == NULL )
		strCategoryName = _T("");
const CExtReportGridColumn * pRGC =
		ReportColumnGet( strColumnName, strCategoryName );
	if( pRGC == NULL )
		return false;
	ASSERT_VALID( pRGC );
bool bActive = pRGC->ColumnIsActive();
	return bActive;
}

bool CExtReportGridWnd::ReportColumnRename(
	__EXT_MFC_SAFE_LPCTSTR strColumnNameOld, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strCategoryNameOld, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strColumnNameNew, // = NULL
	__EXT_MFC_SAFE_LPCTSTR strCategoryNameNew, // = NULL
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	if( strColumnNameOld == NULL )
		strColumnNameOld = _T("");
	if( strCategoryNameOld == NULL )
		strCategoryNameOld = _T("");
	if( strColumnNameNew == NULL )
		strColumnNameNew = _T("");
	if( strCategoryNameNew == NULL )
		strCategoryNameNew = _T("");
LONG nIndex, nCount;
CExtReportGridColumn * pMovingRGC = NULL;
CTypedPtrArray < CPtrArray, CExtReportGridColumn * >
		* pArrCategoryColumns = NULL;
	if( _tcscmp(strCategoryNameNew,strCategoryNameOld) != 0 )
	{
		if(	m_mapColumnCategories.Lookup(
				strCategoryNameOld,
				(void*&)pArrCategoryColumns
				)
			)
		{
			ASSERT( pArrCategoryColumns != NULL );
			nCount = LONG( pArrCategoryColumns->GetSize() );
			ASSERT( nCount > 0 );
			if( nCount > 1 )
			{
				for( nIndex = 0; nIndex < nCount; nIndex ++ )
				{
					CExtReportGridColumn * pRGC =
						pArrCategoryColumns->GetAt( nIndex );
					ASSERT_VALID( pRGC );
					ASSERT( pRGC->m_pRGW == this );
					if( _tcscmp(
							pRGC->ColumnNameGet(),
							strCategoryNameOld
							) == 0
						)
					{
						pMovingRGC = pRGC;
						pArrCategoryColumns->RemoveAt( nIndex );
						break;
					}
				} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
				if( pMovingRGC == NULL )
					return false; // not found
				ASSERT_VALID( pMovingRGC );
			} // if( nCount > 1 )
			else
			{
				ASSERT( nCount == 1 );
				pMovingRGC = pArrCategoryColumns->GetAt( 0 );
				ASSERT_VALID( pMovingRGC );
				if(	_tcscmp(
						pMovingRGC->ColumnNameGet(),
						strColumnNameOld
						) != 0
					)
					return false; // not found
				delete pArrCategoryColumns;
				m_mapColumnCategories.RemoveKey( strCategoryNameOld );
				OnReportGridColumnCategoryRemoved( strCategoryNameOld );
			} // else from if( nCount > 1 )
		}
		ASSERT_VALID( pMovingRGC );
		pArrCategoryColumns = NULL;
		if(	! m_mapColumnCategories.Lookup(
				strCategoryNameNew,
				(void*&)pArrCategoryColumns
				)
			)
		{
			pArrCategoryColumns = new
				CTypedPtrArray < CPtrArray, CExtReportGridColumn * >;
			m_mapColumnCategories.SetAt( strCategoryNameNew, pArrCategoryColumns );
			OnReportGridColumnCategoryAdded( strCategoryNameNew );
		}
#ifdef _DEBUG
		else
		{
			ASSERT( pArrCategoryColumns != NULL );
		}
#endif // _DEBUG
		pArrCategoryColumns->Add( pMovingRGC );
	} // if( _tcscmp(strCategoryNameNew,strCategoryNameOld) != 0 )
	else
	{
		if(	_tcscmp(
				strColumnNameOld,
				strColumnNameNew
				) == 0
			)
			return true; // both names are equal
		if(	! m_mapColumnCategories.Lookup(
				strCategoryNameNew,
				(void*&)pArrCategoryColumns
				)
			)
			return false; // not found
		ASSERT( pArrCategoryColumns != NULL );
		nCount = LONG( pArrCategoryColumns->GetSize() );
		ASSERT( nCount > 0 );
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtReportGridColumn * pRGC =
				pArrCategoryColumns->GetAt( nIndex );
			ASSERT_VALID( pRGC );
			ASSERT( pRGC->m_pRGW == this );
			if( _tcscmp(
					pRGC->ColumnNameGet(),
					strColumnNameOld
					) == 0
				)
			{
				pMovingRGC = pRGC;
				break;
			}
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
		if( pMovingRGC == NULL )
			return false; // not found
		ASSERT_VALID( pMovingRGC );
	} // else from if( _tcscmp(strCategoryNameNew,strCategoryNameOld) != 0 )
	ASSERT_VALID( pMovingRGC );
	m_mapColumns.SetAt( pMovingRGC, pMovingRGC );
	if( bRedraw && GetSafeHwnd() != NULL )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw && GetSafeHwnd() != NULL )
	return true;
}

void CExtReportGridWnd::OnReportGridColumnCategoryAdded(
	__EXT_MFC_SAFE_LPCTSTR strCategoryName
	)
{
	ASSERT_VALID( this );
CExtReportGridCategoryComboBox * pCAB =
		ReportCategoryComboBoxGet();
	if( pCAB->GetSafeHwnd() == NULL )
		return;
int nIndex = pCAB->AddString( LPCTSTR(strCategoryName) );
	if( nIndex < 0 )
		return;
	pCAB->EnableWindow( TRUE );
	pCAB->SetCurSel( nIndex );
	pCAB->OnSynchronizeCurSel();
}

void CExtReportGridWnd::OnReportGridColumnCategoryRemoved(
	__EXT_MFC_SAFE_LPCTSTR strCategoryName
	)
{
	ASSERT_VALID( this );
CExtReportGridCategoryComboBox * pCAB =
		ReportCategoryComboBoxGet();
	if( pCAB->GetSafeHwnd() == NULL )
		return;
int nIndex = pCAB->FindStringExact( -1, LPCTSTR(strCategoryName) );
	if( nIndex >= 0 )
		pCAB->DeleteString( nIndex );
int nCount = pCAB->GetCount();
	if( nCount == 0 )
	{
		pCAB->SetWindowText( _T("") );
		pCAB->EnableWindow( FALSE );
		return;
	} // if( nCount == 0 )
	if( nIndex == nCount )
		nIndex --;
	pCAB->SetCurSel( nIndex );
	pCAB->OnSynchronizeCurSel();
}

const CExtReportGridColumn * CExtReportGridWnd::ReportColumnGet(
	__EXT_MFC_SAFE_LPCTSTR strColumnName,
	__EXT_MFC_SAFE_LPCTSTR strCategoryName // = NULL
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
		-> ReportColumnGet( strColumnName, strCategoryName );
}

CExtReportGridColumn * CExtReportGridWnd::ReportColumnGet(
	__EXT_MFC_SAFE_LPCTSTR strColumnName,
	__EXT_MFC_SAFE_LPCTSTR strCategoryName // = NULL
	)
{
	ASSERT_VALID( this );
	if( strColumnName == NULL )
		strColumnName = _T("");
	if( strCategoryName == NULL )
		strCategoryName = _T("");
CTypedPtrArray < CPtrArray, CExtReportGridColumn * >
		* pArrCategoryColumns = NULL;
	if(	! m_mapColumnCategories.Lookup(
			strCategoryName,
			(void*&)pArrCategoryColumns
			)
		)
		return false;
	ASSERT( pArrCategoryColumns != NULL );
LONG nIndex, nCount = LONG( pArrCategoryColumns->GetSize() );
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtReportGridColumn * pRGC =
			pArrCategoryColumns->GetAt( nIndex );
		ASSERT_VALID( pRGC );
		ASSERT( pRGC->m_pRGW == this );
		if( _tcscmp(
				pRGC->ColumnNameGet(),
				strColumnName
				) == 0
			)
			return pRGC;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	return NULL;
}

LONG CExtReportGridWnd::ReportColumnGetCount(
	bool bIncludingActiveColumns, // = true
	bool bIncludingInactiveColumns // = true
	) const
{
	ASSERT_VALID( this );
CExtReportGridDataProvider & _DP = _GetReportData();
ULONG nActiveColumnCount = _DP.ActiveColumnCountGet();
	ASSERT( LONG( nActiveColumnCount ) >= 0 );
ULONG nInactiveColumnCount = _DP.InactiveColumnCountGet();
	ASSERT( LONG( nInactiveColumnCount ) >= 0 );
	ASSERT(
			ULONG( m_mapColumns.GetCount() )
			== ( nActiveColumnCount + nInactiveColumnCount )
		);
ULONG nRetVal = 0;
	if( bIncludingActiveColumns )
		nRetVal += nActiveColumnCount;
	if( bIncludingInactiveColumns )
		nRetVal += nInactiveColumnCount;
	ASSERT( LONG( nRetVal ) >= 0 );
	return LONG( nRetVal );
}

POSITION CExtReportGridWnd::ReportColumnGetStartPosition(
	__EXT_MFC_SAFE_LPCTSTR strCategoryName, // = __EXT_MFC_SAFE_LPCTSTR(NULL) // NULL - all categories
	bool bIncludeActive, // = true
	bool bIncludeInactive // = true
	) const
{
	ASSERT_VALID( this );
	if( (! bIncludeActive) && (! bIncludeInactive) )
		return NULL;
POSITION pos = m_mapColumns.GetStartPosition();
	if( pos == NULL )
		return NULL;
	if(		bIncludeActive
		&&	bIncludeInactive
		&&	strCategoryName == NULL
		)
		return pos;
POSITION posSaved = pos;
	for( ; pos != NULL; posSaved = pos )
	{
		CExtReportGridColumn * pRGC = NULL;
		LPVOID ptr = NULL;
		m_mapColumns.GetNextAssoc( pos, (void*&)pRGC, ptr );
		ASSERT_VALID( pRGC );
		ASSERT( pRGC->m_pRGW == this );
		bool bColumnIsActive = pRGC->ColumnIsActive();
		if(		( bIncludeActive && bColumnIsActive )
			||	( bIncludeInactive && (! bColumnIsActive ) )
			)
		{
			if( strCategoryName == NULL )
				return posSaved;
			LPCTSTR strCompare = pRGC->CategoryNameGet();
			ASSERT( strCompare != NULL );
			if( _tcscmp( LPCTSTR(strCategoryName), strCompare ) == 0 )
				return posSaved;
		}
	}
	return NULL;
}

CExtReportGridColumn * CExtReportGridWnd::ReportColumnGetNext(
	POSITION & pos,
	__EXT_MFC_SAFE_LPCTSTR strCategoryName, // = __EXT_MFC_SAFE_LPCTSTR(NULL) // NULL - all categories
	bool bIncludeActive, // = true
	bool bIncludeInactive // = true
	)
{
	ASSERT_VALID( this );
	if( pos == NULL )
		return NULL;
	if( (! bIncludeActive) && (! bIncludeInactive) )
	{
		pos = NULL;
		return NULL;
	}
CExtReportGridColumn * pRetValRGC = NULL;
	for( ; pos != NULL; )
	{
		CExtReportGridColumn * pRGC = NULL;
		LPVOID ptr = NULL;
		m_mapColumns.GetNextAssoc( pos, (void*&)pRGC, ptr );
		ASSERT_VALID( pRGC );
		ASSERT( pRGC->m_pRGW == this );
		if(		bIncludeActive
			&&	bIncludeInactive
			&&	strCategoryName == NULL
			)
		{
			pRetValRGC = pRGC;
			break;
		}
		bool bColumnIsActive = pRGC->ColumnIsActive();
		if(		( bIncludeActive && bColumnIsActive )
			||	( bIncludeInactive && (! bColumnIsActive ) )
			)
		{
			if( strCategoryName == NULL )
			{
				pRetValRGC = pRGC;
				break;
			}
			LPCTSTR strCompare = pRGC->CategoryNameGet();
			ASSERT( strCompare != NULL );
			if( _tcscmp( LPCTSTR(strCategoryName), strCompare ) == 0 )
			{
				pRetValRGC = pRGC;
				break;
			}
		}
	}
	if( pRetValRGC == NULL )
	{
		pos = NULL;
		return NULL;
	}
POSITION posSaved = pos;
	for( ; pos != NULL; posSaved = pos )
	{
		CExtReportGridColumn * pRGC = NULL;
		LPVOID ptr = NULL;
		m_mapColumns.GetNextAssoc( pos, (void*&)pRGC, ptr );
		ASSERT_VALID( pRGC );
		ASSERT( pRGC->m_pRGW == this );
		bool bColumnIsActive = pRGC->ColumnIsActive();
		if(		( bIncludeActive && bColumnIsActive )
			||	( bIncludeInactive && (! bColumnIsActive ) )
			)
		{
			if( strCategoryName == NULL )
			{
				pos = posSaved;
				break;
			}
			LPCTSTR strCompare = pRGC->CategoryNameGet();
			ASSERT( strCompare != NULL );
			if( _tcscmp( LPCTSTR(strCategoryName), strCompare ) == 0 )
			{
				pos = posSaved;
				break;
			}
		}
	}
	return pRetValRGC;
}

const CExtReportGridColumn * CExtReportGridWnd::ReportColumnGetNext(
	POSITION & pos,
	__EXT_MFC_SAFE_LPCTSTR strCategoryName, // = __EXT_MFC_SAFE_LPCTSTR(NULL) // NULL - all categories
	bool bIncludeActive, // = true
	bool bIncludeInactive // = true
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
		-> ReportColumnGetNext(
			pos,
			strCategoryName,
			bIncludeActive,
			bIncludeInactive
			);
}

void CExtReportGridWnd::ReportColumnGetContent(
	__EXT_MFC_SAFE_LPCTSTR strCategoryName, // NULL - all categories
	CTypedPtrArray < CPtrArray, CExtReportGridColumn * > & arrCategoryColumns,
	bool bIncludeActive, // = true
	bool bIncludeInactive // = true
	)
{
	ASSERT_VALID( this );
	arrCategoryColumns.RemoveAll();
	if( (! bIncludeActive) && (! bIncludeInactive) )
		return;
POSITION pos =
		ReportColumnGetStartPosition(
			strCategoryName,
			bIncludeActive,
			bIncludeInactive
			);
	for( ; pos != NULL; )
		arrCategoryColumns.Add(
			ReportColumnGetNext(
				pos,
				strCategoryName,
				bIncludeActive,
				bIncludeInactive
				)
			);
}

LONG CExtReportGridWnd::ReportColumnUnRegisterCategory(
	__EXT_MFC_SAFE_LPCTSTR strCategoryName,
	bool bRedraw // = true
	)
{
CTypedPtrArray < CPtrArray, CExtReportGridColumn * > arrCategoryColumns;
	ReportColumnGetContent(
		strCategoryName,
		arrCategoryColumns
		);
LONG nIndex, nCount = LONG( arrCategoryColumns.GetSize() ), nCountUnRegistered = 0;
	for( nIndex = 0; nIndex < nCount; nIndex ++ )
	{
		CExtReportGridColumn * pRGC = arrCategoryColumns[ nIndex ];
		ASSERT_VALID( pRGC );
		ASSERT( pRGC->m_pRGW == this );
		if( ReportColumnUnRegister( pRGC, false ) )
			nCountUnRegistered ++;
	} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	if( bRedraw && GetSafeHwnd() != NULL )
	{
		ReportSortOrderUpdate( false );
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw && GetSafeHwnd() != NULL )
	return nCountUnRegistered;
}

LONG CExtReportGridWnd::ReportColumnUnRegister( // returns number of unregistered columns
	__EXT_MFC_SAFE_LPCTSTR strColumnName, // = NULL // both NULL = unregister all
	__EXT_MFC_SAFE_LPCTSTR strCategoryName, // = NULL
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
LONG nCountUnRegistered = 0;
	if( strColumnName == NULL && strCategoryName == NULL )
	{
		POSITION pos = m_mapColumns.GetStartPosition();
		for( ; pos != NULL; )
		{
			CExtReportGridColumn * pRGC = NULL;
			LPVOID ptr = NULL;
			m_mapColumns.GetNextAssoc( pos, (void*&)pRGC, ptr );
			ASSERT_VALID( pRGC );
			ASSERT( pRGC->m_pRGW == this );
			if( ! ReportColumnUnRegister( pRGC, false ) )
				break;
			nCountUnRegistered ++;
			pos = m_mapColumns.GetStartPosition();
		} // for( ; pos != NULL; )
	} // if( strColumnName == NULL && strCategoryName == NULL )
	else
	{
		CExtReportGridColumn * pRGC =
			ReportColumnGet( strColumnName, strCategoryName );
		if(		pRGC != NULL
			&&	ReportColumnUnRegister( pRGC, false )
			)
			nCountUnRegistered ++;
	} // else from if( strColumnName == NULL && strCategoryName == NULL )
	if( bRedraw && GetSafeHwnd() != NULL )
	{
		ReportSortOrderUpdate( false );
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw && GetSafeHwnd() != NULL )
	return nCountUnRegistered;
}

bool CExtReportGridWnd::ReportColumnUnRegister(
	CExtReportGridColumn * pRGC,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	if(	pRGC == NULL )
		return false;
	ASSERT_VALID( pRGC );
	if( pRGC->m_pRGW != this )
		return false;
	if( CExtPopupMenuWnd::IsMenuTracking() )
		CExtPopupMenuWnd::CancelMenuTracking();
	m_pCtxMenuRGC = NULL;
__EXT_MFC_SAFE_LPCTSTR strCategoryName = pRGC->CategoryNameGet();
	if( strCategoryName == NULL )
		strCategoryName = _T("");

	m_mapColumns.RemoveKey( pRGC );

CTypedPtrArray < CPtrArray, CExtReportGridColumn * >
		* pArrCategoryColumns = NULL;
	if(	m_mapColumnCategories.Lookup(
			strCategoryName,
			(void*&)pArrCategoryColumns
			)
		)
	{
		LONG nIndex, nCount;
		ASSERT( pArrCategoryColumns != NULL );
		nCount = LONG( pArrCategoryColumns->GetSize() );
		for( nIndex = 0; nIndex < nCount; nIndex ++ )
		{
			CExtReportGridColumn * pWalkRGC =
				pArrCategoryColumns->GetAt( nIndex );
			ASSERT_VALID( pWalkRGC );
			ASSERT( pWalkRGC->m_pRGW == this );
			if( pRGC == pWalkRGC )
			{
				pArrCategoryColumns->RemoveAt( nIndex );
				if( nCount == 1 )
				{
					delete pArrCategoryColumns;
					m_mapColumnCategories.RemoveKey( strCategoryName );
					OnReportGridColumnCategoryRemoved( strCategoryName );
				}
				break;
			}
		} // for( nIndex = 0; nIndex < nCount; nIndex ++ )
	}

	// removing from data provider
CExtReportGridDataProvider & _DP = _GetReportData();
	_DP.RemoveReportColumn( pRGC );

	if( bRedraw && GetSafeHwnd() != NULL )
	{
		ReportSortOrderUpdate( false );
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	} // if( bRedraw && GetSafeHwnd() != NULL )

CExtReportGridCategoryComboBox * pCAB = ReportCategoryComboBoxGet();
	if( pCAB->GetSafeHwnd() != NULL )
	{
		ASSERT_VALID( pCAB );
		pCAB->OnSynchronizeCurSel();
	}

	return true;
}

bool CExtReportGridWnd::ReportColumnIsRegistered(
	const CExtReportGridColumn * pRGC
	) const
{
	ASSERT_VALID( this );
	if( pRGC == NULL )
		return false;
	ASSERT_VALID( pRGC );
	if( pRGC->m_pRGW != this )
		return false;
	return true;
}

void CExtReportGridWnd::ReportColumnGetAvailableCategoryNames(
	CExtSafeStringArray & arrAvailableCategoryNames
	) const
{
	ASSERT_VALID( this );
	arrAvailableCategoryNames.RemoveAll();
CExtSafeMapStringToPtr _map;
POSITION pos = ReportColumnGetStartPosition();
	for( ; pos != NULL; )
	{
		const CExtReportGridColumn * pRGC =
			ReportColumnGetNext( pos );
		ASSERT_VALID( pRGC );
		ASSERT( pRGC->m_pRGW == this );
		_map.SetAt( pRGC->CategoryNameGet(), NULL );
	} // for( ; pos != NULL; )
pos = _map.GetStartPosition();
	for( ; pos != NULL; )
	{
		CExtSafeString str;
		LPVOID ptr = NULL;
		_map.GetNextAssoc( pos, str, ptr );
		arrAvailableCategoryNames.Add( str );
	} // for( ; pos != NULL; )
}

LONG CExtReportGridWnd::_ReportColumnAdjustStylesByMask_Impl(
	CExtReportGridColumn * pRGC,
	HTREEITEM hTreeItem,
	DWORD dwStyleToSet,
	DWORD dwStyleMaskToRemove,
	bool bExtendedStyle
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGC );
	if( hTreeItem == NULL )
		return 0L;
LONG nChildCount = ItemGetChildCount( hTreeItem );
	if( nChildCount > 0 )
	{
		LONG nCountAdjustedRows = 0L, nChildIndex;
		for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex++ )
		{
			HTREEITEM htiChild = ItemGetChildAt( hTreeItem, nChildIndex );
			ASSERT( htiChild != NULL );
			nCountAdjustedRows +=
				_ReportColumnAdjustStylesByMask_Impl(
					pRGC,
					htiChild,
					dwStyleToSet,
					dwStyleMaskToRemove,
					bExtendedStyle
					);
		}
		return nCountAdjustedRows;
	} // if( nChildCount > 0 )
	else
	{
		if( hTreeItem == ItemGetRoot() )
			return 0L;
		CExtReportGridItem * pRGI = ReportItemFromTreeItem( hTreeItem );
		ASSERT_VALID( pRGI );
		CExtGridCell * pCell = ReportItemGetCell( pRGC, pRGI );
		if( pCell == NULL )
			return 0L;
		ASSERT_VALID( pCell );
		if( bExtendedStyle )
		{
			DWORD dwCellStyleEx = pCell->GetStyleEx();
			if( (dwCellStyleEx&dwStyleMaskToRemove) == dwStyleToSet )
				return 0L;
			pCell->ModifyStyleEx( 0, dwStyleMaskToRemove );
			pCell->ModifyStyleEx( dwStyleToSet );
		} // if( bExtendedStyle )
		else
		{
			DWORD dwCellStyle = pCell->GetStyle();
			if( (dwCellStyle&dwStyleMaskToRemove) == dwStyleToSet )
				return 0L;
			pCell->ModifyStyle( 0, dwStyleMaskToRemove );
			pCell->ModifyStyle( dwStyleToSet );
		} // else from if( bExtendedStyle )
		return 1L;
	} // else from if( nChildCount > 0 )
}

void CExtReportGridWnd::ReportColumnAdjustStylesByMask(
	CExtReportGridColumn * pRGC,
	DWORD dwStyleToSet,
	DWORD dwStyleMaskToRemove,
	bool bExtendedStyle,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGC );
	if(		dwStyleMaskToRemove == 0
		||	( dwStyleToSet & (~dwStyleMaskToRemove) ) != 0
		)
		return;
LONG nCountAdjustedRows =
		_ReportColumnAdjustStylesByMask_Impl(
			pRGC,
			ItemGetRoot(),
			dwStyleToSet,
			dwStyleMaskToRemove,
			bExtendedStyle
			);
	if( bExtendedStyle )
	{
		DWORD dwCellStyleEx = pRGC->GetStyleEx();
		if( (dwCellStyleEx&dwStyleMaskToRemove) != dwStyleToSet )
		{
			pRGC->ModifyStyleEx( 0, dwStyleMaskToRemove );
			pRGC->ModifyStyleEx( dwStyleToSet );
			nCountAdjustedRows ++;
		}
	} // if( bExtendedStyle )
	else
	{
		DWORD dwCellStyle = pRGC->GetStyle();
		if( (dwCellStyle&dwStyleMaskToRemove) != dwStyleToSet )
		{
			pRGC->ModifyStyle( 0, dwStyleMaskToRemove );
			pRGC->ModifyStyle( dwStyleToSet );
			nCountAdjustedRows ++;
		}
	} // else from if( bExtendedStyle )
	if(		( ! bRedraw )
		||	nCountAdjustedRows == 0
		||	GetSafeHwnd() == NULL
		)
		return;
	OnSwDoRedraw();
}

void CExtReportGridWnd::ReportColumnAdjustIconAlignmentH(
	CExtReportGridColumn * pRGC,
	DWORD dwAlignment,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGC );
	ReportColumnAdjustStylesByMask(
		pRGC,
		dwAlignment,
		__EGCS_ICA_HORZ_MASK,
		false,
		bRedraw
		);
}

void CExtReportGridWnd::ReportColumnAdjustIconAlignmentV(
	CExtReportGridColumn * pRGC,
	DWORD dwAlignment,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGC );
	ReportColumnAdjustStylesByMask(
		pRGC,
		dwAlignment,
		__EGCS_ICA_VERT_MASK,
		false,
		bRedraw
		);
}

void CExtReportGridWnd::ReportColumnAdjustTextAlignmentH(
	CExtReportGridColumn * pRGC,
	DWORD dwAlignment,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGC );
	ReportColumnAdjustStylesByMask(
		pRGC,
		dwAlignment,
		__EGCS_TA_HORZ_MASK,
		false,
		bRedraw
		);
}

void CExtReportGridWnd::ReportColumnAdjustTextAlignmentV(
	CExtReportGridColumn * pRGC,
	DWORD dwAlignment,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGC );
	ReportColumnAdjustStylesByMask(
		pRGC,
		dwAlignment,
		__EGCS_TA_VERT_MASK,
		false,
		bRedraw
		);
}

bool CExtReportGridWnd::ReportColumnProportionalResizingGet() const
{
	ASSERT_VALID( this );
DWORD dwBseStyleEx = BseGetStyleEx();
DWORD dwScrollTypeH = SiwScrollTypeHGet();
	if(		dwScrollTypeH == __ESIW_ST_NONE
		&&	(dwBseStyleEx&__EGBS_BSE_EX_PROPORTIONAL_COLUMN_WIDTHS) != 0
		)
		return true;
	return false;
}

void CExtReportGridWnd::ReportColumnProportionalResizingSet(
	bool bUseProportionalResizing,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
DWORD dwBseStyleEx = BseGetStyleEx();
DWORD dwScrollTypeH = SiwScrollTypeHGet();
	if( bUseProportionalResizing )
	{
		if(		dwScrollTypeH == __ESIW_ST_NONE
			&&	(dwBseStyleEx&__EGBS_BSE_EX_PROPORTIONAL_COLUMN_WIDTHS) != 0
			)
			return;
		SiwScrollTypeHSet( __ESIW_ST_NONE, false );
		BseModifyStyleEx( __EGBS_BSE_EX_PROPORTIONAL_COLUMN_WIDTHS, 0, false );
	} // if( bUseProportionalResizing )
	else
	{
		if(		dwScrollTypeH != __ESIW_ST_NONE
			&&	(dwBseStyleEx&__EGBS_BSE_EX_PROPORTIONAL_COLUMN_WIDTHS) == 0
			)
			return;
		SiwScrollTypeHSet( __ESIW_ST_PIXEL, false );
		BseModifyStyleEx( 0, __EGBS_BSE_EX_PROPORTIONAL_COLUMN_WIDTHS, false );
	} // else from if( bUseProportionalResizing )
	if( ! bRedraw )
		return;
	if( GetSafeHwnd() == NULL )
		return;
	OnSwUpdateScrollBars();
	OnSwDoRedraw();
}

bool CExtReportGridWnd::ReportAutoPreviewModeGet() const
{
	ASSERT_VALID( this );
DWORD dwReportGridStyle = ReportGridGetStyle();
	if( (dwReportGridStyle&__ERGS_AUTO_PREVIEW) != 0 )
		return true;
	return false;
}

void CExtReportGridWnd::ReportAutoPreviewModeSet(
	bool bAutoPreviewMode,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
CPoint ptFocus = FocusGet();
DWORD dwReportGridStyle = ReportGridGetStyle();
	if( (dwReportGridStyle&__ERGS_AUTO_PREVIEW) != 0 )
	{
		if( bAutoPreviewMode )
			return;
		ReportGridModifyStyle(
			0L,
			__ERGS_AUTO_PREVIEW,
			false
			);
	}
	else
	{
		if( ! bAutoPreviewMode )
			return;
		ReportGridModifyStyle(
			__ERGS_AUTO_PREVIEW,
			0L,
			false
			);
	}
	if( (! bRedraw ) || GetSafeHwnd() == NULL )
		return;
	OnSwUpdateScrollBars();
	if( ptFocus.y >= 0 )
		EnsureVisibleRow( ptFocus.y, false );
	OnSwDoRedraw();
}

bool CExtReportGridWnd::ReportItemRegister(
	CTypedPtrArray < CPtrArray, CExtReportGridItem * > & arrRegisteredItems,
	bool bUpdateSortOrder, // = false
	bool bRedraw // = false
	)
{
	ASSERT_VALID( this );
LONG nIndex, nCountToRegister = LONG( arrRegisteredItems.GetSize() );
	if( nCountToRegister == 0 )
		return true;
HTREEITEM hTreeItem = ItemInsert( NULL, (ULONG(-1)), ULONG(nCountToRegister), false );
	if( hTreeItem == NULL )
		return false; // failed
	for( nIndex = 0; nIndex < nCountToRegister; nIndex ++ )
	{
		ASSERT( hTreeItem != NULL );
		CExtTreeGridCellNode * pNode = CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
		ASSERT_VALID( pNode );
		ASSERT_KINDOF( CExtReportGridItem, pNode );
		CExtReportGridItem * pRGI = STATIC_DOWNCAST( CExtReportGridItem, pNode );
		pRGI->m_pRGW = this;
		pRGI->TreeNodeIndentPxSet( 0 );
		arrRegisteredItems.SetAt( nIndex, pRGI );
		hTreeItem = *( pNode->TreeNodeGetNext( true, false, false ) );
	} // for( nIndex = 0; nIndex < nCountToRegister; nIndex ++ )
	SelectionUnset( true, false );
	if( bUpdateSortOrder )
		ReportSortOrderUpdate( bRedraw );
	return true;
}

CExtReportGridItem * CExtReportGridWnd::ReportItemRegister(
	bool bUpdateSortOrder, // = false
	bool bRedraw // = false
	)
{
	ASSERT_VALID( this );
CTypedPtrArray < CPtrArray, CExtReportGridItem * > arrRegisteredItems;
	arrRegisteredItems.SetSize( 1 );
	if( ! ReportItemRegister( arrRegisteredItems, bUpdateSortOrder, bRedraw ) )
		return NULL;
CExtReportGridItem * pRGI = arrRegisteredItems[ 0 ];
	ASSERT_VALID( pRGI );
	return pRGI;
}

bool CExtReportGridWnd::_ReportItemUnRegisterImpl(
	CExtReportGridItem * pRGI,
	bool bResetFocus,
	bool bRedraw
	)
{
	ASSERT_VALID( this );
	if( pRGI == NULL )
		return false;
HTREEITEM hTreeItem = *pRGI,
		htiRoot = ItemGetRoot(),
		htiFocus = ItemFocusGet()
		;
	ASSERT( hTreeItem != NULL && htiRoot != NULL );
	if( hTreeItem == htiRoot )
		return false;
bool bForceMoveFocusToLast = false;
	if( bResetFocus && hTreeItem == htiFocus )
	{
		bResetFocus = false;
		htiFocus = ItemGetNext( hTreeItem, false, true, false );
		if( htiFocus == NULL )
			bForceMoveFocusToLast = true;
		else
		{
			ASSERT( htiFocus != hTreeItem );
			bResetFocus = true;
		}
	}
// LONG nChildItemCount = ItemGetChildCount( hTreeItem );
// 	ASSERT( nChildItemCount >= 0 );
HTREEITEM htiParent = ItemGetParent( hTreeItem );
	ASSERT( htiParent != NULL );
	LONG nSiblingItemCount = ItemGetChildCount( htiParent );
	ASSERT( nSiblingItemCount > 0 );
	ItemRemove( hTreeItem, false, false );
	if( nSiblingItemCount > 1 || htiParent == htiRoot )
	{ // removing non-stand alone leaf item or item at root
		if( bResetFocus )
			ItemFocusSet( htiFocus, false );
		else if( bForceMoveFocusToLast )
		{
			LONG i = RowCountGet();
			if( i > 0 )
			{
				i --;
				htiFocus = ItemGetByVisibleRowIndex( i );
				if( htiFocus )
					ItemFocusSet( htiFocus, false );
			}
		}
		SelectionUnset( true, false );
		if( bRedraw && GetSafeHwnd() != NULL )
		{
			OnSwUpdateScrollBars();
			OnSwDoRedraw();
		}
		return true;
	} // removing non-stand alone leaf item or item at root
	if( htiParent == htiRoot )
	{
		if( bResetFocus )
			ItemFocusSet( htiFocus, false );
		SelectionUnset( true, false );
		if( bRedraw && GetSafeHwnd() != NULL )
		{
			OnSwUpdateScrollBars();
			OnSwDoRedraw();
		}
		return true;
	}
	pRGI = ReportItemFromTreeItem( htiParent );
	ASSERT_VALID( pRGI );
bool bRetVal = _ReportItemUnRegisterImpl( pRGI, false, false );
	if( bResetFocus )
		ItemFocusSet( htiFocus, false );
	else if( bForceMoveFocusToLast )
	{
		LONG i = RowCountGet();
		if( i > 0 )
		{
			i --;
			htiFocus = ItemGetByVisibleRowIndex( i );
			if( htiFocus )
				ItemFocusSet( htiFocus, false );
		}
	}
	SelectionUnset( true, false );
	if( bRedraw && GetSafeHwnd() != NULL )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	}
	return bRetVal;
}

LONG CExtReportGridWnd::_SelectSubtreeForUnRegistering(
	HTREEITEM hTreeItem
	)
{
	ASSERT_VALID( this );
	ASSERT( hTreeItem != NULL );
LONG nRowNo = ItemGetVisibleIndexOf( hTreeItem );
	ASSERT( nRowNo >= 0 );
	if( ! SelectionGetForCell( 0L, nRowNo ) )
	{
		VERIFY(
			SelectionInsertAt(
				0,
				CRect( 0L, nRowNo, ColumnCountGet() - 1, nRowNo ),
				false
				)
			);
	}
	if( ItemIsExpanded( hTreeItem ) )
	{
		LONG nChildIndex, nChildCount = ItemGetChildCount( hTreeItem );
		for( nChildIndex = 0; nChildIndex < nChildCount; nChildIndex ++ )
		{
			HTREEITEM htiChild = ItemGetChildAt( hTreeItem, nChildIndex );
			ASSERT( htiChild != NULL );
			nRowNo = _SelectSubtreeForUnRegistering( htiChild );
		}
	}
	return nRowNo;
}

bool CExtReportGridWnd::ReportItemModifySelectionForUnRegistering(
	bool bRedraw, // = true
	HTREEITEM * p_htiResetFocus // = NULL
	)
{
	ASSERT_VALID( this );
LONG nRowCount = 0L;
	if(		SelectionIsEmpty()
		||	( nRowCount = RowCountGet() ) == 0
		||	ColumnCountGet() == 0
		)
		return false;
LONG nRowNo = SelectionGetFirstRowInColumn( 0 );
LONG nRowNoResetFocusBottom = -1L;
LONG nRowNoResetFocusTop = -1L;
	if( nRowNo > 0 )
		nRowNoResetFocusTop = nRowNo - 1;
	for( ; nRowNo >= 0 && nRowNo < nRowCount; )
	{
		nRowNoResetFocusBottom = nRowNo + 1;
		HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
		ASSERT( hTreeItem != NULL );
		LONG nChildCount = ItemGetChildCount( hTreeItem );
		if( nChildCount == 0 )
			nRowNo = SelectionGetNextRowInColumn( 0, nRowNo );
		else
		{
			nRowNo = _SelectSubtreeForUnRegistering( hTreeItem );
			ASSERT( nRowNo >= 0 && nRowNo < nRowCount );
			if( nRowNo >= nRowCount )
				break;
			nRowNoResetFocusBottom = nRowNo + 1;
			nRowNo = SelectionGetNextRowInColumn( 0, nRowNo );
		}
	}
	if( p_htiResetFocus != NULL )
	{
		(*p_htiResetFocus) = NULL;
		if( nRowNoResetFocusBottom >= 0 && nRowNoResetFocusBottom < nRowCount )
		{
			(*p_htiResetFocus) = ItemGetByVisibleRowIndex( nRowNoResetFocusBottom );
			ASSERT( (*p_htiResetFocus) != NULL );
		}
		else if( nRowNoResetFocusTop >= 0 && nRowNoResetFocusTop < nRowCount )
		{
			(*p_htiResetFocus) = ItemGetByVisibleRowIndex( nRowNoResetFocusTop );
			ASSERT( (*p_htiResetFocus) != NULL );
		}
	}

	if( bRedraw && GetSafeHwnd() != NULL )
	{
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	}
	return true;
}

bool CExtReportGridWnd::ReportItemUnRegisterSelection(
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	if(		SelectionIsEmpty()
		||	RowCountGet() == 0
		||	ColumnCountGet() == 0
		)
		return false;
HTREEITEM htiNewFocus = NULL;
	if( ! ReportItemModifySelectionForUnRegistering( false, &htiNewFocus ) )
		return false;
	m_nHelerLockOnSwUpdateScrollBars ++;
	m_nHelerLockEnsureVisibleColumn ++;
	m_nHelerLockEnsureVisibleRow ++;
CTypedPtrList < CPtrList, CExtReportGridItem * > _listToRemove;
LONG nRowNo = SelectionGetFirstRowInColumn( 0 );
	for( ; nRowNo >= 0; )
	{
		HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
		ASSERT( hTreeItem != NULL );
		CExtReportGridItem * pRGI = ReportItemFromTreeItem( hTreeItem );
		ASSERT_VALID( pRGI );
		_listToRemove.AddHead( pRGI );
		if( ItemGetChildCount( hTreeItem ) == 0 )
			nRowNo = SelectionGetNextRowInColumn( 0, nRowNo );
		else
		{
			hTreeItem = ItemGetNext( hTreeItem, false, true, false );
			if( hTreeItem == NULL )
				break;
			nRowNo = ItemGetVisibleIndexOf( hTreeItem );
			ASSERT( nRowNo > 0 );
			nRowNo = SelectionGetNextRowInColumn( 0, nRowNo - 1 );
		}
	}
	SelectionUnset( false, false );
LONG nRowEnsureVisible = -1L;
	if( htiNewFocus != NULL )
	{
		nRowEnsureVisible = ItemGetVisibleIndexOf( htiNewFocus );
		ASSERT( nRowEnsureVisible >= 0 );
	} // if( htiNewFocus != NULL )
POSITION pos = _listToRemove.GetHeadPosition();
	for( ; pos != NULL; )
	{
		CExtReportGridItem * pRGI = _listToRemove.GetNext( pos );
		_ReportItemUnRegisterImpl( pRGI, false, false );
	}
	m_nHelerLockOnSwUpdateScrollBars --;
	m_nHelerLockEnsureVisibleColumn --;
	m_nHelerLockEnsureVisibleRow --;

	if( htiNewFocus != NULL )
	{
		LONG nNewRowCount = RowCountGet();
		if( nNewRowCount > 0 )
		{
			if( nRowEnsureVisible < nNewRowCount )
			{
				nRowEnsureVisible = ItemGetVisibleIndexOf( htiNewFocus );
				ASSERT( nRowEnsureVisible >= 0 );
				ItemFocusSet( htiNewFocus, false );
			} // if( nRowEnsureVisible < nNewRowCount )
			else
			{
				nRowEnsureVisible = nNewRowCount - 1L;
				htiNewFocus = ItemGetByVisibleRowIndex( nRowEnsureVisible );
				ASSERT( htiNewFocus != NULL );
				ItemFocusSet( htiNewFocus, false );
			} // else from if( nRowEnsureVisible < nNewRowCount )
		} // if( nNewRowCount > 0 )
		else
			nRowEnsureVisible = -1L;
	} // if( htiNewFocus != NULL )
	else if( RowCountGet() > 0 )
	{
		nRowEnsureVisible = 0;
		FocusSet( CPoint( FocusGet().x, nRowEnsureVisible ), false, false, true, false );
	}

	if( bRedraw && GetSafeHwnd() != NULL )
	{
		OnSwUpdateScrollBars();
		if( nRowEnsureVisible >= 0L )
			EnsureVisibleRow( nRowEnsureVisible, true );
		else
			OnSwDoRedraw();
	}
	return true;
}

bool CExtReportGridWnd::ReportItemUnRegister(
	CExtReportGridItem * pRGI,
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	return _ReportItemUnRegisterImpl( pRGI, true, bRedraw );
}

bool CExtReportGridWnd::ReportItemIsRegistered(
	const CExtReportGridItem * pRGI
	) const
{
	ASSERT_VALID( this );
	if( pRGI == NULL )
		return false;
	ASSERT_VALID( pRGI );
HTREEITEM htiOwnRoot = ItemGetRoot();
	if( htiOwnRoot == NULL )
		return false;
HTREEITEM htiTestedRoot = (* ( pRGI->TreeNodeGetRoot() ) );
	ASSERT( htiTestedRoot != NULL );
	if( htiTestedRoot != htiOwnRoot )
		return false;
	return true;
}

CExtGridCell * CExtReportGridWnd::ReportItemGetCell(
	const CExtReportGridColumn * pRGC,
	const CExtReportGridItem * pRGI,
	CRuntimeClass * pRTC // = NULL
	)
{
	ASSERT_VALID( this );
	if( pRGC == NULL || pRGI == NULL )
		return NULL;
	ASSERT_VALID( pRGC );
	ASSERT_VALID( pRGI );
// 	if( ! ReportColumnIsRegistered( pRGC ) )
// 		return NULL;
// 	if( ! ReportItemIsRegistered( pRGI ) )
// 		return NULL;
CExtReportGridDataProvider & _DP = _GetReportData();
CExtGridCell * pCell = _DP.ReportItemGetCell( pRGC, pRGI, pRTC );
#ifdef _DEBUG
	if( pCell != NULL )
	{
		ASSERT_VALID( pCell );
	}
#endif // _DEBUG
	return pCell;
}

const CExtGridCell * CExtReportGridWnd::ReportItemGetCell(
	const CExtReportGridColumn * pRGC,
	const CExtReportGridItem * pRGI,
	CRuntimeClass * pRTC // = NULL
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
		-> ReportItemGetCell(
			pRGC,
			pRGI,
			pRTC
			);
}

CExtGridCell * CExtReportGridWnd::ReportItemGetCell(
	__EXT_MFC_SAFE_LPCTSTR strColumnName,
	__EXT_MFC_SAFE_LPCTSTR strCategoryName,
	const CExtReportGridItem * pRGI,
	CRuntimeClass * pRTC // = NULL
	)
{
	ASSERT_VALID( this );
CExtReportGridColumn * pRGC = ReportColumnGet( strColumnName, strCategoryName );
	return
		ReportItemGetCell(
			pRGC,
			pRGI,
			pRTC
			);
}

const CExtGridCell * CExtReportGridWnd::ReportItemGetCell(
	__EXT_MFC_SAFE_LPCTSTR strColumnName,
	__EXT_MFC_SAFE_LPCTSTR strCategoryName,
	const CExtReportGridItem * pRGI,
	CRuntimeClass * pRTC // = NULL
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
		-> ReportItemGetCell(
			strColumnName,
			strCategoryName,
			pRGI,
			pRTC
			);
}

bool CExtReportGridWnd::ReportItemSetCell(
	const CExtReportGridColumn * pRGC,
	const CExtReportGridItem * pRGI,
	const CExtGridCell * pCellNewValue, // = NULL // if NULL - empty existing cell values
	bool bReplace, // = false // false - assign to existing cell instances or column/row type created cells, true - create new cloned copies of pCellNewValue
	CRuntimeClass * pInitRTC // = NULL // runtime class for new cell instance (used if bReplace=false)
	)
{
	ASSERT_VALID( this );
	if( pRGC == NULL || pRGI == NULL )
		return false;
	ASSERT_VALID( pRGC );
	ASSERT_VALID( pRGI );
// 	if( ! ReportColumnIsRegistered( pRGC ) )
// 		return false;
// 	if( ! ReportItemIsRegistered( pRGI ) )
// 		return false;
CExtReportGridDataProvider & _DP = _GetReportData();
bool bRetVal =
		_DP.ReportItemSetCell(
			pRGC,
			pRGI,
			pCellNewValue,
			bReplace,
			pInitRTC
			);
	return bRetVal;
}

bool CExtReportGridWnd::ReportItemSetCell(
	__EXT_MFC_SAFE_LPCTSTR strColumnName,
	__EXT_MFC_SAFE_LPCTSTR strCategoryName,
	const CExtReportGridItem * pRGI,
	const CExtGridCell * pCellNewValue, // = NULL // if NULL - empty existing cell values
	bool bReplace, // = false // false - assign to existing cell instances or column/row type created cells, true - create new cloned copies of pCellNewValue
	CRuntimeClass * pInitRTC // = NULL // runtime class for new cell instance (used if bReplace=false)
	)
{
	ASSERT_VALID( this );
CExtReportGridColumn * pRGC = ReportColumnGet( strColumnName, strCategoryName );
bool bRetVal =
		ReportItemSetCell(
			pRGC,
			pRGI,
			pCellNewValue,
			bReplace,
			pInitRTC
			);
	return bRetVal;
}

CExtReportGridItem * CExtReportGridWnd::ReportItemFocusGet()
{
	ASSERT_VALID( this );
HTREEITEM hTreeItem = ItemFocusGet();
	if( hTreeItem == NULL )
		return NULL;
CExtReportGridItem * pRGI = ReportItemFromTreeItem( hTreeItem );
	ASSERT_VALID( pRGI );
	return pRGI;
}

const CExtReportGridItem * CExtReportGridWnd::ReportItemFocusGet() const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
		-> ReportItemFocusGet();
}

CExtReportGridItem * CExtReportGridWnd::ReportItemFromTreeItem(
	HTREEITEM hTreeItem
	)
{
	ASSERT_VALID( this );
	if( hTreeItem == NULL )
		return NULL;
CExtTreeGridCellNode * pTreeNode =
		CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
	ASSERT_VALID( pTreeNode );
CExtReportGridItem * pRGI =
		STATIC_DOWNCAST( CExtReportGridItem, pTreeNode );
	return pRGI;
}

const CExtReportGridItem * CExtReportGridWnd::ReportItemFromTreeItem(
	HTREEITEM hTreeItem
	) const
{
	ASSERT_VALID( this );
	return
		( const_cast < CExtReportGridWnd * > ( this ) )
		-> ReportItemFromTreeItem( hTreeItem );
}

LONG CExtReportGridWnd::ColumnCountGet() const
{
	ASSERT_VALID( this );
// DWORD dwScrollTypeH = SiwScrollTypeHGet();
// 	if(		dwScrollTypeH == __ESIW_ST_VIRTUAL
// 		||	( ! ExternalDataGet() )
// 		)	
// 		return CExtGridBaseWnd::ColumnCountGet();
CExtGridDataProvider & _DataProvider = _GetReportData();
LONG nCount = _DataProvider.ColumnCountGet();
LONG nOuterBefore = OuterColumnCountLeftGet();
LONG nOuterAfter = OuterColumnCountRightGet();
	nCount -= nOuterBefore + nOuterAfter;
	ASSERT( nCount >= 0L );
	return nCount;
}

LONG CExtReportGridWnd::RowCountGet() const
{
	ASSERT_VALID( this );
// DWORD dwScrollTypeV = SiwScrollTypeVGet();
// 	if(		dwScrollTypeV == __ESIW_ST_VIRTUAL
// 		||	( ! ExternalDataGet() )
// 		)	
// 		return CExtGridBaseWnd::RowCountGet();
const CExtGridDataProvider & _DataProvider = _GetReportData();
LONG nCount = _DataProvider.RowCountGet();
LONG nOuterBefore = CExtTreeGridWnd::OuterRowCountTopGet();
LONG nOuterAfter = CExtTreeGridWnd::OuterRowCountBottomGet();
	nCount -= nOuterBefore + nOuterAfter;
	ASSERT( nCount >= 0L );
	return nCount;
}

bool CExtReportGridWnd::GridSortOrderSetup(
	bool bSortColumns,
	const CExtGridDataSortOrder & _gdsoUpdate,
	bool bUpdateExistingSortOrder, // = false
	bool bInvertIntersectionSortOrder, // = true
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	bSortColumns;
	_gdsoUpdate;
	bUpdateExistingSortOrder;
	bInvertIntersectionSortOrder;
	bRedraw;
	return true;
}

bool CExtReportGridWnd::OnTreeGridQueryDrawOutline(
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( nVisibleColNo >= 0 );
	ASSERT( nVisibleRowNo >= 0 );
	ASSERT( nColNo >= 0 );
	ASSERT( nRowNo >= 0 );
//	return
//		CExtTreeGridWnd::OnTreeGridQueryDrawOutline(
//			dc,
//			nVisibleColNo,
//			nVisibleRowNo,
//			nColNo,
//			nRowNo,
//			rcCellExtra,
//			rcCell,
//			rcVisibleRange,
//			dwAreaFlags,
//			dwHelperPaintFlags
//			);
	dc;
	nVisibleColNo;
	nVisibleRowNo;
	nColNo;
	nRowNo;
	rcCellExtra;
	rcCell;
	rcVisibleRange;
	dwAreaFlags;
	dwHelperPaintFlags;


HTREEITEM hTreeItem =
		ItemGetByVisibleRowIndex( nRowNo );
	ASSERT( hTreeItem != NULL );
DWORD dwReportGridStyle = ReportGridGetStyle();
bool bGroups2003 = ( (dwReportGridStyle&__ERGS_LAYOUT_AND_STYLE_2003) != 0 ) ? true : false;

COLORREF clrRowBk = ::GetSysColor( COLOR_3DFACE );
HTREEITEM hRootItem = ItemGetRoot();
	ASSERT( hRootItem != NULL );
CRect rcIndent(
		rcCell.left, rcCellExtra.top,
		rcCell.left, rcCellExtra.bottom
		);
	if( bGroups2003 )
	{
		rcIndent.left = rcCellExtra.left;
		CExtTreeGridCellNode * pNode =
			CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
		ASSERT_VALID( pNode );
		if( ItemGetChildCount(hTreeItem) > 0 )
		{
			INT nPxIndent = (INT)pNode->TreeNodeIndentPxGet();
			if( nPxIndent <= 0L )
				nPxIndent = GetTreeData().m_nIndentPxDefault;
			rcIndent.right -= nPxIndent;
		}
		COLORREF clrBk2003 =
			(	g_PaintManager.m_UxTheme.IsAppThemed()
			&&	PmBridge_GetPM()->OnQuerySystemTheme() != CExtPaintManager::ThemeUnknown	
			)
				? RGB(253,238,201)
				: OnSiwGetSysColor( COLOR_3DFACE )
				;
		dc.FillSolidRect( &rcIndent, clrBk2003 );
// 		// layout 2003 uses full row grid lines
// 		if( (SiwGetStyle()&__EGBS_GRIDLINES_H) != 0 )
// 		{
// 			COLORREF clrFace = OnSiwGetSysColor( COLOR_3DFACE );
// 			if( ItemGetChildCount( hTreeItem ) == 0 )
// 			{
// 				dc.FillSolidRect(
// 					rcCellExtra.left,
// 					rcCell.top-1,
// 					rcCell.right - rcCellExtra.left,
// 					1,
// 					clrFace
// 					);
// 				dc.FillSolidRect(
// 					rcCellExtra.left,
// 					rcCell.bottom-1,
// 					rcCell.right - rcCellExtra.left,
// 					1,
// 					clrFace
// 					);
// 
// 			const CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
// 			if( _rgso.ColumnCountGet() >= 1 && _rgso.ColumnGroupCountGet() == 1 )
// 				dc.FillSolidRect(
// 					rcCellExtra.left,
// 					rcCell.bottom-1,
// 					rcCell.right - rcCellExtra.left,
// 					1,
// 					clrFace
// 					);
// 			
// 			} // if( ItemGetChildCount( hTreeItem ) == 0 )
// 			else
// 			{ 
// 				if(		ItemIsExpanded( hTreeItem )
// 					&&	ItemGetChildCount( ItemGetChildAt( hTreeItem, 0 ) ) == 0
// 					)
// 					dc.FillSolidRect(
// 						rcCellExtra.left,
// 						rcCellExtra.bottom-1,
// 						rcCellExtra.right-rcCellExtra.left, //rcCell.right - rcCellExtra.left + nPxIndent,
// 						1,
// 						clrFace
// 						);
// 			} // if( ItemGetChildCount( hTreeItem ) == 0 )
// 		}
		return false;
	}
COLORREF clrEntireBk = OnSiwGetSysColor( COLOR_WINDOW );

	for( ; true; )
	{
		ASSERT( hTreeItem != NULL );
		CExtTreeGridCellNode * pNode =
			CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
		ASSERT_VALID( pNode );
		INT nPxIndent = (INT)pNode->TreeNodeIndentPxGet();
		if( nPxIndent == ULONG(-1L) )
			nPxIndent = GetTreeData().m_nIndentPxDefault;
		rcIndent.right = rcIndent.left;
		rcIndent.left -= nPxIndent;
		CRect rcIndentPaint = rcIndent;
		dc.FillSolidRect( &rcIndentPaint, clrEntireBk );
			rcIndentPaint.right --;
		dc.FillSolidRect( &rcIndentPaint, clrRowBk );
		hTreeItem = ItemGetParent( hTreeItem );
		if( hTreeItem == hRootItem )
			break;
	}

	return false;
}

bool CExtReportGridWnd::OnTreeGridQueryKbCollapseEnabled(
	HTREEITEM hTreeItem,
	LONG nColNo
	)
{
	ASSERT_VALID( this );
	ASSERT( hTreeItem != NULL );
	if( (SiwGetStyle()&__EGBS_SFB_MASK) != __EGBS_SFB_CELLS )
		return true;
	if( nColNo <= 0 )
		return true;
	if( ItemGetChildCount( hTreeItem ) > 0 )
		return true;
	return false;
}

bool CExtReportGridWnd::OnTreeGridQueryKbExpandEnabled(
	HTREEITEM hTreeItem,
	LONG nColNo
	)
{
	ASSERT_VALID( this );
	ASSERT( hTreeItem != NULL );
	hTreeItem;
	nColNo;
	return true;
}

bool CExtReportGridWnd::OnTreeGridQueryKbJumpParentEnabled(
	HTREEITEM hTreeItem,
	LONG nColNo
	)
{
	ASSERT_VALID( this );
	ASSERT( hTreeItem != NULL );
	if( (SiwGetStyle()&__EGBS_SFB_MASK) != __EGBS_SFB_CELLS )
		return true;
	if( nColNo <= 0 )
		return true;
	if( ItemGetChildCount( hTreeItem ) > 0 )
		return true;
	return false;
}

COLORREF CExtReportGridWnd::OnGridCellQueryTextColor(
	const CExtGridCell & _cell,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
COLORREF clrText =
		CExtTreeGridWnd::OnGridCellQueryTextColor(
			_cell,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			dwAreaFlags,
			dwHelperPaintFlags
			);
	if( clrText != COLORREF(-1L) )
		return clrText;
	if( (SiwGetStyle()&__EGBS_SFB_MASK) == __EGBS_SFB_CELLS )
	{
		if( nColType == 0 && nRowType == 0 && nColNo >= 0 && nRowNo >= 0 )
		{
			CPoint ptFocus = FocusGet();
			if( ptFocus.x == nColNo && ptFocus.y == nRowNo )
				return OnSiwGetSysColor( COLOR_BTNTEXT );
		}
	} // if( (SiwGetStyle()&__EGBS_SFB_MASK) == __EGBS_SFB_CELLS )
	return COLORREF(-1L);
}

bool CExtReportGridWnd::SelectionGetForCellPainting(
	LONG nColNo,
	LONG nRowNo
	) const
{
	ASSERT_VALID( this );
	if( ! CExtTreeGridWnd::SelectionGetForCellPainting( nColNo, nRowNo ) )
		return false;
	if( (SiwGetStyle()&__EGBS_SFB_MASK) == __EGBS_SFB_CELLS )
	{
		CPoint ptFocus = FocusGet();
		if(		nColNo == ptFocus.x
			&&	nRowNo == ptFocus.y
			)
		{
			HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
			ASSERT( hTreeItem  != NULL );
			if( ItemGetChildCount( hTreeItem ) > 0 )
				return true;
			return false;
		}
	} // if( (SiwGetStyle()&__EGBS_SFB_MASK) == __EGBS_SFB_CELLS )
	return true;
}

CRect & CExtReportGridWnd::_SelectionAreaConvert( CRect & rcArea ) const
{
	ASSERT_VALID( this );
	rcArea.left = 0;
	rcArea.right = ColumnCountGet();
	if( rcArea.right > 0 )
		rcArea.right --;
	return rcArea;
}

bool CExtReportGridWnd::SelectionGetForCell(
	LONG nColNo,
	LONG nRowNo
	) const
{
	ASSERT_VALID( this );
	nColNo;
bool bRetVal = CExtTreeGridWnd::SelectionGetForCell( 0, nRowNo );
	return bRetVal;
}

bool CExtReportGridWnd::OnTreeGridQueryColumnOutline(
	LONG nColNo
	) const
{
	ASSERT_VALID( this );
const CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
LONG nColumnGroupCount = _rgso.ColumnGroupCountGet();
	if( nColumnGroupCount == 0 )
		return false;
bool bRetVal = CExtTreeGridWnd::OnTreeGridQueryColumnOutline( nColNo );
	return bRetVal;
}

bool CExtReportGridWnd::OnGbwQueryCellGridLines(
	bool bHorz,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
	if(		bHorz
		&&	m_nHelerLockDrawHL != 0L
		)
		return false;
DWORD dwReportGridStyle = ReportGridGetStyle();
bool bGroups2003 = ( (dwReportGridStyle&__ERGS_LAYOUT_AND_STYLE_2003) != 0 ) ? true : false;
	if( bGroups2003 )
	{
		HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
		ASSERT( hTreeItem != NULL );
		LONG nChildCount = ItemGetChildCount( hTreeItem );
		if( nChildCount > 0 )
			return false;
	}
bool bUseGridLines =
		CExtTreeGridWnd::OnGbwQueryCellGridLines(
			bHorz,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
	if( ! bUseGridLines )	
		return false;
	if( ! bGroups2003 )
	{
		HTREEITEM hTreeItem =
			ItemGetByVisibleRowIndex( nRowNo );
		ASSERT( hTreeItem != NULL );
		LONG nChildCount = ItemGetChildCount( hTreeItem );
		if( nChildCount == 0 )
			return true;
	}
	return bUseGridLines;
}

void CExtReportGridWnd::OnGbwAdjustRects(
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	RECT & rcCellExtraA,
	RECT & rcCellA
	) const
{
	ASSERT_VALID( this );
	CExtTreeGridWnd::OnGbwAdjustRects(
		nColNo,
		nRowNo,
		nColType,
		nRowType,
		rcCellExtraA,
		rcCellA
		);
	if( nColType != 0 || nRowType != 0 )
		return;
HTREEITEM hTreeItem =
		ItemGetByVisibleRowIndex( nRowNo );
	ASSERT( hTreeItem != NULL );
	if( ItemGetChildCount(hTreeItem) == 0 )
	{
		if( OnTreeGridQueryColumnOutline( nColNo ) )
		{
			DWORD dwReportGridStyle = ReportGridGetStyle();
			bool bGroups2003 = ( (dwReportGridStyle&__ERGS_LAYOUT_AND_STYLE_2003) != 0 ) ? true : false;
			if(		bGroups2003
				&&	ItemGetParent( hTreeItem ) != ItemGetRoot()
				)
			{
				CExtTreeGridCellNode * pNode =
					CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
				ASSERT_VALID( pNode );
				CExtReportGridDataProvider & _DP = _GetReportData();
				INT nPxIndentRow = INT( pNode->TreeNodeIndentPxGet() );
				if( nPxIndentRow <= 0 )
					nPxIndentRow = INT( _DP.m_nIndentPxDefault );
				rcCellA.left -= nPxIndentRow;
				rcCellExtraA.left -= nPxIndentRow;
			}
		}
		return;
	}
LONG nColFirst = 0, nColLast = 0, nColFirstOffset = 0;
	OnGbwQueryVisibleColumnRange(
		nColFirst,
		nColLast,
		nColFirstOffset
		);
	ASSERT( nColFirst <= nColLast );
	if( nColNo != nColFirst )
	{
		::SetRectEmpty( &rcCellA );
		::SetRectEmpty( &rcCellExtraA );
		return;
	} // if( nColNo != nColFirst )
//CExtReportGridDataProvider & _DP = _GetReportData();
//LONG nIndentPx = (LONG)_DP.TreeGetRowIndentPx( nRowNo );
CRect rcClient = OnSwGetClientRect(); 
CSize _sizeTotal = OnSwGetTotalSize();
CPoint _pos = OnSwGetScrollPos();
DWORD dwBseStyleEx = BseGetStyleEx();
DWORD dwScrollTypeH = SiwScrollTypeHGet();
	if(		dwScrollTypeH == __ESIW_ST_NONE
		&&	(dwBseStyleEx&__EGBS_BSE_EX_PROPORTIONAL_COLUMN_WIDTHS) != 0
		)
	{
		_sizeTotal.cx = OnSwGetClientRect().Width();
		_pos.x = 0;
	}
	if( _sizeTotal.cx < 0 )
		_sizeTotal.cx = 0;
	if( _sizeTotal.cy < 0 )
		_sizeTotal.cy = 0;
//LONG nLeft = rcClient.left + nIndentPx - _pos.x;
LONG nRight = rcClient.left + _sizeTotal.cx - _pos.x;
// 	rcCellA.left = rcCellExtraA.left = nLeft;
	rcCellA.right = rcCellExtraA.right = nRight;
	if( rcCellA.right < rcCellA.left )
		rcCellA.right = rcCellA.left;
	if( rcCellExtraA.right < rcCellExtraA.left )
		rcCellExtraA.right = rcCellExtraA.left;
}

void CExtReportGridWnd::OnGbwPaintCell(
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( nVisibleColNo >= 0 );
	ASSERT( nVisibleRowNo >= 0 );
	ASSERT( nColNo >= 0 );
	ASSERT( nRowNo >= 0 );
DWORD dwReportGridStyle = ReportGridGetStyle();
bool bGroups2003 = ( (dwReportGridStyle&__ERGS_LAYOUT_AND_STYLE_2003) != 0 ) ? true : false;
HTREEITEM hTreeItem = NULL;
LONG nChildCount = 0L;
CExtReportGridItem * pRGI = NULL;
	if( ( dwAreaFlags & __EGBWA_INNER_CELLS ) != 0 )
	{
		hTreeItem =
			ItemGetByVisibleRowIndex( nRowNo );
		ASSERT( hTreeItem != NULL );
		nChildCount = ItemGetChildCount( hTreeItem );
		if( nChildCount != 0L )
		{
			LONG nColFirst = 0, nColLast = 0, nColFirstOffset = 0;
			OnGbwQueryVisibleColumnRange(
				nColFirst,
				nColLast,
				nColFirstOffset
				);
			ASSERT( nColFirst <= nColLast );
			if( nColNo != nColFirst )
				return;
			CExtTreeGridCellNode * pNode = CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
			ASSERT_VALID( pNode );
			ASSERT_KINDOF( CExtReportGridItem, pNode );
			pRGI = STATIC_DOWNCAST( CExtReportGridItem, pNode );
			if( bGroups2003 )
			{
				CRect rcGroupUnderline2003 = rcCellExtra;
				//rcGroupUnderline2003.bottom --;
				rcGroupUnderline2003.top = rcGroupUnderline2003.bottom - 2;
				COLORREF clr = 0;
				if( g_PaintManager.m_UxTheme.IsAppThemed() )
				{
					switch( PmBridge_GetPM()->OnQuerySystemTheme() )
					{
					case CExtPaintManager::ThemeLunaBlue:
						clr = RGB(123,164,224);
					break;
					case CExtPaintManager::ThemeLunaOlive:
						clr = RGB(181,196,143);
					break;
					case CExtPaintManager::ThemeLunaSilver:
						clr = RGB(165,164,189);
					break;
					default:
						clr = OnSiwGetSysColor( COLOR_3DSHADOW );
					break;
					} // switch( PmBridge_GetPM()->OnQuerySystemTheme() )
				} // if( g_PaintManager.m_UxTheme.IsAppThemed() )
				else
					clr = OnSiwGetSysColor( COLOR_3DSHADOW );
				dc.FillSolidRect( &rcGroupUnderline2003, clr );
			}
		}
		if(		( ! ::EqualRect( &rcCell, &rcCellExtra ) )
			&&	SelectionGetForCell( nColNo, nRowNo )
			)
		{
			dc.FillSolidRect(
				&rcCellExtra,
				OnSiwGetSysColor(
					OnSiwQueryFocusedControlState()
						? COLOR_HIGHLIGHT
						: COLOR_3DFACE
						)
				);
			if( (SiwGetStyle()&__EGBS_SFB_MASK) == __EGBS_SFB_CELLS )
			{
				CPoint ptFocus = FocusGet();
				if( ptFocus.x == nColNo && ptFocus.y == nRowNo )
					dc.FillSolidRect(
						&rcCell,
						OnSiwGetSysColor( COLOR_WINDOW )
						);
			}
		} // if( rcCell != rcCellExtra ...
	} // if( ( dwAreaFlags & __EGBWA_INNER_CELLS ) != 0 )
	m_nHelerLockSiwDrawFocusRect ++;
	if( hTreeItem != NULL )
		m_nHelerLockDrawHL ++;
	CExtTreeGridWnd::OnGbwPaintCell(
		dc,
		nVisibleColNo,
		nVisibleRowNo,
		nColNo,
		nRowNo,
		rcCellExtra,
		rcCell,
		rcVisibleRange,
		dwAreaFlags,
		dwHelperPaintFlags
		);
	if( hTreeItem != NULL )
		m_nHelerLockDrawHL --;
	// grid lines
	if(		GridLinesHorzGet()
		&&	hTreeItem != NULL
		&&	nChildCount == 0L
		)
	{
		COLORREF clrFace = OnSiwGetSysColor( COLOR_3DFACE );
// 		dc.FillSolidRect(
// 			rcCell.left,
// 			rcCellExtra.top-1,
// 			rcCell.right - rcCell.left,
// 			1,
// 			clrFace
// 			);
		dc.FillSolidRect(
			rcCell.left,
			rcCellExtra.bottom-1,
			rcCell.right - rcCell.left,
			1,
			clrFace
			);
		if(		(	(! bGroups2003 )
				||	ReportAutoPreviewModeGet()
				)
			&&	GridLinesVertGet()
			)
			dc.FillSolidRect(
				rcCell.left,
				rcCell.bottom-1,
				rcCell.right - rcCell.left,
				1,
				clrFace
				);
	}
	if( pRGI != NULL )
	{
		CRect rcRowExtra = rcCellExtra;
		CRect rcRow = rcCell;
		if( GridLinesVertGet() )
		{
			rcRow.left -- ;
			if( rcRowExtra.left > rcRow.left )
				rcRowExtra.left = rcRow.left;
		} // if( GridLinesVertGet() )
		OnReportGridPaintGroupRow(
			dc,
			pRGI,
			nVisibleRowNo,
			nRowNo,
			rcRowExtra,
			rcRow,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
	} // if( pRGI != NULL )
	m_nHelerLockSiwDrawFocusRect --;
}

void CExtReportGridWnd::OnGridCalcOuterDropTarget(
	const CExtGridHitTestInfo & htInfoDrag,
	CExtGridHitTestInfo & htInfoDrop,
	CPoint point
	)
{
	ASSERT_VALID( this );
	htInfoDrop.Empty();
	if( htInfoDrag.IsHoverEmpty() )
		return;
LONG nColCountActiveOnly = ReportColumnGetCount( true, false );
	if( nColCountActiveOnly == 0L )
	{
		CRect rcHeader = OnSwGetClientRect();
		rcHeader.bottom = rcHeader.top;
		rcHeader.top -= OuterRowHeightGet( true, 0L );
		if( rcHeader.PtInRect( point ) )
		{
			htInfoDrop.m_ptClient = point;
			htInfoDrop.m_dwAreaFlags = __EGBWA_OUTER_TOP|__EGBWA_OUTER_CELLS;
			htInfoDrop.m_nRowNo = 0;
			htInfoDrop.m_nColNo = 0;
			CRect rcHT = rcHeader;
			rcHT.right = rcHT.left + 10;
			htInfoDrop.m_rcItem = htInfoDrop.m_rcExtra = htInfoDrop.m_rcPart = rcHT;
			return;
		} // if( rcHeader.PtInRect( point ) )
	} // if( nColCountActiveOnly == 0L )
	CExtTreeGridWnd::OnGridCalcOuterDropTarget(
		htInfoDrag,
		htInfoDrop,
		point
		);
}

INT CExtReportGridWnd::OuterColumnWidthGet(
	bool bLeft,
	LONG nColNo,
	bool bZeroIfNoRows // = true
	) const
{
	ASSERT_VALID( this );
	bZeroIfNoRows;
	return
		CExtTreeGridWnd::OuterColumnWidthGet(
			bLeft,
			nColNo,
			false
			);
}

LONG CExtReportGridWnd::OuterColumnCountLeftGet() const
{
	ASSERT_VALID( this );
	return CExtTreeGridWnd::OuterColumnCountLeftGet();
}

LONG CExtReportGridWnd::OuterColumnCountRightGet() const
{
	ASSERT_VALID( this );
	return CExtTreeGridWnd::OuterColumnCountRightGet();
}

LONG CExtReportGridWnd::OuterColumnCountLeftSet(
	LONG nCount, // = 0L
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT( FALSE ); // custom outer columns are not enabled in report grid
	nCount;
	bRedraw;
	return 0L;
}

LONG CExtReportGridWnd::OuterColumnCountRightSet(
	LONG nCount, // = 0L
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT( FALSE ); // custom outer columns are not enabled in report grid
	nCount;
	bRedraw;
	return 0L;
}

LONG CExtReportGridWnd::OuterRowCountTopGet() const
{
	ASSERT_VALID( this );
	return CExtTreeGridWnd::OuterRowCountTopGet();
}

LONG CExtReportGridWnd::OuterRowCountBottomGet() const
{
	ASSERT_VALID( this );
	return CExtTreeGridWnd::OuterRowCountBottomGet();
}

LONG CExtReportGridWnd::OuterRowCountTopSet(
	LONG nCount, // = 0L
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT( FALSE ); // custom outer rows are not enabled in report grid
	nCount;
	bRedraw;
	return 0L;
}

LONG CExtReportGridWnd::OuterRowCountBottomSet(
	LONG nCount, // = 0L
	bool bRedraw // = true
	)
{
	ASSERT_VALID( this );
	ASSERT( FALSE ); // custom outer rows are not enabled in report grid
	nCount;
	bRedraw;
	return 0L;
}

INT CExtReportGridWnd::OuterRowHeightGet(
	bool bTop,
	LONG nRowNo,
	bool bZeroIfNoColumns // = true
	) const
{
	ASSERT_VALID( this );
	bZeroIfNoColumns;
	return
		CExtTreeGridWnd::OuterRowHeightGet(
			bTop,
			nRowNo,
			false
			);
}

void CExtReportGridWnd::OnGbwResizingStateUpdate(
	bool bInitial,
	bool bFinal,
	const CPoint * p_ptClient // = NULL
	)
{
	ASSERT_VALID( this );
	m_nGbwAdjustRectsLock ++;
	CExtTreeGridWnd::OnGbwResizingStateUpdate(
		bInitial,
		bFinal,
		p_ptClient
		);
	m_nGbwAdjustRectsLock --;
}

bool CExtReportGridWnd::OnGbwBeginEdit(
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcInplaceControl,
	bool bContinueMsgLoop, // = true
	__EXT_MFC_SAFE_LPCTSTR strStartEditText // = NULL
	)
{
	ASSERT_VALID( this );
	if( nColType != 0 || nRowType != 0 )
		return false;
HTREEITEM hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
	if( hTreeItem == NULL )
		return false;
	if( ItemGetChildCount( hTreeItem ) != 0 )
		return false;
	return
		CExtTreeGridWnd::OnGbwBeginEdit(
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcInplaceControl,
			bContinueMsgLoop,
			strStartEditText
			);
}

void CExtReportGridWnd::OnGridCellInputComplete(
	CExtGridCell & _cell,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	HWND hWndInputControl // = NULL
	)
{
	ASSERT_VALID( this );
HWND hWndOwn = m_hWnd;
	CExtTreeGridWnd::OnGridCellInputComplete(
		_cell,
		nColNo,
		nRowNo,
		nColType,
		nRowType,
		hWndInputControl
		);
	if(		hWndOwn == NULL
		||	(! IsWindow(hWndOwn) )
		||	CWnd::FromHandlePermanent( hWndOwn ) != this
		)
		return;
	if( nColType != 0 || nRowType != 0 )
		return;
const CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
	if( _rgso.ColumnCountGet() == 0 )
		return;
const CExtReportGridColumn * pRGC =
		STATIC_DOWNCAST(
			CExtReportGridColumn,
			GridCellGetOuterAtTop( nColNo, 0L )
			);
	ASSERT_VALID( pRGC );
	ASSERT( ReportColumnIsRegistered( pRGC ) );
	if( ! pRGC->SortingEnabledGet() )
		return;
	if( _rgso.ColumnGetIndexOf( pRGC ) < 0 )
		return;
	ReportSortOrderUpdate();
}

void CExtReportGridWnd::OnSiwDrawFocusRect(
	CDC &dc,
	LPCRECT pRect,
	CObject * pObjSrc, // = NULL
	LPARAM lParam // = 0L
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( m_nHelerLockSiwDrawFocusRect != 0 )
		return;
	CExtTreeGridWnd::OnSiwDrawFocusRect(
		dc,
		pRect,
		pObjSrc,
		lParam
		);
}

void CExtReportGridWnd::OnReportGridPaintGroupRow(
	CDC & dc,
	const CExtReportGridItem * pRGI,
	LONG nVisibleRowNo,
	LONG nRowNo,
	const RECT & rcRowExtra,
	const RECT & rcRow,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT_VALID( pRGI );
	ASSERT( nVisibleRowNo >= 0 );
	ASSERT( nRowNo >= 0 );
	nVisibleRowNo;
	rcRowExtra;
	rcVisibleRange;
	dwAreaFlags;
	dwHelperPaintFlags;
DWORD dwReportGridStyle = ReportGridGetStyle();
bool bGroups2003 = ( (dwReportGridStyle&__ERGS_LAYOUT_AND_STYLE_2003) != 0 ) ? true : false;
COLORREF clrLineH = ::GetSysColor( COLOR_WINDOW );
CExtReportGridDataProvider & _DP = _GetReportData();
INT nPxIndentRow = INT( pRGI->TreeNodeIndentPxGet() );
	if( nPxIndentRow < 0 )
		nPxIndentRow = INT( _DP.m_nIndentPxDefault );
CPoint ptFocus = FocusGet();
bool bFocused = ( ptFocus.y == nRowNo ) ? true : false;
bool bSelected = SelectionGetForCell( 0, nRowNo );
	if( bGroups2003 )
	{
	} // if( bGroups2003 )
	else
	{
		CRect rcRowFill = rcRowExtra;
		rcRowFill.left = rcRow.left;
		rcRowFill.left -= nPxIndentRow - 1;
		COLORREF clrRowBk = ::GetSysColor( COLOR_3DFACE );
		dc.FillSolidRect( &rcRowFill, clrRowBk );
		if( bSelected || bFocused )
			rcRowFill.bottom --;
		if( bSelected )
		{
			COLORREF clrSel =
				::GetSysColor(
					( ( dwHelperPaintFlags & __EGCPF_FOCUSED_CONTROL ) != 0 )
						? COLOR_HIGHLIGHT
						: COLOR_3DFACE
					);
			dc.FillSolidRect( &rcRowFill, clrSel );
		} // if( bSelected )
		CRect rcUpperLine(
			rcRow.left - nPxIndentRow,
			rcRowExtra.top - 1,
			rcRowExtra.right,
			rcRowExtra.top
			);
		dc.FillSolidRect( &rcUpperLine, clrLineH );
	} // else from if( bGroups2003 )

CExtTreeGridCellNode::e_expand_box_shape_t eEBS =
		pRGI->TreeNodeGetExpandBoxShape();
	if( eEBS != CExtTreeGridCellNode::__EEBS_NONE )
	{
		CRect rcIndent(
			rcRow.left, rcRow.top,
			rcRow.left, rcRow.bottom
			);
		rcIndent.right = rcIndent.left;
		rcIndent.left -= nPxIndentRow;
		CRect rcIndentPaint = rcIndent;
		OnTreeGridPaintExpandButton(
			dc,
			*pRGI,
			( eEBS == CExtTreeGridCellNode::__EEBS_EXPANDED )
				? true : false,
			rcIndent
			);
	} // if( eEBS != CExtTreeGridCellNode::__EEBS_NONE )

LONG nGroupIndex = LONG( pRGI->TreeNodeIndentCompute() );
	ASSERT( nGroupIndex > 0 );
	nGroupIndex --; // suppress root indent
CExtSafeString strRowText;
	OnReportGridFormatGroupRowText( pRGI, strRowText, nGroupIndex );
	if( ! strRowText.IsEmpty() )
	{
		CRect rcRowText = rcRow;
		if( OnSwHasScrollBar( true ) )
		{
			rcRowText.left -= ((CExtScrollWnd*)this)->OnSwGetScrollPos().x;
			rcRowText.right = rcRow.left + ((CExtScrollWnd*)this)->OnSwGetTotalSize().cx;
		}
		rcRowText.DeflateRect( 1, 0, 3, 0 );
		COLORREF clrTextOld = 0;
		if( bGroups2003 )
		{
			if(		g_PaintManager.m_UxTheme.IsAppThemed()
				&&	PmBridge_GetPM()->OnQuerySystemTheme() != CExtPaintManager::ThemeUnknown	
				)
			{
				COLORREF clr = 0;
				if( bSelected && ( dwHelperPaintFlags & __EGCPF_FOCUSED_CONTROL ) != 0 )
					clr = OnSiwGetSysColor( COLOR_WINDOW );
				else
				{
					switch( PmBridge_GetPM()->OnQuerySystemTheme() )
					{
					case CExtPaintManager::ThemeLunaBlue:
						clr = RGB(55,104,185);
					break;
					case CExtPaintManager::ThemeLunaOlive:
						clr = RGB(115,137,84);
					break;
					case CExtPaintManager::ThemeLunaSilver:
						clr = RGB(112,111,145);
					break;
					default:
						clr = OnSiwGetSysColor( COLOR_WINDOW );
					break;
					} // switch( PmBridge_GetPM()->OnQuerySystemTheme() )
				} // else from if( bSelected && ( dwHelperPaintFlags & __EGCPF_FOCUSED_CONTROL ) != 0 )
				clrTextOld = dc.SetTextColor( clr );
			}
			else
				clrTextOld =
					dc.SetTextColor(
						OnSiwGetSysColor(
							( bSelected && ( dwHelperPaintFlags & __EGCPF_FOCUSED_CONTROL ) != 0 )
								? COLOR_HIGHLIGHTTEXT
								: COLOR_3DSHADOW
							)
						);
		}
		else
			clrTextOld = 
				dc.SetTextColor(
					::GetSysColor(
						( bSelected && ( dwHelperPaintFlags & __EGCPF_FOCUSED_CONTROL ) != 0 )
							? COLOR_HIGHLIGHTTEXT
							: COLOR_BTNTEXT
						)
					);
		bool bBoldGroups = ( (ReportGridGetStyle()&__ERGS_BOLD_GROUPS) != 0 ) ? true : false;
		CExtPaintManager * pPM = PmBridge_GetPM();
		CFont * pOldFont =
			dc.SelectObject(
				bBoldGroups
					? (&(pPM->m_FontBold))
					: (&(pPM->m_FontNormal))
					);
		dc.DrawText(
			LPCTSTR(strRowText),
			strRowText.GetLength(),
			&rcRowText,
			DT_LEFT|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS
			);
		dc.SelectObject( pOldFont );
		dc.SetTextColor( clrTextOld );
	} // if( ! strRowText.IsEmpty() )

// 	if(		bFocused
// 		&&	( dwHelperPaintFlags & __EGCPF_FOCUSED_CONTROL ) != 0
// 		)
// 		dc.DrawFocusRect( &rcRowFill );
}

void CExtReportGridWnd::OnReportGridFormatGroupRowText(
	const CExtReportGridItem * pRGI,
	CExtSafeString & strRowText,
	LONG nGroupIndex
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGI );
	ASSERT( nGroupIndex >= 0 );
	strRowText.Empty();
HTREEITEM hTreeItem = *pRGI;
	ASSERT( ItemGetChildCount(hTreeItem) > 0 );
	hTreeItem = ItemGetChildAt( hTreeItem, 0 );
	ASSERT( hTreeItem != NULL );
	for( ; true; hTreeItem = ItemGetChildAt( hTreeItem, 0 ) )
	{
		if( ItemGetChildCount(hTreeItem) == 0 )
			break;
	} // for( ; true; hTreeItem = ItemGetChildAt( hTreeItem, 0 ) )
CExtTreeGridCellNode * pNode = CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
	ASSERT_VALID( pNode );
	ASSERT_KINDOF( CExtReportGridItem, pNode );
CExtReportGridItem * pEffectivrRGI = STATIC_DOWNCAST( CExtReportGridItem, pNode );
const CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
const CExtReportGridColumn * pRGC = _rgso.ColumnGetAt( nGroupIndex );
	ASSERT_VALID( pRGC );
const CExtGridCell * pValueCell = ReportItemGetCell( pRGC, pEffectivrRGI );
	if( pValueCell == NULL )
		return;
	ASSERT_VALID( pValueCell );
CExtSafeString strValue;
	pValueCell->TextGet( strValue );
__EXT_MFC_SAFE_LPCTSTR strColumnName = pRGC->ColumnNameGet();
	strRowText.Format(
		_T("%s : %s"),
		( strColumnName == NULL ) ? _T("") : LPCTSTR(strColumnName),
		strValue.IsEmpty() ? _T("") : LPCTSTR(strValue)
		);
}

void CExtReportGridWnd::OnTreeGridPaintExpandButton(
	CDC & dc,
	HTREEITEM hTreeItem,
	bool bExpanded,
	const CRect & rcIndent
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	ASSERT( hTreeItem != NULL );

	if( (ReportGridGetStyle()&__ERGS_LAYOUT_AND_STYLE_2003) != 0 )
	{
		if( m_iconTreeBoxExpanded.IsEmpty() )
			PmBridge_GetPM()->LoadWinXpTreeBox(
				m_iconTreeBoxExpanded,
				true,
				true
				);
		if( m_iconTreeBoxCollapsed.IsEmpty() )
			PmBridge_GetPM()->LoadWinXpTreeBox(
				m_iconTreeBoxCollapsed,
				false,
				true
				);
	}
	else
	{
		m_iconTreeBoxExpanded.Empty();
		m_iconTreeBoxCollapsed.Empty();
	}

	if(		( ! m_iconTreeBoxExpanded.IsEmpty() )
		&&	( ! m_iconTreeBoxCollapsed.IsEmpty() )
		)
	{
		CExtTreeGridWnd::OnTreeGridPaintExpandButton(
			dc,
			hTreeItem,
			bExpanded,
			rcIndent
			);
		return;
	}
CRect rcRenderBox(
		rcIndent.left,
		rcIndent.top,
		rcIndent.left + 12,
		rcIndent.top + 12
		);
	rcRenderBox.OffsetRect(
		( rcIndent.Width() - rcRenderBox.Width() ) / 2,
		( rcIndent.Height() - rcRenderBox.Height() ) / 2
		);
COLORREF clrFace = ::GetSysColor( COLOR_3DFACE );
COLORREF clrLight = ::GetSysColor( COLOR_3DHIGHLIGHT );
COLORREF clrShadow = ::GetSysColor( COLOR_3DSHADOW );
COLORREF clrDkShadow = ::GetSysColor( COLOR_3DDKSHADOW );
COLORREF clrLine = ::GetSysColor( COLOR_BTNTEXT );
	dc.FillSolidRect( &rcRenderBox, clrFace );
	dc.Draw3dRect( &rcRenderBox, clrLight, clrDkShadow );
	rcRenderBox.DeflateRect( 1, 1 );
	dc.Draw3dRect( &rcRenderBox, clrFace, clrShadow );
	rcRenderBox.DeflateRect( 2, 2 );
INT nHW = rcRenderBox.Width()/2;
INT nHH = rcRenderBox.Height()/2;
CPen _pen( PS_SOLID, 1, clrLine );
CPen * pOldPen = dc.SelectObject( &_pen );
	if( ! bExpanded )
	{
		dc.MoveTo(
			rcRenderBox.left + nHW - 1,
			rcRenderBox.top
			);
		dc.LineTo(
			rcRenderBox.left + nHW - 1,
			rcRenderBox.bottom - 1
			);
	} // if( ! bExpanded )
	dc.MoveTo(
		rcRenderBox.left,
		rcRenderBox.top + nHH - 1
		);
	dc.LineTo(
		rcRenderBox.right - 1,
		rcRenderBox.top + nHH - 1
		);
	dc.SelectObject( pOldPen );
}

void CExtReportGridWnd::OnReportGridFormatSortByToolTipText(
	CExtSafeString & str,
	const CExtReportGridColumn * pRGC
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGC );
LPCTSTR strColumnName = pRGC->ColumnNameGet();
CExtSafeString strFormat;
	if( ! g_ResourceManager->LoadString(
			strFormat,
			IDS_EXT_RG_SORT_BY_FORMAT_STRING
			)
		)
		strFormat = _T("Sort by: %s");
	str.Format(
		LPCTSTR(strFormat),
		( strColumnName != NULL ) ? strColumnName : _T("")
		);
}

void CExtReportGridWnd::OnReportGridColumnChooserNoFieldsAvailableLabel( CExtSafeString & str ) const
{
	ASSERT_VALID( this );
	if( ! g_ResourceManager->LoadString(
			str,
			IDS_EXT_RG_NO_FIELDS_AVAILABLE
			)
		)
		str = _T("(no fields available)");
}

void CExtReportGridWnd::OnReportGridColumnChooserQueryCaption( CExtSafeString & str ) const
{
	ASSERT_VALID( this );
	if( ! g_ResourceManager->LoadString(
			str,
			IDS_EXT_RG_COLUMN_CHOOSER
			)
		)
		str = _T("Field Chooser");
}

void CExtReportGridWnd::OnReportGridGroupAreaQueryEmptyText( CExtSafeString & str ) const
{
	ASSERT_VALID( this );
	if( ! g_ResourceManager->LoadString(
			str,
			IDS_EXT_RG_EMPTY_GROUP_AREA_LABEL
			)
		)
		str = _T("Drag a column header here to group by that content.");
}

void CExtReportGridWnd::OnReportGridEmptyDataMessage( CExtSafeString & str ) const
{
	ASSERT_VALID( this );
	if( ! g_ResourceManager->LoadString(
			str,
			IDS_EXT_RG_EMPTY_GRID_LABEL
			)
		)
		str = _T("There are no items to show in this view.");
}

void CExtReportGridWnd::OnReportGridSortOrderVerificationFailed(
	const CExtReportGridSortOrder & _rgso,
	LONG nFailureIndex,
	bool bGroupingFailed
	)
{
	ASSERT_VALID( this );
	_rgso;
	nFailureIndex;
CExtSafeString strMsg, strColumnName;
const CExtReportGridColumn * pRGC = _rgso.ColumnGetAt( nFailureIndex );
	ASSERT_VALID( pRGC );
	pRGC->TextGet( strColumnName );
	if( strColumnName.IsEmpty() )
	{
		if( ! g_ResourceManager->LoadString(
				strMsg,
				bGroupingFailed
					? IDS_EXT_RG_UNAVAILABLE_GROUPING_FOR_UNNAMED_COLUMN
					: IDS_EXT_RG_UNAVAILABLE_SORTING_FOR_UNNAMED_COLUMN
				)
			)
			strMsg =
				bGroupingFailed
					? _T("You cannot group items by this column.")
					: _T("You cannot sort items by this column.")
					;
	} // if( strColumnName.IsEmpty() )
	else
	{
		CExtSafeString strMsgFormat;
		if( ! g_ResourceManager->LoadString(
				strMsgFormat,
				bGroupingFailed
					? IDS_EXT_RG_UNAVAILABLE_GROUPING_FORMAT
					: IDS_EXT_RG_UNAVAILABLE_SORTING_FORMAT
				)
			)
			strMsgFormat =
				bGroupingFailed
					? _T("You cannot group items by the \"%s\" column.")
					: _T("You cannot sort items by the \"%s\" column.")
					;
		strMsg.Format(
			LPCTSTR(strMsgFormat),
			LPCTSTR(strColumnName)
			);
	} // else from if( strColumnName.IsEmpty() )
	::AfxMessageBox( LPCTSTR(strMsg), MB_OK|MB_ICONINFORMATION );
}

void CExtReportGridWnd::OnSiwPaintForeground(
	CDC & dc,
	bool bFocusedControl
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	CExtTreeGridWnd::OnSiwPaintForeground(
		dc,
		bFocusedControl
		);
	if( RowCountGet() == 0 )
	{
		CRect rcPaintEmptyDataMessage = OnSwGetClientRect();
		rcPaintEmptyDataMessage.DeflateRect( 5, 5 );
		if(		rcPaintEmptyDataMessage.left < rcPaintEmptyDataMessage.right
			&&	rcPaintEmptyDataMessage.top < rcPaintEmptyDataMessage.bottom
			)
		{ // if reasonable rect for painting text message
			CExtSafeString strEmptyDataMessage;
			OnReportGridEmptyDataMessage( strEmptyDataMessage );
			if( ! strEmptyDataMessage.IsEmpty() )
			{
				int nOldBkMode = dc.SetBkMode( TRANSPARENT );
				COLORREF clrOldTextColor = dc.SetTextColor( OnSiwGetSysColor( COLOR_BTNTEXT ) );
				CFont * pOldFont = dc.SelectObject( &(OnSiwGetDefaultFont()) );
				dc.DrawText(
					LPCTSTR(strEmptyDataMessage),
					strEmptyDataMessage.GetLength(),
					rcPaintEmptyDataMessage,
					DT_CENTER|DT_TOP|DT_WORDBREAK
					);
				dc.SelectObject( pOldFont );
				dc.SetTextColor( clrOldTextColor );
				dc.SetBkMode( nOldBkMode );
			} // if( ! strEmptyDataMessage.IsEmpty() )
		} // if reasonable rect for painting text message
	} // if( RowCountGet() == 0 )
}

INT CExtReportGridWnd::OnReportGridQueryItemExtraSpace(
	CExtReportGridItem * pRGI,
	bool bAfter
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGI );
	if( GetSafeHwnd() == NULL )
		return 0;
DWORD dwReportGridStyle = ReportGridGetStyle();
bool bGroups2003 = ( (dwReportGridStyle&__ERGS_LAYOUT_AND_STYLE_2003) != 0 ) ? true : false;
	if( ItemGetChildCount(*pRGI) > 0 )
	{ // group item
		if( bAfter )
		{
			if( bGroups2003 )
				return 4;
		} // if( bAfter )
		else
		{
			if( bGroups2003 )
				return 12;
		} // else from if( bAfter )
		return 0;
	} // group item
	else
	{ // registered report item
		if( ! bAfter )
			return 0;
		if( ! ReportAutoPreviewModeGet() )
			return 0;
		CExtGridCell * pCellPreviewArea =
			pRGI->PreviewAreaGet();
		if( pCellPreviewArea == NULL )
			return 0;
		CSize _sizePreviewArea =
			pCellPreviewArea->MeasureCell(
				const_cast < CExtReportGridWnd * > ( this )
				);
		if( _sizePreviewArea.cy == 0 )
			return 0;
		_sizePreviewArea.cy += 4;
		return _sizePreviewArea.cy;
	} // registered report item
}

void CExtReportGridWnd::OnGbwEraseArea(
	CDC & dc,
	const RECT & rcArea,
	DWORD dwAreaFlags
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	CExtTreeGridWnd::OnGbwEraseArea(
		dc,
		rcArea,
		dwAreaFlags
		);
	if( dwAreaFlags == (__EGBWA_OUTER_CELLS|__EGBWA_OUTER_TOP) )
		OnReportGridPaintHeaderRowBackground(
			dc,
			rcArea,
			(CObject*)this,
			LPARAM(dwAreaFlags)
			);
}

bool CExtReportGridWnd::OnReportGridPaintHeaderRowBackground(
	CDC & dc,
	const RECT & rcArea,
	CObject * pHelperSrc, // = NULL
	LPARAM lParam // = 0L
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL );
	if( (SiwGetStyleEx()&__EGWS_EX_PM_COLORS) != 0 )
	{
		if( g_PaintManager->ReportGrid_PaintHeaderRowBackground(
				dc,
				rcArea,
				pHelperSrc,
				lParam
				)
			)
			return true;
	} // if( (SiwGetStyleEx()&__EGWS_EX_PM_COLORS) != 0 )
	if(		(ReportGridGetStyle()&__ERGS_LAYOUT_AND_STYLE_2003) != 0
		&&	(! g_bmpHeaderAreaBk.IsEmpty() )
		)
	{
		g_bmpHeaderAreaBk.DrawSkinParts(
			dc,
			rcArea,
			g_rcHeaderAreaPadding,
			CExtBitmap::__EDM_STRETCH
			);
	}
	return false;
}

bool CExtReportGridWnd::OnGridHookCellPaintBackground(
	const CExtGridCell & _cell,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
	if( (dwHelperPaintFlags&__EGCPF_HIGHLIGHTED_BY_PRESSED_COLUMN) != 0 )
		return false;
	if( (dwHelperPaintFlags&__EGCPF_OUTER_DND) != 0 )
		return true;
	if(		nColType == 0
		&&	nRowType == -1
		&&	( ReportGridGetStyle() & __ERGS_LAYOUT_AND_STYLE_2003 ) != 0
		)
		return true;
	return
		CExtTreeGridWnd::OnGridHookCellPaintBackground(
			_cell,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
}

bool CExtReportGridWnd::OnGridHookCellPaintForeground(
	const CExtGridCell & _cell,
	CDC & dc,
	LONG nVisibleColNo,
	LONG nVisibleRowNo,
	LONG nColNo,
	LONG nRowNo,
	INT nColType,
	INT nRowType,
	const RECT & rcCellExtra,
	const RECT & rcCell,
	const RECT & rcVisibleRange,
	DWORD dwAreaFlags,
	DWORD dwHelperPaintFlags
	) const
{
	ASSERT_VALID( this );
	return
		CExtTreeGridWnd::OnGridHookCellPaintForeground(
			_cell,
			dc,
			nVisibleColNo,
			nVisibleRowNo,
			nColNo,
			nRowNo,
			nColType,
			nRowType,
			rcCellExtra,
			rcCell,
			rcVisibleRange,
			dwAreaFlags,
			dwHelperPaintFlags
			);
}

bool CExtReportGridWnd::OnGridCellGetToolTipText(
	const CExtGridCell & _cell,
	const CExtGridHitTestInfo & htInfo,
	CExtSafeString & strToolTipText
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( (&_cell) );
	if(		(! htInfo.IsHoverEmpty() )
		&&	htInfo.IsValidRect()
		&&	htInfo.GetInnerOuterTypeOfColumn() == 0
		&&	htInfo.GetInnerOuterTypeOfRow() == -1
		&&	htInfo.m_nRowNo == 0
		)
	{
		ASSERT_KINDOF( CExtReportGridColumn, (&_cell) );
		CExtReportGridColumn * pRGC =
			STATIC_DOWNCAST(
				CExtReportGridColumn,
				( const_cast < CExtGridCell * > ( &_cell ) )
				);
		if( pRGC->SortingEnabledGet() )
		{
			OnReportGridFormatSortByToolTipText(
				strToolTipText,
				pRGC
				);
			if( ! strToolTipText.IsEmpty() )
				return true;
		}
	}
	return
		CExtTreeGridWnd::OnGridCellGetToolTipText(
			_cell,
			htInfo,
			strToolTipText
			);
}

bool CExtReportGridWnd::OnGbwAnalyzeCellMouseClickEvent(
	UINT nChar, // VK_LBUTTON, VK_RBUTTON or VK_MBUTTON only
	UINT nRepCnt, // 0 - button up, 1 - single click, 2 - double click, 3 - post single click & begin editing
	UINT nFlags, // mouse event flags
	CPoint point // mouse pointer in client coordinates
	)
{
	ASSERT_VALID( this );
	ASSERT( 0 <= nRepCnt && nRepCnt <= 3 );
	if( CExtTreeGridWnd::OnGbwAnalyzeCellMouseClickEvent(
			nChar,
			nRepCnt,
			nFlags,
			point
			)
		)
		return true;
	return false;
}

void CExtReportGridWnd::OnGridTrackOuterCellDND(
	CExtGridCell * pCell,
	const CExtGridHitTestInfo & htInfo,
	UINT nChar, // VK_LBUTTON, VK_RBUTTON or VK_MBUTTON only
	UINT nFlags // mouse event flags
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pCell );
CExtReportGridColumn * pRGC =
		DYNAMIC_DOWNCAST( CExtReportGridColumn, pCell );
	if(		pRGC != NULL
		&&	ReportColumnIsRegistered( pRGC )
		&&	(! pRGC->DragDropEnabledGet() )
		)
		return;
	CExtTreeGridWnd::OnGridTrackOuterCellDND(
		pCell,
		htInfo,
		nChar,
		nFlags
		);
}

bool CExtReportGridWnd::OnSiwWalkItemsH(
	CDC & dc,
	LPVOID pQueryData,
	const RECT & rcRowExtra,
	const RECT & rcRow,
	LONG nVisibleRowNo,
	LONG nRowNo,
	const RECT & rcVisibleRange,
	bool & bVirtualBottomReached,
	DWORD dwAreaFlags,
	bool bFocusedControl
	) const
{
	ASSERT_VALID( this );
	ASSERT( dc.GetSafeHdc() != NULL || pQueryData != NULL );
	m_nHelerLockSiwDrawFocusRect ++;
bool bRetVal =
		CExtTreeGridWnd::OnSiwWalkItemsH(
			dc,
			pQueryData,
			rcRowExtra,
			rcRow,
			nVisibleRowNo,
			nRowNo,
			rcVisibleRange,
			bVirtualBottomReached,
			dwAreaFlags,
			bFocusedControl
			);
	m_nHelerLockSiwDrawFocusRect --;
	if(		(dwAreaFlags&__EGBWA_INNER_CELLS) != 0
		&&	dc.GetSafeHdc() != NULL
		)
	{
		bool bFocusPainting = ( bFocusedControl && FocusGet().y == nRowNo ) ? true : false;
		bool bAutoPreviewPainting = ReportAutoPreviewModeGet();
		HTREEITEM hTreeItem = NULL;
		CExtReportGridItem * pRGI = NULL;
		CExtGridCell * pCellPreviewArea = NULL;
		if( bAutoPreviewPainting )
		{
			hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
			ASSERT( hTreeItem != NULL );
			pRGI =
				const_cast
				< CExtReportGridItem * >
				( ReportItemFromTreeItem( hTreeItem ) );
			ASSERT_VALID( pRGI );
			pCellPreviewArea = pRGI->PreviewAreaGet();
			if( pCellPreviewArea == NULL )
				bAutoPreviewPainting = false;
#ifdef _DEBUG
			else
			{
				ASSERT_VALID( pCellPreviewArea );
			}
#endif // _DEBUG
		} // if( bAutoPreviewPainting )
		if( bFocusPainting || bAutoPreviewPainting )
		{
			INT nSummaryIndent = 0;
			HTREEITEM hRootItem = ItemGetRoot();
			ASSERT( hRootItem != NULL );
			if( hTreeItem == NULL )
			{
				hTreeItem = ItemGetByVisibleRowIndex( nRowNo );
				ASSERT( hTreeItem != NULL );
			}
			HTREEITEM htiAnalyze = hTreeItem;
			LONG nChildCount = ItemGetChildCount( hTreeItem );
			for( ; true; )
			{
				ASSERT( hTreeItem != NULL );
				CExtTreeGridCellNode * pNode =
					CExtTreeGridCellNode::FromHTREEITEM( hTreeItem );
				ASSERT_VALID( pNode );
				hTreeItem = ItemGetParent( hTreeItem );
				if( nChildCount != 0 )
				{
					if( hTreeItem == hRootItem )
						break;
				}
				INT nPxIndent = (INT)pNode->TreeNodeIndentPxGet();
				if( nPxIndent == ULONG(-1L) )
					nPxIndent = GetTreeData().m_nIndentPxDefault;
				nSummaryIndent += nPxIndent;
				if( nChildCount == 0 )
				{
					if( hTreeItem == hRootItem )
						break;
				}
			}
			CRect rcFocus = rcRowExtra;
			bool bScrollableMode = false;
			if(		(! ReportColumnProportionalResizingGet() )
				&&	OnSwHasScrollBar( true )
				)
				bScrollableMode = true;
			if( bScrollableMode )
				rcFocus.right = rcFocus.left + ((CExtScrollWnd*)this)->OnSwGetTotalSize().cx;
			else
			{
				rcFocus.right = rcFocus.left;
				LONG nColCount = ColumnCountGet();
				ASSERT( nColCount >= 0 );
				for( LONG nColNo = 0; nColNo < nColCount; nColNo ++ )
				{
					INT nItemExtent = OnSiwQueryItemExtentH( nColNo );
					ASSERT( nItemExtent >= 0 );
					rcFocus.right += nItemExtent;
				} // for( LONG nColNo = 0; nColNo < nColCount; nColNo ++ )
			}
			rcFocus.left += nSummaryIndent;
			if( ItemGetChildCount(htiAnalyze) == 0 )
			{
				DWORD dwReportGridStyle = ReportGridGetStyle();
				bool bGroups2003 = ( (dwReportGridStyle&__ERGS_LAYOUT_AND_STYLE_2003) != 0 ) ? true : false;
				if(		bGroups2003
					&&	ItemGetParent( htiAnalyze ) != ItemGetRoot()
					)
				{
					CExtTreeGridCellNode * pNode =
						CExtTreeGridCellNode::FromHTREEITEM( htiAnalyze );
					ASSERT_VALID( pNode );
					CExtReportGridDataProvider & _DP = _GetReportData();
					INT nPxIndentRow = INT( pNode->TreeNodeIndentPxGet() );
					if( nPxIndentRow <= 0 )
						nPxIndentRow = INT( _DP.m_nIndentPxDefault );
					rcFocus.left -= nPxIndentRow;
				}
			}
			if( bScrollableMode )
				rcFocus.left -= ((CExtScrollWnd*)this)->OnSwGetScrollPos().x;
			if( bAutoPreviewPainting )
			{
				ASSERT_VALID( pRGI );
				ASSERT_VALID( pCellPreviewArea );
				CRect rcAutoPreview = rcFocus;
				rcAutoPreview.top = rcRow.bottom;
				OnReportGridAutoPreviewAreaPaint(
					dc,
					rcAutoPreview,
					pRGI,
					pCellPreviewArea
					);
			} // if( bAutoPreviewPainting )
			if( bFocusPainting )
				OnSiwDrawFocusRect( dc, &rcFocus );
		} // if( bFocusPainting || bAutoPreviewPainting )
	}
	return bRetVal;
}

void CExtReportGridWnd::OnReportGridAutoPreviewAreaPaint(
	CDC & dc,
	const RECT & rcAutoPreview,
	const CExtReportGridItem * pRGI,
	const CExtGridCell * pCellPreviewArea
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGI );
	ASSERT_VALID( pCellPreviewArea );
	ASSERT( dc.GetSafeHdc() != NULL );
COLORREF clrText =
		OnReportGridAutoPreviewAreaQueryTextColor(
			pRGI,
			pCellPreviewArea
			);
COLORREF clrTextOld = dc.SetTextColor( clrText );
	pCellPreviewArea->OnPaintForeground(
		*this,
		dc,
		0L,
		0L,
		0L,
		0L,
		0,
		0,
		rcAutoPreview,
		rcAutoPreview,
		CRect(0,0,0,0),
		0,
		0
		);
	dc.SetTextColor( clrTextOld );
}

COLORREF CExtReportGridWnd::OnReportGridAutoPreviewAreaQueryTextColor(
	const CExtReportGridItem * pRGI,
	const CExtGridCell * pCellPreviewArea
	) const
{
	ASSERT_VALID( this );
	ASSERT_VALID( pRGI );
	ASSERT_VALID( pCellPreviewArea );
	pCellPreviewArea;
	if( OnSiwQueryFocusedControlState() )
	{
		bool bSelected = false;
		LONG nRowNo = ItemGetVisibleIndexOf( *pRGI );
		if( nRowNo >= 0 )
			bSelected = SelectionGetForCell( 0L, nRowNo );
		if( ! bSelected )
			return RGB(0,0,255);
		COLORREF clr = OnSiwGetSysColor( COLOR_HIGHLIGHTTEXT );
		return clr;
	}
	else
	{
		COLORREF clr = OnSiwGetSysColor( COLOR_BTNTEXT );
		return clr;
	}
}

__EXT_MFC_SAFE_LPCTSTR CExtReportGridWnd::OnReportGridCommandProfileQueryName() const
{
	ASSERT_VALID( this );
	ASSERT( ! m_strLocalCommandProfileName.IsEmpty() );
	return LPCTSTR( m_strLocalCommandProfileName );
}

void CExtReportGridWnd::OnReportGridCommandProfileInit()
{
	ASSERT_VALID( this );
__EXT_MFC_SAFE_LPCTSTR strCmdProfileName = OnReportGridCommandProfileQueryName();
	VERIFY(
		g_CmdManager->ProfileSetup(
			strCmdProfileName,
			m_hWnd
			)
		);
	VERIFY(
		g_CmdManager->UpdateFromMenu(
			strCmdProfileName,
			IDR_EXT_RG_MENU_COLUMN_CONTEXT
			)
		);
CExtBitmap _bmpImages;
	VERIFY(
		g_ResourceManager->LoadBitmap(
			_bmpImages,
			IDB_EXT_RG_CTX_MENU_IMAGES_HI
			)
		);
static const UINT arrCmdIDs[] =
{
	ID_EXT_RG_CTX_SORT_ASCENDING,
	ID_EXT_RG_CTX_SORT_DESCENDING,
	ID_EXT_RG_CTX_GROUP_BY_CONTEXT_FIELD,
	ID_EXT_RG_CTX_SHOW_GROUP_BY_BOX,
	ID_EXT_RG_CTX_FIELD_CHOOSER,
};
static const COLORREF _clrTransparent = RGB(255,0,255);
// 	_bmpImages.Make32();
// 	_bmpImages.AlphaColor( _clrTransparent, RGB(0,0,0), 0 );
static const SIZE _sizeOneImage = { 16, 16 };
INT nCmdIdx, nCmdCount = sizeof(arrCmdIDs)/sizeof(arrCmdIDs[0]);
CRect rcImage( 0, 0, _sizeOneImage.cx, _sizeOneImage.cy );
	for(	nCmdIdx = 0;
			nCmdIdx < nCmdCount;
			nCmdIdx ++, rcImage.OffsetRect( _sizeOneImage.cx, 0 )
			)
	{
		UINT nCmdID = arrCmdIDs[nCmdIdx];
		VERIFY(
			g_CmdManager->CmdSetIcon(
				strCmdProfileName,
				nCmdID,
				_bmpImages,
				_clrTransparent,
				&rcImage
				)
			);
	}
CExtCmdProfile * pCmdProfile =
		g_CmdManager->ProfileGetPtr( strCmdProfileName );
	ASSERT( pCmdProfile != NULL );
POSITION pos = pCmdProfile->m_cmds.GetStartPosition();
	for( ; pos != NULL; )
	{
		UINT nCmdID;
		CExtCmdItem * pCmdItem = NULL;
		pCmdProfile->m_cmds.GetNextAssoc( pos, nCmdID, pCmdItem );
		ASSERT( pCmdItem != NULL );
		nCmdID;
		pCmdItem->StateSetBasic();
	}
}

void CExtReportGridWnd::OnReportGridCommandProfileShutdown()
{
	ASSERT_VALID( this );
	VERIFY(
		g_CmdManager->ProfileDestroy(
			OnReportGridCommandProfileQueryName(),
			true
			)
		);
}

CExtReportGridColumn * CExtReportGridWnd::OnReportGridColumnCtxMenuGetCtxColumn()
{
	ASSERT_VALID( this );
#ifdef _DEBUG
	if( m_pCtxMenuRGC != NULL )
	{
		ASSERT_VALID( m_pCtxMenuRGC );
		ASSERT( m_pCtxMenuRGC->m_pRGW == this );
	}
#endif // _DEBUG
	return m_pCtxMenuRGC;
}

bool CExtReportGridWnd::OnReportGridColumnCtxMenuTrack(
	CWnd * pWndSrc,
	CExtReportGridColumn * pRGC,
	const POINT & ptScreen,
	const RECT & rcItemScreen,
	const RECT & rcItemExtraScreen,
	DWORD dwMenuTrackingFlags // = 0
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pWndSrc );
	ASSERT( pWndSrc->GetSafeHwnd() != NULL );
	ASSERT_VALID( pRGC );
	rcItemScreen;
	rcItemExtraScreen;
DWORD dwReportStyle = ReportGridGetStyle();
	if( ( dwReportStyle & __ERGS_ENABLE_COLUMN_CTX_MENU ) == 0 )
		return false;
bool bRetVal = false;
CExtPopupMenuWnd * pPopup = NULL;
	try
	{
		pPopup = new CExtPopupMenuWnd;
		if( ! pPopup->CreatePopupMenu( m_hWnd ) )
			::AfxThrowUserException();
		if( ! OnReportGridColumnCtxMenuConstruct(
				pPopup,
				pWndSrc,
				pRGC,
				dwMenuTrackingFlags
				)
			)
			::AfxThrowUserException();
		m_pCtxMenuRGC = pRGC;
		if( ! pPopup->TrackPopupMenu(
				dwMenuTrackingFlags,
				ptScreen.x,
				ptScreen.y,
				NULL,
				this,
				NULL,
				NULL,
				true
				)
			)
			::AfxThrowUserException();
		pPopup = NULL; // self deleted
		bRetVal = true;
	} // try
	catch( CException * pException )
	{
		pException->Delete();
	} // catch( CException * pException )
	catch( ... )
	{
	} // catch( ... )
	if( pPopup != NULL )
		delete pPopup;
	return bRetVal;
}

bool CExtReportGridWnd::OnReportGridColumnCtxMenuConstruct(
	CExtPopupMenuWnd * pPopup,
	CWnd * pWndSrc,
	CExtReportGridColumn * pRGC,
	DWORD & dwMenuTrackingFlags
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT_VALID( pPopup );
	ASSERT_VALID( pWndSrc );
	ASSERT( pWndSrc->GetSafeHwnd() != NULL );
	ASSERT_VALID( pRGC );
	pWndSrc;
	pRGC;
	dwMenuTrackingFlags;
	if( ! pPopup->LoadMenu(
			m_hWnd,
			IDR_EXT_RG_MENU_COLUMN_CONTEXT
			)
		)
		return false;
INT nPos;
DWORD dwReportStyle = ReportGridGetStyle();
	nPos = pPopup->ItemFindPosForCmdID( ID_EXT_RG_CTX_COLUMNS );
	if( nPos >= 0 )
	{
		CExtSafeString strText = pPopup->ItemGetText( nPos );
		pPopup->ItemRemove( nPos );
		if( ( dwReportStyle & __ERGS_ENABLE_COLUMN_LIST_IN_CTX_MENU ) != 0 )
		{
			if(	! pPopup->ItemInsert(
					UINT(CExtPopupMenuWnd::TYPE_POPUP),
					nPos,
					LPCTSTR(strText)
					)
				)
				return false;
			CExtPopupMenuWnd * pColumnsPopup = pPopup->ItemGetPopup( nPos );
			ASSERT_VALID( pColumnsPopup );
			UINT nColumnCmdID = m_nColumnListMenuStartID;
			CExtCmdIcon _emptyIcon;
			POSITION pos = ReportColumnGetStartPosition();
			INT nInsertedCount = 0;
			const CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
			LONG nColumnGroupCount = _rgso.ColumnGroupCountGet();
			for( ; pos != NULL; )
			{
				const CExtReportGridColumn * pRGC = ReportColumnGetNext( pos );
				ASSERT_VALID( pRGC );
				if( ! pRGC->CtxMenuVisibleGet() )
				{
					pRGC->m_nColumnListMenuID;
					continue;
				}
				INT nSortIndex = _rgso.ColumnGetIndexOf( pRGC );
				if( nSortIndex >= 0 && nSortIndex < nColumnGroupCount )
					continue;

				//const CExtCmdIcon * pIcon = pRGC->IconGet();
				//const CExtCmdIcon & _icon =
				//	( pIcon != NULL && (! pIcon->IsEmpty() ) )
				//		? (*pIcon)
				//		: _emptyIcon
				//		;
				int nCheck = ReportColumnIsActive( pRGC ) ? 1 : 0;
				pRGC->m_nColumnListMenuID = nColumnCmdID;
				CExtSafeString strInsertText = pRGC->ColumnNameGet();

				INT nInsertPos;
				for( nInsertPos = 0; nInsertPos < nInsertedCount; nInsertPos ++ )
				{
					CExtSafeString strCompareText = pColumnsPopup->ItemGetText( nInsertPos );
					if( strCompareText > strInsertText )
						break;
				}

				if(	! pColumnsPopup->ItemInsertCommand(
						nColumnCmdID,
						nInsertPos, // nInsertedCount
						strInsertText,
						NULL,
						_emptyIcon, //_icon,
						0, // nCheck,
						m_hWnd
						)
					)
					return false;
				CExtPopupMenuWnd::MENUITEMDATA & mi =
					pColumnsPopup->ItemGetInfo( nInsertPos );
				mi.SetForceEnabled();
				mi.SetExtraMark(
					(CExtPopupMenuWnd::MENUITEMDATA::pCbItemClick)
						stat_cbItemClickForColumnVisibility
					);
				mi.LParamSet( (LPARAM)pRGC );
				mi.SetExtraChecked( nCheck ? true : false );
				nColumnCmdID ++;
				nInsertedCount ++;
			} // for( ; pos != NULL; )
		} // if( ( dwReportStyle & __ERGS_ENABLE_COLUMN_LIST_IN_CTX_MENU ) != 0 )
	}
	nPos = pPopup->ItemFindPosForCmdID( ID_EXT_RG_CTX_ARRANGE_BY );
	if( nPos >= 0 )
		pPopup->ItemRemove( nPos );
	if(		pPopup->ItemGetCount() > 0
		&&	pPopup->ItemGetInfo( 0 ).IsSeparator()
		)
		pPopup->ItemRemove( 0 );
	return true;
}

LRESULT CExtReportGridWnd::_OnPopupMenuClosed( WPARAM wParam, LPARAM lParam )
{
	ASSERT_VALID( this );
	wParam;
	lParam;
	if( m_bHelperUpdateAfterMenuClosed )
	{
		m_bHelperUpdateAfterMenuClosed = false;
		OnSwUpdateScrollBars();
		OnSwDoRedraw();
	}
	return 0L;
}

bool CExtReportGridWnd::stat_cbItemClickForColumnVisibility(
	CExtPopupMenuWnd * pPopup,
	LPVOID pItemData
	)
{
	ASSERT_VALID( pPopup );
	pPopup;
	ASSERT( pItemData != NULL );
CExtPopupMenuWnd::MENUITEMDATA & mi =
		*((CExtPopupMenuWnd::MENUITEMDATA *)pItemData);
	ASSERT( mi.IsExtraMark() );
bool bActivate = ! mi.IsExtraChecked();
	mi.SetExtraChecked( bActivate );
CExtReportGridColumn * pRGC = (CExtReportGridColumn*)mi.LParamGet();
	ASSERT_VALID( pRGC );
	ASSERT_KINDOF( CExtReportGridColumn, pRGC );
CExtReportGridWnd * pRGW = pRGC->GetReportGrid();
	pRGW->ReportColumnActivate( pRGC, bActivate, -1L, false );
	pRGW->m_bHelperUpdateAfterMenuClosed = true;
	return true; // not important
}

void CExtReportGridWnd::_OnRgColumnCtxCmdSortAscending()
{
	ASSERT_VALID( this );
	if( m_pCtxMenuRGC == NULL )
		return;
	ASSERT_VALID( m_pCtxMenuRGC );
	ASSERT( m_pCtxMenuRGC->m_pRGW == this );
	if( ! m_pCtxMenuRGC->SortingEnabledGet() )
		return;
CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
LONG nIndex = _rgso.ColumnGetIndexOf( m_pCtxMenuRGC );
	if( nIndex < 0 )
	{
		CExtReportGridSortOrder _rgsoNew = _rgso;
		VERIFY(
			_rgsoNew.ColumnInsert(
				m_pCtxMenuRGC,
				-1,
				true
				)
			);
		VERIFY( ReportSortOrderSet( _rgsoNew, false, true ) );
	}
	else
	{
		bool bAscending = m_pCtxMenuRGC->SortingAscendingGet();
		if( bAscending )
			return;
		VERIFY(
			_rgso.ColumnSetAt(
				m_pCtxMenuRGC,
				nIndex,
				true
				)
			);
		VERIFY( ReportSortOrderUpdate() );
	}
}

void CExtReportGridWnd::_OnRgUpdateColumnCtxCmdSortAscending( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI != NULL );
	if( m_pCtxMenuRGC == NULL )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetRadio( 0 );
		return;
	}
	ASSERT_VALID( m_pCtxMenuRGC );
	ASSERT( m_pCtxMenuRGC->m_pRGW == this );
	if( ! m_pCtxMenuRGC->SortingEnabledGet() )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetRadio( 0 );
		return;
	}
	pCmdUI->Enable();
const CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
LONG nIndex = _rgso.ColumnGetIndexOf( m_pCtxMenuRGC );
	if( nIndex < 0 )
	{
		pCmdUI->SetRadio( 0 );
		return;
	}
bool bAscending = m_pCtxMenuRGC->SortingAscendingGet();
	pCmdUI->SetRadio( bAscending ? 1 : 0 );
}

void CExtReportGridWnd::_OnRgColumnCtxCmdSortDescending()
{
	ASSERT_VALID( this );
	if( m_pCtxMenuRGC == NULL )
		return;
	ASSERT_VALID( m_pCtxMenuRGC );
	ASSERT( m_pCtxMenuRGC->m_pRGW == this );
	if( ! m_pCtxMenuRGC->SortingEnabledGet() )
		return;
CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
LONG nIndex = _rgso.ColumnGetIndexOf( m_pCtxMenuRGC );
	if( nIndex < 0 )
	{
		CExtReportGridSortOrder _rgsoNew = _rgso;
		VERIFY(
			_rgsoNew.ColumnInsert(
				m_pCtxMenuRGC,
				-1,
				false
				)
			);
		VERIFY( ReportSortOrderSet( _rgsoNew, false, true ) );
	}
	else
	{
		bool bAscending = m_pCtxMenuRGC->SortingAscendingGet();
		if( ! bAscending )
			return;
		VERIFY(
			_rgso.ColumnSetAt(
				m_pCtxMenuRGC,
				nIndex,
				false
				)
			);
		VERIFY( ReportSortOrderUpdate() );
	}
}

void CExtReportGridWnd::_OnRgUpdateColumnCtxCmdSortDescending( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI != NULL );
	if( m_pCtxMenuRGC == NULL )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetRadio( 0 );
		return;
	}
	ASSERT_VALID( m_pCtxMenuRGC );
	ASSERT( m_pCtxMenuRGC->m_pRGW == this );
	if( ! m_pCtxMenuRGC->SortingEnabledGet() )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetRadio( 0 );
		return;
	}
	pCmdUI->Enable();
const CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
LONG nIndex = _rgso.ColumnGetIndexOf( m_pCtxMenuRGC );
	if( nIndex < 0 )
	{
		pCmdUI->SetRadio( 0 );
		return;
	}
bool bAscending = m_pCtxMenuRGC->SortingAscendingGet();
	pCmdUI->SetRadio( bAscending ? 0 : 1 );
}

void CExtReportGridWnd::_OnRgColumnCtxCmdGroupByContextField()
{
	ASSERT_VALID( this );
	if( m_pCtxMenuRGC == NULL )
		return;
	ASSERT_VALID( m_pCtxMenuRGC );
	ASSERT( m_pCtxMenuRGC->m_pRGW == this );
	if( ! m_pCtxMenuRGC->GroupingEnabledGet() )
		return;
LONG nMaxColCount = ReportSortOrderMaxGroupColumnCountGet();
bool bAscending = true;
CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
LONG nColumnGroupCount = _rgso.ColumnGroupCountGet();
LONG nIndex = _rgso.ColumnGetIndexOf( m_pCtxMenuRGC );
	if( nIndex >= nColumnGroupCount )
	{
		if( nColumnGroupCount >= nMaxColCount )
		{
			OnReportMaxGroupColumnCountReached();
			return;
		}
		bAscending = m_pCtxMenuRGC->SortingAscendingGet();
		VERIFY( _rgso.ColumnRemove( nIndex, 1 ) );
		nIndex = -1;
	}
	if( nIndex < 0 )
	{
		if( nColumnGroupCount >= nMaxColCount )
		{
			OnReportMaxGroupColumnCountReached();
			return;
		}
		ReportColumnActivate( m_pCtxMenuRGC, false, -1L, false );
		VERIFY(
			_rgso.ColumnInsert(
				m_pCtxMenuRGC,
				nColumnGroupCount,
				bAscending
				)
			);
		nColumnGroupCount ++;
		VERIFY( _rgso.ColumnGroupCountSet( nColumnGroupCount ) );
	}
	else
	{
		nColumnGroupCount --;
		VERIFY( _rgso.ColumnGroupCountSet( nColumnGroupCount ) );
		VERIFY( _rgso.ColumnRemove( nIndex, 1 ) );
		ReportColumnActivate( m_pCtxMenuRGC, true, -1L, false );
	}
	VERIFY( ReportSortOrderUpdate( true ) );
}

void CExtReportGridWnd::_OnRgUpdateColumnCtxCmdGroupByContextField( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI != NULL );
	if( m_pCtxMenuRGC == NULL )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetCheck( 0 );
		return;
	}
	ASSERT_VALID( m_pCtxMenuRGC );
	ASSERT( m_pCtxMenuRGC->m_pRGW == this );
	if( ! m_pCtxMenuRGC->GroupingEnabledGet() )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetCheck( 0 );
		return;
	}
LONG nMaxColCount = ReportSortOrderMaxGroupColumnCountGet();
const CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
LONG nColumnGroupCount = _rgso.ColumnGroupCountGet();
	if( nColumnGroupCount >= nMaxColCount )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetCheck( 0 );
		return;
	}
	if( ! m_pCtxMenuRGC->GroupingEnabledGet() )
	{
		pCmdUI->Enable( FALSE );
		pCmdUI->SetCheck( 0 );
		return;
	}
	pCmdUI->Enable();
LONG nIndex = _rgso.ColumnGetIndexOf( m_pCtxMenuRGC );
	if( nIndex < 0 )
	{
		pCmdUI->SetCheck( 0 );
		return;
	}
	if( nIndex >= nColumnGroupCount )
	{
		pCmdUI->SetCheck( 0 );
		return;
	}
	pCmdUI->SetCheck( 1 );
}

void CExtReportGridWnd::_OnRgColumnCtxCmdShowHideGroupByBox()
{
	ASSERT_VALID( this );
	ReportGroupAreaShow( ! ReportGroupAreaIsVisible() );
}

void CExtReportGridWnd::_OnRgUpdateColumnCtxCmdShowHideGroupByBox( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI != NULL );
	pCmdUI->Enable();
	pCmdUI->SetCheck( ReportGroupAreaIsVisible() );
}

void CExtReportGridWnd::_OnRgColumnCtxCmdRemoveColumn()
{
	ASSERT_VALID( this );
	if( m_pCtxMenuRGC == NULL )
		return;
	ASSERT_VALID( m_pCtxMenuRGC );
CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
LONG nColumnGroupCount = _rgso.ColumnGroupCountGet();
LONG nIndex = _rgso.ColumnGetIndexOf( m_pCtxMenuRGC );
	if( nIndex >= 0 )
	{
		if( nIndex < nColumnGroupCount )
		{
			nColumnGroupCount --;
			VERIFY( _rgso.ColumnGroupCountSet( nColumnGroupCount ) );
			VERIFY( _rgso.ColumnRemove( nIndex, 1 ) );
			ReportColumnActivate( m_pCtxMenuRGC, true, -1L, false );
		}
		else
		{
			VERIFY( _rgso.ColumnRemove( nIndex, 1 ) );
		}
		VERIFY( ReportSortOrderUpdate( true ) );
	}
	VERIFY( ReportColumnActivate( m_pCtxMenuRGC, false ) );
}

void CExtReportGridWnd::_OnRgUpdateColumnCtxCmdRemoveColumn( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI != NULL );
	if( m_pCtxMenuRGC == NULL )
	{
		pCmdUI->Enable( FALSE );
		return;
	}
	ASSERT_VALID( m_pCtxMenuRGC );
	if( ! ReportColumnIsActive( m_pCtxMenuRGC ) )
	{
		CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
		LONG nIndex = _rgso.ColumnGetIndexOf( m_pCtxMenuRGC );
		if( nIndex < 0 )
		{
			pCmdUI->Enable( FALSE );
			return;
		}
	}
	pCmdUI->Enable();
}

void CExtReportGridWnd::_OnRgColumnCtxCmdShowHideFieldChooser()
{
	ASSERT_VALID( this );
	ReportColumnChooserShow( ! ReportColumnChooserIsVisible() );
}

void CExtReportGridWnd::_OnRgUpdateColumnCtxCmdShowHideFieldChooser( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI != NULL );
	pCmdUI->Enable();
	pCmdUI->SetCheck( ReportColumnChooserIsVisible() );
}

void CExtReportGridWnd::_OnRgColumnCtxCmdBestFitContextField()
{
	ASSERT_VALID( this );
	if( m_pCtxMenuRGC == NULL )
		return;
	ASSERT_VALID( m_pCtxMenuRGC );
	if( ! ReportColumnIsActive( m_pCtxMenuRGC ) )
		return;
INT nItemExtentMin = 0, nItemExtentMax = 0;
	if(		( ! m_pCtxMenuRGC->ExtentGet( nItemExtentMin, -1 ) )
		||	( ! m_pCtxMenuRGC->ExtentGet( nItemExtentMax, 1 ) )
		)
		return;
	if( nItemExtentMin == nItemExtentMax )
		return;
LONG nColNo = (LONG)GetReportData().ActiveIndexGet( m_pCtxMenuRGC );
	ASSERT( nColNo >= 0 );
	BestFitColumn( nColNo );
}

void CExtReportGridWnd::_OnRgUpdateColumnCtxCmdBestFitContextField( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI != NULL );
	if( m_pCtxMenuRGC == NULL )
	{
		pCmdUI->Enable( FALSE );
		return;
	}
	ASSERT_VALID( m_pCtxMenuRGC );
	if( ! ReportColumnIsActive( m_pCtxMenuRGC ) )
	{
		pCmdUI->Enable( FALSE );
		return;
	}
INT nItemExtentMin = 0, nItemExtentMax = 0;
	if(		( ! m_pCtxMenuRGC->ExtentGet( nItemExtentMin, -1 ) )
		||	( ! m_pCtxMenuRGC->ExtentGet( nItemExtentMax, 1 ) )
		)
	{
		pCmdUI->Enable( FALSE );
		return;
	}
	pCmdUI->Enable( ( nItemExtentMin != nItemExtentMax ) ? TRUE : FALSE );
}

void CExtReportGridWnd::_OnRgColumnCtxCmdColumnAlignmentLeft()
{
	ASSERT_VALID( this );
	ASSERT_VALID( this );
	if( m_pCtxMenuRGC == NULL )
		return;
	ASSERT_VALID( m_pCtxMenuRGC );
	ReportColumnAdjustTextAlignmentH( m_pCtxMenuRGC, __EGCS_TA_HORZ_LEFT );
}

void CExtReportGridWnd::_OnRgUpdateColumnCtxCmdColumnAlignmentLeft( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI != NULL );
	if( m_pCtxMenuRGC == NULL )
	{
		pCmdUI->SetRadio( 0 );
		pCmdUI->Enable( FALSE );
		return;
	}
	ASSERT_VALID( m_pCtxMenuRGC );
	if( ! m_pCtxMenuRGC->CtxMenuAlignmentCommandsEnabledGet() )
	{
		pCmdUI->SetRadio( 0 );
		pCmdUI->Enable( FALSE );
		return;
	}
DWORD dwAlignment = m_pCtxMenuRGC->GetStyle()&__EGCS_TA_HORZ_MASK;
	pCmdUI->Enable();
	pCmdUI->SetRadio( ( dwAlignment == __EGCS_TA_HORZ_LEFT ) ? true : false );
}

void CExtReportGridWnd::_OnRgColumnCtxCmdColumnAlignmentCenter()
{
	ASSERT_VALID( this );
	ASSERT_VALID( this );
	if( m_pCtxMenuRGC == NULL )
		return;
	ASSERT_VALID( m_pCtxMenuRGC );
	ReportColumnAdjustTextAlignmentH( m_pCtxMenuRGC, __EGCS_TA_HORZ_CENTER );
}

void CExtReportGridWnd::_OnRgUpdateColumnCtxCmdColumnAlignmentCenter( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI != NULL );
	if( m_pCtxMenuRGC == NULL )
	{
		pCmdUI->SetRadio( 0 );
		pCmdUI->Enable( FALSE );
		return;
	}
	ASSERT_VALID( m_pCtxMenuRGC );
	if( ! m_pCtxMenuRGC->CtxMenuAlignmentCommandsEnabledGet() )
	{
		pCmdUI->SetRadio( 0 );
		pCmdUI->Enable( FALSE );
		return;
	}
DWORD dwAlignment = m_pCtxMenuRGC->GetStyle()&__EGCS_TA_HORZ_MASK;
	pCmdUI->Enable();
	pCmdUI->SetRadio( ( dwAlignment == __EGCS_TA_HORZ_CENTER ) ? true : false );
}

void CExtReportGridWnd::_OnRgColumnCtxCmdColumnAlignmentRight()
{
	ASSERT_VALID( this );
	ASSERT_VALID( this );
	if( m_pCtxMenuRGC == NULL )
		return;
	ASSERT_VALID( m_pCtxMenuRGC );
	ReportColumnAdjustTextAlignmentH( m_pCtxMenuRGC, __EGCS_TA_HORZ_RIGHT );
}

void CExtReportGridWnd::_OnRgUpdateColumnCtxCmdColumnAlignmentRight( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI != NULL );
	if( m_pCtxMenuRGC == NULL )
	{
		pCmdUI->SetRadio( 0 );
		pCmdUI->Enable( FALSE );
		return;
	}
	ASSERT_VALID( m_pCtxMenuRGC );
	if( ! m_pCtxMenuRGC->CtxMenuAlignmentCommandsEnabledGet() )
	{
		pCmdUI->SetRadio( 0 );
		pCmdUI->Enable( FALSE );
		return;
	}
DWORD dwAlignment = m_pCtxMenuRGC->GetStyle()&__EGCS_TA_HORZ_MASK;
	pCmdUI->Enable();
	pCmdUI->SetRadio( ( dwAlignment == __EGCS_TA_HORZ_RIGHT ) ? true : false );
}

void CExtReportGridWnd::_OnRgColumnCtxCmdColumnAlignmentByType()
{
	ASSERT_VALID( this );
	ASSERT_VALID( this );
	if( m_pCtxMenuRGC == NULL )
		return;
	ASSERT_VALID( m_pCtxMenuRGC );
	ReportColumnAdjustTextAlignmentH( m_pCtxMenuRGC, __EGCS_TA_HORZ_BY_TYPE );
}

void CExtReportGridWnd::_OnRgUpdateColumnCtxCmdColumnAlignmentByType( CCmdUI * pCmdUI )
{
	ASSERT_VALID( this );
	ASSERT( pCmdUI != NULL );
	if( m_pCtxMenuRGC == NULL )
	{
		pCmdUI->SetRadio( 0 );
		pCmdUI->Enable( FALSE );
		return;
	}
	ASSERT_VALID( m_pCtxMenuRGC );
	if( ! m_pCtxMenuRGC->CtxMenuAlignmentCommandsEnabledGet() )
	{
		pCmdUI->SetRadio( 0 );
		pCmdUI->Enable( FALSE );
		return;
	}
DWORD dwAlignment = m_pCtxMenuRGC->GetStyle()&__EGCS_TA_HORZ_MASK;
	pCmdUI->Enable();
	pCmdUI->SetRadio( ( dwAlignment == __EGCS_TA_HORZ_BY_TYPE ) ? true : false );
}

bool CExtReportGridWnd::OnGridQueryCenteredDndAlignment() const
{
	ASSERT_VALID( this );
	return true;
}

bool CExtReportGridWnd::ReportGridStateSerialize(
	CArchive & ar,
	bool bSerializeColumnAlignment, // = true
	bool bSerializeSortOrder, // = true
	bool bEnableThrowExceptions, // = false
	bool * p_bFormatIncompatibilityDuringLoad // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	if( p_bFormatIncompatibilityDuringLoad != NULL )
		(*p_bFormatIncompatibilityDuringLoad) = false;
bool bFormatIncompatibilityDuringLoad = false;
	try
	{
		DWORD dwGeneralFlags = 0;
		if( ar.IsStoring() )
		{
			if( bSerializeSortOrder )
				dwGeneralFlags |= 0x00000001;
			if( bSerializeColumnAlignment )
				dwGeneralFlags |= 0x00000002;
			if( ReportAutoPreviewModeGet() )
				dwGeneralFlags |= 0x00000004;
			if( ReportGroupAreaIsVisible() )
				dwGeneralFlags |= 0x00000008;
			ar << dwGeneralFlags;
			const CExtReportGridSortOrder & _rgso = ReportSortOrderGet();
			DWORD dwColumnCount = ReportColumnGetCount();
			ar << dwColumnCount;
			POSITION pos = ReportColumnGetStartPosition();
			for( ; pos != NULL; )
			{
				CExtReportGridColumn * pRGC = ReportColumnGetNext( pos );
				ASSERT_VALID( pRGC );
				CExtSafeString strCategoryName = pRGC->CategoryNameGet();
				CExtSafeString strColumnName = pRGC->ColumnNameGet();
				DWORD dwColumnFlags = 0,
					dwAligmentFlags =
						pRGC->GetStyle() &
							(__EGCS_TA_HORZ_MASK
							|__EGCS_TA_VERT_MASK
							|__EGCS_ICA_HORZ_MASK
							|__EGCS_ICA_VERT_MASK
							);
				if( pRGC->SortingEnabledGet() )
					dwColumnFlags |= 0x00000001;
				if( pRGC->GroupingEnabledGet() )
					dwColumnFlags |= 0x00000002;
				if( pRGC->DragDropEnabledGet() )
					dwColumnFlags |= 0x00000004;
				if( pRGC->CtxMenuVisibleGet() )
					dwColumnFlags |= 0x00000008;
				if( pRGC->CtxMenuAlignmentCommandsEnabledGet() )
					dwColumnFlags |= 0x00000010;
				if(		bSerializeSortOrder
					&&	_rgso.ColumnGetIndexOf( pRGC ) >= 0
					)
					dwColumnFlags |= 0x00000020;
				ar << strCategoryName;
				ar << strColumnName;
				ar << dwColumnFlags;
				if( bSerializeColumnAlignment )
					ar << dwAligmentFlags;
				INT nExtentCurrent = 10, nExtentMin = 10, nExtentMax = 32767;
				pRGC->ExtentGet( nExtentCurrent, 0 );
				pRGC->ExtentGet( nExtentMin, -1 );
				pRGC->ExtentGet( nExtentMax, 1 );
				ar << DWORD(nExtentCurrent);
				ar << DWORD(nExtentMin);
				ar << DWORD(nExtentMax);
				double lfExtentPercent = 0.5;
				pRGC->ExtentPercentGet( lfExtentPercent );
				ar << lfExtentPercent;
			} // for( ; pos != NULL; )
			DWORD dwActiveCount = DWORD( ColumnCountGet() );
			ar << dwActiveCount;
			for( DWORD dwActiveIndex = 0; dwActiveIndex < dwActiveCount; dwActiveIndex++ )
			{
				const CExtReportGridColumn * pRGC =
					STATIC_DOWNCAST( CExtReportGridColumn, GridCellGetOuterAtTop( LONG(dwActiveIndex), 0L ) );
				ASSERT_VALID( pRGC );
				CExtSafeString strCategoryName = pRGC->CategoryNameGet();
				CExtSafeString strColumnName = pRGC->ColumnNameGet();
				ar << strCategoryName;
				ar << strColumnName;
			} // for( DWORD dwActiveIndex = 0; dwActiveIndex < dwActiveCount; dwActiveIndex++ )
			if( bSerializeSortOrder )
			{
				DWORD dwColumnCount = (DWORD)_rgso.ColumnCountGet();
				DWORD dwGroupCount = (DWORD)_rgso.ColumnGroupCountGet();
				ASSERT( dwGroupCount <= dwColumnCount );
				ar << dwColumnCount;
				ar << dwGroupCount;
				for( DWORD dwColumnIndex = 0; dwColumnIndex < dwColumnCount; dwColumnIndex ++ )
				{
					bool bAscending = false;
					const CExtReportGridColumn * pRGC =
						_rgso.ColumnGetAt( LONG(dwColumnIndex), &bAscending );
					ASSERT_VALID( pRGC );
					CExtSafeString strCategoryName = pRGC->CategoryNameGet();
					CExtSafeString strColumnName = pRGC->ColumnNameGet();
					BYTE byteSortFlags = bAscending ? BYTE(1) : BYTE(0);
					ar << strCategoryName;
					ar << strColumnName;
					ar << byteSortFlags;
				} // for( DWORD dwColumnIndex = 0; dwColumnIndex < dwColumnCount; dwColumnIndex ++ )
			} // if( bSerializeSortOrder )
		} // if( ar.IsStoring() )
		else
		{
			ar >> dwGeneralFlags;
			CExtReportGridSortOrder _rgso;
			DWORD dwColumnIndex, dwColumnCount;
			ar >> dwColumnCount;
			CExtSafeStringArray arrCategoryNames, arrColumnNames;
			CArray < DWORD, DWORD > arrColumnFlags, arrColumnAlignmentStyles;
			CArray < INT, INT > arrColumnExtentCurrent, arrColumnExtentMin, arrColumnExtentMax;
			CArray < double, double > arrColumnExtentPercent;
			arrCategoryNames.SetSize( INT(dwColumnCount) );
			arrColumnNames.SetSize( INT(dwColumnCount) );
			arrColumnFlags.SetSize( INT(dwColumnCount) );
			if( bSerializeColumnAlignment )
				arrColumnAlignmentStyles.SetSize( INT(dwColumnCount) );
			arrColumnExtentCurrent.SetSize( INT(dwColumnCount) );
			arrColumnExtentMin.SetSize( INT(dwColumnCount) );
			arrColumnExtentMax.SetSize( INT(dwColumnCount) );
			arrColumnExtentPercent.SetSize( INT(dwColumnCount) );
			for( dwColumnIndex = 0; dwColumnIndex < dwColumnCount; dwColumnIndex++ )
			{
				CExtSafeString strCategoryName, strColumnName;
				DWORD dwColumnFlags = 0, dwAligmentFlags = 0;
				ar >> strCategoryName;
				ar >> strColumnName;
				ar >> dwColumnFlags;
				if( bSerializeColumnAlignment || (dwGeneralFlags&0x00000002) != 0 )
					ar >> dwAligmentFlags;
				arrCategoryNames.SetAt( INT(dwColumnIndex), strCategoryName );
				arrColumnNames.SetAt( INT(dwColumnIndex), strColumnName );
				arrColumnFlags.SetAt( INT(dwColumnIndex), dwColumnFlags );
				if( bSerializeColumnAlignment )
					arrColumnAlignmentStyles.SetAt( INT(dwColumnIndex), dwAligmentFlags );
				DWORD dwExtentCurrent = 10, dwExtentMin = 10, dwExtentMax = 32767;
				ar >> dwExtentCurrent;
				ar >> dwExtentMin;
				ar >> dwExtentMax;
				arrColumnExtentCurrent.SetAt( INT(dwColumnIndex), dwExtentCurrent );
				arrColumnExtentMin.SetAt( INT(dwColumnIndex), dwExtentMin );
				arrColumnExtentMax.SetAt( INT(dwColumnIndex), dwExtentMax );
				double lfExtentPercent = 0.5;
				ar >> lfExtentPercent;
				arrColumnExtentPercent.SetAt( INT(dwColumnIndex), lfExtentPercent );
				// check format compatibility for this column
				CExtReportGridColumn * pRGC = ReportColumnGet( LPCTSTR(strColumnName), LPCTSTR(strCategoryName) );
				if( pRGC == NULL )
				{
					if(		bSerializeSortOrder
						&&	(dwColumnFlags&0x00000020) != 0
						)
						bFormatIncompatibilityDuringLoad = true;
					continue;
				}
				ASSERT_VALID( pRGC );
				DWORD dwCheckColumnFlags = 0;
				if( pRGC->SortingEnabledGet() )
					dwCheckColumnFlags |= 0x00000001;
				if( pRGC->GroupingEnabledGet() )
					dwCheckColumnFlags |= 0x00000002;
				//if( pRGC->DragDropEnabledGet() )
				//	dwCheckColumnFlags |= 0x00000004;
				//if( pRGC->CtxMenuVisibleGet() )
				//	dwCheckColumnFlags |= 0x00000008;
				//if( pRGC->CtxMenuAlignmentCommandsEnabledGet() )
				//	dwCheckColumnFlags |= 0x00000010;
				if( (dwCheckColumnFlags&0x03) != (dwColumnFlags&0x03) )
				{
					bFormatIncompatibilityDuringLoad = true;
					continue;
				} // if( (dwCheckColumnFlags&0x03) != dwColumnFlags&0x03 )
			} // for( dwColumnIndex = 0; dwColumnIndex < dwColumnCount; dwColumnIndex++ )
			DWORD dwActiveIndex, dwActiveCount = 0;
			ar >> dwActiveCount;
			CExtSafeStringArray arrCategoryNames_AO, arrColumnNames_AO;
			arrCategoryNames_AO.SetSize( INT(dwActiveCount) );
			arrColumnNames_AO.SetSize( INT(dwActiveCount) );
			for( dwActiveIndex = 0; dwActiveIndex < dwActiveCount; dwActiveIndex++ )
			{
				CExtSafeString strCategoryName, strColumnName;
				ar >> strCategoryName;
				ar >> strColumnName;
				arrCategoryNames_AO.SetAt( INT(dwActiveIndex), strCategoryName );
				arrColumnNames_AO.SetAt( INT(dwActiveIndex), strColumnName );
			} // for( dwActiveIndex = 0; dwActiveIndex < dwActiveCount; dwActiveIndex++ )
			if( bSerializeSortOrder || (dwGeneralFlags&0x00000001) != 0 )
			{
				DWORD dwColumnCount = 0, dwGroupCount = 0;
				ar >> dwColumnCount;
				ar >> dwGroupCount;
				ASSERT( dwGroupCount <= dwColumnCount );
				for( DWORD dwColumnIndex = 0; dwColumnIndex < dwColumnCount; dwColumnIndex ++ )
				{
					bool bAscending = false;
					CExtSafeString strCategoryName, strColumnName;
					BYTE byteSortFlags = 0;
					ar >> strCategoryName;
					ar >> strColumnName;
					ar >> byteSortFlags;
					bAscending = ( ( byteSortFlags & BYTE(1) ) != 0 ) ? true : false;
					if( bFormatIncompatibilityDuringLoad )
						continue;
					CExtReportGridColumn * pRGC = ReportColumnGet( LPCTSTR(strColumnName), LPCTSTR(strCategoryName) );
					ASSERT_VALID( pRGC );
					VERIFY( _rgso.ColumnInsert( pRGC, -1, bAscending ) );
				} // for( DWORD dwColumnIndex = 0; dwColumnIndex < dwColumnCount; dwColumnIndex ++ )
				ASSERT( DWORD(_rgso.ColumnCountGet()) == dwColumnCount );
				VERIFY( _rgso.ColumnGroupCountSet( LONG(dwGroupCount) ) );
			} // if( bSerializeSortOrder || (dwGeneralFlags&0x00000001) != 0 )
			if( p_bFormatIncompatibilityDuringLoad != NULL )
				(*p_bFormatIncompatibilityDuringLoad) = bFormatIncompatibilityDuringLoad;
			if( bFormatIncompatibilityDuringLoad )
			{
				if( bEnableThrowExceptions )
#if _MFC_VER >= 0x0800
					::AfxThrowArchiveException( CArchiveException::genericException, NULL );
#else
					::AfxThrowArchiveException( CArchiveException::generic, NULL );
#endif
				return false;
			}
			// compatible state is loaded and can be applied successfully
			POSITION pos = ReportColumnGetStartPosition();
			for( ; pos != NULL; )
			{
				CExtReportGridColumn * pRGC = ReportColumnGetNext( pos );
				ASSERT_VALID( pRGC );
				if( ! pRGC->ColumnIsActive() )
					continue;
				ReportColumnActivate( pRGC, false, -1L, false );
			} // for( ; pos != NULL; )
			for( dwColumnIndex = 0; dwColumnIndex < dwColumnCount; dwColumnIndex++ )
			{
				LPCTSTR strCategoryName = arrCategoryNames.GetAt( INT(dwColumnIndex) );
				LPCTSTR strColumnName = arrColumnNames.GetAt( INT(dwColumnIndex) );
				CExtReportGridColumn * pRGC = ReportColumnGet( LPCTSTR(strColumnName), LPCTSTR(strCategoryName) );
				if( pRGC == NULL )
					continue;
				ASSERT_VALID( pRGC );
				//DWORD dwColumnFlags = arrColumnFlags.GetAt( INT(dwColumnIndex));
				if( bSerializeColumnAlignment )
				{
					DWORD dwAligmentFlags = arrColumnAlignmentStyles.GetAt( INT(dwColumnIndex) );
					ReportColumnAdjustIconAlignmentH(
						pRGC,
						dwAligmentFlags&__EGCS_ICA_HORZ_MASK,
						false
						);
					ReportColumnAdjustIconAlignmentV(
						pRGC,
						dwAligmentFlags&__EGCS_ICA_VERT_MASK,
						false
						);
					ReportColumnAdjustTextAlignmentH(
						pRGC,
						dwAligmentFlags&__EGCS_TA_HORZ_MASK,
						false
						);
					ReportColumnAdjustTextAlignmentV(
						pRGC,
						dwAligmentFlags&__EGCS_TA_VERT_MASK,
						false
						);
				}
				INT nExtentCurrent = arrColumnExtentCurrent.GetAt( INT(dwColumnIndex) );
				pRGC->ExtentSet( nExtentCurrent, 0 );
				INT nExtentMin = arrColumnExtentMin.GetAt( INT(dwColumnIndex) );
				pRGC->ExtentSet( nExtentMin, -1 );
				INT nExtentMax = arrColumnExtentMax.GetAt( INT(dwColumnIndex) );
				pRGC->ExtentSet( nExtentMax, 1 );
				double lfExtentPercent = arrColumnExtentPercent.GetAt( INT(dwColumnIndex) );
				pRGC->ExtentPercentSet( lfExtentPercent );
			} // for( dwColumnIndex = 0; dwColumnIndex < dwColumnCount; dwColumnIndex++ )
			for( dwActiveIndex = 0; dwActiveIndex < dwActiveCount; dwActiveIndex++ )
			{
				LPCTSTR strCategoryName = arrCategoryNames_AO.GetAt( INT(dwActiveIndex) );
				LPCTSTR strColumnName = arrColumnNames_AO.GetAt( INT(dwActiveIndex) );
				CExtReportGridColumn * pRGC = ReportColumnGet( LPCTSTR(strColumnName), LPCTSTR(strCategoryName) );
				if( pRGC == NULL )
					continue;
				ASSERT_VALID( pRGC );
				VERIFY( ReportColumnActivate( pRGC, true, -1L, false ) );
			} // for( dwActiveIndex = 0; dwActiveIndex < dwActiveCount; dwActiveIndex++ )
			if( bSerializeSortOrder )
			{
				VERIFY( ReportSortOrderSet( _rgso, false, true ) );
			}
			else
			{
				VERIFY( ReportSortOrderUpdate( true ) );
			}
			ReportAutoPreviewModeSet( ( (dwGeneralFlags&0x00000004) != 0 ) ? true : false, true );
			ReportGroupAreaShow( ( (dwGeneralFlags&0x00000008) != 0 ) ? true : false );
		} // else from if( ar.IsStoring() )
		return true;
	} // try
	catch( CException * pException )
	{
		if( bEnableThrowExceptions )
			throw;
		pException->Delete();
	} // catch( CException * pException )
	catch( ... )
	{
		if( bEnableThrowExceptions )
			throw;
	} // catch( ... )
	return false;
}

static CExtSafeString productsection2regkeypath(
	__EXT_MFC_SAFE_LPCTSTR sProfileName,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	__EXT_MFC_SAFE_LPCTSTR sReportGridID
	)
{
CExtSafeString strRG;
	strRG.Format(
		_T("%s\\%s"),
		__PROF_UIS_REG_REPORT_GRID,
		sReportGridID
		);
CExtSafeString str =
		CExtCmdManager::GetSubSystemRegKeyPath(
			strRG,
			sProfileName,
			sSectionNameCompany,
			sSectionNameProduct
			);
	return str;
}

bool CExtReportGridWnd::ReportGridStateLoad(
	__EXT_MFC_SAFE_LPCTSTR sReportGridID,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProfile, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%\%sSectionNameProfile%
	HKEY hKeyRoot, // HKEY_CURRENT_USER
	bool bSerializeColumnAlignment, // = true
	bool bSerializeSortOrder, // = true
	bool bEnableThrowExceptions, // = false
	bool * p_bFormatIncompatibilityDuringLoad // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( sReportGridID != NULL );
	ASSERT( sSectionNameCompany != NULL );
	ASSERT( sSectionNameProduct != NULL );
	ASSERT( sSectionNameProfile != NULL );
__EXT_MFC_SAFE_LPCTSTR strProfileName = g_CmdManager->ProfileNameFromWnd( GetSafeHwnd() );
	if(		strProfileName == NULL
		||	strProfileName[0] == _T('\0')
		)
	{
		//ASSERT( FALSE );
		TRACE0( "Prof-UIS: CExtReportGridWnd::ReportGridStateLoad() failed to load report grid state\n" );
		if( bEnableThrowExceptions )
			::AfxThrowUserException();
		return false;
	}
	try
	{
		CExtSafeString sRegKeyPath=
			productsection2regkeypath(
				sSectionNameProfile,
				sSectionNameCompany,
				sSectionNameProduct,
				sReportGridID
				);
		// prepare memory file and archive,
		// get information from registry
		CMemFile _file;
		if( ! CExtCmdManager::FileObjFromRegistry(
				_file,
				sRegKeyPath,
				hKeyRoot,
				bEnableThrowExceptions
				)
			)
		{
			ASSERT( ! bEnableThrowExceptions );
			TRACE0( "Prof-UIS: CExtReportGridWnd::ReportGridStateLoad() failed to load report grid state\n" );
			return false;
		}
		CArchive ar(
			&_file,
			CArchive::load
			);
		// do serialization
		if( ! ReportGridStateSerialize(
				ar,
				bSerializeColumnAlignment,
				bSerializeSortOrder,
				bEnableThrowExceptions,
				p_bFormatIncompatibilityDuringLoad
				)
			)
		{
			//ASSERT( FALSE );
			TRACE0( "Prof-UIS: CExtReportGridWnd::ReportGridStateLoad() failed to load report grid state\n" );
			return false;
		}
		return true;
	} // try
	catch( CException * pException )
	{
		if( bEnableThrowExceptions )
			throw;
		pException->Delete();
	} // catch( CException * pException )
	catch( ... )
	{
		if( bEnableThrowExceptions )
			throw;
	} // catch( ... )
	//ASSERT( FALSE );
	TRACE0( "Prof-UIS: CExtReportGridWnd::ReportGridStateLoad() failed to load report grid state\n" );
	return false;
}

bool CExtReportGridWnd::ReportGridStateSave(
	__EXT_MFC_SAFE_LPCTSTR sReportGridID,
	__EXT_MFC_SAFE_LPCTSTR sSectionNameCompany, // under HKEY_CURRENT_USER\Software
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProduct, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%
	__EXT_MFC_SAFE_LPCTSTR sSectionNameProfile, // under HKEY_CURRENT_USER\Software\%sSectionNameCompany%\%sSectionNameProfile%
	HKEY hKeyRoot, // HKEY_CURRENT_USER
	bool bSerializeColumnAlignment, // = true
	bool bSerializeSortOrder, // = true
	bool bEnableThrowExceptions, // = false
	bool * p_bFormatIncompatibilityDuringLoad // = NULL
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() != NULL );
	ASSERT( sReportGridID != NULL );
	ASSERT( sSectionNameCompany != NULL );
	ASSERT( sSectionNameProduct != NULL );
	ASSERT( sSectionNameProfile != NULL );
__EXT_MFC_SAFE_LPCTSTR strProfileName = g_CmdManager->ProfileNameFromWnd( GetSafeHwnd() );
	if(		strProfileName == NULL
		||	strProfileName[0] == _T('\0')
		)
	{
		//ASSERT( FALSE );
		TRACE0( "Prof-UIS: CExtReportGridWnd::ReportGridStateSave() failed to save report grid state\n" );
		if( bEnableThrowExceptions )
			::AfxThrowUserException();
		return false;
	}
CExtSafeString sRegKeyPath =
		productsection2regkeypath(
			sSectionNameProfile,
			sSectionNameCompany,
			sSectionNameProduct,
			sReportGridID
			);
	try
	{
		// prepare memory file and archive
		CMemFile _file;
		CArchive ar(
			&_file,
			CArchive::store
			);
		// do serialization
		if( ! ReportGridStateSerialize(
				ar,
				bSerializeColumnAlignment,
				bSerializeSortOrder,
				bEnableThrowExceptions,
				p_bFormatIncompatibilityDuringLoad
				)
			)
		{
			//ASSERT( FALSE );
			TRACE0( "Prof-UIS: CExtReportGridWnd::ReportGridStateLoad() failed to load report grid state\n" );
			return false;
		}
		// OK, serialization passed
		ar.Flush();
		ar.Close();
		// put information to registry
		if(	! CExtCmdManager::FileObjToRegistry(
				_file,
				sRegKeyPath,
				hKeyRoot,
				bEnableThrowExceptions
				)
			)
		{
			ASSERT( ! bEnableThrowExceptions );
			//ASSERT( FALSE );
			TRACE0( "Prof-UIS: CExtReportGridWnd::ReportGridStateSave() failed to save report grid state\n" );
			return false;
		}
		return true;
	} // try
	catch( CException * pException )
	{
		if( bEnableThrowExceptions )
			throw;
		pException->Delete();
	} // catch( CException * pException )
	catch( ... )
	{
		if( bEnableThrowExceptions )
			throw;
	} // catch( ... )
	//ASSERT( FALSE );
	TRACE0( "Prof-UIS: CExtReportGridWnd::ReportGridStateSave() failed to save report grid state\n" );
	return false;
}

#endif // (!defined __EXT_MFC_NO_REPORTGRIDWND)


