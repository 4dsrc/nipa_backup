// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#if (!defined __EXT_NC_FRAME_H)
#define __EXT_NC_FRAME_H

#if (!defined __EXT_MFC_DEF_H)
	#include <ExtMfcDef.h>
#endif // __EXT_MFC_DEF_H

#if (!defined __EXT_RESIZABLE_DIALOG_H)
	#include <ExtResizableDialog.h>
#endif

#if (!defined __EXT_POPUP_MENU_WND_H)
	#include <ExtPopupMenuWnd.h>
#endif

#if (!defined __EXT_HOOK_H)
	#include "../Src/ExtHook.h"
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if (!defined __EXT_MFC_NO_NC_FRAME )

class CExtPmBridgeNC;
class CExtNcFrameImpl;

class __PROF_UIS_API CExtPmBridgeNC
	: public CExtPmBridge
{
	CExtNcFrameImpl * m_pNcFrameImpl;
public:
	DECLARE_CExtPmBridge_MEMBERS( CExtPmBridgeNC );
	CExtPmBridgeNC(
		CExtNcFrameImpl * pNcFrameImpl = NULL
		);
	virtual ~CExtPmBridgeNC();
	const CExtNcFrameImpl * NcFrameImpl_Get() const;
	CExtNcFrameImpl * NcFrameImpl_Get();
	void NcFrameImpl_Set(
		CExtNcFrameImpl * pNcFrameImpl
		);
	virtual void PmBridge_OnPaintManagerChanged(
		CExtPaintManager * pGlobalPM
		);
	virtual void PmBridge_OnDisplayChange(
		CExtPaintManager * pGlobalPM,
		CWnd * pWndNotifySrc,
		INT nDepthBPP,
		CPoint ptSizes
		);
	virtual void PmBridge_OnThemeChanged(
		CExtPaintManager * pGlobalPM,
		CWnd * pWndNotifySrc,
		WPARAM wParam,
		LPARAM lParam
		);
	virtual void _AdjustThemeSettings();
}; // class CExtPmBridgeNC

class __PROF_UIS_API CExtNcFrameImpl
{
protected:
	mutable CMapPtrToPtr m_mapNcFrameImpl_HtRects;
	mutable bool m_bNcFrameImpl_HelperInsideNcHitTest:1;
	CExtPmBridgeNC m_BridgeNC;
	bool m_bNcFrameImpl_IsActive:1, m_bNcFrameImpl_RgnSet:1,
		m_bNcFrameImpl_RestoreBorder:1, m_bNcFrameImpl_DelatayedFrameRecalc:1,
		m_bNcFrameImpl_QuickWindowPlacement:1;
	DWORD m_dwNcFrameImpl_StyleInitial, m_dwNcFrameImpl_StyleExInitial;
	mutable INT m_nNcFrameImpl_Lock;
	LPARAM m_nNcFrameImpl_LastCheckCursorHT;
	CPoint m_ptNcFrameImpl_LastCheckCursor;
	mutable CExtCmdIcon m_iconNcFrameImpl_QueryCache;
	void NcFrameImpl_MapHtRects_Clean();
	void NcFrameImpl_MapHtRects_SetAt( LONG nHT, const RECT & rc ) const;
	bool NcFrameImpl_MapHtRects_GetAt( LONG nHT, RECT & rc ) const;
public:
	static const UINT g_nMsgFindExtNcFrameImpl;

	struct __PROF_UIS_API IExtNcFrameImplBridge
	{
		virtual HWND NcFrameImplBridge_GetSafeHwnd() const = 0;
		virtual bool NcFrameImplBridge_OnQueryCaptionMergeMode() const = 0;
		virtual void NcFrameImplBridge_OnOverPaint(
			CDC & dc,
			CRect rcWnd,
			CRect rcClient
			) const = 0;
		virtual void NcFrameImplBridge_OnDrawCaptionText(
			CDC & dc,
			__EXT_MFC_SAFE_LPCTSTR strCaption,
			CRect rcDrawText
			) const = 0;
		virtual HRGN NcFrameImplBridge_GetNcExcludeHRGN() const = 0;
		virtual HRGN NcFrameImplBridge_GetNcResizingHRGN() const = 0;
		virtual bool NcFrameImplBridge_GetNcScRect( UINT nSC, CRect & rc ) const = 0;
		virtual bool NcFrameImpl_GetNcHtRect(
			CRect & rc,
			UINT nHT,
			bool bScreenMapping = true,
			bool bLayoutBordersH = false,
			bool bLayoutBordersV = false,
			LPMINMAXINFO pMinMaxInfo = NULL,
			LPCRECT pRectWnd = NULL
			) const = 0;
		virtual bool NcFrameImplBridge_OnQueryDrawIcon(
			CRect & rcAdjustLocation
			) const = 0;
		virtual bool NcFrameImplBridge_OnQueryDrawCaptionText(
			CRect & rcAdjustLocation,
			UINT & nAdjustDrawTextFlags
			) const = 0;
		virtual INT NcFrameImplBridge_GetDwmEmbeddedCaptionHeight() const = 0;
	}; // struct IExtNcFrameImplBridge
protected:
	IExtNcFrameImplBridge * m_pNcFrameImplBridge;
public:

	class __PROF_UIS_API NcLock
	{
		const CExtNcFrameImpl & m_NcFrameImpl;
		bool m_bLocked:1;
	public:
		NcLock( const CExtNcFrameImpl & _NcFrameImpl );
		~NcLock();
		void UnlockNow();
	}; // class NcLock
	bool m_bNcFrameImpl_IsEnabled:1, m_bNcFrameImpl_IsDwmBased:1,
		m_bNcFrameImpl_Resizing:1;
	mutable CRect
		m_rcNcFrameImpl_Icon,
		m_rcNcFrameImpl_Text,
		m_rcNcFrameImpl_ScHelp,
		m_rcNcFrameImpl_ScClose,
		m_rcNcFrameImpl_ScMaximize,
		m_rcNcFrameImpl_ScMinimize;
	enum e_StateIndex_t
	{
		__ESI_NORMAL   = 0,
		__ESI_HOVER    = 1,
		__ESI_PRESSED  = 2,
	};
	UINT m_nNcFrameImpl_ScTrackedButtonHover,
		m_nNcFrameImpl_ScTrackedButtonPressed;
	CExtPopupMenuTipWnd m_wndNcFrameImpl_Tip;
	CExtSafeString
		m_strNcFrameImpl_TipMinimize,
		m_strNcFrameImpl_TipMaximize,
		m_strNcFrameImpl_TipRestore,
		m_strNcFrameImpl_TipClose;
	CExtNcFrameImpl();
	virtual ~CExtNcFrameImpl();
	virtual void NcFrameImpl_NcLock( bool bLock ) const;
	virtual DWORD NcFrameImpl_GetInitialStyle() const;
	virtual DWORD NcFrameImpl_GetInitialStyleEx() const;
	virtual HWND NcFrameImpl_OnQueryHWND() = 0;
	HWND NcFrameImpl_OnQueryHWND() const;
	virtual CWnd * NcFrameImpl_GetFrameWindow();
	const CWnd * NcFrameImpl_GetFrameWindow() const;
	CRect NcFrameImpl_GetNcHtRect(
		UINT nHT,
		LPMINMAXINFO pMinMaxInfo,
		LPCRECT pRectWnd = NULL
		) const;
	virtual CRect NcFrameImpl_GetNcHtRect(
		UINT nHT,
		bool bScreenMapping = true,
		bool bLayoutBordersH = false,
		bool bLayoutBordersV = false,
		LPMINMAXINFO pMinMaxInfo = NULL,
		LPCRECT pRectWnd = NULL
		) const;
	virtual CRect NcFrameImpl_GetNcScRect( UINT nSC ) const;
	virtual void NcFrameImpl_GetIcon( CExtCmdIcon & _icon ) const;
	virtual void NcFrameImpl_ReCacheScButtonRects() const;
	virtual void NcFrameImpl_DelayRgnAdjustment();
	virtual void NcFrameImpl_SetupRgn(
		WINDOWPOS * pWndPos = NULL
		);
	virtual bool NcFrameImpl_GetMinMaxInfo(
		LPMINMAXINFO pMMI
		) const;
	virtual __EXT_MFC_SAFE_LPCTSTR NcFrameImpl_GetScTipText( UINT nSC ) const;
	virtual bool NcFrameImpl_IsSupported() const;
	virtual bool NcFrameImpl_IsDwmBased() const;
	virtual bool NcFrameImpl_IsDwmCaptionReplacement() const;
	virtual bool NcFrameImpl_IsForceEmpty() const;
	virtual CRect NcFrameImpl_GetForceEmptyNcBorder() const;
	bool NcFrameImpl_IsForceEmptyNcBorderEmpty() const;
	virtual bool NcFrameImpl_IsActive();
	virtual bool NcFrameImpl_PreWindowProc( LRESULT & lResult, UINT message, WPARAM wParam, LPARAM lParam );
	virtual void NcFrameImpl_PostWindowProc( LRESULT & lResult, UINT message, WPARAM wParam, LPARAM lParam );
	virtual bool NcFrameImpl_OnQueryQuickWindowPlacement() const;
	virtual bool NcFrameImpl_RecalcNcFrame();
	virtual void PreSubclassWindow();
	virtual void PostNcDestroy();
	virtual bool NcFrameImpl_OnQuerySystemCommandEnabled(
		UINT nSystemCommandID
		);
	virtual void NcFrameImpl_CheckCursor(
		CPoint pointScreen,
		LPARAM nHT,
		bool bCheckWindowFromPoint = true
		);
	static CExtNcFrameImpl * NcFrameImpl_FindInstance(
		HWND hWnd,
		IExtNcFrameImplBridge * pNcFrameImplBridge = NULL
		);
	virtual CExtPaintManager * NcFrameImpl_GetPM();
	CExtPaintManager * NcFrameImpl_GetPM() const;
	virtual void NcFrameImpl_OnNcPaint(
		CDC & dcPaint,
		bool bOuterEmulationMode = false
		);
	virtual void NcFrameImpl_AdjustVistaDwmCompatibilityIssues();
}; // class CExtNcFrameImpl

template < class _BTNCW >
class CExtNCW
	: public _BTNCW
	, public CExtNcFrameImpl
{
public:
	CExtNCW()
	{
	}
	virtual ~CExtNCW()
	{
	}
	virtual HWND NcFrameImpl_OnQueryHWND()
	{
		return GetSafeHwnd();
	}
protected:
	virtual void PreSubclassWindow()
	{
		_BTNCW::PreSubclassWindow();
		CExtNcFrameImpl::PreSubclassWindow();
	}
	virtual void PostNcDestroy()
	{
		CExtNcFrameImpl::PostNcDestroy();
		_BTNCW::PostNcDestroy();
	}
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
	{
		if( ! NcFrameImpl_IsSupported() )
			return _BTNCW::WindowProc( message, wParam, lParam );
		HWND hWndOwn = m_hWnd;
		LRESULT lResult = 0;
		if( NcFrameImpl_PreWindowProc( lResult, message, wParam, lParam ) )
			return lResult;
		lResult = _BTNCW::WindowProc( message, wParam, lParam );
		if( ! ::IsWindow( hWndOwn ) )
			return lResult;
		if( CWnd::FromHandlePermanent(hWndOwn) == NULL )
			return lResult;
		NcFrameImpl_PostWindowProc( lResult, message, wParam, lParam );
		return lResult;
	}
}; // class CExtNCW

template < >
class CExtNCW < CFrameWnd >
	: public CFrameWnd
	, public CExtNcFrameImpl
{
public:
	CExtNCW()
	{
	}
	virtual ~CExtNCW()
	{
	}
	virtual HWND NcFrameImpl_OnQueryHWND()
	{
		return GetSafeHwnd();
	}
protected:
	virtual void PreSubclassWindow()
	{
		CFrameWnd::PreSubclassWindow();
		CExtNcFrameImpl::PreSubclassWindow();
	}
	virtual void PostNcDestroy()
	{
		CExtNcFrameImpl::PostNcDestroy();
		CFrameWnd::PostNcDestroy();
	}
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
	{
		if( ! NcFrameImpl_IsSupported() )
			return CFrameWnd::WindowProc( message, wParam, lParam );
		HWND hWndOwn = m_hWnd;
		LRESULT lResult = 0;
		if( NcFrameImpl_PreWindowProc( lResult, message, wParam, lParam ) )
			return lResult;
		lResult = CFrameWnd::WindowProc( message, wParam, lParam );
		if( ! ::IsWindow( hWndOwn ) )
			return lResult;
		if( CWnd::FromHandlePermanent(hWndOwn) == NULL )
			return lResult;
		NcFrameImpl_PostWindowProc( lResult, message, wParam, lParam );
		return lResult;
	} 
public:
	virtual void OnUpdateFrameTitle( BOOL bAddToTitle )
	{
		HWND hWndOwn = GetSafeHwnd();
		CFrameWnd::OnUpdateFrameTitle( bAddToTitle );
		if( hWndOwn != NULL && ::IsWindow( hWndOwn ) )
			::SendMessage( hWndOwn, WM_NCPAINT, 0L, 0L );
	}
	virtual void OnUpdateFrameMenu( HMENU hMenuAlt )
	{
		HWND hWndOwn = GetSafeHwnd();
		CFrameWnd::OnUpdateFrameMenu( hMenuAlt );
		if( hWndOwn != NULL && ::IsWindow( hWndOwn ) )
			::SendMessage( hWndOwn, WM_NCPAINT, 0L, 0L );
	}
}; // class CExtNCW

template < >
class CExtNCW < CMDIFrameWnd >
	: public CMDIFrameWnd
	, public CExtNcFrameImpl
{
public:
	CExtNCW()
	{
	}
	virtual ~CExtNCW()
	{
	}
	virtual HWND NcFrameImpl_OnQueryHWND()
	{
		return GetSafeHwnd();
	}
protected:
	virtual void PreSubclassWindow()
	{
		CMDIFrameWnd::PreSubclassWindow();
		CExtNcFrameImpl::PreSubclassWindow();
	}
	virtual void PostNcDestroy()
	{
		CExtNcFrameImpl::PostNcDestroy();
		CMDIFrameWnd::PostNcDestroy();
	}
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
	{
		if( ! NcFrameImpl_IsSupported() )
			return CMDIFrameWnd::WindowProc( message, wParam, lParam );
		HWND hWndOwn = m_hWnd;
		LRESULT lResult = 0;
		if( NcFrameImpl_PreWindowProc( lResult, message, wParam, lParam ) )
			return lResult;
		lResult = CMDIFrameWnd::WindowProc( message, wParam, lParam );
		if( ! ::IsWindow( hWndOwn ) )
			return lResult;
		if( CWnd::FromHandlePermanent(hWndOwn) == NULL )
			return lResult;
		NcFrameImpl_PostWindowProc( lResult, message, wParam, lParam );
		return lResult;
	} 
public:
	virtual void OnUpdateFrameTitle( BOOL bAddToTitle )
	{
		HWND hWndOwn = GetSafeHwnd();
		CMDIFrameWnd::OnUpdateFrameTitle( bAddToTitle );
		if( hWndOwn != NULL && ::IsWindow( hWndOwn ) )
			::SendMessage( hWndOwn, WM_NCPAINT, 0L, 0L );
	}
	virtual void OnUpdateFrameMenu( HMENU hMenuAlt )
	{
		HWND hWndOwn = GetSafeHwnd();
		CMDIFrameWnd::OnUpdateFrameMenu( hMenuAlt );
		if( hWndOwn != NULL && ::IsWindow( hWndOwn ) )
			::SendMessage( hWndOwn, WM_NCPAINT, 0L, 0L );
	}
}; // class CExtNCW

template < >
class CExtNCW < CExtResizableDialog >
	: public CExtResizableDialog
	, public CExtNcFrameImpl
{
public:
	CExtNCW()
	{
	}
	CExtNCW(
		UINT nIDTemplate,
		CWnd * pParentWnd = NULL
		)
		: CExtResizableDialog( nIDTemplate, pParentWnd )
	{
	}
	CExtNCW(
		__EXT_MFC_SAFE_LPCTSTR lpszTemplateName,
		CWnd * pParentWnd = NULL
		)
		: CExtResizableDialog( lpszTemplateName, pParentWnd )
	{
	}
	virtual ~CExtNCW()
	{
	}
	virtual HWND NcFrameImpl_OnQueryHWND()
	{
		return GetSafeHwnd();
	}
protected:
	virtual void PreSubclassWindow()
	{
		CExtResizableDialog::PreSubclassWindow();
		CExtNcFrameImpl::PreSubclassWindow();
	}
	virtual void PostNcDestroy()
	{
		CExtNcFrameImpl::PostNcDestroy();
		CExtResizableDialog::PostNcDestroy();
	}
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
	{
		if( ! NcFrameImpl_IsSupported() )
			return CExtResizableDialog::WindowProc( message, wParam, lParam );
		HWND hWndOwn = m_hWnd;
		LRESULT lResult = 0;
		if( NcFrameImpl_PreWindowProc( lResult, message, wParam, lParam ) )
			return lResult;
		lResult = CExtResizableDialog::WindowProc( message, wParam, lParam );
		if( ! ::IsWindow( hWndOwn ) )
			return lResult;
		if( CWnd::FromHandlePermanent(hWndOwn) == NULL )
			return lResult;
		NcFrameImpl_PostWindowProc( lResult, message, wParam, lParam );
		return lResult;
	}
}; // class CExtNCW

template < >
class CExtNCW < CFileDialog >
	: public CFileDialog
	, public CExtNcFrameImpl
{
public:
	CExtNCW(
		BOOL bOpenFileDialog,
		__EXT_MFC_SAFE_LPCTSTR lpszDefExt = NULL,
		__EXT_MFC_SAFE_LPCTSTR lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		__EXT_MFC_SAFE_LPCTSTR lpszFilter = NULL,
		CWnd* pParentWnd = NULL
		)
		: CFileDialog( 
			bOpenFileDialog, 
			lpszDefExt, 
			lpszFileName, 
			dwFlags, 
			lpszFilter, 
			pParentWnd 
			)
	{
	}
	virtual ~CExtNCW()
	{
	}
	virtual HWND NcFrameImpl_OnQueryHWND()
	{
		return GetSafeHwnd();
	}
protected:
	virtual void PreSubclassWindow()
	{
		CFileDialog::PreSubclassWindow();
		CExtNcFrameImpl::PreSubclassWindow();
	}
	virtual void PostNcDestroy()
	{
		CExtNcFrameImpl::PostNcDestroy();
		CFileDialog::PostNcDestroy();
	}
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
	{
		if( ! NcFrameImpl_IsSupported() )
			return CFileDialog::WindowProc( message, wParam, lParam );
		HWND hWndOwn = m_hWnd;
		LRESULT lResult = 0;
		if( NcFrameImpl_PreWindowProc( lResult, message, wParam, lParam ) )
			return lResult;
		lResult = CFileDialog::WindowProc( message, wParam, lParam );
		if( ! ::IsWindow( hWndOwn ) )
			return lResult;
		if( CWnd::FromHandlePermanent(hWndOwn) == NULL )
			return lResult;
		NcFrameImpl_PostWindowProc( lResult, message, wParam, lParam );
		return lResult;
	}
}; // class CExtNCW

template < >
class CExtNCW < CPrintDialog >
	: public CPrintDialog
	, public CExtNcFrameImpl
{
public:
	CExtNCW(
		BOOL bPrintSetupOnly,
		DWORD dwFlags = PD_ALLPAGES | PD_USEDEVMODECOPIES | PD_NOPAGENUMS | PD_HIDEPRINTTOFILE | PD_NOSELECTION,
		CWnd* pParentWnd = NULL
		)
		: CPrintDialog(
			bPrintSetupOnly,
			dwFlags,
			pParentWnd
		)
	{
	}
	virtual ~CExtNCW()
	{
	}
	virtual HWND NcFrameImpl_OnQueryHWND()
	{
		return GetSafeHwnd();
	}
protected:
	virtual void PreSubclassWindow()
	{
		CPrintDialog::PreSubclassWindow();
		CExtNcFrameImpl::PreSubclassWindow();
	}
	virtual void PostNcDestroy()
	{
		CExtNcFrameImpl::PostNcDestroy();
		CPrintDialog::PostNcDestroy();
	}
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
	{
		if( ! NcFrameImpl_IsSupported() )
			return CPrintDialog::WindowProc( message, wParam, lParam );
		HWND hWndOwn = m_hWnd;
		LRESULT lResult = 0;
		if( NcFrameImpl_PreWindowProc( lResult, message, wParam, lParam ) )
			return lResult;
		lResult = CPrintDialog::WindowProc( message, wParam, lParam );
		if( ! ::IsWindow( hWndOwn ) )
			return lResult;
		if( CWnd::FromHandlePermanent(hWndOwn) == NULL )
			return lResult;
		NcFrameImpl_PostWindowProc( lResult, message, wParam, lParam );
		return lResult;
	}
}; // class CExtNCW

class __PROF_UIS_API CExtNcFrameWatchMDI
	: public CExtHookSink
	, public CExtPmBridge
{
public:
	DECLARE_CExtPmBridge_MEMBERS( CExtNcFrameWatchMDI );
	CExtNcFrameImpl * m_pNcFrameImpl;
	UINT m_nMsgDelayedRecalc;
	CExtNcFrameWatchMDI();
	~CExtNcFrameWatchMDI();
	void PostSync();
	virtual bool OnHookWndMsg(
		LRESULT & lResult,
		HWND hWndHooked,
		UINT nMessage,
		WPARAM & wParam,
		LPARAM & lParam
		);
	virtual void PmBridge_OnPaintManagerChanged(
		CExtPaintManager * pGlobalPM
		);
}; // class CExtNcFrameWatchMDI

template < >
class CExtNCW < CMDIChildWnd >
	: public CMDIChildWnd
	, public CExtNcFrameImpl
{
protected:
	CExtNcFrameWatchMDI m_hookWatchMDI;
public:
	CExtNCW()
	{
		m_hookWatchMDI.m_pNcFrameImpl = this;
	}
	virtual bool NcFrameImpl_IsActive()
	{
		if( GetSafeHwnd() == NULL )
			return false;
		CMDIFrameWnd * pParentFrame =
			STATIC_DOWNCAST(
				CMDIFrameWnd,
				GetParentFrame()
				);
		CMDIChildWnd * pActiveFrame = pParentFrame->MDIGetActive();
		if( pActiveFrame != this )
			return false;
		return true;
	}
	virtual bool NcFrameImpl_IsForceEmpty() const
	{
		if( GetSafeHwnd() == NULL )
			return false;
		CMDIFrameWnd * pParentFrame =
			STATIC_DOWNCAST(
				CMDIFrameWnd,
				GetParentFrame()
				);
		BOOL bMaximized = FALSE;
		CMDIChildWnd * pActiveFrame = pParentFrame->MDIGetActive( &bMaximized );
		if( pActiveFrame == NULL )
			return false;
		return bMaximized ? true : false;
	}
//	virtual CRect NcFrameImpl_GetForceEmptyNcBorder() const
//	{
//		CRect rcBorder( 0, 0, 0, 0 );
//		rcBorder.bottom = ::GetSystemMetrics( SM_CYBORDER );
//		rcBorder.top = rcBorder.bottom + ::GetSystemMetrics( SM_CYCAPTION );
//		rcBorder.left = rcBorder.right = ::GetSystemMetrics( SM_CXBORDER );
//		return rcBorder;
//	}
	virtual HWND NcFrameImpl_OnQueryHWND()
	{
		return GetSafeHwnd();
	}
	virtual BOOL DestroyWindow()
	{
		m_hookWatchMDI.RemoveAllWndHooks();
		return CMDIChildWnd::DestroyWindow();
	}
protected:
	virtual void PreSubclassWindow()
	{
		CMDIChildWnd::PreSubclassWindow();
		CExtNcFrameImpl::PreSubclassWindow();
	}
	virtual void PostNcDestroy()
	{
		CExtNcFrameImpl::PostNcDestroy();
		CMDIChildWnd::PostNcDestroy();
	}
	virtual bool NcFrameImpl_RecalcNcFrame()
	{
		if( ! CExtNcFrameImpl::NcFrameImpl_RecalcNcFrame() )
			return false;
		CFrameWnd * pFrame = GetParentFrame();
		if( pFrame == NULL )
			return true;
		CMDIFrameWnd * pMdiFrame =
			STATIC_DOWNCAST( CMDIFrameWnd, pFrame );
		ASSERT_VALID( pMdiFrame );
		BOOL bMax = FALSE;
		CMDIChildWnd * pActive = pMdiFrame->MDIGetActive( &bMax );
		pActive;
		if( bMax )
		{
			CRect rc;
			GetParent()->GetClientRect( &rc );
			CRect rcClient, rcWnd;
			GetClientRect( &rcClient );
			GetWindowRect( &rcWnd );
			ScreenToClient( &rcWnd );
			rc.left -= rcClient.left - rcWnd.left;
			rc.top -= rcClient.top - rcWnd.top;
			rc.right += rcWnd.right - rcClient.right;
			rc.bottom += rcWnd.bottom - rcClient.bottom;
			MoveWindow( &rc );
		} // if( bMax )
		return true;
	}
	void _HandleDelayedRecalc()
	{
		MSG _msg;
		::memset( &_msg, 0, sizeof(MSG) );
		while(
			::PeekMessage(
				&_msg,
				m_hWnd,
				m_hookWatchMDI.m_nMsgDelayedRecalc,
				m_hookWatchMDI.m_nMsgDelayedRecalc,
				PM_REMOVE
				)
			);
		NcFrameImpl_RecalcNcFrame();
	}
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
	{
		if( ! NcFrameImpl_IsSupported() )
		{
			if( message == m_hookWatchMDI.m_nMsgDelayedRecalc )
			{
				_HandleDelayedRecalc();
				return 0;
			} // if( message == m_hookWatchMDI.m_nMsgDelayedRecalc )
			return CMDIChildWnd::WindowProc( message, wParam, lParam );
		} // if( ! NcFrameImpl_IsSupported() )
		bool bRestoreRedraw = false;
		if( message == WM_CREATE )
			m_hookWatchMDI.SetupHookWndSink( ::GetParent(m_hWnd) );
		else if( message == WM_DESTROY )
			m_hookWatchMDI.RemoveAllWndHooks();
		else if( message == WM_SYSCOMMAND )
			m_hookWatchMDI.PostSync();
		else if( message == WM_NCACTIVATE )
			return 1;
		else if( message == WM_WINDOWPOSCHANGED )
		{
			CMDIFrameWnd * pMdiFrame =
				STATIC_DOWNCAST( CMDIFrameWnd, GetParentFrame() );
			ASSERT_VALID( pMdiFrame );
			BOOL bMax = FALSE;
			CMDIChildWnd * pActive = pMdiFrame->MDIGetActive( &bMax );
			pActive;
			if( bMax )
			{
				bRestoreRedraw = true;
				SetRedraw( FALSE );
			} // if( bMax )
		} // else if( message == WM_WINDOWPOSCHANGED )
		else if( message == m_hookWatchMDI.m_nMsgDelayedRecalc )
		{
			_HandleDelayedRecalc();
			return 0;
		} // else if( message == m_hookWatchMDI.m_nMsgDelayedRecalc )
		HWND hWndOwn = m_hWnd;
		LRESULT lResult = 0;
		if( NcFrameImpl_PreWindowProc( lResult, message, wParam, lParam ) )
			return lResult;
		lResult = CMDIChildWnd::WindowProc( message, wParam, lParam );
		if( ! ::IsWindow( hWndOwn ) )
			return lResult;
		if( CWnd::FromHandlePermanent(hWndOwn) == NULL )
			return lResult;
		NcFrameImpl_PostWindowProc( lResult, message, wParam, lParam );
		if( bRestoreRedraw )
		{
			SetRedraw( TRUE );
			RedrawWindow(
				NULL, NULL,
				RDW_INVALIDATE|RDW_ERASE|RDW_ALLCHILDREN|RDW_FRAME
				);
		} // if( bRestoreRedraw )
		return lResult;
	}
}; // class CExtNCW

#endif // (!defined __EXT_MFC_NO_NC_FRAME )

#endif // __EXT_NC_FRAME_H

