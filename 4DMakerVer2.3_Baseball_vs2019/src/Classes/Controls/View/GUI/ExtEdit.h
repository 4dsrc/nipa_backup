// This is part of the Professional User Interface Suite library.
// Copyright (C) 2001-2006 FOSS Software, Inc.
// All rights reserved.
//
// http://www.prof-uis.com
// mailto:support@prof-uis.com
//
// This source code can be used, modified and redistributed
// under the terms of the license agreement that is included
// in the Professional User Interface Suite package.
//
// Warranties and Disclaimers:
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND
// INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
// IN NO EVENT WILL FOSS SOFTWARE INC. BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
// INCLUDING DAMAGES FOR LOSS OF PROFITS, LOSS OR INACCURACY OF DATA,
// INCURRED BY ANY PERSON FROM SUCH PERSON'S USAGE OF THIS SOFTWARE
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

#if (!defined __EXT_EDIT_H)
#define __EXT_EDIT_H

#if (!defined __EXT_MFC_DEF_H)
	#include <ExtMfcDef.h>
#endif // __EXT_MFC_DEF_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

/////////////////////////////////////////////////////////////////////////////
// CExtEditBase window

class __PROF_UIS_API CExtEditBase
	: public CEdit
	, public CExtPmBridge
{
public:
	DECLARE_DYNCREATE( CExtEditBase );
	DECLARE_CExtPmBridge_MEMBERS( CExtEditBase );

	DECLARE_PROF_UIS_WINDOW_METHODS;

	CExtEditBase();

// Attributes
public:
	bool m_bHandleCtxMenus:1;
protected:
	CToolTipCtrl m_wndToolTip;
	void InitToolTip();

	COLORREF m_clrBack;
	COLORREF m_clrBackPrev;
	CBrush m_brBack;
	COLORREF m_clrText;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExtEditBase)
	virtual BOOL PreTranslateMessage( MSG * pMsg );
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CExtEditBase();

	void SetTooltipText(
		int nId,
		BOOL bActivate =
		TRUE
		);
	void SetTooltipText(
		CExtSafeString * spText,
		BOOL bActivate = TRUE
		);
	void SetTooltipText(
		CExtSafeString & sText,
		BOOL bActivate = TRUE
		);
	void SetTooltipText(
		__EXT_MFC_SAFE_LPCTSTR sText,
		BOOL bActivate = TRUE
		);
	void ActivateTooltip(
		BOOL bEnable = TRUE
		);

	void SetBkColor( COLORREF clrBk )
	{ 
		ASSERT_VALID( this );
		m_clrBack = clrBk; 
		if( GetSafeHwnd() != NULL )
			Invalidate();
	}
	COLORREF GetBkColor() const
	{ 
		ASSERT_VALID( this );
		return m_clrBack; 
	}
	void SetTextColor( COLORREF clrText )
	{ 
		ASSERT_VALID( this );
		m_clrText = clrText; 
		if( GetSafeHwnd() != NULL )
			Invalidate();
	}
	COLORREF GetTextColor() const
	{ 
		ASSERT_VALID( this );
		return m_clrText; 
	}
	
protected:
	virtual __EXT_MFC_INT_PTR OnToolHitTest(
		CPoint point,
		TOOLINFO * pTI
		) const;

	virtual int OnQueryMaxTipWidth( 
		__EXT_MFC_SAFE_LPCTSTR lpszText 
		);

	// Generated message map functions
protected:
	//{{AFX_MSG(CExtEditBase)
	afx_msg void OnContextMenu(CWnd* pWnd,CPoint pos );	
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
}; // class CExtEditBase

/////////////////////////////////////////////////////////////////////////////
// CExtEdit window

class __PROF_UIS_API CExtEdit
	: public CExtEditBase
{
protected:
	virtual void _DoPaintNC( CDC * pDC );
	virtual void _PostRedraw();
	virtual void _DrawEditImpl(
		CRect rectClient,
		CDC * pDC = NULL
		);
	bool m_bMouseOver:1;

public:
	DECLARE_DYNCREATE( CExtEdit );

	CExtEdit();
	virtual ~CExtEdit();
	
// Attributes
public:
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExtEdit)
	protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:

	// Generated message map functions
protected:
	//{{AFX_MSG(CExtEdit)
	afx_msg void OnTimer(__EXT_MFC_UINT_PTR nIDEvent);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnNcPaint();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
}; // class CExtEdit

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // __EXT_EDIT_H
