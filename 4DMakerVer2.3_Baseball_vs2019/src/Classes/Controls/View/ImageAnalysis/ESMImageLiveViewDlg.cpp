/////////////////////////////////////////////////////////////////////////////
//
// TGImageLiveViewDlg.cpp : implementation file
//
//
// Copyright (c) 2009 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Jung Hongsu (hongsu@esmlab.com)
// @Date	2009-05-20
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TGImageLiveViewDlg.h"
#include "resource.h"
#include "Globalindex.h"
#include "MainFrm.h"

//TimeTrace
#include <sys/timeb.h>
#include <time.h>

#define MAX_WIDTH	2048
#define MAX_HEIGHT	1536

extern CMainFrame* g_pTGMainWnd;	

/////////////////////////////////////////////////////////////////////////////
// CTGImageLiveViewDlg dialog


CTGImageLiveViewDlg::CTGImageLiveViewDlg(CWnd* pParent /*=NULL*/)
: CDialog(CTGImageLiveViewDlg::IDD, pParent)	
	,m_pNetView(NULL)	
	,m_strLiveviewIP(_T(""))
	,m_nChannel(0)
{

}

void CTGImageLiveViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTGImageLiveViewDlg)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);	
}


BEGIN_MESSAGE_MAP(CTGImageLiveViewDlg, CDialog)
	//{{AFX_MSG_MAP(CTGImageLiveViewDlg)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()	
	ON_COMMAND(ID_IMAGE_LIVEVIEW_ON	, OnLiveOn)
	ON_COMMAND(ID_IMAGE_LIVEVIEW_OFF, OnLiveOff)
	ON_COMMAND(ID_IMAGE_OPT			, OnLiveOption)	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTGImageLiveViewDlg message handlers

BOOL CTGImageLiveViewDlg::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}	

	//-- SET MAIN WINDOWS HANDLE
	//-- 2009-05-07
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();
	GetClientRect(m_rcClientFrame);	

	return TRUE;
}

void CTGImageLiveViewDlg::InitImageFrameWnd()
{
	//---------------------------------------------------------------
	//--IMGWND_TYPE_TESTSTEP
	//---------------------------------------------------------------
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_LIVEVIEW_ON	,
		ID_IMAGE_LIVEVIEW_OFF	,
		ID_SEPARATOR			,
		ID_IMAGE_OPT			,
	};
	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));
	m_wndToolBar.ShowWindow(SW_SHOW);
}

CTGImageLiveViewDlg::~CTGImageLiveViewDlg() {}

/////////////////////////////////////////////////////////////////////////////
// CTGImageLiveViewDlg


void CTGImageLiveViewDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	RepositionBars(0,0xFFFF,0);		
	//-- 2012-04-26 hongsu
	ViewerRect();
}

void CTGImageLiveViewDlg::ViewerRect()
{	
	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	m_rcClientFrame.top	= 30;	// tool bar			
}

CRect CTGImageLiveViewDlg::GetRect()
{
	CRect rect;
	rect = m_rcClientFrame;

	//-- SET MODE
	//-- FIX 3:2
	int nWidth = rect.Width();
	int nHeight = rect.Height();
	int nChange;
	
	//-- WIDTH LONGER THAN HEIGHT
	if(nHeight*3 < nWidth*2)
	{
		nChange = nHeight*3/2;
		rect.left	= nWidth/2 - nChange/2;
		rect.right	= nWidth/2 + nChange/2;

	}
	//-- HEIGHT LONGER THAN WIDTH
	else
	{
		nChange = nWidth*2/3;
		rect.top	= nHeight/2 - nChange/2 + 30;
		rect.bottom	= nHeight/2 + nChange/2 + 30;
	}

	return rect;
}

LONG CTGImageLiveViewDlg::InitView(LPARAM param)
{
	m_pNetView = (CTGNetworkView*)param;
	return TG_ERR_NON;
}

CString CTGImageLiveViewDlg::GetModel()	{ return m_pNetView->GetModel(); }
CString CTGImageLiveViewDlg::GetVer()	{ return m_pNetView->GetVer();	 }



void CTGImageLiveViewDlg::OnLiveOn()
{
}

void CTGImageLiveViewDlg::OnLiveOff()
{
}

void CTGImageLiveViewDlg::OnLiveOption()
{
	TGEvent* pMsg = new TGEvent ;
	pMsg->message = WM_TG_OPT_LIVEVIEW;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_TG, WM_TG_OPT, (LPARAM)pMsg);	
}
