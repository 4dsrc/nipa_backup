////////////////////////////////////////////////////////////////////////////////
//
//	TGImageFrameDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TGImageFrameDlg.h"
#include "resource.h"
#include "MainFrm.h"
#include "TGImageFunc.h"


/////////////////////////////////////////////////////////////////////////////
// CTGImageFrameDlg dialog


CTGImageFrameDlg::CTGImageFrameDlg(CWnd* pParent /*=NULL*/)
: CDialog(CTGImageFrameDlg::IDD, pParent)
{
	//-- 2012-07-12 hongsu@esmlab.com
	//-- TEMP
	//CheckRGBImage(NULL,800,600);
}


void CTGImageFrameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTGImageFrameDlg)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
	DDX_Control(pDX, IDC_PROGRESSBAR, m_ProgressText);
}


BEGIN_MESSAGE_MAP(CTGImageFrameDlg, CDialog)
	//{{AFX_MSG_MAP(CTGImageFrameDlg)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()		

	ON_COMMAND(ID_IMAGE_TOOLBAR_RECT,		OnSetToolBarRect)
	ON_COMMAND(ID_IMAGE_TOOLBAR_LINE,		OnSetToolBarLine)

	//-- SAVE CAPTURED AREAS
	ON_COMMAND(ID_IMAGE_SEL_SAVE	,		OnSetSelectSave)
	ON_COMMAND(ID_IMAGE_SEL_CLEAR	,		OnSetSelectClear)
	ON_COMMAND(ID_IMAGE_SEL_SHOW	,		OnSetSelectShow)
	ON_COMMAND(ID_IMAGE_SEL_ERASE	,		OnSetSelectErase)
	
	ON_COMMAND(ID_IMAGE_ZOOMIN,				OnZoomIn )
	ON_COMMAND(ID_IMAGE_ZOOMOUT,			OnZoomOut )
	ON_COMMAND(ID_IMAGE_ZOOM_ORG,			OnResetImageSize )
	ON_COMMAND(ID_IMAGE_ZOOM_ALL,			OnZoomAll)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTGImageFrameDlg message handlers

BOOL CTGImageFrameDlg::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}



void CTGImageFrameDlg::InitImageFrameWnd(int nWndMode)
{
	m_nImageType = nWndMode;
	//---------------------------------------------------------------
	//--IMG_TYPE_OCR
	//---------------------------------------------------------------
	if(m_nImageType == IMG_TYPE_OCR)
	{
		UINT arrCvTbBtns[] =
		{
			ID_IMAGE_ZOOMIN,
			ID_IMAGE_ZOOMOUT,
			ID_IMAGE_ZOOM_ALL,
			ID_IMAGE_ZOOM_ORG,
			ID_SEPARATOR,
			ID_IMAGE_SEL_SHOW,
			ID_IMAGE_SEL_ERASE
		};
		VERIFY(	m_wndToolBar.SetButtons(arrCvTbBtns, sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));
	}		

	GetClientRect(m_rcClientFrame);
	//-- 2009-05-20
	m_ImageMgr.Create(IDD_IMAGE_MANAGER, this);
	m_ImageMgr.SetMode(m_nImageType);
	m_ImageMgr.MoveWindow(m_rcClientFrame);
	//-- REPOSITION TOOLBAR
	RepositionBars(0,0xFFFF,0);	
	m_wndToolBar.ShowWindow(SW_SHOW);

	//-- 2010-9-15 keunbae.song
	InitProgressBar();

}

CTGImageFrameDlg::~CTGImageFrameDlg() {}

/////////////////////////////////////////////////////////////////////////////
// CTGImageFrameDlg


void CTGImageFrameDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);	
	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	m_rcClientFrame.top	= 30;
	//-- RESIZE
	m_ImageMgr.MoveWindow(m_rcClientFrame);
	RepositionBars(0,0xFFFF,0);

	//-- 2010-9-15 keunbae.song
	MoveProgressBar(cx);
}

//------------------------------------------------------------------------------ 
//! @brief		OnZoomAll()
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::OnZoomAll()
{
	if(!m_ImageMgr.FitSizeShow())
		TGLog(0,_T("Fit Size Show"));
}

//------------------------------------------------------------------------------ 
//! @brief		OnZoomIn()
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::OnZoomIn()
{
	double dbHorzScaling, dbVertScaling;
	m_ImageMgr.GetScaleRate(&dbHorzScaling, &dbVertScaling);

	dbHorzScaling +=  (dbHorzScaling * (double)IMG_SCALING_RATE);
	dbVertScaling +=  (dbVertScaling * (double)IMG_SCALING_RATE);
	m_ImageMgr.ResampleImage(dbHorzScaling, dbVertScaling);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnZoomOut()
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::OnZoomOut()
{
	double dbHorzScaling, dbVertScaling;	

	m_ImageMgr.GetScaleRate(&dbHorzScaling, &dbVertScaling);

	dbHorzScaling *= (double)IMG_SCALING_RATE;
	dbVertScaling *= (double)IMG_SCALING_RATE;
	m_ImageMgr.ResampleImage(dbHorzScaling, dbVertScaling);
}


//------------------------------------------------------------------------------ 
//! @brief		OnResetImageSize()
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::OnResetImageSize()
{
	m_ImageMgr.InitScaleRate();
	m_ImageMgr.ResampleImage(1, 1);
}


//------------------------------------------------------------------------------ 
//! @brief    Get OCR Check (Number)
//! @date     2011-11-29
//! @owner    hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT CTGImageFrameDlg::CheckOCR(OCR_CHECK_INFO* pInfo)
{
	return m_ImageMgr.CheckOCR(pInfo);	
}

//------------------------------------------------------------------------------ 
//! @brief    Get OCR Check (Number)
//! @date     2011-11-29
//! @owner    hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT CTGImageFrameDlg::CheckRGBImage(unsigned char* pBuf, int nWidth, int nHeight)
{
	LRESULT lResult = TG_ERR_NON;
	IplImage* pImage = NULL;

	pImage = m_ImageVerification.ImageCheck(pBuf, nWidth, nHeight);	
	if(pImage)
	{
		if(!LoadRGBImage(pImage))
			lResult = TG_ERR_RGB;
		//-- 2012-10-08 joonho.kim
		//TGSetImage( (LPVOID)pImage );
	}	
	//-- Release
	delete []pBuf;
	//cvReleaseImage(&pImage);
	return lResult;
}

//------------------------------------------------------------------------------ 
//! @brief    Get OCR Check (Number)
//! @date     2011-11-29
//! @owner    hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT CTGImageFrameDlg::LoadRGBImage(IplImage* pImage)
{
	LRESULT lResult = TG_ERR_NON;
	lResult = m_ImageMgr.LoadViewerImage(pImage);
	//cvReleaseImage(&pImage);
	return lResult;
}

//------------------------------------------------------------------------------ 
//! @brief    Get image score.
//! @date     2009-05-21
//! @owner    
//! @note
//! @return        
//! @revision Change NIVision into OpenCV 2010-9-10 keunbae.song 
//------------------------------------------------------------------------------ 
float CTGImageFrameDlg::GetImageScore(COMPIMAGEINFO* pInfo, IplImage* pPatternOriginal, BOOL bROIRect)
{
	m_ImageMgr.InitScaleRate();
	m_ImageMgr.ShowWindow(SW_SHOW);	

	//-- IMAGE MANAGER ==> GetImageScore
	if(!bROIRect)
		return m_ImageMgr.GetImageScore(pInfo,pPatternOriginal);
	else
		return m_ImageMgr.GetImageScoreWithROI(pInfo,pPatternOriginal);
}

//------------------------------------------------------------------------------ 
//! @brief    LoadViewerImage
//! @date     2009-05-18
//! @owner    hongsu.jung
//! @note	  Load a captured image
//------------------------------------------------------------------------------ 
BOOL CTGImageFrameDlg::LoadViewerImage(CString strPath)
{
	if(strPath.IsEmpty())
		return FALSE;

	//-- 2010-9-16 keunbae.song
	ProgressIt(TRUE);
	//m_ImageMgr.ClearImage();	
	m_ImageMgr.ShowWindow(SW_SHOW);
	m_ImageMgr.LoadViewerImage(strPath);
	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		
//! @attention	none
//-- 2009-05-18
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::AddImageColorInfo(int nIdx, double r, double g, double b)
{
	m_ImageColor.SetRawData(nIdx, r, g, b);	
}

//------------------------------------------------------------------------------ 
//! @brief		CreateImageColorInfo(CRect rc, int nSize)
//! @date		
//! @attention	none
//-- 2009-05-18
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::CreateImageColorInfo(Rect_struct rc, int nSize)
{
	m_ImageColor.CreateRawData(rc, nSize);
}
//------------------------------------------------------------------------------ 
//! @brief    clear oracle or captured image
//! @date     2010-9-14
//! @owner    keunbae.song
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::ClearImage()
{
	m_ImageMgr.ClearImage();
}

//------------------------------------------------------------------------------ 
//! @brief		InitProgressBar
//! @date		  2010-9-15
//! @attention	none
//! @comment    
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::InitProgressBar()
{
	//Only Captured-Window
	if(IMG_TYPE_OCR != m_nImageType)
	{
		m_ProgressText.ShowWindow(SW_HIDE);
		return ;
	}

	//Set Progress range
	int nProgressCount = 153;
	m_ProgressText.SetRange(0, nProgressCount);
	m_ProgressText.SetPos(0);

	//Set the ToolBar to the ProgressBar's parent
	m_ProgressText.SetParent(&m_wndToolBar);

	m_ProgressText.ShowWindow(SW_SHOW);
	m_ProgressText.SetShowText(TRUE);
	m_ProgressText.SetWindowText(_T("Ready to Image Verification"));
}

//------------------------------------------------------------------------------ 
//! @brief		  MoveProgressBar
//! @date		    2010-9-15
//! @attention	none
//! @comment    Move the ProgressBar at the end of ToolBar's buttons    
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::MoveProgressBar(int nWidth)
{
	//Only Captured-Window
	if(IMG_TYPE_OCR != m_nImageType)
		return ;

	//Get the size of ToolBar's 1st button
	CRect rtButton;
	m_wndToolBar.GetButtonRect(0, &rtButton);

	//Get the count of ToolBar's buttons
	int nCount = m_wndToolBar.GetButtonsCount();

	//Move the ProgressBar
	CRect rtProgress;
	rtProgress.left    = rtButton.Width() * nCount;
	rtProgress.top     = 2;
	rtProgress.right   = nWidth - rtButton.Width();
	rtProgress.bottom  = 20;

	m_ProgressText.MoveWindow(rtProgress);
}

//------------------------------------------------------------------------------ 
//! @brief		ProgressIt
//! @date		  2010-9-15
//! @attention	none
//! @comment    
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::ProgressIt(BOOL bReset, CString strText)
{
	//
	if(bReset)
	{
		m_ProgressText.SetPos(0);
		m_ProgressText.SetForeColour(::GetSysColor(COLOR_HIGHLIGHT));
		m_ProgressText.SetWindowText(_T("Ready to Image Verification"));
		return ;
	}
	
	if(strText.IsEmpty())
	{
		CString strResult;
		int nCurrentPos  = m_ProgressText.GetCurrentPos();
		int nMax         = m_ProgressText.GetMax();
		strResult.Format(_T("%d%%"), (int)(((float)nCurrentPos/nMax)*100));
		m_ProgressText.SetWindowText(strResult);
		m_ProgressText.StepIt();
	}
	else
	{
		if(_tcsstr(strText, TG_POSTFAIL_FAIL) != 0)
			m_ProgressText.SetForeColour(RGB(255, 0, 0));
		m_ProgressText.SetWindowText(strText);
	}
}



//------------------------------------------------------------------------------ 
//! @brief		AnalysisOCR(TGEvent* pMsg) 
//! @date		2011-11-29 hongsu.jung
//! @attention	none
//! @note	 	
//------------------------------------------------------------------------------ 
LRESULT CTGImageFrameDlg::AnalysisOCR(TGEvent* pMsg)
{
	OCR_CHECK_INFO* pOCR = NULL;
	pOCR = (OCR_CHECK_INFO*)pMsg->pParam;
	if(!pOCR)
	{
		TGLog(1, _T("[ANALYSIS] ERROR: Can't Get Info"));	
		return TG_ERR_OCR;
	}

	m_ImageMgr.ShowWindow(SW_SHOW);	
	//-- 2011-11-29 hongsu.jung
	//-- OCR Execution
	return CheckOCR(pOCR);
}

//------------------------------------------------------------------------------
//! @brief		Image Verification
//! @date		2012-07-12
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
LRESULT CTGImageFrameDlg::AnalysisRGB(TGEvent* pMsg)
{
	m_ImageMgr.ShowWindow(SW_SHOW);	
	return CheckRGBImage((unsigned char* )pMsg->pParam, pMsg->time, pMsg->nParam);
}



//------------------------------------------------------------------------------ 
//! @brief		OnSetToolBarLine()
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::OnSetToolBarLine()
{
	//-- 2010-9-8 keunbae.song
	//-- Change NIVision into OpenCV
	//m_ImageMgr.SetCurrentTool(IMAQ_LINE_TOOL);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnSetToolBarRect()
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::OnSetToolBarRect()
{
	//-- 2010-9-8 keunbae.song
	//-- Change NIVision into OpenCV
	//m_ImageMgr.SetCurrentTool(IMAQ_RECTANGLE_TOOL);
}

//------------------------------------------------------------------------------ 
//! @brief		OnSetSelectSave()
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::OnSetSelectSave()
{
	//-- 2010-9-8 keunbae.song
	//-- Change NIVision into OpenCV
	//m_ImageMgr.SetSelectedArea(TRUE);
}

//------------------------------------------------------------------------------ 
//! @brief		OnSetSelectClear()
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::OnSetSelectClear()
{
	//-- 2010-9-8 keunbae.song
	//-- Change NIVision into OpenCV
	//m_ImageMgr.SetSelectedArea(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		OnSetSelectErase()
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::OnSetSelectErase()
{
	//-- 2010-9-8 keunbae.song
	//-- Change NIVision into OpenCV
	//m_ImageMgr.SetSelectedErase();
}


//------------------------------------------------------------------------------ 
//! @brief		OnSetSelectShow()
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CTGImageFrameDlg::OnSetSelectShow()
{
	//-- 2010-9-8 keunbae.song
	//-- Change NIVision into OpenCV
	//m_ImageMgr.SetSavedRectShow();
}
