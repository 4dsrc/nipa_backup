////////////////////////////////////////////////////////////////////////////////
//
//	TGImageTestAreaDlg.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-21
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "TGImageManager.h"
#include "ColorRawData.h"

#include "TGTextProgressCtrl.h"
#include "TGImageVerification.h"

//TimeTrace
#include <sys/timeb.h>
#include <time.h>

/////////////////////////////////////////////////////////////////////////////
// CTGImageTestAreaDlg dialog

class CTGImageTestAreaDlg : public CDialog
{
public:
	enum { IDD = IDD_IMAGE_TESTAREA };

	CTGImageTestAreaDlg(CWnd* pParent = NULL);   
	virtual ~CTGImageTestAreaDlg();

	void InitImageFrameWnd(int nWndMode);		
	void ClearImage(); 	
	void DrawColor(int nColor);
	void DrawStop();

public:
	CTGImageManager m_ImageMgr;
	CString	GetTestAreaTime();

private:	
	CRect m_rcClientFrame;
	int m_nImageType;		
	//-- Start Time
	struct _timeb   m_tStart;

	void StartTimer();
	void DrawTimer();

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTGImageTestAreaDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	// Generated message map functions
	//{{AFX_MSG(CTGImageTestAreaDlg)	
	//}}AFX_MSG
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg void OnSlantEdge();
	LRESULT OnGetImage(WPARAM, LPARAM);	

	DECLARE_MESSAGE_MAP()
};
