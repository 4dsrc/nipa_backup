////////////////////////////////////////////////////////////////////////////////
//
//	TGImageTestAreaDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-21
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TGImageTestAreaDlg.h"
#include "resource.h"
#include "MainFrm.h"
#include "TGImageFunc.h"
//-- 2012-08-21 hongsu@esmlab.com 
#include "TestStepCtrlDef.h"


#define TIMER_DRAW_TIME		WM_USER + 0x00
/////////////////////////////////////////////////////////////////////////////
// CTGImageTestAreaDlg dialog


CTGImageTestAreaDlg::CTGImageTestAreaDlg(CWnd* pParent /*=NULL*/)
: CDialog(CTGImageTestAreaDlg::IDD, pParent)
{
	m_tStart.time = 100000000;
}


void CTGImageTestAreaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTGImageTestAreaDlg)
	//}}AFX_DATA_MAP	
}


BEGIN_MESSAGE_MAP(CTGImageTestAreaDlg, CDialog)
	//{{AFX_MSG_MAP(CTGImageTestAreaDlg)
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
	ON_WM_SIZE()			
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTGImageTestAreaDlg message handlers

BOOL CTGImageTestAreaDlg::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTGImageTestAreaDlg::InitImageFrameWnd(int nWndMode)
{
	m_nImageType = nWndMode;
	
	GetClientRect(m_rcClientFrame);
	//-- 2009-05-20
	m_ImageMgr.Create(IDD_IMAGE_MANAGER, this);
	m_ImageMgr.SetMode(m_nImageType);
	m_ImageMgr.MoveWindow(m_rcClientFrame);
	//-- REPOSITION TOOLBAR
	RepositionBars(0,0xFFFF,0);
}

CTGImageTestAreaDlg::~CTGImageTestAreaDlg() {}

/////////////////////////////////////////////////////////////////////////////
// CTGImageTestAreaDlg


void CTGImageTestAreaDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);	
	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	//-- RESIZE
	m_ImageMgr.MoveWindow(m_rcClientFrame);	
}


//------------------------------------------------------------------------------
//! @function	Show Splash Bitmap by growing 'SourceConstantAlpha' value		
//! @brief				
//! @date		2012-08-02
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return		none	
//! @revision		
//------------------------------------------------------------------------------
void CTGImageTestAreaDlg::OnTimer(UINT nIDEvent) 
{
	switch(nIDEvent)
	{
	case TIMER_DRAW_TIME:
		//-- 2012-08-21 hongsu@esmlab.com
		//-- Draw Time 
		DrawTimer();
		break;
	}
	CDialog::OnTimer(nIDEvent);
}
//------------------------------------------------------------------------------ 
//! @brief    clear oracle or captured image
//! @date     2010-9-14
//! @owner    keunbae.song
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void CTGImageTestAreaDlg::ClearImage()
{
	m_ImageMgr.ClearImage();
}



void CTGImageTestAreaDlg::DrawColor(int nColor)
{
	m_ImageMgr.ShowWindow(SW_SHOW);

	CvScalar color;
	switch(nColor)
	{
	case CTR_OPT2_TA_COLOR_RED		: color = CV_RGB(255,  0,  0);	m_ImageMgr.LoadViewerImage(color);	StartTimer();	break;
	case CTR_OPT2_TA_COLOR_GREEN	: color = CV_RGB(  0,255,  0);	m_ImageMgr.LoadViewerImage(color);	StartTimer();	break;
	case CTR_OPT2_TA_COLOR_BLUE		: color = CV_RGB(  0,  0,255);	m_ImageMgr.LoadViewerImage(color);	StartTimer();	break;
	case CTR_OPT2_TA_COLOR_MIX		: m_ImageMgr.LoadViewerImage()		;								break;
	default:
		TGLog(0,_T("Unknown Color"));
		return;		
	}
}

void CTGImageTestAreaDlg::DrawStop()
{
	//-- 2012-08-22 joonho.kim
	//-- Draw Stop Background White
	CvScalar color;
	color = CV_RGB( 255, 255, 255);
	m_ImageMgr.LoadViewerImage(color);

	KillTimer(TIMER_DRAW_TIME);
	m_tStart.time += 1000000;
}

CString	CTGImageTestAreaDlg::GetTestAreaTime()
{
	int nSec, nMilliSec;
	CString strTime;
	struct _timeb timebuffer;	
	_ftime(&timebuffer);

	//-- 2012-08-21 joonho.kim
	nSec = timebuffer.time - m_tStart.time;
	nMilliSec = timebuffer.millitm - m_tStart.millitm;
	if(nMilliSec < 0 )
	{
		nMilliSec = timebuffer.millitm + 1000 - m_tStart.millitm;
		nSec--;
	}

	if(nSec < 0 || nSec > 60)
		strTime.Format(STR_READY, nSec, nMilliSec);
	else
		strTime.Format(_T("[%02d.%03d]"), nSec, nMilliSec);
	return strTime;
}

void CTGImageTestAreaDlg::StartTimer()
{
	//-- 2012-08-02 hongsu@esmlab.com
	//-- Set Start Time
	_ftime(&m_tStart);
	SetTimer(TIMER_DRAW_TIME, 5, NULL);
}

void CTGImageTestAreaDlg::DrawTimer()
{
	//-- 2012-08-21 hongsu@esmlab.com
	//-- Draw Image
	m_ImageMgr.DrawTimer(GetTestAreaTime());
}
