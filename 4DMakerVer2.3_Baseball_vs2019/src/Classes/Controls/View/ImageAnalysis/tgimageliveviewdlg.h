/////////////////////////////////////////////////////////////////////////////
//
// TGImageLiveViewDlg.h
//
//
// Copyright (c) 2009 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Jung Hongsu (hongsu@esmlab.com)
// @Date	2009-05-20
//
/////////////////////////////////////////////////////////////////////////////


#pragma once

#include "resource.h"
#include "TGImageFunc.h"


class CTGNetworkView;
/////////////////////////////////////////////////////////////////////////////
// CTGImageLiveViewDlg dialog

class CTGImageLiveViewDlg : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar	{virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	// Construction
public:
	CTGImageLiveViewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTGImageLiveViewDlg();

	// Dialog Data
	//{{AFX_DATA(CTGImageLiveViewDlg)
	enum { IDD = IDD_IMAGE_LIVEVIEW };
	//}}AFX_DATA
	CInnerToolControlBar m_wndToolBar;	
	HBITMAP  m_hBmp;	
	HWND m_hMainWnd;	

private:	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTGImageLiveViewDlg)
protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	afx_msg void OnLiveOn();
	afx_msg void OnLiveOff();	
	afx_msg void OnLiveOption();	
	afx_msg BOOL OnBarCheck(UINT nID);	
	DECLARE_MESSAGE_MAP()
	
	
protected:
	// Generated message map functions
	//{{AFX_MSG(CTGImageLiveViewDlg)	
	afx_msg void OnPaint();
	//}}AFX_MSG	
	afx_msg void OnSize(UINT nType, int cx, int cy);		

public:		
	void InitImageFrameWnd();
	void DrawingView();
	BOOL SetStreamingState(CString strIPAddress, BOOL bState);
	BOOL SetStreamingRect(RECT tRect);

	//-- 2012-04-19 hongsu
	CRect m_rcClientFrame;
	LONG InitView(LPARAM param);
	LONG StartXNSLiveview(long hMediaSource)	{};
	LONG StopXNSLiveview()						{};

	//-- 2012-07-10 hongsu@esmlab.com
	// Image Verification
	BITMAPINFOHEADER m_bmiHeader;
	

private:
	CTGNetworkView* m_pNetView;	
	void RePositionControl(RECT &rect);

	CString m_strLiveviewIP;
	int m_nChannel;

public:
	void ViewerRect();
	CRect GetRect();
	CString GetModel();
	CString GetVer();
};
