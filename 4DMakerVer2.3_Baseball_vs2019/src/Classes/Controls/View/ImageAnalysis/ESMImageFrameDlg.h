////////////////////////////////////////////////////////////////////////////////
//
//	TGImageFrameDlg.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "resource.h"
#include "TGImageManager.h"
#include "ColorRawData.h"

#include "TGTextProgressCtrl.h"
#include "TGImageVerification.h"

/////////////////////////////////////////////////////////////////////////////
// CTGImageFrameDlg dialog

class CTGImageFrameDlg : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

public:
	enum { IDD = IDD_IMAGE_FRAME_DLG };

	CTGImageFrameDlg(CWnd* pParent = NULL);   
	virtual ~CTGImageFrameDlg();

	void InitImageFrameWnd(int nWndMode);	
	BOOL LoadViewerImage(CString strPath);
	void ClearImage(); 
	void InitProgressBar();
	void MoveProgressBar(int nWidth);
	void ProgressIt(BOOL bReset=FALSE, CString strText=_T(""));
	void CreateImageColorInfo(Rect_struct rcDraw, int nSize);
	void AddImageColorInfo(int nIdx, double r, double g, double b);
	float GetImageScore(COMPIMAGEINFO* pInfo, IplImage* pPatternOriginal,  BOOL bROIRect = FALSE);

	
	//-- 2011-11-29 hongsu.jung
	//-- Check OCR
	LRESULT CheckOCR(OCR_CHECK_INFO* pInfo);
	//-- 2012-07-12 hongsu@esmlab.com
	//-- Show Image
	CTGImageVerification m_ImageVerification;
	LRESULT CheckRGBImage(unsigned char* pBuf, int nWidth, int nHeight);
	LRESULT LoadRGBImage(IplImage* pImage);
	//-- 2012-07-12 hongsu@esmlab.com
	LRESULT AnalysisOCR(TGEvent* pMsg);	
	LRESULT AnalysisRGB(TGEvent* pMsg);

public:
	CTGImageManager m_ImageMgr;

private:
	CInnerToolControlBar m_wndToolBar;
	CColorRawData m_ImageColor;
	CRect m_rcClientFrame;
	int m_nImageType;	
	CTGTextProgressCtrl	m_ProgressText;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTGImageFrameDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	// Generated message map functions
	//{{AFX_MSG(CTGImageFrameDlg)	
	//}}AFX_MSG
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	afx_msg void OnZoomIn();
	afx_msg void OnZoomOut();
	afx_msg void OnResetImageSize();
	afx_msg void OnZoomAll();
	afx_msg void OnSetToolBarLine();
	afx_msg void OnSetToolBarRect();
	afx_msg void OnSetSelectSave();
	afx_msg void OnSetSelectClear();
	afx_msg void OnSetSelectShow();
	afx_msg void OnSetSelectErase();
	afx_msg void OnSlantEdge();
	LRESULT OnGetImage(WPARAM, LPARAM);	

	DECLARE_MESSAGE_MAP()
};
