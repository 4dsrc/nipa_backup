////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineViewTimeLine.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-27
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TimeLineView.h"
#include "DSCViewer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------
//! @function	Scalable Functions Processing
//------------------------------------------------------------------------------
int CTimeLineViewBase::GetSpanWidth()
{
	CRect rect;
	GetClientRect(rect);
	//-- Scaling from ClientRect
	if(m_bScaleToWindow)
		return rect.Width() - m_nLineLeftMargin - m_nLineRightMargin;
	//-- Original Size
	return GetNumberMajorTicks() *  m_nTickMajorSize;
}

int CTimeLineViewBase::GetNumberMajorTicks()
{
	return m_nTimeMax / m_nTickFreqMajor;
}

//------------------------------------------------------------------------------
//! @function	Positioning Within the Scale
//------------------------------------------------------------------------------
int CTimeLineViewBase::GetSpanTop(int nLineCnt)
{
	return ( nLineCnt * m_nLineHeight ) + m_nLineTopMargin;
}

int CTimeLineViewBase::GetTotalHeight()
{
	//int nLineCnt = GetItemCount();
	//return GetSpanTop(nLineCnt);
	return m_nLineHeight+m_nLineTopMargin;
}

int CTimeLineViewBase::GetXFromTime(int nTime)
{
	return m_nLineLeftMargin + GetSpanWidth() * nTime / m_nTimeMax;
}

//------------------------------------------------------------------------------
//! @function	Inverse Positioning Within the Scale
//------------------------------------------------------------------------------
// what is the time at this x at
int CTimeLineViewBase::GetTimeFromX(int x)
{
	int nRetTime = 0;
	if( x < m_nLineLeftMargin) 
		nRetTime = m_nLineLeftMargin;
	else if( x > m_nLineLeftMargin + GetSpanWidth()) 
		nRetTime = m_nTimeMax;
	else
		nRetTime = (x - m_nLineLeftMargin) * m_nTimeMax / GetSpanWidth();

	//-- 2013-09-26 hongsu@esmlab.com
	//-- Near 33 ms unit
	int nDivision = nRetTime / movie_next_frame_time;
	nDivision *= movie_next_frame_time;
	return nDivision;
}

//------------------------------------------------------------------------------
//! @function	Data Functions
//------------------------------------------------------------------------------
void CTimeLineViewBase::SetTimeMax(int nMax)
{
	if(nMax <= 0)
		return;
	m_nTimeMax = nMax;
}

void CTimeLineViewBase::Refresh()
{
	Invalidate(FALSE);
}
