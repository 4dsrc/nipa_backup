////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineViewBase.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-11
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCViewer.h"
#include "TimeLineView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//IMPLEMENT_DYNCREATE(CTimeLineViewBase, CScrollView)
BEGIN_MESSAGE_MAP(CTimeLineViewBase, CScrollView)
	//{{AFX_MSG_MAP(CTimeLineViewBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()	
	//}}AFX_MSG_MAP
	ON_WM_DESTROY()
	ON_WM_KEYUP()
END_MESSAGE_MAP()


// CTimeLineViewBase
CTimeLineViewBase::CTimeLineViewBase()
{
	m_bRecal			= FALSE;
	m_pFont				= NULL;
	m_bScaleToWindow	= TRUE;	

	//-- 2013-09-13 hongsu@esmlab.com
	//-- For SendMessage
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();

	m_nHeaderTop = DSC_ROW_H + DSC_HEADER_H;
	m_nTickMajorHeight = m_nHeaderTop / 5 * 3 ;
	m_nTickMinorHeight = m_nHeaderTop / 3 ;

	SetTimeMax(DSC_DEFAULT_TIME_MAX);	// 5 second
	m_nTickFreqMinor	= 100;			// Small line
	m_nTickFreqMajor	= 1000;			// Big Line
	m_nTickMajorSize	= 85;			// Size of 1 Second

	m_nLineLeftMargin	= DSC_ICON_TIMESPOT_W/2;
	m_nLineRightMargin	= DSC_ICON_TIMESPOT_W/2;	

	m_nLineHeight		= DSC_ROW_H;
	m_nLineTopMargin	= m_nHeaderTop;// + 5;//2;

	m_nHandleWidth = 8;
	m_nTextMargin = 5;

	m_ptScroll.x	= 0;
	m_ptScroll.y	= 0;

	m_hThreadDraw	= NULL;
	m_bClose		= FALSE;
}

CTimeLineViewBase::~CTimeLineViewBase()
{		
	if(m_pFont)
	{
		delete m_pFont;
		m_pFont = NULL;
	}

	while(m_hThreadDraw)
	{
		m_bClose = TRUE;
		Sleep(1);
	}
	WaitForSingleObject(m_hThreadDraw, INFINITE);
	CloseHandle(m_hThreadDraw);	
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------

int CTimeLineViewBase::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;		
	return 0;
}

BOOL CTimeLineViewBase::OnEraseBkgnd(CDC* pDC) 
{
	return TRUE;
}

void CTimeLineViewBase::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
	UpdateSizes();
} 

void CTimeLineViewBase::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
	UpdateSizes();	
}

void CTimeLineViewBase::UpdateSizes()
{
	CSize sizeTotal;
	sizeTotal.cy = GetTotalHeight();
	sizeTotal.cx = GetSpanWidth() + m_nLineLeftMargin + m_nLineRightMargin;	
	SetScrollSizes(MM_TEXT, sizeTotal);

	m_ptScroll.x = GetScrollPosition().x;
	m_ptScroll.y = GetScrollPosition().y;
}

void CTimeLineViewBase::SetFrameFont(CDC* pDC, int nSize, CString strFont, int nFontType)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	pDC->SelectObject(m_pFont);	
}

void CTimeLineViewBase::OnDestroy()
{
	__super::OnDestroy();
}

