////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineViewBase.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-11
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "ESMDropTarget.h"

class CTimeLineViewBase : public CScrollView, public CESMDropTarget
{
	//DECLARE_DYNCREATE(CTimeLineViewBase)
public:
	CTimeLineViewBase();
	~CTimeLineViewBase();

public:

	void UpdateSizes();		
	void DrawTimeLine(CDC* pDC);
	void CheckOverObject(CPoint point);
	void CheckOverReset();

protected:
	//-- parent view
	CWnd* m_pDSCViewer;
	HWND m_hMainWnd;
	
	

public:
	BOOL	m_bRecal;
	BOOL	m_bClose;
	HANDLE	m_hThreadDraw;
	
	BOOL	m_bScaleToWindow;	
	CPoint	m_ptScroll;
	int		m_nHeaderTop;
	int		m_nTickMajorHeight;
	int		m_nTickMinorHeight;	
	int		m_nTickMajorSize;
	int		m_nTimeMax;

	int		m_nLineLeftMargin;
	int		m_nLineRightMargin;
	int		m_nTickFreqMinor;
	int		m_nTickFreqMajor;	

	int		m_nLineHeight;
	int		m_nTextMargin;
	int		m_nHandleWidth;
	int		m_nLineTopMargin;

public:
	//------------------------------------------------------------------------------
	//! @function	Scalable Functions Processing
	//------------------------------------------------------------------------------
	int GetSpanWidth();
	int GetNumberMajorTicks();

	//------------------------------------------------------------------------------
	//! @function	Positioning Within the Scale
	//------------------------------------------------------------------------------
	int GetSpanTop(int nLineCnt);
	int GetTotalHeight();
	int GetXFromTime(int nTime);
	int GetTimeFromX(int x);
	void SetTimeMax(int Max);
	void Refresh();


	//------------------------------------------------------------------------------
	//! @function	Control Object
	//------------------------------------------------------------------------------
	//-- 2013-12-12 kjpark@esmlab.com
	//-- if converting, toolbar disenable
	HWND GetTimeLineViewHandle(){return m_hMainWnd;}
	
protected:
	CFont *m_pFont;
	void SetFrameFont(CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);	

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimeLineView)
protected:
	//virtual void OnDraw(CDC* pDC);   // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct		
	

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	//}}AFX_VIRTUAL	
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnDestroy();	
};

