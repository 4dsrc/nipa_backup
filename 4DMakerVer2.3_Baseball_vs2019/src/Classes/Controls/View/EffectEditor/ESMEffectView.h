////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectView.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "DSCItem.h"
#include "TimeLineViewBase.h"
#include "ESMTimeLineObjectEditor.h"

class CESMEffectEditor;
class CESMObjectFrame;
class CESMTimeLineObjectEditor;

class CESMEffectView : public CTimeLineViewBase
{
	//DECLARE_DYNCREATE(CESMEffectView)
public:
	CESMEffectView(CWnd* pParentWnd);
	~CESMEffectView();

private:
	CRITICAL_SECTION m_CR_EffectView;
	
protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
	//-- Get Parent View
	CESMEffectEditor* m_pEffectEditor;	

	void DrawObjectZoomRatio();
	void DrawObjectZoomRatioTimeLine(CDC* pDC);
	void DrawObjectZoomRatioGraph(CDC* pDC);

	void DrawFilmBase(CDC* pDC);
	void DrawFilmFrame(CDC* pDC, CESMTimeLineObjectEditor* pObject);
	
	CESMTimeLineObjectEditor* GetMovieObject();
	void LoadMovieObject();

	void LoadSpots();
	void CreateBaseSpots();

	
	
public:
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimeLineView)
protected:
	virtual void OnDraw(CDC* pDC);   // overridden to draw this view
	//}}AFX_VIRTUAL	
	DECLARE_MESSAGE_MAP()
public:	
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
};

