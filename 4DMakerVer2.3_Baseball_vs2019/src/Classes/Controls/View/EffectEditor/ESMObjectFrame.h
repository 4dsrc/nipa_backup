////////////////////////////////////////////////////////////////////////////////
//
//	ESMObjectFrame.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-17
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMDefine.h"

class CESMObjectFrame : public CWnd
{
public:
	CESMObjectFrame();
	CESMObjectFrame(CString strDSC, int nTime);
	virtual ~CESMObjectFrame();
	DECLARE_DYNCREATE(CESMObjectFrame)

private:
	BOOL	m_bSpot;
	
	CString m_strDSC;
	int		m_nTime;
	int		m_nPosX;
	int		m_nPosY;

	BOOL	m_bStepFrame;
	int		m_nStartZoom;
	int		m_nEndZoom;
//	int		m_nStepFrameCount;

	BOOL	m_bZoom			;
	int		m_nZoomRatio	;	
	int		m_nZoomRange	;
	float	m_fZoomWeight	;
	BOOL	m_bBlur			;
	int		m_nBlurSize		;
	int		m_nBlurWeight	;	

	//wgkim@esmlab.com
	double	m_dOverBoundaryX;
	double	m_dOverBoundaryY;

public:			
	int m_nStatus;
	void InitEffectInfo();
	int GetTime()				{ return m_nTime;}
	CString GetDSC()			{ return m_strDSC;}

	void SetSpot(BOOL b);
	BOOL IsSpot()				{ return m_bSpot; }
	BOOL IsBaseSpot();

	int GetPosX()				{ return m_nPosX;}
	void SetPosX(int n)			{ m_nPosX = n;}
	int GetPosY()				{ return m_nPosY;}
	void SetPosY(int n)			{ m_nPosY = n;}
	
	BOOL IsZoom()				{ return m_bZoom; }
	void SetZoom(BOOL b)		{ m_bZoom = b; }
	int GetZoomRatio()			{ return m_nZoomRatio;}
	void SetZoomRatio(int n)	{ m_nZoomRatio = n;}
	float GetZoomWeight()		{ return m_fZoomWeight;}
	void SetZoomWeight(float f)	{ m_fZoomWeight = f;}

	BOOL IsStepFrame()			{ return m_bStepFrame;}
	void SetStepFrame(BOOL b)	{ m_bStepFrame = b; }
	//void SetStepFrameCount(int nCount) { m_nStepFrameCount = nCount;}
	//int	GetStepFrameCount() { return m_nStepFrameCount;}
	void SetStartZoom(int nZoom){ m_nStartZoom = nZoom;}
	int GetStartZoom()			{ return m_nStartZoom;}
	void SetEndZoom(int nZoom)	{ m_nEndZoom = nZoom;}
	int GetEndZoom()			{ return m_nEndZoom;}
	
	BOOL IsBlur()				{ return m_bBlur; }
	void SetBlur(BOOL b)		{ m_bBlur = b; }
	int GetBlurSize()			{ return m_nBlurSize;}
	void SetBlurSize(int n)		{ m_nBlurSize = n;}
	int GetBlurWeight()			{ return m_nBlurWeight;}
	void SetBlurWeight(int n)	{ m_nBlurWeight = n;}

	BOOL DrawFrame();
	void FillFrame(CDC* pDC, CRect rect);
	void Copy(CESMObjectFrame* pSrc);			
	void ResetSelection();
	void SelectFrame(BOOL bSpot = FALSE);

	//wgkim@esmlab.com
	void SetOverBoundaryX(double d)	{m_dOverBoundaryX = d;}
	double GetOverBoundaryX()		{return m_dOverBoundaryX;}
	void SetOverBoundaryY(double d)	{m_dOverBoundaryY = d;}
	double GetOverBoundaryY()		{return m_dOverBoundaryY;}
	

protected:
	// Generated message map functions
	//{{AFX_MSG(C4DMakerListView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);	
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG	

protected:
	DECLARE_MESSAGE_MAP()	
};

