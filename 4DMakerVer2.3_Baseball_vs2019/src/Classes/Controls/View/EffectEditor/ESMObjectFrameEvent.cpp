////////////////////////////////////////////////////////////////////////////////
//
//	ESMObjectFrame.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-17
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMObjectFrame.h"

#include "ESMEffectView.h"
#include "ESMEffectEditor.h"
#include "ESMTimeLineObject.h"

#include "ESMCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CESMObjectFrame::SelectFrame(BOOL bSpot)
{
	//-- 2014-07-17 hongsu@esmlab.com
	//-- Draw Effect Frame Viewer
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_EFFECT_FRAME_VIEW;
	pMsg->pParam	= (LPARAM)this;
	pMsg->nParam1	= bSpot;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	
}

void CESMObjectFrame::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CWnd::OnLButtonDown(nFlags, point);

	if(m_nStatus == ESM_OBJECT_FRAME_STATUS_NONE || m_nStatus == ESM_OBJECT_FRAME_STATUS_SPOT)
	{
		//-- 2014-07-17 hongsu@esmlab.com
		//-- Reset another frame
		CESMEffectView* pParentWnd = NULL;
		pParentWnd = (CESMEffectView*)GetParent();
		if(pParentWnd)
			pParentWnd->m_pEffectEditor->m_pMovieObject->ResetSelection();

		if(m_nStatus == ESM_OBJECT_FRAME_STATUS_NONE )
		{
			m_nStatus = ESM_OBJECT_FRAME_STATUS_SELECT;
			DrawFrame();
		}

		BOOL bSetSpotInfo = FALSE;
		if(m_nStatus == ESM_OBJECT_FRAME_STATUS_SPOT )
			bSetSpotInfo = TRUE;
		
		//-- Select Frame
		SelectFrame(bSetSpotInfo);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDblClk
//! @date		2011-07-27
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CESMObjectFrame::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	//-- 2014-07-17 hongsu@esmlab.com
	//-- Reset another frame
	CESMEffectView* pParentWnd = NULL;
	pParentWnd = (CESMEffectView*)GetParent();
	if(pParentWnd)
		pParentWnd->m_pEffectEditor->m_pMovieObject->ResetSelection();

	if(m_nStatus != ESM_OBJECT_FRAME_STATUS_SPOT)
	{
		//-- 2014-07-17 hongsu@esmlab.com
		//-- Add to ListCtrl		
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_EFFECT_LIST_CHANGE;
		pMsg->nParam1	= TRUE;
		pMsg->pParam	= (LPARAM)this;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);
	}
	else
	{
		//-- 2014-07-17 hongsu@esmlab.com
		//-- Remove ListCtrl		
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_EFFECT_LIST_CHANGE;
		pMsg->nParam1	= FALSE;
		pMsg->pParam	= (LPARAM)this;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);
	}

	//-- Redraw
	DrawFrame();
	CWnd::OnLButtonDblClk(nFlags, point);	
}

