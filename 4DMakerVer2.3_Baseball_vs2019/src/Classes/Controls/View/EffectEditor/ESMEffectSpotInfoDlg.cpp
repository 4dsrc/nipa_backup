////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectSpotInfoDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-15
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMEffectSpotInfoDlg.h"
#include "resource.h"
#include "ESMIndex.h"
#include "MainFrm.h"
#include "ESMObjectFrame.h"
#include "ESMEffectFrame.h"

/////////////////////////////////////////////////////////////////////////////
// CESMEffectSpotInfoDlg dialog

CESMEffectSpotInfoDlg::CESMEffectSpotInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMEffectSpotInfoDlg::IDD, pParent)	
	, m_strDSC		(_T(""))
	, m_nSpotTime	(0)
	, m_nPosX		(FULL_UHD_WIDTH/2)	
	, m_nPosY		(FULL_UHD_HEIGHT/2)	
	, m_nZoomRatio	(200)
	, m_nZoomRange  (0)
	, m_fZoomWeight	(1.0)
	, m_nBlurSize	(50)
	, m_nBlurWeight	(50)
	, m_bBlur(FALSE)
	, m_bZoom(FALSE)
	, m_bSpot(FALSE)
{	
	m_pEffectFrame  = NULL;
	m_pEffectEditor = (CESMEffectEditor*)pParent;
}

CESMEffectSpotInfoDlg::~CESMEffectSpotInfoDlg() 
{
	m_pEffectEditor = NULL;
}

void CESMEffectSpotInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMEffectSpotInfoDlg)
	DDX_Control(pDX, IDC_EFFECT_SPOT_LIST, m_listSpot);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_EFFECT_SPOT_DSC			, m_strDSC		);
	DDX_Text(pDX, IDC_EFFECT_SPOT_TIME			, m_nSpotTime	);
	DDX_Text(pDX, IDC_EFFECT_SPOT_X				, m_nPosX		);
	DDX_Text(pDX, IDC_EFFECT_SPOT_Y				, m_nPosY		);
	DDX_Text(pDX, IDC_EFFECT_SPOT_ZOOM_RATIO	, m_nZoomRatio	);
	DDX_Text(pDX, IDC_EFFECT_SPOT_ZOOM_RANGE	, m_nZoomRange	);
	DDX_Text(pDX, IDC_EFFECT_SPOT_ZOOM_WEIGHT	, m_fZoomWeight	);
	DDX_Text(pDX, IDC_EFFECT_SPOT_BLUR_SIZE		, m_nBlurSize	);
	DDX_Text(pDX, IDC_EFFECT_SPOT_BLUR_WEIGHT	, m_nBlurWeight	);
	DDV_MinMaxInt(pDX, m_nZoomRatio	, EFFECT_ZOOM_RATIO_MIN, EFFECT_ZOOM_RATIO_MAX);
	DDV_MinMaxFloat(pDX, m_fZoomWeight, EFFECT_ZOOM_WEIGHT_MIN, EFFECT_ZOOM_WEIGHT_MAX);
	DDV_MinMaxInt(pDX, m_nBlurSize	, EFFECT_BLUR_SIZE_MIN, EFFECT_BLUR_SIZE_MAX);
	DDV_MinMaxInt(pDX, m_nBlurWeight, EFFECT_BLUR_WEIGHT_MIN, EFFECT_BLUR_WEIGHT_MAX);
	DDX_Check(pDX, IDC_EFFECT_SPOT_CHECK_BLUR, m_bBlur);
	DDX_Check(pDX, IDC_EFFECT_SPOT_CHECK_ZOOM, m_bZoom);
}

BOOL CESMEffectSpotInfoDlg::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN || pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
			return FALSE;
	}

	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN)
		{
			BOOL bShift = ((GetKeyState(VK_SHIFT) & 0x8000) != 0);
			BOOL bControl = ((GetKeyState(VK_CONTROL) & 0x8000) != 0);
			BOOL bAlt = ((GetKeyState(VK_MENU) & 0x8000) != 0);

			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			if(bControl && !bShift && !bAlt)
			{
				ESMEvent* pEsmMsg = NULL;
				pEsmMsg = new ESMEvent();

				pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
				pEsmMsg->nParam1 = pMsg->wParam;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
			}

		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMEffectSpotInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CESMEffectSpotInfoDlg)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()	
	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_EFFECT_SPOT_CHECK_BLUR, OnClickedEffectSpotCheckBlur)
	ON_BN_CLICKED(IDC_EFFECT_SPOT_CHECK_ZOOM, OnClickedEffectSpotCheckZoom)
	ON_BN_CLICKED(IDC_EFFECT_SPOT_RESET, &CESMEffectSpotInfoDlg::OnBnClickedEffectSpotReset)
	ON_BN_CLICKED(IDC_EFFECT_SPOT_ACCEPT, &CESMEffectSpotInfoDlg::OnBnClickedEffectSpotAccept)
	ON_BN_CLICKED(IDC_EFFECT_SPOT_RANGE, &CESMEffectSpotInfoDlg::OnBnClickedEffectSpotRange)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CESMEffectSpotInfoDlg message handlers

BOOL CESMEffectSpotInfoDlg::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}	

	//-- SET MAIN WINDOWS HANDLE
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();	
	m_brush.CreateSolidBrush(COLOR_BASIC_FG_1); 

	//-- 2014-07-16 hongsu@esmlab.com
	//-- Set Box En/Disable 
	SetStatusZoom();
	SetStatusBlur();

	GetDlgItem(IDC_EFFECT_SPOT_DSC)->EnableWindow(FALSE);
	GetDlgItem(IDC_EFFECT_SPOT_TIME)->EnableWindow(FALSE);

	m_listSpot.Init();

	return TRUE;
}

BOOL CESMEffectSpotInfoDlg::OnEraseBkgnd(CDC* pDC)
{ 
	CRect r;
	GetClientRect(&r);
	pDC->FillRect(&r, &m_brush);
	return TRUE;
}

HBRUSH CESMEffectSpotInfoDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	switch(nCtlColor)
	{
	case CTLCOLOR_EDIT	:	pDC->SetBkColor(COLOR_WHITE)		;
							pDC->SetTextColor(COLOR_BASIC_BG_DARK)	; break;
	default:				pDC->SetBkColor(COLOR_BASIC_FG_1)	;
							pDC->SetTextColor(COLOR_BASIC_BG_DARK)	; break;
	}
	
	return (HBRUSH) m_brush;
}

/////////////////////////////////////////////////////////////////////////////
// CESMEffectSpotInfoDlg

void CESMEffectSpotInfoDlg::OnPaint()
{	
	CDialog::OnPaint();
}

void CESMEffectSpotInfoDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
}

void CESMEffectSpotInfoDlg::OnClose()
{
	CDialog::OnClose();
}

void CESMEffectSpotInfoDlg::OnClickedEffectSpotCheckZoom()
{
	UpdateData(TRUE);
	//-- 2014-07-16 hongsu@esmlab.com
	//-- change Status Enable/Disable 
	SetStatusZoom();

	if(m_pEffectFrame)
		m_pEffectFrame->UpdateFrame();

	//-- 2014-07-21 hongsu@esmlab.com
	//-- Set Focus
	m_pEffectFrame->SetFocus();
	
	UpdateData(FALSE);
}

void CESMEffectSpotInfoDlg::OnClickedEffectSpotCheckBlur()
{
	UpdateData(TRUE);
	//-- 2014-07-16 hongsu@esmlab.com
	//-- change Status Enable/Disable 
	SetStatusBlur();
	UpdateData(FALSE);
}

void CESMEffectSpotInfoDlg::SetStatusDlg()
{
	GetDlgItem(IDC_EFFECT_SPOT_DSC				)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_TIME				)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_X				)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_Y				)->EnableWindow(m_bSpot);

	GetDlgItem(IDC_EFFECT_SPOT_CHECK_ZOOM		)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_CHECK_BLUR		)->EnableWindow(m_bSpot);

	GetDlgItem(IDC_EFFECT_SPOT_BLUR_SIZE		)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_SIZE_S1		)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_SIZE_S2		)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_WEIGHT		)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_WEIGHT_S1	)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_WEIGHT_S2	)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_SIZE		)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_SIZE_S1		)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_SIZE_S2		)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_WEIGHT		)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_WEIGHT_S1	)->EnableWindow(m_bSpot);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_WEIGHT_S2	)->EnableWindow(m_bSpot);
}

void CESMEffectSpotInfoDlg::SetStatusZoom()
{
	SetStatusZoomDlg();	
	//-- 2014-07-21 hongsu@esmlab.com
	//-- Update ObjectFrame	
	if(m_pEffectEditor)
		if(m_pEffectEditor->m_pSelectFrame)
		{
			m_pEffectEditor->m_pSelectFrame->SetZoom(m_bZoom);
			m_listSpot.RedrawItems(m_pEffectEditor->m_pSelectFrame);
		}
}

void CESMEffectSpotInfoDlg::SetStatusZoomDlg()
{
	//-- 2014-07-22 hongsu@esmlab.com
	//-- Clear Preview
	if(!m_bZoom)
		if(m_pEffectFrame)
			m_pEffectFrame->ClearPreview();

	BOOL bZoom = m_bZoom;
	if(!m_bSpot)
		bZoom = FALSE;
	GetDlgItem(IDC_EFFECT_SPOT_ZOOM_RATIO		)->EnableWindow(bZoom);
	GetDlgItem(IDC_EFFECT_SPOT_ZOOM_RATIO_S1	)->EnableWindow(bZoom);
	GetDlgItem(IDC_EFFECT_SPOT_ZOOM_RATIO_S2	)->EnableWindow(bZoom);
	GetDlgItem(IDC_EFFECT_SPOT_ZOOM_WEIGHT		)->EnableWindow(bZoom);
	GetDlgItem(IDC_EFFECT_SPOT_ZOOM_WEIGHT_S1	)->EnableWindow(bZoom);
	GetDlgItem(IDC_EFFECT_SPOT_ZOOM_WEIGHT_S2	)->EnableWindow(bZoom);
}

void CESMEffectSpotInfoDlg::SetStatusBlur()
{
	SetStatusBlurDlg();
	//-- 2014-07-21 hongsu@esmlab.com
	//-- Update ObjectFrame	
	if(m_pEffectEditor)
		if(m_pEffectEditor->m_pSelectFrame)
		{
			m_pEffectEditor->m_pSelectFrame->SetZoom(m_bBlur);
			m_listSpot.RedrawItems(m_pEffectEditor->m_pSelectFrame);
		}
}

void CESMEffectSpotInfoDlg::SetStatusBlurDlg()
{
	BOOL bBlur = m_bBlur;
	if(!m_bSpot)
		bBlur = FALSE;

	GetDlgItem(IDC_EFFECT_SPOT_BLUR_SIZE		)->EnableWindow(bBlur);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_SIZE_S1		)->EnableWindow(bBlur);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_SIZE_S2		)->EnableWindow(bBlur);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_WEIGHT		)->EnableWindow(bBlur);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_WEIGHT_S1	)->EnableWindow(bBlur);
	GetDlgItem(IDC_EFFECT_SPOT_BLUR_WEIGHT_S2	)->EnableWindow(bBlur);
}

void CESMEffectSpotInfoDlg::UpdateList(CESMObjectFrame* pObjFrm, BOOL bAdd)
{
	if(!m_pEffectEditor)
		return;

	if(bAdd)
	{
		int nOrder = 0;
		nOrder = m_pEffectEditor->GetInsertOrder(pObjFrm);
		m_listSpot.InsertSpot(nOrder, pObjFrm);

		//-- 2014-09-15 kcd
		//-- Movie Laod delay 확인 필요.일단 주석.
		//m_pEffectEditor->SelectListItem(pObjFrm);
	}
	else
	{
		m_listSpot.RemoveSpot(pObjFrm);
	}
}

void CESMEffectSpotInfoDlg::SetSelectFrm(CESMObjectFrame* pObjFrm, BOOL bSpot)
{
	if(!pObjFrm)
		return;

	//-- Is Spot?
	m_bSpot = bSpot;
	SetStatusDlg();

	CRect rect;
	m_pEffectFrame->GetView()->GetClientRect(rect);

	//-- 2014-07-21 hongsu@esmlab.com
	//-- Set Item Focus 
	m_listSpot.SetItemFocus(pObjFrm);

	m_strDSC		= pObjFrm->GetDSC();
	m_nSpotTime		= pObjFrm->GetTime();

	m_nPosX			= pObjFrm->GetPosX();
	if(m_nPosX == 0 )
	{
		m_nPosX = FULL_UHD_WIDTH/2;
		pObjFrm->SetPosX(m_nPosX);
	}
	m_nPosY			= pObjFrm->GetPosY();
	if(m_nPosY== 0 )
	{
		m_nPosY = FULL_UHD_HEIGHT/2;
		pObjFrm->SetPosY(m_nPosY);
	}

	m_bZoom			= pObjFrm->IsZoom();
	SetStatusZoomDlg();
	m_nZoomRatio	= pObjFrm->GetZoomRatio();	
	m_fZoomWeight	= pObjFrm->GetZoomWeight();

	m_bBlur			= pObjFrm->IsBlur();
	SetStatusBlurDlg();
	m_nBlurSize		= pObjFrm->GetBlurSize();
	m_nBlurWeight	= pObjFrm->GetBlurWeight();

	//-- Frame Draw
	m_pEffectFrame->DrawFrame(pObjFrm);
	UpdateData(FALSE);
}

//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
void CESMEffectSpotInfoDlg::UpdatePosition(int nX , int nY, BOOL bAllChange)
{
	m_nPosX	= nX;
	m_nPosY	= nY;

	if(m_pEffectEditor->m_pSelectFrame)
	{
		m_pEffectEditor->m_pSelectFrame->SetPosX(nX);
		m_pEffectEditor->m_pSelectFrame->SetPosY(nY);
	}
	UpdateData(FALSE);
}

//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
void CESMEffectSpotInfoDlg::AcceptPosition(int nX , int nY, BOOL bAllChange)
{
	UpdatePosition(nX, nY);
	//-- 2014-07-22 hongsu@esmlab.com
	//-- Recalculation
	if ( m_pEffectEditor->m_pMovieObject )
		m_pEffectEditor->m_pMovieObject->RecalculateInfo();
}

//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
void CESMEffectSpotInfoDlg::UpdateZoomRatio(int nRatio, BOOL bAllChange)
{
	m_nZoomRatio	= nRatio;
	//-- 2014-07-21 hongsu@esmlab.com
	//-- Change to ListView
	if(m_pEffectEditor->m_pSelectFrame)
	{
		CSize szZoom, ptZoom;
		double fZoomSize = (double)nRatio / 100.0;
		szZoom= CSize((int) (FULL_UHD_WIDTH/fZoomSize), (int) (FULL_UHD_HEIGHT/fZoomSize));
		ptZoom= CSize((int) (m_pEffectEditor->m_pSelectFrame->GetPosX()), (int) (m_pEffectEditor->m_pSelectFrame->GetPosY()));

		if(ptZoom.cx < szZoom.cx/2)							
			ptZoom.cx = szZoom.cx/2;
		else if(ptZoom.cx > FULL_UHD_WIDTH - szZoom.cx/2)	
			ptZoom.cx = FULL_UHD_WIDTH - szZoom.cx/2;

		if(ptZoom.cy < szZoom.cy/2)							
			ptZoom.cy = szZoom.cy/2;
		else if(ptZoom.cy > FULL_UHD_HEIGHT - szZoom.cy/2)	
			ptZoom.cy = FULL_UHD_HEIGHT - szZoom.cy/2;

		m_pEffectEditor->m_pSelectFrame->SetPosX(ptZoom.cx);
		m_pEffectEditor->m_pSelectFrame->SetPosY(ptZoom.cy);

		m_pEffectEditor->m_pSelectFrame->SetZoomRatio(nRatio);
		m_listSpot.RedrawItems(m_pEffectEditor->m_pSelectFrame);

		//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
		AdjustEffectInfo(bAllChange);

		//-- 2014-07-21 hongsu@esmlab.com
		//-- Redraw
		m_pEffectEditor->m_pMovieObject->RecalculateInfo();
		m_pEffectEditor->UpdateFrames();
	}
	UpdateData(FALSE);
}

void CESMEffectSpotInfoDlg::UpdateZoomRatioforRange(int nRatio, BOOL bAllChange)
{
	m_nZoomRatio	= nRatio;
	//-- 2014-07-21 hongsu@esmlab.com
	//-- Change to ListView
	if(m_pEffectEditor->m_pSelectFrame)
	{
		CSize szZoom, ptZoom;
		double fZoomSize = (double)nRatio / 100.0;
		szZoom= CSize((int) (FULL_UHD_WIDTH/fZoomSize), (int) (FULL_UHD_HEIGHT/fZoomSize));
		ptZoom= CSize((int) (m_pEffectEditor->m_pSelectFrame->GetPosX()), (int) (m_pEffectEditor->m_pSelectFrame->GetPosY()));

		if(ptZoom.cx < szZoom.cx/2)							
			ptZoom.cx = szZoom.cx/2;
		else if(ptZoom.cx > FULL_UHD_WIDTH - szZoom.cx/2)	
			ptZoom.cx = FULL_UHD_WIDTH - szZoom.cx/2;

		if(ptZoom.cy < szZoom.cy/2)							
			ptZoom.cy = szZoom.cy/2;
		else if(ptZoom.cy > FULL_UHD_HEIGHT - szZoom.cy/2)	
			ptZoom.cy = FULL_UHD_HEIGHT - szZoom.cy/2;

		m_pEffectEditor->m_pSelectFrame->SetPosX(ptZoom.cx);
		m_pEffectEditor->m_pSelectFrame->SetPosY(ptZoom.cy);

		m_pEffectEditor->m_pSelectFrame->SetZoomRatio(nRatio);
		m_listSpot.RedrawItems(m_pEffectEditor->m_pSelectFrame);

		//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
		AdjustEffectInfoforRange(bAllChange);

		//-- 2014-07-21 hongsu@esmlab.com
		//-- Redraw
		m_pEffectEditor->m_pMovieObject->RecalculateInfo();
		m_pEffectEditor->UpdateFrames();
	}
	UpdateData(FALSE);
}


//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
void CESMEffectSpotInfoDlg::AdjustEffectInfo(BOOL bAllChange)
{
	CESMTimeLineObjectEditor* pObj = m_pEffectEditor->GetMovieObject();
	if ( pObj==NULL)
		return;
		int nOrder = pObj->GetObjectFrameOrder(m_pEffectEditor->m_pSelectFrame);
	for ( int i = nOrder+1 ; i < pObj->GetObjectFrameCount() ; i++ )
	{
		CESMObjectFrame* pObjFrame = pObj->GetObjectFrame(i);		
		pObjFrame->SetZoomRatio(pObj->GetObjectFrame(nOrder)->GetZoomRatio());
		pObjFrame->SetPosX(pObj->GetObjectFrame(nOrder)->GetPosX());
		pObjFrame->SetPosY(pObj->GetObjectFrame(nOrder)->GetPosY());

		m_listSpot.RedrawItems(pObjFrame);
	}
	

	ESMEvent* pMsg = new ESMEvent();	
	pMsg->message = WM_ESM_FRAME_ADJUST_EFFECT_INFO;
	pMsg->nParam1 = nOrder;
	//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
	pMsg->nParam2 = bAllChange;
	pMsg->pParam = (LPARAM)pObj;
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);
}
void CESMEffectSpotInfoDlg::AdjustEffectInfoforRange(BOOL bAllChange)
{
	CESMTimeLineObjectEditor* pObj = m_pEffectEditor->GetMovieObject();
	if ( pObj==NULL)
		return;
	int range = 0;
	int nOrder = pObj->GetObjectFrameOrder(m_pEffectEditor->m_pSelectFrame);
	for ( int i = nOrder+1 ; i < pObj->GetObjectFrameCount() ; i++ )
	{
		CESMObjectFrame* pObjFrame = pObj->GetObjectFrame(i);
		range += m_nZoomRange;
		pObjFrame->SetZoomRatio(pObj->GetObjectFrame(nOrder)->GetZoomRatio()+range);
		pObjFrame->SetPosX(pObj->GetObjectFrame(nOrder)->GetPosX());
		pObjFrame->SetPosY(pObj->GetObjectFrame(nOrder)->GetPosY());

		m_listSpot.RedrawItems(pObjFrame);
	}


	ESMEvent* pMsg = new ESMEvent();	
	pMsg->message = WM_ESM_FRAME_ADJUST_EFFECT_INFO;
	pMsg->nParam1 = nOrder;
	//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
	pMsg->nParam2 = bAllChange;
	pMsg->pParam = (LPARAM)pObj;
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);
}

//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
void CESMEffectSpotInfoDlg::UpdateZoomWeight(float fWeight, BOOL bAllChange)
{
	m_fZoomWeight	= fWeight;
	//-- 2014-07-21 hongsu@esmlab.com
	//-- Change to ListView
	if(m_pEffectEditor->m_pSelectFrame)
	{
		m_pEffectEditor->m_pSelectFrame->SetZoomWeight(fWeight);
		m_listSpot.RedrawItems(m_pEffectEditor->m_pSelectFrame);

		//-- 2014-07-21 hongsu@esmlab.com
		//-- Redraw
		m_pEffectEditor->m_pMovieObject->RecalculateInfo();
		m_pEffectEditor->UpdateFrames();
	}
	UpdateData(FALSE);
}

#ifdef ONE_ACCEPT
void CESMEffectSpotInfoDlg::OnBnClickedEffectSpotReset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CESMObjectFrame* pObjFrame = m_pEffectEditor->m_pSelectFrame;
	if ( pObjFrame == NULL )
		return;

	CESMTimeLineObjectEditor* pObj = m_pEffectEditor->GetMovieObject();
	if ( pObj==NULL)
		return;

	AdjustEffectInfo();

	m_pEffectEditor->m_pMovieObject->InitSpotInfo();
	m_pEffectEditor->m_pMovieObject->RecalculateInfo();

	m_pEffectFrame->UpdateFrame();
	
	ESMEvent* pMsg = NULL;

	pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_EFFECT_UPDATE;
	pMsg->pParam	= (LPARAM)m_pEffectEditor->m_pMovieObject;		
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	
}
#else
//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
void CESMEffectSpotInfoDlg::OnBnClickedEffectSpotReset()
{
	UpdateData(TRUE);

	CESMObjectFrame* pObjFrame = m_pEffectEditor->m_pSelectFrame;
	if ( pObjFrame == NULL )
		return;

	CESMTimeLineObjectEditor* pObj = m_pEffectEditor->GetMovieObject();
	if ( pObj==NULL)
		return;

	int nWidth, nHeight;

	nWidth = (int)((double)FULL_UHD_WIDTH / ( (double)m_nZoomRatio / 100.0 ));
	nHeight = (int)((double)FULL_UHD_HEIGHT / ( (double)m_nZoomRatio / 100.0 ));

	if ( m_nPosY + nHeight/2 > FULL_UHD_HEIGHT )
		m_nPosY = FULL_UHD_HEIGHT - nHeight/2;
	else if ( m_nPosY - nHeight/2 < 0 )
		m_nPosY = 0;

	if ( m_nPosX + nWidth/2 > FULL_UHD_WIDTH )
		m_nPosX = FULL_UHD_WIDTH - nWidth/2;
	else if ( m_nPosX - nWidth/2 < 0 )
		m_nPosX = 0;

	pObjFrame->SetPosY(m_nPosY);
	pObjFrame->SetPosX(m_nPosX);

	pObjFrame->SetZoom(m_bZoom);
	pObjFrame->SetZoomWeight(m_fZoomWeight);
	pObjFrame->SetZoomRatio(m_nZoomRatio);

	pObjFrame->SetBlur(m_bBlur);
	pObjFrame->SetBlurSize(m_nBlurSize);
	pObjFrame->SetBlurWeight(m_nBlurWeight);

	AdjustEffectInfo(FALSE);

	m_pEffectEditor->m_pMovieObject->RecalculateInfo();
	m_pEffectFrame->UpdateFrame();

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_EFFECT_UPDATE;
	pMsg->pParam	= (LPARAM)m_pEffectEditor->m_pMovieObject;		
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	

	UpdateZoomWeight(m_fZoomWeight, FALSE);
	UpdateZoomRatio(m_nZoomRatio, FALSE);
	UpdatePosition(m_nPosX , m_nPosY, FALSE);
}

#endif

void CESMEffectSpotInfoDlg::OnBnClickedEffectSpotAccept()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	CESMObjectFrame* pObjFrame = m_pEffectEditor->m_pSelectFrame;
	if ( pObjFrame == NULL )
		return;

	CESMTimeLineObjectEditor* pObj = m_pEffectEditor->GetMovieObject();
	if ( pObj==NULL)
		return;

	int nWidth, nHeight;

	nWidth = (int)((double)FULL_UHD_WIDTH / ( (double)m_nZoomRatio / 100.0 ));
	nHeight = (int)((double)FULL_UHD_HEIGHT / ( (double)m_nZoomRatio / 100.0 ));

	if ( m_nPosY + nHeight/2 > FULL_UHD_HEIGHT )
		m_nPosY = FULL_UHD_HEIGHT - nHeight/2;
	else if ( m_nPosY - nHeight/2 < 0 )
		m_nPosY = 0;

	if ( m_nPosX + nWidth/2 > FULL_UHD_WIDTH )
		m_nPosX = FULL_UHD_WIDTH - nWidth/2;
	else if ( m_nPosX - nWidth/2 < 0 )
		m_nPosX = 0;

	pObjFrame->SetPosY(m_nPosY);
	pObjFrame->SetPosX(m_nPosX);
		
	pObjFrame->SetZoom(m_bZoom);
	pObjFrame->SetZoomWeight(m_fZoomWeight);
	pObjFrame->SetZoomRatio(m_nZoomRatio);
	
	pObjFrame->SetBlur(m_bBlur);
	pObjFrame->SetBlurSize(m_nBlurSize);
	pObjFrame->SetBlurWeight(m_nBlurWeight);
	
	//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
	AdjustEffectInfo(TRUE);

	m_pEffectEditor->m_pMovieObject->RecalculateInfo();
	m_pEffectFrame->UpdateFrame();
	
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_EFFECT_UPDATE;
	pMsg->pParam	= (LPARAM)m_pEffectEditor->m_pMovieObject;		
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	

	UpdateZoomWeight(m_fZoomWeight);
	UpdateZoomRatio(m_nZoomRatio);
	UpdatePosition(m_nPosX , m_nPosY);

}


void CESMEffectSpotInfoDlg::OnBnClickedEffectSpotRange()
{
	UpdateData(TRUE);	
	CESMObjectFrame* pObjFrame = m_pEffectEditor->m_pSelectFrame;
	if ( pObjFrame == NULL )
		return;

	CESMTimeLineObjectEditor* pObj = m_pEffectEditor->GetMovieObject();
	if ( pObj==NULL)
		return;

	int nWidth, nHeight;

	nWidth = (int)((double)FULL_UHD_WIDTH / ( (double)m_nZoomRatio / 100.0 ));
	nHeight = (int)((double)FULL_UHD_HEIGHT / ( (double)m_nZoomRatio / 100.0 ));

	if ( m_nPosY + nHeight/2 > FULL_UHD_HEIGHT )
		m_nPosY = FULL_UHD_HEIGHT - nHeight/2;
	else if ( m_nPosY - nHeight/2 < 0 )
		m_nPosY = 0;

	if ( m_nPosX + nWidth/2 > FULL_UHD_WIDTH )
		m_nPosX = FULL_UHD_WIDTH - nWidth/2;
	else if ( m_nPosX - nWidth/2 < 0 )
		m_nPosX = 0;

	pObjFrame->SetPosY(m_nPosY);
	pObjFrame->SetPosX(m_nPosX);

	pObjFrame->SetZoom(m_bZoom);
	pObjFrame->SetZoomWeight(m_fZoomWeight);
	pObjFrame->SetZoomRatio(m_nZoomRatio);

	pObjFrame->SetBlur(m_bBlur);
	pObjFrame->SetBlurSize(m_nBlurSize);
	pObjFrame->SetBlurWeight(m_nBlurWeight);

	//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
	AdjustEffectInfo(TRUE);

	m_pEffectEditor->m_pMovieObject->RecalculateInfo();
	m_pEffectFrame->UpdateFrame();

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_EFFECT_UPDATE;
	pMsg->pParam	= (LPARAM)m_pEffectEditor->m_pMovieObject;		
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	

	UpdateZoomWeight(m_fZoomWeight);
	UpdateZoomRatioforRange(m_nZoomRatio);
	UpdatePosition(m_nPosX , m_nPosY);

}
