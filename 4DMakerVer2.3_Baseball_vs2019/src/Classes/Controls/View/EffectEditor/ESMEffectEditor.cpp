////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectEditor.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-03
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MainFrm.h"
#include "ESMFunc.h"
#include "ESMEffectEditor.h"
#include "ESMObjectFrame.h"
#include "ESMEffectFrameView.h"

#include <afxmt.h>

#define	PREVIEW_PLAY	0x01
#define PREVIEW_TIMER	150

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//jhhan 170703 Mutex Add
CMutex g_mutexEfE(FALSE, NULL);

// CFrameNailDlg dialog
//IMPLEMENT_DYNAMIC(CESMEffectEditor, CDialog)
BEGIN_MESSAGE_MAP(CESMEffectEditor, CDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()	
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
	ON_COMMAND(ID_IMAGE_MOVIEPLAY , OnSaveEffectInfo )
	ON_UPDATE_COMMAND_UI(ID_IMAGE_MOVIEPLAY	, OnUpdateSave )	
END_MESSAGE_MAP()

CESMEffectEditor::CESMEffectEditor(CWnd* pParent /*=NULL*/)
	: CDialog(CESMEffectEditor::IDD, pParent)	
{	
	m_pEffectView		= NULL;
	m_pMovieObject		= NULL;
	m_pSelectFrame		= NULL;
	m_bPreviewPlay		= FALSE;
	m_nPreviewPlayFrame	= 0;
}

CESMEffectEditor::~CESMEffectEditor()
{
	m_pEffectView	= NULL;
	m_pMovieObject		= NULL;
}

void CESMEffectEditor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
	//{{AFX_DATA_MAP(CESMEffectEditor)
	//}}AFX_DATA_MAP	
}

BOOL CESMEffectEditor::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN || pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
			return FALSE;
	}

	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN)
		{
			BOOL bShift = ((GetKeyState(VK_SHIFT) & 0x8000) != 0);
			BOOL bControl = ((GetKeyState(VK_CONTROL) & 0x8000) != 0);
			BOOL bAlt = ((GetKeyState(VK_MENU) & 0x8000) != 0);

			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			if(bControl && !bShift && !bAlt)
			{
				ESMEvent* pEsmMsg = NULL;
				pEsmMsg = new ESMEvent();

				pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
				pEsmMsg->nParam1 = pMsg->wParam;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
			}

		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CESMEffectEditor::OnEraseBkgnd(CDC* pDC)
{ 
	CRect rect;
	GetClientRect(&rect);
	pDC->FillSolidRect(rect, COLOR_BASIC_BG_1 );
	Invalidate(FALSE);
	return TRUE;
}

// CFrameNailDlg message handlers
BOOL CESMEffectEditor::OnInitDialog()
{
	CDialog::OnInitDialog();

	//-- 2014-07-15 hongsu@esmlab.com
	//-- Create Object Zoom Ratio
	if(!m_pEffectView)
	{
		m_pEffectView = new CESMEffectView(this);
		m_pEffectView->Create(NULL, NULL, DS_SETFONT | WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDD_VIEW_TIMELINE);		
	}
	else
		return FALSE;

	//-- 2014-07-15 hongsu@esmlab.com
	//-- Create Object Info
	m_dlgObjectInfo.Create(IDD_EFFECT_OBJECTINFO,this);
	m_dlgObjectInfo.SetEffectEditor(this);

	//-- 2014-07-15 hongsu@esmlab.com
	//-- Create Object Info
	m_dlgSpotInfo.Create(IDD_EFFECT_SPOTINFO,this);
	m_dlgSpotInfo.SetEffectEditor(this);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CESMEffectEditor::OnDestroy()
{
}

void CESMEffectEditor::InitImageFrameWnd()
{
	//UINT arrCvTbBtns[] =
	//{
	//	ID_IMAGE_MOVIEPLAY		,
	//};

	//VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	

	GetClientRect(m_rcClientFrame);	

	//m_wndToolBar.ShowWindow(SW_SHOW);
}

void CESMEffectEditor::OnSize(UINT nType, int cx, int cy) 
{
	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	//m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;
	m_rcClientFrame.top	= 40;
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width()		< FRAME_MIN_WIDTH || 
		m_rcClientFrame.Height()	< FRAME_MIN_HEIGHT)
		return;

	//-- ReloadFrame
	UpdateFrames();

	CDialog::OnSize(nType, cx, cy);	
} 

void CESMEffectEditor::OnPaint()
{	
	UpdateFrames();
	CDialog::OnPaint();
}

void CESMEffectEditor::OnSaveEffectInfo()
{
	//this->SaveEffectInfo(_T("C:\\Test.vmcc"));
}

void CESMEffectEditor::SaveEffectInfo(CArchive& ar)
{ 
	if ( m_pMovieObject == NULL )
		return;

	m_pMovieObject->SaveEffectInfo(ar);
}

void CESMEffectEditor::LoadEffectInfo(CArchive& ar)
{ 
	if ( m_pMovieObject == NULL )
		return;

	m_pMovieObject->LoadEffectInfo(ar);
//	UpdateFrames();

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_EFFECT_UPDATE;
	pMsg->pParam	= (LPARAM)m_pMovieObject;		
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	

	pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_EFFECT_SPOTLIST_UPDATE;
	pMsg->pParam	= (LPARAM)m_pMovieObject;		
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	

}

void CESMEffectEditor::OnUpdateSave(CCmdUI *pCmdUI)
{

}

void CESMEffectEditor::UpdateFrames()
{
	g_mutexEfE.Lock();
	//-- Rounding Dialog
	CPaintDC dc(this);	
	CRect rtTimeLine, rtObjectInfo, rtSpotInfo;
	
	//-- 2014-07-10 hongsu@esmlab.com
	//-- Not exist Toolbar 
	m_rcClientFrame.top	= 0;
	rtTimeLine.CopyRect(m_rcClientFrame);
	rtTimeLine.bottom = SPOT_TIME_LINE_H;

	
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rtTimeLine.Width(), rtTimeLine.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rtTimeLine.Width(), rtTimeLine.Height(), SRCCOPY);

	CString strFilePath;
	mDC.FillRect(rtTimeLine, &CBrush(COLOR_BASIC_BG_3));

	//-- Object Zoom Ratio View
	if(m_pEffectView)
	{
		//-- 2014-07-16 hongsu@esmlab.com
		//-- Draw Text
		DrawTitle(&mDC);

		//-- Draw TimeLine
		rtTimeLine.left = SPOT_GRAPH_L;
		m_pEffectView->ShowWindow(SW_SHOW);
		
		m_pEffectView->MoveWindow(rtTimeLine);		
		m_pEffectView->Invalidate(FALSE);	
	}

	dc.BitBlt(0, 0, rtTimeLine.Width(), rtTimeLine.Height(), &mDC, 0, 0, SRCCOPY);
	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();


	//-- 2014-07-15 hongsu@esmlab.com
	//-- Object Info Dialog 
	rtObjectInfo.CopyRect(m_rcClientFrame);
	rtObjectInfo.top = SPOT_TIME_LINE_H;
	rtObjectInfo.right = OBJ_INFO_WINDOW_W;

	m_dlgObjectInfo.ShowWindow(SW_SHOW);
	m_dlgObjectInfo.MoveWindow(rtObjectInfo);
	//-- 2014-07-16 hongsu@esmlab.com
	//-- Update Information 
	m_dlgObjectInfo.UpdateInfo();
	m_dlgObjectInfo.Invalidate(FALSE);

	//-- Spot Info Dialog
	rtSpotInfo.CopyRect(m_rcClientFrame);
	rtSpotInfo.top = SPOT_TIME_LINE_H;
	rtSpotInfo.left = OBJ_INFO_WINDOW_W;

	m_dlgSpotInfo.ShowWindow(SW_SHOW);
	m_dlgSpotInfo.MoveWindow(rtSpotInfo);
	m_dlgSpotInfo.Invalidate(FALSE);

	g_mutexEfE.Unlock();
}
//------------------------------------------------------------------------------
//! @function	Reload Movie Object
//! @brief				
//! @date		2014-07-22
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMEffectEditor::LoadMovieObject(CESMTimeLineObjectEditor*	pObj)
{
	if(m_pMovieObject == pObj)
		return;
	
	//-- Hide Object Frames
	if(m_pMovieObject)
		m_pMovieObject->ShowFrame(FALSE);
	//-- Change Object
	m_pMovieObject = pObj;
	//-- 2014-07-18 hongsu@esmlab.com	
	m_dlgSpotInfo.ResetList();
	//-- 2014-07-22 hongsu@esmlab.com
	//-- Redraw FrameViewer
	m_dlgSpotInfo.m_pEffectFrame->ClearPreview();
	//-- Create Object Frame
	m_pEffectView->LoadMovieObject();	

}

void CESMEffectEditor::RemoveMovieObject()
{
	if(m_pMovieObject == NULL)
		return;

	//-- Change Object
	m_pMovieObject = NULL;
	//-- 2014-07-18 hongsu@esmlab.com	
	m_dlgSpotInfo.ResetList();
	//-- 2014-07-22 hongsu@esmlab.com
	//-- Redraw FrameViewer
	m_dlgSpotInfo.m_pEffectFrame->ClearPreview();
	//-- Create Object Frame
	m_pEffectView->LoadMovieObject();	

}

void CESMEffectEditor::DrawTitle(CDC* pDC)
{
	CFont font;
	font.CreateFont(13, 0, 0, 0, FW_NORMAL, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FIXED_PITCH|FF_MODERN, _T("Segoe UI"));
	pDC->SelectObject(&font);
	CPen penTitle;
	penTitle.CreatePen(PS_SOLID  ,2 , COLOR_GREEN );
	pDC->SelectObject(&penTitle);
	pDC->SetTextColor(COLOR_WHITE);
	pDC->SetBkColor(COLOR_BASIC_BG_3);

	CRect rtText;

	//-- TimeLine
	rtText.top	= SPOT_BASE_MARGIN;
	rtText.bottom = rtText.top + 20;
	rtText.left = SPOT_BASE_MARGIN;	
	rtText.right = rtText.left + SPOT_GRAPH_L;
	pDC->DrawText(_T("TimeLine"), rtText, DT_LEFT | DT_TOP | DT_SINGLELINE);

	//-- Zoom Ratio Title
	rtText.left = SPOT_BASE_MARGIN;
	rtText.right = rtText.left + SPOT_GRAPH_L - SPOT_BASE_MARGIN;
	rtText.top	= ( DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H ) + SPOT_GRAPH_H / 6 * 1 - SPOT_BASE_MARGIN;	
	rtText.bottom = rtText.top + SPOT_BASE_TEXT_H;
	
	pDC->DrawText(_T("Ratio"), rtText, DT_LEFT | DT_TOP | DT_SINGLELINE);

	//-- Zoom Ratio
	rtText.left = 0;
	pDC->SetTextColor(COLOR_BASIC_BG_DARK);
	pDC->DrawText(_T("300"), rtText, DT_RIGHT | DT_TOP | DT_SINGLELINE);

	rtText.top	= ( DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H ) + SPOT_GRAPH_H / 6 * 3 - SPOT_BASE_MARGIN;	
	rtText.bottom = rtText.top + SPOT_BASE_TEXT_H;
	pDC->DrawText(_T("200"), rtText, DT_RIGHT | DT_TOP | DT_SINGLELINE);

	rtText.top	= ( DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H ) + SPOT_GRAPH_H / 6 * 5 - SPOT_BASE_MARGIN;
	rtText.bottom = rtText.top + SPOT_BASE_TEXT_H;
	pDC->DrawText(_T("100"), rtText, DT_RIGHT | DT_TOP | DT_SINGLELINE);


}


void CESMEffectEditor::UpdateList(CESMObjectFrame* pObjFrm, BOOL bAdd)
{
	m_dlgSpotInfo.UpdateList(pObjFrm, bAdd);
	//-- 2014-07-21 hongsu@esmlab.com
	//-- Redraw Graph
	UpdateFrames();
}

int CESMEffectEditor::GetInsertOrder(CESMObjectFrame* pObjFrm)
{
	return m_pMovieObject->GetInsertOrder(pObjFrm);
}

void CESMEffectEditor::SelectListItem(CESMObjectFrame* pObjFrm, BOOL bSpot)
{
	//-- 2014-07-18 hongsu@esmlab.com
	//-- Set Item
	m_pSelectFrame = pObjFrm;
	//-- 2014-07-18 hongsu@esmlab.com
	//-- Set SpotInfoDlg
	m_dlgSpotInfo.SetSelectFrm(m_pSelectFrame, bSpot);	
}

void CESMEffectEditor::SelectListItem(BOOL bNext)
{
	if(m_pMovieObject == NULL)
		return;

	int nOrder = m_pMovieObject->GetObjectFrameOrder(m_pSelectFrame);	
	//-- 2014-07-18 hongsu@esmlab.com
	if(bNext)	nOrder++;
	else		nOrder--;
	m_pMovieObject->ClickFrame(nOrder, bNext);
}

void CESMEffectEditor::PreviewPlay()
{
	if(m_bPreviewPlay)
	{
		m_bPreviewPlay = FALSE;
		KillTimer(PREVIEW_PLAY);
	}
	else
	{
		//-- 2014-07-22 hongsu@esmlab.com
		//-- Check Frame
		if(!m_pMovieObject)
			return;
		int nAll = m_pMovieObject->GetObjectFrameCount();
		if(nAll < 2)
			return;

		//-- Start Play
		m_bPreviewPlay = TRUE;
		m_nPreviewPlayFrame = 0;
		//-- Reselect All
		m_pMovieObject->ResetSelection();
		SetTimer(PREVIEW_PLAY, PREVIEW_TIMER, NULL);
	}
}

void CESMEffectEditor::OnTimer(UINT_PTR nIDEvent) 
{
	switch(nIDEvent)
	{
	case PREVIEW_PLAY:
		//-- 2014-07-22 hongsu@esmlab.com
		//-- Click Object Frame		
		if(!m_pMovieObject)
		{
			KillTimer(PREVIEW_PLAY);
			return;
		}

		m_pMovieObject->ClickFrame(m_nPreviewPlayFrame++);
		break;
	}
	CDialog::OnTimer(nIDEvent);
}