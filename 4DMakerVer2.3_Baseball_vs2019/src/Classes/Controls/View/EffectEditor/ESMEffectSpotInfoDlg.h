////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectSpotInfoDlg.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-15
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "resource.h"
#include "afxwin.h"
#include "ESMEffectSpotListCtrl.h"

class CESMEffectEditor;
class CESMObjectFrame;
class CESMEffectFrame;

/////////////////////////////////////////////////////////////////////////////
// CESMEffectSpotInfoDlg dialog

class CESMEffectSpotInfoDlg : public CDialog
{
	// Construction
public:
	CESMEffectSpotInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMEffectSpotInfoDlg();

	// Dialog Data
	//{{AFX_DATA(CESMEffectSpotInfoDlg)
	enum { IDD = IDD_EFFECT_SPOTINFO };
	//}}AFX_DATA

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	HWND m_hMainWnd;
	CBrush m_brush;
	//-- Get Parent View
	CESMEffectEditor* m_pEffectEditor;
	//-- List Control
	CESMEffectSpotListCtrl m_listSpot;
public:
	//-- FrameView
	CESMEffectFrame* m_pEffectFrame;


public:
	CString m_strDSC;
	int m_nSpotTime		;
	int m_nPosX			;
	int m_nPosY			;
	int m_nZoomRatio	;	
	int m_nZoomRange	;
	float m_fZoomWeight	;
	int m_nBlurSize		;
	int m_nBlurWeight	;

	BOOL m_bBlur;
	BOOL m_bZoom;
	BOOL m_bSpot;

private:	
	void SetStatusDlg();
	void SetStatusZoom();
	void SetStatusZoomDlg();
	void SetStatusBlur();
	void SetStatusBlurDlg();
	void CreateSpotBase();

public:
	void SetEffectEditor(CESMEffectEditor* pEditor) { m_pEffectEditor = pEditor ; m_listSpot.SetEffectEditor(pEditor); }
	void SetFrameView(CESMEffectFrame* pView) { m_pEffectFrame = pView; }
	void SetSelectFrm(CESMObjectFrame* pObjFrm, BOOL bSpot = TRUE);
	void UpdateList(CESMObjectFrame* pObjFrm, BOOL bAdd);
	void ResetList() {m_listSpot.DeleteAllItems();}
	//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
	void UpdatePosition(int nX , int nY, BOOL bAllChange = TRUE);
	void AcceptPosition(int nX , int nY, BOOL bAllChange = TRUE);	
	void UpdateZoomRatio(int nRatio, BOOL bAllChange = TRUE);
	void UpdateZoomWeight(float fWeight, BOOL bAllChange = TRUE);
	void AdjustEffectInfo(BOOL bAllChange);
	void AdjustEffectInfoforRange(BOOL bAllChange);
	void UpdateZoomRatioforRange(int nRatio, BOOL bAllChange = TRUE);
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMEffectSpotInfoDlg)
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	DECLARE_MESSAGE_MAP()

	afx_msg void OnClickedEffectSpotCheckBlur();
	afx_msg void OnClickedEffectSpotCheckZoom();
	
	
protected:
	// Generated message map functions
	//{{AFX_MSG(CESMEffectSpotInfoDlg)	
	afx_msg void OnPaint();
	//}}AFX_MSG	
	afx_msg void OnSize(UINT nType, int cx, int cy);		
public:
	afx_msg void OnBnClickedEffectSpotReset();
	afx_msg void OnBnClickedEffectSpotAccept();
	afx_msg void OnBnClickedEffectSpotRange();
};
