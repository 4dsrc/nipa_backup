////////////////////////////////////////////////////////////////////////////////
//
//	ESMObjectFrame.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-17
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMObjectFrame.h"
#include "ESMEffectView.h"
#include "ESMCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMObjectFrame
IMPLEMENT_DYNCREATE(CESMObjectFrame, CWnd)
BEGIN_MESSAGE_MAP(CESMObjectFrame, CWnd)	
	ON_WM_CREATE()	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()

CESMObjectFrame::CESMObjectFrame()
{
	m_nStatus	= ESM_OBJECT_FRAME_STATUS_NONE;
	m_bSpot		= FALSE;
	m_bStepFrame = FALSE;
}

CESMObjectFrame::CESMObjectFrame(CString strDSC, int nTime)	
{
	m_strDSC	= strDSC;
	m_nTime		= nTime;
	m_bStepFrame = FALSE;

	InitEffectInfo();
}

CESMObjectFrame::~CESMObjectFrame()
{
	
}

void CESMObjectFrame::InitEffectInfo()
{
	//CMiLRe 20151119 동영상 사이즈에따라 X,Y 값 default 설정
	if(ESMGetMovieSize() == ESM_MOVIESIZE_FHD)
	{
		m_nPosX			= FULL_HD_WIDTH/2;
		m_nPosY			= FULL_HD_HEIGHT/2;
	}
	else
	{
		m_nPosX			= FULL_UHD_WIDTH/2;
		m_nPosY			= FULL_UHD_HEIGHT/2;
	}
	
	m_bZoom			= TRUE;
	m_nZoomRatio	= 100;
	m_nZoomRange	= 1;
	m_fZoomWeight	= 1.0;
	m_bBlur			= FALSE;
	m_nBlurSize		= 50;
	m_nBlurWeight	= 50;
	m_bSpot			= FALSE;
	m_nStatus = ESM_OBJECT_FRAME_STATUS_NONE;
}

BOOL CESMObjectFrame::IsBaseSpot()
{
	if ( m_nPosX == FULL_HD_WIDTH/2 
		&& m_nPosY == FULL_HD_HEIGHT/2
		&& m_nZoomRatio	== 100
		&& m_fZoomWeight	== 1.0
		&& m_bBlur			== FALSE
		&& m_nBlurSize		== 50
		&& m_nBlurWeight	== 50 )
		return TRUE;
	else
		return FALSE;
}

int CESMObjectFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd ::OnCreate(lpCreateStruct) == -1)
		return -1;	
	return 0;
}

//------------------------------------------------------------------------------
//! @function	sample function
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMObjectFrame::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
	DrawFrame();
} 

void CESMObjectFrame::Copy(CESMObjectFrame* pSrc)
{
	m_nTime		= pSrc->GetTime();
	m_strDSC	= pSrc->GetDSC();
}

void CESMObjectFrame::ResetSelection()
{
	if(m_nStatus == ESM_OBJECT_FRAME_STATUS_SELECT)
	{
		m_nStatus = ESM_OBJECT_FRAME_STATUS_NONE;
		DrawFrame();
	}
}

void CESMObjectFrame::SetSpot(BOOL b)
{ 
	m_bSpot = b;
	if(m_bSpot)
		m_nStatus = ESM_OBJECT_FRAME_STATUS_SPOT;
	else
		m_nStatus = ESM_OBJECT_FRAME_STATUS_NONE;
}


BOOL CESMObjectFrame::DrawFrame()
{
	if(!m_hWnd)	return FALSE;

	CDC *pDC = GetDC();
	if(!pDC) return FALSE;

	CRect rect;
	GetClientRect(&rect);
	if(!rect.Width() || !rect.Height())	
		return FALSE;

	CDC		mDC;
	CBitmap mBitmap;
	mDC.CreateCompatibleDC(pDC);
	mBitmap.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height()); 

	mDC.SelectObject(&mBitmap);	
	mDC.PatBlt(0,0,rect.Width(),rect.Height(),SRCCOPY);

	//-- DRAW
	mDC.FillRect(rect, &CBrush(RGB(0,0,0)));

	CRect rtFrame;
	rtFrame.CopyRect(rect);
	rtFrame.top += 2;
	rtFrame.bottom -= 2;
	rtFrame.left += 1;
	rtFrame.right -= 1;
	FillFrame(&mDC, rtFrame);
	
	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
	mDC.DeleteDC();
	ReleaseDC(pDC);
	return TRUE;	
}

void CESMObjectFrame::FillFrame(CDC* pDC, CRect rect)
{
	TRIVERTEX vertex[2] ;	

	switch(m_nStatus)
	{
		case ESM_OBJECT_FRAME_STATUS_NONE:
		{
			vertex[0].Red   = 0xE100;
			vertex[0].Green = 0xCB00;
			vertex[0].Blue  = 0x9C00;
			vertex[1].Red   = 0xC200;
			vertex[1].Green = 0x9900;
			vertex[1].Blue  = 0x5B00;
			break;
		}		
		case ESM_OBJECT_FRAME_STATUS_SELECT:
		{
			vertex[0].Red   = 0xF300;
			vertex[0].Green = 0xAE00;
			vertex[0].Blue  = 0x2500;
			vertex[1].Red   = 0xDF00;
			vertex[1].Green = 0x7600;
			vertex[1].Blue  = 0x1A00;
			break;
		}	
		case ESM_OBJECT_FRAME_STATUS_SPOT:
		{
			vertex[0].Red   = 0xD500;
			vertex[0].Green = 0x2D00;
			vertex[0].Blue  = 0x2000;
			vertex[1].Red   = 0x9100;
			vertex[1].Green = 0x0400;
			vertex[1].Blue  = 0x1400;
			break;
		}	
	}
	
	vertex[0].x     = rect.left ;
	vertex[0].y     = rect.top  ;
	vertex[0].Alpha = 0x8000;

	vertex[1].x     = rect.right	;
	vertex[1].y     = rect.bottom	; 
	vertex[1].Alpha = 0x0000;

	GRADIENT_RECT gRect;
	gRect.UpperLeft  = 0;
	gRect.LowerRight = 1;

	pDC->Rectangle(rect);
	pDC->GradientFill(vertex, 2, &gRect, 1, GRADIENT_FILL_RECT_H);
}