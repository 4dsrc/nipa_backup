////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectEditor.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-03
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include <vector>

//-- 2014-07-10 hongsu@esmlab.com
#include "ESMEffectView.h"
#include "ESMTimeLineObjectEditor.h"
#include "ESMEffectObjectInfoDlg.h"
#include "ESMEffectSpotInfoDlg.h"


class CESMObjectFrame;
class CESMEffectFrame;

// CESMFrameNailDlg dialog
class CESMEffectEditor: public CDialog
{
	//DECLARE_DYNAMIC(CESMEffectEditor)
public:
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};
	CESMEffectEditor(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMEffectEditor();

	//-- For Init Dialog
	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CESMEffectEditor)
	enum { IDD = IDD_VIEW_EFFECT_EDITOR };
	//}}AFX_DATA

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	CRect m_rcClientFrame;
	//CInnerToolControlBar m_wndToolBar;

public:	
	CESMEffectView*				m_pEffectView;
	CESMEffectObjectInfoDlg		m_dlgObjectInfo;
	CESMEffectSpotInfoDlg		m_dlgSpotInfo;

	//------------------------------------------------------------------------------
	//! @function	Control Spot
	CESMTimeLineObjectEditor*	m_pMovieObject;
	//------------------------------------------------------------------------------	
	void LoadMovieObject(CESMTimeLineObjectEditor*	pObj);
	void RemoveMovieObject();
	CESMTimeLineObjectEditor* GetMovieObject() { return m_pMovieObject;}

	void UpdateFrames();
	void DrawTitle(CDC* pDC);
	void UpdateList(CESMObjectFrame* pObjFrm, BOOL bAdd);

	//------------------------------------------------------------------------------
	//! @function	Control Object Frame
	//------------------------------------------------------------------------------
	CESMObjectFrame* m_pSelectFrame;
	void SelectListItem(CESMObjectFrame* pObjFrm, BOOL bSpot = TRUE);
	void SelectListItem(BOOL bNext = TRUE);
	int GetInsertOrder(CESMObjectFrame* pObjFrm); 	

	//------------------------------------------------------------------------------
	//! @function	Control Frameview
	//------------------------------------------------------------------------------
	void SetFrameView(CESMEffectFrame* pView) { m_dlgSpotInfo.SetFrameView(pView);}
	BOOL m_bPreviewPlay;
	BOOL IsPlay() {return m_bPreviewPlay;}
	int m_nPreviewPlayFrame;
	void PreviewPlay();
	void SaveEffectInfo(CArchive& ar);
	void LoadEffectInfo(CArchive& ar);
		

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();

	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSaveEffectInfo();
	afx_msg void OnUpdateSave(CCmdUI *pCmdUI);	
	DECLARE_MESSAGE_MAP()
};