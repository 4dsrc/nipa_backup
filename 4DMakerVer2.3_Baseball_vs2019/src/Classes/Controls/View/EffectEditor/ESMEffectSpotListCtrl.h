////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectSpotListCtrl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-18
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "SdiDefines.h"
#include "ESMReportCtrl.h"
#include "ESMObjectFrame.h"

class CESMEffectEditor;

class CESMEffectSpotListCtrl : public CESMReportCtrl //CListCtrl
{
public:
	CESMEffectSpotListCtrl(void);
public:
	~CESMEffectSpotListCtrl(void);	
	DECLARE_MESSAGE_MAP()

private:
	//-- Get Parent View
	CESMEffectEditor* m_pEffectEditor;

public:	
	void Init();
	void SetEffectEditor(CESMEffectEditor* pEditor) { m_pEffectEditor = pEditor ;}

	void InsertSpot(int nOrder, CESMObjectFrame* pObjFrm);
	void RemoveSpot(CESMObjectFrame* pObjFrm);
	void SetItemFocus(CESMObjectFrame* pObjFrm);
	void SetItemInfo(int nOrder, CESMObjectFrame* pObjFrm);
	void RedrawItems(CESMObjectFrame* pObjFrm);

private:
	CESMObjectFrame* GetFrame(int nIndex);

public:
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);	
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};
