////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectEditorViewer.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FileOperations.h"
#include "ESMEffectFrameViewer.h"
#include "DSCItem.h"

#include "ESMFunc.h"
#include "SRSIndex.h"
#include "ESMObjectFrame.h"
#include "FFmpegManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CESMEffectFrameViewer, CDialog)

CESMEffectFrameViewer::CESMEffectFrameViewer(CWnd* pParent /*=NULL*/)
	: CDialog(CESMEffectFrameViewer::IDD, pParent)	
{
	m_pObjectFrame = NULL;
}

CESMEffectFrameViewer::~CESMEffectFrameViewer()
{
	
}

void CESMEffectFrameViewer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMEffectFrameViewer)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
}

BOOL CESMEffectFrameViewer::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN || pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
			return FALSE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMEffectFrameViewer, CDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()		
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

BOOL CESMEffectFrameViewer::OnEraseBkgnd(CDC* pDC)
{
	return true;
}

// CFrameNailDlg message handlers
BOOL CESMEffectFrameViewer::OnInitDialog()
{
	CDialog::OnInitDialog();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CESMEffectFrameViewer::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_ZOOMIN			,	
		ID_IMAGE_ZOOMOUT		,
		ID_SEPARATOR			,
		ID_IMAGE_LEFT			,
		ID_IMAGE_RIGHT			,
	};

	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	

	m_wndToolBar.ShowWindow(SW_SHOW);
}

void CESMEffectFrameViewer::OnSize(UINT nType, int cx, int cy) 
{
	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width()		< FRAME_MIN_WIDTH || 
	    m_rcClientFrame.Height()	< FRAME_MIN_HEIGHT)
		return;

	//-- ReloadFrame
	UpdateFrames(TRUE);
	CDialog::OnSize(nType, cx, cy);	
} 

void CESMEffectFrameViewer::OnPaint()
{	
	UpdateFrames();
	CDialog::OnPaint();
}

void CESMEffectFrameViewer::UpdateFrames(BOOL bReloadImage)
{
	//-- 2014-07-17 hongsu@esmlab.com
	//-- Draw Frame
	CPaintDC dc(this);
		
	//-- Rounding Dialog
	CRect rect;	
	rect.CopyRect(m_rcClientFrame);

	int nHeight = rect.Height();
	int nWidth	= rect.Width();
	BOOL bRatio = FALSE; // [TRUE] WIDTH {FALSE] HEIGHT
	int nMargin;
	//-- 2014-07-17 hongsu@esmlab.com
	//-- Draw Backgroud 
	dc.FillSolidRect(rect, COLOR_BLACK);

	CDC memDC;
	CBitmap* pImage	= NULL;
	Bitmap* pFrame	= NULL;
	HBITMAP	hBmp	= NULL;

	//-- Change Image Size
	{
		float nRatio = (float)nWidth/(float)nHeight;
		float nBaseRatio = (float)1920/(float)1080;

		if(nRatio > nBaseRatio)
		{
			nWidth =  (int)((float)nHeight / (float)1080 * (float) 1920);
			bRatio = TRUE;
			nMargin = (rect.Width() - nWidth)/2;
		}
		else
		{
			nHeight = (int)((float)nWidth / (float)1920* (float) 1080 );
			bRatio = FALSE;
			nMargin = (rect.Height() - nHeight)/2;
		}
	}

	pImage = new CBitmap();		
	memDC.CreateCompatibleDC(&dc);
	pImage->CreateCompatibleBitmap(&dc, nWidth, nHeight);

	if(m_pObjectFrame)
	{
		int nPicHeight	= nHeight;
		int nPicWidth	= nWidth;		
		
		//-- 2014-07-17 hongsu@esmlab.com
		//-- Get Frame Image 
		CString strDSC, strFileName;
		int nTime;
		strDSC	= m_pObjectFrame->GetDSC();
		nTime	= m_pObjectFrame->GetTime();
		//-- Get Movie File
		strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), strDSC);
		FFmpegManager ffMpegMgr(FALSE);

		if(!ffMpegMgr.GetCaptureImage(strFileName, pImage, ESMGetFrameIndex(nTime), nWidth, nHeight))
			ESMLog(0,_T("Get Frame [%s][%d]"),strDSC,nTime);		
	}

	Invalidate(FALSE);
	memDC.SelectObject(pImage);
	pImage->DeleteObject();
	if(pImage)
	{
		delete pImage;
		pImage = NULL;
	}		
	
	if(bRatio)  // Width
		dc.BitBlt(nMargin, rect.top, nWidth, nHeight, &memDC, 0, 0, SRCCOPY);
	else		// Height
		dc.BitBlt(0, rect.top + nMargin, nWidth, nHeight, &memDC, 0, 0, SRCCOPY);
	
}

void CESMEffectFrameViewer::DrawFrame(CESMObjectFrame* pObjFrm)
{
	if(m_pObjectFrame == pObjFrm)
		return;

	m_pObjectFrame = pObjFrm;
	UpdateFrames();
}