////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectViewTimeLine.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCViewer.h"
#include "ESMEffectView.h"

#include "ESMEffectEditor.h"
#include "ESMTimeLineObject.h"
#include "ESMObjectFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-07-15
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMEffectView::DrawObjectZoomRatioTimeLine(CDC* pDC)
{
	//-- 2014-07-15 hongsu@esmlab.com
	//-- Set Total Size
	CESMTimeLineObjectEditor* pMovieObject = NULL;
	pMovieObject = GetMovieObject();
	if(pMovieObject)
	{
		//-- Calculate Total Time		
		SetTimeMax(pMovieObject->GetDuration());
	}

	//-- draw Background
	CRect rtTop;
	GetClientRect(&rtTop);
	pDC->FillSolidRect(rtTop, COLOR_BASIC_BG_2 );

	//-- 2013-10-01 hongsu@esmlab.com
	//-- For Drawing Text 
	CRect rtTxt;
	CString strTxt;
	rtTxt.top		= m_ptScroll.y;
	rtTxt.bottom	= m_ptScroll.y+m_nTickMajorHeight;

	SetFrameFont(pDC, 13, _T("Noto Sans CJK KR Regular"));
	pDC->SetTextColor(COLOR_WHITE);
	pDC->SetBkColor(COLOR_BASIC_BG_3);

	// draw the top portion
	rtTop.bottom = rtTop.top + m_nHeaderTop;
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Calculate Scroll size 
	rtTop.left += m_ptScroll.x;
	rtTop.right += m_ptScroll.x;
	rtTop.top += m_ptScroll.y;
	rtTop.bottom += m_ptScroll.y;

	pDC->FillSolidRect(rtTop, COLOR_BASIC_BG_3);	

	CRect rtTick;
	rtTick.top = m_ptScroll.y;
	rtTick.bottom = rtTick.top + m_nHeaderTop;
	rtTick.left = GetXFromTime(0);
	rtTick.right = rtTick.left + GetSpanWidth();

	CPen penMinorTicks;
	CPen penMajorTicks;
	penMinorTicks.CreatePen(PS_SOLID  ,1 , COLOR_BASIC_FG_1 );
	penMajorTicks.CreatePen(PS_SOLID  ,1 , COLOR_WHITE);

	int x;
	int TX;
	//-- 2013-09-06 hongsu@esmlab.com
	//-- mill second 
	pDC->SelectObject(&penMinorTicks);	
	for(x = 0; x < m_nTimeMax; x+=m_nTickFreqMinor)
	{
		TX = GetXFromTime(x);
		pDC->MoveTo(TX,m_ptScroll.y);
		pDC->LineTo(TX,m_ptScroll.y+m_nTickMinorHeight);
	}

	//-- 2013-09-06 hongsu@esmlab.com
	//-- second
	pDC->SelectObject(&penMajorTicks);
	for(x = 0; x<=m_nTimeMax; x+=m_nTickFreqMajor)
	{
		TX = GetXFromTime(x);
		pDC->MoveTo(TX,m_ptScroll.y);
		pDC->LineTo(TX,m_ptScroll.y+m_nTickMajorHeight);

		//-- 2013-10-01 hongsu@esmlab.com
		//-- Draw Second
		strTxt.Format(_T("%d sec"),x/1000);
		rtTxt.left	= TX + DSC_TIMELINE_SEC_GAP;
		rtTxt.right	= TX + GetXFromTime(2000);
		pDC->DrawText(strTxt, rtTxt,  DT_LEFT | DT_BOTTOM | DT_SINGLELINE);
		
	}

	//-- 2014-07-16 hongsu@esmlab.com
	//-- Draw TimeLine Bar
	if(pMovieObject)
		DrawFilmFrame(pDC, pMovieObject);
	else
		DrawFilmBase(pDC);
}

void CESMEffectView::DrawFilmBase(CDC* pDC)
{
	//-- Draw Background
	CRect rt;
	GetClientRect(&rt);
	
	rt.top = DSC_IMAGE_EDIT_TIMELINE_H;
	rt.bottom = rt.top + DSC_HEADER_H;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- Set Max Width 
	int nRight = rt.right;
	pDC->FillRect(rt, &CBrush(RGB(0,0,0)));

	//-- Draw Film
	CRect rtFilm;
	rtFilm.top		= rt.top	+ 2;
	rtFilm.bottom	= rt.bottom	- 2;
	rtFilm.left		= INNER_FILM_RECT_LENGTH / 6 ;//+ m_nLineLeftMargin;
	rtFilm.right	= rtFilm.left + INNER_FILM_RECT_LENGTH;


	TRIVERTEX vertex[2] ;
	vertex[0].x     = rtFilm.left;
	vertex[0].y     = rtFilm.top;	
	vertex[0].Alpha = 0x8000;

	vertex[1].x     = rtFilm.right;
	vertex[1].y     = rtFilm.bottom;
	vertex[1].Alpha = 0x0000;

	GRADIENT_RECT gRect;
	gRect.UpperLeft  = 0;
	gRect.LowerRight = 1;

	//-- Set Color
	vertex[0].Red   = 0xF700;
	vertex[0].Green = 0xF700;
	vertex[0].Blue  = 0xF700;
	vertex[1].Red   = 0xD400;
	vertex[1].Green = 0xD400;
	vertex[1].Blue  = 0xD400;

	while(1)
	{
		pDC->GradientFill(vertex, 2, &gRect, 1, GRADIENT_FILL_RECT_H);	
		rtFilm.left		= rtFilm.right + INNER_FILM_RECT_LENGTH / 6;
		rtFilm.right	= rtFilm.left + INNER_FILM_RECT_LENGTH;

		vertex[0].x     = rtFilm.left;		
		vertex[1].x     = rtFilm.right;


		if(rtFilm.right > nRight)
		{
			rtFilm.right	= nRight - INNER_FILM_RECT_LENGTH / 6;
			vertex[0].x     = rtFilm.left;
			vertex[0].y     = rtFilm.top;
			vertex[1].x     = rtFilm.right;
			vertex[1].y     = rtFilm.bottom;

			if(rtFilm.right > rtFilm.left)
				pDC->GradientFill(vertex, 2, &gRect, 1, GRADIENT_FILL_RECT_H);
			break;
		}
	}
}

void CESMEffectView::DrawFilmFrame(CDC* pDC, CESMTimeLineObjectEditor* pObject)
{
	int nAll;
	CESMObjectFrame* pObjFrm = NULL;
	//-- Draw Background
	CRect rt;
	GetClientRect(&rt);

	rt.top = DSC_IMAGE_EDIT_TIMELINE_H;
	rt.bottom = rt.top + DSC_HEADER_H;

	//-- Create Object Frames
	nAll = m_pEffectEditor->m_pMovieObject->GetObjectFrameCount();
	if(!nAll)
		return;
		
	//-- Draw Frames	
	float nWidth = (float)rt.Width() / (float)nAll;
	float nPos = 0;
	for(int n = 0 ; n < nAll ; n ++)	
	{
		rt.left		= (LONG)(nWidth*n);
		rt.right	= (LONG)(nWidth*(n+1));

		pObjFrm = m_pEffectEditor->m_pMovieObject->GetObjectFrame(n);
		if(!pObjFrm)
			break;
		pObjFrm->MoveWindow(rt);		
		pObjFrm->DrawFrame();		
	}
}

void CESMEffectView::LoadMovieObject()
{
	CESMTimeLineObjectEditor* pObject = m_pEffectEditor->m_pMovieObject;

	if(!pObject)
		return;

	int nAll, nTimeCnt, nFrmCnt		= 0;
	int nDSCCnt		= pObject->GetDSCCount();
	int nStartTime	= pObject->GetStartTime();			//-- Start DSC : Time
	int nEndTime	= pObject->GetEndTime();			//-- End DSC : Time
	int nFpsTime	= pObject->GetNextTime();
	BOOL bOrder;

	//-- Remove Object Frame
	//-- Check Previous Addition Objects
	nAll = pObject->GetObjectFrameCount();
	if(nAll)
	{
		pObject->ShowFrame(TRUE);		
		
		LoadSpots(); 
		return;
	}

	pObject->SetObjectFrame();
	pObject->DrawObjectFrame(this);

	//-- 2014-07-18 hongsu@esmlab.com
	//-- Load Exist Spots
	LoadSpots();
}

