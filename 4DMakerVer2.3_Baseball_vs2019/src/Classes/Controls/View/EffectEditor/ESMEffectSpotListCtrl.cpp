////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectSpotListCtrl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-18
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMEffectSpotListCtrl.h"
#include "ESMEffectEditor.h"
#include "ESMIndex.h"
#include "ESMUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CESMEffectSpotListCtrl::CESMEffectSpotListCtrl(void)
{
	m_pEffectEditor = NULL;
}

CESMEffectSpotListCtrl::~CESMEffectSpotListCtrl(void)
{
	m_pEffectEditor = NULL;
}

BEGIN_MESSAGE_MAP(CESMEffectSpotListCtrl, CListCtrl)
	ON_WM_MEASUREITEM()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()	
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-08
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMEffectSpotListCtrl::Init()
{
	SetExtendedStyle(GetExtendedStyle() | LVS_EX_FULLROWSELECT);
	// Initialize list control with some data.
	InsertColumn(0, _T("DSC")	, LVCFMT_CENTER, 40);
	InsertColumn(1, _T("Time")	, LVCFMT_CENTER, 40);
	InsertColumn(2, _T("Z/R")	, LVCFMT_CENTER, 38);	
	InsertColumn(3, _T("Z/W")	, LVCFMT_CENTER, 38);
	InsertColumn(4, _T("B/R")	, LVCFMT_CENTER, 38);	
	InsertColumn(5, _T("B/W")	, LVCFMT_CENTER, 38);
}

void CESMEffectSpotListCtrl::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	CESMReportCtrl::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CESMEffectSpotListCtrl::OnDestroy()
{
	CESMReportCtrl::OnDestroy();
}

void CESMEffectSpotListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	if (lplvcd->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if (lplvcd->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	{
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
	}
	else if (lplvcd->nmcd.dwDrawStage == (CDDS_ITEMPREPAINT | CDDS_SUBITEM))
	{
		CItemResultData* p = (CItemResultData*)(CListCtrl::GetItemData(lplvcd->nmcd.dwItemSpec));
		ASSERT(p != NULL);
		ASSERT(lplvcd->iSubItem >= 0 && lplvcd->iSubItem < p->aTextColors.GetSize());
		lplvcd->clrText = p->aTextColors[lplvcd->iSubItem];
		lplvcd->clrTextBk = p->aBkColors[lplvcd->iSubItem];
		*pResult = CDRF_DODEFAULT;
	}
}

//------------------------------------------------------------------------------
//! @function	Event (LButtonDown)
//! @brief				
//! @date		2014-07-21
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMEffectSpotListCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem;  
	CESMReportCtrl::OnLButtonDown(nFlags, point);

	UINT uFlags;  
	if((nItem = HitTest(point, &uFlags)) != -1)  
	{  
		if (uFlags & LVHT_ONITEMLABEL)
		{			
			CESMObjectFrame* pObjFrm = GetFrame(nItem);
			if(pObjFrm)
				m_pEffectEditor->SelectListItem(pObjFrm);
		}
	}  	
}

void CESMEffectSpotListCtrl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	switch (nChar)
	{
	case ESM_KEY_RIGHT:	//-- Select Next Item
		m_pEffectEditor->SelectListItem();
		break;
	case ESM_KEY_LEFT:	//-- Select Previous Item
		m_pEffectEditor->SelectListItem(FALSE);
		break;
	case ESM_KEY_ENTER:	//-- Play or Stop Preview
		m_pEffectEditor->PreviewPlay();
		break;
	}
	CESMReportCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-14
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMEffectSpotListCtrl::InsertSpot(int nOrder, CESMObjectFrame* pObjFrm)
{	
	LVITEM item = {0};
	item.mask = LVIF_TEXT | LVIF_PARAM;
	item.iItem		= nOrder;
	item.pszText = (LPWSTR)(LPCWSTR)pObjFrm->GetDSC();
	item.lParam	= (LPARAM)pObjFrm;

	//-- DSC 
	InsertItem(&item);
	//-- SetItemInfo
	SetItemInfo(nOrder, pObjFrm);
	//-- 2014-07-21 hongsu@esmlab.com
	//-- Set Item Focus 
	SetItemFocus(pObjFrm);
	//-- 2014-07-18 hongsu@esmlab.com
	//-- Check List UP
	pObjFrm->SetSpot(TRUE);
}

void CESMEffectSpotListCtrl::SetItemInfo(int nOrder, CESMObjectFrame* pObjFrm)
{
	CString strItem;
	BOOL	bItem;

	//-- Time
	float fTime = (float)pObjFrm->GetTime();
	strItem.Format(_T("%.3f"),fTime/1000);
	SetItemText(nOrder, 1, strItem);

	//-- Zoom On/Off
	bItem = pObjFrm->IsZoom();
	if(bItem)
	{
		strItem.Format(_T("%d"),pObjFrm->GetZoomRatio());
		SetItemText(nOrder, 2, strItem);
		strItem.Format(_T("%.1f"),pObjFrm->GetZoomWeight());
		SetItemText(nOrder, 3, strItem);
	}
	else
	{
		SetItemText(nOrder, 2, _T("-"));
		SetItemText(nOrder, 3, _T("-"));
	}

	//-- Zoom On/Off
	bItem = pObjFrm->IsBlur();
	if(bItem)
	{
		strItem.Format(_T("%d"),pObjFrm->GetBlurSize());
		SetItemText(nOrder, 4, strItem);
		strItem.Format(_T("%d"),pObjFrm->GetBlurWeight());
		SetItemText(nOrder, 5, strItem);
	}
	else
	{
		SetItemText(nOrder, 4, _T("-"));
		SetItemText(nOrder, 5, _T("-"));
	}
}

void CESMEffectSpotListCtrl::RemoveSpot(CESMObjectFrame* pObjFrm)
{
	if(!m_pEffectEditor)
		return;

	int nOrder = m_pEffectEditor->m_pMovieObject->GetObjectFrameOrder(pObjFrm);
	//-- Non Delete 1st
	if(nOrder == 0)
		return;	
	//-- Non Delete Last
	int nAll = m_pEffectEditor->m_pMovieObject->GetObjectFrameCount();
	if(nAll-1 == nOrder)
		return;

	nOrder = m_pEffectEditor->m_pMovieObject->GetInsertOrder(pObjFrm);
	DeleteItem(nOrder);
	//-- 2014-07-18 hongsu@esmlab.com
	//-- Check List UP
	pObjFrm->SetSpot(FALSE);	
}

CESMObjectFrame* CESMEffectSpotListCtrl::GetFrame(int nIndex)
{
	CESMObjectFrame* pObjFrm = NULL;
	LVITEM item;
	item.mask =  LVIF_PARAM;
	item.iItem = nIndex;
	GetItem(&item);			

	pObjFrm = (CESMObjectFrame*)item.lParam;
	return pObjFrm;
}

void CESMEffectSpotListCtrl::SetItemFocus(CESMObjectFrame* pObjFrm)
{
	SetFocus();

	CESMObjectFrame* pExistObjFrm = NULL;
	int n = 0;
	int nAll = GetItemCount();
		
	for(int i = 0 ; i < nAll ; i++)
	{
		pExistObjFrm = GetFrame(i);
		if(pObjFrm == pExistObjFrm)
		{
			EnsureVisible(i,TRUE);
			SetItemStates(i, LVIS_FOCUSED | LVIS_SELECTED);				
		}
		else
		{
			//EnsureVisible(i,FALSE);
			SetItemState(i, 0, LVIS_SELECTED);
			SetItemState(i, 0, LVIS_FOCUSED);
		}
	}

	Invalidate(FALSE);
}

void CESMEffectSpotListCtrl::RedrawItems(CESMObjectFrame* pObjFrm)
{
	CESMObjectFrame* pExistObjFrm = NULL;
	int n = 0;
	int nAll = GetItemCount();

	for(int i = 0 ; i < nAll ; i++)
	{
		pExistObjFrm = GetFrame(i);
		if(pObjFrm == pExistObjFrm)
		{
			SetItemInfo(i, pObjFrm);
		}		
	}
	Invalidate(FALSE);
}