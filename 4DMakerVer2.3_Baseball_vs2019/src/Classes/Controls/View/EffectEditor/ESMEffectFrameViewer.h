////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectFrameViewer.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-10
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include <vector>
#include "ToolTipListCtrl.h"
//-- 2011-07-27 hongsu.jung
#include "ESMFrameList.h"

class CDSCItem;
class CESMObjectFrame;
// CESMFrameNailDlg dialog
class CESMEffectFrameViewer : public CDialog
{
	//-- For Toolbar
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	DECLARE_DYNAMIC(CESMEffectFrameViewer)
public:
	CESMEffectFrameViewer(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMEffectFrameViewer();

	//-- For Toolbar
	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CESMEffectFrameViewer)
	enum { IDD = IDD_VIEW_ONLY_TOOLBAR };
	//}}AFX_DATA

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
	CESMObjectFrame* m_pObjectFrame;

private:
	CRect m_rcClientFrame;	
	CInnerToolControlBar m_wndToolBar;	


public:
	void DrawFrame(CESMObjectFrame* pObjFrm);
	void UpdateFrames(BOOL bReloadImage = TRUE);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg void OnTimer(UINT_PTR nIDEvent);	
	DECLARE_MESSAGE_MAP()		

};