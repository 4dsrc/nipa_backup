////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCViewer.h"
#include "ESMEffectEditor.h"
#include "ESMEffectView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//IMPLEMENT_DYNCREATE(CESMEffectView, CTimeLineViewBase)
BEGIN_MESSAGE_MAP(CESMEffectView, CTimeLineViewBase)
	//{{AFX_MSG_MAP(CTimeLineView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()	
	//}}AFX_MSG_MAP
	ON_WM_DESTROY()
	ON_WM_KEYUP()
END_MESSAGE_MAP()


// CESMEffectView
CESMEffectView::CESMEffectView(CWnd* pParentWnd):CTimeLineViewBase()
{
	m_pEffectEditor = (CESMEffectEditor*)pParentWnd;
	InitializeCriticalSection (&m_CR_EffectView);
}

CESMEffectView::~CESMEffectView()
{
	m_pEffectEditor = NULL;
	DeleteCriticalSection (&m_CR_EffectView);	
}

BOOL CESMEffectView::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN || pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
			return FALSE;				
	}

	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN)
		{
			BOOL bShift = ((GetKeyState(VK_SHIFT) & 0x8000) != 0);
			BOOL bControl = ((GetKeyState(VK_CONTROL) & 0x8000) != 0);
			BOOL bAlt = ((GetKeyState(VK_MENU) & 0x8000) != 0);

			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			if(bControl && !bShift && !bAlt)
			{
				ESMEvent* pEsmMsg = NULL;
				pEsmMsg = new ESMEvent();

				pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
				pEsmMsg->nParam1 = pMsg->wParam;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
			}

		}
	}

	return CWnd::PreTranslateMessage(pMsg);
}
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMEffectView::OnDraw(CDC* pDC)
{
	DrawObjectZoomRatio();
}

CESMTimeLineObjectEditor* CESMEffectView::GetMovieObject()
{
	if(!m_pEffectEditor)
		return NULL;
	return m_pEffectEditor->GetMovieObject();
}

void CESMEffectView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	switch (nChar)
	{
	case ESM_KEY_DEL:
		break;
	}
	__super::OnKeyUp(nChar, nRepCnt, nFlags);
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMEffectView::DrawObjectZoomRatio()
{	
	CDC* pDC = GetDC();
	if(!pDC) 
		return;

	// update the scroll bars
	UpdateSizes();

	//-- 2014-07-15 hongsu@esmlab.com
	//-- TimeLine Base
	DrawObjectZoomRatioTimeLine(pDC);
	
	//-- 2014-07-15 hongsu@esmlab.com
	//-- Graph
	DrawObjectZoomRatioGraph(pDC);	

	ReleaseDC(pDC);	
}

void CESMEffectView::LoadSpots()
{
	int nAll = m_pEffectEditor->m_pMovieObject->GetObjectFrameCount();

	//-- VMCC 林籍贸府
	/*if(nAll < 2)
	{
		ESMLog(0,_T("Non Exist Enough Frames [%d]"),nAll);
		return;
	}*/

	int nSpot = m_pEffectEditor->m_pMovieObject->GetSpotCount();

	//-- VMCC 林籍贸府
	/*if(nSpot < 2)
	{
		CreateBaseSpots();
	}
	//-- 2014-07-21 hongsu@esmlab.com
	//-- Find Spots
	else*/
	{
		CESMObjectFrame* pObjFrm = NULL;
		int nSpotIndex = -1;	
		while(1)
		{
			//-- Find Spot Object
			nSpotIndex = m_pEffectEditor->m_pMovieObject->FindNextSpotObject(nSpotIndex);		
			if(nSpotIndex == -1)
				break;

			//-- Set Ratio
			pObjFrm = m_pEffectEditor->m_pMovieObject->GetObjectFrame(nSpotIndex);
			if(pObjFrm)
			{
				pObjFrm->m_nStatus = ESM_OBJECT_FRAME_STATUS_SPOT;
				m_pEffectEditor->UpdateList(pObjFrm, TRUE);				
				pObjFrm->DrawFrame();
			}		
		}	
	}	
}

void CESMEffectView::CreateBaseSpots()
{
	int nAll = m_pEffectEditor->m_pMovieObject->GetObjectFrameCount();
	if(nAll < 3)
		return;

	CESMObjectFrame* pObjFrm = NULL;
	ESMEvent* pMsg	= NULL;
	//-- 2014-07-18 hongsu@esmlab.com
	//-- Create 1st
	pObjFrm = m_pEffectEditor->m_pMovieObject->GetObjectFrame(0);
	if(pObjFrm)
	{
		pObjFrm->SetSpot(TRUE);
		pObjFrm->m_nStatus = ESM_OBJECT_FRAME_STATUS_SPOT;
		m_pEffectEditor->UpdateList(pObjFrm, TRUE);
		pObjFrm->DrawFrame();
	}

	//-- Create Last
	pObjFrm = m_pEffectEditor->m_pMovieObject->GetObjectFrame(nAll-1);
	if(pObjFrm)
	{
		pObjFrm->SetSpot(TRUE);
		pObjFrm->m_nStatus = ESM_OBJECT_FRAME_STATUS_SPOT;
		m_pEffectEditor->UpdateList(pObjFrm, TRUE);
		pObjFrm->DrawFrame();
	}	
}