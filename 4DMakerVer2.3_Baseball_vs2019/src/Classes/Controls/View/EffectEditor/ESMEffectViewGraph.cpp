////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectViewGraph.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCViewer.h"
#include "ESMEffectView.h"

#include "ESMEffectEditor.h"
#include "ESMTimeLineObject.h"
#include "ESMObjectFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CESMEffectView::DrawObjectZoomRatioGraph(CDC* pDC)
{
	//-- Draw Background
	CRect rtLine;
	GetClientRect(&rtLine);

	rtLine.top = DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H;
	pDC->FillSolidRect(rtLine, COLOR_BASIC_BG_3 );

	//-- 2014-07-16 hongsu@esmlab.com
	//-- Draw Line
	int nHeight;
	for(int n = 0; n < 3; n++)
	{
		nHeight = DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + (SPOT_GRAPH_H / 6 * ((n*2)+1)) ;
		pDC->MoveTo(rtLine.left  + SPOT_BASE_MARGIN, nHeight);
		pDC->LineTo(rtLine.right - SPOT_BASE_MARGIN, nHeight);
	}

	//-- 2014-07-16 hongsu@esmlab.com
	//-- Get Object 
	CESMTimeLineObjectEditor* pMovieObject = NULL;
	pMovieObject = GetMovieObject();
	if(!pMovieObject)
		return;

	CPen pen;
	pen.CreatePen(PS_DOT,  3, COLOR_RED );	
	pDC->SelectObject(&pen);

	CESMObjectFrame* pPrevFrame = NULL;
	CESMObjectFrame* pNextFrame = NULL;
	CRect rtPrev, rtNext;
	float nHeightPrev, nHeightNext;
	int nAll = m_pEffectEditor->m_pMovieObject->GetObjectFrameCount();
	if(nAll < 2)
		return;


	pPrevFrame = m_pEffectEditor->m_pMovieObject->GetObjectFrame(0);
	pNextFrame = m_pEffectEditor->m_pMovieObject->GetObjectFrame(1);
	pPrevFrame->GetWindowRect(rtPrev);
	pNextFrame->GetWindowRect(rtNext);

	int nMargin = rtPrev.left;

	
	
	for(int i = 0 ; i < nAll - 1 ; i ++)
	{
		pPrevFrame = m_pEffectEditor->m_pMovieObject->GetObjectFrame(i);
		pNextFrame = m_pEffectEditor->m_pMovieObject->GetObjectFrame(i+1);
		if(!pPrevFrame || !pNextFrame)
			continue;

		pPrevFrame->GetWindowRect(rtPrev);
		pNextFrame->GetWindowRect(rtNext);

		// Base DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + SPOT_GRAPH_H / 6
		// SPOT_GRAPH_H / 6 * 1 ==> 100;
		// SPOT_GRAPH_H / 6 * 3 ==> 200;
		// SPOT_GRAPH_H / 6 * 5 ==> 300;
		nHeightPrev = DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + SPOT_GRAPH_H * 5 / 6 - (SPOT_GRAPH_H / 3 * (float)((float)((float)pPrevFrame->GetZoomRatio() - 100)/100));
		nHeightNext = DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + SPOT_GRAPH_H * 5 / 6 - (SPOT_GRAPH_H / 3 * (float)((float)((float)pNextFrame->GetZoomRatio() - 100)/100));

		pDC->MoveTo(rtPrev.left + rtPrev.Width() - SPOT_GRAPH_L - nMargin + 65,	(int)nHeightPrev);
		pDC->LineTo(rtNext.left + rtNext.Width() - SPOT_GRAPH_L - nMargin + 65,	(int)nHeightNext);
	}

	//-- 2014-07-22 hongsu@esmlab.com
	//-- Draw Preview TimeLine 
	/*
	if(m_pEffectEditor->m_bPreviewPlay)
	{
		CPen pen;
		pen.CreatePen(PS_DOT,  3, COLOR_GREEN );	
		pDC->SelectObject(&pen);

		int nPreviewFrame = m_pEffectEditor->m_nPreviewPlayFrame;
		pPrevFrame = m_pEffectEditor->m_pMovieObject->GetObjectFrame(nPreviewFrame);
		pPrevFrame->GetWindowRect(rtPrev);
		int nPos = rtPrev.left + rtPrev.Width()/2 - SPOT_GRAPH_L;
		pDC->MoveTo(nPos, DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H );
		pDC->LineTo(nPos, DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + SPOT_GRAPH_H );
	}
	*/
}

