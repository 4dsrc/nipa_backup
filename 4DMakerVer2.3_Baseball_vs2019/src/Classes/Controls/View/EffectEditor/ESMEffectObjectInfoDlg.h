////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectObjectInfoDlg.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-15
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "resource.h"
#include "afxwin.h"


class CESMEffectEditor;
/////////////////////////////////////////////////////////////////////////////
// CESMEffectObjectInfoDlg dialog

class CESMEffectObjectInfoDlg : public CDialog
{
	// Construction
public:
	CESMEffectObjectInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMEffectObjectInfoDlg();

	// Dialog Data
	//{{AFX_DATA(CESMEffectObjectInfoDlg)
	enum { IDD = IDD_EFFECT_OBJECTINFO };
	//}}AFX_DATA

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	HWND m_hMainWnd;	
	CBrush m_brush;
	//-- Get Parent View
	CESMEffectEditor* m_pEffectEditor;

public:
	int m_nTimeStart;
	int m_nTimeEnd;
	int m_nFps;
	CString m_strDSCStart;
	CString m_strDSCEnd;

public:	
	void UpdateInfo();
	void SetEffectEditor(CESMEffectEditor* pEditor) { m_pEffectEditor = pEditor ; }
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMEffectObjectInfoDlg)
protected:
	afx_msg void OnClose();	
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	DECLARE_MESSAGE_MAP()
	
	
protected:
	// Generated message map functions
	//{{AFX_MSG(CESMEffectObjectInfoDlg)	
	afx_msg void OnPaint();
	//}}AFX_MSG	
	afx_msg void OnSize(UINT nType, int cx, int cy);		
public:
	afx_msg void OnBnClickedObjInfoSave();
	afx_msg void OnBnClickedObjInfoLoad();
};
