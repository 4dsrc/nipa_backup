////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectObjectInfoDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-15
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMEffectObjectInfoDlg.h"
#include "resource.h"
#include "MainFrm.h"
#include "ESMEffectEditor.h"

/////////////////////////////////////////////////////////////////////////////
// CESMEffectObjectInfoDlg dialog

CESMEffectObjectInfoDlg::CESMEffectObjectInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMEffectObjectInfoDlg::IDD, pParent)	
	, m_nTimeStart(0)
	, m_nTimeEnd(0)
	, m_nFps(30)
	, m_strDSCStart(_T("-"))
	, m_strDSCEnd(_T("-"))
{		
	m_pEffectEditor = (CESMEffectEditor*)pParent;	
}

CESMEffectObjectInfoDlg::~CESMEffectObjectInfoDlg() 
{
	m_pEffectEditor = NULL;
}

void CESMEffectObjectInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMEffectObjectInfoDlg)
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_OBJ_INFO_TIME_START, m_nTimeStart);
	DDX_Text(pDX, IDC_OBJ_INFO_TIME_END, m_nTimeEnd);
	DDX_Text(pDX, IDC_OBJ_INFO_TIME_FPS, m_nFps);
	DDX_Text(pDX, IDC_OBJ_INFO_DSC_START, m_strDSCStart);
	DDX_Text(pDX, IDC_OBJ_INFO_DSC_END, m_strDSCEnd);
}

BOOL CESMEffectObjectInfoDlg::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN || pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
			return FALSE;
	}

	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN)
		{
			BOOL bShift = ((GetKeyState(VK_SHIFT) & 0x8000) != 0);
			BOOL bControl = ((GetKeyState(VK_CONTROL) & 0x8000) != 0);
			BOOL bAlt = ((GetKeyState(VK_MENU) & 0x8000) != 0);

			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			if(bControl && !bShift && !bAlt)
			{
				ESMEvent* pEsmMsg = NULL;
				pEsmMsg = new ESMEvent();

				pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
				pEsmMsg->nParam1 = pMsg->wParam;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
			}

		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMEffectObjectInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CESMEffectObjectInfoDlg)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()	
	ON_WM_PAINT()
	ON_WM_CLOSE()	
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_OBJ_INFO_SAVE, &CESMEffectObjectInfoDlg::OnBnClickedObjInfoSave)
	ON_BN_CLICKED(IDC_OBJ_INFO_LOAD, &CESMEffectObjectInfoDlg::OnBnClickedObjInfoLoad)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CESMEffectObjectInfoDlg message handlers
BOOL CESMEffectObjectInfoDlg::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}	

	//-- SET MAIN WINDOWS HANDLE
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();	
	m_brush.CreateSolidBrush(COLOR_BASIC_FG_1); 
	return TRUE;
}

BOOL CESMEffectObjectInfoDlg::OnEraseBkgnd(CDC* pDC)
{ 
	CRect r;
	GetClientRect(&r);
	pDC->FillRect(&r, &m_brush);
	return TRUE;
}

HBRUSH CESMEffectObjectInfoDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	switch(nCtlColor)
	{
	case CTLCOLOR_EDIT	:	pDC->SetBkColor(COLOR_WHITE)			;
							pDC->SetTextColor(COLOR_BASIC_BG_DARK)	; break;
	default:				pDC->SetBkColor(COLOR_BASIC_FG_1)		;
							pDC->SetTextColor(COLOR_BASIC_BG_DARK)	; break;
	}	
	return (HBRUSH) m_brush;
}

/////////////////////////////////////////////////////////////////////////////
// CESMEffectObjectInfoDlg

void CESMEffectObjectInfoDlg::OnPaint()
{	
	CPaintDC dc(this);
	CDialog::OnPaint();
}

void CESMEffectObjectInfoDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
}

void CESMEffectObjectInfoDlg::OnClose()
{
	CDialog::OnClose();
}

void CESMEffectObjectInfoDlg::UpdateInfo()
{
	//-- 2014-07-16 hongsu@esmlab.com
	//-- Check Object
	if(!m_pEffectEditor)
		return;

	CESMTimeLineObjectEditor* pMovieObject = NULL;
	pMovieObject = m_pEffectEditor->GetMovieObject();
	if(!pMovieObject)
		return;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- Set Information 
	m_nTimeStart	= pMovieObject->GetStartTime();
	m_nTimeEnd		= pMovieObject->GetEndTime();
	m_nFps			= pMovieObject->GetNextTime();

	//-- Get DSC Info	
	int nDSC		= pMovieObject->GetDSCCount();	
	if(nDSC > 0)
	{			
		CDSCItem* pItem = NULL;
		pItem = pMovieObject->GetDSC(0);
		if(pItem)	
			m_strDSCStart = pItem->GetDeviceDSCID();
		pItem = pMovieObject->GetDSC(nDSC-1);
		if(pItem)	
			m_strDSCEnd = pItem->GetDeviceDSCID();
	}

	//-- UPDATE
	UpdateData(FALSE);
}

void CESMEffectObjectInfoDlg::OnBnClickedObjInfoSave()
{
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_EFFECT));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_EFFECT));

	CString szFilter = _T("Effect Info File (*.vmcc)|*.vmcc|");
	CString strTitle = _T("Effect Info Files Save");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;	

	//-- 2013-10-02 hongsu@esmlab.com
	//-- Default Save Name
	CTime time = CTime::GetCurrentTime();
	CString strFile;
	strFile.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());

	int length = 1024;
	LPWSTR pwsz = strFile.GetBuffer(length);
	dlg.m_ofn.lpstrFile = pwsz;
	strFile.ReleaseBuffer();

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		CString strFilePath;
		strFilePath.Format(_T("%s%s"), strFileName, _T(".vmcc"));
		//-- 2013-09-04 hongsu@esmlab.com

		CFile file;
		CArchive ar(&file, CArchive::store);
		if(!file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
			return;

		m_pEffectEditor->SaveEffectInfo(ar);

		ar.Close();
		file.Close();
	}
}

void CESMEffectObjectInfoDlg::OnBnClickedObjInfoLoad()
{
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_EFFECT));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_EFFECT));

	CString szFilter = _T("Effect Info File (*.vmcc)|*.vmcc|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();

		CFile file;

		if(!file.Open(strFileName, CFile::modeRead))
		{
			ESMLog(0,_T("Load Effect Info File [%s]") , strFileName);
			return;
		}

		CArchive ar(&file, CArchive::load);
		m_pEffectEditor->LoadEffectInfo(ar);
		
		ar.Close();
		file.Close();
	}
}
