////////////////////////////////////////////////////////////////////////////////
//
//	ESMFrameViewer.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-24
// @ver.1.0	4DMaker
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include <vector>
#include "ToolTipListCtrl.h"
//-- 2011-07-27 hongsu.jung
#include "ESMFrameList.h"
#include "ESMIndex.h"
#include "ESMFrameAutoDetectDlg.h"

class CDSCItem;
// CESMFrameNailDlg dialog
class CESMFrameViewer : public CDialog
{
	//-- For Toolbar
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	DECLARE_DYNAMIC(CESMFrameViewer)
public:
	CESMFrameViewer(UINT nIDTemplate, CWnd* pParent = NULL);
	CESMFrameViewer(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMFrameViewer();

	//-- For Toolbar
	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CESMFrameViewer)
	enum { IDD = IDD_VIEW_ONLY_TOOLBAR };
	//}}AFX_DATA

protected:
	//seo
	virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
	CESMFrameList* m_pDlgFrameList;

private:
	CRect m_rcClientFrame;
	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;	

public:	
	void UpdateFrames(BOOL bReloadImage = FALSE);	
	void SetSelectedTime(int nTime, int nLine = 0)	{ if(m_pDlgFrameList) m_pDlgFrameList->SetSelectedTime(nTime, nLine); }
	int GetSelectedTime(int nLine = 0)			{ if(m_pDlgFrameList) return m_pDlgFrameList->GetSelectedTime(nLine); return 0; }

	void SetSelectedDSC(CString str){ if(m_pDlgFrameList) m_pDlgFrameList->SetSelectedDSC(str); }
	CString GetSelectedDSC()		{ if(m_pDlgFrameList) return m_pDlgFrameList->GetSelectedDSC(); return _T(""); }
	void SetSelectedIP(CString str){ if(m_pDlgFrameList) m_pDlgFrameList->SetSelectedIP(str); }
	CString GetSelectedIP()		{ if(m_pDlgFrameList) return m_pDlgFrameList->GetSelectedIP(); return _T(""); }


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg void OnZoomIn();
	afx_msg void OnZoomOut();
	afx_msg BOOL OnMouseWheel( UINT nFlags, short zDelta, CPoint pt );

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnReload(WPARAM , LPARAM );
	DECLARE_MESSAGE_MAP()		
	afx_msg void OnFrameSel();
	afx_msg void OnBnSet();
	afx_msg void OnBnSetSize();
	//170529 hjcho
	afx_msg void OnATSet();


	//170522 wgkim@esmlab.com
	afx_msg void OnBnSetZoom150();
	afx_msg void OnBnSetZoom200();
	afx_msg void OnBnSetZoom300();
	afx_msg void OnBnSetZoom400();

public:
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	afx_msg void OnMouseHWheel(UINT nFlags, short zDelta, CPoint pt);
	track_info cam_data;

	//hjcho declared
	CRect AT_GetScaleBtnRect();
	void OnDrawButton(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

	//170529 hjcho AutoDetecting
	CESMFrameViewBalltracking* m_pATView;

	//190402 hjcho
	void SaveCurShowImage(CString str)
	{
		m_pDlgFrameList->SaveCurrentImage(str);
	}
};
