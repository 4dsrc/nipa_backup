////////////////////////////////////////////////////////////////////////////////
//
//	ESMFrameList.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-24
// @ver.1.0	4DMaker
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FileOperations.h"
#include "ESMFrameList.h"
#include "ESMFunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CESMFrameList dialog
IMPLEMENT_DYNAMIC(CESMFrameList, CRSChildDialog)

	CESMFrameList::CESMFrameList(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CESMFrameList::IDD, pParent)
{
	//-- 2011-05-18 hongsu.jung
	//-- Load From Config
	m_strOpenedFile		= _T("");
	m_nTooltipPicture	= -1;
	m_nDrawedPicture	= -1;
	m_nFirstPic			= 0;
	
	m_rtScrollBase		= CRect(0,0,0,0);
	m_rtScrollBar		= CRect(0,0,0,0);

	m_szFrameImage.cx = 192;
	m_szFrameImage.cy = 120;

	m_nFrame_margin	= 12;
	m_nFrame_margin_y	= 15;
	
	m_nFrame_margin_start	= 3+15;
	m_nFrame_picture_size	= 88;
	m_nFrame_picture_gap	= 6;

	m_nImageCount		= 0;
	m_pImageList		= NULL;

	m_pFont				= NULL;
	m_ThreadHandle		= NULL;

	m_strDSCID			= _T("");
	for(int i = 0; i < MAX_SELECT_LINE; i++)
		m_nSelectedTime[i]		= -1;
	m_nSelectedFrame	= -1;	

	//  2013-10-23 Ryumin
	m_nCurWidth			= 0;
	m_nCurHeight		= 0;
	m_nHScrollPos		= 0;
	m_nVScrollPos		= 0;

	m_nZoom				= 100;
	m_nPosX				= 0 ;
	m_nPosY				= 0 ;
	m_nSrcPosX			= 0;
	m_nSrcPosY			= 0;
	m_nPrevX			= 0;
	m_nPrevY			= 0;
	m_ReLoadImage		= TRUE;
	m_bThreadRun		= FALSE;

	m_bSetBallSize		= FALSE;
	selected_frame		= 0;
	Auto_num			= 0;

	nSelect				= 0;

	nSkipCount			= 0;
	nSkipIndex			= 0;
	nSrcHeight			= 0;
	nSrcWidth			= 0;

	for(int i = 0 ; i < 4; i++)
	{
		m_nArrMov_x[i] = 0;
		m_nArrMov_y[i] = 0;
		m_nArrRatioCol[i] = 8;
		m_nArrRatioRow[i] = 6;
	}

	m_nDetectingTime = 0;
	//wgkim@esmlab.com
	flagMousePointOnFrameViewer = FALSE;
	m_pTemplateMgr = NULL;
	m_dZoomValue = 200;
	m_nCircleSize = 0;
	m_bIsPressedRButton = FALSE;

	//17-03-10 hjcho
	InitializeCriticalSection(&m_cr);
	InitializeCriticalSection(&m_crFrameLoad);

	//17-07-27 hjcho
	m_nFrameLoadCnt = 0;
	for(int i =0 ; i < 2; i++)
	{
		m_bThreadLoad[i] = FALSE;
		m_bThread[i]	 = FALSE;
	}
	m_strPreExpandFile = _T("");
	
	m_bIsFrameHalf = FALSE;
	m_nExpandTime  = -1;
	m_nDivTime	= 1000;
	m_nSkipIdx = 30;
	//m_nCurMovieIdx = -1;
	m_nArrMovieIdx[0] = -1;
	m_nArrMovieIdx[1] = -1;

	//wgkim
	m_bVertical = 0;

	//wgkim 180515
	m_dGop			= 20.f;
	m_dFrameRate	= 60.f;
	m_dDurationTime = 1.f;
	m_bCheck4DPOrNot= FALSE;
	}

CESMFrameList::~CESMFrameList()
{
	if(m_pFont)
	{
		delete m_pFont;
		m_pFont = NULL;
	}

	RemoveImageList();

	WaitForSingleObject(m_ThreadHandle, INFINITE);
	CloseHandle(m_ThreadHandle);

	DeleteCriticalSection(&m_cr);
	DeleteCriticalSection(&m_crFrameLoad);
}

void CESMFrameList::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);
}

//------------------------------------------------------------------------------ 
//! @brief		OnInitDialog()
//! @date		2010-08-01
//! @author	hongsu.jung
// CESMFrameList message handlers
//------------------------------------------------------------------------------ 
BOOL CESMFrameList::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();
	//-- Call OnReload
	PostMessage(WM_LOAD_FRAME);

	//  2013-10-23 Ryumin
	GetWindowRect(&m_rcRect);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

//------------------------------------------------------------------------------ 
//! @brief		OnReload()
//! @date		2010-08-01
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
LRESULT CESMFrameList::OnReload(WPARAM w, LPARAM l)
{
	UpdateFrames();
	return 0;
}

//------------------------------------------------------------------------------ 
//! @brief		DrawList()
//! @date		2010-07-27
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CESMFrameList::UpdateFrames()
{
	//-- Change To Thread
	if(!ISSet())
		return;
	if(m_ThreadHandle)
		return;	

	m_ThreadHandle = (HANDLE) _beginthread( _DrawFrame, 0, (void*)this); // create thread
}

void CESMFrameList::SetZoomIn()		
{	
	if(m_nZoom > 1)
	{
		m_nZoom = m_nZoom / 2;
	}
	else
	{
		m_nZoom =1;
	}
}

void CESMFrameList::SetZoomOut()		
{	
	CString strZoom;
	if(m_nZoom < 100){
		m_nZoom = m_nZoom * 2;
	}
	else
	{
		m_nZoom = 100;
	}
}
void CESMFrameList::MoveLeft()
{
	track_info stTrack = ESMGetTrackInfo();
	CString strDSCID = GetSelectedDSC();

	for(int i = 0 ; i < 4; i++)
	{
		CString strCurID;
		strCurID.Format(_T("%s"),stTrack.st_AxisInfo[i].strCamID);

		if(strDSCID == strCurID)
		{
			m_nArrMov_x[i] = -2;
			m_nArrMov_y[i] = 0;
			break;
		}
		else
			continue;
	}
	ESMSetTrackInfo(stTrack);

/*
	if(stTrack.st_AxisInfo[1].strCamID == strDSCID)
	{
		mov_x = -2;
		mov_y = 0;
	}
	else if(stTrack.st_AxisInfo[0].strCamID == strDSCID)
	{
		mov_x1 = -2;
		mov_y1 = 0;
	}*/
}
void CESMFrameList::MoveRight()
{
	track_info stTrack = ESMGetTrackInfo();
	CString strDSCID = GetSelectedDSC();

	for(int i = 0 ; i < 4; i++)
	{
		CString strCurID;
		strCurID.Format(_T("%s"),stTrack.st_AxisInfo[i].strCamID);

		if(strDSCID == strCurID)
		{
			m_nArrMov_x[i] = 2;
			m_nArrMov_y[i] = 0;
			break;
		}
		else
			continue;
	}
	ESMSetTrackInfo(stTrack);
/*

	if(stTrack.st_AxisInfo[1].strCamID == strDSCID)
	{
		mov_x = 2;
		mov_y = 0;
	}
	else if(stTrack.st_AxisInfo[0].strCamID == strDSCID)
	{
		mov_x1 = 2;
		mov_y1 = 0;
	}*/
}
void CESMFrameList::MoveDown()
{
	track_info stTrack = ESMGetTrackInfo();
	CString strDSCID = GetSelectedDSC();

	for(int i = 0 ; i < 4; i++)
	{
		CString strCurID;
		strCurID.Format(_T("%s"),stTrack.st_AxisInfo[i].strCamID);

		if(strDSCID == strCurID)
		{
			m_nArrMov_x[i] = 0;
			m_nArrMov_y[i] = 2;
			break;
		}
		else
			continue;
	}
	ESMSetTrackInfo(stTrack);

/*
	if(stTrack.st_AxisInfo[1].strCamID == strDSCID)
	{
		mov_x = 0;
		mov_y = 2;
	}
	else if(stTrack.st_AxisInfo[0].strCamID == strDSCID)
	{
		mov_x1 = 0;
		mov_y1 = 2;
	}*/
}
void CESMFrameList::MoveUp()
{
	track_info stTrack = ESMGetTrackInfo();
	CString strDSCID = GetSelectedDSC();

	for(int i = 0 ; i < 4; i++)
	{
		CString strCurID;
		strCurID.Format(_T("%s"),stTrack.st_AxisInfo[i].strCamID);

		if(strDSCID == strCurID)
		{
			m_nArrMov_x[i] = 0;
			m_nArrMov_y[i] = -2;
			break;
		}
		else
			continue;
	}
	ESMSetTrackInfo(stTrack);

	/*if(stTrack.st_AxisInfo[1].strCamID == strDSCID)
	{
		mov_x = 0;
		mov_y = -2;
	}
	else if(stTrack.st_AxisInfo[0].strCamID == strDSCID)
	{
		mov_x1 = 0;
		mov_y1 = -2;
	}*/
}
void CESMFrameList::ScaleUp()
{
	track_info stTrack = ESMGetTrackInfo();
	CString strDSCID = GetSelectedDSC();

	for(int i = 0 ; i < 4; i++)
	{
		CString strCurID;
		strCurID.Format(_T("%s"),stTrack.st_AxisInfo[i].strCamID);

		if(strDSCID == strCurID)
		{
			m_nArrRatioCol[i] ++;
			m_nArrRatioRow[i] ++;

			if(m_nArrRatioCol[i] > 32)
				m_nArrRatioCol[i] = 32;

			if(m_nArrRatioRow[i] > 24)
				m_nArrRatioRow[i] = 24;

			break;
		}
		else
			continue;
	}
	ESMSetTrackInfo(stTrack);
}
void CESMFrameList::ScaleDown()
{
	track_info stTrack = ESMGetTrackInfo();
	CString strDSCID = GetSelectedDSC();

	for(int i = 0 ; i < 4; i++)
	{
		CString strCurID;
		strCurID.Format(_T("%s"),stTrack.st_AxisInfo[i].strCamID);
		
		if(strDSCID == strCurID)
		{
			m_nArrRatioCol[i] --;
			m_nArrRatioRow[i] --;

			if(m_nArrRatioCol[i] < 8)
				m_nArrRatioCol[i] = 8;

			if(m_nArrRatioRow[i] < 6)
				m_nArrRatioRow[i] = 6;

			break;
		}
		else
			continue;
	}
	ESMSetTrackInfo(stTrack);
/*

	if(stTrack.st_AxisInfo.strCamID == strDSCID)
	{
		m_nRatioCol_Left--;
		m_nRatioRow_Left--;

		if(m_nRatioCol_Left <  8 && m_nRatioRow_Left < 6)
		{
			m_nRatioCol_Left = 8;
			m_nRatioRow_Left = 6;
		}
	}
	else if(stTrack.st_Right.strCamID == strDSCID)
	{
		m_nRatioCol_Right--;
		m_nRatioRow_Right--;

		if(m_nRatioCol_Right <  8 && m_nRatioRow_Right < 6)
		{
			m_nRatioCol_Right = 8;
			m_nRatioRow_Right = 6;
		}
	}*/
}
void CESMFrameList::SetSize()
{
	if(!m_bSetBallSize)
	{
		m_bSetBallSize = 1;
		ESMLog(1,_T("[AutoDetecting] Set Size start"));
	}
	else
	{
		m_bSetBallSize = 0;
		ESMLog(1,_T("[AutoDetecting] Set Size Finish"));
	}
}
cv::Point CESMFrameList::GetPOSData()
{
	cv::Point pAxis;
	pAxis.x = pPointData.x;
	pAxis.y = pPointData.y;

	return pAxis;
}
void CESMFrameList::SetPOSData(CPoint point)
{
	pPointData = point;
}
void CESMFrameList::SetAxisData(CPoint point)
{
	pAxisData = point;
}
cv::Point CESMFrameList::GetAxisData(cv::Point pPoint,CSize m_szFrame,int nWidth,int nHeight)
{
	cv::Point pAxis;

	double dbZoomRatio = (double)ESMGetValue(ESM_VALUE_VIEW_RATIO) / 100.0;
	if(dbZoomRatio < 1.0)
		dbZoomRatio = 1.0;
	double dRatioHDtoUHD = m_pTemplateMgr->GetWidth() / (double)nWidth;

	int nLeft = 0,nTop = 0,nReWidth = 0,nReHeight = 0;

	GetImageRect(dbZoomRatio,m_szFrame.cx,m_szFrame.cy,nLeft,nTop,nReWidth,nReHeight);

	pPoint.x = (pPoint.x / dbZoomRatio) + nLeft;
	pPoint.y = (pPoint.y / dbZoomRatio) + nTop;

	//wgkim 191028
	if(true)//ESMGetValue(ESM_VALUE_TEMPLATEPOINT))
	{
		double dRatioX = ((double) nWidth  / (double) m_szFrame.cx);
		double dRatioY = ((double) nHeight / (double) m_szFrame.cy);

		double pRePointX = dRatioX * (double) pPoint.x * dRatioHDtoUHD;
		double pRePointY = dRatioY * (double) pPoint.y * dRatioHDtoUHD;

		pAxis.x = (int)pRePointX;
		pAxis.y = (int)pRePointY;

	//	ESMLog(5,_T("Set Point is (%d,%d)"),pAxis.x,pAxis.y);
	}
	else
	{
		pAxis.x = -1;
		pAxis.y = -1;
	}
	return pAxis;
}
void CESMFrameList::ScaleColsUp()
{
	track_info stTrack = ESMGetTrackInfo();
	CString strDSCID = GetSelectedDSC();

	for(int i = 0 ; i < 4; i++)
	{
		CString strCurID;
		strCurID.Format(_T("%s"),stTrack.st_AxisInfo[i].strCamID);

		if(strDSCID == strCurID)
		{
			m_nArrRatioCol[i] ++;

			if(m_nArrRatioCol[i] > 64)
				m_nArrRatioCol[i] = 64;

			stTrack.st_AxisInfo[i].nScaleX = m_nArrRatioCol[i];
			break;
		}
		else
			continue;
	}
	ESMSetTrackInfo(stTrack);
/*

	if(stTrack.st_AxisInfo[1].strCamID == strDSCID)
	{
		m_nRatioCol_Left++;
		if(m_nRatioCol_Left > 64)
		{
			m_nRatioCol_Left = 64;
		}
		stTrack.st_AxisInfo[1].nScaleX = m_nRatioCol_Left;
	}
	else if(stTrack.st_AxisInfo[0].strCamID == strDSCID)
	{
		m_nRatioCol_Right++;
		if(m_nRatioCol_Right > 64)
		{
			m_nRatioCol_Right = 64;
		}
		stTrack.st_AxisInfo[0].nScaleX = m_nRatioCol_Right;
	}
	ESMSetTrackInfo(stTrack);*/
}
void CESMFrameList::ScaleColsDown()
{
	track_info stTrack = ESMGetTrackInfo();
	CString strDSCID = GetSelectedDSC();

	for(int i = 0 ; i < 4; i++)
	{
		CString strCurID;
		strCurID.Format(_T("%s"),stTrack.st_AxisInfo[i].strCamID);

		if(strDSCID == strCurID)
		{
			m_nArrRatioCol[i] --;

			if(m_nArrRatioCol[i] < 8)
				m_nArrRatioCol[i] = 8;

			stTrack.st_AxisInfo[i].nScaleX = m_nArrRatioCol[i];
			break;
		}
		else
			continue;
	}
	ESMSetTrackInfo(stTrack);

/*
	if(stTrack.st_AxisInfo[1].strCamID == strDSCID)
	{
		m_nRatioCol_Left--;
		if(m_nRatioCol_Left < 8)
		{
			m_nRatioCol_Left = 8;
		}
		stTrack.st_AxisInfo[1].nScaleX = m_nRatioCol_Left;
	}
	else if(stTrack.st_AxisInfo[0].strCamID == strDSCID)
	{
		m_nRatioCol_Right--;
		if(m_nRatioCol_Right < 8)
		{
			m_nRatioCol_Right = 8;
		}
		stTrack.st_AxisInfo[0].nScaleX = m_nRatioCol_Right;
	}
	ESMSetTrackInfo(stTrack);*/
}
void CESMFrameList::ScaleRowsUp()
{
	track_info stTrack = ESMGetTrackInfo();
	CString strDSCID = GetSelectedDSC();


	for(int i = 0 ; i < 4; i++)
	{
		CString strCurID;
		strCurID.Format(_T("%s"),stTrack.st_AxisInfo[i].strCamID);

		if(strDSCID == strCurID)
		{
			m_nArrRatioRow[i] ++;

			if(m_nArrRatioRow[i] > 32)
				m_nArrRatioRow[i] = 32;

			stTrack.st_AxisInfo[i].nScaleY = m_nArrRatioRow[i];
			break;
		}
		else
			continue;
	}
	ESMSetTrackInfo(stTrack);

/*

	if(stTrack.st_Left.strCamID == strDSCID)
	{
		m_nRatioRow_Left++;
		if(m_nRatioRow_Left > 32)
		{
			m_nRatioRow_Left = 32;
		}
		stTrack.st_Left.nScaleY = m_nRatioRow_Left;
	}
	else if(stTrack.st_Right.strCamID == strDSCID)
	{
		m_nRatioRow_Right++;
		if(m_nRatioRow_Right > 32)
		{
			m_nRatioRow_Right = 32;
		}
		stTrack.st_Right.nScaleY = m_nRatioRow_Right;
	}
	ESMSetTrackInfo(stTrack);*/
}
void CESMFrameList::ScaleRowsDown()
{
	track_info stTrack = ESMGetTrackInfo();
	CString strDSCID = GetSelectedDSC();

	for(int i = 0 ; i < 4; i++)
	{
		CString strCurID;
		strCurID.Format(_T("%s"),stTrack.st_AxisInfo[i].strCamID);

		if(strDSCID == strCurID)
		{
			m_nArrRatioRow[i] --;

			if(m_nArrRatioRow[i] < 4)
				m_nArrRatioRow[i] = 4;

			stTrack.st_AxisInfo[i].nScaleY = m_nArrRatioRow[i];
			break;
		}
		else
			continue;
	}
	ESMSetTrackInfo(stTrack);

/*
	if(stTrack.st_AxisInfo[1].strCamID == strDSCID)
	{
		m_nRatioRow_Left--;
		if(m_nRatioRow_Left > 4)
		{
			m_nRatioRow_Left = 4;
		}
		stTrack.st_AxisInfo[1].nScaleY = m_nRatioRow_Left;
	}
	else if(stTrack.st_AxisInfo[0].strCamID == strDSCID)
	{
		m_nRatioRow_Right--;
		if(m_nRatioRow_Right > 4)
		{
			m_nRatioRow_Right = 4;
		}
		stTrack.st_AxisInfo[0].nScaleY = m_nRatioRow_Right;
	}
	ESMSetTrackInfo(stTrack);*/
}
