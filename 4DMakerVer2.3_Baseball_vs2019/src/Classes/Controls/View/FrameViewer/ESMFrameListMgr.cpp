////////////////////////////////////////////////////////////////////////////////
//
//	ESMFrameListMgr.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-26
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMFrameList.h"
#include "ESMFunc.h"
#include "ESMIndex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


void CESMFrameList::CreateImageList(int cx, int cy)
{
	if(m_pImageList)
		RemoveImageList();

	m_pImageList = new CImageList();
	m_pImageList->Create(cx, cy, ILC_COLOR24, 0, 0);//| ILC_MASK, 0, 1);	
}

void CESMFrameList::RemoveImageList()
{
	if(!m_pImageList)
		return;

	//-- Remove Image List
	for(int i = 0; i<m_pImageList->GetImageCount(); i++)
		m_pImageList->Remove(i);
	m_pImageList->DeleteImageList();
	if( m_pImageList)
	{
		delete m_pImageList;
		m_pImageList = NULL;
	}		
}

void CESMFrameList::SetFrameFont(CDC* pDC, int nSize, CString strFont, int nFontType)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	pDC->SelectObject(m_pFont);	
}

//wgkim@esmlab.com
void CESMFrameList::SetViewPointInFrameViewer(CPoint point)
{
	m_pViewPointInFrameViewer= getPointByConsideringMargin(point);
}

CPoint CESMFrameList::GetViewPointInFrameViewer()
{
	return m_pViewPointInFrameViewer;
}

void CESMFrameList::SetTransPointInFrameViewer(CPoint point)
{		
	m_pTransPointInFrameViewer = getPointByConsideringMargin(point);
}

CPoint CESMFrameList::GetTransPointInFrameViewer()
{
	return m_pTransPointInFrameViewer;
}

void CESMFrameList::SetSelectedRealPoint(CPoint point)
{		
	cv::Point pointInImage;
	
	pointInImage.x = point.x;
	pointInImage.y = point.y;

	pointInImage = GetAxisData(pointInImage, m_szFrameImage, nSrcWidth, nSrcHeight);

	m_nRealPoint.x = pointInImage.x;
	m_nRealPoint.y = pointInImage.y;
}

CPoint CESMFrameList::GetSelectedRealPoint()
{
	return m_nRealPoint;
}

Point2f CESMFrameList::ConvertRealPointToFrameViewerPoint(cv::Point2f point)
{
	Point2f resultPoint;

	resultPoint.x = point.x / (3804./m_szFrameImage.cx);
	resultPoint.y = point.y / (2160./m_szFrameImage.cy);	

	return resultPoint;
}

void CESMFrameList::SetTemplateMgr(CESMTemplateMgr* templateMgr)
{
	m_pTemplateMgr = templateMgr;
}

CPoint CESMFrameList::getPointByConsideringMargin(CPoint point)
{
	CPoint pointInImage;
	CRect viewerRect;
	CSize viewerSize;
	CSize frameSize;

	int marginX = 0;
	int marginY = 0;

	GetClientRect(&viewerRect);	
	viewerSize.cx = viewerRect.right;
	viewerSize.cy = viewerRect.bottom;
	frameSize = GetImageSize();

	marginX = m_nLeftMargin;
	marginY = m_nTopMargin;

	pointInImage.x = point.x - marginX;
	pointInImage.y = point.y - marginY;

	return pointInImage;
}

CPoint CESMFrameList::Point2fToCPoint(Point2f point)
{
	CPoint temp;
	temp.x = point.x;
	temp.y = point.y;

	return temp;
}

Point2f CESMFrameList::CPointToPoint2f(CPoint point)
{
	Point2f temp;
	temp.x = point.x;
	temp.y = point.y;

	return temp;
}

//wgkim 180512
void CESMFrameList::SetVerticalImagePointInFrameViewer(CPoint point)
{
	CPoint pointInImage;
	CRect viewerRect;
	CSize viewerSize;
	CSize frameSize;

	int marginX = 0;
	int marginY = 0;

	GetClientRect(&viewerRect);	
	viewerSize.cx = viewerRect.right;
	viewerSize.cy = viewerRect.bottom;
	frameSize = GetImageSize();

	marginX = m_nLeftMargin;
	marginY = m_nTopMargin;

	pointInImage.x = point.x - marginX;
	pointInImage.y = point.y - marginY;
	
	m_pVerticalImagePointInFrameViewer = pointInImage;
}

CPoint CESMFrameList::GetVerticalImagePointInFrameViewer()
{
	return m_pVerticalImagePointInFrameViewer;
}

CPoint CESMFrameList::GetVerticalImagePointFromRealSize()
{
	CPoint pRealSizePoint;

	pRealSizePoint.x = (double)m_pVerticalImagePointInFrameViewer.x * ((double)nSrcWidth/m_szFrameImage.cx);
	pRealSizePoint.y = (double)m_pVerticalImagePointInFrameViewer.y * ((double)nSrcHeight/m_szFrameImage.cy);

	return pRealSizePoint;
}

CPoint	CESMFrameList::GetVerticalImagePointToTemplatePoint()
{
	return m_pVerticalImagePointToTemplatePoint;
}