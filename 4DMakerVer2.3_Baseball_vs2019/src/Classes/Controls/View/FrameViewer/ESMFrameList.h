////////////////////////////////////////////////////////////////////////////////
//
//	ESMFrameList.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-24
// @ver.1.0	4DMaker
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include <vector>
#include <sstream>
#include "ToolTipListCtrl.h"
#include "RSChildDialog.h"
#include "ESMIndexStructure.h"
#include "highgui.h"
#include "ESMIndex.h"
#include "cv.h"
#include "DSCFrameSelector.h"
#include "ESMFrameAutoDetect.h"

using namespace cv;

#define WM_LOAD_FRAME	WM_USER + 4001

#define FRAME_MARGIN_DIV_X	200	
#define FRAME_MARGIN_DIV_Y	25
#define FRAME_MARGIN_IMAGE	3
#define MAX_SELECT_LINE 100
void _DrawFrame(void *param);



typedef struct _ESM_FRAME_ARRAY_DATA
{
	vector<ESMFrameArray*> arrFrame;
	CString strImagePath;
	int nFrameWidth;
	int nFrameHeight;
	int nMsg;
	int nIndex;

}ESM_FRAME_ARRAY_DATA;

// CESMFrameList dialog
class CESMFrameList : public CRSChildDialog
{
	DECLARE_DYNAMIC(CESMFrameList)
public:
	CESMFrameList(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMFrameList();

// Dialog Data
//	enum { IDD = IDD_VIEW_ONLY_TOOLBAR };
	enum { IDD = IDD_VIEW_FRAMELIST };
	enum {
		FILE_NONE = -1,
		FILE_ADD,
		FILE_DELETE,
		FILE_ERR,
	};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();		
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);		
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	afx_msg LRESULT OnReload(WPARAM , LPARAM );
	afx_msg BOOL PreTranslateMessage(MSG* pMsg);
	
	DECLARE_MESSAGE_MAP()	
		
private:	
	CImageList*		m_pImageList;		// image list holding the frames
	int				m_nSelectedTime[MAX_SELECT_LINE];	// milli second unit
	CString			m_strOpenedFile;
	CString			m_strDSCID;
	CString			m_strIP;
	int				m_nSelectedFrame;
	CDSCFrameSelector *m_pFrameSelector;	
	int				m_nFirstPic;
	int				m_nTooltipPicture;
	int				m_nDrawedPicture;	
	int				m_nPressed;
	int				m_nImageCount;
	BOOL			m_bDrawAll;	
	BOOL			m_bLeftAlign;
	CRect			m_rtScrollBase;
	CRect			m_rtScrollBar;	

	int				m_nZoom;
	int				m_nPosX, m_nPosY;
	int				m_nSrcPosX, m_nSrcPosY;
	int				m_nPrevX, m_nPrevY;

	int				m_nArrMov_x[4];
	int				m_nArrMov_y[4];

public:
	HANDLE			m_ThreadHandle;
	BOOL			m_ReLoadImage;
	CStringArray	m_arrImageName;
	ESM_FRAME_ARRAY_DATA m_frameArrayData;
	ESM_FRAME_ARRAY_DATA m_OriArrayData;
	BOOL			m_bThreadRun;
	CString			m_strPreMovieFile;
public:
	static unsigned WINAPI CatpureImageThread(LPVOID param);
	void CreateImageList(int cx, int cy);
	void RemoveImageList();
	
	void DrawList(CRect rect);
	void OnMoveScroll(BOOL bRight, BOOL bOneStep);	
	
	//---------------------------------------------------------------
	//-- Control Frame List
	//---------------------------------------------------------------
	HBITMAP GetRotatedBitmap( HBITMAP hBitmap, float radians, COLORREF clrBack );
	void SetFrameSize();
	BOOL LoadImageList(CDC* pDC, CStringArray* pArImage);				// gather the image file names	
	BOOL LoadImagePicture(CStringArray* pArImage);
	void DrawImage(CDC* pDC, CStringArray* pArImage);		// draw the frames in list control	
	void DrawBlank(CDC* pDC);
	CString GetFramePath();	
	
	BOOL IsDrawAll()		{ return m_bDrawAll; }
	BOOL IsFirstPic()		{ if(m_nFirstPic) return FALSE; else return TRUE; }
	BOOL IsLeftAlign()		{ return  m_bLeftAlign; }
	float GetInnerBarLength();
	float GetInnerBarLocation();
	
	void SetSelectedDSC(CString str)	
	{	
		TRACE(_T("%s"),str);
		/*m_strDSCID.Format(_T("%s"),str);*/
		m_strDSCID = str;	
	}
	CString GetSelectedDSC()		{	return m_strDSCID;}
	void SetSelectedIP(CString str)	{m_strIP = str;}
	CString GetSelectedIP()			{return m_strIP;}

	void SetZoomIn();
	void SetZoomOut();

	void SetSelectedTime(int nTime, int nLine) 
	{
		nSelect = nLine; 
		m_nSelectedTime[nLine] = nTime;	

		TRACE(_T("!!!!!!!!!!!!!!!!!!m_nSelectedTime %d\n"), nTime);
	}
	int GetSelectedTime(int nLine)				{nLine = nSelect; return m_nSelectedTime[nLine];}
	CString GetFrameFileName(int nBaseTime, int nAddIndex);

	void SetImageCnt(int n)	{ m_nImageCount = n;}
	int  GetImageCnt()		{ return m_nImageCount ;}

	CSize GetImageSize()	{ return m_szFrameImage; }


	/*Section AutoDetecting*/
	//2016-06-27 hjcho
	void MoveLeft();
	void MoveRight();
	void MoveUp();
	void MoveDown();
	void ScaleColsUp();
	void ScaleColsDown();
	void ScaleRowsUp();
	void ScaleRowsDown();

	void ScaleUp();
	void ScaleDown();

	//2016-08-18 hjcho
	void SetSize();

	//2017-05-29 hjcho
	void DrawAutoDetectingLine(CDC* mDC,CString strCurCamID,int nX,int nY);
	void DrawLine(CDC* mDC,int nStrX,int nStrY,int nEndX,int nEndY,int nX,int nY,int nImgX,int nImgY);
	void Get_ROI(Mat ori,Mat ROI,int t_x, int t_y);	
	int ATSetBallSize(track_info stTrack,CString strCamID,int nSearchIndex);
	int DoAutoDetect(track_info stTrack,int nSearchIndex,int nMovieIndex,BOOL bReverse,CString strDSCID,int nExpandSize = 30);
	BOOL m_bSetBallSize;
	int m_nCircleSize;
	int m_nDetectingTime;
	int selected_frame;
	int Auto_num;
	
	//2016-11-07 hjcho
	BOOL GetCaptureImageFromVideo(CString strpath,CString strDSCID,vector<ESMFrameArray*>pImgArr);
	
	//2016-11-18 hjcho
	int GetVideoSize(CString strpath,int* nSrcWidth,int* nSrcHeight);
	//180515 wgkim
	int GetVideoSize(CString strpath,int* nSrcWidth,int* nSrcHeight, int nMaxFrameCount);
	
	//2016-11-23 hjcho
	int nSelect;

	//2017-02-13 hjcho
	static unsigned WINAPI DecodingFrame(LPVOID param);

	//2017-03-10 hjcho
	CRITICAL_SECTION m_cr;
	CRITICAL_SECTION m_crFrameLoad;

	//2016-12-07 hjcho Adapting adjust at Frameviewer
	BOOL AdaptingAdjust(Mat img,int nSrcWidth, int nSrcHeight,int m_Width,int m_Height,BOOL reverse);
	void LoadAdjust(CString strDSC);
	void CpuMoveImage(Mat* gMat, int nX, int nY);
	void CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle, BOOL bReverse);
	int Round(double dData);
	stAdjustInfo adjInfo;
	int m_MarginX,m_MarginY;

	//2017-07-27 hjcho FrameViewer 확장
	int GetRealTime(char key);	
	BOOL GetCaptureImageFromVideo(CString strpath,CString strPre,BOOL bIsPreOrNext,vector<ESMFrameArray*>pImgArr);
	static unsigned WINAPI DecodingExpandFrame(LPVOID param);
	BOOL GetThreadLoad(int nIdx){return m_bThreadLoad[nIdx];}
	void SetThreadLoad(BOOL m_b,int nIdx){m_bThreadLoad[nIdx] = m_b;}

	CString	m_strPreExpandFile;
	BOOL m_bThreadLoad[2];
	BOOL m_bThread[2];
	int m_nFrameLoadCnt;
	BOOL m_bIsFrameHalf;
	int m_nExpandTime;
	int m_nDivTime;
	int m_nSkipIdx;
	int m_nArrMovieIdx[2];
	//마우스 커서가 영상에 있을 시 좌표값 얻어오기 위함.
	cv::Point GetPOSData();
	void SetPOSData(CPoint point);		
	CPoint pPointData;
	int nSkipCount;
	int nSkipIndex;
	int nSrcWidth, nSrcHeight;

	//마우스 우클릭 및 원본 좌표 출력
	void SetAxisData(CPoint point);
	cv::Point GetAxisData(cv::Point pPoint,CSize m_szFrame,int nWidth,int nHeight);
	cv::Point pAxisPoint;	//프레임뷰 저장용 좌표
	cv::Point pRealPoint;	//실제 좌표계 출력	
	CPoint pAxisData;

	//2016-12-11 wgkim@esmlab.com	
public:	
	void	SetViewPointInFrameViewer(CPoint point);
	CPoint	GetViewPointInFrameViewer();

	void	SetTransPointInFrameViewer(CPoint point);
	CPoint	GetTransPointInFrameViewer();

	void	SetSelectedRealPoint(CPoint point);
	CPoint  GetSelectedRealPoint();
	Point2f	ConvertRealPointToFrameViewerPoint(cv::Point2f point);	

	void	SetTemplateMgr(CESMTemplateMgr* templateMgr);

	CPoint	Point2fToCPoint(Point2f point);
	Point2f CPointToPoint2f(CPoint point);

	void	DrawViewWindowAndCenterLine(Mat img);
	void	DrawAreaWhereCoordinateMove(Mat img);
	bool	clickFlag;

	BOOL	IsMousePointOnFrameViewer();	
	void	SetWhetherMousePointIsOnFrameViewer(BOOL flag);

	//Template Zoom Value in FrameViewer
	void	SetZoomValueInFrameViewer(int zoomValue);	

	//hjcho 171210 - FrameView with Zoom
	void	SetImageUsingZoomRatio(Mat &img);
	void	GetImageRect(double dbZoomRatio,int nOriWidth,int nOriHeight,int &nLeft,int &nTop,int &nWidth,int &nHeight);

private:
	CPoint	getPointByConsideringMargin(CPoint point);

	CESMTemplateMgr* m_pTemplateMgr;
	CPoint	m_pViewPointInFrameViewer;
	CPoint	m_pTransPointInFrameViewer;
	CPoint  m_nRealPoint;

	int fixXValueWhenShiftKeyIsPressed;
	BOOL flagMousePointOnFrameViewer;

	//Template Zoom Value in FrameViewer
	int m_dZoomValue;

	//171124 wgkim
	int m_nLeftMargin;
	int m_nTopMargin;
	//////////////////////////////

public:
	//wgkim 180512
	void	TransImageHorizonToVertical(Mat& img, int nVertical);

	void	DrawViewWindowAndCenterLineInVertical(Mat img);

	void	SetVerticalImagePointInFrameViewer(CPoint point);
	CPoint	GetVerticalImagePointInFrameViewer();
	CPoint	GetVerticalImagePointFromRealSize();
	CPoint	GetVerticalImagePointToTemplatePoint();

	BOOL	m_bVertical;
	CPoint	m_pVerticalImagePointInFrameViewer;
	CPoint	m_pVerticalImagePointToTemplatePoint;

public:
	//wgkim 180514 SplitVideoAsGOP
	int SplitVideoOnGop(CString strDscId, double dSplitCount, int nMinute, CString strInputFilePath, CString strOutputFilePath);
	
	//wgkim 180515
	float m_dGop;
	float m_dFrameRate;
	float m_dDurationTime;

	BOOL m_bCheck4DPOrNot;

	static unsigned WINAPI DecodingFrameInFrameViewer(LPVOID param);

public:
	int m_nArrRatioCol[4];
	int m_nArrRatioRow[4];
private:

	CSize m_szFrameImage;
	int m_nFrame_margin	;
	int m_nFrame_margin_y	;
	
	int m_nFrame_margin_start;
	int m_nFrame_picture_size	;
	int m_nFrame_picture_gap	;

	CFont *m_pFont;
	
	//---------------------------------------------------------------
	//-- Control File Management
	//---------------------------------------------------------------
	void UpdateFrames();
	void SetFrameFont(CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);

	//  2013-10-23 Ryumin
	//	Add H-Scroll
private:
	int		m_nCurWidth;
	int		m_nCurHeight;
	int		m_nHScrollPos;
	int		m_nVScrollPos;
	CRect	m_rcRect;

public:
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

	//2016-12-11 wgkim@esmlab.com	
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	BOOL m_bIsPressedRButton;
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);

	//jhhan 181228
	BOOL GetCaptureImage(CString strPath);
	static unsigned WINAPI DecodingFrameEx(LPVOID param);
	static unsigned WINAPI DecodeFrameViewerEx(LPVOID param);
	static unsigned WINAPI DecodeFrameViewerSub(LPVOID param);
	BOOL LoadImageListEx(CDC* pDC, CStringArray* pArImage);				// gather the image file names	
	static unsigned WINAPI SplitVideoOnGopEx(LPVOID param);
	void SplitVideoExtender(CString strFile, int nPrevNext = 0);
	vector<CString> m_vSplit;

	//190402 hjcho
	void SaveCurrentImage(CString str)
	{
		if(ESMGetFrameArray().at((selected_frame))->pImg)
		{
			ESMFrameArray* pArr = ESMGetFrameArray().at(selected_frame);

			Mat img(pArr->nHeight,pArr->nWidth,CV_8UC3,pArr->pImg);
			resize(img,img,cv::Size(720,447),0,0,cv::INTER_LINEAR);

			char strPath[200];
			sprintf(strPath,"%S",str);

			imwrite(strPath,img);
		}
		else
		{
			for(int i = 0 ; i < 10 ; i ++)
				ESMLog(0,_T("[RTSP] DO NOE SAVE STILL IMAGE!!!!"));
		}
	}
};
//2017-02-21 hjcho
struct ThreadDecoding
{
	ThreadDecoding()
	{
		bRet = FALSE;
		pData = NULL;
	}
	CString strPath;
	vector<ESMFrameArray*>pImg;
	CESMFrameList* pParent;
	BOOL bRet;
	vector<ESMFrameArray*> *pData;
};
//2017-07-23 hjcho
struct ThreadFrameExpandDecoding
{
	ThreadFrameExpandDecoding()
	{
		nStart = -1;
		nEnd   = -1;
		//pImgArr = NULL;
		nDelay = 0;
	}
	CString strPath;
	vector<ESMFrameArray*>pImgArr;
	CESMFrameList* pParent;
	int nStart;
	int nEnd;
	int nIdx;
	int nDelay;
};