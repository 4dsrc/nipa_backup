////////////////////////////////////////////////////////////////////////////////
//
//	ESMFrameListMgr.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-26
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMFrameList.h"
#include "ESMFileOperation.h"
#include "ESMFunc.h"
#include "ESMIndex.h"
#include "ESMCtrl.h"
#include "DSCViewDefine.h"
#include "FFmpegManager.h"
#include "DSCItem.h"
#include "ESMFrameViewer.h"
#include "ESMDefine.h"
#define VIDEOCAPTURE
#define MSG_PRE_FRAME_DECODING 99
#define MSG_NEXT_FRAME_DECODING 100
//#define EXPAND

#include "ESMMovieMgr.h"
#include "ESMImgMgr.h"
// #ifdef _DEBUG
// #define new DEBUG_NEW
// #undef THIS_FILE
// static char THIS_FILE[] = __FILE__;
// #endif
//----------------------------------------------------------------
//-- 2013-04-22 hongsu.jung
//-- Thread Function
//----------------------------------------------------------------

CMutex g_mtxDraw(FALSE, _T("Frame"));

cv::Rect box;
bool drawing_box = false;
void draw_box(Mat img, cv::Rect rect)
{
	rectangle(img, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), cvScalar(0xff, 0x00, 0x00));
	//cvCircle(img,cvPoint(rect.x,rect.y),5,cvScalar(0xff, 0x00, 0x00),1,8);
}
void Frame_OnEventhandle (int event, int x, int y, int flags, void* param) //핸들러 함수
{
	Mat* img = (Mat*) (param);
	switch(event)
	{
	case EVENT_MOUSEMOVE:
		{
			if(drawing_box)
			{
				box.width = x-box.x;
				box.height = y-box.y;
			}
		}
		break;
	case CV_EVENT_LBUTTONDOWN:
		{
			drawing_box = true;
			box = cv::Rect(x, y, 0, 0);
		}
		break;
	case CV_EVENT_LBUTTONUP :
		{
			drawing_box = false;
			if(box.width < 0)
			{
				box.x += box.width;
				box.width *= -1;
			}
			if(box.height < 0)
			{
				box.y += box.height;
				box.height *= -1;
			}
			draw_box(*img, box);
		}
		break;
	}
}

void _DrawFrame(void *param)
{
	CESMFrameList* pView = (CESMFrameList*)param;
	if (!pView) return;

	CDC* pDC = pView->GetDC();
	if(!pDC) return;

	pView->SetFrameSize();

	//Mouse Event
	//-- Add Image File from List Size and Selected Time
	if( ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
	{	
		if(pView->m_ReLoadImage || ESMGetValue(ESM_VALUE_TEMPLATEPOINT) || pView->m_bVertical != 0)
		{
			pView->m_arrImageName.RemoveAll();
			pView->CreateImageList(pView->GetImageSize().cx, pView->GetImageSize().cy);
			pView->LoadImageList(pDC, &pView->m_arrImageName);
			//jhhan 190110
			//pView->LoadImageListEx(pDC, &pView->m_arrImageName);
			pView->DrawImage(pDC, &pView->m_arrImageName);
			pView->m_ReLoadImage = FALSE;
		}
		else
		{
			pView->DrawImage(pDC, &pView->m_arrImageName);
		}
	}
	else
	{
		if( pView->m_ReLoadImage)
		{
			pView->m_arrImageName.RemoveAll();
			pView->CreateImageList(pView->GetImageSize().cx, pView->GetImageSize().cy);
			pView->LoadImagePicture(&pView->m_arrImageName);
			pView->DrawImage(pDC, &pView->m_arrImageName);
			pView->m_ReLoadImage = FALSE;
		}
		else
		{
			pView->DrawImage(pDC, &pView->m_arrImageName);
		}
	}

	//-- Clear
	pView->m_ThreadHandle = NULL;
	pView->ReleaseDC(pDC);
	_endthread();
}
//------------------------------------------------------------------------------ 
//! @brief		SetFrameSize()
//! @date		2010-07-07
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void  CESMFrameList::SetFrameSize()
{
	CRect rectAll;
	GetClientRect(&rectAll);	

	float nFullHD_W	= FULL_HD_WIDTH;
	float nFullHD_H	= FULL_HD_HEIGHT;

	//-- Get Margin X,Y
	int nMarginX = rectAll.Width()	/ FRAME_MARGIN_DIV_X;
	int nMarginY = rectAll.Height()	/ FRAME_MARGIN_DIV_Y;

	//-- Get Y Position
	float nImgY = (float)(rectAll.Height() - nMarginY);
	if(nImgY < FRAME_MIN_HEIGHT)
		nImgY = FRAME_MIN_HEIGHT;
	//-- Get X Position
	float nImgX = nImgY * nFullHD_W / nFullHD_H;
	if(nImgX < FRAME_MIN_WIDTH)
		nImgX = FRAME_MIN_WIDTH;
	//-- Get Image Count
	int nImgCnt = (int)(rectAll.Width() / (nImgX+nMarginX));
	if(nImgCnt == 0 ||  ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
		nImgCnt = 1;
	//-- 2013-09-25 hongsu@esmlab.com
	//-- Set Image Count
	SetImageCnt(nImgCnt);

	//-- Set Image Size
	m_szFrameImage.cx = (LONG)nImgX;
	m_szFrameImage.cy = (LONG)nImgY;
}

unsigned WINAPI CESMFrameList::CatpureImageThread(LPVOID param)
{
	CESMFrameList* pMain = (CESMFrameList*)param;
	pMain->m_bThreadRun = TRUE;

	FFmpegManager FFmpegMgr(FALSE);
	FFmpegMgr.GetCaptureImage(pMain->m_frameArrayData.strImagePath, 
		pMain->m_frameArrayData.nIndex, 
		//pMain->m_frameArrayData.nFrameWidth, 
		//pMain->m_frameArrayData.nFrameHeight, 
		pMain->m_frameArrayData.arrFrame, 
		pMain->m_frameArrayData.nMsg);

	pMain->m_bThreadRun = FALSE;
	return 0;
}

CCriticalSection g_cs;
#define FRAMEVIEWER_SPLIT
//------------------------------------------------------------------------------ 
//! @brief		LoadImageList(CStringArray* pArImage)
//! @date		2010-07-07
//! @date		2010-07-25
//! @author	hongsu.jung
//! @edit		Add Option (Added, Deleted, Error)
//------------------------------------------------------------------------------ 
BOOL CESMFrameList::LoadImageList(CDC* pDC, CStringArray* pArImage)
{			
#ifdef EXPAND
	/*for(int i = 0 ; i < 2; i++)
	{
	if(m_bThread[i])
	{
	while(1)
	{
	ESMLog(1, _T("###########Frame Treading...."));
	Sleep(10);
	if(!m_bThread[i])
	break;
	}
	}
	}*/
#else
	if(m_bThreadRun)
	{
		while(1)
		{
			ESMLog(1, _T("###########Frame Treading...."));
			Sleep(10);
			if(!m_bThreadRun)
				break;
		}
	}
#endif

	CString		strExt		= _T("");
	CString		strName		= _T("");
	CString		strPath		= _T("");
	CString		strImagePath= _T("");
	CBitmap*	pImage		= NULL;	
	BOOL		bRC			= TRUE;
	int			nAll		= 0;

#ifdef FRAMEVIEWER_SPLIT
	//if(!m_bCheck4DPOrNot)
	if(ESMGetFrameRate() != MOVIE_FPS_FHD_120P)
	{
		m_dGop = 20.f;
		m_dFrameRate = 60.f;
		m_dDurationTime = 1.f;

		if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P) //180308
		{
			m_dFrameRate = 50.f;

			if(ESMGetDevice() == SDI_MODEL_NX1)
				m_dGop = 25.f;
			if(ESMGetDevice() == SDI_MODEL_GH5)
				m_dGop = 16.f;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_25P || ESMGetFrameRate() == MOVIE_FPS_UHD_25P)
		{
			m_dFrameRate = 25.f;

			if(ESMGetDevice() == SDI_MODEL_NX1)
				m_dGop = 12.f;
			if(ESMGetDevice() == SDI_MODEL_GH5)
				m_dGop = 8.f;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
		{
			m_dFrameRate = 60.f;

			if(ESMGetDevice() == SDI_MODEL_NX1)
				m_dGop = 30.f;
			if(ESMGetDevice() == SDI_MODEL_GH5)
				m_dGop = 20.f;
		}

		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
		{
			m_dFrameRate = 120.f;

			if(ESMGetDevice() == SDI_MODEL_NX1)
				m_dGop = 30.f;
			if(ESMGetDevice() == SDI_MODEL_GH5)
				m_dGop = 40.f;
		}

		else
		{
			m_dFrameRate = 30.f;

			if(ESMGetDevice() == SDI_MODEL_NX1)
				m_dGop = 15.f;
			if(ESMGetDevice() == SDI_MODEL_GH5)
				m_dGop = 10.f;
		}
	}
#endif

	//jhhan 170615 Load 초기화 방지
	/*
	m_nSrcPosX = 0;
	m_nSrcPosY = 0;
	m_nPosX = 0;
	m_nPosY = 0;
	m_nPrevX = 0;
	m_nPrevY = 0;
	*/

	//-- Find First File
	int nCnt = GetImageCnt();
	int nTime = GetRealTime(ESMGetKeyValue());

	if(nTime < 0)
	{
		//ESMLog(5, _T("Image Load Fail NonSelectTime[%d]"), nTime);
		ESMSetLoadingFrame(FALSE);
		return FALSE;
	}
	EnterCriticalSection(&m_cr);
	CString strDSCID = GetSelectedDSC();
	LeaveCriticalSection(&m_cr);
	if(!strDSCID.GetLength())
	{
		//ESMLog(5, _T("Image Load Fail NonSelectDSC[%d]"), nTime);
		ESMSetLoadingFrame(FALSE);
		return FALSE;
	}

	m_bCheck4DPOrNot = ESMGetCheckIf4DPOrNot(ESMGetIPFromDSCIDInFrameViewer(strDSCID));
	
	//190222 hjcho
	if(ESMGetValue(ESM_VALUE_RTSP) == TRUE && ESMGetLoadRecord() == TRUE)
		m_bCheck4DPOrNot = TRUE;

	//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 ImageLoader에서 Image 얻어온다
	int nFIdx = ESMGetFrameIndex(nTime);
	if(ESMIsImageFromImageLoader(strDSCID, nFIdx))
	{
		ESMLog(5, _T("ImageLoader[%s][%d]"), strDSCID, nFIdx);
		pArImage->Add(GetFrameFileName(nTime, 0));	

		if(m_pImageList)
			m_pImageList->SetImageCount(1);
		pImage = ESMGetImageFromImageLoader(strDSCID, nFIdx, pDC, m_szFrameImage.cx, m_szFrameImage.cy);
		if(m_pImageList)
			m_pImageList->Replace(0, pImage, NULL);

		pImage->DeleteObject();
		if(pImage)
		{
			delete pImage;
			pImage = NULL;
		}

		/*
		pImage = ESMGetImageFromImageLoader(strDSCID, nFIdx, pDC);
		m_pImageList->Replace(0, pImage, NULL);
		pImage->DeleteObject();
		if(pImage)
		{
		delete pImage;
		pImage = NULL;
		}
		*/
		return TRUE;
	}
	strImagePath = ESMGetMoviePath(strDSCID, nFIdx, FALSE, TRUE);

	int nMid = nCnt/2;
	strName = GetFrameFileName(nTime, 0-nMid);
	CESMFileOperation fo;
	BOOL bCheckFile = FALSE;
	if(fo.IsFileExist(strImagePath))
	{
		pArImage->Add(strName);
		bCheckFile = TRUE;
	}
	else
	{
		ESMLog(5, _T("Load Fail Movie[%s]"), strImagePath);
		return FALSE;
	}

	/*if(nSkipCount == 0)
	ESMLog(5, _T("Load Movie[%s] [%d]"), strImagePath, GetTickCount());*/
	//ESMSetSelectDSC(strDSCID);


	// set the size of the image l6ist
	nAll = (int)pArImage->GetSize();
	if(m_pImageList)
		m_pImageList->SetImageCount(nAll);

	FFmpegManager FFmpegMgr(FALSE);

	int nShiftFrame = 0;
	nShiftFrame = ESMGetShiftFrame(strDSCID);

	nShiftFrame = nShiftFrame + ESMGetFrameIndex(nTime);
	int nMovieIndex = 0, nRealFrameIndex = 0; 
	ESMGetMovieIndex(nShiftFrame, nMovieIndex, nRealFrameIndex);

	//wgkim 170728	
	ESMSetSelectedFrameNumber(nShiftFrame);

	//ESMLog(5, _T("ESMGetMovieIndex( %d, %d, %d )"), nShiftFrame, nMovieIndex, nRealFrameIndex);
	BOOL bRet = FALSE;

#ifdef EXPAND
	//BOOL bIsFrameHalf;
	CString strExpandPath = _T("");
	int nSkipCount = 0;

	if(m_nFrameLoadCnt == 0 || (m_strPreMovieFile.CompareNoCase(strImagePath)!=0 &&
		m_strPreExpandFile.CompareNoCase(strImagePath)!= 0))
	{
		//새로운 파일 입력 시
		if(m_nFrameLoadCnt != 0)
		{
			for(int i = 0 ; i < 2; i++)
			{
				if(!GetThreadLoad(i))
				{
					while(1)
					{
						ESMLog(5,_T("#######New File Threading[%d]"),i);
						//SetThreadLoad(FALSE);

						if(GetThreadLoad(i))
							break;

						Sleep(10);
					}
				}
			}
		}

		int nDivIdx = 0;
		if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P)
		{
			m_nDivTime = 500;
			m_nSkipIdx = (movie_frame_per_second/2); // 25
			nDivIdx = (movie_frame_per_second/4);	 //12
		}
		else if(ESMGetFrameRate()==MOVIE_FPS_UHD_25P || ESMGetFrameRate()==MOVIE_FPS_FHD_25P)
		{
			m_nDivTime = 960;
			m_nSkipIdx = (movie_frame_per_second-1);//24
			nDivIdx = (movie_frame_per_second-1)/2 - 1; //11
		}
		else
		{
			m_nDivTime = 1000;
			m_nSkipIdx = movie_frame_per_second; //30
			nDivIdx    = (movie_frame_per_second / 2) - 1; // 14
		}


#if 1
		int nExpandMovieIdx = 0;
		ESMLog(5,_T("nDivIdx = %d"),nDivIdx);

		if(nRealFrameIndex > nDivIdx)
		{
			m_nExpandTime = nTime + m_nDivTime;
			m_nArrMovieIdx[0] = nMovieIndex;
			m_nArrMovieIdx[1] = nMovieIndex + 1;
			m_bIsFrameHalf = TRUE;
		}
		else
		{
			m_nExpandTime = nTime - m_nDivTime;
			m_nArrMovieIdx[0] = nMovieIndex - 1;
			m_nArrMovieIdx[1] = nMovieIndex;
			m_bIsFrameHalf = FALSE;			

			if(m_nExpandTime < 0)
			{			
				m_nExpandTime = nTime + m_nDivTime;	
				m_nArrMovieIdx[0] = nMovieIndex;
				m_nArrMovieIdx[1] = nMovieIndex + 1;
				m_bIsFrameHalf = TRUE;
			}
		}
#else
		//서로다른 경로가 들어오면 새로 로드
		if(nTime % m_nDivTime > (m_nDivTime/2))
		{
			m_nExpandTime = nTime + m_nDivTime;
			m_bIsFrameHalf = TRUE;
		}
		else
		{
			m_nExpandTime = nTime - m_nDivTime;
			m_bIsFrameHalf = FALSE;			

			if(m_nExpandTime < 0)
			{			
				m_nExpandTime = nTime + m_nDivTime;			
				m_bIsFrameHalf = TRUE;
			}
		}
#endif
		int nExpandFIdx = ESMGetFrameIndex(m_nExpandTime);
		strExpandPath = ESMGetMoviePath(strDSCID, nExpandFIdx);

	}
	else if(m_strPreExpandFile.CompareNoCase(strImagePath)== 0)
	{
		m_strPreExpandFile = m_strPreMovieFile;
		m_strPreMovieFile = strImagePath;

		if(m_bIsFrameHalf)
			m_bIsFrameHalf = FALSE;
		else
			m_bIsFrameHalf = TRUE;
	}
	else
	{
		strExpandPath = m_strPreExpandFile;
	}

	if(!m_bIsFrameHalf)
		nRealFrameIndex += m_nSkipIdx;

	/*nSkipCount = m_nMovieSize;*/
#endif	
	vector<ESMFrameArray*> * pFrame = NULL;

	while(nAll--)
	{	
		strName = pArImage->GetAt(nAll);
		pImage = new CBitmap();
		pImage->CreateCompatibleBitmap(pDC, m_szFrameImage.cx, m_szFrameImage.cy);

		BOOL bSkip = FALSE;
		int nSearchIndex;

		//wgkim 180514
#ifdef FRAMEVIEWER_SPLIT
		//if(!m_bCheck4DPOrNot)
		if(ESMGetFrameRate() != MOVIE_FPS_FHD_120P && m_bCheck4DPOrNot == FALSE)
		{
			int nSearchNum = nRealFrameIndex / m_dGop;
			nRealFrameIndex = nRealFrameIndex % (int)m_dGop;

			CString strFrameViewerSplitVideoPath = ESMGetFrameViewerSplitVideo4dmPath();
			ESMCreateAllDirectories(strFrameViewerSplitVideoPath);

			if(GetFileAttributes(strFrameViewerSplitVideoPath) != 0xFFFFFFFF)
			{
				CString strTok, strTokWithExtension;

				int nStrCount = ESMUtil::GetFindCharCount(strImagePath, '_');

				AfxExtractSubString(strTokWithExtension, strImagePath, nStrCount, '_');
				AfxExtractSubString(strTok, strTokWithExtension, 0, '.');

				int nStrLength = strTok.GetLength();

				float dSplitCount = (m_dDurationTime * m_dFrameRate) / m_dGop;

				if(strImagePath.Find(strTokWithExtension))
				{
					CString strNumber;
					int nNumber = _ttoi(strTok);
					nNumber = nNumber * dSplitCount + nSearchNum;
					float dStartTime = m_dDurationTime / dSplitCount * nSearchNum;
					float dEndTime = m_dDurationTime / dSplitCount * (nSearchNum+1);
					strNumber.Format(_T("%d"), nNumber);

					SplitVideoOnGop(strDSCID, dSplitCount, _ttoi(strTok), strImagePath, strFrameViewerSplitVideoPath);
					
					CString strReplace = strNumber+_T(".mp4");
					strFrameViewerSplitVideoPath.Append(strDSCID + _T("_") + strReplace);
					strImagePath = strFrameViewerSplitVideoPath;
				}
			}
			else
			{
				ESMLog(5, _T("[FrameViewer]not Exist File in Ramdisk Path"));
				return 0;
			}
		}
#endif
		///////////////////////////////////////

#ifdef VIDEOCAPTURE
		//Load Check, m_strLoadExpand까지 확인할 것.
#ifdef EXPAND
		if(m_strPreMovieFile.CompareNoCase(strImagePath) == 0 || m_strPreExpandFile.CompareNoCase(strExpandPath) == 0)
#else
		//190107
		pFrame = ESMGetFrame(strImagePath);

		if(m_strPreMovieFile.CompareNoCase(strImagePath) == 0)
#endif		
		{
			int nSelectedFrameIndex;
			if(!m_bCheck4DPOrNot)
				nSelectedFrameIndex = nRealFrameIndex % (int)m_dGop;
			else
				nSelectedFrameIndex = nRealFrameIndex;
			//#ifdef FRAMEVIEWER_SPLIT
			//if(pFrame->at(nSelectedFrameIndex)->bLoad)
			if(ESMGetFrameArray().at(nSelectedFrameIndex)->bLoad)
			//#else
			//			if(ESMGetFrameArray().at(nRealFrameIndex)->bLoad)
				//#endif
				//Real Skip
				bSkip = TRUE;
			else
			{
				//ReLoad
				bSkip = FALSE;
			}
		}
		else
		{
#ifdef EXPAND
			m_strPreExpandFile = strExpandPath;
#endif
			m_strPreMovieFile = strImagePath;
			//EnterCriticalSection(&m_cr);
			//SetLoadThread(TRUE);
			//LeaveCriticalSection(&m_cr);
			bSkip = FALSE;
		}

		nSearchIndex =  nRealFrameIndex; // 1sec movie
#else
		nSearchIndex =  nRealFrameIndex % 15; //GOP size
		bSkip = ESMGetFrameArray().at(nSearchIndex)->bLoad;
#endif
		//if(ESMGetGPUDecodeSupport())
		//GTX 1060 Use
		//{
		//	
		//	if(m_strPreMovieFile.CompareNoCase(strImagePath) == 0)
		//		bSkip = TRUE;
		//	else
		//	{
		//		m_strPreMovieFile = strImagePath;
		//		bSkip = FALSE;
		//	}

		//	nSearchIndex =  nRealFrameIndex; // 1sec movie
		//}
		//else
		//{
		//	nSearchIndex =  nRealFrameIndex % 15; //GOP size
		//	bSkip = ESMGetFrameArray().at(nSearchIndex)->bLoad;
		//}

		//ESMFrameArray* pData = ESMGetFrameArray().at(nRealFrameIndex);

		/**///영상 사이즈 체크	
		if(/*ESMGetFrameAdjust() &&*/ bSkip)
			ESMSetFrameDecodingFlag(TRUE);

		//여기서 파일체크
		//쓰레드로 파일 유무 체크 및 디코딩

		if(!bSkip || !ESMGetFrameDecodingFlag() /*&& GetLoadThread()*/)
		{
			//jhhan TEST
			nSkipIndex  = nSearchIndex;

			nSkipCount = 0;

			//#ifdef FRAMEVIEWER_SPLIT
			int nFrameSize = m_dGop;
			if(m_bCheck4DPOrNot)
				nFrameSize = 30;

			int nCheck = GetVideoSize(strImagePath,&nSrcWidth,&nSrcHeight, nFrameSize);
			int nFileExist = 1;
			if(bCheckFile && nCheck != 1)
			{
				//파일 존재하지만, FFMPeg에서 Open을 하지 못할 경우
				//1.5초 동안 20ms간격  파일 체크
				for(int i = 0 ; i < 75; i++)
				{
					Sleep(20);

					nFileExist = GetVideoSize(strImagePath,&nSrcWidth,&nSrcHeight, nFrameSize);

					//중간에 파일이 있을 경우
					if(nFileExist != -1)
					{
						ESMLog(5,_T("FrameViewer Waiting Time: %d"),i*20);
						break;
					}
				}
			}

			if(nFileExist != 1)
			{
				ESMLog(5,_T("Cannot Open File [%s]"),strImagePath);
				return -1;
			}

			/*int nValue =  (nTime / 1000) + ((nTime / 1000) / (movie_frame_per_second-1));*/

			int nNextLoad = 0;
			if(nSkipCount == 0)
				ESMLog(1, _T("[CHECK_A]Load Frame %d , (%dms), %ds"), nSearchIndex, nTime, nTime/1000/*nMovieIndex*//*nValue*/);

			EnterCriticalSection(&m_crFrameLoad);
			ESMInitFrameArray(m_szFrameImage.cx, m_szFrameImage.cy, nSrcWidth, nSrcHeight); //Init Frame Array
			LeaveCriticalSection(&m_crFrameLoad);
#ifndef VIDEOCAPTURE		
			bRet = FFmpegMgr.GetCaptureImage(strImagePath, nRealFrameIndex, ESMGetFrameArray(),nNextLoad);
			if(!ESMGetGPUDecodeSupport())  //ffmpeg 디코딩시 사용
			{
				if(nNextLoad < 10)  // 이전 영상 Decoding
				{
					m_bThreadRun = FALSE;
					int nIndex;
					int nStartIndex, nEndIndex;
					if(nRealFrameIndex - 5 < 0)
						nIndex = 29;
					else
						nIndex = 14;

					nFIdx -= 15;
					strImagePath = ESMGetMoviePath(strDSCID, nFIdx);
					nNextLoad = MSG_PRE_FRAME_DECODING;


					vector<ESMFrameArray*> arrFrame = ESMGetFrameArray();

					//Frame View
					for(int n = 0; n < arrFrame.size(); n++)
					{
						ESMFrameArray* pData = NULL;
						pData = arrFrame.at(n);
						m_frameArrayData.arrFrame.push_back(pData);
					}

					m_frameArrayData.strImagePath = strImagePath;
					m_frameArrayData.nIndex = nIndex;
					m_frameArrayData.nFrameWidth = m_szFrameImage.cx;
					m_frameArrayData.nFrameHeight = m_szFrameImage.cy;
					m_frameArrayData.nMsg = nNextLoad;

					HANDLE hHandle = NULL;
					hHandle = (HANDLE) _beginthreadex(NULL, 0, CatpureImageThread, this, 0, NULL);
					CloseHandle(hHandle);
				}
				else if(nNextLoad > 13) // 이후 영상 Decoding
				{
					m_bThreadRun = FALSE;
					int nIndex;
					int nStartIndex, nEndIndex;
					if(nRealFrameIndex + 5  > 29)
						nIndex = 14;
					else
						nIndex = 29;

					nFIdx += 15;
					strImagePath = ESMGetMoviePath(strDSCID, nFIdx);
					nNextLoad = MSG_NEXT_FRAME_DECODING;


					vector<ESMFrameArray*> arrFrame = ESMGetFrameArray();

					//Frame View
					for(int n = 0; n < arrFrame.size(); n++)
					{	
						ESMFrameArray* pData = NULL;
						pData = arrFrame.at(n);
						m_frameArrayData.arrFrame.push_back(pData);
					}

					m_frameArrayData.strImagePath = strImagePath;
					m_frameArrayData.nIndex = nIndex;
					m_frameArrayData.nFrameWidth = m_szFrameImage.cx;
					m_frameArrayData.nFrameHeight = m_szFrameImage.cy;
					m_frameArrayData.nMsg = nNextLoad;

					HANDLE hHandle = NULL;
					hHandle = (HANDLE) _beginthreadex(NULL, 0, CatpureImageThread, this, 0, NULL);
					CloseHandle(hHandle);
				}
			}
#else

#ifdef EXPAND
			for(int i = 0 ; i < 2; i++)
				m_bThread[i] = FALSE;

			bRet = GetCaptureImageFromVideo(strImagePath,strExpandPath,m_bIsFrameHalf,ESMGetFrameArray());
#else
			m_bThreadRun = FALSE;
			bRet = GetCaptureImageFromVideo(strImagePath,strDSCID,ESMGetFrameArray());
			//bRet = GetCaptureImage(strImagePath);

#endif
			while(1)
			{
				//if(pFrame->at(nSearchIndex)->bLoad)
				if(ESMGetFrameArray().at(nSearchIndex)->bLoad)
				{
					//Success
					bRet = TRUE;
					break;
				}

				//Wait
				//if(pFrame->at(nSearchIndex)->bError == -100)
				if(ESMGetFrameArray().at(nSearchIndex)->bError == -100)
				{
					continue;
				}

				//if(pFrame->at(nSearchIndex)->bError == 0)
				if(ESMGetFrameArray().at(nSearchIndex)->bError == 0)
				{
					bRet = FALSE;
					break;
				}
				Sleep(10);
			}

#endif
		}
		else
		{
			//if(!pFrame->at(nSearchIndex)->bLoad)
			if(!ESMGetFrameArray().at(nSearchIndex)->bLoad)
			{
				ESMLog(5,_T("##1 Not include Image Array"));
				return -1;
			}
			if(nSkipIndex != nSearchIndex)
			{
				nSkipCount = 0;
				nSkipIndex = nSearchIndex;
			}
			if(nSkipCount == 0)
				ESMLog(1, _T("[CHECK_A]Load Skip Frame %d , (%dms)"), nSearchIndex, nTime);
			bRet = TRUE;
		}

		if(bRet == -1) 
		{
			ESMLog(1,_T("File Error [%s]"),strDSCID);
			return -1;
		}

		//		//Start Auto Detecting
		//		track_info stTrack= ESMGetTrackInfo();
		//		BOOL bStart = stTrack.bStart;
		//		m_nCircleSize = stTrack.nCircleSize;
		//
		//		CString strCurID;
		//		BOOL bATCam = FALSE;
		//		for(int i = 0 ; i < 4; i++)
		//		{
		//			strCurID = stTrack.st_AxisInfo[i].strCamID;
		//
		//			if(strCurID == strDSCID)
		//			{
		//				bATCam = TRUE;
		//				break;
		//			}
		//		}
		//		
		//		if(bATCam && m_bSetBallSize && !stTrack.bStart)
		//		{
		//			m_nCircleSize = ATSetBallSize(stTrack,strDSCID,nSearchIndex);
		//			stTrack.nCircleSize = m_nCircleSize;
		//			if(m_nCircleSize > 1)
		//			{
		//				ESMSetTrackInfo(stTrack);
		//				m_bSetBallSize = FALSE;
		//				ESMLog(1,_T("[AutoDetecting] Set Size Finish"));
		//			}
		//			else
		//			{
		//				ESMLog(0,_T("[AutoDetecting] Re-Set Ball Size!!!!"));
		//			}
		//		}
		//		
		selected_frame = nSearchIndex;
		//
		//		if(bATCam && !m_bSetBallSize && ESMGetKeyValue() == VK_F11 && ESMGetValue(ESM_VALUE_AUTODETECT)/*stTrack.bStart*/)
		//		{
		//#ifdef EXPAND
		//			selected_frame = DoAutoDetect(stTrack,nSearchIndex,nMovieIndex,stTrack.bReverse,strDSCID,m_nSkipIdx);
		//#else
		//			selected_frame = DoAutoDetect(stTrack,nSearchIndex,nMovieIndex,stTrack.bReverse,strDSCID);
		//#endif
		//			int nFineMovieIdx = m_nArrMovieIdx[0];
		//			if(selected_frame <= 0)
		//			{
		//				selected_frame = nSearchIndex;
		//
		//				ESMEvent *pMsg = new ESMEvent;
		//				pMsg->message = WM_ESM_VIEW_AUTODETECT_FAIL;
		//				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);		
		//			}
		//			else
		//			{
		//				//jhhan TEST
		//				int nFindIdx = selected_frame;
		//				if(selected_frame >= /*30*/m_nSkipIdx)
		//				{
		//#ifdef EXPAND
		//					nFineMovieIdx = m_nArrMovieIdx[1];
		//					nFindIdx = selected_frame - m_nSkipIdx;
		//#else
		//					//nMovieIndex ++;
		//					//selected_frame = 0;
		//#endif
		//				}
		//#ifdef EXPAND
		//				int nFindTime = 0;
		//				//ESMGetMovieTime(&nFindTime,)
		//				m_nDetectingTime = nFineMovieIdx * 1000 + nFindIdx * /*33*/movie_next_frame_time;
		//				//m_nCurMovieIdx;
		//#else
		//				m_nDetectingTime = nMovieIndex*1000 + nFindIdx * /*33*/movie_next_frame_time;
		//#endif
		//				//m_nDetectingTime = nTime;
		//				stTrack.nTime = m_nDetectingTime;
		//				ESMSetTrackInfo(stTrack);
		//
		///*
		//				
		//				pMsg = new ESMEvent;
		//				pMsg->message = WM_ESM_VIEW_FRMESPOTVIEWON;
		//				pMsg->nParam1 = nTime;
		//				pMsg->nParam2 = ESMGetDSCIndex(strDSCID);
		//				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		//				*/
		//				ESMEvent *pMsg = new ESMEvent;
		//				pMsg->message = WM_ESM_VIEW_AUTODETECT;
		//				pMsg->nParam1 = m_nDetectingTime;
		//				pMsg->nParam2 = ESMGetDSCIndex(strDSCID);
		//
		//				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		//		
		//				//return TRUE;
		//			}
		//		}
		//프레임뷰 
#ifndef VIDEOCAPTURE
		IplImage* img = (IplImage*) ESMGetFrameArray().at((selected_frame))->pImg;
#else
		EnterCriticalSection(&m_crFrameLoad);
		//if(!pFrame->at(selected_frame)->bLoad || pFrame->at(selected_frame)->pImg == NULL)
		if(!ESMGetFrameArray().at(selected_frame)->bLoad || ESMGetFrameArray().at(selected_frame)->pImg == NULL)
		{
			ESMLog(5,_T("##2 Not include Image Array"));
			LeaveCriticalSection(&m_crFrameLoad);
			//while(1)
			//{
			//	ESMLog(5,_T("Reloading..."));				
			//	
			//	if(ESMGetFrameArray().at(selected_frame)->bLoad)
			//	{
			//		if(ESMGetFrameArray().at(selected_frame)->pImg != NULL)
			//			break;
			//	}

			//	Sleep(1);
			//}
			return -1;
		}
		//int nHeight = pFrame->at((selected_frame))->nHeight;
		int nHeight = ESMGetFrameArray().at((selected_frame))->nHeight;
		//int nWidth = pFrame->at((selected_frame))->nWidth;
		int nWidth = ESMGetFrameArray().at((selected_frame))->nWidth;
		int total = nHeight * nWidth * 3;
		IplImage* img = cvCreateImage(cvSize(nWidth,nHeight),8,3);
		//memcpy(img->imageData,(IplImage*) pFrame->at((selected_frame))->pImg,total);
		memcpy(img->imageData,(IplImage*) ESMGetFrameArray().at((selected_frame))->pImg,total);
		LeaveCriticalSection(&m_crFrameLoad);

		////////////////////////////////////////////////////////////////////////// canon selphy	//  [2018/10/2/ stim]
		BOOL bUseCanonSelphy = ESMGetValue(ESM_VALUE_CEREMONY_USE_CANON_SELPHY);
		if (bUseCanonSelphy)
		{
			CString strSelphyPath;
			strSelphyPath.Format(_T("%s\\selphy.jpg"), (ESMGetPath(ESM_PATH_HOME)));
			std::string strTemp; 
			strTemp = std::string(CT2CA(strSelphyPath.operator LPCWSTR()));
			Mat imgSelphy = cvarrToMat(img);
			imwrite(strTemp, imgSelphy);
		}
		//////////////////////////////////////////////////////////////////////////

		if(ESMGetValue(ESM_VALUE_CEREMONYUSE) && ESMGetValue(ESM_VALUE_CEREMONY_STILLIMAGE))
			ESMSetPushTime(nMovieIndex,selected_frame);
#endif
		if(ESMGetReverseMovie())
		{
			int nStart = GetTickCount();
			Mat img2(m_szFrameImage.cy,m_szFrameImage.cx,CV_8UC3);
			resize(cvarrToMat(img),img2,cv::Size(m_szFrameImage.cx,m_szFrameImage.cy),0,0,CV_INTER_AREA);

			cv::Point cvPointData;		
			cvPointData = GetPOSData();

			//wgkim 180512
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);
			CDSCItem* pItem = (CDSCItem*)arDSCList.GetAt(ESMGetDSCIndex(strDSCID));

			m_bVertical = pItem->GetVertical();
			if(pItem != NULL)
			{
				if((pItem->GetVertical() == -1 || pItem->GetVertical() == 1)/* && IsMousePointOnFrameViewer()*/)
				{
					TransImageHorizonToVertical(img2, pItem->GetVertical());
					DrawViewWindowAndCenterLineInVertical(img2);
					pItem->SetPtVerticalCenterPoint(GetVerticalImagePointFromRealSize());

					nSkipCount++;
				}
			}

			if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT) && IsMousePointOnFrameViewer())
			{
				LoadAdjust(strDSCID);
				BOOL bApplyAdjust = AdaptingAdjust(img2,nSrcWidth,nSrcHeight,m_szFrameImage.cx,m_szFrameImage.cy,ESMGetReverseMovie());
				if(bApplyAdjust == FALSE)
					return FALSE;

				nSkipCount++;
			}
			flip(img2,img2,-1);
			
			if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT) && IsMousePointOnFrameViewer())
				DrawViewWindowAndCenterLine(img2);

			SetImageUsingZoomRatio(img2);

			Mat imgA(m_szFrameImage.cy,m_szFrameImage.cx,CV_8UC4);
			cvtColor(img2,imgA,CV_BGR2BGRA);

			pImage->SetBitmapBits(m_szFrameImage.cx*m_szFrameImage.cy*4, imgA.data);
			int nEnd = GetTickCount();
			if(nSkipCount<= 1)
				ESMLog(1, _T("CPU Reverse Frame Time [%d ms]"), nEnd - nStart);
		}
		else
		{
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);

			CDSCItem* pItem = (CDSCItem*)arDSCList.GetAt(ESMGetDSCIndex(strDSCID));
			//CDSCItem*pItem
			if(pItem != NULL)
				if(pItem->m_bReverse)
					cvFlip(img,img,-1);

			Mat img2(m_szFrameImage.cy,m_szFrameImage.cx,CV_8UC3);

			resize(cvarrToMat(img),img2,cv::Size(m_szFrameImage.cx,m_szFrameImage.cy),0,0,CV_INTER_AREA);

			//wgkim 180512
			m_bVertical = pItem->GetVertical();
			if(pItem != NULL)
			{
				if((pItem->GetVertical() == -1 || pItem->GetVertical() == 1)/* && IsMousePointOnFrameViewer()*/)
				{
					TransImageHorizonToVertical(img2, pItem->GetVertical());
					DrawViewWindowAndCenterLineInVertical(img2);
					pItem->SetPtVerticalCenterPoint(GetVerticalImagePointFromRealSize());

					nSkipCount++;
				}
			}

			cv::Point cvPointData;		
			cvPointData = GetPOSData();

			if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT) && IsMousePointOnFrameViewer() )
			{
				LoadAdjust(strDSCID);
				BOOL bApplyAdjust = AdaptingAdjust(img2,nSrcWidth,nSrcHeight,m_szFrameImage.cx,m_szFrameImage.cy,ESMGetReverseMovie());				
				if(bApplyAdjust == FALSE)
					return FALSE;

				DrawViewWindowAndCenterLine(img2);
				nSkipCount++;
			}

			SetImageUsingZoomRatio(img2);

			Mat imgA(m_szFrameImage.cy,m_szFrameImage.cx,CV_8UC4);
			cvtColor(img2,imgA,CV_BGR2BGRA);
			pImage->SetBitmapBits(m_szFrameImage.cx*m_szFrameImage.cy*4, imgA.data);

		}
		cvReleaseImage(&img);

		if(m_pImageList)
		{
			m_pImageList->Replace(nAll, pImage, NULL);
		}

		pImage->DeleteObject();

		if(pImage)
		{
			delete pImage;
			pImage = NULL;
		}
		if( bRet && nSkipCount <= 1)
		{
			ESMLog(5, _T("[CHECK_B]Success Load Movie[%s] [%d]"), strImagePath, GetTickCount());

			//wgkim 180712 
			int nTime_cur = GetRealTime(ESMGetKeyValue());
			if(nTime != nTime_cur)
			{
				_DrawFrame(this);
				return TRUE;
			}
		}
		else if(!bRet && nSkipCount > 1)
			ESMLog(0, _T("Fail Movie[%s]"), strImagePath);

		ESMSetLoadingFrame(FALSE);

	}

	m_nFrameLoadCnt ++;

	return TRUE;
}

void CESMFrameList::SetWhetherMousePointIsOnFrameViewer(BOOL flag)
{
	flagMousePointOnFrameViewer = flag;
}

BOOL CESMFrameList::IsMousePointOnFrameViewer()
{	
	return flagMousePointOnFrameViewer;	
}

void CESMFrameList::SetZoomValueInFrameViewer(int zoomValue)
{
	m_dZoomValue = zoomValue;
	//ESMSetValue(ESM_VALUE_VIEW_RATIO,zoomValue);
}
int CESMFrameList::GetVideoSize(CString strpath,int* nSrcWidth,int* nSrcHeight)
{
	CT2CA pszConvertedAnsiString (strpath);
	string strDst(pszConvertedAnsiString);

	VideoCapture vc(strDst);
	Mat tmp;
	if(!vc.isOpened())
	{
		//ESMLog(1,_T("Cannot Open %s"),strpath);
		for(int i=0;i<30;i++)
		{
			ESMGetFrameArray().at(i)->bLoad = FALSE;
		}
		vc.release();	
		return -1;
	}
	vc>>tmp;
	*nSrcHeight = tmp.rows;
	*nSrcWidth  = tmp.cols;
	vc.release();

	return 1;
}

int CESMFrameList::GetVideoSize(CString strpath,int* nSrcWidth,int* nSrcHeight, int nMaxFrameCount)
{
	CT2CA pszConvertedAnsiString (strpath);
	string strDst(pszConvertedAnsiString);

	VideoCapture vc(strDst);
	Mat tmp;
	if(!vc.isOpened())
	{
		//ESMLog(1,_T("Cannot Open %s"),strpath);
		for(int i=0;i<nMaxFrameCount;i++)
		{
			ESMGetFrameArray().at(i)->bLoad = FALSE;
		}
		vc.release();	
		return -1;
	}
	vc>>tmp;
	*nSrcHeight = tmp.rows;
	*nSrcWidth  = tmp.cols;
	vc.release();

	return 1;
}


BOOL CESMFrameList::GetCaptureImageFromVideo(CString strpath,CString strDSCID,vector<ESMFrameArray*>pImgArr)
{
#if 1
	if(!m_bCheck4DPOrNot)
	{
		//Current Frame Viewer
		HANDLE hSyncTime = NULL;
		ThreadDecoding* pThreadData = new ThreadDecoding;
		pThreadData->pImg = pImgArr;
		pThreadData->strPath = strpath;
		pThreadData->pParent = this;
		hSyncTime = (HANDLE) _beginthreadex(NULL,0,DecodingFrameInFrameViewer, (void*)pThreadData,0,NULL);
		CloseHandle(hSyncTime);
	}
	else
	{
		//Current Frame Viewer
		HANDLE hSyncTime = NULL;
		ThreadDecoding* pThreadData = new ThreadDecoding;
		pThreadData->pImg = pImgArr;
		pThreadData->strPath = strpath;
		pThreadData->pParent = this;
		hSyncTime = (HANDLE) _beginthreadex(NULL,0,DecodingFrame,(void*)pThreadData,0,NULL);
		CloseHandle(hSyncTime);
	}


/*
	//Pre or Next Frame Viewer 
	HANDLE hSyncTime = NULL;
	ThreadDecoding* pThreadData = new ThreadDecoding;
	pThreadData->pImg = pImgArr;
	pThreadData->strPath = strpath;
	pThreadData->pParent = this;
	hSyncTime = (HANDLE) _beginthreadex(NULL,0,DecodingFrame,(void*)pThreadData,0,NULL);
	CloseHandle(hSyncTime);*/

	return TRUE;
#else
	CT2CA pszConvertedAnsiString (strpath);
	string strDst(pszConvertedAnsiString);

	VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		ESMLog(1,_T("No video File at %s"),strpath);
		for(int i=0;i<30;i++)
		{
			pImgArr.at(i)->bLoad = FALSE;
			pImgArr.at(i)->bError = 0;

		}
		vc.release();
		return -1;
	}
	int pCnt = 0;
	Mat img;

	while(1)
	{
		vc>>img;
		if(img.empty()) 
		{
			if(pCnt != 30)
			{
				for(int i = pCnt ; i < 30; i++)
				{
					pImgArr.at(i)->bLoad = FALSE;
					pImgArr.at(i)->bError = 0;
				}
			}
			break;
		}

		pImgArr.at(pCnt)->nHeight = img.rows;
		pImgArr.at(pCnt)->nWidth = img.cols;
		int nImageSize = img.total()*img.channels();
		pImgArr.at(pCnt)->bLoad = TRUE;
		pImgArr.at(pCnt)->bError = 100;

		if(pImgArr.at(pCnt)->pImg == NULL)
		{
			pImgArr.at(pCnt)->pImg = new BYTE [nImageSize];
			memcpy(pImgArr.at(pCnt)->pImg,img.data,nImageSize);
		}

		pCnt++;
	}
	vc.release();

	return TRUE;
#endif
}
unsigned WINAPI CESMFrameList::DecodingFrame(LPVOID param)
{
	ThreadDecoding* pData = (ThreadDecoding*) param;
	CString strpath = pData->strPath;
	vector<ESMFrameArray*> pImgArr = pData->pImg;
	CESMFrameList* pParent = pData->pParent;
	CT2CA pszConvertedAnsiString (strpath);
	string strDst(pszConvertedAnsiString);

	pParent->m_bThreadRun = TRUE;

	int nFrameCnt = 0;
	if(ESMGetFrameRate() == MOVIE_FPS_UHD_30P_X2)
	{
		//180308 최대공간 수정 //180202_C
		/*if(ESMGetRecordFileSplit())
			nFrameCnt = 30;
		else*/
			nFrameCnt = 60;
		
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X2)
	{
		//180308 최대공간 수정 //180202_C
		/*if(ESMGetRecordFileSplit())
			nFrameCnt = 60;
		else*/
			nFrameCnt = 120;
	}else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P) //180308
	{
		nFrameCnt = 50;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
	{
		nFrameCnt = 60;
	}
	else
		nFrameCnt = 30;

	VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		ESMLog(1,_T("No video File at %s"),strpath);
		EnterCriticalSection(&pParent->m_crFrameLoad);
		for(int i=0;i<nFrameCnt;i++)
		{
			pImgArr.at(i)->bLoad = FALSE;
			pImgArr.at(i)->bError = 0;

		}
		LeaveCriticalSection(&pParent->m_crFrameLoad);
		vc.release();
		pParent->m_bThreadRun = FALSE;

		return -1;
	}
	int pCnt = 0;
	Mat img,reimg;

	while(1)
	{
		if(pCnt >= nFrameCnt)
			break;

		vc>>img;
		if(img.empty()) 
		{
			if(pCnt != nFrameCnt)
			{
				for(int i = pCnt ; i < nFrameCnt; i++)
				{
					pImgArr.at(i)->bLoad = FALSE;
					pImgArr.at(i)->bError = 0;
				}
			}
			pParent->m_bThreadRun = FALSE;
			break;
		}
		//새로운 파일이 감지되면 쓰레드 정지
	//	EnterCriticalSection(&pParent->m_cr);
		//if(!pParent->GetLoadThread())
		//	break;
	//	LeaveCriticalSection(&pParent->m_cr);

		//파일 Memory에 저장
		EnterCriticalSection(&pParent->m_crFrameLoad);
		pImgArr.at(pCnt)->nHeight = img.rows;
		pImgArr.at(pCnt)->nWidth = img.cols;
		int nImageSize = img.total()*img.channels();
		pImgArr.at(pCnt)->bLoad = TRUE;
		pImgArr.at(pCnt)->bError = 100;

		if(pImgArr.at(pCnt)->pImg == NULL)
		{
			pImgArr.at(pCnt)->pImg = new BYTE [nImageSize];
			memcpy(pImgArr.at(pCnt)->pImg,img.data,nImageSize);
		}
		LeaveCriticalSection(&pParent->m_crFrameLoad);


		//새로운 파일이 감지되면 쓰레드 정지
	//	EnterCriticalSection(&pParent->m_cr);
		//if(!pParent->GetLoadThread())
		//	break;
	//	LeaveCriticalSection(&pParent->m_cr);

		pCnt++;
	}
	ESMLog(5,_T("[FRAMEVIEW] %d"),pCnt);
	vc.release();

	pParent->m_bThreadRun = FALSE;

	delete pData;
	pData = NULL;
	return TRUE;
}

unsigned WINAPI CESMFrameList::DecodingFrameInFrameViewer(LPVOID param)
{
	ThreadDecoding* pData = (ThreadDecoding*) param;
	CString strpath = pData->strPath;
	vector<ESMFrameArray*> pImgArr = pData->pImg;
	CESMFrameList* pParent = pData->pParent;
	CT2CA pszConvertedAnsiString (strpath);
	string strDst(pszConvertedAnsiString);

	pParent->m_bThreadRun = TRUE;

	int nFrameCnt = 0;
	float dSplitCount = (pParent->m_dDurationTime * pParent->m_dFrameRate) / pParent->m_dGop;
	if(ESMGetFrameRate() == MOVIE_FPS_UHD_30P_X2)
	{
		//180308 최대공간 수정 //180202_C
		/*if(ESMGetRecordFileSplit())
			nFrameCnt = 30;
		else*/
			nFrameCnt = 60;
		
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X2)
	{
		//180308 최대공간 수정 //180202_C
		/*if(ESMGetRecordFileSplit())
			nFrameCnt = 60;
		else*/
			nFrameCnt = 120;
	}else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P) //180308
	{
		nFrameCnt = 50/dSplitCount;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
	{
		nFrameCnt = 60/dSplitCount;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
	{
		nFrameCnt = 30;
	}
	else
		nFrameCnt = 30/dSplitCount;

	VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		ESMLog(1,_T("No video File at %s"),strpath);
		EnterCriticalSection(&pParent->m_crFrameLoad);
		for(int i=0;i<nFrameCnt;i++)
		{
			pImgArr.at(i)->bLoad = FALSE;
			pImgArr.at(i)->bError = 0;

		}
		LeaveCriticalSection(&pParent->m_crFrameLoad);
		vc.release();
		pParent->m_bThreadRun = FALSE;

		return -1;
	}
	int pCnt = 0;
	Mat img,reimg;

	while(1)
	{
		if(pCnt >= nFrameCnt)
			break;

		vc>>img;
		if(img.empty()) 
		{
			if(pCnt != nFrameCnt)
			{
				for(int i = pCnt ; i < nFrameCnt; i++)
				{
					pImgArr.at(i)->bLoad = FALSE;
					pImgArr.at(i)->bError = 0;
				}
			}
			pParent->m_bThreadRun = FALSE;
			break;
		}
		//새로운 파일이 감지되면 쓰레드 정지
	//	EnterCriticalSection(&pParent->m_cr);
		//if(!pParent->GetLoadThread())
		//	break;
	//	LeaveCriticalSection(&pParent->m_cr);

		//파일 Memory에 저장
		EnterCriticalSection(&pParent->m_crFrameLoad);
		pImgArr.at(pCnt)->nHeight = img.rows;
		pImgArr.at(pCnt)->nWidth = img.cols;
		int nImageSize = img.total()*img.channels();
		pImgArr.at(pCnt)->bLoad = TRUE;
		pImgArr.at(pCnt)->bError = 100;

		if(pImgArr.at(pCnt)->pImg == NULL)
		{
			pImgArr.at(pCnt)->pImg = new BYTE [nImageSize];
			memcpy(pImgArr.at(pCnt)->pImg,img.data,nImageSize);
		}
		LeaveCriticalSection(&pParent->m_crFrameLoad);


		//새로운 파일이 감지되면 쓰레드 정지
	//	EnterCriticalSection(&pParent->m_cr);
		//if(!pParent->GetLoadThread())
		//	break;
	//	LeaveCriticalSection(&pParent->m_cr);

		pCnt++;
	}
	ESMLog(5,_T("[FRAMEVIEW] %d"),pCnt);
	vc.release();

	pParent->m_bThreadRun = FALSE;

	delete pData;
	pData = NULL;
	return TRUE;
}

void CESMFrameList::Get_ROI(Mat ori,Mat ROI,int t_x, int t_y)
{
	int i,j;
	int ori_col = ori.cols;
	int col = ROI.cols;
	int row = ROI.rows;

	for(i=0;i<row;i++)
	{
		for(j=0;j<col;j++)
		{
			ROI.data[col*(i*3)+(j*3)] = ori.data[ori_col* ((i+t_y)*3) + ((j+t_x)*3)];
			ROI.data[col*(i*3)+(j*3)+1] = ori.data[ori_col* ((i+t_y)*3) + ((j+t_x)*3)+1];
			ROI.data[col*(i*3)+(j*3)+2] = ori.data[ori_col* ((i+t_y)*3) + ((j+t_x)*3)+2];
		}
	}
}
void CESMFrameList::CpuMoveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void CESMFrameList::CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle, BOOL bReverse)
{
	Mat Iimage2;// = cvCreateImage(cvGetSize(Iimage), IPL_DEPTH_8U, 3);

	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;
	//cvShowImage("2",Iimage);
	CvMat *rot_mat = cvCreateMat( 2, 3, CV_32FC1);
	CvPoint2D32f rot_center = cvPoint2D32f( nCenterX, nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust;// = -1 * (dAngle + 90);
	if(bReverse) //Reverse Movie
		dbAngleAdjust = -1 * (180);   
	else
		dbAngleAdjust = -1 * (dAngle + 90);   //Normal Movie

	//CString strTemp;
	//strTemp.Format(_T("%f"), dbAngleAdjust);
	//m_AdjustDsc2ndList.SetItemText(m_nSelectedNum, 17, strTemp);

	cv2DRotationMatrix( rot_center, dbAngleAdjust, dScale, rot_mat);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), cv::INTER_CUBIC+cv::WARP_FILL_OUTLIERS);
	//Mat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cvWarpAffine(&IplImage(Iimage), &IplImage(Iimage), rot_mat,cv::INTER_LINEAR+cv::WARP_FILL_OUTLIERS);	//d_rotate.copyTo(*gMat);
	//imshow("Iimage",Iimage);
	//cvWaitKey(0);
	//gMat =&(cvarrToMat(Iimage2));//
	//Iimage = cvCreateImage(cvGetSize(&Iimage), IPL_DEPTH_8U, 3);
	cvReleaseMat(&rot_mat);
}
int CESMFrameList::Round(double dData)
{
	int nResult = 0;
	if( dData == 0)
		nResult = 0;
	else if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);

	return nResult;
}

//wgkim@esmlab.com
void CESMFrameList::DrawViewWindowAndCenterLine(Mat img)
{
	ostringstream oss;
	oss << "Set Position Tracking Point(" << m_dZoomValue << ")";
	string text = oss.str();// = "Set Template Point";

	cv::Point textLocation = cv::Point(10, 30);
	int fontFace= 4; double fontScale = 0.8;
	putText(img, text, textLocation, fontFace, fontScale, Scalar::all(255));

	int lineThickness = 2;
	int centerPointLineLength = 250;
	cv::Point viewPoint;
	cv::Point centerPointOfRotation; 

	CPoint viewImageSize = GetImageSize();
	int width = viewImageSize.x;
	int height = viewImageSize.y;

	if(m_pTemplateMgr == NULL)
	{
		ESMLog(0,_T("Template Point Load Error"));
		return;
	}

	if(m_pTemplateMgr->IsOverBoundary())
	{	
		viewPoint = ConvertRealPointToFrameViewerPoint(m_pTemplateMgr->GetModifiedViewPoint());
		centerPointOfRotation= ConvertRealPointToFrameViewerPoint(m_pTemplateMgr->GetModifiedCenterPoint());
	}
	else
	{
		if(!m_pTemplateMgr->IsArrTemplatePointValid() || m_bIsPressedRButton)
		{
			viewPoint = CPointToPoint2f(GetViewPointInFrameViewer());
			centerPointOfRotation= CPointToPoint2f(GetTransPointInFrameViewer());

			if(m_bIsPressedRButton == TRUE)
			{				
				double dbZoomRatio = (double)ESMGetValue(ESM_VALUE_VIEW_RATIO) / 100.0;
				if(dbZoomRatio < 1.0)
					dbZoomRatio = 1.0;

				int nLeft = 0,nTop = 0,nReWidth = 0,nReHeight = 0;

				GetImageRect(dbZoomRatio,m_szFrameImage.cx,m_szFrameImage.cy,nLeft,nTop,nReWidth,nReHeight);

				viewPoint.x = (viewPoint.x / dbZoomRatio) + nLeft;
				viewPoint.y = (viewPoint.y / dbZoomRatio) + nTop;

				centerPointOfRotation.x = (centerPointOfRotation.x / dbZoomRatio) + nLeft;
				centerPointOfRotation.y = (centerPointOfRotation.y / dbZoomRatio) + nTop;
			}
		}
		else
		{
			CString strDsc = GetSelectedDSC();
			int dscIndex = m_pTemplateMgr->GetDscIndexFrom(strDsc);
			
			viewPoint = m_pTemplateMgr->GetTemplatePoint(dscIndex).viewPoint ;					
			centerPointOfRotation= m_pTemplateMgr->GetTemplatePoint(dscIndex).centerPointOfRotation;

			viewPoint.x = viewPoint.x * (width/m_pTemplateMgr->GetWidth());
			viewPoint.y = viewPoint.y * (height/m_pTemplateMgr->GetHeight());

			centerPointOfRotation.x = centerPointOfRotation.x * (width/m_pTemplateMgr->GetWidth());
			centerPointOfRotation.y = centerPointOfRotation.y * (height/m_pTemplateMgr->GetHeight());

			if(GetAsyncKeyState(VK_RBUTTON))
				viewPoint = CPointToPoint2f(GetViewPointInFrameViewer());
			
			if(m_bIsPressedRButton == TRUE)
			{				
				double dbZoomRatio = (double)ESMGetValue(ESM_VALUE_VIEW_RATIO) / 100.0;
				if(dbZoomRatio < 1.0)
					dbZoomRatio = 1.0;

				int nLeft = 0,nTop = 0,nReWidth = 0,nReHeight = 0;

				GetImageRect(dbZoomRatio,m_szFrameImage.cx,m_szFrameImage.cy,nLeft,nTop,nReWidth,nReHeight);

				viewPoint.x = (viewPoint.x / dbZoomRatio) + nLeft;
				viewPoint.y = (viewPoint.y / dbZoomRatio) + nTop;
			}
		}		
	}

	//if(clickFlag == true)
	//	DrawAreaWhereCoordinateMove(img);
	int nRatio = ESMGetValue(ESM_VALUE_VIEW_RATIO);
	if(nRatio < 100)
		nRatio = 100;
	
	double zoomRatio = m_dZoomValue/100./* * 100./(double)nRatio*/;
	
	line(img, cv::Point(viewPoint.x-10, viewPoint.y), cv::Point(viewPoint.x+10, viewPoint.y), Scalar(128, 128, 255), lineThickness, 8, 0);
	line(img, cv::Point(viewPoint.x, viewPoint.y-10), cv::Point(viewPoint.x, viewPoint.y+10), Scalar(128, 128, 255), lineThickness, 8, 0);	

	line(img, cv::Point(viewPoint.x - ((width/zoomRatio)/2), viewPoint.y - ((height/zoomRatio)/2)), 
		cv::Point(viewPoint.x + ((width/zoomRatio)/2), viewPoint.y - ((height/zoomRatio)/2)), Scalar(128, 128, 255), lineThickness, 8, 0);
	line(img, cv::Point(viewPoint.x - ((width/zoomRatio)/2), viewPoint.y + ((height/zoomRatio)/2)), 
		cv::Point(viewPoint.x + ((width/zoomRatio)/2), viewPoint.y + ((height/zoomRatio)/2)), Scalar(128, 128, 255), lineThickness, 8, 0);
	line(img, cv::Point(viewPoint.x - ((width/zoomRatio)/2), viewPoint.y - ((height/zoomRatio)/2)), 
		cv::Point(viewPoint.x - ((width/zoomRatio)/2), viewPoint.y + ((height/zoomRatio)/2)), Scalar(128, 128, 255), lineThickness, 8, 0);
	line(img, cv::Point(viewPoint.x + ((width/zoomRatio)/2), viewPoint.y - ((height/zoomRatio)/2)), 
		cv::Point(viewPoint.x + ((width/zoomRatio)/2), viewPoint.y + ((height/zoomRatio)/2)), Scalar(128, 128, 255), lineThickness, 8, 0);

	line(img, cv::Point(centerPointOfRotation.x, centerPointOfRotation.y-(centerPointLineLength/2)), centerPointOfRotation, Scalar(255, 128, 128), lineThickness, 8, 0);
	LineIterator it(img, centerPointOfRotation, cv::Point(centerPointOfRotation.x, centerPointOfRotation.y+(centerPointLineLength/2)), 8);		
	for(int i = 0; i < it.count; i++,it++)
		if ( i%5!=0 && (i+1)%5!=0) {(*it)[0]/*B*/ = 255;(*it)[1]/*G*/ = 128;(*it)[2]/*R*/ = 128;} 
	circle(img, centerPointOfRotation, 1, Scalar(255, 0, 0), 2, 8);
	ellipse(img, centerPointOfRotation, cvSize(20, 10), 0, 0, 360, Scalar(255, 128, 128), lineThickness);	
}

void CESMFrameList::DrawAreaWhereCoordinateMove(Mat img)
{
	for(int j = 0; j < img.rows; j = j + img.rows/20)
	{
		for(int i = 0; i < img.cols; i = i + img.cols/20)
		{
			CPoint point(i, j);

			SetSelectedRealPoint(GetViewPointInFrameViewer());				
			m_pTemplateMgr->SetViewPoint(CPointToPoint2f(GetSelectedRealPoint()));

			SetTransPointInFrameViewer(point);
			SetSelectedRealPoint(GetTransPointInFrameViewer());	
			Invalidate();

			m_pTemplateMgr->ClearArrTemplatePoint();	
			//m_pTemplateMgr->SetArrTemplatePoint(GetSelectedDSC(), CPointToPoint2f(GetSelectedRealPoint()));	

			for(int k = 0; k < m_pTemplateMgr->GetDscCount(); k++)
			{
				Point2f testPoint = m_pTemplateMgr->GetTemplatePoint(k).viewPoint;

				if(testPoint.x-850 < 0 || testPoint.x+850 > 3840 || testPoint.y-500 < 0 || testPoint.y+500 > 2160)
				{
					circle(img, CPointToPoint2f(point), 1, Scalar(255, 0, 0), 2, 8);
				}
			}

			//img.data[j*img.step + (i*3)+0] = img.data[j*img.step + (i*3)+0]+50;
			//img.data[j*img.step + (i*3)+1] = img.data[j*img.step + (i*3)+1]+50;
			//img.data[j*img.step + (i*3)+2] = img.data[j*img.step + (i*3)+2]+50;
		}
	}

	ESMLog(5,_T("End Point Set"));
}

BOOL CESMFrameList::AdaptingAdjust(Mat img,int nSrcWidth, int nSrcHeight,int m_Width,int m_Height, BOOL reverse)
{
	int nRotateX = 0, nRotateY = 0, nSize = 0;
	double dSize = 0.0;

	double dRatio = 1;
	double dRatioHD_UHD = (double)nSrcWidth/m_pTemplateMgr->GetWidth();
//	nSrcWidth = nSrcWidth * (nSrcWidth /m_pTemplateMgr->m_dWidth.);
//	nSrcHeight= nSrcHeight* (nSrcHeight/m_pTemplateMgr->m_dWidth.);

	if(nSrcWidth != m_Width && nSrcHeight != m_Height)
		dRatio = (double)m_Width / (double)nSrcWidth;

	int nMoveX = 0, nMoveY = 0;
	int nMarginX = m_MarginX * dRatio * dRatioHD_UHD;
	int nMarginY = m_MarginY * dRatio * dRatioHD_UHD;
	
	//Rotation
	nRotateX = Round(adjInfo.AdjptRotate.x * dRatioHD_UHD * dRatio );
	nRotateY = Round(adjInfo.AdjptRotate.y * dRatioHD_UHD * dRatio );
	CpuRotateImage(img, nRotateX ,nRotateY, adjInfo.AdjSize, adjInfo.AdjAngle,0);

	//Move
	nMoveX = Round(adjInfo.AdjMove.x * dRatioHD_UHD * dRatio);
	nMoveY = Round(adjInfo.AdjMove.y * dRatioHD_UHD * dRatio);
	CpuMoveImage(&img, nMoveX,  nMoveY);

	double dbMarginScale = 1 / ( (double)(m_Width- nMarginX * 2) /(double) m_Width);

	if(dbMarginScale < 0 || dbMarginScale > 5)
	{
		ESMLog(0, _T("Margin Error : Adjust can not be applied to a Frame Viewer."));
		return FALSE;
	}

	Mat pMatResize;
	resize(img, pMatResize, cv::Size(m_Width*dbMarginScale ,m_Height*dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );

	int nLeft = (int)((m_Width *dbMarginScale - m_Width )/2);
	int nTop  = (int)((m_Height*dbMarginScale - m_Height)/2);

	Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, m_Width, m_Height));

	pMatCut.copyTo(img);
	
	//wgkim@esmlab.com
	//DrawViewWindowAndCenterLine(img);
	return TRUE;
}

void CESMFrameList::LoadAdjust(CString strDSC)
{
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();

	adjInfo = pMovieMgr->GetAdjustData(strDSC);
	m_MarginX = pMovieMgr->GetMarginX();
	m_MarginY = pMovieMgr->GetMarginY();
}
// GetRotatedBitmap	- Create a new bitmap with rotated image
// Returns		- Returns new bitmap with rotated image
// hBitmap		- Bitmap to rotate
// radians		- Angle of rotation in radians
// clrBack		- Color of pixels in the resulting bitmap that do
//			  not get covered by source pixels
// Note			- If the bitmap uses colors not in the system palette 
//			  then the result is unexpected. You can fix this by
//			  adding an argument for the logical palette.

HBITMAP CESMFrameList::GetRotatedBitmap( HBITMAP hBitmap, float radians, COLORREF clrBack )
{
	// Create a memory DC compatible with the display
	CDC sourceDC, destDC;
	sourceDC.CreateCompatibleDC( NULL );
	destDC.CreateCompatibleDC( NULL );

	// Get logical coordinates
	BITMAP bm;
	::GetObject( hBitmap, sizeof( bm ), &bm );

	float cosine = (float)cos(radians);
	float sine = (float)sin(radians);

	// Compute dimensions of the resulting bitmap
	// First get the coordinates of the 3 corners other than origin
	int x1 = (int)(-bm.bmHeight * sine);
	int y1 = (int)(bm.bmHeight * cosine);
	int x2 = (int)(bm.bmWidth * cosine - bm.bmHeight * sine);
	int y2 = (int)(bm.bmHeight * cosine + bm.bmWidth * sine);
	int x3 = (int)(bm.bmWidth * cosine);
	int y3 = (int)(bm.bmWidth * sine);

	int minx = min(0,min(x1, min(x2,x3)));
	int miny = min(0,min(y1, min(y2,y3)));
	int maxx = max(x1, max(x2,x3));
	int maxy = max(y1, max(y2,y3));

	int w = maxx - minx;
	int h = maxy - miny;


	// Create a bitmap to hold the result
	HBITMAP hbmResult = ::CreateCompatibleBitmap(CClientDC(NULL), w, h);

	HBITMAP hbmOldSource = (HBITMAP)::SelectObject( sourceDC.m_hDC, hBitmap );
	HBITMAP hbmOldDest = (HBITMAP)::SelectObject( destDC.m_hDC, hbmResult );

	// Draw the background color before we change mapping mode
	HBRUSH hbrBack = CreateSolidBrush( clrBack );
	HBRUSH hbrOld = (HBRUSH)::SelectObject( destDC.m_hDC, hbrBack );
	destDC.PatBlt( 0, 0, w, h, PATCOPY );
	::DeleteObject( ::SelectObject( destDC.m_hDC, hbrOld ) );

	// Set mapping mode so that +ve y axis is upwords
	sourceDC.SetMapMode(MM_ISOTROPIC);
	sourceDC.SetWindowExt(1,1);
	sourceDC.SetViewportExt(1,-1);
	sourceDC.SetViewportOrg(0, bm.bmHeight-1);

	destDC.SetMapMode(MM_ISOTROPIC);
	destDC.SetWindowExt(1,1);
	destDC.SetViewportExt(1,-1);
	destDC.SetWindowOrg(minx, maxy);

	// Now do the actual rotating - a pixel at a time
	// Computing the destination point for each source point
	// will leave a few pixels that do not get covered
	// So we use a reverse transform - e.i. compute the source point
	// for each destination point

	for( int y = miny; y < maxy; y++ )
	{
		for( int x = minx; x < maxx; x++ )
		{
			int sourcex = (int)(x*cosine + y*sine);
			int sourcey = (int)(y*cosine - x*sine);
			if( sourcex >= 0 && sourcex < bm.bmWidth && sourcey >= 0
				&& sourcey < bm.bmHeight )destDC.SetPixel(x,y,sourceDC.GetPixel(sourcex,sourcey));
		}
	}// Restore DCs
	::SelectObject( sourceDC.m_hDC, hbmOldSource );
	::SelectObject( destDC.m_hDC, hbmOldDest );

	return hbmResult;
}
BOOL CESMFrameList::LoadImagePicture(CStringArray* pArImage)
{
	CString		strFileName = _T("");
	CBitmap*	pImage		= NULL;	
	Bitmap*		pFrame		= NULL;
	HBITMAP		hBmp		= NULL;
	int nAll = 0;
	//-- Find First File
	m_nSelectedFrame = 0;

	//-- Find First File
	int nCnt = GetImageCnt();
	EnterCriticalSection(&m_cr);
	CString strDSCID = GetSelectedDSC();
	LeaveCriticalSection(&m_cr);

	if(!strDSCID.GetLength())
		return FALSE;

	//-- Get Frame Path
	CESMFileOperation fo;

	if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		strFileName = ESMGetPicturePath(strDSCID);
	else
		strFileName.Format(_T("%s\\%s\\%s.jpg"), ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), strDSCID);
	
	//strFileName.Format(_T("%s\\%s\\%s.jpg"), ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), strDSCID);
	if(!fo.IsFileExist(strFileName))
	{
		ESMLog(5, _T("Load Fail Image[%s]"), strFileName);
		return FALSE;
	}

	ESMLog(5, _T("Load Image[%s]"), strFileName);
	
	pArImage->Add(strFileName);	

	// set the size of the image list
	nAll = (int)pArImage->GetSize();
	CreateImageList(m_szFrameImage.cx, m_szFrameImage.cy);
	m_pImageList->SetImageCount(nAll);

	Bitmap img(strFileName);
	UINT o_height = img.GetHeight();
	UINT o_width = img.GetWidth();
	INT n_width = m_szFrameImage.cx;
	INT n_height = m_szFrameImage.cy;
	double ratio = ((double)o_width) / ((double)o_height);
	if (o_width > o_height) {
		// Resize down by width
		n_height = static_cast<UINT>(((double)n_width) / ratio);
	} else {
		n_width = static_cast<UINT>(n_height * ratio);
	}
	Gdiplus::Bitmap* newBitmap = new Gdiplus::Bitmap(n_width, n_height, img.GetPixelFormat());
	Gdiplus::Graphics graphics(newBitmap);
	graphics.DrawImage(&img, 0, 0, n_width, n_height);
	
	newBitmap->GetHBITMAP(NULL, &hBmp);
	pImage = new CBitmap();		 
	pImage->Attach(hBmp);
	// add bitmap to our image list
	m_pImageList->Replace(0, pImage, NULL);
	if(pImage)
	{
		delete pImage;
		pImage = NULL;
	}
	if(pFrame)
	{
		delete pFrame;
		pFrame = NULL;
	}

	if(!newBitmap)	
	{
		ESMLog(0,_T("Get Frame Image [%s]"),strFileName);
		return FALSE;
	}
	else
	{
		ESMLog(1,_T("Success Frame Image [%s]"),strFileName);
		delete newBitmap;
		newBitmap = NULL;
	}

	return TRUE;
}
CString CESMFrameList::GetFrameFileName(int nBaseTime, int nAddIndex)
{
	CString strName;

	int nSec	= nBaseTime/1000;;
	int nMilli	= nBaseTime - (nSec*1000);

	nMilli += nAddIndex*movie_next_frame_time;
	if(nMilli < 0 )
	{
		nSec--;
		nMilli += 30*movie_next_frame_time;
	}
	else if(nMilli > 1000 - movie_next_frame_time)
	{
		nSec++;
		nMilli -= 30*movie_next_frame_time;
	}

	strName.Format(_T("%d.%03d"),nSec, nMilli);	
	return strName;
}
//------------------------------------------------------------------------------ 
//! @brief		DrawImage()
//! @date		2010-07-07
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void  CESMFrameList::DrawImage(CDC* pDC, CStringArray* pArImage)
{
	int nZoomSize = 0;

	if(!m_pImageList)
		return;

	CDC mDC;
	CBitmap mBitmap;		
	CRect rectAll;
	GetClientRect(&rectAll);	

	mDC.CreateCompatibleDC(pDC);
	mBitmap.CreateCompatibleBitmap(pDC, rectAll.Width(), rectAll.Height()); 
	mDC.SelectObject(&mBitmap);

	//-- Draw BackGround
	mDC.PatBlt(0,0,rectAll.Width(), rectAll.Height(), SRCCOPY);	
	mDC.FillRect(rectAll, &CBrush(COLOR_BASIC_BG_1/*COLOR_BASIC_BG_2*/));

	//-- 2013-09-26 hongsu@esmlab.com
	//-- Set Font 
	SetFrameFont(&mDC);

	int nX, nY;	
	int nMarginX = rectAll.Width()	/ FRAME_MARGIN_DIV_X;
	int nMarginY = rectAll.Height()	/ FRAME_MARGIN_DIV_Y;
	int nImgX	= m_szFrameImage.cx;
	int nImgY	= m_szFrameImage.cy;
	int nImgCnt	= GetImageCnt();

	int nZoomPosX = 0;
	int nZoomPosY = 0;

	int posImgStartX = (int)((rectAll.Width()-(((nImgX+nMarginX)*nImgCnt-nMarginX))) / 2);
	
	EnterCriticalSection(&m_cr);
	CString strCamInfo = GetSelectedDSC();
	LeaveCriticalSection(&m_cr);
		
	for(int i=0; i<nImgCnt; i++)
	{
		mDC.SetTextColor(COLOR_WHITE);
		mDC.SetBkColor(COLOR_BLACK);

		nX = (int)(posImgStartX + (i*(nImgX+nMarginX)));
		nY = (nMarginY/2);
		//-- Draw Selection
		if(m_nSelectedFrame == i ) 	
		{
			CBrush brush;
			brush.CreateSolidBrush(COLOR_YELLOW);
			CRect rect = CRect(nX - 5, nY - 5, nX + nImgX + 5,  nY + nImgY + 5);
			mDC.FillRect(&rect,&brush);
		}
		CPoint tpPoint;
		int tpPointX, tpPointY;
		tpPointX = m_nSrcPosX - m_nPosX;
		tpPointY = m_nSrcPosY - m_nPosY;
		nX =nX - tpPointX;
		nY =nY - tpPointY;
		m_pImageList->Draw (&mDC, i, CPoint(nX, nY ),NULL);
		//2017-05-29 hjcho
		DrawAutoDetectingLine(&mDC,strCamInfo,nX,nY);

		//171124 wgkim
		m_nLeftMargin = nX;
		m_nTopMargin = nY;

		//-- 2013-09-26 hongsu@esmlab.com
		//-- Draw Time
		CString strInfo;		
		if( pArImage->GetSize() > i)
		{
			CString strTime;
			if(m_nDetectingTime > 0)
				strTime.Format(_T("%d.%03d"),m_nDetectingTime/1000,m_nDetectingTime%1000);
			else
			strTime = pArImage->GetAt(i);

			double ntime = _wtof(strTime);

			//-- 2014-09-16 hongsu@esmlab.com
			//-- Show DSC Name 
			EnterCriticalSection(&m_cr);
			CString strDSC = GetSelectedDSC();
			LeaveCriticalSection(&m_cr);
			strInfo.Format(_T(" dsc:[%s] sec:[%s] "),strDSC,strTime);
			
			m_nDetectingTime = 0;
		}

		CRect rtText;
		rtText.bottom	= nY + nImgY + 02;
		rtText.top		= rtText.bottom	- 15;
		rtText.left		= nX;
		rtText.right	= nX + nImgX - 10;

		mDC.SetTextColor(COLOR_WHITE);
		mDC.SetBkColor(COLOR_BLACK);
		mDC.DrawText(strInfo, rtText,  DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	}
	pDC->StretchBlt(0, 0, rectAll.Width()*(100/m_nZoom), rectAll.Height()*(100/m_nZoom), &mDC, 0, 0, rectAll.Width(), rectAll.Height(), SRCCOPY);
	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	//TRACE(_T("DrawImage %d, %d \n"), tpPoint.x, tpPoint.y);
	mDC.DeleteDC();
}
void CESMFrameList::DrawAutoDetectingLine(CDC *mDC,CString strCurCamID,int nX,int nY)
{
	track_info stTrack = ESMGetTrackInfo();

	CString strCurID;
	
	int center_x = 0, center_y = 0;
	double mar_x = 0.0,mar_y = 0.0;
	double row_ratio = 0.0,	col_ratio  = 0.0;

	int nStrX,nStrY,nEndX,nEndY;
	int nMovX,nMovY;
	int nTmpX,nTmpY;

	for(int i = 0 ; i < 4; i++)
	{
		strCurID = stTrack.st_AxisInfo[i].strCamID;

		if(strCurID == strCurCamID)
		{
			int nImgX = m_szFrameImage.cx;
			int nImgY = m_szFrameImage.cy;

			center_x = nImgX / 2;
			center_y = nImgY / 2;

			if(m_nArrRatioCol[i] == 0)
				m_nArrRatioCol[i] = 8;
			
			if(m_nArrRatioRow[i] == 0)
				m_nArrRatioRow[i] = 6;

			mar_x = (double) nImgX / m_nArrRatioCol[i];
			mar_y = (double) nImgY / m_nArrRatioRow[i];

			//double tmp_row = stTrack.;
			double tmp_col = stTrack.nWidth;
			double tmp_row = stTrack.nHeight;


			row_ratio = (double)(tmp_row / (double) nImgY);
			col_ratio = (double)(tmp_col / (double) nImgX);

			nTmpX = stTrack.st_AxisInfo[i].nMovX + m_nArrMov_x[i];
			nTmpY = stTrack.st_AxisInfo[i].nMovY + m_nArrMov_y[i];

			nStrX = (int)(((double)(center_x + nTmpX) - mar_x) + (double)nX);
			nStrY = (int)(((double)(center_y + nTmpY) - mar_y) + (double)nY);
			nEndX = (int)(((double)(center_x + nTmpX) + mar_x) + (double)nX);
			nEndY = (int)(((double)(center_y + nTmpY) + mar_y) + (double)nY);

			DrawLine(mDC,nStrX,nStrY,nEndX,nEndY,nX,nY,nImgX,nImgY);

			stTrack.st_AxisInfo[i].nStartX	= (int)((nStrX - nX) * col_ratio);
			stTrack.st_AxisInfo[i].nStartY	= (int)((nStrY - nY) * row_ratio);
			stTrack.st_AxisInfo[i].nEndX	= (int)((nEndX - nX) * col_ratio);
			stTrack.st_AxisInfo[i].nEndY	= (int)((nEndY - nY) * row_ratio);
			stTrack.st_AxisInfo[i].nMovX	= nTmpX;
			stTrack.st_AxisInfo[i].nMovY	= nTmpY;
			stTrack.st_AxisInfo[i].nScaleX = m_nArrRatioCol[i];
			stTrack.st_AxisInfo[i].nScaleY = m_nArrRatioRow[i];
			//stTrack.st_Left.nScaleX = 

			ESMSetTrackInfo(stTrack);

			m_nArrMov_x[i] = 0;
			m_nArrMov_y[i] = 0;

			break;
		}
		else
			continue;
	}
	
/*
	{
		

		if(strCurCamID == strLeft)
		{
			center_x = nImgX / 2;
			center_y = nImgY / 2;

			mar_x = (double) nImgX / m_nRatioCol_Left;
			mar_y = (double) nImgY / m_nRatioRow_Left;

			//double tmp_row = stTrack.;
			double tmp_col = stTrack.nWidth;
			double tmp_row = stTrack.nHeight;


			row_ratio = (double)(tmp_row / (double) nImgY);
			col_ratio = (double)(tmp_col / (double) nImgX);

			nTmpX = stTrack.st_Left.nMovX + mov_x;
			nTmpY = stTrack.st_Left.nMovY + mov_y;

			nStrX = (int)(((double)(center_x + nTmpX) - mar_x) + (double)nX);
			nStrY = (int)(((double)(center_y + nTmpY) - mar_y) + (double)nY);
			nEndX = (int)(((double)(center_x + nTmpX) + mar_x) + (double)nX);
			nEndY = (int)(((double)(center_y + nTmpY) + mar_y) + (double)nY);

			DrawLine(mDC,nStrX,nStrY,nEndX,nEndY,nX,nY,nImgX,nImgY);
			
			stTrack.st_Left.nStartX	= (int)((nStrX - nX) * col_ratio);
			stTrack.st_Left.nStartY	= (int)((nStrY - nY) * row_ratio);
			stTrack.st_Left.nEndX	= (int)((nEndX - nX) * col_ratio);
			stTrack.st_Left.nEndY	= (int)((nEndY - nY) * row_ratio);
			stTrack.st_Left.nMovX	= nTmpX;
			stTrack.st_Left.nMovY	= nTmpY;
			stTrack.st_Left.nScaleX = m_nRatioCol_Left;
			stTrack.st_Left.nScaleY = m_nRatioRow_Left;
			//stTrack.st_Left.nScaleX = 
			
			ESMSetTrackInfo(stTrack);

			mov_x = 0;
			mov_y = 0;

		}
		else if(strCurCamID == strRight)
		{
			center_x = nImgX / 2;
			center_y = nImgY / 2;

			mar_x = (double) nImgX / m_nRatioCol_Right;
			mar_y = (double) nImgY / m_nRatioRow_Right;

			double tmp_col = stTrack.nWidth;
			double tmp_row = stTrack.nHeight;

			row_ratio = (double)(tmp_row / (double) nImgY);
			col_ratio = (double)(tmp_col / (double) nImgX);

			nTmpX = stTrack.st_Right.nMovX + mov_x1;
			nTmpY = stTrack.st_Right.nMovY + mov_y1;

			nStrX = (int)(((double)(center_x + nTmpX) - mar_x) + (double)nX);
			nStrY = (int)(((double)(center_y + nTmpY) - mar_y) + (double)nY);
			nEndX = (int)(((double)(center_x + nTmpX) + mar_x) + (double)nX);
			nEndY = (int)(((double)(center_y + nTmpY) + mar_y) + (double)nY);

			DrawLine(mDC,nStrX,nStrY,nEndX,nEndY,nX,nY,nImgX,nImgY);

			stTrack.st_Right.nStartX	= (int)((nStrX - nX) * col_ratio);
			stTrack.st_Right.nStartY	= (int)((nStrY - nY) * row_ratio);
			stTrack.st_Right.nEndX	= (int)((nEndX - nX) * col_ratio);
			stTrack.st_Right.nEndY	= (int)((nEndY - nY) * row_ratio);
			stTrack.st_Right.nMovX	= nTmpX;
			stTrack.st_Right.nMovY	= nTmpY;
			stTrack.st_Right.nScaleX = m_nRatioCol_Right;
			stTrack.st_Right.nScaleY = m_nRatioRow_Right;

			ESMSetTrackInfo(stTrack);

			mov_x1 = 0;
			mov_y1 = 0;
		}
	}*/

}
void CESMFrameList::DrawLine(CDC* mDC,int nStrX,int nStrY,int nEndX,int nEndY,int nX,int nY,int nImgX,int nImgY)
{
	if(nStrX < nX) 
	{
		nStrX = nX;
	}
	if(nStrY < nY) 
	{
		nStrY = nY;
	}
	if(nEndX > nImgX + nX)
	{
		nEndX = nImgX + nX;
	}
	if(nEndY > nImgY + nY)
	{
		nEndY = nImgY + nY;
	}

	mDC->MoveTo(nStrX,nStrY);
	mDC->LineTo(nStrX,nEndY);
	mDC->LineTo(nEndX,nEndY);
	mDC->LineTo(nEndX,nStrY);
	mDC->LineTo(nStrX,nStrY);
}
//------------------------------------------------------------------------------ 
//! @brief		DrawBlank()
//! @date		2010-07-07
//! @author		hongsu.jung
//------------------------------------------------------------------------------ 
void  CESMFrameList::DrawBlank(CDC* pDC)
{

	CDC mDC;
	CBitmap mBitmap;		
	CRect rectAll;
	GetClientRect(&rectAll);	

	mDC.CreateCompatibleDC(pDC);
	mBitmap.CreateCompatibleBitmap(pDC, rectAll.Width(), rectAll.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rectAll.Width(), rectAll.Height(), WHITENESS);
	//-- Draw BackGround
	mDC.FillRect(rectAll, &CBrush(COLOR_BASIC_BG_1));

	//-- just draw text
	//-- 2013-09-26 hongsu@esmlab.com
	//-- Set Font 
	SetFrameFont(&mDC);

	mDC.SetTextColor(COLOR_WHITE);
	mDC.SetBkColor(COLOR_BASIC_BG_DARK);
	mDC.DrawText(_T("Not Available Frames"), rectAll,  DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	pDC->BitBlt(0, 0, rectAll.Width(), rectAll.Height(), &mDC, 0, 0, SRCCOPY);	

	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();
}

int CESMFrameList::ATSetBallSize(track_info stTrack,CString strCamID,int nSearchIndex)
{
	ESMLog(1,_T("[AutoDetecting: Set Ball Size!]"));

	int nStartX,nEndX,nStartY,nEndY;
	int nGetStartX,nGetStartY,nGetEndX,nGetEndY;
	int nCircleSize = 0;

	for(int i = 0 ; i < 4; i++)
	{
		CString strCurDSC = ESMGetTrackInfo().st_AxisInfo[i].strCamID;

		if(strCamID == strCurDSC)
		{
			nGetStartX = ESMGetTrackInfo().st_AxisInfo[i].nStartX;
			nGetStartY = ESMGetTrackInfo().st_AxisInfo[i].nStartY;
			nGetEndX   = ESMGetTrackInfo().st_AxisInfo[i].nEndX;
			nGetEndY   = ESMGetTrackInfo().st_AxisInfo[i].nEndY;
		}
	}
/*
	if(strCamID == strCamLeft)
	{
		nGetStartX = ESMGetTrackInfo().st_Left.nStartX;
		nGetStartY = ESMGetTrackInfo().st_Left.nStartY;
		nGetEndX   = ESMGetTrackInfo().st_Left.nEndX;
		nGetEndY   = ESMGetTrackInfo().st_Left.nEndY;
	}
	else
	{
		nGetStartX = ESMGetTrackInfo().st_Right.nStartX;
		nGetStartY = ESMGetTrackInfo().st_Right.nStartY;
		nGetEndX   = ESMGetTrackInfo().st_Right.nEndX;
		nGetEndY   = ESMGetTrackInfo().st_Right.nEndY;
	}
*/

	if (nGetEndX - nGetStartX > 0)
	{
		nStartX = nGetStartX;
		nEndX   = nGetEndX;
	}
	else
	{
		nStartX  = nGetEndX;
		nEndX	= nGetStartX;
	}
	if (nGetEndY - nGetStartY > 0)
	{
		nStartY = nGetStartY;
		nEndY   = nGetEndY;
	}
	else
	{
		nStartY = nGetEndY;
		nEndY = nGetStartY;
	}

	int roi_row = nEndY - nStartY + 1;
	int roi_col = nEndX - nStartX + 1;

	Mat ROI(roi_row,roi_col,CV_8UC3);
	box = cv::Rect(-1,-1,0,0);
	namedWindow("SetSize");
	setMouseCallback("SetSize", Frame_OnEventhandle, &ROI);

	int height = ESMGetFrameArray().at(nSearchIndex)->nHeight;
	int width = ESMGetFrameArray().at(nSearchIndex)->nWidth;

	if(!ESMGetFrameArray().at(nSearchIndex)->bLoad)
	{
		ESMLog(1,_T("Cannot Open Image"));
		return FALSE;
	}
	else
	{
		Mat ori(height,width,CV_8UC3,ESMGetFrameArray().at(nSearchIndex)->pImg);

		Get_ROI(ori,ROI,nStartX,nStartY);

		if(ESMGetReverseMovie())
			flip(ori,ori,-1);

		Mat temp = ROI.clone();

		while(1){
			temp = ROI.clone();
			if(drawing_box) draw_box(temp, box);
			imshow("SetSize", temp);
			if(waitKey(15) == 27)break;
		}
		nCircleSize = box.width * box.height;
		ESMLog(5,_T("Height: %d, Width: %d Area: %d"),box.height,box.width,nCircleSize);
	}
	destroyAllWindows();

	return nCircleSize;
}
int CESMFrameList::DoAutoDetect(track_info stTrack,int nSearchIndex,int nMovieIndex,BOOL bReverse,CString strDSCID,int nExpandSize)
{
	int nStartX,nEndX,nStartY,nEndY;
	int nGetStartX,nGetStartY,nGetEndX,nGetEndY;
	int nCircleSize = 0;

	for(int i = 0 ; i < 4; i++)
	{
		CString strCurDSC = ESMGetTrackInfo().st_AxisInfo[i].strCamID;

		if(strDSCID == strCurDSC)
		{
			nGetStartX = ESMGetTrackInfo().st_AxisInfo[i].nStartX;
			nGetStartY = ESMGetTrackInfo().st_AxisInfo[i].nStartY;
			nGetEndX   = ESMGetTrackInfo().st_AxisInfo[i].nEndX;
			nGetEndY   = ESMGetTrackInfo().st_AxisInfo[i].nEndY;
		}
	}
/*
	if(strDSCID == strCamLeft)
	{
		nGetStartX = ESMGetTrackInfo().st_Left.nStartX;
		nGetStartY = ESMGetTrackInfo().st_Left.nStartY;
		nGetEndX   = ESMGetTrackInfo().st_Left.nEndX;
		nGetEndY   = ESMGetTrackInfo().st_Left.nEndY;
	}
	else
	{
		nGetStartX = ESMGetTrackInfo().st_Right.nStartX;
		nGetStartY = ESMGetTrackInfo().st_Right.nStartY;
		nGetEndX   = ESMGetTrackInfo().st_Right.nEndX;
		nGetEndY   = ESMGetTrackInfo().st_Right.nEndY;
	}*/

	if (nGetEndX - nGetStartX > 0)
	{
		nStartX = nGetStartX;
		nEndX   = nGetEndX;
	}
	else
	{
		nStartX  = nGetEndX;
		nEndX	= nGetStartX;
	}
	if (nGetEndY - nGetStartY > 0)
	{
		nStartY = nGetStartY;
		nEndY   = nGetEndY;
	}
	else
	{
		nStartY = nGetEndY;
		nEndY = nGetStartY;
	}

	int roi_row = nEndY - nStartY + 1;
	int roi_col = nEndX - nStartX + 1;

	if(m_nCircleSize < 1)
	{
		AfxMessageBox(_T("Set a Ball Size!!"));
		return -1;
	}
	
	int nStartIdx,nEndIdx,nCnt = 0;
	int nFindFrame;
	//int nMaxIdx = 

	ESMGetFramePerSec();
	nStartIdx = nSearchIndex - 5;
	nEndIdx = nSearchIndex + 5;

	if(nStartIdx < 0) nStartIdx = 0;
	if(nEndIdx >= movie_frame_per_second) nEndIdx = movie_frame_per_second - 1;

	int nSearchSize = nEndIdx - nStartIdx + 1;
	CESMAutoDetect AutoDetect;
	AutoDetect.SetDetectingInfo(nStartX,nStartY,nEndX,nEndY,m_nCircleSize,bReverse,strDSCID,nSearchSize);

	ESMLog(1,_T("[AutoDetecting] .....Start!]"));

	for(int i=nStartIdx;i<=nEndIdx;i++) //Searching All Image
	{
		//Mat tmp_yuv(height+height/2,width,CV_8UC1,ESMGetFrameArray().at(i)->pYUV);
#if 0

		int height = ESMGetFrameArray().at(i)->nHeight;
		int width = ESMGetFrameArray().at(i)->nWidth;
		//Mat tmp_yuv(height+height/2,width,CV_8UC1,ESMGetFrameArray().at(i)->pYUV);
		Mat BGR(height,width,CV_8UC3,ESMGetFrameArray().at(i)->pImg);
		Mat frame(height,width,CV_8UC1);
		cvtColor(BGR,frame,cv::COLOR_BGR2GRAY);

		if(bReverse)
			flip(frame,frame,-1);

		if(frame.empty() || !ESMGetFrameArray().at(i)->bLoad) continue;
		else
		{
			nFindFrame = AutoDetect.VideoPlay(frame,nCnt);
			nCnt ++;
		}

#else
		if(!ESMGetFrameArray().at(i)->bLoad)
		{
			while(1)
			{
				if(ESMGetFrameArray().at(i)->bLoad)
					break;

				Sleep(10);
			}
		}
		
		/*if(/ *frame.empty() || * /!ESMGetFrameArray().at(i)->bLoad)
		{	
			ESMLog(5,_T("### %d/%d frame Skip"),i,nEndIdx);
			continue;
		}*/

		EnterCriticalSection(&m_crFrameLoad);
		int height = ESMGetFrameArray().at(i)->nHeight;
		int width = ESMGetFrameArray().at(i)->nWidth;
		Mat BGR(height,width,CV_8UC3,ESMGetFrameArray().at(i)->pImg);
		LeaveCriticalSection(&m_crFrameLoad);

		Mat frame(height,width,CV_8UC1);

		if(bReverse)
			flip(frame,frame,-1);

		cvtColor(BGR,frame,cv::COLOR_BGR2GRAY);

		nFindFrame = AutoDetect.VideoPlay(frame,nCnt);
		nCnt ++;

#endif
	}
	//Detecting 후 메모리 해제 및 정확한 프레임 찾기
	nFindFrame = AutoDetect.ATCheckValid(nFindFrame);

	ESMLog(1,_T("[AutoDetecting] .....Finish!"));

	if(nFindFrame == -1)
	{
		ESMLog(1,_T("[AutoDetecting] .....Fail[%d/%d]"),nCnt,nEndIdx - nStartIdx + 1);

		return 0;
	}
	else
	{
		ESMLog(1,_T("[AutoDetecting] .....Succeed! : %d[%d/%d]"),nStartIdx + nFindFrame,nCnt,nEndIdx - nStartIdx + 1);
		int nFindingTime = nMovieIndex*1000 + (nStartIdx + nFindFrame) * 33;
		ESMLog(1,_T("[AutoDetecting] ..... Finding time is %d.%03d"),nFindingTime/1000,nFindingTime%1000);

		return 	nStartIdx + nFindFrame;
	}
}
int CESMFrameList::GetRealTime(char key)
{
	int nTime;

	switch(key)
	{
	case VK_F5:
		nTime = GetSelectedTime(0);
		break;
	case VK_F6:
		nTime = GetSelectedTime(1);
		break;
	case VK_F7:
		nTime = GetSelectedTime(2);
		break;
	case VK_F8:
		nTime = GetSelectedTime(3);
		break;
	case VK_F10:
		nTime = GetSelectedTime(ESMGetViewPointNum());
		break;
	case VK_F11:
		nTime = GetSelectedTime(ESMGetViewPointNum());
		break;
	case VK_F12:
		nTime = GetSelectedTime(ESMGetViewPointNum());
		break;
	default:
		nTime = GetSelectedTime(0);
		break;
	}

	return nTime;
}
BOOL CESMFrameList::GetCaptureImageFromVideo(CString strpath,CString strPre,BOOL bIsPreOrNext,vector<ESMFrameArray *>pImgArr)
{
	int nArrStartIdx[2]={-1,};
	int nArrEndIdx[2] = {-1,};
	CString strArrPath[2] = {_T(""),};
	int nArrDelay[2] = {-1,};
	int nFps/* = 30*/;

	if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P)
		nFps = 25;
	else if(ESMGetFrameRate() == MOVIE_FPS_UHD_25P || ESMGetFrameRate()==MOVIE_FPS_UHD_25P)
		nFps = 24;
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P)
		nFps = 60;
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
		nFps = 120;
	else
		nFps = 30;

	if(!bIsPreOrNext)
	{
		nArrStartIdx[0] = nFps;
		nArrStartIdx[1] = 0;

		nArrEndIdx[0] = nFps*2;
		nArrEndIdx[1] = nFps;

		strArrPath[0] = strpath;
		strArrPath[1] = strPre;
		
		nArrDelay[0] = 0;
		nArrDelay[1] = 200;
	}
	else//뒷파일
	{
		nArrStartIdx[0] = 0;
		nArrStartIdx[1] = nFps;

		nArrEndIdx[0] = nFps;
		nArrEndIdx[1] = nFps*2;

		strArrPath[0] = strpath;
		strArrPath[1] = strPre;

		nArrDelay[0] = 0;
		nArrDelay[1] = 200;
	}

	for(int i = 0 ; i < 2 ; i ++)
	{
		Sleep(nArrDelay[i]);
		ESMLog(5,strArrPath[i]);
		ThreadFrameExpandDecoding* pExpand = new ThreadFrameExpandDecoding;
		pExpand->pImgArr = pImgArr;
		pExpand->pParent = this;
		pExpand->strPath = strArrPath[i];
		pExpand->nStart  = nArrStartIdx[i];
		pExpand->nEnd    = nArrEndIdx[i];
		pExpand->nIdx	 = i;
		pExpand->nDelay  = nArrDelay[i];

		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE) _beginthreadex(NULL,0,DecodingExpandFrame,(void*)pExpand,0,NULL);
		//hSyncTime = (HANDLE) _beginthreadex(NULL,0,DecodingExpandFrame,(void*)pExpand,0,NULL);
		CloseHandle(hSyncTime);
	}

	return TRUE;
}
unsigned WINAPI CESMFrameList::DecodingExpandFrame(LPVOID param)
{
	ThreadFrameExpandDecoding* pData = (ThreadFrameExpandDecoding*) param;
	CESMFrameList* pParent = pData->pParent;
	vector<ESMFrameArray*> pImgArr = pData->pImgArr;
	
	int nStart = pData->nStart;
	int nEnd   = pData->nEnd;
	int nIdx   = pData->nIdx;
	int nDelay = pData->nDelay;

	pParent->SetThreadLoad(FALSE,nIdx);
	//Sleep(nDelay);

	CString strpath = pData->strPath;
	CT2CA pszConvertedAnsiString (strpath);
	string strDst(pszConvertedAnsiString);

	pParent->m_bThread[nIdx] = TRUE;

	int nHeight = 0, nWidth = 0;
	BOOL bLoaded = FALSE;
	int nFileLoad = pParent->GetVideoSize(strpath,&nWidth,&nHeight);
	
	if(nFileLoad == 1)
		bLoaded = TRUE;

	if(nFileLoad == -1)
	{
		for(int i = 0 ; i < 75; i++)
		{
			Sleep(20);
			int nLoad = pParent->GetVideoSize(strpath,&nWidth,&nHeight);
			
			if(nLoad == 1)
			{
				bLoaded = TRUE;
				ESMLog(5,_T("[%d]Decoding Time: %d"),nIdx,i*20);
				break;
			}
		}
	}

	if(bLoaded == FALSE)
	{
		//파일 못읽어옴
		for(int i = nStart; i < nEnd; i++)
		{
			EnterCriticalSection(&pParent->m_crFrameLoad);
			pImgArr.at(i)->bLoad = FALSE;
			pImgArr.at(i)->bError = 0;
			LeaveCriticalSection(&pParent->m_crFrameLoad);
		}
		pParent->m_bThread[nIdx] = FALSE;
		pParent->SetThreadLoad(TRUE,nIdx);
	}
	else
	{
		VideoCapture vc(strDst);
		Mat frame;
		int nImageSize = nHeight * nWidth * 3;
		char strSavePath[100];
		for(int i = nStart; i < nEnd ; i++)
		{
			vc>>frame;
			
			if(frame.empty())
			{
				for(int j = i ; j < nEnd ; j++)
				{
					EnterCriticalSection(&pParent->m_crFrameLoad);
					pImgArr.at(i)->bLoad = FALSE;
					pImgArr.at(i)->bError = 0;
					LeaveCriticalSection(&pParent->m_crFrameLoad);
				}
				pParent->m_bThread[nIdx] = FALSE;
				
				break;
				/*return FALSE;*/
			}
			if(pParent->GetThreadLoad(nIdx))
			{
				EnterCriticalSection(&pParent->m_crFrameLoad);
				for(int j = i ; j < nEnd ; j++)
				{
					pImgArr.at(i)->bLoad = FALSE;
					pImgArr.at(i)->bError = 0;
				}
				LeaveCriticalSection(&pParent->m_crFrameLoad);
				pParent->SetThreadLoad(TRUE,nIdx);
				ESMLog(5,_T("#########ThreadKill[0]"));
				break;
			}
/*
			sprintf(strSavePath,"M:\\%d_%d.jpg",nIdx,i);
			imwrite(strSavePath,frame);*/
			if(pParent->GetThreadLoad(nIdx))
			{
				EnterCriticalSection(&pParent->m_crFrameLoad);
				for(int j = i ; j < nEnd ; j++)
				{
					pImgArr.at(i)->bLoad = FALSE;
					pImgArr.at(i)->bError = 0;
				}
				LeaveCriticalSection(&pParent->m_crFrameLoad);
				pParent->SetThreadLoad(TRUE,nIdx);
				ESMLog(5,_T("#########ThreadKill[1]"));
				break;
			}
			
			EnterCriticalSection(&pParent->m_crFrameLoad);
			pImgArr.at(i)->nHeight = nHeight;
			pImgArr.at(i)->nWidth = nWidth;
			pImgArr.at(i)->bLoad = TRUE;
			pImgArr.at(i)->bError = 100;

			if(pImgArr.at(i)->pImg == NULL)
			{
				pImgArr.at(i)->pImg = new BYTE [nImageSize];
				memcpy(pImgArr.at(i)->pImg,frame.data,nImageSize);
			}

			if(pParent->GetThreadLoad(nIdx))
			{
				LeaveCriticalSection(&pParent->m_crFrameLoad);

				EnterCriticalSection(&pParent->m_crFrameLoad);
				for(int j = i ; j < nEnd ; j++)
				{
					pImgArr.at(i)->bLoad = FALSE;
					pImgArr.at(i)->bError = 0;
				}
				LeaveCriticalSection(&pParent->m_crFrameLoad);
				pParent->SetThreadLoad(TRUE,nIdx);
				ESMLog(5,_T("#########ThreadKill[2]"));
				break;
			}
			LeaveCriticalSection(&pParent->m_crFrameLoad);

			if(pParent->GetThreadLoad(nIdx))
			{
				pParent->SetThreadLoad(TRUE,nIdx);
				ESMLog(5,_T("#########ThreadKill[3]"));
				break;
			}

		}
		vc.release();
	}
	pParent->SetThreadLoad(TRUE,nIdx);
	pParent->m_bThread[nIdx] = FALSE;

	ESMLog(5,_T("[%d] Thread Finish ( %d - %d )"),nIdx,nStart,nEnd);

	delete pData;
	pData = NULL;
	return TRUE;
}
void CESMFrameList::SetImageUsingZoomRatio(Mat &img)
{
	int nZoomValue = ESMGetValue(ESM_VALUE_VIEW_RATIO);
	int nOriWidth  = img.cols;
	int nOriHeight = img.rows; 

	if(nZoomValue < 100)
		return;

	double dbZoomRatio = (double)nZoomValue/100.0;

	if(dbZoomRatio <= 1.0)
		return;

	int nLeft = 0,nTop = 0,nWidth = 0,nHeight = 0;
	
	GetImageRect(dbZoomRatio,nOriWidth,nOriHeight,nLeft,nTop,nWidth,nHeight);

	Mat tmp = (img)(cv::Rect(nLeft,nTop,nWidth,nHeight));
	
	Mat re;
	//imshow("tmp",tmp);
	resize(tmp,re,cv::Size(nOriWidth,nOriHeight),0,0,CV_INTER_AREA);
	
	re.copyTo(img);
	//imshow("iii",img);
}
void CESMFrameList::GetImageRect(double dbZoomRatio,int nOriWidth,int nOriHeight,int &nLeft,int &nTop,int &nWidth,int &nHeight)
{
	//영상 센터
	int nCenterX = nOriWidth  / 2;
	int nCenterY = nOriHeight / 2;

	//비율에 따른 줌 계산
	nLeft = nCenterX - nOriWidth/dbZoomRatio/2;
	nTop  = nCenterY - nOriHeight/dbZoomRatio/2;

	nWidth = (nOriWidth/dbZoomRatio - 1);
	if((nLeft + nWidth) > nOriWidth)
		nLeft = nLeft - ((nLeft + nWidth) - nOriWidth);
	else if(nLeft < 0)
		nLeft = 0;

	nHeight = (nOriHeight/dbZoomRatio - 1);
	if((nTop + nHeight) > nOriHeight)
		nTop = nTop - ((nTop + nHeight) - nOriHeight);
	else if (nTop < 0)
		nTop = 0;
}

//wgkim 180512
void CESMFrameList::TransImageHorizonToVertical(Mat& img, int nVertical)
{
	double degree = 0;
	double resizeRatio = 9./16.;

	if(nVertical == 1)
		degree = 90;
	else if(nVertical == -1)
		degree = -90;	

	Point2f centralPoint = Point2f(img.cols/2, img.rows/2);
	Mat rotationMatrix = getRotationMatrix2D(centralPoint, degree, resizeRatio);
	
	warpAffine(img, img, rotationMatrix, img.size(), cv::INTER_LINEAR|cv::WARP_FILL_OUTLIERS);
}

//wgkim 180512
void CESMFrameList::DrawViewWindowAndCenterLineInVertical(Mat img)
{
	ostringstream oss;
	oss << "Vertical Image";//(" << m_dZoomValue << ")";
	string text = oss.str();// = "Set Template Point";

	cv::Point textLocation = cv::Point(10, 30);
	int fontFace= CV_FONT_HERSHEY_DUPLEX; double fontScale = 0.8;
	putText(img, text, textLocation, fontFace, fontScale, Scalar::all(255));

	int lineThickness = 2;
	int centerPointLineLength = 250;
	cv::Point viewPoint;

	double zoomRatio = pow((double)img.cols/img.rows, 2);// * (m_dZoomValue/100.);
	CPoint viewImageSize = GetImageSize();
	int width = viewImageSize.x;
	int height = viewImageSize.y;

	CPoint ptPointInFrameViewer	= GetVerticalImagePointInFrameViewer();
	CPoint ptPointFromRealSize	= GetVerticalImagePointFromRealSize();

	CPoint ptSquareCenterPoint;
	ptSquareCenterPoint.x = width / 2;
	ptSquareCenterPoint.y = ptPointInFrameViewer.y;

	line(img, 
		cv::Point(	ptSquareCenterPoint.x - (width	/zoomRatio)/2, 
					ptSquareCenterPoint.y - (height	/zoomRatio)/2), 
		cv::Point(	ptSquareCenterPoint.x + (width	/zoomRatio)/2, 
					ptSquareCenterPoint.y - (height	/zoomRatio)/2), 
					Scalar(128, 128, 255), lineThickness, 8, 0);
	line(img, 
		cv::Point(	ptSquareCenterPoint.x - (width	/zoomRatio)/2, 
					ptSquareCenterPoint.y + (height	/zoomRatio)/2), 
		cv::Point(	ptSquareCenterPoint.x + (width	/zoomRatio)/2, 
					ptSquareCenterPoint.y + (height	/zoomRatio)/2), 
					Scalar(128, 128, 255), lineThickness, 8, 0);
	line(img, 
		cv::Point(	ptSquareCenterPoint.x - (width	/zoomRatio)/2,
					ptSquareCenterPoint.y - (height	/zoomRatio)/2), 
		cv::Point(	ptSquareCenterPoint.x - (width	/zoomRatio)/2, 
					ptSquareCenterPoint.y + (height	/zoomRatio)/2), 
					Scalar(128, 128, 255), lineThickness, 8, 0);
	line(img, 
		cv::Point(	ptSquareCenterPoint.x + (width	/zoomRatio)/2, 
					ptSquareCenterPoint.y - (height	/zoomRatio)/2), 
		cv::Point(	ptSquareCenterPoint.x + (width	/zoomRatio)/2, 
					ptSquareCenterPoint.y + (height	/zoomRatio)/2), 
					Scalar(128, 128, 255), lineThickness, 8, 0);

	double dInnerZoomRatio = zoomRatio * (m_dZoomValue/100.);
	if (ptPointInFrameViewer.x < width/2 - (width / zoomRatio)/2/* + (width/dInnerZoomRatio)/2 */)
		ptSquareCenterPoint.x = width/2 - (width / zoomRatio)/2/* + (width/dInnerZoomRatio)/2*/;
	else if	(ptPointInFrameViewer.x > width/2 + (width / zoomRatio)/2/* - (width/dInnerZoomRatio)/2*/)
		ptSquareCenterPoint.x = width/2 + (width / zoomRatio)/2/* - (width/dInnerZoomRatio)/2*/;
	else
		ptSquareCenterPoint.x = ptPointInFrameViewer.x;

	line(img, 
		cv::Point(	ptSquareCenterPoint.x - (width	/dInnerZoomRatio)/2, 
					ptSquareCenterPoint.y - (height	/dInnerZoomRatio)/2), 
		cv::Point(	ptSquareCenterPoint.x + (width	/dInnerZoomRatio)/2, 
					ptSquareCenterPoint.y - (height	/dInnerZoomRatio)/2), 
					Scalar(255, 0, 0), lineThickness, 8, 0);
	line(img, 
		cv::Point(	ptSquareCenterPoint.x - (width	/dInnerZoomRatio)/2, 
					ptSquareCenterPoint.y + (height	/dInnerZoomRatio)/2), 
		cv::Point(	ptSquareCenterPoint.x + (width	/dInnerZoomRatio)/2, 
					ptSquareCenterPoint.y + (height	/dInnerZoomRatio)/2), 
					Scalar(255, 0, 0), lineThickness, 8, 0);
	line(img, 
		cv::Point(	ptSquareCenterPoint.x - (width	/dInnerZoomRatio)/2,
					ptSquareCenterPoint.y - (height	/dInnerZoomRatio)/2), 
		cv::Point(	ptSquareCenterPoint.x - (width	/dInnerZoomRatio)/2, 
					ptSquareCenterPoint.y + (height	/dInnerZoomRatio)/2), 
					Scalar(255, 0, 0), lineThickness, 8, 0);
	line(img, 
		cv::Point(	ptSquareCenterPoint.x + (width	/dInnerZoomRatio)/2, 
					ptSquareCenterPoint.y - (height	/dInnerZoomRatio)/2), 
		cv::Point(	ptSquareCenterPoint.x + (width	/dInnerZoomRatio)/2, 
					ptSquareCenterPoint.y + (height	/dInnerZoomRatio)/2), 
		Scalar(255, 0, 0), lineThickness, 8, 0);

//	ESMLog(5, _T("Vertical Center Point : (%d, %d)"), ptPointFromRealSize.x, ptPointFromRealSize.y);
//	ESMLog(5, _T("Vertical Viewer Point : (%d, %d)"), ptPointInFrameViewer.x, ptPointInFrameViewer.y);
}

int CESMFrameList::SplitVideoOnGop(CString strDscId, double dSplitCount, int nMinute, CString strInputFilePath, CString strOutputFilePath)
{	
#ifdef _WIN64
	char* szInputFileName;
	char* szOutputFileName;

	wchar_t* wCharInputStr;
	wchar_t* wCharOutputStr;
	int      szLength;

	wCharInputStr = strInputFilePath.GetBuffer(strInputFilePath.GetLength());
	szLength = WideCharToMultiByte(CP_ACP, 0, wCharInputStr, -1, NULL, 0, NULL, NULL); 
	szInputFileName = new char[szLength];
	WideCharToMultiByte(CP_ACP, 0, wCharInputStr, -1, szInputFileName, szLength, 0,0);

	AVOutputFormat *ofmt = NULL;
	AVFormatContext *ifmt_ctx = NULL, *ofmt_ctx = NULL;

	AVPacket pkt;
	int ret, i;

	av_register_all();

	if ((ret = avformat_open_input(&ifmt_ctx, szInputFileName, 0, 0)) < 0) {	
		fprintf(stderr, "Could not open input file '%s'", szInputFileName);
		ESMLog(5, _T("[FrameViewer]Could not open %s In 4DA"), strInputFilePath);
		return 0;
	}

	int nCount = (int)dSplitCount;
	for(int i = 0; i < nCount; i++)
	{
		CString strNumber;
		int nNewIndex = nMinute;
		nNewIndex = nMinute * dSplitCount + i;
		float dStartSeconds = m_dDurationTime / dSplitCount * i;
		float dEndSeconds= m_dDurationTime / dSplitCount * (i+1);

		strNumber.Format(_T("%d"), nNewIndex);

		CString strOutputFile;
		CString strReplace = strNumber+_T(".mp4");
		strOutputFile = strOutputFilePath + strDscId + _T("_") + strReplace;
		//strOutputFilePath.Append(strDscId + _T("_") + strReplace);
		
		if(FileExists(strOutputFile))
			continue;

		m_vSplit.push_back(strOutputFile);

		wCharOutputStr = strOutputFile.GetBuffer(strOutputFile.GetLength());
		szLength = WideCharToMultiByte(CP_ACP, 0, wCharOutputStr, -1, NULL, 0, NULL, NULL); 
		szOutputFileName = new char[szLength];
		WideCharToMultiByte(CP_ACP, 0, wCharOutputStr, -1, szOutputFileName, szLength, 0,0);

		avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, szOutputFileName);	
		if (!ofmt_ctx) {		
			fprintf(stderr, "Could not create output context\n");
			ret = AVERROR_UNKNOWN;
			ESMLog(5, _T("[FrameViewer]Could not Access to RamDisk : %s"), strOutputFilePath);
			return 0;
		}		
		ofmt = ofmt_ctx->oformat;

		for (int j = 0; j < ifmt_ctx->nb_streams; j++) {
			if(j == AVMEDIA_TYPE_VIDEO)
			{
				AVStream *in_stream = ifmt_ctx->streams[j];
				AVStream *out_stream = avformat_new_stream(ofmt_ctx, in_stream->codec->codec);
				if (!out_stream) {
					fprintf(stderr, "Failed allocating output stream\n");
					ret = AVERROR_UNKNOWN;
				}

				ret = avcodec_copy_context(out_stream->codec, in_stream->codec);		

				if (ret < 0) {
					fprintf(stderr, "Failed to copy context from input to output stream codec context\n");
				}
				out_stream->codec->codec_tag = 0;
				if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
					out_stream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER;
			}
		}

		av_dump_format(ofmt_ctx, 0, szOutputFileName, 1);

		if (!(ofmt->flags & AVFMT_NOFILE)) {
			ret = avio_open(&ofmt_ctx->pb, szOutputFileName, AVIO_FLAG_WRITE);
			if (ret < 0) {
				fprintf(stderr, "Could not open output file '%s'", szOutputFileName);
			}
		}

		ret = avformat_write_header(ofmt_ctx, NULL);

		if (ret < 0) {
			fprintf(stderr, "Error occurred when opening output file\n");
		}

		ret = av_seek_frame(ifmt_ctx, -1, dStartSeconds*AV_TIME_BASE, AVSEEK_FLAG_ANY);
		if (ret < 0) {
			fprintf(stderr, "Error seek\n");		
		}	

		int64_t *dts_start_from = (int64_t*)malloc(sizeof(int64_t) * ifmt_ctx->nb_streams);
		memset(dts_start_from, 0, sizeof(int64_t) * ifmt_ctx->nb_streams);
		int64_t *pts_start_from = (int64_t*)malloc(sizeof(int64_t) * ifmt_ctx->nb_streams);
		memset(pts_start_from, 0, sizeof(int64_t) * ifmt_ctx->nb_streams);		

		int cnt = 0;
		int cnt_aud = 0;
		while (1) {
			AVStream *in_stream, *out_stream;

			ret = av_read_frame(ifmt_ctx, &pkt);
			if (ret < 0)
				break;

			if(pkt.stream_index == AVMEDIA_TYPE_VIDEO)
			{
				in_stream  = ifmt_ctx->streams[pkt.stream_index];
				out_stream = ofmt_ctx->streams[pkt.stream_index];

				if (av_q2d(in_stream->time_base) * pkt.pts > dEndSeconds) {
					av_free_packet(&pkt);
					break;
				}

				if(cnt == 0)
				{
					if (dts_start_from[pkt.stream_index] == 0)
						dts_start_from[pkt.stream_index] = pkt.dts;	
					if (pts_start_from[pkt.stream_index] == 0) 
						pts_start_from[pkt.stream_index] = pkt.pts;
				}

				/* copy packet */
				pkt.pts = av_rescale_q(pkt.pts - pts_start_from[pkt.stream_index], in_stream->time_base, out_stream->time_base);
				pkt.dts = av_rescale_q(pkt.dts - dts_start_from[pkt.stream_index], in_stream->time_base, out_stream->time_base);
				pkt.pts = pkt.dts;

				pkt.duration = (int)av_rescale_q((int64_t)pkt.duration, in_stream->time_base, out_stream->time_base);			
				pkt.pos = -1;

				/* copy packet */						
				printf("Packet : %d\n", cnt);
				cnt++;

				ret = av_write_frame(ofmt_ctx, &pkt);
				if (ret < 0) {
					fprintf(stderr, "Error muxing packet\n");
					break;
				}
				av_free_packet(&pkt);
			}
		}
		free(dts_start_from);
		free(pts_start_from);

		av_write_trailer(ofmt_ctx);

		/* close output */
		if (ofmt_ctx && !(ofmt->flags & AVFMT_NOFILE))
			avio_closep(&ofmt_ctx->pb);
		avformat_free_context(ofmt_ctx);

		if (ret < 0 && ret != AVERROR_EOF) {
			return 1;
		}
	}
	avformat_close_input(&ifmt_ctx);
#endif
	return 0;
}

BOOL CESMFrameList::GetCaptureImage(CString strPath)
{
	//ESMLog(5, _T("GetCaptureImage : %s"), strPath);

	//int nDir = strPath.ReverseFind('\\') + 1;
	//int nDot = strPath.ReverseFind('.') + 1;
	//CString strTemp = strPath.Mid(nDir, nDot-nDir-1);
	////ESMLog(5, _T("GetCaptureImageEx : %s"), strTemp);
	//CString strId, strIdx;
	//AfxExtractSubString(strId, strTemp, 0, '_');
	//AfxExtractSubString(strIdx, strTemp, 1, '_');

	//CString strPrev, strNext;
	//strPrev = strPath;
	//strNext = strPath;
	//CString strTmp;
	//int nIdx = _ttoi(strIdx);
	//strTmp.Format(_T("%s_%d"),strId, nIdx - 1);
	//strPrev.Replace(strTemp, strTmp);
	//strTmp.Format(_T("%s_%d"),strId, nIdx + 1);
	//strNext.Replace(strTemp, strTmp);
	//ESMLog(5, _T("GetCaptureImageEx : %s"), strPrev);
	//ESMLog(5, _T("GetCaptureImageEx : %s"), strNext);

	vector<ESMFrameArray*> *pData = ESMGetFrame(strPath);

	HANDLE hSyncTime = NULL;
	ThreadDecoding* pThreadData = new ThreadDecoding;
	pThreadData->pData = pData;
	pThreadData->strPath = strPath;
	pThreadData->pParent = this;

	if(!m_bCheck4DPOrNot)
	{
		//Current Frame Viewer
		hSyncTime = (HANDLE) _beginthreadex(NULL,0,DecodeFrameViewerEx, (void*)pThreadData,0,NULL);
		CloseHandle(hSyncTime);
	}
	else
	{
		//Current Frame Viewer
		hSyncTime = (HANDLE) _beginthreadex(NULL,0,DecodingFrameEx,(void*)pThreadData,0,NULL);
		CloseHandle(hSyncTime);
	}
	
	return TRUE;
}

unsigned WINAPI CESMFrameList::DecodingFrameEx(LPVOID param)
{
	ThreadDecoding* pData = (ThreadDecoding*) param;
	CString strpath = pData->strPath;

	//vector<ESMFrameArray*> pImgArr = pData->pImg;
	vector<ESMFrameArray*> *pFrame = pData->pData;

	CESMFrameList* pParent = pData->pParent;
	CT2CA pszConvertedAnsiString (strpath);
	string strDst(pszConvertedAnsiString);

	pParent->m_bThreadRun = TRUE;

	int nFrameCnt = 0;
	if(ESMGetFrameRate() == MOVIE_FPS_UHD_30P_X2)
	{
			nFrameCnt = 60;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X2)
	{
		nFrameCnt = 120;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P) //180308
	{
		nFrameCnt = 50;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
	{
		nFrameCnt = 60;
	}
	else
		nFrameCnt = 30;

	VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		ESMLog(1,_T("No video File at %s"),strpath);
		EnterCriticalSection(&pParent->m_crFrameLoad);
		for(int i=0;i<nFrameCnt;i++)
		{
			pFrame->at(i)->bLoad = FALSE;
			pFrame->at(i)->bError = 0;

		}
		LeaveCriticalSection(&pParent->m_crFrameLoad);
		vc.release();
		pParent->m_bThreadRun = FALSE;

		return -1;
	}
	int pCnt = 0;
	Mat img,reimg;

	while(1)
	{
		if(pCnt >= nFrameCnt)
			break;

		vc>>img;
		if(img.empty()) 
		{
			if(pCnt != nFrameCnt)
			{
				for(int i = pCnt ; i < nFrameCnt; i++)
				{
					pFrame->at(i)->bLoad = FALSE;
					pFrame->at(i)->bError = 0;
				}
			}
			pParent->m_bThreadRun = FALSE;
			break;
		}
		//파일 Memory에 저장
		EnterCriticalSection(&pParent->m_crFrameLoad);
		
		pFrame->at(pCnt)->nHeight = img.rows;
		pFrame->at(pCnt)->nWidth = img.cols;
		
		int nImageSize = img.total()*img.channels();
		
		pFrame->at(pCnt)->bLoad = TRUE;
		pFrame->at(pCnt)->bError = 100;


		
		if(pFrame->at(pCnt)->pImg == NULL)
		{
			pFrame->at(pCnt)->pImg = new BYTE [nImageSize];
			memcpy(pFrame->at(pCnt)->pImg, img.data, nImageSize);
		}
		LeaveCriticalSection(&pParent->m_crFrameLoad);

		pCnt++;
	}
	ESMLog(5,_T("[FRAMEVIEW] %d"),pCnt);
	vc.release();

	pParent->m_bThreadRun = FALSE;

	delete pData;
	pData = NULL;
	return TRUE;
}

unsigned WINAPI CESMFrameList::DecodeFrameViewerEx(LPVOID param)
{
	ThreadDecoding* pData = (ThreadDecoding*) param;
	CString strpath = pData->strPath;
	//vector<ESMFrameArray*> pImgArr = pData->pImg;
	vector<ESMFrameArray*> *pFrame = pData->pData;
	CESMFrameList* pParent = pData->pParent;
	CT2CA pszConvertedAnsiString (strpath);
	string strDst(pszConvertedAnsiString);

	//ESMLog(5, _T("[_Ex_] %s"), strpath);

	pParent->m_bThreadRun = TRUE;

	int nFrameCnt = 0;
	float dSplitCount = (pParent->m_dDurationTime * pParent->m_dFrameRate) / pParent->m_dGop;
	if(ESMGetFrameRate() == MOVIE_FPS_UHD_30P_X2)
	{
		nFrameCnt = 60;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X2)
	{
		nFrameCnt = 120;
	}else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P) //180308
	{
		nFrameCnt = 50/dSplitCount;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
	{
		nFrameCnt = 60/dSplitCount;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
	{
		nFrameCnt = 30;
	}
	else
		nFrameCnt = 30/dSplitCount;

	VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		ESMLog(1,_T("No video File at %s"),strpath);
		EnterCriticalSection(&pParent->m_crFrameLoad);
		for(int i=0;i<nFrameCnt;i++)
		{
			pFrame->at(i)->bLoad = FALSE;
			pFrame->at(i)->bError = 0;

		}
		LeaveCriticalSection(&pParent->m_crFrameLoad);
		vc.release();
		pParent->m_bThreadRun = FALSE;
		return -1;
	}
	int pCnt = 0;
	Mat img,reimg;

	while(1)
	{
		if(pCnt >= nFrameCnt)
			break;

		if(pFrame->at(pCnt)->bLoad == TRUE)
		{
			//ESMLog(5, _T("[_Ex_] %s"), _T("skip...."));
			pCnt++;
			continue;
		}

		vc>>img;
		if(img.empty()) 
		{
			if(pCnt != nFrameCnt)
			{
				for(int i = pCnt ; i < nFrameCnt; i++)
				{
					pFrame->at(i)->bLoad = FALSE;
					pFrame->at(i)->bError = 0;
				}
			}
			pParent->m_bThreadRun = FALSE;
			break;
		}


		//파일 Memory에 저장
		EnterCriticalSection(&pParent->m_crFrameLoad);
		pFrame->at(pCnt)->nHeight = img.rows;
		pFrame->at(pCnt)->nWidth = img.cols;
		int nImageSize = img.total()*img.channels();
		pFrame->at(pCnt)->bLoad = TRUE;
		pFrame->at(pCnt)->bError = 100;

		if(pFrame->at(pCnt)->pImg == NULL)
		{
			pFrame->at(pCnt)->pImg = new BYTE [nImageSize];
			memcpy(pFrame->at(pCnt)->pImg,img.data,nImageSize);
		}
		LeaveCriticalSection(&pParent->m_crFrameLoad);

		pCnt++;
	}

	ESMLog(5,_T("[FRAMEVIEW] %d"),pCnt);
	vc.release();

	pParent->m_bThreadRun = FALSE;

	//190110 _Sub Decode
	CString _strPath = pData->strPath;
	int nDir = _strPath.ReverseFind('\\') + 1;
	int nDot = _strPath.ReverseFind('.') + 1;
	CString strTemp = _strPath.Mid(nDir, nDot-nDir-1);
	//ESMLog(5, _T("GetCaptureImageEx : %s"), strTemp);
	CString strId, strIdx;
	AfxExtractSubString(strId, strTemp, 0, '_');
	AfxExtractSubString(strIdx, strTemp, 1, '_');

	CString strPrev, strNext;
	strPrev = _strPath;
	strNext = _strPath;
	CString strTmp;
	int nIdx = _ttoi(strIdx);
	if( nIdx > 0)
	{
		strTmp.Format(_T("%s_%d"),strId, nIdx - 1);
		if( strPrev.Replace(strTemp, strTmp) !=0 )
		{
			

			//ESMLog(5, _T("GetCaptureImageEx : %s"), strNext);
			vector<ESMFrameArray*> *_pData = ESMGetFrame(strPrev);

			HANDLE hSyncTime = NULL;
			ThreadDecoding* pThreadData = new ThreadDecoding;
			pThreadData->pData = _pData;
			pThreadData->strPath = strPrev;
			pThreadData->pParent = pParent;

			//Current Frame Viewer
			hSyncTime = (HANDLE) _beginthreadex(NULL,0,DecodeFrameViewerSub, (void*)pThreadData,0,NULL);
			CloseHandle(hSyncTime);
		}
		strTmp.Format(_T("%s_%d"),strId, nIdx + 1);
		if( strNext.Replace(strTemp, strTmp) != 0)
		{
			

			//ESMLog(5, _T("GetCaptureImageEx : %s"), strNext);
			vector<ESMFrameArray*> *_pData = ESMGetFrame(strNext);

			HANDLE hSyncTime = NULL;
			ThreadDecoding* pThreadData = new ThreadDecoding;
			pThreadData->pData = _pData;
			pThreadData->strPath = strNext;
			pThreadData->pParent = pParent;

			//Current Frame Viewer
			hSyncTime = (HANDLE) _beginthreadex(NULL,0,DecodeFrameViewerSub, (void*)pThreadData,0,NULL);
			CloseHandle(hSyncTime);

		}
	}
	
	//ESMLog(5, _T("GetCaptureImageEx : %s"), strPrev);
	//ESMLog(5, _T("GetCaptureImageEx : %s"), strNext);
	
	delete pData;
	pData = NULL;

	return TRUE;
}

BOOL CESMFrameList::LoadImageListEx(CDC* pDC, CStringArray* pArImage)
{			
	if(m_bThreadRun)
	{
		while(1)
		{
			ESMLog(1, _T("###########Frame Treading...."));
			Sleep(10);
			if(!m_bThreadRun)
				break;
		}
	}

	CString		strExt		= _T("");
	CString		strName		= _T("");
	CString		strPath		= _T("");
	CString		strImagePath= _T("");
	CBitmap*	pImage		= NULL;	
	BOOL		bRC			= TRUE;
	int			nAll		= 0;

#ifdef FRAMEVIEWER_SPLIT
	//if(!m_bCheck4DPOrNot)
	if(ESMGetFrameRate() != MOVIE_FPS_FHD_120P)
	{
		m_dGop = 20.f;
		m_dFrameRate = 60.f;
		m_dDurationTime = 1.f;

		if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P) //180308
		{
			m_dFrameRate = 50.f;

			if(ESMGetDevice() == SDI_MODEL_NX1)
				m_dGop = 25.f;
			if(ESMGetDevice() == SDI_MODEL_GH5)
				m_dGop = 16.f;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_25P || ESMGetFrameRate() == MOVIE_FPS_UHD_25P)
		{
			m_dFrameRate = 25.f;

			if(ESMGetDevice() == SDI_MODEL_NX1)
				m_dGop = 12.f;
			if(ESMGetDevice() == SDI_MODEL_GH5)
				m_dGop = 8.f;
		}
		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
		{
			m_dFrameRate = 60.f;

			if(ESMGetDevice() == SDI_MODEL_NX1)
				m_dGop = 30.f;
			if(ESMGetDevice() == SDI_MODEL_GH5)
				m_dGop = 20.f;
		}

		else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
		{
			m_dFrameRate = 120.f;

			if(ESMGetDevice() == SDI_MODEL_NX1)
				m_dGop = 30.f;
			if(ESMGetDevice() == SDI_MODEL_GH5)
				m_dGop = 40.f;
		}

		else
		{
			m_dFrameRate = 30.f;

			if(ESMGetDevice() == SDI_MODEL_NX1)
				m_dGop = 15.f;
			if(ESMGetDevice() == SDI_MODEL_GH5)
				m_dGop = 10.f;
		}
	}
#endif
	
	//-- Find First File
	int nCnt = GetImageCnt();
	int nTime = GetRealTime(ESMGetKeyValue());

	if(nTime < 0)
	{
		//ESMLog(5, _T("Image Load Fail NonSelectTime[%d]"), nTime);
		ESMSetLoadingFrame(FALSE);
		return FALSE;
	}
	EnterCriticalSection(&m_cr);
	CString strDSCID = GetSelectedDSC();
	LeaveCriticalSection(&m_cr);
	if(!strDSCID.GetLength())
	{
		//ESMLog(5, _T("Image Load Fail NonSelectDSC[%d]"), nTime);
		ESMSetLoadingFrame(FALSE);
		return FALSE;
	}

	m_bCheck4DPOrNot = ESMGetCheckIf4DPOrNot(ESMGetIPFromDSCIDInFrameViewer(strDSCID));

	//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 ImageLoader에서 Image 얻어온다
	int nFIdx = ESMGetFrameIndex(nTime);
	if(ESMIsImageFromImageLoader(strDSCID, nFIdx))
	{
		ESMLog(5, _T("ImageLoader[%s][%d]"), strDSCID, nFIdx);
		pArImage->Add(GetFrameFileName(nTime, 0));	

		if(m_pImageList)
			m_pImageList->SetImageCount(1);
		pImage = ESMGetImageFromImageLoader(strDSCID, nFIdx, pDC, m_szFrameImage.cx, m_szFrameImage.cy);
		if(m_pImageList)
			m_pImageList->Replace(0, pImage, NULL);

		pImage->DeleteObject();
		if(pImage)
		{
			delete pImage;
			pImage = NULL;
		}
				
		return TRUE;
	}

	strImagePath = ESMGetMoviePath(strDSCID, nFIdx, FALSE, TRUE);

	int nMid = nCnt/2;
	strName = GetFrameFileName(nTime, 0-nMid);
	CESMFileOperation fo;
	BOOL bCheckFile = FALSE;
	if(fo.IsFileExist(strImagePath))
	{
		pArImage->Add(strName);
		bCheckFile = TRUE;
	}
	else
	{
		ESMLog(5, _T("Load Fail Movie[%s]"), strImagePath);
		return FALSE;
	}

	// set the size of the image l6ist
	nAll = (int)pArImage->GetSize();
	if(m_pImageList)
		m_pImageList->SetImageCount(nAll);

	FFmpegManager FFmpegMgr(FALSE);

	int nShiftFrame = 0;
	nShiftFrame = ESMGetShiftFrame(strDSCID);

	nShiftFrame = nShiftFrame + ESMGetFrameIndex(nTime);
	int nMovieIndex = 0, nRealFrameIndex = 0; 
	ESMGetMovieIndex(nShiftFrame, nMovieIndex, nRealFrameIndex);

	//wgkim 170728	
	ESMSetSelectedFrameNumber(nShiftFrame);
		
	BOOL bRet = FALSE;

	vector<ESMFrameArray*> * pFrame = NULL;

	while(nAll--)
	{
		strName = pArImage->GetAt(nAll);
		pImage = new CBitmap();
		pImage->CreateCompatibleBitmap(pDC, m_szFrameImage.cx, m_szFrameImage.cy);

		BOOL bSkip = FALSE;
		int nSearchIndex;

		//wgkim 180514
#ifdef FRAMEVIEWER_SPLIT
		//if(!m_bCheck4DPOrNot)
		if(ESMGetFrameRate() != MOVIE_FPS_FHD_120P)
		{
			int nSearchNum = nRealFrameIndex / m_dGop;
			nRealFrameIndex = nRealFrameIndex % (int)m_dGop;

			CString strFrameViewerSplitVideoPath = ESMGetFrameViewerSplitVideo4dmPath();
			ESMCreateAllDirectories(strFrameViewerSplitVideoPath);

			if(GetFileAttributes(strFrameViewerSplitVideoPath) != 0xFFFFFFFF)
			{
				CString strTok, strTokWithExtension;

				int nStrCount = ESMUtil::GetFindCharCount(strImagePath, '_');

				AfxExtractSubString(strTokWithExtension, strImagePath, nStrCount, '_');
				AfxExtractSubString(strTok, strTokWithExtension, 0, '.');

				int nStrLength = strTok.GetLength();

				float dSplitCount = (m_dDurationTime * m_dFrameRate) / m_dGop;

				if(strImagePath.Find(strTokWithExtension))
				{
					CString strNumber;
					int nNumber = _ttoi(strTok);
					nNumber = nNumber * dSplitCount + nSearchNum;
					float dStartTime = m_dDurationTime / dSplitCount * nSearchNum;
					float dEndTime = m_dDurationTime / dSplitCount * (nSearchNum+1);
					strNumber.Format(_T("%d"), nNumber);

					SplitVideoOnGop(strDSCID, dSplitCount, _ttoi(strTok), strImagePath, strFrameViewerSplitVideoPath);

					SplitVideoExtender(strImagePath, nSearchNum - 1);

					CString strReplace = strNumber+_T(".mp4");
					strFrameViewerSplitVideoPath.Append(strDSCID + _T("_") + strReplace);
					strImagePath = strFrameViewerSplitVideoPath;
				}
			}
			else
			{
				ESMLog(5, _T("[FrameViewer]not Exist File in Ramdisk Path"));
				return 0;
			}
		}
#endif
		///////////////////////////////////////

		//190107
		g_mtxDraw.Lock();

		pFrame = ESMGetFrame(strImagePath);

		if(m_strPreMovieFile.CompareNoCase(strImagePath) == 0)
		
		{
			int nSelectedFrameIndex;
			if(!m_bCheck4DPOrNot)
				nSelectedFrameIndex = nRealFrameIndex % (int)m_dGop;
			else
				nSelectedFrameIndex = nRealFrameIndex;
			
			if(pFrame->at(nSelectedFrameIndex)->bLoad)
			{
				bSkip = TRUE;
			}
			else
			{
				//ReLoad
				bSkip = FALSE;
			}
		}
		else
		{
			m_strPreMovieFile = strImagePath;
			bSkip = FALSE;
		}

		nSearchIndex =  nRealFrameIndex; // 1sec movie

		/**///영상 사이즈 체크	
		if(/*ESMGetFrameAdjust() &&*/ bSkip)
			ESMSetFrameDecodingFlag(TRUE);

		//여기서 파일체크
		//쓰레드로 파일 유무 체크 및 디코딩

		if(!bSkip || !ESMGetFrameDecodingFlag() /*&& GetLoadThread()*/)
		{
			//jhhan TEST
			nSkipIndex  = nSearchIndex;

			nSkipCount = 0;

			//#ifdef FRAMEVIEWER_SPLIT
			int nFrameSize = m_dGop;
			if(m_bCheck4DPOrNot)
				nFrameSize = 30;

			int nCheck = GetVideoSize(strImagePath,&nSrcWidth,&nSrcHeight, nFrameSize);
			int nFileExist = 1;
			if(bCheckFile && nCheck != 1)
			{
				//파일 존재하지만, FFMPeg에서 Open을 하지 못할 경우
				//1.5초 동안 20ms간격  파일 체크
				for(int i = 0 ; i < 75; i++)
				{
					Sleep(20);

					nFileExist = GetVideoSize(strImagePath,&nSrcWidth,&nSrcHeight, nFrameSize);

					//중간에 파일이 있을 경우
					if(nFileExist != -1)
					{
						ESMLog(5,_T("FrameViewer Waiting Time: %d"),i*20);
						break;
					}
				}
			}

			if(nFileExist != 1)
			{
				ESMLog(5,_T("Cannot Open File [%s]"),strImagePath);
				g_mtxDraw.Unlock();
				return -1;
			}

			/*int nValue =  (nTime / 1000) + ((nTime / 1000) / (movie_frame_per_second-1));*/

			int nNextLoad = 0;
			if(nSkipCount == 0)
				ESMLog(1, _T("[CHECK_A]Load Frame %d , (%dms), %ds"), nSearchIndex, nTime, nTime/1000/*nMovieIndex*//*nValue*/);

			EnterCriticalSection(&m_crFrameLoad);
			ESMInitFrameArray(m_szFrameImage.cx, m_szFrameImage.cy, nSrcWidth, nSrcHeight); //Init Frame Array
			LeaveCriticalSection(&m_crFrameLoad);


			m_bThreadRun = FALSE;
			bRet = GetCaptureImage(strImagePath);

			while(1)
			{
				if(pFrame->at(nSearchIndex)->bLoad)
				{
					//Success
					bRet = TRUE;
					break;
				}

				//Wait
				if(pFrame->at(nSearchIndex)->bError == -100)
				{
					continue;
				}

				if(pFrame->at(nSearchIndex)->bError == 0)
				{
					bRet = FALSE;
					break;
				}
				Sleep(10);
			}
		}
		else
		{
			if(!pFrame->at(nSearchIndex)->bLoad)
			{
				ESMLog(5,_T("##1 Not include Image Array"));
				return -1;
			}
			if(nSkipIndex != nSearchIndex)
			{
				nSkipCount = 0;
				nSkipIndex = nSearchIndex;
			}
			if(nSkipCount == 0)
				ESMLog(1, _T("[CHECK_A]Load Skip Frame %d , (%dms)"), nSearchIndex, nTime);
			bRet = TRUE;
		}

		if(bRet == -1) 
		{
			ESMLog(1,_T("File Error [%s]"),strDSCID);
			return -1;
		}
				
		selected_frame = nSearchIndex;
		
		//프레임뷰 

		EnterCriticalSection(&m_crFrameLoad);
		if(!pFrame->at(selected_frame)->bLoad || pFrame->at(selected_frame)->pImg == NULL)
		{
			ESMLog(5,_T("##2 Not include Image Array"));
			LeaveCriticalSection(&m_crFrameLoad);
			g_mtxDraw.Unlock();
			return -1;
		}
		int nHeight = pFrame->at((selected_frame))->nHeight;
		int nWidth = pFrame->at((selected_frame))->nWidth;
		int total = nHeight * nWidth * 3;
		IplImage* img = cvCreateImage(cvSize(nWidth,nHeight),8,3);
		memcpy(img->imageData,(IplImage*) pFrame->at((selected_frame))->pImg,total);
		LeaveCriticalSection(&m_crFrameLoad);
		g_mtxDraw.Unlock();

		////////////////////////////////////////////////////////////////////////// canon selphy	//  [2018/10/2/ stim]
		BOOL bUseCanonSelphy = ESMGetValue(ESM_VALUE_CEREMONY_USE_CANON_SELPHY);
		if (bUseCanonSelphy)
		{
			CString strSelphyPath;
			strSelphyPath.Format(_T("%s\\selphy.jpg"), (ESMGetPath(ESM_PATH_HOME)));
			std::string strTemp; 
			strTemp = std::string(CT2CA(strSelphyPath.operator LPCWSTR()));
			Mat imgSelphy = cvarrToMat(img);
			imwrite(strTemp, imgSelphy);
		}
		//////////////////////////////////////////////////////////////////////////

		if(ESMGetValue(ESM_VALUE_CEREMONYUSE) && ESMGetValue(ESM_VALUE_CEREMONY_STILLIMAGE))
			ESMSetPushTime(nMovieIndex,selected_frame);

		if(ESMGetReverseMovie())
		{
			int nStart = GetTickCount();
			Mat img2(m_szFrameImage.cy,m_szFrameImage.cx,CV_8UC3);
			resize(cvarrToMat(img),img2,cv::Size(m_szFrameImage.cx,m_szFrameImage.cy),0,0,CV_INTER_AREA);

			cv::Point cvPointData;		
			cvPointData = GetPOSData();

			//wgkim 180512
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);
			CDSCItem* pItem = (CDSCItem*)arDSCList.GetAt(ESMGetDSCIndex(strDSCID));

			m_bVertical = pItem->GetVertical();
			if(pItem != NULL)
			{
				if((pItem->GetVertical() == -1 || pItem->GetVertical() == 1)/* && IsMousePointOnFrameViewer()*/)
				{
					TransImageHorizonToVertical(img2, pItem->GetVertical());
					DrawViewWindowAndCenterLineInVertical(img2);
					pItem->SetPtVerticalCenterPoint(GetVerticalImagePointFromRealSize());

					nSkipCount++;
				}
			}

			if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT) && IsMousePointOnFrameViewer())
			{
				LoadAdjust(strDSCID);
				BOOL bApplyAdjust = AdaptingAdjust(img2,nSrcWidth,nSrcHeight,m_szFrameImage.cx,m_szFrameImage.cy,ESMGetReverseMovie());
				if(bApplyAdjust == FALSE)
					return FALSE;

				nSkipCount++;
			}
			flip(img2,img2,-1);

			if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT) && IsMousePointOnFrameViewer())
				DrawViewWindowAndCenterLine(img2);

			SetImageUsingZoomRatio(img2);

			Mat imgA(m_szFrameImage.cy,m_szFrameImage.cx,CV_8UC4);
			cvtColor(img2,imgA,CV_BGR2BGRA);

			pImage->SetBitmapBits(m_szFrameImage.cx*m_szFrameImage.cy*4, imgA.data);
			int nEnd = GetTickCount();
			if(nSkipCount<= 1)
				ESMLog(1, _T("CPU Reverse Frame Time [%d ms]"), nEnd - nStart);
		}
		else
		{
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);

			CDSCItem* pItem = (CDSCItem*)arDSCList.GetAt(ESMGetDSCIndex(strDSCID));
			//CDSCItem*pItem
			if(pItem != NULL)
				if(pItem->m_bReverse)
					cvFlip(img,img,-1);

			Mat img2(m_szFrameImage.cy,m_szFrameImage.cx,CV_8UC3);

			resize(cvarrToMat(img),img2,cv::Size(m_szFrameImage.cx,m_szFrameImage.cy),0,0,CV_INTER_AREA);

			//wgkim 180512
			m_bVertical = pItem->GetVertical();
			if(pItem != NULL)
			{
				if((pItem->GetVertical() == -1 || pItem->GetVertical() == 1)/* && IsMousePointOnFrameViewer()*/)
				{
					TransImageHorizonToVertical(img2, pItem->GetVertical());
					DrawViewWindowAndCenterLineInVertical(img2);
					pItem->SetPtVerticalCenterPoint(GetVerticalImagePointFromRealSize());

					nSkipCount++;
				}
			}

			cv::Point cvPointData;		
			cvPointData = GetPOSData();

			if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT) && IsMousePointOnFrameViewer() )
			{
				LoadAdjust(strDSCID);
				BOOL bApplyAdjust = AdaptingAdjust(img2,nSrcWidth,nSrcHeight,m_szFrameImage.cx,m_szFrameImage.cy,ESMGetReverseMovie());				
				if(bApplyAdjust == FALSE)
					return FALSE;
				DrawViewWindowAndCenterLine(img2);

				nSkipCount++;
			}

			SetImageUsingZoomRatio(img2);

			Mat imgA(m_szFrameImage.cy,m_szFrameImage.cx,CV_8UC4);
			cvtColor(img2,imgA,CV_BGR2BGRA);
			pImage->SetBitmapBits(m_szFrameImage.cx*m_szFrameImage.cy*4, imgA.data);

		}
		cvReleaseImage(&img);

		if(m_pImageList)
		{
			m_pImageList->Replace(nAll, pImage, NULL);
		}

		pImage->DeleteObject();

		if(pImage)
		{
			delete pImage;
			pImage = NULL;
		}
		if( bRet && nSkipCount <= 1)
		{
			ESMLog(5, _T("[CHECK_B]Success Load Movie[%s] [%d]"), strImagePath, GetTickCount());

			//wgkim 180712 
			int nTime_cur = GetRealTime(ESMGetKeyValue());
			if(nTime != nTime_cur)
			{
				_DrawFrame(this);
				return TRUE;
			}
		}
		else if(!bRet && nSkipCount > 1)
			ESMLog(0, _T("Fail Movie[%s]"), strImagePath);

		ESMSetLoadingFrame(FALSE);

	}

	m_nFrameLoadCnt ++;

	return TRUE;
}

unsigned WINAPI CESMFrameList::DecodeFrameViewerSub(LPVOID param)
{
	ThreadDecoding* pData = (ThreadDecoding*) param;
	CString strpath = pData->strPath;
	//vector<ESMFrameArray*> pImgArr = pData->pImg;
	vector<ESMFrameArray*> *pFrame = pData->pData;
	CESMFrameList* pParent = pData->pParent;
	CT2CA pszConvertedAnsiString (strpath);
	string strDst(pszConvertedAnsiString);

	//ESMLog(5, _T("[_Sub_] %s"), strpath);

	//pParent->m_bThreadRun = TRUE;

	int nFrameCnt = 0;
	float dSplitCount = (pParent->m_dDurationTime * pParent->m_dFrameRate) / pParent->m_dGop;
	if(ESMGetFrameRate() == MOVIE_FPS_UHD_30P_X2)
	{
		nFrameCnt = 60;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_UHD_60P_X2)
	{
		nFrameCnt = 120;
	}else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || ESMGetFrameRate() == MOVIE_FPS_UHD_50P) //180308
	{
		nFrameCnt = 50/dSplitCount;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
	{
		nFrameCnt = 60/dSplitCount;
	}
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_120P)
	{
		nFrameCnt = 30;
	}
	else
		nFrameCnt = 30/dSplitCount;
	//g_mtxDraw.Lock();
	VideoCapture vc(strDst);
	if(!vc.isOpened())
	{
		ESMLog(1,_T("No video File at %s"),strpath);
		EnterCriticalSection(&pParent->m_crFrameLoad);
		for(int i=0;i<nFrameCnt;i++)
		{
			pFrame->at(i)->bLoad = FALSE;
			pFrame->at(i)->bError = 0;

		}
		LeaveCriticalSection(&pParent->m_crFrameLoad);
		vc.release();
		//pParent->m_bThreadRun = FALSE;

		//g_mtxDraw.Unlock();

		return -1;
	}
	int pCnt = 0;
	Mat img,reimg;

	while(1)
	{
		if(pCnt >= nFrameCnt)
			break;

		if(pFrame->at(pCnt)->bLoad == TRUE)
		{
			//ESMLog(5, _T("[_Ex_] %s"), _T("skip...."));
			pCnt++;
			continue;
		}

		vc>>img;
		if(img.empty()) 
		{
			if(pCnt != nFrameCnt)
			{
				for(int i = pCnt ; i < nFrameCnt; i++)
				{
					pFrame->at(i)->bLoad = FALSE;
					pFrame->at(i)->bError = 0;
				}
			}
			//pParent->m_bThreadRun = FALSE;
			break;
		}


		//파일 Memory에 저장
		EnterCriticalSection(&pParent->m_crFrameLoad);
		pFrame->at(pCnt)->nHeight = img.rows;
		pFrame->at(pCnt)->nWidth = img.cols;
		int nImageSize = img.total()*img.channels();
		pFrame->at(pCnt)->bLoad = TRUE;
		pFrame->at(pCnt)->bError = 100;

		if(pFrame->at(pCnt)->pImg == NULL)
		{
			pFrame->at(pCnt)->pImg = new BYTE [nImageSize];
			memcpy(pFrame->at(pCnt)->pImg,img.data,nImageSize);
		}
		LeaveCriticalSection(&pParent->m_crFrameLoad);

		pCnt++;
	}

	//g_mtxDraw.Unlock();

	//ESMLog(5,_T("[_Sub_] %d"),pCnt);
	vc.release();

	//pParent->m_bThreadRun = FALSE;

	delete pData;
	pData = NULL;

	return TRUE;
}

unsigned WINAPI CESMFrameList::SplitVideoOnGopEx(LPVOID param)
{	
	ThreadDecoding* pData = (ThreadDecoding*) param;
	CESMFrameList* pParent = pData->pParent;

	CString strFile = pData->strPath;
	CString strDscId;
	CString strTok, strTokWithExtension;

	int nStrCount = ESMUtil::GetFindCharCount(strFile, '_');
	AfxExtractSubString(strTokWithExtension, strFile, nStrCount, '_');
	AfxExtractSubString(strTok, strTokWithExtension, 0, '.');

	int nFile = strFile.ReverseFind('\\') + 1;
	CString strTemp = strFile.Right(strFile.GetLength() - nFile);
	AfxExtractSubString(strDscId, strTemp, 0, '_');

	double dSplitCount = (pParent->m_dDurationTime * pParent->m_dFrameRate) / pParent->m_dGop;; 
	int nMinute = _ttoi(strTok);
	CString strOutputFilePath = ESMGetFrameViewerSplitVideo4dmPath();


#ifdef _WIN64
	char* szInputFileName;
	char* szOutputFileName;

	wchar_t* wCharInputStr;
	wchar_t* wCharOutputStr;
	int      szLength;

	wCharInputStr = strFile.GetBuffer(strFile.GetLength());
	szLength = WideCharToMultiByte(CP_ACP, 0, wCharInputStr, -1, NULL, 0, NULL, NULL); 
	szInputFileName = new char[szLength];
	WideCharToMultiByte(CP_ACP, 0, wCharInputStr, -1, szInputFileName, szLength, 0,0);

	AVOutputFormat *ofmt = NULL;
	AVFormatContext *ifmt_ctx = NULL, *ofmt_ctx = NULL;

	AVPacket pkt;
	int ret, i;

	av_register_all();

	if ((ret = avformat_open_input(&ifmt_ctx, szInputFileName, 0, 0)) < 0) {	
		fprintf(stderr, "Could not open input file '%s'", szInputFileName);
		ESMLog(0, _T("[FrameViewer]Could not open %s In 4DA"), strFile);
		return 0;
	}

	int nCount = (int)dSplitCount;
	for(int i = 0; i < nCount; i++)
	{
		CString strNumber;
		int nNewIndex = nMinute;
		nNewIndex = nMinute * dSplitCount + i;
		float dStartSeconds = pParent->m_dDurationTime / dSplitCount * i;
		float dEndSeconds= pParent->m_dDurationTime / dSplitCount * (i+1);

		strNumber.Format(_T("%d"), nNewIndex);

		CString strOutputFile;
		CString strReplace = strNumber+_T(".mp4");
		strOutputFile = strOutputFilePath + strDscId + _T("_") + strReplace;
		//strOutputFilePath.Append(strDscId + _T("_") + strReplace);
		
		if(FileExists(strOutputFile))
			continue;

		pParent->m_vSplit.push_back(strOutputFile);

		wCharOutputStr = strOutputFile.GetBuffer(strOutputFile.GetLength());
		szLength = WideCharToMultiByte(CP_ACP, 0, wCharOutputStr, -1, NULL, 0, NULL, NULL); 
		szOutputFileName = new char[szLength];
		WideCharToMultiByte(CP_ACP, 0, wCharOutputStr, -1, szOutputFileName, szLength, 0,0);

		avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, szOutputFileName);	
		if (!ofmt_ctx) {		
			fprintf(stderr, "Could not create output context\n");
			ret = AVERROR_UNKNOWN;
			ESMLog(5, _T("[FrameViewer]Could not Access to RamDisk : %s"), strOutputFilePath);
			return 0;
		}		
		ofmt = ofmt_ctx->oformat;

		for (int j = 0; j < ifmt_ctx->nb_streams; j++) {
			if(j == AVMEDIA_TYPE_VIDEO)
			{
				AVStream *in_stream = ifmt_ctx->streams[j];
				AVStream *out_stream = avformat_new_stream(ofmt_ctx, in_stream->codec->codec);
				if (!out_stream) {
					fprintf(stderr, "Failed allocating output stream\n");
					ret = AVERROR_UNKNOWN;
				}

				ret = avcodec_copy_context(out_stream->codec, in_stream->codec);		

				if (ret < 0) {
					fprintf(stderr, "Failed to copy context from input to output stream codec context\n");
				}
				out_stream->codec->codec_tag = 0;
				if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
					out_stream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER;
			}
		}

		av_dump_format(ofmt_ctx, 0, szOutputFileName, 1);

		if (!(ofmt->flags & AVFMT_NOFILE)) {
			ret = avio_open(&ofmt_ctx->pb, szOutputFileName, AVIO_FLAG_WRITE);
			if (ret < 0) {
				fprintf(stderr, "Could not open output file '%s'", szOutputFileName);
			}
		}

		ret = avformat_write_header(ofmt_ctx, NULL);

		if (ret < 0) {
			fprintf(stderr, "Error occurred when opening output file\n");
		}

		ret = av_seek_frame(ifmt_ctx, -1, dStartSeconds*AV_TIME_BASE, AVSEEK_FLAG_ANY);
		if (ret < 0) {
			fprintf(stderr, "Error seek\n");		
		}	

		int64_t *dts_start_from = (int64_t*)malloc(sizeof(int64_t) * ifmt_ctx->nb_streams);
		memset(dts_start_from, 0, sizeof(int64_t) * ifmt_ctx->nb_streams);
		int64_t *pts_start_from = (int64_t*)malloc(sizeof(int64_t) * ifmt_ctx->nb_streams);
		memset(pts_start_from, 0, sizeof(int64_t) * ifmt_ctx->nb_streams);		

		int cnt = 0;
		int cnt_aud = 0;
		while (1) {
			AVStream *in_stream, *out_stream;

			ret = av_read_frame(ifmt_ctx, &pkt);
			if (ret < 0)
				break;

			if(pkt.stream_index == AVMEDIA_TYPE_VIDEO)
			{
				in_stream  = ifmt_ctx->streams[pkt.stream_index];
				out_stream = ofmt_ctx->streams[pkt.stream_index];

				if (av_q2d(in_stream->time_base) * pkt.pts > dEndSeconds) {
					av_free_packet(&pkt);
					break;
				}

				if(cnt == 0)
				{
					if (dts_start_from[pkt.stream_index] == 0)
						dts_start_from[pkt.stream_index] = pkt.dts;	
					if (pts_start_from[pkt.stream_index] == 0) 
						pts_start_from[pkt.stream_index] = pkt.pts;
				}

				/* copy packet */
				pkt.pts = av_rescale_q(pkt.pts - pts_start_from[pkt.stream_index], in_stream->time_base, out_stream->time_base);
				pkt.dts = av_rescale_q(pkt.dts - dts_start_from[pkt.stream_index], in_stream->time_base, out_stream->time_base);
				pkt.pts = pkt.dts;

				pkt.duration = (int)av_rescale_q((int64_t)pkt.duration, in_stream->time_base, out_stream->time_base);			
				pkt.pos = -1;

				/* copy packet */						
				printf("Packet : %d\n", cnt);
				cnt++;

				ret = av_write_frame(ofmt_ctx, &pkt);
				if (ret < 0) {
					fprintf(stderr, "Error muxing packet\n");
					break;
				}
				av_free_packet(&pkt);
			}
		}
		free(dts_start_from);
		free(pts_start_from);

		av_write_trailer(ofmt_ctx);

		/* close output */
		if (ofmt_ctx && !(ofmt->flags & AVFMT_NOFILE))
			avio_closep(&ofmt_ctx->pb);
		avformat_free_context(ofmt_ctx);

		if (ret < 0 && ret != AVERROR_EOF) {
			return 1;
		}
	}
	avformat_close_input(&ifmt_ctx);
#endif

	vector<CString>::iterator it;

	CString strKey;
	if(pParent->m_vSplit.size() > 120)
	{
		while(1)
		{
			it = pParent->m_vSplit.begin();
			strKey = *it;
			pParent->m_vSplit.erase(it);

			CESMFileOperation fo;
			fo.Delete(strKey);
			//ESMDeleteFile(strKey);

			if(pParent->m_vSplit.size() <= 120)
				break;
		}

	}
	return 0;
}

void CESMFrameList::SplitVideoExtender(CString strFile, int nPrevNext /*= 0*/)
{
	
	int nDir = strFile.ReverseFind('\\') + 1;
	int nDot = strFile.ReverseFind('.') + 1;
	CString strTemp = strFile.Mid(nDir, nDot-nDir-1);
	
	CString strId, strIdx;
	AfxExtractSubString(strId, strTemp, 0, '_');
	AfxExtractSubString(strIdx, strTemp, 1, '_');

	CString strSplit;
	strSplit = strFile;
	CString strTmp;
	int nIdx = _ttoi(strIdx);
	
	if(nPrevNext != 0)
	{
		int _nIdx = nIdx + nPrevNext;
		if(_nIdx >= 0)
		{
			strTmp.Format(_T("%s_%d"),strId, _nIdx);
			if(strSplit.Replace(strTemp, strTmp) != 0)
			{
				HANDLE hSyncTime = NULL;
				ThreadDecoding* pThread = new ThreadDecoding;
				pThread->strPath = strSplit;
				pThread->pParent = this;

				hSyncTime = (HANDLE) _beginthreadex(NULL,0,SplitVideoOnGopEx, (void*)pThread, 0, NULL);
				CloseHandle(hSyncTime);

				//ESMLog(5, _T("SplitVideoExtender : %s"), strSplit);
			}
		}
		
	}
}