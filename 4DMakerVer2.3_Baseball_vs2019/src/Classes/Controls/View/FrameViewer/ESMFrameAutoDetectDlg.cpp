#include "stdafx.h"
#include "ESMFrameAutoDetectDlg.h"
#include "ESMFileOperation.h"
IMPLEMENT_DYNAMIC(CESMFrameViewBalltracking, CDialogEx)

	CESMFrameViewBalltracking::CESMFrameViewBalltracking(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMFrameViewBalltracking::IDD, pParent)
	,m_nCheckState(0)
{
	data_info	= NULL;
	m_strArrSection[0] = _T("Right");
	m_strArrSection[1] = _T("Left");
	m_strArrSection[2] = _T("RightH");
	m_strArrSection[3] = _T("LeftH");
}

CESMFrameViewBalltracking::~CESMFrameViewBalltracking()
{

}
void CESMFrameViewBalltracking::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STANDARD_CAM_SEL, m_selector);
	DDX_Control(pDX, IDC_CHECK_REVERSE	 , b_chkReverse);
	DDX_Control(pDX, IDC_RADIO_FHD		 , b_FHD);
	DDX_Control(pDX, IDC_RADIO_UHD		 , b_UHD);
	DDX_Control(pDX, IDC_CHECK_AT_SHOWMOVIE, m_bShowTracking);
}
BOOL CESMFrameViewBalltracking::OnInitDialog()
{
	CDialog::OnInitDialog();
	//LoadData(strPath);
	cam_sel = ESMGetTrackInfo();

	if(!data_info)
	{
		data_info = NULL;
	}
	//ESMGetTrack_Info(trck_info);
	LoadData();

	CString str;
	ESMGetDSCList(&num_info);

	CDSCItem* pItem = NULL;

	for(int i = 0; i <num_info.GetSize();i++)
	{
		pItem = (CDSCItem*)num_info.GetAt(i);
		str.Format(_T("%s"),pItem->GetDeviceDSCID());
		m_selector.AddString(str);
	}

	SetDlgItemText(IDC_AT_EDIT_RIGHT,cam_sel.st_AxisInfo[0].strCamID);
	SetDlgItemText(IDC_AT_EDIT_LEFT,cam_sel.st_AxisInfo[1].strCamID);
	SetDlgItemText(IDC_AT_EDIT_H_RIGHT,cam_sel.st_AxisInfo[2].strCamID);
	SetDlgItemText(IDC_AT_EDIT_H_LEFT,cam_sel.st_AxisInfo[3].strCamID);
	
	if(cam_sel.nWidth == 3840 && cam_sel.nHeight == 2160)
	{	
		b_UHD.SetCheck(TRUE);
		b_FHD.SetCheck(FALSE);
	}
	else
	{
		b_FHD.SetCheck(TRUE);
		b_UHD.SetCheck(FALSE);
	}
	b_chkReverse.SetCheck(cam_sel.bReverse);
	m_bShowTracking.SetCheck(cam_sel.bShowTracking);

	return TRUE;  // return TRUE  unless you set the focus to a control
}
BEGIN_MESSAGE_MAP(CESMFrameViewBalltracking, CDialogEx)
	ON_CBN_SELCHANGE(IDC_STANDARD_CAM_SEL, &CESMFrameViewBalltracking::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDOK2, &CESMFrameViewBalltracking::OnBnClickedOk2)
	ON_BN_CLICKED(IDCANCEL, &CESMFrameViewBalltracking::OnBnClickedCancel)
	ON_CONTROL_RANGE(BN_CLICKED,IDC_RADIO_SEL_FHD,IDC_RADIO_SEL_UHD,&CESMFrameViewBalltracking::OnRadioCheck)
	ON_BN_CLICKED(IDC_AT_BTN_LEFT, &CESMFrameViewBalltracking::OnBnClickedAtBtnLeft)
	ON_BN_CLICKED(IDC_AT_BTN_RIGHT, &CESMFrameViewBalltracking::OnBnClickedAtBtnRight)
	ON_BN_CLICKED(IDC_AT_BTN_H_LEFT, &CESMFrameViewBalltracking::OnBnClickedAtBtnHLeft)
	ON_BN_CLICKED(IDC_AT_BTN_H_RIGHT, &CESMFrameViewBalltracking::OnBnClickedAtBtnHRight)
END_MESSAGE_MAP()
void CESMFrameViewBalltracking::OnCbnSelchangeCombo1()
{
	CString str;
	m_selector.GetLBText(m_selector.GetCurSel(),str);
	m_strNumIdx = str;
}
void CESMFrameViewBalltracking::OnBnClickedOk2()
{
	//cam_sel.num = num_idx;
	CString str;
	GetDlgItem(IDC_AT_EDIT_RIGHT)->GetWindowText(str); 
	cam_sel.st_AxisInfo[0].strCamID = str;

	GetDlgItemText(IDC_AT_EDIT_LEFT,str);
	cam_sel.st_AxisInfo[1].strCamID = str;

	GetDlgItem(IDC_AT_EDIT_H_RIGHT)->GetWindowText(str); 
	cam_sel.st_AxisInfo[2].strCamID = str;

	GetDlgItem(IDC_AT_EDIT_H_LEFT)->GetWindowText(str); 
	cam_sel.st_AxisInfo[3].strCamID = str;

	CButton *chk = (CButton *)GetDlgItem(IDC_CHECK_REVERSE);
	BOOL bState = chk->GetCheck();
	cam_sel.bReverse = bState;
	
	chk = (CButton*)GetDlgItem(IDC_CHECK_AT_SHOWMOVIE);
	cam_sel.bShowTracking = chk->GetCheck();

	ESMSetTrackInfo(cam_sel);

	//Save Info
	SaveData();
	CDialogEx::OnOK();
}
void CESMFrameViewBalltracking::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}
void CESMFrameViewBalltracking::OnRadioCheck(UINT value)
{
	m_nCheckState = GetCheckedRadioButton(IDC_RADIO_SEL_FHD,IDC_RADIO_SEL_UHD);
	if(m_nCheckState == IDC_RADIO_FHD)
	{
		m_nResolution_col = 1920;
		m_nResolution_row = 1080;
		cam_sel.nHeight = 1080;
		cam_sel.nWidth = 1920;
	}
	else if(m_nCheckState == IDC_RADIO_UHD)
	{
		m_nResolution_col = 3840;
		m_nResolution_row = 2160;
		cam_sel.nHeight = 2160;
		cam_sel.nWidth = 3840;
	}
	else
	{
		m_nResolution_col = 0;
		m_nResolution_row = 0;
		cam_sel.nHeight = 0;
		cam_sel.nWidth = 0;
	}
}
void CESMFrameViewBalltracking::OnBnClickedAtBtnLeft()
{
	CString str;
	m_selector.GetLBText(m_selector.GetCurSel(),str);
	SetDlgItemText(IDC_AT_EDIT_LEFT,str);
}
void CESMFrameViewBalltracking::OnBnClickedAtBtnRight()
{
	CString str;
	m_selector.GetLBText(m_selector.GetCurSel(),str);
	SetDlgItemText(IDC_AT_EDIT_RIGHT,str);
}
void CESMFrameViewBalltracking::LoadData()
{
	CESMIni ini;

	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_AUTODETECTING);

	track_info stTrack;
	CString strIni;
	if(ini.SetIniFilename(strFile))
	{
		strIni = ini.GetString(STR_SECTION_INFO,STR_REVERSE);
		stTrack.bReverse = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_INFO,STR_CIRCLE_SIZE);
		stTrack.nCircleSize = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_INFO,STR_RESOLUTION_WIDTH);
		stTrack.nWidth = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_INFO,STR_RESOLUTION_HEIGHT);
		stTrack.nHeight = _ttoi(strIni);

		for(int i = 0 ; i < 4; i++)
		{
			stTrack.st_AxisInfo[i].strCamID = ini.GetString(m_strArrSection[i],STR_CAM);

			strIni = ini.GetString(m_strArrSection[i],STR_START_X);
			stTrack.st_AxisInfo[i].nStartX = _ttoi(strIni);

			strIni = ini.GetString(m_strArrSection[i],STR_START_Y);
			stTrack.st_AxisInfo[i].nStartY = _ttoi(strIni);

			strIni = ini.GetString(m_strArrSection[i],STR_END_X);
			stTrack.st_AxisInfo[i].nEndX = _ttoi(strIni);

			strIni = ini.GetString(m_strArrSection[i],STR_END_Y);
			stTrack.st_AxisInfo[i].nEndY = _ttoi(strIni);

			strIni = ini.GetString(m_strArrSection[i],STR_MOV_X);
			stTrack.st_AxisInfo[i].nMovX = _ttoi(strIni);

			strIni = ini.GetString(m_strArrSection[i],STR_MOV_Y);
			stTrack.st_AxisInfo[i].nMovY = _ttoi(strIni);

			strIni = ini.GetString(m_strArrSection[i],STR_SCL_X);
			stTrack.st_AxisInfo[i].nScaleX = _ttoi(strIni);
			/*if(_ttoi(strIni) == 0)
				strIni.Format(_T("8"));*/
			m_pDlgFrameList->m_nArrRatioCol[i] = _ttoi(strIni);

			strIni = ini.GetString(m_strArrSection[i],STR_SCL_Y);
			stTrack.st_AxisInfo[i].nScaleY = _ttoi(strIni);
			/*if(_ttoi(strIni) == 0)
				strIni.Format(_T("6"));*/
			m_pDlgFrameList->m_nArrRatioRow[i] = _ttoi(strIni);
		}

		/*stTrack.st_Left.strCamID = ini.GetString(STR_SECTION_LEFT,STR_CAM);

		strIni = ini.GetString(STR_SECTION_LEFT,STR_START_X);
		stTrack.st_Left.nStartX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT,STR_START_Y);
		stTrack.st_Left.nStartY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT,STR_END_X);
		stTrack.st_Left.nEndX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT,STR_END_Y);
		stTrack.st_Left.nEndY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT,STR_MOV_X);
		stTrack.st_Left.nMovX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT,STR_MOV_Y);
		stTrack.st_Left.nMovY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT,STR_SCL_X);
		stTrack.st_Left.nScaleX = _ttoi(strIni);
		m_pDlgFrameList->m_nRatioCol_Left = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT,STR_SCL_Y);
		stTrack.st_Left.nScaleY = _ttoi(strIni);
		m_pDlgFrameList->m_nRatioRow_Left = _ttoi(strIni);

		stTrack.st_Right.strCamID = ini.GetString(STR_SECTION_RIGHT,STR_CAM);

		strIni = ini.GetString(STR_SECTION_RIGHT,STR_START_X);
		stTrack.st_Right.nStartX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT,STR_START_Y);
		stTrack.st_Right.nStartY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT,STR_END_X);
		stTrack.st_Right.nEndX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT,STR_END_Y);
		stTrack.st_Right.nEndY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT,STR_MOV_X);
		stTrack.st_Right.nMovX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT,STR_MOV_Y);
		stTrack.st_Right.nMovY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT,STR_SCL_X);
		stTrack.st_Right.nScaleX = _ttoi(strIni);
		m_pDlgFrameList->m_nRatioCol_Right = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT,STR_SCL_Y);
		stTrack.st_Right.nScaleY = _ttoi(strIni);
		m_pDlgFrameList->m_nRatioRow_Right = _ttoi(strIni);



		stTrack.st_LeftH.strCamID = ini.GetString(STR_SECTION_LEFT_H,STR_CAM);

		strIni = ini.GetString(STR_SECTION_LEFT_H,STR_START_X);
		stTrack.st_LeftH.nStartX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT_H,STR_START_Y);
		stTrack.st_LeftH.nStartY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT_H,STR_END_X);
		stTrack.st_LeftH.nEndX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT_H,STR_END_Y);
		stTrack.st_LeftH.nEndY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT_H,STR_MOV_X);
		stTrack.st_LeftH.nMovX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT_H,STR_MOV_Y);
		stTrack.st_LeftH.nMovY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT_H,STR_SCL_X);
		stTrack.st_LeftH.nScaleX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_LEFT_H,STR_SCL_Y);
		stTrack.st_LeftH.nScaleY = _ttoi(strIni);

		stTrack.st_RightH.strCamID = ini.GetString(STR_SECTION_RIGHT_H,STR_CAM);

		strIni = ini.GetString(STR_SECTION_RIGHT_H,STR_START_X);
		stTrack.st_RightH.nStartX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT_H,STR_START_Y);
		stTrack.st_RightH.nStartY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT_H,STR_END_X);
		stTrack.st_RightH.nEndX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT_H,STR_END_Y);
		stTrack.st_RightH.nEndY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT_H,STR_MOV_X);
		stTrack.st_RightH.nMovX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT_H,STR_MOV_Y);
		stTrack.st_RightH.nMovY = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT_H,STR_SCL_X);
		stTrack.st_RightH.nScaleX = _ttoi(strIni);

		strIni = ini.GetString(STR_SECTION_RIGHT_H,STR_SCL_Y);
		stTrack.st_RightH.nScaleY = _ttoi(strIni);*/



				/*strIni = ini.GetString(STR_SECTION_INFO,STR_SHOW_TRACKING);
		stTrack.bShowTracking = _ttoi(strIni);*/
		ESMSetTrackInfo(stTrack);

		cam_sel = stTrack;
	}
}
void CESMFrameViewBalltracking::SaveData()
{
	ESMLog(5,_T("[AutoDetecting] Save AutoDetect Info"));

	CESMIni ini;

	CString strFile,strIni;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_AUTODETECTING);

	//-- Check File Exist
	CESMFileOperation fo;
	if(!fo.IsFileExist(strFile))
	{
		CFile file;
		file.Open(strFile, CFile::modeCreate);
		file.Close();
	}
	cam_sel = ESMGetTrackInfo();

	//CString strSection = _T("");
	if(ini.SetIniFilename (strFile))
	{
		strIni.Format(_T("%d"),cam_sel.bReverse);
		ini.WriteString(STR_SECTION_INFO,STR_REVERSE,strIni);
		strIni.Format(_T("%d"),cam_sel.nCircleSize);
		ini.WriteString(STR_SECTION_INFO,STR_CIRCLE_SIZE,strIni);
		strIni.Format(_T("%d"),cam_sel.nWidth);
		ini.WriteString(STR_SECTION_INFO,STR_RESOLUTION_WIDTH,strIni);
		strIni.Format(_T("%d"),cam_sel.nHeight);
		ini.WriteString(STR_SECTION_INFO,STR_RESOLUTION_HEIGHT,strIni);
		/*strIni.Format(_T("%d"),cam_sel.bShowTracking);
		ini.WriteString(STR_SECTION_INFO,STR_SHOW_TRACKING,strIni);*/

		for(int i = 0 ; i < 4; i++)
		{
			ini.WriteString(m_strArrSection[i],STR_CAM,cam_sel.st_AxisInfo[i].strCamID);

			strIni.Format(_T("%d"),cam_sel.st_AxisInfo[i].nStartX);
			ini.WriteString(m_strArrSection[i],STR_START_X,strIni);
			strIni.Format(_T("%d"),cam_sel.st_AxisInfo[i].nStartY);
			ini.WriteString(m_strArrSection[i],STR_START_Y,strIni);
			strIni.Format(_T("%d"),cam_sel.st_AxisInfo[i].nEndX);
			ini.WriteString(m_strArrSection[i],STR_END_X,strIni);
			strIni.Format(_T("%d"),cam_sel.st_AxisInfo[i].nEndY);
			ini.WriteString(m_strArrSection[i],STR_END_Y,strIni);
			strIni.Format(_T("%d"),cam_sel.st_AxisInfo[i].nMovX);
			ini.WriteString(m_strArrSection[i],STR_MOV_X,strIni);
			strIni.Format(_T("%d"),cam_sel.st_AxisInfo[i].nMovY);
			ini.WriteString(m_strArrSection[i],STR_MOV_Y,strIni);
			strIni.Format(_T("%d"),cam_sel.st_AxisInfo[i].nScaleX);
			ini.WriteString(m_strArrSection[i],STR_SCL_X,strIni);
			strIni.Format(_T("%d"),cam_sel.st_AxisInfo[i].nScaleY);
			ini.WriteString(m_strArrSection[i],STR_SCL_Y,strIni);
		}
		/*ini.WriteString(STR_SECTION_LEFT,STR_CAM,cam_sel.st_Left.strCamID);

		strIni.Format(_T("%d"),cam_sel.st_Left.nStartX);
		ini.WriteString(STR_SECTION_LEFT,STR_START_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Left.nStartY);
		ini.WriteString(STR_SECTION_LEFT,STR_START_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Left.nEndX);
		ini.WriteString(STR_SECTION_LEFT,STR_END_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Left.nEndY);
		ini.WriteString(STR_SECTION_LEFT,STR_END_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Left.nMovX);
		ini.WriteString(STR_SECTION_LEFT,STR_MOV_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Left.nMovY);
		ini.WriteString(STR_SECTION_LEFT,STR_MOV_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Left.nScaleX);
		ini.WriteString(STR_SECTION_LEFT,STR_SCL_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Left.nScaleY);
		ini.WriteString(STR_SECTION_LEFT,STR_SCL_Y,strIni);

		ini.WriteString(STR_SECTION_RIGHT,STR_CAM,cam_sel.st_Right.strCamID);

		strIni.Format(_T("%d"),cam_sel.st_Right.nStartX);
		ini.WriteString(STR_SECTION_RIGHT,STR_START_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Right.nStartY);
		ini.WriteString(STR_SECTION_RIGHT,STR_START_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Right.nEndX);
		ini.WriteString(STR_SECTION_RIGHT,STR_END_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Right.nEndY);
		ini.WriteString(STR_SECTION_RIGHT,STR_END_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Right.nMovX);
		ini.WriteString(STR_SECTION_RIGHT,STR_MOV_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Right.nMovY);
		ini.WriteString(STR_SECTION_RIGHT,STR_MOV_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Right.nScaleX);
		ini.WriteString(STR_SECTION_RIGHT,STR_SCL_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_Right.nScaleY);
		ini.WriteString(STR_SECTION_RIGHT,STR_SCL_Y,strIni);

		ini.WriteString(STR_SECTION_LEFT_H,STR_CAM,cam_sel.st_LeftH.strCamID);

		strIni.Format(_T("%d"),cam_sel.st_LeftH.nStartX);
		ini.WriteString(STR_SECTION_LEFT_H,STR_START_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_LeftH.nStartY);
		ini.WriteString(STR_SECTION_LEFT_H,STR_START_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_LeftH.nEndX);
		ini.WriteString(STR_SECTION_LEFT_H,STR_END_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_LeftH.nEndY);
		ini.WriteString(STR_SECTION_LEFT_H,STR_END_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_LeftH.nMovX);
		ini.WriteString(STR_SECTION_LEFT_H,STR_MOV_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_LeftH.nMovY);
		ini.WriteString(STR_SECTION_LEFT_H,STR_MOV_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_LeftH.nScaleX);
		ini.WriteString(STR_SECTION_LEFT_H,STR_SCL_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_LeftH.nScaleY);
		ini.WriteString(STR_SECTION_LEFT_H,STR_SCL_Y,strIni);

		ini.WriteString(STR_SECTION_RIGHT_H,STR_CAM,cam_sel.st_RightH.strCamID);

		strIni.Format(_T("%d"),cam_sel.st_RightH.nStartX);
		ini.WriteString(STR_SECTION_RIGHT_H,STR_START_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_RightH.nStartY);
		ini.WriteString(STR_SECTION_RIGHT_H,STR_START_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_RightH.nEndX);
		ini.WriteString(STR_SECTION_RIGHT_H,STR_END_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_RightH.nEndY);
		ini.WriteString(STR_SECTION_RIGHT_H,STR_END_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_RightH.nMovX);
		ini.WriteString(STR_SECTION_RIGHT_H,STR_MOV_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_RightH.nMovY);
		ini.WriteString(STR_SECTION_RIGHT_H,STR_MOV_Y,strIni);
		strIni.Format(_T("%d"),cam_sel.st_RightH.nScaleX);
		ini.WriteString(STR_SECTION_RIGHT_H,STR_SCL_X,strIni);
		strIni.Format(_T("%d"),cam_sel.st_RightH.nScaleY);
		ini.WriteString(STR_SECTION_RIGHT_H,STR_SCL_Y,strIni);*/

	}
}
void CESMFrameViewBalltracking::OnBnClickedAtBtnHLeft()
{
	CString str;
	m_selector.GetLBText(m_selector.GetCurSel(),str);
	SetDlgItemText(IDC_AT_EDIT_H_LEFT,str);
}
void CESMFrameViewBalltracking::OnBnClickedAtBtnHRight()
{
	CString str;
	m_selector.GetLBText(m_selector.GetCurSel(),str);
	SetDlgItemText(IDC_AT_EDIT_H_RIGHT,str);
}
void CESMFrameViewBalltracking::SetFrameList(CESMFrameList* pParent)
{
	m_pDlgFrameList = pParent;
}