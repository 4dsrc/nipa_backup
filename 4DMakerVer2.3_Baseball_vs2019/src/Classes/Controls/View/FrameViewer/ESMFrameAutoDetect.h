#pragma once

#include "resource.h"
#include <vector>
#include <sstream>
#include "ToolTipListCtrl.h"
#include "RSChildDialog.h"
#include "ESMIndexStructure.h"
#include "highgui.h"
#include "ESMIndex.h"
#include "cv.h"
#include "DSCFrameSelector.h"


class CESMAutoDetect
{
private:
	int ROI_row;
	int ROI_col;
	int Astr_x;
	int Astr_y;
	int ROI_size;
	int label_cnt;
	int radius;
	cv::Point cen;
	int distance;
	int Matching_Frame;
	int circle_size;
	BOOL Reverse;
	int mat;
	int m_nSearchingSize;
	vector<BOOL>*nArrMatchSize;
	vector<cv::Point>*ptArrPoint;
	vector<double>*dbArrDistance;

	cv::Point m_PointCenter;
	BOOL bCheckLeftOrRight;
public:
	CESMAutoDetect();
	~CESMAutoDetect();
	void Otsu(Mat img, Mat result);
	void labeling(const Mat img,int* table1,int* table2,int tsize,
		int* pass1,int* pass2,int*cnt_label);
	int show_label(Mat img, int* cnt_label, Mat ROI,int cnt);
	void CreateROI(Mat ori,Mat ROI);
	int VideoPlay(Mat input, int selected_frame);
	void RemoveNoise(Mat ROI);
	void Image_Subtract(Mat ROI,Mat result,int cnt);
	int Counting_pix(Mat b_result);
	void SetDetectingInfo(int Rstr_x,int Rstr_y,int Rend_x,int Rend_y, int circle,BOOL reverse,CString strDSC,int nSearchSize);
	int ATCheckValid(int nMatchFrame);
	int CheckSearchArea(cv::Point ptPoint,BOOL bWheterLeftorRight);
};