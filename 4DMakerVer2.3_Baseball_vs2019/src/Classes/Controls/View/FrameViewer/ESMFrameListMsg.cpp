////////////////////////////////////////////////////////////////////////////////
//
//	ESMFrameListMsg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-26
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FileOperations.h"
#include "ESMFrameList.h"
#include "ESMFunc.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <sys/timeb.h>

BEGIN_MESSAGE_MAP(CESMFrameList, CRSChildDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()	
	ON_MESSAGE(WM_LOAD_FRAME,OnReload)	
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()


void CESMFrameList::OnPaint()
{	
	UpdateFrames();
	CDialogEx::OnPaint();
}

void CESMFrameList::OnSize(UINT nType, int cx, int cy) 
{
    CDialog::OnSize(nType, cx, cy);

	//  2013-10-23 Ryumin
	int nScrollMax	= 0;

	m_nCurHeight	= cy;
	//	V-Scroll
	if (cy < m_rcRect.Height())
		nScrollMax = m_rcRect.Height() - cy;
	else
		nScrollMax = 0;

	SCROLLINFO siVScroll;
	siVScroll.cbSize= sizeof(SCROLLINFO);
	siVScroll.fMask	= SIF_ALL; // SIF_ALL = SIF_PAGE | SIF_RANGE | SIF_POS;
	siVScroll.nMin	= 0;
	siVScroll.nMax	= nScrollMax;
	siVScroll.nPage	= siVScroll.nMax /10;
	siVScroll.nPos	= 0;
	SetScrollInfo(SB_VERT, &siVScroll, TRUE); 

	//	H-Scroll
	m_nCurWidth		= cx;
	if (cx < m_rcRect.Width())
		nScrollMax = m_rcRect.Width() - cx;
	else
		nScrollMax = 0;

	SCROLLINFO siHScroll;
	siHScroll.cbSize= sizeof(SCROLLINFO);
	siHScroll.fMask	= SIF_ALL; // SIF_ALL = SIF_PAGE | SIF_RANGE | SIF_POS;
	siHScroll.nMin	= 0;
	siHScroll.nMax	= nScrollMax;
	siHScroll.nPage	= siHScroll.nMax /10;
	siHScroll.nPos	= 0;
	SetScrollInfo(SB_HORZ, &siHScroll, TRUE); 

	UpdateFrames();
} 

/************************************************************************
 * @method:		CESMFrameList::OnHScroll
 * @function:	OnHScroll
 * @date:		2013-10-23
 * @owner		Ryumin (ryumin@esmlab.com)
 * @param:		UINT nSBCode
 * @param:		UINT nPos
 * @param:		CScrollBar * pScrollBar
 * @return:		void
 */
void CESMFrameList::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	int nSelected	= GetSelectedTime(0);

	int nDelta;
	int nMaxPos = m_rcRect.Width() - m_nCurHeight;

	switch (nSBCode)
	{
	case SB_LINERIGHT:
		if (m_nHScrollPos >= nMaxPos)
			return;
		nDelta = min(nMaxPos /100, nMaxPos - m_nHScrollPos);
		ESMGetNextFrameTime(nSelected);
		break;
	case SB_LINELEFT:
		if (m_nHScrollPos <= 0)
			return;
		nDelta = -min(nMaxPos /100, m_nHScrollPos);
		ESMGetPreviousFrameTime(nSelected);
		break;
	case SB_PAGERIGHT:
		if (m_nHScrollPos >= nMaxPos)
			return;
		nDelta = min(nMaxPos /10, nMaxPos - m_nHScrollPos);
		break;
	case SB_THUMBPOSITION:
		nDelta = (int)nPos - m_nHScrollPos;
		break;
	case SB_PAGELEFT:
		if (m_nHScrollPos <= 0)
			return;
		nDelta = -min(nMaxPos /10, m_nHScrollPos);
		break;
	default:
		return;
	}
	m_nHScrollPos += nDelta;
	SetScrollPos(SB_HORZ, m_nHScrollPos, TRUE);
	ScrollWindow(0, -nDelta);

	//	시작 시간, 끝시간 체크 필요.
	SetSelectedTime(nSelected, 0);

	UpdateFrames();

	CRSChildDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

/************************************************************************
 * @method:		CESMFrameList::OnVScroll
 * @function:	OnVScroll
 * @date:		2013-10-23
 * @owner		Ryumin (ryumin@esmlab.com)
 * @param:		UINT nSBCode
 * @param:		UINT nPos
 * @param:		CScrollBar * pScrollBar
 * @return:		void
 */
void CESMFrameList::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	int nDelta;
	int nMaxPos = m_rcRect.Width() - m_nCurWidth;

	switch (nSBCode)
	{
	case SB_LINEDOWN:
		if (m_nVScrollPos >= nMaxPos)
			return;
		nDelta = min(nMaxPos /100, nMaxPos - m_nVScrollPos);
		break;

	case SB_LINEUP:
		if (m_nVScrollPos <= 0)
			return;
		nDelta = -min(nMaxPos /100, m_nVScrollPos);
		break;

	case SB_PAGEDOWN:
		if (m_nVScrollPos >= nMaxPos)
			return;
		nDelta = min(nMaxPos /10, nMaxPos - m_nVScrollPos);
		break;

	case SB_THUMBPOSITION:
		nDelta = (int)nPos - m_nVScrollPos;
		break;

	case SB_PAGEUP:
		if (m_nVScrollPos <= 0)
			return;
		nDelta = -min(nMaxPos /10, m_nVScrollPos);
		break;

	default:
		return;
	}
	m_nVScrollPos += nDelta;
	SetScrollPos(SB_VERT, m_nVScrollPos, TRUE);
	ScrollWindow(0, -nDelta);

	CRSChildDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CESMFrameList::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nX = 0;	
	int nItems = 0;
	CRect rect, rectPrev;	

	m_nSrcPosX = point.x + m_nPrevX;
	m_nSrcPosY = point.y + m_nPrevY;

	/*
	for(int nIndex = m_nFirstPic; nIndex < m_nFirstPic + m_nDrawedPicture ; nIndex ++)
	{
		nX = nItems*(88+6);
		rect = CRect( CPoint(nX,2), CSize(88,88));
		if(rect.PtInRect(point))
		{
			if(m_nSelectedFrame >= m_nFirstPic && m_nSelectedFrame < m_nFirstPic + m_nDrawedPicture)
			{
				nX = (m_nSelectedFrame-m_nFirstPic)*(88+6);
				//-- Get Prev Selected Item				
				rectPrev = CRect( CPoint(nX,2), CSize(88,88));
				CBrush brush = CBrush(RGB(63,63,63));	
				InvalidateRect(rectPrev);
			}
			m_nSelectedFrame =  nIndex;
			InvalidateRect(rect);
			break;
		}	
		nItems++;
	}
	*/
	CDialog::OnLButtonDown(nFlags, point);	
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDblClk
//! @date		2011-07-27
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CESMFrameList::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	SetAxisData(point);
	Invalidate();

	CDialog::OnLButtonDown(nFlags, point);	
}

void CESMFrameList::OnMouseMove(UINT nFlags, CPoint point)
{	
	TRACKMOUSEEVENT        mouseEvent;
	mouseEvent.cbSize = sizeof(mouseEvent);
	mouseEvent.dwFlags = TME_LEAVE|TME_HOVER;
	mouseEvent.hwndTrack = m_hWnd;
	mouseEvent.dwHoverTime = 1;
	::_TrackMouseEvent(&mouseEvent);
	
	if(m_bIsPressedRButton)
	{
		if(m_pTemplateMgr == NULL)
			return;
		if(m_bIsPressedRButton)
		{
			if(!(GetAsyncKeyState(VK_LSHIFT) & 0x8000))
			{
				point.x = fixXValueWhenShiftKeyIsPressed;
			}

			SetTransPointInFrameViewer(point);
			Invalidate();
		}
	}

	switch(nFlags)
	{
	case 0: //Move Event
		SetPOSData(point);
		Invalidate();
		break;
	case MK_LBUTTON:
		m_nPosX = point.x;
		m_nPosY = point.y;
		Invalidate();
		break;
	case MK_RBUTTON:
		break;
	}
}

//wgkim@esmlab.com 좌표처리기
void CESMFrameList::OnRButtonUp(UINT nFlags, CPoint point)
{
	SetVerticalImagePointInFrameViewer(point);
	UpdateFrames();
	m_bIsPressedRButton = FALSE;
	if(m_pTemplateMgr == NULL)
		return;
	if(!(GetAsyncKeyState(VK_LSHIFT) & 0x8000))
	{
		point.x = fixXValueWhenShiftKeyIsPressed;
	}

	SetTransPointInFrameViewer(point);
	SetSelectedRealPoint(GetTransPointInFrameViewer());
	Invalidate();

	clickFlag = false;

	m_pTemplateMgr->ClearArrTemplatePoint();
	m_pTemplateMgr->SetCenterPoint(CPointToPoint2f(GetSelectedRealPoint()));

	//17-02-09	
	m_pTemplateMgr->SetDscIndex(GetSelectedDSC());
	m_pTemplateMgr->SetArrTemplatePoint();

	CRSChildDialog::OnRButtonUp(nFlags, point);
}

void CESMFrameList::OnRButtonDown(UINT nFlags, CPoint point)
{
	m_bIsPressedRButton = TRUE;
	if(m_pTemplateMgr == NULL)
		return;	

	fixXValueWhenShiftKeyIsPressed = point.x;

	SetViewPointInFrameViewer(point);
	SetSelectedRealPoint(GetViewPointInFrameViewer());
	Invalidate();

	m_pTemplateMgr->SetOverBoundaryFlag(FALSE);

	m_pTemplateMgr->SetViewPoint(CPointToPoint2f(GetSelectedRealPoint()));

	clickFlag = true;
	ESMLog(5, _T("Template Point : (%d, %d)"), 
		GetSelectedRealPoint().x, GetSelectedRealPoint().y);

	CRSChildDialog::OnRButtonDown(nFlags, point);
}

void CESMFrameList::OnLButtonUp(UINT nFlags, CPoint point)
{
	Invalidate();
	m_nPrevX =m_nSrcPosX - m_nPosX;
	m_nPrevY =m_nSrcPosY - m_nPosY;
	
	HWND hWND = GetSafeHwnd();
	::SetFocus(hWND);
	//ESMLog(5,_T("Focus!"));
}

BOOL CESMFrameList::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	int nZoomRatio = ESMGetValue(ESM_VALUE_VIEW_RATIO);

	if(zDelta > 0)
	{
		//줌 비율 증가, +5, 250%까지
		nZoomRatio += 5;
		if(nZoomRatio >= 400)
			nZoomRatio = 400;

		ESMSetValue(ESM_VALUE_VIEW_RATIO,nZoomRatio);
	}
	else
	{
		//줌 비율 감소, -5, 100%까지
		nZoomRatio -= 5;
		if(nZoomRatio <= 100)
			nZoomRatio = 100;

		ESMSetValue(ESM_VALUE_VIEW_RATIO,nZoomRatio);
	}
	UpdateFrames();
	return TRUE;
}

BOOL CESMFrameList::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->wParam==VK_RETURN)
	{
		CMainFrame* m_pMainWnd;
		m_pMainWnd = (CMainFrame*)AfxGetMainWnd();
		if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT))
		{
			ESMSetValue(ESM_VALUE_TEMPLATEPOINT, FALSE);
			m_pMainWnd->m_wndDSCViewer.m_pListView->m_FrameAdj = "-";				
		}
		else
		{
			ESMSetValue(ESM_VALUE_TEMPLATEPOINT, TRUE);
			m_pMainWnd->m_wndDSCViewer.m_pListView->m_FrameAdj = "TEM";	
			m_pMainWnd->LoadTemplateSquaresProfile();
		}
		m_pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();
		return FALSE;
	}
	return FALSE;
}
