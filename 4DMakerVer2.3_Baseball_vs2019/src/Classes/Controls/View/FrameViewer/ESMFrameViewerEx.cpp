#include "stdafx.h"
#include "ESMFrameViewerEx.h"

IMPLEMENT_DYNAMIC(CESMFrameViewerEx, CDialog)

CESMFrameViewerEx::CESMFrameViewerEx(CWnd* pParent /*=NULL*/)
	: CDialog(CESMFrameViewerEx::IDD, pParent)	
{
//	m_pRemoteCtrlDlg = NULL;//new CESMUtilRemoteCtrl;
	//m_pRemoteCtrlDlg->Create(IDD,this);
	//m_pRemoteCtrlDlg->ShowWindow(SW_HIDE);
}

CESMFrameViewerEx::~CESMFrameViewerEx()
{

}
BOOL CESMFrameViewerEx::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetClientRect(&m_rcDlgSize);

//	m_pRemoteCtrlDlg = new CESMUtilRemoteCtrl;
//	m_pRemoteCtrlDlg->Create(IDD_UTIL_REMOTECTRL,this);
//	m_pRemoteCtrlDlg->MoveWindow(m_rcDlgSize);
//	m_pRemoteCtrlDlg->ShowWindow(TRUE);

	return TRUE;
}
void CESMFrameViewerEx::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		ID_TEMPLATE_ZOOM_150,
		ID_TEMPLATE_ZOOM_200,
		ID_TEMPLATE_ZOOM_300,
		ID_TEMPLATE_ZOOM_400,
		//ID_IMAGE_ZOOMIN			,	
		//ID_IMAGE_ZOOMOUT		,
	};

	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	

	m_wndToolBar.ShowWindow(SW_SHOW);
}
void CESMFrameViewerEx::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
}

BEGIN_MESSAGE_MAP(CESMFrameViewerEx, CDialog)
	ON_WM_SIZE()
	ON_COMMAND(ID_IMAGE_ZOOMIN			, Test		)
	ON_COMMAND(ID_TEMPLATE_ZOOM_150		, OnBnSetZoom150	)
	ON_COMMAND(ID_TEMPLATE_ZOOM_200		, OnBnSetZoom200	)
	ON_COMMAND(ID_TEMPLATE_ZOOM_300		, OnBnSetZoom300	)
	ON_COMMAND(ID_TEMPLATE_ZOOM_400		, OnBnSetZoom400	)
END_MESSAGE_MAP()

void CESMFrameViewerEx::Test()
{
	//TEST
	/*for(int i = 0 ; i < 2; i++)
	{
		m_pRemoteCtrlDlg->m_strArrDSCID.push_back(_T("11111"));
	}
	
	m_pRemoteCtrlDlg->CreateRemoteFrameDlg();*/
}
void CESMFrameViewerEx::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;

	RepositionBars(0,0xFFFF,0);

	//if(m_pRemoteCtrlDlg)
	//{
	//	if(m_pRemoteCtrlDlg->GetRecordProcess())
	//		return;

		//GetClientRect(&m_rcDlgSize);
		//m_pRemoteCtrlDlg->ResizeWindow(m_rcClientFrame);
	//	m_pRemoteCtrlDlg->MoveWindow(m_rcClientFrame);
	//}
}
void CESMFrameViewerEx::OnBnSetZoom150()
{
//	if(m_pRemoteCtrlDlg)
//	{
//		m_pRemoteCtrlDlg->SetZoomGuide(150);
//	}
}
void CESMFrameViewerEx::OnBnSetZoom200()
{
	//if(m_pRemoteCtrlDlg)
	//{
	//	m_pRemoteCtrlDlg->SetZoomGuide(200);

	//}
}
void CESMFrameViewerEx::OnBnSetZoom300()
{
	//if(m_pRemoteCtrlDlg)
	//{
	//	m_pRemoteCtrlDlg->SetZoomGuide(300);
	//}
}
void CESMFrameViewerEx::OnBnSetZoom400()
{
	//if(m_pRemoteCtrlDlg)
	//{
	//	m_pRemoteCtrlDlg->SetZoomGuide(400);
	//}
}
void CESMFrameViewerEx::SetFocusAsFrame()
{
	//if(m_pRemoteCtrlDlg)
	//{
	//	m_pRemoteCtrlDlg->SetFocus();
	//	m_pRemoteCtrlDlg->SetFocusFromMainFrm();
	//}
}