STR_SIZE_BIG
////////////////////////////////////////////////////////////////////////////////
//
//	ESMFrameViewer.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-24
// @ver.1.0	4DMaker
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FileOperations.h"
#include "ESMFrameViewer.h"
#include "DSCItem.h"
#include "MainFrm.h"

#include "ESMFunc.h"
#include "SRSIndex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CESMFrameViewer, CDialog)
CESMFrameViewer::CESMFrameViewer(UINT nIDTemplate, CWnd* pParent /*= NULL*/)
	: CDialog(nIDTemplate, pParent)	
{
	m_pDlgFrameList	= NULL;	
	m_pATView = NULL;
}

CESMFrameViewer::CESMFrameViewer(CWnd* pParent /*=NULL*/)
	: CDialog(CESMFrameViewer::IDD, pParent)	
{
	//-- 2011-05-18 hongsu.jung
	//-- Load From Config
	m_pDlgFrameList	= NULL;	
	m_pATView = NULL;
}

CESMFrameViewer::~CESMFrameViewer()
{
	if(m_pDlgFrameList)
	{
		delete m_pDlgFrameList;
		m_pDlgFrameList = NULL;
	}
	if(m_pATView)
	{
		delete m_pATView;
		m_pATView = NULL;
	}
}

void CESMFrameViewer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMFrameViewer)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
}

BOOL CESMFrameViewer::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		//wgkim@esmlab.com
		if(pMsg->message == WM_MOUSELEAVE)
		{
			m_pDlgFrameList->SetWhetherMousePointIsOnFrameViewer(FALSE);
			OnPaint();					
		}
		if(pMsg->message == WM_MOUSEHOVER)
		{
			m_pDlgFrameList->SetWhetherMousePointIsOnFrameViewer(TRUE);
		}
		//////////////////////
		
		if(pMsg->message == WM_KEYDOWN)
		{
			if(pMsg->wParam==VK_ESCAPE)
				return FALSE;

			BOOL bShift = ((GetKeyState(VK_SHIFT) & 0x8000) != 0);
			BOOL bControl = ((GetKeyState(VK_CONTROL) & 0x8000) != 0);
			BOOL bAlt = ((GetKeyState(VK_MENU) & 0x8000) != 0);

			if(bControl && !bShift && !bAlt)
			{

				switch(pMsg->wParam)
				{
				case VK_NUMPAD8://UP
					{
						m_pDlgFrameList->MoveUp();
						UpdateFrames();	
					}
					break;
				case VK_NUMPAD5://DOWN
					{
						m_pDlgFrameList->MoveDown();
						UpdateFrames();	
					}
					break;
				case VK_NUMPAD4://Left
					{
						m_pDlgFrameList->MoveLeft();
						UpdateFrames();	
					}
					break;
				case VK_NUMPAD6://Right
					{
						m_pDlgFrameList->MoveRight();
						UpdateFrames();	
					}
					break;
				case VK_NUMPAD7://Scale cols Down
					{
						m_pDlgFrameList->ScaleColsUp();
						UpdateFrames();	
					}
					break;
				case VK_NUMPAD1://Scale cols Up
					{
						m_pDlgFrameList->ScaleColsDown();
						UpdateFrames();	
					}
					break;
				case VK_NUMPAD9://Scale rows Down
					{
						m_pDlgFrameList->ScaleRowsUp();
						UpdateFrames();	
					}
					break;
				case VK_NUMPAD3://Scale rows Up
					{
						m_pDlgFrameList->ScaleRowsDown();
						UpdateFrames();	
					}
					break;
				case VK_NUMPAD2: {}break;
				case VK_ADD:
					{
						m_pATView->SaveData();
					}	
					break;
				case VK_RETURN:
					{
						CMainFrame* m_pMainWnd;
						m_pMainWnd = (CMainFrame*)AfxGetMainWnd();
						if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT))
						{
							ESMSetValue(ESM_VALUE_TEMPLATEPOINT, FALSE);
							m_pMainWnd->m_wndDSCViewer.m_pListView->m_FrameAdj = "-";				
						}
						else
						{
							ESMSetValue(ESM_VALUE_TEMPLATEPOINT, TRUE);
							m_pMainWnd->m_wndDSCViewer.m_pListView->m_FrameAdj = "TEM";	
							m_pMainWnd->LoadTemplateSquaresProfile();
						}
						m_pMainWnd->m_wndDSCViewer.m_pListView->Invalidate();						
					}
					break;
				default:					
					break;
				}
			}
			
			if(!bControl && !bShift && !bAlt)
			{
				if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
					return FALSE;

				ESMEvent* pEsmMsg = NULL;
				pEsmMsg = new ESMEvent();
				pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
				pEsmMsg->pParam = (LPARAM)pMsg;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
			}
			
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMFrameViewer, CDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()	
	ON_WM_MOUSEWHEEL()
	ON_COMMAND(ID_IMAGE_ZOOMIN			, OnZoomIn		)
	ON_COMMAND(ID_IMAGE_ZOOMOUT			, OnZoomOut		)
	ON_COMMAND(ID_CAMSEL				, OnFrameSel	)
	ON_COMMAND(ID_SetSize				, OnBnSetSize	)
	ON_COMMAND(ID_AT_SET				, OnATSet		)

	ON_COMMAND(ID_TEMPLATE_ZOOM_150		, OnBnSetZoom150	)
	ON_COMMAND(ID_TEMPLATE_ZOOM_200		, OnBnSetZoom200	)
	ON_COMMAND(ID_TEMPLATE_ZOOM_300		, OnBnSetZoom300	)
	ON_COMMAND(ID_TEMPLATE_ZOOM_400		, OnBnSetZoom400	)
	ON_WM_MOUSEACTIVATE()
	ON_WM_MOUSEHWHEEL()
END_MESSAGE_MAP()

// CFrameNailDlg message handlers
BOOL CESMFrameViewer::OnInitDialog()
{
	CDialog::OnInitDialog();

	if(m_pDlgFrameList)
	{
		delete m_pDlgFrameList;
		m_pDlgFrameList = NULL;
	}

	m_pDlgFrameList = new CESMFrameList();
	m_pDlgFrameList->Create(CESMFrameList::IDD, this);

	if(m_pATView)
	{
		delete m_pATView;
		m_pATView = NULL;
	}

	m_pATView = new CESMFrameViewBalltracking;
	m_pATView->SetFrameList(m_pDlgFrameList);
	m_pATView->LoadData();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CESMFrameViewer::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_ZOOMIN			,	
		ID_IMAGE_ZOOMOUT		,
		ID_SEPARATOR			,
		//ID_IMAGE_LEFT			,
		//ID_IMAGE_RIGHT			,
		//ID_SEPARATOR			,
		//ID_CAMSEL				,
		//ID_SetSize				,
		//ID_AT_SET				,
		//ID_SEPARATOR			,
		ID_TEMPLATE_ZOOM_150	,
		ID_TEMPLATE_ZOOM_200	,
		ID_TEMPLATE_ZOOM_300	,
		ID_TEMPLATE_ZOOM_400	,
		//ID_SEPARATOR			,
		//ID_CAMSEL				,
	};

	CRect itemRect = AT_GetScaleBtnRect();
	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	

	m_wndToolBar.ShowWindow(SW_SHOW);
}

CRect CESMFrameViewer::AT_GetScaleBtnRect()
{
	CRect a;
	a.top = 10;
	a.left = 0;
	a.bottom = 500;
	a.right = 80;

	return a;
}
void CESMFrameViewer::OnDrawButton(CDC* pDC)
{
	CRect rect = AT_GetScaleBtnRect();

	pDC->FillSolidRect(rect,COLOR_YELLOW);
}
void CESMFrameViewer::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect rtBtn = AT_GetScaleBtnRect();
	if(rtBtn.PtInRect(point))
	{

	}
	CDialog::OnLButtonDown(nFlags,point);
}
void CESMFrameViewer::OnSize(UINT nType, int cx, int cy) 
{
	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	//m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;
	m_rcClientFrame.top	= 40;
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width()		< FRAME_MIN_WIDTH || 
	    m_rcClientFrame.Height()	< FRAME_MIN_HEIGHT)
		return;

	//-- ReloadFrame
	UpdateFrames(TRUE);

	if(m_pDlgFrameList->GetSafeHwnd())
		m_pDlgFrameList->MoveWindow(0, 40, cx, cy - 40);

	CDialog::OnSize(nType, cx, cy);	
} 
void CESMFrameViewer::OnPaint()
{	
	UpdateFrames();
	CDialog::OnPaint();
}
void CESMFrameViewer::UpdateFrames(BOOL bReloadImage)
{
	//-- Rounding Dialog
	CPaintDC dc(this);
	if(bReloadImage)
		m_pDlgFrameList->m_ReLoadImage = TRUE;
	CRect rect;
	rect.CopyRect(m_rcClientFrame);

	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), SRCCOPY);

	CString strFilePath;
	mDC.FillRect(rect, &CBrush(COLOR_BASIC_BG_1/*COLOR_BASIC_BG_2*/));
		
	//-- Frame List
	if(m_pDlgFrameList)
	{
		m_pDlgFrameList->ShowWindow(SW_SHOW);
		m_pDlgFrameList->MoveWindow(rect);
		m_pDlgFrameList->Invalidate(FALSE);	
	}
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();
}

void CESMFrameViewer::OnZoomIn()
{
	m_pDlgFrameList->SetZoomIn();
	UpdateFrames();
}

void CESMFrameViewer::OnZoomOut()
{
	m_pDlgFrameList->SetZoomOut();
	UpdateFrames();
}

void CESMFrameViewer::OnFrameSel()
{
	m_pATView->data_info = m_pDlgFrameList;
	m_pATView->LoadData();

	m_pATView->DoModal();
	m_pATView->DestroyWindow();
	/*CESMFrameViewBalltracking k;
	k.data_info = m_pDlgFrameList;
	k.DoModal();
	k.DestroyWindow();*/
}
void CESMFrameViewer::OnATSet()
{
	m_pATView->SaveData();

	//영상 데이터 저장

}

void CESMFrameViewer::OnBnSetSize()
{
	m_pDlgFrameList->SetSize();
	UpdateFrames();
}

void CESMFrameViewer::OnBnSetZoom150()
{
	m_pDlgFrameList->SetZoomValueInFrameViewer(150);
}

void CESMFrameViewer::OnBnSetZoom200()
{
	m_pDlgFrameList->SetZoomValueInFrameViewer(200);
}

void CESMFrameViewer::OnBnSetZoom300()
{
	m_pDlgFrameList->SetZoomValueInFrameViewer(300);
}

void CESMFrameViewer::OnBnSetZoom400()
{
	m_pDlgFrameList->SetZoomValueInFrameViewer(400);
}

BOOL CESMFrameViewer::OnMouseWheel( UINT nFlags, short zDelta, CPoint pt )
{
	CPoint ptChild;
	CRect rect;

	m_pDlgFrameList->GetWindowRect(&rect);
	ptChild.x = rect.left;
	ptChild.y = rect.top;

	ScreenToClient(&ptChild);

	int nPtX = pt.x;
	int nPtY = pt.y;

	CString strPtX;
	CString strPtY;
	strPtX.Format(_T("%d"), nPtX);
	strPtY.Format(_T("%d"), nPtY);

	if(zDelta == 120)
	{
		m_pDlgFrameList->SetZoomIn();
		UpdateFrames();
		
	}
	else if(zDelta == -120)
	{
		m_pDlgFrameList->SetZoomOut();
		UpdateFrames();
	}

	return TRUE;
}

int CESMFrameViewer::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	return CDialog::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

void CESMFrameViewer::OnMouseHWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// 이 기능을 사용하려면 Windows Vista 이상이 있어야 합니다.
	// _WIN32_WINNT 기호는 0x0600보다 크거나 같아야 합니다.
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnMouseHWheel(nFlags, zDelta, pt);
}