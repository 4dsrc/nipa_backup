#pragma once
#include "stdafx.h"
#include "resource.h"
#include <vector>
#include "ToolTipListCtrl.h"
#include "ESMFrameList.h"
#include "ESMIndex.h"
#include "ESMCtrl.h"

class CDSCItem;

#define STR_SECTION_INFO _T("Info")

#define STR_CAM _T("Cam")
#define STR_START_X _T("StartX")
#define STR_START_Y _T("StartY")
#define STR_END_X _T("EndX")
#define STR_END_Y _T("EndY")
#define STR_MOV_X _T("MovX")
#define STR_MOV_Y _T("MovY")
#define STR_SCL_X _T("ScaleX")
#define STR_SCL_Y _T("ScaleY")

/*
#define STR_RIGHT_START_X _T("StartX")
#define STR_RIGHT_START_Y _T("StartY")
#define STR_RIGHT_END_X _T("EndX")
#define STR_RIGHT_END_Y _T("EndY")
#define STR_RIGHT_MOV_X _T("MovX")
#define STR_RIGHT_MOV_Y _T("MovY")*/

#define STR_REVERSE _T("Reverse")
#define STR_CIRCLE_SIZE _T("Circle Size")
#define STR_RESOLUTION_WIDTH _T("RESOLUTION X")
#define STR_RESOLUTION_HEIGHT _T("RESOLUTION Y")
#define STR_SHOW_TRACKING _T("ShowInfo")
class CESMFrameViewBalltracking : public CDialogEx
{
	DECLARE_DYNAMIC(CESMFrameViewBalltracking)

private:

public:
	CESMFrameViewBalltracking(CWnd* pParent = NULL);
	virtual ~CESMFrameViewBalltracking();
	enum { IDD = IDD_FRAME_SEL };
	CESMFrameList* data_info;
	CObArray num_info;
	CString m_strNumIdx;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_selector;
	afx_msg void OnRadioCheck(UINT value);
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnBnClickedOk2();
	afx_msg void OnBnClickedCancel();
	track_info cam_sel;
	afx_msg void OnCbnSelchangeStandardCamSel();
	int m_nCheckState;
	CButton b_chkReverse;
	CButton b_UHD;
	CButton b_FHD;
	int m_nResolution_row;
	int m_nResolution_col;
	afx_msg void OnBnClickedRadioUhd();
	afx_msg void OnBnClickedAtBtnLeft();
	afx_msg void OnBnClickedAtBtnRight();

	//170529 hjcho
	void LoadData();
	void SaveData();
	CString m_strSection;

	CButton m_bShowTracking;
	afx_msg void OnBnClickedAtBtnHLeft();
	afx_msg void OnBnClickedAtBtnHRight();

	void SetFrameList(CESMFrameList* pParent);
	CESMFrameList* m_pDlgFrameList;

	CString m_strArrSection[4];
};
