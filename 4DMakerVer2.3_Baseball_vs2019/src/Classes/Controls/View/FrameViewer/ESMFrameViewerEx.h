#pragma once

#include "resource.h"
#include "ToolTipListCtrl.h"
#include "ESMFrameViewer.h"
#include "ESMUtilRemoteCtrl.h"

class CESMFrameViewerEx : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	DECLARE_DYNAMIC(CESMFrameViewerEx)

public:
	enum { IDD = IDD_VIEW_ONLY_TOOLBAR };

	CESMFrameViewerEx(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMFrameViewerEx();

	void InitImageFrameWnd();
	//CESMUtilRemoteCtrl* m_pRemoteCtrlDlg;
	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;
	CRect m_rcDlgSize,m_rcClientFrame;

	void Test();
	void OnBnSetZoom150();
	void OnBnSetZoom200();
	void OnBnSetZoom300();
	void OnBnSetZoom400();

	void SetFocusAsFrame();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
public:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSize(UINT nType, int cx, int cy);
};