#include "stdafx.h"
#include "ESMFrameAutoDetect.h"

CESMAutoDetect::CESMAutoDetect()
{
	ROI_col = 0;
	ROI_row = 0;
	Astr_x = 0;
	Astr_y = 0;
	ROI_size = ROI_col * ROI_row;
	cen = cv::Point(0,0);
	radius = 0;
	distance = INT_MAX;
	Matching_Frame = 0;
	circle_size = 0;
	Reverse = FALSE;
	m_nSearchingSize = 0;
	bCheckLeftOrRight = FALSE;
	nArrMatchSize = NULL;
	ptArrPoint = NULL;
	dbArrDistance = NULL;
}
CESMAutoDetect::~CESMAutoDetect()
{
	if(nArrMatchSize != NULL)
	{
		delete nArrMatchSize;
		nArrMatchSize = NULL;
	}
	if(ptArrPoint != NULL)
	{
		delete ptArrPoint;
		ptArrPoint = NULL;
	}
	if(dbArrDistance!= NULL)
	{
		delete dbArrDistance;
		dbArrDistance = NULL;
	}
}
void CESMAutoDetect::Otsu(Mat img, Mat result)
{
	int i,t;
	int total = 0;
	int hist[256];
	double prob[256];

	for(i=0;i<256;i++)
	{
		hist[i] = 0;
		prob[i] = 0.0f;
	}
	for(i=0;i<img.total();i++)
	{
		hist[img.data[i]]++;
	}
	for(i=0;i<256;i++)
	{
		prob[i] = (double) hist[i] / (double) img.total();
	}

	double wsv_min = 1000000.0f;
	double wsv_u1,wsv_u2, wsv_s1,wsv_s2;
	int wsv_t;

	for(t=0;t<256;t++)
	{
		double q1 = 0.0f, q2 = 0.0f;

		for(i=0;i<t;i++) q1 += prob[i];
		for(i=t;i<256;i++)q2 += prob[i];
		if(q1 == 0 || q2 == 0) continue;

		double u1 = 0.0f, u2 = 0.0f;
		for(i=0;i<t;i++) u1 += i*prob[i]; u1/=q1;
		for(i=t;i<256;i++) u2 += i*prob[i]; u2 /=q2;

		double s1=0.0f,s2=0.0f;
		for(i=0;i<t;i++) s1 += (i-u1) * (i-u1) * prob[i]; s1 /= q1;
		for(i=t;i<256;i++) s2 += (i-u2) * (i-u2) * prob[i]; s2 /= q2;

		float wsv = q1*s1 + q2*s2;

		if(wsv < wsv_min)
		{
			wsv_min = wsv;wsv_t = t;
			wsv_u1 = u1;wsv_u2 = u2;
			wsv_s1 = s1;wsv_s2 = s2;
		}

	}
	for(i=0;i<img.total();i++) if(img.data[i]<wsv_t)result.data[i] = 0; else result.data[i]= 255;
}
void CESMAutoDetect::labeling(const Mat img,int* table1,int* table2,int tsize,
	int* pass1,int* pass2,int*cnt_label)
{
	int x,y;
	int i;
	int cnt = 0;
	int label = 0;
	int row = img.rows;
	int col = img.cols;
	int size = row * col;
	memset(pass1,0,size*sizeof(int));
	memset(pass2,0,size*sizeof(int));

	for(i=0;i<tsize;i++)
		table1[i] = i;

	memset(table2,0,tsize*sizeof(int));

	for(y=1;y<row;y++){
		for(x=1;x<col;x++){
			int value = img.data[col * y + x];

			if(value>128)//foreground
			{
				int up,le;
				up = pass1[(y-1)*col + x];
				le = pass1[y*col + (x-1)];

				if(up == 0 && le == 0){
					++cnt;
					pass1[y*col + x] = cnt;
				}
				else if(up != 0 && le != 0){
					if(up>le){
						pass1[y*col + x] = le;
						table1[up] = table1[le];
					}else{
						pass1[y*col+x] = up;
						table1[le] = table1[up];
					}
				}else{
					pass1[y*col+x] = up+le;
				}
			}
		}
	}

	for(y=1;y<row;y++){
		for(x=1;x<col;x++){
			if(pass1[y*col+x]){
				int v = table1[pass1[y*col+x]];

				if(table2[v] == 0){
					++label;
					table2[v] = label;
				}

				pass2[y*col+x] = table2[v];
			}
		}
	}

	if(cnt_label != NULL)
		*cnt_label = label;
}
int CESMAutoDetect::show_label(Mat img,int*cnt_label, Mat ROI, int cnt)
{
	int *pass1,*pass2;
	int *table1,*table2;
	int col = img.cols;
	int row = img.rows;
	int size = col * row;

	int i,j,h,w;

	pass1=(int*)malloc(size * sizeof(int));
	pass2=(int*)malloc(size*sizeof(int));
	table1 = (int*) malloc(size/2*sizeof(int));
	table2 = (int*) malloc(size/2*sizeof(int));

	labeling(img,table1,table2,size/2,pass1,pass2,cnt_label);
	if(cnt_label == NULL) return -1;
	Mat tmp;
	tmp = img.clone();

	float *num,*tmp_x,*tmp_y;

	num = (float*) calloc(*cnt_label,sizeof(float));
	tmp_x = (float*) calloc(*cnt_label,sizeof(float));
	tmp_y = (float*) calloc(*cnt_label,sizeof(float));

	for(i=0;i<row;i++)
	{
		for(j=0;j<col;j++)
		{
			int label = pass2[col*i+j];

			if(label){
				tmp.data[col*i+j] = label;
				tmp_x[label-1] += j;
				tmp_y[label-1] += i;
				num[label-1] ++;
			}
		}
	}
	double tmp_distance;

	for(i=0;i<*cnt_label;i++)
	{
		int cen_x = (int) (tmp_x[i] / num[i]);
		int cen_y = (int) (tmp_y[i] / num[i]);

		if(num[i] < circle_size/2) continue;

		tmp_distance = sqrt((double)((cen.x - cen_x) * (cen.x - cen_x) + (cen.y - cen_y) * (cen.y - cen_y)));

		if((int)tmp_distance<distance)
		{
			distance = (int)tmp_distance;
			cv::Point ptPoint(cen_x,cen_y);
			ptArrPoint->at(cnt) = ptPoint;

			nArrMatchSize->at(cnt) = TRUE;

			dbArrDistance->at(cnt) = tmp_distance;

			Matching_Frame = cnt;
			return Matching_Frame;
		}
	}

	////�ȼ� Labeling ���� ���Ͱ�
	//Mat sub_ROI(50,50,CV_8UC1);
	//vector<Vec3f> circles;
	//cv::Point center;
	//double tmp_distance;
	//for(i=0;i<*cnt_label;i++)
	//{
	//	int cen_x = (int) (tmp_x[i] / num[i]);
	//	int cen_y = (int) (tmp_y[i] / num[i]);
	//	sub_ROI = Scalar(0);

	//	GaussianBlur(ROI,ROI,cv::Size(5,5),1.5);
	//	HoughCircles(ROI,circles,CV_HOUGH_GRADIENT,1,50,70,15,1,circle_size);
	//	for(j = 0; j<circles.size();j++)
	//	{
	//		center = cv::Point(cvRound(circles[j][0]),cvRound(circles[j][1]));
	//		radius = circles[j][2];
	//		circle(ROI,center,radius,Scalar(255,255,255),1,8);
	//	}

	//	for(h=-25;h<25;h++)
	//	{
	//		for(w=-25;w<25;w++)
	//		{
	//			if(cen_y + h < 0 || cen_y + h > ROI_row || cen_x + w < 0 || cen_x + w > ROI_col) continue;
	//			sub_ROI.data[50*(h+25)+(w+25)] = ROI.data[ROI.cols * (cen_y + h) + (cen_x + w)];
	//		} 
	//	}
	//	HoughCircles(sub_ROI,circles,CV_HOUGH_GRADIENT,1,50,70,15,1,circle_size);

	//	if(circles.empty()) continue;
	//	else
	//	{
	//		for(j = 0; j<circles.size();j++)
	//		{
	//			center = cv::Point(cvRound(circles[j][0]),cvRound(circles[j][1]));
	//			radius = circles[j][2];

	//			tmp_distance = sqrt((double)((cen.x - cen_x) * (cen.x - cen_x) + (cen.y - cen_y) * (cen.y - cen_y))); 

	//			if((int)tmp_distance<distance)
	//			{
	//				distance = (int)tmp_distance;
	//				Matching_Frame = cnt;
	//				return Matching_Frame;
	//			}

	//		}
	//	}
	//}

	return Matching_Frame;

	free(num);
	free(tmp_x); 
	free(tmp_y);
	free(pass1);
	free(pass2);
	free(table1);
	free(table2);
}
void CESMAutoDetect::CreateROI(Mat ori,Mat ROI)
{
	if(ori.channels() != 1 || ROI.channels() != 1)
	{
		printf("Set channel 1!\n");
		return;
	}

	int i,j;
	int row = ROI.rows;
	int col = ROI.cols;
	int ori_col = ori.cols;
	for(i=0;i<row;i++)
	{
		for(j=0;j<col;j++)
		{
			ROI.data[col*i+j] = ori.data[ori_col* (i+Astr_y) + (j+Astr_x)];
		}
	}
}
int CESMAutoDetect::VideoPlay(Mat input, int selected_frame)
{
	Mat ROI(ROI_row,ROI_col,CV_8UC1);
	Mat result(ROI_row,ROI_col,CV_8UC1);
	Mat b_result(ROI_row,ROI_col,CV_8UC1);
	int data;
	char str[100];

	//if(ESMGetReverseMovie()) flip(input,input,-1);
	//cvtColor(input,input,cv::COLOR_BGR2GRAY);
	CreateROI(input,ROI);


	Image_Subtract(ROI,result,selected_frame);
	Otsu(result,b_result);
	data = Counting_pix(b_result);

	if(data < (ROI_size/12))
	{
		mat = show_label(b_result,&label_cnt,ROI,selected_frame);
		//nArrMatchSize->at(selected_frame) = TRUE;
	}
	else
		nArrMatchSize->at(selected_frame) = FALSE;

	char strIdx[4];
	sprintf(strIdx,"%d",selected_frame);
	if(ESMGetTrackInfo().bShowTracking)
	{
		namedWindow("ROI",CV_WINDOW_FREERATIO);
		namedWindow("Binary",CV_WINDOW_FREERATIO);
		namedWindow("result",CV_WINDOW_FREERATIO);

		putText(ROI,strIdx,cv::Point(30,30),CV_FONT_HERSHEY_COMPLEX,2,Scalar(255,255,255),1,8);
		imshow("ROI",ROI);
		imshow("Binary",b_result);
		imshow("result",result);
		waitKey(0);
		destroyAllWindows();
	}
	return mat;
}
void CESMAutoDetect::Image_Subtract(Mat ROI,Mat result,int cnt)
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_IMAGE));

	CString strDSC;
	strDSC.Format(_T("temp_img.jpg"));
	CString strPath;
	strPath.Format(_T("%s%s"), strAdjustFolder, strDSC);
	char pchPath[MAX_PATH] = {0};
	int nRequiredSize = (int)wcstombs(pchPath, strPath, MAX_PATH);

	double AVG = 0;
	int max = 0;
	int i,j;
	GaussianBlur(ROI,ROI,cv::Size(5,5),0,0);
	if(cnt == 0){
		imwrite(pchPath,ROI);
		return;
	}
	else
	{
		Mat temp = imread(pchPath,0);
		subtract(ROI,temp,result,noArray(),-1);
		//for(i=0;i<result.total();i++)
		//{
		//	int data =  result.data[i];
		//	AVG += result.data[i];
		//	if(data > max) max = data;
		//}
		//AVG = ceil(AVG/result.total());

		//int threshold = max / (int)AVG;

		//for(i=0;i<result.rows;i++)
		//{
		//	for(j=0;j<result.cols;j++)
		//	{
		//		int data = result.data[result.cols*i+j];
		//		if(data<threshold) data = 0;
		//		result.data[result.cols*i+j] = data;
		//	}
		//}
	}
}
int CESMAutoDetect::Counting_pix(Mat b_result)
{
	int cnt = 0;

	for(int i=0;i<ROI_size;i++)
	{
		int pixel = b_result.data[i];
		if(pixel == 255) cnt++;
	}
	return cnt;
}
void CESMAutoDetect::RemoveNoise(Mat img)
{
	Mat element(5,5,CV_8U);

	erode(img,img,element,cv::Point(-1,-1),1,0);
}
void CESMAutoDetect::SetDetectingInfo(int Rstr_x,int Rstr_y,int Rend_x,int Rend_y,int circle,BOOL reverse,CString strDSC,int nSearchSize)
{
	ROI_col = Rend_x - Rstr_x + 1;
	ROI_row = Rend_y - Rstr_y + 1;
	ROI_size = ROI_col * ROI_row;
	Astr_x = Rstr_x;
	Astr_y = Rstr_y;
	Reverse = reverse;
	circle_size = circle;
	mat = 0;
	track_info stTrackInfo;
	stTrackInfo = ESMGetTrackInfo();

	if(nArrMatchSize == NULL)
		nArrMatchSize = new vector<BOOL>(nSearchSize);
	if(ptArrPoint == NULL)
		ptArrPoint = new vector<cv::Point>(nSearchSize);
	if(dbArrDistance == NULL)
		dbArrDistance = new vector<double>(nSearchSize);

	m_PointCenter = cv::Point(ROI_col/2,ROI_row/2);

	if(Reverse && !ESMGetReverseMovie())
	{	
		if(stTrackInfo.st_AxisInfo[0].strCamID == strDSC || stTrackInfo.st_AxisInfo[3].strCamID == strDSC)
		{
			cen = cv::Point(ROI_col*0.9,ROI_row*0.1);

		}
		else if(stTrackInfo.st_AxisInfo[1].strCamID == strDSC || stTrackInfo.st_AxisInfo[2].strCamID == strDSC)
		{
			cen = cv::Point(ROI_col*0.1,ROI_row*0.1);
		}
	}
	else
	{
		if(stTrackInfo.st_AxisInfo[0].strCamID == strDSC || stTrackInfo.st_AxisInfo[3].strCamID == strDSC)//��Ÿ��, �¿�
		{
			cen = cv::Point(ROI_col*0.2,ROI_row*0.5);
			bCheckLeftOrRight = FALSE;
		}
		else if(stTrackInfo.st_AxisInfo[1].strCamID == strDSC || stTrackInfo.st_AxisInfo[2].strCamID == strDSC)//��Ÿ��, ���
		{
			cen = cv::Point(ROI_col*0.8,ROI_row*0.5);
			bCheckLeftOrRight = TRUE;
		}
	}
}
int CESMAutoDetect::ATCheckValid(int nMatchFrame)
{
	int nRealFrame = -1;
	int nCnt = 0,nNextIdx = -1;
	double dbDistance = 0.0,dbMinDistance = 999999999;
	BOOL bFind = FALSE;
	for(int i = 0 ; i < nArrMatchSize->size(); i++)
	{
		if(nNextIdx != -1 && nCnt >= 2 && bFind)
			break;

		if(nRealFrame != -1 && bFind == TRUE && nCnt == 0)
			break;

		
		if(nArrMatchSize->at(i) == FALSE)
		{
			nCnt++;
			continue;
		}
		else
		{
			cv::Point ptPoint = ptArrPoint->at(i);
			
			//���� ��ǥ���� �Ÿ�
			dbDistance = sqrt((double)((cen.x - ptPoint.x) * (cen.x - ptPoint.x) 
								+ (cen.y - ptPoint.y) * (cen.y - ptPoint.y)));
			
			if(dbDistance < dbMinDistance)
				dbMinDistance = dbDistance;
			else
				continue;
	
			nNextIdx = CheckSearchArea(ptPoint,bCheckLeftOrRight);

			nRealFrame = i;
			nCnt = 0;
			bFind = TRUE;
		}
	}
	
	if(nArrMatchSize != NULL)
	{
		delete nArrMatchSize;
		nArrMatchSize = NULL;
	}
	if(ptArrPoint != NULL)
	{
		delete ptArrPoint;
		ptArrPoint = NULL;
	}
	if(dbArrDistance != NULL)
	{
		delete dbArrDistance;
		dbArrDistance = NULL;
	}

	return nRealFrame + nNextIdx;
}
int CESMAutoDetect::CheckSearchArea(cv::Point ptPoint,BOOL bWheterLeftorRight)
{
	int nArea;
	int nAreaX[3] = {ROI_col/5,ROI_col/2,ROI_col*4/5};

	if(bWheterLeftorRight)//��Ÿ��, ���
	{
		if(ptPoint.x >= 0 && ptPoint.x < nAreaX[0])
			nArea = 3;
		else if(ptPoint.x >= nAreaX[0] && ptPoint.x < nAreaX[2])
			nArea = 2;
		/*
		else if(ptPoint.x >= nAreaX[1] && ptPoint.x < nAreaX[2])
					nArea = 2;*/
		else
			nArea = 1;
	}
	else//��Ÿ��, �¿�
	{
		if(ptPoint.x >= 0 && ptPoint.x < nAreaX[0])
			nArea = 1;
		else if(ptPoint.x >= nAreaX[0] && ptPoint.x < nAreaX[2])
			nArea = 2;
/*
		else if(ptPoint.x >= nAreaX[1] && ptPoint.x < nAreaX[2])
			nArea = 3;*/
		else
			nArea = 3;
	}

	return nArea;
}