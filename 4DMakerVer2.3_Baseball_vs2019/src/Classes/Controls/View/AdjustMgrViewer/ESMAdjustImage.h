#pragma once


#include "cv.h"
// ESMAdjustImage

class ESMAdjustImage : public CStatic
{
	DECLARE_DYNAMIC(ESMAdjustImage)

public:
	ESMAdjustImage();
	virtual ~ESMAdjustImage();

	CDC* m_memDC;
	BITMAP m_bmpinfo;	
	double m_dMultiple;

	//wgkim 17-06-20
	double m_dPrevMultiple;

	int m_nImageWidth, m_nImageHeight;
	int m_nCircleSize;
	BOOL m_bImageMove;
	CPoint m_ptImageMoveStart, m_ptImageMoving, m_ptImageLeftTop;
//	BOOL InitSetup();

	BOOL SetImageBuffer(BYTE* pImage, int nWidth, int nHeight, int nChannel);
	void SetImagePos(int nPosX, int nPosY);
	void Redraw();
	void ImageZoom(CPoint ptMouse, BOOL bZoom);
	double GetMultiple() { return m_dMultiple;}
	CPoint GetImagePos() { return m_ptImageLeftTop; }

	//-- 2014-08-30 hongsu@esmlab.com
	//-- Find Exact Position
	BYTE* m_pTpImage;
	IplImage* m_pImage;
	IplImage* BYTE2IplImage(BYTE* pByte, int nWidth, int nHeight);
	CPoint FindExactPosition(int& nX, int& nY, int nThreshhold, BOOL bAutoFind);
	void GetImageRGB(int nX, int nY, int& nColorR, int& nColorG, int& nColorB);	
	CPoint FindCenter(int nX, int nY, CRect rtArea);	
	void DrawRect(CRect rtDraw, COLORREF color);
	void DrawLine(int nHeight, COLORREF color);
	void DrawPoint(int nX, int nY, COLORREF color);
	void DrawCenterLine(int nWidth, COLORREF color);
	void DrawZoom(float nZoom, CRect* rt, CPoint margin);
	void WriteRGB(IplImage*	pImgArea, int nWidth, int nHeight, int &nMin, int &nMax, int &nRgbAvg);
	void FindCenterRegion(IplImage*	pImgArea, int nLength,  CPoint& ptCenter, int nRgbMin, int nRgbMax, int nRgbAvg);
	int GetRGBAvg(IplImage*	pImgArea, int nX, int nY);

protected:
	BOOL DrawDefectPoint(int PosX, int PosY, COLORREF color);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};


