// ESMAdjustMgrDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#ifdef _4DMODEL
#include "4DModeler.h"
#else
#include "4DMaker.h"
#endif
#include "ESMAdjustMgrDlg.h"
#include "afxdialogex.h"
#include "FFmpegManager.h"
#include "ESMIni.h"
#include "ESMCtrl.h"
#include "ESMAdjustImage.h"
#include "ESMFileOperation.h"
#include "ESMFunc.h"
#include "ESMMovieMgr.h"
#include "ESMImgMgr.h"

#include <math.h>

#define PI 3.1415926535897932384
#define TIMER_CHANGE_NEXT_ITEM	0x8001
#define TIME_CHANGE_NEXT_ITEM	700

// ESMAdjustMgrDlg 대화 상자입니다.
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


enum{
	FIRST_ADJUST_VIEW = 0,
	SECOND_ADJUST_VIEW,
	THIRD_ADJUST_VIEW,
	KZONE_PRISM_VIEW,
};

enum{
	ADJUST_IMAGE = 0,
	ORIGINAL_IMAGE,
	AFTER_IMAGE,
	RE_IMAGE,
	MAX_IMAGE_VIEW,
};

enum{
	HOME_BASE_POINT = 0,
	FIRST_BASE_POINT,
	SECOND_BASE_POINT,
	THIRD_BASE_POINT,
	PITCHER_BASE_POINT,
	MAX_BASE_POINT,
};

enum{
	ADJUST_LEFT_UP = 0,
	ADJUST_LEFT_DOWN,
	ADJUST_RIGHT_UP,
	ADJUST_RIGHT_DOWN,
};

IMPLEMENT_DYNAMIC(ESMAdjustMgrDlg, CDialogEx)


	ESMAdjustMgrDlg::ESMAdjustMgrDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(ESMAdjustMgrDlg::IDD, pParent)
	, m_recals(0)
{
	m_nImgView = ADJUST_IMAGE;
	m_nView = FIRST_ADJUST_VIEW;
	m_nSelectedNum = -1;
	m_nPosX = 0, m_nPosY = 0;
	m_nSelectPos = 0;
	m_nThreshold = 0;
	m_hTheadHandle = NULL;
	m_nMovieState = ESM_ADJ_FILM_STATE_MOVIE;
	m_bAutoFind = FALSE;
	m_bCenterGrid = FALSE;
	m_bAutoMove = TRUE;
	m_bZoomUse = TRUE;
	m_bDistanceUse = TRUE;
	m_nAdjViewHeight = 0;
	m_nAdjViewWidth = 0;
	m_nTargetLenth = 0;
	m_nCameraZoom = 0;	
	m_bChangeFlag = FALSE;

	m_MarginX = 0;
	m_MarginY = 0;

	//jhhan
	m_nCurWidth			= 0;
	m_nCurHeight		= 0;
	//m_nHScrollPos		= 0;
	//m_nVScrollPos		= 0;
	m_bThreadLoad		= FALSE;
	m_bThreadStop		= TRUE;

	//wgkim 17-06-21
	m_bThreadCloseFlag	= FALSE;
	m_dThresholdFocus	= 0.f;
	m_bAdjSave			= FALSE;

	m_background.CreateSolidBrush(RGB(44,44,44));
	m_AdjustDscList.SetUseCamStatusColor(FALSE);

}

ESMAdjustMgrDlg::~ESMAdjustMgrDlg()
{
	m_bThreadCloseFlag	= TRUE;

	WaitForSingleObject(m_hTheadHandle, INFINITE);
	CloseHandle(m_hTheadHandle);
}

void ESMAdjustMgrDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX,	IDC_ST_ADJUSTIMAGE_RECALIBRATION, m_StAdjustImageRe);
	DDX_Control(pDX,	IDC_ST_ADJUSTIMAGE,			m_StAdjustImage);
	DDX_Control(pDX,	IDC_ST_ADJUSTIMAGE_ORIGIN,	m_StAdjustImageOrigin);
	DDX_Control(pDX,	IDC_ST_ADJUSTIMAGE_AFTER,	m_StAdjustImageAfter);
	DDX_Control(pDX,	IDC_ADJUSTDSCLISTCTRL,		m_AdjustDscList);
	DDX_Control(pDX,	IDC_ADJUSTDSCLISTCTRL_2ND,	m_AdjustDsc2ndList);
	DDX_Radio(	pDX,	IDC_RD_ADJUSTVIEWCOLOR,		m_RdImageViewColor);
	DDX_Radio(	pDX,	IDC_RD_ADJUST_DETECT_BLACK,	m_nRdDetectColor);
	DDX_Check(	pDX,	IDC_CHECK_AUTOFIND,			m_bAutoFind);
	DDX_Check(	pDX,	IDC_CHECK_CENTER_GRID,		m_bCenterGrid);
	DDX_Check(	pDX,	IDC_CHECK_CAMERAZOOM,		m_bZoomUse);
	DDX_Check(	pDX,	IDC_CHECK_CAMERADISTANCE,	m_bDistanceUse);
	DDX_Check(	pDX,	IDC_CHECK_AUTOMOVE,			m_bAutoMove);
	DDX_Control(pDX, IDC_EDT_ADJUST_FRAMEINDEx, m_edit_frameIndex);
	DDX_Control(pDX, IDC_OPTION_2ND_ADJUST, m_ctrl2ndOption);
	DDX_Control(pDX, IDC_STATIC_CENTERLINE, m_stCenterLine);
	DDX_Control(pDX, IDC_EDT_CENTER_LINE, m_ctrlCenterLine);
	DDX_Control(pDX, IDC_BTN_2ND_SAVEPOINT, m_btnSavePoint);
	DDX_Control(pDX, IDC_BTN_2ND_SAVEPOINT_ADJ, m_btnSavePointAdj);
	DDX_Control(pDX, IDC_BTN_COPY_POINT, m_btnCopyPoint);
	DDX_Control(pDX, IDC_BTN_2ND_LOADPOINT, m_btnLoadPoint);
	DDX_Control(pDX, IDC_BTN_ROTATE_CAL, m_btnCalculate);
	DDX_Control(pDX, IDC_BTN_MOVE_CAL, m_btnMoveCal);
	DDX_Control(pDX, IDC_BTN_SAVE_ADJUST, m_btnSaveAdjust);
	DDX_Control(pDX, IDC_CMB_CHECK_POINT, m_ctrlCheckPoint);
	DDX_Control(pDX, IDC_BTN_LEFT,			m_btnLeft);
	DDX_Control(pDX, IDC_BTN_RIGHT,			m_btnRight);
	DDX_Control(pDX, IDC_BTN_UP,			m_btnUp);
	DDX_Control(pDX, IDC_BTN_DOWN,			m_btnDown);
	DDX_Control(pDX, IDC_BTN_ROTATE_LEFT,	m_btnRotateLeft);
	DDX_Control(pDX, IDC_BTN_ROTATE_RIGHT,	m_btnRotateRight);
	DDX_Control(pDX, IDC_BTN_SAVE_IMG,		m_btnSaveImg);
	DDX_Control(pDX, IDC_BTN_LOAD_IMAGE,		m_btnLoadImg);
	DDX_Control(pDX, IDC_BTN_RE_SAVE_ADJUST,	m_btnReSaveAdjust);
	DDX_Control(pDX, IDC_Recal_Sub, m_recal_sub);
	DDX_Control(pDX, IDC_Recal_Puzzle, m_recal_puz);
	DDX_Control(pDX, IDC_Recal_Puzzle, m_recal_puz);
	DDX_Control(pDX, IDC_BTN_ADJUSTSAVE, m_btnSaveAdjust_View1);

	DDX_Control(pDX, IDC_EDT_BOX_SIZE, m_ctrlBoxSize);
	DDX_Control(pDX, IDC_EDT_LINE1, m_editLine1);
	DDX_Control(pDX, IDC_EDT_LINE2, m_ctrlLine2);

	//DDX_Control(pDX,IDC_BTN_ADJUSTPOINTSAVE,m_btnSavePoint);
	//DDX_Control(pDX, IDC_ADJDLG_GRP_POINTSET, m_ctrlGrpPointSetting);
	//DDX_Control(pDX, IDC_ADJDLG_GRP_ACTION, m_ctrlGrpAction);
	//DDX_Control(pDX, IDC_ADJDLG_GRP_POINTLOAD, m_ctrlGrpPointLoad);
	//DDX_Control(pDX, IDC_ADJDLG_GRP_DISTANCEREG, m_ctrlGrpDisReg);
	//DDX_Control(pDX, IDC_ADJDLG_GRP_FOCUS, m_ctrlGrpFocus);
	//DDX_Control(pDX, IDC_ADJDLG_GRP_INFO, m_ctrlGrpInfo);
	//DDX_Control(pDX, IDC_ADJDLG_GRP_FRAMEINDEX, m_ctrlGrpFramdIdx);
	DDX_Control(pDX, IDC_BTN_CALCADJUST, m_btnCalcAdjust);
	DDX_Control(pDX, IDC_BTN_CALCDISTANCE, m_btnCalcDistance);
	DDX_Control(pDX, IDC_BTN_ADJUSTPOINTSAVE, m_btnPointSave);
	DDX_Control(pDX, IDC_BTN_ADJUSTPOINTLOAD, m_btnPointLoad);
	DDX_Control(pDX, IDC_BTN_ADJUSTPOINTRESET, m_btnPointReset);
	DDX_Control(pDX, IDC_BTN_DISTANCEREGI, m_btnDistanceReg);
	DDX_Control(pDX, IDC_BTN_ALLREGIST, m_btnAllReg);
	DDX_Control(pDX, IDC_BTN_DISTANCEDEL, m_btnDistanceDelete);
	DDX_Control(pDX, IDC_BTN_DISTANCERESET, m_btnDistanceClear);
	DDX_Control(pDX, IDC_BTN_FOCUS_SAVE, m_btnFocusSave);
	DDX_Control(pDX, IDC_BTN_FOCUS_DELETE, m_btnFocusDelete);
	DDX_Control(pDX, IDC_BTN_FOCUS_CALCULATION, m_btnFocusCalc);
	DDX_Control(pDX, IDC_BTN_COMMIT, m_btnFrameCommit);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(ESMAdjustMgrDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_SAVE_ADJUST, &ESMAdjustMgrDlg::OnBnClickedBtnSaveAdjus)
	ON_BN_CLICKED(IDC_BTN_MOVE_CAL, &ESMAdjustMgrDlg::OnBnClickedBtnMoveCal)
	ON_BN_CLICKED(IDC_BTN_ROTATE_CAL, &ESMAdjustMgrDlg::OnBnClickedBtnRotateCal)
	ON_BN_CLICKED(IDC_BTN_2ND_SAVEPOINT_ADJ, &ESMAdjustMgrDlg::OnBnClickedBtn2ndSavePointAdj)
	ON_BN_CLICKED(IDC_BTN_COPY_POINT, &ESMAdjustMgrDlg::OnBnClickedBtnCopyPoint)
	ON_BN_CLICKED(IDC_BTN_2ND_SAVEPOINT, &ESMAdjustMgrDlg::OnBnClickedBtn2ndSavePoint)
	ON_BN_CLICKED(IDC_BTN_2ND_LOADPOINT, &ESMAdjustMgrDlg::OnBnClickedBtn2ndLoadPoint)
	ON_BN_CLICKED(IDC_BTN_LEFT, &ESMAdjustMgrDlg::OnBnClickedBtnLeft)
	ON_BN_CLICKED(IDC_BTN_RIGHT, &ESMAdjustMgrDlg::OnBnClickedBtnRight)
	ON_BN_CLICKED(IDC_BTN_UP, &ESMAdjustMgrDlg::OnBnClickedBtnUp)
	ON_BN_CLICKED(IDC_BTN_DOWN, &ESMAdjustMgrDlg::OnBnClickedBtnDown)
	ON_BN_CLICKED(IDC_BTN_ROTATE_LEFT, &ESMAdjustMgrDlg::OnBnClickedBtnRotateLeft)
	ON_BN_CLICKED(IDC_BTN_ROTATE_RIGHT, &ESMAdjustMgrDlg::OnBnClickedBtnRotateRight)
	ON_BN_CLICKED(IDC_BTN_SAVE_IMG, &ESMAdjustMgrDlg::OnBnClickedBtnSaveImg)
	ON_BN_CLICKED(IDC_BTN_LOAD_IMAGE, &ESMAdjustMgrDlg::OnBnClickedBtnLoadImg)
	ON_BN_CLICKED(IDC_BTN_RE_SAVE_ADJUST, &ESMAdjustMgrDlg::OnBnClickedBtnReSaveAdjust)
	ON_BN_CLICKED(IDC_BTN_VIEW1, &ESMAdjustMgrDlg::OnBnClickedBtnView1)
	ON_BN_CLICKED(IDC_BTN_VIEW2, &ESMAdjustMgrDlg::OnBnClickedBtnView2)
	ON_BN_CLICKED(IDC_BTN_VIEW3, &ESMAdjustMgrDlg::OnBnClickedBtnView3)
	ON_BN_CLICKED(IDOK, &ESMAdjustMgrDlg::OnBnClickedOk)
	ON_WM_TIMER()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDBLCLK()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_ADJUSTDSCLISTCTRL, &ESMAdjustMgrDlg::OnLvnItemchangedAdjust)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_ADJUSTDSCLISTCTRL_2ND, &ESMAdjustMgrDlg::OnLvnItemchangedAdjust2nd)
	ON_BN_CLICKED(IDC_BTN_AUTOSEARCH, &ESMAdjustMgrDlg::OnBnClickedBtnAutosearch)
	ON_BN_CLICKED(IDC_RD_ADJUSTVIEWCOLOR, &ESMAdjustMgrDlg::OnBnClickedRdAdjustviewcolor)
	ON_BN_CLICKED(IDC_RD_ADJUSTVIEWGRAY, &ESMAdjustMgrDlg::OnBnClickedRdAdjustviewgray)
	ON_BN_CLICKED(IDC_BTN_CALCADJUST, &ESMAdjustMgrDlg::OnBnClickedBtnCalcadjust)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_ADJUSTDSCLISTCTRL, OnCustomdrawTcpList)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_ADJUSTDSCLISTCTRL_2ND, OnCustomdrawTcpList2nd)
	ON_BN_CLICKED(IDC_BTN_ADJUSTPOINTLOAD, &ESMAdjustMgrDlg::OnBnClickedBtnAdjustpointload)
	ON_BN_CLICKED(IDC_BTN_ADJUSTPOINTSAVE, &ESMAdjustMgrDlg::OnBnClickedBtnAdjustpointsave)
	ON_EN_KILLFOCUS(IDC_EDT_LINE2, &ESMAdjustMgrDlg::OnEnKillfocusEdtLine2)
	ON_EN_KILLFOCUS(IDC_EDT_LINE1, &ESMAdjustMgrDlg::OnEnKillfocusEdtLine1)
	ON_BN_CLICKED(IDC_BTN_CALCDISTANCE, &ESMAdjustMgrDlg::OnBnClickedBtnCalcdistance)
	ON_BN_CLICKED(IDC_RD_ADJUST_DETECT_BLACK, &ESMAdjustMgrDlg::OnBnClickedRdAdjustDetectBlack)
	ON_BN_CLICKED(IDC_RD_ADJUST_DETECT_WHITE, &ESMAdjustMgrDlg::OnBnClickedRdAdjustDetectWhite)
	ON_BN_CLICKED(IDC_BTN_DISTANCEREGI, &ESMAdjustMgrDlg::OnBnClickedBtnDistanceAdd)
	ON_BN_CLICKED(IDC_BTN_DISTANCEDEL, &ESMAdjustMgrDlg::OnBnClickedBtnDistanceDel)
	ON_BN_CLICKED(IDC_BTN_DISTANCERESET, &ESMAdjustMgrDlg::OnBnClickedBtnDistanceReset)
	ON_BN_CLICKED(IDC_BTN_ADJUSTPOINTRESET, &ESMAdjustMgrDlg::OnBnClickedBtnAdjustPointReset)
	ON_BN_CLICKED(IDC_BTN_COMMIT, &ESMAdjustMgrDlg::OnBnClickedBtnCommit)
	ON_CONTROL_RANGE(BN_CLICKED,IDC_Recal_Sub,IDC_Recal_Puzzle,&ESMAdjustMgrDlg::OnRadioCheck)
//	ON_WM_SIZE()
//	ON_WM_HSCROLL()
//	ON_WM_VSCROLL()
	ON_BN_CLICKED(IDC_CHECK_AUTOFIND, &ESMAdjustMgrDlg::OnBnClickedBtnAutoFind)
	ON_BN_CLICKED(IDC_BTN_ADJUSTSAVE, &ESMAdjustMgrDlg::OnBnClickedSaveAdjust)
	ON_BN_CLICKED(IDC_BTN_FOCUS_CALCULATION,&ESMAdjustMgrDlg::OnBnClickedBtnFocus)
	ON_BN_CLICKED(IDC_BTN_FOCUS_SAVE,&ESMAdjustMgrDlg::OnBnClickedBtnFocusSave)	
	ON_BN_CLICKED(IDC_BTN_FOCUS_DELETE,&ESMAdjustMgrDlg::OnBnClickedBtnFocusDelete)	
	ON_BN_CLICKED(IDC_BTN_ALLREGIST,&ESMAdjustMgrDlg::OnBnClickedAllReg)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()
void ESMAdjustMgrDlg::OnRadioCheck(UINT value)
{
	int nCheck = GetCheckedRadioButton(IDC_Recal_Sub,IDC_Recal_Puzzle);

	if(nCheck == IDC_Recal_Puzzle)
	{
		m_recals = 2;
	}
	else if(nCheck == IDC_Recal_Sub)
	{
		m_recals = 1;
	}
	else
	{
		m_recals = 0;
	}
}
void ESMAdjustMgrDlg::OnBnClickedBtnSaveAdjus()
{
	for(int i = 0; i < m_pAdjustMgr.m_ArrDscInfo.size(); i++)
	{
		DscAdjustInfo* pinfo = m_pAdjustMgr.m_ArrDscInfo.at(i);
		pinfo->dAngle = m_adjinfo.at(i).AdjAngle;
		pinfo->dAdjustX = m_adjinfo.at(i).AdjMove.x;
		pinfo->dAdjustY = m_adjinfo.at(i).AdjMove.y;		
	}

	ESMEvent* pMsg = new ESMEvent();  
	pMsg->message = WM_ESM_MOVIE_SAVE_ADJUST;
	pMsg->pParam = (LPARAM)& m_adjinfo;
	pMsg->nParam1 = m_MarginX;
	pMsg->nParam2 = m_MarginY;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
}
void ESMAdjustMgrDlg::OnBnClickedBtnMoveCal()
{
	BOOL org_bflag = FALSE, adj_bflag = FALSE;
	CPoint org, adj;
	CString strData;

	CPoint org_gap, adj_gap;
	double org_tan, adj_tan;
	double org_ang, adj_ang;
	double angle_gap;
	m_MarginX = 0;
	m_MarginY = 0;
	for(int i = 0; i < m_AdjustDsc2ndList.GetItemCount(); i++)
	{
		org.x = _ttoi(m_AdjustDsc2ndList.GetItemText(i, 10));
		org.y = _ttoi(m_AdjustDsc2ndList.GetItemText(i, 11));

		adj.x = _ttoi(m_AdjustDsc2ndList.GetItemText(i, 20));
		adj.y = _ttoi(m_AdjustDsc2ndList.GetItemText(i, 21));


		if(adj.x > 0 && adj.y > 0)
		{
			stAdjustInfo* AdjustData = &m_adjinfo.at(i);
			//AdjustData->AdjMove.x = AdjustData->AdjMove.x + (org.x - adj.x);
			//AdjustData->AdjMove.y = AdjustData->AdjMove.y + (org.y - adj.y);

			AdjustData->AdjMove.x = m_adjinfo.at(i).AdjSize * (double)(org.x - adj.x);
			AdjustData->AdjMove.y = m_adjinfo.at(i).AdjSize * (double)(org.y - adj.y);

			DscAdjustInfo* pAdjustInfo = m_pAdjustMgrAfter.GetDscAt(i);
			pAdjustInfo->bAdjust = FALSE;
			pAdjustInfo->bRotate = TRUE;
			pAdjustInfo->bMove = FALSE;
			pAdjustInfo->bImgCut = FALSE;;

			stAdjustInfo adjInfo = m_adjinfo.at(i);
			CESMImgMgr* pImgMgr = new CESMImgMgr();
			pImgMgr->SetMargin(m_MarginX, m_MarginY, adjInfo);
		}

	}
}
void ESMAdjustMgrDlg::OnBnClickedBtnRotateCal()
{
	CPoint org1, org2;
	CPoint adj1, adj2;
	CPoint org[MAX_BASE_POINT];
	CPoint adj[MAX_BASE_POINT];
	double org1_distance, org2_distance;
	double adj1_distance, adj2_distance;
	int nOrgPos, nAdjPos, nBase1, nBase2;
	double org_x, org_y, adj_x, adj_y;



	BOOL org_bflag = FALSE, adj_bflag = FALSE;

	CString strData;

	CPoint org_gap, adj_gap;
	double org_tan, adj_tan;
	double org_ang, adj_ang;
	double angle_gap;
	for(int i = 0; i < m_AdjustDsc2ndList.GetItemCount(); i++)
	{
		org[HOME_BASE_POINT].x		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 2));
		org[HOME_BASE_POINT].y		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 3));
		org[FIRST_BASE_POINT].x		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 4));
		org[FIRST_BASE_POINT].y		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 5));
		org[SECOND_BASE_POINT].x	= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 6));
		org[SECOND_BASE_POINT].y	= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 7));
		org[THIRD_BASE_POINT].x		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 8));
		org[THIRD_BASE_POINT].y		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 9));

		adj[HOME_BASE_POINT].x		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 12));
		adj[HOME_BASE_POINT].y		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 13));
		adj[FIRST_BASE_POINT].x		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 14));
		adj[FIRST_BASE_POINT].y		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 15));
		adj[SECOND_BASE_POINT].x	= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 16));
		adj[SECOND_BASE_POINT].y	= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 17));
		adj[THIRD_BASE_POINT].x		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 18));
		adj[THIRD_BASE_POINT].y		= _ttoi(m_AdjustDsc2ndList.GetItemText(i, 19));

		//-- Distance
		org1_distance = sqrt(pow((double)org[HOME_BASE_POINT].x - (double)org[SECOND_BASE_POINT].x, 2) + pow((double)org[HOME_BASE_POINT].y - (double)org[SECOND_BASE_POINT].y, 2));
		org2_distance = sqrt(pow((double)org[FIRST_BASE_POINT].x - (double)org[THIRD_BASE_POINT].x, 2) + pow((double)org[FIRST_BASE_POINT].y - (double)org[THIRD_BASE_POINT].y, 2));

		adj1_distance = sqrt(pow((double)adj[HOME_BASE_POINT].x - (double)adj[SECOND_BASE_POINT].x, 2) + pow((double)adj[HOME_BASE_POINT].y - (double)adj[SECOND_BASE_POINT].y, 2));
		adj2_distance = sqrt(pow((double)adj[FIRST_BASE_POINT].x - (double)adj[THIRD_BASE_POINT].x, 2) + pow((double)adj[FIRST_BASE_POINT].y - (double)adj[THIRD_BASE_POINT].y, 2));
		//


		//------------
		if(org1_distance > org2_distance) //홈베이스, 2루베이스 기준
		{
			nBase1 = HOME_BASE_POINT;
			nBase2 = SECOND_BASE_POINT;
		}
		else //1루베이스, 3루베이스 기준
		{
			nBase1 = FIRST_BASE_POINT;
			nBase2 = THIRD_BASE_POINT;
		}

		if(org[nBase1].x < org[nBase2].x)// Left
		{
			if(org[nBase1].y > org[nBase2].y) // up
				nOrgPos = ADJUST_LEFT_UP;
			else // down
				nOrgPos = ADJUST_LEFT_DOWN;
		}
		else // Right
		{
			if(org[nBase1].y > org[nBase2].y) // up
				nOrgPos = ADJUST_RIGHT_UP;
			else // down
				nOrgPos = ADJUST_RIGHT_DOWN;
		}

		if(adj[nBase1].x < adj[nBase2].x)// Left
		{
			if(adj[nBase1].y > adj[nBase2].y) // up
				nAdjPos = ADJUST_LEFT_UP;
			else // down
				nAdjPos = ADJUST_LEFT_DOWN;
		}
		else // Right
		{
			if(adj[nBase1].y > adj[nBase2].y) // up
				nAdjPos = ADJUST_RIGHT_UP;
			else // down
				nAdjPos = ADJUST_RIGHT_DOWN;
		}
		//------------------------


		//------------- Rotate 각구하기
		org_gap.x = abs(org[nBase1].x - org[nBase2].x);
		org_gap.y = abs(org[nBase1].y - org[nBase2].y);
		org_tan = atan2((double)org_gap.y, (double)org_gap.x) * 180.0 / PI;
		org_ang = org_tan;

		adj_gap.x = abs(adj[nBase1].x - adj[nBase2].x);
		adj_gap.y = abs(adj[nBase1].y - adj[nBase2].y);
		adj_tan = atan2((double)adj_gap.y, (double)adj_gap.x) * 180.0 / PI;
		adj_ang = adj_tan;



		switch(nOrgPos)
		{
		case ADJUST_LEFT_UP:
			if(nOrgPos == nAdjPos)
				angle_gap = org_ang - adj_ang;
			else
				angle_gap = org_ang + adj_ang;
			break;
		case ADJUST_LEFT_DOWN:
			if(nOrgPos == nAdjPos)
				angle_gap = -1* (org_ang - adj_ang);
			else
				angle_gap = -1* (org_ang + adj_ang);
			break;
		case ADJUST_RIGHT_UP:
			if(nOrgPos == nAdjPos)
				angle_gap = -1* (org_ang - adj_ang);
			else
				angle_gap = -1* (org_ang + adj_ang);
			break;
		case ADJUST_RIGHT_DOWN:
			if(nOrgPos == nAdjPos)
				angle_gap = org_ang - adj_ang;
			else
				angle_gap = org_ang + adj_ang;
			break;
		}

		//------------


		//------------ Rotate 적용
		stAdjustInfo* AdjustData = &m_adjinfo.at(i);
		CString strData;
		strData.Format(_T("%02f"), AdjustData->AdjAngle );
		m_AdjustDsc2ndList.SetItemText(i, 22, strData);

		AdjustData->AdjAngle = AdjustData->AdjAngle - angle_gap;

		strData.Format(_T("%02f"), angle_gap);
		m_AdjustDsc2ndList.SetItemText(i, 23, strData);

		strData.Format(_T("%02f"), AdjustData->AdjAngle);
		m_AdjustDsc2ndList.SetItemText(i, 24, strData);

		strData.Format(_T("%d"), nOrgPos);
		m_AdjustDsc2ndList.SetItemText(i, 25, strData);

		DscAdjustInfo* pAdjustInfo = m_pAdjustMgrAfter.GetDscAt(i);
		pAdjustInfo->bAdjust = FALSE;
		pAdjustInfo->bRotate = FALSE;
		pAdjustInfo->bMove = TRUE;
		pAdjustInfo->bImgCut = TRUE;;
		//----------------

		/*

		stAdjustInfo* AdjustData = &m_adjinfo.at(i);

		CString strData;
		strData.Format(_T("%02f"), AdjustData->AdjAngle );
		m_AdjustDsc2ndList.SetItemText(i, 14, strData);

		AdjustData->AdjAngle2 = -90 + angle_gap;

		strData.Format(_T("%02f"), angle_gap);
		m_AdjustDsc2ndList.SetItemText(i, 15, strData);

		strData.Format(_T("%02f"), AdjustData->AdjAngle2);
		m_AdjustDsc2ndList.SetItemText(i, 16, strData);


		DscAdjustInfo* pAdjustInfo = m_pAdjustMgrAfter.GetDscAt(i);
		pAdjustInfo->bAdjust = FALSE;
		pAdjustInfo->bRotate = FALSE;
		pAdjustInfo->bMove = TRUE;
		pAdjustInfo->bImgCut = TRUE;;


		AdjustData->AdjptRotate2.x = _ttoi(m_AdjustDsc2ndList.GetItemText(i, 8));
		AdjustData->AdjptRotate2.y = _ttoi(m_AdjustDsc2ndList.GetItemText(i, 9));
		AdjustData->AdjSize2 = 1;
		*/
	}
}
void ESMAdjustMgrDlg::OnBnClickedBtn2ndSavePoint()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	CString szFilter = _T("Point List (*.ptl)|*.ptl|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  	
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Save");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();	
		if( strFileName.Right(4) != _T(".ptl"))
			strFileName = strFileName + _T(".ptl");
		SavePointData(strFileName);


		//Save Image Data
		CString strAdjustFolder;
		strAdjustFolder.Format(_T("%s\\img"),ESMGetPath(ESM_PATH_SETUP));
		CreateDirectory(strAdjustFolder, NULL);

		for(int i =0; i< m_pAdjustMgrAfter.GetDscCount(); i++)
		{
			CString strFile;
			DscAdjustInfo* pAdjustInfoOrigin = NULL;
			pAdjustInfoOrigin = m_pAdjustMgrAfter.GetDscAt(i);

			m_StAdjustImageAfter.SetImageBuffer(pAdjustInfoOrigin->pBmpBits, pAdjustInfoOrigin->nWidht, pAdjustInfoOrigin->nHeight, 3);
			m_StAdjustImageAfter.m_pImage;

			strFile.Format(_T("%s\\%d.jpg"), strAdjustFolder,i);

			char path[MAX_PATH];
			wcstombs(path, strFile, MAX_PATH);
			//cvSaveImage(path, m_StAdjustImageAfter.m_pImage);
		}
	}
}
void ESMAdjustMgrDlg::OnBnClickedBtnCopyPoint()
{
	CString strData;
	for(int i = 0; i < m_AdjustDsc2ndList.GetItemCount(); i++)
	{
		for(int j = 2; j < 12; j++)
		{
			strData = m_AdjustDsc2ndList.GetItemText(i, j);
			m_AdjustDsc2ndList.SetItemText(i,j+10,strData);
		}
	}
}
void ESMAdjustMgrDlg::OnBnClickedBtn2ndSavePointAdj()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	CString szFilter = _T("Point List (*.ptl)|*.ptl|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  	
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Save");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();	
		if( strFileName.Right(4) != _T(".ptl"))
			strFileName = strFileName + _T(".ptl");
		SavePointData(strFileName);


		//Save Image Data
		CString strAdjustFolder;
		strAdjustFolder.Format(_T("%s\\img"),ESMGetPath(ESM_PATH_SETUP));
		CreateDirectory(strAdjustFolder, NULL);
	}
}
void ESMAdjustMgrDlg::OnBnClickedBtnLeft()
{
	m_adjinfo.at(m_nSelectedNum).AdjMove.x -= 1;


	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
	if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
	{
		delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
		m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
	}

	Showimage();
	Invalidate();
}
void ESMAdjustMgrDlg::OnBnClickedBtnRight()
{
	//Image Right Move
	m_adjinfo.at(m_nSelectedNum).AdjMove.x += 1;


	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
	if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
	{
		delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
		m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
	}

	Showimage();
	Invalidate();
}
void ESMAdjustMgrDlg::OnBnClickedBtnDown()
{
	//Image Down
	m_adjinfo.at(m_nSelectedNum).AdjMove.y += 1;


	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
	if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
	{
		delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
		m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
	}

	Showimage();
	Invalidate();
}
void ESMAdjustMgrDlg::OnBnClickedBtnUp()
{
	//Image Up
	m_adjinfo.at(m_nSelectedNum).AdjMove.y -= 1;


	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
	if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
	{
		delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
		m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
	}

	Showimage();
	Invalidate();
}
void ESMAdjustMgrDlg::OnBnClickedBtnRotateLeft()
{
	//Image Rotate left
	//160905 hjcho 0.1도씩 움직이게 재설정 
	m_adjinfo.at(m_nSelectedNum).AdjAngle -= 0.1;

	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
	if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
	{
		delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
		m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
	}

	Showimage();
	Invalidate();

}
void ESMAdjustMgrDlg::OnBnClickedBtnRotateRight()
{
	//160905 hjcho 0.1도씩 움직이게 재설정 
	m_adjinfo.at(m_nSelectedNum).AdjAngle += 0.1;

	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
	m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
	if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
	{
		delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
		m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
	}

	Showimage();
	Invalidate();
}
void ESMAdjustMgrDlg::OnBnClickedBtnLoadImg()
{
//	DscAdjustInfo* Load_Memory;
//	CString strAdjustFolder;
//	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_IMAGE));
//
//	for(int i=0;i < m_pAdjustMgrRe.m_ArrDscInfo.size(); i++)
//	{
//		DscAdjustInfo* Load_Data = m_pAdjustMgrRe.GetDscAt(i);
//		m_pAdjustMgrMemory.AddDscInfo(Load_Data->strDscName, Load_Data->strDscIp);
//		Load_Memory = m_pAdjustMgrMemory.GetDscAt(i);
//
//		CString strDSC;
//		strDSC.Format(_T("%s.jpg"), Load_Data->strDscName);
//		CString strPath;
//		strPath.Format(_T("%s%s"), strAdjustFolder, strDSC);
//		char pchPath[MAX_PATH] = {0};
//		int nRequiredSize = (int)wcstombs(pchPath, strPath, MAX_PATH);
//		IplImage* Load_Image = cvLoadImage(pchPath,1);
//		if(Load_Image != NULL)
//		{
//			if(Load_Image->imageData != NULL)
//			{
//				if(Load_Memory->strDscName == Load_Data->strDscName)
//				{
//					Load_Memory->pBmpBits = new BYTE [Load_Image->imageSize];
//					memcpy(Load_Memory->pBmpBits,(BYTE*)Load_Image->imageData,Load_Image->imageSize);
//				}
//			}
//		}
//		cvReleaseImage(&Load_Image);
//	}
//	m_ImageLoaded = 1;
//	MessageBox(_T("ReCalibration\nImage Load Complete!"),_T("ReCalibration"),NULL);
//
}

void ESMAdjustMgrDlg::OnBnClickedBtnReSaveAdjust()
{
	m_MarginX = 0;
	m_MarginY = 0;

	//wgkim 190527
	CESMImgMgr* pImgMgr = new CESMImgMgr();

	for(int i = 0; i < m_pAdjustMgrRe.m_ArrDscInfo.size(); i++)
	{
		DscAdjustInfo* pinfo = m_pAdjustMgrRe.m_ArrDscInfo.at(i);
		pinfo->dAngle = m_adjinfo.at(i).AdjAngle;
		pinfo->dAdjustX = m_adjinfo.at(i).AdjMove.x;
		pinfo->dAdjustY = m_adjinfo.at(i).AdjMove.y;

		stAdjustInfo adjInfo = m_adjinfo.at(i);
		adjInfo.AdjAngle = m_adjinfo.at(i).AdjAngle;
		adjInfo.AdjMove.x = m_adjinfo.at(i).AdjMove.x;
		adjInfo.AdjMove.y = m_adjinfo.at(i).AdjMove.y;

		//wgkim 190520
		pImgMgr->SetMargin(m_MarginX,m_MarginY,adjInfo);
		//CESMImgMgr* pImgMgr = new CESMImgMgr();
		pImgMgr->SetRectMargin(adjInfo);
	}

	//wgkim 190520
	for(int i = 0; i < m_pAdjustMgrRe.m_ArrDscInfo.size(); i++)
	{
		stAdjustInfo adjInfo = m_adjinfo.at(i);
		adjInfo.rtMargin = pImgMgr->GetMinimumMargin();
	}

	delete pImgMgr;


	ESMEvent* pMsg = new ESMEvent();  
	pMsg->message = WM_ESM_MOVIE_SAVE_ADJUST;
	pMsg->pParam = (LPARAM)& m_adjinfo;
	pMsg->nParam1 = m_MarginX;
	pMsg->nParam2 = m_MarginY;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	MessageBox(_T("ReCalibration\nAdjust Save Complete!"),_T("ReCalibration"),NULL);

}
void ESMAdjustMgrDlg::OnBnClickedBtnSaveImg()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_IMAGE));

	//Image Save
	for(int i = 0; i < m_pAdjustMgrRe.m_ArrDscInfo.size(); i++)
	{
		CString strDSC;
		DscAdjustInfo* pData = m_pAdjustMgrRe.GetDscAt(i);
		IplImage* pImage;
		pImage = new IplImage;	
		pImage = cvCreateImage(cvSize(pData->nWidht, pData->nHeight), IPL_DEPTH_8U, 3);

		memcpy(pImage->imageData, pData->pBmpBits, pData->nWidht * pData->nHeight*3);
		strDSC.Format(_T("%s.jpg"), pData->strDscName);
		CString strPath;
		strPath.Format(_T("%s\\%s"), strAdjustFolder, strDSC);
		char pchPath[MAX_PATH] = {0};
		int nRequiredSize = (int)wcstombs(pchPath, strPath, MAX_PATH); //유니코드
		//cvSaveImage(pchPath, pImage);
	}
	MessageBox(_T("ReCalibration\nImage Save Complete!"),_T("ReCalibration"),NULL);
}


void ESMAdjustMgrDlg::OnBnClickedBtn2ndLoadPoint()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	// 	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
	// 		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	// 	else
	// 		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_CONF));

	CString szFilter = _T("Point List (*.ptl)|*.ptl|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Load");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();

		LoadPointData(strFileName);


		m_bChangeFlag = TRUE;
	}
}
void ESMAdjustMgrDlg::OnBnClickedBtnView1()
{
	LoadView(FIRST_ADJUST_VIEW);

}
void ESMAdjustMgrDlg::OnBnClickedBtnView2()
{
	LoadView(SECOND_ADJUST_VIEW);

}
void ESMAdjustMgrDlg::OnBnClickedBtnView3()
{
	int cnt = 0;
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();

	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		CString strDSC = m_AdjustDscList.GetItemText(i,1);
		m_adjinfo.push_back(pMovieMgr->GetAdjustData(strDSC));
	}

	for(int i = 0; i < m_adjinfo.size(); i++)
	{
		stAdjustInfo adjInfo = m_adjinfo.at(i);
		if(adjInfo.AdjAngle== 0.0) cnt++;
	}
	if(cnt ==  m_adjinfo.size())
	{
		MessageBox(_T("Adjust를 생성하세요!"),_T("ReCalibration"),NULL);
		return;
	}
	else
	{
		LoadView(THIRD_ADJUST_VIEW);
	}
}
void ESMAdjustMgrDlg::LoadView(int nView)
{
	switch(nView)
	{
	case FIRST_ADJUST_VIEW:
		m_nImgView = ADJUST_IMAGE;
		m_nView = FIRST_ADJUST_VIEW;
		m_StAdjustImage.ShowWindow(SW_SHOW);
		m_AdjustDscList.ShowWindow(SW_SHOW);

		m_btnLeft.ShowWindow(SW_HIDE);
		m_btnRight.ShowWindow(SW_HIDE);
		m_btnUp.ShowWindow(SW_HIDE);
		m_btnDown.ShowWindow(SW_HIDE);
		m_btnRotateLeft.ShowWindow(SW_HIDE);
		m_btnRotateRight.ShowWindow(SW_HIDE);
		m_btnSaveImg.ShowWindow(SW_HIDE);
		m_btnLoadImg.ShowWindow(SW_HIDE);
		m_btnReSaveAdjust.ShowWindow(SW_HIDE);

		m_StAdjustImageRe.ShowWindow(SW_HIDE);
		m_StAdjustImageOrigin.ShowWindow(SW_HIDE);
		m_StAdjustImageAfter.ShowWindow(SW_HIDE);
		m_ctrl2ndOption.ShowWindow(SW_HIDE);
		m_AdjustDsc2ndList.ShowWindow(SW_HIDE);
		m_stCenterLine.ShowWindow(SW_HIDE);
		m_ctrlCenterLine.ShowWindow(SW_HIDE);
		m_btnSavePoint.ShowWindow(SW_HIDE);
		m_btnLoadPoint.ShowWindow(SW_HIDE);
		m_btnCalculate.ShowWindow(SW_HIDE);
		m_btnMoveCal.ShowWindow(SW_HIDE);
		m_btnSaveAdjust.ShowWindow(SW_HIDE);
		m_ctrlCheckPoint.ShowWindow(SW_HIDE);
		m_btnSavePointAdj.ShowWindow(SW_HIDE);
		m_btnCopyPoint.ShowWindow(SW_HIDE);
		m_recal_sub.ShowWindow(SW_HIDE);
		m_recal_puz.ShowWindow(SW_HIDE);
		m_btnSaveAdjust_View1.ShowWindow(SW_SHOW);
		break;
	case SECOND_ADJUST_VIEW:
		{
			m_nImgView = ORIGINAL_IMAGE;
			m_nView = SECOND_ADJUST_VIEW;
			m_adjinfo.clear();
			m_StAdjustImage.ShowWindow(SW_HIDE);
			m_AdjustDscList.ShowWindow(SW_HIDE);
			m_StAdjustImageRe.ShowWindow(SW_HIDE);

			m_btnLeft.ShowWindow(SW_HIDE);
			m_btnRight.ShowWindow(SW_HIDE);
			m_btnUp.ShowWindow(SW_HIDE);
			m_btnDown.ShowWindow(SW_HIDE);
			m_btnRotateLeft.ShowWindow(SW_HIDE);
			m_btnRotateRight.ShowWindow(SW_HIDE);
			m_btnSaveImg.ShowWindow(SW_HIDE);
			m_btnLoadImg.ShowWindow(SW_HIDE);
			m_btnReSaveAdjust.ShowWindow(SW_HIDE);

			m_StAdjustImageOrigin.ShowWindow(SW_SHOW);
			m_StAdjustImageAfter.ShowWindow(SW_SHOW);
			m_ctrl2ndOption.ShowWindow(SW_SHOW);
			m_AdjustDsc2ndList.ShowWindow(SW_SHOW);
			m_stCenterLine.ShowWindow(SW_SHOW);
			m_ctrlCenterLine.ShowWindow(SW_SHOW);
			m_btnSavePoint.ShowWindow(SW_SHOW);
			m_btnLoadPoint.ShowWindow(SW_SHOW);
			m_btnCalculate.ShowWindow(SW_SHOW);
			m_btnMoveCal.ShowWindow(SW_SHOW);
			m_btnSaveAdjust.ShowWindow(SW_SHOW);
			m_ctrlCheckPoint.ShowWindow(SW_SHOW);
			m_btnSavePointAdj.ShowWindow(SW_SHOW);
			m_btnCopyPoint.ShowWindow(SW_SHOW);
			m_recal_sub.ShowWindow(SW_HIDE);
			m_recal_puz.ShowWindow(SW_HIDE);
			m_btnSaveAdjust_View1.ShowWindow(SW_HIDE);

			CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();


			for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
			{
				CString strDSC = m_AdjustDscList.GetItemText(i,1);
				m_adjinfo.push_back(pMovieMgr->GetAdjustData(strDSC));
			}

			for(int i = 0; i < m_adjinfo.size(); i++)
			{
				stAdjustInfo adjInfo = m_adjinfo.at(i);
				CESMImgMgr* pImgMgr = new CESMImgMgr();
				pImgMgr->SetMargin(m_MarginX, m_MarginY, adjInfo);
			}
		}
		break;
	case THIRD_ADJUST_VIEW:
		{
			m_nImgView = RE_IMAGE;
			m_nView = THIRD_ADJUST_VIEW;
			m_AdjustDscList.ShowWindow(SW_SHOW);
			m_StAdjustImageRe.ShowWindow(SW_SHOW);

			m_btnLeft.ShowWindow(SW_SHOW);
			m_btnRight.ShowWindow(SW_SHOW);
			m_btnUp.ShowWindow(SW_SHOW);
			m_btnDown.ShowWindow(SW_SHOW);
			m_btnRotateLeft.ShowWindow(SW_SHOW);
			m_btnRotateRight.ShowWindow(SW_SHOW);
			m_btnSaveImg.ShowWindow(SW_SHOW);
			m_btnLoadImg.ShowWindow(SW_SHOW);
			m_btnReSaveAdjust.ShowWindow(SW_SHOW);

			m_StAdjustImage.ShowWindow(SW_HIDE);
			m_StAdjustImageOrigin.ShowWindow(SW_HIDE);
			m_StAdjustImageAfter.ShowWindow(SW_HIDE);
			m_ctrl2ndOption.ShowWindow(SW_HIDE);
			m_AdjustDsc2ndList.ShowWindow(SW_HIDE);
			m_stCenterLine.ShowWindow(SW_HIDE);
			m_ctrlCenterLine.ShowWindow(SW_HIDE);
			m_btnSavePoint.ShowWindow(SW_HIDE);
			m_btnLoadPoint.ShowWindow(SW_HIDE);
			m_btnCalculate.ShowWindow(SW_HIDE);
			m_btnMoveCal.ShowWindow(SW_HIDE);
			m_btnSaveAdjust.ShowWindow(SW_HIDE);
			m_ctrlCheckPoint.ShowWindow(SW_HIDE);
			m_btnSavePointAdj.ShowWindow(SW_HIDE);
			m_btnCopyPoint.ShowWindow(SW_HIDE);
			m_recal_sub.ShowWindow(SW_SHOW);
			m_recal_puz.ShowWindow(SW_SHOW);
			m_btnSaveAdjust_View1.ShowWindow(SW_HIDE);

			CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();


			for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
			{
				CString strDSC = m_AdjustDscList.GetItemText(i,1);
				m_adjinfo.push_back(pMovieMgr->GetAdjustData(strDSC));
			}

			for(int i = 0; i < m_adjinfo.size(); i++)
			{
				stAdjustInfo adjInfo = m_adjinfo.at(i);
				CESMImgMgr* pImgMgr = new CESMImgMgr();
				pImgMgr->SetMargin(m_MarginX, m_MarginY, adjInfo);
			}
		}

		break;
	}


}
void ESMAdjustMgrDlg::OnBnClickedOk()
{
	CString strTemp;
	GetDlgItem(IDC_EDT_ADJUST_CAMERAZOOM)->GetWindowText(strTemp);
	int nTargetZoom = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_CAMERAZOOM, nTargetZoom);

	GetDlgItem(IDC_EDT_ADJUST_TARGETLENTH)->GetWindowText(strTemp);
	int nTargetLenth = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_TARGETLENTH, nTargetLenth);

	GetDlgItem(IDC_EDT_ADJUSTTHRESHOLD)->GetWindowText(strTemp);
	int nThresdhold = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_TH, nThresdhold);

	GetDlgItem(IDC_EDT_ADJUST_POINTGAPMIN)->GetWindowText(strTemp);
	int nPointGapMin = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_POINTGAPMIN, nPointGapMin);

	GetDlgItem(IDC_EDT_ADJUST_MIN_WIDTH)->GetWindowText(strTemp);
	int nMinWidth = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_MIN_WIDTH, nMinWidth);

	GetDlgItem(IDC_EDT_ADJUST_MIN_HEIGHT)->GetWindowText(strTemp);
	int nMinHeight = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_MIN_HEIGHT, nMinHeight);

	GetDlgItem(IDC_EDT_ADJUST_MAX_WIDTH)->GetWindowText(strTemp);
	int nMaxWidth = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_MAX_WIDTH, nMaxWidth);

	GetDlgItem(IDC_EDT_ADJUST_MAX_HEIGHT)->GetWindowText(strTemp);
	int nMaxHeight = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_MAX_HEIGHT, nMaxHeight);

	CESMIni ini;
	CString strInfoConfig;
	strInfoConfig.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONFIG);

	if(!ini.SetIniFilename (strInfoConfig))
	{
		CString strLog;
		strLog.Format(_T("File Not Exist %s"), strInfoConfig);
		AfxMessageBox(strLog );
		return;
	}
	ini.WriteInt	(INFO_SECTION_MOVIE,	INFO_ADJ_TH,					nThresdhold);
	ini.WriteInt	(INFO_SECTION_MOVIE,	INFO_ADJ_POINTGAPMIN,			nPointGapMin);
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MIN_WIDTH,				nMinWidth);	
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MIN_HEIGHT,			nMinHeight);	
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MAX_WIDTH,				nMaxWidth);	
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MAX_HEIGHT,			nMaxHeight);

	CDialog::OnOK();
}
BOOL ESMAdjustMgrDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	//m_AdjustDscList.ModifyStyle(0, LVS_SHOWSELALWAYS);
	m_AdjustDscList.InsertColumn(0, _T("ID"), 0, 40);
	m_AdjustDscList.InsertColumn(1, _T("DscId"), 0, 60);
	m_AdjustDscList.InsertColumn(2, _T("Focus"), 0, 45);
	m_AdjustDscList.InsertColumn(3, _T("Pos1 X"), 0, 45);
	m_AdjustDscList.InsertColumn(4, _T("Pos1 Y"), 0, 45); 
	m_AdjustDscList.InsertColumn(5, _T("Pos2 X"), 0, 45);
	m_AdjustDscList.InsertColumn(6, _T("Pos2 Y"), 0, 45);
	m_AdjustDscList.InsertColumn(7, _T("Pos3 X"), 0, 45);
	m_AdjustDscList.InsertColumn(8, _T("Pos3 Y"), 0, 45);


	m_AdjustDscList.InsertColumn(9, _T("Scale"), 0, 65);
	m_AdjustDscList.InsertColumn(10, _T("Select"), 0, 45);

	m_AdjustDscList.InsertColumn(11, _T("AdjustX"), 0, 65);
	m_AdjustDscList.InsertColumn(12, _T("AdjustY"), 0, 65);
	m_AdjustDscList.InsertColumn(13, _T("Angle"), 0, 65);
	m_AdjustDscList.InsertColumn(14, _T("RotateX"), 0, 65);
	m_AdjustDscList.InsertColumn(15, _T("RotateY"), 0, 65);
	//m_AdjustDscList.InsertColumn(13, _T("Scale"), 0, 65);
	m_AdjustDscList.InsertColumn(16, _T("Distance"), 0, 65);
	//m_AdjustDscList.InsertColumn(15, _T("Select"), 0, 45);

	m_AdjustDscList.SetExtendedStyle(/*LVS_EX_GRIDLINES|*/LVS_EX_FULLROWSELECT);


	m_AdjustDsc2ndList.InsertColumn(0, _T("ID"), 0, 30);
	m_AdjustDsc2ndList.InsertColumn(1, _T("DscId"), 0, 44);
	m_AdjustDsc2ndList.InsertColumn(2, _T("Org X"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(3, _T("Org Y"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(4, _T("Org1 X"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(5, _T("Org1 Y"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(6, _T("Org2 X"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(7, _T("Org2 Y"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(8, _T("Org3 X"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(9, _T("Org3 Y"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(10, _T("Org4 X"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(11, _T("Org4 Y"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(12, _T("Adj X"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(13, _T("Adj Y"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(14, _T("Adj1 X"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(15, _T("Adj1 Y"), 0, 50); 
	m_AdjustDsc2ndList.InsertColumn(16, _T("Adj2 X"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(17, _T("Adj2 Y"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(18, _T("Adj3 X"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(19, _T("Adj3 Y"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(20, _T("Adj4 X"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(21, _T("Adj4 Y"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(22, _T("Angle1"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(23, _T("Angle2"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(24, _T("Angle3"), 0, 50);
	m_AdjustDsc2ndList.InsertColumn(25, _T("pos"), 0, 50);
	m_AdjustDsc2ndList.SetExtendedStyle(LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT);


	m_ctrlCheckPoint.AddString(_T("Home Base"));
	m_ctrlCheckPoint.AddString(_T("1st Base"));
	m_ctrlCheckPoint.AddString(_T("2nd Base"));
	m_ctrlCheckPoint.AddString(_T("3rd Base"));
	m_ctrlCheckPoint.AddString(_T("Pitcher"));
	m_ctrlCheckPoint.SetCurSel(0);

	//m_edit_frameIndex.SetWindowText(_T("0"));
	m_frameIndex = 0;
	LoadData();
	Load2ndData();

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), ADJUSTTEMPFILE);
	LoadPointData(strFileName);
	LoadView(FIRST_ADJUST_VIEW);	

	//jhhan
	GetWindowRect(m_rcRect);

	//ShowWindow(SW_MAXIMIZE);

	m_btnCalcAdjust.ChangeFont(18); 
	m_btnCalcAdjust.ChangeFont(18);
	m_btnCalcDistance.ChangeFont(18);
	m_btnPointSave.ChangeFont(18);
	m_btnPointLoad.ChangeFont(18);
	m_btnPointReset.ChangeFont(18);
	m_btnDistanceReg.ChangeFont(18);
	m_btnAllReg.ChangeFont(18);
	m_btnDistanceDelete.ChangeFont(18);
	m_btnDistanceClear.ChangeFont(18);
	m_btnFocusSave.ChangeFont(18);
	m_btnFocusDelete.ChangeFont(18);
	m_btnFocusCalc.ChangeFont(18);
	m_btnFrameCommit.ChangeFont(18);
	m_btnOK.ChangeFont(18);
	m_btnCancel.ChangeFont(18);

	::SetWindowPos(this->m_hWnd,NULL,0,0,1920,1080,SWP_NOMOVE);
	//MoveWindow(0,0,1920,1080,TRUE);

	//hide 
	GetDlgItem(IDC_STATIC_FOCUSGROUPBOX)->ShowWindow(FALSE);
	GetDlgItem(IDC_BTN_CALCDISTANCE)->ShowWindow(FALSE);
	GetDlgItem(IDC_BTN_FOCUS_SAVE)->ShowWindow(FALSE);
	GetDlgItem(IDC_BTN_FOCUS_DELETE)->ShowWindow(FALSE);
	GetDlgItem(IDC_BTN_FOCUS_CALCULATION)->ShowWindow(FALSE);

	UpdateData(FALSE);
	return TRUE;
}
void ESMAdjustMgrDlg::LoadData()
{
	m_AdjustDscList.DeleteAllItems();

	for(int i =0; i< m_pAdjustMgr.GetDscCount(); i++)
	{
		//m_pAdjustMgr
		CString strTp;
		strTp.Format(_T("%d"), i + 1);
		m_AdjustDscList.InsertItem(i, strTp);
		strTp = m_pAdjustMgr.GetDscAt(i)->strDscName;
		m_AdjustDscList.SetItemText(i, 1, strTp);
	}

	m_RdImageViewColor = 0;
	CString strTemp;

	strTemp.Format(_T("%d"), ESMGetValue(ESM_VALUE_ADJ_CAMERAZOOM));
	GetDlgItem(IDC_EDT_ADJUST_CAMERAZOOM)->SetWindowText(strTemp);

	strTemp.Format(_T("%d"), ESMGetValue(ESM_VALUE_ADJ_TARGETLENTH));
	GetDlgItem(IDC_EDT_ADJUST_TARGETLENTH)->SetWindowText(strTemp);

	strTemp.Format(_T("%d"), ESMGetValue(ESM_VALUE_ADJ_TH));
	GetDlgItem(IDC_EDT_ADJUSTTHRESHOLD)->SetWindowText(strTemp);

	strTemp.Format(_T("%d"), ESMGetValue(ESM_VALUE_ADJ_POINTGAPMIN));
	GetDlgItem(IDC_EDT_ADJUST_POINTGAPMIN)->SetWindowText(strTemp);

	strTemp.Format(_T("%d"), ESMGetValue(ESM_VALUE_ADJ_MIN_WIDTH));
	GetDlgItem(IDC_EDT_ADJUST_MIN_WIDTH)->SetWindowText(strTemp);
	strTemp.Format(_T("%d"), ESMGetValue(ESM_VALUE_ADJ_MIN_HEIGHT));
	GetDlgItem(IDC_EDT_ADJUST_MIN_HEIGHT)->SetWindowText(strTemp);

	strTemp.Format(_T("%d"), ESMGetValue(ESM_VALUE_ADJ_MAX_WIDTH));
	GetDlgItem(IDC_EDT_ADJUST_MAX_WIDTH)->SetWindowText(strTemp);
	strTemp.Format(_T("%d"), ESMGetValue(ESM_VALUE_ADJ_MAX_HEIGHT));
	GetDlgItem(IDC_EDT_ADJUST_MAX_HEIGHT)->SetWindowText(strTemp);

	m_nRdDetectColor = 0;
	m_clrDetectColor = COLOR_BLACK;
	//m_pAdjustMgr.GetResolution(m_nAdjViewWidth,m_nAdjViewHeight);
	//int nLine1 = m_nAdjViewHeight * 0.33;
	//int nLine2 = m_nAdjViewHeight * 0.66;
	//int nLine3 = m_nAdjViewHeight * 0.5;

	if( m_nMovieState == ESM_ADJ_FILM_STATE_MOVIE)
	{
		if( m_hTheadHandle ) 
		{
			CloseHandle(m_hTheadHandle);
			m_hTheadHandle = NULL;
		}

		while(!m_bThreadStop)
		{
			Sleep(10);
		}

		m_bThreadLoad = TRUE;
		m_bThreadStop = FALSE;
		m_hTheadHandle = (HANDLE) _beginthreadex(NULL, 0, GetMovieDataThread, (void *)this, 0, NULL);
	}
	else
	{
		DscAdjustInfo* pAdjustInfo = NULL;
		for(int i =0; i< m_pAdjustMgr.GetDscCount(); i++)
		{
			pAdjustInfo = m_pAdjustMgr.GetDscAt(i);
			if( pAdjustInfo != NULL)
			{
				if( m_hTheadHandle ) 
				{
					CloseHandle(m_hTheadHandle);
					m_hTheadHandle = NULL;
				}

				m_hTheadHandle = (HANDLE) _beginthreadex(NULL, 0, GetImageDataThread, (void *)pAdjustInfo, 0, NULL);
			}
		}
	}

	m_ctrlBoxSize.SetWindowText(_T("5"));//->SetWindowText(_T("5"));


	/*CString	 strLine1, strLine2, strLine3;
	strLine1.Format(_T("%d"), nLine1);
	strLine2.Format(_T("%d"), nLine2);
	strLine3.Format(_T("%d"), nLine3);
	GetDlgItem(IDC_EDT_LINE1)->SetWindowText(strLine1);
	GetDlgItem(IDC_EDT_LINE2)->SetWindowText(strLine2);
	GetDlgItem(IDC_EDT_CENTER_LINE)->SetWindowText(strLine3);*/
}
void ESMAdjustMgrDlg::Load2ndData()
{

	m_AdjustDsc2ndList.DeleteAllItems();
	for(int i =0; i< m_pAdjustMgrOrigin.GetDscCount(); i++)
	{
		//m_pAdjustMgr
		CString strTp;
		strTp.Format(_T("%d"), i + 1);
		m_AdjustDsc2ndList.InsertItem(i, strTp);
		strTp = m_pAdjustMgrOrigin.GetDscAt(i)->strDscName;
		m_AdjustDsc2ndList.SetItemText(i, 1, strTp);
	}

	if( m_nMovieState == ESM_ADJ_FILM_STATE_MOVIE)
	{

	}
	else
	{
		//Original
		DscAdjustInfo* pAdjustInfoOrigin = NULL;
		for(int i =0; i< m_pAdjustMgrOrigin.GetDscCount(); i++)
		{
			pAdjustInfoOrigin = m_pAdjustMgrOrigin.GetDscAt(i);
			if( pAdjustInfoOrigin != NULL)
			{
				if( m_hTheadHandle ) 
				{
					CloseHandle(m_hTheadHandle);
					m_hTheadHandle = NULL;
				}

				m_hTheadHandle = (HANDLE) _beginthreadex(NULL, 0, GetImageDataThread, (void *)pAdjustInfoOrigin, 0, NULL);
			}
		}

		//After
		DscAdjustInfo* pAdjustInfoAfter = NULL;
		for(int i =0; i< m_pAdjustMgrAfter.GetDscCount(); i++)
		{
			pAdjustInfoAfter = m_pAdjustMgrAfter.GetDscAt(i);
			if( pAdjustInfoAfter != NULL)
			{
				if( m_hTheadHandle ) 
				{
					CloseHandle(m_hTheadHandle);
					m_hTheadHandle = NULL;
				}

				m_hTheadHandle = (HANDLE) _beginthreadex(NULL, 0, GetImageDataThread, (void *)pAdjustInfoAfter, 0, NULL);
			}
		}
	}
}
void ESMAdjustMgrDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	ESMAdjustImage* pAdjustImg[MAX_IMAGE_VIEW];
	pAdjustImg[ADJUST_IMAGE] = &m_StAdjustImage;
	pAdjustImg[ORIGINAL_IMAGE] = &m_StAdjustImageOrigin;
	pAdjustImg[AFTER_IMAGE] = &m_StAdjustImageAfter;
	pAdjustImg[RE_IMAGE] = &m_StAdjustImageRe;

	//for(int i = nCount; i < MAX_IMAGE_VIEW; i++)
	{
		CString strPos;
		CRect rect;
		pAdjustImg[m_nImgView]->GetWindowRect(&rect);
		ScreenToClient(&rect);
		int nPos = 0;
		if( point.x - rect.left < 0 || point.x > rect.right || point.y - rect.top < 0 || point.y > rect.bottom)
		{

			CDialogEx::OnMouseMove(nFlags, point);
			return;
		}

		//-- 2014-08-31 hongsu@esmlab.com
		//-- Get Image Position from Mouse Point
		CPoint ptImage;
		ptImage = GetPointFromMouse(point);

		strPos.Format(_T("%d"), ptImage.x);
		m_nPosX = ptImage.x - 1;
		GetDlgItem(IDC_ADJUST_MOUSEPOSX)->SetWindowText(strPos);

		strPos.Format(_T("%d"), ptImage.y);
		m_nPosY = ptImage.y - 1;
		GetDlgItem(IDC_ADJUST_MOUSEPOSY)->SetWindowText(strPos);


		pAdjustImg[m_nImgView]->OnMouseMove(nFlags, point);
	}


	CDialogEx::OnMouseMove(nFlags, point);
}
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-08-31
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
CPoint ESMAdjustMgrDlg::GetPointFromMouse(CPoint pt)
{
	ESMAdjustImage* pAdjustImg[MAX_IMAGE_VIEW];
	pAdjustImg[ADJUST_IMAGE] = &m_StAdjustImage;
	pAdjustImg[ORIGINAL_IMAGE] = &m_StAdjustImageOrigin;
	pAdjustImg[AFTER_IMAGE] = &m_StAdjustImageAfter;
	pAdjustImg[RE_IMAGE] = &m_StAdjustImageRe;

	CRect rect;
	pAdjustImg[m_nImgView]->GetWindowRect(&rect);
	ScreenToClient(&rect);

	CPoint ptRet;
	pt.x = pt.x - rect.left;
	pt.y = pt.y - rect.top;
	CPoint ptImage = pAdjustImg[m_nImgView]->GetImagePos();
	ptRet.x = (int)(ptImage.x * pAdjustImg[m_nImgView]->GetMultiple() + pt.x * pAdjustImg[m_nImgView]->GetMultiple());
	ptRet.y = (int)(ptImage.y * pAdjustImg[m_nImgView]->GetMultiple() + pt.y * pAdjustImg[m_nImgView]->GetMultiple());

	//ESMLog(5, _T("Moust Point x:%d y:%d"), ptRet.x, ptRet.y);
	return ptRet;
}
void ESMAdjustMgrDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	ESMAdjustImage* pAdjustImg[MAX_IMAGE_VIEW];
	pAdjustImg[ADJUST_IMAGE] = &m_StAdjustImage;
	pAdjustImg[ORIGINAL_IMAGE] = &m_StAdjustImageOrigin;
	pAdjustImg[AFTER_IMAGE] = &m_StAdjustImageAfter;
	pAdjustImg[RE_IMAGE] = &m_StAdjustImageRe;

	int nCount;
	switch(m_nView)
	{
	case FIRST_ADJUST_VIEW:
		nCount = ADJUST_IMAGE;
		m_nImgView = ADJUST_IMAGE;
		pAdjustImg[m_nImgView]->OnLButtonDown(nFlags, point);
		pAdjustImg[m_nImgView]->SetFocus();
		break;
	case SECOND_ADJUST_VIEW:
		{
			RECT rClient1, rClient2;
			pAdjustImg[ORIGINAL_IMAGE]->GetClientRect(&rClient1);
			pAdjustImg[AFTER_IMAGE]->GetClientRect(&rClient2);

			if(rClient1.top <= point.y && rClient1.bottom >= point.y)
				m_nImgView = ORIGINAL_IMAGE;
			else
				m_nImgView = AFTER_IMAGE;
		}

		pAdjustImg[ORIGINAL_IMAGE]->OnLButtonDown(nFlags, point);
		pAdjustImg[ORIGINAL_IMAGE]->SetFocus();

		pAdjustImg[AFTER_IMAGE]->OnLButtonDown(nFlags, point);
		pAdjustImg[AFTER_IMAGE]->SetFocus();
		break;
	case THIRD_ADJUST_VIEW:
		nCount = RE_IMAGE;
		m_nImgView = RE_IMAGE;
		pAdjustImg[m_nImgView]->OnLButtonDown(nFlags, point);
		pAdjustImg[m_nImgView]->SetFocus();
		break;
	}




	CDialogEx::OnLButtonDown(nFlags, point);
}
void ESMAdjustMgrDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ESMAdjustImage* pAdjustImg[MAX_IMAGE_VIEW];
	pAdjustImg[ADJUST_IMAGE] = &m_StAdjustImage;
	pAdjustImg[ORIGINAL_IMAGE] = &m_StAdjustImageOrigin;
	pAdjustImg[AFTER_IMAGE] = &m_StAdjustImageAfter;
	pAdjustImg[RE_IMAGE] = &m_StAdjustImageRe;

	switch(m_nView)
	{
	case FIRST_ADJUST_VIEW:
		pAdjustImg[ADJUST_IMAGE]->OnLButtonUp(nFlags, point);
		break;
	case SECOND_ADJUST_VIEW:
		pAdjustImg[ORIGINAL_IMAGE]->OnLButtonUp(nFlags, point);
		pAdjustImg[AFTER_IMAGE]->OnLButtonUp(nFlags, point);
		break;
	case THIRD_ADJUST_VIEW:
		pAdjustImg[RE_IMAGE]->OnLButtonUp(nFlags, point);
		break;
	}


	CDialogEx::OnLButtonUp(nFlags, point);
}
BOOL ESMAdjustMgrDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	ESMAdjustImage* pAdjustImg[MAX_IMAGE_VIEW];
	int nCount;
	switch(m_nView)
	{
	case FIRST_ADJUST_VIEW:
		nCount = ADJUST_IMAGE;
		break;
	case SECOND_ADJUST_VIEW:
		nCount = ORIGINAL_IMAGE;
		break;
	case THIRD_ADJUST_VIEW:
		nCount = RE_IMAGE;
		break;
	}

	pAdjustImg[ADJUST_IMAGE] = &m_StAdjustImage;
	pAdjustImg[ORIGINAL_IMAGE] = &m_StAdjustImageOrigin;
	pAdjustImg[AFTER_IMAGE] = &m_StAdjustImageAfter;
	pAdjustImg[RE_IMAGE] = &m_StAdjustImageRe;

	for(int i = nCount; i < MAX_IMAGE_VIEW; i++)
	{
		CRect rtWindow;
		GetWindowRect(rtWindow);

		pt.x -= rtWindow.left;
		pt.y -= rtWindow.top;


		CPoint ptImage;
		ptImage = GetPointFromMouse(pt);

		if( zDelta > 0)
		{
			if(  pAdjustImg[i]->GetMultiple() <= 0.2)
				return TRUE;
			pAdjustImg[i]->ImageZoom(ptImage, TRUE);
		}
		else
			pAdjustImg[i]->ImageZoom(ptImage, FALSE);

		CString strTp;
		strTp.Format(_T("%.03lf"), pAdjustImg[i]->GetMultiple());
		GetDlgItem(IDC_ADJUST_IMAGESIZE)->SetWindowText(strTp);
	}

	return CDialogEx::OnMouseWheel(nFlags, zDelta, pt);
}
void ESMAdjustMgrDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// 	if( m_nSelectPos > 2)
	// 		m_nSelectPos = 0;
	// 
	// 	m_nSelectPos++;
	// 	CString strText;
	// 	int nSelectedItem = m_AdjustDscList.GetNextItem( -1, LVNI_SELECTED );
	// 	strText.Format(_T("%d"), m_nPosX);
	// 	m_AdjustDscList.SetItemText(nSelectedItem, m_nSelectPos * 2, strText);
	// 	strText.Format(_T("%d"), m_nPosY);
	// 	m_AdjustDscList.SetItemText(nSelectedItem, 1 + m_nSelectPos * 2, strText);
	CDialogEx::OnLButtonDblClk(nFlags, point);
}
void ESMAdjustMgrDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	int nStep;
	CListCtrl* pList;
	ESMAdjustImage* pAdjustImg;
	switch(m_nView)
	{
	case FIRST_ADJUST_VIEW:
		nStep = 2;
		pList = &m_AdjustDscList;
		pAdjustImg = &m_StAdjustImage;
		break;
	case SECOND_ADJUST_VIEW:
		nStep = 4;
		pList = &m_AdjustDsc2ndList;

		if(m_nImgView == ORIGINAL_IMAGE)
			pAdjustImg = &m_StAdjustImageOrigin;
		else
			pAdjustImg = &m_StAdjustImageAfter;

		break;
	case THIRD_ADJUST_VIEW:
		nStep = 2;
		pList = &m_AdjustDscList;
		pAdjustImg = &m_StAdjustImageRe;
		break;
	}

	DrawImage();

	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	CString strLine1, strLine2, strLine3;

	GetDlgItem(IDC_EDT_LINE1)->GetWindowText(strLine1);
	GetDlgItem(IDC_EDT_LINE2)->GetWindowText(strLine2);
	GetDlgItem(IDC_EDT_CENTER_LINE)->GetWindowText(strLine3);
	int nLine1 = _ttoi(strLine1);
	int nLine2 = _ttoi(strLine2);
	int nLine3 = _ttoi(strLine3);

	switch(m_nView)
	{
	case FIRST_ADJUST_VIEW:
		if(ESMGetValue(ESM_VALUE_HORIZON) == FALSE)
		{
			if ( m_nPosY < nLine1 || m_nPosY == nLine1 )
				m_nSelectPos = 0;
			else if ( m_nPosY < nLine2 || m_nPosY == nLine2 )
				m_nSelectPos = 1;
			else
				m_nSelectPos = 2;
			break;
		}
		else
		{
			if ( m_nPosX < nLine1 || m_nPosX == nLine1 )
				m_nSelectPos = 0;
			else if ( m_nPosX < nLine2 || m_nPosX == nLine2 )
				m_nSelectPos = 1;
			else
				m_nSelectPos = 2;
			break;
		}
	case SECOND_ADJUST_VIEW:
		{
			int nPoint = m_ctrlCheckPoint.GetCurSel();

			if(m_bChangeFlag)
			{
				switch(nPoint)
				{
				case HOME_BASE_POINT:	m_nSelectPos = 4;	break;
				case FIRST_BASE_POINT:	m_nSelectPos = 5;	break;
				case SECOND_BASE_POINT:	m_nSelectPos = 6;	break;
				case THIRD_BASE_POINT:	m_nSelectPos = 7;	break;
				case PITCHER_BASE_POINT: m_nSelectPos = 8;	break;
				default:
					m_nSelectPos = 4;	break;
				}
			}
			else
			{
				switch(nPoint)
				{
				case HOME_BASE_POINT:	m_nSelectPos = 0;	break;
				case FIRST_BASE_POINT:	m_nSelectPos = 1;	break;
				case SECOND_BASE_POINT:	m_nSelectPos = 2;	break;
				case THIRD_BASE_POINT:	m_nSelectPos = 3;	break;
				case PITCHER_BASE_POINT: m_nSelectPos = 4;	break;
				default:
					m_nSelectPos = 0;	break;
				}
			}

			/*if(m_bChangeFlag)
			{
			if ( m_nPosY < nLine1 || m_nPosY == nLine1 )
			m_nSelectPos = 3;
			else if ( m_nPosY < nLine2 || m_nPosY == nLine2 )
			m_nSelectPos = 4;
			else
			m_nSelectPos = 5;
			}
			else
			{

			if ( m_nPosY < nLine1 || m_nPosY == nLine1 )
			m_nSelectPos = 0;
			else if ( m_nPosY < nLine2 || m_nPosY == nLine2 )
			m_nSelectPos = 1;
			else
			m_nSelectPos = 2;
			}*/
		}


		break;
	case THIRD_ADJUST_VIEW:
		if ( m_nPosY < nLine1 || m_nPosY == nLine1 )
			m_nSelectPos = 0;
		else if ( m_nPosY < nLine2 || m_nPosY == nLine2 )
			m_nSelectPos = 1;
		else
			m_nSelectPos = 2;
		break;
	}



	CString strText;


	//-- 2014-08-30 hongsu@esmlab.com
	//-- Get Calculated Position
	CString strSize;
	//GetDlgItem(IDC_EDT_BOX_SIZE)->GetWindowText(strSize);
	m_ctrlBoxSize.GetWindowText(strSize);
	pAdjustImg->m_nCircleSize =  _ttoi(strSize) * 4;

	FindExactPosition(m_nPosX,m_nPosY);	
	TRACE("LINE: %d , %d , %d , %d \n",nLine1,nLine2,nLine3,m_nSelectPos);

	strText.Format(_T("%d"), m_nPosX);
	pList->SetItemText(nSelectedItem, m_nSelectPos * 2 + nStep+1, strText);
	strText.Format(_T("%d"), m_nPosY);
	pList->SetItemText(nSelectedItem, m_nSelectPos * 2 + nStep+2, strText);

	//-- Check Position
	BOOL bFinish = TRUE;
	for (int i = 0 ; i < 3 ; i++)
	{
		CString strPosX = pList->GetItemText(nSelectedItem, i*2 + 3 );
		CString strPosY = pList->GetItemText(nSelectedItem, i*2 + 4);

		int nPosX, nPosY;
		nPosX = _ttoi(strPosX);
		nPosY = _ttoi(strPosY);

		if ( m_nSelectPos != i )
			DrawCenterPoint(nPosX, nPosY);

		if ( strPosX == _T("") || strPosX == _T("0") || strPosY == _T("") || strPosY == _T("0") )
		{
			bFinish = FALSE;
			break;
		}
	}

	//-- 2016-11-01 wgkim@esmlab.com
	//-- Input Features for AutoAdjust
	if( m_bAutoFind == TRUE)
	{
		adjustAsAuto.InputAdjustFeaturesFromList(pList, nSelectedItem);
	}

	//-- 2014-08-30 hongsu@esmlab.com
	//-- Change Next Item
	if( bFinish == TRUE && m_bAutoMove == TRUE)
	{
		int nAllItem = m_AdjustDscList.GetItemCount();
		if(nAllItem > nSelectedItem+1)
		{ 
			SetTimer(TIMER_CHANGE_NEXT_ITEM, TIME_CHANGE_NEXT_ITEM, NULL);			
		}		
	}

	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), ADJUSTTEMPFILE);
	SavePointData(strFileName);

	CDialogEx::OnRButtonDown(nFlags, point);
}
void ESMAdjustMgrDlg::AddDscInfo(CString strDscId, CString strDscIp,BOOL bReverse)
{
	m_pAdjustMgrRe.AddDscInfo(strDscId, strDscIp,bReverse);
	m_pAdjustMgr.AddDscInfo(strDscId, strDscIp,bReverse);
	m_pAdjustMgrOrigin.AddDscInfo(strDscId, strDscIp,bReverse);
	m_pAdjustMgrAfter.AddDscInfo(strDscId, strDscIp,bReverse);
}
void ESMAdjustMgrDlg::DrawCenterPointAll()
{

	CString strTp;
	int nPosX, nPosY;

	for ( int i = 1 ; i < 4 ; i++ )
	{
		strTp = m_AdjustDscList.GetItemText(m_nSelectedNum, i*2 + 1);
		nPosX = _ttoi(strTp);
		strTp = m_AdjustDscList.GetItemText(m_nSelectedNum, i*2 + 2);
		nPosY = _ttoi(strTp);

		DrawCenterPoint(nPosX, nPosY);
	}

}
void ESMAdjustMgrDlg::DrawCenterPoint(int nPosX, int nPosY)
{
	int nCircle = m_StAdjustImage.m_nCircleSize;

	if ( nPosX != 0 && nPosY != 0 )
	{
		CRect rtOutLine(nPosX-nCircle,nPosY-nCircle,nPosX+nCircle,nPosY+nCircle);
		m_StAdjustImage.DrawRect(rtOutLine, RGB(0,255,0));

		CRect rtCenter(nPosX-1,nPosY-1,nPosX+1,nPosY+1);
		m_StAdjustImage.DrawRect(rtCenter, RGB(255,0,0));
	}
}
void ESMAdjustMgrDlg::OnLvnItemchangedAdjust(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	TRACE(_T("##########OnLvnItemchangedAdjust\n"));
	Showimage();
	Invalidate();
	*pResult = 0;
}
void ESMAdjustMgrDlg::OnLvnItemchangedAdjust2nd(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	Showimage();
	Invalidate();
	*pResult = 0;
}
void ESMAdjustMgrDlg::Showimage()
{
	CListCtrl* pList;
	ESMAdjustImage* pAdjustImg[MAX_IMAGE_VIEW];
	int nCount, nMaxCount;
	switch(m_nView)
	{
	case FIRST_ADJUST_VIEW:
	case THIRD_ADJUST_VIEW:
		pList = &m_AdjustDscList;
		break;
	case SECOND_ADJUST_VIEW:
		pList = &m_AdjustDsc2ndList;
		break;
	}

	pAdjustImg[ADJUST_IMAGE] = &m_StAdjustImage;
	pAdjustImg[ORIGINAL_IMAGE] = &m_StAdjustImageOrigin;
	pAdjustImg[AFTER_IMAGE] = &m_StAdjustImageAfter;
	pAdjustImg[RE_IMAGE] = &m_StAdjustImageRe;

	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	if( nSelectedItem < 0)
		return;


	if(m_nView == THIRD_ADJUST_VIEW)
	{
		DscAdjustInfo* pAdjustInfoRe = m_pAdjustMgrRe.GetDscAt(nSelectedItem);
		if(pAdjustInfoRe->pBmpBits == NULL)
		{
			FFmpegManager FFmpegMgr(FALSE);
			CString strFolder, strDscId, strDscIp;
			strDscId = pAdjustInfoRe->strDscName;
			strDscIp = pAdjustInfoRe->strDscIp;
			strFolder = ESMGetMoviePath(strDscId, 0);

			if( strFolder == _T(""))
				return ;

			BYTE* pBmpBits = NULL;
			int nWidht =0, nHeight = 0;
			FFmpegMgr.GetCaptureImage(strFolder, &pBmpBits,m_frameIndex, &nWidht, &nHeight, ESMGetGopSize());
			if( pBmpBits != NULL)
			{
				pAdjustInfoRe->pBmpBits = pBmpBits;
				pAdjustInfoRe->nWidht = nWidht;
				pAdjustInfoRe->nHeight = nHeight;
			}
		}
	}

	if(m_nView == SECOND_ADJUST_VIEW)
	{
		//Original
		{
			DscAdjustInfo* pAdjustInfoOrigin = m_pAdjustMgrOrigin.GetDscAt(nSelectedItem);
			if(pAdjustInfoOrigin->pBmpBits == NULL)
			{
				FFmpegManager FFmpegMgr(FALSE);
				CString strFolder, strDscId, strDscIp;
				strDscId = pAdjustInfoOrigin->strDscName;
				strDscIp = pAdjustInfoOrigin->strDscIp;
				strFolder = ESMGetMoviePath(strDscId, 0);

				if( strFolder == _T(""))
					return;

				BYTE* pBmpBits = NULL;
				int nWidht =0, nHeight = 0;
				FFmpegMgr.GetCaptureImage(strFolder, &pBmpBits,m_frameIndex, &nWidht, &nHeight, ESMGetGopSize());
				if( pBmpBits != NULL)
				{
					pAdjustInfoOrigin->pBmpBits = pBmpBits;
					pAdjustInfoOrigin->nWidht = nWidht;
					pAdjustInfoOrigin->nHeight = nHeight;
				}
			}

			if(m_bChangeFlag)
			{
				DscAdjustInfo* adjinfo = m_pAdjustMgrOrigin.GetDscAt(nSelectedItem);

				if(!adjinfo->bImgLoad)
				{
					//Image Load
					CString strAdjustFolder;
					strAdjustFolder.Format(_T("%s\\img"),ESMGetPath(ESM_PATH_SETUP));
					CString strFile;

					strFile.Format(_T("%s\\%d.jpg"), strAdjustFolder,nSelectedItem);
					char path[MAX_PATH];
					wcstombs(path, strFile, MAX_PATH);

					//wgkim 191018
					//IplImage* pImg = cvLoadImage(path);
					cv::Mat mImg = cv::imread(path);

					if(!mImg.empty())
					{
						if(adjinfo->pBmpBits && adjinfo->nWidht > 0 && adjinfo->nHeight > 0 )
							memcpy(
								adjinfo->pBmpBits, 
								(BYTE*)mImg.data, 
								mImg.total() * mImg.elemSize() * sizeof(byte));
					}
					adjinfo->bImgLoad = TRUE;
					ESMLog(5, _T("Adjust Img Load: %s\n"), strFile);
				}

			}
		}

		//After
		{
			DscAdjustInfo* pAdjustInfoAfter = m_pAdjustMgrAfter.GetDscAt(nSelectedItem);
			if(pAdjustInfoAfter->pBmpBits == NULL)
			{
				FFmpegManager FFmpegMgr(FALSE);
				CString strFolder, strDscId, strDscIp;
				strDscId = pAdjustInfoAfter->strDscName;
				strDscIp = pAdjustInfoAfter->strDscIp;
				strFolder = ESMGetMoviePath(strDscId, 0);

				if( strFolder == _T(""))
					return ;

				BYTE* pBmpBits = NULL;
				int nWidht =0, nHeight = 0;
				FFmpegMgr.GetCaptureImage(strFolder, &pBmpBits,m_frameIndex, &nWidht, &nHeight, ESMGetGopSize());
				if( pBmpBits != NULL)
				{
					pAdjustInfoAfter->pBmpBits = pBmpBits;
					pAdjustInfoAfter->nWidht = nWidht;
					pAdjustInfoAfter->nHeight = nHeight;
				}
			}
		}
	}


	m_nSelectedNum = nSelectedItem;
	CString strSelect = pList->GetItemText(nSelectedItem, 1);
	DscAdjustInfo* pAdjustInfo = NULL;
	pAdjustInfo = m_pAdjustMgr.GetDscAt(nSelectedItem);
	if(pAdjustInfo->pBmpBits != NULL || pAdjustImg[m_nImgView]->m_memDC == NULL)
	{
		switch(m_nView)
		{
		case FIRST_ADJUST_VIEW:
			TRACE(_T("####DrawImage\n"));
			DrawImage(ADJUST_IMAGE);
			break;
		case SECOND_ADJUST_VIEW:
			DrawImage(ORIGINAL_IMAGE);
			DrawImage(AFTER_IMAGE);
			break;
		case THIRD_ADJUST_VIEW:
			DrawImage(RE_IMAGE);
			break;
		}

		//-- 2016-11-01 wgkim@esmlab.com
		//-- Apply Calculated Adjust
		//-- 마지막 카메라에 도달시 자동으로 Auto Find옵션 체크해제
		if( m_bAutoFind == TRUE)
		{						
			adjustAsAuto.ApplyCalculatedAdjustFeatureToList(pList, nSelectedItem);		
			if(	m_nSelectedNum == pList->GetItemCount() - 1 )
			{			
				((CButton*)GetDlgItem(IDC_CHECK_AUTOFIND))->SetCheck(0);				
				GetDlgItem(IDC_CHECK_AUTOMOVE)->EnableWindow(TRUE);
			}
		}		

		DrawCenterPointAll();
	}
	else
	{
		DeleteImage();
	}
}
void ESMAdjustMgrDlg::DrawImage(int nImg)
{
	CListCtrl* pList;
	ESMAdjustImage* pAdjustImg;

	DscAdjustInfo* pAdjustInfo = NULL;
	switch(nImg)
	{
	case ADJUST_IMAGE:
		pList = &m_AdjustDscList;
		pAdjustImg = &m_StAdjustImage;
		break;
	case ORIGINAL_IMAGE:
		pList = &m_AdjustDsc2ndList;
		pAdjustImg = &m_StAdjustImageOrigin;
		break;
	case AFTER_IMAGE:
		pList = &m_AdjustDsc2ndList;
		pAdjustImg = &m_StAdjustImageAfter;
		break;
	case RE_IMAGE:
		pList = &m_AdjustDscList;
		pAdjustImg = &m_StAdjustImageRe;
		break;
	}

	UpdateData();
	int nSelectedItem = pList->GetNextItem( -1, LVNI_SELECTED );
	if( nSelectedItem < 0)
		return;
	bool s;
	m_nSelectedNum = nSelectedItem;
	CString strSelect = pList->GetItemText(nSelectedItem, 1);


	CString strLine1, strLine2, strLine3;
	GetDlgItem(IDC_EDT_LINE1)->GetWindowText(strLine1);
	GetDlgItem(IDC_EDT_LINE2)->GetWindowText(strLine2);
	GetDlgItem(IDC_EDT_CENTER_LINE)->GetWindowText(strLine3);

	if(ESMGetCudaSupport()) //GPU 지원
	{
		switch(nImg)
		{
		case ADJUST_IMAGE:
			pAdjustInfo = m_pAdjustMgr.GetDscAt(nSelectedItem);
			if( pAdjustInfo != NULL)
			{
				double dTargetDistance = 0;		
				if( m_RdImageViewColor  == 0)
					s = pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);		
				else
					s = pAdjustImg->SetImageBuffer(pAdjustInfo->pGrayBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);

			}	

			//-- Draw Line
			if(m_bCenterGrid)
			{
				pAdjustImg->DrawLine(pAdjustInfo->nHeight/2, RGB(0,0,0));
				pAdjustImg->DrawCenterLine( pAdjustInfo->nWidht, RGB(0,0,0));
			}


			pAdjustImg->DrawLine(_ttoi(strLine1), RGB(0,255,0));
			pAdjustImg->DrawLine(_ttoi(strLine2), RGB(0,255,0));
			pAdjustImg->Redraw();
			break;
		case ORIGINAL_IMAGE:
			//Original
			pAdjustInfo = m_pAdjustMgrOrigin.GetDscAt(nSelectedItem);
			if( pAdjustInfo != NULL)
			{
				pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);
				pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
				pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
				//pAdjustImg->DrawLine(_ttoi(strLine1), RGB(0,255,0));
				//pAdjustImg->DrawLine(_ttoi(strLine2), RGB(0,255,0));
				pAdjustImg->Redraw();
			}
			break;
		case AFTER_IMAGE:
			{				
				pAdjustInfo = m_pAdjustMgrAfter.GetDscAt(nSelectedItem);
				if( pAdjustInfo != NULL)
				{
					if(pAdjustInfo->bAdjust )
					{
						pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);
						pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
						pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
						//pAdjustImg->DrawLine(_ttoi(strLine1), RGB(0,255,0));
						//pAdjustImg->DrawLine(_ttoi(strLine2), RGB(0,255,0));
						pAdjustImg->Redraw();
						break;
					}
					else if(pAdjustInfo->nHeight == 0 || pAdjustInfo->nWidht == 0)
						break;

					Mat src(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
					memcpy(src.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);
					cuda::GpuMat* pImageMat = new cuda::GpuMat;
					pImageMat->upload(src);

					if( pImageMat == NULL)
					{
						TRACE(_T("Image Error \n"));
						return ;
					}


					int nRotateX = 0, nRotateY = 0, nSize = 0;
					double dSize = 0.0;
					double dRatio = 1;
					stAdjustInfo AdjustData = m_adjinfo.at(m_nSelectedNum);
					if(!pAdjustInfo->bRotate)
					{
						// Rotate 구현					
						nRotateX = Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
						nRotateY = Round(AdjustData.AdjptRotate.y  * dRatio );
						GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
					}

					if(!pAdjustInfo->bMove)
					{
						//-- MOVE EVENT
						int nMoveX = 0, nMoveY = 0;
						nMoveX = Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
						nMoveY = Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
						GpuMoveImage(pImageMat, nMoveX,  nMoveY);
					}

					if(!pAdjustInfo->bImgCut)
					{
						//-- IMAGE CUT EVENT
						int nMarginX = m_MarginX * dRatio;
						int nMarginY = m_MarginY * dRatio;
						if ( nMarginX < 0 || nMarginX >= pAdjustInfo->nWidht/2 || nMarginY < 0 || nMarginY >= pAdjustInfo->nHeight/2  )
						{
							TRACE(_T("Image Adjust Error 2\n"));
							return ;
						}
						//GpuMakeMargin(pImageMat, nMarginX, nMarginY);
						double dbMarginScale = 1 / ( (double)( pAdjustInfo->nWidht - nMarginX * 2) /(double) pAdjustInfo->nWidht  );
						cuda::GpuMat* pMatResize = new cuda::GpuMat;
						cuda::resize(*pImageMat, *pMatResize, cv::Size(pAdjustInfo->nWidht*dbMarginScale ,pAdjustInfo->nHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
						int nLeft = (int)((pAdjustInfo->nWidht*dbMarginScale - pAdjustInfo->nWidht)/2);
						int nTop = (int)((pAdjustInfo->nHeight*dbMarginScale - pAdjustInfo->nHeight)/2);
						cuda::GpuMat pMatCut =  (*pMatResize)(cv::Rect(nLeft, nTop, pAdjustInfo->nWidht, pAdjustInfo->nHeight));
						pMatCut.copyTo(*pImageMat);
						delete pMatResize;
					}


					Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
					pImageMat->download(srcImage);
					int size = srcImage.total() * srcImage.elemSize();
					memcpy(pAdjustInfo->pBmpBits, srcImage.data, size);
					s = pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);	

					pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
					pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
					//pAdjustImg->DrawLine(_ttoi(strLine1), RGB(0,255,0));
					//pAdjustImg->DrawLine(_ttoi(strLine2), RGB(0,255,0));
					pAdjustImg->Redraw();

					pAdjustInfo->bAdjust = TRUE;
					pAdjustInfo->bRotate = TRUE;
					pAdjustInfo->bMove = TRUE;
					pAdjustInfo->bImgCut = TRUE;

				}
			}
			break;
		case RE_IMAGE:
			{
				pAdjustInfo = m_pAdjustMgrRe.GetDscAt(nSelectedItem);
				if( pAdjustInfo != NULL)
				{
					if(pAdjustInfo->bAdjust )
					{
						pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);
						pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
						pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
						//pAdjustImg->DrawLine(_ttoi(strLine1), RGB(0,255,0));
						//pAdjustImg->DrawLine(_ttoi(strLine2), RGB(0,255,0));
						pAdjustImg->Redraw();
						break;
					}
					else if(pAdjustInfo->nHeight == 0 || pAdjustInfo->nWidht == 0)
						break;

					Mat src(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
					memcpy(src.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);
					cuda::GpuMat* pImageMat = new cuda::GpuMat;
					pImageMat->upload(src);

					int i_row = src.rows;
					int i_col = src.cols;

					int nRotateX = 0, nRotateY = 0, nSize = 0;
					double dSize = 0.0;
					double dRatio = 1;
					stAdjustInfo AdjustData = m_adjinfo.at(m_nSelectedNum);
					if(AdjustData.AdjptRotate.x == 0.0 && AdjustData.AdjptRotate.y == 0.0 && AdjustData.AdjAngle == 0.0)
					{
						m_adjinfo.at(m_nSelectedNum).AdjptRotate.x = i_col / 2;
						m_adjinfo.at(m_nSelectedNum).AdjptRotate.y = i_row / 2;
						m_adjinfo.at(m_nSelectedNum).AdjSize = 1.0;
						m_adjinfo.at(m_nSelectedNum).AdjAngle = -90;

						AdjustData.AdjptRotate.x = i_col / 2;
						AdjustData.AdjptRotate.y = i_row / 2;
						AdjustData.AdjSize = 1.0;
						AdjustData.AdjAngle = -90;
					}
					if(!pAdjustInfo->bRotate)
					{
						// Rotate 구현						
						nRotateX = Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
						nRotateY = Round(AdjustData.AdjptRotate.y  * dRatio );
						GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
					}

					if(!pAdjustInfo->bMove)
					{
						//-- MOVE EVENT
						int nMoveX = 0, nMoveY = 0;
						nMoveX = Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
						nMoveY = Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
						GpuMoveImage(pImageMat, nMoveX,  nMoveY);
					}

					if(!pAdjustInfo->bImgCut)
					{
						//-- IMAGE CUT EVENT
						int nMarginX = m_MarginX * dRatio;
						int nMarginY = m_MarginY * dRatio;


						if ( nMarginX < 0 || nMarginX >= pAdjustInfo->nWidht/2 || nMarginY < 0 || nMarginY >= pAdjustInfo->nHeight/2  )
						{
							ESMLog(0,_T("dRatio = %f, Mx = %d, My = %d, w = %d, h = %d"), dRatio, nMarginX, nMarginY, pAdjustInfo->nWidht, pAdjustInfo->nHeight);
							TRACE(_T("Image Adjust Error 2\n"));
							return ;
						}
						//GpuMakeMargin(pImageMat, nMarginX, nMarginY);
						double dbMarginScale = 1 / ( (double)( pAdjustInfo->nWidht - nMarginX * 2) /(double) pAdjustInfo->nWidht  );
						cuda::GpuMat* pMatResize = new cuda::GpuMat;
						cuda::resize(*pImageMat, *pMatResize, cv::Size(pAdjustInfo->nWidht*dbMarginScale ,pAdjustInfo->nHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );

						int nLeft = (int)((pAdjustInfo->nWidht*dbMarginScale - pAdjustInfo->nWidht)/2);
						int nTop = (int)((pAdjustInfo->nHeight*dbMarginScale - pAdjustInfo->nHeight)/2);
						cuda::GpuMat pMatCut =  (*pMatResize)(cv::Rect(nLeft, nTop, pAdjustInfo->nWidht, pAdjustInfo->nHeight));
						pMatCut.copyTo(*pImageMat);
						delete pMatResize;

						Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
						pImageMat->download(srcImage);
						int size = srcImage.total() * srcImage.elemSize();

						Mat Loaded(pImageMat->rows, pImageMat->cols, CV_8UC3);
						Mat result(pImageMat->rows, pImageMat->cols, CV_8UC3);
						DscAdjustInfo* pMemoryData = m_pAdjustMgrMemory.GetDscAt(nSelectedItem);

						if(m_ImageLoaded == 1)
						{
							if(m_pAdjustMgrRe.GetDscAt(nSelectedItem)->strDscName == m_pAdjustMgrMemory.GetDscAt(nSelectedItem)->strDscName)
							{ 
								if(pMemoryData->pBmpBits != NULL && srcImage.data != NULL)
								{
									memcpy(Loaded.data,(BYTE*)pMemoryData->pBmpBits,size);

									if(m_recals == 1) subtract(srcImage,Loaded,result,noArray(),-1);
									else if(m_recals==2) Matrix_Image(srcImage,Loaded,result);
									else MessageBox(_T("ReCalibration\Choose a method"),_T("ReCalibration"),NULL);
									memcpy((BYTE*)pAdjustInfo->pBmpBits, result.data, size);//Highlight
								}
							}	
						}
						else
						{
							memcpy((BYTE*)pAdjustInfo->pBmpBits,srcImage.data,size);
						}
					}				
					delete pImageMat;
					s = pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);	

					pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
					pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
					//pAdjustImg->DrawLine(_ttoi(strLine1), RGB(0,255,0));
					//pAdjustImg->DrawLine(_ttoi(strLine2), RGB(0,255,0));
					pAdjustImg->Redraw();
					pAdjustInfo->bAdjust = TRUE;
					pAdjustInfo->bRotate = TRUE;
					pAdjustInfo->bMove = TRUE;
					pAdjustInfo->bImgCut = TRUE;
				}
			}
			break;
		}
	}
	else //Cpu 사용
	{
		switch(nImg)
		{
		case ADJUST_IMAGE:
			pAdjustInfo = m_pAdjustMgr.GetDscAt(nSelectedItem);

			if(pAdjustInfo->pBmpBits == NULL)
			{
				AfxMessageBox(_T("Wait Loading..."));
				return;
			}

			if( pAdjustInfo != NULL)
			{
				double dTargetDistance = 0;		
				if( m_RdImageViewColor  == 0)
					s = pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);		
				else
					s = pAdjustImg->SetImageBuffer(pAdjustInfo->pGrayBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);

			}	

			//-- Draw Line
			if(m_bCenterGrid)
			{
				pAdjustImg->DrawLine(pAdjustInfo->nHeight/2, RGB(0,0,0));
				pAdjustImg->DrawCenterLine( pAdjustInfo->nWidht, RGB(0,0,0));
			}


			pAdjustImg->DrawLine(_ttoi(strLine1), RGB(0,255,0));
			pAdjustImg->DrawLine(_ttoi(strLine2), RGB(0,255,0));
			pAdjustImg->Redraw();

			//-- 2016-11-01 wgkim@esmlab.com
			if( m_bAutoFind == TRUE)
			{
				adjustAsAuto.CalcAutoAdjustUsingOpticalFlow(cvarrToMat(pAdjustImg->m_pImage));		
			}

			break;
		case ORIGINAL_IMAGE:
			//Original
			pAdjustInfo = m_pAdjustMgrOrigin.GetDscAt(nSelectedItem);
			if( pAdjustInfo != NULL)
			{
				pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);
				pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
				pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
				pAdjustImg->Redraw();
			}
			break;
		case AFTER_IMAGE:
			{				
				pAdjustInfo = m_pAdjustMgrAfter.GetDscAt(nSelectedItem);
				if( pAdjustInfo != NULL)
				{
					if(pAdjustInfo->bAdjust )
					{
						pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);
						pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
						pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
						pAdjustImg->Redraw();
						break;
					}
					else if(pAdjustInfo->nHeight == 0 || pAdjustInfo->nWidht == 0)
						break;

					Mat src(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
					memcpy(src.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);

					int nRotateX = 0, nRotateY = 0, nSize = 0;
					double dSize = 0.0;
					double dRatio = 1;
					stAdjustInfo AdjustData = m_adjinfo.at(m_nSelectedNum);

					if(!pAdjustInfo->bRotate)
					{
						// Rotate 구현					
						nRotateX = Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
						nRotateY = Round(AdjustData.AdjptRotate.y  * dRatio );

						//IplImage *Iimage = new IplImage(src);
						CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);;
						//delete Iimage;
					}
					//imwrite("C:\\Program Files\\ESMLab\\4DMaker\\img\\rotated.png",src);

					//imshow("rotate",src);

					if(!pAdjustInfo->bMove)
					{
						//-- MOVE EVENT
						int nMoveX = 0, nMoveY = 0;
						nMoveX = Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
						nMoveY = Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
						CpuMoveImage(&src, nMoveX,  nMoveY);
					}

					if(!pAdjustInfo->bImgCut)
					{
						//-- IMAGE CUT EVENT
						int nMarginX = m_MarginX * dRatio;
						int nMarginY = m_MarginY * dRatio;
						if ( nMarginX < 0 || nMarginX >= pAdjustInfo->nWidht/2 || nMarginY < 0 || nMarginY >= pAdjustInfo->nHeight/2  )
						{
							TRACE(_T("Image Adjust Error 2\n"));
							return ;
						}
						//CpuMakeMargin(&src, nMarginX, nMarginY);

						double dbMarginScale = 1 / ( (double)( pAdjustInfo->nWidht - nMarginX * 2) /(double) pAdjustInfo->nWidht  );
						Mat  pMatResize;

						resize(src, pMatResize, cv::Size(pAdjustInfo->nWidht*dbMarginScale ,pAdjustInfo->nHeight * dbMarginScale ), 0.0, 0.0 ,CV_INTER_NN);
						int nLeft = (int)((pAdjustInfo->nWidht*dbMarginScale - pAdjustInfo->nWidht)/2);
						int nTop = (int)((pAdjustInfo->nHeight*dbMarginScale - pAdjustInfo->nHeight)/2);
						Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, pAdjustInfo->nWidht, pAdjustInfo->nHeight));
						pMatCut.copyTo(src);
						pMatResize = NULL;
					}

					int size = src.total() * src.elemSize();
					memcpy(pAdjustInfo->pBmpBits, src.data, size);
					s = pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);	

					pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
					pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
					pAdjustImg->Redraw();

					pAdjustInfo->bAdjust = TRUE;
					pAdjustInfo->bRotate = TRUE;
					pAdjustInfo->bMove = TRUE;
					pAdjustInfo->bImgCut = TRUE;

				}
			}
			break;
		case RE_IMAGE:
			{
				pAdjustInfo = m_pAdjustMgrRe.GetDscAt(nSelectedItem);
				if( pAdjustInfo != NULL)
				{
					if(pAdjustInfo->bAdjust )
					{
						pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);
						pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
						pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
						pAdjustImg->Redraw();
						break;
					}
					else if(pAdjustInfo->nHeight == 0 || pAdjustInfo->nWidht == 0)
						break;


					Mat src(pAdjustInfo->nHeight, pAdjustInfo->nWidht, CV_8UC3);
					memcpy(src.data, pAdjustInfo->pBmpBits, pAdjustInfo->nHeight * pAdjustInfo->nWidht * 3);

					int nRotateX = 0, nRotateY = 0, nSize = 0;
					double dSize = 0.0;
					double dRatio = 1;
					stAdjustInfo AdjustData = m_adjinfo.at(m_nSelectedNum);
					int i_col = src.cols;
					int i_row = src.rows;
					if(AdjustData.AdjptRotate.x == 0.0 && AdjustData.AdjptRotate.y == 0.0 && AdjustData.AdjAngle == 0.0)
					{
						m_adjinfo.at(m_nSelectedNum).AdjptRotate.x = i_col / 2;
						m_adjinfo.at(m_nSelectedNum).AdjptRotate.y = i_row / 2;
						m_adjinfo.at(m_nSelectedNum).AdjSize = 1.0;
						m_adjinfo.at(m_nSelectedNum).AdjAngle = -90;


						AdjustData.AdjptRotate.x = i_col / 2;
						AdjustData.AdjptRotate.y = i_row / 2;
						AdjustData.AdjSize = 1.0;
						AdjustData.AdjAngle = -90;
					}
					if(!pAdjustInfo->bRotate)
					{
						// Rotate 구현					
						nRotateX = Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
						nRotateY = Round(AdjustData.AdjptRotate.y  * dRatio );
						//IplImage *Iimage = new IplImage(src);

						CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
					}

					if(!pAdjustInfo->bMove)
					{
						//-- MOVE EVENT
						int nMoveX = 0, nMoveY = 0;
						nMoveX = Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
						nMoveY = Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
						CpuMoveImage(&src, nMoveX,  nMoveY);
					}

					if(!pAdjustInfo->bImgCut)
					{
						//-- IMAGE CUT EVENT
						int nMarginX = m_MarginX * dRatio;
						int nMarginY = m_MarginY * dRatio;


						if ( nMarginX < 0 || nMarginX >= pAdjustInfo->nWidht/2 || nMarginY < 0 || nMarginY >= pAdjustInfo->nHeight/2  )
						{
							ESMLog(0,_T("dRatio = %f, Mx = %d, My = %d, w = %d, h = %d"), dRatio, nMarginX, nMarginY, pAdjustInfo->nWidht, pAdjustInfo->nHeight);
							TRACE(_T("Image Adjust Error 2\n"));
							return ;
						}

						//CpuMakeMargin(&src, nMarginX, nMarginY);

						double dbMarginScale = 1 / ( (double)( pAdjustInfo->nWidht - nMarginX * 2) /(double) pAdjustInfo->nWidht  );
						Mat  pMatResize;
						resize(src, pMatResize, cv::Size(pAdjustInfo->nWidht*dbMarginScale ,pAdjustInfo->nHeight * dbMarginScale ), 0.0, 0.0 ,cv::INTER_LANCZOS4);		
						int nLeft = (int)((pAdjustInfo->nWidht*dbMarginScale - pAdjustInfo->nWidht)/2);
						int nTop = (int)((pAdjustInfo->nHeight*dbMarginScale - pAdjustInfo->nHeight)/2);
						Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, pAdjustInfo->nWidht, pAdjustInfo->nHeight));
						pMatCut.copyTo(src);

						pMatResize = NULL;

						int size = src.total() * src.elemSize();
						memcpy((BYTE*)pAdjustInfo->pBmpBits,src.data,size);


						Mat Loaded(src.rows, src.cols, CV_8UC3);
						Mat result(src.rows, src.cols, CV_8UC3);
						DscAdjustInfo* pMemoryData = m_pAdjustMgrMemory.GetDscAt(nSelectedItem);

						if(m_ImageLoaded == 1)
						{
							if(m_pAdjustMgrRe.GetDscAt(nSelectedItem)->strDscName == 
								m_pAdjustMgrMemory.GetDscAt(nSelectedItem)->strDscName)
							{ 
								if(pMemoryData->pBmpBits != NULL && src.data != NULL)
								{
									memcpy(Loaded.data,(BYTE*)pMemoryData->pBmpBits,size);
									if(m_recals == 1) subtract(src,Loaded,result,noArray(),-1);
									else if(m_recals==2) Matrix_Image(src,Loaded,result);
									else MessageBox(_T("ReCalibration\Choose a method"),_T("ReCalibration"),NULL);
									memcpy((BYTE*)pAdjustInfo->pBmpBits, result.data,size);//Highlight
								}
							}	
						}
						else
						{
							memcpy((BYTE*)pAdjustInfo->pBmpBits,src.data,size);
						}
					}				

					s = pAdjustImg->SetImageBuffer(pAdjustInfo->pBmpBits, pAdjustInfo->nWidht, pAdjustInfo->nHeight, 3);	

					pAdjustImg->DrawLine(_ttoi(strLine3), RGB(255,0,0));
					pAdjustImg->DrawCenterLine(pAdjustInfo->nWidht, RGB(255,0,0));
					//pAdjustImg->DrawLine(_ttoi(strLine1), RGB(0,255,0));
					//pAdjustImg->DrawLine(_ttoi(strLine2), RGB(0,255,0));
					pAdjustImg->Redraw();
					pAdjustInfo->bAdjust = TRUE;
					pAdjustInfo->bRotate = TRUE;
					pAdjustInfo->bMove = TRUE;
					pAdjustInfo->bImgCut = TRUE;
				}
			}
			break;
		}
	}



	//m_nSelectPos = 0;
}

int ESMAdjustMgrDlg::Round(double dData)
{
	int nResult = 0;
	if( dData == 0)
		nResult = 0;
	else if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);

	return nResult;
}
void ESMAdjustMgrDlg::GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}


	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	Mat rot_mat;
	rot_mat.create(cv::Size(2, 3), CV_32FC1);

	cv::Point rot_center = cvPoint2D32f( nCenterX, nCenterY);

	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	//CString strTemp;
	//strTemp.Format(_T("%f"), dbAngleAdjust);
	//m_AdjustDsc2ndList.SetItemText(m_nSelectedNum, 17, strTemp);

	//cv2DRotationMatrix( rot_center, dbAngleAdjust, dScale, rot_mat);
	rot_mat = getRotationMatrix2D(rot_center, dbAngleAdjust, dScale);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), cv::INTER_CUBIC+cv::WARP_FILL_OUTLIERS);
	cuda::GpuMat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cuda::warpAffine(*gMat, d_rotate, rot_mat, cv::Size(gMat->cols, gMat->rows), cv::INTER_CUBIC);
	d_rotate.copyTo(*gMat);

	//cvReleaseMat(&rot_mat);

}
void ESMAdjustMgrDlg::GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;


	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void ESMAdjustMgrDlg::GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void ESMAdjustMgrDlg::CpuMakeMargin(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;


	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);

}
void ESMAdjustMgrDlg::CpuMoveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void ESMAdjustMgrDlg::CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	Mat Iimage2;// = cvCreateImage(cvGetSize(Iimage), IPL_DEPTH_8U, 3);

	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;
	//cvShowImage("2",Iimage);
	CvMat *rot_mat = cvCreateMat( 2, 3, CV_32FC1);
	CvPoint2D32f rot_center = cvPoint2D32f( nCenterX, nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	//CString strTemp;
	//strTemp.Format(_T("%f"), dbAngleAdjust);
	//m_AdjustDsc2ndList.SetItemText(m_nSelectedNum, 17, strTemp);

	cv2DRotationMatrix( rot_center, dbAngleAdjust, dScale, rot_mat);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), cv::INTER_CUBIC+cv::WARP_FILL_OUTLIERS);
	//Mat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cvWarpAffine(&IplImage(Iimage), &IplImage(Iimage), rot_mat,cv::INTER_LINEAR+cv::WARP_FILL_OUTLIERS);	//d_rotate.copyTo(*gMat);
	//imshow("Iimage",Iimage);
	//cvWaitKey(0);
	//gMat =&(cvarrToMat(Iimage2));//
	//Iimage = cvCreateImage(cvGetSize(&Iimage), IPL_DEPTH_8U, 3);
	cvReleaseMat(&rot_mat);
}
void ESMAdjustMgrDlg::DeleteImage()
{
	ESMAdjustImage* pAdjustImg;
	switch(m_nView)
	{
	case FIRST_ADJUST_VIEW:
		pAdjustImg = &m_StAdjustImage;
		break;
	case SECOND_ADJUST_VIEW:
		pAdjustImg = &m_StAdjustImageOrigin;
		break;
	case THIRD_ADJUST_VIEW:
		pAdjustImg = &m_StAdjustImageRe;
		break;
	}

	pAdjustImg->m_memDC->FillSolidRect(0,0,m_StAdjustImage.m_nImageWidth,m_StAdjustImage.m_nImageHeight,GetSysColor(COLOR_BTNFACE));
}
void ESMAdjustMgrDlg::OnBnClickedBtnAutosearch()
{
	UpdateData();

	CString strTemp;
	GetDlgItem(IDC_CHECK_CAMERAZOOM)->GetWindowText(strTemp);
	m_nTargetLenth = _wtoi(strTemp);

	GetDlgItem(IDC_CHECK_CAMERAZOOM)->GetWindowText(strTemp);
	m_nCameraZoom = _wtoi(strTemp);

	GetDlgItem(IDC_EDT_ADJUSTTHRESHOLD)->GetWindowText(strTemp);
	m_nThreshold = _wtoi(strTemp);

	GetDlgItem(IDC_EDT_ADJUST_POINTGAPMIN)->GetWindowText(strTemp);
	m_nPointGapMin = _wtoi(strTemp);

	GetDlgItem(IDC_EDT_ADJUST_MIN_WIDTH)->GetWindowText(strTemp);
	int nWidth = _wtoi(strTemp);
	GetDlgItem(IDC_EDT_ADJUST_MIN_HEIGHT)->GetWindowText(strTemp);
	int nHeight = _wtoi(strTemp);
	m_sizeDetectMIN.SetSize(nWidth, nHeight);

	GetDlgItem(IDC_EDT_ADJUST_MAX_WIDTH)->GetWindowText(strTemp);
	nWidth = _wtoi(strTemp);
	GetDlgItem(IDC_EDT_ADJUST_MAX_HEIGHT)->GetWindowText(strTemp);
	nHeight = _wtoi(strTemp);
	m_sizeDetectMAX.SetSize(nWidth, nHeight);

	DscAdjustInfo* pAdjustInfo;
	for(int i =0; i< m_pAdjustMgr.GetDscCount(); i++)
	{
		pAdjustInfo = m_pAdjustMgr.GetDscAt(i);

		CString strText = _T("");
		pAdjustInfo->HighPos.SetPoint(0,0);
		m_AdjustDscList.SetItemText(i, 2, strText);
		m_AdjustDscList.SetItemText(i, 3, strText);

		pAdjustInfo->MiddlePos.SetPoint(0,0);
		m_AdjustDscList.SetItemText(i, 4, strText);
		m_AdjustDscList.SetItemText(i, 5, strText);

		pAdjustInfo->LowPos.SetPoint(0,0);
		m_AdjustDscList.SetItemText(i, 6, strText);
		m_AdjustDscList.SetItemText(i, 7, strText);

		m_pAdjustMgr.GetSearchDetectPoint(pAdjustInfo, m_nThreshold, m_sizeDetectMIN, m_sizeDetectMAX, m_nPointGapMin, m_clrDetectColor);

		if(pAdjustInfo->HighPos != CPoint(0,0))
		{
			strText.Format(_T("%d"), pAdjustInfo->HighPos.x);
			m_AdjustDscList.SetItemText(i, 2, strText);
			strText.Format(_T("%d"), pAdjustInfo->HighPos.y);
			m_AdjustDscList.SetItemText(i, 3, strText);
		}
		if(pAdjustInfo->MiddlePos != CPoint(0,0))
		{
			strText.Format(_T("%d"), pAdjustInfo->MiddlePos.x);
			m_AdjustDscList.SetItemText(i, 4, strText);
			strText.Format(_T("%d"), pAdjustInfo->MiddlePos.y);
			m_AdjustDscList.SetItemText(i, 5, strText);
		}
		if(pAdjustInfo->LowPos != CPoint(0,0))
		{
			strText.Format(_T("%d"), pAdjustInfo->LowPos.x);
			m_AdjustDscList.SetItemText(i, 6, strText);
			strText.Format(_T("%d"), pAdjustInfo->LowPos.y);
			m_AdjustDscList.SetItemText(i, 7, strText);
		}
	}
	CalcAdjustData();

	DrawImage();
}
/*
unsigned WINAPI ESMAdjustMgrDlg::GetSearchThread(LPVOID param)
{
ESMAdjustMgrDlg* pAdjustMgrDlg = (ESMAdjustMgrDlg*)param;

DscAdjustInfo* pAdjustInfo;
for(int i =0; i< pAdjustMgrDlg->m_pAdjustMgr.GetDscCount(); i++)
{
pAdjustInfo = pAdjustMgrDlg->m_pAdjustMgr.GetDscAt(i);
int nChannel = 3;
pAdjustMgrDlg->m_pAdjustMgr.GetSearchPoint(pAdjustInfo, pAdjustMgrDlg->m_nThreshold, nChannel);
}
return 0;
}
*/
unsigned WINAPI ESMAdjustMgrDlg::GetMovieDataThread(LPVOID param)
{	
	ESMAdjustMgrDlg* pAdjustMgrDlg = (ESMAdjustMgrDlg*)param;
	DscAdjustInfo* pAdjustInfo = NULL;
	DscAdjustInfo* pAdjustInfoOrigin = NULL;
	DscAdjustInfo* pAdjustInfoAfter = NULL;
	BOOL bFirst = TRUE;

	for(int i =0; i< pAdjustMgrDlg->m_pAdjustMgr.GetDscCount(); i++)
	{
		if(!pAdjustMgrDlg->m_bThreadLoad)
		{
			TRACE(_T("###########################################Thread Stop!!!\n"));
			pAdjustMgrDlg->m_bThreadStop = TRUE;
			return 0;
		}

		//Adjust
		{
			pAdjustInfo = pAdjustMgrDlg->m_pAdjustMgr.GetDscAt(i);
			
			FFmpegManager FFmpegMgr(FALSE);
			CString strFolder, strDscId, strDscIp;
			strDscId = pAdjustInfo->strDscName;
			strDscIp = pAdjustInfo->strDscIp;
			//strFolder = ESMGetMoviePath(strDscId, 0);
			strFolder = ESMGetMoviePath(strDscId, pAdjustMgrDlg->m_frameIndex);
			int nFrameIndex = 0;
			if(pAdjustMgrDlg->m_frameIndex > 0)
			{
				nFrameIndex = pAdjustMgrDlg->m_frameIndex % 30;
			}

			if( strFolder == _T(""))
				return 0;

			BYTE* pBmpBits = NULL;
			int nWidht =0, nHeight = 0;
			//FFmpegMgr.GetCaptureImage(strFolder, &pBmpBits,pAdjustMgrDlg->m_frameIndex, &nWidht, &nHeight);
			FFmpegMgr.GetCaptureImage(strFolder, &pBmpBits,nFrameIndex, &nWidht, &nHeight, ESMGetGopSize(),pAdjustInfo->bReverse);

			if( pBmpBits != NULL)
			{
				pAdjustInfo->pBmpBits = pBmpBits;
				pAdjustInfo->nWidht = nWidht;
				pAdjustInfo->nHeight = nHeight;

				if(bFirst)
				{
					bFirst = FALSE;
					pAdjustMgrDlg->m_nAdjViewHeight = nHeight;
					pAdjustMgrDlg->m_nAdjViewWidth = nWidht;

					int nLine1;// = pAdjustMgrDlg->m_nAdjViewHeight * 0.33;
					int nLine2;// = pAdjustMgrDlg->m_nAdjViewHeight * 0.66;

					if(ESMGetValue(ESM_VALUE_HORIZON) == FALSE)
					{
						nLine1 = pAdjustMgrDlg->m_nAdjViewHeight * 0.33;
						nLine2 = pAdjustMgrDlg->m_nAdjViewHeight * 0.66;
					}
					else
					{
						nLine1 = pAdjustMgrDlg->m_nAdjViewWidth * 0.33;
						nLine2 = pAdjustMgrDlg->m_nAdjViewWidth * 0.66;
					}
					int nLine3 = pAdjustMgrDlg->m_nAdjViewHeight * 0.5;

					CString	 strLine1, strLine2, strLine3;
					strLine1.Format(_T("%d"), nLine1);
					strLine2.Format(_T("%d"), nLine2);
					strLine3.Format(_T("%d"), nLine3);
					pAdjustMgrDlg->GetDlgItem(IDC_EDT_LINE1)->SetWindowText(strLine1);
					pAdjustMgrDlg->GetDlgItem(IDC_EDT_LINE2)->SetWindowText(strLine2);
					pAdjustMgrDlg->GetDlgItem(IDC_EDT_CENTER_LINE)->SetWindowText(strLine3);
				}

			}
		}


		/*//Original
		{
		pAdjustInfoOrigin = pAdjustMgrDlg->m_pAdjustMgrOrigin.GetDscAt(i);

		FFmpegManager FFmpegMgr(FALSE);
		CString strFolder, strDscId, strDscIp;
		strDscId = pAdjustInfoOrigin->strDscName;
		strDscIp = pAdjustInfoOrigin->strDscIp;
		strFolder = ESMGetMoviePath(strDscId, 0);

		if( strFolder == _T(""))
		return 0;

		BYTE* pBmpBits = NULL;
		int nWidht =0, nHeight = 0;
		FFmpegMgr.GetCaptureImage(strFolder, &pBmpBits,pAdjustMgrDlg->m_frameIndex, &nWidht, &nHeight);
		if( pBmpBits != NULL)
		{
		pAdjustInfoOrigin->pBmpBits = pBmpBits;
		pAdjustInfoOrigin->nWidht = nWidht;
		pAdjustInfoOrigin->nHeight = nHeight;
		}
		}

		//After
		{
		pAdjustInfoAfter = pAdjustMgrDlg->m_pAdjustMgrAfter.GetDscAt(i);
		FFmpegManager FFmpegMgr(FALSE);
		CString strFolder, strDscId, strDscIp;
		strDscId = pAdjustInfoAfter->strDscName;
		strDscIp = pAdjustInfoAfter->strDscIp;
		strFolder = ESMGetMoviePath(strDscId, 0);

		if( strFolder == _T(""))
		return 0;

		BYTE* pBmpBits = NULL;
		int nWidht =0, nHeight = 0;
		FFmpegMgr.GetCaptureImage(strFolder, &pBmpBits,pAdjustMgrDlg->m_frameIndex, &nWidht, &nHeight);
		if( pBmpBits != NULL)
		{
		pAdjustInfoAfter->pBmpBits = pBmpBits;
		pAdjustInfoAfter->nWidht = nWidht;
		pAdjustInfoAfter->nHeight = nHeight;
		}
		}*/

		//wgkim@esmlab.com 17-06-21 
		if(pAdjustMgrDlg->m_bThreadCloseFlag == TRUE)
			return 0;

	}
	pAdjustMgrDlg->m_bThreadStop = TRUE;
	return 0;
}
unsigned WINAPI ESMAdjustMgrDlg::GetImageDataThread(LPVOID param)
{	
	//DscAdjustInfo* pAdjustInfo= (DscAdjustInfo*)param;

	//CString strFolder, strDscId;
	//strDscId = pAdjustInfo->strDscName;
	////if(ESMGetFrameRecord().GetLength())
	////	strFolder.Format(_T("%s\\%s\\%s.jpg"), ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), strDscId);

	//if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	//	strFolder = ESMGetPicturePath(strDscId);
	//else
	//	strFolder.Format(_T("%s\\%s\\%s.jpg"), ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), strDscId);

	//if( strFolder == _T(""))
	//	return 0;

	//IplImage *pImage, *pTpImage;
	//int nTarHeight = 1238, nTarWidth = 2200;

	//char pchPath[MAX_PATH] = {0};
	//int nRequiredSize = (int)wcstombs(pchPath, strFolder, MAX_PATH); 
	//pTpImage = cvLoadImage(pchPath);
	////pImage = cvLoadImage(pchPath);
	//// 	pTpImage = cvCreateImage(cvSize(nTarWidth, nTarHeight), pImage->depth, pImage->nChannels); 
	//// 	cvResize(pImage, pTpImage);

	//pTpImage->width;
	//pTpImage->height;
	//BYTE* pBmpBits = NULL;
	//int nWidht =0, nHeight = 0;
	//pBmpBits = new BYTE[pTpImage->height * pTpImage->widthStep];
	//memcpy(pBmpBits, pTpImage->imageData, pTpImage->height * pTpImage->widthStep);
	//if( pBmpBits != NULL)
	//{
	//	pAdjustInfo->pBmpBits = pBmpBits;
	//	pAdjustInfo->nWidht = pTpImage->width;
	//	pAdjustInfo->nHeight = pTpImage->height;
	//}
	////cvReleaseImage( &pImage );
	//cvReleaseImage( &pTpImage );

	return 0;
}
void ESMAdjustMgrDlg::OnBnClickedRdAdjustviewcolor()
{
	DrawImage();
}
void ESMAdjustMgrDlg::OnBnClickedRdAdjustviewgray()
{
	DrawImage();
}
void ESMAdjustMgrDlg::OnBnClickedRdAdjustDetectBlack()
{
	m_clrDetectColor = COLOR_BLACK;
}
void ESMAdjustMgrDlg::OnBnClickedRdAdjustDetectWhite()
{
	m_clrDetectColor = COLOR_WHITE;
}
void ESMAdjustMgrDlg::OnBnClickedBtnCalcadjust()
{
	OnBnClickedBtnCalcdistance();
	CalcAdjustData();
}
void ESMAdjustMgrDlg::CalcAdjustData()
{
	UpdateData(TRUE);
	double dTpPosX = 0.0, dTpPosY = 0.0;
	DscAdjustInfo* pAdjustInfo;
	CString strCtrlName, strTp;
	for( int i =0 ;i < m_AdjustDscList.GetItemCount(); i++ )
	{
		strCtrlName = m_AdjustDscList.GetItemText(i, 1);
		for( int j =0; j< m_pAdjustMgr.GetDscCount(); j++)
		{
			pAdjustInfo = m_pAdjustMgr.GetDscAt(i);
			if( pAdjustInfo->strDscName == strCtrlName)
			{
				strTp = m_AdjustDscList.GetItemText(i, 3);
				pAdjustInfo->HighPos.x = _ttoi(strTp);
				strTp = m_AdjustDscList.GetItemText(i, 4);
				pAdjustInfo->HighPos.y = _ttoi(strTp);;
				strTp = m_AdjustDscList.GetItemText(i, 5);
				pAdjustInfo->MiddlePos.x = _ttoi(strTp);;
				strTp = m_AdjustDscList.GetItemText(i, 6);
				pAdjustInfo->MiddlePos.y = _ttoi(strTp);;
				strTp = m_AdjustDscList.GetItemText(i, 7);
				pAdjustInfo->LowPos.x = _ttoi(strTp);;
				strTp = m_AdjustDscList.GetItemText(i, 8);
				pAdjustInfo->LowPos.y = _ttoi(strTp);;
			}
		}
	}

	int nAvgCount = 0;
	CString strTemp;
	GetDlgItem(IDC_EDT_ADJUST_TARGETLENTH)->GetWindowText(strTemp);
	int nTargetLenth = _ttoi(strTemp);
	m_nTargetLenth = nTargetLenth;
	GetDlgItem(IDC_EDT_ADJUST_CAMERAZOOM)->GetWindowText(strTemp);
	m_nCameraZoom = _ttoi(strTemp);
	int nZoom = 0;
	if( m_bZoomUse )
		nZoom = m_nCameraZoom;
	else
		nZoom = 0;
	for(int i =0; i< m_pAdjustMgr.GetDscCount(); i++)
	{
		pAdjustInfo = m_pAdjustMgr.GetDscAt(i);
		if( m_pAdjustMgr.CalcAdjustData(pAdjustInfo, m_nTargetLenth, nZoom))
		{
			dTpPosX += pAdjustInfo->dAdjustX;
			dTpPosY += pAdjustInfo->dAdjustY;
			pAdjustInfo->nHeight = m_nAdjViewHeight;
			pAdjustInfo->nWidht = m_nAdjViewWidth;
			nAvgCount++;
		}
	}

	//wgkim 190527
	CESMImgMgr* pImgMgr = new CESMImgMgr;

	//중점 좌표의 평균
	dTpPosX = dTpPosX / nAvgCount;
	dTpPosY = dTpPosY / nAvgCount;
	CString strText;
	for(int i =0; i< m_pAdjustMgr.GetDscCount(); i++)
	{
		pAdjustInfo = m_pAdjustMgr.GetDscAt(i);
		if( pAdjustInfo->LowPos.x == 0 || pAdjustInfo->MiddlePos.x == 0 || pAdjustInfo->HighPos.x == 0)
			continue;

		pAdjustInfo->dAdjustX = dTpPosX - pAdjustInfo->dAdjustX;
		pAdjustInfo->dAdjustY = dTpPosY - pAdjustInfo->dAdjustY;


		//Adjust X,Y 
		strText.Format(_T("%.4lf"), pAdjustInfo->dAdjustX);
		m_AdjustDscList.SetItemText(i, 11, strText);
		strText.Format(_T("%.4lf"), pAdjustInfo->dAdjustY);
		m_AdjustDscList.SetItemText(i, 12, strText);
		//Angle
		strText.Format(_T("%.4lf"), pAdjustInfo->dAngle);
		m_AdjustDscList.SetItemText(i, 13, strText);

		//Rotate X,Y 
		strText.Format(_T("%.4lf"), pAdjustInfo->dRotateX);
		m_AdjustDscList.SetItemText(i, 14, strText);
		strText.Format(_T("%.4lf"), pAdjustInfo->dRotateY);
		m_AdjustDscList.SetItemText(i, 15, strText);

		//Scale
		strText.Format(_T("%.4lf"), pAdjustInfo->dScale);
		m_AdjustDscList.SetItemText(i, 9, strText);

		//Distance
		strText.Format(_T("%.4lf"), pAdjustInfo->dDistance);
		m_AdjustDscList.SetItemText(i, 16, strText);


		//wgkim 190520
		stAdjustInfo adjInfo;
		adjInfo.AdjAngle = pAdjustInfo->dAngle;
		adjInfo.AdjSize = pAdjustInfo->dScale;
		adjInfo.AdjptRotate.x = pAdjustInfo->dRotateX;
		adjInfo.AdjptRotate.y = pAdjustInfo->dRotateY;
		adjInfo.AdjMove.x = pAdjustInfo->dAdjustX;
		adjInfo.AdjMove.y = pAdjustInfo->dAdjustY;
		adjInfo.strDSC = pAdjustInfo->strDscName;
		adjInfo.nHeight = pAdjustInfo->nHeight;
		adjInfo.nWidth = pAdjustInfo->nWidht;

		//wgkim 190520
		pImgMgr->SetRectMargin(adjInfo);
	}

	for(int i =0; i< m_pAdjustMgr.GetDscCount(); i++)
	{
		pAdjustInfo = m_pAdjustMgr.GetDscAt(i);
		pAdjustInfo->rtMargin = pImgMgr->GetMinimumMargin();
	}

	delete pImgMgr;
}

void ESMAdjustMgrDlg::GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo)
{
	m_pAdjustMgr.GetDscInfo(pArrDscInfo);
}
void ESMAdjustMgrDlg::SetMovieState(int nMovieState )
{
	m_nMovieState = nMovieState;
}
void ESMAdjustMgrDlg::ClearDscInfo()
{
	m_AdjustDscList.DeleteAllItems();
	m_pAdjustMgr.DscClear();
}
void ESMAdjustMgrDlg::OnCustomdrawTcpList(NMHDR* pNMHDR, LRESULT* pResult)
{
	//LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
	NMLVCUSTOMDRAW* lplvcd = (NMLVCUSTOMDRAW*)pNMHDR;
	if(lplvcd->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if(lplvcd->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	{
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
	}
	else if(lplvcd->nmcd.dwDrawStage == (CDDS_ITEMPREPAINT | CDDS_SUBITEM))
	{
		if( lplvcd->nmcd.dwItemSpec == m_nSelectedNum)
		{
			if(m_nSelectPos * 2<=  lplvcd->iSubItem && m_nSelectPos * 2 + 1 >=  lplvcd->iSubItem)
			{
				lplvcd->clrTextBk = RGB(200, 200, 255);
				lplvcd->clrText = RGB(255, 0, 0);
			}			
			else
			{
				lplvcd->clrTextBk = RGB(255, 255, 255);
				lplvcd->clrText = RGB(0, 0, 0);
			}
		}

		//wgkim@esmlab.com 17-07-11
		if(lplvcd->iSubItem == 2)
		{
			CString strFocusValue = m_AdjustDscList.GetItemText(lplvcd->nmcd.dwItemSpec, 2);
			CString strThresholdFocusValue;
			strThresholdFocusValue.Format(_T("%.3f"), m_dThresholdFocus);
			CString strMinFocusValue;
			strMinFocusValue.Format(_T("%.3f"), m_pAdjustMgr.GetMinFocusValue());

			float focusValue = _ttof(strFocusValue);
			float thresholdFocusValue = _ttof(strThresholdFocusValue);
			float minFocusValue = _ttof(strMinFocusValue);

			if(thresholdFocusValue < focusValue)
				lplvcd->clrText = RGB(0, 0, 255);
			else if(thresholdFocusValue == focusValue)
				lplvcd->clrText = RGB(0, 255, 0);
			else
			{				
				lplvcd->clrText = RGB(255, 0, 0);
				if(minFocusValue == focusValue)
					lplvcd->clrTextBk = RGB(255, 180, 180);
			}
		}
		else
		{
			lplvcd->clrTextBk = RGB(255, 255, 255);
			lplvcd->clrText = RGB(0, 0, 0);
		}

		*pResult = CDRF_DODEFAULT;
	}
}
void ESMAdjustMgrDlg::OnCustomdrawTcpList2nd(NMHDR* pNMHDR, LRESULT* pResult)
{
	//LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	NMLVCUSTOMDRAW* lplvcd = (NMLVCUSTOMDRAW*)pNMHDR;
	if(lplvcd->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if(lplvcd->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	{
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
	}
	else if(lplvcd->nmcd.dwDrawStage == (CDDS_ITEMPREPAINT | CDDS_SUBITEM))
	{
		if( lplvcd->nmcd.dwItemSpec == m_nSelectedNum)
		{
			if(m_nSelectPos * 2<=  lplvcd->iSubItem && m_nSelectPos * 2 + 1 >=  lplvcd->iSubItem)
			{
				lplvcd->clrTextBk = RGB(200, 200, 255);
				lplvcd->clrText = RGB(255, 0, 0);
			}
			else
			{
				lplvcd->clrTextBk = RGB(255, 255, 255);
				lplvcd->clrText = RGB(0, 0, 0);
			}
		}
		*pResult = CDRF_DODEFAULT;
	}
}
void ESMAdjustMgrDlg::OnBnClickedBtnAdjustpointload()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	// 	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
	// 		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	// 	else
	// 		strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_CONF));

	CString szFilter = _T("Point List (*.ptl)|*.ptl|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Load");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();

		LoadPointData(strFileName);
	}
}
void ESMAdjustMgrDlg::OnBnClickedBtnAdjustpointsave()
{
	CString strAdjustFolder;
	strAdjustFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));
	CString szFilter = _T("Point List (*.ptl)|*.ptl|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  	
	//strAdjustFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = _T("Point Save");
	dlg.m_ofn.lpstrInitialDir = strAdjustFolder;	

	CString strFileName;
	if( dlg.DoModal() == IDOK )
	{
		strFileName = dlg.GetPathName();	
		if( strFileName.Right(4) != _T(".ptl"))
			strFileName = strFileName + _T(".ptl");
		SavePointData(strFileName);
	}
}
void ESMAdjustMgrDlg::SavePointData(CString strFileName)
{
	CESMIni ini;
	CFile file;
	file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite);
	file.Close();

	if(!ini.SetIniFilename (strFileName))
		return;

	ini.WriteInt(_T("MovieSize"), _T("HEIGHT"), m_nAdjViewHeight);
	ini.WriteInt(_T("MovieSize"), _T("WIDTH"), m_nAdjViewWidth);

	if(m_nView == FIRST_ADJUST_VIEW)
	{
		for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
		{
			CString strSelect = m_AdjustDscList.GetItemText(i, 1);
			ini.WriteString(strSelect, _T("Pos1X"), m_AdjustDscList.GetItemText(i, 3));
			ini.WriteString(strSelect, _T("Pos1Y"), m_AdjustDscList.GetItemText(i, 4));
			ini.WriteString(strSelect, _T("Pos2X"), m_AdjustDscList.GetItemText(i, 5));
			ini.WriteString(strSelect, _T("Pos2Y"), m_AdjustDscList.GetItemText(i, 6));
			ini.WriteString(strSelect, _T("Pos3X"), m_AdjustDscList.GetItemText(i, 7));
			ini.WriteString(strSelect, _T("Pos3Y"), m_AdjustDscList.GetItemText(i, 8));
		}
	}
	else
	{
		for(int i = 0; i < m_AdjustDsc2ndList.GetItemCount(); i++)
		{
			CString strSelect = m_AdjustDsc2ndList.GetItemText(i, 1);
			ini.WriteString(strSelect, _T("Pos1X"), m_AdjustDsc2ndList.GetItemText(i, 2));
			ini.WriteString(strSelect, _T("Pos1Y"), m_AdjustDsc2ndList.GetItemText(i, 3));
			ini.WriteString(strSelect, _T("Pos2X"), m_AdjustDsc2ndList.GetItemText(i, 4));
			ini.WriteString(strSelect, _T("Pos2Y"), m_AdjustDsc2ndList.GetItemText(i, 5));
			ini.WriteString(strSelect, _T("Pos3X"), m_AdjustDsc2ndList.GetItemText(i, 6));
			ini.WriteString(strSelect, _T("Pos3Y"), m_AdjustDsc2ndList.GetItemText(i, 7));
			ini.WriteString(strSelect, _T("Pos4X"), m_AdjustDsc2ndList.GetItemText(i, 8));
			ini.WriteString(strSelect, _T("Pos4Y"), m_AdjustDsc2ndList.GetItemText(i, 9));
			ini.WriteString(strSelect, _T("Pos5X"), m_AdjustDsc2ndList.GetItemText(i, 10));
			ini.WriteString(strSelect, _T("Pos5Y"), m_AdjustDsc2ndList.GetItemText(i, 11));
			ini.WriteString(strSelect, _T("Pos6X"), m_AdjustDsc2ndList.GetItemText(i, 12));
			ini.WriteString(strSelect, _T("Pos6Y"), m_AdjustDsc2ndList.GetItemText(i, 13));
			ini.WriteString(strSelect, _T("Pos7X"), m_AdjustDsc2ndList.GetItemText(i, 14));
			ini.WriteString(strSelect, _T("Pos7Y"), m_AdjustDsc2ndList.GetItemText(i, 15));
			ini.WriteString(strSelect, _T("Pos8X"), m_AdjustDsc2ndList.GetItemText(i, 16));
			ini.WriteString(strSelect, _T("Pos8Y"), m_AdjustDsc2ndList.GetItemText(i, 17));
			ini.WriteString(strSelect, _T("Pos9X"), m_AdjustDsc2ndList.GetItemText(i, 18));
			ini.WriteString(strSelect, _T("Pos9Y"), m_AdjustDsc2ndList.GetItemText(i, 19));
			ini.WriteString(strSelect, _T("Pos10X"), m_AdjustDsc2ndList.GetItemText(i, 20));
			ini.WriteString(strSelect, _T("Pos10Y"), m_AdjustDsc2ndList.GetItemText(i, 21));
		}		
	}
}
void ESMAdjustMgrDlg::LoadPointData(CString strFileName)
{
	CESMIni ini;
	if(!ini.SetIniFilename (strFileName))
		return;

	int nHeight = 0, nWidth = 0;
	nHeight = ini.GetInt(_T("MovieSize"), _T("HEIGHT"), 0);
	nWidth = ini.GetInt(_T("MovieSize"), _T("WIDTH"), 0);
	if(nHeight != 0)
		m_nAdjViewHeight = nHeight;

	if(nWidth != 0)
		m_nAdjViewWidth = nWidth;

	CString strData;

	if(m_nView == FIRST_ADJUST_VIEW)
	{
		for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
		{
			CString strSelect = m_AdjustDscList.GetItemText(i, 1);
			strData = ini.GetString(strSelect, _T("Pos1X"));
			m_AdjustDscList.SetItemText(i, 3, strData);
			strData = ini.GetString(strSelect, _T("Pos1Y"));
			m_AdjustDscList.SetItemText(i, 4, strData);
			strData = ini.GetString(strSelect, _T("Pos2X"));
			m_AdjustDscList.SetItemText(i, 5, strData);
			strData = ini.GetString(strSelect, _T("Pos2Y"));
			m_AdjustDscList.SetItemText(i, 6, strData);
			strData = ini.GetString(strSelect, _T("Pos3X"));
			m_AdjustDscList.SetItemText(i, 7, strData);
			strData = ini.GetString(strSelect, _T("Pos3Y"));
			m_AdjustDscList.SetItemText(i, 8, strData);
		}
	}
	else
	{
		for(int i = 0; i < m_AdjustDsc2ndList.GetItemCount(); i++)
		{
			CString strSelect = m_AdjustDsc2ndList.GetItemText(i, 1);
			strData = ini.GetString(strSelect, _T("Pos1X"));
			m_AdjustDsc2ndList.SetItemText(i, 2, strData);
			strData = ini.GetString(strSelect, _T("Pos1Y"));
			m_AdjustDsc2ndList.SetItemText(i, 3, strData);
			strData = ini.GetString(strSelect, _T("Pos2X"));
			m_AdjustDsc2ndList.SetItemText(i, 4, strData);
			strData = ini.GetString(strSelect, _T("Pos2Y"));
			m_AdjustDsc2ndList.SetItemText(i, 5, strData);
			strData = ini.GetString(strSelect, _T("Pos3X"));
			m_AdjustDsc2ndList.SetItemText(i, 6, strData);
			strData = ini.GetString(strSelect, _T("Pos3Y"));
			m_AdjustDsc2ndList.SetItemText(i, 7, strData);
			strData = ini.GetString(strSelect, _T("Pos4X"));
			m_AdjustDsc2ndList.SetItemText(i, 8, strData);
			strData = ini.GetString(strSelect, _T("Pos4Y"));
			m_AdjustDsc2ndList.SetItemText(i, 9, strData);

			strData = ini.GetString(strSelect, _T("Pos5X"));
			m_AdjustDsc2ndList.SetItemText(i, 10, strData);
			strData = ini.GetString(strSelect, _T("Pos5Y"));
			m_AdjustDsc2ndList.SetItemText(i, 11, strData);
			strData = ini.GetString(strSelect, _T("Pos6X"));
			m_AdjustDsc2ndList.SetItemText(i, 12, strData);
			strData = ini.GetString(strSelect, _T("Pos6Y"));
			m_AdjustDsc2ndList.SetItemText(i, 13, strData);
			strData = ini.GetString(strSelect, _T("Pos7X"));
			m_AdjustDsc2ndList.SetItemText(i, 14, strData);
			strData = ini.GetString(strSelect, _T("Pos7Y"));
			m_AdjustDsc2ndList.SetItemText(i, 15, strData);
			strData = ini.GetString(strSelect, _T("Pos8X"));
			m_AdjustDsc2ndList.SetItemText(i, 16, strData);
			strData = ini.GetString(strSelect, _T("Pos8Y"));
			m_AdjustDsc2ndList.SetItemText(i, 17, strData);

			strData = ini.GetString(strSelect, _T("Pos9X"));
			m_AdjustDsc2ndList.SetItemText(i, 18, strData);
			strData = ini.GetString(strSelect, _T("Pos9Y"));
			m_AdjustDsc2ndList.SetItemText(i, 19, strData);
			strData = ini.GetString(strSelect, _T("Pos10X"));
			m_AdjustDsc2ndList.SetItemText(i, 20, strData);
			strData = ini.GetString(strSelect, _T("Pos10Y"));
			m_AdjustDsc2ndList.SetItemText(i, 21, strData);
		}


	}
}
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-08-30
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void ESMAdjustMgrDlg::FindExactPosition(int& nX, int& nY)
{
	//-- 2014-08-30 hongsu@esmlab.com
	//-- Load Image 
	ESMAdjustImage* pAdjustImg;
	switch(m_nImgView)
	{
	case ADJUST_IMAGE:
		pAdjustImg = &m_StAdjustImage;
		break;
	case ORIGINAL_IMAGE:
		pAdjustImg = &m_StAdjustImageOrigin;
		break;
	case AFTER_IMAGE:
		pAdjustImg = &m_StAdjustImageAfter;
		break;
	case RE_IMAGE:
		pAdjustImg = &m_StAdjustImageRe;
		break;	
	}


	UpdateData();
	//-- 2016-11-01 wgkim@esmlab.com
	//임시수정
	//pAdjustImg->FindExactPosition(nX, nY, m_nThreshold, m_bAutoFind);
	pAdjustImg->FindExactPosition(nX, nY, m_nThreshold, FALSE);	
}
void ESMAdjustMgrDlg::OnTimer(UINT_PTR nIDEvent) 
{
	switch(nIDEvent)
	{
	case TIMER_CHANGE_NEXT_ITEM:
		{
			//-- 2014-08-31 hongsu@esmlab.com
			//-- Change Next Item
			ESMLog(5,_T("Change Next Item"));

			int nSelectedItem = m_AdjustDscList.GetNextItem( -1, LVNI_SELECTED );
			m_AdjustDscList.SetItemState(nSelectedItem+1,LVIS_SELECTED,  LVIS_SELECTED);
			m_AdjustDscList.SetItemState(nSelectedItem, 0 ,  LVIS_SELECTED);
			m_AdjustDscList.SetFocus();
			m_nSelectPos = 0;

			KillTimer(TIMER_CHANGE_NEXT_ITEM);
		}
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}
void ESMAdjustMgrDlg::OnEnKillfocusEdtLine2()
{
	DrawImage();
}
void ESMAdjustMgrDlg::OnEnKillfocusEdtLine1()
{
	DrawImage();
}
void ESMAdjustMgrDlg::OnBnClickedBtnCalcdistance()
{
	UpdateData(TRUE);
	// 선택 Point 의 Distance 계산.
	for(int nDscIndex =1; nDscIndex< m_pAdjustMgr.GetDscCount(); nDscIndex++)
		m_pAdjustMgr.GetDscAt(nDscIndex)->dDistance = 0.0;

	if(m_bDistanceUse)
		CalcSelectedDistance();

	CString strDsc;
	double dTargetDistance = 0.0, dFirstDist = 0.0, dSecondDist = 0.0, nGabDist = 0.0;
	int nAvgCount = 0, nfirstIndex = 0, nSecondIndex = 0;
	if( m_pAdjustMgr.GetDscCount() > 0)
	{
		if(m_bDistanceUse)
			dFirstDist = m_pAdjustMgr.GetDscAt(0)->dDistance;
		else
			dFirstDist = m_pAdjustMgr.GetDistanceData(m_pAdjustMgr.GetDscAt(0)->strDscName);
	}

	for(int nDscIndex =1; nDscIndex< m_pAdjustMgr.GetDscCount(); nDscIndex++)
	{
		strDsc = m_pAdjustMgr.GetDscAt(nDscIndex)->strDscName;
		if(m_bDistanceUse)
			dTargetDistance = m_pAdjustMgr.GetDscAt(nDscIndex)->dDistance;
		else
			dTargetDistance = m_pAdjustMgr.GetDistanceData(strDsc);

		if( dTargetDistance != 0.0)
		{
			dSecondDist = dTargetDistance;
			nSecondIndex = nDscIndex;
			nGabDist = dSecondDist - dFirstDist;
			nGabDist = nGabDist / ( nSecondIndex - nfirstIndex);
			dTargetDistance = dFirstDist + nGabDist;
			for( int nInsertIndex = nfirstIndex + 1; nInsertIndex < nSecondIndex; nInsertIndex++)
			{
				m_pAdjustMgr.GetDscAt(nInsertIndex)->dDistance = dTargetDistance;
				dTargetDistance += nGabDist;
			}
			m_pAdjustMgr.GetDscAt(nfirstIndex)->dDistance = dFirstDist;
			m_pAdjustMgr.GetDscAt(nSecondIndex)->dDistance = dSecondDist;
			nfirstIndex = nDscIndex;
			dFirstDist = dTargetDistance;
		}
	}
}
void ESMAdjustMgrDlg::CalcSelectedDistance()
{
	UpdateData(TRUE);
	double dTpPosX = 0.0, dTpPosY = 0.0;
	DscAdjustInfo* pAdjustInfo;
	CString strCtrlName, strTp, strTp1;
	for( int i =0 ;i < m_AdjustDscList.GetItemCount(); i++ )
	{
		strCtrlName = m_AdjustDscList.GetItemText(i, 1);
		for( int j =0; j< m_pAdjustMgr.GetDscCount(); j++)
		{
			pAdjustInfo = m_pAdjustMgr.GetDscAt(i);
			if( pAdjustInfo->strDscName == strCtrlName)
			{
				strTp = m_AdjustDscList.GetItemText(i, 3);
				pAdjustInfo->HighPos.x = _ttoi(strTp);
				strTp = m_AdjustDscList.GetItemText(i, 4);
				pAdjustInfo->HighPos.y = _ttoi(strTp);;
				strTp = m_AdjustDscList.GetItemText(i, 5);
				pAdjustInfo->MiddlePos.x = _ttoi(strTp);;
				strTp = m_AdjustDscList.GetItemText(i, 6);
				pAdjustInfo->MiddlePos.y = _ttoi(strTp);;
				strTp = m_AdjustDscList.GetItemText(i, 7);
				pAdjustInfo->LowPos.x = _ttoi(strTp);;
				strTp = m_AdjustDscList.GetItemText(i, 8);
				pAdjustInfo->LowPos.y = _ttoi(strTp);;
			}
		}
	}

	int nAvgCount = 0;
	CString strTemp;
	GetDlgItem(IDC_EDT_ADJUST_TARGETLENTH)->GetWindowText(strTemp);
	int nTargetLenth = _ttoi(strTemp);
	m_nTargetLenth = nTargetLenth;
	GetDlgItem(IDC_EDT_ADJUST_CAMERAZOOM)->GetWindowText(strTemp);
	m_nCameraZoom = _ttoi(strTemp);

	for( int j =0 ;j < m_AdjustDscList.GetItemCount(); j++ )
	{
		strTp = m_AdjustDscList.GetItemText(j, 10);
		strCtrlName = m_AdjustDscList.GetItemText(j, 1);
		if( strTp == _T("0"))
		{
			for(int i =0; i< m_pAdjustMgr.GetDscCount(); i++)
			{
				pAdjustInfo = m_pAdjustMgr.GetDscAt(i);

				if( strCtrlName == pAdjustInfo->strDscName)
				{
					//Size
					double dTargetDistance = 0;
					double dTargetLength = 0;

					int nZoom = 0;
					if( m_bZoomUse )
						nZoom = m_nCameraZoom;
					else
						nZoom = m_pAdjustMgr.GetZoomData(pAdjustInfo->strDscName);


					int nPixelDefaultSize = 0;
					if( pAdjustInfo->nWidht == 1920)
						nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;
					else if( pAdjustInfo->nWidht == 5472)
						nPixelDefaultSize = DEFAULT_PIXEL5472_DISTANCE;
					else if( pAdjustInfo->nWidht == 2200 && pAdjustInfo->nHeight == 1238)
						nPixelDefaultSize = DEFAULT_PIXEL5472_3080_DISTANCE;
					else if( pAdjustInfo->nWidht == 3840 && pAdjustInfo->nHeight == 2160)
						nPixelDefaultSize = DEFAULT_PIXEL3840_2160_DISTANCE;
					else
						nPixelDefaultSize = DEFAULT_PIXEL1920_DISTANCE;

					int nHeight = pAdjustInfo->LowPos.y - pAdjustInfo->HighPos.y;
					int nWidth = abs(pAdjustInfo->LowPos.x - pAdjustInfo->HighPos.x);
					if( nHeight != 0 || nWidth != 0 )
					{
						double dTpResize = 0.0;
						if( nWidth != 0)
							dTpResize = sqrt(double(nHeight * nHeight + nWidth * nWidth));
						else
							dTpResize = nHeight;

						dTargetDistance = m_nTargetLenth * nPixelDefaultSize / DEFAULT_TARGETLENGTH * nZoom /DEFAULT_CAMERAZOOM / dTpResize;
						int a = 0;
						a++;
					}
					strTp1.Format(_T("%.4lf"), dTargetDistance);
					pAdjustInfo->dDistance = dTargetDistance;
					m_AdjustDscList.SetItemText(j, 15, strTp1);
				}
			}
		}
	}
}
void ESMAdjustMgrDlg::OnBnClickedBtnDistanceAdd()
{
	POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();
	int nItem = m_AdjustDscList.GetNextSelectedItem(pos);

	m_AdjustDscList.SetItemText(nItem, 10, _T("0"));
}
void ESMAdjustMgrDlg::OnBnClickedBtnDistanceDel()
{
	POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();
	int nItem = m_AdjustDscList.GetNextSelectedItem(pos);

	m_AdjustDscList.SetItemText(nItem, 10, _T(""));
}
void ESMAdjustMgrDlg::OnBnClickedBtnDistanceReset()
{
	for( int i =0 ;i < m_AdjustDscList.GetItemCount(); i++ )
	{
		m_AdjustDscList.SetItemText(i, 10, _T(""));
	}
}
BOOL ESMAdjustMgrDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
		if(pMsg->message == WM_KEYDOWN)
		{
			/*if(m_ImageLoaded == 1)
			{
				switch(pMsg->wParam)
				{
					if(m_nView == THIRD_ADJUST_VIEW)
					{
				case VK_NUMPAD7:
					{
						m_adjinfo.at(m_nSelectedNum).AdjAngle -= 0.5;

						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
						if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
						{
							delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
							m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
						}

						Showimage();
						Invalidate();
						break;
					}
				case VK_NUMPAD9:
					{
						m_adjinfo.at(m_nSelectedNum).AdjAngle += 0.5;

						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
						if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
						{
							delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
							m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
						}

						Showimage();
						Invalidate();				
						break;
					}
				case VK_NUMPAD8:
					{
						m_adjinfo.at(m_nSelectedNum).AdjMove.y -= 1;

						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
						if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
						{
							delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
							m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
						}

						Showimage();
						Invalidate();
						break;
					}
				case VK_NUMPAD5:
					{
						m_adjinfo.at(m_nSelectedNum).AdjMove.y += 1;

						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
						if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
						{
							delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
							m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
						}

						Showimage();
						Invalidate();
						break;
					}
				case VK_NUMPAD4:
					{
						m_adjinfo.at(m_nSelectedNum).AdjMove.x -= 1;

						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
						if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
						{
							delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
							m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
						}

						Showimage();
						Invalidate();
						break;
					}
				case VK_NUMPAD6:
					{
						m_adjinfo.at(m_nSelectedNum).AdjMove.x += 1;

						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bAdjust = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bRotate = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bMove = FALSE;
						m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->bImgCut = FALSE;
						if(m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits != NULL)
						{
							delete m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits;
							m_pAdjustMgrRe.GetDscAt(m_nSelectedNum)->pBmpBits = NULL;
						}

						Showimage();
						Invalidate();
						break;
					}
					}
				}
			}
			else*/
			{
				switch(pMsg->wParam)
				{
				case VK_DELETE://170615 - added by hjcho
					{
						DeleteSelectedList();
					}
					break;
				}
			}
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}
void ESMAdjustMgrDlg::OnBnClickedBtnCommit()
{
	//CString indexstr;
	//m_edit_frameIndex.GetWindowTextW(indexstr);
	//m_frameIndex = _ttoi(indexstr);
	//m_bThreadLoad = FALSE;
	//LoadData();
}
void ESMAdjustMgrDlg::OnBnClickedBtnAdjustPointReset()
{
	CESMFileOperation fo;
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), ADJUSTTEMPFILE);
	fo.Delete(strFileName);
	CString strData = _T("");

	if(m_nView == FIRST_ADJUST_VIEW)
	{
		for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
		{
			CString strSelect = m_AdjustDscList.GetItemText(i, 1);
			m_AdjustDscList.SetItemText(i, 3, strData);
			m_AdjustDscList.SetItemText(i, 4, strData);
			m_AdjustDscList.SetItemText(i, 5, strData);
			m_AdjustDscList.SetItemText(i, 6, strData);
			m_AdjustDscList.SetItemText(i, 7, strData);
			m_AdjustDscList.SetItemText(i, 8, strData);

		}
	}
	else
	{
		for(int i = 0; i < m_AdjustDsc2ndList.GetItemCount(); i++)
		{
			CString strSelect = m_AdjustDsc2ndList.GetItemText(i, 1);
			m_AdjustDsc2ndList.SetItemText(i, 2, strData);
			m_AdjustDsc2ndList.SetItemText(i, 3, strData);
			m_AdjustDsc2ndList.SetItemText(i, 4, strData);
			m_AdjustDsc2ndList.SetItemText(i, 5, strData);
			m_AdjustDsc2ndList.SetItemText(i, 6, strData);
			m_AdjustDsc2ndList.SetItemText(i, 7, strData);

		}
	}

}
void ESMAdjustMgrDlg::Matrix_Image(Mat a1,Mat a2,Mat result)
{
	int i,j;

	int row = a1.rows;
	int col = a1.cols;
	//Image segmentation
	int h=0,w=0;
	int col_lim = col/20;
	int row_lim = row/10;
	int w_state,h_state;
	w_state = 1;
	h_state = 1;

	for(i=0;i<row;i++)
	{
		for(j=0;j<col;j++)
		{	
			if(w_state == 1 && h_state == 1)
			{
				result.data[col*(i*3) + (j*3)] = a1.data[col*(i*3) + (j*3)];
				result.data[col*(i*3) + (j*3)+1] = a1.data[col*(i*3) + (j*3)+1];
				result.data[col*(i*3) + (j*3)+2] = a1.data[col*(i*3) + (j*3)+2];
			}
			else if(w_state == 2 && h_state == 2)
			{
				result.data[col*(i*3) + (j*3)] = a1.data[col*(i*3) + (j*3)];
				result.data[col*(i*3) + (j*3)+1] = a1.data[col*(i*3) + (j*3)+1];
				result.data[col*(i*3) + (j*3)+2] = a1.data[col*(i*3) + (j*3)+2];
			}
			else if(w_state == 2 && h_state == 1)
			{
				result.data[col*(i*3) + (j*3)] = a2.data[col*(i*3) + (j*3)];
				result.data[col*(i*3) + (j*3)+1] = a2.data[col*(i*3) + (j*3)+1];
				result.data[col*(i*3) + (j*3)+2] = a2.data[col*(i*3) + (j*3)+2];
			}
			else if(w_state == 1 && h_state == 2)
			{
				result.data[col*(i*3) + (j*3)] = a2.data[col*(i*3) + (j*3)];
				result.data[col*(i*3) + (j*3)+1] = a2.data[col*(i*3) + (j*3)+1];
				result.data[col*(i*3) + (j*3)+2] = a2.data[col*(i*3) + (j*3)+2];
			}
			if(w == col_lim && w_state == 1) 
			{
				w_state = 2;
				w = 0;
			}
			else if(w == col_lim && w_state == 2) 
			{
				w_state = 1;
				w = 0;
			}
			w++;
		}
		if(h == row_lim && h_state == 1)
		{
			h_state = 2;
			h = 0;
		}
		else if(h == row_lim && h_state == 2)
		{
			h_state = 1;
			h = 0;
		}
		h++;
	}
	//Image lining
	cv::Point str,end;
	for(i=0;i<row;i+=row_lim)
	{
		str = cv::Point(0,i);
		end = cv::Point(col,i);
		line(result,str,end,Scalar(255,0,0),2,8);
	}
	for(i=0;i<col;i+=col_lim)
	{
		str = cv::Point(i,0);
		end = cv::Point(i,row);
		line(result,str,end,Scalar(255,0,0),2,8);
	}
}

//jhhan
//void ESMAdjustMgrDlg::OnSize(UINT nType, int cx, int cy) 
//{
//	//CDialog::OnSize(nType, cx, cy);
//
//	//int nScrollMax	= 0;
//
//	//m_nCurHeight	= cy;
//	////	V-Scroll
//	//if (cy < m_rcRect.Height())
//	//	nScrollMax = m_rcRect.Height() - cy;
//	//else
//	//	nScrollMax = 0;
//
//	//SCROLLINFO siVScroll;
//	//siVScroll.cbSize= sizeof(SCROLLINFO);
//	//siVScroll.fMask	= SIF_ALL; // SIF_ALL = SIF_PAGE | SIF_RANGE | SIF_POS;
//	//siVScroll.nMin	= 0;
//	//siVScroll.nMax	= nScrollMax;
//	//siVScroll.nPage	= siVScroll.nMax /10;
//	//siVScroll.nPos	= 0;
//	//SetScrollInfo(SB_VERT, &siVScroll, TRUE); 
//
//
//	////	H-Scroll
//	//m_nCurWidth		= cx;
//	//if (cx < m_rcRect.Width())
//	//	nScrollMax = m_rcRect.Width() - cx;
//	//else
//	//	nScrollMax = 0;
//
//	//SCROLLINFO siHScroll;
//	//siHScroll.cbSize= sizeof(SCROLLINFO);
//	//siHScroll.fMask	= SIF_ALL; // SIF_ALL = SIF_PAGE | SIF_RANGE | SIF_POS;
//	//siHScroll.nMin	= 0;
//	//siHScroll.nMax	= nScrollMax;
//	//siHScroll.nPage	= siHScroll.nMax /10;
//	//siHScroll.nPos	= 0;
//	//SetScrollInfo(SB_HORZ, &siHScroll, TRUE);
//} 
//void ESMAdjustMgrDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
//{
//	/*int nDelta;
//	int nMaxPos = m_rcRect.Width() - m_nCurWidth;
//
//	switch (nSBCode)
//	{
//	case SB_LINERIGHT:
//		if (m_nHScrollPos >= nMaxPos)
//			return;
//		nDelta = min(nMaxPos /100, nMaxPos - m_nHScrollPos);
//		break;
//	case SB_LINELEFT:
//		if (m_nHScrollPos <= 0)
//			return;
//		nDelta = -min(nMaxPos /100, m_nHScrollPos);
//		break;
//	case SB_PAGERIGHT:
//		if (m_nHScrollPos >= nMaxPos)
//			return;
//		nDelta = min(nMaxPos /10, nMaxPos - m_nHScrollPos);
//		break;
//	case SB_THUMBPOSITION:
//		nDelta = (int)nPos - m_nHScrollPos;
//		break;
//	case SB_PAGELEFT:
//		if (m_nHScrollPos <= 0)
//			return;
//		nDelta = -min(nMaxPos /10, m_nHScrollPos);
//		break;
//	default:
//		return;
//	}
//
//	m_nHScrollPos += nDelta;
//	SetScrollPos(SB_HORZ, m_nHScrollPos, TRUE);
//	ScrollWindow(-nDelta, 0);*/
//
//	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
//}
//void ESMAdjustMgrDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
//{
//	/*int nDelta;
//	int nMaxPos = m_rcRect.Height() - m_nCurHeight;
//
//	switch (nSBCode)
//	{
//	case SB_LINEDOWN:
//		if (m_nVScrollPos >= nMaxPos)
//			return;
//		nDelta = min(nMaxPos /100, nMaxPos - m_nVScrollPos);
//		break;
//	case SB_LINEUP:
//		if (m_nVScrollPos <= 0)
//			return;
//		nDelta = -min(nMaxPos /100, m_nVScrollPos);
//		break;
//	case SB_PAGEDOWN:
//		if (m_nVScrollPos >= nMaxPos)
//			return;
//		nDelta = min(nMaxPos /10, nMaxPos - m_nVScrollPos);
//		break;
//	case SB_THUMBPOSITION:
//		nDelta = (int)nPos - m_nVScrollPos;
//		break;
//	case SB_PAGEUP:
//		if (m_nVScrollPos <= 0)
//			return;
//		nDelta = -min(nMaxPos /10, m_nVScrollPos);
//		break;
//
//	default:
//		return;
//	}
//
//	m_nVScrollPos += nDelta;
//	SetScrollPos(SB_VERT, m_nVScrollPos, TRUE);
//	ScrollWindow(0, -nDelta);*/
//
//	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
//}

//-- 2016-11-01 wgkim@esmlab.com
void ESMAdjustMgrDlg::OnBnClickedBtnAutoFind()
{
	if(((CButton*)GetDlgItem(IDC_CHECK_AUTOFIND))->GetCheck())
	{
		GetDlgItem(IDC_CHECK_AUTOMOVE)->EnableWindow(FALSE);
		((CButton*)GetDlgItem(IDC_CHECK_AUTOMOVE))->SetCheck(0);
	}
	else
	{
		GetDlgItem(IDC_CHECK_AUTOMOVE)->EnableWindow(TRUE);
	}
}
void ESMAdjustMgrDlg::DeleteSelectedList()
{
	int nSelectedCount = m_AdjustDscList.GetSelectedCount();

	if(nSelectedCount == 1)
	{
		POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();
		int nDeleteItem = m_AdjustDscList.GetNextSelectedItem(pos);

		DeleteSelectItem(nDeleteItem);
	}
	else
	{
		POSITION pos = m_AdjustDscList.GetFirstSelectedItemPosition();

		while(pos)
		{			
			int nDeleteItem = m_AdjustDscList.GetNextSelectedItem(pos);
			DeleteSelectItem(nDeleteItem);
		}
	}
}
void ESMAdjustMgrDlg::DeleteSelectItem(int nIndex)
{
	CString strFileName;
	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), ADJUSTTEMPFILE);

	//선택 아이템 삭제 ( 2,3,4,5,6,7,8 )
	for(int i = 2; i < 9 ; i++)
	{
		m_AdjustDscList.SetItemText(nIndex,i,NULL);
	}
	SavePointData(strFileName);
}
void ESMAdjustMgrDlg::SaveAdjust()
{
	CString strTemp;
	GetDlgItem(IDC_EDT_ADJUST_CAMERAZOOM)->GetWindowText(strTemp);
	int nTargetZoom = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_CAMERAZOOM, nTargetZoom);

	GetDlgItem(IDC_EDT_ADJUST_TARGETLENTH)->GetWindowText(strTemp);
	int nTargetLenth = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_TARGETLENTH, nTargetLenth);

	GetDlgItem(IDC_EDT_ADJUSTTHRESHOLD)->GetWindowText(strTemp);
	int nThresdhold = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_TH, nThresdhold);

	GetDlgItem(IDC_EDT_ADJUST_POINTGAPMIN)->GetWindowText(strTemp);
	int nPointGapMin = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_POINTGAPMIN, nPointGapMin);

	GetDlgItem(IDC_EDT_ADJUST_MIN_WIDTH)->GetWindowText(strTemp);
	int nMinWidth = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_MIN_WIDTH, nMinWidth);

	GetDlgItem(IDC_EDT_ADJUST_MIN_HEIGHT)->GetWindowText(strTemp);
	int nMinHeight = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_MIN_HEIGHT, nMinHeight);

	GetDlgItem(IDC_EDT_ADJUST_MAX_WIDTH)->GetWindowText(strTemp);
	int nMaxWidth = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_MAX_WIDTH, nMaxWidth);

	GetDlgItem(IDC_EDT_ADJUST_MAX_HEIGHT)->GetWindowText(strTemp);
	int nMaxHeight = _ttoi(strTemp);
	ESMSetValue(ESM_VALUE_ADJ_MAX_HEIGHT, nMaxHeight);

	CESMIni ini;
	CString strInfoConfig;
	strInfoConfig.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONFIG);

	if(!ini.SetIniFilename (strInfoConfig))
	{
		CString strLog;
		strLog.Format(_T("File Not Exist %s"), strInfoConfig);
		AfxMessageBox(strLog );
		return;
	}
	ini.WriteInt	(INFO_SECTION_MOVIE,	INFO_ADJ_TH,					nThresdhold);
	ini.WriteInt	(INFO_SECTION_MOVIE,	INFO_ADJ_POINTGAPMIN,			nPointGapMin);
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MIN_WIDTH,				nMinWidth);	
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MIN_HEIGHT,			nMinHeight);	
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MAX_WIDTH,				nMaxWidth);	
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MAX_HEIGHT,			nMaxHeight);
}

void ESMAdjustMgrDlg::OnBnClickedSaveAdjust()
{
	SaveAdjust();

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_MOVIE_SAVE_ADJUST_RUN;
	pMsg->pParam = (LPARAM)this;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);

	m_bAdjSave = TRUE;
#if 0
	int nMarginX = 0;
	int nMarginY = 0;
	vector<stAdjustInfo>stArrAdjInfo;
	vector<DscAdjustInfo*>* pArrDscInfo = NULL;

	GetDscInfo(&pArrDscInfo);
	CESMImgMgr* pImgMgr = new CESMImgMgr();

	int nFalseCnt = 0;
	for(int i = 0 ; i < pArrDscInfo->size(); i++)
	{
		DscAdjustInfo* pAdjustinfo = pArrDscInfo->at(i);
		stAdjustInfo adjust;
		adjust.AdjAngle		 = pAdjustinfo->dAngle;
		adjust.AdjSize		 = pAdjustinfo->dScale;
		adjust.AdjptRotate.x = pAdjustinfo->dRotateX;
		adjust.AdjptRotate.y = pAdjustinfo->dRotateY;
		adjust.AdjMove.x	 = pAdjustinfo->dAdjustX;
		adjust.AdjMove.y	 = pAdjustinfo->dAdjustY;		
		adjust.strDSC		 = pAdjustinfo->strDscName;
		adjust.nHeight		 = pAdjustinfo->nHeight;
		adjust.nWidth		 = pAdjustinfo->nWidht;

		stArrAdjInfo.push_back(adjust);
		pImgMgr->SetMargin(nMarginX,nMarginY,adjust);

		if(adjust.AdjAngle == 0.0)
			nFalseCnt ++;
	}
	if(nFalseCnt == pArrDscInfo->size())
	{
		AfxMessageBox(_T("Create Adjust Data!!"));
		return;
	}
	else
	{
		ESMEvent* pMsg = new ESMEvent();
		pMsg->message = WM_ESM_MOVIE_SAVE_ADJUST;
		pMsg->pParam  = (LPARAM)&stArrAdjInfo;//(LPARAM)pArrDscInfo;
		pMsg->nParam1 = nMarginX;
		pMsg->nParam2 = nMarginY;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	}
#endif
}

//wgkim 17-06-30
void ESMAdjustMgrDlg::OnBnClickedBtnFocus()
{
	////
	//CESMIni ini;
	//CString strFileName;
	//strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), _T("Focus.ini"));	
	//if(!ini.SetIniFilename (strFileName))
	//	return;

	//int nFocusData;	
	//nFocusData = ini.GetInt(_T("Focus"), _T("Threshold Value"), 0);

	//m_dThresholdFocus = nFocusData/1000.;	

	//m_pAdjustMgr.CalcThresholdFocusValue();	

	if(m_nSelectedNum < 0)
	{
		AfxMessageBox(_T("기준을 잡을 카메라를 선택하세요."));
		return;
	}
	
	int nPosX = _ttoi(m_AdjustDscList.GetItemText(m_nSelectedNum, 7));
	int nPosY = _ttoi(m_AdjustDscList.GetItemText(m_nSelectedNum, 8));	
	
	m_dThresholdFocus = m_pAdjustMgr.FocusValue(m_pAdjustMgr.GetDscAt(m_nSelectedNum), nPosX, nPosY);
	
	for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
	{
		nPosX = _ttoi(m_AdjustDscList.GetItemText(i, 7));
		nPosY = _ttoi(m_AdjustDscList.GetItemText(i, 8));

		DscAdjustInfo* pAdjustInfo = NULL;
		pAdjustInfo = m_pAdjustMgr.GetDscAt(i);

		if( nPosX == 0 || nPosY == 0 || pAdjustInfo->pBmpBits == NULL ||
			nPosX > pAdjustInfo->nWidht - (pAdjustInfo->nWidht/10.) ||
			nPosY > pAdjustInfo->nHeight - (pAdjustInfo->nHeight/10.) )			
			continue;	

		float result = m_pAdjustMgr.FocusValue(pAdjustInfo, nPosX, nPosY);		

		CString strResult;
		strResult.Format(_T("%.3f"), result);		
		m_AdjustDscList.SetItemText(i, 2, strResult);			
	}
	
}

void ESMAdjustMgrDlg::OnBnClickedBtnFocusSave()
{
	if (AfxMessageBox(_T("포커스 데이터를 새로 저장하시겠습니까?"),MB_YESNO)==IDYES) 
	{
		for(int i = 0; i < m_AdjustDscList.GetItemCount(); i++)
		{
			int nPosX = _ttoi(m_AdjustDscList.GetItemText(i, 7));
			int nPosY = _ttoi(m_AdjustDscList.GetItemText(i, 8));

			DscAdjustInfo* pAdjustInfo = NULL;
			pAdjustInfo = m_pAdjustMgr.GetDscAt(i);

			if( nPosX == 0 || nPosY == 0 || pAdjustInfo->pBmpBits == NULL ||
				nPosX > pAdjustInfo->nWidht - (pAdjustInfo->nWidht/10.) ||
				nPosY > pAdjustInfo->nHeight - (pAdjustInfo->nHeight/10.) )			
				continue;	

			float result = m_pAdjustMgr.FocusValue(pAdjustInfo, nPosX, nPosY);		

			CString strResult;
			strResult.Format(_T("%.3f"), result);		
			m_AdjustDscList.SetItemText(i, 2, strResult);				
		}

	//	m_pAdjustMgr.CalcThresholdFocusValue();

	//	m_dThresholdFocus = m_pAdjustMgr.GetThresholdFocusValue();

	//	CESMIni ini;
	//	CFile file;

	//	CString strFileName;
	//	strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), _T("Focus.ini"));	

	//	file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite);
	//	file.Close();

	//	if(!ini.SetIniFilename (strFileName))
	//		return;

	//	int nFocusData;
	//	nFocusData = m_dThresholdFocus * 1000;

	//	ini.WriteInt(_T("Focus"), _T("Threshold Value"), nFocusData);	
	//}
	//else 
	//{
	//	return;
	}	
}

void ESMAdjustMgrDlg::OnBnClickedBtnFocusDelete()
{
	if (AfxMessageBox(_T("포커스 데이터를 삭제하시겠습니까?"),MB_YESNO)==IDYES) 
	{
		CESMIni ini;
		CFile file;

		CString strFileName;
		strFileName.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP), _T("Focus.ini"));	

		file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite);
		file.Close();

		if(!ini.SetIniFilename (strFileName))
			return;	
		else
			DeleteFiles(strFileName);
	}
	else
	{
		return;
	}
}
void ESMAdjustMgrDlg::OnBnClickedAllReg()
{
	int nTotalItem = m_AdjustDscList.GetItemCount();
	
	for(int i = 0 ; i < nTotalItem; i++)
	{
		m_AdjustDscList.SetItemText(i, 10, _T("0"));
	}
}
HBRUSH ESMAdjustMgrDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	hbr = (HBRUSH)m_background;
	switch(nCtlColor)
	{
	case CTLCOLOR_STATIC:
		{

			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(56, 56, 56));  // 글자 배경색 변경
			pDC->SetBkMode(TRANSPARENT);
			//return (HBRUSH)m_brush;
		}
		break;
	case CTLCOLOR_EDIT:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0, 0 ,0));
			//hbr = (HBRUSH)(hbr.GetSafeHandle());  
		}
		break;
	}    

	
	return hbr;
}