#pragma once

// ESMAdjustMgrDlg 대화 상자입니다.
#include "ESMAdjustImage.h"
#include "ESMFunc.h"
#include "ESMAdjustMgr.h"
#include "ESMAdjustAsAuto.h"
#include <vector>
#include "afxwin.h"
#include "ESMNetworkListCtrl.h"
#include "ESMEditorEX.h"
#include "ESMGroupBox.h"
#include "ESMButtonEx.h"

#define ADJUSTTEMPFILE _T("temp.ptl")
class ESMAdjustMgrDlg : public CDialogEx
{
	DECLARE_DYNAMIC(ESMAdjustMgrDlg)

public:
	ESMAdjustMgrDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~ESMAdjustMgrDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ADJUSTMGR_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	BOOL m_bThreadStop;
	BOOL m_bThreadLoad;
public:
	afx_msg void OnBnClickedBtnCopyPoint();
	afx_msg void OnBnClickedBtnSaveAdjus();
	afx_msg void OnBnClickedBtnMoveCal();
	afx_msg void OnBnClickedBtnRotateCal();
	afx_msg void OnBnClickedBtn2ndSavePointAdj();
	afx_msg void OnBnClickedBtn2ndSavePoint();
	afx_msg void OnBnClickedBtn2ndLoadPoint();

	afx_msg void OnBnClickedBtnLeft();
	afx_msg void OnBnClickedBtnRight();
	afx_msg void OnBnClickedBtnDown();
	afx_msg void OnBnClickedBtnUp();
	afx_msg void OnBnClickedBtnRotateLeft();
	afx_msg void OnBnClickedBtnRotateRight();
	afx_msg void OnBnClickedBtnSaveImg();
	afx_msg void OnBnClickedBtnLoadImg();
	afx_msg void OnBnClickedBtnReSaveAdjust();
	//wgkim 17-06-26
	afx_msg void OnBnClickedBtnFocus();
	afx_msg void OnBnClickedBtnFocusSave();
	afx_msg void OnBnClickedBtnFocusDelete();

	afx_msg void OnBnClickedBtnView1();
	afx_msg void OnBnClickedBtnView2();
	afx_msg void OnBnClickedBtnView3();
	afx_msg void OnBnClickedOk();

	BOOL OnKeyboardedRotateLeft(MSG* pMSG);
	virtual BOOL OnInitDialog();
	void LoadView(int nView);
	void AddDscInfo(CString strDscId, CString strDscIp,BOOL bReverse = FALSE);
	void SetMovieState(int nMovieState = ESM_ADJ_FILM_STATE_MOVIE);
	static unsigned WINAPI GetMovieDataThread(LPVOID param);
	static unsigned WINAPI GetImageDataThread(LPVOID param);
//	static unsigned WINAPI GetSearchThread(LPVOID param);
	void Showimage();
	void DeleteImage();   
	void DrawImage(int nImg = 0);   
	void DrawCenterPointAll();
	void DrawCenterPoint(int nPosX, int nPosY);
	void CalcAdjustData();
	void GetDscInfo(vector<DscAdjustInfo*>** pArrDscInfo);
	void ClearDscInfo();
	void LoadData();
	void Load2ndData();
	void SavePointData(CString strFileName);
	void LoadPointData(CString strFileName);
	void CalcSelectedDistance();

	int Round(double dData);
	void GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle);
	void GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY);
	void GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY);

	void CpuMakeMargin(Mat* gMat, int nX, int nY);
	void CpuMoveImage(Mat* gMat, int nX, int nY);
	void CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle);

	//2016-08-03 hjcho
	void Matrix_Image(Mat a1,Mat a2,Mat result);

	//-- 2014-08-31 hongsu@esmlab.com
	//-- Change Real Image Position from Mouse Point
	CPoint GetPointFromMouse(CPoint pt);

	//-- 2014-08-30 hongsu@esmlab.com
	//-- Find Exact Position
	void FindExactPosition(int& nX, int& nY);
	
	//Ctrl
	vector<stAdjustInfo> m_adjinfo;
	CESMNetworkListCtrl m_AdjustDscList;
	CESMNetworkListCtrl m_AdjustDsc2ndList;
	ESMAdjustImage m_StAdjustImageRe;
	ESMAdjustImage m_StAdjustImage;
	ESMAdjustImage m_StAdjustImageOrigin;
	ESMAdjustImage m_StAdjustImageAfter;
	ESMAdjustMgr m_pAdjustMgr;
	ESMAdjustMgr m_check_adj;
	ESMAdjustMgr m_pAdjustMgrOrigin;
	ESMAdjustMgr m_pAdjustMgrAfter;
	ESMAdjustMgr m_pAdjustMgrRe;
	ESMAdjustMgr m_pAdjustMgrMemory;
	int m_RdImageViewColor;
	int m_nRdDetectColor;
	COLORREF m_clrDetectColor;
	CESMEditorEX m_edit_frameIndex;
	//변수
	int m_nSelectedNum;
	int m_nPosX, m_nPosY;
	int m_nSelectPos;
	int m_nThreshold;
	int m_nMovieState;
	int m_nAdjViewWidth, m_nAdjViewHeight;
	int m_nPointGapMin;
	int m_nTargetLenth;
	int m_nCameraZoom;
	int m_ImageLoaded;
	CSize m_sizeDetectMIN, m_sizeDetectMAX;
	int m_frameIndex;
	int m_nView;
	int m_nImgView;
	int m_MarginX;
	int m_MarginY;
	BOOL m_bChangeFlag;
	BOOL m_bCenterGrid;
	BOOL m_bAutoFind;
	BOOL m_bZoomUse;
	BOOL m_bAutoMove;
	BOOL m_bDistanceUse;
	HANDLE	m_hTheadHandle;

	//wgkim@esmlab.com 17-06-21
	BOOL m_bThreadCloseFlag;


	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLvnItemchangedAdjust(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedAdjust2nd(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnAutosearch();
	afx_msg void OnBnClickedRdAdjustviewcolor();
	afx_msg void OnBnClickedRdAdjustviewgray();
	afx_msg void OnBnClickedRdAdjustDetectBlack();
	afx_msg void OnBnClickedRdAdjustDetectWhite();
	afx_msg void OnBnClickedBtnCalcadjust();
	afx_msg void OnCustomdrawTcpList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCustomdrawTcpList2nd(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedBtnAdjustpointload();
	afx_msg void OnBnClickedBtnAdjustpointsave();
	//-- 2014-08-31 hongsu@esmlab.com
	//-- Change Next Item
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnEnKillfocusEdtLine2();
	afx_msg void OnEnKillfocusEdtLine1();
	afx_msg void OnBnClickedBtnCalcdistance();
	afx_msg void OnBnClickedBtnDistanceAdd();
	afx_msg void OnBnClickedBtnDistanceDel();
	afx_msg void OnBnClickedBtnDistanceReset();
	afx_msg void OnBnClickedBtnAdjustPointReset();
	afx_msg void OnBnClickedBtnCommit();
	afx_msg void OnRadioCheck(UINT value);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	
	CStatic m_ctrl2ndOption;
	CStatic m_stCenterLine;
	CESMEditorEX m_ctrlCenterLine;
	CESMButtonEx m_btnSavePoint;
	CESMButtonEx m_btnLoadPoint;
	CESMButtonEx m_btnCalculate;
	CESMButtonEx m_btnMoveCal;
	CESMButtonEx m_btnSaveAdjust;
	CESMButtonEx m_btnSavePointAdj;
	CESMButtonEx m_btnCopyPoint;
	CESMButtonEx m_btnLeft;
	CESMButtonEx m_btnRight;
	CESMButtonEx m_btnUp;
	CESMButtonEx m_btnDown;
	CESMButtonEx m_btnRotateLeft;
	CESMButtonEx m_btnRotateRight;
	CESMButtonEx m_btnSaveImg;
	CESMButtonEx m_btnLoadImg;
	CESMButtonEx m_btnReSaveAdjust;
	CComboBox m_ctrlCheckPoint;

	//2016-08-03 hjcho
	int sel;
	int m_recals;
	CESMButtonEx m_recal_sub;
	CESMButtonEx m_recal_puz;

	//jhhan --Dialog Scroll
private:
	int		m_nCurWidth;
	int		m_nCurHeight;
	int		m_nHScrollPos;
	int		m_nVScrollPos;
	CRect	m_rcRect;

public:
//	afx_msg void OnSize(UINT nType, int cx, int cy);
//	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
//	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

	afx_msg void OnBnClickedBtnAutoFind();

public:
	//-- 2016-11-01 wgkim@esmlab.com
	//Implementation AutoAdjust Using LKOpticalFlow
	ESMAdjustAsAuto adjustAsAuto;

	//170615 hjcho - 선택 영역 지우기
	void DeleteSelectedList();
	void DeleteSelectItem(int nIndex);

	//170615 hjcho - Adjust Save
	afx_msg void OnBnClickedSaveAdjust();	
	CESMButtonEx m_btnSaveAdjust_View1;
	BOOL m_bAdjSave;
	void SaveAdjust();

	//170713 wgkim - Focus
	float m_dThresholdFocus;

	//171205 hjcho
	afx_msg void OnBnClickedAllReg();

	//180528 hjcho
	CBrush m_background;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CESMEditorEX m_ctrlBoxSize;
	CESMEditorEX m_editLine1;
	CESMEditorEX m_ctrlLine2;
	CStatic m_ctrlGrpPointSetting;
	CStatic m_ctrlGrpAction;
	CStatic m_ctrlGrpPointLoad;
	CStatic m_ctrlGrpDisReg;
	CStatic m_ctrlGrpFocus;
	CStatic m_ctrlGrpInfo;
	CStatic m_ctrlGrpFramdIdx;
	CESMButtonEx m_btnCalcAdjust;
	CESMButtonEx m_btnCalcDistance;
	CESMButtonEx m_btnPointSave;
	CESMButtonEx m_btnPointLoad;
	CESMButtonEx m_btnPointReset;
	CESMButtonEx m_btnDistanceReg;
	CESMButtonEx m_btnAllReg;
	CESMButtonEx m_btnDistanceDelete;
	CESMButtonEx m_btnDistanceClear;
	CESMButtonEx m_btnFocusSave;
	CESMButtonEx m_btnFocusDelete;
	CESMButtonEx m_btnFocusCalc;
	CESMButtonEx m_btnFrameCommit;
	CESMButtonEx m_btnOK;
	CESMButtonEx m_btnCancel;
};
 
