////////////////////////////////////////////////////////////////////////////////
//
//	4DMakerAbout.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-17
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "4DMaker.h"
#include "4DMakerAbout.h"
#include "ESMFunc.h"


// C4DMakerAbout 대화 상자입니다.

IMPLEMENT_DYNAMIC(C4DMakerAbout, CDialog)

C4DMakerAbout::C4DMakerAbout(CWnd* pParent /*=NULL*/)
	: CDialog(C4DMakerAbout::IDD, pParent)	
{
	//m_strVersion = ESMGetOption(ESM_OPT_4DMaker_VERSION);
	m_strVersion = _T("");
}

C4DMakerAbout::~C4DMakerAbout()
{
}

void C4DMakerAbout::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_VER, m_strVersion);
}


BEGIN_MESSAGE_MAP(C4DMakerAbout, CDialog)
	ON_BN_CLICKED(IDOK, &C4DMakerAbout::OnBnClickedOk)
END_MESSAGE_MAP()

void C4DMakerAbout::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialog::OnOK();
}
