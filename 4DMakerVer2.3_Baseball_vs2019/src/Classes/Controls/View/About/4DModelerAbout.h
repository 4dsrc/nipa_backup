////////////////////////////////////////////////////////////////////////////////
//
//	4DModelerAbout.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-17
//
////////////////////////////////////////////////////////////////////////////////

#pragma once


// C4DModelerAbout 대화 상자입니다.

class C4DModelerAbout : public CDialog
{
	DECLARE_DYNAMIC(C4DModelerAbout)

public:
	C4DModelerAbout(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~C4DModelerAbout();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()

public:
	CString m_strVersion;
	afx_msg void OnBnClickedOk();
};
