////////////////////////////////////////////////////////////////////////////////
//
//	VDAbout.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-17
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VDAgent.h"
#include "VDAbout.h"


// CVDAbout 대화 상자입니다.

IMPLEMENT_DYNAMIC(CVDAbout, CDialog)

CVDAbout::CVDAbout(CWnd* pParent /*=NULL*/)
	: CDialog(CVDAbout::IDD, pParent)	
{
	//m_strVersion = TGGetOption(TG_OPT_SMILE_VERSION);
}

CVDAbout::~CVDAbout()
{
}

void CVDAbout::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
//	DDX_Text(pDX, IDC_VER, m_strVersion);
}


BEGIN_MESSAGE_MAP(CVDAbout, CDialog)
END_MESSAGE_MAP()