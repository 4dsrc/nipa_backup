// ESMFileMergeDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMFileMergeDlg.h"
#include "afxdialogex.h"

#include "ESMFileOperation.h"

// CESMFileMergeDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMFileMergeDlg, CDialogEx)

CESMFileMergeDlg::CESMFileMergeDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMFileMergeDlg::IDD, pParent)
{
	
}
void CESMFileMergeDlg::InitImageFrameWnd()
{
 
}

CESMFileMergeDlg::~CESMFileMergeDlg()
{
}

void CESMFileMergeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MERGE_BTN_1, m_Btn_Img1);
	DDX_Control(pDX, IDC_MERGE_BTN_2, m_Btn_Img2);
	DDX_Control(pDX, IDC_MERGE_BTN_3, m_Btn_Img3);
	DDX_Control(pDX, IDC_MERGE_BTN_4, m_Btn_Img4);
	DDX_Control(pDX, IDC_MERGE_BTN_5, m_Btn_Img5);
	DDX_Control(pDX, IDC_MERGE_BTN_6, m_Btn_Img6);
	DDX_Control(pDX, IDC_MERGE_BTN_7, m_Btn_Img7);
	DDX_Control(pDX, IDC_MERGE_BTN_8, m_Btn_Img8);
	DDX_Control(pDX, IDC_MERGE_BTN_9, m_Btn_Img9);

	DDX_Control(pDX, IDC_LISTVIEW_FILELIST, m_FileList);
	DDX_Control(pDX, IDC_LISTVIEW_SELECT_LIST, m_SelectList);
}


BEGIN_MESSAGE_MAP(CESMFileMergeDlg, CDialogEx)
	ON_BN_CLICKED(IDC_MERGE_BTN_1, &CESMFileMergeDlg::OnBnClickedMergeBtn1)
	ON_BN_CLICKED(IDC_MERGE_BTN_2, &CESMFileMergeDlg::OnBnClickedMergeBtn2)
	ON_BN_CLICKED(IDC_MERGE_BTN_3, &CESMFileMergeDlg::OnBnClickedMergeBtn3)
	ON_BN_CLICKED(IDC_MERGE_BTN_4, &CESMFileMergeDlg::OnBnClickedMergeBtn4)
	ON_BN_CLICKED(IDC_MERGE_BTN_5, &CESMFileMergeDlg::OnBnClickedMergeBtn5)
	ON_BN_CLICKED(IDC_MERGE_BTN_6, &CESMFileMergeDlg::OnBnClickedMergeBtn6)
	ON_BN_CLICKED(IDC_MERGE_BTN_7, &CESMFileMergeDlg::OnBnClickedMergeBtn7)
	ON_BN_CLICKED(IDC_MERGE_BTN_8, &CESMFileMergeDlg::OnBnClickedMergeBtn8)
	ON_BN_CLICKED(IDC_MERGE_BTN_9, &CESMFileMergeDlg::OnBnClickedMergeBtn9)
	ON_BN_CLICKED(IDC_MERGE_SELECT_CLEAR, &CESMFileMergeDlg::OnBnClickedMergeSelectClear)
	ON_BN_CLICKED(IDC_MERGE_MERGE, &CESMFileMergeDlg::OnBnClickedMergeMerge)
	ON_BN_CLICKED(IDC_MERGE_LOAD, &CESMFileMergeDlg::OnBnClickedMergeLoad)
END_MESSAGE_MAP()


// CESMFileMergeDlg 메시지 처리기입니다.


BOOL CESMFileMergeDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_Btn_Img1.LoadBitmaps(IDB_MERGE_IMG_RED_1,NULL,NULL,NULL);
	m_Btn_Img2.LoadBitmaps(IDB_MERGE_IMG_RED_2,NULL,NULL,NULL);
	m_Btn_Img3.LoadBitmaps(IDB_MERGE_IMG_RED_3,NULL,NULL,NULL);
	m_Btn_Img4.LoadBitmaps(IDB_MERGE_IMG_RED_4,NULL,NULL,NULL);
	m_Btn_Img5.LoadBitmaps(IDB_MERGE_IMG_RED_5,NULL,NULL,NULL);
	m_Btn_Img6.LoadBitmaps(IDB_MERGE_IMG_RED_6,NULL,NULL,NULL);
	m_Btn_Img7.LoadBitmaps(IDB_MERGE_IMG_RED_7,NULL,NULL,NULL);
	m_Btn_Img8.LoadBitmaps(IDB_MERGE_IMG_RED_8,NULL,NULL,NULL);
	m_Btn_Img9.LoadBitmaps(IDB_MERGE_IMG_RED_9,NULL,NULL,NULL);


	m_Btn_Img1.SizeToContent();
	m_Btn_Img2.SizeToContent();
	m_Btn_Img3.SizeToContent();
	m_Btn_Img4.SizeToContent();
	m_Btn_Img5.SizeToContent();
	m_Btn_Img6.SizeToContent();
	m_Btn_Img7.SizeToContent();
	m_Btn_Img8.SizeToContent();
	m_Btn_Img9.SizeToContent();

	btn1 = 0;
	btn2 = 0;
	btn3 = 0;
	btn4 = 0;
	btn5 = 0;
	btn6 = 0;
	btn7 = 0;
	btn8 = 0;
	btn9 = 0;


	m_FileList.InsertColumn(0, _T("파일명"), 250, -1);  
	m_SelectList.InsertColumn(0, _T("파일명"), 250, -1);



	m_FileList.SetColumnWidth(0, 250);
	m_SelectList.SetColumnWidth(0, 250);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CESMFileMergeDlg::SetMoviePathArr(CString moviePath,CString strName,bool m_flag)
{
	
	
	if(m_flag)
	{
		ClearMoviePath();
		//m_arrMovie.clear();
		//m_FileList.DeleteAllItems(); 
		CString str;
		AfxExtractSubString(str,strName,1,'#');
		SetBtnImage(str[0]);
		CString bstr = str[0];
		int nNumber;
		nNumber = _ttoi(bstr);

		MovieFile m;
		m.FileName = strName;
		m.path = moviePath;
		m.nNumber = nNumber;
		m.FileTime = str.Mid( 3, str.GetLength());

		m_arrFileMovie.push_back(m);	
		m_arrMovie = m_arrFileMovie;


		for(int i = 0 ; i < m_arrMovie.size();i++)
		{			
			m_FileList.InsertItem(0,m_arrMovie[i].FileName); 
			SetBtnImage(m_arrMovie[i].nNumber);
		}
	}
	else
	{
		m_arrMovie.clear();
		m_arrMovie = m_arrMovieSub;

		for(int i = 0 ; i < m_arrMovie.size();i++)
		{			
			
			m_FileList.InsertItem(0,m_arrMovie[i].FileName); 
			SetBtnImage(m_arrMovie[i].nNumber);
		}
		
	}
	

	
	
}

void CESMFileMergeDlg:: SetBtnImage(int nNumber)
{
	
	switch(nNumber)
	{
	case 1: m_Btn_Img7.LoadBitmaps(IDB_MERGE_IMG_7,NULL,NULL,NULL); m_Btn_Img7.SizeToContent(); btn7 = 1; break;
	case 2: m_Btn_Img8.LoadBitmaps(IDB_MERGE_IMG_8,NULL,NULL,NULL); m_Btn_Img8.SizeToContent(); btn8 = 1;break;
	case 3: m_Btn_Img9.LoadBitmaps(IDB_MERGE_IMG_9,NULL,NULL,NULL); m_Btn_Img9.SizeToContent(); btn9 = 1;break;
	case 4: m_Btn_Img4.LoadBitmaps(IDB_MERGE_IMG_4,NULL,NULL,NULL); m_Btn_Img4.SizeToContent(); btn4 = 1;break;
	case 5: m_Btn_Img5.LoadBitmaps(IDB_MERGE_IMG_5,NULL,NULL,NULL); m_Btn_Img5.SizeToContent(); btn5 = 1;break;
	case 6: m_Btn_Img6.LoadBitmaps(IDB_MERGE_IMG_6,NULL,NULL,NULL); m_Btn_Img6.SizeToContent(); btn6 = 1;break;
	case 7: m_Btn_Img1.LoadBitmaps(IDB_MERGE_IMG_1,NULL,NULL,NULL); m_Btn_Img1.SizeToContent(); btn1 = 1;break;
	case 8: m_Btn_Img2.LoadBitmaps(IDB_MERGE_IMG_2,NULL,NULL,NULL); m_Btn_Img2.SizeToContent(); btn2 = 1;break;
	case 9: m_Btn_Img3.LoadBitmaps(IDB_MERGE_IMG_3,NULL,NULL,NULL); m_Btn_Img3.SizeToContent(); btn3 = 1;break;

	}
	UpdateData(FALSE);

	Invalidate();
}

void CESMFileMergeDlg::ClearMoviePath()
{
	m_arrMovie.clear();
	m_FileList.DeleteAllItems(); 
	m_SelectList.DeleteAllItems(); 

	m_Btn_Img1.LoadBitmaps(IDB_MERGE_IMG_RED_1,NULL,NULL,NULL);
	m_Btn_Img2.LoadBitmaps(IDB_MERGE_IMG_RED_2,NULL,NULL,NULL);
	m_Btn_Img3.LoadBitmaps(IDB_MERGE_IMG_RED_3,NULL,NULL,NULL);
	m_Btn_Img4.LoadBitmaps(IDB_MERGE_IMG_RED_4,NULL,NULL,NULL);
	m_Btn_Img5.LoadBitmaps(IDB_MERGE_IMG_RED_5,NULL,NULL,NULL);
	m_Btn_Img6.LoadBitmaps(IDB_MERGE_IMG_RED_6,NULL,NULL,NULL);
	m_Btn_Img7.LoadBitmaps(IDB_MERGE_IMG_RED_7,NULL,NULL,NULL);
	m_Btn_Img8.LoadBitmaps(IDB_MERGE_IMG_RED_8,NULL,NULL,NULL);
	m_Btn_Img9.LoadBitmaps(IDB_MERGE_IMG_RED_9,NULL,NULL,NULL);


	m_Btn_Img1.SizeToContent();
	m_Btn_Img2.SizeToContent();
	m_Btn_Img3.SizeToContent();
	m_Btn_Img4.SizeToContent();
	m_Btn_Img5.SizeToContent();
	m_Btn_Img6.SizeToContent();
	m_Btn_Img7.SizeToContent();
	m_Btn_Img8.SizeToContent();
	m_Btn_Img9.SizeToContent();
}

void CESMFileMergeDlg::OnUnSelectAll()
{
	m_FileList.SetFocus();
	int n =0;
	int nLast = m_FileList.GetItemCount();

	for(;n<nLast;n++)
	{
		if(m_FileList.GetItemState(n,LVIS_SELECTED == LVIS_SELECTED))
		{
			m_FileList.SetItemState(n,0,LVIS_SELECTED);
			m_FileList.SetItemState(n,0,LVIS_FOCUSED);
		}
	}
}

void CESMFileMergeDlg::OnBnClickedMergeBtn1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(btn1 != 0)
	{
		if(btn1 == 1)
		{
			int cnt = 0 ;
			CString str;
			int selectNumber = m_FileList.GetNextItem( -1, LVNI_SELECTED );
			str = m_FileList.GetItemText( selectNumber, 0);
			CString list_Str;
			int nIndex;
			int g = m_FileList.GetItemCount();
			for(int a = 0; a< g;a++)
			{
				list_Str = m_FileList.GetItemText( a, 0);
				nIndex = list_Str.Find( '#' );
				if(list_Str[nIndex+1] == '7')
				{
					cnt++;
				}
			}

			for(int j = 0 ; j< m_arrMovie.size();j++ )
			{
				if(m_arrMovie[j].nNumber == 7)
				{
					if( cnt >1)
					{
						if(m_arrMovie[j].FileName == str)
						{
							m_arrSelectMovie.push_back(m_arrMovie[j].path);
							m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
							m_Btn_Img1.LoadBitmaps(IDB_MERGE_IMG_BLUE_1,NULL,NULL,NULL);
							m_Btn_Img1.SizeToContent();
							OnUnSelectAll();
							return;

						}
					}					
					else
					{
						m_arrSelectMovie.push_back(m_arrMovie[j].path);
						m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
						m_Btn_Img1.LoadBitmaps(IDB_MERGE_IMG_BLUE_1,NULL,NULL,NULL);
						m_Btn_Img1.SizeToContent();
						OnUnSelectAll();
						return;
					}
				}
			}

			if(cnt >1)
			{
				AfxMessageBox(_T("해당 위치 영상이 1개 이상입니다. 리스트에서 영상을 선택해주세요."));
				return;
			}

			
		//	m_arrSelectMovie.push_back(0);
		//	m_SelectList.InsertItem(0,strName);
		}
	}
}


void CESMFileMergeDlg::OnBnClickedMergeBtn2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(btn2 != 0)
	{
		if(btn2 == 1)
		{
			int cnt = 0 ;
			CString str;
			int selectNumber = m_FileList.GetNextItem( -1, LVNI_SELECTED );
			str = m_FileList.GetItemText( selectNumber, 0);
			CString list_Str;
			int nIndex;
			int g = m_FileList.GetItemCount();
			for(int a = 0; a< g;a++)
			{
				list_Str = m_FileList.GetItemText( a, 0);
				nIndex = list_Str.Find( '#' );
				if(list_Str[nIndex+1] == '8')
				{
					cnt++;
				}
			}

			for(int j = 0 ; j< m_arrMovie.size();j++ )
			{
				if(m_arrMovie[j].nNumber == 8)
				{
					if( cnt >1)
					{
						if(m_arrMovie[j].FileName == str)
						{
							m_arrSelectMovie.push_back(m_arrMovie[j].path);
							m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
							m_Btn_Img2.LoadBitmaps(IDB_MERGE_IMG_BLUE_2,NULL,NULL,NULL);
							m_Btn_Img2.SizeToContent();
							OnUnSelectAll();
							return;

						}
					}					
					else
					{
						m_arrSelectMovie.push_back(m_arrMovie[j].path);
						m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
						m_Btn_Img2.LoadBitmaps(IDB_MERGE_IMG_BLUE_2,NULL,NULL,NULL);
						m_Btn_Img2.SizeToContent();
						OnUnSelectAll();
						return;
					}
				}
			}

			if(cnt >1)
			{
				AfxMessageBox(_T("해당 위치 영상이 1개 이상입니다. 리스트에서 영상을 선택해주세요."));
				return;
			}


			//	m_arrSelectMovie.push_back(0);
			//	m_SelectList.InsertItem(0,strName);
		}
	}
}


void CESMFileMergeDlg::OnBnClickedMergeBtn3()
{
	if(btn3 != 0)
	{
		if(btn3 == 1)
		{
			int cnt = 0 ;
			CString str;
			int selectNumber = m_FileList.GetNextItem( -1, LVNI_SELECTED );
			str = m_FileList.GetItemText( selectNumber, 0);
			CString list_Str;
			int nIndex;
			int g = m_FileList.GetItemCount();
			for(int a = 0; a< g;a++)
			{
				list_Str = m_FileList.GetItemText( a, 0);
				nIndex = list_Str.Find( '#' );
				if(list_Str[nIndex+1] == '9')
				{
					cnt++;
				}
			}

			for(int j = 0 ; j< m_arrMovie.size();j++ )
			{
				if(m_arrMovie[j].nNumber == 9)
				{
					if( cnt >1)
					{
						if(m_arrMovie[j].FileName == str)
						{
							m_arrSelectMovie.push_back(m_arrMovie[j].path);
							m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
							m_Btn_Img3.LoadBitmaps(IDB_MERGE_IMG_BLUE_3,NULL,NULL,NULL);
							m_Btn_Img3.SizeToContent();
							OnUnSelectAll();
							return;

						}
					}					
					else
					{
						m_arrSelectMovie.push_back(m_arrMovie[j].path);
						m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
						m_Btn_Img3.LoadBitmaps(IDB_MERGE_IMG_BLUE_3,NULL,NULL,NULL);
						m_Btn_Img3.SizeToContent();
						OnUnSelectAll();
						return;
					}
				}
			}

			if(cnt >1)
			{
				AfxMessageBox(_T("해당 위치 영상이 1개 이상입니다. 리스트에서 영상을 선택해주세요."));
				return;
			}


			//	m_arrSelectMovie.push_back(0);
			//	m_SelectList.InsertItem(0,strName);
		}
	}
}


void CESMFileMergeDlg::OnBnClickedMergeBtn4()
{
	if(btn4 != 0)
	{
		if(btn4 == 1)
		{
			int cnt = 0 ;
			CString str;
			int selectNumber = m_FileList.GetNextItem( -1, LVNI_SELECTED );
			str = m_FileList.GetItemText( selectNumber, 0);
			CString list_Str;
			int nIndex;
			int g = m_FileList.GetItemCount();
			for(int a = 0; a< g;a++)
			{
				list_Str = m_FileList.GetItemText( a, 0);
				nIndex = list_Str.Find( '#' );
				if(list_Str[nIndex+1] == '4')
				{
					cnt++;
				}
			}

			for(int j = 0 ; j< m_arrMovie.size();j++ )
			{
				if(m_arrMovie[j].nNumber == 4)
				{
					if( cnt >1)
					{
						if(m_arrMovie[j].FileName == str)
						{
							m_arrSelectMovie.push_back(m_arrMovie[j].path);
							m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
							m_Btn_Img4.LoadBitmaps(IDB_MERGE_IMG_BLUE_4,NULL,NULL,NULL);
							m_Btn_Img4.SizeToContent();
							OnUnSelectAll();
							return;

						}
					}					
					else
					{
						m_arrSelectMovie.push_back(m_arrMovie[j].path);
						m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
						m_Btn_Img4.LoadBitmaps(IDB_MERGE_IMG_BLUE_4,NULL,NULL,NULL);
						m_Btn_Img4.SizeToContent();
						OnUnSelectAll();
						return;
					}
				}
			}

			if(cnt >1)
			{
				AfxMessageBox(_T("해당 위치 영상이 1개 이상입니다. 리스트에서 영상을 선택해주세요."));
				return;
			}


			//	m_arrSelectMovie.push_back(0);
			//	m_SelectList.InsertItem(0,strName);
		}
	}
}


void CESMFileMergeDlg::OnBnClickedMergeBtn5()
{
	if(btn5 != 0)
	{
		if(btn5 == 1)
		{
			int cnt = 0 ;
			CString str;
			int selectNumber = m_FileList.GetNextItem( -1, LVNI_SELECTED );
			str = m_FileList.GetItemText( selectNumber, 0);
			CString list_Str;
			int nIndex;
			int g = m_FileList.GetItemCount();
			for(int a = 0; a< g;a++)
			{
				list_Str = m_FileList.GetItemText( a, 0);
				nIndex = list_Str.Find( '#' );
				if(list_Str[nIndex+1] == '5')
				{
					cnt++;
				}
			}

			for(int j = 0 ; j< m_arrMovie.size();j++ )
			{
				if(m_arrMovie[j].nNumber == 5)
				{
					if( cnt >1)
					{
						if(m_arrMovie[j].FileName == str)
						{
							m_arrSelectMovie.push_back(m_arrMovie[j].path);
							m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
							m_Btn_Img5.LoadBitmaps(IDB_MERGE_IMG_BLUE_5,NULL,NULL,NULL);
							m_Btn_Img5.SizeToContent();
							OnUnSelectAll();
							return;

						}
					}					
					else
					{
						m_arrSelectMovie.push_back(m_arrMovie[j].path);
						m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
						m_Btn_Img5.LoadBitmaps(IDB_MERGE_IMG_BLUE_5,NULL,NULL,NULL);
						m_Btn_Img5.SizeToContent();
						OnUnSelectAll();
						return;
					}
				}
			}

			if(cnt >1)
			{
				AfxMessageBox(_T("해당 위치 영상이 1개 이상입니다. 리스트에서 영상을 선택해주세요."));
				return;
			}


			//	m_arrSelectMovie.push_back(0);
			//	m_SelectList.InsertItem(0,strName);
		}
	}
}


void CESMFileMergeDlg::OnBnClickedMergeBtn6()
{
	if(btn6 != 0)
	{
		if(btn6 == 1)
		{
			int cnt = 0 ;
			CString str;
			int selectNumber = m_FileList.GetNextItem( -1, LVNI_SELECTED );
			str = m_FileList.GetItemText( selectNumber, 0);
			CString list_Str;
			int nIndex;
			int g = m_FileList.GetItemCount();
			for(int a = 0; a< g;a++)
			{
				list_Str = m_FileList.GetItemText( a, 0);
				nIndex = list_Str.Find( '#' );
				if(list_Str[nIndex+1] == '6')
				{
					cnt++;
				}
			}

			for(int j = 0 ; j< m_arrMovie.size();j++ )
			{
				if(m_arrMovie[j].nNumber == 6)
				{
					if( cnt >1)
					{
						if(m_arrMovie[j].FileName == str)
						{
							m_arrSelectMovie.push_back(m_arrMovie[j].path);
							m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
							m_Btn_Img6.LoadBitmaps(IDB_MERGE_IMG_BLUE_6,NULL,NULL,NULL);
							m_Btn_Img6.SizeToContent();
							OnUnSelectAll();
							return;

						}
					}					
					else
					{
						m_arrSelectMovie.push_back(m_arrMovie[j].path);
						m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
						m_Btn_Img6.LoadBitmaps(IDB_MERGE_IMG_BLUE_6,NULL,NULL,NULL);
						m_Btn_Img6.SizeToContent();
						OnUnSelectAll();
						return;

					}
				}
			}

			if(cnt >1)
			{
				AfxMessageBox(_T("해당 위치 영상이 1개 이상입니다. 리스트에서 영상을 선택해주세요."));
				return;
			}


			//	m_arrSelectMovie.push_back(0);
			//	m_SelectList.InsertItem(0,strName);
		}
	}
}


void CESMFileMergeDlg::OnBnClickedMergeBtn7()
{
	if(btn7 != 0)
	{
		if(btn7 == 1)
		{
			int cnt = 0 ;
			CString str;
			int selectNumber = m_FileList.GetNextItem( -1, LVNI_SELECTED );
			str = m_FileList.GetItemText( selectNumber, 0);
			CString list_Str;
			int nIndex;
			int g = m_FileList.GetItemCount();
			for(int a = 0; a< g;a++)
			{
				list_Str = m_FileList.GetItemText( a, 0);
				nIndex = list_Str.Find( '#' );
				if(list_Str[nIndex+1] == '1')
				{
					cnt++;
				}
			}

			for(int j = 0 ; j< m_arrMovie.size();j++ )
			{
				if(m_arrMovie[j].nNumber == 1)
				{
					if( cnt >1)
					{
						if(m_arrMovie[j].FileName == str)
						{
							m_arrSelectMovie.push_back(m_arrMovie[j].path);
							m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
							m_Btn_Img7.LoadBitmaps(IDB_MERGE_IMG_BLUE_7,NULL,NULL,NULL);
							m_Btn_Img7.SizeToContent();
							OnUnSelectAll();
							return;

						}
					}					
					else
					{						
						m_arrSelectMovie.push_back(m_arrMovie[j].path);
						m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
						m_Btn_Img7.LoadBitmaps(IDB_MERGE_IMG_BLUE_7,NULL,NULL,NULL);
						m_Btn_Img7.SizeToContent();
						OnUnSelectAll();
						return;
					}
				}
			}

			if(cnt >1)
			{
				AfxMessageBox(_T("해당 위치 영상이 1개 이상입니다. 리스트에서 영상을 선택해주세요."));
				return;
			}


			//	m_arrSelectMovie.push_back(0);
			//	m_SelectList.InsertItem(0,strName);
		}
	}
}


void CESMFileMergeDlg::OnBnClickedMergeBtn8()
{
	if(btn8 != 0)
	{
		if(btn8 == 1)
		{
			int cnt = 0 ;
			CString str;
			int selectNumber = m_FileList.GetNextItem( -1, LVNI_SELECTED );
			str = m_FileList.GetItemText( selectNumber, 0);
			CString list_Str;
			int nIndex;
			int g = m_FileList.GetItemCount();
			for(int a = 0; a< g;a++)
			{
				list_Str = m_FileList.GetItemText( a, 0);
				nIndex = list_Str.Find( '#' );
				if(list_Str[nIndex+1] == '2')
				{
					cnt++;
				}
			}

			for(int j = 0 ; j< m_arrMovie.size();j++ )
			{
				if(m_arrMovie[j].nNumber == 2)
				{
					if( cnt >1)
					{
						if(m_arrMovie[j].FileName == str)
						{
							m_arrSelectMovie.push_back(m_arrMovie[j].path);
							m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
							m_Btn_Img8.LoadBitmaps(IDB_MERGE_IMG_BLUE_8,NULL,NULL,NULL);
							m_Btn_Img8.SizeToContent();
							OnUnSelectAll();
							return;

						}
					}					
					else
					{
						m_arrSelectMovie.push_back(m_arrMovie[j].path);
						m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
						m_Btn_Img8.LoadBitmaps(IDB_MERGE_IMG_BLUE_8,NULL,NULL,NULL);
						m_Btn_Img8.SizeToContent();
						OnUnSelectAll();
						return;
					}
				}
			}

			if(cnt >1)
			{
				AfxMessageBox(_T("해당 위치 영상이 1개 이상입니다. 리스트에서 영상을 선택해주세요."));
				return;
			}


			//	m_arrSelectMovie.push_back(0);
			//	m_SelectList.InsertItem(0,strName);
		}
	}
}


void CESMFileMergeDlg::OnBnClickedMergeBtn9()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(btn9 != 0)
	{
		if(btn9 == 1)
		{
			int cnt = 0 ;
			CString str;
			int selectNumber = m_FileList.GetNextItem( -1, LVNI_SELECTED );
			str = m_FileList.GetItemText( selectNumber, 0);
			CString list_Str;
			int nIndex;
			int g = m_FileList.GetItemCount();
			for(int a = 0; a< g;a++)
			{
				list_Str = m_FileList.GetItemText( a, 0);
				nIndex = list_Str.Find( '#' );
				if(list_Str[nIndex+1] == '3')
				{
					cnt++;
				}
			}

			for(int j = 0 ; j< m_arrMovie.size();j++ )
			{
				if(m_arrMovie[j].nNumber == 3)
				{
					if( cnt >1)
					{
						if(m_arrMovie[j].FileName == str)
						{
							m_arrSelectMovie.push_back(m_arrMovie[j].path);
							m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
							m_Btn_Img9.LoadBitmaps(IDB_MERGE_IMG_BLUE_9,NULL,NULL,NULL);
							m_Btn_Img9.SizeToContent();
							OnUnSelectAll();
							return;

						}
					}					
					else
					{
						m_arrSelectMovie.push_back(m_arrMovie[j].path);
						m_SelectList.InsertItem(0,m_arrMovie[j].FileName); 
						m_Btn_Img9.LoadBitmaps(IDB_MERGE_IMG_BLUE_1,NULL,NULL,NULL);
						m_Btn_Img9.SizeToContent();
						OnUnSelectAll();
						return;
					}
				}
			}

			if(cnt >1)
			{
				AfxMessageBox(_T("해당 위치 영상이 1개 이상입니다. 리스트에서 영상을 선택해주세요."));
				return;
			}


			//	m_arrSelectMovie.push_back(0);
			//	m_SelectList.InsertItem(0,strName);
		}
	}
}


void CESMFileMergeDlg::OnBnClickedMergeSelectClear()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_arrSelectMovie.clear();
	m_SelectList.DeleteAllItems(); 

}


BOOL CESMFileMergeDlg::ISFILE(CString szFile)
{
	HANDLE hFind;
	WIN32_FIND_DATA fd;
	if ((hFind = ::FindFirstFile(szFile, &fd)) != INVALID_HANDLE_VALUE) {
		FindClose(hFind);
		return TRUE;
	}
	return FALSE;
}
void CESMFileMergeDlg::ChangeMovieSize(CString strMovieFile, CString strConcertPath)
{
	//Change  Size
	CString strCmd, strOpt;
	strCmd.Format(_T("\"%s\\bin\\ffmpeg.exe\""),ESMGetPath(ESM_PATH_HOME));

	ESMLog(1,_T("[Movie] Change Size Movie ... [%s]"),strMovieFile);

	strOpt.Format(_T("-threads 4 -y -i "));
	strOpt.Append(_T("\""));
	strOpt.Append(strMovieFile);
	strOpt.Append(_T("\""));
	strOpt.Append(_T(" -r 29.97 -vcodec libx264 -s 1920x1080 -b:v 8000k -profile:v high "));
	strOpt.Append(_T("\""));
	strOpt.Append(strConcertPath);
	strOpt.Append(_T("\""));

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
}


void CESMFileMergeDlg::OnBnClickedMergeMerge()
{
	CString strName = _T("4dmaker");
	//if(ESMGetBaseBallFlag())
	//{
	//	CString strInning = g_strInning[ESMGetBaseBallInning()];
	//	CTime time = CTime::GetCurrentTime();
	//	CString strTime = time.Format("%H_%M_%S");
	//	strName.Format(_T("MERGE_%s_%s"), strInning,strTime);	
	//}

	CString strMovieFile, strBcakupFile;
	strMovieFile.Format(_T("%s\\2D\\%s.mp4"), ESMGetPath(ESM_PATH_OUTPUT) ,strName);
	int nCount = 0;
	//CMiLRe 20151119 영상 저장 경로 변경
	while(FileExists(strMovieFile))
	{
		strMovieFile.Format(_T("%s\\2D\\%s_%d.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strName, nCount);
		nCount++;
	}

	strBcakupFile.Format(_T("%s\\2D\\BackUp\\%s_%s.mp4"),ESMGetPath(ESM_PATH_OUTPUT), strName, ESMGetFrameRecord());
	CESMFileOperation fo;
	//fo.Delete(strMovieFile);

	if(fo.Delete(strMovieFile))
	{
		CString strMovieFile_;
		strMovieFile_.Format(_T("%s\\2D\\%s_.mp4"), ESMGetPath(ESM_PATH_OUTPUT),strName);

		fo.Delete(strMovieFile_);
	}else
	{
		if (FileExists(strMovieFile))
		{
			ESMLog(0,_T("[Movie] MergeMovie File Delete Fail]"),strMovieFile);
			strMovieFile.Format(_T("%s\\2D\\%s_.mp4"), ESMGetPath(ESM_PATH_OUTPUT),strName);

			fo.Delete(strMovieFile);
		}else
		{
			CString strMovieFile_;
			strMovieFile_.Format(_T("%s\\2D\\%s_.mp4"), ESMGetPath(ESM_PATH_OUTPUT),strName);

			fo.Delete(strMovieFile_);
		}
	}

	CString strFile;
	CString strCmd, strOpt, strOpt1, strOpt2, strFileNames;

	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));	
	strFileNames.Format(_T("%s\\bin\\filenames.txt"),ESMGetPath(ESM_PATH_HOME));	

	ESMLog(1,_T("[CHECK_G][Movie] Create Movie ... [%s]"),strMovieFile);

	//strOpt.Format(_T("-i \"concat:"));
	strOpt.Format(_T("-y -f concat -i \"%s\" -an -c:v copy "), strFileNames);
	CFile ReadFile;
	int lFileSize = 0;
	
	// filenames create
	CFile file;
	file.Open(strFileNames, CFile::modeCreate|CFile::modeWrite);

	for(int i = 0 ; i < m_arrSelectMovie.size() ; i ++)
	{
		strFile = m_arrSelectMovie.at(i);
		ESMLog(5,_T("[Movie] Merge Movie File [%s]"),strFile);
		if(ReadFile.Open(strFile, CFile::modeRead))
		{
			lFileSize = ReadFile.GetLength();
			ReadFile.Close();
		}
		else
			lFileSize = 0;

		if (FileExists(strFile) && lFileSize > 0)
		{
			USES_CONVERSION;

			file.SeekToEnd();
			CString strData;
			LPCSTR pstr;
			strData.Format(_T("file '%s'\r\n"), strFile);
			pstr = W2A(strData);
			
			file.Write(pstr, strData.GetLength());
			
			/*if(i!=0 && strOpt != _T("-i \"concat:"))
				strOpt.Append(_T("|"));
			strOpt.Append(strFile);*/
		}
		else
		{
			ESMLog(5, _T("Not Exist File[%s]"), strFile);
			continue;
		}
	}		

	file.Close();

	strOpt.Append(_T("\""));
	strOpt.Append(strMovieFile);
	strOpt.Append(_T("\""));

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	fo.Copy(strMovieFile, strBcakupFile);
	ESMLog(5,_T("[Movie] Create Movie Finish [%s]"), strMovieFile);

	m_arrMovieSub = m_arrMovie;

	// Array Delete all
	m_arrSelectMovie.clear();
	m_arrFileMovie.clear();
	ESMEvent* pMsg2 = NULL;
	pMsg2 = new ESMEvent();
	pMsg2->message = WM_ESM_VIEW_MERGEMOVIE_CLEAR;
	pMsg2->pParam = NULL;
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);

#if 0  //-- Rotate Movie
	CString strMovieFile2;
	if(ESMGetReverseMovie())
	{
		int nStart;
		nStart= GetTickCount();
		ESMLog(5, _T("Rotate Time = %d"), nStart);
		strMovieFile2.Format(_T("%s\\2D\\4dmaker_re_%d.mp4"),ESMGetPath(ESM_PATH_OUTPUT), nCount);
		strOpt.Format(_T("-i \"%s\" -vf \"rotate=PI:bilinear=0,format=yuv420p\" -metadata:s:v rotate=0 -codec:v libx264 -codec:a copy \"%s\""), strMovieFile, strMovieFile2);
		SHELLEXECUTEINFO lpExecInfo;
		lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
		lpExecInfo.lpFile = strCmd;
		lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		lpExecInfo.hwnd = NULL;  
		lpExecInfo.lpVerb = L"open";
		lpExecInfo.lpParameters = strOpt;
		lpExecInfo.lpDirectory = NULL;

		lpExecInfo.nShow = SW_HIDE; // hide shell during execution
		lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
		ShellExecuteEx(&lpExecInfo);

		// wait until the process is finished
		if (lpExecInfo.hProcess != NULL)
		{
			::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
			::CloseHandle(lpExecInfo.hProcess);
		}	
		ESMLog(5, _T("Rotate End Time = %d, Gap = %d"), GetTickCount(), GetTickCount() - nStart);

		fo.Copy(strMovieFile2, strBcakupFile);
		ESMLog(5,_T("[Movie] Create Movie Finish [%s]"), strMovieFile2);

		if(ESMGetBaseBallFlag())
		{
			m_arrSelectMovie.push_back(strMovieFile2);
			ESMEvent* pMsg2 = NULL;
			pMsg2 = new ESMEvent();
			pMsg2->message = WM_ESM_VIEW_MERGEMOVIE_PUSHBACK;
			pMsg2->pParam = (LPARAM)&strMovieFile;
			::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
		}
	}
	else
	{
		fo.Copy(strMovieFile, strBcakupFile);
		ESMLog(5,_T("[Movie] Create Movie Finish [%s]"), strMovieFile);

		if(ESMGetBaseBallFlag())
		{
			m_arrSelectMovie.push_back(strMovieFile);
			ESMEvent* pMsg2 = NULL;
			pMsg2 = new ESMEvent();
			pMsg2->message = WM_ESM_VIEW_MERGEMOVIE_PUSHBACK;
			pMsg2->pParam = (LPARAM)&strMovieFile;
			::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
		}
	}

	if(1) //Auto Play
	{
		CString strMoviePath;
		if(ESMGetReverseMovie())
			strMoviePath = strMovieFile2;
		else
			strMoviePath = strMovieFile;

		//CMiLRe 20151119 영상 저장 경로 변경
		TCHAR* cPath = new TCHAR[1+strMoviePath.GetLength()];
		_tcscpy(cPath, strMoviePath);

		ESMEvent* pMsg2 = NULL;
		pMsg2 = new ESMEvent();
		pMsg2->message = WM_ESM_VIEW_MAKEMOVIEPLAY;
		pMsg2->pParam = (LPARAM)cPath;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
	}
#else
	if(1) //Auto Play
	{

		//CMiLRe 20151119 영상 저장 경로 변경
		TCHAR* cPath = new TCHAR[1+strMovieFile.GetLength()];
		_tcscpy(cPath, strMovieFile);

		ESMEvent* pMsg2 = NULL;
		pMsg2 = new ESMEvent();
		pMsg2->message = WM_ESM_VIEW_MAKEMOVIEPLAY;
		pMsg2->pParam = (LPARAM)cPath;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
	}
#endif

	strFile.Format(_T("%s"), ESMGetServerRamPath());
	fo.Delete(strFile,FALSE);

	if(ESMGetValue(ESM_VALUE_CEREMONYUSE))
	{
		CString strConcertPath;
		//CString strTel1 = ESMGetCeremonyInfoString(_T("InputTel"));
		CString strTel1 = ESMGetCeremonyInfoString(_T("OutputTel"));

		//file check
		int nIndex = 1;
		while(1)
		{
			strConcertPath.Format(_T("%s\\%s_%d.mp4"), ESMGetCeremony(ESM_VALUE_CEREMONYMOVIEPATH), strTel1, nIndex);
			if(!ISFILE(strConcertPath))
			{
				break;
			}
			else
				nIndex++;

			if(nIndex > 100)
				break;
		}

		//CopyFile(strMovieFile, strConcertPath, FALSE);
		ChangeMovieSize(strMovieFile, strConcertPath);
		//CHange Siize End
		//ESMSetCeremonyInfoString(_T("TelNum2"), strTel1);
		//ESMSetCeremonyInfoString(_T("OutputTel"), strTel1);
		ESMSetCeremonyInfoint(_T("SelectorSignal"), 1);
		ESMSetCeremonyInfoint(_T("PrintorSignal"), 2);
	}


	ESMLog(5,_T("[CHECK_H]Make Movie Finish [%d]"), GetTickCount());

	ESMEvent* pMsgDelete	= new ESMEvent;
	pMsgDelete->message = WM_ESM_FRAME_REMOVEALL_OBJ_EDITOR;
	::PostMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsgDelete);	
#ifdef CMiLRe0
	SetMakingMovieFlag(FALSE);
#endif
}


void CESMFileMergeDlg::OnBnClickedMergeLoad()
{
	ClearMoviePath();
	SetMoviePathArr("","",false);
}
