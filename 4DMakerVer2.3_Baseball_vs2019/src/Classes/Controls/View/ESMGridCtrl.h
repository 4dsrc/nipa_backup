////////////////////////////////////////////////////////////////////////////////
//
//	ESMGridCtrl.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <afxtempl.h>
#include "ESM.h"
#include "GlobalIndex.h"

//	[3/26/2009 Hongsu.Jung]	
#include "TestBase.h"

typedef struct _COLUMN_TEXT_PROP
{
	COLORREF	colColumn;
	CString		strMsg;
	int nIndex ;
} COLUMN_TEXT_PROP;
class CPtrList2 : public CPtrList
{
public:
	CPtrList2(){ };
	CPtrList2( CPtrList2& ref )
	{		
		POSITION pos = ref.GetHeadPosition();
		while (pos != NULL)
		{
			AddTail(ref.GetNext(pos));
		}
	}	
};

class CItemInfo //should have called this LV_INSERTITEM ..what ever
{
public:	
	CItemInfo()	
	{
		InitData();	
	}

	CItemInfo(CTestBase* pTest)
	{ 
		InitData();
		m_pTestBase = pTest; 
	}

	//-- ESMA, TQM
	CTestBase* m_pTestBase;

	void InitData()
	{
		m_iImage		= (const int)-1;
		m_lParam		= NULL;
		m_wParam		= NULL;
		m_nParamStatus	= ESMA_STATUS_READY;
		m_clf			= (COLORREF)-1;
		m_nRunStatus	= ESMA_STATUS_READY;
		m_nProjectDataType		= 0;
		m_pTestBase		= NULL;
	}

	enum COLUMNPROP {PROP_EDITABLE, PROP_NUM, PROP_DONTEDIT};
	enum CONTROLTYPE {
		edit/*default*/ = 1, 
		combobox, 
		datecontrol/*your control*/, 
		spinbutton/*your control*/, 
		dropdownlistviewwhatevercontrol/*your control*/
	};
	
	void SetItemText(const CString& strItem){ m_strItemName = strItem; }
	void SetItemText(const CString& strItem, COLORREF clf) { m_strItemName = strItem; m_clf = clf;}	
	void AddSubItemText(const CString& strSubItem){ m_SubItems.Add(strSubItem); }
	void AddSubItemText(const CString& strSubItem, COLORREF clf)
	{ 
		int nIndex = m_SubItems.Add(strSubItem); 
		m_mapClf.SetAt(nIndex, clf);
	}
	void SetSubItemText(int iSubItem, const CString& strSubItem, int iImage = -1)
	{
		m_SubItems.SetAESMrow(iSubItem, strSubItem); 		
		if( iImage >= 0 )
			m_iImageSubItems.SetAt(iSubItem, iImage);
	}
	void SetSubItemText(int iSubItem, const CString& strSubItem, COLORREF clf)
	{
		m_SubItems.SetAESMrow(iSubItem, strSubItem);
		m_mapClf.SetAt(iSubItem, clf);
	}
	void SetSubItemImage(int iSubItem,int iImage)	{ m_iImageSubItems.SetAt(iSubItem, iImage);	}

	const CString& GetItemText(void)	{ return m_strItemName; }
	CString GetSubItem(int iSubItem)
	{ 
		if(iSubItem < m_SubItems.GetSize() && iSubItem >= 0)
			return m_SubItems.GetAt(iSubItem); 
		else
			return _T("");
	}
	int GetItemCount(void) const { return m_SubItems.GetSize(); }	
	//all cols in this row is default edit
	void SetControlType(CONTROLTYPE enumCtrlType, int nCol=-1){ m_controlType.SetAt(nCol, enumCtrlType); }
	BOOL GetControlType(int nCol, CONTROLTYPE& controlType) const
	{
		if(!m_controlType.Lookup(nCol,controlType))
		{
			controlType = edit;//default;
			return 0;
		}
		return 1;
	}

	void SetColumnProperty(int nProp, int nCol=-1){ m_ColumnProp.SetAt(nCol, nProp);}
	BOOL GetColumnProperty(int nCol, int &nProp) const
	{
		if(!m_ColumnProp.Lookup(nCol,nProp))
		{
			nProp = PROP_EDITABLE;//default;
			return 0;
		}
		return 1;
	}
	void SetListData(int iSubItem, CStringList *strInitArr)
	{
		CStringList *list;
		list = new CStringList;//will be deleted in destructor
		list->AddTail(strInitArr);
		m_listdata.SetAt(iSubItem, list);
	}	
	BOOL GetListData(int iSubItem, CStringList*& pList) const
	{	
		return m_listdata.Lookup(iSubItem, pList);
	}
	int GetDataType(void) const {return m_nProjectDataType;}
	void SetRunStatus(ESMA_STATUS nRunStatus, BOOL bForce = FALSE)
	{ 
		switch(m_nRunStatus)
		{
		case ESMA_STATUS_FAIL:
			if(bForce)
				m_nRunStatus = nRunStatus;
			break;
		case ESMA_STATUS_RUNNING:			
		case ESMA_STATUS_PASS:			
		case ESMA_STATUS_PAUSE:
		case ESMA_STATUS_READY:
		case ESMA_STATUS_STOP:
		case ESMA_STATUS_RECORDING:
		case ESMA_STATUS_CONDITION:
			m_nRunStatus = nRunStatus;
			break;
		}
	}

	ESMA_STATUS GetRunStatus(void) const {return m_nRunStatus;}
	
	#ifdef _ESMCC
		UINT	GetESMCCStatus(void) const {return m_ESMCCData. nStatus;}	
		void	SetESMCCStatus(UINT n) { m_ESMCCData. nStatus = n;}	
	#endif //_ESMCC

	void SetDataTypeWithImage(int iImage, int nDataType)
	{
		m_iImage			= iImage;
		m_nProjectDataType	= nDataType;		
	}	
	void SetDataType(int nDataType)	{ m_nProjectDataType = nDataType; }
	int GetImage(void) const { return m_iImage;}
	int GetSubItemImage(int iSubItem) const 
	{
		int iSubImage=-1;
		m_iImageSubItems.Lookup(iSubItem, iSubImage);
		return iSubImage;
	}

	COLORREF GetItemClr(void) const {return m_clf;}
	COLORREF GetBkColor(int iSubItem) const
	{
		COLORREF clref;
		if(!m_mapClf.Lookup(iSubItem,clref))
		{
			return (COLORREF)-1;
		}
		return clref;
	}

	////////////////////////////////////////////////////////////////////
	// RETURN VALUE
	//				TRUE	: 최단 Node. Child Node를 갖을 수 없음. 
	//				FALSE	: Child Node를 갖을 수 있다.
	////////////////////////////////////////////////////////////////////
	BOOL IsEndNode()
	{
		#ifdef _ESMCC
			return ( m_nProjectDataType == ESMCC_GROUP ? FALSE : TRUE );
		#endif //_ESMCC
		return FALSE;
	}

	//--
	//-- SET / GET CHECK
	void SetCheck(bool bCheck)
	{ 
		if(m_pTestBase)	
			m_pTestBase->SetExecuteOption(bCheck);	
	}

	bool GetCheck(void) 
	{		
		if(m_pTestBase) 
			return m_pTestBase->GetExecuteOption();	
		return FALSE;
	}

	//YOU SHOULD MODIFY THIS WHEN EVER YOU ADD NEW DATA TO THIS CLASS
	void CopyObjects(CItemInfo* pItemInfo)
	{
		SetItemText(pItemInfo->GetItemText());
		m_SubItems.Copy(pItemInfo->m_SubItems);
		CopyControls(pItemInfo);
		CopyColors(pItemInfo);
		SetCheck(pItemInfo->GetCheck());
		m_lParam = pItemInfo->m_lParam;
		m_clf=pItemInfo->m_clf;
		CopySubItemImageList(pItemInfo);

		CopyColumnProp(pItemInfo);
		SetRunStatus(pItemInfo->GetRunStatus());		
		
		//SetDataTypeWithImage(pItemInfo->GetImage());
		m_iImage			= pItemInfo->GetImage();
		m_nProjectDataType	= pItemInfo->GetDataType();

		m_wParam		= pItemInfo->m_wParam;
		m_lParam		= pItemInfo->m_lParam;
		m_nParamStatus	= pItemInfo->m_nParamStatus;
	}

	void CopyColumnProp(CItemInfo* pItemInfo)
	{
		POSITION pos = pItemInfo->m_ColumnProp.GetStartPosition();
		while(pos != NULL)
		{
			int nKey;
			int nProp;
			pItemInfo->m_ColumnProp.GetNextAssoc(pos, nKey, nProp);
			m_ColumnProp.SetAt(nKey, nProp);
		}
	}

	void CopySubItemImageList(CItemInfo* pItemInfo)
	{
		POSITION pos = pItemInfo->m_iImageSubItems.GetStartPosition();
		while(pos != NULL)
		{
			int nKey;
			int iImage=-1;
			pItemInfo->m_iImageSubItems.GetNextAssoc(pos, nKey, iImage);
			if(iImage!=-1)
				m_iImageSubItems.SetAt(nKey, iImage);
		}
	}

	void CopyControls(CItemInfo* pItemInfo)
	{
		for(int nCol=0; nCol < pItemInfo->GetItemCount(); nCol++)
		{
			CItemInfo::CONTROLTYPE ctrlType;
			if(pItemInfo->GetControlType(nCol, ctrlType))
			{				
				SetControlType(ctrlType, nCol);
				CStringList *list = NULL;
				pItemInfo->GetListData(nCol, list);
				if(list!=NULL)
					SetListData(nCol, list);	
			}
		}
	}

	void CopyColors(CItemInfo* pItemInfo)
	{
		POSITION pos = pItemInfo->m_mapClf.GetStartPosition();
		while(pos != NULL)
		{
			int nKey;
			COLORREF clref;
			pItemInfo->m_mapClf.GetNextAssoc(pos, nKey, clref);
			m_mapClf.SetAt(nKey, clref);
		}
	}

	~CItemInfo()
	{
		POSITION pos = m_listdata.GetStartPosition();
		while(pos != NULL)
		{
			int nKey;
			CStringList* b; 
			m_listdata.GetNextAssoc(pos, nKey, b);
			if(b!=NULL)
			{
				b->RemoveAll();
				delete b;
				b = NULL;
			}
		}

		m_listdata.RemoveAll();
		//-- DEBUG		
		int nAll = m_SubItems.GetSize();
		while(nAll--)
			m_SubItems.RemoveAt(nAll);
		m_SubItems.RemoveAll();
	}

public:
	
	//-------------------------------------------------------------------------------------------*
	// 이하의 변수는 parameter 용입니다.  항목의 설정값으로 사용하지 말아주세요.
	//-------------------------------------------------------------------------------------------*
	WPARAM				m_wParam;
	LPARAM				m_lParam;	
	ESMA_STATUS			m_nParamStatus;	
	//-------------------------------------------------------------------------------------------*


private:
	/////////////////////////////////////////////////////////////////////////////////
	// InitData, CopyObjects  
	/////////////////////////////////////////////////////////////////////////////////
	
	CMap<int,int, CONTROLTYPE, CONTROLTYPE&>	m_controlType;		//hmm
	CMap<int,int, COLORREF, COLORREF&>			m_mapClf;			//colors
	CMap<int,int, CStringList*, CStringList*>	m_listdata;			//listbox
	CMap<int,int, int, int&>					m_ColumnProp;		// 입력가능 유/무, 숫자Only입력, Result
	CMap<int,int,int,int&>						m_iImageSubItems;

	CONTROLTYPE		m_enumCtrlType; 
	COLORREF		m_clf;
	
	CString			m_strItemName;			
	CStringArray	m_SubItems;				//col 1... N
	int				m_iImage;	
		
	UINT			m_nTreeType;			// ESMExplore, TQMExplore, ESMCCExplore
	ESMA_STATUS		m_nRunStatus;			// 실행결과 : 미수행, 성공, 실패
	int				m_nProjectDataType;		// Project, Suite, case
};

typedef struct	TREEITEMLIST 
{
	CItemInfo*				pItem;
	CPtrList2				listChild;
	struct TREEITEMLIST*	pNext;	

} TREEITEMLIST;//, far *TREEITEMLIST*;


/////////////////////////////////////////////////////////////////////////////
//
// CESMGridCtrl 
//
/////////////////////////////////////////////////////////////////////////////
class CESMGridCtrl : public CListCtrl
{
	
// Construction
public:	
	CESMGridCtrl();
protected:
	//nested class forward decl.
	class CTreeItem;	

public:	
	// Overrides
	//MUST override this to make a copy of CItemInfo..see the CTCTreeCtrl.cpp for implementation
	//virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//used in drag/drop operations
	virtual CItemInfo* CopyData(CItemInfo* lpSrc);
	//sets the icon state...called from within DrawItem returns valid image index
	//You MUST override this function to set your current icon, must of course be a valid CImageList index
	virtual int GetIcon( CTreeItem* pItem);	
	//override this to set the color for current cell
	virtual COLORREF GetCellRGB(void);
	//override this to update listview items, called from within OnEndlabeledit.
	virtual void OnUpdateListViewItem(CTreeItem* lpItem, LV_ITEM *plvItem);
	//override this to activate any control when lButtonDown in a cell, called from within OnLButtonDown
	virtual void OnControlLButtonDown(UINT nFlags, CPoint point, LVHITTESTINFO& ht);
	//override this to provide your dragimage, default: creates an image + text
	virtual CImageList *CreateDragImageEx(int nItem);
    //called before item is about to explode, return TRUE to continue, FALSE to prevent expanding
	virtual BOOL OnItemExpanding(CTreeItem *pItem, int iItem);
	//called after item has expanded
	virtual BOOL OnItemExpanded(CTreeItem* pItem, int iItem);
	//called before item are collapsed,return TRUE to continue, FALSE to prevent collapse
	virtual BOOL OnCollapsing(CTreeItem *pItem);
	//called after item has collapsed
	virtual BOOL OnItemCollapsed(CTreeItem *pItem);
	//called before item is about to be deleted,return TRUE to continue, FALSE to prevent delete item
	virtual BOOL OnDeleteItem(CTreeItem* pItem, int nIndex);
	//called before VK_MULTIPLY keydown, return TRUE to continue, FALSE to prevent expandall
	virtual BOOL OnVKMultiply(CTreeItem *pItem, int nIndex);
	//called before VK_SUBTRACT keydown, return TRUE to continue, FALSE to prevent collapse item
	virtual BOOL OnVkSubTract(CTreeItem *pItem, int nIndex);
	//called before VK_ADD keydown, return TRUE to continue, FALSE to prevent expanding item
	virtual BOOL OnVKAdd(CTreeItem *pItem, int nIndex);
	//called from PreTranslateMessage, override this to handle other controls than editctrl's
	virtual BOOL OnVkReturn(void);
	//called before from within OnlButtonDown and OnDblclk, but before anything happens, return TRUE to continue, FALSE to say not selecting the item
	virtual BOOL OnItemLButtonDown(LVHITTESTINFO& ht);
	// ADD
	virtual void	NotifyDoubleClick(BOOL bFirst = FALSE);
	virtual int		GetLowestLevel();
	virtual void	ShowPopupMenu		(CPoint point);
	virtual BOOL	GetTextProp			(CTreeItem* pItem, int nCol, COLUMN_TEXT_PROP& textProp);	
	virtual BOOL	CheckDropPosition	(CTreeItem* pTarget, CTreeItem* pSource);	

public:
	//creates a root
	CTreeItem*  InsertRootItem(CItemInfo * lpRoot, short nAddType=ADD_TAIL);
	//from the looks of it,...it deletes a rootitem
	void DeleteRootItem(CTreeItem * lpRoot);
	//hmm
	BOOL IsRoot(CTreeItem * lpItem);
	//given the rootindex it returns the rootptr
	CTreeItem* GetRootItem(int nIndex);
	int GetRootCount() { return m_RootItems.GetCount();}
	//call this to delete all items in grid
	void DeleteAll();
	//return previous node from pItem, given a RootItem
	CTreeItem* GetPrev(CTreeItem* pRoot, CTreeItem *pItem, BOOL bInit = TRUE, BOOL bDontIncludeHidden=TRUE);
	//return next node from pItem, given a RootItem
	CTreeItem* GetNext(CTreeItem* pRoot, CTreeItem* pNode, BOOL bInit = TRUE, BOOL bDontIncludeHidden=TRUE);
	//returns the selected item :)
	int GetSelectedItem(void) const;
	//returns the itemdata associated with the supergrid
	CTreeItem* GetTreeItem(int nIndex);
	// Retrieves the number of items associated with the SuperGrid control.
	UINT GetCount(void);
	//returns number of children given the pItem node ptr.
	int NumChildren(const CTreeItem *pItem) const;
	//Determines if this tree item is a child of the specified parent
	BOOL IsChildOf(const CTreeItem* pParent, const CTreeItem* pChild) const;
	//hmm
	BOOL ItemHasChildren(const CTreeItem* pItem) const;
	// Use this to indicate that pItem has children, but has not been inserted yet.
	void SetChildrenFlag(CTreeItem *pItem,int nFlag) const;
	//are children collapsed
	BOOL IsCollapsed(const CTreeItem* pItem) const;
	//return the Indent Level of pItem
	int GetIndent(const CTreeItem* pItem) const;
	//Sets the Indentlevel of pItem
	void SetIndent(CTreeItem *pItem, int iIndent);
	//get the pItems current listview index, 
	int GetCurIndex(const CTreeItem *pItem) const;
	//set pItems current listview index
	void SetCurIndex(CTreeItem* pItem, int nIndex);
	//sets the pItem' parent
	void SetParentItem(CTreeItem*pItem, CTreeItem* pParent);
	//gets pItems parent
	CTreeItem* GetParentItem(const CTreeItem* pItem); 
	//return ptr to CItemInfo daaa
	CItemInfo* GetData(const CTreeItem* pItem); 
	//sets the CItemInfo ptr of pItem, if bUpdateRow true, it uses the InvalidateItem(CTreeItem pItem) 
	void UpdateData(CTreeItem* pItem, CItemInfo* lpInfo, BOOL bUpdateRow=FALSE); 
	//-- 2008-07-03
	void UpdateData();
	//Insert item and return new parent node.
	CTreeItem* InsertItem(CTreeItem *pParent, CItemInfo* lpInfo, BOOL bUpdate=FALSE, short nAddType=ADD_TAIL, POSITION pos = NULL);
	//collapse all children from pItem
	void Collapse(CTreeItem *pItem);
	//expand one folder and return the last index of the expanded folder
	int Expand(CTreeItem* pSelItem, int nIndex);
	//expand all items from pItem and return last index of the expanded folder
	void ExpandAll(CTreeItem *pItem, int& nScroll);	
	//-- 2008-06-14
	//expand all items 
	void ExpandAll();
	//expand all node in pItem and stop at pStopAt, used in SelectNode function
	void ExpandUntil(CTreeItem *pItem, CTreeItem* pStopAt);
	//use this if you want to select a node 
	int SelectNode(CTreeItem *pLocateNode);
	//Delete an item in the listview 
	void DeleteItemEx(CTreeItem *pSelItem, int nItem);	
	//-- 2008-07-01
	//-- DELETE SELECTED ITEM
	void DeleteSelectedItem();
	//returns the number of columns in the listview
	int GetNumCol(void);
	//-- MOVE FROM TCCONTROL
	//-- 2008-07-01
	CTreeItem*	Search (CString strItem);
	CTreeItem*	SearchEx (CTreeItem *pStartPosition, CString strItem);
	void SortData();
	//-- GET SELECTED ITEM
	//-- 2008-07-01
	CTreeItem* GetSelectedTreeItem();
	BOOL IsExistRoot(CString strRootItem);
	//-- COPY CLONE
	//-- 2008-07-03
	//virtual void CopyTree(CESMGridCtrl* pClone) {};
	//-- 2008-07-11
	void CleanAll();
	//does a Quicksort of the rootitems and all children if bSortChildren set TRUE
	void SortEx(BOOL bSortChildren);
	//does a Quicksort of the pParents children and if bSortChildren set, 
	//all items from pParent are sorted. 
	void Sort(CTreeItem* pParent, BOOL bSortChildren);
	// simpel wrapper for the CObList in CTreeItem, same usage as in the COblist
	POSITION GetHeadPosition(CTreeItem* pItem) const;
	POSITION GetTailPosition(CTreeItem *pItem) const;
	CTreeItem* GetNextChild(CTreeItem *pItem, POSITION& pos) const;
	CTreeItem* GetPrevChild(CTreeItem *pItem, POSITION& pos) const;
	void AddTail(CTreeItem *pParent, CTreeItem *pChild);
	//simpel wrapper for the CPtrList collection of rootitems
	//feel free to add more of these simple wrappers.
	POSITION GetRootHeadPosition(void) const;
	POSITION GetRootTailPosition(void) const;
	CTreeItem* GetNextRoot(POSITION& pos) const;
	CTreeItem* GetPrevRoot(POSITION& pos) const;

	//---------------------------------
	// ADD++
	//---------------------------------	
	void	InvalidateItemRect(int nItem);				//invalidates the nItems rectbound.	
	void	InvalidateItemRectPtr(CTreeItem *pItem);	//invalidates the pItem rectbound.	
	void	InvalidateItem(CTreeItem *pItem);			//use this when you do dynamic updates, it writes the content of pItem to the listview	
	void	SetCheckTopDown(CTreeItem* pItem, bool bCheck);
	void	SetCheckDownUp(CTreeItem* pItem);
	//-- 2008-06-30
	CString GetFileName(CString strFullPath);	

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMGridCtrl)
	public:
	//handle VK_xxxx
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL
// Implementation
public:
	virtual ~CESMGridCtrl();

protected:
	//{{AFX_MSG(CESMGridCtrl)
	afx_msg int OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDblclk			(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEndlabeledit		(NMHDR* pNMHDR, LRESULT* pResult);	
	afx_msg void OnHScroll			(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll			(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);	
	afx_msg void OnLButtonDown		(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp		(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove		(UINT nFlags, CPoint point);
	afx_msg void OnKeydown			(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTimer			(UINT nIDEvent);
	afx_msg void OnBegindrag		(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void MeasureItem		(LPMEASUREITEMSTRUCT lpMeasureItemStruct);	
	afx_msg void DrawItem			(LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnSysColorChange	();
	afx_msg void OnRButtonDown		(UINT nFlags, CPoint point);
	afx_msg void OnHeaderNotify		(NMHDR *pNMHDR, LRESULT *pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


protected:
	//given the rootptr it returns the rootindex.
	int GetRootIndex(CTreeItem * lpRoot);
	//delete pItem and all of its children
	BOOL Delete(CTreeItem *pItem, BOOL bClean=TRUE/*delete itemdata*/);
	//drag/drop thing....clipboard not supported!
	BOOL DoDragDrop(CTreeItem* pTarget, CTreeItem* pSelItem);
	//updates internal nodes, called when ever insert,delete on listview
	void InternaleUpdateTree(void);
	//Get ListView Index from pNode
	int NodeToIndex(CTreeItem *pNode);
	//see if user clicked the [+] [-] sign, also handles LVS_EX_CHECKBOXES.
	BOOL HitTestOnSign(CPoint point, LVHITTESTINFO& ht);
	//positions an edit-control and creates it
	CEdit* EditLabelEx(int nItem, int nCol);
	int m_cxImage, m_cyImage;//icon size
	//translates column index value to a column order value.
	int IndexToOrder(int iIndex);
	//set hideflag for pItem
	void Hide(CTreeItem* pItem, BOOL bFlag);
	//hmm
	int GetCurSubItem(void){return m_CurSubItem;}
	int GetDragItem(void) const {return m_nDragItem; }
	int GetDropTargetItem(void) const {return m_nDragTarget; }

	// ADD++
	BOOL SetSubItemImageList(CImageList *pImageList=NULL)
	{
		if(pImageList!=NULL)
		{
			m_iSubItemImage.Create(pImageList);
			int nCount=pImageList->GetImageCount(); 
			for (int i=0; i < nCount; i++)
				 m_iSubItemImage.Copy(i,pImageList,i);

			return TRUE;
		}
		else return FALSE;
	}

	void DragProc();

	//hmm
	void DrawTreeItem(CDC* pDC, CTreeItem* pSelItem, int nListItem, const CRect& rcBounds);

	//internal helpers
	BOOL _Delete(CTreeItem* pStartAt, CTreeItem *pItem, BOOL bClean=TRUE/*delete itemdata*/);
	UINT _GetCount(CTreeItem* pItem, UINT& nCount);	

	//makes the dot ... thing
	LPCTSTR MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset);

	//set the hideflag from pItem and all its children
	void HideChildren(CTreeItem *pItem, BOOL bHide, int iIndent);	

	//checks whether a column is visible, if not scrolls to it
	void MakeColumnVisible(int nCol);	

	//-- FOCUSSING CELL
	void DrawFocusCell(CDC *pDC, int nItem, int iSubItem, int nIconOffset=0);	

	//--draw the down arrow if any combobox precent in the listview item
	void DrawComboBox(CDC *pDC, CTreeItem *pSelItem, int nItem, int nColumn, int iSubIconOffset = 0);	 	

	//helper compare fn used with Quicksort
	static int CompareChildren(const void* p1, const void* p2);
	void	CleanMe(CTreeItem *pItem);
	int		_NodeToIndex(CTreeItem *pStartpos, CTreeItem* pNode, int& nIndex, BOOL binit = TRUE);

	BOOL	SortListItem(CPtrList2& TreeItems, CTreeItem* pParent = NULL, short bAscending = -9);
	BOOL	SortListItemSub( TREEITEMLIST* pParentSortedList, CTreeItem* pParent );
	TREEITEMLIST* GetSortResult(CPtrList2 TreeItems, BOOL bRoot);


public:
	//used in drag/drop operation..
	void	CopyChildren(CTreeItem* pDest, CTreeItem* pSrc);
	void	SetParentWnd(HWND hWnd){m_hParentWnd=hWnd;};
	void	AddFunction(int nType);
	int		GetFunction(){return m_nAddFunction;};
	HWND	m_hParentWnd;	

	afx_msg void OnClose();

public:
	CPtrList2 		m_RootItems;

protected:
	CImageList		m_iSubItemImage;	//imagelist for subitems	
	CPen			m_psTreeLine;
	CPen			m_psRectangle;
	CPen			m_psPlusMinus;
	CBrush			m_brushErase;
	friend class	CRectangle;
	HIMAGELIST		m_himl;
	BOOL			m_bIsDragging;	
    int				m_nDragItem, m_nDragTarget;   //hmm these represents the stat of my expresso machine
	int				m_CurSubItem;		//keyboard handling..it is what it says
	int				m_nAddFunction;		// DragDrop 기능 유/무
	
};


class CRectangle
{
public:
	CRectangle(CESMGridCtrl* pCtrl, CDC* pDC, int iIndent, const CRect& rcBounds);
	~CRectangle();
	void DrawRectangle(CESMGridCtrl *pCtrl);
	BOOL HitTest(CPoint pt);
	void DrawPlus(void);
	void DrawMinus(void);
	int GetLeft(void){return m_left;}
	int GetTop(void){return m_top;}
	CRect GetHitTestRect(void) { return CRect(m_left_top, m_right_bottom);}
private:
	CDC *m_pDC;
	SIZE m_right_bottom;
	int m_left;
	int m_top;
	POINT m_left_top;
	int m_topdown;
};

class CESMGridCtrl::CTreeItem : public CObject
{
	CTreeItem(): m_pParent(NULL), m_bHideChildren(1), m_nIndex(-1), m_nIndent(-1),m_bSetChildFlag(-1),m_lpNodeInfo(NULL){};
	~CTreeItem();
	
	BOOL			m_bHideChildren;  
	int				m_nIndex; //CListCtrl index
	int				m_nIndent; 
	int				m_bSetChildFlag;
	friend class	CESMGridCtrl;

public:
	CPtrList2		m_listChild;
	CTreeItem*		m_pParent;
	CItemInfo*		m_lpNodeInfo;
	int				GetIndex()	{	return m_nIndex; }
};
