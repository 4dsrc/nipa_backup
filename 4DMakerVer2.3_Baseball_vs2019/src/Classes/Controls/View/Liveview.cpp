/////////////////////////////////////////////////////////////////////////////
//
//  Liveview.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudioDlg.h"
#include "SRSIndex.h"
#include "Liveview.h"


IMPLEMENT_DYNCREATE(CLiveview, CView)

CLiveview::CLiveview(){}
CLiveview::CLiveview(int nDevice, BOOL bMain, int nType)
{
	m_nDeviceID = nDevice;
	m_nShift = SHIFT_0;	
	m_bLiveview = FALSE;
	m_bMainView = bMain;
	//-- 2011-06-22 hongsu.jung
	m_bDrawZoomPos	= FALSE;
	m_bSetZoomPos		= FALSE;
	m_bZoomMove		= FALSE;
	m_ptZoom = CPoint(0,0);
	m_szZoom	= CSize(0,0);
	m_nZoomSize	= ZOOM_1_4;	
	m_nAFFocus		= AF_NULL;
	m_nLiveviewType = nType;
	m_colorAF = Color(255,0,0,255);
	m_bIsSupported = TRUE;
}

CLiveview::~CLiveview(){}

BEGIN_MESSAGE_MAP(CLiveview, CView)	
	ON_WM_PAINT()
	ON_WM_SIZE()		
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

// CLiveview diagnostics
#ifdef _DEBUG
void CLiveview::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CLiveview::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG

void CLiveview::OnPaint()
{	
	DrawLiveview();
	CView::OnPaint();
}

void CLiveview::OnSize(UINT nType, int cx, int cy)
{
	//-- 2011-06-22 hongsu.jung
	if(m_bDrawZoomPos)
	{
 		UpdatePosition(cx, cy);
		GetClientRect(m_rect);
	}
	CView::OnSize(nType, cx, cy);	
}

CSize CLiveview::GetSrcSize()
{
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	return pMainWnd->GetSrcSize(m_nDeviceID);	
}

CSize CLiveview::GetOffsetSize()
{
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	return pMainWnd->GetOffsetSize(m_nDeviceID);	
}

//------------------------------------------------------------------------------ 
//! @brief		IsLiveviewOn
//! @date		2010-05-19
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
BOOL CLiveview::IsLiveviewOn()
{
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	IplImage *pImgSrc = pMainWnd->GetLiveviewBuffer(m_nDeviceID);
	if(pImgSrc && m_bLiveview)
		return TRUE;
	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-19
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveview::DrawLiveview()
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);	
	
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), BLACKNESS);

	
	//-- Get Liveview
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	IplImage *pImgSrc = pMainWnd->GetLiveviewBuffer(m_nDeviceID);
	

	if(!pImgSrc || !m_bLiveview || !m_bIsSupported)
	{
#ifndef	IFA
		//-- 2011-9-2 Lee JungTaek
		//-- For IFA
		if(!m_bIsSupported)
		{
			Rect		DesRect;
			DesRect.X = rect.left;
			DesRect.Y = rect.top;
			DesRect.Width = rect.right - rect.left;
			DesRect.Height = rect.bottom - rect.top;
			Graphics gc(mDC.GetSafeHdc());
			
			SolidBrush S = RGB_BLACK;
			gc.FillRectangle(&S,DesRect);
			dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
			return;
		}
#endif
		
		CString strFile;
		strFile =RSGetRoot(_T("img\\03.Live View\\liveview_bk.png"));

		Image Img(strFile);
		{
			Rect		DesRect;
			DesRect.X = rect.left;
			DesRect.Y = rect.top;
			DesRect.Width = rect.right - rect.left;
			DesRect.Height = rect.bottom - rect.top;
			Graphics gc(mDC.GetSafeHdc());
			gc.DrawImage(&Img, DesRect);			

		
			if(!m_bIsSupported)
			{
				dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
				return;
			}

			switch(m_nLiveviewType)
			{
			case LIVEVIEW_TYPE_MAINFRM		:	strFile =RSGetRoot(_T("img\\03.Live View\\01_lightstudio_NC_thumbnail.png"));		break;
			case LIVEVIEW_TYPE_MULTIFRM		:	strFile =RSGetRoot(_T("img\\03.Live View\\03_Multi_frame_nc_image.png"));		break;
			case LIVEVIEW_TYPE_LIVEVIEWFRM	:	strFile =RSGetRoot(_T("img\\03.Live View\\03_liveview_nc_bg.png"));		break;
			}

			Image ImgIcon(strFile);

			DesRect.X = (rect.left + rect.right - ImgIcon.GetWidth()) / 2;
			DesRect.Y = (rect.top + rect.bottom - ImgIcon.GetHeight()) / 2;
			DesRect.Width = ImgIcon.GetWidth();
			DesRect.Height = ImgIcon.GetHeight();

			Graphics gcIcon(mDC.GetSafeHdc());
			gcIcon.DrawImage(&ImgIcon, DesRect);				
		}
		
		dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
		return;
	}

	try{
		CvvImage cImage;
		//-- 2011-07-15 hongsu.jung
		//-- Cut Offset Data
		if(m_bMainView)
		{
 			CSize szOffset	= GetOffsetSize();
 			//-- Cut Image

 			if	(szOffset.cx > 0)			
 				cvSetImageROI(pImgSrc, cvRect( szOffset.cx + 4, 0, pImgSrc->width-2*szOffset.cx - 4, pImgSrc->height));
 			else if (szOffset.cy > 0)		
 				cvSetImageROI(pImgSrc, cvRect( 0, szOffset.cy, pImgSrc->width, pImgSrc->height - 2*szOffset.cy ));
 			else							
 				cvSetImageROI(pImgSrc, cvRect( 0, 0, pImgSrc->width, pImgSrc->height ));
		}
		else
			cvSetImageROI(pImgSrc, cvRect( 0, 0, pImgSrc->width, pImgSrc->height ));

		//--
		//-- Sub Liveviews
		if(!m_bMainView)
		{
			IplImage *pImgResize		= NULL;
			pImgResize = cvCreateImage( cvSize(rect.Width(),rect.Height()),pImgSrc->depth, pImgSrc->nChannels );
			cvResize(pImgSrc, pImgResize);

			//-- Copy To CImage
			cImage.CopyOf(pImgResize,8);		
			cImage.Show(mDC.GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height() );						
			//memory ����
			cvReleaseImage(&pImgResize);
		}
		//--
		//-- Main Liveview
		else
		{
			//-- Draw Liveview
			CvvImage cImage;	
			IplImage *pImgRotated	= NULL;
			IplImage *pImgResize		= NULL;

			//-- Rotate
			if(m_nShift)
				pImgRotated = rotateImage(pImgSrc,  m_nShift);
			else
				pImgRotated = pImgSrc;

			pImgResize = cvCreateImage( cvSize(rect.Width(),rect.Height()),pImgSrc->depth, pImgSrc->nChannels );
			cvResize(pImgRotated, pImgResize );

			//-- Copy To CImage
			cImage.CopyOf(pImgResize ,8);
			cImage.Show(mDC.GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height() );

			//-- 2011-06-22 hongsu.jung
			//-- Draw Grid Line (Draw png file)
			DrawGrid(&mDC, rect);

			//-- 2011-06-22 hongsu.jung
			//-- Draw Zoom Position
			DrawZoomPosition(&mDC, rect);

			//-- 2011-07-22 hongsu.jung
			//-- Draw Focus Rectangle
			DrawFocus(&mDC, rect);
	
			//-- DrawZoomPanel	
			if(m_bDrawZoomPos && GetZoomOpt())
			{
				if(pMainWnd->m_pDlgZoom->IsWindowVisible())
				{
					//-- Get Zoom Position
					cvSetImageROI(pImgResize, cvRect(m_ptZoom.x - m_szZoom.cx/2, m_ptZoom.y - m_szZoom.cy/2,  m_szZoom.cx, m_szZoom.cy));			
					pMainWnd->m_pDlgZoom->GetZoomView()->CopyImage(pImgResize);
				}
			}
			//memory ����
			cvReleaseImage(&pImgResize);	
			if(m_nShift)
				cvReleaseImage(&pImgRotated);	
		}
		cImage.Destroy();
	}catch(cv::Exception &e){
		TRACE(_T("\n\ncv::Exception [%d] - line %d : %s\n\n"), e.code, e.line, e.err);
	}
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		rotateImage(const IplImage *src, int nShift)
//! @date		2010-05-19
//! @author	hongsu.jung
//! @note	 	Rotate the image clockwise (or counter-clockwise if negative).
//!				Remember to free the returned image.
//------------------------------------------------------------------------------ 
IplImage* CLiveview::rotateImage(IplImage *src, int nShift)
{
	// Create a map_matrix, where the left 2x2 matrix
	// is the transform and the right 2x1 is the dimensions.
	float m[6];
	CvMat M = cvMat(2, 3, CV_32F, m);
	int w,h;
	float angleDegrees;
	//-- Rotate
	switch(nShift)
	{
	case 	SHIFT_180:	
		w = src->width;
		h = src->height;
		m[2] = w*0.5f;  
		m[5] = h*0.5f;  
		angleDegrees = 180;
		break;		
	case 	SHIFT_90:		
		w = src->height;
		h = src->width;
		m[2] = w*0.76f;  
		m[5] = h*0.35f;  
		angleDegrees = 90;
		break;
	case 	SHIFT_270:
		w = src->height;
		h = src->width;
		m[2] = w*0.76f;  
		m[5] = h*0.35f;  
		angleDegrees = 270;
		break;
	case	SHIFT_0:		
	default:
		return src;
	}

	float angleRadians = angleDegrees * ((float)CV_PI / 180.0f);
	m[0] = (float)( cos(angleRadians) );
	m[1] = (float)( sin(angleRadians) );	
	m[3] = -m[1];
	m[4] = m[0];		

	// Make a spare image for the result
	CvSize sizeRotated;
	sizeRotated.width = cvRound(w);
	sizeRotated.height = cvRound(h);

	// Rotate
	IplImage *imageRotated = cvCreateImage( sizeRotated, src->depth, src->nChannels );
	// Transform the image
	cvGetQuadrangleSubPix( src, imageRotated, &M);	
	return imageRotated;
}

//------------------------------------------------------------------------------ 
//! @brief		RotateRect(const IplImage *src, int nShift)
//! @date		2010-05-19
//! @author	hongsu.jung
//! @note	 	Rotate the image clockwise (or counter-clockwise if negative).
//!				Remember to free the returned image.
//------------------------------------------------------------------------------ 
CRect CLiveview::RotateRect(CRect rect)
{
	CRect tempRect;
	tempRect.top			= rect.top;
	tempRect.left			= rect.left;
	tempRect.bottom	= rect.right;
	tempRect.right		= rect.bottom;
	return tempRect;
}

//------------------------------------------------------------------------------ 
//! @brief		DrawGrid(CDC* pMemDC, CRect rect)
//! @date		2010-06-22
//! @Modify		2010-08-16	Lee JungTaek - Apply Alpha Blending
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveview::DrawGrid(CDC* pMemDC, CRect rect)
{
	Graphics gc(pMemDC->GetSafeHdc());
	Pen pen(Color(70, 0,0,0), 1);
	
	switch(GetGridOpt())
	{
	case GRID_PLUS:
		{		
			int nX[3], nY[3];
			nX[0] = rect.left;
			nX[1] = nX[0] + rect.Width()/2;
			nX[2] = rect.right;
			nY[0] = rect.top;
			nY[1] = nY[0] + rect.Height()/2;
			nY[2] = rect.bottom;
			//-- 1 vertical line
			gc.DrawLine(&pen, nX[1],nY[0], nX[1],nY[2]);
			//-- 1 horizontal line
			gc.DrawLine(&pen, nX[0],nY[1], nX[2],nY[1]);			
		}
		break;
 	case GRID_2X2:
		{
			//-- Pen Setting
			int nX[4], nY[4];
			nX[0] = rect.left;
			nX[1] = nX[0] + rect.Width()/3;
			nX[2] = nX[1] + rect.Width()/3;
			nX[3] = rect.right;
			nY[0] = rect.top;
			nY[1] = nY[0] + rect.Height()/3;
			nY[2] = nY[1] + rect.Height()/3;
			nY[3] = rect.bottom;
			//-- 2 vertical line
			gc.DrawLine(&pen, nX[1],nY[0], nX[1],nY[3]);
			gc.DrawLine(&pen, nX[2],nY[0], nX[2],nY[3]);
			//-- 2 horizontal line
			gc.DrawLine(&pen, nX[0],nY[1], nX[3],nY[1]);
			gc.DrawLine(&pen, nX[0],nY[2], nX[3],nY[2]);
		}
		break;
 	case GRID_3X3:
 		{
 			//-- Pen Setting
 			int nX[5], nY[5];
 			nX[0] = rect.left;
 			nX[1] = nX[0] + rect.Width()/4;
 			nX[2] = nX[1] + rect.Width()/4;
 			nX[3] = nX[2] + rect.Width()/4;
 			nX[4] = rect.right;
 			nY[0] = rect.top;
 			nY[1] = nY[0] + rect.Height()/4;
 			nY[2] = nY[1] + rect.Height()/4;
 			nY[3] = nY[2] + rect.Height()/4;
 			nY[4] = rect.bottom;
 			//-- 3 vertical line
			gc.DrawLine(&pen, nX[1],nY[0], nX[1],nY[4]);
			gc.DrawLine(&pen, nX[2],nY[0], nX[2],nY[4]);
			gc.DrawLine(&pen, nX[3],nY[0], nX[3],nY[4]);
			//-- 3 horizontal line
			gc.DrawLine(&pen, nX[0],nY[1], nX[4],nY[1]);
			gc.DrawLine(&pen, nX[0],nY[2], nX[4],nY[2]);
			gc.DrawLine(&pen, nX[0],nY[3], nX[4],nY[3]);
		}
 		break;	
 	case GRID_7X7:
 		{
 			//-- Pen Setting
 			int nX[5], nY[5];
 			nX[0] = rect.left;
 			nX[1] = nX[0] + rect.Width()/4;
 			nX[2] = nX[1] + rect.Width()/4;
 			nX[3] = nX[2] + rect.Width()/4;
 			nX[4] = rect.right;
 			nY[0] = rect.top;
 			nY[1] = nY[0] + rect.Height()/4;
 			nY[2] = nY[1] + rect.Height()/4;
 			nY[3] = nY[2] + rect.Height()/4;
 			nY[4] = rect.bottom;
 			//-- 3 vertical line
			gc.DrawLine(&pen,nX[1],nY[0], nX[1],nY[4]);
			gc.DrawLine(&pen,nX[2],nY[0], nX[2],nY[4]);
			gc.DrawLine(&pen,nX[3],nY[0], nX[3],nY[4]);
 			//-- 3 horizontal line
			gc.DrawLine(&pen,nX[0],nY[1], nX[4],nY[1]);
			gc.DrawLine(&pen,nX[0],nY[2], nX[4],nY[2]);
			gc.DrawLine(&pen,nX[0],nY[3], nX[4],nY[3]);
 		
			//-- Dot Line
			pen.SetDashStyle(DashStyleDot);

 			//-- 4 vertical line
			gc.DrawLine(&pen,nX[0] + rect.Width()/8 ,nY[0], nX[0] + rect.Width()/8,nY[4]);
			gc.DrawLine(&pen,nX[1] + rect.Width()/8 ,nY[0], nX[1] + rect.Width()/8,nY[4]);
			gc.DrawLine(&pen,nX[2] + rect.Width()/8 ,nY[0], nX[2] + rect.Width()/8,nY[4]);
			gc.DrawLine(&pen,nX[3] + rect.Width()/8 ,nY[0], nX[3] + rect.Width()/8,nY[4]);
			//-- 4 horizontal line
			gc.DrawLine(&pen,nX[0],nY[0] + rect.Height()/8, nX[4],nY[0] + rect.Height()/8);
			gc.DrawLine(&pen,nX[0],nY[1] + rect.Height()/8, nX[4],nY[1] + rect.Height()/8);
			gc.DrawLine(&pen,nX[0],nY[2] + rect.Height()/8, nX[4],nY[2] + rect.Height()/8);
			gc.DrawLine(&pen,nX[0],nY[3] + rect.Height()/8, nX[4],nY[3] + rect.Height()/8);

 			pMemDC->MoveTo(nX[0],nY[0] + rect.Height()/8);		pMemDC->LineTo(nX[4],nY[0] + rect.Height()/8);
 			pMemDC->MoveTo(nX[0],nY[1] + rect.Height()/8);		pMemDC->LineTo(nX[4],nY[1] + rect.Height()/8);
 			pMemDC->MoveTo(nX[0],nY[2] + rect.Height()/8);		pMemDC->LineTo(nX[4],nY[2] + rect.Height()/8);
 			pMemDC->MoveTo(nX[0],nY[3] + rect.Height()/8);		pMemDC->LineTo(nX[4],nY[3] + rect.Height()/8);
  		}
 		break;
 	case GRID_X:
 		{
 			//-- Pen Setting
			//-- Draw Solid Line
			gc.DrawLine(&pen, rect.left,rect.top, rect.right,rect.bottom);
			gc.DrawLine(&pen, rect.left,rect.bottom, rect.right,rect.top);
 			
  			//-- Dot Line
			pen.SetDashStyle(DashStyleDot);

			int nX[5], nY[5];
			nX[0] = rect.left;
			nX[1] = nX[0] + rect.Width()/4;
			nX[2] = nX[1] + rect.Width()/4;
			nX[3] = nX[2] + rect.Width()/4;
			nX[4] = rect.right;
			nY[0] = rect.top;
			nY[1] = nY[0] + rect.Height()/4;
			nY[2] = nY[1] + rect.Height()/4;
			nY[3] = nY[2] + rect.Height()/4;
			nY[4] = rect.bottom;

			//-- 3 vertical line
			gc.DrawLine(&pen, nX[1],nY[0], nX[1],nY[4]);
			gc.DrawLine(&pen, nX[2],nY[0], nX[2],nY[4]);
			gc.DrawLine(&pen, nX[3],nY[0], nX[3],nY[4]);
			//-- 3 horizontal line
			gc.DrawLine(&pen, nX[0],nY[1], nX[4],nY[1]);
			gc.DrawLine(&pen, nX[0],nY[2], nX[4],nY[2]);
			gc.DrawLine(&pen, nX[0],nY[3], nX[4],nY[3]);
 		}
 		break;
	default:		
		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		DrawFocus(CDC* pMemDC, CRect rect)
//! @date		2010-07-22
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveview::DrawFocus(CDC* pMemDC, CRect rect)
{
	//-- 2011-8-28 Lee JungTaek
	//-- Modify
	pMemDC->SetBkMode(TRANSPARENT);	
	int nFocusOpt = GetAFFocus();

	Graphics gc(pMemDC->GetSafeHdc());
	Pen pen(m_colorAF, 2);

	switch(nFocusOpt)
	{
	case AF_DETECT:				m_colorAF = Color(255,0,255,0);		break;
	case AF_FAIL:				m_colorAF = Color(255,255,0,0);		break;
	case AF_CANCEL:				
	case AF_FAIL_CONTINOUSAF:	m_colorAF = Color(255,255,255,255);		break;
	case AF_NULL:				m_colorAF = Color(0,0,0,0);		break;
	}

	CPoint pt = rect.CenterPoint();
	//-- Get Rect Position
	CRect rt = CRect(CPoint(pt.x - rect.Width()/16, pt.y - rect.Height()/12), CSize(rect.Width()/8, rect.Height()/6));
	
	//-- TopLeft
	gc.DrawLine(&pen, rt.left , rt.top, rt.left + rt.Height() / 10, rt.top);
	gc.DrawLine(&pen, rt.left , rt.top, rt.left, rt.top + rt.Height() / 10);
	
	//-- BottomLeft
	gc.DrawLine(&pen, rt.left, rt.bottom, rt.left, rt.bottom - rt.Height()/10);
	gc.DrawLine(&pen, rt.left, rt.bottom, rt.left + rt.Height()/10, rt.bottom);

	//-- TopRight
	gc.DrawLine(&pen, rt.right, rt.top, rt.right - rt.Height()/10 ,rt.top);
	gc.DrawLine(&pen, rt.right, rt.top, rt.right, rt.top + rt.Height()/10);

	//-- BottomRight
	gc.DrawLine(&pen, rt.right, rt.bottom, rt.right, rt.bottom - rt.Height()/10);
	gc.DrawLine(&pen, rt.right, rt.bottom, rt.right - rt.Height()/10, rt.bottom);
}
//------------------------------------------------------------------------------ 
//! @brief		DrawZoomPosition(CDC* pMemDC, CRect rect)
//! @date		2010-06-22
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveview::DrawZoomPosition(CDC* pMemDC, CRect rect)
{
	if(!m_bDrawZoomPos)
		return;

	if(!m_bSetZoomPos)
	{
		m_ptZoom = rect.CenterPoint();
		GetClientRect(m_rect);
		m_bSetZoomPos = TRUE;
	}

	//-- Change Size
	switch(GetZoomOpt())
	{
	case ZOOM_1_2:	m_nZoomSize = 2; break;
	case ZOOM_1_3:	m_nZoomSize = 3; break;
	case ZOOM_1_4:	m_nZoomSize = 4; break;	
	case ZOOM_OFF	:	
	default:
		return;	
	}
	m_szZoom = CSize(rect.Width()/m_nZoomSize, rect.Height()/m_nZoomSize);
	
	//-- 2011-06-23
	//-- Check Position
	SetPtZomm(m_ptZoom);

	CRect rt;
	rt.left		= m_ptZoom.x - m_szZoom.cx/2;
	rt.right	= m_ptZoom.x + m_szZoom.cx/2;
	rt.top		= m_ptZoom.y - m_szZoom.cy/2;
	rt.bottom	= m_ptZoom.y + m_szZoom.cy/2;

	CPen newPen;
	CPen *oldPen;
	if(m_bZoomMove)
	{
		newPen.CreatePen(PS_SOLID, 3, RGB(255,0,0));		
		oldPen=pMemDC->SelectObject(&newPen);
	}
	else
	{
		newPen.CreatePen(PS_SOLID, 3, RGB(233,233,233));
		oldPen=pMemDC->SelectObject(&newPen);
	}
	//-- Draw Rect
	//-- TopLeft
	pMemDC->MoveTo(rt.left + rt.Height()/10 ,rt.top);
	pMemDC->LineTo(rt.left, rt.top);
	pMemDC->LineTo(rt.left, rt.top + rt.Height()/10);

	//-- BottomLeft
	pMemDC->MoveTo(rt.left , rt.bottom - rt.Height()/10 );
	pMemDC->LineTo(rt.left, rt.bottom);
	pMemDC->LineTo(rt.left + rt.Height()/10, rt.bottom);

	//-- TopRight
	pMemDC->MoveTo(rt.right - rt.Height()/10 ,rt.top);
	pMemDC->LineTo(rt.right, rt.top);
	pMemDC->LineTo(rt.right, rt.top + rt.Height()/10);

	//-- BottomRight
	pMemDC->MoveTo(rt.right , rt.bottom - rt.Height()/10 );
	pMemDC->LineTo(rt.right, rt.bottom);
	pMemDC->LineTo(rt.right - rt.Height()/10, rt.bottom);

	//-- Center
	/*
	pMemDC->MoveTo(rt.CenterPoint().x , rt.CenterPoint().y - rt.Height()/10 );
	pMemDC->LineTo(rt.CenterPoint().x , rt.CenterPoint().y + rt.Height()/10 );
	pMemDC->MoveTo(rt.CenterPoint().x - rt.Height()/10 , rt.CenterPoint().y );
	pMemDC->LineTo(rt.CenterPoint().x + rt.Height()/10 , rt.CenterPoint().y );
	*/

	pMemDC->SelectObject(oldPen);
	newPen.DeleteObject();
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-22
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CLiveview::OnMouseMove(UINT nFlags, CPoint point)
{
	if(m_bZoomMove)
	{
		//-- Set Base Rect Size
		GetClientRect(m_rect);
		//-- Check Size, and Rect
		SetPtZomm(point);
		//-- MOUSE LEAVE
		MouseEvent();
		//-- ReDraw
		Invalidate(FALSE);  
	}
	CView::OnMouseMove(nFlags, point);
}

void CLiveview::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CLiveview::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
    if(m_bZoomMove)
	{
		m_bZoomMove = FALSE;
		Invalidate(FALSE);   
	}	
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-22
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CLiveview::OnLButtonDown(UINT nFlags, CPoint point)
{
	if(m_bDrawZoomPos)
	{
		if(PtInRect(point))
			m_bZoomMove = TRUE;
		else
			m_bZoomMove = FALSE;
		 Invalidate(FALSE); 
	}
	CView::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-22
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CLiveview::OnLButtonUp(UINT nFlags, CPoint point)
{
	if(m_bDrawZoomPos)
	{
		m_bZoomMove = FALSE;
		Invalidate(FALSE);   
	}
	CView::OnLButtonUp(nFlags, point);
}

BOOL CLiveview::PtInRect(CPoint pt)
{
	CRect rt;
	rt.left		= m_ptZoom.x - m_szZoom.cx/2;
	rt.right	= m_ptZoom.x + m_szZoom.cx/2;
	rt.top		= m_ptZoom.y - m_szZoom.cy/2;
	rt.bottom	= m_ptZoom.y + m_szZoom.cy/2;
		
	if(rt.PtInRect(pt))
		return TRUE;
	return FALSE;
}

void CLiveview::SetPtZomm(CPoint pt)
{
	if(pt.x < m_szZoom.cx/2)								m_ptZoom.x = m_szZoom.cx/2;
	else if(pt.x > m_rect.Width() - m_szZoom.cx/2)		m_ptZoom.x = m_rect.Width() - m_szZoom.cx/2;
	else														m_ptZoom.x = pt.x;

	if(pt.y < m_szZoom.cy/2)								m_ptZoom.y = m_szZoom.cy/2;
	else if(pt.y > m_rect.Height() - m_szZoom.cy/2)		m_ptZoom.y = m_rect.Height() - m_szZoom.cy/2;
	else														m_ptZoom.y = pt.y;
}

void CLiveview::UpdatePosition(int x, int y)
{
	//-- Re Position
	if(m_bDrawZoomPos)
	{
		//-- Repositioning
		m_ptZoom.x = int( ( (float) x * (float)m_ptZoom.x) / (float)m_rect.Width());
		m_ptZoom.y = int( ( (float) y * (float)m_ptZoom.y) / (float)m_rect.Height());
		//-- Rescaling
		m_szZoom = CSize(x/m_nZoomSize, y/m_nZoomSize);
	}
}

void CLiveview::SetShift(int n)
{
	m_nShift = n; 
	//-- 2011-07-15 hongsu.jung
	//-- Check Zoom Size
	if(IsRotate())
	{
		int nX, nY;
		nX = m_szZoom.cx;
		nY = m_szZoom.cy;
		m_szZoom.SetSize(nY, nX);
		
		nX = m_ptZoom.x;
		nY = m_ptZoom.y;
		m_ptZoom.SetPoint(nY, nX);

		//-- Change Set SrcSize
	}
}
