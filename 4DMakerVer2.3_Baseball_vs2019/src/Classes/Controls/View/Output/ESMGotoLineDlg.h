////////////////////////////////////////////////////////////////////////////////
//
//	ESMGotoLineDlg.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once
#include "afxwin.h"


// CESMGotoLineDlg 대화 상자입니다.

class CESMGotoLineDlg : public CDialog
{
	DECLARE_DYNAMIC(CESMGotoLineDlg)

public:
	CESMGotoLineDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMGotoLineDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_GOTO_LINE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();


	DECLARE_MESSAGE_MAP()
public:
	CString m_strLineNumberTitle;
	int m_nLineNumber;
	CEdit m_ctrlLineNumber;

public :
	void SetLineNumberTitle(long lStrat, long lEnd);

};
