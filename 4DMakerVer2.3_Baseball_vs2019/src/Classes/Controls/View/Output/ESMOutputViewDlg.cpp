////////////////////////////////////////////////////////////////////////////////
//
//	ESMOutputViewDlg.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MainFrm.h"
#include "SdiSingleMgr.h"
#include "ESMOutputViewDlg.h"
#include "ESMGotoLineDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CESMOutputViewDlg::CESMOutputViewDlg(CWnd* pParent /*=NULL*/)
: CDialog(CESMOutputViewDlg::IDD, pParent)
{
	m_bPause = FALSE;	
	m_pWndESMOutputFindStirng = NULL;
}

CESMOutputViewDlg::~CESMOutputViewDlg()
{
	int nFindStringCnt = 0;
	nFindStringCnt = (int)m_strFindstrings.GetCount();
	if(nFindStringCnt != 0)
	{
		m_strFindstrings.RemoveAll();
	}
}

void CESMOutputViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OUTPUT_CTRL, m_wndScintilla);
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
}


BEGIN_MESSAGE_MAP(CESMOutputViewDlg, CDialog)
	ON_WM_SIZE()
	ON_COMMAND(ID_IMAGE_BK			, OnToggleBookMark)
	ON_COMMAND(ID_IMAGE_BK_PREV		, OnPreviousBookMark)
	ON_COMMAND(ID_IMAGE_BK_NEXT		, OnNextBookMark)
	ON_COMMAND(ID_IMAGE_BK_DEL		, OnClearAllBookMarks)
	ON_COMMAND(ID_IMAGE_PAUSE		, OnShowESMOutput)
	ON_COMMAND(ID_IMAGE_START		, OnShowESMOutput)
	ON_COMMAND(ID_IMAGE_FINDSTRING	, OnFindString)
	ON_COMMAND(ID_IMAGE_GOTOLINE	, OnGotoLine)
	ON_COMMAND(ID_IMAGE_CLEARALL	, OnClearAllDocument)	
	ON_COMMAND(ID_IMAGE_LINE_LAST	, OnGotoLast)	
	ON_MESSAGE(WM_SCI, OnSCIMsg)
END_MESSAGE_MAP()


// CESMOutputViewDlg 메시지 처리기입니다.
BOOL CESMOutputViewDlg::OnInitDialog() 
{
	if(!CDialog::OnInitDialog())
	{
		ASSERT( FALSE );
		return FALSE;
	}

	//-- Only Read
	m_wndScintilla.m_bESMOutputView = TRUE;
	m_wndScintilla.Init();
	m_wndScintilla.SetDisplaySelection(FALSE);

	return TRUE;
}

void CESMOutputViewDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);	
	if(m_wndScintilla.GetSafeHwnd())
		m_wndScintilla.MoveWindow(0, 40, cx, cy - 40);
	RepositionBars(0,0xFFFF,0);	
}

void CESMOutputViewDlg::InitImageFrameWnd()
{
	//---------------------------------------------------------------
	//--IMGWND_TYPE_TESTSTEP
	//---------------------------------------------------------------
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_PAUSE,
		ID_SEPARATOR,
		//ID_IMAGE_BK,
		//ID_IMAGE_BK_PREV,
		//ID_IMAGE_BK_NEXT,
		//ID_IMAGE_BK_DEL,
		ID_SEPARATOR,
		//ID_IMAGE_GOTOLINE,
		ID_IMAGE_FINDSTRING,
		ID_SEPARATOR,
		ID_IMAGE_CLEARALL,
		//ID_IMAGE_LOG_FILTER,
		ID_SEPARATOR,
		ID_IMAGE_LINE_LAST,
	};

	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));

	//2012-07-03
	//DWORD dwStyle = WS_CHILD|WS_VISIBLE|WS_TABSTOP|ES_CENTER; //|WS_DISABLED;
	//m_edtLogFilter.Create(dwStyle,	CRect(0,8,100,18), &m_wndToolBar, ID_IMAGE_LOG_FILTER);
	//m_edtLogFilter.SetFont(CFont::FromHandle((HFONT)::GetStockObject(DEFAULT_GUI_FONT)));
	//m_edtLogFilter.SetWindowText(_T(""));

	//m_wndToolBar.SetButtonCtrl(m_wndToolBar.CommandToIndex(ID_IMAGE_LOG_FILTER), &m_edtLogFilter);

	RepositionBars(0,0xFFFF,0);	
	m_wndToolBar.ShowWindow(SW_SHOW);
}

//--To add Serial
void CESMOutputViewDlg::AddESMOutput(CString strToken, BOOL bLoad)
{
	if(strToken.GetLength() > 0)
	{
		strToken.Replace(_T("\r\n"), _T("\n"));
		strToken.Replace(_T("\n"), _T("\r\n"));
	}
		
	CString strSerialLog = _T("");
	int nPos = 0;

	while(strToken != _T(""))
	{
		nPos = strToken.Find(_T("\r\n"));
		if(nPos > -1)
		{
			//-- Send the left string
			strSerialLog =strToken.Left(nPos);
			int nStrLen = strToken.GetLength();
			if( nStrLen > nPos + 2)
				strToken = strToken.Mid(nPos + 2);
			else
				strToken = _T("");

			strSerialLog.Append(_T("\r\n"));

			//-- 2012-09-13 hongsu@esmlab.com
			//-- Exception
			if(m_wndScintilla)
				m_wndScintilla.AppendText(strSerialLog);
		}
		else if(nPos == -1)
		{
			// 2012-05-16 eunkyu.park
			// strToken에 \r\n이 없을시 무한 루프에 빠지는 현상 방지.
			break;
		}
	}
}

//------------------------------------------------------------------------------ 
//! @brief   Set the show mode. If you press the pause button, the serial log saved 
//!          the buffer.
//! @date     
//! @owner    
//! @note
//! @return        
//------------------------------------------------------------------------------ 
void CESMOutputViewDlg::ShowESMOutput(CString strLog)
{
	//CString strFilter = _T("");
	//m_edtLogFilter.GetWindowText(strFilter);

	if(!m_bPause)
	{
		long lLineCnt = m_wndScintilla.GetLineCount();
		if(lLineCnt > MAX_LINE_COUNT)
			m_wndScintilla.SetText(_T(""));

		//if(strFilter.GetLength() > 0)
		//{
		//	if(strLog.Find(strFilter) >= 0)
		//		AddESMOutput(strLog);
		//}
		//else
		//{
			AddESMOutput(strLog);
		//}
	}
}

//--Goto Selected Line in Filtering Window
//--Filtering Window send the message.
void CESMOutputViewDlg::GotoSelectedLine(CString strLineNum)
{
	long lLineNum = 0;
	long lStartPos = 0;
	long lEndPos = 0;

	lLineNum = _ttoi(strLineNum);

	//Go to line
	m_wndScintilla.GotoLine(lLineNum);
	m_wndScintilla.SetFocus();

	//--To select the line
	lStartPos = m_wndScintilla.GetCurrentLineEndPosition(lLineNum - 1);
	lEndPos = m_wndScintilla.GetCurrentPosition();

	m_wndScintilla.SetSelection(lStartPos, lEndPos);
}

//-- To pause / start
void CESMOutputViewDlg::OnShowESMOutput()
{
	if(m_bPause == TRUE)
	{
		m_bPause = FALSE;
		ShowESMOutput(_T(""));

		ChangeToolBarImage();
	}
	else
	{
		m_bPause = TRUE;

		//--To change the button image.
		ChangeToolBarImage();		
	}
}

void CESMOutputViewDlg::ChangeToolBarImage()
{
	if(m_bPause == TRUE)
	{
		UINT arrCvTbBtns[] =
		{
			ID_IMAGE_START,
			ID_SEPARATOR,
			//ID_IMAGE_BK,
			//ID_IMAGE_BK_PREV,
			//ID_IMAGE_BK_NEXT,
			//ID_IMAGE_BK_DEL,
			ID_SEPARATOR,
			//ID_IMAGE_GOTOLINE,
			ID_IMAGE_FINDSTRING,
			ID_SEPARATOR,
			ID_IMAGE_CLEARALL,
			ID_SEPARATOR,
			ID_IMAGE_LINE_LAST,
		};
		VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));
		RepositionBars(0,0xFFFF,0);	
		m_wndToolBar.ShowWindow(SW_SHOW);
	}
	else
	{
		UINT arrCvTbBtns[] =
		{
			ID_IMAGE_PAUSE,
			ID_SEPARATOR,
			//ID_IMAGE_BK,
			//ID_IMAGE_BK_PREV,
			//ID_IMAGE_BK_NEXT,
			//ID_IMAGE_BK_DEL,
			ID_SEPARATOR,
			//ID_IMAGE_GOTOLINE,
			ID_IMAGE_FINDSTRING,
			ID_SEPARATOR,
			ID_IMAGE_CLEARALL,
			ID_SEPARATOR,
			ID_IMAGE_LINE_LAST,
		};
		VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));
		RepositionBars(0,0xFFFF,0);	
		m_wndToolBar.ShowWindow(SW_SHOW);
	}
}

//-- Clear All Document
void CESMOutputViewDlg::OnClearAllDocument()
{
	m_wndScintilla.SetText(_T(""));
}

//-- Find String
void CESMOutputViewDlg::OnFindString()
{
	//-- Create Find String dialog
	m_pWndESMOutputFindStirng = new CESMFindStringDlg(this);
	if(!m_pWndESMOutputFindStirng)
		return;

	m_pWndESMOutputFindStirng->Create(CESMFindStringDlg::IDD);
	m_pWndESMOutputFindStirng->ShowWindow(SW_SHOW);

	//-- Dialog Init
	m_pWndESMOutputFindStirng->m_wndESMSerialViewDlg = this->GetSafeHwnd();
	m_pWndESMOutputFindStirng->InitDialog(&m_strFindstrings);
}

//-- Find Next String
void CESMOutputViewDlg::FindNextString()
{
	CString strFindString = _T("");
	int nSearchFlag = 0;

	//-- Get the find option
	nSearchFlag = m_pWndESMOutputFindStirng->SetSearchFlags();

	//-- Get the find string
	strFindString = m_pWndESMOutputFindStirng->m_strFindString;

	//-- Add the old find string
	if(CheckComboBox(strFindString))
		m_strFindstrings.AddHead(strFindString);

	//-- Set the find option
	m_wndScintilla.SetSearchflags(nSearchFlag);

	//-- Find the string
	if(!m_wndScintilla.SearchForward(strFindString.LockBuffer()))
	{
		AfxMessageBox(_T("Can't search the string"));

		return;
	}

	//-- Release
	strFindString.UnlockBuffer();
}

//-- Find Previous String
void CESMOutputViewDlg::FindPreviousString()
{
	CString strFindString = _T("");
	int nSearchFlag = 0;

	//-- Get the find option
	nSearchFlag = m_pWndESMOutputFindStirng->SetSearchFlags();

	//-- Get the find string
	strFindString = m_pWndESMOutputFindStirng->m_strFindString;

	//-- Add the old find string
	if(CheckComboBox(strFindString))
		m_strFindstrings.AddHead(strFindString);

	//-- Set the find option
	m_wndScintilla.SetSearchflags(nSearchFlag);

	//-- Find the string
	if(!m_wndScintilla.SearchBackward(strFindString.LockBuffer()))
	{
		AfxMessageBox(_T("Can't search the string"));

		return;
	}

	//-- Release
	strFindString.UnlockBuffer();
}

//-- Check the same string
BOOL CESMOutputViewDlg::CheckComboBox(CString strFindString)
{
	CString strPreFindString = _T("");
	POSITION pos;
	int nSameString = 0;

	pos = m_strFindstrings.GetHeadPosition();

	//-- No String array
	if(pos == 0 && strPreFindString.IsEmpty() == FALSE)
	{
		return TRUE;
	}

	//-- Find the same string
	while(pos)
	{
		strPreFindString = m_strFindstrings.GetNext(pos);

		if(strFindString.Compare(strPreFindString) == 0)
		{
			nSameString++;
		}
	}

	//-- No same string
	if(nSameString == 0)
		return TRUE;

	return FALSE;
}

//-- To goto function using dialog
void CESMOutputViewDlg::OnGotoLine()
{
	CESMGotoLineDlg dlg;

	long lLineCnt;
	long lStartPos;
	long lEndPos;	

	lLineCnt = m_wndScintilla.GetLineCount();

	//-- Set the dialog title
	dlg.SetLineNumberTitle(1, lLineCnt);
	dlg.m_nLineNumber = m_wndScintilla.GetCurrentLine();

	if(dlg.DoModal() == IDOK)
	{
		m_wndScintilla.GotoLine(dlg.m_nLineNumber);

		//--To select the line
		lStartPos = m_wndScintilla.GetCurrentLineEndPosition(dlg.m_nLineNumber - 1);
		lEndPos = m_wndScintilla.GetCurrentPosition();

		m_wndScintilla.SetSelection(lStartPos, lEndPos);
	}
}

//-- Add or remove BookMark
void CESMOutputViewDlg::OnToggleBookMark()
{
	int nLineCnt = m_wndScintilla.GetCurrentLine();

	m_wndScintilla.ToggleBookMark(nLineCnt);
}

//-- Move next Bookmark
void CESMOutputViewDlg::OnNextBookMark()
{
	m_wndScintilla.FindNextBookmark();
}

//-- Move previous Bookmark
void CESMOutputViewDlg::OnPreviousBookMark()
{
	m_wndScintilla.FindPreviousBookmark();
}

//-- Clear all Bookmark
void CESMOutputViewDlg::OnClearAllBookMarks()
{
	m_wndScintilla.ClearAllBookMarks();
}

//-- To check the scintillaWnd event
BOOL CESMOutputViewDlg::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	NMHDR *phDR;
	phDR = (NMHDR*)lParam;

	//-- ScintillaWnd notiry
	if (phDR != NULL && phDR->hwndFrom == m_wndScintilla.m_hWnd)
	{
		SCNotification *pMsg = (SCNotification*)lParam;
		switch (phDR->code)
		{
		case SCN_MARGINCLICK:
			m_wndScintilla.DoToggleBookMark(pMsg->margin, pMsg->position);
			break;
		}
		return TRUE;
	}
	return CWnd::OnNotify(wParam, lParam, pResult);
}

LRESULT CESMOutputViewDlg::OnSCIMsg(WPARAM w, LPARAM l)
{
	l;
	switch((int)w)
	{
	case WM_SCI_SER_FIND_NEXT:
		FindNextString();
		break;
	case WM_SCI_SER_FIND_PRE:
		FindPreviousString();
		break;
	case WM_SCI_SER_POPUP_FIND_DLG:
		OnFindString();
		break;
	default:
		break;
	}
	return 0L;
}

void CESMOutputViewDlg::ClearOutputView()
{
	CString strRemainText = _T("");

	for(int i = MAX_LINE_COUNT / 2; i < MAX_LINE_COUNT; i++)
	{
		strRemainText.Append(m_wndScintilla.GetLineText(i));
		strRemainText.Append(_T("\r\n"));
	}
			
	m_wndScintilla.SetText(_T(""));

	CString strToken = strRemainText;
	strToken.Replace(_T("\r\n"), _T("\n"));
	strToken.Replace(_T("\n"), _T("\r\n"));	

	int nPos;

	while(strToken != _T(""))
	{
		nPos = strToken.Find(_T("\r\n"));

		if(nPos > -1)
		{
			//-- Send the left string
			strRemainText =strToken.Left(nPos);
			strToken = strToken.Mid(nPos + 2);

			m_wndScintilla.AppendText(strRemainText);		
		}
	}
}

//-- 2012-09-21 hongsu@esmlab.com
//-- Goto Last Line 
void CESMOutputViewDlg::OnGotoLast()
{
	m_wndScintilla.GotoLastLine();
}

BOOL CESMOutputViewDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg)
	{
		if(pMsg->message == WM_KEYDOWN)
		{
			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			ESMEvent* pEsmMsg = NULL;
			pEsmMsg = new ESMEvent();
			pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
			pEsmMsg->pParam = (LPARAM)pMsg;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}
