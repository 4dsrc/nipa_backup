////////////////////////////////////////////////////////////////////////////////
//
//	ESMFilteringListCtrl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "afxcmn.h"
#include "Scintilla.h"


typedef int (* fGetType) (int);

class CESMFilteringListCtrl : public CListCtrl
{
public:
	CESMFilteringListCtrl(void);
public:
	~CESMFilteringListCtrl(void);

	class CHeaderRightClick;

public:
	virtual void AddFilteredLog(CString strSerialLog, CString strLineNum);
	void SetEmptyMessage(const CString &message);
	//Combo
	void SetColumnType(fGetType func) { m_fGetType = func;}

protected:
	CComboBox * ComboItem( int nItem,  int nSubItem);
	
	DECLARE_MESSAGE_MAP()
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);

public :
	//Combo
	CStringList m_strList;
	fGetType m_fGetType;
	CString m_emptyMessage;

public:
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnPaint();

};
