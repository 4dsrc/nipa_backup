////////////////////////////////////////////////////////////////////////////////
//
//	ESMGotoLineDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


// Serial View 의 GotoLine 기능을 위한 다이얼로그

#include "stdafx.h"
#include "Resource.h"
#include "ESMGotoLineDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMGotoLineDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMGotoLineDlg, CDialog)

CESMGotoLineDlg::CESMGotoLineDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMGotoLineDlg::IDD, pParent)
	, m_strLineNumberTitle(_T(""))
	, m_nLineNumber(0)
{

}

CESMGotoLineDlg::~CESMGotoLineDlg()
{
}

void CESMGotoLineDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_GOTO_LINE_STATIC, m_strLineNumberTitle);
	DDX_Text(pDX, IDC_GOTO_LINE_EDIT, m_nLineNumber);
	DDX_Control(pDX, IDC_GOTO_LINE_EDIT, m_ctrlLineNumber);
}

BOOL CESMGotoLineDlg::OnInitDialog() 
{
	if(!CDialog::OnInitDialog())
	{
		ASSERT( FALSE );
		return FALSE;
	}

	int nTextLength;
	nTextLength = m_ctrlLineNumber.GetWindowTextLength();

	m_ctrlLineNumber.SetSel(0, nTextLength);

	return TRUE;
}

BEGIN_MESSAGE_MAP(CESMGotoLineDlg, CDialog)
END_MESSAGE_MAP()

void CESMGotoLineDlg::SetLineNumberTitle(long lStrat, long lEnd)
{
	CString strTitle =_T("");
	strTitle.Format(_T("Line Number (%ld ~ %ld):"), lStrat, lEnd);
	m_strLineNumberTitle = strTitle;
}


// CESMGotoLineDlg 메시지 처리기입니다.
