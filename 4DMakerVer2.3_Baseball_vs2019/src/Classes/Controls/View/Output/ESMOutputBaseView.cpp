////////////////////////////////////////////////////////////////////////////////
//
//	ESMOutputBaseView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "resource.h"
//#include "MainFrm.h"
#include "ESMOutputBaseView.h"
//#include "AnalyzerDefine.h"
//#include "ESMGotoLineSerialDlg.h"
#include "ESMCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//-------------------------------------------------------------------------- 
//! @brief	Constructor
//! @date	2011-6-2
//! @note
//! @revision	
//-------------------------------------------------------------------------- 
CESMOutputBaseView::CESMOutputBaseView(CWnd* pParent)
: CDialog(CESMOutputBaseView::IDD, pParent)	
{
	m_bPause = FALSE;
}

//-------------------------------------------------------------------------- 
//! @brief	  Called by the framework to exchange and validate dialog data
//! @date	  2011-2-18
//! @note
//! @revision	
//-------------------------------------------------------------------------- 
void CESMOutputBaseView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMOutputBaseView)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
}

BEGIN_MESSAGE_MAP(CESMOutputBaseView, CDialog)
	//{{AFX_MSG_MAP(CESMOutputBaseView)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()		

END_MESSAGE_MAP()

//-------------------------------------------------------------------------- 
//! @brief	  called in response to the WM_INITDIALOG message
//! @date	  2011-2-18
//! @note
//! @revision	
//--------------------------------------------------------------------------
BOOL CESMOutputBaseView::OnInitDialog() 
{
	if(!CDialog::OnInitDialog())
	{
		ASSERT(FALSE);
		return FALSE;
	}	

	m_wndScintilla.Init();
	m_wndScintilla.SetDisplayLinenumbers(FALSE);

	return TRUE;
}

//-------------------------------------------------------------------------- 
//! @brief	The framework calls this member function after the window's size has changed
//! @date	2011-2-18
//! @note
//! @revision	
//--------------------------------------------------------------------------
void CESMOutputBaseView::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	if(m_wndScintilla.GetSafeHwnd())
		m_wndScintilla.MoveWindow(0, 30, cx, cy-30);
	RepositionBars(0,0xFFFF,0);	
}

//-------------------------------------------------------------------------- 
//! @brief	Add thread analyzing data to the Graph
//! @date	2011-2-18
//! @note
//! @revision	
//--------------------------------------------------------------------------
BOOL CESMOutputBaseView::AppendText(CString strText)
{
	m_wndScintilla.AppendText(strText);
	return TRUE;
}

//-------------------------------------------------------------------------- 
//! @brief	  
//! @note
//! @revision	
//--------------------------------------------------------------------------
void CESMOutputBaseView::Clear()
{
	m_wndScintilla.SetText(_T(""));
}


//-------------------------------------------------------------------------- 
//! @brief	  Move previous Bookmark
//! @note
//! @revision	
//--------------------------------------------------------------------------
//void CESMOutputBaseView::OnPreviousBookMark()
//{
//	m_wndScintilla.FindPreviousBookmark();
//}

//-------------------------------------------------------------------------- 
//! @brief	  Move next Bookmark
//! @date		  2011-2-18
//! @note
//! @revision	
//--------------------------------------------------------------------------
//void CESMOutputBaseView::OnNextBookMark()
//{
//	m_wndScintilla.FindNextBookmark();
//}

//-------------------------------------------------------------------------- 
//! @brief	  Clear all Bookmark
//! @date		  2011-2-18
//! @note
//! @revision	
//--------------------------------------------------------------------------
//void CESMOutputBaseView::OnClearAllBookMarks()
//{
//	m_wndScintilla.ClearAllBookMarks();
//}

//-------------------------------------------------------------------------- 
//! @brief	  To goto function using dialog
//! @date		  2011-2-18
//! @note
//! @revision	
//--------------------------------------------------------------------------
//void CESMOutputBaseView::OnGotoLine()
//{
//	CESMGotoLineDlg dlg;
//
//	long lLineCnt;
//	long lStartPos;
//	long lEndPos;	
//
//	lLineCnt = m_wndScintilla.GetLineCount();
//
//	//-- Set the dialog title
//	dlg.SetLineNumberTitle(1, lLineCnt);
//	dlg.m_nLineNumber = m_wndScintilla.GetCurrentLine();
//
//	if(dlg.DoModal() == IDOK)
//	{
//		m_wndScintilla.GotoLine(dlg.m_nLineNumber);
//
//		//--To select the line
//		lStartPos = m_wndScintilla.GetCurrentLineEndPosition(dlg.m_nLineNumber - 1);
//		lEndPos = m_wndScintilla.GetCurrentPosition();
//
//		m_wndScintilla.SetSelection(lStartPos, lEndPos);
//	}
//}

//-------------------------------------------------------------------------- 
//! @brief	  Find String
//! @date		  2011-2-18
//! @note
//! @revision	
//--------------------------------------------------------------------------
//void CESMOutputBaseView::OnFindString()
//{
//	//-- Create Find String dialog
//	m_wndSerialFindString = new CESMFindStringDlg();
//
//	m_wndSerialFindString->Create(CESMFindStringDlg::IDD);
//	m_wndSerialFindString->ShowWindow(SW_SHOW);
//
//	//-- Dialog Init
//	m_wndSerialFindString->m_wndESMSerialViewDlg = this->GetSafeHwnd();
//	m_wndSerialFindString->InitDialog(&m_strFindstrings);
//}

//-------------------------------------------------------------------------- 
//! @brief	  check the scintillaWnd event
//! @date		  2011-2-18
//! @note
//! @revision	
//--------------------------------------------------------------------------
BOOL CESMOutputBaseView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	NMHDR* phDR;
	phDR = (NMHDR*)lParam;

	//ScintillaWnd notify
	if(phDR != NULL && phDR->hwndFrom == m_wndScintilla.m_hWnd)
	{
		SCNotification *pMsg = (SCNotification*)lParam;
		switch (phDR->code)
		{
		case SCN_MARGINCLICK:
			m_wndScintilla.DoToggleBookMark(pMsg->margin, pMsg->position);
			break;
		}
		return TRUE;
	}
	return CWnd::OnNotify(wParam, lParam, pResult);
}


