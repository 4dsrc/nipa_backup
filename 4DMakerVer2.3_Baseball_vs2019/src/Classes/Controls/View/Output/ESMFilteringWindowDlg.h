////////////////////////////////////////////////////////////////////////////////
//
//	ESMFilteringWindowDlg.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once
#include "afxcmn.h"
#include "ESMFilteringListCtrl.h"

class CESMOption;


// CESMFilteringWindowDlg 대화 상자입니다.

class CESMFilteringWindowDlg : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar	{virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

public:
	CESMFilteringWindowDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMFilteringWindowDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VIEW_FILTERING };

	//-- To Toolbar
	CInnerToolControlBar m_wndToolBar;

	//-- To ListView control
	CESMFilteringListCtrl m_ctrlFilteringWidow;
	CString m_strPortName;
	//-- To Match the serial View
	HWND m_hSerialView;	
	//-- To add the filtered string to save
	CStringList m_listFilteringSave;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	afx_msg void OnSize(UINT nType, int cx, int cy);

	//Communication with SerialView
	afx_msg void OnBookMarkAll();
	afx_msg void OnGotoSerialViewLine();
	afx_msg void OnOpenFiteringOptionDlg();
	afx_msg void OnClearAllItem();
	afx_msg void OnSaveFilteringWindow();

	DECLARE_MESSAGE_MAP()
public:
	void InitImageFrameWnd();
	void AddFilteredSerialLog(ESMFilteringInfo * filteringInfo);	//-- To add Filtering stirng
	void ClearAllFilteringList();			//Clear all list

	LRESULT OnSCIMsg(WPARAM w, LPARAM l);

public:
	afx_msg void OnLvnEndlabeleditFilteringlist(NMHDR *pNMHDR, LRESULT *pResult);
};
