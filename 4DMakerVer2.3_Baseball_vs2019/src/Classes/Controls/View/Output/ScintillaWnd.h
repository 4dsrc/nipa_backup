////////////////////////////////////////////////////////////////////////////////
//
//	ScintillaWnd.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "scintilla.h"
#include "scilexer.h"
#include "ESMTextProgressCtrl.h"

#define MAX_LINE_COUNT	10000

////////////////////////////////////
// @class CScintillaWnd | Class of a GCL Scintilla syntax coloring edit control for MFC
// @base public | CWnd


class CScintillaWnd : public CWnd  
{
public:
// @access public constructor - destructor
// @cmember empty Constructor
	CScintillaWnd();
// @cmember destructor
	virtual ~CScintillaWnd();

public:
// @access public macro members
// @cmember return linenumber display flag
   BOOL GetDisplayLinenumbers ()	{return m_bLinenumbers;};
// @cmember return selection display flag
   BOOL GetDisplaySelection ()		{return m_bSelection;};
// @cmember return folding margin display flag
   BOOL GetDisplayFolding ()		{return m_bFolding;};
// @cmember set search flags
   virtual void SetSearchflags (int nSearchflags)	{m_nSearchflags = nSearchflags;};
   //-- 2012-11-28 hongsu@esmlab.com
   //-- Change Coloe
   void SetBGColor(COLORREF color);
   	

public:
// @access public function members
// @cmember register a window class to use scintilla in a dialog
   static BOOL Register(CWinApp *app, UINT id);
// @cmember try to load Lexer DLL
   static HMODULE LoadScintillaDll (LPCSTR szLexerDll = NULL);
// @cmember create window
   virtual BOOL Create (LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);

	//-- Function (2010.1.21)
	//--Interface
   	virtual void Init();
	virtual void SetDisplayLinenumbers(BOOL bFlag = TRUE);
	virtual void SetDisplayFolding(BOOL bFlag = TRUE);
	virtual void SetDisplaySelection(BOOL bFlag = TRUE);

	//-- About Line, Position Info
	virtual int GetLinenumberWidth ();
	virtual int GetLinenumberChars ();
	virtual long GetCurrentLine();
	virtual long GetCurrentColumn();
	virtual long GetCurrentPosition();
	virtual long GetCurrentLineEndPosition(long lLine);
	virtual long GetLineCount();
	virtual long GetTextLenght();
	virtual long GetSelectionStart();
	virtual void GotoPosition(long lPos);
	virtual void GotoLine(long lLine);
	virtual void GotoLastLine();
	virtual void SetSelection(long lStart, long lEnd);
	//--About Edit String
	virtual void SetText (LPCTSTR szText);
	virtual void AddText (int nLength, LPCSTR szText, BOOL bLoad = FALSE);
	//virtual void AppendText(int nLength, LPCSTR szText,BOOL bLoad = FALSE);
	virtual void AppendText(LPCTSTR szText,BOOL bLoad = FALSE);
	virtual void InsertText(LPCSTR szText);

	//--About BookMark
	virtual void ToggleBookMark(long lLine);
	virtual BOOL HasBookmark(long lLine);
	virtual void FindNextBookmark();
	virtual void FindPreviousBookmark();
	virtual void ClearAllBookMarks();
	virtual void DoToggleBookMark(int nMarge, long lPosition);

	//-- Find Strint
	virtual BOOL SearchForward(LPTSTR szText);
	virtual BOOL SearchBackward(LPTSTR szText);
	virtual BOOL CheckSearchString(LPTSTR szText, BOOL bForward);

	//--Reload 기능 추가
	//--2010-01-28 이정택
	virtual BOOL LoadFile (LPCTSTR szFile, CESMTextProgressCtrl &ctrlProgressBar);

	//-- Save the Filtering String Function
	virtual CString GetLineText(int nLineNum);

	//-- To Send the debugcommand
	void WriteChar(UINT nChar, UINT nRepCnt = 1, UINT nFlags = 1);

	//-- To ContextMenu Enable
	void ModifyPopupMenu(HMENU hMenu);

	long m_lLastPos;	
	CString m_strPreString;
	CString m_strPreChar;

	BOOL m_bFindString;	
	int m_nDeviceNum;
	//-- Check Value
	BOOL m_bESMOutputView;
	BOOL m_bEndCussor;

protected:
	// @access protected data members
	// @cmember flag if we should display line numbers
	BOOL m_bLinenumbers;
	// @cmember flag if we should display selection and bookmark margin
	BOOL m_bSelection;
	// @cmember flag if we should display folding margin
	BOOL m_bFolding;
	// @cmember search flags
	int  m_nSearchflags;

	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);

	//-- Context View
	afx_msg void OnCopy()		{ SendMessage(SCI_COPY);}
	afx_msg void OnSelectAll()	{ SendMessage(SCI_SELECTALL);}	
	afx_msg void OnClear()		{ SendMessage(SCI_CLEAR);}

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);	

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	virtual int GetCurrentStyle();

};
#define STR_SCINTILLAWND _T("Scintilla")
#define STR_LEXERDLL     _T("SciLexer.dll")
