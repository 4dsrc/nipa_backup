////////////////////////////////////////////////////////////////////////////////
//
//	TGFilteringWindowDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

// FiteringWindow 의 Dialog (껍데기)

#include "stdafx.h"
#include "TG.h"
#include "TGFilteringWindowDlg.h"
#include "GlobalIndex.h"

// CTGFilteringWindowDlg 대화 상자입니다.

CTGFilteringWindowDlg::CTGFilteringWindowDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTGFilteringWindowDlg::IDD, pParent)
{
	m_strPortName = _T("");
}

CTGFilteringWindowDlg::~CTGFilteringWindowDlg()
{
}

void CTGFilteringWindowDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FILTERINGLIST, m_ctrlFilteringWidow);
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
}


BEGIN_MESSAGE_MAP(CTGFilteringWindowDlg, CDialog)
	ON_WM_SIZE()
	ON_MESSAGE(WM_SCI, OnSCIMsg)	
	ON_COMMAND(ID_IMAGE_OPT, OnOpenFiteringOptionDlg)
	ON_COMMAND(ID_IMAGE_BK, OnBookMarkAll)
	ON_COMMAND(ID_IMAGE_GOTOLINE, OnGotoSerialViewLine)
	ON_COMMAND(ID_IMAGE_CLEARALL, OnClearAllItem)
	ON_COMMAND(ID_CASE_SAVE, OnSaveFilteringWindow)	
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_FILTERINGLIST, &CTGFilteringWindowDlg::OnLvnEndlabeleditFilteringlist)
END_MESSAGE_MAP()

// CTGFilteringWindowDlg 메시지 처리기입니다.
static BOOL _List_Type(int col)
{
	if (col == 2)
		return TRUE;
	// else :
	return FALSE;
}

BOOL CTGFilteringWindowDlg::OnInitDialog() 
{
	if(!CDialog::OnInitDialog())
	{
		ASSERT( FALSE );
		return FALSE;
	}

	//Filtering ListView Init
  	m_ctrlFilteringWidow.InsertColumn(0, _T("Line Num"), LVCFMT_CENTER, 60);
	m_ctrlFilteringWidow.InsertColumn(1, _T("Filtered Serial Log"), LVCFMT_LEFT, 500);
	m_ctrlFilteringWidow.InsertColumn(2, _T("Save Flag"), LVCFMT_CENTER, 80);
	m_ctrlFilteringWidow.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	//-- 2010-10-20 hongsu.jung
	//-- COMBO BOX 
	m_ctrlFilteringWidow.SetColumnType ( (fGetType)_List_Type );
	m_ctrlFilteringWidow.m_strList.AddTail(_T(""));
	m_ctrlFilteringWidow.m_strList.AddTail(_T("From"));
	m_ctrlFilteringWidow.m_strList.AddTail(_T("To"));

	//-- [2010-3-19 Lee JungTaek]
	//-- Set the Empty String
	m_ctrlFilteringWidow.SetEmptyMessage(_T("There isn't any filtered string."));

	return TRUE;
}

void CTGFilteringWindowDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);	
	if (m_ctrlFilteringWidow.GetSafeHwnd())
		m_ctrlFilteringWidow.MoveWindow(0, 30, cx, cy-30);
	RepositionBars(0,0xFFFF,0);	
}

void CTGFilteringWindowDlg::InitImageFrameWnd()
{
	//---------------------------------------------------------------
	//--IMGWND_TYPE_TESTSTEP
	//---------------------------------------------------------------
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_BK,
		ID_IMAGE_GOTOLINE,
		ID_SEPARATOR,
		ID_IMAGE_CLEARALL,
		ID_SEPARATOR,
		ID_CASE_SAVE,
		ID_SEPARATOR,
		ID_IMAGE_OPT,
	};
	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));
	RepositionBars(0,0xFFFF,0);	
	m_wndToolBar.ShowWindow(SW_SHOW);
}

//-- Add the Filtered serial log
void CTGFilteringWindowDlg::AddFilteredSerialLog(TGFilteringInfo * filteringInfo)
{
	CString strLineNum = _T("");
	CString strSerialLog = _T("");

	//-- Get the filtering info
	strLineNum.Format(_T("%d"), filteringInfo->nLIneNum);
	strSerialLog = filteringInfo->strSerialLog;

	//-- Add the string to listview
	m_ctrlFilteringWidow.AddFilteredLog(strLineNum, strSerialLog);
}

//-- To reload thefile, clear all item in listview
void CTGFilteringWindowDlg::ClearAllFilteringList()
{
	if(m_ctrlFilteringWidow.GetItemCount() != 0)
		m_ctrlFilteringWidow.DeleteAllItems();
}

//-- Open the option dialog
void CTGFilteringWindowDlg::OnOpenFiteringOptionDlg()
{
	TGEvent* pMsg = new TGEvent;
	pMsg->message = WM_TG_OPT_FILTER;
	AfxGetMainWnd()->SendMessage(WM_TG,WM_TG_OPT, (LPARAM)pMsg);
}

//-- Set the bookmark to Serial view
void CTGFilteringWindowDlg::OnBookMarkAll()
{
	long nRow = 0;
	CString strLineNum = (_T(""));
	TGEvent msgSci;

	nRow = m_ctrlFilteringWidow.GetItemCount();

	for(int i = 0; i < nRow; i++)
	{
		strLineNum = m_ctrlFilteringWidow.GetItemText(i, 0);
		msgSci.message = WM_SCI_FILTER_SER_BOOKMARKALL;				
		msgSci.pParam	= (LPARAM)(LPCTSTR)strLineNum;
		::SendMessage(m_hSerialView, WM_SCI, WM_SCI_FILTER_SER_BOOKMARKALL, (LPARAM)&msgSci);	
	}
}


//-- Goto serial view which user select
void CTGFilteringWindowDlg::OnGotoSerialViewLine()
{
	POSITION pos = 0;
	CString strLineNum = (_T(""));
	TGEvent msgSci;
	int nLineNum = 0;

	pos = m_ctrlFilteringWidow.GetFirstSelectedItemPosition();
	
	if(pos != NULL)
	{
		nLineNum = m_ctrlFilteringWidow.GetNextSelectedItem(pos);
		strLineNum = m_ctrlFilteringWidow.GetItemText(nLineNum, 0);

		msgSci.message = WM_SCI_FILTER_SER_BOOKMARKALL;				
		msgSci.pParam	= (LPARAM)(LPCTSTR)strLineNum;
		::SendMessage(m_hSerialView, WM_SCI, WM_SCI_FILTER_SER_GOTO, (LPARAM)&msgSci);
	}
}

//-- To Clear All List
void CTGFilteringWindowDlg::OnClearAllItem()
{
	if(m_ctrlFilteringWidow.GetItemCount() != 0)
		m_ctrlFilteringWidow.DeleteAllItems();
}

//-- To save the filtering string to file
void CTGFilteringWindowDlg::OnSaveFilteringWindow()
{
	//-- To Get the File Name
	CString strLogPath = TGGetPath(TG_PATH_LOG_SERIAL_DATE);
	CString strSavePath = _T("");

	BOOL bPairFromTo = TRUE;
	BOOL bAbleToSingle = TRUE;

	//-- Set the Default file name
	CTime time  = CTime::GetCurrentTime();
	CString strData = _T("");
	strSavePath = time.Format("%Y_%m_%d");	

	strData.Format(_T("_%02d%02d%02d_Filtering_SerialLog_%s.log"), time.GetHour(), time.GetMinute(), time.GetSecond(), m_strPortName);
	strSavePath.Append(strData);

	//-- Create the Save Dialog
	TCHAR szFilter[] = _T("Log Files(*.log)|*.log|All Files(*.*)|*.*||");
	CString strTitle = _T("Filtering Window Save As");
	CFileDialog dlg(FALSE, _T(".log"), strSavePath, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, this);
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = strLogPath;

	//-- Open the Dialog
	if(dlg.DoModal() == IDOK)
		strSavePath = dlg.GetPathName();
	else
		return;
	 
	long lRowCnt = 0L;
	CString strSaveColumn = _T("");
	CString strSaveFlag = _T("");
	CString strSaveInfo = _T("");

	TGEvent msgSci;	
	//-- Get the list item count
	lRowCnt = m_ctrlFilteringWidow.GetItemCount();

	for(int nRow = 0; nRow < lRowCnt; nRow++)
	{	
		strSaveFlag = m_ctrlFilteringWidow.GetItemText(nRow, 2);

		//-- Get the single "To" line
		if(strSaveFlag.Compare(_T("To")) == 0 && bPairFromTo == TRUE && bAbleToSingle == TRUE)
		{
			strSaveColumn = m_ctrlFilteringWidow.GetItemText(nRow, 0);

			strSaveInfo.Format(_T("1,%s"), strSaveColumn);
			
			m_listFilteringSave.AddTail(strSaveInfo);

			strSaveInfo = _T("");
			bAbleToSingle = FALSE;

			continue;
		}

		//-- Get the "From" Line count
		if(strSaveFlag.Compare(_T("From")) == 0 && bPairFromTo == TRUE)
		{
			strSaveColumn = m_ctrlFilteringWidow.GetItemText(nRow, 0);
			bPairFromTo = FALSE;
			bAbleToSingle = FALSE;

			//-- Set the left wing in pair
			strSaveInfo.Append(strSaveColumn + _T(","));
		}

		//-- Get the "To" line count
		if(strSaveFlag.Compare(_T("To")) == 0 && bPairFromTo == FALSE && bAbleToSingle == FALSE)
		{
			strSaveColumn = m_ctrlFilteringWidow.GetItemText(nRow, 0);
			bPairFromTo = TRUE;

			//-- Set the right wing in pair
			strSaveInfo.Append(strSaveColumn);
		}

		//-- Add the string list to send the message
		if(!strSaveInfo.IsEmpty() && bPairFromTo == TRUE)
		{
			m_listFilteringSave.AddTail(strSaveInfo);
			strSaveInfo = _T("");			//-- Delete the pair
		}
	}

	//-- Get the single "From" line
	if(!strSaveInfo.IsEmpty() && bPairFromTo == FALSE)
	{
		strSaveColumn = _T("End");

		strSaveInfo.Append(strSaveColumn);

		m_listFilteringSave.AddTail(strSaveInfo);

		strSaveInfo = _T("");
	}

	//-- Send the Pair to SerialView
	if(m_listFilteringSave.GetCount() > 0)
	{
		//-- Send the Comment first
		msgSci.message = WM_SCI_FILTER_SER_SAVE_PATH;
		msgSci.pParam = (LPARAM)(LPCTSTR)strSavePath;
		::SendMessage(m_hSerialView, WM_SCI, WM_SCI_FILTER_SER_SAVE_PATH, (LPARAM)&msgSci);

		//-- Set the SaveInfo info and send the message
		msgSci.message = WM_SCI_FILTER_SER_SAVE;
		msgSci.pParam = (LPARAM)&m_listFilteringSave;
		::SendMessage(m_hSerialView, WM_SCI, WM_SCI_FILTER_SER_SAVE, (LPARAM)&msgSci);
	}
	else
	{
		AfxMessageBox(_T("You must check the save flag"));
	}	
}

LRESULT CTGFilteringWindowDlg::OnSCIMsg(WPARAM w, LPARAM l)
{
	switch((int)w)
	{
	case WM_SCI_SER_FILTER_SEND:
		{
			TGEvent* pMsg = (TGEvent*)l;
			if(pMsg)
				AddFilteredSerialLog((TGFilteringInfo *)pMsg->pParam);
		}
		break;
	case WM_SCI_SER_FILTER_CLEAR:
		ClearAllFilteringList();
		break;
	case WM_SCI_FILTER_SER_GOTO:
		OnGotoSerialViewLine();
		break;
	default:
		break;
	}
	return 0L;
}

//-- To set the column (Save Flag)
void CTGFilteringWindowDlg::OnLvnEndlabeleditFilteringlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LV_DISPINFO *plvDispInfo = (LV_DISPINFO*)pNMHDR;
	LV_ITEM *plvItem = &plvDispInfo->item;

	if (plvItem->pszText != NULL)	//-- valid text
	{
		if(plvItem->iItem != -1)	//-- valid item
		{
			m_ctrlFilteringWidow.SetItemText(plvItem->iItem, plvItem->iSubItem, plvItem->pszText);
		}
	}

	*pResult = 0;
}
