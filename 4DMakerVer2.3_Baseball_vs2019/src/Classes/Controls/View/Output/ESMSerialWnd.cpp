////////////////////////////////////////////////////////////////////////////////
//
//	TGSerialWnd.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#define STRICT
#include "stdafx.h"
#include <crtdbg.h>
#include <tchar.h>
#include <windows.h>


//////////////////////////////////////////////////////////////////////
// Include module headerfile

#include "TGSerialWnd.h"


//////////////////////////////////////////////////////////////////////
// Disable warning C4127: conditional expression is constant, which
// is generated when using the _RPTF and _ASSERTE macros.

#pragma warning(disable: 4127)


//////////////////////////////////////////////////////////////////////
// Enable debug memory manager

#ifdef _DEBUG

#ifdef THIS_FILE
#undef THIS_FILE
#endif

static const char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW

#endif


//////////////////////////////////////////////////////////////////////
// Code

// Register the standard CTGSerialWnd COM message
const UINT CTGSerialWnd::mg_nDefaultComMsg = ::RegisterWindowMessage(_T("CTGSerialWnd_DefaultComMsg"));


CTGSerialWnd::CTGSerialWnd()
	: m_hwndDest(0)
	, m_nComMsg(WM_NULL)
	, m_lParam(0)
{
}

CTGSerialWnd::~CTGSerialWnd()
{
	// Check if the thread handle is still there. If so, then we
	// didn't close the serial port. We cannot depend on the
	// CSerial destructor, because if it calls Close then it
	// won't call our overridden Close.
	if (m_hThread)
		Close();
}

LONG CTGSerialWnd::Open (LPCTSTR lpszDevice, HWND hwndDest, UINT nComMsg, LPARAM lParam, DWORD dwInQueue, DWORD dwOutQueue)
{
	// Call the base class first
	long lLastError = CSerialEx::Open(lpszDevice,dwInQueue,dwOutQueue);
	if (lLastError != ERROR_SUCCESS)
		return lLastError;

	// Save the window handle, notification message and user message
	m_hwndDest = hwndDest;
	m_nComMsg  = nComMsg?nComMsg:mg_nDefaultComMsg;
	m_lParam   = lParam;

	// Start the listener thread
	lLastError = StartListener();
	if (lLastError != ERROR_SUCCESS)
	{
		// Close the serial port
		Close();

		// Return the error-code
		m_lLastError = lLastError;
		return m_lLastError;
	}

	// Return the error
	m_lLastError = ERROR_SUCCESS;
	return m_lLastError;
}

LONG CTGSerialWnd::Close (void)
{
	// Reset all members
	m_hwndDest   = 0;
	m_nComMsg    = WM_NULL;

	// Call the base class
	return CSerialEx::Close();
}

void CTGSerialWnd::OnEvent (EEvent eEvent, EError eError)
{
	// Post message to the client window
	::PostMessage(m_hwndDest,m_nComMsg,MAKEWPARAM(eEvent,eError),LPARAM(m_lParam));
}
