////////////////////////////////////////////////////////////////////////////////
//
//	ESMOutputViewDlg.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "ScintillaWnd.h"
#include "ESMFindStringDlg.h"

// CESMOutputViewDlg 대화 상자입니다.

class CESMOutputViewDlg : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar	{virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

public:
	CESMOutputViewDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMOutputViewDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_VIEW_OUTPUT };
	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;		

	CExtEdit m_edtLogFilter;

	//-- Viewer
	CScintillaWnd m_wndScintilla;
	
	//-- To pause / start filter
	BOOL m_bPause;	

	//-- Find String
	CESMFindStringDlg * m_pWndESMOutputFindStirng;
	CStringList m_strFindstrings;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	afx_msg void OnSize(UINT nType, int cx, int cy);

	//-- Toolbar message
	//-- Toolbar message
	afx_msg void OnToggleBookMark();
	afx_msg void OnNextBookMark();
	afx_msg void OnPreviousBookMark();
	afx_msg void OnClearAllBookMarks();
	afx_msg void OnShowESMOutput();
	afx_msg void OnGotoLine();
	afx_msg void OnClearAllDocument();
	afx_msg void OnFindString();
	//-- 2012-09-21 hongsu@esmlab.com
	//-- Goto Last Line 
	afx_msg void OnGotoLast();	
	//{{AFX_MSG(ESMSerialViewDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void InitImageFrameWnd();
	
	void AddESMOutput(CString strToken, BOOL bLoad = FALSE);	//-- Add the serial Log
	void ShowESMOutput(CString strLog);						//-- To Check the pause state
	void ChangeToolBarImage();								//-- To Change the pause icon
	void GotoSelectedLine(CString strLineNum);				//-- Goto the selected line
	
	void FindNextString();									//-- Find next string
	void FindPreviousString();								//-- Find previou string

	void AddBookMarkAll(CString strLineNum);				//-- Add Filtering String Bookmark all
	BOOL CheckComboBox(CString strFindString);				//-- To check same string

	LRESULT OnSCIMsg(WPARAM w, LPARAM l);
	virtual BOOL OnNotify( WPARAM wParam, LPARAM lParam, LRESULT* pResult );

	void ClearOutputView();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
