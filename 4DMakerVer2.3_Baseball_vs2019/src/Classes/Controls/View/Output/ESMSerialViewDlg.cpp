////////////////////////////////////////////////////////////////////////////////
//
//	TGSerialViewDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


// Serial View 를 나타내는 Dialog(껍데기)

#include "stdafx.h"
#include "TG.h"
#include "TGSerialViewDlg.h"
#include "GlobalIndex.h"
#include "TGGotoLineSerialDlg.h"
#include "MainFrm.h"
#include "TGEdit.h"

// CTGSerialViewDlg 대화 상자입니다.

//IMPLEMENT_DYNAMIC(CTGSerialViewDlg, CDialog)
CTGSerialViewDlg::CTGSerialViewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTGSerialViewDlg::IDD, pParent)
{
	m_bPause = FALSE;
	m_nPart = 0;
	m_strSaveFilePath = _T("");	
	m_strHaltMemoryDump = _T("");
}

CTGSerialViewDlg::~CTGSerialViewDlg()
{
	int nFindStringCnt = 0;

	nFindStringCnt = m_strFindstrings.GetCount();

	if(nFindStringCnt != 0)
	{
		m_strFindstrings.RemoveAll();
	}
}

void CTGSerialViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SERIALCTRL, m_wndScintilla);
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
	DDX_Control(pDX, IDC_SERIAL_PROGRESS, m_ctrlSerialProgressBar);
}


BEGIN_MESSAGE_MAP(CTGSerialViewDlg, CDialog)
	ON_WM_SIZE()
	ON_COMMAND(ID_PRJ_OPEN			, OnReloadSerialLog)
	ON_COMMAND(ID_IMAGE_BK			, OnToggleBookMark)
	ON_COMMAND(ID_IMAGE_BK_PREV		, OnPreviousBookMark)
	ON_COMMAND(ID_IMAGE_BK_NEXT		, OnNextBookMark)
	ON_COMMAND(ID_IMAGE_BK_DEL		, OnClearAllBookMarks)	
	ON_COMMAND(ID_IMAGE_GOTOLINE	, OnGotoLine)
	ON_COMMAND(ID_IMAGE_FINDSTRING	, OnFindString)
	ON_COMMAND(ID_IMAGE_CLEARALL	, OnClearAllDocument)	
	ON_COMMAND(ID_IMAGE_PAUSE		, OnShowSerialLog)
	ON_COMMAND(ID_IMAGE_START		, OnShowSerialLog)
	ON_COMMAND(ID_IMAGE_REFILTERING	, OnReFiltering)
	ON_COMMAND(ID_IMAGE_OPT			, OnSerialOption)
	ON_COMMAND(ID_IMAGE_LINE_LAST	, OnGotoLast)
	ON_MESSAGE(WM_SCI, OnSCIMsg)
END_MESSAGE_MAP()


// CTGSerialViewDlg 메시지 처리기입니다.
BOOL CTGSerialViewDlg::OnInitDialog() 
{
	if(!CDialog::OnInitDialog())
	{
		ASSERT( FALSE );
		return FALSE;
	}	
	
 	m_wndScintilla.Init();
	
	//Add Progressbar
	m_ctrlSerialProgressBar.SetPos(0);
	m_ctrlSerialProgressBar.SetRange(0,100);

	return TRUE;
}

BOOL CTGSerialViewDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYUP)
	{
	switch(pMsg->wParam)
		{
		case VK_RETURN:
			break;
		case VK_UP:
			break;
		case VK_RIGHT:
			break;
		case VK_DOWN:
			break;
		case VK_SPACE:
			break;
		}		
	}
		
	return CDialog::PreTranslateMessage(pMsg);
}



void CTGSerialViewDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);	

	if(m_wndScintilla.GetSafeHwnd())
		m_wndScintilla.MoveWindow(0, 30, cx, cy-50);		

	if(m_ctrlSerialProgressBar.GetSafeHwnd())
		m_ctrlSerialProgressBar.MoveWindow(0, cy - 20, cx, cy);
	
	RepositionBars(0,0xFFFF,0);	
}

void CTGSerialViewDlg::InitImageFrameWnd()
{
	//---------------------------------------------------------------
	//--IMGWND_TYPE_TESTSTEP
	//---------------------------------------------------------------
	UINT arrCvTbBtns[] =
	{
		ID_PRJ_OPEN				,
		ID_SEPARATOR			,
		ID_IMAGE_BK				,
		ID_IMAGE_BK_PREV		,
		ID_IMAGE_BK_NEXT		,
		ID_IMAGE_BK_DEL			,
		ID_SEPARATOR			,
		ID_IMAGE_GOTOLINE		,
		ID_IMAGE_FINDSTRING		,
		ID_SEPARATOR			,
		ID_IMAGE_REFILTERING	,
		ID_IMAGE_CLEARALL		,	
		ID_SEPARATOR			,
		ID_IMAGE_PAUSE			,
		ID_SEPARATOR			,
		ID_IMAGE_OPT			,
		ID_SEPARATOR			,
		ID_IMAGE_LINE_LAST		,
	};
	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));
	RepositionBars(0,0xFFFF,0);	
	m_wndToolBar.ShowWindow(SW_SHOW);
}

//--To add Serial
void CTGSerialViewDlg::AddSerialLog(CString strSerialLog, BOOL bLoad)
{
	//TRACE(_T("ADD SERIAL :: %s\n"),strSerialLog);

	//-- For Tokenize
	CString strToken = _T("");
	int nPos = 0;

	//-- For View info
	long lLineCnt = 0;

	//-- To send the message
	TGEvent msgSci;
	TGFilteringInfo filteringInfo;

	//-- For Waiting Log Tokenize
	strToken = strSerialLog;
	//strToken.Replace(_T("\r\n"), _T("\n"));
	strToken.Replace(_T("\r"), _T(""));
	strToken.Replace(_T("\n"), _T("\r\n"));

	while(strToken != _T(""))
	{
		//-- Find the end line string
		nPos = strToken.Find(_T("\r\n"));
		//  [2/11/2010 jt48.lee]
		if(nPos > -1)
		{
			//-- Send the left string
			strSerialLog =strToken.Left(nPos);
			strToken = strToken.Mid(nPos + 2);

			strSerialLog.Append(_T("\r\n"));

			//--Check SerialView Exclude String
			if(LogExcludeFilter(strSerialLog))
			{
				if(bLoad == FALSE)
					m_wndScintilla.AppendText(strSerialLog);
				else
					m_wndScintilla.AppendText(strSerialLog, TRUE);
			} 
			
			//--Move cursor Last Text
			//--2012-07-29 dongho.shin
			long lDocLength = m_wndScintilla.GetTextLenght();
			m_wndScintilla.AddText(lDocLength, 0, 0);
			//	int ILineCnt=0;
			//	lLineCnt = m_wndScintilla.GetLineCount();
			//	m_wndScintilla.GotoLine(lLineCnt);	
			
			//--Check FilteringWindow Include String
			if(LogIncludeFilter(strSerialLog, bLoad))
			{
				m_wndScintilla.SendMessage(SCI_SETKEYWORDS, 0, (long)(LPCSTR)_T("display"));
				//			m_wndScintilla.Refresh();

				//-- Get the current line number
				long lLineCnt = m_wndScintilla.GetLineCount();

				filteringInfo.nLIneNum = lLineCnt - 1;
				filteringInfo.strSerialLog = strSerialLog;

				//-- Set the filtering info and send the message
				msgSci.message = WM_SCI_SER_FILTER_SEND;				
				msgSci.pParam	= (LPARAM)&filteringInfo;

				::SendMessage(m_hFilterView, WM_SCI, WM_SCI_SER_FILTER_SEND, (LPARAM)&msgSci);	
			}			
		}		
	}
}

//--Set the show mode. If you press the pause button,
//--the serial log saved the buffer.
void CTGSerialViewDlg::ShowSerialLog(CString strSerialLog)
{	
	//-- [2010-5-27 Lee JungTaek]
	//-- Revision To Ignore New Line Char
	int nPos = 0;
	CString strLog = _T("");
	CString strTime = _T("");

	//--Play status
	if(m_bPause == FALSE)
	{
		long lLineCnt = m_wndScintilla.GetLineCount();
		//-- Clear Serial View
// 		if(lLineCnt > MAX_LINE_COUNT)
// 			ClearSerialView();

		strLog = strSerialLog;
		nPos = strLog.Find(_T(']'));

		strTime = strLog.Left(nPos);
		strLog = strLog.Mid(nPos + 1);
		strLog.TrimLeft(_T(' '));

		if(strLog != _T("\r\n"))
			//--add the serial log
			AddSerialLog(strSerialLog);
	}	
}

//--Add Book Mark All
//--Filtering Window send the message.
//--Add BookMark to Filtered Serial Log
void CTGSerialViewDlg::AddBookMarkAll(CString strLineNum)
{
	int nLineNum = 0;
	nLineNum = _ttoi(strLineNum);
	
	//--Toggle -> goto line -> set focus
	m_wndScintilla.ToggleBookMark(nLineNum);
	m_wndScintilla.GotoLine(nLineNum);
	m_wndScintilla.SetFocus();
}

//--Goto Selected Line in Filtering Window
//--Filtering Window send the message.
void CTGSerialViewDlg::GotoSelectedLine(CString strLineNum)
{
	long lLineNum = 0;
	long lStartPos = 0;
	long lEndPos = 0;

	lLineNum = _ttoi(strLineNum);

	//Go to line
	m_wndScintilla.GotoLine(lLineNum);
	m_wndScintilla.SetFocus();

	//--To select the line
	lStartPos = m_wndScintilla.GetCurrentLineEndPosition(lLineNum - 1);
	lEndPos = m_wndScintilla.GetCurrentPosition();

	m_wndScintilla.SetSelection(lStartPos, lEndPos);
}

//-- Save the filtering string
void CTGSerialViewDlg::SaveFilteringString(CStringList * listFilteringString)
{
	POSITION posList = 0;
	int nPos = 0;

	CString strLineInfo = _T("");

	CStringArray arrayFrom;
	CStringArray arrayTo;

	CString strFromLineNumber = _T("");
	CString strToLineNumber = _T("");

	CString strFilteringInfo = _T("");

	int nListCount = 0;
	int nFrom = 0;
	int nTo = 0;

	//--Get the Filtering info
	nListCount = listFilteringString->GetCount();

	m_ctrlSerialProgressBar.SetRange(0, nListCount);

	posList = listFilteringString->GetHeadPosition();

	while(posList)
	{
		strLineInfo = listFilteringString->GetNext(posList);
		
		nPos = strLineInfo.Find(_T(","));

		strFromLineNumber = strLineInfo.Left(nPos);
		strToLineNumber = strLineInfo.Mid(nPos + 1);

		if(strToLineNumber.Compare(_T("End")) == 0)
		{
			nTo = m_wndScintilla.GetLineCount();

			strToLineNumber.Format(_T("%d"), nTo);
		}

		//-- Get the pair of line
		arrayFrom.Add(strFromLineNumber);
		arrayTo.Add(strToLineNumber);
	}

	CString strText = _T("");

	//-- Get the current line text
	for(int i = 0; i < nListCount; i++)
	{
		nFrom = _ttoi(arrayFrom.GetAt(i));
		nTo = _ttoi(arrayTo.GetAt(i));

		//-- Display filtering info
		m_nPart++;	
		if(m_nPart == 1)
			strFilteringInfo.Format(_T("Part %d\n"), m_nPart);
		else
			strFilteringInfo.Format(_T("\nPart %d\n"), m_nPart);
		strText += strFilteringInfo;

		//-- To line count
		for(int nLineNum = nFrom -1; nLineNum < nTo -1; nLineNum++)
		{
			strText += m_wndScintilla.GetLineText(nLineNum) + _T("\n");
		}		

		//-- Set the ProgressBar position
		m_ctrlSerialProgressBar.SetPos(i + 1);
	}
	
	//-- Write the file
	SaveFile(strText);

	//-- Clear buffer
	listFilteringString->RemoveAll();
	m_nPart = 0;

	//-- Init the progress bar
	Sleep(200);
	m_ctrlSerialProgressBar.SetPos(0);

	//-- Remove StringArray
	//-- 2012-05-22 hongsu
	int nAll;
	nAll = arrayFrom.GetSize();
	while(nAll--)
		arrayFrom.RemoveAt(nAll);
	arrayFrom.RemoveAll();

	nAll = arrayTo.GetSize();
	while(nAll--)
		arrayTo.RemoveAt(nAll);
	arrayTo.RemoveAll();
}

//-- Write the file
void CTGSerialViewDlg::SaveFile(CString strFilteringString, BOOL bSavePath)
{
	//-- Set Save File Name
	if(bSavePath == TRUE)
	{
		m_strSaveFilePath = strFilteringString;
		return;
	}
	
	//-- File open
	FILE *fOut;

	if((fOut = _tfopen(m_strSaveFilePath, _T("w+"))) == NULL)
	{
		TRACE(_T("Can't save the file"));

		return;
	}
	else
	{
		//-- Write the string
		_ftprintf(fOut, _T("%s\n"), strFilteringString);

		//-- Close the file
		fclose(fOut);
	}	
}

//-- Add or remove BookMark
void CTGSerialViewDlg::OnToggleBookMark()
{
	int nLineCnt = m_wndScintilla.GetCurrentLine();

	m_wndScintilla.ToggleBookMark(nLineCnt);
}

//-- Move next Bookmark
void CTGSerialViewDlg::OnNextBookMark()
{
	m_wndScintilla.FindNextBookmark();
}

//-- Move previous Bookmark
void CTGSerialViewDlg::OnPreviousBookMark()
{
	m_wndScintilla.FindPreviousBookmark();
}

//-- Clear all Bookmark
void CTGSerialViewDlg::OnClearAllBookMarks()
{
	m_wndScintilla.ClearAllBookMarks();
}

//-- To pause / start
void CTGSerialViewDlg::OnShowSerialLog()
{
	if(m_bPause == TRUE)
	{
		m_bPause = FALSE;
		ShowSerialLog(_T(""));

		ChangeToolBarImage();
	}
	else
	{
		m_bPause = TRUE;

		//--To change the button image.
		ChangeToolBarImage();		
	}
}

void CTGSerialViewDlg::ChangeToolBarImage()
{
	if(m_bPause == TRUE)
	{
		UINT arrCvTbBtns[] =
		{
			ID_PRJ_OPEN,
			ID_SEPARATOR,
			ID_IMAGE_BK,
			ID_IMAGE_BK_PREV,
			ID_IMAGE_BK_NEXT,
			ID_IMAGE_BK_DEL,
			ID_SEPARATOR,
			ID_IMAGE_GOTOLINE,
			ID_IMAGE_FINDSTRING,
			ID_SEPARATOR,
			ID_IMAGE_REFILTERING,
			ID_IMAGE_CLEARALL,	
			ID_SEPARATOR,
			ID_IMAGE_START,
			ID_SEPARATOR,
			ID_IMAGE_LINE_LAST,
		};
		VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));
		RepositionBars(0,0xFFFF,0);	
		m_wndToolBar.ShowWindow(SW_SHOW);
	}
	else
	{
		UINT arrCvTbBtns[] =
		{
			ID_PRJ_OPEN,
			ID_SEPARATOR,
			ID_IMAGE_BK,
			ID_IMAGE_BK_PREV,
			ID_IMAGE_BK_NEXT,
			ID_IMAGE_BK_DEL,
			ID_SEPARATOR,
			ID_IMAGE_GOTOLINE,
			ID_IMAGE_FINDSTRING,
			ID_SEPARATOR,
			ID_IMAGE_REFILTERING,
			ID_IMAGE_CLEARALL,	
			ID_SEPARATOR,
			ID_IMAGE_PAUSE,
			ID_SEPARATOR,
			ID_IMAGE_LINE_LAST,
		};
		VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));
		RepositionBars(0,0xFFFF,0);	
		m_wndToolBar.ShowWindow(SW_SHOW);
	}
}

//--Load the log file
void CTGSerialViewDlg::OnReloadSerialLog()
{
	//Pause the show serial
	m_bPause = TRUE;

	CString strLogPath = TGGetPath(TG_PATH_LOG_SERIAL_DATE);
	long lLastPos = 0;
	TGEvent msgSci;

	int nFileNameLength = 0;
	CString strLog = _T("");

	//-- Create an Open dialog
	TCHAR szFilters[]= _T("Log Files (*.log)|*.log|All Files (*.*)|*.*||");
	CFileDialog fileDlg (TRUE, _T("log"), _T("*.log"),OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, szFilters, this);
	fileDlg.m_ofn.lpstrInitialDir = strLogPath;
	
	//-- Open File Dialog
	if(fileDlg.DoModal ()==IDOK)
	{
		//-- Check the file format
		strLogPath = fileDlg.GetPathName();
		nFileNameLength = strLogPath.GetLength();
		strLog = strLogPath.Mid(nFileNameLength - 4);

		if(strLog.CompareNoCase(_T(".log")) != 0)
		{
			AfxMessageBox(_T("Please open the log file"));
			m_bPause = FALSE;
			return;
		}
	}	
	else
	{
		m_bPause = FALSE;
		return;
	}

	//-- Clear Serial View
	m_wndScintilla.SetText(_T(""));

	//-- Clear Filter View
	msgSci.message = WM_SCI_SER_FILTER_CLEAR;
	::SendMessage(m_hFilterView, WM_SCI, WM_SCI_SER_FILTER_CLEAR, (LPARAM)&msgSci);
	
	//-- Write Text To Serial
	m_wndScintilla.LoadFile(strLogPath, m_ctrlSerialProgressBar);

	//-- Set Cursor
 	lLastPos = m_wndScintilla.GetTextLenght();
 	m_wndScintilla.GotoPosition(lLastPos);

	//-- Set ProgressBar init
	Sleep(200);
	m_ctrlSerialProgressBar.SetPos(0);

	m_bPause = FALSE;
}

//-- To goto function using dialog
void CTGSerialViewDlg::OnGotoLine()
{
	CTGGotoLineSerialDlg dlg;

	long lLineCnt;
	long lStartPos;
	long lEndPos;	

	lLineCnt = m_wndScintilla.GetLineCount();

	//-- Set the dialog title
	dlg.SetLineNumberTitle(1, lLineCnt);
	dlg.m_nLineNumber = m_wndScintilla.GetCurrentLine();

	if(dlg.DoModal() == IDOK)
	{
		m_wndScintilla.GotoLine(dlg.m_nLineNumber);

		//--To select the line
		lStartPos = m_wndScintilla.GetCurrentLineEndPosition(dlg.m_nLineNumber - 1);
		lEndPos = m_wndScintilla.GetCurrentPosition();

		m_wndScintilla.SetSelection(lStartPos, lEndPos);
	}
}

//-- Clear All Document
void CTGSerialViewDlg::OnClearAllDocument()
{
	m_wndScintilla.SetText(_T(""));
}

//-- Find String
void CTGSerialViewDlg::OnFindString()
{
	//-- Create Find String dialog
	m_wndSerilaFindStirng = new CTGSerialFindStringDlg();
	
	m_wndSerilaFindStirng->Create(CTGSerialFindStringDlg::IDD);
	m_wndSerilaFindStirng->ShowWindow(SW_SHOW);

	//-- Dialog Init
	m_wndSerilaFindStirng->m_wndTGSerialViewDlg = this->GetSafeHwnd();
	m_wndSerilaFindStirng->InitDialog(&m_strFindstrings);
}

//-- Find Next String
void CTGSerialViewDlg::FindNextString()
{
	CString strFindString = _T("");
	int nSearchFlag = 0;

	//-- Get the find option
	nSearchFlag = m_wndSerilaFindStirng->SetSearchFlags();

	//-- Get the find string
	strFindString = m_wndSerilaFindStirng->m_strFindString;

	//-- Add the old find string
	if(CheckComboBox(strFindString))
		m_strFindstrings.AddHead(strFindString);

	//-- Set the find option
	m_wndScintilla.SetSearchflags(nSearchFlag);

	//-- Find the string
	if(!m_wndScintilla.SearchForward(strFindString.LockBuffer()))
	{
		AfxMessageBox(_T("Can't search the string"));

		return;
	}

	//-- Release
	strFindString.UnlockBuffer();
}

//-- Find Previous String
void CTGSerialViewDlg::FindPreviousString()
{
	CString strFindString = _T("");
	int nSearchFlag = 0;

	//-- Get the find option
	nSearchFlag = m_wndSerilaFindStirng->SetSearchFlags();

	//-- Get the find string
	strFindString = m_wndSerilaFindStirng->m_strFindString;

	//-- Add the old find string
	if(CheckComboBox(strFindString))
		m_strFindstrings.AddHead(strFindString);

	//-- Set the find option
	m_wndScintilla.SetSearchflags(nSearchFlag);

	//-- Find the string
	if(!m_wndScintilla.SearchBackward(strFindString.LockBuffer()))
	{
		AfxMessageBox(_T("Can't search the string"));

		return;
	}

	//-- Release
	strFindString.UnlockBuffer();
}

//-- Check the same string
BOOL CTGSerialViewDlg::CheckComboBox(CString strFindString)
{
	CString strPreFindString = _T("");
	POSITION pos;
	int nSameString = 0;
	
	pos = m_strFindstrings.GetHeadPosition();

	//-- No String array
	if(pos == 0 && strPreFindString.IsEmpty() == FALSE)
	{
		return TRUE;
	}

	//-- Find the same string
	while(pos)
	{
		strPreFindString = m_strFindstrings.GetNext(pos);
		
		if(strFindString.Compare(strPreFindString) == 0)
		{
			nSameString++;
		}
	}

	//-- No same string
	if(nSameString == 0)
		return TRUE;

	return FALSE;
}

//-- [2/19/2010 Lee JungTaek]
//-- Refiltering
void CTGSerialViewDlg::OnReFiltering()
{
	m_bPause = TRUE;

	TGEvent msgSci;
	BOOL bReFiltering = TRUE;

	//-- Clear Filter View
	msgSci.message = WM_SCI_SER_FILTER_CLEAR;
	::SendMessage(m_hFilterView, WM_SCI, WM_SCI_SER_FILTER_CLEAR, (LPARAM)&msgSci);

	//1. Get the serial string
	long lLinecnt = 0;
	CString strSerialLog = _T("");
	TGFilteringInfo filteringInfo;

	lLinecnt = m_wndScintilla.GetLineCount();

	for(long lLineNum = 0; lLineNum < lLinecnt; lLineNum++)
	{
		strSerialLog = m_wndScintilla.GetLineText(lLineNum);

		if(LogIncludeFilter(strSerialLog, bReFiltering))
		{
			filteringInfo.nLIneNum = lLineNum + 1;
			filteringInfo.strSerialLog = strSerialLog;

			//-- Set the filtering info and send the message
			msgSci.message = WM_SCI_SER_FILTER_SEND;				
			msgSci.pParam	= (LPARAM)&filteringInfo;

			::SendMessage(m_hFilterView, WM_SCI, WM_SCI_SER_FILTER_SEND, (LPARAM)&msgSci);	
		}
	}

	m_bPause = FALSE;
	bReFiltering = FALSE;
}

//--Filter
//--Filter 추가
BOOL CTGSerialViewDlg::LogIncludeFilter(CString strSerialLog, BOOL bLoad)
{
	CString strPutMessage = _T("");

	//--check include
	for(int i = 0; i < sizeof(m_tgLogFilter->bInCheck)/sizeof(BOOL); ++i )
	{
		if(m_tgLogFilter->bInCheck[i])
		{
			//-- 2009-12-26 hongsu.jung
			//-- Non Tokenized
			if(strSerialLog.Find(m_tgLogFilter->strInclude[i]) != -1)
			{
				//-- Don't put the message in Load mode
				if(bLoad == FALSE)
				{
					//-- Check the put message
					if(m_tgLogFilter->bInPutCheck[i])
					{
						//-- Get the message
						strPutMessage = m_tgLogFilter->strInPutMsg[i];

						//-- Pause the send the serial message
						LogPutMessage(strPutMessage);	

						return TRUE;
					}
				}				

				return TRUE;
			}			
		}
	}

	return FALSE;
}

//-- To exclude serial log
BOOL CTGSerialViewDlg::LogExcludeFilter(CString strSerialLog)
{
	//--check exclude
	for(int i = 0; i < sizeof(m_tgLogFilter->bExCheck)/sizeof(BOOL); ++i)
	{
		if(m_tgLogFilter->bExCheck[i])
		{
			//-- 2009-12-26 hongsu.jung
			//-- Non Tokenized
			if( strSerialLog.Find(m_tgLogFilter->strExclude[i]) != -1 )
				return FALSE;			
		}
	}

	return TRUE;
}

//-- To put the message
BOOL CTGSerialViewDlg::LogPutMessage(CString strPutMessage)
{
	int nLength = strPutMessage.GetLength();
	m_wndScintilla.AppendText(strPutMessage);
 
 	TCHAR tszMsg = {0}; 	
 	for(int i = 0; i < nLength ; i++)
 	{
 		tszMsg = strPutMessage.GetAt(i); 
 		m_wndScintilla.WriteChar((UINT)tszMsg);
		Sleep(10);
 	}
 
	//-- Send the char
 	m_wndScintilla.WriteChar(13);	//CR
	//-- 2011-5-16 haneol.Lee
	m_wndScintilla.WriteChar(10);	//LF
	return TRUE;
}

LRESULT CTGSerialViewDlg::OnSCIMsg(WPARAM w, LPARAM l)
{
	TGEvent* pMsg = (TGEvent*)l;
	if(!pMsg)
		return 0L;

	switch((int)w)
	{
	case WM_SCI_FILTER_SER_BOOKMARKALL:
		AddBookMarkAll((CString)(LPCTSTR)pMsg->pParam);

		break;
	case WM_SCI_FILTER_SER_GOTO:
		GotoSelectedLine((CString)(LPCTSTR)pMsg->pParam);
		break;

	case WM_SCI_SER_LOAD:
		AddSerialLog((CString)(LPCTSTR)pMsg->pParam, TRUE);
		break;

	case WM_SCI_FILTER_SER_SAVE:
		SaveFilteringString((CStringList*)pMsg->pParam);
		break;

	case WM_SCI_FILTER_SER_SAVE_PATH:
		SaveFile((CString)(LPCTSTR)pMsg->pParam, TRUE);
		break;

	case WM_SCI_SER_FIND_NEXT:
		FindNextString();
		break;

	case WM_SCI_SER_FIND_PRE:
		FindPreviousString();
		break;

	case WM_SCI_SER_POPUP_FIND_DLG:
		OnFindString();
		break;

	default:
		break;
	}
	return 0L;
}

//-- To check the scintillaWnd event
BOOL CTGSerialViewDlg::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	NMHDR *phDR;
	phDR = (NMHDR*)lParam;

	//-- ScintillaWnd notiry
	if (phDR != NULL && phDR->hwndFrom == m_wndScintilla.m_hWnd)
	{
		SCNotification *pMsg = (SCNotification*)lParam;
		switch (phDR->code)
		{
		case SCN_MARGINCLICK:
			m_wndScintilla.DoToggleBookMark(pMsg->margin, pMsg->position);
			break;
		}
		return TRUE;
	}
	return CWnd::OnNotify(wParam, lParam, pResult);
}

void CTGSerialViewDlg::ClearSerialView()
{
	CString strRemainText = _T("");

	for(int i = MAX_LINE_COUNT / 2; i < MAX_LINE_COUNT; i++)
	{
		strRemainText.Append(m_wndScintilla.GetLineText(i));
		strRemainText.Append(_T("\r\n"));
	}

	m_wndScintilla.SetText(_T(""));

	CString strToken = strRemainText;
	strToken.Replace(_T("\r\n"), _T("\n"));
	strToken.Replace(_T("\n"), _T("\r\n"));	

	int nPos;

	while(strToken != _T(""))
	{
		nPos = strToken.Find(_T("\r\n"));

		if(nPos > -1)
		{
			//-- Send the left string
			strRemainText =strToken.Left(nPos);
			strToken = strToken.Mid(nPos + 2);

			m_wndScintilla.AppendText(strRemainText);
		}
	}
}

void CTGSerialViewDlg::OnSerialOption()
{
	TGEvent* pMsg = new TGEvent ;
	pMsg->message = WM_TG_OPT_SERIAL;
	pMsg->nParam  = GetIndex();
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_TG, WM_TG_OPT, (LPARAM)pMsg);	
}


//-- 2012-09-21 hongsu@esmlab.com
//-- Goto Last Line 
void CTGSerialViewDlg::OnGotoLast()
{
	m_wndScintilla.GotoLastLine();
}