////////////////////////////////////////////////////////////////////////////////
//
//	ESMOutputBaseView.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "ScintillaWnd.h"

class CESMOutputBaseView : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar
	{
		virtual CExtBarContentExpandButton* OnCreateBarRightBtn()	{ return NULL; }
		virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam)		{ ((CESMOutputBaseView*)GetOwner())->OnToolbarCommand(wParam, lParam); return TRUE; }
	};

public:
	enum { IDD = IDD_VIEW_ESM_OUTPUT_BASE };

	CESMOutputBaseView(CWnd* pParent=NULL);   
	//virtual ~CESMOutputBaseView();

	virtual BOOL InitImageFrameWnd(int nToolBarID) = 0;
	BOOL AppendText(CString strText);
	virtual BOOL OnToolbarCommand(WPARAM wParam, LPARAM lParam) = 0;


protected:
	void Clear();

	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;
	BOOL m_bPause;
	CArray<UINT, UINT> m_arrTBBtn;

private:
	CScintillaWnd m_wndScintilla;

protected:	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMOutputBaseView)
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    
	//}}AFX_VIRTUAL
	DECLARE_MESSAGE_MAP()

protected:
	// Generated message map functions
	//{{AFX_MSG(CESMOutputBaseView)	
	//}}AFX_MSG	
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	virtual BOOL OnNotify( WPARAM wParam, LPARAM lParam, LRESULT* pResult);
};
