////////////////////////////////////////////////////////////////////////////////
//
//	ESMSerialFindStringDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMFindStringDlg.h"
#include "Scintilla.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CESMFindStringDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMFindStringDlg, CDialog)

CESMFindStringDlg::CESMFindStringDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMFindStringDlg::IDD, pParent)
	, m_strFindString(_T(""))
	, m_bMatchCase(FALSE)
	, m_bMatchWords(FALSE)
{
	m_bForward = FALSE;
	m_wndESMSerialViewDlg = NULL;
}

CESMFindStringDlg::~CESMFindStringDlg()
{
	delete this;
}

void CESMFindStringDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_CBString(pDX, IDC_FIND_STRING_COMBO, m_strFindString);
	DDX_Check(pDX, IDC_MATCHCASE, m_bMatchCase);
	DDX_Check(pDX, IDC_MATCHWORDS, m_bMatchWords);
	DDX_Control(pDX, IDC_FIND_STRING_COMBO, m_ctrlFindStrings);
	DDX_Control(pDX, IDC_PRE_FIND, m_ctrlPreButton);
	DDX_Control(pDX, IDC_NEXT_FIND, m_ctrlNextButton);
}


BEGIN_MESSAGE_MAP(CESMFindStringDlg, CDialog)
	ON_BN_CLICKED(IDC_PRE_FIND, &CESMFindStringDlg::OnBnClickedPreFind)
	ON_BN_CLICKED(IDC_NEXT_FIND, &CESMFindStringDlg::OnBnClickedNextFind)
END_MESSAGE_MAP()


BOOL CESMFindStringDlg::InitDialog(CStringList *strFindStrings) 
{
	POSITION pos;
	CString strFindString = _T("");

	pos = strFindStrings->GetHeadPosition();

	while(pos)
	{
		strFindString = strFindStrings->GetNext(pos);
		m_ctrlFindStrings.AddString(strFindString);
	}

	return TRUE;
}

//-- Set the find option
int CESMFindStringDlg::SetSearchFlags()
{
	int nSearchFlags = 0;

	if(m_bMatchCase)
		nSearchFlags |= SCFIND_MATCHCASE;
	if(m_bMatchWords)
		nSearchFlags |= SCFIND_WHOLEWORD;

	return nSearchFlags;
}

//-- Check the same string in list
BOOL CESMFindStringDlg::CheckComboBox(CString strFindString)
{
	int nTopIndex = 0;
	int nListCnt = 0;

	nTopIndex = m_ctrlFindStrings.GetTopIndex();
	nListCnt = m_ctrlFindStrings.GetCount();

	//-- Add the find string
	if(strFindString.IsEmpty() == FALSE)
	{
		//-- ComboBox empty
		if(nListCnt == 0)
			return TRUE;
	
		//-- Search the same string
		for(int i = nTopIndex; i < nListCnt; i++)
		{
			if(m_ctrlFindStrings.FindStringExact(i, strFindString) == -1)
				return TRUE;
		}
	}		

	return FALSE;
}

// CESMFindStringDlg 메시지 처리기입니다.
void CESMFindStringDlg::OnBnClickedPreFind()
{
	m_bForward = FALSE;

	//-- Get the control's value
	UpdateData();

	//-- Declare local value
	CString strFindString = _T("");
	int nTopIndex = 0;

	strFindString = m_strFindString;
	nTopIndex = m_ctrlFindStrings.GetTopIndex();

	//-- Check the same string. If no, add string
	if(CheckComboBox(strFindString))
		m_ctrlFindStrings.InsertString(nTopIndex, strFindString);

	//-- Send the notify
	SCIEvent msgSci;
	msgSci.message = WM_SCI_SER_FIND_PRE;

	::SendMessage(m_wndESMSerialViewDlg, WM_SCI, WM_SCI_SER_FIND_PRE, (LPARAM)&msgSci);	
}

void CESMFindStringDlg::OnBnClickedNextFind()
{	
	m_bForward = TRUE;

	//-- Get the control's value
	UpdateData();

	//-- Declare local value
	CString strFindString = _T("");
	int nTopIndex = 0;

	strFindString = m_strFindString;
	nTopIndex = m_ctrlFindStrings.GetTopIndex();

	//-- Check the same string. If no, add string
	if(CheckComboBox(strFindString))
		m_ctrlFindStrings.InsertString(nTopIndex, strFindString);

	//-- Send the notify
	SCIEvent msgSci;
	msgSci.message = WM_SCI_SER_FIND_NEXT;
	::SendMessage(m_wndESMSerialViewDlg, WM_SCI, WM_SCI_SER_FIND_NEXT, (LPARAM)&msgSci);
}

BOOL CESMFindStringDlg::PreTranslateMessage(MSG* pMsg)
{
	short ctrl;
	ctrl= GetAsyncKeyState(VK_CONTROL);

	if(pMsg->message == WM_KEYDOWN)
	{
		//-- Check the key event
		if(pMsg->wParam == VK_RETURN)
		{
			if(m_bForward == TRUE)
			{
				m_ctrlNextButton.SetFocus();
				OnBnClickedNextFind();	
				m_ctrlNextButton.SetFocus();
			}
			else
			{
				m_ctrlPreButton.SetFocus();
				OnBnClickedPreFind();
				m_ctrlPreButton.SetFocus();
			}

			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}
