////////////////////////////////////////////////////////////////////////////////
//
//	TGSerialViewDlg.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "resource.h"
#include "ScintillaWnd.h"
#include "afxcmn.h"
#include "TGTextProgressCtrl.h"
#include "TGSerialFindStringDlg.h"

// CTGSerialViewDlg 대화 상자입니다.

class CTGSerialViewDlg : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar	{virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

public:
	CTGSerialViewDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTGSerialViewDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SERIAL_WINDOW };
	CInnerToolControlBar m_wndToolBar;		

	//-- Viewer
	CScintillaWnd m_wndScintilla;
	//-- To pause / start filter
	BOOL m_bPause;	
	//-- Filter
	HWND m_hFilterView;	
	TGLogFilter *m_tgLogFilter;	
	int m_nPart;
	//-- Filter save
	CString m_strSaveFilePath;
	//-- Find String
	CTGSerialFindStringDlg * m_wndSerilaFindStirng;
	CStringList m_strFindstrings;	
	//ProgressBar
	CTGTextProgressCtrl m_ctrlSerialProgressBar;
	CString m_strHaltMemoryDump;


private:
	int m_nIndex;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	afx_msg void OnSize(UINT nType, int cx, int cy);

	//-- Toolbar message
	afx_msg void OnToggleBookMark();
	afx_msg void OnNextBookMark();
	afx_msg void OnPreviousBookMark();
	afx_msg void OnClearAllBookMarks();
	afx_msg void OnShowSerialLog();
	afx_msg void OnReloadSerialLog();
	afx_msg void OnGotoLine();
	afx_msg void OnClearAllDocument();
	afx_msg void OnFindString();
	afx_msg void OnReFiltering();
	afx_msg void OnSerialOption();	
	//-- 2012-09-21 hongsu@esmlab.com
	//-- Goto Last Line 
	afx_msg void OnGotoLast();	
	//{{AFX_MSG(TGSerialViewDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void InitImageFrameWnd();
	void AddSerialLog(CString strSerialLog, BOOL bLoad = FALSE);			//-- Add the serial Log
	void ShowSerialLog(CString strSerialLog);			//-- To Check the pause state
	void ChangeToolBarImage();							//-- To Change the pause icon
	void AddBookMarkAll(CString strLineNum);			//-- Add Filtering String Bookmark all
	void GotoSelectedLine(CString strLineNum);			//-- Goto the selected line
	void SaveFilteringString(CStringList * listFilteringString);	//-- Save the Filtering string
	void SaveFile(CString strFilteringString, BOOL bSavePath = FALSE);

	BOOL LogIncludeFilter(CString strSerialLog, BOOL bLoad = FALSE);		//-- Check the include string
	BOOL LogExcludeFilter(CString strSerialLog);		//-- Check the exclude string
	BOOL LogPutMessage(CString strPutMessage);			//-- Put the Message
	virtual BOOL PreTranslateMessage(MSG *pMsg);

	void FindNextString();								//-- Find next string
	void FindPreviousString();							//-- Find previou string
	BOOL CheckComboBox(CString strFindString);			//-- To check same string

	LRESULT OnSCIMsg(WPARAM w, LPARAM l);
    virtual BOOL OnNotify( WPARAM wParam, LPARAM lParam, LRESULT* pResult );

	void ClearSerialView();
	//-- 2012-07-23 hongsu@esmlab.com
	//-- Index
	void SetIndex(int n)	{ m_nIndex = n ; }
	int GetIndex()			{ return m_nIndex; }
};
