////////////////////////////////////////////////////////////////////////////////
//
//	TGFilteringListCtrl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////



#include "stdafx.h"
#include "TGFilteringListCtrl.h"
#include "ComboItem.h"

//-- Check the ListView column type
//-- 1 : ComboBox
static int _List_Type(int col)
{
	if(col == 1)
		return 1;
}

CTGFilteringListCtrl::CTGFilteringListCtrl(void)
{
}

CTGFilteringListCtrl::~CTGFilteringListCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CTGFilteringListCtrl, CListCtrl)
	ON_WM_PAINT()
	ON_WM_LBUTTONDBLCLK()
	ON_NOTIFY_REFLECT(NM_CLICK, &CTGFilteringListCtrl::OnNMClick)

END_MESSAGE_MAP()

void CTGFilteringListCtrl::AddFilteredLog(CString strLineNum, CString strSerialLog)
{
	//-- Get the list count
	int nRow = GetItemCount();

	CSize sizeScroll;
	sizeScroll.cy = 100;

	//-- Insert Data
	this->InsertItem(nRow, strLineNum);
	this->SetItemText(nRow, 1, strSerialLog);
 
	//-- Scroll last line
 	this->Scroll(sizeScroll);
}

void CTGFilteringListCtrl::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	GetParent()->SendMessage(WM_SCI, WM_SCI_FILTER_SER_GOTO);
}


//Create ComboBox
CComboBox * CTGFilteringListCtrl::ComboItem(int nItem, int nSubItem)
{
#define IDC_COMBOBOXINLISTVIEW 0x1235

	CString strFind = GetItemText(nItem, nSubItem);

	//basic code start
	CRect rect;
	int offset = 0;
	// Make sure that the item is visible
	if( !EnsureVisible(nItem, TRUE)) 
		return NULL;

	GetSubItemRect(nItem, nSubItem, LVIR_BOUNDS, rect);
	// Now scroll if we need to expose the column
	CRect rcClient;
	GetClientRect(rcClient);
	if( offset + rect.left < 0 || offset + rect.left > rcClient.right )
	{
		CSize size;
		size.cx = offset + rect.left;
		size.cy = 0;
		Scroll(size);
		rect.left -= size.cx;
	}

	rect.left += offset;	
	rect.right = rect.left + GetColumnWidth(nSubItem);
	if(rect.right > rcClient.right) 
		rect.right = rcClient.right;
	//basic code end

	rect.bottom += 5 * rect.Height();//dropdown area

	DWORD dwStyle =  WS_CHILD | WS_VISIBLE |CBS_DROPDOWNLIST | CBS_DISABLENOSCROLL;//| WS_VSCROLL | WS_HSCROLL|CBS_DROPDOWNLIST | CBS_DISABLENOSCROLL;
	CComboBox *pList = new CComboItem(nItem, nSubItem, &m_strList);
	pList->Create(dwStyle, rect, this, IDC_COMBOBOXINLISTVIEW);
	pList->ModifyStyleEx(0,WS_EX_CLIENTEDGE);//can we tell at all

	pList->ShowDropDown();
	pList->SelectString(-1, strFind.GetBuffer(1));
	strFind.ReleaseBuffer();
	// The returned pointer should not be saved
	return pList;
}


void CTGFilteringListCtrl::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( GetFocus() != this) 
		SetFocus();

	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	if (m_fGetType && m_fGetType( pNMListView->iSubItem ) == TRUE)
		ComboItem(pNMListView->iItem, pNMListView->iSubItem);

	*pResult = 0;
}

void CTGFilteringListCtrl::OnPaint() 
{
	Default();
	
	//-- [2010-3-19 Lee JungTaek]
	//-- Add Default String
	if (GetItemCount() <= 0 && !m_emptyMessage.IsEmpty())
	{
		CDC* pDC = GetDC();
		int nSavedDC = pDC->SaveDC();

		CRect rc;
		GetWindowRect(&rc);
		ScreenToClient(&rc);
		CHeaderCtrl* pHC = GetHeaderCtrl();
		if (pHC != NULL)
		{
			CRect rcH;
			pHC->GetItemRect(0, &rcH);
			rc.top += rcH.bottom;
		}
		rc.top += 10;

		COLORREF crText =  CListCtrl::GetTextColor();

		pDC->SetTextColor(crText);
		int oldMode = pDC->SetBkMode(TRANSPARENT);

		pDC->SelectStockObject(ANSI_VAR_FONT);
		pDC->DrawText(m_emptyMessage, -1, rc, DT_CENTER | DT_WORDBREAK | DT_NOPREFIX | DT_NOCLIP);
		pDC->SetBkMode(oldMode);
		pDC->RestoreDC(nSavedDC);
		ReleaseDC(pDC);
	}
}

void CTGFilteringListCtrl::SetEmptyMessage(const CString &message)
{
	m_emptyMessage = message;
}