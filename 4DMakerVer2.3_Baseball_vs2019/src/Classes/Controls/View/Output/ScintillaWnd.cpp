////////////////////////////////////////////////////////////////////////////////
//
//	ScintillaWnd.cpp : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////
//Serial View 의 Edit Control

#include "stdafx.h"
#include "ScintillaWnd.h"

#include "Accessor.h"
#include "Propset.h"
#include "keywords.h"

#include <fstream>
#include <io.h>

#include "ESMTextProgressCtrl.h"
#include "ESMUtil.h"
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

BEGIN_MESSAGE_MAP(CScintillaWnd, CWnd)
	//{{AFX_MSG_MAP(CScintillaWnd)
	ON_WM_CHAR()
	ON_COMMAND(ID_SCINTILLAVIEW_COPY		, OnCopy)
	ON_COMMAND(ID_SCINTILLAVIEW_SELECTALL	, OnSelectAll)
	ON_COMMAND(ID_SCINTILLAVIEW_CLEAR		, OnClear)	
END_MESSAGE_MAP()

/////////////////////////////////////
// @mfunc This is an empty constructor
// @rvalue void | not used
//
CScintillaWnd::CScintillaWnd()
{
   m_bLinenumbers = FALSE;
   m_bSelection = TRUE;
   m_bFolding = FALSE;

   m_nSearchflags = 0;
   m_lLastPos = 0;
   m_bFindString = FALSE;
   m_nDeviceNum = 0;
   m_bESMOutputView = FALSE;
   m_bEndCussor = TRUE;
}

/////////////////////////////////////
// @mfunc This is a destructor
// @rvalue void | not used
CScintillaWnd::~CScintillaWnd(){}

/////////////////////////////////////
// @mfunc Create the window
// @rvalue BOOL | TRUE on success else FALSE on error
BOOL CScintillaWnd::Create (LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
{
  	if (!CWnd::CreateEx(WS_EX_CLIENTEDGE, STR_SCINTILLAWND,lpszWindowName, dwStyle, rect, pParentWnd,(UINT)nID))
    {   
      LPVOID lpMsgBuf;
      ::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
               NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf,0,NULL);
      // Write to stderr
      TRACE (_T("%s\n"), (LPCTSTR)lpMsgBuf);
      // Free the buffer.
      LocalFree( lpMsgBuf );
      return FALSE;
	}

   return TRUE;
}
/////////////////////////////////////
// @mfunc Try to load the Scintilla dll - usually named "SciLexer.dll" or "Scintilla.dll".  We try to locate the dll in 
// the current dirtectory and along the path environment.
// Call this function in your CWinApp derived application in the InitInstance function by calling:<nl>
// CScintillaWnd::LoadScintillaDll()<nl>
// @rvalue BOOL | FALSE on error - TRUE on success
HMODULE CScintillaWnd::LoadScintillaDll (LPCSTR szDllFile) //@parm filename of the lexer dll - default "SciLexer.dll"
{
   CString strLexer = STR_LEXERDLL;
   if (szDllFile != NULL)
      strLexer = szDllFile;
// this call to LoadLibrary searches in:
// 1.) current directory
// 2.) wint
// 3.) winnt/system32
// 4.) path
	HMODULE hModule = ::LoadLibrary(strLexer);
// if load fails get an extended error message 
	if (hModule == NULL)
	{
      LPVOID lpMsgBuf;
      ::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
               NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf,0,NULL);
      // Write to stderr
      TRACE (_T("%s:%s\n"), (LPCTSTR)strLexer, (LPCTSTR)lpMsgBuf);
      // Free the buffer.
      LocalFree( lpMsgBuf );
      return NULL;
	}
   return hModule;
}


void CScintillaWnd::Init()
{
	// clear all text styles
	SendMessage(SCI_CLEARDOCUMENTSTYLE, 0, 0);
	// set the number of styling bits to 7 - the asp/html views need a lot of styling - default is 5
	// If you leave the default you will see twiggle lines instead of ASP code
	SendMessage(SCI_SETSTYLEBITS, 7, 0);
	// set the display for indetation guides to on - this displays virtical dotted lines from the beginning of 
	// a code block to the end of the block
	SendMessage(SCI_SETINDENTATIONGUIDES, TRUE, 0);
	// set tabwidth to 3
	SendMessage(SCI_SETTABWIDTH,3,0);
	// set indention to 3
	SendMessage(SCI_SETINDENT,3,0);
	// set the caret blinking time to 400 milliseconds
	SendMessage(SCI_SETCARETPERIOD,400,0);
	// source folding section
	
	// set the forground color for some styles
	
	SendMessage(SCI_STYLESETFORE, 0, RGB(0,0,0));
	SendMessage(SCI_STYLESETFORE, 2, RGB(0,64,0));
	SendMessage(SCI_STYLESETFORE, 5, RGB(0,0,255));
	SendMessage(SCI_STYLESETFORE, 6, RGB(200,20,0));
	SendMessage(SCI_STYLESETFORE, 9, RGB(0,0,255));
	SendMessage(SCI_STYLESETFORE, 10, RGB(255,0,64));
	SendMessage(SCI_STYLESETFORE, 11, RGB(0,0,0));
	

	COLORREF crBackground = RGB(0x2C, 0x2C, 0x2C);
	SendMessage(SCI_STYLESETBACK, STYLE_DEFAULT, (long)crBackground);
	SendMessage(SCI_STYLESETBACK, GetCurrentStyle(), (long)crBackground);
	COLORREF crForeground = RGB(0xFF, 0xFF, 0xFF);
	SendMessage(SCI_STYLESETFORE, STYLE_DEFAULT, (long)crForeground);
	SendMessage(SCI_STYLESETFORE, GetCurrentStyle(), (long)crForeground);
		
	// Set Font
	//SendMessage(SCI_STYLESETFONT, 0, (LPARAM)"Fixedsys"); //ESMUtil::GetCharString(_T("Fixedsys")));
	//SendMessage(SCI_STYLESETFONT, 0, (LPARAM)ESMUtil::GetCharString(_T("Segoe UI")));
	/*SendMessage(SCI_STYLESETFONT, 0, (LPARAM)"Segoe UI");*/
	SendMessage(SCI_STYLESETFONT, 0, (LPARAM)"Noto Sans CJK KR Regular");
	SendMessage(SCI_STYLESETSIZE, 0, (LPARAM)9);


	// set the backgroundcolor of brace highlights
	SendMessage(SCI_STYLESETBACK, STYLE_BRACELIGHT, RGB(0,255,0));
	// set end of line mode to CRLF
	SendMessage(SCI_CONVERTEOLS, 2, 0);
	SendMessage(SCI_SETEOLMODE, 2, 0);
	//   SendMessage(SCI_SETVIEWEOL, TRUE, 0);
	// set markersymbol for marker type 0 - bookmark
	SendMessage(SCI_MARKERDEFINE, 0, SC_MARK_ROUNDRECT);
	SendMessage(SCI_MARKERSETBACK, 0, RGB(200,20,0));
	//SendMessage(SC_MARK_BACKGROUND, 0, crBackground);
	// set the sensitive
	SendMessage(SCI_SETMARGINSENSITIVEN, 1, TRUE);
	// display all margins
	SetDisplayLinenumbers(TRUE);
	SetDisplayFolding(FALSE);
	SetDisplaySelection(TRUE);

	SendMessage(SCI_SETHSCROLLBAR, 0, 0);
	//SendMessage(SCI_SETVSCROLLBAR, 0, 0);
	//SendMessage(SCI_STYLECLEARALL, 0, 0L);

}

// About UI
//-- Set the linenumbers tab 
void CScintillaWnd::SetDisplayLinenumbers(
	BOOL bFlag) //@parm flag if we shuld display line numbers
{
	if(m_bESMOutputView)
		bFlag = FALSE;

	// if nothing changes just return
	if (GetDisplayLinenumbers() == bFlag)
		return;
	// if display is turned off we set margin 0 to 0
	if (!bFlag)
	{
		SendMessage(SCI_SETMARGINWIDTHN, 0, 0);
	}
	// if display is turned o we set margin 0 to the calculated width
	else
	{
		int nWidth = GetLinenumberWidth() + 4;
		SendMessage(SCI_SETMARGINWIDTHN, 0, nWidth);
	}
	m_bLinenumbers = bFlag;
}

//-- Set the folding tab
void CScintillaWnd::SetDisplayFolding(BOOL bFlag)
{
	m_bFolding = bFlag;
	if (bFlag)
		SendMessage(SCI_SETMARGINWIDTHN, 2, 16);
	else
		SendMessage(SCI_SETMARGINWIDTHN, 2, 0);
}

//-- Set the selection
void CScintillaWnd::SetDisplaySelection(BOOL bFlag)
{
	m_bSelection = bFlag;
	if (bFlag)
		SendMessage(SCI_SETMARGINWIDTHN, 1, 16);
	else
		SendMessage(SCI_SETMARGINWIDTHN, 1, 0);
}

//About Line,Position Info
//-- Set Linnumber width
int CScintillaWnd::GetLinenumberWidth ()
{
	// get number of chars needed to display highest linenumber
	int nChars = GetLinenumberChars ()+1;
	// get width of character '9' in pixels
	LRESULT lWidth = SendMessage(SCI_TEXTWIDTH, STYLE_LINENUMBER, (LPARAM)_T("16"));
	return (int)(nChars * lWidth);
}

//-- Set Linenumber char type
int CScintillaWnd::GetLinenumberChars ()
{
	// try to get number of lines in control
	LRESULT lLines = SendMessage(SCI_GETLINECOUNT, 0, 0);

	int nChars = 1;
	while (lLines > 0)
	{
		lLines = lLines / 10;
		nChars++;
	}
	return nChars; // allow an extra char for safety
}

//-- Move the cursor position, move the scroll too.
void CScintillaWnd::GotoPosition(long lPos) //@parm new character position
{
	SendMessage(SCI_GOTOPOS, lPos, 0);
}

//-- Get the current line number
long CScintillaWnd::GetCurrentLine()
{
	long lPos = (long)SendMessage(SCI_GETCURRENTPOS, 0, 0);
	return (long)SendMessage(SCI_LINEFROMPOSITION, lPos, 0) + 1;
}

//-- Get the current colum position
long CScintillaWnd::GetCurrentColumn()
{
	long lPos = (long)SendMessage(SCI_GETCURRENTPOS, 0, 0);
	return (long)SendMessage(SCI_GETCOLUMN, lPos, 0) + 1;
}

//-- Get current cursor position
long CScintillaWnd::GetCurrentPosition()
{
	return (long)SendMessage(SCI_GETCURRENTPOS, 0, 0);
}

//-- Get current line ending position
long CScintillaWnd::GetCurrentLineEndPosition(long lLine)
{
	return (long)SendMessage(SCI_GETLINEENDPOSITION, lLine, 0);
}

//-- Get the total line count
long CScintillaWnd::GetLineCount()
{
	return (long)SendMessage(SCI_GETLINECOUNT,0 ,0);
}

//-- Move the cursor to line
void CScintillaWnd::GotoLine(long lLine) //@parm new line - lines start at 1
{
	SendMessage(SCI_GOTOLINE, lLine-1, 0);
}

//-- 2012-09-21 hongsu@esmlab.com
//-- Move the cursor to Last
void CScintillaWnd::GotoLastLine()
{
	long nLast = GetLineCount() -1;
	if(nLast < 0)
		nLast = 0;
	SendMessage(SCI_GOTOLINE, nLast, 0);
	//-- 2012-09-21 hongsu@esmlab.com
	//-- Set End Cursor 
	m_bEndCussor = TRUE;
}


//-- Set the string selection range
void CScintillaWnd::SetSelection(long lStart, long lEnd)
{
	SendMessage(SCI_SETSEL, lStart, lEnd);
}

//-- Get the document length
long CScintillaWnd::GetTextLenght()
{
	return (long)SendMessage(SCI_GETTEXTLENGTH, 0);
}

//-- Temp
long CScintillaWnd::GetSelectionStart()
{
	return (long)SendMessage(SCI_GETSELECTIONSTART, 0, 0);
}

//-- About Edit String
//-- Rewrite document
void CScintillaWnd::SetText (LPCTSTR szText) //@parm pointer to new text
{
	char* pchCommand = new char[SCINT_SIZE];
	if(!pchCommand)
		return;

	memset(pchCommand, 0, SCINT_SIZE);
#ifdef UNICODE
	wcstombs(pchCommand, szText, _tcslen(szText)); 
#else
	strcpy(pchCommand, szText);
#endif
	

	LRESULT lResult = 0;
	if(szText != NULL)
		lResult = SendMessage(SCI_SETTEXT,0,(LPARAM)pchCommand);

	GotoPosition(0);

	if(pchCommand)
	{
		delete []pchCommand;
		pchCommand = NULL;
	}
}

//-- Add the text, mobe the cursor position to string end.
void CScintillaWnd::AddText(int nLength, LPCSTR szText, BOOL bLoad) 
{
	if (szText != NULL)
		SendMessage(SCI_ADDTEXT, nLength, (sptr_t)szText);
	if(bLoad == FALSE)
	{	
		long lDocLength = (long)SendMessage(SCI_GETLENGTH, 0, 0);
		GotoPosition(lDocLength);
	}
}

//-- Add the text to last line
void CScintillaWnd::AppendText(LPCTSTR szText, BOOL bLoad)
{
	if(!IsWindow(m_hWnd))
		return ;

	//-- 2012-06-26 hongsu@esmlab.com
	//-- bLoad
	bLoad = !m_bEndCussor;

	char* pchCommand = new char[SCINT_SIZE];
	if(!pchCommand)
		return;

	memset(pchCommand, 0, SCINT_SIZE);

	CString strTemp = szText;

	//Changes Korean text string into char('.') because scintillawnd doesn't support.
	for(int i=0; i<strTemp.GetLength(); i++)
	{
		if(strTemp.GetAt(i) & 0x8000)
			strTemp.SetAt(i, _T('.'));

		else if(strTemp.GetAt(i) == 0x2022)
			strTemp.SetAt(i, _T('.'));
	}

	int nStrLength = (int)_tcslen(strTemp);	
	wcstombs(pchCommand, strTemp, nStrLength);
//#ifdef UNICODE
//	wcstombs(pchCommand, szText, _tcslen(szText)); 		
//#else
//	strcpy(pchCommand, szText);
//#endif	
	
 	if(szText != NULL)
		SendMessage(SCI_APPENDTEXT, nStrLength, (LPARAM)pchCommand);	
		//SendMessage(SCI_ADDTEXT, nStrLength, (LPARAM)pchCommand);	

	//-- 2012-06-26 hongsu@esmlab.com
	//-- Check Position

	if(!bLoad)
	{	
		long lDocLength = (long)SendMessage(SCI_GETLENGTH, 0, 0);
		GotoPosition(lDocLength);
	}
	
	if(pchCommand)
	{
		delete []pchCommand;
		pchCommand = NULL;
	}
}

//-- Insert Text line, don't move the cursor position
void CScintillaWnd::InsertText(LPCSTR szText)
{
 	LRESULT lResult = 0;
	if (szText != NULL)
		lResult = SendMessage(SCI_INSERTTEXT, 0, (LPARAM)szText);	
}

//-- About BookMark
//-- Toggle BookMark
void CScintillaWnd::ToggleBookMark(long lLine) //@parm line where to delete bookmark - lines start at 1
{
	if(HasBookmark(lLine))
	{
		SendMessage(SCI_MARKERDELETE, lLine-1, 0);
	}
	else
	{
		SendMessage(SCI_MARKERADD, lLine-1, 0);
	}
}

//-- Check the line has bookmark
BOOL CScintillaWnd::HasBookmark(long lLine) //@parm line where to add bookmark - lines start at 1
{
	int n = (int)SendMessage(SCI_MARKERGET, lLine-1, 0);
	// check mask for markerbit 0
	if (n & 0x1)
		return TRUE;
	return FALSE;
}

//-- Find next bookmark and move
void CScintillaWnd::FindNextBookmark()
{
	long lLineNum = 0;
	long lStartPos = 0;
	long lEndPos = 0;
	long lLine = (long)SendMessage(SCI_MARKERNEXT, GetCurrentLine(), 1);

	if (lLine >= 0)
		SendMessage(SCI_GOTOLINE, lLine,0);
	else
	{
		for(int nLineCnt = 0; nLineCnt < GetLineCount(); nLineCnt++)
		{
			if(HasBookmark(nLineCnt) == TRUE)
			{
				GotoLine(nLineCnt);
				break;
			}
		}
	}

	//--To select the line
	lLineNum = GetCurrentLine();
	lStartPos = GetCurrentLineEndPosition(lLineNum - 1);
	lEndPos = GetCurrentPosition();

	SetSelection(lStartPos, lEndPos);

}

//-- Find previous bookmark and move
void CScintillaWnd::FindPreviousBookmark()
{
//	long lLastLine;
	long lLineNum = 0;
	long lStartPos = 0;
	long lEndPos = 0;
	long lLine = (long)SendMessage(SCI_MARKERPREVIOUS, GetCurrentLine()-2, 1);

	if (lLine >= 0)
		SendMessage(SCI_GOTOLINE, lLine,0);	
	else
	{
		int nLineCnt = GetLineCount() - 1;

		while(nLineCnt)
		{
			if(HasBookmark(nLineCnt) == TRUE)
			{
				GotoLine(nLineCnt);

				break;
			}

			nLineCnt--;
		}
	}

	//--To select the line
	lLineNum = GetCurrentLine();
	lStartPos = GetCurrentLineEndPosition(lLineNum - 1);
	lEndPos = GetCurrentPosition();

	SetSelection(lStartPos, lEndPos);
}

//-- Clear All BookMarks
void CScintillaWnd::ClearAllBookMarks()
{
	SendMessage(SCI_MARKERDELETEALL, (WPARAM)-1, (LPARAM)0);
}

//-- Toggle the bookmark by click
void CScintillaWnd::DoToggleBookMark(int nMargin, long lPosition)
{
	if (nMargin == 1)
	{
		long lLine = (long)SendMessage(SCI_LINEFROMPOSITION, lPosition, 0);
		ToggleBookMark(lLine + 1);
	}
}

//-- Reload the file
BOOL CScintillaWnd::LoadFile (LPCTSTR szPath, CESMTextProgressCtrl &ctrlProgressBar) //@parm filename of to load
{
	CTime time  = CTime::GetCurrentTime();
	int	nHour = time.GetHour();		//시
	int nMin = time.GetMinute();	//분
	int nSec = time.GetSecond();

	CString strTime;
	CString strLine = _T("");

	strTime.Format(_T("%02d-%02d-%02d\r\n"), nHour, nMin, nSec);

	//파일 읽는법 연구
	FILE *fIn;
	
	char line[1024] = {0};
	SCIEvent msgSci;
	long lFileSize = 0;
	long lTotalFileSize = 0;
	int nLineLength = 0;

	if((fIn = _tfopen(szPath, _T("r"))) == NULL)
	{
		TRACE(_T("Can't open the file"));
		return FALSE;
	}
	else
	{
		//-- Get the file size
		fseek(fIn, 0, SEEK_END);
		lTotalFileSize = ftell(fIn);
		fseek(fIn, 0, SEEK_SET);
		
		//-- Set the progress bar range
		ctrlProgressBar.SetRange(0, lTotalFileSize);
		TRACE(_T("Gets()_Start : %s\n"), strTime);

		while(1)
		{
			if(feof(fIn)) 
				break;
			
			//-- Get the line
			//fgets(line, LOG_LINE_SIZE, fIn);			
			//_fgetts(line, LOG_LINE_SIZE, fIn);

			fgets(line, SCINT_LINE_SIZE, fIn);
			nLineLength = (int)strlen(line);
			strLine = line;

			//-- Modify
			if(strLine.Find(_T("\n")) < 0)
				strLine.Append(_T("\r\n"));

			//-- Send the line to serial view
			msgSci.message = WM_SCI_SER_LOAD;
			msgSci.pParam = (LPARAM)(LPCTSTR)strLine;		 
			GetParent()->SendMessage(WM_SCI, WM_SCI_SER_LOAD, (LPARAM)&msgSci);

			//-- Set the progress position
 			ctrlProgressBar.SetPos(lFileSize); 
 			lFileSize += strLine.GetLength();
		}

		time  = CTime::GetCurrentTime();
		nHour = time.GetHour();		//시
		nMin = time.GetMinute();	//분
		nSec = time.GetSecond();
 

		strTime.Format(_T("%02d-%02d-%02d\r\n"), nHour, nMin, nSec);
		TRACE(_T("Gets()_End Time : %s\n"), strTime);

		fclose(fIn);

		return TRUE;
	}	

	return FALSE;
}

//-- Get the current line number
CString CScintillaWnd::GetLineText(int nLineNum)
{
	CString strError = _T("");

	char szText[SCINT_LINE_SIZE] = {0};

	int nLineLength = 0;
	
	LRESULT lResult = 0;

	lResult = SendMessage(SCI_GETLINE, nLineNum, (LPARAM)szText);

	if(lResult != 0)
	{
		nLineLength = (int)SendMessage(SCI_LINELENGTH, nLineNum, 0);
		szText[nLineLength - 1] = '\0';

		return szText;
	}
	else
	{
		strError = _T("Can't find the string");
		return strError;
	}
}

//-- Find next string
BOOL CScintillaWnd::SearchForward(LPTSTR szText) //@parm text to search
{
	CString strText = (LPCTSTR)szText;
	if (strText.IsEmpty() == TRUE)
		return FALSE;

	char pchCommand[1024] = {0};
	
#ifdef UNICODE
	wcstombs(pchCommand, szText, _tcslen(szText)); 	
#else
	strcpy(pchCommand, szText);
#endif

	long lPos = GetCurrentPosition();
	TextToFind tf;
	tf.lpstrText = pchCommand;
	tf.chrg.cpMin = lPos+1;
	tf.chrg.cpMax = (long)SendMessage(SCI_GETLENGTH, 0, 0);
	lPos = (long)SendMessage(SCI_FINDTEXT, m_nSearchflags, (LPARAM)&tf);
	if (lPos >= 0)
	{
		//SetFocus();
		GotoPosition(lPos);
		SendMessage(SCI_SETSEL, tf.chrgText.cpMin, tf.chrgText.cpMax);
		SendMessage(SCI_FINDTEXT, m_nSearchflags, (LPARAM)&tf);

		return TRUE;
	}
	else
	{
		if(CheckSearchString(szText, TRUE) == TRUE)
		{
			GotoLine(0);
			SearchForward(szText);

			return TRUE;
		}
	}

	return FALSE;
}

//-- Find previous string
BOOL CScintillaWnd::SearchBackward(LPTSTR szText) //@parm text to search
{
	CString strText = (LPCTSTR)szText;
	if (strText.IsEmpty() == TRUE)
		return FALSE;

	char pchCommand[1024] = {0};
#ifdef UNICODE
	wcstombs(pchCommand, szText, _tcslen(szText)); 
#else
	strcpy(pchCommand, szText);
#endif

	long lLastLine = 0;
	long lPos = GetCurrentPosition();
	long lMinSel = GetSelectionStart();
	TextToFind tf;
	tf.lpstrText = pchCommand;
	if (lMinSel >= 0)
		tf.chrg.cpMin = lMinSel-1;
	else
		tf.chrg.cpMin = lPos-1;
	tf.chrg.cpMax = 0;
	lPos = (long)SendMessage(SCI_FINDTEXT, m_nSearchflags, (LPARAM)&tf);
	if (lPos >= 0)
	{
		GotoPosition(lPos);
		SendMessage(SCI_SETSEL, tf.chrgText.cpMin, tf.chrgText.cpMax);
		SendMessage(SCI_FINDTEXT, m_nSearchflags, (LPARAM)&tf);

		return TRUE;
	}
	else
	{
		if(CheckSearchString(szText, FALSE) == TRUE)
		{
			lLastLine = GetLineCount();
			GotoLine(lLastLine);
			SearchBackward(szText);

			return TRUE;
		}
		else
			return FALSE;
	}
	return FALSE;
}
//-----------------------------------
//-- Check the string to loop
BOOL CScintillaWnd::CheckSearchString(LPTSTR szText, BOOL bForward)
{
	long lPos = GetCurrentPosition();
	long lMinSel = GetSelectionStart();

	char pchCommand[1024] = {0};
#ifdef UNICODE
	wcstombs(pchCommand, szText, _tcslen(szText)); 
#else
	strcpy(pchCommand, szText);
#endif

	TextToFind tf;
	tf.lpstrText = pchCommand;

	if(bForward == TRUE)
	{
		if (lMinSel >= 0)
			tf.chrg.cpMin = lMinSel-1;
		else
			tf.chrg.cpMin = lPos-1;
		tf.chrg.cpMax = 0;
		lPos = (long)SendMessage(SCI_FINDTEXT, m_nSearchflags, (LPARAM)&tf);

		if (lPos >= 0)
			return TRUE;
		else
			return FALSE;
	}
	else
	{
		tf.chrg.cpMin = lPos+1;
		tf.chrg.cpMax = (long)SendMessage(SCI_GETLENGTH, 0, 0);
		lPos = (long)SendMessage(SCI_FINDTEXT, m_nSearchflags, (LPARAM)&tf);

		if (lPos >= 0)
			return TRUE;
		else
			return FALSE;
	}	
}
//-----------------------------------
//-- About Message
void CScintillaWnd::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	//-- Read Only
	if(m_bESMOutputView)
		return;

	//-- Send the data through the serial connection (if it's open)
	long lDocLength = (long)SendMessage(SCI_GETLENGTH, 0, 0);
	GotoPosition(lDocLength);

	CWnd::OnChar(nChar, nRepCnt, nFlags);
}
//-----------------------------------
//-- To put the message
void CScintillaWnd::WriteChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CWnd::OnChar(nChar, nRepCnt, nFlags);
}

BOOL CScintillaWnd::OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	if(message == WM_GETTEXT)
		return TRUE;

	//-- Only Output
	if(message == WM_RBUTTONDOWN)
	{
		CMenu menu;
		menu.CreatePopupMenu();
		menu.AppendMenu( MF_STRING, ID_SCINTILLAVIEW_COPY, _T( "Copy" ) );
		menu.AppendMenu( MF_STRING, ID_SCINTILLAVIEW_SELECTALL, _T( "SelectAll" ) );
		menu.AppendMenu( MF_STRING, ID_SCINTILLAVIEW_CLEAR, _T( "Clear" ) );

		ModifyPopupMenu((HMENU)menu);

		CPoint p;
		GetCursorPos( &p );
		menu.TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON, p.x, p.y, this );
		menu.DestroyMenu();

		return TRUE;
	}

	if(message == WM_KEYDOWN)
		return TRUE;

	if (message == WM_NCDESTROY) {
		wchar_t tmpStr[100] = { 0 };
		wsprintf(tmpStr, L"caution : destroy window msg(%d) bye pass for prevent send messgae to parent window..\n", message); // Siseong Ahn 20190613
		OutputDebugString(tmpStr);
		return TRUE;
	}

	return CWnd::OnWndMsg(message, wParam, lParam, pResult);
}

void CScintillaWnd::ModifyPopupMenu(HMENU hMenu)
{
	int nStartPos = (int)SendMessage(SCI_GETSELECTIONSTART, 0, 0);
	int nEndPos = (int)SendMessage(SCI_GETSELECTIONEND, 0, 0);

	if(nEndPos - nStartPos == 0)
	{
		EnableMenuItem(hMenu, ID_SCINTILLAVIEW_COPY	, MF_BYCOMMAND | MF_GRAYED);
	}
}
//-----------------------------------
//-- To Key Capture
 BOOL CScintillaWnd::PreTranslateMessage(MSG* pMsg)
 { 
	short ctrl;
	ctrl= GetAsyncKeyState(VK_CONTROL);

	SCIEvent msgSci;

	if(	pMsg->message == WM_MOUSEWHEEL		||
		pMsg->message == WM_LBUTTONDOWN		||
		pMsg->message == WM_LBUTTONUP       ||
		pMsg->message == WM_LBUTTONDBLCLK   ||
		pMsg->message == WM_RBUTTONDOWN     ||
		pMsg->message == WM_RBUTTONUP       ||
		pMsg->message == WM_RBUTTONDBLCLK   ||
		pMsg->message == WM_MBUTTONDOWN     ||
		pMsg->message == WM_MBUTTONUP       ||
		pMsg->message == WM_MBUTTONDBLCLK  
		)
		m_bEndCussor = FALSE;	

	else if(pMsg->message == WM_KEYDOWN)
	{
		m_bEndCussor = FALSE;

		if((pMsg->wParam == 'f' || pMsg->wParam == 'F' ) && ctrl) {
			msgSci.message = WM_SCI_SER_POPUP_FIND_DLG;
			GetParent()->SendMessage(WM_SCI, WM_SCI_SER_POPUP_FIND_DLG, (LPARAM)&msgSci);		
			return TRUE;
		}
 		if((pMsg->wParam == 'c' || pMsg->wParam == 'C') && ctrl)	{
 			SendMessage(SCI_COPY); 
 			return TRUE;
 		}
		if((pMsg->wParam == 'a' || pMsg->wParam == 'A') && ctrl)	{
			SendMessage(SCI_SELECTALL);
			return TRUE;
		}
		if(pMsg->wParam == VK_END  && ctrl)	{
			SendMessage(SCI_DOCUMENTEND);
			m_bEndCussor = TRUE;
			return TRUE;
		}
		if(pMsg->wParam == VK_HOME && ctrl)	{
			SendMessage(SCI_DOCUMENTSTART);
			return TRUE;
		}

		switch(pMsg->wParam)
		{
		//case VK_UP	:	SendMessage(SCI_LINEUP);	return TRUE;
		//case VK_DOWN	:	SendMessage(SCI_LINEDOWN);	return TRUE;
		//case VK_LEFT	:	SendMessage(SCI_CHARLEFT);	return TRUE;
		//case VK_RIGHT	:	SendMessage(SCI_CHARRIGHT);	return TRUE;
		//case VK_HOME	:	SendMessage(SCI_HOME);		return TRUE;
		//case VK_END	:	SendMessage(SCI_LINEEND);	return TRUE;
		//case VK_PRIOR	:	SendMessage(SCI_PAGEUP);	return TRUE;
		//case VK_NEXT	:	SendMessage(SCI_PAGEDOWN);	return TRUE;
		case VK_F5		:	ToggleBookMark(GetCurrentLine());	return TRUE;
		case VK_F3		:	FindPreviousBookmark();		return TRUE;
		case VK_F4		:	FindNextBookmark();			return TRUE;
		case VK_ESCAPE	:	ClearAllBookMarks();		return TRUE;
		default: break;
		}

		//-- Only Read Only
		if(!m_bESMOutputView)
		{
			//-- Edit Text - Disable Case		
			if(m_strPreChar.GetLength() > 0)
			{
				long lDocLength = (long)SendMessage(SCI_GETLENGTH, 0, 0);
				long lCurrentPos = (long)SendMessage(SCI_GETCURRENTPOS, 0, 0);
				if(lCurrentPos < lDocLength)
				{
					if(pMsg->wParam == VK_RETURN ||
						pMsg->wParam == VK_BACK ||
						pMsg->wParam == VK_DELETE ||
						pMsg->wParam == VK_SPACE)
						return TRUE;
				}
			}
			else
			{
				if(pMsg->wParam == VK_BACK ||
					pMsg->wParam == VK_DELETE ||
					pMsg->wParam == VK_SPACE)
					return TRUE;
	 			
				long lDocLength = (long)SendMessage(SCI_GETLENGTH, 0, 0);
				long lCurrentPos = (long)SendMessage(SCI_GETCURRENTPOS, 0, 0);

				if(lCurrentPos != lDocLength)
				{
					if(pMsg->wParam == VK_RETURN)
						return TRUE;
				}
			}	
		}
	}

	return CWnd::PreTranslateMessage(pMsg);
 }
//-----------------------------------
//-- SET CHANGE COLOR
void CScintillaWnd::SetBGColor(COLORREF color)
{
	SendMessage(SCI_STYLESETFORE, 0, RGB(0,0,0));
	SendMessage(SCI_STYLESETFORE, 2, RGB( 0, 0, 0 ));
	SendMessage(SCI_STYLESETFORE, 5, RGB( 0, 0, 0 ));
	SendMessage(SCI_STYLESETFORE, 6, RGB( 0, 0, 0 ));
	SendMessage(SCI_STYLESETFORE, 9, RGB( 0, 0, 0 ));
	SendMessage(SCI_STYLESETFORE, 10, RGB( 0, 0, 0 ));
	SendMessage(SCI_STYLESETFORE, 11, RGB( 0, 0, 0 ));
	

	// Set foreground color
	SendMessage( SCI_STYLESETFORE, STYLE_DEFAULT, (LPARAM)RGB( 255, 255, 255 ) );
	// Set background color
	SendMessage( SCI_STYLESETBACK, STYLE_DEFAULT, (LPARAM)RGB( 0, 0, 0 ) );
	// Set font
	SendMessage( SCI_STYLESETFONT, STYLE_DEFAULT, (LPARAM)"Courier New" );
	// Set selection color
	SendMessage( SCI_SETSELBACK, (WPARAM)TRUE, (LPARAM)RGB( 0, 0, 255 ) );
}

int CScintillaWnd::GetCurrentStyle()
{
	long lPos = SendMessage(SCI_GETCURRENTPOS, 0, 0);
	return SendMessage(SCI_GETSTYLEAT, lPos, 0);
}