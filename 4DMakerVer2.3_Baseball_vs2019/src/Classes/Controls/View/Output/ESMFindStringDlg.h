////////////////////////////////////////////////////////////////////////////////
//
//	ESMFindStringDlg.h : implementation of the TesESMuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "resource.h"
#include "ScintillaWnd.h"

class CESMFindStringDlg : public CDialog
{
	DECLARE_DYNAMIC(CESMFindStringDlg)

public:
	CESMFindStringDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMFindStringDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_OUTPUT_FIND };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	afx_msg void OnBnClickedPreFind();
	afx_msg void OnBnClickedNextFind();

	DECLARE_MESSAGE_MAP()
public:
	CString m_strFindString;
	BOOL m_bMatchWords;
	BOOL m_bMatchCase;
	HWND m_wndESMSerialViewDlg;
	BOOL m_bForward;
	
	CComboBox m_ctrlFindStrings;	
	CButton m_ctrlPreButton;
	CButton m_ctrlNextButton;

public:	
	virtual BOOL InitDialog(CStringList *strFindStrings);
	virtual int SetSearchFlags();
	virtual BOOL CheckComboBox(CString strFindString);

	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
