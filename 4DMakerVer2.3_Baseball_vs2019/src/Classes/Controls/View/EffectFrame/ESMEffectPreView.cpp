////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectEditorViewer.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FileOperations.h"
#include "ESMEffectPreview.h"
#include "DSCItem.h"

#include "ESMFunc.h"
#include "SRSIndex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CESMEffectPreview, CDialog)

CESMEffectPreview::CESMEffectPreview(CWnd* pParent /*=NULL*/)
	: CDialog(CESMEffectPreview::IDD, pParent)	
{
	m_pPreviewView = NULL;
}

CESMEffectPreview::~CESMEffectPreview()
{
	if(m_pPreviewView)
	{
		delete m_pPreviewView;
		m_pPreviewView = NULL;
	}	
}

void CESMEffectPreview::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMEffectPreview)
	//}}AFX_DATA_MAP	
}

BOOL CESMEffectPreview::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN || pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
			return FALSE;
	}

	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN)
		{
			BOOL bShift = ((GetKeyState(VK_SHIFT) & 0x8000) != 0);
			BOOL bControl = ((GetKeyState(VK_CONTROL) & 0x8000) != 0);
			BOOL bAlt = ((GetKeyState(VK_MENU) & 0x8000) != 0);

			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			if(bControl && !bShift && !bAlt)
			{
				ESMEvent* pEsmMsg = NULL;
				pEsmMsg = new ESMEvent();

				pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
				pEsmMsg->nParam1 = pMsg->wParam;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
			}

		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMEffectPreview, CDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()		
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

BOOL CESMEffectPreview::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);
	pDC->FillSolidRect(rect, COLOR_BASIC_BG_2 );
	Invalidate(FALSE);
	return TRUE;
}

// CFrameNailDlg message handlers
BOOL CESMEffectPreview::OnInitDialog()
{
	CDialog::OnInitDialog();

	//-- 2014-07-18 hongsu@esmlab.com
	//-- Create Viewer
	if(!m_pPreviewView)
	{
		m_pPreviewView = new CESMEffectPreviewView();
		m_pPreviewView ->Create(CESMEffectPreviewView::IDD, this);
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}


void CESMEffectPreview::OnSize(UINT nType, int cx, int cy) 
{
	//-- ReloadFrame
	UpdateFrame(TRUE);
	CDialog::OnSize(nType, cx, cy);	
} 

void CESMEffectPreview::OnPaint()
{	
	UpdateFrame();
	CDialog::OnPaint();
}

void CESMEffectPreview::ClearPreview()
{
	m_pPreviewView->ClearPreview();
	m_pPreviewView->ShowWindow(SW_HIDE);
}

void CESMEffectPreview::UpdateFrame(BOOL bReloadImage)
{
	//-- 2014-07-17 hongsu@esmlab.com
	//-- Draw Frame
	CPaintDC dc(this);
		
	CRect rect;	
	GetClientRect(rect);
	int nHeight = rect.Height();
	int nWidth	= rect.Width();
	
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), SRCCOPY);

	mDC.FillRect(rect, &CBrush(COLOR_BASIC_BG_2));
	
	//-- Frame List
	if(m_pPreviewView)
	{
		BOOL bRatio = FALSE; // [TRUE] WIDTH {FALSE] HEIGHT
		int nMargin;	
		float nRatio = (float)nWidth/(float)nHeight;
		float nBaseRatio = (float) FULL_HD_WIDTH /(float) FULL_HD_HEIGHT;						// 3:2

		if(nRatio > nBaseRatio)
		{
			nWidth =  (int)((float)nHeight / (float)FULL_HD_HEIGHT * (float)FULL_HD_WIDTH);	// 3:2
			bRatio = TRUE;
			nMargin = (rect.Width() - nWidth)/2;			
		}
		else
		{
			nHeight = (int)((float)nWidth / (float)FULL_HD_WIDTH* (float) FULL_HD_HEIGHT );	// 3:2
			bRatio = FALSE;
			nMargin = (rect.Height() - nHeight)/2;
		}

		if(bRatio)  rect.left += nMargin;	// Width
		else		rect.top += nMargin;	// Height

		rect.right = rect.left + nWidth;
		rect.bottom = rect.top + nHeight;

		//-- 2014-07-18 hongsu@esmlab.com
		//-- Move Effect Frame View 
		m_pPreviewView->ShowWindow(SW_SHOW);
		m_pPreviewView->MoveWindow(rect);
		m_pPreviewView->Invalidate(FALSE);			
	}
	

	
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);	
	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();
}

