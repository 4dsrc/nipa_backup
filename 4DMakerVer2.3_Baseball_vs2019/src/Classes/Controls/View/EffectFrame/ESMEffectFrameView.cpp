////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectFrameView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-18
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "DSCViewDefine.h"
#include "ESMEffectFrameView.h"
#include "ESMObjectFrame.h"
#include "ESMEffectEditor.h"
#include "ESMEffectPreview.h"
#include "ESMCtrl.h"
#include <math.h>

#include "ESMMovieMgr.h"
#ifdef _WIN64
//#include "CvvImage.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CESMEffectFrameView, CDialog)

void OnEventhandle1 (int event, int x, int y, int flags, void* param);

CESMEffectFrameView::CESMEffectFrameView(CWnd* pParent /*=NULL*/)
	: CESMFrameViewBase(pParent)
{
	m_pImage				= NULL;
	m_pEffectEditor			= NULL;
	m_pEffectPreview	= NULL;

	m_ptZoom = CPoint(0,0);
	m_szZoom = CSize(0, 0);
	m_bZoomMove = FALSE;
	m_nCircleSize = 10;
	m_bAdjust = FALSE;
	m_nPosX = 0;
	m_nPosY = 0;

	m_bFirstPos = TRUE;
	m_nLinePos1X = 0;
	m_nLinePos1Y = 0;

	m_nLinePos2X = 0;
	m_nLinePos2Y = 0;

	m_nSelectPosX = 1920;
	m_nSelectPosY = 1080;

	m_MarginX	=	0;
	m_MarginY	=	0;
}

CESMEffectFrameView::~CESMEffectFrameView()
{
	//-- IplImage release
	cvReleaseImage( &m_pImage);
}

void CESMEffectFrameView::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX,	IDC_EFFECT_IMAGE,	m_EffectImage);
	DDX_Control(pDX, IDC_EDT_SELECT_POINT_X, m_ctrlSelectPosX);
	DDX_Control(pDX, IDC_EDT_SELECT_POINT_Y, m_ctrlSelectPosY);
	DDX_Control(pDX, IDC_CHK_DRAW_LINE, m_btnDrawLine);
	DDX_Control(pDX, IDC_EDT_LINE_COUNT, m_edLineCount);
}


BEGIN_MESSAGE_MAP(CESMEffectFrameView, CESMFrameViewBase)
	ON_WM_PAINT()
	ON_WM_SIZE()		
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEMOVE()	
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_KEYDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEHWHEEL()
	ON_BN_CLICKED(IDC_BTN_POINT_MOVE, OnBtnPosMove)
	ON_BN_CLICKED(IDC_BTN_DRAW_LINE, &CESMEffectFrameView::OnBnClickedBtnDrawLine)
END_MESSAGE_MAP()

// CFrameNailDlg message handlers
BOOL CESMEffectFrameView::OnInitDialog()
{
	
	return CESMFrameViewBase::OnInitDialog();
}

void CESMEffectFrameView::OnSize(UINT nType, int cx, int cy) 
{
	GetClientRect(m_rect);
	CESMFrameViewBase::OnSize(nType, cx, cy);
} 

void CESMEffectFrameView::OnPaint()
{	
	//UpdateFrame();
	CDialog::OnPaint();
}

BOOL CESMEffectFrameView::OnEraseBkgnd(CDC* pDC)
{
	return true;
}


void CESMEffectFrameView::UpdateFrame()
{
	if(!m_pObjectFrame)
		return;
	
#if 1 //신규 VMCC 기능
	ESMLog(1, _T("[Effect] Load Frame Start"));
	LoadFrame();
#else 
	CRect rect;
	GetClientRect(&rect);	
	int nWidth	= rect.Width();
	int nHeight = rect.Height();

	//-- 2014-07-17 hongsu@esmlab.com
	CPaintDC dc(this);
	CDC mDC;
	CBitmap mBitmap;		

	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, nWidth, nHeight); 

	mDC.SelectObject(&mBitmap);	
	mDC.PatBlt(0,0,nWidth, nHeight,SRCCOPY);
	SetFrameFont(&mDC);

	//-- Load Frame Image
	//LoadFrame(&mDC);

	//-- Draw DSC / Time
	DrawInfo(&mDC);
	//-- Draw Zoom
	DrawZoomIndicator(&mDC);

	mDC.MoveTo(rect.right/2, rect.top);
	mDC.LineTo(rect.right/2, rect.bottom);	
	mDC.MoveTo(rect.left, rect.bottom/2);
	mDC.LineTo(rect.right, rect.bottom/2);
	dc.BitBlt(0, 0, nWidth, nHeight, &mDC, 0, 0, SRCCOPY);
	mDC.DeleteDC();
	Invalidate(FALSE);
#endif

	
}

void CESMEffectFrameView::DrawInfo(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);	
	
	pDC->SetTextColor(COLOR_BASIC_FG_1);	
	pDC->SetBkColor(TRANSPARENT);	
		
	CString strText;
	CRect rtText;

	//-- DSC Info
	rtText.top		= rect.bottom - (DSC_TEXT_H * 3);
	rtText.bottom	= rect.bottom - (DSC_TEXT_H * 2);
	rtText.left		= rect.left;
	rtText.right	= rect.right  - (DSC_TEXT_H * 1);
	strText.Format(_T("DSC : %s"), m_pObjectFrame->GetDSC());
	pDC->DrawText(strText, rtText,  DT_RIGHT | DT_VCENTER | DT_SINGLELINE);

	//-- Time Info
	rtText.top		= rect.bottom - (DSC_TEXT_H * 2);
	rtText.bottom	= rect.bottom - (DSC_TEXT_H * 1);
	float nTime = (float)m_pObjectFrame->GetTime();
	strText.Format(_T("Time : %.3f"),nTime/1000);
	pDC->DrawText(strText, rtText,  DT_RIGHT | DT_VCENTER | DT_SINGLELINE);
}

void CESMEffectFrameView::LoadFrame(CDC* pDC)
{
	//-- GetRect
	CRect rect;
	GetClientRect(&rect);
	int nWidth	= rect.Width();
	int nHeight = rect.Height();

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(pDC);
	mBitmap.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	if(m_pImage)
		cvReleaseImage( &m_pImage);
	m_pImage = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3);

	//-- 2014-07-17 hongsu@esmlab.com
	//-- Get Frame Image 
	CString strDSC, strFileName;
	int nTime;
	strDSC	= m_pObjectFrame->GetDSC();
	nTime	= m_pObjectFrame->GetTime();
	int nFIdx = ESMGetFrameIndex(nTime);
	//-- Get Movie File

	/*
	int nTime = GetSelectedTime();
	if(nTime < 0)
	return FALSE;

	CString strDSCID = GetSelectedDSC();
	if(!strDSCID.GetLength())
	return FALSE;
	//-- 2015-03-30 cygil@esmlab.com ImageLoader 추가 ImageLoader에서 Image 얻어온다
	int nFIdx = ESMGetFrameIndex(nTime);
	*/	
	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	CString strDscIp;
	ESMGetDSCList(&arDSCList);
	for( int i = 0; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			if(pItem->GetDeviceDSCID() == strDSC)
			{
				strDscIp.Format(_T("%s"), pItem->m_strInfo[DSC_INFO_LOCATION]);
#if 1
				//CMiLRe 20151119 GOP 변경으로 실제 동영상 파일경로 찾기
				strFileName = ESMGetMoviePath(strDSC, nFIdx);
#else
				if(ESMGetExecuteMode())
					strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientRamPath(strDscIp),  pItem->GetDeviceDSCID());
				else
					strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());
#endif
			}
		}
	}
	//strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), strDSC);
	FFmpegManager ffMpegMgr(FALSE);

	//CMiLRe 20151119 GOP 변경으로 실제 동영상 파일경로 찾기
	int nFrame;
	int nMovieIndex = 0,  nRealFrameIndex = 0;
	//CMiLRe 20160115 VMCC FRAME View와 Frame View 이미지 다른 버그 수정
	ESMGetMovieIndex(nFIdx, nMovieIndex, nRealFrameIndex);

	//CMiLRe 20160115 VMCC FRAME View와 Frame View 이미지 다른 버그 수정
	if(!ffMpegMgr.GetCaptureImage(strFileName, m_pImage, nRealFrameIndex))
		ESMLog(0,_T("Get Frame [%s][%d]"),strDSC,nFIdx);		

	//-- Draw for Preview
	//CvvImage cImage;		
	//cImage.CopyOf(m_pImage,8);
	//cImage.Show(mDC.GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height() );	
	//cImage.Destroy();

	//-- 2011-08-02 hongsu.jung
	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);

	//-- 2014-07-22 hongsu@esmlab.com
	//-- Memory Leak 
	mDC.DeleteDC();
}

BOOL CESMEffectFrameView::LoadAdjust()
{
	m_adjinfo.clear();

	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();

	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	CString strDscIp;
	ESMGetDSCList(&arDSCList);
	for( int i = 0; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
			m_adjinfo.push_back(pMovieMgr->GetAdjustData(pItem->GetDeviceDSCID()));
	}

	for(int i = 0; i < m_adjinfo.size(); i++)
	{
		stAdjustInfo adjInfo = m_adjinfo.at(i);
		CESMImgMgr* pImgMgr = new CESMImgMgr();
		pImgMgr->SetMargin(m_MarginX, m_MarginY, adjInfo);
	}

	ESMLog(1,_T("[Effect] Set MarginX = %d, MarginY = %d"), m_MarginX, m_MarginX);

	if(m_adjinfo.at(0).AdjAngle == 0)
		return FALSE;
	else
		return TRUE;
}

void CESMEffectFrameView::OnBtnPosMove()
{
	CString strData;
	m_ctrlSelectPosX.GetWindowText(strData);
	m_pEffectEditor->m_pSelectFrame->SetPosX(_ttoi(strData));
	m_ctrlSelectPosY.GetWindowText(strData);
	m_pEffectEditor->m_pSelectFrame->SetPosY(_ttoi(strData));
	LoadFrame();
}

void CESMEffectFrameView::LoadFrame()
{
	if(!m_bAdjust)
	{
		ESMLog(1, _T("[Effect] Load Adjust"));
		if(LoadAdjust())
			m_bAdjust = TRUE;
		else
			ESMLog(0, _T("[Effect] Load Adjust"));
	}
	//-- 2014-07-17 hongsu@esmlab.com
	//-- Get Frame Image 
	CString strDSC, strFileName;
	int nTime;
	strDSC	= m_pObjectFrame->GetDSC();
	nTime	= m_pObjectFrame->GetTime();
	int nFIdx = ESMGetFrameIndex(nTime);

	//-- Get Movie File

	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	CString strDscIp;
	ESMGetDSCList(&arDSCList);
	for( int i = 0; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			if(pItem->GetDeviceDSCID() == strDSC)
			{
				strDscIp.Format(_T("%s"), pItem->m_strInfo[DSC_INFO_LOCATION]);
				strFileName = ESMGetMoviePath(strDSC, nFIdx);
			}
		}
	}
	FFmpegManager ffMpegMgr(FALSE);
	int nFrame;
	int nMovieIndex = 0,  nRealFrameIndex = 0;
	ESMGetMovieIndex(nFIdx, nMovieIndex, nRealFrameIndex);


	BYTE* pBmpBits = NULL;
	int width =0, height = 0;
	ffMpegMgr.GetCaptureImage(strFileName, &pBmpBits,nRealFrameIndex, &width, &height, ESMGetGopSize());
	if( pBmpBits != NULL)
	{
		m_AdjustInfo.pBmpBits = pBmpBits;
		m_AdjustInfo.nWidht = width;
		m_AdjustInfo.nHeight = height;
		
	}
	else
	{
		

		if(m_pEffectPreview)
		{
			if(m_pImage)
				cvReleaseImage( &m_pImage);
			m_pImage = cvCreateImage(cvSize(m_AdjustInfo.nWidht, m_AdjustInfo.nHeight), IPL_DEPTH_8U, 3);
			memset(m_pImage->imageData, 0 , m_AdjustInfo.nWidht*m_AdjustInfo.nHeight*3);

			if(m_pImage)
			{
				//-- Get Zoom Position
				//cvSetImageROI(m_pImage, cvRect(rt.left, rt.top, m_AdjustInfo.nWidht/m_fZoomSize, m_AdjustInfo.nHeight/m_fZoomSize));
				m_pEffectPreview->CopyImage(m_pImage);
			}		
		}


		ESMLog(0, _T("[Effect] Load Frame End %s"), strFileName); //Error
		m_EffectImage.Redraw();
		return;
	}

	//m_EffectImage.SetImageBuffer(m_AdjustInfo.pBmpBits, m_AdjustInfo.nWidht, m_AdjustInfo.nHeight, 3);

	//Adjust 적용

	//if(ESMGetCudaSupport())
	//{
	//	ESMLog(1, _T("[Effect] GPU Load Frame"));
	//	if(m_AdjustInfo.nHeight > 2160 || m_AdjustInfo.nHeight < 0 || m_AdjustInfo.nWidht > 3840 || m_AdjustInfo.nWidht < 0 || pBmpBits == NULL)
	//	{
	//		ESMLog(0, _T("[Effect] Size error Width = %d, Height = %d"), m_AdjustInfo.nWidht, m_AdjustInfo.nHeight);
	//		return;
	//	}

	//	Mat src(m_AdjustInfo.nHeight, m_AdjustInfo.nWidht, CV_8UC3);
	//	memcpy(src.data, m_AdjustInfo.pBmpBits, m_AdjustInfo.nHeight * m_AdjustInfo.nWidht * 3);
	//	cuda::GpuMat* pImageMat = new cuda::GpuMat;
	//	pImageMat->upload(src);

	//	if( pImageMat == NULL)
	//	{
	//		ESMLog(0, _T("[Effect] Image Error"));
	//		return ;
	//	}

	//	int nRotateX = 0, nRotateY = 0, nSize = 0;
	//	double dSize = 0.0;
	//	double dRatio = 1;
	//	stAdjustInfo AdjustData;

	//	for(int i = 0; i < m_adjinfo.size(); i++)
	//	{
	//		if(strDSC.CompareNoCase(m_adjinfo.at(i).strDSC) == 0)
	//		{
	//			AdjustData = m_adjinfo.at(i);
	//			break;
	//		}
	//	}

	//	// Rotate 구현					
	//	nRotateX = Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
	//	nRotateY = Round(AdjustData.AdjptRotate.y  * dRatio );
	//	GpuRotateImage(pImageMat, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);
	//			
	//	//-- MOVE EVENT
	//	int nMoveX = 0, nMoveY = 0;
	//	nMoveX = Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
	//	nMoveY = Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
	//	GpuMoveImage(pImageMat, nMoveX,  nMoveY);


	//	//-- IMAGE CUT EVENT
	//	int nMarginX = m_MarginX * dRatio;
	//	int nMarginY = m_MarginY * dRatio;
	//	if ( nMarginX < 0 || nMarginX >= m_AdjustInfo.nWidht/2 || nMarginY < 0 || nMarginY >= m_AdjustInfo.nHeight/2  )
	//	{
	//		ESMLog(1, _T("[Effect] Image Error dRatio = %f, width = %d, height = %d, MarginX = %d, marginY = %d"), dRatio,m_AdjustInfo.nWidht, m_AdjustInfo.nHeight, nMarginX, nMarginY);
	//		return ;
	//	}
	//	GpuMakeMargin(pImageMat, nMarginX, nMarginY);
	//	double dbMarginScale = 1 / ( (double)( m_AdjustInfo.nWidht - nMarginX * 2) /(double) m_AdjustInfo.nWidht  );
	//	cuda::GpuMat* pMatResize = new cuda::GpuMat;
	//	cuda::resize(*pImageMat, *pMatResize, cv::Size(m_AdjustInfo.nWidht*dbMarginScale ,m_AdjustInfo.nHeight * dbMarginScale ), 0.0, 0.0 ,INTER_CUBIC );
	//	int nLeft = (int)((m_AdjustInfo.nWidht*dbMarginScale - m_AdjustInfo.nWidht)/2);
	//	int nTop = (int)((m_AdjustInfo.nHeight*dbMarginScale - m_AdjustInfo.nHeight)/2);
	//	cuda::GpuMat pMatCut =  (*pMatResize)(cv::Rect(nLeft, nTop, m_AdjustInfo.nWidht, m_AdjustInfo.nHeight));
	//	pMatCut.copyTo(*pImageMat);
	//	delete pMatResize;


	//	Mat srcImage(pImageMat->rows, pImageMat->cols, CV_8UC3);
	//	pImageMat->download(srcImage);

	//	delete pImageMat;

	//	int size = srcImage.total() * srcImage.elemSize();
	//	memcpy( m_AdjustInfo.pBmpBits, srcImage.data, size);
	//	m_EffectImage.SetImageBuffer(m_AdjustInfo.pBmpBits, m_AdjustInfo.nWidht, m_AdjustInfo.nHeight, 3);	

	//	//-- Draw Line
	//	m_EffectImage.DrawLine(m_AdjustInfo.nHeight/2, RGB(0,0,0));
	//	m_EffectImage.DrawCenterLine(m_AdjustInfo.nWidht, RGB(0,0,0));

	//	//if(m_btnDrawLine.GetCheck())
	//	{
	//		m_EffectImage.DrawPoint(m_nLinePos1X, m_nLinePos1Y, RGB(255,0,0)); 
	//		m_EffectImage.DrawPoint(m_nLinePos2X, m_nLinePos2Y, RGB(0,0,255));


	//		if(m_nPointCnt)
	//		{
	//			double nX, nY;
	//			double nGabX = 0, nGabY = 0;
	//			for(int i = 0; i < m_nPointCnt; i++)
	//			{
	//				nX = m_nLinePos1X - m_nLinePos2X;
	//				nY = m_nLinePos1Y - m_nLinePos2Y;

	//				nGabX += nX / m_nPointCnt;
	//				nGabY += nY / m_nPointCnt;

	//				
	//				m_EffectImage.DrawPoint(m_nLinePos1X-nGabX, m_nLinePos1Y-nGabY, RGB(0,255,0)); 
	//			}
	//		}
	//	}

	//	//-- Draw Zoom
	//	if(m_pEffectEditor)
	//	{
	//		m_fZoomSize = (float)m_pEffectEditor->m_pSelectFrame->GetZoomRatio();
	//		m_fZoomSize /= 100;

	//		CRect rt;
	//		CPoint margin_pos;

	//		margin_pos.x = m_pEffectEditor->m_pSelectFrame->GetPosX();
	//		margin_pos.y = m_pEffectEditor->m_pSelectFrame->GetPosY();

	//		CString strData;
	//		strData.Format(_T("%d"), margin_pos.x);
	//		m_ctrlSelectPosX.SetWindowText(strData);
	//		strData.Format(_T("%d"), margin_pos.y);
	//		m_ctrlSelectPosY.SetWindowText(strData);

	//		m_EffectImage.DrawZoom(m_fZoomSize, &rt , margin_pos);

	//		if(m_pEffectPreview)
	//		{
	//			if(m_pImage)
	//				cvReleaseImage( &m_pImage);
	//			m_pImage = cvCreateImage(cvSize(m_AdjustInfo.nWidht, m_AdjustInfo.nHeight), IPL_DEPTH_8U, 3);
	//			memcpy(m_pImage->imageData, m_AdjustInfo.pBmpBits, m_AdjustInfo.nWidht*m_AdjustInfo.nHeight*3);

	//			if(m_pImage)
	//			{
	//				//-- Get Zoom Position
	//				cvSetImageROI(m_pImage, cvRect(rt.left, rt.top, m_AdjustInfo.nWidht/m_fZoomSize, m_AdjustInfo.nHeight/m_fZoomSize));
	//				m_pEffectPreview->CopyImage(m_pImage);
	//			}		
	//		}
	//	}
	//}
	//else
	{
		ESMLog(1, _T("[Effect] CPU Load Frame"));
		Mat src(m_AdjustInfo.nHeight, m_AdjustInfo.nWidht, CV_8UC3);
		memcpy(src.data, m_AdjustInfo.pBmpBits, m_AdjustInfo.nHeight * m_AdjustInfo.nWidht * 3);


		int nRotateX = 0, nRotateY = 0, nSize = 0;
		double dSize = 0.0;
		double dRatio = 1;
		stAdjustInfo AdjustData;

		for(int i = 0; i < m_adjinfo.size(); i++)
		{
			if(strDSC.CompareNoCase(m_adjinfo.at(i).strDSC) == 0)
			{
				AdjustData = m_adjinfo.at(i);
				break;
			}
		}

		// Rotate 구현					
		nRotateX = Round(AdjustData.AdjptRotate.x  * dRatio );	 // 반올림
		nRotateY = Round(AdjustData.AdjptRotate.y  * dRatio );

		CpuRotateImage(src, nRotateX ,nRotateY, AdjustData.AdjSize, AdjustData.AdjAngle);

		//-- MOVE EVENT
		int nMoveX = 0, nMoveY = 0;
		nMoveX = Round(AdjustData.AdjMove.x  * dRatio);	 // 반올림
		nMoveY = Round(AdjustData.AdjMove.y  * dRatio);	 // 반올림
		CpuMoveImage(&src, nMoveX,  nMoveY);


		//-- IMAGE CUT EVENT
		int nMarginX = m_MarginX * dRatio;
		int nMarginY = m_MarginY * dRatio;
		if ( nMarginX < 0 || nMarginX >= m_AdjustInfo.nWidht/2 || nMarginY < 0 || nMarginY >= m_AdjustInfo.nHeight/2  )
		{
			ESMLog(0, _T("[Effect] Image Error"));
			return ;
		}
		//CpuMakeMargin(&src, nMarginX, nMarginY);

		double dbMarginScale = 1 / ( (double)( m_AdjustInfo.nWidht - nMarginX * 2) /(double) m_AdjustInfo.nWidht  );
		Mat  pMatResize;
		resize(src, pMatResize, cv::Size(m_AdjustInfo.nWidht*dbMarginScale ,m_AdjustInfo.nHeight * dbMarginScale ), 0.0, 0.0 ,CV_INTER_NN);
		//src = cvarrToMat(Iimage);
		int nLeft = (int)((m_AdjustInfo.nWidht*dbMarginScale - m_AdjustInfo.nWidht)/2);
		int nTop = (int)((m_AdjustInfo.nHeight*dbMarginScale - m_AdjustInfo.nHeight)/2);
		Mat pMatCut =  (pMatResize)(cv::Rect(nLeft, nTop, m_AdjustInfo.nWidht, m_AdjustInfo.nHeight));
		pMatCut.copyTo(src);
		pMatResize = NULL;

		int size = src.total() * src.elemSize();
		memcpy( m_AdjustInfo.pBmpBits, src.data, size);
		m_EffectImage.SetImageBuffer(m_AdjustInfo.pBmpBits, m_AdjustInfo.nWidht, m_AdjustInfo.nHeight, 3);	

		//-- Draw Line
		m_EffectImage.DrawLine(m_AdjustInfo.nHeight/2, RGB(0,0,0));
		m_EffectImage.DrawCenterLine(m_AdjustInfo.nWidht, RGB(0,0,0));

		//-- Draw Zoom
		if(m_pEffectEditor)
		{
			m_fZoomSize = (float)m_pEffectEditor->m_pSelectFrame->GetZoomRatio();
			m_fZoomSize /= 100;

			CRect rt;
			CPoint margin_pos;

			margin_pos.x = m_pEffectEditor->m_pSelectFrame->GetPosX();
			margin_pos.y = m_pEffectEditor->m_pSelectFrame->GetPosY();

			CString strData;
			strData.Format(_T("%d"), margin_pos.x);
			m_ctrlSelectPosX.SetWindowText(strData);
			strData.Format(_T("%d"), margin_pos.y);
			m_ctrlSelectPosY.SetWindowText(strData);

			m_EffectImage.DrawZoom(m_fZoomSize, &rt , margin_pos);

			if(m_pEffectPreview)
			{
				if(m_pImage)
					cvReleaseImage( &m_pImage);
				m_pImage = cvCreateImage(cvSize(m_AdjustInfo.nWidht, m_AdjustInfo.nHeight), IPL_DEPTH_8U, 3);
				memcpy(m_pImage->imageData, m_AdjustInfo.pBmpBits, m_AdjustInfo.nWidht*m_AdjustInfo.nHeight*3);

				if(m_pImage)
				{
					//-- Get Zoom Position
					if(rt.left <= m_AdjustInfo.nWidht/m_fZoomSize && rt.top <= m_AdjustInfo.nHeight/m_fZoomSize)
					{
						cvSetImageROI(m_pImage, cvRect(rt.left, rt.top, m_AdjustInfo.nWidht/m_fZoomSize, m_AdjustInfo.nHeight/m_fZoomSize));
						m_pEffectPreview->CopyImage(m_pImage);
					}
					
				}		
			}
		}


	}
	

	

	if(pBmpBits != NULL)
		free(pBmpBits);

	ESMLog(1, _T("[Effect] Load Frame End"));
	m_EffectImage.Redraw();
}


void CESMEffectFrameView::OnRButtonDown(UINT nFlags, CPoint point)
{
#if 1 //신규 VMCC 기능
	CPoint pos;
	

	if(m_btnDrawLine.GetCheck())
	{
		pos = FindExactPosition(m_nPosX,m_nPosY);

		if(m_bFirstPos)
		{
			m_bFirstPos = FALSE;
			m_nLinePos1X = pos.x;
			m_nLinePos1Y = pos.y;	
		}
		else
		{
			m_bFirstPos = TRUE;
			m_nLinePos2X = pos.x;
			m_nLinePos2Y = pos.y;	
		}
		
	}
	else
	{
		pos = FindExactPosition(m_nPosX,m_nPosY);
		m_nSelectPosX = pos.x;
		m_nSelectPosY = pos.y;
		SendPostionInfo();
	}
	
	
	UpdateFrame();
#else
	FindExactPosition(point.x,point.y);
#endif

	CESMFrameViewBase::OnRButtonDown(nFlags, point);
}

CPoint CESMEffectFrameView::FindExactPosition(int nX, int nY)
{
#if 1 //신규 VMCC 기능
	CPoint pos;
	ESMAdjustImage* pAdjustImg;
	pAdjustImg = &m_EffectImage;
	
	UpdateData();
	pos = pAdjustImg->FindExactPosition(nX, nY, 0, FALSE);
	return pos;

#else
	if( m_pImage == NULL)
		return;

	if ( nX - m_nCircleSize < 0 )
		nX = m_nCircleSize;
	if ( nX + m_nCircleSize > m_pImage->width )
		nX = m_pImage->width - m_nCircleSize;

	if ( nY - m_nCircleSize < 0 )
		nY = m_nCircleSize;
	if ( nY + m_nCircleSize > m_pImage->height )
		nY = m_pImage->height - m_nCircleSize;

	POINT ptm;
	GetCursorPos( &ptm );

	CRect rtDraw(nX-m_nCircleSize,nY-m_nCircleSize,nX+m_nCircleSize,nY+m_nCircleSize);
	//DrawRect(rtDraw, RGB(0,255,0));
	CPoint pt;

	//if ( bAutoFind == FALSE )
	{
		IplImage*	pImgArea = NULL;
		IplImage*	pImgResize = NULL;

		pImgArea = cvCreateImage (cvSize(rtDraw.Width(), rtDraw.Height()) , IPL_DEPTH_8U, 3);

		cvSetImageROI (m_pImage, cvRect(rtDraw.left, rtDraw.top, rtDraw.Width(), rtDraw.Height()));
		cvCopy(m_pImage, pImgArea);
		cvResetImageROI(m_pImage);

		int nPixel = 20;
		// 1:1 비율로 변환
		pImgResize = cvCreateImage(cvSize(pImgArea->width*nPixel , pImgArea->height*nPixel), IPL_DEPTH_8U, 3);
		cvResize(pImgArea, pImgResize, cv::INTER_LANCZOS4);


		for ( int i=1 ; i < pImgArea->width ; i++ )
			cvLine(pImgResize, cvPoint(i*nPixel, 0), cvPoint(i*nPixel, pImgArea->width*nPixel), CV_RGB(207,237,197) , 2);

		for ( int i=1 ; i < pImgArea->height; i++ )
			cvLine(pImgResize, cvPoint(0, i*nPixel), cvPoint(pImgArea->height*nPixel, i*nPixel), CV_RGB(207,237,197), 2 );

		ESMEvent* pMsg = new ESMEvent;
		pMsg->pParam = (LPARAM)pImgResize;
		pMsg->nParam1 = 0;
		pMsg->nParam2 = 0;

		// 팝업
		cvNamedWindow( "Area Image", 0 );
		cvResizeWindow( "Area Image", pImgResize->width, pImgResize->height );
		cvShowImage( "Area Image", pImgResize );

		CRect rect;

		GetWindowRect(rect);
		cvMoveWindow("Area Image", ptm.x - pImgResize->width/2, ptm.y - pImgResize->height/2);
		cvSetMouseCallback("Area Image", OnEventhandle1,  (void *)pMsg);
		//cvDestroyWindow("Area Image");
		cvWaitKey(0);

		pt.x = rtDraw.left + (pMsg->nParam1)/nPixel + 1;
		pt.y = rtDraw.top + (pMsg->nParam2)/nPixel + 1;

		cvReleaseImage(&pImgArea);
		cvReleaseImage(&pImgResize);
		delete pMsg;
	}

	SetPtZomm(pt);
	//-- MOUSE LEAVE
	//MouseEvent();
	//-- ReDraw
	Invalidate(FALSE);
	//-- 2014-07-19 hongsu@esmlab.com
	//-- Update Position
	SendPostionInfo();
#endif
}

void OnEventhandle1 (int event, int x, int y, int flags, void* param) //핸들러 함수
{
	ESMEvent* pMsg =  (ESMEvent* )param;
	IplImage    *image;
	image = (IplImage *)pMsg->pParam;

	switch(event)
	{
	case CV_EVENT_RBUTTONDOWN:
		pMsg->nParam1 = x;
		pMsg->nParam2 = y;
		cvDestroyWindow("Area Image");
		break;
	}
	//cvShowImage("Area Image", image);
}

void CESMEffectFrameView::OnMouseHWheel(UINT nFlags, short zDelta, CPoint pt)
{
		
	CESMFrameViewBase::OnMouseHWheel(nFlags, zDelta, pt);
}

CPoint CESMEffectFrameView::GetPointFromMouse(CPoint pt)
{
	ESMAdjustImage* pAdjustImg;
	pAdjustImg = &m_EffectImage;
	

	CRect rect;
	pAdjustImg->GetWindowRect(&rect);
	ScreenToClient(&rect);

	CPoint ptRet;
	pt.x = pt.x - rect.left;
	pt.y = pt.y - rect.top;
	CPoint ptImage = pAdjustImg->GetImagePos();
	ptRet.x = (int)(ptImage.x * pAdjustImg->GetMultiple() + pt.x * pAdjustImg->GetMultiple());
	ptRet.y = (int)(ptImage.y * pAdjustImg->GetMultiple() + pt.y * pAdjustImg->GetMultiple());

	return ptRet;
}

void CESMEffectFrameView::GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;

	Mat rot_mat(cv::Size(2, 3), CV_32FC1);
	cv::Point rot_center( nCenterX, nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	//CString strTemp;
	//strTemp.Format(_T("%f"), dbAngleAdjust);
	//m_AdjustDsc2ndList.SetItemText(m_nSelectedNum, 17, strTemp);

	rot_mat = getRotationMatrix2D(rot_center, dbAngleAdjust, dScale);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), cv::INTER_CUBIC+cv::WARP_FILL_OUTLIERS);
	cuda::GpuMat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cuda::warpAffine(*gMat, d_rotate, rot_mat, cv::Size(gMat->cols, gMat->rows), cv::INTER_CUBIC);
	d_rotate.copyTo(*gMat);
		
	//releaseMat(rot_mat);

}

void CESMEffectFrameView::GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}

void CESMEffectFrameView::GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY)
{
	cuda::GpuMat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;


	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	cuda::copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}


void CESMEffectFrameView::CpuMakeMargin(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nHeight,nWidth;

	nWidth = gMat->cols - nX*2;
	nHeight = gMat->rows - nY*2;


	gMatCut = (*gMat)(cv::Rect(nX, nY,nWidth, nHeight));
	copyMakeBorder(gMatCut, gMatPaste, nY,nY,nX,nX, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);

}
void CESMEffectFrameView::CpuMoveImage(Mat* gMat, int nX, int nY)
{
	Mat gMatCut, gMatPaste;
	int nCutTop,nCutHeight,nCutLeft,nCutWidth;
	int nPasteTop=0,nPasteBottom=0,nPasteLeft=0,nPasteRight=0;

	if ( nX > 0 )
	{
		nCutLeft = 0;
		nCutWidth = gMat->cols - nX;

		nPasteLeft = nX;
	}
	else
	{
		nCutLeft = -nX;
		nCutWidth = gMat->cols + nX;

		nPasteRight = -nX;
	}

	if ( nY > 0 )
	{
		nCutTop = 0;
		nCutHeight = gMat->rows - nY;

		nPasteTop = nY;
	}
	else
	{
		nCutTop = -nY;
		nCutHeight = gMat->rows + nY;

		nPasteBottom = -nY;
	}

	gMatCut = (*gMat)(cv::Rect(nCutLeft, nCutTop,nCutWidth, nCutHeight));
	copyMakeBorder(gMatCut, gMatPaste, nPasteTop,nPasteBottom,nPasteLeft,nPasteRight, BORDER_CONSTANT, 0);
	gMatPaste.copyTo(*gMat);
}
void CESMEffectFrameView::CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle)
{
	Mat Iimage2;// = cvCreateImage(cvGetSize(Iimage), IPL_DEPTH_8U, 3);

	if(dScale == 0)
	{
		dScale = 1;
		dAngle += -90;
	}

	if( dAngle == 0.0 && dScale == 0.0)
		return ;
	//cvShowImage("2",Iimage);
	CvMat *rot_mat = cvCreateMat( 2, 3, CV_32FC1);
	CvPoint2D32f rot_center = cvPoint2D32f( nCenterX, nCenterY);
	if( dScale == 0.0 && dAngle == 0.0 )
		return;

	double dbAngleAdjust = -1 * (dAngle + 90);

	//CString strTemp;
	//strTemp.Format(_T("%f"), dbAngleAdjust);
	//m_AdjustDsc2ndList.SetItemText(m_nSelectedNum, 17, strTemp);

	cv2DRotationMatrix( rot_center, dbAngleAdjust, dScale, rot_mat);

	//cuda::warpAffine(*gMat,d_rotate, rot_mat, Size(2500, 1500), cv::INTER_CUBIC+cv::WARP_FILL_OUTLIERS);
	//Mat d_rotate;

	//-- 2014-07-16 hongsu@esmlab.com
	//-- ? CvPoint2D32f srcTri[3], dstTri[3];
	cvWarpAffine(&IplImage(Iimage), &IplImage(Iimage), rot_mat,cv::INTER_LINEAR+cv::WARP_FILL_OUTLIERS);	//d_rotate.copyTo(*gMat);
	//imshow("Iimage",Iimage);
	//cvWaitKey(0);
	//gMat =&(cvarrToMat(Iimage2));//
	//Iimage = cvCreateImage(cvGetSize(&Iimage), IPL_DEPTH_8U, 3);
	cvReleaseMat(&rot_mat);
}

int CESMEffectFrameView::Round(double dData)
{
	int nResult = 0;
	if( dData == 0)
		nResult = 0;
	else if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);

	return nResult;
}

void CESMEffectFrameView::OnBnClickedBtnDrawLine()
{
	CString strData;
	m_nPointCnt = 0;
	m_edLineCount.GetWindowText(strData);
	m_nPointCnt = _ttoi(strData);
	LoadFrame();
}
