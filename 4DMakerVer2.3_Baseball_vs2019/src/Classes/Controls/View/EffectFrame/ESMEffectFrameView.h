////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectFrameView.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-10
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "ESMFrameViewBase.h"
#include "cv.h"
#include "ESMAdjustImage.h"
#include "ESMIndex.h"
#include "ESMDefine.h"
#include "afxwin.h"

class CESMEffectEditor;
class CESMEffectPreview;

using namespace cv;

// CESMFrameNailDlg dialog
class CESMEffectFrameView : public CESMFrameViewBase
{
	DECLARE_DYNAMIC(CESMEffectFrameView)



public:
	CESMEffectFrameView(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMEffectFrameView();

	// Dialog Data
	//{{AFX_DATA(CESMEffectFrameView)
	enum { IDD = IDD_VIEW_EFFECT_VIEWER };
	//}}AFX_DATA

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.


private:
	IplImage* m_pImage;

public:	
	CEdit m_ctrlSelectPosX;
	CEdit m_ctrlSelectPosY;

	BOOL m_bAdjust;
	int m_MarginX;
	int m_MarginY;
	ESMAdjustImage m_EffectImage;
	CESMEffectEditor* m_pEffectEditor;
	CESMEffectPreview* m_pEffectPreview;
	DscAdjustInfo m_AdjustInfo;
	vector<stAdjustInfo> m_adjinfo;
	int m_nPosX, m_nPosY;
	int m_nSelectPosX, m_nSelectPosY;
	int m_nLinePos1X, m_nLinePos1Y;
	int m_nLinePos2X, m_nLinePos2Y;
	BOOL m_bFirstPos;
	int m_nPointCnt;

	//Adjust
	int Round(double dData);
	void GpuMakeMargin(cuda::GpuMat* gMat, int nX, int nY);
	void GpuMoveImage(cuda::GpuMat* gMat, int nX, int nY);
	void GpuRotateImage(cuda::GpuMat* gMat, double nCenterX, double nCenterY,  double dScale, double dAngle);

	void CpuMakeMargin(Mat* gMat, int nX, int nY);
	void CpuMoveImage(Mat* gMat, int nX, int nY);
	void CpuRotateImage(Mat Iimage, double nCenterX, double nCenterY,  double dScale, double dAngle);

	BOOL LoadAdjust();
	void SetEffectEditor(CESMEffectEditor* pEditor) {m_pEffectEditor = pEditor;}
	void SetPreview(CESMEffectPreview* pView) {m_pEffectPreview = pView;}

	void UpdateFrame();
	void DrawInfo(CDC* pDC);
	void LoadFrame(CDC* pDC);
	void LoadFrame();
	void SendPostionInfo();
	void SendPostionAccept();
	BOOL DoMouseWheel(UINT, short, CPoint);
	CPoint GetPointFromMouse(CPoint pt);


protected:
	CRect m_rect;
	CSize m_szZoom;
	CPoint m_ptZoom;
	float m_fZoomSize;
	BOOL m_bZoomMove;	
	BOOL m_bSetZoomPos;
	int m_nCircleSize;

	void DrawZoomIndicator(CDC* pDC);
	void SetPtZomm(CPoint pt);
	void SetZommRatio(int nRatio);


	BOOL PtInRect(CPoint pt);	
	void MouseEvent();

protected:
	virtual BOOL OnInitDialog();	
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	afx_msg void OnBtnPosMove();


	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);	
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	DECLARE_MESSAGE_MAP()		

	//CMiLRe 20160115 Effect Time View 방향키로 포지션 이동
public:
	void SetKeyMoveCenterPosition(CPoint point);
	void SetKeyMoveCenterPositionUpdate(){Invalidate(FALSE);};
	CPoint GetZoomPoint() {return m_ptZoom;};
	void SetZoomMove(BOOL bValue) {m_bZoomMove = bValue;};
	BOOL GetZoomMove() {return m_bZoomMove;};
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	CPoint FindExactPosition(int nX, int nY);
	afx_msg void OnMouseHWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnBnClickedBtnDrawLine();
	CButton m_btnDrawLine;
	CEdit m_edLineCount;
};