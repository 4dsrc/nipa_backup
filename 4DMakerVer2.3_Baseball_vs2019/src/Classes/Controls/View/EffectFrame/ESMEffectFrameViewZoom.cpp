////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectFrameView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-18
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "DSCViewDefine.h"
#include "ESMEffectFrameView.h"
#include "ESMObjectFrame.h"
#include "ESMEffectEditor.h"
#include "ESMEffectPreview.h"

#include "ESMCtrl.h"
#include "ESMIndex.h"

//------------------------------------------------------------------------------ 
//! @brief		DrawZoomPosition(CDC* pMemDC, CRect rect)
//! @date		2010-06-22
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CESMEffectFrameView::DrawZoomIndicator(CDC* pDC)
{
	//-- Check Zoom Option
	if(!m_pEffectEditor)
		return;

	//if(!m_pEffectEditor->m_dlgSpotInfo.m_bZoom)
	//	return;
	
	m_fZoomSize = (float)m_pEffectEditor->m_pSelectFrame->GetZoomRatio();
	m_fZoomSize /= 100;

	CRect rect;
	GetClientRect(&rect);
	m_szZoom = CSize((int) (rect.Width()/m_fZoomSize), (int) (rect.Height()/m_fZoomSize));
	
	m_ptZoom.x = m_pEffectEditor->m_pSelectFrame->GetPosX() * rect.Width()	/ FULL_UHD_WIDTH;
	m_ptZoom.y = m_pEffectEditor->m_pSelectFrame->GetPosY() * rect.Height() / FULL_UHD_HEIGHT;
	//-- 2011-06-23
	//-- Check Position
	SetPtZomm(m_ptZoom);

	CRect rt;
	rt.left		= m_ptZoom.x - m_szZoom.cx/2;
	rt.right	= m_ptZoom.x + m_szZoom.cx/2;
	rt.top		= m_ptZoom.y - m_szZoom.cy/2;
	rt.bottom	= m_ptZoom.y + m_szZoom.cy/2;

	CPen newPen;
	CPen *oldPen;
	
	if(m_bZoomMove)
	{
		newPen.CreatePen(PS_SOLID, 3, RGB(255,0,0));		
		oldPen=pDC->SelectObject(&newPen);
	}
	else
	{
		newPen.CreatePen(PS_SOLID, 3, RGB(233,233,233));
		oldPen=pDC->SelectObject(&newPen);
	}

	//-- Draw Rect
	//-- TopLeft
	pDC->MoveTo(rt.left + rt.Height()/10 ,rt.top);
	pDC->LineTo(rt.left, rt.top);
	pDC->LineTo(rt.left, rt.top + rt.Height()/10);

	//-- BottomLeft
	pDC->MoveTo(rt.left , rt.bottom - rt.Height()/10 );
	pDC->LineTo(rt.left, rt.bottom);
	pDC->LineTo(rt.left + rt.Height()/10, rt.bottom);

	//-- TopRight
	pDC->MoveTo(rt.right - rt.Height()/10 ,rt.top);
	pDC->LineTo(rt.right, rt.top);
	pDC->LineTo(rt.right, rt.top + rt.Height()/10);

	//-- BottomRight
	pDC->MoveTo(rt.right , rt.bottom - rt.Height()/10 );
	pDC->LineTo(rt.right, rt.bottom);
	pDC->LineTo(rt.right - rt.Height()/10, rt.bottom);
	/*
	pDC->MoveTo(rt.right/2 , rt.top);
	pDC->LineTo(rt.right/2, rt.bottom);
	pDC->MoveTo(rt.left , rt.bottom/2);
	pDC->LineTo(rt.right, rt.bottom/2);
	*/
	//-- Center	
	
	pDC->MoveTo(rt.CenterPoint().x , rt.CenterPoint().y - rt.Height()/10 );
	pDC->LineTo(rt.CenterPoint().x , rt.CenterPoint().y + rt.Height()/10 );
	pDC->MoveTo(rt.CenterPoint().x - rt.Height()/10 , rt.CenterPoint().y );
	pDC->LineTo(rt.CenterPoint().x + rt.Height()/10 , rt.CenterPoint().y );
	

	//-- 2014-07-19 hongsu@esmlab.com
	//-- Send Image to Preview
	if(m_pEffectPreview)
	{
		if(m_pImage)
		{
			//-- Get Zoom Position
			cvSetImageROI(m_pImage, cvRect(m_ptZoom.x - m_szZoom.cx/2, m_ptZoom.y - m_szZoom.cy/2,  m_szZoom.cx, m_szZoom.cy));
			m_pEffectPreview->CopyImage(m_pImage);
		}		
	}

	pDC->SelectObject(oldPen);
	newPen.DeleteObject();

}

BOOL CESMEffectFrameView::PtInRect(CPoint pt)
{
	CRect rt;
	rt.left		= m_ptZoom.x - m_szZoom.cx/2;
	rt.right	= m_ptZoom.x + m_szZoom.cx/2;
	rt.top		= m_ptZoom.y - m_szZoom.cy/2;
	rt.bottom	= m_ptZoom.y + m_szZoom.cy/2;
		
	if(rt.PtInRect(pt))
		return TRUE;
	return FALSE;
}

void CESMEffectFrameView::SetPtZomm(CPoint pt)
{
	if(pt.x < m_szZoom.cx/2)							m_ptZoom.x = m_szZoom.cx/2;
	else if(pt.x > m_rect.Width() - m_szZoom.cx/2)		m_ptZoom.x = m_rect.Width() - m_szZoom.cx/2;
	else												m_ptZoom.x = pt.x;

	if(pt.y < m_szZoom.cy/2)							m_ptZoom.y = m_szZoom.cy/2;
	else if(pt.y > m_rect.Height() - m_szZoom.cy/2)		m_ptZoom.y = m_rect.Height() - m_szZoom.cy/2;
	else												m_ptZoom.y = pt.y;
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-07-21
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMEffectFrameView::SetZommRatio(int nRatio)
{
	m_fZoomSize = (float)((float)nRatio/100);
	CRect rect;
	GetClientRect(&rect);
	m_szZoom = CSize((int) (rect.Width()/m_fZoomSize), (int) (rect.Height()/m_fZoomSize));

	if(m_ptZoom.x < m_szZoom.cx/2)							
		m_ptZoom.x = m_szZoom.cx/2;
	else if(m_ptZoom.x > m_rect.Width() - m_szZoom.cx/2)	
		m_ptZoom.x = m_rect.Width() - m_szZoom.cx/2;

	if(m_ptZoom.y < m_szZoom.cy/2)							
		m_ptZoom.y = m_szZoom.cy/2;
	else if(m_ptZoom.y > m_rect.Height() - m_szZoom.cy/2)	
		m_ptZoom.y = m_rect.Height() - m_szZoom.cy/2;
}

void CESMEffectFrameView::SendPostionInfo()
{
#if 1
	float fX = (float) m_nSelectPosX;
	float fY = (float) m_nSelectPosY;
#else
	//-- 2014-07-19 hongsu@esmlab.com
	//-- Change Ratio
	CRect rect;
	GetClientRect(rect);
	float fX = (float) ( m_ptZoom.x * FULL_UHD_WIDTH  / rect.Width());
	float fY = (float) ( m_ptZoom.y * FULL_UHD_HEIGHT / rect.Height());

#endif

	m_pEffectEditor->m_dlgSpotInfo.UpdatePosition((int)fX , (int)fY);
}

void CESMEffectFrameView::SendPostionAccept()
{
	//-- 2014-07-19 hongsu@esmlab.com
	//-- Change Ratio
	CRect rect;
	GetClientRect(rect);
	float fX = (float) ( m_ptZoom.x * FULL_UHD_WIDTH  / rect.Width());
	float fY = (float) ( m_ptZoom.y * FULL_UHD_HEIGHT / rect.Height());
	m_pEffectEditor->m_dlgSpotInfo.AcceptPosition((int)fX , (int)fY);
}
