////////////////////////////////////////////////////////////////////////////////
//
//	ESMFrameViewBase.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-18
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FileOperations.h"
#include "ESMFrameViewBase.h"

#include "ESMFunc.h"
#include "ESMObjectFrame.h"
#include "FFmpegManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CESMFrameViewBase, CDialog)

CESMFrameViewBase::CESMFrameViewBase(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMFrameViewBase::IDD, pParent)	
{
	m_pObjectFrame  = NULL;
	m_pFont			= NULL;
}

CESMFrameViewBase::~CESMFrameViewBase()
{
	if(m_pFont)
	{
		delete m_pFont;
		m_pFont = NULL;
	}
}

void CESMFrameViewBase::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMFrameViewBase)
	//}}AFX_DATA_MAP	
}

BOOL CESMFrameViewBase::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN || pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
			return FALSE;		
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMFrameViewBase, CDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()		
	ON_WM_ERASEBKGND()	
END_MESSAGE_MAP()

BOOL CESMFrameViewBase::OnEraseBkgnd(CDC* pDC)
{
	return true;
}

// CFrameNailDlg message handlers
BOOL CESMFrameViewBase::OnInitDialog()
{
	CDialog::OnInitDialog();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CESMFrameViewBase::OnSize(UINT nType, int cx, int cy) 
{
	UpdateFrame();
	CDialog::OnSize(nType, cx, cy);	
} 

void CESMFrameViewBase::OnPaint()
{	
	UpdateFrame();
	CDialog::OnPaint();
}

void CESMFrameViewBase::SetObjectFrame(CESMObjectFrame* pObjFrm)
{
	m_pObjectFrame = pObjFrm;
	UpdateFrame();
}

void CESMFrameViewBase::DrawFrame(CESMObjectFrame* pObjFrm)
{
	if(m_pObjectFrame == pObjFrm)
		return;
	m_pObjectFrame = pObjFrm;
	UpdateFrame();
}

void CESMFrameViewBase::SetFrameFont(CDC* pDC, int nSize, CString strFont, int nFontType)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	pDC->SelectObject(m_pFont);	
}
