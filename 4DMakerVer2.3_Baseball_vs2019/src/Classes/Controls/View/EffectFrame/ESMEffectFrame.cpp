////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectEditorViewer.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FileOperations.h"
#include "ESMEffectFrame.h"
#include "ESMEffectPreview.h"
#include "ESMEffectEditor.h"
#include "DSCItem.h"

#include "ESMFunc.h"
#include "SRSIndex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CESMEffectFrame, CDialog)

CESMEffectFrame::CESMEffectFrame(CWnd* pParent /*=NULL*/)
	: CDialog(CESMEffectFrame::IDD, pParent)	
{
	m_pFrameView	= NULL;	
}

CESMEffectFrame::~CESMEffectFrame()
{
	if(m_pFrameView)
	{
		delete m_pFrameView;
		m_pFrameView = NULL;
	}
}

void CESMEffectFrame::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMEffectFrame)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
}

BEGIN_MESSAGE_MAP(CESMEffectFrame, CDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()		
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEWHEEL()
	ON_WM_KEYDOWN()
	ON_COMMAND(ID_IMAGE_MOVIEPLAY , OnPreviewPlay )
	ON_UPDATE_COMMAND_UI(ID_IMAGE_MOVIEPLAY	, OnUpdatePlay )	
END_MESSAGE_MAP()

BOOL CESMEffectFrame::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);
	pDC->FillSolidRect(rect, COLOR_BASIC_BG_2 );
	Invalidate(FALSE);
	return TRUE;
}

BOOL CESMEffectFrame::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN || pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
			return FALSE;				
	}

	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN)
		{
			BOOL bShift = ((GetKeyState(VK_SHIFT) & 0x8000) != 0);
			BOOL bControl = ((GetKeyState(VK_CONTROL) & 0x8000) != 0);
			BOOL bAlt = ((GetKeyState(VK_MENU) & 0x8000) != 0);

			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			if(bControl && !bShift && !bAlt)
			{
				ESMEvent* pEsmMsg = NULL;
				pEsmMsg = new ESMEvent();

				pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
				pEsmMsg->nParam1 = pMsg->wParam;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
			}

			//CMiLRe 20160115 Effect Time View 방향키로 포지션 이동
			if(pMsg->wParam >= VK_LEFT && pMsg->wParam <= VK_DOWN)
			{
				if(!m_pFrameView->GetZoomMove())
				{
					m_pFrameView->SetZoomMove(TRUE);
				}
				int nMoveSize = 1;
				CPoint NowPoint = m_pFrameView->GetZoomPoint();
				switch(pMsg->wParam)
				{
				case VK_LEFT:
					NowPoint.x = NowPoint.x - nMoveSize;
					break;
				case VK_RIGHT:
					NowPoint.x = NowPoint.x + nMoveSize;
					break;
				case VK_UP:
					NowPoint.y = NowPoint.y - nMoveSize;
					break;
				case VK_DOWN:
					NowPoint.y = NowPoint.y + nMoveSize;
					break;
				}				
				m_pFrameView->SetKeyMoveCenterPosition(NowPoint);

				
				
				return TRUE;
			}
				

		}
		else if(pMsg->message==WM_KEYUP)
		{
			if(pMsg->wParam >= VK_LEFT && pMsg->wParam <= VK_DOWN)
			{
				m_pFrameView->SetZoomMove(FALSE);
				m_pFrameView->SetKeyMoveCenterPositionUpdate();
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

// CFrameNailDlg message handlers
BOOL CESMEffectFrame::OnInitDialog()
{
	CDialog::OnInitDialog();

	//-- 2014-07-18 hongsu@esmlab.com
	//-- Create Viewer
	if(!m_pFrameView)
	{
		m_pFrameView = new CESMEffectFrameView();
		m_pFrameView ->Create(CESMEffectFrameView::IDD, this);
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CESMEffectFrame::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_MOVIEPLAY		,
	};

	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	

	m_wndToolBar.ShowWindow(SW_SHOW);
}

void CESMEffectFrame::OnSize(UINT nType, int cx, int cy) 
{
	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width()		< FRAME_MIN_WIDTH || 
	    m_rcClientFrame.Height()	< FRAME_MIN_HEIGHT)
		return;

	//-- ReloadFrame
	UpdateFrame(TRUE);
	CDialog::OnSize(nType, cx, cy);	
} 

void CESMEffectFrame::OnPaint()
{	
	UpdateFrame();
	CDialog::OnPaint();
}

void CESMEffectFrame::OnPreviewPlay()
{
	m_pFrameView->m_pEffectEditor->PreviewPlay();
}

void CESMEffectFrame::OnUpdatePlay(CCmdUI *pCmdUI)
{
	if(m_pFrameView->m_pEffectEditor->IsPlay())
		pCmdUI->SetCheck(TRUE);
	else
		pCmdUI->SetCheck(FALSE);	
}

void CESMEffectFrame::UpdateFrame(BOOL bReloadImage)
{
	//-- 2014-07-17 hongsu@esmlab.com
	//-- Draw Frame
	CPaintDC dc(this);
		
	CRect rect;	
	rect.CopyRect(m_rcClientFrame);
	int nHeight = rect.Height();
	int nWidth	= rect.Width();
	
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), SRCCOPY);

	CString strFilePath;
	mDC.FillRect(rect, &CBrush(COLOR_BASIC_BG_2));

	//-- Frame List
	if(m_pFrameView)
	{
		BOOL bRatio = FALSE; // [TRUE] WIDTH {FALSE] HEIGHT
		int nMargin;	
		float nRatio = (float)nWidth/(float)nHeight;
		float nBaseRatio = (float) FULL_UHD_WIDTH /(float) FULL_UHD_HEIGHT;						// 3:2

		if(nRatio > nBaseRatio)
		{
			nWidth =  (int)((float)nHeight / (float)FULL_UHD_HEIGHT * (float)FULL_UHD_WIDTH);	// 3:2
			bRatio = TRUE;
			nMargin = (rect.Width() - nWidth)/2;			
		}
		else
		{
			nHeight = (int)((float)nWidth / (float)FULL_UHD_WIDTH* (float) FULL_UHD_HEIGHT );	// 3:2
			bRatio = FALSE;
			nMargin = (rect.Height() - nHeight)/2;
		}

		if(bRatio)  rect.left += nMargin;	// Width
		else		rect.top += nMargin;	// Height

		rect.right = rect.left + nWidth;
		rect.bottom = rect.top + nHeight;

		//-- 2014-07-18 hongsu@esmlab.com
		//-- Move Effect Frame View 
		m_pFrameView->ShowWindow(SW_SHOW);
		m_pFrameView->MoveWindow(rect);
		m_pFrameView->Invalidate(FALSE);	
	}	
	
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();
}

void CESMEffectFrame::ClearPreview()
{
	m_pFrameView->m_pEffectPreview->ClearPreview();
}

void CESMEffectFrame::DrawFrame(CESMObjectFrame* pObjFrm)
{
	if(!m_pFrameView)
		return;

	if(m_pFrameView->m_pObjectFrame == pObjFrm)
		return;

	//-- Set New Object Frame
	m_pFrameView->SetObjectFrame(pObjFrm);
}

//------------------------------------------------------------------------------
//! @function	Mouse Wheel
//! @brief		Send Event to FrameViewer
//! @date		2014-07-21
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
BOOL CESMEffectFrame::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	return m_pFrameView->DoMouseWheel(nFlags, zDelta, pt);
}

//------------------------------------------------------------------------------ 
//! @brief		OnKeyDown
//! @date		2011-06-22
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CESMEffectFrame::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	switch (nChar)
	{
	case ESM_KEY_ENTER:	//-- Play or Stop Preview
		m_pFrameView->m_pEffectEditor->PreviewPlay();
		break;
	}
	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}
