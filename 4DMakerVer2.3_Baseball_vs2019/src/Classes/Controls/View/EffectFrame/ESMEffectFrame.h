////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectFrame.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-10
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
//-- 2011-07-27 hongsu.jung
#include "ESMEffectFrameView.h"


class CDSCItem;
class CESMObjectFrame;
class CESMEffectEditor;

// CESMFrameNailDlg dialog
class CESMEffectFrame : public CDialog
{
	//-- For Toolbar
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	DECLARE_DYNAMIC(CESMEffectFrame)
public:
	CESMEffectFrame(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMEffectFrame();

	//-- For Toolbar
	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CESMEffectFrame)
	enum { IDD = IDD_VIEW_ONLY_TOOLBAR };
	//}}AFX_DATA

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	CRect m_rcClientFrame;	
	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;		

public:	
	CESMEffectFrameView* m_pFrameView;

	void ClearPreview();
	void SetEffectEditor(CESMEffectEditor* pEditor) {m_pFrameView->SetEffectEditor(pEditor);}
	void SetPreview(CESMEffectPreview* pView)		{m_pFrameView->SetPreview(pView);}
	void DrawFrame(CESMObjectFrame* pObjFrm);
	void UpdateFrame(BOOL bReloadImage = TRUE);
	CESMEffectFrameView* GetView()					{return m_pFrameView;}

	void OnPreviewPlay();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	afx_msg BOOL OnMouseWheel(UINT, short, CPoint);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnUpdatePlay(CCmdUI *pCmdUI);	
	DECLARE_MESSAGE_MAP()		

};