////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectPreviewView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-18
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "DSCViewDefine.h"
#include "ESMEffectPreviewView.h"
#include "ESMObjectFrame.h"
#include "ESMCtrl.h"

#include "ESMFunc.h"
#ifdef _WIN64
//#include "CvvImage.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CESMEffectPreviewView, CDialog)

CESMEffectPreviewView::CESMEffectPreviewView(CWnd* pParent /*=NULL*/)
	: CDialog(CESMEffectPreviewView::IDD, pParent)
{
	m_pImgZoom = NULL;
}

CESMEffectPreviewView::~CESMEffectPreviewView()
{

}

BEGIN_MESSAGE_MAP(CESMEffectPreviewView, CDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()		
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CFrameNailDlg message handlers
BOOL CESMEffectPreviewView::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN || pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
			return FALSE;
	}

	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN)
		{
			BOOL bShift = ((GetKeyState(VK_SHIFT) & 0x8000) != 0);
			BOOL bControl = ((GetKeyState(VK_CONTROL) & 0x8000) != 0);
			BOOL bAlt = ((GetKeyState(VK_MENU) & 0x8000) != 0);

			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			if(bControl && !bShift && !bAlt)
			{
				ESMEvent* pEsmMsg = NULL;
				pEsmMsg = new ESMEvent();

				pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
				pEsmMsg->nParam1 = pMsg->wParam;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
			}

		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CESMEffectPreviewView::OnInitDialog()
{
	return CDialog::OnInitDialog();
}

void CESMEffectPreviewView::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	UpdateFrame();
} 

void CESMEffectPreviewView::OnPaint()
{	
	UpdateFrame();
	CDialog::OnPaint();
}

BOOL CESMEffectPreviewView::OnEraseBkgnd(CDC* pDC)
{
	return true;
}

void CESMEffectPreviewView::ClearPreview()
{
	if(m_pImgZoom)
		cvReleaseImage(&m_pImgZoom);

	//-- 2011-05-16 hongsu.jung		
	CPaintDC dc(this);	

	//-- GetRect
	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	mDC.FillRect(rect, &CBrush(COLOR_BASIC_BG_DARK));

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);	
	Invalidate(FALSE);

	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();
}

void CESMEffectPreviewView::CopyImage(IplImage *pImgZoom)
{
	CRect rect;
	GetClientRect(&rect);
	if(m_pImgZoom)
		cvReleaseImage(&m_pImgZoom);
	m_pImgZoom = cvCreateImage( cvSize(rect.Width(),rect.Height()),pImgZoom->depth, pImgZoom->nChannels );
	cvResize(pImgZoom, m_pImgZoom );

	//-- Test
	//cvShowImage("test", m_pImgZoom);
	//cvShowImage("test1", pImgZoom);


	UpdateFrame();	
	Invalidate(FALSE);
}

void CESMEffectPreviewView::UpdateFrame()
{
	if(!m_pImgZoom)	
		return;

	//-- GetRect
	CRect rect;
	GetClientRect(&rect);

	//-- 2011-05-16 hongsu.jung		
	CPaintDC dc(this);	
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);


	//-- Draw Liveview
	//CvvImage cImage;		
	//cImage.CopyOf(m_pImgZoom,8);

	//cImage.Show(mDC.GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height() );	
	//cImage.Destroy();

	mDC.MoveTo(rect.right/2, rect.top);
	mDC.LineTo(rect.right/2, rect.bottom);	
	mDC.MoveTo(rect.left, rect.bottom/2);
	mDC.LineTo(rect.right, rect.bottom/2);
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();
}

