////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectPreviewView.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-10
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "cv.h"


// CESMFrameNailDlg dialog
class CESMEffectPreviewView : public CDialog
{
	DECLARE_DYNAMIC(CESMEffectPreviewView)
public:
	CESMEffectPreviewView(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMEffectPreviewView();

	// Dialog Data
	//{{AFX_DATA(CESMEffectPreviewView)
	enum { IDD = IDD_VIEW_EMPTY };
	//}}AFX_DATA

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	IplImage* m_pImgZoom;

public:	
	void ClearPreview();
	void UpdateFrame();	
	void CopyImage(IplImage *pImgZoom);

protected:
	virtual BOOL OnInitDialog();	
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	DECLARE_MESSAGE_MAP()		

};