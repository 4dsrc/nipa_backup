////////////////////////////////////////////////////////////////////////////////
//
//	ESMEffectFrameView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-18
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "DSCViewDefine.h"
#include "ESMEffectFrameView.h"
#include "ESMObjectFrame.h"
#include "ESMEffectEditor.h"
#include "ESMEffectPreview.h"

#include "ESMCtrl.h"

#define RATIO_WHEEL_UNIT 2
#define WEIGHT_WHEEL_UNIT 1

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-22
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CESMEffectFrameView::OnLButtonDown(UINT nFlags, CPoint point)
{
#if 1
	ESMAdjustImage* pAdjustImg;
	pAdjustImg = &m_EffectImage;
	
	pAdjustImg->OnLButtonDown(nFlags, point);
	pAdjustImg->SetFocus();

#else
	if ( m_pObjectFrame == NULL )
		return;

	if(PtInRect(point))
		m_bZoomMove = TRUE;
	else
		m_bZoomMove = FALSE;
	Invalidate(FALSE); 

	//-- 2014-07-21 hongsu@esmlab.com
	//-- Set Focus to Parent
	::SetFocus(GetParent()->GetSafeHwnd());
#endif

	CESMFrameViewBase::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-22
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CESMEffectFrameView::OnLButtonUp(UINT nFlags, CPoint point)
{
#if 1
	ESMAdjustImage* pAdjustImg;
	pAdjustImg = &m_EffectImage;
	
	pAdjustImg->OnLButtonUp(nFlags, point);
		
#else
	if ( m_pObjectFrame == NULL )
		return;

	m_bZoomMove = FALSE;
	Invalidate(FALSE);   
	//-- 2014-07-22 hongsu@esmlab.com
	//-- Recalculation
	SendPostionAccept();
#endif
	CESMFrameViewBase::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-22
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CESMEffectFrameView::OnMouseMove(UINT nFlags, CPoint point)
{
#if 1
	ESMAdjustImage* pAdjustImg;
	pAdjustImg = &m_EffectImage;
	
	{
		CString strPos;
		CRect rect;
		pAdjustImg->GetWindowRect(&rect);
		ScreenToClient(&rect);
		int nPos = 0;
		if( point.x - rect.left < 0 || point.x > rect.right || point.y - rect.top < 0 || point.y > rect.bottom)
		{

			CDialogEx::OnMouseMove(nFlags, point);
			return;
		}

		//-- 2014-08-31 hongsu@esmlab.com
		//-- Get Image Position from Mouse Point
		CPoint ptImage;
		ptImage = GetPointFromMouse(point);
		m_nPosX = ptImage.x - 1;
		m_nPosY = ptImage.y - 1;

		pAdjustImg->OnMouseMove(nFlags, point);
	}
#else
	if ( m_pObjectFrame == NULL )
		return;

	if(!m_pEffectEditor->m_dlgSpotInfo.m_bZoom)
		return;
	if(m_bZoomMove)
	{		
		//-- Set Base Rect Size
		GetClientRect(m_rect);
		//-- Check Size, and Rect
		SetPtZomm(point);
		//-- MOUSE LEAVE
		MouseEvent();
		//-- ReDraw
		Invalidate(FALSE);
		//-- 2014-07-19 hongsu@esmlab.com
		//-- Update Position
		SendPostionInfo();
		//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
		m_pEffectEditor->m_dlgSpotInfo.AdjustEffectInfo(FALSE);
	}
#endif
	CESMFrameViewBase::OnMouseMove(nFlags, point);
}

void CESMEffectFrameView::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CESMEffectFrameView::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	if ( m_pObjectFrame == NULL )
		return FALSE;

	if(m_bZoomMove)
	{
		m_bZoomMove = FALSE;
		Invalidate(FALSE);   
	}	
	return 0L;
}   

//CMiLRe 20160115 Effect Time View 방향키로 포지션 이동
void CESMEffectFrameView::SetKeyMoveCenterPosition(CPoint point)
{
	if ( m_pObjectFrame == NULL )
		return;

	if(!m_pEffectEditor->m_dlgSpotInfo.m_bZoom)
		return;
	if(m_bZoomMove)
	{		
		//-- Set Base Rect Size
		GetClientRect(m_rect);
		//-- Check Size, and Rect		
		SetPtZomm(point);
		//-- 2014-07-19 hongsu@esmlab.com
		//-- Update Position
		SendPostionInfo();
		//m_pEffectEditor->m_dlgSpotInfo.AdjustEffectInfo();
		SendPostionAccept();
	}
}

//------------------------------------------------------------------------------
//! @function	Mouse Wheel
//! @brief		Event From Parent Dialog
//! @date		2014-07-21
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
BOOL CESMEffectFrameView::DoMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
#if 1
	ESMAdjustImage* pAdjustImg;

	pAdjustImg = &m_EffectImage;
	CRect rtWindow;
	GetWindowRect(rtWindow);

	pt.x -= rtWindow.left;
	pt.y -= rtWindow.top;


	CPoint ptImage;
	ptImage = GetPointFromMouse(pt);

	if( zDelta > 0)
	{
		if(  pAdjustImg->GetMultiple() <= 0.2)
			return TRUE;
		pAdjustImg->ImageZoom(ptImage, TRUE);
	}
	else
		pAdjustImg->ImageZoom(ptImage, FALSE);

	return TRUE;
#else
	if ( m_pObjectFrame == NULL )
		return FALSE;

	//-- Check Zoom Option
	if(!m_pEffectEditor)
		return FALSE;
	if(!m_pEffectEditor->m_dlgSpotInfo.m_bZoom)
		return FALSE;

	switch(nFlags)
	{
		// WEIGHT
	case MK_CONTROL:	
		{
			float fMove = (float)zDelta/(float)WHEEL_DELTA;
			float fWeight = m_pEffectEditor->m_dlgSpotInfo.m_fZoomWeight;

			if(fMove > 0)
			{
				if(fWeight>=1)	fMove /= (float)2.0;
				else			fMove /= (float)10.0;
			}
			else
			{
				if(fWeight>1)	fMove /= (float)2.0;
				else			fMove /= (float)10.0;
			}


			fWeight	+= fMove;
			if(fWeight < EFFECT_ZOOM_WEIGHT_MIN) fWeight= EFFECT_ZOOM_WEIGHT_MIN;
			if(fWeight > EFFECT_ZOOM_WEIGHT_MAX) fWeight= EFFECT_ZOOM_WEIGHT_MAX;
			//-- Update Info to Info Dialog
			m_pEffectEditor->m_dlgSpotInfo.UpdateZoomWeight(fWeight);
		}
		break;
		// ZOOM
	default:		
		{
			int nRatio = m_pEffectEditor->m_dlgSpotInfo.m_nZoomRatio - (zDelta/WHEEL_DELTA*RATIO_WHEEL_UNIT);
			if(nRatio < EFFECT_ZOOM_RATIO_MIN) nRatio = EFFECT_ZOOM_RATIO_MIN;
			if(nRatio > EFFECT_ZOOM_RATIO_MAX) nRatio = EFFECT_ZOOM_RATIO_MAX;
			//-- 2014-07-21 hongsu@esmlab.com
			//-- Check Ratio & Position
			SetZommRatio(nRatio);
			//-- Update Info to Info Dialog
			m_pEffectEditor->m_dlgSpotInfo.UpdateZoomRatio(nRatio);


		}
		break;
	}	

	//-- Redraw 
	Invalidate(FALSE); 
	return TRUE;
#endif

	
}