////////////////////////////////////////////////////////////////////////////////
//
//	ESMFrameViewBase.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-18
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "cv.h"

class CESMObjectFrame;
// CESMFrameNailDlg dialog
class CESMFrameViewBase : public CDialogEx
{
	DECLARE_DYNAMIC(CESMFrameViewBase)
public:
	CESMFrameViewBase(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMFrameViewBase();

	// Dialog Data
	//{{AFX_DATA(CESMFrameViewBase)
	enum { IDD = IDD_VIEW_EMPTY };
	//}}AFX_DATA

public:
	CESMObjectFrame* m_pObjectFrame;
	void SetObjectFrame(CESMObjectFrame* pObjFrm);

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	CFont* m_pFont;
	void SetFrameFont(CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);	

public:
	virtual void UpdateFrame() {};
	virtual void DrawFrame(CESMObjectFrame* pObjFrm);	
	virtual void LoadFrame(CDC* pDC) {};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	DECLARE_MESSAGE_MAP()
};