////////////////////////////////////////////////////////////////////////////////
//
//	DSCViewerAdjust.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCListViewer.h"
#include "DSCViewer.h"
#include "SdiSingleMgr.h"
#include "ESMFileOperation.h"
#include "ESMDefine.h"
#include "ESMImgMgr.h"
#include "ESMIni.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


void CDSCViewer::SetDscAdjustData(vector<DscAdjustInfo*>* pArrDscInfo)
{
	m_MarginX = 0;
	m_MarginY = 0;
	DscAdjustInfo* pAdjustinfo;
	CDSCItem* pItem = NULL;
	int nAll = GetItemCount();
	CvPoint2D32f ptBase;
	CvPoint2D32f ptDetect;
	memset(m_nAdjPosition, 0, sizeof(float) * DSC_ADJ_VAL_CNT);

	//wgkim 190527
	CESMImgMgr* pImgMgr = new CESMImgMgr();
	for( int i= 0;i < pArrDscInfo->size(); i++)
	{
		pAdjustinfo = pArrDscInfo->at(i);
		for(int j = 0 ; j < nAll ; j ++)
		{
			pItem = GetItemData(j);
			if(pItem->GetDeviceDSCID() == pAdjustinfo->strDscName)
			{
				pItem->SetDSCAdj(DSC_ADJ_X, pAdjustinfo->dAdjustX);
				pItem->SetDSCAdj(DSC_ADJ_Y, pAdjustinfo->dAdjustY);
				pItem->SetDSCAdj(DSC_ADJ_A, pAdjustinfo->dAngle);
				pItem->SetDSCAdj(DSC_ADJ_RX, pAdjustinfo->dRotateX);
				pItem->SetDSCAdj(DSC_ADJ_RY, pAdjustinfo->dRotateY);
				pItem->SetDSCAdj(DSC_ADJ_SCALE, pAdjustinfo->dScale);
				pItem->SetDSCAdj(DSC_ADJ_DISTANCE, pAdjustinfo->dDistance);

				ptBase.x = pAdjustinfo->dRotateX + pAdjustinfo->dAdjustX;
				ptBase.y = pAdjustinfo->dRotateY + pAdjustinfo->dAdjustY;
				ptDetect.x = pAdjustinfo->dRotateX;
				ptDetect.y = pAdjustinfo->dRotateY;
				//if( pAdjustinfo->dRotateX > 1770 && pAdjustinfo->dRotateX < 2070)
				//{
				//	pItem->SetWidth(3840);
				//	pItem->SetHeight(2160);
				//}
				//else
				//{
				//	pItem->SetWidth(1920);
				//	pItem->SetHeight(1080);
				//}

				pItem->SetWidth(pAdjustinfo->nWidht);
				pItem->SetHeight(pAdjustinfo->nHeight);
				SetAdjustPosition(pItem, ptBase, ptDetect);

				stAdjustInfo adjInfo;
				adjInfo.AdjAngle = pItem->GetDSCAdj(DSC_ADJ_A);
				adjInfo.AdjSize = pItem->GetDSCAdj(DSC_ADJ_SCALE);
				adjInfo.AdjptRotate.x = pItem->GetDSCAdj(DSC_ADJ_RX);
				adjInfo.AdjptRotate.y = pItem->GetDSCAdj(DSC_ADJ_RY);
				adjInfo.AdjMove.x = pItem->GetDSCAdj(DSC_ADJ_X);
				adjInfo.AdjMove.y = pItem->GetDSCAdj(DSC_ADJ_Y);
				adjInfo.strDSC = pItem->GetDeviceDSCID();
				adjInfo.nHeight = pItem->GetHeight();
				adjInfo.nWidth = pItem->GetWidth();

				pImgMgr->SetMargin(m_MarginX, m_MarginY, adjInfo);

				//wgkim 190527
				pImgMgr->SetRectMargin(adjInfo);
			}
		}
	}
	SetMaxAngle();

	//wgkim 190520
	for(int i = 0; i < pArrDscInfo->size(); i++)
	{
		pItem = GetItemData(i);
		pItem->m_rtMargin = pImgMgr->GetMinimumMargin();
	}
	delete pImgMgr;
}

void CDSCViewer::SetMargin(vector<DscAdjustInfo*>* pArrDscInfo)
{
	//m_MarginX = 0;
	//m_MarginY = 0;

	CDSCItem* pItem = NULL;
	int nAll = GetItemCount();

	stMarginInfo stMargin;
	stMargin.nMarginX = m_MarginX;
	stMargin.nMarginY = m_MarginY;

	for( int i= 0;i < nAll; i++)
	{
		pItem = GetItemData(i);

#if 1
		pItem->SetMargin(stMargin);
#else
		stAdjustInfo adjInfo;
		adjInfo.AdjAngle = pItem->GetDSCAdj(DSC_ADJ_A);
		adjInfo.AdjSize = pItem->GetDSCAdj(DSC_ADJ_SCALE);
		adjInfo.AdjptRotate.x = pItem->GetDSCAdj(DSC_ADJ_RX);
		adjInfo.AdjptRotate.y = pItem->GetDSCAdj(DSC_ADJ_RY);
		adjInfo.AdjMove.x = pItem->GetDSCAdj(DSC_ADJ_X);
		adjInfo.AdjMove.y = pItem->GetDSCAdj(DSC_ADJ_Y);
		adjInfo.strDSC = pItem->GetDeviceDSCID();
		adjInfo.nHeight = pItem->GetHeight();
		adjInfo.nWidth = pItem->GetWidth();

		CESMImgMgr* pImgMgr = new CESMImgMgr();
		pImgMgr->SetMargin(m_MarginX, m_MarginY, adjInfo);
#endif
	}
}

void CDSCViewer::ReSetAdjustPosition()
{
	//-- For Adjust
	for(int i = 0 ; i < DSC_ADJ_VAL_CNT ; i ++)
		m_nAdjPosition[i] = 0;
}

void CDSCViewer::OpenAdjustPage()
{
	if(m_pListView->SetShowPage(IDC_BTN_FOLDER_ADJ, TRUE))
		ReDraw();
}

void CDSCViewer::SetMaxAngle()
{
	float nAngle;
	CDSCItem* pItem = NULL;
	int nAll = GetItemCount();

	m_nAdjPosition[DSC_ADJ_VAL_MAXANGLE] = 0;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = GetItemData(i);
		nAngle = pItem->GetDSCAdj(DSC_ADJ_A);
		//-- Save Max angle value
		if(abs(m_nAdjPosition[DSC_ADJ_VAL_MAXANGLE]) < abs(nAngle))
			m_nAdjPosition[DSC_ADJ_VAL_MAXANGLE] = nAngle;
	}
}

void CDSCViewer::SetAdjustPosition(CDSCItem* pItem, CvPoint2D32f ptBase, CvPoint2D32f ptDetect)
{
	//-- Set Max, Min Range
	CString strVal;
	double nAdjX, nAdjY;
	nAdjX = ptBase.x - ptDetect.x;
	nAdjY = ptBase.y - ptDetect.y;

	if(fabs(nAdjX) > fabs(m_nAdjPosition[DSC_ADJ_VAL_LEFT	]))	m_nAdjPosition[DSC_ADJ_VAL_LEFT		]= fabs(nAdjX) +1;
	//if(fabs(nAdjX) < fabs(m_nAdjPosition[DSC_ADJ_VAL_RIGHT	]))	m_nAdjPosition[DSC_ADJ_VAL_RIGHT	]= fabs(nAdjX);
	if(fabs(nAdjY) > fabs(m_nAdjPosition[DSC_ADJ_VAL_TOP	]))	m_nAdjPosition[DSC_ADJ_VAL_TOP		]= fabs(nAdjY) +20;
	//if(fabs(nAdjY) < fabs(m_nAdjPosition[DSC_ADJ_VAL_BOTTOM]))	m_nAdjPosition[DSC_ADJ_VAL_BOTTOM	]= fabs(nAdjY);

	//-- X
	strVal.Format(_T("%f"),nAdjX);
	pItem->SetDSCAdj(DSC_ADJ_X,nAdjX);
	pItem->SetDSCAdj(DSC_ADJ_Y,nAdjY);
}