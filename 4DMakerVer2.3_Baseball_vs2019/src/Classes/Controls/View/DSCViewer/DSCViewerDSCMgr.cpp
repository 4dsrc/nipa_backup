////////////////////////////////////////////////////////////////////////////////
//
//	DSCViewerDSCMgr.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MainFrm.h"
#include "SRSIndex.h"
#include "DSCListViewer.h"
#include "DSCViewer.h"
#include "DSCMgr.h"
#include "SdiSingleMgr.h"
#include "DSCGroup.h"
#include "ESMPropertyCtrl.h"
#include "ESMIni.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCViewer::AddDSC(CString strUSBID)
{
	CDSCItem* pDSC = NULL;
	//-- 2013-09-10 hongsu@esmlab.com
	//-- Check Exist ID
	CString strDSCID = strUSBID;
	//strDSCID = strDSCID.SpanIncluding (_T("0123456789"));		// 숫자로 된 ID만 가져오도록 하는부분..
	if(5 > strDSCID.GetLength())
		return;

	if(IsExist(strUSBID))
		return;

	//-- 2013-09-23 hongsu@esmlab.com
	//-- Sort After Create SdiManager
	pDSC = new CDSCItem(strUSBID,(int)m_bRCMode);
	pDSC->SetIndex(m_arDSCItem.GetSize());
	m_arDSCItem.Add((CObject*)pDSC);
	//-- 2013-09-23 hongsu@esmlab.com
	//-- Add DSC 
	CDSCGroup* pGroup = GetGroupData(0);
	if(!pGroup)
	{
		pGroup = new CDSCGroup(0);
		m_arDSCGroup.Add((CObject*)pGroup);
	}
	pGroup->AddDSC(pDSC);
	//-- 2013-09-10 hongsu@esmlab.com

	SortDSC();	
}

BOOL CDSCViewer::IsExist(CString strUSBID)
{
	CDSCItem* pDSC = NULL;
	int nAll = GetItemCount();
	while(nAll--)
	{
		pDSC = GetItemData(nAll);

		if(pDSC->GetDSCInfo(DSC_INFO_ID) == strUSBID)
			return TRUE;

		if(pDSC->m_pSdi)
		{
			if(pDSC->m_pSdi->GetDeviceVendor()== strUSBID)
				return TRUE;
		}
	}
	return FALSE;
}

void CDSCViewer::CheckDisconnet(CStringArray *pArList)
{
	int nAll = GetItemCount();
	if(!nAll)
		return;

	ESMLog(5, _T("CheckDisconnet GetItemCount[%d]"), nAll);

	CString srtVendor;
	CDSCItem * pItem = NULL;

	while(nAll--)
	{
		BOOL bExist = FALSE;
		pItem = GetItemData(nAll);

		if(!pItem)
			continue;

		if(pItem->GetType() == DSC_REMOTE || pItem->GetType() == DSC_UNPREPARED)
			continue;

		if(!pItem->m_pSdi)
			continue;

		if( pItem->m_pSdi == NULL)
			continue;

		srtVendor = pItem->m_pSdi->GetDeviceVendor();

		//-- 2013-02-12 hongsu.jung
		//-- Is Exist
		int nConnectAll = (int)pArList->GetCount();

		while(nConnectAll--)
		{
			if(srtVendor == pArList->GetAt(nConnectAll))
				bExist = TRUE;
		}

		//-- 2013-09-11 hongsu@esmlab.com
		//-- Change Status "Disconnect" 
		if(!bExist)
		{
			//CString strTp;
			//strTp.Format(_T("DSCID[%s] DisConnect!!!!!!"), pItem->GetDeviceDSCID());
			//AfxMessageBox(strTp);

			ESMLog(5, _T("##################Disconnect [%s]"), pItem->GetDeviceDSCID());
			pItem->SetDSCStatus(SDI_STATUS_DISCONNECT);
			pItem->DeleteSdiMgr();
		}
		else
		{
			ESMLog(5,_T("(CAMREVISION)Change Status"));
			if(pItem->GetDSCRecStatus() != DSC_REC_ON)
				pItem->SetDSCStatus(SDI_STATUS_CONNECTED);

			//jhhan 170926
			if(ESMGetGPUMakeFile())
			{
				if(ESMGetValue(ESM_VALUE_REFEREEREAD) == FALSE)
					ESMSet4DPMgr(pItem->GetDeviceDSCID());
			}
		}

		pItem = NULL;
	}
}
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-04-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @revision		
//------------------------------------------------------------------------------
void CDSCViewer::AddDSC(CDSCItem* pDSC)
{
	if(!pDSC)
	{
		return;
	}

	//-- 2013-09-10 hongsu@esmlab.com
	//-- Check Exist ID
	if(IsExist(pDSC->GetDeviceDSCID()))
	{
		////AddDSC
		//ESMLog(5, _T("4DA : %s"),pDSC->GetDeviceDSCID());
		//ESMSet4DAFileInit(pDSC->GetDeviceDSCID());


		CDSCItem* pItem;
		pItem = GetItemData(pDSC->GetDeviceDSCID());
		pItem->SetDSCInfo(DSC_INFO_MODEL	 , pDSC->GetDSCInfo(DSC_INFO_MODEL));
		pItem->SetDSCInfo(DSC_INFO_UNIQUEID, pDSC->GetDSCInfo(DSC_INFO_UNIQUEID));
		pItem->SetDSCInfo(DSC_INFO_LOCATION, pDSC->GetDSCInfo(DSC_INFO_LOCATION));
		pItem->SetType(pDSC->GetType());
		pItem->m_nRemoteID = pDSC->m_nRemoteID;
		pItem->SetDSCStatus(pDSC->m_nStatus);

		//pItem->m_bReverse = pDSC->m_bReverse;
		//pItem->m_bDelete  = pDSC->m_bDelete;
		//pItem->SetIndex(m_arDSCItem.GetSize());
		if(pDSC)
		{
			delete pDSC;
			pDSC = NULL;
		}
		return;
	}

	m_arDSCItem.Add((CObject*)pDSC);

	//-- 2013-09-23 hongsu@esmlab.com
	//-- Add DSC 
	CDSCGroup* pGroup = GetGroupData(pDSC->m_nRemoteID);
	
	if(!pGroup)
	{
		pGroup = new CDSCGroup(pDSC->m_nRemoteID);
		pGroup->m_strIP = pDSC->m_strIP;
		m_arDSCGroup.Add((CObject*)pGroup);
		//-- Find Network and Connect Pointer
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_VIEW_SYNC_GROUP;
		pMsg->pParam	= (LPARAM)pGroup;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, WM_ESM_VIEW, (LPARAM)pMsg);
	}
	else
		pGroup->m_strIP = pDSC->m_strIP;

	pGroup->AddDSC(pDSC);

	//-- 2013-09-10 hongsu@esmlab.com
	//-- Redraw
	ReDraw();
}

int CDSCViewer::GetItemCount()
{
	return m_arDSCItem.GetCount();
}

CDSCItem* CDSCViewer::GetItemData(int nIndex)
{
	if(GetItemCount() < 0 || nIndex >= GetItemCount())
		return NULL;
		
	return (CDSCItem*)m_arDSCItem.GetAt(nIndex);
}

int CDSCViewer::GetItemIndex(CString strDSC)
{
	CDSCItem* pExist = NULL;
	int nAll = GetItemCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExist = GetItemData(i);
		if(pExist->GetDeviceDSCID() == strDSC)
			return i;
	}
	return -1;
}

CDSCItem* CDSCViewer::GetItemData(CString strDSC, BOOL bUUID)
{
	CDSCItem* pExist = NULL;
	int nAll = GetItemCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExist = GetItemData(i);
		if (bUUID)
		{
			if(pExist->GetDeviceUniqueID() == strDSC)
				return pExist;
		}
		else
		{
			if(pExist->GetDeviceDSCID() == strDSC)
				return pExist;
		}
		
	}
	return NULL;
}

void CDSCViewer::GetDSCList(CObArray* pAr)
{
	pAr->RemoveAll();
	CDSCItem* pExist = NULL;
	int nAll = GetItemCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExist = GetItemData(i);
		if(pExist)
			pAr->Add((CObject*)pExist);
	}
}

void CDSCViewer::RemoveListAll()
{
	int nAll = 0;
	CDSCItem* pItem = NULL;

	//jhhan 170410 쓰레드 종료 추가
	/*nAll = GetGroupCount();
	CDSCGroup* pGroupTrd = NULL;
	while(nAll--)
	{
		pGroupTrd = GetGroup(nAll);
		pGroupTrd->m_bThread = FALSE;
		pGroupTrd->WaitThreadEnd();
	}
*/

	nAll = GetItemCount();
	while(nAll--)
	{
		pItem = GetItemData(nAll);
		if(pItem)
		{
			delete pItem;
			pItem = NULL;
		}

		m_arDSCItem.RemoveAt(nAll);
	}
	m_arDSCItem.RemoveAll();

	//-- 2013-12-14 kcd
	//-- Delete Group
	nAll = GetGroupCount();
	CDSCGroup* pGroup = NULL;
	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		//jhhan 170410 쓰레드 종료 추가
		pGroup->m_bThread = FALSE;
		if(pGroup)
		{
			delete pGroup;
			pGroup = NULL;
		}
		m_arDSCGroup.RemoveAt(nAll);
	}
	m_arDSCGroup.RemoveAll();

	//jhhan 16-09-19
	ESMLog(5, _T("DSC-RemoveListAll"));
}

void CDSCViewer::DeleteItem(CString strDSC)
{
	int nAll = GetItemCount();
	CDSCItem* pDelItem = NULL;

	while(nAll--)
	{
		pDelItem = GetItemData(nAll);
		if(pDelItem->GetDSCInfo(DSC_INFO_ID) == strDSC)
		{
			if(pDelItem)
			{
				delete pDelItem;
				pDelItem = NULL;
			}

			m_arDSCItem.RemoveAt(nAll);
			break;
		}
	}
}

//------------------------------------------------------------
//-- Control DSC Group
//------------------------------------------------------------
int CDSCViewer::GetGroupCount()
{
	return m_arDSCGroup.GetCount();
}

CDSCGroup* CDSCViewer::GetGroup(int nIndex)
{
	return (CDSCGroup*)m_arDSCGroup.GetAt(nIndex);
}

CDSCGroup* CDSCViewer::GetGroupData(int nIP)
{
	int nAll = GetGroupCount();
	CDSCGroup* pExist = NULL;
	while(nAll--)
	{
		pExist = GetGroup(nAll);
		if(pExist->m_nIP == nIP)
			return pExist;
	}
	return NULL;
}

CDSCGroup* CDSCViewer::GetDSCBaseGroup()
{ 
  	if( GetGroupCount() <= 0)
  	 	return NULL;

	return GetGroup(0);
}

int CDSCViewer::GetMarginX()
{
	return m_MarginX;
}

int CDSCViewer::GetMarginY()
{
	return m_MarginY;
}

void CDSCViewer::SetMarginX(int nMarginX)
{
	m_MarginX = nMarginX;
}

void CDSCViewer::SetMarginY(int nMarginY)
{
	m_MarginY = nMarginY;
}

void CDSCViewer::SetMargin(CRect rtMarginRect)
{
	m_rtMarginRect = rtMarginRect;
}


//------------------------------------------------------------------------------
//! @brief		Clear Time Line Editor
//! @date		2013-10-20
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
void CDSCViewer::RemoveTimeLineEditor()
{
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_VIEW_TIMELINE_RESET;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, WM_ESM_VIEW, (LPARAM)pMsg);
}

void CDSCViewer::SetFocusMode(BOOL bAF)
{
	ESMEvent* pMsg = NULL;
	CString strAF;

	pMsg = new ESMEvent();
	pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = PTP_CODE_FOCUSMODE;
	pMsg->nParam2 = PTP_VALUE_UINT_16;

	if(bAF)		
	{
		pMsg->pParam = (LPARAM) eUCS_CAP_AF_MODE_CAF;
		strAF.Format(_T("CAF"));
	}
	else
	{
		pMsg->pParam = (LPARAM) eUCS_CAP_AF_MODE_MF;	
		strAF.Format(_T("MF"));
	}

	//-- 2013-10-15 hongsu@esmlab.com
	SetGroupProperty(pMsg);
	ESMLog(5,_T("Change Focus Mode [%s]"),strAF);
}

void CDSCViewer::SetFullHDMode()
{
	ESMLog(5,_T("Change Size 1920X1080"));

	int nAll = GetItemCount();
	CDSCItem* pItem = NULL;
	ESMEvent* pMsg = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = GetItemData(i);
		if(!pItem)
			continue;

		pMsg = new ESMEvent;
		pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
		pMsg->nParam1 = PTP_CODE_IMAGESIZE;
		pMsg->nParam2 = PTP_VALUE_STRING;
		pMsg->pParam = (LPARAM) eUCS_CAP_PHOTO_SIZE_2M_WIDE;
		pMsg->pDest = (LPARAM)pItem;
		m_pDSCMgr->AddMsg(pMsg);

	}
}	

void CDSCViewer::SetGroupProperty(ESMEvent* pMsg)
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 12"));
		return;
	}

	CDSCGroup* pGroup = NULL;
	ESMEvent* pCopyMsg = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;

		pCopyMsg = new ESMEvent;
		memcpy(pCopyMsg, pMsg, sizeof(ESMEvent));
		pCopyMsg->message	= WM_SDI_OP_SET_PROPERTY_VALUE;
		pGroup->SetGroupAllProperty(pCopyMsg);
	}
}

void CDSCViewer::SetGroupHiddenCommand(ESMEvent* pMsg)
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 12"));
		return;
	}

	CDSCGroup* pGroup = NULL;
	ESMEvent* pCopyMsg = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;

		pCopyMsg = new ESMEvent;
		memcpy(pCopyMsg, pMsg, sizeof(ESMEvent));
		pCopyMsg->message	= WM_SDI_OP_HIDDEN_COMMAND;
		if( pCopyMsg->nParam1 = HIDDEN_COMMAND_CHANGE_MODE) 
			pCopyMsg->pParam = pCopyMsg->nParam2;
		pGroup->SetGroupAllHiddenCommand(pCopyMsg);
	}
}

void CDSCViewer::SetHiddenCommandAll(ESMEvent* pMsg)
{


	CDSCItem* pDSCitem = NULL;
	for(int nIndex =0 ; nIndex < GetItemCount(); nIndex++)
	{
		ESMEvent* pSdiMsg = new ESMEvent;
		pSdiMsg->message = pMsg->message;
		pSdiMsg->nParam1 = pMsg->nParam1;

		pDSCitem = GetItemData(nIndex);
		if(!pDSCitem)
			continue;
		pDSCitem->SdiAddMsg(pSdiMsg);

	}

	return ;
}

void CDSCViewer::FormatDeviceAll()
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 13"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;
		pGroup->FormatDeviceGroup();
	}
}

//-- 2014-9-4 hongsu@esmlab.com
//-- F/W Update
void CDSCViewer::FWUpdateAll()
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 13"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;
		pGroup->FWUpdateGroup();
	}
}

void CDSCViewer::MovieMakingStop()
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 13"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;
		pGroup->MovieMakingStopGroup();
	}
}

void CDSCViewer::CaptureResume(int nResumeFrame)
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 13"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;

		pGroup->CaptureResumeGroup(nResumeFrame);
	}
}

void CDSCViewer::CamShutDownAll()
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 13"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;
		pGroup->CamShutDownGroup();
	}
}

void CDSCViewer::InitMovieFile()
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 13"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;
		pGroup->InitMovieGroup();
	}
}

void CDSCViewer::ViewLarge(int nLarge)
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 13"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;
		pGroup->VIewLargeGroup(nLarge);
	}
}

int CDSCViewer::GetConnectStatus(int nRemoteId)
{
	CDSCGroup* pGroup = GetGroupData(nRemoteId);
	int nCamCount = pGroup->GetCount();
	CDSCItem* pDSCitem;
	int nStatus = -1;
	int nAllStatus = -1;
	int nIndex =0;
	for( nIndex =0 ; nIndex < nCamCount; nIndex++)
	{
		pDSCitem = pGroup->GetDSC(nIndex);
		nStatus = pDSCitem->GetDSCStatus();
		if( nStatus != SDI_STATUS_CONNECTED )
			break;
	}
	if( nIndex == nCamCount)
		nAllStatus = 1;
	else
		nAllStatus = -1;

	return nAllStatus;
}

//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
void CDSCViewer::GetFocusAll(BOOL bSave)
{
	m_bGetFocusState = TRUE;
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 14"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;

		//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
		pGroup->GetFocusGroup(bSave);
	}
}


void CDSCViewer::SetFocusAll()
{
	m_bGetFocusState = FALSE;
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 14"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;

		pGroup->SetFocusGroup();
	}
}
void CDSCViewer::SetFocus(CString strDSCId, int nSetFocus)
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 14"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;

		pGroup->SetFocus(strDSCId, nSetFocus);
	}
}

void CDSCViewer::SetFocus(CString strDSCId, int MinFocus, int nSetFocus)
{
 	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 14"));
		return;
	}
	FocusData* pFocusData = new FocusData;
	TCHAR* strIdBuf = new TCHAR[strDSCId.GetLength() + 1];
	_tcscpy(strIdBuf, strDSCId.GetBuffer(0));
	strDSCId.ReleaseBuffer();
	pFocusData->strId = strIdBuf;
	pFocusData->nMinFocus = MinFocus;
	pFocusData->nSetFocus = nSetFocus;
	pFocusData->pDSCViewer = this;
	HANDLE hHandle = NULL;
	hHandle = (HANDLE)_beginthreadex(NULL, 0, SetFocusThread, (void *)pFocusData, 0, NULL);
	CloseHandle(hHandle);
}

unsigned WINAPI CDSCViewer::SetFocusThread(LPVOID param)
{
	FocusData* pFocusData = (FocusData*)param;
	CDSCViewer* pDscViewer = pFocusData->pDSCViewer;
	int nMinFocus = pFocusData->nMinFocus;
	int nSetFocus = pFocusData->nSetFocus;
	TCHAR* strId = pFocusData->strId;


	CDSCGroup* pGroup = NULL;
	int nAll = pDscViewer->GetGroupCount();	
	//pDscViewer->SetFocusMode(FALSE);
	Sleep(200);
	while(nAll--)
	{
		pGroup = pDscViewer->GetGroup(nAll);
		if(!pGroup)
			continue;

		pGroup->SetFocus(strId, nMinFocus);
	}
	Sleep(2000);
	nAll = pDscViewer->GetGroupCount();	
	while(nAll--)
	{
		pGroup = pDscViewer->GetGroup(nAll);
		if(!pGroup)
			continue;

		pGroup->SetFocus(strId, nSetFocus);
	}

	Sleep(500);
// 	nAll = pDscViewer->GetGroupCount();	
// 	while(nAll--)
// 	{
// 		pGroup = pDscViewer->GetGroup(nAll);
// 		if(!pGroup)
// 			continue;
// 
// 		pGroup->SetFocus(strId, nSetFocus);
// 	}

	if( strId )
	{
		delete strId;
		strId = NULL;
	}
	if( pFocusData )
	{
		delete pFocusData;
		pFocusData = NULL;
	}
	return 0;
}

void CDSCViewer::SetFocusLoadFile(CString strPath)
{
	int nAll = GetItemCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 23"));
		return;
	}

	int nCurFocus=0, nMinFocus=0;
	CString strEntry, strSection;
	CESMIni ini;
	
	if(!ini.SetIniFilename (strPath))
		return;

	CDSCItem* pItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSCItem.GetAt(i);
		if(!pItem)
			continue;

		strEntry.Format(_T("%s"), pItem->GetDSCInfo(DSC_INFO_ID));
		nCurFocus = ini.GetInt(INFO_SECTION_FOCUS, strEntry, 0);
		nMinFocus	= ini.GetInt(INFO_SECTION_FOCUS, strEntry + _T("_MIN"), 0);

		if(nCurFocus == 0 || nMinFocus == 0)
			continue;

		ESMEvent* pMsg = new ESMEvent();
		pMsg->message = WM_SDI_OP_SET_FOCUS;
		pMsg->nParam1 = nCurFocus;
		pItem->SdiAddMsg(pMsg);
		Sleep(100);
	}
	/*
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 22"));
		return;
	}

	CDSCGroup* pGroup = NULL;
	
	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;

		pGroup->SetFocusLoadFile(strPath);
	}
	*/
}

void CDSCViewer::GetPropertyAll(int nProp, CString strDscID)
{
	ESMEvent* pMsg = NULL;

	int nAll = GetItemCount();
	CDSCItem* pItem = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = GetItemData(i);
		if(!pItem)
			continue;

		if (strDscID.IsEmpty())
		{
			pMsg = new ESMEvent;
			pMsg->message = WM_SDI_OP_GET_PROPERTY_DESC;
			pMsg->nParam1 = nProp;
			pMsg->pDest = (LPARAM)pItem;
			pItem->SdiAddMsg(pMsg);
		}
		else
		{
			if (pItem->GetDeviceDSCID() == strDscID)
			{
				pMsg = new ESMEvent;
				pMsg->message = WM_SDI_OP_GET_PROPERTY_DESC;
				pMsg->nParam1 = nProp;
				pMsg->pDest = (LPARAM)pItem;
				pItem->SdiAddMsg(pMsg);
				break;
			}
		}		
	}
}

void CDSCViewer::GetPropertyListView(CString strDscID)
{
	ESMSetPropertyCheck(TRUE);

	ESMPropertyDiffCheck();
	ESMPropertyMinMaxInfoCheck();

	for(int i=0; i< DSC_PROP_CNT; i++)
	{
		m_strCompare[i] = _T("");
	}

	//GetPropertyAll(PTP_CODE_FUNCTIONALMODE);
	//GetPropertyAll(PTP_CODE_SAMSUNG_ZOOM_CTRL_GET);
	GetPropertyAll(PTP_CODE_F_NUMBER, strDscID);
	GetPropertyAll(PTP_DPC_SET_SYSTEM_FREQUENCY, strDscID);
	GetPropertyAll(PTP_CODE_SAMSUNG_SHUTTERSPEED, strDscID);
	GetPropertyAll(PTP_CODE_EXPOSUREINDEX, strDscID);
	GetPropertyAll(PTP_CODE_SAMSUNG_MOV_SIZE, strDscID);
	
	//jhhan 16-09-09
	GetPropertyAll(PTP_CODE_WHITEBALANCE, strDscID);
	GetPropertyAll(PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN, strDscID);
	GetPropertyAll(PTP_DPC_WB_DETAIL_KELVIN, strDscID);
	GetPropertyAll(PTP_CODE_SAMSUNG_PICTUREWIZARD, strDscID);

	GetPropertyAll(PTP_DPC_PHOTO_STYLE, strDscID);		// GH5 picture wizard
	
	if (strDscID.IsEmpty())
		ESMLog(5,_T("Get Property Info ListView"));
	else
		ESMLog(5,_T("Get Property Info ListView ID: %s"), strDscID);
	
	
	//jhhan 16-09-08
	/*ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_VIEW_REDRAW;	
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);*/
}

void CDSCViewer::SetPropertyListView(ESMEvent* pMsg)
{
	if(!pMsg)
		return;

	CString strDSCID;
	int nType = 0;
	int nValue = 0;
		
	PropertyDiffCount stDiffValues = ESMGetPropertyDiffCount();
	PropertyMinMaxInfo stMinMaxInfoValues = ESMGetPropertyMinMaxInfo();

	if(pMsg->message == WM_SDI_RESULT_GET_PROPERTY_DESC)
	{
		IDeviceInfo *pDeviceInfo = (IDeviceInfo *)pMsg->pParam;
		if(!pDeviceInfo)
			return;
		CSdiSingleMgr* pSdiMgr = (CSdiSingleMgr*)pMsg->pDest;
		strDSCID = pSdiMgr->GetDeviceDSCID();
		nType  = pDeviceInfo->GetPropCode();
		nValue = pDeviceInfo->GetCurrentEnum();
		
	}
	else if(pMsg->message == WM_ESM_LIST_PROPERTY_RESULT)
	{
		CString* pDSCID;
		pDSCID = (CString*)pMsg->pDest;
		strDSCID = pDSCID->GetString();
		nType  = (int)pMsg->nParam1;
		nValue = (int)pMsg->nParam2;
	}
	else
	{
		return;
	}

	 
	//-- Check Property DSC
	if(!strDSCID.GetString())
		return;

	CDSCItem* pItem = NULL;
	pItem = GetItemData(strDSCID);

	if(!pItem)
		return;

	PropertyDiffCheckedID stCheckedID;
	if (stDiffValues.mapCheckedID)
		stDiffValues.mapCheckedID->Lookup(strDSCID, stCheckedID);

	//jhhan 16-09-09
#if _ORIGINAL
	switch(nType)
	{
	case PTP_CODE_SAMSUNG_ZOOM_CTRL_GET		: pItem->SetDSCProp(DSC_PROP_ZOOM,GetPropertyValuetoStr(nType, nValue&0x00FF, &pItem->GetDSCInfo(DSC_INFO_MODEL)));	break;
	//case PTP_CODE_FUNCTIONALMODE			: pItem->SetDSCProp(DSC_PROP_FUNC,GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL)));		break;
	case PTP_CODE_SAMSUNG_SHUTTERSPEED		: pItem->SetDSCProp(DSC_PROP_SS,  GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL)));		break;
	case PTP_CODE_F_NUMBER					: pItem->SetDSCProp(DSC_PROP_FNUM,GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL)));		break;
	case PTP_CODE_EXPOSUREINDEX				: pItem->SetDSCProp(DSC_PROP_ISO, GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL)));		break;
	case PTP_CODE_SAMSUNG_MOV_SIZE			: pItem->SetDSCProp(DSC_PROP_MOVIE_SIZE, GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL)));		break;
	}
#else
	CString strValue = _T("");

	switch(nType)
	{
	//case PTP_CODE_SAMSUNG_ZOOM_CTRL_GET		: //jhhan 16-09-09
	//	strValue = GetPropertyValuetoStr(nType, nValue&0x00FF, &pItem->GetDSCInfo(DSC_INFO_MODEL));
	//	if(!ESMGetPropertyValue(DSC_PROP_ZOOM).IsEmpty())
	//	{
	//		if(strValue.Compare(ESMGetPropertyValue(DSC_PROP_ZOOM)) != 0)
	//		{
	//			//색상변경
	//			ESMLog(5, _T("[DSC : %s] Warning!! Check Zoom Value : %s"), pItem->GetDeviceDSCID(), strValue);
	//			strValue.Insert(0, _T("*** "));
	//		}
	//	}
	//	pItem->SetDSCProp(DSC_PROP_ZOOM, strValue);	
	//	break;
	////case PTP_CODE_FUNCTIONALMODE			: pItem->SetDSCProp(DSC_PROP_FUNC,GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL)));		break;
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM1:
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM2:
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM3:
		{
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_GET_PICTURE_WIZARD_DETAIL;
			pMsg->nParam1	= nType;
			pMsg->nParam2	= nValue;
			if( ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
			{
				::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, WM_ESM_VIEW, (LPARAM)pMsg);
			}
		}
		break;
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM1_ETC:
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM2_ETC:
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM3_ETC:
		{
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_GET_PICTURE_WIZARD_DETAIL_ETC;
			pMsg->nParam1	= nType;
			pMsg->nParam2	= nValue;
			if( ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
			{
				::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, WM_ESM_VIEW, (LPARAM)pMsg);
			}
		}
		break;
	case PTP_CODE_SAMSUNG_SHUTTERSPEED		:
		strValue = GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL));
		if(!ESMGetPropertyValue(DSC_PROP_SS).IsEmpty())
		{
			if(strValue.Compare(ESMGetPropertyValue(DSC_PROP_SS)) != 0)
			{				
				//알림
				//ESMLog(5, _T("[DSC : %s] Warning!! Check Shutter Value : %s"), pItem->GetDeviceDSCID(), strValue);
				strValue.Insert(0, _T("*** "));
				ESMSetPropertyCheck(FALSE);				

				if (!stCheckedID.bShutterSpeed)
					stDiffValues.nShutterSpeed++;					

				stCheckedID.bShutterSpeed = true;					
			}
		}
		else
		{
			ESMSetPropertyValue(DSC_PROP_SS, strValue);
		}
		pItem->SetDSCProp(DSC_PROP_SS,  strValue);		
		break;
	case PTP_CODE_SAMSUNG_PICTUREWIZARD		:
		strValue = GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL));
		if(!ESMGetPropertyValue(DSC_PROP_PW).IsEmpty())
		{
			if(strValue.Compare(ESMGetPropertyValue(DSC_PROP_PW)) != 0)
			{
				//알림
				//ESMLog(5, _T("[DSC : %s] Warning!! Check Picture Wizard Value : %s"), pItem->GetDeviceDSCID(), strValue);
				strValue.Insert(0, _T("*** "));
				ESMSetPropertyCheck(FALSE);

				if (!stCheckedID.bPictureWizard)
					stDiffValues.nPictureWizard++;					

				stCheckedID.bPictureWizard = true;	
			}
		}
		else
		{
			ESMSetPropertyValue(DSC_PROP_PW, strValue);
		}
		pItem->SetDSCProp(DSC_PROP_PW,  strValue);		
		break;
	//case PTP_CODE_F_NUMBER					: //jhhan 16-09-09
	//	strValue = GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL));
	//	if(!ESMGetPropertyValue(DSC_PROP_FNUM).IsEmpty())
	//	{
	//		if(strValue.Compare(ESMGetPropertyValue(DSC_PROP_FNUM)) != 0)
	//		{
	//			//알림
	//			ESMLog(5, _T("[DSC : %s] Warning!! Check F# Value : %s"), pItem->GetDeviceDSCID(), strValue);
	//			strValue.Insert(0, _T("*** "));
	//		}
	//	}
	//	pItem->SetDSCProp(DSC_PROP_FNUM,strValue);		
	//	break;
	case PTP_CODE_F_NUMBER:
		{
			if (!stMinMaxInfoValues.strIDList)
				break;

			POSITION pos = stMinMaxInfoValues.strIDList->Find(strDSCID);
			if(pos == NULL)
			{
				if (nValue == 0x0000FFFE)
					stMinMaxInfoValues.nUnknown++;
				else if (nValue == 0x0000FFFF)
					stMinMaxInfoValues.nAuto++;
				else
				{
					if (stMinMaxInfoValues.nFMin == 0)
						stMinMaxInfoValues.nFMin = nValue;
					else if (stMinMaxInfoValues.nFMin >= nValue)
						stMinMaxInfoValues.nFMin = nValue;

					if (stMinMaxInfoValues.nFMax == 0)
						stMinMaxInfoValues.nFMax = nValue;
					else if (stMinMaxInfoValues.nFMax <= nValue)
						stMinMaxInfoValues.nFMax = nValue;					
				}

				stMinMaxInfoValues.strIDList->AddTail(strDSCID);

				//ESMLog(5, _T("%s , min:%d, max%d, value:%d \n"), pItem->GetDeviceDSCID(), minMaxInfoValues.nFMin, minMaxInfoValues.nFMax,nValue);
			}
		}
		break;
	case PTP_CODE_EXPOSUREINDEX				: 
		strValue = GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL));
		if(!ESMGetPropertyValue(DSC_PROP_ISO).IsEmpty())
		{
			if(strValue.Compare(ESMGetPropertyValue(DSC_PROP_ISO)) != 0)
			{
				//알림
				//ESMLog(5, _T("[DSC : %s] Warning!! Check ISO Value : %s"), pItem->GetDeviceDSCID(), strValue);
				strValue.Insert(0, _T("*** "));
				ESMSetPropertyCheck(FALSE);

				if (!stCheckedID.bIso)
					stDiffValues.nIso++;					

				stCheckedID.bIso = true;	
			}
		}
		else
		{
			ESMSetPropertyValue(DSC_PROP_ISO, strValue);
		}
		pItem->SetDSCProp(DSC_PROP_ISO, strValue);		
		break;
	case PTP_CODE_SAMSUNG_MOV_SIZE			: 
		{
			strValue = GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL));
			CString strModel = pItem->GetDeviceModel();
			CString strDscId = pItem->GetDeviceDSCID();
			CString strDefaultID = ESMGetDefaultCameraID();
			if (strDefaultID.IsEmpty())
			{
				ESMSetDefaultCameraID(strDscId);
			}

			if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
			{				
				CString strFrequency;
				strFrequency = pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_SYSTEM_FREQUENCY);

				if (strFrequency.Find(_T("PAL")) != -1)
				{
					if(strValue.Find(_T("4K/30p")) != -1)
						strValue = _T("4K/25p");
					else if(strValue.Find(_T("4K/60p")) != -1)
						strValue = _T("4K/50p");
					else if(strValue.Find(_T("FHD/30p")) != -1)
						strValue = _T("FHD/25p");
					else if(strValue.Find(_T("FHD/60p")) != -1)
						strValue = _T("FHD/50p");
				}

				//jhhan 180307
				ESMSetDevice(SDI_MODEL_GH5);

				if (ESMGetDefaultCameraID() == strDscId)	// 레코딩시 프로퍼티 기준 카메라.
				{

					//jhhan 180411 - X2 옵션 활성화
					if(strValue.Find(_T("4K/30p")) != -1)
					{
#ifdef _FILE_2SEC
						if(ESMGetFile2Sec() == FALSE)
							ESMSetFrameRate(MOVIE_FPS_UHD_30P);	
						else
							ESMSetFrameRate(MOVIE_FPS_UHD_30P_X2);	
#else 
						ESMSetFrameRate(MOVIE_FPS_UHD_30P);	
#endif
					}
					else if(strValue.Find(_T("4K/60p")) != -1)
					{
#ifdef _FILE_2SEC
						if(ESMGetFile2Sec() == FALSE)
							ESMSetFrameRate(MOVIE_FPS_UHD_60P);	
						else
							ESMSetFrameRate(MOVIE_FPS_UHD_60P_X2);	
#else
						ESMSetFrameRate(MOVIE_FPS_UHD_60P);	
#endif
					}
					else if(strValue.Find(_T("FHD/30p")) != -1)
						ESMSetFrameRate(MOVIE_FPS_FHD_30P);
					else if(strValue.Find(_T("FHD/60p")) != -1)
						ESMSetFrameRate(MOVIE_FPS_FHD_60P);
					else if(strValue.Find(_T("4K/25p")) != -1)
						ESMSetFrameRate(MOVIE_FPS_UHD_25P);
					else if(strValue.Find(_T("4K/50p")) != -1)
						ESMSetFrameRate(MOVIE_FPS_UHD_50P);
					else if(strValue.Find(_T("FHD/25p")) != -1)
						ESMSetFrameRate(MOVIE_FPS_FHD_25P);
					else if(strValue.Find(_T("FHD/50p")) != -1)
						ESMSetFrameRate(MOVIE_FPS_FHD_50P);

					ESMLog(5, _T("#######  framerate: %s[%d]"), strValue, ESMGetFrameRate());
				}
			}
			else if (strModel.CompareNoCase(_T("NX1")) == 0)
			{
				//jhhan 180307
				ESMSetDevice(SDI_MODEL_NX1);

				if (ESMGetDefaultCameraID() == strDscId)	// 레코딩시 프로퍼티 기준 카메라.
				{
					if(strValue.Find(_T("UHD 25P")) != -1)
					{
						ESMSetFrameRate(MOVIE_FPS_UHD_25P);
					}
					else if(strValue.Find(_T("FHD 50P")) != -1)
					{
						ESMSetFrameRate(MOVIE_FPS_FHD_50P);
					}
					else if(strValue.Find(_T("FHD 30P")) != -1)
					{
						ESMSetFrameRate(MOVIE_FPS_FHD_30P);
					}
					else if(strValue.Find(_T("UHD 30P")) != -1)
					{
						ESMSetFrameRate(MOVIE_FPS_UHD_30P);
					}
					else if(strValue.Find(_T("FHD 25P")) != -1)
					{
						ESMSetFrameRate(MOVIE_FPS_FHD_25P);
					}
					else if(strValue.Find(_T("FHD 60P")) != -1)
					{
						ESMSetFrameRate(MOVIE_FPS_FHD_60P);
					}
					else if(strValue.Find(_T("FHD 120P")) != -1 || strValue.Find(_T("DC  24P")) != -1)
					{
						ESMSetFrameRate(MOVIE_FPS_FHD_120P);
					}
					else
					{
						ESMSetFrameRate(MOVIE_FPS_UNKNOWN);
						ESMLog(1, _T("#### FRAMERATE : Unknown"));
					}

					ESMLog(5, _T("#######  framerate: %s[%d]"), strValue, ESMGetFrameRate());
				}
			}
			else if(strModel.CompareNoCase(_T("NX500")) == 0)
			{
				ESMSetDevice(SDI_MODEL_NX500);

#if 0
				/*
				81 = NTSC 4096X2160 24F NTSC
				82 = NTSC 3840X2160 30F
				83 = NTSC 1920X1080 60F
				84 = NTSC 1920X1080 30F
				85 = NTSC 1920X1080 24F
				86 = NTSC 1280X720 120F
				87 = NTSC 1280X720 60F
				88 = NTSC 1280X720 30F
				89 = NTSC 640X480 60F
				8A = NTSC 640X480 30F
				8B = NTSC 640X480 MPEG
				91 = PAL 4096X2160 24F
				92 = PAL 3840X2160 25F
				93 = PAL 1920X1080 50F
				94 = PAL 1920X1080 25F
				95 = PAL 1920X1080 24F
				96 = PAL 1280X720 100F
				97 = PAL 1280X720 50F
				98 = PAL 1280X720 25F
				99 = PAL 640X480 50F
				9A = PAL 640X480 25F
				9B = PAL 640X480 MPEG
				*/
#endif
				if (ESMGetDefaultCameraID() == strDscId)	// 레코딩시 프로퍼티 기준 카메라.
				{

					//if(strValue.CompareNoCase(_T("82")) == 0)
					if (nValue == 130)		// hex 82
					{
						ESMSetFrameRate(MOVIE_FPS_UHD_30P);
					}
					else if (nValue == 131)		// hex 83
					{
						ESMSetFrameRate(MOVIE_FPS_FHD_60P);
					}
					else if (nValue == 132)		// hex 84
					{
						ESMSetFrameRate(MOVIE_FPS_FHD_30P);
					}
					else if (nValue == 146)		// hex 92
					{
						ESMSetFrameRate(MOVIE_FPS_UHD_25P);
					}
					else if (nValue == 147)		// hex 93
					{
						ESMSetFrameRate(MOVIE_FPS_FHD_50P);
					}
					else if (nValue == 148)		// hex 94
					{
						ESMSetFrameRate(MOVIE_FPS_FHD_25P);
					}
					else
					{
						ESMSetFrameRate(MOVIE_FPS_UNKNOWN);
						ESMLog(1, _T("#### FRAMERATE : Unknown"));
					}
				}
			}
			else
			{
				ESMSetFrameRate(MOVIE_FPS_UNKNOWN);
				ESMLog(1, _T("#### FRAMERATE_0 : Unknown"));
			}			
		
			if(!ESMGetPropertyValue(DSC_PROP_MOVIE_SIZE).IsEmpty())
			{
				if(strValue.Compare(ESMGetPropertyValue(DSC_PROP_MOVIE_SIZE)) != 0)
				{
					//알림
					//ESMLog(5, _T("[DSC : %s] Warning!! Check Size Value : %s"), pItem->GetDeviceDSCID(), strValue);
					strValue.Insert(0, _T("*** "));
					ESMSetPropertyCheck(FALSE);

					if (!stCheckedID.bMovieSize)
						stDiffValues.nMovieSize++;					

					stCheckedID.bMovieSize = true;	
				}
			}
			else
			{
				ESMSetPropertyValue(DSC_PROP_MOVIE_SIZE, strValue);
			}
			pItem->SetDSCProp(DSC_PROP_MOVIE_SIZE, strValue);		
		}
		break;
	//jhhan 16-09-09
	case PTP_CODE_WHITEBALANCE			:
		strValue = GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL));
		if(!ESMGetPropertyValue(DSC_PROP_WB).IsEmpty())
		{
			CString strTemp = ESMGetPropertyValue(DSC_PROP_WB);
			if(strValue.Compare(ESMGetPropertyValue(DSC_PROP_WB)) != 0)
			{
				//알림
				//ESMLog(5, _T("[DSC : %s] Warning!! Check WhiteBalance Value : %s"), pItem->GetDeviceDSCID(), strValue);
				strValue.Insert(0, _T("*** "));
				ESMSetPropertyCheck(FALSE);

				if (!stCheckedID.bWhiteBalance)
					stDiffValues.nWhiteBalance++;					

				stCheckedID.bWhiteBalance = true;	
			}
		}
		else
		{
			ESMSetPropertyValue(DSC_PROP_WB, strValue);
		}
		strValue.Insert(0, _T(" "));
		pItem->SetDSCProp(DSC_PROP_WB, strValue);
		break;
	case PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN : 
		//strValue = GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL));
		strValue.Format(_T("%d"), nValue);
		if(!ESMGetPropertyValue(DSC_PROP_COLOR_TEMPERATURE).IsEmpty())
		{
			if(strValue.Compare(ESMGetPropertyValue(DSC_PROP_COLOR_TEMPERATURE)) != 0)
			{
				//CString strWB = ESMGetPropertyValue(DSC_PROP_WB);
				//if(strWB.Find(_T("AWB")) == -1)
				//	ESMLog(5, _T("[DSC : %s] Warning!! Check ColorTemperature Value_1 : %s"), pItem->GetDeviceDSCID(), strValue);
				strValue.Insert(0, _T("*** "));
				ESMSetPropertyCheck(FALSE);

				if (!stCheckedID.bColorTemp)
					stDiffValues.nColorTemp++;					

				stCheckedID.bColorTemp = true;	
			}
		}
		else
		{
			ESMSetPropertyValue(DSC_PROP_COLOR_TEMPERATURE, strValue);
		}
		pItem->SetDSCProp(DSC_PROP_COLOR_TEMPERATURE, strValue);
		break;
	case PTP_DPC_WB_DETAIL_KELVIN : 
		{
			if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
				strValue = GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL));
			else // NX Series
				strValue.Format(_T("%d"), nValue);
			CString strTemp = ESMGetPropertyValue(DSC_PROP_COLOR_TEMPERATURE);

			//ESMLog(5, _T("[DSC : %s] ColorTemperature Value : %s, modify: %s"), pItem->GetDeviceDSCID(), strValue,strTemp);

  			if (strTemp.IsEmpty())
  			{
  				ESMSetPropertyValue(DSC_PROP_COLOR_TEMPERATURE, strValue);
  			}

			if(!ESMGetPropertyValue(DSC_PROP_COLOR_TEMPERATURE).IsEmpty())
			{
				if(strValue.Compare(ESMGetPropertyValue(DSC_PROP_COLOR_TEMPERATURE)) != 0)
				{
					//CString strWB = ESMGetPropertyValue(DSC_PROP_WB);
					//if(strWB.Find(_T("AWB")) == -1)
					//	ESMLog(5, _T("[DSC : %s] Warning!! Check ColorTemperature Value : %s"), pItem->GetDeviceDSCID(), strValue);
					strValue.Insert(0, _T("*** "));
					ESMSetPropertyCheck(FALSE);

					if (!stCheckedID.bColorTemp)
						stDiffValues.nColorTemp++;					

					stCheckedID.bColorTemp = true;	
				}
			}
			else
			{
				ESMSetPropertyValue(DSC_PROP_COLOR_TEMPERATURE, strValue);
			}
			pItem->SetDSCProp(DSC_PROP_COLOR_TEMPERATURE, strValue);
		}
		break;
	case PTP_DPC_PHOTO_STYLE			:
		strValue = GetPropertyValuetoStr(nType, nValue, &pItem->GetDSCInfo(DSC_INFO_MODEL));
		//if(!ESMGetPropertyValue(DSC_PROP_PHOTO_STYLE).IsEmpty())
		if(!ESMGetPropertyValue(DSC_PROP_PW).IsEmpty())
		{
			//if(strValue.Compare(ESMGetPropertyValue(DSC_PROP_PHOTO_STYLE)) != 0)
			if(strValue.Compare(ESMGetPropertyValue(DSC_PROP_PW)) != 0)
			{
				//알림
				//ESMLog(5, _T("[DSC : %s] Warning!! Check PhotoStyle Value : %s"), pItem->GetDeviceDSCID(), strValue);
				strValue.Insert(0, _T("*** "));
				ESMSetPropertyCheck(FALSE);

				if (!stCheckedID.bPhotoStyle)
					stDiffValues.nPhotoStyle++;					

				stCheckedID.bPhotoStyle = true;	
			}
		}
		else
		{
			ESMSetPropertyValue(DSC_PROP_PW, strValue);
		}
		strValue.Insert(0, _T(" "));
		//pItem->SetDSCProp(DSC_PROP_PHOTO_STYLE, strValue);
		pItem->SetDSCProp(DSC_PROP_PW, strValue);
		break;
	}	

	if (stDiffValues.mapCheckedID)
		stDiffValues.mapCheckedID->SetAt(strDSCID, stCheckedID);

	ESMSetPropertyDiffCount(stDiffValues);
	ESMSetPropertyMinMaxInfo(stMinMaxInfoValues);

#endif
}

void CDSCViewer::TickSyncAgent()
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 15"));
		return;
	}

	CDSCGroup* pGroup = NULL;

	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;

		if(pGroup->m_nIP == 0)
			continue ;

		if(!pGroup->GetRCMgr())
			continue ;

		if(!pGroup->GetRCMgr()->m_bConnected)
			continue ;

		pGroup->GetRCMgr()->m_bPCSyncTime = FALSE;
		pGroup->SyncTickCount();
	}
}

BOOL CDSCViewer::GetPcSyncCheck()
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 16"));
		return FALSE;
	}

	CDSCGroup* pGroup = NULL;
	BOOL bPcSyncCheck = FALSE;
	int iRoop = 0;
	for(iRoop = 0;iRoop < nAll; iRoop++)
	{
		pGroup = GetGroup(iRoop);
		if(!pGroup)
			continue;

		if(pGroup->m_nIP == 0)
			continue ;

		if(!pGroup->GetRCMgr())
			continue ;

		if( !pGroup->GetRCMgr()->m_bConnected)
			continue;

		if(pGroup->GetRCMgr()->m_bPCSyncTime == FALSE)
			break;
	}
	if( iRoop == nAll)
		bPcSyncCheck = TRUE;
	else
		bPcSyncCheck = FALSE;
	return bPcSyncCheck;

}
//-- Sort DSC
#define STRIDE_FACTOR 3
void CDSCViewer::SortDSC()
{
// 	int iElements = GetItemCount();
// 	if(iElements < 2)
// 		return;
// 
// 	BOOL bFound, bChange = FALSE;
// 	int nLineA, nLineB;
// 	int iInner,iOuter,iStride = 1;
// 	CObject *pTmp;
// 	CObject **pData = &m_arDSCItem[0];
// 
// 	while (iStride <= iElements)
// 		iStride = iStride * STRIDE_FACTOR + 1;
// 
// 	while (iStride > (STRIDE_FACTOR - 1))
// 	{
// 		iStride = iStride / STRIDE_FACTOR;
// 		for (iOuter = iStride; iOuter < iElements; iOuter++)
// 		{
// 			bFound = 0;
// 			iInner = iOuter - iStride;
// 			while ((iInner >= 0) && !bFound)
// 			{
// 				if ( (CompareDSC((CDSCItem*)pData[iInner+iStride],(CDSCItem*)pData[iInner])) > 0)
// 				{
// 					//-- TRACE
// 					//ESMLog(5,_T("Change Data[%d][%s] <-> Data[%d][%s]"),iInner,			((CDSCItem*)pData[iInner		])->GetDeviceDSCID(),
// 					//													iInner+iStride, ((CDSCItem*)pData[iInner+iStride])->GetDeviceDSCID());
// 
// 					nLineA = ((CDSCItem*)pData[iInner+iStride])->GetRow();
// 					nLineB = ((CDSCItem*)pData[iInner])->GetRow();
// 					//ESMLog(5,_T("\t[Before]\tData[%d][%s] Line = %d || Data[%d][%s] Line = %d"),
// 					//			iInner		  , ((CDSCItem*)pData[iInner		])->GetDeviceDSCID(), ((CDSCItem*)pData[iInner			])->GetRow(),
// 					//			iInner+iStride, ((CDSCItem*)pData[iInner+iStride])->GetDeviceDSCID(), ((CDSCItem*)pData[iInner+iStride	])->GetRow());
// 
// 					pTmp = pData[iInner+iStride];
// 					pData[iInner+iStride] = pData[iInner];
// 					//-- 2013-09-23 hongsu@esmlab.com
// 					//-- Change Each Items Line Position
// 					((CDSCItem*)pData[iInner+iStride])->ChangeRow(nLineA);
// 
// 					
// 					pData[iInner] = pTmp;					
// 					//-- 2013-09-23 hongsu@esmlab.com
// 					//-- Change Each Items Line Position
// 					((CDSCItem*)pData[iInner])->ChangeRow(nLineB);
// 
// 					nLineA = ((CDSCItem*)pData[iInner+iStride])->GetRow();
// 					nLineB = ((CDSCItem*)pData[iInner])->GetRow();
// 					//ESMLog(5,_T("\t[After]\tData[%d][%s] Line = %d || Data[%d][%s] Line = %d"),
// 					//	iInner		  , ((CDSCItem*)pData[iInner		])->GetDeviceDSCID(), ((CDSCItem*)pData[iInner			])->GetRow(),
// 					//	iInner+iStride, ((CDSCItem*)pData[iInner+iStride])->GetDeviceDSCID(), ((CDSCItem*)pData[iInner+iStride	])->GetRow());
// 
// 
// 					//-- 2013-09-23 hongsu@esmlab.com
// 					//-- Check Selected Row
// 					m_pListView->ChangeRow(nLineA,nLineB);
// 
// 					iInner -= iStride;
// 					bChange = TRUE;
// 				}
// 				else
// 					bFound = 1;				
// 			}	
// 		}
// 	}

	//-- 2013-09-23 hongsu@esmlab.com
	//-- Update View 
	OrganizeIndex();
	ReDraw();
// 	if(bChange)
// 	{
// 		ReDraw();
// 	}
}

int CDSCViewer::CompareDSC(CDSCItem* pA, CDSCItem* pB)
{
	if(!pA|| !pB)
		return -1;

	CString strA = pA->GetDeviceDSCID();
	CString strB = pB->GetDeviceDSCID();

	if(strA.CompareNoCase(strB) > 0)
		return 0;
	return 1;
}

void CDSCViewer::CameraListLoad()
{
	CESMIni iniInfo;
	CESMIni iniNet;
	CString strInfoConfig;
	CString strInfoNet;

	strInfoConfig.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONFIG);
	strInfoNet.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);
	iniNet.SetIniFilename (strInfoNet);

	if(!strInfoConfig.GetLength())
		return;

	if(!iniInfo.SetIniFilename (strInfoConfig))
		return;

	CDSCItem* pDSC = NULL;

	int nCamCount = iniInfo.GetSectionCount(INFO_CAM_INFO);
	nCamCount = nCamCount / 2;

	CString strCamSectionName;
	CString strCamSectionIP;

	CString strTemp;
	CString strDSCID;
	CString strDSCIP;
	CString strDSCDelete;
	CString strReverse;
	int nDSCGroupID;

	CString strLocalIP;
	strLocalIP = ESMGetLocalIP();

	CString strGroup;

	//wgkim 180512
	CString strVertical;

	CString strDetect;
	
/*	wchar_t chThisPath[_MAX_PATH];
	GetCurrentDirectory( _MAX_PATH, chThisPath);
	UpdateData(TRUE);
	CString strThisPath ;

	strThisPath.Format(_T("%s\\%s.xlsx"), ESMGetPath(ESM_PATH_SETUP), _T("4DMaker"));
	GetModuleFileName( NULL, chThisPath, _MAX_PATH);
	CXLEzAutomation XL(FALSE); 
	XL.OpenExcelFile(strThisPath);
	
	int i = 0;

	while(1)
	{
		i++;
		strTemp	=	XL.GetCellValue(1,i);
		strDSCID = strTemp.Left(5);

		if(strDSCID != _T(""))
		{
			strDSCID = strDSCID.Left(strDSCID.Find(_T(".")));
			strDSCIP	=	XL.GetCellValue(2,i);
			nDSCGroupID = ESMGetIDfromIP(strDSCIP);

			pDSC = new CDSCItem(strDSCID, DSC_REMOTE);
			pDSC->SetDSCInfo(DSC_INFO_LOCATION, strDSCIP);
			pDSC->SetDSCStatus(SDI_STATUS_UNKNOWN);	
			pDSC->SetIndex(i);

			if(strLocalIP == strDSCIP)
			{
				pDSC->m_nRemoteID = 0;
				AddDSC(pDSC);
			}
			else
			{
				pDSC->m_nRemoteID = nDSCGroupID;		
				strInfoConfig = iniNet.GetStringValuetoKey(INFO_SECTION_RC,strDSCIP);
				if(strInfoConfig.GetLength())
				{
					AddDSC(pDSC);
				}
				else
				{
					delete pDSC;
					pDSC = NULL;
				}
			}
		}
		else
		{
			break;
		}
	}

	XL.ReleaseExcel(); */

	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\CameraList.csv"), ESMGetPath(ESM_PATH_SETUP));

	if(!ReadFile.Open(strInputData, CFile::modeRead))
		return;

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if(iLength == 0)
		return;

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0, j = 0, nInsertIndex = 1;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;

	while(1)
	{
		j++;
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;

		AfxExtractSubString(strDSCID, sLine, 0, ',');
		AfxExtractSubString(strDSCIP, sLine, 1, ',');
		AfxExtractSubString(strDSCDelete, sLine, 3, ',');
		AfxExtractSubString(strReverse, sLine, 5, ',');

		//jhhan 180509
		AfxExtractSubString(strGroup, sLine, 6, ',');

		//wgkim 180509
		AfxExtractSubString(strVertical, sLine, 7, ',');
		
		AfxExtractSubString(strDetect, sLine, 8, ',');

		strDSCID.Trim();
		strDSCIP.Trim();
		strDSCDelete.Trim();
		strReverse.Trim();

		strGroup.Trim();
		strVertical.Trim();

		strDetect.Trim();
		
		if(strDSCID != _T(""))
		{
			nDSCGroupID = ESMGetIDfromIP(strDSCIP);

			pDSC = new CDSCItem(strDSCID, DSC_REMOTE);
			pDSC->SetDSCInfo(DSC_INFO_LOCATION, strDSCIP);
			pDSC->SetDSCStatus(SDI_STATUS_UNKNOWN);	

			if(strReverse == _T("REVERSE"))
				pDSC->m_bReverse = TRUE;

			if(strDSCDelete.CompareNoCase(_T("FALSE")) == 0)
				pDSC->m_bDelete = TRUE;
			//pDSC->SetIndex(j);

			//jhhan 180508 Group
			//pDSC->m_strGroup = strGroup;
			pDSC->SetGroup(strGroup);

			//180512 wgkim
			if(strVertical== "NORMAL")
				pDSC->SetVertical(0);
			else if(strVertical== "-90 Degree")
				pDSC->SetVertical(-1);
			else if(strVertical == "90 Degree")
				pDSC->SetVertical(1);
			else
				pDSC->SetVertical(0);

			//jhhan 181210
			if(strDetect == "TRUE")
				pDSC->SetDetect(TRUE);
			else
				pDSC->SetDetect(FALSE);
			
			if(strLocalIP == strDSCIP)
			{
				pDSC->m_nRemoteID = 0;
				pDSC->SetIndex(nInsertIndex++);
				AddDSC(pDSC);
			}
			else
			{
				pDSC->m_nRemoteID = nDSCGroupID;	
				pDSC->m_strIP = strDSCIP;
				strInfoConfig = iniNet.GetStringValuetoKey(INFO_SECTION_RC,strDSCIP);
				if(strInfoConfig.GetLength())
				{
					pDSC->SetIndex(nInsertIndex++);
					AddDSC(pDSC);
				}
				else
				{
					delete pDSC;
					pDSC = NULL;
				}
			}
		}
		else
			break;
	}

/*	for (int i=0; i<nCamCount; i++)
	{
		strCamSectionName.Format(_T("%s_%d"), INFO_CAM_NAME, i);
		strDSCID	=	iniInfo.GetString(INFO_CAM_INFO, strCamSectionName);
		strCamSectionIP.Format(_T("%s_%d"), INFO_CAM_IP, i);
		strDSCIP	=	iniInfo.GetString(INFO_CAM_INFO, strCamSectionIP);

		nDSCGroupID = ESMGetIDfromIP(strDSCIP);
//		if(nDSCGroupID < 0) continue;

		pDSC = new CDSCItem(strDSCID, DSC_REMOTE);
		pDSC->SetDSCInfo(DSC_INFO_LOCATION, strDSCIP);
		pDSC->SetDSCStatus(SDI_STATUS_UNKNOWN);	
		pDSC->SetIndex(i);

		if(strLocalIP == strDSCIP)
		{
			pDSC->m_nRemoteID = 0;
			AddDSC(pDSC);
		}
		else
		{
			pDSC->m_nRemoteID = nDSCGroupID;		
			strInfoConfig = iniNet.GetStringValuetoKey(INFO_SECTION_RC,strDSCIP);
			if(strInfoConfig.GetLength())
			{
				AddDSC(pDSC);
			}
			else
			{
				delete pDSC;
				pDSC = NULL;
			}
		}
	} */

	// Set Dsc Count
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_MOVIE_SET_MOVIECOUNT;
	pMsg->nParam1	= (LPARAM)j-1;//nCamCount;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, WM_ESM_MOVIE, (LPARAM)pMsg);
	SortDSC();
}


void CDSCViewer::CameraListReLoad()
{
	CESMIni iniInfo;
	CESMIni iniNet;
	CString strInfoConfig;
	CString strInfoNet;

	strInfoConfig.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONFIG);
	strInfoNet.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);
	iniNet.SetIniFilename (strInfoNet);

	if(!strInfoConfig.GetLength())
		return;

	if(!iniInfo.SetIniFilename (strInfoConfig))
		return;

	CDSCItem* pDSC = NULL;

	int nCamCount = iniInfo.GetSectionCount(INFO_CAM_INFO);
	nCamCount = nCamCount / 2;

	CString strCamSectionName;
	CString strCamSectionIP;

	CString strTemp;
	CString strDSCID;
	CString strDSCIP;
	CString strDSCDelete;
	CString strReverse;
	int nDSCGroupID;

	CString strLocalIP;
	strLocalIP = ESMGetLocalIP();

	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\CameraList.csv"), ESMGetPath(ESM_PATH_SETUP));

	if(!ReadFile.Open(strInputData, CFile::modeRead))
		return;

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if(iLength == 0)
		return;

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0, j = 0, nInsertIndex = 1;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;

	while(1)
	{
		j++;
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;

		AfxExtractSubString(strDSCID, sLine, 0, ',');
		AfxExtractSubString(strDSCIP, sLine, 1, ',');
		AfxExtractSubString(strDSCDelete, sLine, 3, ',');
		AfxExtractSubString(strReverse, sLine, 5, ',');

		strDSCID.Trim();
		strDSCIP.Trim();
		strDSCDelete.Trim();
		strReverse.Trim();

		if(strDSCID != _T(""))
		{
			pDSC = GetItemData(strDSCID);

			if(strReverse == _T("REVERSE"))
				pDSC->m_bReverse = TRUE;

			if(pDSC)
			{
				if(strDSCDelete.CompareNoCase(_T("FALSE")) == 0)
					pDSC->m_bDelete = TRUE;
				else
					pDSC->m_bDelete = FALSE;
			}
		}
		else
			break;
	}
}

void CDSCViewer::SetMovieSaveCheck(bool bCheck)
{
	int nAll = GetGroupCount();	
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 17"));
		return;
	}

	CDSCGroup* pGroup = NULL;
	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(!pGroup)
			continue;
		pGroup->DoMovieSaveCheck(bCheck);
	}
}
CString CDSCViewer::GetDSCIDFromIndex(int nIdx)
{
	CDSCItem* pExist = NULL;

	pExist = GetItemData(nIdx);

	return pExist->GetDeviceDSCID();
}