////////////////////////////////////////////////////////////////////////////////
//
//	DSCViewer.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCViewer.h"
#include "resource.h"
#include "ESMDefine.h"
#include "DSCMgr.h"
#include "ESMSplashWnd.h"
#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CDSCViewer
IMPLEMENT_DYNCREATE(CDSCViewer, CWnd)
BEGIN_MESSAGE_MAP(CDSCViewer, CWnd)	
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()	
END_MESSAGE_MAP()

CDSCViewer::CDSCViewer()
{
	m_bClose			= FALSE;
	m_pListView			= NULL;
	m_pDSCMgr			= NULL;
	m_pFrameSelector	= NULL;
	m_nSelectLine		= -1;
	m_bScaleToWindow	= FALSE;
	m_bInsertID			= FALSE;
	m_bExecuteMovieMode	= FALSE;
	m_bGetFocusState	= FALSE;
	m_MarginX = 0;
	m_MarginY = 0;
	m_bPauseMode = FALSE;
	m_bRecordStep = ESM_RECORD_STOP;

	//-- For Adjust
	for(int i = 0 ; i < DSC_ADJ_VAL_CNT ; i ++)
		m_nAdjPosition[i] = 0;

	InitializeCriticalSection (&m_CriSection);
}

CDSCViewer::~CDSCViewer()
{
	int nAll;

	nAll= GetItemCount();
	CDSCItem* pDSC = NULL;
	while(nAll--)
	{
		pDSC = (CDSCItem*)m_arDSCItem.GetAt(nAll);
		if(pDSC)
		{
			delete pDSC;
			pDSC = NULL;
		}
		m_arDSCItem.RemoveAt(nAll);
	}

	if(m_arDSCItem.GetCount() > 0)
		m_arDSCItem.RemoveAll();	

	nAll = GetGroupCount();
	CDSCGroup* pGroup = NULL;
	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(pGroup)
		{
			delete pGroup;
			pGroup = NULL;
		}
		m_arDSCGroup.RemoveAt(nAll);
	}
	m_arDSCGroup.RemoveAll();	

	if(m_pDSCMgr)
	{
		delete m_pDSCMgr;
		m_pDSCMgr = NULL;
	}

	//-- 2013-09-05 hongsu@esmlab.com
	/* Delete CView ???
	if(m_pListView)			{ delete m_pListView;		m_pListView = NULL;		}
	if(m_pFrameSelector)	{ delete m_pFrameSelector;	m_pFrameSelector = NULL;}
	*/

	DeleteCriticalSection (&m_CriSection);
}

int CDSCViewer::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd ::OnCreate(lpCreateStruct) == -1)
		return -1;	
	return 0;
}

BOOL CDSCViewer::OnEraseBkgnd(CDC* pDC) 
{
	CRect rect;
	GetClientRect(&rect);
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
	pDC->FillSolidRect(rect, COLOR_BASIC_BG_5 );
#else
	pDC->FillSolidRect(rect, COLOR_BASIC_BG_1 );
#endif
	Invalidate(FALSE);
	return TRUE;
}

//------------------------------------------------------------------------------
//! @function	sample function
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCViewer::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
	ReDraw();
} 

void CDSCViewer::ReDraw()
{
	CRect rectListView;
	CRect rectEditView;
	GetClientRect(&rectListView);

	CDC *pDC = GetDC();
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
	pDC->FillSolidRect(rectListView, COLOR_BASIC_BG_5 );
#else
	pDC->FillSolidRect(rectListView, COLOR_BASIC_BG_1 );
#endif
	Invalidate(FALSE);
	
	//-- 2013-09-05 hongsu@esmlab.com
	//-- At First Draw ListView	
	if(!m_pListView)
	{
		ReleaseDC(pDC);
		return;
	}

	//-- Size Calculate
	m_pListView->SizeCalculate(&rectListView);
	//-- Move Window
	m_pListView->MoveWindow(rectListView);

	GetClientRect(&rectEditView);
	rectEditView.left	= rectListView.right;

	//-- Scroll Size
	int v_scroll_width = GetSystemMetrics(SM_CXVSCROLL);

	rectEditView.bottom	= rectListView.bottom + v_scroll_width;
	m_pFrameSelector->MoveWindow(rectEditView);

	//-- Draw Non Plate
	CRect rectOthers;
	GetClientRect(&rectOthers);
	rectOthers.top = rectEditView.bottom;
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
	pDC->FillSolidRect(rectOthers, COLOR_BASIC_BG_5);
#else
	pDC->FillSolidRect(rectOthers, COLOR_BASIC_BG_1);
#endif
	ReleaseDC(pDC);

}

//------------------------------------------------------------------------------
//! @function	Organize Index
//------------------------------------------------------------------------------
void CDSCViewer::OrganizeIndex() 
{
	int nAll = GetItemCount();
	CDSCItem* pItem = NULL;
	CString strIndex;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = GetItemData(i);
		if(pItem)
		{
			strIndex.Format(_T("%d"),i+1);
			pItem->SetDSCInfo(DSC_INFO_NUM,strIndex);

			for(int n = 0 ; n < DSC_INFO_CNT; n ++)
			{
				if(pItem->m_pStaticInfo[n])
				{
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
					if(i%2)	pItem->m_pStaticInfo[n]->SetColors(COLOR_BASIC_FG_3, COLOR_BASIC_BG_5, COLOR_WHITE, COLOR_BASIC_HBG	);	//Hot BG
					else	pItem->m_pStaticInfo[n]->SetColors(COLOR_BASIC_FG_4, COLOR_BASIC_BG_6, COLOR_WHITE, COLOR_BASIC_HBG	);	//Hot BG
#else
					if(i%2)	pItem->m_pStaticInfo[n]->SetColors(COLOR_BASIC_FG_1, COLOR_BASIC_BG_1, COLOR_BASIC_HFG, COLOR_BASIC_HBG	);	//Hot BG
					else	pItem->m_pStaticInfo[n]->SetColors(COLOR_BASIC_FG_2, COLOR_BASIC_BG_2, COLOR_BASIC_HFG, COLOR_BASIC_HBG	);	//Hot BG
#endif
				}
			}

			for(int n = 0 ; n < DSC_ADJ_CNT; n ++)
			{
				if(pItem->m_pStaticAdj[n])
				{
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
					if(i%2)	pItem->m_pStaticAdj[n]->SetColors(COLOR_BASIC_FG_3, COLOR_BASIC_BG_5, COLOR_WHITE, COLOR_BASIC_HBG	);	//Hot BG
					else	pItem->m_pStaticAdj[n]->SetColors(COLOR_BASIC_FG_4, COLOR_BASIC_BG_6, COLOR_WHITE, COLOR_BASIC_HBG	);	//Hot BG
#else
					if(i%2)	pItem->m_pStaticAdj[n]->SetColors(COLOR_BASIC_FG_1, COLOR_BASIC_BG_1, COLOR_BASIC_HFG, COLOR_BASIC_HBG	);	//Hot BG
					else	pItem->m_pStaticAdj[n]->SetColors(COLOR_BASIC_FG_2, COLOR_BASIC_BG_2, COLOR_BASIC_HFG, COLOR_BASIC_HBG	);	//Hot BG
#endif
				}
			}

			for(int n = 0 ; n < DSC_PROP_CNT; n ++)
			{
				if(pItem->m_pStaticProp[n])
				{
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
					if(i%2)	pItem->m_pStaticProp[n]->SetColors(COLOR_BASIC_FG_3, COLOR_BASIC_BG_5, COLOR_WHITE, COLOR_BASIC_HBG	);	//Hot BG
					else	pItem->m_pStaticProp[n]->SetColors(COLOR_BASIC_FG_4, COLOR_BASIC_BG_6, COLOR_WHITE, COLOR_BASIC_HBG	);	//Hot BG
#else
					if(i%2)	pItem->m_pStaticProp[n]->SetColors(COLOR_BASIC_FG_1, COLOR_BASIC_BG_1, COLOR_BASIC_HFG, COLOR_BASIC_HBG	);	//Hot BG
					else	pItem->m_pStaticProp[n]->SetColors(COLOR_BASIC_FG_2, COLOR_BASIC_BG_2, COLOR_BASIC_HFG, COLOR_BASIC_HBG	);	//Hot BG
#endif
				}
			}
		}		
	}
}


//------------------------------------------------------------------------------
//! @function	Init
//------------------------------------------------------------------------------
void CDSCViewer::InitDSCViewer(HWND hWnd) 
{
	m_hParentWnd = hWnd;
	//-- 2013-09-05 hongsu@esmlab.com
	//-- 2013-09-10 hongsu@esmlab.com
	//-- Init DSC Viewer 
	if(m_pSplash)
		m_pSplash->AddTextLine( _T("Create DSC Manager") );	
	//-- Create DSC Manager
	InitDSCMgr();

	if(m_pSplash)
		m_pSplash->AddTextLine( _T("Create Viewer") );

	m_pListView = new CDSCListViewer(this);	
	m_pFrameSelector= new CDSCFrameSelector(this);	

	m_pListView->Create(NULL, NULL, DS_SETFONT | WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDD_VIEW_DSC_LIST);
	m_pFrameSelector->Create(NULL, NULL, DS_SETFONT | DS_CONTROL | WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL , CRect(0,0,0,0), this, IDD_VIEW_DSC_IMG);	
}

void CDSCViewer::InitDSCMgr()
{
	if(m_pDSCMgr)
	{
		delete m_pDSCMgr;
		m_pDSCMgr = NULL;
	}

	m_pDSCMgr = new CDSCMgr(this);
	//-- Create Thread
	m_pDSCMgr->CreateThread();
}

//------------------------------------------------------------------------------
//! @function	from Message
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCViewer::SetGridStatus(int nRow, int nStatus)
{
	m_pListView->SetGridStatus(nRow, nStatus);
	m_pFrameSelector->SetGridStatus(nRow, nStatus);
}


BOOL CDSCViewer::GetKeyStatus(int nKey)
{
	return ((GetKeyState(nKey) & 0x8000) != 0);		
}

//------------------------------------------------------------------------------
//! @function	Adjust Manager (Load/Save)
//! @date		2013-10-14
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
void CDSCViewer::LoadAdjustProfile(CString strFileName)
{
	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		CString strHome = ESMGetPath(ESM_PATH_FILESERVER);
		if(strFileName.Find(_T("F:")) != -1)
		{
			strHome.Replace(_T("4DMaker"), _T(""));
			strFileName.Replace(_T("F:\\"),strHome);
		}
	}
	CFile file;
	ESMEvent* pMsgValid = NULL;
	//CString strFile;
	//strFile.Format(_T("%s\\%s.adj"),ESMGetPath(ESM_PATH_ADJUST_CONF),strFileName);	

	if(!file.Open(strFileName, CFile::modeRead))
	{
		ESMLog(0,_T("Load Adjust Config File [%s]") , strFileName);
		return;
	}

	//-- Save DSCItem List
	CArchive ar(&file, CArchive::load);
	CDSCItem* pItem = NULL;

	int nAll;
	ar>>nAll;

	float nVersion = -20.0;
	if( nAll < -1 || nAll > 100)
	{

		if(-11 <  nAll && nAll < -1)	//Version
			nVersion = (float)nAll;
		else
			memcpy(&nVersion, &nAll, sizeof(float));

		ar>>nAll;

	}

	//-- 2013-10-05 hongsu@esmlab.com
	//-- Liveview Info
	for(int i = 0 ; i < DSC_ADJ_VAL_CNT; i ++)
		ar>>m_nAdjPosition[i];

	if( ESMGetVersion(nVersion) >= ESM_4DMAKER_VERSION_10006)
	{
		ar>>m_MarginX;
		ar>>m_MarginY;
	}

	int nTemp;
	float fTemp;
	CString strTemp;
	CString strDSC;
	for(int i = 0 ; i < nAll ; i ++)
	{
		//-- 2013-09-23 hongsu@esmlab.com
		//-- Sort After Create SdiManager
		ar >> strDSC;

		//-- 2013-10-14 hongsu@esmlab.com
		//-- Find DSC;
		pItem = GetItemData(strDSC);
		if(!pItem)
		{
			CDSCItem* pItemDummy = new CDSCItem;
			pItemDummy->SetVersionInfo((float)nVersion);
			pItemDummy->Serialize(ar);
			//pItemDummy->UpdateAdj();

			delete pItemDummy;
			/*ar>>nTemp;
			for( i = 0 ; i < DSC_INFO_CNT ; i++ )	ar>>strTemp;
			for( i = 0 ; i < DSC_ADJ_CNT ; i++ )	ar>>fTemp;
			for( i = 0 ; i < DSC_PROP_CNT ; i++ )	ar>>strTemp;*/
		}
		else
		{
			//-- 2013-10-14 hongsu@esmlab.com
			pItem->SetVersionInfo((float)nVersion);
			pItem->Serialize(ar);
			pItem->SetMargin(m_MarginX,m_MarginY);
			pItem->UpdateAdj();
		}
	}

	//-- 2014-07-15 hjjang
	//-- Set Adjust Info
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_ESM_MOVIE_SET_ADJUSTINFO;
	pMsg->nParam1 = GetMarginX();
	pMsg->nParam2 = GetMarginY();
	::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);

	ar.Close();
	file.Close();

	//jhhan 16-11-18 Adjust 로드 여부 기록
	ESMSetAdjustLoad(TRUE);

	//-- Redraw View
	ReDraw();
	ESMLog(1,_T("[CHECK_D]Finish Load Adjust Profile [%s]") , strFileName);
}

void CDSCViewer::SaveAdjustProfile(CString strFileName, void* pAdj)
{
	CFile file;
	CString strFile;
	strFile = strFileName;
	if( strFile.Right(4) != _T(".adj"))
		strFile = strFile + _T(".adj");

	//strFile = strFile + _T(".adj");
	//strFile.Format(_T("%s\\%s.adj"),ESMGetPath(ESM_PATH_ADJUST_CONF),strFileName);	

	if(!file.Open(strFile, CFile::modeCreate | CFile::modeReadWrite))
	{
		ESMLog(0,_T("Create Adjust Config File [%s]") , strFile);
		return;
	}


	//-- Save DSCItem List
	CArchive ar(&file, CArchive::store);

	CDSCItem* pItem = NULL;
	int nAll = GetItemCount();

	float nVersion = (float)ESM_4DMAKER_VERSION_CURRENTVER;
	ar<<nVersion;
	ar<<nAll;

	//-- 2013-10-05 hongsu@esmlab.com
	//-- Liveview Info
	for(int i = 0 ; i < DSC_ADJ_VAL_CNT; i ++)
		ar<<m_nAdjPosition[i];

	ar<<m_MarginX;
	ar<<m_MarginY;

	vector<stAdjustInfo>* arrAdj = NULL;
	arrAdj = (vector<stAdjustInfo>*)pAdj;
	
	for(int i = 0 ; i < nAll ; i ++)
	{
		
		stAdjustInfo* adj = NULL;
		if(arrAdj)
		{
			adj = &arrAdj->at(i);
		}
		pItem = GetItemData(i);
		ar<<pItem->GetDeviceDSCID();		
		pItem->Serialize(ar, adj);
	}

	/*
	//-- 2014-09-17 hongsu@esmlab.com
	//-- Add Valid Frame Info

	ValidFrame* pExist = NULL;
	nAll = m_pDSCMgr->GetValidFrameCount();
	ar<<nAll;
	while(nAll--)
	{
		pExist = m_pDSCMgr->GetValidFrame(nAll);
		ar.WriteString(pExist->strDSC);
		//ar<<pExist->strDSC;			//- DSC ID
		ar<<pExist->nCurrentIndex;	//- Current Index
		ar<<pExist->nCorrectIndex;	//- Correct Index
	}

	*/

	ar.Close();
	file.Flush();
	file.Close();

	CString strNewFileName;
	strNewFileName.Format(_T("%s\\NewestAdj.adj"), ESMGetPath(ESM_PATH_MOVIE_CONF));
	CopyFile(strFile, strNewFileName, FALSE);
	ESMLog(1,_T("Finish Save Adjust Profile [%s]") , strFile);
}

void CDSCViewer::SaveSyncTestFile(CString strFileName)
{
	CFile file;
	CString strFile;
	strFile = strFileName;
	if( strFile.Right(4) != _T(".csv"))
		strFile = strFile + _T(".csv");

	//strFile = strFile + _T(".adj");
	//strFile.Format(_T("%s\\%s.adj"),ESMGetPath(ESM_PATH_ADJUST_CONF),strFileName);	

	if(!file.Open(strFile, CFile::modeCreate | CFile::modeReadWrite))
	{
		ESMLog(0,_T("Create File [%s]") , strFile);
		return;
	}


	CString strData;
	strData.Append(_T("IP,DSC,"));
	strData.Append(m_pListView->m_strTesting[TESTING_PROP_TOTAL_CNT]);
	strData.Append(_T(","));
	strData.Append(m_pListView->m_strTesting[TESTING_PROP_FAIL_CNT]);
	strData.Append(_T(","));
	strData.Append(m_pListView->m_strTesting[TESTING_PROP_AS_CNT]);
	strData.Append(_T(","));
	strData.Append(m_pListView->m_strTesting[TESTING_PROP_AS_AVG]);
	strData.Append(_T(","));
	strData.Append(m_pListView->m_strTesting[TESTING_PROP_AS_MIN]);
	strData.Append(_T(","));
	strData.Append(m_pListView->m_strTesting[TESTING_PROP_AS_MAX]);
	strData.Append(_T(","));
	strData.Append(m_pListView->m_strTesting[TESTING_PROP_AC_CNT]);
	strData.Append(_T(","));
	strData.Append(m_pListView->m_strTesting[TESTING_PROP_AC_AVG]);
	strData.Append(_T(","));
	strData.Append(m_pListView->m_strTesting[TESTING_PROP_AC_MIN]);
	strData.Append(_T(","));
	strData.Append(m_pListView->m_strTesting[TESTING_PROP_AC_MAX]);
	strData.Append(_T("\n"));
	


	CDSCGroup* pGroup = NULL;
	CString strIP;
	CString strVal;
	
	int nAll = m_pListView->GetItemCount();
	CDSCItem* pDSCItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = m_pListView->GetItemData(i);
		strIP = ESMGetIPFromDSCID(pDSCItem->GetDeviceDSCID());

		strData.Append(strIP);
		strData.Append(_T(","));
		strData.Append(pDSCItem->GetDeviceDSCID());
		strData.Append(_T(","));

		for(int n = 0; n < TESTING_PROP_CNT; n++)
		{
			strVal.Format(_T("%.02f,"), pDSCItem->m_nTesting[n]);
			strData.Append(strVal);
		}
		strData.Append(_T("\n"));
	}	

	file.Write(strData,strData.GetLength()*2);
	
	file.Flush();
	file.Close();

	ESMLog(1,_T("Finish Save Sync Test File [%s]") , strFile);
}

#include "FFmpegManager.h"
void CDSCViewer::LoadMovieFile(CString strID, int nIndex)
{
	CDSCItem* pItem = NULL;
	ESMEvent* pMsg	= NULL;
	CString strExistID, strFolder;
	int nAll = GetItemCount();
	int nFileCount = 0 , nTime = 0;
	CESMFileOperation fo;
	//FFmpegManager FFmpegMgr(FALSE);
	while(nAll--)
	{
		pItem = GetItemData(nAll);
		if(!pItem)
			break;
		//-- 2013-10-18 hongsu@esmlab.com
		//-- Find File
		strExistID = pItem->GetDeviceDSCID();
		if(strExistID == strID)
		{
			//strFolder.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), strID);

// 			nTime = FFmpegMgr.GetRunningTime(strFolder);
// 			pItem->SetSavedLastTime(nTime);
			pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_FRAME_CONVERT_FINISH;
			pMsg->nParam1		= nIndex;
			pMsg->pDest		= (LPARAM)pItem;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);

			return;
		}
	}	
}


void CDSCViewer::GroupUpdateStatus(ESMEvent* pMsg)
{
	int nAll = GetGroupCount();
	CDSCGroup* pGroup = NULL;
	CDSCItem* pItem = NULL;
	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(pGroup->m_nIP == pMsg->nParam1)
		{
			int nItemCount = pGroup->m_arDSC.GetCount();
			while(nItemCount--)
			{
				pItem = (CDSCItem*)pGroup->m_arDSC.GetAt(nItemCount);
				if(pItem)
				{
					if (!pMsg->nParam2)
					{
						pItem->SetDSCStatus(SDI_STATUS_DISCONNECT);
						pItem->SetTemperatureWarning(FALSE);

						m_pListView->RedrawInfo();
					}
					//else
						//pItem->SetDSCStatus(SDI_STATUS_CONNECTED);
				}
			}
		}	
	}
	//Sleep(34);
	SortDSC();
}

void CDSCViewer::GroupSyncNet(ESMEvent* pMsg)
{
	int nAll = GetGroupCount();
	CDSCGroup* pGroup = NULL;
	while(nAll--)
	{
		pGroup = GetGroup(nAll);
		if(pGroup->m_nIP == pMsg->nParam1)
		{
			pGroup->SetRCMgr((CESMRCManager*)pMsg->pParam);
		}	
	}
	SortDSC();
}

BOOL CDSCViewer::GetExecuteMovieMode()
{
	return m_bExecuteMovieMode;
}

void CDSCViewer::SetExecuteMovieMode(BOOL bExecuteMovieMode)
{
	m_bExecuteMovieMode = bExecuteMovieMode;
}

void CDSCViewer::SaveDscInfo(CArchive& ar, BOOL bInit, BOOL bLog)
{
	//jhhan 16-11-23 SyncData 점검
	if(ESMGetExecuteMode())
		m_pDSCMgr->OnEventSyncDataSet();


	CDSCItem* pItem = NULL;
	int nDscCount = GetItemCount();
	ar<<nDscCount;
	for(int i = 0 ; i < DSC_ADJ_VAL_CNT; i ++)
		ar<<m_nAdjPosition[i];

	ar<<m_MarginX;
	ar<<m_MarginY;
	ar<<ESMGetRecTime(bInit);

	for(int i = 0 ; i < nDscCount ; i ++)
	{
		pItem = GetItemData(i);
		pItem->Serialize(ar);
	}

	for( int i = 0 ;i < nDscCount; i++)
	{
		pItem = GetItemData(i);
		ar<<pItem->GetSensorSync();
		ar<<pItem->GetCaptureExcept();
		ar<<pItem->GetCaptureImageRecExcept();

		if(pItem->GetSensorSync() == FALSE/* || pItem->GetCaptureExcept() != FALSE*/)
		{
			if(bLog)
			{
				//jhhan 16-11-22 SensorSync 저장 확인
				ESMLog(5, _T("SaveDscInfo[%d] - DSCID : %s, SensorSync : %d, CaptureExcept : %d"), i+1, pItem->GetDeviceDSCID(), pItem->GetSensorSync(), pItem->GetCaptureExcept());
			}
		}
		
	}

	ValidFrame* pExist = NULL;
	int nAll = m_pDSCMgr->GetValidFrameCount();
	ar<<nAll;
	CString strTp;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExist = m_pDSCMgr->GetValidFrame(i);
		TRACE(_T("Save strDSC [%d/%d]: %s, nCurrentIndex: %d, nCorrectIndex: %d\n"), i+1, nAll, pExist->strDSC, pExist->nCurrentIndex, pExist->nCorrectIndex);
		strTp.Format(_T("%s\n"), pExist->strDSC);
		ar.WriteString(strTp);
		ar<<pExist->nCurrentIndex;	//- Current Index
		ar<<pExist->nCorrectIndex;	//- Correct Index
	}
}

void CDSCViewer::LoadDscInfo(CArchive& ar, int nCount ,float nVersion)
{
	CDSCItem* pItem = NULL;
	ValidFrame* pNewValidInfo = NULL;
	ESMEvent* pMsgValid = NULL;

	//-- 2013-10-05 hongsu@esmlab.com
	//-- Liveview Info
	for(int i = 0 ; i < DSC_ADJ_VAL_CNT; i ++)
		ar>>m_nAdjPosition[i];

	if( ESMGetVersion(nVersion) >= ESM_4DMAKER_VERSION_10006)
	{
		ar>>m_MarginX;
		ar>>m_MarginY;
	}
	int nRunningTime = 0, nMovieHeight = 0, nMovieWidth = 0;
	if( ESMGetVersion(nVersion) >= ESM_4DMAKER_VERSION_10011)
	{
		ar>>nRunningTime;
		ESMSetRecTime(nRunningTime);
	}
	for(int i = 0 ; i < nCount ; i ++)
	{
		//-- 2013-09-23 hongsu@esmlab.com
		//-- Sort After Create SdiManager
		pItem = new CDSCItem();
		pItem->SetVersionInfo(nVersion);
		pItem->Serialize(ar);
		//pItem->SetType(DSC_UNPREPARED);
		//pItem->SetIndex(i);
		pItem->SetIndex(i+1);
		m_arDSCItem.Add((CObject*)pItem);
		//-- 2013-10-19 hongsu@esmlab.com
		//-- Set Status
		pItem->SetDSCStatus(SDI_STATUS_LOAD);

		//180410 hjcho
		pItem->SetMargin(m_MarginX,m_MarginY);

		if( ESMGetFilmState() ==ESM_ADJ_FILM_STATE_MOVIE)
		{
			pItem->SetSavedLastTime(nRunningTime);
			LoadMovieFile(pItem->GetDeviceDSCID(), i);
		}
		else
			pItem->SetDSCStatus(SDI_STATUS_CAPTURE_OK);
	}

	//-- 2014-09-17 hongsu@esmlab.com
	//-- Add Valid Frame Info
	if( ESMGetVersion(nVersion) >= ESM_4DMAKER_VERSION_10008)
	{
		BOOL bSync = FALSE;
		for(int i = 0 ; i < nCount ; i ++)
		{
			pItem = GetItemData(i);
			ar>>bSync;
			pItem->SetSensorSync(bSync);
			ar>>bSync;
			pItem->SetCaptureExcept(bSync);
			
			if (ESMGetVersion(nVersion) >= ESM_4DMAKER_VERSION_10016)
			{
				ar>>bSync;
				pItem->SetCaptureImageRecExcept(bSync);
			}

			if(pItem->GetSensorSync() == FALSE/* || pItem->GetCaptureExcept() != FALSE*/)
			{
				//jhhan 16-11-22 SensorSync 저장 확인
				ESMLog(5, _T("LoadDscInfo[%d] - DSCID : %s, SensorSync : %d, CaptureExcept : %d"), i+1, pItem->GetDeviceDSCID(), pItem->GetSensorSync(), pItem->GetCaptureExcept());
			}
			

		}

		ValidFrame* pNewValidInfo = NULL;
		ValidFrame* pNewValidMovie = NULL;
		int nAll;
		CString strDSC;		//- DSC ID
		int nCurrentIndex = 0;	//- Current Index
		int nCorrectIndex = 0;	//- Correct Index

		ar>>nAll;
		CString strTp;
		while(nAll--)
		{
			pNewValidInfo = new ValidFrame;
			ar.ReadString(strDSC);
			int nSt = pNewValidInfo->strDSC.GetLength();
			ar>>nCurrentIndex;	//- Current Index
			ar>>nCorrectIndex;	//- Correct Index
			pNewValidInfo->strDSC = strDSC;
			pNewValidInfo->nCurrentIndex	= nCurrentIndex;
			pNewValidInfo->nCorrectIndex	= nCorrectIndex;
			m_pDSCMgr->AddValidFrame(pNewValidInfo);

			TRACE(_T("Load strDSC: %s, nCurrentIndex: %d, nCorrectIndex: %d\r\n"), pNewValidInfo->strDSC, pNewValidInfo->nCurrentIndex, pNewValidInfo->nCorrectIndex);
			//------------------------------------------
			//-- 2014/09/18 hongsu
			//-- Send To Movie Manager
			//------------------------------------------
			pNewValidMovie = new ValidFrame;
			pNewValidMovie->strDSC			=strDSC			;				
			pNewValidMovie->nCurrentIndex	=nCurrentIndex	;
			pNewValidMovie->nCorrectIndex	=nCorrectIndex	;

			pMsgValid = new ESMEvent();
			pMsgValid->message = WM_ESM_MOVIE_VALIDFRAME;
			pMsgValid->pParam	= (LPARAM)pNewValidMovie;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsgValid);
		}
	}
	//-- Redraw View
	ReDraw();

	m_bExecuteMovieMode	 = FALSE;
}

int CDSCViewer::GetDelayTime(CString strDSCId)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\CaptureDelayTime.csv"), ESMGetPath(ESM_PATH_SETUP));
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());
	if( iLength == 0 )
	{
		ReadFile.Close();
		return 0;
	}
	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue1, strTpValue2;
	int nDistance = 0;
	while (iPos  < iLength )
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		AfxExtractSubString(strTpValue1, sLine, 0, ',');
		AfxExtractSubString(strTpValue2, sLine, 1, ',');
		if( strTpValue1 == strDSCId)
		{
			nDistance = _ttoi(strTpValue2);
			break;
		}
	}
	return nDistance;
}
int CDSCViewer::GetSensorOnDesignation(CString strDSCId)
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\SensorOnDesignation.csv"), ESMGetPath(ESM_PATH_SETUP));
	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		return FALSE;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());
	if( iLength == 0 )
	{
		ReadFile.Close();
		return 0;
	}

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;
	CString strTpValue1, strTpValue2;
	int nDistance = 0;
	while (iPos  < iLength )
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;
		AfxExtractSubString(strTpValue1, sLine, 0, ',');
		AfxExtractSubString(strTpValue2, sLine, 1, ',');
		if( strTpValue1 == strDSCId)
		{
			nDistance = _ttoi(strTpValue2);
			break;
		}
	}
	return nDistance;
}

void CDSCViewer::SaveDscInfoEx(CArchive& ar)
{
	//jhhan 16-11-23 SyncData 점검
	if(ESMGetExecuteMode())
		m_pDSCMgr->OnEventSyncDataSet();


	CDSCItem* pItem = NULL;
	int nDscCount = GetItemCount();
	ar<<nDscCount;
	for(int i = 0 ; i < DSC_ADJ_VAL_CNT; i ++)
		ar<<m_nAdjPosition[i];

	ar<<m_MarginX;
	ar<<m_MarginY;
	ar<<ESMGetRecTime()+5000;

	for(int i = 0 ; i < nDscCount ; i ++)
	{
		pItem = GetItemData(i);
		pItem->Serialize(ar);
	}

	for( int i = 0 ;i < nDscCount; i++)
	{
		pItem = GetItemData(i);
		ar<<pItem->GetSensorSync();
		ar<<pItem->GetCaptureExcept();
		ar<<pItem->GetCaptureImageRecExcept();

		if(pItem->GetSensorSync() == FALSE/* || pItem->GetCaptureExcept() != FALSE*/)
		{
			//if(bLog)
			//{
				////jhhan 16-11-22 SensorSync 저장 확인
				//ESMLog(5, _T("SaveDscInfo[%d] - DSCID : %s, SensorSync : %d, CaptureExcept : %d"), i+1, pItem->GetDeviceDSCID(), pItem->GetSensorSync(), pItem->GetCaptureExcept());
			//}
		}

	}

	ValidFrame* pExist = NULL;
	int nAll = m_pDSCMgr->GetValidFrameCount();
	ar<<nAll;
	CString strTp;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExist = m_pDSCMgr->GetValidFrame(i);
		TRACE(_T("Save strDSC [%d/%d]: %s, nCurrentIndex: %d, nCorrectIndex: %d\n"), i+1, nAll, pExist->strDSC, pExist->nCurrentIndex, pExist->nCorrectIndex);
		strTp.Format(_T("%s\n"), pExist->strDSC);
		ar.WriteString(strTp);
		ar<<pExist->nCurrentIndex;	//- Current Index
		ar<<pExist->nCorrectIndex;	//- Correct Index
	}
}