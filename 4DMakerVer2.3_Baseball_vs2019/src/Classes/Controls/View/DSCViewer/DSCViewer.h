////////////////////////////////////////////////////////////////////////////////
//
//	DSCViewer.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "DSCListViewer.h"
#include "DSCFrameSelector.h"
#include "DSCItem.h"
#include "DSCGroup.h"
#include "DSCViewDefine.h"
#ifdef _4DMODEL
#include "4DModelerVersion.h"
#else
#include "4DMakerVersion.h"
#endif

#include "FFmpegManager.h"

class CDSCMgr;
class CESMSplashWnd;

//-- Thread Function
void _DoTakePicture(void *param);
void _DoMakeMovie3D(void *param);
void _DoLoadProfile(void *param);
void _DoSaveProfile(void *param);

struct FocusData
{
	FocusData()
	{
		pDSCViewer = NULL;
		strId = NULL;
		nMinFocus = 0;
		nSetFocus = 0;
	}
	CDSCViewer* pDSCViewer;
	TCHAR*	strId;
	int		nMinFocus;
	int		nSetFocus;
};

class CDSCViewer : public CWnd
{
public:
	CDSCViewer();
	virtual ~CDSCViewer();
	DECLARE_DYNCREATE(CDSCViewer)

public:	
	//-- DSC Info Array 
	CESMArray			m_arDSCItem;
	CESMArray			m_arDSCGroup;
	//-- DSC Manager
	CDSCMgr				* m_pDSCMgr;
	//-- Viewer
	CDSCListViewer		* m_pListView;
	CDSCFrameSelector	* m_pFrameSelector;
	//-- Splash
	CESMSplashWnd		* m_pSplash;
	//-- HWND
	HWND				m_hParentWnd;
	BOOL				m_bClose;

	BOOL				m_bPauseMode;
	int					m_bRecordStep;
	CString				m_strPage; //16/07/15

private:
	//-- Selected Line
	int		m_nSelectLine;	
	BOOL	m_bGetFocusState;

public:
	//-- Thread Handle
	BOOL	m_bExecuteMovieMode;

public:
	//-- Scaling Time line
	BOOL m_bScaleToWindow;

	BOOL m_bInsertID;
	BOOL m_bRCMode;
	int	 m_MarginX;
	int	 m_MarginY;
	int  m_nMovieWidth, m_nMovieHeight;
	CRect m_rtMarginRect;	//wgkim 190527
	CRITICAL_SECTION m_CriSection;
	
public:
	void InitDSCViewer(HWND hWnd);
	void InitDSCMgr();
	void RemoveListAll();
	//void RemoveWorkFile();
	void RemoveTimeLineEditor();
	int GetConnectStatus(int nRemoteId);
	void SaveDscInfo(CArchive& ar, BOOL bInit = FALSE, BOOL bLog = TRUE);
	void LoadDscInfo(CArchive& ar, int nCount ,float nVersion);

	int GetDelayTime(CString strDSCId);
	int GetSensorOnDesignation(CString strDSCId);
	//-- CONNECTION
	void AddDSC(CDSCItem* pDSC);	
	void AddDSC(CString strUsbID);	
	void SortDSC();
	int CompareDSC(CDSCItem* pA, CDSCItem* pB);
	BOOL IsExist(CString strUsbID);
	void CheckDisconnet(CStringArray* pArList);

	//----------------------------------------
	//-- Array Management
	//----------------------------------------
	int GetItemCount();
	int GetItemIndex(CString strDSC);
	CDSCItem* GetItemData(int nIndex);
	CDSCItem* GetItemData(CString strDSC, BOOL bUUID=FALSE);
	void DeleteItem(CString strDSC);
	
	void GetDSCList(CObArray* pAr);

	int GetGroupCount();
	CDSCGroup* GetGroup(int nIndex);
	CDSCGroup* GetGroupData(int nIP);
	CDSCGroup* GetDSCBaseGroup();
	int GetMarginX();
	int GetMarginY();
	void SetMargin(vector<DscAdjustInfo*>* pArrDscInfo);
	void SetMarginX(int nMarginX);
	void SetMarginY(int nMarginY);

	//wgkim 190527
	void SetMargin(CRect rtMarginRect);

	//----------------------------------------
	//-- Event DSC
	//----------------------------------------	
	void DoMakeMovie3D();	
	IplImage* Create3DPicture(CvSize sizeMovie, CString strPicLeft, CString strPicRight);
	void DoMoviePlay();	

	//-- Change Property
	void SetFocusMode(BOOL bAF);
	void SetFullHDMode();			//-- 1920 X 1080 FULL HD Mode
	void SetGroupProperty(ESMEvent* pMsg);
	void SetGroupHiddenCommand(ESMEvent* pMsg);
	void FormatDeviceAll();
	void FWUpdateAll();				//-- 2014-9-4 hongsu@esmlab.com
	void MovieMakingStop();
	void DscDisconnect();
	void CaptureResume(int nResumeFrame);
	void CamShutDownAll();
	void ViewLarge(int nLarge);				//-- 2014-9-4 cdkim
	void InitMovieFile();

	void GetPropertyAll(int nProp, CString strDscID=_T(""));
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	//CMiLRe 20160204 Focus All Setting 시 Focus 저장되는 버그 수정
	void GetFocusAll(BOOL bSave = TRUE);
	void SetFocusAll();
	void SetFocus(CString strDSCId, int nMinFocus, int nSetFocus);
	void SetFocus(CString strDSCId, int nSetFocus);
	void SetFocusLoadFile(CString strPath);
	void SetHiddenCommandAll(ESMEvent* pMsg);
	void GetPropertyListView(CString strDscID = _T(""));
	void SetPropertyListView(ESMEvent* pMsg);

	//--  Camera Synchronization before Movie Capture
	void TickSyncAgent();
	BOOL GetPcSyncCheck();

	//-- Adjust - Position
	float m_nAdjPosition[DSC_ADJ_VAL_CNT];
	CvPoint2D32f m_ptLeftTop;
	CvPoint2D32f m_ptRightBottom;
	void DoAdjust();
	void SetDscAdjustData(vector<DscAdjustInfo*>* pArrDscInfo);
	void ReSetAdjustPosition();
	void OpenAdjustPage();
	void SetAdjustPosition(CDSCItem* pItem, CvPoint2D32f ptBase, CvPoint2D32f ptDetect);
	void SetMaxAngle();

	//-- Adjust - Color
	void SetAdjustColor(int nIndex, COLORREF colorBase, COLORREF colorTarget);

	//-- Line(Row)
	int GetSelectLine()			{ return m_nSelectLine; }
	void SetSelectLine(int n)	{ m_nSelectLine = n;	}

	//-- 2013-09-27 hongsu@esmlab.com
	//-- For Key Press Check
	BOOL GetKeyStatus(int nKey);

	//-- 2013-10-14 hongsu@esmlab.com
	//-- Load/Save Adjust Profile 
	void LoadAdjustProfile(CString strFileName);
	void SaveAdjustProfile(CString strFileName, void* pAdj = NULL);
	void SaveSyncTestFile(CString strFileName);

	//-- Load Files
	void LoadMovieFile(CString strID, int nIndex = -1);
	//-- Load Camera List
	void CameraListLoad();
	void CameraListReLoad();
	void GroupUpdateStatus(ESMEvent* pMsg);
	void GroupSyncNet(ESMEvent* pMsg);
	//-- Set Movie Save Check
	void SetMovieSaveCheck(bool bCheck);
	BOOL GetExecuteMovieMode();
	void SetExecuteMovieMode(BOOL bExecuteMovieMode);
	BOOL GetFocusState() { return m_bGetFocusState; }
	static unsigned WINAPI SetFocusThread(LPVOID param);
	//180423 hjcho
	CString GetDSCIDFromIndex(int nIdx);
public:	

	//-- Control DSC Info
	void ReDraw();	
	void OrganizeIndex();
	//------------------------------------------------------------------------------
	//! @function	Message
	//------------------------------------------------------------------------------
	void SetGridStatus(int nRow, int nStatus);
	


protected:
	// Generated message map functions
	//{{AFX_MSG(C4DMakerListView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	//}}AFX_MSG	

protected:
	DECLARE_MESSAGE_MAP()

	//jhhan 16-09-09
	CString m_strCompare[DSC_PROP_CNT];
public:
	void SaveDscInfoEx(CArchive& ar);
};
