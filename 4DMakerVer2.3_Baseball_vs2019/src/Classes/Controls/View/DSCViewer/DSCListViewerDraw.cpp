////////////////////////////////////////////////////////////////////////////////
//
//	DSCListViewer.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-05
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "DSCListViewer.h"
#include "DSCViewer.h"
#include "MainFrm.h" //2016/07/15

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------

void CDSCListViewer::OnDraw(CDC* pDC)
{
	//-- 2013-09-05 hongsu@esmlab.com
	//-- Draw Background
	CRect rect;
	GetClientRect(rect);
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
	pDC->FillSolidRect(rect, COLOR_BASIC_BG_5);
#else
	pDC->FillSolidRect(rect, RGB(0,0,0)/*COLOR_BASIC_BG_1*/);
#endif	
	//-- Draw Scroll 
	OnDrawScroll();
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Draw Info 
	OnDrawInfo();
	
//-- 2015-02-24 cygil@esmlab.com 4DModeler Adjust 사용안함
#ifndef _4DMODEL
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Draw Adjust
	OnDrawAdjust();
#endif
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Draw Property
	OnDrawProperty();

	//joonho.kim 170619
	//OnDrawTesting();

	//-- 2013-09-06 hongsu@esmlab.com
	//-- Draw Separator 
	OnDrawSeparator();

	//-- 2013-10-01 hongsu@esmlab.com
	//-- OnDrawBtn | m_BtnScaling
	OnDrawButton(pDC);
}





//------------------------------------------------------------------------------
//! @function	OnDrawAdjust		
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
BOOL CDSCListViewer::SetShowPage(int nBtn, BOOL bOpen)
{
	switch(nBtn)
	{
		case IDC_BTN_FOLDER_ADJ:	
			if(m_bShowAdj != bOpen)
			{
				m_bShowAdj = bOpen;
				return TRUE;				
			}
			break;
		case IDC_BTN_FOLDER_PROP:	
			if(m_bShowProp != bOpen)
			{
				m_bShowProp = bOpen;
				return TRUE;				
			}
	}
	return FALSE;
}

//------------------------------------------------------------------------------
//! @function	OnDrawInfo		
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCListViewer::OnDrawInfo()
{


	//-- 2013-09-05 hongsu@esmlab.com
	//-- Draw Each DSC Info
	CDSCItem* pDSCItem = NULL;
	int nAll = GetItemCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = GetItemData(i);
		if(!pDSCItem)
			continue;
		//-- 2013-09-06 hongsu@esmlab.com
		//-- Draw Info
		for(int n = 0 ; n < DSC_INFO_CNT; n ++)
		{
			if(!pDSCItem->m_pStaticInfo[n])
			{
				//-- Create
				pDSCItem->m_pStaticInfo[n] = new CESMEdit();
				pDSCItem->m_pStaticInfo[n]->Create(WS_CHILD | WS_VISIBLE | DT_CENTER | SS_CENTERIMAGE, CRect(0,0,0,0), this, IDC_DSC_VIEW_ITEM);				
				pDSCItem->m_pStaticInfo[n]->SetReadOnly();

				//if(pDSCItem->m_bReverse)
				//{
				//	//pDSCItem->m_pStaticInfo[n]->SetColorFg(COLOR_REVERSE_FG);
				//	//pDSCItem->m_pStaticInfo[n]->SetColorBg(COLOR_REVERSE_BG);
				//	pDSCItem->m_pStaticInfo[n]->ChangeFontItalic(DSC_TEXT_H, DSC_FONT-1,FW_BOLD,TRUE);
				//}
				//else
				pDSCItem->m_pStaticInfo[n]->ChangeFont(DSC_TEXT_H, DSC_FONT, FW_NORMAL, pDSCItem->GetTemperatureWarning());

				//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
				if(i%2)
					pDSCItem->m_pStaticInfo[n]->SetColors(COLOR_BASIC_FG_3,	//FG
					COLOR_BASIC_BG_5,	//BG
					COLOR_WHITE	,	//Hot FG
					COLOR_BASIC_HBG	);	//Hot BG
				else
					pDSCItem->m_pStaticInfo[n]->SetColors(COLOR_BASIC_FG_4,	//FG
					COLOR_BASIC_BG_4,	//BG
					COLOR_WHITE	,	//Hot FG
					COLOR_BASIC_HBG	);	//Hot BG
#else
				if(i%2)	pDSCItem->m_pStaticInfo[n]->SetColors(COLOR_BASIC_FG_1, COLOR_BASIC_BG_1, COLOR_BASIC_HFG, COLOR_BASIC_HBG	);	//Hot BG
				else	pDSCItem->m_pStaticInfo[n]->SetColors(COLOR_BASIC_FG_2, COLOR_BASIC_BG_2, COLOR_BASIC_HFG, COLOR_BASIC_HBG	);	//Hot BG

#endif
				pDSCItem->m_pStaticInfo[n]->SetPos(CPoint(n,i));
			}
			else
			{
				pDSCItem->m_pStaticInfo[n]->ReChangeFont(DSC_TEXT_H, DSC_FONT, FW_NORMAL, pDSCItem->GetTemperatureWarning());
			}
			//-- DrawText
			pDSCItem->m_pStaticInfo[n]->bFlag = pDSCItem->m_bDelete;
			if (n == DSC_INFO_ID && pDSCItem->m_bReverse)
				pDSCItem->m_pStaticInfo[n]->SetWindowText(_T("*") + pDSCItem->GetDSCInfo(n) + _T("*"));
			else
				pDSCItem->m_pStaticInfo[n]->SetWindowText(pDSCItem->GetDSCInfo(n));

			if(i*DSC_ROW_H < m_nScroll)
			{
				pDSCItem->m_pStaticInfo[n]->ShowWindow(SW_HIDE);
			}
			else
			{
				pDSCItem->m_pStaticInfo[n]->ShowWindow(SW_SHOW);
				switch(n)
				{
				case DSC_INFO_NUM		:	pDSCItem->m_pStaticInfo[n]->MoveWindow(DSC_BTN_W										,DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + i*DSC_ROW_H - m_nScroll, DSC_INFO_NUM_W			,DSC_ROW_H);	break;
				case DSC_INFO_LOCATION	:	pDSCItem->m_pStaticInfo[n]->MoveWindow(DSC_BTN_W+DSC_INFO_NUM_W							,DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + i*DSC_ROW_H - m_nScroll, DSC_INFO_LOCATION_W	,DSC_ROW_H);	break;
				case DSC_INFO_ID		:	pDSCItem->m_pStaticInfo[n]->MoveWindow(DSC_BTN_W+DSC_INFO_NUM_W + DSC_INFO_LOCATION_W	,DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + i*DSC_ROW_H - m_nScroll, DSC_INFO_ID_W			,DSC_ROW_H);	break;
				case DSC_INFO_STATUS	:	pDSCItem->m_pStaticInfo[n]->MoveWindow(DSC_BTN_W+DSC_INFO_W - DSC_INFO_STATUS_W			,DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + i*DSC_ROW_H - m_nScroll, DSC_INFO_STATUS_W		,DSC_ROW_H);	break;				
				}
			}

		}
	}

	//-- 2013-09-05 hongsu@esmlab.com
	//-- Draw Column Header
	for(int n = 0 ; n < DSC_INFO_CNT; n ++)
	{
		if(!m_ColHDInfo[n])
		{
			//-- Create
			if(n == DSC_INFO_ID)
				m_ColHDInfo[n].Create(NULL,WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_BTN_HEADER_ID);
			else
				m_ColHDInfo[n].Create(NULL,WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);

			m_ColHDInfo[n].ChangeFont(DSC_TEXT_H, DSC_FONT, FW_HEAVY);
			//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
			m_ColHDInfo[n].SetColors(	COLOR_BASIC_FG_3,	//FG
				COLOR_BASIC_BG_5,	//BG
				COLOR_WHITE	,	//Hot FG
				COLOR_BASIC_HBG	);	//Hot BG	
#else
			m_ColHDInfo[n].SetColors(RGB(0xFF,0xFF,0xFF)/*COLOR_BASIC_FG_1*/,	//FG
				RGB(0,0,0)/*COLOR_BASIC_BG_1*/,	//BG
				COLOR_BASIC_HFG	,	//Hot FG
				COLOR_BASIC_HBG	);	//Hot BG	
#endif
		}
		if(m_ColHDInfo[n])
		{
			//-- DrawText
			m_ColHDInfo[n].SetWindowText(m_strHDInfo[n]);
			switch(n)
			{
			case DSC_INFO_NUM		:	m_ColHDInfo[n].MoveWindow(DSC_BTN_W										,DSC_IMAGE_EDIT_TIMELINE_H ,DSC_INFO_NUM_W		,DSC_HEADER_H);	break;
			case DSC_INFO_LOCATION	:	m_ColHDInfo[n].MoveWindow(DSC_BTN_W+DSC_INFO_NUM_W						,DSC_IMAGE_EDIT_TIMELINE_H ,DSC_INFO_LOCATION_W	,DSC_HEADER_H);	break;
			case DSC_INFO_ID		:	m_ColHDInfo[n].MoveWindow(DSC_BTN_W+DSC_INFO_NUM_W + DSC_INFO_LOCATION_W	,DSC_IMAGE_EDIT_TIMELINE_H ,DSC_INFO_ID_W		,DSC_HEADER_H);	break;
			case DSC_INFO_STATUS	:	m_ColHDInfo[n].MoveWindow(DSC_BTN_W+DSC_INFO_W - DSC_INFO_STATUS_W		,DSC_IMAGE_EDIT_TIMELINE_H ,DSC_INFO_STATUS_W	,DSC_HEADER_H);	break;				
			}
		}
	}	

	//-- Bottom
	m_nDrawRight	= DSC_BTN_W+DSC_INFO_W;
	m_nDrawBottom	= DSC_HEADER_H + nAll*DSC_ROW_H;

	
}

void CDSCListViewer::OnDrawScroll()
{
	//-- Folder String
	CString strSign;
	strSign = _T("=");

	if(!m_BtnScroll)
	{
		m_BtnScroll.Create(NULL, WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_BTN_SCROLL);
		m_BtnScroll.ChangeFont(DSC_TEXT_H, DSC_FONT);
		m_BtnScroll.SetColors(	COLOR_BASIC_HFG,	//FG
			COLOR_BASIC_HFG,	//BG
			COLOR_BASIC_FG_1,	//Hot FG
			COLOR_BASIC_FG_1);	//Hot BG
		/*m_BtnScroll.SetGradientColors(	255, COLOR_BASIC_BG_1,	// default upper color
			255, COLOR_BASIC_BG_2,	// default lower color
			255, COLOR_BASIC_FG_1);	// default lower HOT color*/
	};
	m_BtnScroll.SetWindowText(strSign);	
	m_BtnScroll.MoveWindow(0	, DSC_IMAGE_EDIT_TIMELINE_H+m_nScroll , DSC_BTN_W	,100);

	m_nDrawRight	+= DSC_BTN_W;
}


void CDSCListViewer::OnDrawAdjust()
{
	//-- Folder String
	CString strSign;
	if(m_bShowAdj)
		strSign = _T("<");
	else
		strSign = _T(">");

	//-- 2013-09-06 hongsu@esmlab.com
	//-- Draw Spread/Fold Button
	if(!m_BtnFolderAdj)
	{
		m_BtnFolderAdj.Create(NULL, WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_BTN_FOLDER_ADJ);
		m_BtnFolderAdj.ChangeFont(DSC_TEXT_H, DSC_FONT);
		m_BtnFolderAdj.SetColors(	COLOR_BASIC_HFG,	//FG
									COLOR_BASIC_HFG,	//BG
									COLOR_BASIC_FG_1,	//Hot FG
									COLOR_BASIC_FG_1);	//Hot BG
		m_BtnFolderAdj.SetGradientColors(	255, RGB(0,0,0)/*COLOR_BASIC_BG_1*/,	// default upper color
											255, RGB(44,44,44)/*COLOR_BASIC_BG_2*/,	// default lower color
											255, COLOR_BASIC_FG_1);	// default lower HOT color
	};
	m_BtnFolderAdj.SetWindowText(strSign);	
	m_BtnFolderAdj.MoveWindow(m_nDrawRight	, DSC_IMAGE_EDIT_TIMELINE_H , DSC_BTN_W	,m_nDrawBottom);
	
	
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Set Right Position 
	m_nDrawRight	+= DSC_BTN_W;
	//-- Draw Adjust Items
	for(int n = 0 ; n < DSC_ADJ_CNT; n ++)
	{
		if(!m_ColHDAdj[n])
		{
			m_ColHDAdj[n].Create(NULL,WS_CHILD | WS_VISIBLE | DT_CENTER, CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);
			m_ColHDAdj[n].SetWindowText(m_strHDAdj[n]);
			m_ColHDAdj[n].ChangeFont(DSC_TEXT_H, DSC_FONT, FW_HEAVY);
			m_ColHDAdj[n].SetColors(	COLOR_BASIC_FG_1,	//FG
				COLOR_BASIC_BG_1,	//BG
				COLOR_BASIC_HFG	,	//Hot FG
				COLOR_BASIC_HBG	);	//Hot BG
		}		
		m_ColHDAdj[n].MoveWindow(m_nDrawRight + n*DSC_ADJ_W , DSC_IMAGE_EDIT_TIMELINE_H , DSC_ADJ_W, DSC_HEADER_H);;		
		//-- Show/Hide
		m_ColHDAdj[n].ShowWindow(m_bShowAdj);
	}	
	//-- 2013-09-05 hongsu@esmlab.com
	//-- Draw Each DSC Info
	CDSCItem* pDSCItem = NULL;
	CString strVal;
	int nAll = GetItemCount();
	
	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = GetItemData(i);
		//-- 2013-09-06 hongsu@esmlab.com
		//-- Draw Info
		for(int n = 0 ; n < DSC_ADJ_CNT; n ++)
		{
			if(!pDSCItem->m_pStaticAdj[n])
			{
				strVal.Format(_T("%.4f"),pDSCItem->m_nAdj[n]);
				pDSCItem->m_pStaticAdj[n] = new CESMEdit();
				pDSCItem->m_pStaticAdj[n]->Create(WS_CHILD | WS_VISIBLE | DT_CENTER | SS_CENTERIMAGE, CRect(0,0,0,0), this, IDC_DSC_VIEW_ITEM);						
				pDSCItem->m_pStaticAdj[n]->SetWindowText(strVal);
				pDSCItem->m_pStaticAdj[n]->SetReadOnly();				
				pDSCItem->m_pStaticAdj[n]->ChangeFont(DSC_TEXT_H, DSC_FONT);	
				if(i%2)	pDSCItem->m_pStaticAdj[n]->SetColors(COLOR_BASIC_FG_1, COLOR_BASIC_BG_1, COLOR_BASIC_HFG, COLOR_BASIC_HBG	);	//Hot BG
				else	pDSCItem->m_pStaticAdj[n]->SetColors(COLOR_BASIC_FG_2, COLOR_BASIC_BG_2, COLOR_BASIC_HFG, COLOR_BASIC_HBG	);	//Hot BG
				pDSCItem->m_pStaticAdj[n]->SetPos(CPoint(DSC_INFO_CNT+n,i));				
			}
			//-- MOVE WINDOWS
			if(i*DSC_ROW_H < m_nScroll)
			{
				pDSCItem->m_pStaticAdj[n]->ShowWindow(SW_HIDE);
			}
			else
			{
				pDSCItem->m_pStaticAdj[n]->ShowWindow(m_bShowAdj);
				pDSCItem->m_pStaticAdj[n]->MoveWindow(m_nDrawRight + n*DSC_ADJ_W , DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + i*DSC_ROW_H - m_nScroll, DSC_ADJ_W	,DSC_ROW_H);
			}
			//-- Show/Hide
			//pDSCItem->m_pStaticAdj[n]->ShowWindow(m_bShowAdj);		
		}		
	}	

	if(m_bShowAdj)
		m_nDrawRight += DSC_ADJ_CNT*DSC_ADJ_W;
}

void CDSCListViewer::ReDrawTesting()
{
	CDSCItem* pDSCItem = NULL;
	CString strVal;
	int nAll = GetItemCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = GetItemData(i);
		for(int n = 0 ; n < TESTING_PROP_CNT; n ++)
		{
			strVal.Format(_T("%.2f"),pDSCItem->m_nTesting[n]);
			pDSCItem->m_pStaticTesting[n]->SetWindowText(strVal);
		}		
	}	
}

void CDSCListViewer::OnDrawTesting()
{
	//-- Folder String
	CString strSign;
	if(m_bShowTesting)
		strSign = _T("<");
	else
		strSign = _T(">");

	//-- Draw Spread/Fold Button
	if(!m_BtnTesting)
	{
		m_BtnTesting.Create(NULL, WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_BTN_TESTING);
		m_BtnTesting.ChangeFont(DSC_TEXT_H, DSC_FONT);
		m_BtnTesting.SetColors(	COLOR_BASIC_HFG,	//FG
			COLOR_BASIC_HFG,	//BG
			COLOR_BASIC_FG_1,	//Hot FG
			COLOR_BASIC_FG_1);	//Hot BG
		m_BtnTesting.SetGradientColors(	255, RGB(0,0,0)/*COLOR_BASIC_BG_1*/,	// default upper color
			255, RGB(44,44,44)/*COLOR_BASIC_BG_2*/,	// default lower color
			255, COLOR_BASIC_FG_1);	// default lower HOT color
	};
	m_BtnTesting.SetWindowText(strSign);	
	m_BtnTesting.MoveWindow(m_nDrawRight	, DSC_IMAGE_EDIT_TIMELINE_H , DSC_BTN_W	,m_nDrawBottom);

	m_nDrawRight	+= DSC_BTN_W;
	//-- Draw Prop Items
	for(int n = 0 ; n < TESTING_PROP_CNT; n ++)
	{
		if(!m_ColTesting[n])
		{
			m_ColTesting[n].Create(NULL,WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);
			m_ColTesting[n].SetWindowText(m_strTesting[n]);
			m_ColTesting[n].ChangeFont(DSC_TEXT_H, DSC_FONT, FW_HEAVY);
			//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경

			m_ColTesting[n].SetColors(	COLOR_BASIC_FG_1,	//FG
				COLOR_BASIC_BG_1,	//BG
				COLOR_BASIC_HFG	,	//Hot FG
				COLOR_BASIC_HBG	);	//Hot BG	
		}		
		m_ColTesting[n].MoveWindow(m_nDrawRight + n*DSC_TESTING_W , DSC_IMAGE_EDIT_TIMELINE_H , DSC_TESTING_W, DSC_HEADER_H);
		//-- Show/Hide
		m_ColTesting[n].ShowWindow(m_bShowTesting);		
	}

	CDSCItem* pDSCItem = NULL;
	CString strVal;
	int nAll = GetItemCount();

	if(!ESMGetLoadRecord())
	{
		for(int i = 0 ; i < nAll ; i ++)
		{
			pDSCItem = GetItemData(i);
			for(int n = 0 ; n < TESTING_PROP_CNT; n ++)
			{
				if(!pDSCItem->m_pStaticTesting[n])
				{
					strVal.Format(_T("%.2f"),pDSCItem->m_nTesting[n]);
					pDSCItem->m_pStaticTesting[n] = new CESMEdit();
					pDSCItem->m_pStaticTesting[n]->Create(WS_CHILD | WS_VISIBLE | DT_CENTER | SS_CENTERIMAGE, CRect(0,0,0,0), this, IDC_DSC_VIEW_ITEM);						
					pDSCItem->m_pStaticTesting[n]->SetWindowText(strVal);
					pDSCItem->m_pStaticTesting[n]->SetReadOnly();				
					pDSCItem->m_pStaticTesting[n]->ChangeFont(DSC_TEXT_H, DSC_FONT);	
					if(i%2)
						pDSCItem->m_pStaticTesting[n]->SetColors(COLOR_BASIC_FG_1,	//FG
						COLOR_BASIC_BG_1,	//BG
						COLOR_BASIC_HFG	,	//Hot FG
						COLOR_BASIC_HBG	);	//Hot BG
					else
						pDSCItem->m_pStaticTesting[n]->SetColors(COLOR_BASIC_FG_2,	//FG
						COLOR_BASIC_BG_2,	//BG
						COLOR_BASIC_HFG	,	//Hot FG
						COLOR_BASIC_HBG	);	//Hot BG
					pDSCItem->m_pStaticTesting[n]->SetPos(CPoint(TESTING_PROP_CNT+n,i));				
				}
				//-- MOVE WINDOWS
				if(i*DSC_ROW_H < m_nScroll)
				{
					pDSCItem->m_pStaticTesting[n]->ShowWindow(SW_HIDE);
				}
				else
				{
					pDSCItem->m_pStaticTesting[n]->ShowWindow(m_bShowTesting);
					pDSCItem->m_pStaticTesting[n]->MoveWindow(m_nDrawRight + n*DSC_TESTING_W , DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + i*DSC_ROW_H - m_nScroll, DSC_TESTING_W	,DSC_ROW_H);
				}
				//-- Show/Hide
				//pDSCItem->m_pStaticAdj[n]->ShowWindow(m_bShowAdj);		
			}		
		}	
	}
	

	if(m_bShowTesting)
		m_nDrawRight += TESTING_PROP_CNT*DSC_TESTING_W;
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCListViewer::OnDrawProperty()
{
	//-- Folder String
	CString strSign;
	if(m_bShowProp)
		strSign = _T("<");
	else
		strSign = _T(">");

	//-- 2013-09-06 hongsu@esmlab.com
	//-- Draw Spread/Fold Button
	if(!m_BtnFolderProp)
	{
		m_BtnFolderProp.Create(NULL, WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_BTN_FOLDER_PROP);
		m_BtnFolderProp.ChangeFont(DSC_TEXT_H, DSC_FONT);
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
		m_BtnFolderProp.SetColors(	COLOR_BASIC_HFG,	//FG
			COLOR_WHITE,	//BG
			COLOR_BASIC_FG_3,	//Hot FG
			COLOR_BASIC_FG_3);	//Hot BG
		m_BtnFolderProp.SetGradientColors(	255, COLOR_BASIC_BG_5,	// default upper color
			255, COLOR_BASIC_BG_6,	// default lower color
			255, COLOR_BASIC_FG_1);	// default lower HOT color		
#else
		m_BtnFolderProp.SetColors(	COLOR_BASIC_HFG,	//FG
			COLOR_BASIC_HFG,	//BG
			COLOR_BASIC_FG_1,	//Hot FG
			COLOR_BASIC_FG_1);	//Hot BG
		m_BtnFolderProp.SetGradientColors(	255, RGB(0,0,0)/*COLOR_BASIC_BG_1*/,	// default upper color
			255, RGB(44,44,44)/*COLOR_BASIC_BG_2*/,	// default lower color
			255, COLOR_BASIC_FG_1);	// default lower HOT color	
#endif
	};

	m_BtnFolderProp.MoveWindow(m_nDrawRight	, DSC_IMAGE_EDIT_TIMELINE_H , DSC_BTN_W	,m_nDrawBottom);
	m_BtnFolderProp.SetWindowText(strSign);	

	//-- 2013-09-06 hongsu@esmlab.com
	//-- Set Right Position 
	m_nDrawRight	+= DSC_BTN_W;

	//-- Draw Prop Items
	for(int n = 0 ; n < DSC_PROP_CNT; n ++)
	{
		if(!m_ColHDProp[n])
		{
			m_ColHDProp[n].Create(NULL,WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);
			m_ColHDProp[n].SetWindowText(m_strHDProp[n]);
			m_ColHDProp[n].ChangeFont(DSC_TEXT_H, DSC_FONT, FW_HEAVY);
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
			m_ColHDProp[n].SetColors(	COLOR_BASIC_FG_3,	//FG
				COLOR_BASIC_BG_5,	//BG
				COLOR_WHITE	,	//Hot FG
				COLOR_BASIC_HBG	);	//Hot BG			
#else
			m_ColHDProp[n].SetColors(	COLOR_BASIC_FG_1,	//FG
				COLOR_BASIC_BG_1,	//BG
				COLOR_BASIC_HFG	,	//Hot FG
				COLOR_BASIC_HBG	);	//Hot BG	
#endif
		}		
		m_ColHDProp[n].MoveWindow(m_nDrawRight + n*DSC_PROP_W , DSC_IMAGE_EDIT_TIMELINE_H , DSC_PROP_W, DSC_HEADER_H);
		//-- Show/Hide
		m_ColHDProp[n].ShowWindow(m_bShowProp);		
	}	

	//-- 2013-09-05 hongsu@esmlab.com
	//-- Draw Each DSC Info
	CDSCItem* pDSCItem = NULL;
	CString strVal;
	int nAll = GetItemCount();

	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = GetItemData(i);
		//-- 2013-09-06 hongsu@esmlab.com
		//-- Draw Info
		for(int n = 0 ; n < DSC_PROP_CNT; n ++)
		{
			if(!pDSCItem->m_pStaticProp[n])
			{
				pDSCItem->m_pStaticProp[n] = new CESMEdit();
				pDSCItem->m_pStaticProp[n]->Create(WS_CHILD | WS_VISIBLE | DT_CENTER | SS_CENTERIMAGE, CRect(0,0,0,0), this, IDC_DSC_VIEW_ITEM);
				pDSCItem->m_pStaticProp[n]->SetWindowText(pDSCItem->m_strProp[n]);
				pDSCItem->m_pStaticProp[n]->SetReadOnly();				
				pDSCItem->m_pStaticProp[n]->ChangeFont(DSC_TEXT_H, DSC_FONT);
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
				if(i%2)
					pDSCItem->m_pStaticProp[n]->SetColors(COLOR_BASIC_FG_3,	//FG
					COLOR_BASIC_BG_5,	//BG
					COLOR_WHITE	,	//Hot FG
					COLOR_BASIC_HBG	);	//Hot BG
				else
					pDSCItem->m_pStaticProp[n]->SetColors(COLOR_BASIC_FG_4,	//FG
					COLOR_BASIC_BG_4,	//BG
					COLOR_WHITE	,	//Hot FG
					COLOR_BASIC_HBG	);	//Hot BG
#else
				if(i%2)
					pDSCItem->m_pStaticProp[n]->SetColors(COLOR_BASIC_FG_1,	//FG
					COLOR_BASIC_BG_1,	//BG
					COLOR_BASIC_HFG	,	//Hot FG
					COLOR_BASIC_HBG	);	//Hot BG
				else
					pDSCItem->m_pStaticProp[n]->SetColors(COLOR_BASIC_FG_2,	//FG
					COLOR_BASIC_BG_2,	//BG
					COLOR_BASIC_HFG	,	//Hot FG
					COLOR_BASIC_HBG	);	//Hot BG
#endif
				pDSCItem->m_pStaticProp[n]->SetPos(CPoint(DSC_INFO_CNT+DSC_ADJ_CNT+n,i));		
			}
			//-- MOVE WINDOWS
			if(i*DSC_ROW_H < m_nScroll)
			{
				pDSCItem->m_pStaticProp[n]->ShowWindow(SW_HIDE);
			}
			else
			{
				pDSCItem->m_pStaticProp[n]->ShowWindow(m_bShowProp);		
				pDSCItem->m_pStaticProp[n]->MoveWindow(m_nDrawRight + n*DSC_PROP_W , DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + i*DSC_ROW_H - m_nScroll, DSC_PROP_W	,DSC_ROW_H);
			}
			//-- Show/Hide
			
		}		
	}	

	if(m_bShowProp)
		m_nDrawRight += DSC_PROP_CNT*DSC_PROP_W;
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCListViewer::OnDrawSeparator()
{
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Draw Separator
	if(!m_StaticSeparator)
	{
		//-- Create
		m_StaticSeparator.Create(NULL, WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_BTN_FOLDER_PROP);
		m_StaticSeparator.SetColors(
			COLOR_SELECT_BG,	//FG
			COLOR_BASIC_BG_2,	//BG
			COLOR_SELECT_BG,	//Hot FG
			COLOR_SELECT_BG);	//Hot BG		
	};
	m_StaticSeparator.MoveWindow(m_nDrawRight, DSC_IMAGE_EDIT_TIMELINE_H, DSC_SEPARATOR_W , m_nDrawBottom );
	//-- Add Right Position
	m_nDrawRight += DSC_SEPARATOR_W;
}



//------------------------------------------------------------------------------
//! @function			
//! @brief		m_BtnScaling			
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//------------------------------------------------------------------------------
void CDSCListViewer::OnDrawButton(CDC* pDC)
{
	CRect rect = GetScaleBtnRect();
	if(m_pView->m_bScaleToWindow)
		pDC->FillSolidRect(rect,RGB(0xDF,0x59,0x3A)/*COLOR_YELLOW*/);

	pDC->SetBkColor(RGB(255,255,255));
	pDC->SetTextColor(RGB(0,0,0));
	pDC->DrawEdge(rect, EDGE_BUMP, BF_RECT);

	//SetFrameFont(pDC); //CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);
	SetFrameFont(pDC, 13, DSC_FONT);
	pDC->SetTextColor(RGB(0xFF,0xFF,0xFF)/*COLOR_YELLOW*/);
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
	pDC->SetBkColor(COLOR_BASIC_BG_5);
#else
	pDC->SetBkColor(RGB(0,0,0)/*COLOR_BASIC_BG_1*/);
#endif
	CRect rtTxt = rect;
	/*rtTxt.left	= rtTxt.right + 2;
	rtTxt.right	= rtTxt.left + 100;*/
	rtTxt.left	= rtTxt.left - 35;
	rtTxt.right	= rtTxt.right - 5;
	pDC->DrawText(_T("Scaling"), rtTxt, DT_TOP);

	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		CRect rect_1 = GetInsertIDBtnRect();
		if(m_pView->m_bInsertID)
			pDC->FillSolidRect(rect_1,RGB(0xDF,0x59,0x3A)/*COLOR_YELLOW*/);

		pDC->SetBkColor(RGB(255,255,255));
		pDC->SetTextColor(RGB(0,0,0));
		pDC->DrawEdge(rect_1, EDGE_BUMP, BF_RECT);


		pDC->SetTextColor(RGB(0xFF,0xFF,0xFF)/*COLOR_YELLOW*/);
		pDC->SetBkColor(RGB(0,0,0)/*COLOR_BASIC_BG_1*/);

		CRect rtTxt1 = rect;
		CString strPage;
		rtTxt1.left	= 50; //rtTxt1.right + 2;
		rtTxt1.right = 200;//rtTxt1.left - 200;
		//m_wndDSCViewer.m_pListView.m_strPage

		CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
		pDC->DrawText(_T(""), rtTxt1, DT_TOP);
		pDC->DrawText(pMainWnd->m_wndDSCViewer.m_pListView->m_strPage, rtTxt1, DT_TOP);

		CRect rtTxt2 = rect;
		//rtTxt2.left = 20;
		//rtTxt2.right = 40;
		rtTxt2.left = 5;
		rtTxt2.right = 20;
		pDC->DrawText(_T(""), rtTxt2, DT_TOP);
		pDC->DrawText(_T("ID"), rtTxt2, DT_TOP);

		CRect rtTxt3 = rect;
		rtTxt3.left = 200;
		rtTxt3.right = 300;
		pDC->DrawText(_T(""), rtTxt3, DT_TOP);
		pDC->DrawText(pMainWnd->m_wndDSCViewer.m_pListView->m_strDelay, rtTxt3, DT_TOP);

		rtTxt3.left = 75;
		rtTxt3.right = 200;
		pDC->DrawText(_T(""), rtTxt3, DT_TOP);
		CString strCapture;
		strCapture.Format(_T("-%s-"), pMainWnd->m_wndDSCViewer.m_pListView->m_strCapture);
		pDC->DrawText(strCapture, rtTxt3, DT_TOP);

		CRect rtTxt4 = rect;
		rtTxt4.left = 100;
		rtTxt4.right = 300;

		pDC->DrawText(_T(""), rtTxt4, DT_TOP);
		pDC->DrawText(pMainWnd->m_wndDSCViewer.m_pListView->m_AUTO, rtTxt4, DT_TOP);

		CRect rtTxt5 = rect;
		rtTxt5.left = 160;
		rtTxt5.right = 300;

		pDC->DrawText(_T(""), rtTxt5, DT_TOP);
		pDC->DrawText(pMainWnd->m_wndDSCViewer.m_pListView->m_FrameAdj, rtTxt5, DT_TOP);
	
	}
}

CRect CDSCListViewer::GetScaleBtnRect()
{
	CRect rect;
	GetClientRect(&rect);

	rect.top	= DSC_ICON_SCALE_H;	
	rect.bottom	= rect.top	+ DSC_ICON_SCALE_H;
	rect.left	= 270/*rect.right -= 60*//*DSC_ICON_SCALE_W*/;
	rect.right	= rect.left	+ DSC_ICON_SCALE_H;

	return rect;
}

CRect CDSCListViewer::GetInsertIDBtnRect()
{
	CRect rect;
	GetClientRect(&rect);

	rect.top	= DSC_ICON_SCALE_H;	
	rect.bottom	= rect.top	+ DSC_ICON_SCALE_H;
	
//	rect.left	= 0;
//	rect.right	= DSC_ICON_SCALE_H;
	rect.left	= 20;
	rect.right	= rect.left+DSC_ICON_SCALE_H;

	return rect;
}

