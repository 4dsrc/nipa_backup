////////////////////////////////////////////////////////////////////////////////
//
//	DSCItem.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCItem.h"
#include "SdiSingleMgr.h"
#include "resource.h"
#include "DSCFrameSelector.h"
#include "ESMIni.h"
#ifdef _4DMODEL
#include "4DModelerVersion.h"
#else
#include "4DMakerVersion.h"
#endif
#include "FFmpegManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_SERIAL(CDSCItem, CObject, 1)

CDSCItem::CDSCItem()
{
	m_nSyncCount = 0;
	m_nCheckCount = 0;
	m_nCheckTime = 0;

	//-- 2015-10-22 joonho.kim GOP
	m_nFileIndex = 0;
	m_bRecordStep = 0;//(CAMREVISION)
	m_nStatus = SDI_STATUS_UNKNOWN;
	m_pSdi = NULL;
	m_nTakenPictureTime = 0;
	m_bMovieSaveCheck = TRUE;
	m_bTickSuccess	  = FALSE;	
	m_bMovieCancel	 = FALSE;
	m_memImageDC	 = NULL;
	m_nVersion = -10.0;
	
	//-- 2013-09-29 hongsu@esmlab.com
	//-- For Message From Thread
	m_hParentWnd = ESMGetMainWnd();
	m_nCurFocus = 0;
	m_nMaxFocus = 0;
	m_nMinFocus = 0;
	m_nMovieTime= 0;
	m_nTickTime = 0;
	m_nMovieSize = 0;
	m_nMovieFileClose = 0;
	m_nSensorOnDesignation= 0;
	
	//-- 2013-10-19 hongsu@esmlab.com
	//-- Init Static
	InitStatic();

	//-- 2014-09-15 changdo
	m_bCaptureExcept = FALSE;
	m_bCaptureImageRecExcept = FALSE;

	//-- 2014-09-15 hongsu@esmlab.com
	//-- Sync DSC
	m_bSensorSync = TRUE;

	m_nDelayTime = 0;
	//m_bGetStartTime = FALSE;

	m_bDelete = FALSE;
	//-- Critical Section
	//InitializeCriticalSection (&_DSCItem);
	InitializeCriticalSectionAndSpinCount(&_DSCItem, 2000);

	m_pRCClient = NULL;

	m_nFocus = 0;
	m_bReverse = FALSE;

	m_nVertical = 0;

	m_bPreRecInitGH5 = FALSE;
	m_nCamVersion = -1;

	m_bReSyncGetTickReady = TRUE;
	m_bRecordReady = TRUE;

	m_bTemperatureWarning = FALSE;

	m_nResyncGetTickCount = 0;

	m_bDetect = FALSE;
}

//-- 2013-09-05 hongsu@esmlab.com
//-- sample 
CDSCItem::CDSCItem(CString strUSBID, int bREMode)
{
	m_nSyncCount = 0;
	m_nCheckCount = 0;
	m_nCheckTime = 0;

	//-- 2015-10-22 joonho.kim GOP
	m_nFileIndex = 0;
	m_bRecordStep = 0;//(CAMREVISION)
	//-- Get Info From MainFrame (Server/Agent)
	//-- MainPC -> Local, Agent -> Toss
	m_nType = bREMode; // 0 Local, 1 Toss, 2 Remote
	m_nRec	= DSC_REC_NONE;
	m_memImageDC	 = NULL;

	m_nStatus = SDI_STATUS_UNKNOWN;
	m_pSdi = NULL;
	m_nTakenPictureTime = 0;
	m_nRemoteID = 0;
	m_bMovieSaveCheck = TRUE;
	m_bTickSuccess	  = FALSE;	
	m_bMovieCancel	 = FALSE;

	//-- 2013-09-06 hongsu@esmlab.com
	m_strInfo[DSC_INFO_NUM		] = _T("");
	m_strInfo[DSC_INFO_LOCATION	] = GetStatusString();
	m_strInfo[DSC_INFO_ID		] = strUSBID;
	m_strInfo[DSC_INFO_STATUS	] = GetStatusString();
	m_nAdj[DSC_ADJ_X			] = 0;
	m_nAdj[DSC_ADJ_Y			] = 0;
	m_nAdj[DSC_ADJ_A			] = 0;	
	m_nAdj[DSC_ADJ_RX			] = 0;
	m_nAdj[DSC_ADJ_RY			] = 0;
	m_nAdj[DSC_ADJ_SCALE		] = 0;
	m_nAdj[DSC_ADJ_DISTANCE		] = 0;
	//m_nAdj[DSC_ADJ_REVERSE		] = 0;
	//m_strProp[DSC_PROP_FUNC		] = _T("Unknown");
	//jhhan 16-09-09
	//m_strProp[DSC_PROP_ZOOM		] = _T("Unknown");
	//m_strProp[DSC_PROP_FNUM		] = _T("Unknown");
	m_strProp[DSC_PROP_SS		] = _T("Unknown");
	m_strProp[DSC_PROP_ISO		] = _T("Unknown");
	m_strProp[DSC_PROP_MOVIE_SIZE] = _T("Unknown");
	//jhhan 16-09-09
	m_strProp[DSC_PROP_WB		] = _T("Unknown");
	m_strProp[DSC_PROP_COLOR_TEMPERATURE] = _T("Unknown");
	m_strProp[DSC_PROP_PW] = _T("Unknown");
//	m_strProp[DSC_PROP_PHOTO_STYLE] = _T("Unknown");

	m_nTesting[TESTING_PROP_TOTAL_CNT			] = 0;
	m_nTesting[TESTING_PROP_FAIL_CNT			] = 0;
	m_nTesting[TESTING_PROP_AS_CNT			] = 0;
	m_nTesting[TESTING_PROP_AS_AVG			] = 0;
	m_nTesting[TESTING_PROP_AS_MIN			] = 0;	
	m_nTesting[TESTING_PROP_AS_MAX			] = 0;
	m_nTesting[TESTING_PROP_AC_CNT			] = 0;
	m_nTesting[TESTING_PROP_AC_AVG			] = 0;
	m_nTesting[TESTING_PROP_AC_MIN			] = 0;
	m_nTesting[TESTING_PROP_AC_MAX			] = 0;

	m_nCurFocus = 0;
	m_nMaxFocus = 0;
	m_nMinFocus = 0;
	m_nMovieTime= 0;
	m_nWidth = 0;
	m_nHeight = 0;
	m_nTickTime = 0;
	m_nVersion = -10.0;
	m_nMovieSize = 0;
	m_nMovieFileClose = 0;
	m_nSensorOnDesignation= 0;
	//-- 2013-09-29 hongsu@esmlab.com
	//-- For Message From Thread
	m_hParentWnd = ESMGetMainWnd();

	//-- 2014-09-15 changdo
	m_bCaptureExcept = FALSE;
	m_bCaptureImageRecExcept = FALSE;

	//-- 2014-09-15 hongsu@esmlab.com
	//-- Sync DSC
	m_bSensorSync = TRUE;

	m_nDelayTime = 0;

	m_bDelete = FALSE;
//	m_bGetStartTime = FALSE;
	//-- 2013-10-19 hongsu@esmlab.com
	//-- Init Static
	InitStatic();

	//-- Critical Section
	//InitializeCriticalSection (&_DSCItem);
	InitializeCriticalSectionAndSpinCount(&_DSCItem, 2000);

	m_pRCClient = NULL;

	m_nFocus = 0;
	m_bReverse = FALSE;

	m_bPreRecInitGH5 = FALSE;
	m_nCamVersion = -1;

	m_bReSyncGetTickReady = TRUE;
	m_bRecordReady = TRUE;

	m_bTemperatureWarning = FALSE;

	m_nResyncGetTickCount = 0;

	m_bDetect = FALSE;
}

CDSCItem::~CDSCItem()
{

	if(m_pRCClient != NULL)
	{
		m_pRCClient->m_bThreadStop = TRUE;
		m_pRCClient->m_bConnected = FALSE;
		m_pRCClient->Disconnect();
		m_pRCClient->RemoveAllMsg();
		if(m_pRCClient)
		{
			delete m_pRCClient;
			m_pRCClient = NULL;
		}
	}

	if(m_pSdi)
	{
		delete m_pSdi;
		m_pSdi = NULL;
	}

	//-- 2013-10-19 hongsu@esmlab.com
	//-- Remove Static Edit
	RemoveStaticAll();

	if( m_memImageDC)
	{
		m_memImageDC->DeleteDC();
		delete m_memImageDC;
		m_memImageDC = NULL;
	}

	for(int n = 0 ; n < DSC_PROP_CNT; n ++)
	{
		if(m_pStaticProp[n])
		{
			delete m_pStaticProp[n];
			m_pStaticProp[n] = NULL;
		}
	}

	DeleteCriticalSection (&_DSCItem);
}

CString CDSCItem::GetStatusString(int nStatus)
{
	CString strStatus;
	switch(nStatus)
	{
	case SDI_STATUS_NORMAL		: strStatus.Format(_T("Normal")				); break;
	case SDI_STATUS_CONNECTED	: 
		strStatus.Format(_T("Connect")			); 
		break;		
	case SDI_STATUS_FOCUSSING	: strStatus.Format(_T("Focussing ...")		); break;
	case SDI_STATUS_FOCUS_OK	: strStatus.Format(_T("Focussed")			); break;
	case SDI_STATUS_FOCUS_FAIL	: strStatus.Format(_T("Focus Fail")			); break;
	case SDI_STATUS_CAPTURE		: strStatus.Format(_T("Capturing ...")		); break;
	case SDI_STATUS_GET_FILE	: strStatus.Format(_T("Saving File ...")	); break;
	case SDI_STATUS_CAPTURE_OK	: strStatus.Format(_T("Capture Success")	); break;
	case SDI_STATUS_REC			: 
		strStatus.Format(_T("Recording ...")		); 
		break;
	case SDI_STATUS_REC_CONVERT	: strStatus.Format(_T("File Converting ...")); break;
	case SDI_STATUS_REC_FINISH	: 
		strStatus.Format(_T("Ready to Edit")		); 
		break;	
	case SDI_STATUS_OPENSESSION	: strStatus.Format(_T("Try Connection ...") ); break;
	case SDI_STATUS_DISCONNECT	: strStatus.Format(_T("Disconnected")		); break;		
	case SDI_STATUS_LOAD		: strStatus.Format(_T("Load")				); break;
	case SDI_STATUS_ERROR_TICK	: strStatus.Format(_T("Error Get Tick Time")); break;
	case SDI_STATUS_REC_READY   : 
		strStatus.Format(_T("Ready to Record")); 
		break;
	case SDI_STATUS_ERROR   : 	//jhhan 180829
		strStatus.Format(_T("Error ...")); 
		break;
	case SDI_STATUS_UNKNOWN		: 
	default:					  strStatus.Format(_T("Unknown")			); break;
	}
	return strStatus;
}

void CDSCItem::SetDSCStatus(int nStatus)
{
	// [10/5/2013 Administrator]
	// Critical Section
	//kcd _DSCItem CriticalSection 다른 Funtion의 _DSCItem 충돌
	//EnterCriticalSection (&_DSCItem);

	//jhhan 180829
	if(nStatus == SDI_STATUS_REC_FINISH)
	{
		if(m_nStatus != SDI_STATUS_REC)
			return;
	}

	m_nStatus = nStatus;
	SetDSCInfo(DSC_INFO_STATUS,GetStatusString());

	if(m_pStaticInfo[DSC_INFO_STATUS])
	{
		m_pStaticInfo[DSC_INFO_STATUS]->SetWindowText(GetDSCInfo(DSC_INFO_STATUS));
		m_pStaticInfo[DSC_INFO_STATUS]->Invalidate();
	}
	if( nStatus == SDI_STATUS_CONNECTED)
		SetCaptureExcept(FALSE);
	//-- 2013-09-12 hongsu@esmlab.com
	//-- Set Select Picture Time 
	m_nTakenPictureTime = 0;
	//LeaveCriticalSection (&_DSCItem);
	if(m_nType == DSC_LOCAL)
	{
		ESMEvent* pMsg = new ESMEvent();
		//-- Release Focusing
		pMsg->message	= WM_RS_RC_STATUS_CHANGE;
		pMsg->nParam1	= nStatus;
		pMsg->pDest		= (LPARAM)this;
		::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}
	else
	{
		if( nStatus == SDI_STATUS_REC_FINISH && ESMGetExecuteMode() == TRUE)
		{
			int nRecTime = ESMGetRecTime();
			SetSavedLastTime(nRecTime);
		}
	}
}

BOOL CDSCItem::CheckDSCStatus(int nStatus)
{
	if(GetDSCInfo(DSC_INFO_STATUS) == GetStatusString(nStatus))
		return TRUE;
	return FALSE;
}

void CDSCItem::SetSDIMgr(CSdiSingleMgr* pMgr)
{
	m_pSdi = pMgr;
	m_pSdi->SetParent(this);

	SetDSCStatus(SDI_STATUS_CONNECTED);

	m_nType = DSC_LOCAL;
	//-- 2013-09-06 hongsu@esmlab.com
	if(!GetDSCInfo(DSC_INFO_NUM).GetLength())
		SetDSCInfo(DSC_INFO_NUM,_T(""));

	SetDSCInfo(DSC_INFO_LOCATION	, m_pSdi->GetDeviceLocation());
	SetDSCInfo(DSC_INFO_ID			, m_pSdi->GetDeviceDSCID());
	SetDSCInfo(DSC_INFO_STATUS		, GetStatusString());
	SetDSCInfo(DSC_INFO_UNIQUEID	, m_pSdi->GetDeviceUniqueID());
	SetDSCInfo(DSC_INFO_MODEL		, m_pSdi->GetDeviceModel());
	m_nAdj[DSC_ADJ_X			] = 0;
	m_nAdj[DSC_ADJ_Y			] = 0;
	m_nAdj[DSC_ADJ_A			] = 0;
	m_nAdj[DSC_ADJ_RX			] = 0;
	m_nAdj[DSC_ADJ_RY			] = 0;
	m_nAdj[DSC_ADJ_SCALE		] = 0;
	m_nAdj[DSC_ADJ_DISTANCE		] = 0;
//	m_nAdj[DSC_ADJ_REVERSE		] = 0;
	//m_strProp[DSC_PROP_FUNC		] = _T("Unknown");
	//jhhan 16-09-09
	//m_strProp[DSC_PROP_ZOOM		] = _T("Unknown");
	//m_strProp[DSC_PROP_FNUM		] = _T("Unknown");
	m_strProp[DSC_PROP_SS		] = _T("Unknown");
	m_strProp[DSC_PROP_ISO		] = _T("Unknown");
	m_strProp[DSC_PROP_MOVIE_SIZE] = _T("Unknown");

	//jhhan 16-09-09
	m_strProp[DSC_PROP_WB		] = _T("Unknown");
	m_strProp[DSC_PROP_COLOR_TEMPERATURE] = _T("Unknown");
	m_strProp[DSC_PROP_PW] = _T("Unknown");
//	m_strProp[DSC_PROP_PHOTO_STYLE] = _T("Unknown");

	int n;
	for(n = 0 ; n < DSC_INFO_CNT; n ++)	{	if(m_pStaticInfo[n])	{ m_pStaticInfo[n]->SetWindowText(GetDSCInfo(n));	m_pStaticInfo[n]->Invalidate();	}}
	for(n = 0 ; n < DSC_ADJ_CNT; n ++)	{	if(m_pStaticAdj[n])		{													m_pStaticAdj[n]->Invalidate();	}}
	for(n = 0 ; n < DSC_PROP_CNT; n ++)	{	if(m_pStaticProp[n])	{ m_pStaticProp[n]->SetWindowText(m_strProp[n]);	m_pStaticProp[n]->Invalidate();	}}

	if(m_nType == DSC_LOCAL) // true TOSS
	{
		ESMEvent* pMsg = new ESMEvent();
		pMsg->message	= WM_RS_RC_NETWORK_CONNECT;
		pMsg->pDest		= (LPARAM)this;
		::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}
	LoadFocus();
}

void CDSCItem::DeleteSdiMgr()
{
	EnterCriticalSection (&_DSCItem);
	if( m_pSdi )
	{
		if(m_pSdi->m_bCloseSession == TRUE)
		{
			m_pSdi = NULL;
			LeaveCriticalSection (&_DSCItem);
			return;
		}

		m_pSdi->m_bCloseSession = TRUE;

		delete m_pSdi;
		m_pSdi = NULL;
	}
	LeaveCriticalSection (&_DSCItem);
}

void CDSCItem::SetIndex(int nIndex)
{
	//ESMLog(1,_T("##########%s Set Index = %d"),GetDeviceDSCID(), nIndex);
	m_nIndex = nIndex;
}

int CDSCItem::GetIndex()
{
	return m_nIndex;
}

void CDSCItem::SetLiveview(CDSCItem* pItem, BOOL b)
{
	if(pItem) 
	{
		if(pItem->GetType() == DSC_REMOTE)
		{
			ESMEvent* pMsg = new ESMEvent();
			pMsg->message	= WM_RS_RC_LIVEVIEW;
			pMsg->nParam1	= b;
			pMsg->pDest		= (LPARAM)this;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
		else
		{
			//-- 2013-10-07 hongsu@esmlab.com
			//-- Check Null Pointer 
			if(pItem->m_pSdi)
			{
				pItem->m_pSdi->SetLiveview(b);

				if(b && pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
				{
					ESMEvent* pMsg = new ESMEvent();
					pMsg->message	= WM_SDI_OP_GET_FOCUS_FRAME;
					SdiAddMsg(pMsg);
				}
			}
		}
	}
}

void CDSCItem::InitLiveView()
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message	= WM_RS_RC_LIVEVIEW;
	pMsg->nParam1	= FALSE;
	pMsg->pDest		= (LPARAM)this;
	::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}

void CDSCItem::SetInitPreRec( BOOL b )
{
	if (m_pSdi)
	{
		m_pSdi->SetInitPreRec(b);
	}
	m_bPreRecInitGH5 = b;
}

BOOL CDSCItem::GetInitPreRec()
{
	if (m_pSdi)
	{
		return m_pSdi->GetInitPreRec();
	}

	return m_bPreRecInitGH5;
}

void CDSCItem::SetCamVersion( int nVer )
{
	m_nCamVersion = nVer;
}

int CDSCItem::GetCamVersion()
{
	return m_nCamVersion;
}

void CDSCItem::ChangeRow(int nLine)
{
	int n;
	for(n = 0 ; n < DSC_INFO_CNT; n ++)		m_pStaticInfo[n]->ChangeRow(nLine);
	for(n = 0 ; n < DSC_ADJ_CNT; n ++)		m_pStaticAdj[n]->ChangeRow(nLine);
	for(n = 0 ; n < DSC_PROP_CNT; n ++)		m_pStaticProp[n]->ChangeRow(nLine);
	for(n = 0 ; n < TESTING_PROP_CNT; n ++)		m_pStaticTesting[n]->ChangeRow(nLine);
}

void CDSCItem::InitStatic()
{
	int n;
	for(n = 0 ; n < DSC_INFO_CNT; n ++)		m_pStaticInfo[n] = NULL	;
	for(n = 0 ; n < DSC_ADJ_CNT; n ++)		m_pStaticAdj[n] = NULL	;
	for(n = 0 ; n < DSC_PROP_CNT; n ++)		m_pStaticProp[n] = NULL	;
	for(n = 0 ; n < TESTING_PROP_CNT; n ++)		m_pStaticTesting[n] = NULL	;
}

void CDSCItem::RemoveStaticAll()
{
	int n;
	for(n = 0 ; n < DSC_INFO_CNT; n ++)		if(m_pStaticInfo[n]	) {delete m_pStaticInfo[n]; m_pStaticInfo[n] = NULL;}
	for(n = 0 ; n < DSC_ADJ_CNT; n ++)		if(m_pStaticAdj[n]	) {delete m_pStaticAdj[n];  m_pStaticAdj[n]  = NULL;}
	for(n = 0 ; n < DSC_PROP_CNT; n ++)		if(m_pStaticProp[n]	) {delete m_pStaticProp[n]; m_pStaticProp[n] = NULL;}
	for(n = 0 ; n < TESTING_PROP_CNT; n ++)		if(m_pStaticTesting[n]	) {delete m_pStaticTesting[n]; m_pStaticTesting[n] = NULL;}
}

void CDSCItem::ResetSyncDataAll()
{
	int n;
	for(n = 0 ; n < TESTING_PROP_CNT; n ++)		{m_nTesting[n] = 0;}
	m_nCheckTime = 0;
}

//------------------------------------------------------------------------------
//! @function	Recording Status
//! @brief				
//! @date		2013-09-12
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCItem::SetDSCRecStatus(int nRec)
{
	// [10/5/2013 Administrator]
	// Critical Section
	EnterCriticalSection (&_DSCItem);

	if(nRec == DSC_REC_BTN)
	{
		if(m_nRec == DSC_REC_ON	) m_nRec = DSC_REC_STOP;
		else if(m_nRec == DSC_REC_STOP || m_nRec == DSC_REC_NONE	) m_nRec = DSC_REC_ON;
	}
	else
		m_nRec = nRec;	

	LeaveCriticalSection (&_DSCItem);
}

//------------------------------------------------------------------------------
//! @function	Serialize
//! @date		2013-09-12
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCItem::Serialize( CArchive& ar, stAdjustInfo* pAdj)
{
	
	CObject::Serialize( ar );
	int i;
	//-- 2013-10-02 hongsu@esmlab.com
	//-- SAVE
	int nType = DSC_REMOTE;
	if (ar.IsStoring())
	{	
		ar<<nType;
		for( i = 0 ; i < DSC_INFO_CNT ; i++ )
			ar<<m_strInfo[i];
		for( i = 0 ; i < DSC_ADJ_CNT ; i++ )
		{
			if(pAdj)
			{
				switch(i)
				{
				case DSC_ADJ_X:	ar<<(float)pAdj->AdjMove.x; break;
				case DSC_ADJ_Y: ar<<(float)pAdj->AdjMove.y; break;
				case DSC_ADJ_A: ar<<(float)pAdj->AdjAngle; break;
				default:		ar<<m_nAdj[i]; break;
				}
			}
			else
				ar<<m_nAdj[i];
		}

#if 0
		for( i = 0 ; i < DSC_PROP_CNT ; i++ )
			ar<<m_strProp[i];
#endif

		ar<<m_nHeight;
		ar<<m_nWidth;

		CString strVal;
		//180313 hjcho
		if(m_bReverse)
		{
			strVal.Format(_T("REVERSE"));
			ar<<strVal;
		}
		else
		{
			strVal.Format(_T("NORMAL"));
			ar<<strVal;
		}

		//wgkim 190527
		ar << m_rtMargin;
	}

	//-- 2013-10-02 hongsu@esmlab.com
	//-- LOAD
	else
	{
		ar>>m_nType;
		CString strTp;
		for( i = 0 ; i < DSC_INFO_CNT ; i++ )
		{
			ar>>strTp;
			if( ESMGetExecuteMode() )
			{
				if( i != DSC_INFO_LOCATION && i != DSC_INFO_STATUS && i != DSC_INFO_MODEL && i != DSC_INFO_NUM)
					m_strInfo[i] = strTp;
			}
			else
				m_strInfo[i] = strTp;

		}

		ar>>m_nAdj[DSC_ADJ_X];
		ar>>m_nAdj[DSC_ADJ_Y];
		ar>>m_nAdj[DSC_ADJ_A];
		ar>>m_nAdj[DSC_ADJ_RX];
		ar>>m_nAdj[DSC_ADJ_RY];
		ar>>m_nAdj[DSC_ADJ_SCALE];
		m_nAdj[DSC_ADJ_DISTANCE] = 0.0;

		if( ESMGetVersion(GetVersionInfo()) >= ESM_4DMAKER_VERSION_10001)
			ar>>m_nAdj[DSC_ADJ_DISTANCE];

		if(ESMGetVersion(GetVersionInfo()) >= ESM_4DMAKER_VERSION_10017)
		{
			//for( i = 0 ; i < DSC_PROP_CNT ; i++ )
			//	ar>>m_strProp[i];
		}
		else if(ESMGetVersion(GetVersionInfo()) >= ESM_4DMAKER_VERSION_10013)
		{
			for( i = 0 ; i < DSC_PROP_CNT ; i++ )
				ar>>m_strProp[i];
		}
		else
		{
			for( i = 0 ; i < DSC_PROP_CNT - 1 ; i++ )
				ar>>m_strProp[i];
		}

		//-- Add Height/Width 
		if( ESMGetVersion(GetVersionInfo()) >= ESM_4DMAKER_VERSION_10005)
		{
			ar>>m_nHeight;
			ar>>m_nWidth;
		}

		//180312 hjcho
		if(ESMGetVersion(GetVersionInfo())>= ESM_4DMAKER_VERSION_10015)
		{
			CString strTemp;
			ar>>strTemp;//m_nAdj[DSC_ADJ_REVERSE];
			if(ESMGetLoadRecord())
			{
				if(strTemp == _T("REVERSE"))
					m_bReverse = TRUE;//m_nAdj[DSC_ADJ_REVERSE] = TRUE;
				else
					m_bReverse = FALSE;//m_nAdj[DSC_ADJ_REVERSE] = FALSE;
			}
			else
			{
				if(m_bReverse == TRUE)
					m_bReverse = TRUE;
				else
					m_bReverse = FALSE;
			}
		}
		//SetDSCStatus(SDI_STATUS_REC_FINISH);

		//wgkim 190527
		if(ESMGetVersion(GetVersionInfo())>= ESM_4DMAKER_VERSION_10018)
		{
			ar >> m_rtMargin;
			//	ar>>m_nAdj[DSC_ADJ_MARGIN_LEFT];
			//	ar>>m_nAdj[DSC_ADJ_MARGIN_TOP];
			//	ar>>m_nAdj[DSC_ADJ_MARGIN_RIGHT];
			//	ar>>m_nAdj[DSC_ADJ_MARGIN_BOTTOM];
		}
	}
}


void CDSCItem::SdiAddMsg(ESMEvent* pMsg)
{
	if(m_nType == DSC_UNPREPARED)
	{
		//ESMLog(0,_T("Not Connect DSC"));
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
	}
	else if(m_nType == DSC_REMOTE)
	{
		if(pMsg->message == WM_SDI_OP_SET_PROPERTY_VALUE)
		{
			/*if(pMsg->nParam1 == 0x5003)
			{
				TRACE(_T("IMAGE_SIZE TEST"));
			}*/
			if(GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
			{
				pMsg->message = WM_ESM_LIST_SET_PROPERTY_EX;
				if(pMsg->nParam2 <= PTP_VALUE_UINT_32)
					pMsg->nParam2 = PTP_DPV_UINT32;
				else if(pMsg->nParam2 == PTP_VALUE_STRING)
					pMsg->nParam2 = PTP_DPV_STR;
				
				pMsg->pDest = (LPARAM)this;
				::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
			}
			else // NX Series
			{
				pMsg->message = WM_ESM_LIST_SET_PROPERTY;
				pMsg->pDest = (LPARAM)this;
				::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
			}
			
		}
		else if(pMsg->message == WM_SDI_OP_GET_PROPERTY_DESC)
		{
			pMsg->message = WM_ESM_LIST_GET_PROPERTY;
			pMsg->pDest = (LPARAM)this;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
		else if(pMsg->message == WM_SDI_OP_HIDDEN_COMMAND)
		{
			pMsg->message = WM_ESM_LIST_HIDDEN_COMMAND;
			pMsg->pDest = (LPARAM)this;
			if(pMsg->nParam1 == HIDDEN_COMMAND_CHANGE_MODE || 
				pMsg->nParam1 == HIDDEN_COMMAND_CHANGE_MOV_SIZE ||
				pMsg->nParam1 == HIDDEN_COMMAND_CHANGE_MOV_VOICE ||
				pMsg->nParam1 == HIDDEN_COMMAND_MOVIEANDJPG ||
				pMsg->nParam1 == HIDDEN_COMMAND_NONENCORDING)
			{
				pMsg->nParam2 = pMsg->pParam;
			}
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
		else if(pMsg->message == WM_SDI_OP_SET_ENLARGE)
		{
			pMsg->pDest = (LPARAM)this;
			pMsg->message = WM_ESM_LIST_SET_ENLARGE_TOCLIENT;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
		else if(pMsg->message == WM_SDI_OP_HALF_SHUTTER)
		{
			pMsg->pDest = (LPARAM)this;
			pMsg->message = WM_ESM_LIST_HALF_SHUTTER_TOCLIENT;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
		else if(pMsg->message == WM_SDI_OP_GET_FOCUS_VALUE)
		{
			pMsg->message = WM_ESM_LIST_GETFOCUS_TOCLIENT;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
		else if(pMsg->message == WM_SDI_OP_GETITEMFOCUS_VALUE)
		{
			pMsg->message = WM_ESM_LIST_GETITEMFOCUS;
			//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
			pMsg->pDest = (LPARAM)this;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
		else if(pMsg->message == WM_SDI_OP_SET_FOCUS)
		{
			pMsg->message = WM_ESM_LIST_SET_FOCUS;
			pMsg->pDest = (LPARAM)this;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
		//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
		else if(pMsg->message ==WM_SDI_OP_SET_FOCUS_VALUE)
		{
			pMsg->message = WM_ESM_LIST_SET_FOCUS_VALUE;
			pMsg->pDest = (LPARAM)this;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
		else if (pMsg->message == WM_SDI_OP_FOCUS_LOCATE_SAVE)
		{
			pMsg->message = WM_ESM_LIST_FOCUS_LOCATE_SAVE_TOCLIENT;
			pMsg->pDest = (LPARAM)this;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
		else if (pMsg->message == WM_SDI_OP_SET_FOCUS_FRAME)
		{
			pMsg->message = WM_ESM_LIST_SET_FOCUS_FRAME_TOCLIENT;
			pMsg->pDest = (LPARAM)this;
			::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
		else
		{
			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}
		}
	}
	else
	{
		/*if(pMsg->nParam1 == 0x5003)
			TRACE(_T("IMAGE_TEST"));*/

		if(m_pSdi)
			m_pSdi->SdiAddMsg((SdiMessage*)pMsg);
	}
}

CString CDSCItem::GetDeviceDSCID() 
{
	CString strID;
	strID.Format(_T("%s"),GetDSCInfo(DSC_INFO_ID));
	return strID; 
}
CString CDSCItem::GetDeviceVendor()
{
	return _T("");
}
CString CDSCItem::GetDeviceModel	() 
{
	return GetDSCInfo(DSC_INFO_MODEL);
}
CString CDSCItem::GetDeviceUniqueID()
{
	return GetDSCInfo(DSC_INFO_UNIQUEID);
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
CString CDSCItem::GetSavedFile()
{
	CString strFile;

	if(m_pSdi)
		strFile = m_pSdi->GetSavedFile();
	else
		strFile.Format(_T("%s\\%s.mp4"),ESMGetPath(ESM_PATH_MOVIE),GetDeviceDSCID());
	return strFile;
}

CString CDSCItem::GetFirstFrame()
{
	CString strFile;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strFile.Format(_T("%s\\%s\\%s\\0.000.jpg"),ESMGetDataPath(GetDSCInfo(DSC_INFO_LOCATION)), ESMGetFrameRecord(), GetDeviceDSCID());
	else
		strFile.Format(_T("%s\\%s\\%s.jpg"),ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), GetDeviceDSCID());
	return strFile;
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-09-09
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCItem::SetDSCInfo	(int n, CString str)
{
	if(n > DSC_INFO_CNT)
	{
		ESMLog(0, _T("SetDSCInfo %d"), n);
		return;
	}
	// [10/5/2013 Administrator]
	// Critical Section
	EnterCriticalSection (&_DSCItem);
	m_strInfo[n]	= str	;
	LeaveCriticalSection(&_DSCItem);

	if(m_pStaticInfo[n])
	{
		//if(m_bReverse)
		//	m_pStaticInfo[n]->SetFont(&m_fReverse);

		m_pStaticInfo[n]->SetWindowText(GetDSCInfo(n));	
	}
}

CString CDSCItem::GetDSCInfo(int nInfo)
{
	CString strInfo;
	EnterCriticalSection (&_DSCItem);	
	strInfo = m_strInfo[nInfo];
	LeaveCriticalSection(&_DSCItem);
	return strInfo;
}

void CDSCItem::SetDSCAdj	(int n, float nData)
{
	// [10/5/2013 Administrator]
	// Critical Section
	EnterCriticalSection (&_DSCItem);
	m_nAdj[n]		= nData	;
	
	if(m_pStaticAdj[n])
	{
		CString strVal;
		strVal.Format(_T("%.4f"),m_nAdj[n]);		
		//if(n == DSC_ADJ_REVERSE)
		//{
		//	if(nData == TRUE)//Reverse
		//		strVal.Format(_T("REVERSE"));
		//	else
		//		strVal.Format(_T("NORMAL"));
		//}
		m_pStaticAdj[n]->SetWindowText(strVal);
	}
	LeaveCriticalSection(&_DSCItem);
}

//------------------------------------------------------------------------------
//! @brief		Update Changed Values
//! @date		2013-10-14
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
void CDSCItem::UpdateAdj  ()
{
	CString strVal;
	for(int n = 0 ; n < DSC_ADJ_CNT; n ++)
	{	
		/*if(n == DSC_ADJ_REVERSE)
		{
		if(m_bReverse)
		strVal.Format(_T("REVERSE"));
		else
		strVal.Format(_T("NORMAL"));

		m_pStaticAdj[n]->SetWindowText(strVal);
		continue;
		}*/
		strVal.Format(_T("%.4f"),m_nAdj[n]);
		m_pStaticAdj[n]->SetWindowText(strVal);
	}	
}

void CDSCItem::SetDSCProp	(int n, CString str)
{
	// [10/5/2013 Administrator]
	// Critical Section
	EnterCriticalSection (&_DSCItem);

	m_strProp[n]	= str	;	
	if(m_pStaticProp[n])
		m_pStaticProp[n]->SetWindowText(m_strProp[n]);

	LeaveCriticalSection(&_DSCItem);
}

void CDSCItem::SetMovieCancel(BOOL bMovieCancel)
{
	m_bMovieCancel = bMovieCancel;
}

void CDSCItem::SetAdjInfo(BOOL bRight, ESM3DInfo* p3DInfo)
{
	for(int i = 0 ; i < DSC_ADJ_CNT; i++)
		p3DInfo->nAdj[bRight][i] = m_nAdj[i];
}

void CDSCItem::SetFocus( int nFocus )
{
	m_nFocus = nFocus;
}

int CDSCItem::Focus()
{
	return m_nFocus;
}

void CDSCItem::SetFocusData( CString strPath, int nFocus )
{
	CString strValue, strEntry;
	CESMIni ini;

	strPath = strPath + _T(".foc");

	CFile WriteFile;
	if(WriteFile.Open(strPath, CFile::modeCreate | CFile::modeRead |CFile::modeNoTruncate))
		WriteFile.Close();
	else
		return ;

	if(!ini.SetIniFilename (strPath))
		return;

	strEntry.Format(_T("%s"), GetDSCInfo(DSC_INFO_ID));
	ini.WriteInt(INFO_SECTION_FOCUS, strEntry, nFocus);
}

void CDSCItem::SetFocusData(int nCurFocus, int nMaxFocus, int nMinFocus)
{
	m_nCurFocus = nCurFocus;
	m_nMaxFocus = nMaxFocus;
	m_nMinFocus = nMinFocus;
	SaveFocus(m_nMaxFocus, m_nMinFocus, m_nCurFocus);
}

void CDSCItem::SaveFocus(int nMaxFocus, int nMinFocus, int nCurFocus)
{
	if( nCurFocus == 0)
	{
		ESMLog(1, _T("Set Focus Fail Group : [%d], ID : [%s]"), m_nRemoteID, GetDeviceDSCID());
		return;
	}

	CString strValue, strEntry, strSection;
	CString strConfigForder, strConfigFile;
	CESMIni ini;
	//-- 2013-05-04 hongsu@esmlab.com
	//-- Load Config File
	strConfigForder.Format(_T("%s"), ESMGetPath(ESM_PATH_SETUP));
	CreateDirectory(strConfigForder, NULL);
	strConfigFile = strConfigForder + _T("\\Focus.info");

	CFile WriteFile;
	if(WriteFile.Open(strConfigForder + _T("\\Focus.info"), CFile::modeCreate | CFile::modeRead |CFile::modeNoTruncate))
		WriteFile.Close();
	else
		return ;

	if(!ini.SetIniFilename (strConfigFile))
		return;

	strValue.Format(_T("%d"), m_nCurFocus);
	strEntry.Format(_T("%s"), GetDSCInfo(DSC_INFO_ID));
	ini.WriteInt(INFO_SECTION_FOCUS, strEntry, nCurFocus);
	ini.WriteInt(INFO_SECTION_FOCUS, strEntry + _T("_MIN"), nMinFocus);
	ini.WriteInt(INFO_SECTION_FOCUS, strEntry + _T("_MAX"), nMaxFocus);
}

void CDSCItem::LoadFocus()
{
	CString strValue, strEntry, strSection;
	CString strConfigForder, strConfigFile;
	CESMIni ini;
	//-- 2013-05-04 hongsu@esmlab.com
	//-- Load Config File
	strConfigForder.Format(_T("%s"), ESMGetPath(ESM_PATH_SETUP));
	CreateDirectory(strConfigForder, NULL);
	strConfigFile = strConfigForder + _T("\\Focus.info");
	if(!ini.SetIniFilename (strConfigFile))
		return;

	strValue.Format(_T("%d"), m_nCurFocus);
	strEntry.Format(_T("%s"), GetDSCInfo(DSC_INFO_ID));
	m_nCurFocus = ini.GetInt(INFO_SECTION_FOCUS, strEntry, 0);
	m_nMinFocus	= ini.GetInt(INFO_SECTION_FOCUS, strEntry + _T("_MIN"), 0);
	m_nMaxFocus	= ini.GetInt(INFO_SECTION_FOCUS, strEntry + _T("_MAX"), 0);
}

int CDSCItem::GetLoadFocusValue( CString strPath )
{
	int nValue = 0;
	CString strEntry;
	CString strConfigForder, strConfigFile;
	CESMIni ini;

	if (strPath.IsEmpty())
	{
		strConfigForder.Format(_T("%s"), ESMGetPath(ESM_PATH_SETUP));
		CreateDirectory(strConfigForder, NULL);
		strConfigFile = strConfigForder + _T("\\Focus.info");
	}
	else
	{
		strConfigFile = strPath;
	}
	
	if(!ini.SetIniFilename (strConfigFile))
		return -1;
	
	strEntry.Format(_T("%s"), GetDSCInfo(DSC_INFO_ID));
	nValue = ini.GetInt(INFO_SECTION_FOCUS, strEntry, 0);

	return nValue;
}


void CDSCItem::SetDSCCaptureInfo(int nType, CString strValue)
{
	int nIndex = 0;
	switch(nType)
	{
	case PTP_CODE_FUNCTIONALMODE		: 
		m_strCaptureInfo[DSC_INFO_CAPTURE_MODE]  = strValue;
		break;
	case PTP_CODE_IMAGESIZE				: 
		m_strCaptureInfo[DSC_INFO_CAPTURE_SIZE]  = strValue;
		break;
	case PTP_CODE_COMPRESSIONSETTING	: 
		m_strCaptureInfo[DSC_INFO_CAPTURE_QUALITY]  = strValue;
		break;
	case PTP_CODE_SAMSUNG_SHUTTERSPEED	: 
		m_strCaptureInfo[DSC_INFO_CAPTURE_SHUTTERSPEED]  = strValue;
		break;
	case PTP_CODE_F_NUMBER				: 
		m_strCaptureInfo[DSC_INFO_CAPTURE_APERTURE]  = strValue;
		break;
	case PTP_CODE_EXPOSUREINDEX			: 
		m_strCaptureInfo[DSC_INFO_CAPTURE_ISO]  = strValue;
		break;
	case PTP_CODE_WHITEBALANCE			: 
		m_strCaptureInfo[DSC_INFO_CAPTURE_WHITEBALANCE]  = strValue;
		break;
	case PTP_CODE_EXPOSUREMETERINGMODE	: 
		m_strCaptureInfo[DSC_INFO_CAPTURE_METERING]  = strValue;
		break;
	case PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN	: 
		break;
	case PTP_DPC_WB_DETAIL_KELVIN			:
		m_strCaptureInfo[DSC_INFO_CAPTURE_COLORTEMPERATURE]  = strValue;
		break;
	case PTP_CODE_SAMSUNG_EV			: 
		m_strCaptureInfo[DSC_INFO_CAPTURE_EVC]  = strValue;
		break;
	case PTP_CODE_SAMSUNG_PICTUREWIZARD			: 
		m_strCaptureInfo[DSC_INFO_CAPTURE_PICTUREW]  = strValue;
		break;
	case PTP_CODE_SAMSUNG_ZOOM_CTRL_GET			: 
		m_strCaptureInfo[DSC_INFO_CAPTURE_ZOOM]  = strValue;
		break;
	case PTP_CODE_SAMSUNG_MOV_SIZE				:
		m_strCaptureInfo[DSC_INFO_CAPTURE_MOVIESZIE]  = strValue;
		break;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	case PTP_OC_SAMSUNG_GetFocusPosition:
		m_strCaptureInfo[DSC_INFO_CAPTURE_FOCUS]  = strValue;		
		break;
	case PTP_DPC_PHOTO_STYLE:
		m_strCaptureInfo[DSC_INFO_CAPTURE_PHOTO_STYLE]  = strValue;		
		break;
	case PTP_DPC_PS_CONTRAST:
		m_strCaptureInfo[DSC_INFO_CAPTURE_PS_CONTRAST]  = strValue;
		break;
	case PTP_DPC_PS_SHARPNESS:
		m_strCaptureInfo[DSC_INFO_CAPTURE_PS_SHARPNESS]  = strValue;
		break;
	case PTP_DPC_PS_NOISE_REDUCTION:
		m_strCaptureInfo[DSC_INFO_CAPTURE_PS_NOISE_REDUCTION]  = strValue;
		break;
	case PTP_DPC_PS_SATURATION:
		m_strCaptureInfo[DSC_INFO_CAPTURE_PS_SATURATION]  = strValue;
		break;
	case PTP_DPC_PS_HUE:
		m_strCaptureInfo[DSC_INFO_CAPTURE_PS_HUE]  = strValue;
		break;
	case PTP_DPC_PS_FILTER_EFFECT:
		m_strCaptureInfo[DSC_INFO_CAPTURE_PS_FILTER_EFFECT]  = strValue;
		break;
	case PTP_DPC_WB_ADJUST_AB:
		m_strCaptureInfo[DSC_INFO_CAPTURE_WB_ADJUST_AB]  = strValue;		
		break;
	case PTP_DPC_WB_ADJUST_MG:
		m_strCaptureInfo[DSC_INFO_CAPTURE_WB_ADJUST_MG]  = strValue;		
		break;
	case PTP_DPC_MOV_STA_E_STABILIZATION:
		m_strCaptureInfo[DSC_INFO_CAPTURE_E_STABILIZATION]  = strValue;		
		break;
	case PTP_DPC_SAMSUNG_FW_VERSION:
		m_strCaptureInfo[DSC_INFO_CAPTURE_FW_VERSION]  = strValue;		
		break;
	case PTP_DPC_SAMSUNG_ISO_STEP:
		m_strCaptureInfo[DSC_INFO_CAPTURE_ISO_STEP]  = strValue;		
		break;
	case PTP_DPC_SAMSUNG_ISO_EXPANSION:
		m_strCaptureInfo[DSC_INFO_CAPTURE_ISO_EXPANSION]  = strValue;		
		break;
	case PTP_DPC_LCD:
		m_strCaptureInfo[DSC_INFO_CAPTURE_LCD]  = strValue;		
		break;
	case PTP_DPC_PEAKING:
		m_strCaptureInfo[DSC_INFO_CAPTURE_PEAKING]  = strValue;		
		break;
	case PTP_DPC_SET_SYSTEM_FREQUENCY:
		m_strCaptureInfo[DSC_INFO_CAPTURE_SYSTEM_FREQUENCY]  = strValue;		
		break;
	case PTP_DPC_LENS_POWER_CTRL:
		m_strCaptureInfo[DSC_INFO_CAPTURE_LENS_POWER_CTRL]  = strValue;		
		break;
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM1:
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM2:
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM3:
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM1_ETC:
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM2_ETC:
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM3_ETC:
		break;
	default:
		ESMLog(5, _T("[ERROR DSC SAVE] SetDSCCaptureInfo type:%d default :%s"), nType, strValue);
		break;
	}
}

void CDSCItem::SetTickStatus(BOOL bTickStatus)
{
	m_bTickSuccess = bTickStatus;
}

CString CDSCItem::GetDSCCaptureInfo(int n)
{
	return m_strCaptureInfo[n];
}

void CDSCItem::SetSensorSync(BOOL b)		
{ 
	m_bSensorSync = b;
}

BOOL CDSCItem::DSCGetTickCount_resync(int& tPC, double& tDSC, int& nCheckCnt, int& nCheckTime, int nDSCSyncTime)
{
	if(!m_pSdi)
		return FALSE;

	//ESMSetDeviceDSCID(GetDeviceDSCID());

	//////AddDSC
	//ESMLog(5, _T("4DA : %s"),GetDeviceDSCID());
	//ESMSet4DAFileInit(GetDeviceDSCID());

	// resync 하려면 gettick을 레코딩 중에 해야하므로 상태체크 주석처리..
#if 0
	int nState = m_pSdi->PTP_GetRecordStatus();
	ESMLog(5,_T("[%s] CDSCItem::DSCGetTickCount PTP_GetRecordStatus[%d]"), GetDeviceDSCID(), nState);

	if (nState == 0)
	{
		ESMLog(5,_T("[%s] CDSCItem::DSCGetTickCount DSC REC Off"), GetDeviceDSCID());
		m_pSdi->PTP_SendCapture_Require(S3_PRESS);
		m_pSdi->PTP_SendCapture_Require(S3_RELEASE);
	}
#endif 

	CString strDscId = GetDeviceDSCID();
	CString str4DP = ESMGet4DAPath(strDscId);
	ESMLog(5,_T("[%s] DSC Start / %s, nDSCSyncTime:%d"), strDscId, str4DP, nDSCSyncTime);

	//int nRet = m_pSdi->PTP_GetTick(tPC, tDSC, nCheckCnt, nCheckTime, nDSCSyncTime, m_hParentWnd);
	int nRet = m_pSdi->PTP_GetTick(tPC, tDSC, nCheckCnt, nCheckTime, nDSCSyncTime);

	if((nRet == SDI_ERR_SEMAPHORE_TIMEOUT) || (nCheckTime < 0) || (nCheckTime > 15))
	{
		m_bTickSuccess = FALSE;
		SetDSCStatus(SDI_STATUS_ERROR_TICK);
		ESMLog(0,_T("Get Tick Count ERR of DSC %s nCheckTime[%d]"),m_pSdi->GetDeviceDSCID(),nCheckTime);
		return FALSE;
	}
	else
	{
		m_bTickSuccess = TRUE;
	}

#ifdef _4DP_CONNECT
	//jhhan 4DP Connection
	//RCManager Create & Connect To 4DP
	if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
	{
		if(m_pRCClient == NULL)
		{
			m_pRCClient = new CESMRCManager(str4DP, 0);
			m_pRCClient->CreateThread();

			m_pRCClient->ConnectToProcessor();
		}
	}
#endif

	//-- 2013-10-23 hongsu@esmlab.com
	//-- Set Time
	if (m_nResyncGetTickCount < 1)
	{
		m_tDSC	= tDSC;
		m_tPC	= tPC;
	}
	else
	{
		if (tDSC < m_tDSC)
		{
			m_tDSC	= tDSC;
			m_tPC	= tPC;
		}

		//ESMLog(5, _T("[GET TICK_resync_111][Set Time[%s] => PC[%d] DSC[%lf] Gap[%d] Count[%d],,,,,PC[%d] DSC[%lf]"),GetDeviceDSCID(), m_tPC, m_tDSC, nCheckTime, nCheckCnt, tPC, tDSC);
	}

	ESMLog(5, _T("[GET TICK_resync][Set Time[%s] => PC[%d] DSC[%lf] Gap[%d] Count[%d]"),GetDeviceDSCID(), m_tPC, m_tDSC, nCheckTime, nCheckCnt);
	TRACE(_T("Set Time[%s] => PC[%d] DSC[%lf]\n"),GetDeviceDSCID(), m_tPC, m_tDSC);

	//jhhan 170726 PropertyList
	//ESMGetPropertyListView();

	return TRUE;
}

//------------------------------------------------------
//-- Sync (Record)
//------------------------------------------------------
BOOL CDSCItem::DSCGetTickCount(int& tPC, double& tDSC, int& nCheckCnt, int& nCheckTime, int nDSCSyncTime)
{
	if(!m_pSdi)
		return FALSE;

	//ESMSetDeviceDSCID(GetDeviceDSCID());

	//////AddDSC
	//ESMLog(5, _T("4DA : %s"),GetDeviceDSCID());
	//ESMSet4DAFileInit(GetDeviceDSCID());

	// resync 하려면 gettick을 레코딩 중에 해야하므로 상태체크 주석처리..
#if 0
	int nState = m_pSdi->PTP_GetRecordStatus();
	ESMLog(5,_T("[%s] CDSCItem::DSCGetTickCount PTP_GetRecordStatus[%d]"), GetDeviceDSCID(), nState);

	if (nState == 0)
	{
		ESMLog(5,_T("[%s] CDSCItem::DSCGetTickCount DSC REC Off"), GetDeviceDSCID());
		m_pSdi->PTP_SendCapture_Require(S3_PRESS);
		m_pSdi->PTP_SendCapture_Require(S3_RELEASE);
	}
#endif 

	CString strDscId = GetDeviceDSCID();
	CString str4DP = ESMGet4DAPath(strDscId);
	ESMLog(5,_T("[%s] DSC Start / %s"), strDscId, str4DP);

	//int nRet = m_pSdi->PTP_GetTick(tPC, tDSC, nCheckCnt, nCheckTime, nDSCSyncTime, m_hParentWnd);
	int nRet = m_pSdi->PTP_GetTick(tPC, tDSC, nCheckCnt, nCheckTime, nDSCSyncTime);

	if((nRet == SDI_ERR_SEMAPHORE_TIMEOUT) || (nCheckTime < 0) || (nCheckTime > 15))
	{
		m_bTickSuccess = FALSE;
		SetDSCStatus(SDI_STATUS_ERROR_TICK);
		ESMLog(0,_T("Get Tick Count ERR of DSC %s nCheckTime[%d]"),m_pSdi->GetDeviceDSCID(),nCheckTime);
		return FALSE;
	}
	else
	{
		m_bTickSuccess = TRUE;
	}

#ifdef _4DP_CONNECT
	//jhhan 4DP Connection
	//RCManager Create & Connect To 4DP
	if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
	{
		if(m_pRCClient == NULL)
		{
			m_pRCClient = new CESMRCManager(str4DP, 0);
			m_pRCClient->CreateThread();

			m_pRCClient->ConnectToProcessor();
		}
	}
#endif
	
	//-- 2013-10-23 hongsu@esmlab.com
	//-- Set Time
	m_tDSC	= tDSC;
	m_tPC	= tPC;
	ESMLog(5, _T("[GET TICK][Set Time[%s] => PC[%d] DSC[%lf] Gap[%d] Count[%d]"),GetDeviceDSCID(), m_tPC, m_tDSC, nCheckTime, nCheckCnt);
	TRACE(_T("Set Time[%s] => PC[%d] DSC[%lf]\n"),GetDeviceDSCID(), m_tPC, m_tDSC);

	//jhhan 170726 PropertyList
	//ESMGetPropertyListView(GetDeviceDSCID());

	return TRUE;
}

void CDSCItem::SetTickCount(double nStartTime, int nIntervalSensorOn)
{
	if(!m_pSdi)
		return;

	//-- 2013-10-23 hongsu@esmlab.com
	ESMEvent* pMsg = new ESMEvent();

	if(GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
	{
		if( m_pSdi )
		{
			pMsg = new ESMEvent();
			//-- Release Focusing
			pMsg->message	= WM_RS_SET_TICK;
			pMsg->nParam1	= (UINT)nStartTime;
			pMsg->nParam2	= (UINT)nIntervalSensorOn;
			m_pSdi->AddMsg((CObject*)pMsg);
			TRACE(_T("[Non Msg Queue] PTP_SetTick [%s]: %.4lf\n"), GetDeviceDSCID(),nStartTime);
			ESMLog(5, _T("[SET TICK] [%s]: %u"), GetDeviceDSCID(), pMsg->nParam1);
		}
	}
	else
	{
		//if( m_bPauseMode)
		{
			pMsg->message	= WM_RS_SET_PAUSE;
			//pMsg->nParam1	= (UINT)TRUE;
			pMsg->nParam1	= (UINT)FALSE;
			m_pSdi->AddMsg((CObject*)pMsg);
		}
		if( m_pSdi )
		{
			pMsg = new ESMEvent();
			//-- Release Focusing
			pMsg->message	= WM_RS_SET_TICK;
			pMsg->nParam1	= (UINT)nStartTime;
			pMsg->nParam2	= (UINT)nIntervalSensorOn;
			m_pSdi->AddMsg((CObject*)pMsg);
			TRACE(_T("[Non Msg Queue] PTP_SetTick [%s]: %.4lf\n"), GetDeviceDSCID(),nStartTime);
		}
	}
	
}

void CDSCItem::SetResume(double nFrameCount)
{
	if( m_pSdi )
	{
		ESMEvent* pMsg = new ESMEvent();
		//-- Release Focusing
		pMsg->message	= WM_RS_SET_RESUME;
		pMsg->nParam1	= (UINT)nFrameCount;
		m_pSdi->AddMsg((CObject*)pMsg);
	}
}
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-09-17
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCItem::AddErrorFrame(int nIndex, int nInterval)
{
	ValidFrame* pValidFrameExcept = NULL;
	ESMEvent* pMsgExcept = NULL;

	CString strID;
	strID = GetDeviceDSCID();
	float nTime1, nTime2;

	nTime1 = ESMGetFrameTime(nIndex);	
	/*
	ESMLog(0, _T("[%s] Error Frame cnt:[%d] Interval[%d]"), 
		strID,
		nIndex,
		nInterval);
	*/

	//-- 2014/09/20 hongsu
	//-- Check Wrong Interval (micro second)
	if(nInterval> 1000)
	{
		ESMLog(5,_T("##[AGENT][%s][%.3f] Interval Between TickCount and Timestamp [%d] ms"),strID, (float)(nTime1/(float)1000), nInterval/1000);
		return;
	}

	if(nInterval< 10)
	{
		//ESMLog(5,_T("##[AGENT][%s][%.3f] Check Time Interval [%d] ms"),strID, (float)(nTime1/(float)1000), nInterval);
		return;
	}

// 	int nSkipCnt;	// Frame Skip Count
// 	nSkipCnt = (nInterval - movie_next_frame_time_margin) /movie_next_frame_time;
// 	nSkipCnt+=2;

	pValidFrameExcept = new ValidFrame;
	pValidFrameExcept->strDSC = strID;
	pValidFrameExcept->nCurrentIndex	=  nIndex;
	pValidFrameExcept->nCorrectIndex	= -1;

 	pMsgExcept = new ESMEvent();
 	pMsgExcept->message = WM_ESM_MOVIE_VALIDFRAME;
 	pMsgExcept->pParam	= (LPARAM)pValidFrameExcept;
 	::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsgExcept);

// 	int nAllSkipCnt = nSkipCnt;
// 	//	Error Frame
// 	while(nSkipCnt--)
// 	{			
// 		pValidFrameExcept = new ValidFrame;
// 		pValidFrameExcept->strDSC = strID;
// 		pValidFrameExcept->nCurrentIndex	=  nIndex-nSkipCnt;
// 		pValidFrameExcept->nCorrectIndex	= -1;
// 		//-- TRACE
// 		nTime1 = ESMGetFrameTime(pValidFrameExcept->nCurrentIndex);	
// 		ESMLog(5,_T("[AGENT][%s][%.03f] Except Frame Interval [%d]ms [%d/%d]"),
// 			pValidFrameExcept->strDSC			,
// 			(float)(nTime1/(float)1000)		, 
// 			//pValidFrameExcept->nCurrentIndex,
// 			nInterval,
// 			nSkipCnt+1,
// 			nAllSkipCnt);
// 
// 		//-- 2014-09-17 hongsu@esmlab.com
// 		//-- Add to Movie Manager
// 		pMsgExcept = new ESMEvent();
// 		pMsgExcept->message = WM_ESM_MOVIE_VALIDFRAME;
// 		pMsgExcept->pParam	= (LPARAM)pValidFrameExcept;
// 		::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsgExcept);
// 	}
// 
// 	
// 	//-- Check Error Frame Info (39,40,41)
// 	if( (nInterval < movie_next_frame_time + movie_next_frame_time_margin) &&
// 		(nInterval >= movie_next_frame_time - movie_next_frame_time_margin) )
// 	{		
// 		ValidFrame* pValidFrameCorrect = new ValidFrame;
// 		pValidFrameCorrect->strDSC =strID;
// 		pValidFrameCorrect->nCurrentIndex	= nIndex;
// 		pValidFrameCorrect->nCorrectIndex	= nIndex-1;
// 		//-- TRACE
// 		nTime1 = ESMGetFrameTime(pValidFrameCorrect->nCurrentIndex);
// 		nTime2 = ESMGetFrameTime(pValidFrameCorrect->nCorrectIndex);
// 		ESMLog(5,_T("[AGENT][%s][%.03f]->[%.03f] Change Frame"),
// 			pValidFrameCorrect->strDSC			,
// 			(float)(nTime1/(float)1000)		, 
// 			(float)(nTime2/(float)1000)		
// 			//pValidFrameCorrect->nCurrentIndex	,
// 			//pValidFrameCorrect->nCorrectIndex	);
// 			);
// 
// 		//-- 2014-09-17 hongsu@esmlab.com
// 		//-- Add to Movie Manager
// 		ESMEvent* pMsgCorrect = new ESMEvent();  
// 		pMsgCorrect->message = WM_ESM_MOVIE_VALIDFRAME;
// 		pMsgCorrect->pParam	=  (LPARAM)pValidFrameCorrect;
// 		::SendMessage(m_hParentWnd, WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsgCorrect);		
// 	}	
}

void CDSCItem::SetWidth(int nWidth)
{
	m_nWidth = nWidth;
}

void CDSCItem::SetHeight(int nHeight)
{
	m_nHeight = nHeight;
}

int CDSCItem::GetWidth()
{
	return m_nWidth;
}

int CDSCItem::GetHeight()
{
	return m_nHeight;
}

void CDSCItem::SetReSyncGetTickReady( BOOL b )
{
	m_bReSyncGetTickReady = b;
}

BOOL CDSCItem::GetReSyncGetTickReady()
{
	return m_bReSyncGetTickReady;
}

void CDSCItem::SetTemperatureWarning( BOOL b )
{
	m_bTemperatureWarning = b;
}

BOOL CDSCItem::GetTemperatureWarning()
{
	return m_bTemperatureWarning;
}