////////////////////////////////////////////////////////////////////////////////
//
//	DSCInfoViewBase.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-05
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "DSCItem.h"
#include "ESMCtrl.h"


class CDSCViewer;

class CDSCInfoViewBase : public CScrollView
{
	DECLARE_DYNCREATE(CDSCInfoViewBase)

public:
	CDSCInfoViewBase();
	CDSCInfoViewBase(CDSCViewer* pParent);
	virtual ~CDSCInfoViewBase();

public:
	int GetItemCount();
	CDSCItem* GetItemData(int nIndex);
	void UpdateSizes();
	
	int GetSelectLine();		
	void SetSelectLine(int n);

	//-- 2013-09-10 hongsu@esmlab.com
	//-- Control Grid Status (normal,over,selected) 
	virtual void SetGridStatus(int nRow, int nStatus) {};

public:
	//-- parent view
	CDSCViewer	* m_pView;
	HWND m_hMainWnd;

protected:
	CFont *m_pFont;
	void SetFrameFont(CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);	

	//------------------------------------------------------------------------------
	//! @function	Message
	//------------------------------------------------------------------------------
	LRESULT OnDSCGridOver(WPARAM w, LPARAM l);	


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDSCInfoViewBase)
public:
	virtual BOOL OnScroll(UINT nScrollCode, UINT nPos, BOOL bDoScroll = TRUE);
protected:
	virtual void OnDraw(CDC* pDC) {};   // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct	
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	//}}AFX_VIRTUAL	
	DECLARE_MESSAGE_MAP()
};

