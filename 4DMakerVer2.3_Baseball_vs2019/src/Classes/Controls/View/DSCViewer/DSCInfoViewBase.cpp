////////////////////////////////////////////////////////////////////////////////
//
//	DSCInfoViewBase.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-05
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "DSCViewer.h"
#include "DSCInfoViewBase.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CDSCInfoViewBase, CScrollView)
BEGIN_MESSAGE_MAP(CDSCInfoViewBase, CScrollView)
	//{{AFX_MSG_MAP(CTimeLineEditor)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()	
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_DSC_INFO_OVER	,	OnDSCGridOver)	
END_MESSAGE_MAP()
// CDSCInfoViewBase
CDSCInfoViewBase::CDSCInfoViewBase()
{
	m_pView			= NULL;	
	m_pFont			= NULL;	
}

// CDSCInfoViewBase
CDSCInfoViewBase::CDSCInfoViewBase(CDSCViewer* pParent)
{
	m_pView			= pParent;
	m_pFont			= NULL;
}

CDSCInfoViewBase::~CDSCInfoViewBase()
{
	m_pView	= NULL;	
	if(m_pFont)
	{
		delete m_pFont;
		m_pFont =NULL;
	}
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------

int CDSCInfoViewBase::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;	
	return 0;
}

BOOL CDSCInfoViewBase::OnEraseBkgnd(CDC* pDC) 
{
	return TRUE;
}

void CDSCInfoViewBase::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
	UpdateSizes();
} 

void CDSCInfoViewBase::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
	UpdateSizes();
}

void CDSCInfoViewBase::UpdateSizes()
{
	CRect rect;
	GetClientRect(rect);

	CSize sizeTotal;
	sizeTotal.cy = rect.Height();
	sizeTotal.cx = rect.Width();

	SetScrollSizes(MM_TEXT, sizeTotal);
}

BOOL CDSCInfoViewBase::OnScroll(UINT nScrollCode, UINT nPos, BOOL bDoScroll) 
{
	UpdateSizes();
	return CScrollView::OnScroll(nScrollCode, nPos, bDoScroll);
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
int CDSCInfoViewBase::GetItemCount()
{
	return m_pView->GetItemCount();
}

CDSCItem* CDSCInfoViewBase::GetItemData(int nIndex)
{
	return m_pView->GetItemData(nIndex);
}


//------------------------------------------------------------------------------
//! @function	MESSAGE from Edit
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
LRESULT CDSCInfoViewBase::OnDSCGridOver(WPARAM w, LPARAM l)
{
	//-- Select Row
	int nRow	= (int)w;
	int nStatus	= (int)l;
	m_pView->SetGridStatus(nRow, nStatus);
	return 0L;
}


int CDSCInfoViewBase::GetSelectLine()
{ 
	return m_pView->GetSelectLine(); 
}

void CDSCInfoViewBase::SetSelectLine(int n)
{ 
	m_pView->SetSelectLine(n);		
}

void CDSCInfoViewBase::SetFrameFont(CDC* pDC, int nSize, CString strFont, int nFontType)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	pDC->SelectObject(m_pFont);	
}