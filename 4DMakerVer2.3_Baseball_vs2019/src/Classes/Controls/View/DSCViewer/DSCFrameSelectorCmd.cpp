////////////////////////////////////////////////////////////////////////////////
//
//	DSCFrameSelectorCmd.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-25
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCFrameSelector.h"
#include "DSCViewer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-25
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCFrameSelector::SelectLine(CPoint pt)
{
	int nScroll = ESMGetScroll();
	//int nLine = GetLineFromY(pt.y);
	int nLine = GetLineFromY(pt.y + nScroll);
	//TRACE("SelectLine : nLine[%d], pt.y[%d], nScroll[%d]\r\n", nLine, pt.y, nScroll);

	//-- Check Click Already Selected Item
	if(GetSelectLine() == nLine)
		return;
	if(nLine < 0 || nLine >= GetItemCount())
		return;

	CDSCItem* pDSCItem = NULL;	
	pDSCItem = GetItemData(nLine);
	if(pDSCItem)
	{
		m_pView->m_pListView->SetSelectItem(nLine, pDSCItem);		
	}
}

int CDSCFrameSelector::GetLineFromY(int nPos)
{
	nPos -= m_nHeaderTop;
	if(nPos < 0)
		return -1;

	nPos /= DSC_ROW_H;
	return nPos;
}

int CDSCFrameSelector::GetYFromLine(int nLine)
{
	int nPos = m_nHeaderTop;
	nPos += nLine*DSC_ROW_H;
	return nPos;
}

void CDSCFrameSelector::RemoveObject()
{
	if(m_pTimeLineObject)
	{
		if(!m_pTimeLineObject->m_hWnd)
			return;
		m_pTimeLineObject->ShowWindow(FALSE);
	}
}

void CDSCFrameSelector::UpdateObject(CESMTimeLineObject* pObject)
{
	if(m_pTimeLineObject)
	{
		if(!m_pTimeLineObject->m_hWnd)
			return;
		//-- Update Infomation
		//m_pTimeLineObject->
		m_pTimeLineObject->ShowWindow(TRUE);
	}
	else
	{
		//-- new
	}
}
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-01
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCFrameSelector::InsertDSCList(CObArray* pAr, int nSelStart, int nSelEnd)
{
	//jhhan 180504
	//pItem - NULL ó�� �߰�
	int nLine;
	CDSCItem* pItem = NULL;
	nLine = nSelStart;
	//-- Normal
	if(nSelStart < nSelEnd)
	{		
		while(1)
		{
			pItem = m_pView->GetItemData(nLine);
			if(pItem)
				pAr->Add((CObject*)pItem);
			nLine++;
			if(nLine > nSelEnd)
				break;
		}		
	}
	//-- Reverse
	else
	{
		while(1)
		{
			if(nLine < 0)
			{
				break;
			}
			else
			{
				pItem = m_pView->GetItemData(nLine);
				nLine--;
			}
			if(pItem)
				pAr->Add((CObject*)pItem);
			
			if(nLine < nSelEnd)
				break;
		}
	}	
}

/************************************************************************
 * @method:		CDSCFrameSelector::UpdateObject
 * @function:	UpdateObject
 * @date:		2013-10-14
 * @owner		Ryumin (ryumin@esmlab.com)
 * @param:		CObArray * pAr
 * @param:		int nSelStart
 * @param:		int nSelEnd
 * @return:		void
 */
void CDSCFrameSelector::UpdateObject(CObArray* pAr, int nSelStart, int nSelEnd)
{
	CPoint ptS, ptE;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	//**************************************************************************
	//	Start Position
	m_spotSelect.nTime = nSelStart;
	ptS.x = GetXFromTime(nSelStart);
	CDSCItem*	pStartItem = (CDSCItem*)pAr->GetAt(0);
	if (!pStartItem) return;
	CString		strStartDSC= pStartItem->GetDeviceDSCID();
	int nIdx = 0;
	for (; nIdx < arDSCList.GetCount(); ++nIdx)
	{
		CDSCItem*	pCurItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if (!pCurItem) return;
		CString strCurID = pCurItem->GetDeviceDSCID();
		if (strStartDSC.Compare(strCurID) == 0)
		{
			m_spotSelect.strDSC = strCurID;
			break;
		}
	}
	m_spotSelect.nSelectLine = nIdx;
	ptS.y = GetYFromLine(nIdx);

	//**************************************************************************
	//	End Position
	ptE.x = GetXFromTime(nSelEnd);
	m_ptMouse.x = ptE.x;

	CDSCItem*	pEndItem = (CDSCItem*)pAr->GetAt(pAr->GetCount() -1);
	if (!pEndItem) return;
	CString		strEndDSC= pEndItem->GetDeviceDSCID();
	nIdx = 0;
	for (; nIdx < arDSCList.GetCount(); ++nIdx)
	{
		CDSCItem*	pCurItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if (!pCurItem) return;
		CString strCurID = pCurItem->GetDeviceDSCID();
		if (strEndDSC.Compare(strCurID) == 0)
			break;
	}
	//ptE.y = GetYFromLine(nIdx +1);
	ptE.y = GetYFromLine(nIdx);
	m_ptMouse.y = ptE.y;

	ESMSetEditObj(FALSE);
	ESMDeleteAllDrawPos();
	ESMSetDrawPos(ptS, ptE);

	TRACE(_T("Input (%u,%u) -> (%u,%u)\n", ptS.x,ptS.y,ptE.x,ptE.y));
	// **************************************************************************
	//	Create
	if (m_pTimeLineObject)
	{
		m_pTimeLineObject->DestroyWindow();
		delete m_pTimeLineObject;
		m_pTimeLineObject = NULL;
	}
	m_pTimeLineObject = new CESMTimeLineObjectSelector(	*pAr, nSelStart, nSelEnd );

	// **************************************************************************
	//	SetChangePos
	if(ptS.x > ptE.x)
	{
		int nTemp = ptS.x;
		ptS.x = ptE.x;
		ptE.x = nTemp;		
		m_pTimeLineObject->SetChangePos(TRUE,FALSE);
	}		
	ptS.x -= DSC_ROW_H/2;
	ptE.x += DSC_ROW_H/2;

	if(ptS.y > ptE.y)
	{
		int nTemp = ptS.y;
		ptS.y = ptE.y;
		ptE.y = nTemp;
		ptS.y -= DSC_ROW_H;
		ptE.y += DSC_ROW_H;
		m_pTimeLineObject->SetChangePos(TRUE,TRUE);
	}
	m_pTimeLineObject->Create(NULL, NULL, DS_SETFONT | WS_CHILD | WS_VISIBLE,
						CRect(ptS.x, ptS.y, ptE.x, ptE.y), this, IDD_VIEW_TIMELINE_OBJ);

	m_bUpdateDraw = TRUE;
	//-- Redraw
	DrawEditor();
	m_bUpdateDraw = FALSE;

	arDSCList.RemoveAll();
}

