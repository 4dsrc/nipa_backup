////////////////////////////////////////////////////////////////////////////////
//
//	DSCListViewer.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-05
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "DSCListViewer.h"
#include "DSCViewer.h"
#include "DSCMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CDSCListViewer
IMPLEMENT_DYNCREATE(CDSCListViewer, CDSCInfoViewBase)
BEGIN_MESSAGE_MAP(CDSCListViewer, CDSCInfoViewBase)
	//{{AFX_MSG_MAP(CDSCListViewer)
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_DSC_INFO_BTN_CLICK,	OnDSCBtnClick)
	ON_MESSAGE(WM_DSC_INFO_GRID_CLICK,	OnDSCGridClick)
	ON_MESSAGE(WM_DSC_INFO_GRID_RCLICK,	OnDSCGridRClick)
	ON_MESSAGE(WM_DSC_INFO_BTN_DOWN,	OnDSCBtnDown)
	ON_MESSAGE(WM_DSC_INFO_BTN_MOVE,	OnDSCBtnMove)
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEWHEEL()
	ON_WM_TIMER()
END_MESSAGE_MAP()

CDSCListViewer::CDSCListViewer(CDSCViewer* pParent) : CDSCInfoViewBase(pParent)
{
	m_bShowAdj	= FALSE;
	m_bShowProp = FALSE;
	m_bScroll	= FALSE;
	m_bShowTesting = FALSE;

	m_nScroll		= 0;
	m_nDrawRight	= 0;
	m_nDrawBottom	= 0;		

	//-- 2013-09-06 hongsu@esmlab.com
	//-- Set Header Column Title 
	m_strHDInfo[DSC_INFO_NUM		]	=	_T("#");
	m_strHDInfo[DSC_INFO_LOCATION	] 	=	_T("Location");
	m_strHDInfo[DSC_INFO_ID			] 	=	_T("ID");
	m_strHDInfo[DSC_INFO_STATUS		] 	=	_T("Status");	
	m_strHDAdj [DSC_ADJ_X			] 	=	_T("Adjust X");
	m_strHDAdj [DSC_ADJ_Y			] 	=	_T("Adjust Y");
	m_strHDAdj [DSC_ADJ_A			] 	=	_T("Angle");	
	m_strHDAdj [DSC_ADJ_RX			] 	=	_T("Rotate X");
	m_strHDAdj [DSC_ADJ_RY			] 	=	_T("Rotate Y");
	m_strHDAdj [DSC_ADJ_SCALE		] 	=	_T("Scale");
	m_strHDAdj [DSC_ADJ_DISTANCE	] 	=	_T("Distance");
//	m_strHDAdj [DSC_ADJ_REVERSE		]   =	_T("Reverse");
	//m_strHDProp[DSC_PROP_FUNC		] 	=	_T("Mode");
	
	//jhhan 16-09-09
	//m_strHDProp[DSC_PROP_ZOOM		] 	=	_T("Zoom");
	//m_strHDProp[DSC_PROP_FNUM		] 	=	_T("F#");
	m_strHDProp[DSC_PROP_SS			] 	=	_T("Shutter");
	m_strHDProp[DSC_PROP_ISO		] 	=	_T("ISO");
	m_strHDProp[DSC_PROP_MOVIE_SIZE ] 	=	_T("Size");

	//jhhan 16-09-09
	m_strHDProp[DSC_PROP_WB			]	=	_T("WB");
	m_strHDProp[DSC_PROP_COLOR_TEMPERATURE] = _T("Color Tem.");
	m_strHDProp[DSC_PROP_PW] = _T("PW");
//	m_strHDProp[DSC_PROP_PHOTO_STYLE]	=	_T("PS");		//Photo Style

	m_strTesting[TESTING_PROP_TOTAL_CNT]	= _T("PC Cnt");
	m_strTesting[TESTING_PROP_FAIL_CNT]		= _T("PC Fail");
	m_strTesting[TESTING_PROP_AS_CNT]		= _T("PC R-Cnt");
	m_strTesting[TESTING_PROP_AS_AVG]		= _T("PC Avg");
	m_strTesting[TESTING_PROP_AS_MIN]		= _T("PC Min");
	m_strTesting[TESTING_PROP_AS_MAX]		= _T("PC Max");
	m_strTesting[TESTING_PROP_AC_CNT]		= _T("DSC Cnt");
	m_strTesting[TESTING_PROP_AC_AVG]		= _T("DSC Avg");
	m_strTesting[TESTING_PROP_AC_MIN]		= _T("DSC Min");
	m_strTesting[TESTING_PROP_AC_MAX]		= _T("DSC Max");
	//-- 2013-09-13 hongsu@esmlab.com
	//-- For SendMessage
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();

	//CString m_strPage; //2016/07/15 Lee SangA page를 화면에 나타낼 string

};
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCListViewer::SizeCalculate(LPRECT lpRect)
{
	//-- Info
	int nRight = DSC_INFO_W;
	//-- Scroll
	nRight += DSC_BTN_W;
	//-- Adjust
	nRight += DSC_BTN_W;
	if(m_bShowAdj)	
		nRight += DSC_ADJ_CNT * DSC_ADJ_W;
	//-- Property
	nRight += DSC_BTN_W;
	if(m_bShowProp)
		nRight += DSC_PROP_CNT * DSC_PROP_W;

	/*nRight += DSC_BTN_W;
	if(m_bShowTesting)
	nRight += TESTING_PROP_CNT * DSC_TESTING_W;*/

	int nAll = GetItemCount();
	int nBottom = DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H + nAll*DSC_ROW_H;
	
	lpRect->right	= nRight;
	lpRect->bottom	= nBottom;
}

void CDSCListViewer::SetGridStatus(CDSCItem* pDSCItem, int nStatus)
{
	int n = 0;
	CString strDSCID = pDSCItem->GetDeviceDSCID();
	for(n = 0 ; n < DSC_INFO_CNT; n ++)	
	{	
		if(pDSCItem->m_pStaticInfo[n])
		{
			pDSCItem->m_pStaticInfo[n]->m_nIndex = ESMGetDSCIndex(strDSCID);

			pDSCItem->m_pStaticInfo[n]->bFlag = pDSCItem->m_bDelete;

			//if(pDSCItem->m_bDelete)
			//	ESMLog(1, _T("#Delete DSC %s"), pDSCItem->GetDeviceDSCID());
			//else
			//	ESMLog(1, _T("#Add DSC %s"), pDSCItem->GetDeviceDSCID());

			pDSCItem->m_pStaticInfo[n]->SetGridStatus(nStatus);
		}
	}

	for(n = 0 ; n < DSC_ADJ_CNT; n ++)	
	{	
		if(pDSCItem->m_pStaticAdj[n])	
		{
			pDSCItem->m_pStaticAdj[n]->bFlag = pDSCItem->m_bDelete;
			pDSCItem->m_pStaticAdj[n]->SetGridStatus(nStatus);	
		}
	}	

	for(n = 0 ; n < DSC_PROP_CNT; n ++)	
	{	
		if(pDSCItem->m_pStaticProp[n])	
		{
			pDSCItem->m_pStaticProp[n]->bFlag = pDSCItem->m_bDelete;
			pDSCItem->m_pStaticProp[n]->SetGridStatus(nStatus);	
		}
	}	
}	

void CDSCListViewer::SetGridStatus(int nRow, int nStatus)
{
	//-- 2013-09-05 hongsu@esmlab.com
	//-- Get Each DSC Info Class
	CDSCItem* pDSCItem = NULL;	
	//-- 2013-09-05 hongsu@esmlab.com
	pDSCItem = GetItemData(nRow);
	if(!pDSCItem)
		return;
	//-- 2013-09-06 hongsu@esmlab.com
	SetGridStatus(pDSCItem, nStatus);
}
LRESULT CDSCListViewer::OnDSCBtnMove(WPARAM w, LPARAM l)
{
	


	int nBtnID = (int)w;
	int posY = (int)l;
	switch(nBtnID)
	{
	case IDC_BTN_SCROLL	:	
		{
			if(m_bScroll)
			{
				m_nScroll = posY - 150;
				
				//jhhan 16-09-19 아래로 순서변경
				/*if(m_nScroll < 0)
					m_nScroll = 0;*/

				if(m_nScroll > m_nDrawBottom-100)
					m_nScroll = m_nDrawBottom-100;
				
				//jhhan 16-09-19
				if(m_nScroll < 0)
					m_nScroll = 0;

				m_BtnScroll.MoveWindow(0	, DSC_IMAGE_EDIT_TIMELINE_H+m_nScroll , DSC_BTN_W	,100);
				
				ESMSetScroll(m_nScroll);

				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message	= WM_ESM_LIST_SCROLL;
				::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
			}
		}
		break;
	}	
	return 0L;
}

LRESULT CDSCListViewer::OnDSCBtnDown(WPARAM w, LPARAM l)
{
	int nBtnID = (int)w;
	switch(nBtnID)
	{
	case IDC_BTN_SCROLL	:	
		m_bScroll = TRUE;
		break;
	}	
	return 0L;
}

LRESULT CDSCListViewer::OnDSCBtnClick(WPARAM w, LPARAM l)
{
	int nBtnID = (int)w;
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Select Row
	switch(nBtnID)
	{
	case IDC_BTN_SCROLL	:	m_bScroll = FALSE;	break;
	case IDC_BTN_FOLDER_ADJ	:	m_bShowAdj	= !m_bShowAdj	;	m_pView->ReDraw();	break;
	case IDC_BTN_TESTING : 
		{
			if(ESMGetLoadRecord())
			{
				AfxMessageBox(_T("Need to reconnect after LoadRecord"));
				break;
			}
			m_bShowTesting = !m_bShowTesting; m_pView->ReDraw();	break;
		}
	case IDC_BTN_FOLDER_PROP:
		{
			//jhhan 16-09-19
			if(ESMGetLoadRecord())
			{
				AfxMessageBox(_T("Need to reconnect after LoadRecord"));
				break;
			}
			int nY = (int)l;
			CDSCItem* pDSCItem = NULL;	
			pDSCItem = GetItemData(nY);

			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_LIST_SELECT;
			pMsg->pDest		= (LPARAM)pDSCItem;		
			::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);

			//SetTimer(111,4000,NULL);

			m_pView->GetPropertyListView();
			m_bShowProp = !m_bShowProp	;
			m_pView->ReDraw();
			break;
		}
	case IDC_BTN_HEADER_ID:
		//-- Sort
		m_pView->SortDSC();
		break;
	}	
	return 0L;
}

LRESULT CDSCListViewer::OnDSCGridClick(WPARAM w, LPARAM l)
{
	int nX = (int)w;
	int nY = (int)l;

	//-- Check Click Already Selected Item
	if(GetSelectLine() == nY)
		return 0L;
	
	//jhhan 170315
	ESMSetGridYPoint(nY);
	//TRACE("OnDSCGridClick : %d\r\n", nY*20+60+10);
	//TRACE("OnDSCGridClick nScroll : %d\r\n", ESMGetScroll());

	//-- 2013-09-05 hongsu@esmlab.com
	//-- Get Each DSC Info Class
	CDSCItem* pDSCItem = NULL;	
	pDSCItem = GetItemData(nY);

	if(pDSCItem)
		SetSelectItem(nY, pDSCItem);
	else
		return 0L;

	ESMLog(5, _T("Grid Select %s Item"), pDSCItem->GetDeviceDSCID());

	//Select DSC
	ESMSetSelectDSC(pDSCItem->GetDeviceDSCID());

	return 0L;
}

LRESULT CDSCListViewer::OnDSCGridRClick(WPARAM w, LPARAM l)
{
	
	int nX = (int)w;
	int nY = (int)l;

	//-- Check Click Already Selected Item
	/*if(GetSelectLine() == nY)
		return 0L;*/

	//jhhan 170315
	ESMSetGridYPoint(nY);
	//TRACE("OnDSCGridClick : %d\r\n", nY*20+60+10);
	//TRACE("OnDSCGridClick nScroll : %d\r\n", ESMGetScroll());

	//-- 2013-09-05 hongsu@esmlab.com
	//-- Get Each DSC Info Class
	CDSCItem* pDSCItem = NULL;	
	pDSCItem = GetItemData(nY);

	ESMLog(5, _T("Grid Select %s Item"), pDSCItem->GetDeviceDSCID());

	//Select DSC
	//ESMSetSelectDSC(pDSCItem->GetDeviceDSCID());
	CString strMsg;
	int nRet;
	//if(MessageBox(strMsg,_T("DSC Make Delete"),MB_OKCANCEL) ;

	if(ESMGetValue(ESM_VALUE_RTSP))
	{
		strMsg.Format(_T("[%s/%d]\nExcept DSC From 4DS"),pDSCItem->GetDeviceDSCID(),nY+1);
		nRet = MessageBox(strMsg,_T("DSC Except"),MB_OKCANCEL);
		if(nRet == 1)
		{
			ESMSendRTSPHttpSync(nY+1);
		}
		nRet = 0;
	}
	


	if(pDSCItem->m_bDelete == TRUE)
	{
		strMsg.Format(_T("[%s]\nMake Add!"), pDSCItem->GetDeviceDSCID());
		nRet = MessageBox(strMsg,_T("DSC Make Add"),MB_OKCANCEL);
	}
	else
	{
		strMsg.Format(_T("[%s]\nMake Delete!"), pDSCItem->GetDeviceDSCID());
		nRet = MessageBox(strMsg,_T("DSC Make Delete"),MB_OKCANCEL);
	}

	switch(nRet)
	{
	case 1:
		if(pDSCItem)
		{
			if(pDSCItem->m_bDelete == FALSE)
			{
				pDSCItem->m_bDelete = TRUE;
				SetDeleteItem(nY, pDSCItem,TRUE);
			}
			else
			{
				pDSCItem->m_bDelete = FALSE;
				SetDeleteItem(nY,pDSCItem,FALSE);
			}
			
			
		}

		ESMSetMakeDeleteDSC(pDSCItem->GetDeviceDSCID(),pDSCItem->m_bDelete);
		break;
	default:
		break;
	}
	
	return 0L;
}

void CDSCListViewer::SetSelectItem(int nRow, CDSCItem* pItem)
{
	//-- Change Previous Selected Item
	if(GetSelectLine() > -1 && GetItemCount() > GetSelectLine())
		SetGridStatus(GetItemData(GetSelectLine()), GRID_STATUS_NULL);

	//-- Set Select Row, Change Status
	SetSelectLine(nRow);

	if(!pItem->m_bDelete)
		SetGridStatus(pItem, GRID_STATUS_SELECTED);

	//-- 2013-09-12 yongmin.lee
	//-- Send To Liveview
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_LIST_SELECT;
	pMsg->pDest		= (LPARAM)pItem;		
	::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
}

void CDSCListViewer::SetDeleteItem(int nRow, CDSCItem* pItem,BOOL bDelete /*= FALSE*/)
{
	//pItem->m_bDelete = TRUE;
	int nState;
	if(bDelete == TRUE)
		nState = GRID_STATUS_DELETE;
	else
		nState = GRID_STATUS_MKADD;

	SetGridStatus(pItem, nState);
}

void CDSCListViewer::ChangeRow(int nLineA, int nLineB)
{
		 if(GetSelectLine() == nLineA)	SetSelectLine(nLineB);
	else if(GetSelectLine() == nLineB)	SetSelectLine(nLineA);
}


//------------------------------------------------------------------------------
//! @function	Message Handlers: Mouse
//------------------------------------------------------------------------------
void CDSCListViewer::OnLButtonDown(UINT nFlags, CPoint point) 
{	
	CRect rtBtn = GetScaleBtnRect();
	if(rtBtn.PtInRect(point))
	{
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_VIEW_SCALING;		
		::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);		
	}

	rtBtn = GetInsertIDBtnRect();
	if(rtBtn.PtInRect(point))
	{
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_VIEW_INSERTID;		
		::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);		
	}

	CScrollView::OnLButtonDown(nFlags, point);
}

//------------------------------------------------------------------------------
//! @function	3D Partner DSC
//------------------------------------------------------------------------------
void CDSCListViewer::ESMGet3DInfo(ESM3DInfo* p3DInfo)
{
	CDSCItem* pItem = NULL;
	BOOL bClockwise = ESMGetValue(ESM_VALUE_ADJ_ORDER_RTL);

	int nAll = GetItemCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = GetItemData(i);
		if(pItem->GetDeviceDSCID() == p3DInfo->strDSC[1])
		{
			if(bClockwise)
			{
				if(nAll == i +1)
				{
					//-- Set Info Left
					p3DInfo->strDSC[0] = p3DInfo->strDSC[1];
					pItem->SetAdjInfo(0, p3DInfo);
					
					//-- Set Info Right
					pItem = GetItemData(i-1);	
					p3DInfo->strDSC[1] = pItem ->GetDeviceDSCID();
					pItem->SetAdjInfo(1, p3DInfo);
				}
				else
				{
					//-- 2013-10-07 hongsu@esmlab.com
					//-- Set Info Right
					pItem->SetAdjInfo(1, p3DInfo);

					//-- Set Info Left
					pItem = GetItemData(i+1);				
					p3DInfo->strDSC[0] = pItem->GetDeviceDSCID();
					pItem->SetAdjInfo(0, p3DInfo);					
				}				
			}
			else
			{
				if(i == 0)
				{
					//-- Set Info Left
					p3DInfo->strDSC[0] = p3DInfo->strDSC[1];
					pItem->SetAdjInfo(0, p3DInfo);

					//-- Set Info Right
					pItem = GetItemData(i+1);	
					p3DInfo->strDSC[1] = pItem ->GetDeviceDSCID();
					pItem->SetAdjInfo(1, p3DInfo);
				}
				else
				{
					//-- 2013-10-07 hongsu@esmlab.com
					//-- Set Info Right
					pItem->SetAdjInfo(1, p3DInfo);

					//-- Set Info Left
					pItem = GetItemData(i-1);				
					p3DInfo->strDSC[0] = pItem->GetDeviceDSCID();
					pItem->SetAdjInfo(0, p3DInfo);	
				}							
			}
			break;
		}
	}
}




void CDSCListViewer::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDSCInfoViewBase::OnRButtonDown(nFlags, point);
}


BOOL CDSCListViewer::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	if(zDelta < 0)
		m_nScroll += 10;
	else
		m_nScroll -= 10;

	//jhhan 16-09-19 아래로 순서변경
	/*if(m_nScroll < 0)
	m_nScroll = 0;*/
	

	if(m_nScroll > m_nDrawBottom-100)
		m_nScroll = m_nDrawBottom-100;

	//ESMLog(1, _T("Scroll %d %d %d"),nFlags, zDelta, m_nScroll);

	//jhhan 16-09-19
	if(m_nScroll < 0)
		m_nScroll = 0;

	
	m_BtnScroll.MoveWindow(0	, DSC_IMAGE_EDIT_TIMELINE_H+m_nScroll , DSC_BTN_W	,100);
	ESMSetScroll(m_nScroll);

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_LIST_SCROLL;
	::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);

	return CDSCInfoViewBase::OnMouseWheel(nFlags, zDelta, pt);
}


void CDSCListViewer::OnTimer(UINT_PTR nIDEvent)
{
	//jhhan 16-09-19
	if(ESMGetLoadRecord())
	{
		AfxMessageBox(_T("Need to reconnect after LoadRecord"));
	
	}
	int nY = (int)1;
	CDSCItem* pDSCItem = NULL;	
	pDSCItem = GetItemData(nY);

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_LIST_SELECT;
	pMsg->pDest		= (LPARAM)pDSCItem;		
	::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);

	

	m_pView->GetPropertyListView();
	m_bShowProp = !m_bShowProp	;
	m_pView->ReDraw();

	CDSCInfoViewBase::OnTimer(nIDEvent);
}

void CDSCListViewer::SetMouseWheel( BOOL bUp )
{
	short nZDelta = -120;
	if (bUp)
		nZDelta = 120;

	OnMouseWheel(0, nZDelta, CPoint());
}
