////////////////////////////////////////////////////////////////////////////////
//
//	DSCFrameSelectorDraw.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-06
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCFrameSelector.h"
#include "ESMTimeLineObject.h"
#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//----------------------------------------------------------------
//-- 2013-04-22 hongsu.jung
//-- Thread Function
//----------------------------------------------------------------
void _DrawFrameSelector(void *param)
{
	/*
	CDSCFrameSelector* pView = (CDSCFrameSelector*)param;

	CDC* pDC = pView->GetDC();
	if(!pDC)
	return;

	/*
	//-- MEM DC

	CRect rect;
	pView->GetClientRect(&rect);

	// draw Background
	CDC mDC;
	CBitmap mBitmap;
	mDC.CreateCompatibleDC(pDC);
	mBitmap.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height()); 

	mDC.SelectObject(&mBitmap);	
	mDC.PatBlt(0,0,rect.Width(),rect.Height(),SRCCOPY);

	---

	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
	*/

	/*
	//-- 2013-09-05 hongsu@esmlab.com
	//-- TimeLine
	pView->DrawTimeLine(pDC);
	//-- 2013-09-05 hongsu@esmlab.com
	//-- DSC Recording Line
	pView->DrawMovieLine(pDC);
	//-- 2013-09-28 hongsu@esmlab.com
	//-- Draw TimeLine Object
	pView->DrawTimeLineObject(pDC);	

	//-- 2013-09-30 hongsu@esmlab.com
	//-- Check TimeLine Object 
	if(pView->m_pTimeLineObject)
	pView->m_pTimeLineObject->DrawObject();

	_endthread();
	*/
}


//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCFrameSelector::OnDraw(CDC* pDC)
{
	// update the scroll bars
	UpdateSizes();
	DrawEditor();	
}

void CDSCFrameSelector::DrawEditor(BOOL bRemote/* = FALSE*/)
{	
	CDC* pDC = GetDC();
	if(!pDC)
		return;

	//-- MEM DC

	CRect rect;
	GetClientRect(&rect);

	// draw Background
	CDC mDC;
	CBitmap mBitmap;
	mDC.CreateCompatibleDC(pDC);
	mBitmap.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height()); 

	mDC.SelectObject(&mBitmap);	
	mDC.PatBlt(0,0,rect.Width(),rect.Height(),SRCCOPY);

	//-- 2013-09-05 hongsu@esmlab.com
	//-- TimeLine
	DrawTimeLine(&mDC);
	//-- 2013-09-05 hongsu@esmlab.com
	//-- DSC Recording Line
	DrawMovieLine(&mDC,bRemote);
	//-- 2013-09-28 hongsu@esmlab.com
	//-- Draw TimeLine Object
	DrawTimeLineObject(&mDC);	

	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);

	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();
	ReleaseDC(pDC);


	// 	//-- 2013-09-30 hongsu@esmlab.com
	// 	//-- Check TimeLine Object 
	if(m_pTimeLineObject)
		m_pTimeLineObject->DrawObject();

}

void CDSCFrameSelector::DrawTimeLine(CDC* pDC)
{
	//TRACE(_T("####Time %d\n"), GetTickCount());

	// [12/18/2013 Administrator]
	if(!pDC->m_hDC)
		return;

	// draw Background
	CRect rtTop;
	GetClientRect(&rtTop);
	//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
	pDC->FillSolidRect(rtTop, COLOR_BASIC_BG_5 );
#else
	pDC->FillSolidRect(rtTop, RGB(0,0,0)/*COLOR_BASIC_BG_1*/ );
#endif

	//-- 2013-10-01 hongsu@esmlab.com
	//-- For Drawing Text 
	CRect rtTxt;
	CString strTxt;
	rtTxt.top		= m_ptScroll.y;
	rtTxt.bottom	= m_ptScroll.y+m_nTickMajorHeight;

	SetFrameFont(pDC, 13, _T("Noto Sans CJK KR Regular"));
	pDC->SetTextColor(COLOR_WHITE);
	//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
	pDC->SetBkColor(COLOR_BASIC_BG_5);
#else
	pDC->SetBkColor(RGB(0,0,0)/*COLOR_BASIC_BG_3*/);
#endif
	// draw the top portion
	rtTop.bottom = rtTop.top + m_nHeaderTop;

	//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
	pDC->FillSolidRect(rtTop, COLOR_BASIC_BG_5);
#else
	pDC->FillSolidRect(rtTop, RGB(0x16,0x16,0x16)/*COLOR_BASIC_BG_3*/);
#endif
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Calculate Scroll size 
	rtTop.left += m_ptScroll.x;
	rtTop.right += m_ptScroll.x;
	rtTop.top += m_ptScroll.y;
	rtTop.bottom += m_ptScroll.y;	

	CRect rtTick;
	rtTick.top = m_ptScroll.y;
	rtTick.bottom = rtTick.top + m_nHeaderTop;
	rtTick.left = GetXFromTime(0);
	rtTick.right = rtTick.left + GetSpanWidth();

	CPen penMinorTicks;
	CPen penMajorTicks;
	penMinorTicks.CreatePen(PS_SOLID  ,1 , RGB(0x33,0x33,0x33)/*COLOR_BASIC_FG_1*/ );
	penMajorTicks.CreatePen(PS_SOLID  ,1 , RGB(0x79,0x79,0x79)/*COLOR_WHITE*/);

	int x;
	int TX;
	//-- 2013-09-06 hongsu@esmlab.com
	//-- mill second 
	pDC->SelectObject(&penMinorTicks);	
	for(x = 0; x < m_nTimeMax; x+=m_nTickFreqMinor)
	{
		TX = GetXFromTime(x);
		pDC->MoveTo(TX , m_ptScroll.y);
		pDC->LineTo(TX, m_ptScroll.y+m_nTickMinorHeight);
	}

	//-- 2013-09-06 hongsu@esmlab.com
	//-- second
	pDC->SelectObject(&penMajorTicks);
	for(x = 0; x<=m_nTimeMax; x+=m_nTickFreqMajor)
	{
		if(ESMGetViewScaling())		//jhhan 16-12-13 시간 표시 간격 조정 5초간격, 10초간격
		{
			if(m_nTimeMax > m_nTickFreqMajor * 60)			//60sec -> 10sec
			{
				if( x % (m_nTickFreqMajor*10) == 0)
				{
					TX = GetXFromTime(x);
					pDC->MoveTo(TX,m_ptScroll.y);
					pDC->LineTo(TX,m_ptScroll.y+m_nTickMajorHeight);

					strTxt.Format(_T("%d"),x/1000);
					rtTxt.left	= TX + DSC_TIMELINE_SEC_GAP;
					rtTxt.right	= TX + m_ptScroll.x+ GetXFromTime(1000)*3;
					pDC->DrawText(strTxt, rtTxt,  DT_LEFT | DT_BOTTOM | DT_SINGLELINE);
				}
				else
				{
					TX = GetXFromTime(x);
					pDC->MoveTo(TX,m_ptScroll.y);
					pDC->LineTo(TX,m_ptScroll.y+m_nTickMinorHeight);
				}

			}else if(m_nTimeMax > m_nTickFreqMajor * 10)	//10sec -> 5sec
			{
				if( x % (m_nTickFreqMajor*5) == 0)
				{
					TX = GetXFromTime(x);
					pDC->MoveTo(TX,m_ptScroll.y);
					pDC->LineTo(TX,m_ptScroll.y+m_nTickMajorHeight);

					strTxt.Format(_T("%d"),x/1000);
					rtTxt.left	= TX + DSC_TIMELINE_SEC_GAP;
					rtTxt.right	= TX + m_ptScroll.x+ GetXFromTime(1000)*2;
					pDC->DrawText(strTxt, rtTxt,  DT_LEFT | DT_BOTTOM | DT_SINGLELINE);
				}
				else
				{
					TX = GetXFromTime(x);
					pDC->MoveTo(TX,m_ptScroll.y);
					pDC->LineTo(TX,m_ptScroll.y+m_nTickMinorHeight);
				}
			}else											//Normal
			{
				TX = GetXFromTime(x);
				pDC->MoveTo(TX,m_ptScroll.y);
				pDC->LineTo(TX,m_ptScroll.y+m_nTickMajorHeight);

				//-- 2013-10-01 hongsu@esmlab.com
				//-- Draw Second
				strTxt.Format(_T("%d sec"),x/1000);
				rtTxt.left	= TX + DSC_TIMELINE_SEC_GAP;
				rtTxt.right	= TX + m_ptScroll.x+ GetXFromTime(2000);
				pDC->DrawText(strTxt, rtTxt,  DT_LEFT | DT_BOTTOM | DT_SINGLELINE);
			}


		}else
		{
			TX = GetXFromTime(x);
			pDC->MoveTo(TX,m_ptScroll.y);
			pDC->LineTo(TX,m_ptScroll.y+m_nTickMajorHeight);

			//-- 2013-10-01 hongsu@esmlab.com
			//-- Draw Second
			strTxt.Format(_T("%d sec"),x/1000);
			rtTxt.left	= TX + DSC_TIMELINE_SEC_GAP;
			rtTxt.right	= TX + m_ptScroll.x+ GetXFromTime(2000);
			pDC->DrawText(strTxt, rtTxt,  DT_LEFT | DT_BOTTOM | DT_SINGLELINE);
		}
		
	}	
}

void CDSCFrameSelector::DrawMovieLine(CDC* pDC,BOOL bRemote/* = FALSE*/)
{
	int nDrawLineTop = m_nHeaderTop;
	int nDrawLineLeft= 0 ; //m_nLineLeftMargin;
	int nAll = GetItemCount();

	CPen penLine;
	penLine.CreatePen(PS_SOLID  ,1 , COLOR_BASIC_BG_2 );
	pDC->SelectObject(&penLine);

	// [12/20/2013 Administrator]
	int nIconPos = GetXFromTime(0);

	CDSCItem* pItem = NULL;

	int nScroll = ESMGetScroll();
	for(int i = 0 ; i < nAll ; i ++)
	{
		//-- 2013-09-06 hongsu@esmlab.com
		//-- Draw Info
		if(i*DSC_ROW_H >= nScroll)
		{
			pDC->MoveTo(nDrawLineLeft										, nDrawLineTop+ i*DSC_ROW_H - nScroll);
			pDC->LineTo(nDrawLineLeft + GetSpanWidth() + m_nLineLeftMargin	, nDrawLineTop+ i*DSC_ROW_H - nScroll);
		}


		//-- 2013-09-12 hongsu@esmlab.com
		//-- Check Icon
		pItem = GetItemData(i);
		if(pItem)
		{
			int nStatus = pItem->GetDSCStatus();
			//-- Get Item Time
			switch(nStatus)
			{
			case SDI_STATUS_CAPTURE_OK:
				if(i*DSC_ROW_H >= nScroll)
					DrawIcon(pDC, pItem, nIconPos, nDrawLineTop+ i*DSC_ROW_H + 2- nScroll);
				break;
			case SDI_STATUS_REC:
			case SDI_STATUS_REC_CONVERT:
			case SDI_STATUS_GET_FILE:
				if(i*DSC_ROW_H >= nScroll)
				{
					DrawFilm(pDC, nDrawLineTop+ i*DSC_ROW_H + 2- nScroll, nStatus);
					DrawSelectedSpot(pDC,bRemote);
				}
				break;
			case SDI_STATUS_REC_READY:	//jhhan 180829
				if(i*DSC_ROW_H >= nScroll)
				{
					DrawFilm(pDC, nDrawLineTop+ i*DSC_ROW_H + 2- nScroll, SDI_STATUS_ERROR);
					DrawSelectedSpot(pDC,bRemote);
				}
				break;
			case SDI_STATUS_REC_FINISH:
				if(i*DSC_ROW_H >= nScroll)
				{
					DrawFilm(pDC, nDrawLineTop+ i*DSC_ROW_H + 2- nScroll, nStatus);
					DrawSelectedSpot(pDC,bRemote);
				}
				break;
			case SDI_STATUS_ERROR_TICK:	//jhhan 180829
			case SDI_STATUS_ERROR:
				if(i*DSC_ROW_H >= nScroll)
				{
					DrawFilm(pDC, nDrawLineTop+ i*DSC_ROW_H + 2- nScroll, SDI_STATUS_ERROR);
					DrawSelectedSpot(pDC,bRemote);
				}
				break;
			default:
				{
					if(i*DSC_ROW_H >= nScroll)
					{
						DrawFilm(pDC, nDrawLineTop+ i*DSC_ROW_H + 2- nScroll, SDI_STATUS_REC_FINISH);
						DrawSelectedSpot(pDC,bRemote);
					}
					break;
				}
				break;
			}	
		}
		else
		{
			if(i*DSC_ROW_H >= nScroll)
			{
				DrawFilm(pDC, nDrawLineTop+ i*DSC_ROW_H + 2- nScroll, SDI_STATUS_REC_FINISH);
				DrawSelectedSpot(pDC,bRemote);
			}
		}
		
	}


	//-- 2013-09-12 hongsu@esmlab.com
	//-- Draw Last Line
	pDC->MoveTo(nDrawLineLeft										, nDrawLineTop+ nAll*DSC_ROW_H - nScroll);
	pDC->LineTo(nDrawLineLeft + GetSpanWidth() + m_nLineLeftMargin	, nDrawLineTop+ nAll*DSC_ROW_H - nScroll);

	m_StaticLine.Invalidate();
	m_SelectLine1.Invalidate();
	m_SelectLine2.Invalidate();
	m_SelectLine3.Invalidate();
	m_SelectLine4.Invalidate();

	for(int i = 0; i < m_nMultiLine+1; i ++)
	{
		m_MultiSelectLine[i].Invalidate();
	}
}


//------------------------------------------------------------------------------
//! @function			
//! @brief		Message From Mouse Event		
//! @date		2013-09-09
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCFrameSelector::MoveTimeLine(CPoint pt)
{
	CRect rect;
	GetClientRect(rect);

	//-- Minimum
	if(pt.x < DSC_ICON_TIMESPOT_W/2)
		pt.x = DSC_ICON_TIMESPOT_W/2;
	//-- maximum
	if(pt.x > GetSpanWidth() + m_nLineLeftMargin )
		pt.x = GetSpanWidth() + m_nLineLeftMargin ;


	m_StaticTri.MoveWindow(pt.x - DSC_ICON_TIMESPOT_W/2, DSC_IMAGE_EDIT_TIMELINE_H , DSC_ICON_TIMESPOT_W, DSC_ICON_TIMESPOT_H);
	m_StaticTri.Invalidate();

	//-- Draw Spot Time
	m_StaticTime.MoveWindow(pt.x + DSC_ICON_TIMESPOT_W/2 + 2, DSC_IMAGE_EDIT_TIMELINE_H , DSC_ICON_TIMESPOT_W * 4, DSC_ICON_TIMESPOT_H);
	int nSpotTime = GetTimeFromX(pt.x);
	double dTime = nSpotTime/ 1000.0;
	CString strTime;
	strTime.Format(_T("%02.03f secs"), (float)dTime);
	m_StaticTime.SetWindowText(strTime);	
	m_StaticTime.Invalidate();

	//-- Draw Red Time Line
	int v_scroll_width = GetSystemMetrics(SM_CXVSCROLL);
	m_StaticLine.MoveWindow(pt.x, DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H , 1, rect.Height() - (DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H) );

	m_StaticTri.ShowWindow(TRUE);
	m_StaticTime.ShowWindow(TRUE);
	m_StaticLine.ShowWindow(TRUE);
}

//------------------------------------------------------------------------------
//! @function			
//! @brief		Select Time Line
//! @date		2014-09-21
//! @owner		changdo kim
//! @return			
//! @revision		
//----------------------------------------------------------6--------------------
void CDSCFrameSelector::SelectTimeLine(int nSelectTime, int nLine)
{
	CPoint pt;
	pt.x = GetXFromTime(nSelectTime);
	pt.y = 0;	

	CRect rect;
	GetClientRect(rect);

	//-- Minimum
	if(pt.x < DSC_ICON_TIMESPOT_W/2)
		pt.x = DSC_ICON_TIMESPOT_W/2;
	//-- maximum
	if(pt.x > GetSpanWidth() + m_nLineLeftMargin )
		pt.x = GetSpanWidth() + m_nLineLeftMargin ;

	//-- Draw Yellow Time Line
	int v_scroll_width = GetSystemMetrics(SM_CXVSCROLL);

	/*switch(nLine)
	{
	case 0:
		m_SelectLine1.MoveWindow(pt.x, DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H , 1, rect.Height() - (DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H) );
		m_SelectLine1.ShowWindow(TRUE);
		break;
	case 1:
		m_SelectLine2.MoveWindow(pt.x, DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H , 1, rect.Height() - (DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H) );
		m_SelectLine2.ShowWindow(TRUE);
		break;
	case 2:
		m_SelectLine3.MoveWindow(pt.x, DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H , 1, rect.Height() - (DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H) );
		m_SelectLine3.ShowWindow(TRUE);
		break;
	case 3:
		m_SelectLine4.MoveWindow(pt.x, DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H , 1, rect.Height() - (DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H) );
		m_SelectLine4.ShowWindow(TRUE);
		break;
	}*/

	if(nLine > MAX_SELECT_LINE)
	{
 		AfxMessageBox(_T("Select Point is over 50!"));
		return;
	}

	//ESMLog(1,_T("SelectTimeLine %d, Line %d ,pt.x %d\n"),nSelectTime, nLine, pt.x );
	m_MultiSelectLine[nLine].MoveWindow(pt.x, DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H , 1, rect.Height() - (DSC_IMAGE_EDIT_TIMELINE_H + DSC_ICON_TIMESPOT_H) );
	m_MultiSelectLine[nLine].ShowWindow(TRUE);

	if(nLine > m_nMultiLine)
		m_nMultiLine = nLine;


}

void CDSCFrameSelector::SetSelectTimeLine(int nSelectTime, int nLine)
{
	m_nSelectLineTime[nLine] = nSelectTime;
	//m_nSelectLineTime = GetRecTime();
	SelectTimeLine(m_nSelectLineTime[nLine], nLine);
}

int  CDSCFrameSelector::SetSelectDscIndex(int nSelectTime)
{
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	int nDscCount =  arDSCList.GetSize();	
	int nOrderDirection = ESMGetRecordingInfoInt(_T("OrderDirection"), DSC_REC_DELAY_FORWARDDIRECTION);
	CString strSelectDSC;

	strSelectDSC = ESMGetSelectDSC();

	if(ESMGetValue(ESM_VALUE_AUTODETECT))
		strSelectDSC = GetAutoDscID(strSelectDSC);

	if(strSelectDSC.IsEmpty())
	{
		strSelectDSC = ESMGetSelectDSC();

		if(strSelectDSC.IsEmpty())// 파일 유무까지 확인.
			strSelectDSC = ESMGetMiddleDSCSearch(nOrderDirection);
	}

	int nSelectDsc = ESMGetDSCIndex(strSelectDSC);

	int nFIdx = ESMGetFrameIndex(nSelectTime);

	int nSelectedIndex = -1;

	for( int i =nSelectDsc ;i < nDscCount; i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		CString strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);


		if(!strFileName.GetString())
			continue;


		CESMFileOperation fo;
		//ESMLog(5,_T("SetSelectDscIndex: %s[%d]"),strFileName,strFileName.GetLength());			
		if(fo.IsFileExist(strFileName))
		{
#if/*ndef*/ _TRAY_CATCH
			CFile file;
			CFileException e;
			if (file.Open(strFileName, CFile::modeRead, &e))
			{
				if(file.GetLength() > 0)
				{
					m_nSelectLineDSC = pItem->GetDeviceDSCID();
					nSelectedIndex = i;

					file.Close();
					break;
				}
			}else
			{
				ESMLog(0, _T("CFileException : %d"), e.m_cause);
			}
#else
			try 
			{
				CFile file;
				if (file.Open(strFileName, CFile::modeRead))
				{
					if(file.GetLength() > 0)
					{
						m_nSelectLineDSC = pItem->GetDeviceDSCID();
						nSelectedIndex = i;

						file.Close();
						//ESMLog(5,_T("Load %s"),strFileName);
						break;
					}
				}
			}
			catch(...)
			{
				ESMLog(0, _T("CFileException"));
				//e->ReportError();
			}
			
#endif
			
		}
		else
		{
			ESMLog(5,_T("Not Exist file"));			
		}
	}
	return nSelectedIndex;
}

int  CDSCFrameSelector::SetSelectAUTOLeftDscIndex(int nSelectTime)
{
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	int nDscCount =  arDSCList.GetSize();	
	int nOrderDirection = ESMGetRecordingInfoInt(_T("OrderDirection"), DSC_REC_DELAY_FORWARDDIRECTION);
	CString strSelectDSC;

	//170529 hjcho revision
	strSelectDSC = ESMGetTrackInfo().st_AxisInfo[1].strCamID;

	if(strSelectDSC.IsEmpty())
	{
		strSelectDSC = ESMGetMiddleDSCSearch(nOrderDirection); // 파일 유무까지 확인.
	}

	int nSelectDsc = ESMGetDSCIndex(strSelectDSC);

	if(nSelectDsc == -1)
	{
		strSelectDSC = ESMGetMiddleDSCSearch(nOrderDirection);
		nSelectDsc = ESMGetDSCIndex(strSelectDSC);
	}

	int nFIdx = ESMGetFrameIndex(nSelectTime);

	int nSelectedIndex = 0;
	for( int i =nSelectDsc ;i < nDscCount; i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		CString strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);

		CESMFileOperation fo;
		if(fo.IsFileExist(strFileName))
		{
			CFile file;
			if (file.Open(strFileName, CFile::modeRead))
			{
				if(file.GetLength() > 0)
				{
					m_nSelectLineDSC = pItem->GetDeviceDSCID();
					nSelectedIndex = i;

					file.Close();
					break;
				}
			}
		}
	}
	return nSelectedIndex;
}
int  CDSCFrameSelector::SetSelectAUTORightDscIndex(int nSelectTime)
{
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	int nDscCount =  arDSCList.GetSize();	
	int nOrderDirection = ESMGetRecordingInfoInt(_T("OrderDirection"), DSC_REC_DELAY_FORWARDDIRECTION);
	CString strSelectDSC;
	//track_info a = ESMGetTrack_Info();

	strSelectDSC = ESMGetTrackInfo().st_AxisInfo[1].strCamID;

	if(strSelectDSC.IsEmpty())
	{
		strSelectDSC = ESMGetMiddleDSCSearch(nOrderDirection); // 파일 유무까지 확인.
	}


	int nSelectDsc = ESMGetDSCIndex(strSelectDSC);

	if(nSelectDsc == -1)
	{
		strSelectDSC = ESMGetMiddleDSCSearch(nOrderDirection);
		nSelectDsc = ESMGetDSCIndex(strSelectDSC);
	}

	int nFIdx = ESMGetFrameIndex(nSelectTime);

	int nSelectedIndex = 0;
	for( int i =nSelectDsc ;i < nDscCount; i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		CString strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);

		CESMFileOperation fo;
		if(fo.IsFileExist(strFileName))
		{
			CFile file;
			if (file.Open(strFileName, CFile::modeRead))
			{
				if(file.GetLength() > 0)
				{
					m_nSelectLineDSC = pItem->GetDeviceDSCID();
					nSelectedIndex = i;

					file.Close();
					break;
				}
			}
		}
	}
	return nSelectedIndex;
}
void CDSCFrameSelector::ReflashSelectTimeLine()
{
	for(int i = 0; i < m_nMultiLine+1; i++)
		SelectTimeLine(m_nSelectLineTime[i], i);

}


void CDSCFrameSelector::DrawRecBar()
{
	//-- 2013-09-25 hongsu@esmlab.com
	//-- Move Time Line
	CPoint pt;
	pt.x = GetXFromTime(m_nRecTime);
	pt.y = 0;	
	MoveTimeLine(pt);
}


//------------------------------------------------------------------------------
//! @function			
//! @brief		SDI_STATUS_REC:
//!				SDI_STATUS_REC_CONVERT:
//!				SDI_STATUS_REC_FINISH:
//!				SDI_STATUS_GET_FILE:
//! @date		2013-09-29
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCFrameSelector::DrawFilm(CDC* pDC, int nTop, int nRecStatus)
{
	//-- Draw Background
	CRect rt;
	int nRight = GetXFromTime(m_nRecTime);

	rt.left = 0;
	rt.right = nRight; 
	rt.top = nTop;
	rt.bottom = rt.top + DSC_ICON_PICTURE_H;

	pDC->FillRect(rt, &CBrush(RGB(0,0,0)));

	//-- Draw Film
	CRect rtFilm;
	rtFilm.top		= rt.top	/*+ 2*/;
	rtFilm.bottom	= rt.bottom	/*- 2*/;
	rtFilm.left		= INNER_FILM_RECT_LENGTH / 6 + m_nLineLeftMargin;
	rtFilm.right	= rtFilm.left + INNER_FILM_RECT_LENGTH;


	TRIVERTEX vertex[2] ;
	vertex[0].x     = rtFilm.left;
	vertex[0].y     = rtFilm.top;	
	vertex[0].Alpha = 0x8000;

	vertex[1].x     = rtFilm.right;
	vertex[1].y     = rtFilm.bottom;
	vertex[1].Alpha = 0x0000;

	GRADIENT_RECT gRect;
	gRect.UpperLeft  = 0;
	gRect.LowerRight = 1;


	//-- 1st 4E615D	: DC 2A 1E
	//-- 2nd 5A7870	: E4 46 26
	//-- 3rd 94C2BF	: F0 70 32
	//-- 4th BBD8D4	: FB 96 3D
	switch(nRecStatus)
	{
	case SDI_STATUS_REC			:	
		{
			vertex[0].Red   = 0xDC00;
			vertex[0].Green = 0x2A00;
			vertex[0].Blue  = 0x1E00;
			vertex[1].Red   = 0xE400;
			vertex[1].Green = 0x4600;
			vertex[1].Blue  = 0x2600;
		}
		break;
	case SDI_STATUS_GET_FILE	:	
		{
			vertex[0].Red   = 0xE400;
			vertex[0].Green = 0x4600;
			vertex[0].Blue  = 0x2600;
			vertex[1].Red   = 0xF000;
			vertex[1].Green = 0x7000;
			vertex[1].Blue  = 0x3200;
		}
		break;
	case SDI_STATUS_REC_CONVERT	:	
		{
			vertex[0].Red   = 0xF000;
			vertex[0].Green = 0x7000;
			vertex[0].Blue  = 0x3200;
			vertex[1].Red   = 0xFB00;
			vertex[1].Green = 0x9600;
			vertex[1].Blue  = 0x3D00;
		}
		break;
	case SDI_STATUS_REC_FINISH	:	
		{
			/*vertex[0].Red   = 0xB900;
			vertex[0].Green = 0xF400;
			vertex[0].Blue  = 0xFF00;
			vertex[1].Red   = 0xE600;
			vertex[1].Green = 0xE600;
			vertex[1].Blue  = 0xE600;*/
			vertex[0].Red   = 0x2C00;
			vertex[0].Green = 0x2C00;
			vertex[0].Blue  = 0x2C00;
			vertex[1].Red   = 0x2C00;
			vertex[1].Green = 0x2C00;
			vertex[1].Blue  = 0x2C00;

		}		
		break;
	case SDI_STATUS_REC_READY :
		{
			vertex[0].Red   = 0xDC00;
			vertex[0].Green = 0x2A00;
			vertex[0].Blue  = 0x1E00;
			vertex[1].Red   = 0xE400;
			vertex[1].Green = 0x4600;
			vertex[1].Blue  = 0x2600;
		}
		break;
	case SDI_STATUS_ERROR : //jhhan 180829
		{
			vertex[0].Red   = 0x5300;
			vertex[0].Green = 0x5300;
			vertex[0].Blue  = 0x5300;
			vertex[1].Red   = 0x5300;
			vertex[1].Green = 0x5300;
			vertex[1].Blue  = 0x5300;
		}
		break;
	default:
		{
			/*//B9F4FF
			vertex[0].Red   = 0xB900;
			vertex[0].Green = 0xF400;
			vertex[0].Blue  = 0xFF00;
			vertex[1].Red   = 0xF900;
			vertex[1].Green = 0x8D00;
			vertex[1].Blue  = 0x3B00;*/

			vertex[0].Red   = 0xDC00;
			vertex[0].Green = 0x2A00;
			vertex[0].Blue  = 0x1E00;
			vertex[1].Red   = 0xE400;
			vertex[1].Green = 0x4600;
			vertex[1].Blue  = 0x2600;
		}
		break;
	}


	while(1)
	{
		pDC->GradientFill(vertex, 2, &gRect, 1, GRADIENT_FILL_RECT_H);	
		rtFilm.left		= rtFilm.right + INNER_FILM_RECT_LENGTH / 6;
		rtFilm.right	= rtFilm.left + INNER_FILM_RECT_LENGTH;

		vertex[0].x     = rtFilm.left;		
		vertex[1].x     = rtFilm.right;


		if(rtFilm.right > nRight)
		{
			rtFilm.right	= nRight - INNER_FILM_RECT_LENGTH / 6;
			vertex[0].x     = rtFilm.left;
			vertex[0].y     = rtFilm.top;
			vertex[1].x     = rtFilm.right;
			vertex[1].y     = rtFilm.bottom;

			if(rtFilm.right > rtFilm.left)
				pDC->GradientFill(vertex, 2, &gRect, 1, GRADIENT_FILL_RECT_H);
			break;
		}

	}
}

void CDSCFrameSelector::DrawIcon(CDC* pDC, CDSCItem* pItem, int nLeft, int nTop)
{
	int nTime;
	nTime = pItem->GetPictureTime();
	int nIcopPos = GetXFromTime(nTime);

	if(!pItem->m_memImageDC)
	{
		pItem->m_memImageDC = new CDC;
		if(!pItem->m_memImageDC->CreateCompatibleDC(pDC))
			return;

		HBITMAP		hBmp;
		CBitmap		cBitmap;
		hBmp = (HBITMAP)::LoadImage( AfxGetInstanceHandle(),  MAKEINTRESOURCE(IDB_ESM_PICTURE),IMAGE_BITMAP , 0,0, LR_LOADMAP3DCOLORS );
		cBitmap.Attach(hBmp);

		pItem->m_memImageDC->SelectObject(&cBitmap);
		int nX = nIcopPos;
		pDC->StretchBlt(nX, nTop, DSC_ICON_PICTURE_W, DSC_ICON_PICTURE_H, pItem->m_memImageDC, 0, 0, DSC_ICON_PICTURE_W, DSC_ICON_PICTURE_H, SRCCOPY);

	}
	else
	{
		int nX = nIcopPos;
		pDC->StretchBlt(nX, nTop,  DSC_ICON_PICTURE_W,  DSC_ICON_PICTURE_H, pItem->m_memImageDC, 0, 0, DSC_ICON_PICTURE_W, DSC_ICON_PICTURE_H, SRCCOPY);
	}
}

void CDSCFrameSelector::DrawSelectedSpot(CDC* pDC,BOOL bRemote/* = FALSE*/)
{
	int nScroll = ESMGetScroll();
	int nDrawLineTop = m_nHeaderTop;
	nDrawLineTop = nDrawLineTop+ m_nSelectedDsc*DSC_ROW_H - nScroll;


	if( m_bIsShowSpot == FALSE)
		return;

	int nRight = GetXFromTime(m_nSelectedTime);

	CPen pen;
	
	if(bRemote)
		pen.CreatePen( PS_DOT, 2, RGB(191,216,191) );  
	else
		pen.CreatePen( PS_DOT, 2, RGB(216,191,216) );  

	CPen* oldPen = pDC->SelectObject( &pen );
	
	CBrush brush;
	if(bRemote)
		brush.CreateSolidBrush(  RGB(0,69,255) );   
	else
		brush.CreateSolidBrush(  RGB(255,69,0) );   

	CBrush* oldBrush = pDC->SelectObject( &brush );

	CRect rt;
	rt.left = nRight;
	rt.right = rt.left + DSC_ICON_PICTURE_H; 
	rt.top = nDrawLineTop;
	rt.bottom = rt.top + DSC_ICON_PICTURE_H;

	rt.left = nRight - DSC_ICON_PICTURE_H/4;
	rt.right = rt.left + DSC_ICON_PICTURE_H *3 /4; 
	rt.top = nDrawLineTop + DSC_ICON_PICTURE_H/4 + 1;
	rt.bottom = rt.top + DSC_ICON_PICTURE_H*3 /4;
	//if(nDrawLineTop > nScroll)
	//jhhan 170314
	if(nDrawLineTop >= m_nHeaderTop)
	{
		pDC->Ellipse(rt);

	}
	//ESMLog(5, _T("DrawSelectedSpot : %d"), nDrawLineTop);
	pDC->SelectObject( oldPen   ); 
	pDC->SelectObject( oldBrush );  
}

void CDSCFrameSelector::DrawTimeLineObject(CDC* pDC)
{
	//-- 2013-09-27 hongsu@esmlab.com
	//-- Draw TimeLine Object
	int nScroll = ESMGetScroll();
	if(m_bLBtnDown && m_spotSelect.strDSC)
	{
		//-- 2013-09-27 hongsu@esmlab.com
		//-- Draw Timeline Object
		CPoint ptS, ptE;
		ptS.x = GetXFromTime(m_spotSelect.nTime);
		ptS.y = GetYFromLine(m_spotSelect.nSelectLine);
		ptS.y +=  DSC_ROW_H/2;//DSC_ROW_H/4;	

		if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		{
			ptE.x = GetXFromTime(GetTimeFromX(m_ptMouse.x));
		}
		else
		{
			ptE.x = GetXFromTime(DEFAULT_ESM_PICTURE_DRAGTIME);
		}

		int nEndLine = GetLineFromY(m_ptMouse.y+nScroll);
		if(nEndLine < 0)
			return;
		if(nEndLine > GetItemCount() - 1)
			return;

		ptE.y = GetYFromLine(nEndLine);
		ptE.y +=  DSC_ROW_H/2;//DSC_ROW_H/4;


		if(nScroll>0)
		{
			ptS.y -= nScroll;
			ptE.y -= nScroll;
		}

		CPen pen;
		if( (ptS.x == ptE.x) || (ptS.y == ptE.y))
			pen.CreatePen(PS_SOLID,  DSC_ROW_H/2, COLOR_GREEN );
		else
			pen.CreatePen(PS_SOLID,  DSC_ROW_H/2, COLOR_YELLOW );


		pDC->SelectObject(&pen);

		//-- Draw Line
		pDC->MoveTo(ptS.x,ptS.y);
		pDC->LineTo(ptE.x,ptE.y);
		m_ptMouseValue = m_ptMouse;
		m_nEndLine = nEndLine;

	}

	CPoint ptS, ptE;
	ptS.x = GetXFromTime(m_spotSelect.nTime);
	ptS.y = GetYFromLine(m_spotSelect.nSelectLine);
	ptS.y +=  DSC_ROW_H/2;//DSC_ROW_H/4;	
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
	{
		ptE.x = GetXFromTime(GetTimeFromX(m_ptMouseValue.x));
	}
	else
	{
		ptE.x = GetXFromTime(DEFAULT_ESM_PICTURE_DRAGTIME);
	}	
	if(m_nEndLine < 0)
		return;
	if(m_nEndLine > GetItemCount() - 1)
		return;



	ptE.y = GetYFromLine(m_nEndLine);
	ptE.y +=  DSC_ROW_H/2;//DSC_ROW_H/4;
	 

	if(nScroll>0)
	{
		ptS.y -= nScroll;
		ptE.y -= nScroll;
	}
	CPen pen;
	


	

	//-- Draw Line
	if(!ESMGetEditObj())
	{
		pen.CreatePen(PS_SOLID,  DSC_ROW_H/2, COLOR_BLACK );
		pDC->SelectObject(&pen);

		vector<CPoint> arrptS, arrptE;
		ESMGetDrawPos(arrptS, arrptE);

		for(int i = 0; i < arrptS.size(); i++)
		{
			ptS = arrptS.at(i);
			ptE = arrptE.at(i);

			ptS.y +=  DSC_ROW_H/2;//DSC_ROW_H/4;
			ptE.y +=  DSC_ROW_H/2;//DSC_ROW_H/4;

			if(ptS.y >nScroll && ptE.y >nScroll )
			{
				TRACE(_T("Draw (%u,%u) -> (%u,%u)\n", ptS.x,ptS.y,ptE.x,ptE.y));
				pDC->MoveTo(ptS.x,ptS.y);
				pDC->LineTo(ptE.x,ptE.y);
			}
		}
	}
	else
	{
		if( (ptS.x == ptE.x) || (ptS.y == ptE.y))
			pen.CreatePen(PS_SOLID,  DSC_ROW_H/2, COLOR_GREEN );
		else
			pen.CreatePen(PS_SOLID,  DSC_ROW_H/2, COLOR_YELLOW );
		pDC->SelectObject(&pen);

		if(ptS.y >nScroll && ptE.y >nScroll )
		{
			TRACE(_T("Draw (%u,%u) -> (%u,%u)\n", ptS.x,ptS.y,ptE.x,ptE.y));
			pDC->MoveTo(ptS.x,ptS.y);
			pDC->LineTo(ptE.x,ptE.y);
		}
	}
	
	
	

	/*pDC->MoveTo(106,167);
	pDC->LineTo(391,167);*/

}
CString CDSCFrameSelector::GetAutoDscID(CString strCurDSC)
{
	track_info stTrack = ESMGetTrackInfo();

#if 1
	CString strTemp = _T("");
	for(int i = 0 ; i < 4 ; i ++)
	{
		if(strCurDSC == stTrack.st_AxisInfo[i].strCamID)
		{
			strTemp = strCurDSC;
			break;
		}
	}

	return strTemp;
#else
	if(ESMGetRecordState() == 1)//우타자
	{
		CString strBall = stTrack.st_AxisInfo[0].strCamID;
		CString strHand = stTrack.st_AxisInfo[2].strCamID;

		if(strCurDSC == strBall)
		{
			stTrack.bStart = TRUE;
			ESMSetTrackInfo(stTrack);
			return strBall;
		}
		else if(strCurDSC == strHand)
		{
			stTrack.bStart = TRUE;
			ESMSetTrackInfo(stTrack);
			return strHand;
		}
		else
			return strCurDSC;
	}
	else if(ESMGetRecordState() == 2)//좌타자
	{
		CString strBall = stTrack.st_AxisInfo[1].strCamID;
		CString strHand = stTrack.st_AxisInfo[3].strCamID;

		if(strCurDSC == strBall)
		{
			stTrack.bStart = TRUE;
			ESMSetTrackInfo(stTrack);
			return strBall;
		}
		else if(strCurDSC == strHand)
		{
			stTrack.bStart = TRUE;
			ESMSetTrackInfo(stTrack);
			return strHand;
		}
		else
			return strCurDSC;
	}
	else
		return strCurDSC;
#endif
}
