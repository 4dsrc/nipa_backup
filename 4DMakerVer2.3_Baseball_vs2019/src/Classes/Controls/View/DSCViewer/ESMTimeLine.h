////////////////////////////////////////////////////////////////////////////////
//
//	CESMTimeLine.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-08-21
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>

using namespace std;
class CESMTimeSpan;

class CESMTimeLine
{
	private:
		vector<CESMTimeSpan> m_ESMTimeSpans;
		void swap(int a, int b);
		void qsort(int l = -100, int r = -100);

	public:
		CESMTimeLine();
		CESMTimeLine(const CESMTimeLine & c);
		~CESMTimeLine();
		void Copy(const CESMTimeLine & c);

		// Regarding Time Spans
		int		GetSpanCount();
		bool	AddTimeSpan(int Owner, int Action);
		int		GetTimeSpan(int Owner, int Action);

		void	SetAction(int Span, int Action);
		void	SetOwner(int Span, int Owner);

		int		GetAction(int Span);
		int		GetOwner(int Span);
		
		// Regarding Time Slices
		int		GetSliceCount(int Span);
		bool	AddTimeSlice(int Span, int Time, int Length, double Weight);
		int		GetTimeSlice(int Span, int Time);
		int		GetSlice    (int Span, int Time); // why did I do this?, not sure

		// Unified Structure
		void	GetSlice(int Span, int Slice, int & Start, int & Length, double & Weight); // this gets the data
		void	Edit(int Span, int Slice, int Start, int Length, double Weight);
		void	Delete(int Span, int Slice = -1);
		
		// verification, note: no internal means for collision are used since they may or may not be needed
		int		isCollision(int Span, int Skip, int Start, int Length);// is there a collision between this data and other data		
};
