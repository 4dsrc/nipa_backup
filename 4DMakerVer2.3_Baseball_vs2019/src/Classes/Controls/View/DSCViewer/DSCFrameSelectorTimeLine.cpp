////////////////////////////////////////////////////////////////////////////////
//
//	DSCFrameSelectorTimeLine.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-09
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCFrameSelector.h"
#include "DSCViewer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//------------------------------------------------------------------------------
//! @function	Scalable Functions Processing
//------------------------------------------------------------------------------
int CDSCFrameSelector::GetSpanWidth()
{
	CRect rect;
	GetClientRect(rect);
	//-- Scaling from ClientRect
	if(m_pView->m_bScaleToWindow)
		return rect.Width() - m_nLineLeftMargin - m_nLineRightMargin;
	//-- Original Size
	return GetNumberMajorTicks() *  m_nTickMajorSize;
}

int CDSCFrameSelector::GetNumberMajorTicks()
{
	return m_nTimeMax / m_nTickFreqMajor;
}

//------------------------------------------------------------------------------
//! @function	Positioning Within the Scale
//------------------------------------------------------------------------------
int CDSCFrameSelector::GetSpanTop(int nLineCnt)
{
	return ( nLineCnt * m_nLineHeight ) + m_nLineTopMargin;
}

int CDSCFrameSelector::GetTotalHeight()
{
	int nLineCnt = GetItemCount();
	return GetSpanTop(nLineCnt);
}

int CDSCFrameSelector::GetXFromTime(int nTime)
{
	nTime = nTime - GetTimeFromXRange(m_ptScroll.x + m_nLineLeftMargin);
	return m_nLineLeftMargin + GetSpanWidth() * nTime / m_nTimeMax;
}

//------------------------------------------------------------------------------
//! @function	Inverse Positioning Within the Scale
//------------------------------------------------------------------------------
// what is the time at this x at
int CDSCFrameSelector::GetTimeFromX(int x)
{
	//- 2014-01-22 kcd
	//- Scroll 좌표 삽입
	x = x + m_ptScroll.x; 
	return GetTimeFromXRange(x);
}

int	CDSCFrameSelector::GetTimeFromXRange(int x)
{
	int nRetTime = 0;
	if( x < m_nLineLeftMargin) 
		nRetTime = m_nLineLeftMargin;
	else if( x > m_nLineLeftMargin + GetSpanWidth()) 
		nRetTime = m_nTimeMax;
	else
	{
		unsigned long nResult = (x - m_nLineLeftMargin) * m_nTimeMax;
		if(nResult == 0)
			nRetTime = 0;
		else
			nRetTime = nResult / GetSpanWidth();
	}

	//-- 2013-09-26 hongsu@esmlab.com
	//-- Near 33 ms unit
	int nSec, nMilli;
	nSec = nRetTime / 1000;
	nMilli = (nRetTime - (nSec*1000)) / movie_next_frame_time; //movie_next_frame_time;
	/*if(nRetTime >500 && nRetTime < 1200)
		ESMLog(5, _T("%d = (%d - (%d*1000)) / %d"), nMilli, nRetTime, nSec, movie_next_frame_time);*/

	int nDivision;
	
	//if(nMilli == movie_frame_per_second)
	if(nMilli >= movie_frame_per_second)		//jhhan 171102 시간 소수점 예외 정리
		nDivision = (nSec+1)*1000;
	else
		nDivision = nSec*1000 + nMilli* movie_next_frame_time; //movie_next_frame_time;
	return nDivision;
}

//------------------------------------------------------------------------------
//! @function	Data Functions
//------------------------------------------------------------------------------

int g_nStart = GetTickCount();
int g_nEnd = 0;
void CDSCFrameSelector::SetTimeMax(int nMax)
{
	g_nEnd = GetTickCount();
	//ESMLog(1, _T("##SetTimeMax %d gap = %d"), nMax, g_nEnd - g_nStart);
	g_nStart = g_nEnd;

	if(nMax <= 0)
		return;
	m_nTimeMax = nMax;
}

void CDSCFrameSelector::Refresh()
{
	Invalidate(FALSE);
}

void CDSCFrameSelector::FocusWindow()
{
	Invalidate();
}

void CDSCFrameSelector::UpdateSizes()
{
	CSize sizeTotal;
	sizeTotal.cy = GetTotalHeight();
	sizeTotal.cx = GetSpanWidth() + m_nLineLeftMargin + m_nLineRightMargin;	
	SetScrollSizes(MM_TEXT, sizeTotal);

	m_ptScroll.x = GetScrollPosition().x;
	m_ptScroll.y = GetScrollPosition().y;
}

//-- 2013-09-29 hongsu@esmlab.com
BOOL CDSCFrameSelector::SetSavedLastTime(int nTime)
{
	if(nTime <= 0)
		return FALSE;

	if(m_nRecTime < nTime)
		m_nRecTime  = nTime;

	if(m_nTimeMax < m_nRecTime)
	{
		m_nTimeMax = m_nRecTime;		
		SetTimeMax(m_nTimeMax);
	}
	return TRUE;
}