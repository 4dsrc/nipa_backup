////////////////////////////////////////////////////////////////////////////////
//
//	DSCViewerCapture.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-25
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "DSCListViewer.h"
#include "DSCViewer.h"
#include "DSCItem.h"
#include "DSCMgr.h"
#include "SdiSingleMgr.h"
#include "ESMImgMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void _DoTakePicture(void *param)
{
	CDSCViewer* pDSCView = (CDSCViewer*)param;
	CDSCItem* pDSCItem = NULL;
	int nAll = pDSCView->GetItemCount();
	ESMLog(1,_T("[Capture] Start [%d] DSC"),nAll);

	//-- 2013-05-02 hongsu@esmlab.com
	//-- Reserved Message
	ESMEvent* pMsg[255] = {NULL,};

	ESMLog(4,_T("[Capture] Command Start"));
	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = (CDSCItem*)pDSCView->GetItemData(i);
		//-- PRESS S2
		pMsg[i] = new ESMEvent();
		pMsg[i]->pDest = (LPARAM)pDSCItem;
		pMsg[i]->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2;
		pDSCItem->SetDSCStatus(DSC_INFO_STATUS);
		//-- Send Message to DSC Manager
		pDSCView->m_pDSCMgr->AddMsg((ESMEvent*)pMsg[i]);
	}	
	
	//-- Wait Until Capture
	Sleep(100);

	//--------------------------------------------
	//-- Get Base X, Y
	CESMImgMgr* pImgMgr = new CESMImgMgr();
	CvPoint ptAdjust;	
	CString strFile;

	//--------------------------------------------
	//-- Get Taken Pictures	
	for(int i = 0 ; i < nAll ; i ++)
	{
		pDSCItem = (CDSCItem*)pDSCView->GetItemData(i);
		if(!pDSCItem)
			continue;

		//-- Get Adjust X,Y
		//-- X
		ptAdjust.x = (int)pDSCItem->GetDSCAdj(DSC_ADJ_X);
		ptAdjust.y = (int)pDSCItem->GetDSCAdj(DSC_ADJ_Y);

		//--------------------------------------------
		//-- Waiting Until Get Picture ( First Get Base Picture)	
		ESMLog(5,_T("[Capture] Waiting until Get Picture [%s]"),pDSCItem->GetDeviceDSCID());
		while(!pDSCItem->CheckDSCStatus(SDI_STATUS_CAPTURE_OK))
			Sleep(5);

		ESMLog(5,_T("[Capture] Move via Adjust [x:%d y:%d] [%s]"), ptAdjust.x, ptAdjust.y, strFile);
	}

	if(pImgMgr)
	{
		delete pImgMgr;
		pImgMgr = NULL;
	}

	ESMLog(1,_T("[Capture] Success"));
	_endthread();
}