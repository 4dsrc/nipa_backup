////////////////////////////////////////////////////////////////////////////////
//
//	DSCGroup.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCGroup.h"
#include "DSCItem.h"
#include "ESMIni.h"
#include "ESMRCManager.h"
#include "SdiSingleMgr.h"
#include "ESMFileOperation.h"
#include "WavePlayer.h"
#include "resource.h"
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <MMSystem.h>
#pragma comment(lib, "winmm")

CDSCGroup::CDSCGroup(int nIP)
{
	m_hMainWnd = ESMGetMainWnd();
	m_nIP = nIP;
	m_pRCMgr = NULL;
	m_dTpTime = 0.0;

	m_bThread = TRUE;
	m_pThread = NULL;	

	for(int i = 0; i< MAX_4DA_CAMERA; i++)
	{
		m_nGrpSec[i] = 0;
	}
}

CDSCGroup::~CDSCGroup()
{
	//WaitThreadEnd();
	
	//if(m_arDSC.GetCount() > 0)
	//	m_arDSC.RemoveAll();

	
}

void CDSCGroup::DeleteDSC(int nIndex)
{
	m_arDSC.RemoveAt(nIndex);
}

void CDSCGroup::DoTakeGroupPicture(int nTickTime)
{
	if (m_nIP == 0)
	{
		nTickTime += ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN);;
		TRACE(_T("[Sync Exe] [Group:%d] [Server] 2.Set Execute Time %d\n"),m_nIP ,nTickTime);
		DoTakePictureLocal(nTickTime);
	}
	else if(m_pRCMgr)
	{
		TRACE(_T("[Sync Exe] [Group:%d] [Server] 2.1.Set Local Base Time %d -> Change To Agent\n"),m_nIP ,nTickTime);
		DoTakePictureAgent(nTickTime);
	}
}

void CDSCGroup::DoTakePictureLocal(int nTickTime)
{
	int nAll = m_arDSC.GetCount();
	CDSCItem* pItem = NULL;

	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 1"));
		return;
	}
	else
	{
		for(int i = 0 ; i < nAll ; i ++)
		{
			pItem = (CDSCItem*)m_arDSC.GetAt(i);
			if(pItem)
			{
				if(pItem->GetType() == DSC_UNPREPARED)
				{
					ESMLog(1,_T("Non DSC Conection 2"));
					return;
				}
			}
		}
	}

	ESMEvent* pMsg = NULL;
/*
	while(1)
	{
		if(nTickTime <= ESMGetTick())
			break;
	}
*/
	int tPrevPC,nGap;
	double tPrevDSC, tExeDSC;
	int tCurrPC	= nTickTime;

	CESMFileOperation fo;
	CString strPath;
	strPath.Format(_T("%s"),ESMGetClientRamPath());
	/*fo.Delete(strPath);
	if( fo.CheckPath(strPath))
		fo.CreateFolder(strPath);*/

	ESMDeleteMovieFolder();

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pItem->m_nTickTime = nTickTime;
		HANDLE hHandle = NULL;
		hHandle = (HANDLE) _beginthreadex(NULL, 0, PictureThread, (void *)pItem, 0, NULL);
		CloseHandle(hHandle);
	}
	pItem->SetTickStatus(TRUE);
#ifdef _4DMODEL
	//-- 2015-02-15 cygil@esmlab.com
	//-- Add picture coutdown info 전송
	if(ESMGetIDfromIP(ESMGetLocalIP()) == ESMGetValue(ESM_VALUE_COUNTDOWNLASTIP))
	{
		CWavePlayer wavPlay;
		int nGetTickTime = ESMGetTick();
		int nSleepTime = nTickTime - (nGetTickTime + 4200);
		ESMLog(5,_T("Countdown Play nTickTime[%d], nGetTickTime[%d], nSleepTime[%d]"), nTickTime, nGetTickTime, nSleepTime);
		if(nSleepTime > 0)
			Sleep(nSleepTime);
		wavPlay.PlaySoundFile(COUNTDOWNFILE);
	}
#endif
	ESMLog(5,_T("S2 Press"));
}

unsigned WINAPI CDSCGroup::MovieThread(LPVOID param)
{
	ESMEvent* pMsg = NULL;
	CDSCItem* pItem = (CDSCItem*)param;
	int tPrevPC = 0,nGap = 0;
	double tPrevDSC = 0.0, tExeDSC = 0.0;
	int tCurrPC	= pItem->m_nTickTime;
	int nRepeatCount = 1;
	int nSleepTime = 20;
	//pItem->SetDSCStatus(SDI_STATUS_CAPTURE);

	//-- Check Status
	int nStatus = pItem->GetDSCRecStatus();
	//-- Set Capture Timing
	if(nStatus == DSC_REC_ON)
	{
		nRepeatCount = 1;
		pItem->SetMovieCancel(FALSE);
		tPrevPC		= pItem->m_tPC;
		tPrevDSC	= pItem->m_tDSC;
		nGap = tCurrPC-tPrevPC;
		//-- Execute Time
		//kcd Camera Time um 변경
		if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		{
			int nDelay = pItem->GetDelayTime();
			tExeDSC = tPrevDSC - ESMConvertTime(nGap) - ESMConvertTime(nDelay);
			pItem->SetExecuteTime(tExeDSC);
			pItem->IntitMovieSize();
			ESMLog(5, _T("[Sync|%s]    PrevPCTime[%d] => PicTime[%d]      PrevDSCTime[%lf] => PicDscTime[%lf]"),	 pItem->GetDeviceDSCID(), tPrevPC, tCurrPC, tPrevDSC, tExeDSC);
			pItem->SetTickCount(tExeDSC, ESMConvertTime(ESMGetValue(ESM_VALUE_SENSORONTIME)));
		}
		else
		{
			tExeDSC = tPrevDSC + nGap + pItem->GetDelayTime();
			pItem->SetExecuteTime(tExeDSC);
			pItem->IntitMovieSize();
			ESMLog(5, _T("[Sync|%s]    PrevPCTime[%d] => PicTime[%d]      PrevDSCTime[%lf] => PicDscTime[%lf]"),	 pItem->GetDeviceDSCID(), tPrevPC, tCurrPC, tPrevDSC, tExeDSC);
			pItem->SetTickCount(tExeDSC, ESMGetValue(ESM_VALUE_SENSORONTIME));
		}
		
		
		
	}	
	else
		ESMLog(5, _T("[Sync|%s]    Stop Recording"), pItem->GetDeviceDSCID());


	Sleep(nSleepTime);

	ESMLog(1, _T("(CAMREVISION)#####GetDSCStatus %d"), pItem->GetDSCStatus());
	if(pItem->GetDSCStatus() == SDI_STATUS_CONNECTED)
	{
		if(!pItem->GetRecordStep())
		{
			ESMLog(1, _T("Non Record ID %s"), pItem->GetDeviceDSCID());
			return 0;
		}
	}

	if(pItem->GetDeviceModel() == _T("GH5"))
	{
		//Sleep(500);//joonho.kim ????????

		// GH5 Version
		int nVersion = -1;
		nVersion = pItem->GetCamVersion();

		pMsg = new ESMEvent();
		pMsg->message = WM_RS_MC_CAPTURE_IMAGE_RECORD;
		pMsg->nParam1 = nStatus;
		pMsg->nParam2 = nVersion;
		pItem->SdiAddMsg(pMsg);	
	}
	else
	{
		for( int i = 0; i< nRepeatCount; i++)
		{
			pMsg= new ESMEvent();
			pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5;
			pItem->SdiAddMsg(pMsg);
			Sleep(nSleepTime);
		}

		Sleep(800);

		for( int i = 0; i< nRepeatCount; i++)
		{
			pMsg= new ESMEvent();
			pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_6;
			pItem->SdiAddMsg(pMsg);
			Sleep(nSleepTime);
		}
	}

	
	

	return 0;
}

unsigned WINAPI CDSCGroup::PictureThread(LPVOID param)
{
	CDSCItem* pItem = (CDSCItem*)param;
	int tPrevPC = 0,nGap = 0;
	double tPrevDSC = 0.0, tExeDSC = 0.0;
	int tCurrPC	= pItem->m_nTickTime;

	pItem->SetDSCStatus(SDI_STATUS_CAPTURE);

	if( pItem->GetDeviceModel() == _T("GH5"))
	{
		tPrevPC		= pItem->m_tPC;
		tPrevDSC	= pItem->m_tDSC;
		nGap = tCurrPC-tPrevPC;

		int nDelay = pItem->GetDelayTime();
		tExeDSC = tPrevDSC - ESMConvertTime(nGap) - ESMConvertTime(nDelay);
		pItem->SetExecuteTime(tExeDSC);
		//pItem->IntitMovieSize();
		ESMLog(5, _T("[Sync|%s]    PrevPCTime[%d] => PicTime[%d]      PrevDSCTime[%lf] => PicDscTime[%lf]"),	 pItem->GetDeviceDSCID(), tPrevPC, tCurrPC, tPrevDSC, tExeDSC);
		pItem->SetTickCount(tExeDSC, ESMConvertTime(ESMGetValue(ESM_VALUE_SENSORONTIME)));

		Sleep(1000);

		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent;
		pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_ONCE;
		pMsg->nParam1 = SDI_MODEL_GH5;

		pItem->SdiAddMsg(pMsg);
	}
	else if( pItem->GetDeviceModel() == _T("NX3000"))
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1;
		pMsg->pDest		= (LPARAM)pItem;
		pItem->SdiAddMsg(pMsg);

		tPrevPC		= pItem->m_tPC;
		tPrevDSC	= pItem->m_tDSC;
		nGap = tCurrPC-tPrevPC;
		tExeDSC = tPrevDSC + nGap + pItem->GetDelayTime();

		ESMLog(5, _T("[Sync|%s]    PrevPCTime[%d] => PicTime[%d]      PrevDSCTime[%lf] => PicDscTime[%lf]"),	pItem->GetDeviceDSCID(), tPrevPC, tCurrPC, tPrevDSC, tExeDSC);
		pItem->SetTickCount(tExeDSC, ESMGetValue(ESM_VALUE_SENSORONTIME));

		Sleep(1000);
		pMsg = new ESMEvent();
		pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2;
		pMsg->pDest		= (LPARAM)pItem;
		pItem->SdiAddMsg(pMsg);

	}
	else
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1;
		pMsg->pDest		= (LPARAM)pItem;
		pItem->SdiAddMsg(pMsg);

		tPrevPC		= pItem->m_tPC;
		tPrevDSC	= pItem->m_tDSC;
		nGap = tCurrPC-tPrevPC;
		tExeDSC = tPrevDSC + nGap + pItem->GetDelayTime();

		ESMLog(5, _T("[Sync|%s]    PrevPCTime[%d] => PicTime[%d]      PrevDSCTime[%lf] => PicDscTime[%lf]"),	pItem->GetDeviceDSCID(), tPrevPC, tCurrPC, tPrevDSC, tExeDSC);
		pItem->SetTickCount(tExeDSC, ESMGetValue(ESM_VALUE_SENSORONTIME));

		Sleep(1000);
		pMsg = new ESMEvent();
		pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2;
		pMsg->pDest		= (LPARAM)pItem;
		pItem->SdiAddMsg(pMsg);

		//Sleep(600);

		pMsg = new ESMEvent();
		pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_3;
		pMsg->pDest		= (LPARAM)pItem;
		pItem->SdiAddMsg(pMsg);

		pMsg = new ESMEvent();
		pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4;
		pMsg->pDest		= (LPARAM)pItem;
		pItem->SdiAddMsg(pMsg);
	}
	return 0;
}

void CDSCGroup::DoTakePictureAgent(int nTickTime, SDI_MODEL model)
{
	TRACE(_T("[Sync Exe] [Group:%d] [Agent] Get Local Base Time %d\n"),m_nIP ,nTickTime);

	//  Waiting until Sync Time
	double nAgentTime;
	double nSavedLocalTime;
	double nAgentExeTime;

	nAgentTime = (double)m_pRCMgr->GetAgentTime();	
	nSavedLocalTime = (double)m_pRCMgr->_GetLocalTime();

	nAgentExeTime = nAgentTime + ((double)nTickTime - nSavedLocalTime);
	TRACE(_T("[Sync Exe] [Group:%d] [Agent] 3.1. Saved Agent Time %lf\n"),m_nIP ,nAgentTime);
	TRACE(_T("[Sync Exe] [Group:%d] [Agent] 3.2. Saved Local Time %lf\n"),m_nIP ,nSavedLocalTime);
	TRACE(_T("[Sync Exe] [Group:%d] [Agent] 3.3. Now Agent Time %lf\n"),m_nIP ,nAgentExeTime);	
	nAgentExeTime += ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN);
	TRACE(_T("[Sync Exe] [Group:%d] [Agent] 3.4. Set Agent Exe Time %lf\n"),m_nIP ,nAgentExeTime);

	ESMEvent* pMsg = new ESMEvent();
	if (model == SDI_MODEL_GH5)
	{
		pMsg->message	= WM_RS_RC_CAPTURE_IMAGE_REQUIRE_GH5;
	}
	else
	{	
		pMsg->message	= WM_RS_RC_CAPTURE_IMAGE_REQUIRE;
		pMsg->nParam1	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2;
	}
	
	pMsg->nParam2	= (UINT)nAgentExeTime;
	m_pRCMgr->AddMsg(pMsg);
}

void CDSCGroup::DoMovieGroupCapture(int nTickTime, BOOL bRecordStatus)
{
	if (m_nIP == 0)
	{
		nTickTime += ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN);
		//-- Tunning (1mm)
		//nTickTime++;
		ESMLog(5, _T("[Sync Exe] [Group:%d] [Server] 2.Set Execute Time %d\n"),m_nIP ,nTickTime);
		DoMovieCaptureLocal(nTickTime);		
	}
	else if(m_pRCMgr)
	{
		//ESMLog(5, _T("[Sync Exe] [Group:%d] [Server] 2.1.Set Local Base Time %d -> Change To Agent"),m_nIP ,nTickTime);
		
		//SendToDeleteDSC();
		DoMovieCaptureAgent(nTickTime, bRecordStatus);

		//jhhan 170324 CheckValid 녹화 중 쓰레드 체크!!
		//if(ESMGetRecState() == ESM_NOMAL_START)
		//{
		//	if(m_pThread)
		//	{
		//		m_bStatus = FALSE;
		//	}

		//	for (int i=0; i<MAX_4DA_CAMERA; i++)
		//	{
		//		m_nGrpSec[i] = 0;
		//	}
		//	
		//	ESMInitCheckFrame();

		//	m_bStatus = TRUE;
		//	
		//	if(m_pThread == NULL)
		//	{
		//	if(ESMGetGPUMakeFile() != TRUE)
		//	m_pThread = AfxBeginThread(GetCheckRecordFrame, this, THREAD_PRIORITY_HIGHEST);
		//	}
		//}else
		//{
		//	//m_bStatus = FALSE;
		//}
		
	}
}

void CDSCGroup::DoMakingInfo()
{
	if(m_pRCMgr)
	{
		ESMEvent* pMsg = new ESMEvent();
		pMsg->pParam	= (LPARAM)ESMGetMakingInfo();
		pMsg->message	= WM_RS_RC_SET_MAKING_INFO;
		
		m_pRCMgr->AddMsg(pMsg);
	}
}

void CDSCGroup::SendToDeleteDSC()
{
	vector<CString> _DeleteDSCList;
	_DeleteDSCList = ESMGetDeleteDSC();

	CString _Delarr;
	
	for(int i = 0 ; i < _DeleteDSCList.size();i++)
	{
		if(i ==_DeleteDSCList.size() -1 )
		{
			_Delarr.Format(_T(""+_Delarr+_DeleteDSCList[i]));
		}
		else
		{
			_Delarr.Format(_T(""+_Delarr+_DeleteDSCList[i]+","));
		}

	}

	CString * pStrData = NULL;
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_DELETE_DSC;
	pStrData = new CString;
	pStrData->Format(_T("%s"), _Delarr);

	pMsg->pParam = (LPARAM)pStrData;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

}

BOOL CDSCGroup::DeleteDSCList(CString str)
{
	vector<CString> _DSCDeleteList;
	_DSCDeleteList = ESMGetDeleteDSCList();

	for(int i = 0; i< _DSCDeleteList.size();i++)
	{
		if(str == _DSCDeleteList[i])
		{
			ESMLog(5, _T("DeleteDSCList : %s"), _DSCDeleteList[i]);
			return FALSE;
		}
	}
	return TRUE;
}

void CDSCGroup::DoMovieCaptureLocal(int nTickTime)
{
	ESMLog(5, _T("[Sync Exe] [Group:%d] 3.DoMovieCaptureLocal %d\n"),m_nIP ,nTickTime);

	int nAll = m_arDSC.GetCount();
	CDSCItem* pItem = NULL;

	if(!nAll)
	{
		ESMLog(1,_T("Non DSC Conection 3"));
		return;
	}
	else
	{
		for(int i = 0 ; i < nAll ; i ++)
		{
			pItem = (CDSCItem*)m_arDSC.GetAt(i);
			if(pItem)
			{
				if(pItem->GetType() == DSC_UNPREPARED)
				{
					ESMLog(1,_T("Non DSC Conection 4"));
					return;
				}
			}
		}
	}

	int nStatus = DSC_REC_STOP;
	ESMEvent* pMsg = NULL;
	CString strPath;
	CESMFileOperation fo;

	//-- 2014-01-09 hjjang
	// Home Path로 변경하여 생성
	//-- 2013-09-30 hongsu@esmlab.com
	//-- Previous Work for Recording 
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		CString str = pItem->GetDeviceDSCID();
		//-- Check DSC Status
		if(!pItem								|| 
			pItem->GetTickStatus()	  == FALSE	|| 
			pItem->GetCaptureExcept() == TRUE||
			pItem->GetCaptureImageRecExcept() == TRUE||
			DeleteDSCList(str) == FALSE)
			continue;

		//-- 2013-10-23 hongsu@esmlab.com
		//-- Start Draw Recording Bar
		pItem->SetDSCRecStatus(DSC_REC_BTN);
		//-- Check Status
		nStatus = pItem->GetDSCRecStatus();
		
		//wgkim 180127
		if(ESMGetRecordStatus())
		//if(nStatus == DSC_REC_ON)
		{
			//(CAMREVISION)
			if(pItem->GetRecordStep())
				pItem->SetDSCStatus(SDI_STATUS_REC);
			else
			{
				pItem->m_nRec = DSC_REC_STOP;
			}
//			pItem->m_bGetStartTime = FALSE;
		}
	}
	
	//wgkim 180127
	if(ESMGetRecordStatus())		
	//if(nStatus == DSC_REC_ON)
	{
		CString strSubFolder;
		strSubFolder.Format(_T("%s\\record\\files\\%s"),ESMGetPath(ESM_PATH_HOME), ESMGetFrameRecord());
		CreateDirectory(strSubFolder, NULL);
		CESMFileOperation fo;
		//if(ESMGetValue(ESM_VALUE_MOVIESAVELOCATION))
		{
			CString strLocalPath;
			strLocalPath.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_LOCALMOVIE), ESMGetFrameRecord());
			fo.CreateFolder(strLocalPath);
			ESMLog(5,_T("DoMovieCaptureLocal CreateDirectory  ESM_PATH_LOCALMOVIE [%s]"), strLocalPath);
		}

		ESMDeleteMovieFolder();

		strPath.Format(_T("%s\\%s\\%s"),ESMGetClientRamPath(), ESMGetFrameRecord(), _T("Temp"));
		fo.CreateFolder(strPath);
		ESMLog(5, _T("Create Folder [%s]"), strPath);

		for(int i = 0 ; i < nAll ; i ++)
		{
			pItem = (CDSCItem*)m_arDSC.GetAt(i);
			CString str = pItem->GetDeviceDSCID();
			//-- Check DSC Status
			if(!pItem								|| 
				pItem->GetTickStatus()	  == FALSE	|| 
				pItem->GetCaptureExcept() == TRUE||
				pItem->GetCaptureImageRecExcept() == TRUE||
				DeleteDSCList(str) == FALSE)
				continue;

			ESMLog(1, _T("File create %s"), pItem->GetDeviceDSCID());

			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, CreateMpegFiles, (void *)pItem, 0, NULL);
			CloseHandle(hHandle);
		}

// 		strPath.Format(_T("%s"),ESMGetClientRamPath());
// 		fo.Delete(strPath, FALSE);
// 		fo.CreateFolder(strPath);
// 		if(!fo.CheckPath(strPath))
// 		{
// 			ESMLog(1, _T("Save Path : [%s] Non Created"), strPath);
// 		}

// 		pMsg = new ESMEvent();  
// 		pMsg->message = WM_ESM_MOVIE_INFORESET;
// 		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
// 
// 		//-- 2014-9-18 hongsu@esmlab.com
// 		//-- Reset Valid Data
// 		pMsg = new ESMEvent();  
// 		pMsg->message = WM_ESM_MOVIE_VALIDRESET;
// 		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	}
	else
	{
		pMsg = new ESMEvent();  
		pMsg->message = WM_ESM_MOVIE_INFORESET;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		 
		//-- 2014-9-18 hongsu@esmlab.com
		//-- Reset Valid Data
		pMsg = new ESMEvent();  
		pMsg->message = WM_ESM_MOVIE_VALIDRESET;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);

		CESMFileOperation fo;
		fo.Delete(ESMGetClientBackupRamPath());
		fo.CreateFolder(ESMGetClientBackupRamPath());
	}

	//-- 2013-09-30 hongsu@esmlab.com
	//-- Send Movie Record Message 	
	//CDSCItem* pArItem[DSC_LOCAL_CNT];	
	//ESMEvent* pArMsg [DSC_LOCAL_CNT];

	int tPrevPC,nGap;
	double tPrevDSC, tExeDSC;
	int tCurrPC	= nTickTime;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		CString str = pItem->GetDeviceDSCID();
		//-- Check DSC Status
		if(!pItem								|| 
			pItem->GetTickStatus()	  == FALSE	|| 
			pItem->GetCaptureExcept() == TRUE	||
			pItem->GetCaptureImageRecExcept() == TRUE||
			DeleteDSCList(str) == FALSE)
		{
			ESMLog(0,_T("#######ID:%s TickStatus %d, Except %d, DeleteList = %d, ImageRecExcept %d"), str, pItem->GetTickStatus(), pItem->GetCaptureExcept(), DeleteDSCList(str), pItem->GetCaptureImageRecExcept());
			continue;
		}

		//-- 2014-09-15 hongsu@esmlab.com
		//-- Set Tick Time 
		pItem->m_nTickTime = nTickTime;

		//-- 2014-09-15 hongsu@esmlab.com
		//-- Reset Sync Info
		pItem->SetSensorSync(TRUE);

		//-- Processing on Thread 
		HANDLE hHandle = NULL;
		hHandle = (HANDLE) _beginthreadex(NULL, 0, MovieThread, (void *)pItem, 0, NULL);
		CloseHandle(hHandle);
	}
	
	pItem->SetTickStatus(TRUE);
}

unsigned WINAPI CDSCGroup::CreateMpegFiles(LPVOID param)
{
	CString strPath;
	CString strFile;
	int nCount = 0;
	CDSCItem* pItem = (CDSCItem*)param;

	if (!pItem)
		return 0;

	//jhhan File Temp
#ifdef _FILE_TEMP
	if(ESMGetRecordFileSplit())
		strPath.Format(_T("%s\\%s\\%s"),ESMGetClientRamPath(), ESMGetFrameRecord(), _T("Temp"));
	else
		strPath.Format(_T("%s\\%s"),ESMGetClientRamPath(), ESMGetFrameRecord());
#else
	else
		strPath.Format(_T("%s\\%s"),ESMGetClientRamPath(), ESMGetFrameRecord());
#endif
	
	ESMLog(1, _T("Save Path Dir : [%s\\%s] File Created start [%d]"), strPath, pItem->GetDeviceDSCID(), GetTickCount());

	while (TRUE)
	{
		strFile.Format(_T("%s\\%s_%d.mp4"), strPath, pItem->GetDeviceDSCID(), nCount);

		int fp;
		if((fp = _topen(strFile, _O_CREAT | _O_BINARY | _O_RDWR, 0777)) != -1)
		{
			//_write(fp, NULL, 0);
			//_commit(fp);
			_close(fp);
		}

		if (nCount++ > 200)
			break;
		// 		else
		// 			Sleep(1);
	}

	ESMLog(1, _T("Save Path Dir : [%s\\%s] File Created end [%d]"), strPath, pItem->GetDeviceDSCID(), GetTickCount());

	return 0;
}


void CDSCGroup::ESMDeleteMovieFolder(CString strDscIp)
{
#ifndef _AGENT_DELETE_OLD
	CString strDeletePath = ESMGetClientRamPath(strDscIp), strPathData, strPath;
	ESMLog(5, _T("Delete Search Forder[%s]"), strDeletePath);
	vector<CString> arrFileName;
	CFileFind finder;
	bool working = finder.FindFile( strDeletePath + _T("\\*.*") );
	while ( working )
	{
		working = finder.FindNextFile();
		if ( finder.IsDots() ) 
			continue;

		if ( finder.IsDirectory() )   
		{
			CString curfile = finder.GetFileName(); 
			strPathData = ESMGetClientRamPath(strDscIp) + _T("\\") + curfile;
			arrFileName.push_back(strPathData);
		}  
	}
	sort(arrFileName.begin(), arrFileName.end());
	if( arrFileName.size() >= 1)
	{
		for(int i = 0 ; i < arrFileName.size() - 1; i++)
		{
			strPath = arrFileName.at(i);
			CESMFileOperation fo;
			fo.Delete(strPath, TRUE);
			//ESMLog(5, _T("Delete Folder [%s] "), strPath);
		}
	}
#else
	//180119
	CESMFileOperation fo;
	CString strPath;
	strPath.Format(_T("%s"),ESMGetClientRamPath());
	fo.Delete(strPath, FALSE);
#endif
}

void CDSCGroup::DoMovieCaptureAgent(int nTickTime, BOOL bRecordStatus)
{
	//TRACE(_T("[Sync Exe] [Group:%d] [Agent] Get Local Base Time %d\n"),m_nIP ,nTickTime);

	//  Waiting until Sync Time
	double nAgentTime;
	double nSavedLocalTime;
	double nAgentExeTime;

	nAgentTime = (double)m_pRCMgr->GetAgentTime();	
	nSavedLocalTime = (double)m_pRCMgr->_GetLocalTime();

	nAgentExeTime = nAgentTime + ((double)nTickTime - nSavedLocalTime);
// 	TRACE(_T("[Sync Exe] [Group:%d] [Agent] 3.1. Saved Agent Time %lf\n"),m_nIP ,nAgentTime);
// 	TRACE(_T("[Sync Exe] [Group:%d] [Agent] 3.2. Saved Local Time %lf\n"),m_nIP ,nSavedLocalTime);
// 	TRACE(_T("[Sync Exe] [Group:%d] [Agent] 3.3. Now Agent Time %lf\n"),m_nIP ,nAgentExeTime);	
	nAgentExeTime += ESMGetValue(ESM_VALUE_SYNC_TIME_MARGIN);;
// 	TRACE(_T("[Sync Exe] [Group:%d] [Agent] 3.4. Set Agent Exe Time %lf\n"),m_nIP ,nAgentExeTime);
//	TRACE(_T("Gap Cirfirm : nAgentTime[%lf] m_dTpTime[%lf] Gap[%lf]\n"), nAgentTime, m_dTpTime, nAgentTime - m_dTpTime);
	m_dTpTime = nAgentTime;	
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message	= WM_RS_RC_CAPTURE_IMAGE_REQUIRE;
	pMsg->nParam1	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_5;
	pMsg->nParam2	= (UINT)nAgentExeTime;
	pMsg->nParam3	= bRecordStatus; 
	m_pRCMgr->AddMsg(pMsg);

}

void CDSCGroup::DoMovieCancel()
{
	if (m_nIP == 0)
	{
		DoMovieCancelLocal();		
	}
	else if(m_pRCMgr)
	{
		DoMovieCancelAgent();
	}
}

void CDSCGroup::DoMovieCancelLocal()
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection"));
		return;
	}

	CDSCItem* pItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		//-- Release Focusing	
		pItem->SetMovieCancel(TRUE);
	}
}

void CDSCGroup::DoMovieCancelAgent()
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message	= WM_RS_RC_CAPTURE_CANCEL;
	m_pRCMgr->AddMsg(pMsg);
}

void CDSCGroup::DoGroupS1Press(CDSCItem* pItem)
{
	if (m_nIP == 0)
		DoS1PressLocal(pItem);
	else if(m_pRCMgr)
		DoS1PressAgent(pItem);
}

void CDSCGroup::DoGroupS1Press()
{
	if (m_nIP == 0)
		DoS1PressLocal();
	else if(m_pRCMgr)
		DoS1PressAgent();
}

void CDSCGroup::DoS1PressLocal(CDSCItem* pItem)
{
	if(!pItem)
		return;

	ESMEvent* pMsg = new ESMEvent();
	pItem->SetDSCStatus(SDI_STATUS_NORMAL);
	pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1;		
	pItem->SdiAddMsg(pMsg);

	ESMLog(5,_T("S1 Press"));

	Sleep(2000);

	DoS1ReleaseLocal();
}

void CDSCGroup::DoS1PressLocal()
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 5"));
		return;
	}

	CDSCItem* pItem = NULL;
	ESMEvent* pMsg = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pMsg = new ESMEvent();
		//-- Release Focusing
		pItem->SetDSCStatus(SDI_STATUS_NORMAL);
		pMsg->message	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1;		
		pItem->SdiAddMsg(pMsg);
	}
	ESMLog(5,_T("S1 Press"));

	Sleep(2000);

	DoS1ReleaseLocal();
}

void CDSCGroup::DoS1PressAgent(CDSCItem* pItem)
{
	//int nDscId = _ttoi(pItem->GetDeviceDSCID());
	ESMEvent* pMsg = new ESMEvent();
	//-- Release Focusing
	pMsg->message	= WM_RS_RC_CAPTURE_IMAGE_REQUIRE;
	pMsg->nParam1	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_7;	
	//pMsg->nParam2	= nDscId;
	pMsg->pDest		= (LPARAM)pItem;
	m_pRCMgr->AddMsg(pMsg);
	//::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}

void CDSCGroup::DoS1PressAgent()
{
	ESMEvent* pMsg = new ESMEvent();
	//-- Release Focusing
	pMsg->message	= WM_RS_RC_CAPTURE_IMAGE_REQUIRE;
	pMsg->nParam1	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1;	
	m_pRCMgr->AddMsg(pMsg);
	//::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}

void CDSCGroup::DoGroupS1Release()
{
	if (m_nIP == 0)
		DoS1ReleaseLocal();
	else if(m_pRCMgr)
		DoS1ReleaseAgent();
}

void CDSCGroup::DoS1ReleaseLocal()
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 6"));
		return;
	}

	CDSCItem* pItem = NULL;
	ESMEvent* pMsg  = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pMsg = new ESMEvent();
		//-- Release Focusing
		pItem->SetDSCStatus(SDI_STATUS_NORMAL);
		pMsg->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4;
		pMsg->pDest		= (LPARAM)pItem;
		::SendMessage(m_hMainWnd, WM_ESM, WM_ESM_DSC, (LPARAM)pMsg);
	}

	ESMLog(5,_T("S1 Release"));
}

void CDSCGroup::DoS1ReleaseAgent()
{
	ESMEvent* pMsg = new ESMEvent();
	//-- Release Focusing
	pMsg->message	= WM_RS_RC_CAPTURE_IMAGE_REQUIRE;
	pMsg->nParam1	= WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4;
	m_pRCMgr->AddMsg(pMsg);
	//::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}

void CDSCGroup::DoMinFocus()
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 5"));
		return;
	}

	CDSCItem* pItem = NULL;
	ESMEvent* pMsg = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		SetFocus(pItem->GetDeviceDSCID(), pItem->m_nMinFocus);
	}
}

void CDSCGroup::SetGroupAllProperty(ESMEvent* pMsg)
{
	if (m_nIP == 0)
		SetAllPropertyLocal(pMsg);
	else if(m_pRCMgr)
		SetAllPropertyAgent(pMsg);
}

void CDSCGroup::SetAllPropertyLocal(ESMEvent* pMsg)
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 7"));
		return;
	}

	CDSCItem* pItem = NULL;
	ESMEvent* pCopyMsg = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pCopyMsg = new ESMEvent();
		memcpy(pCopyMsg, pMsg, sizeof(ESMEvent));
		pCopyMsg->message	= WM_SDI_OP_SET_PROPERTY_VALUE;
		//pCopyMsg->pDest		= (LPARAM)pItem;

		pItem->SdiAddMsg(pCopyMsg);
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

}

void CDSCGroup::SetAllPropertyAgent(ESMEvent* pMsg)
{
	pMsg->message = WM_ESM_LIST_SET_GROUP_PROPERTY;
	m_pRCMgr->AddMsg(pMsg);
	//::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}

void CDSCGroup::SetGroupAllHiddenCommand(ESMEvent* pMsg)
{
	if (m_nIP == 0)
		SetAllHiddenCommandLocal(pMsg);
	else if(m_pRCMgr)
		SetAllHiddenCommandAgent(pMsg);
}

void CDSCGroup::SetAllHiddenCommandLocal(ESMEvent* pMsg)
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 7"));
		return;
	}

	CDSCItem* pItem = NULL;
	ESMEvent* pCopyMsg = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pCopyMsg = new ESMEvent();
		memcpy(pCopyMsg, pMsg, sizeof(ESMEvent));
		pCopyMsg->message	= WM_SDI_OP_HIDDEN_COMMAND;
		//pCopyMsg->pDest		= (LPARAM)pItem;

		pItem->SdiAddMsg(pCopyMsg);
	}

	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}

}

void CDSCGroup::SetAllHiddenCommandAgent(ESMEvent* pMsg)
{
	pMsg->message = WM_ESM_LIST_HIDDEN_COMMAND;
	m_pRCMgr->AddMsg(pMsg);
	//::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}




//////////////////////////////////////////////////////////////////////////
//
// Format Device
//
//////////////////////////////////////////////////////////////////////////
void CDSCGroup::FormatDeviceGroup()
{
	if (m_nIP == 0)
		FormatDeviceLocal();
	else if(m_pRCMgr)
		FormatDeviceAgent();
}

void CDSCGroup::FormatDeviceLocal()
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 8"));
		return;
	}

	CDSCItem* pItem = NULL;
	ESMEvent* pMsg = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pMsg = new ESMEvent();
		pMsg->message	= WM_SDI_OP_FORMAT;
		pMsg->pDest		= (LPARAM)pItem;

		pItem->SdiAddMsg(pMsg);
	}
}

void CDSCGroup::FormatDeviceAgent()
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_LIST_FORMAT_ALL;
	m_pRCMgr->AddMsg(pMsg);
}


//////////////////////////////////////////////////////////////////////////
//-- 2014-9-4 hongsu@esmlab.com
//-- F/W Update
//////////////////////////////////////////////////////////////////////////
void CDSCGroup::FWUpdateGroup()
{
	if (m_nIP == 0)
		FWUpdateLocal();
	else if(m_pRCMgr)
		FWUpdateAgent();
}

void CDSCGroup::FWUpdateLocal()
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
		return;

	CDSCItem* pItem = NULL;
	ESMEvent* pMsg = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;
		if( pItem->GetDeviceModel() == _T("NX2000") || pItem->GetDeviceModel() == _T("NX1") || pItem->GetDeviceModel() == _T("NX500"))
		{
			pMsg = new ESMEvent();
			pMsg->message	= WM_SDI_OP_FWUPDATE;
			pMsg->pDest		= (LPARAM)pItem;
			pItem->SdiAddMsg(pMsg);
		}
		else if( pItem->GetDeviceModel() == _T("NX3000"))
		{
			TCHAR* strPath = NULL;
			strPath = new TCHAR[MAX_PATH];
			_tcscpy(strPath, _T("\\\\192.\\DATANX3000.bin"));
			//_stprintf(strPath, _T("%s\\DATANX3000.bin"), ESMGetPath(ESM_PATH_SETUP));
			pMsg = new ESMEvent();
			pMsg->message	= WM_SDI_OP_FW_DOWNLOAD;
			pMsg->pDest		= (LPARAM)pItem;
			pMsg->pParam    = (LPARAM)strPath;
			pItem->SdiAddMsg(pMsg);
		}
		else if( pItem->GetDeviceModel() == _T("GH5"))
		{
			pMsg = new ESMEvent();
			pMsg->message = WM_SDI_OP_FW_UPDATE;
			pItem->SdiAddMsg(pMsg);
		}
	}
}

void CDSCGroup::FWUpdateAgent()
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_LIST_FW_UPDATE;
	m_pRCMgr->AddMsg(pMsg);	
}

//////////////////////////////////////////////////////////////////////////
//-- 2014-9-4 hongsu@esmlab.com
//-- Movie Making Stop
//////////////////////////////////////////////////////////////////////////
void CDSCGroup::MovieMakingStopGroup()
{
	if (m_nIP == 0)
		MovieMakingStopLocal();
	else if(m_pRCMgr)
		MovieMakingStopAgent();
}

void CDSCGroup::MovieMakingStopLocal()
{
	//그냥 MovieManage 쪽으로 보내주기만하면끝.
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_MOVIE_MAKINGSTOP;
	::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_MOVIE, (LPARAM)pMsg);
}

void CDSCGroup::MovieMakingStopAgent()
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_LIST_MOVIEMAKINGSTOP;
	m_pRCMgr->AddMsg(pMsg);	
}



//////////////////////////////////////////////////////////////////////////
//-- 2014-9-4 hongsu@esmlab.com
//-- F/W Update
//////////////////////////////////////////////////////////////////////////

void CDSCGroup::CaptureResumeGroup(int nResumeFrame)
{
	if (m_nIP == 0)
		CaptureResumeLocal(nResumeFrame);
	else if(m_pRCMgr)
		CaptureResumeAgent(nResumeFrame);
}

void CDSCGroup::CaptureResumeLocal(int nResumeFrame)
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
		return;

	CDSCItem* pItem = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pItem->SetResume(nResumeFrame);

		CString strLog;
		strLog.Format(_T("DSC[%s] : Resume Count[%d]"),pItem->GetDeviceDSCID(),  nResumeFrame);
		ESMLog(5, strLog);
	}
}

void CDSCGroup::CaptureResumeAgent(int nResumeFrame)
{
	ESMEvent* pSendMsg = new ESMEvent();
	pSendMsg->message = WM_ESM_LIST_CAPTURERESUME;
	pSendMsg->nParam1 = nResumeFrame;
	m_pRCMgr->AddMsg(pSendMsg);	
}

//CamShut Update
void CDSCGroup::CamShutDownGroup()
{
	if (m_nIP == 0)
		CamShutDownLocal();
	else if(m_pRCMgr)
		CamShutDownAgent();
}

void CDSCGroup::CamShutDownLocal()
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
		return;

	CDSCItem* pItem = NULL;
	ESMEvent* pMsg = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pMsg = new ESMEvent();
		pMsg->message	= WM_SDI_OP_DEVICE_POWER_OFF;
		pMsg->pDest		= (LPARAM)pItem;
		pItem->SdiAddMsg(pMsg);
	}
}

void CDSCGroup::CamShutDownAgent()
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_LIST_CAMSHUTDOWN;
	m_pRCMgr->AddMsg(pMsg);	
}

//////////////////////////////////////////////////////////////////////////
//-- 2014-11-04 changdo
//-- Init Movie File
//////////////////////////////////////////////////////////////////////////
void CDSCGroup::InitMovieGroup()
{
	if (m_nIP == 0)
		InitMovieLocal();
	else if(m_pRCMgr)
		InitMovieAgent();
}

void CDSCGroup::InitMovieLocal()
{
// 	int nAll = m_arDSC.GetCount();
// 	if(!nAll)
// 		return;
// 
// 	CDSCItem* pItem = NULL;
// 	ESMEvent* pMsg = NULL;
// 	CString strRamFile, strRamFile2;
// 	for(int i = 0 ; i < nAll ; i ++)
// 	{
// 		pItem = (CDSCItem*)m_arDSC.GetAt(i);
// 		if(!pItem)
// 			continue;
// 
// 		strRamFile.Format(_T("%s\\%s.mp4"), ESMGetClientRamPath(), pItem->GetDeviceDSCID());
// 		strRamFile2.Format(_T("%s\\%s.mp4"), ESMGetClientBackupRamPath(), pItem->GetDeviceDSCID());
// 		CopyFile(strRamFile, strRamFile2, false);
// 
// // 		TCHAR* cPath = new TCHAR[1+strRamFile2.GetLength()];
// // 		_tcscpy(cPath, strRamFile2);
// // 		TCHAR* strId = new TCHAR[pItem->GetDeviceDSCID().GetLength() + 1];
// // 		_tcscpy(strId, pItem->GetDeviceDSCID());
// // 		ESMEvent* pMsgBuffer	= new ESMEvent();
// // 		pMsgBuffer->message	= WM_ESM_MOVIE_SET_MOVIEDATA;
// // 		pMsgBuffer->pParam	= (LPARAM)cPath;
// // 		pMsgBuffer->pDest	= (LPARAM)strId;
// // 		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsgBuffer);
// 	}
}

void CDSCGroup::InitMovieAgent()
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_LIST_INITMOVIEFILE;
	m_pRCMgr->AddMsg(pMsg);	
}


//////////////////////////////////////////////////////////////////////////
//-- 2014-9-4 cdkim
//-- View Large
//////////////////////////////////////////////////////////////////////////
void CDSCGroup::VIewLargeGroup(int nLarge)
{
	if (m_nIP == 0)
		VIewLargeLocal(nLarge);
	else if(m_pRCMgr)
		VIewLargeAgent(nLarge);
}

void CDSCGroup::VIewLargeLocal(int nLarge)
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
		return;

	CDSCItem* pItem = NULL;
	ESMEvent* pMsg = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pMsg = new ESMEvent();
		pMsg->message	= WM_SDI_OP_SETVIEWLARGER;
		pMsg->pDest		= (LPARAM)pItem;
		pMsg->nParam1 = nLarge;
		pItem->SdiAddMsg(pMsg);
	}
}

void CDSCGroup::VIewLargeAgent(int nLarge)
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_LIST_VIEWLARGE;
	pMsg->nParam1 = nLarge;
	m_pRCMgr->AddMsg(pMsg);	
}

//////////////////////////////////////////////////////////////////////////

//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
void CDSCGroup::GetFocusGroup(BOOL bSave)
{
	if (m_nIP == 0)
		GetFocusLocal(bSave);
	else if(m_pRCMgr)
		GetFocusAgent(bSave);
}

void CDSCGroup::SetFocusGroup()
{
	if (m_nIP == 0)
		SetFocusLocal();
	else if(m_pRCMgr)
		SetFocusAgent();
}

//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
void CDSCGroup::GetFocusLocal(BOOL bSave)
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 9"));
		return;
	}

	CDSCItem* pItem = NULL;
	ESMEvent* pMsg = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pMsg = new ESMEvent();
		//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
		if(bSave)
			pMsg->message	= WM_SDI_OP_GET_FOCUS_VALUE;
		else
			pMsg->message	= WM_SDI_OP_GET_FOCUS_VALUE;
		pMsg->pDest		= (LPARAM)pItem;
		pMsg->nParam1 = bSave;
		pItem->SdiAddMsg(pMsg);
	}
}

//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
void CDSCGroup::GetFocusAgent(BOOL bSave)
{
	ESMEvent* pMsg = new ESMEvent();
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	if(bSave)
		pMsg->message = WM_ESM_LIST_GETFOCUS_TOCLIENT_READ_ONLY;
	else
		pMsg->message = WM_ESM_LIST_GETFOCUS_TOCLIENT;
	pMsg->nParam1 = bSave;
	m_pRCMgr->AddMsg(pMsg);
}

void CDSCGroup::SetFocusLocal()
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 9"));
		return;
	}

	CDSCItem* pItem = NULL;
	ESMEvent* pMsg = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pMsg = new ESMEvent();
		pMsg->message	= WM_SDI_OP_SET_FOCUS_VALUE;
		pMsg->pDest		= (LPARAM)pItem;
		pMsg->nParam1	= pItem->m_nMinFocus;
		pMsg->nParam2	= pItem->m_nCurFocus;
		pItem->SdiAddMsg(pMsg);
	}
	Sleep(1000);
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pMsg = new ESMEvent();
		pMsg->message	= WM_SDI_OP_SET_FOCUS_VALUE;
		pMsg->pDest		= (LPARAM)pItem;
		pMsg->nParam1	= pItem->m_nCurFocus;
		//ESMLog(_T(""))
		pItem->SdiAddMsg(pMsg);
	}
}

void CDSCGroup::SetFocusAgent()
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 11"));
		return;
	}

	CDSCItem* pItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pItem->LoadFocus();
		ESMEvent* pMsg = new ESMEvent();
		pMsg->pDest = (LPARAM)pItem;
		pMsg->nParam1 = pItem->m_nMinFocus;
		pMsg->nParam2 = pItem->m_nCurFocus;
		pMsg->message = WM_ESM_LIST_SETFOCUS_TOCLIENT;
		m_pRCMgr->AddMsg(pMsg);
	}
}

void CDSCGroup::SetFocusLoadFile(CString strPath)
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 11"));
		return;
	}

	int nCurFocus=0, nMinFocus=0;
	CString strEntry, strSection;
	CESMIni ini;
	
	if(!ini.SetIniFilename (strPath))
		return;

	CDSCItem* pItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		strEntry.Format(_T("%s"), pItem->GetDSCInfo(DSC_INFO_ID));
		nCurFocus = ini.GetInt(INFO_SECTION_FOCUS, strEntry, 0);
		nMinFocus	= ini.GetInt(INFO_SECTION_FOCUS, strEntry + _T("_MIN"), 0);

		if(nCurFocus == 0 || nMinFocus == 0)
			continue;

		ESMEvent* pMsg = new ESMEvent();
		pMsg->pDest = (LPARAM)pItem;
		pMsg->nParam1 = nMinFocus;
		pMsg->nParam2 = nCurFocus;
		pMsg->message = WM_ESM_LIST_SETFOCUS_TOCLIENT;
		m_pRCMgr->AddMsg(pMsg);
	}
}

void CDSCGroup::SetFocus(CString strDSCId, int nSetFocus)
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 11"));
		return;
	}

	CDSCItem* pItem = NULL;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		if(pItem->GetDeviceDSCID() == strDSCId)
		{
			ESMEvent* pMsg = new ESMEvent();
			pMsg->message	= WM_SDI_OP_SET_FOCUS_VALUE;
			pMsg->nParam1 = nSetFocus;
			pItem->SdiAddMsg(pMsg);
		}
	}
}

void CDSCGroup::SetPreRecInitGH5Group()
{
	if (m_nIP == 0)
		SetPreRecInitGH5Local();
	else if(m_pRCMgr)
		SetPreRecInitGH5Agent();
}

void CDSCGroup::SetPreRecInitGH5Local()
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 10"));
		return;
	}

	CDSCItem* pItem = NULL;
	int nThreadCount = 0;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);

		if(!pItem)
			continue;

		//ESMLog(1,_T("$$$$$$$$$$$ Pre Rec init GH5 %s"), pItem->GetDeviceDSCID());

		ESMEvent* pMsg = new ESMEvent();
		pMsg->message = WM_SDI_OP_GH5_INITIALIZE;
		pItem->SdiAddMsg(pMsg);	
	}
}

void CDSCGroup::SetPreRecInitGH5Agent()
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_RS_RC_PRE_REC_INIT_GH5;
	m_pRCMgr->AddMsg(pMsg);
}

void CDSCGroup::GetTickCmdGroup(int nCount)
{
	if (m_nIP == 0)
		GetTickCmdLocal(nCount);
	else if(m_pRCMgr)
		GetTickCmdAgent();
}

void CDSCGroup::GetTickCmdLocal(int nCount)
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 10 ---"));
		return;
	}	

	CDSCItem* pItem = NULL;
	BOOL bRet = TRUE;
	HANDLE* hSyncTimeThread = NULL;
	hSyncTimeThread = new HANDLE[nAll];
	int nThreadCount = 0;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);

		if(!pItem)
			continue;

		ESMLog(1,_T("############ GetTickCmdLocal for resync %s, gettick count:%d"), pItem->GetDeviceDSCID(), nCount);

		pItem->SetResyncGetTickCount(nCount);
		pItem->SetHWND(m_hMainWnd);
		hSyncTimeThread[nThreadCount++] = (HANDLE) _beginthreadex(NULL, 0, GetTickCmdCountThread, (void *)pItem, 0, NULL);
	}

	//joonho.kim 17.06.27 Sync
	if(ESMGetValue(ESM_VALUE_DSC_SYNC_TIME) > 100)
	{
		if(ESMGetValue(ESM_VALUE_DSC_WAIT_TIME) > (ESMGetValue(ESM_VALUE_DSC_SYNC_TIME)+10000))
			WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, (ESMGetValue(ESM_VALUE_DSC_WAIT_TIME)/10));
		else if(ESMGetValue(ESM_VALUE_DSC_WAIT_TIME) <= 0)
			WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, INFINITE);
		else
			WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, (ESMGetValue(ESM_VALUE_DSC_SYNC_TIME)/10)+1000);
	}
	else
	{
		WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, (ESMGetValue(ESM_VALUE_DSC_WAIT_TIME)/10));
	}
	//WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, DEFAULT_ESM_SDKDELAY_WAITTIME);
	//WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, DEFAULT_ESM_DSC_SYNC_WAITTIME);

	for( int i =0 ;i < nThreadCount; i++)
	{
		DWORD dwExitCode = 0;
		::GetExitCodeThread(hSyncTimeThread[i], &dwExitCode);
		if (dwExitCode == STILL_ACTIVE)
		{
			pItem = (CDSCItem*)m_arDSC.GetAt(i);
			
			ESMLog(0, _T("EXCEPTIONDSC [%s]"), pItem->GetDeviceDSCID());
		}
		else
			CloseHandle(hSyncTimeThread[i]);
	}

#if 0		// gettick count 1
	if (nCount < 2)
		return;
#endif

	ESMLog(1, _T("DSC Sync Success"));
	SetSync(TRUE);
	CString strIP = ESMGetLocalIP();
	int nRemoteID = ESMGetIDfromIP(strIP);

	// Send Sync Result to Server
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_RS_RC_CAMERA_SYNC_RESULT;
	pMsg->nParam1 = nRemoteID;
	::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}

void CDSCGroup::GetTickCmdAgent()
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_RS_RC_GET_TICK_CMD;
	m_pRCMgr->AddMsg(pMsg);
}

void CDSCGroup::GetTickGroup()
{
	if (m_nIP == 0)
		GetTickLocal();
	else if(m_pRCMgr)
		GetTickAgent();
}

void CDSCGroup::GetTickLocal()
{
	int nAll = m_arDSC.GetCount();//EVEN , ODD
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 10"));
		return;
	}

	CDSCItem* pItem = NULL;
	BOOL bRet = TRUE;
	HANDLE* hSyncTimeThread = NULL;
	hSyncTimeThread = new HANDLE[nAll];
	int nThreadCount = 0;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		/*if(!pItem || pItem->GetCaptureExcept() == TRUE)
			continue;*/

		if(!pItem)
			continue;
		else
		{
			pItem->SetCaptureExcept(FALSE);
			pItem->SetCaptureImageRecExcept(FALSE);
		}



		//ESMLog(1,_T("############ GetTickLocal %s"), pItem->GetDeviceDSCID());

		// 2014-09-17 kcd
		// change SetTick To Thread.

		pItem->SetHWND(m_hMainWnd);
		hSyncTimeThread[nThreadCount++] = (HANDLE) _beginthreadex(NULL, 0, GetTickCountThread, (void *)pItem, 0, NULL);
// 		bRet = pItem->GetTickCount(tPC, tDSC);
// 		if (bRet == FALSE)
// 		{
// 			pItem->SetTickStatus(FALSE);
// 			return;
// 		}
	}

	//joonho.kim 17.06.27 Sync
	if(ESMGetValue(ESM_VALUE_DSC_SYNC_TIME) > 100)
	{
		if(ESMGetValue(ESM_VALUE_DSC_WAIT_TIME) > (ESMGetValue(ESM_VALUE_DSC_SYNC_TIME)+10000))
			WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, (ESMGetValue(ESM_VALUE_DSC_WAIT_TIME)/10));
		else if(ESMGetValue(ESM_VALUE_DSC_WAIT_TIME) <= 0)
			WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, INFINITE);
		else
			WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, (ESMGetValue(ESM_VALUE_DSC_SYNC_TIME)/10)+1000);
	}
	else
	{
		WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, (ESMGetValue(ESM_VALUE_DSC_WAIT_TIME)/10));
	}
	//WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, DEFAULT_ESM_SDKDELAY_WAITTIME);
	//WaitForMultipleObjects(nThreadCount, hSyncTimeThread, TRUE, DEFAULT_ESM_DSC_SYNC_WAITTIME);

	for( int i =0 ;i < nThreadCount; i++)
	{
		DWORD dwExitCode = 0;
		::GetExitCodeThread(hSyncTimeThread[i], &dwExitCode);
		if (dwExitCode == STILL_ACTIVE)
		{
			pItem = (CDSCItem*)m_arDSC.GetAt(i);
			pItem->SetCaptureExcept(TRUE);

			//-- Exception Dsc
			//-- Send To Server
			ESMEvent* pNewMsg	= new ESMEvent();
			pNewMsg->pDest		= (LPARAM)pItem;
			pNewMsg->message	= WM_ESM_NET_EXCEPTIONDSC;
			::SendMessage(m_hMainWnd, WM_ESM, WM_ESM_NET, (LPARAM)pNewMsg);

			ESMLog(0, _T("EXCEPTIONDSC [%s]"), pItem->GetDeviceDSCID());
		}
		else
			CloseHandle(hSyncTimeThread[i]);
	}

	ESMLog(1, _T("DSC Sync Success"));
	//jhhan 190130 - 목록 초기화
	ESMM3u8Reset();

	SetSync(TRUE);
	CString strIP = ESMGetLocalIP();
	int nRemoteID = ESMGetIDfromIP(strIP);
	
	// Send Sync Result to Server
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_RS_RC_CAMERA_SYNC_RESULT;
	pMsg->nParam1 = nRemoteID;
	::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}

unsigned WINAPI CDSCGroup::GetTickCmdCountThread(LPVOID param)
{	
	int nCheckCnt, nCheckTime;

	CDSCItem* pItem = (CDSCItem*)param;
	BOOL bRet = TRUE;
	int tPC = 0;
	double tDSC = 0.0;
	//bRet = pItem->DSCGetTickCount_resync(tPC, tDSC, nCheckCnt, nCheckTime, ESMGetValue(ESM_VALUE_DSC_SYNC_TIME));
	bRet = pItem->DSCGetTickCount_resync(tPC, tDSC, nCheckCnt, nCheckTime, 2000);
	
#if 0		// gettick count 1 변경.
 	int nResyncGetTickCount = pItem->GetResyncGetTickCount();
 	if (nResyncGetTickCount < 2)
 		return 0;
#endif

	CString strIP = ESMGetLocalIP();
	int nRemoteID = ESMGetIDfromIP(strIP);
	// Send Sync Result to Server
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_RS_RC_GET_TICK_CMD_RESULT;
	//pMsg->nParam1 = _ttoi(pItem->GetDeviceDSCID());
	pMsg->nParam2 = bRet;
	pMsg->pDest = (LPARAM)pItem;
	::SendMessage(pItem->m_hwnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

	//ESMLog(5, _T("pc[%d], dsc[%lf], id[%d], result[%d]"), tPC, tDSC, _ttoi(pItem->GetDeviceDSCID()), bRet);

	if (bRet == FALSE)
	{
		pItem->SetTickStatus(FALSE);
	}
	else
	{
		CString strIP = ESMGetLocalIP();
		int nRemoteID = ESMGetIDfromIP(strIP);
		// Send Sync Result to Server
		ESMEvent* pMsg = new ESMEvent();
		pMsg->message = WM_RS_RC_CAMERA_SYNC_CHECK;
		//pMsg->nParam1 = nRemoteID;
		pMsg->nParam1 = nCheckCnt;
		//pMsg->nParam2 = _ttoi(pItem->GetDeviceDSCID());
		pMsg->nParam3 = nCheckTime;
		pMsg->pDest = (LPARAM)pItem;
		::SendMessage(pItem->m_hwnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

		pItem->SetTickStatus(TRUE);
	}

	return 0;
}

unsigned WINAPI CDSCGroup::GetTickCountThread(LPVOID param)
{	
	int nCheckCnt, nCheckTime;

	CDSCItem* pItem = (CDSCItem*)param;
	BOOL bRet = TRUE;
	int tPC = 0;
	double tDSC = 0.0;
	bRet = pItem->DSCGetTickCount(tPC, tDSC, nCheckCnt, nCheckTime, ESMGetValue(ESM_VALUE_DSC_SYNC_TIME));
	if (bRet == FALSE)
	{
		pItem->SetTickStatus(FALSE);
	}
	else
	{
		CString strIP = ESMGetLocalIP();
		int nRemoteID = ESMGetIDfromIP(strIP);
		// Send Sync Result to Server
		ESMEvent* pMsg = new ESMEvent();
		pMsg->message = WM_RS_RC_CAMERA_SYNC_CHECK;
		//pMsg->nParam1 = nRemoteID;
		pMsg->nParam1 = nCheckCnt;
		//pMsg->nParam2 = _ttoi(pItem->GetDeviceDSCID());
		pMsg->nParam3 = nCheckTime;
		pMsg->pDest = (LPARAM)pItem;
		::SendMessage(pItem->m_hwnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

		pItem->SetTickStatus(TRUE);
	}
	return 0;
}

void CDSCGroup::GetTickAgent()
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_RS_RC_CAMERA_SYNC;
	m_pRCMgr->AddMsg(pMsg);
	//::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}

int CDSCGroup::SyncTickCount()
{
	if(!m_pRCMgr)
		return false;

	if(m_nIP)
	{
		//m_pRCMgr->SetThreadSleep(FALSE);
		m_pRCMgr->m_nSyncMin = -1;
		m_pRCMgr->SyncTickCount(m_nIP);
	}
	return true;
}

void CDSCGroup::DoMovieSaveCheck(bool bChaeck)
{
	if (m_nIP == 0)
		DoMovieSaveCheckLocal(bChaeck);
	else if(m_pRCMgr)
		DoMovieSaveCheckAgent(bChaeck);
}

void CDSCGroup::DoMovieSaveCheckLocal(bool bChaeck)
{
	int nAll = m_arDSC.GetCount();
	if(!nAll)
	{
		ESMLog(5,_T("Non DSC Conection 11"));
		return;
	}

	CDSCItem* pItem = NULL;

	for(int i = 0 ; i < nAll ; i ++)
	{
		pItem = (CDSCItem*)m_arDSC.GetAt(i);
		if(!pItem)
			continue;

		pItem->m_bMovieSaveCheck = bChaeck;
	}
}

void CDSCGroup::DoMovieSaveCheckAgent(bool bChaeck)
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_RS_RC_MOVIESAVE_CHECK;
	pMsg->nParam1 = bChaeck;
	m_pRCMgr->AddMsg(pMsg);
}
void CDSCGroup::RequestData(vector<MakeFrameInfo>* pMovieInfo)
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_REQUEST_DATA;
	pMsg->pParam  = (LPARAM)pMovieInfo;	
	m_pRCMgr->AddMsg(pMsg);
}

void CDSCGroup::DoMovieMakeFrameMovie(vector<MakeFrameInfo>* pMovieInfo, int nMovieNum, BOOL bPlay, BOOL bLocal)
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_MAKEFRAMEMOVIETOCLIENT;
	pMsg->pParam  = (LPARAM)pMovieInfo;
	pMsg->nParam1  = bLocal;
	pMsg->nParam2  = nMovieNum;
	pMsg->nParam3  = bPlay;
	m_pRCMgr->AddMsg(pMsg);

	CString strTime;
	strTime.Format(_T("%s"),pMovieInfo->at(0).strTime);

	//jhhan 17-01-06 TEST_PROCESSOR
	//jhhan 17-01-05
	//TimeLine Processor Info 전송
	int nId = m_pRCMgr->GetID();		//Check
	CString strIp = m_pRCMgr->GetIP();		//Check
	int nFrameCnt = pMovieInfo->size();
	int nMakeFile = nMovieNum;
	
	//시간정보 가져오기
	CString *pTime = new CString;
	pTime->Format(_T("%s"),strTime);
	ESMEvent * pProcessor = new ESMEvent;

	pProcessor->message = WM_ESM_PROCESSOR_INFO;
	pProcessor->nParam1 = nId;
	pProcessor->nParam2 = nFrameCnt;
	pProcessor->nParam3 = nMakeFile;
	pProcessor->pParam = (LPARAM) pTime;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pProcessor);

}

BOOL CDSCGroup::DeleteMakeDSCList(CString str)
{
	vector<CString> _DSCMakeDeleteList;
	_DSCMakeDeleteList = ESMGetMakeDeleteDSCList();

	for(int i = 0; i< _DSCMakeDeleteList.size();i++)
	{
		if(str == _DSCMakeDeleteList[i])
		{
			ESMLog(5, _T("DeleteDSCList : %s"), _DSCMakeDeleteList[i]);
			return FALSE;
		}
	}
	return TRUE;
}

UINT CDSCGroup::GetCheckRecordFrame(LPVOID pParam)
{
	CDSCGroup * pParent = (CDSCGroup*)pParam;
	
	//int nSec = 0;
	BOOL bFirst = FALSE;
	//int nTime = ESMGetValue(ESM_VALUE_CHECK_VALID_TIME);
	

	/*int nGrpSec[5] = {0,};*/

	//BOOL nInit = 0;

	while(1)
	{
		if(pParent->m_bThread != TRUE)
			break;

		if(pParent->m_bStatus != TRUE)
		{
			bFirst = FALSE;
			//nSec = 0;

			/*for(int i = 0; i< 5; i++)
			{
			nGrpSec[i] = 0;
			}*/

			Sleep(10);
			continue;
		}
		else
		{
			if(bFirst == FALSE)
			{
				//jhhan 170327
				//Sleep(nTime);
				bFirst = TRUE;
			}
		}
		//ESMLog(5, _T("m_arDSC : %d"), pParent->m_arDSC.GetCount());

		//170406 저장경로 가져오기 - 환경설정
#ifndef _PATH_CONFIG
		CString strFile = ESMGetPath(ESM_PATH_MOVIE_FILE);
		if(strFile.Find(_T("HOME")) != -1)
			strFile.Replace(_T("$(HOME)"), _T("\\4DMaker"));	
		else
		{
			int nPos;
			nPos = strFile.Find(_T("\\"));
			strFile.Delete(0,nPos+1);
		}
#endif

		CString strPath;

		//IP Check
		CString strFrontIP = ESMGetFrontIP();

		for(int i=0; i < pParent->m_arDSC.GetCount(); i++)
		{
			CDSCItem * pItem = NULL;
			pItem = (CDSCItem*)pParent->m_arDSC.GetAt(i);
			if(pItem != NULL)
			{
				if(pParent->m_bThread != TRUE)
					break;

				//strPath.Format(_T("\\\\192.168.0.%d\\%s\\%s\\%s_%d.mp4"), pParent->m_nIP, _T("Movie"), ESMGetFrameRecord(), pItem->GetDSCInfo(DSC_INFO_ID), nSec);
				//strPath.Format(_T("\\\\192.168.0.%d\\%s\\%s\\%s_%d.mp4"), pParent->m_nIP, strFile/*_T("RecordSave\\Record\\Movie\\files")*/, ESMGetFrameRecord(), pItem->GetDSCInfo(DSC_INFO_ID), nGrpSec[i]/*nSec*/);
				//strPath.Format(_T("\\\\192.168.0.%d\\%s\\%s\\%s_%d.mp4"), pParent->m_nIP, strFile/*_T("RecordSave\\Record\\Movie\\files")*/, ESMGetFrameRecord(), pItem->GetDSCInfo(DSC_INFO_ID), pParent->m_nGrpSec[i]/*nSec*/);
				strPath.Format(_T("\\\\%s%d\\%s\\%s\\%s_%d.mp4"),strFrontIP, pParent->m_nIP, strFile/*_T("RecordSave\\Record\\Movie\\files")*/, ESMGetFrameRecord(), pItem->GetDSCInfo(DSC_INFO_ID), pParent->m_nGrpSec[i]/*nSec*/);

				CESMFileOperation fo;
				if(fo.IsFileExist(strPath))
				{
					CFile file;
					if(file.Open(strPath, CFile::modeRead))
					{
						if(file.GetLength() > 0)
						{
							//ESMLog(5, _T("[%d] : %s"), pParent->m_nIP, strPath);
							
							ESMSetCheckFrame(strPath);
							/*if(i == 0)
							{
							ESMLog(5, _T("[%d] : %s"), pParent->m_nIP, strPath);
							}*/

							//nGrpSec[i]++;

							pParent->m_nGrpSec[i]++;
						}else
						{
							//ESMLog(0, _T("GetCheckRecordFrame [%d] : %s"), 3, strPath);
						}
						file.Close();
					}
					else
					{
						//ESMLog(0, _T("GetCheckRecordFrame [%d] : %s"), 2, strPath);
					}
				}
				else
				{
					//ESMLog(0, _T("GetCheckRecordFrame [%d] : %s"), 1, strPath);
				}
			}
			Sleep(10);
		}
		if(pParent->m_bThread != TRUE)
			break;
		//nSec++;

		Sleep(10);
	}
	return 0;
}

void CDSCGroup::WaitThreadEnd()
{
	if(m_pThread != NULL)
	{
		m_bThread = FALSE;
		DWORD dwExit=0;
		DWORD dwRet = WaitForSingleObject(m_pThread, 1000/*INFINITE*/);
		if(dwRet == WAIT_OBJECT_0)
		{
			m_pThread = NULL;
		}
		else if(dwRet == WAIT_TIMEOUT)
		{
			//GetExitCodeThread(m_pThread->m_hThread, &dwExit);

			::TerminateThread(m_pThread->m_hThread, 0);
			delete m_pThread;
			m_pThread = NULL;
		}

	}
}