////////////////////////////////////////////////////////////////////////////////
//
//	ESMDropSource.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-27
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "afxole.h" 

class CESMDropSource : protected COleDataSource
{
public:
	CESMDropSource();
	virtual ~CESMDropSource();
	virtual DROPEFFECT StartDragging( DWORD_PTR Data, RECT* rClient, CPoint* MousePos);
protected:
	virtual void CompleteMove() {};
private:

};
