////////////////////////////////////////////////////////////////////////////////
//
//	DSCListViewer.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-05
//
////////////////////////////////////////////////////////////////////////////////


#pragma once
#include "DSCInfoViewBase.h"
#include "ESMIndex.h"

class CDSCViewer;
class CDSCItem;

class CDSCListViewer : public CDSCInfoViewBase
{
	DECLARE_DYNCREATE(CDSCListViewer)

public:
	CDSCListViewer() {};
	CDSCListViewer(CDSCViewer* pParent);
	virtual ~CDSCListViewer() {};

	CString m_strPage; //2016/07/15
	int m_iPage; //2016/07/15
//------------------------------------------------------------------------------
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
	//jhhan
	CString m_strDelay;

	//hjcho
	CString m_AUTO;

	//hjcho
	CString m_FrameAdj;

	//jhhan 
	CString m_strCapture;

public:
	BOOL m_bScroll;
	BOOL m_bShowAdj;
	BOOL m_bShowProp;
	BOOL m_bShowTesting;

	int m_nScroll;
	int m_nDrawRight;
	int m_nDrawBottom;	

	//-- HEADER
	CESMBtn	m_ColHDInfo[DSC_INFO_CNT];
	CESMBtn	m_ColHDAdj[DSC_ADJ_CNT];
	CESMBtn	m_ColHDProp[DSC_PROP_CNT];
	CESMBtn	m_ColHDTime[DSC_TIME_CNT];
	CESMBtn	m_ColTesting[TESTING_PROP_CNT];
	//-- SEPARATOR BTN
	CESMBtn	m_BtnScroll;
	CESMBtn	m_BtnFolderAdj;
	CESMBtn	m_BtnFolderProp;
	CESMBtn	m_BtnTesting;
	//-- DUMMY LINE
	CESMStatic	m_StaticSeparator;

	CString	m_strHDInfo[DSC_INFO_CNT];
	CString	m_strHDAdj[DSC_ADJ_CNT];
	CString	m_strHDProp[DSC_PROP_CNT];
	CString m_strTesting[TESTING_PROP_CNT];

	
protected:
	void OnDraw(CDC* pDC);      // overridden to draw this view
	void OnDrawScroll();
	void OnDrawInfo();
	void OnDrawAdjust();
	void OnDrawProperty();
	void OnDrawSeparator();
	void OnDrawButton(CDC* pDC);
	CRect GetScaleBtnRect();
	CRect GetInsertIDBtnRect();
	void OnDrawTesting();
public:
	void RedrawInfo() {OnDrawInfo(); };
	void ReDrawTesting();
	void SetMouseWheel(BOOL bUp);

	//----------------------------------------
	//-- Control list Control
	//----------------------------------------
public:
	void InitListCtrl();	
	void RemoveListAll();
	void RemoveList(int nIndex);

	//----------------------------------------
	//-- Control DSC
	//----------------------------------------
	void InitDSCMgr();		
	//-- Grid Status
	void SetDeleteItem(int nRow, CDSCItem* pItem,BOOL bDelete = FALSE);
	void SetSelectItem(int nRow, CDSCItem* pItem);
	void SetGridStatus(int nStatus, CSdiSingleMgr* pSDIMgr);
	BOOL SetShowPage(int nBtn, BOOL bOpen);
	void ChangeRow(int nLineA, int nLineB);

	//----------------------------------------
	//-- 3D Movie
	//----------------------------------------
	void ESMGet3DInfo(ESM3DInfo* p3DInfo);

public:
	virtual void SetGridStatus(int nRow, int nStatus);
	void SetGridStatus(CDSCItem* pDSCItem, int nStatus);
	void SizeCalculate(LPRECT lpRect);	

	//------------------------------------------------------------------------------
	//! @function	Message
	//------------------------------------------------------------------------------
	LRESULT OnDSCBtnMove(WPARAM w, LPARAM l);
	LRESULT OnDSCBtnDown(WPARAM w, LPARAM l);
	LRESULT OnDSCBtnClick(WPARAM w, LPARAM l);
	LRESULT OnDSCGridClick(WPARAM w, LPARAM l);
	LRESULT OnDSCGridRClick(WPARAM w, LPARAM l);
	// Generated message map functions
	//{{AFX_MSG(CDSCFrameSelector)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);

	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};

