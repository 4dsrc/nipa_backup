	////////////////////////////////////////////////////////////////////////////////
	//
	//	DSCFrameSelector.cpp : implementation of the TestGuarantee Function Class.
	//
	//  ESMLab, Inc. PROPRIETARY INFORMATION.
	//  The following contains information proprietary to ESMLab, Inc. and may not be copied
	//  nor disclosed except upon written agreement by ESMLab, Inc.
	//
	//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
	//
	// @author	Hongsu Jung (hongsu@esmlab.com)
	// @Date	2013-09-05
	//
	////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCFrameSelector.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

	CDSCFrameSelector::CDSCFrameSelector(CDSCViewer* pParent) : CDSCInfoViewBase(pParent)
{
	m_nMultiLine		= 0;
	m_pFont				= NULL;

	m_nHeaderTop		= DSC_IMAGE_EDIT_TIMELINE_H + DSC_HEADER_H;
	m_nTickMajorHeight	= m_nHeaderTop / 5 * 3 ;
	m_nTickMinorHeight	= m_nHeaderTop / 3 ;

	SetTimeMax(DSC_DEFAULT_TIME_MAX);
	m_nTickFreqMinor	= 100;		// Small line
	m_nTickFreqMajor	= 1000;		// Big Line
	m_nTickMajorSize	= 100;		// Size of 1 Second

	m_nLineLeftMargin	= DSC_ICON_TIMESPOT_W/2;
	m_nLineRightMargin	= DSC_ICON_TIMESPOT_W/2;	

	m_nLineHeight		= DSC_ROW_H;
	m_nLineTopMargin	= m_nHeaderTop;// + 5;//2;

	m_nHandleWidth = 8;
	m_nTextMargin = 5;			

	//-- Recording Bar
	m_nRec			= DSC_REC_NONE;
	m_nRecTime		= 0;
	m_ptScroll.x	= 0;
	m_ptScroll.y	= 0;
	//-- 2013-09-13 hongsu@esmlab.com
	//-- For SendMessage
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();

	//-- 2013-09-27 hongsu@esmlab.com
	//-- Selected Spot
	m_bLBtnDown			= FALSE;
	m_pTimeLineObject	= NULL;	

	//-- 2013-10-14 hongsu@esmlab.com
	//-- Init Spot Select Info
	m_spotSelect.nSelectLine = 0;
	m_spotSelect.nTime		 = 0;
	m_spotSelect.strDSC		 = _T("");

	//-- 2014-01-04 hjjang@esmlab.com
	//-- Init Prev Select Info
	m_prevSelect.nSelectLine = 0;
	m_prevSelect.nTime		 = 0;
	m_prevSelect.strDSC		 = _T("");

	m_nSelectedDsc = 0;
	m_nSelectedTime = 0;
	m_bIsShowSpot = FALSE;

	for(int i = 0; i < MAX_SELECT_LINE; i++)
		m_nSelectLineTime[i] = 0;
}

// CTimeLineView
CDSCFrameSelector::~CDSCFrameSelector()
{
	if(m_pFont)
	{
		delete m_pFont;
		m_pFont = NULL;
	}
}
//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CDSCFrameSelector::OnInitialUpdate()
{
	CDSCInfoViewBase::OnInitialUpdate();
	HBITMAP hBmp;

	//-- 2013-09-09 hongsu@esmlab.com
	//-- Create Time Line 
	m_StaticLine.Create(NULL,WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);
	m_SelectLine1.Create(NULL,WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);
	m_SelectLine2.Create(NULL,WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);
	m_SelectLine3.Create(NULL,WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);
	m_SelectLine4.Create(NULL,WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);

	for(int i = 0; i < MAX_SELECT_LINE; i++)
	{
		m_MultiSelectLine[i].Create(NULL,WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);
	}
	m_StaticTime.Create(NULL,WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);
	m_StaticTime.ChangeFont(DSC_TEXT_H + 1, DSC_FONT, FW_HEAVY);
//-- 2015-02-24 cygil@esmlab.com 4DModeler 색상 회색계열로 변경
#ifdef _4DMODEL
	m_StaticTime.SetColors(	COLOR_YELLOW,	//FG
		COLOR_BASIC_BG_5,	//BG
		COLOR_WHITE	,	//Hot FG
		COLOR_BASIC_HBG	);	//Hot BG
#else
	m_StaticTime.SetColors(	COLOR_YELLOW,	//FG
		COLOR_BASIC_BG_3,	//BG
		COLOR_BASIC_HFG	,	//Hot FG
		COLOR_BASIC_HBG	);	//Hot BG
#endif
	//-- Create Triangle Image
	m_StaticTri.Create(NULL,WS_CHILD|WS_VISIBLE|SS_BITMAP|SS_CENTERIMAGE , CRect(0,0,0,0), this, IDC_DSC_VIEW_HEADER);	
	hBmp = (HBITMAP)::LoadImage( AfxGetInstanceHandle(),  MAKEINTRESOURCE(IDB_ESM_TIMESPOT),IMAGE_BITMAP , 0,0, LR_LOADMAP3DCOLORS );
	if( hBmp == NULL )	
		return;	
	hBmp = m_StaticTri.SetBitmap(hBmp);
	if(hBmp) 
		::DeleteObject(hBmp);
	m_StaticTri.RedrawWindow();

	m_StaticLine.SetColors(COLOR_BASIC_RED, COLOR_BASIC_RED , COLOR_BASIC_RED, COLOR_BASIC_RED);
	m_SelectLine1.SetColors(COLOR_BLUE, COLOR_BLUE , COLOR_BLUE, COLOR_BLUE);
	m_SelectLine2.SetColors(COLOR_BLUE, COLOR_BLUE , COLOR_BLUE, COLOR_BLUE);
	m_SelectLine3.SetColors(COLOR_BLUE, COLOR_BLUE , COLOR_BLUE, COLOR_BLUE);
	m_SelectLine4.SetColors(COLOR_BLUE, COLOR_BLUE , COLOR_BLUE, COLOR_BLUE);

	for(int i = 0; i < MAX_SELECT_LINE; i++)
	{
		m_MultiSelectLine[i].SetColors(COLOR_BLUE, COLOR_BLUE , COLOR_BLUE, COLOR_BLUE);
	}
	//-- 2013-09-09 hongsu@esmlab.com
	m_StaticTri.ShowWindow(FALSE);		
	m_StaticTime.ShowWindow(FALSE);	
	m_SelectLine1.ShowWindow(FALSE);
	m_SelectLine2.ShowWindow(FALSE);
	m_SelectLine3.ShowWindow(FALSE);
	m_SelectLine4.ShowWindow(FALSE);

	for(int i = 0; i < MAX_SELECT_LINE; i++)
	{
		m_MultiSelectLine[i].ShowWindow(FALSE);
	}
}

BOOL CDSCFrameSelector::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	// TODO: Add your specialized code here and/or call the base class
	Refresh();
	return CScrollView::OnScrollBy(sizeScroll, bDoScroll);
}

BOOL CDSCFrameSelector::OnScroll(UINT nScrollCode, UINT nPos, BOOL bDoScroll) 
{
	Refresh();
	return CScrollView::OnScroll(nScrollCode, nPos, bDoScroll);
}

void CDSCFrameSelector::SetGridStatus(int nRow, int nStatus)
{

}

void CDSCFrameSelector::SetRecStatusEditer(DSC_REC_STATUS nStatus)	
{
	switch(nStatus)
	{
	case DSC_REC_ON:
#if 1
		m_nRecTime = 0;
		m_nCheckTime = 250;
		m_nCheckStartTime = GetTickCount();
		::SetTimer(m_hWnd, REC_TIMER_ON, 5, NULL);		
#else	
		::SetTimer(m_hWnd, REC_TIMER_ON, REC_TIMER_DRAW_TIME, NULL);		
#endif
		//m_nRecTime = 0;

		break;
	case DSC_REC_STOP:	
		::KillTimer(m_hWnd, REC_TIMER_ON);
		
		break;
	}
}

void CDSCFrameSelector::SetRecStatus(int nStatus)
{
	if(nStatus == DSC_REC_BTN)
	{
		if(m_nRec == DSC_REC_ON	) 
			m_nRec = DSC_REC_STOP;
		else
			m_nRec = DSC_REC_ON;
		SetRecStatus(m_nRec);
	}

	switch(m_nRec)
	{
	case DSC_REC_ON:	

#if 1
		m_nRecTime = 0;
		m_nCheckTime = 250;
		m_nCheckStartTime = GetTickCount();
		::SetTimer(m_hWnd, REC_TIMER_ON, 5, NULL);		
#else
		::SetTimer(m_hWnd, REC_TIMER_ON, REC_TIMER_DRAW_TIME, NULL);		
		m_nRecTime = 0;
#endif
		break;
	case DSC_REC_STOP:	
		::KillTimer(m_hWnd, REC_TIMER_ON);
		
		break;
	}
}

void CDSCFrameSelector::SetFrameFont(CDC* pDC, int nSize, CString strFont, int nFontType)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	pDC->SelectObject(m_pFont);	
}

void CDSCFrameSelector::ResetTimeLineObject()
{
	if(m_pTimeLineObject)
	{
		delete m_pTimeLineObject;
		m_pTimeLineObject = NULL;
	}
	SetTimeMax(DSC_DEFAULT_TIME_MAX);
}

void CDSCFrameSelector::SpotViewState(BOOL bIsShowSpot, int nSelectedDsc, int nSelectedTime)
{
	if( nSelectedDsc >= 0)
		m_nSelectedDsc = nSelectedDsc;
	if( nSelectedTime >= 0)
		m_nSelectedTime = nSelectedTime;

	m_bIsShowSpot = bIsShowSpot;
}

void CDSCFrameSelector::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDSCInfoViewBase::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CDSCFrameSelector::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDSCInfoViewBase::OnVScroll(nSBCode, nPos, pScrollBar);
}
