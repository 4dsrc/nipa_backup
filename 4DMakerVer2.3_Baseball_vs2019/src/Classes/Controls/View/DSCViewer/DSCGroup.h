////////////////////////////////////////////////////////////////////////////////
//
//	DSCGroup.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ESMFunc.h"
#include "ESMIndexStructure.h"
#include "ESMCtrl.h"
#include "SdiDefines.h"
#include "DSCViewDefine.h"
#include "SdiSingleMgr.h"

class CESMRCManager;
class CDSCItem;

#define MAX_4DA_CAMERA	5

class CDSCGroup: public CObject
{
public:
	CDSCGroup() {};
	CDSCGroup(int nIP);
	virtual ~CDSCGroup();

public:
	CString  m_strIP;
	BOOL m_bGPUUse;
	int m_nIP;
	CESMArray m_arDSC;
	HWND m_hMainWnd;
private:
	CESMRCManager* m_pRCMgr;
	BOOL m_bSync;

	double m_dTpTime;	

public:
	void AddDSC(CDSCItem* pItem) { m_arDSC.Add((CObject*)pItem); }
	int GetCount() {return m_arDSC.GetCount();}
	CDSCItem* GetDSC(int nIndex) {return (CDSCItem*)m_arDSC.GetAt(nIndex);}
	BOOL GetSync() { return m_bSync; }
	void SetSync(BOOL bSync) { m_bSync = bSync; }
	void DeleteDSC(int nIndex);
	BOOL DeleteDSCList(CString str);
	//Capture
	void DoTakeGroupPicture(int nTickTime);
	void DoTakePictureLocal(int nTickTime);
	void DoTakePictureAgent(int nTickTime, SDI_MODEL model=SDI_MODEL_UNKNOWN);
	static unsigned WINAPI PictureThread(LPVOID param);
	static unsigned WINAPI MovieThread(LPVOID param);
	static unsigned WINAPI CreateMpegFiles(LPVOID param);

	//Movie
	void DoMakingInfo();
	void DoMovieGroupCapture(int nTickTime,  BOOL bRecordStatus);
	void DoMovieCaptureLocal(int nTickTime);
	void DoMovieCaptureAgent(int nTickTime, BOOL bRecordStatus);
	void DoMovieCancel();
	void DoMovieCancelLocal();
	void DoMovieCancelAgent();
	void SendToDeleteDSC();
	void ESMDeleteMovieFolder(CString strDscIp = _T(""));

	//Focus
	void DoGroupS1Press(CDSCItem* pItem);
	void DoGroupS1Press();
	void DoS1PressLocal(CDSCItem* pItem);
	void DoS1PressLocal();
	void DoS1PressAgent(CDSCItem* pItem);
	void DoS1PressAgent();

	void DoGroupS1Release();
	void DoS1ReleaseLocal();
	void DoS1ReleaseAgent();

	void DoMinFocus();

	//Property
	void SetGroupAllProperty(ESMEvent* pMsg);
	void SetAllPropertyLocal(ESMEvent* pMsg);
	void SetAllPropertyAgent(ESMEvent* pMsg);

	//HiddenCommand
	void SetGroupAllHiddenCommand(ESMEvent* pMsg);
	void SetAllHiddenCommandLocal(ESMEvent* pMsg);
	void SetAllHiddenCommandAgent(ESMEvent* pMsg);

	//Format
	void FormatDeviceGroup();
	void FormatDeviceLocal();
	void FormatDeviceAgent();

	//FW Update
	void FWUpdateGroup();
	void FWUpdateLocal();
	void FWUpdateAgent();

	//Movie Making Stop
	void MovieMakingStopGroup();
	void MovieMakingStopLocal();
	void MovieMakingStopAgent();

	//DSC CaptureResume
	void CaptureResumeGroup(int nResumeFrame);
	void CaptureResumeLocal(int nResumeFrame);
	void CaptureResumeAgent(int nResumeFrame);

	//CamShut Update
	void CamShutDownGroup();
	void CamShutDownLocal();
	void CamShutDownAgent();

	//Init Movie File
	void InitMovieGroup();
	void InitMovieLocal();
	void InitMovieAgent();

	//View Large
	void VIewLargeGroup(int nLarge);
	void VIewLargeLocal(int nLarge);
	void VIewLargeAgent(int nLarge);

	//GetFoucs
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	void GetFocusGroup(BOOL bSave = FALSE);
	void GetFocusLocal(BOOL bSave = FALSE);
	void GetFocusAgent(BOOL bSave = FALSE);
	void SetFocusGroup();
	void SetFocusLocal();
	void SetFocusAgent();
	void SetFocus(CString strDSCId, int nSetFocus);
	void SetFocusLoadFile(CString strPath);

	//Movie Save Check
	void DoMovieSaveCheck(bool bChaeck);
	void DoMovieSaveCheckLocal(bool bChaeck);
	void DoMovieSaveCheckAgent(bool bChaeck);

	void DoMovieMakeFrameMovie(vector<MakeFrameInfo>* pMovieInfo,  int nMovieNum, BOOL bPlay = FALSE, BOOL bLocal = FALSE);

	void RequestData(vector<MakeFrameInfo>* pMovieInfo);

	// gh5 pre record init
	void SetPreRecInitGH5Group();
	void SetPreRecInitGH5Local();
	void SetPreRecInitGH5Agent();

	//Tick Sync
	void GetTickCmdGroup(int nCount=0);		// resync에 쓰기위해
	void GetTickCmdLocal(int nCount);
	void GetTickCmdAgent();
	static unsigned WINAPI GetTickCmdCountThread(LPVOID param);
	void GetTickGroup();
	void GetTickLocal();
	void GetTickAgent();
	int SyncTickCount();
	static unsigned WINAPI GetTickCountThread(LPVOID param);

	//-- Set RC Manager
	void SetRCMgr(CESMRCManager* pMgr) { m_pRCMgr = pMgr;}
	CESMRCManager* GetRCMgr() { return m_pRCMgr;}


	BOOL DeleteMakeDSCList(CString str);

	//jhhan 170324 CheckValid Thread
	CWinThread *m_pThread;
	BOOL m_bThread;
	BOOL m_bStatus;
	static UINT WINAPI GetCheckRecordFrame(LPVOID pParam);
	void WaitThreadEnd();
	int m_nGrpSec[MAX_4DA_CAMERA];
};

