////////////////////////////////////////////////////////////////////////////////
//
//	DSCItem.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ESMFunc.h"
#include "ESMIndex.h"
#include "ESMIndexStructure.h"
#include "ESMCtrl.h"
#include "SdiDefines.h"
#include "DSCViewDefine.h"
#include "SdiSingleMgr.h"

#include "ESMRCManager.h"

class CSdiSingleMgr;
class CESMRCManager;

class CDSCItem : public CObject
{
	//-- For Serialization
	DECLARE_SERIAL( CDSCItem )

public:
	CDSCItem();
	CDSCItem(CString strUSBID, int bREMode);
	virtual ~CDSCItem();

public://(CAMREVISION)
	int m_nType;		//DSC_LOCAL(Server), DSC_TOSS(Agent), DSC_REMOTE(Server/network), DSC_UNPREPARED(List Load)
	int m_nRec;			//DSC_REC_NONE,	DSC_REC_ON,	DSC_REC_STOP, DSC_REC_BTN	
	CRITICAL_SECTION _DSCItem;
	int		m_nHeight, m_nWidth;

	//wgkim 190527
	CRect m_rtMargin;

	int m_nSyncCount;
	int m_nCheckCount;
	int m_nCheckTime;
	int m_nCheckMin;
	int m_nCheckMax;

	BOOL m_bDelete;	

	BOOL m_bReSyncGetTickReady;

	BOOL m_bTemperatureWarning;

	int m_nResyncGetTickCount;

public:
	CSdiSingleMgr* m_pSdi;
	HWND m_hParentWnd;	// AfxGetMainWnd()->GetSafeHwnd();
	int m_nStatus;
	CString m_strCaptureInfo[DSC_INFO_CAPTURE_CNT];
	
	//-- 2013-09-06 hongsu@esmlab.com
	CESMEdit	*m_pStaticInfo[DSC_INFO_CNT];
	CESMEdit	*m_pStaticAdj[DSC_ADJ_CNT];
	CESMEdit	*m_pStaticProp[DSC_PROP_CNT];	
	CESMEdit	*m_pStaticTesting[TESTING_PROP_CNT];
	
	//-- 2013-09-05 hongsu@esmlab.com	
	float	m_nAdj[DSC_ADJ_CNT];
	CString m_strInfo[DSC_INFO_CNT];
	CString m_strProp[DSC_PROP_CNT];
	float	m_nTesting[TESTING_PROP_CNT];
	//CStatic	m_IconPicture;					//-- Draw Picture Icon	
	CDC*	m_memImageDC;						//-- Draw Picture Icon	DC
	int		m_nTakenPictureTime;				//-- Icon Time (milli second)	
	int		m_nMovieTime;
	int		m_nRemoteID;						//-- Remote Socket ID				
	int		m_nFocus;							//-- Focus Position
	int		m_nIndex;							//-- 
	bool	m_bMovieSaveCheck, m_bMovieCancel;	//-- Movie Save  (TRUE:Save		FLASE:Not Save)
	BOOL	m_bTickSuccess;	
	int		m_nCurFocus, m_nMaxFocus, m_nMinFocus;
	int		m_nTickTime;
	CString m_strIP;
	//BOOL	m_bGetStartTime;

	//-- 2015-10-22 joonho.kim GOP
	int		m_nFileIndex;
	CString m_strPrevRecordProfile;

	BOOL	m_bPreRecInitGH5;
	int		m_nCamVersion;

public:	
	void InitStatic();
	void RemoveStaticAll();
	void ResetSyncDataAll();

	int	GetType()	{ return m_nType; }
	void SetType(int nType){	m_nType = nType; }
	void SetSDIMgr(CSdiSingleMgr* pMgr);//
	CSdiSingleMgr* GetSDIMgr() {return m_pSdi;};//
	void DeleteSdiMgr();
	void SetIndex(int nIndex);
	int GetIndex();
	//------------------------------------------------------
	//-- Message
	//------------------------------------------------------
	void SdiAddMsg(ESMEvent* pMsg);// {};
	
	//------------------------------------------------------
	//-- Interface
	//------------------------------------------------------	
	CString GetDeviceDSCID	();
	CString GetDeviceVendor	();
	CString GetDeviceModel	();
	CString GetDeviceUniqueID();
	CString GetSavedFile	();// {return m_pSdi->GetSavedFile();}
	CString GetFirstFrame	();
	//------------------------------------------------------
	//-- LiveView
	//------------------------------------------------------
	void SetLiveview(CDSCItem* pItem, BOOL b);
	void InitLiveView();

	//////////////////////////////////////////////////////////////////////////
	// GH5 rec pre init
	void SetInitPreRec(BOOL b);
	BOOL GetInitPreRec();

	//////////////////////////////////////////////////////////////////////////
	// cam version
	void SetCamVersion(int nVer);
	int GetCamVersion();

	//------------------------------------------------------
	//-- Draw
	//------------------------------------------------------
	void SetPictureTime(int nTime)	{ m_nTakenPictureTime = nTime;}
	int  GetPictureTime()			{ return  m_nTakenPictureTime;}
	bool IsDrawPicture();	

	//------------------------------------------------------
	//-- Movie
	//------------------------------------------------------
	void SetSavedLastTime(int nTime)	{ m_nMovieTime = nTime;}
	int GetSavedLastTime()				{ return m_nMovieTime; }

	//------------------------------------------------------
	//-- Set Info
	//------------------------------------------------------
	void SetDSCInfo	(int n, CString str);//	{m_strInfo[n]	= str	;	}
	void SetDSCAdj	(int n, float nData);//	{m_nAdj[n]		= nData	;	}
	void SetDSCProp	(int n, CString str);//	{m_strProp[n]	= str	;	}
	void UpdateAdj  ();
	void SetMovieCancel(BOOL bMovieCancel);
	void SetDSCCaptureInfo(int nType, CString strValue);
	void SetTickStatus(BOOL bTickStatus);	

	//------------------------------------------------------
	//-- GetInfo
	//------------------------------------------------------	
	CString GetDSCInfo(int nInfo);
	float	GetDSCAdj(int nAdj)		{return m_nAdj[nAdj];}
	CString GetDSCProp(int nProp)	{return m_strProp[nProp];}
	BOOL    GetTickStatus()			{return m_bTickSuccess;}	
	CString GetDSCCaptureInfo(int n);

	//------------------------------------------------------
	//-- Status
	//------------------------------------------------------
	int GetDSCStatus()	{ return m_nStatus; }
	CString GetStatusString() { return GetStatusString(m_nStatus);}
	CString GetStatusString(int nStatus);
	void SetDSCStatus(int nStatus);	
	BOOL CheckDSCStatus(int nStatus);	
	//-- Rec
	void SetDSCRecStatus(int nRec);
	int GetDSCRecStatus() { return m_nRec; }
	//------------------------------------------------------
	//-- Row (Line)
	//------------------------------------------------------
	void ChangeRow(int nLine);
	int GetRow() {if(!m_pStaticInfo[0]) return 0; return m_pStaticInfo[0]->GetRow(); }

	//------------------------------------------------------
	//-- 3D Info ( Adjust)
	//------------------------------------------------------
	void SetAdjInfo(BOOL bRight, ESM3DInfo* p3DInfo);

	void SetFocus(int nFocus);
	int Focus();
	void SetFocusData(int nCurFocus, int nMaxFocus, int nMinFocus);
	void SaveFocus(int nMaxFocus, int nMinFocus, int nCurFocus);
	void LoadFocus();
	int GetLoadFocusValue(CString strPath="");
	void SetFocusData(CString strPath, int nFocus);

	//------------------------------------------------------
	//-- Sync (Record)
	//------------------------------------------------------
	double	m_tDSC;
	int		m_tPC;
	BOOL DSCGetTickCount_resync(int& tPC, double& tDSC, int& nCheckCnt, int& nCheckTime, int nDSCSyncTime);
	BOOL DSCGetTickCount(int& tPC, double& tDSC, int& nCheckCnt, int& nCheckTime, int nDSCSyncTime);
	void SetTickCount(double nStartTime, int nIntervalSensorOn);
	void SetResume(double nFrameCount);

	//------------------------------------------------------
	//-- Load/Save Serialize
	//------------------------------------------------------
	void Serialize( CArchive& archive, stAdjustInfo* pAdj = NULL);
	
	//------------------------------------------------------
	//-- Moviefile 
	//------------------------------------------------------
	int m_nMovieSize;
	int m_nMovieFileClose;
	void IntitMovieSize() {  m_nMovieSize =0;}
	void AddMovieSize(int FileSize) { m_nMovieSize += FileSize;};
	void SetMovieFileClose(int nClose){ m_nMovieFileClose = nClose;};
	int GetMovieSize(){return m_nMovieSize;};
	int GetMovieFileClose(){return m_nMovieFileClose;};


	//------------------------------------------------------
	//-- Exception DSC
	//-- 2014-09-15 changdo
	//------------------------------------------------------
	BOOL m_bCaptureExcept;			
	void SetCaptureExcept(BOOL b)	{ m_bCaptureExcept = b; }
	BOOL GetCaptureExcept()			{return m_bCaptureExcept;}

	BOOL m_bCaptureImageRecExcept;			
	void SetCaptureImageRecExcept(BOOL b)	{ m_bCaptureImageRecExcept = b; }
	BOOL GetCaptureImageRecExcept()			{return m_bCaptureImageRecExcept;}

	//------------------------------------------------------
	//-- HWND
	//
	//------------------------------------------------------
	HWND m_hwnd;
	void SetHWND(HWND h) { m_hwnd = h;}
	

	//------------------------------------------------------
	//-- Exception DSC
	//-- 2014-09-15 changdo
	//------------------------------------------------------
	BOOL m_bRecordReady;			
	void SetRecordReady(BOOL b)	{ 	m_bRecordReady = b;	}
	BOOL GetRecordReady()			{return m_bRecordReady;}

	//------------------------------------------------------
	//-- Sync Frame
	//-- 2014-09-15 hongsu@esmlab.com	
	//------------------------------------------------------
	BOOL 	m_bSensorSync;					//-- Sensor Sync	
	void SetSensorSync(BOOL b);
	BOOL GetSensorSync()			{ return m_bSensorSync; }		

	int m_nSensorOnDesignation;
	void SetSensorOnDesignation(int nSensorOnDesignation) { m_nSensorOnDesignation = nSensorOnDesignation; }
	int GetSensorOnDesignation() { return m_nSensorOnDesignation; }

	BOOL m_bPauseMode;
	void SetPauseMode(BOOL bPauseMode) { m_bPauseMode = bPauseMode; }
	BOOL GetPauseMode() {return m_bPauseMode;}
	int m_nDelayTime;
	void SetDelayTime(int nDelayTime) { m_nDelayTime = nDelayTime; }
	int GetDelayTime() { return m_nDelayTime; }
	//(CAMREVISION)
	BOOL m_bRecordStep;
	void SetRecordStep(BOOL bStep) { m_bRecordStep = bStep; }
	BOOL GetRecordStep() { return m_bRecordStep; }
	//-- Execute Time
	UINT m_nExecuteTickTime;
	void SetExecuteTime(UINT nTime)	{ m_nExecuteTickTime = nTime; }
	UINT GetExecuteTime()			{ return m_nExecuteTickTime; }

	void SetWidth(int nWidth);
	void SetHeight(int nHeight);
	int GetWidth();
	int GetHeight();

	//-- Error Frame (Valid Index)	
	//-- Send to Movie Manager
	void AddErrorFrame(int nIndex, int nInterval);	

	//-- ReSync GetTick Ready
	void SetReSyncGetTickReady(BOOL b);
	BOOL GetReSyncGetTickReady();

	void SetTemperatureWarning(BOOL b);
	BOOL GetTemperatureWarning();

	//------------------------------------------------------
	//-- Program Version ����
	//------------------------------------------------------
private:
	float	m_nVersion;
public:
	void SetVersionInfo(float nVersion) {m_nVersion = nVersion;}
	float GetVersionInfo() { return m_nVersion;}

public:
	CESMRCManager * m_pRCClient;

	//180313 hjcho
	BOOL	m_bReverse;

	//180410 hjcho
	void SetMargin(stMarginInfo st){m_stMargin = st;}
	void SetMargin(int nX,int nY){m_stMargin.nMarginX = nX,m_stMargin.nMarginY = nY;}
	stMarginInfo GetMargin(){return m_stMargin;}
	CRect GetMarginRect(){return m_rtMargin;}
private:
	stMarginInfo m_stMargin;

	//jhhan 180508 Group
	CString m_strGroup;


	//wgkim 180512
	int m_nVertical;
	CPoint m_ptVerticalCenterPoint;

	BOOL m_bDetect;

public:
	void SetGroup(CString str) { m_strGroup = str; }
	CString GetGroup() { return m_strGroup; }

	//wgkim 180512
	void SetVertical(int nVertical) {m_nVertical = nVertical;}
	int GetVertical() { return m_nVertical; }

	void SetPtVerticalCenterPoint(CPoint ptVerticalCenterPoint)	{ m_ptVerticalCenterPoint = ptVerticalCenterPoint;}
	CPoint GetPtVerticalCenterPoint() { return m_ptVerticalCenterPoint; }

	void SetResyncGetTickCount(int nCount) { m_nResyncGetTickCount = nCount; }
	int GetResyncGetTickCount() { return m_nResyncGetTickCount; }

	void SetDetect(BOOL bOn) { m_bDetect = bOn; }
	BOOL GetDetect() { return m_bDetect; }
};

