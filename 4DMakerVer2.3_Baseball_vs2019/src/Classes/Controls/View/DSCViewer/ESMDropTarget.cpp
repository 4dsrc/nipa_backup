////////////////////////////////////////////////////////////////////////////////
//
//	ESMDropTarget.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-27
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "afxole.h" 
#include "ESMDropTarget.h" 

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CESMDropTarget::CESMDropTarget()	{}
CESMDropTarget::~CESMDropTarget()	
{
	Revoke();
}

DROPEFFECT CESMDropTarget::OnDragEnter(CWnd* pWnd, COleDataObject* 
                           pDataObject, DWORD dwKeyState, CPoint point )
{       
    if((dwKeyState&MK_CONTROL) == MK_CONTROL)	
		return DROPEFFECT_COPY; // Copy the source text
    else										
		return DROPEFFECT_MOVE; // Move the source text
} 

void CESMDropTarget::OnDragLeave(CWnd* pWnd)
{
    COleDropTarget:: OnDragLeave(pWnd);
}
 

DROPEFFECT CESMDropTarget::OnDragOver(CWnd* pWnd, COleDataObject* 
           pDataObject, DWORD dwKeyState, CPoint point )
{
    if((dwKeyState&MK_CONTROL) == MK_CONTROL)
        return DROPEFFECT_COPY;
    else
        return DROPEFFECT_MOVE;    
}


BOOL CESMDropTarget::OnDrop(CWnd* pWnd, COleDataObject* pDataObject, 
                 DROPEFFECT dropEffect, CPoint point )
{           
    HGLOBAL  hGlobal;
    LPCSTR   pData;

    hGlobal=pDataObject->GetGlobalData(CF_OWNERDISPLAY); //Change to CF_TEXT if you are only working with text

    pData=(LPCSTR)GlobalLock(hGlobal);    
    ASSERT(pData!=NULL); 
                   
	pWnd->SetWindowText((LPCTSTR)pData);
    GlobalUnlock(hGlobal);

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// 

BOOL CESMDropTarget::InitDropTarget(CWnd* wnd)
{
	BOOL success = Register(wnd);
	if(!success )
		return FALSE;
	return TRUE;
}
