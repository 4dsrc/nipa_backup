////////////////////////////////////////////////////////////////////////////////
//
//	DSCFrameSelector.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-05
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "DSCInfoViewBase.h"
#include "DSCViewDefine.h"
#include "ESMTimeLineObjectSelector.h"


//-- 2013-09-27 hongsu@esmlab.com
//-- For Drag
#include "ESMDropSource.h"
void _DrawFrameSelector(void *param);

#define MAX_SELECT_LINE 50

struct TIMELINE_SELECT_SPOT
{
	CString strDSC;
	int nSelectLine;	
	int nTime;			// milli second
};

class CDSCFrameSelector : public CDSCInfoViewBase
{
	DECLARE_DYNCREATE(CDSCFrameSelector)
public:
	CDSCFrameSelector() {};
	CDSCFrameSelector(CDSCViewer* pParent);
	virtual ~CDSCFrameSelector();


	// Attributes
private:	
	CStatic		m_StaticTri;
	int			m_nRec;
	int			m_nRecTime;
	int			m_nCheckTime;
	int			m_nCheckStartTime;
	int			m_nCheckEndTime;
	CESMStatic	m_StaticTime;
	CESMStatic	m_StaticLine;
	CESMStatic	m_SelectLine1;		//2014/09/21 kcd Select Line
	CESMStatic	m_SelectLine2;
	CESMStatic	m_SelectLine3;
	CESMStatic	m_SelectLine4;

	CESMStatic	m_MultiSelectLine[MAX_SELECT_LINE];
	int			m_nMultiLine;

	//2014-01-09 kcd SelectSpot ǥ��
	int m_nSelectedDsc;
	int m_nSelectedTime;
	BOOL m_bIsShowSpot;
	
public:	
	int		m_nHeaderTop;
	int		m_nTickMajorHeight;
	int		m_nTickMinorHeight;	
	int		m_nTickMajorSize;
	int		m_nTimeMax;
	
	int		m_nLineLeftMargin;
	int		m_nLineRightMargin;
	int		m_nTickFreqMinor;
	int		m_nTickFreqMajor;	

	int		m_nLineHeight;
	int		m_nTextMargin;
	int		m_nHandleWidth;
	int		m_nLineTopMargin;
	CPoint	m_ptScroll;
	CPoint	m_ptMouseValue;	
	int		m_nEndLine;
	BOOL	m_bLBtnDown;
	CPoint	m_ptMouse;
	//  2013-10-14 Ryumin
	BOOL	m_bUpdateDraw;
	int		m_nSelectLineTime[MAX_SELECT_LINE];
	CString	m_nSelectLineDSC;

	TIMELINE_SELECT_SPOT	m_spotSelect;
	TIMELINE_SELECT_SPOT	m_prevSelect;
	CESMTimeLineObjectSelector* m_pTimeLineObject;
	
	void	FocusWindow();
	//------------------------------------------------------------------------------
	//! @function	Various Helper Functions
	//------------------------------------------------------------------------------			
	void	UpdateSizes();

	//------------------------------------------------------------------------------
	//! @function	Position Functions for Scalable Rendering
	//------------------------------------------------------------------------------		
	int		GetSpanTop(int nLineCnt);
	int		GetTotalHeight();
	int		GetSpanWidth();
	int		GetNumberMajorTicks();

	//------------------------------------------------------------------------------
	//! @function	Rec State Get 
	//------------------------------------------------------------------------------		
	int GetDSCRecStatus() { return m_nRec; }
	int GetSelectedDsc() { return m_nSelectedDsc; }
	int GetSelectedTime() { return m_nSelectedTime; }
	void SetSelectedTime(int nTime){m_nSelectedTime = nTime;}
	

	//------------------------------------------------------------------------------
	//! @function	inverse position functions
	//------------------------------------------------------------------------------	
	int		GetTimeFromX(int x);	
	int		GetTimeFromXRange(int x);	
	int		GetXFromTime(int nTime);

	int		GetLineFromY(int nPos);
	int		GetYFromLine(int nLine);
	int		GetRecTime() { return m_nRecTime; }

	//------------------------------------------------------------------------------
	//! @function	DRAW
	//------------------------------------------------------------------------------	
	void	DrawEditor(BOOL bRemote = FALSE);
	void	DrawTimeLine(CDC* pDC);
	void	DrawMovieLine(CDC* pDC,BOOL bRemote = FALSE);
	void	DrawTimeLineObject(CDC* pDC);
	// Draw TimeLine
	void	MoveTimeLine(CPoint pt);
	void	SelectTimeLine(int nSelectTime, int nLine = 0);
	void	DrawIcon(CDSCItem* pDSCItem);
	// Draw Recording Bar
	void	DrawRecBar();
	void	DrawFilm(CDC* pDC, int nTop, int nRecStatus);
	void	DrawIcon(CDC* pDC, CDSCItem* pItem, int nLeft, int nTop);
	void	DrawSelectedSpot(CDC* pDC,BOOL bRemote = FALSE);
	void	ResetTimeLineObject();
	void	SpotViewState(BOOL bIsShowSpot, int nSelectedDsc = -1, int nSelectedTime = -1);
	// Operations
public:
	void	SetTimeMax(int Max);	
	void	Refresh();
	void	SelectLine(CPoint pt);
	void	InsertDSCList(CObArray* pAr, int nSelStart, int nSelEnd);

	//-- Draw Rec
	void	SetRecStatus(int nStatus);	
	virtual void SetGridStatus(int nRow, int nStatus);

	//-- 2013-09-29 hongsu@esmlab.com
	BOOL SetSavedLastTime(int nTime);
	void SetInitRecTime(){m_nRecTime = 0; }
	void SetRecTime(int nRecTime){m_nRecTime = nRecTime; }
	void RemoveObject();
	void UpdateObject(CESMTimeLineObject* pObject);
	//  2013-10-14 Ryumin
	void UpdateObject(CObArray* pAr, int nSelStart, int nSelEnd);
	void SetSelectTimeLine(int nSelectTime, int nLine = 0);
	void InitLine() {  m_nMultiLine = 0;}
	int  SetSelectDscIndex(int nSelectTime);
	void ReflashSelectTimeLine();
	void SelectFrameLine(int nLine = 0);
	void SelectFrameLine(CPoint point, int nLine = 0);
	void SelectFrameLine(int nTime, int DSCnum, int nLine = 0);


	void	SetRecStatusEditer(DSC_REC_STATUS nStatus);	

	//hjcho 08181818
	int SetSelectAUTOLeftDscIndex(int nSelectTime);
	int SetSelectAUTORightDscIndex(int nSelectTime);

	//hjcho 170728
	CString GetAutoDscID(CString strCurDSC);

	//hjcho 171218
	void ShowFrameSpotFromRemote(int nDSCIndex,int nCount,int nTime);
	//------------------------------------------------------------------------------
	//! @function	DRAW
	//------------------------------------------------------------------------------	
	//void CreatePictureIcon(int nIndex);

protected:
	CFont *m_pFont;
	void SetFrameFont(CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);	

protected:
	void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
public:
	virtual BOOL OnScroll(UINT nScrollCode, UINT nPos, BOOL bDoScroll = TRUE);


	// Generated message map functions
	//{{AFX_MSG(CDSCFrameSelector)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};

