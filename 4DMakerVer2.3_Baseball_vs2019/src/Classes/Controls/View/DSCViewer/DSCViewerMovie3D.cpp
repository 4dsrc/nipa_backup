////////////////////////////////////////////////////////////////////////////////
//
//	DSCViewerMovie3D.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCListViewer.h"
#include "DSCViewer.h"
#include "SdiSingleMgr.h"
#include "ESMImgMgr.h"
#include "ESMUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CDSCViewer::DoMakeMovie3D()
{
	HANDLE handle;
	handle = (HANDLE) _beginthread( _DoMakeMovie3D, 0, (void*)this); // create thread
}

//IplImage* CDSCViewer::Create3DPicture(CvSize sizeMovie, CString strPicLeft, CString strPicRight)
//{
//	//-- 2013-02-05 hongsu.jung		
//	//-- Make Left 50%, Right 50%
//	CStringA ansiStrPicLeft(strPicLeft);
//	CStringA ansiStrPicRight(strPicRight);
//
//	IplImage * image1 = cvLoadImage(ansiStrPicLeft);
//	if(!image1)
//		return NULL;
//
//	IplImage * image2 = cvLoadImage(ansiStrPicRight);
//	if(!image2)
//		return NULL;
//
//	//-- 2013-02-05 hongsu.jung
//	//-- Full HD 1920 X 540 (1080/2)
//	CvSize size_half = cvSize(FULL_HD_WIDTH/2, FULL_HD_HEIGHT);
//
//	IplImage * temp1	= cvCreateImage(size_half	, image1->depth, image1->nChannels);
//	cvResize( image1, temp1 );		
//
//	IplImage * temp2	= cvCreateImage(size_half	, image2->depth, image2->nChannels);
//	cvResize( image2, temp2 );
//
//
//	//-- 3D Image
//	IplImage * temp3D	= cvCreateImage (sizeMovie	, image1->depth, image1->nChannels);
//
//	cvSetImageROI (temp3D, cvRect(	0			, 0				,
//		temp1->width, temp1->height));
//	cvCopy(temp1, temp3D);
//	cvSetImageROI (temp3D, cvRect(	temp1->width, 0				,
//		temp2->width, temp2->height));
//	cvCopy(temp2, temp3D);
//
//	cvResetImageROI(temp3D);
//
//	//-- Release Image
//	cvReleaseImage( &image1 );		
//	cvReleaseImage( &image2 );		
//	cvReleaseImage( &temp1 );	
//	cvReleaseImage( &temp2 );
//
//	ESMLog(5,_T("Create 3D Picture (Left[%s], Right[%s])"),strPicLeft, strPicRight);
//	return temp3D;
//}

void _DoMakeMovie3D(void *param)
{
	CDSCViewer* pLiveView = (CDSCViewer*)param;
	_endthread();
}


