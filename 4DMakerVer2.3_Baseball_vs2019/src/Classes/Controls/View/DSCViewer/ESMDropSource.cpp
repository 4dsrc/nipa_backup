////////////////////////////////////////////////////////////////////////////////
//
//	ESMDropSource.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-27
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"  
#include "afxole.h"
#include "ESMDropSource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CESMDropSource::CESMDropSource()
{
}


CESMDropSource::~CESMDropSource()
{
}


DROPEFFECT CESMDropSource::StartDragging(DWORD_PTR Data, RECT * rClient, CPoint * MousePos)
{
	//Use the following if you are only using text
	//HGLOBAL hgData=GlobalAlloc(GPTR,10 );   
	HGLOBAL hgData = GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, 50);
	ASSERT(hgData != NULL);

	//wgkim 190925 Type, Size Change LPCSTR -> char*, 10 -> 50
	char* lpData = (char*)GlobalLock(hgData);
	ASSERT(lpData != NULL);
	sprintf_s(lpData, 50, "%lld", Data);

	//Use the following if you are only using text
	//CacheGlobalData(CF_TEXT, hgData);   
	CacheGlobalData(CF_OWNERDISPLAY, hgData);
	//CacheGlobalData(CF_DSPENHMETAFILE, hgData);

	DROPEFFECT dropEffect = DoDragDrop(DROPEFFECT_COPY | DROPEFFECT_MOVE, (LPCRECT)rClient);

	if ((dropEffect & DROPEFFECT_MOVE) == DROPEFFECT_MOVE)
		CompleteMove();

	LPARAM lparam;

	lparam = MousePos->y;
	lparam = lparam << 16;
	lparam &= MousePos->x;

	SendMessage(GetActiveWindow(), WM_LBUTTONUP, 0, lparam);

	//19.01.13 joonho.kim 프로그램오류발생 임시 주석 
	//Empty();
	return dropEffect;
}
