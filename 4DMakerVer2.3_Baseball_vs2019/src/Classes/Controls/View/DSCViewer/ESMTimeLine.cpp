////////////////////////////////////////////////////////////////////////////////
//
//	CESMTimeLine.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-08-21
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMTimeLine.h"

class CESMTimeSlice
{
private:
	int m_time_start; // start time
	int m_time_length; // length of action
	double m_action_weight; // what is the action weight, or action details

private:
	char compare(const CESMTimeSlice & c);

public:
	// basic management details
	CESMTimeSlice();
	CESMTimeSlice(const CESMTimeSlice & c);
	CESMTimeSlice(int start, int length, double weight);
	~CESMTimeSlice();

	void Copy(const CESMTimeSlice & c);
	bool operator <  (const CESMTimeSlice & c);
	bool operator >  (const CESMTimeSlice & c);
	bool operator == (const CESMTimeSlice & c);
	bool operator <= (const CESMTimeSlice & c);
	bool operator >= (const CESMTimeSlice & c);


	bool isIn(int time);

	void SetStart(int start);
	void SetLength(int length);
	void SetWeight(double weight);
	int GetStart();
	int GetLength();
	double GetWeight();

	void Edit(int start, int length, double weight);
	int get_time_end();
};


class CESMTimeSpan
{
private:
	vector<CESMTimeSlice> m_splices;
	int m_action;
	int m_owner;
private:
	void swap(int a, int b);
	void qsort(int l = -100, int r = -100);
	char compare(const CESMTimeSpan & c);
public:
	CESMTimeSpan(int owner = 0, int action = 0);
	CESMTimeSpan(const CESMTimeSpan & c);
	~CESMTimeSpan();
	void Copy(const CESMTimeSpan &c);

	bool operator <  (const CESMTimeSpan & c);
	bool operator >  (const CESMTimeSpan & c);
	bool operator == (const CESMTimeSpan & c);
	bool operator <= (const CESMTimeSpan & c);
	bool operator >= (const CESMTimeSpan & c);


	// data insertion
	bool AddSplice(int start, int length, double weight);
	void SetOwner(int owner);
	void SetAction(int action);

	int GetOwner();
	int GetAction();
	int	 GetSliceAt(int time);

	void Edit(int slice, int start, int length, double weight);
	void Delete(int slice);

	int GetSliceCount();
	void GetSlice(int Slice, int & Start, int & Length, double & Weight);

};

// CPP
// Constructor
CESMTimeSlice::CESMTimeSlice()
{
	m_time_start	=	0;
	m_time_length	=	-1;
	m_action_weight =   0.0;
}

// Copy Constructor, mirror of operator = 
// 100%
CESMTimeSlice::CESMTimeSlice(const CESMTimeSlice & c)
{
	*this = c;
}

CESMTimeSlice::CESMTimeSlice(int start, int length, double weight)
{
	m_time_start		= start;
	m_time_length		= length;
	m_action_weight		= weight;
}

// destructor
CESMTimeSlice::~CESMTimeSlice()
{
	// ZERO THE MEMORY, for Aethistics (SP?)
	m_time_start		= 0.0;
	m_time_length		= 0.0;
	m_action_weight		= 0.0;
}


void CESMTimeSlice::Copy(const CESMTimeSlice & c)
{
	m_time_start		= c.m_time_start;
	m_time_length		= c.m_time_length;
	m_action_weight		= c.m_action_weight;
}

char CESMTimeSlice::compare(const CESMTimeSlice & c)
{
	if(m_time_start < c.m_time_start ) return -1;
	if(m_time_start == c.m_time_start) return  0;
	return 1;
}

int CESMTimeSlice::get_time_end()
{
	return m_time_start + m_time_length;
}

bool CESMTimeSlice::isIn(int time)
{
	return time >= m_time_start && time <= get_time_end() ? true : false;
}


/////////////////////////////////////////////////////////////////////////////////////////////
CESMTimeSpan::CESMTimeSpan(int owner, int action)
{
	m_owner = owner;
	m_action = action;
}

CESMTimeSpan::~CESMTimeSpan()
{
	m_owner = 0;
	m_action = 0;
	m_splices.clear();
}

CESMTimeSpan::CESMTimeSpan(const CESMTimeSpan & c)
{
	*this = c;
}

void CESMTimeSpan::Copy(const CESMTimeSpan & c)
{
	m_action = c.m_action;
	m_owner = c.m_owner;
	m_splices.clear();
	for(int i = 0 ; i < c.m_splices.size(); i++)
	{
		m_splices.push_back(c.m_splices[i]);
	}
}
// basic manager for the time span and time splices

bool CESMTimeSpan::AddSplice(int start, int length, double weight)
{
	CESMTimeSlice Add(start,length,weight);
	m_splices.push_back(Add);
	qsort();
	return true;
}

void CESMTimeSpan::swap(int a, int b)
{
	CESMTimeSlice T;
	T = m_splices[a];
		m_splices[a] =	m_splices[b];
						m_splices[b] = T;
}

void CESMTimeSpan::qsort(int l, int r)
{
	if(l==-100&&r==-100)
	{
		qsort(0, m_splices.size()-1);
	}
	else
	{
		int i, j;
		if(r > l)
		{
			CESMTimeSlice v;
			v = m_splices[r];
			i = l - 1;
			j = r;
			
			for(;;)
			{
				while(m_splices[++i] < v) ;
				while(m_splices[--j] > v) ;
				if(i>=j) break;
				swap(i,j);
			}
			swap(i,r);
			qsort(l,i-1);
			qsort(i+1, r);

		}
	}
}

char CESMTimeSpan::compare(const CESMTimeSpan &c)
{
	if(m_owner < c.m_owner) return -1;
	if(m_owner > c.m_owner) return 1;
	// since owner is equal
	if(m_action < c.m_action) return -1;
	if(m_action > c.m_action) return 1;
	// actions are equal
	return 0;
}


int CESMTimeSpan::GetSliceAt(int time)
{
	for(int i = 0; i < m_splices.size() ; i++)
	{
		if(m_splices[i].isIn(time)==true) return i;
	}
	return -1;
}

void CESMTimeSpan::Delete(int slice)
{
	for(int i = slice; i < m_splices.size() - 1 ; i++)
	{
		m_splices[i] = m_splices[i+1];
	}
	m_splices.pop_back();
}
/////////////////////////////////////////////////////////////////////////////////////////////
CESMTimeLine::CESMTimeLine()
{
}

CESMTimeLine::~CESMTimeLine()
{
	m_ESMTimeSpans.clear();
}

CESMTimeLine::CESMTimeLine(const CESMTimeLine & c)
{
	*this = c;
}

void CESMTimeLine::Copy(const CESMTimeLine & c)
{
	for(int i=0;i<c.m_ESMTimeSpans.size();i++)
	{
		m_ESMTimeSpans.push_back(c.m_ESMTimeSpans[i]);
	}
}
void CESMTimeLine::swap(int a, int b)
{
	CESMTimeSpan T;
		      T = m_ESMTimeSpans[a];
			      m_ESMTimeSpans[a] = m_ESMTimeSpans[b];
								   m_ESMTimeSpans[b] = T;
}
void CESMTimeLine::qsort(int l, int r)
{
	if(l==-100&&r==-100)
	{
		qsort(0, m_ESMTimeSpans.size()-1);
	}
	else
	{
		int i, j;
		if(r > l)
		{
			CESMTimeSpan v;
			v = m_ESMTimeSpans[r];
			i = l - 1;
			j = r;
			
			for(;;)
			{
				while(m_ESMTimeSpans[++i] < v) ;
				while(m_ESMTimeSpans[--j] > v) ;
				if(i>=j) break;
				swap(i,j);
			}
			swap(i,r);
			qsort(l,i-1);
			qsort(i+1, r);

		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
bool CESMTimeLine::AddTimeSpan(int Owner, int Action)
{
	CESMTimeSpan Temp;
	Temp.SetOwner(Owner);
	Temp.SetAction(Action);
	m_ESMTimeSpans.push_back(Temp);
	qsort();
	return true;
}

int CESMTimeLine::GetTimeSpan(int Owner, int Action)
{
	int jump;
	jump = m_ESMTimeSpans.size() - 1;
	int pos = 0;
	CESMTimeSpan Test(Owner, Action);
	while(jump > 0)
	{
		// return conditions
		if(m_ESMTimeSpans[pos] == Test)
			return pos;
		if(m_ESMTimeSpans[pos+jump] == Test)
			return pos + jump;
		
		// jumping
		if(m_ESMTimeSpans[pos+jump] > Test)
		{
			jump /=2;
		}

		if(m_ESMTimeSpans[pos+jump] < Test)
		{
			pos+=jump;
		}

		// failure conditions
		if(pos==m_ESMTimeSpans.size()) return -1;
		while(pos+jump >= m_ESMTimeSpans.size()) jump/=2;
	}
	if(m_ESMTimeSpans[pos] == Test)
		return pos;
	return -1;
}

bool CESMTimeLine::AddTimeSlice(int Span, int Time, int Length, double Weight)
{
	return m_ESMTimeSpans[Span].AddSplice(Time, Length, Weight);
}

int CESMTimeLine::GetTimeSlice(int Span, int Time)
{
	return m_ESMTimeSpans[Span].GetSliceAt(Time);
}
void CESMTimeLine::Edit(int Span, int Slice, int Start, int Length, double Weight)
{
	m_ESMTimeSpans[Span].Edit(Slice,Start,Length,Weight);
}
void CESMTimeLine::Delete(int Span, int Slice)
{
	if(Slice>=0)
	{
		// delete a slice of a span
		m_ESMTimeSpans[Span].Delete(Slice);
	}
	else
	{
		// delete a span
		for(int i = Span; i < m_ESMTimeSpans.size() - 1 ; i++)
		{
			m_ESMTimeSpans[i] = m_ESMTimeSpans[i+1];
		}
		m_ESMTimeSpans.pop_back();
	}
}

bool	CESMTimeSlice::operator <  (const CESMTimeSlice & c) { return compare(c)< 0; } 
bool	CESMTimeSlice::operator >  (const CESMTimeSlice & c) { return compare(c)> 0; } 
bool	CESMTimeSlice::operator == (const CESMTimeSlice & c) { return compare(c)==0; } 
bool	CESMTimeSlice::operator <= (const CESMTimeSlice & c) { return compare(c)<=0; } 
bool	CESMTimeSlice::operator >= (const CESMTimeSlice & c) { return compare(c)>=0; } 
void	CESMTimeSlice::SetStart(int start) { m_time_start = start; } 
void	CESMTimeSlice::SetLength(int length) { m_time_length = length; }
void	CESMTimeSlice::SetWeight(double weight) { m_action_weight = weight; }
int		CESMTimeSlice::GetStart() { return m_time_start; }
int		CESMTimeSlice::GetLength() { return m_time_length; }
double	CESMTimeSlice::GetWeight() { return m_action_weight; }
void	CESMTimeSlice::Edit(int start, int length, double weight) { m_time_start = start; m_time_length = length; m_action_weight = weight; }
bool	CESMTimeSpan::operator <  (const CESMTimeSpan & c) { return compare(c)< 0; } 
bool	CESMTimeSpan::operator >  (const CESMTimeSpan & c) { return compare(c)> 0; } 
bool	CESMTimeSpan::operator == (const CESMTimeSpan & c) { return compare(c)==0; } 
bool	CESMTimeSpan::operator <= (const CESMTimeSpan & c) { return compare(c)<=0; } 
bool	CESMTimeSpan::operator >= (const CESMTimeSpan & c) { return compare(c)>=0; } 
void	CESMTimeSpan::SetOwner(int owner) { m_owner = owner; }
void	CESMTimeSpan::SetAction(int action) { m_action = action; }
int		CESMTimeSpan::GetOwner() { return m_owner ; }
int		CESMTimeSpan::GetAction() { return m_action ; }
void	CESMTimeSpan::Edit(int slice, int start, int length, double weight) { m_splices[slice].Edit(start, length, weight); }
int		CESMTimeSpan::GetSliceCount() { return m_splices.size(); }
void	CESMTimeSpan::GetSlice(int Slice, int & Start, int & Length, double & Weight) {	Start = m_splices[Slice].GetStart();Length = m_splices[Slice].GetLength();Weight = m_splices[Slice].GetWeight();}
int		CESMTimeLine::GetSpanCount() { return m_ESMTimeSpans.size(); }
int		CESMTimeLine::GetSliceCount(int Span) { return m_ESMTimeSpans[Span].GetSliceCount(); }
void	CESMTimeLine::GetSlice(int Span, int Slice, int & Start, int & Length, double & Weight) { m_ESMTimeSpans[Span].GetSlice(Slice,Start,Length, Weight); }
void	CESMTimeLine::SetAction(int Span, int Action) { m_ESMTimeSpans[Span].SetAction(Action); }
int		CESMTimeLine::GetAction(int Span) { return m_ESMTimeSpans[Span].GetAction(); }
void	CESMTimeLine::SetOwner(int Span, int Owner) { m_ESMTimeSpans[Span].SetOwner(Owner); }
int		CESMTimeLine::GetOwner(int Span) { return m_ESMTimeSpans[Span].GetOwner(); }


bool isIn(int min, int max, int test)
{
	if(min<=test && test<=max) return true;
	return false;
}

int	CESMTimeLine::isCollision(int Span, int Skip, int Start, int Length)
{
	int cStart, cLength;
	double w;
	for(int i = 0; i < m_ESMTimeSpans[Span].GetSliceCount(); i++)
	{
		if(i!=Skip)
		{
			m_ESMTimeSpans[Span].GetSlice(i,cStart, cLength, w);
			if(isIn(cStart,cStart + cLength,  Start)) return i;
			if(isIn(cStart,cStart + cLength,  Start + Length)) return i;
			if(isIn( Start, Start +  Length, cStart)) return i;
			if(isIn( Start, Start +  Length, cStart + cLength)) return i;
		}
	}
	return -1;
}

int CESMTimeLine::GetSlice(int Span, int Time)
{
	return m_ESMTimeSpans[Span].GetSliceAt(Time);
}