////////////////////////////////////////////////////////////////////////////////
//
//	DSCFrameSelectorMsg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-09
//
////////////////////////////////////////////////////////////////////////////////



#include "stdafx.h"
#include "DSCFrameSelector.h"
#include "DSCViewer.h"
#include "FFmpegManager.h"
#include "ESMFileOperation.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CDSCImagesView
IMPLEMENT_DYNCREATE(CDSCFrameSelector, CDSCInfoViewBase)
BEGIN_MESSAGE_MAP(CDSCFrameSelector, CScrollView)
	//{{AFX_MSG_MAP(CDSCFrameSelector)	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_ERASEBKGND()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_CREATE()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	ON_WM_KEYDOWN()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
END_MESSAGE_MAP()

//------------------------------------------------------------------------------
//! @function	Message Handlers: Mouse
//------------------------------------------------------------------------------
void CDSCFrameSelector::OnLButtonDown(UINT nFlags, CPoint point) 
{	
	CScrollView::OnLButtonDown(nFlags, point);

	//-- 2013-09-30 hongsu@esmlab.com
	//-- Remove Previous Line 
	RemoveObject();

	m_bLBtnDown = TRUE;

	//-- 2013-09-25 hongsu@esmlab.com
	//-- Select Line
	SelectLine(point);
	int nTime = GetTimeFromX(point.x);
	
	//-- Draw Frame
	int nSelectLine = GetSelectLine();
	if(nSelectLine < 0)
		return;

	CDSCItem* pDSCItem = NULL;	
	pDSCItem = GetItemData(nSelectLine);
	if(pDSCItem)
	{
		//-- Check Movie
		if(pDSCItem->GetDSCStatus() != SDI_STATUS_REC_FINISH && ESMGetFilmState() != ESM_ADJ_FILM_STATE_PICTURE)
		{
			if(pDSCItem->GetDSCStatus() != SDI_STATUS_LOAD)
			{
				ESMLog(5,_T("Not Recored Movie File"));
				//jhhan 180918 Editer Mode
				if(ESMGetValue(ESM_VALUE_TEMPLATE_EDITOR) != TRUE)
					return;
			}
		}

		//-- Set Select Time line Spot
		m_spotSelect.strDSC = pDSCItem->GetDeviceDSCID();
		m_spotSelect.nSelectLine = nSelectLine;
		if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		{
			m_spotSelect.nTime = nTime;
			ESMLog(5,_T("Time Line Start Spot [%s][%03.03f]"), m_spotSelect.strDSC, m_spotSelect.nTime);
		}
		else
		{
			m_spotSelect.nTime = DEFAULT_ESM_PICTURE_DRAGTIME;
			ESMLog(5,_T("Time Line Picture Start Spot [%s]"), m_spotSelect.strDSC);
		}

		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent;
		pMsg->message = WM_ESM_VIEW_FRMESPOTVIEWOFF;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

		//-- 2013-09-26 hongsu@esmlab.com
		//-- Redraw
		DrawEditor();
	}	
}

void CDSCFrameSelector::OnLButtonUp(UINT nFlags, CPoint point) 
{
	ESMSetEditObj(TRUE);

	m_bLBtnDown = FALSE;	
	//-- 2013-09-28 hongsu@esmlab.com
	//-- Create Object
	//-- 2013-09-25 hongsu@esmlab.com
	//-- Select Line
	SelectLine(point);
	int nTime = GetTimeFromX(point.x);

	//-- Draw Frame
	int nSelectLine = GetSelectLine();
	if(nSelectLine < 0)
		return;

	CDSCItem* pDSCItem = NULL;	
	pDSCItem = GetItemData(nSelectLine);
	if(pDSCItem)
	{
		//-- Check Movie
		if(pDSCItem->GetDSCStatus() != SDI_STATUS_REC_FINISH && ESMGetFilmState() != ESM_ADJ_FILM_STATE_PICTURE)
		{
			if(pDSCItem->GetDSCStatus() != SDI_STATUS_LOAD)
			{
				ESMLog(5,_T("Not Recored Movie File"));
				//jhhan 180918 Editer Mode
				if(ESMGetValue(ESM_VALUE_TEMPLATE_EDITOR) != TRUE)
					return;
			}
			
		}

		TIMELINE_SELECT_SPOT spotEnd;
		//-- Set Select Time line Spot
		spotEnd.strDSC = pDSCItem->GetDeviceDSCID();
		spotEnd.nSelectLine = nSelectLine;
		int nSelectedTime = 0;
		int nEndTime = 0;
		if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		{
			nSelectedTime = m_spotSelect.nTime;
			nEndTime = nTime;
			spotEnd.nTime = nTime;
			ESMLog(5,_T("Time Line End Spot [%s][%03.03f]"), spotEnd.strDSC, spotEnd.nTime);

			//Select Point
			//ESMSetSelectDSC(spotEnd.strDSC);
		}
		else
		{
			nSelectedTime = 0;
			nEndTime = 0;
			spotEnd.nTime = DEFAULT_ESM_PICTURE_DRAGTIME;
			ESMLog(5,_T("Time Line Picture End Spot [%s]"), spotEnd.strDSC);
		}

		if(m_pTimeLineObject)
		{
			m_pTimeLineObject->DestroyWindow();
			delete m_pTimeLineObject;
			m_pTimeLineObject = NULL;
		}

		//-- Check pItem Index;
		CObArray arDSC;
		InsertDSCList(&arDSC, m_spotSelect.nSelectLine , spotEnd.nSelectLine );
		m_pTimeLineObject = new CESMTimeLineObjectSelector(
			arDSC,
			nSelectedTime,
			nEndTime);
		//-- Clear Array
		arDSC.RemoveAll();

		
		//-- 2013-09-28 hongsu@esmlab.com
		//-- Get Position 
		CPoint ptS, ptE;
		int nScroll = ESMGetScroll();
		ptS.x = GetXFromTime(m_spotSelect.nTime);		
		ptS.y = GetYFromLine(m_spotSelect.nSelectLine);		
		if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
			ptE.x = GetXFromTime(GetTimeFromX(point.x));
		else
			ptE.x = GetXFromTime(DEFAULT_ESM_PICTURE_DRAGTIME);

		int nEndLine = GetLineFromY(point.y+ + nScroll);
		if(nEndLine < 0)
			return;

		if(nEndLine > GetItemCount() - 1)
			return;

		ptE.y = GetYFromLine(nEndLine);
		if(ptS.x > ptE.x)
		{
			int nTemp = ptS.x;
			ptS.x = ptE.x;
			ptE.x = nTemp;		
			m_pTimeLineObject->SetChangePos(TRUE,FALSE);

		}		
		ptS.x -= DSC_ROW_H/2;
		ptE.x += DSC_ROW_H/2;

		if(ptS.y > ptE.y)
		{
			int nTemp = ptS.y;
			ptS.y = ptE.y;
			ptE.y = nTemp;
			m_pTimeLineObject->SetChangePos(TRUE,TRUE);
			
		}
		ptE.y += DSC_ROW_H;
		//m_pTimeLineObject->SetPointES(ptS,ptE);
		ptE.y -= nScroll;
		ptS.y -= nScroll;
		CRect rect = CRect(ptS.x,ptS.y,ptE.x,ptE.y);
		m_pTimeLineObject->Create(NULL, NULL, DS_SETFONT | WS_CHILD | WS_VISIBLE , CRect(ptS.x,ptS.y,ptE.x,ptE.y), this, IDD_VIEW_TIMELINE_OBJ);		
		
		
		/*		
		//  2013-10-15 Ryumin
		//	투명 윈도우
		m_pTimeLineObject->Create(NULL, NULL,
									DS_SETFONT | WS_OVERLAPPED | WS_VISIBLE,
									CRect(ptS.x, ptS.y, ptE.x, ptE.y),
									this, IDD_VIEW_TIMELINE_OBJ);
		m_pTimeLineObject->ModifyStyleEx(0, WS_EX_LAYERED);
		m_pTimeLineObject->SetLayeredWindowAttributes(RGB(0,0,0), 50, LWA_ALPHA);
*/

		//-- 2013-09-26 hongsu@esmlab.com
		//-- Redraw
		DrawEditor();
	}
	CScrollView::OnLButtonUp(nFlags, point);
}

void CDSCFrameSelector::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CScrollView::OnLButtonDblClk(nFlags, point);	
}
// Mouse Movement
void CDSCFrameSelector::OnMouseMove(UINT nFlags, CPoint point) 
{
	//-- 2013-09-27 hongsu@esmlab.com
	//-- Draw Time Line & Shift TimeLine Object 
	//-- 2013-10-10 Ryumin@esm.com
	MoveTimeLine(point);
	//TRACE("%d, %d\r\n", point.x, point.y);

	m_ptMouse = point;

	//DrawEditor();
	CScrollView::OnMouseMove(nFlags, point);
}

// Right Mouse Button Activity
// for dealing with editing
void CDSCFrameSelector::OnRButtonUp(UINT nFlags, CPoint point) 
{
	CScrollView::OnRButtonUp(nFlags, point);
}

void CDSCFrameSelector::OnRButtonDown(UINT nFlags, CPoint point) 
{
	ESMSetKeyValue(VK_F5);
	SelectFrameLine(point);

	CScrollView::OnRButtonDown(nFlags, point);
}

void CDSCFrameSelector::SelectFrameLine(int nLine)
{
	CPoint point;
	CObArray arDSCList;

	ESMGetDSCList(&arDSCList);
	int nDscCount = arDSCList.GetSize(), nTpTime = 0;;
	CString strSelectedDsc = m_nSelectLineDSC;
	if( strSelectedDsc == _T(""))
	{
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_FRAME_RESET;
		::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);
		return;
	}

	int nSelectDsc  = ESMGetDSCIndex(strSelectedDsc);
	if( nSelectDsc < 0 )
		return ;

	CString strDscIp, strFileName;
	CDSCItem* pItem = NULL;
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3;
	CESMFileOperation fo;

	int nStartDsc = nSelectDsc;
	for( int i = nStartDsc; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if(pItem == NULL)
			continue;

		strDscIp.Format(_T("%s"), pItem->m_strInfo[DSC_INFO_LOCATION]);

		int nFIdx = ESMGetFrameIndex(m_nSelectLineTime[nLine]);
		strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);
// 
// 		if(ESMGetExecuteMode())
// 			strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientBackupRamPath(strDscIp),  pItem->GetDeviceDSCID());
// 		else
// 			strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());

		if( pItem->GetSensorSync() == FALSE || !fo.IsFileExist(strFileName))
		{
			ESMLog(1, _T("Select Frame Skip %s"), strFileName);
			nSelectDsc++;
		}
		else
			break;
	}
	if( nSelectDsc == arDSCList.GetCount())
		nSelectDsc = 0;

	SetSelectLine(nSelectDsc);
	point.x = GetXFromTime(m_nSelectLineTime[nLine]);
	//jhhan 170315
	int nYPoint = 0;
	nYPoint = ESMGetGridYPoint();
	if(nYPoint > 0)
	{
		point.y = nYPoint;
	}
	SelectFrameLine(point, nLine);
}

void CDSCFrameSelector::SelectFrameLine(CPoint point, int nLine)
{
	//-- 2013-09-30 hongsu@esmlab.com
	//-- Remove Object 
	RemoveObject();	

	//-- 2013-09-25 hongsu@esmlab.com
	//-- Select Line
	SelectLine(point);
	int nTime;
	if(ESMGetKeyValue() == VK_F5)
		nTime = GetTimeFromX(point.x);
	else
		nTime = m_nSelectLineTime[nLine];

	//-- Draw Frame
	int nSelectLine = GetSelectLine();
	if(nSelectLine < 0)
		return;

	CDSCItem* pDSCItem = NULL;	
	pDSCItem = GetItemData(nSelectLine);
	if(pDSCItem)
	{
		//-- Check Movie
		/*if(pDSCItem->GetDSCStatus() != SDI_STATUS_REC_FINISH && pDSCItem->GetDSCStatus() != SDI_STATUS_CAPTURE_OK  )
		{
			ESMLog(5,_T("Not Recored Movie File"));
			return;
		}*/

		if(ESMGetFilmState() ==ESM_ADJ_FILM_STATE_MOVIE)
		{
			//-- Set Prev Select Time line Spot
			m_prevSelect.strDSC = pDSCItem->GetDeviceDSCID();
			m_prevSelect.nSelectLine = nSelectLine;
			m_prevSelect.nTime = nTime;

			//-- 2013-09-25 hongsu@esmlab.com
			//-- Send Message To Frame
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_FRAME_SHOW;
			pMsg->pDest		= (LPARAM)pDSCItem;
			pMsg->nParam1	= nTime;
			pMsg->nParam2	= nLine;
			::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);

			pMsg = new ESMEvent;
			pMsg->message = WM_ESM_VIEW_FRMESPOTVIEWOFF;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

			//-- 2013-09-26 hongsu@esmlab.com
			//-- Redraw
			SpotViewState(TRUE, nSelectLine, nTime);
			DrawEditor();
		}
		else
		{
			//-- Set Prev Select Time line Spot
			m_prevSelect.strDSC = pDSCItem->GetDeviceDSCID();
			m_prevSelect.nSelectLine = nSelectLine;
			m_prevSelect.nTime = nTime;

			//-- 2013-09-25 hongsu@esmlab.com
			//-- Send Message To Frame
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_FRAME_SHOW;
			pMsg->pDest		= (LPARAM)pDSCItem;
			pMsg->nParam1	= nTime;
			::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);

			pMsg = new ESMEvent;
			pMsg->message = WM_ESM_VIEW_FRMESPOTVIEWOFF;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

			//-- 2013-09-26 hongsu@esmlab.com
			//-- Redraw
			SpotViewState(TRUE, nSelectLine, nTime);
			DrawEditor();
		}
	}	

}

void CDSCFrameSelector::SelectFrameLine(int nTime, int DSCnum, int nLine)
{
	//-- Draw Frame
	int nSelectLine = DSCnum;
	if(nSelectLine < 0)
		return;

	CDSCItem* pDSCItem = NULL;	
	pDSCItem = GetItemData(nSelectLine);

	if(pDSCItem)
	{
		//-- Check Movie
		/*if(pDSCItem->GetDSCStatus() != SDI_STATUS_REC_FINISH && pDSCItem->GetDSCStatus() != SDI_STATUS_CAPTURE_OK  )
		{
			ESMLog(5,_T("Not Recored Movie File"));
			return;
		}*/

		if(ESMGetFilmState() ==ESM_ADJ_FILM_STATE_MOVIE)
		{
			//-- Set Prev Select Time line Spot
			m_prevSelect.strDSC = pDSCItem->GetDeviceDSCID();
			m_prevSelect.nSelectLine = nSelectLine;
			m_prevSelect.nTime = nTime;

			//-- 2013-09-25 hongsu@esmlab.com
			//-- Send Message To Frame
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_FRAME_SHOW;
			pMsg->pDest		= (LPARAM)pDSCItem;
			pMsg->nParam1	= nTime;
			pMsg->nParam2	= nLine;
			::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);

			pMsg = new ESMEvent;
			pMsg->message = WM_ESM_VIEW_FRMESPOTVIEWOFF;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

			//-- 2013-09-26 hongsu@esmlab.com
			//-- Redraw
			SpotViewState(TRUE, nSelectLine, nTime);
			DrawEditor();
		}
		else
		{
			//-- Set Prev Select Time line Spot
			m_prevSelect.strDSC = pDSCItem->GetDeviceDSCID();
			m_prevSelect.nSelectLine = nSelectLine;
			m_prevSelect.nTime = nTime;

			//-- 2013-09-25 hongsu@esmlab.com
			//-- Send Message To Frame
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_FRAME_SHOW;
			pMsg->pDest		= (LPARAM)pDSCItem;
			pMsg->nParam1	= nTime;
			::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);

			pMsg = new ESMEvent;
			pMsg->message = WM_ESM_VIEW_FRMESPOTVIEWOFF;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

			//-- 2013-09-26 hongsu@esmlab.com
			//-- Redraw
			SpotViewState(TRUE, nSelectLine, nTime);
			DrawEditor();
		}
	}	

}

void CDSCFrameSelector::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//-- 2013-09-30 hongsu@esmlab.com
	//-- Remove Object
	ESMSetFrameViewFlag(FALSE);
	BOOL bFrameSkip = FALSE;
	RemoveObject();

	int nTime = m_prevSelect.nTime;
	int nSelectLine = m_prevSelect.nSelectLine;
	int nLastLine = GetItemCount()-1;
	int bDirectionKey = FALSE;
	switch (nChar)
	{
	case ESM_KEY_UP:
		if (nSelectLine == 0)
			return;
		nSelectLine--;
		bDirectionKey = TRUE;
		break;
	case ESM_KEY_DOWN:
		if ( nSelectLine == nLastLine )
			return;
		nSelectLine++;
		bDirectionKey = TRUE;
		break;
	case ESM_KEY_RIGHT:
		if (m_nTimeMax - nTime < movie_next_frame_time )
			return;
		if(ESMGetValue(ESM_VALUE_AUTODETECT))
			ESMSetKeyValue(VK_F10);
		ESMGetNextFrameTime(nTime);
		bDirectionKey = TRUE;
		bFrameSkip = TRUE;
		break;
	case ESM_KEY_LEFT:
		if (nTime == 0)
			return;
		if(ESMGetValue(ESM_VALUE_AUTODETECT))
			ESMSetKeyValue(VK_F10);
		ESMGetPreviousFrameTime(nTime);
		bDirectionKey = TRUE;
		bFrameSkip = TRUE;
		break;
	case VK_PRIOR:
		nSelectLine = ESMGetViewJump(nSelectLine, TRUE);
		//ESMLog(5, _T("VK_PRIOR : %d"), nSelectLine+1);
		bDirectionKey = TRUE;
		break;
	case VK_NEXT:
		nSelectLine = ESMGetViewJump(nSelectLine, FALSE);
		//ESMLog(5, _T("VK_NEXT : %d"), nSelectLine+1);
		bDirectionKey = TRUE;
		break;
	}
	CDSCItem* pDSCItem = NULL;	
	pDSCItem = GetItemData(nSelectLine);
	if(pDSCItem && bDirectionKey == TRUE)
	{
		//-- Check Movie
		/*if(pDSCItem->GetDSCStatus() != SDI_STATUS_REC_FINISH)
		{
			ESMLog(5,_T("Not Recored Movie File"));
			return;
		}*/

		//-- Set Prev Select Time line Spot
		m_prevSelect.strDSC = pDSCItem->GetDeviceDSCID();
		m_prevSelect.nSelectLine = nSelectLine;
		m_prevSelect.nTime = nTime;

		//-- 2013-09-25 hongsu@esmlab.com
		//-- Send Message To Frame
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_FRAME_SHOW;
		pMsg->pDest		= (LPARAM)pDSCItem;
		pMsg->nParam1	= nTime;
		pMsg->nParam2	= ESMGetViewPointNum();
		pMsg->nParam3	= bFrameSkip;   // 이미지 연경우 Skip
		::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);

		pMsg = new ESMEvent;
		pMsg->message = WM_ESM_VIEW_FRMESPOTVIEWON;
		pMsg->nParam1 = nTime;
		pMsg->nParam2 = nSelectLine;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

		//-- 2013-09-26 hongsu@esmlab.com
		//-- Redraw
		DrawEditor();
	}
	CDSCInfoViewBase::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CDSCFrameSelector::OnTimer(UINT_PTR nIDEvent) 
{
	switch(nIDEvent)
	{
	case REC_TIMER_ON:
#if 1
		m_nCheckEndTime = GetTickCount();
		if(m_nCheckEndTime - m_nCheckStartTime > m_nCheckTime)
		{
			//TRACE(_T("########### %d\n"), m_nCheckEndTime - m_nCheckStartTime);
			m_nRecTime += REC_TIMER_DRAW_TIME;		
			//-- Check Total Size
			if(m_nRecTime > m_nTimeMax)
				SetTimeMax(m_nRecTime);
			DrawRecBar();
			ReflashSelectTimeLine();

			if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
			{
				ESMSetRecordingInfoint(_T("RecTime"), m_nRecTime);
			}

			
			m_nCheckTime += REC_TIMER_DRAW_TIME;
			
			if(ESMGetViewScaling())
				DrawEditor();

			//jhhan 181210
			if(m_nCheckTime % 5000 == 0)
			{
				ESMSaveProfile();
			}
		}

		
#else
		//-- Add Time Position
		m_nRecTime += REC_TIMER_DRAW_TIME;		
		//-- Check Total Size
		if(m_nRecTime > m_nTimeMax)
			SetTimeMax(m_nRecTime);
		DrawRecBar();
		ReflashSelectTimeLine();

		if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		{
			ESMSetRecordingInfoint(_T("RecTime"), m_nRecTime);
		}
#endif
		break;
	}
	
	CScrollView::OnTimer(nIDEvent);
}
void CDSCFrameSelector::ShowFrameSpotFromRemote(int nDSCIndex,int nCount,int nTime)
{
	SetSelectTimeLine(nTime,nCount);
	if(SetSelectDscIndex(nTime) == -1)
		return;

	CPoint point;

	int nSelectDsc  = nDSCIndex;
	if( nSelectDsc < 0 )
		return ;

	CString strDscIp, strFileName;
	CDSCItem* pItem = NULL;
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3;
	CESMFileOperation fo;

	int nStartDsc = nSelectDsc;

	SetSelectLine(nSelectDsc);
	point.x = GetXFromTime(m_nSelectLineTime[nCount]);
	//jhhan 170315
	int nYPoint = 0;
	nYPoint = ESMGetGridYPoint();
	if(nYPoint > 0)
	{
		point.y = nYPoint;
	}

	SelectLine(point);
	nTime = GetTimeFromX(point.x);

	//-- Draw Frame
	int nSelectLine = GetSelectLine();
	if(nSelectLine < 0)
		return;

	CDSCItem* pDSCItem = NULL;	
	pDSCItem = GetItemData(nSelectLine);

	m_prevSelect.strDSC = pDSCItem->GetDeviceDSCID();
	m_prevSelect.nSelectLine = nSelectLine;
	m_prevSelect.nTime = nTime;

	SpotViewState(TRUE, nSelectLine, nTime);
	DrawEditor(TRUE);

	//SelectFrameLine(point, nLine);
}
