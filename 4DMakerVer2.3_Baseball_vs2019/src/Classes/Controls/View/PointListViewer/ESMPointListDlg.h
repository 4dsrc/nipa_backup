////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineEditor.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-27
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include <vector>


// CESMFrameNailDlg dialog
class CESMPointListDlg: public CDialog
{
	//-- For Toolbar
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	//DECLARE_DYNAMIC(CESMPointListDlg)
public:
	CESMPointListDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMPointListDlg();

	//-- For Toolbar
	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CESMPointListDlg)
	enum { IDD = IDD_VIEW_POINTLIST_VIEW };
	//}}AFX_DATA

protected:
	//seo
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	CRect m_rcClientFrame;	
	CInnerToolControlBar m_wndToolBar;	
	CListCtrl m_ctrlList;

public:	
	BOOL InitData();
	void SetStartPoint();
	void SetEndPoint();
	int GetStartDsc();
	int GetStartTime();
	int GetEndDSc();
	int GetEndTime();
	void ReSetData();


protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	afx_msg void OnDestroy();

	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	DECLARE_MESSAGE_MAP()


public:
	afx_msg void OnLvnItemchangedPointlistCtrl(NMHDR *pNMHDR, LRESULT *pResult);
};