////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineEditor.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-27
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FileOperations.h"
#include "MainFrm.h"
#include "ESMFunc.h"
#include "ESMPointListDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CFrameNailDlg dialog
//IMPLEMENT_DYNAMIC(CESMPointListDlg, CDialog)
BEGIN_MESSAGE_MAP(CESMPointListDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_POINTLIST_CTRL, &CESMPointListDlg::OnLvnItemchangedPointlistCtrl)
END_MESSAGE_MAP()


CESMPointListDlg::CESMPointListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMPointListDlg::IDD, pParent)	
{	
}

CESMPointListDlg::~CESMPointListDlg()
{
}

void CESMPointListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMPointListDlg)
	//}}AFX_DATA_MAP

	DDX_Control(pDX, IDC_POINTLIST_CTRL, m_ctrlList);
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
}

//seo
BOOL CESMPointListDlg::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN || pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
			return FALSE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

// CFrameNailDlg message handlers
BOOL CESMPointListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitData();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CESMPointListDlg::OnDestroy()
{
}

void CESMPointListDlg::InitImageFrameWnd()
{
}

void CESMPointListDlg::OnSize(UINT nType, int cx, int cy) 
{
	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width()		< FRAME_MIN_WIDTH || 
		m_rcClientFrame.Height()	< FRAME_MIN_HEIGHT)
		return;

	//-- ReloadFrame
	CDialog::OnSize(nType, cx, cy);	
} 

void CESMPointListDlg::OnPaint()
{	
	CDialog::OnPaint();
}


BOOL CESMPointListDlg::InitData()
{
	m_ctrlList.InsertColumn(0, _T("Index"), LVCFMT_LEFT, 100);
	m_ctrlList.InsertColumn(1, _T("DSC Index"), LVCFMT_LEFT, 100);
	m_ctrlList.InsertColumn(2, _T("Time"), LVCFMT_LEFT, 100);
	m_ctrlList.ModifyStyle(LVS_TYPEMASK, LVS_REPORT);
	m_ctrlList.SetExtendedStyle( LVS_EX_FULLROWSELECT );

	m_ctrlList.InsertItem(0, _T("StartPoint"));
	m_ctrlList.InsertItem(1, _T("EndPoint"));
	return TRUE;
}

void CESMPointListDlg::SetStartPoint()
{
	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nSelectedDSC = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedDsc();
	int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
	CString strTp;
	strTp.Format(_T("%d"), nSelectedDSC + 1);
	m_ctrlList.SetItemText(0, 0, _T("StartPoint"));
 	m_ctrlList.SetItemText(0, 1, strTp);
	strTp.Format(_T("%d"), nSelectedTime);
 	m_ctrlList.SetItemText(0, 2, strTp);
}

void CESMPointListDlg::SetEndPoint()
{
	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nSelectedDSC = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedDsc();
	int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();

	CString strTp;
	strTp.Format(_T("%d"), nSelectedDSC + 1);
	m_ctrlList.SetItemText(1, 0, _T("EndPoint"));
 	m_ctrlList.SetItemText(1, 1, strTp);
	strTp.Format(_T("%d"), nSelectedTime);
 	m_ctrlList.SetItemText(1, 2, strTp);
}

int CESMPointListDlg::GetStartDsc()
{
	CString strTp = m_ctrlList.GetItemText(0, 1);
	return _ttoi(strTp);
}

int CESMPointListDlg::GetStartTime()
{
	CString strTp = m_ctrlList.GetItemText(0, 2);
	return _ttoi(strTp);
}

int CESMPointListDlg::GetEndDSc()
{
	CString strTp = m_ctrlList.GetItemText(1, 1);
	return _ttoi(strTp);
}

int CESMPointListDlg::GetEndTime()
{
	CString strTp = m_ctrlList.GetItemText(1, 2);
	return _ttoi(strTp);
}

void CESMPointListDlg::ReSetData()
{
	m_ctrlList.SetItemText(0, 1, _T(""));
	m_ctrlList.SetItemText(0, 2, _T(""));
	m_ctrlList.SetItemText(1, 1, _T(""));
	m_ctrlList.SetItemText(1, 2, _T(""));
}

void CESMPointListDlg::OnLvnItemchangedPointlistCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);


	int nSelectedItem = m_ctrlList.GetNextItem( -1, LVNI_SELECTED );
	CString strKey = m_ctrlList.GetItemText(nSelectedItem, 0);
	if ( strKey == "")
	{
		return ;
	}
	CString strDsc = m_ctrlList.GetItemText(nSelectedItem, 1);
	CString strTime = m_ctrlList.GetItemText(nSelectedItem, 2);
	int nDsc = _ttoi(strDsc) - 1;
	int nTime = _ttoi(strTime);

	//-- 2014-07-10 hongsu@esmlab.com
	//-- exception
	if(nDsc < 0)
	{
		ESMLog(0,_T("Non Selected Item for get Point"));
		return;
	}

	ESMEvent* pMsg = new ESMEvent;
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_VIEW_FRMESPOTVIEWON;
	pMsg->nParam1 = nTime;
	pMsg->nParam2 = nDsc;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

	//-- 2013-09-25 hongsu@esmlab.com
	//-- Send Message To Frame
	CDSCItem* pDSCItem = NULL;	
	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	pDSCItem = pMainWnd->m_wndDSCViewer.GetItemData(nDsc);

	pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_SHOW;
	pMsg->pDest		= (LPARAM)pDSCItem;
	pMsg->nParam1	= nTime;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);

	*pResult = 0;
}