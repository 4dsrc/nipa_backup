////////////////////////////////////////////////////////////////////////////////
//
//	ESMNetworkListCtrl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMMovieListCtrl.h"
#include "ESMMovieListDlg.h"
#include "ESMFunc.h"
#include "ESMIndex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CESMMovieListCtrl::CESMMovieListCtrl(void)
{
	m_nSelectedItem = 0;
	m_bBKColor	= FALSE;
	m_pParent	= NULL;
}

CESMMovieListCtrl::~CESMMovieListCtrl(void)
{

}

BEGIN_MESSAGE_MAP(CESMMovieListCtrl, CListCtrl)
	ON_WM_MEASUREITEM()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CESMMovieListCtrl::OnNMDblclk)
	ON_NOTIFY_REFLECT(NM_CLICK, &CESMMovieListCtrl::OnNMClick)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW,&CESMMovieListCtrl::OnNMCustomdraw)
END_MESSAGE_MAP()

void CESMMovieListCtrl::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	CListCtrl::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CESMMovieListCtrl::OnDestroy()
{
	CListCtrl::OnDestroy();
}

void CESMMovieListCtrl::Clear()
{

}

void CESMMovieListCtrl::DoDataExchange(CDataExchange* pDX)
{

}
void CESMMovieListCtrl::Init(CESMMovieListDlg* pParent)
{
	m_pParent = pParent;
	SetExtendedStyle(LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT );

	// Initialize list control with some data.
	InsertColumn(0, _T("File Name"), LVCFMT_LEFT, 100);
	InsertColumn(1, _T("Time Info"), LVCFMT_LEFT, 50);
	InsertColumn(2, _T("File Path"), LVCFMT_LEFT, 500);

}

void CESMMovieListCtrl::Delete()
{
	DeleteItem(m_nSelectedItem);
	m_pParent->SaveInfo();
	
}

void CESMMovieListCtrl::DeleteAll()
{
	DeleteAllItems();
	m_pParent->DeleteINIFile();
}

void CESMMovieListCtrl::AddMovieList(CString strList,CString strPath,BOOL bFinish)
{
	CString strFinish;
	if(bFinish)
		strFinish.Format(_T("O"));
	else
		strFinish.Format(_T("X"));

	//m_pParent->SaveInfo();
	
	CT2CA pszConvertedAnsiString(strPath);
	std::string strLoadPath(pszConvertedAnsiString);
	VideoCapture vc(strLoadPath);

	CString strTimeInfo;
	double dbTime = 0.0;
	if(vc.isOpened())
	{
		double dFps = vc.get(cv::CAP_PROP_FPS);
		double dTotalFrame = vc.get(cv::CAP_PROP_FRAME_COUNT);

		dbTime = (1/dFps) * dTotalFrame;
		strTimeInfo.Format(_T("%02d:%02d:%02d"),0,(int)dbTime/60,(int)dbTime%60);
	}
	else
		strTimeInfo.Format(_T("-"));

	int nCount = GetItemCount();
	InsertItem(0, strList);
	SetItem(0,1,LVIF_TEXT,strTimeInfo,0,0,0,NULL);
	SetItem(0,2,LVIF_TEXT,strPath,0,0,0,NULL);
}

void CESMMovieListCtrl::LoadInfo()
{
	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_MOVIE_LIST);
}

void CESMMovieListCtrl::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	int nSelectedItem= GetNextItem( -1, LVNI_SELECTED );
	CString strPath = GetItemText(nSelectedItem,2);

	if(!strPath.GetLength())
	{
		ESMLog(5,_T("Non Select File"));
		return;
	}
	
	CString *pPath = new CString;
	pPath->Format(_T("%s"),strPath);

	//pParam  : File Path
	//nParam1 : Play
	//nParam2 : Stop
	//nParam3 : Mode Change

	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_ESM_AJA_SELECT_PLAY;
	pMsg->pParam = (LPARAM)pPath;
	pMsg->nParam1 = FALSE;
	pMsg->nParam2 = FALSE;
	pMsg->nParam3 = FALSE;

	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);

	//AfxMessageBox(GetItemText(nSelectedItem,0));

	*pResult = 0;
}

void CESMMovieListCtrl::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	m_nSelectedItem= GetNextItem( -1, LVNI_SELECTED );
	*pResult = 0;
}
void CESMMovieListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{

}
void CESMMovieListCtrl::SetStatus(BOOL bStatus)
{

}