////////////////////////////////////////////////////////////////////////////////
//
//	ESMNetworkExDlg.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"

#include "ESMMovieListDlg.h"
#include "ESMMovieListCtrl.h"
#include "ESMIndexStructure.h"
class CESMOption;

/////////////////////////////////////////////////////////////////////////////
// CESMMovieListDlg dialog

class CESMMovieListDlg : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	// Construction
public:
	CESMMovieListDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMMovieListDlg();

	// Dialog Data
	//{{AFX_DATA(CESMMovieListDlg)
	enum { IDD = IDD_VIEW_MOVIELIST };
	//}}AFX_DATA
	void InitImageFrameWnd();

protected:
	//seo
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	CRect m_rcClientFrame;	
	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;	
	HWND m_hMainWnd;		
	CESMOption* m_pESMOpt;
		
public:

	void LoadInfo();
	void SaveInfo();
	void OnAddFile(CString strFile,CString strPath,BOOL bFinish);
	CESMMovieListCtrl m_ctrlList;

	//--------------------------------------------------------
	//-- Message
	//--------------------------------------------------------
	void AddMsg(ESMEvent* pMsg);
	void RemoveAllMsg();
	HWND GetCESMMovieListDlgHandle(){return m_hMainWnd;}
	
	
private:	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMMovieListDlg)

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	
	DECLARE_MESSAGE_MAP()

protected:
	// Generated message map functions
	//{{AFX_MSG(CESMMovieListDlg)	
	afx_msg void OnPaint();
	afx_msg void OnAddList();
	afx_msg void OnDeleteList();
	afx_msg void OnDeleteAll();
	afx_msg void OnMovieListOpt();

	//}}AFX_MSG	
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnBnClkPlayPause();
	afx_msg void OnBnClkStop();
	afx_msg void OnBnClkCheck();
public:
	BOOL m_bPlayState;
	CButton *m_ctrlPlay;
	CButton *m_ctrlStop;
	CButton *m_ctrlBoard;
	CStatic *m_ctrlStatus;

	CFont m_font;

public:
	void BoardStatus(BOOL bStatus);
	void DeleteINIFile();
};
