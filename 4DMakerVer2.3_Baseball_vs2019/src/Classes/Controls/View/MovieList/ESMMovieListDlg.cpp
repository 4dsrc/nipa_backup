////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieListDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-04
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMMovieListDlg.h"
#include "resource.h"
#include "MainFrm.h"

#include "ESMIni.h"
#include "ESMUtil.h"

#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
enum{
	AJA_DUMMY,
	AJA_PAUSE,
	AJA_PLAY,
	AJA_STOP
};
enum{
	DUMMY,
	AA,
	BB,
	CC,
	DD,
};
#define MOVIELIST _T("File Name")
#define MOVIEPATH _T("Movie Path")
#define MOVIEINFO _T("Time Info")
/////////////////////////////////////////////////////////////////////////////
// CESMMovieListDlg dialog
CESMMovieListDlg::CESMMovieListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMMovieListDlg::IDD, pParent)	
{	
	WSADATA wsa;
	WSAStartup(MAKEWORD(2,2), &wsa);
	m_ctrlPlay = new CButton;
	m_ctrlStop = new CButton;
	m_ctrlBoard = new CButton;
	m_ctrlStatus = new CStatic;
	m_bPlayState = FALSE;
	//m_font.CreatePointFont(13,_T("굴림"));
	m_font.CreateFont( 13, 13, 0,0,	1,	0, 	0, 	0, 	0, 
		OUT_DEFAULT_PRECIS, 	0, 	DEFAULT_QUALITY, 
		DEFAULT_PITCH | FF_DONTCARE, _T("굴림"));
}

CESMMovieListDlg::~CESMMovieListDlg()
{
	delete m_ctrlPlay;
	delete m_ctrlStop;
	delete m_ctrlBoard;
	delete m_ctrlStatus;
}

void CESMMovieListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMMovieListDlg)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);	
	DDX_Control(pDX, IDC_NETWORK_CTRL, m_ctrlList);

}

//seo
BOOL CESMMovieListDlg::PreTranslateMessage(MSG* pMsg)
{
	if(NULL != pMsg)
	{
		if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN || pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
			return FALSE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMMovieListDlg, CDialog)
	//{{AFX_MSG_MAP(CESMMovieListDlg)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_IMAGE_MOVIELIST_ADD		, OnAddList)
	ON_COMMAND(ID_IMAGE_MOVIELIST_DEL		, OnDeleteList)
	ON_COMMAND(ID_IMAGE_MOVIELIST_DEL_ALL	, OnDeleteAll)
	ON_COMMAND(ID_IMAGE_MOVIELIST_OPT		, OnMovieListOpt)
	ON_COMMAND(AA,&CESMMovieListDlg::OnBnClkPlayPause)
	ON_COMMAND(BB,&CESMMovieListDlg::OnBnClkStop)
	ON_COMMAND(CC,&CESMMovieListDlg::OnBnClkCheck)
	ON_WM_SIZE()	
	ON_WM_PAINT()
	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CESMMovieListDlg message handlers

void CESMMovieListDlg::OnAddList()
{
	CString strRecordFolder;
	strRecordFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE));


	ITEMIDLIST*  pildBrowse; 
	TCHAR   pszPathname[MAX_PATH]; 
	BROWSEINFO  bInfo; 
	memset(&bInfo, 0, sizeof(bInfo)); 
	bInfo.hwndOwner   = GetSafeHwnd(); 
	bInfo.pidlRoot   = NULL; 
	bInfo.pszDisplayName = pszPathname; 
	bInfo.lpszTitle   = _T("Please select a directory"); 
	bInfo.ulFlags   = BIF_RETURNONLYFSDIRS;  
	bInfo.lpfn    = NULL; 
	bInfo.lParam  = (LPARAM)(LPCTSTR)"C:\\"; 
	bInfo.lParam  = (LPARAM)NULL; 
	pildBrowse    = ::SHBrowseForFolder(&bInfo); 
	if(pildBrowse) 
	{ 
		SHGetPathFromIDList(pildBrowse, pszPathname); 
		CFileFind find;
		int res = 1;
		CString strPath;
		strPath.Format(_T("%s\\*.mp4*"), pszPathname);
		find.FindFile(strPath);
		CString fname;
		CString pname;

		while(res)
		{
			res = find.FindNextFile();
			fname = find.GetFileName(); // 파일 및 폴더의 이름을 얻어옴
			pname = find.GetFilePath(); // 전체 패스를 얻어옴

			if(find.IsDirectory()) // 디렉토리 일때
			{
				continue;
			}
			else
			{
				m_ctrlList.AddMovieList(fname,pname,TRUE);
			}
		}
		SaveInfo(); 
	} 
	
	
}

void CESMMovieListDlg::OnDeleteList()
{
	m_ctrlList.Delete();
}

void CESMMovieListDlg::OnDeleteAll()
{
	m_ctrlList.DeleteAll();
}

void CESMMovieListDlg::OnMovieListOpt()
{
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_ESM_ENV_4D_OPTION;
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);	
}

BOOL CESMMovieListDlg::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}

	/*m_ctrlPlay->Create(_T("▶"),BS_DEFPUSHBUTTON,CRect(0,25,25,50),this,AA); 
	m_ctrlStop->Create(_T("■"),BS_DEFPUSHBUTTON,CRect(25,25,50,50),this,BB); 
	m_ctrlBoard->Create(_T("C"),BS_DEFPUSHBUTTON,CRect(50,25,75,50),this,CC); 
	m_ctrlStatus->Create(_T("Loading.."),WS_CHILD|WS_VISIBLE|SS_CENTER,CRect(75,26,250,50),this,DD);*/

	m_ctrlPlay->Create(_T("▶"),BS_DEFPUSHBUTTON,CRect(0,40,25,65),this,AA); 
	m_ctrlStop->Create(_T("■"),BS_DEFPUSHBUTTON,CRect(25,40,50,65),this,BB); 
	m_ctrlBoard->Create(_T("C"),BS_DEFPUSHBUTTON,CRect(50,40,75,65),this,CC); 
	m_ctrlStatus->Create(_T("Loading.."),WS_CHILD|WS_VISIBLE|SS_CENTER,CRect(75,42,250,65),this,DD);

	m_ctrlPlay->SetFont(&m_font,TRUE);
	m_ctrlStop->SetFont(&m_font,TRUE);
	m_ctrlBoard->SetFont(&m_font,TRUE);
	//m_ctrlStatus->SetFont(&m_font,TRUE);


	m_ctrlPlay->ShowWindow(SW_SHOW);
	m_ctrlStop->ShowWindow(SW_SHOW);
	m_ctrlBoard->ShowWindow(SW_SHOW);


	//-- SET MAIN WINDOWS HANDLE
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();		
	m_ctrlList.Init(this);
	
	Invalidate();
	return TRUE;
}



//-- 2013-12-13 kjpark@esmlab.com
//-- Network Program connect Check
void ConvterToolBarShow(CESMMovieListDlg* pView, BOOL isShow)
{
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_TOOLBAR_SHOW;
	pMsg->pDest		= isShow;
	pMsg->nParam3	= ESM_NETWORK_4DP;
	::SendMessage(pView->GetCESMMovieListDlgHandle(),WM_ESM, (WPARAM)WM_ESM_NET,(LPARAM)pMsg);
}

/////////////////////////////////////////////////////////////////////////////
// CESMMovieListDlg

void CESMMovieListDlg::OnPaint()
{	
	CDialog::OnPaint();
}

void CESMMovieListDlg::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_MOVIELIST_OPT,
		ID_IMAGE_MOVIELIST_ADD	,	
		ID_IMAGE_MOVIELIST_DEL	,	
		ID_IMAGE_MOVIELIST_DEL_ALL		,	
	};

	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	
	m_wndToolBar.ShowWindow(SW_SHOW);
}


void CESMMovieListDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	//m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT+25;
	m_rcClientFrame.top	= 40+25;
	RepositionBars(0,0xFFFF,0);

	if(m_ctrlList)
		m_ctrlList.MoveWindow(m_rcClientFrame);
}

void CESMMovieListDlg::LoadInfo()
{
	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_MOVIE_LIST);

	//-- Load Config File
	CESMIni ini;	
	if(ini.SetIniFilename (strFile))
	{
		CString strMovieList, strMovieName,strMovieIndex,strMoviePath,strMovieTime;

		int nIndex = 0;

		while(1)
		{
			strMovieIndex.Format(_T("%d"),nIndex);

			strMovieList = ini.GetString(strMovieIndex,MOVIELIST);
			strMoviePath = ini.GetString(strMovieIndex,MOVIEPATH);
			strMovieTime = ini.GetString(strMovieIndex,MOVIEINFO);
			if(!strMovieList.GetLength())
				break;

			m_ctrlList.AddMovieList(strMovieList,strMoviePath,TRUE);
			nIndex++;
		}			

	}

	Invalidate(TRUE);
}

void CESMMovieListDlg::SaveInfo()
{
	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_MOVIE_LIST);
	
	//-- Check File Exist
	CESMFileOperation fo;
	if(!fo.IsFileExist(strFile))
	{
		CFile file;
		file.Open(strFile, CFile::modeCreate);
		file.Close();
	}

	CESMIni ini;
	if(ini.SetIniFilename (strFile))
	{
		ini.DeleteSection(INFO_MOVIE_LIST);
		CString strMovieList;
		CString strMovieName;
		CString strMovieIndex;
		CString strMovieTime;
		int nIndex = 0;
		int nAll = m_ctrlList.GetItemCount();
		for(int i = nAll-1 ; i >= 0 ; i--)
		{
			strMovieList.Format(_T("%s"),m_ctrlList.GetItemText(i,0));
			strMovieName.Format(_T("%s"), m_ctrlList.GetItemText(i,2));
			strMovieTime.Format(_T("%s"),m_ctrlList.GetItemText(i,1));
			strMovieIndex.Format(_T("%d"),i);
			//strMovieList.Format(_T("%s_%d"),INFO_MOVIE_LISTNUM,nIndex);
			//ini.WriteString	(strMovieIndex, strMovieName,strMovieList);
			ini.WriteString(strMovieIndex,MOVIELIST,strMovieList);
			ini.WriteString(strMovieIndex,MOVIEPATH,strMovieName);
			ini.WriteString(strMovieIndex,MOVIEINFO,strMovieTime);

			nIndex++;
		}			
	}
}

void CESMMovieListDlg::RemoveAllMsg()
{
}

void CESMMovieListDlg::OnAddFile(CString strFile,CString strPath,BOOL bFinish)
{
	m_ctrlList.AddMovieList(strFile,strPath,bFinish);
	SaveInfo();
	//SaveInfo();
}
void CESMMovieListDlg::OnBnClkPlayPause()
{
	//m_bPlayState -> 0: Play, 1: Pause
	if(!m_bPlayState)//Play
	{
		m_bPlayState = TRUE;
		m_ctrlPlay->SetWindowText(_T("▶"));

		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_AJA_PLAY_OPT;
		pMsg->nParam1 = AJA_PLAY;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	}
	else//Pause
	{
		m_bPlayState = FALSE;
		m_ctrlPlay->SetWindowText(_T("∥"));

		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_AJA_PLAY_OPT;
		pMsg->nParam1 = AJA_PAUSE;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	}	
}
void CESMMovieListDlg::OnBnClkStop()
{
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_ESM_AJA_PLAY_OPT;
	pMsg->nParam1 = AJA_STOP;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
}
void CESMMovieListDlg::OnBnClkCheck()
{
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = WM_ESM_AJA_RELOAD;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
}
void CESMMovieListDlg::BoardStatus(BOOL bStatus)
{
	if(bStatus)
		m_ctrlStatus->SetWindowText(_T("Success"));
	else
		m_ctrlStatus->SetWindowText(_T("Fail"));
}
void CESMMovieListDlg::DeleteINIFile()
{
	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_MOVIE_LIST);

	//-- Check File Exist
	CESMFileOperation fo;
	if(!fo.IsFileExist(strFile))
	{
		return;
	}
	fo.Delete(strFile);
}