////////////////////////////////////////////////////////////////////////////////
//
//	ESMMovieListCtrl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "opencv2\imgproc.hpp"
#include "opencv2\highgui.hpp"
#include "opencv2\core.hpp"
#include "opencv2\opencv.hpp"

using namespace cv;

class CESMMovieListDlg;

class CESMMovieListCtrl : public CListCtrl
{
public:
	CESMMovieListCtrl(void);
public:
	~CESMMovieListCtrl(void);
	void Clear();
	void Delete();
	void DeleteAll();
	void AddMovieList(CString strList,CString strPath,BOOL bFinish);
	void Init(CESMMovieListDlg* pParent);
	void Save(CESMMovieListDlg* pParent);
	void LoadInfo();
	
private:
	BOOL m_bBKColor;
	CESMMovieListDlg* m_pParent;
	
public:
	int m_nSelectedItem;

protected:
	CString strIP;
	CString strPort;
	CListCtrl m_IPList;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDestroy();
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);

public:
	void SetStatus(BOOL bStatus);
};
