////////////////////////////////////////////////////////////////////////////////
//
//	TGGraphicBase.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "TGGraphManager.h"

class CTGGraphicBase : public CScrollView
{
public: // serialization에서만 만들어집니다.
	CTGGraphicBase();
	DECLARE_DYNCREATE(CTGGraphicBase)
	virtual ~CTGGraphicBase();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:
	int m_nTotalByte;
	void ClearViewer();

	//-- 2010-3-22 hongsu.jung
	//-- Add Status Info
	void AddStatusInfo(int nID, CString strName);

//-- 2010-2-25 hongsu.jung
//-- 
protected:		
	CTGGraphManager	m_Graph;		//** 그래프 관리	
	virtual void CreateGraph() {};
// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnDraw(CDC* pDC) {pDC;};      // pass on pure virtual	
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);	
	//-- 2010-2-25 hongsu.jung	
	afx_msg LRESULT TGGraphMessage(UINT wparam, LONG lparam);
};

