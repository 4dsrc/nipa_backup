#pragma once
#include "afxext.h"
#include "afxcmn.h"


#include "ESMFunc.h"
#include "ESMIndex.h"
#include "ESMDefine.h"
#include <vector>
#include "afxwin.h"
// CESMFileMergeDlg 대화 상자입니다.



struct MovieFile
{
	CString path;
	CString FileName;
	int nNumber;
	CString FileTime;
};


class CESMFileMergeDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CESMFileMergeDlg)

public:
	CESMFileMergeDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMFileMergeDlg();
	
	//-- For Toolbar
	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CESMPointListDlg)
	enum { IDD = IDD_VIEW_FILEMERGE };
	//}}AFX_DATA


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CBitmapButton m_Btn_Img1;
	CBitmapButton m_Btn_Img2;
	CBitmapButton m_Btn_Img3;
	CBitmapButton m_Btn_Img4;
	CBitmapButton m_Btn_Img5;
	CBitmapButton m_Btn_Img6;
	CBitmapButton m_Btn_Img7;
	CBitmapButton m_Btn_Img8;
	CBitmapButton m_Btn_Img9;
	virtual BOOL OnInitDialog();

private:

public:
	vector<MovieFile> m_arrMovie;
	vector<MovieFile> m_arrFileMovie;
	vector<CString> m_arrSelectMovie;
	vector<MovieFile> m_arrMovieSub;
private:
	int btn1;
	int btn2;
	int btn3;
	int btn4;
	int btn5;
	int btn6;
	int btn7;
	int btn8;
	int btn9;
	// btn == 0 non file
	// btn == 1 be file
	// btn == 2 select file
public:
	void SetMoviePathArr(CString moviePath,CString strName,bool m_flag);
	void ClearMoviePath();
	void SetBtnImage(int nNumber);
	void OnUnSelectAll();
	BOOL ISFILE(CString szFile);
	void ChangeMovieSize(CString strMovieFile, CString strConcertPath);

	
	CListCtrl m_FileList;
	CListCtrl m_SelectList;
	afx_msg void OnBnClickedMergeBtn1();
	afx_msg void OnBnClickedMergeBtn2();
	afx_msg void OnBnClickedMergeBtn3();
	afx_msg void OnBnClickedMergeBtn4();
	afx_msg void OnBnClickedMergeBtn5();
	afx_msg void OnBnClickedMergeBtn6();
	afx_msg void OnBnClickedMergeBtn7();
	afx_msg void OnBnClickedMergeBtn8();
	afx_msg void OnBnClickedMergeBtn9();
	afx_msg void OnBnClickedMergeSelectClear();
	afx_msg void OnBnClickedMergeMerge();
	afx_msg void OnBnClickedMergeLoad();
};
