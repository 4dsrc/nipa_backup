/////////////////////////////////////////////////////////////////////////////
//
// TGEdit.cpp: implementation of the CTGIni class.
//
//
// Copyright (c) 2008 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2008-07-31
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "TG.h"
#include "TGEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define VK_C        67
#define VK_V        86
#define VK_X        88
#define VK_Z        90


/////////////////////////////////////////////////////////////////////////////
// CTGEdit

BEGIN_MESSAGE_MAP(CTGEdit, CEdit)
	//{{AFX_MSG_MAP(CTGEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CTGEdit methods

CTGEdit::CTGEdit()
{
}

CTGEdit::~CTGEdit()
{
}

BOOL CTGEdit::PreTranslateMessage(MSG* pMsg)
{
	// If edit control is visible in tree view control, sending a
    // WM_KEYDOWN message to the edit control will dismiss the edit
    // control.  When ENTER key was sent to the edit control, the parent
    // window of the tree view control is responsible for updating the
    // item's label in TVN_ENDLABELEDIT notification code.
    if ( pMsg->message == WM_KEYDOWN )
    {
        if( GetKeyState( VK_CONTROL ))
		{
            if( pMsg->wParam == VK_C )
            {
                Copy();
                return TRUE;
            }
            if(  pMsg->wParam == VK_V )
            {
                Paste();
                return TRUE;
            }
            if(  pMsg->wParam == VK_X )
            {
                Cut();
                return TRUE;
            }
            if(  pMsg->wParam == VK_Z )
            {
                Undo();
                return TRUE;
            }
        }
        if( pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE ||
            pMsg->wParam == VK_CONTROL || pMsg->wParam == VK_INSERT ||
            pMsg->wParam == VK_SHIFT )
        {
            SendMessage(WM_KEYDOWN, pMsg->wParam, pMsg->lParam);
            return TRUE;
        }
	}
	return CEdit::PreTranslateMessage(pMsg);
}
