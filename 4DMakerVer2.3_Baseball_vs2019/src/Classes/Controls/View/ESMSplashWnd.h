////////////////////////////////////////////////////////////////////////////////
//
//	ESMSplashWnd.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "resource.h"
/////////////////////////////////////////////////////////////////////////////
// CESMSplashWnd window

class CESMSplashWnd : public CWnd
{
protected:
	CWnd m_wndInvisibleParent;
	CBitmap m_bitmap;
	CSize m_sizeBitmap;
	CStringArray m_arrTextLines;
// Construction
public:

	CESMSplashWnd(
		CWnd * pWndParent = CWnd::GetDesktopWindow(),
		UINT nBitmapID = IDB_BITMAP_SPLASH
		);

// Attributes
public:
	CFont m_font;
	INT m_nMaxTextLines, m_nLineHeight;
	COLORREF m_clrText;
	CRect m_rcText;

// Operations
public:
	bool Create(
		CWnd * pWndParent,
		UINT nBitmapID
		);

	static bool RegisterESMSplashWndClass();

	void AddTextLine(
		LPCTSTR sText = NULL,
		bool bReplaceLast = false
		);
	void ReplaceLastLine(
		LPCTSTR sText = NULL
		);
	void ClearLines();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMSplashWnd)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CESMSplashWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CESMSplashWnd)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp);
	afx_msg UINT OnNcHitTest(CPoint point);
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define __SPLASH_WND_CLASS__ _T("__SPLASH_WND_CLASS__")