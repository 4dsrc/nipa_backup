////////////////////////////////////////////////////////////////////////////////
//
//	LocalPropertyGridCtrl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
class CLocalPropertyValue;

/////////////////////////////////////////////////////////////////////////////
// CLocalPropertyGridCtrl window

class CLocalPropertyGridCtrl : public CExtPPVW < CExtPropertyGridCtrl >
{
public:
	CLocalPropertyGridCtrl();
	virtual ~CLocalPropertyGridCtrl();

	CExtPropertyStore m_psGridProperties, m_psCombinedSelection, m_psFocusedRow;

	void Init();
	virtual void OnPgcStoreSelect( // added by Mr. Eyal Cohen
		CExtPropertyStore * pPS
		);
	virtual void OnPgcInputComplete(
		CExtPropertyGridWnd * pPGW,
		CExtPropertyItem * pPropertyItem
		);
	virtual void OnPgcResetValue(
		CExtPropertyGridWnd * pPGW,
		CExtPropertyValue * pPropertyValue
		);
	void HandleValueEditingCompleteAny(
		CExtPropertyGridWnd * pPGW,
		CExtPropertyValue * pPropertyValue
		);
	void HandleValueEditingComplete(
		CExtPropertyGridWnd * pPGW,
		CLocalPropertyValue * pPropertyValueL
		);

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLocalPropertyGridCtrl)
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CLocalPropertyGridCtrl)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
