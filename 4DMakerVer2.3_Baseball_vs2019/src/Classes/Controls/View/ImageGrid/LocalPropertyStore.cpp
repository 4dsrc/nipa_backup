////////////////////////////////////////////////////////////////////////////////
//
//	LocalPropertyStore.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LocalPropertyStore.h"

/////////////////////////////////////////////////////////////////////////////
// CLocalPropertyStore

IMPLEMENT_SERIAL( CLocalPropertyStore, CExtPropertyStore, VERSIONABLE_SCHEMA|1 );

CLocalPropertyStore::CLocalPropertyStore(
	__EXT_MFC_SAFE_LPCTSTR strName // = NULL
	)
	: CExtPropertyStore( strName )
	, m_nRowNo( 0L )
{
}

CLocalPropertyStore::~CLocalPropertyStore()
{
}

#ifdef _DEBUG

void CLocalPropertyStore::AssertValid() const
{
	CExtPropertyStore::AssertValid();
}

void CLocalPropertyStore::Dump( CDumpContext & dc ) const
{
	CExtPropertyStore::Dump( dc );
}

#endif
