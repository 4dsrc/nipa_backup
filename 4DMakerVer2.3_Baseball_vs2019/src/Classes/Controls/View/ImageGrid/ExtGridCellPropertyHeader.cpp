////////////////////////////////////////////////////////////////////////////////
//
//	ExtGridCellPropertyHeader.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ExtGridCellPropertyHeader.h"
#include "LocalPropertyValue.h"

IMPLEMENT_SERIAL( CExtGridCellPropertyHeader, CExtGridCellHeader, VERSIONABLE_SCHEMA|1 );

/////////////////////////////////////////////////////////////////////////////
// CExtGridCellPropertyHeader

CExtGridCellPropertyHeader::CExtGridCellPropertyHeader(
	CExtGridDataProvider * pDataProvider // = NULL
	)
	: CExtGridCellHeader( pDataProvider )
{
}

CExtGridCellPropertyHeader::~CExtGridCellPropertyHeader()
{
}

void CExtGridCellPropertyHeader::ScanProperties( CExtGridWnd & wndGrid, LONG nRowNo )
{
	ASSERT_VALID( this );
	ASSERT( 0 <= nRowNo && nRowNo < wndGrid.RowCountGet() );
	m_psRow.ItemRemove();
	LONG nColNo, nColCount = wndGrid.ColumnCountGet();
	for( nColNo = 0; nColNo < nColCount; nColNo ++ )
	{
		CExtGridCell * pHdr0 = wndGrid.GridCellGetOuterAtTop( nColNo, 0L );
		ASSERT_VALID( pHdr0 );
		CExtSafeString strCategoryName;
		pHdr0->TextGet( strCategoryName );
		CExtGridCell * pHdr1 = wndGrid.GridCellGetOuterAtTop( nColNo, 1L );
		ASSERT_VALID( pHdr1 );
		CExtSafeString strValueName;
		pHdr1->TextGet( strValueName );
		CExtGridCell * pDataCell = wndGrid.GridCellGet( nColNo, nRowNo );
		ASSERT_VALID( pDataCell );
		CExtPropertyCategory * pCat = NULL;
		CExtPropertyItem * pItem = m_psRow.ItemGetByName( LPCTSTR(strCategoryName) );
		if( pItem != NULL )
			pCat = STATIC_DOWNCAST( CExtPropertyCategory, pItem );
		else
		{
			pCat = new CExtPropertyCategory( LPCTSTR(strCategoryName) );
			m_psRow.ItemInsert( pCat );
		}
		CLocalPropertyValue * pVal = new CLocalPropertyValue( LPCTSTR(strValueName), pDataCell );
		pVal->m_nColNo = nColNo;
		CExtGridCellPictureBase * pPictureCell = DYNAMIC_DOWNCAST( CExtGridCellPictureBase, pDataCell );
		if( pPictureCell != NULL )
			pVal->HeightPxSet( pDataCell->MeasureCell( &wndGrid ).cy + 6 );
		pCat->ItemInsert( pVal );
	} // for( nColNo = 0; nColNo < nColCount; nColNo ++ )
}
