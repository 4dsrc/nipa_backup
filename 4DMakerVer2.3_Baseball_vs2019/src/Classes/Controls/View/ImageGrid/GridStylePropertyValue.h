////////////////////////////////////////////////////////////////////////////////
//
//	GridStylePropertyValue.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once


/////////////////////////////////////////////////////////////////////////////
// CGridStylePropertyValue

class CGridStylePropertyValue : public CExtPropertyValueSingleCell
{
public:
	DECLARE_SERIAL( CGridStylePropertyValue );
	enum e_style_type
	{
		est_siw    = 0,
		est_siw_ex = 1,
		est_bse    = 2,
		est_bse_ex = 3,
	};
	CExtGridWnd * m_pWndGrid;
	e_style_type m_est;
	DWORD m_dwStyleMask;
	CGridStylePropertyValue(
		__EXT_MFC_SAFE_LPCTSTR strName = NULL,
		CExtGridWnd * pWndGrid = NULL,
		e_style_type _est = est_siw,
		DWORD dwStyleMask = 0,
		CExtGridCell * pValue = NULL,
		bool bClone = false
		);
	virtual ~CGridStylePropertyValue();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump( CDumpContext & dc ) const;
#endif
}; // class CGridStylePropertyValue
