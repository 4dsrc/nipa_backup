////////////////////////////////////////////////////////////////////////////////
//
//	ExtGridCellPropertyHeader.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "LocalPropertyStore.h"

/////////////////////////////////////////////////////////////////////////////
// CExtGridCellPropertyHeader

class CExtGridCellPropertyHeader : public CExtGridCellHeader
{
public:
	CLocalPropertyStore m_psRow;
	DECLARE_SERIAL( CExtGridCellPropertyHeader );
	IMPLEMENT_ExtGridCell_Clone( CExtGridCellPropertyHeader, CExtGridCellHeader );
	CExtGridCellPropertyHeader(
		CExtGridDataProvider * pDataProvider = NULL
		);
	virtual ~CExtGridCellPropertyHeader();
	void ScanProperties( CExtGridWnd & wndGrid, LONG nRowNo );
}; // class CExtGridCellPropertyHeader

