////////////////////////////////////////////////////////////////////////////////
//
//	LocalPropertyValue.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

/////////////////////////////////////////////////////////////////////////////
// CLocalPropertyValue

class CLocalPropertyValue : public CExtPropertyValueSingleCell
{
public:
	DECLARE_SERIAL( CLocalPropertyValue );
	LONG m_nColNo;
	CLocalPropertyValue(
		__EXT_MFC_SAFE_LPCTSTR strName = NULL,
		CExtGridCell * pValue = NULL,
		bool bClone = false
		);
	virtual ~CLocalPropertyValue();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump( CDumpContext & dc ) const;
#endif
}; // class CLocalPropertyValue
