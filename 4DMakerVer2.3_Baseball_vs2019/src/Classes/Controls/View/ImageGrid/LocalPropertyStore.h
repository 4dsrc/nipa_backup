////////////////////////////////////////////////////////////////////////////////
//
//	LocalPropertyStore.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

/////////////////////////////////////////////////////////////////////////////
// CLocalPropertyStore

class CLocalPropertyStore : public CExtPropertyStore
{
public:
	DECLARE_SERIAL( CLocalPropertyStore );
	LONG m_nRowNo;
	CLocalPropertyStore(
		__EXT_MFC_SAFE_LPCTSTR strName = NULL
		);
	virtual ~CLocalPropertyStore();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump( CDumpContext & dc ) const;
#endif
}; // class CLocalPropertyStore
