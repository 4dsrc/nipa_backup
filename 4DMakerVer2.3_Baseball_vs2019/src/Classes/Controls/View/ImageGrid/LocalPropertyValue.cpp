////////////////////////////////////////////////////////////////////////////////
//
//	LocalPropertyValue.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LocalPropertyValue.h"


/////////////////////////////////////////////////////////////////////////////
// CLocalPropertyValue

IMPLEMENT_SERIAL( CLocalPropertyValue, CExtPropertyValueSingleCell, VERSIONABLE_SCHEMA|1 );

CLocalPropertyValue::CLocalPropertyValue(
	__EXT_MFC_SAFE_LPCTSTR strName, // = NULL
	CExtGridCell * pValue, // = NULL
	bool bClone // = false
	)
	: CExtPropertyValueSingleCell( strName, pValue, bClone )
	, m_nColNo( 0L )
{
}

CLocalPropertyValue::~CLocalPropertyValue()
{
	m_pValueActive = NULL; // do not delete it
}

#ifdef _DEBUG

void CLocalPropertyValue::AssertValid() const
{
	CExtPropertyValueSingleCell::AssertValid();
	ASSERT( m_pValueDefault == NULL );
}

void CLocalPropertyValue::Dump( CDumpContext & dc ) const
{
	CExtPropertyValueSingleCell::Dump( dc );
}

#endif
