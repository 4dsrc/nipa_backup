////////////////////////////////////////////////////////////////////////////////
//
//	LocalPropertyGridCtrl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MainFrm.h"
#include "GridStylePropertyValue.h"

#include "LocalPropertyGridCtrl.h"
#include "LocalPropertyValue.h"
#include "LocalPropertyStore.h"



/////////////////////////////////////////////////////////////////////////////
// CLocalPropertyGridCtrl window

CLocalPropertyGridCtrl::CLocalPropertyGridCtrl()
	: m_psGridProperties( _T("Grid Properties") )
	, m_psCombinedSelection( _T("Selected Rows") )
	, m_psFocusedRow( _T("Focused Row") )
{
	m_bCreateTipBar = false;
	m_bEnableResetCommand = false;
}

CLocalPropertyGridCtrl::~CLocalPropertyGridCtrl()
{
}

BEGIN_MESSAGE_MAP( CLocalPropertyGridCtrl, CExtPropertyGridCtrl )
	//{{AFX_MSG_MAP(CLocalPropertyGridCtrl)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CLocalPropertyGridCtrl::Init()
{
	ASSERT_VALID( this );

	CMainFrame * pMainFrame = STATIC_DOWNCAST( CMainFrame, ::AfxGetMainWnd() );
	CExtPropertyGridComboBoxBar * pComboBox =
		STATIC_DOWNCAST( CExtPropertyGridComboBoxBar, GetChildByRTC( RUNTIME_CLASS(CExtPropertyGridComboBoxBar) ) );
	ASSERT( pComboBox->GetCount() == 0 );

	CExtPropertyCategory * pCat2 = NULL, * pCat = new CExtPropertyCategory( _T("Cell join") );
	m_psGridProperties.ItemInsert( pCat );
					CExtPropertyValue * pVal = new CExtPropertyValueSingleCell( __PROP_VAL_NAME_ENABLE_JOINED_CELL_AREAS );
									CExtGridCellCheckBox * pCellCheckBox = (CExtGridCellCheckBox*)pVal->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellCheckBox) );
									pCellCheckBox->SetCheck( pMainFrame->m_wndGrid.m_bEnableCellJoins ? 1 : 0 );
									pCellCheckBox->ModifyStyle( __EGCS_ICA_HORZ_CENTER );
									pCat->ItemInsert( pVal );

	pCat = new CExtPropertyCategory( _T("Basic") );
	m_psGridProperties.ItemInsert( pCat );
			pCat2 = new CExtPropertyCategory( _T("Scroll bars") );
			pCat->ItemInsert( pCat2 );
// __ESIS_STH_NONE __ESIS_STH_PIXEL __ESIS_STH_ITEM
// __ESIS_STV_NONE __ESIS_STV_PIXEL __ESIS_STV_ITEM
					pVal = new CGridStylePropertyValue( _T("Static horizontal thumb tracking"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __ESIS_DISABLE_THUMBTRACK_H );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Static vertical thumb tracking"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __ESIS_DISABLE_THUMBTRACK_V );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Auto-hide horizontal scroll bar"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __ESIS_DISABLE_AUTOHIDE_SB_H );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Auto-hide vertical scroll bar"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __ESIS_DISABLE_AUTOHIDE_SB_V );
									pCat2->ItemInsert( pVal );
			pCat2 = new CExtPropertyCategory( _T("Selection") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Select outer columns"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_SF_SELECT_OUTER_COLUMNS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Select outer rows"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_SF_SELECT_OUTER_ROWS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Use multi-area selection"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_MULTI_AREA_SELECTION );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Do not hide selection"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_NO_HIDE_SELECTION );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Use list-box-like extended selection"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_LBEXT_SELECTION );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Subtract selection areas"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_SUBTRACT_SEL_AREAS );
									pCat2->ItemInsert( pVal );
			pCat2 = new CExtPropertyCategory( _T("Sizing") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Resize outer cells horizontally"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_RESIZING_CELLS_OUTER_H );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Resize outer cells vertically"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_RESIZING_CELLS_OUTER_V );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Resize inner cells horizontally"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_RESIZING_CELLS_INNER_H );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Resize inner cells vertically"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_RESIZING_CELLS_INNER_V );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Use dynamic resizing horizontally"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_DYNAMIC_RESIZING_H );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Use dynamic resizing vertically"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_DYNAMIC_RESIZING_V );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Fixed-size columns"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_FIXED_SIZE_COLUMNS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Fixed-size rows"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_FIXED_SIZE_ROWS );
									pCat2->ItemInsert( pVal );
			pCat2 = new CExtPropertyCategory( _T("Common") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Perfer horizontal walk in painting and hit-testing algorithms"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __ESIS_PREFER_HORZ_WALK );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Focus bottom-right cell in selected range"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_AUTO_FOCUS_BOTTOM_RIGHT );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Draw horizontal grid lines"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_GRIDLINES_H );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Draw vertical grid lines"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw, __EGBS_GRIDLINES_V );
									pCat2->ItemInsert( pVal );

	pCat = new CExtPropertyCategory( _T("Extended") );
	m_psGridProperties.ItemInsert( pCat );
			pCat2 = new CExtPropertyCategory( _T("Common") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Use paint manager"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGWS_EX_PM_COLORS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Use Windows theme API"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGWS_EX_USE_THEME_API );
									pCat2->ItemInsert( pVal );
			pCat2 = new CExtPropertyCategory( _T("Hover events") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Enable for inner data cells"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_HVI_EVENT_CELLS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Enable for outer header cells"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_HVO_EVENT_CELLS );
									pCat2->ItemInsert( pVal );
			pCat2 = new CExtPropertyCategory( _T("Hover highlighting") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Highlight inner columns on hover"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_HVI_HIGHLIGHT_COLUMNS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Highlight outer columns on hover"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_HVO_HIGHLIGHT_COLUMNS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Highlight inner rows on hover"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_HVI_HIGHLIGHT_ROWS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Highlight outer rows on hover"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_HVO_HIGHLIGHT_ROWS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Highlight inner cells on hover"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_HVI_HIGHLIGHT_CELL );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Highlight outer cells on hover"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_HVO_HIGHLIGHT_CELL );
									pCat2->ItemInsert( pVal );
			pCat2 = new CExtPropertyCategory( _T("Selection highlighting") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Highlight inner columns"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_SI_HIGHLIGHT_COLUMNS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Highlight outer columns"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_SO_HIGHLIGHT_COLUMNS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Highlight inner rows"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_SI_HIGHLIGHT_ROWS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Highlight outer rows"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_SO_HIGHLIGHT_ROWS );
									pCat2->ItemInsert( pVal );
			pCat2 = new CExtPropertyCategory( _T("Focus highlighting") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Highlight inner columns"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_FI_HIGHLIGHT_COLUMNS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Highlight outer columns"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_FO_HIGHLIGHT_COLUMNS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Highlight inner rows"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_FI_HIGHLIGHT_ROWS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Highlight outer rows"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_FO_HIGHLIGHT_ROWS );
									pCat2->ItemInsert( pVal );
			pCat2 = new CExtPropertyCategory( _T("Cell tooltips") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Outer at left"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CELL_TOOLTIPS_OUTER_L );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Outer at right"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CELL_TOOLTIPS_OUTER_R );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Outer at top"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CELL_TOOLTIPS_OUTER_T );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Outer at bottom"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CELL_TOOLTIPS_OUTER_B );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Inner"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CELL_TOOLTIPS_INNER );
									pCat2->ItemInsert( pVal );
			pCat2 = new CExtPropertyCategory( _T("Cell expanding tips") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Outer at left"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CELL_EXPANDING_OUTER_L );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Outer at right"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CELL_EXPANDING_OUTER_R );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Outer at top"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CELL_EXPANDING_OUTER_T );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Outer at bottom"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CELL_EXPANDING_OUTER_B );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Inner"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CELL_EXPANDING_INNER );
									pCat2->ItemInsert( pVal );
//			pCat2 = new CExtPropertyCategory( _T("Border effects of outer corner areas in theme 2K") );
//			pCat->ItemInsert( pCat2 );
//					pVal = new CGridStylePropertyValue( _T("3D effect"), &pMainFrame->m_wndGrid,
//									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CORNER_AREAS_3D );
//									pCat2->ItemInsert( pVal );
//					pVal = new CGridStylePropertyValue( _T("Curve effect"), &pMainFrame->m_wndGrid,
//									CGridStylePropertyValue::est_siw_ex, __EGBS_EX_CORNER_AREAS_CURVE );
//									pCat2->ItemInsert( pVal );

	pCat = new CExtPropertyCategory( _T("Behavior") );
	m_psGridProperties.ItemInsert( pCat );
			pCat2 = new CExtPropertyCategory( _T("Editing") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Edit outer cells at top"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_EDIT_CELLS_OUTER_T );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Edit outer cells at bottom"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_EDIT_CELLS_OUTER_B );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Edit outer cells at left"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_EDIT_CELLS_OUTER_L );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Edit outer cells at right"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_EDIT_CELLS_OUTER_R );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Edit inner cells"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_EDIT_CELLS_INNER );
									pCat2->ItemInsert( pVal );

					pVal = new CGridStylePropertyValue( _T("Edit on LMB single click"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_EDIT_SINGLE_LCLICK );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Edit focused cell only"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_EDIT_SINGLE_FOCUSED_ONLY );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Edit on LMB double click"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_EDIT_DOUBLE_LCLICK );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Edit on ENTER key"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_EDIT_RETURN_CLICK );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Edit on F2 key"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_EDIT_F2_CLICK );
									pCat2->ItemInsert( pVal );

					pVal = new CGridStylePropertyValue( _T("Automatic editing"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_EDIT_AUTO );
									pCat2->ItemInsert( pVal );

			pCat2 = new CExtPropertyCategory( _T("Built-in cell buttons") );
			pCat->ItemInsert( pCat2 );
					pVal = new CGridStylePropertyValue( _T("Use persistent buttons"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_BUTTONS_PERSISTENT );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Show buttons in focused cell"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_BUTTONS_IN_FOCUSED_CELL );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Show buttons in focused column"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_BUTTONS_IN_FOCUSED_COLUMN );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Show buttons in focused row"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_BUTTONS_IN_FOCUSED_ROW );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Show buttons in hovered cells"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_BUTTONS_IN_HOVERED_CELL );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Show buttons in hovered column"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_BUTTONS_IN_HOVERED_COLUMN );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Show buttons in hovered row"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_BUTTONS_IN_HOVERED_ROW );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Show buttons in selected cells"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_BUTTONS_IN_SELECTED_CELLS );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Show buttons in selected column"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_BUTTONS_IN_SELECTED_COLUMN );
									pCat2->ItemInsert( pVal );
					pVal = new CGridStylePropertyValue( _T("Show buttons in selected row"), &pMainFrame->m_wndGrid,
									CGridStylePropertyValue::est_bse, __EGWS_BSE_BUTTONS_IN_SELECTED_ROW );
									pCat2->ItemInsert( pVal );
//__EGWS_BSE_WALK_HORZ
//__EGWS_BSE_WALK_VERT
//__EGWS_BSE_SORT_COLUMNS_ALLOW_MULTIPLE
//__EGWS_BSE_SORT_ROWS_ALLOW_MULTIPLE
//__EGWS_BSE_SORT_COLUMNS_T
//__EGWS_BSE_SORT_COLUMNS_B
//__EGWS_BSE_SORT_ROWS_L
//__EGWS_BSE_SORT_ROWS_R

	pComboBox->PropertyStoreInsert( &m_psGridProperties );
	pComboBox->PropertyStoreInsert( &m_psCombinedSelection );
	pComboBox->PropertyStoreInsert( &m_psFocusedRow );
	pComboBox->SetCurSel( 1 );
	pComboBox->SynchronizeCurSel();
CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
	OnPgcQueryGrids( arrGrids );
INT nGridIdx;
	for( nGridIdx = 0; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
	{
		CExtPropertyGridWnd * pPGW = arrGrids[ nGridIdx ];
		ASSERT_VALID( pPGW );
		pPGW->SetProportionalColumnWidth( 0.4 );
	}
}

void CLocalPropertyGridCtrl::OnPgcStoreSelect( // added by Mr. Eyal Cohen
	CExtPropertyStore * pPS
	)
{
	ASSERT_VALID( this );
	CExtPPVW < CExtPropertyGridCtrl > :: OnPgcStoreSelect( pPS );
	if( LPVOID(pPS) == LPVOID(&m_psGridProperties) )
		PropertyStoreSynchronize();
	else if( LPVOID(pPS) == LPVOID(&m_psCombinedSelection) )
	{
		CMainFrame * pMainFrame = STATIC_DOWNCAST( CMainFrame, ::AfxGetMainWnd() );
		ASSERT_VALID( pMainFrame );
		pMainFrame->m_wndGrid.OnGbwSelectionChanged();
	}
	else if( LPVOID(pPS) == LPVOID(&m_psFocusedRow) )
	{
		CMainFrame * pMainFrame = STATIC_DOWNCAST( CMainFrame, ::AfxGetMainWnd() );
		ASSERT_VALID( pMainFrame );
		CPoint ptOldFocus = pMainFrame->m_wndGrid.FocusGet();
		CPoint ptNewFocus = ptOldFocus;
		pMainFrame->m_wndGrid.OnGbwFocusChanged( ptOldFocus, ptNewFocus );
	}
}

void CLocalPropertyGridCtrl::OnPgcInputComplete(
	CExtPropertyGridWnd * pPGW,
	CExtPropertyItem * pPropertyItem
	)
{
	ASSERT_VALID( this );
	CExtPPVW < CExtPropertyGridCtrl > :: OnPgcInputComplete( pPGW, pPropertyItem );
CExtPropertyValue * pPropertyValue = STATIC_DOWNCAST( CExtPropertyValue, pPropertyItem );
	HandleValueEditingCompleteAny( pPGW, pPropertyValue );
}

void CLocalPropertyGridCtrl::OnPgcResetValue(
	CExtPropertyGridWnd * pPGW,
	CExtPropertyValue * pPropertyValue
	)
{
	ASSERT_VALID( this );
	CExtPPVW < CExtPropertyGridCtrl > :: OnPgcResetValue( pPGW, pPropertyValue );
	HandleValueEditingCompleteAny( pPGW, pPropertyValue );
}

void CLocalPropertyGridCtrl::HandleValueEditingCompleteAny(
	CExtPropertyGridWnd * pPGW,
	CExtPropertyValue * pPropertyValue
	)
{
	ASSERT_VALID( this );
	ASSERT_VALID( pPGW );
	ASSERT_VALID( pPropertyValue );
CMainFrame * pMainFrame = STATIC_DOWNCAST( CMainFrame, ::AfxGetMainWnd() );
	ASSERT_VALID( pMainFrame );
CLocalPropertyValue * pPropertyValueL = DYNAMIC_DOWNCAST( CLocalPropertyValue, pPropertyValue );
	if( pPropertyValueL != NULL )
	{
		HandleValueEditingComplete( pPGW, pPropertyValueL );
		return;
	}
CExtPropertyValueMixed * pPropertyValueMixed = DYNAMIC_DOWNCAST( CExtPropertyValueMixed, pPropertyValue );
	if( pPropertyValueMixed != NULL )
	{
		INT nItemIndex, nItemCount = pPropertyValueMixed->ItemGetCount();
		for( nItemIndex = 0; nItemIndex < nItemCount; nItemIndex++ )
		{
			CExtPropertyItem * pPropertyItem = pPropertyValueMixed->ItemGetAt( nItemIndex );
			ASSERT_VALID( pPropertyItem );
			CLocalPropertyValue * pPropertyValueL = DYNAMIC_DOWNCAST( CLocalPropertyValue, pPropertyItem );
			if( pPropertyValueL != NULL )
				HandleValueEditingComplete( pPGW, pPropertyValueL );
		}
		return;
	}
	if( _tcscmp( pPropertyValue->NameGet(), __PROP_VAL_NAME_ENABLE_JOINED_CELL_AREAS ) == 0 )
	{
		pMainFrame->m_wndGrid.m_bEnableCellJoins = ((CExtGridCellCheckBox *)pPropertyValue->ValueActiveGet())->GetCheck() ? true : false;
		pMainFrame->m_wndGrid.OnSwInvalidate( true );
		return;
	}
CGridStylePropertyValue * pGridStylePropertyValue = DYNAMIC_DOWNCAST( CGridStylePropertyValue, pPropertyValue );
	if( pGridStylePropertyValue != NULL )
	{
		CExtGridCellCheckBox * pCellCheckBox = DYNAMIC_DOWNCAST( CExtGridCellCheckBox, pGridStylePropertyValue->ValueActiveGet() );
		if( pCellCheckBox != NULL )
		{
			DWORD dwStyle = pCellCheckBox->GetCheck() ? pGridStylePropertyValue->m_dwStyleMask : 0;
			switch( pGridStylePropertyValue->m_est )
			{
			case CGridStylePropertyValue::est_siw:
				pMainFrame->m_wndGrid.SiwModifyStyle( 0, pGridStylePropertyValue->m_dwStyleMask, false );
				if( dwStyle != NULL )
					pMainFrame->m_wndGrid.SiwModifyStyle( dwStyle, 0, false );
			break;
			case CGridStylePropertyValue::est_siw_ex:
				pMainFrame->m_wndGrid.SiwModifyStyleEx( 0, pGridStylePropertyValue->m_dwStyleMask, false );
				if( dwStyle != NULL )
					pMainFrame->m_wndGrid.SiwModifyStyleEx( dwStyle, 0, false );
			break;
			case CGridStylePropertyValue::est_bse:
				pMainFrame->m_wndGrid.BseModifyStyle( 0, pGridStylePropertyValue->m_dwStyleMask, false );
				if( dwStyle != NULL )
					pMainFrame->m_wndGrid.BseModifyStyle( dwStyle, 0, false );
			break;
			case CGridStylePropertyValue::est_bse_ex:
				pMainFrame->m_wndGrid.BseModifyStyleEx( 0, pGridStylePropertyValue->m_dwStyleMask, false );
				if( dwStyle != NULL )
					pMainFrame->m_wndGrid.BseModifyStyleEx( dwStyle, 0, false );
			break;
			}
			pMainFrame->m_wndGrid.OnSwInvalidate( true );
		}
		return;
	}
}

void CLocalPropertyGridCtrl::HandleValueEditingComplete(
	CExtPropertyGridWnd * pPGW,
	CLocalPropertyValue * pPropertyValueL
	)
{
	ASSERT_VALID( this );
	LONG nColNo =  pPropertyValueL->m_nColNo;
	CLocalPropertyStore * pPS = DYNAMIC_DOWNCAST( CLocalPropertyStore, pPropertyValueL->GetStore() );
	LONG nRowNo =  pPS->m_nRowNo;
	CMainFrame * pMainFrame = STATIC_DOWNCAST( CMainFrame, ::AfxGetMainWnd() );
	ASSERT_VALID( pMainFrame );
	pMainFrame->m_wndGrid.HandleValueEditingComplete( nColNo, nRowNo, pPGW, pPropertyValueL );
}

