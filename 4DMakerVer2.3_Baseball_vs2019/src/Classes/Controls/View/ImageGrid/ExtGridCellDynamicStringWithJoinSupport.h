////////////////////////////////////////////////////////////////////////////////
//
//	ExtGridCellDynamicStringWithJoinSupport.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

/////////////////////////////////////////////////////////////////////////////
// CExtGridCellDynamicStringWithJoinSupport
class CExtGridCellDynamicStringWithJoinSupport : public CExtGCJ < CExtGridCellStringDM >
{
public:
	DECLARE_SERIAL( CExtGridCellDynamicStringWithJoinSupport );
	IMPLEMENT_ExtGridCell_Clone( CExtGridCellDynamicStringWithJoinSupport, CExtGCJ < CExtGridCellStringDM > );
	CExtGridCellDynamicStringWithJoinSupport(
		CExtGridDataProvider * pDataProvider = NULL
		);
}; // class CExtGridCellDynamicStringWithJoinSupport
