////////////////////////////////////////////////////////////////////////////////
//
//	GridStylePropertyValue.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GridStylePropertyValue.h"

/////////////////////////////////////////////////////////////////////////////
// CGridStylePropertyValue

IMPLEMENT_SERIAL( CGridStylePropertyValue, CExtPropertyValueSingleCell, VERSIONABLE_SCHEMA|1 );

CGridStylePropertyValue::CGridStylePropertyValue(
	__EXT_MFC_SAFE_LPCTSTR strName, // = NULL
	CExtGridWnd * pWndGrid, // = NULL
	CGridStylePropertyValue::e_style_type _est, // = est_siw
	DWORD dwStyleMask, // = 0,
	CExtGridCell * pValue, // = NULL
	bool bClone // = false
	)
	: CExtPropertyValueSingleCell( strName, pValue, bClone )
	, m_est( _est )
	, m_dwStyleMask( dwStyleMask )
	, m_pWndGrid( pWndGrid )
{
	if( pValue == NULL && m_dwStyleMask != 0 && m_pWndGrid != NULL )
	{
		ASSERT_VALID( m_pWndGrid );
		CExtGridCellCheckBox * pCellCheckBox = (CExtGridCellCheckBox *)ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellCheckBox) );
		pCellCheckBox->ModifyStyle( __EGCS_ICA_HORZ_CENTER );
		DWORD dwStyle = 0;
		switch( m_est )
		{
		case est_siw:
			dwStyle = m_pWndGrid->SiwGetStyle();
		break;
		case est_siw_ex:
			dwStyle = m_pWndGrid->SiwGetStyleEx();
		break;
		case est_bse:
			dwStyle = m_pWndGrid->BseGetStyle();
		break;
		case est_bse_ex:
			dwStyle = m_pWndGrid->BseGetStyleEx();
		break;
		}
		bool bVal = ( ( dwStyle & dwStyleMask ) != 0 ) ? true : false;
		pCellCheckBox->SetCheck( bVal ? 1 : 0 );
	}
}

CGridStylePropertyValue::~CGridStylePropertyValue()
{
	//m_pValueActive = NULL; // do not delete it
}

#ifdef _DEBUG

void CGridStylePropertyValue::AssertValid() const
{
	CExtPropertyValueSingleCell::AssertValid();
	ASSERT( m_pValueDefault == NULL );
}

void CGridStylePropertyValue::Dump( CDumpContext & dc ) const
{
	CExtPropertyValueSingleCell::Dump( dc );
}

#endif
