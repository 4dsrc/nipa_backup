////////////////////////////////////////////////////////////////////////////////
//
//	TGFramerateGraph.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "GlobalIndex.h"
#include "TGFramerateGraph.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define STRING_FRAME_CNT						_T("Target Frame")
#define STRING_FRAME_BASE						_T("Frame Base")
#define STRING_TARGET_DEV						_T("File Timestamp(ms)")
#define STRING_PC_DEV							_T("Receive Time (ms)")

// CTGFramerateGraph 생성/소멸
CTGFramerateGraph::CTGFramerateGraph()
{	
}

CTGFramerateGraph::~CTGFramerateGraph()
{	
}

//-- 2010-3-9 hongsu.jung
//-- CreateGraph
void CTGFramerateGraph::CreateGraph()
{
	m_Graph.SetMessageID(WM_GRAPH_MSG);	
	m_Graph.SetXDataType(XDataTYPE_DECIMAL);
	m_Graph.SetINIDirectory(TGGetPath(TG_PATH_HOME_CONFIG));
	m_Graph.SetMaxScrollPage(4000);
	m_Graph.SetDefaultScrollPage(2000);
	//-- 2010-2-25 hongsu.jung	
	//m_Graph.SetPopUpMenu(UNIGRAPH_POPUP_ALL);	
	m_Graph.SetEventValue(UNIGRAPH_LBUTTONDBCLK);
	//-- 2010-8-10 hongsu.jung
	//m_Graph.SetPopUpMenu(UNIGRAPH_POPUP_USAGE);	
}

void CTGFramerateGraph::AddGraph(CString strCnt, CString strDeviation)
{
	int nMask = 
		UNIGRAPH_DISPLAY_TITLE |	//** Show Title
		UNIGRAPH_DISPLAY_XTITLE | 	//** Show XTitle
		UNIGRAPH_DISPLAY_YTITLE |	//** Show YTitle
		UNIGRAPH_DISPLAY_XGRID |	//** Show XGrid
		UNIGRAPH_DISPLAY_YGRID |	//** Show YGrid
		UNIGRAPH_WND_VERT |		
		UNIGRAPH_STATUS |			//** 그래프 데이타 이름을 보여줄것인가.,
		UNIGRAPH_SELETION_VALUE |	//** 마우스 포이트 위치 값을 보여줄것인가.,
		//UNIGRAPH_THRESHOLD |		//** 임계치 정보를 보여줄것인가..,
		//UNIGRAPH_SCALE |			//** 스케일 윈도우를 생성할것인가...,
		//UNIGRAPH_SAVEDATA |		//** 그래프 데이터를 파일로 저장할 것인가..,
		UNIGRAPH_POPUPMENU;			//** 팝업메뉴를 생성할 것인가..,

	GRAPH_LINE_STYLE LineStyle;
	LineStyle.dLimit = -1;
	LineStyle.nStyle = LINE_STYLE_LINE;
	LineStyle.nWidth = LINE_DEFAULT_WIDTH;
	LineStyle.nShow  = LINE_TYPE_SHOW;
	
	//-- Add 1st Graph
	m_Graph.AddGraph(nMask, strCnt, this, GRAPH_MONMULTI_WINID);
	m_Graph.SetYTickAttr(strCnt,"Frame");
	LineStyle.strName = STRING_FRAME_BASE;
	m_Graph.SetGraphStyle(strCnt, 0, LineStyle);	
	LineStyle.strName = STRING_FRAME_CNT;
	m_Graph.SetGraphStyle(strCnt, 1, LineStyle);


	//-- Add 2nd Graph
	//-- 2012-08-20 hongsu@esmlab.com
	//-- Change To 3D 
	LineStyle.nStyle = LINE_STYLE_3DBAR;//LINE_STYLE_BAR;	
	m_Graph.AddGraph(nMask, strDeviation, this, GRAPH_MONMULTI_WINID+1);
	m_Graph.SetYTickAttr(strDeviation,"Deviation (ms)");
	LineStyle.strName = STRING_TARGET_DEV;
	m_Graph.SetGraphStyle(strDeviation, 0, LineStyle);
	LineStyle.strName = STRING_PC_DEV;
	m_Graph.SetGraphStyle(strDeviation, 1, LineStyle);
}

void CTGFramerateGraph::DeleteGraph(CString strName)
{
	m_Graph.DeleteGraph(strName);
}

//-- 2012-04-19 hongsu
void CTGFramerateGraph::DeleteGraphAll()
{
	m_Graph.DeleteGraphAll();
}


/*
void CTGFramerateGraph::AddData(CString strName, SYSTEMTIME time, double dFrame)
{
	CString strTime;
	strTime.Format(_T("%04d%02d%02d%02d%02d%02d"), time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);
	TRACE(_T("Add Framerate [%s][%s][%f]\n"), strName, strTime, dFrame);
	m_Graph.AddLineData(strName, 0, strTime, dFrame);	
}
*/

void CTGFramerateGraph::AddData(CString strName, int nGap, double dTargetDev, double dPCDev)
{
	CString strTime;
	int nSec = nGap/1000;
	nGap = nGap%1000;
	strTime.Format(_T("%02d.%03d"),nSec, nGap);

	TRACE(_T("Add Frame [%s][%s] [%f][%f]\n"), strName, strTime, dTargetDev, dPCDev);
	m_Graph.AddLineData(strName, 0, strTime, dTargetDev);
	m_Graph.AddLineData(strName, 1, strTime, dPCDev);
}

void CTGFramerateGraph::AddDeviation(CString strName, int nGap, double dBaseFrame, double dFrame)
{
	CString strTime;
	int nSec = nGap/1000;
	nGap = nGap%1000;
	strTime.Format(_T("%02d.%03d"),nSec, nGap);

	TRACE(_T("Add Frame [%s][%s] [%f]\n"), strName, strTime, dFrame);
	m_Graph.AddLineData(strName, 0, strTime, dBaseFrame);
	m_Graph.AddLineData(strName, 1, strTime, dFrame);
}