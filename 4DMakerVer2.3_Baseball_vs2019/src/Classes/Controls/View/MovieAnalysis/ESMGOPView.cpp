////////////////////////////////////////////////////////////////////////////////
//
//	TGGOPView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TGGOPView.h"
#include "resource.h"
#include "TestStepDef.h"
#include "XnsMediaInterface.h"

CTGGOPView::CTGGOPView(CWnd* pParent /*=NULL*/)
: CDialog(CTGGOPView::IDD, pParent)	
{	
	m_pTGGOPGraph		= NULL;	
	m_nGOPCount			= 0;
	m_tTime				= 0;
	m_bStart			= FALSE;
	m_bChkResultData	= FALSE;
	m_nGOPFailCount		= 0;
}

CTGGOPView::~CTGGOPView() 
{	
}

void CTGGOPView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTGGOPView)
	//}}AFX_DATA_MAP	
}


BEGIN_MESSAGE_MAP(CTGGOPView, CDialog)
	//{{AFX_MSG_MAP(CTGGOPView)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()	
	ON_WM_CLOSE()	
END_MESSAGE_MAP()


BOOL CTGGOPView::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}	

	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();
	m_pTGGOPGraph  = new CTGGOPGraph();
	m_pTGGOPGraph->Create(NULL, _T(""), WS_VISIBLE|WS_CHILD, CRect(0,0,0,0), this, ID_VIEW_BAR_GOP);
	m_dwSequence = 0;

	return TRUE;
}

void CTGGOPView::OnClose()
{
	CDialog::OnClose();
}

void CTGGOPView::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	RepositionBars(0,0xFFFF,0);

	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;
	GetClientRect(m_rcClientFrame);
	//-- RESIZE CONTROL
	m_pTGGOPGraph->MoveWindow(m_rcClientFrame);
}

void CTGGOPView::Clear()
{
	m_nGOPCount = 0;
	m_bStart	= FALSE;

	m_pTGGOPGraph->ClearViewer();
}

void CTGGOPView::InitImageFrameWnd()
{
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);
}


BOOL CTGGOPView::InsertToDB(CString srtTime, int nFrame)
{
	/*
	CTGDBFrameAnalysis* pInfo = new CTGDBFrameAnalysis(m_dwSequence++, srtTime, nFrame);
	if(!TGSendMessageToDB(WM_TG_ODBC_INSERT_MOVIE_ANALYSIS, (PVOID)pInfo))
	{
		delete pInfo;
		pInfo = NULL;
	}
	*/
	return TRUE;
}

LONG CTGGOPView::InitGraph(CString strModel, CString strVer)
{
	Clear();

	m_strTitle.Format(_T("%s : %s"),strModel,strVer);
	m_pTGGOPGraph->DeleteGraphAll();
	m_pTGGOPGraph->AddGraph(m_strTitle,GRAPH_MONMULTI_WINID);

	//-- 2012-09-24 joonho.kim
	//-- Init Sequence
	m_dwSequence = 0;
	m_nSumCount = 0;

	//-- Init GOP Count
	TGSetValue(TG_VAL_MIN_GOPCOUNT, 30);

	return TG_ERR_NON;
}

LONG CTGGOPView::AddTime(FRAME_INFO* pFrame)
{
	LONG lResult;
	if(!pFrame)
		return TG_ERR_MSG;

	long nTimeStamp;
	long nFrameType;
	nTimeStamp	= pFrame->nTimeStamp;
	nFrameType	= pFrame->nFrameType;

	//-- 2012-07-31 hongsu@esmlab.com
	//-- GOP ( I,P,B Frame)
	if( AddGOP(nTimeStamp, nFrameType))
		lResult = TG_ERR_NON;
	else
		lResult = TG_ERR_GOP_COUNT;

	return lResult;
}

//-- 2012-07-31 hongsu@esmlab.com
//-- GOP ( I,P,B Frame)
BOOL CTGGOPView::AddGOP(long nTime, long nFrameType)
{
	BOOL bRet = TRUE;
	int nGOPCount = 0;
	if(nFrameType == XFRAME_IVOP)
	{
		if(!m_bStart)
		{
			m_tFirstTime = nTime;			
			m_bStart = TRUE;
			
		}
		else
		{
			m_nGOPCount++;

			TRACE(_T("Time[%d] GOP Count[%d]\n"),m_tTime, m_nGOPCount);
			//-- Add Line to Graph			
			m_pTGGOPGraph->AddData(m_strTitle, m_tTime-m_tFirstTime, m_nGOPCount);
			//-- 2012-09-07 joonho.kim
			m_bChkResultData = TRUE;
			m_nSumCount = m_nGOPCount;
			nGOPCount = TGGetValue( TG_VAL_GOPSIZE );
			if( nGOPCount != m_nGOPCount)
			{
				TGLog(0,_T("GOP Count Base[%d] Present[%d]"), nGOPCount, m_nGOPCount);
				m_nGOPFailCount++;
				bRet = FALSE;
			}

			int nGOPTemp = TGGetValue(TG_VAL_MIN_GOPCOUNT);
			if( nGOPTemp > m_nGOPCount )
				TGSetValue( TG_VAL_MIN_GOPCOUNT , m_nGOPCount );
		}	
		m_tTime	= nTime;
		m_nGOPCount = 0;
	}
	else
		m_nGOPCount++;

	return bRet;
}
