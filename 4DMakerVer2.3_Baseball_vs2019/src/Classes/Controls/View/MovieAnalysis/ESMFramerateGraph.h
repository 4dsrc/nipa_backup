////////////////////////////////////////////////////////////////////////////////
//
//	TGFramerateGraph.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "TGGraphicBase.h"

class CTGFramerateGraph : public CTGGraphicBase
{
public:
	CTGFramerateGraph();
	virtual ~CTGFramerateGraph();

protected:
	void CreateGraph();
	//-- 2010-3-28 hongsu.jung
	void AddArray(int nTickTime, int nTask);

	
public:
	//void AddData(CString strName, SYSTEMTIME time, double dFrame);
	void AddDeviation(CString strName, int nGap, double dTargetDev, double dPCDev);
	void AddData(CString strName, int nGap, double dBaseFrame, double dFrame);
	
	void AddGraph(CString strCnt, CString strDeviation);
	void DeleteGraph(CString strName);
	void DeleteGraphAll();		
};
