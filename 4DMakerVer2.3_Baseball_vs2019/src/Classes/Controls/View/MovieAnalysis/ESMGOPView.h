////////////////////////////////////////////////////////////////////////////////
//
//	TGGOPView.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////



#pragma once
#include "resource.h"
#include <list>
#include <vector>
#include "TGGOPGraph.h"
#include "TGGOPView.h"

using namespace std;
class CTGGOPView : public CDialog
{
	class CTimeCnt
	{
	public:
		CTimeCnt(long nTime, long tFrameTime) {m_nTime = nTime; m_tFrameTime = tFrameTime; m_nCnt = 1;}
		long m_nTime;
		long m_tFrameTime;
		int  m_nCnt;
	};

public:
	CTGGOPView(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTGGOPView();
	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CTGGOPView)
	enum { IDD = IDD_GRAPH_NON_TOOLBAR };
	//}}AFX_DATA


private:
	
	CString		m_strTitle;
	DWORD		m_dwSequence;

	int			m_nSumCount;
	int			m_nGOPCount;
	int			m_nGOPFailCount;
	long		m_tTime;
	long		m_tFirstTime;
	BOOL		m_bStart;
	BOOL		m_bChkResultData;

	CTGGOPGraph* m_pTGGOPGraph;
	CRect m_rcClientFrame;		
	
	HWND m_hMainWnd;
	BOOL InsertToDB(CString srtTime, int nFrame);		

	//-- 2012-07-31 hongsu@esmlab.com
	//-- GOP ( I,P,B Frame)
	BOOL AddGOP(long nTime, long nFrameType);

public:
	//-- 2012-09-11 joonho.kim
	void SetCheck(BOOL bChk)	{ m_bChkResultData	=	bChk; };
	BOOL GetCheck()				{ return m_bChkResultData; };
	int  GetGOPCount()			{ return m_nSumCount; };
	int	 GetGOPFailCount()		{ return m_nGOPFailCount; };

	void SetGOPFailCount(int nValue) { m_nGOPFailCount	= nValue; };

	LONG InitGraph(CString strModel, CString strVer);
	LONG AddTime(FRAME_INFO* pFrame);
	
	void Clear();
	void Close(){ return OnClose();}


protected:	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTGGOPView)
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	//}}AFX_VIRTUAL
	DECLARE_MESSAGE_MAP()

protected:
	// Generated message map functions
	//{{AFX_MSG(CTGGOPView)	
	//}}AFX_MSG		
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	afx_msg void OnClose();	

};