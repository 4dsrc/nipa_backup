////////////////////////////////////////////////////////////////////////////////
//
//	TGGOPGraph.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "TGGraphicBase.h"

class CTGGOPGraph : public CTGGraphicBase
{
public:
	CTGGOPGraph();
	virtual ~CTGGOPGraph();

protected:
	void CreateGraph();
	//-- 2010-3-28 hongsu.jung
	void AddArray(int nTickTime, int nTask);
	double m_nGOPBase;

	
public:
	void AddData(CString strName, long nTime, double nGOP);
	void AddGraph(CString strName, unsigned int unIndex);
	void DeleteGraph(CString strName);
	void DeleteGraphAll();		
};
