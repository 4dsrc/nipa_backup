////////////////////////////////////////////////////////////////////////////////
//
//	TGFramerateView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TGFramerateView.h"
#include "resource.h"
#include "TestStepDef.h"

CTGFramerateView::CTGFramerateView(CWnd* pParent /*=NULL*/)
: CDialog(CTGFramerateView::IDD, pParent)	
{	
	m_bChkResultData	= FALSE;
	m_pTGFramerateGraph	= NULL;	
	ResetDeviation();

	m_nFrameMaxDeviation		= 0;
	m_nFrameReverseFailCount	= 0;

}

CTGFramerateView::~CTGFramerateView() 
{	
}

void CTGFramerateView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTGFramerateView)
	//}}AFX_DATA_MAP	
}


BEGIN_MESSAGE_MAP(CTGFramerateView, CDialog)
	//{{AFX_MSG_MAP(CTGFramerateView)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()		
	ON_WM_CLOSE()	
END_MESSAGE_MAP()


BOOL CTGFramerateView::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}	

	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();
	m_pTGFramerateGraph  = new CTGFramerateGraph();
	m_pTGFramerateGraph->Create(NULL, _T(""), WS_VISIBLE|WS_CHILD, CRect(0,0,0,0), this, ID_VIEW_BAR_FRAME);
	m_dwSequence = 0;

	return TRUE;
}

void CTGFramerateView::OnClose()
{
	ResetDeviation();	
	CDialog::OnClose();
}

void CTGFramerateView::InitImageFrameWnd()
{
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	
}

void CTGFramerateView::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);
	//-- RESIZE CONTROL
	m_pTGFramerateGraph->MoveWindow(m_rcClientFrame);
}

void CTGFramerateView::Clear()
{
	m_pTGFramerateGraph->ClearViewer();
}

void CTGFramerateView::OnToolbarOpenOptionDlg()
{
	MessageBox(_T("Framerate Analysis Viewer Option Dialog")); 
	return;
	//-- 2012-07-16 hongsu@esmlab.com
	//-- CIC Check
	/*
	TGEvent* pMsg = new TGEvent ;
	pMsg->message = WM_TG_OPT_LIVEVIEW;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_TG, WM_TG_OPT, (LPARAM)pMsg);
	*/	
}


BOOL CTGFramerateView::InsertToDB(CString srtTime, int nFrameCount, int nFrameDeviation, int nGOPFailCount, int nFrameDeviationFailCount, int nPCDeviation)
{
	TGSetSequence( m_dwSequence );
	CTGDBFrameAnalysis* pInfo = new CTGDBFrameAnalysis(m_dwSequence++, srtTime, nFrameCount, nFrameDeviation, nGOPFailCount, nFrameDeviationFailCount ,nPCDeviation);
	if(!TGSendMessageToDB(WM_TG_ODBC_INSERT_MOVIE_ANALYSIS, (PVOID)pInfo))
	{
		delete pInfo;
		pInfo = NULL;
	}
	return TRUE;
}

LONG CTGFramerateView::InitGraph(int nFrame, int nGap, CString strModel, CString strVer)
{
	//-- 2012-07-31 hongsu@esmlab.com
	//-- Remove All 
	m_pTGFramerateGraph->DeleteGraphAll();

	m_strTitleFrameCnt.Format(_T("[Frame Count] %s : %s"),strModel,strVer);
	m_strTitleDeviation.Format(_T("[Frame Deviation] %s : %s"),strModel,strVer);
	m_pTGFramerateGraph->AddGraph(m_strTitleFrameCnt, m_strTitleDeviation);

	//-- Set Framerate
	m_nFramerate = nFrame;
	//-- 2012-09-10 joonho.kim
	m_nGap = nGap;

	//-- 2012-09-24 joonho.kim
	//-- Init Sequence
	m_dwSequence = 0;
	

	//-- Remove Graph
	ResetDeviation();
	return TG_ERR_NON;
}

void CTGFramerateView::ResetDeviation(BOOL bResetAll)
{
	if(bResetAll)
	{
		m_nFirstTime = 0;
		m_nPrevTime = 0;
		RemoveAllFrameTime();
	}
	else
	{
		CTGFrameTime* pLast = NULL;				
		//-- 2012-07-31 hongsu@esmlab.com
		//-- Remain Last Data
		pLast = GetFrameTime(GetFrameTimeSize()-1);
		
		if(pLast)
		{
			int nMovie = pLast->m_nTarget;
			int nPC	= pLast->m_nPC;

			RemoveAllFrameTime();
			AddFrameTime(nMovie, nPC);
		}			
	}
}

BOOL CTGFramerateView::IsChangeTime(long nMillisec)
{
	//-- 2012-07-31 hongsu@esmlab.com
	//-- First
	if(m_nPrevTime == 0)
	{
		m_bFailFramerate = FALSE;	
		m_nPrevTime = nMillisec;
		m_nFirstTime = nMillisec;
		return FALSE;
	}

	if(m_nPrevTime/1000 < nMillisec/1000)
		return TRUE;
	return FALSE;
}

LONG CTGFramerateView::AddTime(FRAME_INFO* pFrame)
{
	if(!pFrame)
		return TG_ERR_MSG;

	LONG ret = 0x00;
	long nTime;
	long tFrame;	
	long nMilliSec;
	//-- 2012-09-19 hongsu@esmlab.com
	//-- Add PC Stamp
	long nPCTimeStamp;

	nTime			= pFrame->nDeviceTime;
	tFrame			= pFrame->nRecordTime;	
	nMilliSec		= pFrame->nTimeStamp;
	nPCTimeStamp	= pFrame->nTimeStampPC;

	//-- 2012-07-31 hongsu@esmlab.com
	//-- Add Time 
	AddFrameTime(nMilliSec, nPCTimeStamp);

	if(IsChangeTime(nMilliSec))
	{
		m_nPrevTime = nMilliSec;
		//-- Add Graph
		SYSTEMTIME tTime = CTGTimeUtil::TickToSystemTime(tFrame);		
		int nTimeGap = m_nPrevTime - m_nFirstTime;
		//-- 2012-08-01 hongsu@esmlab.com
		//-- Remove First 1 second Data
		if(nTimeGap<1000)
		{
			TGLog(5,_T("Remove 1st second data"));
			ResetDeviation(FALSE);
			return ret;
		}

		//-- 2012-09-11 joonho.kim
		m_bChkResultData = TRUE;
		//-- Add Line to Graph
		if(!AddLineFrameCnt(nTimeGap, tTime))
			ret = TG_ERR_FRAME_CNT;
		//-- Deviation
		if(TGGetCategory()==TG_INF_CATEGORY_DVR && ((TGGetCategorySub()==TG_INF_CATEGORYSUB_BANDWIDTH) || (TGGetCategorySub() == TG_INF_CATEGORYSUB_LIVESTREAM)) 
			|| TGGetCategorySub().CompareNoCase(TG_INF_CATEGORYSUB_REBOOT)==0)
		{
			ret = TG_ERR_NON;
		}
		else
			ret = ret | AddLineFrameDeviation(nTimeGap, tTime);
		//-- 2012-07-31 hongsu@esmlab.com
		//-- RESET NON ALL
		ResetDeviation(FALSE);		
	}
	return ret;
}

//------------------------------------------------------------------------------
//! @brief		
//! @date		2012-07-31
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Frame Count
//! @revision		
//------------------------------------------------------------------------------

BOOL CTGFramerateView::AddLineFrameCnt(int nTimeGap, SYSTEMTIME time)
{
	long nCnt = GetFrameTimeSize() -1;	//-- Remove compare data	
	m_pTGFramerateGraph->AddData(m_strTitleFrameCnt, /*nTimeGap*/(m_dwSequence+1)*1000, m_nFramerate, nCnt);	

	//-- 2012-09-11 joonho.kim
	m_nFrameCount = nCnt;
	m_time = time;
	/*//-- Send To DB
	CString strTime;		
	strTime.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"), time.wYear, time.wMonth, time.wDay, 
		time.wHour, time.wMinute, time.wSecond);
	InsertToDB(strTime,nCnt);*/

	//-- 2012-07-31 hongsu@esmlab.com
	//-- Decide Pass. Fail
	return SetResultFrameCnt(nCnt);		
}

BOOL CTGFramerateView::SetResultFrameCnt(int nCnt)
{
	//-- Check Pass/Fail
	int nGap = abs(m_nFramerate-nCnt);
	
	//-- 2012-09-12 joonho.kim
	int nGapCount = m_nGap;

	if(TGGetCategory()==TG_INF_CATEGORY_DVR && ((TGGetCategorySub()==TG_INF_CATEGORYSUB_BANDWIDTH) || (TGGetCategorySub() == TG_INF_CATEGORYSUB_LIVESTREAM)) 
		|| TGGetCategorySub().CompareNoCase(TG_INF_CATEGORYSUB_REBOOT)==0)
	{
		return TRUE;
	}

	else if(nGap > nGapCount)
	{
		CString strCause;
		strCause.Format(_T("[FPS:Fail] Range (%d~%d) : Now%d / base:%d / tolerance:%d"), m_nFramerate-nGapCount, m_nFramerate+nGapCount, nCnt, m_nFramerate, nGapCount);
		TGSetOption(TG_VAL_CAUSE,strCause);
		TGLog(3,strCause);
		//-- Fail Get Count
		return FALSE;
	}
	return TRUE;
}
//------------------------------------------------------------------------------
//! @brief		
//! @date		2012-07-31
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Deviation
//! @revision		
//------------------------------------------------------------------------------
long CTGFramerateView::AddLineFrameDeviation(int nTimeGap, SYSTEMTIME time)
{
	//-- Get Array and Calculate Deviation
	int nAll = GetFrameTimeSize();
	long lResult = TG_ERR_NON;

	CTGFrameTime* pFrameCurrent = NULL;
	CTGFrameTime* pFramePrev	= NULL;

	int nBaseDeviation;

	int nTargetTimeCurrent;
	int nTargetTimePrev;
	int nTargetTimeDiff;
	int nTargetTimeGap;
	int nTargetTimeTotal = 0;
	int nTargetDeviation;

	int nPCTimeCurrent;
	int nPCTimePrev;
	int nPCTimeDiff;
	int nPCTimeGap;
	int nPCTimeTotal = 0;
	int nPCDeviation;

	
	//-- 2012-08-01 hongsu@esmlab.com
	//-- Get Expect Deviation 
	int nfps = TGGetValue(TG_VAL_FRAMERATE);
	if(!nfps)
	{
		TGLog(0,_T("Wrong Setting Framerate fps:[%d]"),nfps);
		return TG_ERR_FRAME_CNT;
	}

	nBaseDeviation = 1000/nfps;
	//nBaseDeviation = 1000/30;
	if(!nBaseDeviation)
	{
		TGLog(0,_T("Wrong Framerate Data [%d]"),TGGetValue(TG_VAL_FRAMERATE));
		return FALSE;
	}


	for(int i = 0 ; i < nAll-1 ; i ++)
	{

		pFramePrev			= GetFrameTime(i);
		pFrameCurrent		= GetFrameTime(i+1);
		if(!pFramePrev || !pFrameCurrent)
			break;

		nTargetTimePrev		= pFramePrev->m_nTarget;
		nTargetTimeCurrent	= pFrameCurrent->m_nTarget;
		nTargetTimeDiff		= nTargetTimeCurrent - nTargetTimePrev;

		nPCTimePrev			= pFramePrev->m_nPC;
		nPCTimeCurrent		= pFrameCurrent->m_nPC;
		nPCTimeDiff			= nPCTimeCurrent - nPCTimePrev;
		if(nPCTimeDiff < 0)
			nPCTimeDiff += 1000;

		//-- 2012-08-16 hongsu@esmlab.com
		//-- Check Reverse Frame
		if(nTargetTimeDiff < 0)
		{
			TGLog(0,_T("Detection Reverse Frame Previous Time[%d] Present Time[%d]"), nTargetTimePrev, nTargetTimeCurrent);
			lResult = TG_ERR_FRAME_REVERSE;

			TGSetPreTime( nTargetTimePrev );
			TGSetNextTime( nTargetTimeCurrent );

			m_nFrameReverseFailCount++;
		}

		//-- Compare Base
		nTargetTimeGap	= (int)nTargetTimeDiff - (int)nBaseDeviation;
		//-- square 
		nTargetTimeTotal	+= nTargetTimeGap * nTargetTimeGap;


		//-- Compare Base
		nPCTimeGap	= (int)nPCTimeDiff  - (int)nBaseDeviation;
		//-- square 
		nPCTimeTotal	+= nPCTimeGap * nPCTimeGap;


		//TRACE("Time[%d:%d] Expect[%d] Gap [%d] => Gap2 [%d / Total[%d]]\n", nPrevTime, nTime, nBaseDeviation, nGap, nGap*nGap, nTotal);
		TGLog(5,_T("Expect[%03d] Target[%09d|%09d %03d(%03d)]\tPC[%03d|%03d %03d(%03d)] "), nBaseDeviation, 
									nTargetTimePrev	,nTargetTimeCurrent	,nTargetTimeDiff, nTargetTimeGap	,
									nPCTimePrev		,nPCTimeCurrent		,nPCTimeDiff	, nPCTimeGap);		

		if(m_nFrameMaxDeviation < nTargetTimeGap)
			m_nFrameMaxDeviation = nTargetTimeGap;
	}

	//-- 2012-08-16 hongsu@esmlab.com
	//-- divide All Count 
	nTargetTimeTotal =  nTargetTimeTotal / (nAll-1);
	nTargetDeviation = (long)::sqrt((float)nTargetTimeTotal);

	nPCTimeTotal =  nPCTimeTotal / (nAll-1);
	nPCDeviation = (long)::sqrt((float)nPCTimeTotal);
	
	
	m_pTGFramerateGraph->AddDeviation(m_strTitleDeviation,/* nTimeGap*/(m_dwSequence+1)*1000, nTargetDeviation, nPCDeviation);	
	//m_pTGFramerateGraph->AddData(m_strTitleDeviation, nTimeGap, nTargetDeviation);	

	//-- 2012-09-11 joonho.kim
	m_nFrameDeviation = nTargetDeviation;
	m_nPCDeviation = nPCDeviation;

	//-- 2012-07-31 hongsu@esmlab.com
	//-- Decide Pass. Fail
	lResult = lResult | SetResultFrameDeviation(nTargetDeviation , nPCDeviation);
	return lResult;
}

long CTGFramerateView::SetResultFrameDeviation(long nDeviation, long nPCDeviation)
{
	//-- 2012-08-16 joonho.kim
	long lResult = 0;
	BOOL bFPSOpt , bPCOpt;
	double dbFPSTolerance , dbPCTolerance;
	double dbMaxFPS , dbMaxPC; 
	double dbBaseDeviation;
	
	bFPSOpt = (BOOL)TGGetValue(TG_OPT_TOLERANCE_FPS_OPT);
	bPCOpt	= (BOOL)TGGetValue(TG_OPT_TOLERANCE_PC_OPT);

	dbFPSTolerance = (double)TGGetValue(TG_OPT_TOLERANCE_FPS_GAP);
	dbPCTolerance = (double)TGGetValue(TG_OPT_TOLERANCE_PC_GAP);

	if(!bFPSOpt)
	{
		//Framerate Percent ( % )
		dbBaseDeviation = 1000.0/(double)TGGetValue(TG_VAL_FRAMERATE);
		dbMaxFPS = (dbBaseDeviation * (dbFPSTolerance/100.0));
		if(nDeviation > dbMaxFPS)
			lResult = lResult | TG_ERR_FRAME_DEVIATION;
	}
	else
	{
		//Framerate milisecond ( ms )
		if(nDeviation > dbFPSTolerance)
			lResult = lResult | TG_ERR_FRAME_DEVIATION;
	}

	if(!bPCOpt)
	{
		//Framerate Percent ( % )
		dbBaseDeviation = 1000.0/(double)TGGetValue(TG_VAL_FRAMERATE);
		dbMaxPC = (dbBaseDeviation * (dbPCTolerance/100.0));
		if(nPCDeviation > dbMaxPC)
			lResult = lResult | TG_ERR_FRAME_PC_DEVIATION;
	}
	else
	{
		//Framerate milisecond ( ms )
		if(nPCDeviation > dbPCTolerance)
			lResult = lResult | TG_ERR_FRAME_PC_DEVIATION;
	}
	return lResult;
}


//------------------------------------------------------------------------------
//! @brief		Managing FrameTime Array
//! @date		2012-09-19
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CTGFramerateView::AddFrameTime(long nTarget, long nPC)
{
	CTGFrameTime* pNew = new CTGFrameTime(nTarget, nPC);
	AddFrameTime(pNew);
}

void CTGFramerateView::RemoveAllFrameTime()
{
	CTGFrameTime* pExist = NULL;
	int nAll = GetFrameTimeSize();
	while(nAll--)
	{
		pExist = GetFrameTime(nAll);
		if(pExist)
			delete pExist;
		m_arFrameTime.RemoveAt(nAll);
	}
	m_arFrameTime.RemoveAll();
}

CTGFrameTime* CTGFramerateView::GetFrameTime(int nIndex)
{
	if(GetFrameTimeSize()-1 < nIndex || nIndex < 0)
		return NULL;
	return (CTGFrameTime*)m_arFrameTime.GetAt(nIndex);
}
