////////////////////////////////////////////////////////////////////////////////
//
//	TGFramerateView.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////



#pragma once
#include "resource.h"
#include "TGFramerateGraph.h"
#include "TGFramerateView.h"


class CTGFrameTime
{
public:	
	CTGFrameTime(long nTargetTime, long nPCTime)
	{
		m_nTarget	= nTargetTime;	
		m_nPC		= nPCTime;	
	}
	long m_nTarget	;
	long m_nPC		;
};

class CTGFramerateView : public CDialog
{

public:
	CTGFramerateView(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTGFramerateView();

	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CTGFramerateView)
	enum { IDD = IDD_GRAPH_NON_TOOLBAR };
	//}}AFX_DATA


private:
	CObArray	m_arTime;
	CString		m_strTitleFrameCnt;
	CString		m_strTitleDeviation;
	
	long		m_nPrevTime;
	long		m_nFirstTime;
	DWORD		m_dwSequence;

	BOOL		m_bFailFramerate;
	int			m_nFramerate;
	int			m_nGap;

	//-- 2012-09-11 joonho.kim
	int			m_nFrameCount;
	int			m_nFrameDeviation;
	int			m_nFrameMaxDeviation;
	int			m_nFrameReverseFailCount;
	int			m_nPCDeviation;
	BOOL		m_bChkResultData;
	SYSTEMTIME  m_time;

	CTGFramerateGraph* m_pTGFramerateGraph;
	CRect m_rcClientFrame;	
	
	HWND m_hMainWnd;
	
	

	//-- 2012-07-31 hongsu@esmlab.com
	//-- Deviation
	CObArray m_arFrameTime;	
	void AddFrameTime(long nTarget, long nPC);	
	void RemoveAllFrameTime();
	CTGFrameTime* GetFrameTime(int nIndex);
	void AddFrameTime(CTGFrameTime* pFrame) { m_arFrameTime.Add((CObject*)pFrame);}
	int GetFrameTimeSize()					{ return m_arFrameTime.GetSize();}
	
	BOOL IsChangeTime(long nMillisec);
	void ResetDeviation(BOOL bResetAll = TRUE);	
	
	BOOL AddLineFrameCnt(int nTimeGap, SYSTEMTIME time);
	long AddLineFrameDeviation(int nTimeGap, SYSTEMTIME time);

	BOOL SetResultFrameCnt(int nCnt);
	long SetResultFrameDeviation(long nDeviation, long nPCDeviation	);


public:
	//-- 2012-09-11 joonho.kim
	BOOL InsertToDB(CString srtTime, int nFrameCount, int nFrameDeviation, int nGOPFailCount, int nFrameDeviationFailCount, int nPCDeviation);
	SYSTEMTIME GetTime()		{ return m_time;		   };
	int	 GetFrameCount()		{ return m_nFrameCount;	   };
	int  GetFrameDeviation()	{ return m_nFrameDeviation;};
	int	 GetFrameReverseCount() { return m_nFrameReverseFailCount;};
	int  GetPCDeviation()		{ return m_nPCDeviation;};

	void SetFrameReverseCount( int nValue ) { m_nFrameReverseFailCount = nValue; };
	void SetFrameMaxDeviation( int nValue ) { m_nFrameMaxDeviation = nValue; };

	void SetCheck( BOOL bChk )	{ m_bChkResultData = bChk; };
	BOOL GetCheck()				{ return m_bChkResultData; };

	LONG InitGraph(int nFrame, int nGap, CString strModel, CString strVer);
	LONG AddTime(FRAME_INFO* pFrame);
	
	void Clear();
	void Close(){	m_arTime.RemoveAll(); return OnClose();}


protected:	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTGFramerateView)
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	//}}AFX_VIRTUAL
	DECLARE_MESSAGE_MAP()

protected:
	// Generated message map functions
	//{{AFX_MSG(CTGFramerateView)	
	//}}AFX_MSG	
	afx_msg void OnToolbarOpenOptionDlg();	
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	afx_msg void OnClose();	

};