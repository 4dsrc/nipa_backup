////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptLog.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-07
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ESMOptBase.h"
#include "resource.h"
#include "atlcomtime.h"
#include "ESMEditorEX.h"

// CESMOptLog 대화 상자입니다.

class CESMOptLog : public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOptLog)

public:
	CESMOptLog();
	virtual ~CESMOptLog();
	void InitProp(ESMLogOpt& log);
	BOOL SaveProp(ESMLogOpt& log);
// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_LOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:	
	CString m_strLogName;
	int m_nVerbosity;
	int m_nLimitday;
public:
	BOOL m_bTaceTime;
	afx_msg void OnBnClickedBtnBackup();
	COleDateTime m_dateLog;

	CString OnSelectPath();
	int FolderSelect(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData);
	CESMEditorEX m_ctrlLogName;
	CESMEditorEX m_ctrlLogVerbosity;
	CESMEditorEX m_ctrlLogLimitDay;
	CStatic m_Frame1;
};
