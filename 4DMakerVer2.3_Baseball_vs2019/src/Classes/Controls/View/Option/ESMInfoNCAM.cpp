////////////////////////////////////////////////////////////////////////////////
//
//	TGIngoCGI.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "SMILE.h"
#include "TGIngoCGI.h"
#include "TGPagePath.h"
#include "TGIni.h"


#define MODEL_NAME	_T("SNP-6200")

// CTGIngoCGI 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTGIngoCGI, CTGPropertyPage)

CTGIngoCGI::CTGIngoCGI(){}
CTGIngoCGI::~CTGIngoCGI(){}

//---------------------------------------------------------------
//-- Load Infomation from Each Model
//---------------------------------------------------------------
int CTGIngoCGI::LoadInfoValue(CString strModel, CString strItem, CString strProperty)
{
	CString strFilePath;
	CString strValue;
	
	strFilePath.Format(TGGetPath(TG_PATH_CONFIG_NCAM));
	strFilePath.AppendFormat(_T("\\%s.info"),strModel);

	CTGIni ini;
	if(!ini.SetIniFilename (strFilePath))
		return -1;

	strValue = ini.GetString(strItem,strProperty);
	
	return _ttol( strValue );
}

CString CTGIngoCGI::LoadInfoKey(CString strModel, CString strItem, int nValue)
{
	CString strFilePath;
	CString strKey;
	CString strValue;
	
	strFilePath.Format(TGGetPath(TG_PATH_CONFIG_NCAM));
	strFilePath.AppendFormat(_T("\\%s.info"),strModel);

	CTGIni ini;
	if(!ini.SetIniFilename (strFilePath))
		return strKey;

	strValue.Format(_T("%d"),nValue);
	strKey = ini.GetStringValuetoKey(strItem,strValue);
	
	return strKey;
}

int CTGIngoCGI::GetPropertyValue(int nItem, CString strKey)
{	
	CString strModel = MODEL_NAME;
	return LoadInfoValue(strModel, g_arInfoNCAMItem[nItem], strKey);
}

CString CTGIngoCGI::GetPropertyKey(int nItem, int nValue)
{	
	CString strModel = MODEL_NAME;
	CString strTemp;

	if (NCAM_ITEM_CNT <= nItem)
	{
		strTemp.Format(_T("Item MAX %d"),NCAM_ITEM_CNT-1);
		return strTemp;
	}
	return LoadInfoKey(strModel, g_arInfoNCAMItem[nItem], nValue);
}