////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptAdjust.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMOptAdjust.h"
#include "ESMOptPath.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMOptAdjust 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOptAdjust, CESMOptBase)

CESMOptAdjust::CESMOptAdjust()
	: CESMOptBase(CESMOptAdjust::IDD)	
	, m_nThreshold(ADJ_BASIC_VAL_THRESHOLD)
	, m_nFlowmoTime(ADJ_BASIC_VAL_FLOWMO_TIME)
	, m_nAutoBrightness(ADJ_BASIC_VAL_AUTOBRIGHTNESS)
	, m_bClockwise(TRUE)
	, m_bClockreverse(FALSE)
	, m_nCamZoom(0)
	, m_nPointGapMin(ADJ_BASIC_VAL_POINTGAPMIN)
	, m_nMinWidth(ADJ_BASIC_VAL_MIN_WIDTH)
 	, m_nMinHeight(ADJ_BASIC_VAL_MIN_HEIGHT)
 	, m_nMaxWidth(ADJ_BASIC_VAL_MAX_WIDTH)
 	, m_nMaxHeight(ADJ_BASIC_VAL_MAX_HEIGHT)
	, m_nTargetLenth(100)
{
}

CESMOptAdjust::~CESMOptAdjust()
{
}

void CESMOptAdjust::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);

	DDX_Text(pDX,  IDC_ADJUST_TH				, m_nThreshold);
	DDX_Text(pDX,  IDC_ADJUST_MIN_POINTGAP		, m_nPointGapMin);
	DDX_Text(pDX,  IDC_ADJUST_MIN_WIDTH			, m_nMinWidth);	
	DDX_Text(pDX,  IDC_ADJUST_MIN_HEIGHT		, m_nMinHeight);
	DDX_Text(pDX,  IDC_ADJUST_MAX_WIDTH			, m_nMaxWidth);	
	DDX_Text(pDX,  IDC_ADJUST_MAX_HEIGHT		, m_nMaxHeight);
	DDX_Text(pDX,  IDC_ADJUST_TARGETLENTH	, m_nTargetLenth);

	DDX_Text(pDX,  IDC_ADJUST_FLOW_T			, m_nFlowmoTime);
	DDX_Text(pDX,  IDC_ADJUST_AUTO_BRIGHTNESS	, m_nAutoBrightness);
	DDX_Check(pDX, IDC_ADJUST_ORDER_RTL			, m_bClockwise);	
	DDX_Check(pDX, IDC_ADJUST_ORDER_LTR			, m_bClockreverse);	
	DDX_Text(pDX,  IDC_ADJUST_CAMERAZOOM		, m_nCamZoom);

	DDX_Control(pDX, IDC_STATIC_FRAME_SPS, m_ctrlFrameSPS);
	DDX_Control(pDX, IDC_STATIC_FRAME_DETECTSIZE, m_ctrlFrameDetectSize);
	DDX_Control(pDX, IDC_STATIC_FRAME_CAMERA_ORDER, m_ctrlCameraOrder);
	DDX_Control(pDX, IDC_STATIC_THREADHOLD, m_ctrlThredHold);
	DDX_Control(pDX, IDC_STATIC_MINPOINTGAP, m_ctrlMinPointGap);
	DDX_Control(pDX, IDC_ADJUST_TH, m_ctrlAdjustTh);
	DDX_Control(pDX, IDC_ADJUST_MIN_POINTGAP, m_ctrlAdjustMinPointGap);
	DDX_Control(pDX, IDC_ADJUST_MIN_WIDTH, m_ctrlAdjustMinWidth);
	DDX_Control(pDX, IDC_ADJUST_MAX_WIDTH, m_ctrlAdjustMaxWidth);
	DDX_Control(pDX, IDC_ADJUST_MIN_HEIGHT, m_ctrlAdjustMinHeight);
	DDX_Control(pDX, IDC_ADJUST_MAX_HEIGHT, m_ctrlAdjustMaxHeight);
	DDX_Control(pDX, IDC_ADJUST_FLOW_T, m_ctrlAdjustFlowT);
	DDX_Control(pDX, IDC_ADJUST_CAMERAZOOM, m_ctrlAdjustCamZoom);
	DDX_Control(pDX, IDC_ADJUST_AUTO_BRIGHTNESS, m_ctrlAutoBR);
	DDX_Control(pDX, IDC_ADJUST_TARGETLENTH, m_ctrlAdjustTargetLength);
}

void CESMOptAdjust::InitProp(ESMAdjust& Opt)
{
	//-- 2013-10-11 jaehyun@esmlab.com
	//-- Change error value to basic value
	if(!Opt.nThresdhold)						Opt.nThresdhold = ADJ_BASIC_VAL_THRESHOLD;
	if(!Opt.nFlowmoTime)						Opt.nFlowmoTime = ADJ_BASIC_VAL_FLOWMO_TIME;
	if(!Opt.bClockwise && !Opt.bClockreverse)	Opt.bClockwise = TRUE;

	m_nThreshold		= Opt.nThresdhold;
	m_nFlowmoTime		= Opt.nFlowmoTime;
	m_nAutoBrightness	= Opt.nAutoBrightness;
	m_bClockwise		= Opt.bClockwise;
	m_bClockreverse		= Opt.bClockreverse;
	m_nCamZoom			= Opt.nCamZoom;

	m_nPointGapMin		= Opt.nPointGapMin;
	m_nMinWidth			= Opt.nMinWidth;
	m_nMinHeight		= Opt.nMinHeight;
	m_nMaxWidth			= Opt.nMaxWidth;
	m_nMaxHeight		= Opt.nMaxHeight;
	m_nTargetLenth		= Opt.nTargetLenth;

	GetDlgItem(IDC_STATIC_FRAME_SPS)->			SetWindowPos(NULL,0,0,514,322,NULL);
	GetDlgItem(IDC_STATIC_FRAME_DETECTSIZE)->	SetWindowPos(NULL,24,81,474,117,NULL);
	GetDlgItem(IDC_STATIC_FRAME_CAMERA_ORDER)->	SetWindowPos(NULL,0,356,514,120,NULL);

	GetDlgItem(IDC_STATIC_THREADHOLD)->			SetWindowPos(NULL,36,30,80,24,NULL);
	GetDlgItem(IDC_ADJUST_TH)->					SetWindowPos(NULL,143,30,80,24,NULL);
	GetDlgItem(IDC_STATIC_MINPOINTGAP)->		SetWindowPos(NULL,257,30,80,24,NULL);
	GetDlgItem(IDC_ADJUST_MIN_POINTGAP)->		SetWindowPos(NULL,376,30,80,24,NULL);

	GetDlgItem(IDC_STATIC_MIN_WIDTH)->			SetWindowPos(NULL,36,108,80,24,NULL);
	GetDlgItem(IDC_ADJUST_MIN_WIDTH)->			SetWindowPos(NULL,143,108,80,24,NULL);
	GetDlgItem(IDC_STATIC_MAX_WIDTH)->			SetWindowPos(NULL,257,108,80,24,NULL);
	GetDlgItem(IDC_ADJUST_MAX_WIDTH)->			SetWindowPos(NULL,376,108,80,24,NULL);

	GetDlgItem(IDC_STATIC_MIN_HEIGHT)->			SetWindowPos(NULL,36,147,80,24,NULL);
	GetDlgItem(IDC_ADJUST_MIN_HEIGHT)->			SetWindowPos(NULL,143,147,80,24,NULL);
	GetDlgItem(IDC_STATIC_MAX_HEIGHT)->			SetWindowPos(NULL,257,147,80,24,NULL);
	GetDlgItem(IDC_ADJUST_MAX_HEIGHT)->			SetWindowPos(NULL,376,147,80,24,NULL);

	GetDlgItem(IDC_STATIC_FLOW_MO_TIME)->		SetWindowPos(NULL,24,225,100,24,NULL);
	GetDlgItem(IDC_ADJUST_FLOW_T)->				SetWindowPos(NULL,143,225,80,24,NULL);
	GetDlgItem(IDC_STATIC_CAMERA_ZOOM)->		SetWindowPos(NULL,257,225,100,24,NULL);
	GetDlgItem(IDC_ADJUST_CAMERAZOOM)->			SetWindowPos(NULL,376,225,80,24,NULL);
	GetDlgItem(IDC_STATIC_CM)->					SetWindowPos(NULL,466,225,50,24,NULL);

	GetDlgItem(IDC_STATIC_AUTO_BR)->			SetWindowPos(NULL,24,264,100,24,NULL);
	GetDlgItem(IDC_ADJUST_AUTO_BRIGHTNESS)->	SetWindowPos(NULL,143,264,80,24,NULL);
	GetDlgItem(IDC_STATIC_TARGET_LENGTH)->		SetWindowPos(NULL,257,264,80,24,NULL);
	GetDlgItem(IDC_ADJUST_TARGETLENTH)->		SetWindowPos(NULL,376,264,80,24,NULL);
	GetDlgItem(IDC_STATIC_MM)->					SetWindowPos(NULL,466,264,80,24,NULL);

	//GetDlgItem(IDC_ADJUST_ORDER_RTL)->			SetWindowPos(NULL,24,376,150,20,NULL);
	//GetDlgItem(IDC_ADJUST_ORDER_LTR)->			SetWindowPos(NULL,24,416,150,20,NULL);

	UpdateData(FALSE);
}

BOOL CESMOptAdjust::SaveProp(ESMAdjust& Opt)
{
	if(!UpdateData(TRUE))
		return FALSE;
	Opt.nThresdhold		= m_nThreshold;
	Opt.nFlowmoTime		= m_nFlowmoTime;
	Opt.nAutoBrightness	= m_nAutoBrightness;
	Opt.bClockwise		= m_bClockwise;
	Opt.bClockreverse	= m_bClockreverse;
	Opt.nCamZoom		= m_nCamZoom;

	Opt.nPointGapMin	= m_nPointGapMin;
	Opt.nMinWidth		= m_nMinWidth;
	Opt.nMinHeight		= m_nMinHeight;
	Opt.nMaxWidth		= m_nMaxWidth;
	Opt.nMaxHeight		= m_nMaxHeight;
	Opt.nTargetLenth	= m_nTargetLenth;

	return TRUE;
}

BEGIN_MESSAGE_MAP(CESMOptAdjust, CESMOptBase)
END_MESSAGE_MAP()
