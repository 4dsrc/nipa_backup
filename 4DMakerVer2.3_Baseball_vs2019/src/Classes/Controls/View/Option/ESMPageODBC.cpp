////////////////////////////////////////////////////////////////////////////////
//
//	TGPageODBC.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TG.h"
#include "TGPageODBC.h"
//-- 2012-04-06 hongsu.jung
#include "TGFunc.h"

#include "MainFrm.h"
extern CMainFrame* g_pTGMainWnd;	
// CTGPageODBC 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTGPageODBC, CTGPropertyPage)

CTGPageODBC::CTGPageODBC()
	: CTGPropertyPage(CTGPageODBC::IDD)	
	, m_strODBCDataSource	(_T(""))
	, m_strODBCInitCatalog	(_T(""))
	, m_strODBCPassword		(_T(""))	
	, m_strODBCProvider		(_T(""))
	, m_strODBCUser			(_T(""))	
	, m_bODBCCheck(FALSE)	
{
}

CTGPageODBC::~CTGPageODBC()
{
}

void CTGPageODBC::DoDataExchange(CDataExchange* pDX)
{
	CTGPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_ODBC_DATA	,m_strODBCDataSource	);
	DDX_Text(pDX, IDC_ODBC_USER	,m_strODBCUser			);
	DDX_Text(pDX, IDC_ODBC_PW	,m_strODBCPassword		);	
	DDX_Text(pDX, IDC_ODBC_PROV	,m_strODBCProvider		);
	DDX_Text(pDX, IDC_ODBC_CATA	,m_strODBCInitCatalog	);		
	DDX_Control(pDX, IDC_STATUS_ODBC, m_ctrStatusODBC);
	DDX_Check(pDX, IDC_ODBC_CHECK, m_bODBCCheck);	
	DDX_Control(pDX, IDC_ODBC_PROV, m_ctrTMSProvider);
	DDX_Control(pDX, IDC_ODBC_CATA, m_ctrTMSInitCatalog);
	DDX_Control(pDX, IDC_ODBC_DATA, m_ctrTMSDataSource);
	DDX_Control(pDX, IDC_ODBC_USER, m_ctrTMSUser);
	DDX_Control(pDX, IDC_ODBC_PW, m_ctrTMSPW);
}

void CTGPageODBC::InitProp(TGODBC& odbc)
{
	m_bODBCCheck			= odbc.bODBCCheck;		
	
	m_strODBCDataSource		= odbc.strODBCDataSource	;
	m_strODBCUser			= odbc.strODBCUser			;
	m_strODBCPassword		= odbc.strODBCPassword		;	
	m_strODBCProvider		= odbc.strODBCProvider		;
	m_strODBCInitCatalog	= odbc.strODBCInitCatalog	;

	//-- 2012-04-06 hongsu.jung
	CheckStatus();	

	UpdateData(FALSE);
}

BOOL CTGPageODBC::SaveProp(TGODBC& odbc)
{
	if(!UpdateData(TRUE))
		return FALSE;
	
	odbc.bODBCCheck			= m_bODBCCheck			;
	odbc.strODBCDataSource	= m_strODBCDataSource	;
	odbc.strODBCUser		= m_strODBCUser			;
	odbc.strODBCPassword	= m_strODBCPassword		;	
	odbc.strODBCProvider	= m_strODBCProvider		;
	odbc.strODBCInitCatalog	= m_strODBCInitCatalog	;

	return TRUE;
}

BEGIN_MESSAGE_MAP(CTGPageODBC, CTGPropertyPage)
	ON_BN_CLICKED(IDC_DIR_OPEN, OnSelectPath)
	ON_BN_CLICKED(IDC_ODBC_CHECK, OnOdbcCheck)	
END_MESSAGE_MAP()


//------------------------------------------------------------------------------ 
//! @brief		OnOdbcCheck()
//! @date		2010-5-28 Lee JungTaek
//! @attention	none
//! @note	 	Change Check Option
//------------------------------------------------------------------------------
void CTGPageODBC::OnOdbcCheck()
{
	if(!UpdateData(TRUE))
		return;

	//-- 2012-08-01 hongsu@esmlab.com
	//-- Save
	SaveProp(g_pTGMainWnd->GetTGOption()->m_ODBC);
	//-- 2012-04-06 hongsu.jung
	CheckStatus();	

	UpdateData(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		CheckStatus
//! @date		2012-04-06 
//! @attention	hongsu jung
//! @note	 	show status
//------------------------------------------------------------------------------
void CTGPageODBC::CheckStatus()
{
	//-- Close Database
	if(!m_bODBCCheck)
	{
		TGLog(3,_T("[DB] Close / Remove DB Manager"));
		//-- Set Database Setting
		TGSetValue(TG_OPT_ODBC_TMS_CHECK,m_bODBCCheck);
		//-- Send Message
		TGEvent *pMsg = new TGEvent;
		pMsg->message = WM_TG_ODBC_CLOSE;		
		pMsg->pParam = NULL;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_TG, (WPARAM)WM_TG_ODBC, (LPARAM)pMsg);

		//-- 2012-10-26 hongsu@esmlab.com
		//-- Set Connection Info 
		TGSetValue(TG_OPT_ODBC_TMS_CONNECTION, m_bODBCCheck); // FALSE
	}

	//-- Open Database
	else
	{
		TGLog(3,_T("[DB] Open / Create DB Manager"));
		//-- Set Database Setting
		TGSetValue(TG_OPT_ODBC_TMS_CHECK,m_bODBCCheck);		
		//-- Send Message
		TGEvent *pMsg = new TGEvent;
		pMsg->message = WM_TG_ODBC_OPEN;
		pMsg->pParam = NULL;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_TG, (WPARAM)WM_TG_ODBC, (LPARAM)pMsg);	
		
	}
}

//------------------------------------------------------------------------------ 
//! @brief		CheckStatus
//! @date		2012-04-06 
//! @attention	hongsu jung
//! @note	 	Show Status Icon
//------------------------------------------------------------------------------
void CTGPageODBC::ShowStatus(BOOL bStatus)
{
	//-- Status Image
	HBITMAP hbmp;	
	if(bStatus)
	{
		hbmp = (HBITMAP)::LoadImage(AfxGetInstanceHandle(),
										MAKEINTRESOURCE(IDB_STATUS_CONNECT),
										IMAGE_BITMAP,
										0,0,LR_LOADMAP3DCOLORS);
		m_ctrStatusODBC.SetBitmap(hbmp);
		GetDlgItem(IDC_CONNECT_ODBC)->SetWindowText(_T("Open"));
		//-- 2012-05-21 hongsu
		TGLog(3,_T("Open Icon Draw"));
	}
	else
	{
		hbmp = (HBITMAP)::LoadImage(AfxGetInstanceHandle(),
										MAKEINTRESOURCE(IDB_STATUS_DISCONNECT),
										IMAGE_BITMAP,
										0,0,LR_LOADMAP3DCOLORS);
		m_ctrStatusODBC.SetBitmap(hbmp);
		GetDlgItem(IDC_CONNECT_ODBC)->SetWindowText(_T("Close"));
		//-- 2012-05-21 hongsu
		TGLog(3,_T("Close Icon Draw"));
	}	
	UpdateData(FALSE);
}
	

