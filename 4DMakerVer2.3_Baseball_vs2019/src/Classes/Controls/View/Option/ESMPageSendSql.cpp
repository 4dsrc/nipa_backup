////////////////////////////////////////////////////////////////////////////////
//
//	TGPageSendSql.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TG.h"
#include "TGPageSendSql.h"
//-- 2012-04-06 hongsu.jung
#include "TGFunc.h"

#include "MainFrm.h"
#include "TGFileOperation.h"
extern CMainFrame* g_pTGMainWnd;	
// CTGPageSendSql 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTGPageSendSql, CTGPropertyPage)

CTGPageSendSql::CTGPageSendSql()
	: CTGPropertyPage(CTGPageSendSql::IDD)	
{
}

CTGPageSendSql::~CTGPageSendSql()
{
}

void CTGPageSendSql::DoDataExchange(CDataExchange* pDX)
{
	CTGPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_UPGRADE_FILE_LIST	, m_ListCtrlSelectedFileList	);
}

BEGIN_MESSAGE_MAP(CTGPageSendSql, CTGPropertyPage)
	ON_BN_CLICKED(IDC_BTN_UPGRADE, OnSendSql)	
	ON_BN_CLICKED(IDCANCEL, &CTGPageSendSql::OnBnClickedCancel)
END_MESSAGE_MAP()


void CTGPageSendSql::InitProp()
{
	CString strSQLFolder = TGGetPath(TG_PATH_SQL);
	SqlGetFiles(strSQLFolder);
}

void CTGPageSendSql::SqlGetFiles(CString strPath)
{
	int nCount = 0;
	int nTok;
	int pos;
	CFileFind find;
	CString strFileName, strIP, strTC, strUUID, token, strTok;
	BOOL bFind = FALSE;
	strPath += _T("\\*.sql");
	bFind = find.FindFile(strPath);

	m_ListCtrlSelectedFileList.DeleteAllItems();

	while(bFind)
	{
		nTok = 0;
		pos = 0;
		bFind = find.FindNextFileW();
		if(find.IsDots() || find.IsDirectory())
			continue;
		else
		{
			strFileName = find.GetFileName();
		}

		nCount++;

		token = strFileName.Tokenize(_T("[]"), pos);
		while(token != _T(""))
		{
			switch(nTok)
			{
			case TG_LOCAL_IP :	strIP.Format(_T("%s"), token);		break;
			case TG_TC_NAME  :	strTC.Format(_T("%s"), token);		break;
			case TG_UUID	 :	strUUID.Format(_T("%s"), token);	break;
			}
			nTok++;
			token = strFileName.Tokenize(_T("[]"), pos);
		}	
		SqlListSetFiles( strIP, strTC, strUUID, nCount );
	}
}

void CTGPageSendSql::SqlListSetFiles(CString strIP,CString strTC,CString strUUID,int nCount )
{
	m_ListCtrlSelectedFileList.InsertItem( 0, strTC);
	m_ListCtrlSelectedFileList.SetItemText( 0, 0, strIP);
	m_ListCtrlSelectedFileList.SetItemText( 0, 1, strTC);
	m_ListCtrlSelectedFileList.SetItemText( 0, 2, strUUID);
}



BOOL CTGPageSendSql::OnInitDialog()
{
	CDialog::OnInitDialog();
	InitListCtrl();
	return TRUE;	
}


void CTGPageSendSql::InitListCtrl()
{
	//-- List Control Init
	m_ListCtrlSelectedFileList.DeleteAllItems();
	// List Style
	m_ListCtrlSelectedFileList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_ListCtrlSelectedFileList.InsertColumn(0, _T("Local IP"), LVCFMT_CENTER, 100, -1);
	m_ListCtrlSelectedFileList.InsertColumn(1, _T("TC"), LVCFMT_CENTER, 500, -1);
	m_ListCtrlSelectedFileList.InsertColumn(2, _T("UUID"), LVCFMT_CENTER, 300, -1);
}
//------------------------------------------------------------------------------ 
//! @brief		OnOdbcCheck()
//! @date		2010-5-28 Lee JungTaek
//! @attention	none
//! @note	 	Change Check Option
//------------------------------------------------------------------------------
void CTGPageSendSql::OnSendSql()
{
	CString strIP, strTC, strUUID;
	CString strFileName, strData;
	CStdioFile	file;
	CFileException	fileException;

	if(!UpdateData(TRUE))
		return;

	int nSelect =	m_ListCtrlSelectedFileList.GetNextItem(-1,LVNI_SELECTED);
	strIP = m_ListCtrlSelectedFileList.GetItemText(nSelect, 0);
	strTC = m_ListCtrlSelectedFileList.GetItemText(nSelect, 1);
	strUUID = m_ListCtrlSelectedFileList.GetItemText(nSelect, 2);

	strFileName.Format(_T("[%s][%s][%s].sql"), strIP, strTC, strUUID);
	CString strSQLFolder = TGGetPath(TG_PATH_SQL);
	strSQLFolder += _T("\\");
	strSQLFolder += strFileName;

	if(!file.Open(strSQLFolder, CFile::modeRead, &fileException))
	{
		fileException.ReportError();
		return;
	}

	while(file.ReadString( strData ))
	{
		strData.Replace(_T("\r\n"), _T(""));
		// Send SQL Data
		CTGLocalSQL* pInfo = new CTGLocalSQL(strData);
		if(!TGSendMessageToDB(WM_TG_ODBC_LOCAL_SQL, (PVOID)pInfo))
		{
			delete pInfo;
			pInfo = NULL;
		} 
		Sleep(10);
	}
	file.Close();
	DeleteFile(strSQLFolder);
	SqlGetFiles( TGGetPath(TG_PATH_SQL) );
	TGLog(5,_T("Send SQL Files"));
	UpdateData(FALSE);
}

void CTGPageSendSql::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
