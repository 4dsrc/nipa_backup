#include "stdafx.h"
#include "TG.h"
#include "TGPageSrmview.h"

#include "TestStepDef.h"

IMPLEMENT_DYNAMIC(CTGPageSrmview, CTGPropertyPage)
BEGIN_MESSAGE_MAP(CTGPageSrmview, CTGPropertyPage)
END_MESSAGE_MAP()

CTGPageSrmview::CTGPageSrmview() 
: CTGPropertyPage(CTGPageSrmview::IDD)	
{
	m_strModel	= g_arStepDeviceType[0];
	m_strIP		= LOCAL_HOST	;
	m_nPort		= SRM_BASE_PORT	;	
}
CTGPageSrmview::~CTGPageSrmview(){}

void CTGPageSrmview::DoDataExchange(CDataExchange* pDX)
{
	CTGPropertyPage::DoDataExchange(pDX);
	((CEdit*)GetDlgItem(IDC_EDIT_PORT))->SetLimitText(5);
	
	DDX_Control	(pDX,	IDC_COMBO_MODEL		, m_cmbModel);
	DDX_Control	(pDX,	IDC_COMM_IPADDRESS	, m_ipcIP);
	DDX_Text	(pDX,	IDC_EDIT_PORT		, m_nPort);
	DDV_MinMaxInt(pDX, m_nPort, 100, 65535);
}

void CTGPageSrmview::InitProp(TGSrmview& opt)
{
	//Get [Web Test] section values from the SMILE configuration file(info.tgw)
	if(opt.m_strModel.GetLength())	m_strModel	= opt.m_strModel;
	if(opt.m_strIP.GetLength())		m_strIP		= opt.m_strIP	;	
	if(opt.m_nPort)					m_nPort		= opt.m_nPort	;

	//-- MODEL 
	m_cmbModel.AddString(g_arStepDeviceType[0]);
	m_cmbModel.AddString(g_arStepDeviceType[2]);
	int nIndex = m_cmbModel.FindStringExact(m_cmbModel.GetTopIndex(), m_strModel);
	if(nIndex != CB_ERR)
		m_cmbModel.SetCurSel(nIndex);
	else
		m_cmbModel.SetCurSel(m_cmbModel.GetTopIndex());

	//-- IP
	if(m_strIP.GetLength() == 0)
		m_strIP = _T("127.0.0.1");
	m_ipcIP.SetWindowText(m_strIP);

	//-- Port
	if(0 == m_nPort)
		m_nPort = SRM_BASE_PORT;

	UpdateData(FALSE);
}

BOOL CTGPageSrmview::SaveProp(TGSrmview& opt)
{
	//If TRUE, data is being retrieved.
	if(!UpdateData(TRUE))
		return FALSE;

	//-- Model
	m_cmbModel.GetLBText(m_cmbModel.GetCurSel(),m_strModel);

	//-- IP
	m_ipcIP.GetWindowText(m_strIP);

	opt.m_strModel	= m_strModel;
	opt.m_strIP		= m_strIP	;	
	opt.m_nPort		= m_nPort	;
	return TRUE;
}
