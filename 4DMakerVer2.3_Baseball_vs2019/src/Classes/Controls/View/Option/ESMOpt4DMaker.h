////////////////////////////////////////////////////////////////////////////////
//
//	ESMOpt4DMaker.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-07
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMOptBase.h"
#include "resource.h"
#include "afxwin.h"
#include "ESMEditorEX.h"
#include "ESMGroupBox.h"
#include "ESMButtonEx.h"


class CESMOpt4DMaker : public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOpt4DMaker)

public:
	CESMOpt4DMaker();
	virtual ~CESMOpt4DMaker();
	void InitProp(ESM4DMaker& Opt);
	BOOL SaveProp(ESM4DMaker& Opt);
	enum { IDD = IDD_OPTION_4DMAKER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	DECLARE_MESSAGE_MAP()

public:	
	ESM4DMaker m_Opt;
	BOOL m_bMakeServer;
	BOOL m_bMode;
	BOOL m_bViewInfo;
	BOOL m_bTestMode;
	BOOL m_bMakeAndAotuPlay;
	BOOL m_bInsertLogo;
	BOOL m_b3DLogo;
	BOOL m_bLogoZoom;
	BOOL m_bInsertCameraID;
	BOOL m_bTimeLinePreview;
	BOOL m_bEnableVMCC;
	BOOL m_bEnableCopy;
	BOOL m_bEnableBaseBall;
	BOOL m_bReverseMovie;
	BOOL m_bGPUMakeFile;
	BOOL m_bRepeatMovie;
	BOOL m_bUHDtoFHD;
	BOOL m_bSaveImg;
	BOOL m_bCheckValid;
	BOOL m_bInsertKzone;//16/08/11 hjcho
	int  m_nPrismValue; //16.09.12 hjcho
	int  m_nPrismValue2; //16.10.19 hjcho
	BOOL m_bInsertWhiteBal; //161212 hjcho
	BOOL m_bColorRivision;
	BOOL m_bPrnColorInfo;
	BOOL m_bSyncSkip;
	int m_nBaseBallInning;
	int m_bServerMode;
	int m_nPort;
	int m_nDeleteCnt;
	int m_nRepeatMovieCnt;
	int m_nDSCSyncCnt;
	int m_nWaitSaveDSC;
	//jhhan
	int m_nWaitSaveDSCSub;
	int m_nWaitSaveDSCSel;

	int m_nSyncTimeMargin;
	int m_nSensorOnTime;
	int m_nTemplateStartMargin;
	int m_nTemplateEndMargin;
	int m_nTemplateStartSleep;
	int m_nTemplateEndSleep;

	////16-12-12 wgkim@esmlab.com
	BOOL m_bTemplatePoint;
	//17-05-17
	BOOL m_bTemplateStabilization;

	//17-07-27	
	BOOL m_bLightWeight;
	
	//17-01-13 HJCHO
	BOOL m_bCheckAJALogo;
	BOOL m_bCheckAJAReplay;

	//17-02-07 hjcho
	BOOL m_bCheckAJAOnOff;
	//17-03-19 hjcho
	int m_nAJASampleCnt;

	//17-07-06 hjcho
	BOOL m_bGPUMakeSkip;
	
	//17-07-28 hjcho
	BOOL m_bAutoDetect;

	//17-08-30 hjcho
	BOOL m_bDirectMux;

	//17-11-27 wgkim
	BOOL m_bEncodingForSmartPhones;

	//-- 2015-02-15 cygil@esmlab.com
	//-- Add picture coutdown
	int m_nCountdownLastIP;
	CButton m_BtnServer;
	CButton m_BtnAgent;

	CButton m_BtnMakeOn;
	CButton m_BtnMakeOff;

	CButton m_BtnServerBoth;
	CButton m_BtnServerMaking;
	CButton m_BtnServerRecording;

	int m_nMovieSaveLocation;

	afx_msg void OnBnClickedMakeRadio();
	afx_msg void OnBnClicked4dmakerRadio();
	afx_msg void OnBnClicked4dserverRadio();
	afx_msg void OnBnClickedCheckEnableVmcc();
	afx_msg void OnBnClickedCheckEnable4DPCopy();
	afx_msg void OnBnClickedCheckEnableSaveImg();
	afx_msg void OnBnClickedCheckEnableBaseBall();
	afx_msg void OnBnClickedCheckEnableValid();
	CButton m_ctrlEnableBaseBall;
	CButton m_ctrlEnableVMCC;
	CButton m_ctrlEnableCopy;
	CButton m_ctrlUHDtoFHD;
	CButton	m_ctrlEnableSaveImg;
	CButton m_ctrlCheckValid;
	CComboBox m_ctrlBaseBall;
	CString m_strBaseBall;

	//jhhan
	CESMEditorEX m_EditWaitSaveDSCSub;

	BOOL m_bRamDiskSize;	
	BOOL m_bTemplateModify;
	afx_msg void OnBnClickedBtnSyncSet();

	//hjcho 170904
	CButton m_btn4DP_MUL;
	CButton m_btn4DP_ENC;
	CButton m_btn4DP_60p;
	int m_n4DPMethod;

	//int		m_n4DPMethod;
	afx_msg void OnBnClickedChkGpuMake();
	afx_msg void OnBnClickedRadio4dpMulgpu();
	BOOL m_bRemote;
	CIPAddressCtrl m_ctlRemoteIp;
	BOOL m_bRemoteSkip;
	
	//hjcho 171210
	int m_nFrameZoomRatio;

private:
	CToolTipCtrl m_tooltip;
public:
	BOOL m_bHorizontal;
	
	//wgkim 180119
	int m_nTemplateTargetOutPercent;
	BOOL m_bRecordFileSplit;

	//180508 hjcho
	BOOL m_bAJANetwork;
	CIPAddressCtrl m_ipAJANetwork;

	//180629 hjcho
	BOOL m_bGIFMaking;
	CComboBox m_cbGIFQuality;
	int	m_nGIFQuaility;
	CComboBox m_cbGIFSize;
	int m_nGIFSize;

	afx_msg void OnBnClickedCheckAutodetecting();
	CESMEditorEX m_ctrlPort;
	CESMEditorEX m_ctrlCaptureDelay;
	CESMEditorEX m_ctrlSyncTime;
	CESMEditorEX m_ctrlSyncCount;
	CESMEditorEX m_ctrlMovieDeleteCnt;
	CESMEditorEX m_ctrlMovieRepeatCnt;
	CESMEditorEX m_ctrlTemplateTarget;
	CStatic m_ctrlProgramState;
	CStatic m_ctrlFrameMovie;
	CStatic m_ctrlEventMovie;
	CStatic m_ctrlLogoOption;
	CStatic m_ctrlCameraSync;
	CStatic m_ctrlAJA;
	CStatic m_ctrl4DPMethod;
	CStatic m_ctrl4DP;
	CStatic m_ctrlTemplatePoint;
	CStatic m_ctrlServerMake;
	CESMButtonEx m_btnSyncSet;
	afx_msg void OnCbnSelchangeCbnGifquality();
	afx_msg void OnCbnSelchangeCbnGifSize();

	//wgkim 180629
	int m_nSetFrameRateIndex;
	CComboBox m_cbSetFrameRate;

	//hjcho 180718
	BOOL m_bRefereeRead;
	afx_msg void OnCbnSelchangeCbnSetFrameRate();
	CStatic m_ctrlGifMake;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	BOOL m_bUseReSync;
	BOOL m_bAutoDelete;
	BOOL m_bAutoAdjustPositionTracking;

	//hjcho 180927
	BOOL m_bRTSP;
	BOOL m_bAutoAdjustKZone;
	BOOL m_b4DAP;

	//wgkim 181119
	int m_nSetTemplateModifyMode;
	CComboBox m_cbSetTemplateModifyMode;
	afx_msg void OnSelchangeComboSetTemplateModify();

	//hjcho 190103
	int m_nEncodingSmartPhoneBitRate;
	CESMEditorEX m_ctrlEncodeBitrate;
};
