/////////////////////////////////////////////////////////////////////////////
//
//  CommandDlg.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "RSChildDialog.h"
#include "SdiMultiMgr.h"
#include "SRSIndex.h"
//#include "RSButton.h"
//#include "NXRemoteStudioFunc.h"



// CProgressBar dialog
class CTitleBase : public CRSChildDialog
{
	DECLARE_DYNAMIC(CTitleBase)

public:
	CTitleBase(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTitleBase();

// Dialog Data
	enum { IDD = IDD_DLG_TITLE_BASE};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();	
	//CMiLRe 20141119 Sleep Mode ���� ĸ��
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()	
	
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	
	
	BOOL m_bRecStartStop; //NX1 dh0.seo 2014-08-28
	//CSdiMultiMgr* GetMTPMgr();	
	//CMiLRe 20141119 Sleep Mode ���� ĸ��
	void MouseMoveSet(CPoint point);

private:
	CRect	m_rect[BTN_CMD_CNT];
	int		m_nStatus[BTN_CMD_CNT];
	int		m_nMouseIn;

	void DrawBackground();	
	void DrawItem(CDC* pDC, int nItem);
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus, BOOL bRedraw = TRUE);
	void ChangeStatusAll();
	void MouseEvent();
	
	void SetFocusPositionValue(int nValue);

	//dh0.seo 2014-11-14
	BOOL SetPosition(CPoint point);
	CString m_strTitleBarName;

public:
	BOOL m_bEnable;
	void EnableControl(BOOL bEnable) { m_bEnable = bEnable;}
	void SetTitleBarName(CString strValue) { m_strTitleBarName = strValue;}
	BOOL m_bSetting;
	BOOL m_bMaxMin;
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

	void FullScreen();
	BOOL m_bIsFileListDlgVisibled;

//FullScreen enable/disable
public:
	bool EnterFullscreen(HWND hwnd, int fullscreenWidth, int fullscreenHeight, int colourBits, int refreshRate);
	bool ExitFullscreen(HWND hwnd, int windowX, int windowY, int windowedWidth, int windowedHeight, int windowedPaddingX, int windowedPaddingY);
	HWND CreateFullscreenWindow(HWND hwnd);
	RECT m_pPreviousWindowRect;		
};
