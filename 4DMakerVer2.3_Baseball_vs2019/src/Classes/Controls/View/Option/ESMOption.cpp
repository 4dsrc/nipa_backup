/////////////////////////////////////////////////////////////////////////////
//
//	ESMOption.cpp: main header file for the TestGuarantee application
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, Inc., and may not be copied
//  nor disclosed except upon written agreement
//  by ESMLab, Inc..
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2009-02-01
//
/////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMOption.h"
#include "ESMIni.h"
#include "ESMFunc.h"
#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//==============================================================================
// Constructor/Destructor
//==============================================================================

CESMOption::CESMOption()
{
	m_strIP					= _T("0.0.0.0");
	m_strHostname			= _T("Unknown");
	m_bDirectLoadProfile    = FALSE;

	//-- 2011-11-28 hongsu.jung
	//-- Set Local IP
	GetNetworkInfo();

	m_TemplateOpt.bRecovery = FALSE;
}

CESMOption::~CESMOption(){}

//------------------------------------------------------------------------------ 
//! @brief		Load
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CESMOption::Load(CString strFile)
{
	CESMIni ini;

	//-- 2013-10-05 hongsu@esmlab.com
	//-- Set Config File
	m_strConfig = strFile;

	ESMAdjustMovie AdjustMovie;
	ESM4DMaker MakerOptionData;
	ESMCeremony CeremonyData;
	ESMManagement ManagementData;
	CString strPath[ESM_OPT_PATH_ALL], strCamInfo[MAX_CAMCOUNT], strCamName[MAX_CAMCOUNT], strBackup[ESM_BACKUP_STRING_ALL];	
	CString strFilename, strVerbosity, strLimitday, strPort;
	//CMiLRe 20151013 Template Load INI File
	//CString strCtrl1,strCtrl2,strCtrl3,strCtrl4, strCtrl5, strCtrl6, strCtrl7, strCtrl8, strCtrl9, strPage;
	CString strPage;
	CString strCtrl[5][9]; //2016/07/11 Lee SangA Replace with strCtrl1~9

	BOOL bTraceTime, bClockwise, bClockreverse;
	int nThreshold = 0, nFlowmoTime = 0, nAutoBrightness = 0;
	int nCamCount = 0, dCamZoom = 0;
	int nMinWidth = 0, nMinHeight = 0, nMaxWidth = 0, nMaxHeight = 0, nPointGapMin = 0, nTargetLenth = 0;

	//jhhan
	BOOL bDefault;

	//-- Load Config File
	if(!ini.SetIniFilename(strFile))
	{
		TCHAR currentDir[MAX_PATH];
		GetCurrentDirectory( MAX_PATH, currentDir );

		CString strWorkingDir;
		strWorkingDir.Format(_T("%s"),currentDir);

		CESMFileOperation fo;
		strWorkingDir = fo.GetPreviousFolder(strWorkingDir);
		strWorkingDir = fo.GetPreviousFolder(strWorkingDir);

		strPath[ESM_OPT_PATH_HOME	].Format(_T("%s"),strWorkingDir);
		strPath[ESM_OPT_PATH_FILESVR].Format(_T("$(HOME)"));
		strPath[ESM_OPT_PATH_LOG	].Format(_T("$(HOME)\\log"));		
		strPath[ESM_OPT_PATH_CONF	].Format(_T("$(HOME)\\config"));		
		strPath[ESM_OPT_PATH_WORK	].Format(_T("$(HOME)\\work"));

		strPath[ESM_OPT_PATH_RECORD	].Format(_T("$(HOME)\\Record"));
		strPath[ESM_OPT_PATH_OUTPUT	].Format(_T("$(HOME)\\output"));	
		strPath[ESM_OPT_PATH_SERVERRAM	].Format(_T("M:\\Movie"));
		strPath[ESM_OPT_PATH_CLIENTRAM	].Format(_T("M:\\Movie"));
		strPath[ESM_OPT_PATH_SAVE_IMG	] = _T("");
		
		//jhhan
		bDefault = FALSE;

		strFilename	=	_T("4DMaker.log");
		strVerbosity=	_T("5");
		strLimitday	=	_T("5");
		bTraceTime	=	true;
		MakerOptionData.bMakeServer	=	TRUE;
		MakerOptionData.bRCMode		=	false;
		nThreshold	=	150;
		MakerOptionData.nWaitSaveDSC		= DEFAULT_WAIT_SAVEDSC;
		//jhhan
		MakerOptionData.nWaitSaveDSCSub		= DEFAULT_WAIT_SAVEDSC;
		MakerOptionData.nWaitSaveDSCSel		= 0;
		MakerOptionData.bViewInfo			= FALSE;
		MakerOptionData.bTestMode			= FALSE;
		MakerOptionData.bMakeAndAotuPlay	= TRUE;
		MakerOptionData.bInsertLogo			= FALSE;
		MakerOptionData.b3DLogo				= FALSE;
		MakerOptionData.bLogoZoom			= FALSE;
		MakerOptionData.bInsertCameraID		= FALSE;
		MakerOptionData.nMovieSaveLocation	= 1;
		MakerOptionData.nSyncTimeMargin		= DEFAULT_SYNG_TIME_MARGIN;
		MakerOptionData.nSensorOnTime		= DEFAULT_SYNG_TIME_MARGIN;
		MakerOptionData.nTemplateEndMargin = 0;
		MakerOptionData.nTemplateStartMargin = 0;
		MakerOptionData.nTemplateStartSleep = 0;
		MakerOptionData.nTemplateEndSleep = 0;
		MakerOptionData.bTimeLinePreview	= TRUE;
		MakerOptionData.bEnableBaseBall		= FALSE;
		MakerOptionData.bReverseMovie		= FALSE;
		MakerOptionData.bGPUMakeFile		= FALSE;
		MakerOptionData.bEnableVMCC			= FALSE;
		MakerOptionData.bEnableCopy			= FALSE;
		MakerOptionData.bUHDtoFHD			= FALSE;
		MakerOptionData.bSaveImg			= FALSE;
		MakerOptionData.bCheckValid			= TRUE;
		MakerOptionData.bInsertWhiteBalance = FALSE;		
		MakerOptionData.bTemplatePoint		= FALSE;
		MakerOptionData.bTemplateStabilization= FALSE;
		MakerOptionData.bTemplateModify		= FALSE;
		MakerOptionData.bAJALogo			= FALSE;
		MakerOptionData.bAJAReplay			= FALSE;
		//-- Add picture coutdown
		MakerOptionData.nCountdownLastIP	= 110;
		MakerOptionData.nBaseBallInning		= 0;
		MakerOptionData.nAJASapleCnt = 33;
		MakerOptionData.bColorRevision = FALSE;
		MakerOptionData.bGPUSkip = FALSE;
		/*strCtrl1 = _T("");
		strCtrl2 = _T("");
		strCtrl3 = _T("");
		strCtrl4 = _T("");*/
		//jhhan 170327
		MakerOptionData.nCheckVaildTime = 7000;
		MakerOptionData.nDivFrame = 15;

		//joonho.kim 170628
		MakerOptionData.nPCSyncTime = 2;
		MakerOptionData.nDSCSyncTime = 8;
		MakerOptionData.nDSCSyncWaitTime = 10000;

		//wgkim 170727
		MakerOptionData.bLightWeight = FALSE;

		//hjcho 170830
		MakerOptionData.bDirectMux	 = FALSE;

		//wgkim 171127
		MakerOptionData.bEncodingForSmartPhones = FALSE;

		MakerOptionData.nFrameZoomRatio	= 100;

		//wgkim 180119
		MakerOptionData.nTemplateTargetOutPercent = 0;
		MakerOptionData.bRecordFileSplit = FALSE;

		MakerOptionData.bAJANetwork	= FALSE;
		MakerOptionData.strAJAIP	= _T("0.0.0.0");

		MakerOptionData.bGIFMaking	= FALSE;
		MakerOptionData.nGIFQuality = 0;
		MakerOptionData.nGIFSize = 0;

		//wgkim 180629
		MakerOptionData.nSetFrameRateIndex = 0;
		MakerOptionData.bRefereeRead = FALSE;

		MakerOptionData.bUseReSync = FALSE;

		MakerOptionData.bAutoAdjustPositionTracking = FALSE;
		MakerOptionData.bAutoAdjustKZone= FALSE;
		MakerOptionData.nRTSPWaitTime = 60000;
	}	
	else
	{
		//- 2011-10-17 hongsu.jung
		//- Change Code For Easy Additional	
		strPath[ESM_OPT_PATH_HOME		]=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_HOME	);
		strPath[ESM_OPT_PATH_FILESVR	]=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_FILESVR	);
		strPath[ESM_OPT_PATH_LOG		]=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_LOG		);
		strPath[ESM_OPT_PATH_CONF		]=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_CONFIG	);			
		strPath[ESM_OPT_PATH_WORK		]=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_WORK	);	//-- Movie
		strPath[ESM_OPT_PATH_RECORD		]=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_RECORD	);	//-- Picture	
		strPath[ESM_OPT_PATH_OUTPUT		]=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_OUTPUT	);	//-- 
		strPath[ESM_OPT_PATH_SERVERRAM	]=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_SERVERRAM);	//-- 
		strPath[ESM_OPT_PATH_CLIENTRAM	]=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_CLIENTRAM);	//--  
		strPath[ESM_OPT_PATH_SAVE_IMG	]=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_SAVEIMG);	//--  

		strBackup[ESM_BACKUP_STRING_SRCFOLDER	]=	ini.GetString(INFO_SECTION_BACKUP , INFO_BACKUP_STRING_SRCFOLDER	);
		strBackup[ESM_BACKUP_STRING_DSTFOLDER	]=	ini.GetString(INFO_SECTION_BACKUP , INFO_BACKUP_STRING_DSTFOLDER	);
		strBackup[ESM_BACKUP_STRING_FTPHOST		]=	ini.GetString(INFO_SECTION_BACKUP , INFO_BACKUP_STRING_FTPHOST		);
		strBackup[ESM_BACKUP_STRING_FTPID		]=	ini.GetString(INFO_SECTION_BACKUP , INFO_BACKUP_STRING_FTPID	);			
		strBackup[ESM_BACKUP_STRING_FTPPW		]=	ini.GetString(INFO_SECTION_BACKUP , INFO_BACKUP_STRING_FTPPW	);	//-- Movie
		
		//jhhan
		bDefault = ini.GetBoolean(INFO_SECTION_PATH, INFO_PATH_DEFAULT);
		
		strFilename		=	ini.GetString(INFO_SECTION_LOG , INFO_LOG_LOGNAME, _T("4DMaker.log"));
		strVerbosity	=	ini.GetString(INFO_SECTION_LOG , INFO_LOG_VERBOSITY, _T("5"));
		strLimitday		=	ini.GetString(INFO_SECTION_LOG , INFO_LOG_LIMITDAY, _T("5"));
		bTraceTime		=	ini.GetBoolean(INFO_SECTION_LOG , INFO_LOG_TRACETIME_Enable, 1);

		MakerOptionData.bMakeServer			= ini.GetBoolean(INFO_SECTION_RC , INFO_RC_SERVERMAKE);
		MakerOptionData.bRCMode				= ini.GetBoolean(INFO_SECTION_RC , INFO_RC_MODE);
		MakerOptionData.nServerMode			= ini.GetInt(INFO_SECTION_RC, INFO_RC_SERVERMODE, 0);
		strPort								= ini.GetString(INFO_SECTION_RC , INFO_RC_PORT);
		MakerOptionData.nWaitSaveDSC		= ini.GetInt(INFO_SECTION_RC , INFO_RC_WAIT_TIME_DSC, DEFAULT_WAIT_SAVEDSC);
		//jhhan
		MakerOptionData.nWaitSaveDSCSub		= ini.GetInt(INFO_SECTION_RC , INFO_RC_WAIT_TIME_DSC_SUB, DEFAULT_WAIT_SAVEDSC);
		MakerOptionData.nWaitSaveDSCSel		= ini.GetInt(INFO_SECTION_RC , INFO_RC_WAIT_TIME_DSC_SEL, 0);
		MakerOptionData.bViewInfo			= ini.GetInt(INFO_SECTION_RC , INFO_RC_VIEW_INFO, FALSE);
		MakerOptionData.bTestMode			= ini.GetInt(INFO_SECTION_RC , INFO_RC_TEST_MODE, FALSE);
		MakerOptionData.bMakeAndAotuPlay	= ini.GetInt(INFO_SECTION_RC , INFO_RC_MAKEANDAUTOPLAY, TRUE);
		MakerOptionData.bInsertLogo			= ini.GetInt(INFO_SECTION_RC , INFO_RC_INSERTLOGO, FALSE);
		MakerOptionData.b3DLogo				= ini.GetInt(INFO_SECTION_RC , INFO_RC_3DLOGO, FALSE);
		MakerOptionData.bLogoZoom			= ini.GetInt(INFO_SECTION_RC , INFO_RC_LOGOZOOM, FALSE);
		MakerOptionData.bInsertPrism		= ini.GetInt(INFO_SECTION_RC , INFO_RC_INSERT_KZONE);
		MakerOptionData.bSyncSkip			= ini.GetInt(INFO_SECTION_RC,	INFO_RC_SYNCSKIP);
		MakerOptionData.nPrismValue			= ini.GetInt(INFO_SECTION_RC,	INFO_RC_KZONEVALUE);
		MakerOptionData.nPrismValue2		= ini.GetInt(INFO_SECTION_RC,	INFO_RC_KZONEVALUE1);
		MakerOptionData.bInsertWhiteBalance = ini.GetInt(INFO_SECTION_RC,   INFO_RC_INSERT_WHITEBAL);
		//MakerOptionData.bInsertCameraID		= ini.GetInt(INFO_SECTION_RC , INFO_RC_INSERTCAMERAID, FALSE);
		MakerOptionData.bInsertCameraID		= FALSE;
		MakerOptionData.bTimeLinePreview	= ini.GetInt(INFO_SECTION_RC , INFO_RC_TIMELINEPREVIEW, TRUE);
		MakerOptionData.bEnableBaseBall		= ini.GetInt(INFO_SECTION_RC , INFO_RC_ENABLE_BASEBALL, FALSE);
		MakerOptionData.bReverseMovie		= ini.GetInt(INFO_SECTION_RC , INFO_RC_REVERSE_MOVIE, FALSE);
		MakerOptionData.bGPUMakeFile		= ini.GetInt(INFO_SECTION_RC , INFO_RC_GPU_MAKE_FILE, FALSE);
		MakerOptionData.bGPUMakeFile		= ini.GetInt(INFO_SECTION_RC , INFO_RC_GPU_MAKE_FILE, FALSE);
		MakerOptionData.bRepeatMovie		= ini.GetInt(INFO_SECTION_RC , INFO_RC_REPEAT_MOVIE, FALSE);
		MakerOptionData.bTemplatePoint		= ini.GetInt(INFO_SECTION_RC , INFO_RC_TEMPLATE_POINT, FALSE);
		MakerOptionData.bTemplateStabilization= ini.GetInt(INFO_SECTION_RC , INFO_RC_TEMPLATE_STABILIZATION, FALSE);
		MakerOptionData.bTemplateModify		= ini.GetInt(INFO_SECTION_RC , INFO_RC_TEMPLATE_MODIFY, FALSE);		
		MakerOptionData.bEnableVMCC			= ini.GetInt(INFO_SECTION_RC , INFO_RC_ENABLE_VMCC, FALSE);
		MakerOptionData.bEnableCopy			= ini.GetInt(INFO_SECTION_RC , INFO_RC_ENABLE_4DPCOPY, FALSE);
		MakerOptionData.nBaseBallInning		= ini.GetInt(INFO_SECTION_RC , INFO_RC_BASEBALL_INNING, FALSE);
		MakerOptionData.bUHDtoFHD			= ini.GetInt(INFO_SECTION_RC , INFO_RC_UHD_TO_FHD, FALSE);
		MakerOptionData.bSaveImg			= ini.GetInt(INFO_SECTION_RC , INFO_RC_ENABLE_SAVE_IMG, FALSE);
		MakerOptionData.bCheckValid			= ini.GetInt(INFO_SECTION_RC , INFO_RC_ENABLE_CHECK_VALID, FALSE);
		MakerOptionData.nDeleteCnt			= ini.GetInt(INFO_SECTION_RC , INFO_RC_DELETE_CNT, 30);
		MakerOptionData.nRepeatMovieCnt		= ini.GetInt(INFO_SECTION_RC , INFO_RC_REPEAT_CNT, 0);
		MakerOptionData.nDSCSyncCnt			= ini.GetInt(INFO_SECTION_RC , INFO_RC_DSC_SYNC_CNT, 400);
		MakerOptionData.nMovieSaveLocation	= ini.GetInt(INFO_SECTION_RC , INFO_RC_MOVIESAVELOCATION	, TRUE);
		MakerOptionData.nSyncTimeMargin		= ini.GetInt(INFO_SECTION_RC , INFO_RC_SYNC_TIME_MARGIN, DEFAULT_SYNG_TIME_MARGIN);
		MakerOptionData.nSensorOnTime		= ini.GetInt(INFO_SECTION_RC , INFO_RC_SENSOR_ON_TIME, DEFAULT_SENSORON_TIME);
		MakerOptionData.nTemplateStartMargin= ini.GetInt(INFO_SECTION_RC , INFO_RC_TEMPLATESTARTMARGIN, 0);
		MakerOptionData.nTemplateEndMargin	= ini.GetInt(INFO_SECTION_RC , INFO_RC_TEMPLATEENDMARGIN, 0);
		MakerOptionData.nTemplateStartSleep	= ini.GetInt(INFO_SECTION_RC , INFO_RC_TEMPLATESTARTSLEEP, 0);
		MakerOptionData.nTemplateEndSleep	= ini.GetInt(INFO_SECTION_RC , INFO_RC_TEMPLATEENDSLEEP, 0);
		MakerOptionData.bAJALogo			= ini.GetInt(INFO_SECTION_RC , INFO_RC_AJALOGO , 0);
		MakerOptionData.bAJAReplay			= ini.GetInt(INFO_SECTION_RC , INFO_RC_AJAREPLAY , 0);
		MakerOptionData.bAJAOnOff			= ini.GetInt(INFO_SECTION_RC , INFO_RC_AJAONOFF , 0);
		MakerOptionData.nAJASapleCnt		= ini.GetInt(INFO_SECTION_RC , INFO_RC_AJASAMPLE,33);
		MakerOptionData.bGetRamDiskSize		= ini.GetInt(INFO_SECTION_RC , INFO_RC_GETRAMDISKSIZE , 0);
		MakerOptionData.bColorRevision		= ini.GetInt(INFO_SECTION_RC,INFO_RC_COLORREVISION,0);
		MakerOptionData.bPrnColorInfo		= ini.GetInt(INFO_SECTION_RC,INFO_RC_PRINTCOLORINFO,0);
		MakerOptionData.bGPUSkip			= ini.GetInt(INFO_SECTION_RC,INFO_RC_GPUSKIP,0);
		MakerOptionData.bAutoDetect			= ini.GetInt(INFO_SECTION_RC,INFO_RC_AUTODETECT);
		//-- 2015-02-15 cygil@esmlab.com
		//-- Add picture coutdown
		MakerOptionData.nCountdownLastIP	= ini.GetInt(INFO_SECTION_RC , INFO_RC_COUNTDOWNLASTIP, 110);

		//jhhan 170327
		MakerOptionData.nCheckVaildTime		= ini.GetInt(INFO_SECTION_RC, INFO_RC_CHECK_VALID_TIME, 7000);
		MakerOptionData.nDivFrame		= ini.GetInt(INFO_SECTION_RC, INFO_RC_DIV_FRAME, 15);

		//joonho.kim 170628
		MakerOptionData.nPCSyncTime			= ini.GetInt(INFO_SECTION_RC, INFO_RC_PC_SYNC_TIME, 2);
		MakerOptionData.nDSCSyncTime		= ini.GetInt(INFO_SECTION_RC, INFO_RC_DSC_SYNC_TIME, 8);
		MakerOptionData.nDSCSyncWaitTime	= ini.GetInt(INFO_SECTION_RC, INFO_RC_DSC_WAIT_TIME, 10000);

		//wgkim 170727
		MakerOptionData.bLightWeight		= ini.GetInt(INFO_SECTION_RC , INFO_RC_LIGHT_WEIGHT, FALSE);

		//hjcho 170830
		MakerOptionData.bDirectMux	 = ini.GetInt(INFO_SECTION_RC , INFO_RC_DIRECT_MUX, FALSE);

		//hjcho 170904
		MakerOptionData.n4DPMethod	= ini.GetInt(INFO_SECTION_RC,INFO_RC_4DPMETHOD,1);
		
		MakerOptionData.bRemote = ini.GetBoolean(INFO_SECTION_RC, INFO_RC_REMOTE_USE , FALSE);
		MakerOptionData.strRemote = ini.GetString(INFO_SECTION_RC, INFO_RC_REMOTE_IP,_T("127.0.0.1"));
		MakerOptionData.bRemoteSkip = ini.GetBoolean(INFO_SECTION_RC, INFO_RC_REMOTE_SKIP , FALSE);
		
		//hjcho 171113
		MakerOptionData.bHorizonAdj = ini.GetBoolean(INFO_SECTION_RC, INFO_RC_HORIZON_ADJUST , FALSE);

		//wgkim 171127
		MakerOptionData.bEncodingForSmartPhones = ini.GetBoolean(INFO_SECTION_RC, INFO_RC_ENCODING_FOR_SMARTPHONES, FALSE);
		//180308 Ini 값  //jhhan 180307 TRUE 고정
		MakerOptionData.bRecordFileSplit = ini.GetBoolean(INFO_SECTION_RC, INFO_RC_RECORD_FILE_SPLIT, FALSE);

		//jhhan 171129
		MakerOptionData.bProcessorShare = ini.GetBoolean(INFO_SECTION_RC, INFO_RC_PROCESSOR_SHARE, FALSE);

		MakerOptionData.nFuncKey = ini.GetInt(INFO_SECTION_RC, INFO_RC_FUNC_KEY, VK_F1);
		//hjcho 171210
		MakerOptionData.nFrameZoomRatio	= ini.GetInt(INFO_SECTION_RC, INFO_RC_FRAMEZOOMRATIO, 100);

		//wgkim 180119
		MakerOptionData.nTemplateTargetOutPercent = ini.GetInt(INFO_SECTION_RC, INFO_SECTION_RC_TEMPLATE_TARGETOUT_PERCENT, 0);
		
		//jhhan 180411
		MakerOptionData.bFile2Sec = ini.GetBoolean(INFO_SECTION_RC, INFO_RC_FILE_2SEC, FALSE);

		//hjcho 180508
		MakerOptionData.bAJANetwork = ini.GetInt(INFO_SECTION_RC,INFO_RC_AJANETWORK,0);
		MakerOptionData.strAJAIP  = ini.GetString(INFO_SECTION_RC,INFO_RC_AJANETWORK_IP,_T("192.168.0.70"));
		
		//hjcho 180629
		MakerOptionData.bGIFMaking = ini.GetInt(INFO_SECTION_RC,INFO_RC_GIF_MAKING,0);
		MakerOptionData.nGIFQuality= ini.GetInt(INFO_SECTION_RC,INFO_RC_GIF_QUAILITY,0);
		MakerOptionData.nGIFSize	= ini.GetInt(INFO_SECTION_RC,INFO_RC_GIF_SIZE,0);

		//wgkim 180629
		MakerOptionData.nSetFrameRateIndex = ini.GetInt(INFO_SECTION_RC,INFO_RC_SET_FRAMERATE,0);

		//hjcho 180718
		MakerOptionData.bRefereeRead = ini.GetInt(INFO_SECTION_RC,INFO_RC_REFEREEREAD,0);
		MakerOptionData.bUseReSync = ini.GetInt(INFO_SECTION_RC,INFO_RC_USE_RESYNC,0);

		MakerOptionData.nResyncRecWaitTime = ini.GetInt(INFO_SECTION_RC,INFO_RC_RESYNC_REC_WAIT_TIME,30000);
		
		//jhhan 180828
		MakerOptionData.bAutoDelete = ini.GetInt(INFO_SECTION_RC, INFO_RC_AUTO_DELETE, 0);
		MakerOptionData.nStoredTime = ini.GetInt(INFO_SECTION_RC, INFO_RC_STORED_TIME, 5);

		//jhhan 180918
		MakerOptionData.bTemplateEditor = ini.GetInt(INFO_SECTION_RC, INFO_RC_TEMPLATE_EDITOR, 0);

		//wgkim 180921
		MakerOptionData.bAutoAdjustPositionTracking = ini.GetInt(INFO_SECTION_RC, INFO_RC_AUTOADJUST_POSITION_TRACKING, FALSE);
		MakerOptionData.bAutoAdjustKZone= ini.GetInt(INFO_SECTION_RC, INFO_RC_AUTOADJUST_KZONE, FALSE);

		//hjcho 180927 
		MakerOptionData.bRTSP = ini.GetInt(INFO_SECTION_RC,INFO_RC_RTSP,FALSE);
		//jhhan 181029
		MakerOptionData.b4DAP = ini.GetBoolean(INFO_SECTION_RC,INFO_RC_4DAP,FALSE);

		//wgkim 181119
		MakerOptionData.nSetTemplateModifyMode = ini.GetInt(INFO_SECTION_RC,INFO_RC_SET_TEMPLATE_MODIFY, 0);

		//hjcho 190103
		MakerOptionData.nSmartPhoneBitrate = ini.GetInt(INFO_SECTION_RC,INFO_RC_BITRATE_FOR_ENCODING,0);
		
		//hjcho 190319
		MakerOptionData.nRTSPWaitTime = ini.GetInt(INFO_SECTION_RC,INFO_RC_RTSP_WAITTIME,60000);
		//Ceremony
		CString strDbAddrIP;
		CString strDbId;
		CString strDbPasswd;
		CString strDbName;
		CString strImagePath;
		CString strMoviePath;

		CeremonyData.bCeremonyUse			=	ini.GetInt(INFO_CEREMONY , INFO_CEREMONY_USE, 0);
		CeremonyData.bPutImage				=	ini.GetInt(INFO_CEREMONY , INFO_CEREMONY_PUTIMAGE, 0);
		CeremonyData.bCeremonyRatio			=	ini.GetInt(INFO_CEREMONY , INFO_CEREMONY_RATIO, 0);
		CeremonyData.strDbAddrIP			=	ini.GetString(INFO_CEREMONY , INFO_CEREMONY_DBADDRIP);
		CeremonyData.strDbId				=	ini.GetString(INFO_CEREMONY , INFO_CEREMONY_DBID);
		CeremonyData.strDbPasswd			=	ini.GetString(INFO_CEREMONY , INFO_CEREMONY_DBPW);
		CeremonyData.strDbName				=	ini.GetString(INFO_CEREMONY , INFO_CEREMONY_DBNAME);
		CeremonyData.strImagePath			=	ini.GetString(INFO_CEREMONY , INFO_CEREMONY_DBIMAGEPATH);
		CeremonyData.strMoviePath			=	ini.GetString(INFO_CEREMONY , INFO_CEREMONY_DBMOVIEPATH);
		CeremonyData.strWidth				=	ini.GetString(INFO_CEREMONY	,	INFO_CEREMONY_WIDTH);
		CeremonyData.strHeight				=	ini.GetString(INFO_CEREMONY	,	INFO_CEREMONY_HEIGHT);
		CeremonyData.bStillImage			=	ini.GetInt(INFO_CEREMONY,	INFO_CEREMONY_STILLIMG);
		CeremonyData.bUseCanonSelphy		=	ini.GetInt(INFO_CEREMONY,	INFO_CEREMONY_CANON_SELPHY);
		
		ManagementData.strSaveFilePath		=	ini.GetString(INFO_MANAGEMENT , INFO_MANAGEMENT_SAVEFILEPATH);
		ManagementData.strSelectFilePath	=	ini.GetString(INFO_MANAGEMENT , INFO_MANAGEMENT_SELFILEPATH);

		AdjustMovie.strTarPath				=	ini.GetString(INFO_SECTION_ADJUST_MOVIE, INFO_ADJ_TARGETLENTH);

		nFlowmoTime		=	ini.GetInt(INFO_SECTION_MOVIE, INFO_ADJ_FLOW_T);
		nAutoBrightness = 	ini.GetInt(INFO_SECTION_MOVIE, INFO_ADJ_AUTOBRIGHTNESS);
		bClockwise		=	ini.GetInt(INFO_SECTION_MOVIE, INFO_ADJ_ORDER_RTL);
		bClockreverse	=	ini.GetInt(INFO_SECTION_MOVIE, INFO_ADJ_ORDER_LTR);
		dCamZoom		=	ini.GetInt(INFO_SECTION_MOVIE, INFO_ADJ_CAMERAZOOM);

		nThreshold		=	ini.GetInt(INFO_SECTION_ADJUST, INFO_ADJ_TH);
		nPointGapMin	=	ini.GetInt(INFO_SECTION_ADJUST, INFO_ADJ_POINTGAPMIN);
		nMinWidth		=	ini.GetInt(INFO_SECTION_ADJUST, INFO_ADJ_MIN_WIDTH);
		nMinHeight		=	ini.GetInt(INFO_SECTION_ADJUST, INFO_ADJ_MIN_HEIGHT);
		nMaxWidth		=	ini.GetInt(INFO_SECTION_ADJUST, INFO_ADJ_MAX_WIDTH);
		nMaxHeight		=	ini.GetInt(INFO_SECTION_ADJUST, INFO_ADJ_MAX_HEIGHT);
		nTargetLenth	=	ini.GetInt(INFO_SECTION_ADJUST, INFO_ADJ_TARGETLENTH);

		/*strCtrl1		=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_CTRL1);
		strCtrl2		=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_CTRL2);
		strCtrl3		=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_CTRL3);
		strCtrl4		=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_CTRL4);
		//CMiLRe 20151013 Template Load INI File
		strCtrl5		=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_CTRL5);
		strCtrl6		=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_CTRL6);
		strCtrl7		=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_CTRL7);
		strCtrl8		=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_CTRL8);
		strCtrl9		=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_CTRL9);*/

		strCtrl[0][0]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE1_CTRL1);
		strCtrl[0][1]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE1_CTRL2);
		strCtrl[0][2]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE1_CTRL3);
		strCtrl[0][3]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE1_CTRL4);
		strCtrl[0][4]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE1_CTRL5);
		strCtrl[0][5]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE1_CTRL6);
		strCtrl[0][6]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE1_CTRL7);
		strCtrl[0][7]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE1_CTRL8);
		strCtrl[0][8]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE1_CTRL9);

		strCtrl[1][0]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE2_CTRL1);
		strCtrl[1][1]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE2_CTRL2);
		strCtrl[1][2]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE2_CTRL3);
		strCtrl[1][3]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE2_CTRL4);
		strCtrl[1][4]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE2_CTRL5);
		strCtrl[1][5]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE2_CTRL6);
		strCtrl[1][6]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE2_CTRL7);
		strCtrl[1][7]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE2_CTRL8);
		strCtrl[1][8]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE2_CTRL9);

		strCtrl[2][0]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE3_CTRL1);
		strCtrl[2][1]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE3_CTRL2);
		strCtrl[2][2]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE3_CTRL3);
		strCtrl[2][3]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE3_CTRL4);
		strCtrl[2][4]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE3_CTRL5);
		strCtrl[2][5]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE3_CTRL6);
		strCtrl[2][6]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE3_CTRL7);
		strCtrl[2][7]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE3_CTRL8);
		strCtrl[2][8]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE3_CTRL9);

		strCtrl[3][0]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE4_CTRL1);
		strCtrl[3][1]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE4_CTRL2);
		strCtrl[3][2]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE4_CTRL3);
		strCtrl[3][3]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE4_CTRL4);
		strCtrl[3][4]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE4_CTRL5);
		strCtrl[3][5]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE4_CTRL6);
		strCtrl[3][6]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE4_CTRL7);
		strCtrl[3][7]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE4_CTRL8);
		strCtrl[3][8]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE4_CTRL9);

		strCtrl[4][0]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE5_CTRL1);
		strCtrl[4][1]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE5_CTRL2);
		strCtrl[4][2]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE5_CTRL3);
		strCtrl[4][3]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE5_CTRL4);
		strCtrl[4][4]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE5_CTRL5);
		strCtrl[4][5]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE5_CTRL6);
		strCtrl[4][6]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE5_CTRL7);
		strCtrl[4][7]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE5_CTRL8);
		strCtrl[4][8]	=	ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE5_CTRL9);

		strPage			=   ini.GetString(INFO_TEMPLATE, INFO_TEMPLATE_PAGE);

// 		nCamCount = ini.GetSectionCount(INFO_CAM_INFO);
// 		nCamCount = nCamCount / 2;
// 		CString strCamSectionName;
// 		CString strCamSectionIP;
// 		for (int i=0; i<nCamCount; i++)
// 		{
// 			strCamSectionName.Format(_T("%s_%d"), INFO_CAM_NAME, i);
// 			strCamInfo[i]	=	ini.GetString(INFO_CAM_INFO, strCamSectionName);
// 			strCamSectionIP.Format(_T("%s_%d"), INFO_CAM_IP, i);
// 			strCamName[i]	=	ini.GetString(INFO_CAM_INFO, strCamSectionIP);
// 		}

		if(!nFlowmoTime)		nFlowmoTime		= ADJ_BASIC_VAL_FLOWMO_TIME;
		if(!nAutoBrightness)	nAutoBrightness = ADJ_BASIC_VAL_AUTOBRIGHTNESS;
		if(!dCamZoom)			dCamZoom		= ADJ_BASIC_VAL_CAMERAZOOM;

		if(!nThreshold)			nThreshold		= ADJ_BASIC_VAL_THRESHOLD;
		if(!nPointGapMin)		nPointGapMin	= ADJ_BASIC_VAL_POINTGAPMIN;
		if(!nMinWidth)			nMinWidth		= ADJ_BASIC_VAL_MIN_WIDTH;
		if(!nMinHeight)			nMinHeight		= ADJ_BASIC_VAL_MIN_HEIGHT;	
		if(!nMaxWidth)			nMaxWidth		= ADJ_BASIC_VAL_MAX_WIDTH;
		if(!nMaxHeight)			nMaxHeight		= ADJ_BASIC_VAL_MAX_HEIGHT;
		if(!nTargetLenth)		nTargetLenth	= ADJ_BASIC_VAL_TARGETLENTH;

		m_CamInfo.strView = ini.GetString(INFO_SECTION_CAM, INFO_CAM_VIEW);
		ESMSetViewJump(m_CamInfo.strView);
	}

	if( MakerOptionData.bRCMode == 1)
		ESMSetExecuteMode(TRUE);

	MakerOptionData.nPort		= _ttoi(strPort);

	LoadPath(&strPath[0]);
	LoadBackup(&strBackup[0]);
	//jhhan
	LoadPathDefault(bDefault);

	LoadLog(strFilename, strVerbosity, strLimitday, bTraceTime);
	Load4DOpt(MakerOptionData);
	LoadAdj(nThreshold,nFlowmoTime,bClockwise,bClockreverse,
			dCamZoom, nAutoBrightness, nMinWidth, nMinHeight, nMaxWidth, nMaxHeight, nPointGapMin, nTargetLenth);
	//LoadCamInfo(&strCamInfo[0], &strCamName[0], nCamCount);
	//CMiLRe 20151013 Template Load INI File
	if( MakerOptionData.bRCMode == 0)
		//2016/07/11 Lee SangA LoadTemplate 대용
	{
		//LoadTemplate(strCtrl1,strCtrl2,strCtrl3,strCtrl4, strCtrl5, strCtrl6, strCtrl7, strCtrl8, strCtrl9, strPage);
		LoadTemplate2(strCtrl, strPage);
	}
	LoadCeremony(CeremonyData);
	LoadManagement(ManagementData);
	LoadAdjMovie(AdjustMovie);

	//-- Create Directory
	CreateDir();


	//-- 2013-10-19 hongsu@esmlab.com
	//-- Create Output 2D/3D
	CString strFolder;
	strFolder.Format(_T("%s\\2D"),ESMGetPath(ESM_PATH_OUTPUT));
	CreateDirectory(strFolder, NULL);
	strFolder.Format(_T("%s\\3D"),ESMGetPath(ESM_PATH_OUTPUT));
	CreateDirectory(strFolder, NULL);
}

//------------------------------------------------------------------------------ 
//! @brief		LoadPath
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CESMOption::LoadPath(CString* pStrPath)
{
	for( int i = 0 ; i < ESM_OPT_PATH_ALL; i ++)
		m_Path.strPath[i] = *pStrPath++;
}

void CESMOption::LoadBackup(CString* pStrString)
{
	for( int i = 0 ; i < ESM_BACKUP_STRING_ALL; i ++)
		m_BackupString.strString[i] = *pStrString++;
}

//jhhan
void CESMOption::LoadPathDefault(BOOL bDefault)
{
	m_Path.bDefault = bDefault;
}

//------------------------------------------------------------------------------ 
//! @brief		LoadCamInfo
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
void CESMOption::LoadCamInfo(CString* pStrCamInfo, CString* pStrCamIP, int nCamCount)
{
	m_CamInfo.nCamCount = nCamCount;
	for( int i = 0 ; i < nCamCount; i ++){
		m_CamInfo.strCamIP[i] = *pStrCamIP++;
		m_CamInfo.strCamID[i] = *pStrCamInfo++;
	}
}

/*
//------------------------------------------------------------------------------ 
//! @brief		LoadCamInfo
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------ 
//CMiLRe 20151013 Template Load INI File
void CESMOption::LoadTemplate(CString strCtrl1, CString strCtrl2, CString strCtrl3, CString strCtrl4, CString strCtrl5, CString strCtrl6, CString strCtrl7, CString strCtrl8, CString strCtrl9, CString strPage )
{

	m_TemplateOpt.strTemplate1 = strCtrl1;
	m_TemplateOpt.strTemplate2 = strCtrl2;
	m_TemplateOpt.strTemplate3 = strCtrl3;
	m_TemplateOpt.strTemplate4 = strCtrl4;
	//CMiLRe 20151013 Template Load INI File
	m_TemplateOpt.strTemplate5 = strCtrl5;
	m_TemplateOpt.strTemplate6 = strCtrl6;
	m_TemplateOpt.strTemplate7 = strCtrl7;
	m_TemplateOpt.strTemplate8 = strCtrl8;
	m_TemplateOpt.strTemplate9 = strCtrl9;
	//CMiLRe 20151014 Template Save INI File
	m_TemplateOpt.strSelectPage = strPage;
	m_TemplateOpt.intSelectPage = _ttoi(strPage);
	//Lee SangA 2016/07/08 Template load page number

	ResetLoadTemplate();
	LoadTemplateFormFile();

}*/

//2016/07/12 Lee SangA Template Load INI File. LoadTemplate대용
void CESMOption::LoadTemplate2(CString (*strCtrl)[9], CString strPage)
{

	m_TemplateOpt.strSelectPage = strPage;
	m_TemplateOpt.intSelectPage = _ttoi(strPage);

	for(int i=0; i<5; i++)
	{
		m_TemplateOpt.strTemplate[i][0] = strCtrl[i][0];
		m_TemplateOpt.strTemplate[i][1] = strCtrl[i][1];
		m_TemplateOpt.strTemplate[i][2] = strCtrl[i][2];
		m_TemplateOpt.strTemplate[i][3] = strCtrl[i][3];
		m_TemplateOpt.strTemplate[i][4] = strCtrl[i][4];
		m_TemplateOpt.strTemplate[i][5] = strCtrl[i][5];
		m_TemplateOpt.strTemplate[i][6] = strCtrl[i][6];
		m_TemplateOpt.strTemplate[i][7] = strCtrl[i][7];
		m_TemplateOpt.strTemplate[i][8] = strCtrl[i][8];
	}

	ResetLoadTemplate();
	LoadTemplateFormFile();

}


//2016/07/13 Lee SangA Template Load INI File. LoadTemplateFormFile 대용
void CESMOption::LoadTemplateFormFile()
{
	CESMFileOperation fileOperation;
	
	for(int i=0; i<5; i++)
		for(int j=0; j<9; j++)
			if(fileOperation.IsFileExist(m_TemplateOpt.strTemplate[i][j]))
			{
				LoadTemplateFile(m_TemplateOpt.strTemplate[i][j], (i+1)*10 + (j+1), m_TemplateOpt.bRecovery);
				//F1~F5   : 1   2   3   4   5   6   7   8   9 
				//F1(i=0) : 11, 12, 13, 14, 15, 16, 17, 18, 19
				//F2(i=1) : 21, 22, 23, 24, 25, 26, 27, 28, 29
				//F3(i=2) : 31, 32, 33, 34, 35, 36, 37, 38, 39
				//F4(i=3) : 41, 42, 43, 44, 45, 46, 47, 48, 49
				//F5(i=4) : 51, 52, 53, 54, 55, 56, 57, 58, 59
			}
}

//------------------------------------------------------------------------------ 
//! @brief		LoadLog
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------
void CESMOption::LoadLog(CString strLogName,CString strVerbosity,CString strLimitday, BOOL bTraceTime)
{
	m_Log.strLogName	= strLogName;
	m_Log.nVerbosity	= _ttoi(strVerbosity);
	m_Log.nLimitday		= _ttoi(strLimitday);
	//-- CHANGE TREE ITEM
	//-- 2009-06-09
	m_Log.bTraceTime	= bTraceTime;
}

//------------------------------------------------------------------------------ 
//! @brief		Load4DOpt
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------
void CESMOption::Load4DOpt(ESM4DMaker MakerOptionData)
{
	m_4DOpt = MakerOptionData;
}
void CESMOption::LoadCeremony(ESMCeremony CeremonyData)
{
	m_Ceremony = CeremonyData;
}

void CESMOption::LoadAdjMovie(ESMAdjustMovie& AdjustMovie)
{
	m_AdjustMovie.strTarPath = AdjustMovie.strTarPath;
}

void CESMOption::LoadManagement(ESMManagement ManagementData)
{
	m_menagement = ManagementData;
}

//------------------------------------------------------------------------------ 
//! @brief		Load4DOpt
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------
void CESMOption::LoadAdj(int nTH, int nFlowmoTime, BOOL bClockwise, BOOL bClockreverse,
						  int nCamZoom, int nAutoBrightness, int nMinWidth, int nMinHeight, int nMaxWidth, int nMaxHeight, int nPointGapMin, int nTargetLenth)
{
	m_Adjust.nFlowmoTime		= nFlowmoTime;
	m_Adjust.nAutoBrightness	= nAutoBrightness;
	m_Adjust.bClockwise			= bClockwise;
	m_Adjust.bClockreverse		= bClockreverse;
	m_Adjust.nCamZoom			= nCamZoom;

	m_Adjust.nThresdhold		= nTH;
	m_Adjust.nPointGapMin		= nPointGapMin;
	m_Adjust.nMinWidth			= nMinWidth;
	m_Adjust.nMinHeight			= nMinHeight;
	m_Adjust.nMaxWidth			= nMaxWidth;
	m_Adjust.nMaxHeight			= nMaxHeight;
	m_Adjust.nTargetLenth			= nTargetLenth;

}

//------------------------------------------------------------------------------ 
//! @brief		CreateDir
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------
void CESMOption::CreateDir()
{
	//-- 2009-05-20
	for(int i = 0 ; i < ESM_PATH_ALL ; i ++)
	{
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
		{
			if(ESM_PATH_FILESERVER != i)
				ESMCreateAllDirectories(ESMGetPath(i));		
		}else
			ESMCreateAllDirectories(ESMGetPath(i));		
	}
}

//=============================================================
// CTestGuaranteeApp Functions
//=============================================================
#include <winsock2.h>

//------------------------------------------------------------------------------ 
//! @brief		GetNetworkInfo
//! @date		
//! @attention	none
//! @note	 	none
//------------------------------------------------------------------------------
void CESMOption::GetNetworkInfo()
{
	CString strIPInfo = _T("");

	// Add 'ws2_32.lib' to your linker options
	WSADATA WSAData;
	// Initialize winsock dll
	if(::WSAStartup(MAKEWORD(1, 0), &WSAData))
		return;

	// Get local host name
	char szHostName[128] = "";
	if(::gethostname(szHostName, sizeof(szHostName)))
		return;

#ifdef UNICODE
	TCHAR pchHostName[128] = {0};
	mbstowcs(pchHostName, szHostName, strlen(szHostName));
	m_strHostname.Format(_T("%s"), pchHostName);
#else
	m_strHostname.Format (_T("%s"),szHostName);
#endif

	// Get local IP addresses
	struct sockaddr_in SocketAddress;
	struct hostent *pHost = 0;

	pHost = ::gethostbyname(szHostName);
	if(!pHost)
		return;

	char aszIPAddresses[10][16]; // maximum of ten IP addresses
	for(int iCnt = 0; ((pHost->h_addr_list[iCnt]) && (iCnt < 10)); ++iCnt)
	{
		memcpy(&SocketAddress.sin_addr, pHost->h_addr_list[iCnt], pHost->h_length);
		strcpy(aszIPAddresses[iCnt], inet_ntoa(SocketAddress.sin_addr));
	}

	// Cleanup
	WSACleanup();

#ifdef UNICODE
	TCHAR pchIPAddr[64] = {0};
	mbstowcs(pchIPAddr, aszIPAddresses[0], strlen(aszIPAddresses[0]));
	m_strIP.Format(_T("%s"), pchIPAddr);
#else
	m_strIP.Format(_T("%s"),aszIPAddresses[0]);
#endif

	//-- 2012-03-09 hongsu@esmlab.com
	//-- Check IP
	int nPoint = m_strIP.Find('.');	//-- 2012-04-03 hongsu@esmlab.com
	if( nPoint > 4 || nPoint < 1 )		
		m_strIP.Format(_T("0.0.0.0"));
}

