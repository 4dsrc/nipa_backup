////////////////////////////////////////////////////////////////////////////////
//
//	TGPropertiesLoad.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-24
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TGProperties.h"
#include "GlobalIndex.h"

//-- 2011-3-2 keunbae.song
#define PROP_ITEM_COUNT 31

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// Load Property
void CTGPropertyGridCtrl::LoadExecuteOption()
{
	InitExecuteOption();

	//--------------------------------------------------------------------------
	//-- Execution
	//--------------------------------------------------------------------------

	CString strTemp;
	if( m_pItemOpt )
	{
		STATIC_DOWNCAST( CExtGridCellBool, m_pItemOpt->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellBool) ) ) -> DataSet(m_pTestBase->GetExecuteOption());
		m_pItemOpt->ValueActiveFromDefault();
	}
	if( m_pItemCount )
	{
		strTemp.Format(_T("%d"),m_pTestBase->GetTestCount());
		m_pItemCount->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strTemp );			
		m_pItemCount->ValueActiveFromDefault();
	}
}

//-------------------------------------------------------------------------- 
//! @brief	  
//! @date		  
//! @owner    
//! @note
//! @revision	
//-------------------------------------------------------------------------- 
void CTGPropertyGridCtrl::LoadBaseData(CTestBase* pTestBase)
{
	if(!pTestBase)
		return;

	m_bTeststep = false;	//step view 아님
	m_pTestBase = pTestBase;

	//-----------------------------------------
	//-- Update Options
	//-----------------------------------------
	LoadExecuteOption();

	//-- DELETE ALL PREVIOUS POINTER
	if(m_pCategoryProperty)	
		m_pCategoryProperty->ItemRemove();
	if(m_pCategoryVideo) 
		m_pCategoryVideo->ItemRemove();

	//-----------------------------------------
	//-- Load Base Property Data
	//-----------------------------------------
	CExtPropertyValue* pItem[13];
	DWORD dwIndex = 0;

	CRuntimeClass* pInitRTC = RUNTIME_CLASS(CExtGridCellString);

	//-- 1. Title
	pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_TITLE);	
	pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetID().c_str());
	pItem[dwIndex]->ValueActiveFromDefault();
	VERIFY(m_pCategoryProperty->ItemInsert(pItem[dwIndex]));
	dwIndex++;

	//-- 2. Category
	pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_CATEGORY);	
	pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetCategory());
	SetPropertyComboBox(pItem[dwIndex], g_arPropertyCategory, PROPERTY_CATEGORY_MAX, m_pTestBase->GetCategory());
	pItem[dwIndex]->ValueActiveFromDefault();
	VERIFY(m_pCategoryProperty->ItemInsert(pItem[dwIndex]));
	dwIndex++;

	//-- 3. CategorySub
	pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_CATEGORYSUB);	
	pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetCategorySub());
	SetPropertyComboBox(pItem[dwIndex], g_arPropertyCategorySub, PROPERTY_CATEGORYSUB_MAX, m_pTestBase->GetCategorySub());
	pItem[dwIndex]->ValueActiveFromDefault();
	VERIFY(m_pCategoryProperty->ItemInsert(pItem[dwIndex]));
	dwIndex++;

	if(pTestBase->GetType() == TEST_TYPE_CASE)
	{
		//-- 4. DeviceType
		pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_DEVICE_TYPE);	
		pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetDeviceType());
		vector<CString> DeviceTypeList = TGGetDeviceTypeList();
		SetPropertyComboBox(pItem[dwIndex], DeviceTypeList, m_pTestBase->GetDeviceType());
		pItem[dwIndex]->ValueActiveFromDefault();
		VERIFY(m_pCategoryProperty->ItemInsert(pItem[dwIndex]));
		dwIndex++;

		//-- 5. Model
		pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_MODEL);	
		pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetModel());
		pItem[dwIndex]->ValueActiveFromDefault();
		VERIFY( m_pCategoryProperty->ItemInsert(pItem[dwIndex]));
		dwIndex++;

		//-- 6. IP Address
		pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_IP_ADDRESS);	
		pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetIPAddress());
		pItem[dwIndex]->ValueActiveFromDefault();
		VERIFY( m_pCategoryProperty->ItemInsert(pItem[dwIndex]));
		dwIndex++;

		//-- 7. Port
		pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_PORT);
		pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetPort());
		pItem[dwIndex]->ValueActiveFromDefault();
		VERIFY( m_pCategoryProperty->ItemInsert(pItem[dwIndex]));
		dwIndex++;

		//-- 8. User ID
		pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_USER_ID);
		pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetUserID());
		pItem[dwIndex]->ValueActiveFromDefault();
		VERIFY( m_pCategoryProperty->ItemInsert(pItem[dwIndex]));
		dwIndex++;

		//-- 9. User PW
		pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_USER_PW);
		pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetUserPW());
		pItem[dwIndex]->ValueActiveFromDefault();
		VERIFY( m_pCategoryProperty->ItemInsert(pItem[dwIndex]));
		dwIndex++;
	}

	//-- 10. Creator
	pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_CREATOR);
	pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetCreator());
	pItem[dwIndex]->ValueActiveFromDefault();
	VERIFY( m_pCategoryProperty->ItemInsert(pItem[dwIndex]));
	dwIndex++;

	//-- 11. Creation Date
	pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_CRE_DATE);	
	pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetCreateDate()); 
	pItem[dwIndex]->ValueActiveFromDefault();
	VERIFY( m_pCategoryProperty->ItemInsert(pItem[dwIndex]));	
	dwIndex++;

	//-- 12. Comment
	pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_COMMENT);	
	pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetComment()); 
	pItem[dwIndex]->ValueActiveFromDefault();
	VERIFY(m_pCategoryProperty->ItemInsert(pItem[dwIndex]));	
	dwIndex++;

	//-- 13. Description
	pItem[dwIndex] =	new CExtPropertyValue(PROPERTY_ITEM_DESC);	
	pItem[dwIndex]->ValueDefaultGetByRTC(pInitRTC)->TextSet(m_pTestBase->GetDescription());
	pItem[dwIndex]->ValueActiveFromDefault();
	VERIFY(m_pCategoryProperty->ItemInsert(pItem[dwIndex]));
	dwIndex++;

	PropertyStoreSynchronize();
	SetRedraw(TRUE);
}

//-------------------------------------------------------------------------- 
//! @brief	  [1]changes for step EXECUTE-OPTION. 
//!           [2]deletes all the previous pointers.
//!           [3]loads the properties for each steps
//!           [4]Synchronizes the content of the grid windows in the property grid control.
//! @date		  
//! @owner    keunbae.song
//! @note
//! @revision	2011-3-2 keunbae.song TestStep 진행중 Property View를 클릭할 경우 Accession Violation 해결
//-------------------------------------------------------------------------- 
void CTGPropertyGridCtrl::LoadStepData(CTestStep* pTestStep, tstring strTC, int nStep)
{
	//-- 2011-3-2 keunbae.song
	EnableWindow(FALSE);

	m_pTestStep = pTestStep ;
	m_bTeststep = TRUE;

	//-- CHANGE FOR STEP EXECUTE OPTION
	LoadStepExeOpt(pTestStep);

	//-- DELETE ALL PREVIOUS POINTER
	m_pCategoryProperty->ItemRemove();
	if(m_pCategoryVideo) m_pCategoryVideo->ItemRemove();

	switch(pTestStep->GetType())
	{
	//-- 2011-11-28 hongsu.jung
	//-- Using Parents Function
	case STEP_TYPE_SER:
	case STEP_TYPE_NET_NVR:
	case STEP_TYPE_NET_DVR:
	case STEP_TYPE_NET_NCAM:
	case STEP_TYPE_PTZ_NVR:
	case STEP_TYPE_PTZ_DVR:
	case STEP_TYPE_PTZ_NCAM:
	case STEP_TYPE_PTZ_ACAM:
	case STEP_TYPE_ANALYSIS:
	case STEP_TYPE_CGI:
	case STEP_TYPE_SRM:
		{
			//-- 2011-11-28 hongsu.jung
			//-- Set Property
			m_pTestStep->SetPropertyPage(m_pCategoryProperty);	
			break;
		}
	case STEP_TYPE_GOTO:	LoadGoto();		break;
	case STEP_TYPE_CTR:		LoadCtl();		break;
	case STEP_TYPE_MOV:		LoadMovie();	break;		
	case STEP_TYPE_CHK:		LoadMlc();		break;	
	default:				
		break;
	}	

	PropertyStoreSynchronize();
	SetRedraw(TRUE);

	//-- 2011-3-2 keunbae.song
	EnableWindow(TRUE);
}

void CTGPropertyGridCtrl::LoadStepExeOpt(CTestStep* pTestStep)
{
	CString strTitle;
	int nType = pTestStep->GetType();
	if(m_pCategoryExecution)
		m_pCategoryExecution->ItemRemove ();
	//-----------------------------------------------
	//-- TYPE
	//-----------------------------------------------
	m_pItemType =	new CExtPropertyValue(PROPERTY_ITEM_TYPE);
	VERIFY( m_pCategoryExecution->ItemInsert( m_pItemType ) );
	if( m_pItemType )
	{
		switch(nType)
		{
		case STEP_TYPE_GOTO:		m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_GOTO	);	break;
		case STEP_TYPE_CTR:			m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_CTR	);	break;
		case STEP_TYPE_MOV:			m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_MOV	);	break;
		case STEP_TYPE_SER:			m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_SER	);	break;
		case STEP_TYPE_NET_NVR:		m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_NVR	);	break;
		case STEP_TYPE_NET_DVR:		m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_DVR	);	break;
		case STEP_TYPE_NET_NCAM:	m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_NCAM	);	break;
		case STEP_TYPE_PTZ_NVR:		m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_NVR	);	break;
		case STEP_TYPE_PTZ_DVR:		m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_DVR	);	break;
		case STEP_TYPE_PTZ_NCAM:	m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_NCAM	);	break;
		case STEP_TYPE_PTZ_ACAM:	m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_ACAM	);	break;
		case STEP_TYPE_CHK:			m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_MLC	);	break;
		case STEP_TYPE_CGI:			m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_CGI	);	break;
		case STEP_TYPE_SRM:			m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_SRM	);	break;
		default:					m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(INFO_STEP_OTH	);	break;
		}
		m_pItemType->ValueActiveFromDefault();
	}

	//-- 2012-03-23 hongsu@esmlab.com
	//-- Move Comment 

	//-- strComment
	CString strComment = pTestStep->GetComment().c_str();
	//--COMMENT
	CExtPropertyValue* pItemComment =	new CExtPropertyValue(PROPERTY_ITEM_COMMENT);	
	pItemComment->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strComment ); 
	pItemComment->ValueActiveFromDefault();
	//-- Insert to Category Execution
	//VERIFY(m_pCategoryProperty->ItemInsert(pItemComment));	
	VERIFY(m_pCategoryExecution->ItemInsert(pItemComment));	
}

void CTGPropertyGridCtrl::LoadNVRVideoData()
{
	m_pCategoryVideo =	new CExtPropertyCategory(PROPERTY_ITEM_VIDEO);
	VERIFY(m_PS.ItemInsert(m_pCategoryVideo));

	CString strTemp[5];
	CExtPropertyValue* pItem[5];

	m_pCategoryVideo->Reset();

	pItem[0]   = 	new CExtPropertyValue(PROPERTY_ITEM_VIDEO_CODEC_TYPE);	
	pItem[1]   = 	new CExtPropertyValue(PROPERTY_ITEM_VIDEO_FRAME_RATE);	
	pItem[2]   = 	new CExtPropertyValue(PROPERTY_ITEM_VIDEO_QUALITY);	
	pItem[3]   = 	new CExtPropertyValue(PROPERTY_ITEM_VIDEO_RESOLUTION_WIDTH);
	pItem[4]   = 	new CExtPropertyValue(PROPERTY_ITEM_VIDEO_RESOLUTION_HEIGHT);

	m_pComboCodecType = STATIC_DOWNCAST( CExtGridCellComboBox, pItem[0]->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellComboBox)));

	for(DWORD i=0;g_dwCodecTypeXNS[i] != LIST_END_VALUE;i++)
	{
		m_pComboCodecType->InsertString(g_strCodecTypeXNS[i]);
	}
	m_pComboCodecType->SetCurSel(0);//m_pTestBase->GetPostFail());
	VERIFY(m_pCategoryVideo->ItemInsert(pItem[0]));
	pItem[0]->ValueActiveFromDefault();


	int nDefaultParamSize = 8;
	for(int i=1;i<5;i++)
	{
		strTemp[i] = m_pTestStep->GetParam(nDefaultParamSize+i).c_str(); 
		pItem[i]->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strTemp[i] ); 
		pItem[i]->ValueActiveFromDefault();

		VERIFY(m_pCategoryVideo->ItemInsert(pItem[i]));
	}

}

//------------------------------------------------------------------------------ 
//! @brief		LoadGoto()
//! @date		
//! @attention	none
//! @note	 	2009-04-28
//------------------------------------------------------------------------------ 
void CTGPropertyGridCtrl::LoadGoto(void)
{
	//-- strComment
	CString strTemp;
	strTemp = m_pTestStep->GetParam(STEP_TESTCOUNT).c_str();
	CExtPropertyValue* pItemOptCount =	new CExtPropertyValue(PROPERTY_ITEM_GOTO_REPEAT_COUNT);	
	pItemOptCount->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strTemp ); 
	pItemOptCount->ValueActiveFromDefault();
	//-- Insert to Category Execution
	VERIFY( m_pCategoryProperty->ItemInsert(pItemOptCount));

	// 	int nAllParamCnt = lpvecStr->size();	
	// 
	// 	//------------------------------------	
	// 	//-- 2009-04-28				
	// 	//-- GOTO OPTION
	// 	CExtPropertyItem* pItemGotoOpt =	new CExtPropertyValue(PROPERTY_ITEM_GOTO_OPT);				
	// 	
	// 	m_pComboGotoOption = STATIC_DOWNCAST( CExtGridCellComboBox, pItemGotoOpt->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellComboBox)));
	// 	m_pComboGotoOption->InsertString(STR_GOTO_OPT_NOTHING				);
	// 	m_pComboGotoOption->InsertString(STR_GOTO_OPT_EQUALS				);
	// 	m_pComboGotoOption->InsertString(STR_GOTO_OPT_NOT_EQUAL				);
	// 	m_pComboGotoOption->InsertString(STR_GOTO_OPT_GREATER_THAN			);
	// 	m_pComboGotoOption->InsertString(STR_GOTO_OPT_LESS_THAN				);
	// 	m_pComboGotoOption->InsertString(STR_GOTO_OPT_GREATER_THAN_OR_EQUAL	);
	// 	m_pComboGotoOption->InsertString(STR_GOTO_OPT_LESS__THAN_OR_EQUAL	);
	// 
	// 	if(nAllParamCnt>3)
	// 		m_pComboGotoOption->SetCurSel(m_pComboGotoOption->FindString (lpvecStr->at(3).c_str()));
	// 	else
	// 		m_pComboGotoOption->SetCurSel(m_pComboGotoOption->FindString (STR_GOTO_OPT_NOTHING));
	// 	pItemGotoOpt->ValueActiveFromDefault();
	// 	VERIFY( m_pCategoryProperty->ItemInsert( pItemGotoOpt ) );
	// 		
	// 	//-- GOTO OPTION EXPECTION
	// 	CExtPropertyValue* pItemGotoOptExpect =	new CExtPropertyValue(PROPERTY_ITEM_GOTO_OPT_EXPECT);	
	// 	
	// 	//-- CHECK NOTHING
	// 	if(!m_pComboGotoOption->GetCurSel())
	// 		pItemGotoOptExpect->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( _T("") );
	// 	else if(nAllParamCnt>4)
	// 		pItemGotoOptExpect->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( lpvecStr->at(4).c_str() ); 
	// 	else
	// 		pItemGotoOptExpect->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( _T("") );
	// 
	// 	pItemGotoOptExpect->ValueActiveFromDefault();
	// 	VERIFY( m_pCategoryProperty->ItemInsert( pItemGotoOptExpect ) );
}


//------------------------------------------------------------------------------ 
//! @brief		LoadImage()
//! @date		
//! @attention	none
//! @note	 	2009-04-28
//------------------------------------------------------------------------------ 
void CTGPropertyGridCtrl::LoadImage(tstring strTC, int nStep)
{
	//-- strComment
	CString strTemp;
	int nMode = m_pTestStep->GetParamType(STEP_SUBJECT);
	//-- TERGET PATH
	strTemp  = m_pTestStep->GetParam(STEP_PARAM1).c_str();

	CExtPropertyValue* pItemOptTarget =	new CExtPropertyValue(PROPERTY_ITEM_CAP_FILEPATH);	
	pItemOptTarget->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strTemp ); 
	pItemOptTarget->ValueActiveFromDefault();	
	VERIFY( m_pCategoryProperty->ItemInsert( pItemOptTarget ) );	

	//-- NON USING IN SFR
	if(	nMode == IMG_CMD_PATTERN ||
		nMode == IMG_CMD_REPOS		)
	{
		//-- ORACLE PATH
		strTemp = m_pTestStep->GetParam(STEP_PARAM2).c_str();

		CExtPropertyValue* pItemOptOracle =	new CExtPropertyValue(PROPERTY_ITEM_ORA_FILEPATH);	
		pItemOptOracle->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strTemp ); 
		pItemOptOracle->ValueActiveFromDefault();
		//-- Insert to Category Execution
		VERIFY( m_pCategoryProperty->ItemInsert( pItemOptOracle ) );	
	}


	CString strTitle;
	//-- SHOW RECT/LINE Area
	switch(nMode)
	{
	case IMG_CMD_PATTERN:		
		break;
	case IMG_CMD_REPOS:
		break;
	case IMG_CMD_SFR:
		break;
	case IMG_CMD_RGB:
		break;
	case IMG_CMD_EXIF:
		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		LoadCtl()
//! @date		
//! @attention	none
//! @note	 	2009-04-28
//------------------------------------------------------------------------------ 
void CTGPropertyGridCtrl::LoadCtl(void)
{
	//-- strComment
}

//------------------------------------------------------------------------------ 
//! @brief		LoadMlc()
//! @date		
//! @attention	none
//! @note	 	2009-04-28
//------------------------------------------------------------------------------ 
void CTGPropertyGridCtrl::LoadMlc(void)
{
	//-- Task ID
	CString strTemp;
	strTemp = m_pTestStep->GetParam(STEP_PARAM2).c_str();
	CExtPropertyValue* pItem=	new CExtPropertyValue(PROPERTY_ITEM_MLC_OSD_PATH);	
	pItem->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strTemp ); 
	pItem->ValueActiveFromDefault();
	//-- Insert to Category Execution
	VERIFY( m_pCategoryProperty->ItemInsert(pItem));
}

//------------------------------------------------------------------------------ 
//! @brief		LoadDebug()
//! @date		
//! @attention	none
//! @note	 	2009-04-28
//------------------------------------------------------------------------------ 
void CTGPropertyGridCtrl::LoadDebug(void)
{
	//-- Task ID
	CString strTemp;
	strTemp = m_pTestStep->GetParam(STEP_PARAM2).c_str();
	CExtPropertyValue* pItem=	new CExtPropertyValue(PROPERTY_ITEM_DBG_CMD);	
	pItem->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strTemp ); 
	pItem->ValueActiveFromDefault();
	//-- Insert to Category Execution
	VERIFY( m_pCategoryProperty->ItemInsert(pItem));
}

//------------------------------------------------------------------------------ 
//! @brief		LoadDebug()
//! @date		
//! @attention	none
//! @note	 	2009-04-28
//------------------------------------------------------------------------------ 
void CTGPropertyGridCtrl::LoadMovie(void)
{

	//-- 2011-11-13 Jeongyong.kim
	//-- Property Data
	CString strTemp[6];
	CExtPropertyValue* pItem[6];

	//m_pCategoryProperty->Reset();

	//-- 2011-11-2 Jeongyong.kim
	pItem[0]   = 	new CExtPropertyValue(PROPERTY_ITEM_MOVIE_VIDEO_CODEC);	
	pItem[1]   = 	new CExtPropertyValue(PROPERTY_ITEM_MOVIE_RESOLUTION);	
	pItem[2]   = 	new CExtPropertyValue(PROPERTY_ITEM_MOVIE_FILE_SIZE);	
	pItem[3]   = 	new CExtPropertyValue(PROPERTY_ITEM_MOVIE_TOTAL_FRAME);
	pItem[4]   = 	new CExtPropertyValue(PROPERTY_ITEM_MOVIE_FRAME_RATE);
	pItem[5]   = 	new CExtPropertyValue(PROPERTY_ITEM_MOVIE_AVERAGE_TIME_PER_FRAME);

	for(int i = 0 ; i < 6 ; i++)
	{
		strTemp[i] = m_pTestStep->GetParam(i+4).c_str(); 
		if(i == STEP_PARAM4)
		{
			int nTemp = _ttoi(strTemp[i]);
			double dTemp = (double)nTemp/1000;
			CString strTempFR;
			strTempFR.Format(_T("%0.3f"),dTemp);
			pItem[i]->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strTempFR );
		}
		else
		{
			pItem[i]->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strTemp[i] ); 
		}
		
		pItem[i]->ValueActiveFromDefault();
		//-- Insert to Category Execution
		VERIFY( m_pCategoryProperty->ItemInsert(pItem[i]));
	}
}