////////////////////////////////////////////////////////////////////////////////
//
//	TGPageWebOption.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	keunbae.song
// @Date	  2012-07-02
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TG.h"
#include "TGPageWebOption.h"
#include "TGPagePath.h"
#include "TGNetworkView.h"
#include "TGProcessControl.h"

static const CString g_WebOptionLogLevel[] = 
{
	_T("all"), _T("trace"), _T("debug"), _T("info"), _T("warn"), _T("error"), _T("fatal"), _T("off")
};

IMPLEMENT_DYNAMIC(CTGPageWebOption, CTGPropertyPage)

BEGIN_MESSAGE_MAP(CTGPageWebOption, CTGPropertyPage)
END_MESSAGE_MAP()

//------------------------------------------------------------------------------ 
//! @brief     Constructor
//! @date      2012-07-02
//! @owner     keunbae.song
//! @note      
//! @return    
//! @revision   
//------------------------------------------------------------------------------
CTGPageWebOption::CTGPageWebOption() : CTGPropertyPage(CTGPageWebOption::IDD)	
{
	m_strAgentIP  = _T("");
	m_nAgentPort  = 28704;
  
	m_nBrowserType   = 0;
	m_strBrowserType = _T("");
}

//------------------------------------------------------------------------------ 
//! @brief     Destructor
//! @date      2012-07-02
//! @owner     keunbae.song
//! @note      
//! @return    
//! @revision   
//------------------------------------------------------------------------------
CTGPageWebOption::~CTGPageWebOption()
{
}

//------------------------------------------------------------------------------ 
//! @brief     Called by the framework to exchange and validate dialog data.
//! @date      2012-07-02
//! @owner     keunbae.song
//! @note      
//! @return    
//! @revision   
//------------------------------------------------------------------------------
void CTGPageWebOption::DoDataExchange(CDataExchange* pDX)
{
	CTGPropertyPage::DoDataExchange(pDX);

	//Set the text limit, in characters
	((CEdit*)GetDlgItem(IDC_EDIT_PORT))->SetLimitText(5);

	DDX_Text(pDX, IDC_EDIT_PORT, m_nAgentPort);
	DDX_Control(pDX,IDC_COMM_IPADDRESS, m_ipcAgentIP);
	DDX_Control(pDX, IDC_COMBO_LOGLEVEL, m_cmbLogLevel);
	DDX_Radio(pDX, IDC_WEB_RDO_IEXPLORER, m_nBrowserType);

	DDV_MinMaxInt(pDX, m_nAgentPort, 1000, 65535);
}

//------------------------------------------------------------------------------ 
//! @brief     
//! @date      2012-07-02
//! @owner     keunbae.song
//! @note      
//! @return    
//! @revision   
//------------------------------------------------------------------------------
void CTGPageWebOption::InitProp(TGWebOpt& opt)
{
	//Get [Web Test] section values from the SMILE cofiguration file(info.tgw)
	m_strAgentIP     = opt.strAgentIP;
	m_nAgentPort     = opt.nAgentPort;
	m_strLogLevel    = opt.strLogLevel;
	m_strBrowserType = opt.strSetBrowserType;

	//Web Agent IP
	if(m_strAgentIP.GetLength() == 0)
		m_strAgentIP = _T("127.0.0.1");

	m_ipcAgentIP.SetWindowText(m_strAgentIP);
  
	//Web Agent Port
	if(0 == m_nAgentPort)
		m_nAgentPort = 28704;

	//Log Option Level
	if(m_strLogLevel.GetLength() == 0)
		m_strLogLevel = _T("all");

	for(int i=0; i<ARRAY_SIZE(g_WebOptionLogLevel); i++)
		m_cmbLogLevel.AddString(g_WebOptionLogLevel[i]);

	int nIndex = m_cmbLogLevel.FindStringExact(m_cmbLogLevel.GetTopIndex(), m_strLogLevel);
	if(nIndex != CB_ERR)
		m_cmbLogLevel.SetCurSel(nIndex);
	else
		m_cmbLogLevel.SetCurSel(m_cmbLogLevel.GetTopIndex());

	//Browser Type
	m_nBrowserType = 0;
	for(int i=0; i<ARRAY_SIZE(g_WebOptionBrowserType); i++)
	{
		if(m_strBrowserType == g_WebOptionBrowserType[i])
		{
			m_nBrowserType = i;
			break;
		}
	}

	//If FALSE, dialog box is being intialized.
	UpdateData(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief     
//! @date      2012-07-02
//! @owner     keunbae.song
//! @note      
//! @return    
//! @revision   
//------------------------------------------------------------------------------
BOOL CTGPageWebOption::SaveProp(TGWebOpt& opt)
{
	//If TRUE, data is being retrieved.
	if(!UpdateData(TRUE))
		return FALSE;

  //Web Agent IP
	m_ipcAgentIP.GetWindowText(m_strAgentIP);

	//Log Option Level
	m_cmbLogLevel.GetLBText(m_cmbLogLevel.GetCurSel(),m_strLogLevel);

	//Browser Type
	m_strBrowserType = g_WebOptionBrowserType[m_nBrowserType];

	//Saves the current setting values to the SMILE cofiguration file(info.tgw)
	opt.strAgentIP     = m_strAgentIP;
	opt.nAgentPort     = m_nAgentPort;
	opt.strLogLevel    = m_strLogLevel;
	opt.strSetBrowserType = m_strBrowserType;
	if(m_nBrowserType == TG_WEBBROWSER_ALL)
		opt.strCurrentBrowserType = g_WebOptionBrowserType[TG_WEBBROWSER_IEXPLORE];
	else opt.strCurrentBrowserType = m_strBrowserType;
	return TRUE;
}
