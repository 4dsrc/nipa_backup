////////////////////////////////////////////////////////////////////////////////
//
//	TGPageLiveview.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-19
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TG.h"
#include "TGPageLiveview.h"

#include "TestStepDef.h"

IMPLEMENT_DYNAMIC(CTGPageLiveview, CTGPropertyPage)
BEGIN_MESSAGE_MAP(CTGPageLiveview, CTGPropertyPage)
END_MESSAGE_MAP()

CTGPageLiveview::CTGPageLiveview() 
: CTGPropertyPage(CTGPageLiveview::IDD)	
{
	m_strModel	= g_arStepDeviceType[0];
	m_strIP		= LOCAL_HOST	;
	m_strID		= XNS_BASE_ID	;
	m_strPW		= XNS_BASE_PW	;
	m_nPort		= XNS_BASE_PORT	;	
	m_nProfile	= 1;
}
CTGPageLiveview::~CTGPageLiveview(){}

void CTGPageLiveview::DoDataExchange(CDataExchange* pDX)
{
	CTGPropertyPage::DoDataExchange(pDX);
	((CEdit*)GetDlgItem(IDC_EDIT_PORT))->SetLimitText(5);

	
	DDX_Control	(pDX,	IDC_COMBO_MODEL		, m_cmbModel);
	DDX_Control	(pDX,	IDC_COMM_IPADDRESS	, m_ipcIP);
	DDX_Text	(pDX,	IDC_EDIT_PORT		, m_nPort);
	DDX_Text	(pDX,	IDC_EDIT_ID			, m_strID);
	DDX_Text	(pDX,	IDC_EDIT_PASSWORD	, m_strPW);
	DDX_Text	(pDX,	IDC_EDIT_PROFILE	, m_nProfile);
	DDV_MinMaxInt(pDX, m_nPort, 100, 65535);
}

void CTGPageLiveview::InitProp(TGLiveview& opt)
{
	//Get [Web Test] section values from the SMILE configuration file(info.tgw)
	if(opt.m_strModel.GetLength())	m_strModel	= opt.m_strModel;
	if(opt.m_strIP.GetLength())		m_strIP		= opt.m_strIP	;	
	if(opt.m_strID.GetLength())		m_strID		= opt.m_strID	;	
	if(opt.m_strPW.GetLength())		m_strPW		= opt.m_strPW	;	
	if(opt.m_nPort)					m_nPort		= opt.m_nPort	;
	if(opt.m_nProfile)				m_nProfile	= opt.m_nProfile;

	//-- MODEL 
	for(int i=0; i<ARRAY_SIZE(g_arStepDeviceType); i++)
		m_cmbModel.AddString(g_arStepDeviceType[i]);
	int nIndex = m_cmbModel.FindStringExact(m_cmbModel.GetTopIndex(), m_strModel);
	if(nIndex != CB_ERR)
		m_cmbModel.SetCurSel(nIndex);
	else
		m_cmbModel.SetCurSel(m_cmbModel.GetTopIndex());

	//-- IP
	if(m_strIP.GetLength() == 0)
		m_strIP = _T("127.0.0.1");
	m_ipcIP.SetWindowText(m_strIP);

	//-- Port
	if(0 == m_nPort)
		m_nPort = XNS_BASE_PORT;

	UpdateData(FALSE);
}

BOOL CTGPageLiveview::SaveProp(TGLiveview& opt)
{
	//If TRUE, data is being retrieved.
	if(!UpdateData(TRUE))
		return FALSE;

	//-- Model
	m_cmbModel.GetLBText(m_cmbModel.GetCurSel(),m_strModel);

	//-- IP
	m_ipcIP.GetWindowText(m_strIP);

	opt.m_strModel	=m_strModel	;
	opt.m_strIP		= m_strIP	;	
	opt.m_strID		= m_strID	;	
	opt.m_strPW		= m_strPW	;	
	opt.m_nPort		= m_nPort	;
	opt.m_nProfile	= m_nProfile	;
	return TRUE;
}
