////////////////////////////////////////////////////////////////////////////////
//
//	TGPageTest.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TG.h"
#include "TGPageTest.h"


// CTGPageTest 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTGPageTest, CTGPropertyPage)

CTGPageTest::CTGPageTest()
	: CTGPropertyPage(CTGPageTest::IDD)		
{

}

CTGPageTest::~CTGPageTest()
{
}

void CTGPageTest::DoDataExchange(CDataExchange* pDX)
{
	CTGPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_REMOVE_AFTER,	m_bRemoveFile);		
	DDX_Check(pDX, IDC_CHECK_REMOVE_BEFORE,	m_bRemoveFiles);	
	DDX_Text(pDX, IDC_INTERVAL_TC,			m_nIntervalTC);
	//-- 2012-04-17	
	DDX_Control(pDX, IDC_TEST_POSTFAIL,		m_ctrlPostFail);
	DDX_Check(pDX, IDC_CHECK_SRM,			m_bChkSRM);
}

void CTGPageTest::InitProp(TGTest& test)
{	
	m_nIntervalTC	= test.nIntervalTC;
	m_bRemoveFile	= test.bRemoveFile;
	m_bRemoveFiles	= test.bRemoveFiles;	
	m_bChkSRM		= test.bChkSRM;
/*
	enum TG_POSTFAIL
{
	TG_PF_SYSTEM = 0,
	TG_PF_PASS,
	TG_PF_STOP,
	TG_PF_RETRY,	
};
*/
	m_ctrlPostFail.AddString(TG_POSTFAIL_PASS);
	m_ctrlPostFail.AddString(TG_POSTFAIL_STOP);
	m_ctrlPostFail.AddString(TG_POSTFAIL_RETRY);
	m_ctrlPostFail.SetCurSel(test.nPostFail);	
	UpdateData(FALSE);
}

BOOL CTGPageTest::SaveProp(TGTest& test)
{
	if(!UpdateData(TRUE))
		return FALSE;

	test.nIntervalTC	= m_nIntervalTC;	
	test.nPostFail		= m_ctrlPostFail.GetCurSel();	
	test.bRemoveFile	= m_bRemoveFile;
	test.bRemoveFiles	= m_bRemoveFiles;
	test.bChkSRM		= m_bChkSRM;

	return TRUE;
}

BEGIN_MESSAGE_MAP(CTGPageTest, CTGPropertyPage)	
END_MESSAGE_MAP()
