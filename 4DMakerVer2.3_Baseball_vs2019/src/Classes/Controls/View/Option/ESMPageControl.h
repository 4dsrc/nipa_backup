////////////////////////////////////////////////////////////////////////////////
//
//	TGPageControl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once
#include "TGPropertyPage.h"
// CTGPageControl 대화 상자입니다.

class CTGPageControl : public CTGPropertyPage
{
	DECLARE_DYNAMIC(CTGPageControl)

public:
	CTGPageControl();
	virtual ~CTGPageControl();
	void InitProp(TGControl& opt);
	BOOL SaveProp(TGControl& opt);

	enum { IDD = IDD_OPTION_PAGE_OPT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()
public:
	CString m_str;
	BOOL m_bPowCheck;
	BOOL m_bSwitchCheck;
	BOOL m_bRemoconCheck;		
};
