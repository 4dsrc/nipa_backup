////////////////////////////////////////////////////////////////////////////////
//
//	ESMProperties.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMProperties.h"

#include "MainFrm.h"
#include "GlobalIndex.h"

#include "TestStepSerial.h"
#include "TestStepPower.h"
#include "TestStepAnalysisDef.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CStarButtonProperty_FillColor

CColorProperty::CColorProperty(CString strTitle, COLORREF rgb)	
: CExtPropertyValue(strTitle)
{
	CExtGridCellColor * pValue = STATIC_DOWNCAST( CExtGridCellColor, ValueActiveGetByRTC( RUNTIME_CLASS(CExtGridCellColor) ) );	
	ASSERT_VALID( pValue );
	pValue->SetColor(rgb);
	ValueDefaultFromActive();
}

COLORREF CColorProperty::GetRGB()
{
	CExtGridCellColor * pValueColor = STATIC_DOWNCAST(CExtGridCellColor,ValueActiveGet());
	return pValueColor->GetColor();	
}

/////////////////////////////////////////////////////////////////////////////
// CESMPropertyGridCtrl window

CESMPropertyGridCtrl::CESMPropertyGridCtrl()
: m_PS(_T("Property view"))
{
	m_pTestBase = NULL;
	m_pTestStep = NULL;
	m_pCategoryExecution	= NULL;
	m_pItemType				= NULL;
	m_pItemOpt				= NULL;
	m_pItemCount			= NULL;
	m_pItemRetryCnt			= NULL;
	m_pItemFailProgress		= NULL;
	m_pComboFailProgress	= NULL;
	m_pComboGotoOption		= NULL;
	m_pCategoryVideo		= NULL;
	//-- 2012-07-17 hongsu@esmlab.com
	//-- CIC Check
	m_pCategoryProperty		= NULL;
	m_pComboCodecType		= NULL;
}

CESMPropertyGridCtrl::~CESMPropertyGridCtrl() 
{
	RemovePropertyItem();
}


void CESMPropertyGridCtrl::RemovePropertyItem()
{
	//-------------------------------------------------
	//-- DELETE Property Store
	//-------------------------------------------------
	if(m_PS.ItemGetCount ())
		m_PS.ItemRemove();

	//-- 2012-05-22 hongsu
	/*
	if(m_pCategoryExecution)
	{
		delete m_pCategoryExecution;
		m_pCategoryExecution =  NULL;
	}

	if(m_pCategoryProperty)
	{
		delete m_pCategoryProperty;
		m_pCategoryProperty =  NULL;
	}


	if(m_pItemType)
	{
		delete m_pItemType;
		m_pItemType = NULL;
	}

	if(m_pItemOpt)
	{
		delete m_pItemOpt;
		m_pItemOpt = NULL;
	}

	if(m_pItemCount)
	{
		delete m_pItemCount;
		m_pItemCount = NULL;
	}

	if(m_pItemRetryCnt)
	{
		delete m_pItemRetryCnt;
		m_pItemRetryCnt = NULL;
	}

	if(m_pItemFailProgress)
	{
		delete m_pItemFailProgress;
		m_pItemFailProgress = NULL;
	}

	if(m_pComboFailProgress)
	{
		delete m_pComboFailProgress;
		m_pComboFailProgress = NULL;
	}

	if(m_pComboGotoOption)
	{
		delete m_pComboGotoOption;
		m_pComboGotoOption = NULL;
	}

	if(m_pCategoryVideo)
	{
		delete m_pCategoryVideo;
		m_pCategoryVideo = NULL;
	}
		*/
}

BEGIN_MESSAGE_MAP(CESMPropertyGridCtrl, CExtPropertyGridCtrl)
	ON_WM_CREATE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CESMPropertyGridCtrl WINDOW MESSAGE
//------------------------------------------------------------------------------ 
//! @brief    Called when the user has completed editing of a property value.
//! @date     2009-08-31
//! @owner    hongsu.jung
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void CESMPropertyGridCtrl::OnPgcInputComplete(CExtPropertyGridWnd* pPGW,	CExtPropertyItem* pPropertyItem)
{
	CString strPropText;
	CString strPropName;


	UINT nType; 
	{
		//-- GET CHANGED TEST
		pPropertyItem->ValueActiveGet()->TextGet( strPropText );
		strPropName = pPropertyItem->NameGet();

		// -------------------------------------------------------------------------------------------------
		//-- TEST STEP
		// -------------------------------------------------------------------------------------------------						
		if( m_bTeststep )
		{	
			if( IsSame(strPropName , PROPERTY_ITEM_COMMENT ))
				m_pTestStep->SetComment((tstring)strPropText);

			//-- SPECIFIC PROPERTIES
			CString strItem;
			nType = m_pTestStep->GetType();

			switch(nType)
			{
			case STEP_TYPE_GOTO:					
				if(IsSame(strPropName , PROPERTY_ITEM_GOTO_OPT))
				{
					m_pTestStep->SetParam (STEP_PARAM3, (tstring)strPropText);
					if(m_pTestStep->GetParam(STEP_PARAM3) == STR_GOTO_OPT_NOTHING)
					{
						m_pTestStep->SetParam (STEP_PARAM4, _T(""));
						LoadStepData(m_pTestStep);
						return ;
					}
				}
				else if( IsSame(strPropName , PROPERTY_ITEM_GOTO_OPT_EXPECT))
					if(!m_pTestStep->SetParam (STEP_PARAM4,(tstring)strPropText))
					{
						LoadStepData(m_pTestStep);
						return;
					}
					break;
			//-- 2009-11-9 hongsu.jung
			//-- Multi Language
			case STEP_TYPE_CHK:
				if( IsSame(strPropName , PROPERTY_ITEM_MLC_OSD_PATH ))
					m_pTestStep->SetParam (STEP_PARAM2,(tstring)strPropText);
				break;		
			case STEP_TYPE_SER:	
			case STEP_TYPE_NET_NVR:
			case STEP_TYPE_NET_DVR:
			case STEP_TYPE_NET_NCAM:
			case STEP_TYPE_PTZ_NVR:
			case STEP_TYPE_PTZ_DVR:
			case STEP_TYPE_PTZ_NCAM:
			case STEP_TYPE_PTZ_ACAM:
			case STEP_TYPE_ANALYSIS:
				{
					CString strValue = m_pTestStep->GetParam(strPropName).c_str();
					m_pTestStep->SetParam(strPropName,(tstring)strPropText);

					if(((nType == STEP_TYPE_ANALYSIS) && (m_pTestStep->GetParamType(STEP_SUBJECT) == ANALYSIS_SUB_VIDEO) && 
						(m_pTestStep->GetParamType(STEP_OPT1) == ANALYSIS_SUB_VIDEO_OPT1_COMPARE) && (m_pTestStep->GetParamType(STEP_OPT2) == ANALYSIS_SUB_VIDEO_OPT1_COMPARE_OPT2_OPTION) &&
						((strPropName.Compare(PROPERTY_ITEM_RECORD_DEVICE_TYPE) == 0) || (strPropName.Compare(PROPERTY_ITEM_RECORD_ENCODING) == 0)) && 
						(strPropText.Compare(strValue) != 0)) ||
					
					   ((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
						(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
						&& (strPropName.Compare(PROPERTY_ITEM_RECORD_ENCODING) == 0) && (strPropText.Compare(strValue) != 0)) ||

						//-- 2012-07-18 joonho.kim
						((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
						(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
						&& (strPropName.Compare(PROPERTY_ITEM_RECORD_MEGA) == 0) && (strPropText.Compare(strValue) != 0)) ||

						((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
						(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
						&& (strPropName.Compare(PROPERTY_ITEM_RECORD_BIT_CTRL) == 0) && (strPropText.Compare(strValue) != 0)) ||

						((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
						(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
						&& (strPropName.Compare(PROPERTY_ITEM_RECORD_RESOLUTION) == 0) && (strPropText.Compare(strValue) != 0)) ||

						((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
						(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
						&& (strPropName.Compare(PROPERTY_ITEM_RECORD_PROFILE) == 0) && (strPropText.Compare(strValue) != 0)) ||

						((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
						(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
						&& (strPropName.Compare(PROPERTY_ITEM_RECORD_BITRATE) == 0) && (strPropText.Compare(strValue) != 0)) ||

						((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
						(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
						&& (strPropName.Compare(PROPERTY_ITEM_RECORD_PRIORITY) == 0) && (strPropText.Compare(strValue) != 0)) ||

						/////////////////////////////////

					   ((nType == STEP_TYPE_NET_NVR) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_TIME) && 
						(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_TIME_OPT1_DEVICE) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_TIME_OPT1_DEVICE_OPT2_SET) &&
						(strPropName.Compare(PROPERTY_ITEM_DATE_TIME_DST) == 0) && (strPropText.Compare(strValue) != 0)))
					{
						LoadStepData(m_pTestStep);
						return;
					}
				}
				break;		
			case STEP_TYPE_MOV:	break;						
			case STEP_TYPE_CGI: break;
			case STEP_TYPE_SRM: break;
			default:						
				break;
			}
		}

		// -------------------------------------------------------------------------------------------------
		//-- WITHOUT TESTSTEP
		// -------------------------------------------------------------------------------------------------
		else	
		{
			
			//-------------------------------------------------------------------------------------------------
			//-- Execute Option
			//-------------------------------------------------------------------------------------------------	
			if(IsSame(pPropertyItem->ItemParentGet()->NameGet() , PROPERTY_ITEM_EXEC) )
			{
				if( IsSame(strPropName , PROPERTY_ITEM_EXEC_OPT ) )
				{
					if( strPropText == "True" )	m_pTestBase->SetExecuteOption(TRUE) ;	
					else						m_pTestBase->SetExecuteOption(FALSE) ;

					ESMEvent* pMsg = new ESMEvent;
					pMsg->message = WM_ESM_PROPERTY_MODIFY_EXEC_OPT;
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd(),WM_ESM,(WPARAM)WM_ESM_PROPERTY,(LPARAM)pMsg);
				}
				else if(IsSame(strPropName , PROPERTY_ITEM_EXEC_CNT ))
				{
					m_pTestBase->SetTestCount(_ttoi(strPropText));
					ESMEvent* pMsg = new ESMEvent;
					pMsg->message = WM_ESM_PROPERTY_MODIFY_EXEC_CNT;
					pMsg->nParam = (LPARAM)m_pTestBase;
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd(),WM_ESM,(WPARAM)WM_ESM_PROPERTY,(LPARAM)pMsg);
				}
				else if( IsSame(strPropName , PROPERTY_ITEM_FAIL_PRO ))
				{
					if( strPropText == ESM_POSTFAIL_SYSTEM )			m_pTestBase->SetPostFail( ESM_PF_SYSTEM) ;
					else if( strPropText == ESM_POSTFAIL_PASS )		m_pTestBase->SetPostFail( ESM_PF_PASS ) ;
					else if( strPropText == ESM_POSTFAIL_RETRY )		m_pTestBase->SetPostFail( ESM_PF_RETRY ) ;
					else if( strPropText == ESM_POSTFAIL_STOP )		m_pTestBase->SetPostFail( ESM_PF_STOP ) ;					

					CheckRetryCount();					
					SetRedraw( TRUE );
				}
				else if( IsSame(strPropName , PROPERTY_ITEM_RETRY_CNT ))
				{
					m_pTestBase->SetRetryCnt( _ttoi(strPropText) );
				}
			}
			//-------------------------------------------------------------------------------------------------
			//-- Property
			//-------------------------------------------------------------------------------------------------
			else if(IsSame(pPropertyItem->ItemParentGet()->NameGet() , PROPERTY_ITEM_PROPERTY ))
			{
				if( IsSame(strPropName , PROPERTY_ITEM_CREATOR ) )
					m_pTestBase->SetCreator(strPropText);	
				else if( IsSame(strPropName , PROPERTY_ITEM_DESC ))
					m_pTestBase->SetComment(strPropText);	
			}
		}	// end if(m_bTeststep)
	} // end if( pPropertyItem->IsModified() )

	CExtPropertyGridCtrl::OnPgcInputComplete(pPGW, pPropertyItem);
}
//-------------------------------------------------------------------------- 
//! @brief	  
//! @date	    2011-2-18
//! @owner    keunbae.song
//! @note
//! @revision	
//--------------------------------------------------------------------------
void CESMPropertyGridCtrl::OnPgcInputComplete_RAN()
{
	CExtPropertyItem* pItem = NULL;
	CString strValue = _T("");

	//[PARAM2] Interval
	pItem = m_pCategoryProperty->ItemGetAt(0);
	pItem->ValueActiveGet()->TextGet(strValue);
	if(!strValue.IsEmpty())
		m_pTestStep->SetParam(STEP_PARAM2, (tstring)strValue); 

	//[PARAM3] Touch Interval
	pItem = m_pCategoryProperty->ItemGetAt(1);
	pItem->ValueActiveGet()->TextGet(strValue);
	if(!strValue.IsEmpty())
		m_pTestStep->SetParam(STEP_PARAM3, (tstring)strValue); 

	//[PARAM4] x
	pItem = m_pCategoryProperty->ItemGetAt(2);
	pItem->ValueActiveGet()->TextGet(strValue);
	if(!strValue.IsEmpty())
		m_pTestStep->SetParam(STEP_PARAM4, (tstring)strValue); 

	//[PARAM5] y
	pItem = m_pCategoryProperty->ItemGetAt(3);
	pItem->ValueActiveGet()->TextGet(strValue);
	if(!strValue.IsEmpty())
		m_pTestStep->SetParam(STEP_PARAM5, (tstring)strValue); 

	//[PARAM6] x'
	pItem = m_pCategoryProperty->ItemGetAt(4);
	pItem->ValueActiveGet()->TextGet(strValue);
	if(!strValue.IsEmpty())
		m_pTestStep->SetParam(STEP_PARAM6, (tstring)strValue); 

	//[PARAM7] y'
	pItem = m_pCategoryProperty->ItemGetAt(5);
	pItem->ValueActiveGet()->TextGet(strValue);
	if(!strValue.IsEmpty())
		m_pTestStep->SetParam(STEP_PARAM7, (tstring)strValue); 


	//[PARAM8] Weight
	pItem = m_pCategoryProperty->ItemGetAt(6);
	pItem->ValueActiveGet()->TextGet(strValue);
	if(!strValue.IsEmpty())
		m_pTestStep->SetParam(STEP_PARAM8, (tstring)strValue); 

	//Comment
	pItem = m_pCategoryProperty->ItemGetAt(7);
	pItem->ValueActiveGet()->TextGet(strValue);
}


LRESULT CESMPropertyGridCtrl::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	LRESULT lResult =	CExtPropertyGridCtrl::WindowProc( message, wParam, lParam );
	if( message == WM_CREATE )
	{
		PropertyStoreSet( &m_PS );
		CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
		OnPgcQueryGrids( arrGrids );
		INT nGridIdx = 0;
		for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
		{
			CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
			ASSERT_VALID( pGrid );
			g_PaintManager->LoadWinXpTreeBox( pGrid->m_iconTreeBoxExpanded, true );
			g_PaintManager->LoadWinXpTreeBox( pGrid->m_iconTreeBoxCollapsed, false );
		} // for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
		CExtPropertyGridComboBoxBar * pCBB = STATIC_DOWNCAST( CExtPropertyGridComboBoxBar, GetChildByRTC( RUNTIME_CLASS(CExtPropertyGridComboBoxBar) ));
		pCBB->PropertyStoreInsert( &m_PS );
	} // if( message == WM_CREATE )

	return lResult;
}

/////////////////////////////////////////////////////////////////////////////
// CESMPropertyGridCtrl WINDOW MESSAGE

int CESMPropertyGridCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CExtPropertyGridCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;
	//-----------------------------------------------
	//-- 
	//-- CREATE EXECUTE CATEGORY
	//-- 
	//-----------------------------------------------

	//-----------------------------------------------
	//-- CATEGORY TITLE
	m_pCategoryExecution =	new CExtPropertyCategory(PROPERTY_ITEM_EXEC);
	//-- CATEGORY COMMENT FROM STRING TABLE
	VERIFY( m_PS.ItemInsert( m_pCategoryExecution ) );
	//-----------------------------------------------
	//-- 
	//-- CREATE PROPERTY CATEGORY
	//-- 
	//-----------------------------------------------
	//-- CATEGORY TITLE : PROPERTY
	m_pCategoryProperty = new CExtPropertyCategory(PROPERTY_ITEM_PROPERTY);	
	//-- Insert to Category Execution
	VERIFY( m_PS.ItemInsert( m_pCategoryProperty ) );
	return 0;
}

void CESMPropertyGridCtrl::InitExecuteOption()
{
	if(m_pCategoryExecution)
		m_pCategoryExecution->ItemRemove();

	if(m_pCategoryProperty)
		m_pCategoryProperty->ItemRemove();

	if(m_pCategoryVideo) m_pCategoryVideo->ItemRemove();

	CString strTemp;

	//-----------------------------------------------
	//-- TYPE
	//-----------------------------------------------
	m_pItemType =	new CExtPropertyValue(PROPERTY_ITEM_TYPE);
	VERIFY( m_pCategoryExecution->ItemInsert( m_pItemType ) );
	//------------------------------------
	//-- EXECUTE OPTION (BOOLEAN)
	m_pItemOpt = new CExtPropertyValue(PROPERTY_ITEM_EXEC_OPT);
	VERIFY( m_pCategoryExecution->ItemInsert( m_pItemOpt ) );
	//------------------------------------
	//-- EXE COUNT (Normal String)
	m_pItemCount = new CExtPropertyValue( PROPERTY_ITEM_EXEC_CNT);
	VERIFY( m_pCategoryExecution->ItemInsert( m_pItemCount ) );		

	switch(m_pTestBase->GetType())
	{
	case TEST_TYPE_PROJ:
		strTemp = _T("TEST PROJECT");
		break;
	case TEST_TYPE_SUITE:		
		strTemp = _T("TEST SUITE");
		break;
	case TEST_TYPE_CASE:
		strTemp = _T("TEST CASE");
		//------------------------------------
		//-- Fail Option
		//-- COMBOBOX
		m_pItemFailProgress = new CExtPropertyValue(PROPERTY_ITEM_FAIL_PRO);
		m_pComboFailProgress = STATIC_DOWNCAST( CExtGridCellComboBox, m_pItemFailProgress->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellComboBox)));
		m_pComboFailProgress->InsertString(ESM_POSTFAIL_SYSTEM);
		m_pComboFailProgress->InsertString(ESM_POSTFAIL_PASS);
		m_pComboFailProgress->InsertString(ESM_POSTFAIL_STOP);
		m_pComboFailProgress->InsertString(ESM_POSTFAIL_RETRY);
		m_pComboFailProgress->SetCurSel(m_pTestBase->GetPostFail());
		VERIFY( m_pCategoryExecution->ItemInsert( m_pItemFailProgress ) );
		m_pItemFailProgress->ValueActiveFromDefault();

		//-- CHECK RETRY
		m_pItemRetryCnt = new CExtPropertyValue(PROPERTY_ITEM_RETRY_CNT);
		VERIFY( m_pCategoryExecution->ItemInsert( m_pItemRetryCnt ) );

		if(m_pTestBase->GetPostFail() == ESM_PF_RETRY)
			m_pItemRetryCnt->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(m_pTestBase->GetRetryCountStr().c_str());
		else
			m_pItemRetryCnt->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(_T(""));
		m_pItemRetryCnt->ValueActiveFromDefault();
		break;
	default:
		strTemp = ESM_POSTFAIL_NA;
		break;
	}

	if( m_pItemType )
	{
		m_pItemType->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(strTemp);
		m_pItemType->ValueActiveFromDefault();
	}
	if( m_pItemOpt )
	{
		m_pItemOpt->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(_T(""));
		m_pItemOpt->ValueActiveFromDefault();
	}
	if( m_pItemCount )
	{
		m_pItemCount->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(_T(""));
		m_pItemCount->ValueActiveFromDefault();
	}	
}

void CESMPropertyGridCtrl::CheckRetryCount()
{
	if(m_pTestBase->GetPostFail() == ESM_PF_RETRY)
	{
		m_pItemRetryCnt->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(m_pTestBase->GetRetryCountStr().c_str());
		m_pItemRetryCnt->ValueActiveFromDefault();
	}
	else
	{
		m_pItemRetryCnt->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(_T(""));			
		m_pItemRetryCnt->ValueActiveFromDefault();
	}	
}

//--------------------------------------------------------------
//-- Input Complete
//--------------------------------------------------------------

BOOL CESMPropertyGridCtrl::IsSame(CString strA, CString strB)
{
	if(strA == strB)
		return TRUE;
	return FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		SetNoInplace()
//! @date		
//! @attention	none
//! @note	 	2010-7-7
//------------------------------------------------------------------------------ 
void CESMPropertyGridCtrl::SetNoInplace(CExtPropertyValue* pItem, CString strText, BOOL bNoInplace, COLORREF refColor)
{
	CExtGridCellString* pValue;

	pValue = STATIC_DOWNCAST(CExtGridCellString, pItem->ValueActiveGetByRTC(RUNTIME_CLASS(CExtGridCellString)));
	pValue->TextSet(strText);
	pValue->TextColorSet(CExtGridCell::__ECS_ALL, refColor);

	if(refColor != RGB(0, 0, 0))
	{
		pValue->FontWeightSet(700);  //range(0~1000). 400:Normal, 700:Bold
	}

	//-- 2011-3-2 keunbae.song
	if(bNoInplace)
		pValue->ModifyStyle(__EGCS_NO_INPLACE_CONTROL); 

	VERIFY( m_pCategoryProperty->ItemInsert(pItem));
}

BOOL CESMPropertyGridCtrl::SetPropertyComboBox(CExtPropertyValue* pItem, vector<CString> vList, CString strCurrentName)
{
	CExtGridCellComboBox* pComboBox = NULL;
	pComboBox = STATIC_DOWNCAST(CExtGridCellComboBox, pItem->ValueDefaultGetByRTC(RUNTIME_CLASS(CExtGridCellComboBox)));
	if(pComboBox == NULL) return FALSE;

	DWORD dwListSize = vList.size();
	for(DWORD i=0;i<dwListSize;i++)
	{
		pComboBox->InsertString(vList[i]);
	}

	pComboBox->SetCurSel(pComboBox->FindString(strCurrentName));

	return TRUE;
}

BOOL CESMPropertyGridCtrl::SetPropertyComboBox(CExtPropertyValue* pItem, const CString *strList, DWORD dwListSize, CString strCurrentName)
{
	CExtGridCellComboBox* pComboBox = NULL;
	pComboBox = STATIC_DOWNCAST(CExtGridCellComboBox, pItem->ValueDefaultGetByRTC(RUNTIME_CLASS(CExtGridCellComboBox)));
	if(pComboBox == NULL) return FALSE;

	for(DWORD i=0;i<dwListSize;i++)
	{
		pComboBox->InsertString(strList[i]);
	}

	pComboBox->SetCurSel(pComboBox->FindString(strCurrentName));

	return TRUE;
}
