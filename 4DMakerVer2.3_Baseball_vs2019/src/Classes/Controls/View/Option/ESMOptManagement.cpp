XConnPtContainer
// ESMOptManagement2.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMOptManagement.h"
#include "afxdialogex.h"
#include "ESMFileOperation.h"
#include "BackupFolderSelectorDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CESMOptManagement 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOptManagement, CDialogEx)

	CESMOptManagement::CESMOptManagement()
	: CESMOptBase(CESMOptManagement::IDD)
{
	m_nStartCurSel = 0;
	m_nEndCurSel = 0;

	m_nTotalFileCount = 0;
	m_nCurrentFileCount = 0;
	m_strFtpHostIp = _T("192.168.0.6");
	m_strFtpHostId = _T("esmlab");
	m_strFtpHostPassword = _T("5dldptmdpa!");

	m_strSrcFolder = _T("\\RecordSave\\Record\\Movie\\files");
	m_strDstFolder = _T("Movie_src/Test/");	

	m_pBackupMgr = NULL;	
}

CESMOptManagement::~CESMOptManagement()
{
	threadstopflag = true;	

	if(m_pBackupMgr != NULL)
	{
		//m_pBackupMgr->CloseFtp();
		//OnBnClickedBackupPause();
	}
}

// void CESMOptManagement::XSleep(DWORD dwMilliseconds)
// {
// 	DWORD dwStart = 0;
// 	MSG	msg = {0,};
// 
// 	dwStart = GetTickCount();
// 	while(GetTickCount() - dwStart < dwMilliseconds)
// 	{
// 		while(PeekMessage(&msg, NULL,0, 0, PM_REMOVE))
// 		{
// 			TranslateMessage(&msg);
// 			DispatchMessage(&msg);
// 		}
// 		::Sleep(1);
// 	}
// }

void CESMOptManagement::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_CB_OPT_STARTFOLDER, m_Cb_StartFolder);
	DDX_Control(pDX, IDC_CB_OPT_ENDFOLDER, m_Cb_EndFolder);
	DDX_Control(pDX, IDC_EDT_OPT_SRCFOLDER, m_edit_srcforder);
	DDX_Control(pDX, IDC_PRO_STATUS, m_pro_status);
	DDX_Control(pDX, IDC_STATIC_TARGET, m_Target_text);
	DDX_Control(pDX, IDC_STATIC_PROGRESS, m_staticProgress);
	DDX_Control(pDX, IDC_LIST_OPT_IP, m_listCtrlIP);
	DDX_Text(pDX, IDC_EDIT_OPT_ADDRESS, m_strFtpHostIp);
	DDX_Text(pDX, IDC_EDIT_OPT_ID, m_strFtpHostId);
	DDX_Text(pDX, IDC_EDIT_OPT_PWD, m_strFtpHostPassword);
	DDX_Text(pDX, IDC_EDT_OPT_DSTFOLDER, m_strDstFolder);
	DDX_Text(pDX, IDC_EDT_OPT_SRCFOLDER, m_strSrcFolder);
	DDX_Control(pDX, IDC_CHECK_USE_FOLDER_SELECT, m_ctrlUseFolderSelect);
	DDX_Control(pDX, IDC_CHECK_DELETE_AFTER_TRANSFER, m_ctrlDeleteAfterTransfer);
	DDX_Control(pDX, IDC_EDT_OPT_DSTFOLDER, m_ctrlTargetFolder);
	DDX_Control(pDX, IDC_EDIT_OPT_ADDRESS, m_ctrlFTPHost);
	DDX_Control(pDX, IDC_EDIT_OPT_ID, m_ctrlFTPID);
	DDX_Control(pDX, IDC_EDIT_OPT_PWD, m_ctrlFTPPW);
	DDX_Control(pDX, IDC_STATIC_FRAME_CTOSERVER, m_ctrlFrame1);
	DDX_Control(pDX, IDC_STATIC_FRAME_BACKUP, m_ctrlFrame2);
}

BEGIN_MESSAGE_MAP(CESMOptManagement, CESMOptBase)	
	ON_BN_CLICKED(IDC_BTN_OPT_COPYFOLDER, &CESMOptManagement::OnBnClickedBtnOptCopyfolder)
	ON_BN_CLICKED(IDC_BTN_OPT_DELFOLDER, &CESMOptManagement::OnBnClickedBtnOptDelfolder)
	ON_BN_CLICKED(IDC_BTN_OPT_ALLDELFOLDER, &CESMOptManagement::OnBnClickedBtnOptAlldelfolder)
	ON_BN_CLICKED(IDC_BTN_OPT_SELECTFOLDER, &CESMOptManagement::OnBnClickedBtnOptSelectfolder)
	ON_CBN_CLOSEUP(IDC_CB_OPT_ENDFOLDER, &CESMOptManagement::OnCbnCloseupCbOptEndfolder)
	ON_CBN_CLOSEUP(IDC_CB_OPT_STARTFOLDER, &CESMOptManagement::OnCbnCloseupCbOptStartfolder)	
	ON_MESSAGE(WM_ESM_OPT_MSG,	OnESMOPTMsg)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_OPT_BACKUP, &CESMOptManagement::OnBnClickedButtonOptBackup)
	ON_BN_CLICKED(IDC_BUTTON_BACKUP_PAUSE, &CESMOptManagement::OnBnClickedBackupPause)
	ON_BN_CLICKED(IDC_BUTTON_FOLDER_SELECT, &CESMOptManagement::OnBnClickedButtonFolderSelect)
	ON_MESSAGE(WM_ESM_OPT_DELETE_FOLDER_LIST, &CESMOptManagement::OnESMOptDeleteFolderList)
END_MESSAGE_MAP()


// CESMOptManagement 메시지 처리기입니다.

BOOL CESMOptManagement::OnInitDialog() 
{
	if(!CDialog::OnInitDialog())
	{
		ASSERT( FALSE );
		return FALSE;
	}

	CFileFind file;
	CString strFolder = ESMGetPath(ESM_PATH_MOVIE_FILE);
	BOOL b = file.FindFile(strFolder + _T("\\*.*"));
	CString strFolderItem, strFileExt, strTempString;
	while(b)
	{
		b = file.FindNextFile();
		if(file.IsDirectory() && !file.IsDots())
		{
			strFolderItem = file.GetFileName();
			m_arrPathList.push_back(strFolderItem);
		}
	}
	sort(m_arrPathList.begin(), m_arrPathList.end());
	for( int i =0 ;i < m_arrPathList.size(); i++)
	{
		m_Cb_StartFolder.AddString(m_arrPathList.at(i));
		m_Cb_EndFolder.AddString(m_arrPathList.at(i));
	}

	m_Cb_StartFolder.SetCurSel(0);
	m_Cb_EndFolder.SetCurSel(0);
	CString addr = GetLocalIP();

	CString targetAddr;

	targetAddr.Format(_T(""+addr+"\\"));

	//m_Target_text.SetWindowTextW(targetAddr);

	GetIPList();
	threadstopflag = false;
	m_staticProgress.SetWindowTextW(_T("0/0"));	
		
	InitBackupListCtrl();
	Invalidate();

	m_ctrlDeleteAfterTransfer.ShowWindow(SW_HIDE);		// 안보이게..

	return TRUE;
}

void CESMOptManagement::InitProp(ESMFileCopy& Opt)
{
	GetDlgItem(IDC_STATIC_FRAME_CTOSERVER)->			SetWindowPos(NULL,0,0,515,152,NULL);

	GetDlgItem(IDC_STATIC_SRC_FOLDER)->					SetWindowPos(NULL,20,31,107,24,NULL);
	GetDlgItem(IDC_STATIC_TARGET_FOLDER)->				SetWindowPos(NULL,20,65,107,24,NULL);

	GetDlgItem(IDC_EDT_OPT_SRCFOLDER)->					SetWindowPos(NULL,130,31,246,24,NULL);
	GetDlgItem(IDC_BTN_OPT_SELECTFOLDER)->				SetWindowPos(NULL,386,31,107,24,NULL);
	GetDlgItem(IDC_EDT_OPT_DSTFOLDER)->					SetWindowPos(NULL,130,65,246,24,NULL);

	GetDlgItem(IDC_BTN_OPT_COPYFOLDER)->				SetWindowPos(NULL,19,104,107,24,NULL);
	GetDlgItem(IDC_BTN_OPT_DELFOLDER)->					SetWindowPos(NULL,141,104,107,24,NULL);
	GetDlgItem(IDC_BTN_OPT_ALLDELFOLDER)->				SetWindowPos(NULL,263,104,107,24,NULL);
	GetDlgItem(IDC_PRO_STATUS)->						SetWindowPos(NULL,385,104,107,24,NULL);

	GetDlgItem(IDC_STATIC_FRAME_BACKUP)->				SetWindowPos(NULL,0,160,515,440,NULL);

	//GetDlgItem(IDC_RADIO_OPT_FTP)->						SetWindowPos(NULL,20,192,50,14,NULL);
	//GetDlgItem(IDC_RADIO_OPT_FILECOPY)->				SetWindowPos(NULL,103,192,110,14,NULL);

	GetDlgItem(IDC_EDIT_OPT_ADDRESS)->					SetWindowPos(NULL,345,188,147,24,NULL);
	GetDlgItem(IDC_EDIT_OPT_ID)->						SetWindowPos(NULL,105,227,147,24,NULL);
	GetDlgItem(IDC_EDIT_OPT_PWD)->						SetWindowPos(NULL,345,227,147,24,NULL);

	GetDlgItem(IDC_STATIC_FTP)->						SetWindowPos(NULL,265,188,70,14,NULL);
	GetDlgItem(IDC_STATIC_ID)->							SetWindowPos(NULL,20,227,70,14,NULL);
	GetDlgItem(IDC_STATIC_PW)->							SetWindowPos(NULL,265,227,70,14,NULL);

	GetDlgItem(IDC_STATIC_START_FOLDER)->				SetWindowPos(NULL,20,266,90,14,NULL);
	GetDlgItem(IDC_STATIC_ENDFOLDER)->					SetWindowPos(NULL,20,305,90,14,NULL);
	GetDlgItem(IDC_CB_OPT_STARTFOLDER)->				SetWindowPos(NULL,143,266,243,24,NULL);
	GetDlgItem(IDC_CB_OPT_ENDFOLDER)->					SetWindowPos(NULL,143,305,243,24,NULL);

	GetDlgItem(IDC_BUTTON_FOLDER_SELECT)->				SetWindowPos(NULL,20,345,97,24,NULL);
	GetDlgItem(IDC_CHECK_USE_FOLDER_SELECT)->			SetWindowPos(NULL,171,350,120,14,NULL);
	GetDlgItem(IDC_CHECK_DELETE_AFTER_TRANSFER)->		SetWindowPos(NULL,346,350,120,14,NULL);

	GetDlgItem(IDC_LIST_OPT_IP)->						SetWindowPos(NULL,20,379,472,161,NULL);
	GetDlgItem(IDC_BUTTON_OPT_BACKUP)->					SetWindowPos(NULL,20,550,232,34,NULL);
	GetDlgItem(IDC_BUTTON_BACKUP_PAUSE)->				SetWindowPos(NULL,260,550,232,34,NULL);


	UpdateData(FALSE);
}

void CESMOptManagement::GetIPList()
{	
	m_IPList.clear();

	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);	
	CESMIni ini;	
	if(ini.SetIniFilename (strFile))
	{
		CString strIPSection;
		CString strIP;
		int nIndex = 0;
		while(1)
		{
			strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);			
			strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);	
			if(!strIP.GetLength())
				break;		
			nIndex++;	
			m_IPList.push_back(strIP);
			AddRCMgr(strIP, 21);
		}			
	}
}

CString CESMOptManagement::GetLocalIP()
{
	WSADATA wsaData;
	char name[255];

	CString ip;

	PHOSTENT hostinfo;

	if( WSAStartup( MAKEWORD( 2, 0 ), &wsaData ) == 0 )
	{
		if( gethostname ( name, sizeof(name)) == 0)
		{
			if((hostinfo = gethostbyname(name)) != NULL)
			{
				ip = inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list);
			}
		}      
		WSACleanup( );
	} 
	return ip;

}

/*void CESMOptManagement::FileCopy()
{
	CString formEditText;
	CString saveEditText;
	CString strPach;	
	CString cbText;
	CString fromText;
	bool complete;


	CESMFileOperation fo;
	m_edit_srcforder.GetWindowText(formEditText);

	start = m_Cb_StartFolder.GetCurSel();
	end = m_Cb_EndFolder.GetCurSel();	

	int nProgressBarTatal = m_IPList.size() * (end - start + 1);

	CString tartgetText;
	CString staticText;

	m_Target_text.GetWindowTextW(staticText);

	//tartgetText
	tartgetText.Format(_T("\\\\"+staticText+saveEditText));
	
	
	int maxsize = 0;
	for(int i = 0 ; i <m_IPList.size(); i++)
	{		
		for(int j = start ; j<= end ; j++)
		{
			cbText = m_arrPathList.at(j);
			strPach.Format(_T("\\\\"+m_IPList.at(0)+"\\"+formEditText+"\\"+cbText));			
			Recurse(strPach);
			maxsize += m_count;
			m_count = 0;

		}
	}
	COPYINFO * coinfo = new COPYINFO();
	
	
	sprintf(coinfo->copyText,"%S",formEditText);
	sprintf(coinfo->targetText,"%S",tartgetText);
	sprintf(coinfo->EndFolder,"%S",m_arrPathList.at(end));
	sprintf(coinfo->startFolder,"%S",m_arrPathList.at(start));
	coinfo->end_index = end;
	coinfo->start_index = start;

	CString* pStrPath = NULL;
	ESMEvent* pMsg1 = NULL;
	pMsg1 = new ESMEvent();
	pMsg1->message = WM_ESM_NET_OPTION_MAN_COPY_PATH;
	pStrPath = new CString;

	pStrPath->Format(_T("%s"), tartgetText);
	pMsg1->pParam = (LPARAM)coinfo;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg1);

	m_pro_status.SetRange(0,maxsize);
	m_pro_status.SetPos(0);

	THREADPAPAMS *threadparm = new THREADPAPAMS();

	threadparm->count = maxsize;
	threadparm->com = this;
	threadparm->tartgetText = tartgetText;

	HANDLE hHandle = NULL;
	hHandle = (HANDLE)_beginthreadex(NULL, 0, FileCopyThread, (THREADPAPAMS *)threadparm, 0, NULL);

}
*/

typedef struct tagTREADPARAMS { 
	CESMOptManagement *com;
	int count; 
	CString tartgetText;
	int start;
	int end;
	CString strPach;
	CString targetText;
	BOOL	bAll;
} THREADPAPAMS; 

typedef struct stFTPPARAMS {
	std::shared_ptr<CEMSFtp> pFtp;
	CString strLocalFile;
	CString strFoldername;
	CString strFilename;	
} FTPPARAMS;

void CESMOptManagement::FileCopy()
{
	int start;
	int end;
	CString formEditText;
	CString saveEditText;
	CString strPach;	
	CString cbText;
	CString fromText;
	CString targetText;
	CString staticText;
	CESMFileOperation fo;

	m_edit_srcforder.GetWindowText(formEditText);

	m_Target_text.GetWindowTextW(staticText);
	//targetText.Format(_T("\\\\"+staticText+saveEditText));
	targetText = staticText;

	if( fo.CheckPath(targetText) != PATH_IS_FOLDER)
	{
		AfxMessageBox(_T("DESTINATION FOLDER ["+targetText+"]"+" NOT FOUND!!"));
		return;
	}

	start = m_Cb_StartFolder.GetCurSel();
	end = m_Cb_EndFolder.GetCurSel();	

	//Total File Count Check
	m_pro_status.SetPos(0);
	GetFileCount();	

	for(int i =0; i < m_IPList.size();i++)			
	{
		strPach.Format(_T("\\\\"+m_IPList.at(i)+"\\"+formEditText));

		THREADPAPAMS *threadparm = new THREADPAPAMS();

		threadparm->start = start;
		threadparm->end = end;
		threadparm->strPach = strPach;
		threadparm->com = this;
		threadparm->targetText = targetText;

		HANDLE hHandle = NULL;
		hHandle = (HANDLE)_beginthreadex(NULL, 0, FileCopyThread, (THREADPAPAMS *)threadparm, 0, NULL);
	}
	
//	XSleep(5000);	//Wait - Total File Count Checking
	m_iPrevcount = 0;
	SetTimer(WM_ESM_OPT_COPY_TIMER, 5000, NULL);
}

void CESMOptManagement::GetFileCount(BOOL bAll)
{
	CESMFileOperation fo;
	int start;
	int end;
	CString formEditText;
	CString saveEditText;
	CString strPach;	
	CString cbText;
	CString fromText;
	CString targetText;
	CString staticText;

	m_edit_srcforder.GetWindowText(formEditText);

	m_Target_text.GetWindowTextW(staticText);
	targetText = staticText;

	start = m_Cb_StartFolder.GetCurSel();
	end = m_Cb_EndFolder.GetCurSel();	

	m_nTotalFileCount = 0;
	m_nCurrentFileCount = 0;

	for(int i =0; i < m_IPList.size();i++)			
	{
		strPach.Format(_T("\\\\"+m_IPList.at(i)+"\\"+formEditText));

		THREADPAPAMS *threadparm = new THREADPAPAMS();

		threadparm->start = start;
		threadparm->end = end;
		threadparm->strPach = strPach;
		threadparm->com = this;
		threadparm->targetText = targetText;
		threadparm->bAll = bAll;
		if(bAll == TRUE)
		{
			threadparm->start = 0;
			threadparm->end = 0;
			threadparm->bAll = bAll;
		}	

		HANDLE hHandle = NULL;
		hHandle = (HANDLE)_beginthreadex(NULL, 0, FileCountThread, (THREADPAPAMS *)threadparm, 0, NULL);
	}
}

void CESMOptManagement::FileSelectDelete()
{

	int start;
	int end;
	CString formEditText;
	CString saveEditText;
	CString strPach;	
	CString cbText;
	CString fromText;
	CString targetText;
	CString staticText;

	m_edit_srcforder.GetWindowText(formEditText);

	m_Target_text.GetWindowTextW(staticText);
	//targetText.Format(_T("\\\\"+staticText+saveEditText));
	targetText = staticText;

	start = m_Cb_StartFolder.GetCurSel();
	end = m_Cb_EndFolder.GetCurSel();	

	//Total File Count Check
	m_pro_status.SetPos(0);
	GetFileCount();	

	for(int i =0; i < m_IPList.size();i++)			
	{
		strPach.Format(_T("\\\\"+m_IPList.at(i)+"\\"+formEditText));

		THREADPAPAMS *threadparm = new THREADPAPAMS();

		threadparm->start = start;
		threadparm->end = end;
		threadparm->strPach = strPach;
		threadparm->com = this;
		threadparm->targetText = targetText;
		threadparm->bAll = FALSE;

		HANDLE hHandle = NULL;
		hHandle = (HANDLE)_beginthreadex(NULL, 0, FileDelThread, (THREADPAPAMS *)threadparm, 0, NULL);
	}
}

void CESMOptManagement::FileAllDelete()
{
	CString formEditText;
	CString saveEditText;
	CString strPach;	
	CString cbText;
	CString fromText;
	CString targetText;
	CString staticText;

	m_edit_srcforder.GetWindowText(formEditText);

	//Total File Count Check
	m_pro_status.SetPos(0);
	GetFileCount(TRUE);	

	for(int i =0; i < m_IPList.size();i++)			
	{
		strPach.Format(_T("\\\\"+m_IPList.at(i)+"\\"+formEditText));

		THREADPAPAMS *threadparm = new THREADPAPAMS();

		threadparm->start = 0;
		threadparm->end = 0;
		threadparm->strPach = strPach;
		threadparm->com = this;
		threadparm->bAll = TRUE;

		HANDLE hHandle = NULL;
		hHandle = (HANDLE)_beginthreadex(NULL, 0, FileDelThread, (THREADPAPAMS *)threadparm, 0, NULL);
	}
}

// 
// void CESMOptManagement::FileSelectDelete()
// {
// 	CString formEditText;
// 	CString saveEditText;
// 	CString strPach;	
// 	CString cbText;
// 	CString fromText;
// 	bool complete;
// 	CString tartgetText;
// 	CString staticText;
// 
// 	CESMFileOperation fo;
// 	m_edit_srcforder.GetWindowText(formEditText);
// 	m_Target_text.GetWindowTextW(staticText);
// 
// 	start = m_Cb_StartFolder.GetCurSel();
// 	end = m_Cb_EndFolder.GetCurSel();	
// 	int nProgressBarTatal = m_IPList.size() * (end - start + 1);
// 
// 	//tartgetText
// 	tartgetText.Format(_T("\\\\"+staticText+saveEditText));
// 	
// 	
// 	int maxsize = 0;
// 	for(int i = 0 ; i <m_IPList.size(); i++)
// 	{
// 		
// 		for(int j = start ; j<= end ; j++)
// 		{
// 			cbText = m_arrPathList.at(j);
// 			strPach.Format(_T("\\\\"+m_IPList.at(0)+"\\"+formEditText+"\\"+cbText));			
// 			Recurse(strPach);
// 			maxsize += m_count;
// 			m_count = 0;
// 
// 		}
// 	}
// 	COPYINFO * coinfo = new COPYINFO();
// 	
// 	
// 	sprintf(coinfo->copyText,"%S",formEditText);
// 	sprintf(coinfo->targetText,"%S",tartgetText);
// 	sprintf(coinfo->EndFolder,"%S",m_arrPathList.at(end));
// 	sprintf(coinfo->startFolder,"%S",m_arrPathList.at(start));
// 	coinfo->end_index = end;
// 	coinfo->start_index = start;
// 
// 	CString* pStrPath = NULL;
// 	ESMEvent* pMsg1 = NULL;
// 	pMsg1 = new ESMEvent();
// 	pMsg1->message = WM_ESM_NET_OPTION_MAN_DEL_PACH;
// 	pStrPath = new CString;
// 
// 	pStrPath->Format(_T("%s"), tartgetText);
// 	pMsg1->pParam = (LPARAM)coinfo;
// 	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg1);
// 
// 	m_pro_status.SetRange(0,maxsize);
// 	m_pro_status.SetPos(0);
// 
// 	THREADPAPAMS *threadparm = new THREADPAPAMS();
// 
// 	threadparm->count = maxsize;
// 	threadparm->com = this;
// 	threadparm->tartgetText = tartgetText;
// 	HANDLE hHandle = NULL;
// 	hHandle = (HANDLE)_beginthreadex(NULL, 0, FileDelThread, (THREADPAPAMS *)threadparm, 0, NULL);
// }

unsigned WINAPI CESMOptManagement::FileCountThread(LPVOID param)
{

	THREADPAPAMS *pThreadParams = (THREADPAPAMS *)param; 

	CESMOptManagement *com = pThreadParams->com;
	int maxcount = pThreadParams->count;
	int count = 0;
	CString strPach;
	CString fromeditText;
	com->m_edit_srcforder.GetWindowText(fromeditText);
	CString cbText;
	CESMFileOperation fo;
	

	for(int j = pThreadParams->start; j <= pThreadParams->end; j++)
	{
		if(pThreadParams->bAll == TRUE)
		{
			strPach.Format(_T(""+pThreadParams->strPach));		
		}
		else
		{
			cbText = com->m_arrPathList.at(j);
			//strPach.Format(_T("\\\\"+com->m_IPList.at(0)+"\\"+fromeditText+"\\"+cbText));
			strPach.Format(_T(""+pThreadParams->strPach+"\\"+cbText));		
		}
		fo.GetFileCount(strPach,count);
		com->m_nTotalFileCount += count;
		count = 0;			
	}
	//Progress Bar Set
	com->m_pro_status.SetRange(0, com->m_nTotalFileCount);	
	CString str;
	str.Format(_T("0/%d"), com->m_nTotalFileCount);
	com->m_staticProgress.SetWindowTextW(str);

	return 0;
}

unsigned WINAPI CESMOptManagement::FileCopyThread(LPVOID param)
{

	THREADPAPAMS *pThreadParams = (THREADPAPAMS *)param; 

	CESMOptManagement *com = pThreadParams->com;
	CString filelog;
	CString cbText;
	CString fromText;

	CESMFileOperation fo;
	BOOL complete;
	fo.SetOverwriteMode(TRUE);
	for(int j = pThreadParams->start; j <= pThreadParams->end; j++)
	{
		fo.m_iFileCount = 0;

		if(com->threadstopflag)
		{
			ESMLog(5,_T("File Copy -  " + pThreadParams->strPach+" - Stop"));
			break;
		}
		cbText = com->m_arrPathList.at(j);
		fromText.Format(_T(""+pThreadParams->strPach+"\\"+cbText));		
		//complete = fo.Replace(fromText,pThreadParams->targetText );
		complete = fo.Copy(fromText, pThreadParams->targetText);

		if(complete)
		{
			filelog.Format(_T("File Copy -  "+fromText+" : Copy Complete"));
		}
		else
		{
			filelog.Format(_T("File Copy -  "+fromText+" : Copy Error"));
			ESMLog(5,filelog);
		}

		if(com->threadstopflag)
		{
			ESMLog(5,_T("File Copy -  " + pThreadParams->strPach+" - Stop"));
			break;
		}

		
		//Progress Bar Refresh
		com->m_nCurrentFileCount += fo.m_iFileCount;
		/*
		com->m_pro_status.OffsetPos(fo.m_iFileCount);
		CString str;
		str.Format(_T("%d/%d"),com->m_nCurrentFileCount,com->m_nTotalFileCount);
		com->m_staticProgress.SetWindowTextW(str);
		*/
		if(com->m_nCurrentFileCount == com->m_nTotalFileCount)
		{
			::PostMessage(com->GetSafeHwnd(), WM_ESM_OPT_MSG, (WPARAM)WM_ESM_OPT_COPY_SUCCESS, NULL );
		}
		else
		{
			::PostMessage(com->GetSafeHwnd(), WM_ESM_OPT_MSG, (WPARAM)WM_ESM_OPT_COPY_FAIL, NULL );
		}
	}
	ESMLog(5,_T("File Copy -  " + pThreadParams->strPach+" - Finish"));
	return 0;
}

unsigned WINAPI CESMOptManagement::FileDelThread(LPVOID param)
{

	THREADPAPAMS *pThreadParams = (THREADPAPAMS *)param; 

	CESMOptManagement *com = pThreadParams->com;
	CString filelog;
	CString cbText;
	CString fromText;

	CESMFileOperation fo;
	BOOL complete;
	fo.SetOverwriteMode(TRUE);
	for(int j = pThreadParams->start; j <= pThreadParams->end; j++)
	{
		fo.m_iFileCount = 0;

		if(com->threadstopflag)
		{
			ESMLog(5,_T("File Delete -  " + pThreadParams->strPach+" - Stop"));
			break;
		}
		if(pThreadParams->bAll == TRUE)
		{
			fromText.Format(_T(""+pThreadParams->strPach));		
			complete = fo.Delete(fromText, FALSE);
		}
		else
		{
			cbText = com->m_arrPathList.at(j);
			fromText.Format(_T(""+pThreadParams->strPach+"\\"+cbText));		
			//complete = fo.Replace(fromText,pThreadParams->targetText );
			complete = fo.Delete(fromText);
		}

		if(complete)
		{
			filelog.Format(_T("File Delete -  "+fromText+" : Delete Complete"));
		}
		else
		{
			filelog.Format(_T("File Delete -  "+fromText+" : Delete Error"));
			ESMLog(5,filelog);
		}

		if(com->threadstopflag)
		{
			ESMLog(5,_T("File Delete -  " + pThreadParams->strPach+" - Stop"));
			break;
		}

		//Progress Bar Refresh
		com->m_nCurrentFileCount += fo.m_iFileCount;
		com->m_pro_status.OffsetPos(fo.m_iFileCount);
		CString str;
		str.Format(_T("%d/%d"),com->m_nCurrentFileCount,com->m_nTotalFileCount);
		com->m_staticProgress.SetWindowTextW(str);

		if(com->m_nCurrentFileCount == com->m_nTotalFileCount)
		{
			::PostMessage(com->GetSafeHwnd(), WM_ESM_OPT_MSG, (WPARAM)WM_ESM_OPT_DELETE_END, NULL );
		}
	}
	ESMLog(5,_T("File Delete -  " + pThreadParams->strPach+" - Finish"));
	return 0;
}

/*
void CESMOptManagement::FileAllDelete()
{

	CString formEditText;
	CString saveEditText;
	CString strPach;	
	CString cbText;
	CString fromText;
	bool complete;
	CString tartgetText;
	CString staticText;

	CESMFileOperation fo;
	m_edit_srcforder.GetWindowText(formEditText);
	m_Target_text.GetWindowTextW(staticText);

	start = m_Cb_StartFolder.GetCurSel();
	end = m_Cb_EndFolder.GetCurSel();	
	int nProgressBarTatal = m_IPList.size() * (end - start + 1);

	//tartgetText
	tartgetText.Format(_T("\\\\"+staticText+saveEditText));

	int maxsize = 0;
	for(int i = 0 ; i <m_IPList.size(); i++)
	{

		for(int j = start ; j<= end ; j++)
		{
			cbText = m_arrPathList.at(j);
			strPach.Format(_T("\\\\"+m_IPList.at(0)+"\\"+formEditText+"\\"+cbText));			
			Recurse(strPach);
			maxsize += m_count;
			m_count = 0;

		}
	}
	COPYINFO * coinfo = new COPYINFO();


	sprintf(coinfo->copyText,"%S",formEditText);
	sprintf(coinfo->targetText,"%S",tartgetText);
	sprintf(coinfo->EndFolder,"%S",m_arrPathList.at(end));
	sprintf(coinfo->startFolder,"%S",m_arrPathList.at(start));
	coinfo->end_index = -1;
	coinfo->start_index = -1;

	CString* pStrPath = NULL;
	ESMEvent* pMsg1 = NULL;
	pMsg1 = new ESMEvent();
	pMsg1->message = WM_ESM_NET_OPTION_MAN_DEL_PACH;
	pStrPath = new CString;

	pStrPath->Format(_T("%s"), tartgetText);
	pMsg1->pParam = (LPARAM)coinfo;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg1);

	m_pro_status.SetRange(0,maxsize);
	m_pro_status.SetPos(0);

	THREADPAPAMS *threadparm = new THREADPAPAMS();

	threadparm->count = maxsize;
	threadparm->com = this;
	threadparm->tartgetText = tartgetText;
	HANDLE hHandle = NULL;
	hHandle = (HANDLE)_beginthreadex(NULL, 0, FileDelThread, (THREADPAPAMS *)threadparm, 0, NULL);
}
*/

void CESMOptManagement::OnBnClickedBtnOptCopyfolder()
{
	FileCopy();
}

void CESMOptManagement::OnBnClickedBtnOptDelfolder()
{
	if(IDYES == AfxMessageBox(_T("삭제된 파일은 복구가 되지 않습니다. 삭제하시겠습니까?"),MB_YESNO))
	{
		FileSelectDelete();
	}	
}


void CESMOptManagement::OnBnClickedBtnOptAlldelfolder()
{
	if(IDYES == AfxMessageBox(_T("삭제된 파일은 복구가 되지 않습니다. 삭제하시겠습니까?"),MB_YESNO))
	{
		FileAllDelete();
	}
}

void CESMOptManagement::OnBnClickedBtnOptSelectfolder()
{
	ITEMIDLIST*  pildBrowse;
	TCHAR   pszPathname[MAX_PATH];
	CString strPath;
	m_Target_text.GetWindowTextW(strPath);
	//strPath.GetBuffer();
	BROWSEINFO  bInfo;
	memset(&bInfo, 0, sizeof(bInfo));
	bInfo.hwndOwner   = GetSafeHwnd();
	bInfo.pidlRoot   = NULL;
	bInfo.pszDisplayName = pszPathname;
	bInfo.lpszTitle   = _T("Please select a directory");
	bInfo.ulFlags   = BIF_RETURNONLYFSDIRS; 
	bInfo.lpfn    = NULL;
	bInfo.lParam  = (LPARAM)(LPCTSTR)"C:\\";
	bInfo.lParam  = (LPARAM)NULL;
	pildBrowse    = SHBrowseForFolder(&bInfo);
	if(pildBrowse)
	{
		SHGetPathFromIDList(pildBrowse, pszPathname);
		//m_Target_text.SetWindowTextW(pszPathname);		
		m_strSrcFolder = pszPathname;
		UpdateData(FALSE);
	}
}

void CESMOptManagement::OnCbnCloseupCbOptEndfolder()
{
	int startIndex = m_Cb_StartFolder.GetCurSel();
	int endIndex = m_Cb_EndFolder.GetCurSel();

	if( startIndex > endIndex)
	{
		m_Cb_EndFolder.SetCurSel(startIndex);
	}
}


void CESMOptManagement::OnCbnCloseupCbOptStartfolder()
{
	int startIndex = m_Cb_StartFolder.GetCurSel();
	int endIndex = m_Cb_EndFolder.GetCurSel();

	if( startIndex > endIndex)
	{
		m_Cb_EndFolder.SetCurSel(startIndex);
	}
}

BOOL CESMOptManagement::SaveProp(ESMManagement& SaveData)
{
	UpdateData();

	m_edit_srcforder.GetWindowText(SaveData.strSelectFilePath);
	m_Target_text.GetWindowTextW(SaveData.strSaveFilePath);

	return TRUE;
}

void CESMOptManagement::InitProp(ESMManagement& SaveData)
{
	m_edit_srcforder.SetWindowText(SaveData.strSelectFilePath);
	m_Target_text.SetWindowTextW(SaveData.strSaveFilePath);

	UpdateData(FALSE);
}

void CESMOptManagement::SetBackupMgr(CESMBackup* pBackupMgr)
{
	m_pBackupMgr = pBackupMgr;
}

LRESULT CESMOptManagement::OnESMOPTMsg(WPARAM wParam, LPARAM lParam)
{
	switch((int)wParam)
	{
		case WM_ESM_OPT_COPY_SUCCESS:
		//	KillTimer(WM_ESM_OPT_COPY_TIMER);
		//	OnBnClickedBtnOptDelfolder();
		case WM_ESM_OPT_COPY_FAIL:
		//	KillTimer(WM_ESM_OPT_COPY_TIMER);
			break;
		case WM_ESM_OPT_DELETE_END:
			AfxMessageBox(_T("해당 파일이 삭제 되었습니다."));
			break;
		default: break;
	}
	return TRUE;
}

void CESMOptManagement::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent = WM_ESM_OPT_COPY_TIMER)
	{
		if(m_nTotalFileCount !=0)
		{
			KillTimer(WM_ESM_OPT_COPY_TIMER);
			CESMFileOperation fo;
			CString str, strTarget;
			int count;
			m_Target_text.GetWindowTextW(strTarget);

			count = 0;
			fo.GetFileCount(strTarget, count);
			if(m_nTotalFileCount >= count)
			{
				m_pro_status.OffsetPos(count - m_iPrevcount);
				str.Format(_T("%d/%d"), count, m_nTotalFileCount);
				m_staticProgress.SetWindowTextW(str);
			}
			m_iPrevcount = count;

			if( m_nTotalFileCount == count)			
				OnBnClickedBtnOptDelfolder();
			else
				SetTimer(WM_ESM_OPT_COPY_TIMER, 500, NULL);
		}
	}	
	CESMOptBase::OnTimer(nIDEvent);
}

void CESMOptManagement::InitBackupListCtrl()
{
	//init Header
	m_listCtrlIP.InitProgressColumn(1);
	m_listCtrlIP.InsertColumn(0,_T("IP"),LVCFMT_LEFT,100);
	m_listCtrlIP.InsertColumn(1,_T("Progress"),LVCFMT_LEFT,100);
	m_listCtrlIP.InsertColumn(2,_T("done/total"),LVCFMT_LEFT,100);
	
	//todo : insert ip 	
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);

		m_listCtrlIP.InsertItem(nAll,pExist->GetIP());
		
		CString pProgressPer;
		pProgressPer = _T("Ready");
		m_listCtrlIP.SetItem(nAll, 2, LVIF_TEXT, pProgressPer, 0, 0, 0, NULL);
		m_listCtrlIP.SetProgressPercent(nAll,
			(float)pExist->m_nBackupCurrentFileCount/pExist->m_nBackupAllFileCount * 100.
			);
	}		
}

void CESMOptManagement::OnBnClickedButtonOptBackup()
{	
	CButton *pButton =  (CButton*)GetDlgItem(IDC_RADIO_OPT_FTP);

	// ftp
	if(pButton->GetCheck())
	{	
		int nAll = GetRCMgrCount();
		int g_nAgentAll		= nAll;
		int g_nAgentStatus  = nAll;
		bool bUseFolerList = m_ctrlUseFolderSelect.GetCheck();
		bool bDeleteAfterTransfer = m_ctrlDeleteAfterTransfer.GetCheck();

		CESMRCManager* pExist = NULL;
		while(nAll--)
		{
			pExist = GetRCMgr(nAll);

			if( pExist)
			{
				BackupData* pBackupData;
				pBackupData= new BackupData;
				pBackupData->pView = this;
				pBackupData->pMRCMgr = pExist;
				pBackupData->nReTryCount = 1;
				pBackupData->nNum = nAll;
				pBackupData->bUseFolerList = bUseFolerList;
				pBackupData->bUseDeleteAfterTransfer = bDeleteAfterTransfer;

				HANDLE hHandle = NULL;
				hHandle = (HANDLE) _beginthreadex(NULL, 0, BackupStartThread, (void *)pBackupData, 0, NULL);
				CloseHandle(hHandle);

				BackupData* pBackupData_;
				pBackupData_= new BackupData;
				pBackupData_->pView = this;
				pBackupData_->pMRCMgr = pExist;
				pBackupData_->nReTryCount = 1;
				pBackupData_->nNum = nAll;

				if(pExist->m_bIsExcutingBackupProgram)
					continue;

				HANDLE hHandle_ = NULL;
				hHandle_ = (HANDLE) _beginthreadex(NULL, 0, BackupCurrentFileCountThread, (void *)pBackupData_, 0, NULL);				

				m_pArrHandle.insert(pair<CString, HANDLE>(
					pExist->GetIP(), 
					hHandle_));

				pExist->m_bIsExcutingBackupProgram = TRUE;
			}
		}
		m_listCtrlIP.OnPaint();
	}
	//fliecopy
	else
	{

	}
}

void CESMOptManagement::OnBnClickedBackupPause()
{
	int nAll = GetRCMgrCount();
	int g_nAgentAll		= nAll;
	int g_nAgentStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{		
		pExist = GetRCMgr(nAll);

		if( pExist)
		{
			BackupData* pBackupData;
			pBackupData= new BackupData;
			pBackupData->pView = this;
			pBackupData->pMRCMgr = pExist;
			pBackupData->nReTryCount = 1;
			pBackupData->nNum = nAll;

			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, BackupEndThread, (void *)pBackupData, 0, NULL);
			CloseHandle(hHandle);

			if(pExist->m_bIsExcutingBackupProgram)
			{	
				pExist->m_bIsExcutingBackupProgram = FALSE;				

				HANDLE hHandle = m_pArrHandle[pExist->GetIP()];
				::TerminateThread( hHandle, 0 );
				CloseHandle(hHandle);			
			}
		}
	}
	
	m_pArrHandle.clear();
	m_listCtrlIP.OnPaint();
}

unsigned WINAPI CESMOptManagement::BackupStartThread(LPVOID param)
{	
	BackupData* pBackupData = (BackupData*)param;
	CESMOptManagement* pView = pBackupData->pView;
	CESMRCManager* pMRCMgr = pBackupData->pMRCMgr;
	int nNum = pBackupData->nNum;
	bool bUseFolerList = pBackupData->bUseFolerList;
	bool bDeleteBackup = pBackupData->bUseDeleteAfterTransfer;

	if(pBackupData)
	{
		delete pBackupData;
		pBackupData = NULL;
	}
	if(!pView)
		return 0;

	if(pMRCMgr)			
	{
		BOOL bConnect = pMRCMgr->ConnectToAgent(TRUE);

		if(bConnect)
			pView->m_listCtrlIP.SetItem(nNum, 2, LVIF_TEXT, _T("Connect"), 0, 0, 0, NULL);
		else
		{
			pView->m_listCtrlIP.SetItem(nNum, 2, LVIF_TEXT, _T("Fail"), 0, 0, 0, NULL);
			return 0;
		}

//		if( pMRCMgr->GetServerSocket())
		{
//			CString strIP = pMRCMgr->GetIP();
			pMRCMgr->BackupStart(bUseFolerList, bDeleteBackup);

		}
	}
	return 0;
}

unsigned WINAPI CESMOptManagement::BackupEndThread(LPVOID param)
{	
	BackupData* pBackupData = (BackupData*)param;
	CESMOptManagement* pView = pBackupData->pView;
	CESMRCManager* pMRCMgr = pBackupData->pMRCMgr;
	int nNum = pBackupData->nNum;

	if(pBackupData)
	{
		delete pBackupData;
		pBackupData = NULL;
	}
	if(!pView)
		return 0;

	if(pMRCMgr)			
	{
		BOOL bConnect = pMRCMgr->ConnectToAgent(TRUE);

		if(bConnect)
			pView->m_listCtrlIP.SetItem(nNum, 2, LVIF_TEXT, _T("Pause"), 0, 0, 0, NULL);		

		//if( pMRCMgr->GetServerSocket())
		{
			CString strIP = pMRCMgr->GetIP();

			pMRCMgr->BackupEnd();
		}

	}
	return 0;
}

unsigned WINAPI CESMOptManagement::BackupCurrentFileCountThread(LPVOID param)
{
	BackupData* pBackupData = (BackupData*)param;
	CESMOptManagement* pView = pBackupData->pView;
	CESMRCManager* pMRCMgr = pBackupData->pMRCMgr;
	if(pBackupData)
	{
		delete pBackupData;
		pBackupData = NULL;
	}

	if(!pView)
		return 0;

	while(1)
	{	
		Sleep(1000);
		if(pMRCMgr)			
		{
			//pMRCMgr->ConnectToAgent(TRUE);
//			if( pMRCMgr->GetServerSocket())
			{
				CString strIP = pMRCMgr->GetIP();
				pMRCMgr->BackupCurrentFileCount();

				if(!pView->m_pArrHandle.empty())
					pView->RedrawBackupList();
			} 
		}
	}
	return 0;
}

void CESMOptManagement::RedrawBackupList()
{
	int nAll = GetRCMgrCount();		
	CESMRCManager* pExist = NULL;

	while(nAll--)
	{
		pExist = GetRCMgr(nAll);

		for (int j = 0; j < m_listCtrlIP.GetItemCount() ; j++)
		{
			CString strListIp = m_listCtrlIP.GetItemText(j, 0);

			if (strListIp != pExist->GetIP())
				continue;

			CString pProgressPer;
			pProgressPer.Format(_T("%d/%d"), pExist->m_nBackupCurrentFileCount, pExist->m_nBackupAllFileCount);

			m_listCtrlIP.SetItem(j, 2, LVIF_TEXT, pProgressPer, 0, 0, 0, NULL);

			int nPercent= (float)pExist->m_nBackupCurrentFileCount/pExist->m_nBackupAllFileCount*100.;
			m_listCtrlIP.SetProgressPercent(j, nPercent);

			//if(pExist->m_nBackupCurrentFileCount == pExist->m_nBackupAllFileCount)
			if (pExist->m_bFinish)
			{
				m_listCtrlIP.SetItem(j, 2, LVIF_TEXT, _T("Finish"), 0, 0, 0, NULL);
			}
		}
	}
}

void CESMOptManagement::AddRCMgr(CString strIP, int nPort)
{
	CESMRCManager* pRCMgr = NULL;
	pRCMgr = new CESMRCManager(strIP, nPort);
	pRCMgr->CreateThread();
	m_arRCServerList.Add((CObject*)pRCMgr);
}

CESMRCManager* CESMOptManagement::GetRCMgr(int nIndex)
{
	return (CESMRCManager*)m_arRCServerList.GetAt(nIndex);
}

void CESMOptManagement::InitProp(ESMBackupString& backupString)
{
	m_strSrcFolder		= backupString.strString[ESM_BACKUP_STRING_SRCFOLDER];
	m_strDstFolder		= backupString.strString[ESM_BACKUP_STRING_DSTFOLDER];
	m_strFtpHostIp		= backupString.strString[ESM_BACKUP_STRING_FTPHOST	];
	m_strFtpHostId		= backupString.strString[ESM_BACKUP_STRING_FTPID	];	
	m_strFtpHostPassword= backupString.strString[ESM_BACKUP_STRING_FTPPW	];	

	UpdateData(FALSE);
}

BOOL CESMOptManagement::SaveProp(ESMBackupString& backupString)
{
	if(!UpdateData(TRUE))
		return FALSE;

	backupString.strString[ESM_BACKUP_STRING_SRCFOLDER	] = m_strSrcFolder		;
	backupString.strString[ESM_BACKUP_STRING_DSTFOLDER	] = m_strDstFolder		;
	backupString.strString[ESM_BACKUP_STRING_FTPHOST	] = m_strFtpHostIp		;
	backupString.strString[ESM_BACKUP_STRING_FTPID		] = m_strFtpHostId		;	
	backupString.strString[ESM_BACKUP_STRING_FTPPW		] = m_strFtpHostPassword;

	UpdateData(FALSE);
	return TRUE;
}


void CESMOptManagement::OnBnClickedButtonFolderSelect()
{
	CBackupFolderSelectorDlg pBackupFolderSelectorDlg(this);

	pBackupFolderSelectorDlg.SetBackupMgr(m_pBackupMgr);

	pBackupFolderSelectorDlg.DoModal();
}

LRESULT CESMOptManagement::OnESMOptDeleteFolderList(WPARAM wParam, LPARAM lParam)
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pMRCMgr = NULL;
	while(nAll--)
	{
		pMRCMgr = GetRCMgr(nAll);
 		BOOL bConnect = pMRCMgr->ConnectToAgent(TRUE);
 
 		if(pMRCMgr && bConnect)
 		{
			pMRCMgr->DeleteSelectList();
		}
	}
	return 0;
}
