////////////////////////////////////////////////////////////////////////////////
//
//	TGPageSendSql.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-10-26
//
////////////////////////////////////////////////////////////////////////////////


#pragma once
#include "TGPropertyPage.h"
#include "afxwin.h"

// CTGPageSendSql 대화 상자입니다.

class CTGPageSendSql : public CTGPropertyPage
{
	DECLARE_DYNAMIC(CTGPageSendSql)

public:
	CTGPageSendSql();
	virtual ~CTGPageSendSql();	
// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_PAGE_SENDSQL};
	CListCtrl		m_ListCtrlSelectedFileList;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()	

public:
	virtual BOOL OnInitDialog();
	void InitProp();
	void InitListCtrl();
	void SqlGetFiles(CString strPath);

	void SqlListSetFiles(CString strIP,CString strTC,CString strUUID,int nCount );

	afx_msg void OnSendSql();

	afx_msg void OnBnClickedCancel();
};
