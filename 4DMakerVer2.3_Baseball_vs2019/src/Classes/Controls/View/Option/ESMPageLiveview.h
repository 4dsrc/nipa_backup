////////////////////////////////////////////////////////////////////////////////
//
//	TGPageLiveview.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-19
//
////////////////////////////////////////////////////////////////////////////////



#pragma once

#include "TGPropertyPage.h"

#define LOCAL_HOST		_T("127.0.0.1");
#define	XNS_BASE_ID		_T("admin");
#define	XNS_BASE_PW		_T("4321");;
#define	XNS_BASE_PORT	4520


class CTGPageLiveview : public CTGPropertyPage
{
	DECLARE_DYNAMIC(CTGPageLiveview)
	DECLARE_MESSAGE_MAP()

public:
	CTGPageLiveview();
	virtual ~CTGPageLiveview();

	enum { IDD = IDD_OPTION_PAGE_LIVEVIEW };

	void InitProp(TGLiveview& opt);
	BOOL SaveProp(TGLiveview& opt);

public:	
	CString m_strModel	;
	CString m_strIP		;
	CString m_strID		;
	CString m_strPW		;
	int		m_nPort		;	
	int		m_nProfile	;

private:
	CIPAddressCtrl m_ipcIP;	
	CComboBox m_cmbModel;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
};
