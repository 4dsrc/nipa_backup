////////////////////////////////////////////////////////////////////////////////
//
//	TGCGIInfo.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TG.h"
#include "TGCGIInfo.h"
#include "TGPagePath.h"
#include "TGIni.h"


// CTGCGIInfo 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTGCGIInfo, CTGPropertyPage)

CTGCGIInfo::CTGCGIInfo(){}
CTGCGIInfo::~CTGCGIInfo(){}

//---------------------------------------------------------------
//-- Load Infomation from Each Model
//---------------------------------------------------------------
int CTGCGIInfo::GetCGIValue(int nItem, CString strKey)
{	
	CString strModel = TGGetModel();
	return LoadInfoValue(strModel, g_arInfoNCAMItem[nItem], strKey);
}

int CTGCGIInfo::GetCGIValueCount(int nItem)
{
	CString strModel = TGGetModel();
	return GetSectionCount(strModel, g_arInfoNCAMItem[nItem]);
}

CString CTGCGIInfo::GetCGIKey(int nItem, int nValue)
{	
	CString strModel = TGGetModel();
	CString strTemp;

	if (NCAM_ITEM_CNT <= nItem)
	{
		strTemp.Format(_T("Item MAX %d"),NCAM_ITEM_CNT-1);
		return strTemp;
	}
	return LoadInfoKey(strModel, g_arInfoNCAMItem[nItem], nValue);
}

CString CTGCGIInfo::GetConvertLanguage(int nItem, CString strKey)
{	
//	CString strModel = TGGetModel();
	CString strTemp;

	if (NCAM_ITEM_CNT <= nItem)
	{
		strTemp.Format(_T("Item MAX %d"),NCAM_ITEM_CNT-1);
		return strTemp;
	}
	return LoadInfoKey(g_arInfoNCAMItem[nItem], strKey);
}

void CTGCGIInfo::GetCGIKeyArray(int nItem, CStringArray& arList)
{
	CString strModel = TGGetModel();
	GetSectionList(strModel, g_arInfoNCAMItem[nItem], arList);
}

int CTGCGIInfo::LoadInfoValue(CString strModel, CString strItem, CString strProperty)
{
	CString strFilePath;
	CString strValue;
	
	strFilePath.Format(TGGetPath(TG_PATH_CONFIG_NCAM));
	strFilePath.AppendFormat(_T("\\%s.info"),strModel);

	CTGIni ini;
	if(!ini.SetIniFilename (strFilePath))
		return -1;

	strValue = ini.GetString(strItem,strProperty);

	//-- 2012-05-02
	//-- hongsu
	TGLog(3,_T("[CGI:%s] %s:%s = %s"), strModel, strItem, strProperty, strValue );
	return _ttol( strValue );
}

int CTGCGIInfo::GetSectionCount(CString strModel, CString strItem)
{
	CString strFilePath;
	CString strValue;
	
	strFilePath.Format(TGGetPath(TG_PATH_CONFIG_NCAM));
	strFilePath.AppendFormat(_T("\\%s.info"),strModel);

	CTGIni ini;
	if(!ini.SetIniFilename (strFilePath))
		return -1;

	return ini.GetSectionCount(strItem);
}

void CTGCGIInfo::GetSectionList(CString strModel, CString strItem, CStringArray& arList)
{
	CString strFilePath;
	CString strValue;
	
	strFilePath.Format(TGGetPath(TG_PATH_CONFIG_NCAM));
	strFilePath.AppendFormat(_T("\\%s.info"),strModel);

	CTGIni ini;
	ini.SetIniFilename (strFilePath);
	ini.GetSectionList(strItem, arList);
}



CString CTGCGIInfo::LoadInfoKey(CString strModel, CString strItem, int nValue)
{
	CString strFilePath;
	CString strKey;
	CString strValue;
	
	strFilePath.Format(TGGetPath(TG_PATH_CONFIG_NCAM));
	strFilePath.AppendFormat(_T("\\%s.info"),strModel);

	CTGIni ini;
	if(!ini.SetIniFilename (strFilePath))
		return strKey;

	strValue.Format(_T("%d"),nValue);
	strKey = ini.GetStringValuetoKey(strItem,strValue);
	
	return strKey;
}

CString CTGCGIInfo::LoadInfoKey(CString strItem, CString strKey)
{
	CString strFilePath = _T("");;
	CString strValue = _T("");
	
	strFilePath.Format(TGGetPath(TG_PATH_CONFIG_LANGUAGE));

	CTGIni ini;
	if(!ini.SetIniFilename(strFilePath))
		return strValue;

	strValue = ini.GetString(strItem, strKey);
	
	return strValue;
}