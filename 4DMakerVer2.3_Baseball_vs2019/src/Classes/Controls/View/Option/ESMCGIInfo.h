////////////////////////////////////////////////////////////////////////////////
//
//	TGCGIInfo.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

class CTGCGIInfo
{
	DECLARE_DYNAMIC(CTGCGIInfo)

public:
	CTGCGIInfo();
	virtual ~CTGCGIInfo();

public:
	int GetCGIValue(int nItem, CString strKey);
	int GetCGIValueCount(int nItem);	
	CString GetCGIKey(int nItem, int nValue);
	void GetCGIKeyArray(int nItem, CStringArray& arList);
	CString GetConvertLanguage(int nItem, CString strKey);
	

private:
	int LoadInfoValue(CString strModel, CString strItem, CString strKey);
	CString LoadInfoKey(CString strModel, CString strItem, int nValue);
	CString LoadInfoKey(CString strItem, CString strKey);
	int GetSectionCount(CString strModel, CString strItem);
	void GetSectionList(CString strModel, CString strItem, CStringArray& arList);

};