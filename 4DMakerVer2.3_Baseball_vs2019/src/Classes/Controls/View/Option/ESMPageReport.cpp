////////////////////////////////////////////////////////////////////////////////
//
//	TGPageReport.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-10-26
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TG.h"
#include "TGPageReport.h"
#include "globalindex.h"

//-- 2012-04-06 hongsu.jung
#include "TGFunc.h"
#include "MainFrm.h"
	
// CTGPageReport 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTGPageReport, CTGPropertyPage)

CTGPageReport::CTGPageReport()
	: CTGPropertyPage(CTGPageReport::IDD)	
{
	m_nReportOpt = 0;
}

CTGPageReport::~CTGPageReport()
{
}

void CTGPageReport::DoDataExchange(CDataExchange* pDX)
{
	CTGPropertyPage::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_TG_REPORT_AUTO, m_nReportOpt);
}

void CTGPageReport::InitProp(TGReport& report)
{
	//-- 2012-10-26 hongsu@esmlab.com
	//-- Data From Configuration 
	m_nReportOpt	= report.nReportOpt;	
	//-- 2012-04-06 hongsu.jung
	UpdateData(FALSE);
}

BOOL CTGPageReport::SaveProp(TGReport& report)
{
	if(!UpdateData(TRUE))
		return FALSE;
	
	report.nReportOpt = m_nReportOpt;
	TGSetValue(TG_OPT_REPORT_SAVE_OPT, m_nReportOpt);
	return TRUE;
}

BEGIN_MESSAGE_MAP(CTGPageReport, CTGPropertyPage)
/*
ON_BN_CLICKED(IDC_TG_REPORT_AUTO	, OnBnClickedTgReportAuto	)
ON_BN_CLICKED(IDC_TG_REPORT_LOCAL	, OnBnClickedTgReportLocal	)
ON_BN_CLICKED(IDC_TG_REPORT_NONE	, OnBnClickedTgReportNone	)
*/
END_MESSAGE_MAP()

/*
void CTGPageReport::OnBnClickedTgReportAuto		() { m_nReportOpt = TG_REPORT_OPT_CONNECT;	UpdateData(FALSE);}
void CTGPageReport::OnBnClickedTgReportLocal	() { m_nReportOpt = TG_REPORT_OPT_LOCAL;	UpdateData(FALSE);}
void CTGPageReport::OnBnClickedTgReportNone		() { m_nReportOpt = TG_REPORT_OPT_NONE;		UpdateData(FALSE);}
*/
