////////////////////////////////////////////////////////////////////////////////
//
//	ESMOpt4DMaker.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMFunc.h"
#include "ESMOpt4DMaker.h"
#include "ESMOptPath.h"
#include "ESMOptSyncDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMOpt4DMaker 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOpt4DMaker, CESMOptBase)

CESMOpt4DMaker::CESMOpt4DMaker()
	: CESMOptBase(CESMOpt4DMaker::IDD)	
	, m_bMakeServer(1)
	, m_bMode(0)
	, m_bServerMode(0)
	, m_nPort(0)	
	, m_nDeleteCnt(30)
	, m_nRepeatMovieCnt(0)
	, m_nDSCSyncCnt(400)
	, m_nWaitSaveDSC(0)	
	, m_bViewInfo(FALSE)
	, m_bTestMode(FALSE)
	, m_bMakeAndAotuPlay(TRUE)
	, m_bInsertLogo(FALSE)
	, m_b3DLogo(FALSE)
	, m_bLogoZoom(FALSE)
	, m_bInsertCameraID(FALSE)
	, m_nMovieSaveLocation(1)
	, m_nTemplateStartMargin(0)
	, m_nTemplateEndMargin(0)
	, m_nTemplateStartSleep(0)
	, m_nTemplateEndSleep(0)
	, m_bTimeLinePreview(TRUE)
	, m_bEnableVMCC(FALSE)
	, m_bEnableCopy(FALSE)
	, m_bUHDtoFHD(FALSE)
	, m_bSaveImg(FALSE)
	, m_bEnableBaseBall(FALSE)
	, m_bReverseMovie(FALSE)
	, m_bGPUMakeFile(FALSE)
	, m_bRepeatMovie(FALSE)
	, m_strBaseBall(_T(""))
	, m_nBaseBallInning(0)
	, m_bCheckValid(TRUE)
	, m_nWaitSaveDSCSub(0)
	, m_bSyncSkip(FALSE)
	, m_nPrismValue(100)
	, m_nPrismValue2(100)
	, m_bInsertWhiteBal(0)	
	, m_bTemplatePoint(FALSE)
	, m_bTemplateStabilization(FALSE)
	, m_bTemplateModify(FALSE)
	, m_bCheckAJALogo(FALSE)
	, m_bCheckAJAReplay(FALSE)
	, m_bCheckAJAOnOff(FALSE)
	, m_nAJASampleCnt(33)
	, m_bRamDiskSize(FALSE)
	,m_bColorRivision(FALSE)
	,m_bPrnColorInfo(FALSE)
	,m_bGPUMakeSkip(FALSE)
	,m_bLightWeight(FALSE)
	,m_bAutoDetect(FALSE)
	,m_bDirectMux(FALSE)
	,m_n4DPMethod(1)
	, m_bRemote(FALSE)
	, m_bRemoteSkip(FALSE)
	//, m_n4DPMethod(0)
	, m_bHorizontal(FALSE)
	, m_bEncodingForSmartPhones(FALSE)
	, m_nFrameZoomRatio(100)
	, m_nTemplateTargetOutPercent(0)
	, m_bRecordFileSplit(FALSE)
	, m_bAJANetwork(FALSE)
	, m_bGIFMaking(FALSE)
	, m_nGIFQuaility(0)
	, m_nGIFSize(0)
	, m_nSetFrameRateIndex(0)
	, m_bRefereeRead(0)
	, m_bUseReSync(0)
	, m_bAutoDelete(FALSE)
	, m_bAutoAdjustPositionTracking(FALSE)
	, m_bAutoAdjustKZone(FALSE)
	, m_bRTSP(FALSE)
	, m_b4DAP(FALSE)
	, m_nSetTemplateModifyMode(0)
	, m_nEncodingSmartPhoneBitRate(5)
{	
	
}

CESMOpt4DMaker::~CESMOpt4DMaker()
{
}

void CESMOpt4DMaker::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);
	DDX_Text(pDX,  IDC_4DMAKER_PORT , m_nPort);	
	DDX_Text(pDX,  IDC_WAIT_SAVEDSC , m_nWaitSaveDSC);	
	DDX_Text(pDX,  IDC_4DMAKER_SYNCTIME_MARGIN , m_nSyncTimeMargin);
	DDX_Text(pDX,  IDC_4DMAKER_TAMPLATEMARGIN_START , m_nTemplateStartMargin);
	DDX_Text(pDX,  IDC_4DMAKER_TAMPLATEMARGIN_END , m_nTemplateEndMargin);
	DDX_Text(pDX,  IDC_4DMAKER_TAMPLATESLEEP_START , m_nTemplateStartSleep);
	DDX_Text(pDX,  IDC_4DMAKER_TAMPLATESLEEP_END , m_nTemplateEndSleep);
	DDX_Text(pDX,  IDC_4DMAKER_SYNCTIME_SLEEP , m_nSensorOnTime);
	//-- 2015-02-15 cygil@esmlab.com
	//-- Add picture coutdown
	DDX_Text(pDX,  IDC_4DMAKER_COUNTDOWN_LASTIP , m_nCountdownLastIP);
	DDX_Control(pDX, IDC_4DMAKER_SERVER, m_BtnServer);
	DDX_Control(pDX, IDC_4DMAKER_AGENT, m_BtnAgent);
	DDX_Control(pDX, IDC_MAKE_SERVER_ON, m_BtnMakeOn);
	DDX_Control(pDX, IDC_MAKE_SERVER_OFF, m_BtnMakeOff);

	DDX_Check(pDX, IDC_CHECK_ENABLE_VIEW_INFO	, m_bViewInfo);
	DDX_Check(pDX, IDC_CHECK_ENABLE_TEST	, m_bTestMode);
	DDX_Check(pDX, IDC_ADJUST_MAKEANDAUTOPLAY	, m_bMakeAndAotuPlay);
	DDX_Check(pDX, IDC_ADJUST_INSERTLOGO	,	m_bInsertLogo);
	DDX_Check(pDX, IDC_CHECK_3DLOGO	,	m_b3DLogo);
	DDX_Check(pDX, IDC_CHECK_LOGOZOOM	,	m_bLogoZoom);
	DDX_Check(pDX, IDC_ADJUST_INSERTCAMERAID	,	m_bInsertCameraID);
	DDX_Check(pDX, IDC_CHECK_PRISM	,	m_bInsertKzone);
	DDX_Check(pDX, IDC_CHECK_SyncSkip	,	m_bSyncSkip);//hjcho 160912
	DDX_Text(pDX, IDC_EDIT_PRISM_VALUE, m_nPrismValue);//hjcho 160912
	DDX_Text(pDX, IDC_EDIT_PRISM_VALUE2, m_nPrismValue2);//hjcho 161019
	DDX_Check(pDX, IDC_CHECK_WHITEBALANCE, m_bInsertWhiteBal); //hjcho 161212
	DDX_Check(pDX,IDC_CHECK_COLORREVISION,m_bColorRivision);
	DDX_Check(pDX,IDC_CHECK_COLORINFO,m_bPrnColorInfo);
	DDX_Check(pDX, IDC_CHECK_TIMELINE_PRREVIEW	, m_bTimeLinePreview);
	DDX_Radio(pDX, IDC_RADIO_MOVIESAVE_SERVER, m_nMovieSaveLocation);
	DDX_Control(pDX, IDC_4DSERVER_BOTH, m_BtnServerBoth);
	DDX_Control(pDX, IDC_4DSERVER_MAKING, m_BtnServerMaking);
	DDX_Control(pDX, IDC_4DSERVER_RECORING, m_BtnServerRecording);
	DDX_Check(pDX, IDC_CHECK_ENABLE_VMCC , m_bEnableVMCC);
	DDX_Check(pDX, IDC_CHECK_ENABLE_4DP_COPY , m_bEnableCopy);
	DDX_Check(pDX, IDC_CHECK_BASE_BALL_MODE , m_bEnableBaseBall);
	DDX_Check(pDX, IDC_CHECK_REVERSE_MOVIE , m_bReverseMovie);
	DDX_Check(pDX, IDC_CHK_GPU_MAKE , m_bGPUMakeFile);
	DDX_Check(pDX, IDC_CHK_REPEAT , m_bRepeatMovie);
	DDX_Check(pDX, IDC_CHECK_UHD_TO_FHD	, m_bUHDtoFHD);
	DDX_Control(pDX, IDC_CHECK_ENABLE_VMCC, m_ctrlEnableVMCC);
	DDX_Control(pDX, IDC_CHECK_ENABLE_4DP_COPY, m_ctrlEnableCopy);
	DDX_Control(pDX, IDC_CHECK_BASE_BALL_MODE, m_ctrlEnableBaseBall);
	DDX_Control(pDX, IDC_CHECK_UHD_TO_FHD, m_ctrlUHDtoFHD);
	DDX_Text(pDX,  IDC_MOVIE_DELETE_CNT , m_nDeleteCnt);	
	DDX_Text(pDX,  IDC_MOVIE_REPEAT_CNT , m_nRepeatMovieCnt);	
	DDX_Check(pDX, IDC_CHECK_ENABLE_SAVE_IMG , m_bSaveImg);
	DDX_Control(pDX, IDC_CHECK_ENABLE_SAVE_IMG, m_ctrlEnableSaveImg);
	DDX_Control(pDX, IDC_CMB_BASEBALL, m_ctrlBaseBall);
	DDX_CBString(pDX, IDC_CMB_BASEBALL, m_strBaseBall);

	DDX_Check(pDX, IDC_CHECK_ENABLE_VALID , m_bCheckValid);
	DDX_Control(pDX, IDC_CHECK_ENABLE_VALID, m_ctrlCheckValid);

	DDX_Text(pDX,  IDC_DSC_SYNC_COUNT , m_nDSCSyncCnt);	
	DDX_Text(pDX, IDC_WAIT_SAVEDSC_SUB, m_nWaitSaveDSCSub);
	DDX_Control(pDX, IDC_WAIT_SAVEDSC_SUB, m_EditWaitSaveDSCSub);
	DDX_Check(pDX, IDC_CHK_TEMPLATE, m_bTemplatePoint);
	DDX_Check(pDX, IDC_CHK_TEMPLATE_STABILIZATION, m_bTemplateStabilization);
	DDX_Check(pDX, IDC_CHK_TEMPLATE_MODIFY, m_bTemplateModify);
	DDX_Check(pDX,IDC_AJA_LOGO,m_bCheckAJALogo);
	DDX_Check(pDX,IDC_AJA_REPLAY,m_bCheckAJAReplay);
	//170207 hjcho
	DDX_Check(pDX,IDC_AJA_ONOFF,m_bCheckAJAOnOff);
	DDX_Text(pDX,IDC_EDIT_AJASAMPLECNT,m_nAJASampleCnt);

	//  DDX_Control(pDX, IDC_CHK_GPU_MAKE, m_ctrlTemplatePoint);
	DDX_Check(pDX, IDC_CHECK_GET_RAMDISK, m_bRamDiskSize);
	DDX_Check(pDX, IDC_CHK_TEMPLATE_STABILIZATION, m_bTemplateStabilization);
	DDX_Check(pDX, IDC_CHK_TEMPLATE_MODIFY, m_bTemplateModify);

	DDX_Check(pDX, IDC_CHK_4DPSKIP,m_bGPUMakeSkip);

	//170727 wgkim
	DDX_Check(pDX, IDC_CHECK_LIGHTWEIGHT, m_bLightWeight);

	//170729 hjcho
	DDX_Check(pDX, IDC_CHECK_AUTODETECTING,m_bAutoDetect);

	//170830 hjcho
	DDX_Check(pDX, IDC_CHECK_DIRECT_MUX, m_bDirectMux);

	//170904 hjcho
	DDX_Control(pDX, IDC_RADIO_4DP_MULGPU, m_btn4DP_MUL);
	DDX_Control(pDX, IDC_RADIO_4DP_ENCODE, m_btn4DP_ENC);
	DDX_Control(pDX, IDC_RADIO_4DP_60p,	   m_btn4DP_60p);

	DDX_Check(pDX, IDC_CHECK_REMOTE_CTRL, m_bRemote);
	DDX_Control(pDX, IDC_IPADDRESS_REMOTE, m_ctlRemoteIp);
	DDX_Check(pDX, IDC_CHECK_REMOTE_SKIP, m_bRemoteSkip);
	DDX_Radio(pDX, IDC_RADIO_4DP_MULGPU, m_n4DPMethod);
	DDX_Check(pDX, IDC_CHK_HORIZONTALADJ, m_bHorizontal);

	//171127 wgkim
	DDX_Check(pDX, IDC_CHECK_ENCODING_FOR_SMARTPHONES, m_bEncodingForSmartPhones);

	//171210 hjcho
	DDX_Text(pDX,IDC_OPT_EDIT_ZOOMRATIO,m_nFrameZoomRatio);

	//180119 wgkim
	DDX_Text(pDX, IDC_EDIT_TEMPLATE_TARGETOUT_PERCENT, m_nTemplateTargetOutPercent);
	DDX_Check(pDX, IDC_CHECK_RECORD_FILE_SPLIT, m_bRecordFileSplit);
	DDX_Control(pDX, IDC_OPT_AJAIP, m_ipAJANetwork);
	DDX_Check(pDX,IDC_OPT_AJA_ON,m_bAJANetwork);
	DDX_Control(pDX, IDC_4DMAKER_PORT, m_ctrlPort);
	DDX_Control(pDX, IDC_WAIT_SAVEDSC, m_ctrlCaptureDelay);
	DDX_Control(pDX, IDC_4DMAKER_SYNCTIME_MARGIN, m_ctrlSyncTime);
	DDX_Control(pDX, IDC_DSC_SYNC_COUNT, m_ctrlSyncCount);
	DDX_Control(pDX, IDC_MOVIE_DELETE_CNT, m_ctrlMovieDeleteCnt);
	DDX_Control(pDX, IDC_MOVIE_REPEAT_CNT, m_ctrlMovieRepeatCnt);
	DDX_Control(pDX, IDC_EDIT_TEMPLATE_TARGETOUT_PERCENT, m_ctrlTemplateTarget);
	DDX_Control(pDX, IDC_STATIC_FRAME_PGSTATE, m_ctrlProgramState);
	DDX_Control(pDX, IDC_STATIC_FRAME_MOVIE, m_ctrlFrameMovie);
	DDX_Control(pDX, IDC_STATIC_FRAME_EVENTMOVIE, m_ctrlEventMovie);
	DDX_Control(pDX, IDC_STATIC_FRAME_LOGOOPT, m_ctrlLogoOption);
	DDX_Control(pDX, IDC_STATIC_FRAME_CAMERA_SYNC, m_ctrlCameraSync);
	DDX_Control(pDX, IDC_STATIC_FRAME_AJA, m_ctrlAJA);
	DDX_Control(pDX, IDC_STATIC_FRAME_4DP_METHOD, m_ctrl4DPMethod);
	DDX_Control(pDX, IDC_STATIC_FRAME_4DP, m_ctrl4DP);
	DDX_Control(pDX, IDC_STATIC_FRAME_TEMPLATE_POS, m_ctrlTemplatePoint);
	DDX_Control(pDX, IDC_STATIC_FRAME_SERVERMAKE, m_ctrlServerMake);
	DDX_Control(pDX, IDC_BTN_SYNC_SET, m_btnSyncSet);

	DDX_Check(pDX, IDC_CHK_GIF_MAKING, m_bGIFMaking);
	DDX_Control(pDX, IDC_CBN_GIFQUALITY, m_cbGIFQuality);
	DDX_Control(pDX, IDC_CBN_GIF_SIZE, m_cbGIFSize);

	//wgkim 180629
	DDX_Control(pDX, IDC_COMBO_SET_FRAMERATE, m_cbSetFrameRate);
	DDX_Control(pDX, IDC_STATIC_GIF_GRP, m_ctrlGifMake);

	DDX_Check(pDX, IDC_OPT_CHECK_REFREE, m_bRefereeRead);
	DDX_Check(pDX, IDC_CHECK_USE_RESYNC, m_bUseReSync);
	DDX_Check(pDX, IDC_CHECK_DELETE, m_bAutoDelete);
	DDX_Check(pDX, IDC_CHK_AUTOADJUST_POSITION_TRACKING, m_bAutoAdjustPositionTracking);
	DDX_Check(pDX, IDC_CHK_RTSP, m_bRTSP);
	DDX_Check(pDX, IDC_CHECK_AUTOADJUST_KZONE, m_bAutoAdjustKZone);
	DDX_Check(pDX, IDC_CHECK_4DAP, m_b4DAP);

	//wgkim 181119
	DDX_Control(pDX, IDC_COMBO_SET_TEMPLATE_MODIFY, m_cbSetTemplateModifyMode);

	//hjcho 190103
	DDX_Text(pDX,IDC_OPT_EDIT_ENCBITRATE,m_nEncodingSmartPhoneBitRate);
	DDX_Control(pDX, IDC_OPT_EDIT_ENCBITRATE, m_ctrlEncodeBitrate);
}

void CESMOpt4DMaker::InitProp(ESM4DMaker& Opt)
{
	m_Opt = Opt;

	m_bMakeServer	= Opt.bMakeServer;
	m_bMode			= Opt.bRCMode;
	
	//m_bServerMode	= ESM_SERVERMODE_BOTH;//Opt.nServerMode;
	m_bServerMode	= Opt.nServerMode;

	m_nPort			= Opt.nPort;
	m_nDeleteCnt	= Opt.nDeleteCnt;
	m_nRepeatMovieCnt = Opt.nRepeatMovieCnt;
	m_nDSCSyncCnt	= Opt.nDSCSyncCnt;
	m_bMakeAndAotuPlay		= Opt.bMakeAndAotuPlay;
	m_bViewInfo		= Opt.bViewInfo;
	m_bTestMode		= Opt.bTestMode;
	m_bInsertLogo		= Opt.bInsertLogo;
	m_b3DLogo		= Opt.b3DLogo;
	m_bLogoZoom		= Opt.bLogoZoom;
	m_bInsertCameraID	= Opt.bInsertCameraID;
	m_nMovieSaveLocation		= Opt.nMovieSaveLocation;
	
	m_nWaitSaveDSC = Opt.nWaitSaveDSC;
	//jhhan
	m_nWaitSaveDSCSub = Opt.nWaitSaveDSCSub;
	m_nWaitSaveDSCSel = Opt.nWaitSaveDSCSel;


	m_nSensorOnTime	= Opt.nSensorOnTime;
	m_nSyncTimeMargin	= Opt.nSyncTimeMargin;
	m_nTemplateStartMargin = Opt.nTemplateStartMargin;
	m_nTemplateEndMargin = Opt.nTemplateEndMargin;
	m_nTemplateStartSleep = Opt.nTemplateStartSleep;
	m_nTemplateEndSleep = Opt.nTemplateEndSleep;
	m_bTimeLinePreview = Opt.bTimeLinePreview;
	m_bEnableVMCC = Opt.bEnableVMCC;
	m_bEnableCopy = Opt.bEnableCopy;
	m_bEnableBaseBall = Opt.bEnableBaseBall;
	m_bReverseMovie = Opt.bReverseMovie;
	m_bGPUMakeFile = Opt.bGPUMakeFile;
	m_bRepeatMovie = Opt.bRepeatMovie;
	m_bSaveImg	= Opt.bSaveImg;
	m_bCheckValid = Opt.bCheckValid;
	m_nBaseBallInning = Opt.nBaseBallInning;
	m_bInsertKzone = Opt.bInsertPrism;

	//16-12-12 wgkim@esmlab.com
	m_bTemplatePoint = Opt.bTemplatePoint;

	//17-05-17
	m_bTemplateStabilization= Opt.bTemplateStabilization;
	m_bTemplateModify= Opt.bTemplateModify;

	//17-07-27
	m_bLightWeight = Opt.bLightWeight;
	
	//hjcho 160912
	m_bSyncSkip = Opt.bSyncSkip;
	m_nPrismValue = Opt.nPrismValue;
	m_bColorRivision = Opt.bColorRevision;
	//hjcho 161019
	m_nPrismValue2 = Opt.nPrismValue2;
	//hjcho 161212
	m_bInsertWhiteBal = Opt.bInsertWhiteBalance;
	//hjcho 170113
	m_bCheckAJALogo = Opt.bAJALogo;
	m_bCheckAJAReplay = Opt.bAJAReplay;
	//hjcho 170207
	m_bCheckAJAOnOff = Opt.bAJAOnOff;
	m_nAJASampleCnt = Opt.nAJASapleCnt;
	//hjcho 170418
	m_bPrnColorInfo = Opt.bPrnColorInfo;
	//hjcho 170706
	m_bGPUMakeSkip  = Opt.bGPUSkip;
	/*if(m_bEnableVMCC)
	{
		m_ctrlUHDtoFHD.SetCheck(1);
		//m_ctrlUHDtoFHD.EnableWindow(FALSE);
		m_bUHDtoFHD = TRUE;
	}
	else*/
	m_bUHDtoFHD = Opt.bUHDtoFHD;
	//m_bRamDiskSize = Opt.bGetRamDiskSize;
	m_bRamDiskSize = TRUE;

	m_bAutoDetect = Opt.bAutoDetect;
	//hjcho 170830
	m_bDirectMux = Opt.bDirectMux;

	//wgkim 171127
	m_bEncodingForSmartPhones = Opt.bEncodingForSmartPhones;
	m_bRecordFileSplit = Opt.bRecordFileSplit;

	//hjcho 171210
	m_nFrameZoomRatio = Opt.nFrameZoomRatio;

	//wgkim 180119
	m_nTemplateTargetOutPercent = Opt.nTemplateTargetOutPercent;
	
	//-- 2015-02-15 cygil@esmlab.com
	//-- Add picture coutdown
	m_nCountdownLastIP = Opt.nCountdownLastIP;
	if(m_bMode)
		m_BtnAgent.SetCheck(1);
	else
		m_BtnServer.SetCheck(1);

	if(m_bMakeServer)
		m_BtnMakeOn.SetCheck(1);
	else
		m_BtnMakeOff.SetCheck(1);

	if(m_bMode == ESM_MODESTATE_SERVER)
	{
		m_BtnServerMaking.EnableWindow(TRUE);
		m_BtnServerRecording.EnableWindow(TRUE);
		m_BtnServerBoth.EnableWindow(TRUE);
		if(m_bServerMode == ESM_SERVERMODE_MAKING)
		{
			m_BtnServerMaking.SetCheck(1);
			m_BtnServerRecording.SetCheck(0);
			m_BtnServerBoth.SetCheck(0);
		}
		else if(m_bServerMode == ESM_SERVERMODE_RECORDING)
		{
			m_BtnServerRecording.SetCheck(1);
			m_BtnServerMaking.SetCheck(0);
			m_BtnServerBoth.SetCheck(0);
		}
		else 
		{
			m_BtnServerMaking.SetCheck(0);
			m_BtnServerRecording.SetCheck(0);
			m_BtnServerBoth.SetCheck(1);
		}
		//jhhan
		m_EditWaitSaveDSCSub.EnableWindow(TRUE);
	}
	else
	{
		m_BtnServerMaking.EnableWindow(FALSE);
		m_BtnServerRecording.EnableWindow(FALSE);
		m_BtnServerBoth.EnableWindow(FALSE);
		//jhhan
		m_EditWaitSaveDSCSub.EnableWindow(FALSE);

	}

	m_n4DPMethod = Opt.n4DPMethod;

	if(m_bGPUMakeFile)
	{
		//GetDlgItem(IDC_OPT_CHECK_REFREE)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_RTSP)->EnableWindow(TRUE);

		m_bRTSP = Opt.bRTSP;
		//m_bRefereeRead = Opt.bRefereeRead;

		switch(m_n4DPMethod)
		{
		case 0:
			{
				m_btn4DP_ENC.SetCheck(0);
			}
			break;

		case 1:
			{
				m_btn4DP_ENC.SetCheck(1);
			}
			break;

		case  2:
			{
				m_btn4DP_ENC.SetCheck(2);
			}
			break;
		}
	}
	else
	{
		m_btn4DP_MUL.EnableWindow(FALSE);
		m_btn4DP_ENC.EnableWindow(FALSE);
		m_btn4DP_60p.EnableWindow(FALSE);
		//GetDlgItem(IDC_OPT_CHECK_REFREE)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHK_RTSP)->EnableWindow(FALSE);

		m_bRTSP = FALSE;
		//m_bRefereeRead = FALSE;

		//CheckDlgButton(IDC_CHK_RTSP,FALSE);
		//CheckDlgButton(IDC_OPT_CHECK_REFREE,FALSE);
	}

	m_bRefereeRead = Opt.bRefereeRead;
	m_bRemote = Opt.bRemote;
	char pchPath[MAX_PATH] = {0};
	int nRequiredSize = (int)wcstombs(pchPath, Opt.strRemote, MAX_PATH);
	m_ctlRemoteIp.SetAddress(htonl(inet_addr(pchPath)));
	m_bRemoteSkip = Opt.bRemoteSkip;
	m_bHorizontal = Opt.bHorizonAdj;

	//180508 hjcho
	m_bAJANetwork = Opt.bAJANetwork;
	nRequiredSize = (int)wcstombs(pchPath, Opt.strAJAIP, MAX_PATH);
	m_ipAJANetwork.SetAddress(htonl(inet_addr(pchPath)));

	//180629 hjcho
	m_bGIFMaking = Opt.bGIFMaking;
	m_nGIFQuaility = Opt.nGIFQuality;
	m_nGIFSize = Opt.nGIFSize;
	
	m_cbGIFQuality.SetCurSel(m_nGIFQuaility);
	m_cbGIFSize.SetCurSel(m_nGIFSize);	

	//180629 wgkim
	m_nSetFrameRateIndex = Opt.nSetFrameRateIndex;
	m_cbSetFrameRate.SetCurSel(m_nSetFrameRateIndex);

	//180718 hjcho
	//m_bRefereeRead = Opt.bRefereeRead;
	m_bUseReSync = Opt.bUseReSync;
	//jhhan 180828
	m_bAutoDelete = Opt.bAutoDelete;

	//wgkim 180921
	m_bAutoAdjustPositionTracking= Opt.bAutoAdjustPositionTracking;
	m_bAutoAdjustKZone= Opt.bAutoAdjustKZone;

	//jhhan 181029
	m_b4DAP = Opt.b4DAP;

	//wgkim 181119
	m_nSetTemplateModifyMode = Opt.nSetTemplateModifyMode;
	m_cbSetTemplateModifyMode.SetCurSel(m_nSetTemplateModifyMode);
	
	//hjcho 190103
	m_nEncodingSmartPhoneBitRate = Opt.nSmartPhoneBitrate;
	UpdateData(FALSE);

	m_ctrlBaseBall.AddString(_T("1st_start"));
	m_ctrlBaseBall.AddString(_T("1st_end"));
	m_ctrlBaseBall.AddString(_T("2nd_start"));
	m_ctrlBaseBall.AddString(_T("2nd_end"));
	m_ctrlBaseBall.AddString(_T("3rd_start"));
	m_ctrlBaseBall.AddString(_T("3rd_end"));
	m_ctrlBaseBall.AddString(_T("4th_start"));
	m_ctrlBaseBall.AddString(_T("4th_end"));
	m_ctrlBaseBall.AddString(_T("5th_start"));
	m_ctrlBaseBall.AddString(_T("5th_end"));
	m_ctrlBaseBall.AddString(_T("6th_start"));
	m_ctrlBaseBall.AddString(_T("6th_end"));
	m_ctrlBaseBall.AddString(_T("7th_start"));
	m_ctrlBaseBall.AddString(_T("7th_end"));
	m_ctrlBaseBall.AddString(_T("8th_start"));
	m_ctrlBaseBall.AddString(_T("8th_end"));
	m_ctrlBaseBall.AddString(_T("9th_start"));
	m_ctrlBaseBall.AddString(_T("9th_end"));
	m_ctrlBaseBall.AddString(_T("10th_start"));
	m_ctrlBaseBall.AddString(_T("10th_end"));
	m_ctrlBaseBall.AddString(_T("11th_start"));
	m_ctrlBaseBall.AddString(_T("11th_end"));
	m_ctrlBaseBall.AddString(_T("12th_start"));
	m_ctrlBaseBall.AddString(_T("12th_end"));

	m_ctrlBaseBall.SetCurSel(m_nBaseBallInning);

	GetDlgItem(IDC_STATIC_FRAME_PGSTATE)->				SetWindowPos(NULL,0,0,250,95,NULL);
	GetDlgItem(IDC_STATIC_MODE)->						SetWindowPos(NULL,17,29,50,24,NULL);
	//GetDlgItem(IDC_4DMAKER_SERVER)->					SetWindowPos(NULL,84,29,84,24,NULL);
	//GetDlgItem(IDC_4DMAKER_AGENT)->						SetWindowPos(NULL,167,29,84,24,NULL);
	GetDlgItem(IDC_STATIC_PORT)->						SetWindowPos(NULL,17,54,50,24,NULL);
	GetDlgItem(IDC_4DMAKER_PORT)->						SetWindowPos(NULL,84,54,147,24,NULL);

	GetDlgItem(IDC_STATIC_FRAME_MOVIE)->				SetWindowPos(NULL,0,100,250,280,NULL);
	GetDlgItem(IDC_ADJUST_INSERTCAMERAID)->				SetWindowPos(NULL,17,132,120,14,NULL);
	GetDlgItem(IDC_CHECK_ENABLE_VALID)->				SetWindowPos(NULL,17,156,110,14,NULL);
	GetDlgItem(IDC_CHECK_ENABLE_VMCC)->					SetWindowPos(NULL,17,180,110,14,NULL);
	GetDlgItem(IDC_CHECK_UHD_TO_FHD)->					SetWindowPos(NULL,17,204,110,14,NULL);
	GetDlgItem(IDC_CHECK_COLORREVISION)->				SetWindowPos(NULL,17,228,110,14,NULL);
	GetDlgItem(IDC_CHECK_ENCODING_FOR_SMARTPHONES)->	SetWindowPos(NULL,17,252,120,14,NULL);
	GetDlgItem(IDC_ADJUST_MAKEANDAUTOPLAY)->			SetWindowPos(NULL,17,276,120,14,NULL);
	GetDlgItem(IDC_CHECK_REVERSE_MOVIE)->				SetWindowPos(NULL,17,300,120,14,NULL);
	GetDlgItem(IDC_CHK_HORIZONTALADJ)->					SetWindowPos(NULL,17,324,120,14,NULL);
	GetDlgItem(IDC_CHECK_LIGHTWEIGHT)->					SetWindowPos(NULL,17,348,80,14,NULL);
	//GetDlgItem(IDC_CHK_TEMPLATE_STABILIZATION)->		SetWindowPos(NULL,124,348,120,14,NULL);
	GetDlgItem(IDC_CHK_TEMPLATE_STABILIZATION)->		SetWindowPos(NULL,17,324,120,14,NULL);

	//wgkim 180629
	GetDlgItem(IDC_COMBO_SET_FRAMERATE)->				SetWindowPos(NULL,100,348,120,14,NULL);
	GetDlgItem(IDC_STATIC_FRAMERATE)->					SetWindowPos(NULL,17,348,120,14,NULL);

	GetDlgItem(IDC_STATIC_FRAME_EVENTMOVIE)->			SetWindowPos(NULL,0,390,250,220,NULL);
	GetDlgItem(IDC_STATIC_FRAME_LOGOOPT)->				SetWindowPos(NULL,17,420,202,100,NULL);
	GetDlgItem(IDC_ADJUST_INSERTLOGO)->					SetWindowPos(NULL,37,444,120,24,NULL);
	GetDlgItem(IDC_CHECK_3DLOGO)->						SetWindowPos(NULL,37,468,120,24,NULL);
	GetDlgItem(IDC_CHECK_LOGOZOOM)->					SetWindowPos(NULL,37,492,120,24,NULL);

	GetDlgItem(IDC_STATIC_FRAME_REPEAT)->				SetWindowPos(NULL,17,530,202,60,NULL);
	GetDlgItem(IDC_CHK_REPEAT)->						SetWindowPos(NULL,37,558,14,14,NULL);
	GetDlgItem(IDC_MOVIE_REPEAT_CNT)->					SetWindowPos(NULL,58,553,80,24,NULL);
	
	GetDlgItem(IDC_STATIC_FRAME_CAMERA_SYNC)->			SetWindowPos(NULL,260,0,250,280,NULL);
	GetDlgItem(IDC_WAIT_SAVEDSC)->						SetWindowPos(NULL,380,27,87,24,NULL);
	GetDlgItem(IDC_WAIT_SAVEDSC_SUB)->					SetWindowPos(NULL,380,61,87,24,NULL);
	GetDlgItem(IDC_4DMAKER_SYNCTIME_MARGIN)->			SetWindowPos(NULL,380,101,87,24,NULL);
	GetDlgItem(IDC_DSC_SYNC_COUNT)->					SetWindowPos(NULL,380,141,87,24,NULL);
	GetDlgItem(IDC_MOVIE_DELETE_CNT)->					SetWindowPos(NULL,380,181,87,24,NULL);
	GetDlgItem(IDC_BTN_SYNC_SET)->						SetWindowPos(NULL,380,222,87,42,NULL);

	GetDlgItem(IDC_STATIC_CAP_DELAY)->					SetWindowPos(NULL,281,39,60,40,NULL);
	GetDlgItem(IDC_STATIC_A)->							SetWindowPos(NULL,350,32,14,14,NULL);
	GetDlgItem(IDC_STATIC_A_TIME)->						SetWindowPos(NULL,477,32,20,14,NULL);
	GetDlgItem(IDC_STATIC_CAP_WAIT)->					SetWindowPos(NULL,281,86,80,60,NULL);
	GetDlgItem(IDC_STATIC_B)->							SetWindowPos(NULL,350,66,14,14,NULL);
	GetDlgItem(IDC_STATIC_B_TIME)->						SetWindowPos(NULL,477,66,20,14,NULL);
	GetDlgItem(IDC_STATIC_CAP_TIME)->					SetWindowPos(NULL,477,106,20,14,NULL);
	GetDlgItem(IDC_STATIC_DSC_SYNCCOUNT)->				SetWindowPos(NULL,281,141,60,40,NULL);
	GetDlgItem(IDC_STATIC_MOVIE_DELETECNT)->			SetWindowPos(NULL,281,177,80,40,NULL);
	GetDlgItem(IDC_CHECK_SyncSkip)->					SetWindowPos(NULL,281,222,80,14,NULL);
	GetDlgItem(IDC_CHECK_GET_RAMDISK)->					SetWindowPos(NULL,281,238,80,14,NULL);
	GetDlgItem(IDC_CHECK_USE_RESYNC)->					SetWindowPos(NULL,281,254,90,14,NULL);

	GetDlgItem(IDC_STATIC_FRAME_AJA)->					SetWindowPos(NULL,260,280,250,60,NULL);
	GetDlgItem(IDC_OPT_AJA_ON)->						SetWindowPos(NULL,281,310,14,14,NULL);
	GetDlgItem(IDC_OPT_AJAIP)->							SetWindowPos(NULL,305,305,200,24,NULL);

	GetDlgItem(IDC_STATIC_FRAME_4DP_METHOD)->			SetWindowPos(NULL,260,350,120,140,NULL);
	GetDlgItem(IDC_CHK_4DPSKIP)->						SetWindowPos(NULL,281,373,90,14,NULL);
	GetDlgItem(IDC_CHECK_DIRECT_MUX)->					SetWindowPos(NULL,281,403,90,14,NULL);
	GetDlgItem(IDC_CHECK_ENABLE_4DP_COPY)->				SetWindowPos(NULL,281,433,100,14,NULL);

	GetDlgItem(IDC_STATIC_FRAME_4DP)->					SetWindowPos(NULL,390,350,120,140,NULL);
	GetDlgItem(IDC_CHK_GPU_MAKE)->						SetWindowPos(NULL,400,373,30,14,NULL);
	GetDlgItem(IDC_OPT_CHECK_REFREE)->					SetWindowPos(NULL,435,373,120,14,NULL);
	GetDlgItem(IDC_CHK_RTSP)->							SetWindowPos(NULL,435,390,120,14,NULL);
	//GetDlgItem(IDC_RADIO_4DP_MULGPU)->					SetWindowPos(NULL,410,397,90,14,NULL);
	//GetDlgItem(IDC_RADIO_4DP_ENCODE)->					SetWindowPos(NULL,410,421,90,14,NULL);
	//GetDlgItem(IDC_RADIO_4DP_60p)->						SetWindowPos(NULL,410,445,90,14,NULL);
	GetDlgItem(IDC_CHECK_REMOTE_SKIP)->					SetWindowPos(NULL,410,469,90,14,NULL);	

	GetDlgItem(IDC_STATIC_FRAME_TEMPLATE_POS)->			SetWindowPos(NULL,260,490,250,60,NULL);
	GetDlgItem(IDC_CHK_TEMPLATE)->						SetWindowPos(NULL,281,517,40,14,NULL);
	GetDlgItem(IDC_CHK_TEMPLATE_MODIFY)->				SetWindowPos(NULL,333,513,100,14,NULL);
	GetDlgItem(IDC_EDIT_TEMPLATE_TARGETOUT_PERCENT)->	SetWindowPos(NULL,433,512,44,24,NULL);
	GetDlgItem(IDC_STATIC_PERCENT)->					SetWindowPos(NULL,487,517,14,14,NULL);

	GetDlgItem(IDC_STATIC_FRAME_SERVERMAKE)->			SetWindowPos(NULL,260,550,120,60,NULL);
	//GetDlgItem(IDC_MAKE_SERVER_ON)->					SetWindowPos(NULL,281,578,50,14,NULL);
	//GetDlgItem(IDC_MAKE_SERVER_OFF)->					SetWindowPos(NULL,333,578,50,14,NULL);
	GetDlgItem(IDC_CHECK_AUTODETECTING)->				SetWindowPos(NULL,410,578,100,14,NULL);

	GetDlgItem(IDC_STATIC_GIF_GRP)->					SetWindowPos(NULL,130,146,115,100,NULL);
	GetDlgItem(IDC_CHK_GIF_MAKING)->					SetWindowPos(NULL,140,170,100,14,NULL);

	GetDlgItem(IDC_STATIC_GIF_QUAL)->					SetWindowPos(NULL,140,197,30,14,NULL);
	GetDlgItem(IDC_CBN_GIFQUALITY)->					SetWindowPos(NULL,170,190,70,14,NULL);

	GetDlgItem(IDC_STATIC_GIF_SIZE)->					SetWindowPos(NULL,140,220,30,14,NULL);
	GetDlgItem(IDC_CBN_GIF_SIZE)->						SetWindowPos(NULL,170,213,70,14,NULL);
	
	GetDlgItem(IDC_CHECK_DELETE)->						SetWindowPos(NULL,140,276,120,14,NULL);
	GetDlgItem(IDC_CHK_AUTOADJUST_POSITION_TRACKING)->	SetWindowPos(NULL,333,533,100,14,NULL);

	//wgkim 181005
	GetDlgItem(IDC_CHECK_AUTOADJUST_KZONE)->			SetWindowPos(NULL,410,598,100,14,NULL);

	//jhhan 181028
	GetDlgItem(IDC_CHECK_4DAP)->						SetWindowPos(NULL,281,463,90,14,NULL);

	//hjcho 190103
	GetDlgItem(IDC_OPT_EDIT_ENCBITRATE)->				SetWindowPos(NULL,140,247,30,20,NULL);
	GetDlgItem(IDC_OPT_STATIC_BITRATE)->				SetWindowPos(NULL,170,250,30,20,NULL);
}	

BOOL CESMOpt4DMaker::SaveProp(ESM4DMaker& Opt)
{
	if(!UpdateData(TRUE))
		return FALSE;

	Opt.bMakeServer = m_bMakeServer;
	Opt.bRCMode		= m_bMode;
	
	//Opt.nServerMode	= ESM_SERVERMODE_BOTH;
	
	Opt.nPort		= m_nPort;
	Opt.nDeleteCnt	= m_nDeleteCnt;
	Opt.nRepeatMovieCnt = m_nRepeatMovieCnt;
	Opt.nDSCSyncCnt = m_nDSCSyncCnt;
	
	Opt.nWaitSaveDSC	= m_nWaitSaveDSC;
	//jhhan
	Opt.nWaitSaveDSCSub	= m_nWaitSaveDSCSub;
	Opt.nWaitSaveDSCSel = m_nWaitSaveDSCSel;
	Opt.bViewInfo = m_bViewInfo;
	Opt.bTestMode = m_bTestMode;
	Opt.bMakeAndAotuPlay = m_bMakeAndAotuPlay;
	Opt.bInsertLogo = m_bInsertLogo;
	Opt.b3DLogo = m_b3DLogo;
	Opt.bLogoZoom = m_bLogoZoom;
	Opt.bInsertCameraID = m_bInsertCameraID;
	Opt.bTimeLinePreview  = m_bTimeLinePreview;
	Opt.nMovieSaveLocation	= m_nMovieSaveLocation;
	Opt.nSyncTimeMargin	= m_nSyncTimeMargin;
	Opt.nSensorOnTime	= m_nSensorOnTime;
	Opt.nTemplateEndMargin	= m_nTemplateEndMargin;
	Opt.nTemplateStartMargin	= m_nTemplateStartMargin;
	Opt.nTemplateStartSleep	= m_nTemplateStartSleep;
	Opt.nTemplateEndSleep	= m_nTemplateEndSleep;
	Opt.bEnableBaseBall = m_bEnableBaseBall;
	Opt.bReverseMovie = m_bReverseMovie;
	Opt.bGPUMakeFile = m_bGPUMakeFile;
	Opt.bRepeatMovie = m_bRepeatMovie;
	Opt.bEnableVMCC = m_bEnableVMCC;
	Opt.bEnableCopy = m_bEnableCopy;
	Opt.bUHDtoFHD = m_bUHDtoFHD;
	Opt.bSaveImg = m_bSaveImg;
	Opt.bCheckValid = m_bCheckValid;
	Opt.bInsertPrism = m_bInsertKzone;
	Opt.bColorRevision = m_bColorRivision;
	//-- 2015-02-15 cygil@esmlab.com
	//-- Add picture coutdown
	Opt.nCountdownLastIP	= m_nCountdownLastIP;

	//hjcho 160912
	Opt.bSyncSkip = m_bSyncSkip;
	Opt.nPrismValue = m_nPrismValue;
	//hjcho 161019
	Opt.nPrismValue2 = m_nPrismValue2;
	//hjcho 161212
	Opt.bInsertWhiteBalance = m_bInsertWhiteBal;

	//16-12-12 wgkim@esmlab.com
	Opt.bTemplatePoint = m_bTemplatePoint;

	//17-05-17
	Opt.bTemplateStabilization= m_bTemplateStabilization;
	Opt.bTemplateModify= m_bTemplateModify;

	//17-07-27
	Opt.bLightWeight = m_bLightWeight;

	//hjcho 170113
	Opt.bAJALogo = m_bCheckAJALogo;
	Opt.bAJAReplay = m_bCheckAJAReplay;

	//hjcho 170207
	Opt.bAJAOnOff = m_bCheckAJAOnOff;
	Opt.nAJASapleCnt = m_nAJASampleCnt;
	m_nBaseBallInning = m_ctrlBaseBall.GetCurSel();
	Opt.nBaseBallInning = m_nBaseBallInning;

	//hjcho 170418
	Opt.bPrnColorInfo = m_bPrnColorInfo;

	Opt.bGetRamDiskSize = m_bRamDiskSize;
	
	//hjcho 170706
	Opt.bGPUSkip = m_bGPUMakeSkip;
	//joonho.kim 170628
	Opt.nPCSyncTime			=	m_Opt.nPCSyncTime;
	Opt.nDSCSyncTime		=	m_Opt.nDSCSyncTime;
	Opt.nDSCSyncWaitTime	=	m_Opt.nDSCSyncWaitTime;

	Opt.nResyncRecWaitTime	=	m_Opt.nResyncRecWaitTime;

	Opt.bAutoDetect			= m_bAutoDetect;

	//hjcho 170830
	Opt.bDirectMux			= m_bDirectMux;

	//hjcho 170904
	Opt.n4DPMethod			= m_n4DPMethod;

	Opt.bRemote				= m_bRemote;
	DWORD dwIP;
	m_ctlRemoteIp.GetAddress(dwIP);
	Opt.strRemote.Format(_T("%d.%d.%d.%d"),FIRST_IPADDRESS(dwIP),SECOND_IPADDRESS(dwIP),THIRD_IPADDRESS(dwIP),FOURTH_IPADDRESS(dwIP));
	Opt.bRemoteSkip			= m_bRemoteSkip;
	Opt.bHorizonAdj			= m_bHorizontal;
	
	//17-07-27
	Opt.bEncodingForSmartPhones = m_bEncodingForSmartPhones;
	Opt.bRecordFileSplit = m_bRecordFileSplit;

	//hjcho 171210
	Opt.nFrameZoomRatio		= m_nFrameZoomRatio;

	//wgkim 180119
	Opt.nTemplateTargetOutPercent = m_nTemplateTargetOutPercent;

	//hjcho 180508
	m_ipAJANetwork.GetAddress(dwIP);
	Opt.strAJAIP.Format(_T("%d.%d.%d.%d"),FIRST_IPADDRESS(dwIP),SECOND_IPADDRESS(dwIP),THIRD_IPADDRESS(dwIP),FOURTH_IPADDRESS(dwIP));
	Opt.bAJANetwork	= m_bAJANetwork;

	//hjcho 180629
	Opt.bGIFMaking = m_bGIFMaking;
	Opt.nGIFQuality	= m_nGIFQuaility;
	Opt.nGIFSize = m_nGIFSize;

	//wgkim 180629
	Opt.nSetFrameRateIndex = m_nSetFrameRateIndex;

	//hjcho 180718 
	Opt.bRefereeRead = m_bRefereeRead;

	Opt.bUseReSync = m_bUseReSync;

	//jhhan 180907 - Delete 셋팅 이전의 촬영분 삭제 방지
	if(Opt.bAutoDelete  == FALSE)
	{
		if(m_bAutoDelete)
		{
			//ESMSetFrameRecord(_T(""));
			ESMSetDeleteFolder(_T(""));
		}
	}
	//jhhan 180828
	Opt.bAutoDelete = m_bAutoDelete;

	//wgkim 180921
	Opt.bAutoAdjustPositionTracking= m_bAutoAdjustPositionTracking;
	Opt.bAutoAdjustKZone= m_bAutoAdjustKZone;

	//hjcho 180927
	Opt.bRTSP = m_bRTSP;

	//jhan 181029
	Opt.b4DAP = m_b4DAP;

	//wgkim 181119
	Opt.nSetTemplateModifyMode= m_nSetTemplateModifyMode;

	Opt.nSmartPhoneBitrate	= m_nEncodingSmartPhoneBitRate;

	return TRUE;
}

BEGIN_MESSAGE_MAP(CESMOpt4DMaker, CESMOptBase)
	ON_BN_CLICKED(IDC_4DMAKER_SERVER, &CESMOpt4DMaker::OnBnClicked4dmakerRadio)
	ON_BN_CLICKED(IDC_4DMAKER_AGENT, &CESMOpt4DMaker::OnBnClicked4dmakerRadio)
	ON_BN_CLICKED(IDC_MAKE_SERVER_ON, &CESMOpt4DMaker::OnBnClickedMakeRadio)
	ON_BN_CLICKED(IDC_MAKE_SERVER_OFF, &CESMOpt4DMaker::OnBnClickedMakeRadio)
	ON_BN_CLICKED(IDC_4DSERVER_RECORING, &CESMOpt4DMaker::OnBnClicked4dserverRadio)
	ON_BN_CLICKED(IDC_4DSERVER_MAKING, &CESMOpt4DMaker::OnBnClicked4dserverRadio)
	ON_BN_CLICKED(IDC_4DSERVER_BOTH, &CESMOpt4DMaker::OnBnClicked4dserverRadio)
	ON_BN_CLICKED(IDC_CHECK_ENABLE_VMCC, &CESMOpt4DMaker::OnBnClickedCheckEnableVmcc)
	ON_BN_CLICKED(IDC_CHECK_ENABLE_4DP_COPY, &CESMOpt4DMaker::OnBnClickedCheckEnable4DPCopy)
	ON_BN_CLICKED(IDC_CHECK_ENABLE_SAVE_IMG, &CESMOpt4DMaker::OnBnClickedCheckEnableSaveImg)
	ON_BN_CLICKED(IDC_CHECK_BASE_BALL_MODE, &CESMOpt4DMaker::OnBnClickedCheckEnableBaseBall)
	ON_BN_CLICKED(IDC_CHECK_ENABLE_VALID, &CESMOpt4DMaker::OnBnClickedCheckEnableValid)
	ON_BN_CLICKED(IDC_BTN_SYNC_SET, &CESMOpt4DMaker::OnBnClickedBtnSyncSet)
	ON_BN_CLICKED(IDC_CHK_GPU_MAKE, &CESMOpt4DMaker::OnBnClickedChkGpuMake)
	ON_BN_CLICKED(IDC_RADIO_4DP_MULGPU, &CESMOpt4DMaker::OnBnClickedRadio4dpMulgpu)
	ON_BN_CLICKED(IDC_RADIO_4DP_ENCODE, &CESMOpt4DMaker::OnBnClickedRadio4dpMulgpu)
	ON_BN_CLICKED(IDC_RADIO_4DP_60p, &CESMOpt4DMaker::OnBnClickedRadio4dpMulgpu)
	ON_BN_CLICKED(IDC_CHECK_AUTODETECTING, &CESMOpt4DMaker::OnBnClickedCheckAutodetecting)
	ON_CBN_SELCHANGE(IDC_CBN_GIFQUALITY, &CESMOpt4DMaker::OnCbnSelchangeCbnGifquality)
	ON_CBN_SELCHANGE(IDC_CBN_GIF_SIZE, &CESMOpt4DMaker::OnCbnSelchangeCbnGifSize)
	ON_CBN_SELCHANGE(IDC_COMBO_SET_FRAMERATE, &CESMOpt4DMaker::OnCbnSelchangeCbnSetFrameRate)
	ON_WM_CTLCOLOR()
	ON_CBN_SELCHANGE(IDC_COMBO_SET_TEMPLATE_MODIFY, &CESMOpt4DMaker::OnSelchangeComboSetTemplateModify)
END_MESSAGE_MAP()

void CESMOpt4DMaker::OnBnClickedMakeRadio()
{
	m_bMakeServer = m_BtnMakeOn.GetCheck();

	if(m_bMakeServer == ESM_MAKE_SERVER_ON)
	{
		m_BtnMakeOff.SetCheck(0);
	}
	else
	{
		m_BtnMakeOff.SetCheck(1);
	}
}

void CESMOpt4DMaker::OnBnClicked4dmakerRadio()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bMode = m_BtnAgent.GetCheck();

	if(m_bMode == ESM_MODESTATE_SERVER)
	{
		m_BtnServerMaking.EnableWindow(TRUE);
		m_BtnServerRecording.EnableWindow(TRUE);
		m_BtnServerBoth.EnableWindow(TRUE);
		if(m_bServerMode == ESM_SERVERMODE_MAKING)
			m_BtnServerMaking.SetCheck(1);
		else if(m_bServerMode == ESM_SERVERMODE_RECORDING)
			m_BtnServerRecording.SetCheck(1);
		else 
			m_BtnServerBoth.SetCheck(1);

		//jhhan
		m_EditWaitSaveDSCSub.EnableWindow(TRUE);
	}
	else
	{
		m_BtnServerMaking.EnableWindow(FALSE);
		m_BtnServerRecording.EnableWindow(FALSE);
		m_BtnServerBoth.EnableWindow(FALSE);

		//jhhan
		m_EditWaitSaveDSCSub.EnableWindow(FALSE);
	}
}


void CESMOpt4DMaker::OnBnClicked4dserverRadio()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_BtnServerRecording.GetCheck() == TRUE)
		m_bServerMode = ESM_SERVERMODE_RECORDING;
	if(m_BtnServerMaking.GetCheck() == TRUE)
		m_bServerMode = ESM_SERVERMODE_MAKING;
	if(m_BtnServerBoth.GetCheck() == TRUE)
		m_bServerMode = ESM_SERVERMODE_BOTH;
}

void CESMOpt4DMaker::OnBnClickedCheckEnable4DPCopy()
{

}


void CESMOpt4DMaker::OnBnClickedCheckEnableVmcc()
{
	if(m_ctrlEnableVMCC.GetCheck())
	{
		m_ctrlUHDtoFHD.SetCheck(1);
		//m_ctrlUHDtoFHD.EnableWindow(FALSE);
	}
	else
	{
		//m_ctrlUHDtoFHD.EnableWindow(TRUE);
	}
}

void CESMOpt4DMaker::OnBnClickedCheckEnableValid()
{

}

void CESMOpt4DMaker::OnBnClickedCheckEnableSaveImg()
{
	
}

void CESMOpt4DMaker::OnBnClickedCheckEnableBaseBall()
{

}

void CESMOpt4DMaker::OnBnClickedBtnSyncSet()
{
	CESMOptSyncDlg syncDlg;

	syncDlg.InitProp(&m_Opt);
	syncDlg.DoModal();
}

void CESMOpt4DMaker::OnBnClickedChkGpuMake()
{
	UpdateData(TRUE);

	if(m_bGPUMakeFile)
	{
		GetDlgItem(IDC_RADIO_4DP_MULGPU)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO_4DP_ENCODE)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO_4DP_60p)->EnableWindow(TRUE);
		//GetDlgItem(IDC_OPT_CHECK_REFREE)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHK_RTSP)->EnableWindow(TRUE);
		
		/*switch(m_n4DPMethod)
		{
		case 0:
		{
		m_btn4DP_ENC.SetCheck(0);
		}
		break;

		case 1:
		{
		m_btn4DP_ENC.SetCheck(1);
		}
		break;

		case  2:
		{
		m_btn4DP_ENC.SetCheck(2);
		}
		break;
		}*/
	}
	else
	{
		GetDlgItem(IDC_RADIO_4DP_MULGPU)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO_4DP_ENCODE)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO_4DP_60p)->EnableWindow(FALSE);
		//GetDlgItem(IDC_OPT_CHECK_REFREE)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHK_RTSP)->EnableWindow(FALSE);
		
		m_bRTSP = FALSE;
		//m_bRefereeRead = FALSE;
		//CheckDlgButton(IDC_OPT_CHECK_REFREE,FALSE);
		//CheckDlgButton(IDC_CHK_RTSP,FALSE);
	}

	UpdateData(FALSE);
}

void CESMOpt4DMaker::OnBnClickedRadio4dpMulgpu()
{
	UpdateData(TRUE);
	//m_n4DPMethod = m_btn4DP_MUL.GetCheck();
	//ESMLog(5,_T("4DP Method [%d]"),m_n4DPMethod);
	UpdateData(FALSE);
		/*switch(m_n4DPMethod)
		{
		case 0:
		{
		m_btn4DP_ENC.SetCheck(0);
		}
		break;

		case 1:
		{
		m_btn4DP_ENC.SetCheck(1);
		}
		break;

		case  2:
		{
		m_btn4DP_ENC.SetCheck(2);
		}
		break;
		}*/
}


BOOL CESMOpt4DMaker::OnInitDialog()
{
	CESMOptBase::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_tooltip.Create(this);
	m_tooltip.AddTool(GetDlgItem(IDC_4DMAKER_SYNCTIME_MARGIN), _T("FHD:30000, UHD:35000"));
	
	m_tooltip.SetTipBkColor(RGB(255, 255, 0));
	m_tooltip.SetTipTextColor(RGB(255,0, 0));

	m_cbGIFQuality.AddString(_T("High"));
	m_cbGIFQuality.AddString(_T("Standard"));

	m_cbGIFSize.AddString(_T("FHD"));
	m_cbGIFSize.AddString(_T("HD"));
	m_cbGIFSize.AddString(_T("SD"));

	m_cbSetFrameRate.AddString(_T("30p"));
	m_cbSetFrameRate.AddString(_T("60p"));
	m_cbSetFrameRate.AddString(_T("25p"));
	m_cbSetFrameRate.AddString(_T("50p"));

	m_cbSetTemplateModifyMode.AddString(_T("None"));
	m_cbSetTemplateModifyMode.AddString(_T("Modify Zoom"));
	m_cbSetTemplateModifyMode.AddString(_T("Modify Coordinate"));

	m_btnSyncSet.ChangeFont(15);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CESMOpt4DMaker::PreTranslateMessage(MSG* pMsg)
{
	m_tooltip.RelayEvent(pMsg);

	return CESMOptBase::PreTranslateMessage(pMsg);
}


void CESMOpt4DMaker::OnBnClickedCheckAutodetecting()
{
	UpdateData(TRUE);

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message = WM_ESM_VIEW_AUTODETECT_INFO;
	pMsg->nParam1 = m_bAutoDetect;	// Template

	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);		
}


void CESMOpt4DMaker::OnCbnSelchangeCbnGifquality()
{
	int nIndex = m_cbGIFQuality.GetCurSel();
	m_nGIFQuaility = nIndex;
}


void CESMOpt4DMaker::OnCbnSelchangeCbnGifSize()
{
	int nIndex = m_cbGIFSize.GetCurSel();
	m_nGIFSize = nIndex;
}

//wgkim 180629
void CESMOpt4DMaker::OnCbnSelchangeCbnSetFrameRate()
{
	int nIndex = m_cbSetFrameRate.GetCurSel();
	m_nSetFrameRateIndex = nIndex;
}

HBRUSH CESMOpt4DMaker::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CESMOptBase::OnCtlColor(pDC, pWnd, nCtlColor);
	switch(nCtlColor)
	{
	case CTLCOLOR_LISTBOX:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(0,0,0));
			pDC->SetBkMode(TRANSPARENT);
		}
		break;
	}
	return hbr;
}


void CESMOpt4DMaker::OnSelchangeComboSetTemplateModify()
{
	int nIndex = m_cbSetTemplateModifyMode.GetCurSel();
	m_nSetTemplateModifyMode= nIndex;
}
