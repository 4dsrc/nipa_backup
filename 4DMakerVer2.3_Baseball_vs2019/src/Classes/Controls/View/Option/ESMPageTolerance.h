////////////////////////////////////////////////////////////////////////////////
//
//	TGPageTolerance.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-16
//
////////////////////////////////////////////////////////////////////////////////


#pragma once
#include "TGPropertyPage.h"
// CTGPageTolerance 대화 상자입니다.

class CTGPageTolerance : public CTGPropertyPage
{
	DECLARE_DYNAMIC(CTGPageTolerance)

	//-- 2011-11-29 hongsu.jung	
	int	m_nToleraceFps;	
	int m_nToleraceOCR;

	//-- 2012-08-16 joonho.kim
	int		m_nToleranceFpsPercent;
	int		m_nToleranceFpsMs;

	int		m_nTolerancePCPercent;
	int		m_nTolerancePCMs;

	CButton m_ChkFpsPercent;
	CButton m_ChkFpsMs;
	CEdit	m_edFpsPercent;
	CEdit	m_edFpsMs;

	CButton m_ChkPCPercent;
	CButton m_ChkPCMs;
	CEdit	m_edPCPercent;
	CEdit	m_edPCMs;

	int		m_nToleranceImgR;
	int		m_nToleranceImgG;
	int		m_nToleranceImgB;
	int		m_nToleranceLTColor;


	//-- 2011-12-2 hongsu.jung	
	int m_nTolerancePan;	
	int m_nToleranceTilt;
	int m_nToleranceZoom;
	int m_nToleranceFocus;
	
public:
	CTGPageTolerance();
	virtual ~CTGPageTolerance();
	afx_msg void OnCheckTolerance();
	afx_msg void OnPCCheckTolerance();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_PAGE_TOLERANCE };

	void InitProp(TGTolerance& tolerance);
	BOOL SaveProp(TGTolerance& tolerance);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()

	
};
