////////////////////////////////////////////////////////////////////////////////
//
//	TGPTZInfo.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

class CTGPTZInfo
{
	DECLARE_DYNAMIC(CTGPTZInfo)

public:
	CTGPTZInfo();
	virtual ~CTGPTZInfo();

public:
	int GetInfoValuetoInt(CString strItem,CString strKey);
	CString GetInfoValuetoString(CString strItem,CString strKey);
	BOOL SetInfoValue(CString strItem,CString strKey,CString strValue);
	
	void SetPTZInfoPreset(int nPreset, PTZ_COORDINATE tPTZPos);
	void GetPTZInfoPreset(int nPreset, PTZ_COORDINATE* tPTZPos);
	void SetPTZInfoTrace(int nTrace, PTZ_COORDINATE tPTZPos);
	void GetPTZInfoTrace(int nTrace, PTZ_COORDINATE* tPTZPos);

	BOOL ClearSequence(int nType, int nTypeNum);
	BOOL SetSequence(int nType, int nTypeNum, int nPreset);
	CString GetSequenceList(int nType, int nTypeNum);

	int m_nType;
	int m_nTypeNum;

private:
	CString InfoGetPath();
	int LoadInfoValueToInt(CString strSection, CString strKey);
	CString LoadInfoValueToString(CString strSection, CString strKey);
	int GetSectionCount(CString strModel, CString strItem);
	void GetSectionList(CString strModel, CString strItem, CStringArray& arList);

};