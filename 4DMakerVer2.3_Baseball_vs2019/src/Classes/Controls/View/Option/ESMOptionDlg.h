//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-05-07
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------

#pragma once

#include "ESMOption.h"

#include "ESMOptPath.h"
#include "ESMOpt4DMaker.h"
//#include "ESMOptMovie.h"
#include "ESMOptAdjust.h"
#include "ESMOptLog.h"
#include "ESMOptLogFilter.h"
#include "ESMOptCamInfo.h"
#include "ESMOptTemplate.h"
#include "ESMOptManagement.h"
#include "ESMOptCeremony.h"
#include "ESMOptAdjustMovie.h"
#include "ESMOptPCInfo.h"
#include "ESMOptSyncDlg.h"


enum ENV_OPTION
{
	ENV_PATH_SET,
	ENV_ADJUST_OPT,
	ENV_ADJUST_MOV,
	ENV_4DM_OPT,
	ENV_CAMERA_INFO,
	ENV_PC_INFO,
	ENV_TEMPLATE_OPT,
	ENV_CEREMONY,
	ENV_MANAGEMENT,
	ENV_LOG_OPTION,
	ENV_LOG_FILTER,
	ENV_MAX,
};
struct YUVMatrix{
	//2x3 matrix
	cv::Mat matY;
	cv::Mat matUV;
	
	//Y
	cv::Size szNewY;
	cv::Size szNewUV;

	//Point
	cv::Point ptNewY;
	cv::Point ptNewUV;

	//Original Size
	cv::Size szOriYImage;
	cv::Size szOriUVImage;
};

// CESMOptionDlg 대화 상자입니다.
class CESMOptionDlg : public CDialog
{
	DECLARE_DYNAMIC(CESMOptionDlg)

public:
	CESMOptionDlg(CESMOption* pOption, int nInitPage = IDD_OPTION_PATH, int nInitOpt = 0, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMOptionDlg();
	void FileSave(CString strFilename);
	void ExcelSave();
	void ExcelSavePC();
	void SelectTreeItem(int nID);

	BOOL Apply();
	void SetPathData(int nIndex, CString strPath);
	void SetBackupString(int nIndex, CString strPath);
	void SetAdjPathData(CString strPath);
// 대화 상자 데이터입니다.
	enum { IDD = IDD_VIEW_PROPERTY };
	BOOL m_bDlgCreated;

	// Page
	CESMOptPath				m_OptPath		;
	CESMOpt4DMaker			m_Opt4DMaker	;
#ifndef _4DMODEL
	CESMOptAdjust			m_OptAdjust		;	
#endif
	CESMOptAdjustMovie		m_OptAdjustMovie	;
	CESMOptLog				m_OptLog		;	
	CESMOptLogFilter		m_OptLogFilter	;
	CESMOptCamInfo			m_OptCamInfo	;
	ESMOptPCInfo			m_OptPCInfo	;
	CESMOptTemplate			m_OptTemplate	;
	CESMOptManagement		m_OptManagement	;
	CESMOptCeremony			m_OptCeremony	;

	CESMButtonEx/*CButton*/		m_btnOk;
	CESMButtonEx/*CButton*/		m_btnCancel;

	CBrush		m_brush;
	HICON		m_icon;
	// Tree Control
	CTreeCtrl	m_crtTree;
	// data struct pointer
	CESMOption*	m_pOption;
	//-- 2010-02-3 hongsu.jung
	//-- First Open Page
	int m_nInitPage;
	int m_nInitOpt;
	HTREEITEM m_hItem[ENV_MAX];

	virtual BOOL OnInitDialog();
	afx_msg void OnTvnSelchangedPropTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTvnSelchangingPropTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	YUVMatrix SetRotMatrix(int nIdx);
	CString WriteMatValue(YUVMatrix matrix);
	int Round(double dData);
};
