////////////////////////////////////////////////////////////////////////////////
//
//	ESMOpt4DMaker.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMOptTemplate.h"
#include "ESMOptPath.h"
#include "ESMFunc.h"
#include "MainFrm.h"
#include <string.h> //2016/07/15 Lee SangA

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMOptEffect 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOptTemplate, CESMOptBase)

CESMOptTemplate::CESMOptTemplate()
	: CESMOptBase(CESMOptTemplate::IDD)	
	/*, m_nPosX(0)
	, m_nPosY(0)
	, m_nWeight1(0)
	, m_nWeight2(0)
	, m_nRatio(0)*/
	, m_bRecovery(FALSE)
{
}

CESMOptTemplate::~CESMOptTemplate()
{
}

void CESMOptTemplate::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);

	DDX_Text(pDX,  IDC_PATH_CTRL_1, m_strTemplate1);	
	DDX_Text(pDX,  IDC_PATH_CTRL_2, m_strTemplate2);
	DDX_Text(pDX,  IDC_PATH_CTRL_3, m_strTemplate3);
	DDX_Text(pDX,  IDC_PATH_CTRL_4, m_strTemplate4);
	//CMiLRe 20151013 Template Load INI File
	DDX_Text(pDX,  IDC_PATH_CTRL_5, m_strTemplate5);
	DDX_Text(pDX,  IDC_PATH_CTRL_6, m_strTemplate6);
	DDX_Text(pDX,  IDC_PATH_CTRL_7, m_strTemplate7);
	DDX_Text(pDX,  IDC_PATH_CTRL_8, m_strTemplate8);
	DDX_Text(pDX,  IDC_PATH_CTRL_9, m_strTemplate9);
	DDX_Control(pDX, IDC_TAB4, m_Tab2);
	DDX_Control(pDX, IDC_STATIC1, m_txtNumkey1);
	DDX_Control(pDX, IDC_STATIC2, m_txtNumkey2);
	DDX_Control(pDX, IDC_STATIC3, m_txtNumkey3);
	DDX_Control(pDX, IDC_STATIC4, m_txtNumkey4);
	DDX_Control(pDX, IDC_STATIC5, m_txtNumkey5);
	DDX_Control(pDX, IDC_STATIC6, m_txtNumkey6);
	DDX_Control(pDX, IDC_STATIC7, m_txtNumkey7);
	DDX_Control(pDX, IDC_STATIC8, m_txtNumkey8);
	DDX_Control(pDX, IDC_STATIC9, m_txtNumkey9);
	DDX_Check(pDX, IDC_CHECK_RECOVERY, m_bRecovery);
	DDX_Control(pDX, IDC_PATH_CTRL_1, m_ctrlPath1);
	DDX_Control(pDX, IDC_PATH_CTRL_2, m_ctrlPath2);
	DDX_Control(pDX, IDC_PATH_CTRL_3, m_ctrlPath3);
	DDX_Control(pDX, IDC_PATH_CTRL_4, m_ctrlPath4);
	DDX_Control(pDX, IDC_PATH_CTRL_5, m_ctrlPath5);
	DDX_Control(pDX, IDC_PATH_CTRL_6, m_ctrlPath6);
	DDX_Control(pDX, IDC_PATH_CTRL_7, m_ctrlPath7);
	DDX_Control(pDX, IDC_PATH_CTRL_8, m_ctrlPath8);
	DDX_Control(pDX, IDC_PATH_CTRL_9, m_ctrlPath9);
	DDX_Control(pDX, IDC_BUTTON_CTRL_1, m_btnCtrl1);
	DDX_Control(pDX, IDC_BUTTON_CTRL_2, m_btnCtrl2);
	DDX_Control(pDX, IDC_BUTTON_CTRL_3, m_btnCtrl3);
	DDX_Control(pDX, IDC_BUTTON_CTRL_4, m_btnCtrl4);
	DDX_Control(pDX, IDC_BUTTON_CTRL_5, m_btnCtrl5);
	DDX_Control(pDX, IDC_BUTTON_CTRL_6, m_btnCtrl6);
	DDX_Control(pDX, IDC_BUTTON_CTRL_7, m_btnCtrl7);
	DDX_Control(pDX, IDC_BUTTON_CTRL_8, m_btnCtrl8);
	DDX_Control(pDX, IDC_BUTTON_CTRL_9, m_btnCtrl9);
}

BEGIN_MESSAGE_MAP(CESMOptTemplate, CESMOptBase)
	ON_BN_CLICKED(IDC_BUTTON_CTRL_1, &CESMOptTemplate::OnBnClickedCtrl1)
	ON_BN_CLICKED(IDC_BUTTON_CTRL_2, &CESMOptTemplate::OnBnClickedCtrl2)
	ON_BN_CLICKED(IDC_BUTTON_CTRL_3, &CESMOptTemplate::OnBnClickedCtrl3)
	ON_BN_CLICKED(IDC_BUTTON_CTRL_4, &CESMOptTemplate::OnBnClickedCtrl4)
	//CMiLRe 20151013 Template Load INI File
	ON_BN_CLICKED(IDC_BUTTON_CTRL_5, &CESMOptTemplate::OnBnClickedCtrl5)
	ON_BN_CLICKED(IDC_BUTTON_CTRL_6, &CESMOptTemplate::OnBnClickedCtrl6)
	ON_BN_CLICKED(IDC_BUTTON_CTRL_7, &CESMOptTemplate::OnBnClickedCtrl7)
	ON_BN_CLICKED(IDC_BUTTON_CTRL_8, &CESMOptTemplate::OnBnClickedCtrl8)
	ON_BN_CLICKED(IDC_BUTTON_CTRL_9, &CESMOptTemplate::OnBnClickedCtrl9)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB4, &CESMOptTemplate::OnTcnSelchangeTab4)
	ON_WM_CTLCOLOR()
	ON_EN_CHANGE(IDC_PATH_CTRL_1, &CESMOptTemplate::OnEnChangePathCtrl1)
	ON_EN_CHANGE(IDC_PATH_CTRL_2, &CESMOptTemplate::OnEnChangePathCtrl2)
//	ON_EN_KILLFOCUS(IDC_PATH_CTRL_1, &CESMOptTemplate::OnEnKillfocusPathCtrl1)
//	ON_EN_KILLFOCUS(IDC_PATH_CTRL_2, &CESMOptTemplate::OnEnKillfocusPathCtrl2)
ON_EN_CHANGE(IDC_PATH_CTRL_3, &CESMOptTemplate::OnEnChangePathCtrl3)
ON_EN_CHANGE(IDC_PATH_CTRL_4, &CESMOptTemplate::OnEnChangePathCtrl4)
ON_EN_CHANGE(IDC_PATH_CTRL_5, &CESMOptTemplate::OnEnChangePathCtrl5)
ON_EN_CHANGE(IDC_PATH_CTRL_6, &CESMOptTemplate::OnEnChangePathCtrl6)
ON_EN_CHANGE(IDC_PATH_CTRL_7, &CESMOptTemplate::OnEnChangePathCtrl7)
ON_EN_CHANGE(IDC_PATH_CTRL_8, &CESMOptTemplate::OnEnChangePathCtrl8)
ON_EN_CHANGE(IDC_PATH_CTRL_9, &CESMOptTemplate::OnEnChangePathCtrl9)
END_MESSAGE_MAP()

void CESMOptTemplate::InitProp(ESMTemplate& Opt)
{
	m_Tab2.colorEx.SetFont(IDX_FONT_SMALL, _T("Noto Sans CJK KR Regular"), 9);
	m_Tab2.ModifyStyle(TCS_SCROLLOPPOSITE|TCS_BUTTONS, TCS_OWNERDRAWFIXED|TCS_FIXEDWIDTH);
	m_Tab2.InsertItem(0,  _T("F1"), 0); 
	m_Tab2.InsertItem(1,  _T("F2"), 1);
	m_Tab2.InsertItem(2,  _T("F3"), 2);
	m_Tab2.InsertItem(3,  _T("F4"), 3);
	m_Tab2.InsertItem(4,  _T("F5"), 4);
	m_Tab2.SetColor(RGB(255,255,255), RGB(44,44,44), RGB(0x0e,0x0e,0x0e), RGB(44,44,44));
	m_Tab2.SetItemSize(CSize(80,28));
	

	for (int i=0 ; i<5 ; i++)
	{
		m_contents[i].path[0] = Opt.strTemplate[i][0];
		m_contents[i].path[1] = Opt.strTemplate[i][1];
		m_contents[i].path[2] = Opt.strTemplate[i][2];
		m_contents[i].path[3] = Opt.strTemplate[i][3];
		m_contents[i].path[4] = Opt.strTemplate[i][4];
		m_contents[i].path[5] = Opt.strTemplate[i][5];
		m_contents[i].path[6] = Opt.strTemplate[i][6];
		m_contents[i].path[7] = Opt.strTemplate[i][7];
		m_contents[i].path[8] = Opt.strTemplate[i][8];
	}

	m_Tab2.SetCurSel(_ttoi(Opt.strSelectPage));
	curPageNum = this->m_Tab2.GetCurSel();

	if(curPageNum>5 || curPageNum<0) { //F1, F2, F3, F4, F5키를 누른 후 바로 옵션 버튼을 클릭할 때 curPageNum이 -1이 되는 문제 방지
		CMainFrame* pMainWnd = 	(CMainFrame*)AfxGetMainWnd();
		curPageNum = pMainWnd->m_wndDSCViewer.m_pListView->m_iPage;
		this->m_Tab2.SetCurSel(curPageNum);
	}

	m_strTemplate1 = Opt.strTemplate[curPageNum][0];
	m_strTemplate2 = Opt.strTemplate[curPageNum][1];
	m_strTemplate3 = Opt.strTemplate[curPageNum][2];
	m_strTemplate4 = Opt.strTemplate[curPageNum][3];
	m_strTemplate5 = Opt.strTemplate[curPageNum][4];
	m_strTemplate6 = Opt.strTemplate[curPageNum][5];
	m_strTemplate7 = Opt.strTemplate[curPageNum][6];
	m_strTemplate8 = Opt.strTemplate[curPageNum][7];
	m_strTemplate9 = Opt.strTemplate[curPageNum][8];

	m_bRecovery = Opt.bRecovery;
	GetDlgItem(IDC_STATIC10)->	SetWindowPos(NULL,23,45,80,24,NULL);

	GetDlgItem(IDC_TAB4)->			SetWindowPos(NULL,0,0,490,482,NULL);
	GetDlgItem(IDC_STATIC1)->		SetWindowPos(NULL,23,86,24,24,NULL);
	GetDlgItem(IDC_PATH_CTRL_1)->	SetWindowPos(NULL,53,86,312,24,NULL);
	GetDlgItem(IDC_BUTTON_CTRL_1)->	SetWindowPos(NULL,380,86,80,24,NULL);

	GetDlgItem(IDC_STATIC2)->		SetWindowPos(NULL,23,130,24,24,NULL);
	GetDlgItem(IDC_PATH_CTRL_2)->	SetWindowPos(NULL,53,130,312,24,NULL);
	GetDlgItem(IDC_BUTTON_CTRL_2)->	SetWindowPos(NULL,380,130,80,24,NULL);

	GetDlgItem(IDC_STATIC3)->		SetWindowPos(NULL,23,174,24,24,NULL);
	GetDlgItem(IDC_PATH_CTRL_3)->	SetWindowPos(NULL,53,174,312,24,NULL);
	GetDlgItem(IDC_BUTTON_CTRL_3)->	SetWindowPos(NULL,380,174,80,24,NULL);

	GetDlgItem(IDC_STATIC4)->		SetWindowPos(NULL,23,218,24,24,NULL);
	GetDlgItem(IDC_PATH_CTRL_4)->	SetWindowPos(NULL,53,218,312,24,NULL);
	GetDlgItem(IDC_BUTTON_CTRL_4)->	SetWindowPos(NULL,380,218,80,24,NULL);

	GetDlgItem(IDC_STATIC5)->		SetWindowPos(NULL,23,262,24,24,NULL);
	GetDlgItem(IDC_PATH_CTRL_5)->	SetWindowPos(NULL,53,262,312,24,NULL);
	GetDlgItem(IDC_BUTTON_CTRL_5)->	SetWindowPos(NULL,380,262,80,24,NULL);

	GetDlgItem(IDC_STATIC6)->		SetWindowPos(NULL,23,306,24,24,NULL);
	GetDlgItem(IDC_PATH_CTRL_6)->	SetWindowPos(NULL,53,306,312,24,NULL);
	GetDlgItem(IDC_BUTTON_CTRL_6)->	SetWindowPos(NULL,380,306,80,24,NULL);

	GetDlgItem(IDC_STATIC7)->		SetWindowPos(NULL,23,350,24,24,NULL);
	GetDlgItem(IDC_PATH_CTRL_7)->	SetWindowPos(NULL,53,350,312,24,NULL);
	GetDlgItem(IDC_BUTTON_CTRL_7)->	SetWindowPos(NULL,380,350,80,24,NULL);

	GetDlgItem(IDC_STATIC8)->		SetWindowPos(NULL,23,394,24,24,NULL);
	GetDlgItem(IDC_PATH_CTRL_8)->	SetWindowPos(NULL,53,394,312,24,NULL);
	GetDlgItem(IDC_BUTTON_CTRL_8)->	SetWindowPos(NULL,380,394,80,24,NULL);

	GetDlgItem(IDC_STATIC9)->		SetWindowPos(NULL,23,438,24,24,NULL);
	GetDlgItem(IDC_PATH_CTRL_9)->	SetWindowPos(NULL,53,438,312,24,NULL);
	GetDlgItem(IDC_BUTTON_CTRL_9)->	SetWindowPos(NULL,380,438,80,24,NULL);

	GetDlgItem(IDC_CHECK_RECOVERY)->	SetWindowPos(NULL,0,507,80,14,NULL);

	m_btnCtrl1.ChangeFont(15);
	m_btnCtrl2.ChangeFont(15);
	m_btnCtrl3.ChangeFont(15);
	m_btnCtrl4.ChangeFont(15);
	m_btnCtrl5.ChangeFont(15);
	m_btnCtrl6.ChangeFont(15);
	m_btnCtrl7.ChangeFont(15);
	m_btnCtrl8.ChangeFont(15);
	m_btnCtrl9.ChangeFont(15);
	UpdateData(FALSE);
}

//Save the contents of [Template Option]
BOOL CESMOptTemplate::SaveProp(ESMTemplate& Opt)
{
	if(!UpdateData(TRUE))
		return FALSE;

	//2016/07/11 Lee SangA page update
	for(int i=0; i<5; i++)
	{
		Opt.strTemplate[i][0] = m_contents[i].path[0];
		Opt.strTemplate[i][1] = m_contents[i].path[1];
		Opt.strTemplate[i][2] = m_contents[i].path[2];
		Opt.strTemplate[i][3] = m_contents[i].path[3];
		Opt.strTemplate[i][4] = m_contents[i].path[4];
		Opt.strTemplate[i][5] = m_contents[i].path[5];
		Opt.strTemplate[i][6] = m_contents[i].path[6];
		Opt.strTemplate[i][7] = m_contents[i].path[7];
		Opt.strTemplate[i][8] = m_contents[i].path[8];
	}

	CString strSelectPage;
	strSelectPage.Format(_T("%d"), m_Tab2.GetCurSel());

	Opt.strSelectPage = strSelectPage;

	Opt.bRecovery = m_bRecovery;

	return TRUE;
}

void CESMOptTemplate::OnBnClickedCtrl1()
{

	//load (browse button clicked)
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	CString szFilter = _T("Movie Template File (*.mvtm)|*.mvtm|");


	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK ) //file 선택했을 때
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		m_strTemplate1 = strFileName;
		UpdateData(FALSE);

		m_contents[curPageNum].path[0] = strFileName;
	}
	
}

void CESMOptTemplate::OnBnClickedCtrl2()
{
	//load
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	CString szFilter = _T("Movie Template File (*.mvtm)|*.mvtm|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		m_strTemplate2 = strFileName;
		UpdateData(FALSE);

		m_contents[curPageNum].path[1] = strFileName;
	}
}

void CESMOptTemplate::OnBnClickedCtrl3()
{
	//load
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	CString szFilter = _T("Movie Template File (*.mvtm)|*.mvtm|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		m_strTemplate3 = strFileName;
		UpdateData(FALSE);

		m_contents[curPageNum].path[2] = strFileName;
	}
}

void CESMOptTemplate::OnBnClickedCtrl4()
{
	//load
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	CString szFilter = _T("Movie Template File (*.mvtm)|*.mvtm|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		m_strTemplate4 = strFileName;
		UpdateData(FALSE);

		m_contents[curPageNum].path[3] = strFileName;
	}
}

//CMiLRe 20151013 Template Load INI File
void CESMOptTemplate::OnBnClickedCtrl5()
{
	//load
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	CString szFilter = _T("Movie Template File (*.mvtm)|*.mvtm|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		m_strTemplate5 = strFileName;
		UpdateData(FALSE);

		m_contents[curPageNum].path[4] = strFileName;
	}
}

//CMiLRe 20151013 Template Load INI File
void CESMOptTemplate::OnBnClickedCtrl6()
{
	//load
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	CString szFilter = _T("Movie Template File (*.mvtm)|*.mvtm|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		m_strTemplate6 = strFileName;
		UpdateData(FALSE);

		m_contents[curPageNum].path[5] = strFileName;
	}
}

//CMiLRe 20151013 Template Load INI File
void CESMOptTemplate::OnBnClickedCtrl7()
{
	//load
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	CString szFilter = _T("Movie Template File (*.mvtm)|*.mvtm|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		m_strTemplate7 = strFileName;
		UpdateData(FALSE);

		m_contents[curPageNum].path[6] = strFileName;
	}
}

//CMiLRe 20151013 Template Load INI File
void CESMOptTemplate::OnBnClickedCtrl8()
{
	//load
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	CString szFilter = _T("Movie Template File (*.mvtm)|*.mvtm|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		m_strTemplate8 = strFileName;
		UpdateData(FALSE);

		m_contents[curPageNum].path[7] = strFileName;
	}
}

//CMiLRe 20151013 Template Load INI File
void CESMOptTemplate::OnBnClickedCtrl9()
{
	//load
	CString strEffectInfoFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strEffectInfoFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	CString szFilter = _T("Movie Template File (*.mvtm)|*.mvtm|");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strEffectInfoFolder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strEffectInfoFolder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		m_strTemplate9 = strFileName;
		UpdateData(FALSE);

		m_contents[curPageNum].path[8] = strFileName;
	}
}




HBRUSH CESMOptTemplate::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CESMOptBase::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	switch(nCtlColor)
	{

	case CTLCOLOR_STATIC:
		{

			if(pWnd->GetDlgCtrlID() == IDC_STATIC0)
			{
				pDC->SetTextColor(RGB(0, 0, 0));
				pDC->SetBkColor(RGB(240, 240, 240));
				return (HBRUSH)GetStockObject(NULL_BRUSH);
			}



			if(pWnd->GetDlgCtrlID() == IDC_STATIC1)
			{
				pDC->SetTextColor(RGB(255, 255, 255));
				//pDC->SetBkMode(TRANSPARENT);
				return (HBRUSH)GetStockObject(NULL_BRUSH);
			}


			if(pWnd->GetDlgCtrlID() == IDC_STATIC2)
			{
				pDC->SetTextColor(RGB(255, 255, 255));
				//pDC->SetBkMode(TRANSPARENT);
				return (HBRUSH)GetStockObject(NULL_BRUSH);
			}


			if(pWnd->GetDlgCtrlID() == IDC_STATIC3)
			{
				pDC->SetTextColor(RGB(255, 255, 255));
				//pDC->SetBkMode(TRANSPARENT);
				return (HBRUSH)GetStockObject(NULL_BRUSH);
			}


			if(pWnd->GetDlgCtrlID() == IDC_STATIC4)
			{
				pDC->SetTextColor(RGB(255, 255, 255));
				//pDC->SetBkMode(TRANSPARENT);
				return (HBRUSH)GetStockObject(NULL_BRUSH);
			}


			if(pWnd->GetDlgCtrlID() == IDC_STATIC5)
			{
				pDC->SetTextColor(RGB(255, 255, 255));
				//pDC->SetBkMode(TRANSPARENT);
				return (HBRUSH)GetStockObject(NULL_BRUSH);
			}


			if(pWnd->GetDlgCtrlID() == IDC_STATIC6)
			{
				pDC->SetTextColor(RGB(255, 255, 255));
				//pDC->SetBkMode(TRANSPARENT);
				return (HBRUSH)GetStockObject(NULL_BRUSH);
			}


			if(pWnd->GetDlgCtrlID() == IDC_STATIC7)
			{
				pDC->SetTextColor(RGB(255, 255, 255));
				//pDC->SetBkMode(TRANSPARENT);
				return (HBRUSH)GetStockObject(NULL_BRUSH);
			}


			if(pWnd->GetDlgCtrlID() == IDC_STATIC8)
			{
				pDC->SetTextColor(RGB(255, 255, 255));
				//pDC->SetBkMode(TRANSPARENT);
				return (HBRUSH)GetStockObject(NULL_BRUSH);
			}


			if(pWnd->GetDlgCtrlID() == IDC_STATIC9)
			{
				pDC->SetTextColor(RGB(255, 255, 255));
				//pDC->SetBkMode(TRANSPARENT);
				return (HBRUSH)GetStockObject(NULL_BRUSH);
			}

			if(pWnd->GetDlgCtrlID() == IDC_STATIC10)
			{
				pDC->SetTextColor(RGB(255, 255, 255));
				//pDC->SetBkMode(TRANSPARENT);
				return (HBRUSH)GetStockObject(NULL_BRUSH);
			}
			
		}
	}


	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}



//2016/07/25
void CESMOptTemplate::OnTcnSelchangeTab4(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	TRACE(_T("Change Tab\n"));
	CString strPage;
	curPageNum = m_Tab2.GetCurSel();

	this->m_strTemplate1 = m_contents[curPageNum].path[0];
	this->m_strTemplate2 = m_contents[curPageNum].path[1];
	this->m_strTemplate3 = m_contents[curPageNum].path[2];
	this->m_strTemplate4 = m_contents[curPageNum].path[3];
	this->m_strTemplate5 = m_contents[curPageNum].path[4];
	this->m_strTemplate6 = m_contents[curPageNum].path[5];
	this->m_strTemplate7 = m_contents[curPageNum].path[6];
	this->m_strTemplate8 = m_contents[curPageNum].path[7];
	this->m_strTemplate9 = m_contents[curPageNum].path[8];


	UpdateData(FALSE);

	*pResult = 0;


}

void CESMOptTemplate::OnEnChangePathCtrl1()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CESMOptBase::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.	UpdateData(FALSE);
	UpdateData(TRUE);
	TRACE(_T("Change path control1\n"));
	curPageNum = m_Tab2.GetCurSel();
	
	
	m_contents[curPageNum].path[0] = m_strTemplate1.GetString();

	
	

}


void CESMOptTemplate::OnEnChangePathCtrl2()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CESMOptBase::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	TRACE(_T("Change path control2\n"));
	
	curPageNum = m_Tab2.GetCurSel();
	m_contents[curPageNum].path[1] = m_strTemplate2.GetString();
}



void CESMOptTemplate::OnEnChangePathCtrl3()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CESMOptBase::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	TRACE(_T("Change path control3\n"));

	curPageNum = m_Tab2.GetCurSel();
	m_contents[curPageNum].path[2] = m_strTemplate3.GetString();
}


void CESMOptTemplate::OnEnChangePathCtrl4()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CESMOptBase::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	TRACE(_T("Change path control4\n"));

	curPageNum = m_Tab2.GetCurSel();
	m_contents[curPageNum].path[3] = m_strTemplate4.GetString();
}


void CESMOptTemplate::OnEnChangePathCtrl5()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CESMOptBase::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	TRACE(_T("Change path control5\n"));

	curPageNum = m_Tab2.GetCurSel();
	m_contents[curPageNum].path[4] = m_strTemplate5.GetString();
}


void CESMOptTemplate::OnEnChangePathCtrl6()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CESMOptBase::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	TRACE(_T("Change path control6\n"));

	curPageNum = m_Tab2.GetCurSel();
	m_contents[curPageNum].path[5] = m_strTemplate6.GetString();
}

void CESMOptTemplate::OnEnChangePathCtrl7()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CESMOptBase::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	TRACE(_T("Change path control7\n"));

	curPageNum = m_Tab2.GetCurSel();
	m_contents[curPageNum].path[6] = m_strTemplate7.GetString();
}



void CESMOptTemplate::OnEnChangePathCtrl8()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CESMOptBase::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	TRACE(_T("Change path control8\n"));

	curPageNum = m_Tab2.GetCurSel();
	m_contents[curPageNum].path[7] = m_strTemplate8.GetString();
}


void CESMOptTemplate::OnEnChangePathCtrl9()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CESMOptBase::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	TRACE(_T("Change path control9\n"));

	curPageNum = m_Tab2.GetCurSel();
	m_contents[curPageNum].path[8] = m_strTemplate9.GetString();
}



