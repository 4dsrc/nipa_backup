/////////////////////////////////////////////////////////////////////////////
//
//  ESMProperties.h : implementation file
//
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, Inc., and may not be copied
//  nor disclosed except upon written agreement
//  by ESMLab, Inc..
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2009-02-01
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#if (!defined __AFXPRIV_H__)
	#include <AfxPriv.h>
#endif

#define MAX_PROP	3


/////////////////////////////////////////////////////////////////////////////
// CESMPropertyGridCtrl window

class CESMPropertyGridCtrl : public CExtPropertyGridCtrl
{
	CExtPropertyStore m_PS;

public:
	CESMPropertyGridCtrl();
	~CESMPropertyGridCtrl();


protected:
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam );


private:
	//------------------------------------------------------
	//-- Basic Execution Option
	//------------------------------------------------------
	CExtPropertyItem* m_pCategoryExecution;

	CExtPropertyItem* m_pItemType; 
	CExtPropertyItem* m_pItemOpt; 
	CExtPropertyItem* m_pItemCount; 
	CExtPropertyItem* m_pItemRetryCnt; 
	//-- ComboBox
	CExtPropertyItem* m_pItemFailProgress;	
	CExtGridCellComboBox* m_pComboFailProgress;
	//-- GotoOption
	CExtGridCellComboBox* m_pComboGotoOption;
	//------------------------------------------------------
	//-- Property
	//------------------------------------------------------
	CExtPropertyItem* m_pCategoryProperty;
	CExtPropertyItem* m_pCategoryVideo;
	CExtGridCellComboBox* m_pComboCodecType;

	//-- GET POINT
	CTestBase*	m_pTestBase;
	CTestStep*	m_pTestStep;
	BOOL		m_bTeststep;

	BOOL SetPropertyComboBox(CExtPropertyValue* pItem, vector<CString> vList, CString strCurrentName);
	BOOL SetPropertyComboBox(CExtPropertyValue* pItem, const CString *strList, DWORD dwListSize, CString strCurrentName);

public:
	//------------------------------------------------------
	//-- Member Function
	//------------------------------------------------------
	BOOL IsSame(CString strA, CString strB);
	
	//-- BASE DATA 
	void InitExecuteOption();	
	void LoadExecuteOption();
	void LoadBaseData(CTestBase* pTestBase);
	void CheckRetryCount();

	//-- STEP DATA
	void LoadStepData(CTestStep* pTestStep, tstring strTC = _T(""), int nStep = -1);
	void LoadStepExeOpt(CTestStep* pTestStep);

	//-- 2009-04-28	
	void LoadCtl(void);	
	void LoadGoto(void);
	void LoadImg(void);
	
	//-- 2011-11-7 Jeongyong.kim
	void LoadMovie(void);
	//-- 2009-05-06
	void LoadImage(tstring strTC, int nStep);
	//-- 2011-3-2 keunbae.song
	void SetNoInplace(CExtPropertyValue* pItem, CString strText, BOOL bNoInplace=TRUE, COLORREF refColor=RGB(0, 0, 0));

	//-- 2009-11-9 hongsu.jung
	void LoadMlc(void);		
	//-- 2009-12-29 hongsu.jung
	void LoadDebug(void);
	void LoadNVRVideoData();
	

	void RemovePropertyItem();
	//-- INPUT COMPLETE
	virtual void OnPgcInputComplete (CExtPropertyGridWnd * pPGW, CExtPropertyItem * pPropertyItem);
	//-- 2011-2-18 keunbae.song
	void OnPgcInputComplete_RAN();

	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
}; 

