////////////////////////////////////////////////////////////////////////////////
//
//	TGPageWebOption.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	keunbae.song
// @Date	  2012-07-02
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "TGPropertyPage.h"
#include "afxcmn.h"

class CTGPageWebOption : public CTGPropertyPage
{
	DECLARE_DYNAMIC(CTGPageWebOption)
	DECLARE_MESSAGE_MAP()

public:
	CTGPageWebOption();
	virtual ~CTGPageWebOption();

	enum { IDD = IDD_OPTION_PAGE_WEBOPTION };

	void InitProp(TGWebOpt& opt);
	BOOL SaveProp(TGWebOpt& opt);

public:	
	CString m_strAgentIP;
	int m_nAgentPort;
	CString m_strLogLevel;
	CString m_strBrowserType;

private:
	CIPAddressCtrl m_ipcAgentIP;
	//BOOL m_bAgentAlive;
	CComboBox m_cmbLogLevel;
	int m_nBrowserType;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
};
