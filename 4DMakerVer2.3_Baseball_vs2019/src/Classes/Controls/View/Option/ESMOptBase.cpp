////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptBase.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-07
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMOptBase.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//-- CALLBACK
static int CALLBACK BrowseCallbackProc(
									   HWND hWnd, 
									   UINT uMsg, 
									   LPARAM lParam, 
									   LPARAM lpData
									   )
{
	TCHAR szPath[ MAX_PATH ] = _T("");
	switch (uMsg) 
	{
	case BFFM_INITIALIZED:
		if( lpData != NULL )
			::SendMessage( 
			hWnd, 
			BFFM_SETSELECTION, 
			WPARAM(TRUE), 
			LPARAM(lpData)
			);
		break;
	case BFFM_SELCHANGED:
		::SHGetPathFromIDList( LPITEMIDLIST(lParam), szPath );
		::SendMessage(
			hWnd, 
			BFFM_SETSTATUSTEXT, 
			WPARAM(NULL), 
			LPARAM(szPath)
			);
		break;
	}
	return 0;
}


IMPLEMENT_DYNAMIC(CESMOptBase, CPropertyPage)
CESMOptBase::CESMOptBase(UINT IDD) : CPropertyPage(IDD)
, m_strPath(_T(""))
{
	m_brush.CreateSolidBrush(RGB(44,44,44));
}

CESMOptBase::~CESMOptBase()
{
}

BEGIN_MESSAGE_MAP(CESMOptBase, CPropertyPage)
	ON_WM_CTLCOLOR()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

HBRUSH CESMOptBase::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//-- 2012-07-16 hongsu@esmlab.com
	//-- CIC Check
	// HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	/*switch(nCtlColor) 
	{
		case CTLCOLOR_STATIC:
			TCHAR lpszClassName[255];
			GetClassName(pWnd->m_hWnd, lpszClassName, 255);
			if(_tcscmp(lpszClassName, TRACKBAR_CLASS) == 0)
				return m_brush;
			break;
		case CTLCOLOR_BTN:
			pDC->SetBkMode(TRANSPARENT);
			break;
		default:
			break;			
	}*/

	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
		
	switch(nCtlColor)
	{
		case CTLCOLOR_STATIC:
		{
			
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(56, 56, 56));  // 글자 배경색 변경
			pDC->SetBkMode(TRANSPARENT);
			return (HBRUSH)m_brush;
		}
		break;
		case CTLCOLOR_EDIT:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0, 0 ,0));
			hbr = (HBRUSH)(m_brush.GetSafeHandle());  
		}
		break;
	}    

	return m_brush;
}

BOOL CESMOptBase::OnEraseBkgnd(CDC* pDC)
{
	CRect rt;
	GetClientRect(rt);
	pDC->FillSolidRect(rt,RGB(44,44,44));
	return TRUE;

	return CPropertyPage::OnEraseBkgnd(pDC);
}

void CESMOptBase::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}

void CESMOptBase::OnSelectPath()
{	
	UpdateData(TRUE);

	BROWSEINFO bi;
	memset(&bi, 0, sizeof(bi));
	CString strPath;

	bi.hwndOwner	= this->GetSafeHwnd();
	bi.lpszTitle	= _T("Select Folder");
	bi.ulFlags		= BIF_NEWDIALOGSTYLE;
	bi.lParam		= (LPARAM)(LPCTSTR)m_strPath;
	bi.lpfn			= BrowseCallbackProc;
	
	//-- 2013-10-20 hongsu@esmlab.com
	//-- Do App 
	// ::OleInitialize(NULL);

	LPITEMIDLIST pIDL = ::SHBrowseForFolder(&bi);

	if ( pIDL )
	{
		//char buffer[_MAX_PATH] = {'\0'};
		TCHAR buffer[_MAX_PATH] = {0};
		if (::SHGetPathFromIDList(pIDL, (LPTSTR)buffer) != 0)
		{
			strPath.Format(_T("%s"), buffer);
		}
		::OleUninitialize();
		m_strPath = strPath;
	}	
	UpdateData(FALSE);
}




