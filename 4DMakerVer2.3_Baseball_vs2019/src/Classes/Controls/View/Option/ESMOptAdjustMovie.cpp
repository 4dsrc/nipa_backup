////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptAdjust.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "4DMaker.h"
#include "ESMOptAdjustMovie.h"
#include "afxdialogex.h"
#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMOptAdjust 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOptAdjustMovie, CESMOptBase)

CESMOptAdjustMovie::CESMOptAdjustMovie()
	: CESMOptBase(CESMOptAdjustMovie::IDD)	
	, m_strFolder(_T(""))
	, m_strTarFolder(_T(""))
	, m_strStartFolder(_T(""))
	, m_strEndFolder(_T(""))
{
}

CESMOptAdjustMovie::~CESMOptAdjustMovie()
{
}

void CESMOptAdjustMovie::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDC_ADJ_START_FOLDER, m_Cb_StartFolder);
	DDX_Control(pDX, IDC_ADJ_END_FOLDER, m_Cb_EndFolder);
	DDX_Control(pDX, IDC_ADJ_TARGET_FOLDER, m_edit_tarforder);
	DDX_Control(pDX, IDC_ADJ_SOURCE_FOLDER, m_edit_srcforder);
	CESMOptBase::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_ADJ_SOURCE_FOLDER, m_strFolder);
	DDX_Text(pDX, IDC_ADJ_TARGET_FOLDER, m_strTarFolder);
	DDX_CBString(pDX, IDC_ADJ_START_FOLDER, m_strStartFolder);
	DDX_CBString(pDX, IDC_ADJ_END_FOLDER, m_strEndFolder);
	DDX_Control(pDX, IDC_CURRENT_COUNT, m_ctrCurrentCount);
	DDX_Control(pDX, IDC_TOTAL_COUNT, m_ctrlTotalCount);
	DDX_Control(pDX, IDC_STATIC_FRAME_ORIGINTOADJ, m_ctrlFrame);
}

void CESMOptAdjustMovie::InitProp(ESMAdjustMovie& Opt)
{
	m_strTarFolder = Opt.strTarPath;

	GetDlgItem(IDC_STATIC_FRAME_ORIGINTOADJ)->		SetWindowPos(NULL,0,34,514,260,NULL);

	GetDlgItem(IDC_STATIC_SRCFOLDER)->				SetWindowPos(NULL,24,64,100,24,NULL);
	GetDlgItem(IDC_ADJ_SOURCE_FOLDER)->				SetWindowPos(NULL,143,64,243,24,NULL);
	GetDlgItem(IDC_BTN_CHANGE)->					SetWindowPos(NULL,396,64,97,24,NULL);

	GetDlgItem(IDC_STATIC_TARGET_FOLDER)->			SetWindowPos(NULL,24,103,100,24,NULL);
	GetDlgItem(IDC_ADJ_TARGET_FOLDER)->				SetWindowPos(NULL,143,103,243,24,NULL);
	GetDlgItem(IDC_BTN_ADJ_SELECT)->				SetWindowPos(NULL,396,103,97,24,NULL);

	GetDlgItem(IDC_STATIC_START_FOLDER)->			SetWindowPos(NULL,24,142,100,24,NULL);
	GetDlgItem(IDC_ADJ_START_FOLDER)->				SetWindowPos(NULL,143,142,243,24,NULL);

	GetDlgItem(IDC_STATIC_END_FOLDER)->				SetWindowPos(NULL,24,181,100,24,NULL);
	GetDlgItem(IDC_ADJ_END_FOLDER)->				SetWindowPos(NULL,143,181,243,24,NULL);

	GetDlgItem(IDC_STATIC_COUNTING)->				SetWindowPos(NULL,24,220,100,24,NULL);
	GetDlgItem(IDC_CURRENT_COUNT)->					SetWindowPos(NULL,143,220,50,24,NULL);
	GetDlgItem(IDC_STATIC_SLASH)->					SetWindowPos(NULL,216,220,50,24,NULL);
	GetDlgItem(IDC_TOTAL_COUNT)->					SetWindowPos(NULL,266,220,50,24,NULL);
	GetDlgItem(IDC_BTN_CONVERT)->					SetWindowPos(NULL,396,220,97,24,NULL);

	UpdateData(FALSE);
}

BOOL CESMOptAdjustMovie::SaveProp(ESMAdjustMovie& Opt)
{
	if(!UpdateData(TRUE))
		return FALSE;

	Opt.arrPathList.RemoveAll();

	BOOL bCheck = FALSE;
	for(int i = 0; i < m_arrPathList.GetCount(); i++)
	{
		if(m_arrPathList.GetAt(i).CompareNoCase(m_strStartFolder) == 0)
		{
			bCheck = TRUE;
		}
		
		if(m_arrPathList.GetAt(i).CompareNoCase(m_strEndFolder) == 0)
		{
			Opt.arrPathList.Add(m_arrPathList.GetAt(i));
			break;
		}
		
		if(bCheck)
			Opt.arrPathList.Add(m_arrPathList.GetAt(i));

		
	}

	Opt.strSrcPath		= m_strFolder;
	Opt.strTarPath		= m_strTarFolder;
	Opt.strStartPath	= m_strStartFolder;
	Opt.strEndPath		= m_strEndFolder;


	return TRUE;
}

BEGIN_MESSAGE_MAP(CESMOptAdjustMovie, CESMOptBase)
	ON_BN_CLICKED(IDC_BTN_OPT_SELECTFOLDER, &CESMOptAdjustMovie::OnBnClickedBtnOptSelectfolder)
	ON_BN_CLICKED(IDC_BTN_CONVERT, &CESMOptAdjustMovie::OnBnClickedBtnConvert)
	ON_CBN_CLOSEUP(IDC_ADJ_END_FOLDER, &CESMOptAdjustMovie::OnCbnCloseupCbOptEndfolder)
	ON_CBN_CLOSEUP(IDC_ADJ_START_FOLDER, &CESMOptAdjustMovie::OnCbnCloseupCbOptStartfolder)	
	ON_BN_CLICKED(IDC_BTN_ADJ_SELECT, &CESMOptAdjustMovie::OnBnClickedBtnAdjSelect)
	ON_BN_CLICKED(IDC_BTN_OPT_SELECTFOLDER, &CESMOptAdjustMovie::OnBnClickedBtnOptSelectfolder)
	ON_EN_CHANGE(IDC_ADJ_SOURCE_FOLDER, &CESMOptAdjustMovie::OnChangeAdjSourceFolder)
	ON_EN_UPDATE(IDC_ADJ_SOURCE_FOLDER, &CESMOptAdjustMovie::OnUpdateAdjSourceFolder)
	ON_BN_CLICKED(IDC_BTN_CHANGE, &CESMOptAdjustMovie::OnBnClickedBtnChange)
END_MESSAGE_MAP()

void CESMOptAdjustMovie::OnBnClickedBtnConvert()
{
	m_nTotalCount = 0;
	m_nCurrentCount = 0;

	m_ctrlTotalCount.SetWindowText(_T("0"));
	m_ctrCurrentCount.SetWindowText(_T("0"));

	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();  
	pMsg->message = WM_ESM_MOVIE_MAKE_ADJUST;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
}

void CESMOptAdjustMovie::SetCount(int nCurrent, int nTotal)
{
	CString strData;
	strData.Format(_T("%d"), nTotal);
	m_ctrlTotalCount.SetWindowText(strData);
	strData.Format(_T("%d"), nCurrent);
	m_ctrCurrentCount.SetWindowText(strData);
}

void CESMOptAdjustMovie::OnCbnCloseupCbOptEndfolder()
{
	int startIndex = m_Cb_StartFolder.GetCurSel();

	int endIndex = m_Cb_EndFolder.GetCurSel();


	if( startIndex > endIndex)
	{
		m_Cb_EndFolder.SetCurSel(startIndex);
	}
}


void CESMOptAdjustMovie::OnCbnCloseupCbOptStartfolder()
{
	int startIndex = m_Cb_StartFolder.GetCurSel();

	int endIndex = m_Cb_EndFolder.GetCurSel();


	if( startIndex > endIndex)
	{
		m_Cb_EndFolder.SetCurSel(startIndex);
	}
}

void CESMOptAdjustMovie::OnBnClickedBtnAdjSelect()
{
	ITEMIDLIST*  pildBrowse;
	TCHAR   pszPathname[MAX_PATH];
	BROWSEINFO  bInfo;
	memset(&bInfo, 0, sizeof(bInfo));
	bInfo.hwndOwner   = GetSafeHwnd();
	bInfo.pidlRoot   = NULL;
	bInfo.pszDisplayName = pszPathname;
	bInfo.lpszTitle   = _T("Please select a directory");
	bInfo.ulFlags   = BIF_RETURNONLYFSDIRS; 
	bInfo.lpfn    = NULL;
	bInfo.lParam  = (LPARAM)(LPCTSTR)"C:\\";
	bInfo.lParam  = (LPARAM)NULL;
	pildBrowse    = SHBrowseForFolder(&bInfo);
	if(pildBrowse)
	{
		SHGetPathFromIDList(pildBrowse, pszPathname);
		m_edit_tarforder.SetWindowTextW(pszPathname);
	}
}


void CESMOptAdjustMovie::OnBnClickedBtnOptSelectfolder()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


BOOL CESMOptAdjustMovie::OnInitDialog()
{
	CESMOptBase::OnInitDialog();


	LoadFolder();
	

	//GetIPList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int comparisonFunctionString(const void *a, const void *b) 
{
	CString *pStr1 = (CString*)a ;
	CString *pStr2 = (CString*)b ;
	int nReturn = pStr1->Compare(*pStr2);

	return nReturn ;
}

void CESMOptAdjustMovie::LoadFolder()
{
	CFileFind file;
	CString strFolder = ESMGetPath(ESM_PATH_LOCALMOVIE);

	m_strFolder = strFolder;
	m_edit_srcforder.SetWindowText(strFolder);

	BOOL b = file.FindFile(strFolder + _T("\\*.*"));
	CString strFolderItem, strFileExt, strTempString;
	while(b)
	{
		b = file.FindNextFile();
		if(file.IsDirectory() && !file.IsDots())
		{
			strFolderItem = file.GetFileName();
			m_arrPathList.Add(strFolderItem);
		}
	}

	qsort(m_arrPathList.GetData(), m_arrPathList.GetCount(), sizeof(CString*), comparisonFunctionString);

	for( int i =0 ;i < m_arrPathList.GetCount(); i++)
	{
		m_Cb_StartFolder.AddString(m_arrPathList.GetAt(i));
		m_Cb_EndFolder.AddString(m_arrPathList.GetAt(i));
	}

	m_Cb_StartFolder.SetCurSel(0);
	m_Cb_EndFolder.SetCurSel(0);
}

void CESMOptAdjustMovie::OnChangeAdjSourceFolder()
{
}


void CESMOptAdjustMovie::OnUpdateAdjSourceFolder()
{
	
	
}


void CESMOptAdjustMovie::OnBnClickedBtnChange()
{
	m_arrPathList.RemoveAll();
	m_Cb_StartFolder.ResetContent();
	m_Cb_EndFolder.ResetContent();

	CFileFind file;
	CString strFolder;
	m_edit_srcforder.GetWindowText(strFolder);

	m_strFolder = strFolder;
	m_edit_srcforder.SetWindowText(strFolder);

	BOOL b = file.FindFile(strFolder + _T("\\*.*"));
	CString strFolderItem, strFileExt, strTempString;
	while(b)
	{
		b = file.FindNextFile();
		if(file.IsDirectory() && !file.IsDots())
		{
			strFolderItem = file.GetFileName();
			m_arrPathList.Add(strFolderItem);
		}
	}

	qsort(m_arrPathList.GetData(), m_arrPathList.GetCount(), sizeof(CString*), comparisonFunctionString);

	for( int i =0 ;i < m_arrPathList.GetCount(); i++)
	{
		m_Cb_StartFolder.AddString(m_arrPathList.GetAt(i));
		m_Cb_EndFolder.AddString(m_arrPathList.GetAt(i));
	}

	m_Cb_StartFolder.SetCurSel(0);
	m_Cb_EndFolder.SetCurSel(0);
}
