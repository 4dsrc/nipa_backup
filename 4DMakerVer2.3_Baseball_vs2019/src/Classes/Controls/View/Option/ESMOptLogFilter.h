////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptLogFilter.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once
#include "ESMOptBase.h"
#include "resource.h"
#include "ESMEditorEX.h"


class CESMOptLogFilter :
	public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOptLogFilter)

public:
	CESMOptLogFilter();
	virtual ~CESMOptLogFilter();
	void InitProp(ESMLogFilter& ser);
	BOOL SaveProp(ESMLogFilter& ser);
	// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_LOG_FILTER };

	afx_msg void OnBnClickedCheck();
	afx_msg void OnBnClickedCheckEx();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()
public:

	BOOL m_bInChk[4];
	BOOL m_bInPutMsg[4];
	BOOL m_bExChk[4];
	CString m_strInclude[4];
	CString m_strInPutMsg[4];
	CString m_strExclude[4];

	CESMEditorEX m_ctrlString1;
	CESMEditorEX m_ctrlString2;
	CESMEditorEX m_ctrlString3;
	CESMEditorEX m_ctrlString4;
	CESMEditorEX m_ctrlMessage1;
	CESMEditorEX m_ctrlMessage2;
	CESMEditorEX m_ctrlMessage3;
	CESMEditorEX m_ctrlMessage4;
	CESMEditorEX m_ctrlSerialLog1;
	CESMEditorEX m_ctrlSerialLog2;
	CESMEditorEX m_ctrlSerialLog3;
	CESMEditorEX m_ctrlSerialLog4;
	CStatic m_ctrlFrame1;
	CStatic m_ctrlFrame2;
};
