////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptTemplate.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-07
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMOptBase.h"
#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "ESMEditorEX.h"
#include "ESMTabCtrlEx.h"
#include "TabCtrlEx.h"
#include "ESMButtonEx.h"

//2016/07/08 Lee SangA Number of hotkey and page
/*
#define INFO_TEMPLATE_PAGE_NUM 5
#define INFO_TEMPLATE_HOTKEY_NUM 9
*/

//2016/07/08 Lee SangA Save information of each page(F1...F5)
struct INFO_TEMPLATE_CONTENTS
{
	CString path[9];
};


class CESMOptTemplate : public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOptTemplate)

public:
	CESMOptTemplate();
	virtual ~CESMOptTemplate();
	void InitProp(ESMTemplate& Opt);
	BOOL SaveProp(ESMTemplate& Opt);
	enum { IDD = IDD_OPTION_TEMPLATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()

public:	
	CString m_strTemplate1;
	CString m_strTemplate2;
	CString m_strTemplate3;
	CString m_strTemplate4;
	//CMiLRe 20151013 Template Load INI File
	CString m_strTemplate5;
	CString m_strTemplate6;
	CString m_strTemplate7;
	CString m_strTemplate8;
	CString m_strTemplate9;

	//2016/07/08 Lee SangA Array that save contents of each page(F1...F5). After replace with m_strTemplate1...9.
	INFO_TEMPLATE_CONTENTS m_contents[5];
	int curPageNum;
	
	afx_msg void OnBnClickedCtrl1();
	afx_msg void OnBnClickedCtrl2();
	afx_msg void OnBnClickedCtrl3();
	afx_msg void OnBnClickedCtrl4();
	//CMiLRe 20151013 Template Load INI File
	afx_msg void OnBnClickedCtrl5();
	afx_msg void OnBnClickedCtrl6();
	afx_msg void OnBnClickedCtrl7();
	afx_msg void OnBnClickedCtrl8();
	afx_msg void OnBnClickedCtrl9();
	afx_msg void OnCbnSelchangeSelectPage();
	afx_msg void OnBnClickedButton2();
	CTabCtrlEx m_Tab2;
	//CESMTabCtrlEx m_Tab2;
	CImageList *m_pImageList;
	afx_msg void OnTcnSelchangeTab4(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CStatic m_txtNumkey1;
	CStatic m_txtNumkey2;
	CStatic m_txtNumkey3;
	CStatic m_txtNumkey4;
	CStatic m_txtNumkey5;
	CStatic m_txtNumkey6;
	CStatic m_txtNumkey7;
	CStatic m_txtNumkey8;
	CStatic m_txtNumkey9;
	afx_msg void OnEnChangePathCtrl1();
	afx_msg void OnEnChangePathCtrl2();
//	afx_msg void OnEnKillfocusPathCtrl1();
//	afx_msg void OnEnKillfocusPathCtrl2();
//	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnEnChangePathCtrl3();
	afx_msg void OnEnChangePathCtrl4();
	afx_msg void OnEnChangePathCtrl5();
	afx_msg void OnEnChangePathCtrl6();
	afx_msg void OnEnChangePathCtrl7();
	afx_msg void OnEnChangePathCtrl8();
	afx_msg void OnEnChangePathCtrl9();
	BOOL m_bRecovery;
	CESMEditorEX m_ctrlPath1;
	CESMEditorEX m_ctrlPath2;
	CESMEditorEX m_ctrlPath3;
	CESMEditorEX m_ctrlPath4;
	CESMEditorEX m_ctrlPath5;
	CESMEditorEX m_ctrlPath6;
	CESMEditorEX m_ctrlPath7;
	CESMEditorEX m_ctrlPath8;
	CESMEditorEX m_ctrlPath9;
	CESMButtonEx m_btnCtrl1;
	CESMButtonEx m_btnCtrl2;
	CESMButtonEx m_btnCtrl3;
	CESMButtonEx m_btnCtrl4;
	CESMButtonEx m_btnCtrl5;
	CESMButtonEx m_btnCtrl6;
	CESMButtonEx m_btnCtrl7;
	CESMButtonEx m_btnCtrl8;
	CESMButtonEx m_btnCtrl9;
};
