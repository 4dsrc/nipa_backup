﻿////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptLog.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMOptLog.h"
#include "ESMOptPath.h"
#include "ESMBackupDlg.h"
#include "ESMFunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMOptLog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOptLog, CESMOptBase)

CESMOptLog::CESMOptLog()
	: CESMOptBase(CESMOptLog::IDD)	
	, m_strLogName(_T(""))
	, m_nVerbosity(0)
	, m_nLimitday(0)
	, m_bTaceTime(FALSE)
	, m_dateLog(COleDateTime::GetCurrentTime())
{	
}

CESMOptLog::~CESMOptLog()
{
}

void CESMOptLog::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);
	DDX_Text(pDX,  IDC_LOG_NAME			, m_strLogName);
	DDX_Text(pDX,  IDC_LOG_VERBOSITY	, m_nVerbosity);
	DDX_Text(pDX,  IDC_LOG_LIMITDAY		, m_nLimitday);
	DDX_Check(pDX, IDC_LOG_TRACE_TIME	, m_bTaceTime);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER, m_dateLog);
	DDX_Control(pDX, IDC_LOG_NAME, m_ctrlLogName);
	DDX_Control(pDX, IDC_LOG_VERBOSITY, m_ctrlLogVerbosity);
	DDX_Control(pDX, IDC_LOG_LIMITDAY, m_ctrlLogLimitDay);
	DDX_Control(pDX, IDC_STATIC_LOGBACKUP, m_Frame1);
}

void CESMOptLog::InitProp(ESMLogOpt& log)
{
	m_strLogName	= log.strLogName;
	m_nVerbosity	= log.nVerbosity;
	m_nLimitday		= log.nLimitday;
	m_bTaceTime		= log.bTraceTime;	

	GetDlgItem(IDC_STATIC_LOGNAME)->		SetWindowPos(NULL,0,8,80,24,NULL);
	GetDlgItem(IDC_STATIC_LOGVER)->			SetWindowPos(NULL,0,42,80,24,NULL);
	GetDlgItem(IDC_STATIC_LOG)->			SetWindowPos(NULL,0,76,80,24,NULL);

	GetDlgItem(IDC_LOG_NAME)->				SetWindowPos(NULL,106,8,146,24,NULL);
	GetDlgItem(IDC_LOG_VERBOSITY)->			SetWindowPos(NULL,106,42,146,24,NULL);
	GetDlgItem(IDC_LOG_LIMITDAY)->			SetWindowPos(NULL,106,76,146,24,NULL);

	GetDlgItem(IDC_STATIC_LOGBACKUP)->		SetWindowPos(NULL,0,144,250,110,NULL);

	GetDlgItem(IDC_LOG_TRACE_TIME)->		SetWindowPos(NULL,1,115,250,14,NULL);
	GetDlgItem(IDC_DATETIMEPICKER)->		SetWindowPos(NULL,20,173,212,24,NULL);
	GetDlgItem(IDC_BTN_BACKUP)->			SetWindowPos(NULL,135,209,97,24,NULL);


	UpdateData(FALSE);
}

BOOL CESMOptLog::SaveProp(ESMLogOpt& log)
{
	if(!UpdateData(TRUE))
		return FALSE;
	log.strLogName	= m_strLogName;
	log.nVerbosity	= m_nVerbosity;
	log.nLimitday	= m_nLimitday;
	log.bTraceTime  = m_bTaceTime;
	return TRUE;
}

BEGIN_MESSAGE_MAP(CESMOptLog, CESMOptBase)
	ON_BN_CLICKED(IDC_BTN_BACKUP, &CESMOptLog::OnBnClickedBtnBackup)
END_MESSAGE_MAP()


void CESMOptLog::OnBnClickedBtnBackup()
{
	UpdateData(TRUE);

	CString strPath = OnSelectPath();
	CString strDate;
	strDate.Format(_T("%04d-%02d-%02d"), m_dateLog.GetYear(), m_dateLog.GetMonth(), m_dateLog.GetDay());
	ESMLog(5, _T("Log Backup Path : %s, Target : %04d-%02d-%02d"), strPath, m_dateLog.GetYear(), m_dateLog.GetMonth(), m_dateLog.GetDay());

	if(strPath.IsEmpty())
		return;

	CESMBackupDlg *pDlg = new CESMBackupDlg();

	pDlg->Create(this);
	pDlg->ShowWindow(SW_SHOW);
	
	pDlg->BackupLogFile(strDate, strPath);
}

CString CESMOptLog::OnSelectPath()
{
	CString strPath;
#if 0
	LPITEMIDLIST pidlBrowse;
	
	BROWSEINFO BrInfo;

	BrInfo.hwndOwner = GetSafeHwnd();
	BrInfo.pidlRoot = NULL;

	memset( &BrInfo, 0, sizeof(BrInfo) );
	BrInfo.pszDisplayName = (LPWSTR)(LPCTSTR)strPath;
	BrInfo.lpszTitle = _T("Select Folder");
	//BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;
	BrInfo.ulFlags = BIF_NEWDIALOGSTYLE;

	// 다이얼로그 띄우기
	pidlBrowse = ::SHBrowseForFolder(&BrInfo);

	if( pidlBrowse != NULL )
	{
		BOOL bSuccess = ::SHGetPathFromIDList(pidlBrowse, (LPWSTR)(LPCTSTR)strPath);

		if(!bSuccess)
		{
			return _T("");
		}

	}
#else
	CFolderPickerDialog dlg(_T(""), 0, NULL, 0);
	if(dlg.DoModal() == IDOK)
	{
		strPath = dlg.GetPathName();
	}
#endif

	return strPath;
}

