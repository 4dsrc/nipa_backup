////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptAdjust.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-10-05
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMEditorEX.h"
#include "ESMOptBase.h"
#include "resource.h"

class CESMOptAdjust : public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOptAdjust)

public:
	CESMOptAdjust();
	virtual ~CESMOptAdjust();
	void InitProp(ESMAdjust& Opt);
	BOOL SaveProp(ESMAdjust& Opt);
	enum { IDD = IDD_OPTION_ADJUST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()

public:	
	int m_nThreshold;
	int m_nPointGapMin;
	int m_nMinWidth;
	int m_nMinHeight;
	int m_nMaxWidth;
	int m_nMaxHeight;
	int	m_nTargetLenth;

	int m_nFlowmoTime;
	int m_nAutoBrightness;
	int m_nCamZoom;

	BOOL m_bClockwise;
	BOOL m_bClockreverse;

	afx_msg void OnBnClickedAdjustOrder();
	CStatic m_ctrlFrameSPS;
	CStatic m_ctrlFrameDetectSize;
	CStatic m_ctrlCameraOrder;
	CStatic m_ctrlThredHold;
	CStatic m_ctrlMinPointGap;
	CESMEditorEX m_ctrlAdjustTh;
	CESMEditorEX m_ctrlAdjustMinPointGap;
	CESMEditorEX m_ctrlAdjustMinWidth;
	CESMEditorEX m_ctrlAdjustMaxWidth;
	CESMEditorEX m_ctrlAdjustMinHeight;
	CESMEditorEX m_ctrlAdjustMaxHeight;
	CESMEditorEX m_ctrlAdjustFlowT;
	CESMEditorEX m_ctrlAdjustCamZoom;
	CESMEditorEX m_ctrlAutoBR;
	CESMEditorEX m_ctrlAdjustTargetLength;
};
