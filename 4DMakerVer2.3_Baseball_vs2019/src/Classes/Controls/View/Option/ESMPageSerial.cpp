////////////////////////////////////////////////////////////////////////////////
//
//	TGPageSerial.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////



#include "stdafx.h"
#include "TG.h"
#include "TGPageSerial.h"
#include "Serial.h"


// CTGPageSerial 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTGPageSerial, CTGPropertyPage)

CTGPageSerial::CTGPageSerial()
	: CTGPropertyPage(CTGPageSerial::IDD)
	, m_strBaurate(szComBaurate[6])
	, m_strDatabits(szComDataBit[3])
	, m_strStopbits(szComStopBit[0])
	, m_strFlowcontrol(szComFlowControl[0])
	, m_strParitybits(szComParityBit[0])	
	, m_bPortAutoConnect(FALSE)
	, m_nPort(0)
{
}

CTGPageSerial::~CTGPageSerial()
{
}

void CTGPageSerial::DoDataExchange(CDataExchange* pDX)
{
	CTGPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_PORT, m_bPortAutoConnect);
	DDX_Control(pDX, IDC_STATUS_PORT, m_ctrStatusPort);
	DDX_CBString(pDX, IDC_SER_BAURATE, m_strBaurate);
	DDX_CBString(pDX, IDC_SER_DATABITS, m_strDatabits);
	DDX_CBString(pDX, IDC_SER_STOPBITS, m_strStopbits);
	DDX_CBString(pDX, IDC_SER_FLOWCONTROL, m_strFlowcontrol);
	DDX_CBString(pDX, IDC_SER_PARITYBITS, m_strParitybits);
	DDX_Control(pDX, IDC_SER_BAURATE, m_ctrlBaurate);
	DDX_Control(pDX, IDC_SER_DATABITS, m_ctrlDatabits);
	DDX_Control(pDX, IDC_SER_PARITYBITS, m_ctrlParitybits);
	DDX_Control(pDX, IDC_SER_STOPBITS, m_ctrlStopbits);
	DDX_Control(pDX, IDC_SER_FLOWCONTROL, m_ctrlFlowcontrol);
	DDX_Text(pDX, IDC_SER_PORT, m_nPort);
}

//2010-01-18 Lee Jugn Taek
//콤보박스에 해당 데이터를 삽입하는 함수
//TGPageSerial.h 에 ARRY 선언
void CTGPageSerial::InitComPort()
{
	for(int i=0; i<ARRAY_SIZE(szComBaurate); i++)
	{
		m_ctrlBaurate.AddString(szComBaurate[i]);
	}

	for(int i=0; i<ARRAY_SIZE(szComDataBit); i++)
	{
		m_ctrlDatabits.AddString(szComDataBit[i]);
	}

	for(int i=0; i<ARRAY_SIZE(szComParityBit); i++)
	{
		m_ctrlParitybits.AddString(szComParityBit[i]);
	}

	for(int i=0; i<ARRAY_SIZE(szComStopBit); i++)
	{
		m_ctrlStopbits.AddString(szComStopBit[i]);
	}

	for(int i=0; i<ARRAY_SIZE(szComFlowControl); i++)
	{
		m_ctrlFlowcontrol.AddString(szComFlowControl[i]);
	}
}

//2010-01-18 Lee Jugn Taek
//콤보박스의 포커스를 지정하기 위한 함수
//Config 값과 콤보박스의 값들을 비교해 일치하는 데이터에 포커스
void CTGPageSerial::SetComPort(TGSerial& ser)
{
	m_strBaurate.Format(_T("%d"), ser.nBautrate);
	m_strDatabits.Format(_T("%d"), ser.nDateBits);
	m_strStopbits.Format(_T("%d"), ser.nStopBits);
	m_strParitybits = ser.strParityScheme;
	m_strFlowcontrol = ser.strHandShaking;

	for(int i=0; i<ARRAY_SIZE(szComBaurate); i++)
	{
		if(!m_strBaurate.CompareNoCase(szComBaurate[i]))
		{
			m_ctrlBaurate.SetCurSel(i);
			break;
		}	
		else
		{
			m_ctrlBaurate.SetCurSel(0);
		}
	}

	for(int i=0; i<ARRAY_SIZE(szComDataBit); i++)
	{
		if(!m_strDatabits.CompareNoCase(szComDataBit[i]))
		{
			m_ctrlDatabits.SetCurSel(i);
			break;
		}	
		else
		{
			m_ctrlDatabits.SetCurSel(0);
		}
	}

	for(int i=0; i<ARRAY_SIZE(szComParityBit); i++)
	{
		if(!m_strParitybits.CompareNoCase(szComParityBit[i]))
		{
			m_ctrlParitybits.SetCurSel(i);
			break;
		}	
		else
		{
			m_ctrlParitybits.SetCurSel(0);
		}
	}

	for(int i=0; i<ARRAY_SIZE(szComStopBit); i++)
	{
		if(!m_strStopbits.CompareNoCase(szComStopBit[i]))
		{
			m_ctrlStopbits.SetCurSel(i);
			break;
		}	
		else
		{
			m_ctrlStopbits.SetCurSel(0);
		}
	}

	for(int i=0; i<ARRAY_SIZE(szComFlowControl); i++)
	{
		if(!m_strFlowcontrol.CompareNoCase(szComFlowControl[i]))
		{
			m_ctrlFlowcontrol.SetCurSel(i);
			break;
		}	
		else
		{
			m_ctrlFlowcontrol.SetCurSel(0);
		}
	}
}

void CTGPageSerial::InitProp(TGSerial& ser)
{
	InitComPort();			//Com Port 콤보박스 초기화	
	SetComPort(ser);		//Com Port 콤보박스 포커스 지정함수

	//이하 기존 소스
	CSerial serial;
	HBITMAP hbmp;

	m_nPort				= ser.nPort;
	m_bPortAutoConnect	= ser.bPortAutoConnect;

	//-- Set Port
	if(m_bPortAutoConnect)
	{
		if(!serial.CheckPort(GetPort()))
		{
			hbmp = (HBITMAP)::LoadImage(AfxGetInstanceHandle(),
												MAKEINTRESOURCE(IDB_STATUS_CONNECT),
												IMAGE_BITMAP,
												0,0,LR_LOADMAP3DCOLORS);
			m_ctrStatusPort.SetBitmap(hbmp);
			GetDlgItem(IDC_CONNECT_PORT)->SetWindowText(_T("Connect"));
		}
		else
		{
			hbmp = (HBITMAP)::LoadImage(AfxGetInstanceHandle(),
												MAKEINTRESOURCE(IDB_STATUS_DISCONNECT),
												IMAGE_BITMAP,
												0,0,LR_LOADMAP3DCOLORS);
			m_ctrStatusPort.SetBitmap(hbmp);
			GetDlgItem(IDC_CONNECT_PORT)->SetWindowText(_T("Disconnect"));
		}
	}
	else
	{
		hbmp = (HBITMAP)::LoadImage(AfxGetInstanceHandle(),
											MAKEINTRESOURCE(IDB_STATUS_DISABLE),
											IMAGE_BITMAP,
											0,0,LR_LOADMAP3DCOLORS);
		m_ctrStatusPort.SetBitmap(hbmp);
		GetDlgItem(IDC_CONNECT_PORT)->SetWindowText(_T("Disable"));
	}

	UpdateData(FALSE);
}

BOOL CTGPageSerial::SaveProp(TGSerial& ser)
{
	if(!UpdateData(TRUE))
		return FALSE;

  	ser.nBautrate		= _ttoi(m_strBaurate);
  	ser.nDateBits		= _ttoi(m_strDatabits);
  	ser.nStopBits		= _ttoi(m_strStopbits);
	ser.strHandShaking	= m_strFlowcontrol;
	ser.strParityScheme	= m_strParitybits;
	ser.nPort			= m_nPort;
	ser.bPortAutoConnect= m_bPortAutoConnect;	

	return TRUE;
}

BEGIN_MESSAGE_MAP(CTGPageSerial, CTGPropertyPage)
	ON_BN_CLICKED(IDC_CHECK_PORT, OnBnClickedCheckPort)	
	ON_EN_CHANGE(IDC_SER_PORT, &CTGPageSerial::OnEnChangeSerPort)
END_MESSAGE_MAP()

// CTGPageSerial 메시지 처리기입니다.
void CTGPageSerial::OnBnClickedCheckPort()
{
	UpdateData(TRUE);
	
	// Change Port Setting
	CSerial serial;
	if(m_bPortAutoConnect)
	{
		if(!serial.CheckPort(GetPort()))
		{
			HBITMAP hbmp = (HBITMAP)::LoadImage(AfxGetInstanceHandle(),
												MAKEINTRESOURCE(IDB_STATUS_CONNECT),
												IMAGE_BITMAP,
												0,0,LR_LOADMAP3DCOLORS);
			m_ctrStatusPort.SetBitmap(hbmp);
			GetDlgItem(IDC_CONNECT_PORT)->SetWindowText(_T("Connect"));
		}
		else
		{
			HBITMAP hbmp = (HBITMAP)::LoadImage(AfxGetInstanceHandle(),
												MAKEINTRESOURCE(IDB_STATUS_DISCONNECT),
												IMAGE_BITMAP,
												0,0,LR_LOADMAP3DCOLORS);
			m_ctrStatusPort.SetBitmap(hbmp);
			GetDlgItem(IDC_CONNECT_PORT)->SetWindowText(_T("Disconnect"));
		}
	}
	else
	{
		HBITMAP hbmp = (HBITMAP)::LoadImage(AfxGetInstanceHandle(),
											MAKEINTRESOURCE(IDB_STATUS_DISABLE),
											IMAGE_BITMAP,
											0,0,LR_LOADMAP3DCOLORS);
		m_ctrStatusPort.SetBitmap(hbmp);
		GetDlgItem(IDC_CONNECT_PORT)->SetWindowText(_T("Disable"));
	}	
}

void CTGPageSerial::OnEnChangeSerPort()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CTGPropertyPage::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
