#pragma once
#include "ESMOptBase.h"
#include "ESMEditorEX.h"
#include "ESMButtonEx.h"

// CESMOptCeremony 대화 상자입니다.

class CESMOptCeremony : public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOptCeremony)

public:
	CESMOptCeremony(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMOptCeremony();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_CEREMONY };
	BOOL SaveProp(ESMCeremony& SaveData);
	void InitProp(ESMCeremony& SaveData);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	BOOL m_bCeremonyUse;
	BOOL m_bCeremonyPutImage;
	BOOL m_bCeremonyRatio;

	CString m_strWidth;
	CString m_strHeight;
	CString m_strDbAddrIP;
	CString m_strDbId;
	CString m_strDbPasswd;
	CString m_strDbName;
	CString m_strImagePath;
	CString m_strMoviePath;

	afx_msg void OnBnClickedBtnCeremonyImagepath();
	afx_msg void OnBnClickedBtnCeremonyMoviepath();

	//180423 hjcho
	BOOL	m_bStillImage;
	CESMEditorEX m_ctrlDBIP;
	CESMEditorEX m_ctrlDBID;
	CESMEditorEX m_ctrlDBPW;
	CESMEditorEX m_ctrlDBName;
	CESMEditorEX m_ctrlWidth;
	CESMEditorEX m_ctrlHeight;
	CESMEditorEX m_ctrlImagePath;
	CESMEditorEX m_ctrlMoviePath;
	CStatic m_ctrlFrame1;
	CStatic m_ctrlFrame2;
	CESMButtonEx m_btnImagePath;
	CESMButtonEx m_btnMoviePath;

	BOOL m_bUseCanonSelphy;
};