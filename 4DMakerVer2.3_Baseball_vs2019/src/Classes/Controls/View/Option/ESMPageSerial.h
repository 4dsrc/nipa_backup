////////////////////////////////////////////////////////////////////////////////
//
//	TGPageSerial.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////



#pragma once
#include "TGPropertyPage.h"
#include "resource.h"
#include "afxwin.h"
// CTGPageSerial 대화 상자입니다.

//2010-01-18 Lee Jugn Taek
//COM Port OPtion UI 변경
#define ARRAY_SIZE(A)	(sizeof(A) / sizeof(A[0])) //Arry 의 아이템 갯수를 반환

//Serial.h 참조
//COM Port의 Baurate 값을 가진 변수 타입
static TCHAR szComBaurate[][10] = 
{
	_T("110"),
	_T("300"),
	_T("600"),
	_T("1200"),
	_T("2400"),
	_T("4800"),
	_T("9600"),
	_T("14400"),
	_T("19200"),
	_T("38400"),
	_T("56000"),
	_T("57600"),
	_T("115200"),
	_T("128000"),
	_T("256000")
};

//COM Port의 DataBit 값을 가진 변수 타입
static TCHAR szComDataBit[][10] = 
{
	_T("5"),
	_T("6"),
	_T("7"),
	_T("8"),
};

//COM Port의 ParityBit 값을 가진 변수 타입
static TCHAR szComParityBit[][10] = 
{
	_T("None"),
	_T("Odd"),
	_T("Even"),
	_T("Mark"),
	_T("Space")
};

//COM Port의 StopBit 값을 가진 변수 타입
static TCHAR szComStopBit[][10] = 
{
	_T("1"),
	_T("2")
};

//COM Port의 Flow Control 값을 가진 변수 타입
static TCHAR szComFlowControl[][10] = 
{
	_T("None"),
	_T("Xon/Xoff"),
	_T("Hardware")
};


class CTGPageSerial : public CTGPropertyPage
{
	DECLARE_DYNAMIC(CTGPageSerial)

public:
	CTGPageSerial();
	virtual ~CTGPageSerial();
	void InitProp(TGSerial& ser);
	BOOL SaveProp(TGSerial& ser);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_PAGE_SERIAL };
	afx_msg void OnBnClickedCheckPort();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()
private:

	//CoboBox Value
	UINT m_nPort;
	CString m_strBaurate;
	CString m_strDatabits;
	CString m_strStopbits;
	CString m_strFlowcontrol;		//기존 Handshaking
	CString m_strParitybits;		//기존 Parityscheme	
	BOOL	m_bPortAutoConnect;
	CStatic m_ctrStatusPort;

	//ComboBox Control
	CComboBox m_ctrlBaurate;
	CComboBox m_ctrlDatabits;
	CComboBox m_ctrlParitybits;
	CComboBox m_ctrlStopbits;
	CComboBox m_ctrlFlowcontrol;

	//2010-01-18 Lee Jugn Taek
	//COM Port OPtion UI 변경 관련 함수	
	void InitComPort();					//Com Port ComboBox 값 Add 함수
	void SetComPort(TGSerial& ser);		//Com Port Focus 를 정하기 위한 함수

public:
	
	afx_msg void OnEnChangeSerPort();
	CString GetPort() { CString str; str.Format(_T("COM%d"),m_nPort); return str; }
};
