#pragma once


// CESMOptManagement 대화 상자입니다.
#include "ESMFunc.h"
#include "algorithm"
#include "ESMIni.h"
#include "afxwin.h"
#include <Shlobj.h>
#include "afxcmn.h"
#include "ESMOptBase.h"
#include "ESMDefine.h"
#include "ProgressListCtrl.h"
#include "EMSFtp.h"
#include "ESMBackup.h"
#include "ESMRCManager.h"
#include "ESMEditorEX.h"


class CESMOptManagement : public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOptManagement)

public:
	CESMOptManagement();   // 표준 생성자입니다.
	virtual ~CESMOptManagement();

	void InitProp(ESMFileCopy& Opt);
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_MANAGEMANT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	vector<CString> m_arrPathList;
	vector<CString> m_IPList;
	CComboBox m_Cb_StartFolder;
	CComboBox m_Cb_EndFolder;
	int m_nStartCurSel, m_nEndCurSel;
	int start;
	int end;
	int m_iPrevcount;
	bool threadstopflag;

	int m_nTotalFileCount;
	int m_nCurrentFileCount;

public:
	struct BackupData
	{
		BackupData()
		{
			nNum = 0;
			nReTryCount = 1;
			nGroupIndex = 0;
			nMarginX = 0;
			nMarginY = 0;
			bUseFolerList = false;
			bUseDeleteAfterTransfer = false;
		}
		CESMOptManagement* pView;
		CESMRCManager* pMRCMgr;
		int nNum;
		int nReTryCount;
		int nGroupIndex;
		int nMarginX;
		int nMarginY;
		bool bUseFolerList;
		bool bUseDeleteAfterTransfer;
	};


	
	CProgressCtrl m_pro_status;
	CESMBackup* m_pBackupMgr;

public:
	BOOL SaveProp(ESMManagement& SaveData);
	void InitProp(ESMManagement& SaveData);
	void SetBackupMgr(CESMBackup* pBackupMgr);

private:
	void GetIPList();
	void FileCopy();
	void FileSelectDelete();
	void FileAllDelete();
	CString GetLocalIP();
	void GetFileCount(BOOL bAll = FALSE);

	void InitBackupListCtrl();

	static unsigned WINAPI FileCopyThread(LPVOID param);
	static unsigned WINAPI FileDelThread(LPVOID param);
	static unsigned WINAPI FileCountThread(LPVOID param);
	CRITICAL_SECTION m_CS;

	CEMSFtp m_Ftp;
	static unsigned WINAPI FTPFileUpload(LPVOID param);	
	static unsigned WINAPI FTPBackup(LPVOID param);
	
public:	
	afx_msg void OnCbnCloseupCbOptEndfolder();
	afx_msg void OnCbnCloseupCbOptStartfolder();
	afx_msg void OnBnClickedBtnOptCopyfolder();
	afx_msg void OnBnClickedBtnOptDelfolder();
	afx_msg void OnBnClickedBtnOptAlldelfolder();
	afx_msg void OnBnClickedBtnOptSelectfolder();
	
	LRESULT OnESMOPTMsg(WPARAM w, LPARAM l);

	CStatic m_Target_text;
	CStatic m_staticProgress;
	CProgressListCtrl m_listCtrlIP;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	//void XSleep(DWORD dwMilliseconds);
	afx_msg void OnBnClickedButtonOptBackup();
	
	CString m_strFtpHostIp;
	CString m_strFtpHostId;
	CString m_strFtpHostPassword;
		
	afx_msg void OnBnClickedBackupPause();
	static unsigned WINAPI BackupStartThread(LPVOID param);
	static unsigned WINAPI BackupEndThread(LPVOID param);
	static unsigned WINAPI BackupCurrentFileCountThread(LPVOID param);
	map<CString, HANDLE> m_pArrHandle;

	void RedrawBackupList();
	CString m_strDstFolder;

	CObArray m_arRCServerList;
	void AddRCMgr(CString strIP, int nPort);
	int GetRCMgrCount()			{ return m_arRCServerList.GetCount(); }	
	CESMRCManager* GetRCMgr(int nIndex);
	CString m_strSrcFolder;

	void InitProp(ESMBackupString& backupString);
	BOOL SaveProp(ESMBackupString& backupString);
	afx_msg void OnBnClickedButtonFolderSelect();
private:
	CButton m_ctrlUseFolderSelect;
	CButton m_ctrlDeleteAfterTransfer;
protected:
	afx_msg LRESULT OnESMOptDeleteFolderList(WPARAM wParam, LPARAM lParam);
public:
	CESMEditorEX m_edit_srcforder;
	CESMEditorEX m_ctrlTargetFolder;
	CESMEditorEX m_ctrlFTPHost;
	CESMEditorEX m_ctrlFTPID;
	CESMEditorEX m_ctrlFTPPW;
	CStatic m_ctrlFrame1;
	CStatic m_ctrlFrame2;
};
	