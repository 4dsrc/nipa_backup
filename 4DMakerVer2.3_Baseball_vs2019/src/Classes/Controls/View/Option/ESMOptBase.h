////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptBase.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-07
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxdlgs.h"
#include "afxcmn.h"
#include "ESMIndex.h"

#define COLOR_BK_PROGRESS	RGB(225,225,225)

class CESMOptBase : public CPropertyPage
{
	DECLARE_DYNAMIC(CESMOptBase)
public:
	CESMOptBase(UINT IDD);
	virtual ~CESMOptBase();

	CBrush m_brush;
	CString m_strPath;
	void OnSelectPath();

	DECLARE_MESSAGE_MAP()
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

