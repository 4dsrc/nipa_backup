////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptPath.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "ESMEditorEX.h"
#include "ESMOptBase.h"
#include "resource.h"
#include "ESMButtonEx.h"


class CESMOptPath : public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOptPath)

public:
	CESMOptPath();
	virtual ~CESMOptPath();
	void InitProp(ESMBasicPath& path);
	BOOL SaveProp(ESMBasicPath& path);

	enum { IDD = IDD_OPTION_PATH };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP()
public:
	CString m_strHomePath	;
	CString m_strFileSvrPath;
	CString m_strLogPath	;
	CString m_strConfigPath	;	
	CString m_strWorkPath	;
	CString m_strRecord		;
	CString m_strOutput		;
	CString m_strServerRamPath		;	
	CString m_strClientRamPath		;
	CString m_strSaveImgPath;
	
	//jhhan
	BOOL m_bDefault;
	afx_msg void OnBnClickedBtnDefault();
	afx_msg void OnBnClickedCheckDefault();
	void EnableControlItems(BOOL bEnable = TRUE);
	void InitPathInfo();
	CESMEditorEX m_ctrlHomePath;
	CESMEditorEX m_ctrlFileServerPath;
	CESMEditorEX m_ctrlLogPath;
	CESMEditorEX m_ctrlConfigPath;
	CESMEditorEX m_ctrlRecordPath;
	CESMEditorEX m_ctrlOutputPath;
	CESMEditorEX m_ctrlServerRamPath;
	CESMEditorEX m_ctrlClientRamPath;
	CESMEditorEX m_ctrlWorkPath;
	CESMEditorEX m_ctrlBMPSavePath;
	CESMButtonEx m_btnOpen;
};
