////////////////////////////////////////////////////////////////////////////////
//
//	TGPTZInfo.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TG.h"
#include "TGPTZInfo.h"
#include "TestStepPTZDef.h"
#include "TGPagePath.h"
#include "TGIni.h"
#include "TGUtil.h"


// CTGPTZInfo 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTGPTZInfo, CTGPropertyPage)

CTGPTZInfo::CTGPTZInfo()
{
	m_nType = 0;
	m_nTypeNum = 0;
}
CTGPTZInfo::~CTGPTZInfo(){}

//---------------------------------------------------------------
//-- Load Infomation from Each Model
//---------------------------------------------------------------


//------------------------------------------------------------------------------								
//! @brief		LoadInfoValueToInt
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
CString CTGPTZInfo::InfoGetPath()
{
	CString strFilePath;
	
	strFilePath.Format(TGGetPath(TG_PATH_CONFIG_PTZ));
	strFilePath.AppendFormat(_T("\\%s\\info\\%s.info"),TGGetCategory(),TGGetModel());

	return strFilePath;
}

//------------------------------------------------------------------------------								
//! @brief		LoadInfoValueToInt
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
int CTGPTZInfo::LoadInfoValueToInt(CString strSection, CString strKey)
{
	CString strFilePath;
	int nValue;
	
	strFilePath = InfoGetPath();

	CTGIni ini;
	if(!ini.SetIniFilename (strFilePath))
		return -1;

	nValue = ini.GetInt(strSection,strKey);

	return nValue;
}

//------------------------------------------------------------------------------								
//! @brief		LoadInfoValueToString
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
CString CTGPTZInfo::LoadInfoValueToString(CString strSection, CString strKey)
{
	CString strFilePath;
	CString strValue;
	
	strFilePath = InfoGetPath();

	CTGIni ini;
	if(!ini.SetIniFilename (strFilePath))
		return strValue;

	strValue = ini.GetString(strSection,strKey);

	return strValue;
}

//------------------------------------------------------------------------------								
//! @brief		GetSectionCount
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
int CTGPTZInfo::GetSectionCount(CString strModel, CString strItem)
{
	CString strFilePath;
	CString strValue;
	
	strFilePath = InfoGetPath();

	CTGIni ini;
	if(!ini.SetIniFilename (strFilePath))
		return -1;

	return ini.GetSectionCount(strItem);
}

//------------------------------------------------------------------------------								
//! @brief		GetSectionList
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
void CTGPTZInfo::GetSectionList(CString strModel, CString strItem, CStringArray& arList)
{
	CString strFilePath;
	CString strValue;
	
	strFilePath = InfoGetPath();

	CTGIni ini;
	ini.SetIniFilename (strFilePath);
	ini.GetSectionList(strItem, arList);
}

//------------------------------------------------------------------------------								
//! @brief		SetInfoValue
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
BOOL CTGPTZInfo::SetInfoValue(CString strItem,CString strKey,CString strValue)
{
	CString strFilePath;
	BOOL bResult = FALSE;
	
	strFilePath = InfoGetPath();

	CTGIni ini;
	if(!ini.SetIniFilename (strFilePath))
		return bResult;

	bResult = ini.WriteString(strItem,strKey,strValue);
	//TGLog(3,_T("[CGI:%s] %s:%s = %s"), TGGetModel(), strItem, strKey, strValue );

	return bResult;
}


//------------------------------------------------------------------------------								
//! @brief		GetInfoValuetoInt
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
int CTGPTZInfo::GetInfoValuetoInt(CString strItem,CString strKey)
{
	//TGLog(3,_T("[CGI:%s] %s:%s = %s"), TGGetModel(), strItem, strKey, strValue );
	return LoadInfoValueToInt(strItem,strKey);
}

//------------------------------------------------------------------------------								
//! @brief		GetInfoValuetoString
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
CString CTGPTZInfo::GetInfoValuetoString(CString strItem,CString strKey)
{
	//TGLog(3,_T("[CGI:%s] %s:%s = %s"), TGGetModel(), strItem, strKey, strValue );
	return LoadInfoValueToString(strItem,strKey);
}

//------------------------------------------------------------------------------								
//! @brief		SetPTZInfoPreset
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
void CTGPTZInfo::SetPTZInfoPreset(int nPreset, PTZ_COORDINATE tPTZPos)
{
	CString strTemp;
	CString strValue;

	strTemp.Format(_T("%d"),nPreset);
	strValue.Format(_T("%d,%d,%d"),tPTZPos.dwPan,tPTZPos.dwTilt,tPTZPos.dwZoom);

	SetInfoValue("PRESET",strTemp,strValue);
	TGLog(3, _T("[Set Preset Info : %3d] Pan :%d   Tilt:%d   Zoom:%d"), nPreset, tPTZPos.dwPan, tPTZPos.dwTilt, tPTZPos.dwZoom);
}

//------------------------------------------------------------------------------								
//! @brief		GetPTZInfoPreset
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
void CTGPTZInfo::GetPTZInfoPreset(int nPreset, PTZ_COORDINATE* tPTZPos)
{
	CString strTemp;
	CStringArray arrTemp;
	
	strTemp.Format(_T("%d"),nPreset);
	strTemp = GetInfoValuetoString("PRESET",strTemp);

	TGUtil::StringToken(strTemp,&arrTemp,_T(","));

	tPTZPos->dwPan = _ttoi(arrTemp.GetAt(0));
	tPTZPos->dwTilt = _ttoi(arrTemp.GetAt(1));
	tPTZPos->dwZoom = _ttoi(arrTemp.GetAt(2));

	TGLog(3, _T("[Get Preset Info : %3d] Pan  : %d	Tilt : %d	Zoom : %d"), nPreset, tPTZPos->dwPan, tPTZPos->dwTilt, tPTZPos->dwZoom);
}

//------------------------------------------------------------------------------								
//! @brief		SetPTZInfoTrace
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
void CTGPTZInfo::SetPTZInfoTrace(int nTrace, PTZ_COORDINATE tPTZPos)
{
	CString strTemp;
	CString strValue;

	strTemp.Format(_T("%d"),nTrace);
	strValue.Format(_T("%d,%d,%d"),tPTZPos.dwPan,tPTZPos.dwTilt,tPTZPos.dwZoom);

	SetInfoValue("TraceLocation",strTemp,strValue);
	TGLog(3, _T("Set [Trace:%3d]"), nTrace);
	TGLog(3, _T("[Set Trace Info] Pan  : %d	Tilt : %d	Zoom : %d"), tPTZPos.dwPan, tPTZPos.dwTilt, tPTZPos.dwZoom);
}

//------------------------------------------------------------------------------								
//! @brief		GetPTZInfoTrace
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
void CTGPTZInfo::GetPTZInfoTrace(int nTrace, PTZ_COORDINATE* tPTZPos)
{
	CString strTemp;
	CStringArray arrTemp;
	
	strTemp.Format(_T("%d"),nTrace);
	strTemp = GetInfoValuetoString("TraceLocation",strTemp);

	TGUtil::StringToken(strTemp,&arrTemp,_T(","));

	tPTZPos->dwPan = _ttoi(arrTemp.GetAt(0));
	tPTZPos->dwTilt = _ttoi(arrTemp.GetAt(1));
	tPTZPos->dwZoom = _ttoi(arrTemp.GetAt(2));

	TGLog(3, _T("[Get Trace:%3d]"), nTrace);
	TGLog(3, _T("[Set Trace Info] Pan  : %d	Tilt : %d	Zoom : %d"), tPTZPos->dwPan, tPTZPos->dwTilt, tPTZPos->dwZoom);
}
//------------------------------------------------------------------------------								
//! @brief		ClearSequence
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
BOOL CTGPTZInfo::ClearSequence(int nType, int nTypeNum)
{
	BOOL bResult = FALSE;
	
	CString strTypeNum;
	strTypeNum.Format( _T("%d"), nTypeNum);

	bResult = SetInfoValue(g_arStepPTZCommadOPT1[nType],strTypeNum,"");

	return bResult;
}

//------------------------------------------------------------------------------								
//! @brief		SetSequence
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
BOOL CTGPTZInfo::SetSequence(int nType, int nTypeNum, int nPreset)
{
	CString strFilePath;
	BOOL bResult = FALSE;
	
	strFilePath = InfoGetPath();

	CTGIni ini;
	if(!ini.SetIniFilename (strFilePath))
		return bResult;

	CString strTypeNum;
	strTypeNum.Format( _T("%d"), nTypeNum);

	bResult = ini.WriteIntAdd(g_arStepPTZCommadOPT1[nType],strTypeNum,nPreset);

	return bResult;
}

//------------------------------------------------------------------------------								
//! @brief		GetSequencePresetList
//! @date		2012-07-18
//! @owner		yongmin.lee
//------------------------------------------------------------------------------ 
CString CTGPTZInfo::GetSequenceList(int nType, int nTypeNum)
{
	CString strPresetList;
	CString strGroupList;
	CString strTypeNum;
	CStringArray arrTemp;

	strTypeNum.Format( _T("%d"), nTypeNum);

	switch(nType)
	{
	case PTZ_COM_OPT1_SWING:
		strPresetList = LoadInfoValueToString(g_arStepPTZCommadOPT1[PTZ_COM_OPT1_SWING],strTypeNum);
		break;
	case PTZ_COM_OPT1_GROUP:
		strPresetList = LoadInfoValueToString(g_arStepPTZCommadOPT1[PTZ_COM_OPT1_GROUP],strTypeNum);
		break;
	case PTZ_COM_OPT1_TOUR:
		strGroupList = LoadInfoValueToString(g_arStepPTZCommadOPT1[PTZ_COM_OPT1_TOUR],strTypeNum);
		TGUtil::StringToken(strGroupList,&arrTemp,_T(","));
		for(int i=0 ; i<arrTemp.GetSize() ; i++)
		{
			strPresetList += LoadInfoValueToString(g_arStepPTZCommadOPT1[PTZ_COM_OPT1_GROUP], arrTemp.GetAt(i));
			strPresetList += _T(",");
		}
		break;
	case PTZ_COM_OPT1_TRACE:
		strPresetList = LoadInfoValueToString(g_arStepPTZCommadOPT1[PTZ_COM_OPT1_TRACE],strTypeNum);
		break;
	}


	return strPresetList;
}