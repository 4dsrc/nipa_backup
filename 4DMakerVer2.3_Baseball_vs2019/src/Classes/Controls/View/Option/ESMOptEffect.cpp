////////////////////////////////////////////////////////////////////////////////
//
//	ESMOpt4DMaker.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMOptEffect.h"
#include "ESMOptPath.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMOptEffect 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOptEffect, CESMOptBase)

CESMOptEffect::CESMOptEffect()
	: CESMOptBase(CESMOptEffect::IDD)	
	, m_nPosX(0)
	, m_nPosY(0)
	, m_nWeight1(0)
	, m_nWeight2(0)
	, m_nRatio(0)
{	
}

CESMOptEffect::~CESMOptEffect()
{
}

void CESMOptEffect::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);
	DDX_Text(pDX,  IDC_EFFECT_POSX , m_nPosX);	
	DDX_Text(pDX,  IDC_EFFECT_POSY, m_nPosY);	
	DDX_Text(pDX,  IDC_EFFECT_RATIO, m_nRatio);
	DDX_Text(pDX,  IDC_EFFECT_WEIGHT1, m_nWeight1);
	DDX_Text(pDX,  IDC_EFFECT_WEIGHT2, m_nWeight2);
}

BEGIN_MESSAGE_MAP(CESMOptEffect, CESMOptBase)
END_MESSAGE_MAP()

void CESMOptEffect::InitProp(ESMEffect& Opt)
{
	m_nPosX		= Opt.nPosX;	
	m_nPosY		= Opt.nPosY;
	m_nRatio	= Opt.nRatio;
	m_nWeight1	= Opt.nWeight1;
	m_nWeight2	= Opt.nWeight2;
	
	UpdateData(FALSE);
}

BOOL CESMOptEffect::SaveProp(ESMEffect& Opt)
{
	if(!UpdateData(TRUE))
		return FALSE;

	Opt.nPosX = m_nPosX;
	Opt.nPosY = m_nPosY;
	Opt.nRatio= m_nRatio;
	Opt.nWeight1 = m_nWeight1;
	Opt.nWeight2 = m_nWeight2;
	return TRUE;
}
