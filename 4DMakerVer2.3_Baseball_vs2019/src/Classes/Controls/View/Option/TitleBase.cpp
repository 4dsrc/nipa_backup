/////////////////////////////////////////////////////////////////////////////
//
//  CommandDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TitleBase.h"
#include "SRSIndex.h"
#include "SdiDefines.h"
#include "ESMOptionDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define timer_transfer_check 0x00

// CTitleBase dialog
IMPLEMENT_DYNAMIC(CTitleBase,  CRSChildDialog)
CTitleBase::CTitleBase(CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CTitleBase::IDD, pParent)
{
	m_bSetting = FALSE;
	m_strTitleBarName = _T("");
	m_bMaxMin = FALSE;
}

CTitleBase::~CTitleBase() 
{
}
void CTitleBase::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);		
}

BEGIN_MESSAGE_MAP(CTitleBase, CRSChildDialog)	
	ON_WM_PAINT()	
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
//	ON_WM_LBUTTONUP()

	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)	
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

void CTitleBase::OnPaint()
{	
	CRect rect;
	GetClientRect(&rect);
	InvalidateRect(CRect(0,0, rect.Width(), rect.Height()), FALSE);
	DrawBackground();
	CRSChildDialog::OnPaint();
}

BOOL CTitleBase::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();


	return TRUE;
}

void CTitleBase::DrawBackground()
{	
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width() - 1, rect.Height(), WHITENESS); 

	//mDC.FillRect(rect, &CBrush(RGB(0x1e,0x1e,0x1e)));
	mDC.FillRect(rect, &CBrush(RGB(25,26,32)));
	//DrawStr(&mDC, STR_SIZE_TEXT_BOLD, m_strTitleBarName, CRect(rect.Width()/2, rect.Height()/2, 500, 20));
	DrawStr(&mDC, STR_SIZE_TEXT_BOLD, m_strTitleBarName, CRect(0, 0, rect.right, 20), RGB(0x92, 0x92, 0x92),  TRUE);

	m_rect[BTN_SETTING]		= CRect(0, 0, 24, 24);
	m_rect[BTN_MIN]		= CRect(rect.right-6-15-15-15, 5, rect.right-6-15-15-15+15, 5+15);
	m_rect[BTN_MAX]		= CRect(rect.right-6-15-15, 5, rect.right-6-15-15+15, 5+15);
	m_rect[BTN_CLOSE]	= CRect(rect.right-6-15, 5, rect.right-6-15+15, 5+15);

	DrawItem(&mDC, BTN_SETTING);
	DrawItem(&mDC, BTN_MIN);
	DrawItem(&mDC, BTN_MAX);
	DrawItem(&mDC, BTN_CLOSE);
	
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

void CTitleBase::DrawItem(CDC* pDC, int nItem)
{
	/*CString strSetting = RSGetRoot( _T("img\\Common\\sw_setting_nor.png"));
	CString strSettingSel = RSGetRoot( _T("img\\Common\\sw_setting_sel.png"));
	CString strMin = RSGetRoot( _T("img\\Common\\title_min_nor.png"));
	CString strMinSel = RSGetRoot( _T("img\\Common\\title_min_sel.png"));
	CString strMax = RSGetRoot( _T("img\\Common\\title_max_nor.png"));
	CString strMaxSel = RSGetRoot( _T("img\\Common\\title_max_sel.png"));
	CString strClose = RSGetRoot( _T("img\\Common\\title_close_nor.png"));
	CString strCloseSel = RSGetRoot( _T("img\\Common\\title_close_sel.png"));

	//CMiLRe 20150424 최대 사이즈에서 우측상단 이전 아이콘으로 변경

	if(m_bMaxMin)// 최대 사이즈 설정
	{
		strMax = RSGetRoot( _T("img\\Common\\title_normal_nor.png"));
		strMaxSel = RSGetRoot( _T("img\\Common\\title_normal_sel.png"));
	}

	switch(nItem)
	{
	case BTN_SETTING:
		if(m_nStatus[nItem] == BTN_INSIDE)		DrawGraphicsImage(pDC, strSettingSel	,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());
		else if(m_nStatus[nItem] == BTN_DOWN)	DrawGraphicsImage(pDC, strSetting,	m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else									DrawGraphicsImage(pDC, strSetting	,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());
		break;
	case BTN_MIN:
		if(m_nStatus[nItem] == BTN_INSIDE)		DrawGraphicsImage(pDC, strMinSel, m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());
		else if(m_nStatus[nItem] == BTN_DOWN)	DrawGraphicsImage(pDC, strMin, m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else									DrawGraphicsImage(pDC, strMin, m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());
		break;
	case BTN_MAX:
		if(m_nStatus[nItem] == BTN_INSIDE)		DrawGraphicsImage(pDC, strMaxSel	,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());
		else if(m_nStatus[nItem] == BTN_DOWN)	DrawGraphicsImage(pDC, strMax,	m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else									DrawGraphicsImage(pDC, strMax	,m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());
		break;
	case BTN_CLOSE:
		if(m_nStatus[nItem] == BTN_INSIDE)		DrawGraphicsImage(pDC, strCloseSel, m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else if(m_nStatus[nItem] == BTN_DOWN)	DrawGraphicsImage(pDC, strClose, m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height()); 
		else									DrawGraphicsImage(pDC, strClose, m_rect[nItem].left, m_rect[nItem]	.top, m_rect[nItem]	.Width(), m_rect[nItem]	.Height());
		break;
	}*/

	InvalidateRect(m_rect[nItem], FALSE);
}

void CTitleBase::OnMouseMove(UINT nFlags, CPoint point)
{
	m_nMouseIn = PtInRect(point, TRUE);
	CDialog::OnMouseMove(nFlags, point);
}

void CTitleBase::MouseEvent()
{
	if(m_bEnable)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.hwndTrack = m_hWnd;
		tme.dwFlags = TME_LEAVE;
		tme.dwHoverTime = 1;
		TrackMouseEvent(&tme);
	}
}

LRESULT CTitleBase::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_CMD_CNT; i ++)
		ChangeStatus(i,BTN_NORMAL);

	return 0L;
}   



void CTitleBase::OnTimer(UINT_PTR nIDEvent)
{
	/*switch(nIDEvent)
	{
	case timer_transfer_check:
		{
			CESMOptionDlg* pMain = (CESMOptionDlg*)GetParent();
			pMain->ShowWindow(SW_HIDE);
			if(pMain->m_bTransferFlag)
			{
#if 0
				RSEvent* pMsg	= new RSEvent;
				pMsg->message	= WM_RS_SET_KEY_MASK;
				pMsg->nParam1	= 10;
				pMsg->nParam2	= 10;
				::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
#endif
				KillTimer(timer_transfer_check);
				::SendMessage(pMain->GetSafeHwnd(), WM_CLOSE, NULL, NULL);
				break;
			}
		}
		break;
	}
	*/
	CRSChildDialog::OnTimer(nIDEvent);
}

BOOL CTitleBase::SetPosition(CPoint point)
{
	return FALSE;
}

int CTitleBase::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = BTN_CMD_NULL;
	int nStart = 0;

	for(int i = nStart ; i < BTN_CMD_CNT; i ++)
	{
		if(m_rect[i].PtInRect(pt))
		{
			nReturn = i;
			ChangeStatus(i,BTN_INSIDE);
		}
		else
			ChangeStatus(i,BTN_NORMAL);
	}
	return nReturn;
}

void CTitleBase::ChangeStatus(int nItem, int nStatus, BOOL bRedraw)
{
	if(m_nStatus[nItem] != nStatus)
	{
		m_nStatus[nItem] = nStatus;
		if(bRedraw)
		{
			CPaintDC dc(this);
			//-- 2011-08-02 hongsu.jung
			// Double Buffering
			CDC mDC;
			CBitmap mBitmap;		
			mDC.CreateCompatibleDC(&dc);
			mBitmap.CreateCompatibleBitmap(&dc, m_rect[nItem].Width(), m_rect[nItem].Height()); 
			mDC.SelectObject(&mBitmap);
			mDC.PatBlt(0,0,m_rect[nItem].Width(), m_rect[nItem].Height(), WHITENESS);

			DrawItem(&mDC, nItem);
			InvalidateRect(m_rect[nItem], FALSE);
			//-- 2011-08-02 hongsu.jung
			// Double Buffering
			dc.BitBlt(0, 0, m_rect[nItem].Width(), m_rect[nItem].Height(), &mDC, 0, 0, SRCCOPY);
		}
	}	
	UpdateWindow();
}

void CTitleBase::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CESMOptionDlg* pMain = (CESMOptionDlg*)GetParent();

	int nItem = PtInRect(point, FALSE);

//	int nAll = GetDeviceInfoArray()->GetPTPCount();
//	int nSelect = GetSelectDevice();

	switch(nItem)
	{
	case BTN_MAX:	
	case BTN_MIN:
	case BTN_SETTING:
	case BTN_CLOSE:
		break;
	default:
		{
			/*pMain->OptionSettingShow(SW_HIDE);
			m_bSetting = FALSE;

			if(!m_bMaxMin)
				pMain->SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, 0 );*/

			//if(!m_bMaxMin)
			//{
			//	ESMSetFullScreen(TRUE);
			//	m_bMaxMin = TRUE;
			//	pMain->ShowWindow(SW_MAXIMIZE);
			//	pMain->MaxCreateFrame();				
			//	this->Invalidate(FALSE);				
			//	
			//}
			//else
			//{
			//	ESMSetFullScreen(FALSE);
			//	m_bMaxMin = FALSE;
			//	pMain->ShowWindow(SW_NORMAL);
			//	pMain->MinCreateFrame();				
			//	this->Invalidate(FALSE);
			//	
			//}
		}
		break;
	}
	CRSChildDialog::OnLButtonDblClk(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CTitleBase::OnLButtonDown(UINT nFlags, CPoint point)
{
	CESMOptionDlg* pMain = (CESMOptionDlg*)GetParent();
	int nItem = PtInRect(point, FALSE);
	switch(nItem)
	{
	case BTN_SETTING:
	case BTN_MIN:
	case BTN_MAX:	
	case BTN_CLOSE:
		break;
	default:
		{
			/*pMain->OptionSettingShow(SW_HIDE);
			m_bSetting = FALSE;

			if(!m_bMaxMin)
			pMain->SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, 0 );*/
		}
		break;
	}
	CDialog::OnLButtonDown(nFlags, point);	
}

void CTitleBase::FullScreen()
{
	/*CRect rect;
	CESMOptionDlg* pMain = (CESMOptionDlg*)GetParent();

	if(!m_bMaxMin)
	{			

		pMain->GetFileListDlg()->SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
		pMain->SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
		
		ShowCursor( FALSE ); 
		ShowWindow(SW_HIDE);
		ESMSetFullScreen(TRUE);
		m_bMaxMin = TRUE;		
		pMain->ShowWindow(SW_MAXIMIZE);					
		pMain->MaxCreateFrame();			
		this->Invalidate(FALSE);
	}
	else
	{			
		ShowCursor( TRUE ); 
		TRACE("DEBUG : Normalize Window\n");
		ShowWindow(SW_SHOW);
		ESMSetFullScreen(FALSE);
		m_bMaxMin = FALSE;		
		pMain->ShowWindow(SW_NORMAL);	
		pMain->MinCreateFrame();		

		//pMain->SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
		pMain->GetFileListDlg()->SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
				
		this->Invalidate(FALSE);
	}
	pMain->GetClientRect(&rect);
	RSSetMainRect(rect);	*/	
}

HWND CTitleBase::CreateFullscreenWindow(HWND hwnd)
{
	CESMOptionDlg* pMain = (CESMOptionDlg*)GetParent();
	HINSTANCE g_hInst = GetModuleHandle(NULL);
	HMONITOR hmon = MonitorFromWindow(hwnd,
		MONITOR_DEFAULTTONEAREST);
	MONITORINFO mi = { sizeof(mi) };
	if (!GetMonitorInfo(hmon, &mi)) return NULL;
	return CreateWindow(TEXT("static"),
		TEXT("something interesting might go here"),
		WS_POPUP | WS_VISIBLE,
		mi.rcMonitor.left,
		mi.rcMonitor.top,
		mi.rcMonitor.right - mi.rcMonitor.left,
		mi.rcMonitor.bottom - mi.rcMonitor.top,
		hwnd, NULL, g_hInst, 0);
}

bool CTitleBase::EnterFullscreen(HWND hwnd, int fullscreenWidth, int fullscreenHeight, int colourBits, int refreshRate) 
{
	CESMOptionDlg* pMain = (CESMOptionDlg*)GetParent();
	DEVMODE fullscreenSettings;
	bool isChangeSuccessful;
	RECT windowBoundary;

	EnumDisplaySettings(NULL, 0, &fullscreenSettings);
	fullscreenSettings.dmPelsWidth        = fullscreenWidth;
	fullscreenSettings.dmPelsHeight       = fullscreenHeight;
	fullscreenSettings.dmBitsPerPel       = colourBits;
	fullscreenSettings.dmDisplayFrequency = refreshRate;
	fullscreenSettings.dmFields           = DM_PELSWIDTH |
		DM_PELSHEIGHT |
		DM_BITSPERPEL |
		DM_DISPLAYFREQUENCY;

	SetWindowLongPtr(hwnd, GWL_EXSTYLE, WS_EX_APPWINDOW | WS_EX_TOPMOST);
	SetWindowLongPtr(hwnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);
	::SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, fullscreenWidth, fullscreenHeight, SWP_SHOWWINDOW);
	isChangeSuccessful = ChangeDisplaySettings(&fullscreenSettings, CDS_FULLSCREEN) == DISP_CHANGE_SUCCESSFUL;
	::ShowWindow(hwnd, SW_MAXIMIZE);

	return isChangeSuccessful;
}

bool CTitleBase::ExitFullscreen(HWND hwnd, int windowX, int windowY, int windowedWidth, int windowedHeight, int windowedPaddingX, int windowedPaddingY) 
{
	bool isChangeSuccessful;
		
	SetWindowLongPtr(hwnd, GWL_STYLE, WS_VISIBLE);
	isChangeSuccessful = ChangeDisplaySettings(NULL, CDS_RESET) == DISP_CHANGE_SUCCESSFUL;
	::SetWindowPos(hwnd, HWND_NOTOPMOST, windowX, windowY, windowedWidth + windowedPaddingX, windowedHeight + windowedPaddingY, SWP_SHOWWINDOW);
	
	return isChangeSuccessful;
}

void CTitleBase::OnLButtonUp(UINT nFlags, CPoint point)
{
	CESMOptionDlg* pMain = (CESMOptionDlg*)GetParent();

	int nItem = PtInRect(point, FALSE);

//	int nAll = GetDeviceInfoArray()->GetPTPCount();
//	int nSelect = GetSelectDevice();

	switch(nItem)
	{
	case BTN_SETTING:
		if(!m_bSetting)
		{
			//pMain->OptionSettingShow(SW_SHOW);
			m_bSetting = TRUE;
		}
		else
		{
			//pMain->OptionSettingShow(SW_HIDE);
			m_bSetting = FALSE;
		}
		break;
	case BTN_MIN:
		pMain->ShowWindow(SW_SHOWMINIMIZED);
		break;
	case BTN_MAX:	
		{
			FullScreen();
		}
		break;
	case BTN_CLOSE:
#ifdef INTERVAL_CAPTURE_REMOVE
		pMain->GetIntervalPopup()->ShowWindow(SW_HIDE);
		pMain->GetIntervalPopup()->SetStop();
#endif
		/*if(RSGetRecordLive())
		{
			RSEvent* pMsg	= new RSEvent;
			pMsg->message	= WM_RS_SET_MOVIE_CANCEL;
			::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
		}

		SetTimer(timer_transfer_check, 100, NULL);*/
		
		break;
	default:
		{
			//pMain->OptionSettingShow(SW_HIDE);
			m_bSetting = FALSE;

			
		}
		break;
	}

	CRSChildDialog::OnLButtonUp(nFlags, point);
}
