////////////////////////////////////////////////////////////////////////////////
//
//	TGPageReport.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-10-26
//
////////////////////////////////////////////////////////////////////////////////



#pragma once
#include "TGPropertyPage.h"
#include "afxwin.h"

// CTGPageReport 대화 상자입니다.

class CTGPageReport : public CTGPropertyPage
{
	DECLARE_DYNAMIC(CTGPageReport)

public:
	CTGPageReport();
	virtual ~CTGPageReport();
	void InitProp(TGReport& report);
	BOOL SaveProp(TGReport& report);
// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_PAGE_REPORT};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()

private:
	int m_nReportOpt;
public:
	/*
	afx_msg void	OnBnClickedTgReportAuto	();
	afx_msg void	OnBnClickedTgReportLocal();	
	afx_msg void	OnBnClickedTgReportNone	();
	*/


};
