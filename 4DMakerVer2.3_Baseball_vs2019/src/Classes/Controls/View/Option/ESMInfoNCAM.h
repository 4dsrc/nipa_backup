////////////////////////////////////////////////////////////////////////////////
//
//	TGIngoCGI.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

//-- Sample
//-- H.264;1280X720;3fps;2048;10;CBR;Compression;3;CAVLC;BaseLine;On;
//-- MJPEG;1920X1080;1fps;64;Best;

#pragma once

// CTGIngoCGI 대화 상자입니다.
enum
{
	NCAM_ITEM_ENCORDING	= 0,
	NCAM_ITEM_RESOLUTION,
	NCAM_ITEM_FRAMERATE,
	NCAM_ITEM_BITCONTRO,
	NCAM_ITEM_COMPRESSION,
	NCAM_ITEM_ENCODINGPRIORITY,
	NCAM_ITEM_GOPSIZE,
	NCAM_ITEM_TYPEPROFILE,
	NCAM_ITEM_PROFILENUMBER,
	NCAM_ITEM_CHANNELNUMBER,
	NCAM_ITEM_CNT
};

//-- Profile Number
const CString g_arInfoNCAMItem[] =
{
	_T("Encording"),
	_T("Resolution"),
	_T("FrameRate"),
	_T("BitContro"),
	_T("Compression"),
	_T("EncodingPriority"),
	_T("GOPSize"),
	_T("TypeProfile"),
	_T("ProfileNumber"),
	_T("ChannelNumber")
};

class CTGIngoCGI
{
	DECLARE_DYNAMIC(CTGIngoCGI)

public:
	CTGIngoCGI();
	virtual ~CTGIngoCGI();

public:
	int GetPropertyValue(int nItem, CString strKey);
	CString GetPropertyKey(int nItem, int nValue);

private:
	int LoadInfoValue(CString strModel, CString strItem, CString strKey);
	CString LoadInfoKey(CString strModel, CString strItem, int nValue);

};