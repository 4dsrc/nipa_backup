////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptAdjust.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-10-05
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMFunc.h"
#include "algorithm"
#include "ESMIni.h"
#include "afxwin.h"
#include <Shlobj.h>
#include "afxcmn.h"
#include "ESMOptBase.h"
#include "resource.h"
#include "ESMEditorEX.h"

class CESMOptAdjustMovie : public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOptAdjustMovie)

public:
	CESMOptAdjustMovie();
	virtual ~CESMOptAdjustMovie();
	void InitProp(ESMAdjustMovie& Opt);
	BOOL SaveProp(ESMAdjustMovie& Opt);
	void LoadFolder();
	enum { IDD = IDD_OPTION_ADJUST_MOVIE };

public:
	int m_nTotalCount;
	int m_nCurrentCount;

	CComboBox m_Cb_StartFolder;
	CComboBox m_Cb_EndFolder;
	CESMEditorEX m_edit_tarforder;
	CESMEditorEX m_edit_srcforder;	
	CStringArray m_arrPathList;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()

public:	
	afx_msg void OnCbnCloseupCbOptEndfolder();
	afx_msg void OnCbnCloseupCbOptStartfolder();
	afx_msg void OnBnClickedBtnConvert();
	afx_msg void OnBnClickedBtnAdjSelect();
	afx_msg void OnBnClickedBtnOptSelectfolder();
	virtual BOOL OnInitDialog();
	CString m_strFolder;
	CString m_strTarFolder;
	CString m_strStartFolder;
	CString m_strEndFolder;
	afx_msg void OnChangeAdjSourceFolder();
	afx_msg void OnUpdateAdjSourceFolder();
	afx_msg void OnBnClickedBtnChange();
	CStatic m_ctrCurrentCount;
	CStatic m_ctrlTotalCount;
	void SetCount(int nCurrent, int nTotal);
	CStatic m_ctrlFrame;
};
