// ESMOptCeremony.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMOptCeremony.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



// CESMOptCeremony 대화 상자입니다.
IMPLEMENT_DYNAMIC(CESMOptCeremony, CESMOptBase)

CESMOptCeremony::CESMOptCeremony(CWnd* pParent /*=NULL*/)
	: CESMOptBase(CESMOptCeremony::IDD)
	, m_bCeremonyUse(FALSE)
	, m_bCeremonyPutImage(FALSE)
	, m_bCeremonyRatio(FALSE)
	, m_strDbAddrIP(_T(""))
	, m_strDbId(_T(""))
	, m_strDbPasswd(_T(""))
	, m_strDbName(_T(""))
	, m_strImagePath(_T(""))
	, m_strMoviePath(_T(""))
	, m_strWidth(_T(""))
	, m_strHeight(_T(""))
	, m_bStillImage(FALSE)
	, m_bUseCanonSelphy(FALSE)
{

}

CESMOptCeremony::~CESMOptCeremony()
{
}

void CESMOptCeremony::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);
	DDX_Check(pDX,IDC_CHECK_CEREMONY_CANON_SELPHY,m_bUseCanonSelphy);
	DDX_Check(pDX,IDC_CHECK_CEREMONY_STILLIMG,m_bStillImage);
	DDX_Check(pDX, IDC_CHECK_CEREMONY_USE, m_bCeremonyUse);
	DDX_Check(pDX, IDC_CHECK_CEREMONY_PUTIMAGE, m_bCeremonyPutImage);
	DDX_Check(pDX, IDC_CHECK_CEREMONY_RATIO, m_bCeremonyRatio);
	DDX_Text(pDX, IDC_EDT_CEREMONY_SIZE_WIDTH, m_strWidth);
	DDX_Text(pDX, IDC_EDT_CEREMONY_SIZE_HEIGHT, m_strHeight);
	DDX_Text(pDX, IDC_EDT_CEREMONY_DB_ADDRIP, m_strDbAddrIP);
	DDX_Text(pDX, IDC_EDT_CEREMONY_DB_ID, m_strDbId);
	DDX_Text(pDX, IDC_EDT_CEREMONY_DB_PASSWORD, m_strDbPasswd);
	DDX_Text(pDX, IDC_EDT_CEREMONY_DB_NAME, m_strDbName);
	DDX_Text(pDX, IDC_EDT_CEREMONY_IMAGEPATH, m_strImagePath);
	DDX_Text(pDX, IDC_EDT_CEREMONY_MOVIEPATH, m_strMoviePath);
	DDX_Control(pDX, IDC_EDT_CEREMONY_DB_ADDRIP, m_ctrlDBIP);
	DDX_Control(pDX, IDC_EDT_CEREMONY_DB_ID, m_ctrlDBID);
	DDX_Control(pDX, IDC_EDT_CEREMONY_DB_PASSWORD, m_ctrlDBPW);
	DDX_Control(pDX, IDC_EDT_CEREMONY_DB_NAME, m_ctrlDBName);
	DDX_Control(pDX, IDC_EDT_CEREMONY_SIZE_WIDTH, m_ctrlWidth);
	DDX_Control(pDX, IDC_EDT_CEREMONY_SIZE_HEIGHT, m_ctrlHeight);
	DDX_Control(pDX, IDC_EDT_CEREMONY_IMAGEPATH, m_ctrlImagePath);
	DDX_Control(pDX, IDC_EDT_CEREMONY_MOVIEPATH, m_ctrlMoviePath);
	DDX_Control(pDX, IDC_STATIC_CEREMONY_OPT, m_ctrlFrame1);
	DDX_Control(pDX, IDC_STATIC_DB_INFO, m_ctrlFrame2);
	DDX_Control(pDX, IDC_BTN_CEREMONY_IMAGEPATH, m_btnImagePath);
	DDX_Control(pDX, IDC_BTN_CEREMONY_MOVIEPATH, m_btnMoviePath);
}


BEGIN_MESSAGE_MAP(CESMOptCeremony, CESMOptBase)
	ON_BN_CLICKED(IDC_BTN_CEREMONY_IMAGEPATH, &CESMOptCeremony::OnBnClickedBtnCeremonyImagepath)
	ON_BN_CLICKED(IDC_BTN_CEREMONY_MOVIEPATH, &CESMOptCeremony::OnBnClickedBtnCeremonyMoviepath)
END_MESSAGE_MAP()


// CESMOptCeremony 메시지 처리기입니다.

BOOL CESMOptCeremony::SaveProp(ESMCeremony& SaveData)
{
	UpdateData();
	SaveData.bCeremonyUse	= m_bCeremonyUse;
	SaveData.bPutImage		= m_bCeremonyPutImage;
	SaveData.bCeremonyRatio = m_bCeremonyRatio;
	SaveData.strDbAddrIP	= m_strDbAddrIP;
	SaveData.strDbId		= m_strDbId;
	SaveData.strDbPasswd	= m_strDbPasswd;
	SaveData.strDbName		= m_strDbName;
	SaveData.strImagePath	= m_strImagePath;
	SaveData.strMoviePath	= m_strMoviePath;
	SaveData.strWidth		= m_strWidth;
	SaveData.strHeight		= m_strHeight;
	SaveData.bStillImage	= m_bStillImage;
	SaveData.bUseCanonSelphy = m_bUseCanonSelphy;
	return TRUE;
}

void CESMOptCeremony::InitProp(ESMCeremony& SaveData)
{
	m_bCeremonyUse = SaveData.bCeremonyUse;
	m_bCeremonyPutImage = SaveData.bPutImage;
	m_bCeremonyRatio = SaveData.bCeremonyRatio;

	m_strDbAddrIP	= SaveData.strDbAddrIP;
	m_strDbId		= SaveData.strDbId;
	m_strDbPasswd	= SaveData.strDbPasswd;
	m_strDbName		= SaveData.strDbName;
	m_strImagePath	= SaveData.strImagePath;
	m_strMoviePath	= SaveData.strMoviePath;
	m_strWidth		= SaveData.strWidth;
	m_strHeight		= SaveData.strHeight;
	m_bStillImage	= SaveData.bStillImage;
	m_bUseCanonSelphy = SaveData.bUseCanonSelphy;

	GetDlgItem(IDC_CHECK_CEREMONY_USE)->			SetWindowPos(NULL,21,29,120,14,NULL);
	GetDlgItem(IDC_CHECK_CEREMONY_PUTIMAGE)->		SetWindowPos(NULL,21,58,80,14,NULL);
	GetDlgItem(IDC_CHECK_CEREMONY_STILLIMG)->		SetWindowPos(NULL,21,87,80,14,NULL);
	GetDlgItem(IDC_CHECK_CEREMONY_CANON_SELPHY)->	SetWindowPos(NULL,21,116,140,14,NULL);

	GetDlgItem(IDC_EDT_CEREMONY_DB_ADDRIP)->		SetWindowPos(NULL,289,55,183,24,NULL);
	GetDlgItem(IDC_EDT_CEREMONY_DB_ID)->			SetWindowPos(NULL,289,89,183,24,NULL);
	GetDlgItem(IDC_EDT_CEREMONY_DB_PASSWORD)->		SetWindowPos(NULL,289,123,183,24,NULL);
	GetDlgItem(IDC_EDT_CEREMONY_DB_NAME)->			SetWindowPos(NULL,289,157,183,24,NULL);

	GetDlgItem(IDC_STATIC_DB_IP)->					SetWindowPos(NULL,199,55,80,24,NULL);
	GetDlgItem(IDC_STATIC_DB_ID)->					SetWindowPos(NULL,199,89,80,24,NULL);
	GetDlgItem(IDC_STATIC_DB_PW)->					SetWindowPos(NULL,199,123,80,24,NULL);
	GetDlgItem(IDC_STATIC_DB_NAME)->				SetWindowPos(NULL,199,157,80,24,NULL);

	GetDlgItem(IDC_EDT_CEREMONY_SIZE_WIDTH)->		SetWindowPos(NULL,160,237,246,24,NULL);
	GetDlgItem(IDC_EDT_CEREMONY_SIZE_HEIGHT)->		SetWindowPos(NULL,160,276,246,24,NULL);
	GetDlgItem(IDC_EDT_CEREMONY_IMAGEPATH)->		SetWindowPos(NULL,160,318,246,24,NULL);
	GetDlgItem(IDC_EDT_CEREMONY_MOVIEPATH)->		SetWindowPos(NULL,160,360,246,24,NULL);

	GetDlgItem(IDC_STATIC_WIDTH)->					SetWindowPos(NULL,21,237,80,24,NULL);
	GetDlgItem(IDC_STATIC_HEIGHT)->					SetWindowPos(NULL,21,276,80,24,NULL);
	GetDlgItem(IDC_STATIC_IMG_PATH)->				SetWindowPos(NULL,21,318,80,32,NULL);
	GetDlgItem(IDC_STATIC_MOVIE_PATH)->				SetWindowPos(NULL,21,360,80,32,NULL);

	GetDlgItem(IDC_CHECK_CEREMONY_RATIO)->			SetWindowPos(NULL,416,237,77,24,NULL);
	GetDlgItem(IDC_BTN_CEREMONY_IMAGEPATH)->		SetWindowPos(NULL,416,318,77,24,NULL);
	GetDlgItem(IDC_BTN_CEREMONY_MOVIEPATH)->		SetWindowPos(NULL,416,360,77,24,NULL);

	GetDlgItem(IDC_STATIC_CEREMONY_OPT)->			SetWindowPos(NULL,0,0,514,408,NULL);
	GetDlgItem(IDC_STATIC_DB_INFO)->				SetWindowPos(NULL,174,25,320,175,NULL);

	m_btnImagePath.ChangeFont(15);
	m_btnMoviePath.ChangeFont(15);

	UpdateData(FALSE);
}

void CESMOptCeremony::OnBnClickedBtnCeremonyImagepath()
{
	UpdateData();
	ITEMIDLIST        *pidlBrowse;
	TCHAR    pszPathname[MAX_PATH];
	BROWSEINFO BrInfo;
	BrInfo.hwndOwner = NULL; //GetSafeHwnd();
	BrInfo.pidlRoot = NULL;
	memset( &BrInfo, 0, sizeof(BrInfo) );
	BrInfo.pszDisplayName = pszPathname;
	BrInfo.lpszTitle = _T("Please select a directory");
	BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;

	// 다이얼로그를 띄우기
	pidlBrowse = ::SHBrowseForFolder(&BrInfo);   
	if( pidlBrowse != NULL)
	{
		// 패스를 얻어옴
		::SHGetPathFromIDList(pidlBrowse, pszPathname);   
		m_strImagePath = pszPathname;
	}
	UpdateData(FALSE);
}


void CESMOptCeremony::OnBnClickedBtnCeremonyMoviepath()
{
	UpdateData();
	ITEMIDLIST        *pidlBrowse;
	TCHAR    pszPathname[MAX_PATH];
	BROWSEINFO BrInfo;
	BrInfo.hwndOwner = NULL; //GetSafeHwnd();
	BrInfo.pidlRoot = NULL;
	memset( &BrInfo, 0, sizeof(BrInfo) );
	BrInfo.pszDisplayName = pszPathname;
	BrInfo.lpszTitle = _T("Please select a directory");
	BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;

	// 다이얼로그를 띄우기
	pidlBrowse = ::SHBrowseForFolder(&BrInfo);   
	if( pidlBrowse != NULL)
	{
		// 패스를 얻어옴
		::SHGetPathFromIDList(pidlBrowse, pszPathname);   
		m_strMoviePath = pszPathname;
	}
	UpdateData(FALSE);
}
