////////////////////////////////////////////////////////////////////////////////
//
//	TGPageControl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TG.h"
#include "TGPageControl.h"

// CTGPageControl 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTGPageControl, CESMOptBase)

CTGPageControl::CTGPageControl()
	: CESMOptBase(CTGPageControl::IDD)
	, m_bPowCheck(FALSE)
	, m_bSwitchCheck(FALSE)	
	, m_bRemoconCheck(FALSE)	
{
}

CTGPageControl::~CTGPageControl()
{
}

void CTGPageControl::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_OPT_POW,		m_bPowCheck);	
	DDX_Check(pDX, IDC_OPT_SWITCH,	m_bSwitchCheck);
	DDX_Check(pDX, IDC_OPT_REMOCON,	m_bRemoconCheck);		
}

void CTGPageControl::InitProp(TGControl& opt)
{
	m_bPowCheck		= opt.bCheck[TG_OPT_CHECK_POW];
	m_bSwitchCheck	= opt.bCheck[TG_OPT_CHECK_SWITCH];	
	m_bRemoconCheck	= opt.bCheck[TG_OPT_CHECK_REMOCON];	
	UpdateData(FALSE);
}

BOOL CTGPageControl::SaveProp(TGControl& opt)
{
	if(!UpdateData(TRUE))
		return FALSE;

	opt.bCheck[TG_OPT_CHECK_POW]		= m_bPowCheck;	
	opt.bCheck[TG_OPT_CHECK_SWITCH]		= m_bSwitchCheck;	
	opt.bCheck[TG_OPT_CHECK_REMOCON]	= m_bRemoconCheck;		

	return TRUE;
}

BEGIN_MESSAGE_MAP(CTGPageControl, CESMOptBase)
END_MESSAGE_MAP()
