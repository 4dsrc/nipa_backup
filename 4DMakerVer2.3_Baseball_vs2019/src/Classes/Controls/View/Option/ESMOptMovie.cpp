////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptMovie.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMOptMovie.h"
#include "ESMOptPath.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMOptMovie 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOptMovie, CESMOptBase)

CESMOptMovie::CESMOptMovie()
	: CESMOptBase(CESMOptMovie::IDD)	
	, m_nThreshold(ADJ_BASIC_VAL_THRESHOLD)
	, m_nFlowmoTime(ADJ_BASIC_VAL_FLOWMO_TIME)
	, m_nAutoBrightness(ADJ_BASIC_VAL_AUTOBRIGHTNESS)
	, m_bClockwise(TRUE)
	, m_bClockreverse(FALSE)
	, m_bOutputMP4(TRUE)
	, m_bOutputTS(FALSE)
 	, m_nFirstPointY(ADJ_BASIC_VAL_FIRSTPOINTY)
 	, m_nPointX(ADJ_BASIC_VAL_POINTX)
 	, m_nXRange(ADJ_BASIC_VAL_XRANGE)
 	, m_nFirstYRange(ADJ_BASIC_VAL_FIRSTYRANGE)
	, m_nSecondYRange(ADJ_BASIC_VAL_SECONDYRANGE)
	, m_nThirdYRange(ADJ_BASIC_VAL_THIRDYRANGE)
	, m_bRevisionUse(TRUE)
	, m_bRevReSizeImage(TRUE)
	, m_bInsertBanner(FALSE)
	, m_nTargetLength(0)
	, m_nCamZoom(0)
{
}

CESMOptMovie::~CESMOptMovie()
{
}

void CESMOptMovie::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);

	DDX_Text(pDX,  IDC_ADJUST_FIRSTPOINTY		, m_nFirstPointY);	
	DDX_Text(pDX,  IDC_ADJUST_POINTX			, m_nPointX);
	DDX_Text(pDX,  IDC_ADJUST_XRANGE			, m_nXRange);	
	DDX_Text(pDX,  IDC_ADJUST_FIRSTYRANGE		, m_nFirstYRange);
	DDX_Text(pDX,  IDC_ADJUST_SECONDYRANGE		, m_nSecondYRange);
	DDX_Text(pDX,  IDC_ADJUST_THIRDYRANGE		, m_nThirdYRange);
	DDX_Text(pDX,  IDC_ADJUST_TH				, m_nThreshold);	
	DDX_Text(pDX,  IDC_ADJUST_FLOW_T			, m_nFlowmoTime);
	DDX_Text(pDX,  IDC_ADJUST_AUTO_BRIGHTNESS	, m_nAutoBrightness);
	DDX_Check(pDX, IDC_ADJUST_ORDER_RTL			, m_bClockwise);	
	DDX_Check(pDX, IDC_ADJUST_ORDER_LTR			, m_bClockreverse);	
	DDX_Check(pDX, IDC_ADJUST_OUT_MP4			, m_bOutputMP4);	
	DDX_Check(pDX, IDC_ADJUST_OUT_TS			, m_bOutputTS);
	DDX_Check(pDX, IDC_ADJUST_REVISION_USE		, m_bRevisionUse);
	DDX_Check(pDX, IDC_ADJUST_RESIZEIMAGE		, m_bRevReSizeImage);
	DDX_Check(pDX, IDC_CHECK_INSERT_BANNER		, m_bInsertBanner);
	DDX_Text(pDX,  IDC_ADJUST_TARGETLENGTH		, m_nTargetLength);	
	DDX_Text(pDX,  IDC_ADJUST_CAMERAZOOM		, m_nCamZoom);	
}

void CESMOptMovie::InitProp(ESMAdjust& Opt)
{
	//-- 2013-10-11 jaehyun@esmlab.com
	//-- Change error value to basic value
	if(!Opt.nThresdhold)
		Opt.nThresdhold = ADJ_BASIC_VAL_THRESHOLD;
	if(!Opt.nFlowmoTime)
		Opt.nFlowmoTime = ADJ_BASIC_VAL_FLOWMO_TIME;
	if(!Opt.bClockwise && !Opt.bClockreverse)
		Opt.bClockwise = TRUE;
	if(!Opt.bOutputMP4 && !Opt.bOutputTS)
		Opt.bOutputMP4 = TRUE;

	m_nThreshold		= Opt.nThresdhold;
	m_nFlowmoTime		= Opt.nFlowmoTime;
	m_nAutoBrightness	= Opt.nAutoBrightness;
	m_bClockwise		= Opt.bClockwise;
	m_bClockreverse		= Opt.bClockreverse;
	m_bOutputMP4		= Opt.bOutputMP4;
	m_bOutputTS			= Opt.bOutputTS;
	m_bRevisionUse		= Opt.bRevisionUse;
	m_bRevReSizeImage	= Opt.bReSizeImage;
	m_bInsertBanner		= Opt.bInsertBanner;

	m_nFirstPointY		= Opt.nFirstPointY;
	m_nPointX			= Opt.nPointX;
	m_nXRange			= Opt.nXRange;
	m_nFirstYRange		= Opt.nFirstYRange;
	m_nSecondYRange		= Opt.nSecondYRange;
	m_nThirdYRange		= Opt.nThirdYRange;
	m_nTargetLength		= Opt.nTargetLength;
	m_nCamZoom			= Opt.nCamZoom;

	UpdateData(FALSE);
}

BOOL CESMOptMovie::SaveProp(ESMAdjust& Opt)
{
	if(!UpdateData(TRUE))
		return FALSE;
	Opt.nThresdhold		= m_nThreshold;
	Opt.nFlowmoTime		= m_nFlowmoTime;
	Opt.nAutoBrightness		= m_nAutoBrightness;
	Opt.bClockwise		= m_bClockwise;
	Opt.bClockreverse	= m_bClockreverse;
	Opt.bOutputMP4		= m_bOutputMP4;
	Opt.bOutputTS		= m_bOutputTS;
	Opt.bRevisionUse	= m_bRevisionUse;
	Opt.bReSizeImage	= m_bRevReSizeImage;
	Opt.bInsertBanner	= m_bInsertBanner;

	Opt.nFirstPointY	= m_nFirstPointY;
	Opt.nPointX			= m_nPointX;
	Opt.nXRange			 =m_nXRange;
	Opt.nTargetLength	 = m_nTargetLength;
	Opt.nFirstYRange		= m_nFirstYRange;
	Opt.nSecondYRange		= m_nSecondYRange;
	Opt.nThirdYRange		= m_nThirdYRange;
	Opt.nCamZoom			= m_nCamZoom;

	return TRUE;
}

BEGIN_MESSAGE_MAP(CESMOptMovie, CESMOptBase)
END_MESSAGE_MAP()
