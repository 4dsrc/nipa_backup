////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptionDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-07
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMOptionDlg.h"
#include "ESMIni.h"
#include "ESMFunc.h"
#include "ESMFileOperation.h"
#include "DSCItem.h"
#include "ESMUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CESMOptionDlg, CDialog)
BEGIN_MESSAGE_MAP(CESMOptionDlg, CDialog)
	ON_NOTIFY(TVN_SELCHANGED, IDC_PROP_TREE, OnTvnSelchangedPropTree)
	ON_NOTIFY(TVN_SELCHANGING, IDC_PROP_TREE, OnTvnSelchangingPropTree)
	ON_BN_CLICKED(IDC_OPTION_OK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_OPTION_CANCEL, OnBnClickedCancel)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

//------------------------------------------------------------------------------ 
//! @brief     Constructor
//! @date      
//! @owner     
//! @note      
//! @return    
//! @revision  
//------------------------------------------------------------------------------
CESMOptionDlg::CESMOptionDlg(CESMOption* pOption, int nInitPage, int nInitOpt, CWnd* pParent /*=NULL*/)
	: CDialog(CESMOptionDlg::IDD, pParent)
{
	m_pOption		= pOption;
	m_bDlgCreated	= false;
	m_brush.CreateSolidBrush(RGB(44,44,44));
	m_icon = AfxGetApp()->LoadIcon(IDI_ICON_CONFIGURATION);
	//m_icon = AfxGetApp()->LoadIcon(ID_IMAGE_OPTION);	

	m_nInitPage		= nInitPage;
	m_nInitOpt		= nInitOpt;
}

//------------------------------------------------------------------------------ 
//! @brief     Destructor
//! @date      
//! @owner     
//! @note      
//! @return    
//! @revision  
//------------------------------------------------------------------------------
CESMOptionDlg::~CESMOptionDlg()
{
}

//------------------------------------------------------------------------------ 
//! @brief     Called by the framework to exchange and validate dialog data.
//! @date      
//! @owner     
//! @note      
//! @return    
//! @revision  
//------------------------------------------------------------------------------
void CESMOptionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROP_TREE, m_crtTree);
	DDX_Control(pDX, IDC_OPTION_OK, m_btnOk);
	DDX_Control(pDX, IDC_OPTION_CANCEL, m_btnCancel);
}

HBRUSH CESMOptionDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	return m_brush;
}

void CESMOptionDlg::OnBnClickedCancel()
{
	OnCancel();
}

void CESMOptionDlg::OnBnClickedOk()
{
	if(Apply())
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_VIEW_SAVE_INFO;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
		return OnOK();
	}
	OnCancel();
}

//------------------------------------------------------------------------------ 
//! @brief     Saves the config file("C:\Program Files\Samsung\SMILE\config\info.tgw")
//! @date      2009-09-09
//! @owner     
//! @note      
//! @return    
//! @revision  2012-07-02 keunbae.song
//------------------------------------------------------------------------------ 
void CESMOptionDlg::FileSave(CString strFilename)
{
	CESMIni ini;
	if(!ini.SetIniFilename (strFilename))
		return;
	
	ini.WriteString(INFO_SECTION_PATH,		INFO_PATH_HOME,					m_pOption->m_Path.strPath[ESM_OPT_PATH_HOME]);
	ini.WriteString(INFO_SECTION_PATH,		INFO_PATH_FILESVR,				m_pOption->m_Path.strPath[ESM_OPT_PATH_FILESVR]);
	ini.WriteString(INFO_SECTION_PATH,		INFO_PATH_LOG,					m_pOption->m_Path.strPath[ESM_OPT_PATH_LOG]);
	ini.WriteString(INFO_SECTION_PATH,		INFO_PATH_CONFIG,				m_pOption->m_Path.strPath[ESM_OPT_PATH_CONF]);	
	ini.WriteString(INFO_SECTION_PATH,		INFO_PATH_WORK,					m_pOption->m_Path.strPath[ESM_OPT_PATH_WORK]);		
	ini.WriteString(INFO_SECTION_PATH,		INFO_PATH_RECORD,				m_pOption->m_Path.strPath[ESM_OPT_PATH_RECORD]);	
	ini.WriteString(INFO_SECTION_PATH,		INFO_PATH_OUTPUT,				m_pOption->m_Path.strPath[ESM_OPT_PATH_OUTPUT]);	
	ini.WriteString(INFO_SECTION_PATH,		INFO_PATH_SERVERRAM,			m_pOption->m_Path.strPath[ESM_OPT_PATH_SERVERRAM]);	
	ini.WriteString(INFO_SECTION_PATH,		INFO_PATH_CLIENTRAM,			m_pOption->m_Path.strPath[ESM_OPT_PATH_CLIENTRAM]);	
	ini.WriteString(INFO_SECTION_PATH,		INFO_PATH_SAVEIMG,				m_pOption->m_Path.strPath[ESM_OPT_PATH_SAVE_IMG]);	

	//jhhan
	ini.WriteBoolean(INFO_SECTION_PATH,		INFO_PATH_DEFAULT,				m_pOption->m_Path.bDefault);	

	//wgkim
	ini.WriteString(INFO_SECTION_BACKUP,	INFO_BACKUP_STRING_SRCFOLDER,	m_pOption->m_BackupString.strString[ESM_BACKUP_STRING_SRCFOLDER]);
	ini.WriteString(INFO_SECTION_BACKUP,	INFO_BACKUP_STRING_DSTFOLDER,	m_pOption->m_BackupString.strString[ESM_BACKUP_STRING_DSTFOLDER]);
	ini.WriteString(INFO_SECTION_BACKUP,	INFO_BACKUP_STRING_FTPHOST	,	m_pOption->m_BackupString.strString[ESM_BACKUP_STRING_FTPHOST]);
	ini.WriteString(INFO_SECTION_BACKUP,	INFO_BACKUP_STRING_FTPID,		m_pOption->m_BackupString.strString[ESM_BACKUP_STRING_FTPID]);	
	ini.WriteString(INFO_SECTION_BACKUP,	INFO_BACKUP_STRING_FTPPW,		m_pOption->m_BackupString.strString[ESM_BACKUP_STRING_FTPPW]);			

	ini.WriteString	(INFO_SECTION_LOG,		INFO_LOG_LOGNAME,				m_pOption->m_Log.strLogName);
	ini.WriteInt	(INFO_SECTION_LOG,		INFO_LOG_VERBOSITY,				m_pOption->m_Log.nVerbosity);
	ini.WriteInt	(INFO_SECTION_LOG,		INFO_LOG_LIMITDAY,				m_pOption->m_Log.nLimitday);
	ini.WriteBoolean(INFO_SECTION_LOG,		INFO_LOG_TRACETIME_Enable,		m_pOption->m_Log.bTraceTime);	

	ini.WriteBoolean(INFO_SECTION_RC,		INFO_RC_SERVERMAKE,				m_pOption->m_4DOpt.bMakeServer);
	ini.WriteBoolean(INFO_SECTION_RC,		INFO_RC_MODE,					m_pOption->m_4DOpt.bRCMode);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_SERVERMODE,				m_pOption->m_4DOpt.nServerMode);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_PORT,					m_pOption->m_4DOpt.nPort);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_SYNC_TIME_MARGIN,		m_pOption->m_4DOpt.nSyncTimeMargin);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_SENSOR_ON_TIME,			m_pOption->m_4DOpt.nSensorOnTime);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_TEMPLATESTARTMARGIN,	m_pOption->m_4DOpt.nTemplateStartMargin);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_TEMPLATEENDMARGIN,		m_pOption->m_4DOpt.nTemplateEndMargin);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_TEMPLATESTARTSLEEP,		m_pOption->m_4DOpt.nTemplateStartSleep);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_TEMPLATEENDSLEEP,		m_pOption->m_4DOpt.nTemplateEndSleep);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_WAIT_TIME_DSC,			m_pOption->m_4DOpt.nWaitSaveDSC);	
	//jhhan
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_WAIT_TIME_DSC_SUB,		m_pOption->m_4DOpt.nWaitSaveDSCSub);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_WAIT_TIME_DSC_SEL,		m_pOption->m_4DOpt.nWaitSaveDSCSel);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_TEST_MODE,				m_pOption->m_4DOpt.bTestMode);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_VIEW_INFO,				m_pOption->m_4DOpt.bViewInfo);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_MAKEANDAUTOPLAY,		m_pOption->m_4DOpt.bMakeAndAotuPlay);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_INSERTLOGO,				m_pOption->m_4DOpt.bInsertLogo);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_3DLOGO,					m_pOption->m_4DOpt.b3DLogo);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_LOGOZOOM,				m_pOption->m_4DOpt.bLogoZoom);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_INSERT_KZONE,			m_pOption->m_4DOpt.bInsertPrism);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_INSERTCAMERAID,			m_pOption->m_4DOpt.bInsertCameraID);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_TIMELINEPREVIEW,		m_pOption->m_4DOpt.bTimeLinePreview);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_ENABLE_BASEBALL,		m_pOption->m_4DOpt.bEnableBaseBall);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_REVERSE_MOVIE,			m_pOption->m_4DOpt.bReverseMovie);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_GPU_MAKE_FILE,			m_pOption->m_4DOpt.bGPUMakeFile);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_REPEAT_MOVIE,			m_pOption->m_4DOpt.bRepeatMovie);	

	//wgkim
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_TEMPLATE_POINT,			m_pOption->m_4DOpt.bTemplatePoint);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_TEMPLATE_STABILIZATION,	m_pOption->m_4DOpt.bTemplateStabilization);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_TEMPLATE_MODIFY,		m_pOption->m_4DOpt.bTemplateModify);	

	//wgkim 170727
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_LIGHT_WEIGHT,			m_pOption->m_4DOpt.bLightWeight);	

	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_ENABLE_VMCC,			m_pOption->m_4DOpt.bEnableVMCC);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_ENABLE_4DPCOPY,			m_pOption->m_4DOpt.bEnableCopy);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_BASEBALL_INNING,		m_pOption->m_4DOpt.nBaseBallInning);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_UHD_TO_FHD,				m_pOption->m_4DOpt.bUHDtoFHD);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_MOVIESAVELOCATION,		m_pOption->m_4DOpt.nMovieSaveLocation);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_DELETE_CNT,				m_pOption->m_4DOpt.nDeleteCnt);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_REPEAT_CNT,				m_pOption->m_4DOpt.nRepeatMovieCnt);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_DSC_SYNC_CNT,			m_pOption->m_4DOpt.nDSCSyncCnt);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_ENABLE_SAVE_IMG,		m_pOption->m_4DOpt.bSaveImg);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_ENABLE_CHECK_VALID,		m_pOption->m_4DOpt.bCheckValid);
	//160912 hjcho
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_KZONEVALUE,				m_pOption->m_4DOpt.nPrismValue);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_SYNCSKIP,				m_pOption->m_4DOpt.bSyncSkip);
	//161019 hjcho
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_KZONEVALUE1,			m_pOption->m_4DOpt.nPrismValue2);
	//161212 hjcho
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_INSERT_WHITEBAL,		m_pOption->m_4DOpt.bInsertWhiteBalance);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_COLORREVISION,			m_pOption->m_4DOpt.bColorRevision);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_PRINTCOLORINFO,			m_pOption->m_4DOpt.bPrnColorInfo);

	//170113 hjcho
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_AJALOGO,				m_pOption->m_4DOpt.bAJALogo);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_AJAREPLAY,				m_pOption->m_4DOpt.bAJAReplay);
	//170207 hjcho
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_AJAONOFF,				m_pOption->m_4DOpt.bAJAOnOff);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_AJASAMPLE,				m_pOption->m_4DOpt.nAJASapleCnt);

	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_GETRAMDISKSIZE,			m_pOption->m_4DOpt.bGetRamDiskSize);

	//jhhan 170327
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_CHECK_VALID_TIME,		m_pOption->m_4DOpt.nCheckVaildTime);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_DIV_FRAME,				m_pOption->m_4DOpt.nDivFrame);

	//joonho.kim 170628
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_PC_SYNC_TIME,			m_pOption->m_4DOpt.nPCSyncTime);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_DSC_SYNC_TIME,			m_pOption->m_4DOpt.nDSCSyncTime);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_DSC_WAIT_TIME,			m_pOption->m_4DOpt.nDSCSyncWaitTime);
	
	//hjcho 170706
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_GPUSKIP,				m_pOption->m_4DOpt.bGPUSkip);
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_AUTODETECT,				m_pOption->m_4DOpt.bAutoDetect);

	//hjcho 170830
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_DIRECT_MUX,				m_pOption->m_4DOpt.bDirectMux);

	//hjcho 170904
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_4DPMETHOD,				m_pOption->m_4DOpt.n4DPMethod);

	//wgkim
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_ENCODING_FOR_SMARTPHONES, m_pOption->m_4DOpt.bEncodingForSmartPhones);	
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_RECORD_FILE_SPLIT,		m_pOption->m_4DOpt.bRecordFileSplit);	

	//wgkim 180119
	ini.WriteInt	(INFO_SECTION_RC,		INFO_SECTION_RC_TEMPLATE_TARGETOUT_PERCENT, m_pOption->m_4DOpt.nTemplateTargetOutPercent)	;

	//hjcho 171210
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_FRAMEZOOMRATIO,			m_pOption->m_4DOpt.nFrameZoomRatio);

	//-- 2015-02-15 cygil@esmlab.com
	//-- Add picture coutdown
	ini.WriteInt	(INFO_SECTION_RC,		INFO_RC_COUNTDOWNLASTIP,		m_pOption->m_4DOpt.nCountdownLastIP);
	
	ini.WriteBoolean(INFO_SECTION_RC,		INFO_RC_REMOTE_USE,				m_pOption->m_4DOpt.bRemote);
	ini.WriteString(INFO_SECTION_RC,		INFO_RC_REMOTE_IP,				m_pOption->m_4DOpt.strRemote);	
	ini.WriteBoolean(INFO_SECTION_RC,		INFO_RC_REMOTE_SKIP,			m_pOption->m_4DOpt.bRemoteSkip);
	
	ini.WriteBoolean(INFO_SECTION_RC,		INFO_RC_HORIZON_ADJUST,			m_pOption->m_4DOpt.bHorizonAdj);

	ini.WriteBoolean(INFO_SECTION_RC,		INFO_RC_PROCESSOR_SHARE,		m_pOption->m_4DOpt.bProcessorShare);

	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_FUNC_KEY,				m_pOption->m_4DOpt.nFuncKey);

	//hjcho 180508
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_AJANETWORK,				m_pOption->m_4DOpt.bAJANetwork);
	ini.WriteString(INFO_SECTION_RC,		INFO_RC_AJANETWORK_IP,			m_pOption->m_4DOpt.strAJAIP);
	//hjcho 180629
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_GIF_MAKING,				m_pOption->m_4DOpt.bGIFMaking);
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_GIF_QUAILITY,			m_pOption->m_4DOpt.nGIFQuality);
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_GIF_SIZE	,			m_pOption->m_4DOpt.nGIFSize);

	//wgkim 180629
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_SET_FRAMERATE,			m_pOption->m_4DOpt.nSetFrameRateIndex);
	//hjcho 180718
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_REFEREEREAD,			m_pOption->m_4DOpt.bRefereeRead);
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_USE_RESYNC,				m_pOption->m_4DOpt.bUseReSync);

	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_RESYNC_REC_WAIT_TIME,	m_pOption->m_4DOpt.nResyncRecWaitTime);

	//jhhan 180828
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_AUTO_DELETE,			m_pOption->m_4DOpt.bAutoDelete);
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_STORED_TIME,			m_pOption->m_4DOpt.nStoredTime);
	//jhhan 180918
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_TEMPLATE_EDITOR,			m_pOption->m_4DOpt.bTemplateEditor);

	//wgkim 180921
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_AUTOADJUST_POSITION_TRACKING, m_pOption->m_4DOpt.bAutoAdjustPositionTracking );
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_AUTOADJUST_KZONE,		m_pOption->m_4DOpt.bAutoAdjustKZone);

	//hjcho 180927
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_RTSP,					m_pOption->m_4DOpt.bRTSP);
	ini.WriteBoolean(INFO_SECTION_RC,			INFO_RC_4DAP,					m_pOption->m_4DOpt.b4DAP);

	ini.WriteInt	(INFO_SECTION_MOVIE,	INFO_ADJ_FLOW_T,				m_pOption->m_Adjust.nFlowmoTime);	
	ini.WriteInt	(INFO_SECTION_MOVIE,	INFO_ADJ_AUTOBRIGHTNESS,		m_pOption->m_Adjust.nAutoBrightness);	
	ini.WriteInt	(INFO_SECTION_MOVIE,	INFO_ADJ_ORDER_RTL,				m_pOption->m_Adjust.bClockwise);	
	ini.WriteInt	(INFO_SECTION_MOVIE,	INFO_ADJ_ORDER_LTR,				m_pOption->m_Adjust.bClockreverse);

	ini.WriteInt	(INFO_SECTION_MOVIE,	INFO_ADJ_CAMERAZOOM,			m_pOption->m_Adjust.nCamZoom);	

	ini.WriteInt	(INFO_SECTION_MOVIE,	INFO_ADJ_TH,					m_pOption->m_Adjust.nThresdhold);
	ini.WriteInt	(INFO_SECTION_MOVIE,	INFO_ADJ_POINTGAPMIN,			m_pOption->m_Adjust.nPointGapMin);
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MIN_WIDTH,				m_pOption->m_Adjust.nMinWidth);	
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MIN_HEIGHT,			m_pOption->m_Adjust.nMinHeight);	
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MAX_WIDTH,				m_pOption->m_Adjust.nMaxWidth);	
	ini.WriteInt	(INFO_SECTION_ADJUST,	INFO_ADJ_MAX_HEIGHT,			m_pOption->m_Adjust.nMaxHeight);	

	ini.WriteString(INFO_SECTION_PATH,		INFO_PATH_FILESVR,				m_pOption->m_Path.strPath[ESM_OPT_PATH_FILESVR]);
	ini.WriteString	(INFO_SECTION_ADJUST_MOVIE,	INFO_ADJ_TARGETLENTH,		m_pOption->m_AdjustMovie.strTarPath);

	ini.WriteBoolean(INFO_SECTION_RC,		INFO_RC_FILE_2SEC,		m_pOption->m_4DOpt.bFile2Sec);

	//wgkim 181119
	ini.WriteInt(INFO_SECTION_RC,			INFO_RC_SET_TEMPLATE_MODIFY,			m_pOption->m_4DOpt.nSetTemplateModifyMode);
	
	//hjcho 190103
	ini.WriteInt(INFO_SECTION_RC, INFO_RC_BITRATE_FOR_ENCODING,m_pOption->m_4DOpt.nSmartPhoneBitrate);

	//190319
	ini.WriteInt(INFO_SECTION_RC,INFO_RC_RTSP_WAITTIME,m_pOption->m_4DOpt.nRTSPWaitTime);
	//Template

	/*ini.WriteString	(INFO_TEMPLATE,			INFO_TEMPLATE_CTRL1,			m_pOption->m_TemplateOpt.strTemplate1);
	ini.WriteString	(INFO_TEMPLATE,			INFO_TEMPLATE_CTRL2,			m_pOption->m_TemplateOpt.strTemplate2);
	ini.WriteString	(INFO_TEMPLATE,			INFO_TEMPLATE_CTRL3,			m_pOption->m_TemplateOpt.strTemplate3);
	ini.WriteString	(INFO_TEMPLATE,			INFO_TEMPLATE_CTRL4,			m_pOption->m_TemplateOpt.strTemplate4);
	//CMiLRe 20151013 Template Load INI File
	ini.WriteString	(INFO_TEMPLATE,			INFO_TEMPLATE_CTRL5,			m_pOption->m_TemplateOpt.strTemplate5);
	ini.WriteString	(INFO_TEMPLATE,			INFO_TEMPLATE_CTRL6,			m_pOption->m_TemplateOpt.strTemplate6);
	ini.WriteString	(INFO_TEMPLATE,			INFO_TEMPLATE_CTRL7,			m_pOption->m_TemplateOpt.strTemplate7);
	ini.WriteString	(INFO_TEMPLATE,			INFO_TEMPLATE_CTRL8,			m_pOption->m_TemplateOpt.strTemplate8);
	ini.WriteString	(INFO_TEMPLATE,			INFO_TEMPLATE_CTRL9,			m_pOption->m_TemplateOpt.strTemplate9);*/
	
	//2016/07/11 Lee SangA Template Load INI File
	//page 1
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE1_CTRL1,		m_pOption->m_TemplateOpt.strTemplate[0][0]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE1_CTRL2,		m_pOption->m_TemplateOpt.strTemplate[0][1]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE1_CTRL3,		m_pOption->m_TemplateOpt.strTemplate[0][2]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE1_CTRL4,		m_pOption->m_TemplateOpt.strTemplate[0][3]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE1_CTRL5,		m_pOption->m_TemplateOpt.strTemplate[0][4]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE1_CTRL6,		m_pOption->m_TemplateOpt.strTemplate[0][5]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE1_CTRL7,		m_pOption->m_TemplateOpt.strTemplate[0][6]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE1_CTRL8,		m_pOption->m_TemplateOpt.strTemplate[0][7]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE1_CTRL9,		m_pOption->m_TemplateOpt.strTemplate[0][8]);

	//page 2
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE2_CTRL1,		m_pOption->m_TemplateOpt.strTemplate[1][0]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE2_CTRL2,		m_pOption->m_TemplateOpt.strTemplate[1][1]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE2_CTRL3,		m_pOption->m_TemplateOpt.strTemplate[1][2]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE2_CTRL4,		m_pOption->m_TemplateOpt.strTemplate[1][3]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE2_CTRL5,		m_pOption->m_TemplateOpt.strTemplate[1][4]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE2_CTRL6,		m_pOption->m_TemplateOpt.strTemplate[1][5]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE2_CTRL7,		m_pOption->m_TemplateOpt.strTemplate[1][6]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE2_CTRL8,		m_pOption->m_TemplateOpt.strTemplate[1][7]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE2_CTRL9,		m_pOption->m_TemplateOpt.strTemplate[1][8]);

	//page 3
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE3_CTRL1,		m_pOption->m_TemplateOpt.strTemplate[2][0]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE3_CTRL2,		m_pOption->m_TemplateOpt.strTemplate[2][1]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE3_CTRL3,		m_pOption->m_TemplateOpt.strTemplate[2][2]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE3_CTRL4,		m_pOption->m_TemplateOpt.strTemplate[2][3]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE3_CTRL5,		m_pOption->m_TemplateOpt.strTemplate[2][4]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE3_CTRL6,		m_pOption->m_TemplateOpt.strTemplate[2][5]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE3_CTRL7,		m_pOption->m_TemplateOpt.strTemplate[2][6]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE3_CTRL8,		m_pOption->m_TemplateOpt.strTemplate[2][7]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE3_CTRL9,		m_pOption->m_TemplateOpt.strTemplate[2][8]);

	//page 4
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE4_CTRL1,		m_pOption->m_TemplateOpt.strTemplate[3][0]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE4_CTRL2,		m_pOption->m_TemplateOpt.strTemplate[3][1]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE4_CTRL3,		m_pOption->m_TemplateOpt.strTemplate[3][2]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE4_CTRL4,		m_pOption->m_TemplateOpt.strTemplate[3][3]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE4_CTRL5,		m_pOption->m_TemplateOpt.strTemplate[3][4]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE4_CTRL6,		m_pOption->m_TemplateOpt.strTemplate[3][5]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE4_CTRL7,		m_pOption->m_TemplateOpt.strTemplate[3][6]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE4_CTRL8,		m_pOption->m_TemplateOpt.strTemplate[3][7]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE4_CTRL9,		m_pOption->m_TemplateOpt.strTemplate[3][8]);

	//page 5
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE5_CTRL1,		m_pOption->m_TemplateOpt.strTemplate[4][0]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE5_CTRL2,		m_pOption->m_TemplateOpt.strTemplate[4][1]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE5_CTRL3,		m_pOption->m_TemplateOpt.strTemplate[4][2]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE5_CTRL4,		m_pOption->m_TemplateOpt.strTemplate[4][3]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE5_CTRL5,		m_pOption->m_TemplateOpt.strTemplate[4][4]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE5_CTRL6,		m_pOption->m_TemplateOpt.strTemplate[4][5]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE5_CTRL7,		m_pOption->m_TemplateOpt.strTemplate[4][6]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE5_CTRL8,		m_pOption->m_TemplateOpt.strTemplate[4][7]);
	ini.WriteString(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE5_CTRL9,		m_pOption->m_TemplateOpt.strTemplate[4][8]);

	// Page number
	ini.WriteString	(INFO_TEMPLATE,			INFO_TEMPLATE_PAGE,				m_pOption->m_TemplateOpt.strSelectPage);

	// Ceremony
	ini.WriteInt	(INFO_CEREMONY,			INFO_CEREMONY_USE,				m_pOption->m_Ceremony.bCeremonyUse);
	ini.WriteInt	(INFO_CEREMONY,			INFO_CEREMONY_PUTIMAGE,			m_pOption->m_Ceremony.bPutImage);
	ini.WriteInt	(INFO_CEREMONY,			INFO_CEREMONY_RATIO,			m_pOption->m_Ceremony.bCeremonyRatio);
	ini.WriteInt	(INFO_CEREMONY,			INFO_CEREMONY_STILLIMG,			m_pOption->m_Ceremony.bStillImage);
	ini.WriteInt	(INFO_CEREMONY,			INFO_CEREMONY_CANON_SELPHY,		m_pOption->m_Ceremony.bUseCanonSelphy);

	ini.WriteString	(INFO_CEREMONY,			INFO_CEREMONY_DBADDRIP,			m_pOption->m_Ceremony.strDbAddrIP);
	ini.WriteString	(INFO_CEREMONY,			INFO_CEREMONY_DBID,				m_pOption->m_Ceremony.strDbId);
	ini.WriteString	(INFO_CEREMONY,			INFO_CEREMONY_DBPW,				m_pOption->m_Ceremony.strDbPasswd);
	ini.WriteString	(INFO_CEREMONY,			INFO_CEREMONY_DBNAME,			m_pOption->m_Ceremony.strDbName);
	ini.WriteString	(INFO_CEREMONY,			INFO_CEREMONY_DBIMAGEPATH,		m_pOption->m_Ceremony.strImagePath);
	ini.WriteString	(INFO_CEREMONY,			INFO_CEREMONY_DBMOVIEPATH,		m_pOption->m_Ceremony.strMoviePath);
	ini.WriteString	(INFO_CEREMONY,			INFO_CEREMONY_WIDTH,			m_pOption->m_Ceremony.strWidth);
	ini.WriteString	(INFO_CEREMONY,			INFO_CEREMONY_HEIGHT,			m_pOption->m_Ceremony.strHeight);
	ini.WriteString	(INFO_MANAGEMENT, INFO_MANAGEMENT_SELFILEPATH,		m_pOption->m_menagement.strSelectFilePath);
	ini.WriteString	(INFO_MANAGEMENT, INFO_MANAGEMENT_SAVEFILEPATH,		m_pOption->m_menagement.strSaveFilePath);


	ini.WriteString	(INFO_SECTION_CAM,			INFO_CAM_VIEW,			m_pOption->m_CamInfo.strView);

	// 	//-- Camera Info
// 	CString strCamSectionName;
// 	CString strCamSectionIP;
// 	ini.DeleteSection(INFO_CAM_INFO);
// 	for( int i =0 ;i < m_pOption->m_CamInfo.nCamCount; i++)
// 	{
// 		strCamSectionName.Format(_T("%s_%d"), INFO_CAM_NAME, i);
// 		ini.WriteString	(INFO_CAM_INFO,	 strCamSectionName		, m_pOption->m_CamInfo.strCamID[i]);
// 		strCamSectionIP.Format(_T("%s_%d"), INFO_CAM_IP, i);
// 		ini.WriteString	(INFO_CAM_INFO,	 strCamSectionIP		, m_pOption->m_CamInfo.strCamIP[i]);
// 	}
	ExcelSave(); //-- Camera Info ini 파일에서 Excel로 변경
	ExcelSavePC();
	//CMiLRe 20151014 Template Save INI File
	ResetLoadTemplate();
	//CMiLRe 20151013 Template Load INI File
	m_pOption->LoadTemplateFormFile();
}

void CESMOptionDlg::ExcelSave()
{
	CFile file;
	CString strFilePath, WriteData;
	strFilePath.Format(_T("%s\\CameraList.csv"), ESMGetPath(ESM_PATH_SETUP));
	CESMFileOperation fo;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	char* pstrData = NULL;

	CString _UsedFlag, _makeFlag, _reverseFlag, _DirectionFlag, _VerticalFlag, _DetectFlag;
	if(file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
	{
		int nWidth = 0;
		int nHeight = 0;
		for( int i = 0 ;i < m_pOption->m_CamInfo.nCamCount; i++)
		{
			if(m_pOption->m_CamInfo.usedFlag[i])
			{
				_UsedFlag = _T("TRUE");
			}
			else
			{
				_UsedFlag = _T("FALSE");
			}

			//jhhan 16-10-10
			if(m_pOption->m_CamInfo.makeFlag[i])
			{
				_makeFlag = _T("TRUE");
			}
			else
			{
				_makeFlag = _T("FALSE");
			}

			//hjcho 180312
			if(m_pOption->m_CamInfo.reverse[i])
			{
				_reverseFlag = _T("REVERSE");
			}
			else
			{
				_reverseFlag = _T("NORMAL");
			}

			//wgkim 180512
			if(m_pOption->m_CamInfo.nArrVertical[i] == 0)
			{
				_VerticalFlag = _T("NORMAL");
			}
			else if(m_pOption->m_CamInfo.nArrVertical[i] == -1)
			{
				_VerticalFlag = _T("-90 Degree");
			}
			else if(m_pOption->m_CamInfo.nArrVertical[i] == 1)
			{
				_VerticalFlag = _T("90 Degree");
			}
			else
			{
				_VerticalFlag = _T("NORMAL");
			}

			//hjcho 180312
			if(m_pOption->m_CamInfo.detectFlag[i])
			{
				_DetectFlag = _T("TRUE");
			}
			else
			{
				_DetectFlag = _T("FALSE");
			}
					
			//WriteData.Format(_T("%s, %s, %s, %s, %s, %s"), m_pOption->m_CamInfo.strCamID[i], m_pOption->m_CamInfo.strCamIP[i],_UsedFlag, _makeFlag, m_pOption->m_CamInfo.strCamName[i],_reverseFlag);
			//jhhan 180508 Group
			WriteData.Format(_T("%s, %s, %s, %s, %s, %s, %s, %s, %s"), 
				m_pOption->m_CamInfo.strCamID[i], 
				m_pOption->m_CamInfo.strCamIP[i],
				_UsedFlag, 
				_makeFlag, 
				m_pOption->m_CamInfo.strCamName[i],
				_reverseFlag, 
				m_pOption->m_CamInfo.strGroup[i], 
				_VerticalFlag,
				_DetectFlag);
			//180312 hjcho camera list에서 adjust data 삭제

//			CString strDscId = m_pOption->m_CamInfo.strCamID[i];
//			
//			CString strAdj;
//			for(int j = 0; j < arDSCList.GetCount(); j++)
//			{
//				CDSCItem * pItem = NULL;
//				pItem = (CDSCItem * )arDSCList.GetAt(j);
//				
//				if(strDscId.CompareNoCase(pItem->GetDeviceDSCID()) == 0)
//				{
//					YUVMatrix matrix = SetRotMatrix(j);
//#if 1
//					strAdj = WriteMatValue(matrix);
//#else
//					//strAdj.Format(_T(", %f, %f, %f, %f, %f, %f, %d, %d"), pItem->GetDSCAdj(DSC_ADJ_A),pItem->GetDSCAdj(DSC_ADJ_SCALE),pItem->GetDSCAdj(DSC_ADJ_RX),pItem->GetDSCAdj(DSC_ADJ_RY),pItem->GetDSCAdj(DSC_ADJ_X),pItem->GetDSCAdj(DSC_ADJ_Y),pItem->GetWidth(),pItem->GetHeight());
//					strAdj.Format(_T(", %f, %f, %f, %f, %f, %f"), 
//						pItem->GetDSCAdj(DSC_ADJ_A),pItem->GetDSCAdj(DSC_ADJ_SCALE),
//						pItem->GetDSCAdj(DSC_ADJ_RX),pItem->GetDSCAdj(DSC_ADJ_RY),
//						pItem->GetDSCAdj(DSC_ADJ_X),pItem->GetDSCAdj(DSC_ADJ_Y));
//#endif
//					nWidth = pItem->GetWidth();
//					nHeight = pItem->GetHeight();
//
//					break;
//				}
//			}
//			WriteData.Append(strAdj);
			WriteData.Append(_T("\r\n"));
			
			pstrData = ESMUtil::CStringToChar(WriteData);
			file.Write(pstrData, strlen(pstrData));
			delete[] pstrData;
			pstrData = NULL;

			
		}
		//WriteData.Format(_T("%s, %d, %d, %d, %d\r\n"), _T("INFO"), nWidth, nHeight, ESMGetMarginX(),  ESMGetMarginY());
		//pstrData = ESMUtil::CStringToChar(WriteData);
		//file.Write(pstrData, strlen(pstrData));
		//delete[] pstrData;
		//pstrData = NULL;
		
		file.Close();
	}

/*	wchar_t chThisPath[_MAX_PATH];
	GetCurrentDirectory( _MAX_PATH, chThisPath);
	UpdateData(TRUE);
	CString strThisPath ;

	strThisPath.Format(_T("%s\\%s.xlsx"), ESMGetPath(ESM_PATH_SETUP), _T("4DMaker"));
	GetModuleFileName( NULL, chThisPath, _MAX_PATH);
	CXLEzAutomation XL(FALSE); 
	XL.OpenExcelFile(strThisPath);
	CString strData;
	while(1)
	{
		strData = XL.GetCellValue(1, 1);
		if( strData == _T(""))
			break;
		else
			XL.DeleteRow(1);
	}

	for( int i =0 ;i < m_pOption->m_CamInfo.nCamCount; i++)
	{
		XL.SetCellValue(1, i+1, m_pOption->m_CamInfo.strCamID[i]);
		XL.SetCellValue(2, i+1, m_pOption->m_CamInfo.strCamIP[i]);
	}
	XL.SaveFileAs(strThisPath);
	XL.ReleaseExcel(); */
}

void CESMOptionDlg::ExcelSavePC()
{
	CFile file;
	CString strFilePath, WriteData;
	strFilePath.Format(_T("%s\\PCList.csv"), ESMGetPath(ESM_PATH_SETUP));
	CESMFileOperation fo;
	CObArray pItemList;
	ESMGetDSCList(&pItemList);
	char* pstrData = NULL;

	if(file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
	{
		for( int i = 0 ;i < m_pOption->m_PcInfo.nPCount; i++)
		{
			WriteData.Format(_T("%s\r\n"), m_pOption->m_PcInfo.strPCIP[i]);
			pstrData = ESMUtil::CStringToChar(WriteData);
			file.Write(pstrData, strlen(pstrData));
			delete[] pstrData;
			pstrData = NULL;
		}
		file.Close();
	}
}
//------------------------------------------------------------------------------ 
//! @brief     Called in response to the WM_INITDIALOG message.
//! @date      
//! @owner     
//! @note      
//! @return    
//! @revision  
//------------------------------------------------------------------------------ 
BOOL CESMOptionDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	//this->SetIcon(m_icon, FALSE);

	

	CString strTitle;
	HTREEITEM hTreeItem,hSelectedItemLogFilter;		//Root

	//-- 2010-02-3 hongsu.jung
	//-- Init Open Page 1. Log Filter
	
	//-- Environment


	hTreeItem = m_crtTree.InsertItem(_T("Environment"), TVI_ROOT, TVI_LAST);
	// * DWORD(32 bit) casting might be lost ponter value at 64bit Build so change to DWORD_PTR : Siseong Ahn 20160614
	m_crtTree.SetItemData(hTreeItem, (DWORD_PTR)& m_OptPath); // Path
	//- Basic
	m_hItem[ENV_PATH_SET] = m_crtTree.InsertItem(_T("Path Setting(F6)"), hTreeItem, TVI_LAST);
	m_crtTree.SetItemData(m_hItem[ENV_PATH_SET], (DWORD_PTR)& m_OptPath); // Path
	//- Adjustment
#ifndef _4DMODEL
	//m_hItem[ENV_ADJUST_OPT] = m_crtTree.InsertItem(_T("Adjustment Options"), hTreeItem, TVI_LAST);
	//m_crtTree.SetItemData(m_hItem[ENV_ADJUST_OPT], (DWORD)&m_OptAdjust);	//  - Control Option
#endif
	//- AdjustMovie
	//m_hItem[ENV_ADJUST_MOV] = m_crtTree.InsertItem(_T("Adjustment Movie"), hTreeItem, TVI_LAST);
	//m_crtTree.SetItemData(m_hItem[ENV_ADJUST_MOV], (DWORD)&m_OptAdjustMovie);

	//- Test Option
	m_hItem[ENV_4DM_OPT] = m_crtTree.InsertItem(_T("4DReplay Option(F7)"), hTreeItem, TVI_LAST);
	m_crtTree.SetItemData(m_hItem[ENV_4DM_OPT], (DWORD_PTR)& m_Opt4DMaker);	//  - Control Option
	//- Camera Information
	m_hItem[ENV_CAMERA_INFO] = m_crtTree.InsertItem(_T("Camera Information(F8)"), hTreeItem, TVI_LAST);
	m_crtTree.SetItemData(m_hItem[ENV_CAMERA_INFO], (DWORD_PTR)& m_OptCamInfo);	//  - Camera Information m_OptPCInfo

	m_hItem[ENV_PC_INFO] = m_crtTree.InsertItem(_T("PC Information(F9)"), hTreeItem, TVI_LAST);
	m_crtTree.SetItemData(m_hItem[ENV_PC_INFO], (DWORD_PTR)& m_OptPCInfo);	//  - Camera Information 

	m_hItem[ENV_TEMPLATE_OPT] = m_crtTree.InsertItem(_T("Template Option(F10)"), hTreeItem, TVI_LAST);
	m_crtTree.SetItemData(m_hItem[ENV_TEMPLATE_OPT], (DWORD_PTR)& m_OptTemplate);	//  - Effect Option

	m_hItem[ENV_CEREMONY] = m_crtTree.InsertItem(_T("Event(F11)"), hTreeItem, TVI_LAST);
	m_crtTree.SetItemData(m_hItem[ENV_CEREMONY], (DWORD_PTR)& m_OptCeremony);	//  - Ceremony Option

		
	//m_hItem[ENV_MANAGEMENT] = m_crtTree.InsertItem(_T("Management"), hTreeItem, TVI_LAST);
	//m_crtTree.SetItemData(m_hItem[ENV_MANAGEMENT], (DWORD)&m_OptManagement);	//  - Movie File Copyt
			
	//-- Output 
	//hSelectedItemLogFilter = m_crtTree.InsertItem(_T("Output"), TVI_ROOT, TVI_LAST);
	//m_crtTree.SetItemData(hSelectedItemLogFilter, (DWORD)&m_OptLog);
	//m_hItem[ENV_LOG_OPTION] = m_crtTree.InsertItem(_T("Log Option"), hSelectedItemLogFilter, TVI_LAST);
	//m_crtTree.SetItemData(m_hItem[ENV_LOG_OPTION], (DWORD)&m_OptLog);		//-- Log
	//- Log Filter
	//m_hItem[ENV_LOG_FILTER] = m_crtTree.InsertItem(_T("Log Filter"), hSelectedItemLogFilter, TVI_LAST);
	//m_crtTree.SetItemData(m_hItem[ENV_LOG_FILTER], (DWORD)&m_OptLogFilter);	//-- Filter	

	//------------------------------------------------------------------------------
	//-- Date	 : 2010-02-3 
	//-- Owner	 : hongsu.jung
	//-- Comment : Create Page
	//------------------------------------------------------------------------------	
	m_OptPath		.Create(IDD_OPTION_PATH			, this);
	m_Opt4DMaker	.Create(IDD_OPTION_4DMAKER		, this);
#ifndef _4DMODEL
	m_OptAdjust		.Create(IDD_OPTION_ADJUST		, this);
#endif
	m_OptAdjustMovie.Create(IDD_OPTION_ADJUST_MOVIE , this);
	m_OptLog		.Create(IDD_OPTION_LOG			, this);
	m_OptLogFilter	.Create(IDD_OPTION_LOG_FILTER	, this);
	m_OptCamInfo	.Create(IDD_OPTION_CAMINFO		, this);
	m_OptPCInfo		.Create(IDD_OPTION_PCINFO		, this);
	m_OptTemplate	.Create(IDD_OPTION_TEMPLATE		, this);
	m_OptCeremony	.Create(IDD_OPTION_CEREMONY		, this);
	m_OptManagement	.Create(IDD_OPTION_MANAGEMANT	, this);

	//------------------------------------------------------------------------------
	//-- Date	 : 2010-02-3 
	//-- Owner	 : hongsu.jung
	//-- Comment : Initialize property
	//------------------------------------------------------------------------------
	m_OptPath		.InitProp(m_pOption->m_Path		);
	m_Opt4DMaker	.InitProp(m_pOption->m_4DOpt	);
#ifndef _4DMODEL
	m_OptAdjust		.InitProp(m_pOption->m_Adjust	);	
	
#endif
	m_OptAdjustMovie.InitProp(m_pOption->m_AdjustMovie	);	
	m_OptLog		.InitProp(m_pOption->m_Log		);
	m_OptLogFilter	.InitProp(m_pOption->m_LogFilter);
	m_OptCamInfo	.InitProp(m_pOption->m_CamInfo	);
	m_OptPCInfo		.InitProp(m_pOption->m_PcInfo	);
	m_OptCeremony	.InitProp(m_pOption->m_Ceremony	);
	m_OptManagement .InitProp(m_pOption->m_menagement);
	m_OptTemplate	.InitProp(m_pOption->m_TemplateOpt);
	m_OptManagement	.InitProp(m_pOption->m_FileCopy);
	m_OptManagement	.InitProp(m_pOption->m_BackupString);

	//------------------------------------------------------------------------------
	//-- Date	 : 2010-02-3 
	//-- Owner	 : hongsu.jung
	//-- Comment : Create Path Directory
	//------------------------------------------------------------------------------
	m_pOption->CreateDir();
	CRect rcDlgs(230+34,0+25,800,671);
	//GetDlgItem(IDC_DLG_AREA)->GetWindowRect(rcDlgs);
	//ScreenToClient(rcDlgs);

	m_OptPath		.MoveWindow(rcDlgs);
	m_Opt4DMaker	.MoveWindow(rcDlgs);
#ifndef _4DMODEL
	m_OptAdjust		.MoveWindow(rcDlgs);
#endif
	m_OptAdjustMovie.MoveWindow(rcDlgs);
	m_OptLog		.MoveWindow(rcDlgs);
	m_OptLogFilter	.MoveWindow(rcDlgs);
	m_OptCamInfo	.MoveWindow(rcDlgs);
	m_OptPCInfo		.MoveWindow(rcDlgs);
	m_OptTemplate	.MoveWindow(rcDlgs);
	m_OptCeremony	.MoveWindow(rcDlgs);
	m_OptManagement	.MoveWindow(rcDlgs);

	m_bDlgCreated = true;	
		
	SelectTreeItem(m_nInitPage);

	m_crtTree.Expand(hTreeItem, TVE_EXPAND);
	//m_crtTree.Expand(hSelectedItemLogFilter, TVE_EXPAND);

	::SetWindowPos(this->GetSafeHwnd(), HWND_TOPMOST, 0, 0, 980, 671, SWP_NOMOVE ); 

	m_crtTree.SetWindowPos(NULL,0,0,229,671,NULL);
	m_crtTree.SetTextColor(RGB(255, 255, 255));
	m_crtTree.SetBkColor(RGB(44, 44, 44));

	/*m_btnOk.SetWindowPos(NULL, 807,32,66,26,NULL);
	m_btnCancel.SetWindowPos(NULL, 883,32,66,26,NULL);*/
	m_btnCancel.SetWindowPos(NULL, 807,32,66,26,NULL);
	m_btnOk.SetWindowPos(NULL, 883,32,66,26,NULL);
	m_btnOk.ChangeFont(15);
	m_btnCancel.ChangeFont(15);

	// force repaint immediately
	m_crtTree.Invalidate();

	UpdateData(FALSE);

	
	return TRUE;  
}

//------------------------------------------------------------------------------ 
//! @brief    Handles TVN_SELCHANGED t. 
//! @date      
//! @owner     
//! @note      
//! @return    
//! @revision  
//------------------------------------------------------------------------------ 
void CESMOptionDlg::OnTvnSelchangedPropTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	if(m_bDlgCreated)
	{
		HTREEITEM hCurrent = m_crtTree.GetSelectedItem();
		if(hCurrent)
			((CPropertyPage*)m_crtTree.GetItemData(hCurrent))->ShowWindow(SW_SHOW);
	}
	*pResult = 0;
}

//------------------------------------------------------------------------------ 
//! @brief     Handles TVN_SELCHANGING notification.
//! @date      
//! @owner     
//! @note      
//! @return    
//! @revision  
//------------------------------------------------------------------------------ 
void CESMOptionDlg::OnTvnSelchangingPropTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	if(m_bDlgCreated)
	{
		HTREEITEM hCurrent = m_crtTree.GetSelectedItem();
		if(hCurrent)
			((CPropertyPage*)m_crtTree.GetItemData(hCurrent))->ShowWindow(SW_HIDE);
	//	else
	//		(CPropertyPage*)m_OptPath.ShowWindow(SW_HIDE);
	}
	*pResult = 0;
}

//------------------------------------------------------------------------------ 
//! @brief     Applies the changes of configuration.
//! @date      2009-10-20
//! @owner     
//! @note      
//! @return    
//! @revision  2011-11-20 hongsu.jung
//!            2011-11-02 hongsu.jung
//!            2012-07-02 keunbae.song
//------------------------------------------------------------------------------
BOOL CESMOptionDlg::Apply()
{
	if(!m_OptPath.SaveProp(m_pOption->m_Path))
		return FALSE;

	if(!m_Opt4DMaker.SaveProp(m_pOption->m_4DOpt))
		return FALSE;
#ifndef _4DMODEL
	if(!m_OptAdjust.SaveProp(m_pOption->m_Adjust))
		return FALSE;
#endif
	if(!m_OptAdjustMovie.SaveProp(m_pOption->m_AdjustMovie))
		return FALSE;

	if(!m_OptLog.SaveProp(m_pOption->m_Log))
		return FALSE;

	if(!m_OptLogFilter.SaveProp(m_pOption->m_LogFilter))
		return FALSE;

	if(!m_OptCamInfo.SaveProp(m_pOption->m_CamInfo))	
		return FALSE;

	if(!m_OptPCInfo.SaveProp(m_pOption->m_PcInfo))
		return FALSE;

	if(!m_OptCeremony.SaveProp(m_pOption->m_Ceremony))
		return FALSE;

	if(!m_OptTemplate.SaveProp(m_pOption->m_TemplateOpt))
		return FALSE;

	if(!m_OptManagement.SaveProp(m_pOption->m_menagement))
		return FALSE;

	if(!m_OptManagement.SaveProp(m_pOption->m_BackupString))
		return FALSE;


	//Save configuration file.
	CString strConfig;
	strConfig = m_pOption->m_strConfig;	

	FileSave(strConfig);

	//Draw Cam Info
	/*ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_VIEW_DSC_RELOAD;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);*/

	
	if( ESMGetValue(ESM_VALUE_SERVERMODE) != ESM_SERVERMODE_MAKING )
	{
		//20140305 kcd UI Cam List Update
		int nServerMode = ESMGetValue(ESM_VALUE_NET_MODE);
		if(!nServerMode && m_OptCamInfo.GetIsModefy())
		{
			ESMEvent* pMsg = NULL;
			pMsg = new ESMEvent();
			pMsg->message = WM_ESM_VIEW_TIMELINE_RELOAD;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

			ESMEvent* pMsg2 = NULL;
			pMsg2 = new ESMEvent();
			pMsg2->message = WM_ESM_NET_ALLDISCONNET;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg2);
			
			//jhhan 16-11-22
			ESMEvent* pMsg3 = NULL;
			pMsg3 = new ESMEvent();
			pMsg3->message = WM_ESM_NET_ALLDISCONNET;
			pMsg3->nParam3 = ESM_NETWORK_4DP;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg3);


			m_OptCamInfo.SetIIsApply();
		}

		// Adjust Movie Path 전송
		{
			CString* pStrPath = NULL;
			ESMEvent* pMsg1 = NULL;
			pMsg1 = new ESMEvent();
			pMsg1->message = WM_ESM_NET_OPTION_ADJ_TARGET_PATH;
			pStrPath = new CString;

			pStrPath->Format(_T("%s"), m_pOption->m_AdjustMovie.strTarPath);
			pMsg1->pParam = (LPARAM)pStrPath;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg1);
		}

		// To Client Path 전송
		CString* pStrData = NULL;
		for( int i = 0; i< ESM_OPT_PATH_ALL; i++)
		{
			if( i == ESM_OPT_PATH_SERVERRAM)
				continue;

			ESMEvent* pMsg = NULL;
			pMsg = new ESMEvent();
			pMsg->message = WM_ESM_NET_OPTION_PATH;
			pMsg->nParam1 = i;
			pStrData = new CString;

			if( i == ESM_OPT_PATH_CLIENTRAM)
				pStrData->Format(_T("%s"), m_pOption->m_Path.strPath[ESM_OPT_PATH_SERVERRAM]);
			else
				pStrData->Format(_T("%s"), m_pOption->m_Path.strPath[i]);

			pMsg->pParam = (LPARAM)pStrData;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}

		// To Client Backup 전송
		for( int i = 0; i< ESM_BACKUP_STRING_ALL; i++)
		{
			ESMEvent* pMsg = NULL;
			pMsg = new ESMEvent();
			pMsg->message = WM_ESM_NET_BACKUP_STRING;
			pMsg->nParam1 = i;
			pStrData = new CString;

			pStrData->Format(_T("%s"), m_pOption->m_BackupString.strString[i]);

			pMsg->pParam = (LPARAM)pStrData;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}

		// To Client Base info 전송
		{
			ESMEvent* pMsg = NULL;
			pMsg = new ESMEvent();
			pMsg->message = WM_ESM_NET_OPTION_BASE;
			pMsg->nParam1 = m_pOption->m_4DOpt.nMovieSaveLocation;
			pMsg->nParam2 = m_pOption->m_4DOpt.nSensorOnTime;
			pMsg->nParam3 = m_pOption->m_4DOpt.nDeleteCnt;
			pStrData = new CString;
			pStrData->Format(_T("%d"), m_pOption->m_4DOpt.nWaitSaveDSC);
			
			pMsg->pParam = (LPARAM)pStrData;

			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}

		// ETC info 전송
		{
			ESMEvent* pMsg = NULL;
			pMsg = new ESMEvent();
			pMsg->message = WM_ESM_NET_OPTION_ETC;
			pMsg->nParam1 = m_pOption->m_4DOpt.nDSCSyncCnt;
			pMsg->nParam2 = m_pOption->m_4DOpt.nMovieSaveLocation;
			pMsg->nParam3 = m_pOption->m_4DOpt.bRemoteSkip;
						
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}

		// Sync Data 전송
		{
			ESMEvent* pMsg = NULL;
			pMsg = new ESMEvent();
			pMsg->message = WM_ESM_NET_OPTION_SYNC;
			pMsg->nParam1 = m_pOption->m_4DOpt.nPCSyncTime;
			pMsg->nParam2 = m_pOption->m_4DOpt.nDSCSyncTime;
			pMsg->nParam3 = m_pOption->m_4DOpt.nDSCSyncWaitTime;

			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
		}
	}
	if( ESMGetValue(ESM_VALUE_SERVERMODE) != ESM_SERVERMODE_RECORDING )
	{

		CString* pStrData = NULL;
		for( int i = 0; i< ESM_OPT_PATH_ALL; i++)
		{
			if( i == ESM_OPT_PATH_SERVERRAM)
			{
				ESMEvent* pMsg = NULL;
				pMsg = new ESMEvent();
				pMsg->message = WM_ESM_NET_OPTION_PATH;
				pMsg->nParam1 = i;
				pStrData = new CString;

				CString strServerIP, strTp;
				strTp.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
				strServerIP = strTp.Left(strTp.ReverseFind('\\'));

				pStrData->Format(_T("%s%s"), strServerIP, m_pOption->m_Path.strPath[ESM_OPT_PATH_CLIENTRAM]);
				pMsg->pParam = (LPARAM)pStrData;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
			}
		}
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_NET_OPTION_MOVIESIZE;
		pMsg->nParam1 = m_pOption->m_4DOpt.bEnableVMCC;
		pMsg->nParam2 = m_pOption->m_4DOpt.bUHDtoFHD;
		pMsg->nParam3 = m_pOption->m_4DOpt.bReverseMovie;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);

		ESMEvent* pMsg1 = NULL;
		pMsg1 = new ESMEvent();
		pMsg1->message = WM_ESM_NET_OPTION_MAKING_TYPE;
		pMsg1->nParam1 = m_pOption->m_4DOpt.bGPUMakeFile;
		pMsg1->nParam2 = m_pOption->m_4DOpt.bAJAOnOff;//Add by HJCHO 
		pMsg1->nParam3 = m_pOption->m_4DOpt.bEnableCopy;  //4DP Copy Option
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg1);

		ESMEvent* pMsg2 = NULL;
		pMsg2 = new ESMEvent();
		pMsg2->message = WM_ESM_NET_OPTION_LIGHT_WEIGHT;
		pMsg2->nParam1 = m_pOption->m_4DOpt.bLightWeight;
		pMsg2->nParam2 = m_pOption->m_4DOpt.n4DPMethod;//hjcho Added
		pMsg2->nParam3 = m_pOption->m_4DOpt.bRefereeRead;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg2);

		ESMEvent* pMsg3 = NULL;
		pMsg3 = new ESMEvent();
		pMsg3->message = WM_ESM_NET_OPTION_VIEW_INFO;
		pMsg3->nParam1 = m_pOption->m_4DOpt.bViewInfo;
		pMsg3->nParam2 = m_pOption->m_4DOpt.bHorizonAdj;
		//jhhan 180828
		pMsg3->nParam3 = m_pOption->m_4DOpt.nStoredTime;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg3);

		ESMEvent* pMsg4 = NULL;
		pMsg4 = new ESMEvent();
		pMsg4->message = WM_ESM_NET_OPTION_RECORD_FILE_SPLIT;
		pMsg4->nParam1 = m_pOption->m_4DOpt.bRecordFileSplit;		
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg4);

		//wgkim 180629
		ESMEvent* pMsg5 = NULL;
		pMsg5 = new ESMEvent();
		pMsg5->message = WM_ESM_NET_OPTION_SET_FRAME_RATE;
		pMsg5->nParam1 = m_pOption->m_4DOpt.nSetFrameRateIndex;
		pMsg5->nParam2 = m_pOption->m_4DOpt.bRTSP;//180927
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg5);

		//wgkim 181005
		ESMEvent* pMsg6 = NULL;
		pMsg6 = new ESMEvent();
		pMsg6->message = WM_ESM_NET_OPTION_AUTOADJUST_KZONE;
		pMsg6->nParam1 = m_pOption->m_4DOpt.bAutoAdjustKZone;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg6);
	}
#ifdef _4DMODEL
	//-- 2015-02-15 cygil@esmlab.com
	//-- Add picture coutdown info 전송
	ESMEvent* pCountdownMsg = NULL;
	pCountdownMsg = new ESMEvent();
	pCountdownMsg->message = WM_ESM_NET_OPTION_COUNTDOWN;
	pCountdownMsg->nParam1 = m_pOption->m_4DOpt.nCountdownLastIP;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pCountdownMsg);
#endif
	// Distance.csv, Zoom.csv Check 생성
	CFile file;
	CString strFilePath, WriteData;
	strFilePath.Format(_T("%s\\DSCDistance.csv"), ESMGetPath(ESM_PATH_SETUP));
	CESMFileOperation fo;
	CObArray pItemList;
	ESMGetDSCList(&pItemList);
	CDSCItem* pDSCItem = NULL;
	if(!fo.IsFileExist(strFilePath))
	{
		if(file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
		{
			for( int i = 0 ;i < pItemList.GetCount(); i++)
			{
				pDSCItem = (CDSCItem*)pItemList.GetAt(i);
				WriteData.Format(_T("%s, \r\n"), pDSCItem->GetDeviceDSCID());
				file.Write(WriteData, WriteData.GetLength()*2);
			}
			file.Close();
		}
	}
	strFilePath.Format(_T("%s\\DSCZoom.csv"), ESMGetPath(ESM_PATH_SETUP));
	if(!fo.IsFileExist(strFilePath))
	{
		if(file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
		{
			for( int i = 0 ;i < pItemList.GetCount(); i++)
			{
				pDSCItem = (CDSCItem*)pItemList.GetAt(i);
				WriteData.Format(_T("%s, \r\n"), pDSCItem->GetDeviceDSCID());
				file.Write(WriteData, WriteData.GetLength()*2);
			}
			file.Close();
		}
	}
	strFilePath.Format(_T("%s\\CaptureDelayTime.csv"), ESMGetPath(ESM_PATH_SETUP));
	if(!fo.IsFileExist(strFilePath))
	{
		if(file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
		{
			for( int i = 0 ;i < pItemList.GetCount(); i++)
			{
				pDSCItem = (CDSCItem*)pItemList.GetAt(i);
				WriteData.Format(_T("%s, \r\n"), pDSCItem->GetDeviceDSCID());
				file.Write(WriteData, WriteData.GetLength()*2);
			}
			file.Close();
		}
	}
	strFilePath.Format(_T("%s\\SensorOnDesignation.csv"), ESMGetPath(ESM_PATH_SETUP));
	if(!fo.IsFileExist(strFilePath))
	{
		if(file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
		{
			for( int i = 0 ;i < pItemList.GetCount(); i++)
			{
				pDSCItem = (CDSCItem*)pItemList.GetAt(i);
				WriteData.Format(_T("%s, \r\n"), pDSCItem->GetDeviceDSCID());
				file.Write(WriteData, WriteData.GetLength()*2);
			}
			file.Close();
		}
	}

	ESMInitDirection();

	ESMSendToLiveList();

	return TRUE;
}

void CESMOptionDlg::SetAdjPathData(CString strPath)
{
	m_pOption->m_AdjustMovie.strTarPath = strPath;
}
void CESMOptionDlg::SetPathData(int nIndex, CString strPath)
{
	m_pOption->m_Path.strPath[nIndex] = strPath;
}

void CESMOptionDlg::SetBackupString(int nIndex, CString strPath)
{
	m_pOption->m_BackupString.strString[nIndex] = strPath;
}

void CESMOptionDlg::SelectTreeItem(int nID)
{
	switch(nID)
	{
	case IDD_OPTION_PATH		:	m_crtTree.SelectItem(m_hItem[ENV_PATH_SET]);	break;
	case IDD_OPTION_4DMAKER		:	m_crtTree.SelectItem(m_hItem[ENV_4DM_OPT]);		break;
#ifndef _4DMODEL
	case IDD_OPTION_ADJUST		:	m_crtTree.SelectItem(m_hItem[ENV_ADJUST_OPT]);	break;
#endif
	case IDD_OPTION_ADJUST_MOVIE:	m_crtTree.SelectItem(m_hItem[ENV_ADJUST_MOV]);	break;
	case IDD_OPTION_LOG			:	m_crtTree.SelectItem(m_hItem[ENV_LOG_OPTION]);	break;
	case IDD_OPTION_LOG_FILTER	:	m_crtTree.SelectItem(m_hItem[ENV_LOG_FILTER]);	break;
	case IDD_OPTION_CAMINFO		:	m_crtTree.SelectItem(m_hItem[ENV_CAMERA_INFO]);	break;
	case IDD_OPTION_PCINFO		:	m_crtTree.SelectItem(m_hItem[ENV_PC_INFO]);		break;
	case IDD_OPTION_TEMPLATE	:	m_crtTree.SelectItem(m_hItem[ENV_TEMPLATE_OPT]);break;
	case IDD_OPTION_CEREMONY	:	m_crtTree.SelectItem(m_hItem[ENV_CEREMONY]);	break;
	case IDD_OPTION_MANAGEMANT	:	m_crtTree.SelectItem(m_hItem[ENV_MANAGEMENT]);	break;
	default						:	m_crtTree.SelectItem(m_hItem[ENV_PATH_SET]);	break;
	}
}

BOOL CESMOptionDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_F6)
			SelectTreeItem(IDD_OPTION_PATH);
		if(pMsg->wParam == VK_F7)
			SelectTreeItem(IDD_OPTION_4DMAKER);
		if(pMsg->wParam == VK_F8)
			SelectTreeItem(IDD_OPTION_CAMINFO);
		if(pMsg->wParam == VK_F9)
			SelectTreeItem(IDD_OPTION_PCINFO);
		if(pMsg->wParam == VK_F11)
			SelectTreeItem(IDD_OPTION_CEREMONY);
		/*if(pMsg->wParam == 'a' || pMsg->wParam == 'A')
			ESMVerifyInfo();*/
	}
	else if( pMsg->message == WM_SYSKEYDOWN )
	{    
		if(pMsg->wParam == VK_F10)
			SelectTreeItem(IDD_OPTION_TEMPLATE);
		return TRUE;
	}
	
	

	return CDialog::PreTranslateMessage(pMsg);
}

YUVMatrix CESMOptionDlg::SetRotMatrix(int nIdx)
{
	YUVMatrix matrix;

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CDSCItem * pItem = NULL;
	pItem = (CDSCItem * )arDSCList.GetAt(nIdx);

	cv::Mat m;
	double degree = pItem->GetDSCAdj(DSC_ADJ_A);
	double dbAngleAdjust = -1 * (degree + 90);

	double dbScale = pItem->GetDSCAdj(DSC_ADJ_SCALE);

	//Calc Y Data
	double dbRotX = (double)Round(pItem->GetDSCAdj(DSC_ADJ_RX));
	double dbRotY = (double)Round(pItem->GetDSCAdj(DSC_ADJ_RY));
	double dbMovX = (double)Round(pItem->GetDSCAdj(DSC_ADJ_X));
	double dbMovY = (double)Round(pItem->GetDSCAdj(DSC_ADJ_Y));

	int nMarginX = ESMGetMarginX();
	int nMarginY = ESMGetMarginY();
	int nWidth = pItem->GetWidth();
	int nHeight = pItem->GetHeight();

	matrix.matY = cv::getRotationMatrix2D(cv::Point2f(dbRotX,dbRotY),dbAngleAdjust,dbScale);
	if(dbRotX == 0 && dbRotY == 0)
	{
		matrix.matY.at<double>(0,2) = 0;
		matrix.matY.at<double>(1,2) = 0;		
	}
	else
	{
		matrix.matY.at<double>(0,2) += (dbMovX / dbScale);
		matrix.matY.at<double>(1,2) += (dbMovY / dbScale);
	}

	matrix.szOriYImage.width  = nWidth;
	matrix.szOriYImage.height = nHeight;

	double dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2)/ nWidth);

	matrix.szNewY.width  = nWidth * dbMarginScale;
	matrix.szNewY.height = nHeight * dbMarginScale;

	matrix.ptNewY.x = (int)((nWidth  * dbMarginScale - nWidth)/2);
	matrix.ptNewY.y = (int)((nHeight  * dbMarginScale - nHeight)/2);

	//Calc UV Data
	dbRotX = (double)Round(pItem->GetDSCAdj(DSC_ADJ_RX) * 0.5);
	dbRotY = (double)Round(pItem->GetDSCAdj(DSC_ADJ_RY) * 0.5);
	dbMovX = (double)Round(pItem->GetDSCAdj(DSC_ADJ_X) * 0.5);
	dbMovY = (double)Round(pItem->GetDSCAdj(DSC_ADJ_Y) * 0.5);
	
	nMarginX = ESMGetMarginX() * 0.5;
	nMarginY = ESMGetMarginY() * 0.5;
	nWidth = pItem->GetWidth() * 0.5;
	nHeight = pItem->GetHeight() * 0.5;

	matrix.matUV = cv::getRotationMatrix2D(cv::Point2f(dbRotX,dbRotY),dbAngleAdjust,dbScale);
	if(dbRotX == 0 && dbRotY == 0)
	{
		matrix.matY.at<double>(0,2) = 0;
		matrix.matY.at<double>(1,2) = 0;		
	}
	else
	{
		matrix.matY.at<double>(0,2) += (dbMovX / dbScale);
		matrix.matY.at<double>(1,2) += (dbMovY / dbScale);
	}

	matrix.szOriUVImage.width  = nWidth;
	matrix.szOriUVImage.height = nHeight;

	dbMarginScale = 1 / ((double)(nWidth - nMarginX * 2)/ nWidth);

	matrix.szNewUV.width  = nWidth * dbMarginScale;
	matrix.szNewUV.height = nHeight * dbMarginScale;

	matrix.ptNewUV.x = (int)((nWidth  * dbMarginScale - nWidth)/2);
	matrix.ptNewUV.y = (int)((nHeight  * dbMarginScale - nHeight)/2);

	return matrix;
}
int CESMOptionDlg::Round(double dData)
{
	int nResult = 0;

	if( dData > 0.0)
		nResult = int(dData + 0.5);
	else if( dData < 0.0)
		nResult = int(dData - 0.5);
	else
		nResult = 0;

	return nResult;
}
CString CESMOptionDlg::WriteMatValue(YUVMatrix matrix)
{
	CString strAdj;

	//Y Value
	for(int i = 0 ; i < matrix.matY.rows; i++)
	{
		for(int j = 0 ; j < matrix.matY.cols; j++)
		{
			CString str;
			str.Format(_T(", %f"),matrix.matY.at<double>(i,j));

			strAdj.Append(str);
		}
	}
	CString str;
	str.Format(_T(", %d, %d"),matrix.szNewY.width,matrix.szNewY.height);
	strAdj.Append(str);

	str.Format(_T(", %d, %d"),matrix.ptNewY.x,matrix.ptNewY.y);
	strAdj.Append(str);

	str.Format(_T(", %d, %d"),matrix.szOriYImage.width,matrix.szOriYImage.height);
	strAdj.Append(str);

	//UV value
	for(int i = 0 ; i < matrix.matUV.rows; i++)
	{
		for(int j = 0 ; j < matrix.matUV.cols; j++)
		{
			CString str;
			str.Format(_T(", %f"),matrix.matUV.at<double>(i,j));

			strAdj.Append(str);
		}
	}

	str.Format(_T(", %d, %d"),matrix.szNewUV.width,matrix.szNewUV.height);
	strAdj.Append(str);

	str.Format(_T(", %d, %d"),matrix.ptNewUV.x,matrix.ptNewUV.y);
	strAdj.Append(str);

	str.Format(_T(", %d, %d"),matrix.szOriUVImage.width,matrix.szOriUVImage.height);
	strAdj.Append(str);

	return strAdj;
}