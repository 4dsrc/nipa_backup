////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptMovie.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-10-05
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMOptBase.h"
#include "resource.h"

class CESMOptMovie : public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOptMovie)

public:
	CESMOptMovie();
	virtual ~CESMOptMovie();
	void InitProp(ESMAdjust& Opt);
	BOOL SaveProp(ESMAdjust& Opt);
	enum { IDD = IDD_OPTION_ADJUST_OLD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()

public:	
	int m_nThreshold;
	int m_nFlowmoTime;
	int m_nAutoBrightness;
	BOOL m_bClockwise;
	BOOL m_bClockreverse;
	BOOL m_bOutputMP4;
	BOOL m_bOutputTS;
	
	int m_nFirstPointY;
	int m_nPointX;
	int m_nXRange;
	int m_nFirstYRange;
	int m_nSecondYRange;
	int m_nThirdYRange;

	BOOL m_bRevisionUse;
	BOOL m_bRevReSizeImage;
	BOOL m_bInsertBanner;

	int m_nCamZoom;
	int m_nTargetLength;

	afx_msg void OnBnClickedAdjustOrder();
};
