XAccessible
	// ESMOptCamInfo.cpp : 구현 파일입니다.
	//

#include "stdafx.h"
#include "ESMOptCamInfo.h"
#include "ESMIni.h"
#include "ESMFunc.h"
#include "DSCItem.h"
#include "ESMOptCamDirection.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
	static char THIS_FILE[] = __FILE__;
#endif


// ESMOptCamInfo 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOptCamInfo, CESMOptBase)

	CESMOptCamInfo::CESMOptCamInfo()
	: CESMOptBase(CESMOptCamInfo::IDD)
	, m_strCamID (_T(""))
	, m_strCamName(_T(""))
	, m_strView(_T(""))
{
	m_bModify = FALSE;
	m_CamList.SetUseCamStatusColor(FALSE);
}

CESMOptCamInfo::~CESMOptCamInfo(){}

void CESMOptCamInfo::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_CAMINFO_CAMID	, m_strCamID);
	DDX_Text(pDX, IDC_CAMINFO_CAMIP, m_strCamIP);
	DDX_Control(pDX, IDC_CAMINFO_CAMLIST, m_CamList);
	DDX_Control(pDX, IDC_CAMINFO_NUM, m_edit_num);
	DDX_Text(pDX, IDC_CAMINFO_CAMNAME, m_strCamName);
	DDX_Control(pDX, IDC_COMBO_GROUP, m_cmbGroup);
	DDX_Control(pDX, IDC_CAMINFO_CAMID, m_ctrlCamInfoID);
	DDX_Control(pDX, IDC_CAMINFO_CAMIP, m_ctrlCamInfoIP);
	DDX_Control(pDX, IDC_CAMINFO_CAMNAME, m_ctrlCamInfoName);
	DDX_Control(pDX, IDC_CAMINFO_UP, m_btnUp);
	DDX_Control(pDX, IDC_CAMINFO_DOWN, m_btnDown);
	DDX_Control(pDX, IDC_BTN_INIT, m_btnInit);
	DDX_Control(pDX, IDC_CAMINFO_MOVE, m_btnMove);
	DDX_Control(pDX, IDC_CAMINFO_ADD, m_btnAdd);
	DDX_Control(pDX, IDC_CAMINFO_MODIFY, m_btnModify);
	DDX_Control(pDX, IDC_CAMINFO_DELETE, m_btnDelete);
	DDX_Control(pDX, IDC_CAMINFO_ADD_ALL, m_btnAddAll);
	DDX_Control(pDX, IDC_CAMINFO_REVERSE_ADD, m_btnReverseAll);
	DDX_Control(pDX, IDC_CAMINFO_DELALL, m_btnDelAll);
	DDX_Control(pDX, IDC_CAMINFO_REVERSE_DELETE, m_btnReverseDel);
	DDX_Control(pDX, IDC_CAMINFO_SYNC_DELETE, m_btnSyncDelete);
	DDX_Control(pDX, IDC_CAMINFO_SYNC_ADD, m_btnSyncAdd);
	DDX_Control(pDX, IDC_CAMINFO_MAKE_DELETE, m_btnMakeDelete);
	DDX_Control(pDX, IDC_CAMINFO_MAKE_ADD, m_btnMakeAdd);
	DDX_Control(pDX, IDC_BTN_DIRECTION, m_btnEditDirection);
	DDX_Control(pDX, IDC_CAMINFO_VERTICAL_SET, m_btnVerticalSet);
	DDX_Control(pDX, IDC_CAMINFO_VERTICAL_OFF, m_btnVerticalOff);
	DDX_Control(pDX, IDC_EDIT_VIEW, m_editView);
	DDX_Control(pDX, IDC_BTN_VIEW_SET, m_btnViewSet);
	DDX_Text(pDX, IDC_EDIT_VIEW, m_strView);
	DDX_Control(pDX, IDC_COMBO_VIEW, m_cmbView);
	DDX_Control(pDX, IDC_CAMINFO_DETECT_ON, m_btnDetectOn);
	DDX_Control(pDX, IDC_CAMINFO_DETECT_OFF, m_btnDetectOff);
}

BEGIN_MESSAGE_MAP(CESMOptCamInfo, CESMOptBase)
	ON_BN_CLICKED(IDC_CAMINFO_ADD, &CESMOptCamInfo::OnBnClickedAdd)
	ON_BN_CLICKED(IDC_CAMINFO_DELETE, &CESMOptCamInfo::OnBnClickedDelete)
	ON_BN_CLICKED(IDC_CAMINFO_ADD_ALL, &CESMOptCamInfo::OnBnClickedAddAll)
	ON_BN_CLICKED(IDC_CAMINFO_UP, &CESMOptCamInfo::OnBnClickedUp)
	ON_BN_CLICKED(IDC_CAMINFO_DOWN, &CESMOptCamInfo::OnBnClickedDown)
	ON_BN_CLICKED(IDC_CAMINFO_DELALL, &CESMOptCamInfo::OnBnClickedDelAll)
	ON_NOTIFY(HDN_ITEMCLICK, 0, &CESMOptCamInfo::SortCamInfo)
	ON_BN_CLICKED(IDC_CAMINFO_MOVE, &CESMOptCamInfo::OnBnClickedCaminfoMove)
	ON_BN_CLICKED(IDC_CAMINFO_SYNC_DELETE, &CESMOptCamInfo::OnBnClickedCaminfoSyncDelete)
	ON_BN_CLICKED(IDC_CAMINFO_SYNC_ADD, &CESMOptCamInfo::OnBnClickedCaminfoSyncAdd)
	ON_BN_CLICKED(IDC_CAMINFO_MAKE_DELETE, &CESMOptCamInfo::OnBnClickedCaminfoMakeDelete)
	ON_BN_CLICKED(IDC_CAMINFO_MAKE_ADD, &CESMOptCamInfo::OnBnClickedCaminfoMakeAdd)
	ON_NOTIFY(NM_CLICK, IDC_CAMINFO_CAMLIST, &CESMOptCamInfo::OnNMClickCaminfoCamlist)
	ON_BN_CLICKED(IDC_CAMINFO_MODIFY, &CESMOptCamInfo::OnBnClickedCaminfoModify)
	ON_BN_CLICKED(IDC_CAMINFO_REVERSE_ADD, &CESMOptCamInfo::OnBnClickedCaminfoReverseAdd)
	ON_BN_CLICKED(IDC_CAMINFO_REVERSE_DELETE, &CESMOptCamInfo::OnBnClickedCaminfoReverseDelete)
	ON_CBN_SELENDOK(IDC_COMBO_GROUP, &CESMOptCamInfo::OnCbnSelendokComboGroup)
	ON_BN_CLICKED(IDC_BTN_DIRECTION, &CESMOptCamInfo::OnBnClickedBtnDirection)
	ON_BN_CLICKED(IDC_CAMINFO_VERTICAL_SET, &CESMOptCamInfo::OnBnClickedCaminfoVerticalSet)
	ON_BN_CLICKED(IDC_CAMINFO_VERTICAL_OFF, &CESMOptCamInfo::OnBnClickedCaminfoVerticalOff)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_INIT, &CESMOptCamInfo::OnBnClickedBtnInit)
	ON_BN_CLICKED(IDC_BTN_VIEW_SET, &CESMOptCamInfo::OnBnClickedBtnViewSet)
	ON_BN_CLICKED(IDC_CAMINFO_DETECT_ON, &CESMOptCamInfo::OnBnClickedCaminfoDetectOn)
	ON_BN_CLICKED(IDC_CAMINFO_DETECT_OFF, &CESMOptCamInfo::OnBnClickedCaminfoDetectOff)
END_MESSAGE_MAP()


// ESMOptCamInfo 메시지 처리기입니다.

void CESMOptCamInfo::OnBnClickedAdd()
{
	UpdateData();
	CString strNum;
	int nRowCount = m_CamList.GetItemCount();
	strNum.Format(_T("%d"), nRowCount+1);
	m_CamList.InsertItem(nRowCount, strNum);
	m_CamList.SetItemText(nRowCount, 1, m_strCamIP);
	m_CamList.SetItemText(nRowCount, 2, m_strCamID);
	m_CamList.SetItemText(nRowCount, 5, m_strCamName);
	m_bModify = TRUE;
}

BOOL CESMOptCamInfo::SaveProp(ESMCamInfo& path)
{
	if(!UpdateData(TRUE))
		return FALSE;

	path.nCamCount = m_CamList.GetItemCount();
	vector<int> nArrRTSPDeleteIndex;

	CString strTp;
	for(int i=0; i<path.nCamCount; i++)
	{
		strTp =  m_CamList.GetItemText(i, 2);
		path.strCamIP[i] = m_CamList.GetItemText(i, 1);
		path.strCamID[i] = m_CamList.GetItemText(i, 2);
		if(m_CamList.GetItemText(i, 3) == "")
		{
			path.usedFlag[i] = TRUE;
		}
		else
		{
			path.usedFlag[i] = FALSE;
			nArrRTSPDeleteIndex.push_back(i+1);
		}

		//jhhan 16-10-10
		if(m_CamList.GetItemText(i, 4) == "")
		{
			path.makeFlag[i] = TRUE;
		}
		else
		{
			path.makeFlag[i] = FALSE;
		}

		path.strCamName[i] = m_CamList.GetItemText(i, 5);

		//hjcho 180312
		if(m_CamList.GetItemText(i,6) == "")
		{
			path.reverse[i] = FALSE;
		}else
		{
			path.reverse[i] = TRUE;
		}

		//jhhan 180508 Group
		path.strGroup[i] = m_CamList.GetItemText(i, 7);

		//180512 wgkim Vertical
		if(m_CamList.GetItemText(i, 8) == "NORMAL")
			path.nArrVertical[i] = 0;
		else if(m_CamList.GetItemText(i, 8) == "-90 Degree")
			path.nArrVertical[i] = -1;
		else if(m_CamList.GetItemText(i, 8) == "90 Degree")
			path.nArrVertical[i] = 1;
		else
			path.nArrVertical[i] = 0;

		if(m_CamList.GetItemText(i, 9) == "")
		{
			path.detectFlag[i] = FALSE;
		}else
		{
			path.detectFlag[i] = TRUE;
		}

	}
	CString _strView;
	//path.strView = m_strView;
	for(int i=0; i<m_cmbView.GetCount(); i++)
	{
		CString strView;
		m_cmbView.GetLBText(i, strView);
		if(strView.IsEmpty() != TRUE)
		{
			CString strTemp = strView.Mid(1,3);
			int nNum = _ttoi(strTemp);
			if(nNum > 0)
			{
				strTemp.Format(_T("%d,"), nNum);
				_strView.Append(strTemp);
			}
		}
	}
	path.strView = _strView;
	ESMSetViewJump(path.strView);

	if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
	{
		if(nArrRTSPDeleteIndex.size())
		{
			for(int i = 0 ; i < nArrRTSPDeleteIndex.size() ; i++)
			{
				int nIdx = nArrRTSPDeleteIndex.at(i);
				ESMLog(5,_T("[RTSP] %d index excepted.."),nIdx);
				ESMSendRTSPHttpSync(nIdx);
			}
		}
	}
	return TRUE;
}

void CESMOptCamInfo::InitProp(ESMCamInfo& path)
{
	/*	for(int i=0; i < path.nCamCount; i++)
	{
	CString strNum;
	strNum.Format(_T("%d"),i+1);
	m_CamList.InsertItem(i, strNum);
	m_CamList.SetItemText(i, 1, path.strCamIP[i]);
	m_CamList.SetItemText(i, 2, path.strCamID[i]);
	} */

	GetDlgItem(IDC_STATIC_CAMERA_LIST)->		SetWindowPos(NULL,0,0,200,24,NULL);
	GetDlgItem(IDC_CAMINFO_CAMLIST)->			SetWindowPos(NULL,0,25,306,315,NULL);
	GetDlgItem(IDC_CAMINFO_UP)->				SetWindowPos(NULL,0,351,65,34,NULL);
	GetDlgItem(IDC_CAMINFO_DOWN)->				SetWindowPos(NULL,80,351,65,34,NULL);
	GetDlgItem(IDC_COMBO_GROUP)->				SetWindowPos(NULL,160,356,65,24,NULL);
	GetDlgItem(IDC_BTN_INIT)->					SetWindowPos(NULL,240,351,65,34,NULL);

	//VIEW 181014 jhhan
	GetDlgItem(IDC_STATIC_VIEW)->				SetWindowPos(NULL,0,395,47,24,NULL);
	//GetDlgItem(IDC_EDIT_VIEW)->					SetWindowPos(NULL,57,395,168,24,NULL);
	GetDlgItem(IDC_COMBO_VIEW)->				SetWindowPos(NULL,57,395,168,24,NULL);
	GetDlgItem(IDC_BTN_VIEW_SET)->				SetWindowPos(NULL,240,395,65,24,NULL);

	GetDlgItem(IDC_STATIC_NUM)->				SetWindowPos(NULL,326,25,47,24,NULL);
	GetDlgItem(IDC_CAMINFO_NUM)->				SetWindowPos(NULL,379,25,73,24,NULL);
	GetDlgItem(IDC_CAMINFO_MOVE)->				SetWindowPos(NULL,457,25,57,24,NULL);

	GetDlgItem(IDC_STATIC_ID)->					SetWindowPos(NULL,326,69,47,24,NULL);
	GetDlgItem(IDC_CAMINFO_CAMID)->				SetWindowPos(NULL,379,69,135,24,NULL);
	GetDlgItem(IDC_STATIC_IP)->					SetWindowPos(NULL,326,101,47,24,NULL);
	GetDlgItem(IDC_CAMINFO_CAMIP)->				SetWindowPos(NULL,379,101,135,24,NULL);
	GetDlgItem(IDC_STATIC_NAME)->				SetWindowPos(NULL,326,133,47,24,NULL);
	GetDlgItem(IDC_CAMINFO_CAMNAME)->			SetWindowPos(NULL,379,133,135,24,NULL);

	GetDlgItem(IDC_CAMINFO_ADD)->				SetWindowPos(NULL,326,171,60,34,NULL);
	GetDlgItem(IDC_CAMINFO_MODIFY)->			SetWindowPos(NULL,390,171,60,34,NULL);
	GetDlgItem(IDC_CAMINFO_DELETE)->			SetWindowPos(NULL,454,171,60,34,NULL);

	GetDlgItem(IDC_CAMINFO_ADD_ALL)->			SetWindowPos(NULL,326,209,92,34,NULL);
	GetDlgItem(IDC_CAMINFO_REVERSE_ADD)->		SetWindowPos(NULL,422,209,92,34,NULL);

	GetDlgItem(IDC_CAMINFO_DELALL)->			SetWindowPos(NULL,326,247,92,34,NULL);
	GetDlgItem(IDC_CAMINFO_REVERSE_DELETE)->	SetWindowPos(NULL,422,247,92,34,NULL);

	GetDlgItem(IDC_CAMINFO_SYNC_DELETE)->		SetWindowPos(NULL,326,312,92,34,NULL);
	GetDlgItem(IDC_CAMINFO_SYNC_ADD)->			SetWindowPos(NULL,422,312,92,34,NULL);

	GetDlgItem(IDC_CAMINFO_MAKE_DELETE)->		SetWindowPos(NULL,326,350,92,34,NULL);
	GetDlgItem(IDC_CAMINFO_MAKE_ADD)->			SetWindowPos(NULL,422,350,92,34,NULL);

	GetDlgItem(IDC_BTN_DIRECTION)->				SetWindowPos(NULL,326,415,188,34,NULL);
	GetDlgItem(IDC_CAMINFO_VERTICAL_SET)->		SetWindowPos(NULL,326,453,92,34,NULL);
	GetDlgItem(IDC_CAMINFO_VERTICAL_OFF)->		SetWindowPos(NULL,422,453,92,34,NULL);

	GetDlgItem(IDC_CAMINFO_DETECT_ON)->			SetWindowPos(NULL,117,453,92,34,NULL);
	GetDlgItem(IDC_CAMINFO_DETECT_OFF)->		SetWindowPos(NULL,213,453,92,34,NULL);

	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\CameraList.csv"), ESMGetPath(ESM_PATH_SETUP));

	if(!ReadFile.Open(strInputData, CFile::modeRead))
		return;

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if(iLength == 0)
		return;

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0, i = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;

	CString strNum, strDSCIP, strDSCID,strUsedFlag, strMakeFlag, strName, strReverse, strGroup, strVertical, strDetect;

	while(1)
	{
		i++;
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;

		AfxExtractSubString(strDSCID, sLine, 0, ',');
		AfxExtractSubString(strDSCIP, sLine, 1, ',');
		AfxExtractSubString(strUsedFlag, sLine, 2, ',');
		AfxExtractSubString(strMakeFlag, sLine, 3, ',');
		AfxExtractSubString(strName, sLine, 4, ',');
		AfxExtractSubString(strReverse, sLine, 5, ',');

		AfxExtractSubString(strGroup, sLine, 6, ',');
		AfxExtractSubString(strVertical, sLine, 7, ',');

		AfxExtractSubString(strDetect, sLine, 8, ',');

		strDSCID.Trim();
		strDSCIP.Trim();
		strUsedFlag.Trim();
		strMakeFlag.Trim();
		strName.Trim();
		strReverse.Trim();

		strGroup.Trim();
		strVertical.Trim();

		strDetect.Trim();

		if(strDSCID != _T(""))
		{
			strNum.Format(_T("%d"),i);
			strDSCIP.Replace( _T(" "), NULL );
			strDSCID.Replace( _T(" "), NULL );
			m_CamList.InsertItem(i-1, strNum);
			m_CamList.SetItemText(i-1, 1, strDSCIP);
			m_CamList.SetItemText(i-1, 2, strDSCID);

			if(strUsedFlag =="TRUE")
			{
				m_CamList.SetItemText(i-1, 3, _T(""));
			}
			else
			{
				m_CamList.SetItemText(i-1, 3, _T("X"));
			}

			if(strMakeFlag =="TRUE")
			{
				m_CamList.SetItemText(i-1, 4, _T(""));
			}
			else
			{
				m_CamList.SetItemText(i-1, 4, _T("X"));
			}

			m_CamList.SetItemText(i-1, 5, strName);

			//180312 hjcho
			if(strReverse == "REVERSE")
				m_CamList.SetItemText(i-1,6,_T("O"));
			else
				m_CamList.SetItemText(i-1,6,_T(""));

			//jhhan 180508 Group
			m_CamList.SetItemText(i-1, 7, strGroup);

			//180512 wgkim
			if(strVertical == "NORMAL")
				m_CamList.SetItemText(i-1,8,_T("NORMAL"));
			else if(strVertical == "-90 Degree")
				m_CamList.SetItemText(i-1,8,_T("-90 Degree"));
			else if(strVertical == "90 Degree")
				m_CamList.SetItemText(i-1,8,_T("90 Degree"));
			else
				m_CamList.SetItemText(i-1,8,_T("NORMAL"));

			if(strDetect == "TRUE")
				m_CamList.SetItemText(i-1,9,_T("O"));
			else
				m_CamList.SetItemText(i-1,9,_T(""));
		}
		else
			break;
	}

	//jhhan 181004 VIEW
	//m_strView = path.strView;
	CString strView = path.strView;
	BOOL bToken = TRUE;
	int nToken = 0;
	while(bToken)
	{
		CString strValue;
		bToken = AfxExtractSubString(strValue, strView, nToken++, ',');
		if(strValue.IsEmpty() != TRUE)
		{
			CString _str;
			_str.Format(_T("[%03d]"), _ttoi(strValue));
			m_cmbView.AddString(_str);
		}
	}
	


	/*	wchar_t chThisPath[_MAX_PATH];
	GetCurrentDirectory( _MAX_PATH, chThisPath);
	UpdateData(TRUE);
	CString strThisPath ;

	strThisPath.Format(_T("%s\\%s.xlsx"), ESMGetPath(ESM_PATH_SETUP), _T("4DMaker"));
	GetModuleFileName( NULL, chThisPath, _MAX_PATH);
	CXLEzAutomation XL(FALSE); 
	XL.OpenExcelFile(strThisPath);

	int i = 0;

	CString strTemp;
	CString strDSCID;
	CString strDSCIP;
	CString strNum;

	while(1)
	{
	i++;
	strTemp = XL.GetCellValue(1,i);
	strDSCID = strTemp.Left(5);

	if(strDSCID != _T(""))
	{
	strDSCIP	=	XL.GetCellValue(2,i);
	strNum.Format(_T("%d"),i);
	strDSCID = strDSCID.Left(strDSCID.Find(_T(".")));
	m_CamList.InsertItem(i-1, strNum);
	m_CamList.SetItemText(i-1, 1, strDSCIP);
	m_CamList.SetItemText(i-1, 2, strDSCID);
	}
	else
	{
	break;
	}
	}

	XL.ReleaseExcel(); */

	


	UpdateData(FALSE);
}

BOOL CESMOptCamInfo::OnInitDialog()
{
	CESMOptBase::OnInitDialog();

	InitData();

	CStringArray strGroup;
	
	if(CameraGroupLoad(strGroup) == TRUE)
	{
		for(int i = 0; i < strGroup.GetCount(); i++)
		{
			m_cmbGroup.AddString(strGroup.GetAt(i));
		}
	}
	m_btnUp.ChangeFont(15);
	m_btnDown.ChangeFont(15);
	m_btnInit.ChangeFont(15);
	m_btnMove.ChangeFont(15);
	m_btnAdd.ChangeFont(15);
	m_btnModify.ChangeFont(15);
	m_btnDelete.ChangeFont(15);
	m_btnAddAll.ChangeFont(15);
	m_btnReverseAll.ChangeFont(15);
	m_btnDelAll.ChangeFont(15);
	m_btnReverseDel.ChangeFont(15);
	m_btnSyncDelete.ChangeFont(15);
	m_btnSyncAdd.ChangeFont(15);
	m_btnMakeDelete.ChangeFont(15);
	m_btnMakeAdd.ChangeFont(15);
	m_btnEditDirection.ChangeFont(15);
	m_btnVerticalSet.ChangeFont(15);
	m_btnVerticalOff.ChangeFont(15);

	m_btnViewSet.ChangeFont(15);

	m_btnDetectOn.ChangeFont(15);
	m_btnDetectOff.ChangeFont(15);

	return TRUE;
}

BOOL CESMOptCamInfo::InitData()
{
	// 이곳은 Dialog가 초기 화해야는 모든 데이터를 여기서한다.
	m_CamList.InsertColumn(0, _T("NUM"), LVCFMT_LEFT, 40);
	m_CamList.InsertColumn(1, _T("IP"), LVCFMT_LEFT, 100);
	m_CamList.InsertColumn(2, _T("ID"), LVCFMT_LEFT, 65);
	m_CamList.InsertColumn(3, _T("USED"), LVCFMT_LEFT, 50);
	
	//jhhan 16-10-10 /*Making Delete*/
	m_CamList.InsertColumn(4, _T("MAKE"), LVCFMT_LEFT, 50);
	m_CamList.InsertColumn(5, _T("NAME"), LVCFMT_LEFT, 80);
	m_CamList.InsertColumn(6, _T("REVERSE"),LVCFMT_CENTER,50);

	m_CamList.InsertColumn(7, _T("GROUP"), LVCFMT_LEFT, 50);

	m_CamList.InsertColumn(8, _T("VERTICAL"),LVCFMT_CENTER,150);

	m_CamList.InsertColumn(9, _T("DETECT"), LVCFMT_LEFT, 50);

	m_CamList.ModifyStyle(LVS_TYPEMASK, LVS_REPORT);
	m_CamList.SetExtendedStyle( LVS_EX_FULLROWSELECT );
	return TRUE;

}

void CESMOptCamInfo::OnBnClickedDelete()
{
	UpdateData();

	POSITION pos = NULL; 
	int nItemIdx  = 0;
	pos  = m_CamList.GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		nItemIdx = m_CamList.GetNextSelectedItem( pos );
	}
	m_CamList.DeleteItem(nItemIdx);
	m_bModify = TRUE;

}

void CESMOptCamInfo::OnNMDblclkStatusLst(NMHDR *pNMHDR, LRESULT *pResult)
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	int itemindex = pNMListView->iItem;
	CString idx;
	idx.Format(_T("%d"), itemindex);
	*pResult = 0;
	m_bModify = TRUE;
}

void CESMOptCamInfo::OnBnClickedAddAll()
{
	m_CamList.DeleteAllItems();
	CObArray pItemList;// = new CObArray;

	ESMGetDSCList(&pItemList);

	int nAll = pItemList.GetCount();
	CDSCItem* pExist = NULL;
	CString strNum;
	CString strID;
	int nRowCount = 0;
	CString strName; //jhhan 16-11-01 ex) IP:192.168.0.xxx / CamID:12345 = xxx_12345 

	while (nAll--)
	{
		pExist = (CDSCItem*)pItemList.GetAt(nRowCount);
		strID = pExist->GetDeviceDSCID();
		strNum.Format(_T("%d"), nRowCount+1);
		CString strTemp = pExist->GetDSCInfo(DSC_INFO_LOCATION);
		CString strIp;
		AfxExtractSubString(strIp, strTemp, 3, '.');
		strIp.Trim();
		strName.Format(_T("%s_%s"), strIp, strID);

		m_CamList.InsertItem(nRowCount, strNum);
		m_CamList.SetItemText(nRowCount, 1, pExist->GetDSCInfo(DSC_INFO_LOCATION));
		m_CamList.SetItemText(nRowCount, 2, strID);
		m_CamList.SetItemText(nRowCount, 5, strName);
		nRowCount++;
		/*pExist = (CDSCItem*)pItemList.GetAt(nAll);
		m_strCamID = pExist->GetDeviceDSCID();
		OnBnClickedAdd();*/
	}
	UpdateData(FALSE);
}


void CESMOptCamInfo::OnBnClickedUp()
{
	UpdateData();

	CListCtrl *pList = &m_CamList;

	POSITION pos = pList->GetFirstSelectedItemPosition();
	if(pos == NULL)
	{
		return;
	}

	int PreIndex=-1;
	int Index;

	while(pos)
	{
		Index = pList->GetNextSelectedItem(pos);

		if( PreIndex+1 != Index)
		{
			MoveTo(pList, Index, Index-1);
			pList->SetItemState(Index-1, LVIS_SELECTED, LVIS_SELECTED);
			PreIndex = Index-1;			
		}
		else 
			PreIndex = Index;				
	}
	m_CamList.SetFocus();
	m_bModify = TRUE;



	/*CString strID;
	CString strDownID;
	CString strIP;
	CString strDownIP;

	POSITION pos = NULL;
	int nItemIdx  = 0;
	int nDownIdx = 0;
	pos  = m_CamList.GetFirstSelectedItemPosition();
	int uSelectedCount = m_CamList.GetSelectedCount();
	int cnt;
	cnt = 0;

	if( NULL != pos )
	{
	nItemIdx = m_CamList.GetNextSelectedItem( pos );
	nDownIdx = nItemIdx - 1 ;
	}

	if(nItemIdx == 0)
	{
	m_CamList.SetFocus();
	return;
	}
	else
	{
	strID = m_CamList.GetItemText(nItemIdx, 2);
	strDownID = m_CamList.GetItemText(nDownIdx, 2);
	strIP = m_CamList.GetItemText(nItemIdx, 1);
	strDownIP = m_CamList.GetItemText(nDownIdx, 1);

	m_CamList.SetItemText(nDownIdx, 2, strID);
	m_CamList.SetItemText(nItemIdx, 2, strDownID);
	m_CamList.SetItemText(nDownIdx, 1, strIP);
	m_CamList.SetItemText(nItemIdx, 1, strDownIP);

	m_CamList.SetItemState(nDownIdx, LVIS_SELECTED,  LVIS_SELECTED);
	m_CamList.SetItemState(nItemIdx, 0,  LVIS_SELECTED);
	m_CamList.EnsureVisible(nDownIdx, TRUE);	
	m_CamList.SetFocus();
	}
	*/
}
void CESMOptCamInfo::MoveTo(CListCtrl* pList, int index1, int index2) 
{                                                     
	if(index1==index2) return;

	int idx1 = index1;
	int idx2 = index2;

	if(index1 > index2)
	{
		idx1 = index2;
		idx2 = index1;
	}


	int nCount = pList->GetHeaderCtrl()->GetItemCount();
	CString str;


	str = pList->GetItemText(idx1,0);
	pList->InsertItem(idx2+1, str);

	for(int k=1; k<nCount; k++)
	{
		str = pList->GetItemText(idx1,k);
		pList->SetItemText(idx2+1, k,str);
	}

	pList->DeleteItem(idx1);


	int itemscount = pList->GetItemCount();

	CString strNum;
		
	for(int i = 0; i <itemscount;i++)
	{
		strNum.Format(_T("%d"), i+1);
		pList->SetItemText(i,0,strNum);
	}




}


void CESMOptCamInfo::OnBnClickedDown()
{
	UpdateData();

	CListCtrl *pList = &m_CamList;


	POSITION pos = pList->GetFirstSelectedItemPosition();
	if(pos == NULL)
	{
		return;
	}

	vector<int> IndexData;

	while(pos)
	{
		int Index = pList->GetNextSelectedItem(pos);
		IndexData.push_back(Index);	
	}

	int size = IndexData.size()-1;

	int PreIndex=pList->GetItemCount();
	int Index;

	for( ;0<=size ; --size)
	{
		Index = IndexData[size];
		if(Index+1 != PreIndex)
		{
			MoveTo(pList, Index, Index+1);
			pList->SetItemState(Index+1, LVIS_SELECTED, LVIS_SELECTED);
			PreIndex = Index+1;
		}		
		else
			PreIndex = Index;		
	}

	m_CamList.SetFocus();
	m_bModify = TRUE;



	/*CString strID;
	CString strUpID;
	CString strIP;
	CString strUpIP;

	POSITION pos = NULL;
	POSITION nextpos= NULL;
	int nItemIdx  = 0;
	int nUpIdx = 0;
	pos  = m_CamList.GetFirstSelectedItemPosition();


	int uSelectedCount = m_CamList.GetSelectedCount();	
	int cnt;
	cnt = 0;
	int index = 0;
	CString str;
	int nextindex;



	if( NULL != pos )
	{
	nItemIdx = m_CamList.GetNextSelectedItem( pos );		
	nUpIdx = nItemIdx + 1 ;

	}
	index = nItemIdx;


	int nRowCount = m_CamList.GetItemCount();

	if(nItemIdx == nRowCount-1)
	{
	m_CamList.SetFocus();
	return;
	}
	else
	{
	strID = m_CamList.GetItemText(nItemIdx, 2);
	strUpID = m_CamList.GetItemText(nUpIdx, 2);
	strIP = m_CamList.GetItemText(nItemIdx, 1);
	strUpIP = m_CamList.GetItemText(nUpIdx, 1);

	m_CamList.SetItemText(nUpIdx, 2, strID);
	m_CamList.SetItemText(nItemIdx, 2, strUpID);
	m_CamList.SetItemText(nUpIdx, 1, strIP);
	m_CamList.SetItemText(nItemIdx, 1, strUpIP);

	m_CamList.SetItemState(nUpIdx, LVIS_SELECTED,  LVIS_SELECTED);
	m_CamList.SetItemState(nItemIdx, 0,  LVIS_SELECTED);
	m_CamList.EnsureVisible(nUpIdx, TRUE); 
	m_CamList.SetFocus();
	}	

	m_bModify = TRUE;
	*/
	
}


void CESMOptCamInfo::OnBnClickedDelAll()
{
	UpdateData();

	m_CamList.DeleteAllItems();
}

#define STRIDE_FACTOR 3
void CESMOptCamInfo::SortCamInfo(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	int nSelectedCol = pNMLV->iItem;
	CString strNum;

	for(int i=0; i<m_CamList.GetItemCount(); i++)
	{
		m_CamList.SetItemData(i, i);
	}

	m_bAscend = !m_bAscend;

	SORT_PARAMS sort_params;
	sort_params.hWnd = m_CamList.GetSafeHwnd();
	sort_params.nCol = nSelectedCol;
	sort_params.bAscend = m_bAscend;

	m_CamList.SortItems(&CESMOptCamInfo::SortFunc, (LPARAM) &sort_params);

	*pResult = 0;

	for(int i=0; i<m_CamList.GetItemCount(); i++)
	{
		strNum.Format(_T("%d"), i+1);
		m_CamList.SetItemText(i, 0, strNum);
	}
}

int CALLBACK CESMOptCamInfo::SortFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	SORT_PARAMS *pSortParams = (SORT_PARAMS *)lParamSort;

	CListCtrl *pListCtrl = (CListCtrl *)CWnd::FromHandle(pSortParams->hWnd);

	BOOL bAscend = pSortParams->bAscend;
	int nCol = pSortParams->nCol;

	CString strItem1 = pListCtrl->GetItemText((int)lParam1, nCol);
	CString strItem2 = pListCtrl->GetItemText((int)lParam2, nCol);

	strItem1.MakeLower();
	strItem2.MakeLower();

	if(bAscend)
		return strItem1.Compare(strItem2);
	else
		return strItem2.Compare(strItem1);
}

void CESMOptCamInfo::OnBnClickedCaminfoMove()
{	
	vector<int> IndexData;
	CListCtrl *pList = &m_CamList;

	POSITION ppos = pList->GetFirstSelectedItemPosition();

	int selectindex;
	CString selindex;

	m_edit_num.GetWindowTextW(selindex);

	LVFINDINFO fo;

	fo.flags = LVFI_STRING;
	fo.psz = selindex;

	fo.vkDirection = VK_DOWN;
	int idx = ListView_FindItem(pList->m_hWnd,-1,&fo);



	int cnt;
	cnt = 0;
	int startindex;
	startindex =  pList->GetNextSelectedItem(ppos);


	if(startindex < idx)
	{
		for(int i = startindex ; i< idx ; i++)
		{
			POSITION pos = pList->GetFirstSelectedItemPosition();

			IndexData.clear();
			while(pos)
			{
				int Index = pList->GetNextSelectedItem(pos)+cnt;
				IndexData.push_back(Index);	
			}
			int size = IndexData.size()-1;

			int PreIndex=pList->GetItemCount();
			int Index;

			for( ;0<=size ; --size)
			{
				Index = IndexData[size];
				if(Index+1 != PreIndex)
				{
					MoveTo(pList, Index, Index+1);
					pList->SetItemState(Index+1, LVIS_SELECTED, LVIS_SELECTED);
					PreIndex = Index+1;
				}		
				else
					PreIndex = Index;		


				m_CamList.SetFocus();
				m_bModify = TRUE;
			}

		}
	}
	else
	{

		for(int i = idx ; i < startindex ; i++)
		{
			POSITION pos = pList->GetFirstSelectedItemPosition();


			int PreIndex=-1;
			int Index;

			IndexData.clear();
			while(pos)
			{
				Index = pList->GetNextSelectedItem(pos);

				if( PreIndex+1 != Index)
				{
					MoveTo(pList, Index, Index-1);
					pList->SetItemState(Index-1, LVIS_SELECTED, LVIS_SELECTED);
					PreIndex = Index-1;			
				}
				else 
					PreIndex = Index;				
			}
			m_CamList.SetFocus();
			m_bModify = TRUE;

		}
	}


}


void CESMOptCamInfo::OnBnClickedCaminfoSyncDelete()
{
	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		while(pos)	//jhhan 16-11-01 복수선택
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );
			pList->SetItemText(nItemIdx, 3, _T("X"));
			//m_pcList.DeleteItem(nItemIdx);
			//jhhan 16-10-10
			pList->SetItemText(nItemIdx, 4, _T("X"));	//Sync Delete 체크시 Making Delete 자동 체크
		}
	}
}


void CESMOptCamInfo::OnBnClickedCaminfoSyncAdd()
{

	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		//jhhan 복수 선택에 따른 MakeDelete 반복 제거
		BOOL bContinue = FALSE;
		BOOL bOK = FALSE;
		while(pos)	//jhhan 16-11-01 복수선택
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );
			pList->SetItemText(nItemIdx, 3, _T(""));
			//jhhan 16-10-10
			//pList->SetItemText(nItemIdx, 4, _T(""));

			if(pList->GetItemText(nItemIdx, 4) == "X")				//Sync Delete 해제시 Make Delete 해제를 물어본다.
			{
				if(bContinue == FALSE)
				{
					if(AfxMessageBox(_T("Make Delete cancellation?"), MB_YESNO) == IDYES)
					{
						pList->SetItemText(nItemIdx, 4, _T(""));
						bOK = TRUE;
					}
					else
					{
						bOK = FALSE;
					}
					bContinue = TRUE;
				}else
				{
					if(bOK)
					{
						pList->SetItemText(nItemIdx, 4, _T(""));
					}
				}
				
			}
		}
	}
}


void CESMOptCamInfo::OnBnClickedCaminfoMakeDelete()
{
	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		while(pos)	//jhhan 16-11-01 복수선택
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );
			pList->SetItemText(nItemIdx, 4, _T("X"));
		}
		m_bModify = TRUE;
	}
}




void CESMOptCamInfo::OnBnClickedCaminfoMakeAdd()
{
	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		while(pos)	//jhhan 16-11-01 복수선택
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );
			if(pList->GetItemText(nItemIdx, 3) == "X")				//Sync Delete 체크 일시 Making Delete 설정시 Sync Delete도 해제
			{
				//if(AfxMessageBox(_T("Sync Delete cancellation?"), MB_YESNO) == IDYES)
				{
					pList->SetItemText(nItemIdx, 3, _T(""));
				}
			}
			pList->SetItemText(nItemIdx, 4, _T(""));
		}
		m_bModify = TRUE;
	}
	
}

//jhhan 16-10-19 KT_CAMName
void CESMOptCamInfo::OnNMClickCaminfoCamlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	int index = pNMItemActivate->iItem;
	CListCtrl *pList = &m_CamList;

	if(index >= 0 && index < pList->GetItemCount())
	{
		m_strCamIP = pList->GetItemText(index, 1);
		m_strCamID = pList->GetItemText(index, 2);

		m_strCamName = pList->GetItemText(index, 5);
		UpdateData(FALSE);
	}


	*pResult = 0;
}

//jhhan 16-10-19 KT_CAMName
void CESMOptCamInfo::OnBnClickedCaminfoModify()
{
	UpdateData();

	POSITION pos = NULL; 
	int nItemIdx  = 0;
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		nItemIdx = pList->GetNextSelectedItem( pos );

		pList->SetItemText(nItemIdx, 1, m_strCamIP);
		pList->SetItemText(nItemIdx, 2, m_strCamID);
		pList->SetItemText(nItemIdx, 5, m_strCamName);
	}
}


void CESMOptCamInfo::OnBnClickedCaminfoReverseAdd()
{
	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );
			pList->SetItemText(nItemIdx, 6, _T("O"));
		}
		m_bModify = TRUE;
	}
}


void CESMOptCamInfo::OnBnClickedCaminfoReverseDelete()
{
	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );
			if(pList->GetItemText(nItemIdx,6) == "O")
				pList->SetItemText(nItemIdx, 6, _T(""));
			
			pList->SetItemText(nItemIdx, 6, _T(""));
		}
		m_bModify = TRUE;
	}
}

void CESMOptCamInfo::OnCbnSelendokComboGroup()
{
	CString strGroup;
	m_cmbGroup.GetLBText(m_cmbGroup.GetCurSel(), strGroup);
	if(strGroup.IsEmpty())
	{
		ESMLog(0, _T("COMBOBOX_Group is Empty!!"));
		return;
	}

	int nSelect = 0;
	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	nSelect = pList->GetSelectedCount();

	if( NULL != pos )
	{
		int nCnt = 1;
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );

			//CString strName;
			//strName.Format(_T("%s[%d]"), strGroup,nCnt);
			CString strGrp;
			strGrp.Format(_T("%s/%d/%d"), strGroup, nCnt++, nSelect);
			
			
			//pList->SetItemText(nItemIdx, 5, strName);
			pList->SetItemText(nItemIdx, 7, strGrp);
		}
		m_bModify = TRUE;
	}
}


void CESMOptCamInfo::OnBnClickedBtnDirection()
{
	CESMOptCamDirection dlg;
	dlg.DoModal();
}


void CESMOptCamInfo::OnBnClickedCaminfoVerticalSet()
{
	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );

			if(pList->GetItemText(nItemIdx,8) == "-90 Degree")
				pList->SetItemText(nItemIdx, 8, _T("90 Degree"));
			else if(pList->GetItemText(nItemIdx,8) == "90 Degree")
				pList->SetItemText(nItemIdx, 8, _T("-90 Degree"));
			else
				pList->SetItemText(nItemIdx, 8, _T("90 Degree"));
		}
		m_bModify = TRUE;
	}
}


void CESMOptCamInfo::OnBnClickedCaminfoVerticalOff()
{
	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );
			pList->SetItemText(nItemIdx, 8, _T("NORMAL"));
		}
		m_bModify = TRUE;
	}
}


HBRUSH CESMOptCamInfo::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CESMOptBase::OnCtlColor(pDC, pWnd, nCtlColor);

	switch(nCtlColor)
	{
	case CTLCOLOR_LISTBOX:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(0,0,0));
			pDC->SetBkMode(TRANSPARENT);
		}
		break;
	}
	return hbr;
}

void CESMOptCamInfo::OnBnClickedBtnInit()
{
	m_cmbView.ResetContent();
}


void CESMOptCamInfo::OnBnClickedBtnViewSet()
{
	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		//m_cmbView.ResetContent();

		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );

			BOOL bFind = FALSE;
			if(m_cmbView.GetCount() > 0)
			{
				for(int i = 0; i < m_cmbView.GetCount(); i++)
				{
					CString strTemp;
					m_cmbView.GetLBText(i, strTemp);
					if(strTemp.IsEmpty() != TRUE)
					{
						CString strView;
						strView.Format(_T("[%03d]"), _ttoi(pList->GetItemText(nItemIdx, 0)));

						if(strTemp.Find(strView) != -1)
						{
							bFind = TRUE;
							m_cmbView.SetCurSel(i);
							break;
						}
					}
				}
				if(bFind == FALSE)
				{
					CString strView;
					//strView.Format(_T("[%03d] %s"), nItemIdx + 1, pList->GetItemText(nItemIdx, 2));
					strView.Format(_T("[%03d]"), nItemIdx + 1);
					
					int nSel = m_cmbView.AddString(strView);
					m_cmbView.SetCurSel(nSel);
				}	
			}else
			{
				CString strView;
				//strView.Format(_T("[%03d] %s"), nItemIdx + 1, pList->GetItemText(nItemIdx, 2));
				strView.Format(_T("[%03d]"), nItemIdx + 1);
				
				int nSel = m_cmbView.AddString(strView);
				m_cmbView.SetCurSel(nSel);
			}
			
			
		}
		m_bModify = TRUE;
	}

	UpdateData(FALSE);
	
}


void CESMOptCamInfo::OnBnClickedCaminfoDetectOn()
{
	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );
			pList->SetItemText(nItemIdx, 9, _T("O"));
		}
		m_bModify = TRUE;
	}
}


void CESMOptCamInfo::OnBnClickedCaminfoDetectOff()
{
	POSITION pos = NULL; 
	CListCtrl *pList = &m_CamList;
	pos  = pList->GetFirstSelectedItemPosition();
	if( NULL != pos )
	{
		while(pos)
		{
			int nItemIdx = pList->GetNextSelectedItem( pos );
			pList->SetItemText(nItemIdx, 9, _T(""));
		}
		m_bModify = TRUE;
	}
}