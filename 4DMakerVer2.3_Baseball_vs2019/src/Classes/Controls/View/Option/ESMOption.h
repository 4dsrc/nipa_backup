/////////////////////////////////////////////////////////////////////////////
//
//	ESMOption.h: main header file for the TestGuarantee application
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, Inc., and may not be copied
//  nor disclosed except upon written agreement
//  by ESMLab, Inc..
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2009-02-01
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ESMIndex.h"

class CESMOption
{
public:
	CESMOption();
	virtual ~CESMOption();

	// page data struct	
	ESMBasicPath		m_Path		;
	ESMBackupString		m_BackupString;
	ESM4DMaker			m_4DOpt		;
	ESMAdjust			m_Adjust	;
	ESMAdjustMovie		m_AdjustMovie;
	ESMLogOpt			m_Log		;
	ESMLogFilter		m_LogFilter	;
	ESMCamInfo			m_CamInfo	;
	ESMPCInfo			m_PcInfo	;
	ESMCeremony			m_Ceremony	;
	ESMManagement		m_menagement;
	ESMTemplate			m_TemplateOpt;
	ESMFileCopy			m_FileCopy	;
	CString				m_strIP;
	CString				m_strHostname;
	CString				m_strConfig;
	BOOL				m_bDirectLoadProfile;

public:
	//-- LOAD
	void Load(CString strFile);
	//-- Create Path Directory
	void CreateDir();
	//CMiLRe 20151013 Template Load INI File
	void LoadTemplateFormFile();
	void LoadTemplateFormFile2();

private:
	void LoadPath(CString* );
	void LoadBackup(CString* );
	//jhhan
	void LoadPathDefault(BOOL bDefault);

	void LoadCamInfo(CString* pStrCamInfo, CString* pStrCamIP, int nCamCount);
	void LoadLog(CString strLogName,CString strVerbosity,CString strLimitday, BOOL bTraceTime);	
	void Load4DOpt(ESM4DMaker MakerOptionData);
	void LoadCeremony(ESMCeremony CeremonyData);
	void LoadAdjMovie(ESMAdjustMovie& AdjustMovie);
	void LoadManagement(ESMManagement ManagementData);
	void LoadAdj(int nTH, int nFlowmoTime, BOOL bClockwise, BOOL bClockreverse,
		int nCamZoom, int nAutoBrightness, int nMinWidth, int nMinHeight, int nMaxWidth, int nMaxHeight, int nPointGapMin, int nTargetLenth);
	//CMiLRe 20151013 Template Load INI File
	//void LoadTemplate(CString strCtrl1, CString strCtrl2, CString strCtrl3, CString strCtrl4, CString strCtrl5, CString strCtrl6, CString strCtrl7, CString strCtrl8, CString strCtrl9, CString strPage );	
	void LoadTemplate2(CString strCtrl[5][9], CString strPage); //2016/07/11 Lee SangA LoadTemplate ���
	void GetNetworkInfo();
};
