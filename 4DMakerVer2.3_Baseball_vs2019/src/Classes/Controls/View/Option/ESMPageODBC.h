////////////////////////////////////////////////////////////////////////////////
//
//	TGPageODBC.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once
#include "TGPropertyPage.h"
#include "afxwin.h"

// CTGPageODBC 대화 상자입니다.

class CTGPageODBC : public CTGPropertyPage
{
	DECLARE_DYNAMIC(CTGPageODBC)

public:
	CTGPageODBC();
	virtual ~CTGPageODBC();
	void InitProp(TGODBC& odbc);
	BOOL SaveProp(TGODBC& odbc);
// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_PAGE_ODBC};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()

public:
	CString m_strODBCDataSource;
	CString m_strODBCUser;	
	CString m_strODBCPassword;	
	CString m_strODBCProvider;
	CString m_strODBCInitCatalog;
	CStatic m_ctrStatusODBC;

	//-- [2010-5-28 Lee JungTaek]
	BOOL m_bODBCCheck;
	CEdit m_ctrTMSProvider;
	CEdit m_ctrTMSInitCatalog;
	CEdit m_ctrTMSDataSource;
	CEdit m_ctrTMSUser;
	CEdit m_ctrTMSPW;

	void ShowStatus(BOOL bStatus = FALSE);

private:
	void CheckStatus();

public:
	afx_msg void OnOdbcCheck();

};
