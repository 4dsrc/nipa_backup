////////////////////////////////////////////////////////////////////////////////
//
//	TGPageTest.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#pragma once
#include "TGPropertyPage.h"
// CTGPageTest 대화 상자입니다.

class CTGPageTest : public CTGPropertyPage
{
	DECLARE_DYNAMIC(CTGPageTest)

	//-- 2010-3-3 hongsu.jung
	int m_nIntervalTC;
	BOOL m_bRemoveFile;
	BOOL m_bRemoveFiles;
	BOOL m_bChkSRM;
	CComboBox m_ctrlPostFail;

	
public:
	CTGPageTest();
	virtual ~CTGPageTest();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_PAGE_TEST };

	void InitProp(TGTest& test);
	BOOL SaveProp(TGTest& test);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()
};
