#pragma once

#include "ESMOptBase.h"
#include "resource.h"
#include "afxcmn.h"
#include "afxwin.h"
#include "ESMEditorEX.h"
#include "ESMNetworkListCtrl.h"
#include "ESMButtonEx.h"

// ESMOptCamInfo 대화 상자입니다.

typedef struct SORT_PARAMS
{
	HWND hWnd;
	int nCol;
	BOOL bAscend;
}SORT_PARAMS;

class CESMOptCamInfo : public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOptCamInfo)

public:
	CESMOptCamInfo();   // 표준 생성자입니다.
	virtual ~CESMOptCamInfo();
	enum { IDD = IDD_OPTION_CAMINFO	};

	CESMOptCamInfo*	m_pCamInfo;	

protected:
	CString m_strCamID;
	CString m_strCamIP;
	//CListCtrl m_CamList;
	CESMNetworkListCtrl m_CamList;
	
	BOOL m_bAscend;
	BOOL m_bModify;

	vector<int> m_vecView;

	DECLARE_MESSAGE_MAP()

public:
	void InitProp(ESMCamInfo& path);
	BOOL SaveProp(ESMCamInfo& path);
	BOOL InitData();
	BOOL GetIsModefy() { return m_bModify; }
	void SetIIsApply() { m_bModify = FALSE; }
	void MoveTo(CListCtrl* pList, int index1, int index2) ;
	virtual BOOL OnInitDialog();
	void OnNMDblclkStatusLst(NMHDR *pNMHDR, LRESULT *pResult);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다

	afx_msg void OnBnClickedAdd();
	afx_msg void OnEnChangeCamid();
	afx_msg void OnBnClickedDelete();
	afx_msg void OnBnClickedAddAll();
	afx_msg void OnBnClickedUp();
	afx_msg void OnBnClickedDown();
	afx_msg void OnBnClickedDelAll();
	afx_msg void SortCamInfo(NMHDR *pNMHDR, LRESULT *pResult);
	static int CALLBACK CESMOptCamInfo::SortFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	afx_msg void OnBnClickedCaminfoMove();
	afx_msg void OnBnClickedCaminfoSyncDelete();
	afx_msg void OnBnClickedCaminfoSyncAdd();
	afx_msg void OnBnClickedCaminfoMakeDelete();
	afx_msg void OnBnClickedCaminfoMakeAdd();
	afx_msg void OnNMClickCaminfoCamlist(NMHDR *pNMHDR, LRESULT *pResult);
	CString m_strCamName;
	afx_msg void OnBnClickedCaminfoModify();

	afx_msg void OnBnClickedCaminfoReverseAdd();
	afx_msg void OnBnClickedCaminfoReverseDelete();
		
	CComboBox m_cmbGroup;
	afx_msg void OnCbnSelendokComboGroup();
	afx_msg void OnBnClickedBtnDirection();
	afx_msg void OnBnClickedCaminfoVerticalSet();
	afx_msg void OnBnClickedCaminfoVerticalOff();

	CESMEditorEX m_edit_num;
	CESMEditorEX m_ctrlCamInfoID;
	CESMEditorEX m_ctrlCamInfoIP;
	CESMEditorEX m_ctrlCamInfoName;
	CESMButtonEx m_btnUp;
	CESMButtonEx m_btnDown;
	CESMButtonEx m_btnInit;
	CESMButtonEx m_btnMove;
	CESMButtonEx m_btnAdd;
	CESMButtonEx m_btnModify;
	CESMButtonEx m_btnDelete;
	CESMButtonEx m_btnAddAll;
	CESMButtonEx m_btnReverseAll;
	CESMButtonEx m_btnDelAll;
	CESMButtonEx m_btnReverseDel;
	CESMButtonEx m_btnSyncDelete;
	CESMButtonEx m_btnSyncAdd;
	CESMButtonEx m_btnMakeDelete;
	CESMButtonEx m_btnMakeAdd;
	CESMButtonEx m_btnEditDirection;
	CESMButtonEx m_btnVerticalSet;
	CESMButtonEx m_btnVerticalOff;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnInit();
	afx_msg void OnBnClickedBtnViewSet();
	CESMEditorEX m_editView;
	CESMButtonEx m_btnViewSet;
	CString m_strView;
	CComboBox m_cmbView;
	afx_msg void OnBnClickedCaminfoDetectOn();
	afx_msg void OnBnClickedCaminfoDetectOff();
	CESMButtonEx m_btnDetectOn;
	CESMButtonEx m_btnDetectOff;
};


