////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptPath.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-07
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMOptPath.h"
#include "ESMUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMOptPath 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMOptPath, CESMOptBase)

CESMOptPath::CESMOptPath()
	: CESMOptBase(CESMOptPath::IDD)	
	, m_strHomePath		(_T(""))	
	, m_strFileSvrPath	(_T(""))
	, m_strLogPath		(_T(""))	
	, m_strConfigPath	(_T(""))
	, m_strWorkPath		(_T(""))	
	, m_strRecord		(_T(""))		
	, m_strOutput		(_T(""))	
	, m_bDefault(FALSE)
{	
}
CESMOptPath::~CESMOptPath(){}

void CESMOptPath::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PATH_HOME		, m_strHomePath		);
	DDX_Text(pDX, IDC_PATH_FILESVR	, m_strFileSvrPath	);
	DDX_Text(pDX, IDC_PATH_LOG		, m_strLogPath		);
	DDX_Text(pDX, IDC_PATH_CONF		, m_strConfigPath	);
	DDX_Text(pDX, IDC_PATH_WORK		, m_strWorkPath		);	
	DDX_Text(pDX, IDC_PATH_RECORD	, m_strRecord		);
	DDX_Text(pDX, IDC_PATH_OUTPUT	, m_strOutput		);	
	DDX_Text(pDX, IDC_PATH_SERVER_RAMPATH	, m_strServerRamPath		);	
	DDX_Text(pDX, IDC_PATH_CLIENT_RAMPATH	, m_strClientRamPath		);	
	DDX_Text(pDX, IDC_PATH_SAVE_IMG			, m_strSaveImgPath		);	
	DDX_Check(pDX, IDC_CHECK_DEFAULT, m_bDefault);
	DDX_Control(pDX, IDC_PATH_HOME, m_ctrlHomePath);
	DDX_Control(pDX, IDC_PATH_FILESVR, m_ctrlFileServerPath);
	DDX_Control(pDX, IDC_PATH_LOG, m_ctrlLogPath);
	DDX_Control(pDX, IDC_PATH_CONF, m_ctrlConfigPath);
	DDX_Control(pDX, IDC_PATH_RECORD, m_ctrlRecordPath);
	DDX_Control(pDX, IDC_PATH_OUTPUT, m_ctrlOutputPath);
	DDX_Control(pDX, IDC_PATH_SERVER_RAMPATH, m_ctrlServerRamPath);
	DDX_Control(pDX, IDC_PATH_CLIENT_RAMPATH, m_ctrlClientRamPath);
	DDX_Control(pDX, IDC_PATH_WORK, m_ctrlWorkPath);
	DDX_Control(pDX, IDC_PATH_SAVE_IMG, m_ctrlBMPSavePath);
	DDX_Control(pDX, IDC_DIR_OPEN, m_btnOpen);
}

BEGIN_MESSAGE_MAP(CESMOptPath, CESMOptBase)
	ON_BN_CLICKED(IDC_DIR_OPEN, OnSelectPath)
	ON_BN_CLICKED(IDC_BTN_DEFAULT, &CESMOptPath::OnBnClickedBtnDefault)
	ON_BN_CLICKED(IDC_CHECK_DEFAULT, &CESMOptPath::OnBnClickedCheckDefault)
END_MESSAGE_MAP()


void CESMOptPath::InitProp(ESMBasicPath& path)
{
	m_strHomePath		= path.strPath[ESM_OPT_PATH_HOME	];
	m_strFileSvrPath	= path.strPath[ESM_OPT_PATH_FILESVR	];
	m_strLogPath		= path.strPath[ESM_OPT_PATH_LOG		];
	m_strConfigPath		= path.strPath[ESM_OPT_PATH_CONF	];	
	m_strWorkPath		= path.strPath[ESM_OPT_PATH_WORK	];	
	m_strRecord			= path.strPath[ESM_OPT_PATH_RECORD	];
	m_strOutput			= path.strPath[ESM_OPT_PATH_OUTPUT	];
	m_strServerRamPath	= path.strPath[ESM_OPT_PATH_SERVERRAM];
	m_strClientRamPath	= path.strPath[ESM_OPT_PATH_CLIENTRAM];
	m_strSaveImgPath	= path.strPath[ESM_OPT_PATH_SAVE_IMG];
	//jhhan
	m_bDefault = path.bDefault;
	UpdateData(FALSE);
	
	m_btnOpen.									SetWindowPos(NULL,460,24,30,24,NULL);

	GetDlgItem(IDC_STATIC_HOME_PATH)->			SetWindowPos(NULL,0,0,200,24,NULL);
	GetDlgItem(IDC_STATIC_FILE_SERVER_PATH)->	SetWindowPos(NULL,0,61,200,24,NULL);
	GetDlgItem(IDC_STATIC_LOG_PATH)->			SetWindowPos(NULL,0,122,200,24,NULL);
	GetDlgItem(IDC_STATIC_CONFIG_PATH)->		SetWindowPos(NULL,0,183,200,24,NULL);
	GetDlgItem(IDC_STATIC_RECORD_PATH)->		SetWindowPos(NULL,0,244,200,24,NULL);
	GetDlgItem(IDC_STATIC_OUTPUT_PATH)->		SetWindowPos(NULL,0,305,200,24,NULL);
	GetDlgItem(IDC_STATIC_SERVER_RAM_PATH)->	SetWindowPos(NULL,0,366,200,24,NULL);
	GetDlgItem(IDC_STATIC_CLIENT_RAM_PATH)->	SetWindowPos(NULL,0,427,200,24,NULL);
	GetDlgItem(IDC_STATIC_WORK_PATH)->			SetWindowPos(NULL,0,488,200,24,NULL);
	GetDlgItem(IDC_STATIC_BMP_SAVE_PATH)->		SetWindowPos(NULL,0,549,200,24,NULL);
	GetDlgItem(IDC_CHECK_DEFAULT)->				SetWindowPos(NULL,0,557,200,24,NULL);

	m_ctrlHomePath.								SetWindowPos(NULL,0,24,450,24,NULL);
	m_ctrlFileServerPath.						SetWindowPos(NULL,0,85,450,24,NULL);
	m_ctrlLogPath.								SetWindowPos(NULL,0,146,450,24,NULL);
	m_ctrlConfigPath.							SetWindowPos(NULL,0,207,450,24,NULL);
	m_ctrlRecordPath.							SetWindowPos(NULL,0,268,450,24,NULL);
	m_ctrlOutputPath.							SetWindowPos(NULL,0,329,450,24,NULL);
	m_ctrlServerRamPath.						SetWindowPos(NULL,0,390,450,24,NULL);
	m_ctrlClientRamPath.						SetWindowPos(NULL,0,451,450,24,NULL);
	m_ctrlWorkPath.								SetWindowPos(NULL,0,512,450,24,NULL);
	m_ctrlBMPSavePath.							SetWindowPos(NULL,0,573,450,24,NULL);

	EnableControlItems(!m_bDefault);
}

BOOL CESMOptPath::SaveProp(ESMBasicPath& path)
{
	if(!UpdateData(TRUE))
		return FALSE;

	path.strPath[ESM_OPT_PATH_HOME		] = m_strHomePath		;
	path.strPath[ESM_OPT_PATH_FILESVR	] = m_strFileSvrPath	;
	path.strPath[ESM_OPT_PATH_LOG		] = m_strLogPath		;
	path.strPath[ESM_OPT_PATH_CONF		] = m_strConfigPath		;	
	path.strPath[ESM_OPT_PATH_WORK		] = m_strWorkPath		;
	path.strPath[ESM_OPT_PATH_RECORD	] = m_strRecord			;
	path.strPath[ESM_OPT_PATH_OUTPUT	] = m_strOutput			;
	path.strPath[ESM_OPT_PATH_SERVERRAM	] = m_strServerRamPath	;
	path.strPath[ESM_OPT_PATH_CLIENTRAM	] = m_strClientRamPath	;
	path.strPath[ESM_OPT_PATH_SAVE_IMG  ] = m_strSaveImgPath	;
	//jhhan
	path.bDefault = m_bDefault;

	

	return TRUE;
}

//jhhan
void CESMOptPath::OnBnClickedBtnDefault()
{
	if(AfxMessageBox(_T("Are you sure you want to reset?"), MB_YESNO) == IDYES)
	{
		InitPathInfo();	
	}
}


void CESMOptPath::OnBnClickedCheckDefault()
{
	UpdateData(TRUE);

	if(m_bDefault)
	{
		if(AfxMessageBox(_T("Are you sure you want to reset?"), MB_YESNO) == IDYES)
		{
			InitPathInfo();
		}else
		{
			m_bDefault = FALSE;
			UpdateData(FALSE);
			return;
		}
	}

	EnableControlItems(!m_bDefault);
}

void CESMOptPath::EnableControlItems(BOOL bEnable/* = TRUE*/)
{
	GetDlgItem(IDC_DIR_OPEN)->EnableWindow(bEnable);
	GetDlgItem(IDC_PATH_HOME)->EnableWindow(bEnable);
	GetDlgItem(IDC_PATH_FILESVR)->EnableWindow(bEnable);
	GetDlgItem(IDC_PATH_LOG)->EnableWindow(bEnable);
	GetDlgItem(IDC_PATH_CONF)->EnableWindow(bEnable);
	GetDlgItem(IDC_PATH_WORK)->EnableWindow(bEnable);
	GetDlgItem(IDC_PATH_RECORD)->EnableWindow(bEnable);
	GetDlgItem(IDC_PATH_OUTPUT)->EnableWindow(bEnable);
	GetDlgItem(IDC_PATH_SERVER_RAMPATH)->EnableWindow(bEnable);
	GetDlgItem(IDC_PATH_CLIENT_RAMPATH)->EnableWindow(bEnable);
	GetDlgItem(IDC_PATH_SAVE_IMG)->EnableWindow(bEnable);
}

void CESMOptPath::InitPathInfo()
{
	CString strPath[ESM_OPT_PATH_ALL];
	strPath[ESM_OPT_PATH_HOME	].Format(_T("%s"),_T("C:\\Program Files\\ESMLab\\4DMaker"));
	strPath[ESM_OPT_PATH_FILESVR].Format(_T("\\\\%s\\4DMaker"), ESMUtil::GetLocalIPAddress());
	strPath[ESM_OPT_PATH_LOG	].Format(_T("$(HOME)\\log"));		
	strPath[ESM_OPT_PATH_CONF	].Format(_T("$(HOME)\\config"));		
	strPath[ESM_OPT_PATH_RECORD	].Format(_T("F:\\RecordSave\\Record"));
	strPath[ESM_OPT_PATH_OUTPUT	].Format(_T("F:\\RecordSave\\Output"));	
	strPath[ESM_OPT_PATH_SERVERRAM	].Format(_T("M:\\Movie"));
	strPath[ESM_OPT_PATH_CLIENTRAM	].Format(_T("\\Movie"));
	strPath[ESM_OPT_PATH_WORK	].Format(_T("$(HOME)\\work"));
	strPath[ESM_OPT_PATH_SAVE_IMG	] = _T("");

	m_strHomePath		= strPath[ESM_OPT_PATH_HOME	];
	m_strFileSvrPath	= strPath[ESM_OPT_PATH_FILESVR	];
	m_strLogPath		= strPath[ESM_OPT_PATH_LOG		];
	m_strConfigPath		= strPath[ESM_OPT_PATH_CONF	];	
	m_strWorkPath		= strPath[ESM_OPT_PATH_WORK	];	
	m_strRecord			= strPath[ESM_OPT_PATH_RECORD	];
	m_strOutput			= strPath[ESM_OPT_PATH_OUTPUT	];
	m_strServerRamPath	= strPath[ESM_OPT_PATH_SERVERRAM];
	m_strClientRamPath	= strPath[ESM_OPT_PATH_CLIENTRAM];
	m_strSaveImgPath	= strPath[ESM_OPT_PATH_SAVE_IMG];

	UpdateData(FALSE);	
}