////////////////////////////////////////////////////////////////////////////////
//
//	ESMOptLogFilter.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMOptLogFilter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CESMOptLogFilter, CESMOptBase)

BEGIN_MESSAGE_MAP(CESMOptLogFilter, CESMOptBase)
	ON_BN_CLICKED(IDC_OPT_LOG_FILTER_IN_CHECK1, OnBnClickedCheck)
	ON_BN_CLICKED(IDC_OPT_LOG_FILTER_IN_CHECK2, OnBnClickedCheck)
	ON_BN_CLICKED(IDC_OPT_LOG_FILTER_IN_CHECK3, OnBnClickedCheck)
	ON_BN_CLICKED(IDC_OPT_LOG_FILTER_IN_CHECK4, OnBnClickedCheck)
	ON_BN_CLICKED(IDC_OPT_LOG_FILTER_EX_CHECK1, OnBnClickedCheckEx)
	ON_BN_CLICKED(IDC_OPT_LOG_FILTER_EX_CHECK2, OnBnClickedCheckEx)
	ON_BN_CLICKED(IDC_OPT_LOG_FILTER_EX_CHECK3, OnBnClickedCheckEx)
	ON_BN_CLICKED(IDC_OPT_LOG_FILTER_EX_CHECK4, OnBnClickedCheckEx)
END_MESSAGE_MAP()

CESMOptLogFilter::CESMOptLogFilter()
: CESMOptBase(CESMOptLogFilter::IDD){}
CESMOptLogFilter::~CESMOptLogFilter(){}

void CESMOptLogFilter::DoDataExchange(CDataExchange* pDX)
{
	CESMOptBase::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_IN_CHECK1, m_bInChk[0]);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_IN_CHECK2, m_bInChk[1]);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_IN_CHECK3, m_bInChk[2]);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_IN_CHECK4, m_bInChk[3]);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_IN_CHECK5, m_bInPutMsg[0]);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_IN_CHECK6, m_bInPutMsg[1]);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_IN_CHECK7, m_bInPutMsg[2]);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_IN_CHECK8, m_bInPutMsg[3]);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_EX_CHECK1, m_bExChk[0]);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_EX_CHECK2, m_bExChk[1]);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_EX_CHECK3, m_bExChk[2]);
	DDX_Check(pDX, IDC_OPT_LOG_FILTER_EX_CHECK4, m_bExChk[3]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_IN_EDIT1, m_strInclude[0]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_IN_EDIT2, m_strInclude[1]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_IN_EDIT3, m_strInclude[2]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_IN_EDIT4, m_strInclude[3]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_IN_EDIT5, m_strInPutMsg[0]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_IN_EDIT6, m_strInPutMsg[1]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_IN_EDIT7, m_strInPutMsg[2]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_IN_EDIT8, m_strInPutMsg[3]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_EX_EDIT1, m_strExclude[0]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_EX_EDIT2, m_strExclude[1]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_EX_EDIT3, m_strExclude[2]);
	DDX_Text(pDX, IDC_OPT_LOG_FILTER_EX_EDIT4, m_strExclude[3]);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_IN_EDIT1, m_ctrlString1);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_IN_EDIT2, m_ctrlString2);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_IN_EDIT3, m_ctrlString3);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_IN_EDIT4, m_ctrlString4);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_IN_EDIT5, m_ctrlMessage1);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_IN_EDIT6, m_ctrlMessage2);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_IN_EDIT7, m_ctrlMessage3);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_IN_EDIT8, m_ctrlMessage4);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_EX_EDIT1, m_ctrlSerialLog1);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_EX_EDIT2, m_ctrlSerialLog2);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_EX_EDIT3, m_ctrlSerialLog3);
	DDX_Control(pDX, IDC_OPT_LOG_FILTER_EX_EDIT4, m_ctrlSerialLog4);
	DDX_Control(pDX, IDC_STATIC_INC_FILTER, m_ctrlFrame1);
	DDX_Control(pDX, IDC_STATIC_EXLOG, m_ctrlFrame2);
}


void CESMOptLogFilter::InitProp(ESMLogFilter& logfilter)
{
	//-- SET TEXT
	for( int i = 0; i < sizeof(m_strInclude) / sizeof(CString); ++i )	m_strInclude[i] = logfilter.strInclude[i];
	for( int i = 0; i < sizeof(m_strExclude) / sizeof(CString); ++i )	m_strExclude[i] = logfilter.strExclude[i];	
	for( int i = 0; i < sizeof(m_strInPutMsg) / sizeof(CString); ++i )	m_strInPutMsg[i] = logfilter.strInPutMsg[i];	


	//-- 2010-02-2 hongsu.jung
	//-- SET WINDOWS ENABLE
	for( int i = 0; i < sizeof(m_bInChk) / sizeof(BOOL) ; ++i )			
	{
		m_bInChk[i] = logfilter.bInCheck[i];
		GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT1+i)->EnableWindow(m_bInChk[i]);
		GetDlgItem(IDC_OPT_LOG_FILTER_IN_CHECK5+i)->EnableWindow(m_bInChk[i]);
		GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT5+i)->EnableWindow(m_bInChk[i]);	
	}
	for( int i = 0; i < sizeof(m_bExChk) / sizeof(BOOL) ; ++i )			
	{
		m_bExChk[i] = logfilter.bExCheck[i];
		GetDlgItem(IDC_OPT_LOG_FILTER_EX_EDIT1+i)->EnableWindow(m_bExChk[i]);	
	}
	for( int i = 0; i < sizeof(m_bInChk) / sizeof(BOOL) ; ++i )			
	{
		m_bInPutMsg[i] = logfilter.bInPutCheck[i];
	}

	GetDlgItem(IDC_STATIC_INC_FILTER)->				SetWindowPos(NULL,0,0,520,210,NULL);

	GetDlgItem(IDC_STATIC_CHECK_STRING)->			SetWindowPos(NULL,45,29,100,24,NULL);
	GetDlgItem(IDC_STATIC_PUT_MSG)->				SetWindowPos(NULL,286,29,100,24,NULL);

	GetDlgItem(IDC_OPT_LOG_FILTER_IN_CHECK1)->		SetWindowPos(NULL,21,64,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT1)->		SetWindowPos(NULL,45,59,207,24,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_CHECK5)->		SetWindowPos(NULL,262,64,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT5)->		SetWindowPos(NULL,286,59,207,24,NULL);

	GetDlgItem(IDC_OPT_LOG_FILTER_IN_CHECK2)->		SetWindowPos(NULL,21,98,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT2)->		SetWindowPos(NULL,45,93,207,24,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_CHECK6)->		SetWindowPos(NULL,262,98,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT6)->		SetWindowPos(NULL,286,93,207,24,NULL);

	GetDlgItem(IDC_OPT_LOG_FILTER_IN_CHECK3)->		SetWindowPos(NULL,21,132,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT3)->		SetWindowPos(NULL,45,127,207,24,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_CHECK7)->		SetWindowPos(NULL,262,132,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT7)->		SetWindowPos(NULL,286,127,207,24,NULL);

	GetDlgItem(IDC_OPT_LOG_FILTER_IN_CHECK4)->		SetWindowPos(NULL,21,166,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT4)->		SetWindowPos(NULL,45,161,207,24,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_CHECK8)->		SetWindowPos(NULL,262,166,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT8)->		SetWindowPos(NULL,286,161,207,24,NULL);

	GetDlgItem(IDC_STATIC_EXLOG)->					SetWindowPos(NULL,0,230,520,190,NULL);

	GetDlgItem(IDC_OPT_LOG_FILTER_EX_CHECK1)->		SetWindowPos(NULL,21,273,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_EX_EDIT1)->		SetWindowPos(NULL,45,268,448,24,NULL);

	GetDlgItem(IDC_OPT_LOG_FILTER_EX_CHECK2)->		SetWindowPos(NULL,21,307,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_EX_EDIT2)->		SetWindowPos(NULL,45,302,448,24,NULL);

	GetDlgItem(IDC_OPT_LOG_FILTER_EX_CHECK3)->		SetWindowPos(NULL,21,341,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_EX_EDIT3)->		SetWindowPos(NULL,45,336,448,24,NULL);

	GetDlgItem(IDC_OPT_LOG_FILTER_EX_CHECK4)->		SetWindowPos(NULL,21,375,14,14,NULL);
	GetDlgItem(IDC_OPT_LOG_FILTER_EX_EDIT4)->		SetWindowPos(NULL,45,370,448,24,NULL);

	UpdateData(FALSE);
}

BOOL CESMOptLogFilter::SaveProp(ESMLogFilter& logfilter)
{
	if(!UpdateData(TRUE))
		return FALSE;

	for( int i = 0; i < sizeof(m_bInChk) / sizeof(BOOL) ; ++i )			logfilter.bInCheck[i] = m_bInChk[i] ;
	for( int i = 0; i < sizeof(m_bExChk) / sizeof(BOOL) ; ++i )			logfilter.bExCheck[i] = m_bExChk[i] ;
	for( int i = 0; i < sizeof(m_strInclude) / sizeof(CString); ++i )	logfilter.strInclude[i] = m_strInclude[i] ;
	for( int i = 0; i < sizeof(m_strExclude) / sizeof(CString); ++i )	logfilter.strExclude[i] = m_strExclude[i] ;
	
	//-- Add Push Message
	for( int i = 0; i < sizeof(m_bInPutMsg) / sizeof(BOOL) ; ++i )		logfilter.bInPutCheck[i] = m_bInPutMsg[i] ;
	for( int i = 0; i < sizeof(m_strInPutMsg) / sizeof(CString); ++i )	logfilter.strInPutMsg[i] = m_strInPutMsg[i] ;

	return TRUE;
}

void CESMOptLogFilter::OnBnClickedCheck()
{
	UpdateData(TRUE);
	int nIndex;
	const MSG *pMsg = GetCurrentMessage();
	switch(pMsg->wParam)
	{
	case IDC_OPT_LOG_FILTER_IN_CHECK1:
		nIndex = 0;
		break;
	case IDC_OPT_LOG_FILTER_IN_CHECK2:
		nIndex = 1;
		break;
	case IDC_OPT_LOG_FILTER_IN_CHECK3:
		nIndex = 2;
		break;
	case IDC_OPT_LOG_FILTER_IN_CHECK4:
		nIndex = 3;
		break;
	default:
		return;
	}

	GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT1+nIndex)->EnableWindow(m_bInChk[nIndex]);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_CHECK5+nIndex)->EnableWindow(m_bInChk[nIndex]);
	GetDlgItem(IDC_OPT_LOG_FILTER_IN_EDIT5+nIndex)->EnableWindow(m_bInChk[nIndex]);	
}

void CESMOptLogFilter::OnBnClickedCheckEx()
{
	UpdateData(TRUE);
	int nIndex;
	const MSG *pMsg = GetCurrentMessage();
	switch(pMsg->wParam)
	{
	case IDC_OPT_LOG_FILTER_EX_CHECK1:
		nIndex = 0;
		break;
	case IDC_OPT_LOG_FILTER_EX_CHECK2:
		nIndex = 1;
		break;
	case IDC_OPT_LOG_FILTER_EX_CHECK3:
		nIndex = 2;
		break;
	case IDC_OPT_LOG_FILTER_EX_CHECK4:
		nIndex = 3;
		break;
	default:
		return;
	}

	GetDlgItem(IDC_OPT_LOG_FILTER_EX_EDIT1+nIndex)->EnableWindow(m_bExChk[nIndex]);	
}


//------------------------------------------------------------------------------
//-- Date	 : 2010-02-2 
//-- Owner	 : hongsu.jung
//-- Comment : Put Message To Serial m_bInPutMsg[0~3]);
//------------------------------------------------------------------------------
