#pragma once

#include "TGPropertyPage.h"

#define LOCAL_HOST		_T("127.0.0.1");
#define	SRM_BASE_PORT	28688


class CTGPageSrmview : public CTGPropertyPage
{
	DECLARE_DYNAMIC(CTGPageSrmview)
	DECLARE_MESSAGE_MAP()

public:
	CTGPageSrmview();
	virtual ~CTGPageSrmview();

	enum { IDD = IDD_OPTION_PAGE_SRMVIEW };

	void InitProp(TGSrmview& opt);
	BOOL SaveProp(TGSrmview& opt);

public:	
	CString m_strModel	;
	CString m_strIP		;
	int		m_nPort		;	

private:
	CIPAddressCtrl m_ipcIP;	
	CComboBox m_cmbModel;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
};
