////////////////////////////////////////////////////////////////////////////////
//
//	ESMOpt4DMaker.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-07
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "ESMOptBase.h"
#include "resource.h"
#include "afxwin.h"

class CESMOptEffect : public CESMOptBase
{
	DECLARE_DYNAMIC(CESMOptEffect)

public:
	CESMOptEffect();
	virtual ~CESMOptEffect();
	void InitProp(ESMEffect& Opt);
	BOOL SaveProp(ESMEffect& Opt);
	enum { IDD = IDD_OPTION_EFFECT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()

public:	
	int m_nPosX;
	int m_nPosY;
	int m_nWeight1;
	int m_nWeight2;
	int m_nRatio;
};
