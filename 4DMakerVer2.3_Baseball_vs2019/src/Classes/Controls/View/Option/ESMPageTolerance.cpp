////////////////////////////////////////////////////////////////////////////////
//
//	TGPageTolerance.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-16
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TG.h"
#include "TGPageTolerance.h"


// CTGPageTolerance 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTGPageTolerance, CTGPropertyPage)

CTGPageTolerance::CTGPageTolerance()
	: CTGPropertyPage(CTGPageTolerance::IDD)		
{

}

CTGPageTolerance::~CTGPageTolerance()
{
}

void CTGPageTolerance::DoDataExchange(CDataExchange* pDX)
{
	CTGPropertyPage::DoDataExchange(pDX);
	//-- 2011-12-2 hongsu.jung
	DDX_Text(pDX, IDC_OCR_TOLERANCE,	m_nToleraceOCR);	
	DDX_Text(pDX, IDC_FPS_TOLERANCE,	m_nToleraceFps);

	//-- 2012-08-16 joonho.kim
	DDX_Text(pDX, IDC_FPS_TOLERANCE_PERCENT,		m_nToleranceFpsPercent);
	DDX_Text(pDX, IDC_FPS_TOLERANCE_MS,				m_nToleranceFpsMs);
	DDX_Control(pDX, IDC_FPS_TOLERANCE_PERCENT,		m_edFpsPercent);
	DDX_Control(pDX, IDC_FPS_TOLERANCE_MS,			m_edFpsMs);
	DDX_Control(pDX, IDC_FPS_TOLERANCE_CHK_PERCENT,	m_ChkFpsPercent);
	DDX_Control(pDX, IDC_FPS_TOLERANCE_CHK_MS,		m_ChkFpsMs);

	DDX_Text(pDX, IDC_PC_TOLERANCE_PERCENT,			m_nTolerancePCPercent);
	DDX_Text(pDX, IDC_PC_TOLERANCE_MS,				m_nTolerancePCMs);
	DDX_Control(pDX, IDC_PC_TOLERANCE_PERCENT,		m_edPCPercent);
	DDX_Control(pDX, IDC_PC_TOLERANCE_MS,			m_edPCMs);
	DDX_Control(pDX, IDC_PC_TOLERANCE_CHK_PERCENT,	m_ChkPCPercent);
	DDX_Control(pDX, IDC_PC_TOLERANCE_CHK_MS,		m_ChkPCMs);

	DDX_Text(pDX, IDC_IMAGE_TOLERANCE_R		 ,	m_nToleranceImgR		);
	DDX_Text(pDX, IDC_IMAGE_TOLERANCE_G		 ,	m_nToleranceImgG		);
	DDX_Text(pDX, IDC_IMAGE_TOLERANCE_B		 ,	m_nToleranceImgB		);
	DDX_Text(pDX, IDC_LATENCY_TOLERANCE_COLOR,	m_nToleranceLTColor		);

	DDX_Text(pDX, IDC_TOLERANCE_PAN,		m_nTolerancePan);
	DDX_Text(pDX, IDC_TOLERANCE_TILT,		m_nToleranceTilt);
	DDX_Text(pDX, IDC_TOLERANCE_ZOOM,		m_nToleranceZoom);
	DDX_Text(pDX, IDC_TOLERANCE_FOCUS,		m_nToleranceFocus);
}

BEGIN_MESSAGE_MAP(CTGPageTolerance, CTGPropertyPage)
	ON_BN_CLICKED(IDC_FPS_TOLERANCE_CHK_PERCENT, &CTGPageTolerance::OnCheckTolerance)
	ON_BN_CLICKED(IDC_FPS_TOLERANCE_CHK_MS, &CTGPageTolerance::OnCheckTolerance)

	ON_BN_CLICKED(IDC_PC_TOLERANCE_CHK_PERCENT, &CTGPageTolerance::OnPCCheckTolerance)
	ON_BN_CLICKED(IDC_PC_TOLERANCE_CHK_MS, &CTGPageTolerance::OnPCCheckTolerance)
END_MESSAGE_MAP()

void CTGPageTolerance::OnCheckTolerance()
{
	if(m_ChkFpsPercent.GetCheck())
	{
		m_edFpsPercent.EnableWindow(TRUE);
		m_edFpsMs.EnableWindow(FALSE);
	}
	else
	{
		m_edFpsPercent.EnableWindow(FALSE);
		m_edFpsMs.EnableWindow(TRUE);
	}
}

void CTGPageTolerance::OnPCCheckTolerance()
{
	if(m_ChkPCPercent.GetCheck())
	{
		m_edPCPercent.EnableWindow(TRUE);
		m_edPCMs.EnableWindow(FALSE);
	}
	else
	{
		m_edPCPercent.EnableWindow(FALSE);
		m_edPCMs.EnableWindow(TRUE);
	}
}
void CTGPageTolerance::InitProp(TGTolerance& tolerance)
{	
	m_nToleraceOCR	= tolerance.nToleraceOCR;	
	m_nToleraceFps	= tolerance.nFps;

	//-- 2012-08-16 joonho.kim
	if(!tolerance.bFPSOpt)
	{
		m_nToleranceFpsPercent	=	tolerance.nFPSTolerance;
		m_nToleranceFpsMs		=	0;
		m_ChkFpsPercent.SetCheck(1);
		m_edFpsPercent.EnableWindow(TRUE);
		m_edFpsMs.EnableWindow(FALSE);
	}
	else
	{
		m_nToleranceFpsMs		=	tolerance.nFPSTolerance;
		m_nToleranceFpsPercent	=	0;
		m_ChkFpsMs.SetCheck(1);
		m_edFpsPercent.EnableWindow(FALSE);
		m_edFpsMs.EnableWindow(TRUE);
	}

	//-- 2012-09-25 joonho.kim
	if(!tolerance.bPCOpt)
	{
		m_nTolerancePCPercent	=	tolerance.nPCTolerance;
		m_nTolerancePCMs		=	0;
		m_ChkPCPercent.SetCheck(1);
		m_edPCPercent.EnableWindow(TRUE);
		m_edPCMs.EnableWindow(FALSE);
	}
	else
	{
		m_nTolerancePCMs		=	tolerance.nPCTolerance;
		m_nTolerancePCPercent	=	0;
		m_ChkPCMs.SetCheck(1);
		m_edPCPercent.EnableWindow(FALSE);
		m_edPCMs.EnableWindow(TRUE);
	}

	//-- 2012-08-20 joonho.kim
	m_nToleranceImgR = tolerance.nToleranceImgR;
	m_nToleranceImgG = tolerance.nToleranceImgG;
	m_nToleranceImgB = tolerance.nToleranceImgB;
	m_nToleranceLTColor = tolerance.nToleranceLTColor;

	m_nTolerancePan = tolerance.nTolerancePan;
	m_nToleranceTilt = tolerance.nToleranceTilt;
	m_nToleranceZoom = tolerance.nToleranceZoom;
	m_nToleranceFocus = tolerance.nToleranceFocus;	

	UpdateData(FALSE);
}

BOOL CTGPageTolerance::SaveProp(TGTolerance& tolerance)
{
	if(!UpdateData(TRUE))
		return FALSE;

	tolerance.nToleraceOCR	= m_nToleraceOCR;
	tolerance.nFps			= m_nToleraceFps;

	//-- 2012-08-16 joonho.kim
	if(m_ChkFpsPercent.GetCheck())
	{
		tolerance.bFPSOpt = FALSE;
		tolerance.nFPSTolerance = m_nToleranceFpsPercent;
	}
	else
	{
		tolerance.bFPSOpt = TRUE;
		tolerance.nFPSTolerance = m_nToleranceFpsMs;
	}

	if(m_ChkPCPercent.GetCheck())
	{
		tolerance.bPCOpt = FALSE;
		tolerance.nPCTolerance = m_nTolerancePCPercent;
	}
	else
	{
		tolerance.bPCOpt = TRUE;
		tolerance.nPCTolerance = m_nTolerancePCMs;
	}
	//-- 2012-08-20 joonho.kim
	tolerance.nToleranceImgR	= m_nToleranceImgR;
	tolerance.nToleranceImgG	= m_nToleranceImgG;
	tolerance.nToleranceImgB	= m_nToleranceImgB;
	tolerance.nToleranceLTColor = m_nToleranceLTColor;

	tolerance.nTolerancePan	= m_nTolerancePan;
	tolerance.nToleranceTilt = m_nToleranceTilt;
	tolerance.nToleranceZoom = m_nToleranceZoom;
	tolerance.nToleranceFocus= m_nToleranceFocus;
	return TRUE;
}
