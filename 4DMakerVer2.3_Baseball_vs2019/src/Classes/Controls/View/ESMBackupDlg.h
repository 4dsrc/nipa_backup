#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include <vector>

// CESMBackupDlg 대화 상자입니다.

class CESMBackupDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CESMBackupDlg)

public:
	CESMBackupDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CESMBackupDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_BACKUP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	
public:
	//Value
	CStatic m_stTotalCount;
	CStatic m_stCopyCount;
	int m_nTotalFileCount;
	int m_nCurrentFileCount;
	CString	m_strProfile;

	//jhhan
	CString m_str4DM;
	void Backup4DMFile(CString strPath);

	//Function
	virtual BOOL Create( CWnd* pParentWnd = NULL);
	void BackupRecordFile(CString strPath);
	void GetFileCount(CString strPath);
	virtual BOOL DestroyWindow();
	virtual void PostNcDestroy();
	afx_msg void OnBnClickedOk();
	LRESULT OnESMOPTMsg(WPARAM w, LPARAM l);

private:
	//Value
	vector<CString> m_IPList;
	
	//Function
	void GetIPList();

	static unsigned WINAPI FileCountThread(LPVOID param);
	static unsigned WINAPI FileCopyThread(LPVOID param);	

	//jhhan
	static unsigned WINAPI File4DMCopyThread(LPVOID param);

public:
	virtual BOOL OnInitDialog();
	CStatic m_stLog;
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void GetFileCount(CString strPath, CString strDest);
	void BackupLogFile(CString strPath, CString strDest);
};
