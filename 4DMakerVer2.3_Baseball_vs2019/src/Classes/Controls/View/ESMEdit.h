/////////////////////////////////////////////////////////////////////////////
//
// TGEdit.h: implementation of the CTGIni class.
//
//
// Copyright (c) 2008 ESMLab, Inc.
// All rights reserved.
//
// This software is the confidential and proprietary information of ESMLab, Inc. ("Confidential Information").  
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2008-07-31
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

/////////////////////////////////////////////////////////////////////////////
// CTGEdit window

class CTGEdit : public CEdit
{
public:
	// Construction
	CTGEdit();
	virtual ~CTGEdit();

public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTGEdit)
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CTGEdit)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
