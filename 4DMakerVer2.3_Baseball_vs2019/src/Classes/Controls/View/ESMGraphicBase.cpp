////////////////////////////////////////////////////////////////////////////////
//
//	TGGraphicBase.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////



#include "stdafx.h"
#include "TGGraphicBase.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define MULTI_VIEW_FRAME_LEFT					180
#define MULTI_VIEW_LEFT_SCROLL_SIZE_WIDTH		280
#define MULTI_VIEW_LEFT_SCROLL_SIZE_HEIGHT		150

// CTGGraphicBase
IMPLEMENT_DYNCREATE(CTGGraphicBase, CScrollView)
BEGIN_MESSAGE_MAP(CTGGraphicBase, CScrollView)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_MESSAGE(WM_GRAPH_MSG, TGGraphMessage)
END_MESSAGE_MAP()

// CTGGraphicBase 생성/소멸

CTGGraphicBase::CTGGraphicBase()
{
	m_nTotalByte = NULL;
}

CTGGraphicBase::~CTGGraphicBase()
{
}

// CTGGraphicBase 진단

#ifdef _DEBUG
void CTGGraphicBase::AssertValid() const
{
	CView::AssertValid();
}

void CTGGraphicBase::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG


// TGGraphicBase 메시지 처리기

void CTGGraphicBase::OnSize(UINT nType, int cx, int cy)
{
	
	/*if(cx < MULTI_VIEW_LEFT_SCROLL_SIZE_WIDTH + MULTI_VIEW_FRAME_LEFT )
		cx = MULTI_VIEW_LEFT_SCROLL_SIZE_WIDTH + MULTI_VIEW_FRAME_LEFT;

	if(cy < MULTI_VIEW_LEFT_SCROLL_SIZE_HEIGHT)
		cy = MULTI_VIEW_LEFT_SCROLL_SIZE_HEIGHT;

	m_Graph.m_rcParent = CRect(0, 0, cx, cy);
	CRect rcGap = CRect(0, 0, 0, 0);
	m_Graph.SetChildPosition(rcGap);	

	//** Debugging 2001/9/6
	//** 스크롤을 맨 위로 이동 시킨다.
	ScrollToPosition(CPoint(0,0));
	CScrollView::OnSize(nType, cx, cy);	
*/
	nType;

	m_Graph.m_rcParent = CRect(0, 0, cx, cy);
	m_Graph.SetChildPosition(CRect(0, 0, 0, 0));
}

void CTGGraphicBase::OnInitialUpdate()
{
	//-- Create Graph
	//-- 2010-8-11 hongsu.jung
	//-- MOVE TO OnCreate 
	// CreateGraph();
}

int CTGGraphicBase::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CSize sizeTotal;
	sizeTotal.cx = MULTI_VIEW_LEFT_SCROLL_SIZE_WIDTH + MULTI_VIEW_FRAME_LEFT;
	sizeTotal.cy = MULTI_VIEW_LEFT_SCROLL_SIZE_HEIGHT;
	SetScrollSizes(MM_TEXT, sizeTotal);

	//-- 2010-8-11 hongsu.jung
	//-- Create Graph
	CreateGraph();
	return 0;
}

LRESULT CTGGraphicBase::TGGraphMessage(UINT wparam, LONG lparam)
{
	m_Graph.TGGraphMessage(wparam, lparam);
	return 1L;
}

//------------------------------------------------------------------------------
//-- Date	 : 2010-3-6 
//-- Owner	 : hongsu.jung
//-- Comment : Add Data To Graph
//------------------------------------------------------------------------------
void CTGGraphicBase::ClearViewer()
{	
	//-- Revision : Check Graph Manager
	if(m_Graph.m_pTGGraphScaleWnd)
		m_Graph.RemoveAllGraphData();	
}

//------------------------------------------------------------------------------
//-- Date	 : 2010-3-6 
//-- Owner	 : hongsu.jung
//-- Comment : Add Data To Graph
//------------------------------------------------------------------------------
void CTGGraphicBase::AddStatusInfo(int nID, CString strName)
{	
	//-- Revision : Check Graph Manager
	if(m_Graph.m_pTGGraphScaleWnd)
		m_Graph.AddStatusInfo(nID, strName);
}
