////////////////////////////////////////////////////////////////////////////////
//
//	TGGridCtrl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "TGGridCtrl.h"
#include "TGListEditCtrl.h"

#include "TG.h"
#include "MainFrm.h"
#include "TGGridCtrl.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BOOL g_bAscending = TRUE;
void *SortLlinkedList(	void *pList, 
							unsigned index,
							int	(*CompareFunc)(void *, void *, void *), 
							void *pointer, 
							unsigned long *pcount)
{
	TRACE( _T("+[SortLlinkedList]\r\n" )) ;

	unsigned base;
	unsigned long block_size;

	struct tape
	{
		struct TREEITEMLIST *first, *last;
		unsigned long count;
	} tape[4];

	// Distribute the records alternately to tape[0] and tape[1].

	tape[0].count = tape[1].count = 0L;
	tape[0].first = NULL;
	base = 0;
	while (pList != NULL)
	{
		TREEITEMLIST* next = ((TREEITEMLIST*)pList)->pNext;
		((TREEITEMLIST*)pList)->pNext = tape[base].first;
		tape[base].first = ((TREEITEMLIST *)pList);
		tape[base].count++;
		pList = next;
		base ^= 1;
	}

	// If the list is empty or contains only a single record, then 
	// tape[1].count == 0L and this part is vacuous.               

	for (base = 0, block_size = 1L; tape[base+1].count != 0L;
		base ^= 2, block_size <<= 1)
	{
		int dest;
		struct tape *tape0, *tape1;
		tape0 = tape + base;
		tape1 = tape + base + 1;
		dest = base ^ 2;
		tape[dest].count = tape[dest+1].count = 0;
		for (; tape0->count != 0; dest ^= 1)
		{
			unsigned long n0, n1;
			struct tape *output_tape = tape + dest;
			n0 = n1 = block_size;
			while (1)
			{
				struct TREEITEMLIST *chosen_record;
				struct tape *chosen_tape;
				if (n0 == 0 || tape0->count == 0)
				{
					if (n1 == 0 || tape1->count == 0)
						break;
					chosen_tape = tape1;
					n1--;
				}
				else if (n1 == 0 || tape1->count == 0)
				{
					chosen_tape = tape0;
					n0--;
				}	
				else if ((*CompareFunc)(tape0->first, tape1->first, pointer) > 0 )
				{
					chosen_tape = tape1;
					n1--;
				}
				else
				{
					chosen_tape = tape0;
					n0--;
				}

				chosen_tape->count--;
				chosen_record = chosen_tape->first;
				chosen_tape->first = chosen_record->pNext;
				if (output_tape->count == 0)
					output_tape->first = chosen_record;
				else
					output_tape->last->pNext = chosen_record;
				output_tape->last = chosen_record;
				output_tape->count++;
			}
		}
	}

	if (tape[base].count > 1L)
		tape[base].last->pNext = NULL;
	if (pcount != NULL)
		*pcount = tape[base].count;
	
	TRACE( _T("-[SortLlinkedList]\r\n" )) ;
	return tape[base].first;
}

int CompareListItem(void *ft1, void *ft2, void *pointer)
{
	TREEITEMLIST* p1;
	TREEITEMLIST* p2;

	int nRst = 0;

	p1 = (TREEITEMLIST*)ft1;
	p2 = (TREEITEMLIST*)ft2;

	if( p1 == NULL || p2 == NULL ) return 0;

	CString str1	= (p1)->pItem->GetItemText();
	CString str2	= (p2)->pItem->GetItemText();
	if( str1.IsEmpty() && str2.IsEmpty() ) return 0;
	if( str1.IsEmpty() ) return 1;
	if( str2.IsEmpty() ) return -1;	

	if( g_bAscending )
		nRst = str1.CompareNoCase(str2);
	else
		nRst = str2.CompareNoCase(str1);

	TRACE( _T("[CompareListItem] %s : %s ==> %d\r\n"), str1, str2, nRst);

	return nRst;
}

/////////////////////////////////////////////////////////////////////////////
// CTGGridCtrl
CTGGridCtrl::CTGGridCtrl()
{
	m_cxImage = m_cyImage = m_bIsDragging = m_CurSubItem = 0;
    m_nDragTarget=m_nDragItem = -1;
	m_psTreeLine.CreatePen(PS_SOLID, 1, RGB(192,192,192));
	m_psRectangle.CreatePen(PS_SOLID, 1, RGB(198,198,198));
	m_psPlusMinus.CreatePen(PS_SOLID, 1, RGB(0,0,0));
	m_brushErase.CreateSolidBrush(RGB(255,255,255));
	m_himl = NULL;
	m_nAddFunction = 0;
}


CTGGridCtrl::~CTGGridCtrl() {}

BEGIN_MESSAGE_MAP(CTGGridCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CTGGridCtrl)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk)
	ON_NOTIFY_REFLECT(LVN_ENDLABELEDIT, OnEndlabeledit)
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_TIMER()
	ON_WM_VSCROLL()
	ON_WM_LBUTTONUP()
	ON_NOTIFY_REFLECT(LVN_KEYDOWN, OnKeydown)
	ON_NOTIFY_REFLECT(LVN_BEGINDRAG, OnBegindrag)
	ON_WM_MEASUREITEM_REFLECT()
	ON_WM_DRAWITEM_REFLECT()
	ON_WM_SYSCOLORCHANGE()
	//}}AFX_MSG_MAP
	ON_WM_RBUTTONDOWN()
	ON_WM_CLOSE()

	ON_NOTIFY(HDN_ITEMCLICKA, 0, OnHeaderNotify) 
	ON_NOTIFY(HDN_ITEMCLICKW, 0, OnHeaderNotify)


END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTGGridCtrl message handlers

BOOL CTGGridCtrl::PreCreateWindow(CREATESTRUCT& cs) 
{
	cs.style |= LVS_REPORT | LVS_SINGLESEL | LVS_SHAREIMAGELISTS | LVS_OWNERDRAWFIXED | LVS_SHOWSELALWAYS;	
	return CListCtrl::PreCreateWindow(cs);
}

int CTGGridCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;
	return 0;
}


//--------------------------------------------------------------------------
//-- VIRTUAL FUCNTIONS
//-- 2008-06-30
//--------------------------------------------------------------------------
void CTGGridCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) {}

void CTGGridCtrl::DrawComboBox(CDC* pDC, CTreeItem *pSelItem, int nItem, int nColumn, int /*iSubIconOffset*/)
{
	CItemInfo* pInfo = GetData(pSelItem);
	CItemInfo::CONTROLTYPE ctrlType;
	if(pInfo->GetControlType(nColumn-1, ctrlType))
	{
		if(ctrlType == pInfo->combobox) 
		{
			CRect rect;
			GetSubItemRect(nItem, nColumn, LVIR_BOUNDS, rect);
			rect.left=rect.right - GetSystemMetrics(SM_CYVSCROLL);
			pDC->DrawFrameControl(rect, DFC_SCROLL, DFCS_SCROLLDOWN);
		}
	}
}


//this piece of code is borrowed from the wndproc.c file in the odlistvw.exe example from MSDN and was converted to mfc-style
void CTGGridCtrl::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    if (lpMeasureItemStruct->CtlType != ODT_LISTVIEW)
        return;

	TEXTMETRIC tm;
	CClientDC dc(this);	
	CFont* pFont = GetFont();
	CFont* pOldFont = dc.SelectObject(pFont);	
	dc.GetTextMetrics(&tm);
	int nItemHeight = tm.tmHeight + tm.tmExternalLeading;
	lpMeasureItemStruct->itemHeight = nItemHeight + 4; //or should I go for max(nItemheight+4, m_cxImage+2);
	dc.SelectObject(pOldFont);
}


LPCTSTR CTGGridCtrl::MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset)
{
	static const _TCHAR szThreeDots[]=_T("...");
	static CString csShort;

	int nStringLen=lstrlen(lpszLong);

	if(nStringLen==0 || pDC->GetTextExtent(lpszLong,nStringLen).cx + nOffset < nColumnLen+1)
		return(lpszLong);

	int nAddLen = pDC->GetTextExtent(szThreeDots,sizeof(szThreeDots)).cx;

	csShort = lpszLong;

	for(int i=nStringLen-1; i > 0; i--)
	{
		csShort = csShort.Left( csShort.GetLength() - 1 );
		if(pDC->GetTextExtent(csShort,i).cx + nOffset + nAddLen < nColumnLen)
			break;
	}
	csShort += szThreeDots;
	return csShort;
}

void CTGGridCtrl::OnKeydown(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;
	switch(pLVKeyDow->wVKey)
	{
		case VK_SPACE: 
			{
				if(GetExtendedStyle() & LVS_EX_CHECKBOXES)
				{	
					/* DEL++
					int nIndex = GetSelectedItem();
					if(nIndex !=-1)
					{
						CTreeItem* pItem = GetTreeItem(nIndex);
						if(pItem!=NULL)
						{
							CItemInfo *pInfo = GetData(pItem);
							pInfo->SetCheck(!pInfo->GetCheck());
						}
					}*/
					//SetCheckTopDown(GetSelectedItem());
		
				}
			}break;

		/* DEL++
		case VK_DELETE: 
			{
				int nItem = GetSelectedItem();
				if(nItem!=-1)
				{
					CTreeItem* pSelItem = GetTreeItem(nItem);
					if(pSelItem != NULL)
					{
						if(OnDeleteItem(pSelItem, nItem))
							DeleteItemEx(pSelItem, nItem);
					}	
				}
			}	break;*/

		case VK_MULTIPLY:
			{  
				int nIndex = GetSelectedItem();
				if(nIndex != -1)
				{
					CWaitCursor wait;
					SetRedraw(0);
					CTreeItem *pParent = GetTreeItem(nIndex);
					int nScroll=0;
					if(OnVKMultiply(pParent, nIndex))
					{	
						ExpandAll(pParent, nScroll);
					}		
					SetRedraw(1);
					RedrawItems(nIndex, nScroll);
					EnsureVisible(nScroll, TRUE);
				 }
			 }break;

		case VK_ADD:
			{
					int nIndex = GetSelectedItem();
					if(nIndex!=-1)
					{
						CWaitCursor wait;
						CTreeItem *pSelItem = GetTreeItem(nIndex);
						int nScrollIndex = 0;
						if(OnVKAdd(pSelItem, nIndex))
						{
							 nScrollIndex = Expand(pSelItem, nIndex);
						}
						/*CRect rc;
						GetItemRect(nIndex, rc, LVIR_BOUNDS);
						InvalidateRect(rc);
						UpdateWindow();
						EnsureVisible(nScrollIndex, 1);
						*/
						InvalidateItemRect(nIndex);
						EnsureVisible(nScrollIndex, 1);
					}

			}break;


		case VK_SUBTRACT:
			{
				int nIndex = GetSelectedItem();
				if(nIndex!=-1)
				{
					CWaitCursor wait;
					CTreeItem *pSelItem = GetTreeItem(nIndex);
					if(OnVkSubTract(pSelItem, nIndex))
					{
						Collapse(pSelItem);
					}
					/*CRect rc;
					GetItemRect(nIndex, rc, LVIR_BOUNDS);
					InvalidateRect(rc);
					UpdateWindow();
					*/
					InvalidateItemRect(nIndex);
				}
			}break;
		default :break;
	}
	*pResult = 0;
}




BOOL CTGGridCtrl::HitTestOnSign(CPoint point, LVHITTESTINFO& ht)
{
	ht.pt = point;
	// Test which subitem was clicked.
	SubItemHitTest(&ht);
	if(ht.iItem!=-1)
	{
		//first hittest on checkbox
		BOOL bHit = FALSE;
		if(GetExtendedStyle() & LVS_EX_CHECKBOXES)
		{
			if (ht.flags == LVHT_ONITEM && (GetStyle() & LVS_OWNERDRAWFIXED))//isn't this allways ownerdrawfixed :-)
			{
				//-----------------------------------------
				//-- 2008-06-19
				//-- CHANGE CLICK RANGE
				//-- DEBUGGING OK'
				//-----------------------------------------
				CRect rcIcon;
				GetItemRect(ht.iItem, rcIcon, LVIR_ICON);
				if ( point.x > (rcIcon.left  ) && 
					 point.x < (rcIcon.right ) )
					bHit = TRUE;
			}
			else if (ht.flags & LVHT_ONITEMSTATEICON)
				bHit = TRUE;
		}

		CTreeItem* pItem = GetTreeItem(ht.iItem);
		if(pItem!=NULL)
		{
			if(bHit)//if checkbox
			{
				//yes I know..have to maintain to sets of checkstates here...
				//one for listview statemask and one for CTreeItem..but its located here so no harm done
				/*SetCheck(ht.iItem,!GetCheck(ht.iItem));
				CItemInfo *pInfo = GetData(pItem);
				pInfo->SetCheck(!pInfo->GetCheck());*/

				// ADD++
				if( (GetExtendedStyle() & LVS_EX_CHECKBOXES) )
				{
					SetCheckTopDown(pItem, !(pItem->m_lpNodeInfo->GetCheck()));
					SetCheckDownUp(pItem);
				}
			}
			//if haschildren and clicked on + or - then expand/collapse
			if(ItemHasChildren(pItem))
			{
				//hittest on the plus/sign "button" 
				CRect rcBounds;
				GetItemRect(ht.iItem, rcBounds, LVIR_BOUNDS);
				CRectangle rect(this, NULL, GetIndent(pItem), rcBounds);
				BOOL bRedraw=0;//if OnItemExpanding or OnCollapsing returns false, dont redraw
				if(rect.HitTest(point))
				{
					SetRedraw(0);
					
					int nScrollIndex = GetCurIndex( pItem ); // AND NOT = 0 !!!
					if(IsCollapsed(pItem))
					{	
						if(OnItemExpanding(pItem, ht.iItem))
						{
							nScrollIndex = Expand(pItem, ht.iItem);
							OnItemExpanded(pItem, ht.iItem);
							bRedraw=1;
						}
					}	
					else {
					   if(OnCollapsing(pItem))
					   {
							Collapse(pItem);
							OnItemCollapsed(pItem);
							bRedraw=1;
					   }
					}
					SetRedraw(1);
					if(bRedraw)
					{
						/*CRect rc;
						GetItemRect(ht.iItem, rc, LVIR_BOUNDS);
						InvalidateRect(rc);
						UpdateWindow();
						EnsureVisible(nScrollIndex, 1);
						*/
						InvalidateItemRect(ht.iItem);
						EnsureVisible(nScrollIndex, 1);
						return 0;
					}	
				}
			}//has kids
		}//pItem!=NULL
	}
	return 1;
}



void CTGGridCtrl::OnDblclk(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if( GetFocus() != this) 
		SetFocus();

	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	LVHITTESTINFO ht;
	ht.pt = pNMListView->ptAction;
	SubItemHitTest(&ht);
	if(OnItemLButtonDown(ht))
	{
		NotifyDoubleClick();
		

		BOOL bSelect=1;
		bSelect = HitTestOnSign(pNMListView->ptAction, ht);
		//normal selection
		if(bSelect && ht.iItem !=-1)
		{		
			int nIndex = GetSelectedItem();
			if(nIndex!=-1)
			{
				CTreeItem *pSelItem = GetTreeItem(nIndex);
				if (pSelItem != NULL)
				{
					BOOL bRedraw=0;
					if(ItemHasChildren(pSelItem))
					{
						SetRedraw(0);
						int nScrollIndex=0;
						if(IsCollapsed(pSelItem))
						{		
							if(OnItemExpanding(pSelItem, nIndex))
							{
								nScrollIndex = Expand(pSelItem, nIndex);
								OnItemExpanded(pSelItem, nIndex);
								bRedraw=1;
							}
						}	
					
						else 
						{
						   if(OnCollapsing(pSelItem))
						   {
								Collapse(pSelItem);
								OnItemCollapsed(pSelItem);
								bRedraw=1;
						   }
						}
						SetRedraw(1);

						if(bRedraw)
						{
							/*CRect rc;
							GetItemRect(nIndex,rc,LVIR_BOUNDS);
							InvalidateRect(rc);
							UpdateWindow();
							EnsureVisible(nScrollIndex,1);*/

							InvalidateItemRect(nIndex);
							EnsureVisible(nScrollIndex,1);
						}
					}//ItemHasChildren	
				}//!=NULL
			}
		}
	}
	*pResult = 0;
}



void CTGGridCtrl::OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(pNMListView->iItem != -1)
	{
		m_nDragItem = pNMListView->iItem;
		CImageList* pDragImage=NULL;
		pDragImage = CreateDragImageEx(m_nDragItem);//override this if you want another dragimage or none at all.
		if(pDragImage)
		{
			pDragImage->BeginDrag(0, CPoint(0,0));
			pDragImage->DragEnter(this, pNMListView->ptAction);
			SetCapture();
			m_bIsDragging = TRUE;
		}
		delete pDragImage;
		pDragImage = NULL;
	}
	*pResult = 0;
}




//Create dragimage : Icon + the itemtext
CImageList *CTGGridCtrl::CreateDragImageEx(int nItem)
{
    CImageList *pList = new CImageList;          
	//get image index
	LV_ITEM lvItem;
	lvItem.mask =  LVIF_IMAGE;
	lvItem.iItem = nItem;
	lvItem.iSubItem = 0;
	GetItem(&lvItem);

	CRect rc;
	GetItemRect(nItem, &rc, LVIR_BOUNDS);         

	CString str;
	str = GetItemText(nItem, 0);
	CFont *pFont = GetFont();

	rc.OffsetRect(-rc.left, -rc.top);            
	rc.right = GetColumnWidth(0);                
	pList->Create(rc.Width(), rc.Height(),ILC_COLOR24| ILC_MASK , 1, 1);
	CDC *pDC = GetDC();                          
	if(pDC) 
	{
		CDC dc;	      
		dc.CreateCompatibleDC(pDC);      
		CBitmap bmpMap;
		bmpMap.CreateCompatibleBitmap(pDC, rc.Width(), rc.Height());

		CBitmap *pOldBmp = dc.SelectObject(&bmpMap);
		CFont *pOldFont = dc.SelectObject(pFont);
		dc.FillSolidRect(rc, GetSysColor(COLOR_WINDOW));
		CImageList *pImgList = GetImageList(LVSIL_SMALL);
		if(pImgList)
			pImgList->Draw(&dc, lvItem.iImage, CPoint(0,0), ILD_TRANSPARENT);
		dc.TextOut(m_cxImage + 4, 0, str);
		dc.SelectObject(pOldFont);
		dc.SelectObject(pOldBmp);                 
		//causes an error if the 1st column is hidden so must check the imagelist
		if(pList->m_hImageList != NULL)
			pList->Add(&bmpMap, RGB(255,255,255));
		else { 
			delete pList;
			pList=NULL;
		}
		ReleaseDC(pDC);   
	}   
	return pList;
}



void CTGGridCtrl::OnMouseMove(UINT nFlags, CPoint point) 
{
	//TRACE0("[CTGGridCtrl::OnMouseMove]\r\n");

    if(m_bIsDragging)
    {
		//TRACE0("[CTGGridCtrl::OnMouseMove] m_bIsDragging \r\n");

		KillTimer(1);
		if (CWnd::GetCapture() != this)
			m_bIsDragging=0;
		
		if(nFlags==MK_RBUTTON || nFlags==MK_MBUTTON)
			m_bIsDragging=0;

		
		if(GetKeyState(VK_ESCAPE) < 0)		
			m_bIsDragging=0;
		
		if(!m_bIsDragging)//why not put this in a funtion :)
		{
			SetItemState (m_nDragTarget, 0, LVIS_DROPHILITED);
			CImageList::DragLeave(this);
			CImageList::EndDrag();
			ReleaseCapture();
			InvalidateRect(NULL);
			UpdateWindow();
		}
		else
		{
			CPoint ptList(point);
			MapWindowPoints(this, &ptList, 1);
			CImageList::DragMove(ptList);
			UINT uHitTest = LVHT_ONITEM;
			m_nDragTarget = HitTest(ptList, &uHitTest);
			// try turn off hilight for previous DROPHILITED state
			int nPrev = GetNextItem(-1,LVNI_DROPHILITED);
			if(nPrev != m_nDragTarget)//prevents flicker 
				SetItemState(nPrev, 0, LVIS_DROPHILITED);

			CRect rect;
			GetClientRect (rect);
			int cy = rect.Height();
			if(m_nDragTarget!=-1)
			{
				SetItemState(m_nDragTarget, LVIS_DROPHILITED, LVIS_DROPHILITED);
				CTreeItem* pTarget = GetTreeItem(m_nDragTarget);
				if ((point.y >= 0 && point.y <= m_cyImage) || (point.y >= cy - m_cyImage && point.y <= cy) || 	
					( pTarget!=NULL && ItemHasChildren(pTarget) && IsCollapsed(pTarget)))
				{
					SetTimer(1,300,NULL);
				}
			}
		}
    }
	CListCtrl::OnMouseMove(nFlags, point);
}




void CTGGridCtrl::OnTimer(UINT nIDEvent) 
{
	CListCtrl::OnTimer(nIDEvent);
	if(nIDEvent==1)
	{
		if(CWnd::GetCapture()!=this)
		{
			SetItemState(m_nDragTarget, 0, LVIS_DROPHILITED);
			m_bIsDragging=0;
			CImageList::DragLeave(this);
			CImageList::EndDrag();
			ReleaseCapture();
			InvalidateRect(NULL);
			UpdateWindow();
			KillTimer(1);
			return;
		}

		SetTimer(1,300,NULL);//reset timer
		DWORD dwPos = ::GetMessagePos();
		CPoint ptList(LOWORD(dwPos),HIWORD(dwPos));
		ScreenToClient(&ptList);

		CRect rect;
		GetClientRect(rect);
		int cy = rect.Height();
		//
		// perform autoscroll if the cursor is near the top or bottom.
		//
		if (ptList.y >= 0 && ptList.y <= m_cyImage) 
		{
			int n = GetTopIndex(); 
			CImageList::DragShowNolock(0);
			SendMessage(WM_VSCROLL, MAKEWPARAM(SB_LINEUP, 0), NULL);
			CImageList::DragShowNolock(1);
			if (GetTopIndex()== n)
				KillTimer (1);
			else {
				CImageList::DragShowNolock(0);
				CImageList::DragMove(ptList);
				CImageList::DragShowNolock(1);
				return;
			}
		}
		else if (ptList.y >= cy - m_cyImage && ptList.y <= cy) 
		{
			int n = GetTopIndex(); 
			CImageList::DragShowNolock(0);
			SendMessage(WM_VSCROLL, MAKEWPARAM(SB_LINEDOWN, 0), NULL);
			CImageList::DragShowNolock(1);
			if (GetTopIndex()== n)
				KillTimer (1);
			else {
				CImageList::DragShowNolock(0);
				CImageList::DragMove(ptList);
				CImageList::DragShowNolock(1);
				return;
			}
		}
		//Hover test 
		CImageList::DragMove(ptList);
		UINT uHitTest = LVHT_ONITEM;
		m_nDragTarget = HitTest(ptList, &uHitTest);
	
		if(m_nDragTarget!=-1)
		{
			//if the target has children
			//expand them
			CTreeItem* pTarget=GetTreeItem(m_nDragTarget);
			if(pTarget != NULL && ItemHasChildren(pTarget) && IsCollapsed(pTarget) && (m_nDragItem!=-1))
			{
				CImageList::DragShowNolock(0);
				CTreeItem* pSource = GetTreeItem(m_nDragItem);
	
				SetRedraw(0);
				int nScrollIndex=0;
				if(ItemHasChildren(pTarget) && IsCollapsed(pTarget))
				{	
					if(OnItemExpanding(pTarget, m_nDragTarget))
					{	
						nScrollIndex = Expand(pTarget, m_nDragTarget);
						OnItemExpanded(pTarget, m_nDragTarget);
					}
				}		
				m_nDragItem = NodeToIndex(pSource);
				SetRedraw(1);
				EnsureVisible(nScrollIndex, 1);
				InvalidateRect(NULL);
				UpdateWindow();
				CImageList::DragShowNolock(1);
				KillTimer(1);
				return;
			}	
		}
		KillTimer(1);
	}
}


void CTGGridCtrl::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if(m_bIsDragging == TRUE)
		DragProc();
	else
		CListCtrl::OnLButtonUp(nFlags, point);
}

void CTGGridCtrl::DragProc()
{
	KillTimer(1);
    CImageList::DragLeave(this);
    CImageList::EndDrag();
    ReleaseCapture();
    m_bIsDragging = FALSE;
	SetItemState(m_nDragTarget, 0, LVIS_DROPHILITED);
    if((m_nDragTarget != -1) && (m_nDragTarget != m_nDragItem) && (m_nDragItem!=-1))//no drop on me self
    {
	
		CTreeItem* pSource = GetTreeItem(m_nDragItem);
		CTreeItem* pTarget = GetTreeItem(m_nDragTarget);
		if(IsRoot(pSource))
			return;

		//CTreeItem* pParent = GetParentItem(pSource);		
		//if(pParent==pTarget) //can't drag child to parent
		//	return;

		if(!IsChildOf(pSource, pTarget))//can't drag parent to child
		{
			CWaitCursor wait;
			SetRedraw(0);
			if(DoDragDrop(pTarget, pSource))
			{
				UINT uflag = LVIS_SELECTED | LVIS_FOCUSED;
				SetItemState(m_nDragTarget, uflag, uflag);
				m_nDragItem=-1;

				//delete source
				int nIndex = NodeToIndex(pSource);		
				if( nIndex >= 0 )
				{
					DeleteItem(nIndex);
					HideChildren(pSource, TRUE, nIndex);
				}
				Delete(pSource);
				InternaleUpdateTree();
				UpdateData();
			}else
				SetRedraw(1);
		}
	}
}

//used with the drag/drop operation
void CTGGridCtrl::CopyChildren(CTreeItem* pDest, CTreeItem* pSrc)
{
	if(!pDest)
		pDest = GetRootItem(0);

	if (ItemHasChildren(pSrc))
	{
		POSITION pos = pSrc->m_listChild.GetHeadPosition();
		while (pos != NULL)
		{
			CTreeItem* pItem = (CTreeItem *)pSrc->m_listChild.GetNext(pos);
			CItemInfo* lp = CopyData(GetData(pItem));
			CTreeItem* pNewItem = InsertItem(pDest, lp);
			if( pNewItem == NULL )
				return;

			CopyChildren(pNewItem, pItem);
		}
	}
}

BOOL CTGGridCtrl::DoDragDrop(CTreeItem* pTarget, CTreeItem* pSource)
{
	if( !(m_nAddFunction & PGS_DRAGNDROP)	||  pTarget==NULL	)
		return FALSE;

	if( FALSE == CheckDropPosition(pTarget, pSource)) 
		return FALSE;

	BOOL bUpdate=FALSE;
	if(!IsCollapsed(pTarget))
		bUpdate=TRUE; //children are expanded, want to see update right away
	
	//make a copy of the source data
	CItemInfo* pDragItemInfo = CopyData(GetData(pSource));
	//create new node with the source data and make pTarget the parent
	
	CTreeItem* pDragParent		= GetParentItem(pSource);		
	CTreeItem* pTargetParent	= GetParentItem(pTarget);

	CTreeItem* pNewParent = NULL;
	if( pDragParent == pTargetParent || pTarget->m_lpNodeInfo->IsEndNode())
	{
		POSITION pos = pTargetParent->m_listChild.Find(pTarget);

		if( pos ) 
			pNewParent = InsertItem( pTargetParent, pDragItemInfo, TRUE, ADD_INSERT, pos);
	}
	else
	{
		pNewParent = InsertItem( pTarget, pDragItemInfo, bUpdate, (pDragParent == pTarget ? ADD_HEAD : ADD_TAIL));				
	}

	if( pNewParent == NULL ) return FALSE;

	//if the source has children copy all source data and make the newly create item the parent
	if(ItemHasChildren(pSource))
		CopyChildren(pNewParent, pSource);
	

	return 1;
}


BOOL CTGGridCtrl::CheckDropPosition(CTreeItem* /*pTarget*/, CTreeItem* /*pSource*/)
{
	return TRUE;
}

void CTGGridCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	//its not meself
	if( GetFocus() != this) 
		SetFocus();
	
	CListCtrl::OnHScroll(nSBCode, nPos, pScrollBar);
}




void CTGGridCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if( GetFocus() != this) 
		SetFocus();
	CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
}




BOOL CTGGridCtrl::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(GetFocus()==this)
		{
			switch( pMsg->wParam )
			{
				case VK_LEFT:
					{
						// Decrement the order number.
						m_CurSubItem--;
						if(m_CurSubItem < 0) 
							m_CurSubItem = 0;
						else{
							
							CHeaderCtrl* pHeader = GetHeaderCtrl();
							// Make the column visible.
							// We have to take into account that the header may be reordered.
							MakeColumnVisible( Header_OrderToIndex( pHeader->m_hWnd, m_CurSubItem));
							// Invalidate the item.
							int iItem = GetSelectedItem();
							if( iItem != -1 )
							{
								/*CRect rcBounds;
								GetItemRect(iItem, rcBounds, LVIR_BOUNDS);
								InvalidateRect(&rcBounds);
								UpdateWindow();*/
								InvalidateItemRect(iItem);
							}
						}
					}
					return TRUE;

				case VK_RIGHT:
					{
						// Increment the order number.
						m_CurSubItem++;
						CHeaderCtrl* pHeader = GetHeaderCtrl();
						int nColumnCount = pHeader->GetItemCount();
						// Don't go beyond the last column.
						if( m_CurSubItem > nColumnCount -1 ) 
							m_CurSubItem = nColumnCount-1;
						else
						{
							MakeColumnVisible(Header_OrderToIndex( pHeader->m_hWnd, m_CurSubItem));
							 
							int iItem = GetSelectedItem();
							// Invalidate the item.
							if( iItem != -1 )
							{
								/*CRect rcBounds;
								GetItemRect(iItem, rcBounds, LVIR_BOUNDS);
								InvalidateRect(&rcBounds);
								UpdateWindow();*/

								InvalidateItemRect(iItem);
							}
						}
					}
					return TRUE;

				case VK_RETURN://edit itemdata
					{
						BOOL bResult = OnVkReturn();
						if(!bResult)
						{
							int iItem = GetSelectedItem();
							if( m_CurSubItem != -1 && iItem != -1)
							{
								CHeaderCtrl* pHeader = GetHeaderCtrl();
								int iSubItem = Header_OrderToIndex(pHeader->m_hWnd, m_CurSubItem);
								if(iSubItem==0)//that's just me saying all nodes in col 0 are edit-controls, you may modify this
								{
									CRect rcItem;
									GetItemRect(iItem, rcItem, LVIR_LABEL);
									DWORD dwStyle = WS_BORDER | WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL|ES_LEFT;
									CEdit *pEdit = new CTGListEditCtrl(iItem, iSubItem, GetItemText(iItem, iSubItem));
									pEdit->Create(dwStyle, rcItem, this, 0x1233);	
								}
								else
									EditLabelEx(iItem, iSubItem);	
								return 1;
							}
						}
					}
					break;
				default:
					break;
			}
		}
	}
	return CListCtrl::PreTranslateMessage(pMsg);
}



#define IDC_EDITCTRL 0x1234
CEdit* CTGGridCtrl::EditLabelEx(int nItem, int nCol)
{
	CRect rect;
	int offset = 0;
	if(!EnsureVisible(nItem, TRUE)) 
		return NULL;
	GetSubItemRect(nItem, nCol, LVIR_BOUNDS, rect);
	// Now scroll if we need to expose the column
	CRect rcClient;
	GetClientRect(rcClient);
	if( offset + rect.left < 0 || offset + rect.left > rcClient.right )
	{
		CSize size(offset + rect.left,0);		
		Scroll(size);
		rect.left -= size.cx;
	}
	rect.left += offset;	
	rect.right = rect.left + GetColumnWidth(nCol);
	if(rect.right > rcClient.right) 
	   rect.right = rcClient.right;

	// Get Column alignment	
	LV_COLUMN lvcol;
	lvcol.mask = LVCF_FMT;
	GetColumn(nCol, &lvcol);

	DWORD dwStyle;
	if((lvcol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)
		dwStyle = ES_LEFT;
	else if((lvcol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT)
		dwStyle = ES_RIGHT;
	else 
		dwStyle = ES_CENTER;	

	//YOU MAY WANNA COMMENT THIS OUT,YOU DECIDE ..
	//now you could take into account here, that an subitem might have an icon
	CTreeItem *p = GetTreeItem(nItem);
	if(p!=NULL)
	{
		CItemInfo *pInfo = GetData(p);
		if(pInfo!=NULL)
		{
			int iSubImage = pInfo->GetSubItemImage(nCol-1); 
			if(iSubImage!=-1)
			{
				//m_cxImage is actually the width of the "tree" imagelist not your subitem imagelist..
				//remember that, when you create your bitmap, I was to lazy getting the icon size of the subitem imagelist
				rect.left+=m_cxImage;
			}
		}
	}
	//////////////////////////////////
	
	dwStyle |=WS_BORDER|WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL|ES_NUMBER;
	CEdit *pEdit = new CTGListEditCtrl(nItem, nCol, GetItemText(nItem, nCol));
	pEdit->Create(dwStyle, rect, this, IDC_EDITCTRL);	
	//pEdit->ModifyStyleEx(0,WS_EX_CLIENTEDGE); //funny thing happend here, uncomment this, 
												//and then edit an item, 
												//enter a long text so that the ES_AUTOHSCROLL comes to rescue
												//yes that's look funny, ???.
	return pEdit;
}




void CTGGridCtrl::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO *plvDispInfo = (LV_DISPINFO*)pNMHDR;
 	LV_ITEM *plvItem = &plvDispInfo->item;
	if (plvItem->pszText != NULL)//valid text
	{
		if(plvItem->iItem != -1) //valid item
		{
			CTreeItem*pSelItem = GetTreeItem(plvItem->iItem);
			if(pSelItem != NULL)
			{
				OnUpdateListViewItem(pSelItem, plvItem);
			}
		}
	}
	*pResult = 0;
}



int CTGGridCtrl::GetNumCol()
{
	CHeaderCtrl* pHeader = GetHeaderCtrl();
	return pHeader ? pHeader->GetItemCount() : 0;
}



//Think Rex Myer is spooking here
void CTGGridCtrl::MakeColumnVisible(int nCol)
{
	if(nCol < 0)
		return;
	// Get the order array to total the column offset.
	CHeaderCtrl* pHeader = GetHeaderCtrl();

	int nColCount = pHeader->GetItemCount();
	ASSERT( nCol < nColCount);
	int *pOrderarray = new int[nColCount];
	Header_GetOrderArray(pHeader->m_hWnd, nColCount, pOrderarray);
	// Get the column offset
	int offset = 0;
	for(int i = 0; pOrderarray[i] != nCol; i++)
		offset += GetColumnWidth(pOrderarray[i]);

	int colwidth = GetColumnWidth(nCol);
	delete[] pOrderarray;

	CRect rect;
	GetItemRect(0, &rect, LVIR_BOUNDS);
	// Now scroll if we need to show the column
	CRect rcClient;
	GetClientRect(&rcClient);
	if(offset + rect.left < 0 || offset + colwidth + rect.left > rcClient.right)
	{
		CSize size(offset + rect.left,0);
		Scroll(size);
		InvalidateRect(NULL);
		UpdateWindow();
	}
}



//Think Rex Myer is spooking here
int CTGGridCtrl::IndexToOrder( int iIndex )
{
	// This translates a column index value to a column order value.
	CHeaderCtrl* pHeader = GetHeaderCtrl();
	int nColCount = pHeader->GetItemCount();
	int *pOrderarray = new int[nColCount];
	Header_GetOrderArray(pHeader->m_hWnd, nColCount, pOrderarray);
	for(int i=0; i<nColCount; i++)
	{
		if(pOrderarray[i] == iIndex )
		{
			delete[] pOrderarray;
			return i;
		}
	}
	delete[] pOrderarray;
	return -1;
}



void CTGGridCtrl::DrawFocusCell(CDC *pDC, int nItem, int iSubItem,int nIconOffset)
{
	if(iSubItem==m_CurSubItem)
	{
		CRect rect;
		GetSubItemRect(nItem, iSubItem, LVIR_BOUNDS, rect);
		CBrush br(GetCellRGB());
		if(iSubItem==0)
			GetItemRect(iSubItem, rect, LVIR_LABEL);
		rect.left+=nIconOffset;
		pDC->FillRect(rect, &br);
		pDC->DrawFocusRect(rect);
	}
}


//insert item and return new parent pointer.
CTGGridCtrl::CTreeItem* CTGGridCtrl::InsertItem(CTreeItem *pParent, CItemInfo* lpInfo,  BOOL bUpdate, short nAddType, POSITION pos)
{
	if( pParent==NULL )
		return NULL;

	if(lpInfo==NULL)		
		lpInfo = new CItemInfo;

	CTreeItem *pItem = new CTreeItem();
	UpdateData(pItem, lpInfo);


	SetIndent(pItem, GetIndent(pParent)+1);
	SetParentItem(pItem, pParent);
	
	switch(nAddType)
	{
	case ADD_HEAD:
		pParent->m_listChild.AddHead(pItem);
		break;

	case ADD_INSERT:
		{	
			pParent->m_listChild.InsertBefore(pos, pItem);
		}
		break;

	default:// ADD_TAIL:
		pParent->m_listChild.AddTail(pItem);
		break;
	}
	

	if(!bUpdate)
		 Hide(pParent, TRUE);	
	else
	{
		//calc listview index for the new node
		int nIndex = NodeToIndex(pItem);			
		CString str = GetData(pItem)->GetItemText();
		LV_ITEM     lvItem;
		lvItem.mask = LVIF_TEXT | LVIF_INDENT | LVIF_PARAM;
		lvItem.pszText = str.GetBuffer(1); 
		//insert item
		lvItem.iItem = nIndex;
		lvItem.iSubItem = 0;
		lvItem.lParam = (LPARAM)pItem;
		lvItem.iIndent = GetIndent(pItem);
		CListCtrl::InsertItem(&lvItem);
		if(lpInfo->GetCheck())
			SetCheck(nIndex);

		//Get subitems
		int nSize = GetData(pItem)->GetItemCount();
		for(int i=0; i < nSize;i++)
		{
		   CString str = GetData(pItem)->GetSubItem(i);
		   lvItem.mask = LVIF_TEXT;
		   lvItem.iSubItem = i+1;
		   lvItem.pszText = str.GetBuffer(1);
		   SetItem(&lvItem);
		}
		str.ReleaseBuffer();
		InternaleUpdateTree();//better do this
	}
	return pItem;
}	



void CTGGridCtrl::InternaleUpdateTree()
{
	int nItems = GetItemCount();
	for(int nItem=0; nItem < nItems; nItem++)
	{
		CTreeItem* pItem = GetTreeItem(nItem);
		if(pItem)
			SetCurIndex(pItem, nItem);
	}
}


int CTGGridCtrl::NodeToIndex(CTreeItem *pNode)
{
	int nStartIndex=0;
	POSITION pos = m_RootItems.GetHeadPosition();
	while(pos!=NULL)
	{
		CTreeItem * root = (CTreeItem*)m_RootItems.GetNext(pos);
		int ret = _NodeToIndex(root, pNode, nStartIndex);
		if(ret != -1)
			return ret;
	}
	return -1;
}

CTGGridCtrl::CTreeItem* CTGGridCtrl::GetRootItem(int nIndex)
{
	POSITION pos = m_RootItems.FindIndex(nIndex);
	if(pos==NULL)
		return NULL;
	return (CTreeItem*)m_RootItems.GetAt(pos);
}



int CTGGridCtrl::GetRootIndex(CTreeItem * root)
{
	int nIndex = 0;
	POSITION pos = m_RootItems.GetHeadPosition();
	while(pos != NULL)
	{
		CTreeItem * pItem = (CTreeItem*)m_RootItems.GetNext(pos);
		if(pItem== root)
			return nIndex;
		nIndex++;
	}
	return -1;
}



BOOL CTGGridCtrl::IsRoot(CTreeItem * lpItem)
{
	return m_RootItems.Find(lpItem) != NULL;
}


void CTGGridCtrl::DeleteRootItem(CTreeItem * root)
{
	TRACE( _T("[CTGGridCtrl::DeleteRootItem]\r\n" )) ;

	POSITION pos = m_RootItems.Find(root);
	if(pos!=NULL)
	{
		CTreeItem* pRoot=(CTreeItem*)m_RootItems.GetAt(pos);
		if(pRoot->m_lpNodeInfo!=NULL)
		{
			delete pRoot->m_lpNodeInfo;
			pRoot->m_lpNodeInfo = NULL;
		}
		delete pRoot;
		pRoot = NULL;
		m_RootItems.RemoveAt(pos);
	}
}

CTGGridCtrl::CTreeItem*  CTGGridCtrl::InsertRootItem(CItemInfo * lpInfo, short nAddType)
{
	TRACE( _T("+[CTGGridCtrl::InsertRootItem]\r\n" )) ;
	if(lpInfo==NULL)
		lpInfo = new CItemInfo;

	CTreeItem* pRoot = NULL;	
	pRoot =  new CTreeItem();

	CleanMe(pRoot);
	UpdateData(pRoot, lpInfo);

	SetIndent(pRoot, 1);
	SetCurIndex(pRoot, GetItemCount());
	SetParentItem(pRoot, NULL);

	CItemInfo* lp = GetData(pRoot);
	LV_ITEM lvItem;		
	lvItem.mask = LVIF_TEXT | LVIF_INDENT | LVIF_PARAM;
	CString strItem = lp->GetItemText();
	lvItem.pszText = strItem.GetBuffer(1); 
	lvItem.iItem = GetItemCount();
	lvItem.lParam = (LPARAM)pRoot;
	lvItem.iIndent = 1;
	lvItem.iSubItem = 0;
	strItem.ReleaseBuffer();

	CListCtrl::InsertItem(&lvItem);	
	
	int nSize = lp->GetItemCount();
	for(int i=0; i < nSize;i++)
	{
	   CString str = lp->GetSubItem(i);
	   lvItem.mask = LVIF_TEXT;
	   lvItem.iSubItem = i+1;
	   lvItem.pszText = str.GetBuffer(1);
	   str.ReleaseBuffer();
	   SetItem(&lvItem);
	}

	if( nAddType == ADD_TAIL )
		m_RootItems.AddTail(pRoot);

	else if( nAddType == ADD_INSERT )
	{			
		POSITION pos = m_RootItems.GetTailPosition();
		CTreeItem* pItem = (CTreeItem*)m_RootItems.GetAt(pos);

		if( pItem && pItem->m_lpNodeInfo->GetItemText() == TGCC_NOT_REGISTERD )
			m_RootItems.InsertBefore(pos, pRoot);
		else
			m_RootItems.AddTail(pRoot);
	}


#ifdef _DEBUG
	if( nAddType == ADD_INSERT )
	{
		int i = 0;
		POSITION pos = m_RootItems.GetHeadPosition();
		while(pos)
		{
			CTreeItem * pItem = (CTreeItem*)m_RootItems.GetNext(pos);
			if( pItem )
				TRACE( _T("\r\n m_RootItems(%d)=%s\r\n"), i++ , pItem->m_lpNodeInfo->GetItemText());	
		}
	}
#endif

	TRACE( _T("-[CTGGridCtrl::InsertRootItem]\r\n" )) ;

	return pRoot;
}



void CTGGridCtrl::DrawTreeItem(CDC* pDC, CTreeItem* pSelItem, int nListItem, const CRect& rcBounds)
{
	int nColWidth = GetColumnWidth(0);
	int yDown = rcBounds.top;
    CPen* pPenTreeLine = pDC->SelectObject(&m_psTreeLine);
	int iIndent = GetIndent(pSelItem);
	int nHalfImage = (m_cxImage >> 1);
	int nBottomDown = yDown + nHalfImage + ((rcBounds.Height() - m_cyImage) >> 1);
	//
	BOOL bChild = ItemHasChildren(pSelItem);
	BOOL bCollapsed = IsCollapsed(pSelItem);
	//draw outline	
	while(1)
	{
		CTreeItem* pParent = GetParentItem(pSelItem);
		if(pParent==NULL)//no more parents, stop
			break;

		POSITION pos = pParent->m_listChild.GetTailPosition();
		while(pos!=NULL)
		{
			CTreeItem *pLastChild = (CTreeItem*)pParent->m_listChild.GetPrev(pos);
			int nIndex = GetCurIndex(pLastChild);
			int nCurIndent = GetIndent(pLastChild);
			if(nListItem > nIndex && iIndent > nCurIndent)//no need to go further in this loop
				break;

			//no drawing outside the 1st columns right
			int xLine =  rcBounds.left + nCurIndent * m_cxImage - nHalfImage;
			if(nIndex == nListItem && nCurIndent==iIndent)
			{
				//draw '-
				int x;
				pDC->MoveTo(xLine, yDown);
				pDC->LineTo(xLine, nBottomDown);
				// -
				xLine + nHalfImage > nColWidth ? x = nColWidth: x = xLine + nHalfImage;
				
				pDC->MoveTo(xLine, nBottomDown);
				pDC->LineTo(x, nBottomDown);
				break;
			}
			else
			if(nIndex > nListItem && nCurIndent==iIndent)
			{
				//draw |-
				int x;
				xLine + nHalfImage > nColWidth ? x = nColWidth : x = xLine + nHalfImage;
				pDC->MoveTo(xLine, nBottomDown);
				pDC->LineTo(x, nBottomDown);
				//-
				pDC->MoveTo(xLine, yDown);
				pDC->LineTo(xLine, rcBounds.bottom);
				break;
			}
			else
			if(nIndex > nListItem && nCurIndent < iIndent)
			{
				//draw |
				pDC->MoveTo(xLine, yDown);
				pDC->LineTo(xLine, rcBounds.bottom);
				break;
			}
		}			
		pSelItem = pParent;//next
	}

	//draw plus/minus sign
	if(bChild)
	{
		CRectangle rect(this, pDC, iIndent, rcBounds);

		rect.DrawRectangle(this);

		CPen* pPenPlusMinus = pDC->SelectObject(&m_psPlusMinus);
		if(bCollapsed)
			rect.DrawPlus();
		else {
			rect.DrawMinus();
			//draw line up to parent folder
			CPen* pLine = pDC->SelectObject(&m_psTreeLine);
			int nOffset = (rcBounds.Height() - m_cyImage)/2;
			pDC->MoveTo(rect.GetLeft()+ m_cxImage, rcBounds.top + m_cyImage+nOffset);
			pDC->LineTo(rect.GetLeft() + m_cxImage, rcBounds.bottom);
			pDC->SelectObject(pLine);		
		}
		pDC->SelectObject(pPenPlusMinus);		
	}
	pDC->SelectObject(pPenTreeLine);
}



//walk all over the place setting the hide/show flag of the nodes.
//it also deletes items from the listviewctrl.
void CTGGridCtrl::HideChildren(CTreeItem *pItem, BOOL bHide,int nItem)
{
	if(!IsCollapsed(pItem))
	if(ItemHasChildren(pItem))
	{
		Hide(pItem, bHide);
		POSITION pos = pItem->m_listChild.GetHeadPosition();
		while (pos != NULL)
		{
			HideChildren((CTreeItem *)pItem->m_listChild.GetNext(pos),bHide,nItem+1);
			DeleteItem(nItem);
		}
	}
}

void CTGGridCtrl::Collapse(CTreeItem *pItem)
{
	if(pItem != NULL && ItemHasChildren(pItem))
	{
		SetRedraw(0);
		int nIndex = NodeToIndex(pItem);			
		HideChildren(pItem, TRUE, nIndex+1);
		InternaleUpdateTree();
		SetRedraw(1);
	}
}


//-----------------------------------------------------------
//--
//-- 2008-06-14
//--
//-----------------------------------------------------------

void CTGGridCtrl::ExpandAll()
{
	int nScroll;
	POSITION pos = m_RootItems.GetHeadPosition();
	while(pos!=NULL)
	{
		CTreeItem *pRoot = (CTreeItem*)m_RootItems.GetNext(pos);
		ExpandAll(pRoot, nScroll);			
	}
}


void CTGGridCtrl::ExpandAll(CTreeItem *pItem, int& nScroll)
{
	const int nChildren = pItem->m_listChild.GetCount();
	if (nChildren > 0)
	{
		int nIndex = NodeToIndex(pItem);
		nScroll = Expand(pItem, nIndex);
	}

	POSITION pos = pItem->m_listChild.GetHeadPosition();
	while (pos)
	{
		CTreeItem *pChild = (CTreeItem*)pItem->m_listChild.GetNext(pos);
		ExpandAll(pChild, nScroll);
	}	
}

int CTGGridCtrl::Expand(CTreeItem* pSelItem, int nIndex)
{
	if(ItemHasChildren(pSelItem) && IsCollapsed(pSelItem))
	{
		LV_ITEM lvItem;
		lvItem.mask = LVIF_INDENT;
		lvItem.iItem = nIndex;
		lvItem.iSubItem = 0;
		lvItem.lParam=(LPARAM)pSelItem;
		lvItem.iIndent = GetIndent(pSelItem);
		SetItem(&lvItem);

		Hide(pSelItem, FALSE);
		//expand children
		POSITION pos = pSelItem->m_listChild.GetHeadPosition();
		while(pos != NULL)
		{
			CTreeItem* pNextNode = (CTreeItem*)pSelItem->m_listChild.GetNext(pos);
			CString str = GetData(pNextNode)->GetItemText();
			LV_ITEM lvItem;
			lvItem.mask = LVIF_TEXT | LVIF_INDENT | LVIF_PARAM;
			lvItem.pszText =str.GetBuffer(1); 
			str.ReleaseBuffer();
			lvItem.iItem = nIndex + 1;
			lvItem.iSubItem = 0;
			lvItem.lParam=(LPARAM)pNextNode;
			lvItem.iIndent = GetIndent(pSelItem)+1;
			CListCtrl::InsertItem(&lvItem);
			if(GetData(pNextNode)->GetCheck())
			{
				SetCheck(nIndex + 1);
				//-- 2012-03-09 hongsu@esmlab.com
				//-- TRACE( _T("Expand>>>>>>>>  %d  \n"), (nIndex + 1) );
			}
			//get subitems
			int nSize = GetData(pNextNode)->GetItemCount();
			for(int i=0; i< nSize;i++)
			{
			   CString str=GetData(pNextNode)->GetSubItem(i);
			   lvItem.mask = LVIF_TEXT;
			   lvItem.iSubItem = i+1;
			   lvItem.pszText=str.GetBuffer(1);
			   SetItem(&lvItem);
			}
			str.ReleaseBuffer();
			nIndex++;
		}
	}
	InternaleUpdateTree();
	return nIndex;
}


int CTGGridCtrl::SelectNode(CTreeItem *pLocateNode)
{
	//TRACE( _T("[CTGGridCtrl::SelectNode]\r\n" )) ;

	if(IsRoot(pLocateNode))
	{
		UINT uflag = LVIS_SELECTED | LVIS_FOCUSED;
		SetItemState(0, uflag, uflag);
		return 0;
	}
	int nSelectedItem=-1;
	CTreeItem* pNode = pLocateNode;
	CTreeItem* pTopLevelParent=NULL;
	//Get top parent
	while(1)
	{
		CTreeItem *pParent = GetParentItem(pLocateNode);
		if(IsRoot(pParent))
			break;
		pLocateNode = pParent;
	}
	pTopLevelParent = pLocateNode;//on top of all
	//Expand the folder
	if(pTopLevelParent != NULL)
	{
		SetRedraw(0);
		CWaitCursor wait;
		CTreeItem *pRoot = GetParentItem(pTopLevelParent);

		if(IsCollapsed(pRoot))
			Expand(pRoot,0);

		ExpandUntil(pTopLevelParent, pNode);

		UINT uflag = LVIS_SELECTED | LVIS_FOCUSED;
		nSelectedItem = NodeToIndex(pNode);

		SetItemState(nSelectedItem, uflag, uflag);

		SetRedraw(1);
		EnsureVisible(nSelectedItem, TRUE);
	}
	return nSelectedItem;
}

void CTGGridCtrl::ExpandUntil(CTreeItem *pItem, CTreeItem* pStopAt)
{
	//TRACE( _T("[CTGGridCtrl::ExpandUntil]\r\n" )) ;

	const int nChildren = pItem->m_listChild.GetCount();
	if (nChildren > 0)
	{
		POSITION pos = pItem->m_listChild.GetHeadPosition();
		while (pos)
		{
			CTreeItem *pChild = (CTreeItem*)pItem->m_listChild.GetNext(pos);
			if(pChild == pStopAt)	
			{
				int nSize = GetIndent(pChild);
				CTreeItem** ppParentArray = new CTreeItem*[nSize];
				int i=0;
				while(1)
				{
					CTreeItem *pParent = GetParentItem(pChild);
					
					if(IsRoot(pParent))
						break;
					pChild = pParent;
					ppParentArray[i] = pChild;
					i++;
				}

				for(int x=i; x > 0; x--)
				{
					CTreeItem *pParent = ppParentArray[x-1];
					Expand(pParent, NodeToIndex(pParent));
				}
				delete [] ppParentArray;
				return;
			}
		}
	}

	POSITION pos = pItem->m_listChild.GetHeadPosition();
	while (pos)
	{
		CTreeItem *pChild = (CTreeItem*)pItem->m_listChild.GetNext(pos);
		ExpandUntil(pChild, pStopAt);
	}
	
}

void CTGGridCtrl::DeleteItemEx(CTreeItem *pSelItem, int nItem)
{
	TRACE( _T("[CTGGridCtrl::DeleteItemEx]\r\n" )) ;

	SetRedraw(0);	
	DeleteItem(nItem);
	HideChildren(pSelItem, TRUE, nItem);
	// delete all internal nodes	
	if(GetParentItem(pSelItem) == NULL )
		DeleteRootItem(pSelItem);
	else
		Delete(pSelItem);

	InternaleUpdateTree();
	if(nItem-1<0)//no more items in list
	{
		UpdateData();
		return;
	}

	UINT uflag = LVIS_SELECTED | LVIS_FOCUSED;
	CRect rcTestIfItemIsValidToSelectOtherWiseSubtrackOneFromItem;//just to get the documention right :)
	GetItemRect(nItem, rcTestIfItemIsValidToSelectOtherWiseSubtrackOneFromItem ,LVIR_LABEL) ? SetItemState(nItem, uflag, uflag) : SetItemState(nItem-1, uflag, uflag);
	
	UpdateData();
}


void CTGGridCtrl::CleanMe(CTreeItem *pItem)
{
	TRACE( _T("[CTGGridCtrl::CleanMe]\r\n" )) ;

	//  delete child nodes
	POSITION pos = pItem->m_listChild.GetHeadPosition();
	while (pos != NULL)
	{
		CTreeItem* pItemData = (CTreeItem*)pItem->m_listChild.GetNext(pos);
		if(pItemData!=NULL)
		{
			if(pItemData->m_lpNodeInfo!=NULL)
			{
				delete pItemData->m_lpNodeInfo;
				pItemData->m_lpNodeInfo = NULL;
			}
			pItemData->m_listChild.RemoveAll();
			delete pItemData;
			pItemData = NULL;
		}
	}
	pItem->m_listChild.RemoveAll();
}




CTGGridCtrl::CTreeItem* CTGGridCtrl::GetNext(CTreeItem* pStartAt, CTreeItem* pNode, BOOL bInit, BOOL bDontIncludeHidden)
{
	static BOOL bFound;
	if (bInit)
		bFound = FALSE;
		
	if (pNode == pStartAt)
		bFound = TRUE;

	if(bDontIncludeHidden)
	{
		if (!IsCollapsed(pStartAt))
		{
			POSITION pos = pStartAt->m_listChild.GetHeadPosition();
			while (pos != NULL)
			{
				CTreeItem* pChild = (CTreeItem*)pStartAt->m_listChild.GetNext(pos);
				if (bFound)
					return pChild;
				pChild = GetNext(pChild, pNode, FALSE, TRUE);
				if (pChild != NULL)
					return pChild;
			}
		}
	}
	else {
			POSITION pos = pStartAt->m_listChild.GetHeadPosition();
			while (pos != NULL)
			{
				CTreeItem* pChild = (CTreeItem*)pStartAt->m_listChild.GetNext(pos);
				if (bFound)
					return pChild;
				pChild = GetNext(pChild, pNode, FALSE,FALSE);
				if (pChild != NULL)
					return pChild;
			}
	}
	// if reached top and last level return original
	if (bInit)
		return pNode;
	else
		return NULL;
}



CTGGridCtrl::CTreeItem* CTGGridCtrl::GetPrev(CTreeItem* pStartAt, CTreeItem* pNode, BOOL bInit, BOOL bDontIncludeHidden)
{
	static CTreeItem* pPrev;
	if (bInit)
		pPrev = pStartAt;

	if (pNode == pStartAt)
		return pPrev;

	pPrev = pStartAt;

	if(bDontIncludeHidden)
	{
		if (!IsCollapsed(pStartAt))
		{
			POSITION pos = pStartAt->m_listChild.GetHeadPosition();
			while (pos != NULL)
			{
				CTreeItem* pCur = (CTreeItem*)pStartAt->m_listChild.GetNext(pos);
				CTreeItem* pChild = GetPrev(pCur,pNode, FALSE,TRUE);
				if (pChild != NULL)
					return pChild;
			}
		}
	}
	else {
		POSITION pos = pStartAt->m_listChild.GetHeadPosition();
		while (pos != NULL)
		{
			CTreeItem* pCur = (CTreeItem*)pStartAt->m_listChild.GetNext(pos);
			CTreeItem* pChild = GetPrev(pCur,pNode, FALSE,FALSE);
			if (pChild != NULL)
				return pChild;
		}
	}

	if (bInit)
		return pPrev;
	else
		return NULL;
}


int CTGGridCtrl::_NodeToIndex(CTreeItem *pStartpos, CTreeItem* pNode, int& nIndex, BOOL binit)
{
	static BOOL bFound;	
	// Account for other root nodes
	if(GetParentItem(pStartpos) == NULL && GetRootIndex(pStartpos) !=0)
		nIndex++;

	if(binit)
		bFound=FALSE;

	if(pStartpos==pNode)
		bFound=TRUE;

	if(!IsCollapsed(pStartpos))
	{
		POSITION pos = GetHeadPosition(pStartpos);
		while (pos)
		{
			CTreeItem *pChild = GetNextChild(pStartpos, pos);
			if(bFound)
				return nIndex;

//	Craig Schmidt: Cannot set nIndex as return value.  Worked find with single root but
//				   the calling function get confused since the return value may indicate
//				   that the next root needs to be searched.  Didn'd spend much time on
//				   this so there is probably a better way of doing this.
//			nIndex = _NodeToIndex(pChild, pNode, nIndex, binit);
			_NodeToIndex(pChild, pNode, nIndex, binit);
			nIndex++;
		}
	}
	if(binit && bFound)
		return nIndex;
	else
		return -1;
}


BOOL CTGGridCtrl::Delete(CTreeItem* pNode, BOOL bClean)
{
	POSITION pos = m_RootItems.GetHeadPosition();
	while(pos!=NULL)
	{
		CTreeItem * pRoot = (CTreeItem*)m_RootItems.GetNext(pos);
		if(_Delete(pRoot, pNode, bClean))
			return TRUE;
	}
	return FALSE;
}



BOOL CTGGridCtrl::_Delete(CTreeItem* pStartAt, CTreeItem* pNode, BOOL bClean)
{
	POSITION pos = pStartAt->m_listChild.GetHeadPosition();
	while (pos != NULL)
	{
		POSITION posPrev = pos;
		CTreeItem *pChild = (CTreeItem*)pStartAt->m_listChild.GetNext(pos);
		if (pChild == pNode)
		{
			pStartAt->m_listChild.RemoveAt(posPrev);
			if(bClean)
			{
				CItemInfo* pInfo = GetData(pNode);
				if(pInfo!=NULL)
				{
					delete pInfo;
					pInfo = NULL;
				}
				delete pNode;
				pNode = NULL;
			}
			return TRUE;
		}
		if (_Delete(pChild, pNode) == TRUE)
			return TRUE;
	}
	return FALSE;
}




BOOL CTGGridCtrl::IsChildOf(const CTreeItem* pParent, const CTreeItem* pChild) const
{
	if (pChild == pParent)
		return TRUE;
	POSITION pos = pParent->m_listChild.GetHeadPosition();
	while (pos != NULL)
	{
		CTreeItem* pNode = (CTreeItem*)pParent->m_listChild.GetNext(pos);
		if (IsChildOf(pNode, pChild))
			return TRUE;
	}
	return FALSE;
}


void CTGGridCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if( GetFocus() != this) 
		SetFocus();

	LVHITTESTINFO ht;
	ht.pt = point;
	
	SubItemHitTest(&ht);

	if(OnItemLButtonDown(ht))
	{
		BOOL bSelect=1;
		bSelect = HitTestOnSign(point, ht);
	
		if(bSelect && ht.iItem!=-1)
		{
			m_CurSubItem = IndexToOrder(ht.iSubItem);
			CHeaderCtrl* pHeader = GetHeaderCtrl();

			// Make the column fully visible.
			MakeColumnVisible(Header_OrderToIndex(pHeader->m_hWnd, m_CurSubItem));
			CListCtrl::OnLButtonDown(nFlags, point);
			OnControlLButtonDown(nFlags, point, ht);

			//update row anyway for selection bar
			InvalidateItemRect(ht.iItem);
		}
	}
}



void CTGGridCtrl::OnUpdateListViewItem(CTreeItem* lpItem, LV_ITEM *plvItem)
{
	//default implementation you would go for this 9 out of 10 times
	CItemInfo *lp = GetData(lpItem);
	CString str = (CString)plvItem->pszText;
	if(lp!=NULL)
	{
		if(plvItem->iSubItem==0)
			lp->SetItemText(str);
		else //subitem data 
			lp->SetSubItemText(plvItem->iSubItem-1, str);
	   UpdateData(lpItem, lp);// do not use bUpdateRow here, hence we only update a specific item or subitem..not all of them
	}
	SetItemText(plvItem->iItem, plvItem->iSubItem, plvItem->pszText);
}


void CTGGridCtrl::DeleteAll()
{
	TRACE( _T("[CTGGridCtrl::DeleteAll]\r\n" )) ;

	SetRedraw(false);
	DeleteAllItems();
	POSITION pos = m_RootItems.GetHeadPosition();
	while(pos!=NULL)
	{
		CTreeItem * pRoot = (CTreeItem*)m_RootItems.GetNext(pos);
		if(pRoot!=NULL)
		{
			// If root, must delete from m_rootData
			if(GetParentItem(pRoot) == NULL )
			{
				DeleteRootItem(pRoot);
			}
			else
				Delete(pRoot);
		}
	}

	UpdateData();
}

POSITION CTGGridCtrl::GetRootHeadPosition() const
{
	return m_RootItems.GetHeadPosition();
}


POSITION CTGGridCtrl::GetRootTailPosition() const
{
	return m_RootItems.GetTailPosition();
}


CTGGridCtrl::CTreeItem* CTGGridCtrl::GetNextRoot(POSITION& pos) const
{
	return (CTreeItem*)m_RootItems.GetNext(pos);
}


CTGGridCtrl::CTreeItem* CTGGridCtrl::GetPrevRoot(POSITION& pos) const
{
	return (CTreeItem*)m_RootItems.GetNext(pos);
}


POSITION CTGGridCtrl::GetHeadPosition(CTreeItem* pItem) const
{
	return pItem->m_listChild.GetHeadPosition();
}



CTGGridCtrl::CTreeItem* CTGGridCtrl::GetNextChild(CTreeItem *pItem, POSITION& pos) const
{
	return (CTreeItem*)pItem->m_listChild.GetNext(pos);
}



CTGGridCtrl::CTreeItem* CTGGridCtrl::GetPrevChild(CTreeItem *pItem, POSITION& pos) const
{
	return (CTreeItem*)pItem->m_listChild.GetPrev(pos);
}



POSITION CTGGridCtrl::GetTailPosition(CTreeItem *pItem) const
{
	return pItem->m_listChild.GetTailPosition();
}



void CTGGridCtrl::AddTail(CTreeItem *pParent, CTreeItem *pChild)
{
	pParent->m_listChild.AddTail(pChild);
}


inline int StrComp(const CString* pElement1, const CString* pElement2)
{
	return pElement1->Compare(*pElement2);
}



int CTGGridCtrl::CompareChildren(const void* p1, const void* p2)
{
	CTreeItem * pChild1 = *(CTreeItem**)p1;
	CTreeItem * pChild2 = *((CTreeItem**)p2);
	CItemInfo *pItem1=(*pChild1).m_lpNodeInfo;
	CItemInfo *pItem2=(*pChild2).m_lpNodeInfo;
	return StrComp(&(pItem1->GetItemText()), &(pItem2->GetItemText()));
}


void CTGGridCtrl::Sort(CTreeItem* pParent, BOOL bSortChildren)
{
	int i;
	const int nChildren = NumChildren(pParent);
	if (nChildren > 1)
	{
		CTreeItem** ppSortArray = new CTreeItem*[nChildren];
		// Fill in array with pointers to our children.
		POSITION pos = pParent->m_listChild.GetHeadPosition();
		for (i=0; pos; i++)
		{
			ASSERT(i < nChildren);
			ppSortArray[i] = (CTreeItem*)pParent->m_listChild.GetAt(pos);
			pParent->m_listChild.GetNext(pos);
		}

		// SERIOUS	qsort(ppSortArray, nChildren, sizeof(CTreeItem*), CompareChildren);
		// reorg children with new sorted list
		pos = pParent->m_listChild.GetHeadPosition();
		for (i=0; pos; i++)
		{
			ASSERT(i < nChildren);
			pParent->m_listChild.SetAt(pos, ppSortArray[i]);
			pParent->m_listChild.GetNext(pos);
		}

		delete [] ppSortArray;
	}

	if(bSortChildren)
	{
		POSITION pos = pParent->m_listChild.GetHeadPosition();
		while (pos)
		{
			CTreeItem *pChild = (CTreeItem*)pParent->m_listChild.GetNext(pos);
			Sort(pChild, TRUE);
		}
	}
}

void CTGGridCtrl::SortEx(BOOL bSortChildren)
{
	int i;

	int nItems = m_RootItems.GetCount();
	if (nItems > 0)
	{
		if(bSortChildren)
		{
			POSITION posSortChildren = GetRootHeadPosition();
			while(posSortChildren != NULL)
			{
					CTreeItem *pParent =(CTreeItem*)GetNextRoot(posSortChildren); 
					Sort(pParent, TRUE);//sort children			
			}	
		}		
		//set hideflag for rootitems
		POSITION posHide = GetRootHeadPosition();
		while(posHide != NULL)
		{
				CTreeItem *pParent =(CTreeItem*)GetNextRoot(posHide); 
				Collapse(pParent);
		}	

		DeleteAllItems();//this is quite okay, I don�t delete the internal state
		//sort rootitems
		CTreeItem** ppSortArray = new CTreeItem*[nItems];
		// Fill in array with pointers to our children.
		POSITION posCur = m_RootItems.GetHeadPosition();
		for ( i=0; posCur; i++)
		{
			ppSortArray[i] = (CTreeItem*)m_RootItems.GetAt(posCur);
			m_RootItems.GetNext(posCur);
		}

// SERIOUS		qsort(ppSortArray, nItems, sizeof(CTreeItem*), &CompareChildren);
		// reorg rootitems with new sorted list
		posCur = m_RootItems.GetHeadPosition();
		for (i=0; posCur; i++)
		{
			m_RootItems.SetAt(posCur, ppSortArray[i]);
			m_RootItems.GetNext(posCur);
		}
		delete [] ppSortArray;
		//do a "repaint" of only the rootitems...you could "refresh" the children with a expand 
		int nIndex=0;//no suprise here
		POSITION pos = GetRootHeadPosition();
		while(pos != NULL)
		{
			CTreeItem *pParent = (CTreeItem*)GetNextRoot(pos); 
			if(pParent!=NULL)
			{
				LV_ITEM lvItem;
				lvItem.mask = LVIF_TEXT | LVIF_INDENT | LVIF_PARAM;
				CItemInfo* lp = GetData(pParent); 
				CString str = lp->GetItemText();
				lvItem.pszText = str.GetBuffer(1); 
				str.ReleaseBuffer();
				lvItem.iItem = nIndex;
				lvItem.iSubItem = 0;
				lvItem.lParam = (LPARAM)pParent;//associate the root and all its children with this listviewitem
				lvItem.iIndent = GetIndent(pParent);
				CListCtrl::InsertItem(&lvItem);
				int nSize = lp->GetItemCount();
				for(i=0; i < nSize; i++)
				{
				   CString strSubItems = lp->GetSubItem(i);
				   lvItem.mask = LVIF_TEXT;
				   lvItem.iSubItem = i+1;
				   lvItem.pszText = strSubItems.GetBuffer(1);
				   strSubItems.ReleaseBuffer();
				   SetItem(&lvItem);
				}
				nIndex++;
			}
		}//while
	}//nItems
}

int CTGGridCtrl::NumChildren(const CTreeItem *pItem) const
{
	return pItem->m_listChild.GetCount();
}



BOOL CTGGridCtrl::ItemHasChildren(const CTreeItem* pItem) const
{ 
	BOOL bChildren = pItem->m_listChild.GetCount() != 0;
	//see if we have a flag
	int nFlag = pItem->m_bSetChildFlag;
	if(nFlag!=-1)
		return 1;
	else
		return bChildren;
}


void CTGGridCtrl::SetChildrenFlag(CTreeItem *pItem, int nFlag) const
{
	pItem->m_bSetChildFlag = nFlag;
}


BOOL CTGGridCtrl::IsCollapsed(const CTreeItem* pItem) const
{
	return pItem->m_bHideChildren;//e.g not visible
}


void CTGGridCtrl::Hide(CTreeItem* pItem, BOOL bFlag)
{
	pItem->m_bHideChildren=bFlag;
}


int CTGGridCtrl::GetIndent(const CTreeItem* pItem) const
{
	return pItem->m_nIndent;
}


void CTGGridCtrl::SetIndent(CTreeItem *pItem, int iIndent)
{
	pItem->m_nIndent = iIndent;
}



int CTGGridCtrl::GetCurIndex(const CTreeItem *pItem) const
{
	return pItem->m_nIndex;
}


void CTGGridCtrl::SetCurIndex(CTreeItem* pItem, int nIndex) 
{
	pItem->m_nIndex = nIndex;
}


void CTGGridCtrl::SetParentItem(CTreeItem*pItem, CTreeItem* pParent)
{
	pItem->m_pParent=pParent;

}


CTGGridCtrl::CTreeItem* CTGGridCtrl::GetParentItem(const CTreeItem* pItem) 
{
	return pItem->m_pParent;
};



CItemInfo* CTGGridCtrl::GetData(const CTreeItem* pItem) 
{
	return pItem->m_lpNodeInfo;
}

void CTGGridCtrl::UpdateData(CTreeItem* pItem, CItemInfo* lpInfo, BOOL bUpdateRow)
{
	pItem->m_lpNodeInfo = lpInfo;
	// ADD++
	if(bUpdateRow)//update listview item and subitems
		InvalidateItem(pItem);
}


//overrides
CItemInfo* CTGGridCtrl::CopyData(CItemInfo* /*lpSrc*/)
{
	ASSERT(FALSE);//debug
	return NULL;  //release
}

//default implementation for setting icons
int CTGGridCtrl::GetIcon( CTreeItem* /*pItem*/)
{	
	return 0;//just take the first item in CImageList ..what ever that is
}


void CTGGridCtrl::ShowPopupMenu(CPoint /*point*/)
{
	return;
}

void CTGGridCtrl::NotifyDoubleClick(BOOL bFirst)
{
	return;
}

BOOL CTGGridCtrl::GetTextProp(CTreeItem* /*pItem*/, int /*nCol*/, COLUMN_TEXT_PROP& /*textProp*/)
{
	return TRUE;
}


void CTGGridCtrl::OnControlLButtonDown(UINT /*nFlags*/, CPoint /*point*/, LVHITTESTINFO& ht)
{
	if(ht.iSubItem==0)
	{
		CRect rcItem;
		GetItemRect(ht.iItem, rcItem, LVIR_LABEL);
		DWORD dwStyle = WS_BORDER | WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL|ES_LEFT;
		CEdit *pEdit = new CTGListEditCtrl(ht.iItem, ht.iSubItem, GetItemText(ht.iItem, ht.iSubItem));
		pEdit->Create(dwStyle, rcItem, this, 0x1233);	
	}
	else
		EditLabelEx(ht.iItem, ht.iSubItem);	
}
	

COLORREF CTGGridCtrl::GetCellRGB()
{
	return RGB(192,0,0);
}

	
BOOL CTGGridCtrl::OnVKMultiply(CTreeItem* /*pItem*/, int /*nIndex*/)
{
	return 1;
}



BOOL CTGGridCtrl::OnVkSubTract(CTreeItem* /*pItem*/, int /*nIndex*/)
{
	return 1;
}



BOOL CTGGridCtrl::OnVKAdd(CTreeItem* /*pItem*/, int /*nIndex*/)
{
	return 1;
}


BOOL CTGGridCtrl::OnDeleteItem(CTreeItem* /*pItem*/, int /*nIndex*/)
{
	return 1;
}



BOOL CTGGridCtrl::OnItemExpanding(CTreeItem* /*pItem*/, int /*nIndex*/)
{
	return 1;
}


BOOL CTGGridCtrl::OnItemExpanded(CTreeItem* /*pItem*/, int /*nIndex*/)
{
	return 1;
}


BOOL CTGGridCtrl::OnCollapsing(CTreeItem* /*pItem*/)
{
	return 1;
}


BOOL CTGGridCtrl::OnItemCollapsed(CTreeItem* /*pItem*/)
{
	return 1;
}


BOOL CTGGridCtrl::OnItemLButtonDown(LVHITTESTINFO& /*ht*/)
{
	return 1;
}


BOOL CTGGridCtrl::OnVkReturn(void)
{
	return 0;
}


void CTGGridCtrl::OnSysColorChange() 
{
	CListCtrl::OnSysColorChange();
	//nop nothing yet
}

UINT CTGGridCtrl::_GetCount(CTreeItem* pItem, UINT& nCount)
{
	POSITION pos = pItem->m_listChild.GetHeadPosition();
	while (pos)
	{
		CTreeItem *pChild = (CTreeItem*)pItem->m_listChild.GetNext(pos);
		nCount = _GetCount(pChild, nCount);
		nCount++;				
	}
	return nCount;
}

UINT CTGGridCtrl::GetCount(void) 
{
	UINT nCount=0;
	UINT _nCount=0;
	POSITION pos = m_RootItems.GetHeadPosition();
	while(pos!=NULL)
	{
		CTreeItem * pRoot = (CTreeItem*)m_RootItems.GetNext(pos);
		nCount += _GetCount(pRoot, _nCount) + 1;
	}
	return nCount;
}


CTGGridCtrl::CTreeItem* CTGGridCtrl::GetTreeItem(int nIndex /*nIndex must be valid of course*/ ) 
{
	return reinterpret_cast<CTreeItem*>(GetItemData(nIndex));
}

int CTGGridCtrl::GetSelectedItem(void) const
{
	return GetNextItem(-1, LVNI_ALL | LVNI_SELECTED); 
}

void CTGGridCtrl::InvalidateItem(CTreeItem *pItem)
{
	if(pItem!=NULL)
	{
		int nItem = GetCurIndex(pItem);//just has to be visible
		if(nItem != -1)
		{
			CItemInfo *lp = GetData(pItem);
			int nSize = lp->GetItemCount();
			SetRedraw(false);
			SetItemText(nItem, 0,lp->GetItemText());
			for(int i=0; i < nSize;i++)
			{
				CString str = lp->GetSubItem(i);
				SetItemText(nItem, i+1, str.GetBuffer(1));
				str.ReleaseBuffer();
			}
			SetRedraw(true);
		}
	}
}

void CTGGridCtrl::InvalidateItemRect(int nItem/*better be valid item*/)
{
	if(nItem != -1)
	{
		CRect rc;
		GetItemRect(nItem, rc, LVIR_BOUNDS);
		InvalidateRect(rc);
		UpdateWindow();
	}
}


void CTGGridCtrl::InvalidateItemRectPtr(CTreeItem *pItem)	
{
	if(pItem!=NULL)
	{
		int nItem = GetCurIndex(pItem);
		if(nItem!=-1)
			InvalidateItemRect(nItem);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// not much but ... 
//
//////////////////////////////////////////////////////////////////////////

CTGGridCtrl::CTreeItem::~CTreeItem()
{
	// delete child nodes
	POSITION pos = m_listChild.GetHeadPosition();
	while (pos != NULL)
	{
		CTreeItem* pItem = (CTreeItem*)m_listChild.GetNext(pos);
		if(pItem!=NULL)
		{
			if(pItem->m_lpNodeInfo!=NULL)
			{
				delete pItem->m_lpNodeInfo;
				pItem->m_lpNodeInfo = NULL;
			}
			delete pItem;
			pItem = NULL;
		}
	}
	m_listChild.RemoveAll();
}


//////////////////////////////////////////////////////////////////////////
//
// Simple class CRectangle for the + - sign, 
//
//////////////////////////////////////////////////////////////////////////

CRectangle::CRectangle(CTGGridCtrl* pCtrl, CDC* pDC, int iIndent, const CRect& rcBounds)
{
	m_pDC=pDC;
	int nHalfImage = (pCtrl->m_cxImage >> 1);
	int nBottomDown = rcBounds.top + nHalfImage + ((rcBounds.Height() - pCtrl->m_cyImage) >> 1);
	m_right_bottom.cx = (pCtrl->m_cxImage>>1)+2+1;
	m_right_bottom.cy = (pCtrl->m_cyImage>>1)+2+1;
	m_left = rcBounds.left  + iIndent * pCtrl->m_cxImage - nHalfImage;
	m_top = nBottomDown - (m_right_bottom.cy >> 1);
	m_left_top.x = m_left -  (m_right_bottom.cx >> 1);
	m_left_top.y = m_top;
	m_topdown = nBottomDown;
}

	
void CRectangle::DrawRectangle(CTGGridCtrl* pCtrl)
{
	//erase bkgrnd
	CRect rc(m_left_top, m_right_bottom);
	m_pDC->FillRect(rc, &pCtrl->m_brushErase);
	
	//draw rectangle	
	//-- 2012-07-16 hongsu@esmlab.com
	//-- CIC Check 
	// CPen* pPenRectangle = m_pDC->SelectObject(&pCtrl->m_psRectangle);

	m_pDC->Rectangle(rc);
//	m_pDC->SelectObject(pPenRectangle);		
}


CRectangle::~CRectangle()
{
}


BOOL CRectangle::HitTest(CPoint pt)
{
	CRect rc = GetHitTestRect();
	return rc.PtInRect(pt);
}


void CRectangle::DrawPlus(void)
{
	m_pDC->MoveTo(m_left, m_topdown-2);
	m_pDC->LineTo(m_left, m_topdown+3);

	m_pDC->MoveTo(m_left-2, m_topdown);
	m_pDC->LineTo(m_left+3, m_topdown);
}


void CRectangle::DrawMinus(void)
{
	m_pDC->MoveTo(m_left-2, m_topdown);
	m_pDC->LineTo(m_left+3, m_topdown);
}

  
void CTGGridCtrl::OnRButtonDown(UINT nFlags, CPoint point)
{	
	OnLButtonDown(nFlags, point);	
	ShowPopupMenu(point);	
	CListCtrl::OnRButtonDown(nFlags, point);
}

// ADD++
void CTGGridCtrl::SetCheckTopDown(CTreeItem* pItem, bool bCheck )
{	
	if(pItem == NULL) 
		return;
	
	// Check Status Setting.	
	int nIdx = NodeToIndex(pItem);
	pItem->m_lpNodeInfo->SetCheck(bCheck);		// �������� ����
	if(nIdx >= 0)	SetCheck(nIdx, bCheck);		// ���� ����Ʈ�� ǥ�õǰ� �ִ� �׸��̶��, List Control�� SetCheck 
	
	TRACE( _T("Check >>>>>>>>  %d : %s(%d)  \n"), nIdx, pItem->m_lpNodeInfo->GetItemText(), bCheck );
		

	// �� �̻� ������ ���ٸ� ����.
	int nDataType = pItem->m_lpNodeInfo->GetDataType();
	if( nDataType == GetLowestLevel() )	return;

	const int nChildren = pItem->m_listChild.GetCount();
	if (nChildren > 0)
	{	
		POSITION pos = pItem->m_listChild.GetHeadPosition();
		while(pos)
		{
			CTreeItem* pNextNode = (CTreeItem*)pItem->m_listChild.GetNext(pos);
			SetCheckTopDown(pNextNode, bCheck);
		}
	}
}

// ADD++
void CTGGridCtrl::SetCheckDownUp(CTreeItem* pItem)
{	
	if(pItem == NULL) 
		return;

	// �θ� ���ٸ� ����
	CTreeItem* pParent = GetParentItem(pItem);
	if( pParent == NULL ) return;

	int nIdx = NodeToIndex(pParent);
	TRACE( _T("Parent Check Status >>>>>>>>  %d : %s(%d)  \n"), nIdx, pParent->m_lpNodeInfo->GetItemText(), pParent->m_lpNodeInfo->GetCheck() );

	bool bCheck = true;
	const int nChildren = pParent->m_listChild.GetCount();
	if (nChildren > 0)
	{	
		POSITION pos = pParent->m_listChild.GetHeadPosition();

		while(pos)
		{
			CTreeItem* pNextNode = (CTreeItem*)pItem->m_listChild.GetNext(pos);
			TRACE( _T("Parent Check Status >>>>>>>>  %d : %s(%d)  \n"), nIdx, pNextNode->m_lpNodeInfo->GetItemText(), pParent->m_lpNodeInfo->GetCheck() );
			if( FALSE == pNextNode->m_lpNodeInfo->GetCheck() )
			{
				bCheck = FALSE;
				break;
			}
		}
	}

	if( bCheck != pParent->m_lpNodeInfo->GetCheck() )
	{
		if(nIdx >= 0)	
			SetCheck(nIdx, bCheck);		// ���� ����Ʈ�� ǥ�õǰ� �ִ� �׸��̶��, List Control�� SetCheck 
		pParent->m_lpNodeInfo->SetCheck(bCheck);	// check ���� ����

		SetCheckDownUp(pParent);
	}
}

// ADD++
int CTGGridCtrl::GetLowestLevel()
{
	return 0;
}

// ADD++
void CTGGridCtrl::AddFunction(int nType)
{
	m_nAddFunction |= nType;
}

//the search function here search all nodes regardless if collapsed or expanded
CTGGridCtrl::CTreeItem* CTGGridCtrl::Search(CString strItem)
{
	if(!GetItemCount())
		return NULL;

	va_list args;
	va_start(args, strItem);
	int nCount=0;
	
	for(;;)
	{
		LPCTSTR lpsz = va_arg(args, LPCTSTR);
		if(lpsz==NULL)
			break;
	   nCount++;
	}


	POSITION pos = GetRootHeadPosition();
	while(pos != NULL)
	{
		CTreeItem *pParent = (CTreeItem*)GetNextRoot(pos); 
		CTreeItem *pItem = pParent;
		CItemInfo* lp = GetData(pParent);
		CString strData = lp->GetItemText();
		if(strData==strItem)//must be a match before going further...suit you self
			return pParent;

		//GetNext ....loop through all children 
		for(;;)
		{
			CTreeItem *pCur = GetNext(pParent, pItem, TRUE, FALSE/*regardless of the item are hidden or not, set to TRUE if only visible items must be included in the search*/);	  
			if(!IsChildOf(pParent, pCur))
				break;
			else
			if(pCur==pItem)
				break;
			CItemInfo* lp = GetData(pCur);
			CString strData = lp->GetItemText();
			if(strData==strItem)
				return pCur;
			pItem=pCur;//next;
		}
	}	
	return NULL;
}

CTGGridCtrl::CTreeItem* CTGGridCtrl::SearchEx(CTreeItem *pStartPosition, CString strItem)
{
	CItemInfo* lp = GetData(pStartPosition);
	//if(lp->GetCheck()) another condition here maybe
	CString strData = lp->GetItemText();
	if(strData==strItem)
	{
		return pStartPosition;
	}

	const int nChildren = NumChildren(pStartPosition);
	if (nChildren > 0)
	{
		POSITION pos = GetHeadPosition(pStartPosition);
		while (pos)
		{
			CTreeItem *pChild = GetNextChild(pStartPosition, pos);
			CItemInfo* lp = GetData(pChild);
			CString strData = lp->GetItemText();
			if(strData==strItem)
			{
				return pChild;
			}
		}
	}

	POSITION pos = GetHeadPosition(pStartPosition);
	while (pos)
	{
		CTreeItem *pChild = GetNextChild(pStartPosition, pos);
		CItemInfo* lp = GetData(pChild);
		CString strData = lp->GetItemText();
		if(strData==strItem)
		{
			return pChild;
		}

		pChild = SearchEx(pChild, strItem);
		if(pChild!=NULL)
			return pChild;
	}
	return NULL;
}

void CTGGridCtrl::SortData()
{
	int nIndex = GetNextItem(-1, LVNI_ALL | LVNI_SELECTED); 
	if(nIndex==-1)
		return;

	if(AfxMessageBox(_T("Sort roots and all children(Yes)\nor just sort the rootitems(No)"),MB_YESNO)==IDYES)
		SortEx(1);			
	else
		SortEx(0);
}



//-----------------------------------------------------------------------------
//--
//-- GET FILE NAME
//-- 2008-06-30
//--
//-----------------------------------------------------------------------------
CString CTGGridCtrl::GetFileName(CString strFullPath)
{
	int nLocate;
	CString strFileName;
	nLocate = strFullPath.ReverseFind ('\\');
    strFileName = strFullPath.Right(strFullPath.GetLength ()-nLocate-1);

	//-- REMOVE GTP
	//strFileName = strID.Left(strID.GetLength ()-4);
	return strFileName;
}

void CTGGridCtrl::DeleteSelectedItem()
{
	TRACE(_T("[CTGGridCtrl::DeleteSelectedItem] \r\n"));

	int nItem = GetSelectedItem();
	if(nItem < 0) 
		return;
	CTreeItem* pSelItem = GetTreeItem(nItem);
	if(pSelItem == NULL) 
		return;
	DeleteItemEx(pSelItem, nItem);
}

CTGGridCtrl::CTreeItem* CTGGridCtrl::GetSelectedTreeItem()
{
	int nItem = GetSelectedItem();
	if(nItem < 0) 
		return NULL;
	//-- GET SELECTED ITEM
	CTreeItem* pSelItem = GetTreeItem(nItem);
	return pSelItem;
}

BOOL CTGGridCtrl::IsExistRoot(CString strRootItem)
{
	CTreeItem* pParent = NULL;
	POSITION pos = m_RootItems.GetHeadPosition();
	for (int i=0; pos; i++)
	{
		pParent = (CTreeItem*)m_RootItems.GetAt(pos);			
		if(!pParent)
			return FALSE;
		if(pParent->m_lpNodeInfo->GetItemText () == strRootItem)
			return TRUE;
		
		m_RootItems.GetNext(pos);
	}
	return FALSE;
}

void CTGGridCtrl::OnClose()
{
	m_psPlusMinus.DeleteObject();
	m_psRectangle.DeleteObject();
	m_psTreeLine.DeleteObject();
	m_brushErase.DeleteObject();

	while(m_RootItems.GetCount())
	{
		CTreeItem * root = (CTreeItem*)m_RootItems.RemoveHead();
		if(root!=NULL && GetData(root) != NULL)
		{
			CItemInfo* pInfo = GetData(root);
			delete pInfo;
			pInfo = NULL;
		}
		delete root;
		root = NULL;
	}

	m_RootItems.RemoveAll();
	CListCtrl::OnClose();
}

void CTGGridCtrl::UpdateData()
{
	//-- 2012-3-7 hongsu
	//-- TRACE(_T("[CTGGridCtrl::UpdateData] \r\n"));

	SetRedraw(true);
	InvalidateRect(NULL);
	UpdateWindow();
}

void CTGGridCtrl::CleanAll()
{
	TRACE(_T("[CTGGridCtrl::CleanAll] \r\n"));

	//DeleteAllItems();
	POSITION pos = m_RootItems.GetHeadPosition();
	while(pos!=NULL)
	{
		CTreeItem * pRoot = (CTreeItem*)m_RootItems.GetNext(pos);
		if(pRoot!=NULL)
		{
			// If root, must delete from m_rootData
			if(GetParentItem(pRoot) == NULL )
			{
				DeleteRootItem(pRoot);
			}
			else
				Delete(pRoot);
		}
	}
}

void CTGGridCtrl::OnHeaderNotify(NMHDR* pNMHDR, LRESULT* pResult) 
{
	TRACE(_T("[CTGGridCtrl::OnHeaderNotify] \r\n"));

    HD_NOTIFY *phdn = (HD_NOTIFY *) pNMHDR;

	*pResult = 0;

	if( phdn->iItem == 0 )		
	{		
		SortListItem(m_RootItems);
		g_bAscending = !g_bAscending;
	}
}

TREEITEMLIST* CTGGridCtrl::GetSortResult(CPtrList2 TreeItems, BOOL bRoot)
{	
	TRACE(_T("+[CTGGridCtrl::GetSortResult] \r\n"));

	TREEITEMLIST*	pOrgList = new TREEITEMLIST;
	TREEITEMLIST*	pNewList = NULL;

	if( pOrgList == NULL ) return FALSE;

	pOrgList->pItem		= NULL;
	pOrgList->pNext		= NULL;

	TREEITEMLIST*	pTemp = pOrgList;
	POSITION pos = TreeItems.GetHeadPosition();
	while(pos!=NULL)
	{
		CTreeItem * pRoot = (CTreeItem*)TreeItems.GetNext(pos);
		if(pRoot == NULL) goto GetSortResult_Error;

		if( bRoot && (GetParentItem(pRoot) || ( pRoot->m_lpNodeInfo->GetItemText() == TGCC_NOT_REGISTERD) ) )
			continue;
			
		pNewList = new TREEITEMLIST;			
		if( pNewList == NULL ) goto GetSortResult_Error;

		pNewList->pItem		= pRoot->m_lpNodeInfo;		
		POSITION pos = pRoot->m_listChild.GetHeadPosition();
		while (pos != NULL)
			pNewList->listChild.AddTail(pRoot->m_listChild.GetNext(pos));

		pNewList->pNext	= NULL;
		pTemp->pNext	= pNewList;
		pTemp			= pNewList;
	}

	TRACE(_T("-[CTGGridCtrl::GetSortResult] \r\n"));
	return (TREEITEMLIST*) SortLlinkedList((void*)(pOrgList->pNext), 0, &CompareListItem, NULL, NULL);

GetSortResult_Error:

	while( pOrgList ) 
	{			
		TREEITEMLIST* pBackUp = pOrgList;
		pOrgList = pOrgList->pNext;

		delete pBackUp;
	}

	TRACE(_T("-[CTGGridCtrl::GetSortResult] GetSortResult_Error \r\n"));
	return NULL;
}

BOOL CTGGridCtrl::SortListItem(CPtrList2& TreeItems, CTreeItem* pParent, short bAscending)
{
	TRACE(_T("+[CTGGridCtrl::SortListItem] \r\n"));

	TREEITEMLIST*	pParentSortedList = GetSortResult(TreeItems, (pParent==NULL? TRUE:FALSE));

	if( pParentSortedList == NULL ) return FALSE;

	DeleteAllItems();
	TreeItems.RemoveAll();

	pParent = InsertRootItem(pParentSortedList->pItem);
	TRACE(_T("[CTGGridCtrl::SortListItem] InsertRootItem(%s)\r\n"), pParentSortedList->pItem->GetItemText());

	SortListItemSub( pParentSortedList, pParent );
		
	UpdateData();
	Expand(pParent, 0);//ExpandAll();//IsCollapsed

	TRACE(_T("-[CTGGridCtrl::SortListItem] \r\n"));
	return TRUE;
}

BOOL CTGGridCtrl::SortListItemSub( TREEITEMLIST* pParentSortedList, CTreeItem* pParent )
{
	while( pParentSortedList ) 
	{			
		if( pParentSortedList->listChild.GetCount() > 0 )
		{
			TRACE(_T("[CTGGridCtrl::SortListItemSub] Child sort\r\n"));			
			TREEITEMLIST*	pChildSortedList = GetSortResult(pParentSortedList->listChild, FALSE);
			
			while( pChildSortedList )
			{
				CTreeItem* pTreeItem = InsertItem(pParent, pChildSortedList->pItem, TRUE);
				TRACE(_T("[CTGGridCtrl::SortListItemSub] Child Item insert (%s)\r\n"), pChildSortedList->pItem->GetItemText());


				if( pChildSortedList->listChild.GetCount() > 0 )
					SortListItemSub(pChildSortedList, pTreeItem);				

				pChildSortedList = pChildSortedList->pNext;
			}
		}

		pParent = NULL;		
		pParentSortedList = pParentSortedList->pNext;
	}

	return TRUE;
}
