/////////////////////////////////////////////////////////////////////////////
//
//  Liveview.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#pragma once

#include "cv.h"
#include "highgui.h"
// CLiveview view

enum
{
	LIVEVIEW_TYPE_NULL = -1,
	LIVEVIEW_TYPE_MAINFRM,
	LIVEVIEW_TYPE_MULTIFRM,
	LIVEVIEW_TYPE_LIVEVIEWFRM,
};

class CLiveview : public CView
{
	DECLARE_DYNCREATE(CLiveview)

public:
	CLiveview();									// protected constructor used by dynamic creation
	CLiveview(int nDevice, BOOL bMain, int nType);    // protected constructor used by dynamic creation
	virtual ~CLiveview();

public:
	virtual void OnDraw(CDC* pDC) {};      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);

protected:
	DECLARE_MESSAGE_MAP()

public:
	void SetShift(int n);//	{ m_nShift = n; }
	BOOL IsRotate()	{ return m_nShift % 2; }
	CRect RotateRect(CRect rect);
	void SetLiveview(BOOL b)	{ m_bLiveview = b; Invalidate(FALSE);}
	BOOL IsSetLiveview()		{return m_bLiveview;}
	BOOL IsLiveviewOn();
	CSize GetSrcSize();
	CSize GetOffsetSize();
	
	//-- 2011-06-22 hongsu.jung
	void SetGridOpt(int n)		{ m_nGridLine = n; }
	int GetGridOpt()				{ return m_nGridLine; }	
	void SetZoomOpt(int n)		{ m_nZoomOpt= n; }
	int GetZoomOpt()				{ return m_nZoomOpt; }	
	void SetAFFocus(int n)		{ m_nAFFocus= n; }
	int GetAFFocus()				{ return m_nAFFocus; }	
	
	//-- 2011-06-22 hongsu.jung
	void SetDrawZoomPos(BOOL b) { m_bDrawZoomPos = b; }

	//-- 2011-8-28 Lee JungTaek
	BOOL GetDrawAFFrame()	{return m_bDrawAFFrame;}
	void SetDrawAFFrame(BOOL b)	{m_bDrawAFFrame = b;}

	void SetSupported(BOOL bSupported){ m_bIsSupported = bSupported; }
	
private:
	afx_msg void OnPaint();
	BOOL m_bMainView;	//-- Main Liveview
	BOOL m_bLiveview;		//-- Show or Not
	//-- Shift
	int m_nShift;
	//-- Device Number
	int m_nDeviceID;
	//-- Grid Line
	int m_nGridLine;	
	int m_nZoomOpt;
	int m_nAFFocus;

	//-- Zoom Position
	BOOL		m_bDrawZoomPos; //-- Draw Zoom Pos
	BOOL		m_bSetZoomPos;
	BOOL		m_bZoomMove;
	int			m_nZoomSize;
	CPoint		m_ptZoom;
	CSize		m_szZoom;
	CRect		m_rect;

	//-- 2011-8-9 Lee JungTaek
	//-- Check Liveview Type
	int		m_nLiveviewType;
	//-- 2011-8-28 Lee JungTaek
	BOOL m_bDrawAFFrame;
	Color m_colorAF;

	//-- 2011-09-02 jeansu
	BOOL m_bIsSupported;

	//-- 2011-05-16	
	void DrawLiveview();
	IplImage* rotateImage(IplImage *src, int nShift);

	void DrawZoomPosition(CDC* pMemDC, CRect rect);
	void DrawGrid(CDC* pMemDC, CRect rect);
	void DrawFocus(CDC* pMemDC, CRect rect);
	

	BOOL PtInRect(CPoint pt);
	void SetPtZomm(CPoint pt);
	void UpdatePosition(int, int );
	void MouseEvent();
};

