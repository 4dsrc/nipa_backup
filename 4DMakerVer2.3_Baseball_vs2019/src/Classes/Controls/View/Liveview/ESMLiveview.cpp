////////////////////////////////////////////////////////////////////////////////
//
//	ESMLiveview.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-23
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMLiveview.h"
#include "ESMIndex.h"
#include "ESMFunc.h"

CMutex g_MutexLiveView(TRUE, _T("LiveView"));

IMPLEMENT_DYNCREATE(CESMLiveview, CView)

CESMLiveview::CESMLiveview()
{	
	m_bLiveview = FALSE;
	m_pFrameData = NULL;
	m_nRgbAvg = 0;
	m_bUseFocusFrame = FALSE;
	m_nFocusFrameX = 1;
	m_nFocusFrameY = 1;
	m_nFocusFrameH = 1;
	m_nFocusFrameW = 1;
	m_nFocusFrameMag = 0;
	m_bClickedMouseL = FALSE;
	m_pLiveItem = NULL;
	m_bMouseHoverFlag = FALSE;
	m_bMouseSleepFlag = FALSE;
}
CESMLiveview::~CESMLiveview()
{	
	if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		SetLiveview(FALSE);

		if( m_pFrameData)
		{
			if(m_pFrameData->nFrameData)
			{
				delete[] m_pFrameData->nFrameData;
				m_pFrameData->nFrameData = NULL;
			}
			delete m_pFrameData;
			m_pFrameData = NULL;
		}
	}
}

BEGIN_MESSAGE_MAP(CESMLiveview, CView)	
	ON_WM_PAINT()	
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSELEAVE()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEHOVER()
	ON_WM_MOUSEWHEEL()
	ON_WM_TIMER()
END_MESSAGE_MAP()

void CESMLiveview::OnPaint()
{	
	if(m_bLiveview)
		DrawLiveview();	

	CView::OnPaint();
}


//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-19
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CESMLiveview::DrawLiveview()
{
	g_MutexLiveView.Lock();
	
	//-- GetRect
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);

	//-- Get Liveview	
	if(!m_pFrameData || !m_bLiveview)
	{		
		//-- Black Background	
		dc.FillRect(rect, &CBrush(RGB(0,0,0)));		
		g_MutexLiveView.Unlock();
		return;
	}

	BITMAPINFOHEADER bih;
	// 	cvFlip(m_pLiveviewBuffer,m_pLiveviewBuffer,-1);// 상하 반전
	// 	cvFlip(m_pLiveviewBuffer,m_pLiveviewBuffer,1); // 좌우 반전
	int nChenel = 3;
	int nDepth = 8;
	memset(&bih, 0, sizeof(BITMAPINFOHEADER));    
	bih.biSize      = sizeof(BITMAPINFOHEADER);
	bih.biWidth      = m_pFrameData->nWidth;  
	bih.biHeight   = m_pFrameData->nHeight; 
	bih.biPlanes   = 1;
	bih.biBitCount  = nChenel * nDepth;
	bih.biSizeImage  = m_pFrameData->nWidth*m_pFrameData->nHeight * nChenel;
	bih.biCompression = BI_RGB;
	dc.SetStretchBltMode(COLORONCOLOR);

	//3:2 to 16:9
	double dRatio = 59/64.;
	int nTop = m_pFrameData->nHeight * dRatio;
	int nHeight= m_pFrameData->nHeight * (58/64.*dRatio);
	
	::StretchDIBits(dc,	// hDC
		rect.TopLeft().x,				// XDest
		rect.TopLeft().y,				// YDest
		rect.Width(),					// nDestWidth
		rect.Height(),					// nDestHeight
		0,								// XSrc
		nTop,							// YSrc
		m_pFrameData->nWidth,			// nSrcWidth
		-nHeight,						// nSrcHeight
		(BYTE*)m_pFrameData->nFrameData,// lpBits
		(BITMAPINFO*)&bih,				// lpBitsInfo
		DIB_RGB_COLORS,					// wUsage
		SRCCOPY);						// dwROP	
	

	CPen obPen(PS_SOLID, 1, RGB(0,0,0)); 
	CPen* OldPen = dc.SelectObject(&obPen);
	dc.SetROP2(R2_NOT);
	int nAvgRed = 0;
	int nAvgGreen = 0;
	int nAvgBlue = 0;

	int nCount = 0;
	for( int i =m_pFrameData->nHeight /4; i < m_pFrameData->nHeight /4 * 3; i++ )
	{
		for( int j =m_pFrameData->nWidth / 4 ; j< m_pFrameData->nWidth / 4 * 3; j++ )
		{
			nCount++;
			nAvgBlue += m_pFrameData->nFrameData[m_pFrameData->nWidth * i * 3 + j* 3 ];
			nAvgGreen += m_pFrameData->nFrameData[m_pFrameData->nWidth * i * 3 + j* 3  + 1];
			nAvgRed += m_pFrameData->nFrameData[m_pFrameData->nWidth * i * 3 + j* 3  + 2];
		}
	}

	nAvgRed = nAvgRed / nCount;
	nAvgGreen = nAvgGreen / nCount;
	nAvgBlue = nAvgBlue / nCount;
	
	//m_nRgbAvg = (nAvgRed + nAvgGreen + nAvgBlue) /3;
	m_nRgbAvg = nAvgGreen;
	CString strTpData;
	CFont font, *pOldFont;
	font.CreatePointFont(110, _T("Lucida Grande Bold"));
	pOldFont=(CFont*)dc.SelectObject(&font);

	// 옵션에 의한 표시 유무 
// 	strTpData.Format(_T("Focus : %d"), nAvgGreen);
// 	dc.TextOut(rect.Width() - 120, rect.Height() - 60, strTpData);

	strTpData.Format(_T("Red  : %d"), nAvgRed);
	dc.TextOut(rect.Width() - 120, rect.Height() - 15, strTpData);
	strTpData.Format(_T("Blue  : %d"), nAvgBlue);
	dc.TextOut(rect.Width() - 120, rect.Height() - 30, strTpData);
	strTpData.Format(_T("Green: %d"), nAvgGreen);
	dc.TextOut(rect.Width() - 120, rect.Height() - 45, strTpData);

	(CFont*)dc.SelectObject(pOldFont);

	dc.SelectObject(OldPen);
	obPen.DeleteObject();

	//////////////////////////////////////////////////////////////////////////

	if (m_pLiveItem && 
		(m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0) 
		&& (m_pLiveItem->GetCamVersion() >= 0x100))
	{
		SetUseFocusFrame(TRUE);
	}
	else
	{
		SetUseFocusFrame(FALSE);
	}
	

	if (m_bUseFocusFrame)
	{
		int nW = rect.Width();
		int nH = rect.Height();
		double dFrameX1 = m_nFocusFrameX;
		double dFrameY1 = m_nFocusFrameY;
		double dFrameW = m_nFocusFrameW;
		double dFrameH = m_nFocusFrameH;
		double dFrameX2 = dFrameX1;
		double dFrameY2 = dFrameY1;

		dFrameW = dFrameW / 1000;
		dFrameW = nW * dFrameW;
		dFrameH = dFrameH / 1000;
		dFrameH = nH * dFrameH;

		if (dFrameX1 > 0)
		{
			dFrameX1 = nW * (dFrameX1 / 1000);			
			dFrameX2 = dFrameX1 + (dFrameW/2);
			dFrameX1 = dFrameX1 - (dFrameW/2);
		}

		if (dFrameY1 > 0)
		{
			dFrameY1 = nH * (dFrameY1 / 1000);
			dFrameY2 = dFrameY1 + (dFrameH/2);
			dFrameY1 = dFrameY1 - (dFrameH/2);
		}
 		
		dc.SetROP2(R2_MASKPENNOT);

		obPen.CreatePen( PS_SOLID, 3, RGB(255,0,0) );  

		OldPen = dc.SelectObject( &obPen );
		SelectObject(dc,GetStockObject(NULL_BRUSH));
		dc.Rectangle(dFrameX1, dFrameY1, dFrameX2, dFrameY2);
		dc.SelectObject( OldPen );
		obPen.DeleteObject();
	}
	
	
	g_MutexLiveView.Unlock();
}

void CESMLiveview::SetFocusFrame( int nX, int nY, int nW, int nH, int nMag )
{
	m_nFocusFrameX = nX;
	m_nFocusFrameY = nY;
	m_nFocusFrameW = nW;
	m_nFocusFrameH = nH;
	m_nFocusFrameMag = nMag;
}

void CESMLiveview::SetUseFocusFrame( BOOL b )
{
	m_bUseFocusFrame = b;
}

void CESMLiveview::SetLiveview(BOOL b, CDSCItem* pItem)
{ 
	m_bLiveview = b;
	if(b)
	{
		Invalidate(FALSE);

		if (pItem != NULL)
		{
			m_pLiveItem = pItem;		
		}
	}
	else
	{		
		m_pLiveItem = NULL;
	}

	m_bMouseHoverFlag = FALSE;
	m_bMouseSleepFlag = FALSE;
}

void CESMLiveview::SetBuffer(PVOID pBuffer)
{ 
	g_MutexLiveView.Lock();

	FrameData* pFrameData = NULL;
	//FrameData* pFrameData = new FrameData;
	int nImageInfoBuf = sizeof(UINT) * 2;
	int nImageSize = 0;
	//m_pLiveviewBuffer = (IplImage*)pBuffer;
	if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
 	{
		if (pBuffer == NULL)
		{
			ESMLog(5, _T("@@@ CESMLiveview::SetBuffer pBuffer null..."));
		}

		FrameData tpFrameData;
		memset(&tpFrameData, 0, sizeof(struct FrameData));
		memcpy(&tpFrameData, pBuffer, nImageInfoBuf);
		
		if( m_pFrameData == NULL)
		{
			m_pFrameData = new FrameData;		
 			memset(m_pFrameData, 0, sizeof(struct FrameData));
			m_pFrameData->nFrameData = new BYTE[tpFrameData.nHeight * tpFrameData.nWidth * 3];
		}		

		pFrameData = (FrameData*)pBuffer;

		if( tpFrameData.nHeight != m_pFrameData->nHeight || tpFrameData.nWidth != m_pFrameData->nWidth )
		{

			ESMLog(5, _T("@@@ CESMLiveview::SetBuffer change ..."));
			if (m_pFrameData->nFrameData)
			{
				delete[] m_pFrameData->nFrameData;					
				m_pFrameData->nFrameData = NULL;
			}

			m_pFrameData->nFrameData = new BYTE[tpFrameData.nHeight * tpFrameData.nWidth * 3];
		}

		memcpy(m_pFrameData, (FrameData*)pBuffer, nImageInfoBuf);
 		nImageSize = m_pFrameData->nHeight * m_pFrameData->nWidth * 3;
 		memcpy(m_pFrameData->nFrameData, pFrameData->nFrameData, nImageSize);

 	}

	else
		m_pFrameData = (FrameData*)pBuffer;

	g_MutexLiveView.Unlock();
}

//-- 2013-03-08 hongsu.jung
void CESMLiveview::DrawBlank()
{
	SetLiveview(FALSE);
	SetBuffer(NULL);
	DrawLiveview();
}

void CESMLiveview::ScalingLiveview(CRect rect)
{
	MoveWindow(rect);
}

int CESMLiveview::GetRGBAvg()
{
	return m_nRgbAvg;
}

BOOL CESMLiveview::OnEraseBkgnd(CDC* pDC)
{
	CBrush backBrush(RGB(44, 44, 44)); // <- 흰색칼러로. 
	CBrush* pOldBrush = pDC->SelectObject(&backBrush); 
	CRect rect; pDC->GetClipBox(&rect); 
	pDC->PatBlt(rect.left, rect.top, rect.Width(), rect.Height(), PATCOPY);
	pDC->SelectObject(pOldBrush); 

	return TRUE;
	//return CView::OnEraseBkgnd(pDC);
}


void CESMLiveview::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (!m_bClickedMouseL)
		return;

	if (!m_pLiveItem)
		return;

	if (!m_bLiveview)
		return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
		return;

	if (m_pLiveItem->GetCamVersion() < 0x0100)
		return;

	if (m_bMouseSleepFlag)
		return;

	m_bMouseSleepFlag = TRUE;

	//////////////////////////////////////////////////////////////////////////
	CRect rect;
	GetClientRect(&rect);

	int nW = rect.Width();
	int nH = rect.Height();
	int nX = point.x;
	int nY = point.y;
	double dX = 0.0;
	double dY = 0.0;

	if (nX > 0 && nW > 0)
	{
		dX = ((double)nX / (double)nW) * 1000;
	}

	if (nY > 0 && nH > 0)
	{
		dY = ((double)nY / (double)nH) * 1000;
	}

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_SDI_OP_SET_FOCUS_FRAME;
	pMsg->nParam1 = (int)dX;
	pMsg->nParam2 = (int)dY;
	pMsg->nParam3 = 0;

	m_pLiveItem->SdiAddMsg(pMsg);

	SetTimer(TIMER_BANK_SAVE, 1500, NULL);

	CView::OnLButtonUp(nFlags, point);
}

void CESMLiveview::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (!m_pLiveItem)
		return;

	if (!m_bLiveview)
		return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
		return;
	
	m_bClickedMouseL = TRUE;

	CView::OnLButtonDown(nFlags, point);
}

void CESMLiveview::OnMouseLeave()
{
	m_bMouseHoverFlag = FALSE;
	m_bClickedMouseL = FALSE;
	m_bMouseSleepFlag = FALSE;
	
	CView::OnMouseLeave();
}

void CESMLiveview::OnMouseMove(UINT nFlags, CPoint point)
{
	if (!m_pLiveItem)
		return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
		return;

	TRACKMOUSEEVENT trackMouseEvent;

	trackMouseEvent.cbSize = sizeof( trackMouseEvent );

	trackMouseEvent.dwFlags = TME_LEAVE | TME_HOVER;
	trackMouseEvent.hwndTrack = GetSafeHwnd();

	trackMouseEvent.dwHoverTime = 0x00000001;
	_TrackMouseEvent( &trackMouseEvent );

	CView::OnMouseMove(nFlags, point);
}

void CESMLiveview::OnMouseHover(UINT nFlags, CPoint point)
{
	if (!m_pLiveItem)
		return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
		return;

	m_bMouseHoverFlag = TRUE;
	CView::OnMouseHover(nFlags, point);
}

void CESMLiveview::SetCenterFocusFrame()
{
	if (!m_pLiveItem)
		return;

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_SDI_OP_SET_FOCUS_FRAME;
	pMsg->nParam1 = 500;
	pMsg->nParam2 = 500;
	pMsg->nParam3 = 0;

	m_pLiveItem->SdiAddMsg(pMsg);
}


BOOL CESMLiveview::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	if (!m_pLiveItem)
		return FALSE;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
		return FALSE;

	if (!m_bMouseHoverFlag)
		return FALSE;

	if (m_bMouseSleepFlag)
		return FALSE;

	m_bMouseSleepFlag = TRUE;

	int nMag = m_nFocusFrameMag;

	if (zDelta <=0) 
	{
		// down
		if (nMag > 9)
			return FALSE;

		if (nMag < 3)
			nMag = 3;
		else if (nMag > 2)
			nMag++;
	}
	else
	{
		// up
		if (nMag < 1)
			return FALSE;

		if (nMag < 3)
			nMag = 1;
		else if (nMag > 3)
			nMag--;
	}

	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_SDI_OP_SET_FOCUS_FRAME;
	pMsg->nParam1 = m_nFocusFrameX;
	pMsg->nParam2 = m_nFocusFrameY;
	pMsg->nParam3 = nMag;

	m_pLiveItem->SdiAddMsg(pMsg);	

	SetTimer(TIMER_BANK_SAVE, 1000, NULL);

	return CView::OnMouseWheel(nFlags, zDelta, pt);
}


void CESMLiveview::OnTimer(UINT_PTR nIDEvent)
{
	CView::OnTimer(nIDEvent);

	switch (nIDEvent)
	{
	case TIMER_BANK_SAVE:
		{
			BankSaveAll();

			KillTimer(TIMER_BANK_SAVE);

			m_bMouseSleepFlag = FALSE;
		}
		break;
	default:
		break;
	}
}

void CESMLiveview::BankSaveAll()
{
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	CString strValue;

	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		if (pItem == NULL)
			continue;

		ESMEvent* pSdiMsg = NULL;
		pSdiMsg = new ESMEvent;
		pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
		pSdiMsg->nParam1 = HIDDEN_COMMAND_BANK_SAVE;
		pSdiMsg->nParam2 = PTP_VALUE_NONE;
		pItem->SdiAddMsg(pSdiMsg);
	}
}
