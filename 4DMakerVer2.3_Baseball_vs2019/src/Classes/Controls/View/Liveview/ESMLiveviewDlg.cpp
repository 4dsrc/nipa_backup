////////////////////////////////////////////////////////////////////////////////
//
//	ESMLiveviewDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-23
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMLiveviewDlg.h"
#include "resource.h"
#include "MainFrm.h"
#include "SdiSingleMgr.h"
#include "SdiDefines.h"
#include "HalfShutterDlg.h"
#include "ZoomAllDlg.h"

//TimeTrace
#include <sys/timeb.h>
#include <time.h>

/////////////////////////////////////////////////////////////////////////////
// CESMLiveviewDlg dialog

CESMLiveviewDlg::CESMLiveviewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMLiveviewDlg::IDD, pParent)	
	, m_pLiveview	(NULL)	
	, m_pLiveItem	(NULL)
{	
	m_bLiveview = FALSE;
	m_bViewLager = 0;
	m_bIsAutoCalib = FALSE;
	m_nZoomValue = 0;
	m_background.CreateSolidBrush(RGB(44,44,44));
}

CESMLiveviewDlg::~CESMLiveviewDlg() 
{
	//CloseLiveview();
}

void CESMLiveviewDlg::CloseLiveview()
{
	//-- Send Finish Liveview Message
	if(m_pLiveItem)
	{
		OnHalfShutterAll(1);
		Disconnection(m_pLiveItem);
	}

}

void CESMLiveviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMLiveviewDlg)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);	
}

BOOL CESMLiveviewDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg)
	{
		if(pMsg->message == WM_KEYDOWN)
		{
			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			ESMEvent* pEsmMsg = NULL;
			pEsmMsg = new ESMEvent();
			pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
			pEsmMsg->pParam = (LPARAM)pMsg;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMLiveviewDlg, CDialog)
	//{{AFX_MSG_MAP(CESMLiveviewDlg)
	//}}AFX_MSG_MAP
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()	
	ON_WM_PAINT()
	ON_COMMAND(ID_IMAGE_START		, OnLiveOn)
	ON_COMMAND(ID_IMAGE_STOP		, OnLiveOff)
	ON_COMMAND(ID_IMAGE_LEFT		, OnBnClickedFarther)
	ON_COMMAND(ID_IMAGE_SMALE_LEFT	, OnBnClickedFar)
	ON_COMMAND(ID_IMAGE_SMALE_RIGHT	, OnBnClickedNear)
	ON_COMMAND(ID_IMAGE_RIGHT		, OnBnClickedNearer)
	ON_COMMAND(ID_IMAGE_VIEWLARGE	, OnViewLarge)
	ON_COMMAND(ID_IMAGE_AUTOCALIB	, OnAutoCalib)
	ON_COMMAND(ID_IMAGE_AUTOCALIBSTOP, OnAutoCalibStop)
	ON_COMMAND(ID_IMAGE_SAVEFOCUS	, OnSaveFocus)

	ON_WM_CLOSE()
	ON_COMMAND(ID_IMAGE_AUTO_FOCUS, &CESMLiveviewDlg::OnImageAutoFocus)
	ON_COMMAND(ID_IMAGE_ZOOMIN, &CESMLiveviewDlg::OnImageZoomin)
	ON_COMMAND(ID_IMAGE_ZOOMOUT, &CESMLiveviewDlg::OnImageZoomout)
	ON_WM_TIMER()
	ON_COMMAND(ID_IMAGE_AUTO_FOCUS_ALL, &CESMLiveviewDlg::OnImageAutoFocusAll)
	ON_COMMAND(ID_IMAGE_ZOOM_ALL, &CESMLiveviewDlg::OnImageZoomAll)
	ON_COMMAND(ID_IMAGE_TOUCH_AF, &CESMLiveviewDlg::OnImageTouchAf)
	ON_COMMAND(ID_IMAGE_TOUCH_AF_ALL, &CESMLiveviewDlg::OnImageTouchAfAll)
	ON_WM_CTLCOLOR()
	ON_COMMAND(ID_IMAGE_CENTER_FOCUS_FRAME, &CESMLiveviewDlg::OnImageCenterFocusFrame)
	ON_COMMAND(ID_IMAGE_CENTER_FOCUS_FRAME_ALL, &CESMLiveviewDlg::OnImageCenterFocusFrameAll)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CESMLiveviewDlg message handlers

BOOL CESMLiveviewDlg::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}	
	
	//-- SET MAIN WINDOWS HANDLE
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();

	//-- Create Liveview
	m_pLiveview = new CESMLiveview();
	m_pLiveview->Create(NULL,_T(""),WS_VISIBLE|WS_CHILD,CRect(CPoint(5,5),CSize(1000,1000)),this,IDC_LIVEVIEW_VIEW);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CESMLiveviewDlg

void CESMLiveviewDlg::OnPaint()
{	
	//-- DrawLiveview
	DrawLiveview();
	CDialog::OnPaint();
}

void CESMLiveviewDlg::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_START			,	
		ID_IMAGE_STOP			,
		ID_SEPARATOR			,
		ID_IMAGE_LEFT			,
		ID_IMAGE_SMALE_LEFT		,
		ID_IMAGE_SMALE_RIGHT	,
		ID_IMAGE_RIGHT			,
		ID_IMAGE_ZOOMIN			,
		ID_IMAGE_ZOOMOUT		,
		ID_IMAGE_ZOOM_ALL		,
		ID_SEPARATOR			,
		ID_IMAGE_AUTO_FOCUS		,
		ID_IMAGE_AUTO_FOCUS_ALL	,
		ID_IMAGE_TOUCH_AF		,
		ID_IMAGE_TOUCH_AF_ALL	,
		ID_IMAGE_SAVEFOCUS		, 
		ID_SEPARATOR			,
		ID_IMAGE_VIEWLARGE		,
		ID_SEPARATOR			,
		ID_IMAGE_AUTOCALIB		,
		ID_IMAGE_AUTOCALIBSTOP	,
		ID_SEPARATOR			,
		ID_IMAGE_CENTER_FOCUS_FRAME,
		ID_IMAGE_CENTER_FOCUS_FRAME_ALL,
	};

	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	

	m_wndToolBar.ShowWindow(SW_SHOW);
}


void CESMLiveviewDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	//m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;
	m_rcClientFrame.top	= 40;
	RepositionBars(0,0xFFFF,0);

	//-- RESIZE CONTROL
	m_pLiveview->ScalingLiveview(m_rcClientFrame);
	
	//-- 2011-05-30 Draw Liveview
	DrawLiveview();
}

CRect CESMLiveviewDlg::GetRect()
{
	CRect rect;
	rect.CopyRect(m_rcClientFrame);

	//-- SET MODE
	//-- FIX 3:2
	int nWidth = rect.Width();
	int nHeight = rect.Height();
	int nChange;
	
	//-- WIDTH LONGER THAN HEIGHT
	if(nHeight*16 < nWidth*9)
	{
		nChange = nHeight*16/9;
		rect.left	= nWidth/2 - nChange/2;
		rect.right	= nWidth/2 + nChange/2;
	}
	//-- HEIGHT LONGER THAN WIDTH
	else
	{
		nChange = nWidth*9/16;
		rect.top	= nHeight/2 - nChange/2 + 30;
		rect.bottom	= nHeight/2 + nChange/2 + 30;
	}

	return rect;
}


//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CESMLiveviewDlg::DrawLiveview()
{
	if(!m_pLiveview)
		return;

	CPaintDC dc(this);
	CRect rect;
	rect = GetRect();	

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	//-- 1. Background
	mDC.FillRect(rect, &CBrush(COLOR_BASIC_BG_DARK));

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);	
	Invalidate(FALSE);

	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();

	m_pLiveview->ScalingLiveview(rect);
}

void CESMLiveviewDlg::Connection(CDSCItem* pLiveItem)
{
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		pItem->InitLiveView();	// live view start > 4dmaker 재실행 > live view start > 영상겹치는 문제가있음
	}

	if(m_bLiveview)
	{
		if(m_pLiveItem == pLiveItem)
			return;

		if(!pLiveItem)
		{
			m_pLiveItem->SetLiveview(m_pLiveItem,FALSE);
			return;
		}

		if(m_pLiveItem)
		{
			if(pLiveItem != m_pLiveItem)
			{
				OnHalfShutter(pLiveItem, 1);
				m_pLiveItem->SetLiveview(m_pLiveItem,FALSE);		
			}
		}
		
		m_pLiveItem = pLiveItem;
		m_pLiveItem->SetLiveview(m_pLiveItem,TRUE);
		m_pLiveview->SetLiveview(TRUE, m_pLiveItem);

//		UpdateData(FALSE);
	}
	else
	{
		if(m_pLiveItem == pLiveItem)
			return;
		else
		{
			m_pLiveItem = pLiveItem;
		}
	}
}

void CESMLiveviewDlg::Disconnection(CDSCItem* pLiveItem)
{
	if(m_pLiveItem == pLiveItem)
	{
		m_pLiveItem->SetLiveview(m_pLiveItem,FALSE);
		//-- 2013-05-01 hongsu@esmlab.com
		//-- DRAW NONE
		if(m_pLiveview)
		{
			m_pLiveview->SetLiveview(FALSE);
		}		
		m_pLiveItem = NULL;
		return;
	}
	UpdateData(FALSE);
}

void CESMLiveviewDlg::SetBuffer(PVOID pBuffer)
{
	if(m_pLiveview)
	{
		m_pLiveview->SetBuffer(pBuffer);		
		m_pLiveview->Invalidate(FALSE);
	}
}

void CESMLiveviewDlg::SendFocusMsg(ULONG nFocusType)
{
	if (!m_pLiveItem) return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
	{
		int nValue = (int)nFocusType;
		ESMEvent* pMsg = new ESMEvent();
		pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
		pMsg->nParam1 = PTP_OC_SAMSUNG_SetFocusPosition;
		pMsg->nParam2 = nValue;
		pMsg->pParam = (LPARAM) nValue;

		m_pLiveItem->SdiAddMsg(pMsg);

		SetTimer(TIMER_TYPE_FOCUS_SAVE, 1000*2, NULL);
	}
	else
	{
		ESMEvent* pMsg = new ESMEvent();
		pMsg->message = WM_SDI_OP_SET_FOCUS;
		pMsg->pDest =	(LPARAM)m_pLiveItem;
		pMsg->nParam1 = nFocusType;
		m_pLiveItem->SdiAddMsg(pMsg);
	}
}

void CESMLiveviewDlg::OnLiveOn()
{
	ESMLog(5,_T("OnLiveOn"));
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		pItem->InitLiveView();	// live view start > 4dmaker 재실행 > live view start > 영상겹치는 문제가있음
	}

	m_bLiveview = TRUE;
	
#if 0
	if(m_pLiveItem)
		m_pLiveItem->SetLiveview(m_pLiveItem,TRUE);
	if(m_pLiveview)
		m_pLiveview->SetLiveview(TRUE, m_pLiveItem);
#endif

	if (m_pLiveItem == NULL)
	{
		if (arDSCList.GetSize() > 0)
			pItem = (CDSCItem*)arDSCList.GetAt(0);
	}
	else
		pItem = m_pLiveItem;

	ESMLog(5, _T("Liveview On : %s"), pItem->GetDeviceDSCID());

	if(pItem)
		pItem->SetLiveview(pItem,TRUE);
	if(m_pLiveview)
		m_pLiveview->SetLiveview(TRUE, pItem);

}

void CESMLiveviewDlg::OnLiveOff()
{
	m_bLiveview = FALSE;
	if(m_pLiveItem)
		m_pLiveItem->SetLiveview(m_pLiveItem,FALSE);
	if(m_pLiveview)
		m_pLiveview->SetLiveview(FALSE);

	ESMLog(5,_T("OnLiveOff"));
	ESMSetLiveViewLogFlag(FALSE);
}


void CESMLiveviewDlg::OnViewLarge()
{
	if( m_bViewLager == 0)
		m_bViewLager = 1;
	else
		m_bViewLager = 0;
	ESMEvent* pMsg;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_LIST_VIEWLARGE;
	pMsg->nParam1 = m_bViewLager;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
}

void CESMLiveviewDlg::OnAutoCalib()
{
	HANDLE hHandle = NULL;
	hHandle = (HANDLE) _beginthreadex(NULL, 0, AutoCalibThread, (void *)this, 0, NULL);
	CloseHandle(hHandle);
}

void CESMLiveviewDlg::OnAutoCalibStop()
{
	m_bIsAutoCalib = FALSE;
}

void CESMLiveviewDlg::OnSaveFocus()
{
	ESMLog(5, _T("Clicked Save Focus"));
	if( m_pLiveItem )
	{
		if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)	// GH5
		{
			// all save
			SetFocusSave(TRUE);
		}
		else
		{
			ESMEvent* pSendMsg = NULL;
			pSendMsg = new ESMEvent;
			pSendMsg->message = WM_SDI_OP_GETITEMFOCUS_VALUE;
			m_pLiveItem->SdiAddMsg(pSendMsg);
		}
	}
}


unsigned WINAPI CESMLiveviewDlg::AutoCalibThread(LPVOID param)
{
	CESMLiveviewDlg* pLiveviewDlg = (CESMLiveviewDlg*)param;
	if (!pLiveviewDlg) 
		return 0;

	pLiveviewDlg->m_bIsAutoCalib = TRUE;
	int nBrightGab = ESMGetValue(ESM_VALUE_ADJ_AUTOBRIGHTNESS);
	pLiveviewDlg->m_pLiveview->GetRGBAvg();
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;
	int nAll = arDSCList.GetCount();
	int nStandardAvg = 0, nRgbAvg = 0;

	ESMEvent* pMsg = NULL;
	for(int nIdx=0; nIdx<nAll; nIdx++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if (!pItem) break;

		pLiveviewDlg->Connection(pItem);
		Sleep(2000);
		if(pLiveviewDlg->m_pLiveview)
			nRgbAvg = pLiveviewDlg->m_pLiveview->GetRGBAvg();
		else 
			nRgbAvg = 0;

		if( nIdx == 0)	// 첫번째 카메라를 기준으로 삼는다.
			nStandardAvg = nRgbAvg;

		int nInterval = abs(nStandardAvg -nRgbAvg);

		ESMLog(5, _T("[%s] : FirstDscAvg[%d], RGBAvg[%d], BrightGab[%d], Gab[%d][%d/%d]"), pItem->GetDeviceDSCID(), nStandardAvg, nRgbAvg, nBrightGab, nInterval, nIdx, nAll);
		if(nStandardAvg > nRgbAvg + nBrightGab)
		{
			ESMLog(5, _T("[%s] : ISO Increase!!"), pItem->GetDeviceDSCID());
			pMsg = new ESMEvent;
			pMsg->pDest = (LPARAM)pItem;
			pMsg->message = WM_ESM_LIST_PROPERTY_ISO_INCREASE;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);	
		}
		else if(nStandardAvg < nRgbAvg - nBrightGab)
		{
			ESMLog(5, _T("[%s] : ISO Decrease!!"), pItem->GetDeviceDSCID());
			pMsg = new ESMEvent;
			pMsg->pDest = (LPARAM)pItem;
			pMsg->message = WM_ESM_LIST_PROPERTY_ISO_DECREASE;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);	
		}
		else
		{
			nStandardAvg = nRgbAvg;
		}

		if( pLiveviewDlg->m_bIsAutoCalib == FALSE)
			break;
	}
	AfxMessageBox(_T("Calibration is complete."));
	return 1;
}

void CESMLiveviewDlg::OnClose()
{
	CloseLiveview();
	CDialog::OnClose();
}

void CESMLiveviewDlg::OnBnClickedFarther()
{
	if (!m_pLiveItem) return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		SendFocusMsg(GH5_FOCUS_FAR_FAST);
	else
		SendFocusMsg(SDI_FOCUS_FARTHER);
}

void CESMLiveviewDlg::OnBnClickedFar()
{
	if (!m_pLiveItem) return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		SendFocusMsg(GH5_FOCUS_FAR_SLOW);
	else
		SendFocusMsg(SDI_FOCUS_FAR);
}

void CESMLiveviewDlg::OnBnClickedNear()
{
	if (!m_pLiveItem) return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		SendFocusMsg(GH5_FOCUS_NEAR_SLOW);
	else
		SendFocusMsg(SDI_FOCUS_NEAR);
}

void CESMLiveviewDlg::OnBnClickedNearer()
{
	if (!m_pLiveItem) return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		SendFocusMsg(GH5_FOCUS_NEAR_FAST);
	else
		SendFocusMsg(SDI_FOCUS_NEARER);
}

void CESMLiveviewDlg::OnHalfShutter(CDSCItem* pItem, int nRelease, int nReleaseSec)
{
	if (!pItem)
		return;
	
	ESMEvent* pSendMsg = NULL;
	pSendMsg = new ESMEvent;
	pSendMsg->message = WM_SDI_OP_HALF_SHUTTER;
	pSendMsg->nParam1 = nRelease;		// 0: press , 1: release, 2: one shot af
	pSendMsg->nParam2 = nReleaseSec;
	pItem->SdiAddMsg(pSendMsg);

	if (nRelease == 1)
		return;

	if(pItem->GetType() == DSC_LOCAL)
	{
		//SetTimer(TIMER_TYPE_AF, 1000*nReleaseSec, NULL);	// bank save 이전에 select item을 바꾸면 문제..
		if (nRelease == 0)
			SetTimer(TIMER_TYPE_AF_ALL, 1000*nReleaseSec, NULL);	
		else if (nRelease == 2)
			SetTimer(TIMER_TYPE_FOCUS_SAVE_ALL, 1000*nReleaseSec, NULL);	
	}
}

void CESMLiveviewDlg::OnHalfShutterAll( int nRelease , int nReleaseSec )
{
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	CString strValue;

	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		if (pItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
			continue;

		if (!nRelease)
		{
			CString strTemp;
			strTemp.Format(_T("Set Auto Focus [%s]"), pItem->GetDeviceDSCID());
			ESMLog(5, strTemp);
		}		

		OnHalfShutter(pItem, nRelease, nReleaseSec);
	}
}

void CESMLiveviewDlg::OnImageAutoFocus()
{
	ESMLog(5, _T("Clicked Auto focus (AFS/AFF/AFC Mode)"));

	if (!m_pLiveItem)
		return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
		return;

	CHalfShutterDlg dlg;
	if (dlg.DoModal() != IDOK)
		return;

	int nReleaseSec = dlg.GetReleaseSec();

	CString strTemp;
	strTemp.Format(_T("Set Auto Focus [%s]"), m_pLiveItem->GetDeviceDSCID());
	ESMLog(5, strTemp);
	
	OnHalfShutter(m_pLiveItem, 0, nReleaseSec);	
}

void CESMLiveviewDlg::OnImageAutoFocusAll()
{
	ESMLog(5, _T("Clicked Auto focus ALL (AFS/AFF/AFC Mode)"));

	CHalfShutterDlg dlg;
	if (dlg.DoModal() != IDOK)
		return;

	int nReleaseSec = dlg.GetReleaseSec();

	ESMLog(5, _T("Auto Focus ALL"));

	OnHalfShutterAll(0, nReleaseSec);	
}

void CESMLiveviewDlg::OnImageZoomin()
{
	if (!m_pLiveItem)
		return;

	m_nZoomValue++;

	if (m_nZoomValue > 10)
		m_nZoomValue = 10;

	if (m_nZoomValue < 3)
		m_nZoomValue = 3;

	SetZoom(m_nZoomValue);	
}


void CESMLiveviewDlg::OnImageZoomout()
{
	if (!m_pLiveItem)
		return;

	m_nZoomValue--;

	if (m_nZoomValue < 3)
		m_nZoomValue = 0;

	SetZoom(m_nZoomValue);	
}


void CESMLiveviewDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	switch (nIDEvent)
	{
	case TIMER_TYPE_AF:
		{
			OnHalfShutter(m_pLiveItem, 1);			

			SetFocusSave(FALSE);

			KillTimer(TIMER_TYPE_AF);
		}
		break;
	case TIMER_TYPE_AF_ALL:
		{
			OnHalfShutterAll(1);

			SetFocusSave(TRUE);

			KillTimer(TIMER_TYPE_AF_ALL);
		}
		break;
	case TIMER_TYPE_FOCUS_SAVE:
		{
			SetFocusSave(FALSE);

			KillTimer(TIMER_TYPE_FOCUS_SAVE);
		}
		break;
	case TIMER_TYPE_FOCUS_SAVE_ALL:
		{
			SetFocusSave(TRUE);

			KillTimer(TIMER_TYPE_FOCUS_SAVE_ALL);
		}		
		break;
	default:
		break;
	}
}

void CESMLiveviewDlg::SetFocusSave( BOOL nAll )
{
	if (nAll)
	{
		CDSCItem* pItem = NULL;
		CObArray arDSCList;
		CString strValue;

		ESMGetDSCList(&arDSCList);
		int nDSCCnt =  arDSCList.GetCount();
		for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
		{
			pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

			if (pItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
				continue;

			ESMEvent* pSdiMsg = NULL;
			pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_FOCUS_LOCATE_SAVE;
			pItem->SdiAddMsg(pSdiMsg);	

			pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
			pSdiMsg->nParam1 = HIDDEN_COMMAND_BANK_SAVE;
			pSdiMsg->nParam2 = PTP_VALUE_NONE;
			pItem->SdiAddMsg(pSdiMsg);
		}
	}
	else
	{
		if (!m_pLiveItem)
			return;

		if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
			return;

		ESMEvent* pSdiMsg = NULL;

		pSdiMsg = new ESMEvent;
		pSdiMsg->message = WM_SDI_OP_FOCUS_LOCATE_SAVE;
		m_pLiveItem->SdiAddMsg(pSdiMsg);

		pSdiMsg = new ESMEvent;
		pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
		pSdiMsg->nParam1 = HIDDEN_COMMAND_BANK_SAVE;
		pSdiMsg->nParam2 = PTP_VALUE_NONE;
		m_pLiveItem->SdiAddMsg(pSdiMsg);
	}
}

void CESMLiveviewDlg::SetZoom( int nValue )
{	
	if (!m_pLiveItem)
		return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
		return;

	SetZoom(m_pLiveItem, nValue);
}

void CESMLiveviewDlg::SetZoom( CDSCItem* pItem, int nValue )
{
	if (!pItem)
		return;

	if (pItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
		return;

	CString strTemp;
	strTemp.Format(_T("[%s]Set Zoom %d"), pItem->GetDeviceDSCID(), nValue);
	ESMLog(5, strTemp);

	ESMEvent* pSendMsg = NULL;
	pSendMsg = new ESMEvent;
	pSendMsg->message = WM_SDI_OP_SET_ENLARGE;
	pSendMsg->nParam1 = nValue;
	pItem->SdiAddMsg(pSendMsg);
}


void CESMLiveviewDlg::OnImageZoomAll()
{
	CZoomAllDlg dlg;
	dlg.DoModal();
}


void CESMLiveviewDlg::OnImageTouchAf()
{
	ESMLog(5, _T("Clicked Touch Auto focus (MF Mode)"));

	if (!m_pLiveItem)
		return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
	{
		CString strTemp;
		strTemp.Format(_T("Set Touch Auto Focus [%s]"), m_pLiveItem->GetDeviceDSCID());
		ESMLog(5, strTemp);

		OnHalfShutter(m_pLiveItem, 2);		// one shot AF
	}
	else
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_LIST_ONE_SHOT_AF;
		pMsg->nParam1 = 0;
		pMsg->pParam = (LPARAM)m_pLiveItem;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
	}
}


void CESMLiveviewDlg::OnImageTouchAfAll()
{
	ESMLog(5, _T("Clicked Touch Auto focus ALL (MF Mode)"));

// 	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
// 		return;

	CString strModel;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;
	for( int i =0 ;i < arDSCList.GetSize(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if (pItem == NULL)
			continue;

		strModel = pItem->GetDeviceModel();
		if (strModel.IsEmpty())
			continue;

		break;
	}

	if (strModel.CompareNoCase(_T("GH5")) == 0)
	{
		// GH5
		TouchAfAll(SDI_MODEL_GH5);		// one shot AF
	}
	else
	{
		// NX1
		TouchAfAll(SDI_MODEL_NX1);
	}
}

void CESMLiveviewDlg::TouchAfAll(int nModel)
{
	if (nModel == SDI_MODEL_GH5)
	{
		OnHalfShutterAll(2);
	}
	else if (nModel == SDI_MODEL_NX1)
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_LIST_ONE_SHOT_AF;
		pMsg->nParam1 = 1;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
	}
}


BOOL CESMLiveviewDlg::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);
	pDC->FillSolidRect( rect, RGB(44,44,44) );
	//return CDialog::OnEraseBkgnd(pDC);
	return TRUE;
}


HBRUSH CESMLiveviewDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	hbr = (HBRUSH)m_background;
	return hbr;
}


void CESMLiveviewDlg::OnImageCenterFocusFrame()
{
	if (!m_pLiveItem)
		return;

	if (m_pLiveItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
	{
		ESMLog(5, _T("[%s][Center Focus Frame Box] Not GH5 "), m_pLiveItem->GetDeviceDSCID());
		return;
	}

	if (m_pLiveItem->GetCamVersion() < 0x0100)
	{
		ESMLog(5, _T("[%s][Center Focus Frame Box] Please checked version[%d]"), m_pLiveItem->GetDeviceDSCID(), m_pLiveItem->GetCamVersion());
		return;
	}

	ESMLog(5, _T("[%s] Center Focus Frame Box "), m_pLiveItem->GetDeviceDSCID());

	m_pLiveview->SetCenterFocusFrame();

	SetTimer(TIMER_TYPE_FOCUS_SAVE_ALL, 1000*2, NULL);
}


void CESMLiveviewDlg::OnImageCenterFocusFrameAll()
{
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	CString strValue;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();

	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		if (pItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
			continue;

		ESMEvent* pMsg = new ESMEvent();
		pMsg->message = WM_SDI_OP_SET_FOCUS_FRAME;
		pMsg->nParam1 = 500;
		pMsg->nParam2 = 500;
		pMsg->nParam3 = 0;

		pItem->SdiAddMsg(pMsg);
	}

	ESMLog(5, _T("Center Focus Frame Box ALL"));

	SetTimer(TIMER_TYPE_FOCUS_SAVE_ALL, 1000*2, NULL);
}
