////////////////////////////////////////////////////////////////////////////////
//
//	ESMLiveview.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-23
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "SdiDefines.h"
#include "DSCItem.h"
// CESMLiveview view

class CESMLiveview : public CView
{
	DECLARE_DYNCREATE(CESMLiveview)
public:
	enum TIMER
	{
		TIMER_BANK_SAVE,
	};
public:
	CESMLiveview();	
	virtual ~CESMLiveview();

public:
	virtual void OnDraw(CDC* pDC) {};      // overridden to draw this view
	
protected:
	DECLARE_MESSAGE_MAP()

public:
	void SetFocusFrame(int nX, int nY, int nW, int nH, int nMag);
	void SetUseFocusFrame(BOOL b);
	void SetLiveview(BOOL b, CDSCItem* pItem=NULL);
	void SetBuffer(PVOID pBuffer);
	void DrawBlank();
	void DrawLiveview();	
	void ScalingLiveview(CRect rect);
	int GetRGBAvg();
	void BankSaveAll();

private:	
	CDSCItem* m_pLiveItem;
	BOOL m_bMouseHoverFlag;
	BOOL m_bClickedMouseL;
	int m_nFocusFrameX;
	int m_nFocusFrameY;
	int m_nFocusFrameH;
	int m_nFocusFrameW;
	int m_nFocusFrameMag;
	BOOL m_bMouseSleepFlag;
	BOOL m_bUseFocusFrame;	// gh5 focus frame
	BOOL m_bLiveview;		//-- Show or Not	
	int m_nRgbAvg;
	FrameData* m_pFrameData;
	afx_msg void OnPaint();	
	
public:
	void SetCenterFocusFrame();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseHover(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};

