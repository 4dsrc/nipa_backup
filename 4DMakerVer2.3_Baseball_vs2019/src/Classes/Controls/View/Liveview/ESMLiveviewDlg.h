////////////////////////////////////////////////////////////////////////////////
//
//	ESMLiveviewDlg.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-04-23
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxwin.h"
#include "resource.h"
#include "ESMLiveview.h"
#include "SdiDefines.h"

class CDSCItem;

/////////////////////////////////////////////////////////////////////////////
// CESMLiveviewDlg dialog

class CESMLiveviewDlg : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	// Construction
public:
	enum TIMER_TYPE
	{
		TIMER_TYPE_AF,
		TIMER_TYPE_AF_ALL,
		TIMER_TYPE_FOCUS_SAVE,
		TIMER_TYPE_FOCUS_SAVE_ALL,
	};
public:
	CESMLiveviewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMLiveviewDlg();

	// Dialog Data
	//{{AFX_DATA(CESMLiveviewDlg)
	enum { IDD = IDD_VIEW_ONLY_TOOLBAR };
	//}}AFX_DATA
	void InitImageFrameWnd();

	afx_msg void OnLiveOn();
	afx_msg void OnLiveOff();
	afx_msg void OnViewLarge();
	afx_msg void OnAutoCalib();
	afx_msg void OnAutoCalibStop();
	afx_msg void OnSaveFocus();

protected:
	//seo
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	CRect m_rcClientFrame;	
	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;	

	HWND m_hMainWnd;	
	CDSCItem* m_pLiveItem;
	BOOL m_bLiveview;
	int m_bViewLager;
	BOOL m_bIsAutoCalib;
	int m_nZoomValue;

private:	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMLiveviewDlg)
protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	afx_msg void OnBnClickedFarther();
	afx_msg void OnBnClickedFar();
	afx_msg void OnBnClickedNear();
	afx_msg void OnBnClickedNearer();
	afx_msg BOOL OnBarCheck(UINT nID);	
	DECLARE_MESSAGE_MAP()
	
	
protected:
	// Generated message map functions
	//{{AFX_MSG(CESMLiveviewDlg)	
	afx_msg void OnPaint();
	//}}AFX_MSG	
	afx_msg void OnSize(UINT nType, int cx, int cy);		

public:		
	void CloseLiveview();
	void DrawLiveview();
	void Connection(CDSCItem* pLiveSDI);
	void Disconnection(CDSCItem* pLiveSDI);
	CESMLiveview* GetLiveView() { return m_pLiveview; };

private:
	CESMLiveview* m_pLiveview;
	void RePositionControl(RECT &rect);

	void ViewerRect();
	CRect GetRect();	
	void SendFocusMsg(ULONG nFocusType);
	void SetFocusSave(BOOL nAll);	

public:	
	void SetZoom(int nValue);
	void SetZoom(CDSCItem* pItem, int nValue);
	void SetBuffer(PVOID pBuffer);
	static unsigned WINAPI AutoCalibThread(LPVOID param);
	afx_msg void OnClose();
	void OnHalfShutter(CDSCItem* pItem, int nRelease, int nReleaseSec=2);
	void OnHalfShutterAll(int nRelease, int nReleaseSec=2);
	afx_msg void OnImageAutoFocus();
	afx_msg void OnImageAutoFocusAll();
	afx_msg void OnImageZoomin();
	afx_msg void OnImageZoomout();
	afx_msg void OnTimer(UINT_PTR nIDEvent);	
	afx_msg void OnImageZoomAll();
	afx_msg void OnImageTouchAf();
	afx_msg void OnImageTouchAfAll();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void TouchAfAll(int nModel);

	CBrush m_background;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnImageCenterFocusFrame();
	afx_msg void OnImageCenterFocusFrameAll();
};
