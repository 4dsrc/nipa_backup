////////////////////////////////////////////////////////////////////////////////
//
//	ESMTimeLineObjectEditor.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-30
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ESMDropTarget.h"
#include "ESMDropSource.h"
#include "DSCViewDefine.h"
#include "ESMTimeLineObject.h"
#include "TimeLineView.h"
#include "ESMImgMgr.h"
#include "FFmpegManager.h"


//wgkim@esmlab.com
#include "ESMTemplateMgr.h"

class CDSCItem;
class CTimeLineView;

#define ESM_ADJ_CHEK_TIME	200

class CESMTimeLineObjectEditor : public CESMTimeLineObject
{
public:
	CESMTimeLineObjectEditor();
	//CESMTimeLineObjectEditor(const CObArray& ar, int nStartTime, int nEndTime, int nNextTime = movie_next_frame_time);
	//CMiLRe 20151022 Template Group 단위로 삭제
	CESMTimeLineObjectEditor(const CObArray& ar, int nStartTime, int nEndTime, int nNextTime, int nGroupNo = 0, int nStepFrame = 1);
	~CESMTimeLineObjectEditor();
	DECLARE_DYNCREATE(CESMTimeLineObjectEditor)

public:		
	BOOL DrawObject();		
	void DrawBackground(CDC* pDC, CRect rect);
	void DrawInfoTime(CDC* pDC, CRect rect);
	void DrawInfoDSC(CDC* pDC, CRect rect);
	void DrawFrame(CDC* pDC, CRect rect);
	void DrawInfoNo(CDC* pDC, CRect rect);
	void DrawInfoZoom(CDC* pDC, CRect rect);
	void DrawInfoCount(CDC* pDC, CRect rect);
public:
	//-- Create Movie File
	BOOL m_bMakeMovieDone;
	int  m_nObjectIndex;
	BOOL IsMakeMovie()			{ return m_bMakeMovieDone; }

	void SetMakeMovie(BOOL b)	{ m_bMakeMovieDone = b;		}
	void MakeMovie(int nIndex);
	void MakeMovieToClient(int nIndex);
	void Make3DMovie(int nIndex, vector<CString>* arrAdjedFile, CRITICAL_SECTION* CR_ADJProcess);
	
	void MakePictureMovie(int nIndex, vector<CString>* arrAdjedFile, CRITICAL_SECTION* CR_ADJProcess);
	void MakePicture3DMovie(int nIndex, vector<CString>* arrAdjedFile, CRITICAL_SECTION* CR_ADJProcess);

	BOOL AdJustPictureFile(CDSCItem* pItem, CString strDSC, CESMImgMgr* pImgMgr, CString strTargetFolder, int nSec, int nMilli, int nIndex);
	void OneDscMakeMovie(CDSCItem* pItem, CString strDSC, CESMImgMgr* pImgMgr, CString strTargetFolder);	// 이전 버전

	static unsigned WINAPI MakeObjMovieThread(LPVOID param);
	static unsigned WINAPI MakeMovieToClientThread(LPVOID param);
	static unsigned WINAPI MakeObj3DMovieThread(LPVOID param);
	
	static unsigned WINAPI MakePictureObjMovieThread(LPVOID param);
	static unsigned WINAPI MakePictureObj3DMovieThread(LPVOID param);

	BOOL CheckAdjustFile(CRITICAL_SECTION* pCR_ADJProcess, vector<CString>* pArrAdjedFile, CString strTargetFile);
	void SetMovieData(FFmpegManager* pFFmpegMgr, CDSCItem* pItem , int nStartTime, int nEndTime, CESMImgMgr* pImgMgr, BOOL bLogo = FALSE);
	
	//-- 2014-08-20 hjjang
	void SetMovieData(MakeMovieInfo* stMovieData, CDSCItem* pItem , int nStartTime, int nEndTime, int nFrameSpeed, int nMovieModule);
	void SetTimeLineInfo(int* nEffectStart, int* nEffectCnt, TimeLineInfo* stTineLineData, MakeMovieInfo stMovieData);
	//-- 2013-10-08 hongsu@esmlab.com
	//-- Draw Progress
	void SetProgress(float nProgress, int nKind);
	BOOL IsExistFrame(int nDscIndex, int nTimeIndex);

	// -- Effect
	void SaveEffectInfo(CArchive& ar);
	void LoadEffectInfo(CArchive& ar);
	BOOL IsEffectAdded();
	void InitSpotInfo();

	int  GetFrameData(vector<MakeFrameInfo>* pArrFrameInfo, int nObjectIndex,CString strTime);
	void SetFrameData(vector<MakeFrameInfo>* pArrFrameInfo, CDSCItem* pItem , int nStartTime, int nEndTime, int nFrameSpeed, int nObjectIndex,CString strTime, BOOL bSameDSC = FALSE);

private:
	int m_nBK;
	float m_fProgress;
	int m_nProgressKind;
	// 2014 0210 kcd
	// adj File Skip
	vector<CString>* m_arrAdjedFile;
	CRITICAL_SECTION* m_CR_ADJProcess;
	CRITICAL_SECTION m_csLock;

	int m_nCurrentBK;
public:
	void SetBK(int nBK) { m_nBK = nBK;}
	int GetBK()			{ return m_nBK;}

public:
	CRect   m_rtBox; 
	CDC*	m_memDC;
	BOOL	m_bImageLoad;
	void	SetRect(CRect rt)	{ m_rtBox = rt; }
	int		PtInObject(CPoint pt);	
	int		m_nMovieNotFinished;
	//CMiLRe 20151022 Template Group 단위로 삭제
	int     m_nGroupNo;
	int     GetTemplateGroupNO() {return m_nGroupNo;};
	void	SetTemplateGroupNO(int nValue) {m_nGroupNo = nValue;};
	CTimeLineView* pParentTimeLineView;

	BOOL	m_bSyncObj;

protected:
	// Generated message map functions
	//{{AFX_MSG(CESMTimeLineObjectEditor)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);	
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG	

private:
	LONG GetSelectMenu(CPoint point);
	void TimelineEdit();
public:
	void SetStartZoom( int nStartZoom ) {m_nZoom[FALSE] = nStartZoom;}
	int GetStartZoom() { return m_nZoom[FALSE];}
	void SetEndZoom( int nEndZoom ) {m_nZoom[TRUE] = nEndZoom;}
	int GetEndZoom() { return m_nZoom[TRUE];}

	void SetStartTime(int nStartTime) { m_nTime[FALSE] = nStartTime; }
	int GetStartTime() { return m_nTime[FALSE];}
	void SetEndTime(int nEndTime) { m_nTime[TRUE] = nEndTime; }
	int GetEndTime() { return m_nTime[TRUE]; }
	void SetNFT(int nNFT) { m_nTime[2] = nNFT; }
	void SetDSC(int nStartIndex, int nEnndIndex);
	void SetObjectFrame();
	void GetCenterIndex(int &nDscIndex, int &nTimeIndex);
	//  2013-10-15 Ryumin
	CBitmap bmpFrame;
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	//Adding Kzone Index in TimeLineView edited by hjcho at 161024 
	void SetKzoneIdx(int kzoneIdx) { m_nKzoneIdx = kzoneIdx; };
	int GetKzoneIdx() { return m_nKzoneIdx; };	

	void SetStepFrame( int nStepFrame ) {m_nStepFrame = nStepFrame;}
	int GetStepFrame() { return m_nStepFrame;}


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
};

