#pragma once

#include "afxcmn.h"
#include "ESMFunc.h"
#include "ESMDefine.h"
#include "ESMIndexStructure.h"
#include "ESMRCManager.h"

class CMainFrame;

#define MAX_MOVIE 4
class CESMMakeViewMove
{
public:
	CESMMakeViewMove();
	~CESMMakeViewMove();

	BOOL GetMakingFinish(){return m_bMakingFinish;}
	void SetMakingFinish(BOOL b){m_bMakingFinish = b;}

	BOOL GetThreadStop(){return m_bThreadStop;}
	void SetThreadStop(BOOL b){m_bThreadStop = b;}

	void Launch();
	void AddViewStruct(VIEW_STRUCT view);
	void DeleteViewStruct()
	{
		EnterCriticalSection(&m_criInsert);
		m_arrViewStruct.erase(m_arrViewStruct.begin());
		LeaveCriticalSection(&m_criInsert);
	}
	VIEW_STRUCT GetViewStruct()
	{
		VIEW_STRUCT view;
		EnterCriticalSection(&m_criInsert);
		view = m_arrViewStruct.at(0);
		LeaveCriticalSection(&m_criInsert);
		return view;
	}
	int GetViewStructCount()
	{
		int nCount = 0;
		EnterCriticalSection(&m_criInsert);
		nCount = m_arrViewStruct.size();
		LeaveCriticalSection(&m_criInsert);
		return nCount;
	}
	int SendImage(VIEW_STRUCT view);
	static unsigned WINAPI _LaunchThread(LPVOID param);

	CRITICAL_SECTION m_criInsert;

	void SetThreadRun(BOOL b){m_bThreadRun = b;}
	BOOL GetThreadRun(){return m_bThreadRun;}
	int m_nRecvCnt;
	int m_nSendCnt;
	void GetRecv(int nIdx,BOOL bSuccess);
	void CreateViewMovie(VIEW_STRUCT view,int nCount);
	void RunEncode(CString strTempPath,CString strMoviePath);
private:
	BOOL m_bThreadStop;
	BOOL m_bThreadRun;
	BOOL m_bMakingFinish;
	CObArray m_arRCServerList;
	CMainFrame *m_pMainWnd;
	vector<VIEW_STRUCT> m_arrViewStruct;
};