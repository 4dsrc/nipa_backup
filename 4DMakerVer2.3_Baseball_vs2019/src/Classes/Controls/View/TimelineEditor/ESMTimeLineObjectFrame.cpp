////////////////////////////////////////////////////////////////////////////////
//
//	ESMTimeLineObjectFrame.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "ESMTimeLineObject.h"
#include "ESMObjectFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2014-07-15
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMTimeLineObject::AddObjectFrame(CESMObjectFrame* pObjFrm)
{
	//-- Create Object
	m_arObjectFrame.Add((CObject*) pObjFrm);
}

void CESMTimeLineObject::DrawObjectFrame(CWnd* pWnd)
{
	CESMObjectFrame* pObjFrm = NULL;
	int nCnt = GetObjectFrameCount();
	for ( int i = 0 ; i < nCnt ; i++ )
	{
		pObjFrm = GetObjectFrame(i);
		pObjFrm->Create(NULL, NULL, DS_SETFONT | WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), pWnd, IDD_EFFECT_OBJ_FRAME);
		pObjFrm->DrawFrame();
	}
	
}

CESMObjectFrame* CESMTimeLineObject::GetObjectFrame(int nIndex)
{
	if(m_arObjectFrame.GetCount() -1 < nIndex)
		return NULL;
	return (CESMObjectFrame*)m_arObjectFrame.GetAt(nIndex);
}

CESMObjectFrame* CESMTimeLineObject::GetObjectFrame(CString strDSCID)
{
	if(m_arObjectFrame.GetCount() < 1)
		return NULL;
	CESMObjectFrame* pItem;
	for(int i = 0 ; i < m_arObjectFrame.GetCount() ; i++)
	{
		pItem = (CESMObjectFrame*)m_arObjectFrame.GetAt(i);
		if(pItem->GetDSC().CompareNoCase(strDSCID) == 0)
		{
			break;
		}
	}	
	return pItem;
}

//int CESMTimeLineObject::GetObjectFrameCount()
//{ 
//	return m_arObjectFrame.GetCount();
//}

void CESMTimeLineObject::RemoveAllObjectFrame()
{
	CESMObjectFrame* pObjFrm = NULL;
	int nAll = GetObjectFrameCount();
	while(nAll--)
	{
		pObjFrm = GetObjectFrame(nAll);
		if(pObjFrm)
		{
			pObjFrm->DestroyWindow();
			delete pObjFrm;
		}
		m_arObjectFrame.RemoveAt(nAll);
	}
	m_arObjectFrame.RemoveAll();
}

void CESMTimeLineObject::ShowFrame(BOOL bShow)
{
	int nStatus;
	if(bShow)
		nStatus = SW_SHOW;
	else
		nStatus = SW_HIDE;	

	CESMObjectFrame* pObjFrm = NULL;
	int nAll = GetObjectFrameCount();
	while(nAll--)
	{
		pObjFrm = GetObjectFrame(nAll);
		pObjFrm->ShowWindow(nStatus);
	}
}

void CESMTimeLineObject::ResetSelection()
{
	CESMObjectFrame* pObjFrm = NULL;
	int nAll = GetObjectFrameCount();
	while(nAll--)
	{
		pObjFrm = GetObjectFrame(nAll);
		pObjFrm->ResetSelection();
	}
}

int CESMTimeLineObject::GetObjectFrameOrder(CESMObjectFrame* pObjFrm)
{
	int nOrder = 0;
	CESMObjectFrame* pExist = NULL;
	int nAll = GetObjectFrameCount();
	for(int n = 0 ; n < nAll ; n ++)
	{
		pExist = GetObjectFrame(n);
		if(pExist == pObjFrm)
			return n;
	}	
	return -1;
}

int CESMTimeLineObject::GetInsertOrder(CESMObjectFrame* pObjFrm)
{
	int nOrder = 0;
	CESMObjectFrame* pExist = NULL;
	int nAll = GetObjectFrameCount();
	for(int n = 0 ; n < nAll ; n ++)
	{
		pExist = GetObjectFrame(n);
		if(pExist == pObjFrm)
			return nOrder;
		if(pExist->IsSpot())
			nOrder++;
	}	
	return nOrder;
}

#include "MainFrm.h"
void CESMTimeLineObject::RecalculateInfo()
{
	CESMObjectFrame* pExistA = NULL;
	CESMObjectFrame* pExistB = NULL;
	CESMObjectFrame* pExistFrame = NULL;
	int nAll = GetObjectFrameCount();
	if(!nAll)
		return;
	int nSpotNextIndex, nSpotIndex = 0;	
	int nX, nY;

	//-- 2014-07-21 hongsu@esmlab.com
	//-- Find Next  
	while(1)
	{
		//-- Find Spot Object
		nSpotNextIndex = FindNextSpotObject(nSpotIndex);		
		if(nSpotNextIndex == -1)
			break;

		//-- Get Frame (Between A & B)
		pExistA = GetObjectFrame(nSpotIndex);
		pExistB = GetObjectFrame(nSpotNextIndex);

		float nXSize = (float)(nSpotNextIndex-nSpotIndex);
		float nXpos, nYPos;
		float nWeight = (float)pExistB->GetZoomWeight();
		//-- Ratio
		float nStartRatio = (float)pExistA->GetZoomRatio();		
		float nRatio = pExistB->GetZoomRatio() - nStartRatio;		
		int nChangeRatio;

		//-- Position
		CPoint ptA = CPoint(pExistA->GetPosX(), pExistA->GetPosY());
		CPoint ptB = CPoint(pExistB->GetPosX(), pExistB->GetPosY());
		float nXdiv = (float)ptB.x - (float)ptA.x;
		float nYdiv = (float)ptB.y - (float)ptA.y;
		int nXchange, nYchange;

		int nIndex = nSpotIndex;		
		while(1)
		{
			nIndex++;			

			pExistFrame = GetObjectFrame(nIndex);
			if(pExistFrame)
			{
				nXpos = (float)(nIndex - nSpotIndex)/nXSize;
				nYPos = GetWeightPercentage(nXpos, nWeight);

				//-- Ratio
				nChangeRatio = (int)(nStartRatio + (nRatio * nYPos));
				pExistFrame->SetZoomRatio(nChangeRatio);
				//-- X
				nXchange = (int)(ptA.x + (nXdiv * nYPos));
				pExistFrame->SetPosX(nXchange);
				//-- Y
				nYchange = (int)(ptA.y + (nYdiv * nYPos));
				pExistFrame->SetPosY(nYchange);
			}

			if(nIndex == nSpotNextIndex)
				break;
		}		

		if(nSpotNextIndex == nAll-1)
			break;
		nSpotIndex = nSpotNextIndex;
	}	
}


//wgkim@esmlab.com
void CESMTimeLineObject::RecalculateInfo(CESMTemplateMgr* templateMgr)
{
	CESMObjectFrame* pExistA = NULL;
	CESMObjectFrame* pExistB = NULL;
	CESMObjectFrame* pExistFrame = NULL;
	int nAll = GetObjectFrameCount();
	if(!nAll)
		return;
	int nSpotNextIndex, nSpotIndex = 0;	
	int nX, nY;

	//-- 2014-07-21 hongsu@esmlab.com
	//-- Find Next  
	while(1)
	{
		//-- Find Spot Object
		nSpotNextIndex = FindNextSpotObject(nSpotIndex);		
		if(nSpotNextIndex == -1)
		{
			pExistFrame = GetObjectFrame(nSpotIndex);
			int dscIdIndex = templateMgr->GetDscIndexFrom(pExistFrame->GetDSC());
			
			//-- X			
			int nXchange = templateMgr->GetTemplatePoint(dscIdIndex).viewPoint.x;
			pExistFrame->SetPosX(nXchange);
			//-- Y			
			int nYchange = templateMgr->GetTemplatePoint(dscIdIndex).viewPoint.y;
			pExistFrame->SetPosY(nYchange);
			
			/*if(ESMGetValue(ESM_VALUE_REVERSE_MOVIE))
			{
				track_info stTrack= ESMGetTrackInfo();
				nXchange = stTrack.nWidth - templateMgr->GetTemplatePoint(dscIdIndex).viewPoint.x;
				nYchange = stTrack.nHeight - templateMgr->GetTemplatePoint(dscIdIndex).viewPoint.y;
			}*/

			//if(templateMgr->temp == true)
				templateMgr->CalcOverBoundary(nXchange, nYchange, pExistFrame->GetZoomRatio(), pExistFrame->GetDSC());			
			
			break;
		}

		//-- Get Frame (Between A & B)
		pExistA = GetObjectFrame(nSpotIndex);
		pExistB = GetObjectFrame(nSpotNextIndex);

		float nXSize = (float)(nSpotNextIndex-nSpotIndex);
		float nXpos, nYPos;
		float nWeight = (float)pExistB->GetZoomWeight();
		//-- Ratio
		float nStartRatio = (float)pExistA->GetZoomRatio();		
		float nRatio = pExistB->GetZoomRatio() - nStartRatio;		
		int nChangeRatio;

		//-- Position
		CPoint ptA = CPoint(pExistA->GetPosX(), pExistA->GetPosY());
		CPoint ptB = CPoint(pExistB->GetPosX(), pExistB->GetPosY());
		float nXdiv = (float)ptB.x - (float)ptA.x;
		float nYdiv = (float)ptB.y - (float)ptA.y;
		int nXchange, nYchange;

		int nIndex = nSpotIndex;		

		while(1)
		{	
			pExistFrame = GetObjectFrame(nIndex);
			int dscIdIndex = templateMgr->GetDscIndexFrom(pExistFrame->GetDSC());
			
			if(pExistFrame)
			{
				nXpos = (float)(nIndex - nSpotIndex)/nXSize;
				nYPos = GetWeightPercentage(nXpos, nWeight);

				//-- Ratio
				nChangeRatio = (int)(nStartRatio + (nRatio * nYPos));
				pExistFrame->SetZoomRatio(nChangeRatio);

				////줌에 따른 회전포인트, 뷰포인트 비율 조절
				//int deltaViewPointX = 0;
				//int deltaViewPointY = 0;
				//{
				//	double zoomRatio = pExistFrame->GetZoomRatio()/100.;
				//						
				//	deltaViewPointY = (templateMgr->GetTemplatePoint(dscIdIndex).centerPointOfRotation.y
				//		-templateMgr->GetTemplatePoint(dscIdIndex).viewPoint.y)*zoomRatio;
				//}

				////-- X
				//nXchange = templateMgr->GetTemplatePoint(dscIdIndex).viewPoint.x;
				//pExistFrame->SetPosX(nXchange);
				////-- Y
				//nYchange = templateMgr->GetTemplatePoint(dscIdIndex).viewPoint.y + deltaViewPointY*2;
				//pExistFrame->SetPosY(nYchange);				
				/////////////////////////////////////////////

				//-- X
				nXchange = templateMgr->GetTemplatePoint(dscIdIndex).viewPoint.x;
				pExistFrame->SetPosX(nXchange);
				//-- Y
				nYchange = templateMgr->GetTemplatePoint(dscIdIndex).viewPoint.y;
				pExistFrame->SetPosY(nYchange);				
				
				//if(templateMgr->temp == true)
					templateMgr->CalcOverBoundary(nXchange, nYchange, nChangeRatio, pExistFrame->GetDSC());				
			}					

			if(nIndex == nSpotNextIndex)
				break;

			nIndex++;
		}		

		if(nSpotNextIndex == nAll-1)
			break;
		nSpotIndex = nSpotNextIndex;
	}		

	for(int i = 0; i < GetObjectFrameCount(); i++)	
	{
		templateMgr->m_arrExistFrame.push_back(GetObjectFrame(i));	
	}
}

int CESMTimeLineObject::GetSpotCount()
{
	int nSpot = 0;
	CESMObjectFrame* pExist = NULL;
	int nAll = GetObjectFrameCount();
	while(nAll--)
	{
		pExist = GetObjectFrame(nAll);
		if(pExist->IsSpot())
			nSpot++;
	}
	return nSpot;
}

int CESMTimeLineObject::FindNextSpotObject(int nPrev)
{
	CESMObjectFrame* pExist = NULL;
	int nAll = GetObjectFrameCount();
	while(1)
	{
		nPrev++;
		if(nPrev > nAll-1)
			break;
		pExist = GetObjectFrame(nPrev);
		if(pExist->IsSpot())
			return nPrev;		
	}
	return -1;
}

BOOL CESMTimeLineObject::ClickFrame(int nOrder, BOOL bNext)
{
	int nAll = GetObjectFrameCount();
	//-- Check Order
	if(nOrder > nAll-1)	nOrder = 0;
	if(nOrder < 0)		nOrder = nAll-1;

	int nPrev;
	if(bNext)	// -->
	{
		if(nOrder == 0)		nPrev = nAll-1;
		else				nPrev = nOrder-1;
	}
	else		// <--
	{
		if(nOrder >= nAll-1)	nPrev = 0;
		else					nPrev = nOrder+1;
	}

	CESMObjectFrame* pExist = NULL;
	//-- 2014-07-22 hongsu@esmlab.com
	//-- Redraw Previous Frame 
	pExist = GetObjectFrame(nPrev);
	if(!pExist)
		return FALSE;
	if(pExist->m_nStatus != ESM_OBJECT_FRAME_STATUS_SPOT)
	{
		pExist->m_nStatus = ESM_OBJECT_FRAME_STATUS_NONE;
		pExist->DrawFrame();
	}

	//-- Select Frame
	pExist = GetObjectFrame(nOrder);
	if(!pExist)
		return FALSE;
	if(pExist->m_nStatus != ESM_OBJECT_FRAME_STATUS_SPOT)
	{
		pExist->m_nStatus = ESM_OBJECT_FRAME_STATUS_SELECT;
		pExist->DrawFrame();
	}
	pExist->SelectFrame();
	return TRUE;
}

//------------------------------------------------------------------------------
//! @function	Make Curves
//! @brief		Xⁿ+yⁿ= 1	
//! @date		2014-07-22
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
float CESMTimeLineObject::GetWeightPercentage(float fX, float fWeight)
{
	return 1- (pow( (1- pow(fX,fWeight)) , 1/fWeight));
}