////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineViewEdit.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-10-01
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TimeLineView.h"
#include "DSCViewer.h"
#include "ESMTimeLineObjectEditor.h"
#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CTimeLineView::Make3DMovie()
{
	//-- 2013-10-19 hongsu@esmlab.com
	//-- 2D Movie 
	m_bCreate3D = TRUE;

	HANDLE handle;
	handle = (HANDLE) _beginthread( _DoMake3DMovie, 0, (void*)this); // create thread
}

void CTimeLineView::MakePicture3DMovie()
{
	//-- 2013-10-19 hongsu@esmlab.com
	//-- 2D Movie 
	m_bCreate3D = TRUE;

	HANDLE handle;
	handle = (HANDLE) _beginthreadex(NULL, 0, MakePicture3DMovieThread, (void *)this, 0, NULL);
	CloseHandle(handle);
}

//-- 2013-12-12 kjpark@esmlab.com
//-- if converting, toolbar disenable
void ConvterToolBarShow3D(CTimeLineView* pView, BOOL isShow)
{
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_TOOLBAR_SHOW;
	pMsg->pDest		= isShow;			
	::SendMessage(pView->GetTimeLineViewHandle(),WM_ESM, (WPARAM)WM_ESM_FRAME,(LPARAM)pMsg);
}

void _DoMake3DMovie(void *param)
{
	CTimeLineView* pView = (CTimeLineView*)param;
	if(!pView)
		return;
	
	//-- 2013-12-12 kjpark@esmlab.com
	//-- if converting, toolbar disenable
	ConvterToolBarShow3D(pView, FALSE);

	//-- 2013-10-04 hongsu@esmlab.com
	//-- move Previous Output file
	CString strMovieFile, strFile, strFolder;
	strMovieFile.Format(_T("%s"),ESMGetPath(ESM_PATH_OUTPUT));
	
	CESMFileOperation fo;
	//-- delete adjust files
	CString strAdjustFile;
	strAdjustFile.Format(_T("%s"),ESMGetPath(ESM_PATH_ADJUST));
	fo.Delete(strAdjustFile,FALSE);

	//-- 2013-10-19 hongsu@esmlab.com
	//-- 2D or 3D
	if(pView->m_bCreate3D)	strMovieFile.Append(_T("\\3D"));
	else					strMovieFile.Append(_T("\\2D"));

	//-- 2013-10-09 jaehyun@esmlab.com
	//-- Output Type option
// 	BOOL bOutputMP4 = ESMGetValue(ESM_VALUE_ADJ_OUT_MP4);
// 	if(bOutputMP4)
// 		strMovieFile.Append(_T("\\4dmaker.mp4"));
// 	else
// 		strMovieFile.Append(_T("\\4dmaker.ts"));

	strMovieFile.Append(_T("\\4dmaker.mp4"));
	fo.Delete(strMovieFile);


	//-- Make ts file on each object
	CESMTimeLineObjectEditor* pObj = NULL;
	int nAll = pView->GetObjCount();
	vector<CString> arrAdjedFile;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pObj = pView->GetObj(i);
		if(!pObj)
			break;
		pObj->Make3DMovie(i, &arrAdjedFile, &pView->m_CR_ADJProcess);
	}	


	//-- 2013-10-01 hongsu@esmlab.com
	//-- Check Finish Make Movies 
	BOOL bFinish;
	while(1)
	{
		bFinish = TRUE;
		Sleep(10);
		for(int i = 0 ; i < nAll ; i ++)
		{
			pObj = pView->GetObj(i);
			if(!pObj)
				break;
			if(!pObj->IsMakeMovie())
				bFinish = FALSE;
		}		
		if(bFinish)
			break;
	}



	CString strCmd, strOpt, strOpt1, strOpt2;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));	

	if (nAll < 2)
	{
		//! => ffmpeg -i foo.ts -c copy foo.mp4
		strFile.Format(_T("-i \"%s\\%d\\%d.ts\""), ESMGetPath(ESM_PATH_OBJECT), 0,0);
		strOpt.Format(_T("%s -c copy -bsf:a aac_adtstoasc "),strFile);		
	}
	else
	{
		//-- 2013-10-01 hongsu@esmlab.com
		//-- Combine Each Movies from Object
		//! => ffmpeg -i "concat:intermediate1.ts | intermediate2.ts" -c copy -bsf:a aac_adtstoasc output.mp4
		//! => ffmpeg -i "concat:C:\Program Files\ESMLab\4DMaker\result\movie\object\0\0.ts|C:\Program Files\ESMLab\4DMaker\result\movie\object\1\1.ts|C:\Program Files\ESMLab\4DMaker\result\movie\object\2\2.ts" 
		//			  -c copy -bsf:a aac_adtstoasc "C:\Program Files\ESMLab\4DMaker\result\movie\object\output.mp4"	
		strOpt.Format(_T("-i \"concat:"));
		for(int i = 0 ; i < nAll ; i ++)
		{
			if(i>0)
				strOpt.Append(_T("|"));
			strFile.Format(_T("%s\\%d\\%d.ts"), ESMGetPath(ESM_PATH_OBJECT), i,i);
			strOpt.Append(strFile);
		}		
		strOpt.Append(_T("\" -c copy -bsf:a aac_adtstoasc "));
	}
	
	//-- 2013-10-19 hongsu@esmlab.com
	//-- Add Result File Name
	strOpt.Append(_T("\""));
	strOpt.Append(strMovieFile);
	strOpt.Append(_T("\""));

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Log 
	ESMLog(1,_T("[Movie] Create 3D Movie ... [%s]"),strFile);

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}

	ESMLog(1,_T("[Movie] Make 3D Movie Success [%s]"),strFile);
	//-- 2013-12-12 kjpark@esmlab.com
	//-- if converting, toolbar disenable
	ConvterToolBarShow3D(pView, TRUE);

	if(ESMGetValue(ESM_VALUE_MAKEAUTOPLAY))
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_VIEW_MAKEMOVIEPLAY;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	}
}

unsigned WINAPI CTimeLineView::MakePicture3DMovieThread(LPVOID param)
{
	CTimeLineView* pTimeLineView = (CTimeLineView*)param;
	if(!pTimeLineView)
		return 0;

	//-- 2013-12-12 kjpark@esmlab.com
	//-- if converting, toolbar disenable
	ConvterToolBarShow3D(pTimeLineView, FALSE);

	CString strMovieFile, strFile, strFolder;
	strMovieFile.Format(_T("%s"),ESMGetPath(ESM_PATH_OUTPUT));


	CESMFileOperation fo;
	CString strAdjustFile;
	strAdjustFile.Format(_T("%s"),ESMGetPath(ESM_PATH_ADJUST));
	fo.Delete(strAdjustFile,FALSE);

	//-- 2013-10-19 hongsu@esmlab.com
	//-- 2D or 3D
	if(pTimeLineView->m_bCreate3D)	strMovieFile.Append(_T("\\3D"));
	else					strMovieFile.Append(_T("\\2D"));

	//-- 2013-10-09 jaehyun@esmlab.com
	//-- Output Type option
// 	BOOL bOutputMP4 = ESMGetValue(ESM_VALUE_ADJ_OUT_MP4);
// 	if(bOutputMP4)
// 		strMovieFile.Append(_T("\\4dmaker.mp4"));
// 	else
// 		strMovieFile.Append(_T("\\4dmaker.ts"));

	strMovieFile.Append(_T("\\4dmaker.mp4"));
	fo.Delete(strMovieFile);

	//-- Make ts file on each object
	CESMTimeLineObjectEditor* pObj = NULL;
	int nAll = pTimeLineView->GetObjCount();
	vector<CString> arrAdjedFile;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pObj = pTimeLineView->GetObj(i);
		if(!pObj)
			break;
		pObj->MakePicture3DMovie(i, &arrAdjedFile, &pTimeLineView->m_CR_ADJProcess);
	}	

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Check Finish Make Movies 
	BOOL bFinish;
	while(1)
	{
		bFinish = TRUE;
		Sleep(10);
		for(int i = 0 ; i < nAll ; i ++)
		{
			pObj = pTimeLineView->GetObj(i);
			if(!pObj)
				break;
			if(!pObj->IsMakeMovie())
				bFinish = FALSE;
		}		
		if(bFinish)
			break;
	}



	CString strCmd, strOpt, strOpt1, strOpt2;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));	

	if (nAll < 2)
	{
		//! => ffmpeg -i foo.ts -c copy foo.mp4
		strFile.Format(_T("-i \"%s\\%d\\%d.ts\""), ESMGetPath(ESM_PATH_OBJECT), 0,0);
		strOpt.Format(_T("%s -c copy -bsf:a aac_adtstoasc "),strFile);		
	}
	else
	{
		//-- 2013-10-01 hongsu@esmlab.com
		//-- Combine Each Movies from Object
		//! => ffmpeg -i "concat:intermediate1.ts | intermediate2.ts" -c copy -bsf:a aac_adtstoasc output.mp4
		//! => ffmpeg -i "concat:C:\Program Files\ESMLab\4DMaker\result\movie\object\0\0.ts|C:\Program Files\ESMLab\4DMaker\result\movie\object\1\1.ts|C:\Program Files\ESMLab\4DMaker\result\movie\object\2\2.ts" 
		//			  -c copy -bsf:a aac_adtstoasc "C:\Program Files\ESMLab\4DMaker\result\movie\object\output.mp4"	
		strOpt.Format(_T("-i \"concat:"));
		for(int i = 0 ; i < nAll ; i ++)
		{
			if(i>0)
				strOpt.Append(_T("|"));
			strFile.Format(_T("%s\\%d\\%d.ts"), ESMGetPath(ESM_PATH_OBJECT), i,i);
			strOpt.Append(strFile);
		}		strOpt.Append(_T("\" -c copy -bsf:a aac_adtstoasc "));
	}

	//-- 2013-10-19 hongsu@esmlab.com
	//-- Add Result File Name
	strOpt.Append(_T("\""));
	strOpt.Append(strMovieFile);
	strOpt.Append(_T("\""));

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Log 
	ESMLog(1,_T("[Movie] Create 3D Movie ... [%s]"),strFile);

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}

	ESMLog(1,_T("[Movie] Make 3D Movie Success [%s]"),strFile);
	//-- 2013-12-12 kjpark@esmlab.com
	//-- if converting, toolbar disenable
	ConvterToolBarShow3D(pTimeLineView, TRUE);

	if(ESMGetValue(ESM_VALUE_MAKEAUTOPLAY))
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_VIEW_MAKEMOVIEPLAY;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	}
	return 0;
}