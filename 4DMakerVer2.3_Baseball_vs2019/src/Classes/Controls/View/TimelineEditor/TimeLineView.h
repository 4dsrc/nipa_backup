////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineView.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-05
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "io.h"
#include "resource.h"
#include "DSCItem.h"
#include "TimeLineViewBase.h"
#include "ESMCtrl.h"
#include "ESMTimeLineObjectEditor.h"
#include "ESMMovieMetadataMgr.h"

#include "ESMStabilizationMgr.h"
#include "ESMMakeViewMovie.h"

void _DoMakeMovie(void *param);
void _DoMakeMovieDP(void *param);
void _DoMakeMovieAdjustDP(void *param);
void _DoMake3DMovie(void *param);
void _DrawTimeLineEiditor(void *param);
void _DoMakeMovieFrameBaseMovie(void *param);
void _DoMakeMovieFrameBaseAdjustMovie(void *param);

#define PLAYDELETFRAME
#include "DSCGroup.h"

struct movieInfo
{
	CDSCGroup *pGroup;
	int fileNum;
	vector<MakeFrameInfo> m_MovieInfo;
	BOOL makingFin;
	BOOL serverFlag;
};

struct MakingData
{
	MakingData()
	{
		nSendCount = 0;
		nRecvCount = 0;
		nAJACount = 0;
	}
	int nSendCount;
	int nRecvCount;
	int nAJACount;
};

typedef struct MAKE_GROUP_FRAME
{
	BOOL bPlayMaking;
	vector<int> arryCount;
	vector<int> arryPlayCount;
	CESMArray* arDSCGroup;
	CESMArray* arPlayDSCGroup;
	int nSendGourpCount;
	int nPlay;
	CDSCGroup* pGroup;
	int nTotalFrameCount;
	int nPlayFrameCount;
	int nDivision;
	int nCPUMakeNet;
	int nCPUMakeCount;
	int nRemainder;
	int nNetMgrCount;
	int nPlayDivision;
	int nPlayMgrCount;
	int nPlaynet;
	BOOL bFirst;
	CString strTime;
	int nAJASendCnt;
	int nAJARotFrameIdx;
}_MAKE_GROUP_FRAME;

class CDSCViewer;
class CESMTimeLineObjectEditor;

class CTimeLineView : public CTimeLineViewBase
{
	//DECLARE_DYNCREATE(CTimeLineView)
public:
	CTimeLineView();
	~CTimeLineView();

public:
	
	void DrawEditor();
	
	void DrawObjects(CDC* pDC);
	void DrawObject(CDC* pDC, CESMTimeLineObjectEditor* pObject, CPoint* pPoint);
	void RemoveObject();
	void RemoveObject(CESMTimeLineObjectEditor* pObject);
	void RemoveAll();	
	//CMiLRe 20151020 TimeLine Object Delete Key 추가
	void RemoveObjectTail();	
	void RemoveObjectHeader();
	
	void DrawTimeLine(CDC* pDC);
	void CheckOverObject(CPoint point);
	void CheckOverReset();
	void CalculateTime();
	void InitMovieFile();

	//vector<MakeFrameInfo>* m_pArrFrameInfo;
	int m_nSendCount;
	int m_nRecvCount;
	//vector<MakingData>* pArrMakingData;
	map<CString, MakingData> map_MakingData;
	int m_nAdjMovieIndex;

	vector<CString> m_arrMoviePath;
	vector<CString> m_IPList;
	vector<CString> m_IPAllList;
	vector<int> m_PCList;
	vector<int> m_PCAllList;
	vector<int> m_NoMakingList;
	vector<movieInfo> m_movieMakingList;

	void GetIpList();
	void SetPCList();
	void GetIpAllList();
	void SetPCAllList();
	void SetNoMakingList();
	void CheckValidFrame(vector<MakeFrameInfo>* pArrFrameInfo);
	void MakeMovieFrameBaseAdjustMovie(vector<MakeFrameInfo>* pArrFrameInfo, ESMAdjustInfo info);
	void MakeMovieFrameBaseMovie(vector<MakeFrameInfo>* pArrFrameInfo);
	void MakeMovieFrameBaseAdjust(ESMEvent* pMsg);
	void MakeMovieFrameBase(ESMEvent* pMsg);
	void MakeMovieFrameBaseMovieFinish(ESMEvent* pMsg);

	void RequestData(ESMEvent* pMsg);

	void WaitReciveMovie(CString strTime);
	void WaitReciveAdjustMovie(int nCount);
	void MergeMovie(CString strTime);
	void MergeArrayMovie();
	void MergeAdjustMovie(CString strTargetPath, ESMAdjustMovie& info);
	//CMiLRe 20151119 동영상 사이즈(FHD, UHD) Check
	int CheckImageSize(vector<MakeFrameInfo>* pArrMovieInfo);
	int CheckAdjustImageSize(CString strPath);
	void ChangeMovieSize(CString strMovieFile, CString strConcertPath);
	BOOL ISFILE(CString szFile);
	
	//170330 hjcho
	FrameRGBInfo DecodeImage(vector<MakeFrameInfo>*pArrFrameInfo);

	//170612 hjcho
	void WriteIni(CString strFilePath);
	static unsigned WINAPI MakeMovieFinishThread(LPVOID param);
	//180531 hjcho
	CString GetMoviePathBaseOnIP(CString strPath);

	//190323 hjcho
	void SaveDecodeImage(vector<MakeFrameInfo>*pArrFrameInfo);
public:
	BOOL PlayNetworkCheck(MAKE_GROUP_FRAME& info, vector<MakeFrameInfo>* pArrFrameInfo);
	void PlayCountCheck(MAKE_GROUP_FRAME& info, vector<MakeFrameInfo>* pArrFrameInfo);
	BOOL CalculateDivision(MAKE_GROUP_FRAME& info);
	void StartAJAThreadInit(MAKE_GROUP_FRAME& info);
	void StartMuxingView();
	void MakingGroupFrame(MAKE_GROUP_FRAME& info, vector<MakeFrameInfo>* pArrFrameInfo);
public:
	BOOL	m_bCreate3D;		
	int		m_nInsertObjPos;
	CRITICAL_SECTION m_CR_ADJProcess;
	BOOL	m_bMovieMaking;
	void*	m_pMainFrm;
	int		m_nImageSize;
	int m_nMakingThreadIndex;
	static int m_nMakedThreadIndex;

public:

	//------------------------------------------------------------------------------
	//! @function	Control Object
	//------------------------------------------------------------------------------
	CObArray m_arObject;
	void InsertObj(CESMTimeLineObjectEditor* pObj,BOOL bPos);
	void AddObj(CESMTimeLineObjectEditor* pObj, BOOL bProcess = FALSE);
	//20151015 Template VMCC Save/Load
	void AddObj(CESMTimeLineObjectEditor* pObj, CESMTemplateMgr* templateMgr, vector<TEMPLATE_VMCC_STRUCT>* verVMCC_Data);
	int GetObjCount() { return m_arObject.GetCount();}
	int GetOrder(CESMTimeLineObjectEditor* pCurObj);
	void SetMovieMaking(BOOL bMovieMaking) {m_bMovieMaking = bMovieMaking; }
	BOOL GetMovieMaking() {return m_bMovieMaking; }
	CESMTimeLineObjectEditor* GetPrevObj(CESMTimeLineObjectEditor* pCurObj);
	CESMTimeLineObjectEditor* GetNextObj(CESMTimeLineObjectEditor* pCurObj);
	CESMTimeLineObjectEditor* GetObj(int nIndex);
	CESMTimeLineObjectEditor* GetSelectedObj();
	void ReCalculateTime();

	//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
	void SyncEffectInfo(CESMTimeLineObjectEditor* pSelectedObj, CESMTimeLineObjectEditor* pObj, int nFrameIdx, BOOL nAllChange = FALSE);
	void Swap(int idx1, int idx2);

	//-- 2013-10-08 hongsu@esmlab.com
	//-- Draw Progress Percentage
	void SetProgress(float nProgress, int nObj, int nKind);
	
	//------------------------------------------------------------------------------
	//! @function	Control Object
	//------------------------------------------------------------------------------
	void MakeAdjustMovie();
	void MakeMovie();
	void MakePictureMovie();
	void Make3DMovie();
	void MakePicture3DMovie();
	
	//wgkim@esmlab.com 17-05-18	
	static unsigned WINAPI MakeStabiliationMovie(LPVOID param);
	HANDLE m_hThreadHandle;
	CString m_strMovieFile;

	//wgkim 180211
	void SetStabilizationInterval(vector<MakeFrameInfo>* pArrFrameInfo);
	int m_stabilizerStartFrameNumber;
	int m_stabilizerEndFrameNumber;


	//wgkim@esmlab.com 17-07-25	
	void GetMetadata(CString strFile);
	void SetMetadata(CString strMovieFile, CString m_strTotalMetadata);
	CString m_strTotalMetadata;

	CStringArray m_arrDSCFolder;
	BOOL CheckMakeDSCFolder(CString strDSC);

	static unsigned WINAPI MakePictureMovieThread(LPVOID param);
	static unsigned WINAPI MakePicture3DMovieThread(LPVOID param);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimeLineView)
protected:
	virtual void OnDraw(CDC* pDC);   // overridden to draw this view
		
	virtual BOOL OnDrop(CWnd* pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point );		// Drop after Drag
	virtual DROPEFFECT OnDragOver(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point );
	
	//}}AFX_VIRTUAL	
	DECLARE_MESSAGE_MAP()

public:	
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);

	//CMiLRe 20151022 Template Group 단위로 삭제
	int m_nGroupTemplateNO;
	void SetTemplateGroupNO(int nValue) {m_nGroupTemplateNO = nValue;};
	int GetTemplateGroupNO() {return m_nGroupTemplateNO;};
	void SetTemplateGroupIncrease() {m_nGroupTemplateNO++;};
	void SetTemplateGroupDecrease() {m_nGroupTemplateNO--;};

	int m_nAgentPC;

	BOOL m_bDrawSec;
	void SetDrawSec(BOOL bDraw) { m_bDrawSec = bDraw; }
	BOOL GetDrawSec() { return m_bDrawSec; }

	CString m_strStatus;
	void SetDrawStatus(CString strStatus) { m_strStatus = strStatus; }
	CString GetDrawStatus() { return m_strStatus; }

	void DeleteMovie();

	CMap<UINT, UINT, UINT, UINT> m_Client;
	void SetClientId(UINT nFile, UINT nIp);
	UINT GetClientId(UINT nFile);
	void SetClientInit() {m_Client.RemoveAll();}
	virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
	//180510 hjcho
	void SendAJAGPUFrameInfo(CString strPath,int nMakingIndex,int nStartIndex,int nEndIndex,double dbZoomRatio,stFPoint ptPos,int nFrameRate,int nReverse);
	void SendAJAGPUFrameInfo(vector<MakeFrameInfo>* pMovieInfo, int nMovieNum);
	void SendAJACPUFrameInfo(int nIdx);
	BOOL m_bAJAMaking;

	//180629 hjcho
	void CreateGifFile(CString strPath,CString strDate,CString str4DMName);
	static unsigned WINAPI _LaunchGIFFile(LPVOID param);

	//181221 hjcho
	CESMMakeViewMove* m_pViewMovie;
	vector<VIEW_STRUCT>m_arrViewSturct;
	int GetViewStructCount(){return m_arrViewSturct.size();}
	void AddViewStruct(VIEW_STRUCT view){m_arrViewSturct.push_back(view);}
	VIEW_STRUCT GetViewStruct(){return m_arrViewSturct.at(0);}
	void DeleteViewStruct(){m_arrViewSturct.erase(m_arrViewSturct.begin());}
};
struct GIFTHREADDATA
{
	CString strPath;
	CString strDate;
	CString str4DMName;
	CTimeLineView* pParent;
};
struct FrameThreadData
{
	ESMAdjustInfo info;
	CTimeLineView * pTimeLineView;
	vector<MakeFrameInfo>* pArrFrameInfo;
};
