
#include "stdafx.h"
#include "ESMTimeLineObjectEditor.h"
#include "resource.h"

#include "DSCViewDefine.h"
#include "DSCItem.h"
#include "ESMCtrl.h"
#include "ESMFileOperation.h"
#include "ESMObjectFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


void CESMTimeLineObjectEditor::SaveEffectInfo(CArchive& ar)
{
	int nCnt = GetObjectFrameCount();

	int nSpotCnt = GetSpotCount();
	ar<<nSpotCnt;

	for ( int i=0 ; i<nCnt ; i++)
	{
		CESMObjectFrame* pObj = GetObjectFrame(i);
		if ( pObj->IsSpot() == TRUE)
		{
			int nOrder = GetObjectFrameOrder(pObj);

			double dRatioTime;
			if ( nCnt == nOrder + 1 )
				dRatioTime = 1;
			else
				dRatioTime = (double)nOrder / (double)nCnt;
			double dRatioX	  = (double)(pObj->GetPosX()) / (double)FULL_HD_WIDTH;
			double dRatioY	  = (double)(pObj->GetPosY()) / (double)FULL_HD_HEIGHT;
			int nZoom	= pObj->GetZoomRatio();
			float dWeight	= pObj->GetZoomWeight();

			ar<<dRatioTime;
			ar<<dRatioX;
			ar<<dRatioY;
			ar<<nZoom;
			ar<<dWeight;

			// Write to vmcc file
		}
	}
}

void CESMTimeLineObjectEditor::LoadEffectInfo(CArchive& ar)
{
	int nCnt = GetObjectFrameCount();
	int nSpotCnt;
	ar>>nSpotCnt;

	InitSpotInfo();

	for ( int i=0 ; i<nSpotCnt ; i++)
	{
		// Read vmcc File
		double dRatioTime, dRatioX, dRatioY;
		int nZoom;
		float dWeight;

		ar>>dRatioTime;
		ar>>dRatioX;
		ar>>dRatioY;
		ar>>nZoom;
		ar>>dWeight;

		// Calculate Ratio
		int nOrder, nPosX, nPosY;

		nOrder =round(dRatioTime*(double)nCnt);
		if ( nOrder == nCnt )
			nOrder = nCnt - 1;
		nPosX =round(dRatioX*(double)FULL_HD_WIDTH);
		nPosY =round(dRatioY*(double)FULL_HD_HEIGHT);

		// Set Spot
		CESMObjectFrame* pObj = GetObjectFrame(nOrder);
		pObj->SetSpot(TRUE);
		pObj->SetZoom(TRUE);
		pObj->SetPosX(nPosX);
		pObj->SetPosY(nPosY);
		pObj->SetZoomRatio(nZoom);
		pObj->SetZoomWeight(dWeight);
	}

	RecalculateInfo();
}

void CESMTimeLineObjectEditor::InitSpotInfo()
{
	int nCnt = GetObjectFrameCount();
	CESMObjectFrame* pObj = NULL;
	for ( int i=0 ; i<nCnt ; i++)
	{
		pObj = GetObjectFrame(i);

		if ( pObj->IsSpot() == TRUE )
			pObj->InitEffectInfo();
	}

	// Create Base Spot
	int nAll = GetObjectFrameCount();

	pObj = GetObjectFrame(0);
	if(pObj)
	{
		pObj->SetSpot(TRUE);
		pObj->m_nStatus = ESM_OBJECT_FRAME_STATUS_SPOT;
		pObj->DrawFrame();
	}

	//-- Create Last
	pObj = GetObjectFrame(nAll-1);
	if(pObj)
	{
		pObj->SetSpot(TRUE);
		pObj->m_nStatus = ESM_OBJECT_FRAME_STATUS_SPOT;
		pObj->DrawFrame();
	}
}

BOOL CESMTimeLineObjectEditor::IsEffectAdded()
{
	if ( GetSpotCount() != 2 )
		return TRUE;

	CESMObjectFrame* pObj = NULL;
	for ( int i=0 ; i<GetObjectFrameCount() ; i++ )
	{
		pObj = GetObjectFrame(i);
		if ( pObj->IsSpot() == TRUE )
		{
			if ( pObj->IsBaseSpot() == FALSE )
				return TRUE;
		}
	}

	return FALSE;
}
