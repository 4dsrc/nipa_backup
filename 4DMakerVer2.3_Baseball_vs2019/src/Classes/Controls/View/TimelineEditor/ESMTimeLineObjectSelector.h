////////////////////////////////////////////////////////////////////////////////
//
//	ESMTimeLineObjectSelector.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-30
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ESMDropTarget.h"
#include "ESMDropSource.h"
#include "DSCViewDefine.h"
#include "ESMTimeLineObject.h"

class CDSCItem;

class CESMTimeLineObjectSelector : public CESMTimeLineObject
{
public:
	CESMTimeLineObjectSelector();
	CESMTimeLineObjectSelector(const CObArray& ar, int nStartTime, int nEndTime, int nNextTime  = movie_next_frame_time);
	virtual ~CESMTimeLineObjectSelector() {};
	DECLARE_DYNCREATE(CESMTimeLineObjectSelector)

private:
	BOOL m_bChangePos[2];	// FALSE(0) -> width : TRUE(1) -> height

public:		
	BOOL DrawObject();		

	void SetChangePos(BOOL bChange, BOOL bHeight = FALSE)	{ m_bChangePos[bHeight] = bChange;}

protected:
	// Generated message map functions
	//{{AFX_MSG(CESMTimeLineObjectSelector)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);	
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);	
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG	

protected:
	DECLARE_MESSAGE_MAP()
};

