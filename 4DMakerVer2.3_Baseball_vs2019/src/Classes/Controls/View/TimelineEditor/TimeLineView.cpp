////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-27
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCViewer.h"
#include "TimeLineView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//IMPLEMENT_DYNCREATE(CTimeLineView, CTimeLineViewBase)
BEGIN_MESSAGE_MAP(CTimeLineView, CTimeLineViewBase)
	//{{AFX_MSG_MAP(CTimeLineView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()	
	//}}AFX_MSG_MAP
	ON_WM_DESTROY()
	ON_WM_KEYUP()
END_MESSAGE_MAP()


int CTimeLineView::m_nMakedThreadIndex = 1;

// CTimeLineView
CTimeLineView::CTimeLineView():CTimeLineViewBase()
{
	m_nAdjMovieIndex = 0;
	m_nInsertObjPos	= 0;
	m_bMovieMaking = FALSE;
	m_nSendCount	= 0;
	m_nRecvCount	= 0;
	//CMiLRe 20151022 Template Group 단위로 삭제
	m_nGroupTemplateNO = 0;
	//m_nImageSize	= 0;
	m_nImageSize	= -1;
	m_nMakingThreadIndex = 0;

	m_bDrawSec = TRUE;
	m_strStatus = _T("");
	m_bCreate3D = FALSE;

	m_hThreadHandle = NULL;

	//wgkim@esmlab.com 180211
	m_stabilizerStartFrameNumber = 0;
	m_stabilizerEndFrameNumber = 0;

	InitializeCriticalSection (&m_CR_ADJProcess);

	m_pViewMovie = new CESMMakeViewMove();
}

CTimeLineView::~CTimeLineView()
{
	RemoveObject();
	DeleteCriticalSection (&m_CR_ADJProcess);

	if(m_pViewMovie)
	{
		delete m_pViewMovie;
		m_pViewMovie = NULL;
	}
}

void CTimeLineView::OnDraw(CDC* pDC)
{
	// update the scroll bars
	UpdateSizes();
	DrawEditor();	
}

void CTimeLineView::RemoveObject()
{
	int nAll = m_arObject.GetCount();
	CESMTimeLineObjectEditor* pObj = NULL;

	while(nAll--)
	{
		pObj = GetObj(nAll);
		if(pObj)
		{
			pObj->DestroyWindow();
			delete pObj;
			pObj = NULL;
		}
		m_arObject.RemoveAt(nAll);
	}
	m_arObject.RemoveAll();

	// Reset Effect Editor
 	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_REMOVEALL_OBJ_EDITOR;
	pMsg->pDest		= (LPARAM)this;

	//CESMTimeLineObject::OnRButtonDown(nFlags, point);	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	
}

CESMTimeLineObjectEditor* CTimeLineView::GetObj(int nIndex)
{ 
	int nAll = m_arObject.GetCount();
	if(nAll > nIndex)
		return (CESMTimeLineObjectEditor*)m_arObject.GetAt(nIndex);
	return NULL;
}

CESMTimeLineObjectEditor* CTimeLineView::GetPrevObj(CESMTimeLineObjectEditor* pCurObj)
{ 
	int nIdx = GetOrder(pCurObj);
	
	if ( nIdx == 0 )
		return NULL;
	else
		return GetObj(nIdx-1);
}

CESMTimeLineObjectEditor* CTimeLineView::GetNextObj(CESMTimeLineObjectEditor* pCurObj)
{ 
	int nCnt = GetObjCount();
	int nIdx = GetOrder(pCurObj);

	if ( nIdx + 2 > nCnt )
		return NULL;
	else
		return GetObj( nIdx + 1 );
}

int CTimeLineView::GetOrder(CESMTimeLineObjectEditor* pCurObj)
{ 
	int nAll = m_arObject.GetCount();
	CESMTimeLineObjectEditor* pObj = NULL;

	while(nAll--)
	{
		pObj = GetObj(nAll);
		if(pObj == pCurObj)
			return nAll;
	}
	
	return -1;
}

CESMTimeLineObjectEditor* CTimeLineView::GetSelectedObj()
{
	CESMTimeLineObjectEditor* pObjectEditor = NULL;
	for(int i =0 ; i < GetObjCount(); i++)
	{
		pObjectEditor = GetObj(i);
		CString strTp;

		if( pObjectEditor->GetBK() == TIMELINE_BACKGROUND_ALL)
		{
			return pObjectEditor;
		}
	}

	return NULL;
}


void CTimeLineView::Swap(int nFirst, int nSecond)
{
	CObject *pFirst, *pSecond;

	pFirst = m_arObject.GetAt(nFirst);
	pSecond = m_arObject.GetAt(nSecond);
	m_arObject.SetAt(nFirst, pSecond);
	m_arObject.SetAt(nSecond, pFirst);

}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------

void CTimeLineView::RemoveObject(CESMTimeLineObjectEditor* pObject)
{
	int nAll = m_arObject.GetCount();
	CESMTimeLineObjectEditor* pExist = NULL;
	while(nAll--)
	{
		pExist = GetObj(nAll);
		if(pExist == pObject)
		{
			pExist->DestroyWindow();
			delete pExist;		
			pExist = NULL;
			m_arObject.RemoveAt(nAll);
			//CMiLRe 20151022 Template Group 단위로 삭제
			SetTemplateGroupDecrease();
			return;
		}		
	}
}

void CTimeLineView::RemoveAll()
{
	int nAll = m_arObject.GetSize();
	while(nAll--)
	{
		CESMTimeLineObjectEditor* pExist = GetObj(nAll);
		if(pExist)
		{
			pExist->DestroyWindow();
			delete pExist;
			pExist = NULL;
			m_arObject.RemoveAt(nAll);
		}	
		else
		{
			m_arObject.RemoveAll();
			nAll = 0;
		}
	}
	SetTemplateGroupNO(0);
}

//CMiLRe 20151020 TimeLine Object Delete Key 추가
//CMiLRe 20151022 Template Group 단위로 삭제
void CTimeLineView::RemoveObjectTail()
{
	int nAll = m_arObject.GetSize();
	BOOL isDeleteObject = FALSE;
	while(nAll--)
	{
		CESMTimeLineObjectEditor* pExist = GetObj(nAll);
		if(pExist)
		{
			int nGroupNO = pExist->GetTemplateGroupNO();
			if(nGroupNO == (GetTemplateGroupNO()-1))
			{
				delete pExist;
				pExist = NULL;
				m_arObject.RemoveAt(nAll);
				isDeleteObject = TRUE;
			}				
		}			
	}
	if(isDeleteObject) {SetTemplateGroupDecrease();}

}

//CMiLRe 20151020 TimeLine Object Delete Key 추가
//CMiLRe 20151022 Template Group 단위로 삭제
void CTimeLineView::RemoveObjectHeader()
{
	int nAll = m_arObject.GetSize();
	BOOL isDeleteObject = FALSE;
	while(nAll--)
	{
		CESMTimeLineObjectEditor* pExist = GetObj(nAll);
		if(pExist)
		{
			int nGroupNO = pExist->GetTemplateGroupNO();
			if(nGroupNO == 0)
			{
				delete pExist;
				pExist = NULL;
				m_arObject.RemoveAt(nAll);
				isDeleteObject = TRUE;
			}	
			else
			{
				pExist->SetTemplateGroupNO(nGroupNO-1);					
				continue;
			}
		}			
	}
	if(isDeleteObject) {SetTemplateGroupDecrease();}
}

void CTimeLineView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch (nChar)
	{
	//CMiLRe 20151022 Template Group 단위로 삭제
	case VK_DELETE:
	case VK_DECIMAL:
	case VK_BACK:
		CESMTimeLineObjectEditor* pSelectedEditor = NULL;

		for(int i =0 ; i < this->GetObjCount(); i++)
		{
			pSelectedEditor = this->GetObj(i);
			CString strTp;

			if( pSelectedEditor->GetBK() == TIMELINE_BACKGROUND_ALL)
				break;
		}

		if ( pSelectedEditor == NULL )
			return;

		//-- Remove This
		ESMEvent* pMsg	= new ESMEvent;
		//CMiLRe 20151022 Template Group 단위로 삭제
		if(nChar == VK_BACK)
			pMsg->message	= WM_ESM_FRAME_REMOVE_OBJ_HEADER;
		else
			pMsg->message	= WM_ESM_FRAME_REMOVE_OBJ_TAIL;
		pMsg->pDest		= (LPARAM)pSelectedEditor;

		//CESMTimeLineObject::OnRButtonDown(nFlags, point);	
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	
		return;
		break;
	}

	__super::OnKeyUp(nChar, nRepCnt, nFlags);
}


BOOL CTimeLineView::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		BOOL bShift = ((GetKeyState(VK_SHIFT) & 0x8000) != 0); // Shift 키가 눌렸는지의 여부 저장
		ESMSetSyncObj(bShift);
	}

	if(pMsg->message == WM_KEYUP)
	{
		ESMSetSyncObj(FALSE);
	}

	return CTimeLineViewBase::PreTranslateMessage(pMsg);
}
