////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineEditor.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-27
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include <vector>
#include "ToolTipListCtrl.h"

//-- 2011-07-27 hongsu.jung
#include "TimeLineView.h"

#include "ESMDropTarget.h"
#include "ESMDropSource.h"

//	2014.11.04	Ryumin
#include "ESMTimerEx.h"

//CMiLRe 20151013 Template Load INI File
#ifndef CMiLRe 
#include "ESMIni.h"
#include <map>
using namespace std;

#include "ESMTemplateMgr.h"

#endif

class CDSCItem;
class CTimeLineView;
class CMainFrame;

// CESMFrameNailDlg dialog
class CTimeLineEditor: public CDialog
{
	//-- For Toolbar
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	//DECLARE_DYNAMIC(CTimeLineEditor)
public:
	CTimeLineEditor(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTimeLineEditor();

	//-- For Toolbar
	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CTimeLineEditor)
	enum { IDD = IDD_VIEW_ONLY_TOOLBAR };
	//}}AFX_DATA

public:	
	CTimeLineView*	m_pTimeLineView;
	DWORD m_nMakeTime;
	int m_nRunningTime;
	CMainFrame* m_pMainWnd;

protected:
	//seo
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	CRect m_rcClientFrame;	
	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;	

public:	
	void UpdateFrames();
	void Reset()	{ m_pTimeLineView->RemoveObject(); }
	void SetProgress(float nProgress, int nObj, int nKind) { m_pTimeLineView->SetProgress(nProgress, nObj, nKind);}
	//20151015 Template VMCC Save/Load
	void MakeTimeLine(int nDscStart, int nDscEnd, int nStartTime, int nEndTime, int nNextTime = movie_next_frame_time);
	//CMiLRe 20151022 Template Group 단위로 삭제
	//CMiLRe 20151119 Sensor Sync Check 하여 실제 제작해야할 DSC ID 설정
	void MakeTimeLine(int nDscStart, int nDscEnd, int nStartTime, int nEndTime, int nNextTime, int nGroupNO, int KzoneIdx, vector<TEMPLATE_VMCC_STRUCT>* verVMCC_Data = NULL, int nStepFrame = 1);
	//CMiLRe 20151013 Template Load INI File
	//CMiLRe 20151202 VMCC 템플리 호출 함수 추가
	void VmccTamplate(int nTemplate = -1);
	void VmccTamplate(int nRecTime, int nSelectTime, int nTemplate = -1);
	void MakeBasicTamplete();
	void MakeBasicTampleteRe();
	void MakeAdjustTamplete();
	//20151015 Template VMCC Save/Load
	//CMiLRe 20151022 Template Group 단위로 삭제
	void MakeTampleteMake(int nStartDSC, int nEndDSC, int nGroupNO, vector<TEMPLATE_STRUCT> vTemplate);
	void MakeTampleteMake(int nStartDSC, int nEndDSC, int nSlectedTime, int nGroupNO, vector<TEMPLATE_STRUCT> vTemplate);
	void MakeBasicTampleteFreze();
	void MakeBasicTampleteFrezeRe();
	void MakeBasicHG();
	BOOL CheckSaveValidation();
	void SaveMovieTemplate(CArchive& ar);
	void LoadMovieTemplate(CArchive& ar);
	void SaveTimeLineTemplate(CArchive& ar);
	void LoadTimeLineTemplate(CArchive& ar);

	void OutputImage(CString strFileName, int nCurFrameIndex);

	//-- 2014-10-22 cygil@esmlab.com
	BOOL GetExistMovieDsc(int nDSCIdx);		//DSC의 정상영상 여부 확인
	int FindExistMovieDsc(int nDSCIdx);		//DSC의 영상이 없는경우 앞뒤 DSC의 정상 영상이 있는지 찾는다

	void SaveTimeLineInfo(CArchive& ar);
	void LoadTimeLineInfo(CArchive& ar, float nVersion);
	void ItemAllDelete();

	//CMiLRe 20151119 영상 저장 경로 변경
	void DSCMoviePlay			(CString strParh = _T(""));
	afx_msg void OnDSCMoviePlay			();
	afx_msg void OnDSCMakeMovie			();
	afx_msg void OnDSCMakeMovie3D		();
	afx_msg void OnDSCADJMakeMovie		();
protected:
	afx_msg void OnDSCSCaling			();
	afx_msg void OnItemLeftMove			();
	afx_msg void OnItemRightMove		();
	afx_msg void OnItemDeleteAll		();
	//CMiLRe 20151013 Template Load INI File
	afx_msg void OnItemTemplateRefresh		();
	//CMiLRe 20151014 Template Save INI File
	afx_msg void OnItemTemplateSave			();	
	afx_msg void OnItemTemplateTimeSync();
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	afx_msg void OnDestroy();

	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnReload(WPARAM , LPARAM );

	afx_msg void OnUpdateDSCTool(CCmdUI *pCmdUI);	
	DECLARE_MESSAGE_MAP()

	//-- 2013-12-12 kjpark@esmlab.com
	//-- if converting, toolbar disenable
public:
	void SetToolbarShowNHide(BOOL isShow);


	////////////////////////////////////////////////////////////////////////
	//	2014.11.04	Ryumin
	TTimerEx<CTimeLineEditor> m_tTimerEx;
	//	타이머 이벤트를 이용하여 입력 받은 인터벌 후 함수 자동 실행
	void  MovieStop(const CString& strPlayerName, UINT RunningTime, UINT nInterval = 3000);
	//	팟 플레이어 메인 핸들에 F4 키 메세지 전달
	//	플레이이 중 또는 플레이가 끝난 동영상을 닫는다.
	void  SendMovieStopMessage();

	//CMiLRe 20151013 Template Load INI File	
	map<int,vector<TEMPLATE_STRUCT>> m_TemplateVector;
	vector<TEMPLATE_STRUCT> LoadTemplateElement(int nNum, BOOL bDSCCheck = FALSE);
	void LoadTemplateFromMvtmFile(CString strFileName = _T("C:\\CMiLRe.mvtm"), int nNum = 0, BOOL bDSCCheck = FALSE);

	//CMiLRe 20151014 Template Save INI File
	void TemplateDataSave(CString strFilename);
	void ResetTemplate() {m_TemplateVector.clear();};
	void ResetViewTemplate(){m_stViewTemplate.clear();};

	static unsigned WINAPI FocusObserverThread(LPVOID param);	

public:
	//16-12-12 wgkim@esmlab.com
	void SetTemplateMgr(CESMTemplateMgr *adjustMgr);

	void UpdateAllPosition(int n_PosX, int n_PosY);

	BOOL m_bUsingTemplatePoint;
	CESMTemplateMgr *m_pTemplateMgr;

	//17-05-23
	vector<TEMPLATE_STRUCT> m_pLoadTemplateVector;

	void LoadTemplateRecovery(CString strFileName = _T("C:\\CMiLRe.mvtm"), int nNum = 0, BOOL bDSCCheck = FALSE);
	
	//18-12-21
	map<int,VIEW_STRUCT> m_stViewTemplate;
	void ViewTemplate(int nTemplate = -1);
	VIEW_STRUCT GetViewTempalte(int nTemplate = -1);
};