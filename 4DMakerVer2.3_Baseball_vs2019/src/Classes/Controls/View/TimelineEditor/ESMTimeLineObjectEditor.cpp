////////////////////////////////////////////////////////////////////////////////
//
//	ESMTimeLineObject.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMTimeLineObjectEditor.h"
#include "resource.h"

#include "DSCViewDefine.h"
#include "DSCItem.h"
#include "ESMCtrl.h"
#include "ESMFileOperation.h"
#include "TimeLineObjectEditorDlg.h"
#include "ESMObjectFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMTimeLineObject
IMPLEMENT_DYNCREATE(CESMTimeLineObjectEditor, CWnd)
BEGIN_MESSAGE_MAP(CESMTimeLineObjectEditor, CWnd)	
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()


CESMTimeLineObjectEditor::CESMTimeLineObjectEditor() 
{
	pParentTimeLineView = NULL;
	m_arrAdjedFile = NULL;
	m_memDC = NULL;
	m_bImageLoad = FALSE;
	m_nMovieNotFinished = FALSE;
	//CMiLRe 20151022 Template Group 단위로 삭제
	m_nGroupNo = 0;
	m_bSyncObj = FALSE;
	InitializeCriticalSection(&m_csLock);
}

//CMiLRe 20151022 Template Group 단위로 삭제
CESMTimeLineObjectEditor::CESMTimeLineObjectEditor(const CObArray& ar, int nStartTime, int nEndTime, int nNextTime, int nGroupNo, int nStepFrame)
	: CESMTimeLineObject(ar, nStartTime, nEndTime, nNextTime, nGroupNo, nStepFrame)
{
	pParentTimeLineView = NULL;
	m_nBK = TIMELINE_BACKGROUND_NON;
	m_nProgressKind = ESM_ADJ_KIND_NULL;
	m_fProgress = 0;
	m_arrAdjedFile = NULL;
	m_memDC = NULL;
	m_bImageLoad = FALSE;
	m_nMovieNotFinished = FALSE;
	//CMiLRe 20151022 Template Group 단위로 삭제
	m_nGroupNo = nGroupNo;

	m_nCurrentBK = TIMELINE_BACKGROUND_NON;
	m_bSyncObj = FALSE;
	InitializeCriticalSection(&m_csLock);
}

CESMTimeLineObjectEditor::~CESMTimeLineObjectEditor()
{
	if( m_memDC)
	{
		m_memDC->DeleteDC();
		delete m_memDC;
	}
	WaitForSingleObject(m_hMakeObjMovie, INFINITE);
	CloseHandle(m_hMakeObjMovie);

	while(m_hMake3DMovie)
	{
		m_bClose = TRUE;
		Sleep(1);
	}
	CloseHandle(m_hMake3DMovie);

	DeleteCriticalSection(&m_csLock);

	pParentTimeLineView = NULL;
}

BOOL CESMTimeLineObjectEditor::DrawObject()
{
	if(!m_hWnd)	return FALSE;

	CDC *pDC = GetDC();
	if(!pDC) return FALSE;

	CRect rect;
	GetClientRect(&rect);
	if(!rect.Width() || !rect.Height())	return FALSE;

	CDC		mDC;
	CBitmap mBitmap;
	mDC.CreateCompatibleDC(pDC);
	mBitmap.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height()); 

	mDC.SelectObject(&mBitmap);	
	mDC.PatBlt(0,0,rect.Width(),rect.Height(),SRCCOPY);
	
	SetFrameFont(&mDC, 13, _T("Noto Sans CJK KR Regular"));
	//-- Draw Background 
	DrawBackground(&mDC, rect);

	CRect rectTop, rectBody1, rectBody2, rectBody3, rectCenter;
	
	rectTop.top		= DSC_TIMELINE_MARGIN;
	rectTop.bottom	= rectTop.top	+ DSC_TIMELINE_TOP;
	rectTop.left	= rect.left		+ DSC_TIMELINE_MARGIN;
	rectTop.right	= rect.right	- DSC_TIMELINE_MARGIN; //jhhan 17-01-05 이미지 간격 조정

	rectBody1.top	= rectTop.bottom;
	rectBody1.bottom	= rectBody1.top	+ DSC_TIMELINE_TOP;
	rectBody1.left	= rectTop.left	;
	rectBody1.right	= rectTop.right	;

	rectBody2.top	= rectBody1.bottom;
	rectBody2.bottom	= rectBody2.top	+ DSC_TIMELINE_TOP;
	rectBody2.left	= rectTop.left	;
	rectBody2.right	= rectTop.right	;

	

	rectBody3.top	= rectBody2.bottom;
	//rectBody3.bottom	= rectBody3.top	+ DSC_TIMELINE_BODY + DSC_TIMELINE_MARGIN;
	rectBody3.bottom	= rectBody3.top	+ DSC_TIMELINE_BODY - (DSC_TIMELINE_TOP * 2) + DSC_TIMELINE_MARGIN;
	rectBody3.left	= rectTop.left	;
	rectBody3.right	= rectTop.right	;

	rectCenter.top	= rectBody2.bottom;
	rectCenter.bottom	= rectBody3.top	+ DSC_TIMELINE_TOP;
	rectCenter.left	= rectTop.left	;
	rectCenter.right	= rectTop.right	;
	
	

	mDC.FillSolidRect(rectTop	, COLOR_TL_INFO_TOP	);	//#4D615F
	mDC.FillSolidRect(rectBody1	, COLOR_TL_INFO_BODY);	//#BED7D4
	mDC.FillSolidRect(rectBody2	, COLOR_TL_INFO_BODY);	//#BED7D4
	mDC.FillSolidRect(rectCenter, COLOR_TL_INFO_BODY);	//#BED7D4
	mDC.FillSolidRect(rectBody3	, COLOR_TL_INFO_BODY);	//#BED7D4
	

	rectBody1.bottom	-= DSC_TIMELINE_MARGIN;

	//-- Draw Start/End Time 
	DrawInfoTime(&mDC, rectTop);

	if(GetProcessMode() != TRUE)
	{
		//-- Draw No
		DrawInfoNo(&mDC, rectBody1);

		//-- Draw Zoom
		DrawInfoZoom(&mDC, rectBody2);

		//-- Draw Count
		DrawInfoCount(&mDC, rectCenter);
	}
	//-- Draw DSC
	DrawInfoDSC(&mDC, rectBody3);

	//-- Draw Thumbnail
	EnterCriticalSection(&m_csLock);
	
	//-- 2014-06-25 hjjang 임시 주석처리
	if( ESMGetValue(ESM_VALUE_TIMELINEPREVIEW))
		DrawFrame(&mDC, rectBody1);
	LeaveCriticalSection(&m_csLock);
	
	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);

	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();
	ReleaseDC(pDC);
	return TRUE;	
}

void CESMTimeLineObjectEditor::DrawBackground(CDC* pDC, CRect rect)
{
	TRIVERTEX vertex[2] ;
	m_bSyncObj = FALSE;

	//-- BK COLOR_BASIC_BG_3	78
	//-- Select Color #EF6B31
	switch(m_nBK)
	{
	case TIMELINE_BACKGROUND_LEFT:
		{
			vertex[0].Red   = 0xE100;
			vertex[0].Green = 0x3A00;
			vertex[0].Blue  = 0x2300;
			vertex[1].Red   = 0x5900;
			vertex[1].Green = 0x5900;
			vertex[1].Blue  = 0x5900;
		}
		break;
	case TIMELINE_BACKGROUND_RIGHT:
		{
			vertex[0].Red   = 0x5900;
			vertex[0].Green = 0x5900;
			vertex[0].Blue  = 0x5900;
			vertex[1].Red   = 0xE100;
			vertex[1].Green = 0x3A00;
			vertex[1].Blue  = 0x2300;
		}
		break;
	case TIMELINE_BACKGROUND_ALL:
		if(ESMGetSyncObj())
		{
			pDC->FillSolidRect(rect, COLOR_BLUE);		
			m_bSyncObj = TRUE;
		}
		else
			pDC->FillSolidRect(rect, RGB(187,66,49));		
		return;
	case TIMELINE_BACKGROUND_READY:
	case TIMELINE_BACKGROUND_START:
	case TIMELINE_BACKGROUND_FINISH:
		/*pDC->FillSolidRect(rect, COLOR_BLUE);*/		
		//jhhan 180817 완료 표시 색상 변경 - 대표님 요청(녹색계통)
		pDC->FillSolidRect(rect, RGB(0x00, 0xCC, 0x33));
		return;
	case TIMELINE_BACKGROUND_SELECT:
		pDC->FillSolidRect(rect, COLOR_BASIC_BG_DARK);		
		return;
	default:
		pDC->FillSolidRect(rect, COLOR_BASIC_BG_1 );
		return;
	}

	vertex[0].x     = rect.left ;
	vertex[0].y     = rect.top  ;
	vertex[0].Alpha = 0x8000;

	vertex[1].x     = rect.right;
	vertex[1].y     = rect.bottom	; 
	vertex[1].Alpha = 0x0000;

	GRADIENT_RECT gRect;
	gRect.UpperLeft  = 0;
	gRect.LowerRight = 1;

	pDC->Rectangle(rect);//, &CBrush(RGB(0,0,0)));
	pDC->GradientFill(vertex, 2, &gRect, 1, GRADIENT_FILL_RECT_H);
}

void CESMTimeLineObjectEditor::DrawInfoTime(CDC* pDC, CRect rect)
{
	//SetFrameFont(pDC, 13, _T("Noto Sans CJK KR Regular")); //CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);
	pDC->SetTextColor(COLOR_WHITE);
	pDC->SetBkColor(COLOR_TL_INFO_TOP);

	CString strStart, strEnd;

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Text Margin 
	rect.left += DSC_TIMELINE_MARGIN;
	rect.right-= DSC_TIMELINE_MARGIN;

	if(GetProcessMode() != TRUE)
	{
		/*if(m_nTime[0] == m_nTime[1])
		{
			strStart.Format(_T("%0.03f"), (float)m_nTime[0]/1000);
			pDC->DrawText(strStart, rect,  DT_CENTER | DT_BOTTOM | DT_SINGLELINE);
		}
		else*/
		{
			/*int nStart,nEnd;

			nStart = m_nTime[0] - ESMGetSelectedTime();
			nEnd = m_nTime[1] - ESMGetSelectedTime();

			strStart.Format(_T("%0.03f"), (float)nStart/1000);
			strEnd.Format(_T("%0.03f")	, (float)nEnd/1000);*/

			strStart.Format(_T("%0.03f"), (float)m_nTime[0]/1000);
			strEnd.Format(_T("%0.03f"), (float)m_nTime[1]/1000);
			//strEnd.Format(_T("테스트 항목에대한 자료 메일로 드리겠습니다.%0.03f")	, (float)m_nTime[1]/1000);

			pDC->DrawText(strStart, rect,  DT_LEFT	| DT_BOTTOM | DT_SINGLELINE);
			pDC->DrawText(strEnd, rect,  DT_RIGHT	| DT_BOTTOM | DT_SINGLELINE);
		}
	}
	else
	{
		CString strPCInfo;
		strPCInfo = GetMakePC();
		if(strPCInfo.GetLength() > 3)
		{
			int nTmp = strPCInfo.ReverseFind('.') + 1;
			strPCInfo = strPCInfo.Mid(nTmp, strPCInfo.GetLength());
		}
		pDC->DrawText(strPCInfo, rect,  DT_CENTER | DT_BOTTOM | DT_SINGLELINE);
	}

	
}

void CESMTimeLineObjectEditor::DrawInfoCount(CDC* pDC, CRect rect)
{
	//SetFrameFont(pDC, 13, _T("Noto Sans CJK KR Regular")); //CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);
	pDC->SetTextColor(COLOR_BLACK);
	pDC->SetBkColor(COLOR_TL_INFO_BODY);

	CString strStart, strEnd;

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Text Margin 
	rect.left += DSC_TIMELINE_MARGIN;
	rect.right-= DSC_TIMELINE_MARGIN;

	if(GetProcessMode() != TRUE)
	{
		strStart.Format(_T("%d"), m_nStepFrame);
		pDC->DrawText(strStart, rect,  DT_CENTER	| DT_BOTTOM | DT_SINGLELINE);
	}
	else
	{
		CString strPCInfo;
		strPCInfo = GetMakePC();
		if(strPCInfo.GetLength() > 3)
		{
			int nTmp = strPCInfo.ReverseFind('.') + 1;
			strPCInfo = strPCInfo.Mid(nTmp, strPCInfo.GetLength());
		}
		pDC->DrawText(strPCInfo, rect,  DT_CENTER | DT_BOTTOM | DT_SINGLELINE);
	}
}

void CESMTimeLineObjectEditor::DrawInfoNo(CDC* pDC, CRect rect)
{
	//SetFrameFont(pDC, 13, _T("Noto Sans CJK KR Regular")); //CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);
	pDC->SetTextColor(COLOR_BLACK);
	pDC->SetBkColor(COLOR_TL_INFO_BODY);

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Text Margin 
	rect.left += DSC_TIMELINE_MARGIN;
	rect.right-= DSC_TIMELINE_MARGIN;

	int nDSCCnt = GetDSCCount();
	if(nDSCCnt)
	{
		CString strDSC;
		strDSC.Format(_T("%d"), GetDSCIndex(0));
		pDC->DrawText(strDSC , rect,  DT_LEFT | DT_TOP | DT_SINGLELINE);
		m_nStartDSC = GetDSCIndex(0);

		if(nDSCCnt>1)
		{
			CString strEnd;
			strEnd.Format(_T("%d"), GetDSCIndex(nDSCCnt - 1));	
			pDC->DrawText(strEnd, rect,  DT_RIGHT	| DT_TOP | DT_SINGLELINE);
			m_nEndDSC = GetDSCIndex(nDSCCnt - 1);

			TRACE(_T("Start = %s, End = %s\n"), strDSC, strEnd);
		}
		else
		{
			pDC->DrawText(strDSC, rect,  DT_RIGHT	| DT_TOP | DT_SINGLELINE);
			m_nEndDSC = m_nStartDSC;

			TRACE(_T("Start = %s, End = %s\n"), strDSC, strDSC);
		}
		
		//-- 2013-10-08 hongsu@esmlab.com
		//-- Draw Progress
		if(m_nProgressKind)
		{
			int nBody = DSC_TIMELINE_BODY +- DSC_TIMELINE_MARGIN;
			CString strIndicator, strPer;

			//-- 2013-10-08 hongsu@esmlab.com
			//-- Draw Kind
			rect.top    = nBody - DSC_TIMELINE_TOP;
			rect.bottom = nBody ;	

			switch(m_nProgressKind)
			{
			case ESM_ADJ_KIND_POSITION:	strIndicator.Format(_T("Adjusting"));		break;
			case ESM_ADJ_KIND_CREATE3D:	strIndicator.Format(_T("Creating 3D"));		break;
			default:
				return;
			}
			pDC->DrawText(strIndicator, rect,  DT_RIGHT	| DT_TOP | DT_SINGLELINE);


			//-- 2013-10-08 hongsu@esmlab.com
			//-- Draw Value 
			if(m_fProgress >= 100)
				strPer.Format(_T("         [Finish] "));
			else
			{
				strPer.Format(_T("  %.2f"),m_fProgress);
				strPer.Append(_T("%  "));	
			}		

			rect.top    = nBody ;
			rect.bottom = nBody + DSC_TIMELINE_TOP;
			pDC->DrawText(strPer, rect,  DT_RIGHT	| DT_TOP | DT_SINGLELINE);
		}
	}
	else
	{
		CString strFrame;
		strFrame.Format(_T("%d"), GetFrameCnt());
		CString strFile = GetMovieFile();

		pDC->DrawText(strFrame , rect,  DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		pDC->DrawText(strFile , rect,  DT_CENTER | DT_BOTTOM | DT_SINGLELINE);

	}
}
void CESMTimeLineObjectEditor::DrawInfoZoom(CDC* pDC, CRect rect)
{
	//SetFrameFont(pDC, 13, _T("Noto Sans CJK KR Regular")); //CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);
	pDC->SetTextColor(COLOR_BLACK);
	pDC->SetBkColor(COLOR_TL_INFO_BODY);

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Text Margin 
	rect.left += DSC_TIMELINE_MARGIN;
	rect.right-= DSC_TIMELINE_MARGIN;

	CString strStart, strEnd;
	if(GetProcessMode() != TRUE)
	{
		/*if(m_nTime[0] == m_nTime[1])
		{
			strStart.Format(_T("%0.03f"), (float)m_nTime[0]/1000);
			pDC->DrawText(strStart, rect,  DT_CENTER | DT_BOTTOM | DT_SINGLELINE);
		}
		else*/
		{
			strStart.Format(_T("%d"), m_nZoom[0]);
			strEnd.Format(_T("%d")	, m_nZoom[1]);

			pDC->DrawText(strStart, rect,  DT_LEFT	| DT_TOP | DT_SINGLELINE);
			pDC->DrawText(strEnd, rect,  DT_RIGHT	| DT_TOP | DT_SINGLELINE);
		}
	}
	else
	{
		CString strPCInfo;
		strPCInfo = GetMakePC();
		if(strPCInfo.GetLength() > 3)
		{
			int nTmp = strPCInfo.ReverseFind('.') + 1;
			strPCInfo = strPCInfo.Mid(nTmp, strPCInfo.GetLength());
		}
		pDC->DrawText(strPCInfo, rect,  DT_CENTER | DT_TOP | DT_SINGLELINE);
	}
}



void CESMTimeLineObjectEditor::DrawInfoDSC(CDC* pDC, CRect rect)
{
	//SetFrameFont(pDC, 13, _T("Noto Sans CJK KR Regular")); //CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);
	pDC->SetTextColor(COLOR_BLACK);
	pDC->SetBkColor(COLOR_TL_INFO_BODY);

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Text Margin 
	rect.left += DSC_TIMELINE_MARGIN;
	rect.right-= DSC_TIMELINE_MARGIN;

	int nDSCCnt = GetDSCCount();
	if(nDSCCnt)
	{
		CString strDSC = GetDSCID(0);
		pDC->DrawText(strDSC , rect,  DT_LEFT | DT_BOTTOM | DT_SINGLELINE);

		if(nDSCCnt>1)
		{
			CString strEnd = GetDSCID(nDSCCnt - 1);	
			pDC->DrawText(strEnd, rect,  DT_RIGHT	| DT_BOTTOM | DT_SINGLELINE);
		}
		else
		{
			pDC->DrawText(strDSC, rect,  DT_RIGHT	| DT_BOTTOM | DT_SINGLELINE);
		}

		//-- 2013-10-08 hongsu@esmlab.com
		//-- Draw Progress
		if(m_nProgressKind)
		{
			int nBody = DSC_TIMELINE_BODY +- DSC_TIMELINE_MARGIN;
			CString strIndicator, strPer;

			//-- 2013-10-08 hongsu@esmlab.com
			//-- Draw Kind
			rect.top    = nBody - DSC_TIMELINE_TOP;
			rect.bottom = nBody ;	

			switch(m_nProgressKind)
			{
			case ESM_ADJ_KIND_POSITION:	strIndicator.Format(_T("Adjusting"));		break;
			case ESM_ADJ_KIND_CREATE3D:	strIndicator.Format(_T("Creating 3D"));		break;
			default:
				return;
			}
			pDC->DrawText(strIndicator, rect,  DT_RIGHT	| DT_BOTTOM | DT_SINGLELINE);


			//-- 2013-10-08 hongsu@esmlab.com
			//-- Draw Value 
			if(m_fProgress >= 100)
				strPer.Format(_T("         [Finish] "));
			else
			{
				strPer.Format(_T("  %.2f"),m_fProgress);
				strPer.Append(_T("%  "));	
			}		

			rect.top    = nBody ;
			rect.bottom = nBody + DSC_TIMELINE_TOP;
			pDC->DrawText(strPer, rect,  DT_RIGHT	| DT_BOTTOM | DT_SINGLELINE);
		}
	}
	else
	{
		CString strFrame;
		strFrame.Format(_T("%d"), GetFrameCnt());
		CString strFile = GetMovieFile();
		rect.top -= DSC_TIMELINE_TOP*2;
		pDC->DrawText(strFrame , rect,  DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		pDC->DrawText(strFile , rect,  DT_CENTER | DT_BOTTOM | DT_SINGLELINE);

	}
}

void CESMTimeLineObjectEditor::DrawFrame(CDC* pDC, CRect rect)
{
	int nHeight = DSC_TIMELINE_IMG_HEIGHT;
	int nWidth	= DSC_TIMELINE_IMG_WIDTH;
	if( m_memDC == NULL)
	{
		CBitmap* pImage	= NULL;
		Bitmap* pFrame	= NULL;
		HBITMAP	hBmp	= NULL;

		CString strName, strFileName;
		float nTime;

		int nDSCCnt = GetDSCCount();
		if(!nDSCCnt)
			return;

		strName = GetDSCID(0);
		if(m_nTime[0] < m_nTime[1])
			nTime = (float) m_nTime[0]/1000;
		else

			nTime = (float) m_nTime[1]/1000;
		//-- Image File Name
		CDSCItem* pItem = (CDSCItem*)m_arDSCinObj.GetAt(0);
		if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		{
			strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());
		}
		else
		{
			strFileName.Format(_T("%s\\%s\\%s.jpg"),ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());
		}

		m_memDC = new CDC;
		FFmpegManager FFmpegMgr(FALSE);
		pImage = new CBitmap();	
		m_memDC->CreateCompatibleDC(pDC);
		pImage->CreateCompatibleBitmap(pDC, nWidth, nHeight);
		//FFmpegMgr.GetCaptureImage(strFileName, pImage, ESMGetFrameIndex((int)(nTime * 1000)), nWidth, nHeight, ESMGetFrameArray());


		//-- 2013-10-01 hongsu@esmlab.com
		//-- Text Margin 
		rect.left += DSC_TIMELINE_MARGIN;
		//-- Over Size	
		nWidth = rect.Width() - DSC_TIMELINE_MARGIN;

		//  2013-10-15 Ryumin
		Invalidate(FALSE);

		m_memDC->SelectObject(pImage);

		pImage->DeleteObject();
		if(pImage)
		{
			delete pImage;
			pImage = NULL;
		}

		m_bImageLoad = TRUE;
	}
	if(m_bImageLoad == FALSE)
		return;

	rect.left += DSC_TIMELINE_MARGIN;
	//-- Over Size	
	nWidth = rect.Width() - DSC_TIMELINE_MARGIN;
	pDC->BitBlt(rect.left, rect.top + DSC_TIMELINE_TOP, nWidth, nHeight, m_memDC, 0, 0, SRCCOPY);
} 

void CESMTimeLineObjectEditor::SetProgress(float nProgress, int nKind)
{
	m_fProgress = nProgress;
	m_nProgressKind = nKind;

	//-- 2013-10-08 hongsu@esmlab.com
	//-- Redraw
	if(!m_hWnd)
		return;
	CDC *pDC = GetDC();
	if(!pDC)
		return;

	CRect rect;
	GetClientRect(&rect);
	if(!rect.Width() || !rect.Height())
	{
		ReleaseDC(pDC);
		return;
	}

	CRect rectBody;
	rectBody.top	= DSC_TIMELINE_MARGIN + DSC_TIMELINE_TOP;
	rectBody.bottom	= rectBody.top	+ DSC_TIMELINE_BODY;
	rectBody.left	= rect.left		+ DSC_TIMELINE_MARGIN;
	rectBody.right	= rect.right	- 2 * DSC_TIMELINE_MARGIN;

	//-- Draw DSC
	DrawInfoDSC(pDC, rectBody);
	ReleaseDC(pDC);
}

int CESMTimeLineObjectEditor::PtInObject(CPoint pt)
{
	//-- Check Pt In Rect
	if(!m_rtBox.PtInRect(pt))
		return TIMELINE_BACKGROUND_NON;

	//-- Left or Right
	int nMid = m_rtBox.left + m_rtBox.Width()/2;
	if(pt.x > nMid)
		return TIMELINE_BACKGROUND_RIGHT;
	return TIMELINE_BACKGROUND_LEFT;
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMTimeLineObjectEditor::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CESMTimeLineObject::OnLButtonDown(nFlags, point);

	if(GetProcessMode() != TRUE)
	{
		ESMEvent* pMsg = NULL;
		//-- 2013-10-14 Ryumin
		//-- Update ESMView Object Update
		pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_VIEW_OBJ_UPDATE;
		pMsg->nParam1	= (UINT)this;		
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);	

		//-- 2014-07-15 hongsu@esmlab.com
		//-- Update Effect Editor
		pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_EFFECT_UPDATE;
		pMsg->pParam	= (LPARAM)this;		
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	
	}
	else
	{
		//ProcessMode

		m_nCurrentBK = m_nBK;
		SetBK(TIMELINE_BACKGROUND_SELECT);
		DrawObject();
	}
	

	return;
}

/************************************************************************
 * @method:		CESMTimeLineObjectEditor::OnLButtonUp
 * @function:	OnLButtonUp
 * @date:		2013-10-15
 * @owner		Ryumin (ryumin@esmlab.com)
 * @param:		UINT nFlags
 * @param:		CPoint point
 * @return:		void
 */
void CESMTimeLineObjectEditor::OnLButtonUp(UINT nFlags, CPoint point)
{
	if(GetProcessMode() != TRUE)
	{
		SetBK(TIMELINE_BACKGROUND_ALL);

		//DrawObject();
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_FRAME_UPDATE_OBJ_EDITOR;
		pMsg->pDest		= (LPARAM)this;	
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);
	}
	else
	{
		//ProcessMode
		SetBK(m_nCurrentBK);
		DrawObject();
	}


	CESMTimeLineObject::OnLButtonUp(nFlags, point);
}

/************************************************************************
 * @method:		OnRButtonDown [CESMTimeLineObjectEditor::OnRButtonDown]
 * @function:	
 * @date:		2013-10-11
 * @owner		Ryumin (ryumin@esmlab.com)
 * @param:		UINT nFlags
 * @param:		CPoint point
 * @return:		void
 */
void CESMTimeLineObjectEditor::OnRButtonDown(UINT nFlags, CPoint point)
{
	if(GetProcessMode() != TRUE)
	{
		LONG nItem = GetSelectMenu(point);
		switch (nItem)
		{
		case ID_TIMELINEMENU_COPY:
			ESMSetCopyObj((void*)this);
			break;
		case ID_TIMELINEMENU_INSERT_M:
			{
				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message	= WM_ESM_FRAME_INSERT_OBJ_EDITOR_M;
				pMsg->pDest		= (LPARAM)ESMGetCopyObj();

				CESMTimeLineObject::OnRButtonDown(nFlags, point);	
				if(pMsg->pDest != NULL)
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	
			}
			break;
		case ID_TIMELINEMENU_INSERT_P:
			{
				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message	= WM_ESM_FRAME_INSERT_OBJ_EDITOR_P;
				pMsg->pDest		= (LPARAM)ESMGetCopyObj();

				CESMTimeLineObject::OnRButtonDown(nFlags, point);	
				if(pMsg->pDest != NULL)
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	
			}
			break;
		case ID_TIMELINEMENU_EDIT:
			{
				ESMSetSelectedTime(0);
				/*ESMEvent* pMsg	= new ESMEvent;
				pMsg->message	= WM_ESM_FRAME_UPDATE_OBJ_EDITOR;
				pMsg->pDest		= (LPARAM)this;	
				::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	*/
				TimelineEdit();
			}
			break;
		case ID_TIMELINEMENU_DELETE:
			{
				int nRet = MessageBox(_T("정보를 삭제 하시겠습니까?"),_T("Question"), MB_YESNO);
				if (nRet == IDYES)
				{
					//-- Remove This
					ESMEvent* pMsg	= new ESMEvent;
					pMsg->message	= WM_ESM_FRAME_REMOVE_OBJ_EDITOR;
					pMsg->pDest		= (LPARAM)this;

					CESMTimeLineObject::OnRButtonDown(nFlags, point);	
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	
					return;
				}
			}
			break;
		}
	}
	else
	{
		//ProcessMode
	}

	CESMTimeLineObject::OnRButtonDown(nFlags, point);	
	return;
}

LONG CESMTimeLineObjectEditor::GetSelectMenu(CPoint point)
{
// 	Context Menu
// 	Menu Item
// 		- Edit (Dialog)
// 		- Delete
	CMenu	MenuLoader;
	CMenu*	pTimelineMenu;
	CPoint	ptMenu = point;
	ClientToScreen(&ptMenu);

	MenuLoader.LoadMenu(IDR_TIMELINE_MENU);
	pTimelineMenu = MenuLoader.GetSubMenu(0);
	LONG nItem = pTimelineMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RETURNCMD,
		ptMenu.x, ptMenu.y, this);
	return nItem;
}

void CESMTimeLineObjectEditor::SetDSC(int nStartIndex, int nEnndIndex)
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	if (m_arDSCinObj.GetCount() < 1) return;

	CDSCItem* pItem = NULL;
	CString strID(_T(""));
	int nStart	= nStartIndex;
	int nEnd	= nEnndIndex;

	m_arDSCinObj.RemoveAll();
	//	DSC Order 순방향
	if (nStart < nEnd)
	{
		for (int nIdx = nStart; nIdx <= nEnd; ++nIdx )
		{
			pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
			if (!pItem) break;

			m_arDSCinObj.Add(pItem);
		}
	}
	//	DSC Order 역방향
	else
	{
		for (int nIdx = nStart; nIdx >= nEnd; --nIdx )
		{
			pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
			if (!pItem) break;

			m_arDSCinObj.Add(pItem);
		}
	}
	arDSCList.RemoveAll();
}

void CESMTimeLineObjectEditor::TimelineEdit()
{
// 	Edit Dialog
// 	- Edit (Dialog)
// 		- StartTime/EndTime
// 		- Start DSC/End DSC
// 		- Next Frame Time
	CTimeLineObjectEditorDlg EditorDlg(this); 
	if (EditorDlg.DoModal() == IDOK)
	{
	
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_FRAME_UPDATE_OBJ_EDITOR;
		pMsg->pDest		= (LPARAM)this;	
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	

		//CMiLRe 20151020 TimeLine 수정 후 Effect List Update
		pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_EFFECT_REFLASH;
		pMsg->pParam	= (LPARAM)this;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);

		//DrawObject();
	}
}

BOOL CESMTimeLineObjectEditor::OnEraseBkgnd(CDC* pDC)
{
	CDC dcMemory;

	dcMemory.CreateCompatibleDC(pDC);
	CBitmap* pOldBitmap = dcMemory.SelectObject(&bmpFrame);
	CRect rcClient;
	GetClientRect(&rcClient);
	pDC->BitBlt(0, 0, rcClient.Width(),rcClient.Height(), &dcMemory, 0, 0, SRCCOPY);
	dcMemory.SelectObject(pOldBitmap);
	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	dcMemory.DeleteDC();
	return TRUE;
//	return CESMTimeLineObject::OnEraseBkgnd(pDC);
}

void CESMTimeLineObjectEditor::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	if(GetProcessMode() != TRUE)
	{
		TimelineEdit();
	}
	else
	{
		/*CString strFile = this->GetMovieFile();
		AfxMessageBox(strFile);*/
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_FRAME_PROCESSOR_PLAY;
		pMsg->pDest		= (LPARAM)this;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);
	}

	

	CESMTimeLineObject::OnLButtonDblClk(nFlags, point);
}

void CESMTimeLineObjectEditor::SetObjectFrame()
{
	int nAll, nTimeCnt, nFrmCnt		= 0;
	int nDSCCnt		= GetDSCCount();
	int nStartTime	= GetStartTime();			//-- Start DSC : Time
	int nEndTime	= GetEndTime();			//-- End DSC : Time
	int nFpsTime	= GetNextTime();
	int nKzoneIdx   = GetKzoneIdx();
	BOOL bOrder;

	//-- Remove Object Frame
	//-- Check Previous Addition Objects
	nAll = GetObjectFrameCount();
	if	(nAll)
		return;

	//-- Get Time Count
	int nTime = nEndTime - nStartTime;	
	if( (nEndTime-nStartTime) > 0)	bOrder = TRUE;
	else							bOrder = FALSE;
	nTimeCnt = ESMGetCntMovieTime((abs)(nEndTime-nStartTime));

	CESMObjectFrame* pObjFrm = NULL;
	CString strDSC;	

	//-- 2014-07-17 hongsu@esmlab.com
	if(!nDSCCnt)
		return;

	//----------------------------------------------------
	//-- 1.Same DSC
	//----------------------------------------------------
	if(nDSCCnt == 1)
	{
		strDSC = GetDSCID(0);
		nTime = nStartTime;
		while(1)
		{
			pObjFrm = new CESMObjectFrame(strDSC, nTime);
			AddObjectFrame(pObjFrm);

			//-- next Frame
			if(bOrder)
			{
				ESMGetNextFrameTime(nTime);
				if(nEndTime < nTime)
					break;
			}
			else
			{
				ESMGetPreviousFrameTime(nTime);
				if(nEndTime > nTime)
					break;
			}
		}		
	}

	//----------------------------------------------------
	//-- 2.Same Time
	//----------------------------------------------------
	else if(nStartTime == nEndTime)
	{
		CString strDSC;
		for ( int i=0 ; i<nDSCCnt ; i++ )
		{
			strDSC = GetDSCID(i);
			pObjFrm = new CESMObjectFrame(strDSC, nStartTime);
			AddObjectFrame(pObjFrm);
		}		
	}	

	//----------------------------------------------------
	//-- 3. Time > DSC Count
	//----------------------------------------------------
	else if(nTimeCnt >= nDSCCnt)
	{
		float fBase = 0.0, fCalc = 0.0;	
		int nDSCIndex = 0;
		int nTimeIndex = 0;

		fBase = (float)((float)nDSCCnt/(float)nTimeCnt);
		nTime = nStartTime;

		while(1)
		{
			strDSC = GetDSCID(nDSCIndex);			
			pObjFrm = new CESMObjectFrame(strDSC, nTime);
			AddObjectFrame(pObjFrm);

			//-- Check Time Index
			nTimeIndex++;
			if(nTimeIndex >= nTimeCnt)
				break;

			//-- Next Time
			if(bOrder)
			{
				ESMGetNextFrameTime(nTime);
				if(nEndTime < nTime)
					break;
			}
			else
			{
				ESMGetPreviousFrameTime(nTime);
				if(nEndTime > nTime)
					break;
			}

			//-- Next DSC
			fCalc = ((float)nTimeIndex* fBase);
			if( (nDSCIndex+1) < fCalc)
				nDSCIndex++;
		}		
	}

	//----------------------------------------------------
	//-- 4. Time < DSC Count
	//----------------------------------------------------
	else if(nTimeCnt < nDSCCnt)
	{
		float fBase = 0.0, fCalc = 0.0;	
		int nDSCIndex = 0;
		int nTimeIndex = 0;

		fBase = (float)((float)nTimeCnt/(float)nDSCCnt);
		nTime = nStartTime;

		int i = 0;
		while(1)
		{
			strDSC = GetDSCID(nDSCIndex);
			pObjFrm = new CESMObjectFrame(strDSC, nTime);
			AddObjectFrame(pObjFrm);

			//-- Check DSC Index
			nDSCIndex++;
			if(nDSCIndex >= nDSCCnt)
				break;

			//-- Next DSC
			fCalc = ((float)nDSCIndex * fBase);			
			if( (nTimeIndex+1) < (fCalc+0.5))	// round on
			{
				nTimeIndex++;
				//-- Next Time
				if(bOrder)
				{
					ESMGetNextFrameTime(nTime);
					if(nEndTime < nTime)
						break;
				}
				else
				{
					ESMGetPreviousFrameTime(nTime);
					if(nEndTime > nTime)
						break;
				}			
			}
		}
	}
}

BOOL CESMTimeLineObjectEditor::IsExistFrame(int nDscIndex, int nTimeIndex)
{
	int nStartTimeIndex = ESMGetFrameIndex(GetStartTime());
	int nEndTimeIndex = ESMGetFrameIndex(GetEndTime());

	if (nTimeIndex > nStartTimeIndex )
	{
		if ( nTimeIndex > nEndTimeIndex )
			return FALSE;
	}
	else if ( nTimeIndex < nStartTimeIndex )
	{
		if ( nTimeIndex < nEndTimeIndex )
			return FALSE;
	}

	int nCnt = GetDSCCount();

	int nStartDscIndex = ESMGetDSCIndex(GetDSCID(0));
	int nEndDscIndex = ESMGetDSCIndex(GetDSCID(nCnt-1));

	if (nDscIndex > nStartDscIndex )
	{
		if ( nDscIndex > nEndDscIndex )
			return FALSE;
	}
	else if ( nDscIndex < nStartDscIndex )
	{
		if ( nDscIndex < nEndDscIndex )
			return FALSE;
	}

	return TRUE;
}