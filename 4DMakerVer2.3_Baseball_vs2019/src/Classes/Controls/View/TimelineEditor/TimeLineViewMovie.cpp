////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineViewEdit.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-10-01
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TimeLineView.h"
#include "DSCViewer.h"
#include "ESMTimeLineObjectEditor.h"
#include "ESMFileOperation.h"
#include "ESMObjectFrame.h"
#include "MainFrm.h"
#include "ESMUtil.h"
#include <atlconv.h>
#pragma comment (lib, "atls.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;



#endif
//#define _SERVER_SAVE

#define MAX_FRAMRATE 30
#define MIN_FIRST_FRAME 11

#define AJA_AUTOPLAY

CString g_strInning[] = {_T("1st_start"),
	_T("1st_end"),
	_T("2nd_start"),
	_T("2nd_end"),
	_T("3rd_start"),
	_T("3rd_end"),
	_T("4th_start"),
	_T("4th_end"),
	_T("5th_start"),
	_T("5th_end"),
	_T("6th_start"),
	_T("6th_end"),
	_T("7th_start"),
	_T("7th_end"),
	_T("8th_start"),
	_T("8th_end"),
	_T("9th_start"),
	_T("9th_end"),
	_T("10th_start"),
	_T("10th_end"),
	_T("11th_start"),
	_T("11th_end"),
	_T("12th_start"),
	_T("12th_end")};

void CTimeLineView::MakeAdjustMovie()
{
	
	m_bCreate3D = FALSE;
	m_pMainFrm		= (void*)AfxGetMainWnd();
	HANDLE handle;
	
	handle = (HANDLE) _beginthread( _DoMakeMovieAdjustDP, 0, (void*)this); // create thread
}


void CTimeLineView::MakeMovie()
{
	//-- 2013-10-19 hongsu@esmlab.com
	//-- 2D Movie 
	m_bCreate3D = FALSE;
	m_pMainFrm		= (void*)AfxGetMainWnd();
// 	// 20150630 yongmin distributed processing
// 	if (ESMGetValue(ESM_VALUE_MOVIEMAKE_MODE))
// 	{	
		HANDLE handle;
		m_nMakingThreadIndex++;
		handle = (HANDLE) _beginthread( _DoMakeMovieDP, 0, (void*)this); // create thread
// 	}else
// 	{
// 		HANDLE handle;
// 		handle = (HANDLE) _beginthread( _DoMakeMovie, 0, (void*)this); // create thread
// 	}
}

void CTimeLineView::MakePictureMovie()
{
	//-- 2013-10-19 hongsu@esmlab.com
	//-- 2D Movie 
	m_bCreate3D = FALSE;

	HANDLE handle;
	handle = (HANDLE) _beginthreadex(NULL, 0, MakePictureMovieThread, (void *)this, 0, NULL);
	CloseHandle(handle);
}

//-- 2013-12-12 kjpark@esmlab.com
//-- if converting, toolbar disenable
void ConvterToolBarShow(CTimeLineView* pView, BOOL isShow)
{
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_TOOLBAR_SHOW;
	pMsg->pDest		= isShow;			
	::SendMessage(pView->GetTimeLineViewHandle(),WM_ESM, (WPARAM)WM_ESM_FRAME,(LPARAM)pMsg);
}

void _DoMakeMovie(void *param)
{
	CTimeLineView* pView = (CTimeLineView*)param;
	if(!pView)
	{
		ESMSetRecordingInfoint(_T("MakingFlag"), FALSE);
		return;
	}

	ESMEvent* pMsg;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_LIST_INITMOVIEFILE;
	::SendMessage(pView->GetTimeLineViewHandle(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);

	//-- 2013-12-12 kjpark@esmlab.com
	//-- if converting, toolbar disenable
	ConvterToolBarShow(pView, FALSE);

	//-- 2013-10-04 hongsu@esmlab.com
	//-- move Previous Output file
	CString strMovieFile, strMovieBackupFile, strFile;
	strMovieFile.Format(_T("%s"),ESMGetPath(ESM_PATH_OUTPUT));

	CESMFileOperation fo;

	//-- delete adjust files
	CString strAdjustFile;
	strAdjustFile.Format(_T("%s"),ESMGetPath(ESM_PATH_ADJUST));
	fo.Delete(strAdjustFile,FALSE);


	//-- 2013-10-19 hongsu@esmlab.com
	//-- 2D or 3D
	if(pView->m_bCreate3D)	strMovieFile.Append(_T("\\3D"));
	else					strMovieFile.Append(_T("\\2D"));
	CString strTp;
	strTp = strMovieFile;
	//-- 2013-10-09 jaehyun@esmlab.com
	//-- Output Type option
//	BOOL bOutputMP4 = ESMGetValue(ESM_VALUE_ADJ_OUT_MP4);
// 	if(bOutputMP4)
// 	{
		strMovieFile.Append(_T("\\4dmaker.mp4"));
		strTp.Append(_T("\\BackUp"));
		if( !fo.CheckPath(strTp))
			fo.CreateFolder(strTp);
		strMovieBackupFile.Format(_T("%s\\4dmaker_%s.mp4"), strTp, ESMGetFrameRecord());
// 	}
// 	else
// 		strMovieFile.Append(_T("\\4dmaker.ts"));
	fo.Delete(strMovieFile);


	//-- Make ts file on each object
	CESMTimeLineObjectEditor* pObj = NULL;
	int nAll = pView->GetObjCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pObj = pView->GetObj(i);
		if(!pObj)
			break;
		pObj->MakeMovieToClient(i);
	}	


	//-- 2013-10-01 hongsu@esmlab.com
	//-- Check Finish Make Movies 
	BOOL bFinish;
	int nCnt = 0, nMakeMovieStopCount = 2000;

	while(1)
	{
		bFinish = TRUE;
		Sleep(10);
		for(int i = 0 ; i < nAll ; i ++)
		{
			pObj = pView->GetObj(i);
			if(!pObj)
				break;
			if(!pObj->IsMakeMovie())
				bFinish = FALSE;
		}
		if(bFinish)
			break;

		//-- 2014-07-16 hjjang
		//-- Wait Up to 10 Second until receive 'Finish Message' from Client
		if (nCnt++>nMakeMovieStopCount)
		{
			//메세지 전송.
			ESMEvent* pMsg;
			pMsg = new ESMEvent;
			pMsg->message = WM_ESM_LIST_MOVIEMAKINGSTOP;
			::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
			ESMLog(1,_T("[Movie] Couldn't Receive Movie From Client"));
			break;
		}
	}

	ESMSetRecordingInfoint(_T("MakingFlag"), FALSE);

	if( ESMGetExecuteMode())
	{
		pMsg = new ESMEvent;
		pMsg->message = WM_ESM_LIST_TIMELINEALLDELETE;
		::SendMessage(pView->GetTimeLineViewHandle(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	}

// 	if( nCnt > nMakeMovieStopCount)
// 	{
// 		strFile.Format(_T("%s"), ESMGetServerRamPath());
// 		fo.Delete(strFile,FALSE);
// 		pView->SetMovieMaking(FALSE);
// 		return ;
// 	}

	CString strCmd, strOpt, strOpt1, strOpt2;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));	

	ESMLog(1,_T("[CHECK_G][Movie] Create Movie ... [%s]"),strMovieFile);

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Combine Each Movies from Object
	//! => ffmpeg -i "concat:intermediate1.ts | intermediate2.ts" -c copy -bsf:a aac_adtstoasc output.mp4
	//! => ffmpeg -i "concat:C:\Program Files\ESMLab\4DMaker\result\movie\object\0\0.ts|C:\Program Files\ESMLab\4DMaker\result\movie\object\1\1.ts|C:\Program Files\ESMLab\4DMaker\result\movie\object\2\2.ts" 
	//			  -c copy -bsf:a aac_adtstoasc "C:\Program Files\ESMLab\4DMaker\result\movie\object\output.mp4"	
	strOpt.Format(_T("-i \"concat:"));
	CFile ReadFile;
	int lFileSize = 0;
	for(int i = 0 ; i < nAll ; i ++) 
	{
		for( int j=0 ; 1 ; j++)
		{
			strFile.Format(_T("%s\\%d_%d.mp4"), ESMGetServerRamPath(), i,j);
			if(ReadFile.Open(strFile, CFile::modeRead))
			{
				lFileSize = ReadFile.GetLength();
				ReadFile.Close();
			}
			else
				lFileSize = 0;
			if (FileExists(strFile) && lFileSize > 0)
			{

				if(strOpt != _T("-i \"concat:"))
					//if(i!=0 || j!=0)
					strOpt.Append(_T("|"));
				strOpt.Append(strFile);
			}
			else
			{
				if( j > 10)
					break;
			}
		}
	}		
	strOpt.Append(_T("\" -c copy -bsf:a aac_adtstoasc "));

	//-- 2013-10-19 hongsu@esmlab.com
	//-- Add Result File Name
	strOpt.Append(_T("\""));
	strOpt.Append(strMovieFile);
	strOpt.Append(_T("\""));

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Log 
	// ESMLog(5,_T("%s %s"),strCmd, strOpt);


	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}

	ConvterToolBarShow(pView, TRUE);

	//-- Delete temporary movie file
	strFile.Format(_T("%s"), ESMGetServerRamPath());
	fo.Delete(strFile,FALSE);

	if(ESMGetValue(ESM_VALUE_MAKEAUTOPLAY))
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_VIEW_MAKEMOVIEPLAY;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	}
	pView->SetMovieMaking(FALSE);
	fo.Copy(strMovieFile, strMovieBackupFile);
}

unsigned WINAPI CTimeLineView::MakePictureMovieThread(LPVOID param)
{
	// 이곳에서 만들면 End.
	CTimeLineView* pTimeLineView = (CTimeLineView*)param;
	if(!pTimeLineView)
		return 0;

	ConvterToolBarShow(pTimeLineView, FALSE);

	CString strMovieFile, strFile;
	strMovieFile.Format(_T("%s"),ESMGetPath(ESM_PATH_OUTPUT));

	CESMFileOperation fo;
	CString strAdjustFile;
	strAdjustFile.Format(_T("%s"),ESMGetPath(ESM_PATH_ADJUST));
	fo.Delete(strAdjustFile,FALSE);

	if(pTimeLineView->m_bCreate3D)	strMovieFile.Append(_T("\\3D"));
	else					strMovieFile.Append(_T("\\2D"));

	//-- 2013-10-09 jaehyun@esmlab.com
	//-- Output Type option
// 	BOOL bOutputMP4 = ESMGetValue(ESM_VALUE_ADJ_OUT_MP4);
// 	if(bOutputMP4)
// 		strMovieFile.Append(_T("\\4dmaker.mp4"));
// 	else
// 		strMovieFile.Append(_T("\\4dmaker.ts"));

	strMovieFile.Append(_T("\\4dmaker.mp4"));
	fo.Delete(strMovieFile);

	//-- Make ts file on each object
	CESMTimeLineObjectEditor* pObj = NULL;
	int nAll = pTimeLineView->GetObjCount();
	vector<CString> arrAdjedFile;
	for(int i = 0 ; i < nAll ; i ++)
	{
		pObj = pTimeLineView->GetObj(i);
		if(!pObj)
			break;
		pObj->MakePictureMovie(i, &arrAdjedFile, &pTimeLineView->m_CR_ADJProcess);
	}	


	//-- 2013-10-01 hongsu@esmlab.com
	//-- Check Finish Make Movies 
	BOOL bFinish;
	while(1)
	{
		bFinish = TRUE;
		Sleep(10);
		for(int i = 0 ; i < nAll ; i ++)
		{
			pObj = pTimeLineView->GetObj(i);
			if(!pObj)
				break;
			if(!pObj->IsMakeMovie())
				bFinish = FALSE;
		}		
		if(bFinish)
			break;
	}



	CString strCmd, strOpt, strOpt1, strOpt2;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));	

	if (nAll < 2)
	{
		//! => ffmpeg -i foo.ts -c copy foo.mp4
		strFile.Format(_T("-i \"%s\\%d\\%d.ts\""), ESMGetPath(ESM_PATH_OBJECT), 0,0);
		strOpt.Format(_T("%s -c copy -bsf:a aac_adtstoasc "),strFile);		
	}
	else
	{
		//-- 2013-10-01 hongsu@esmlab.com
		//-- Combine Each Movies from Object
		//! => ffmpeg -i "concat:intermediate1.ts | intermediate2.ts" -c copy -bsf:a aac_adtstoasc output.mp4
		//! => ffmpeg -i "concat:C:\Program Files\ESMLab\4DMaker\result\movie\object\0\0.ts|C:\Program Files\ESMLab\4DMaker\result\movie\object\1\1.ts|C:\Program Files\ESMLab\4DMaker\result\movie\object\2\2.ts" 
		//			  -c copy -bsf:a aac_adtstoasc "C:\Program Files\ESMLab\4DMaker\result\movie\object\output.mp4"	
		strOpt.Format(_T("-i \"concat:"));
		for(int i = 0 ; i < nAll ; i ++)
		{
			if(i>0)
				strOpt.Append(_T("|"));
			strFile.Format(_T("%s\\%d\\%d.ts"), ESMGetPath(ESM_PATH_OBJECT), i,i);
			//strFile.Format(_T("%s\\%d\\%d.mp4"), ESMGetPath(ESM_PATH_OBJECT), i,i);
			strOpt.Append(strFile);
		}		
		strOpt.Append(_T("\" -c copy -bsf:a aac_adtstoasc "));
	}

	//-- 2013-10-19 hongsu@esmlab.com
	//-- Add Result File Name
	strOpt.Append(_T("\""));
	strOpt.Append(strMovieFile);
	strOpt.Append(_T("\""));


	//-- 2013-10-01 hongsu@esmlab.com
	//-- Log 
	// ESMLog(5,_T("%s %s"),strCmd, strOpt);
	ESMLog(1,_T("[CHECK_G][Movie] Create Movie ... [%s]"),strMovieFile);

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}

	ESMLog(1,_T("[Movie] Make Movie Success [%s]"),strMovieFile);
	//-- 2013-12-12 kjpark@esmlab.com
	//-- if converting, toolbar disenable
	ConvterToolBarShow(pTimeLineView, TRUE);

	if(ESMGetValue(ESM_VALUE_MAKEAUTOPLAY))
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_VIEW_MAKEMOVIEPLAY;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	}
	pTimeLineView->SetMovieMaking(FALSE);
	return 0;
}

BOOL CTimeLineView::CheckMakeDSCFolder(CString strDSC)
{
	int bRet = FALSE;
	for(int i = 0; i < m_arrDSCFolder.GetCount(); i++)
	{
		if(m_arrDSCFolder.GetAt(i).CompareNoCase(strDSC)  == 0)
		{
			bRet = TRUE;
			break;
		}
	}

	return bRet;
}

void _DoMakeMovieAdjustDP(void *param)
{
	CTimeLineView* pView = (CTimeLineView*)param;
	if(!pView)
	{
		return;
	}

	ESMAdjustMovie info;
	ESMGetAdjustMovieInfo(info);
	
	

	/////////////////Counting
	CString strFilePath;
	ESMAdjustMovie tempinfo;
	int nFileTotal = 0, nCurrentCount = 0;
	for(int i = 0; i < info.arrPathList.GetCount(); i++)
	{
		strFilePath.Format(_T("%s\\%s"), info.strSrcPath, info.arrPathList.GetAt(i));
		nFileTotal += ESMFileCount(strFilePath, tempinfo);
	}
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();  
	pMsg->message = WM_ESM_MOVIE_MAKE_ADJUST_COUNT;
	pMsg->nParam1 = nCurrentCount;
	pMsg->nParam2 = nFileTotal;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	/////////////////////////

	for(int i = 0; i < info.arrPathList.GetCount(); i++)
	{
		
		ESMAdjustMovie mergeInfo;

		pView->m_nSendCount = 999;
		pView->m_nRecvCount = 0;
		CString strTargetPath;
		CString strPath;
		CString strDate;
		strPath.Format(_T("%s\\%s"), info.strSrcPath, info.arrPathList.GetAt(i));

		info.arrFileList.RemoveAll();
		int nFile = ESMFileCount(strPath, info);

		strTargetPath.Format(_T("%s\\%s"), info.strTarPath, info.arrPathList.GetAt(i));
		CreateDirectory(strTargetPath, NULL);
		strDate = strTargetPath;
		ESMSetFrameRecord(info.arrPathList.GetAt(i));

		pView->m_nSendCount = nFile;
		int nDSCIndex = 0;
		for(int j = 0; j < nFile; j++)
		{
			
			vector<MakeFrameInfo>* pArrFrameInfo = new vector<MakeFrameInfo>;

			int nIndex = 0;
			int nPos = 0;
			CString strToken;
			
			CString strDSC;
			
			while((strToken = info.arrFileList.GetAt(j).Tokenize(_T("_."),nPos)) != "")
			{
				if(nIndex == 0)
				{
					strDSC.Format(_T("%s"), strToken);
					
				}
				else if(nIndex == 1)
				{
					//if(_ttoi(strToken) == 0)
					{
						if(!pView->CheckMakeDSCFolder(strDSC))
						{
							pView->m_arrDSCFolder.Add(strDSC);
							mergeInfo.arrFolderList.Add(strDSC);
						}
						
						strTargetPath.Format(_T("%s\\%s\\%s"), info.strTarPath, info.arrPathList.GetAt(i),strDSC);
						CreateDirectory(strTargetPath, NULL);
					}

					nDSCIndex = _ttoi(strToken);
					break;
				}
				
				nIndex++;
			}

			for(int n = 0; n < MAX_FRAMRATE; n++)
			{
				MakeFrameInfo stMakeFrameInfo;
				_stprintf(stMakeFrameInfo.strFramePath, _T("%s\\%s"), strPath, info.arrFileList.GetAt(j));
				stMakeFrameInfo.nFrameSpeed = 30;
				stMakeFrameInfo.nFrameIndex = n;

				if(ESMGetValue(ESM_VALUE_AUTO_WHITEBALANCE))
					stMakeFrameInfo.bInsertWB = TRUE;
				else
					stMakeFrameInfo.bInsertWB = FALSE;

				pArrFrameInfo->push_back(stMakeFrameInfo);
			}

			ESMAdjustInfo adjInfo;
			adjInfo.strTargetPath = strTargetPath;
			adjInfo.nCount = nDSCIndex;
			adjInfo.strDSC = strDSC;


			pView->MakeMovieFrameBaseAdjustMovie(pArrFrameInfo, adjInfo);
			pView->WaitReciveAdjustMovie(j+1);

			nCurrentCount++;
			ESMEvent* pMsg = NULL;
			pMsg = new ESMEvent();  
			pMsg->message = WM_ESM_MOVIE_MAKE_ADJUST_COUNT;
			pMsg->nParam1 = nCurrentCount;
			pMsg->nParam2 = nFileTotal;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}

		pView->MergeAdjustMovie(strDate, mergeInfo);
		
	}
	
	//-- Make ts file on each object
	/*CESMTimeLineObjectEditor* pObj = NULL;
	int nAll = pView->GetObjCount();
	pView->m_nAdjMovieIndex = 0;
	for(int i = 0 ; i < nAll ; i ++)
	{
		vector<MakeFrameInfo>* pArrFrameInfo = new vector<MakeFrameInfo>;
		pObj = pView->GetObj(i);
		if(!pObj)
			break;
		pObj->GetFrameData(pArrFrameInfo, i);

		pView->MakeMovieFrameBaseMovie(pArrFrameInfo);

		// 	//대기
		pView->WaitReciveMovie();
		
		
		pView->MergeAdjustMovie();
		ESMLog(1, _T("Merge OK [%d/%d]"), i+1, nAll);
		//pView->MergeMovie();
	}*/
}

// 20150630 yongmin distributed processing
void _DoMakeMovieDP(void *param)
{	
	//jhhan 17-01-09	//목록 초기화
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_PROCESSOR_INIT;
	::SendMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	

	//Ceremony 
	if(ESMGetValue(ESM_VALUE_CEREMONYUSE))
	{
		CString strTel1 = ESMGetCeremonyInfoString(_T("InputTel"));
		ESMSetCeremonyInfoString(_T("OutputTel"), strTel1);
	}
	CTimeLineView* pView = (CTimeLineView*)param;

	if(!pView)
	{
		ESMSetRecordingInfoint(_T("MakingFlag"), FALSE);

		//jhhan m_bWait = FALSE 설정
		if(ESMGetBaseBallFlag())
		{
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_MOVIE_MAKE_WAIT_END;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}

		return;
	}

	//AJA 선언되어있을 때, Merge 후 삭제
	//if(!ESMGetValue(ESM_VALUE_AJAONOFF))
	//	pView->DeleteMovie();

	pView->m_pViewMovie->SetMakingFinish(FALSE);
	int nMakingThreadIndex = pView->m_nMakingThreadIndex;
	//SetMakingMovieFlag(TRUE);	
	//ESMEvent* pMsg = NULL;

	//-- if converting, toolbar disable
	ConvterToolBarShow(pView, FALSE);

	CString strFile;
	//strFile = _T("M:\\Movie\\4DMaker.mp4");
	strFile = ESMGetServerRamPath() + _T("\\4DMaker.mp4");;
	DeleteFile(strFile);

	vector<MakeFrameInfo>* pArrFrameInfo = new vector<MakeFrameInfo>;
	//-- Make ts file on each object
	
	int nAll = pView->GetObjCount();

	CString strTime;
	strTime.Format(_T("%s"),ESMGetCurTime());
	for(int i = 0 ; i < nAll ; i ++)
	{
		CESMTimeLineObjectEditor* pObj = NULL;

		pObj = pView->GetObj(i);
		if(!pObj)
		{
			//ESMLog(0, _T("pView->GetObj(%d)"), i);
			break;
		}

		//18.04.13 joonho.kim
		//Step Frame
		CDSCItem* pItem	= NULL;
		pItem	= pObj->GetDSC(0);
		if(pItem)
		{
			CESMObjectFrame* pObjFrame = pObj->GetObjectFrame(pItem->GetDeviceDSCID());

			if(pObj->m_nStepFrame > 1)
			{
				pObjFrame->SetStepFrame(TRUE);
			}
			else
				pObjFrame->SetStepFrame(FALSE);

//			pObjFrame->SetStepFrameCount(pObj->m_nStepFrame);
			pObjFrame->SetStartZoom(pObj->m_nZoom[0]);
			pObjFrame->SetEndZoom(pObj->m_nZoom[1]);
		}
		else
		{
			//ESMLog(0, _T("pObj->GetDSC(0)"));
		}
		
		pObj->GetFrameData(pArrFrameInfo, i, strTime);

		/*
		if(pArrFrameInfo->size())
		{
			pArrFrameInfo->at(i).Image = NULL;
			pArrFrameInfo->at(i).pYUVImg = NULL;
			pArrFrameInfo->at(i).pParent = NULL;
		}
		*/
	}

	ESMEvent* pMsgDelete	= new ESMEvent;
	pMsgDelete->message = WM_ESM_FRAME_REMOVEALL_OBJ_EDITOR;
	::PostMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsgDelete);	

	pView->SetMovieMaking(FALSE);

	//while(1)
	//{
	//	if( nMakingThreadIndex == pView->m_nMakedThreadIndex)
	//		break;
	//	Sleep(10);
	//}
	ESMLog(5, _T("[CHECK_E]Making Movie [%d/ %d]"), nMakingThreadIndex, pView->m_nMakingThreadIndex);

	//-- Tamlate 유효성 검사
	if(ESMGetCheckValid())
	{
		pView->CheckValidFrame(pArrFrameInfo);

		if(pArrFrameInfo->size() == 0) 
		{
			AfxMessageBox(_T("Check Valid Time Error"),0,0);
			return;
		}
	}
	else
	{
		ESMLog(1, _T("Check Valid Skip"));
	}
	//wgkim 180211
	if(ESMGetValue(ESM_VALUE_TEMPLATE_STABILIZATION))
	{				
		pView->SetStabilizationInterval(pArrFrameInfo);
	}
	ESMSetMakeTotalFrame(pArrFrameInfo->size());

	MakingData st_Makingdata;
	st_Makingdata.nSendCount = 999;
	st_Makingdata.nRecvCount = 0;

	//pView->map_MakingData.insert(pair<CString,MakingData>(strTime,st_Makingdata));
	pair<CString,MakingData> p(strTime,st_Makingdata);
	pView->map_MakingData.insert(p);

	pView->MakeMovieFrameBaseMovie(pArrFrameInfo);

	// 	//대기
	pView->WaitReciveMovie(strTime);

	if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork() == TRUE
		&& pView->m_bAJAMaking == TRUE)
	{
		ESMEvent* pMsgDelete	= new ESMEvent;
		pMsgDelete->message = WM_ESM_FRAME_FOCUS;
		::PostMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsgDelete);

		ESMLog(5,_T("Finish movie..."));
	}
	else
	{
#ifdef CMiLRe0
		if(pView->m_nSendCount == 0)
		{		
			SetMakingMovieFlag(FALSE);
			return ;
		}
#endif
		pView->MergeMovie(strTime);
		pView->StartMuxingView();

		CString strPath;
		CESMFileOperation fo;
		strPath.Format(_T("%s\\%s"),ESMGetClientRamPath(),strTime);
		fo.Delete(strPath,TRUE);
	}
	pView->map_MakingData.erase(pView->map_MakingData.find(strTime));
	pView->m_pViewMovie->SetMakingFinish(TRUE);
	if(pView->GetViewStructCount())
	{
		VIEW_STRUCT view = pView->GetViewStruct();
		pView->m_pViewMovie->AddViewStruct(view);
		pView->DeleteViewStruct();
	}

	if(ESMGetBaseBallFlag())
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_MOVIE_MAKE_WAIT_END;
		::SendMessage(pView->GetTimeLineViewHandle(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	}

// 	if( ESMGetExecuteMode())
// 	{
// 		pMsg = new ESMEvent;
// 		pMsg->message = WM_ESM_LIST_TIMELINEALLDELETE;
// 		::SendMessage(pView->GetTimeLineViewHandle(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
// 	}
	ESMSetRecordingInfoint(_T("MakingFlag"), FALSE);
	ConvterToolBarShow(pView, TRUE);
	//pView->m_nMakedThreadIndex++;
	return;
}

void CTimeLineView::WaitReciveMovie(CString strTime)
{
	int nCnt = 0, nMakeMovieStopCount = 3000; //30초
	//int nCnt = 0, nMakeMovieStopCount = 1500; //15초
	int nnCnt = 0;

	while(nnCnt < nMakeMovieStopCount)
	{
		nCnt++;
		Sleep(10);
		
		if( map_MakingData[strTime].nSendCount == map_MakingData[strTime].nRecvCount)
		{
			ESMLog(5,_T("Recieve End!!"));
			break;
		}

		if(map_MakingData[strTime].nSendCount < 2)
			nMakeMovieStopCount = 90000; //900초

		if(nCnt >500)
		{
			if(ESMGetMakingUse()&& m_movieMakingList.size() > 1)
			{
				for(int i = 0 ; i < m_movieMakingList.size();i++)
				{
					if(!m_movieMakingList[i].makingFin)
					{
						for(int j = 0 ; j < m_movieMakingList.size();j++)
						{
							if(m_movieMakingList[j].makingFin && !(m_movieMakingList[j].serverFlag) )
							{
								m_movieMakingList[j].fileNum = m_movieMakingList[i].fileNum;
								m_movieMakingList[j].m_MovieInfo = m_movieMakingList[i].m_MovieInfo;
								m_movieMakingList[j].makingFin = FALSE;
								vector<MakeFrameInfo> * MovieInfo = new vector<MakeFrameInfo>;
								//MovieInfo = &m_movieMakingList[i].m_MovieInfo;
									
								for(int a = 0 ; a <m_movieMakingList[i].m_MovieInfo.size();a++ )
								{
									MovieInfo->push_back(m_movieMakingList[i].m_MovieInfo[a]);
								}
								m_movieMakingList[j].pGroup->DoMovieMakeFrameMovie(MovieInfo,m_movieMakingList[i].fileNum);
								ESMLog(5,_T("DoMovieMakeFrameMovie [%d]"), m_movieMakingList[j].pGroup->m_nIP);
								break;
							}
						}
					}					
				}	
					
			}


			nnCnt += 500;
			nCnt = 0;
		}
	}
}

void CTimeLineView::WaitReciveAdjustMovie(int nCount)
{
	while(1)
	{
		Sleep(10);
		if( nCount == m_nRecvCount)
			break;
	}
}

void CTimeLineView::MergeAdjustMovie(CString strTargetPath, ESMAdjustMovie& info)
{
	//ESMAdjustMovie info;
	//ESMFolderCount(strTargetPath, info);


	for(int i = 0; i < info.arrFolderList.GetCount(); i++)
	{
		int nFileCount = 0;
		CString strTarget;
		CString strSrcFile;
		CString strMovieFile, strBcakupFile;
		strMovieFile.Format(_T("%s\\%s.mp4"),strTargetPath, info.arrFolderList.GetAt(i));
	
		CString strFile;
		CString strCmd, strOpt, strOpt1, strOpt2;
		strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));	

		ESMLog(1,_T("[Movie] Create Adjust Movie ... [%s]"),strMovieFile);

		strOpt.Format(_T("-i \"concat:"));
		CFile ReadFile;
		int lFileSize = 0;

		ESMAdjustMovie fileInfo;
		strTarget.Format(_T("%s\\%s"), strTargetPath, info.arrFolderList.GetAt(i));
		//nFileCount = ESMFileCount(strTarget, fileInfo);
		nFileCount = ESMFileSortInfo(strTarget, fileInfo);


		for(int j = 0 ; j < nFileCount ; j ++)
		{
			strFile.Format(_T("%s\\%s\\%s"),strTargetPath, info.arrFolderList.GetAt(i), fileInfo.arrFileList.GetAt(j));
			ESMLog(5,_T("[Movie] Merge Movie File [%s]"),strFile);
			if(ReadFile.Open(strFile, CFile::modeRead))
			{
				lFileSize = ReadFile.GetLength();
				ReadFile.Close();
			}
			else
				lFileSize = 0;

			if (FileExists(strFile) && lFileSize > 0)
			{
				if(j!=0 && strOpt != _T("-i \"concat:"))
					strOpt.Append(_T("|"));
				strOpt.Append(strFile);
			}
			else
			{
				//TEST CODE
				CMainFrame* pMainWnd = (CMainFrame*)m_pMainFrm;
				pMainWnd->m_bTestFlag = FALSE;
				AfxMessageBox(_T("Program Down"));

				ESMLog(5, _T("Not Exist File[%s]"), strFile);
				continue;
			}
		}		
		strOpt.Append(_T("\" -c copy -bsf:a aac_adtstoasc "));
		strOpt.Append(_T("\""));
		strOpt.Append(strMovieFile);
		strOpt.Append(_T("\""));

		SHELLEXECUTEINFO lpExecInfo;
		lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
		lpExecInfo.lpFile = strCmd;
		lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		lpExecInfo.hwnd = NULL;  
		lpExecInfo.lpVerb = L"open";
		lpExecInfo.lpParameters = strOpt;
		lpExecInfo.lpDirectory = NULL;

		lpExecInfo.nShow = SW_HIDE; // hide shell during execution
		lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
		ShellExecuteEx(&lpExecInfo);

		// wait until the process is finished
		if (lpExecInfo.hProcess != NULL)
		{
			::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
			::CloseHandle(lpExecInfo.hProcess);
		}

		ESMLog(5,_T("Make Adjust Movie Finish [%s][%d]"), strMovieFile,GetTickCount());
	}
}

void CTimeLineView::MergeArrayMovie()
{
	CString strName = _T("4dmaker");
	if(ESMGetBaseBallFlag())
	{
		CString strInning = g_strInning[ESMGetBaseBallInning()];
		CTime time = CTime::GetCurrentTime();
		CString strTime = time.Format("%H_%M_%S");
		strName.Format(_T("MERGE_%s_%s"), strInning,strTime);	
	}

	CString strMovieFile, strBcakupFile;
	strMovieFile.Format(_T("%s\\2D\\%s.mp4"), ESMGetPath(ESM_PATH_OUTPUT) ,strName);
	int nCount = 0;
	//CMiLRe 20151119 영상 저장 경로 변경
	while(FileExists(strMovieFile))
	{
		strMovieFile.Format(_T("%s\\2D\\%s_%d.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strName, nCount);
		nCount++;
	}

	strBcakupFile.Format(_T("%s\\2D\\BackUp\\%s_%s.mp4"),ESMGetPath(ESM_PATH_OUTPUT), strName, ESMGetFrameRecord());
	CESMFileOperation fo;
	//fo.Delete(strMovieFile);

	if(fo.Delete(strMovieFile))
	{
		CString strMovieFile_;
		strMovieFile_.Format(_T("%s\\2D\\%s_.mp4"), ESMGetPath(ESM_PATH_OUTPUT),strName);

		fo.Delete(strMovieFile_);
	}else
	{
		if (FileExists(strMovieFile))
		{
			ESMLog(0,_T("[Movie] MergeMovie File Delete Fail]"),strMovieFile);
			strMovieFile.Format(_T("%s\\2D\\%s_.mp4"), ESMGetPath(ESM_PATH_OUTPUT),strName);

			fo.Delete(strMovieFile);
		}else
		{
			CString strMovieFile_;
			strMovieFile_.Format(_T("%s\\2D\\%s_.mp4"), ESMGetPath(ESM_PATH_OUTPUT),strName);

			fo.Delete(strMovieFile_);
		}
	}

	CString strFile;
	CString strCmd, strOpt, strOpt1, strOpt2, strFileNames;

	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));	
	strFileNames.Format(_T("%s\\bin\\filenames.txt"),ESMGetPath(ESM_PATH_HOME));	

	ESMLog(1,_T("[CHECK_G][Movie] Create Movie ... [%s]"),strMovieFile);

	//strOpt.Format(_T("-i \"concat:"));
	strOpt.Format(_T("-y -f concat -i \"%s\" -an -c:v copy "), strFileNames);
	CFile ReadFile;
	int lFileSize = 0;
	
	// filenames create
	CFile file;
	file.Open(strFileNames, CFile::modeCreate|CFile::modeWrite);

	for(int i = 0 ; i < m_arrMoviePath.size() ; i ++)
	{
		strFile = m_arrMoviePath.at(i);
		ESMLog(5,_T("[Movie] Merge Movie File [%s]"),strFile);
		if(ReadFile.Open(strFile, CFile::modeRead))
		{
			lFileSize = ReadFile.GetLength();
			ReadFile.Close();
		}
		else
			lFileSize = 0;

		if (FileExists(strFile) && lFileSize > 0)
		{
			USES_CONVERSION;

			file.SeekToEnd();
			CString strData;
			LPCSTR pstr;
			strData.Format(_T("file '%s'\r\n"), strFile);
			pstr = W2A(strData);
			
			file.Write(pstr, strData.GetLength());
			
			/*if(i!=0 && strOpt != _T("-i \"concat:"))
				strOpt.Append(_T("|"));
			strOpt.Append(strFile);*/
		}
		else
		{
			ESMLog(5, _T("Not Exist File[%s]"), strFile);
			continue;
		}
	}		

	file.Close();

	strOpt.Append(_T("\""));
	strOpt.Append(strMovieFile);
	strOpt.Append(_T("\""));

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	fo.Copy(strMovieFile, strBcakupFile);
	ESMLog(5,_T("[Movie] Create Movie Finish [%s]"), strMovieFile);

	// Array Delete all
	m_arrMoviePath.clear();

	ESMEvent* pMsg2 = NULL;
	pMsg2 = new ESMEvent();
	pMsg2->message = WM_ESM_VIEW_MERGEMOVIE_CLEAR;
	pMsg2->pParam = NULL;
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);

#if 0  //-- Rotate Movie
	CString strMovieFile2;
	if(ESMGetReverseMovie())
	{
		int nStart;
		nStart= GetTickCount();
		ESMLog(5, _T("Rotate Time = %d"), nStart);
		strMovieFile2.Format(_T("%s\\2D\\4dmaker_re_%d.mp4"),ESMGetPath(ESM_PATH_OUTPUT), nCount);
		strOpt.Format(_T("-i \"%s\" -vf \"rotate=PI:bilinear=0,format=yuv420p\" -metadata:s:v rotate=0 -codec:v libx264 -codec:a copy \"%s\""), strMovieFile, strMovieFile2);
		SHELLEXECUTEINFO lpExecInfo;
		lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
		lpExecInfo.lpFile = strCmd;
		lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		lpExecInfo.hwnd = NULL;  
		lpExecInfo.lpVerb = L"open";
		lpExecInfo.lpParameters = strOpt;
		lpExecInfo.lpDirectory = NULL;

		lpExecInfo.nShow = SW_HIDE; // hide shell during execution
		lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
		ShellExecuteEx(&lpExecInfo);

		// wait until the process is finished
		if (lpExecInfo.hProcess != NULL)
		{
			::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
			::CloseHandle(lpExecInfo.hProcess);
		}	
		ESMLog(5, _T("Rotate End Time = %d, Gap = %d"), GetTickCount(), GetTickCount() - nStart);

		fo.Copy(strMovieFile2, strBcakupFile);
		ESMLog(5,_T("[Movie] Create Movie Finish [%s]"), strMovieFile2);

		if(ESMGetBaseBallFlag())
		{
			m_arrMoviePath.push_back(strMovieFile2);
			ESMEvent* pMsg2 = NULL;
			pMsg2 = new ESMEvent();
			pMsg2->message = WM_ESM_VIEW_MERGEMOVIE_PUSHBACK;
			pMsg2->pParam = (LPARAM)&strMovieFile;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
		}
	}
	else
	{
		fo.Copy(strMovieFile, strBcakupFile);
		ESMLog(5,_T("[Movie] Create Movie Finish [%s]"), strMovieFile);

		if(ESMGetBaseBallFlag())
		{
			m_arrMoviePath.push_back(strMovieFile);
			ESMEvent* pMsg2 = NULL;
			pMsg2 = new ESMEvent();
			pMsg2->message = WM_ESM_VIEW_MERGEMOVIE_PUSHBACK;
			pMsg2->pParam = (LPARAM)&strMovieFile;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
		}
	}

	if(1) //Auto Play
	{
		CString strMoviePath;
		if(ESMGetReverseMovie())
			strMoviePath = strMovieFile2;
		else
			strMoviePath = strMovieFile;

		//CMiLRe 20151119 영상 저장 경로 변경
		TCHAR* cPath = new TCHAR[1+strMoviePath.GetLength()];
		_tcscpy(cPath, strMoviePath);

		ESMEvent* pMsg2 = NULL;
		pMsg2 = new ESMEvent();
		pMsg2->message = WM_ESM_VIEW_MAKEMOVIEPLAY;
		pMsg2->pParam = (LPARAM)cPath;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
	}
#else
	if(1) //Auto Play
	{

		//CMiLRe 20151119 영상 저장 경로 변경
		TCHAR* cPath = new TCHAR[1+strMovieFile.GetLength()];
		_tcscpy(cPath, strMovieFile);

		ESMEvent* pMsg2 = NULL;
		pMsg2 = new ESMEvent();
		pMsg2->message = WM_ESM_VIEW_MAKEMOVIEPLAY;
		pMsg2->pParam = (LPARAM)cPath;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
	}
#endif
	


	strFile.Format(_T("%s"), ESMGetServerRamPath());
	fo.Delete(strFile,FALSE);

	if(ESMGetValue(ESM_VALUE_CEREMONYUSE))
	{
		CString strConcertPath;
		//CString strTel1 = ESMGetCeremonyInfoString(_T("InputTel"));
		CString strTel1 = ESMGetCeremonyInfoString(_T("OutputTel"));

		//file check
		int nIndex = 1;
		while(1)
		{
			strConcertPath.Format(_T("%s\\%s_%d.mp4"), ESMGetCeremony(ESM_VALUE_CEREMONYMOVIEPATH), strTel1, nIndex);
			if(!ISFILE(strConcertPath))
			{
				break;
			}
			else
				nIndex++;

			if(nIndex > 100)
				break;
		}

		//CopyFile(strMovieFile, strConcertPath, FALSE);
		ChangeMovieSize(strMovieFile, strConcertPath);
		//CHange Siize End
		//ESMSetCeremonyInfoString(_T("TelNum2"), strTel1);
		//ESMSetCeremonyInfoString(_T("OutputTel"), strTel1);
		ESMSetCeremonyInfoint(_T("SelectorSignal"), 1);
		ESMSetCeremonyInfoint(_T("PrintorSignal"), 2);
	}	

	ESMLog(5,_T("[CHECK_H]Make Movie Finish [%d]"), GetTickCount());

	ESMEvent* pMsgDelete	= new ESMEvent;
	pMsgDelete->message = WM_ESM_FRAME_REMOVEALL_OBJ_EDITOR;
	::PostMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsgDelete);	
#ifdef CMiLRe0
	SetMakingMovieFlag(FALSE);
#endif
}

//wgkim@esmlab.com 17-05-18
unsigned WINAPI CTimeLineView::MakeStabiliationMovie(LPVOID param)
{
	ESMLog(5, _T("Stabilization Movie Making Start"));
		
	CTimeLineView* timeLineView= (CTimeLineView*)param;	

	CT2CA pszConvertedAnsiString (timeLineView->m_strMovieFile);
	string strMovieFile(pszConvertedAnsiString); 	
	
	CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();
	CESMStabilizationMgr* stabilizationMgr = new CESMStabilizationMgr(pMovieMgr->GetUHDtoFHD());
		
	if(stabilizationMgr == NULL)
	{
		ESMLog(5, _T("Stabilization Class is NULL"));
		return 0;
	}	

	bool bStabilizationCheck = stabilizationMgr->VideoDecoding(strMovieFile, timeLineView->m_stabilizerStartFrameNumber, timeLineView->m_stabilizerEndFrameNumber);	
	if(!bStabilizationCheck)
	{		
		return 0;
	}

	string outputFilePath = strMovieFile + "_stab.mp4";
	stabilizationMgr->CreateStabilizatedMovie(outputFilePath, 30);	

	delete stabilizationMgr;

	//if(ESMGetConnectAJANetwork())
	//{
	//	CString strIPPath(outputFilePath.c_str());
	//	timeLineView->WriteIni(strIPPath);
	//}

	CString strIPPath(outputFilePath.c_str());
	timeLineView->WriteIni(strIPPath);

	ESMLog(5, _T("Stabilization Movie Making End"));

	return 0;
}

void CTimeLineView::MergeMovie(CString strTime)
{
	ESMLog(5, _T("MergeMovie Start"));
	CString strTempPath;
	CString strName = _T("4dmaker");
	if(ESMGetBaseBallFlag())
	{
		CString strInning = g_strInning[ESMGetBaseBallInning()];
		CTime time = CTime::GetCurrentTime();
		CString strTime = time.Format("%H_%M_%S");

		switch(ESMGetBaseBallTamplate())
		{
		case VK_NUMPAD1:	strName.Format(_T("%s_(#1)_%s"),strInning,strTime);		break;
		case VK_NUMPAD2:	strName.Format(_T("%s_(#2)_%s"),strInning,strTime);		break;
		case VK_NUMPAD3:	strName.Format(_T("%s_(#3)_%s"),strInning,strTime);		break;
		case VK_NUMPAD4:	strName.Format(_T("%s_(#4)_%s"),strInning,strTime);		break;
		case VK_NUMPAD5:	strName.Format(_T("%s_(#5)_%s"),strInning,strTime);		break;
		case VK_NUMPAD6:	strName.Format(_T("%s_(#6)_%s"),strInning,strTime);		break;
		case VK_NUMPAD7:	strName.Format(_T("%s_(#7)_%s"),strInning,strTime);		break;
		case VK_NUMPAD8:	strName.Format(_T("%s_(#8)_%s"),strInning,strTime);		break;
		case VK_NUMPAD9:	strName.Format(_T("%s_(#9)_%s"),strInning,strTime);		break;
		}
	}

	
	//CString strMovieFile = ESMGetServerRamPath()+ _T("\\4DMaker.mp4");
	SYSTEMTIME st;
	GetLocalTime(&st);

	CString strDate = _T("");
	strDate.Format(_T("%04d%02d%02d"),st.wYear,st.wMonth,st.wDay); 

	CString str4DMName = ESMGetFrameRecord();

	CESMFileOperation fo;
	CString strForder = _T("");
	strForder.Format(_T("%s\\2D\\%s"),ESMGetPath(ESM_PATH_OUTPUT), strDate);
	
	if(!fo.IsFileFolder(strForder))
		fo.CreateFolder(strForder);

	CString strMovieFile;
	strMovieFile.Format(_T("%s\\2D\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName);
	strTempPath.Format(_T("%s\\2D\\%s\\%s_event.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName);
	
	int nCount = 0;
	//CMiLRe 20151119 영상 저장 경로 변경
	/*if(ESMGetValue(ESM_VALUE_AJAONOFF))
	{
		strMovieFile.Format(_T("%s\\2D\\%s_%s.mp4"), ESMGetPath(ESM_PATH_OUTPUT) ,strName,strTime);
		strTempPath.Format(_T("%s\\2D\\%s_%s_event.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strName,strTime);
	}
	while(FileExists(strMovieFile))
	{
		strMovieFile.Format(_T("%s\\2D\\%s_%d.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strName, nCount);
		strTempPath.Format(_T("%s\\2D\\%s_%d_event.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strName, nCount);
		nCount++;
	}*/

	while(FileExists(strMovieFile))
	{
		strMovieFile.Format(_T("%s\\2D\\%s\\%s_%d.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName, nCount);
		strTempPath.Format(_T("%s\\2D\\%s\\%s_%d_event.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName, nCount);
		nCount++;
	}


	//jhhan 170327 불필요 제거
	//CString strBcakupFile;
	//strBcakupFile.Format(_T("%s\\2D\\BackUp\\%s_%s"),ESMGetPath(ESM_PATH_OUTPUT), strName, ESMGetFrameRecord());
	//CESMFileOperation fo;
	//fo.Delete(strMovieFile);

	/*if(fo.Delete(strMovieFile))
	{
		CString strMovieFile_;
		strMovieFile_.Format(_T("%s\\2D\\%s_.mp4"), ESMGetPath(ESM_PATH_OUTPUT),strName);

		fo.Delete(strMovieFile_);
	}else
	{
		if (FileExists(strMovieFile))
		{
			ESMLog(0,_T("[Movie] MergeMovie File Delete Fail]"),strMovieFile);
			strMovieFile.Format(_T("%s\\2D\\%s_.mp4"), ESMGetPath(ESM_PATH_OUTPUT),strName);
			strTempPath.Format(_T("%s\\2D\\%s_event.mp4"), ESMGetPath(ESM_PATH_OUTPUT),strName);
			fo.Delete(strMovieFile);
		}else
		{
			CString strMovieFile_;
			strMovieFile_.Format(_T("%s\\2D\\%s_.mp4"), ESMGetPath(ESM_PATH_OUTPUT),strName);

			fo.Delete(strMovieFile_);
		}
	}*/

	CString strFile;
	CString strCmd, strOpt, strOpt1, strOpt2;

	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));	

	m_movieMakingList.clear();
	ESMLog(1,_T("[CHECK_G][Movie] Create Movie ... [%s]"),strMovieFile);

	strOpt.Format(_T("-i \"concat:"));

	if(ESMGetValue(ESM_VALUE_INSERTLOGO))
	{
		CString LogoMovie;
		LogoMovie.Format(_T("%s\\img\\LogoMovie_intro.mp4"),ESMGetPath(ESM_PATH_HOME));	
		if(FileExists(LogoMovie))
		{
			strOpt.Append(LogoMovie);
			strOpt.Append(_T("|"));
		}
		else
			ESMLog(5, _T("Not Exist File[%s]"), LogoMovie);
	}

	CFile ReadFile;
	int lFileSize = 0;

	int nTotal = 0;
	if(ESMGetValue(ESM_VALUE_REPEAT_MOVIE))
		nTotal = ESMGetValue(ESM_VALUE_MOVIE_REPEAT_COUNT);
	else
		nTotal = 1;

	//170211 hjcho AJA 재생 후 Merge 시작!
	if(ESMGetValue(ESM_VALUE_AJAONOFF))
	{
		while(1)
		{
			if(map_MakingData[strTime].nAJACount == map_MakingData[strTime].nSendCount)
			{
				ESMLog(5,_T("[CHECK_Z]AJA Load Finish"));			
				break;
			}
			Sleep(15);
		}
	}

	//metadataTest	
	if(ESMGetValue(ESM_VALUE_LIGHTWEIGHT))
	{
		m_strTotalMetadata = _T("");
	}

	CString strFrontIP = ESMGetFrontIP();
	for(int n = 0; n < nTotal; n++)
	{
		if(n!=0)
			strOpt.Append(_T("|"));

		for(int i = 0 ; i < map_MakingData[strTime].nSendCount ; i ++)
		{
			if(ESMGetValue(ESM_VALUE_AJAONOFF))
			{
				strFile.Format(_T("%s\\%s\\%d.mp4"),ESMGetServerRamPath(),strTime, i);
			}
			else
			{
#if 1
				//TEST 확인필요

				/*int _nId = 0;
				_nId = GetClientId(i);
				if(_nId > -1)*/
				{
					//strFile.Format(_T("%s\\%d.mp4"),ESMGetServerRamPath(), i);
					//Time base Movie Save
					strFile.Format(_T("%s\\%s\\%d.mp4"),ESMGetServerRamPath(),strTime, i);
					//ESMLog(5, _T("MergeMovie : %s"), strFile);
				}

#else
				int _nId = 0;
				_nId = GetClientId(i);
				if(_nId > -1)
				{
					//strFile.Format(_T("\\\\192.168.0.%d\\Movie\\%d.mp4"), _nId, i);
					strFile.Format(_T("\\\\%s%d\\Movie\\%d.mp4"),strFrontIP, _nId, i);
					//ESMLog(5, _T("MergeMovie : %s"), strFile);
				}
#endif
			}
//#ifdef _SERVER_SAVE		//jhhan 17-01-16 Server Save
//			strFile.Format(_T("%s\\%s\\%d.mp4"),ESMGetServerRamPath(),strTime, i);
//#else
//			int _nId = 0;
//			_nId = GetClientId(i);
//			if(_nId > -1)
//			{
//				strFile.Format(_T("\\\\192.168.0.%d\\Movie\\%d.mp4"), _nId, i);
//				//ESMLog(5, _T("MergeMovie : %s"), strFile);
//			}
//#endif
			ESMLog(5,_T("[Movie] Merge Movie File [%s]"),strFile);
			if(ReadFile.Open(strFile, CFile::modeRead))
			{
				lFileSize = ReadFile.GetLength();
				ReadFile.Close();
			}
			else
				lFileSize = 0;

			ESMLog(5,_T("[Movie] FileCheck Start [%s]"),strFile);
			if (FileExists(strFile) && lFileSize > 0)
			{
				ESMLog(5,_T("[Movie] FileCheck End [%s]"),strFile);
				if(i!=0 && strOpt != _T("-i \"concat:"))
					strOpt.Append(_T("|"));
				strOpt.Append(strFile);
				
				//metadataTest		
				if(ESMGetValue(ESM_VALUE_LIGHTWEIGHT))
				{					
					GetMetadata(strFile);
					ESMLog(5, _T("[Metadata]Metadata Get From %d.mp4"), i);
				}
			}
			else
			{
				ESMLog(5, _T("Not Exist File[%s]"), strFile);
				{
					ESMEvent * pProcessor = new ESMEvent;
					pProcessor->message = WM_ESM_PROCESSOR_INFO_FAIL;
					/*pProcessor->nParam1 = nId;
					pProcessor->nParam2 = nFrameCnt;*/
					pProcessor->nParam3 = i;
					::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pProcessor);
				}
				continue;
			}
		}		
	}
			

	if(ESMGetValue(ESM_VALUE_INSERTLOGO))
	{
		CString LogoMovie;
		LogoMovie.Format(_T("%s\\img\\LogoMovie_end.mp4"),ESMGetPath(ESM_PATH_HOME));	
		if(FileExists(LogoMovie))
		{
			strOpt.Append(_T("|"));
			strOpt.Append(LogoMovie);
		}
		else
			ESMLog(5, _T("Not Exist File[%s]"), LogoMovie);
	}

	strOpt.Append(_T("\" -c copy -bsf:a aac_adtstoasc "));
	strOpt.Append(_T("\""));
	strOpt.Append(strMovieFile);
	strOpt.Append(_T("\""));

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	//fo.Copy(strMovieFile, strBcakupFile);


	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_MOVIE_END_MOVIE_MAKE;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);

	ESMVerfyMake();
	
	ESMLog(5,_T("[Movie] Create Movie Finish [%s]"), strMovieFile);
	if(ESMGetValue(ESM_VALUE_TEST_MODE) == TRUE)
		ESMLog(5,_T("[TC2] End"));

	//View Info
	if(ESMGetValue(ESM_VALUE_INSERT_CAMERAID))
	{
		if(ESMGetValue(ESM_VALUE_TEST_MODE))
		{
			if(ESMGetValue(ESM_VALUE_MAKEAUTOPLAY))
			{
				//CMiLRe 20151119 영상 저장 경로 변경
				TCHAR* cPath = new TCHAR[1+strMovieFile.GetLength()];
				_tcscpy(cPath, strMovieFile);

				ESMEvent* pMsg2 = NULL;
				pMsg2 = new ESMEvent();
				pMsg2->message = WM_ESM_VIEW_MAKEMOVIEPLAY;
				pMsg2->pParam = (LPARAM)cPath;
				::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
			}
		}

		if(ESMGetValue(ESM_VALUE_TEST_MODE))
		{
			int nFrameRate = 30;
			switch(ESMGetFrameRate())
			{
			case MOVIE_FPS_UHD_25P:		nFrameRate = 25; break;
			case MOVIE_FPS_UHD_30P:		nFrameRate = 30; break;
			case MOVIE_FPS_FHD_50P:		nFrameRate = 50; break;
			case MOVIE_FPS_FHD_30P:		nFrameRate = 30; break;
			case MOVIE_FPS_FHD_25P:		nFrameRate = 25; break;
			case MOVIE_FPS_FHD_60P:		nFrameRate = 60; break;
			case MOVIE_FPS_FHD_120P:	nFrameRate = 120; break;
			case MOVIE_FPS_UHD_30P_X2:	nFrameRate = 30; break;
			case MOVIE_FPS_UHD_60P_X2:	nFrameRate = 60; break;
			case MOVIE_FPS_UHD_30P_X1:	nFrameRate = 30; break;
			case MOVIE_FPS_UHD_60P_X1:	nFrameRate = 60; break;
			case MOVIE_FPS_UHD_60P:		nFrameRate = 60; break;
			case MOVIE_FPS_UHD_50P:		nFrameRate = 50; break;
			default: nFrameRate = 30; break;
			}

			CT2CA pszConvertedAnsiString (strMovieFile);
			std::string strPath(pszConvertedAnsiString);
			VideoCapture vc(strPath);
			int nWidth = (int)vc.get(cv::CAP_PROP_FRAME_WIDTH);
			int nHeight = (int)vc.get(cv::CAP_PROP_FRAME_HEIGHT);

			CString strOcrCheck = ESMGetPath(ESM_PATH_CONFIG) +  _T("\\tessdata\\eng.traineddata");
			HANDLE hFind;
			WIN32_FIND_DATA fd;

			if ((hFind = ::FindFirstFile(strOcrCheck, &fd)) != INVALID_HANDLE_VALUE)
			{
				FindClose (hFind);
			}
			else
			{
				FindClose (hFind);
				AfxMessageBox(strOcrCheck + _T("is not Exist!"));
				return;
			}

			CString strOutput;
			strOutput.Format(_T("%s\\2D\\%s\\(Info)%s_%d.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName,nCount);
			CT2CA pszConvertedAnsiString1 (strOutput);
			std::string strOutputPath(pszConvertedAnsiString1);
			VideoWriter writer;//(strOutputPath,)
			writer.open(strOutputPath,cv::VideoWriter::fourcc('m','p','e','g'), nFrameRate, cv::Size(nWidth,nHeight),true);

			CString strOcrDataPath = ESMGetPath(ESM_PATH_CONFIG) +  _T("/tessdata/");
			auto ocr = text::OCRTesseract::create(ESMUtil::CStringToChar(strOcrDataPath), "eng", "0123456789");

			map<CString, int> nArrAvgGap = ESMGetArrGapAvg();

			int nStart = GetTickCount();
			while(1)
			{
				Mat frame;
				vc>>frame;

				if(frame.empty())
					break;

				//ocr///////////////////////////////////////////////////////////////////
				Mat gray;
				//cvtColor(frame(Rect2d(200*2, 10*2, 500*2, 85*2)), gray, COLOR_RGB2GRAY);
				cvtColor(frame(Rect2d(100, 10, 500*2, 250)), gray, COLOR_RGB2GRAY);
				threshold(gray, gray, 230, 255, THRESH_BINARY);
				bitwise_not(gray, gray);
				resize(gray, gray, cv::Size(390/4, 125/4));

				std::string ocrText;
				std::vector<cv::Rect> pBoxes;
				std::vector<string> words;
				vector<float> confidences;

				ocr->run(gray, ocrText, &pBoxes, &words, &confidences, text::OCR_LEVEL_WORD);		

				CString strOcrText;
				if(ocrText.length() >= 5)
				{
					CString strGetOcrText(ocrText.c_str(), ocrText.length()-2);			
					strOcrText = strGetOcrText;
				}
				else
					strOcrText = _T("");

				rectangle(frame, cv::Point(frame.cols-620,70), cv::Point(frame.cols-150,320), Scalar(0, 0, 0), cv::FILLED );

				char* strText = new char[MAX_PATH];
				CString strData;
				strData.Format(_T("1.Resolution : %dx%d"), nWidth, nHeight);
				WideCharToMultiByte(CP_ACP, 0, strData, -1, strText, strData.GetLength()+1,NULL,NULL);
				putText(frame,strText,cv::Point(frame.cols-600,100),CV_FONT_HERSHEY_COMPLEX,1,cv::Scalar(255,255,255),3,8);

				if(ESMGetValue(ESM_VALUE_TEST_MODE))
					strData.Format(_T("2.Making Time : %.0f sec"), (double)ESMGetMakeMovieTime()/1000.0);
				else
					strData.Format(_T("2.Making Time : %.2f sec"), (double)ESMGetMakeMovieTime()/1000.0);
				WideCharToMultiByte(CP_ACP, 0, strData, -1, strText, strData.GetLength()+1,NULL,NULL);
				putText(frame,strText,cv::Point(frame.cols-600,150),CV_FONT_HERSHEY_COMPLEX,1,cv::Scalar(255,255,255),3,8);

				strData.Format(_T("3.FrameRate ; %dP"), nFrameRate);
				WideCharToMultiByte(CP_ACP, 0, strData, -1, strText, strData.GetLength()+1,NULL,NULL);
				putText(frame,strText,cv::Point(frame.cols-600,200),CV_FONT_HERSHEY_COMPLEX,1,cv::Scalar(255,255,255),3,8);

				if(ESMGetValue(ESM_VALUE_TEST_MODE))
					strData.Format(_T("4.Initial Time : %.0f Sec"), (double)ESMGetInitialTime()/1000.0);
				else
					strData.Format(_T("4.Initial Time : %.2f Sec"), (double)ESMGetInitialTime()/1000.0);
				WideCharToMultiByte(CP_ACP, 0, strData, -1, strText, strData.GetLength()+1,NULL,NULL);
				putText(frame,strText,cv::Point(frame.cols-600,250),CV_FONT_HERSHEY_COMPLEX,1,cv::Scalar(255,255,255),3,8);

				//map<CString, int>::iterator iterPos = nArrAvgGap.find(strOcrText);
				/*if(ESMGetValue(ESM_VALUE_TEST_MODE))
				{
				float fValue = nArrAvgGap[strOcrText]/1000.;
				strData.Format(_T("5.Time Tolerance : %.3f Sec"), fValue);
				}
				else
				{
				float nTime = 100;
				CObArray arDSCList;
				ESMGetDSCList(&arDSCList);
				if(arDSCList.GetCount())
				{
				CDSCItem* pItem = (CDSCItem*)arDSCList.GetAt(0);
				if(pItem)
				{
				if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
				nTime = 9.5;
				}
				}				

				strData.Format(_T("5.Time Tolerance : %.1f us"), (float)nArrAvgGap[strOcrText]*nTime);
				}
				WideCharToMultiByte(CP_ACP, 0, strData, -1, strText, strData.GetLength()+1,NULL,NULL);
				putText(frame,strText,cv::Point(frame.cols-600,300),CV_FONT_HERSHEY_COMPLEX,1,cv::Scalar(255,255,255),3,8);
				*/
				if(ESMGetValue(ESM_VALUE_TEST_MODE))
				{
					//Line Draw
					CObArray arDSCList; 			
					ESMGetDSCList(&arDSCList);
					CDSCItem* pItem = (CDSCItem*)arDSCList.GetAt(0);			

					stAdjustInfo pStAdjustData;
					CESMMovieMgr* pMovieMgr = (CESMMovieMgr*)ESMGetMovieMgr();
					pStAdjustData = pMovieMgr->GetAdjustData(pItem->GetDeviceDSCID());

					double dAngleTolerance = 0.8;

					double degree = 360. * (dAngleTolerance/100.);
					degree = degree * 3.141592/180.;

					int nDeltax_1 = 0 * cos(degree) + frame.rows/2 * sin(degree);
					int nDeltax_2 = 0 * -sin(degree) + frame.rows/2 * cos(degree);

					int nX_1 = pStAdjustData.AdjMove.x + pStAdjustData.AdjptRotate.x;
					int nY_1 = pStAdjustData.AdjMove.y + pStAdjustData.AdjptRotate.y;
					int nX_2 = pStAdjustData.AdjMove.x + pStAdjustData.AdjptRotate.x + nDeltax_1;			
					int nY_2 = pStAdjustData.AdjMove.y + pStAdjustData.AdjptRotate.y + nDeltax_2;
					line(frame, cv::Point(nX_1,nY_1), cv::Point(nX_2, nY_2), Scalar(128, 128, 255), 3, LINE_AA, 0);
					line(frame, cv::Point(nX_1,nY_1), cv::Point(nX_2,frame.rows-nY_2), Scalar(128, 128, 255), 3, LINE_AA, 0);

					degree = 360. * (-dAngleTolerance/100.);
					degree = degree * 3.141592/180.;

					nDeltax_1 = 0 * cos(degree) + frame.rows/2 * sin(degree);
					nDeltax_2 = 0 * -sin(degree) + frame.rows/2 * cos(degree);

					nX_1 = pStAdjustData.AdjMove.x + pStAdjustData.AdjptRotate.x;
					nY_1 = pStAdjustData.AdjMove.y + pStAdjustData.AdjptRotate.y;
					nX_2 = pStAdjustData.AdjMove.x + pStAdjustData.AdjptRotate.x + nDeltax_1;			
					nY_2 = pStAdjustData.AdjMove.y + pStAdjustData.AdjptRotate.y + nDeltax_2;

					line(frame, cv::Point(nX_1,nY_1), cv::Point(nX_2, nY_2), Scalar(128, 128, 255), 3, LINE_AA, 0);
					line(frame, cv::Point(nX_1,nY_1), cv::Point(nX_2,frame.rows-nY_2), Scalar(128, 128, 255), 3, LINE_AA, 0);
					/////////////////////////////
				}

				writer<<frame;
			}
			int nEnd = GetTickCount();
			ESMLog(1,_T("#######GAP!!! %d"), nEnd - nStart);

			writer.release();
			strMovieFile = strOutput; 
		}
	}

	ESMEvent* pMsg1 = NULL;
	pMsg1 = new ESMEvent();
	pMsg1->message = WM_ESM_VIEW_SAVE_INFO;
	pMsg1->nParam1 = TRUE;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg1);

	//joonho.kim AutoPlay
	ESMSetRecordingInfoString("AutoMakingPath", strMovieFile);
	ESMSetRecordingInfoint("AutoMakingFlag", 1);

	if(ESMGetValue(ESM_VALUE_ENCODING_FOR_SMARTPHONES))
	{
		int nBitrate = ESMGetValue(ESM_VALUE_BITRATE_FOR_SMARTPHONE);
		ESMLog(5, _T("[Movie]Re-Encoding For Android....."));
		if(nBitrate == 0)
		{
			ESMLog(0, _T("[Movie]Bitrate: %dMbps.....\nChangeBitrate: 5"),nBitrate);
			nBitrate = 5;
		}
		CString strBitrate;
		strBitrate.Format(_T(" -b:v %dM "),nBitrate);

		CString strReEncodingOpt;
		CString strReEncodingMovieFile;
		strReEncodingOpt.Append(_T(" -i \""));
		strReEncodingOpt.Append(strMovieFile);
		strReEncodingOpt.Append(_T("\""));
		strReEncodingOpt.Append(_T(" -c:v libx264 -g 6 -preset ultrafast "));
		strReEncodingOpt.Append(strBitrate);

		strReEncodingMovieFile.Format(_T("%s\\2D\\%s\\%s_%s.ts"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName,_T("Android"));
		
		int nCount = 0;
		while(FileExists(strReEncodingMovieFile))
		{
			strReEncodingMovieFile.Format(_T("%s\\2D\\%s\\%s_%d_%s.ts"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName,nCount,_T("Android"));
			nCount++;
		}

		strReEncodingOpt.Append(_T("\""));
		strReEncodingOpt.Append(strReEncodingMovieFile);
		strReEncodingOpt.Append(_T("\""));
		
		SHELLEXECUTEINFO lpExecInfoReEncoding;
		lpExecInfoReEncoding.cbSize  = sizeof(SHELLEXECUTEINFO);
		lpExecInfoReEncoding.lpFile = strCmd;
		lpExecInfoReEncoding.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		lpExecInfoReEncoding.hwnd = NULL;  
		lpExecInfoReEncoding.lpVerb = L"open";
		lpExecInfoReEncoding.lpParameters = strReEncodingOpt;
		lpExecInfoReEncoding.lpDirectory = NULL;

		lpExecInfoReEncoding.nShow = SW_HIDE; // hide shell during execution
		lpExecInfoReEncoding.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
		ShellExecuteEx(&lpExecInfoReEncoding);

		// wait until the process is finished
		if (lpExecInfoReEncoding.hProcess != NULL)
		{
			::WaitForSingleObject(lpExecInfoReEncoding.hProcess, INFINITE);
			::CloseHandle(lpExecInfoReEncoding.hProcess);
		}
		ESMLog(5, _T("[Movie]Re-Encoding End"));
	}
	
	if(ESMGetValue(ESM_VALUE_GIF_MAKING))
	{
		CreateGifFile(strMovieFile,strDate,str4DMName);
	}

	//wgkim@esmlab.com 17-05-17
	if(ESMGetValue(ESM_VALUE_TEMPLATE_STABILIZATION))
	{				
		if( m_hThreadHandle != NULL) 
		{
			CloseHandle(m_hThreadHandle);
			m_hThreadHandle = NULL;
		}
		m_strMovieFile = strMovieFile;
		m_hThreadHandle = (HANDLE) _beginthreadex(NULL, 0, MakeStabiliationMovie, (void *)this, 0, NULL);					
	}

	//metadataTest
	if(ESMGetValue(ESM_VALUE_LIGHTWEIGHT))
	{
		ESMLog(5, _T("[Metadata]Metadata Merge Start"));
		SetMetadata(strMovieFile, m_strTotalMetadata);
		ESMLog(5, _T("[Metadata]Metadata Merge End"));
	}

	if(ESMGetBaseBallFlag())
	{
		m_arrMoviePath.push_back(strMovieFile);

		ESMEvent* pMsg2 = NULL;
		pMsg2 = new ESMEvent();
		pMsg2->message = WM_ESM_VIEW_MERGEMOVIE_PUSHBACK;

		CString str[2];
		str[0] = strMovieFile;
		str[1] = strName;
		pMsg2->pParam = (LPARAM)&str;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
		ESMLog(1,_T("%s"),strName);
	}	

	if(ESMGetValue(ESM_VALUE_MAKEAUTOPLAY))
	{
		//CMiLRe 20151119 영상 저장 경로 변경
		TCHAR* cPath = new TCHAR[1+strMovieFile.GetLength()];
		_tcscpy(cPath, strMovieFile);

		ESMEvent* pMsg2 = NULL;
		pMsg2 = new ESMEvent();
		pMsg2->message = WM_ESM_VIEW_MAKEMOVIEPLAY;
		pMsg2->pParam = (LPARAM)cPath;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
	}

	//hjcho 170612 메이킹 완료 후 Watcher로 전송
	if(fo.IsFileExist(strMovieFile))
	{
		WriteIni(strMovieFile);
	}

	if(ESMGetValue(ESM_VALUE_CEREMONYUSE))
	{
#if 1 //-- 이벤트 테스트 코드 (영상 사이즈 변경)
		{
			CString strCeremonyWidth;
			CString strCeremonyHeight;
			CString strCeremonyCropWidth;
			CString strCeremonyCropHeight;

			strCeremonyWidth			= ESMGetCeremony(ESM_VALUE_CEREMONY_WIDTH);
			strCeremonyHeight			= ESMGetCeremony(ESM_VALUE_CEREMONY_HEIGHT);

			if(ESMGetValue(ESM_VALUE_CEREMONYRATIO))
				strCeremonyCropWidth	= ESMGetCeremony(ESM_VALUE_CEREMONY_HEIGHT);
			else
				strCeremonyCropWidth	= ESMGetCeremony(ESM_VALUE_CEREMONY_WIDTH);

			strCeremonyCropHeight		= ESMGetCeremony(ESM_VALUE_CEREMONY_HEIGHT);

			strOpt.Format(_T("-i \"%s\" -filter:v \"scale=%s:%s, crop=%s:%s\" -c:a copy \"%s\""),
				strMovieFile, 
				strCeremonyWidth,
				strCeremonyHeight,
				strCeremonyCropWidth,
				strCeremonyCropHeight,
				strTempPath );

			SHELLEXECUTEINFO lpExecInfo;
			lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
			lpExecInfo.lpFile = strCmd;
			lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
			lpExecInfo.hwnd = NULL;  
			lpExecInfo.lpVerb = L"open";
			lpExecInfo.lpParameters = strOpt;
			lpExecInfo.lpDirectory = NULL;

			lpExecInfo.nShow = SW_HIDE; // hide shell during execution
			lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
			ShellExecuteEx(&lpExecInfo);

			// wait until the process is finished
			if (lpExecInfo.hProcess != NULL)
			{
				::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
				::CloseHandle(lpExecInfo.hProcess);
			}

			strMovieFile = strTempPath;
		}
		////////////////////////////
#endif

		CString strConcertPath;
		//CString strTel1 = ESMGetCeremonyInfoString(_T("InputTel"));
		CString strTel1 = ESMGetCeremonyInfoString(_T("OutputTel"));

		//file check
		int nIndex = 1;
		strConcertPath.Format(_T("%s\\%s.mp4"), ESMGetCeremony(ESM_VALUE_CEREMONYMOVIEPATH), strTel1);
		
		if(ISFILE(strConcertPath))
		{
			while(1)
			{
				strConcertPath.Format(_T("%s\\%s_%d.mp4"), ESMGetCeremony(ESM_VALUE_CEREMONYMOVIEPATH), strTel1 , nIndex);

				if(!ISFILE(strConcertPath))//파일이 없을 경우
					break;
				else
					nIndex++;
			}
		}

		CopyFile(strMovieFile, strConcertPath, FALSE);
		//ChangeMovieSize(strMovieFile, strConcertPath);
		//CHange Siize End
		//ESMSetCeremonyInfoString(_T("TelNum2"), strTel1);
		//ESMSetCeremonyInfoString(_T("OutputTel"), strTel1);
		ESMSetCeremonyInfoint(_T("SelectorSignal"), 1);
		ESMSetCeremonyInfoint(_T("PrintorSignal"), 2);
	}

	/*
	if(ESMGetValue(ESM_VALUE_MAKEAUTOPLAY))
	{

		//CMiLRe 20151119 영상 저장 경로 변경
		TCHAR* cPath = new TCHAR[1+strMovieFile.GetLength()];
		_tcscpy(cPath, strMovieFile);

		ESMEvent* pMsg2 = NULL;
		pMsg2 = new ESMEvent();
		pMsg2->message = WM_ESM_VIEW_MAKEMOVIEPLAY;
		pMsg2->pParam = (LPARAM)cPath;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg2);
	}*/

	//jhhan 17-01-09
#if _DELETE_JHHAN
	strFile.Format(_T("%s"), ESMGetServerRamPath());
	fo.Delete(strFile,FALSE);
#endif

	if(ESMGetValue(ESM_VALUE_AJAONOFF))
	{
		//map_MakingData.erase(map_MakingData.find(strTime));
		
		//Movie 시간 얻어오기


		//Add MovieList
		ESMEvent * pMovieList = new ESMEvent();

		CString * pTime =new CString;
		pTime->Format(_T("%s"), strTime);
		CString * pPath =new CString;
		pPath->Format(_T("%s"), strMovieFile);
		pMovieList->message = WM_ESM_MOVIELIST_ADDFILE;			//ADD
		pMovieList->nParam1 = TRUE;
		pMovieList->pParam = (LPARAM)pTime;
		pMovieList->pDest	   = (LPARAM) pPath;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMovieList);

		//List 던진 후 파일 삭제
		//CString strPath;
		//CESMFileOperation fo;
		//strPath.Format(_T("%s\\%s"),ESMGetClientRamPath(),strTime);
		//fo.Delete(strPath);
	}

	ESMLog(5,_T("[CHECK_H]Make Movie Finish [%d], Compete Time[ %d ms ]"), GetTickCount(), ESMGetMakeMovieTime());

	ESMEvent* pMsgDelete	= new ESMEvent;
	pMsgDelete->message = WM_ESM_FRAME_FOCUS;
	::PostMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsgDelete);

	/* //jhhan 16-12-02 ObjectDelete
	ESMEvent* pMsgDelete	= new ESMEvent;
	pMsgDelete->message = WM_ESM_FRAME_REMOVEALL_OBJ_EDITOR;
	::PostMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsgDelete);
	*/
#ifdef CMiLRe0
	SetMakingMovieFlag(FALSE);
#endif
}

BOOL CTimeLineView::ISFILE(CString szFile)
{
	HANDLE hFind;
	WIN32_FIND_DATA fd;
	if ((hFind = ::FindFirstFile(szFile, &fd)) != INVALID_HANDLE_VALUE) {
		FindClose(hFind);
		return TRUE;
	}
	return FALSE;
}


void CTimeLineView::ChangeMovieSize(CString strMovieFile, CString strConcertPath)
{
	//Change  Size
	CString strCmd, strOpt;
	strCmd.Format(_T("\"%s\\bin\\ffmpeg.exe\""),ESMGetPath(ESM_PATH_HOME));

	ESMLog(1,_T("[Movie] Change Size Movie ... [%s]"),strMovieFile);

	strOpt.Format(_T("-threads 4 -y -i "));
	strOpt.Append(_T("\""));
	strOpt.Append(strMovieFile);
	strOpt.Append(_T("\""));
	strOpt.Append(_T(" -r 29.97 -vcodec libx264 -s 1920x1080 -b:v 8000k -profile:v high "));
	strOpt.Append(_T("\""));
	strOpt.Append(strConcertPath);
	strOpt.Append(_T("\""));

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;

	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if(lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
}

void CTimeLineView::CheckValidFrame(vector<MakeFrameInfo>* pArrFrameInfo)
{
	ESMLog(1,_T("Check Valid Start"));

	CString strTemp;
	CString strDSC, strCheckDSC;
	CString strDst;
	CString strSrc;
	BOOL bValid = TRUE;
	CObArray arDSCList; 
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nStart, nEnd;

	CString strPrePath;
	CString strCurPath;

#ifdef _CHANGE_PATH_RECORDSAVE
	CString strFile = ESMGetPath(ESM_PATH_MOVIE_FILE);
	if(strFile.Find(_T("HOME")) != -1)
		strFile.Replace(_T("$(HOME)"), _T("\\4DMaker"));	
	else
	{
		int nPos;
		nPos = strFile.Find(_T("\\"));
		strFile.Delete(0,nPos+1);
	}
#endif

	int nCheckTime_Start, nCheckTime_nEnd;

	nCheckTime_Start = GetTickCount();
	//ESMLog(1,_T("Check 1 Start"));
	vector<int> nArrDeleteIdx;
	for(int i = 0; i < pArrFrameInfo->size(); i++)
	{
		CESMFileOperation fo;
		// 영상 check

		nStart = GetTickCount();
// 
// 		strCurPath.Format(_T("%s"),pArrFrameInfo->at(i).strFramePath);
// 		if(strCurPath.CompareNoCase(strPrePath) == 0 && i != 0)
// 		{
// 			pArrFrameInfo->at(i).bValid = pArrFrameInfo->at(i-1).bValid;
// 		}
// 		else
// 		{
// 			strPrePath = strCurPath;
// 			if(fo.IsFileExist(pArrFrameInfo->at(i).strFramePath))
// 			{
// #ifdef _CHANGE_PATH_RECORDSAVE
// 				//경로변경
// 				CString strRecord;
// 				strRecord.Format(_T("%s"), pArrFrameInfo->at(i).strFramePath);
// 				strRecord.Replace(_T("Movie"), strFile/*_T("RecordSave\\Record\\Movie\\files")*/);
// 				//_stprintf(pArrFrameInfo->at(i).strFramePath, strRecord);//유니코드
// #endif
// 				CFile file;
// 				if (file.Open(pArrFrameInfo->at(i).strFramePath, CFile::modeRead))
// 				{
// 					if(file.GetLength() > 0)
// 						pArrFrameInfo->at(i).bValid = TRUE;
// 					else
// 						pArrFrameInfo->at(i).bValid = FALSE;
// 
// 					file.Close();
// 				}
// 				else
// 					pArrFrameInfo->at(i).bValid = FALSE;
// 			}
// 			else
// 			{
// 				//ESMLog(1, _T("File not exist [%s]") , pArrFrameInfo->at(i).strFramePath);
// 				pArrFrameInfo->at(i).bValid = FALSE;
// 			}
// 		}
// 

	
#ifdef _CHANGE_PATH_RECORDSAVE
		//경로변경
		CString strRecord;
		strRecord.Format(_T("%s"), pArrFrameInfo->at(i).strFramePath);
		strRecord.Replace(_T("Movie"), strFile/*_T("RecordSave\\Record\\Movie\\files")*/);
		_stprintf(pArrFrameInfo->at(i).strFramePath, strRecord);//유니코드
#endif
		
		nEnd = GetTickCount();
		if(nEnd - nStart > 1000)
		{
			ESMLog(1,_T("Movie Check Slow path = %s, time = %d ms"), pArrFrameInfo->at(i).strFramePath, nEnd - nStart);
		}

		// 연속성 유효 check
		BOOL bNext = FALSE;
		//if(i < pArrFrameInfo->size()-1)
		if(i == 0)
		{
			strDSC.Format(_T("%s"),pArrFrameInfo->at(i).strDSC_ID );
			strCheckDSC.Format(_T("%s"),pArrFrameInfo->at(i+1).strDSC_ID );
			bNext = TRUE;
		}
		else
		{
			strDSC.Format(_T("%s"),pArrFrameInfo->at(i).strDSC_ID );
			strCheckDSC.Format(_T("%s"),pArrFrameInfo->at(i-1).strDSC_ID );	
		}

		if(strDSC.CompareNoCase(strCheckDSC) == 0)//동일 DSC
		{
			if(bNext)
			{
				if(pArrFrameInfo->at(i).nFrameIndex == pArrFrameInfo->at(i+1).nFrameIndex)
				{
					if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork() == TRUE)
						pArrFrameInfo->at(i).bPlay = FALSE;
					else
						nArrDeleteIdx.push_back(i);
				}
				else
					pArrFrameInfo->at(i).bPlay = TRUE;
			}
			else
			{
				pArrFrameInfo->at(i).bPlay = TRUE;

				if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork() == TRUE)
				{
					if(pArrFrameInfo->at(i).nFrameIndex < pArrFrameInfo->at(i-1).nFrameIndex)
					{
						CString strPath, strPrePath;
						strPath.Format(_T("%s"), pArrFrameInfo->at(i).strFramePath);
						strPrePath.Format(_T("%s"), pArrFrameInfo->at(i-1).strFramePath);
						if(strPath.CompareNoCase(strPrePath) == 0)
							pArrFrameInfo->at(i).bPlay = FALSE;
					}
					else if(pArrFrameInfo->at(i).nFrameIndex == pArrFrameInfo->at(i-1).nFrameIndex &&
						_tcscmp(pArrFrameInfo->at(i).strFramePath, pArrFrameInfo->at(i-1).strFramePath) == 0)
					{
						nArrDeleteIdx.push_back(i - 1);//전 프레임 삭제 추가
						if(pArrFrameInfo->at(i).bSameDSC)
							pArrFrameInfo->at(i).bPlay = TRUE;
						else
							pArrFrameInfo->at(i).bPlay = FALSE;
						//ESMLog(5,_T("[%d] is Same Frame Index"),i-1);
					}
					else
						pArrFrameInfo->at(i).bPlay = TRUE;
				}
				else
				{
					if(pArrFrameInfo->at(i).nFrameIndex <= pArrFrameInfo->at(i-1).nFrameIndex)
					{
						CString strPath, strPrePath;
						strPath.Format(_T("%s"), pArrFrameInfo->at(i).strFramePath);
						strPrePath.Format(_T("%s"), pArrFrameInfo->at(i-1).strFramePath);
						if(strPath.CompareNoCase(strPrePath) == 0)
							pArrFrameInfo->at(i).bPlay = FALSE;
					}
					else
						pArrFrameInfo->at(i).bPlay = TRUE;
				}
			}
		}
		else //동일 DSC가 아닐경우
		{
			if(i > 0)
			{
				//CString strPreDSC;
				//strPreDSC.Format(_T("%s"),pArrFrameInfo->at(i-1).strDSC_ID );

				//if(strDSC.CompareNoCase(strPreDSC) == 0)
				if(strDSC.CompareNoCase(strCheckDSC) == 0)
				{
					if(pArrFrameInfo->at(i).nFrameIndex == pArrFrameInfo->at(i-1).nFrameIndex)
						pArrFrameInfo->at(i).bPlay = FALSE;
					else
						pArrFrameInfo->at(i).bPlay = TRUE;
				}
				else
					pArrFrameInfo->at(i).bPlay = FALSE;
			}
			else
				pArrFrameInfo->at(i).bPlay = FALSE;
		}

		//Sensor Out Check
		//Tick Out Check
		
		for( int nIndex = 0; nIndex < arDSCList.GetSize(); nIndex++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(nIndex);
			if( pItem )
			{
				if(pItem->GetDeviceDSCID() == strDSC)
				{
					pArrFrameInfo->at(i).bSensorSync = pItem->GetSensorSync();
					pArrFrameInfo->at(i).bTickSync	 = pItem->GetTickStatus();
					break;
				}
			}
		}


		//Check Valid
		strCurPath.Format(_T("%s"),pArrFrameInfo->at(i).strFramePath);
		if(pArrFrameInfo->at(i).bPlay)
		{
			if(strCurPath.CompareNoCase(strPrePath) == 0 && i != 0)
			{
				pArrFrameInfo->at(i).bValid = pArrFrameInfo->at(i-1).bValid;
			}
			else
			{
				strPrePath = strCurPath;
				if(fo.IsFileExist(pArrFrameInfo->at(i).strFramePath))
				{
#ifdef _CHANGE_PATH_RECORDSAVE
					//경로변경
					CString strRecord;
					strRecord.Format(_T("%s"), pArrFrameInfo->at(i).strFramePath);
					strRecord.Replace(_T("Movie"), strFile/*_T("RecordSave\\Record\\Movie\\files")*/);
					//_stprintf(pArrFrameInfo->at(i).strFramePath, strRecord);//유니코드
#endif
					CFile file;
					if (file.Open(pArrFrameInfo->at(i).strFramePath, CFile::modeRead))
					{
						if(file.GetLength() > 0)
							pArrFrameInfo->at(i).bValid = TRUE;
						else
							pArrFrameInfo->at(i).bValid = FALSE;

						file.Close();
					}
					else
						pArrFrameInfo->at(i).bValid = FALSE;
				}
				else
				{
					//ESMLog(1, _T("File not exist [%s]") , pArrFrameInfo->at(i).strFramePath);
					pArrFrameInfo->at(i).bValid = FALSE;
				}
			}
		}
		else
		{
			pArrFrameInfo->at(i).bValid = TRUE;
		}
		/*//Sensor out check
		if(pItem)
		{
			if(pArrFrameInfo->at(i).bSensorSync == FALSE)
			{
				pArrFrameInfo->at(i).bValid = FALSE;
			}

			if(pArrFrameInfo->at(i).bSensorSync == FALSE && pArrFrameInfo->at(i).bPlay == FALSE)
			{
				ESMLog(5, _T("SensorSync Skip DSC[%s]"), pItem->GetDeviceDSCID());
				pArrFrameInfo->erase(pArrFrameInfo->begin() + i);
				i--;
			}
		}*/
	}

	if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork() == TRUE)
	{
		int nDeleteCnt = 0;
		vector<int>nTempDeleteIdx;
		int nDeleteIdx;
		for(int i = 0; i < nArrDeleteIdx.size(); i++)
		{
			//근처 제일 낮은 framerate를 가진 아이들을 지운다.
			int nCurIdx = nArrDeleteIdx.at(i);
			int nCurFrameIdx  = pArrFrameInfo->at(nCurIdx).nFrameIndex;
			int nCurFrameRate = pArrFrameInfo->at(nCurIdx).nFrameSpeed;

			int nStartIdx = 0;
			if(nCurIdx + 1 >= pArrFrameInfo->size()-1)
				nStartIdx = pArrFrameInfo->size()-1;
			else
				nStartIdx = nCurIdx + 1;

			for(int j = nStartIdx ; j < pArrFrameInfo->size() ; j++)
			{
				if(_tcscmp(pArrFrameInfo->at(j).strFramePath, pArrFrameInfo->at(nCurIdx).strFramePath) == 0)
				{
					if(pArrFrameInfo->at(j).nFrameIndex == nCurFrameIdx)
					{
						if(pArrFrameInfo->at(j).nFrameSpeed < nCurFrameRate )
						{
							//다음 영상의 frame rate가 현재 frame rate보다 높거나 같을 경우 이전의 값을 삭제
							nTempDeleteIdx.push_back(j);
						}
						else
							nTempDeleteIdx.push_back(nCurIdx);
					}
					else
						break;
				}
				else//Frame Index가 같지 않으면
				{
					break;
				}
			}
		}
		for(int i = nTempDeleteIdx.size() - 1 ; i >=0 ; i--)
		{
			int nDeleteIdx = nTempDeleteIdx.at(i);
			if(i==0)
				pArrFrameInfo->erase(pArrFrameInfo->begin() + nDeleteIdx);
			else
			{
				int nPrevIdx = nTempDeleteIdx.at(i-1);
				if(nPrevIdx == nDeleteIdx)
					continue;
				else
					pArrFrameInfo->erase(pArrFrameInfo->begin() + nDeleteIdx);
			}
		}
	}

	nCheckTime_nEnd = GetTickCount();
	ESMLog(1,_T("##CHECK_VALID : Open File Check %d ms"), nCheckTime_nEnd - nCheckTime_Start);
	nCheckTime_Start = GetTickCount();
	
	///Test Code - valid 오류
	for(int i = 1; i < pArrFrameInfo->size(); i++)
	{
		if(!pArrFrameInfo->at(i).bValid)
		{
			CString strCurID,strPreID,strSrc;
			strCurID.Format(_T("%s"),pArrFrameInfo->at(i).strDSC_ID);
			strPreID.Format(_T("%s"),pArrFrameInfo->at(i-1).strDSC_ID);
			if(strCurID.CompareNoCase(strPreID) == 0)
			{
				for(int n = i; n >= 0; n--)
				{
					strPreID.Format(_T("%s"),pArrFrameInfo->at(n).strDSC_ID);
					if(strCurID.CompareNoCase(strPreID) == 0)
						pArrFrameInfo->at(n).bValid = FALSE;
				}
				for(int n = i; n < pArrFrameInfo->size(); n++)
				{
					strPreID.Format(_T("%s"),pArrFrameInfo->at(n).strDSC_ID);
					if(strCurID.CompareNoCase(strPreID) == 0)
						pArrFrameInfo->at(n).bValid = FALSE;
				}
			}
		}
	}
	
	//ESMLog(1,_T("Check 2 Start"));
	// 연속성 유효 check
	for(int i = 0; i < pArrFrameInfo->size(); i++)
	{
		bValid = TRUE;

		BOOL b1,b2,b3;
		b1 = pArrFrameInfo->at(i).bSensorSync;
		//b2 = pArrFrameInfo->at(i).bTickSync;
		b3 = pArrFrameInfo->at(i).bValid;


		//if(!pArrFrameInfo->at(i).bSensorSync || !pArrFrameInfo->at(i).bTickSync || !pArrFrameInfo->at(i).bValid)	
		//if(!b1 || !b2 || !b3)	
		if(!b1 || !b3)	
			bValid = FALSE;

		if(bValid == FALSE && pArrFrameInfo->at(i).bPlay == TRUE)
		{
			int nNextIndex = i;
			int nPreIndex = i;

			while(1)
			{
				if( nNextIndex < pArrFrameInfo->size()-1 )
				{
					if(pArrFrameInfo->at(nNextIndex).bValid == TRUE &&
						pArrFrameInfo->at(nNextIndex).bSensorSync == TRUE )
					{
						ESMLog(1, _T("Change Frame Src[%s]") , pArrFrameInfo->at(i).strFramePath);
						strSrc.Format(_T("%s"), pArrFrameInfo->at(i).strFramePath);
						strSrc.Replace(pArrFrameInfo->at(i).strDSC_ID, pArrFrameInfo->at(nNextIndex).strDSC_ID);
						strSrc.Replace(pArrFrameInfo->at(i).strDSC_IP, pArrFrameInfo->at(nNextIndex).strDSC_IP);
						pArrFrameInfo->at(i).bSensorSync = TRUE;
						if(pArrFrameInfo->at(i).strCameraID[0] != _T('\0'))
							memcpy(pArrFrameInfo->at(i).strCameraID, pArrFrameInfo->at(nNextIndex).strDSC_ID, MAX_PATH);

						_stprintf(pArrFrameInfo->at(i).strFramePath, strSrc);//유니코드


						//Local Path
						strSrc.Format(_T("%s"), pArrFrameInfo->at(i).strLocal);
						strSrc.Replace(pArrFrameInfo->at(i).strDSC_ID, pArrFrameInfo->at(nNextIndex).strDSC_ID);
						_stprintf(pArrFrameInfo->at(i).strLocal, strSrc);//유니코드

						
						ESMLog(1, _T("Change Frame Dst[%s]") , pArrFrameInfo->at(i).strFramePath);

						//pArrFrameInfo->at(i).stEffectData = pArrFrameInfo->at(nNextIndex).stEffectData;
						break;
					}
					else
						nNextIndex++;
				}
				else
				{
					if(pArrFrameInfo->at(nPreIndex).bValid == TRUE &&
						pArrFrameInfo->at(nPreIndex).bSensorSync == TRUE )
					{
						ESMLog(1, _T("Change Frame Src[%s]") , pArrFrameInfo->at(i).strFramePath);
						strSrc.Format(_T("%s"), pArrFrameInfo->at(i).strFramePath);
						strSrc.Replace(pArrFrameInfo->at(i).strDSC_ID, pArrFrameInfo->at(nPreIndex).strDSC_ID);
						strSrc.Replace(pArrFrameInfo->at(i).strDSC_IP, pArrFrameInfo->at(nPreIndex).strDSC_IP);
						pArrFrameInfo->at(i).bSensorSync = TRUE;
						if(pArrFrameInfo->at(i).strCameraID[0] != _T('\0'))
							memcpy(pArrFrameInfo->at(i).strCameraID, pArrFrameInfo->at(nNextIndex).strDSC_ID, MAX_PATH);

						_stprintf(pArrFrameInfo->at(i).strFramePath, strSrc);//유니코드
						
						//Local Path
						strSrc.Format(_T("%s"), pArrFrameInfo->at(i).strLocal);
						strSrc.Replace(pArrFrameInfo->at(i).strDSC_ID, pArrFrameInfo->at(nPreIndex).strDSC_ID);
						_stprintf(pArrFrameInfo->at(i).strLocal, strSrc);//유니코드

						ESMLog(1, _T("Change Frame Dst[%s]") , pArrFrameInfo->at(i).strFramePath);

						//pArrFrameInfo->at(i).stEffectData = pArrFrameInfo->at(nPreIndex).stEffectData;
						break;
					}
					else
						nPreIndex--;

					if(nPreIndex < 0)
						break;
				}
			}
		}
	}

	nCheckTime_nEnd = GetTickCount();
	ESMLog(1,_T("##CHECK_VALID : Play Valid Check %d ms"), nCheckTime_nEnd - nCheckTime_Start);
	nCheckTime_Start = GetTickCount();

	//Sensor out check
	for(int n = 0; n < pArrFrameInfo->size(); n++)
	{
		if(pArrFrameInfo->at(n).bSensorSync == FALSE)
		{
			pArrFrameInfo->at(n).bValid = FALSE;
		}

#if 1
		if(pArrFrameInfo->at(n).bPlay == FALSE)
		{
			if(pArrFrameInfo->at(n).bSensorSync == FALSE)
			{
				ESMLog(5, _T("SensorSync Skip DSC[%s]"), pArrFrameInfo->at(n).strDSC_ID);
				pArrFrameInfo->erase(pArrFrameInfo->begin() + n);
				n--;
			}
			else if(pArrFrameInfo->at(n).bValid == FALSE)
			{
				pArrFrameInfo->erase(pArrFrameInfo->begin() + n);
				n--;
			}
		}
#else
		if(pArrFrameInfo->at(n).bSensorSync == FALSE && pArrFrameInfo->at(n).bPlay == FALSE )
		{
			//ESMLog(5, _T("SensorSync Skip DSC[%s]"), pItem->GetDeviceDSCID());
			ESMLog(5, _T("SensorSync Skip DSC[%s]"), pArrFrameInfo->at(n).strDSC_ID);
			pArrFrameInfo->erase(pArrFrameInfo->begin() + n);
			n--;
		}
		else if(pArrFrameInfo->at(n).bValid == FALSE && pArrFrameInfo->at(n).bPlay == FALSE)
		{
			pArrFrameInfo->erase(pArrFrameInfo->begin() + n);
			n--;
		}
#endif
	}

	nCheckTime_nEnd = GetTickCount();
	ESMLog(1,_T("##CHECK_VALID : Sensor Out Check %d ms"), nCheckTime_nEnd - nCheckTime_Start);
	nCheckTime_Start = GetTickCount();

#ifndef _CHECK_DSCID
	//jhhan 170418 Play 구간 DSCID 통일화 - CheckVaild에서 DSCID 변경에 따른 수정
	vector<int> arryCount;
	BOOL _bPlay = FALSE;
	for(int i = 0; i < pArrFrameInfo->size(); i++)
	{
		//#if _DEBUG
		//		ESMLog(5, _T("[ID_CHANGE_ALL_INFO] - %s"), pArrFrameInfo->at(i).strFramePath);
		//#endif
		if(pArrFrameInfo->at(i).bPlay == TRUE)
		{
			arryCount.push_back(i);
		}else
		{
			if(arryCount.size() > 0)
			{
				CString strDscId = _T("");
				CString strDscIp = _T("");
				int nIdx = arryCount.size() - 1;
				//strDscId = ESMGetDSCIDFromPath(pArrFrameInfo->at(arryCount.at(nIdx)).strFramePath);
				strDscId = pArrFrameInfo->at(arryCount.at(nIdx)).strDSC_ID;
				strDscIp = pArrFrameInfo->at(arryCount.at(nIdx)).strDSC_IP;

				for(int n = 0; n < arryCount.size(); n++)
				{
					CString strPath = pArrFrameInfo->at(arryCount.at(n)).strFramePath;
					//CString strId = ESMGetDSCIDFromPath(strPath);
					CString strId = pArrFrameInfo->at(arryCount.at(n)).strDSC_ID;
					CString strIp = pArrFrameInfo->at(arryCount.at(n)).strDSC_IP;

					if(strDscId != strId)
					{
						strPath.Replace(strId, strDscId);
						strPath.Replace(strIp, strDscIp);

						_stprintf(pArrFrameInfo->at(arryCount.at(n)).strFramePath, strPath);

						ESMLog(1, _T("Change : [ID] %s -> %s, [IP] %s -> %s "), strId, strDscId, strIp, strDscIp);
					}
				}
				arryCount.clear();
			}
		}

	}
#endif


	ESMLog(1,_T("[CHECK_F]Check Valid End"));
	//Sleep(100);
}
/*
void CTimeLineView::CheckValidFrame(vector<MakeFrameInfo>* pArrFrameInfo)
{
	ESMLog(1,_T("Check Valid Start"));

	CString strTemp;
	CString strDSC, strCheckDSC;
	CString strDst;
	CString strSrc;
	BOOL bValid = TRUE;
	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nStart, nEnd;

	CString strPrePath;
	CString strCurPath;

	//ESMLog(1,_T("Check 1 Start"));
	for(int i = 0; i < pArrFrameInfo->size(); i++)
	{
		CESMFileOperation fo;
		// 영상 check

		nStart = GetTickCount();

		strCurPath.Format(_T("%s"),pArrFrameInfo->at(i).strFramePath);
		if(strCurPath.CompareNoCase(strPrePath) == 0 && i != 0)
		{
			pArrFrameInfo->at(i).bValid = pArrFrameInfo->at(i-1).bValid;
		}
		else
		{
			strPrePath = strCurPath;
#if/ *ndef* / _CHECK_VALID_THREAD
			if(fo.IsFileExist(pArrFrameInfo->at(i).strFramePath))
			{
				CFile file;
				if (file.Open(pArrFrameInfo->at(i).strFramePath, CFile::modeRead))
				{
					if(file.GetLength() > 0)
						pArrFrameInfo->at(i).bValid = TRUE;
					else
						pArrFrameInfo->at(i).bValid = FALSE;

					file.Close();
				}
				else
					pArrFrameInfo->at(i).bValid = FALSE;
			}
			else
			{
				//ESMLog(1, _T("File not exist [%s]") , pArrFrameInfo->at(i).strFramePath);
				pArrFrameInfo->at(i).bValid = FALSE;
			}
#else
			if(ESMGetExecuteMode() == FALSE)
			{
				if(fo.IsFileExist(pArrFrameInfo->at(i).strFramePath))
				{
					CFile file;
					if (file.Open(pArrFrameInfo->at(i).strFramePath, CFile::modeRead))
					{
						if(file.GetLength() > 0)
							pArrFrameInfo->at(i).bValid = TRUE;
						else
							pArrFrameInfo->at(i).bValid = FALSE;

						file.Close();
					}
					else
						pArrFrameInfo->at(i).bValid = FALSE;
				}
				else
				{
					//ESMLog(1, _T("File not exist [%s]") , pArrFrameInfo->at(i).strFramePath);
					pArrFrameInfo->at(i).bValid = FALSE;
				}

			}else
			{
				if(ESMGetCheckFrame(pArrFrameInfo->at(i).strFramePath))
				{
					pArrFrameInfo->at(i).bValid = TRUE;
				}else
				{
					
					//if(fo.IsFileExist(pArrFrameInfo->at(i).strFramePath))
					//{
					//	CFile file;
					//	if (file.Open(pArrFrameInfo->at(i).strFramePath, CFile::modeRead))
					//	{
					//		if(file.GetLength() > 0)
					//			pArrFrameInfo->at(i).bValid = TRUE;
					//		else
					//			pArrFrameInfo->at(i).bValid = FALSE;

					//		file.Close();
					//	}
					//	else
					//		pArrFrameInfo->at(i).bValid = FALSE;
					//}
					//else
					//{
					//	//ESMLog(1, _T("File not exist [%s]") , pArrFrameInfo->at(i).strFramePath);
					//	pArrFrameInfo->at(i).bValid = FALSE;
					//}
					pArrFrameInfo->at(i).bValid = FALSE;
					ESMLog(0, _T("-----------------------CheckValidFrame[%d] : %s"), pArrFrameInfo->at(i).bValid, pArrFrameInfo->at(i).strFramePath);
				}
			}
			
#endif
		}

		
		nEnd = GetTickCount();
		if(nEnd - nStart > 1000)
		{
			ESMLog(1,_T("Movie Check Slow path = %s, time = %d ms"), pArrFrameInfo->at(i).strFramePath, nEnd - nStart);
		}

		// 연속성 유효 check
		BOOL bNext = FALSE;
		//if(i < pArrFrameInfo->size()-1)
		if(i == 0)
		{
			strDSC.Format(_T("%s"),pArrFrameInfo->at(i).strDSC_ID );
			strCheckDSC.Format(_T("%s"),pArrFrameInfo->at(i+1).strDSC_ID );
			bNext = TRUE;
		}
		else
		{
			strDSC.Format(_T("%s"),pArrFrameInfo->at(i).strDSC_ID );
			strCheckDSC.Format(_T("%s"),pArrFrameInfo->at(i-1).strDSC_ID );	
		}

		if(strDSC.CompareNoCase(strCheckDSC) == 0)
		{
			if(bNext)
			{
				if(pArrFrameInfo->at(i).nFrameIndex == pArrFrameInfo->at(i+1).nFrameIndex)
					pArrFrameInfo->at(i).bPlay = FALSE;
				else
					pArrFrameInfo->at(i).bPlay = TRUE;
			}
			else
			{
				pArrFrameInfo->at(i).bPlay = TRUE;
				if(pArrFrameInfo->at(i).nFrameIndex <= pArrFrameInfo->at(i-1).nFrameIndex)
				{
					CString strPath, strPrePath;
					strPath.Format(_T("%s"), pArrFrameInfo->at(i).strFramePath);
					strPrePath.Format(_T("%s"), pArrFrameInfo->at(i-1).strFramePath);
					if(strPath.CompareNoCase(strPrePath) == 0)
						pArrFrameInfo->at(i).bPlay = FALSE;
				}
				else
					pArrFrameInfo->at(i).bPlay = TRUE;
			}
			//ESMLog(5, _T("CHECK_FILE[%d] == [%d][%d] %s"), i, pArrFrameInfo->at(i).bPlay, pArrFrameInfo->at(i).nFrameIndex, pArrFrameInfo->at(i).strFramePath);
		}
		else
		{
			if(i > 0)
			{
				//CString strPreDSC;
				//strPreDSC.Format(_T("%s"),pArrFrameInfo->at(i-1).strDSC_ID );

				//if(strDSC.CompareNoCase(strPreDSC) == 0)
				if(strDSC.CompareNoCase(strCheckDSC) == 0)
				{
					if(pArrFrameInfo->at(i).nFrameIndex == pArrFrameInfo->at(i-1).nFrameIndex)
						pArrFrameInfo->at(i).bPlay = FALSE;
					else
						pArrFrameInfo->at(i).bPlay = TRUE;
				}
				else
					pArrFrameInfo->at(i).bPlay = FALSE;
			}
			else
				pArrFrameInfo->at(i).bPlay = FALSE;

			//ESMLog(5, _T("CHECK_FILE[%d] != [%d][%d] %s"), i, pArrFrameInfo->at(i).bPlay, pArrFrameInfo->at(i).nFrameIndex, pArrFrameInfo->at(i).strFramePath);
		}

		//Sensor Out Check
		//Tick Out Check
		
		for( int nIndex = 0; nIndex < arDSCList.GetSize(); nIndex++)
		{
			pItem = (CDSCItem*)arDSCList.GetAt(nIndex);
			if( pItem )
			{
				if(pItem->GetDeviceDSCID() == strDSC)
				{
					pArrFrameInfo->at(i).bSensorSync = pItem->GetSensorSync();
					pArrFrameInfo->at(i).bTickSync	 = pItem->GetTickStatus();
					break;
				}
			}
		}

		/ * //Sensor out check
		if(pItem)
		{
			if(pArrFrameInfo->at(i).bSensorSync == FALSE)
			{
				pArrFrameInfo->at(i).bValid = FALSE;
			}

			if(pArrFrameInfo->at(i).bSensorSync == FALSE && pArrFrameInfo->at(i).bPlay == FALSE)
			{
				ESMLog(5, _T("SensorSync Skip DSC[%s]"), pItem->GetDeviceDSCID());
				pArrFrameInfo->erase(pArrFrameInfo->begin() + i);
				i--;
			}
		}* /
	}

	
	

	//ESMLog(1,_T("Check 2 Start"));
	// 연속성 유효 check
	for(int i = 0; i < pArrFrameInfo->size(); i++)
	{
		bValid = TRUE;

		BOOL b1,b2,b3;
		b1 = pArrFrameInfo->at(i).bSensorSync;
		//b2 = pArrFrameInfo->at(i).bTickSync;
		b3 = pArrFrameInfo->at(i).bValid;


		//if(!pArrFrameInfo->at(i).bSensorSync || !pArrFrameInfo->at(i).bTickSync || !pArrFrameInfo->at(i).bValid)	
		//if(!b1 || !b2 || !b3)	
		if(!b1 || !b3)	
			bValid = FALSE;

		CString strLog;
		if(bValid == FALSE && pArrFrameInfo->at(i).bPlay == TRUE)
		{
			int nNextIndex = i;
			int nPreIndex = i;

			while(1)
			{
				if( nNextIndex < pArrFrameInfo->size()-1 )
				{
					if(pArrFrameInfo->at(nNextIndex).bValid == TRUE &&
						pArrFrameInfo->at(nNextIndex).bSensorSync == TRUE )
					{
						strLog = pArrFrameInfo->at(i).strFramePath;

						//ESMLog(1, _T("Change Frame Src[%s]") , pArrFrameInfo->at(i).strFramePath);
						
						strSrc.Format(_T("%s"), pArrFrameInfo->at(i).strFramePath);
						strSrc.Replace(pArrFrameInfo->at(i).strDSC_ID, pArrFrameInfo->at(nNextIndex).strDSC_ID);
						strSrc.Replace(pArrFrameInfo->at(i).strDSC_IP, pArrFrameInfo->at(nNextIndex).strDSC_IP);
						pArrFrameInfo->at(i).bSensorSync = TRUE;
						
						_stprintf(pArrFrameInfo->at(i).strFramePath, strSrc);//유니코드
						
						//ESMLog(1, _T("Change Frame Dst[%s]") , pArrFrameInfo->at(i).strFramePath);

						strLog.Format(_T("Change Frame Src [%s] -> Dst [%s]"), strLog, pArrFrameInfo->at(i).strFramePath);
						ESMLog(1, strLog);

						pArrFrameInfo->at(i).stEffectData = pArrFrameInfo->at(nNextIndex).stEffectData;
						break;
					}
					else
						nNextIndex++;
				}
				else
				{
					if(pArrFrameInfo->at(nPreIndex).bValid == TRUE &&
						pArrFrameInfo->at(nPreIndex).bSensorSync == TRUE )
					{
						strLog = pArrFrameInfo->at(i).strFramePath;

						//ESMLog(1, _T("Change Frame Src[%s]") , pArrFrameInfo->at(i).strFramePath);
						
						strSrc.Format(_T("%s"), pArrFrameInfo->at(i).strFramePath);
						strSrc.Replace(pArrFrameInfo->at(i).strDSC_ID, pArrFrameInfo->at(nPreIndex).strDSC_ID);
						strSrc.Replace(pArrFrameInfo->at(i).strDSC_IP, pArrFrameInfo->at(nPreIndex).strDSC_IP);
						pArrFrameInfo->at(i).bSensorSync = TRUE;

						_stprintf(pArrFrameInfo->at(i).strFramePath, strSrc);//유니코드
						
						//ESMLog(1, _T("Change Frame Dst[%s]") , pArrFrameInfo->at(i).strFramePath);

						strLog.Format(_T("Change Frame Src [%s] -> Dst [%s]"), strLog, pArrFrameInfo->at(i).strFramePath);
						ESMLog(1, strLog);

						pArrFrameInfo->at(i).stEffectData = pArrFrameInfo->at(nPreIndex).stEffectData;
						break;
					}
					else
						nPreIndex--;

					if(nPreIndex < 0)
						break;
				}
			}
		}
	}

	//Sensor out check
	for(int n = 0; n < pArrFrameInfo->size(); n++)
	{
		if(pArrFrameInfo->at(n).bSensorSync == FALSE)
		{
			pArrFrameInfo->at(n).bValid = FALSE;
		}

		if(pArrFrameInfo->at(n).bSensorSync == FALSE && pArrFrameInfo->at(n).bPlay == FALSE)
		{
			//ESMLog(5, _T("SensorSync Skip DSC[%s]"), pItem->GetDeviceDSCID());
			pArrFrameInfo->erase(pArrFrameInfo->begin() + n);
			n--;
		}
	}

	//jhhan 170418 Play 구간 DSCID 통일화 - CheckVaild에서 DSCID 변경에 따른 수정
	vector<int> arryCount;
	BOOL _bPlay = FALSE;
	for(int i = 0; i < pArrFrameInfo->size(); i++)
	{
//#if _DEBUG
//		ESMLog(5, _T("[ID_CHANGE_ALL_INFO] - %s"), pArrFrameInfo->at(i).strFramePath);
//#endif
		if(pArrFrameInfo->at(i).bPlay == TRUE)
		{
			arryCount.push_back(i);
		}else
		{
			if(arryCount.size() > 0)
			{
				CString strDscId = _T("");
				CString strDscIp = _T("");
				int nIdx = arryCount.size() - 1;
				//strDscId = ESMGetDSCIDFromPath(pArrFrameInfo->at(arryCount.at(nIdx)).strFramePath);
				strDscId = pArrFrameInfo->at(arryCount.at(nIdx)).strDSC_ID;
				strDscIp = pArrFrameInfo->at(arryCount.at(nIdx)).strDSC_IP;

				for(int n = 0; n < arryCount.size(); n++)
				{
					CString strPath = pArrFrameInfo->at(arryCount.at(n)).strFramePath;
					//CString strId = ESMGetDSCIDFromPath(strPath);
					CString strId = pArrFrameInfo->at(arryCount.at(n)).strDSC_ID;
					CString strIp = pArrFrameInfo->at(arryCount.at(n)).strDSC_IP;

					if(strDscId != strId)
					{
						strPath.Replace(strId, strDscId);
						strPath.Replace(strIp, strDscIp);

						_stprintf(pArrFrameInfo->at(arryCount.at(n)).strFramePath, strPath);

						ESMLog(1, _T("Change : [ID] %s -> %s, [IP] %s -> %s "), strId, strDscId, strIp, strDscIp);
					}
				}
				arryCount.clear();
			}
		}
		
	}

	ESMLog(1,_T("[CHECK_F]Check Valid End"));
	//Sleep(100);
}
*/

void CTimeLineView::MakeMovieFrameBaseMovie(vector<MakeFrameInfo>* pArrFrameInfo)
{
	ESMLog(5,_T("!!! Make Movie START !!!  %d "), GetTickCount());

	HANDLE handle;
	CMainFrame* pMainWnd = (CMainFrame*)m_pMainFrm;

	FrameThreadData* stThreadData = new FrameThreadData;
	stThreadData->pArrFrameInfo = pArrFrameInfo;
	stThreadData->pTimeLineView = this;

	handle = (HANDLE) _beginthread( _DoMakeMovieFrameBaseMovie, 0, (void*)stThreadData); // create thread
}

void CTimeLineView::MakeMovieFrameBaseAdjustMovie(vector<MakeFrameInfo>* pArrFrameInfo, ESMAdjustInfo info)
{
	ESMLog(5,_T("!!! Make Adjust Movie START !!!  %d "), GetTickCount());

	HANDLE handle;
	CMainFrame* pMainWnd = (CMainFrame*)m_pMainFrm;

	FrameThreadData* stThreadData = new FrameThreadData;
	stThreadData->pArrFrameInfo = pArrFrameInfo;
	stThreadData->pTimeLineView = this;
	stThreadData->info = info;

	handle = (HANDLE) _beginthread( _DoMakeMovieFrameBaseAdjustMovie, 0, (void*)stThreadData); // create thread
}

void _DoMakeMovieFrameBaseAdjustMovie(void *param)
{
	FrameThreadData* stThreadData = (FrameThreadData*)param;
	CTimeLineView * pTimeLineView = stThreadData->pTimeLineView;
	vector<MakeFrameInfo>* pArrFrameInfo = stThreadData->pArrFrameInfo;


	CMainFrame* pMainWnd = (CMainFrame*)pTimeLineView->m_pMainFrm;

	if(!pTimeLineView)
	{
		ESMLog(5, _T("FrameBaseMovie pTimeLineView NULL"));
		return;
	}

	if (!pArrFrameInfo)
	{
		ESMLog(5, _T("FrameBaseMovie m_pArrFrameInfo NULL"));
		ESMLog(5, _T(""));
		return;
	}

	int nFrameCount = pArrFrameInfo->size();
	int nSendCount	= 0,  nSendGourpCount = 0;
	vector<MakeFrameInfo>* pMovieInfo;
	MakeFrameInfo stMakeFrameInfo;


	pMovieInfo = new vector<MakeFrameInfo>;
	for ( int j = 1 ;j <= nFrameCount; j++)
	{
		stMakeFrameInfo = pArrFrameInfo->at(nSendCount++);
		pMovieInfo->push_back(stMakeFrameInfo);
	}

	if(pMovieInfo->size() > 0)
	{
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->pParam  = (LPARAM)pMovieInfo;
		pMsg->nParam2 = -1;
		pMsg->nParam3 = ESMGetReverseMovie();
		pMsg->pDest	  = (LPARAM)&stThreadData->info;
		pTimeLineView->MakeMovieFrameBaseAdjust(pMsg);
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
		}
			
		nSendGourpCount++;
	}

	if (pArrFrameInfo)
	{
		delete pArrFrameInfo;
		pArrFrameInfo = NULL;
	}
	pTimeLineView->m_nSendCount = nSendGourpCount;

	if(nFrameCount == 0)
		nFrameCount = nFrameCount;
	ESMLog(5,_T("!!! Sand Check  [%d/%d] "), nSendCount,nFrameCount);
	ESMLog(5,_T("[CHECK_R]!!! Frame Data Send Complete !!!  %d "), nFrameCount);
}

void CTimeLineView::GetIpList()
{

	m_IPList.clear();
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\PCList.csv"), ESMGetPath(ESM_PATH_SETUP));

	if(!ReadFile.Open(strInputData, CFile::modeRead))
		return;

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if(iLength == 0)
		return;

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength-1] = '\0';
	int len =0; 
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for ( int i =0 ;i < len; i++)
	{
		if(buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0, i = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	BOOL bOk[1];
	for (INT i = 0; i < 1; i ++) bOk[i] = FALSE;

	CString strNum, strDSCIP;

	while(1)
	{
		i++;
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if( sLine == _T(""))
			break;

		AfxExtractSubString(strDSCIP, sLine, 0, ',');
		strDSCIP.Trim();


		if(strDSCIP != _T(""))
		{
			strDSCIP.Replace( _T(" "), NULL );
			m_IPList.push_back(strDSCIP);
		}
		else
			break;
	}
}
void CTimeLineView::SetPCList()
{
	GetIpList();
	m_PCList.clear();
	CString ipStr;
	CString pcStr;
	int pcNum;
	for(int i = 0 ; i < m_IPList.size();i++)
	{
		ipStr = m_IPList[i];
		if(ipStr != "")
		{
			pcStr = ipStr.Right( 3 );
			pcStr.Trim('.');	//jhhan 170406 ip두자리의 경우 . 제거
			pcNum = _ttoi(pcStr);

			m_PCList.push_back(pcNum);
		}
		
	}
	
}

void CTimeLineView::GetIpAllList()
{
	CString strFile;
	m_IPAllList.clear();
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);	
	CESMIni ini;	
	
	if(ini.SetIniFilename (strFile))
	{
		CString strIPSection;
		CString strIP;
		int nIndex = 0;
		while(1)
		{
			strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);			
			strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);	
			if(!strIP.GetLength())
				break;		
			nIndex++;	
			m_IPAllList.push_back(strIP);
		}		
	}

	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_EX);	
	//CESMIni ini;	
	if(ini.SetIniFilename (strFile))
	{
		CString strIPSection;
		CString strIP;
		int nIndex = 0;
		CString strRemote;
		while(1)
		{
			strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);			
			strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);	
			if(!strIP.GetLength())
				break;

//			strRemote = ini.GetSectionCount(strIP, _T("Remote"), _T("N"));
//			if(strRemote.CompareNoCase(_T("N")) == 0)
			{
				nIndex++;	
				m_IPAllList.push_back(strIP);
			}
			
		}		
	}
}
void CTimeLineView::SetPCAllList()
{
	GetIpAllList();
	m_PCAllList.clear();
	CString ipStr;
	CString pcStr;
	int pcNum;
	for(int i = 0 ; i < m_IPAllList.size();i++)
	{
		ipStr = m_IPAllList[i];
		if(ipStr != "")
		{
			pcStr = ipStr.Right( 3 );
			pcStr.Trim('.');	//jhhan 170406 ip두자리의 경우 . 제거
			pcNum = _ttoi(pcStr);

			m_PCAllList.push_back(pcNum);
		}
	}
}
void CTimeLineView::SetNoMakingList()
{
	SetPCList();
	SetPCAllList();
	m_NoMakingList.clear();
	int ipNum;
	int pcNum;
	BOOL m_exist = false;
	for(int i = 0 ; i<m_PCAllList.size();i++)
	{
		m_exist = false;
		ipNum = m_PCAllList[i];
		if(ipNum != NULL)
		{
			for(int j = 0 ; j < m_PCList.size();j++)
			{
				pcNum = m_PCList[j];
				if(pcNum != NULL)
				{
					if(ipNum == pcNum)
					{
						m_exist = TRUE;
						break;
					}
				}
			}

			if(m_exist)
			{
				m_exist = false;
			}
			else
			{
				m_NoMakingList.push_back(ipNum);
			}
		}

	}
}

void _DoMakeMovieFrameBaseMovie(void *param)
{
	FrameThreadData* stThreadData = (FrameThreadData*)param;
	CTimeLineView * pTimeLineView = stThreadData->pTimeLineView;
	vector<MakeFrameInfo>* pArrFrameInfo = stThreadData->pArrFrameInfo;
	CMainFrame* pMainWnd = (CMainFrame*)pTimeLineView->m_pMainFrm;
	CESMArray arDSCGroup;
	CESMArray arPlayDSCGroup;

#if 1
		
	MAKE_GROUP_FRAME group_info;
	group_info.bPlayMaking			= FALSE;
	group_info.arDSCGroup			= &arDSCGroup;
	group_info.arPlayDSCGroup		= &arPlayDSCGroup;
	group_info.nPlay				= 0;
	group_info.pGroup				= NULL;
	group_info.nTotalFrameCount		= pArrFrameInfo->size();
	group_info.nPlayFrameCount		= 0;
	group_info.nDivision			= 0;
	group_info.nCPUMakeNet			= 0;
	group_info.nCPUMakeCount		= 0;
	group_info.bFirst				= FALSE;
	group_info.nRemainder			= 0;
	group_info.nPlayDivision		= 0;
	group_info.strTime				= pArrFrameInfo->at(0).strTime;
	group_info.nNetMgrCount			= 0;
	group_info.nPlayMgrCount		= 0;
	group_info.nPlaynet				= 0;
	group_info.nAJASendCnt			= 0;
	group_info.nAJARotFrameIdx		= 0;

	if(pTimeLineView->PlayNetworkCheck(group_info, pArrFrameInfo))
	{
		pTimeLineView->PlayCountCheck(group_info, pArrFrameInfo);
		if(pTimeLineView->CalculateDivision(group_info))
		{
			pTimeLineView->MakingGroupFrame(group_info, pArrFrameInfo);
			//pTimeLineView->StartMuxingView();
			//AJA Thread 시작
			pTimeLineView->StartAJAThreadInit(group_info);
		}
	}
	
#else
	int nFrameCount = pArrFrameInfo->size();
	int nDivision	= nFrameCount/nNetMgrCount;
	int nRemainder	= nFrameCount%nNetMgrCount;
	BOOL bFirst = FALSE;


	//-- 0.mp4 파일이 10프레임 이하인경우 error
	{
		if(nDivision < MIN_FIRST_FRAME)
		{
			bFirst = TRUE;
			nDivision = (nFrameCount-11)/(nNetMgrCount-1);
			nRemainder = nFrameCount - ( MIN_FIRST_FRAME + ((nNetMgrCount-1) * nDivision));
		}
	}

	int nSendCount	= 0,  nSendGourpCount = 0;
	vector<MakeFrameInfo>* pMovieInfo;
	vector<MakeFrameInfo> _pMovieInfo;
	MakeFrameInfo stMakeFrameInfo;

	for ( int i = 0 ; i<nNetMgrCount ; i++)
	{
 		pMovieInfo = new vector<MakeFrameInfo>;

		if(bFirst == TRUE && i == 0)
		{
			for ( int j = 1 ;j <= MIN_FIRST_FRAME; j++)
			{
				stMakeFrameInfo = pArrFrameInfo->at(nSendCount++);
				pMovieInfo->push_back(stMakeFrameInfo);
				_pMovieInfo.push_back(stMakeFrameInfo);
			}
		}
		else
		{	
			for ( int j = 1 ;j <= nDivision; j++)
			{
				stMakeFrameInfo = pArrFrameInfo->at(nSendCount++);
				pMovieInfo->push_back(stMakeFrameInfo);
				_pMovieInfo.push_back(stMakeFrameInfo);
			}

			if (i<nRemainder)
			{
				stMakeFrameInfo = pArrFrameInfo->at(nSendCount++);
				pMovieInfo->push_back(stMakeFrameInfo);
				_pMovieInfo.push_back(stMakeFrameInfo);
			}
		}


		
		//Client 에 전송해서 해당 영상으로 만들라고 하면 끝.
 		if(pMovieInfo->size() > 0)
 		{
 			if (i < arDSCGroup.GetCount())
 			{
 				pGroup = (CDSCGroup*)arDSCGroup.GetAt(i); 

				movieInfo _movieInfo;
				_movieInfo.pGroup = pGroup;
				_movieInfo.fileNum = i;
				_movieInfo.m_MovieInfo = _pMovieInfo;
				_movieInfo.makingFin = FALSE;
				_movieInfo.serverFlag = FALSE;
				pTimeLineView->m_movieMakingList.push_back(_movieInfo);

 				pGroup->DoMovieMakeFrameMovie(pMovieInfo, i);
 			}
			else
			{	
				if(!ESMGetValue(ESM_VALUE_CEREMONYUSE) && ESMGetValue(ESM_VALUE_MAKE_SERVER))
				{
					ESMEvent* pMsg	= new ESMEvent;
					pMsg->pParam  = (LPARAM)pMovieInfo;
					pMsg->nParam2 = i;
					pMsg->nParam3 = ESMGetValue(ESM_VALUE_SAVE_IMAGE);
					
					CString strImgPath = ESMGetSaveImagePath();
					CString* pStr = new CString;
					pStr->Format(_T("%s"),strImgPath);
					pMsg->pDest		= (LPARAM)pStr;

					CString str = ESMGetLocalIP();

					CString localStr;
					AfxExtractSubString( localStr, str, 3, '.');

					int m_Ip = _ttoi(localStr);

					CDSCGroup* m_pGroup = new CDSCGroup();
					m_pGroup->m_nIP = m_Ip;

					movieInfo _movieInfo;
					_movieInfo.pGroup = m_pGroup;
					_movieInfo.fileNum = i;
					_movieInfo.m_MovieInfo = _pMovieInfo;
					_movieInfo.makingFin = FALSE;
					_movieInfo.serverFlag = TRUE;
					pTimeLineView->m_movieMakingList.push_back(_movieInfo);

					pTimeLineView->MakeMovieFrameBase(pMsg);



					if(pMsg)
					{
						delete pMsg;
						pMsg = NULL;
					}
				}
			}
			nSendGourpCount++;
		}
 	}
 
 	if (pArrFrameInfo)
 	{
 		delete pArrFrameInfo;
 		pArrFrameInfo = NULL;
 	}
 	pTimeLineView->m_nSendCount = nSendGourpCount;
 
	if(nFrameCount == 0)
		nFrameCount = nFrameCount;
 	ESMLog(5,_T("!!! Sand Check  [%d/%d] "), nSendCount,nFrameCount);
 	ESMLog(5,_T("[CHECK_R]!!! Frame Data Send Complete !!!  %d "), nFrameCount);
#endif
}
BOOL CTimeLineView::PlayNetworkCheck(MAKE_GROUP_FRAME& info, vector<MakeFrameInfo>* pArrFrameInfo)
{
	CMainFrame* pMainWnd = (CMainFrame*)m_pMainFrm;

	//전체 메이킹 카운트 입력
	if(pArrFrameInfo->size() < MIN_FIRST_FRAME)
	{
		//ESMLog(0,_T("Array Size: %d"),pArrFrameInfo->size());
		CString strErr;
		strErr.Format(_T("_DoMakeMovieFrameBaseMovie\nSet Making Size 11 frame at least\nArray Size: %d"),pArrFrameInfo->size());
		ESMLog(0,strErr);
		AfxMessageBox(strErr);
		map_MakingData[info.strTime].nSendCount = 0;
		return FALSE;
	}

	if (!pArrFrameInfo)
	{
		ESMLog(5, _T("FrameBaseMovie m_pArrFrameInfo NULL"));
		ESMLog(5, _T(""));
		return FALSE;
	}

	CESMRCManager* pRCMgr = NULL;
	
	//CDSCGroup* pFirstSendGroup = NULL;

	int nRepeatCount	= 0;
	int nIsRcMgrCount	= 0;
	int nConnectedCount = 0;
	int nNet4DP			= 0;

	info.nNetMgrCount = pMainWnd->m_wndNetwork.m_arRCServerList.GetCount();
	for ( int i = 0 ;i < info.nNetMgrCount; i++)
	{
		pRCMgr = (CESMRCManager *)pMainWnd->m_wndNetwork.m_arRCServerList.GetAt(i);
		pRCMgr->m_bCheckConnect = FALSE;
		if (pRCMgr->m_bConnected)
		{
			if( pRCMgr->GetServerSocket())
			{
				//pRCMgr->ConnectCheck(ESMGetFrameRate());	//180308 카메라타입 및 해상도 타입 동시전송
				int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
				pRCMgr->ConnectCheck(ESMGetIdentify() , nSelectedTime);			//jhhan 180822

				//ESMLog(5,_T("Send Agent Check [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());
				TRACE(_T("Send Agent Check [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());

				nIsRcMgrCount++;

				//GPU 사용가능 PC 카운트
				//if(pRCMgr->m_bGPUUse)
				//	nPlayMgrCount++;
			}else
			{
				//ESMLog(5, _T("GetServerSocket Fail [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());
			}
		}
	}

	//170831 hjcho - Direct Mux 시 GPU Count 세지않음
	if(ESMGetValue(ESM_VALUE_DIRECTMUX) == FALSE || ESMGetRemoteSkip() == TRUE
		/*ESMGetValue(ESM_VALUE_GPUMAKESKIP) == TRUE && */)
	{
		nNet4DP = pMainWnd->m_wndNetworkEx.m_arRCServerList.GetCount();
		for ( int i = 0 ;i < nNet4DP; i++)
		{
			pRCMgr = (CESMRCManager *)pMainWnd->m_wndNetworkEx.m_arRCServerList.GetAt(i);
			pRCMgr->m_bCheckConnect = FALSE;
			CString strSendDSC = pRCMgr->GetSourceDSC();

			if (pRCMgr->m_bConnected && strSendDSC.GetLength() < 5)
			{
				if( pRCMgr->GetServerSocket())
				{
					//pRCMgr->ConnectCheck(ESMGetFrameRate());	//180308 카메라타입 및 해상도 타입 동시전송
					pRCMgr->ConnectCheck(ESMGetIdentify());
					//ESMLog(5,_T("Send Agent Check [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());
					TRACE(_T("Send Agent Check [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());

					nIsRcMgrCount++;

					//GPU 사용가능 PC 카운트
					//if(pRCMgr->m_bGPUUse)
					//	nPlayMgrCount++;
				}else
				{
					//ESMLog(5, _T("GetServerSocket Fail [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());
				}
			}
		}
	}

	SetNoMakingList();
	BOOL NoMakingFlg = TRUE;
	int pcNum = 0;
	int nMakePc = 0;	//jhhan 170406 MakingPC Count Log 추가
#if 1   //Agent Check Repeat 200
	while(nRepeatCount++ <= 200)
#else
	while(nRepeatCount++ <= 10)
#endif  //Agent Check Repeat Skip
	{	
		for ( int i = 0 ;i < info.nNetMgrCount; i++)
		{
			pRCMgr = (CESMRCManager *)pMainWnd->m_wndNetwork.m_arRCServerList.GetAt(i);
			//Agent Connect Check
			if (pRCMgr->m_bConnected)
			{
				if (pRCMgr->m_bCheckConnect)
				{
					//ESMLog(5,_T("Receive Agent Connect List add [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());
					TRACE(_T("Receive Agent Connect List add [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());
					pRCMgr->m_bCheckConnect = FALSE;

					info.pGroup = new CDSCGroup(pRCMgr->GetID());
					info.pGroup->SetRCMgr(pRCMgr);

					if(m_NoMakingList.size() != 0 )
					{
						for(int j = 0 ; j <m_NoMakingList.size() ; j++)
						{
							pcNum = m_NoMakingList[j];
							if(pcNum != NULL)
							{
								if(pcNum == info.pGroup->m_nIP)
								{
									NoMakingFlg = FALSE;
								}
							}
						}
					}

					if(NoMakingFlg)
					{
						if(ESMGetValue(ESM_VALUE_RTSP) == FALSE)
						{
							if(info.pGroup->GetRCMgr()->m_bGPUUse)	
							{
								info.nPlayMgrCount++;
								info.arPlayDSCGroup->Add((CObject*)info.pGroup);
							}
						}
						/*else
							info.arDSCGroup->Add((CObject*)info.pGroup);*/

						nMakePc++;
						//jhhan 170322
						ESMLog(5, _T("[4DA] _DoMakeMovieFrameBaseMovie : [ %d ] , GPU : [%d] "), info.pGroup->m_nIP, info.pGroup->GetRCMgr()->m_bGPUUse);
					}	

					NoMakingFlg = TRUE;
					nConnectedCount++;
				}
				else
				{
					//ESMLog(5, _T("m_bCheckConnect Fail [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());
				}
			}
		}

		for ( int i = 0 ;i < nNet4DP; i++)
		{
			pRCMgr = (CESMRCManager *)pMainWnd->m_wndNetworkEx.m_arRCServerList.GetAt(i);
			pRCMgr->m_bGPUUse;
			//Agent Connect Check
			if (pRCMgr->m_bConnected)
			{
				if (pRCMgr->m_bCheckConnect)
				{
					//ESMLog(5,_T("Receive Agent Connect List add [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());
					TRACE(_T("Receive Agent Connect List add [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());
					pRCMgr->m_bCheckConnect = FALSE;

					info.pGroup = new CDSCGroup(pRCMgr->GetID());
					info.pGroup->SetRCMgr(pRCMgr);

					if(m_NoMakingList.size() != 0 )
					{
						for(int j = 0 ; j <m_NoMakingList.size() ; j++)
						{
							pcNum = m_NoMakingList[j];
							if(pcNum != NULL)
							{
								if(pcNum == info.pGroup->m_nIP)
								{
									NoMakingFlg = FALSE;
								}
							}
						}
					}

					if(NoMakingFlg)
					{

						if(info.pGroup->GetRCMgr()->m_bGPUUse)
						{
							info.nPlayMgrCount++;
							info.arPlayDSCGroup->Add((CObject*)info.pGroup);
						}
						else
							info.arDSCGroup->Add((CObject*)info.pGroup);	

						nMakePc++;
						//jhhan 170322
						ESMLog(5, _T("[4DP] _DoMakeMovieFrameBaseMovie : [ %d ] , GPU : [%d] "), info.pGroup->m_nIP, info.pGroup->GetRCMgr()->m_bGPUUse);
					}	

					NoMakingFlg = TRUE;
					nConnectedCount++;
				}
				else
				{
					//ESMLog(5, _T("m_bCheckConnect Fail [IP:%d][Time:%d]"), pRCMgr->GetID(), ESMGetTick());
				}
			}
		}



#if 1   //Agent Check Repeat 200
		if (nIsRcMgrCount == nConnectedCount)		
			break;

		Sleep(10);
#endif  //Agent Check Repeat Skip
	}

	int nCheckCount;// = arDSCGroup.GetCount();
	if(ESMGetValue(ESM_VALUE_GPUMAKESKIP))
		nCheckCount = info.arDSCGroup->GetCount() + info.arPlayDSCGroup->GetCount();
	else
		nCheckCount =info.arDSCGroup->GetCount();

	ESMLog(5,_T("[CHECK_K]Agent Connect Check  [%d/%d] - MakingPC [%d] - Repeat Count[%d]"), 
		nConnectedCount,info.nNetMgrCount+nNet4DP, nMakePc, nRepeatCount);
	// 서버도 Agent 포함시킬시 + 1

	//if(ESMGetValue(ESM_VALUE_CEREMONYUSE) || !ESMGetValue(ESM_VALUE_MAKE_SERVER))
	if(!ESMGetValue(ESM_VALUE_MAKE_SERVER))
		info.nNetMgrCount = nCheckCount;
	else
		info.nNetMgrCount = nCheckCount+1;

	//전체 메이킹 카운트 입력
	//CString strTime = pArrFrameInfo->at(0).strTime;
	//pTimeLineView->map_MakingData[strTime].nSendCount = nNetMgrCount;

	//CMiLRe 20160113 서버에서 Make 하지 않을 경우의 버그
	if(info.nNetMgrCount == 0) //jhhan 190322
	{
		if(info.arPlayDSCGroup->GetCount() == 0)
		{
			AfxMessageBox(_T("사용 client가 없습니다.(4D Maker Options->Movie Make Server 옵션 확인 바랍니다."));
			return FALSE;
		}
	}
	

	return TRUE;
}
void CTimeLineView::PlayCountCheck(MAKE_GROUP_FRAME& group_info, vector<MakeFrameInfo>* pArrFrameInfo)
{
	BOOL bGPUMaking = FALSE;
	BOOL bCheck = FALSE;
	BOOL bAJANonMaking = FALSE;
	//연속 프레임 체크
	int nCount = 0;
	CString strFile;

	//jhhan 180307
	int _nFrameCount;
	if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork() 
		&& ESMGetAJAScreen() == FALSE && ESMGetAJAMaking() == FALSE && ESMGetAJADivProcess() == TRUE)
	{
		_nFrameCount = movie_frame_per_second;
		m_bAJAMaking = TRUE;
		bAJANonMaking = TRUE;
	}
	else
	{
		_nFrameCount = ESMGetDivFrame();
		m_bAJAMaking = FALSE;
	}

	//jhhan 170323
	making_info* info = NULL;
	CString strFirst, strEnd;
	//joonho.kim 임시 주석처리
	/*if(!ESMGetValue(ESM_VALUE_MOVIESAVELOCATION))//0:On, 1:Off
	{
		info = ESMGetMakingInfo();

		strFirst = (LPCSTR)info->strFirstDSC;
		strEnd = (LPCSTR)info->strEndDSC;

		if(arPlayDSCGroup.GetCount() == info->nProcessorCnt)	//접속4DP : info, 현재4DP : arPlayGroup
		{
			_nFrameCount = 30;				//4DP : 30Fps 처리
		}else
		{
			ESMLog(0, _T("MakingInfo [%d] and arPlayDSGGrop [%d] are different."),arPlayDSCGroup.GetCount(), info->nProcessorCnt);
		}
	}*/

	BOOL bCheckColor = FALSE;
	FrameRGBInfo ColorInfo;
	//170330 hjcho for Color Revision
	if(ESMGetValue(ESM_VALUE_COLORREVISION))
	{
		ColorInfo = DecodeImage(pArrFrameInfo);

		if(ColorInfo.dBlue > -1)
			bCheckColor = TRUE;
	}
	if(ESMGetValue(ESM_VALUE_RTSP))
	{
		SaveDecodeImage(pArrFrameInfo);
	}
#ifndef _ORIGINAL_4DA
	//ESMLog(5, _T("DIV : %d"), pArrFrameInfo->size());

	for(int n = 0; n < pArrFrameInfo->size(); n++)
	{
		if(ESMGetValue(ESM_VALUE_COLORREVISION) && bCheckColor)
			pArrFrameInfo->at(n).stColorInfo = ColorInfo;

		pArrFrameInfo->at(n).bAJAMaking = bAJANonMaking;

		//ESMLog(5, _T("FILE[%d] - [%d][%d] %s"), n, pArrFrameInfo->at(n).bPlay, pArrFrameInfo->at(n).nFrameIndex, pArrFrameInfo->at(n).strFramePath);
		if(n == 0)
			strFile = pArrFrameInfo->at(n).strFramePath;

		if(pArrFrameInfo->at(n).bPlay)
		{
			if(strFile.CompareNoCase(pArrFrameInfo->at(n).strFramePath) != 0 && group_info.nPlay != 0 && group_info.nPlay <= _nFrameCount/*15*/)
			{
				group_info.arryPlayCount.push_back(group_info.nPlay);
				group_info.nPlay = 0;
				bCheck = FALSE;
			}
			else if( strFile.CompareNoCase(pArrFrameInfo->at(n).strFramePath) == 0)
			{
				if(n > 1)
				{
					if(pArrFrameInfo->at(n).nFrameIndex < pArrFrameInfo->at(n-1).nFrameIndex || group_info.nPlay == _nFrameCount/*15*/)
					{
						group_info.arryPlayCount.push_back(group_info.nPlay);
						group_info.nPlay = 0;
						bCheck = FALSE;
					}
				}
			}
			//ESMLog(5, _T("bCheck[%d] - %s : %s"), bCheck, strFile, pArrFrameInfo->at(n).strFramePath);



			group_info.nPlay++;
			group_info.nPlayFrameCount++; //연속프레임 count 증가

			if(nCount > 0)
			{
				group_info.arryCount.push_back(nCount);
				nCount = 0;
			}

			if(!bCheck)
			{
				group_info.nPlaynet++;
				bCheck = TRUE;
			}

			strFile = pArrFrameInfo->at(n).strFramePath;
		}
		else
		{
			nCount++;

			if(group_info.nPlay > 0 && group_info.nPlay <= _nFrameCount/*15*/)
			{
				group_info.arryPlayCount.push_back(group_info.nPlay);
				group_info.nPlay = 0;
			}

			bCheck = FALSE;

			strFile = pArrFrameInfo->at(n).strFramePath;
		}
	}

	if(group_info.nPlay > 0)
	{
		group_info.arryPlayCount.push_back(group_info.nPlay);
		group_info.nPlay = 0;
	}

	if(nCount > 0)
	{
		group_info.arryCount.push_back(nCount);
		nCount = 0;
	}

#else
	for(int n = 0; n < pArrFrameInfo->size(); n++)
	{
		ESMLog(5, _T("FILE[%d] - [%d] %s"), n, pArrFrameInfo->at(n).nFrameIndex, pArrFrameInfo->at(n).strFramePath);
		if(n == 0)
			strFile = pArrFrameInfo->at(n).strFramePath;

		if(pArrFrameInfo->at(n).bPlay)
		{
			if(strFile.CompareNoCase(pArrFrameInfo->at(n).strFramePath) != 0 && nPlay != 0 && nPlay <= _FrameCount)
			{
				arryPlayCount.push_back(nPlay);
				nPlay = 0;
				bCheck = FALSE;
			}
			else if( strFile.CompareNoCase(pArrFrameInfo->at(n).strFramePath) == 0)
			{
				if(n > 1)
				{
					if(pArrFrameInfo->at(n).nFrameIndex < pArrFrameInfo->at(n-1).nFrameIndex || nPlay == _FrameCount)
					{
						arryPlayCount.push_back(nPlay);
						nPlay = 0;
						bCheck = FALSE;
					}
				}
			}
			//ESMLog(5, _T("bCheck[%d] - %s : %s"), bCheck, strFile, pArrFrameInfo->at(n).strFramePath);



			nPlay++;
			nPlayFrameCount++; //연속프레임 count 증가

			if(nCount > 0)
			{
				arryCount.push_back(nCount);
				nCount = 0;
			}

			if(!bCheck)
			{
				nPlaynet++;
				bCheck = TRUE;
			}

			strFile = pArrFrameInfo->at(n).strFramePath;
		}
		else
		{
			nCount++;

			if(nPlay > 0 && nPlay <= _FrameCount)
			{
				arryPlayCount.push_back(nPlay);
				nPlay = 0;
			}

			bCheck = FALSE;

			strFile = pArrFrameInfo->at(n).strFramePath;
		}
	}

	if(nPlay > 0)
	{
		arryPlayCount.push_back(nPlay);
		nPlay = 0;
	}

	if(nCount > 0)
	{
		arryCount.push_back(nCount);
		nCount = 0;
	}
#endif

	//Log
	CString strLog = _T("##Play Count : ");
	strLog.Format(_T("##Play Count[%d] : "), group_info.arryPlayCount.size());
	CString strTemp;
	for(int n = 0; n < group_info.arryPlayCount.size(); n++)
	{
		strTemp.Format(_T("%d "), group_info.arryPlayCount.at(n));
		strLog.Append(strTemp);
	}
	ESMLog(1, strLog);

	strLog = _T("##NonPlay Count : ");
	strLog.Format(_T("##NonPlay Count[%d] : "), group_info.arryCount.size());
	for(int n = 0; n < group_info.arryCount.size(); n++)
	{
		strTemp.Format(_T("%d "), group_info.arryCount.at(n));
		strLog.Append(strTemp);
	}
	ESMLog(1, strLog);

	strLog.Format(_T("##Divide Frame : %d"), _nFrameCount);
	ESMLog(1, strLog);
}
BOOL CTimeLineView::CalculateDivision(MAKE_GROUP_FRAME& info)
{
	if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork() 
		&& ESMGetAJAScreen() == FALSE && ESMGetAJAMaking() == FALSE && ESMGetAJADivProcess() == TRUE)
	{
		if(ESMGetValue(ESM_VALUE_GPUMAKESKIP) /*&& !ESMGetValue(ESM_VALUE_DIRECTMUX)*/)
		{
			for(int i = 0; i < info.arPlayDSCGroup->GetCount(); i++)
				info.arDSCGroup->Add(info.arPlayDSCGroup->GetAt(i));
		}

		if(info.nNetMgrCount > 1)
			info.nNetMgrCount = info.arDSCGroup->GetCount();

		if(ESMGetValue(ESM_VALUE_MAKE_SERVER) == TRUE && info.nNetMgrCount == 1 && info.arDSCGroup->GetCount() > 0)		//서버메이킹 ON & 4DA,4DP 접속시
			info.nNetMgrCount += info.arDSCGroup->GetCount();

		if(ESMGetValue(ESM_VALUE_MAKE_SERVER) == FALSE && info.nNetMgrCount == 0 && info.arDSCGroup->GetCount() > 0)		//서버메이킹 OFF & 4DA,4DP 접속시
			info.nNetMgrCount += info.arDSCGroup->GetCount();

		if(ESMGetValue(ESM_VALUE_MAKE_SERVER) == FALSE && info.nNetMgrCount == 0 && info.arDSCGroup->GetCount() > 0)		//서버메이킹 OFF & 4DA,4DP Direct Mux 시
			info.nNetMgrCount += (info.arDSCGroup->GetCount() + info.arPlayDSCGroup->GetCount());

		if(info.nNetMgrCount == 0)
		{	
			AfxMessageBox(_T("Agent Connect Error!"));
			return FALSE;
		}

		info.nDivision	= info.nTotalFrameCount/info.nNetMgrCount;
		info.nRemainder	= info.nTotalFrameCount%(info.nNetMgrCount*info.nDivision);

		if(info.nDivision < MIN_FIRST_FRAME)
		{
			info.bFirst = TRUE;
			info.nDivision = (info.nTotalFrameCount-11)/(info.nNetMgrCount-1);
			info.nRemainder = info.nTotalFrameCount - ( MIN_FIRST_FRAME + ((info.nNetMgrCount-1) * info.nDivision));
		}

		info.bPlayMaking = TRUE;
	}
	else if(info.nPlayMgrCount >= info.nPlaynet && info.nPlaynet > 0
		&& ESMGetValue(ESM_VALUE_MAKE_SERVER) == FALSE 
		&& !ESMGetValue(ESM_VALUE_GPUMAKESKIP))
	{
		info.nCPUMakeNet = info.nNetMgrCount/*+(nPlayMgrCount-nPlaynet)*/;

		//if(_nFrameCount != 30)  //15프레임 구분 일시 기존 로직 수행
		//{
		//	nCPUMakeCount += (nPlayMgrCount-nPlaynet);
		//}

		if(info.nCPUMakeNet != 0) //jhhan 190322
		{	
			//AfxMessageBox(_T("Agent Connect Error!"));
			//return FALSE;

			info.nCPUMakeCount = info.nTotalFrameCount-info.nPlayFrameCount;

			//jhhan 170303
			/*if(arryCount.size() > 1)
			{
			for(int i=1; i<arryCount.size(); i++)
			{
			if(abs(arryCount.at(i-1) - arryCount.at(i)) > MIN_FIRST_FRAME)
			{
			if(nCPUMakeCount - arryCount.at(i-1) < MIN_FIRST_FRAME)
			{
			nCPUMakeNet--;
			nCPUMakeCount -= arryCount.at(i);
			}
			}
			}
			}*/

			if(info.nCPUMakeCount > 0 && info.nCPUMakeNet > 0)
			{
				info.nDivision = info.nCPUMakeCount/info.nCPUMakeNet;
				if(info.nCPUMakeCount % info.nCPUMakeNet != 0)
					info.nDivision++;
			}

			ESMLog(5, _T("nDivision[%d] = nCPUMakeCount[%d] / nCPUMakeNet[%d]"), info.nDivision, info.nCPUMakeCount, info.nCPUMakeNet);

			info.bPlayMaking = TRUE;
		}
		else
		{
			if(ESMGetValue(ESM_VALUE_GPUMAKESKIP) /*&& !ESMGetValue(ESM_VALUE_DIRECTMUX)*/)
			{
				for(int i = 0; i < info.arPlayDSCGroup->GetCount(); i++)
					info.arDSCGroup->Add(info.arPlayDSCGroup->GetAt(i));
			}

			if(info.nNetMgrCount > 1)
				info.nNetMgrCount = info.arDSCGroup->GetCount();

			if(ESMGetValue(ESM_VALUE_MAKE_SERVER) == TRUE && info.nNetMgrCount == 1 && info.arDSCGroup->GetCount() > 0)		//서버메이킹 ON & 4DA,4DP 접속시
				info.nNetMgrCount += info.arDSCGroup->GetCount();

			if(ESMGetValue(ESM_VALUE_MAKE_SERVER) == FALSE && info.nNetMgrCount == 0 && info.arDSCGroup->GetCount() > 0)		//서버메이킹 OFF & 4DA,4DP 접속시
				info.nNetMgrCount += info.arDSCGroup->GetCount();

			if(ESMGetValue(ESM_VALUE_MAKE_SERVER) == FALSE && info.nNetMgrCount == 0 && info.arPlayDSCGroup->GetCount() > 0)		//서버메이킹 OFF & 4DA,4DP Direct Mux 시
				info.nNetMgrCount += (info.arDSCGroup->GetCount() + info.arPlayDSCGroup->GetCount());

			if(info.nNetMgrCount == 0)
			{	
				AfxMessageBox(_T("Agent Connect Error!"));
				return FALSE;
			}

			info.nDivision	= info.nTotalFrameCount/info.nNetMgrCount;
			info.nRemainder	= info.nTotalFrameCount%(info.nNetMgrCount*info.nDivision);

			if(info.nDivision < MIN_FIRST_FRAME)
			{
				info.bFirst = TRUE;
				info.nDivision = (info.nTotalFrameCount-11)/(info.nNetMgrCount-1);
				info.nRemainder = info.nTotalFrameCount - ( MIN_FIRST_FRAME + ((info.nNetMgrCount-1) * info.nDivision));
			}
		}
		

		
	}
	else
	{
		if(ESMGetValue(ESM_VALUE_GPUMAKESKIP) /*&& !ESMGetValue(ESM_VALUE_DIRECTMUX)*/)
		{
			for(int i = 0; i < info.arPlayDSCGroup->GetCount(); i++)
				info.arDSCGroup->Add(info.arPlayDSCGroup->GetAt(i));
		}

		if(info.nNetMgrCount > 1)
			info.nNetMgrCount = info.arDSCGroup->GetCount();
		
		if(ESMGetValue(ESM_VALUE_MAKE_SERVER) == TRUE && info.nNetMgrCount == 1 && info.arDSCGroup->GetCount() > 0)		//서버메이킹 ON & 4DA,4DP 접속시
			info.nNetMgrCount += info.arDSCGroup->GetCount();

		if(ESMGetValue(ESM_VALUE_MAKE_SERVER) == FALSE && info.nNetMgrCount == 0 && info.arDSCGroup->GetCount() > 0)		//서버메이킹 OFF & 4DA,4DP 접속시
			info.nNetMgrCount += info.arDSCGroup->GetCount();

		//jhhan 190322
		if(ESMGetValue(ESM_VALUE_MAKE_SERVER) == FALSE && info.nNetMgrCount == 0 && info.arPlayDSCGroup->GetCount() > 0)		//서버메이킹 OFF & 4DA,4DP Direct Mux 시
			info.nNetMgrCount += (info.arDSCGroup->GetCount() + info.arPlayDSCGroup->GetCount());

		if(info.nNetMgrCount == 0)
		{	
			AfxMessageBox(_T("Agent Connect Error!"));
			return FALSE;
		}

		info.nDivision	= info.nTotalFrameCount/info.nNetMgrCount;
		info.nRemainder	= info.nTotalFrameCount%(info.nNetMgrCount*info.nDivision);

		if(info.nDivision < MIN_FIRST_FRAME)
		{
			info.bFirst = TRUE;
			info.nDivision = (info.nTotalFrameCount-11)/(info.nNetMgrCount-1);
			info.nRemainder = info.nTotalFrameCount - ( MIN_FIRST_FRAME + ((info.nNetMgrCount-1) * info.nDivision));
		}
	}

	return TRUE;
}
void CTimeLineView::MakingGroupFrame(MAKE_GROUP_FRAME& info, vector<MakeFrameInfo>* pArrFrameInfo)
{
	ESMLog(5, _T("Continuous frames generated"));
#if 1
	vector<CString> m_arrPath;
	int nSendCount	= 0;
	int nSendGourpCount = 0;
	vector<MakeFrameInfo>* pMovieInfo = NULL;
	vector<MakeFrameInfo>* pPlayMovieInfo = NULL;
	vector<MakeFrameInfo>* pSendPlayMovieInfo = NULL;
	vector<MakeFrameInfo> _pMovieInfo;
	vector<MakeFrameInfo> _pPlayMovieInfo;
	MakeFrameInfo stMakeFrameInfo;

	int nPlayNetIndex = 0;
	int nNetIndex = 0;

	int nPlayArryIndex = 0;
	int nPlayIndex = 0;
	int nIndex = 0;
	int nTemp = 0;
	int nCheck = 0;
	BOOL bRemain = FALSE;

	//jhhan Test
	/**/
	BOOL _bNonePlay = FALSE;
	/**/
	BOOL bGPUSkip = ESMGetValue(ESM_VALUE_GPUMAKESKIP);
	m_arrPath.clear();


	if(bGPUSkip)
		ESMLog(5,_T("Skip GPU Making"));

	if(info.bPlayMaking && !bGPUSkip) //연속플레이 영상생성
	{
		ESMLog(1,_T("#### GPU Make!"));
		int nCheckTotalCount = 0;
		int nTotalCount = 0;
		int nPlayTotalCount = 0;
		int nCount = 0;
		int nPlayCount = 0;
		for(int i = 0; i < pArrFrameInfo->size(); i++)
		{
			nSendCount++;
			stMakeFrameInfo = pArrFrameInfo->at(i);
	
			BOOL bPlay = stMakeFrameInfo.bPlay;


			if(pMovieInfo == NULL)
				pMovieInfo = new vector<MakeFrameInfo>;

			if(pPlayMovieInfo == NULL)
				pPlayMovieInfo = new vector<MakeFrameInfo>;

			pSendPlayMovieInfo = new vector<MakeFrameInfo>;

			if(bPlay)
			{
				pPlayMovieInfo->push_back(stMakeFrameInfo);
				_pPlayMovieInfo.push_back(stMakeFrameInfo);
				pSendPlayMovieInfo->push_back(stMakeFrameInfo);
				_bNonePlay = FALSE;
			}
			else
			{
				/*{
				ESMLog(5, stMakeFrameInfo.strFramePath);
				}*/
				
				pMovieInfo->push_back(stMakeFrameInfo);
				_pMovieInfo.push_back(stMakeFrameInfo);

				//jhhan 17-02-27 : Test -> 회전프레임 후 연속프레임 일시 확인
				if(info.arryCount.size() > 1)
				{
					if(i < pArrFrameInfo->size() - 1)
					{
						MakeFrameInfo *_pstMakeFrameInfo;
						_pstMakeFrameInfo = &pArrFrameInfo->at(i+1);
						BOOL _bPlay = FALSE;
						_bPlay = _pstMakeFrameInfo->bPlay;
						if(_bPlay)
							_bNonePlay = TRUE;
						/*else if(i == pArrFrameInfo->size()-1)
						_bNonePlay = TRUE;*/
					}else
					{
						_bNonePlay = TRUE;
					}

				}
			}
			
			if(bPlay)
			{
				nPlayCount++;
			}
			else
			{
				nCount++;
				nCheckTotalCount++;
				nTotalCount++;
			}
			//ESMLog(5, _T("bPlay[%d] - nPlayCount [%d], nCount [%d], nCheckTotalCount [%d], nTotalCount [%d]"), bPlay, nPlayCount, nCount, nCheckTotalCount, nTotalCount);

			if(nPlayIndex < info.arryPlayCount.size())
				info.nPlay = info.arryPlayCount.at(nPlayIndex);

			if(bPlay && nPlayCount == info.nPlay)
			{
				nPlayTotalCount += nPlayCount;
				int nFileIdx = nPlayIndex + nIndex + nTemp;
				
				try 
				{
					movieInfo _movieInfo;
					_movieInfo.pGroup = info.pGroup;
					_movieInfo.fileNum = nPlayIndex + nIndex + nTemp;
					_movieInfo.m_MovieInfo = _pPlayMovieInfo;
					_movieInfo.makingFin = FALSE;
					_movieInfo.serverFlag = FALSE;
					m_movieMakingList.push_back(_movieInfo);
					
					BOOL bAgentCheck = FALSE;
					for(int i = 0; i < info.arDSCGroup->GetCount(); i++)
					{
						CDSCGroup* pTempGroup = (CDSCGroup*)info.arDSCGroup->GetAt(i); 
						if(pTempGroup->GetRCMgr()->GetIP().CollateNoCase(pSendPlayMovieInfo->at(0).strDSC_IP) == 0)
							bAgentCheck = TRUE;
					}

					CString strFrontIP = ESMGetFrontIP();

					if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork() 
						&& ESMGetAJAScreen() == FALSE &&	ESMGetAJAMaking() == FALSE 
						&& ESMGetAJADivProcess() == TRUE)
					{
						SendAJAGPUFrameInfo(pPlayMovieInfo,nPlayIndex+nIndex+nTemp);
						//파일 접근
						//int nLastIndex = pPlayMovieInfo->size() - 1;

						//stFPoint pt;
						//pt.x = pPlayMovieInfo->at(0).stEffectData.nPosX;
						//pt.y = pPlayMovieInfo->at(0).stEffectData.nPosY;
						//SendAJAGPUFrameInfo(pPlayMovieInfo->at(0).strFramePath,//Frame Path
						//	nPlayIndex+nIndex+nTemp,							//Making Inde
						//	pPlayMovieInfo->at(0).nFrameIndex,					//Start Index
						//	pPlayMovieInfo->at(nLastIndex).nFrameIndex,			//End Index
						//	pPlayMovieInfo->at(0).stEffectData.nZoomRatio,		//Zoom ratio
						//	pt,													//Point data
						//	pPlayMovieInfo->at(0).nFrameSpeed,					//FrameSpeed
						//	ESMGetReverseMovie());								//ReverseMovie

						nPlayIndex++;
						nPlayCount = 0;

						//delete pPlayMovieInfo;
						//pPlayMovieInfo = NULL;

						pPlayMovieInfo = new vector<MakeFrameInfo>;
						_pPlayMovieInfo.clear();
						nSendGourpCount++;
						
						info.nAJASendCnt++;
					}
					else
					{
						//15프레임 구분일시 기존로직 수행
						//if(ESMGetValue(ESM_VALUE_MOVIESAVELOCATION) || _nFrameCount == 15)//0:On, 1:Off
						{
							//ESMLog(5, _T("PLAY GROUP[%d] : nPlayIndex [%d], nPlayArryIndex [%d]"), arPlayDSCGroup.GetCount(),nPlayIndex, nPlayArryIndex);
							info.pGroup = (CDSCGroup*)info.arPlayDSCGroup->GetAt(nPlayIndex + nPlayArryIndex); 
						}					
						//else
						//{
						//	//jhhan 170322
						//	//4DP : 30프레임 로컬 파일 메이킹
						//	CString _strPath;
						//	if(_pPlayMovieInfo.size() > 0)
						//	{
						//		_strPath = _pPlayMovieInfo[0].strFramePath;
						//	}
						//	 
						//	int _Idx = -1;
						//	if(!_strPath.IsEmpty())
						//	{
						//		int n = _strPath.ReverseFind('_');
						//		CString _strTmp = _strPath.Mid(n+1, _strPath.GetLength());
						//		_strTmp = _strTmp.Left(_strTmp.GetLength() - 4);
						//		int _nSec = _ttoi(_strTmp);

						//		_Idx = _nSec % info->nProcessorCnt;

						//	}
						//	int _nIp = 0;
						//	BOOL _bFind = FALSE;
						//	for(int i = 0; i < arPlayDSCGroup.GetCount(); i++)
						//	{
						//		pGroup = NULL;
						//		pGroup = (CDSCGroup*)arPlayDSCGroup.GetAt(i);

						//		if(info->nProcessorIP[_Idx] == pGroup->GetRCMgr()->GetID())
						//		{
						//			_bFind = TRUE;
						//			break;
						//		}
						//	}

						//	if(_bFind == FALSE)
						//	{
						//		pGroup = (CDSCGroup*)arPlayDSCGroup.GetAt(nPlayIndex + nPlayArryIndex); 
						//	}
						//	//CString _strIp = pGroup->GetRCMgr()->GetIP();
						//	//pGroup->GetRCMgr()->GetID();
						//}

						if(ESMGetValue(ESM_VALUE_ENABLE_4DPCOPY) && bAgentCheck)	//Network 전송방식 joonho.kim
						{
							info.pGroup->GetRCMgr()->m_strLocalPath = pSendPlayMovieInfo->at(0).strLocal;

							CString strIP = pSendPlayMovieInfo->at(0).strDSC_IP;
							CString str4DAIP;
							for(int i = 0; i < info.arDSCGroup->GetCount(); i++)
							{
								info.pGroup = (CDSCGroup*)info.arDSCGroup->GetAt(i);
								//str4DAIP.Format(_T("%s%d"),strFrontIP, pGroup->m_nIP);
								//str4DAIP.Format(_T("%s"), pGroup->m_strIP);
								//str4DAIP.Format(_T("192.168.0.%d"), info.pGroup->m_nIP);
								str4DAIP.Format(_T("%s%d"), ESMGetFrontIP(), info.pGroup->m_nIP);

								if(strIP.CompareNoCase(str4DAIP) == 0)
									break;
							}

							BOOL bCheck = FALSE;
							for(int i = 0; i < m_arrPath.size(); i++)
							{
								if(m_arrPath.at(i).CompareNoCase(pSendPlayMovieInfo->at(0).strLocal) == 0)
								{
									bCheck = TRUE;
									break;
								}
							}

							if(!bCheck)
							{
								m_arrPath.push_back(pSendPlayMovieInfo->at(0).strLocal);
								info.pGroup->RequestData(pSendPlayMovieInfo);
							}

							info.pGroup = (CDSCGroup*)info.arPlayDSCGroup->GetAt(nPlayIndex + nPlayArryIndex); 

							info.pGroup->DoMovieMakeFrameMovie(pPlayMovieInfo, nPlayIndex + nIndex + nTemp, TRUE, TRUE);
							//ESMLog(5, _T("________________PLAY_________________[%s][%d]"), pGroup->GetRCMgr()->GetIP(),nFileIdx);

							nPlayIndex++;
							nPlayCount = 0;

							pPlayMovieInfo = new vector<MakeFrameInfo>;
							_pPlayMovieInfo.clear();
							nSendGourpCount++;
						}
						else
						{
							info.pGroup->DoMovieMakeFrameMovie(pPlayMovieInfo, nPlayIndex + nIndex + nTemp, TRUE);
							//ESMLog(5, _T("________________PLAY_________________[%s][%d]"), pGroup->GetRCMgr()->GetIP(),nFileIdx);
							
							nPlayIndex++;
							nPlayCount = 0;

							pPlayMovieInfo = new vector<MakeFrameInfo>;
							_pPlayMovieInfo.clear();
							nSendGourpCount++;
						}
					}
				}
				catch(...)
				{
					ESMLog(5,_T("pGroup GPU Error : nPlayIndex[%d] + nPlayArryIndex[%d]"), nPlayArryIndex, nPlayArryIndex);
				}
			}
#ifdef _test
			else if(( !bPlay && nCount >= nDivision )|| _bNonePlay)
			{
#else
			else if(!bPlay)
			{
				if(i < pArrFrameInfo->size() -1)
				{
					MakeFrameInfo stTemp = pArrFrameInfo->at(i+1);
					CString strIp = stMakeFrameInfo.strDSC_IP;
					CString strNext = stTemp.strDSC_IP;
					if(strIp.Compare(strNext) == 0 && !stTemp.bPlay)
					{
						//AfxMessageBox(strNext);
						continue;
					}
						
				}
#endif
				if( (info.nTotalFrameCount-info.nPlayFrameCount)-nTotalCount < info.nDivision )
				{
					if( nCheckTotalCount == info.arryCount.at(nCheck))
					{
						//ESMLog(5, _T("Reset = nCheckTotalCount [%d], arry.Count [%d], nCheck [%d]"), nCheckTotalCount, arryCount.at(nCheck), nCheck);
						nCheck++;
						nCheckTotalCount = 0;
					}
					else
					{
						//jhhan 170303
						if(nCount >= info.nDivision)
						{
							//skip
						}
						else if(nCheckTotalCount == nTotalCount)
						{
							//skip
						}
						else
						{
							/*ESMLog(5, _T("nCheckTotalCount [%d], arry.Count [%d], nCheck [%d]"), nCheckTotalCount, arryCount.at(nCheck), nCheck);
							ESMLog(5, _T("_______________CONTINUE_________________"));*/
							continue;
						}
					}
				}
				else
				{
					//ESMLog(5, _T("nCheckTotalCount [%d], arry.Count [%d], nCheck [%d]"), nCheckTotalCount, arryCount.at(nCheck), nCheck);
				}
				
				movieInfo _movieInfo;

				

				CDSCGroup* pSender = NULL;
				try 
				{
					pSender = NULL;

					/*if(nIndex >= arDSCGroup.GetCount())
					{

					pGroup = (CDSCGroup*)arPlayDSCGroup.GetAt(nPlayIndex + nTemp); 
					_movieInfo.fileNum = nPlayIndex + nTemp + nIndex;
					nTemp++;
					}
					else*/
					{
#ifdef _AGENT_DIV_
						info.pGroup = (CDSCGroup*)info.arDSCGroup->GetAt(nIndex); 
						_movieInfo.fileNum = nPlayIndex + nIndex;
						nIndex++;
						pSender = info.pGroup;
#else
						int nCount = info.arDSCGroup->GetCount();
						int nCheck = nIndex;

						//int nIP = _ttoi(stMakeFrameInfo.strDSC_IP);
						int nIP = ESMGetIDfromIP(stMakeFrameInfo.strDSC_IP);
						//jhhan 181030
						//*/
						//프레임 아이피 비교 후 메이킹 IP로 매칭 비교
						/*if(ESMGetValue(ESM_VALUE_4DAP))
						{
							int _nIp = ESMGet4DAPSender(stMakeFrameInfo.strDSC_IP);
							if(_nIp > 0 && _nIp <=255)
							{
								ESMLog(5, _T("[CHANGE] 4DAP %d -> %d"), nIP, _nIp);
								nIP = _nIp;
							}
						}*/
						//*/
						while(nCount--)
						{
							
							info.pGroup = (CDSCGroup*)info.arDSCGroup->GetAt(nCount);
							
							if(nIP == info.pGroup->m_nIP)
							{
								pSender = info.pGroup;

								_movieInfo.fileNum = nPlayIndex + nIndex;
								nIndex++;
								break;
							}
						}

						if(nCheck == nIndex)
						{
							/*
							if(pSender)
								pGroup = pSender;
							else
								pGroup = (CDSCGroup*)arDSCGroup.GetAt(0);

							_movieInfo.fileNum = nPlayIndex + nIndex;
							nIndex++;
							*/
							nCount = info.arDSCGroup->GetCount();
							int nIP = ESMGetIDfromIP(stMakeFrameInfo.strDSC_IP);
							ESMLog(5,_T("int nIP = ESMGetIDfromIP(stMakeFrameInfo.strDSC_IP) [%d]"), nIP);
							while(nCount--)
							{
								info.pGroup = (CDSCGroup*)info.arDSCGroup->GetAt(nCount);
								ESMLog(5,_T("pGroup->m_nIP [%d]"), info.pGroup->m_nIP);
							}
						}
#endif
					}
					//int nFileIdx = _movieInfo.fileNum;

					//jhhan
					/*CString _strPath = _pMovieInfo[0].strFramePath;
					if(!_strPath.IsEmpty())
					{
						int n = _strPath.ReverseFind('_');
						CString _strTmp = _strPath.Mid(n+1, _strPath.GetLength());
						_strTmp = _strTmp.Left(_strTmp.GetLength() - 4);
						int _nSec = _ttoi(_strTmp);
					}*/

					if(pSender != NULL)
					{
						_movieInfo.pGroup = info.pGroup;
						_movieInfo.m_MovieInfo = _pMovieInfo;
						_movieInfo.makingFin = FALSE;
						_movieInfo.serverFlag = FALSE;
						m_movieMakingList.push_back(_movieInfo);

						SendAJACPUFrameInfo(_movieInfo.fileNum);
						info.pGroup->DoMovieMakeFrameMovie(pMovieInfo, _movieInfo.fileNum, FALSE);
						//ESMLog(5, _T("GPU_______________NONE_PLAY_________________[%s] [%d]"), pGroup->GetRCMgr()->GetIP(),nFileIdx);
					}
					else
					{
						ESMLog(5,_T("pGroup Disconnect : File Index [%d]"), nPlayIndex + nIndex);
						//nIndex++;
					}
					
				}
				catch(...)
				{
					ESMLog(5,_T("pGroup CPU Error : nPlayIndex[%d] + nTemp[%d] or nIndex[%d]"), nPlayArryIndex, nTemp, nIndex);
				}


				info.nCPUMakeNet--;
				
				if(nCount != info.nDivision)
				{
					//ESMLog(5, _T("__NON_PLAY__ nCount [%d], nDivision [%d]"), nCount, nDivision);
					info.nCPUMakeCount -= nCount;
				}else
				{
					info.nCPUMakeCount -= info.nDivision;
				}

				if(info.nCPUMakeNet > 0 && info.nCPUMakeCount > 0)
				{
					info.nDivision = (info.nCPUMakeCount)/(info.nCPUMakeNet);
					//ESMLog(5, _T("nDivision[%d] = nCPUMakeCount[%d] / nCPUMakeNet[%d]"), nDivision, nCPUMakeCount, nCPUMakeNet);
					//jhhan 170306
					if(info.nCPUMakeCount % info.nCPUMakeNet != 0)
						info.nDivision++;
				}

				if(info.pGroup->GetRCMgr()->m_bGPUUse)
				{
					nPlayArryIndex++;
					//ESMLog(5, _T("%s"),pGroup->GetRCMgr()->GetIP());
					//ESMLog(5, _T("GPU++"));
				}

				nCount = 0;

				pMovieInfo = new vector<MakeFrameInfo>;
				_pMovieInfo.clear();

				if(pSender != NULL)
				{
					nSendGourpCount++;
				}
			}
		}

		if(pMovieInfo)
			delete pMovieInfo;

		if(pPlayMovieInfo)
			delete pPlayMovieInfo;

	}
	else
	{
		for ( int i = 0 ; i<info.nNetMgrCount ; i++)
		{
			pMovieInfo = new vector<MakeFrameInfo>;

			if(info.bFirst == TRUE && i == 0)
			{
				for ( int j = 1 ;j <= MIN_FIRST_FRAME; j++)
				{
					stMakeFrameInfo = pArrFrameInfo->at(nSendCount++);
					pMovieInfo->push_back(stMakeFrameInfo);
					_pMovieInfo.push_back(stMakeFrameInfo);
				}
			}
			else
			{	
				for ( int j = 1 ;j <= info.nDivision; j++)
				{
					stMakeFrameInfo = pArrFrameInfo->at(nSendCount++);
					pMovieInfo->push_back(stMakeFrameInfo);
					_pMovieInfo.push_back(stMakeFrameInfo);
				}

				//jhhan 170308 프레임 나머지 추가 수정
				if(info.nRemainder > 0)
				{
					//ESMLog(5, _T("nRemainder[%d] : %d"),bFirst, nRemainder);

					if (i <= info.nRemainder && info.bFirst == TRUE)
					{
						stMakeFrameInfo = pArrFrameInfo->at(nSendCount++);
						pMovieInfo->push_back(stMakeFrameInfo);
						_pMovieInfo.push_back(stMakeFrameInfo);
					}else if( i < info.nRemainder && info.bFirst == FALSE)
					{
						stMakeFrameInfo = pArrFrameInfo->at(nSendCount++);
						pMovieInfo->push_back(stMakeFrameInfo);
						_pMovieInfo.push_back(stMakeFrameInfo);
					}
				}
			}



			//Client 에 전송해서 해당 영상으로 만들라고 하면 끝.
			if(pMovieInfo->size() > 0)
			{
				if (i < info.arDSCGroup->GetCount())
				{
					info.pGroup = (CDSCGroup*)info.arDSCGroup->GetAt(i); 

					movieInfo _movieInfo;
					_movieInfo.pGroup = info.pGroup;
					_movieInfo.fileNum = i;
					_movieInfo.m_MovieInfo = _pMovieInfo;
					_movieInfo.makingFin = FALSE;
					_movieInfo.serverFlag = FALSE;
					m_movieMakingList.push_back(_movieInfo);

					//pGroup->m_pRCMgr->GetIP();
					//_pMovieInfo.size();
					//i.mp4
					//??

					//jhhan
					/*CString _strPath = _pMovieInfo[0].strFramePath;
					if(!_strPath.IsEmpty())
					{
					int n = _strPath.ReverseFind('_');
					CString _strTmp = _strPath.Mid(n+1, _strPath.GetLength());
					_strTmp = _strTmp.Left(_strTmp.GetLength() - 4);
					int _nSec = _ttoi(_strTmp);
					}*/

					info.pGroup->DoMovieMakeFrameMovie(pMovieInfo, i);
					//ESMLog(5, _T("CPU_______________NONE_PLAY_________________[%s] [%d]"), info.pGroup->GetRCMgr()->GetIP(),i);
				}
				//jhhan 190322
				else if( i < info.arPlayDSCGroup->GetCount())
				{
					info.pGroup = (CDSCGroup*)info.arPlayDSCGroup->GetAt(i); 

					movieInfo _movieInfo;
					_movieInfo.pGroup = info.pGroup;
					_movieInfo.fileNum = i;
					_movieInfo.m_MovieInfo = _pMovieInfo;
					_movieInfo.makingFin = FALSE;
					_movieInfo.serverFlag = FALSE;
					m_movieMakingList.push_back(_movieInfo);

					info.pGroup->DoMovieMakeFrameMovie(pMovieInfo, i);
				}
				else
				{	
					if(!ESMGetValue(ESM_VALUE_CEREMONYUSE) && ESMGetValue(ESM_VALUE_MAKE_SERVER))
					{
						ESMEvent* pMsg	= new ESMEvent;
						pMsg->pParam  = (LPARAM)pMovieInfo;
						pMsg->nParam2 = i;
						pMsg->nParam3 = -100;  //Single Making

						CString strImgPath = ESMGetSaveImagePath();
						CString* pStr = new CString;
						pStr->Format(_T("%s"),strImgPath);
						pMsg->pDest		= (LPARAM)pStr;

						CString str = ESMGetLocalIP();

						CString localStr;
						AfxExtractSubString( localStr, str, 3, '.');

						int m_Ip = _ttoi(localStr);

						CDSCGroup* m_pGroup = new CDSCGroup();
						m_pGroup->m_nIP = m_Ip;

						movieInfo _movieInfo;
						_movieInfo.pGroup = m_pGroup;
						_movieInfo.fileNum = i;
						_movieInfo.m_MovieInfo = _pMovieInfo;
						_movieInfo.makingFin = FALSE;
						_movieInfo.serverFlag = TRUE;
						m_movieMakingList.push_back(_movieInfo);

						int nId =m_Ip;		//Check
						int nFrameCnt = pMovieInfo->size();
						int nMakeFile = i;

						//jhhan
						CString _strPath = _pMovieInfo[0].strFramePath;
						int _nSec = 0;
						if(!_strPath.IsEmpty())
						{
							int n = _strPath.ReverseFind('_');
							CString _strTmp = _strPath.Mid(n+1, _strPath.GetLength());
							_strTmp = _strTmp.Left(_strTmp.GetLength() - 4);
							_nSec = _ttoi(_strTmp);
						}

						CString* pstrTime = new CString;
						pstrTime->Format(_T("%s"),info.strTime);
						ESMEvent * pProcessor = new ESMEvent;
						pProcessor->message = WM_ESM_PROCESSOR_INFO;
						pProcessor->nParam1 = nId;
						pProcessor->nParam2 = nFrameCnt;
						pProcessor->nParam3 = nMakeFile;
						pProcessor->pParam = (LPARAM) pstrTime;
						::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pProcessor);
						
						MakeMovieFrameBase(pMsg);
						{
							//Server Making INFO
						}


						if(pMsg)
						{
							delete pMsg;
							pMsg = NULL;
						}
					}
				}
				nSendGourpCount++;
			}
		}
	}

	

	if (pArrFrameInfo)
	{
		delete pArrFrameInfo;
		pArrFrameInfo = NULL;
	}
	map_MakingData[info.strTime].nSendCount = nSendGourpCount - info.nAJASendCnt;

	if(info.nTotalFrameCount == 0)
		info.nTotalFrameCount = info.nTotalFrameCount;
	ESMLog(5,_T("!!! Sand Check  [%d/%d] "), nSendCount,info.nTotalFrameCount);
	ESMLog(5,_T("[CHECK_R]!!! Frame Data Send Complete !!!  %d "), info.nTotalFrameCount);

	info.nSendGourpCount = nSendGourpCount - info.nAJASendCnt;

	if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetAJAMaking() == FALSE 
		&& ESMGetAJAScreen() == FALSE && ESMGetAJADivProcess() == TRUE)
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message	= F4DC_MAKING_TOTALCNT;
		pMsg->nParam1	= nSendGourpCount;
		pMsg->nParam2	= map_MakingData[info.strTime].nSendCount;
		ESMSendAJANetwork(pMsg);
	}
#endif
}
void CTimeLineView::StartMuxingView()
{
	if(ESMGetValue(ESM_VALUE_DIRECTMUX) && ESMGetValue(ESM_VALUE_GPUMAKESKIP))
	{
		ESMEvent* pMsg = new ESMEvent;
		pMsg->message = WM_ESM_ENV_MUX;
		::PostMessage(ESMGetMainWnd() , WM_ESM, (WPARAM)WM_ESM_OPT, (LPARAM)pMsg);
	}
}
void CTimeLineView::StartAJAThreadInit(MAKE_GROUP_FRAME& info)
{
	//if(ESMGetValue(ESM_VALUE_AJAONOFF))
	{
#ifdef AJA_AUTOPLAY
		CString strPath;
		strPath.Format(_T("%s\\SDIDIVframe.ini"),ESMGetPath(ESM_PATH_CONFIG));

		CESMIni ini;
		if(ini.SetIniFilename(strPath,0))
		{
			CString strState = ini.GetString(_T("SDIDivFrame"),_T("State"),_T(""));
			if(strState == _T("X"))
			{
				ini.WriteString(_T("SDIDivFrame"),_T("State"),_T("O"));
				CString strNum;
				strNum.Format(_T("%d"),info.nSendGourpCount);
				ini.WriteString(_T("SDIDivFrame"),_T("SendCount"),strNum);
				ini.WriteString(_T("SDIDivFrame"),_T("Time"),info.strTime);

				for(int i = 0 ; i < info.nSendGourpCount; i++)
				{
					strNum.Format(_T("%d"),i);
					ini.WriteString(_T("SDIDivFrame"),strNum,_T("X"));
				}
			}
			//ini.WriteString()
		}
#else
		if(ESMGetValue(ESM_VALUE_AJAONOFF))
		{
			CString* pstrTime = new CString;
			pstrTime->Format(_T("%s"),info.strTime);

			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_AJA_THREADINIT;
			pMsg->pParam = (LPARAM) pstrTime;
			pMsg->nParam1= info.nSendGourpCount;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}
#endif
	}
}
void CTimeLineView::MakeMovieFrameBaseAdjust(ESMEvent* pMsg)
{
	ESMAdjustInfo* info = (ESMAdjustInfo*)pMsg->pDest;
	ESMLog(5,_T("!!! Make Adjust Frame Movie START !!!  %s_%d.mp4  Tick:%d "), info->strDSC, info->nCount, GetTickCount());

	vector<MakeFrameInfo>* pInfo = (vector<MakeFrameInfo>*)pMsg->pParam;
	if(CheckAdjustImageSize(pInfo->at(0).strFramePath) < 0)
	{
		ESMLog(5,_T("!!! CheckAdjustImageSize Error "));
		return;
	}
	


	ESMEvent* pMsg2 = new ESMEvent;
	pMsg2->pParam = (LPARAM)pMsg->pParam;
	pMsg2->nParam1 = ESMGetValue(ESM_VALUE_NET_MODE);
	pMsg2->nParam2 = pMsg->nParam2;
	pMsg2->nParam3 = pMsg->nParam3;
	pMsg2->pDest	  = (LPARAM)pMsg->pDest;
	pMsg2->message = WM_ESM_MOVIE_MAKEFRAME_CLIENT_ADJUST;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg2);
}
void CTimeLineView::RequestData(ESMEvent* pMsg)
{
	ESMLog(5,_T("[#NETWORK] !!! Request Data !!!"));
	ESMEvent* pMsg2 = new ESMEvent;
	pMsg2->pParam = (LPARAM)pMsg->pParam;
	pMsg2->message = WM_ESM_MOVIE_REQUEST_DATA;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg2);
}

int g_nCount = 0;
void CTimeLineView::MakeMovieFrameBase(ESMEvent* pMsg)
{
	//jhhan 170713 클라이언트 셋팅


	ESMLog(5,_T("!!! Make Frame Movie START !!!  %d.mp4  Tick:%d "), pMsg->nParam2, GetTickCount());
	vector<MakeFrameInfo>* pArrMovieInfo = (vector<MakeFrameInfo>*)pMsg->pParam;
	CheckImageSize(pArrMovieInfo);
	//jhhan 16-09-19
	ESMLog(5, _T("!!! CheckImageSize FINISH"));

	//m_pArrFrameInfo = (vector<MakeFrameInfo>*)pMsg->pParam;
	CString strPath;

#if 1
	strPath.Format(_T("%s"),ESMGetClientRamPath());
#else
	if(ESMGetValue(ESM_VALUE_AJAONOFF))
		strPath.Format(_T("%s"),ESMGetClientRamPath());
	else
		strPath.Format(_T("%s"),ESMGetServerRamPath());
#endif
//#ifdef _SERVER_SAVE		//jhhan 17-01-16 Server Save
//	CString strPath = ESMGetServerRamPath();
//#else
//	CString strPath = ESMGetClientRamPath();
//#endif
	TCHAR* pData = NULL;
	int nTpData = (sizeof(TCHAR)* _tcslen(strPath) + 2);
	pData = new TCHAR[nTpData];
	memset(pData, 0, nTpData);
	memcpy(pData, strPath, nTpData);
//  	HANDLE handle;
//  	handle = (HANDLE) _beginthread( _DoMakeMovieFrameBase, 0, (void*)pMsg->pParam); // create thread

	ESMEvent* pMsg2 = new ESMEvent;
	pMsg2->pParam = (LPARAM)pMsg->pParam;
	pMsg2->nParam1 = ESMGetValue(ESM_VALUE_NET_MODE);
	pMsg2->nParam2 = pMsg->nParam2;
	pMsg2->nParam3 = pMsg->nParam3;
	pMsg2->pDest	  = (LPARAM)pData;
	pMsg2->pParent	= (LPARAM)pMsg->pDest;

	if(ESMGetValue(ESM_VALUE_ENABLE_4DPCOPY))  //Local 처리
	{
		if(ESMGetReverseMovie())
			pMsg2->message = WM_ESM_MOVIE_MAKEFRAME_CLIENT_REVERSE_LOCAL;
		else
			pMsg2->message = WM_ESM_MOVIE_MAKEFRAME_CLIENT_LOCAL;
	}
	else
	{
		if(ESMGetReverseMovie())
			pMsg2->message = WM_ESM_MOVIE_MAKEFRAME_CLIENT_REVERSE;
		else
			pMsg2->message = WM_ESM_MOVIE_MAKEFRAME_CLIENT;
	}
	

	//if(g_nCount == 0)
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg2);

	g_nCount++;

// 	CString strPath = ESMGetServerRamPath();
// 	TCHAR* pData = NULL;
// 	int nTpData = (sizeof(TCHAR)* _tcslen(strPath) + 2);
// 	pData = new TCHAR[nTpData];
// 	memset(pData, 0, nTpData);
// 	memcpy(pData, strPath, nTpData);
// 
// 	strPath = ESMGetClientRamRootPath();
// 	TCHAR* pData1 = NULL;
// 	nTpData = (sizeof(TCHAR)* _tcslen(strPath) + 2);
// 	pData1 = new TCHAR[nTpData];
// 	memset(pData1, 0, nTpData);
// 	memcpy(pData1, strPath, nTpData);
// 
// 	ESMEvent* pSendMsg = new ESMEvent;
// 	pSendMsg->message = WM_ESM_MOVIE_MAKEFRAME_START;
// 	pSendMsg->nParam1 = ESMGetValue(ESM_VALUE_NET_MODE);
// 	pSendMsg->nParam2 = pMsg->nParam2;
// 	pSendMsg->nParam3 = ESMGetValue(ESM_VALUE_AVISYNTH_INTERFRAME);
// 	pSendMsg->pDest	  = (LPARAM)pData;
// 	pSendMsg->pParent = (LPARAM)pData1;
// 	pSendMsg->pParam  = pMsg->pParam;
// 	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pSendMsg);
}

typedef struct{
	CTimeLineView* pMain;
	
	int nParam1;
	int nParam2;
	int nParam3;
	int nMakingNum;
	BOOL bError;
	CString strTime;
}MAKE_THREAD_PARAM;

unsigned WINAPI CTimeLineView::MakeMovieFinishThread(LPVOID param)
{
	MAKE_THREAD_PARAM* pData = (MAKE_THREAD_PARAM*)param;
	CTimeLineView *pMain = pData->pMain;
	
	BOOL bError = pData->bError;
	CString strDest;
	CString strMoviePath, strFile;
	int nMakingNum = pData->nMakingNum;
	CString strTime;
	
	strTime = pData->strTime;
	
	int _nId = 0;
		_nId = pMain->GetClientId(pData->nParam1);

	CString strFrontIP = ESMGetFrontIP();
	//Time Based Movie
	//if(ESMGetValue(ESM_VALUE_AJAONOFF))
	{		
		if(_nId > -1)
		{
			//strDest.Format(_T("\\\\192.168.0.%d\\Movie\\%s\\%d.mp4"), _nId,strTime, pData->nParam1);
			strDest.Format(_T("\\\\%s%d\\Movie\\%s\\%d.mp4"),strFrontIP, _nId,strTime, pData->nParam1);
		}
		strMoviePath.Format(_T("%s\\%s"),ESMGetClientRamPath(),strTime);

	}

#ifdef _TCP_SENDER

	if(!strDest.IsEmpty() && pData->nParam3 > 0) //pMsg->nParam3 == RemoteID, 0일 경우 서버메이킹 복사 제외
	{
		CESMFileOperation fo;
		//ESMLog(5,_T("%s -> %s"),strDest,strMoviePath);

		if(!fo.Copy(strDest, strMoviePath, FALSE))
		{
			//ESMLog(5,_T("Copy Fail..."));

			//jhhan Copy Fail 발생시 최대 10번 재시도
			int nCnt = 10;
			while(nCnt)
			{
				if(fo.Copy(strDest, strMoviePath, FALSE))
				{
					break;
				}
				/*else
				{
				ESMLog(5,_T("Copy Fail...[%d]"), nCnt--);
				}*/
			}

		}

		//if(ESMGetValue(ESM_VALUE_AJAONOFF))
		{
			CString strForderPath;
			//strForderPath.Format(_T("\\\\192.168.0.%d\\Movie\\%s"),_nId,strTime);
			strForderPath.Format(_T("\\\\%s%d\\Movie\\%s"),strFrontIP, _nId,strTime);
			fo.Delete(strForderPath,1);
		}
	}

#endif
	
	pMain->map_MakingData[strTime].nRecvCount++;
	
	//For Adjust Movie
	pMain->m_nRecvCount++;
	//ESMLog(5,_T("Frame Movie Finish Agent[%d, %d]:[%d/%d][%d]"),pMsg->nParam3 ,pMsg->nParam1, map_MakingData[strTime].nRecvCount, map_MakingData[strTime].nSendCount,pMsg->nParam2);

	int nId = pData->nParam3;		//Check
	//if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		if(nId == 0)
		{
			CString strIP = ESMGetLocalIP();
			nId = ESMGetIDfromIP(strIP);
		}
	}

	int nFrameCnt = pData->nParam2;
	int nMakeFile = pData->nParam1;

	if(bError == TRUE) // 180305 hjcho
		ESMLog(0,_T("Cannot Load [%s]"),strDest);

	if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetConnectAJANetwork() 
		&& ESMGetAJAScreen() == FALSE && ESMGetAJADivProcess() == TRUE
		/*&& ESMGetAJAMaking() == FALSE*/)
	{
		ESMLog(5,_T("[AJA][4DA:%d] Make Cnt: %d[%d]"),_nId,nFrameCnt,pData->nParam1);
		//CString csRamPath = ESMGetPath(ESM_PATH_LOCALMOVIE);
		//CString* pStr = new CString;
		//pStr->Format(_T("\\\\%s\\movie\\%s\\%d.mp4"),ESMGetLocalIP(),strTime,pData->nParam1);
		////pStr->Format(_T("%s\\%s\\%d.mp4"),csRamPath,strTime,pData->nParam1);

		//ESMEvent* pMsg = new ESMEvent;
		//pMsg->message = F4DC_CPUFRAME_MAKE_FINISH;
		//pMsg->nParam1 = pData->nParam1;
		//pMsg->nParam2 = !pData->bError;
		//pMsg->pDest	  = (LPARAM)pStr;
		//ESMSendAJANetwork(pMsg);
	}
	else
	{
		CString *pStrTime = new CString;
		pStrTime->Format(_T("%s"),strTime);

		ESMEvent * pProcessor = new ESMEvent;
		pProcessor->message = WM_ESM_PROCESSOR_INFO_MAKE;
		pProcessor->nParam1 = nId;
		pProcessor->nParam2 = nFrameCnt;
		pProcessor->nParam3 = nMakeFile;
		pProcessor->pParam   = nMakingNum;
		pProcessor->pDest      = (LPARAM) pStrTime;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pProcessor);

		if(ESMGetValue(ESM_VALUE_AJAONOFF))
		{
			ESMEvent * pProcessor1 = new ESMEvent;
			pProcessor1->message = WM_ESM_PROCESSOR_ADDFILE;
			pProcessor1->nParam1 = pData->nParam1;
			pProcessor1->nParam2 = pMain->map_MakingData[strTime].nSendCount;
			pProcessor1->pDest      = (LPARAM) pStrTime;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pProcessor1);
		}
	}

	/*for(int i = 0 ; i < m_movieMakingList.size();i++)
	{
		movieInfo _movieInfo = m_movieMakingList[i];
		if(_movieInfo.pGroup->m_nIP == pMsg->nParam3)
		{
			m_movieMakingList[i].makingFin = TRUE;
		}
	}*/
	

	if(pData)
	{
		delete pData;
		pData = NULL;
	}
	return 0;
}

void CTimeLineView::MakeMovieFrameBaseMovieFinish(ESMEvent* pMsg)
{
	//Thread
	CString *pPath = new CString;
	MAKE_THREAD_PARAM* pThread = new MAKE_THREAD_PARAM;
	pThread->pMain = this;
	pThread->nParam1 = pMsg->nParam1;
	pThread->nParam2 = pMsg->nParam2;
	pThread->nParam3 = pMsg->nParam3;
	pThread->nMakingNum = pMsg->pParam;
	pThread->bError	 = pMsg->pParam;
	if(pMsg->pDest != NULL)
	{
		pPath = (CString*)pMsg->pDest;
		pThread->strTime.Format(_T("%s"),*pPath);
	}
	if(pPath)
	{
		delete pPath;
		pPath = NULL;
	}

#ifndef AJA_AUTOPLAY
	CString strPath;
	strPath.Format(_T("%s\\SDIDIVframe.ini"),ESMGetPath(ESM_PATH_CONFIG));

	CESMIni ini;
	if(ini.SetIniFilename(strPath))
	{
		CString strNum,strFrameCnt;
		strNum.Format(_T("%d"),pMsg->nParam1);
		strFrameCnt.Format(_T("%d"),pMsg->nParam2);
		ini.WriteString(_T("SDIDivFrame"),strNum,strFrameCnt);
	}
#endif

	HANDLE hSyncTime = NULL;
	ESMLog(5,_T("Make Movie Finish [%d]"), pMsg->nParam3);
	hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, MakeMovieFinishThread, pThread, 0, NULL);
	CloseHandle(hSyncTime);
	return;



#if 0
	//ESMLog(5,_T("Frame Movie Finish Agent[%d, %d]:[%d/%d][%d]"),pMsg->nParam3 ,pMsg->nParam1, ++m_nRecvCount,m_nSendCount,pMsg->nParam2);

	//jhhan 17-01-06 TEST_PROCESSOR
	CString strDest;
	CString strMoviePath, strFile;

	//nParam3 -> MakingNumber
	int nMakingNum = pMsg->pParam;
	CString *pPath = new CString;
	CString strTime;
	if(pMsg->pDest != NULL)
	{
		pPath = (CString*)pMsg->pDest;
		strTime.Format(_T("%s"),*pPath);
	}
	if(pPath)
	{
		delete pPath;
		pPath = NULL;
	}

	int _nId = 0;
		_nId = GetClientId(pMsg->nParam1);

	//Time Based Movie
	//if(ESMGetValue(ESM_VALUE_AJAONOFF))
	{		
		if(_nId > -1)
		{
			strDest.Format(_T("\\\\192.168.0.%d\\Movie\\%s\\%d.mp4"), _nId,strTime, pMsg->nParam1);
			//ESMLog(5, _T("MakeMovieFrameBaseMovieFinish : %s"), strDest);
		}
		strMoviePath.Format(_T("%s\\%s"),ESMGetClientRamPath(),strTime);

	}
	//else
	//{
	//	if(_nId > -1)
	//	{
	//		strDest.Format(_T("\\\\192.168.0.%d\\Movie\\%d.mp4"), _nId, pMsg->nParam1);
	//		//ESMLog(5, _T("MakeMovieFrameBaseMovieFinish : %s"), strDest);
	//	}
	//	strMoviePath.Format(_T("%s"),ESMGetClientRamPath());
	//}
//#ifdef _SERVER_SAVE		//jhhan 17-01-16 Server Save
//	int _nId = 0;
//	_nId = GetClientId(pMsg->nParam1);
//	if(_nId > -1)
//	{
//		strDest.Format(_T("\\\\192.168.0.%d\\Movie\\%d.mp4"), _nId, pMsg->nParam1);
//		//ESMLog(5, _T("MakeMovieFrameBaseMovieFinish : %s"), strDest);
//	}
//#endif

	//strMoviePath.Format(_T("%s\\AJA\\%d.mp4"),ESMGetClientRamPath(), pMsg->nParam1);
	//if(ESMGetValue(ESM_VALUE_AJAONOFF))
	//{
		if(!strDest.IsEmpty() && pMsg->nParam3 > 0) //pMsg->nParam3 == RemoteID, 0일 경우 서버메이킹 복사 제외
		{
#if _COPYFILE
			BOOL bSucess = CopyFile(strDest, strMoviePath, FALSE);
			//if(bSucess!=TRUE)
			{
				ESMLog(5, _T("[%d] Copy %s -> %s"), bSucess, strDest, strMoviePath);
			}
#else
			CESMFileOperation fo;

			ESMLog(5,_T("%s -> %s"),strDest,strMoviePath);

			if(!fo.Copy(strDest, strMoviePath, FALSE))
			{
				ESMLog(5,_T("Copy Fail..."));

				//jhhan Copy Fail 발생시 최대 10번 재시도
				int nCnt = 10;
				while(nCnt)
				{
					if(fo.Copy(strDest, strMoviePath, FALSE))
					{
						break;
					}
					else
					{
						ESMLog(5,_T("Copy Fail...[%d]"), nCnt--);
					}
				}

			}
			
			//if(ESMGetValue(ESM_VALUE_AJAONOFF))
			{
				CString strForderPath;
				strForderPath.Format(_T("\\\\192.168.0.%d\\Movie\\%s"),_nId,strTime);
				fo.Delete(strForderPath,1);
			}

			
			//fo.Delete(strDest);
			//ESMLog(5, _T("Copy %s -> %s"),strDest, strMoviePath);
			//Sleep(50);
#endif		
		}
	//}

	//	strFile.Format(_T("%s\\%d.mp4"),ESMGetClientRamPath(), pMsg->nParam1);
//#if AJAFILE
//	CopyFile(strMoviePath, strFile, FALSE);
//#else
//	CopyFile(strDest, strFile, FALSE);
//#endif
	/*CESMFileOperation fo;
	fo.Copy(strMoviePath, strFile, FALSE);*/

	map_MakingData[strTime].nRecvCount++;
	
	//For Adjust Movie
	m_nRecvCount++;
	ESMLog(5,_T("Frame Movie Finish Agent[%d, %d]:[%d/%d][%d]"),pMsg->nParam3 ,pMsg->nParam1, map_MakingData[strTime].nRecvCount, map_MakingData[strTime].nSendCount,pMsg->nParam2);

	int nId = pMsg->nParam3;		//Check
	//if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		if(nId == 0)
		{
			CString strIP = ESMGetLocalIP();
			nId = ESMGetIDfromIP(strIP);
		}
	}

	int nFrameCnt = pMsg->nParam2;
	int nMakeFile = pMsg->nParam1;
	CString *pStrTime = new CString;
	pStrTime->Format(_T("%s"),strTime);
	
	ESMEvent * pProcessor = new ESMEvent;
	pProcessor->message = WM_ESM_PROCESSOR_INFO_MAKE;
	pProcessor->nParam1 = nId;
	pProcessor->nParam2 = nFrameCnt;
	pProcessor->nParam3 = nMakeFile;
	pProcessor->pParam   = nMakingNum;
	pProcessor->pDest      = (LPARAM) pStrTime;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pProcessor);

	if(ESMGetValue(ESM_VALUE_AJAONOFF))
	{
		ESMEvent * pProcessor1 = new ESMEvent;
		pProcessor1->message = WM_ESM_PROCESSOR_ADDFILE;
		pProcessor1->nParam1 = pMsg->nParam1;
		pProcessor1->nParam2 = map_MakingData[strTime].nSendCount;
		pProcessor1->pDest      = (LPARAM) pStrTime;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pProcessor1);
	}

	/*for(int i = 0 ; i < m_movieMakingList.size();i++)
	{
		movieInfo _movieInfo = m_movieMakingList[i];
		if(_movieInfo.pGroup->m_nIP == pMsg->nParam3)
		{
			m_movieMakingList[i].makingFin = TRUE;
		}
	}*/
#endif	
}

//CMiLRe 20151119 동영상 사이즈(FHD, UHD) Check
int CTimeLineView::CheckImageSize(vector<MakeFrameInfo>* pArrMovieInfo)
{
	CESMFileOperation fo;
	CDSCItem* pItem = NULL;
	//if( m_nImageSize == 0)
	//jhhan 16-09-19
	ESMLog(5, _T("CheckImageSize - (%d)"), m_nImageSize);
	
	int nFrameRate = ESMGetFrameRate();
	if( nFrameRate == MOVIE_FPS_UHD_25P || nFrameRate == MOVIE_FPS_UHD_30P || nFrameRate == MOVIE_FPS_UHD_30P_X2
		|| nFrameRate == MOVIE_FPS_UHD_60P_X2 || nFrameRate == MOVIE_FPS_UHD_30P_X1 || nFrameRate == MOVIE_FPS_UHD_60P_X1
		|| nFrameRate == MOVIE_FPS_UHD_60P || nFrameRate == MOVIE_FPS_UHD_50P )
	{
		m_nImageSize = ESM_MOVIESIZE_UHD;
		ESMLog(5, _T("Set Movie Size [UltraHD]"));
	}
	else if( nFrameRate == MOVIE_FPS_FHD_50P || nFrameRate == MOVIE_FPS_FHD_30P 
		|| nFrameRate == MOVIE_FPS_FHD_25P || nFrameRate == MOVIE_FPS_FHD_60P || nFrameRate == MOVIE_FPS_FHD_120P)
	{
		m_nImageSize = ESM_MOVIESIZE_FHD;
		ESMLog(5, _T("Set Movie Size [FullHD]"));
	}
	else
	{
		//if( m_nImageSize == -1)
		//{	
			ESMLog(5, _T("pArrMovieInfo->size() - (%d)"), pArrMovieInfo->size());
			for( int i = 0; i< pArrMovieInfo->size(); i++)
			{
				MakeFrameInfo stInfo;
				CString strMoviePath, strLocal;

				stInfo = pArrMovieInfo->at(i);
				strMoviePath.Format(_T("%s"), stInfo.strFramePath);
			
				//jhhan 170329
				strLocal.Format(_T("%s"), stInfo.strLocal);
				if(fo.IsFileExist(strLocal))
				{
					strMoviePath = strLocal;
					ESMLog(5, strMoviePath);
				}

				if( !strMoviePath.IsEmpty() )
				{
					if(fo.IsFileExist(strMoviePath))
					{
						int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3, nFrameRate= 0;
						FFmpegManager ffmpeg(FALSE);
						ffmpeg.GetMovieInfo(strMoviePath, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
						if( nWidth == 0)
							continue;

						if( nWidth == 1920)
						{
							m_nImageSize = ESM_MOVIESIZE_FHD;
							ESMLog(5, _T("Set Movie Size [FullHD]"));
						}
						else if( nWidth == 3840 )
						{
							m_nImageSize = ESM_MOVIESIZE_UHD;
							ESMLog(5, _T("Set Movie Size [UltraHD]"));
						}
						else
						{
							m_nImageSize = ESM_MOVIESIZE_FHD;
							ESMLog(5, _T("Set Movie Size [Error Default Set!!]"));
						}
						break;
					} else {
					 ESMLog(0, _T("can't find MP4 File : %s"),strMoviePath);
					}

				} else {
					ESMLog(0, _T("not correct path : %s"),stInfo.strFramePath);
				}

			}
		//}
	}

	ESMEvent* pMsg2 = new ESMEvent;
	pMsg2->nParam1 = m_nImageSize;
	pMsg2->nParam2 = ESMGetMovieFlag();
	pMsg2->nParam3 = ESMGetVMCCFlag();
	pMsg2->message = WM_ESM_MOVIE_SET_IMAGESIZE;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg2);

	//wgkim 170727
	ESMEvent* pMsg3 = new ESMEvent;
	pMsg3->nParam1 = ESMGetValue(ESM_VALUE_LIGHTWEIGHT);
	pMsg3->message = WM_ESM_MOVIE_SET_LIGHT_WEIGHT;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg3);

	//wgkim 180629
	ESMEvent* pMsg4 = new ESMEvent;
	pMsg4->nParam1 = ESMGetValue(ESM_VALUE_FRAMERATE);
	pMsg4->message = WM_ESM_MOVIE_SET_FRAME_RATE;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg4);

	ESMEvent* pMsg5 = new ESMEvent;
	pMsg5->nParam1 = ESMGetValue(ESM_VALUE_AUTOADJUST_KZONE);
	pMsg5->message = WM_ESM_MOVIE_OPTION_AUTOADJUST_KZONE;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg5);

	//CMiLRe 20151119 동영상 사이즈(FHD, UHD) Check
	return m_nImageSize;
}

int CTimeLineView::CheckAdjustImageSize(CString strPath)
{
	CESMFileOperation fo;
	CDSCItem* pItem = NULL;
	//if( m_nImageSize == 0)

	int nFrameRate = ESMGetFrameRate();
	if( nFrameRate == MOVIE_FPS_UHD_30P || nFrameRate == MOVIE_FPS_UHD_25P )
	{
		m_nImageSize = ESM_MOVIESIZE_UHD;
		ESMLog(5, _T("Set Movie Size [UltraHD]"));
	}
	else if( nFrameRate == MOVIE_FPS_FHD_30P || nFrameRate == MOVIE_FPS_FHD_50P || nFrameRate == MOVIE_FPS_FHD_25P || nFrameRate == MOVIE_FPS_FHD_60P || nFrameRate == MOVIE_FPS_FHD_120P)
	{
		m_nImageSize = ESM_MOVIESIZE_FHD;
		ESMLog(5, _T("Set Movie Size [FullHD]"));
	}
	else
	{
		if( !strPath.IsEmpty() )
		{
			if(fo.IsFileExist(strPath))
			{
				int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3, nFrameRate= 0;
				FFmpegManager ffmpeg(FALSE);
				ffmpeg.GetMovieInfo(strPath, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
				if( nWidth == 0)
				{
					ESMLog(5, _T("Set Movie Size [FullHD]"));
					return -1;
				}

				if( nWidth == 1920)
				{
					m_nImageSize = ESM_MOVIESIZE_FHD;
					
				}
				else if( nWidth == 3840 )
				{
					m_nImageSize = ESM_MOVIESIZE_UHD;
					ESMLog(5, _T("Set Movie Size [UltraHD]"));
				}
				else
				{
					m_nImageSize = ESM_MOVIESIZE_FHD;
					ESMLog(5, _T("Set Movie Size [Error Default Set!!]"));
				}

			} else {
				ESMLog(0, _T("can't find MP4 File : %s"),strPath);
			}

		} else {
			ESMLog(0, _T("not correct path : %s"),strPath);
		}
	}

	ESMEvent* pMsg2 = new ESMEvent;
	pMsg2->nParam1 = m_nImageSize;
	pMsg2->nParam2 = ESMGetMovieFlag();
	pMsg2->nParam3 = ESMGetVMCCFlag();
	pMsg2->message = WM_ESM_MOVIE_SET_IMAGESIZE;
	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg2);
	//CMiLRe 20151119 동영상 사이즈(FHD, UHD) Check
	return m_nImageSize;
}

void CTimeLineView::DeleteMovie()
{
	CESMFileOperation fo;
	CString strFile;
	strFile.Format(_T("%s"), ESMGetServerRamPath());
	fo.Delete(strFile,FALSE);

	if(ESMGetValue(ESM_VALUE_AJAONOFF))
	{
		SetClientInit();
	}
//#ifndef _SERVER_SAVE		//jhhan 17-01-16 Server Save
//	SetClientInit();
//#endif
}

void CTimeLineView::SetClientId(UINT nFile, UINT nIp)
{
	m_Client.SetAt(nFile, nIp);
}
UINT CTimeLineView::GetClientId(UINT nFile)
{
	UINT nIp = -1;
	/*BOOL bRet = FALSE;
	bRet = */m_Client.Lookup(nFile, nIp);
	return nIp;
}
FrameRGBInfo CTimeLineView::DecodeImage(vector<MakeFrameInfo>*pArrFrameInfo)
{
	Mat frame;
	FrameRGBInfo stColorInfo;
	BOOL bCheckColor = FALSE;
	for(int i = 0 ; i < pArrFrameInfo->size(); i++)
	{
		CT2CA pszCvted(pArrFrameInfo->at(i).strFramePath);
		std::string strBasePath(pszCvted);

		cv::VideoCapture vc(strBasePath);
		if(!vc.isOpened())
		{
			vc.release();
			continue;
		}
		else
		{
			int nCnt = 0;

			while(1)
			{
				vc>>frame;
				if(frame.empty() || nCnt == pArrFrameInfo->at(i).nFrameIndex)
					break;

				nCnt++;
			}

			bCheckColor = TRUE;
			break;
		}
	}
	if(!bCheckColor)
	{
		ESMLog(5,_T("Connot Load All Frames"));
		stColorInfo.dBlue = -1;

		return stColorInfo;
	}
	int nIdx = 0;
	double dB = 0,dG=0,dR=0;

	for(int i = 0 ; i < frame.total();i++)
	{
		dB += frame.data[nIdx++]; //B
		dG += frame.data[nIdx++]; //G
		dR += frame.data[nIdx++]; //R
	}
	stColorInfo.dBlue = dB / frame.total();
	stColorInfo.dGreen = dG / frame.total();
	stColorInfo.dRed = dR / frame.total();

	//BOOL bYUV;

	//if(pArrFrameInfo->at(0).nDscIndex > -1 
	//	|| pArrFrameInfo->at(0).nPriIndex > -1 || pArrFrameInfo->at(0).nPrinumIndex > 0)
	//	bYUV = FALSE;
	//else
	//	bYUV = TRUE;

	//if(bYUV == FALSE)//RGB Processing
	//{
	//	int nIdx = 0;
	//	double dB = 0,dG=0,dR=0;

	//	for(int i = 0 ; i < frame.total();i++)
	//	{
	//		dB += frame.data[nIdx++]; //B
	//		dG += frame.data[nIdx++]; //G
	//		dR += frame.data[nIdx++]; //R
	//	}
	//	stColorInfo.dBlue = dB / frame.total(); 
	//	stColorInfo.dGreen = dG / frame.total();
	//	stColorInfo.dRed = dR / frame.total();  
	//}
	//else//YUV Processing
	//{
	//	Mat YUV;
	//	int nWidth = frame.cols;
	//	int nHeight = frame.rows;
	//	int nYSize = nWidth * nHeight;
	//	int nUVSize = nWidth/2 * nHeight/2;
	//	cvtColor(frame,YUV,cv::COLOR_BGR2YUV_I420);

	//	Mat Y(nHeight,nWidth,CV_8UC1,YUV.data);
	//	Mat U(nHeight/2,nWidth/2,CV_8UC1,YUV.data+nYSize);
	//	Mat V(nHeight/2,nWidth/2,CV_8UC1,YUV.data+nYSize+nUVSize);

	//	double dY = 0,dU = 0,dV = 0;
	//	for(int i = 0 ; i < nYSize; i++)
	//	{
	//		dY += Y.data[i];
	//	}
	//	for(int i = 0; i < nUVSize; i++)
	//	{
	//		dU += U.data[i];
	//		dV += V.data[i];
	//	}
	//	stColorInfo.dBlue   = dY / nYSize;
	//	stColorInfo.dGreen  = dU / nUVSize;
	//	stColorInfo.dRed	= dV / nUVSize;
	//}

	return stColorInfo;
}

void CTimeLineView::WriteIni(CString strFilePath)
{
	CESMIni ini;

	CString strFile,strIni;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_SDIPLAYER);

	//-- Check File Exist
	CESMFileOperation fo;
	if(!fo.IsFileExist(strFile))
	{
		CFile file;
		file.Open(strFile, CFile::modeCreate);
		file.Close();
	}
	CString strState = _T("O");//문자,영어 'O'
	
	int nIndex = strFilePath.ReverseFind('\\') + 1;
	CString strFileName = strFilePath.Right(strFilePath.GetLength() - nIndex);

	nIndex = strFilePath.Find('\\');
	strFilePath.Delete(0, nIndex);
	CString strPath;
	strPath.Format(_T("\\\\%s%s"), ESMUtil::GetLocalIPAddress(),strFilePath);

	if(ESMGetConnectAJANetwork())
	{
		CString* pCsMovieFile = new CString;
		pCsMovieFile->Format(_T("%s"),strPath);
		ESMEvent* pAJAEvent = new ESMEvent;
		pAJAEvent->message	= F4DC_AUTO_ADD;
		pAJAEvent->pDest	= (LPARAM)pCsMovieFile;
		ESMSendAJANetwork(pAJAEvent);
	}

	if(ini.SetIniFilename (strFile))
	{
		ini.WriteString(STR_SECTION_SDIPLAYER,STR_STATE,strState);
		ini.WriteString(STR_SECTION_SDIPLAYER,STR_FILENAME,strFileName);
		ini.WriteString(STR_SECTION_SDIPLAYER,STR_FILEPATH,strPath);
	}	
}

void CTimeLineView::SetMetadata(CString strMovieFile, CString m_strTotalMetadata)
{
	FILE *fp;	

	char filename[255] = {0,};	
	sprintf(filename, "%S", strMovieFile);

	fp = fopen(filename, "ab");
	
	if(fp == NULL)
	{
		ESMLog(5, _T("[Metadata] File Open Error : %s"), strMovieFile);
		return;
	}	

	fseek(fp, 0, SEEK_END);

	long length = ftell(fp);
	if(length <= sizeof(char)*2047)
	{
		fclose(fp);
		return;
	}

	char metadata[2047] = {0,};	
	sprintf(metadata, "%S", m_strTotalMetadata);	
	fwrite (metadata , 1 , sizeof(metadata) , fp );	

	fclose(fp);
}

//wgkim 180211
void CTimeLineView::SetStabilizationInterval(vector<MakeFrameInfo>* pArrFrameInfo)
{
	m_stabilizerStartFrameNumber = 0;
	m_stabilizerEndFrameNumber = 0;

	double nFrame = 0.;
	if( ESMGetFrameRate() == MOVIE_FPS_UHD_25P || 
		ESMGetFrameRate() == MOVIE_FPS_FHD_25P)
		nFrame = 25;
	else if(ESMGetFrameRate() == MOVIE_FPS_UHD_30P || 
			ESMGetFrameRate() == MOVIE_FPS_FHD_30P)
		nFrame = 30;
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_50P || 
			ESMGetFrameRate() == MOVIE_FPS_UHD_50P)
		nFrame = 20;
	else if(ESMGetFrameRate() == MOVIE_FPS_FHD_60P || 
			ESMGetFrameRate() == MOVIE_FPS_UHD_60P)
		nFrame = 60;
	else
		nFrame = 30;

	CString strStartCameraNumber;

	if(ESMGetValue(ESM_VALUE_AJANETWORK) && ESMGetAJAMaking() == FALSE 
		&& ESMGetAJAScreen() == FALSE && ESMGetAJADivProcess() == TRUE)
	{
		for(int i = 0; i < pArrFrameInfo->size(); i++)
		{
			if(i == 0)
				strStartCameraNumber = pArrFrameInfo->at(0).strDSC_ID;

			if(!strStartCameraNumber.CompareNoCase(pArrFrameInfo->at(i).strDSC_ID))
			{
				m_stabilizerStartFrameNumber ++;
			}
			else
			{
				MakeFrameInfo* pFrame = &pArrFrameInfo->at(i-1);
				pFrame->nStabilizerFrame = 1;
				//pArrFrameInfo->at(i-1).nStabilizerFrame = 1;
				break;
			}
		}
		CString strEndCameraNumber;
		for(int i = pArrFrameInfo->size()-1; i >= 0; i--)
		{
			if(i == pArrFrameInfo->size()-1)
				strEndCameraNumber = pArrFrameInfo->at(pArrFrameInfo->size()-1).strDSC_ID;

			if(!strEndCameraNumber.CompareNoCase(pArrFrameInfo->at(i).strDSC_ID))
			{
				m_stabilizerEndFrameNumber += pArrFrameInfo->at(i).nFrameSpeed/nFrame;
			}
			else
			{
				MakeFrameInfo* pFrame = &pArrFrameInfo->at(i+1);
				pFrame->nStabilizerFrame = 2;
				break;
			}
		}
	}
	else
	{
		CString strStartCameraNumber;
		for(int i = 0; i < pArrFrameInfo->size(); i++)
		{
			if(i == 0)
				strStartCameraNumber = pArrFrameInfo->at(0).strDSC_ID;

			if(!strStartCameraNumber.CompareNoCase(pArrFrameInfo->at(i).strDSC_ID) &&
				pArrFrameInfo->at(i).nFrameSpeed < nFrame * 7)
			{
				m_stabilizerStartFrameNumber += pArrFrameInfo->at(i).nFrameSpeed/nFrame;
			}
			else
			{			
				break;
			}
		}

		CString strEndCameraNumber;
		for(int i = pArrFrameInfo->size()-1; i >= 0; i--)
		{
			if(i == pArrFrameInfo->size()-1)
				strEndCameraNumber = pArrFrameInfo->at(pArrFrameInfo->size()-1).strDSC_ID;

			if(!strEndCameraNumber.CompareNoCase(pArrFrameInfo->at(i).strDSC_ID) &&
				pArrFrameInfo->at(i).nFrameSpeed < nFrame * 7)
			{
				m_stabilizerEndFrameNumber += pArrFrameInfo->at(i).nFrameSpeed/nFrame;
			}
			else
			{
				break;
			}
		}
	}
}
void CTimeLineView::SendAJAGPUFrameInfo(CString strPath,int nMakingIndex,int nStartIndex,int nEndIndex,double dbZoomRatio,stFPoint pt,int nFrameRate,int nReverse)
{
	/*ESMEvent* pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_MAKEFRAMEMOVIETOCLIENT;
	pMsg->pParam  = (LPARAM)pMovieInfo;
	pMsg->nParam1  = bLocal;
	pMsg->nParam2  = nMovieNum;
	pMsg->nParam3  = bPlay;
	m_pRCMgr->AddMsg(pMsg);*/

	AJASendGPUFrameInfo* pstSendInfo = new AJASendGPUFrameInfo;
	_stprintf(pstSendInfo->strPath,strPath);
	pstSendInfo->nMakingIndex	= nMakingIndex;
	pstSendInfo->nStartIndex	= nStartIndex;
	pstSendInfo->nEndIndex		= nEndIndex;
	pstSendInfo->nZoomRatio		= (int)dbZoomRatio;
	pstSendInfo->nPosX			= pt.x;
	pstSendInfo->nPosY			= pt.y;
	pstSendInfo->nFrameRate		= nFrameRate;
	pstSendInfo->nReverse		= nReverse;

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= F4DC_GPUFRAMEPATH_SEND;
	pMsg->pParam	= (LPARAM)pstSendInfo;
	//pMsg->nParam1	= pnArrIndexInfo->size();

	ESMSendAJANetwork(pMsg);
}
void CTimeLineView::SendAJACPUFrameInfo(int nIdx)
{
	ESMEvent* pMsg = new ESMEvent;
	pMsg->message = F4DC_CPUFRAME_INFO;
	pMsg->nParam1 = nIdx;

	ESMSendAJANetwork(pMsg);
}
void CTimeLineView::SendAJAGPUFrameInfo(vector<MakeFrameInfo>* pMovieInfo, int nMovieNum)
{
	ESMEvent* pMsg = new ESMEvent();
	pMsg->message = F4DC_GPUFRAMEPATH_SEND;
	pMsg->pParam  = (LPARAM)pMovieInfo;
	pMsg->nParam1  = nMovieNum;

	ESMSendAJANetwork(pMsg);
}
CString CTimeLineView::GetMoviePathBaseOnIP(CString strPath)
{

	int nIndex = strPath.ReverseFind('\\') + 1;
	CString strFileName = strPath.Right(strPath.GetLength() - nIndex);

	nIndex = strPath.Find('\\');
	strPath.Delete(0, nIndex);
	CString strIPPath;
	strIPPath.Format(_T("\\\\%s%s"), ESMUtil::GetLocalIPAddress(),strPath);

	return strIPPath;
};
void CTimeLineView::CreateGifFile(CString strPath,CString strDate,CString str4DMName)
{
	GIFTHREADDATA* pGIFThreadData = new GIFTHREADDATA;
	pGIFThreadData->pParent		 = this;
	pGIFThreadData->strPath		 = strPath;
	pGIFThreadData->strDate		 = strDate;
	pGIFThreadData->str4DMName	 = str4DMName;

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_LaunchGIFFile,(void*)pGIFThreadData,0,NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CTimeLineView::_LaunchGIFFile(LPVOID param)
{
	GIFTHREADDATA* pGIFThreadData = (GIFTHREADDATA*)param;
	CString strPath = pGIFThreadData->strPath;
	CString strDate = pGIFThreadData->strDate;
	CString str4DMName = pGIFThreadData->str4DMName;
	delete pGIFThreadData;pGIFThreadData = NULL;

	CString strCmd,strPaletteName;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));	

	CString strFileName = ESMGetFileName(strPath);
	int nFind			= strFileName.ReverseFind('.');
	strPaletteName = strFileName.Left(nFind);

	CString strNewFileName = strFileName.Left(nFind);
	CString strPalettePath,strGifPath;
	strPalettePath.Format(_T("%s\\2D\\%s\\%s.png"),ESMGetPath(ESM_PATH_OUTPUT),strDate,strNewFileName);
	strGifPath.Format(_T("%s\\2D\\%s\\%s.gif"),ESMGetPath(ESM_PATH_OUTPUT),strDate,strNewFileName);

	CString strOpt;
	strOpt.Format(_T("-i %s -filter_complex \"[0:v] palettegen\" %s"),strPath,strPalettePath);

	CString strSize,strFrameRate;
	strFrameRate.Format(_T("%f"),ESMGetEncodingFrameRate());

	switch(ESMGetValue(ESM_VALUE_GIF_SIZE))
	{
	case 0: // FHD
		{
			strSize.Format(_T("1920x1080"));
		}
		break;
	case 1: //HD
		{
			strSize.Format(_T("1280x720"));
		}
		break;
	case 2:
		{
			strSize.Format(_T("640x360"));
		}
		break;
	default:
		{
			strSize.Format(_T("1280x720"));
		}
		break;
	}
	


	switch(ESMGetValue(ESM_VALUE_GIF_QUALITY))
	{
	case 0://High Quality;
		{
			ESMLog(5,_T("Palette gen Start"));
			//Create palette
			//ffmpeg -i input -filter_complex "[0:v] palettegen" output.png
			SHELLEXECUTEINFO lpExecInfoReEncoding;
			lpExecInfoReEncoding.cbSize  = sizeof(SHELLEXECUTEINFO);
			lpExecInfoReEncoding.lpFile = strCmd;
			lpExecInfoReEncoding.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
			lpExecInfoReEncoding.hwnd = NULL;  
			lpExecInfoReEncoding.lpVerb = L"open";
			lpExecInfoReEncoding.lpParameters = strOpt;
			lpExecInfoReEncoding.lpDirectory = NULL;

			lpExecInfoReEncoding.nShow = SW_HIDE; // hide shell during execution
			lpExecInfoReEncoding.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
			ShellExecuteEx(&lpExecInfoReEncoding);

			// wait until the process is finished
			if (lpExecInfoReEncoding.hProcess != NULL)
			{
				::WaitForSingleObject(lpExecInfoReEncoding.hProcess, INFINITE);
				::CloseHandle(lpExecInfoReEncoding.hProcess);
			}
			ESMLog(5,_T("Palette gen Finish"));

			//Create Gif file
			strOpt.Format(_T("-i %s -i %s -s %s -r %s -filter_complex \"[0:v][1:v] paletteuse\" %s"),
				strPath,strPalettePath,strSize,strFrameRate,strGifPath);
			//ffmpeg -i input -i palettegen.png -filter_complex "[0:v][1:v] paletteuse" output.gif
			lpExecInfoReEncoding.cbSize  = sizeof(SHELLEXECUTEINFO);
			lpExecInfoReEncoding.lpFile = strCmd;
			lpExecInfoReEncoding.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
			lpExecInfoReEncoding.hwnd = NULL;  
			lpExecInfoReEncoding.lpVerb = L"open";
			lpExecInfoReEncoding.lpParameters = strOpt;
			lpExecInfoReEncoding.lpDirectory = NULL;

			lpExecInfoReEncoding.nShow = SW_HIDE; // hide shell during execution
			lpExecInfoReEncoding.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
			ShellExecuteEx(&lpExecInfoReEncoding);

			// wait until the process is finished
			if (lpExecInfoReEncoding.hProcess != NULL)
			{
				::WaitForSingleObject(lpExecInfoReEncoding.hProcess, INFINITE);
				::CloseHandle(lpExecInfoReEncoding.hProcess);
			}
			ESMLog(5,_T("Gif Making Finish"));

			CESMFileOperation fo;
			if(fo.IsFileExist(strPalettePath))
				fo.Delete(strPalettePath);
		}
		break;
	case 1://Normal
		{
			ESMLog(5,_T("Gif Making Start"));
			strOpt.Format(_T("-i %s -s %s -r %s %s"),strPath,strSize,strFrameRate,strGifPath);
			SHELLEXECUTEINFO lpExecInfoReEncoding;
			lpExecInfoReEncoding.cbSize  = sizeof(SHELLEXECUTEINFO);
			lpExecInfoReEncoding.lpFile = strCmd;
			lpExecInfoReEncoding.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
			lpExecInfoReEncoding.hwnd = NULL;  
			lpExecInfoReEncoding.lpVerb = L"open";
			lpExecInfoReEncoding.lpParameters = strOpt;
			lpExecInfoReEncoding.lpDirectory = NULL;

			lpExecInfoReEncoding.nShow = SW_HIDE; // hide shell during execution
			lpExecInfoReEncoding.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
			ShellExecuteEx(&lpExecInfoReEncoding);

			// wait until the process is finished
			if (lpExecInfoReEncoding.hProcess != NULL)
			{
				::WaitForSingleObject(lpExecInfoReEncoding.hProcess, INFINITE);
				::CloseHandle(lpExecInfoReEncoding.hProcess);
			}
			ESMLog(5,_T("Gif Making Finish"));
		}
		break;
	}

	return TRUE;
}
void CTimeLineView::SaveDecodeImage(vector<MakeFrameInfo>*pArrFrameInfo)
{
#if 1
	SYSTEMTIME st;
	GetLocalTime(&st);

	CString strDate = _T("");
	strDate.Format(_T("%04d%02d%02d"),st.wYear,st.wMonth,st.wDay); 
	CString str4DMName = ESMGetFrameRecord();

	CESMFileOperation fo;
	CString strForder = _T("");
	strForder.Format(_T("%s\\2D\\%s"),ESMGetPath(ESM_PATH_OUTPUT), strDate);

	if(!fo.IsFileFolder(strForder))
		fo.CreateFolder(strForder);

	CString strMovieFile;
	strMovieFile.Format(_T("%s\\2D\\%s\\%s.jpg"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName);

	int nCount = 0;
	while(FileExists(strMovieFile))
	{
		strMovieFile.Format(_T("%s\\2D\\%s\\%s_%d.jpg"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName, nCount);
		nCount++;
	}
	CString* pstr = new CString;
	pstr->Format(_T("%s"),strMovieFile);

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_SAVE_STILLIMAGE;
	pMsg->pParam	= (LPARAM)pstr;

	::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);
#else
	Mat frame;
	BOOL bFind = FALSE;
	for(int i = 0 ; i < pArrFrameInfo->size(); i++)
	{
		CT2CA pszCvted(pArrFrameInfo->at(i).strFramePath);
		std::string strBasePath(pszCvted);

		cv::VideoCapture vc(strBasePath);
		if(!vc.isOpened())
		{
			vc.release();
			continue;
		}
		else
		{
			int nCnt = 0;

			while(1)
			{
				vc>>frame;
				if(frame.empty() || nCnt == pArrFrameInfo->at(i).nFrameIndex)
				{
					bFind = TRUE;
					break;
				}

				nCnt++;
			}

			break;
		}
	} 
	if(bFind == TRUE)
	{
		SYSTEMTIME st;
		GetLocalTime(&st);

		CString strDate = _T("");
		strDate.Format(_T("%04d%02d%02d"),st.wYear,st.wMonth,st.wDay); 

		CString str4DMName = ESMGetFrameRecord();

		CESMFileOperation fo;
		CString strForder = _T("");
		strForder.Format(_T("%s\\2D\\%s"),ESMGetPath(ESM_PATH_OUTPUT), strDate);

		if(!fo.IsFileFolder(strForder))
			fo.CreateFolder(strForder);

		CString strMovieFile;
		strMovieFile.Format(_T("%s\\2D\\%s\\%s.jpg"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName);

		int nCount = 0;
		while(FileExists(strMovieFile))
		{
			strMovieFile.Format(_T("%s\\2D\\%s\\%s_%d.jpg"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName, nCount);
			nCount++;
		}

		resize(frame,frame,cv::Size(720,447),0,0,cv::INTER_CUBIC);

		char strPath[200];
		sprintf(strPath,"%S",strMovieFile);
		ESMLog(5,strMovieFile);

		imwrite(strPath,frame);
	}
#endif
}