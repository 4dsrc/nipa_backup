////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineProcessor.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	
// @Date	
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMFileOperation.h"
#include "TimeLineProcessor.h"

#include "MainFrm.h"
#include "ESMTimeLineObjectSelector.h"
#include "DSCFrameSelector.h"

#include "ESMUtil.h"
#include "ESMFunc.h"
#include "SRSIndex.h"
#include "ESMTimeLineObjectEditor.h"
#include <afxmt.h>

#ifdef _4DMODEL
#include "4DModelerVersion.h"
#else
#include "4DMakerVersion.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CMutex g_MutexUI(FALSE, NULL);

//IMPLEMENT_DYNAMIC(CTimeLineProcessor, CDialog)
BEGIN_MESSAGE_MAP(CTimeLineProcessor, CDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_COMMAND(ID_IMAGE_MAKEMOVIE				, OnDSCMakeMovie	) 
	ON_COMMAND(ID_IMAGE_MAKEMOVIE_3D			, OnDSCMakeMovie3D	)
	ON_COMMAND(ID_IMAGE_MOVIEPLAY				, OnDSCMoviePlay	)
	ON_COMMAND(ID_IMAGE_SCALING					, OnDSCSCaling		)	
	ON_WM_KEYUP()
END_MESSAGE_MAP()


CTimeLineProcessor::CTimeLineProcessor(CWnd* pParent /*=NULL*/)
	: CDialog(CTimeLineProcessor::IDD, pParent)	
{	
	m_pTimeLineView = NULL;
	m_nMakeTime = 0;
	m_nRunningTime = 0;
	m_pMainWnd = NULL;

	m_nStatus = 0;
	m_bLoadPlay = FALSE;

	InitializeCriticalSection(&m_CriSection);
}

CTimeLineProcessor::~CTimeLineProcessor()
{
	DeleteCriticalSection(&m_CriSection);
}

void CTimeLineProcessor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTimeLineProcessor)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
}

//seo
BOOL CTimeLineProcessor::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg)
	{
		if(pMsg->message == WM_KEYDOWN)
		{
			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			ESMEvent* pEsmMsg = NULL;
			pEsmMsg = new ESMEvent();
			pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
			pEsmMsg->pParam = (LPARAM)pMsg;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

// CFrameNailDlg message handlers
BOOL CTimeLineProcessor::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_pMainWnd = (CMainFrame*)AfxGetMainWnd();
	if(!m_pTimeLineView)
	{
		m_pTimeLineView = new CTimeLineView();
		m_pTimeLineView->Create(NULL, NULL, DS_SETFONT | WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDD_VIEW_TIMELINE);
		//-- 2013-09-27 hongsu@esmlab.com
		//-- Draw And Drop
		//m_pTimeLineView->InitDropTarget(m_pTimeLineView);

		m_pTimeLineView->SetDrawSec(FALSE);
	}
	else
		return FALSE;


#ifdef _TEST_MODE
	MakeProcessor();
	m_tTimerEx.Stop();
	//	자동 실행 될 함수 설정
	m_tTimerEx.SetTimedEvent(this, &CTimeLineProcessor::UpdateProcessor);
	//	1. Interval in ms
	//	2. TRUE to call first event immediately
	//	3. TRUE to call timed event only once
	m_tTimerEx.Start(1500, FALSE, FALSE);
#endif


	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTimeLineProcessor::OnDestroy()
{
	m_tTimerEx.Stop();
}

void CTimeLineProcessor::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_MAKEMOVIE			,	
		ID_IMAGE_MAKEMOVIE_3D		,
		//ID_SEPARATOR				,
		//ID_IMAGE_MOVIEPLAY			,
		//ID_SEPARATOR				,
		//ID_IMAGE_SCALING			,
		//ID_SEPARATOR				,
		//ID_IMAGE_ITEMLEFTMOVE		,
		//ID_IMAGE_ITEMRIGHTMOVE		,
		//ID_SEPARATOR				,
		//ID_IMAGE_ITEMDELETEALL		,
		////CMiLRe 20151013 Template Load INI File
		//ID_IMAGE_ITEMTEMPLATE_REFRESH,
		////CMiLRe 20151014 Template Save INI File
		//ID_IMAGE_ITEMTEMPLATE_SAVE,
	};

	//VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	GetClientRect(m_rcClientFrame);	
	//RepositionBars(0,0xFFFF,0);	

	//m_wndToolBar.ShowWindow(SW_SHOW);	
}

void CTimeLineProcessor::OnSize(UINT nType, int cx, int cy) 
{
	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	//m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;
	//RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width()		< FRAME_MIN_WIDTH || 
	    m_rcClientFrame.Height()	< FRAME_MIN_HEIGHT)
		return;

	//-- ReloadFrame
	UpdateFrames();
	CDialog::OnSize(nType, cx, cy);	
} 

void CTimeLineProcessor::OnPaint()
{	
	UpdateFrames();
	CDialog::OnPaint();
}

void CTimeLineProcessor::UpdateFrames()
{
	//-- Rounding Dialog
	CPaintDC dc(this);	

	CRect rect;
	rect.CopyRect(m_rcClientFrame);

	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), SRCCOPY);

	CString strFilePath;
	mDC.FillRect(rect, &CBrush(COLOR_BASIC_BG_2));
	
	//-- Frame List
	if(m_pTimeLineView)
	{
		m_pTimeLineView->ShowWindow(SW_SHOW);
		m_pTimeLineView->MoveWindow(rect);
		m_pTimeLineView->Invalidate(FALSE);	
	}	

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();
}




//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-01
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void  CTimeLineProcessor::OnDSCSCaling(void)				
{
	if(m_pTimeLineView)
		m_pTimeLineView->m_bScaleToWindow = !m_pTimeLineView->m_bScaleToWindow;
	m_pTimeLineView->DrawEditor();
}

void CTimeLineProcessor::OutputImage(CString strFileName, int nCurFrameIndex)
{
	IplImage* pImage;
	CString strDSC;

	pImage = new IplImage;	
	pImage = cvCreateImage(cvSize(1920, 1080), IPL_DEPTH_8U, 3);

	FFmpegManager ffMpegMgr(FALSE);
	ffMpegMgr.GetCaptureImage(strFileName, pImage, nCurFrameIndex);

	CString strTel1 = ESMGetCeremonyInfoString(_T("InputTel"));
	char* pstrTel = NULL;
	pstrTel = ESMUtil::CStringToChar(strTel1);

	char* pIamgePath = ESMUtil::CStringToChar(ESMGetCeremony(ESM_VALUE_CEREMONYIMAGEPATH));

	char* strSavePath = new char[MAX_PATH];
	sprintf(strSavePath, "%s\\%s.jpg", pIamgePath, pstrTel);
	//cvSaveImage(strSavePath,pImage);
	delete[] pstrTel;
	delete[] pIamgePath;


	if(pImage)
		cvReleaseImage( &pImage);
}


void CTimeLineProcessor::ItemAllDelete()
{
	m_pTimeLineView->RemoveAll();
	m_pTimeLineView->DrawEditor();
}

void  CTimeLineProcessor::OnDSCMakeMovie(void)				
{
	//Test Button
	MakeProcessor();
}

void  CTimeLineProcessor::OnDSCMakeMovie3D(void)				
{
	//Test Button
	UpdateProcessor();
}

void  CTimeLineProcessor::OnDSCMoviePlay()				
{		
	//-- 2013-10-04 hongsu@esmlab.com
	//-- Play Movie File
	CString strFile;
	strFile.Format(_T("\"%s\\"),ESMGetPath(ESM_PATH_OUTPUT));
	if(!m_pTimeLineView->m_bCreate3D)
		strFile.Append(_T("2D\\"));
	else
		strFile.Append(_T("3D\\"));

	//-- 2013-10-09 jaehyun@esmlab.com
	//-- Output Type option
	// 	BOOL bOutputMP4 = ESMGetValue(ESM_VALUE_ADJ_OUT_MP4);
	// 	if(bOutputMP4)
	// 		strFile.Append(_T("4dmaker.mp4\""));
	// 	else
	// 		strFile.Append(_T("4dmaker.ts\""));
	strFile.Append(_T("4dmaker.mp4\""));


	////////////////////////////////////////////////////////////////////////
	//	2014.11.04	Ryumin

	CString strTmp(strFile);
	strTmp.Replace(_T("\""), _T(""));
	FFmpegManager	mpegMgr(FALSE);
	int nRunningTime = mpegMgr.GetRunningTime(strTmp);

	ESMLog(5,_T("Movie Play2 [%s]"),strFile);
	ShellExecute(NULL ,_T("open"), strFile, NULL, NULL, SW_SHOWNORMAL);

	HANDLE hFocusObserver = NULL;
	hFocusObserver = (HANDLE) _beginthreadex(NULL, 0, FocusObserverThread, (void *)this, 0, NULL);
	CloseHandle(hFocusObserver);

	//	타이머 이벤트를 이용하여 인터벌 후 함수 자동 실행
	MovieStop(_T("PotPlayer"), nRunningTime * 1 , 4500);

	//
	////////////////////////////////////////////////////////////////////////

}

void  CTimeLineProcessor::MovieStop(const CString& strPlayerName, UINT RunningTime, UINT nInterval)
{
	//	타이머 이벤트를 이용하여 입력 받은 인터벌 후 함수 자동 실행
	m_tTimerEx.Stop();
	//	자동 실행 될 함수 설정
	m_tTimerEx.SetTimedEvent(this, &CTimeLineProcessor::SendMovieStopMessage);
	//	1. Interval in ms
	//	2. TRUE to call first event immediately
	//	3. TRUE to call timed event only once
	m_tTimerEx.Start(RunningTime + nInterval, FALSE, TRUE);
}

void  CTimeLineProcessor::SendMovieStopMessage()
{
	DWORD	dwError = -1;
	HWND	hWnd	= NULL;
	hWnd	= ::FindWindow(_T("PotPlayer"), _T("4DMaker.mp4 - 다음 팟플레이어"));
	if(!hWnd)
		hWnd	= ::FindWindow(_T("PotPlayer64"), _T("4DMaker.mp4 - 다음 팟플레이어"));
	//	팟 플레이어 메인 핸들에 F4 키 메세지 전달
	//	플레이이 중 또는 플레이가 끝난 동영상을 닫는다.
	::PostMessage(hWnd, WM_KEYDOWN, VK_F4, 0x003E0001);
	Sleep(10);
	::SendMessage(hWnd, WM_COMMAND, 0x000127B7, 0x00000000);
	Sleep(10);
	::PostMessage(hWnd, WM_KEYUP, VK_F4, 0xC03E0001);

	dwError = GetLastError();

}
//CMiLRe 20151119 영상 저장 경로 변경
void CTimeLineProcessor::DSCMoviePlay(CString strPath)
{	
	DWORD nCurrentTime;
	FFmpegManager	mpegMgr(FALSE);
	int nRunningTime = mpegMgr.GetRunningTime(strPath);
	if(nRunningTime < 0)
		nRunningTime = 0;

	ESMLog(5,_T("Movie Play1 [%s]"),strPath);

	while(1)
	{
		nCurrentTime = GetTickCount();
		if(nCurrentTime > (m_nMakeTime + m_nRunningTime))
		{
			ShellExecute(NULL ,_T("open"), strPath, NULL, NULL, SW_SHOWNORMAL);
			break;
		}
		//ESMLog(1,_T("Wait Time %d %d"), nCurrentTime, m_nMakeTime+m_nRunningTime);
		Sleep(100);
	}

	HANDLE hFocusObserver = NULL;
	hFocusObserver = (HANDLE) _beginthreadex(NULL, 0, FocusObserverThread, (void *)this, 0, NULL);
	CloseHandle(hFocusObserver);

	m_nMakeTime = GetTickCount();
	m_nRunningTime = nRunningTime;
	//	타이머 이벤트를 이용하여 인터벌 후 함수 자동 실행
	MovieStop(_T("PotPlayer"), nRunningTime * 1 , 4500);
}

unsigned WINAPI CTimeLineProcessor::FocusObserverThread(LPVOID param)
{	
	CTimeLineProcessor* pTimeLineProcessor = (CTimeLineProcessor*)param;
	HWND hWnd = NULL;
	int nRepeatCount =500;
	while(nRepeatCount > 0)
	{
		HWND hWnd = NULL;
		hWnd = ::FindWindow(_T("PotPlayer"), NULL);
		HWND hWnd2 = NULL;
		hWnd2 = ::GetForegroundWindow(); 
		if( hWnd == hWnd2)
		{
			HWND hSlectorWnd = pTimeLineProcessor->m_pMainWnd->m_wndDSCViewer.m_pFrameSelector->m_hWnd;
 			::SetFocus(hSlectorWnd);
 			::SetForegroundWindow(hSlectorWnd);
 			::SetActiveWindow(hSlectorWnd);
			break;
		}
		Sleep(10);
		nRepeatCount--;
	}
	return 0;
}

//-- 2013-12-12 kjpark@esmlab.com
//-- if converting, toolbar disenable
void CTimeLineProcessor::SetToolbarShowNHide(BOOL isShow)
{
	m_wndToolBar.EnableWindow(isShow);
}

int CTimeLineProcessor::GetIPList()
{	
	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);	
	CESMIni ini;

	int nIndex = 0;

	if(ini.SetIniFilename (strFile))
	{
		CString strIPSection;
		CString strIP;
		
		while(1)
		{
			strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);			
			strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);	
			if(!strIP.GetLength())
				break;		
			nIndex++;	
			m_IPList.push_back(strIP);
		}
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		{
			CString csLocal = ESMUtil::GetLocalIPAddress();
			m_IPList.push_back(csLocal);
		}
	}

	return nIndex;
}

void CTimeLineProcessor::MakeProcessor()
{
	int nIndex = GetIPList();

	for(int i=0; i < nIndex; i++)
	{
		MakeProcessor(i);
	}

	int nUpdate = m_pTimeLineView->GetObjCount();
		
	for(int i=0; i < nUpdate; i++)
	{
		CString strPCInfo;
		CESMTimeLineObjectEditor* pObjEdit = m_pTimeLineView->GetObj(i);
		pObjEdit->SetBK(TIMELINE_BACKGROUND_NON);
		//pObjEdit->SetDrawTime(FALSE);

		
		strPCInfo = m_IPList.at(i);
		/*int nTmp = strPCInfo.ReverseFind('.') + 1;
		strPCInfo = strPCInfo.Mid(nTmp, strPCInfo.GetLength());*/
		pObjEdit->SetMakePC(strPCInfo);
		pObjEdit->SetFrameCnt(10);
		CString strFile;
		strFile.Format(_T("%d.mp4"), i);
		pObjEdit->SetMovieFile(strFile);
		pObjEdit->DrawObject();
	}
	CString strMsg = ESMGetFrameRecord();
	//m_pTimeLineView->SetDrawStatus(_T("Initializing..."));
	m_pTimeLineView->SetDrawStatus(strMsg);
	m_pTimeLineView->DrawEditor();
}

void CTimeLineProcessor::MakeProcessor(int nIndex, int nNextTime/* = movie_next_frame_time*/)
{
	CObArray arDSC;
	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	
	CESMTimeLineObjectEditor* pObjEdit = new CESMTimeLineObjectEditor(arDSC, nIndex*1000, (nIndex+1)*1000, movie_next_frame_time);
	pObjEdit->SetProcessMode(TRUE);
	pObjEdit->SetNextTime(nNextTime);
	
	m_pTimeLineView->m_nInsertObjPos = m_pTimeLineView->GetObjCount();
	m_pTimeLineView->AddObj(pObjEdit, TRUE);
	
}

void CTimeLineProcessor::UpdateProcessor()
{
#ifndef _TEST_MODE
	static int nSelect = 0;
	CESMTimeLineObjectEditor* pObjEdit = m_pTimeLineView->GetObj(nSelect++);
	if(pObjEdit != NULL)
	{
		int nBK = pObjEdit->GetBK();

		if(nBK == TIMELINE_BACKGROUND_ALL)
			pObjEdit->SetBK(TIMELINE_BACKGROUND_NON);
		else if(nBK == TIMELINE_BACKGROUND_NON)
			pObjEdit->SetBK(TIMELINE_BACKGROUND_READY);
		else
			pObjEdit->SetBK(TIMELINE_BACKGROUND_ALL);
		pObjEdit->DrawObject();
	}
	if(nSelect > m_pTimeLineView->GetObjCount())
		nSelect = 0;
#endif
}

void CTimeLineProcessor::DeleteProcessor()
{
	//EnterCriticalSection(&m_CriSection);
	g_MutexUI.Lock();

	m_nStatus = 0;
	m_bLoadPlay = FALSE;

	m_pTimeLineView->SetDrawStatus(_T(""));
	m_pTimeLineView->RemoveAll();
	m_pTimeLineView->DrawEditor();

	//LeaveCriticalSection(&m_CriSection);
	g_MutexUI.Unlock();
}

void CTimeLineProcessor::MakeProcessor(ESMEvent * pMsg, int nStatus/* = TIMELINE_BACKGROUND_NON*/)
{
	//EnterCriticalSection(&m_CriSection);
	g_MutexUI.Lock();

	int nId = pMsg->nParam1;
	int nFrameCnt = pMsg->nParam2;
	int nMakeFile = pMsg->nParam3;

	//170211 hjcho
	CString *pstrListUp = (CString*)pMsg->pParam;
	CString strListUp;
	strListUp.Format(_T("%s"),*pstrListUp);

	CString strIp, strFile;
	strIp.Format(_T("%d"), nId);
	//if(ESMGetValue(ESM_VALUE_AJAONOFF))
	//	strFile.Format(_T("%s\\%d.mp4"), strListUp,nMakeFile);
	//else
		strFile.Format(_T("%d.mp4"),nMakeFile);

	int nIdx = m_pTimeLineView->GetObjCount();
	MakeProcessor(nIdx);
		
	CESMTimeLineObjectEditor* pObjEdit = m_pTimeLineView->GetObj(nIdx);
	

	pObjEdit->SetBK(nStatus);

	pObjEdit->SetMakePC(strIp);
	pObjEdit->SetFrameCnt(nFrameCnt);
	pObjEdit->SetMovieFile(strFile);

	pObjEdit->DrawObject();

	CString strMsg = ESMGetFrameRecord();
	//m_pTimeLineView->SetDrawStatus(_T("Initializing..."));
	m_pTimeLineView->SetDrawStatus(strMsg);
	m_pTimeLineView->DrawEditor();

	//LeaveCriticalSection(&m_CriSection);
	g_MutexUI.Unlock();
}

void CTimeLineProcessor::UpdateProcessor(ESMEvent * pMsg)
{
	EnterCriticalSection(&m_CriSection);
	g_MutexUI.Lock();

	int nId = pMsg->nParam1;
	int nFrameCnt = pMsg->nParam2;
	int nMakeFile = pMsg->nParam3;
	int nMakingNum = pMsg->pParam;

	CString strTime;
	CString *pPath = new CString;
	pPath = (CString *) pMsg->pDest;
	strTime.Format(_T("%s"),*pPath);

	CString strIp, strFile;
	strIp.Format(_T("%d"), nId);
	//if(ESMGetValue(ESM_VALUE_AJAONOFF))
	//	strFile.Format(_T("%s\\%d.mp4"), strTime,nMakeFile);
	//else
		strFile.Format(_T("%d.mp4"),nMakeFile);

	//ESMLog(5, _T("UpdateProcessor : %d, %s"), nId, strFile);
	//기존과 중복 체크 = PC정보 & File정보 교차 체크 / 정보 갱신때 사용 예정
	for(int i=0; i < m_pTimeLineView->GetObjCount(); i++)
	{
		CESMTimeLineObjectEditor* pObjEdit = m_pTimeLineView->GetObj(i);
		CString strOldIp = pObjEdit->GetMakePC();
		if(strOldIp == strIp)	//PC 정보 체크
		{
			CString strOldFile;// = pObjEdit->GetMovieFile();
			strOldFile.Format(_T("%s"), pObjEdit->GetMovieFile());
			if(strOldFile == strFile) //File 정보 체크
			{
				pObjEdit->SetBK(TIMELINE_BACKGROUND_FINISH);
				
				//pObjEdit->SetMakePC(strIp);
				//pObjEdit->SetFrameCnt(nFrameCnt);
				//pObjEdit->SetMovieFile(strFile);

				pObjEdit->DrawObject();
				m_nStatus++;

				ESMEvent * pMsg = new ESMEvent();
				pMsg->message = WM_ESM_PROCESSOR_LOAD;			//ADD

				CString * pPath =new CString;
				CString * pTime = new CString;
				pPath->Format(_T("%s"), strFile);
				pTime->Format(_T("%s"), strTime);

				pMsg->pParam = (LPARAM)pPath;
				pMsg->nParam1 = i;
				pMsg->nParam2 = m_pTimeLineView->GetObjCount();
				pMsg->pDest = (LPARAM) pTime;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		
				//ESMLog(5, _T("MakeProcssor Update : %d"), i);
				break;
			}
			/*else
			{
			if(pObjEdit->GetBK() == TIMELINE_BACKGROUND_FINISH)
			m_nStatus++;
			}*/
		}/*else
		 {
		 if(pObjEdit->GetBK() == TIMELINE_BACKGROUND_FINISH)
		 m_nStatus++;
		 }*/
	}

	//if( 100 * m_nStatus / m_pTimeLineView->GetObjCount() >= 100)
	//{
	//	if(!m_bLoadPlay)

	//	{
	//		m_bLoadPlay = TRUE;

	//		ESMEvent * pMsg = new ESMEvent();
	//		pMsg->message = WM_ESM_PROCESSOR_LOAD_PLAY;			//PLAY
	//		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	//	}
	//}
	//LeaveCriticalSection(&m_CriSection);
	g_MutexUI.Unlock();
}

void CTimeLineProcessor::UpdateFailProcessor(ESMEvent * pMsg)
{
	//EnterCriticalSection(&m_CriSection);
	g_MutexUI.Lock();

	int nId = pMsg->nParam1;
	int nFrameCnt = pMsg->nParam2;
	int nMakeFile = pMsg->nParam3;

	CString strIp, strFile;
	strIp.Format(_T("%d"), nId);
	strFile.Format(_T("%d.mp4"), nMakeFile);

	//기존과 중복 체크 = PC정보 & File정보 교차 체크 / 정보 갱신때 사용 예정
	for(int i=0; i < m_pTimeLineView->GetObjCount(); i++)
	{
		CESMTimeLineObjectEditor* pObjEdit = m_pTimeLineView->GetObj(i);
		//CString strOldIp = pObjEdit->GetMakePC();
		//if(strOldIp == strIp)	//PC 정보 체크
		{
			CString strOldFile = pObjEdit->GetMovieFile();
			if(strOldFile == strFile) //File 정보 체크
			{
				pObjEdit->SetBK(TIMELINE_BACKGROUND_ALL);

				//pObjEdit->SetMakePC(strIp);
				//pObjEdit->SetFrameCnt(nFrameCnt);
				//pObjEdit->SetMovieFile(strFile);

				pObjEdit->DrawObject();
				//ESMLog(5, _T("MakeProcssor Fail : %d"), i);

				ESMEvent * pMsg = new ESMEvent();
				pMsg->message = WM_ESM_PROCESSOR_LOAD_STOP;			//STOP
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
				break;
			}
		}
	}

	//LeaveCriticalSection(&m_CriSection);
	g_MutexUI.Unlock();
}
void CTimeLineProcessor::DSCMoviePlay(CESMTimeLineObjectEditor* pObject)
{
	CString strPath = pObject->GetMovieFile();
	//AfxMessageBox(strPath);

	if(strPath.IsEmpty())
	{
		ESMLog(5, _T("DSCMoviePlay - Not found files"));
		return;
	}
	if(pObject->GetBK() == TIMELINE_BACKGROUND_FINISH)
	{
		//ProcessMode
		//Play or SDI Out
		CESMFileOperation fo;
		CString strMoviePath;
		//if(ESMGetValue(ESM_VALUE_AJAONOFF))
		//	strMoviePath.Format(_T("%s\\AJA\\%s"),ESMGetClientRamPath(), strPath);
		//else
			strMoviePath.Format(_T("%s\\%s"),ESMGetClientRamPath(),strPath);
		//AfxMessageBox(strMoviePath);

		DSCMoviePlay(strMoviePath);
	}
	
}
	