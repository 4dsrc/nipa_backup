////////////////////////////////////////////////////////////////////////////////
//
//	ESMTimeLineObject.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMTimeLineObjectEditor.h"
#include "resource.h"

#include "DSCViewDefine.h"
#include "DSCItem.h"
#include "ESMCtrl.h"
#include "ESMFileOperation.h"
#include "ESMImgMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMTimeLineObjectEditor::Make3DMovie(int nIndex, vector<CString>* arrAdjedFile, CRITICAL_SECTION* CR_ADJProcess)
{
	if (m_bClose) return;

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Set Start Make Movie
	SetMakeMovie(FALSE);

	m_nObjectIndex = nIndex;
	m_arrAdjedFile = arrAdjedFile;
	m_CR_ADJProcess = CR_ADJProcess;

	if( m_hMakeObjMovie != NULL)
	{
		CloseHandle(m_hMakeObjMovie);
		m_hMakeObjMovie = NULL;
	}
	m_hMakeObjMovie = (HANDLE) _beginthreadex(NULL, 0, MakeObj3DMovieThread, (void *)this, 0, NULL);
}

unsigned WINAPI CESMTimeLineObjectEditor::MakeObj3DMovieThread(LPVOID param)
{
	CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)param;
	if(!pObj) return 0;
	if(pObj->m_bClose) return 0;

	CESMFileOperation fo;
	CESMImgMgr* pImgMgr = new CESMImgMgr();
	ESM3DInfo info3D;
	//-- Get Info
	//-- Start DSC : Time
	//-- End DSC : Time
	int nDSCCnt				= pObj->GetDSCCount();
	int nStartTime			= pObj->GetStartTime();
	int nEndTime			= pObj->GetEndTime();
	int nNextTime			= pObj->GetNextTime();

	ESMLog(5,_T("[Object] Create TS File on Object [DSC:%d][Time : Start[%d] End[%d]"),nDSCCnt, nStartTime, nEndTime);

	int nIndex;
	int nSec, nMilli, nTargetTime;
	//-- Folder
	CString strTargetFolder, strTargetFile;
	//-- 0 : Left  ||  1 : right
	CString strAdjFolder[2], strSrcFolder[2], strAdjFile[2], strSrcFile[2];		
	CDSCItem* pItem = NULL;		

	strTargetFolder.Format(_T("%s\\%d"), ESMGetPath(ESM_PATH_OBJECT), pObj->m_nObjectIndex);
	::CreateDirectory(strTargetFolder, NULL);
	fo.Delete(strTargetFolder,FALSE);

	//-- Copy frame%d.jpg
	//-- 1. StartItem == EndItem
	if(!nDSCCnt)
	{
		ESMLog(0,_T("Non Exist DSC ID on Make Movie"));
		_endthread();
		return 0;
	}


	//-- Shared Information from View
	CvPoint2D32f ptLeftTop, ptRightBottom;
	double dbMaxAngle;
	ptLeftTop.x		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_LEFT	);
	ptLeftTop.y		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_TOP	);
	ptRightBottom.x	= ESMGetFValue(ESM_VALUE_ADJ_VIEW_RIGHT	);
	ptRightBottom.y	= ESMGetFValue(ESM_VALUE_ADJ_VIEW_BOTTOM);
	dbMaxAngle		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_MA	);		
	//-- Each Info from DSC
	CvPoint2D32f ptAdjust, ptRotate;
	double dbAngleAdjust = 0.0, dAdjustSize = 0.0, dAdjustDistance = 0.0;

	//dbMaxAngle = 0;
	dbAngleAdjust = 0;
	//-- Target Numbering
	nIndex = 1;

	//-- 2013-10-08 hongsu@esmlab.com
	//-- Reset Image Manager 
	pImgMgr->ResetAdjust(pObj->m_nObjectIndex, ESM_ADJ_KIND_POSITION);

	//-- Check Normal, Reverse
	int nTime = nEndTime - nStartTime;
	BOOL bNormal;
	if(nTime > 0)
		bNormal = TRUE;
	else
		bNormal = FALSE;

	ESMLog(5,_T("[Object] Create Adjusted Images ..."));
	//-------------------------------------------------------------------
	//--
	//-- SAME DSC (One Line)
	//--
	//-------------------------------------------------------------------
	if(nDSCCnt == 1)
	{
		//-- 2013-10-07 hongsu@esmlab.com
		//-- Get 3D Information 									
		pItem	= pObj->GetDSC(0);
		info3D.strDSC[1] = pItem->GetDeviceDSCID();
		ESMGet3DInfo(&info3D);
	
		nTargetTime = nStartTime;
		for(int nPos = 0 ; nPos < 2 ; nPos ++)
		{
			//-- 2013-01-02 kcd
			//-- Folder Location Modify
			//strSrcFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_FRAME), info3D.strDSC[nPos]);
			strSrcFolder[nPos].Format(_T("%s\\%s\\%s"),ESMGetDataPath(pItem->GetDSCInfo(DSC_INFO_LOCATION)), ESMGetFrameRecord(), info3D.strDSC[nPos]);
			strAdjFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_ADJUST), info3D.strDSC[nPos]);
			fo.CreateFolder(strAdjFolder[nPos]);
		}

		//-- 2013-10-07 hongsu@esmlab.com
		//-- Make Adjust Files 
		while(1)
		{
			ESMGetMovieTime(nTargetTime, nSec, nMilli);

			for(int nPos = 0 ; nPos < 2 ; nPos ++)
			{
				strSrcFile[nPos].Format(_T("%s\\%d.%03d.jpg"),strSrcFolder[nPos], nSec, nMilli);
				strAdjFile[nPos].Format(_T("%s\\%d.%03d.jpg"),strAdjFolder[nPos], nSec, nMilli);
			}
			if(!fo.IsFileExist(strSrcFile[0]) || !fo.IsFileExist(strSrcFile[1]) )
			{
				//-- 2013-10-19 hongsu@esmlab.com
				//-- Find Next Frame
				if(bNormal) ESMGetNextFrameTime(nTargetTime);						
				else		ESMGetPreviousFrameTime(nTargetTime);

				if(nEndTime < nTargetTime)
					break;

				continue;					
			}
				
			//-- 2013-10-07 hongsu@esmlab.com
			//-- Create Adjust File 
			for( int nPos = 0 ; nPos < 2 ; nPos ++)
			{
				if(!fo.IsFileExist(strAdjFile[nPos]))
				{
					ptAdjust.x		= info3D.nAdj[nPos][DSC_ADJ_X];
					ptAdjust.y		= info3D.nAdj[nPos][DSC_ADJ_Y];
					dbAngleAdjust	= info3D.nAdj[nPos][DSC_ADJ_A];
					ptRotate.x		= info3D.nAdj[nPos][DSC_ADJ_RX];
					ptRotate.y		= info3D.nAdj[nPos][DSC_ADJ_RY];
					dAdjustSize		= info3D.nAdj[nPos][DSC_ADJ_SCALE];
					dAdjustDistance	= info3D.nAdj[nPos][DSC_ADJ_DISTANCE];
					if(!pObj->CheckAdjustFile(pObj->m_CR_ADJProcess, pObj->m_arrAdjedFile, strAdjFile[nPos]))
						continue;

					pImgMgr->SetAdjust(strSrcFile[nPos], strAdjFile[nPos], _T(""), ptAdjust, ptLeftTop,  ptRightBottom, dbAngleAdjust,dAdjustSize, dAdjustDistance, dbMaxAngle, ptRotate, info3D.strDSC[nPos], TRUE);
				}
			}


			if(nEndTime == nTargetTime)
				break;
			if(bNormal)
				ESMGetNextFrameTime(nTargetTime);						
			else
				ESMGetPreviousFrameTime(nTargetTime);
			nIndex++;
		}		

		//-- 2013-10-07 hongsu@esmlab.com
		//-- Waiting until Finish Making Adjustment
		while(1)
		{
			if(pImgMgr->IsFinishAdjust())
				break;
			Sleep(ESM_ADJ_CHEK_TIME);
		}

		ESMLog(5,_T("[Object] Create 3D File on Object [DSC:%d]"),pObj->m_nObjectIndex);

		//-- 2013-10-08 hongsu@esmlab.com
		//-- Reset Image Manager 
		pImgMgr->ResetAdjust(pObj->m_nObjectIndex, ESM_ADJ_KIND_CREATE3D);

		//-- 2013-10-07 hongsu@esmlab.com
		//-- Make Target Frame 			
		//-- 2013-10-07 hongsu@esmlab.com
		//-- Get 3D Information 									
		pItem	= pObj->GetDSC(0);
		info3D.strDSC[1] = pItem->GetDeviceDSCID();
		ESMGet3DInfo(&info3D);
		nTargetTime = nStartTime;
		for(int nPos = 0 ; nPos < 2 ; nPos ++)
		{
			//-- 2013-01-02 kcd
			//-- Folder Location Modify
			//strSrcFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_FRAME), info3D.strDSC[nPos]);
			strSrcFolder[nPos].Format(_T("%s\\%s\\%s"), ESMGetDataPath(pItem->GetDSCInfo(DSC_INFO_LOCATION)), ESMGetFrameRecord(), info3D.strDSC[nPos]);
			strAdjFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_ADJUST), info3D.strDSC[nPos]);
			fo.CreateFolder(strAdjFolder[nPos]);
		}


		nIndex = 1;
		while(1)
		{
			ESMGetMovieTime(nTargetTime, nSec, nMilli);

			for(int nPos = 0 ; nPos < 2 ; nPos ++)
				strAdjFile[nPos].Format(_T("%s\\%d.%03d.jpg"),strAdjFolder[nPos], nSec, nMilli);

			if(!fo.IsFileExist(strAdjFile[0]) || !fo.IsFileExist(strAdjFile[1]) )
			{
				//-- 2013-10-19 hongsu@esmlab.com
				//-- Find Next Frame
				if(bNormal) ESMGetNextFrameTime(nTargetTime);						
				else		ESMGetPreviousFrameTime(nTargetTime);

				if(nEndTime < nTargetTime)
					break;	

				continue;					
			}				
			strTargetFile.Format(_T("%s\\frame%04d.jpg"),strTargetFolder, nIndex++);
				
			//-- Create 3D File
			pImgMgr->Create3DFile(strAdjFile[0], strAdjFile[1], strTargetFile);

			if(nEndTime == nTargetTime)
				break;				
			if(bNormal)
				ESMGetNextFrameTime(nTargetTime);						
			else
				ESMGetPreviousFrameTime(nTargetTime);				
		}

		//-- 2013-10-07 hongsu@esmlab.com
		//-- Waiting until Finish Making Adjustment
		while(1)
		{
			if(pImgMgr->IsFinishAdjust())
				break;
			Sleep(ESM_ADJ_CHEK_TIME);
		}
	}

		
	//--------------------------------------------------------------------------
	//-- 2013-10-01 hongsu@esmlab.com
	//-- One More Selected DSC 
	//--------------------------------------------------------------------------
	else
	{
		//-- Check Normal, Reverse
		int nTime = nEndTime - nStartTime;
		//-------------------------------------------------------------------
		//--
		//-- SAME TIME (One Line)
		//--
		//-------------------------------------------------------------------			
		if(!nTime)
		{
			nTargetTime = nStartTime;
			ESMGetMovieTime(nTargetTime,nSec,nMilli);

			for(int i = 0 ; i < nDSCCnt ; i ++)
			{
				//-- 2013-10-07 hongsu@esmlab.com
				//-- Get 3D Information 											
				pItem	= pObj->GetDSC(i);
				info3D.strDSC[1] = pItem->GetDeviceDSCID();
				ESMGet3DInfo(&info3D);
					
				for(int nPos = 0 ; nPos < 2 ; nPos ++)
				{
					//-- 2013-01-02 kcd
					//-- Folder Location Modify
					//strSrcFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_FRAME), info3D.strDSC[nPos]);
					strSrcFolder[nPos].Format(_T("%s\\%s\\%s"),ESMGetDataPath(pItem->GetDSCInfo(DSC_INFO_LOCATION)), ESMGetFrameRecord(), info3D.strDSC[nPos]);

					strAdjFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_ADJUST), info3D.strDSC[nPos]);
					strSrcFile	[nPos].Format(_T("%s\\%d.%03d.jpg"),strSrcFolder[nPos], nSec, nMilli);
					strAdjFile	[nPos].Format(_T("%s\\%d.%03d.jpg"),strAdjFolder[nPos], nSec, nMilli);
					fo.CreateFolder(strAdjFolder[nPos]);
				}		

				if(!fo.IsFileExist(strSrcFile[0]) || !fo.IsFileExist(strSrcFile[1]))
					continue;

				//-- 2013-10-07 hongsu@esmlab.com
				//-- Create Adjust File 
 				for( int nPos = 0 ; nPos < 2 ; nPos ++)
 				{
					if(!fo.IsFileExist(strAdjFile[nPos]))
					{
						ptAdjust.x		= info3D.nAdj[nPos][DSC_ADJ_X];
						ptAdjust.y		= info3D.nAdj[nPos][DSC_ADJ_Y];
						dbAngleAdjust	= info3D.nAdj[nPos][DSC_ADJ_A];
						ptRotate.x		= info3D.nAdj[nPos][DSC_ADJ_RX];
						ptRotate.y		= info3D.nAdj[nPos][DSC_ADJ_RY];
						dAdjustSize		= info3D.nAdj[nPos][DSC_ADJ_SCALE];	
						dAdjustDistance	= info3D.nAdj[nPos][DSC_ADJ_DISTANCE];	
						if(!pObj->CheckAdjustFile(pObj->m_CR_ADJProcess, pObj->m_arrAdjedFile, strAdjFile[nPos]))
							continue;
						pImgMgr->SetAdjust(strSrcFile[nPos], strAdjFile[nPos], _T(""), ptAdjust, ptLeftTop,  ptRightBottom, dbAngleAdjust, dAdjustSize, dAdjustDistance, dbMaxAngle, ptRotate, info3D.strDSC[nPos], TRUE);
					}
				}			
			}

			//-- 2013-10-07 hongsu@esmlab.com
			//-- Waiting until Finish Making Adjustment
			while(1)
			{
				if(pImgMgr->IsFinishAdjust())
					break;
				Sleep(ESM_ADJ_CHEK_TIME);
			}

			ESMLog(5,_T("[Object] Create 3D File on Object [DSC:%d]"),pObj->m_nObjectIndex);

			//-- 2013-10-08 hongsu@esmlab.com
			//-- Reset Image Manager 
			pImgMgr->ResetAdjust(pObj->m_nObjectIndex, ESM_ADJ_KIND_CREATE3D);
			nTargetTime = nStartTime;
			ESMGetMovieTime(nTargetTime,nSec,nMilli);

			//-- 2013-10-07 hongsu@esmlab.com
			//-- Make Target Frame 
			nIndex = 1;
			for(int i = 0 ; i < nDSCCnt ; i ++)
			{
				
				//-- 2013-10-07 hongsu@esmlab.com
				//-- Get 3D Information 											
				pItem	= pObj->GetDSC(i);
				info3D.strDSC[1] = pItem->GetDeviceDSCID();
				ESMGet3DInfo(&info3D);

				for(int nPos = 0 ; nPos < 2 ; nPos ++)
				{
					strAdjFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_ADJUST), info3D.strDSC[nPos]);						
					strAdjFile[nPos].Format(_T("%s\\%d.%03d.jpg"),strAdjFolder[nPos], nSec, nMilli);
					fo.CreateFolder(strAdjFolder[nPos]);
				}

				if(!fo.IsFileExist(strAdjFile[0]) || !fo.IsFileExist(strAdjFile[1]))
					continue;

				strTargetFile.Format(_T("%s\\frame%04d.jpg"),strTargetFolder, nIndex++);
				//-- thread
				pImgMgr->Create3DFile(strAdjFile[0], strAdjFile[1], strTargetFile);
			}	
			//-- 2013-10-07 hongsu@esmlab.com
			//-- Waiting until Finish Making Adjustment
			while(1)
			{
				if(pImgMgr->IsFinishAdjust())
					break;
				Sleep(ESM_ADJ_CHEK_TIME);
			}
		}
		//-------------------------------------------------------------------
		//--
		//-- LINE (time + dsc)
		//-- 
		//-------------------------------------------------------------------			
		else
		{
			BOOL bNormal;
			if(nTime > 0)
				bNormal = TRUE;
			else
				bNormal = FALSE;


			int nTimeCnt = abs(ESMGetCntMovieTime(nTime));				
			float fBase = 0.0, fCalc = 0.0;				
			int nTimeIndex = 0, nDSCIndex = 0;

			//-------------------------------------------------------------------
			//--
			//-- NORMAL LINE (DSC Count > Frame Time Count)
			//--
			//--    □
			//--	□
			//--	 □
			//--	 □
			//--	  □
			//--	  □
			//--
			//-------------------------------------------------------------------	
			if(nTimeCnt <= nDSCCnt )
			{
				fBase = (float)((float)nTimeCnt / (float)nDSCCnt);
				nTargetTime = nStartTime;
				nTimeIndex	= 0;
				//-- 2013-10-05 hongsu@esmlab.com
				//-- Based on Time 
				for(int i = 0 ; i < nDSCCnt ; i ++)
				{
					fCalc = (float)((float)i*fBase);
					fCalc = (float)((int)(fCalc + 0.5));		//반올림

					if((int)fCalc > nTimeIndex)
					{
						nTimeIndex++;
						if(bNormal)
							ESMGetNextFrameTime(nTargetTime);						
						else
							ESMGetPreviousFrameTime(nTargetTime);
					}

					//-- 2013-10-05 hongsu@esmlab.com
					//-- Check Last
					if(i == nDSCCnt -1)
						nTargetTime = nEndTime;

					//-- Get nSec, nMilli
					ESMGetMovieTime(nTargetTime,nSec,nMilli);

					//-- 2013-10-07 hongsu@esmlab.com
					//-- Get 3D Information 						
					pItem	= pObj->GetDSC(i);
					info3D.strDSC[1] = pItem->GetDeviceDSCID();
					ESMGet3DInfo(&info3D);

					for(int nPos = 0 ; nPos < 2 ; nPos ++)
					{

						//-- 2013-01-02 kcd
						//-- Folder Location Modify
						//strSrcFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_FRAME), info3D.strDSC[nPos]);
						strSrcFolder[nPos].Format(_T("%s\\%s\\%s"),ESMGetDataPath(pItem->GetDSCInfo(DSC_INFO_LOCATION)), ESMGetFrameRecord(), info3D.strDSC[nPos]);
						strAdjFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_ADJUST), info3D.strDSC[nPos]);
						strSrcFile	[nPos].Format(_T("%s\\%d.%03d.jpg"),strSrcFolder[nPos], nSec, nMilli);
						strAdjFile	[nPos].Format(_T("%s\\%d.%03d.jpg"),strAdjFolder[nPos], nSec, nMilli);
						fo.CreateFolder(strAdjFolder[nPos]);
					}		

					if(!fo.IsFileExist(strSrcFile[0]) || !fo.IsFileExist(strSrcFile[1]))
						continue;

					//-- 2013-10-07 hongsu@esmlab.com
					//-- Create Adjust File 
 					for( int nPos = 0 ; nPos < 2 ; nPos ++)
 					{
						if(!fo.IsFileExist(strAdjFile[nPos]))
						{
							ptAdjust.x		= info3D.nAdj[nPos][DSC_ADJ_X];
							ptAdjust.y		= info3D.nAdj[nPos][DSC_ADJ_Y];
							dbAngleAdjust	= info3D.nAdj[nPos][DSC_ADJ_A];
							ptRotate.x		= info3D.nAdj[nPos][DSC_ADJ_RX];
							ptRotate.y		= info3D.nAdj[nPos][DSC_ADJ_RY];		
							dAdjustSize		= info3D.nAdj[nPos][DSC_ADJ_SCALE];
							dAdjustDistance	= info3D.nAdj[nPos][DSC_ADJ_DISTANCE];
							if(!pObj->CheckAdjustFile(pObj->m_CR_ADJProcess, pObj->m_arrAdjedFile, strAdjFile[nPos]))
								continue;
							pImgMgr->SetAdjust(strSrcFile[nPos], strAdjFile[nPos], _T(""), ptAdjust, ptLeftTop,  ptRightBottom, dbAngleAdjust, dAdjustSize, dAdjustDistance, dbMaxAngle, ptRotate, info3D.strDSC[nPos], TRUE);
						}
					}
				}

				//-- 2013-10-07 hongsu@esmlab.com
				//-- Waiting until Finish Making Adjustment
				while(1)
				{
					if(pImgMgr->IsFinishAdjust())
						break;
					Sleep(ESM_ADJ_CHEK_TIME);
				}

				ESMLog(5,_T("[Object] Create 3D File on Object [DSC:%d]"),pObj->m_nObjectIndex);
				//-- 2013-10-08 hongsu@esmlab.com
				//-- Reset Image Manager 
				pImgMgr->ResetAdjust(pObj->m_nObjectIndex, ESM_ADJ_KIND_CREATE3D);
				//-- 2013-10-07 hongsu@esmlab.com
				//-- Make Target Frame 
				fBase = (float)((float)nTimeCnt / (float)nDSCCnt);
				nTargetTime = nStartTime;
				nTimeIndex	= 0;

				nIndex = 1;
				for(int i = 0 ; i < nDSCCnt ; i ++)
				{
					fCalc = ((float)i*fBase);
					fCalc = (float)((int)(fCalc + 0.5));		//반올림

					if((int)fCalc > nTimeIndex)
					{
						nTimeIndex++;
						if(bNormal)
							ESMGetNextFrameTime(nTargetTime);						
						else
							ESMGetPreviousFrameTime(nTargetTime);
					}

					//-- 2013-10-05 hongsu@esmlab.com
					//-- Check Last
					if(i == nDSCCnt -1)
						nTargetTime = nEndTime;

					//-- Get nSec, nMilli
					ESMGetMovieTime(nTargetTime,nSec,nMilli);

					pItem	= pObj->GetDSC(i);
					info3D.strDSC[1] = pItem->GetDeviceDSCID();
					ESMGet3DInfo(&info3D);

					for(int nPos = 0 ; nPos < 2 ; nPos ++)
					{
						strAdjFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_ADJUST), info3D.strDSC[nPos]);						
						strAdjFile[nPos].Format(_T("%s\\%d.%03d.jpg"),strAdjFolder[nPos], nSec, nMilli);
						fo.CreateFolder(strAdjFolder[nPos]);
					}		

					if(!fo.IsFileExist(strAdjFile[0]) || !fo.IsFileExist(strAdjFile[1]))
						continue;

					strTargetFile.Format(_T("%s\\frame%04d.jpg"),strTargetFolder, nIndex++);
					//-- thread
					pImgMgr->Create3DFile(strAdjFile[0], strAdjFile[1], strTargetFile);
				}	
				//-- 2013-10-07 hongsu@esmlab.com
				//-- Waiting until Finish Making Adjustment
				while(1)
				{
					if(pImgMgr->IsFinishAdjust())
						break;
					Sleep(ESM_ADJ_CHEK_TIME);
				}
			}
			//-------------------------------------------------------------------
			//--
			//-- NORMAL LINE (DSC Count < Frame Time Count)
			//--
			//--    □
			//--		□
			//--			□
			//--				□
			//--					□
			//--						□
			//--
			//-------------------------------------------------------------------	
			else
			{
				//-- 2013-10-08 hongsu@esmlab.com
				//-- Reset Image Manager 
				pImgMgr->ResetAdjust(pObj->m_nObjectIndex, ESM_ADJ_KIND_POSITION);

				fBase = (float)((float)nDSCCnt / (float)nTimeCnt);
				nTargetTime = nStartTime;
				nDSCIndex	= 0;

				pItem	= pObj->GetDSC(nDSCIndex);
				info3D.strDSC[1] = pItem->GetDeviceDSCID();
				ESMGet3DInfo(&info3D);
					
				//-- 2013-10-05 hongsu@esmlab.com
				//-- Based on Time 
				for(int i = 0 ; i < nTimeCnt ; i ++)
				{
					fCalc = i*fBase;
					fCalc = (float)((int)(fCalc + 0.5));		//반올림

					if((int)fCalc > nDSCIndex)
					{
						nDSCIndex++;
						if(nDSCIndex >= nDSCCnt)
							nDSCIndex = nDSCCnt-1;
						pItem	= pObj->GetDSC(nDSCIndex);
						info3D.strDSC[1] = pItem->GetDeviceDSCID();
						ESMGet3DInfo(&info3D);
					}

					//-- 2013-10-05 hongsu@esmlab.com
					//-- Check Last
					if(i == nTimeCnt -1)
					{
						pItem = pObj->GetLastDSC();
						info3D.strDSC[1] = pItem->GetDeviceDSCID();
						ESMGet3DInfo(&info3D);
					}

					//-- Get This Time
					ESMGetMovieTime(nTargetTime,nSec,nMilli);

					//-- Set Next Time
					if(bNormal)	ESMGetNextFrameTime(nTargetTime);						
					else		ESMGetPreviousFrameTime(nTargetTime);


					for(int nPos = 0 ; nPos < 2 ; nPos ++)
					{

						//-- 2013-01-02 kcd
						//-- Folder Location Modify
						//strSrcFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_FRAME), info3D.strDSC[nPos]);
						strSrcFolder[nPos].Format(_T("%s\\%s\\%s"),ESMGetDataPath(pItem->GetDSCInfo(DSC_INFO_LOCATION)), ESMGetFrameRecord(), info3D.strDSC[nPos]);

						strAdjFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_ADJUST), info3D.strDSC[nPos]);
						strSrcFile	[nPos].Format(_T("%s\\%d.%03d.jpg"),strSrcFolder[nPos], nSec, nMilli);
						strAdjFile	[nPos].Format(_T("%s\\%d.%03d.jpg"),strAdjFolder[nPos], nSec, nMilli);
						fo.CreateFolder(strAdjFolder[nPos]);
					}		

					if(!fo.IsFileExist(strSrcFile[0]) || !fo.IsFileExist(strSrcFile[1]))
						continue;

					//-- 2013-10-07 hongsu@esmlab.com
					//-- Create Adjust File 
					for( int nPos = 0 ; nPos < 2 ; nPos ++)
					{
						if(!fo.IsFileExist(strAdjFile[nPos]))
						{
							ptAdjust.x		= info3D.nAdj[nPos][DSC_ADJ_X];
							ptAdjust.y		= info3D.nAdj[nPos][DSC_ADJ_Y];
							dbAngleAdjust	= info3D.nAdj[nPos][DSC_ADJ_A];
							ptRotate.x		= info3D.nAdj[nPos][DSC_ADJ_RX];
							ptRotate.y		= info3D.nAdj[nPos][DSC_ADJ_RY];		
							dAdjustSize		= info3D.nAdj[nPos][DSC_ADJ_SCALE];
							dAdjustDistance	= info3D.nAdj[nPos][DSC_ADJ_DISTANCE];	
							if(!pObj->CheckAdjustFile(pObj->m_CR_ADJProcess, pObj->m_arrAdjedFile, strAdjFile[nPos]))
								continue;
							pImgMgr->SetAdjust(strSrcFile[nPos], strAdjFile[nPos], _T(""), ptAdjust, ptLeftTop,  ptRightBottom, dbAngleAdjust, dAdjustSize, dAdjustDistance, dbMaxAngle, ptRotate, info3D.strDSC[nPos], TRUE);
						}
					}				
				}

				//-- 2013-10-07 hongsu@esmlab.com
				//-- Waiting until Finish Making Adjustment
				while(1)
				{
					if(pImgMgr->IsFinishAdjust())
						break;
					Sleep(ESM_ADJ_CHEK_TIME);
				}

				ESMLog(5,_T("[Object] Create 3D File on Object [DSC:%d]"),pObj->m_nObjectIndex);

				//-- 2013-10-08 hongsu@esmlab.com
				//-- Reset Image Manager 
				pImgMgr->ResetAdjust(pObj->m_nObjectIndex, ESM_ADJ_KIND_CREATE3D);
				fBase = (float)((float)nDSCCnt / (float)nTimeCnt);
				nTargetTime = nStartTime;
				nDSCIndex	= 0;

				pItem	= pObj->GetDSC(nDSCIndex);
				info3D.strDSC[1] = pItem->GetDeviceDSCID();
				ESMGet3DInfo(&info3D);
					
				//-- 2013-10-07 hongsu@esmlab.com
				//-- Make Target Frame 
				nIndex = 1;
				for(int i = 0 ; i < nTimeCnt ; i ++)
				{
					fCalc = i*fBase;
					fCalc = (float)((int)(fCalc + 0.5));		//반올림

					if((int)fCalc > nDSCIndex)
					{
						nDSCIndex++;
						if(nDSCIndex >= nDSCCnt)
							nDSCIndex = nDSCCnt-1;
						pItem	= pObj->GetDSC(nDSCIndex);
						info3D.strDSC[1] = pItem->GetDeviceDSCID();
						ESMGet3DInfo(&info3D);
					}

					//-- 2013-10-05 hongsu@esmlab.com
					//-- Check Last
					if(i == nTimeCnt -1)
					{
						pItem = pObj->GetLastDSC();
						info3D.strDSC[1] = pItem->GetDeviceDSCID();
						ESMGet3DInfo(&info3D);
					}

					//-- Get This Time
					ESMGetMovieTime(nTargetTime,nSec,nMilli);

					//-- Set Next Time
					if(bNormal)	ESMGetNextFrameTime(nTargetTime);						
					else		ESMGetPreviousFrameTime(nTargetTime);

					for(int nPos = 0 ; nPos < 2 ; nPos ++)
					{
						strAdjFolder[nPos].Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_ADJUST), info3D.strDSC[nPos]);						
						strAdjFile[nPos].Format(_T("%s\\%d.%03d.jpg"),strAdjFolder[nPos], nSec, nMilli);
						fo.CreateFolder(strAdjFolder[nPos]);
					}

					if(!fo.IsFileExist(strAdjFile[0]) || !fo.IsFileExist(strAdjFile[1]))
						continue;
						
					strTargetFile.Format(_T("%s\\frame%04d.jpg"),strTargetFolder, nIndex++);						
					//-- thread
					pImgMgr->Create3DFile(strAdjFile[0], strAdjFile[1], strTargetFile);
				}	
				//-- 2013-10-07 hongsu@esmlab.com
				//-- Waiting until Finish Making Adjustment
				while(1)
				{
					if(pImgMgr->IsFinishAdjust())
						break;
					Sleep(ESM_ADJ_CHEK_TIME);
				}
			}				
		}	
	}		

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Make ts File
	//-- ffmpeg -i frame%d.jpg output.ts

	CString strCmd;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));

	CString strOpt, strOpt1, strOpt2;	
	int nFps = 1000 / nNextTime;
	strOpt1.Format(_T("-r %d -i \"%s\\"),nFps, strTargetFolder);
	strOpt1.Append(_T("frame%04d.jpg\" -q:v 1 "));
	strOpt2.Format(_T("\"%s\\%d.ts"),strTargetFolder,pObj->m_nObjectIndex);	 

	strOpt.Format(_T("%s %s"),strOpt1, strOpt2);

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Log 
	ESMLog(5,_T("%s %s"),strCmd, strOpt);

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;
	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);
	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}

	ESMLog(5,_T("[Object][%d] Finish Create 3D Movie File [%s]"),pObj->m_nObjectIndex, strTargetFolder);		

	if(pImgMgr)
	{
		delete pImgMgr;
		pImgMgr = NULL;
	}


	pObj->SetMakeMovie(TRUE);
	
	//-- 2013-10-22 hongsu@esmlab.com
	//-- Reset Handle 
	pObj->m_hMake3DMovie = NULL;	
	return 0;
}


void CESMTimeLineObjectEditor::MakePicture3DMovie(int nIndex, vector<CString>* arrAdjedFile, CRITICAL_SECTION* CR_ADJProcess)
{
	if (m_bClose) return;

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Set Start Make Movie
	SetMakeMovie(FALSE);

	m_nObjectIndex = nIndex;
	m_arrAdjedFile = arrAdjedFile;
	m_CR_ADJProcess = CR_ADJProcess;

	if( m_hMakeObjMovie != NULL)
	{
		CloseHandle(m_hMakeObjMovie);
		m_hMakeObjMovie = NULL;
	}
	m_hMakeObjMovie = (HANDLE) _beginthreadex(NULL, 0, MakePictureObj3DMovieThread, (void *)this, 0, NULL);
}

unsigned WINAPI CESMTimeLineObjectEditor::MakePictureObj3DMovieThread(LPVOID param)
{
	CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)param;
	if(!pObj) return 0;
	if(pObj->m_bClose) return 0;

	CESMFileOperation fo;
	CESMImgMgr* pImgMgr = new CESMImgMgr();
	ESM3DInfo info3D;
	//-- Get Info
	//-- Start DSC : Time
	//-- End DSC : Time
	int nDSCCnt				= pObj->GetDSCCount();
	int nStartTime			= pObj->GetStartTime();
	int nEndTime			= pObj->GetEndTime();
	int nNextTime			= pObj->GetNextTime();

	ESMLog(5,_T("[Object] Create TS File on Object [DSC:%d][Time : Start[%d] End[%d]"),nDSCCnt, nStartTime, nEndTime);

	int nIndex;
	int nSec, nMilli, nTargetTime;
	//-- Folder
	CString strTargetFolder, strTargetFile, strDSC;
	//-- 0 : Left  ||  1 : right
	CString strAdjFolder[2], strSrcFolder[2], strAdjFile[2], strSrcFile[2];		
	CDSCItem* pItem = NULL;		

	strTargetFolder.Format(_T("%s\\%d"), ESMGetPath(ESM_PATH_OBJECT), pObj->m_nObjectIndex);
	::CreateDirectory(strTargetFolder, NULL);
	fo.Delete(strTargetFolder,FALSE);

	//-- Copy frame%d.jpg
	//-- 1. StartItem == EndItem
	if(!nDSCCnt)
	{
		ESMLog(0,_T("Non Exist DSC ID on Make Movie"));
		_endthread();
		return 0;
	}


	//-- Shared Information from View
	CvPoint2D32f ptLeftTop, ptRightBottom;
	double dbMaxAngle;
	ptLeftTop.x		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_LEFT	);
	ptLeftTop.y		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_TOP	);
	ptRightBottom.x	= ESMGetFValue(ESM_VALUE_ADJ_VIEW_RIGHT	);
	ptRightBottom.y	= ESMGetFValue(ESM_VALUE_ADJ_VIEW_BOTTOM);
	dbMaxAngle		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_MA	);		
	//-- Each Info from DSC
	CvPoint2D32f ptAdjust, ptRotate;
	double dbAngleAdjust = 0.0, dAdjustSize = 0.0, dAdjustDistance = 0.0;

	//dbMaxAngle = 0;
	dbAngleAdjust = 0;
	//-- Target Numbering
	nIndex = 1;

	//-- 2013-10-08 hongsu@esmlab.com
	//-- Reset Image Manager 
	pImgMgr->ResetAdjust(pObj->m_nObjectIndex, ESM_ADJ_KIND_POSITION);

	//-- Check Normal, Reverse
	int nTime = nEndTime - nStartTime;
	BOOL bNormal;
	if(nTime > 0)
		bNormal = TRUE;
	else
		bNormal = FALSE;

	ESMLog(5,_T("[Object] Create Adjusted Images ..."));
	//-------------------------------------------------------------------
	//--
	//-- SAME TIME (One Line)
	//--
	//-------------------------------------------------------------------			
	if(!nTime)
	{
		nTargetTime = nStartTime;
		ESMGetMovieTime(nTargetTime,nSec,nMilli);


		for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
		{
			pItem	= pObj->GetDSC(nIdx);
			info3D.strDSC[1] = pItem->GetDeviceDSCID();
			strDSC	= pItem->GetDeviceDSCID();
			ESMGet3DInfo(&info3D);

			for(int nPos = 0 ; nPos < 2 ; nPos ++)
			{
				strSrcFile[nPos].Format(_T("%s\\%s\\%s.jpg"),ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), info3D.strDSC[nPos]);
				if(!fo.IsFileExist(strSrcFile[nPos]))
					continue;

				strAdjFolder[nPos].Format(_T("%s"),ESMGetPath(ESM_PATH_ADJUST));
				fo.CreateFolder(strAdjFolder[nPos]);
				strAdjFile[nPos].Format(_T("%s\\%s.jpg"),strAdjFolder[nPos], strDSC);
				strTargetFile.Format(_T("%s\\frame%04d.jpg"),strTargetFolder, nIndex);
			}		

			EnterCriticalSection (pObj->m_CR_ADJProcess);
			int nAdjIndex = 0;
			int nAdjCount = pObj->m_arrAdjedFile->size();
			for(nAdjIndex = 0; nAdjIndex < nAdjCount; nAdjIndex++)
			{
				if( pObj->m_arrAdjedFile->at(nAdjIndex) == strAdjFile[0])
					break;
			}
			if( nAdjIndex != nAdjCount)
			{
				for( int i =0 ;i < 1000; i++)
				{
					if(fo.IsFileExist(strAdjFile[0]))
					{
						fo.Copy(strAdjFile[0], strTargetFile);
						break;
					}
					Sleep(100);
				}
				LeaveCriticalSection (pObj->m_CR_ADJProcess);
				nIndex++;
				continue;
			}

			pObj->m_arrAdjedFile->push_back(strAdjFile[0]);
			LeaveCriticalSection (pObj->m_CR_ADJProcess);
			if(!fo.IsFileExist(strSrcFile[0]) || !fo.IsFileExist(strSrcFile[1]))
				continue;

			//-- 2013-10-07 hongsu@esmlab.com
			//-- Create Adjust File 
			//kcd Adjust On time
			//  		for( int nPos = 0 ; nPos < 2 ; nPos ++)
			//  	{
			int nPos = 0;
			if(!fo.IsFileExist(strAdjFile[nPos]))
			{
				ptAdjust.x		= info3D.nAdj[nPos][DSC_ADJ_X];
				ptAdjust.y		= info3D.nAdj[nPos][DSC_ADJ_Y];
				dbAngleAdjust	= info3D.nAdj[nPos][DSC_ADJ_A];
				ptRotate.x		= info3D.nAdj[nPos][DSC_ADJ_RX];
				ptRotate.y		= info3D.nAdj[nPos][DSC_ADJ_RY];
				dAdjustSize		= info3D.nAdj[nPos][DSC_ADJ_SCALE];	
				dAdjustDistance	= info3D.nAdj[nPos][DSC_ADJ_DISTANCE];	
				pImgMgr->SetAdjust(strSrcFile[nPos], strAdjFile[nPos], _T(""), ptAdjust, ptLeftTop,  ptRightBottom, dbAngleAdjust, dAdjustSize, dAdjustDistance, dbMaxAngle, ptRotate, info3D.strDSC[nPos], TRUE);
			}
			//}		
			nIndex++;
		}

		//-- 2013-10-07 hongsu@esmlab.com
		//-- Waiting until Finish Making Adjustment
		while(1)
		{
			if(pImgMgr->IsFinishAdjust())
				break;
			Sleep(ESM_ADJ_CHEK_TIME);
		}

		ESMLog(5,_T("[Object] Create 3D File on Object [DSC:%d]"),pObj->m_nObjectIndex);

		//-- 2013-10-08 hongsu@esmlab.com
		//-- Reset Image Manager 
		pImgMgr->ResetAdjust(pObj->m_nObjectIndex, ESM_ADJ_KIND_CREATE3D);
		nTargetTime = nStartTime;
		ESMGetMovieTime(nTargetTime,nSec,nMilli);

		nIndex = 1;
		for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
		{
			pItem	= pObj->GetDSC(nIdx);
			info3D.strDSC[1] = pItem->GetDeviceDSCID();
			ESMGet3DInfo(&info3D);
			strDSC	= pItem->GetDeviceDSCID();

			for(int nPos = 0 ; nPos < 2 ; nPos ++)
			{
				strAdjFolder[nPos].Format(_T("%s"),ESMGetPath(ESM_PATH_ADJUST));						
				strAdjFile[nPos].Format(_T("%s\\%s.jpg"),strAdjFolder[nPos], info3D.strDSC[nPos]);
				fo.CreateFolder(strAdjFolder[nPos]);
			}

			if(!fo.IsFileExist(strAdjFile[0]) || !fo.IsFileExist(strAdjFile[1]))
				continue;

			strTargetFile.Format(_T("%s\\frame%04d.jpg"),strTargetFolder, nIndex++);
			//-- thread
			pImgMgr->Create3DFile(strAdjFile[0], strAdjFile[1], strTargetFile);
		}	
		//-- 2013-10-07 hongsu@esmlab.com
		//-- Waiting until Finish Making Adjustment
		while(1)
		{
			if(pImgMgr->IsFinishAdjust())
				break;
			Sleep(ESM_ADJ_CHEK_TIME);
		}
	}

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Make ts File
	//-- ffmpeg -i frame%d.jpg output.ts

	CString strCmd;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));

	CString strOpt, strOpt1, strOpt2;	
	int nFps = 1000 / nNextTime;
	strOpt1.Format(_T("-r %d -i \"%s\\"),nFps, strTargetFolder);
	strOpt1.Append(_T("frame%04d.jpg\" -q:v 1 "));
	strOpt2.Format(_T("\"%s\\%d.ts"),strTargetFolder,pObj->m_nObjectIndex);	 

	strOpt.Format(_T("%s %s"),strOpt1, strOpt2);

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Log 
	ESMLog(5,_T("%s %s"),strCmd, strOpt);

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;
	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);
	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}

	ESMLog(5,_T("[Object][%d] Finish Create 3D Movie File [%s]"),pObj->m_nObjectIndex, strTargetFolder);		

	if(pImgMgr)
	{
		delete pImgMgr;
		pImgMgr = NULL;
	}


	pObj->SetMakeMovie(TRUE);

	//-- 2013-10-22 hongsu@esmlab.com
	//-- Reset Handle 
	pObj->m_hMake3DMovie = NULL;	
	return 0;
}

BOOL CESMTimeLineObjectEditor::CheckAdjustFile(CRITICAL_SECTION* pCR_ADJProcess, vector<CString>* pArrAdjedFile, CString strTargetFile)
{
	CESMFileOperation fo;
	EnterCriticalSection (pCR_ADJProcess);
	int nAdjIndex = 0;
	int nAdjCount = pArrAdjedFile->size();
	for(nAdjIndex = 0; nAdjIndex < nAdjCount; nAdjIndex++)
	{
		if( pArrAdjedFile->at(nAdjIndex) == strTargetFile)
			break;
	}
	if( nAdjIndex != nAdjCount)
	{
		for( int i =0 ;i < 1000; i++)
		{
			if(fo.IsFileExist(strTargetFile))
			{
				break;
			}
			Sleep(100);
		}
		LeaveCriticalSection (pCR_ADJProcess);
		return FALSE;
	}

	pArrAdjedFile->push_back(strTargetFile);
	LeaveCriticalSection (pCR_ADJProcess);
	return TRUE;

}