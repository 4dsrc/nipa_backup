////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineViewTimeLine.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-27
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TimeLineView.h"
#include "DSCViewer.h"
#include "ESMTimeLineObjectEditor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


void CTimeLineView::InsertObj(CESMTimeLineObjectEditor* pObj, BOOL bPos)
{
	CESMTimeLineObjectEditor* pNewObj = new CESMTimeLineObjectEditor();


	pNewObj->m_nTime[0]			= pObj->m_nTime[0];
	pNewObj->m_nTime[1]			= pObj->m_nTime[1];
	pNewObj->m_nTime[2]			= pObj->m_nTime[2];

	pNewObj->m_nZoom[0]			= pObj->m_nZoom[0];
	pNewObj->m_nZoom[1]			= pObj->m_nZoom[1];

	pNewObj->m_nStepFrame		= pObj->m_nStepFrame;
	//-- Add List
	pNewObj->m_arDSCinObj.Copy(pObj->m_arDSCinObj);
	

	int iSelectedIndx = 0;
	for(int i =0 ; i < GetObjCount(); i++)
	{
		CESMTimeLineObjectEditor* pObjectEditor = GetObj(i);
		CString strTp;

		if( pObjectEditor->GetBK() == TIMELINE_BACKGROUND_ALL)
		{
			iSelectedIndx = i;
		}
	}

	if(!bPos)
		m_nInsertObjPos = iSelectedIndx;
	else
		m_nInsertObjPos = iSelectedIndx+1;

	AddObj(pNewObj);

	//memcpy(pNewObj,pObj,sizeof(CESMTimeLineObjectEditor));

	/*m_arObject.InsertAt(m_nInsertObjPos,(CObject*)pNewObj);
	{
		
		//-- Update Effect Editor
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_EFFECT_UPDATE;
		pMsg->pParam	= (LPARAM)pNewObj;		
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);
	}*/

	//-- Calculate Total Time
	CalculateTime();
}

void CTimeLineView::ReCalculateTime()
{
	int nSyncStart = 0, nSynEnd = 0;
	int nPreStart, nPreEnd, nGap;
	for(int i = 0; i < m_arObject.GetSize(); i++)
	{
		CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)m_arObject.GetAt(i);
		if( i > 0)
		{
			if(pObj->m_bSyncObj)
			{
				pObj->m_nTime[0] = nSyncStart;
				pObj->m_nTime[1] = nSynEnd; 
			}
			else
			{
				nGap = pObj->m_nTime[1] - pObj->m_nTime[0];
				pObj->m_nTime[0] = nPreEnd;
				pObj->m_nTime[1] = nPreEnd + nGap; 
			}
		}

		nPreStart = pObj->m_nTime[0];
		nPreEnd = pObj->m_nTime[1];

		if(pObj->m_bSyncObj)
		{
			nSyncStart = pObj->m_nTime[0];
			nSynEnd = pObj->m_nTime[1];
		}
	}
}
//------------------------------------------------------------------------------
//! @function	Add Object
//------------------------------------------------------------------------------
void CTimeLineView::AddObj(CESMTimeLineObjectEditor* pAddObj, BOOL bProcess/* = FALSE*/)
{
	CheckOverReset();
	//-- Create Object
	pAddObj->Create(NULL, NULL, DS_SETFONT | WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDD_VIEW_TIMELINE_OBJ);
	//-- Set Select
	pAddObj->SetBK(TIMELINE_BACKGROUND_ALL);
	pAddObj->pParentTimeLineView = this;
	//-- Insert Position
	m_arObject.InsertAt(m_nInsertObjPos,(CObject*)pAddObj);

	//2018.03.19 joonho.kim
	//Auto Calculate Time
	ReCalculateTime();
	

	if(bProcess != TRUE)// jhhan 17-01-04
	{
		//-- 2014-07-15 hongsu@esmlab.com
		//-- Update Effect Editor
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_EFFECT_UPDATE;
		pMsg->pParam	= (LPARAM)pAddObj;		
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);
	}
		

	//-- 2013-10-04 hongsu@esmlab.com
	//-- Calculate Total Time
	CalculateTime();
}

//20151015 Template VMCC Save/Load
#include "ESMObjectFrame.h"
void CTimeLineView::AddObj(CESMTimeLineObjectEditor* pAddObj, CESMTemplateMgr* templateMgr, vector<TEMPLATE_VMCC_STRUCT>* verVMCC_Data)
{
	//18.03.14 joonho.kim AddZoom
	if(verVMCC_Data->size() > 1)
	{
		pAddObj->m_nZoom[0] = verVMCC_Data->at(0).nZoom;
		pAddObj->m_nZoom[1] = verVMCC_Data->at(verVMCC_Data->size()-1).nZoom;
	}
	else
	{
		pAddObj->m_nZoom[0] = 100;
		pAddObj->m_nZoom[1] = 100;
	}


	CheckOverReset();
	//-- Create Object
	pAddObj->Create(NULL, NULL, DS_SETFONT | WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDD_VIEW_TIMELINE_OBJ);
	//-- Set Select
	pAddObj->SetBK(TIMELINE_BACKGROUND_NON);
	pAddObj->pParentTimeLineView = this;
	//-- Insert Position
	m_arObject.InsertAt(m_nInsertObjPos,(CObject*)pAddObj);

	//-- 2014-07-15 hongsu@esmlab.com
	//-- Update Effect Editor
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_EFFECT_UPDATE;
	pMsg->pParam	= (LPARAM)pAddObj;		
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	

	//-- 2013-10-04 hongsu@esmlab.com
	//-- Calculate Total Time
	CalculateTime();
	
	int nCnt = pAddObj->GetObjectFrameCount();
	int nSpotCnt = verVMCC_Data->size();
	pAddObj->InitSpotInfo();
#if 0
	for ( int i=0 ; i<nSpotCnt ; i++)
	{
		int nOrder, nPosX, nPosY, nWidth, nHeight;
		double dScale;
		nOrder =round(verVMCC_Data->at(i).dRatioTime*(double)nCnt);
		if ( nOrder == nCnt )
			nOrder = nCnt - 1;
		if(ESMGetMovieSize())
		{
			dScale = (double)verVMCC_Data->at(i).nZoom / 100.0;
			nWidth = FULL_UHD_WIDTH/dScale;
			nHeight = FULL_UHD_HEIGHT/dScale;

			nPosX =round(verVMCC_Data->at(i).dRatioX*(double)FULL_UHD_WIDTH);
			if((nPosX + nWidth) > FULL_UHD_WIDTH)
				nPosX = FULL_UHD_WIDTH - (nWidth/2);

			nPosY =round(verVMCC_Data->at(i).dRatioY*(double)FULL_UHD_HEIGHT);
			if((nPosY + nHeight) > FULL_UHD_HEIGHT)
				nPosY = FULL_UHD_HEIGHT - (nHeight/2);
		}
		else
		{
			dScale = (double)verVMCC_Data->at(i).nZoom / 100.0;
			nWidth = FULL_HD_WIDTH/dScale;
			nHeight = FULL_HD_HEIGHT/dScale;

			nPosX =round(verVMCC_Data->at(i).dRatioX*(double)FULL_HD_WIDTH);
			if((nPosX + nWidth) > FULL_HD_WIDTH)
				nPosX = FULL_HD_WIDTH - (nWidth/2);
			nPosY =round(verVMCC_Data->at(i).dRatioY*(double)FULL_HD_HEIGHT);
			if((nPosY + nHeight) > FULL_HD_HEIGHT)
				nPosY = FULL_HD_HEIGHT - (nHeight/2);
		}
		

		CESMObjectFrame* pObj = pAddObj->GetObjectFrame(nOrder);
		pObj->SetSpot(TRUE);
		pObj->SetZoom(TRUE);
		pObj->SetPosX(nPosX);
		pObj->SetPosY(nPosY);
		pObj->SetZoomRatio(verVMCC_Data->at(i).nZoom);
		pObj->SetZoomWeight(verVMCC_Data->at(i).dWeight);
		
		TRACE(_T(""));
	}
#else
	for ( int i=0 ; i<nSpotCnt ; i++)
	{
		int nOrder, nPosX, nPosY;
		nOrder =round(verVMCC_Data->at(i).dRatioTime*(double)nCnt);
		if ( nOrder == nCnt )
			nOrder = nCnt - 1;
		
#if 1   //신규 VMCC 기능
		nPosX =round(verVMCC_Data->at(i).dRatioX);
		nPosY =round(verVMCC_Data->at(i).dRatioY);
#else
		nPosX =round(verVMCC_Data->at(i).dRatioX*(double)FULL_HD_WIDTH);
		nPosY =round(verVMCC_Data->at(i).dRatioY*(double)FULL_HD_HEIGHT);
#endif

		CESMObjectFrame* pObj = pAddObj->GetObjectFrame(nOrder);
		pObj->SetSpot(TRUE);
		pObj->SetZoom(TRUE);
		pObj->SetPosX(nPosX);
		pObj->SetPosY(nPosY);
		pObj->SetZoomRatio(verVMCC_Data->at(i).nZoom);
		pObj->SetZoomWeight(verVMCC_Data->at(i).dWeight);

		TRACE(_T(""));
	}
	//CMiLRe 20160202 Effect Spot List Update 안되는 버그 수정
	ESMEvent* pMsgs	= new ESMEvent;
	pMsgs->message	= WM_ESM_EFFECT_SPOTLIST_UPDATE;
	pMsgs->pParam	= (LPARAM)pAddObj;		
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsgs);	
#endif
	//wgkim@esmlab.com	
	if(!ESMGetValue(ESM_VALUE_TEMPLATEPOINT) || 
		!templateMgr->IsTemplatePointValid())
	{
		pAddObj->RecalculateInfo();	

		if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT) && !templateMgr->IsTemplatePointValid())
			ESMLog(5, _T("Have not set Template Point"));
	}
	else	
		pAddObj->RecalculateInfo(templateMgr);	
}

#include "OoverlapFrameCount.h"
BOOL CTimeLineView::OnDrop(CWnd* pWnd, COleDataObject* pDataObject,DROPEFFECT dropEffect, CPoint point )
{
	HGLOBAL  hGlobal;

	//wgkim 190925 클립보드용 자료형 변경
	hGlobal = pDataObject->GetGlobalData(CF_OWNERDISPLAY);
	char* pData = (char*)GlobalLock(hGlobal);
	ASSERT(pData != NULL);

	CESMTimeLineObject* pObj = (CESMTimeLineObject*)atoll(pData);
	CESMTimeLineObjectEditor* pObjEdit = new CESMTimeLineObjectEditor();
	//-- 2013-09-29 hongsu@esmlab.com
	//-- Add Object To Editor 
	pObjEdit->Copy(pObj);

	if ((pObjEdit->GetEndTime() - pObjEdit->GetStartTime() == 0) && pObjEdit->GetDSCCount() == 1)
	{
		COoverlapFrameCount EditorDlg(this);
		if (EditorDlg.DoModal() == IDOK)
		{
			for (int i = 0; i < EditorDlg.m_nFrameCount; i++)
			{
				CESMTimeLineObjectEditor* pObjEdits = new CESMTimeLineObjectEditor();
				pObjEdits->Copy(pObj);
				AddObj(pObjEdits);
				m_nInsertObjPos++;
			}
		}

		delete pObjEdit;
		pObjEdit = NULL;
	}
	else//GetObjectFrameCount
	{
		m_bRecal = TRUE;
		AddObj(pObjEdit);
	}
	//-- ReDraw
	DrawEditor();
	GlobalUnlock(hGlobal);

	return CESMDropTarget::OnDrop(pWnd, pDataObject, dropEffect, point);
	//return TRUE;
}

DROPEFFECT CTimeLineView::OnDragOver(CWnd* pWnd, COleDataObject* 
	pDataObject, DWORD dwKeyState, CPoint point )
{
	//-- 2013-10-01 hongsu@esmlab.com
	//-- Check PT In Rect 
	CheckOverObject(point);
	return DROPEFFECT_COPY;
}


void CTimeLineView::CheckOverReset()
{
	CESMTimeLineObjectEditor* pObject = NULL;		
	int nAll = m_arObject.GetCount();

	for(int i = 0; i < nAll ; i ++)
	{
		pObject = GetObj(i);
		pObject->SetBK(TIMELINE_BACKGROUND_NON);
	}	
}

void CTimeLineView::CheckOverObject(CPoint point)
{
	CESMTimeLineObjectEditor* pObject = NULL;		
	int nAll = m_arObject.GetCount();

	//-- Set Add Object Position
	m_nInsertObjPos = nAll;

	CPoint ptDraw;
	for(int i = 0; i < nAll ; i ++)
	{
		pObject = GetObj(i);

		int nBK = pObject->PtInObject(point);
		pObject->SetBK(nBK);
		pObject->DrawObject();

		switch(nBK)
		{
		case TIMELINE_BACKGROUND_LEFT:
			m_nInsertObjPos = i;
			break;
		case TIMELINE_BACKGROUND_RIGHT:
			m_nInsertObjPos = i+1;
			break;
		}
	}
}

void CTimeLineView::CalculateTime()
{
	CESMTimeLineObjectEditor* pObject = NULL;		
	int nAll = m_arObject.GetCount();
	int nTime = 0, nTotalTime = 0;

	for(int i = 0; i < nAll ; i ++)
	{
		pObject = GetObj(i);
		if(pObject)
		{
			nTime = pObject->GetDuration();
			nTotalTime += nTime;
		}
	}
	SetTimeMax(nTotalTime);	
}

void CTimeLineView::GetMetadata(CString strFile)
{
	FILE *fp;	

	char filename[255];
	sprintf(filename, "%S", strFile);				

	fp = fopen(filename, "rt+");
	
	if(fp == NULL)
	{
		ESMLog(5, _T("[Metadata] File Open Error : %s"), strFile);
		return;
	}	
	
	fseek( fp,  -2047L, SEEK_END);
	long length = ftell(fp);

	if(length <= sizeof(char)*2047)
	{
		fclose(fp);
		return;
	}

	char fileData[2047] = {0};
	fgets(fileData, 2047, fp);	

	m_strTotalMetadata = m_strTotalMetadata + fileData;

	_chsize(fileno(fp), length);

	fclose(fp);	
}
