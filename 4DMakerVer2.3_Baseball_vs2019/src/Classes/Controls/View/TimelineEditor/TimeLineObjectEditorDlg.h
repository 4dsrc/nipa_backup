////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineViewTimeLine.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Ryumin (ryumin@esmlab.com)
// @date	2013-10-9
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "resource.h"
#include "ESMTimeLineObjectEditor.h"
#include "ESMButtonEx.h"
#include "ESMEditorEX.h"

// CTimeLineObjectEditorDlg 대화 상자
class CTimeLineObjectEditorDlg : public CDialogEx
{
	// 생성입니다.
public:
	CTimeLineObjectEditorDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	CTimeLineObjectEditorDlg(CESMTimeLineObjectEditor* pObjectEditor, CWnd* pParent = NULL);

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_TIMELINEOBJECTEDITOR_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


	// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	CESMTimeLineObjectEditor* m_pParentWnd;

	void SetObjectInfo();
	int	 GetTotalDSCIndex(const CString& strDSC, int nCtrlID);
	int	 GetStartTime();
	int	 GetEndTime();
	int	 CheckTime(const CString& strTime);
	int	 CheckTime(int nTime);
	int	 GetKzoneIndex();

public:
	int  GetStartDSCIndex();
	int  GetEndDSCIndex();
	int  GetStartNFT();

public:

	afx_msg void OnEnKillfocusEditStarttime();
	afx_msg void OnBnClickedOk();
	afx_msg void OnEnKillfocusEditEndtime();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CBrush m_background;
	CESMButtonEx m_btnOK;
	CESMButtonEx m_btnCancel;
	CESMEditorEX m_editStartTime;
	CESMEditorEX m_editEndTime;
	CESMEditorEX m_editStartZoom;
	CESMEditorEX m_editEndZoom;
	CESMEditorEX m_editNextTimeFrame;
	CESMEditorEX m_editStepFrame;
	CESMEditorEX m_editKZoneIdx;
};
