////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineViewTimeLine.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Ryumin (ryumin@esmlab.com)
// @date	2013-10-9
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TimeLineObjectEditorDlg.h"
#include "afxdialogex.h"

#include "ESMFunc.h"
#include "DSCItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CTimeLineObjectEditorDlg 대화 상자

CTimeLineObjectEditorDlg::CTimeLineObjectEditorDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTimeLineObjectEditorDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CTimeLineObjectEditorDlg::CTimeLineObjectEditorDlg(CESMTimeLineObjectEditor* pObjectEditor, CWnd* pParent /*=NULL*/)
	: CDialogEx(CTimeLineObjectEditorDlg::IDD, pParent)
{
	if (!pObjectEditor) return;
	if (!pObjectEditor->GetSafeHwnd()) return;

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pParentWnd = pObjectEditor;
}

void CTimeLineObjectEditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_EDIT_STARTTIME, m_editStartTime);
	DDX_Control(pDX, IDC_EDIT_ENDTIME, m_editEndTime);
	DDX_Control(pDX, IDC_EDIT_STARTZOOM, m_editStartZoom);
	DDX_Control(pDX, IDC_EDIT_ENDZOOM, m_editEndZoom);
	DDX_Control(pDX, IDC_EDIT_NTF, m_editNextTimeFrame);
	DDX_Control(pDX, IDC_EDIT_STEP_FRAME, m_editStepFrame);
	DDX_Control(pDX, IDC_EDIT_KZONE_IDX, m_editKZoneIdx);
}

BEGIN_MESSAGE_MAP(CTimeLineObjectEditorDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_EN_KILLFOCUS(IDC_EDIT_STARTTIME,			OnEnKillfocusEditStarttime)
	ON_EN_KILLFOCUS(IDC_EDIT_ENDTIME,			OnEnKillfocusEditEndtime)
	ON_BN_CLICKED(IDOK,							OnBnClickedOk)	
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CTimeLineObjectEditorDlg 메시지 처리기

BOOL CTimeLineObjectEditorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	SetObjectInfo();
	m_background.CreateSolidBrush(RGB(44,44,44));

	m_btnOK.ChangeFont(15);
	m_btnCancel.ChangeFont(15);

	
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CTimeLineObjectEditorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialogEx::OnSysCommand(nID, lParam);
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CTimeLineObjectEditorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CTimeLineObjectEditorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CTimeLineObjectEditorDlg::SetObjectInfo()
{
	//	Check control
	//	Start Time (ms)
	CWnd* pStrtEdit = GetDlgItem(IDC_EDIT_STARTTIME);
	if (!pStrtEdit) return;
	//	End Time (ms)
	CWnd* pEndEdit = GetDlgItem(IDC_EDIT_ENDTIME);
	if (!pEndEdit) return;
	//	Next Frame Time (10-1000 ms)
	CWnd* pNTFEdit = GetDlgItem(IDC_EDIT_NTF);
	if (!pNTFEdit) return;
	//	Start DSC
	CWnd* pcbStartDSC = GetDlgItem(IDC_COMBO_STARTDSC);
	if (!pcbStartDSC) return;
	//	End DSC
	CWnd* pcbEndDSC = GetDlgItem(IDC_COMBO_ENDDSC);
	if (!pcbEndDSC) return;
	// Kzone Index
	CWnd* pKzoneIdx = GetDlgItem(IDC_EDIT_KZONE_IDX);//161026 hjcho
	if (!pKzoneIdx) return;
	// Start Zoom 
	CWnd* pStartZoom = GetDlgItem(IDC_EDIT_STARTZOOM);
	if(!pStartZoom) return;
	// End Zoom 
	CWnd* pEndZoom = GetDlgItem(IDC_EDIT_ENDZOOM);
	if(!pEndZoom) return;
	// Count
	CWnd* pStepFrame = GetDlgItem(IDC_EDIT_STEP_FRAME);
	if(!pStepFrame) return;

	//	Spin Control
	int nSavedTime = ESMGetDSCSavedTime();
	int nTimeStart = m_pParentWnd->GetStartTime();
	int nTimeEnd = m_pParentWnd->GetEndTime();
	int nKzoneIdx = m_pParentWnd->GetKzoneIdx();//161024 hjcho
	int nZoomStart = m_pParentWnd->GetStartZoom();
	int nZoomEnd = m_pParentWnd->GetEndZoom();
	int nStepFrame = m_pParentWnd->GetStepFrame();


	//	Input Data
	CString nTemp(_T(""));
	if (!m_pParentWnd->GetSafeHwnd()) return;
	//	Start Time (ms)	
	nTemp.Format(_T("%d"),nTimeStart);
	TRACE(nTemp);
	((CEdit*)pStrtEdit)->SetWindowText(nTemp);
	//	End Time (ms)	
	nTemp.Format(_T("%d"), nTimeEnd);
	((CEdit*)pEndEdit)->SetWindowText(nTemp);
	//	Start Zoom (%)	
	nTemp.Format(_T("%d"),nZoomStart);
	TRACE(nTemp);
	((CEdit*)pStartZoom)->SetWindowText(nTemp);
	//	End Zoom (%)	
	nTemp.Format(_T("%d"), nZoomEnd);
	((CEdit*)pEndZoom)->SetWindowText(nTemp);
	//	Step Frame
	nTemp.Format(_T("%d"), nStepFrame);
	((CEdit*)pStepFrame)->SetWindowText(nTemp);
	//	Next Frame Time (10-1000 ms)
	nTemp.Format(_T("%d"), m_pParentWnd->GetNextTime());
	((CEdit*)pNTFEdit)->SetWindowText(nTemp);

	//	KzonePrism Index 161024 hjcho
	nTemp.Format(_T("%d"), nKzoneIdx);
	((CEdit*)pKzoneIdx)->SetWindowText(nTemp);

	//	Get DSC Full List
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem*	pItem = NULL;
	CString		strID(_T(""));

	//	Set Start/End DSC
	int nCnt = arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nCnt; ++nIdx)
	{
		pItem	= (CDSCItem*)arDSCList.GetAt(nIdx);
		if (!pItem) break;
		strID	= pItem->GetDeviceDSCID();
		if (strID.IsEmpty()) break;

		((CComboBox*)pcbStartDSC)->InsertString(nIdx, strID);
		((CComboBox*)pcbEndDSC)->InsertString(nIdx, strID);
	}
	arDSCList.RemoveAll();

	//	Set Default
	CString strStart = m_pParentWnd->GetDSCID(0);
	int nIndex = GetTotalDSCIndex(strStart, IDC_COMBO_STARTDSC);
	((CComboBox*)pcbStartDSC)->SetCurSel(nIndex);
	CString strEnd = m_pParentWnd->GetDSCID( m_pParentWnd->GetDSCCount() -1 );
	nIndex = GetTotalDSCIndex(strEnd, IDC_COMBO_ENDDSC);
	((CComboBox*)pcbEndDSC)->SetCurSel(nIndex);
}

int CTimeLineObjectEditorDlg::GetTotalDSCIndex(const CString& strDSC, int nCtrlID)
{
	CWnd* pcbDSC = GetDlgItem(nCtrlID);
	if (!pcbDSC) return 0;

	int nCnt = ((CComboBox*)pcbDSC)->GetCount();
	for (int nIdx = 0; nIdx < nCnt; ++nIdx)
	{
		CString strTemp(_T(""));
		((CComboBox*)pcbDSC)->GetLBText(nIdx, strTemp);
		if (strDSC.Compare(strTemp) == 0)
			return nIdx;
	}
	return 0;
}

void CTimeLineObjectEditorDlg::OnEnKillfocusEditStarttime()
{
	CString strTime(_T(""));

	CWnd* pStartEdit = GetDlgItem(IDC_EDIT_STARTTIME);
	((CEdit*)pStartEdit)->GetWindowText(strTime);
	if (strTime.IsEmpty())
	{
		AfxMessageBox(_T("Please enter Start Time."));
		strTime.Format(_T("%d"), m_pParentWnd->GetStartTime());
		return;
	}
	int nEnd = GetStartTime();
	//	End Time (ms)
	strTime.Format(_T("%d"), nEnd);
	((CEdit*)pStartEdit)->SetWindowText(strTime);
}

void CTimeLineObjectEditorDlg::OnEnKillfocusEditEndtime()
{
	CString strTime(_T(""));

	CWnd* pEndEdit = GetDlgItem(IDC_EDIT_ENDTIME);
	((CEdit*)pEndEdit)->GetWindowText(strTime);
	if (strTime.IsEmpty())
	{
		AfxMessageBox(_T("Please enter End Time."));
		strTime.Format(_T("%d"), m_pParentWnd->GetEndTime());
		return;
	}
	int nEnd = GetEndTime();
	//	End Time (ms)
	strTime.Format(_T("%d"), nEnd);
	((CEdit*)pEndEdit)->SetWindowText(strTime);
}

int CTimeLineObjectEditorDlg::CheckTime(const CString& strTime)
{
	if (strTime.IsEmpty()) return -1;

	CString strTemp(strTime);
	wchar_t* pszTemp = strTemp.GetBuffer(strTemp.GetLength());
	int nTime = _ttoi64(pszTemp);
	ESMCheckFrameTime(nTime);
	strTemp.ReleaseBuffer();
	return nTime;
}


int CTimeLineObjectEditorDlg::GetStartTime()
{
	CString strTime(_T(""));
	CWnd* pStartEdit = GetDlgItem(IDC_EDIT_STARTTIME);
	((CEdit*)pStartEdit)->GetWindowText(strTime);
	return CheckTime(strTime);
}

int CTimeLineObjectEditorDlg::GetEndTime()
{
	CString strTime(_T(""));
	CWnd* pEndEdit = GetDlgItem(IDC_EDIT_ENDTIME);
	((CEdit*)pEndEdit)->GetWindowText(strTime);
	return CheckTime(strTime);
}

int CTimeLineObjectEditorDlg::GetStartDSCIndex()
{
	CWnd* pcbStartDSC = GetDlgItem(IDC_COMBO_STARTDSC);
	return ((CComboBox*)pcbStartDSC)->GetCurSel();
}

int CTimeLineObjectEditorDlg::GetEndDSCIndex()
{
	CWnd* pcbEndDSC = GetDlgItem(IDC_COMBO_ENDDSC);
	return ((CComboBox*)pcbEndDSC)->GetCurSel();
}

int CTimeLineObjectEditorDlg::GetStartNFT()
{
	CString strTime(_T(""));
	CWnd* pNTFEdit = GetDlgItem(IDC_EDIT_NTF);
	((CEdit*)pNTFEdit)->GetWindowText(strTime);
	wchar_t* pszTemp = strTime.GetBuffer(strTime.GetLength());
	strTime.ReleaseBuffer();
	return _ttoi64(pszTemp);
}

int CTimeLineObjectEditorDlg::GetKzoneIndex()
{
	CString KzoneIdx(_T(""));
	CWnd* pStartEdit = GetDlgItem(IDC_EDIT_KZONE_IDX);
	((CEdit*)pStartEdit)->GetWindowText(KzoneIdx);
	int kidx = _ttoi64(KzoneIdx);
	return kidx;
}

void CTimeLineObjectEditorDlg::OnBnClickedOk()
{
	CString strData(_T(""));
	//	Start Time (ms)
	CWnd* pStrtEdit = GetDlgItem(IDC_EDIT_STARTTIME);
	if (!pStrtEdit) return;
	((CEdit*)pStrtEdit)->GetWindowText(strData);
	wchar_t* pszTemp = strData.GetBuffer(strData.GetLength());
	int nTime = _ttoi64(pszTemp);
	//-- 2014-07-17 hongsu@esmlab.com
	//-- Check Time 
	ESMCheckFrameTime(nTime);
	m_pParentWnd->SetStartTime(nTime);
	strData.ReleaseBuffer();

	//	End Time (ms)
	CWnd* pEndEdit = GetDlgItem(IDC_EDIT_ENDTIME);
	if (!pEndEdit) return;
	((CEdit*)pEndEdit)->GetWindowText(strData);
	pszTemp = strData.GetBuffer(strData.GetLength());
	nTime = _ttoi64(pszTemp);
	//-- 2014-07-17 hongsu@esmlab.com
	//-- Check Time 
	ESMCheckFrameTime(nTime);
	m_pParentWnd->SetEndTime(nTime);
	strData.ReleaseBuffer();

	//	Start Zoom (%)
	CWnd* pStrtZoomEdit = GetDlgItem(IDC_EDIT_STARTZOOM);
	if (!pStrtZoomEdit) return;
	((CEdit*)pStrtZoomEdit)->GetWindowText(strData);
	pszTemp = strData.GetBuffer(strData.GetLength());
	int nZoom = _ttoi64(pszTemp);
	m_pParentWnd->SetStartZoom(nZoom);
	strData.ReleaseBuffer();

	//	End Zoom (%)
	CWnd* pEndZoomEdit = GetDlgItem(IDC_EDIT_ENDZOOM);
	if (!pEndZoomEdit) return;
	((CEdit*)pEndZoomEdit)->GetWindowText(strData);
	pszTemp = strData.GetBuffer(strData.GetLength());
	nZoom = _ttoi64(pszTemp);
	m_pParentWnd->SetEndZoom(nZoom);
	strData.ReleaseBuffer();

	//	Count
	CWnd* pCount = GetDlgItem(IDC_EDIT_STEP_FRAME);
	if (!pCount) return;
	((CEdit*)pCount)->GetWindowText(strData);
	pszTemp = strData.GetBuffer(strData.GetLength());
	int nCount = _ttoi64(pszTemp);
	m_pParentWnd->SetStepFrame(nCount);
	strData.ReleaseBuffer();

	//	Next Frame Time (10-1000 ms)
	CWnd* pNTFEdit = GetDlgItem(IDC_EDIT_NTF);
	if (!pNTFEdit) return;
	((CEdit*)pNTFEdit)->GetWindowText(strData);
	pszTemp = strData.GetBuffer(strData.GetLength());
	nTime = _ttoi64(pszTemp);
	m_pParentWnd->SetNFT(nTime);
	strData.ReleaseBuffer();

	//	Start DSC
	CWnd* pcbStartDSC = GetDlgItem(IDC_COMBO_STARTDSC);
	if (!pcbStartDSC) return;
	int nStartDSC = ((CComboBox*)pcbStartDSC)->GetCurSel();
	//	End DSC
	CWnd* pcbEndDSC = GetDlgItem(IDC_COMBO_ENDDSC);
	if (!pcbEndDSC) return;
	int nEndDSC = ((CComboBox*)pcbEndDSC)->GetCurSel();

	m_pParentWnd->SetDSC(nStartDSC, nEndDSC);

	//K-zone Index
	CWnd* pcbKzoneIdx = GetDlgItem(IDC_EDIT_KZONE_IDX);
	if(!pcbKzoneIdx) return;
	((CEdit*)pcbKzoneIdx)->GetWindowText(strData);
	pszTemp = strData.GetBuffer(strData.GetLength());
	int Kzoneidx = _ttoi64(pszTemp);
	m_pParentWnd->SetKzoneIdx(Kzoneidx);

	CDialogEx::OnOK();
}



HBRUSH CTimeLineObjectEditorDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
	
	hbr = (HBRUSH)m_background;
	switch(nCtlColor)
	{
	case CTLCOLOR_STATIC:
		{

			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(56, 56, 56));  // 글자 배경색 변경
			pDC->SetBkMode(TRANSPARENT);
			//return (HBRUSH)m_brush;
		}
		break;
	case CTLCOLOR_EDIT:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0,0,0));
			//hbr = (HBRUSH)(hbr.GetSafeHandle());  
		}
		break;
	case CTLCOLOR_LISTBOX:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0,0,0));
			pDC->SetBkMode(TRANSPARENT);
		}
		break;
	}    
	return hbr;
}
