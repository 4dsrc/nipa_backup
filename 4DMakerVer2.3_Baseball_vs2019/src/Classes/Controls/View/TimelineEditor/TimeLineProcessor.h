////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineProcessor.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	
// @Date	
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include <vector>
#include "ToolTipListCtrl.h"

//-- 2011-07-27 hongsu.jung
#include "TimeLineView.h"

#include "ESMDropTarget.h"
#include "ESMDropSource.h"

//	2014.11.04	Ryumin
#include "ESMTimerEx.h"

//CMiLRe 20151013 Template Load INI File
#ifndef CMiLRe 
#include "ESMIni.h"
#include <map>
using namespace std;

#include "ESMTemplateMgr.h"
#include "ESMProcessAJA.h"
#include "ESMMovieListDlg.h"
#endif

class CDSCItem;
class CTimeLineView;
class CMainFrame;

// CESMFrameNailDlg dialog
class CTimeLineProcessor: public CDialog
{
	//-- For Toolbar
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	//DECLARE_DYNAMIC(CTimeLineProcessor)
public:
	CTimeLineProcessor(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTimeLineProcessor();

	//-- For Toolbar
	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CTimeLineProcessor)
	enum { IDD = IDD_VIEW_ONLY_TOOLBAR };
	//}}AFX_DATA

public:	
	CTimeLineView*	m_pTimeLineView;
	DWORD m_nMakeTime;
	int m_nRunningTime;
	CMainFrame* m_pMainWnd;

protected:
	//seo
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	CRect m_rcClientFrame;	
	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;	

public:	
	void UpdateFrames();
	void Reset()	{ m_pTimeLineView->RemoveObject(); }
	void SetProgress(float nProgress, int nObj, int nKind) { m_pTimeLineView->SetProgress(nProgress, nObj, nKind);}
	
	void OutputImage(CString strFileName, int nCurFrameIndex);
	
	void ItemAllDelete();

	void DSCMoviePlay			(CString strParh = _T(""));
		
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();	
	afx_msg void OnDestroy();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);	

	afx_msg void OnDSCMakeMovie			();
	afx_msg void OnDSCMakeMovie3D		();
	afx_msg void OnDSCMoviePlay			();
	afx_msg void OnDSCSCaling			();

	DECLARE_MESSAGE_MAP()

public:
	void SetToolbarShowNHide(BOOL isShow);
		
	TTimerEx<CTimeLineProcessor> m_tTimerEx;
	//	타이머 이벤트를 이용하여 입력 받은 인터벌 후 함수 자동 실행
	void  MovieStop(const CString& strPlayerName, UINT RunningTime, UINT nInterval = 3000);
	//	팟 플레이어 메인 핸들에 F4 키 메세지 전달
	//	플레이이 중 또는 플레이가 끝난 동영상을 닫는다.
	void  SendMovieStopMessage();
	static unsigned WINAPI FocusObserverThread(LPVOID param);	

public:
	vector<CString> m_IPList;
	int  GetIPList();
	void MakeProcessor();
	void MakeProcessor(int nIndex, int nNextTime = movie_next_frame_time);
	void UpdateProcessor();
	void DeleteProcessor();

	void MakeProcessor(ESMEvent * pMsg, int nStatus = TIMELINE_BACKGROUND_NON);
	void UpdateProcessor(ESMEvent * pMsg);
	void UpdateFailProcessor(ESMEvent * pMsg);
	CESMMovieListDlg m_MovieList;

	int m_nStatus;
	BOOL m_bLoadPlay;

	CRITICAL_SECTION m_CriSection; 

	void DSCMoviePlay(CESMTimeLineObjectEditor* pObject);

};