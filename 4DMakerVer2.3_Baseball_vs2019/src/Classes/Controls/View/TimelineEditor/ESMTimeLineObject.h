////////////////////////////////////////////////////////////////////////////////
//
//	ESMTimeLineObject.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-27
//
////////////////////////////////////////////////////////////////////////////////


#pragma once


#include "ESMDropTarget.h"
#include "ESMDropSource.h"
#include "DSCViewDefine.h"
#include "ESMDefine.h"
#include "ESMIndex.h"
#include "ESMFunc.h"

//wgkim@esmlab.com
#include "ESMTemplateMgr.h"

//-- 2014-07-22 hongsu@esmlab.com
//-- for Effect Editor Frame 
class CESMObjectFrame;

class CDSCItem;
class CESMTimeLineObject : public CWnd
{
public:
	CESMTimeLineObject();
	//CESMTimeLineObject(const CObArray& ar, int nStartTime, int nEndTime, int nNextTime = movie_next_frame_time);
	//CMiLRe 20151022 Template Group 단위로 삭제
	CESMTimeLineObject(const CObArray& ar, int nStartTime, int nEndTime, int nNextTime, int nGroupNo = 0, int nStepFrame = 1);
	virtual ~CESMTimeLineObject();
	DECLARE_DYNCREATE(CESMTimeLineObject)

public:
	CObArray	m_arDSCinObj;

	BOOL		m_bClose;
	HANDLE		m_hMakeObjMovie;
	HANDLE		m_hMake3DMovie;
	HWND		m_hMainWnd;	// AfxGetMainWnd()->GetSafeHwnd();
	//CMiLRe 20151022 Template Group 단위로 삭제
	int			m_mGroupNo;

	CPoint m_startPos;
	CPoint m_EndPos;
	int m_nStartDSC;
	int m_nEndDSC;
	int m_nZoom[3];
	int m_nTime[3];			// FALSE(0) -> Start : TRUE(1) -> End (milli second) 2 -> Next Time
	int m_nKzoneIdx;		//161024 hjcho
	int m_nStepFrame;
protected:
	CFont *m_pFont;
	void SetFrameFont(CDC* pDC, int nSize = 13, CString strFont = _T("Segoe UI"), int nFontType = FW_NORMAL);	

public:		
	virtual BOOL DrawObject() {return FALSE;}
	void Copy(CESMTimeLineObject* pSrc);

	void SetStartTime(int nStartTime)	{ m_nTime[FALSE] = nStartTime; }
	int GetStartTime()					{ return m_nTime[FALSE];}
	void SetEndTime(int nEndTime)		{ m_nTime[TRUE] = nEndTime; }
	int GetEndTime()					{ return m_nTime[TRUE];	}
	void SetNextTime(int nNextTime)		{ m_nTime[2] = nNextTime; }
	int GetNextTime()					{ return m_nTime[2];	}
	void SetKzoneIdx(int KzoneIdx)		{ m_nKzoneIdx = KzoneIdx; }
	int GetKzoneIdx()					{ return m_nKzoneIdx;	}
	void SetStepFrame(int nStepCnt)		{ m_nStepFrame = nStepCnt; }
	int GetStepFrame()					{ return m_nStepFrame;	}
	int GetDuration();

	int GetDSCCount()					{ return m_arDSCinObj.GetCount(); }
	CDSCItem* GetDSC(int nIndex)		
	{ 
		if(m_arDSCinObj.GetSize())
			return (CDSCItem*)m_arDSCinObj.GetAt(nIndex); 
		else 
			return NULL;
	}
	CDSCItem* GetLastDSC();	
	CString GetDSCID(int nIndex);
	int GetDSCIndex(int nIndex);

	//------------------------------------------------------------------------------
	//! @function	ObjectFrame
	//! @brief		Control ObjectFrame
	//!				- For MCC	
	//! @date		2014-07-17
	//! @owner		Hongsu Jung (hongsu@esmlab.com)	
	//------------------------------------------------------------------------------
	CObArray	m_arObjectFrame;	
	//int GetObjectFrameCount();		
	int GetObjectFrameCount()		{ return m_arObjectFrame.GetCount();}
	void AddObjectFrame(CESMObjectFrame* pObjFrm);
	void DrawObjectFrame(CWnd* pWnd);
	CESMObjectFrame* GetObjectFrame(int nIndex);
	CESMObjectFrame* GetObjectFrame(CString strDSCID);
	void RemoveAllObjectFrame();

	int GetObjectFrameOrder(CESMObjectFrame* pObjFrm);
	int GetInsertOrder(CESMObjectFrame* pObjFrm);
	void ShowFrame(BOOL bShow);
	void ResetSelection();

	void RecalculateInfo();
	//wgkim@esmlab.com
	void RecalculateInfo(CESMTemplateMgr* templateMgr);	

	int FindNextSpotObject(int nPrev);
	int GetSpotCount();
	BOOL ClickFrame(int nOrder, BOOL bNext = TRUE);

	//-- Set Weight (Make Curve) ==> Xⁿ+yⁿ= 1
	float GetWeightPercentage(float fX, float fWeight);

protected:
	// Generated message map functions
	//{{AFX_MSG(C4DMakerListView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);	
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);	
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);	
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG	

protected:
	DECLARE_MESSAGE_MAP()

	//-- 2013-09-27 hongsu@esmlab.com
	//-- For Drag&Drop 
private:
	CESMDropSource m_DropSource;
	BOOL m_bDraging;

public:
	/*BOOL m_bDrawTime;
	void SetDrawTime(BOOL bDraw){ m_bDrawTime = bDraw; }
	BOOL GetDrawTime() { return m_bDrawTime; }*/

	BOOL m_bProcessMode;
	void SetProcessMode(BOOL bMode) { m_bProcessMode = bMode; }
	BOOL GetProcessMode() { return m_bProcessMode; }

	CString m_strMakePC;
	void SetMakePC(CString strPc) { m_strMakePC = strPc; }
	CString GetMakePC() { return m_strMakePC; }

	int m_nFrameCnt;
	void SetFrameCnt(int nCnt) { m_nFrameCnt = nCnt; }
	int GetFrameCnt() { return m_nFrameCnt; }

	CString m_strFile;
	void SetMovieFile(CString strFile) { m_strFile = strFile; }
	CString GetMovieFile() { return m_strFile; }

};

