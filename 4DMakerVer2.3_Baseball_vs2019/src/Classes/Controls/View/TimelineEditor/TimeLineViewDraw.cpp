////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineViewDraw.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-07-11
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DSCViewer.h"
#include "TimeLineView.h"
#include "ESMTimeLineObject.h"
#include "ESMObjectFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//----------------------------------------------------------------
//-- 2013-04-22 hongsu.jung
//-- Thread Function
//----------------------------------------------------------------
void _DrawTimeLineEiditor(void *param)
{
	CTimeLineView* pView = (CTimeLineView*)param;
	if (!pView) 
		return;
	if (pView->m_bClose) 
		return;

	CDC* pDC = NULL;
	pDC = pView->GetDC();
	if(!pDC) 
		return;

	//-- 2013-09-27 hongsu@esmlab.com
	//-- Draw TimeLine
	if(pView)
		pView->DrawTimeLine(pDC);

	//-- 2013-09-29 hongsu@esmlab.com
	//-- Draw TimeLine Object
	if(pView)
	{
		pView->DrawObjects(pDC);
		if(pView->m_bRecal)
			pView->ReCalculateTime();
	}
	

	if (pDC != NULL)
		pView->ReleaseDC(pDC);
	
	if(pView)
		pView->m_hThreadDraw = NULL;
	_endthread();
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------

void CTimeLineView::DrawEditor()
{	
	if (m_bClose) 
		return;	
	m_hThreadDraw = (HANDLE)_beginthread( _DrawTimeLineEiditor, 0, (void*)this); // create thread
}

void CTimeLineView::DrawTimeLine(CDC* pDC)
{
	// draw Background
	CRect rtTop;
	GetClientRect(&rtTop);
	pDC->FillSolidRect(rtTop, COLOR_BASIC_BG_1 );

	//-- 2013-10-01 hongsu@esmlab.com
	//-- For Drawing Text 
	CRect rtTxt;
	CString strTxt;
	rtTxt.top		= m_ptScroll.y;
	rtTxt.bottom	= m_ptScroll.y+m_nTickMajorHeight;

	SetFrameFont(pDC, 13, _T("Noto Sans CJK KR Regular"));
	pDC->SetTextColor(COLOR_WHITE);
	pDC->SetBkColor(COLOR_BASIC_BG_3);


	// draw the top portion
	rtTop.bottom = rtTop.top + m_nHeaderTop;
	//-- 2013-09-06 hongsu@esmlab.com
	//-- Calculate Scroll size 
	rtTop.left += m_ptScroll.x;
	rtTop.right += m_ptScroll.x;
	rtTop.top += m_ptScroll.y;
	rtTop.bottom += m_ptScroll.y;

	pDC->FillSolidRect(rtTop, COLOR_BASIC_BG_3);	

	if(GetDrawSec() == FALSE)
	{
		CPen penMajorTicks;
		penMajorTicks.CreatePen(PS_SOLID  ,3 , RGB(0x79,0x79,0x79)/*COLOR_WHITE*/);
		pDC->SelectObject(&penMajorTicks);
		pDC->DrawText(GetDrawStatus(), rtTop,  DT_LEFT | DT_VCENTER/*DT_BOTTOM*/ | DT_SINGLELINE);
		return;
	}

	CRect rtTick;
	rtTick.top = m_ptScroll.y;
	rtTick.bottom = rtTick.top + m_nHeaderTop;
	rtTick.left = GetXFromTime(0);
	rtTick.right = rtTick.left + GetSpanWidth();

	CPen penMinorTicks;
	CPen penMajorTicks;
	penMinorTicks.CreatePen(PS_SOLID  ,1 , RGB(0x33,0x33,0x33)/*COLOR_BASIC_FG_1*/ );
	penMajorTicks.CreatePen(PS_SOLID  ,1 , RGB(0x79,0x79,0x79)/*COLOR_WHITE*/);
	
	int x;
	int TX;
	//-- 2013-09-06 hongsu@esmlab.com
	//-- mill second 
	pDC->SelectObject(&penMinorTicks);	
	for(x = 0; x < m_nTimeMax; x+=m_nTickFreqMinor)
	{
		TX = GetXFromTime(x);
		pDC->MoveTo(TX,m_ptScroll.y);
		pDC->LineTo(TX,m_ptScroll.y+m_nTickMinorHeight);
	}

	//-- 2013-09-06 hongsu@esmlab.com
	//-- second
	pDC->SelectObject(&penMajorTicks);
	for(x = 0; x<=m_nTimeMax; x+=m_nTickFreqMajor)
	{
		TX = GetXFromTime(x);
		pDC->MoveTo(TX,m_ptScroll.y);
		pDC->LineTo(TX,m_ptScroll.y+m_nTickMajorHeight);

		//-- 2013-10-01 hongsu@esmlab.com
		//-- Draw Second
		if(GetDrawSec())
			strTxt.Format(_T("%d sec"),x/1000);
		else
			strTxt.Format(_T("%d"),(x+m_nTickFreqMajor)/1000);

		rtTxt.left	= TX + DSC_TIMELINE_SEC_GAP;
		rtTxt.right	= TX + GetXFromTime(2000);
		pDC->DrawText(strTxt, rtTxt,  DT_LEFT | DT_BOTTOM | DT_SINGLELINE);
		
	}	
}

void CTimeLineView::DrawObjects(CDC* pDC)
{
	CESMTimeLineObjectEditor* pObject = NULL;		
	int nAll = m_arObject.GetCount();

	CPoint ptDraw;
	ptDraw.x = GetXFromTime(0);
	ptDraw.y = m_nHeaderTop + 2;//m_ptScroll.y;

	if(ptDraw.x > 0 && ptDraw.y > 0)
	{
		for(int i = 0; i < nAll ; i ++)
		{
			pObject = GetObj(i);
			if( pObject)
				DrawObject(pDC, pObject, &ptDraw);		
		}
	}
		
}


void CTimeLineView::DrawObject(CDC* pDC, CESMTimeLineObjectEditor* pObject, CPoint* pPoint)
{
	if(!pObject)
		return;


	CDSCItem* pStartDSC = NULL;
	CDSCItem* pEndDSC	= NULL;
	int nDuration, nDurationX;

	//-- Get Start Time
	nDuration	= pObject->GetDuration() + DSC_TIMELINE_SEC_GAP;

	CRect rtBox;
	rtBox.top		= pPoint->y;
	rtBox.bottom	= rtBox.top + DSC_TIMELINE_TOP + DSC_TIMELINE_BODY + DSC_TIMELINE_MARGIN*3;

	rtBox.left		= pPoint->x;
	nDurationX		= GetXFromTime(nDuration) - m_nLineLeftMargin;
	pPoint->x		+= nDurationX;
	rtBox.right		= pPoint->x;

	//-- Set Rect Position
	pObject->SetRect(rtBox);

	//-- DrawObject
	try 
	{
		pObject->MoveWindow(rtBox);	
		pObject->DrawObject();
	}
	catch(...)
	{
		ESMLog(0, _T("DrawObject"));
	}
	
}


void CTimeLineView::SetProgress(float nProgress, int nObj, int nKind)
{
	CESMTimeLineObjectEditor* pObject = NULL;		
	pObject = GetObj(nObj);
	if(pObject)
		pObject->SetProgress(nProgress, nKind);
}

//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
void CTimeLineView::SyncEffectInfo(CESMTimeLineObjectEditor* pSelectedObj, CESMTimeLineObjectEditor* pObj, int nFrameIdx, BOOL nAllChange)
{
	TRACE( _T("SyncTimeLineZoom") );
	int nCnt = GetObjCount();
	int nIndex = GetOrder(pObj);
	
	CESMTimeLineObjectEditor* pTargetObj = NULL;
	CESMObjectFrame* pTargetObjFrame = NULL;

	CESMObjectFrame* pObjFrame = pObj->GetObjectFrame(nFrameIdx);
		
	// 마지막 오브젝트일 경우
	if ( nCnt < nIndex + 2 )
		return;

	//CMiLRe 20160115 VMCC Spot Info 변경 시 하나의 Object만 변경 가능하도록 수정
	if(nAllChange)
	{
		int nCntObj = GetObjCount();
		for ( int i = nIndex + 1 ; i < nCntObj ; i++)
		{
			pTargetObj = GetObj(i);

			pTargetObj->InitSpotInfo();

			for ( int i = 0 ; i < pTargetObj->GetObjectFrameCount() ; i++ )
			{
				pTargetObjFrame = pTargetObj->GetObjectFrame(i);
				pTargetObjFrame->SetZoomRatio(pObjFrame->GetZoomRatio());
				pTargetObjFrame->SetPosX(pObjFrame->GetPosX());
				pTargetObjFrame->SetPosY(pObjFrame->GetPosY());
			}

			pTargetObj->RecalculateInfo();

			// 수정하려는 오브젝트가 현재 선택된 오브젝트이면 이펙트 업데이트
			if ( pSelectedObj == pTargetObj )
			{
				ESMEvent* pMsg	= new ESMEvent;
				pMsg->message	= WM_ESM_EFFECT_SPOTLIST_UPDATE;
				pMsg->pParam	= (LPARAM)pSelectedObj;		
				::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	
			}
		}
	}
	
}