////////////////////////////////////////////////////////////////////////////////
//
//	TimeLineEditor.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-27
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "FileOperations.h"
#include "ESMFileOperation.h"
#include "TimeLineEditor.h"
#include "DSCItem.h"
#include "PrinterMgr.h"

#include "MainFrm.h"
#include "ESMTimeLineObjectSelector.h"
#include "DSCFrameSelector.h"
#include "DSCMgr.h"
#include "ESMUtil.h"
#include "ESMFunc.h"
#include "SRSIndex.h"
#include "ESMTimeLineObjectEditor.h"
#ifdef _4DMODEL
#include "4DModelerVersion.h"
#else
#include "4DMakerVersion.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const int nAsianGameFrameSpeed = 30;
// CFrameNailDlg dialog
//IMPLEMENT_DYNAMIC(CTimeLineEditor, CDialog)
BEGIN_MESSAGE_MAP(CTimeLineEditor, CDialog)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_UPDATE_COMMAND_UI(ID_IMAGE_MAKEMOVIE		, OnUpdateDSCTool	)	
	ON_UPDATE_COMMAND_UI(ID_IMAGE_MAKEMOVIE_3D	, OnUpdateDSCTool	)		
	ON_UPDATE_COMMAND_UI(ID_IMAGE_MOVIEPLAY		, OnUpdateDSCTool	)	
	ON_COMMAND(ID_IMAGE_MAKEMOVIE				, OnDSCMakeMovie	) 
	ON_COMMAND(ID_IMAGE_MAKEMOVIE_3D			, OnDSCMakeMovie3D	) 
	ON_COMMAND(ID_IMAGE_MOVIEPLAY				, OnDSCMoviePlay	)
	ON_COMMAND(ID_IMAGE_SCALING					, OnDSCSCaling		)	


	ON_COMMAND(ID_IMAGE_ITEMLEFTMOVE			, OnItemLeftMove	) 
	ON_COMMAND(ID_IMAGE_ITEMRIGHTMOVE			, OnItemRightMove	)
	ON_COMMAND(ID_IMAGE_ITEMDELETEALL			, OnItemDeleteAll	)	
	//CMiLRe 20151013 Template Load INI File
	ON_COMMAND(ID_IMAGE_ITEMTEMPLATE_REFRESH			, OnItemTemplateRefresh	)	
	//CMiLRe 20151014 Template Save INI File
	ON_COMMAND(ID_IMAGE_ITEMTEMPLATE_SAVE				, OnItemTemplateSave	)	
	ON_COMMAND( ID_IMAGE_ITEMTEMPLATE_TIMESYNC			, OnItemTemplateTimeSync)
	ON_WM_KEYUP()
END_MESSAGE_MAP()


CTimeLineEditor::CTimeLineEditor(CWnd* pParent /*=NULL*/)
	: CDialog(CTimeLineEditor::IDD, pParent)	
{	
	m_pTimeLineView = NULL;
	m_nMakeTime = 0;
	m_nRunningTime = 0;
	m_pMainWnd = NULL;
	m_pTemplateMgr = NULL;
}

CTimeLineEditor::~CTimeLineEditor()
{
	//20151015 Template VMCC Save/Load
	if(m_TemplateVector.size()>0)
	{
		m_TemplateVector.clear();
	}
}

void CTimeLineEditor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTimeLineEditor)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);
}

//seo
BOOL CTimeLineEditor::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg)
	{
		if(pMsg->message == WM_KEYDOWN)
		{
			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			ESMEvent* pEsmMsg = NULL;
			pEsmMsg = new ESMEvent();
			pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
			pEsmMsg->pParam = (LPARAM)pMsg;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

// CFrameNailDlg message handlers
BOOL CTimeLineEditor::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_pMainWnd = (CMainFrame*)AfxGetMainWnd();
	if(!m_pTimeLineView)
	{
		m_pTimeLineView = new CTimeLineView();
		m_pTimeLineView->Create(NULL, NULL, DS_SETFONT | WS_CHILD | WS_VISIBLE , CRect(0,0,0,0), this, IDD_VIEW_TIMELINE);
		//-- 2013-09-27 hongsu@esmlab.com
		//-- Draw And Drop
		m_pTimeLineView->InitDropTarget(m_pTimeLineView);
	}
	else
		return FALSE;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTimeLineEditor::OnDestroy()
{
}

void CTimeLineEditor::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_MAKEMOVIE			,	
		//ID_IMAGE_MAKEMOVIE_3D		,
		//ID_SEPARATOR				,
		//ID_IMAGE_MOVIEPLAY		,
		ID_SEPARATOR				,
		ID_IMAGE_SCALING			,
		
		ID_SEPARATOR				,
		ID_IMAGE_ITEMLEFTMOVE		,
		ID_IMAGE_ITEMRIGHTMOVE		,
		ID_SEPARATOR				,
		ID_IMAGE_ITEMDELETEALL		,
		//CMiLRe 20151013 Template Load INI File
		ID_IMAGE_ITEMTEMPLATE_REFRESH,
		ID_SEPARATOR				,
		ID_IMAGE_ITEMTEMPLATE_TIMESYNC,
		//CMiLRe 20151014 Template Save INI File
		ID_IMAGE_ITEMTEMPLATE_SAVE,
		
	};

	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	

	m_wndToolBar.ShowWindow(SW_SHOW);	
}

void CTimeLineEditor::OnUpdateDSCTool(CCmdUI *pCmdUI)
{ 
	//-- 2013-04-29 hongsu@esmlab.com
	//-- Check List
// 	if( m_pTimeLineView->GetObjCount() <= 0 &&  ESMGetFilmState() != ESM_ADJ_FILM_STATE_PICTURE)
// 	{
// 		AfxMessageBox(_T("Time LIne에 Object가 없습니다."));
// 		return;
// 	}
// 
// 	if(m_pTimeLineView->GetObjCount())
// 		pCmdUI->Enable(TRUE);
// 	else
// 		pCmdUI->Enable(FALSE);
}	

void CTimeLineEditor::OnSize(UINT nType, int cx, int cy) 
{
	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	//m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;
	m_rcClientFrame.top	= 40;

	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width()		< FRAME_MIN_WIDTH || 
	    m_rcClientFrame.Height()	< FRAME_MIN_HEIGHT)
		return;

	//-- ReloadFrame
	UpdateFrames();
	CDialog::OnSize(nType, cx, cy);	
} 

void CTimeLineEditor::OnPaint()
{	
	UpdateFrames();
	CDialog::OnPaint();
}

void CTimeLineEditor::UpdateFrames()
{
	//-- Rounding Dialog
	CPaintDC dc(this);	

	CRect rect;
	rect.CopyRect(m_rcClientFrame);

	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), SRCCOPY);

	CString strFilePath;
	mDC.FillRect(rect, &CBrush(COLOR_BASIC_BG_2));
	
	//-- Frame List
	if(m_pTimeLineView)
	{
		m_pTimeLineView->ShowWindow(SW_SHOW);
		m_pTimeLineView->MoveWindow(rect);
		m_pTimeLineView->Invalidate(FALSE);	
	}	

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	mDC.DeleteDC();
}




//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-01
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void  CTimeLineEditor::OnDSCSCaling(void)				
{
	if(m_pTimeLineView)
		m_pTimeLineView->m_bScaleToWindow = !m_pTimeLineView->m_bScaleToWindow;
	m_pTimeLineView->DrawEditor();
}

void CTimeLineEditor::OnItemLeftMove()
{
	int iSelectedIndx = 0;
	int temp = 0;
	for(int i =0 ; i < m_pTimeLineView->GetObjCount(); i++)
	{
		CESMTimeLineObjectEditor* pObjectEditor = m_pTimeLineView->GetObj(i);
		CString strTp;

		if( pObjectEditor->GetBK() == TIMELINE_BACKGROUND_ALL)
		{
			iSelectedIndx = i;
		}
	}
	// 여기서 부터 작성.
	CString idx;
	idx.Format(_T("%d"), iSelectedIndx);
	
	if(iSelectedIndx == 0)
		return;

	m_pTimeLineView->Swap(iSelectedIndx - 1, iSelectedIndx);

	m_pTimeLineView->DrawEditor();
}

void CTimeLineEditor::VmccTamplate( int nRecTime, int nSelectTime, int nTemplate)
{
	if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
	{
		AfxMessageBox(_T("Current server mode is video production mode. \n Available in Record or Both mode only."));
		return;
	}

	vector<TEMPLATE_STRUCT> LoadTemplateVector;
	LoadTemplateVector = LoadTemplateElement(nTemplate, TRUE);

	if(LoadTemplateVector.size() <= 0)
	{
		ESMLog(5, _T("Non Template Data!"));

		//jhhan m_bWait = FALSE 설정
		if(ESMGetBaseBallFlag())
		{
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_MOVIE_MAKE_WAIT_END;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}
		return;
	}

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nTotalRecTime = nRecTime;
	ESMGetPreviousFrameTime(nTotalRecTime);
	int nSelectedTime = nSelectTime;
	if(ESMGetExecuteMode())
		pMainWnd->m_wndDSCViewer.m_pDSCMgr->OnEventSyncDataSet();

	int nStartTime = 0, nEndTime = 0;;
	nStartTime = nSelectedTime - ESMGetValue(ESM_VALUE_TEMPLATE_STARTTIME);
	nEndTime = nSelectedTime + ESMGetValue(ESM_VALUE_TEMPLATE_ENDTIME);

	if( nStartTime < 0)
		nStartTime = 0;
	if( nEndTime > nTotalRecTime)
		nEndTime = nTotalRecTime;

	ESMLog(5, _T("@@@@@@@@@@ nSelectedTime = %d, nStartTime = %d, nEndTime = %d @@@@@@@@@@@@@@"), nSelectedTime, nStartTime, nEndTime);

	CESMFileOperation fo;
	int nStartDsc = 0, nEndDsc = 0;
	CDSCItem* pItem = NULL;
	FFmpegManager FFmpegMgr(FALSE);
	CString strFileName, strDscIp;
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3, nFrameRate = 0;

	int nFIdx = ESMGetFrameIndex(nSelectedTime);
	for( int i = 0; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);

			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nStartDsc = i;
				break;
			}
		}
	}
	for( int i = arDSCList.GetCount() - 1; i >= 0; i--)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);

			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nEndDsc = i;
				break;
			}
		}
	}

	//MakeTampleteMake(nStartDsc, nEndDsc, m_pTimeLineView->GetTemplateGroupNO(), LoadTemplateVector);
	MakeTampleteMake(nStartDsc, nEndDsc, nSelectedTime, m_pTimeLineView->GetTemplateGroupNO(), LoadTemplateVector);
	m_pTimeLineView->SetTemplateGroupIncrease();
	LoadTemplateVector.clear();		

	m_pTimeLineView->DrawEditor();
}
//CMiLRe 20151202 VMCC 템플리 호출 함수 추가
void CTimeLineEditor::VmccTamplate(int nTemplate)
{
	if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
	{
		AfxMessageBox(_T("Current server mode is video production mode. \n Available in Record or Both mode only."));
		return;
	}

	vector<TEMPLATE_STRUCT> LoadTemplateVector;
	LoadTemplateVector = LoadTemplateElement(nTemplate, TRUE);

	if(LoadTemplateVector.size() <= 0)
	{
		ESMLog(5, _T("Non Template Data!"));
		return;
	}

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();

	//wgkim 17-05-23
	ESMSetCurrentTemplateVector(LoadTemplateVector);	


	int nTotalRecTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	ESMGetPreviousFrameTime(nTotalRecTime);
	int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
	
	if(ESMGetExecuteMode())
		pMainWnd->m_wndDSCViewer.m_pDSCMgr->OnEventSyncDataSet();

	int nStartTime = 0, nEndTime = 0;;
	nStartTime = nSelectedTime - ESMGetValue(ESM_VALUE_TEMPLATE_STARTTIME);
	nEndTime = nSelectedTime + ESMGetValue(ESM_VALUE_TEMPLATE_ENDTIME);

	if( nStartTime < 0)
		nStartTime = 0;
	if( nEndTime > nTotalRecTime)
		nEndTime = nTotalRecTime;

	CESMFileOperation fo;
	int nStartDsc = 0, nEndDsc = 0;
	CDSCItem* pItem = NULL;
	FFmpegManager FFmpegMgr(FALSE);
	CString strFileName, strDscIp;
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3, nFrameRate = 0;

	int nFIdx = ESMGetFrameIndex(nSelectedTime);
	
#if 1    //VMCC 템플릿 개선
	nStartDsc = 0;
	nEndDsc= arDSCList.GetCount() - 1;
#else
	for( int i = 0; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);
			
			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nStartDsc = i;
				break;
			}
		}
	}
	for( int i = arDSCList.GetCount() - 1; i >= 0; i--)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);
			
			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nEndDsc = i;
				break;
			}
		}
	}
#endif

	int nTick = GetTickCount();
	MakeTampleteMake(nStartDsc, nEndDsc, m_pTimeLineView->GetTemplateGroupNO(), LoadTemplateVector);
		
	//wgkim@esmlab.com Calculate coordinates to be pushed. 
	if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT) /*&& ESMGetValue(ESM_VALUE_TEMPLATE_MODIFY)*/)
	{
		if(m_pTemplateMgr != NULL)
		{
			//m_pTemplateMgr->TemplateModifyForTargetDisappearingFromScreen();

			if(ESMGetValue(ESM_VALUE_TEMPLATE_MODIFY_MODE) == 1)
			{
				m_pTemplateMgr->RecalculatedTemplateZoom();
			}
			else if(ESMGetValue(ESM_VALUE_TEMPLATE_MODIFY_MODE) == 2)
			{
				m_pTemplateMgr->CheckPointBoundary();
				m_pTemplateMgr->RecalculatedCenterPoint();
			}
		}
	}

	ESMLog(5, _T("VMCC Template Loading UI : %d ms"), GetTickCount()-nTick);
	m_pTimeLineView->SetTemplateGroupIncrease();
	LoadTemplateVector.clear();		

	m_pTimeLineView->DrawEditor();	
}

void CTimeLineEditor::MakeAdjustTamplete()
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nTotalRecTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	ESMGetPreviousFrameTime(nTotalRecTime);
	int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
	if(ESMGetExecuteMode())
		pMainWnd->m_wndDSCViewer.m_pDSCMgr->OnEventSyncDataSet();

	int nStartTime = 0, nEndTime = 0;;
	//nStartTime = nSelectedTime - ESMGetValue(ESM_VALUE_TEMPLATE_STARTTIME);
	//nEndTime = nSelectedTime + ESMGetValue(ESM_VALUE_TEMPLATE_ENDTIME);

	//if( nStartTime < 0)
	//	nStartTime = 0;
	//if( nEndTime > nTotalRecTime)
		nEndTime = nTotalRecTime;

	CESMFileOperation fo;
	int nStartDsc = 0, nEndDsc = 0;
	CDSCItem* pItem = NULL;
	FFmpegManager FFmpegMgr(FALSE);
	CString strFileName, strDscIp;
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3, nFrameRate = 0;

	int nFIdx = ESMGetFrameIndex(nSelectedTime);
	for( int i = 0; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);
			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nStartDsc = i;
				break;
			}
		}
	}
	for( int i = arDSCList.GetCount() - 1; i >= 0; i--)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);
			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nEndDsc = i;
				break;
			}
		}
	}

	for(int n = nStartDsc; n <= nEndDsc; n++)
	{
		MakeTimeLine(n, n, nStartTime, nEndTime, nAsianGameFrameSpeed);
	}
	
}

//CMiLRe 20151013 Template Load INI File
void CTimeLineEditor::MakeBasicTamplete()
{
	if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
	{
		AfxMessageBox(_T("Current server mode is video production mode. \n Available in Record or Both mode only."));
		return;
	}
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nTotalRecTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	ESMGetPreviousFrameTime(nTotalRecTime);
	int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
	if(ESMGetExecuteMode())
		pMainWnd->m_wndDSCViewer.m_pDSCMgr->OnEventSyncDataSet();

	int nStartTime = 0, nEndTime = 0;;
	nStartTime = nSelectedTime - ESMGetValue(ESM_VALUE_TEMPLATE_STARTTIME);
	nEndTime = nSelectedTime + ESMGetValue(ESM_VALUE_TEMPLATE_ENDTIME);

	if( nStartTime < 0)
		nStartTime = 0;
	if( nEndTime > nTotalRecTime)
		nEndTime = nTotalRecTime;

	CESMFileOperation fo;
	int nStartDsc = 0, nEndDsc = 0;
	CDSCItem* pItem = NULL;
	FFmpegManager FFmpegMgr(FALSE);
	CString strFileName, strDscIp;
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3, nFrameRate = 0;

	int nFIdx = ESMGetFrameIndex(nSelectedTime);
	for( int i = 0; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);
// 			FFmpegMgr.GetMovieInfo(strFileName, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
// 			if(nWidth != 0 && nHight != 0 && pItem->GetSensorSync() == TRUE)
// 			{
// 				nStartDsc = i;
// 				nWidth = 0;		nHight = 0;
// 				break;
// 			}
//			nWidth = 0;		nHight = 0;
			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nStartDsc = i;
				break;
			}
		}
	}
	for( int i = arDSCList.GetCount() - 1; i >= 0; i--)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);
// 			FFmpegMgr.GetMovieInfo(strFileName, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
// 			if(nWidth != 0 && nHight != 0 && pItem->GetSensorSync() == TRUE)
// 			{
// 				nEndDsc = i;
// 				nWidth = 0;		nHight = 0;
// 				break;
// 			}
//			nWidth = 0;		nHight = 0;
			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nEndDsc = i;
				break;
			}
		}
	}
	
	if(ESMGetValue(ESM_VALUE_CEREMONYUSE) ==TRUE && ESMGetValue(ESM_VALUE_CEREMONYPUTIMAGE) ==TRUE )
	{
		int nSelectLine = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectLine();
		pItem = (CDSCItem*)arDSCList.GetAt(nSelectLine);
		int nCurFrameIndex = ESMGetFrameIndex(nSelectedTime);

		int nIndex = nCurFrameIndex/30;
		int nFrameIndex = nCurFrameIndex%30;
		//strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientBackupRamPath(pItem->m_strInfo[DSC_INFO_LOCATION]),  pItem->GetDeviceDSCID());
		strFileName.Format(_T("%s\\%s\\%s_%d.mp4"),ESMGetClientRamPath(pItem->m_strInfo[DSC_INFO_LOCATION]),ESMGetFrameRecord(),  pItem->GetDeviceDSCID(),nIndex);
		//OutputImage(strFileName, nCurFrameIndex);
		OutputImage(strFileName, nFrameIndex);

		//strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientBackupRamPath(pItem->m_strInfo[DSC_INFO_LOCATION]),  pItem->GetDeviceDSCID());
		//OutputImage(strFileName, nCurFrameIndex);
		//OutputImage(strFileName, 0);
	}
	
	//대전
	//전진행
	//void MakeTimeLine(int nDscStart, int nDscEnd, int nStartTime, int nEndTime, int nNextTime = movie_next_frame_time);
	MakeTimeLine(nStartDsc, nStartDsc, nStartTime, nSelectedTime, nAsianGameFrameSpeed);
	MakeTimeLine(nStartDsc, nStartDsc, nSelectedTime, nSelectedTime, 200);
	//TimeSlice
	MakeTimeLine(nStartDsc , nEndDsc , nSelectedTime, nSelectedTime, ESMGetValue(ESM_VALUE_ADJ_FLOW_T));
	MakeTimeLine(nEndDsc, nEndDsc, nSelectedTime, nSelectedTime, 200);
	MakeTimeLine(nEndDsc, nEndDsc, nSelectedTime, nEndTime, nAsianGameFrameSpeed);


	m_pTimeLineView->DrawEditor();
}



//CMiLRe 20151013 Template Load INI File
void CTimeLineEditor::MakeBasicTampleteRe()
{
	TRACE(_T("MakeBasicTampleteRe 1"));
	if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
	{
		AfxMessageBox(_T("Current server mode is video production mode. \n Available in Record or Both mode only."));
		return;
	}
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nTotalRecTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	ESMGetPreviousFrameTime(nTotalRecTime);
	int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
	if(ESMGetExecuteMode())
		pMainWnd->m_wndDSCViewer.m_pDSCMgr->OnEventSyncDataSet();

	int nStartTime = 0, nEndTime = 0;;
	nStartTime = nSelectedTime - ESMGetValue(ESM_VALUE_TEMPLATE_STARTTIME);
	nEndTime = nSelectedTime + ESMGetValue(ESM_VALUE_TEMPLATE_ENDTIME);

	if( nStartTime < 0)
		nStartTime = 0;
	if( nEndTime > nTotalRecTime)
		nEndTime = nTotalRecTime;


	int nStartDsc = 0, nEndDsc = 0, nMidDsc = 0;
	CDSCItem* pItem = NULL;
	CESMFileOperation fo;
	FFmpegManager FFmpegMgr(FALSE);
	CString strFileName, strDscIp;
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3, nFrameRate= 0;
	TRACE(_T("MakeBasicTampleteRe 2"));

	int nMiddleDsc1 = arDSCList.GetCount() / 2 -1;
	int nMiddleDsc2 = arDSCList.GetCount() / 2;
	int nFIdx = ESMGetFrameIndex(nSelectedTime);

	//for( int i = 0; i< arDSCList.GetCount(); i++)
	for( int i = 10; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);
// 			FFmpegMgr.GetMovieInfo(strFileName, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
// 			if(nWidth != 0 && nHight != 0 && pItem->GetSensorSync() == TRUE)
// 			{
// 				nStartDsc = i;
// 				nWidth = 0;		nHight = 0;
// 				break;
// 			}
//			nWidth = 0;		nHight = 0;
			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nStartDsc = i;
				break;
			}
		}
	}
	for( int i = arDSCList.GetCount() - 1; i >= 0; i--)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{		
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);
// 			FFmpegMgr.GetMovieInfo(strFileName, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
// 			if(nWidth != 0 && nHight != 0 && pItem->GetSensorSync() == TRUE)
// 			{
// 				nEndDsc = i;
// 				nWidth = 0;		nHight = 0;
// 				break;
// 			}
//			nWidth = 0;		nHight = 0;
			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nEndDsc = i;
				break;
			}
		}
	}

	nMidDsc = (nEndDsc+nStartDsc) / 2;
	for( int i = nMidDsc; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strFileName = ESMGetMoviePath(pItem->GetDeviceDSCID(), nFIdx);
			// 			FFmpegMgr.GetMovieInfo(strFileName, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
			// 			if(nWidth != 0 && nHight != 0 && pItem->GetSensorSync() == TRUE)
			// 			{
			// 				nStartDsc = i;
			// 				nWidth = 0;		nHight = 0;
			// 				break;
			// 			}
			//			nWidth = 0;		nHight = 0;
			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nMidDsc = i;
				break;
			}
		}
	}
	TRACE(_T("MakeBasicTampleteRe 3"));
	
	if(ESMGetValue(ESM_VALUE_CEREMONYPUTIMAGE))
	{
		int nSelectLine = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectLine();
		pItem = (CDSCItem*)arDSCList.GetAt(nSelectLine);
		int nCurFrameIndex = ESMGetFrameIndex(nSelectedTime);

		int nIndex = nCurFrameIndex/30;
		int nFrameIndex = nCurFrameIndex%30;
		//strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientBackupRamPath(pItem->m_strInfo[DSC_INFO_LOCATION]),  pItem->GetDeviceDSCID());
		strFileName.Format(_T("%s\\%s\\%s_%d.mp4"),ESMGetClientRamPath(pItem->m_strInfo[DSC_INFO_LOCATION]),ESMGetFrameRecord(),  pItem->GetDeviceDSCID(),nIndex);
		//OutputImage(strFileName, nCurFrameIndex);
		OutputImage(strFileName, nFrameIndex);
	}


	//잠실 댄싱9
	//전진행
	/*MakeTimeLine(nEndDsc, nEndDsc, nStartTime, nSelectedTime, nAsianGameFrameSpeed);
	MakeTimeLine(nEndDsc, nEndDsc, nSelectedTime, nSelectedTime, 200);
	//TimeSlice
	MakeTimeLine(nEndDsc , nStartDsc , nSelectedTime, nSelectedTime, ESMGetValue(ESM_VALUE_ADJ_FLOW_T)*2);
	//후진행
	MakeTimeLine(nStartDsc, nStartDsc, nSelectedTime, nSelectedTime, 200);
	MakeTimeLine(nStartDsc, nStartDsc, nSelectedTime, nEndTime, nAsianGameFrameSpeed);*/

	//SNU
	MakeTimeLine(nEndDsc, nEndDsc, nStartTime, nSelectedTime, nAsianGameFrameSpeed);
	MakeTimeLine(nEndDsc, nEndDsc, nSelectedTime, nSelectedTime, 200);
	//TimeSlice
	MakeTimeLine(nEndDsc , nStartDsc , nSelectedTime, nSelectedTime, ESMGetValue(ESM_VALUE_ADJ_FLOW_T)*2);
	MakeTimeLine(nStartDsc , nMidDsc , nSelectedTime, nSelectedTime, ESMGetValue(ESM_VALUE_ADJ_FLOW_T)*2);
	//후진행
	MakeTimeLine(nMidDsc, nMidDsc, nSelectedTime, nSelectedTime, 200);
	MakeTimeLine(nMidDsc, nMidDsc, nSelectedTime, nEndTime, nAsianGameFrameSpeed);
	
	TRACE(_T("MakeBasicTampleteRe 4"));

	m_pTimeLineView->DrawEditor();
	TRACE(_T("MakeBasicTampleteRe 5"));
}

void CTimeLineEditor::OutputImage(CString strFileName, int nCurFrameIndex)
{
	IplImage* pImage;
	CString strDSC;

	pImage = new IplImage;	
	pImage = cvCreateImage(cvSize(1920, 1080), IPL_DEPTH_8U, 3);

	FFmpegManager ffMpegMgr(FALSE);
	ffMpegMgr.GetCaptureImage(strFileName, pImage, nCurFrameIndex);

	CString strTel1 = ESMGetCeremonyInfoString(_T("InputTel"));
	char* pstrTel = NULL;
	pstrTel = ESMUtil::CStringToChar(strTel1);

	char* pIamgePath = ESMUtil::CStringToChar(ESMGetCeremony(ESM_VALUE_CEREMONYIMAGEPATH));

	char* strSavePath = new char[MAX_PATH];
	sprintf(strSavePath, "%s\\%s.jpg", pIamgePath, pstrTel);
	//cvSaveImage(strSavePath,pImage);
	delete[] pstrTel;
	delete[] pIamgePath;


	if(pImage)
		cvReleaseImage( &pImage);
}

//20151015 Template VMCC Save/Load
//CMiLRe 20151022 Template Group 단위로 삭제
//CMiLRe 20151119 Sensor Sync Check 하여 실제 제작해야할 DSC ID 설정
void CTimeLineEditor::MakeTampleteMake(int nStartDSC, int nEndDSC, int nGroupNO, vector<TEMPLATE_STRUCT> vTemplate)
{
	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
	int nSelectedDSC = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedDsc();
	CDSCItem* pItem = NULL;
	CString strFileName;

	//wgkim
	//if(ESMGetValue(ESM_VALUE_TEMPLATEPOINT) && ESMGetValue(ESM_VALUE_TEMPLATE_MODIFY))
	//{
	//	if(m_pTemplateMgr != NULL)
	//		m_pTemplateMgr->TemplateModifyForTargetDisappearingFromScreen(&vTemplate);
	//}

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	if(ESMGetValue(ESM_VALUE_CEREMONYUSE) ==TRUE && ESMGetValue(ESM_VALUE_CEREMONYPUTIMAGE) ==TRUE )
	{
		int nSelectLine = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectLine();
		pItem = (CDSCItem*)arDSCList.GetAt(nSelectLine);
		int nCurFrameIndex = ESMGetFrameIndex(nSelectedTime);

		int nIndex = nCurFrameIndex/30;
		int nFrameIndex = nCurFrameIndex%30;	
		strFileName.Format(_T("%s\\%s\\%s_%d.mp4"),ESMGetClientRamPath(pItem->m_strInfo[DSC_INFO_LOCATION]),ESMGetFrameRecord(),  pItem->GetDeviceDSCID(),nIndex);

		OutputImage(strFileName, nFrameIndex);

	}

	int nTemplate = 0;
	//CMiLRe 20151202 VMCC 템플리 호출 함수 추가
	for (vector<TEMPLATE_STRUCT>::const_iterator dit = vTemplate.begin(); dit!=vTemplate.end(); dit++)
	{		
#ifdef OPPONENT_SELECT
		//CMiLRe 20151022 Template Group 단위로 삭제
		if(dit->nEndDSC+nSelectedDSC > nEndDSC)
		{
			MakeTimeLine(dit->nStrartDSC+nSelectedDSC+nStartDSC, nEndDSC, dit->nStartTime+nSelectedTime, dit->nEndTime+nSelectedTime, dit->nFrameRate, nGroupNO, (vector<TEMPLATE_VMCC_STRUCT>*)&dit->nVMCC_Data);
		}
		else if(dit->nStrartDSC+nSelectedDSC+nStartDSC < 0)
		{
			MakeTimeLine(nStartDSC, nEndDSC, dit->nStartTime+nSelectedTime, dit->nEndTime+nSelectedTime, dit->nFrameRate, nGroupNO, (vector<TEMPLATE_VMCC_STRUCT>*)&dit->nVMCC_Data);
		}		
		else
		{
			MakeTimeLine(dit->nStrartDSC+nSelectedDSC+nStartDSC, dit->nEndDSC+nSelectedDSC, dit->nStartTime+nSelectedTime, dit->nEndTime+nSelectedTime, dit->nFrameRate, nGroupNO, (vector<TEMPLATE_VMCC_STRUCT>*)&dit->nVMCC_Data);
		}
#else
	
		//CMiLRe 20151202 템플릿 DSC NO 절대값으로 변경
		int nCompareStartDsc = -1;
		int nCompareEndDsc = -1;

		if(nStartDSC > dit->nStrartDSC)
		{
			nCompareStartDsc = nStartDSC;
			//
			ESMLog(0, _T("[Template Info]-[%d] DSC : %d, Template DSC : %d"), nTemplate, nStartDSC, dit->nStrartDSC);
			AfxMessageBox(_T("Template DSC count and DSC count are different"),MB_ICONSTOP);
		}
		else
			nCompareStartDsc = dit->nStrartDSC;
				
		if(nEndDSC < dit->nEndDSC)
		{
			nCompareEndDsc = nEndDSC;
			//
			ESMLog(0, _T("[Template Info]-[%d] DSC : %d, Template DSC : %d"), nTemplate, nEndDSC, dit->nEndDSC);
			AfxMessageBox(_T("Template DSC count and DSC count are different"),MB_ICONSTOP);
		}
		else
			nCompareEndDsc = dit->nEndDSC;
		
		MakeTimeLine(nCompareStartDsc, nCompareEndDsc, dit->nStartTime+nSelectedTime, dit->nEndTime+nSelectedTime, dit->nFrameRate, nGroupNO, dit->KzoneIndex, (vector<TEMPLATE_VMCC_STRUCT>*)&dit->nVMCC_Data, dit->nStepFrame);
		
		nTemplate++;
#endif
	}		
}

void CTimeLineEditor::MakeTampleteMake(int nStartDSC, int nEndDSC, int nSelectedTime, int nGroupNO, vector<TEMPLATE_STRUCT> vTemplate)
{
	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nSelectedDSC = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedDsc();

	//CMiLRe 20151202 VMCC 템플리 호출 함수 추가
	for (vector<TEMPLATE_STRUCT>::const_iterator dit = vTemplate.begin(); dit!=vTemplate.end(); dit++)
	{		
#ifdef OPPONENT_SELECT
		//CMiLRe 20151022 Template Group 단위로 삭제
		if(dit->nEndDSC+nSelectedDSC > nEndDSC)
		{
			MakeTimeLine(dit->nStrartDSC+nSelectedDSC+nStartDSC, nEndDSC, dit->nStartTime+nSelectedTime, dit->nEndTime+nSelectedTime, dit->nFrameRate, nGroupNO, (vector<TEMPLATE_VMCC_STRUCT>*)&dit->nVMCC_Data);
		}
		else if(dit->nStrartDSC+nSelectedDSC+nStartDSC < 0)
		{
			MakeTimeLine(nStartDSC, nEndDSC, dit->nStartTime+nSelectedTime, dit->nEndTime+nSelectedTime, dit->nFrameRate, nGroupNO, (vector<TEMPLATE_VMCC_STRUCT>*)&dit->nVMCC_Data);
		}		
		else
		{
			MakeTimeLine(dit->nStrartDSC+nSelectedDSC+nStartDSC, dit->nEndDSC+nSelectedDSC, dit->nStartTime+nSelectedTime, dit->nEndTime+nSelectedTime, dit->nFrameRate, nGroupNO, (vector<TEMPLATE_VMCC_STRUCT>*)&dit->nVMCC_Data);
		}
#else

		//CMiLRe 20151202 템플릿 DSC NO 절대값으로 변경
		int nCompareStartDsc = -1;
		int nCompareEndDsc = -1;

		if(nStartDSC > dit->nStrartDSC)
		{
			nCompareStartDsc = nStartDSC;
		}
		else
			nCompareStartDsc = dit->nStrartDSC;

		if(nEndDSC < dit->nEndDSC)
		{
			nCompareEndDsc = nEndDSC;
		}
		else
			nCompareEndDsc = dit->nEndDSC;

		MakeTimeLine(nCompareStartDsc, nCompareEndDsc, dit->nStartTime+nSelectedTime, dit->nEndTime+nSelectedTime, dit->nFrameRate, nGroupNO, dit->KzoneIndex, (vector<TEMPLATE_VMCC_STRUCT>*)&dit->nVMCC_Data);

#endif
	}	
}
	
void CTimeLineEditor::MakeBasicHG()
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nTotalRecTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	ESMGetPreviousFrameTime(nTotalRecTime);
	int nSelectedDsc = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedDsc();

	MakeTimeLine(nSelectedDsc, nSelectedDsc, 0, nTotalRecTime, 30);
	m_pTimeLineView->DrawEditor();
}
void CTimeLineEditor::MakeBasicTampleteFreze()
{
	if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
	{
		AfxMessageBox(_T("Current server mode is video production mode. \n Available in Record or Both mode only."));
		return;
	}
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nTotalRecTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	ESMGetPreviousFrameTime(nTotalRecTime);
	int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();

	int nStartTime = 0, nEndTime = 0;;
	nStartTime = nSelectedTime - ESMGetValue(ESM_VALUE_TEMPLATE_STARTTIME);
	nEndTime = nSelectedTime + ESMGetValue(ESM_VALUE_TEMPLATE_ENDTIME);

	if( nStartTime < 0)
		nStartTime = 0;
	if( nEndTime > nTotalRecTime)
		nEndTime = nTotalRecTime;

	CESMFileOperation fo;
	int nStartDsc = 0, nEndDsc = 0;
	CDSCItem* pItem = NULL;
	FFmpegManager FFmpegMgr(FALSE);
	CString strFileName, strDscIp;
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3, nFrameRate = 0;

//	int nMiddleDsc1 = arDSCList.GetCount() / 2 - 1;
//	int nMiddleDsc2 = arDSCList.GetCount() / 2;
	//마산
	int nMiddleDsc1 = arDSCList.GetCount() / 3 - 1;
	int nMiddleDsc2 = arDSCList.GetCount() / 3;

	for( int i = 0; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strDscIp.Format(_T("%s"), pItem->m_strInfo[DSC_INFO_LOCATION]);
			if(ESMGetExecuteMode())
				strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientBackupRamPath(strDscIp),  pItem->GetDeviceDSCID());
			else
				strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());
			FFmpegMgr.GetMovieInfo(strFileName, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
			if(nWidth != 0 && nHight != 0 && pItem->GetSensorSync() == TRUE)
			{
				nStartDsc = i;
				nWidth = 0;		nHight = 0;
				break;
			}
			nWidth = 0;		nHight = 0;
		}
	}
	for( int i = arDSCList.GetCount() - 1; i >= 0; i--)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strDscIp.Format(_T("%s"), pItem->m_strInfo[DSC_INFO_LOCATION]);
			if(ESMGetExecuteMode())
				strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientBackupRamPath(strDscIp),  pItem->GetDeviceDSCID());
			else
				strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());
			FFmpegMgr.GetMovieInfo(strFileName, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
			if(nWidth != 0 && nHight != 0 && pItem->GetSensorSync() == TRUE)
			{
				nEndDsc = i;
				nWidth = 0;		nHight = 0;
				break;
			}
			nWidth = 0;		nHight = 0;
		}
	}

	for( int i = nMiddleDsc2; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strDscIp.Format(_T("%s"), pItem->m_strInfo[DSC_INFO_LOCATION]);
			if(ESMGetExecuteMode())
				strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientBackupRamPath(strDscIp),  pItem->GetDeviceDSCID());
			else
				strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());
			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nMiddleDsc2 = i;
				nWidth = 0;		nHight = 0;
				break;
			}

			nWidth = 0;		nHight = 0;
		}
	}
	for( int i = nMiddleDsc1; i >= 0; i--)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strDscIp.Format(_T("%s"), pItem->m_strInfo[DSC_INFO_LOCATION]);
			if(ESMGetExecuteMode())
				strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientBackupRamPath(strDscIp),  pItem->GetDeviceDSCID());
			else
				strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());

			if(fo.IsFileExist(strFileName) && pItem->GetSensorSync() == TRUE)
			{
				nMiddleDsc1 = i;
				nWidth = 0;		nHight = 0;
				break;
			}
			nWidth = 0;		nHight = 0;
		}
	}	
 

	MakeTimeLine(nEndDsc, nEndDsc, nStartTime, nSelectedTime, nAsianGameFrameSpeed * 2);
	MakeTimeLine(nEndDsc, nEndDsc, nSelectedTime, nSelectedTime, 200);
	//TimeSlice
	MakeTimeLine(nEndDsc , nStartDsc  , nSelectedTime, nSelectedTime, 60);
	
	MakeTimeLine(nStartDsc , nStartDsc  , nSelectedTime, nSelectedTime, 1500);

	MakeTimeLine(nStartDsc , nEndDsc , nSelectedTime, nSelectedTime, 60);

	MakeTimeLine(nEndDsc , nStartDsc  , nSelectedTime, nSelectedTime, 60);

	MakeTimeLine(nStartDsc , nStartDsc  , nSelectedTime, nSelectedTime, 1500);

	MakeTimeLine(nStartDsc , nEndDsc , nSelectedTime, nSelectedTime, 60);
	//후진행
	MakeTimeLine(nEndDsc, nEndDsc, nSelectedTime, nSelectedTime, 200);
	MakeTimeLine(nEndDsc, nEndDsc, nSelectedTime, nEndTime, nAsianGameFrameSpeed * 2);
	m_pTimeLineView->DrawEditor();


















	/*
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nTotalRecTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	ESMGetPreviousFrameTime(nTotalRecTime);
	int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();

	int nStartTime = 0, nEndTime = 0;;
	nStartTime = nSelectedTime - ESMGetValue(ESM_VALUE_TEMPLATE_STARTTIME);
	nEndTime = nSelectedTime + ESMGetValue(ESM_VALUE_TEMPLATE_ENDTIME);

	if( nStartTime < 0)
		nStartTime = 0;
	if( nEndTime > nTotalRecTime)
		nEndTime = nTotalRecTime;


	int nStartDsc = 0, nEndDsc = 0;
	CDSCItem* pItem = NULL;
	FFmpegManager FFmpegMgr(FALSE);
	CString strFileName, strDscIp;
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3, nFrameRate = 0;
	for( int i = 0; i< arDSCList.GetCount(); i++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strDscIp.Format(_T("%s"), pItem->m_strInfo[DSC_INFO_LOCATION]);
			if(ESMGetExecuteMode())
				strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientBackupRamPath(strDscIp),  pItem->GetDeviceDSCID());
			else
				strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());
			FFmpegMgr.GetMovieInfo(strFileName, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
			if(nWidth != 0 && nHight != 0)
			{
				nStartDsc = i;
				break;
			}
		}
	}
	nWidth = 0;
	nHight = 0;
	for( int i = arDSCList.GetCount() - 1; i >= 0; i--)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(i);
		if( pItem )
		{
			strDscIp.Format(_T("%s"), pItem->m_strInfo[DSC_INFO_LOCATION]);
			if(ESMGetExecuteMode())
				strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientBackupRamPath(strDscIp),  pItem->GetDeviceDSCID());
			else
				strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());
			FFmpegMgr.GetMovieInfo(strFileName, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
			if(nWidth != 0 && nHight != 0)
			{
				nEndDsc = i;
				break;
			}
		}
	}

	//전진행
	MakeTimeLine(nStartDsc, nEndDsc, nSelectedTime , nSelectedTime , ESMGetValue(ESM_VALUE_ADJ_FLOW_T));
	 
	m_pTimeLineView->DrawEditor();
	*/
}

void CTimeLineEditor::MakeBasicTampleteFrezeRe()
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	int nTotalRecTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetRecTime();
	ESMGetPreviousFrameTime(nTotalRecTime);
	int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
	MakeTimeLine(arDSCList.GetCount() - 1, 0, nSelectedTime, nSelectedTime, ESMGetValue(ESM_VALUE_ADJ_FLOW_T));
	m_pTimeLineView->DrawEditor();
}

void CTimeLineEditor::MakeTimeLine(int nDscStart, int nDscEnd, int nStartTime, int nEndTime, int nNextTime)
{
	ESMCheckFrameTime(nStartTime);
	ESMCheckFrameTime(nEndTime);
	CObArray arDSC;
	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	pMainWnd->m_wndDSCViewer.m_pFrameSelector->InsertDSCList(&arDSC, nDscStart, nDscEnd);
	CESMTimeLineObjectEditor* pObjEdit = new CESMTimeLineObjectEditor(arDSC, nStartTime, nEndTime, movie_next_frame_time);
	pObjEdit->SetNextTime(nNextTime);
	m_pTimeLineView->m_nInsertObjPos = m_pTimeLineView->GetObjCount();
	m_pTimeLineView->AddObj(pObjEdit);
}


//20151015 Template VMCC Save/Load
//CMiLRe 20151022 Template Group 단위로 삭제
void CTimeLineEditor::MakeTimeLine(int nDscStart, int nDscEnd, int nStartTime, int nEndTime, int nNextTime, int nGroupNO, int KzoneIdx, vector<TEMPLATE_VMCC_STRUCT>* verVMCC_Data, int nStepFrame)
{

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	if(nDscEnd > arDSCList.GetCount())
		nDscEnd = arDSCList.GetCount()-1;
	if(nDscStart > arDSCList.GetCount())
		nDscStart = arDSCList.GetCount()-1;

	ESMCheckFrameTime(nStartTime);
	ESMCheckFrameTime(nEndTime);
	CObArray arDSC;
	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	pMainWnd->m_wndDSCViewer.m_pFrameSelector->InsertDSCList(&arDSC, nDscStart, nDscEnd);

	//CMiLRe 20151022 Template Group 단위로 삭제
	CESMTimeLineObjectEditor* pObjEdit = new CESMTimeLineObjectEditor(arDSC, nStartTime, nEndTime, movie_next_frame_time, nGroupNO ,nStepFrame);
	pObjEdit->SetNextTime(nNextTime);
	pObjEdit->SetKzoneIdx(KzoneIdx);
	m_pTimeLineView->m_nInsertObjPos = m_pTimeLineView->GetObjCount();

	//20151015 Template VMCC Save/Load
	if(verVMCC_Data->size() == 0)
		m_pTimeLineView->AddObj(pObjEdit);
	else
		m_pTimeLineView->AddObj(pObjEdit, m_pTemplateMgr, verVMCC_Data);
}

void CTimeLineEditor::OnItemRightMove()
{
	int iSelectedIndx = 0;
	int temp = 0;
	for(int i =0 ; i < m_pTimeLineView->GetObjCount(); i++)
	{
		CESMTimeLineObjectEditor* pObjectEditor = m_pTimeLineView->GetObj(i);
		CString strTp;

		if( pObjectEditor->GetBK() == TIMELINE_BACKGROUND_ALL)
		{
			iSelectedIndx = i;
		}
	}
	// 여기서 부터 작성.
	CString idx;
	idx.Format(_T("%d"), iSelectedIndx);

	if(iSelectedIndx == m_pTimeLineView->GetObjCount()-1)
		return;

	m_pTimeLineView->Swap(iSelectedIndx, iSelectedIndx +1);

	m_pTimeLineView->DrawEditor();
}

void CTimeLineEditor::OnItemDeleteAll()
{
	int nRet = MessageBox(_T("Are you sure you want to delete all?"),_T("Question"), MB_YESNO);
	if (nRet == IDYES)
	{
		ItemAllDelete();
	}
	return;
}

//CMiLRe 20151013 Template Load INI File
void CTimeLineEditor::OnItemTemplateRefresh()
{
	//CMiLRe 20151014 Template Save INI File
	ResetTemplate();
	ResetViewTemplate();
	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	if(pMainWnd)
	{
		pMainWnd->GetESMOption()->LoadTemplateFormFile();
	}
}
void CTimeLineEditor::OnItemTemplateTimeSync()
{
	//m_pTimeLineView->ReCalculateTime();

	int nStart, nEnd;
	CPoint ptS, ptE;
	int nAll = m_pTimeLineView->m_arObject.GetCount();
	CESMTimeLineObjectEditor* pObj = NULL;
	int nGap = 0;

	for(int i = 0; i < nAll; i++)
	{
		pObj = m_pTimeLineView->GetObj(i);
		if(pObj)
		{
			nStart = pObj->GetStartTime();
			nEnd = pObj->GetEndTime();

			if(i == 0)
			{
				if(nStart < 0)
					nGap = abs(nStart);
				else if(nStart > 0)
					nGap = -1 * nStart;
			}

			ptS.x = ESMGetXFromTime(nStart + nGap);
			ptS.y = ESMGetYFromLine(pObj->m_nStartDSC - 1);

			ptE.x = ESMGetXFromTime(nEnd + nGap);
			ptE.y = ESMGetYFromLine(pObj->m_nEndDSC - 1);

			pObj->m_startPos	= ptS;
			pObj->m_EndPos		= ptE;
		}
	}

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_DRAW_TIMELINE;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	
}

//CMiLRe 20151014 Template Save INI File
void CTimeLineEditor::OnItemTemplateSave()
{
	if(m_pTimeLineView->GetObjCount() == 0 || m_pTimeLineView == NULL)
	{
		AfxMessageBox(_T("Template 가 없습니다."));
		return;
	}

	CString szFilter = _T("Template Files (*.mvtm)|*.mvtm|");
	CString strTitle = _T("Template Files Save");

#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	CFileDialog dlg(FALSE, NULL, NULL, OFN_NOCHANGEDIR | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter, NULL, 0, bVistaStyle );  

	CString strRecordFolder;
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_MOVIE)
		strRecordFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_MOVIE_TEMPLATE));
	else
		strRecordFolder.Format(_T("%s\\"),ESMGetPath(ESM_PATH_PICTURE_TEMPLATE));

	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = strRecordFolder;	

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName, strFilePath, strFolder;
		strFolder = dlg.GetPathName();

		//strFileName = dlg.GetFileTitle();
		//strFolder = dlg.GetFolderPath();
		//strFilePath.Format(_T("%s%s.mvtm"), strRecordFolder,strFileName);
		//strFilePath.Format(_T("%s\\%s.mvtm"), strFolder,strFileName);

		//jhhan
#if _COMPARE
		CString strExt;
		strExt = dlg.GetFileExt();
		if(strExt.CompareNoCase(_T("mvtm")) != 0)			//확장자 유무 확인 후 파일명 명시
			strFilePath.Format(_T("%s.mvtm"), strFolder);
		else
			strFilePath.Format(_T("%s"), strFolder);
#else
		strFolder.TrimRight(_T(".mvtm"));					//문자열 속 .mvtm 전체삭제
		strFilePath.Format(_T("%s.mvtm"), strFolder);
#endif


		//strFilePath.Format(_T("%s.mvtm"), strFolder);
		TemplateDataSave(strFilePath);
		ESMLog(1, _T("Template Save [%s]"),strFilePath);
	}
}

//CMiLRe 20151014 Template Save INI File
void CTimeLineEditor::TemplateDataSave(CString strFilename)
{	

	CESMIni ini;
	ini.SetIniFilename(strFilename, TRUE);
	
	CFile file;

	if(!file.Open(strFilename, CFile::modeCreate | CFile::modeReadWrite |CFile::modeNoTruncate))
	{
		ESMLog(0, _T("Template Save [%s]"),strFilename);
		return ;
	}
	file.Close();


	CESMTimeLineObjectEditor* pTimeLineObject = NULL;
	//20151015 Template VMCC Save/Load
	CESMObjectFrame* pTimeLineEffectObject = NULL;
	CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();

	for( int i = 0; i< m_pTimeLineView->GetObjCount(); i++)
	{
		pTimeLineObject = m_pTimeLineView->GetObj(i);
		int nStartTime = pTimeLineObject->GetStartTime();
		int nEndTime = pTimeLineObject->GetEndTime();
		int nkzoneIdx = pTimeLineObject->GetKzoneIdx();

		CDSCItem*  pStartDsc= pTimeLineObject->GetDSC(0);
		//jhhan 180614 = 1 base change
		int nStartDsc = pStartDsc->GetRow() + 1;
		CDSCItem*  pEndDsc= pTimeLineObject->GetLastDSC();
		//jhhan 180614 = 1 base change
		int nEndDsc = pEndDsc->GetRow() + 1;

		int nTimeFrame = pTimeLineObject->GetNextTime();		

		CString strSectionNO, strStartTime, strStartDsc, strEndTime, strEndDsc,
			strTimeFrame, strVMCC_Cnt, strVmcc, strKzoneIdx, strStepFrame;
		
		//20151015 Template VMCC Save/Load
		int nSelectedTime = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedTime();
		int nSelectedDSC = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedDsc();
		
		int sdf = pMainWnd->m_wndDSCViewer.m_pFrameSelector->GetSelectedDsc();
		int nStepFrame =  pTimeLineObject->GetStepFrame();
		strSectionNO.Format(_T("%d"), i);
#if 0
		strStartTime.Format(_T("%d"), nStartTime-nSelectedTime);
		strStartDsc.Format(_T("%d"), nStartDsc-nSelectedDSC);
		strEndTime.Format(_T("%d"), nEndTime-nSelectedTime);
		strEndDsc.Format(_T("%d"), nEndDsc-nSelectedDSC);
		strTimeFrame.Format(_T("%d"), nTimeFrame);
#else
		//CMiLRe 20151202 템플릿 DSC NO 절대값으로 변경
		strStartTime.Format(_T("%d"), nStartTime-nSelectedTime);
		strStartDsc.Format(_T("%d"), nStartDsc);
		strEndTime.Format(_T("%d"), nEndTime-nSelectedTime);
		strEndDsc.Format(_T("%d"), nEndDsc);
		strTimeFrame.Format(_T("%d"), nTimeFrame);
		strKzoneIdx.Format(_T("%d"), nkzoneIdx);
		strStepFrame.Format(_T("%d"), nStepFrame);
#endif

		ini.WriteString(strSectionNO, TEMPLATE_ENTRY_KZONE, strKzoneIdx);
		ini.WriteString(strSectionNO, TEMPLATE_ENTRY_STARTTIME, strStartTime);
		ini.WriteString(strSectionNO, TEMPLATE_ENTRY_START_DSC, strStartDsc);
		ini.WriteString(strSectionNO, TEMPLATE_ENTRY_ENDTIME, strEndTime);
		ini.WriteString(strSectionNO, TEMPLATE_ENTRY_END_DSC, strEndDsc);
		ini.WriteString(strSectionNO, TEMPLATE_ENTRY_TIMEFRAME, strTimeFrame);
		ini.WriteString(strSectionNO, TEMPLATE_ENTRY_STEP_FRAME, strStepFrame);

#if 1 //New Template 18.03.14 joonho
		CString strVmccSection;
		ini.WriteString(strSectionNO, TEMPLATE_ENTRY_VMCC_COUNT, _T("2"));
		strVmccSection.Format(_T("%s_%d"),TEMPLATE_ENTRY_VMCC, 0);
		strVmcc.Format(_T("0.0:1920:1080:%d:1.0"),pTimeLineObject->GetStartZoom());
		ini.WriteString(strSectionNO, strVmccSection, strVmcc);

		strVmccSection.Format(_T("%s_%d"),TEMPLATE_ENTRY_VMCC, 1);
		strVmcc.Format(_T("1.0:1920:1080:%d:1.0"),pTimeLineObject->GetEndZoom());
		ini.WriteString(strSectionNO, strVmccSection, strVmcc);

		
#else
		int nVmccDataCount = 0;
		int nCnt = pTimeLineObject->GetObjectFrameCount();		
		strVMCC_Cnt.Format(_T("%d"), nCnt);
		ini.WriteString(strSectionNO, TEMPLATE_ENTRY_VMCC_COUNT, strVMCC_Cnt);

		for(int k = 0 ; k < nCnt ; k++)
		{
			pTimeLineEffectObject = pTimeLineObject->GetObjectFrame(k);

			if(pTimeLineEffectObject->IsSpot())
			{

				int nOrder = pTimeLineObject->GetObjectFrameOrder(pTimeLineEffectObject);
				double dRatioTime;
				if ( nCnt == nOrder + 1 )
					dRatioTime = 1;
				else
					dRatioTime = (double)nOrder / (double)nCnt;
				//double dRatioX	  = (double)(pTimeLineEffectObject->GetPosX()) / (double)FULL_HD_WIDTH;
				//double dRatioY	  = (double)(pTimeLineEffectObject->GetPosY()) / (double)FULL_HD_HEIGHT;

				double dRatioX	  = (double)(pTimeLineEffectObject->GetPosX());
				double dRatioY	  = (double)(pTimeLineEffectObject->GetPosY());

				int nZoom	= pTimeLineEffectObject->GetZoomRatio();
				float dWeight	= pTimeLineEffectObject->GetZoomWeight();

				CString strVmccSection;
				strVmccSection.Format(_T("%s_%d"),TEMPLATE_ENTRY_VMCC, nVmccDataCount);
				strVmcc.Format(_T("%.16lf:%.16lf:%.16lf:%d:%.8lf"), dRatioTime, dRatioX ,dRatioY ,nZoom, dWeight);

				ini.WriteString(strSectionNO, strVmccSection, strVmcc);
				nVmccDataCount++;
			}
			strVMCC_Cnt.Format(_T("%d"), nVmccDataCount);
			ini.WriteString(strSectionNO, TEMPLATE_ENTRY_VMCC_COUNT, strVMCC_Cnt);
		}
#endif
	}
}

void CTimeLineEditor::ItemAllDelete()
{
	//-- Remove This
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_REMOVEALL_OBJ_EDITOR;
	pMsg->pDest		= (LPARAM)this;

	//CESMTimeLineObject::OnRButtonDown(nFlags, point);	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);	
	return;
}

void  CTimeLineEditor::OnDSCADJMakeMovie()
{
	ESMLog(5,_T("Adjust Movie Make"));
	m_pTimeLineView->MakeAdjustMovie();
}

void  CTimeLineEditor::OnDSCMakeMovie(void)				
{
	////////////////////////////////////////////////////////////////////////// canon selphy	//  [2018/10/2/ stim]
	BOOL bUseCanonSelphy = ESMGetValue(ESM_VALUE_CEREMONY_USE_CANON_SELPHY);
	if (bUseCanonSelphy)
	{
		CString strSelphyPath;
		strSelphyPath.Format(_T("%s\\selphy.jpg"), (ESMGetPath(ESM_PATH_HOME)));
		CPrinterMgr printMgr;
		printMgr.PrintStart(_T("Canon SELPHY CP900"), strSelphyPath, CPrinterMgr::IMAGE_ZOOM_NORMAL);			
	}
	//////////////////////////////////////////////////////////////////////////

	//jhhan 170713 / 25P
	ESMSetFrameRate(ESMGetFrameRate());

	//joonho.kim 07.04.11
	ESMSetMakeMovie(TRUE);

	//jhhan 16-10-12
	ESMGetDeleteDSC();

	ESMSetRecordingInfoint(_T("MakingFlag"), TRUE);

	if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
	{
		AfxMessageBox(_T("Current server mode is video production mode. \n Available in Record or Both mode only."));
		ESMSetRecordingInfoint(_T("MakingFlag"), FALSE);

		//jhhan m_bWait = FALSE 설정
		if(ESMGetBaseBallFlag())
		{
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_MOVIE_MAKE_WAIT_END;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}

		return;
	}

	//if(ESMGetExecuteMode())
	if(ESMGetExecuteMode() && ESMGetAdjustLoad() == FALSE) //2017.02.02 joonho.kim Load Adj 중복방지
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent;
		pMsg->message = WM_ESM_VIEW_SETNEWESTADJ;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	}
	if( m_pTimeLineView->GetMovieMaking())
	{
		ESMSetRecordingInfoint(_T("MakingFlag"), FALSE);

		//jhhan m_bWait = FALSE 설정
		if(ESMGetBaseBallFlag())
		{
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_MOVIE_MAKE_WAIT_END;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}

		return;
	}

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	if( arDSCList.GetCount() < 1 || m_pTimeLineView->GetObjCount() <1)
	{
		ESMSetRecordingInfoint(_T("MakingFlag"), FALSE);

		//jhhan m_bWait = FALSE 설정
		if(ESMGetBaseBallFlag())
		{
			ESMEvent* pMsg = new ESMEvent;
			pMsg->message = WM_ESM_MOVIE_MAKE_WAIT_END;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}

		return ;
	}


	int cnt = 0;
	for(int i=0;i<arDSCList.GetCount();i++)
	{
		CDSCItem* pDSCItem = (CDSCItem*)arDSCList.GetAt(i);

		if(pDSCItem->m_nAdj[DSC_ADJ_A] == 0.0)	cnt++;	

	}
	if(cnt  ==  arDSCList.GetCount())
	{
		int nRet = MessageBox(_T(" Unload Adjust File!!!\n Do you want to continue?"),_T("Adjust Question"), MB_YESNO);
		if (nRet == IDNO)
		{
			ESMSetRecordingInfoint(_T("MakingFlag"), FALSE);

			//jhhan m_bWait = FALSE 설정
			if(ESMGetBaseBallFlag())
			{
				ESMEvent* pMsg = new ESMEvent;
				pMsg->message = WM_ESM_MOVIE_MAKE_WAIT_END;
				::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
			}
			//jhhan RemoveAll
			//ItemAllDelete();

			return;
		}
	}
	// && pDSCItem->m_nAdj[DSC_ADJ_X] == 0.0 && pDSCItem->m_nAdj[DSC_ADJ_Y] == 0.0 
	//&& pDSCItem->m_nAdj[DSC_ADJ_RX] == 0.0 && pDSCItem->m_nAdj[DSC_ADJ_RY] == 0.0 && pDSCItem->m_nAdj[DSC_ADJ_SCALE] == 0.0 
	//&& pDSCItem->m_nAdj[DSC_ADJ_DISTANCE] == 0.0 
	m_pTimeLineView->SetMovieMaking(TRUE);

	//Make Time
	{
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_MOVIE_START_MOVIE_MAKE;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
	}

	if(ESMGetValue(ESM_VALUE_TEST_MODE) == TRUE)
		ESMLog(5,_T("[TC2] Start"));
	ESMLog(5,_T("Movie Make"));

	

	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
	{
		m_pTimeLineView->MakePictureMovie();
		ESMSetRecordingInfoint(_T("MakingFlag"), FALSE);
	}
	else
		m_pTimeLineView->MakeMovie();
}

void  CTimeLineEditor::OnDSCMakeMovie3D(void)				
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	if( arDSCList.GetCount() < 1 || m_pTimeLineView->GetObjCount() <1)
		return ;

	CDSCItem* pDSCItem = (CDSCItem*)arDSCList.GetAt(0);
	if(pDSCItem->m_nAdj[DSC_ADJ_A] == 0.0)
	{
		int nRet = MessageBox(_T(" Unload Adjust File!!!\n Do you want to continue?"),_T("Adjust Question"), MB_YESNO);
		if (nRet == IDNO)
			return;
	}

	ESMLog(5,_T("3D Movie Make"));
	if(ESMGetFilmState() == ESM_ADJ_FILM_STATE_PICTURE)
		m_pTimeLineView->MakePicture3DMovie();
	else
		m_pTimeLineView->Make3DMovie();		
}


void  CTimeLineEditor::MovieStop(const CString& strPlayerName, UINT RunningTime, UINT nInterval)
{
	//	타이머 이벤트를 이용하여 입력 받은 인터벌 후 함수 자동 실행
	m_tTimerEx.Stop();
	//	자동 실행 될 함수 설정
	m_tTimerEx.SetTimedEvent(this, &CTimeLineEditor::SendMovieStopMessage);
	//	1. Interval in ms
	//	2. TRUE to call first event immediately
	//	3. TRUE to call timed event only once
	m_tTimerEx.Start(RunningTime + nInterval, FALSE, TRUE);
}

void  CTimeLineEditor::SendMovieStopMessage()
{
	DWORD	dwError = -1;
	HWND	hWnd	= NULL;
	hWnd	= ::FindWindow(_T("PotPlayer"), _T("4DMaker.mp4 - 다음 팟플레이어"));
	if(!hWnd)
		hWnd	= ::FindWindow(_T("PotPlayer64"), _T("4DMaker.mp4 - 다음 팟플레이어"));
	//	팟 플레이어 메인 핸들에 F4 키 메세지 전달
	//	플레이이 중 또는 플레이가 끝난 동영상을 닫는다.
	::PostMessage(hWnd, WM_KEYDOWN, VK_F4, 0x003E0001);
	Sleep(10);
	::SendMessage(hWnd, WM_COMMAND, 0x000127B7, 0x00000000);
	Sleep(10);
	::PostMessage(hWnd, WM_KEYUP, VK_F4, 0xC03E0001);

	dwError = GetLastError();

}
//CMiLRe 20151119 영상 저장 경로 변경
void CTimeLineEditor::DSCMoviePlay(CString strPath)
{	
	DWORD nCurrentTime;
	FFmpegManager	mpegMgr(FALSE);
	int nRunningTime = mpegMgr.GetRunningTime(strPath);

	ESMLog(5,_T("Movie Play1 [%s]"),strPath);

	while(1)
	{
		nCurrentTime = GetTickCount();
		if(nCurrentTime > (m_nMakeTime + m_nRunningTime))
		{
			ShellExecute(NULL ,_T("open"), strPath, NULL, NULL, SW_SHOWNORMAL);
			break;
		}
		//ESMLog(1,_T("Wait Time %d %d"), nCurrentTime, m_nMakeTime+m_nRunningTime);
		Sleep(100);
	}

	HANDLE hFocusObserver = NULL;
	hFocusObserver = (HANDLE) _beginthreadex(NULL, 0, FocusObserverThread, (void *)this, 0, NULL);
	CloseHandle(hFocusObserver);

	m_nMakeTime = GetTickCount();
	m_nRunningTime = nRunningTime;
	//	타이머 이벤트를 이용하여 인터벌 후 함수 자동 실행
	MovieStop(_T("PotPlayer"), nRunningTime * 1 , 4500);	
}

void  CTimeLineEditor::OnDSCMoviePlay()				
{		
	//-- 2013-10-04 hongsu@esmlab.com
	//-- Play Movie File
	CString strFile;
	strFile.Format(_T("\"%s\\"),ESMGetPath(ESM_PATH_OUTPUT));
	if(!m_pTimeLineView->m_bCreate3D)
		strFile.Append(_T("2D\\"));
	else
		strFile.Append(_T("3D\\"));

	//-- 2013-10-09 jaehyun@esmlab.com
	//-- Output Type option
// 	BOOL bOutputMP4 = ESMGetValue(ESM_VALUE_ADJ_OUT_MP4);
// 	if(bOutputMP4)
// 		strFile.Append(_T("4dmaker.mp4\""));
// 	else
// 		strFile.Append(_T("4dmaker.ts\""));
	strFile.Append(_T("4dmaker.mp4\""));


////////////////////////////////////////////////////////////////////////
//	2014.11.04	Ryumin

	CString strTmp(strFile);
	strTmp.Replace(_T("\""), _T(""));
	FFmpegManager	mpegMgr(FALSE);
	int nRunningTime = mpegMgr.GetRunningTime(strTmp);

	ESMLog(5,_T("Movie Play2 [%s]"),strFile);
	ShellExecute(NULL ,_T("open"), strFile, NULL, NULL, SW_SHOWNORMAL);

	HANDLE hFocusObserver = NULL;
	hFocusObserver = (HANDLE) _beginthreadex(NULL, 0, FocusObserverThread, (void *)this, 0, NULL);
	CloseHandle(hFocusObserver);

	//	타이머 이벤트를 이용하여 인터벌 후 함수 자동 실행
	MovieStop(_T("PotPlayer"), nRunningTime * 1 , 4500);

//
////////////////////////////////////////////////////////////////////////

}

unsigned WINAPI CTimeLineEditor::FocusObserverThread(LPVOID param)
{	
	CTimeLineEditor* pTimeLineEditor = (CTimeLineEditor*)param;
	HWND hWnd = NULL;
	int nRepeatCount =500;
	while(nRepeatCount > 0)
	{
		HWND hWnd = NULL;
		hWnd = ::FindWindow(_T("PotPlayer"), NULL);
		HWND hWnd2 = NULL;
		hWnd2 = ::GetForegroundWindow(); 
		if( hWnd == hWnd2)
		{
			HWND hSlectorWnd = pTimeLineEditor->m_pMainWnd->m_wndDSCViewer.m_pFrameSelector->m_hWnd;
 			::SetFocus(hSlectorWnd);
 			::SetForegroundWindow(hSlectorWnd);
 			::SetActiveWindow(hSlectorWnd);
			break;
		}
		Sleep(10);
		nRepeatCount--;
	}
	return 0;
}

//-- 2013-12-12 kjpark@esmlab.com
//-- if converting, toolbar disenable
void CTimeLineEditor::SetToolbarShowNHide(BOOL isShow)
{
	m_wndToolBar.EnableWindow(isShow);
}

void CTimeLineEditor::SaveTimeLineInfo(CArchive& ar)
{
	CESMTimeLineObjectEditor* pTimeLineObject = NULL;
	int nObjectCount = m_pTimeLineView->GetObjCount() ;
	ar<<nObjectCount;
	for( int i = 0; i< m_pTimeLineView->GetObjCount(); i++)
	{
		pTimeLineObject = m_pTimeLineView->GetObj(i);
		int nStartTime = pTimeLineObject->GetStartTime();
		ar<<nStartTime;
		int nEndTime = pTimeLineObject->GetEndTime();
		ar<<nEndTime;


 		CDSCItem*  pStartDsc= pTimeLineObject->GetDSC(0);
		if(pStartDsc == NULL)
		{
			ESMLog(0,_T("[SaveTimeLineInfo] - pStartDsc Error"));
			ar<< 0;
		}
		else
		{
			int nStartDsc = pStartDsc->GetRow();
			ar<<nStartDsc;
		}
		
 		CDSCItem*  pEndDsc= pTimeLineObject->GetLastDSC();
		if(pEndDsc == NULL)
		{
			ESMLog(0,_T("[SaveTimeLineInfo] - pEndDSC Error"));
			ar << 0;
		}
		else
		{
			int nEndDsc = pEndDsc->GetRow();
			ar<<nEndDsc;
		}
	}
}

void CTimeLineEditor::LoadTimeLineInfo(CArchive& ar, float nVersion)
{
	CESMTimeLineObjectEditor* pTimeLineObject = NULL;
	int nObjectCount = 0;
	if( ESMGetVersion(nVersion) >= ESM_4DMAKER_VERSION_10002)
	{
		ar>>nObjectCount;
		for( int i = 0; i< nObjectCount; i++)
		{
			pTimeLineObject = m_pTimeLineView->GetObj(i);
			int nStartTime = 0, nEndTime = 0, nStartDsc = 0, nEndDsc = 0;
			ar>>nStartTime;
			ar>>nEndTime;
			ar>>nStartDsc;
			ar>>nEndDsc;

			MakeTimeLine(nStartDsc, nEndDsc, nStartTime, nEndTime);
		}
	}
	m_pTimeLineView->DrawEditor();
}

void CTimeLineEditor::SaveMovieTemplate(CArchive& ar)
{
	SaveTimeLineTemplate(ar);
	int nObjCnt = m_pTimeLineView->GetObjCount();

	CESMTimeLineObjectEditor* pObj = NULL;

	int nEffectObjCnt = 0;
	for ( int i = 0 ; i < nObjCnt ; i++ )
	{
		pObj = m_pTimeLineView->GetObj(i);

		if ( pObj->IsEffectAdded() == TRUE )
			nEffectObjCnt++;
	}

	// Write Effected TimeLine Count
	ar << nEffectObjCnt;

	for ( int i = 0 ; i < nObjCnt ; i++ )
	{
		pObj = m_pTimeLineView->GetObj(i);
		
		if ( pObj->IsEffectAdded() == TRUE )
		{
			// Write TimeLine Index
			ar << i;
			pObj->SaveEffectInfo(ar);
		}
	}
}

void CTimeLineEditor::LoadMovieTemplate(CArchive& ar)
{
	LoadTimeLineTemplate(ar);
	int nEffectObjCnt = 0; 
	ar >> nEffectObjCnt;

	CESMTimeLineObjectEditor* pObj = NULL;

	for ( int i = 0 ; i < nEffectObjCnt ; i++ )
	{
		int nObjIndex;
		ar >> nObjIndex;
		pObj = m_pTimeLineView->GetObj(nObjIndex);

		pObj->LoadEffectInfo(ar);
	}

	// UI Update
	//CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
	//pMainWnd->m_wndEffectEditor.UpdateFrames();

	pObj = m_pTimeLineView->GetSelectedObj();
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_EFFECT_UPDATE;
	pMsg->pParam	= (LPARAM)pObj;		
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	

	pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_EFFECT_SPOTLIST_UPDATE;
	pMsg->pParam	= (LPARAM)pObj;		
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_EFFECT, (LPARAM)pMsg);	

}

BOOL CTimeLineEditor::CheckSaveValidation()
{
	int nSelDscIdx, nSelTimeIdx,nTotalDscCnt,nTotalTimeCnt;

	nTotalTimeCnt = ESMGetFrameIndex(ESMGetRecTime()) + 1;
	nTotalDscCnt = ESMGetDSCCount();

	int nSelTime;
	CString strSelDsc;
	ESMGetSelectedFrame(strSelDsc, nSelTime);
	nSelDscIdx = ESMGetDSCIndex(strSelDsc);
	nSelTimeIdx = ESMGetFrameIndex(nSelTime);

	int nObjCnt = m_pTimeLineView->GetObjCount();
	CESMTimeLineObjectEditor* pObj = NULL;

	BOOL bExistFrame = FALSE;
	for ( int i = 0 ; i < nObjCnt ; i++ )
	{
		pObj = m_pTimeLineView->GetObj(i);
		if ( pObj->IsExistFrame(nSelDscIdx, nSelTimeIdx) == TRUE )
			bExistFrame = TRUE;
	}

	if ( bExistFrame == FALSE )
	{
		AfxMessageBox(_T("타임라인 안의 기준점을 선택해 주세요."));
		return FALSE;
	}

	return TRUE;
}

void CTimeLineEditor::SaveTimeLineTemplate(CArchive& ar)
{
	int nSelDscIdx, nSelTimeIdx,nTotalDscCnt,nTotalTimeCnt;
	double dRatioCDsc, dRatioCTime;

	nTotalTimeCnt = ESMGetFrameIndex(ESMGetRecTime()) + 1;
	nTotalDscCnt = ESMGetDSCCount();

	int nSelTime;
	CString strSelDsc;
	ESMGetSelectedFrame(strSelDsc, nSelTime);
	nSelDscIdx = ESMGetDSCIndex(strSelDsc);
	nSelTimeIdx = ESMGetFrameIndex(nSelTime);

	int nObjCnt = m_pTimeLineView->GetObjCount();
	CESMTimeLineObjectEditor* pObj = NULL;
		
	ar<<nObjCnt;
	for ( int i = 0 ; i < nObjCnt ; i++ )
	{
		pObj = m_pTimeLineView->GetObj(i);
		int nStartTime = ESMGetFrameIndex(pObj->GetStartTime());
		int nEndTime = ESMGetFrameIndex(pObj->GetEndTime());

		CDSCItem*  pStartDsc = pObj->GetDSC(0);
		CDSCItem*  pEndDsc = pObj->GetLastDSC();

		int nStartDsc = pStartDsc->GetRow();
		int nEndDsc = pEndDsc->GetRow();

		int nDStartTime = nStartTime - nSelTimeIdx;
		//int nDStartDsc = nStartDsc - nSelDscIdx;
		int nDStartDsc = nStartDsc;
		int nDEndTime = nEndTime - nSelTimeIdx;
		//int nDEndDsc = nEndDsc - nSelDscIdx;
		int nDEndDsc = nEndDsc;

		double dRatioStartDsc, dRatioEndDsc, dRatioStartTime, dRatioEndTime;
		dRatioStartDsc = (double)(nDStartDsc) / (double)nTotalDscCnt;
		dRatioEndDsc = (double)(nDEndDsc) / (double)nTotalDscCnt;
		dRatioStartTime = nDStartTime;
		dRatioEndTime = nDEndTime;
		//dRatioStartTime = (double)(nDStartTime) / (double)nTotalTimeCnt;
		//dRatioEndTime = (double)(nDEndTime) / (double)nTotalTimeCnt;

		ar<<pObj->GetNextTime();
		ar<<dRatioStartDsc;
		ar<<dRatioEndDsc;
		ar<<dRatioStartTime;
		ar<<dRatioEndTime;
	}
}

void CTimeLineEditor::LoadTimeLineTemplate(CArchive& ar)
{
	//if ( m_pMovieObject == NULL )
	//	return;
	CString strDsc;
	int nCTime, nCDscIndex, nCTimeIndex, nTotalDscCnt,nTotalTimeCnt;
	double dRatioCDsc, dRatioCTime;

	ESMGetSelectedFrame(strDsc, nCTime);

	nCDscIndex = ESMGetDSCIndex(strDsc);
	nCTimeIndex = ESMGetFrameIndex(nCTime);

	nTotalTimeCnt = ESMGetFrameIndex(ESMGetRecTime()) + 1;
	nTotalDscCnt = ESMGetDSCCount();

	m_pTimeLineView->RemoveObject();
	//Sleep(1000);

	int nObjCnt;
	ar>>nObjCnt;

	
	for ( int i=0 ; i<nObjCnt ; i++)
	{
		int nNextTime;
		// Read File
		double dRatioStartDsc, dRatioEndDsc, dRatioStartTime, dRatioEndTime;

		ar>>nNextTime;
		ar>>dRatioStartDsc;
		ar>>dRatioEndDsc;
		ar>>dRatioStartTime;
		ar>>dRatioEndTime;

		int nStartTimeIdx, nStartDsc, nEndTimeIdx, nEndDsc, nStartTime, nEndTime;
		
		//nStartTimeIdx = nCTimeIndex + round(dRatioStartTime*(double)nTotalTimeCnt);
		nStartTimeIdx = nCTimeIndex + dRatioStartTime;
		if ( nStartTimeIdx < 0 )
			nStartTimeIdx = 0;
		else if ( nStartTimeIdx > nTotalTimeCnt - 1 )
			nStartTimeIdx = nTotalTimeCnt - 1;
		nEndTimeIdx = nCTimeIndex + dRatioEndTime;
		//nEndTimeIdx = nCTimeIndex + round(dRatioEndTime*(double)nTotalTimeCnt);
		if ( nEndTimeIdx < 0 )
			nEndTimeIdx = 0;
		else if ( nEndTimeIdx > nTotalTimeCnt - 1 )
			nEndTimeIdx = nTotalTimeCnt - 1;


		nStartDsc = round(dRatioStartDsc*(double)nTotalDscCnt);
		//nStartDsc = nCDscIndex + round(dRatioStartDsc*(double)nTotalDscCnt);
		if ( nStartDsc < 0 )
			nStartDsc = 0;
		else if ( nStartDsc > nTotalDscCnt - 1 )
			nStartDsc = nTotalDscCnt - 1;

		nEndDsc = round(dRatioEndDsc*(double)nTotalDscCnt);
		//nEndDsc = nCDscIndex + round(dRatioEndDsc*(double)nTotalDscCnt);
		if ( nEndDsc < 0 )
			nEndDsc = 0;
		else if ( nEndDsc > nTotalDscCnt - 1 )
			nEndDsc = nTotalDscCnt - 1;

		//-- 2014-10-22 cygil@esmlab.com
		//-- 첫번째 DSC의 정상영상 여부 확인하여 없는경우 정상영상이 있는 DSC를 찾는다
		if(!GetExistMovieDsc(nStartDsc))
		{
			int nTempIdx = FindExistMovieDsc(nStartDsc);
			if(nTempIdx >= 0)
				nStartDsc = nTempIdx;
		}
		//-- 2014-10-22 cygil@esmlab.com
		//-- 마지막 DSC의 정상영상 여부 확인하여 없는경우 정상영상이 있는 DSC를 찾는다
		if(!GetExistMovieDsc(nEndDsc))
		{
			int nTempIdx = FindExistMovieDsc(nEndDsc);
			if(nTempIdx >= 0)
				nEndDsc = nTempIdx;
		}


		nStartTime = ESMGetFrameTime(nStartTimeIdx);
		nEndTime = ESMGetFrameTime(nEndTimeIdx);

		CObArray arDSC;
		CMainFrame* pMainWnd = (CMainFrame*)AfxGetMainWnd();
		pMainWnd->m_wndDSCViewer.m_pFrameSelector->InsertDSCList(&arDSC, nStartDsc, nEndDsc);
		//CMiLRe 20151022 Template Group 단위로 삭제
		CESMTimeLineObjectEditor* pObj = new CESMTimeLineObjectEditor(arDSC, nStartTime, nEndTime, nNextTime, movie_next_frame_time );
		pObj->SetNextTime(nNextTime);
		m_pTimeLineView->m_nInsertObjPos = m_pTimeLineView->GetObjCount();
		m_pTimeLineView->AddObj(pObj);
	}

	//-- ReDraw
	m_pTimeLineView->DrawEditor();
}

//-- 2014-10-22 cygil@esmlab.com DSC의 정상영상 여부 확인
BOOL CTimeLineEditor::GetExistMovieDsc(int nDSCIdx)
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	FFmpegManager FFmpegMgr(FALSE);
	BOOL bExistMovie = FALSE;

	CString strFileName, strDscIp;
	int nRunningTime = 0, nFrameCount = 0, nWidth = 0, nHight = 0, nChannel = 3, nFrameRate = 0;
	CDSCItem* pItem = (CDSCItem*)arDSCList.GetAt(nDSCIdx);
	if(pItem)
	{
		strDscIp.Format(_T("%s"), pItem->m_strInfo[DSC_INFO_LOCATION]);
		if(ESMGetExecuteMode())
			strFileName.Format(_T("%s\\%s.mp4"),ESMGetClientRamPath(strDscIp),  pItem->GetDeviceDSCID());
		else
			strFileName.Format(_T("%s\\%s\\%s.mp4"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord(), pItem->GetDeviceDSCID());
		FFmpegMgr.GetMovieInfo(strFileName, &nRunningTime, &nFrameCount, &nWidth, &nHight, &nChannel, &nFrameRate);
		if(nWidth != 0 && nHight != 0 && pItem->GetSensorSync() == TRUE)
			bExistMovie = TRUE;
	}
	return bExistMovie;
}

//-- 2014-10-22 cygil@esmlab.com DSC의 영상이 없는경우 앞뒤 DSC의 정상 영상이 있는지 찾는다
int CTimeLineEditor::FindExistMovieDsc(int nDSCIdx)
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	CDSCItem* pItem = NULL;
	FFmpegManager FFmpegMgr(FALSE);
	BOOL bExistMovie = FALSE;

	int nDSCFind = -1;
	int nFirstCnt = 0;
	int nLastCnt = arDSCList.GetCount() - 1;

	int nPrevCnt = nDSCIdx - 1;
	int nNextCnt = nDSCIdx + 1;

	BOOL bPrev = TRUE;
	BOOL bNext = TRUE;
	if(nPrevCnt < nFirstCnt)
		bPrev = FALSE;
	if(nNextCnt > nLastCnt)
		bNext = FALSE;


	while(TRUE)
	{
		if(bPrev && nPrevCnt < nFirstCnt)
			bPrev = FALSE;
		if(bNext && nNextCnt > nLastCnt)
			bNext = FALSE;
		if(!bPrev && !bNext)
			break;

		if(bPrev)
		{
			if(GetExistMovieDsc(nPrevCnt))
			{
				nDSCFind = nPrevCnt;
				break;
			}
			else
				nPrevCnt--;
		}

		if(bNext)
		{
			if(GetExistMovieDsc(nNextCnt))
			{
				nDSCFind = nNextCnt;
				break;
			}
			else
				nNextCnt++;
		}
	}
	return nDSCFind;
}

//CMiLRe 20151013 Template Load INI File
void CTimeLineEditor::LoadTemplateFromMvtmFile(CString strFileName, int nNum, BOOL bDSCCheck)
{
	vector<TEMPLATE_STRUCT> TemplateVector;
	TemplateVector.clear();
	CESMIni ini;
	if(!ini.SetIniFilename(strFileName))
	{
		AfxMessageBox(_T("The file does not exist."));
	}

	CStringArray strArSectionName;
	strArSectionName.RemoveAll();
	ini.GetSectionNames(&strArSectionName);

	if(strArSectionName.GetCount() < 1) 
	{
		//AfxMessageBox(_T("세션 리스트가 없습니다."));
		if(nNum > 10)
		{
			int nDivision	= nNum/10;
			int nRemainder	= nNum%10;
			ESMLog(5,_T("Check Template File : %s [F%d : %d]"),strFileName ,nDivision, nRemainder);
		}else
			ESMLog(5,_T("Check Template File : %s [%d]"),strFileName ,nNum);
	}
	
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	
	//nStartDSC, nEndDSC, nStartTime, nEndTime, nFrameRate
	
	for(int i = 0 ; i < strArSectionName.GetSize() ; i++)
	{
		if(strArSectionName.GetAt(i).CompareNoCase(TEMPLATE_COMMON_SECTION) == 0  || strArSectionName.GetAt(i).CompareNoCase(TEMPLATE_VIEW_SECTION) == 0)
			continue;

		//20160111 CMiLRe Template Load 시 이전 정보 Clear 안되어 Effect editor 이전 정보가 추가되는 버그 수정
		TEMPLATE_STRUCT temeTemplate;
		temeTemplate.KzoneIndex = ini.GetInt(strArSectionName.GetAt(i),TEMPLATE_ENTRY_KZONE);
#ifdef _ORIGIN_GET
		temeTemplate.nStartTime = ini.GetInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_STARTTIME);
		temeTemplate.nStrartDSC = ini.GetInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_START_DSC);
		temeTemplate.nEndTime = ini.GetInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_ENDTIME);
		temeTemplate.nEndDSC = ini.GetInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_END_DSC);
		temeTemplate.nFrameRate = ini.GetInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_TIMEFRAME);
#else
		CString strSTime, strETime, strSDSC, strEDSC, strFrameRate, strFrameCount, strStepFrame;

		strFrameCount = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_FRAME_COUNT);
		temeTemplate.nFrameCount = _ttoi(strFrameCount);
		if(temeTemplate.nFrameCount < 1)
			temeTemplate.nFrameCount = 1;

		strSTime = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_STARTTIME);
		strSTime.Trim();

		if(ESMGetNumValidate(strSTime))
		{
			temeTemplate.nStartTime = _ttoi(strSTime);
		}else
		{
			temeTemplate.nStartTime = ini.GetInt(TEMPLATE_COMMON_SECTION, strSTime);
			if(temeTemplate.nStartTime == 0)
				ESMLog(0, _T("[Template] %s : %d"), strSTime, temeTemplate.nStartTime);
		}
		
		strETime = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_ENDTIME);
		strETime.Trim();

		if(ESMGetNumValidate(strETime))
		{
			temeTemplate.nEndTime = _ttoi(strETime);
		}else
		{
			temeTemplate.nEndTime = ini.GetInt(TEMPLATE_COMMON_SECTION, strETime);
			if(temeTemplate.nEndTime == 0)
				ESMLog(0, _T("[Template] %s : %d"), strETime, temeTemplate.nEndTime);
		}
		
		strSDSC = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_START_DSC);
		strSDSC.Trim();

		if(ESMGetNumValidate(strSDSC))
		{
			temeTemplate.nStrartDSC = _ttoi(strSDSC);
		}else
		{
			temeTemplate.nStrartDSC = ini.GetInt(TEMPLATE_COMMON_SECTION, strSDSC);
			if(temeTemplate.nStrartDSC == 0)
				ESMLog(0, _T("[Template] %s : %d"), strSDSC, temeTemplate.nStrartDSC);
		}

		//jhhan 180614
		if(temeTemplate.nStrartDSC > 0)
		{
			temeTemplate.nStrartDSC  = temeTemplate.nStrartDSC - 1;
		}

		strEDSC = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_END_DSC);
		strEDSC.Trim();

		if(ESMGetNumValidate(strEDSC))
		{
			temeTemplate.nEndDSC = _ttoi(strEDSC);
		}else
		{
			temeTemplate.nEndDSC = ini.GetInt(TEMPLATE_COMMON_SECTION, strEDSC);
			if(temeTemplate.nEndDSC == 0)
				ESMLog(0, _T("[Template] %s : %d"), strEDSC, temeTemplate.nEndDSC);
		}

		//jhhan 180614
		if(temeTemplate.nEndDSC > 0)
		{
			temeTemplate.nEndDSC = temeTemplate.nEndDSC - 1;
		}

		strFrameRate = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_TIMEFRAME);
		strFrameRate.Trim();

		if(ESMGetNumValidate(strFrameRate))
		{
			temeTemplate.nFrameRate = _ttoi(strFrameRate);
		}else
		{
			temeTemplate.nFrameRate = ini.GetInt(TEMPLATE_COMMON_SECTION, strFrameRate);
			if(temeTemplate.nFrameRate == 0)
				ESMLog(0, _T("[Template] %s : %d"), strFrameRate, temeTemplate.nFrameRate);
		}

		strStepFrame = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_STEP_FRAME);
		strStepFrame.Trim();
		if(ESMGetNumValidate(strStepFrame))
		{
			temeTemplate.nStepFrame = _ttoi(strStepFrame);
		}else
		{
			temeTemplate.nStepFrame = ini.GetInt(TEMPLATE_COMMON_SECTION, strStepFrame);
			if(temeTemplate.nStepFrame == 0)
				ESMLog(0, _T("[Template] %s : %d"), strStepFrame, temeTemplate.nStepFrame);
		}
#endif
		//20151015 Template VMCC Save/Load
		temeTemplate.nVMCC_Count = ini.GetInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_VMCC_COUNT);
		int nCnt = temeTemplate.nVMCC_Count;
		for(int k = 0 ; k < nCnt ; k++)
		{
			CString strVMCCSectionName, strVMCCEntryData;
			strVMCCSectionName.Format(_T("%s_%d"), TEMPLATE_ENTRY_VMCC, k);
			strVMCCEntryData.Format(_T("%s"), ini.GetString(strArSectionName.GetAt(i), strVMCCSectionName));
			
			if(strVMCCEntryData.IsEmpty())
				continue;

			vector<TEMPLATE_VMCC_STRUCT> vecVMCCEntry;
			TEMPLATE_VMCC_STRUCT structVMCCElement;
			CString strRatioTime, strPosX, strPosY, strZoom, strWeight;

			//RatioTime
			int nPos = strVMCCEntryData.Find(_T(":"));
			strRatioTime.Format(_T("%s"), strVMCCEntryData.Left(nPos));
			strRatioTime.Trim();

			//Weight
			int nPosRatio = strVMCCEntryData.ReverseFind(':');
			strWeight.Format(_T("%s"), strVMCCEntryData.Right(strVMCCEntryData.GetLength() - nPosRatio-1));
			strWeight.Trim();

			//PosX
			CString strTemp;
			strTemp.Format(_T("%s"), strVMCCEntryData.Mid(nPos+1, strVMCCEntryData.GetLength()));
			nPos = strTemp.Find(_T(":"));
			strPosX.Format(_T("%s"), strTemp.Left(nPos));
			strPosX.Trim();

			//PosY
			strTemp.Format(_T("%s"), strTemp.Mid(nPos+1, strTemp.GetLength()));
			nPos = strTemp.Find(_T(":"));
			strPosY.Format(_T("%s"), strTemp.Left(nPos));
			strPosY.Trim();

			//Zoom
			strTemp.Format(_T("%s"), strTemp.Mid(nPos+1, strTemp.GetLength()));
			nPos = strTemp.Find(_T(":"));
			strZoom.Format(_T("%s"), strTemp.Left(nPos));
			strZoom.Trim();

			TRACE(_T(""));
			//jhhan 180502 VMCC - 사전정의어 적용
			if(ESMGetNumValidate(strRatioTime) != TRUE)
			{
				strTemp = ini.GetString(TEMPLATE_COMMON_SECTION, strRatioTime);
				if(strTemp.IsEmpty())
					ESMLog(0, _T("[Template] %s : %s"), strRatioTime, strTemp);

				strRatioTime = strTemp;
				
			}
			if(ESMGetNumValidate(strPosX) != TRUE)
			{
				strTemp = ini.GetString(TEMPLATE_COMMON_SECTION, strPosX);
				if(strTemp.IsEmpty())
					ESMLog(0, _T("[Template] %s : %s"), strPosX, strTemp);

				strPosX = strTemp;
				
			}
			if(ESMGetNumValidate(strPosY) != TRUE)
			{
				strTemp = ini.GetString(TEMPLATE_COMMON_SECTION, strPosY);
				if(strTemp.IsEmpty())
					ESMLog(0, _T("[Template] %s : %s"), strPosY, strTemp);

				strPosY = strTemp;
				
			}
 			if(ESMGetNumValidate(strZoom) != TRUE)
			{
				strTemp = ini.GetString(TEMPLATE_COMMON_SECTION, strZoom);
				if(strTemp.IsEmpty())
					ESMLog(0, _T("[Template] %s : %s"), strZoom, strTemp);

				strZoom = strTemp;
				
			}
			if(ESMGetNumValidate(strWeight) != TRUE)
			{
				strTemp = ini.GetString(TEMPLATE_COMMON_SECTION, strWeight);
				if(strTemp.IsEmpty())
					ESMLog(0, _T("[Template] %s : %s"), strWeight, strTemp);

				strWeight = strTemp;
				
			}

			structVMCCElement.dRatioTime = _tstof(strRatioTime);
			structVMCCElement.dRatioX = _tstof(strPosX);
			structVMCCElement.dRatioY = _tstof(strPosY);
			structVMCCElement.nZoom = _tstof(strZoom);
			structVMCCElement.dWeight = _tstof(strWeight);

			temeTemplate.nVMCC_Data.push_back(structVMCCElement);
		}
		
		int nVMCCDataCount = 0;

		if(bDSCCheck)
		{
			if(temeTemplate.nStrartDSC > arDSCList.GetCount() || temeTemplate.nEndDSC > arDSCList.GetCount())
			{
				AfxMessageBox(_T("No DSC information is set."));
				TemplateVector.clear();
			}
		}

		TemplateVector.push_back(temeTemplate);		
	}
	
	//jhhan 181219 View Template
	VIEW_STRUCT _stView;
	_stView.nCount = ini.GetInt(TEMPLATE_VIEW_SECTION, TEMPLATE_VIEW_COUNT);
	_stView.nTime = ini.GetInt(TEMPLATE_VIEW_SECTION, TEMPLATE_VIEW_TIME);
	if(_stView.nCount > 0)
	{
		for(int i = 0; i < _stView.nCount; i++)
		{
			CString strViewSection, strViewEntry;
			strViewSection.Format(_T("%s_%d"), TEMPLATE_VIEW_ENTRY, i);
			strViewEntry.Format(_T("%s"), ini.GetString(TEMPLATE_VIEW_SECTION, strViewSection));

			if(strViewEntry.IsEmpty())
				continue;

			TEMPLATE_VIEW_STRUCT stViewElement;
			CString strCamera, strZoom, strX, strY;

			AfxExtractSubString(strCamera,	strViewEntry, 0, ':');	strCamera.Trim();
			AfxExtractSubString(strZoom,	strViewEntry, 1, ':');	strZoom.Trim();
			AfxExtractSubString(strX,		strViewEntry, 2, ':');	strX.Trim();
			AfxExtractSubString(strY,		strViewEntry, 3, ':');	strY.Trim();
			stViewElement.nCameraIdx	= _ttoi(strCamera);
			stViewElement.nZoom		= _ttoi(strZoom);
			stViewElement.nX		= _ttoi(strX);
			stViewElement.nY		= _ttoi(strY);

			_stView.nArrData.push_back(stViewElement);
		}

		m_stViewTemplate.insert(make_pair(nNum, _stView));
/*
#if 0
		m_stViewTemplate[nNum] = _stView;
#else
		m_stViewTemplate.insert(make_pair(nNum, _stView));
#endif*/
		//ViewTemplate(nNum);

		VIEW_STRUCT _stData;
		_stData = GetViewTempalte(nNum);
		
#if _DEBUG
		if(_stData.nCount > 0)
		{
			CString strLog;
			strLog.Format(_T("[%d] VIEW_STRUCT : [%d] [%d]"), nNum, _stData.nCount, _stData.nTime);
			for(int i=0; i<_stData.nCount; i++)
			{
				TEMPLATE_VIEW_STRUCT _stCamera;
				_stCamera = _stData.nArrData.at(i);
				CString strTemp;
				strTemp.Format(_T(" / %d, %d, %d, %d"), _stCamera.nCameraIdx, _stCamera.nZoom, _stCamera.nX, _stCamera.nY);
				strLog.Append(strTemp);
			}
			ESMLog(5, strLog);
		}
#endif
	}
	

	m_TemplateVector.insert(make_pair(nNum, TemplateVector));
}

//CMiLRe 20151013 Template Load INI File
vector<TEMPLATE_STRUCT> CTimeLineEditor::LoadTemplateElement(int nNum, BOOL bDSCCheck)
{
	vector<TEMPLATE_STRUCT> temeTemplate;
	if(m_TemplateVector.size() > 0)
	{
		//CMiLRe 20151014 Template Save INI File
		map<int, vector<TEMPLATE_STRUCT>>::iterator itmapList = m_TemplateVector.find(nNum);
		if(itmapList != m_TemplateVector.end())
		{
			temeTemplate = itmapList->second;
		}
		TRACE(_T(""));
		return temeTemplate;
	}
	return temeTemplate;
}

//wgkim@esmlab.com
void CTimeLineEditor::SetTemplateMgr(CESMTemplateMgr *templateMgr)
{
	m_pTemplateMgr = templateMgr;
}

void CTimeLineEditor::LoadTemplateRecovery(CString strFileName, int nNum, BOOL bDSCCheck)
{
	vector<TEMPLATE_STRUCT> TemplateVector;
	TemplateVector.clear();
	CESMIni ini;
	if(!ini.SetIniFilename(strFileName))
	{
		AfxMessageBox(_T("The file does not exist."));
	}

	CStringArray strArSectionName;
	strArSectionName.RemoveAll();
	ini.GetSectionNames(&strArSectionName);

	if(strArSectionName.GetCount() < 1) 
	{
		//AfxMessageBox(_T("세션 리스트가 없습니다."));
		if(nNum > 10)
		{
			int nDivision	= nNum/10;
			int nRemainder	= nNum%10;
			ESMLog(5,_T("Check Template File : %s [F%d : %d]"),strFileName ,nDivision, nRemainder);
		}else
			ESMLog(5,_T("Check Template File : %s [%d]"),strFileName ,nNum);
	}

	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	//nStartDSC, nEndDSC, nStartTime, nEndTime, nFrameRate

	BOOL bRet = FALSE;
	for(int i = 0 ; i < strArSectionName.GetSize() ; i++)
	{
		if(strArSectionName.GetAt(i).CompareNoCase(_T("COMMON")) == 0)
			continue;

		//20160111 CMiLRe Template Load 시 이전 정보 Clear 안되어 Effect editor 이전 정보가 추가되는 버그 수정
		TEMPLATE_STRUCT temeTemplate;
		temeTemplate.KzoneIndex = ini.GetInt(strArSectionName.GetAt(i),TEMPLATE_ENTRY_KZONE);

		CString strSTime, strETime, strSDSC, strEDSC, strFrameRate;

		strSTime = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_STARTTIME);
		if(ESMGetNumValidate(strSTime))
		{
			temeTemplate.nStartTime = _ttoi(strSTime);
		}else
		{
			temeTemplate.nStartTime = ini.GetInt(TEMPLATE_COMMON_SECTION, strSTime);
			//ESMLog(5, _T("[Template] %s : %d"), strSTime, temeTemplate.nStartTime);
			
			ini.WriteInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_STARTTIME, temeTemplate.nStartTime);
			bRet = TRUE;
		}

		strETime = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_ENDTIME);
		if(ESMGetNumValidate(strETime))
		{
			temeTemplate.nEndTime = _ttoi(strETime);
		}else
		{
			temeTemplate.nEndTime = ini.GetInt(TEMPLATE_COMMON_SECTION, strETime);
			//ESMLog(5, _T("[Template] %s : %d"), strETime, temeTemplate.nEndTime);

			ini.WriteInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_ENDTIME, temeTemplate.nEndTime);
			bRet = TRUE;
		}

		strSDSC = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_START_DSC);
		if(ESMGetNumValidate(strSDSC))
		{
			temeTemplate.nStrartDSC = _ttoi(strSDSC);
		}else
		{
			temeTemplate.nStrartDSC = ini.GetInt(TEMPLATE_COMMON_SECTION, strSDSC);
			//ESMLog(5, _T("[Template] %s : %d"), strSDSC, temeTemplate.nStrartDSC);

			ini.WriteInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_START_DSC, temeTemplate.nStrartDSC);
			bRet = TRUE;
		}

		strEDSC = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_END_DSC);
		if(ESMGetNumValidate(strEDSC))
		{
			temeTemplate.nEndDSC = _ttoi(strEDSC);
		}else
		{
			temeTemplate.nEndDSC = ini.GetInt(TEMPLATE_COMMON_SECTION, strEDSC);
			//ESMLog(5, _T("[Template] %s : %d"), strEDSC, temeTemplate.nEndDSC);

			ini.WriteInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_END_DSC, temeTemplate.nEndDSC);
			bRet = TRUE;
		}
		strFrameRate = ini.GetString(strArSectionName.GetAt(i), TEMPLATE_ENTRY_TIMEFRAME);
		if(ESMGetNumValidate(strFrameRate))
		{
			temeTemplate.nFrameRate = _ttoi(strFrameRate);
		}else
		{
			temeTemplate.nFrameRate = ini.GetInt(TEMPLATE_COMMON_SECTION, strFrameRate);
			//ESMLog(5, _T("[Template] %s : %d"), strFrameRate, temeTemplate.nFrameRate);

			ini.WriteInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_TIMEFRAME, temeTemplate.nFrameRate);
			bRet = TRUE;
		}

		//jhhan 180504 VMCC 복구
		temeTemplate.nVMCC_Count = ini.GetInt(strArSectionName.GetAt(i), TEMPLATE_ENTRY_VMCC_COUNT);
		int nCnt = temeTemplate.nVMCC_Count;
		for(int k = 0 ; k < nCnt ; k++)
		{
			CString strVMCCSectionName, strVMCCEntryData;
			strVMCCSectionName.Format(_T("%s_%d"), TEMPLATE_ENTRY_VMCC, k);
			strVMCCEntryData.Format(_T("%s"), ini.GetString(strArSectionName.GetAt(i), strVMCCSectionName));
			
			if(strVMCCEntryData.IsEmpty())
				continue;

			vector<TEMPLATE_VMCC_STRUCT> vecVMCCEntry;
			TEMPLATE_VMCC_STRUCT structVMCCElement;
			CString strRatioTime, strPosX, strPosY, strZoom, strWeight;

			//RatioTime
			int nPos = strVMCCEntryData.Find(_T(":"));
			strRatioTime.Format(_T("%s"), strVMCCEntryData.Left(nPos));
			strRatioTime.Trim();

			//Weight
			int nPosRatio = strVMCCEntryData.ReverseFind(':');
			strWeight.Format(_T("%s"), strVMCCEntryData.Right(strVMCCEntryData.GetLength() - nPosRatio-1));
			strWeight.Trim();

			//PosX
			CString strTemp;
			strTemp.Format(_T("%s"), strVMCCEntryData.Mid(nPos+1, strVMCCEntryData.GetLength()));
			nPos = strTemp.Find(_T(":"));
			strPosX.Format(_T("%s"), strTemp.Left(nPos));
			strPosX.Trim();

			//PosY
			strTemp.Format(_T("%s"), strTemp.Mid(nPos+1, strTemp.GetLength()));
			nPos = strTemp.Find(_T(":"));
			strPosY.Format(_T("%s"), strTemp.Left(nPos));
			strPosY.Trim();

			//Zoom
			strTemp.Format(_T("%s"), strTemp.Mid(nPos+1, strTemp.GetLength()));
			nPos = strTemp.Find(_T(":"));
			strZoom.Format(_T("%s"), strTemp.Left(nPos));
			strZoom.Trim();

			TRACE(_T(""));
			//jhhan 180502 VMCC - 사전정의어 적용
			if(ESMGetNumValidate(strRatioTime) != TRUE)
			{
				strTemp = ini.GetString(TEMPLATE_COMMON_SECTION, strRatioTime);

				strVMCCEntryData.Replace(strRatioTime, strTemp);
				bRet = TRUE;
			}
			if(ESMGetNumValidate(strPosX) != TRUE)
			{
				strTemp = ini.GetString(TEMPLATE_COMMON_SECTION, strPosX);

				strVMCCEntryData.Replace(strPosX, strTemp);
				bRet = TRUE;
			}
			if(ESMGetNumValidate(strPosY) != TRUE)
			{
				strTemp = ini.GetString(TEMPLATE_COMMON_SECTION, strPosY);

				strVMCCEntryData.Replace(strPosY, strTemp);
				bRet = TRUE;
			}
			if(ESMGetNumValidate(strZoom) != TRUE)
			{
				strTemp = ini.GetString(TEMPLATE_COMMON_SECTION, strZoom);
				
				strVMCCEntryData.Replace(strZoom, strTemp);
				bRet = TRUE;
			}
			if(ESMGetNumValidate(strWeight) != TRUE)
			{
				strTemp = ini.GetString(TEMPLATE_COMMON_SECTION, strWeight);
				
				strVMCCEntryData.Replace(strWeight, strTemp);
				bRet = TRUE;
			}

			ini.WriteString(strArSectionName.GetAt(i), strVMCCSectionName, strVMCCEntryData);
		}
	}

	if(bRet)
	{
		ini.DeleteSection(_T("COMMON"));

		ESMLog(5, _T("Template Recovery : %s, %d"), strFileName, nNum);
	}
	
}

void CTimeLineEditor::ViewTemplate(int nTemplate)
{
	if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
	{
		AfxMessageBox(_T("Current server mode is video production mode. \n Available in Record or Both mode only."));
		return;
	}

	VIEW_STRUCT _stView;
	if(m_stViewTemplate.size() > 0)
	{
		map<int, VIEW_STRUCT>::iterator itmapList = m_stViewTemplate.find(nTemplate);
		if(itmapList != m_stViewTemplate.end())
		{
			_stView = itmapList->second;
		}
	}

#if _DEBUG
	ESMLog(5, _T("ViewTemplate[%d] : %d, %d"), nTemplate, _stView.nCount, _stView.nTime);
#endif
	for(int i=0; i<_stView.nCount; i++)
	{
		TEMPLATE_VIEW_STRUCT _stData;
		_stData = _stView.nArrData.at(i);
#if _DEBUG
		ESMLog(5, _T("View Data[%d] : %d, %d, %d, %d"),i+1, _stData.nCameraIdx, _stData.nZoom, _stData.nX, _stData.nY);
#endif
	}
}
VIEW_STRUCT CTimeLineEditor::GetViewTempalte(int nTemplate /*= -1*/)
{
	VIEW_STRUCT _stView;

	if (ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_RECORDING)
	{
		AfxMessageBox(_T("Current server mode is video production mode. \n Available in Record or Both mode only."));
		return _stView;
	}
		
	if(m_stViewTemplate.size() > 0)
	{
		map<int, VIEW_STRUCT>::iterator itmapList = m_stViewTemplate.find(nTemplate);
		if(itmapList != m_stViewTemplate.end())
		{
			_stView = itmapList->second;
		}
	}
	int nSize = _stView.nCount;
	if(nSize != _stView.nArrData.size())
	{
		nSize = _stView.nArrData.size();
		_stView.nCount = nSize;
		ESMLog(0,_T("Total Count is not correct!!!!"));
	}
	for(int i=0; i < nSize; i++)
	{
		TEMPLATE_VIEW_STRUCT _stData;
		_stData = _stView.nArrData.at(i);
	}

	return _stView;
}