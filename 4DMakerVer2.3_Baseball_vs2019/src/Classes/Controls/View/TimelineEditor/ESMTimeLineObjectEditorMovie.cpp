////////////////////////////////////////////////////////////////////////////////
//
//	ESMTimeLineObject.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMTimeLineObjectEditor.h"
#include "resource.h"

#include "DSCViewDefine.h"
#include "DSCItem.h"
#include "ESMCtrl.h"
#include "ESMFileOperation.h"
#include "ESMObjectFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMTimeLineObjectEditor::MakeMovie(int nIndex)
{
	if (m_bClose) 
		return;

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Set Start Make Movie
	SetMakeMovie(FALSE);

	m_nObjectIndex = nIndex;
	if( m_hMakeObjMovie != NULL)
	{
		CloseHandle(m_hMakeObjMovie);
		m_hMakeObjMovie = NULL;
	}

	m_hMakeObjMovie = (HANDLE) _beginthreadex(NULL, 0, MakeObjMovieThread, (void *)this, 0, NULL);
}
void CESMTimeLineObjectEditor::MakeMovieToClient(int nIndex)
{
	if (m_bClose) 
		return;
	
	SetMakeMovie(FALSE);

	m_nObjectIndex = nIndex;
	if( m_hMakeObjMovie != NULL)
	{
		CloseHandle(m_hMakeObjMovie);
		m_hMakeObjMovie = NULL;
	}
	m_hMakeObjMovie = (HANDLE) _beginthreadex(NULL, 0, MakeMovieToClientThread, (void *)this, 0, NULL);
}

void CESMTimeLineObjectEditor::SetMovieData(MakeMovieInfo* stMovieData, CDSCItem* pItem , int nStartTime, int nEndTime, int nFrameSpeed, int nMovieModule)
{
	CString strSrcFile;
	int nShiftFrame			= 0;

	_tcscpy(stMovieData->strDscID, pItem->GetDeviceDSCID().GetBuffer(0));
	nShiftFrame = ESMGetShiftFrame(pItem->GetDeviceDSCID());

	pItem->GetDeviceDSCID().ReleaseBuffer();
	stMovieData->nStartFrame = ESMGetFrameIndex(nStartTime);
	stMovieData->nEndFrame = ESMGetFrameIndex(nEndTime);

	stMovieData->nStartFrame += nShiftFrame;
	stMovieData->nEndFrame += nShiftFrame;
	if(stMovieData->nStartFrame < 0)
		stMovieData->nStartFrame= 0;
	if(stMovieData->nEndFrame < 0)
		stMovieData->nEndFrame= 0;

	if(ESMGetValue(ESM_VALUE_INSERTLOGO))
	{
		stMovieData->nDscIndex = pItem->GetIndex();
		stMovieData->b3DLogo = ESMGetValue(ESM_VALUE_3DLOGO);
		stMovieData->bLogoZoom = ESMGetValue(ESM_VALUE_LOGOZOOM);
	}
	else
		stMovieData->nDscIndex = -1;

	

	int priValue = GetKzoneIdx();
	if(ESMGetValue(ESM_VALUE_INSERTPRISM) || priValue > 0)
	{
		stMovieData->nPriIndex = pItem->GetIndex();
		stMovieData->nPrinumIndex = GetKzoneIdx();
		//stMovieData->nP
		if(ESMGetValue(ESM_VALUE_PRISMVALUE) > 0 && ESMGetValue(ESM_VALUE_PRISMVALUE2) > 0)
		{
			stMovieData->nPriValue	= ESMGetValue(ESM_VALUE_PRISMVALUE);
			stMovieData->nPriValue2	= ESMGetValue(ESM_VALUE_PRISMVALUE2);
		}
		else
		{
			stMovieData->nPriValue	= 30;
			stMovieData->nPriValue2	= 20;	
		}
	}
	else
	{
		stMovieData->nPriIndex  = -1;
		stMovieData->nPriValue  = 0;
		stMovieData->nPriValue2 = 0;
	}

	/*if(ESMGetValue(ESM_VALUE_AUTO_WHITEBALANCE))
		stMovieData->bInsertWB = TRUE;
	else
		stMovieData->bInsertWB = FALSE;*/
	
	if(ESMGetValue(ESM_VALUE_COLORREVISION))
		stMovieData->bColorRevision =TRUE;
	else
		stMovieData->bColorRevision = FALSE;

	if(ESMGetValue(ESM_VALUE_PRINT_COLORINFO))
		stMovieData->bPrnColorInfo = TRUE;
	else
		stMovieData->bPrnColorInfo = FALSE;

	stMovieData->nFrameSpeed = nFrameSpeed;
	if(ESMGetExecuteMode())
		strSrcFile.Format(_T("%s\\%s"),ESMGetServerRamPath(), ESMGetFrameRecord());
	else
		strSrcFile.Format(_T("%s\\%s"), ESMGetPath(ESM_PATH_MOVIE_FILE), ESMGetFrameRecord());
	_stprintf(stMovieData->strMoviePath, _T("%s"), strSrcFile);
	_stprintf(stMovieData->strMovieName, _T("%d_%d"), this->m_nObjectIndex, nMovieModule);
}

void CESMTimeLineObjectEditor::SetTimeLineInfo(int* nEffectStart, int* nEffectCnt, TimeLineInfo* stTimeeLineData, MakeMovieInfo stMovieData)
{
	stTimeeLineData->stMovieData = stMovieData;

	// ObjectFrame Ind ex Check
	*nEffectStart = *nEffectStart + *nEffectCnt;
	if ( stMovieData.nStartFrame > stMovieData.nEndFrame)
		*nEffectCnt = stMovieData.nStartFrame - stMovieData.nEndFrame + 1;
	else
		*nEffectCnt = stMovieData.nEndFrame - stMovieData.nStartFrame + 1;

	// Add Effect data to each frame
	BOOL bhasEffect = FALSE;
	int nFrameCnt = this->GetObjectFrameCount();
	if ( nFrameCnt != 0 )
		bhasEffect = TRUE;

	for ( int i = *nEffectStart ; i < *nEffectStart + *nEffectCnt ; i++ )
	{
		EffectInfo stEffectData;
		if( bhasEffect == TRUE )
		{
			CESMObjectFrame* pObjFrame = this->GetObjectFrame(i);
			if ( nFrameCnt > i  )
			{	
				stEffectData.bZoom = pObjFrame->IsZoom();
				stEffectData.nPosX = pObjFrame->GetPosX();
				stEffectData.nPosY = pObjFrame->GetPosY();
				stEffectData.nZoomRatio = pObjFrame->GetZoomRatio();
				stEffectData.bBlur = pObjFrame->IsBlur();
				stEffectData.nBlurSize = pObjFrame->GetBlurSize();
				stEffectData.nBlurWeight = pObjFrame->GetBlurWeight();
			}
			else
			{
				stEffectData.bZoom = FALSE;
				stEffectData.bBlur = FALSE;
			}
		}
		else
		{
			stEffectData.bZoom = FALSE;
			stEffectData.bBlur = FALSE;
		}
		stTimeeLineData->arrEffectData.push_back(stEffectData);
	}
}

unsigned WINAPI CESMTimeLineObjectEditor::MakeMovieToClientThread(LPVOID param)
{
	CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)param;
	if (!pObj) 
		return 0;
	if (pObj->m_bClose) 
		return 0;

	CESMFileOperation fo;
	CESMImgMgr* pImgMgr = new CESMImgMgr();
	int nDSCCnt				= pObj->GetDSCCount();
	int nStartTime			= pObj->GetStartTime();			//-- Start DSC : Time
	int nEndTime			= pObj->GetEndTime();			//-- End DSC : Time
	int nFpsTime			= pObj->GetNextTime();
	int nGopTime			= 15;
	int nFrameSpeed			= pObj->m_nTime[2];

	vector<TimeLineInfo>* arrTimeLineInfo = NULL;
	ESMLog(5,_T("[Object] Create TS File on Object [DSC:%d][Time : Start[%d] End[%d]"),nDSCCnt, nStartTime, nEndTime);

	int nSec = 0, nMilli = 0, nTargetTime = 0;
	CDSCItem* pItem = NULL, *pPrevItem = NULL;
	ESMEvent* pMsg  = NULL;
	CString strItemIp;
	int nMovieModule = 0;

	int nEffectCnt=0, nEffectStart=0;

	//-- Copy frame%d.jpg
	//-- 1. StartItem == EndItem
	if(!nDSCCnt)
	{
		ESMLog(0,_T("Non Exist DSC ID on Make Movie"));
		_endthread();
		return 0;
	}
	// -- Init Making Movie Count
	pObj->m_nMovieNotFinished = 0;

	//-- Target Numbering
	//-- 2013-10-08 hongsu@esmlab.com
	//-- Reset Image Manager 
	pImgMgr->ResetAdjust(pObj->m_nObjectIndex, ESM_ADJ_KIND_POSITION);

	//-- Check Normal, Reverse
	int nTime = nEndTime - nStartTime;
	BOOL bNormal;
	if(nTime > 0)
		bNormal = TRUE;
	else
		bNormal = FALSE;

	ESMLog(5,_T("[Object][%d] Start Movie Make"),pObj->m_nObjectIndex);
	if(nDSCCnt == 1)
	{
		//-------------------------------------------------------------------
		//-- SAME DSC (One Line)
		//-------------------------------------------------------------------	
		pItem	= pObj->GetDSC(0);
		if(pItem->GetDSCStatus() != SDI_STATUS_REC_FINISH)
			return 0;

		// 2014-09-16 hongsu@esmlab.com
		if( pItem->GetSensorSync() == FALSE )
		{
			ESMLog(0, _T("SensorSync Skip Dsc [%d]"), pItem->GetDeviceDSCID());
			return 0;
		}
		// 데이터 만들어서 전송.MakeMovieToClient
		// DSC 의 IP 정보를 알수가 있나?
		
		arrTimeLineInfo = new vector<TimeLineInfo>;
		MakeMovieInfo stMovieData;

		pObj->SetMovieData(&stMovieData, pItem, nStartTime, nEndTime, nFrameSpeed,0);

		TimeLineInfo stTimeLineInfo;
		pObj->SetTimeLineInfo(&nEffectStart, &nEffectCnt, &stTimeLineInfo, stMovieData);
		arrTimeLineInfo->push_back(stTimeLineInfo);

		// Add Making Movie Count
		pObj->m_nMovieNotFinished++;
		// arrMovieObject 전송
		pMsg = new ESMEvent();  
		pMsg->pParam		= (LPARAM)arrTimeLineInfo;
		pMsg->pDest			= (LPARAM)pItem;
		
		if ( ESMGetExecuteMode() )
		{
			pMsg->message = WM_ESM_NET_MAKEMOVIETOCLIENT;
			::SendMessage(pObj->m_hMainWnd, WM_ESM, WM_ESM_NET, (LPARAM)pMsg);
		}
		else
		{
			pMsg->message = WM_ESM_MOVIE_MAKEMOVIE;
			::SendMessage(pObj->m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}

	}
	if( nDSCCnt != 1 && !nTime)
	{
		//-------------------------------------------------------------------
		//-- SAME TIME (One Line)
		//-- 2013-10-01 hongsu@esmlab.com
		//-- One More Selected DSC 
		//-------------------------------------------------------------------	

		nTargetTime = nStartTime;
		BOOL bLogo = TRUE;

		arrTimeLineInfo = new vector<TimeLineInfo>;
		MakeMovieInfo stMovieData;

		for(int i = 0 ; i < nDSCCnt ; i ++)
		{	
			pItem	= pObj->GetDSC(i);
			if(pItem->GetDSCStatus() != SDI_STATUS_REC_FINISH)
				continue;

			if( pItem->GetSensorSync() == FALSE)
			{
				ESMLog(5, _T("SensorSync Skip DSC[%s]"), pItem->GetDeviceDSCID());
				continue;
			}

			if( ( strItemIp != pItem->GetDSCInfo(DSC_INFO_LOCATION) && strItemIp != _T("")) && ESMGetExecuteMode() )
			{
				// Add Making Movie Count
				pObj->m_nMovieNotFinished++;

				// 데이터 만들어서 전송.MakeMovieToClient
				pMsg = new ESMEvent();
				pMsg->message = WM_ESM_NET_MAKEMOVIETOCLIENT;
				pMsg->pParam		= (LPARAM)arrTimeLineInfo;
				pMsg->pDest			= (LPARAM)pPrevItem;
				::SendMessage(pObj->m_hMainWnd, WM_ESM, WM_ESM_NET, (LPARAM)pMsg);

				arrTimeLineInfo = new vector<TimeLineInfo>;
				nMovieModule++;
			}

			pObj->SetMovieData(&stMovieData, pItem, nStartTime, nEndTime, nFrameSpeed, nMovieModule);

			//arrMovieObject->push_back(stMovieData);
			strItemIp = pItem->GetDSCInfo(DSC_INFO_LOCATION);
			pPrevItem = pItem;

			TimeLineInfo stTimeLineInfo;
			pObj->SetTimeLineInfo(&nEffectStart, &nEffectCnt, &stTimeLineInfo, stMovieData);
			arrTimeLineInfo->push_back(stTimeLineInfo);
		}

		// Add Making Movie Count
		pObj->m_nMovieNotFinished++;

		// 데이터 만들어서 전송.MakeMovieToClient
		pMsg = new ESMEvent();
		pMsg->pParam		= (LPARAM)arrTimeLineInfo;
		pMsg->pDest			= (LPARAM)pItem;
		if ( ESMGetExecuteMode() )
		{
			pMsg->message = WM_ESM_NET_MAKEMOVIETOCLIENT;
			::SendMessage(pObj->m_hMainWnd, WM_ESM, WM_ESM_NET, (LPARAM)pMsg);
		}
		else
		{
			pMsg->message = WM_ESM_MOVIE_MAKEMOVIE;
			::SendMessage(pObj->m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}

	}

	int nTimeCnt = abs(ESMGetCntMovieTime(nTime));				
	float fBase = 0.0, fCalc = 0.0;	
	int nTimeIndex = 0, nDSCIndex = 0;
	if( nDSCCnt != 1 && nTime  && nTimeCnt <= nDSCCnt )
	{
		//-------------------------------------------------------------------
		//-- NORMAL LINE (DSC Count > Frame Time Count)
		//--    □
		//--	□
		//--	 □
		//--	 □
		//--	  □
		//--	  □
		//-------------------------------------------------------------------	
		fBase = (float)((float)nTimeCnt / (float)nDSCCnt);
		nTargetTime = nStartTime;

		nTimeIndex	= 0;
		//-- 2013-10-05 hongsu@esmlab.com
		//-- Based on Time 
		arrTimeLineInfo = new vector<TimeLineInfo>;
		MakeMovieInfo stMovieData;

		for(int i = 0 ; i < nDSCCnt ; i ++)
		{

			fCalc = ((float)i*fBase);
			fCalc = (float)((int)(fCalc + 0.5));		//반올림

			if((int)fCalc > nTimeIndex)
			{
				nTimeIndex++;
				if(bNormal)
					ESMGetNextFrameTime(nTargetTime);						
				else
					ESMGetPreviousFrameTime(nTargetTime);
			}
			//-- 2013-10-05 hongsu@esmlab.com
			//-- Check Last
			if(i == nDSCCnt -1)
				nTargetTime = nEndTime;

			pItem	= pObj->GetDSC(i);
			if(pItem->GetDSCStatus() != SDI_STATUS_REC_FINISH)
				continue;

			//-- 2014-09-16 hongsu@esmlab.com
			//-- Skip Sensor Sync Fail DSC Frames 
			if( pItem->GetSensorSync() == FALSE )
			{
				ESMLog(5, _T("SensorSync Skip DSC[%s]"), pItem->GetDeviceDSCID());
				continue;
			}

			if( ( strItemIp != pItem->GetDSCInfo(DSC_INFO_LOCATION) && strItemIp != _T("")) && ESMGetExecuteMode() )
			{
				// Add Making Movie Count
				pObj->m_nMovieNotFinished++;

				// 데이터 만들어서 전송.MakeMovieToClient
				pMsg = new ESMEvent();
				pMsg->message = WM_ESM_NET_MAKEMOVIETOCLIENT;
				pMsg->pParam		= (LPARAM)arrTimeLineInfo;
				pMsg->pDest			= (LPARAM)pPrevItem;
				::SendMessage(pObj->m_hMainWnd, WM_ESM, WM_ESM_NET, (LPARAM)pMsg);
				arrTimeLineInfo = new vector<TimeLineInfo>;
				nMovieModule++;
			}

			pObj->SetMovieData(&stMovieData, pItem, nTargetTime, nEndTime, nFrameSpeed, nMovieModule);
			stMovieData.nEndFrame = stMovieData.nStartFrame;

			TimeLineInfo stTimeLineInfo;
			pObj->SetTimeLineInfo(&nEffectStart, &nEffectCnt, &stTimeLineInfo, stMovieData);
			arrTimeLineInfo->push_back(stTimeLineInfo);
			strItemIp = pItem->GetDSCInfo(DSC_INFO_LOCATION);
			pPrevItem = pItem;
		}

		// Add Making Movie Count
		pObj->m_nMovieNotFinished++;

		// 데이터 만들어서 전송.MakeMovieToClient
		// arrMovieObject 전송
		pMsg = new ESMEvent();
		pMsg->pParam		= (LPARAM)arrTimeLineInfo;
		pMsg->pDest			= (LPARAM)pItem;
		if ( ESMGetExecuteMode() )
		{
			pMsg->message = WM_ESM_NET_MAKEMOVIETOCLIENT;
			::SendMessage(pObj->m_hMainWnd, WM_ESM, WM_ESM_NET, (LPARAM)pMsg);
		}
		else
		{
			pMsg->message = WM_ESM_MOVIE_MAKEMOVIE;
			::SendMessage(pObj->m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}

		
	}
	if( nDSCCnt != 1 && nTime && nTimeCnt > nDSCCnt)
	{
		//-------------------------------------------------------------------
		//-- NORMAL LINE (DSC Count < Frame Time Count)
		//--    □
		//--		□
		//--			□
		//--				□
		//--					□
		//--						□
		//-------------------------------------------------------------------	
		nDSCIndex	= 0;
		nTargetTime = nStartTime;
		int nGapTime = (nEndTime - nStartTime) / nDSCCnt;
		int nMovieStartTime =0 , nMovieEndTime = 0;
		nMovieStartTime = nStartTime;
		nTargetTime = nTargetTime + nGapTime;
		ESMCheckFrameTime(nTargetTime);
		nMovieEndTime = nTargetTime;

		arrTimeLineInfo = new vector<TimeLineInfo>;
		MakeMovieInfo stMovieData;

		for(int i = 0 ; i < nDSCCnt ; i ++)
		{
			pItem	= pObj->GetDSC(i);	
			if(pItem->GetDSCStatus() != SDI_STATUS_REC_FINISH)
				continue;

			
			//-- 2014-09-16 hongsu@esmlab.com
			//-- Skip Sensor Sync Fail DSC Frames 
			if( pItem->GetSensorSync() == FALSE )
			{
				ESMLog(5, _T("SensorSync Skip DSC[%s]"), pItem->GetDeviceDSCID());
				continue;
			}

			if( ( strItemIp != pItem->GetDSCInfo(DSC_INFO_LOCATION) && strItemIp != _T("")) && ESMGetExecuteMode() )
			{
				// Add Making Movie Count
				pObj->m_nMovieNotFinished++;

				// 데이터 만들어서 전송.MakeMovieToClient
				pMsg = new ESMEvent();
				pMsg->message = WM_ESM_NET_MAKEMOVIETOCLIENT;
				pMsg->pParam		= (LPARAM)arrTimeLineInfo;
				pMsg->pDest			= (LPARAM)pPrevItem;
				::SendMessage(pObj->m_hMainWnd, WM_ESM, WM_ESM_NET, (LPARAM)pMsg);
				arrTimeLineInfo = new vector<TimeLineInfo>;
				nMovieModule++;
			}

			pObj->SetMovieData(&stMovieData, pItem, nMovieStartTime, nMovieEndTime, nFrameSpeed, nMovieModule);
			// 2014. 8.21 hjjang
			// 카메라당 프레임이 한개씩 겹치도록 설정
			//if ( nEffectCnt > 0 )
			//	nEffectStart = nEffectStart - 1;

			TimeLineInfo stTimeLineInfo;
			pObj->SetTimeLineInfo(&nEffectStart, &nEffectCnt, &stTimeLineInfo, stMovieData);
			arrTimeLineInfo->push_back(stTimeLineInfo);

			strItemIp = pItem->GetDSCInfo(DSC_INFO_LOCATION);
			pPrevItem = pItem;

			nTargetTime = nMovieEndTime;
			nMovieEndTime = nStartTime  + nGapTime * (i + 2);
			ESMCheckFrameTime(nMovieEndTime);
			nMovieStartTime = nTargetTime;
			ESMGetNextFrameTime(nMovieStartTime);
		}

		// Add Making Movie Count
		pObj->m_nMovieNotFinished++;

		// 데이터 만들어서 전송.MakeMovieToClient
		// arrMovieObject 전송
		pMsg = new ESMEvent();
		pMsg->pParam		= (LPARAM)arrTimeLineInfo;
		pMsg->pDest			= (LPARAM)pItem;
		if ( ESMGetExecuteMode() )
		{
			pMsg->message = WM_ESM_NET_MAKEMOVIETOCLIENT;
			::SendMessage(pObj->m_hMainWnd, WM_ESM, WM_ESM_NET, (LPARAM)pMsg);
		}
		else
		{
			pMsg->message = WM_ESM_MOVIE_MAKEMOVIE;
			::SendMessage(pObj->m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_MOVIE, (LPARAM)pMsg);
		}

	}		
	ESMLog(5,_T("[Object][%d] End Movie Make"),pObj->m_nObjectIndex);

	//-- 2013-10-07 hongsu@esmlab.com
	//-- Waiting until Finish Adjustment 
	while(1)
	{
		if(pImgMgr->IsFinishAdjust())
			break;
		Sleep(ESM_ADJ_CHEK_TIME);
	}	

	if(pImgMgr)
	{
		delete pImgMgr;
		pImgMgr = NULL;
	}
	return 0;
}

void  CESMTimeLineObjectEditor::MakePictureMovie(int nIndex, vector<CString>* arrAdjedFile, CRITICAL_SECTION* CR_ADJProcess)
{
	if (m_bClose) 
		return;

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Set Start Make Movie
	SetMakeMovie(FALSE);
	m_nObjectIndex = nIndex;
	m_arrAdjedFile = arrAdjedFile;
	m_CR_ADJProcess = CR_ADJProcess;

	if( m_hMakeObjMovie != NULL)
	{
		CloseHandle(m_hMakeObjMovie);
		m_hMakeObjMovie = NULL;
	}
	m_hMakeObjMovie = (HANDLE) _beginthreadex(NULL, 0, MakePictureObjMovieThread, (void *)this, 0, NULL);
}

unsigned WINAPI CESMTimeLineObjectEditor::MakeObjMovieThread(LPVOID param)
{
	/*
	CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)param;
	if (!pObj) 
		return 0;
	if (pObj->m_bClose) 
		return 0;

	CESMFileOperation fo;
	CESMImgMgr* pImgMgr = new CESMImgMgr();

	int nDSCCnt				= pObj->GetDSCCount();
	int nStartTime			= pObj->GetStartTime();			//-- Start DSC : Time
	int nEndTime			= pObj->GetEndTime();			//-- End DSC : Time
	int nFpsTime			= pObj->GetNextTime();
	int nGopTime			= 15;

	ESMLog(5,_T("[Object] Create TS File on Object [DSC:%d][Time : Start[%d] End[%d]"),nDSCCnt, nStartTime, nEndTime);

	int nSec = 0, nMilli = 0, nTargetTime = 0;
	CString strTargetFile, strTargetFolder, strSrcFile;
	CDSCItem* pItem = NULL;
	CString strDSC, strPrevDSC;

	strTargetFolder.Format(_T("%s\\%d"), ESMGetPath(ESM_PATH_OBJECT), pObj->m_nObjectIndex);
	::CreateDirectory(strTargetFolder, NULL);
	fo.Delete(strTargetFolder,FALSE);

	//-- Copy frame%d.jpg
	//-- 1. StartItem == EndItem
	if(!nDSCCnt)
	{
		ESMLog(0,_T("Non Exist DSC ID on Make Movie"));
		_endthread();
		return 0;
	}

	//-- Target Numbering
	int nIndex = 1;
	//-- 2013-10-08 hongsu@esmlab.com
	//-- Reset Image Manager 
	pImgMgr->ResetAdjust(pObj->m_nObjectIndex, ESM_ADJ_KIND_POSITION);

	//-- Check Normal, Reverse
	int nTime = nEndTime - nStartTime;
	BOOL bNormal;
	if(nTime > 0)
		bNormal = TRUE;
	else
		bNormal = FALSE;

	ESMLog(5,_T("[Object][%d] Start Movie Make"),pObj->m_nObjectIndex);
	if(nDSCCnt == 1)
	{
		//-------------------------------------------------------------------
		//-- SAME DSC (One Line)
		//-------------------------------------------------------------------	
		pItem = pObj->GetDSC(0);
		FFmpegManager FFmpegMgr;
		strTargetFile.Format(_T("%s\\%d.mp4"), strTargetFolder, pObj->m_nObjectIndex);
		if(FFmpegMgr.InitEncode(strTargetFile, nFpsTime, AV_CODEC_ID_MPEG2VIDEO, nGopTime))
		{

			pItem	= pObj->GetDSC(0);
			pObj->SetMovieData(&FFmpegMgr, pItem, nStartTime, nEndTime, pImgMgr);
			FFmpegMgr.CloseEncode();
		}
	}
	if( nDSCCnt != 1 && !nTime)
	{
		//-------------------------------------------------------------------
		//-- SAME TIME (One Line)
		//-- 2013-10-01 hongsu@esmlab.com
		//-- One More Selected DSC 
		//-------------------------------------------------------------------	

		nTargetTime = nStartTime;
		int nIndex = 1;
		FFmpegManager FFmpegMgr;
		strTargetFile.Format(_T("%s\\%d.mp4"), strTargetFolder, pObj->m_nObjectIndex);
		BOOL bLogo = TRUE;
		if(FFmpegMgr.InitEncode(strTargetFile, nFpsTime, AV_CODEC_ID_MPEG2VIDEO, nGopTime))
		{
			for(int i = 0 ; i < nDSCCnt ; i ++)
			{
				pItem	= pObj->GetDSC(i);
				pObj->SetMovieData(&FFmpegMgr, pItem, nTargetTime, nTargetTime, pImgMgr, bLogo);
			}
			FFmpegMgr.CloseEncode();
		}
	}

	int nTimeCnt = abs(ESMGetCntMovieTime(nTime));				
	float fBase = 0.0, fCalc = 0.0;	
	int nTimeIndex = 0, nDSCIndex = 0;
	if( nDSCCnt != 1 && nTime  && nTimeCnt <= nDSCCnt )
	{
		//-------------------------------------------------------------------
		//-- NORMAL LINE (DSC Count > Frame Time Count)
		//--    □
		//--	□
		//--	 □
		//--	 □
		//--	  □
		//--	  □
		//-------------------------------------------------------------------	
		fBase = (float)((float)nTimeCnt / (float)nDSCCnt);
		nTargetTime = nStartTime;

		nTimeIndex	= 0;
		//-- 2013-10-05 hongsu@esmlab.com
		//-- Based on Time 


		FFmpegManager FFmpegMgr;
		strTargetFile.Format(_T("%s\\%d.mp4"), strTargetFolder, pObj->m_nObjectIndex);

		if(FFmpegMgr.InitEncode(strTargetFile, nFpsTime, AV_CODEC_ID_MPEG2VIDEO, nGopTime))
		{
			for(int i = 0 ; i < nDSCCnt ; i ++)
			{

				fCalc = ((float)i*fBase);
				fCalc = (float)((int)(fCalc + 0.5));		//반올림

				if((int)fCalc > nTimeIndex)
				{
					nTimeIndex++;
					if(bNormal)
						ESMGetNextFrameTime(nTargetTime);						
					else
						ESMGetPreviousFrameTime(nTargetTime);
				}
				//-- 2013-10-05 hongsu@esmlab.com
				//-- Check Last
				if(i == nDSCCnt -1)
					nTargetTime = nEndTime;

				pItem	= pObj->GetDSC(i);
				pObj->SetMovieData(&FFmpegMgr, pItem, nTargetTime, nTargetTime, pImgMgr);
			}
			FFmpegMgr.CloseEncode();		
		}
	}
	if( nDSCCnt != 1 && nTime && nTimeCnt > nDSCCnt)
	{
		//-------------------------------------------------------------------
		//-- NORMAL LINE (DSC Count < Frame Time Count)
		//--    □
		//--		□
		//--			□
		//--				□
		//--					□
		//--						□
		//-------------------------------------------------------------------	
		nDSCIndex	= 0;
		nTargetTime = nStartTime;
		int nGapTime = (nEndTime - nStartTime) / nDSCCnt;
		int nMovieStartTime =0 , nMovieEndTime = 0;
		nMovieStartTime = nStartTime;
		nTargetTime = nTargetTime + nGapTime;
		ESMCheckFrameTime(nTargetTime);
		nMovieEndTime = nTargetTime;

		FFmpegManager FFmpegMgr;
		strTargetFile.Format(_T("%s\\%d.mp4"), strTargetFolder, pObj->m_nObjectIndex);

		if(FFmpegMgr.InitEncode(strTargetFile, nFpsTime, AV_CODEC_ID_MPEG2VIDEO, nGopTime))
		{
			for(int i = 0 ; i < nDSCCnt ; i ++)
			{
				pItem	= pObj->GetDSC(i);	
				pObj->SetMovieData(&FFmpegMgr, pItem, nMovieStartTime, nMovieEndTime, pImgMgr);

				nTargetTime = nMovieEndTime;
				nMovieEndTime = nStartTime  + nGapTime * (i + 2);
				ESMCheckFrameTime(nMovieEndTime);
				nMovieStartTime = nTargetTime;
			}
			FFmpegMgr.CloseEncode();
		}
	}		
	ESMLog(5,_T("[Object][%d] End Movie Make"),pObj->m_nObjectIndex);

	//-- 2013-10-07 hongsu@esmlab.com
	//-- Waiting until Finish Adjustment 
	while(1)
	{
		if(pImgMgr->IsFinishAdjust())
			break;
		Sleep(ESM_ADJ_CHEK_TIME);
	}
	ESMLog(5,_T("[Object][%d] Finish Create Movie File [%s]"),pObj->m_nObjectIndex, strTargetFolder);		

	if(pImgMgr)
	{
		delete pImgMgr;
		pImgMgr = NULL;
	}

	pObj->SetMakeMovie(TRUE);
	*/
	return 0;
}

void CESMTimeLineObjectEditor::SetMovieData(FFmpegManager* pFFmpegMgr, CDSCItem* pItem , int nStartTime, int nEndTime, CESMImgMgr* pImgMgr, BOOL bLogo)
{
	/*
	CString strDSC	= pItem->GetDeviceDSCID();
	CString strSrcFile;
	AdjustInfo AdjInfo;
	AdjInfo.m_AdjptLT.x		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_LEFT	);
	AdjInfo.m_AdjptLT.y		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_TOP	);
	AdjInfo.m_AdjptRB.x		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_RIGHT	);
	AdjInfo.m_AdjptRB.y		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_BOTTOM);
	AdjInfo.m_AdjdbMaxAngle	= ESMGetFValue(ESM_VALUE_ADJ_VIEW_MA	);	
	AdjInfo.m_AdjptAdj.x	= pItem->GetDSCAdj(DSC_ADJ_X);
	AdjInfo.m_AdjptAdj.y	= pItem->GetDSCAdj(DSC_ADJ_Y);
	AdjInfo.m_AdjptRotate.x = pItem->GetDSCAdj(DSC_ADJ_RX);
	AdjInfo.m_AdjptRotate.y = pItem->GetDSCAdj(DSC_ADJ_RY);
	AdjInfo.m_AdjdbAngleAdjust	= pItem->GetDSCAdj(DSC_ADJ_A);
	AdjInfo.m_AdjdAdjustSize	= pItem->GetDSCAdj(DSC_ADJ_SCALE);
	AdjInfo.m_AdjdAdjustSize	= pItem->GetDSCAdj(DSC_ADJ_SCALE);
	AdjInfo.m_strDSC = strDSC;
	AdjInfo.m_pImgMgr = pImgMgr;

	strSrcFile.Format(_T("%s\\%s\\%s.mp4"),ESMGetPath(ESM_PATH_MOVIE), ESMGetFrameRecord(), strDSC);
	pFFmpegMgr->SetAdjust(&AdjInfo);
	pFFmpegMgr->SetMakeMovie(strSrcFile, strDSC, ESMGetFrameIndex(nStartTime), ESMGetFrameIndex(nEndTime), bLogo);
	*/
}

unsigned WINAPI CESMTimeLineObjectEditor::MakePictureObjMovieThread(LPVOID param)
{
	CESMTimeLineObjectEditor* pObj = (CESMTimeLineObjectEditor*)param;
	if (!pObj) 
		return 0;
	if (pObj->m_bClose) 
		return 0;

	CESMFileOperation fo;
	CESMImgMgr* pImgMgr = new CESMImgMgr();

	int nDSCCnt				= pObj->GetDSCCount();
	int nStartTime			= pObj->GetStartTime();			//-- Start DSC : Time
	int nEndTime			= pObj->GetEndTime();			//-- End DSC : Time
	int nNextTime			= pObj->GetNextTime();

	ESMLog(5,_T("[Object] Create TS File on Object [DSC:%d][Time : Start[%d] End[%d]"),nDSCCnt, nStartTime, nEndTime);

	int nSec = 0, nMilli = 0, nTargetTime = 0;
	CString strTargetFile, strTargetFolder;
	CDSCItem* pItem = NULL;
	CString strDSC, strPrevDSC;
	//-- 2013-12-06 kcd
	//-- Dsc ChangMark 인자
	int arrModCamIndicater[MAX_IMAGECOUNT];
	memset(arrModCamIndicater, -1 , sizeof(int) * MAX_IMAGECOUNT);

	strTargetFolder.Format(_T("%s\\%d"), ESMGetPath(ESM_PATH_OBJECT), pObj->m_nObjectIndex);
	::CreateDirectory(strTargetFolder, NULL);
	fo.Delete(strTargetFolder,FALSE);

	//-- Copy frame%d.jpg
	//-- 1. StartItem == EndItem
	if(!nDSCCnt)
	{
		ESMLog(0,_T("Non Exist DSC ID on Make Movie"));
		_endthread();
		return 0;
	}

	//-- Target Numbering
	int nIndex = 1;
	//-- 2013-10-08 hongsu@esmlab.com
	//-- Reset Image Manager 
	pImgMgr->ResetAdjust(pObj->m_nObjectIndex, ESM_ADJ_KIND_POSITION);

	//-- Check Normal, Reverse
	int nTime = nEndTime - nStartTime;
	BOOL bNormal;
	if(nTime > 0)
		bNormal = TRUE;
	else
		bNormal = FALSE;

	ESMLog(5,_T("[Object][%d] Create Adjusted Images ..."),pObj->m_nObjectIndex);

	vector<int> vPLogo(nDSCCnt);
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);

	if( nDSCCnt != 1 && !nTime)
	{
		//-------------------------------------------------------------------
		//-- SAME TIME (One Line)
		//-- 2013-10-01 hongsu@esmlab.com
		//-- One More Selected DSC 
		//-------------------------------------------------------------------	
		nTargetTime = nStartTime;
		ESMGetMovieTime(nTargetTime,nSec,nMilli);
		for(int i = 0 ; i < nDSCCnt ; i ++)
		{
			pItem	= pObj->GetDSC(i);
			strDSC	= pItem->GetDeviceDSCID();
			arrModCamIndicater[i] = 1;

			int nCamIdx = 0;
			CDSCItem* pItem = NULL;
			for( int j =0 ;j < arDSCList.GetSize(); j++)
			{
				pItem = (CDSCItem*)arDSCList.GetAt(j);
				if (strDSC == pItem->GetDeviceDSCID() )
				{
					nCamIdx = j;
					break;
				}
			}

			vPLogo[i] = nCamIdx;

			CString strAdjFolder;
			CString strTargetFile, strAdjFile, strSrcFile;
			CvPoint2D32f ptAdjust, ptRotate;
			double dAdjustSize = 0.0;
			double dAdjustDistance = 0.0;
			double dbAngleAdjust = 0.0;
			CESMFileOperation fo;

			//-- Shared Information from View
			CvPoint2D32f ptLeftTop, ptRightBottom;
			double dbMaxAngle;
			ptLeftTop.x		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_LEFT	);
			ptLeftTop.y		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_TOP	);
			ptRightBottom.x	= ESMGetFValue(ESM_VALUE_ADJ_VIEW_RIGHT	);
			ptRightBottom.y	= ESMGetFValue(ESM_VALUE_ADJ_VIEW_BOTTOM);
			dbMaxAngle		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_MA	);	

			if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
				strSrcFile = ESMGetPicturePath(strDSC);
			else
				strSrcFile.Format(_T("%s\\%s\\%s.jpg"),ESMGetPath(ESM_PATH_PICTURE_FILE), ESMGetFrameRecord(), strDSC);

			if(!fo.IsFileExist(strSrcFile))
				continue;

			strAdjFolder.Format(_T("%s"),ESMGetPath(ESM_PATH_ADJUST));
			fo.CreateFolder(strAdjFolder);
			strAdjFile.Format(_T("%s\\%s.jpg"),strAdjFolder, strDSC);
			strTargetFile.Format(_T("%s\\frame%04d.jpg"),strTargetFolder, nIndex);


			EnterCriticalSection (pObj->m_CR_ADJProcess);
			int nAdjIndex = 0;
			int nAdjCount = pObj->m_arrAdjedFile->size();
			for(nAdjIndex = 0; nAdjIndex < nAdjCount; nAdjIndex++)
			{
				if( pObj->m_arrAdjedFile->at(nAdjIndex) == strAdjFile)
					break;
			}
			if( nAdjIndex != nAdjCount)
			{
				for( int i =0 ;i < 1000; i++)
				{
					if(fo.IsFileExist(strAdjFile))
					{
						fo.Copy(strAdjFile, strTargetFile);
						break;
					}
					Sleep(100);
				}
				LeaveCriticalSection (pObj->m_CR_ADJProcess);
				nIndex++;
				continue;
			}

			pObj->m_arrAdjedFile->push_back(strAdjFile);
			LeaveCriticalSection (pObj->m_CR_ADJProcess);

			ptAdjust.x		= pItem->GetDSCAdj(DSC_ADJ_X);
			ptAdjust.y		= pItem->GetDSCAdj(DSC_ADJ_Y);
			dbAngleAdjust	= pItem->GetDSCAdj(DSC_ADJ_A);
			ptRotate.x		= pItem->GetDSCAdj(DSC_ADJ_RX);
			ptRotate.y		= pItem->GetDSCAdj(DSC_ADJ_RY);
			dAdjustSize		= pItem->GetDSCAdj(DSC_ADJ_SCALE);
			dAdjustDistance	= pItem->GetDSCAdj(DSC_ADJ_DISTANCE);

			pImgMgr->SetAdjust(strSrcFile, strAdjFile, strTargetFile, ptAdjust, ptLeftTop,  ptRightBottom, dbAngleAdjust, dAdjustSize, dAdjustDistance, dbMaxAngle, ptRotate, strDSC);
			nIndex++;
		}	
	}


	//-- 2013-10-07 hongsu@esmlab.com
	//-- Waiting until Finish Adjustment 
	while(1)
	{
		if(pImgMgr->IsFinishAdjust())
			break;
		Sleep(ESM_ADJ_CHEK_TIME);
	}

	ESMLog(5,_T("[Object][%d] Finish Create Adjusted Images "),pObj->m_nObjectIndex);
	//kcd 영상 Image 보정
// 	if ( ESMGetValue(ESM_VALUE_ADJ_REVISION_USE) )
// 	{
// 		pImgMgr->MakeAvgImage(strTargetFolder, nIndex, arrModCamIndicater);
// 		pImgMgr->AvgImageFileChange(strTargetFolder, nIndex);
// 	}

	if ( ESMGetValue(ESM_VALUE_INSERTLOGO) )
	{
		pImgMgr->AddBanner(strTargetFolder, nIndex, vPLogo);
	}

	CString strCmd;
	strCmd.Format(_T("%s\\bin\\ffmpeg.exe"),ESMGetPath(ESM_PATH_HOME));

	CString strOpt, strOpt1, strOpt2;	
	int nFps = 1000 / nNextTime; 
	strOpt1.Format(_T("-r %d -i \"%s\\"),nFps, strTargetFolder);
	strOpt1.Append(_T("frame%04d.jpg\" -q:v 1 "));
	strOpt2.Format(_T("\"%s\\%d.mp4"),strTargetFolder,pObj->m_nObjectIndex);	 
	//strOpt2.Format(_T("\"%s\\%d.mp4"),strTargetFolder,pObj->m_nObjectIndex);	

	strOpt.Format(_T("%s %s"),strOpt1, strOpt2);

	//-- 2013-10-01 hongsu@esmlab.com
	//-- Log 
	ESMLog(5,_T("%s %s"),strCmd, strOpt);
	ESMLog(5,_T("[Object][%d] Create Movie File ..."),pObj->m_nObjectIndex);

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;
	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);
	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	ESMLog(5,_T("[Object][%d] Finish Create Movie File [%s]"),pObj->m_nObjectIndex, strTargetFolder);		

	if(pImgMgr)
	{
		delete pImgMgr;
		pImgMgr = NULL;
	}

	pObj->SetMakeMovie(TRUE);
	return 0;
}

BOOL CESMTimeLineObjectEditor::AdJustPictureFile(CDSCItem* pItem, CString strDSC, CESMImgMgr* pImgMgr, CString strTargetFolder, int nSec, int nMilli, int nIndex)
{
	CString strSrcFolder, strAdjFolder;
	CString strTargetFile, strAdjFile, strSrcFile;
	CvPoint2D32f ptAdjust, ptRotate;
	double dAdjustSize = 0.0;
	double dAdjustDistance = 0.0;
	double dbAngleAdjust = 0.0;
	CESMFileOperation fo;

	//-- Shared Information from View
	CvPoint2D32f ptLeftTop, ptRightBottom;
	double dbMaxAngle;
	ptLeftTop.x		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_LEFT	);
	ptLeftTop.y		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_TOP	);
	ptRightBottom.x	= ESMGetFValue(ESM_VALUE_ADJ_VIEW_RIGHT	);
	ptRightBottom.y	= ESMGetFValue(ESM_VALUE_ADJ_VIEW_BOTTOM);
	dbMaxAngle		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_MA	);	

	strSrcFolder.Format(_T("%s\\%s\\%s"),ESMGetDataPath(pItem->GetDSCInfo(DSC_INFO_LOCATION)), ESMGetFrameRecord(), strDSC);
	strSrcFile.Format(_T("%s\\%d.%03d.jpg"),strSrcFolder, nSec, nMilli);

	//-- 2013-10-19 hongsu@esmlab.com
	//-- Find Next Frame 
	if(!fo.IsFileExist(strSrcFile))
		return FALSE;

	strAdjFolder.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_ADJUST), strDSC);
	fo.CreateFolder(strAdjFolder);
	strAdjFile.Format(_T("%s\\%d.%03d.jpg"),strAdjFolder, nSec, nMilli);

	strTargetFile.Format(_T("%s\\frame%04d.jpg"),strTargetFolder, nIndex);

	ptAdjust.x		= pItem->GetDSCAdj(DSC_ADJ_X);
	ptAdjust.y		= pItem->GetDSCAdj(DSC_ADJ_Y);
	dbAngleAdjust	= pItem->GetDSCAdj(DSC_ADJ_A);
	ptRotate.x		= pItem->GetDSCAdj(DSC_ADJ_RX);
	ptRotate.y		= pItem->GetDSCAdj(DSC_ADJ_RY);
	dAdjustSize		= pItem->GetDSCAdj(DSC_ADJ_SCALE);
	dAdjustDistance	= pItem->GetDSCAdj(DSC_ADJ_DISTANCE);

	//-- 2013-10-07 hongsu@esmlab.com
	//-- Create Adjust File 
	if(!fo.IsFileExist(strAdjFile))
	{
		pImgMgr->SetAdjust(strSrcFile, strAdjFile, strTargetFile, ptAdjust, ptLeftTop,  ptRightBottom, dbAngleAdjust, dAdjustSize, dAdjustDistance, dbMaxAngle, ptRotate, strDSC);
	}
	else
		fo.Copy(strAdjFile,strTargetFile);

	return TRUE;
}

void CESMTimeLineObjectEditor::OneDscMakeMovie(CDSCItem* pItem, CString strDSC, CESMImgMgr* pImgMgr, CString strTargetFolder)
{
	int nIndex = 1;
	int nSec = 0, nMilli = 0, nTargetTime = 0;
	CString strSrcFolder, strAdjFolder;
	CString strTargetFile, strAdjFile, strSrcFile;

	CvPoint2D32f ptAdjust, ptRotate;
	double dAdjustSize = 0.0;
	double dAdjustDistance = 0.0;
	double dbAngleAdjust = 0.0;
	CESMFileOperation fo;

	//-- Shared Information from View
	CvPoint2D32f ptLeftTop, ptRightBottom;
	double dbMaxAngle;
	ptLeftTop.x		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_LEFT	);
	ptLeftTop.y		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_TOP	);
	ptRightBottom.x	= ESMGetFValue(ESM_VALUE_ADJ_VIEW_RIGHT	);
	ptRightBottom.y	= ESMGetFValue(ESM_VALUE_ADJ_VIEW_BOTTOM);
	dbMaxAngle		= ESMGetFValue(ESM_VALUE_ADJ_VIEW_MA	);	

	nTargetTime = GetStartTime();
	BOOL bNormal;
	if(GetEndTime() > GetStartTime())
		bNormal = TRUE;
	else
		bNormal = FALSE;

	strSrcFolder.Format(_T("%s\\%s\\%s"),ESMGetDataPath(pItem->GetDSCInfo(DSC_INFO_LOCATION)), ESMGetFrameRecord(), strDSC);

	//strSrcFolder.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_FRAME), strDSC);
	strAdjFolder.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_ADJUST), strDSC);
	fo.CreateFolder(strAdjFolder);

	ptAdjust.x		= pItem->GetDSCAdj(DSC_ADJ_X);
	ptAdjust.y		= pItem->GetDSCAdj(DSC_ADJ_Y);
	dbAngleAdjust	= pItem->GetDSCAdj(DSC_ADJ_A);
	ptRotate.x		= pItem->GetDSCAdj(DSC_ADJ_RX);
	ptRotate.y		= pItem->GetDSCAdj(DSC_ADJ_RY);	
	dAdjustSize		= pItem->GetDSCAdj(DSC_ADJ_SCALE);
	dAdjustDistance	= pItem->GetDSCAdj(DSC_ADJ_DISTANCE);

	while(1)
	{
		ESMGetMovieTime(nTargetTime, nSec, nMilli);

		strSrcFile.Format(_T("%s\\%d.%03d.jpg"),strSrcFolder, nSec, nMilli);
		strAdjFile.Format(_T("%s\\%d.%03d.jpg"),strAdjFolder, nSec, nMilli);
		strTargetFile.Format(_T("%s\\frame%04d.jpg"),strTargetFolder, nIndex);

		if(!fo.IsFileExist(strSrcFile))
		{
			//-- 2013-10-19 hongsu@esmlab.com
			//-- Find Next Frame
			if(bNormal) ESMGetNextFrameTime(nTargetTime);						
			else		ESMGetPreviousFrameTime(nTargetTime);

			if(GetEndTime() < nTargetTime)
			{
				ESMLog(5,_T("[Object][%d] Creating Adjusted Images Failed, File Not Exist."), this->m_nObjectIndex);
				break;
			}
			continue;					
		}

		//-- 2013-10-07 hongsu@esmlab.com
		//-- Create Adjust File 
		if(!fo.IsFileExist(strAdjFile))
		{
			pImgMgr->SetAdjust(strSrcFile, strAdjFile, strTargetFile, ptAdjust, ptLeftTop,  ptRightBottom, dbAngleAdjust, dAdjustSize, dAdjustDistance, dbMaxAngle, ptRotate, strDSC);
		}
		else
			fo.Copy(strAdjFile,strTargetFile);

		if(GetEndTime() == nTargetTime)
			break;

		if(bNormal)
			ESMGetNextFrameTime(nTargetTime);						
		else
			ESMGetPreviousFrameTime(nTargetTime);
		nIndex++;
	}
}

void CESMTimeLineObjectEditor::GetCenterIndex(int &nDscIndex, int &nTimeIndex)
{
	int nStartTimeIdx = ESMGetFrameIndex(GetStartTime());
	int nEndTimeIdx = ESMGetFrameIndex(GetEndTime());

	nTimeIndex = ( nStartTimeIdx + nEndTimeIdx ) / 2;

	int nCnt = GetDSCCount();
	CDSCItem* pStartDsc = GetDSC(0);
	CDSCItem* pEndDsc = GetDSC(nCnt-1);

	int nStartDscIdx = ESMGetDSCIndex(pStartDsc->GetDeviceDSCID());
	int nEndDscIdx = ESMGetDSCIndex(pEndDsc->GetDeviceDSCID());

	nDscIndex = ( nStartDscIdx + nEndDscIdx ) / 2;
}


// 20150630 yongmin distributed processing
int CESMTimeLineObjectEditor::GetFrameData(vector<MakeFrameInfo>* pArrFrameInfo, int nObjectIndex,CString strTime)
{
	int nDSCCnt				= GetDSCCount();
	int nStartTime			= GetStartTime();			//-- Start DSC : Time
	int nEndTime			= GetEndTime();				//-- End DSC : Time
	int nFrameSpeed			= m_nTime[2];

	//ESMLog(5, _T("@@@@@@@@@@ time = %d @@@@@@@@@@@@@@"), nStartTime);

	if(!nDSCCnt)
		return 0;

	m_nObjectIndex = nObjectIndex;
	int nSec = 0, nMilli = 0, nTargetTime = 0;
	CDSCItem* pItem = NULL;
	ESMEvent* pMsg  = NULL;
	int nMovieModule = 0;
	int nEffectCnt=0, nEffectStart=0;

	//-- Check Normal, Reverse
	int nTime = nEndTime - nStartTime;
	BOOL bNormal;
	if(nTime > 0)
		bNormal = TRUE;
	else
		bNormal = FALSE;

	int nFrameCount = pArrFrameInfo->size();
	
	ESMLog(5,_T("[Object][%d] Start Movie Make"),m_nObjectIndex);
	if(nDSCCnt == 1)
	{
		//-------------------------------------------------------------------
		//-- SAME DSC (One Line)
		//-------------------------------------------------------------------	
		pItem	= GetDSC(0);
		if(pItem != NULL)
		{
			if(ESMGetMakeDelete(pItem->GetDeviceDSCID()) == FALSE)
			{
				ESMLog(5, _T("Making Skip DSC[%s]"), pItem->GetDeviceDSCID());
				return 0;
			}

			if(ESMGetCheckValid())
			{
				SetFrameData(pArrFrameInfo, pItem, nStartTime, nEndTime, nFrameSpeed, nObjectIndex,strTime, TRUE);
			}
			else
			{
				if( pItem->GetSensorSync() == FALSE)
					ESMLog(5, _T("SensorSync Skip DSC[%s]"), pItem->GetDeviceDSCID());
				else
					SetFrameData(pArrFrameInfo, pItem, nStartTime, nEndTime, nFrameSpeed, nObjectIndex,strTime, TRUE);
			}
		}
	}

	if( nDSCCnt != 1 && !nTime)
	{
		//-------------------------------------------------------------------
		//-- SAME TIME (One Line)
		//-------------------------------------------------------------------	
		for(int i = 0 ; i < nDSCCnt ; i ++)
		{
			pItem = GetDSC(i);
			if(pItem != NULL)
			{
				//jhhan 16-10-11
				//pItem->GetDeviceDSCID();	//Making Delete 체크시 contine;
				if(ESMGetMakeDelete(pItem->GetDeviceDSCID()) == FALSE)
				{
					ESMLog(5, _T("Making Skip DSC[%s]"), pItem->GetDeviceDSCID());
					continue;
				}

				if(ESMGetCheckValid())
				{
					SetFrameData(pArrFrameInfo, pItem, nStartTime, nEndTime, nFrameSpeed, -1,strTime);
				}
				else
				{
					if( pItem->GetSensorSync() == FALSE)
					{
						ESMLog(5, _T("SensorSync Skip DSC[%s]"), pItem->GetDeviceDSCID());
						continue;
					}
					SetFrameData(pArrFrameInfo, pItem, nStartTime, nEndTime, nFrameSpeed, -1,strTime);
				}
			}
		}
	}

	int nTimeCnt = abs(ESMGetCntMovieTime(nTime));
	float fBase = 0.0, fCalc = 0.0;	
	int nTimeIndex = 0, nDSCIndex = 0;
	if( nDSCCnt != 1 && nTime  && nTimeCnt <= nDSCCnt )
	{
		//-------------------------------------------------------------------
		//-- NORMAL LINE (DSC Count > Frame Time Count)
		//--    □
		//--	□
		//--	 □
		//--	 □
		//-------------------------------------------------------------------	
		fBase = (float)((float)nTimeCnt / (float)nDSCCnt);
		nTargetTime = nStartTime;

		nTimeIndex	= 0;

		for(int i = 0 ; i < nDSCCnt ; i ++)
		{
			fCalc = ((float)i*fBase);
			fCalc = (float)((int)(fCalc + 0.5));		//반올림

			if((int)fCalc > nTimeIndex)
			{
				nTimeIndex++;
				if(bNormal)
					ESMGetNextFrameTime(nTargetTime);						
				else
					ESMGetPreviousFrameTime(nTargetTime);
			}
			//-- 2013-10-05 hongsu@esmlab.com
			//-- Check Last
			if(i == nDSCCnt -1)
				nTargetTime = nEndTime;

			pItem	= GetDSC(i);
			if(pItem == NULL) continue;
			//jhhan 16-10-11
			//pItem->GetDeviceDSCID();	//Making Delete 체크시 contine;
			if(ESMGetMakeDelete(pItem->GetDeviceDSCID()) == FALSE)
			{
				ESMLog(5, _T("Making Skip DSC[%s]"), pItem->GetDeviceDSCID());
				continue;
			}	

			if( pItem->GetSensorSync() == FALSE)
			{
				ESMLog(5, _T("SensorSync Skip DSC[%s]"), pItem->GetDeviceDSCID());
				continue;
			}

			SetFrameData(pArrFrameInfo, pItem, nTargetTime, nTargetTime, nFrameSpeed, -1,strTime);
		}
	}

	if( nDSCCnt != 1 && nTime && nTimeCnt > nDSCCnt)
	{
		//-------------------------------------------------------------------
		//-- NORMAL LINE (DSC Count < Frame Time Count)
		//--    □
		//--		□
		//--			□
		//--				□
		//-------------------------------------------------------------------	
		nDSCIndex	= 0;
		nTargetTime = nStartTime;
		int nGapTime = (nEndTime - nStartTime) / nDSCCnt;
		int nMovieStartTime =0 , nMovieEndTime = 0;
		nMovieStartTime = nStartTime;
		nTargetTime = nTargetTime + nGapTime;
		ESMCheckFrameTime(nTargetTime);
		nMovieEndTime = nTargetTime;

		for(int i = 0 ; i < nDSCCnt ; i ++)
		{
			pItem	= GetDSC(i);
			if(pItem == NULL) continue;
			//jhhan 16-10-11
			//pItem->GetDeviceDSCID();	//Making Delete 체크시 contine;
			if(ESMGetMakeDelete(pItem->GetDeviceDSCID()) == FALSE)
			{
				ESMLog(5, _T("Making Skip DSC[%s]"), pItem->GetDeviceDSCID());
				continue;
			}	

			if( pItem->GetSensorSync() == FALSE )
				ESMLog(5, _T("SensorSync Skip DSC[%s]"), pItem->GetDeviceDSCID());
			else
				SetFrameData(pArrFrameInfo, pItem, nMovieStartTime, nMovieEndTime, nFrameSpeed, nObjectIndex,strTime);

			nTargetTime = nMovieEndTime;
			nMovieEndTime = nStartTime  + nGapTime * (i + 2);
			ESMCheckFrameTime(nMovieEndTime);
			nMovieStartTime = nTargetTime;
			ESMGetNextFrameTime(nMovieStartTime);
		}

	}		
	ESMLog(5,_T("[Object][%d] End Movie Make"),m_nObjectIndex);

#if 0
	int sdf = pArrFrameInfo->size();
	for(int i=nFrameCount ; i<pArrFrameInfo->size() ; i++)
	{		
		//2015-08-10 cygil Effect VMCC Add
		CESMObjectFrame* pObjFrame = this->GetObjectFrame(i-nFrameCount);
		if(pObjFrame != NULL)
		{
			if ( pObjFrame->IsZoom() == TRUE  )
			{
				
				pArrFrameInfo->at(i).stEffectData.nBlurWeight = pObjFrame->GetBlurWeight();
			}
		}
	}

	int nIndex = nFrameCount;
	for(int i=nFrameCount ; i<nDSCCnt ; i++)
	{
		pItem = GetDSC(i);
		if(pItem != NULL)
		{
			if( pItem->GetSensorSync() == FALSE)
			{
				continue;
			}
			//2015-08-10 cygil Effect VMCC Add
			CESMObjectFrame* pObjFrame = this->GetObjectFrame(i-nFrameCount);
			if(pObjFrame != NULL)
			{
				if ( pObjFrame->IsZoom() == TRUE  )
				{
					pArrFrameInfo->at(nIndex).stEffectData.bZoom = pObjFrame->IsZoom();
					pArrFrameInfo->at(nIndex).stEffectData.nPosX = pObjFrame->GetPosX();
					pArrFrameInfo->at(nIndex).stEffectData.nPosY = pObjFrame->GetPosY();
					pArrFrameInfo->at(nIndex).stEffectData.nZoomRatio = pObjFrame->GetZoomRatio();
					pArrFrameInfo->at(nIndex).stEffectData.bBlur = pObjFrame->IsBlur();
					pArrFrameInfo->at(nIndex).stEffectData.nBlurSize = pObjFrame->GetBlurSize();
					pArrFrameInfo->at(nIndex).stEffectData.nBlurWeight = pObjFrame->GetBlurWeight();

					nIndex++;
				}
			}
		}
		
	}
#endif
	return 0;
}

void CESMTimeLineObjectEditor::SetFrameData(vector<MakeFrameInfo>* pArrFrameInfo, CDSCItem* pItem , int nStartTime, int nEndTime, int nFrameSpeed, int nObjectIndex,CString strTime, BOOL bSameDSC)
{
	CString strSrcFile, strSrcLocal;
	int nStartFrame = 0;
	int nEndFrame	= 0;
	int nDscIndex	= 0;
	int nMovieIndex = 0, nRealFrameIndex = 0; 

	nStartFrame = ESMGetFrameIndex(nStartTime);
	nEndFrame = ESMGetFrameIndex(nEndTime);

	if(nStartFrame < 0)
		nStartFrame= 0;

	if(nEndFrame < 0)
		nEndFrame= 0;

#if 1
	BOOL bReverse = FALSE;
	if(nEndTime < nStartTime)
		bReverse = TRUE;

	//wgkim 180528
	int nSameDSCIndex = 0;

	if(bReverse)  //Reverse
	{
		for (int i = nStartFrame ; i>=nEndFrame ; i--)
		{
			MakeFrameInfo stMakeFrameInfo;
			strSrcFile = ESMGetMoviePath(pItem->GetDeviceDSCID(), i);
			_stprintf(stMakeFrameInfo.strFramePath, _T("%s"), strSrcFile);

			//jhhan 170328 Local
			strSrcLocal = ESMGetMoviePath(pItem->GetDeviceDSCID(), i, TRUE);
			_stprintf(stMakeFrameInfo.strLocal, _T("%s"), strSrcLocal);

			stMakeFrameInfo.nFrameSpeed = nFrameSpeed;
			ESMGetMovieIndex(i, nMovieIndex, nRealFrameIndex);
			stMakeFrameInfo.nFrameIndex = nRealFrameIndex;
			if( nStartTime == nEndTime && nObjectIndex != -1)
				stMakeFrameInfo.nObjectIndex = -1;
			else
				stMakeFrameInfo.nObjectIndex = nObjectIndex;

			if(ESMGetValue(ESM_VALUE_INSERTLOGO))
			{
				stMakeFrameInfo.nDscIndex   = pItem->GetIndex();
				stMakeFrameInfo.b3DLogo = ESMGetValue(ESM_VALUE_3DLOGO);
				stMakeFrameInfo.bLogoZoom = ESMGetValue(ESM_VALUE_LOGOZOOM);
			}
			else
				stMakeFrameInfo.nDscIndex   = -1;

			int priValue = GetKzoneIdx();
			if(ESMGetValue(ESM_VALUE_INSERTPRISM) || priValue > 0)
			{
				stMakeFrameInfo.nPriIndex	= pItem->GetIndex();
				stMakeFrameInfo.nPrinumIndex = GetKzoneIdx();
				if(ESMGetValue(ESM_VALUE_PRISMVALUE) > 0 && ESMGetValue(ESM_VALUE_PRISMVALUE2) > 0)
				{
					stMakeFrameInfo.nPriValue	= ESMGetValue(ESM_VALUE_PRISMVALUE);
					stMakeFrameInfo.nPriValue2	= ESMGetValue(ESM_VALUE_PRISMVALUE2);
				}
				else
				{
					stMakeFrameInfo.nPriValue	= 30;
					stMakeFrameInfo.nPriValue2	= 20;
				}
			}
			else
				stMakeFrameInfo.nPriIndex	= -1;

			/*if(ESMGetValue(ESM_VALUE_AUTO_WHITEBALANCE))
				stMakeFrameInfo.bInsertWB = TRUE;
			else
				stMakeFrameInfo.bInsertWB = FALSE;*/

			if(ESMGetValue(ESM_VALUE_COLORREVISION))
				stMakeFrameInfo.bColorRevision = TRUE;
			else
				stMakeFrameInfo.bColorRevision = FALSE;

			if(ESMGetValue(ESM_VALUE_INSERT_CAMERAID))
				_stprintf(stMakeFrameInfo.strCameraID, _T("%s"), pItem->GetDeviceDSCID());
			else
				memset(stMakeFrameInfo.strCameraID, 0, MAX_PATH);
			
			if(ESMGetValue(ESM_VALUE_PRINT_COLORINFO))
				stMakeFrameInfo.bPrnColorInfo = TRUE;
			else
				stMakeFrameInfo.bPrnColorInfo = FALSE;

			stMakeFrameInfo.bMovieReverse = pItem->m_bReverse;
			stMakeFrameInfo.nVertical = pItem->GetVertical();
			stMakeFrameInfo.ptVerticalCenterPoint = pItem->GetPtVerticalCenterPoint();

			_stprintf(stMakeFrameInfo.strDSC_ID, _T("%s"), pItem->GetDeviceDSCID());//유니코드
			_stprintf(stMakeFrameInfo.strDSC_IP, _T("%s"), pItem->GetDSCInfo(DSC_INFO_LOCATION));//유니코드

			EffectInfo stEffectData;
			CESMObjectFrame* pObjFrame = this->GetObjectFrame(pItem->GetDeviceDSCID());
			if(pObjFrame != NULL)
			{
				if ( pObjFrame->IsZoom() == TRUE  )
				{
					stMakeFrameInfo.stEffectData.bZoom = pObjFrame->IsZoom();
					stMakeFrameInfo.stEffectData.nPosX = pObjFrame->GetPosX();
					stMakeFrameInfo.stEffectData.nPosY = pObjFrame->GetPosY();

					stMakeFrameInfo.stEffectData.bStepFrame = FALSE;
					if(bSameDSC == FALSE)
						stMakeFrameInfo.stEffectData.nZoomRatio = pObjFrame->GetZoomRatio();
					else
					{
						//wgkim 180528
						CESMObjectFrame* pExistFrame = this->GetObjectFrame(nSameDSCIndex);
						nSameDSCIndex++;
						stMakeFrameInfo.stEffectData.nZoomRatio = pExistFrame->GetZoomRatio();

						if( nStartTime == nEndTime)
							stMakeFrameInfo.stEffectData.bStepFrame = TRUE;
					}

					stMakeFrameInfo.stEffectData.bBlur = pObjFrame->IsBlur();
					stMakeFrameInfo.stEffectData.nBlurSize = pObjFrame->GetBlurSize();
					stMakeFrameInfo.stEffectData.nBlurWeight = pObjFrame->GetBlurWeight();

					//stMakeFrameInfo.stEffectData.bStepFrame = pObjFrame->IsStepFrame();
					stMakeFrameInfo.stEffectData.nStartZoom = pObjFrame->GetStartZoom();
					stMakeFrameInfo.stEffectData.nEndZoom	= pObjFrame->GetEndZoom();

					//stMakeFrameInfo.stEffectData.nStepFrameCount = pObjFrame->GetStepFrameCount();
				}
			}
			_stprintf(stMakeFrameInfo.strTime, _T("%s"), strTime);

			//180703 hjcho
			stMakeFrameInfo.bSameDSC = bSameDSC;
			pArrFrameInfo->push_back(stMakeFrameInfo);
		}
	}
	else // Normal
	{
		for (int i = nStartFrame ; i<=nEndFrame ; i++)
		{
			MakeFrameInfo stMakeFrameInfo;
			strSrcFile = ESMGetMoviePath(pItem->GetDeviceDSCID(), i);
			_stprintf(stMakeFrameInfo.strFramePath, _T("%s"), strSrcFile);

			//jhhan 170328 Local
			strSrcLocal = ESMGetMoviePath(pItem->GetDeviceDSCID(), i, TRUE);
			_stprintf(stMakeFrameInfo.strLocal, _T("%s"), strSrcLocal);

			stMakeFrameInfo.nFrameSpeed = nFrameSpeed;
			ESMGetMovieIndex(i, nMovieIndex, nRealFrameIndex);
			stMakeFrameInfo.nFrameIndex = nRealFrameIndex;
			if( nStartTime == nEndTime && nObjectIndex != -1)
				stMakeFrameInfo.nObjectIndex = -1;
			else
				stMakeFrameInfo.nObjectIndex = nObjectIndex;

			if(ESMGetValue(ESM_VALUE_INSERTLOGO))
			{
				//ESMLog(1,_T("####################%d"), pItem->GetIndex());
				stMakeFrameInfo.nDscIndex   = pItem->GetIndex();
				stMakeFrameInfo.b3DLogo = ESMGetValue(ESM_VALUE_3DLOGO);
				stMakeFrameInfo.bLogoZoom = ESMGetValue(ESM_VALUE_LOGOZOOM);
			}
			else
				stMakeFrameInfo.nDscIndex   = -1;

			int priValue = GetKzoneIdx();
			if(ESMGetValue(ESM_VALUE_INSERTPRISM) || priValue > 0)
			{
				stMakeFrameInfo.nPriIndex	= pItem->GetIndex();
				stMakeFrameInfo.nPrinumIndex = GetKzoneIdx();				
				if(ESMGetValue(ESM_VALUE_PRISMVALUE) > 15 && ESMGetValue(ESM_VALUE_PRISMVALUE2) > 10)
				{
					stMakeFrameInfo.nPriValue	= ESMGetValue(ESM_VALUE_PRISMVALUE);
					stMakeFrameInfo.nPriValue2	= ESMGetValue(ESM_VALUE_PRISMVALUE2);
				}
				else
				{
					stMakeFrameInfo.nPriValue	= 15;
					stMakeFrameInfo.nPriValue2	= 10;
				}
			}
			else
			{
				stMakeFrameInfo.nPriIndex	= -1;
				stMakeFrameInfo.nPriValue   = -1;
				stMakeFrameInfo.nPriValue2  = -1;
			}

			/*if(ESMGetValue(ESM_VALUE_AUTO_WHITEBALANCE))
				stMakeFrameInfo.bInsertWB = TRUE;
			else
				stMakeFrameInfo.bInsertWB = FALSE;*/

			if(ESMGetValue(ESM_VALUE_COLORREVISION))
				stMakeFrameInfo.bColorRevision = TRUE;
			else
				stMakeFrameInfo.bColorRevision = FALSE;

			if(ESMGetValue(ESM_VALUE_INSERT_CAMERAID))
				_stprintf(stMakeFrameInfo.strCameraID, _T("%s"), pItem->GetDeviceDSCID());
			else
				memset(stMakeFrameInfo.strCameraID, 0, MAX_PATH);

			if(ESMGetValue(ESM_VALUE_PRINT_COLORINFO))
				stMakeFrameInfo.bPrnColorInfo = TRUE;
			else
				stMakeFrameInfo.bPrnColorInfo = FALSE;

			stMakeFrameInfo.bMovieReverse = pItem->m_bReverse;
			stMakeFrameInfo.nVertical = pItem->GetVertical();
			stMakeFrameInfo.ptVerticalCenterPoint = pItem->GetPtVerticalCenterPoint();

			_stprintf(stMakeFrameInfo.strDSC_ID, _T("%s"), pItem->GetDeviceDSCID());//유니코드
			_stprintf(stMakeFrameInfo.strDSC_IP, _T("%s"), pItem->GetDSCInfo(DSC_INFO_LOCATION));//유니코드

			EffectInfo stEffectData;
			CESMObjectFrame* pObjFrame = this->GetObjectFrame(pItem->GetDeviceDSCID());
			if(pObjFrame != NULL)
			{
				if ( pObjFrame->IsZoom() == TRUE  )
				{

					stMakeFrameInfo.stEffectData.bZoom = pObjFrame->IsZoom();
					stMakeFrameInfo.stEffectData.nPosX = pObjFrame->GetPosX();
					stMakeFrameInfo.stEffectData.nPosY = pObjFrame->GetPosY();

					stMakeFrameInfo.stEffectData.bStepFrame = FALSE;
					if(bSameDSC == FALSE)
						stMakeFrameInfo.stEffectData.nZoomRatio = pObjFrame->GetZoomRatio();
					else
					{
						//wgkim 180528
						CESMObjectFrame* pExistFrame = this->GetObjectFrame(nSameDSCIndex);
						nSameDSCIndex++;
						stMakeFrameInfo.stEffectData.nZoomRatio = pExistFrame->GetZoomRatio();

						if( nStartTime == nEndTime)
							stMakeFrameInfo.stEffectData.bStepFrame = TRUE;
					}

					stMakeFrameInfo.stEffectData.bBlur = pObjFrame->IsBlur();
					stMakeFrameInfo.stEffectData.nBlurSize = pObjFrame->GetBlurSize();
					stMakeFrameInfo.stEffectData.nBlurWeight = pObjFrame->GetBlurWeight();

					//stMakeFrameInfo.stEffectData.bStepFrame = pObjFrame->IsStepFrame();
					stMakeFrameInfo.stEffectData.nStartZoom = pObjFrame->GetStartZoom();
					stMakeFrameInfo.stEffectData.nEndZoom	= pObjFrame->GetEndZoom();
					//stMakeFrameInfo.stEffectData.nStepFrameCount = pObjFrame->GetStepFrameCount();
				}
			}
			_stprintf(stMakeFrameInfo.strTime, _T("%s"), strTime);

			stMakeFrameInfo.bSameDSC = bSameDSC;
			pArrFrameInfo->push_back(stMakeFrameInfo);
		}
	}

	
#else
	for (int i = nStartFrame ; i<=nEndFrame ; i++)
	{
		MakeFrameInfo stMakeFrameInfo;

		// 		if(ESMGetExecuteMode())
		// 			strSrcFile.Format(_T("\\\\%s\\Movie2\\%s.mp4"),pItem->GetDSCInfo(DSC_INFO_LOCATION),pItem->GetDSCInfo(DSC_INFO_ID));
		// 		else
		// 		{
		// 			//MP4ToJpeg(pItem->GetDSCInfo(DSC_INFO_ID), i);
		// 			CString strServerIP, strTp;
		// 			strTp.Format(_T("%s"), ESMGetPath(ESM_PATH_MOVIE_FILE));
		// 			strSrcFile.Format(_T("%s\\%s\\%s.mp4"), strTp, ESMGetFrameRecord(), pItem->GetDSCInfo(DSC_INFO_ID));
		// // 			CString strServerIP, strTp;
		// // 			strTp.Format(_T("%s"), ESMGetPath(ESM_PATH_FILESERVER));
		// // 			strServerIP = strTp.Left(strTp.ReverseFind('\\'));
		// // 
		// // 			strSrcFile.Format(_T("%s\\Movie\\%s\\%s_%d.jpg"), strServerIP, ESMGetFrameRecord(), pItem->GetDSCInfo(DSC_INFO_ID), i);
		// 
		// 			//			strSrcFile.Format(_T("%s\\%s\\%s_%d.jpg"), ESMGetServerRamPath(), ESMGetFrameRecord(), pItem->GetDSCInfo(DSC_INFO_ID), i);
		// 		}
		strSrcFile = ESMGetMoviePath(pItem->GetDeviceDSCID(), i);
		_stprintf(stMakeFrameInfo.strFramePath, _T("%s"), strSrcFile);
		stMakeFrameInfo.nFrameSpeed = nFrameSpeed;
		ESMGetMovieIndex(i, nMovieIndex, nRealFrameIndex);
		stMakeFrameInfo.nFrameIndex = nRealFrameIndex;
		if( nStartTime == nEndTime && nObjectIndex != -1)
			stMakeFrameInfo.nObjectIndex = -1;
		else
			stMakeFrameInfo.nObjectIndex = nObjectIndex;

		if(ESMGetValue(ESM_VALUE_INSERTLOGO))
		{
			stMakeFrameInfo.nDscIndex   = pItem->GetIndex();
			stMakeFrameInfo.b3DLogo = ESMGetValue(ESM_VALUE_3DLOGO);
			stMakeFrameInfo.bLogoZoom = ESMGetValue(ESM_VALUE_LOGOZOOM);
		}
		else
			stMakeFrameInfo.nDscIndex   = -1;

		

		EffectInfo stEffectData;
		CESMObjectFrame* pObjFrame = this->GetObjectFrame(pItem->GetDeviceDSCID());
		if(pObjFrame != NULL)
		{
			if ( pObjFrame->IsZoom() == TRUE  )
			{
				stMakeFrameInfo.stEffectData.bZoom = pObjFrame->IsZoom();
				stMakeFrameInfo.stEffectData.nPosX = pObjFrame->GetPosX();
				stMakeFrameInfo.stEffectData.nPosY = pObjFrame->GetPosY();
				stMakeFrameInfo.stEffectData.nZoomRatio = pObjFrame->GetZoomRatio();
				stMakeFrameInfo.stEffectData.bBlur = pObjFrame->IsBlur();
				stMakeFrameInfo.stEffectData.nBlurSize = pObjFrame->GetBlurSize();
				stMakeFrameInfo.stEffectData.nBlurWeight = pObjFrame->GetBlurWeight();
			}
		}



		pArrFrameInfo->push_back(stMakeFrameInfo);
	}
#endif
}