#include "stdafx.h"

#include "4DMaker.h"
#include "MainFrm.h"
#include "ESMMakeViewMovie.h"
#include "ESMFileOperation.h"
#include "FFmpegManager.h"

CESMMakeViewMove::CESMMakeViewMove()
{
	m_bMakingFinish = TRUE;
	m_pMainWnd = (CMainFrame*)AfxGetMainWnd();
	m_bThreadStop = TRUE;
	InitializeCriticalSection(&m_criInsert);
	m_bThreadRun = FALSE;
}
CESMMakeViewMove::~CESMMakeViewMove()
{
	DeleteCriticalSection(&m_criInsert);
	SetThreadStop(FALSE);
}
void CESMMakeViewMove::AddViewStruct(VIEW_STRUCT view)
{
	EnterCriticalSection(&m_criInsert);
	m_arrViewStruct.push_back(view);	
	LeaveCriticalSection(&m_criInsert);

	Launch();
};
void CESMMakeViewMove::Launch()
{
	if(GetThreadRun() == FALSE)
	{
		HANDLE hSyncTime = NULL;
		hSyncTime = (HANDLE)_beginthreadex(NULL,0,_LaunchThread,(void*)this,0,NULL);
		CloseHandle(hSyncTime);
	}
}
unsigned WINAPI CESMMakeViewMove::_LaunchThread(LPVOID param)
{
	CESMMakeViewMove* pViewMovie = (CESMMakeViewMove*) param;

	pViewMovie->SetThreadRun(TRUE);
	
	while(pViewMovie->GetThreadStop())
	{
		while(!pViewMovie->GetMakingFinish())
		{
			Sleep(1);
		}

		if(pViewMovie->GetViewStructCount())
		{
			int nSendCnt = pViewMovie->SendImage(pViewMovie->GetViewStruct());

			while(1)
			{
				if(nSendCnt == pViewMovie->m_nRecvCnt)
				{
					pViewMovie->CreateViewMovie(pViewMovie->GetViewStruct(),nSendCnt);
					break;
				}

				Sleep(1);
			}

			pViewMovie->DeleteViewStruct();
		}
		else
			break;

		Sleep(1);
	}
	pViewMovie->SetThreadRun(FALSE);

	return FALSE;
}
int CESMMakeViewMove::SendImage(VIEW_STRUCT view)
{
	CObArray DSCArr;
	ESMGetDSCList(&DSCArr);

	int nDivTime = view.nTime / 2;

	int nStartTime = view.nSelectTime - nDivTime;
	int nEndTime = view.nSelectTime + nDivTime;

	int nStartMovIdx=0,nEndMovIdx = 0;
	int nStartFrmIdx=0,nEndFrmIdx = 0;
	//ESMGetMovieIndex(nStartTime,nStartMovIdx,nStartFrmIdx);
	//ESMGetMovieIndex(nEndTime,nEndMovIdx,nEndFrmIdx);

	int nStart = ESMGetFrameIndex(nStartTime);
	int nEnd = ESMGetFrameIndex(nEndTime);
	m_nRecvCnt = 0;
	m_nSendCnt = 0;
	int nMakeSize = view.nArrData.size();
	m_arRCServerList.RemoveAll();

	int nNetCount = m_pMainWnd->m_wndNetwork.GetRCMgrCount();
	for (int i = 0 ; i < nNetCount ; i++)
	{
		CESMRCManager *pRCMgr = m_pMainWnd->m_wndNetwork.GetRCMgr(i);

		if (pRCMgr->m_bConnected/* && pRCMgr->m_bGPUUse*/)
		{
			pRCMgr->ConnectCheck(ESMGetIdentify(),view.nSelectTime);
			m_arRCServerList.Add((CObject*)pRCMgr);
		}
	}

	for(int i = 0 ; i < m_arRCServerList.GetCount() ; i++)
	{
		CESMRCManager *pRCMgr = (CESMRCManager*) m_arRCServerList.GetAt(i);

		for(int j = 0 ; j < nMakeSize ; j++)
		{
			TEMPLATE_VIEW_STRUCT stTemplateView = view.nArrData.at(j);
			
			if(stTemplateView.bSend == FALSE)
			{
				int nIdx = stTemplateView.nCameraIdx - 1;

				if(nIdx < 0)
					nIdx = 0;
				
				/*if(nIdx >= DSCArr.GetCount())
					nIdx = DSCArr.GetCount() - 1;*/

				if(nIdx >= DSCArr.GetCount())
				{
					ESMLog(5,_T("[MULTIVIEW] Camera Index Error"));
					continue;
				}

				CDSCItem* pItem = (CDSCItem*) DSCArr.GetAt(nIdx);
				
				if(ESMGetIPFromDSCID(pItem->GetDeviceDSCID()) == pRCMgr->GetIP())
				{
					if(pRCMgr->GetMultiViewStart())
					{
						while(1)
						{
							if(pRCMgr->GetMultiViewStart() == FALSE)
								break;

							Sleep(1);
						}
					}
					//Send IP
					MakeMultiView pViewMovie;
					pViewMovie.nStartFrame = nStart;
					pViewMovie.nEndFrame   = nEnd;
					pViewMovie.nMovIdx	   = j;
					pViewMovie.nZoom	   = stTemplateView.nZoom;
					pViewMovie.nPointX	   = stTemplateView.nX;
					pViewMovie.nPointY	   = stTemplateView.nY;
					pViewMovie.nFrameRate  = movie_frame_per_second;

					_stprintf(pViewMovie.strCamID,pItem->GetDeviceDSCID());

					CString strPath = ESMGetMoviePath(pItem->GetDeviceDSCID(), nStart);
					int nFind = strPath.ReverseFind(_T('\\'));
					_stprintf(pViewMovie.strPath, _T("%s"), strPath.Left(nFind+1));

					if(pItem->m_bReverse || ESMGetReverseMovie())
						pViewMovie.bReverse = TRUE;
					else
						pViewMovie.bReverse = FALSE;

					pRCMgr->SendMakeMultiViewData(&pViewMovie);
					stTemplateView.bSend = TRUE;

					m_nSendCnt++;
				}
			}
		}
	}
	ESMLog(5,_T("[MULTIVIEW] - Send Finish"));
	return m_nSendCnt;
}
void CESMMakeViewMove::GetRecv(int nIdx,BOOL bSuccess)
{
	m_nRecvCnt++;

	if(bSuccess == FALSE)
		ESMLog(5,_T("[MULTIVIEW] [%d] Making Fail"),nIdx);
}
void CESMMakeViewMove::CreateViewMovie(VIEW_STRUCT view,int nCount)
{
	SYSTEMTIME st;
	GetLocalTime(&st);

	CString strDate = _T("");
	strDate.Format(_T("%04d%02d%02d"),st.wYear,st.wMonth,st.wDay); 

	CString str4DMName = ESMGetFrameRecord();

	CString strMovieFile;
	strMovieFile.Format(_T("%s\\2D\\%s\\[MULTIVIEW]%s.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName);
	
	CESMFileOperation fo;
	CString strForder = _T("");
	strForder.Format(_T("%s\\2D\\%s"),ESMGetPath(ESM_PATH_OUTPUT), strDate);

	if(!fo.IsFileFolder(strForder))
		fo.CreateFolder(strForder);

	int nCnt = 0;
	while(fo.IsFileExist(strMovieFile))
	{
		strMovieFile.Format(_T("%s\\2D\\%s\\[MULTIVIEW]%s_%d.mp4"), ESMGetPath(ESM_PATH_OUTPUT), strDate,str4DMName, nCnt);
		nCnt++;
	}

	struct multiview_decoding
	{
		VideoCapture vc;
		int nIdx;
		BOOL bExist;
	};
	vector<multiview_decoding> stArrDecode;
	int nMakeFailCnt = 0;
	for(int i = 0 ; i < MAX_MOVIE ; i++)
	{
		char strPath[100];
		sprintf(strPath,"M:\\Movie\\%d.mp4",i);

		multiview_decoding st;
		st.nIdx = i;
		if(st.vc.open(strPath))
		{
			st.bExist = TRUE;
		}
		else
		{
			st.bExist = FALSE;
			nMakeFailCnt++;
		}
		stArrDecode.push_back(st);
	}
	BOOL bMakeStart = TRUE;
	if(nMakeFailCnt == MAX_MOVIE)
		bMakeStart = FALSE;

	CString strTemp;
	strTemp.Format(_T("%s\\4DBack.png"),ESMGetPath(ESM_PATH_IMAGE));
	
	CT2CA pszConvertedAnsiString (strTemp);
	string strDst(pszConvertedAnsiString);	
	Mat temp = imread(strDst);
	if(temp.empty())
	{
		temp.create(1080,1920,CV_8UC3);
		temp = Scalar(0x66,0x99,0xFF);
		putText(temp,"No 4DBack.png",cv::Point(960,540),cv::FONT_HERSHEY_PLAIN,10,Scalar(0,0,255),1,8);
	}
	resize(temp,temp,cv::Size(960,540),0,0,1);
	CString strTempSave;
	strTempSave.Format(_T("M:\\[MultiView]%s_Temp.mp4"),str4DMName);

	FFmpegManager ffmpeg(FALSE);
	BOOL bEncode = ffmpeg.InitEncode(strTempSave,30,AV_CODEC_ID_MPEG2VIDEO,1,1920,1080);

	BOOL bStop = TRUE;

	Mat frame[MAX_MOVIE];

	if(bEncode && bMakeStart)
	{
		while(1)
		{
			Mat img(1080,1920,CV_8UC3);
			//Mat* frame = new Mat[MAX_MOVIE];

			for(int mov = 0; mov < MAX_MOVIE ; mov++)
			{
				if(stArrDecode.at(mov).bExist == FALSE)
				{
					frame[mov] = temp.clone();
				}
				else
				{
					stArrDecode.at(mov).vc >> frame[mov];
					if(frame[mov].empty())
					{
						bStop = FALSE;
					}
					if(bStop == FALSE)
					{
						/*delete frame;
						frame = NULL;*/
						break;
					}
				}
			}

			if(bStop == FALSE)
				break;

			for(int i = 0 ; i < 540 ; i++)
			{
				for(int j = 0 ; j < 960 ; j++)
				{
					for(int c = 0 ; c < 3 ; c++)
					{
						img.data[1920 * ((i+0)*3) + ((j+0)*3)+c]     = frame[0].data[960 * (i*3) + (j*3) + c];
						img.data[1920 * ((i+0)*3) + ((j+960)*3)+c]   = frame[1].data[960 * (i*3) + (j*3) + c];
						img.data[1920 * ((i+540)*3) + ((j+0)*3)+c]   = frame[2].data[960 * (i*3) + (j*3) + c]; 
						img.data[1920 * ((i+540)*3) + ((j+960)*3)+c] = frame[3].data[960 * (i*3) + (j*3) + c];
					}
				}
			}
			ffmpeg.EncodePushImage(img.data);

			/*for(int i = 0 ; i < nCount ; i++)
			{
			frame[i].release();
			frame[i]=NULL;
			}*/
		}
		ffmpeg.CloseEncode();

		RunEncode(strTempSave,strMovieFile);
		
		for(int i = 0 ; i < MAX_MOVIE ; i++)
		{
			stArrDecode.at(i).vc.release();
			CString str;
			str.Format(_T("M:\\Movie\\%d.mp4"),i);

			CESMFileOperation fo2;
			if(fo2.IsFileExist(str))
				fo2.Delete(str);
		}
		fo.Delete(strTempSave);

		ESMLog(5,_T("Finish Encode"));

		int nIndex = strMovieFile.ReverseFind('\\') + 1;
		CString strFileName = strMovieFile.Right(strMovieFile.GetLength() - nIndex);

		nIndex = strMovieFile.Find('\\');
		strMovieFile.Delete(0, nIndex);
		CString strPath;
		strPath.Format(_T("\\\\%s%s"), ESMUtil::GetLocalIPAddress(),strMovieFile);

		if(ESMGetConnectAJANetwork())
		{
			CString* pCsMovieFile = new CString;
			pCsMovieFile->Format(_T("%s"),strPath);
			ESMEvent* pAJAEvent = new ESMEvent;
			pAJAEvent->message	= F4DC_AUTO_ADD;
			pAJAEvent->pDest	= (LPARAM)pCsMovieFile;
			ESMSendAJANetwork(pAJAEvent);
		}
		else
		{
			//Write Auto Play
			if(fo.IsFileExist(strPath))
			{
				CESMIni ini;
				CString strFile;
				strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_SDIPLAYER);

				if(ini.SetIniFilename (strFile))
				{
					ini.WriteString(STR_SECTION_SDIPLAYER,STR_FILENAME,strFileName);
					ini.WriteString(STR_SECTION_SDIPLAYER,STR_FILEPATH,strPath);
					ini.WriteString(STR_SECTION_SDIPLAYER,STR_STATE,_T("O"));
				}	
			}
		}
	}
	else
	{
		if(bEncode)
		{
			ffmpeg.CloseEncode();
			fo.Delete(strTempSave);
		}

		ESMLog(5,_T("Encode Fail.."));
	}
	
}
void CESMMakeViewMove::RunEncode(CString strTempPath,CString strMoviePath)
{
	CString strCmd;
	strCmd.Format(_T("\"C:\\Program Files\\ESMLab\\4DMaker\\bin\\ffmpeg.exe\""));

	CString strOpt;
	strOpt.Format(_T("-i %s"),strTempPath);
	strOpt.Append(_T(" -c copy -bsf:a aac_adtstoasc "));
	strOpt.Append(_T("\""));
	strOpt.Append(strMoviePath);
	strOpt.Append(_T("\""));

	SHELLEXECUTEINFO lpExecInfo;
	lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.lpFile = strCmd;
	lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
	lpExecInfo.hwnd = NULL;  
	lpExecInfo.lpVerb = L"open";
	lpExecInfo.lpParameters = strOpt;
	lpExecInfo.lpDirectory = NULL;
	lpExecInfo.nShow = SW_HIDE; // hide shell during execution
	lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;
	ShellExecuteEx(&lpExecInfo);

	// wait until the process is finished
	if (lpExecInfo.hProcess != NULL)
	{
		::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		::CloseHandle(lpExecInfo.hProcess);
	}
	lpExecInfo.lpFile = _T("exit");
	lpExecInfo.lpVerb = L"close";
	lpExecInfo.lpParameters = NULL;

	if(TerminateProcess(lpExecInfo.hProcess,0))
	{
		lpExecInfo.hProcess = 0;
	}
}