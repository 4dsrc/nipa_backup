////////////////////////////////////////////////////////////////////////////////
//
//	ESMTimeLineObjectSelector.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-30
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMTimeLineObjectSelector.h"
#include "resource.h"

#include "DSCViewDefine.h"
#include "DSCItem.h"
#include "ESMCtrl.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMTimeLineObject
IMPLEMENT_DYNCREATE(CESMTimeLineObjectSelector, CWnd)
BEGIN_MESSAGE_MAP(CESMTimeLineObjectSelector, CWnd)	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()


CESMTimeLineObjectSelector::CESMTimeLineObjectSelector()
{
	//--	
	m_bChangePos[0] = FALSE;
	m_bChangePos[1] = FALSE;
}

CESMTimeLineObjectSelector::CESMTimeLineObjectSelector(const CObArray& ar, int nStartTime, int nEndTime, int nNextTime )
	: CESMTimeLineObject(ar, nStartTime, nEndTime, nNextTime )
{
	m_bChangePos[0] = FALSE;
	m_bChangePos[1] = FALSE;
}

BOOL CESMTimeLineObjectSelector::DrawObject()
{
	if(!m_hWnd) return FALSE;

	CDC *pDC = GetDC();
	if(!pDC) return FALSE;

	CRect rect;
	GetClientRect(&rect);
	if(!rect.Width() || !rect.Height())
	{
		ReleaseDC(pDC);
		return FALSE;
	}

	CDC		memDC;
	CBitmap bmpBitmap;
	memDC.CreateCompatibleDC(pDC);
	bmpBitmap.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height()); 

	memDC.SelectObject(&bmpBitmap);	
	memDC.PatBlt(0, 0, rect.Width(), rect.Height(), SRCCOPY);


	//-- 2013-09-27 hongsu@esmlab.com
	//-- Draw Timeline Object
	CPoint ptS, ptE;

	if(!m_bChangePos[0])
	{
		ptS.x = rect.left;
		ptS.x += DSC_ROW_H/2;
		ptE.x = rect.right;
		ptE.x -= DSC_ROW_H/2;
	}
	else
	{
		ptS.x = rect.right;
		ptS.x -= DSC_ROW_H/2;
		ptE.x = rect.left;
		ptE.x += DSC_ROW_H/2;
	}

	if(!m_bChangePos[1])
	{
		ptS.y = rect.top;
		ptS.y +=  DSC_ROW_H/2;
		ptE.y = rect.bottom;
		ptE.y -=  DSC_ROW_H/2;
	}
	else
	{
		ptS.y = rect.bottom;
		ptS.y -=  DSC_ROW_H/2;
		ptE.y = rect.top;
		ptE.y +=  DSC_ROW_H/2;
	}

	CPen pen;
	if( (ptS.x == ptE.x) || (ptS.y == ptE.y))
		pen.CreatePen(PS_SOLID,  DSC_ROW_H/2, COLOR_GREEN );
	else
		pen.CreatePen(PS_SOLID,  DSC_ROW_H/2, COLOR_YELLOW );	

	memDC.SelectObject(&pen);

	//-- Draw Line
	//memDC.MoveTo(ptS.x, ptS.y);
	//memDC.LineTo(ptE.x, ptE.y);

	Invalidate(FALSE);

//	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	pDC->TransparentBlt(0, 0, rect.Width(), rect.Height(), &memDC,
						0, 0, rect.Width(), rect.Height(),
						RGB(0,0,0));
	//-- 2013-10-19 hongsu@esmlab.com
	//-- memory leak 
	memDC.DeleteDC();
	ReleaseDC(pDC);
	return TRUE;	
}

void CESMTimeLineObjectSelector::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CESMTimeLineObject::OnLButtonDown(nFlags, point);
	return;
}

void CESMTimeLineObjectSelector::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CESMTimeLineObject::OnLButtonUp(nFlags, point);
	return;
}

void CESMTimeLineObjectSelector::OnMouseMove(UINT nFlags, CPoint point) 
{
	CESMTimeLineObject::OnMouseMove(nFlags, point);
	return;
}

void CESMTimeLineObjectSelector::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CESMTimeLineObject::OnRButtonDown(nFlags, point);
	//-- 2013-09-30 hongsu@esmlab.com
	//-- Release TimeLine Object
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_REMOVE_OBJ_SELECTOR;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd() , WM_ESM, (WPARAM)WM_ESM_FRAME, (LPARAM)pMsg);
	return;
}
