////////////////////////////////////////////////////////////////////////////////
//
//	ESMTimeLineObject.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-09-10
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMTimeLineObjectEditor.h"
#include "resource.h"

#include "DSCViewDefine.h"
#include "DSCItem.h"
#include "ESMCtrl.h"
#include "ESMTimeLineObject.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CESMTimeLineObject
IMPLEMENT_DYNCREATE(CESMTimeLineObject, CWnd)
BEGIN_MESSAGE_MAP(CESMTimeLineObject, CWnd)	
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


CESMTimeLineObject::CESMTimeLineObject()
{
	m_pFont = NULL;

	m_nZoom[0]	= 0;
	m_nZoom[1]	= 0;	

	m_nTime[0]	= 0;
	m_nTime[1]	= 0;	
	m_nTime[2]	= movie_next_frame_time;
	m_nKzoneIdx = 0;
	m_bDraging	= FALSE;

	m_bClose	= FALSE;
	m_hMakeObjMovie	= NULL;
	m_hMake3DMovie	= NULL;
	//CMiLRe 20151022 Template Group 단위로 삭제
	m_mGroupNo = 0;
	m_hMainWnd = ESMGetMainWnd();

//	m_bDrawTime = TRUE;
	m_bProcessMode = FALSE;
	m_strMakePC = _T("");
	m_nFrameCnt = 0;
	m_strFile = _T("");

	m_nStepFrame = 1;

}

//CMiLRe 20151022 Template Group 단위로 삭제
CESMTimeLineObject::CESMTimeLineObject(const CObArray& ar, int nStartTime, int nEndTime, int nNextTime, int nGroupNo, int nStepFrame)
{
	m_pFont		= NULL;
	m_nTime[0]	= nStartTime;
	m_nTime[1]	= nEndTime;
	
	if((nNextTime == movie_next_frame_time) && (ar.GetCount() > 1))
		m_nTime[2] = ESMGetValue(ESM_VALUE_ADJ_FLOW_T);
	else
		m_nTime[2] = nNextTime;

	m_arDSCinObj.Copy(ar);

	m_bDraging	= FALSE;

	m_bClose	= FALSE;
	m_hMakeObjMovie	= NULL;
	m_hMake3DMovie	= NULL;
	//CMiLRe 20151022 Template Group 단위로 삭제
	m_mGroupNo = nGroupNo;
	m_hMainWnd = ESMGetMainWnd();

	//m_bDrawTime = TRUE;
	m_bProcessMode = FALSE;
	m_strMakePC = _T("");
	m_nFrameCnt = 0;
	m_strFile = _T("");

	m_nStepFrame = nStepFrame;

}

CESMTimeLineObject::~CESMTimeLineObject()
{
	if( m_pFont )
	{
		delete m_pFont;
		m_pFont = NULL;
	}
	m_arDSCinObj.RemoveAll();

	//-- 2014-07-22 hongsu@esmlab.com
	//-- Remove All Object Frame 
	RemoveAllObjectFrame();
}


int CESMTimeLineObject::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd ::OnCreate(lpCreateStruct) == -1)
		return -1;
	return 0;
}

BOOL CESMTimeLineObject::OnEraseBkgnd(CDC* pDC) 
{
	return TRUE;
}

//------------------------------------------------------------------------------
//! @function	sample function
//! @brief				
//! @date		2013-09-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMTimeLineObject::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
	DrawObject();
} 

void CESMTimeLineObject::Copy(CESMTimeLineObject* pSrc)
{
	m_nTime[0]			= pSrc->GetStartTime();
	m_nTime[1]			= pSrc->GetEndTime();
	m_nTime[2]			= pSrc->GetNextTime();

	m_nZoom[0]			= 100;
	m_nZoom[1]			= 100;

	m_nStepFrame		= pSrc->GetStepFrame();
	//-- Add List
	m_arDSCinObj.Copy(pSrc->m_arDSCinObj);
}


//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-09-27
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision	2013-10-11	Ryumin	Drag&Drop 처리 이동	
//------------------------------------------------------------------------------
void CESMTimeLineObject::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CWnd::OnLButtonDown(nFlags, point);
/*	
	//  2013-10-11 Ryumin	Drag&Drop 처리 이동(OnLButtonDown >> OnMouseMove)
	CESMTimeLineObjectEditor* pObj = new CESMTimeLineObjectEditor();

	//-- 2013-09-30 hongsu@esmlab.com	
	pObj->Copy(this);

	RECT rClient;
	GetClientRect(&rClient);

	m_DropSource.StartDragging((DWORD)pObj, &rClient, &point);
*/
	return;
}

/************************************************************************
 * @method:		OnLButtonUp [CESMTimeLineObject::OnLButtonUp]
 * @function:	
 * @date:		2013-10-11
 * @owner		Ryumin (ryumin@esmlab.com)
 * @param:		UINT nFlags
 * @param:		CPoint point
 * @return:		void
 */
void CESMTimeLineObject::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_bDraging = FALSE;
}

/************************************************************************
 * @method:		OnMouseMove [CESMTimeLineObject::OnMouseMove]
 * @function:	Drag & Drop
 * @date:		2013-10-11
 * @owner		Ryumin (ryumin@esmlab.com)
 * @param:		UINT nFlags
 * @param:		CPoint point
 * @return:		void
 */
void CESMTimeLineObject::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (nFlags & MK_LBUTTON)
	{
		m_bDraging = TRUE;
// 		CESMTimeLineObjectEditor* pObj = new CESMTimeLineObjectEditor();
// 
// 		//-- 2013-09-30 hongsu@esmlab.com	
// 		pObj->Copy(this);

		RECT rClient;
		GetClientRect(&rClient);
		m_DropSource.StartDragging((DWORD_PTR)this, &rClient, &point);
	}
}

int CESMTimeLineObject::GetDuration()
{
	float fDuration;	
	//-- 2013-09-30 hongsu@esmlab.com
	//-- Check Same Time
	//if(m_nTime != NULL)
	{
		if(m_nTime[0] == m_nTime[1])
		{
			fDuration = (float)(GetDSCCount()) * m_nTime[2];
		}
		else
		{
			fDuration = (float)abs(m_nTime[1] - m_nTime[0]);
			fDuration /= movie_next_frame_time;
			fDuration *= m_nTime[2];
		}
		return (int)fDuration;
	}
}

void CESMTimeLineObject::SetFrameFont(CDC* pDC, int nSize, CString strFont, int nFontType)
{
	if(!m_pFont)
	{
		m_pFont = new CFont();
		m_pFont->CreateFont( nSize, 0, 0, 0, nFontType, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			FIXED_PITCH|FF_MODERN, strFont);
	}
	pDC->SelectObject(m_pFont);	
}

CString CESMTimeLineObject::GetDSCID(int nIndex)
{
	CString str;
	CDSCItem* pItem = GetDSC(nIndex);
	if(pItem)
		str = pItem->GetDeviceDSCID();
	return str;
}

int CESMTimeLineObject::GetDSCIndex(int nIndex)
{
	int nDSCIndex = 0;
	CDSCItem* pItem = GetDSC(nIndex);
	if(pItem)
		nDSCIndex = pItem->GetIndex();
	return nDSCIndex;
}

CDSCItem* CESMTimeLineObject::GetLastDSC()
{
	int nAll = GetDSCCount();
	return GetDSC(nAll-1);
}
