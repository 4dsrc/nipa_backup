////////////////////////////////////////////////////////////////////////////////
//
//	ESMNetworkExListCtrl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-10-08
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"

#include "SdiDefines.h"
#include "ESMReportCtrl.h"
#include "ESMTCPSocket.h"
#include "ESMHeaderCtrl.h"
#include <vector>

class CESMRCManager;
class CESMNetworkExDlg;

enum LISTExColumn
{
	LIST_Ex_CODE = 0,
	//LIST_Ex_COUNT,
	LIST_Ex_STATUS/* = 0*/,
	LIST_Ex_ADDRESS,
	LIST_Ex_DISK,
	/*LIST_Ex_RAMDISK,*/
	LIST_Ex_GPU,
	LIST_Ex_NETSPEED,
	LIST_Ex_DESTINATION,
	LIST_Ex_AGENT_IP,
	LIST_Ex_DSC_ID,
	LIST_Ex_TOTAL_COUNT
};

class CESMNetworkExListCtrl : public CListCtrl
{
public:
	CESMNetworkExListCtrl(void);
public:
	~CESMNetworkExListCtrl(void);
	void Clear();
	void Init(CESMNetworkExDlg* pParent);
	void Save(CESMNetworkExDlg* pParent);
	void LoadInfo(BOOL bCreate = FALSE);
	void SetStatus(int Row, CString strStatus);

private:
	BOOL m_bBKColor;
	CESMNetworkExDlg* m_pParent;
	CESMTCPSocket*	m_pRCDSocket;
	HANDLE	m_hThreadHandle;
	CRITICAL_SECTION m_CrSockket;
	CString m_strStatusInfo[NET_STATUS_INFO_CNT];

	CESMHeaderCtrl m_HeaderCtrl;

public:
	void UpdateStatus(CESMRCManager* pRCMgr);
	void UpdateDiskSize(CESMRCManager* pRCMgr);

protected:
	CString strIP;
	CString strPort;
	CListCtrl m_IPList;
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);

	void ProgramExecute(CString strIP);
	void ProgramUpdate(CString strIP);
	void ProgramTererminate(CString strIP);
	BOOL ProgramState(CString strIP);
	void SendComMessage(CString strHostIP,LONG nItem);
	void SystemShutDown(CString strIP, BOOL bMode = TRUE);
	//-- 2013-12-13 kjpark@esmlab.com
	//-- Network Program connect Check
	void ClientRunNConnect(CString strIP);	
	static unsigned WINAPI OpserveThread(LPVOID param);

	void AddIpAgent();
	void ModifyAgent();
	void DeleteAgent();

	void ServiceSockSend(CString strHostIP,int nPort, char* pBuf, int BufSize, char* pRecvBuf);
	void SendUpdateFile(CString strFileName, int nPort ,CString strIP);
	void SendAllClient(char* pbuf,int nSize);
	int recvn(SOCKET s, char *buf, int len, int flags);
	BOOL GetPowerState(CString strIP);
	
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);

	void ProcessUpdate(CString strIP);
	void SendProcessFile(CString strFileName, int nPort ,CString strIP, CString strPath);

	void GetProcState(CString strIP);
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	virtual void PreSubclassWindow();

	//180719 hjcho
	int GetGroupIdx(CString strDSCID);

	void RemoteAgent(CString strIp);
};
