#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "ESMButtonEx.h"
#include "ESMEditorEX.h"
// NetworkAgentAddDlg 대화 상자입니다.

class NetworkAgentAddDlg : public CDialog
{
	DECLARE_DYNAMIC(NetworkAgentAddDlg)

public:
	NetworkAgentAddDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~NetworkAgentAddDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_IPTABLEDLG };

protected:
	CString* m_strIP;
	int* m_nPort;
	BOOL m_bModifyMode;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CString* m_strSendIP;
	CString* m_strSourceIP;
	CString* m_strSourceDSC;

	BOOL m_bExtendMode;
public:

	void SetIP(CString *strIP);
	void SetPort(int *dPort);
	void ModifyMode(BOOL bModifyMode);

	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	int m_nDlgPort;
	CIPAddressCtrl m_nDlgIp;
	virtual BOOL OnInitDialog();

	void SetSendIP(CString *strSendIP){m_strSendIP = strSendIP;}
	void SetSourceIP(CString *strSourceIP){m_strSourceIP = strSourceIP;}
	void SetSourceDSC(CString *strSourceDSC){m_strSourceDSC = strSourceDSC;}
	CIPAddressCtrl m_ctlSendIP;
	CIPAddressCtrl m_ctlSourceIP;
	CESMEditorEX m_ctlSourceDSC;

	void SetExtendMode(BOOL bMode){m_bExtendMode = bMode;}
	CBrush m_background;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CESMButtonEx m_btnOK;
	CESMButtonEx m_btnCancel;
};