////////////////////////////////////////////////////////////////////////////////
//
//	ESMNetworkListCtrl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMNetworkListCtrl.h"
#include "ESMNetworkDlg.h"
#include "NetworkAgentAddDlg.h"
#include "ESMUtil.h"
#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CESMNetworkListCtrl::CESMNetworkListCtrl(void)
{
	m_bBKColor	= FALSE;
	m_pParent	= NULL;
	m_pRCDSocket= NULL;
	InitializeCriticalSection (&m_CrSockket);
	//m_hThreadHandle = (HANDLE) _beginthreadex(NULL, 0, OpserveThread, (void *)this, 0, NULL);

	m_strStatusInfo[NET_STATUS_UNKNOWN] = _T("Unknown");
	m_strStatusInfo[NET_STATUS_READY] = _T("Ready");
	m_strStatusInfo[NET_STATUS_CONNECTING] = _T("Connecting...");
	m_strStatusInfo[NET_STATUS_CONNECT] = _T("Connect");
	m_strStatusInfo[NET_STATUS_DISCONNECT] = _T("DisConnect");
	m_strStatusInfo[NET_STATUS_WARNNING] = _T("Warning");
	m_strStatusInfo[NET_STATUS_DOWNLOAD] = _T("DownLoad");
	m_strStatusInfo[NET_STATUS_BACKUPON] = _T("On");
	m_strStatusInfo[NET_STATUS_BACKUPOFF] = _T("Off");	

	m_bUseCamStatusColor = TRUE;	
}

CESMNetworkListCtrl::~CESMNetworkListCtrl(void)
{
	DeleteCriticalSection (&m_CrSockket);
}

BEGIN_MESSAGE_MAP(CESMNetworkListCtrl, CListCtrl)
	ON_WM_MEASUREITEM()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CESMNetworkListCtrl::OnNMCustomdraw)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CESMNetworkListCtrl::OnNMDblclk)
END_MESSAGE_MAP()

void CESMNetworkListCtrl::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	CListCtrl::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CESMNetworkListCtrl::OnDestroy()
{
	CListCtrl::OnDestroy();
}

void CESMNetworkListCtrl::Clear()
{
	int nItemCount = GetItemCount();
// 	int nColCount  = GetColumnCount();
// 	for(int i=0; i<nItemCount; i++)
// 	{
// 		for(int j=1; j<=nColCount; j++)
// 		{
// 			SetItemText(i, j, _T(""));
// 		}
// 	}
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-08
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMNetworkListCtrl::Init(CESMNetworkDlg* pParent)
{
	m_pParent = pParent;

	// SetExtendedStyle(GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	SetExtendedStyle(LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT );

	// Initialize list control with some data.
	InsertColumn(LIST_CODE, _T(""), LVCFMT_LEFT, 3);
	InsertColumn(LIST_STATUS, _T("Status"), LVCFMT_LEFT, 90);
	InsertColumn(LIST_ADDRESS, _T("IP Address"), LVCFMT_LEFT, 120);
	//InsertColumn(2, _T("Port"), LVCFMT_LEFT, 70);	
	InsertColumn(LIST_DISK, _T("Disk"), LVCFMT_LEFT, 90);	
	InsertColumn(LIST_RAMDISK, _T("RAM"), LVCFMT_LEFT, 90);	
	InsertColumn(LIST_GPU, _T("GPU"), LVCFMT_LEFT, 50);
	InsertColumn(LIST_NETSPEED, _T("NET"), LVCFMT_LEFT, 50);
	InsertColumn(LIST_BACKUPSTATUS, _T("BackUp"), LVCFMT_LEFT, 100);

	//jhhan 16-12-20
	//기능 사용 제외
	//InsertColumn(4, _T("Send Frame IP"), LVCFMT_LEFT, 100);		
}


void CESMNetworkListCtrl::LoadInfo(BOOL bCreate/* = FALSE*/)
{
	CESMRCManager* pRCMgr = NULL;
	int nAll = m_pParent->GetRCMgrCount();
	if(bCreate)
		DeleteAllItems();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pRCMgr = m_pParent->GetRCMgr(i);
		if(pRCMgr)
		{
			//InsertItem (i, m_strStatusInfo[pRCMgr->m_nStatus]);
			if(bCreate)
				InsertItem (i, _T(""));
			SetItemText(i, LIST_STATUS, m_strStatusInfo[pRCMgr->m_nStatus]);		// _T("IP Address"),		
			SetItemText(i, LIST_ADDRESS, (LPCTSTR)pRCMgr->GetIP());		// _T("IP Address"),		
 			CString strPort;
 			//strPort.Format(_T("%d"),pRCMgr->GetPort());
 			//SetItemText(i, 2, (LPCTSTR)strPort);				// _T("Port"),		

			CString strDiskSize;
			strDiskSize.Format(_T("%d/%d"),pRCMgr->m_nDiskFreeSize,pRCMgr->m_nDiskTotalSize);
			SetItemText(i, LIST_DISK, strDiskSize);

			CString strGpuUse;
			if(pRCMgr->m_bGPUUse)
				strGpuUse.Format(_T("O"));
			else
				strGpuUse.Format(_T("X"));
			SetItemText(i, LIST_GPU, strGpuUse);

			if(pRCMgr->m_bNetSpeed)
				strGpuUse.Format(_T("Gbps"));
			else
				strGpuUse.Format(_T("Mbps"));
			SetItemText(i, LIST_NETSPEED, strGpuUse);

			//wgkim 171201			
			//CString strBackupState = _T("...");
			//if(pRCMgr->m_nBackupSrcFileCount == pRCMgr->m_nBackupDstFileCount)			
			//	strBackupState.Format(_T("Pause"));
			//else
			//	strBackupState.Format(_T("%d"), pRCMgr->m_nBackupSrcFileCount);
			//
			//SetItemText(i, LIST_BACKUPSTATUS, strBackupState);			
			//pRCMgr->m_nBackupDstFileCount = pRCMgr-> m_nBackupSrcFileCount;

			//SetItemText(i, 4, pRCMgr->GetSendIP());
		}
	}
	//-- 2014-09-08 hongsu@esmlab.com
	//-- Redraw
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_NET_REDRAW;
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-14
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMNetworkListCtrl::UpdateStatus(CESMRCManager* pRCMgr)
{
	CESMRCManager* pExistRCMgr = NULL;
	int nAll = m_pParent->GetRCMgrCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExistRCMgr = m_pParent->GetRCMgr(i);
		if(pExistRCMgr == pRCMgr)
		{
//			CString strStatus;
// 			if (pRCMgr->m_bConnected)
// 				strStatus.Format(_T("connected"));
// 			else
// 				strStatus.Format(_T("disconnect"));		
// 
// 			InsertItem (i, m_strStatusInfo[pRCMgr->m_nStatus]);
			SetItemText(i, LIST_STATUS, m_strStatusInfo[pRCMgr->m_nStatus]);
			
		}
	}
}

void CESMNetworkListCtrl::UpdateDiskSize(CESMRCManager* pRCMgr)
{
	CESMRCManager* pExistRCMgr = NULL;
	int nAll = m_pParent->GetRCMgrCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExistRCMgr = m_pParent->GetRCMgr(i);
		if(pExistRCMgr == pRCMgr)
		{
			CString strDiskSize;
			if(pRCMgr->m_nDiskFreeSize == pRCMgr->m_nDiskTotalSize)
				strDiskSize = _T("-");
			else
				strDiskSize.Format(_T("%d/%d"),pRCMgr->m_nDiskFreeSize,pRCMgr->m_nDiskTotalSize);
			SetItemText(i, LIST_DISK, strDiskSize);
		}
	}

	LoadInfo();
}

void CESMNetworkListCtrl::UpdateBackupProgramProgress(CESMRCManager* pRCMgr)
{
	CESMRCManager* pExistRCMgr = NULL;
	int nAll = m_pParent->GetRCMgrCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExistRCMgr = m_pParent->GetRCMgr(i);
		if(pExistRCMgr == pRCMgr)
		{
			CString strDiskSize;
			strDiskSize.Format(_T("%d/%d"),pRCMgr->m_nBackupDstFileCount,pRCMgr->m_nBackupSrcFileCount);
			SetItemText(i, LIST_BACKUPSTATUS, strDiskSize);
		}
	}

	LoadInfo();
}

void CESMNetworkListCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	//	blablabla....
	CListCtrl::OnLButtonDown(nFlags, point);
}

/************************************************************************
 * @method:		CESMNetworkListCtrl::OnRButtonDown
 * @function:	OnRButtonDown
 * @date:		2013-10-16
 * @owner		Ryumin (ryumin@esmlab.com)
 * @param:		UINT nFlags
 * @param:		CPoint point
 * @return:		void
 */


void CESMNetworkListCtrl::OnRButtonUp(UINT nFlags, CPoint point)
{
	if(m_bUseCamStatusColor != TRUE)
		return;

	CListCtrl::OnRButtonUp(nFlags, point);
}
void CESMNetworkListCtrl::OnRButtonDown(UINT nFlags, CPoint point)
{
	if(m_bUseCamStatusColor != TRUE)
		return;

	CMenu	MenuLoader;
	CMenu*	pMenu;
	CPoint	ptMenu = point;
	ClientToScreen(&ptMenu);
	
	MenuLoader.LoadMenu(IDR_NETWORKVIEWER_MENU);
	pMenu = MenuLoader.GetSubMenu(0);

	//  2013-10-16 Ryumin
	//	Test Menu Disable
	//pTimelineMenu->EnableMenuItem(ID_NETWORK_4DMAKER_RUN, MF_DISABLED | MF_GRAYED);

	int nSelectedItem= GetNextItem( -1, LVNI_SELECTED );
	CString strIP	 = GetItemText(nSelectedItem, 2);

	//-- 2013-12-13 kjpark@esmlab.com
	//-- Network Program connect/Status Squence & Check

	// hide menu
	for (int i = 0; i < 12 ; i++)
	{
		pMenu->RemoveMenu(3, MF_BYPOSITION);
	}
	
	if( strIP == _T(""))
	{
		pMenu->EnableMenuItem(ID_NETWORK_CONNECT, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_NETWORK_DISCONNECT, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_NETWORK_4DMAKER_RUN, MF_BYCOMMAND | MF_GRAYED);	
		pMenu->EnableMenuItem(ID_NETWORK_4DMAKER_TERMINATE, MF_BYCOMMAND | MF_GRAYED);
		pMenu->EnableMenuItem(ID_NETWORK_4DMAKER_UPDATE, MF_BYCOMMAND | MF_GRAYED);
		//-- 2013-12-13 kjpark@esmlab.com
		//-- Network Program connect Check		
		pMenu->EnableMenuItem(ID_NETWORK_IP_RUN_CONNECT_ALL, MF_BYCOMMAND | MF_GRAYED);
		//pTimelineMenu->EnableMenuItem(ID_NETWORK_CAMERA_ON, MF_BYCOMMAND | MF_GRAYED);
		//pTimelineMenu->EnableMenuItem(ID_NETWORK_CAMERA_OFF, MF_BYCOMMAND | MF_GRAYED);

		pMenu->EnableMenuItem(ID_NETWORK_IP_MODIFY, MF_BYCOMMAND | MF_DISABLED);
		pMenu->EnableMenuItem(ID_NETWORK_REMOTE, MF_BYCOMMAND | MF_DISABLED);
	}
	else
	{		
		pMenu->EnableMenuItem(ID_NETWORK_IP_MODIFY, MF_BYCOMMAND | MF_ENABLED);
		pMenu->EnableMenuItem(ID_NETWORK_REMOTE, MF_BYCOMMAND | MF_ENABLED);

		int nStart = 0, nEnd = 0, nTimeData = 0; 
		nStart = GetTickCount();
		if (GetPowerState(strIP))
		{
			// camera on 비활성화 camera off 활성화
			pMenu->EnableMenuItem(ID_NETWORK_CAMERA_ON, MF_BYCOMMAND | MF_DISABLED);
			//pTimelineMenu->EnableMenuItem(ID_NETWORK_CAMERA_OFF, MF_BYCOMMAND | MF_GRAYED);
		}
		else
		{
			// camera off 비활성화 camera on 활성화
			//pTimelineMenu->EnableMenuItem(ID_NETWORK_CAMERA_ON, MF_BYCOMMAND | MF_GRAYED);
			pMenu->EnableMenuItem(ID_NETWORK_CAMERA_OFF, MF_BYCOMMAND | MF_GRAYED);
		}
		nEnd = GetTickCount();
		if( nEnd - nStart > 500 )
		{
			pMenu->EnableMenuItem(ID_NETWORK_CONNECT, MF_BYCOMMAND | MF_GRAYED);
			pMenu->EnableMenuItem(ID_NETWORK_DISCONNECT, MF_BYCOMMAND | MF_GRAYED);
			pMenu->EnableMenuItem(ID_NETWORK_4DMAKER_TERMINATE, MF_BYCOMMAND | MF_GRAYED);		
		}
		else if(ProgramState(strIP))
		{			
			pMenu->EnableMenuItem(ID_NETWORK_4DMAKER_RUN, MF_BYCOMMAND | MF_GRAYED);	
			pMenu->EnableMenuItem(ID_NETWORK_4DMAKER_UPDATE, MF_BYCOMMAND | MF_GRAYED);
		//-- 2013-12-13 kjpark@esmlab.com
		//-- Network Program connect Check
			pMenu->EnableMenuItem(ID_NETWORK_IP_RUN_CONNECT_ALL, MF_BYCOMMAND | MF_GRAYED);
			if(m_pParent->RCMgrIsConnected(strIP))
			{
				pMenu->EnableMenuItem(ID_NETWORK_DISCONNECT, MF_BYCOMMAND | MF_GRAYED);
			}
			else
			{
				pMenu->EnableMenuItem(ID_NETWORK_CONNECT, MF_BYCOMMAND | MF_GRAYED);
			}

		}
		else
		{
			pMenu->EnableMenuItem(ID_NETWORK_CONNECT, MF_BYCOMMAND | MF_GRAYED);
			pMenu->EnableMenuItem(ID_NETWORK_DISCONNECT, MF_BYCOMMAND | MF_GRAYED);
			pMenu->EnableMenuItem(ID_NETWORK_4DMAKER_TERMINATE, MF_BYCOMMAND | MF_GRAYED);						
		}
	}

	CString strStatus = GetItemText(nSelectedItem, 1);
	if( strStatus == _T("connected"))
	{
		pMenu->EnableMenuItem(ID_NETWORK_IP_MODIFY, MF_BYCOMMAND | MF_GRAYED);
	}


	LONG nItem = pMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RETURNCMD,
		ptMenu.x, ptMenu.y, this);

	switch (nItem)
	{
	case ID_NETWORK_IP_ADD:
		AddIpAgent();
		break;
	case ID_NETWORK_IP_MODIFY:
		ModifyAgent();
		break;
	case ID_NETWORK_IP_DELETE:
		DeleteAgent();
		break;
	case ID_NETWORK_CONNECT:
		m_pParent->RCMgrConnect(strIP);
		break;
	case ID_NETWORK_DISCONNECT:
		m_pParent->RCMgrDisConnect(strIP);
		break;
	case ID_NETWORK_4DMAKER_RUN:
		ProgramExecute(strIP);
		break;
	case ID_NETWORK_4DMAKER_UPDATE:
		ProgramUpdate(strIP);
		break;
	case ID_NETWORK_4DMAKER_TERMINATE:
		ProgramTererminate(strIP);
		break;
	//-- 2013-12-13 kjpark@esmlab.com
	//-- Network Program connect Check
	case ID_NETWORK_IP_RUN_CONNECT_ALL:
		ClientRunNConnect(strIP);
		break;
	case ID_NETWORK_CAMERA_ON:
	case ID_NETWORK_CAMERA_OFF:
		SendComMessage(strIP,nItem);
		break;
	case ID_NETWORK_REMOTE:
		RemoteAgent(strIP);
		break;
	default:
		break;
	}

	CListCtrl::OnRButtonDown(nFlags, point);
}

void CESMNetworkListCtrl::SendComMessage(CString strHostIP,LONG nItem)
{
	//-- 2013-12-13 kjpark@esmlab.com
	//-- Network Program connect Check
	char pRecvBuf[1024] = {0};
	ServiceSockSend(strHostIP, 20500, "0", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0 )
	{
		int nID = ESMGetIDfromIP(strHostIP);
		int dPort = 20500;
		char* pBuf = NULL;
		pBuf = new char[8];

		sprintf(pBuf, "%d", 6);

		if(nItem == ID_NETWORK_CAMERA_OFF)
		{
			//ProgramTererminate(strHostIP);
			//Sleep(1000);
			pBuf[1] = (char)0x00;
		}
		else
			pBuf[1] = (char)0x01;

		ServiceSockSend(strHostIP, dPort, pBuf, 8, pRecvBuf);


		delete[] pBuf;
		pBuf = NULL;
	}
}

void CESMNetworkListCtrl::SendAllClient(char* pBuf,int nSize)
{
	int dPort = 20500;
	int nCount = GetItemCount();
	char pRecvBuf[1024] = {0};
	for(int i =0 ;i < nCount ; i++)
	{
		CString strIP	 = GetItemText(i, 1);
		ServiceSockSend(strIP, dPort, pBuf, nSize, pRecvBuf);
	}
}

void CESMNetworkListCtrl::ServiceSockSend(CString strHostIP,int nPort, char* pBuf, int BufSize, char* pRecvBuf)
{
	char szHostIP[50];
	if(strHostIP.IsEmpty() != TRUE)
	{
		char* strId = NULL;
		strId = ESMUtil::CStringToChar(strHostIP);
		if(strId)
		{
			//memcpy(szHostIP, strId, sizeof(char) * 50);

			BOOL bMemcpy = FALSE;
			bMemcpy = ESMMemcpy(szHostIP, strId, sizeof(char) * 50);

			if( strId)
			{
				delete strId;
				strId = NULL;
			}

			if(bMemcpy == FALSE)
				return;
		}
	}
	EnterCriticalSection (&m_CrSockket);

	if(m_pRCDSocket)
	{
		delete m_pRCDSocket;
		m_pRCDSocket = NULL; 
	}

	//m_pRCDSocket = new CESMTCPSocket(CLIENTMODE);
	m_pRCDSocket = new CESMTCPSocket;
	m_pRCDSocket->Create(AF_INET, SOCK_STREAM, IPPROTO_TCP, 300);
	if(m_pRCDSocket->Connect(szHostIP, nPort, 1) > 0)
	{
		m_pRCDSocket->Send(pBuf, BufSize);
		m_pRCDSocket->Recv(pRecvBuf, 1024,1);
	}

	m_pRCDSocket->Close();
	if(m_pRCDSocket)
	{
		delete m_pRCDSocket;
		m_pRCDSocket = NULL;
	}
	LeaveCriticalSection (&m_CrSockket);
}

void CESMNetworkListCtrl::ProgramExecute(CString strIP)
{
	char pRecvBuf[1024] = {0};
	char szSendBuf[1024] = {0};
	ServiceSockSend(strIP, 20500, "0", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0 )
	{
	//-- 2015-02-16 cygil@esmlab.com
	//-- Add process name
/*
#ifdef _DEBUG
		sprintf(pSendBuf, "11");
#else
		sprintf(pSendBuf, "12");
#endif
*/
		szSendBuf[0] = '1';
#ifdef _DEBUG
		CString strDbgName = ESMGetModuleFileName();
		CString strFileName;
		strFileName.Format(_T("%s.exe"), strDbgName.Left(strDbgName.GetLength() - 5));
#else
		CString strFileName = ESMGetModuleFileName();
#endif
		char* szFile = ESMUtil::CStringToChar(strFileName);
		sprintf(&szSendBuf[1], szFile);
		if(szFile)
		{
			delete szFile;
			szFile = NULL;
		}
		int nSendLength = strlen(szSendBuf) + 2;
		ServiceSockSend(strIP, 20500, szSendBuf, nSendLength, pRecvBuf);
	}
}

void CESMNetworkListCtrl::ProgramUpdate(CString strIP)
{
	vector<CString> arrSendList;

#ifdef _4DMODEL
	arrSendList.push_back(_T("4DModelerD.exe"));
	arrSendList.push_back(_T("4DModeler.exe"));
#else
	//arrSendList.push_back(_T("4DMakerD.exe"));
	arrSendList.push_back(_T("4DMaker.exe"));
	arrSendList.push_back(_T("vosdatafhd50p"));
	arrSendList.push_back(_T("vosdatafhd25p"));
	arrSendList.push_back(_T("vosdatauhd25p"));
#endif
	
	arrSendList.push_back(_T("SdiMgrPure.dll"));
	arrSendList.push_back(_T("SdiCore.dll"));
	arrSendList.push_back(_T("ESMMovie.dll"));
	arrSendList.push_back(_T("ESMMotorizedBase.dll"));
	arrSendList.push_back(_T("procdump64.exe"));
	//arrSendList.push_back(_T("cygwin1.dll"));
	//arrSendList.push_back(_T("iperf3.exe"));
	arrSendList.push_back(_T("GH5.info"));		// config
	//arrSendList.push_back(_T("ESMMotorizedBaseD.dll"));
	//arrSendList.push_back(_T("ESMMovieD.dll"));
	//arrSendList.push_back(_T("SdiCoreD.dll"));
	//arrSendList.push_back(_T("SdiMgrPureD.dll"));
	//arrSendList.push_back(_T("ESMTranscode.exe"));
	arrSendList.push_back(_T("ProfSkin293u.dll"));		//180320 jhhan - Skin 기능 추가로 인한 다운로드 포함
	if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
		arrSendList.push_back(_T("ESMGPUProcess.exe"));

#ifdef _4DCOPY
	CString strPath, strLocal, strDest;

	if(GetModuleFileName(nullptr, strPath.GetBuffer(_MAX_PATH + 1), MAX_PATH ) != FALSE)
	{
		strPath.ReleaseBuffer( );
		strLocal = strPath.Left( strPath.ReverseFind( '\\' )+1 );
	}

	strDest.Format(_T("\\\\%s\\4DMaker\\bin"), strIP);
	CESMFileOperation fo;
	for(int i = 0;i < arrSendList.size(); i++)
	{
		CString strFile;
		strFile.Format(_T("%s%s"), strLocal, arrSendList.at(i));

		CString strDestFile;
		strDestFile.Format(_T("%s\\%s"),strDest, arrSendList.at(i));

		BOOL bRet = FALSE;
		if(fo.IsFileExist(strDestFile))
		{
			fo.Delete(strDestFile);

		}
		
		//bRet = fo.Copy(strFile, strDest);
		bRet = CopyFile(strFile, strDest, FALSE);
		if(bRet == FALSE)
		{
			ESMLog(0, _T("[%d]ProgramUpdatep[%s] : %s"), i+1, strIP, strFile);
		}
		Sleep(10);
	}
#else
	char pRecvBuf[1024] = {0};
	ServiceSockSend(strIP, 20500, "0", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0  )
	{
		for(int i = 0; i < arrSendList.size(); i++)
		{
			BOOL bRet = FALSE;
			if (arrSendList.at(i) == _T("GH5.info"))
			{
				CString strTemp = ESMGetPath(ESM_PATH_CONFIG);
				bRet = SendProcessFile(arrSendList.at(i), 20500, strIP, strTemp);
			}
			else
				bRet = SendUpdateFile(arrSendList.at(i), 20500, strIP);

			if(bRet == FALSE)
			{
				ESMLog(0, _T("[%d]ProgramUpdatep[%s] : %s"), i+1, strIP, arrSendList.at(i));
			}
			//Sleep(10);
		}
	}else
	{
		ESMLog(0, _T("ProgramUpdatep[%s] : %s"), strIP, _T("Fail"));
	}
#endif
}

void CESMNetworkListCtrl::ProgramTererminate(CString strIP)
{
	char pRecvBuf[1024] = {0};
	char szSendBuf[1024] = {0};
	ServiceSockSend(strIP, 20500, "0", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0  )
	{
		//-- 2015-02-16 cygil@esmlab.com
		//-- Add process name
		szSendBuf[0] = '2';
#ifdef _DEBUG
		CString strDbgName = ESMGetModuleFileName();
		CString strFileName;
		strFileName.Format(_T("%s.exe"), strDbgName.Left(strDbgName.GetLength() - 5));
#else
		CString strFileName = ESMGetModuleFileName();
#endif
		char* szFile = ESMUtil::CStringToChar(strFileName);
		sprintf(&szSendBuf[1], szFile);
		if(szFile)
		{
			delete szFile;
			szFile = NULL;
		}
		int nSendLength = strlen(szSendBuf) + 2;
		ServiceSockSend(strIP, 20500, szSendBuf, nSendLength, pRecvBuf);

//		ServiceSockSend(strIP, 20500, "2", 2, pRecvBuf);
	}
}

void CESMNetworkListCtrl::SystemShutDown(CString strIP, BOOL bMode )
{
	char pRecvBuf[1024] = {0};
	ServiceSockSend(strIP, 20500, "0", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0  )
	{
		if(bMode)
			ServiceSockSend(strIP, 20500, "80", 4, pRecvBuf);
		else
			ServiceSockSend(strIP, 20500, "81", 4, pRecvBuf);
	}
}

BOOL CESMNetworkListCtrl::GetPowerState(CString strIP)
{
	char pRecvBuf[1024] = {0};
	ServiceSockSend(strIP, 20500, "7", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0 )
		return true;
	else
		return false;
}

BOOL CESMNetworkListCtrl::ProgramState(CString strIP)
{
	char pRecvBuf[1024] = {0};
	char szSendBuf[1024] = {0};
	ServiceSockSend(strIP, 20500, "0", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0  )
	{
		//-- 2015-02-16 cygil@esmlab.com
		//-- Add process name
		szSendBuf[0] = '3';
#ifdef _DEBUG
		CString strDbgName = ESMGetModuleFileName();
		CString strFileName;
		strFileName.Format(_T("%s.exe"), strDbgName.Left(strDbgName.GetLength() - 5));
#else
		CString strFileName = ESMGetModuleFileName();
#endif
		char* szFile = ESMUtil::CStringToChar(strFileName);
		sprintf(&szSendBuf[1], szFile);
		if(szFile)
		{
			delete szFile;
			szFile = NULL;
		}
		int nSendLength = strlen(szSendBuf) + 2;
		ServiceSockSend(strIP, 20500, szSendBuf, nSendLength, pRecvBuf);

//		ServiceSockSend(strIP, 20500, "3", 2, pRecvBuf);
		if( strcmp(pRecvBuf, "1") == 0  )
			return TRUE;
		else
			return FALSE;
	}

	return FALSE;
}

//-- 2013-12-13 kjpark@esmlab.com
//-- Network Program connect Check
void CESMNetworkListCtrl::ClientRunNConnect(CString strIP)
{
	if(!ProgramState(strIP))
	{
		ProgramExecute(strIP);
		Sleep(2000);
	}
	if(m_pParent->RCMgrConnect(strIP))
	{
		SendComMessage(strIP,ID_NETWORK_CAMERA_ON);		
	}	
}

// 사용자 정의 데이터 수신 함수
int CESMNetworkListCtrl::recvn(SOCKET s, char *buf, int len, int flags)
{
	int received;
	char *ptr = buf;
	int left = len;

	while(left > 0){
		received = recv(s, ptr, left, flags);
		if(received == SOCKET_ERROR) 
			return SOCKET_ERROR;
		else if(received == 0) 
			break;
		left -= received;
		ptr += received;
	}

	return (len - left);
}

void CESMNetworkListCtrl::AddIpAgent()
{
	NetworkAgentAddDlg AgentDlg;
	CString strIp;
	int dPort = 0;
	CString strSendIp;
	AgentDlg.SetIP(&strIp);
	AgentDlg.SetPort(&dPort);
	
	/*if(ESMGetValue(ESM_VALUE_4DAP))
	{
		AgentDlg.SetSendIP(&strSendIp);
	}*/
	

	if(AgentDlg.DoModal() == IDOK)
	{
		/*if(ESMGetValue(ESM_VALUE_4DAP))
		{
			strSendIp.IsEmpty();
		}*/
		m_pParent->AddRCMgr(strIp,dPort);
		m_pParent->SaveInfo();
		LoadInfo(TRUE);
	}

}

void CESMNetworkListCtrl::DeleteAgent()
{
	//jhhan 190711 [LGB-52]
	int nContinue = IDNO;
	nContinue = MessageBoxEx(NULL, _T("Do you want delete it?"), _T("WARNING"), MB_YESNO|MB_ICONWARNING, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US));
	if(nContinue == IDYES)		
	{
		int nSelectedItem= GetNextItem( -1, LVNI_SELECTED );
		CString strIP	 = GetItemText(nSelectedItem, LIST_ADDRESS);
		m_pParent->DeleteRCMgr(strIP);
		m_pParent->SaveInfo();
		LoadInfo(TRUE);
	}
}

void CESMNetworkListCtrl::ModifyAgent()
{
	NetworkAgentAddDlg AgentDlg;

	int nSelectedItem= GetNextItem( -1, LVNI_SELECTED );
	CString strIp	 = GetItemText(nSelectedItem, LIST_ADDRESS);
	//CString strPort = GetItemText(nSelectedItem, 2);
	//int dPort = _ttoi(strPort);
	int dPort = 0;
	CString PrevIp = strIp;
	CString strSendIp;
		
	AgentDlg.SetIP(&strIp);
	AgentDlg.SetPort(&dPort);
	/*if(ESMGetValue(ESM_VALUE_4DAP))
	{
		AgentDlg.SetSendIP(&strSendIp);
	}*/
	AgentDlg.ModifyMode(TRUE);
	
	if(AgentDlg.DoModal() == IDOK)
	{
		m_pParent->ModifyRCMgr(PrevIp, strIp, dPort);
		m_pParent->SaveInfo();
		LoadInfo(TRUE);

		ESMEvent* pMsg2 = NULL;
		pMsg2 = new ESMEvent;
		pMsg2->message = WM_ESM_NET_ALLDISCONNET;
		::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg2);
	}	
}

unsigned WINAPI CESMNetworkListCtrl::OpserveThread(LPVOID param)
{
	CESMNetworkListCtrl* pListCtrl = (CESMNetworkListCtrl*)param;
	int nConnectCount = pListCtrl->GetItemCount();
//	ProgramState();
	return 0;
}


BOOL CESMNetworkListCtrl::SendUpdateFile(CString strFileName, int nPort ,CString strIP)
{
	CString strSendData;

	TCHAR strPath[MAX_PATH];
	//GetCurrentDirectory(MAX_PATH, strPath);
	GetModuleFileName(NULL, strPath, MAX_PATH);
	CString strCurfile = strPath;
	strCurfile = strCurfile.Left(strCurfile.ReverseFind('\\') + 1);
// #ifdef _DEBUG
// 	strCurfile = strCurfile + _T("\\..\\..\\bin\\x64\\Debug\\") + strFileName;
// #else
// 	strCurfile = strCurfile + _T("\\..\\..\\bin\\x64\\Release\\") + strFileName;
// #endif
	strCurfile.Append(strFileName);
	//AfxMessageBox(strCurfile);
	char buf[1024] = {0};
	char szHostIP[50];
	char* strId = ESMUtil::CStringToChar(strIP);
	memcpy(szHostIP, strId, sizeof(char) * 50);
	if( strId)
	{
		delete strId;
		strId = NULL;
	}

	EnterCriticalSection (&m_CrSockket);
	if(m_pRCDSocket)
	{
		delete m_pRCDSocket;
		m_pRCDSocket = NULL;
	}

	//m_pRCDSocket = new CESMTCPSocket(CLIENTMODE);
	m_pRCDSocket = new CESMTCPSocket();
	m_pRCDSocket->Create(AF_INET, SOCK_STREAM, IPPROTO_TCP, 1000);
	m_pRCDSocket->Connect(szHostIP, nPort, 1);

	CFile ReadFile;
	if(!ReadFile.Open(strCurfile, CFile::modeRead|CFile::shareDenyNone))
	{
		LeaveCriticalSection (&m_CrSockket);
		return FALSE;
	}

	int nFileSize = ReadFile.GetLength();
	strSendData.Format(_T("5_%s_%d"), strFileName, nFileSize);
	char pchText[MAX_PATH] = {0};
	wcstombs(pchText, (LPCTSTR)strSendData, _tcslen((LPCTSTR)strSendData));
	m_pRCDSocket->Send(pchText, strSendData.GetLength());	
	m_pRCDSocket->Recv(buf, 1024);					// 파일 받을준비 완료. message
	if( buf[0] == '0')
	{
		ReadFile.Close();
		LeaveCriticalSection (&m_CrSockket);
		return FALSE;
	}

	int nSendSize = 2048;
	int nSendCount = nFileSize / nSendSize;

	char* szData = NULL;
	szData = new char[nSendSize];
	memset(szData, 0, nSendSize);
	int nReadSize = 0;
	for( int i =0 ;i < nSendCount; i++)
	{
		nReadSize = ReadFile.Read(szData, nSendSize);
		m_pRCDSocket->Send(szData, nReadSize);
		//szData += nSendSize;
	}
	if( nFileSize % nSendSize != 0)
	{
		nReadSize = ReadFile.Read(szData, nFileSize % nSendSize);
		m_pRCDSocket->Send(szData, nFileSize % nSendSize);
	}

	m_pRCDSocket->Recv(buf, 1024);
	m_pRCDSocket->Close();
	if(m_pRCDSocket)
	{
		delete m_pRCDSocket;
		m_pRCDSocket = NULL;
	}

	ReadFile.Close();

	LeaveCriticalSection (&m_CrSockket);
	delete[] szData;
	szData = NULL;
	return TRUE;
}


void CESMNetworkListCtrl::SetStatus(int Row, CString strStatus)
{
	SetItemText(Row, LIST_STATUS, strStatus);
}

BOOL CESMNetworkListCtrl::SendProcessFile(CString strFileName, int nPort ,CString strIP, CString strPath)
{
	CString strSendData;
	CString strCurfile;
	strCurfile.Format(_T("%s\\%s"),strPath,strFileName);
	//AfxMessageBox(strCurfile);

	char buf[1024] = {0};
	char szHostIP[50];
	char* strId = ESMUtil::CStringToChar(strIP);
	memcpy(szHostIP, strId, sizeof(char) * 50);
	if( strId)
	{
		delete strId;
		strId = NULL;
	}

	EnterCriticalSection (&m_CrSockket);
	if(m_pRCDSocket)
	{
		delete m_pRCDSocket;
		m_pRCDSocket = NULL;
	}

	//m_pRCDSocket = new CESMTCPSocket(CLIENTMODE);
	m_pRCDSocket = new CESMTCPSocket();
	m_pRCDSocket->Create(AF_INET, SOCK_STREAM, IPPROTO_TCP, 1000);
	m_pRCDSocket->Connect(szHostIP, nPort, 1);


	CFile ReadFile;
	if(!ReadFile.Open(strCurfile, CFile::modeRead))
	{
		LeaveCriticalSection (&m_CrSockket);
		return FALSE;
	}

	int nFileSize = ReadFile.GetLength();
	strSendData.Format(_T("9_%s_%d_%s"), strFileName, nFileSize, strPath);	//4DMakerWatcher 수정
	char pchText[MAX_PATH] = {0};
	wcstombs(pchText, (LPCTSTR)strSendData, _tcslen((LPCTSTR)strSendData));
	m_pRCDSocket->Send(pchText, strSendData.GetLength());	
	m_pRCDSocket->Recv(buf, 1024);					// 파일 받을준비 완료. message
	if( buf[0] == '0')
	{
		ReadFile.Close();
		LeaveCriticalSection (&m_CrSockket);
		return FALSE;
	}

	int nSendSize = 2048;
	int nSendCount = nFileSize / nSendSize;

	char* szData = NULL;
	szData = new char[nSendSize];
	memset(szData, 0, nSendSize);
	int nReadSize = 0;
	for( int i =0 ;i < nSendCount; i++)
	{
		nReadSize = ReadFile.Read(szData, nSendSize);
		m_pRCDSocket->Send(szData, nReadSize);
		//szData += nSendSize;
	}
	if( nFileSize % nSendSize != 0)
	{
		nReadSize = ReadFile.Read(szData, nFileSize % nSendSize);
		m_pRCDSocket->Send(szData, nFileSize % nSendSize);
	}

	m_pRCDSocket->Recv(buf, 1024);
	m_pRCDSocket->Close();
	if(m_pRCDSocket)
	{
		delete m_pRCDSocket;
		m_pRCDSocket = NULL;
	}
	LeaveCriticalSection (&m_CrSockket);
	delete[] szData;
	szData = NULL;
	return TRUE;
}


void CESMNetworkListCtrl::ProcessUpdate(CString strIP)
{
	char pRecvBuf[1024] = {0};
	ServiceSockSend(strIP, 20500, "0", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0  )
	{
		vector<CString> arrSendProcess;
		//arSendList.push_back(_T("4DA -> 4DP 설정 파일"));
		/*arrSendProcess.push_back(_T("4DMakerEx.net"));
		for(int i = 0;i < arrSendProcess.size(); i++)
			SendProcessFile(arrSendProcess.at(i), 20500, strIP, ESMGetPath(ESM_PATH_CONFIG));
*/
		/*if(ESMGetValue(ESM_VALUE_RTSP))
			SendProcessFile(_T("CameraList.csv"),20500,strIP,ESMGetPath(ESM_PATH_SETUP));*/
		//vector<CString> arrSendAdj;	//network 전송
		//arrSendAdj.push_back(_T("NewestAdj.adj"));
		//for(int i = 0;i < arrSendAdj.size(); i++)
		//{
		//	//SendProcessFile(arrSendAdj.at(i), 20500, strIP, ESMGetPath(ESM_PATH_ADJUST));
		//	SendProcessFile(arrSendAdj.at(i), 20500, strIP, ESMGetPath(ESM_PATH_MOVIE_CONF));
		//}

	}
}

void CESMNetworkListCtrl::GetProcState(CString strIP)
{
	char pRecvBuf[1024] = {0};
	CString strCode;
	CString strRamDiskSize;

	//ServiceSockSend(strIP, 20500, "0", 2, pRecvBuf);
	//if( strcmp(pRecvBuf, "1") == 0  )
	//{
		ServiceSockSend(strIP, 20500, "3", 2, pRecvBuf);
		if( strcmp(pRecvBuf, "0") == 0  || strcmp(pRecvBuf, "1") == 0  || strcmp(pRecvBuf, "2") == 0  || strcmp(pRecvBuf, "3") == 0 )
		{
			//0 Off
			//1 ON
			//2 No Response 
			//3 ERR
			
			strCode.Format(_T("%s"),pRecvBuf);
			//ESMLog(5, _T("GetProcState %s:%s"),strIP,pRecvBuf);

			if(ESMGetValue(ESM_VALUE_GETRAMDISKSIZE))
			{
				ServiceSockSend(strIP, 20500, "4", 2, pRecvBuf);
				strRamDiskSize = pRecvBuf;
			}else
				strRamDiskSize.Format(_T("opt off"));
		}
		else
			strCode.Format(_T("4"));

	//}else
	//{
	//	strCode.Format(_T("4"));
	//}

	BOOL bReload = FALSE;
	int nAll = GetItemCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		SetItemText(i, LIST_RAMDISK, strRamDiskSize);

		CString strIp = GetItemText(i, LIST_ADDRESS);
		if(strIp == strIP)
		{
			CString strTemp = GetItemText(i, LIST_CODE);
			if (strTemp != strCode)
			{
				SetItemText(i, LIST_CODE, strCode);		
				bReload = TRUE;
			}
			break;
		}
	}
	if(bReload)
		Invalidate();
}


void CESMNetworkListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	LPNMLVCUSTOMDRAW pLVCD = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);

	if(pLVCD->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
		CDC* pDC = CDC::FromHandle(pNMCD->hdc);
		CRect rect(0, 0, 0, 0);
		GetClientRect(&rect);
		pDC->FillSolidRect(&rect, RGB(44, 44, 44));

		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if(pLVCD->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	{
		int nItem = static_cast<int>(pLVCD->nmcd.dwItemSpec);
		CString strTemp = GetItemText(nItem, LIST_CODE/*pLVCD->iSubItem*/);
		if(!strTemp.IsEmpty())
		{
			if (nItem%2 == 0)
				pLVCD->clrTextBk	= RGB(35,35,35);
			else
				pLVCD->clrTextBk	= RGB(40,40,40);

			if (m_bUseCamStatusColor)
			{
				if(strTemp == _T("1"))	//조건 분기 -> 컬러 변경
					pLVCD->clrText		= RGB(0,144,255); //blue
				else if(strTemp == _T("2"))
					pLVCD->clrText		= RGB(255,0,0); //red
				else if(strTemp == _T("3"))
					pLVCD->clrText		= RGB(13,177,75); //green
				else if(strTemp == _T("4"))
					pLVCD->clrText		= RGB(255,180,0); //orange
				else
					pLVCD->clrText		= RGB(255,255,255);
			}
			else
			{
				pLVCD->clrText		= RGB(255,255,255);
			}

		}
		else
		{
			//기본색상 - 검정 배경 / 화이트 텍스트
			pLVCD->clrTextBk = RGB(44,44,44);
			pLVCD->clrText	 = RGB(255,255,255);
		}
		*pResult = CDRF_DODEFAULT;
	}
	 else if ( ( CDDS_ITEMPREPAINT | CDDS_SUBITEM) == pLVCD->nmcd.dwDrawStage ) 
	 {
		 pLVCD->clrTextBk = RGB(88,88,88);
		 pLVCD->clrText	 = RGB(23,64,255);
		 *pResult = CDRF_DODEFAULT;
	}
}

int CESMNetworkListCtrl::BackupProgramState(CString strIP)
{
	char pRecvBuf[1024] = {0};
	char szSendBuf[1024] = {0};
	ServiceSockSend(strIP, 20500, "0", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0  )
	{
		//-- 2015-02-16 cygil@esmlab.com
		//-- Add process name
		szSendBuf[0] = 'c';
		CString strFileName = "GoodSync.exe";

		char* szFile = ESMUtil::CStringToChar(strFileName);
		sprintf(&szSendBuf[1], szFile);
		if(szFile)
		{
			delete szFile;
			szFile = NULL;
		}
		int nSendLength = strlen(szSendBuf) + 2;
		ServiceSockSend(strIP, 20500, szSendBuf, nSendLength, pRecvBuf);

		//		ServiceSockSend(strIP, 20500, "3", 2, pRecvBuf);
		if( strcmp(pRecvBuf, "1") == 0  )
			return 1;
		else if( strcmp(pRecvBuf, "3") == 0  )
			return 2;
		else
			return 0;
	}

	return 1;
}

void CESMNetworkListCtrl::BackupProgramExcute(CString strIP)
{
	char pRecvBuf[1024] = {0};
	char szSendBuf[1024] = {0};
	ServiceSockSend(strIP, 20500, "0", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0 )
	{
		szSendBuf[0] = 'a';
		CString strFileName = "GoodSync.exe";

		char* szFile = ESMUtil::CStringToChar(strFileName);
		sprintf(&szSendBuf[1], szFile);
		if(szFile)
		{
			delete szFile;
			szFile = NULL;
		}
		int nSendLength = strlen(szSendBuf) + 2;
		ServiceSockSend(strIP, 20500, szSendBuf, nSendLength, pRecvBuf);		
	}
}

void CESMNetworkListCtrl::BackupProgramTerminate(CString strIP)
{
	char pRecvBuf[1024] = {0};
	char szSendBuf[1024] = {0};
	ServiceSockSend(strIP, 20500, "0", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0  )
	{
		//-- 2015-02-16 cygil@esmlab.com
		//-- Add process name
		szSendBuf[0] = 'b';
		CString strFileName = "GoodSync-v10.exe";
		char* szFile = ESMUtil::CStringToChar(strFileName);
		sprintf(&szSendBuf[1], szFile);
		if(szFile)
		{
			delete szFile;
			szFile = NULL;
		}
		int nSendLength = strlen(szSendBuf) + 2;
		ServiceSockSend(strIP, 20500, szSendBuf, nSendLength, pRecvBuf);
	}
}

int CESMNetworkListCtrl::BackupProgramProgress(CString strIP)
{
	char pRecvBuf[1024] = {0};
	char szSendBuf[1024] = {0};
	ServiceSockSend(strIP, 20500, "0", 2, pRecvBuf);
	if( strcmp(pRecvBuf, "1") == 0  )
	{
		//-- 2015-02-16 cygil@esmlab.com
		//-- Add process name
		szSendBuf[0] = 'd';
		CString strFileName = "";

		char* szFile = ESMUtil::CStringToChar(strFileName);
		sprintf(&szSendBuf[1], szFile);
		if(szFile)
		{
			delete szFile;
			szFile = NULL;
		}
		int nSendLength = strlen(szSendBuf) + 2;
		ServiceSockSend(strIP, 20500, szSendBuf, nSendLength, pRecvBuf);

		return atoi(pRecvBuf);
	}

	return -1;
}

void CESMNetworkListCtrl::UpdateBackupStatus(CString strIP)
{
	CESMRCManager* pRCMgr = NULL;	
	
	int nAll = m_pParent->GetRCMgrCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pRCMgr = m_pParent->GetRCMgr(i);
		if(!strIP.CompareNoCase(pRCMgr->GetIP()))
		{					
			CString strBackupState = _T("...");
			if(pRCMgr->m_nBackupSrcFileCount == pRCMgr->m_nBackupDstFileCount)			
				strBackupState.Format(_T("%dMbyte(Pause)"), pRCMgr->m_nBackupSrcFileCount);
			else							
				strBackupState.Format(_T("%dMbyte"), pRCMgr->m_nBackupSrcFileCount);			

			SetItemText(i, LIST_BACKUPSTATUS, strBackupState);			
			pRCMgr->m_nBackupDstFileCount = pRCMgr->m_nBackupSrcFileCount;	
		}
	}

	//-- 2014-09-08 hongsu@esmlab.com
	//-- Redraw
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_NET_REDRAW;
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}


void CESMNetworkListCtrl::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
#if 0
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(m_bUseCamStatusColor != TRUE)
		return;

	if(pNMItemActivate->iItem != -1)
	{
		CString strIP = GetItemText(pNMItemActivate->iItem, 2);
		if(strIP.IsEmpty() != TRUE)
		{
			CString strParam;
			strParam.Format(_T("/v %s"),strIP);
			ESMUtil::ExecuteProcess(_T("NULL"), _T("mstsc"), strParam);
		}		
	}
#endif
	*pResult = 0;
}


void CESMNetworkListCtrl::PreSubclassWindow()
{	
	CListCtrl::PreSubclassWindow();
	m_HeaderCtrl.SubclassWindow(::GetDlgItem(m_hWnd,0));
}

void CESMNetworkListCtrl::SetUseCamStatusColor( BOOL bUse )
{
	m_bUseCamStatusColor = bUse;
}

void CESMNetworkListCtrl::RemoteAgent( CString strIp )
{
	if(!strIp.IsEmpty())
	{
		CString strParam;
		strParam.Format(_T("/v %s"),strIp);
		ESMUtil::ExecuteProcess(_T("NULL"), _T("mstsc"), strParam);
	}		
}
