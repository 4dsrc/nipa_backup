#include "stdafx.h"
#include "ESMAJANetwork.h"

CESMAJANetwork::CESMAJANetwork()
{
	m_bThreadStop = FALSE;
	m_pRCManager = NULL;
	m_bConnect	= FALSE;
	m_bAJAMaking = FALSE;
	m_bScreen = FALSE;

	m_arMsg = new CESMArray;
	m_bSendThreadRun = FALSE;
	m_bDivState		 = FALSE;
}
CESMAJANetwork::~CESMAJANetwork()
{
	m_bThreadStop = TRUE;
	if(m_arMsg)
	{
		delete m_arMsg;
		m_arMsg = NULL;
	}
	if(m_pRCManager)
	{
		delete m_pRCManager;
		m_pRCManager = NULL;
	}
}
void CESMAJANetwork::RunSendThread()
{
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE)_beginthreadex(NULL,0,_SendThread,(void*)this,0,NULL);
	CloseHandle(hSyncTime);
}
void CESMAJANetwork::ConnectAJA()
{
	BOOL bConnect = TRUE;

	if(m_bConnect == FALSE)
	{
		m_pRCManager = new CESMRCManager(ESMGetAJANetworkIP(),PORT);
		m_bConnect = m_pRCManager->ConnectToAJA(ESMGetAJANetworkIP(),PORT);
		
		if(m_bConnect)
		{
			if(m_bSendThreadRun == FALSE)
			{
				RunSendThread();
				m_bSendThreadRun = TRUE;
			}
			
			m_pRCManager->m_bAJAUsage = TRUE;
			ESMSetConnectAJANetwork(TRUE);

			ESMLog(5,_T("AJA Connect sucess"));
			
			CObArray arDSCList;
			ESMGetDSCList(&arDSCList);

			ESMEvent* pAJAMsg = new ESMEvent();
			pAJAMsg->message	= F4DC_ADJUST_SEND;
			pAJAMsg->nParam1	= arDSCList.GetSize();
			ESMSendAJANetwork(pAJAMsg);
		}
		else
			ESMLog(0,_T("AJA Connect Fail.."));
	}
	else
		ESMLog(5,_T("Already connected..."));
}
unsigned WINAPI CESMAJANetwork::_SendThread(LPVOID param)
{
	CESMAJANetwork* pNetwork = (CESMAJANetwork*)param;
	ESMEvent* pMsg = NULL;
	
	while(!pNetwork->m_bThreadStop)
	{
		Sleep(1);
		//pNetwork->m_pArrSendMsg.size();
		
		if(pNetwork->m_bConnect && pNetwork->m_pRCManager)
		{
			if(pNetwork->m_bThreadStop == FALSE)
			{
				int nAll = pNetwork->m_arMsg->GetSize();
				//int nAll = pNetwork->m_pArrSendMsg.size();
				while(nAll--)
				{
					//pMsg = pNetwork->m_pArrSendMsg.at(0);
					pMsg = (ESMEvent*)pNetwork->m_arMsg->GetAt(0);
					//ESMLog(5,_T("[%d][%d] Send Start"),pMsg->message,pMsg->nParam1);
					/*if(pMsg->message == F4DC_SEND_CPU_FILE)
					{
					char* ptemp = new char[pMsg->nParam2];
					memcpy(ptemp,(LPARAM*)pMsg->pDest,pMsg->nParam2);
					if(ptemp)
					{
					delete ptemp;
					ptemp = NULL;
					}
					}*/
					if(!pMsg)
					{
						pNetwork->m_arMsg->RemoveAt(0);
						//pNetwork->m_pArrSendMsg.erase(pNetwork->m_pArrSendMsg.begin());
						continue;
					}

					if(pNetwork->m_bConnect)
						pNetwork->m_pRCManager->SendToAJA(pMsg);

					//ESMLog(5,_T("[%d][%d] Send Finish"),pMsg->message,pMsg->nParam1);
					pNetwork->m_arMsg->RemoveAt(0);
					//pNetwork->m_pArrSendMsg.erase(pNetwork->m_pArrSendMsg.begin());
				}
			}
		}
	}
	if(pNetwork->m_arMsg)
	{
		pNetwork->m_arMsg->RemoveAll();
		delete pNetwork->m_arMsg;
		pNetwork->m_arMsg = NULL;
	}
	ESMLog(5,_T("[AJA] Send Thread Finish"));
	
	return TRUE;
}
void CESMAJANetwork::SendToAJA(ESMEvent* pMsg)
{
	if(m_bConnect && m_pRCManager)
	{
		if(!pMsg)
			return;
		if(ESMGetConnectAJANetwork())
		{
			//m_arMsg.Add((CObject*)pMsg);
			/*if(pMsg->message == F4DC_SEND_CPU_FILE)
			{
				char* ptemp = new char[pMsg->nParam2];
				memcpy(ptemp,(LPARAM*)pMsg->pDest,pMsg->nParam2);
				if(ptemp)
				{
					delete ptemp;
					ptemp = NULL;
				}
			}*/
			//m_pArrSendMsg.push_back(pMsg);
			m_arMsg->Add((CObject*)pMsg);
		}
		//m_pRCManager->SendToAJA(pMsg);
	}
}