#pragma once

#include "resource.h"
#include <WinSock2.h>
#include <Windows.h>
#include <process.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <strsafe.h>

#include "ESMRCManager.h"
#include "ESMFunc.h"
#include "ESMIndexStructure.h"
#include "ESMAJANetworkDefine.h"
#define PORT 1015

class CESMAJANetwork
{
public:
	CESMAJANetwork();
	~CESMAJANetwork();
	CESMRCManager* m_pRCManager;

	BOOL m_bConnect;
	BOOL GetTCPConnect(){return m_bConnect;}
	void SetTCPConnect(BOOL b){m_bConnect = b;}

	BOOL m_bAJAMaking;
	BOOL GetAJAMaking(){return m_bAJAMaking;}
	void SetAJAMaking(BOOL b){m_bAJAMaking = b;}

	BOOL m_bScreen;
	void SetAJAScreen(BOOL b){m_bScreen = b;}
	BOOL GetAJAScreen(){return m_bScreen;}
	void ConnectAJA();
	void SendToAJA(ESMEvent* pMsg);
	vector<ESMEvent*> m_pArrSendMsg;
	static unsigned WINAPI _SendThread(LPVOID param);
	int RecvToAja();
	BOOL m_bThreadStop;
	void RunSendThread();
	CESMArray	*m_arMsg;
	BOOL m_bSendThreadRun;
	BOOL m_bDivState;
};