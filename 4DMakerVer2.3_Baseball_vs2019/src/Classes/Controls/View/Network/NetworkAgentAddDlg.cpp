// NetworkAgentAddDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#ifdef _4DMODEL
#include "4DModeler.h"
#else
#include "4DMaker.h"
#endif
#include "NetworkAgentAddDlg.h"
#include "afxdialogex.h"
#include "ESMFunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// NetworkAgentAddDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(NetworkAgentAddDlg, CDialog)

NetworkAgentAddDlg::NetworkAgentAddDlg(CWnd* pParent /*=NULL*/)
	: CDialog(NetworkAgentAddDlg::IDD, pParent)
	, m_nDlgPort(0)
{
	m_nPort = 0;
	m_bModifyMode = FALSE;
	m_bExtendMode = FALSE;

	m_background.CreateSolidBrush(RGB(44,44,44));
}

NetworkAgentAddDlg::~NetworkAgentAddDlg()
{
}

void NetworkAgentAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDT_ADDNETVIWPORT, m_nDlgPort);
	DDX_Control(pDX, IDC_IPADDR_ADDNETVIW, m_nDlgIp);
	DDX_Control(pDX, IDC_IPADDR_ADDSENDIP, m_ctlSendIP);
	DDX_Control(pDX, IDC_IPADDR_ADDSOURCEIP, m_ctlSourceIP);
	DDX_Control(pDX, IDC_EDT_ADDSOURCEDSC, m_ctlSourceDSC);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(NetworkAgentAddDlg, CDialog)
	ON_BN_CLICKED(IDOK, &NetworkAgentAddDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &NetworkAgentAddDlg::OnBnClickedCancel)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// NetworkAgentAddDlg 메시지 처리기입니다.

void NetworkAgentAddDlg::ModifyMode(BOOL bModifyMode)
{
	m_bModifyMode = bModifyMode;
}

void NetworkAgentAddDlg::OnBnClickedOk()
{
	UpdateData();

	if(m_bExtendMode && ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
	{
		DWORD dwIP;
		if (!m_nDlgIp.IsBlank())
		{
			m_nDlgIp.GetAddress(dwIP);
			(*m_strIP).Format(_T("%d.%d.%d.%d"),FIRST_IPADDRESS(dwIP),SECOND_IPADDRESS(dwIP),THIRD_IPADDRESS(dwIP),FOURTH_IPADDRESS(dwIP)); 
		}else
		{
			//AfxMessageBox(_T("IP Address(4DP) 입력값을 확인하세요."));
			(*m_strIP) = _T("127.0.0.1");
			return;
		}
		
		*m_nPort = m_nDlgPort;

		if (!m_ctlSendIP.IsBlank())
		{
			m_ctlSendIP.GetAddress(dwIP);
			(*m_strSendIP).Format(_T("%d.%d.%d.%d"),FIRST_IPADDRESS(dwIP),SECOND_IPADDRESS(dwIP),THIRD_IPADDRESS(dwIP),FOURTH_IPADDRESS(dwIP)); 
		}else
		{
			//AfxMessageBox(_T("IP Address(Server) 입력값을 확인하세요."));
			(*m_strSendIP) = _T("127.0.0.1");
			//return;
		}
		
		if (!m_ctlSourceIP.IsBlank())
		{
			m_ctlSourceIP.GetAddress(dwIP);
			(*m_strSourceIP).Format(_T("%d.%d.%d.%d"),FIRST_IPADDRESS(dwIP),SECOND_IPADDRESS(dwIP),THIRD_IPADDRESS(dwIP),FOURTH_IPADDRESS(dwIP)); 
		}else
		{
			//AfxMessageBox(_T("IP Address(4DA) 입력값을 확인하세요."));
			(*m_strSourceIP) = _T("127.0.0.1");
			//return;
		}
		

		m_ctlSourceDSC.GetWindowText(*m_strSourceDSC);
		if(m_strSourceDSC->IsEmpty())
		{
			//AfxMessageBox(_T("DSC ID 입력값을 확인하세요."));
			//return;
		}

	}
	else
	{
		DWORD dwIP;
		if (!m_nDlgIp.IsBlank())
		{
			m_nDlgIp.GetAddress(dwIP);
			(*m_strIP).Format(_T("%d.%d.%d.%d"),FIRST_IPADDRESS(dwIP),SECOND_IPADDRESS(dwIP),THIRD_IPADDRESS(dwIP),FOURTH_IPADDRESS(dwIP)); 
		}else
		{
			AfxMessageBox(_T("IP Address(Agent) 입력값을 확인하세요."));
			return;
		}
		
		*m_nPort = m_nDlgPort;
	}

	CDialog::OnOK();
}


void NetworkAgentAddDlg::OnBnClickedCancel()
{
	CDialog::OnCancel();
}

void NetworkAgentAddDlg::SetIP(CString *strIP)
{
	m_strIP = strIP;
}

void NetworkAgentAddDlg::SetPort(int *nPort)
{
	m_nPort = nPort;
}


BOOL NetworkAgentAddDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	if(m_bExtendMode)
	{
		char pchPath[MAX_PATH] = {0};
		int nRequiredSize = (int)wcstombs(pchPath, *m_strIP, MAX_PATH); 
		char pchSendIP[MAX_PATH] = {0};
		if((*m_strSendIP).IsEmpty())
			(*m_strSendIP) = _T("127.0.0.1");
		nRequiredSize = (int)wcstombs(pchSendIP, *m_strSendIP, MAX_PATH); 
		char pchSourceIP[MAX_PATH] = {0};
		if((*m_strSourceIP).IsEmpty())
			(*m_strSourceIP) = _T("127.0.0.1");
		nRequiredSize = (int)wcstombs(pchSourceIP, *m_strSourceIP, MAX_PATH); 	

		m_ctlSendIP.EnableWindow(TRUE);
		m_ctlSourceIP.EnableWindow(TRUE);
		m_ctlSourceDSC.EnableWindow(TRUE);

		if(m_bModifyMode)
		{
			SetWindowText(_T("Processor Modify"));

			m_nDlgIp.SetAddress(htonl(inet_addr(pchPath)));
			m_nDlgPort = *m_nPort;

			m_ctlSendIP.SetAddress(htonl(inet_addr(pchSendIP)));
			m_ctlSourceIP.SetAddress(htonl(inet_addr(pchSourceIP)));
			m_ctlSourceDSC.SetWindowText(*m_strSourceDSC);			
		}
		else 
		{
			SetWindowText(_T("Processor Add"));

			m_nDlgIp.SetAddress(htonl(inet_addr("192.168.0.101")));
			m_nDlgPort = 25000;
		}
	}
	else
	{
		char pchPath[MAX_PATH] = {0};
		int nRequiredSize = (int)wcstombs(pchPath, *m_strIP, MAX_PATH); 
		
		char pchSendIP[MAX_PATH] = {0};
		/*if(ESMGetValue(ESM_VALUE_4DAP))
		{
			if((*m_strSendIP).IsEmpty())
				(*m_strSendIP) = _T("127.0.0.1");
			nRequiredSize = (int)wcstombs(pchSendIP, *m_strSendIP, MAX_PATH); 
			m_ctlSendIP.EnableWindow(TRUE);
		}*/
		

		if(m_bModifyMode)
		{
			SetWindowText(_T("Agent Modify"));

			m_nDlgIp.SetAddress(htonl(inet_addr(pchPath)));
			m_nDlgPort = *m_nPort;
			/*if(ESMGetValue(ESM_VALUE_4DAP))
			{
				m_ctlSendIP.SetAddress(htonl(inet_addr(pchSendIP)));
			}*/
		}
		else 
		{
			m_nDlgIp.SetAddress(htonl(inet_addr("192.168.0.101")));
			m_nDlgPort = 25000;
			/*if(ESMGetValue(ESM_VALUE_4DAP))
			{
				m_ctlSendIP.SetAddress(htonl(inet_addr("127.0.0.1")));
			}*/
		}

	}
	
	CRect rect;
	m_nDlgIp.GetWindowRect(&rect);
	m_nDlgIp.SetWindowPos(NULL, 0,0,rect.Width(), 24, SWP_NOMOVE);
	m_ctlSendIP.SetWindowPos(NULL, 0,0,rect.Width(), 24, SWP_NOMOVE);
	m_ctlSourceIP.SetWindowPos(NULL, 0,0,rect.Width(), 24, SWP_NOMOVE);
		
	UpdateData(FALSE);

	m_btnOK.ChangeFont(15);
	m_btnCancel.ChangeFont(15);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

HBRUSH NetworkAgentAddDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	hbr = (HBRUSH)m_background;
	switch(nCtlColor)
	{
	case CTLCOLOR_STATIC:
		{

			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			//pDC->SetBkColor(RGB(56, 56, 56));  // 글자 배경색 변경
			pDC->SetBkMode(TRANSPARENT);
			//return (HBRUSH)m_brush;
		}
		break;
	case CTLCOLOR_EDIT:
		{
			pDC->SetTextColor(RGB(255, 255, 255));// 글자색 변경
			pDC->SetBkColor(RGB(0, 0 ,0));
			//hbr = (HBRUSH)(hbr.GetSafeHandle());  
		}
		break;
	}    


	return hbr;
}