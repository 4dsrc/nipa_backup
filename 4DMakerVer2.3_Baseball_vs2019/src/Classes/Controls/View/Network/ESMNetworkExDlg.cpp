////////////////////////////////////////////////////////////////////////////////
//
//	ESMNetworkDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-04
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMNetworkExDlg.h"
#include "resource.h"
#include "MainFrm.h"
#include "SdiSingleMgr.h"
#include "ESMIni.h"
#include "ESMUtil.h"

#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//-- 2014-09-08 hongsu@esmlab.com
//-- AllAgent Status Count 
static int g_nAgentExAll	 = 0;
static int g_nAgentExStatus = 0;
static int g_nAgentExOffStatus = 0;

/////////////////////////////////////////////////////////////////////////////
// CESMNetworkExDlg dialog
CESMNetworkExDlg::CESMNetworkExDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMNetworkExDlg::IDD, pParent)	
{	
	WSADATA wsa;
	WSAStartup(MAKEWORD(2,2), &wsa);
	m_bRCMode = ESM_MODESTATE_SERVER;
	m_pRCClient = NULL;
	m_bThreadStop = FALSE;
	m_bUpdate = FALSE;
	m_bOpenClose = FALSE;
	m_nKTState = 0;
	m_pWThd = NULL;
	m_bThread = FALSE;
	m_bLocalLoad = TRUE;
	m_bProcCheck = TRUE;
	m_nTimerCount = 10;

	m_bConnect = FALSE;
	m_bConnectFlag = FALSE;
}

CESMNetworkExDlg::~CESMNetworkExDlg()
{
	m_bThread = FALSE;
	//OnKTDisconnect();

	//  2013-10-22 Ryumin
	m_bThreadStop = TRUE;
 	RemoveAllMsg();
	if(m_bRCMode == ESM_MODESTATE_CLIENT)
 		DestroyAgent();
	else
		RemoveRCMgr();
}

void CESMNetworkExDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMNetworkExDlg)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);	
	DDX_Control(pDX, IDC_NETWORK_CTRL, m_ctrlList);
}

BOOL CESMNetworkExDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg)
	{
		if(pMsg->message == WM_KEYDOWN)
		{
			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			ESMEvent* pEsmMsg = NULL;
			pEsmMsg = new ESMEvent();
			pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
			pEsmMsg->pParam = (LPARAM)pMsg;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMNetworkExDlg, CDialog)
	//{{AFX_MSG_MAP(CESMNetworkExDlg)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()	
	ON_WM_PAINT()
	ON_MESSAGE(WM_NE_RELOADLIST, OnReloadList)
	ON_COMMAND(ID_IMAGE_NET_CONNECT		, OnConnect)
	ON_COMMAND(ID_IMAGE_NET_DISCONNECT	, OnDisConnect)
	ON_COMMAND(ID_4DMAKERALLUPGRADE		, On4DMakerAllUpgrade)
	ON_COMMAND(ID_4DMAKERALLTURNON		, On4DMakerAllProgramRun)
	ON_COMMAND(ID_4DMAKERALLTURNOFF		, On4DMakerAllProgramDown)
	ON_COMMAND(ID_4DMAKERALLCAMERAON	, On4DMakerAllCameraOn)
	ON_COMMAND(ID_4DMAKERALLCAMERAOFF	, On4DMakerAllCameraOff)
	ON_COMMAND(ID_4DMAKERALL_SHUTDOWN	, On4DMakerAllShutDown)
	ON_COMMAND(ID_4DMAKERALL_TURNON		, On4DMakerAllTurnOn)
	ON_COMMAND(ID_4DMAKERALL_REBOOT		, On4DMakerAllReBoot)
	ON_COMMAND(ID_4DMAKERLISTDOWN		, On4DMakerListDown)
	ON_COMMAND(ID_4DMAKERLISTUP			, On4DMakerListUp)
	ON_COMMAND(ID_TEST_1			, test1)
	ON_COMMAND(ID_TEST_2			, test2)
	ON_COMMAND(ID_TEST_3			, test3)
	ON_COMMAND(ID_4DMAKERALLIPERF		, On4DMakerAllIperfOn)
	ON_COMMAND(ID_4DMAKERALLIPERF_RUN	, On4DMakerAllIperfRun)
	ON_WM_TIMER()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CESMNetworkExDlg message handlers

BOOL CESMNetworkExDlg::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}


	//-- SET MAIN WINDOWS HANDLE
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();		
	m_ctrlList.Init(this);

	/*CFont font;
	font.CreatePointFont(80, DSC_FONT);
	m_ctrlList.SetFont(&font);*/

	if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		SetTimer(PROC_CHECK_EX, PROC_CHECKTIMER_EX, NULL);

	return TRUE;
}



//-- 2013-12-13 kjpark@esmlab.com
//-- Network Program connect Check
void ConvterToolBarShow(CESMNetworkExDlg* pView, BOOL isShow)
{
	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_TOOLBAR_SHOW;
	pMsg->pDest		= isShow;
	pMsg->nParam3	= ESM_NETWORK_4DP;
	::SendMessage(pView->GetCESMNetworkExDlgHandle(),WM_ESM, (WPARAM)WM_ESM_NET,(LPARAM)pMsg);
}

void CESMNetworkExDlg::On4DMakerAllUpgrade()
{
	if (m_bConnectFlag)
	{
		ESMLog(5, _T("The agent is connecting."));
		return;
	}
	RunThread(PROGMGR_ALLPROGRAMUPGRAD);
}

void CESMNetworkExDlg::On4DMakerAllProgramRun()
{
	RunThread(PROGMGR_ALLPROGRAMRUN);
}

void CESMNetworkExDlg::On4DMakerAllProgramDown()
{
	m_bConnectFlag = FALSE;
	m_bThreadStop = TRUE;
	RunThread(PROGMGR_ALLPROGRAMDOWN);
}

void CESMNetworkExDlg::On4DMakerAllCameraOn()
{
	RunThread(PROGMGR_ALLCAMERAON);
}
void CESMNetworkExDlg::On4DMakerAllCameraOff()
{
	m_bConnectFlag = FALSE;
	m_bThreadStop = TRUE;
	RunThread(PROGMGR_ALLCAMERAOFF);
}

void CESMNetworkExDlg::On4DMakerAllShutDown()
{
	m_bConnectFlag = FALSE;
	CString strWOL;
	strWOL.Format(_T("%s\\bin\\%s"),ESMGetPath(ESM_PATH_HOME), _T("WakeMeOnLan.exe"));
	CESMFileOperation fo;
	if(fo.IsFileExist(strWOL) != FALSE)
	{

		strWOL.Format(_T("%s\\bin"),ESMGetPath(ESM_PATH_HOME));
		ESMUtil::ExecuteProcess(strWOL, _T("WakeMeOnLan.exe"), _T("/scan"));
		Sleep(500);
	}

	CString strFile, strMacFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_EX);
	strMacFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_MAC);
	//-- Load Config File
	CESMIni ini, iniMac;	
	if(ini.SetIniFilename (strFile) && iniMac.SetIniFilename(strMacFile))
	{
		CString strIPSection;
		CString strIP;

		CString strMacSection;

		int nIndex = 0;
		//Server or Agent 구분
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		{
			iniMac.DeleteSection(INFO_SECTION_MACADDR_EX);
			while(1)
			{
				strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);
				//strPortSection.Format(_T("%s_%d"),INFO_RC_PORT,nIndex);

				strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);
				//nPort = ini.GetInt(INFO_SECTION_RC, strPortSection	);

				if(!strIP.GetLength())
					break;

				CString strMac;
				strMac = ESMGetMACAddress(strIP);
				//strMac.Replace(_T("-"),_T(""));
				//strMac.Trim();
				//ESMUtil::WakeOnLan(strMac);

				strMacSection.Format(_T("%d"), nIndex+1);
				iniMac.WriteString(INFO_SECTION_MACADDR_EX, strMacSection, strMac);

				nIndex++;
			}			
		}

	}

	int nRet = MessageBox(_T("Are you sure you want shut down the Client PC?"),_T("Question"), MB_YESNO);
	if (nRet == IDYES)
	{
		RunThread(PROGMGR_ALLSHUTDOWN);
	}
}

void CESMNetworkExDlg::On4DMakerAllTurnOn()
{
	//AfxMessageBox(_T("Turnon Test"));

	CString strWOL;
	strWOL.Format(_T("%s\\bin\\%s"),ESMGetPath(ESM_PATH_HOME), _T("WakeMeOnLan.exe"));
	CESMFileOperation fo;
	if(fo.IsFileExist(strWOL) == FALSE)
	{
		AfxMessageBox(strWOL, MB_ICONERROR);
		return;
	}


	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_MAC);

	//-- Load Config File
	CESMIni ini;	
	if(ini.SetIniFilename (strFile))
	{
		CString strMacSection;
		CString strMac;

		int nIndex = 1;

		while(1)
		{
			strMacSection.Format(_T("%d"),nIndex);

			strMac = ini.GetString(INFO_SECTION_MACADDR_EX, strMacSection);
			strMac.Replace(_T("-"),_T(""));

			if(!strMac.GetLength())
				break;

			ESMUtil::WakeOnLan(strMac.Trim());
			nIndex++;
		}			
	}
	else 
	{
		AfxMessageBox(_T("Check 4DMaker\\config\\4DMaker.mac file"));
	}

}

void CESMNetworkExDlg::On4DMakerAllReBoot()
{
	int nRet = MessageBox(_T("Are you sure you want Re Boot the Client PC?"),_T("Question"), MB_YESNO);
	if (nRet == IDYES)
	{
		RunThread(PROGMGR_ALLREBOOOT);
	}
}

void CESMNetworkExDlg::On4DMakerListDown()
{
	CString strStatus;
	CString strIP;
	CString strPort;
	CString strUpStatus;
	CString strUpIp;
	CString strUpPort;

	POSITION pos = NULL;
	int nItemIdx  = 0;
	int nUpIdx = 0;

	pos = m_ctrlList.GetFirstSelectedItemPosition();

	if( NULL != pos )
	{
		nItemIdx = m_ctrlList.GetNextSelectedItem( pos );
		nUpIdx = nItemIdx + 1;
	}

	int nRowCount = m_ctrlList.GetItemCount();

	if(nItemIdx == nRowCount-1)
	{
		m_ctrlList.SetFocus();
		return;
	}
	else
	{
		strPort = m_ctrlList.GetItemText(nItemIdx, 2);
		strUpPort = m_ctrlList.GetItemText(nUpIdx, 2);
		strIP = m_ctrlList.GetItemText(nItemIdx, 1);
		strUpIp = m_ctrlList.GetItemText(nUpIdx, 1);
		strStatus = m_ctrlList.GetItemText(nItemIdx, 0);
		strUpStatus = m_ctrlList.GetItemText(nUpIdx, 0);

		m_ctrlList.SetItemText(nUpIdx, 2, strPort);
		m_ctrlList.SetItemText(nItemIdx, 2, strUpPort);
		m_ctrlList.SetItemText(nUpIdx, 1, strIP);
		m_ctrlList.SetItemText(nItemIdx, 1, strUpIp);
		m_ctrlList.SetItemText(nUpIdx, 0, strStatus);
		m_ctrlList.SetItemText(nItemIdx, 0, strUpStatus);

		m_ctrlList.SetItemState(nUpIdx, LVIS_SELECTED,  LVIS_SELECTED);
		m_ctrlList.SetItemState(nItemIdx, 0,  LVIS_SELECTED);
		m_ctrlList.EnsureVisible(nUpIdx, TRUE); 
		m_ctrlList.SetFocus();
	}
	CESMRCManager* pRCMgr1, *pRCMgr2;
	if( m_arRCServerList.GetCount() > nItemIdx)
	{
		pRCMgr1 =  (CESMRCManager*)m_arRCServerList.GetAt(nItemIdx);
		pRCMgr2 =  (CESMRCManager*)m_arRCServerList.GetAt(nItemIdx + 1);
		m_arRCServerList.SetAt(nItemIdx + 1, pRCMgr1);
		m_arRCServerList.SetAt(nItemIdx, pRCMgr2);
	}
	SaveInfo();
}

void CESMNetworkExDlg::On4DMakerAllIperfOn()
{
	RunThread(PROGMGR_ALLRUNIPERF);
}

void CESMNetworkExDlg::On4DMakerAllIperfRun()
{
	RunThread(PROGMGR_ALLTESTIPERF);
}

void CESMNetworkExDlg::On4DMakerListUp()
{
	CString strStatus;
	CString strIP;
	CString strPort;
	CString strDownStatus;
	CString strDownIp;
	CString strDownPort;

	POSITION pos = NULL;
	int nItemIdx  = 0;
	int nDownIdx = 0;

	pos = m_ctrlList.GetFirstSelectedItemPosition();

	if( NULL != pos )
	{
		nItemIdx = m_ctrlList.GetNextSelectedItem( pos );
		nDownIdx = nItemIdx - 1;
	}

	if(nItemIdx == 0)
	{
		m_ctrlList.SetFocus();
		return;
	}
	else
	{
		strPort = m_ctrlList.GetItemText(nItemIdx, 2);
		strDownPort = m_ctrlList.GetItemText(nDownIdx, 2);
		strIP = m_ctrlList.GetItemText(nItemIdx, 1);
		strDownIp = m_ctrlList.GetItemText(nDownIdx, 1);
		strStatus = m_ctrlList.GetItemText(nItemIdx, 0);
		strDownStatus = m_ctrlList.GetItemText(nDownIdx, 0);

		m_ctrlList.SetItemText(nDownIdx, 2, strPort);
		m_ctrlList.SetItemText(nItemIdx, 2, strDownPort);
		m_ctrlList.SetItemText(nDownIdx, 1, strIP);
		m_ctrlList.SetItemText(nItemIdx, 1, strDownIp);
		m_ctrlList.SetItemText(nDownIdx, 0, strStatus);
		m_ctrlList.SetItemText(nItemIdx, 0, strDownStatus);

		m_ctrlList.SetItemState(nDownIdx, LVIS_SELECTED,  LVIS_SELECTED);
		m_ctrlList.SetItemState(nItemIdx, 0,  LVIS_SELECTED);
		m_ctrlList.EnsureVisible(nDownIdx, TRUE);	
		m_ctrlList.SetFocus();
	}
	CESMRCManager* pRCMgr1, *pRCMgr2;
 	if( m_arRCServerList.GetCount() > nItemIdx)
	{
		pRCMgr1 =  (CESMRCManager*)m_arRCServerList.GetAt(nItemIdx);
		pRCMgr2 =  (CESMRCManager*)m_arRCServerList.GetAt(nItemIdx - 1);
		m_arRCServerList.SetAt(nItemIdx - 1, pRCMgr1);
		m_arRCServerList.SetAt(nItemIdx, pRCMgr2);
	}
	SaveInfo();
}

void CESMNetworkExDlg::RunThread(int nThreadIndex)
{
	//-- Remote Agent
	if(m_bRCMode == ESM_MODESTATE_CLIENT)
	{
		// 		if(m_pRCClient)
		// 			m_pRCClient->StartServer(m_nPort);
	}
	//-- Server
	else
	{		
		switch(nThreadIndex)
		{
		case 0:
			DoConnectionAll();	
			FileExistCheck();	
			
			break;
		case 1:
			break;
		case PROGMGR_ALLPROGRAMRUN:
			DoAllProgramRun();						
			break;
		case PROGMGR_ALLPROGRAMDOWN:
			DoDumpProcDownAll();
			Sleep(1000);
			DoAllProgramDown();
			break;
		case PROGMGR_ALLPROGRAMUPGRAD:
			DoDownloadAll();				
			break;
		case PROGMGR_ALLCAMERAON:
			DoCameraOn();			
			break;
		case PROGMGR_ALLCAMERAOFF:
			DoCameraOff();
			DoDumpProcDownAll();
			Sleep(1000);
			DoAllProgramDown();
			break;
		case PROGMGR_ALLSHUTDOWN:
			DoAllShutDown();
			break;
		case PROGMGR_PROCESSUPDATE:
			DoProcessUpdateAll();
			break;
		case PROGMGR_ALLREBOOOT:
			DoAllShutDown(FALSE);
			break;
		case PROGMGR_ALLRUNIPERF:
			DoAllIperfOn();
			break;
		case PROGMGR_ALLTESTIPERF:
			DoAllIperfTest();
			break;
		}

	}	
}

/////////////////////////////////////////////////////////////////////////////
// CESMNetworkExDlg

void CESMNetworkExDlg::OnPaint()
{	
	CDialog::OnPaint();
}

void CESMNetworkExDlg::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_NET_CONNECT	,	
		ID_IMAGE_NET_DISCONNECT	,
		ID_SEPARATOR			,
		ID_4DMAKERALLTURNON		,	
		ID_4DMAKERALLTURNOFF	,
		ID_SEPARATOR			,
		ID_4DMAKERALLUPGRADE	,
		ID_SEPARATOR			,
		ID_4DMAKERALLCAMERAON	,	
		ID_4DMAKERALLCAMERAOFF	,
		ID_SEPARATOR			,
		ID_4DMAKERLISTDOWN		,
		ID_4DMAKERLISTUP		,
		ID_SEPARATOR			,
		ID_4DMAKERALL_SHUTDOWN	,
		ID_4DMAKERALL_TURNON	,
		ID_4DMAKERALL_REBOOT	,
		ID_SEPARATOR			,
		ID_4DMAKERALLIPERF,
		ID_4DMAKERALLIPERF_RUN,
		/*ID_TEST_1,
		ID_TEST_2,
		ID_TEST_3,*/
	};

	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	
	
	m_wndToolBar.ShowWindow(SW_SHOW);

}


void CESMNetworkExDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	/*m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;*/
	m_rcClientFrame.top	= 40;
	RepositionBars(0,0xFFFF,0);
	
	if(m_ctrlList)
		m_ctrlList.MoveWindow(m_rcClientFrame);
}

void CESMNetworkExDlg::LoadInfo()
{
	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_EX);

	m_bRCMode = ESMGetValue(ESM_VALUE_NET_MODE);
	if(m_bRCMode == ESM_MODESTATE_CLIENT)
	{
		if (m_pRCClient == NULL)
		{
			m_pRCClient = new CESMRCManager();
			//m_nPort = ESMGetValue(ESM_VALUE_NET_PORT);
			/*m_pRCClient->StartServer();*/
			m_pRCClient->StartServer(TRUE);	//jhhan 171204 4DP 실시간 프레임뷰 포트 변경
			m_pRCClient->CreateThread();
		}
		//return;
	}

	//-- Load Config File
	CESMIni ini;	
	if(ini.SetIniFilename (strFile))
	{
		CString strIPSection, strPortSection;
		CString strIP, strSendIP, strSendPort;
		int nPort = 0;
		CString strSourceIP, strSourceDSC;

		int nIndex = 0;
		//Server or Agent 구분
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		{
			while(1)
			{
				strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);
				//strPortSection.Format(_T("%s_%d"),INFO_RC_PORT,nIndex);

				strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);
				//nPort = ini.GetInt(INFO_SECTION_RC, strPortSection	);

				if(!strIP.GetLength())
					break;

				strSendIP = ini.GetString(strIP, _T("SendIP"));
				strSendPort = ini.GetString(strIP, _T("SendPort"), _T("8281"));
				strSourceIP = ini.GetString(strIP, _T("SourceIP"));
				strSourceDSC = ini.GetString(strIP, _T("SourceDSC"));
				int nGroupIdx = ini.GetInt(strIP,_T("RefereeIdx"));

				AddRCMgr(strIP, nPort, strSendIP, strSendPort, strSourceIP, strSourceDSC,nGroupIdx);
				nIndex++;
			}			
		}else
		{
#if _SINGLE_LAN

			strIP = ESMUtil::GetLocalIPAddress();

			strSendIP = ini.GetString(strIP, _T("SendIP"));
			strSendPort = ini.GetString(strIP, _T("SendPort"), _T("8281"));
			strSourceIP = ini.GetString(strIP, _T("SourceIP"));
			strSourceDSC = ini.GetString(strIP, _T("SourceDSC"));

			if(strSendIP.GetLength() && strSourceIP.GetLength() && strSourceDSC.GetLength())
			{
				AddRCMgr(strIP, nPort, strSendIP, strSendPort, strSourceIP, strSourceDSC);

				//jhhan 16-11-14 자동 접속!!
				if(ESMGetGPUMakeFile())
				{
					OnKTConnet();
				}
			}

#else //_DUAL_LAN

			CStringArray ArrIp;
			ESMUtil::GetLocalIPAddressArray(ArrIp);
			if(ArrIp.GetSize() > 0)
			{
				for(int i=0; i<ArrIp.GetSize(); i++)
				{
					strIP = ArrIp.GetAt(i);
					
					strSendIP = ini.GetString(strIP, _T("SendIP"));
					if(strSendIP.IsEmpty())
					{
						//ESMLog(5, _T("4DP - LoadInfo Skip : %s"), strIP);
						continue;
					}
					else
					{
						strSendPort = ini.GetString(strIP, _T("SendPort"), _T("8281"));
						strSourceIP = ini.GetString(strIP, _T("SourceIP"));
						strSourceDSC = ini.GetString(strIP, _T("SourceDSC"));
						int nGroupIdx = ini.GetInt(strIP,_T("RefereeIdx"));

						if(strSendIP.GetLength() && strSourceIP.GetLength() && strSourceDSC.GetLength())
						{
							CString str4DA = ini.GetString(_T("4DA"), strSourceDSC);
							if(strIP == str4DA)
							{
								//4DP로컬 파일로드
								m_bLocalLoad = TRUE;
							}
							else
							{
								//4DP -> 4DA원격 파일로드
								m_bLocalLoad = FALSE;
							}
							AddRCMgr(strIP, nPort, strSendIP, strSendPort, strSourceIP, strSourceDSC,nGroupIdx);

							/*BOOL bRemote = FALSE;
							bRemote = ini.GetBoolean(strIP, _T("Remote"), FALSE);
							ESMSetRemoteSkip(bRemote);*/

							//jhhan 16-11-14 자동 접속!!
							if(ESMGetGPUMakeFile() && ESMGetRemoteSkip() != TRUE)
							{
								ESMLog(5,_T("ESMGetGPUMakeFile"));
								CESMFileOperation fo;
								CString strPath;
								strPath.Format(_T("%s"),ESMGetClientRamPath());
								fo.Delete(strPath, FALSE);
								//fo.CreateFolder(strPath);

								if(ESMGetValue(ESM_VALUE_REFEREEREAD) == TRUE)
									OnRefereeModeStart();
								/*else if(ESMGetValue(ESM_VALUE_RTSP) == TRUE)
								{
									ESMLog(5,_T("ESM_VALUE_RTSP"));
									OnRTSPModeStart();
								}*/
								else if(ESMGetValue(ESM_VALUE_RTSP) == FALSE)
									OnKTConnet();
							}
						}
						break;
					}

				}
			}

#endif
		}
	}
	else
	{	//4MakerEx.net 파일 없을시 생성
		CFile file;
		if (file.Open(strFile, CFile::modeCreate | CFile::modeReadWrite))
		{
			file.Close();
		}
	}
	
#ifdef _INI_4DP_NET_RT
	CESMFileOperation fo;
	CString strPath;
	strPath.Format(_T("%s\\RT"),ESMGetPath(ESM_PATH_HOME));
	fo.CreateFolder(strPath);

	strPath.Format(_T("%s\\RT\\RT.ini"),ESMGetPath(ESM_PATH_HOME));
	if(!fo.IsFileExist(strPath))
	{
		CFile file;
		if (file.Open(strPath, CFile::modeCreate | CFile::modeReadWrite))
		{
			file.Close();
		}
	}
#endif


	//-- 2013-10-08 hongsu@esmlab.com
	//-- Load List Control
	m_ctrlList.LoadInfo(TRUE);
	SaveInfo();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- Invalidate
	Invalidate(TRUE);
}

void CESMNetworkExDlg::SaveInfo()
{
	if(m_bRCMode == ESM_MODESTATE_CLIENT)
		return;

	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_EX);
	//-- Load Config File
	CESMIni ini;
	CESMRCManager* pRCMgr = NULL;
	if(ini.SetIniFilename (strFile))
	{
		ini.DeleteSection(INFO_SECTION_RC);
		
		CString strIPSection, strPortSection;
		CString strIP, strSendIP, strSendPort;
		int nPort = 0;
		CString strSourceIP, strSourceDSC;

		int nAll = GetRCMgrCount();
		for(int i = 0 ; i < nAll ; i ++)
		{
			pRCMgr = GetRCMgr(i);
			if(!pRCMgr)
				break;

			strIPSection.Format(_T("%s_%d"),INFO_RC_IP,i);
									
			strIP = pRCMgr->GetIP();
			
			strSendIP = pRCMgr->GetSendIP();
			strSendPort = pRCMgr->GetSendPort();
			strSourceIP = pRCMgr->GetSourceIP();
			strSourceDSC = pRCMgr->GetSourceDSC();

			ini.WriteString	(INFO_SECTION_RC, strIPSection, strIP);
			
			if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
			{
				ini.DeleteSection(strIP);

				if(strSourceIP.CompareNoCase(_T("127.0.0.1"))==0 || strSourceIP.IsEmpty())
					continue;

				int nGroupIdx = pRCMgr->GetGroupIdx();
				//ini.DeleteSection(strIP);
				ini.WriteString	(strIP, _T("SendIP")	, strSendIP);
				ini.WriteString	(strIP, _T("SendPort")	, strSendPort);
				ini.WriteString	(strIP, _T("SourceIP")	, strSourceIP);
				ini.WriteString	(strIP, _T("SourceDSC")	, strSourceDSC);
				ini.WriteInt	(strIP,	_T("RefereeIdx"), nGroupIdx);
				if(ESMGetRemoteSkip() == TRUE)
				{
					ini.WriteBoolean(strIP, _T("Remote"), TRUE);
				}else
				{
					ini.WriteBoolean(strIP, _T("Remote"), FALSE);
				}

				//4DA 저장소 -> 4DP IP
				if(!strSourceDSC.IsEmpty())
					ini.WriteString	(_T("4DA"), strSourceDSC, strIP);

				
			}
		}			
	}
}

void CESMNetworkExDlg::AddRCMgr(CString strIP, int nPort, CString strSendIP/* = _T("")*/, CString strSendPort/* = _T("")*/, CString strSourceIP/* = _T("")*/, CString strSourceDSC/* = _T("")*/,int nGroupIdx /*= -1*/)
{
	CESMRCManager* pRCMgr = NULL;
	pRCMgr = new CESMRCManager(strIP, nPort);
	pRCMgr->CreateThread();
	m_arRCServerList.Add((CObject*)pRCMgr);
	pRCMgr->SetSendIP(strSendIP);
	pRCMgr->SetSendPort(strSendPort);
	pRCMgr->SetSourceIP(strSourceIP);
	pRCMgr->SetSourceDSC(strSourceDSC);
	pRCMgr->SetGroupIdx(nGroupIdx);
	ReloadCamList();
}

void CESMNetworkExDlg::ModifyRCMgr(CString strPrevIP, CString strIP, int nPort, CString strSendIP/* = _T("")*/, CString strSourceIP/* = _T("")*/, CString strSourceDSC/* = _T("")*/,int nGroupIdx /*= -1*/)
{
	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_EX);
	//-- Load Config File
	CESMIni ini;
	

	CESMRCManager* pExistRCMgr = NULL;
	int nAll = GetRCMgrCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExistRCMgr = GetRCMgr(i);
		if(pExistRCMgr->GetIP() == strPrevIP)
		{
			pExistRCMgr->SetIP(strIP);
			
			pExistRCMgr->SetSendIP(strSendIP);
			pExistRCMgr->SetSourceIP(strSourceIP);
			pExistRCMgr->SetSourceDSC(strSourceDSC);
			pExistRCMgr->SetGroupIdx(nGroupIdx);
			ReloadCamList();

			if(ini.SetIniFilename (strFile))	//기존삭제
			{
				ini.DeleteSection(strPrevIP);
				ini.DeleteSection(_T("4DA"));
			}

			return;
		}
	}
}

void CESMNetworkExDlg::DeleteRCMgr(CString strIP)
{
	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_EX);
	//-- Load Config File
	CESMIni ini;
	

	CESMRCManager* pExistRCMgr = NULL;
	int nAll = GetRCMgrCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExistRCMgr = GetRCMgr(i);
		if(pExistRCMgr->GetIP() == strIP)
		{
			pExistRCMgr->m_bConnectStatusCheck =FALSE;
			pExistRCMgr->Disconnect();
			if( pExistRCMgr )
			{
				delete pExistRCMgr;
				pExistRCMgr = NULL;
			}
			m_arRCServerList.RemoveAt(i);
			ReloadCamList();

			if(ini.SetIniFilename (strFile))	//기존삭제
			{
				ini.DeleteSection(strIP);
				ini.DeleteSection(_T("4DA"));
			}

			return;
		}
	}
}

void CESMNetworkExDlg::ReloadCamList()
{
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_VIEW_TIMELINE_RELOAD;
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
}

/************************************************************************
* @method:		CESMNetworkExDlg::RemoveRCMgr
* @function:	RemoveRCMgr
* @date:		2013-10-21
* @owner		Ryumin (ryumin@esmlab.com)
* @return:		void
*/
void CESMNetworkExDlg::RemoveRCMgr()
{
	CESMRCManager* pRCMgr = NULL;
	int nAll = GetRCMgrCount();
	for(int nIdx = 0; nIdx < nAll ; ++nIdx)
	{
		pRCMgr = GetRCMgr(nIdx);
		if(!pRCMgr)	continue;

		delete pRCMgr;
		pRCMgr = NULL;
	}			
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-14
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMNetworkExDlg::UpdateStatus(CESMRCManager* pRCMgr)
{
	m_ctrlList.UpdateStatus(pRCMgr);
}

void CESMNetworkExDlg::UpdateDiskSize(CESMRCManager* pRCMgr)
{
	//jhhan 180731 Not use Disk size
	return;

	m_ctrlList.UpdateDiskSize(pRCMgr);
}

LRESULT CESMNetworkExDlg::OnReloadList(WPARAM wParam, LPARAM lParam)
{
	m_ctrlList.LoadInfo();
	return 0;
}

//------------------------------------------------------------------------------ 
//! @brief		OnConnect
//! @date		2013-09-30
//! @author		yongmin
//------------------------------------------------------------------------------ 
void CESMNetworkExDlg::OnConnect()
{
	m_bConnectFlag = TRUE;

	if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
	{
		RunThread(PROGMGR_PROCESSUPDATE);
	}
	
	//AfxMessageBox(_T("Check Focus!!"));
	ESMSetExecuteMode(TRUE);
	RunThread(0);
}

void CESMNetworkExDlg::DoConnectionAll()
{
	//ESMDSCRemoveAll();

	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentExAll
	g_nAgentExAll		= nAll;
	g_nAgentExStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_CONNECTING;

		if( pExist)
		{
			ThreadDataEx* pThreadData;
			pThreadData = new ThreadDataEx;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;
			pThreadData->nReTryCount = 1;
			pThreadData->nGroupIndex = nAll;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoConnecttionThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);

	int aAll = GetRCMgrCount();

	m_ConnectStatusIPList.clear();
	CESMRCManager* ppExist = NULL;
	ConnectInfo  cinfo;
	while(aAll--)
	{
		CString strIp;
		ppExist = GetRCMgr(aAll);
		strIp = ppExist->GetIP();
		CString ipStr;
		AfxExtractSubString( ipStr, strIp, 3, '.');

		int ipInt = _ttoi(ipStr);
		cinfo.m_Ip = ipInt;
		m_ConnectStatusIPList.push_back(cinfo);
	}

	SetOnConnect(TRUE);
	
}

void CESMNetworkExDlg::DoDumpProcDownAll()
{
	int nAll = GetRCMgrCount();

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);

		if(pExist)
		{
			pExist->DumpProcDown();
		}
	}
}


void CESMNetworkExDlg::FileExistCheck()
{
	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentExAll
	g_nAgentExAll		= nAll;
	g_nAgentExStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_CONNECTING;

		if( pExist)
		{
			ThreadDataEx* pThreadDataEx;
			pThreadDataEx = new ThreadDataEx;
			pThreadDataEx->pView = this;
			pThreadDataEx->pMRCMgr = pExist;
			pThreadDataEx->nReTryCount = 1;
			pThreadDataEx->nGroupIndex = nAll;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoFileExistCheckThread, (void *)pThreadDataEx, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	ESMSetWait(7000);
	m_ctrlList.LoadInfo();
}
void CESMNetworkExDlg::DoAllProgramRun()
{
	ConvterToolBarShow(this, FALSE);

	// 	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentExAll
	g_nAgentExAll		= nAll;
	g_nAgentExStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_READY;

		if( pExist)
		{
			ThreadDataEx* pThreadDataEx;
			pThreadDataEx = new ThreadDataEx;
			pThreadDataEx->pView = this;
			pThreadDataEx->pMRCMgr = pExist;
			pThreadDataEx->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoProgramRunThread, (void *)pThreadDataEx, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();

	ConvterToolBarShow(this, TRUE);

	SetOnConnect(TRUE);
}

void CESMNetworkExDlg::DoAllProgramDown()
{
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentExAll
	g_nAgentExAll		= nAll;
	g_nAgentExStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		
		pExist = GetRCMgr(nAll);
		//ESMLog(1,_T("1 Program Down %s"), pExist->GetIP());
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_DISCONNECT;

		if( pExist)
		{
			ThreadDataEx* pThreadDataEx;
			pThreadDataEx = new ThreadDataEx;
			pThreadDataEx->pView = this;
			pThreadDataEx->pMRCMgr = pExist;
			pThreadDataEx->nReTryCount = 1;
			HANDLE hHandle = NULL;
			//ESMLog(1,_T("2 Program Down %s"), pExist->GetIP());
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoProgramDownThread, (void *)pThreadDataEx, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);

	SetOnConnect(FALSE);
}

void CESMNetworkExDlg::DoDownloadAll()
{
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentExAll
	g_nAgentExAll		= nAll;
	g_nAgentExStatus  = nAll;

	//jhhhan 170413 KT Upgrade
	//ESMSetDump(FALSE);
	HWND _hWnd = NULL;
	DWORD dwPid = 0;

	dwPid = ESMUtil::GetProcessPID(_T("procdump64.exe"));
	if (dwPid != 0)
	{
		_hWnd = ESMUtil::GetWndHandle(dwPid);
		::SendMessage(_hWnd, WM_CLOSE, (WPARAM)NULL, (LPARAM)NULL);
	}

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_DOWNLOAD;

		if( pExist)
		{
			ThreadDataEx* pThreadDataEx;
			pThreadDataEx = new ThreadDataEx;
			pThreadDataEx->pView = this;
			pThreadDataEx->pMRCMgr = pExist;
			pThreadDataEx->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoDownloadThread, (void *)pThreadDataEx, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);
}

void CESMNetworkExDlg::DoCameraOn()
{
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentExAll
	g_nAgentExAll		= nAll;
	g_nAgentExStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_CONNECTING;

		if( pExist)
		{
			ThreadDataEx* pThreadDataEx;
			pThreadDataEx = new ThreadDataEx;
			pThreadDataEx->pView = this;
			pThreadDataEx->pMRCMgr = pExist;
			pThreadDataEx->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoCameraOnThread, (void *)pThreadDataEx, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);
}

void CESMNetworkExDlg::DoCameraOff()
{
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentExAll
	g_nAgentExAll		= nAll;
	g_nAgentExOffStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_DISCONNECT;

		if( pExist)
		{
			ThreadDataEx* pThreadDataEx;
			pThreadDataEx = new ThreadDataEx;
			pThreadDataEx->pView = this;
			pThreadDataEx->pMRCMgr = pExist;
			pThreadDataEx->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoCameraOffThread, (void *)pThreadDataEx, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);
}

void CESMNetworkExDlg::DoAllIperfTest()
{
	CFile file;
	CString strResultPath, strDate, strFilePath, WriteData;
	//strResultPath.Format(_T("%s\\log\\NTRESULT"),ESMGetPath(ESM_PATH_HOME));
	strResultPath.Format(_T("%s\\log"),ESMGetServerRamPath());
	CreateDirectory(strResultPath, NULL);

	//strFilePath.Format(_T("%s\\NTTest.bat"), strResultPath);

	SYSTEMTIME st;
	GetLocalTime(&st);
	int nMonth = st.wMonth;
	int nDay = st.wDay;
	int nHour = st.wHour;
	int nMin  = st.wMinute;
	int nSec = st.wSecond;
	int nMilli = st.wMilliseconds;
	strDate.Format(_T("\\%02d_%02d_%02d_%02d_%02d"), nMonth, nDay,nHour,nMin,nSec);
	strResultPath.Append(strDate);
	CreateDirectory(strResultPath, NULL);

	int nAll = GetRCMgrCount();

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		CString strIP = pExist->GetIP();
		CString strCmd,strOpt;

		CString strLog;
		strLog.Format(_T("%s\\\\%s.txt"),strResultPath, strIP);
		//strLog.Format(_T("%s\\%s.txt"),strResultPath, strIP);
		//strLog.Replace(_T("."), _T("_"));
		/*CFile file;
		CFileException e;
		if(file.Open(strLog, CFile::modeCreate, &e))
			file.Close();
		else
			ESMLog(0, _T("CFileException : %d"), e.m_cause);*/

		//strCmd.Format(_T("\"%s\\bin\\iperf3.exe\""),ESMGetPath(ESM_PATH_HOME));
		strCmd.Format(_T("%s\\bin\\iperf.bat"),ESMGetPath(ESM_PATH_HOME));
		//strOpt.Format(_T("-c %s -i 1 > %s"), strIP, strLog);
		//strOpt.Format(_T("-c %s -i 1 > %s"), strIP, strResultPath, strLog);
		strOpt.Format(_T("%s %s"), strIP, strLog);
		
		//WriteData.Append(strCmd);
		//WriteData.Append(strOpt);
		
		SHELLEXECUTEINFO lpExecInfo;
		lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
		lpExecInfo.lpFile = strCmd;
		lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		lpExecInfo.hwnd = NULL;  
		lpExecInfo.lpVerb = L"open";
		lpExecInfo.lpParameters = strOpt;
		lpExecInfo.lpDirectory = NULL;
		lpExecInfo.nShow	=SW_SHOW;
		//lpExecInfo.nShow = SW_HIDE; // hide shell during execution
		lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;

		BOOL bState = ShellExecuteEx(&lpExecInfo);

		// wait until the process is finished
		if (lpExecInfo.hProcess != NULL)
		{
			::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
			//AfxMessageBox(_T("Test End"));
			//::CloseHandle(lpExecInfo.hProcess);
		}


		/*lpExecInfo.lpFile = _T("exit");
		lpExecInfo.lpVerb = L"close";
		lpExecInfo.lpParameters = NULL;

		if(TerminateProcess(lpExecInfo.hProcess,0))
		{
			lpExecInfo.hProcess = 0;
			ESMLog(5, _T("End Network Test [%s\\%s.txt]"), strResultPath, strIP);
		}*/
	}

	/*char* pstrData = NULL;
	if(file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
	{
		
		pstrData = ESMUtil::CStringToChar(WriteData);
		file.Write(pstrData, strlen(pstrData));
		delete[] pstrData;
		pstrData = NULL;
		
		file.Close();

		CString strCmd, strOpt;
		strCmd.Format(_T("%s"),strFilePath);

		SHELLEXECUTEINFO lpExecInfo;
		lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
		lpExecInfo.lpFile = strCmd;
		lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		lpExecInfo.hwnd = NULL;  
		lpExecInfo.lpVerb = L"open";
		lpExecInfo.lpParameters = strOpt;
		lpExecInfo.lpDirectory = NULL;
		lpExecInfo.nShow	=SW_SHOW;
		//lpExecInfo.nShow = SW_HIDE; // hide shell during execution
		lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;

		BOOL bState = ShellExecuteEx(&lpExecInfo);

		// wait until the process is finished
		if (lpExecInfo.hProcess != NULL)
		{
			::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
			::CloseHandle(lpExecInfo.hProcess);
		}
	}*/
}

void CESMNetworkExDlg::DoAllIperfOn()
{
	int nAll = GetRCMgrCount();

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);

		if(pExist)
		{
			pExist->IPerfRun();
		}
	}
}

void CESMNetworkExDlg::DoAllShutDown(BOOL bMode/* = TRUE*/)
{
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentExAll
	g_nAgentExAll		= nAll;
	g_nAgentExStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);

		if( pExist)
		{
			ThreadDataEx* pThreadDataEx;
			pThreadDataEx = new ThreadDataEx;
			pThreadDataEx->pView = this;
			pThreadDataEx->pMRCMgr = pExist;
			if(bMode)
				pThreadDataEx->nReTryCount = 1;
			else
				pThreadDataEx->nReTryCount = 0;

			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoShutDownThread, (void *)pThreadDataEx, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);

	SetOnConnect(FALSE);
}

//-- 2013-12-13 kjpark@esmlab.com
unsigned WINAPI CESMNetworkExDlg::DoConnecttionThread(LPVOID param)
{	
	ThreadDataEx* pThreadDataEx = (ThreadDataEx*)param;
	CESMNetworkExDlg* pView = pThreadDataEx->pView;
	CESMRCManager* pMRCMgr = pThreadDataEx->pMRCMgr;
	int nReTryCount = pThreadDataEx->nReTryCount;
	int nGroupIndex = pThreadDataEx->nGroupIndex;
	if( pThreadDataEx)
	{
		delete pThreadDataEx;
		pThreadDataEx = NULL;
	}

	if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
	{
		//jhhan 16-11-16 - 4DMakerEx.net 파일 업데이트 대기
		int nCnt = 0;
		while(pView->GetCheckUpdate())
		{
			ESMLog(5, _T("DoConnecttionThread : pView->GetCheckUpdate / %s : [%d]"), pMRCMgr->GetIP(), nCnt++);
			Sleep(10);

			if(nCnt > 100*5)//최대 5초간 대기
				break;
		}
	}
	

	CString strIP = pMRCMgr->GetIP();
	for( int nRetryIndex =0 ;nRetryIndex < nReTryCount; nRetryIndex++)
	{
		if(pMRCMgr->m_bConnected) 
		{	
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_FRAME_CONNECTDIVECE;			
			::SendMessage(pView->GetCESMNetworkExDlgHandle(),WM_ESM, (WPARAM)WM_ESM_FRAME,(LPARAM)pMsg);
			::SendMessage(pView->m_hWnd, WM_NE_RELOADLIST, 0, 0);
			return 0;
		}
		if(!pView->m_ctrlList.ProgramState(strIP)) 
		{
			pView->m_ctrlList.ProgramExecute(strIP);
			Sleep(2500);
			ESMLog(1,_T("[Connect] Program Execute IP =  [%s]"),strIP);	
		}

		//jhhan 170926
		pMRCMgr->ConnectToAgent(FALSE);
		pView->UpdateStatus(pMRCMgr);
		ESMLog(1,_T("[Connect] 4DMaker Connected IP =  [%s]"),strIP);		
		pView->m_ctrlList.SendComMessage(strIP,ID_NETWORK_CAMERA_ON);
		ESMLog(1,_T("[Connect] Camera ON Client IP =  [%s]"),strIP);

		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_FRAME_CONNECTDIVECE;			
		::SendMessage(pView->GetCESMNetworkExDlgHandle(),WM_ESM, (WPARAM)WM_ESM_FRAME,(LPARAM)pMsg);
 		for(int i = 0 ; i< 100; i++)
 		{
			Sleep(100);
			if( pView->m_bThreadStop == TRUE)
			{
				break;
			}

			if( pMRCMgr->m_nStatus == NET_STATUS_CONNECT)
			{
				break;
			}
 		}
		if( pMRCMgr->m_nStatus == NET_STATUS_CONNECT || pMRCMgr->m_nCamConnect == 0)
			break;	

		if( nReTryCount != nRetryIndex + 1)
		{
			pView->m_ctrlList.ProgramTererminate(strIP);
			Sleep(100);
			pView->m_ctrlList.SendComMessage(strIP,ID_NETWORK_CAMERA_OFF);
			Sleep(1000);
		}
	}
	if( pMRCMgr->m_nStatus != NET_STATUS_CONNECT)
	{
		pMRCMgr = pView->GetRCMgr(strIP);
		if( pMRCMgr != NULL)
			pMRCMgr->m_nStatus = NET_STATUS_WARNNING;
	}
	::SendMessage(pView->m_hWnd, WM_NE_RELOADLIST, 0, 0);

// 	if(nGroupIndex == 0)
// 	{
// 	  	ESMEvent* pMsg = NULL;
// 	  	pMsg = new ESMEvent;
// 	  	pMsg->message = WM_ESM_VIEW_SETNEWESTADJ;
// 	  	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
// 	}

	return 0;
}

unsigned WINAPI CESMNetworkExDlg:: DoFileExistCheckThread(LPVOID param)
{
	ThreadDataEx* pThreadDataEx = (ThreadDataEx*)param;
	CESMRCManager* pMRCMgr = pThreadDataEx->pMRCMgr;
	
	//GetFileAttributes
	CString strMovie;
	CString strRecordPath;
	CString strMain;
	CString strIni = _T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info");

	CESMIni ini;
	ini.SetIniFilename(strIni);
	strMovie		=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_CLIENTRAM	);
	strRecordPath	=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_RECORD	);
	
	//Main
	//strMain.Format(_T("\\\\"+pMRCMgr->GetIP()+"\\4DMaker"));
	//Client Ram Path
	strMovie.Format(_T("\\\\"+pMRCMgr->GetIP()+strMovie));
	//Clirnt Record Path

	//if(strRecordPath.Find(_T("HOME")))
	//jhhan 16-10-06 /* != -1 */
	/*if(strRecordPath.Find(_T("HOME")) != -1)
		strRecordPath.Replace(_T("$(HOME)"), _T("\\4DMaker"));	
	else
	{
		int nPos;
		nPos = strRecordPath.Find(_T("\\"));
		strRecordPath.Delete(0,nPos);
	}
	strRecordPath.Format(_T("\\\\"+pMRCMgr->GetIP()+strRecordPath));*/

	if(GetFileAttributes(strMovie)== -1)
		ESMLog(0,_T(""+strMovie+" - No Shared Folders ----- "));
	else
	{
		CString strMoviePath;
		strMoviePath.Format(_T(""+strMovie + "\\1.txt"));
		CFile file;

		if(!(file.Open(strMoviePath, CFile::modeCreate|CFile::modeWrite)))
			ESMLog(0,_T(""+strMovie+" - No Access Folders ----- "));
		else
			ESMLog(5,_T(""+strMovie+" - Access Folders +++++ "));
		file.Close();
		
		DeleteFile(strMoviePath);
		ESMLog(5,_T(""+strMovie+" - Shared Folders +++++ "));
		
	}
	
	/*if(GetFileAttributes(strMain)== -1)
	ESMLog(0,_T(""+strMain+" - No Shared Folders ----- "));
	else
	ESMLog(5,_T(""+strMain+" - Shared Folders +++++ "));

	if(GetFileAttributes(strRecordPath)== -1)
	ESMLog(0,_T(""+strRecordPath+" - No Shared Folders ----- "));
	else
	ESMLog(5,_T(""+strRecordPath+" - Shared Folders +++++ "));*/

	return 0;
}


unsigned WINAPI CESMNetworkExDlg::DoProgramRunThread(LPVOID param)
{	
	ThreadDataEx* pThreadDataEx = (ThreadDataEx*)param;
	CESMNetworkExDlg* pView = pThreadDataEx->pView;
	CESMRCManager* pMRCMgr = pThreadDataEx->pMRCMgr;
	if(pThreadDataEx)
	{
		delete pThreadDataEx;
		pThreadDataEx = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();

		if(!pView->m_ctrlList.ProgramState(strIP)) 
		{
			pView->m_ctrlList.ProgramExecute(strIP);
			//ESMLog(1,_T("[Connect] 4DMarkerEx Execute =  [%s]"),strIP);	
			g_nAgentExStatus--;
			ESMLog(1,_T("[Connect] 4DMarkerEx Execute =  [%s] [%d/%d]"),strIP,g_nAgentExAll-g_nAgentExStatus,g_nAgentExAll);
		}
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkExDlg::DoProgramDownThread(LPVOID param)
{	
	

	ThreadDataEx* pThreadDataEx = (ThreadDataEx*)param;
	CESMNetworkExDlg* pView = pThreadDataEx->pView;
	CESMRCManager* pMRCMgr = pThreadDataEx->pMRCMgr;

	//ESMLog(1,_T("3 Program Down %s"), pMRCMgr->GetIP());
	if(pThreadDataEx)
	{
		delete pThreadDataEx;
		pThreadDataEx = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();
		//ESMLog(1,_T("4 Program Down %s"), pMRCMgr->GetIP());
		if(pView->m_ctrlList.ProgramState(strIP)) 
		{
			pView->m_ctrlList.ProgramTererminate(strIP);
			//ESMLog(1,_T("[Connect] 4DMarkerEx Terminate =  [%s]"),strIP);	
			g_nAgentExStatus--;
			ESMLog(1,_T("[Connect] 4DMarkerEx Terminate =  [%s] [%d/%d]"),strIP,g_nAgentExAll-g_nAgentExStatus,g_nAgentExAll);
		}		
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkExDlg::DoDownloadThread(LPVOID param)
{	
	ThreadDataEx* pThreadDataEx = (ThreadDataEx*)param;
	CESMNetworkExDlg* pView = pThreadDataEx->pView;
	CESMRCManager* pMRCMgr = pThreadDataEx->pMRCMgr;
	if(pThreadDataEx)
	{
		delete pThreadDataEx;
		pThreadDataEx = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();

		if(pMRCMgr->m_bConnected || pView->m_ctrlList.ProgramState(strIP)) 
			return 0;			

		pView->m_ctrlList.ProgramUpdate(strIP);
		//ESMLog(1,_T("[Connect] 4DMarkerEx Upgrade =  [%s]"),strIP);	
		g_nAgentExStatus--;
		ESMLog(1,_T("[Connect] 4DMarkerEx Upgrade =  [%s] [%d/%d]"),strIP,g_nAgentExAll-g_nAgentExStatus,g_nAgentExAll);

		//jhhan 170413 KT Upgrade
		if(g_nAgentExStatus == 0)
		{
			ESMSetDump(TRUE);
		}
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkExDlg::DoCameraOnThread(LPVOID param)
{	
	ThreadDataEx* pThreadDataEx = (ThreadDataEx*)param;
	CESMNetworkExDlg* pView = pThreadDataEx->pView;
	CESMRCManager* pMRCMgr = pThreadDataEx->pMRCMgr;
	if(pThreadDataEx)
	{
		delete pThreadDataEx;
		pThreadDataEx = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();
		pView->m_ctrlList.SendComMessage(strIP,ID_NETWORK_CAMERA_ON);
		//ESMLog(1,_T("[Connect] Carmera Turn On All =  [%s]"),strIP);	
		g_nAgentExStatus--;
		ESMLog(1,_T("[Connect] Carmera Turn On All =  [%s] [%d/%d]"),strIP,g_nAgentExAll-g_nAgentExStatus,g_nAgentExAll);
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkExDlg::DoCameraOffThread(LPVOID param)
{	
	ThreadDataEx* pThreadDataEx = (ThreadDataEx*)param;

	CESMNetworkExDlg* pView = pThreadDataEx->pView;
	CESMRCManager* pMRCMgr = pThreadDataEx->pMRCMgr;
	if( pThreadDataEx)
	{
		delete pThreadDataEx;
		pThreadDataEx = NULL;
	}
	
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();
		pView->m_ctrlList.SendComMessage(strIP,ID_NETWORK_CAMERA_OFF);
		g_nAgentExOffStatus--;
		ESMLog(1,_T("[Connect] Carmera Turn Off All =  [%s] [%d/%d]"),strIP,g_nAgentExAll-g_nAgentExOffStatus,g_nAgentExAll);
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkExDlg::DoShutDownThread(LPVOID param)
{
	ThreadDataEx* pThreadDataEx = (ThreadDataEx*)param;
	CESMNetworkExDlg* pView = pThreadDataEx->pView;
	CESMRCManager* pMRCMgr = pThreadDataEx->pMRCMgr;

	BOOL nMode = pThreadDataEx->nReTryCount;

	if(pThreadDataEx)
	{
		delete pThreadDataEx;
		pThreadDataEx = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();

		if(nMode)
		{
			pView->m_ctrlList.SystemShutDown(strIP);
			//ESMLog(1,_T("[Connect] 4DMarker Terminate =  [%s]"),strIP);	
			g_nAgentExStatus--;
			ESMLog(1,_T("[Connect] 4DMarker ShutDown =  [%s] [%d/%d]"),strIP,g_nAgentExAll-g_nAgentExStatus,g_nAgentExAll);
		}
		else
		{
			pView->m_ctrlList.SystemShutDown(strIP, FALSE);
			//ESMLog(1,_T("[Connect] 4DMarker Terminate =  [%s]"),strIP);	
			g_nAgentExStatus--;
			ESMLog(1,_T("[Connect] 4DMarker Reboot =  [%s] [%d/%d]"),strIP,g_nAgentExAll-g_nAgentExStatus,g_nAgentExAll);
		}

		//pView->m_ctrlList.SystemShutDown(strIP);
		////ESMLog(1,_T("[Connect] 4DMarker Terminate =  [%s]"),strIP);	
		//g_nAgentExStatus--;
		//ESMLog(1,_T("[Connect] 4DMarker ShutDown =  [%s] [%d/%d]"),strIP,g_nAgentExAll-g_nAgentExStatus,g_nAgentExAll);
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

void CESMNetworkExDlg::OnDisConnect()
{
	m_bConnectFlag = FALSE;

	DisConnectAll();
}

//------------------------------------------------------------------------------
//! @brief		Management RC Manager Array
//! @date		2013-10-08
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
CESMRCManager* CESMNetworkExDlg::GetRCMgr(int nIndex)
{
	return (CESMRCManager*)m_arRCServerList.GetAt(nIndex);
}

CESMRCManager* CESMNetworkExDlg::GetRCMgr(CString strIP)
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;

		if(pExist->GetIP() == strIP)
			return pExist;
	}

	return NULL;
}

CESMRCManager* CESMNetworkExDlg::GetRCMgrID(int nID)
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;

		if(pExist->GetID() == nID)
			return pExist;
	}

	return NULL;
}

BOOL CESMNetworkExDlg::RCMgrConnect(CString strIP)
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;

		if(pExist->GetIP() == strIP)
		{
			if(!pExist->m_bConnected)
				pExist->ConnectToAgent();
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CESMNetworkExDlg::RCMgrDisConnect(CString strIP)
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;

		if(pExist->GetIP() == strIP)
		{
			pExist->Disconnect();
			return TRUE;
		}
	}

	return FALSE;
}
/************************************************************************
 * @method:		CESMNetworkExDlg::DestroyAgent
 * @function:	DestroyAgent
 * @date:		2013-10-22
 * @owner		Ryumin (ryumin@esmlab.com)
 * @return:		void
 */
void CESMNetworkExDlg::DestroyAgent()
{
	if (m_pRCClient != NULL)
	{
		//  2013-10-21 Ryumin
		m_pRCClient->m_bThreadStop	= TRUE;
		//		m_pRCClient->m_bAccept		= FALSE;
		m_pRCClient->m_bConnected	= FALSE;
		m_pRCClient->Disconnect();
		m_pRCClient->RemoveAllMsg();
		if( m_pRCClient)
		{
			delete m_pRCClient;
			m_pRCClient = NULL;
		}
	}
}

void CESMNetworkExDlg::AddMsg(ESMEvent* pMsg)
{
	CESMRCManager* pRCMgr = NULL;
	ESMEvent* pCopyMsg = NULL;

	if(m_bRCMode == ESM_MODESTATE_CLIENT)
	{
		if(m_pRCClient)
			m_pRCClient->AddMsg(pMsg);
		else
		{
			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}
		}
	}
	else
	{
		if(pMsg->pDest > 0)
		{
			CDSCItem* pItem = (CDSCItem*)pMsg->pDest;
			pRCMgr = GetRCMgrID(pItem->m_nRemoteID);

			if(pRCMgr)
				pRCMgr->AddMsg(pMsg);
			else
			{
				if(pMsg)
				{
					delete pMsg;
					pMsg = NULL;
				}
			}

		}else
		{
			int nAll = GetRCMgrCount();
			while(nAll--)
			{
				pRCMgr = GetRCMgr(nAll);
				if(!pRCMgr)
					break;

				pCopyMsg = new ESMEvent;
				memcpy(pCopyMsg, pMsg, sizeof(ESMEvent));
				pRCMgr->AddMsg(pCopyMsg);
			}
			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}
		}

	}
}

void CESMNetworkExDlg::DisConnectAll()
{
	//-- Remote Agent
	m_bThreadStop = FALSE;
	if(m_bRCMode == ESM_MODESTATE_CLIENT)
	{
		if(m_pRCClient)
			m_pRCClient->Disconnect();
	}
	//-- Server
	else
	{
		//-- Get List
		int nAll = GetRCMgrCount();
		CESMRCManager* pExist = NULL;
		while(nAll--)
		{
			pExist = GetRCMgr(nAll);
			if(pExist)
			{
				//-- 2013-10-08 hongsu@esmlab.com
				//-- Connect from Local(Server) -> to Agent
				pExist->Disconnect();
			}
		}	
	}

	SetOnConnect(FALSE);
}
void CESMNetworkExDlg::RemoveAllMsg()
{
	if (m_pRCClient != NULL)
		m_pRCClient->RemoveAllMsg();

	CESMRCManager*	pRCMgr	= NULL;
	ESMEvent*		pMsg	= NULL;
	int nCnt = GetRCMgrCount();
	for (int nIdx = 0; nIdx < nCnt; ++nIdx)
	{
		pRCMgr = GetRCMgr(nIdx);
		if (!pRCMgr) continue;

		pRCMgr->RemoveAllMsg();
	}
}

void CESMNetworkExDlg::SetNetStatus(int nRemoteIp, int nStatus)
{
	CESMRCManager*	pRCMgr	= NULL;
	if( nStatus == 1)
	{
		int nCnt = GetRCMgrCount();
		for (int nIdx = 0; nIdx < nCnt; ++nIdx)
		{
			pRCMgr = GetRCMgr(nIdx);
			if(pRCMgr->GetID() == nRemoteIp)
			{
				pRCMgr->m_bConnected = TRUE;
				pRCMgr->m_nStatus = NET_STATUS_CONNECT;
			}
		}
	}
}

void CESMNetworkExDlg::GetClientDiskSize()
{
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_GET_DSIKSIZE;
	
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}

//-- 2013-12-13 kjpark@esmlab.com
//-- Network Program connect Check
BOOL CESMNetworkExDlg::RCMgrIsConnected(CString strIP)
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;

		if(pExist->GetIP() == strIP)
		{			
			if(!pExist->m_bConnected)
				return TRUE;
		}
	}

	return FALSE;
}

void CESMNetworkExDlg::CheckToAgentAlive(int ip)
{


}

void CESMNetworkExDlg::StartCheckToAgentAlive()
{

	
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;
		//pExist->m_bConnectStatusCheck = TRUE;
		pExist->ConnectStatusCheck(TRUE);
	}
	
}
void CESMNetworkExDlg::StopCheckToAgentAlive()
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;
		pExist->m_bConnectStatusCheck = FALSE;
		
	}
	ESMLog(1,_T("ConnectStatusCheck Stop"));
}

void CESMNetworkExDlg::test1()
{
	//jhhan 16-12-20
	//기능 사용 제외
	return;

	int nCount = GetRCMgrCount();
	CString strSendIP, strSendPort, strSouceIP, strSourceDSC;
	CESMRCManager* pRCMgr = NULL;

	if(nCount == 1)
	{
		pRCMgr = (CESMRCManager*)m_arRCServerList.GetAt(0);
		strSendIP = pRCMgr->GetSendIP();
		strSendPort = pRCMgr->GetSendPort();
		strSouceIP = pRCMgr->GetSourceIP();
		strSourceDSC = pRCMgr->GetSourceDSC();
		SetSourceIP(strSouceIP);
		SetSourceDSC(strSourceDSC);

		int nPort = _ttoi(strSendPort)/*8281*/;
		BOOL bRes = m_pRCClient->ConnectToKTServer(strSendIP, nPort);
		if(bRes)
			m_pRCClient->SetKTConnected(TRUE);
	}else
		return;
	
#if _TEST
	KTSendFrame();
#else
	if(m_pWThd == NULL)
	{
		m_bThread = TRUE;
		m_pWThd = AfxBeginThread(_ThreadKTSend, (LPVOID)this);
	}
#endif
	
	

#if 0
	if(ESMGetValue(ESM_VALUE_NET_MODE) != ESM_MODESTATE_CLIENT)
	{
		int nCount = GetRCMgrCount();
		CString strSendIP;
		CESMRCManager* pRCMgr = NULL;
		
		while(nCount--)
		{
			pRCMgr = (CESMRCManager*)m_arRCServerList.GetAt(nCount);
			strSendIP = pRCMgr->GetSendIP();
			if(strSendIP.GetLength())
				pRCMgr->StartSendFrame();
		}
	}
	else
	{
		int nPort = 8281;
		CString SetIP;
		SetIP.Format(_T("192.168.0.82"));

		BOOL bRes = m_pRCClient->ConnectToKTServer(SetIP, nPort);
	}
#endif
}

void CESMNetworkExDlg::test2()
{
	//jhhan 16-12-20
	//기능 사용 제외
	return;

	if(ESMGetValue(ESM_VALUE_NET_MODE) != ESM_MODESTATE_CLIENT)
	{
		int nCount = GetRCMgrCount();
		CString strSendIP;
		CESMRCManager* pRCMgr = NULL;

		while(nCount--)
		{
			pRCMgr = (CESMRCManager*)m_arRCServerList.GetAt(nCount);
			strSendIP = pRCMgr->GetSendIP();
			if(strSendIP.GetLength())
				pRCMgr->StopSendFrame();
		}
	}
	else
	{
		m_bThread = FALSE;
		m_pRCClient->DisConnectToKTServer();
		m_pRCClient->SetKTConnected(FALSE);
	}
}

void CESMNetworkExDlg::test3()
{
	//jhhan 16-12-20
	//기능 사용 제외
	return;

	KSSHeader data;
	data.command[0] = 0x6f;
	data.command[1] = 0x70;
	data.command[2] = 0x65;
	data.command[3] = 0x6e;
	m_pRCClient->SendFrameData(&data);
}

void CESMNetworkExDlg::DoProcessUpdateAll()
{
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentExAll
	g_nAgentExAll	 = nAll;
	g_nAgentExStatus  = nAll;

	//jhhan 16-11-16 - 4DMakerEx.net 파일 업데이트 대기 Flag
	SetCheckUpdate(TRUE);

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_DOWNLOAD;

		if( pExist)
		{
			ThreadDataEx* pThreadDataEx;
			pThreadDataEx = new ThreadDataEx;
			pThreadDataEx->pView = this;
			pThreadDataEx->pMRCMgr = pExist;
			pThreadDataEx->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoProcessUpdateThread, (void *)pThreadDataEx, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	

	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);
}

unsigned WINAPI CESMNetworkExDlg::DoProcessUpdateThread(LPVOID param)
{	
	ThreadDataEx* pThreadDataEx = (ThreadDataEx*)param;
	CESMNetworkExDlg* pView = pThreadDataEx->pView;
	CESMRCManager* pMRCMgr = pThreadDataEx->pMRCMgr;
	if(pThreadDataEx)
	{
		delete pThreadDataEx;
		pThreadDataEx = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();

		/*if(pMRCMgr->m_bConnected || pView->m_ctrlList.ProgramState(strIP)) 
		return 0;*/			

		pView->m_ctrlList.ProcessUpdate(strIP);
		//ESMLog(1,_T("[Connect] 4DMarkerEx Upgrade =  [%s]"),strIP);	
		g_nAgentExStatus--;
		ESMLog(1,_T("[Connect] 4DMarkerEx Upgrade =  [%s] [%d/%d]"),strIP,g_nAgentExAll-g_nAgentExStatus,g_nAgentExAll);
	}
	//jhhan 16-11-16 - 4DMakerEx.net 파일 업데이트 대기 Flag
	if(g_nAgentExAll-g_nAgentExStatus == g_nAgentExAll)
		pView->SetCheckUpdate(FALSE);

	ConvterToolBarShow(pView, TRUE);

	return 0;
}

CString CESMNetworkExDlg::FileReadDone(CString strDestIP, CString strSourceDSC)
{
	CString strMovie;

	CString strFile;
#ifdef _INI_4DA_LOCAL
	//네트워크 접근 경로 - 4DA ini 저장
	strFile.Format(_T("\\\\%s\\movie\\%s.ini"),strDestIP,strSourceDSC);
#endif
#ifdef _INI_4DP_NET_RAM
	//로컬 경로 - 4DP ini 저장
	strFile.Format(_T("%s\\%s.ini"),_T("M:\\movie"),strSourceDSC);
#endif
#ifdef _INI_4DP_NET_RT
	//로컬 경로 - 4DP RT
	strFile.Format(_T("%s\\RT\\RT.ini"),ESMGetPath(ESM_PATH_HOME));
#endif

	CESMIni ini;
	CESMRCManager* pRCMgr = NULL;
	if(ini.SetIniFilename (strFile))
	{
		CString strFlag, strPath;
#ifdef _SAVE_ORIGINAL
		strFlag = ini.GetString(strSourceDSC, _T("FLAG"), _T(""));
		strPath = ini.GetString(strSourceDSC, _T("PATH"), _T(""));
#else
		strFlag = ini.GetString(_T("RT"), _T("FLAG"), _T(""));
		strPath = ini.GetString(_T("RT"), _T("PATH"), _T(""));
#endif
		
		if(strFlag == _T("1"))
		{
#ifdef _SAVE_ORIGINAL
			ini.DeleteSection(strSourceDSC);
#else
			ini.DeleteSection(_T("RT"));
#endif
#ifdef _NETWORK_4DA
			//네트워크 접근 경로
			CString strDest = strPath.Mid(3, strPath.GetLength());
			strMovie.Format(_T("\\\\%s\\%s"), strDestIP, strDest);
			
#else
			if(m_bLocalLoad)
			{
				strMovie = strPath;
			}else
			{
				CString strDest = strPath.Mid(3, strPath.GetLength());
				strMovie.Format(_T("\\\\%s\\%s"), strDestIP, strDest);
			}
#endif
		}
	}
	return strMovie;
}
void CESMNetworkExDlg::KTSendFrame()
{
	CString strIP = GetSourceIP();
	if(strIP.IsEmpty())
		return;
	CString strCamID = GetSourceDSC();
	CString strFile = FileReadDone(strIP, strCamID);
	
	if(!strFile.IsEmpty())
	{
		if(m_bOpenClose == FALSE)
		{
			m_bOpenClose = TRUE;
			ESMSendFrameDataInfo(TRUE);//녹화 시작 신호 전송
			ESMLog(5, _T("KT Start Frame"));
			ESMSetGPUMakeFileCount(0);
		}

		m_nKTState = 0;
		
		CString* pStrData = NULL;
		pStrData = new CString;
		pStrData->Format(_T("%s"), strFile);
		ESMLog(5, strFile);
		
		ESMEvent* pMsg1	= new ESMEvent;
		pMsg1->message = WM_MAKEMOVIE_GPU_FILE_ADD_MSG;
		pMsg1->pParam	= (LPARAM)pStrData;
		HWND phanle = AfxGetMainWnd()->GetSafeHwnd();

		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(),WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg1 );
	}else
	{
		//ESMLog(5, _T("ReadDone"));
		//쓰레드 50ms 간격, 50ms * 100 = 5000ms = 5Sec
		if(m_nKTState < 100)//5초 확인 - 그동안 신호가 없으면 녹화 종료 신호 전송
		{
			m_nKTState++;
			TRACE(_T("KT - FileReadDone / m_nKTState wait : %d\r\n"), m_nKTState);
			return;
		}


		if(m_bOpenClose == TRUE)
		{
			//jhhan 16-12-06 Movie 잔여 확인 수정
			if(ESMGetMovieMgrCnt() > 0)
			{
				m_nKTState = 50;	//추가 1Sec
				return;
			}

			m_bOpenClose = FALSE;
			ESMSendFrameDataInfo(FALSE);//Close 신호
			ESMLog(5, _T("KT Finish Frame"));
			m_nKTState = 0;

			CESMFileOperation fo;
			CString strPath;
			strPath.Format(_T("%s"),ESMGetClientRamPath());
			fo.Delete(strPath, FALSE);
			//fo.CreateFolder(strPath);
		}
	}

}

UINT CESMNetworkExDlg::_ThreadKTSend(LPVOID pParam)
{
	CESMNetworkExDlg * pMain = (CESMNetworkExDlg*)pParam;

	while(1)
	{
		pMain->KTSendFrame();
		Sleep(50);		
		if(pMain->m_bThread == FALSE)
			break;
	}

	return 0;
}

void CESMNetworkExDlg::OnKTConnet()
{
	int nCount = GetRCMgrCount();
	CString strSendIP, strSendPort, strSouceIP, strSourceDSC;
	CESMRCManager* pRCMgr = NULL;

	if(nCount == 1)
	{
		pRCMgr = (CESMRCManager*)m_arRCServerList.GetAt(0);
		strSendIP = pRCMgr->GetSendIP();
		strSendPort = pRCMgr->GetSendPort();
		strSouceIP = pRCMgr->GetSourceIP();
		strSourceDSC = pRCMgr->GetSourceDSC();

		//4DA - IP, Port
		SetSourceIP(strSouceIP);
		SetSourceDSC(strSourceDSC);

		int nPort = _ttoi(strSendPort)/*8281*/;
		
		BOOL bRes = m_pRCClient->ConnectToKTServer(strSendIP, nPort);
		//Open CameraID Add
		m_pRCClient->SetSourceDSC(strSourceDSC);
		pRCMgr->SetKTConnected(bRes);
		if(bRes)
		{
			/*if(m_pWThd == NULL)
			{
				m_bThread = TRUE;
				m_pWThd = AfxBeginThread(_ThreadKTSend, (LPVOID)this);
			}*/
			switch(ESMGetValue(ESM_VALUE_4DPMETHOD))
			{
			case 0:
				{
					ESMLog(5,_T("FHD Frame Transfer Method"));
					ESMMovieRTThread(m_strSourceIP,m_strSourceDSC);
				}
				break;
			case 1:
				{
					ESMLog(5,_T("UHD File Transfer Method"));
					ESMMovieRTSend(m_strSourceIP,m_strSourceDSC);
				}
				break;
			case 2:
				{
					ESMLog(5,_T("FHD 60p File Transfer Method"));
					ESMMovieRTSend60p(m_strSourceIP,m_strSourceDSC);
				}
				break;
			}
			//if(ESMGetValue(ESM_VALUE_4DPMETHOD) == FALSE)//New Method
			//	ESMMovieRTSend(m_strSourceIP,m_strSourceDSC);
			//else
			//	ESMMovieRTThread(m_strSourceIP,m_strSourceDSC);

			ESMLog(5, _T("Connected - KT Server"));
		}
		else
		{
			
			ESMLog(5, _T("Not Connected - KT Server"));
		}
	}
	else
	{
		ESMLog(5, _T("4DMakerEx.net Error"));
	}
}

void CESMNetworkExDlg::OnKTDisconnect()
{
	m_bThread = FALSE;
	
	int nCount = GetRCMgrCount();
	CString strSendIP, strSouceIP, strSourceDSC;
	CESMRCManager* pRCMgr = NULL;

	if(nCount == 1)
	{
		pRCMgr = (CESMRCManager*)m_arRCServerList.GetAt(0);
		//strSendIP = pRCMgr->GetSendIP();
		strSouceIP = pRCMgr->GetSourceIP();
		strSourceDSC = pRCMgr->GetSourceDSC();
		
		SetSourceIP(strSouceIP);
		SetSourceDSC(strSourceDSC);
				
		m_pRCClient->DisConnectToKTServer();
		
		ESMLog(5, _T("Disconnected - KT Server"));
	}
}

void CESMNetworkExDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
	case PROC_CHECK_EX:
		{
			if (m_bProcCheck)
			{
				m_bProcCheck = FALSE;

				ThreadDataEx* pThreadData;
				pThreadData = new ThreadDataEx;
				pThreadData->pView = this;
				HANDLE hHandle = NULL;
				hHandle = (HANDLE) _beginthreadex(NULL, 0, DoProcessExCheck, (void *)pThreadData, 0, NULL);
				CloseHandle(hHandle);
			}
			break;
		}
	default:
		break;
	}

	CDialog::OnTimer(nIDEvent);
}

unsigned WINAPI CESMNetworkExDlg::DoProcessExCheck(LPVOID param)
{	
	ThreadDataEx* pThreadData = (ThreadDataEx*)param;
	CESMNetworkExDlg* pView = pThreadData->pView;

	if (pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}

	int nCount = pView->GetRCMgrCount();
	CString strSendIP;
	CESMRCManager* pRCMgr = NULL;

	while(nCount--)
	{
		pRCMgr = (CESMRCManager*)pView->m_arRCServerList.GetAt(nCount);

		if(pView->m_nTimerCount > 10)
		{
			pRCMgr->GetDiskSize();
			pView->m_nTimerCount = 0;
		}
		else
			pView->m_nTimerCount++;

		strSendIP = pRCMgr->GetIP();
		pView->m_ctrlList.GetProcState(strSendIP);
	}
	pView->m_bProcCheck = TRUE;
	return 0;
}
void CESMNetworkExDlg::OnRefereeModeStart()
{
	int nCount = GetRCMgrCount();
	CString strSendIP, strSendPort, strSouceIP, strSourceDSC;
	CESMRCManager* pRCMgr = NULL;

	if(nCount == 1)
	{
		pRCMgr = (CESMRCManager*)m_arRCServerList.GetAt(0);
		strSendIP = pRCMgr->GetSendIP();
		strSendPort = pRCMgr->GetSendPort();
		strSouceIP = pRCMgr->GetSourceIP();
		strSourceDSC = pRCMgr->GetSourceDSC();
		int nGroupIdx = pRCMgr->GetGroupIdx();

		//4DA - IP, Port
		SetSourceIP(strSouceIP);
		SetSourceDSC(strSourceDSC);

		int nPort = _ttoi(strSendPort)/*8281*/;
		ESMMovieRTSend(strSendIP,strSourceDSC,TRUE);

		ESMLog(5, _T("[Referee Reading] Start"));
	}
	else
	{
		ESMLog(5, _T("4DMakerEx.net Error"));
	}
}
void CESMNetworkExDlg::OnRTSPModeStart()
{
	int nCount = GetRCMgrCount();
	CString strSendIP, strSendPort, strSouceIP, strSourceDSC;
	CESMRCManager* pRCMgr = NULL;

	if(nCount == 1)
	{
		pRCMgr = (CESMRCManager*)m_arRCServerList.GetAt(0);
		strSendIP = pRCMgr->GetSendIP();
		strSendPort = pRCMgr->GetSendPort();
		strSouceIP = pRCMgr->GetSourceIP();
		strSourceDSC = pRCMgr->GetSourceDSC();
		int nGroupIdx = pRCMgr->GetGroupIdx();

		//4DA - IP, Port
		SetSourceIP(strSouceIP);
		SetSourceDSC(strSourceDSC);

		int nPort = _ttoi(strSendPort)/*8281*/;
		ESMMovieRTMovie(strSendIP,strSourceDSC,0);

		ESMLog(5, _T("[Referee Reading] Start"));
	}
	else
	{
		ESMLog(5, _T("4DMakerEx.net Error"));
	}
}

void CESMNetworkExDlg::OnKillTimer()
{
	KillTimer(PROC_CHECK_EX);
}
