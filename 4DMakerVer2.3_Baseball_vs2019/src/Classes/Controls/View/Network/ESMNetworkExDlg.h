////////////////////////////////////////////////////////////////////////////////
//
//	ESMNetworkExDlg.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "SdiDefines.h"
#include "ESMRCManager.h"

#include "ESMNetWorkDlg.h"
#include "ESMNetworkListCtrl.h"
#include "ESMNetworkExListCtrl.h"

//enum  {PROGMGR_ALLPROGRAMRUN = 2, PROGMGR_ALLPROGRAMDOWN, PROGMGR_ALLPROGRAMUPGRAD, PROGMGR_ALLCAMERAON, PROGMGR_ALLCAMERAOFF, PROGMGR_ALLSHUTDOWN};

#define	PROC_CHECK_EX		WM_USER+0x8282
#define PROC_CHECKTIMER_EX	4000

class CSdiSingleMgr;
class CESMOption;

struct ThreadDataEx
{
	ThreadDataEx()
	{
		nReTryCount = 1;
		nGroupIndex = 0;
		nMarginX = 0;
		nMarginY = 0;
	}
	CESMNetworkExDlg* pView;
	CESMRCManager* pMRCMgr;
	int nReTryCount;
	int nGroupIndex;
	int nMarginX;
	int nMarginY;
};

//struct ConnectInfo
//{
//	ConnectInfo()
//	{
//		m_Ip = 0;
//		m_count = 0;
//	}
//	
//	int m_Ip;
//	int m_count;
//};

#define WM_NE_RELOADLIST WM_USER + 0x3252
/////////////////////////////////////////////////////////////////////////////
// CESMNetworkExDlg dialog

class CESMNetworkExDlg : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	// Construction
public:
	CESMNetworkExDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMNetworkExDlg();

	// Dialog Data
	//{{AFX_DATA(CESMNetworkExDlg)
	enum { IDD = IDD_VIEW_NETWORK };
	//}}AFX_DATA
	void InitImageFrameWnd();

protected:
	//seo
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	CRect m_rcClientFrame;	
	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;	
	HWND m_hMainWnd;		
	CESMOption* m_pESMOpt;

	BOOL m_bRCMode;
	BOOL m_bProcCheck;
	//int m_nPort;
	int m_nTimerCount;

	
public:

	//-- 2013-10-08 hongsu@esmlab.com
	//-- Load Information from File 
	void LoadInfo();
	void SaveInfo();

	CESMNetworkExListCtrl m_ctrlList;
	CESMRCManager*		m_pRCClient;
	CObArray			m_arRCServerList;
	BOOL				m_bThreadStop;
	BOOL				m_bModify;
	vector<ConnectInfo> m_ConnectStatusIPList;
	int GetRCMgrCount()			{ return m_arRCServerList.GetCount(); }
	
	void AddRCMgr(CString strIP, int nPort, CString strSendIP = _T(""), CString strSendPort = _T(""), CString strSourceIP = _T(""), CString strSourceDSC = _T(""),int nGroupIdx = -1);
	void ModifyRCMgr(CString strPrevIP, CString strIP, int nPort, CString strSendIP = _T(""), CString strSourceIP = _T(""), CString strSourceDSC = _T(""),int nGroupIdx = -1);
	void DeleteRCMgr(CString strIP);
	//  2013-10-21 Ryumin
	void RemoveRCMgr();
	void DisConnectAll();
	CESMRCManager* GetRCMgr(int nIndex);
	CESMRCManager* GetRCMgr(CString strIP);
	CESMRCManager* GetRCMgrID(int nID);

	BOOL RCMgrConnect(CString strIP);
	BOOL RCMgrDisConnect(CString strIP);

	void DestroyAgent();
	void SetNetStatus(int nRemoteIp, int nStatus);
	void GetClientDiskSize();

	//-- 2013-12-13 kcd
	//-- All Connection
	void DoConnectionAll();
	static unsigned WINAPI DoConnecttionThread(LPVOID param);
	
	void FileExistCheck();
	static unsigned WINAPI DoFileExistCheckThread(LPVOID param);
	//-- All Program Run
	void DoAllProgramRun();
	static unsigned WINAPI DoProgramRunThread(LPVOID param);
	//-- All Program Down
	void DoAllProgramDown();
	static unsigned WINAPI DoProgramDownThread(LPVOID param);
	//-- All Download
	void DoDownloadAll();
	static unsigned WINAPI DoDownloadThread(LPVOID param);
	//-- All Camera On
	void DoCameraOn();
	static unsigned WINAPI DoCameraOnThread(LPVOID param);
	//-- All Camera Off
	void DoCameraOff();
	static unsigned WINAPI DoCameraOffThread(LPVOID param);
	//-- All ShutDown
	void DoAllShutDown(BOOL bMode = TRUE);
	static unsigned WINAPI DoShutDownThread(LPVOID param);

	//-- All Download
	void DoProcessUpdateAll();
	static unsigned WINAPI DoProcessUpdateThread(LPVOID param);

	//-- All IPerf Run
	void DoAllIPerfRun(BOOL bMode);
	static unsigned WINAPI DoAllIPerfRunThread(LPVOID param);

	//-- All IPerf Run
	void DoAllIperfOn();

	//-- All IPerf Test
	void DoAllIperfTest();

	void ReloadCamList();
	void UpdateStatus(CESMRCManager* pRCMgr);
	void UpdateDiskSize(CESMRCManager* pRCMgr);

	//--------------------------------------------------------
	//-- Message
	//--------------------------------------------------------
	void AddMsg(ESMEvent* pMsg);
	//  2013-10-22 Ryumin
	void RemoveAllMsg();

	//-- 2013-12-13 kjpark@esmlab.com
	//-- Network Program connect Check
	BOOL RCMgrIsConnected(CString strIP);
//-- 2013-12-13 kjpark@esmlab.com
//-- Network Program connect Check
	void RunThread(int nThreadIndex);
	HWND GetCESMNetworkExDlgHandle(){return m_hMainWnd;}


	void CheckToAgentAlive(int ip);
	void StartCheckToAgentAlive();
	void StopCheckToAgentAlive();
private:	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMNetworkExDlg)

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	afx_msg LRESULT OnReloadList(WPARAM wParam, LPARAM lParam);
	afx_msg void OnConnect();
	afx_msg void OnDisConnect();
	afx_msg void On4DMakerAllUpgrade();
	afx_msg void On4DMakerAllProgramRun();
	afx_msg void On4DMakerAllProgramDown();
	afx_msg void On4DMakerAllCameraOn();
	afx_msg void On4DMakerAllCameraOff();
	afx_msg void On4DMakerListDown();	
	afx_msg void On4DMakerAllShutDown();
	afx_msg void On4DMakerAllTurnOn();
	afx_msg void On4DMakerAllReBoot();
	afx_msg void On4DMakerListUp();
	afx_msg void test1();
	afx_msg void test2();
	afx_msg void test3();
	afx_msg void On4DMakerAllIperfOn();
	afx_msg void On4DMakerAllIperfRun();
	DECLARE_MESSAGE_MAP()

protected:
	// Generated message map functions
	//{{AFX_MSG(CESMNetworkExDlg)	
	afx_msg void OnPaint();
	//}}AFX_MSG	
	afx_msg void OnSize(UINT nType, int cx, int cy);

	CString m_strSourceIP;
	CString m_strSourceDSC;

	//jhhan 16-11-15
	BOOL m_bUpdate;	//4DMakerEx.net 업데이트 유무 FLAG
	BOOL m_bOpenClose; //OpenClose 상태 전송 FLAG
	int m_nKTState;
	BOOL m_bLocalLoad;

	BOOL m_bConnect;	//접속 유무 확인
public:
	void OnConnectCmd() { OnConnect(); };
	void OnAllCameraOffCmd() { On4DMakerAllCameraOff(); };
	CString FileReadDone(CString strDestIP, CString strSourceDSC);
	
	//4DA - IP
	void SetSourceIP(CString strIP){m_strSourceIP = strIP;}
	CString GetSourceIP(){return m_strSourceIP;}
	
	void KTSendFrame();

	//4DA - CamID
	void SetSourceDSC(CString strCamID){m_strSourceDSC = strCamID;}
	CString GetSourceDSC(){return m_strSourceDSC;}
	
	BOOL m_bThread;
	CWinThread *m_pWThd;
	static UINT _ThreadKTSend(LPVOID pParam);

	//jhhan 16-11-15
	void OnKTConnet();
	void OnKTDisconnect();

	void SetCheckUpdate(BOOL bUpdate){m_bUpdate = bUpdate;}
	BOOL GetCheckUpdate(){return m_bUpdate;}
	void DoDumpProcDownAll();

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	static unsigned WINAPI DoProcessExCheck(LPVOID param);

	void SetOnConnect(BOOL bOn){m_bConnect = bOn;}
	BOOL GetOnConnect(){return m_bConnect;}

	//180719 hjcho
	void OnRefereeModeStart();

	//180927 hjcho
	void OnRTSPModeStart();

	void OnKillTimer();

	BOOL m_bConnectFlag;
};
