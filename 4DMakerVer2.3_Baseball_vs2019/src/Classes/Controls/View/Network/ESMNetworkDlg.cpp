////////////////////////////////////////////////////////////////////////////////
//
//	ESMNetworkDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-04
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMNetworkDlg.h"
#include "resource.h"
#include "MainFrm.h"
#include "SdiSingleMgr.h"
#include "ESMIni.h"
#include "ESMUtil.h"
#include "FocusFileSaveDlg.h"

#include "ESMFileOperation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//-- 2014-09-08 hongsu@esmlab.com
//-- AllAgent Status Count 
static int g_nAgentAll	 = 0;
static int g_nAgentStatus = 0;

/////////////////////////////////////////////////////////////////////////////
// CESMNetworkDlg dialog
CESMNetworkDlg::CESMNetworkDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMNetworkDlg::IDD, pParent)	
{	
	WSADATA wsa;
	WSAStartup(MAKEWORD(2,2), &wsa);
	m_bRCMode = ESM_MODESTATE_SERVER;
	m_pRCClient = NULL;
	m_bThreadStop = FALSE;
	m_bProcCheck = TRUE;
	m_nTimerCount = 10;
	m_bConnectFlag = FALSE;
}

CESMNetworkDlg::~CESMNetworkDlg()
{
	KillTimer(PROC_CHECK);

	//  2013-10-22 Ryumin
	m_bThreadStop = TRUE;
 	RemoveAllMsg();
	if(m_bRCMode == ESM_MODESTATE_CLIENT)
 		DestroyAgent();
	else
		RemoveRCMgr();
}

void CESMNetworkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMNetworkDlg)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);	
	DDX_Control(pDX, IDC_NETWORK_CTRL, m_ctrlList);
}

BOOL CESMNetworkDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg)
	{
		if(pMsg->message == WM_KEYDOWN)
		{
			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			ESMEvent* pEsmMsg = NULL;
			pEsmMsg = new ESMEvent();
			pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
			pEsmMsg->pParam = (LPARAM)pMsg;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
		}		
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMNetworkDlg, CDialog)
	//{{AFX_MSG_MAP(CESMNetworkDlg)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()	
	ON_WM_PAINT()
	ON_MESSAGE(WM_NE_RELOADLIST, OnReloadList)
	ON_COMMAND(ID_IMAGE_NET_CONNECT		, OnConnect)
	ON_COMMAND(ID_IMAGE_NET_DISCONNECT	, OnDisConnect)
	ON_COMMAND(ID_4DMAKERALLUPGRADE		, On4DMakerAllUpgrade)
	ON_COMMAND(ID_4DMAKERALLTURNON		, On4DMakerAllProgramRun)
	ON_COMMAND(ID_4DMAKERALLTURNOFF		, On4DMakerAllProgramDown)
	ON_COMMAND(ID_4DMAKERALLCAMERAON	, On4DMakerAllCameraOn)
	ON_COMMAND(ID_4DMAKERALLCAMERAOFF	, On4DMakerAllCameraOff)
	ON_COMMAND(ID_4DMAKERALL_SHUTDOWN	, On4DMakerAllShutDown)
	ON_COMMAND(ID_4DMAKERALL_TURNON		, On4DMakerAllTurnOn)
	ON_COMMAND(ID_4DMAKERALL_REBOOT		, On4DMakerAllReBoot)
	ON_COMMAND(ID_4DMAKERLISTDOWN		, On4DMakerListDown)
	ON_COMMAND(ID_4DMAKERLISTUP			, On4DMakerListUp)
	ON_COMMAND(ID_TEST_1			, test1)
	ON_COMMAND(ID_TEST_2			, test2)
	ON_COMMAND(ID_TEST_3			, test3)
	ON_COMMAND(ID_4DMAKERALLBACKUP		, On4DMakerAllBackup)
	ON_COMMAND(ID_4DMAKERALLBACKUPOFF	, On4DMakerAllBackupOff)
	ON_COMMAND(ID_4DMAKERALLIPERF		, On4DMakerAllIperfOn)
	ON_COMMAND(ID_4DMAKERALLIPERF_RUN	, On4DMakerAllIperfRun)
	ON_WM_TIMER()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CESMNetworkDlg message handlers

BOOL CESMNetworkDlg::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}


	//-- SET MAIN WINDOWS HANDLE
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();		
	m_ctrlList.Init(this);

	/*CFont font;
	font.CreatePointFont(80, DSC_FONT);
	m_ctrlList.SetFont(&font);*/

	if (ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		SetTimer(PROC_CHECK, PROC_CHECKTIMER, NULL);

	return TRUE;
}



//-- 2013-12-13 kjpark@esmlab.com
//-- Network Program connect Check
void ConvterToolBarShow(CESMNetworkDlg* pView, BOOL isShow)
{	
	return;		 // TODO: 버튼 수행 비활성화 -> 활성화 기능

	ESMEvent* pMsg	= new ESMEvent;
	pMsg->message	= WM_ESM_FRAME_TOOLBAR_SHOW;
	pMsg->pDest		= isShow;			
	::SendMessage(pView->GetCESMNetworkDlgHandle(),WM_ESM, (WPARAM)WM_ESM_NET,(LPARAM)pMsg);
}

void CESMNetworkDlg::On4DMakerAllUpgrade()
{
	if (m_bConnectFlag)
	{
		ESMLog(5, _T("The agent is connecting."));
		return;
	}
	RunThread(PROGMGR_ALLPROGRAMUPGRAD);
}

void CESMNetworkDlg::On4DMakerAllProgramRun()
{
	RunThread(PROGMGR_ALLPROGRAMRUN);
}

void CESMNetworkDlg::On4DMakerAllProgramDown()
{	
	m_bConnectFlag = FALSE;

	ESMSetRecordStatus(FALSE);

	m_bThreadStop = TRUE;
	RunThread(PROGMGR_ALLPROGRAMDOWN);

	//ESMSetDeleteFolder(_T(""));
}

void CESMNetworkDlg::On4DMakerAllCameraOn()
{
	RunThread(PROGMGR_ALLCAMERAON);
}
void CESMNetworkDlg::On4DMakerAllCameraOff()
{
	m_bConnectFlag = FALSE;

	ESMSetRecordStatus(FALSE);

	//jhhan 170726 Disconnect Initial
	ESMSetFrameRate(MOVIE_FPS_UNKNOWN);

	m_bThreadStop = TRUE;
	RunThread(PROGMGR_ALLCAMERAOFF);

	//ESMSetDeleteFolder(_T(""));
}

void CESMNetworkDlg::On4DMakerAllShutDown()
{
	m_bConnectFlag = FALSE;
	if(ESMGetValue(ESM_VALUE_RTSP) == FALSE)
	{
		CString strWOL;
		strWOL.Format(_T("%s\\bin\\%s"),ESMGetPath(ESM_PATH_HOME), _T("WakeMeOnLan.exe"));
		CESMFileOperation fo;
		if(fo.IsFileExist(strWOL) != FALSE)
		{

			strWOL.Format(_T("%s\\bin"),ESMGetPath(ESM_PATH_HOME));
			ESMUtil::ExecuteProcess(strWOL, _T("WakeMeOnLan.exe"), _T("/scan"));
			Sleep(500);
		}
		CString strFile, strMacFile;
		strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);
		strMacFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_MAC);
		//-- Load Config File
		CESMIni ini, iniMac;	
		if(ini.SetIniFilename (strFile) && iniMac.SetIniFilename(strMacFile))
		{
			CString strIPSection;
			CString strIP;

			CString strMacSection;

			int nIndex = 0;
			//Server or Agent 구분
			if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
			{
				iniMac.DeleteSection(INFO_SECTION_MACADDR);
				while(1)
				{
					strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);
					//strPortSection.Format(_T("%s_%d"),INFO_RC_PORT,nIndex);

					strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);
					//nPort = ini.GetInt(INFO_SECTION_RC, strPortSection	);

					if(!strIP.GetLength())
						break;

					CString strMac;
					strMac = ESMGetMACAddress(strIP);
					//strMac.Replace(_T("-"),_T(""));
					//strMac.Trim();
					//ESMUtil::WakeOnLan(strMac);

					strMacSection.Format(_T("%d"), nIndex+1);
					iniMac.WriteString(INFO_SECTION_MACADDR, strMacSection, strMac);

					nIndex++;
				}			
			}

		}
	}

	int nRet = MessageBox(_T("Are you sure you want shut down the Client PC?"),_T("Question"), MB_YESNO);
	if (nRet == IDYES)
	{
		RunThread(PROGMGR_ALLSHUTDOWN);
	}

	//ESMSetDeleteFolder(_T(""));
}

void CESMNetworkDlg::On4DMakerAllTurnOn()
{
	//AfxMessageBox(_T("Turnon Test"));
	CString strWOL;
	strWOL.Format(_T("%s\\bin\\%s"),ESMGetPath(ESM_PATH_HOME), _T("WakeMeOnLan.exe"));
	CESMFileOperation fo;
	if(fo.IsFileExist(strWOL) == FALSE)
	{
		AfxMessageBox(strWOL, MB_ICONERROR);
		return;
	}

	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_MAC);

	//-- Load Config File
	CESMIni ini;	
	if(ini.SetIniFilename (strFile))
	{
		CString strMacSection;
		CString strMac;

		int nIndex = 1;

		while(1)
		{
			strMacSection.Format(_T("%d"),nIndex);

			strMac = ini.GetString(INFO_SECTION_MACADDR, strMacSection);
			strMac.Replace(_T("-"),_T(""));

			if(!strMac.GetLength())
				break;

			ESMUtil::WakeOnLan(strMac.Trim());
			nIndex++;
		}			
	}
	else 
	{
		AfxMessageBox(_T("Check 4DMaker\\config\\4DMaker.mac file"));
	}

}

void CESMNetworkDlg::On4DMakerAllReBoot()
{
	int nRet = MessageBox(_T("Are you sure you want Re Boot the Client PC?"),_T("Question"), MB_YESNO);
	if (nRet == IDYES)
	{
		RunThread(PROGMGR_ALLREBOOOT);
	}

	//ESMSetDeleteFolder(_T(""));
}

void CESMNetworkDlg::On4DMakerListDown()
{
	CString strStatus;
	CString strIP;
	CString strPort;
	CString strUpStatus;
	CString strUpIp;
	CString strUpPort;

	POSITION pos = NULL;
	int nItemIdx  = 0;
	int nUpIdx = 0;

	pos = m_ctrlList.GetFirstSelectedItemPosition();

	if( NULL != pos )
	{
		nItemIdx = m_ctrlList.GetNextSelectedItem( pos );
		nUpIdx = nItemIdx + 1;
	}

	int nRowCount = m_ctrlList.GetItemCount();

	if(nItemIdx == nRowCount-1)
	{
		m_ctrlList.SetFocus();
		return;
	}
	else
	{
		strPort = m_ctrlList.GetItemText(nItemIdx, 2);
		strUpPort = m_ctrlList.GetItemText(nUpIdx, 2);
		strIP = m_ctrlList.GetItemText(nItemIdx, 1);
		strUpIp = m_ctrlList.GetItemText(nUpIdx, 1);
		strStatus = m_ctrlList.GetItemText(nItemIdx, 0);
		strUpStatus = m_ctrlList.GetItemText(nUpIdx, 0);

		m_ctrlList.SetItemText(nUpIdx, 2, strPort);
		m_ctrlList.SetItemText(nItemIdx, 2, strUpPort);
		m_ctrlList.SetItemText(nUpIdx, 1, strIP);
		m_ctrlList.SetItemText(nItemIdx, 1, strUpIp);
		m_ctrlList.SetItemText(nUpIdx, 0, strStatus);
		m_ctrlList.SetItemText(nItemIdx, 0, strUpStatus);

		m_ctrlList.SetItemState(nUpIdx, LVIS_SELECTED,  LVIS_SELECTED);
		m_ctrlList.SetItemState(nItemIdx, 0,  LVIS_SELECTED);
		m_ctrlList.EnsureVisible(nUpIdx, TRUE); 
		m_ctrlList.SetFocus();
	}
	CESMRCManager* pRCMgr1, *pRCMgr2;
	if( m_arRCServerList.GetCount() > nItemIdx)
	{
		pRCMgr1 =  (CESMRCManager*)m_arRCServerList.GetAt(nItemIdx);
		pRCMgr2 =  (CESMRCManager*)m_arRCServerList.GetAt(nItemIdx + 1);
		m_arRCServerList.SetAt(nItemIdx + 1, pRCMgr1);
		m_arRCServerList.SetAt(nItemIdx, pRCMgr2);
	}
	SaveInfo();
}

void CESMNetworkDlg::On4DMakerAllIperfOn()
{
	RunThread(PROGMGR_ALLRUNIPERF);
}

void CESMNetworkDlg::On4DMakerAllIperfRun()
{
	RunThread(PROGMGR_ALLTESTIPERF);
}

void CESMNetworkDlg::On4DMakerListUp()
{
	CString strStatus;
	CString strIP;
	CString strPort;
	CString strDownStatus;
	CString strDownIp;
	CString strDownPort;

	POSITION pos = NULL;
	int nItemIdx  = 0;
	int nDownIdx = 0;

	pos = m_ctrlList.GetFirstSelectedItemPosition();

	if( NULL != pos )
	{
		nItemIdx = m_ctrlList.GetNextSelectedItem( pos );
		nDownIdx = nItemIdx - 1;
	}

	if(nItemIdx == 0)
	{
		m_ctrlList.SetFocus();
		return;
	}
	else
	{
		strPort = m_ctrlList.GetItemText(nItemIdx, 2);
		strDownPort = m_ctrlList.GetItemText(nDownIdx, 2);
		strIP = m_ctrlList.GetItemText(nItemIdx, 1);
		strDownIp = m_ctrlList.GetItemText(nDownIdx, 1);
		strStatus = m_ctrlList.GetItemText(nItemIdx, 0);
		strDownStatus = m_ctrlList.GetItemText(nDownIdx, 0);

		m_ctrlList.SetItemText(nDownIdx, 2, strPort);
		m_ctrlList.SetItemText(nItemIdx, 2, strDownPort);
		m_ctrlList.SetItemText(nDownIdx, 1, strIP);
		m_ctrlList.SetItemText(nItemIdx, 1, strDownIp);
		m_ctrlList.SetItemText(nDownIdx, 0, strStatus);
		m_ctrlList.SetItemText(nItemIdx, 0, strDownStatus);

		m_ctrlList.SetItemState(nDownIdx, LVIS_SELECTED,  LVIS_SELECTED);
		m_ctrlList.SetItemState(nItemIdx, 0,  LVIS_SELECTED);
		m_ctrlList.EnsureVisible(nDownIdx, TRUE);	
		m_ctrlList.SetFocus();
	}
	CESMRCManager* pRCMgr1, *pRCMgr2;
 	if( m_arRCServerList.GetCount() > nItemIdx)
	{
		pRCMgr1 =  (CESMRCManager*)m_arRCServerList.GetAt(nItemIdx);
		pRCMgr2 =  (CESMRCManager*)m_arRCServerList.GetAt(nItemIdx - 1);
		m_arRCServerList.SetAt(nItemIdx - 1, pRCMgr1);
		m_arRCServerList.SetAt(nItemIdx, pRCMgr2);
	}
	SaveInfo();
}


void CESMNetworkDlg::On4DMakerAllBackup()
{
	RunThread(PROGMGR_ALLBACKUPPROGRAMRUN);
}

void CESMNetworkDlg::On4DMakerAllBackupOff()
{
	RunThread(PROGMGR_ALLBACKUPPROGRAMOFF);
}

void CESMNetworkDlg::RunThread(int nThreadIndex)
{
	//-- Remote Agent
	if(m_bRCMode == ESM_MODESTATE_CLIENT)
	{
		// 		if(m_pRCClient)
		// 			m_pRCClient->StartServer(m_nPort);
	}
	//-- Server
	else
	{		
		switch(nThreadIndex)
		{
		case 0:
			DoConnectionAll();	
			FileExistCheck();	
			
			break;
		case 1:
			break;
		case PROGMGR_ALLPROGRAMRUN:
			DoAllProgramRun();						
			break;
		case PROGMGR_ALLPROGRAMDOWN:
			DoDumpProcDownAll();
			Sleep(1000);
			DoAllProgramDown();
			break;
		case PROGMGR_ALLPROGRAMUPGRAD:
			DoDownloadAll();				
			break;
		case PROGMGR_ALLCAMERAON:
			DoCameraOn();			
			break;
		case PROGMGR_ALLCAMERAOFF:
			DoCameraOff();
			DoDumpProcDownAll();
			Sleep(1000);
			DoAllProgramDown();
			break;
		case PROGMGR_ALLSHUTDOWN:
			DoAllShutDown(TRUE);
			break;
		case PROGMGR_PROCESSUPDATE:
			DoProcessUpdateAll();
			break;
		case PROGMGR_ALLREBOOOT:
			DoAllShutDown(FALSE);
			break;
		case PROGMGR_ALLBACKUPPROGRAMRUN:
			DoAllBackupProgramRun();
			break;
		case PROGMGR_ALLBACKUPPROGRAMOFF:
			DoAllBackupProgramOff();
			break;
		case PROGMGR_ALLRUNIPERF:
			DoAllIperfOn();
			break;
		case PROGMGR_ALLTESTIPERF:
			DoAllIperfTest();
			break;
		}

	}	
}

/////////////////////////////////////////////////////////////////////////////
// CESMNetworkDlg

void CESMNetworkDlg::OnPaint()
{	
	CDialog::OnPaint();
}

void CESMNetworkDlg::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		ID_IMAGE_NET_CONNECT	,	
		ID_IMAGE_NET_DISCONNECT	,	
		ID_SEPARATOR			,
		//-- 2013-12-13 kjpark@esmlab.com
		//-- Network Program connect Check
		ID_4DMAKERALLTURNON		,	
		ID_4DMAKERALLTURNOFF	,
		ID_SEPARATOR			,
		ID_4DMAKERALLUPGRADE	,
		ID_SEPARATOR			,
		ID_4DMAKERALLCAMERAON	,	
		ID_4DMAKERALLCAMERAOFF	,
		ID_SEPARATOR			,
		ID_4DMAKERLISTDOWN		,
		ID_4DMAKERLISTUP		,
		ID_SEPARATOR			,
		ID_4DMAKERALL_SHUTDOWN	,
		ID_4DMAKERALL_TURNON	, 
		ID_4DMAKERALL_REBOOT	,
		ID_SEPARATOR			,
		//ID_4DMAKERALLBACKUP		,
		//ID_4DMAKERALLBACKUPOFF	,
		ID_SEPARATOR			,
		ID_4DMAKERALLIPERF,
		ID_4DMAKERALLIPERF_RUN,
		/*ID_TEST_1,
		ID_TEST_2,
		ID_TEST_3,*/
	};
	
	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,2));	
	}else
		VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	

	m_wndToolBar.ShowWindow(SW_SHOW);
}


void CESMNetworkDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	/*m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;*/
	m_rcClientFrame.top	= 40;
	RepositionBars(0,0xFFFF,0);
	
	if(m_ctrlList)
		m_ctrlList.MoveWindow(m_rcClientFrame);
}

void CESMNetworkDlg::LoadInfo()
{
	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);

	m_bRCMode = ESMGetValue(ESM_VALUE_NET_MODE);
	if(m_bRCMode == ESM_MODESTATE_CLIENT)
	{
		if (m_pRCClient == NULL)
		{
			m_pRCClient = new CESMRCManager();
			//m_nPort = ESMGetValue(ESM_VALUE_NET_PORT);
			m_pRCClient->StartServer();
			m_pRCClient->CreateThread();
		}
		return;
	}

	//-- Load Config File
	CESMIni ini;	
	if(ini.SetIniFilename (strFile))
	{
		CString strIPSection, strPortSection;
		CString strIP, strSendIP;
		int nPort = 0;

		int nIndex = 0;
		while(1)
		{
			strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);
			//strPortSection.Format(_T("%s_%d"),INFO_RC_PORT,nIndex);

			strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);
			//nPort = ini.GetInt(INFO_SECTION_RC, strPortSection	);
			
			//jhhan 16-12-20
			//기능 사용 제외
			//strSendIP = ini.GetString(INFO_RC_SENDIP, strIPSection	);

			if(!strIP.GetLength())
				break;

			if(!strSendIP.GetLength())
			{
				//-- Create Remote Control Manager
				AddRCMgr(strIP, nPort);
			}
			else
			{
				AddRCMgr(strIP, nPort, strSendIP);
			}
			nIndex++;
		}			
	}

	//-- 2013-10-08 hongsu@esmlab.com
	//-- Load List Control
	m_ctrlList.LoadInfo(TRUE);

	//-- 2014-09-08 hongsu@esmlab.com
	//-- Invalidate
	Invalidate(TRUE);
}

void CESMNetworkDlg::SaveInfo()
{
	if(m_bRCMode == ESM_MODESTATE_CLIENT)
		return;

	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);
	//-- Load Config File
	CESMIni ini;
	CESMRCManager* pRCMgr = NULL;
	if(ini.SetIniFilename (strFile))
	{
		ini.DeleteSection(INFO_SECTION_RC);
		ini.DeleteSection(_T("4DAP"));

		CString strIPSection, strPortSection;
		CString strIP, strSendIP;
		int nPort;

		int nAll = GetRCMgrCount();
		for(int i = 0 ; i < nAll ; i ++)
		{
			pRCMgr = GetRCMgr(i);
			if(!pRCMgr)
				break;

			strIPSection.Format(_T("%s_%d"),INFO_RC_IP,i);
			strPortSection.Format(_T("%s_%d"),INFO_RC_PORT,i);

			strIP = pRCMgr->GetIP();
			//nPort = pRCMgr->GetPort();
			ini.WriteString	(INFO_SECTION_RC, strIPSection		, strIP);
			//ini.WriteInt	(INFO_SECTION_RC, strPortSection	, nPort);
			strSendIP = pRCMgr->GetSendIP();
			/*if(ESMGetValue(ESM_VALUE_4DAP))
			{
				if(strSendIP.IsEmpty() != TRUE && strSendIP.CompareNoCase(_T("127.0.0.1")) != 0)
				{
					ini.WriteString(_T("4DAP"), strIP, strSendIP);
				}
			}*/
			
		}			
	}
}


void CESMNetworkDlg::AddRCMgr(CString strIP, int nPort)
{
	CESMRCManager* pRCMgr = NULL;
	pRCMgr = new CESMRCManager(strIP, nPort);
	pRCMgr->CreateThread();
	m_arRCServerList.Add((CObject*)pRCMgr);
	ReloadCamList();
}

void CESMNetworkDlg::AddRCMgr(CString strIP, int nPort, CString StrSendIP)
{
	CESMRCManager* pRCMgr = NULL;
	pRCMgr = new CESMRCManager(strIP, nPort);
	pRCMgr->CreateThread();
	m_arRCServerList.Add((CObject*)pRCMgr);
	pRCMgr->SetSendIP(StrSendIP);
	ReloadCamList();
}

void CESMNetworkDlg::ModifyRCMgr(CString strPrevIP, CString strIP, int nPort)
{
	CESMRCManager* pExistRCMgr = NULL;
	int nAll = GetRCMgrCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExistRCMgr = GetRCMgr(i);
		if(pExistRCMgr->GetIP() == strPrevIP)
		{
			//pExistRCMgr->SetPort(nPort);
			pExistRCMgr->SetIP(strIP);
			ReloadCamList();
			return;
		}
	}
}

void CESMNetworkDlg::DeleteRCMgr(CString strIP)
{
	CESMRCManager* pExistRCMgr = NULL;
	int nAll = GetRCMgrCount();
	for(int i = 0 ; i < nAll ; i ++)
	{
		pExistRCMgr = GetRCMgr(i);
		if(pExistRCMgr->GetIP() == strIP)
		{
			pExistRCMgr->m_bConnectStatusCheck =FALSE;
			pExistRCMgr->Disconnect();
			if( pExistRCMgr )
			{
				delete pExistRCMgr;
				pExistRCMgr = NULL;
			}
			m_arRCServerList.RemoveAt(i);
			ReloadCamList();
			return;
		}
	}
}

void CESMNetworkDlg::ReloadCamList()
{
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_VIEW_TIMELINE_RELOAD;
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
}

/************************************************************************
* @method:		CESMNetworkDlg::RemoveRCMgr
* @function:	RemoveRCMgr
* @date:		2013-10-21
* @owner		Ryumin (ryumin@esmlab.com)
* @return:		void
*/
void CESMNetworkDlg::RemoveRCMgr()
{
	CESMRCManager* pRCMgr = NULL;
	int nAll = GetRCMgrCount();
	for(int nIdx = 0; nIdx < nAll ; ++nIdx)
	{
		pRCMgr = GetRCMgr(nIdx);
		if(!pRCMgr)	continue;

		delete pRCMgr;
		pRCMgr = NULL;
	}			
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-10-14
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMNetworkDlg::UpdateStatus(CESMRCManager* pRCMgr)
{
	m_ctrlList.UpdateStatus(pRCMgr);
}

void CESMNetworkDlg::UpdateDiskSize(CESMRCManager* pRCMgr)
{
	m_ctrlList.UpdateDiskSize(pRCMgr);
}

LRESULT CESMNetworkDlg::OnReloadList(WPARAM wParam, LPARAM lParam)
{
	m_ctrlList.LoadInfo();
	return 0;
}

//------------------------------------------------------------------------------ 
//! @brief		OnConnect
//! @date		2013-09-30
//! @author		yongmin
//------------------------------------------------------------------------------ 
void CESMNetworkDlg::OnConnect()
{
	m_bConnectFlag = TRUE;

	// resync init
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		pItem->SetRecordReady(TRUE);
		pItem->SetReSyncGetTickReady(TRUE);
	}

	ESMSetFirstFlag(FALSE);
	//jhhan 16-09-12
	if(ESMGetLoadRecord())
	{
		ESMSetLoadRecord(FALSE);
		
		ESMInitTitleBar();

		ESMReloadCamList();

		/*DisConnectAll();

		ESMDSCRemoveAllList();*/
	}

	if(ESMGetGPUMakeFile())
	{
		if(ESMGetNetworkEx() == FALSE && ESMGetValue(ESM_VALUE_RTSP) == FALSE)
		{
			ESMLog(0, _T("4DProcessor is not connected."));

			if(AfxMessageBox(_T("4DProcessor is not connected.\r\n\nDo you want to contiue?"), MB_YESNO|MB_ICONQUESTION) == IDNO)
			{
				return;
			}
		}
		
		
		RunThread(PROGMGR_PROCESSUPDATE);
	}

	//AfxMessageBox(_T("Check Focus!!"));
	ESMSetExecuteMode(TRUE);
	RunThread(0);

	ESMSetDeleteFolder(_T(""));
	
	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent;
		pMsg->message = WM_ESM_VIEW_SETNEWESTADJ;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);

		ESMSetRecordSync();
		ESMLoadSquarePointData();
	}
	
}

void CESMNetworkDlg::DoConnectionAll()
{
	ESMSetMaingInfoFlag(FALSE);
	//ESMDSCRemoveAll();

	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentAll
	g_nAgentAll		= nAll;
	g_nAgentStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_CONNECTING;

		if( pExist)
		{
			ThreadData* pThreadData;
			pThreadData = new ThreadData;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;
			pThreadData->nReTryCount = 1;
			pThreadData->nGroupIndex = nAll;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoConnecttionThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);

	int aAll = GetRCMgrCount();

	m_ConnectStatusIPList.clear();
	CESMRCManager* ppExist = NULL;
	ConnectInfo  cinfo;
	while(aAll--)
	{
		CString strIp;
		ppExist = GetRCMgr(aAll);
		strIp = ppExist->GetIP();
		CString ipStr;
		AfxExtractSubString( ipStr, strIp, 3, '.');

		int ipInt = _ttoi(ipStr);
		cinfo.m_Ip = ipInt;
		m_ConnectStatusIPList.push_back(cinfo);
	}
}

void CESMNetworkDlg::DoDumpProcDownAll()
{
	int nAll = GetRCMgrCount();
	
	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);

		if(pExist)
		{
			pExist->DumpProcDown();
		}
	}
}


void CESMNetworkDlg::FileExistCheck()
{
	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentAll
	g_nAgentAll		= nAll;
	g_nAgentStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_CONNECTING;

		if( pExist)
		{
			ThreadData* pThreadData;
			pThreadData = new ThreadData;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;
			pThreadData->nReTryCount = 1;
			pThreadData->nGroupIndex = nAll;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoFileExistCheckThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
}
void CESMNetworkDlg::DoAllProgramRun()
{
	ConvterToolBarShow(this, FALSE);

	// 	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentAll
	g_nAgentAll		= nAll;
	g_nAgentStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_READY;

		if( pExist)
		{
			ThreadData* pThreadData;
			pThreadData = new ThreadData;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;
			pThreadData->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoProgramRunThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();

	ConvterToolBarShow(this, TRUE);
}

void CESMNetworkDlg::DoAllProgramDown()
{
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentAll
	g_nAgentAll		= nAll;
	g_nAgentStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_DISCONNECT;

		if( pExist)
		{
			ThreadData* pThreadData;
			pThreadData = new ThreadData;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;
			pThreadData->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoProgramDownThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);
	
	//190425 hjcho
	ESMSetAdjustLoad(FALSE);
}

void CESMNetworkDlg::DoDownloadAll()
{
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentAll
	g_nAgentAll		= nAll;
	g_nAgentStatus  = nAll;

	//ESMSetDump(FALSE);
	HWND _hWnd = NULL;
	DWORD dwPid = 0;
	
	dwPid = ESMUtil::GetProcessPID(_T("procdump64.exe"));
	if (dwPid != 0)
	{
		_hWnd = ESMUtil::GetWndHandle(dwPid);
		::SendMessage(_hWnd, WM_CLOSE, (WPARAM)NULL, (LPARAM)NULL);
	}
	
	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_DOWNLOAD;

		if( pExist)
		{
			ThreadData* pThreadData;
			pThreadData = new ThreadData;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;
			pThreadData->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoDownloadThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);
		}

		//Sleep(500);
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);
}

void CESMNetworkDlg::DoCameraOn()
{
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentAll
	g_nAgentAll		= nAll;
	g_nAgentStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_CONNECTING;

		if( pExist)
		{
			ThreadData* pThreadData;
			pThreadData = new ThreadData;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;
			pThreadData->nReTryCount = 1;

			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoCameraOnThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);
}

void CESMNetworkDlg::DoCameraOff()
{
	ESMSetSyncFlag(FALSE);
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentAll
	g_nAgentAll		= nAll;
	g_nAgentStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_DISCONNECT;

		if( pExist)
		{
			ThreadData* pThreadData;
			pThreadData = new ThreadData;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;
			pThreadData->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoCameraOffThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);
}

void CESMNetworkDlg::DoAllIperfTest()
{
	CFile file;
	CString strResultPath, strDate, strFilePath, WriteData;
	//strResultPath.Format(_T("%s\\log\\NTRESULT"),ESMGetPath(ESM_PATH_HOME));
	strResultPath.Format(_T("%s\\log"),ESMGetServerRamPath());
	CreateDirectory(strResultPath, NULL);

	//strFilePath.Format(_T("%s\\NTTest.bat"), strResultPath);

	SYSTEMTIME st;
	GetLocalTime(&st);
	int nMonth = st.wMonth;
	int nDay = st.wDay;
	int nHour = st.wHour;
	int nMin  = st.wMinute;
	int nSec = st.wSecond;
	int nMilli = st.wMilliseconds;
	strDate.Format(_T("\\%02d_%02d_%02d_%02d_%02d"), nMonth, nDay,nHour,nMin,nSec);
	strResultPath.Append(strDate);
	CreateDirectory(strResultPath, NULL);

	int nAll = GetRCMgrCount();

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		CString strIP = pExist->GetIP();
		CString strCmd,strOpt;

		CString strLog;
		strLog.Format(_T("%s\\\\%s.txt"),strResultPath, strIP);
		//strLog.Format(_T("%s\\%s.txt"),strResultPath, strIP);
		//strLog.Replace(_T("."), _T("_"));
		/*CFile file;
		CFileException e;
		if(file.Open(strLog, CFile::modeCreate, &e))
			file.Close();
		else
			ESMLog(0, _T("CFileException : %d"), e.m_cause);*/

		//strCmd.Format(_T("\"%s\\bin\\iperf3.exe\""),ESMGetPath(ESM_PATH_HOME));
		strCmd.Format(_T("%s\\bin\\iperf.bat"),ESMGetPath(ESM_PATH_HOME));
		//strOpt.Format(_T("-c %s -i 1 > %s"), strIP, strLog);
		//strOpt.Format(_T("-c %s -i 1 > %s"), strIP, strResultPath, strLog);
		strOpt.Format(_T("%s %s"), strIP, strLog);
		
		//WriteData.Append(strCmd);
		//WriteData.Append(strOpt);
		
		SHELLEXECUTEINFO lpExecInfo;
		lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
		lpExecInfo.lpFile = strCmd;
		lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		lpExecInfo.hwnd = NULL;  
		lpExecInfo.lpVerb = L"open";
		lpExecInfo.lpParameters = strOpt;
		lpExecInfo.lpDirectory = NULL;
		lpExecInfo.nShow	=SW_SHOW;
		//lpExecInfo.nShow = SW_HIDE; // hide shell during execution
		lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;

		BOOL bState = ShellExecuteEx(&lpExecInfo);

		// wait until the process is finished
		if (lpExecInfo.hProcess != NULL)
		{
			::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
			//AfxMessageBox(_T("Test End"));
			//::CloseHandle(lpExecInfo.hProcess);
		}


		pExist->IPerfRun(FALSE);
		

		/*lpExecInfo.lpFile = _T("exit");
		lpExecInfo.lpVerb = L"close";
		lpExecInfo.lpParameters = NULL;

		if(TerminateProcess(lpExecInfo.hProcess,0))
		{
			lpExecInfo.hProcess = 0;
			ESMLog(5, _T("End Network Test [%s\\%s.txt]"), strResultPath, strIP);
		}*/
	}

	/*char* pstrData = NULL;
	if(file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
	{
		
		pstrData = ESMUtil::CStringToChar(WriteData);
		file.Write(pstrData, strlen(pstrData));
		delete[] pstrData;
		pstrData = NULL;
		
		file.Close();

		CString strCmd, strOpt;
		strCmd.Format(_T("%s"),strFilePath);

		SHELLEXECUTEINFO lpExecInfo;
		lpExecInfo.cbSize  = sizeof(SHELLEXECUTEINFO);
		lpExecInfo.lpFile = strCmd;
		lpExecInfo.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;     
		lpExecInfo.hwnd = NULL;  
		lpExecInfo.lpVerb = L"open";
		lpExecInfo.lpParameters = strOpt;
		lpExecInfo.lpDirectory = NULL;
		lpExecInfo.nShow	=SW_SHOW;
		//lpExecInfo.nShow = SW_HIDE; // hide shell during execution
		lpExecInfo.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL;

		BOOL bState = ShellExecuteEx(&lpExecInfo);

		// wait until the process is finished
		if (lpExecInfo.hProcess != NULL)
		{
			::WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
			::CloseHandle(lpExecInfo.hProcess);
		}
	}*/

	
	//int nCnt = GetRCMgrCount();
	//while(nCnt--)
	//{
	//	CESMRCManager* pRc = NULL;
	//	pRc = GetRCMgr(nCnt);

	//	if(pRc)
	//	{
	//		//jhhan 180605
	//		pRc->IPerfRun(FALSE);
	//	}
	//}
}

void CESMNetworkDlg::DoAllIperfOn()
{
	int nAll = GetRCMgrCount();

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);

		if(pExist)
		{
			pExist->IPerfRun();
		}
	}
}

void CESMNetworkDlg::DoAllShutDown(BOOL bMode)
{
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentAll
	g_nAgentAll		= nAll;
	g_nAgentStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);

		if( pExist)
		{
			ThreadData* pThreadData;
			pThreadData = new ThreadData;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;

			if(bMode)
				pThreadData->nReTryCount = 1;
			else
				pThreadData->nReTryCount = 0;

			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoShutDownThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);
}

//171130 wgkim
void CESMNetworkDlg::DoAllBackupProgramRun()
{
	ConvterToolBarShow(this, FALSE);
	
	int nAll = GetRCMgrCount();
	g_nAgentAll		= nAll;
	g_nAgentStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);

		if( pExist)
		{
			ThreadData* pThreadData;
			pThreadData = new ThreadData;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;
			pThreadData->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoBackupProgramRunThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);

			if(pThreadData->pMRCMgr->m_hBackupProgramProgressHandle == NULL)
			{				
				ThreadData* pThreadDataProgress;
				pThreadDataProgress= new ThreadData;
				pThreadDataProgress->pView = this;
				pThreadDataProgress->pMRCMgr = pExist;
				pThreadDataProgress->nReTryCount = 1;
				
				m_pArrThreadDataProgress.insert(pair<CString, ThreadData*>(
					pExist->GetIP(), 
					pThreadDataProgress));
				
				HANDLE pHandle = (HANDLE) _beginthreadex(NULL, 0, DoBackupProgramProgressThread, (void *)pThreadDataProgress, 0, NULL);
				m_pArrHandle.insert(pair<CString, HANDLE>(
					pExist->GetIP(), 
					pHandle));

				pThreadDataProgress->pMRCMgr->m_bIsExcutingBackupProgram = TRUE;
			}
		}
	}
	m_ctrlList.LoadInfo();

	ConvterToolBarShow(this, TRUE);
}

void CESMNetworkDlg::DoAllBackupProgramOff()
{
	ConvterToolBarShow(this, FALSE);

	// 	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentAll
	g_nAgentAll		= nAll;
	g_nAgentStatus  = nAll;

	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);

		if( pExist)
		{
			ThreadData* pThreadData;
			pThreadData = new ThreadData;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;
			pThreadData->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoBackupProgramOffThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);
						
			if(pExist->m_bIsExcutingBackupProgram)
			{	
				pExist->m_bIsExcutingBackupProgram = FALSE;				
				
				HANDLE hHandle = m_pArrHandle[pExist->GetIP()];
				::TerminateThread( hHandle, 0 );
				CloseHandle(hHandle);			
			}
		}
	}
	
	m_pArrHandle.clear();
	m_ctrlList.LoadInfo();

	ConvterToolBarShow(this, TRUE);
}


//-- 2013-12-13 kjpark@esmlab.com
unsigned WINAPI CESMNetworkDlg::DoConnecttionThread(LPVOID param)
{	
	ThreadData* pThreadData = (ThreadData*)param;
	CESMNetworkDlg* pView = pThreadData->pView;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;
	int nReTryCount = pThreadData->nReTryCount;
	int nGroupIndex = pThreadData->nGroupIndex;
	if( pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}

	if(ESMGetGPUMakeFile())
	{
		if(ESMGetValue(ESM_VALUE_RTSP) == FALSE)
		{
			//jhhan 16-11-16 - 4DMakerEx.net 파일 업데이트 대기
			int nCnt = 0;
			while(pView->GetCheckUpdate())
			{
				ESMLog(5, _T("DoConnecttionThread : pView->GetCheckUpdate / %s : [%d]"), pMRCMgr->GetIP(), nCnt++);
				Sleep(10);

				if(nCnt > 100*5)//최대 5초간 대기
					break;
			}
		}
	}
	
	CString strIP = pMRCMgr->GetIP();
	for( int nRetryIndex =0 ;nRetryIndex < nReTryCount; nRetryIndex++)
	{
		if(pMRCMgr->m_bConnected) 
		{	
			ESMEvent* pMsg	= new ESMEvent;
			pMsg->message	= WM_ESM_FRAME_CONNECTDIVECE;
			pMsg->nParam1 = nGroupIndex;	//jhhan 180612
			::SendMessage(pView->GetCESMNetworkDlgHandle(),WM_ESM, (WPARAM)WM_ESM_FRAME,(LPARAM)pMsg);
			::SendMessage(pView->m_hWnd, WM_NE_RELOADLIST, 0, 0);
			return 0;
		}
		if(!pView->m_ctrlList.ProgramState(strIP)) 
		{
			pView->m_ctrlList.ProgramExecute(strIP);
			Sleep(2500);
			ESMLog(1,_T("[Connect] Program Execute IP =  [%s]"),strIP);	
		}
		pMRCMgr->ConnectToAgent();
		pView->UpdateStatus(pMRCMgr);
		ESMLog(1,_T("[Connect] 4DMaker Connected IP =  [%s]"),strIP);		
		pView->m_ctrlList.SendComMessage(strIP,ID_NETWORK_CAMERA_ON);
		ESMLog(1,_T("[Connect] Camera ON Client IP =  [%s]"),strIP);

		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message	= WM_ESM_FRAME_CONNECTDIVECE;
		pMsg->nParam1 = nGroupIndex;
		::SendMessage(pView->GetCESMNetworkDlgHandle(),WM_ESM, (WPARAM)WM_ESM_FRAME,(LPARAM)pMsg);
 		for(int i = 0 ; i< 100; i++)
 		{
			Sleep(100);
			if( pView->m_bThreadStop == TRUE)
			{
				break;
			}

			if( pMRCMgr->m_nStatus == NET_STATUS_CONNECT)
			{
				break;
			}
 		}
		if( pMRCMgr->m_nStatus == NET_STATUS_CONNECT || pMRCMgr->m_nCamConnect == 0)
			break;	

		if( nReTryCount != nRetryIndex + 1)
		{
			pView->m_ctrlList.ProgramTererminate(strIP);
			Sleep(100);
			pView->m_ctrlList.SendComMessage(strIP,ID_NETWORK_CAMERA_OFF);
			Sleep(1000);
		}
	}
	if( pMRCMgr->m_nStatus != NET_STATUS_CONNECT)
	{
		pMRCMgr = pView->GetRCMgr(strIP);
		if( pMRCMgr != NULL)
			pMRCMgr->m_nStatus = NET_STATUS_WARNNING;
	}
	::SendMessage(pView->m_hWnd, WM_NE_RELOADLIST, 0, 0);

// 	if(nGroupIndex == 0)
// 	{
// 	  	ESMEvent* pMsg = NULL;
// 	  	pMsg = new ESMEvent;
// 	  	pMsg->message = WM_ESM_VIEW_SETNEWESTADJ;
// 	  	::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pMsg);
// 	}

	return 0;
}

unsigned WINAPI CESMNetworkDlg:: DoFileExistCheckThread(LPVOID param)
{

	ThreadData* pThreadData = (ThreadData*)param;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;
	
	//GetFileAttributes
	CString strMovie;
	CString strRecordPath;
	CString strMain;
	CString strIni = _T("C:\\Program Files\\ESMLab\\4DMaker\\config\\4DMaker.info");

	CESMIni ini;
	ini.SetIniFilename(strIni);
	strMovie		=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_CLIENTRAM	);
	strRecordPath	=	ini.GetString(INFO_SECTION_PATH , INFO_PATH_RECORD	);
	
	//Main
	strMain.Format(_T("\\\\"+pMRCMgr->GetIP()+"\\4DMaker"));
	//Client Ram Path
	strMovie.Format(_T("\\\\"+pMRCMgr->GetIP()+strMovie));
	//Clirnt Record Path

	//if(strRecordPath.Find(_T("HOME")))
	//jhhan 16-10-06 /* != -1 */
	if(strRecordPath.Find(_T("HOME")) != -1)
		strRecordPath.Replace(_T("$(HOME)"), _T("\\4DMaker"));	
	else
	{
		int nPos;
		nPos = strRecordPath.Find(_T("\\"));
		strRecordPath.Delete(0,nPos);
	}
	strRecordPath.Format(_T("\\\\"+pMRCMgr->GetIP()+strRecordPath));

	if(GetFileAttributes(strMovie)== -1)
		ESMLog(0,_T(""+strMovie+" - No Shared Folders ----- "));
	else
	{
		CString strMoviePath;
		strMoviePath.Format(_T(""+strMovie + "\\1.txt"));
		CFile file;

		if(!(file.Open(strMoviePath, CFile::modeCreate|CFile::modeWrite)))
			ESMLog(0,_T(""+strMovie+" - No Access Folders ----- "));
		else
			ESMLog(5,_T(""+strMovie+" - Access Folders +++++ "));
		file.Close();
		
		DeleteFile(strMoviePath);
		ESMLog(5,_T(""+strMovie+" - Shared Folders +++++ "));
		
	}

	if(GetFileAttributes(strMain)== -1)
		ESMLog(0,_T(""+strMain+" - No Shared Folders ----- "));
	else
		ESMLog(5,_T(""+strMain+" - Shared Folders +++++ "));

	if(GetFileAttributes(strRecordPath)== -1)
		ESMLog(0,_T(""+strRecordPath+" - No Shared Folders ----- "));
	else
		ESMLog(5,_T(""+strRecordPath+" - Shared Folders +++++ "));

	return 0;
}


unsigned WINAPI CESMNetworkDlg::DoProgramRunThread(LPVOID param)
{	
	ThreadData* pThreadData = (ThreadData*)param;
	CESMNetworkDlg* pView = pThreadData->pView;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;
	if(pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();

		if(!pView->m_ctrlList.ProgramState(strIP)) 
		{
			pView->m_ctrlList.ProgramExecute(strIP);
			//ESMLog(1,_T("[Connect] 4DMarker Execute =  [%s]"),strIP);	
			g_nAgentStatus--;
			ESMLog(1,_T("[Connect] 4DMarker Execute =  [%s] [%d/%d]"),strIP,g_nAgentAll-g_nAgentStatus,g_nAgentAll);
		}
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkDlg::DoProgramDownThread(LPVOID param)
{	
	ThreadData* pThreadData = (ThreadData*)param;
	CESMNetworkDlg* pView = pThreadData->pView;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;
	if(pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();

		if(pView->m_ctrlList.ProgramState(strIP)) 
		{
			pView->m_ctrlList.ProgramTererminate(strIP);
			//ESMLog(1,_T("[Connect] 4DMarker Terminate =  [%s]"),strIP);	
			g_nAgentStatus--;
			ESMLog(1,_T("[Connect] 4DMarker Terminate =  [%s] [%d/%d]"),strIP,g_nAgentAll-g_nAgentStatus,g_nAgentAll);
		}		
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkDlg::DoDownloadThread(LPVOID param)
{	
	ThreadData* pThreadData = (ThreadData*)param;
	CESMNetworkDlg* pView = pThreadData->pView;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;
	if(pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();

		if(pMRCMgr->m_bConnected || pView->m_ctrlList.ProgramState(strIP)) 
			return 0;			

		pView->m_ctrlList.ProgramUpdate(strIP);
		//ESMLog(1,_T("[Connect] 4DMarker Upgrade =  [%s]"),strIP);	
		g_nAgentStatus--;
		ESMLog(1,_T("[Connect] 4DMarker Upgrade =  [%s] [%d/%d]"),strIP,g_nAgentAll-g_nAgentStatus,g_nAgentAll);
		if(g_nAgentStatus == 0)
		{
			ESMSetDump(TRUE);
		}
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkDlg::DoCameraOnThread(LPVOID param)
{	
	ThreadData* pThreadData = (ThreadData*)param;
	CESMNetworkDlg* pView = pThreadData->pView;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;
	if(pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();
		pView->m_ctrlList.SendComMessage(strIP,ID_NETWORK_CAMERA_ON);
		//ESMLog(1,_T("[Connect] Carmera Turn On All =  [%s]"),strIP);	
		g_nAgentStatus--;
		ESMLog(1,_T("[Connect] Carmera Turn On All =  [%s] [%d/%d]"),strIP,g_nAgentAll-g_nAgentStatus,g_nAgentAll);
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkDlg::DoCameraOffThread(LPVOID param)
{	
	ThreadData* pThreadData = (ThreadData*)param;

	CESMNetworkDlg* pView = pThreadData->pView;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;
	if( pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}

	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();
		pView->m_ctrlList.SendComMessage(strIP,ID_NETWORK_CAMERA_OFF);
		g_nAgentStatus--;
		ESMLog(1,_T("[Connect] Carmera Turn Off All =  [%s] [%d/%d]"),strIP,g_nAgentAll-g_nAgentStatus,g_nAgentAll);
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkDlg::DoShutDownThread(LPVOID param)
{
	ThreadData* pThreadData = (ThreadData*)param;
	CESMNetworkDlg* pView = pThreadData->pView;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;
	BOOL nMode = pThreadData->nReTryCount;
	if(pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();

		if (nMode)
		{
			pView->m_ctrlList.SystemShutDown(strIP);
			//ESMLog(1,_T("[Connect] 4DMarker Terminate =  [%s]"),strIP);	
			g_nAgentStatus--;
			ESMLog(1,_T("[Connect] 4DMarker ShutDown =  [%s] [%d/%d]"),strIP,g_nAgentAll-g_nAgentStatus,g_nAgentAll);
		}
		else
		{
			pView->m_ctrlList.SystemShutDown(strIP, FALSE);
			//ESMLog(1,_T("[Connect] 4DMarker Terminate =  [%s]"),strIP);	
			g_nAgentStatus--;
			ESMLog(1,_T("[Connect] 4DMarker Re Boot =  [%s] [%d/%d]"),strIP,g_nAgentAll-g_nAgentStatus,g_nAgentAll);
		}
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}


unsigned WINAPI CESMNetworkDlg::DoBackupProgramRunThread(LPVOID param)
{	
	ThreadData* pThreadData = (ThreadData*)param;
	CESMNetworkDlg* pView = pThreadData->pView;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;
	if(pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();

		if(pView->m_ctrlList.BackupProgramState(strIP) == 0) 
		{
			pView->m_ctrlList.BackupProgramExcute(strIP);	
			g_nAgentStatus--;
			ESMLog(1,_T("[Connect] Backup Program Excute=  [%s] [%d/%d]"),strIP,g_nAgentAll-g_nAgentStatus,g_nAgentAll);
			pMRCMgr->m_bIsExcutingBackupProgram = TRUE;
		}
		else if(pView->m_ctrlList.BackupProgramState(strIP) == 2) 
		{
			g_nAgentStatus--;
			ESMLog(1,_T("[Connect] Not Exist Backup Drive =  [%s] [%d/%d]"),strIP,g_nAgentAll-g_nAgentStatus,g_nAgentAll);			
		}
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkDlg::DoBackupProgramOffThread(LPVOID param)
{	
	ThreadData* pThreadData = (ThreadData*)param;
	CESMNetworkDlg* pView = pThreadData->pView;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;
	if(pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();

		if(pView->m_ctrlList.BackupProgramState(strIP)) 
		{
			pView->m_ctrlList.BackupProgramTerminate(strIP);
			//ESMLog(1,_T("[Connect] 4DMarker Terminate =  [%s]"),strIP);	
			g_nAgentStatus--;
			ESMLog(1,_T("[Connect] Backup Program Terminate =  [%s] [%d/%d]"),strIP,g_nAgentAll-g_nAgentStatus,g_nAgentAll);
			pMRCMgr->m_bIsExcutingBackupProgram = FALSE;
		}
	}
	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkDlg::DoBackupProgramProgressThread(LPVOID param)
{	
	ThreadData* pThreadData = (ThreadData*)param;
	CESMNetworkDlg* pView = pThreadData->pView;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;

	while(pMRCMgr->m_bIsExcutingBackupProgram)
	{	
		if(pThreadData)
		{
			delete pThreadData;
			pThreadData = NULL;
		}
		if(!pView)
			return 0;

		ConvterToolBarShow(pView, FALSE);		
		CString strIP = pMRCMgr->GetIP();

		if(pView->m_ctrlList.BackupProgramState(strIP) == 1) 
		{
			int nFileCount = pView->m_ctrlList.BackupProgramProgress(strIP);

			pMRCMgr->m_nBackupSrcFileCount = nFileCount;
			pView->m_ctrlList.UpdateBackupStatus(strIP);
		}
				
		ConvterToolBarShow(pView, TRUE);
		Sleep(3000);		
	}

	return 0;
}

void CESMNetworkDlg::OnDisConnect()
{
	m_bConnectFlag = FALSE;

	//jhhan 16-09-08
	if(ESMGetLoadRecord())
	{
		ESMSetLoadRecord(FALSE);

		ESMInitTitleBar();
		
		ESMReloadCamList();
	}

	DisConnectAll();

	//ESMSetDeleteFolder(_T(""));
	if(ESMGetValue(ESM_VALUE_SERVERMODE) == ESM_SERVERMODE_MAKING && ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
	{
		ESMSetRecordSync(FALSE);
	}
}

//------------------------------------------------------------------------------
//! @brief		Management RC Manager Array
//! @date		2013-10-08
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//------------------------------------------------------------------------------
CESMRCManager* CESMNetworkDlg::GetRCMgr(int nIndex)
{
	//jhhan 1800814 4DP만 접속시 에러 문구 발생
	//예외 처리 추가
	if(m_arRCServerList.GetCount() > 0)
		return (CESMRCManager*)m_arRCServerList.GetAt(nIndex);
	
	return NULL;
}

CESMRCManager* CESMNetworkDlg::GetRCMgr(CString strIP)
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;

		if(pExist->GetIP() == strIP)
			return pExist;
	}

	return NULL;
}

CESMRCManager* CESMNetworkDlg::GetRCMgrID(int nID)
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist = NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;

		if(pExist->GetID() == nID)
			return pExist;
	}

	return NULL;
}

BOOL CESMNetworkDlg::RCMgrConnect(CString strIP)
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;

		if(pExist->GetIP() == strIP)
		{
			if(!pExist->m_bConnected)
				pExist->ConnectToAgent();
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CESMNetworkDlg::RCMgrDisConnect(CString strIP)
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;

		if(pExist->GetIP() == strIP)
		{
			pExist->Disconnect();
			return TRUE;
		}
	}

	return FALSE;
}
/************************************************************************
 * @method:		CESMNetworkDlg::DestroyAgent
 * @function:	DestroyAgent
 * @date:		2013-10-22
 * @owner		Ryumin (ryumin@esmlab.com)
 * @return:		void
 */
void CESMNetworkDlg::DestroyAgent()
{
	if (m_pRCClient != NULL)
	{
		//  2013-10-21 Ryumin
		m_pRCClient->m_bThreadStop	= TRUE;
		//		m_pRCClient->m_bAccept		= FALSE;
		m_pRCClient->m_bConnected	= FALSE;
		m_pRCClient->Disconnect();
		m_pRCClient->RemoveAllMsg();
		if( m_pRCClient)
		{
			delete m_pRCClient;
			m_pRCClient = NULL;
		}
	}
}

void CESMNetworkDlg::AddMsg(ESMEvent* pMsg)
{
	CESMRCManager* pRCMgr = NULL;
	ESMEvent* pCopyMsg = NULL;

	if(m_bRCMode == ESM_MODESTATE_CLIENT)
	{
		if(m_pRCClient)
			m_pRCClient->AddMsg(pMsg);
		else
		{
			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}
		}
	}
	else
	{
		if (pMsg->message == WM_ESM_LIST_CHECK)
		{
			if(pMsg->nParam1 >= 0)
			{
				int nGroup = pMsg->nParam1;
				pRCMgr = GetRCMgr(nGroup);
				if(pRCMgr)
				{
					pCopyMsg = new ESMEvent;
					memcpy(pCopyMsg, pMsg, sizeof(ESMEvent));
					pRCMgr->AddMsg(pCopyMsg);
				}
			}
			else
			{
				int nAll = GetRCMgrCount();
				while(nAll--)
				{
					pRCMgr = GetRCMgr(nAll);
					if(!pRCMgr)
						break;


					pCopyMsg = new ESMEvent;
					memcpy(pCopyMsg, pMsg, sizeof(ESMEvent));
					pRCMgr->AddMsg(pCopyMsg);
				}
			}

			if(pMsg)
			{
				delete pMsg;
				pMsg = NULL;
			}
		}
		else
		{
			if(pMsg->pDest > 0)
			{
				CDSCItem* pItem = (CDSCItem*)pMsg->pDest;
				pRCMgr = GetRCMgrID(pItem->m_nRemoteID);

				if(pRCMgr)
					pRCMgr->AddMsg(pMsg);
				else
				{
					if(pMsg)
					{
						delete pMsg;
						pMsg = NULL;
					}
				}

			}else
			{
				int nAll = GetRCMgrCount();
				while(nAll--)
				{
					pRCMgr = GetRCMgr(nAll);
					if(!pRCMgr)
						break;

					pCopyMsg = new ESMEvent;
					memcpy(pCopyMsg, pMsg, sizeof(ESMEvent));
					pRCMgr->AddMsg(pCopyMsg);
				}
				if(pMsg)
				{
					delete pMsg;
					pMsg = NULL;
				}
			}
		}
	}
}

void CESMNetworkDlg::DisConnectAll()
{
	//-- Remote Agent
	m_bThreadStop = FALSE;
	if(m_bRCMode == ESM_MODESTATE_CLIENT)
	{
		if(m_pRCClient)
			m_pRCClient->Disconnect();
	}
	//-- Server
	else
	{
		//-- Get List
		int nAll = GetRCMgrCount();
		CESMRCManager* pExist = NULL;
		while(nAll--)
		{
			pExist = GetRCMgr(nAll);
			if(pExist)
			{
				//-- 2013-10-08 hongsu@esmlab.com
				//-- Connect from Local(Server) -> to Agent
				pExist->Disconnect();
			}
		}	
	}
}
void CESMNetworkDlg::RemoveAllMsg()
{
	if (m_pRCClient != NULL)
		m_pRCClient->RemoveAllMsg();

	CESMRCManager*	pRCMgr	= NULL;
	ESMEvent*		pMsg	= NULL;
	int nCnt = GetRCMgrCount();
	for (int nIdx = 0; nIdx < nCnt; ++nIdx)
	{
		pRCMgr = GetRCMgr(nIdx);
		if (!pRCMgr) continue;

		pRCMgr->RemoveAllMsg();
	}
}

void CESMNetworkDlg::SetNetStatus(int nRemoteIp, int nStatus)
{
	CESMRCManager*	pRCMgr	= NULL;
	if( nStatus == 1)
	{
		int nCnt = GetRCMgrCount();
		for (int nIdx = 0; nIdx < nCnt; ++nIdx)
		{
			pRCMgr = GetRCMgr(nIdx);
			if(pRCMgr->GetID() == nRemoteIp)
			{
				pRCMgr->m_bConnected = TRUE;
				pRCMgr->m_nStatus = NET_STATUS_CONNECT;
			}
		}
	}
}

void CESMNetworkDlg::GetClientDiskSize()
{
	ESMEvent* pMsg = NULL;
	pMsg = new ESMEvent();
	pMsg->message = WM_ESM_NET_GET_DSIKSIZE;
	
	::PostMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
}

//-- 2013-12-13 kjpark@esmlab.com
//-- Network Program connect Check
BOOL CESMNetworkDlg::RCMgrIsConnected(CString strIP)
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;

		if(pExist->GetIP() == strIP)
		{			
			if(!pExist->m_bConnected)
				return TRUE;
		}
	}

	return FALSE;
}

void CESMNetworkDlg::CheckToAgentAlive(int ip)
{


}

void CESMNetworkDlg::StartCheckToAgentAlive()
{

	
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;
		//pExist->m_bConnectStatusCheck = TRUE;
		pExist->ConnectStatusCheck();
	}
	
}
void CESMNetworkDlg::StopCheckToAgentAlive()
{
	int nAll = GetRCMgrCount();
	CESMRCManager* pExist =NULL;
	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist)
			break;
		pExist->m_bConnectStatusCheck = FALSE;
		
	}
	ESMLog(1,_T("ConnectStatusCheck Stop"));
}

void CESMNetworkDlg::AllCameraConnect()
{
	OnConnect();
}

void CESMNetworkDlg::AllCameraOff()
{
	On4DMakerAllCameraOff();
}

void CESMNetworkDlg::test1()
{
	//jhhan 16-12-20
	//기능 사용 제외
	return;

	if(ESMGetValue(ESM_VALUE_NET_MODE) != ESM_MODESTATE_CLIENT)
	{
		int nCount = GetRCMgrCount();
		CString strSendIP;
		CESMRCManager* pRCMgr = NULL;

		while(nCount--)
		{
			pRCMgr = (CESMRCManager*)m_arRCServerList.GetAt(nCount);
			strSendIP = pRCMgr->GetSendIP();
			if(strSendIP.GetLength())
				pRCMgr->StartSendFrame();
		}
	}
	else
	{
		int nPort = 8281;
		CString SetIP;
		SetIP.Format(_T("192.168.0.82"));

		BOOL bRes = m_pRCClient->ConnectToKTServer(SetIP, nPort);
	}
}

void CESMNetworkDlg::test2()
{
	//jhhan 16-12-20
	//기능 사용 제외
	return;

	if(ESMGetValue(ESM_VALUE_NET_MODE) != ESM_MODESTATE_CLIENT)
	{
		int nCount = GetRCMgrCount();
		CString strSendIP;
		CESMRCManager* pRCMgr = NULL;

		while(nCount--)
		{
			pRCMgr = (CESMRCManager*)m_arRCServerList.GetAt(nCount);
			strSendIP = pRCMgr->GetSendIP();
			if(strSendIP.GetLength())
				pRCMgr->StopSendFrame();
		}
	}
	else
		m_pRCClient->DisConnectToKTServer();
}

void CESMNetworkDlg::test3()
{
	//jhhan 16-12-20
	//기능 사용 제외
	return;

	KSSHeader data;
	data.command[0] = 0x6f;
	data.command[1] = 0x70;
	data.command[2] = 0x65;
	data.command[3] = 0x6e;
	m_pRCClient->SendFrameData(&data);
}

void CESMNetworkDlg::DoProcessUpdateAll()
{
	ConvterToolBarShow(this, FALSE);

	int nAll = GetRCMgrCount();
	//-- 2014-09-08 hongsu@esmlab.com
	//-- g_nAgentExAll
	g_nAgentAll	 = nAll;
	g_nAgentStatus  = nAll;

	//jhhan 16-11-16 - 4DMakerEx.net 파일 업데이트 대기 Flag
	SetCheckUpdate(TRUE);

	CESMRCManager* pExist = NULL;
	
	//190208 hjcho
	FILE* out = NULL;

	if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE) && ESMGetValue(ESM_VALUE_RTSP))
		out = fopen("M:\\temp.bat","w");

	while(nAll--)
	{
		pExist = GetRCMgr(nAll);
		if(!pExist->m_bConnected) 
			pExist->m_nStatus = NET_STATUS_DOWNLOAD;

		if( pExist)
		{
			if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE))
			{
				if(ESMGetValue(ESM_VALUE_RTSP))
				{
#if 0
					CString strFileName = _T("CameraList.csv");
					CString strHomePath;
					strHomePath.Format(_T("%s\\"),ESMGetPath(ESM_PATH_SETUP));

					CString strAgentPath;
					strAgentPath.Format(_T("\\\\%s\\4DMaker\\Setup\\"),pExist->GetIP());

					CESMFileOperation fo;
					if(fo.IsFileExist(strAgentPath))
					{
						fo.Delete(strAgentPath);
					}
					fo.Copy(strHomePath,strAgentPath,FALSE);
#else
					CString strFileName = _T("CameraList.csv");

					//delete cameralist
					char strTemp[1024];
					sprintf(strTemp,"del \"\\\\%S\\4DMaker\\Setup\\%S\"\n",pExist->GetIP(),strFileName);
					fprintf(out,strTemp);
					
					//copy cameralist
					sprintf(strTemp,"copy \"%S\\%S\" \"\\\\%S\\4DMaker\\Setup\\%S\"\n",
						ESMGetPath(ESM_PATH_SETUP),strFileName,pExist->GetIP(),strFileName);
					fprintf(out,strTemp);
					
					/*CString strHomePath;
					strHomePath.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_SETUP),strFileName);

					CString strAgentPath;
					strAgentPath.Format(_T("\\\\%s\\4DMaker\\Setup\\"),pExist->GetIP());
					
					CESMFileOperation fo;
					if(fo.IsFileExist(strAgentPath+strFileName))
					{
						fo.Delete(strAgentPath+strFileName);
					}
					fo.Copy(strHomePath,strAgentPath,FALSE);*/
#endif
				}
			}

			ThreadData* pThreadData;
			pThreadData = new ThreadData;
			pThreadData->pView = this;
			pThreadData->pMRCMgr = pExist;
			pThreadData->nReTryCount = 1;
			HANDLE hHandle = NULL;
			hHandle = (HANDLE) _beginthreadex(NULL, 0, DoProcessUpdateThread, (void *)pThreadData, 0, NULL);
			CloseHandle(hHandle);
		}
	}
	//190208 hjcho
	if(ESMGetValue(ESM_VALUE_GPU_MAKE_FILE) && ESMGetValue(ESM_VALUE_RTSP))
	{
		fclose(out);
		WinExec(("M:\\temp.bat"),SW_HIDE);
		WinExec("del \"M:\\temp.bat\"",SW_HIDE);
	}

	m_ctrlList.LoadInfo();
	ConvterToolBarShow(this, TRUE);
}

unsigned WINAPI CESMNetworkDlg::DoProcessUpdateThread(LPVOID param)
{	
	ThreadData* pThreadData = (ThreadData*)param;
	CESMNetworkDlg* pView = pThreadData->pView;
	CESMRCManager* pMRCMgr = pThreadData->pMRCMgr;
	if(pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}
	if(!pView)
		return 0;

	ConvterToolBarShow(pView, FALSE);
	if(pMRCMgr)			
	{	
		CString strIP = pMRCMgr->GetIP();

		/*if(pMRCMgr->m_bConnected || pView->m_ctrlList.ProgramState(strIP)) 
		return 0;*/			

		pView->m_ctrlList.ProcessUpdate(strIP);
		//ESMLog(1,_T("[Connect] 4DA Upgrade =  [%s]"),strIP);	
		g_nAgentStatus--;
		//ESMLog(1,_T("[Connect] 4DMarkerEx Upgrade =  [%s] [%d/%d]"),strIP,g_nAgentAll-g_nAgentStatus,g_nAgentAll);
	}
	//jhhan 16-11-16 - 4DMakerEx.net 파일 업데이트 대기 Flag
	if(g_nAgentAll-g_nAgentStatus == g_nAgentAll)
		pView->SetCheckUpdate(FALSE);

	ConvterToolBarShow(pView, TRUE);

	return 0;
}

unsigned WINAPI CESMNetworkDlg::DoProcessCheck(LPVOID param)
{	
	ThreadData* pThreadData = (ThreadData*)param;
	CESMNetworkDlg* pView = pThreadData->pView;

	if (pThreadData)
	{
		delete pThreadData;
		pThreadData = NULL;
	}

	int nCount = pView->GetRCMgrCount();
	CString strSendIP;
	CESMRCManager* pRCMgr = NULL;

	while(nCount--)
	{
		pRCMgr = (CESMRCManager*)pView->m_arRCServerList.GetAt(nCount);

		if(pView->m_nTimerCount > 10)
		{
			pRCMgr->GetDiskSize();
			pView->m_nTimerCount = 0;
		}
		else
			pView->m_nTimerCount++;

		strSendIP = pRCMgr->GetIP();
		pView->m_ctrlList.GetProcState(strSendIP);
		//ESMLog(5, _T("DoProcessCheck"));
	}
	pView->m_bProcCheck = TRUE;
	return 0;
}

void CESMNetworkDlg::OnTimer(UINT_PTR nIDEvent) 
{
	switch(nIDEvent)
	{
	case PROC_CHECK:
		{
			if (m_bProcCheck)
			{
				m_bProcCheck = FALSE;

				ThreadData* pThreadData;
				pThreadData = new ThreadData;
				pThreadData->pView = this;
				HANDLE hHandle = NULL;
				hHandle = (HANDLE) _beginthreadex(NULL, 0, DoProcessCheck, (void *)pThreadData, 0, NULL);
				CloseHandle(hHandle);
			}
			break;
		}
	default:
		break;
	}
	CDialog::OnTimer(nIDEvent);
}