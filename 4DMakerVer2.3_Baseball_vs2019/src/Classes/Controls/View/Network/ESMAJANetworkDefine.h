#pragma once
#include "ESMDefine.h"
struct AJASendGPUFrameInfo
{
	AJASendGPUFrameInfo()
	{
		memset(strPath,0,MAX_PATH*sizeof(char));
		nMakingIndex	= -1;
		nReverse		= 0;
		nFrameRate		= 33;
		//pnArrIndexInfo	= NULL;
	}
	TCHAR strPath[MAX_PATH];
	//vector<int>*pnArrIndexInfo;
	int		nStartIndex;
	int		nEndIndex;
	int		nZoomRatio;
	int		nPosX;
	int		nPosY;
	int		nFrameRate;
	int		nReverse;
	int		nMakingIndex;
};
struct AJANetworkHeader
{
	AJANetworkHeader()
	{
		memset(protocol, 0, sizeof(char) * 8);
		command = -1;
		param1 = -1;
		param2 = -1;
		param3 = -1;
		bodysize = 0;
	}
	char protocol[8]; //always "RCP1.0"
	DWORD command;
	DWORD param1;
	DWORD param2;
	DWORD param3;
	DWORD bodysize;
};
struct AJAMsg
{
	AJAMsg()
	{
		message = 0;
		nParam1 = 0;
		nParam2 = 0;
		nParam3 = 0;
		pParam	= NULL;
		pDest	= NULL;
		pParent = NULL;
	}
	UINT	message;
	UINT	nParam1;
	UINT	nParam2;
	UINT	nParam3;
	LPARAM	pParam;
	LPARAM	pDest;	// Multi Managing
	LPARAM	pParent; // 4DMaker DSCItem Pointer
};
//AJA -> 4DC
enum{
	AJA_BOARDSTATE = 0,
	AJA_MAKING_START,
	AJA_MAKING_FINISH,
	AJA_ENCODING_START,
	AJA_ENCODING_FINISH,
	AJA_PLAY_SCREEN,
};

//4DC -> AJA
enum{
	F4DC_PROGRAM_STATE = 0,
	F4DC_MAKING_START,
	F4DC_MAKING_TOTALCNT,
	F4DC_ADJUST_SEND,
	F4DC_GPUFRAMEPATH_SEND,
	F4DC_CPUFRAME_INFO,
	F4DC_CPUFRAME_MAKE_FINISH,
	F4DC_AUTO_ADD,
	F4DC_SEND_CPU_FILE,
};