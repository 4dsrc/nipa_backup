////////////////////////////////////////////////////////////////////////////////
//
//	ESMNetworkListCtrl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-10-08
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"

#include "SdiDefines.h"
#include "ESMReportCtrl.h"
#include "ESMTCPSocket.h"
#include "ESMHeaderCtrl.h"
#include <vector>

class CESMRCManager;
class CESMNetworkDlg;

enum RCDCommand
{
	RCDCMD_CAMERA_ON = 9900,
	RCDCMD_CAMERA_OFF, 
	RCDCMD_4DMAKER_ON, 
	RCDCMD_4DMAKER_OFF
};

enum LISTColumn
{
	LIST_CODE = 0,
	LIST_STATUS,
	LIST_ADDRESS,
	LIST_DISK,
	LIST_RAMDISK,
	LIST_GPU,
	LIST_NETSPEED,	
	LIST_BACKUPSTATUS,
	LIST_4DAP
};

typedef struct _TGPHeader
{
	char szProtocol[8];				// VD_PROTOCOL_STR/"SRM1.0"
	char szCommand[8];				// "REQT"/VD_COMMAND_STR_RESP/"NOTI"
	DWORD dwPayloadSize;
} TGPHeader, *PTGPHeader;

typedef struct _VDPayloadHeader
{
	char szName[8];					// VD_NAME_STR_DAEMON/VD_NAME_STR_CONSOLE/"AGENT"
	char szControl[10];				// VD_CONTROL_STR_START/"STOP"/"GETVER"/"FILEUPDATE"/"UNITUPDATE"/"POLL"/"PUSH"/"FINISH"
	char szResult[8];				// "SUCCESS"/"FAIL"
	DWORD dwBodySize;
} VDPayloadHeader, *PVDPayloadHeader;

typedef struct _VDPacket
{
	TGPHeader TgpHeader;
	VDPayloadHeader VdPayloadHeader;
	char szIPAddr[20];
} VDPacket, *PVDPacket;

class CESMNetworkListCtrl : public CListCtrl
{
public:
	CESMNetworkListCtrl(void);
public:
	~CESMNetworkListCtrl(void);
	void Clear();
	void Init(CESMNetworkDlg* pParent);
	void Save(CESMNetworkDlg* pParent);
	void LoadInfo(BOOL bCreate = FALSE);
	void SetStatus(int Row, CString strStatus);
	void SetUseCamStatusColor(BOOL bUse);

private:
	BOOL m_bBKColor;
	CESMNetworkDlg* m_pParent;
	CESMTCPSocket*	m_pRCDSocket;
	HANDLE	m_hThreadHandle;
	CRITICAL_SECTION m_CrSockket;
	CString m_strStatusInfo[NET_STATUS_INFO_CNT];

	CESMHeaderCtrl m_HeaderCtrl;
	BOOL m_bUseCamStatusColor;

public:
	void UpdateStatus(CESMRCManager* pRCMgr);
	void UpdateDiskSize(CESMRCManager* pRCMgr);

protected:
	CString strIP;
	CString strPort;
	CListCtrl m_IPList;
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);

	void ProgramExecute(CString strIP);
	void ProgramUpdate(CString strIP);
	void ProgramTererminate(CString strIP);
	BOOL ProgramState(CString strIP);
	void SendComMessage(CString strHostIP,LONG nItem);
	void SystemShutDown(CString strIP, BOOL bMode = TRUE);
	//-- 2013-12-13 kjpark@esmlab.com
	//-- Network Program connect Check
	void ClientRunNConnect(CString strIP);	
	static unsigned WINAPI OpserveThread(LPVOID param);

	void AddIpAgent();
	void ModifyAgent();
	void DeleteAgent();

	void ServiceSockSend(CString strHostIP,int nPort, char* pBuf, int BufSize, char* pRecvBuf);
	BOOL SendUpdateFile(CString strFileName, int nPort ,CString strIP);
	void SendAllClient(char* pbuf,int nSize);
	int recvn(SOCKET s, char *buf, int len, int flags);
	BOOL GetPowerState(CString strIP);

	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);

	BOOL SendProcessFile(CString strFileName, int nPort ,CString strIP, CString strPath);
	void ProcessUpdate(CString strIP);
	
	//-- 2016-12-21 ymlee@esmlab.com
	//-- 4DMaker State Check
	void GetProcState(CString strIP);
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);

	//20171130 wgkim
	BOOL m_bIsBackupProgramExecute;	
	
	void BackupProgramExcute(CString strIP);
	int  BackupProgramState(CString strIP);
	void BackupProgramTerminate(CString strIP);
	int BackupProgramProgress(CString strIP);
	void UpdateBackupProgramProgress(CESMRCManager* pRCMgr);	

	void UpdateBackupStatus(CString strIP);
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	virtual void PreSubclassWindow();

	void RemoteAgent(CString strIp);	
};
