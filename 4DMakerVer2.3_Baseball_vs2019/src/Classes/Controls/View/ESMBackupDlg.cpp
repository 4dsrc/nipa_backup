// D:\Source\4DMakerVer2.1\src\Classes\Dialog\ESMBackupDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "4DMaker.h"
#include "ESMBackupDlg.h"
#include "afxdialogex.h"
#include "ESMFunc.h"
#include "ESMFileOperation.h"
#include "ESMIni.h"
#include "ESMUtil.h"

// Thread Parameter
typedef struct tagTHREADPARAMS { 
	CESMBackupDlg *com;
	CString strSrc;
	CString strDest;
} THREADPAPAMS; 


// CESMBackupDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CESMBackupDlg, CDialogEx)

CESMBackupDlg::CESMBackupDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMBackupDlg::IDD, pParent)
	,m_nTotalFileCount(0)
	,m_nCurrentFileCount(0)
	,m_strProfile(_T(""))
{

}

CESMBackupDlg::~CESMBackupDlg()
{
}

void CESMBackupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_TOTAL_COUNT, m_stTotalCount);
	DDX_Control(pDX, IDC_STATIC_COPY_COUNT, m_stCopyCount);
	DDX_Control(pDX, IDC_STATIC_LOG, m_stLog);
}

BEGIN_MESSAGE_MAP(CESMBackupDlg, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_MESSAGE(WM_ESM_OPT_MSG,	OnESMOPTMsg)
	ON_BN_CLICKED(IDOK, &CESMBackupDlg::OnBnClickedOk)
	ON_WM_TIMER()
END_MESSAGE_MAP()


BOOL CESMBackupDlg::Create( CWnd* pParentWnd)
{

	return CDialogEx::Create(CESMBackupDlg::IDD, pParentWnd);
}

BOOL CESMBackupDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	GetIPList();
	GetDlgItem(IDOK)->ShowWindow(FALSE);

	m_stTotalCount.SetWindowTextW(_T("0"));
	m_stCopyCount.SetWindowTextW(_T("0"));
	m_stLog.SetWindowTextW(_T("File Backup..."));

	return TRUE;  // return TRUE unless you set the focus to a control
}

void CESMBackupDlg::GetIPList()
{	
	CString strFile;
	strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_CONNECT);	
	CESMIni ini;

	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
	{
		CString csLocal = ESMUtil::GetLocalIPAddress();
		m_IPList.push_back(csLocal);
		return;
	}
	
	if(ini.SetIniFilename (strFile))
	{
		CString strIPSection;
		CString strIP;
		int nIndex = 0;
		while(1)
		{
			strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);			
			strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);	
			if(!strIP.GetLength())
				break;		
			nIndex++;	
			m_IPList.push_back(strIP);
		}
		if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_SERVER)
		{
			CString csLocal = ESMUtil::GetLocalIPAddress();
			m_IPList.push_back(csLocal);
		}

		//jhhan 170808 - NetworkEx 추가
		nIndex = 0;
		strFile.Format(_T("%s\\%s"),ESMGetPath(ESM_PATH_CONFIG),ESM_4DMAKER_EX);	
		if(ini.SetIniFilename(strFile))
		{
			while(1)
			{
				strIPSection.Format(_T("%s_%d"),INFO_RC_IP,nIndex);			
				strIP = ini.GetString(INFO_SECTION_RC, strIPSection	);	
				if(!strIP.GetLength())
					break;		
				nIndex++;	
				m_IPList.push_back(strIP);
			}
		}
	}
}

void CESMBackupDlg::BackupRecordFile(CString strPath)
{
	m_nTotalFileCount = 0;
	m_nCurrentFileCount = 0;
	
	CString str, strSrc;
	m_strProfile = strPath;

	str.Format(_T("%s Backup"), strPath);
	m_stLog.SetWindowTextW(str);
	GetFileCount(strPath);

	CString csLocal = ESMUtil::GetLocalIPAddress();
	for(int i =0; i < m_IPList.size(); i++)			
	{
		if(csLocal == m_IPList.at(i))
			continue;

		strSrc.Format(_T("\\\\"+m_IPList.at(i)+"\\"+ESMGetManageMent(ESM_MANAGEMENT_SEL_PATH)+"\\"+strPath));
		//ESMLog(5, strSrc);

		THREADPAPAMS *threadparm = new THREADPAPAMS();

		threadparm->strSrc = strSrc;
		threadparm->strDest = ESMGetManageMent(ESM_MANAGEMENT_SAVE_PATH);
		threadparm->com = this;

		HANDLE hHandle = NULL;
		hHandle = (HANDLE)_beginthreadex(NULL, 0, FileCopyThread, (THREADPAPAMS *)threadparm, 0, NULL);
	}

	//jhhan 
	if(m_str4DM.IsEmpty() != TRUE)
	{
		THREADPAPAMS *threadparm = new THREADPAPAMS();

		threadparm->strSrc = m_str4DM;
		threadparm->strDest = ESMGetManageMent(ESM_MANAGEMENT_SAVE_PATH);
		threadparm->com = this;

		HANDLE hHandle = NULL;
		hHandle = (HANDLE)_beginthreadex(NULL, 0, File4DMCopyThread, (THREADPAPAMS *)threadparm, 0, NULL);
	}

	

	SetTimer(WM_ESM_OPT_COPY_TIMER, 1000, NULL);
}

void CESMBackupDlg::GetFileCount(CString strPath)
{

	CString strSrc;
	CString csLocal = ESMUtil::GetLocalIPAddress();
	for(int i =0; i < m_IPList.size();i++)			
	{
		if(csLocal == m_IPList.at(i))
			continue;

		strSrc.Format(_T("\\\\"+m_IPList.at(i)+"\\"+ESMGetManageMent(ESM_MANAGEMENT_SEL_PATH)+"\\"+strPath));

		THREADPAPAMS *threadparm = new THREADPAPAMS();

		threadparm->strSrc = strSrc;
		threadparm->strDest = ESMGetManageMent(ESM_MANAGEMENT_SAVE_PATH);
		threadparm->com = this;

		HANDLE hHandle = NULL;
		hHandle = (HANDLE)_beginthreadex(NULL, 0, FileCountThread, (THREADPAPAMS *)threadparm, 0, NULL);
	}

}

BOOL CESMBackupDlg::DestroyWindow()
{

	return CDialogEx::DestroyWindow();
}


void CESMBackupDlg::PostNcDestroy()
{
	delete this;
	//CDialogEx::PostNcDestroy();
}

unsigned WINAPI CESMBackupDlg::FileCountThread(LPVOID param)
{

	THREADPAPAMS *pThreadParams = (THREADPAPAMS *)param; 
	CESMBackupDlg *com = pThreadParams->com;
	CESMFileOperation fo;
	int count = 0;
	
	fo.GetFileCount(pThreadParams->strSrc, count);
	
	if(com->m_nTotalFileCount >= 0)
	{
		com->m_nTotalFileCount += count;
		count = 0;			

		CString str;
		str.Format(_T("%d"), com->m_nTotalFileCount);
		com->m_stTotalCount.SetWindowTextW(str);
	}
	
	return 0;
}

unsigned WINAPI CESMBackupDlg::FileCopyThread(LPVOID param)
{

	THREADPAPAMS *pThreadParams = (THREADPAPAMS *)param; 
	CESMBackupDlg *com = pThreadParams->com;
	CESMFileOperation fo;
	CString filelog;
	bool complete = false;
	
	//ESMLog(5, _T("FileCopy - %s"), pThreadParams->strSrc);

	fo.SetOverwriteMode(TRUE);
	fo.m_iFileCount = 0;

	complete = fo.Copy(pThreadParams->strSrc, pThreadParams->strDest);

	if(complete)
	{
		filelog.Format(_T("File Backup -  "+ pThreadParams->strSrc+" : Copy Complete"));
	}
	else
	{
		filelog.Format(_T("File Backup -  "+ pThreadParams->strSrc+" : Copy Error"));
		//ESMLog(5,filelog);
	}

	/*
	//jhhan
	if(com->m_str4DM.IsEmpty() != TRUE)
	{
		//4DM 복사 및 카운트 해제.
		com->m_str4DM.Empty();

		return 0;
	}
	*/
	
	//Progress Bar Refresh
	com->m_nCurrentFileCount += fo.m_iFileCount;
	CString str;
	str.Format(_T("%d"), com->m_nCurrentFileCount);
	com->m_stCopyCount.SetWindowTextW(str);
	
	if(com->m_nCurrentFileCount == com->m_nTotalFileCount)
	{
#if 0
	 	::PostMessage(com->GetSafeHwnd(), WM_ESM_OPT_MSG, (WPARAM)WM_ESM_OPT_COPY_SUCCESS, NULL );
#else	
		//jhhan
		if(com->m_nTotalFileCount == 0)
			::PostMessage(com->GetSafeHwnd(), WM_ESM_OPT_MSG, (WPARAM)WM_ESM_OPT_COPY_NOT_FOUND, NULL );
		else
			::PostMessage(com->GetSafeHwnd(), WM_ESM_OPT_MSG, (WPARAM)WM_ESM_OPT_COPY_SUCCESS, NULL );
#endif
	}
	else
	{
		::PostMessage(com->GetSafeHwnd(), WM_ESM_OPT_MSG, (WPARAM)WM_ESM_OPT_COPY_FAIL, NULL );
	}
	return 0;
}

LRESULT CESMBackupDlg::OnESMOPTMsg(WPARAM wParam, LPARAM lParam)
{
	CString str;
	switch((int)wParam)
	{
	case WM_ESM_OPT_COPY_SUCCESS:
		str.Format(_T("%s Backup Complete!"), m_strProfile);
		m_stLog.SetWindowTextW(str);
		GetDlgItem(IDOK)->ShowWindow(TRUE);
		break;
	case WM_ESM_OPT_COPY_FAIL:
		str.Format(_T("%s Backup Fail!"), m_strProfile);
		m_stLog.SetWindowTextW(str);
		break;
	
		//jhhan
	case WM_ESM_OPT_COPY_NOT_FOUND:
		str.Format(_T("%s Backup Not Found"), m_strProfile);
		m_stLog.SetWindowTextW(str);
		break;
	default: break;
	}
	return TRUE;
}

void CESMBackupDlg::OnBnClickedOk()
{
	DestroyWindow();
	//CDialogEx::OnOK();
}

void CESMBackupDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == WM_ESM_OPT_COPY_TIMER)
	{
		if(m_nTotalFileCount !=0)
		{
			KillTimer(WM_ESM_OPT_COPY_TIMER);
			CESMFileOperation fo;
			CString str, strDest;
			int count;
			strDest.Format(_T("" + ESMGetManageMent(ESM_MANAGEMENT_SAVE_PATH) +"\\" + m_strProfile));
			
			count = 0;
			fo.GetFileCount(strDest, count);

			if(m_nTotalFileCount >= count)
			{
				str.Format(_T("%d"), count);
				m_stCopyCount.SetWindowTextW(str);
			}
			
			if( m_nTotalFileCount != count)
				SetTimer(WM_ESM_OPT_COPY_TIMER, 500, NULL);
		}
	}else if(nIDEvent == WM_ESM_OPT_LOG_COPY_TIMER)
	{
		if(m_nTotalFileCount !=0)
		{
			KillTimer(WM_ESM_OPT_LOG_COPY_TIMER);
			CESMFileOperation fo;
			CString str, strDest;
			int count;
			strDest.Format(_T("" +  m_strProfile));

			count = 0;
			fo.GetFileCount(strDest, count);

			if(m_nTotalFileCount >= count)
			{
				str.Format(_T("%d"), count);
				m_stCopyCount.SetWindowTextW(str);
			}

			if( m_nTotalFileCount != count)
				SetTimer(WM_ESM_OPT_LOG_COPY_TIMER, 500, NULL);
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}

//jhhan
void CESMBackupDlg::Backup4DMFile(CString strPath)
{
	m_str4DM = strPath;

}

unsigned WINAPI CESMBackupDlg::File4DMCopyThread(LPVOID param)
{

	THREADPAPAMS *pThreadParams = (THREADPAPAMS *)param; 
	CESMBackupDlg *com = pThreadParams->com;
	CESMFileOperation fo;
	CString filelog;
	bool complete = false;

	//ESMLog(5, _T("FileCopy - %s"), pThreadParams->strSrc);

	fo.SetOverwriteMode(TRUE);
	fo.m_iFileCount = 0;

	complete = fo.Copy(pThreadParams->strSrc, pThreadParams->strDest);

	if(complete)
	{
		filelog.Format(_T("File Backup -  "+ pThreadParams->strSrc+" : Copy Complete"));
	}
	else
	{
		filelog.Format(_T("File Backup -  "+ pThreadParams->strSrc+" : Copy Error"));
		ESMLog(5,filelog);
	}

	return 0;
}

void CESMBackupDlg::GetFileCount(CString strPath, CString strDest)
{

	CString strSrc;

	for(int i =0; i < m_IPList.size();i++)			
	{
		strSrc.Format(_T("\\\\"+m_IPList.at(i)+"\\"+_T("4DMaker\\log\\")+/*m_IPList.at(i)+*/"\\"+strPath/*+"\\Log"*/));

		THREADPAPAMS *threadparm = new THREADPAPAMS();

		CString strDestPath = strDest;
		strDestPath.Append(_T("\\")+m_IPList.at(i));
		
		threadparm->strSrc = strSrc;
		threadparm->strDest = strDestPath;
		threadparm->com = this;

		HANDLE hHandle = NULL;
		hHandle = (HANDLE)_beginthreadex(NULL, 0, FileCountThread, (THREADPAPAMS *)threadparm, 0, NULL);
	}

	//170809 jhhan 170807 이전 로그
	for(int i =0; i < m_IPList.size();i++)			
	{
		strSrc.Format(_T("\\\\"+m_IPList.at(i)+"\\"+_T("4DMaker\\log\\")+m_IPList.at(i)+"\\"+strPath+"\\Log"));

		THREADPAPAMS *threadparm = new THREADPAPAMS();

		CString strDestPath = strDest;
		strDestPath.Append(_T("\\")+m_IPList.at(i));

		threadparm->strSrc = strSrc;
		threadparm->strDest = strDestPath;
		threadparm->com = this;

		HANDLE hHandle = NULL;
		hHandle = (HANDLE)_beginthreadex(NULL, 0, FileCountThread, (THREADPAPAMS *)threadparm, 0, NULL);
	}

}

void CESMBackupDlg::BackupLogFile(CString strPath, CString strDest)
{
	m_nTotalFileCount = 0;
	m_nCurrentFileCount = 0;

	CString str, strSrc;
	m_strProfile = strDest;

	str.Format(_T("%s Backup"), strPath);
	m_stLog.SetWindowTextW(str);
	GetFileCount(strPath, strDest);

	for(int i =0; i < m_IPList.size(); i++)			
	{
		strSrc.Format(_T("\\\\"+m_IPList.at(i)+"\\"+_T("4DMaker\\log\\")+/*m_IPList.at(i)+*/"\\"+strPath/*+"\\Log"*/));
		//ESMLog(5, strSrc);

		THREADPAPAMS *threadparm = new THREADPAPAMS();

		CString strDestPath = strDest;
		strDestPath.Append(_T("\\")+m_IPList.at(i));

		threadparm->strSrc = strSrc;
		threadparm->strDest = strDestPath;
		threadparm->com = this;

		HANDLE hHandle = NULL;
		hHandle = (HANDLE)_beginthreadex(NULL, 0, FileCopyThread, (THREADPAPAMS *)threadparm, 0, NULL);
	}

	//170809 jhhan 170807 이전 로그
	for(int i =0; i < m_IPList.size(); i++)			
	{
		strSrc.Format(_T("\\\\"+m_IPList.at(i)+"\\"+_T("4DMaker\\log\\")+m_IPList.at(i)+"\\"+strPath+"\\Log"));
		//ESMLog(5, strSrc);

		THREADPAPAMS *threadparm = new THREADPAPAMS();

		CString strDestPath = strDest;
		strDestPath.Append(_T("\\")+m_IPList.at(i));

		threadparm->strSrc = strSrc;
		threadparm->strDest = strDestPath;
		threadparm->com = this;

		HANDLE hHandle = NULL;
		hHandle = (HANDLE)_beginthreadex(NULL, 0, FileCopyThread, (THREADPAPAMS *)threadparm, 0, NULL);
	}


	SetTimer(WM_ESM_OPT_LOG_COPY_TIMER, 1000, NULL);
}