////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyAnalysisView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TGLatencyAnalysisView.h"
#include "resource.h"
#include "TestStepDef.h"
#include "XnsMediaInterface.h"

CTGLatencyAnalysisView::CTGLatencyAnalysisView(CWnd* pParent /*=NULL*/)
: CDialog(CTGLatencyAnalysisView::IDD, pParent)	
{	
	m_pTGGraph	= NULL;	
	m_nGOPCount		= 0;
	m_tTime			= 0;
	m_bStart		= FALSE;
}

CTGLatencyAnalysisView::~CTGLatencyAnalysisView() 
{	
}

void CTGLatencyAnalysisView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTGLatencyAnalysisView)
	//}}AFX_DATA_MAP	
}


BEGIN_MESSAGE_MAP(CTGLatencyAnalysisView, CDialog)
	//{{AFX_MSG_MAP(CTGLatencyAnalysisView)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()	
	ON_WM_CLOSE()	
END_MESSAGE_MAP()


BOOL CTGLatencyAnalysisView::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}	

	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();
	m_pTGGraph  = new CTGLatencyAnalysisGraph();
	m_pTGGraph->Create(NULL, _T(""), WS_VISIBLE|WS_CHILD, CRect(0,0,0,0), this, ID_VIEW_BAR_LATENCY);
	m_dwSequence = 0;

	return TRUE;
}

void CTGLatencyAnalysisView::OnClose()
{
	CDialog::OnClose();
}

void CTGLatencyAnalysisView::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	RepositionBars(0,0xFFFF,0);

	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;
	GetClientRect(m_rcClientFrame);
	//-- RESIZE CONTROL
	m_pTGGraph->MoveWindow(m_rcClientFrame);
}

void CTGLatencyAnalysisView::Clear()
{
	m_nGOPCount = 0;
	m_bStart	= FALSE;

	m_pTGGraph->ClearViewer();
}

void CTGLatencyAnalysisView::InitImageFrameWnd()
{
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);
}


BOOL CTGLatencyAnalysisView::InsertToDB(CString srtTime, int nFrame)
{
	/*
	CTGDBFrameAnalysis* pInfo = new CTGDBFrameAnalysis(m_dwSequence++, srtTime, nFrame);
	if(!TGSendMessageToDB(WM_TG_ODBC_INSERT_MOVIE_ANALYSIS, (PVOID)pInfo))
	{
		delete pInfo;
		pInfo = NULL;
	}
	*/
	return TRUE;
}

LONG CTGLatencyAnalysisView::InitGraph(CString strModel, CString strVer)
{
	Clear();

	m_strTitle.Format(_T("%s : %s"),strModel,strVer);
	m_pTGGraph->DeleteGraphAll();
	m_pTGGraph->AddGraph(m_strTitle,GRAPH_MONMULTI_WINID);


	return TG_ERR_NON;
}

LONG CTGLatencyAnalysisView::AddTime(CString strTime, float nPercentage)
{
	TRACE(_T("Time[%s] Percentage[%3.02f]\n"),strTime, nPercentage);
	//-- Add Line to Graph			
	m_pTGGraph->AddData(m_strTitle, strTime, nPercentage);

	return TG_ERR_NON;
}
