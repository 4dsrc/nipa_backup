////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyResultListCtrl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-23
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TestStepDef.h"
#include "TGLatencyResultListCtrl.h"


CTGLatencyResultListCtrl::CTGLatencyResultListCtrl(void)
{
	m_dwSequence = 0;
}

CTGLatencyResultListCtrl::~CTGLatencyResultListCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CTGLatencyResultListCtrl, CListCtrl)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CTGLatencyResultListCtrl::OnNMCustomdraw)
	ON_WM_MEASUREITEM()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


void CTGLatencyResultListCtrl::CheckExistData(LatencyResult* pNew)
{
	
}

void CTGLatencyResultListCtrl::AddDetectTime(LatencyResult* pData)
{
	CString strData;

	//Add Result Data
	AddData(pData);
	//Get Result Data
	LatencyResult Result = m_LatencyMgr.GetResultData(pData);
	int nItemCount = GetItemCount();

	for(int nCount = 0; nCount <= nItemCount; nCount++)
	{
		if(pData->strIP == GetItemText( nCount, 0))
		{
			if(pData->strCodec == GetItemText( nCount, 1))
				if(pData->strResolution == GetItemText( nCount, 2))
					if(pData->strFramerate == GetItemText( nCount, 3))
						if(pData->strColor == GetItemText( nCount, 4))
						{
							SetItemText(nCount, 5, (LPCTSTR)Result.strTime);
							strData.Format(_T("%.02f %%"), Result.dbPercentage);
							SetItemText(nCount, 6, (LPCTSTR)strData);
							strData.Format(_T("%d"), Result.nCount);
							SetItemText(nCount, 7, (LPCTSTR)strData);
							SetItemText(nCount, 8, (LPCTSTR)Result.strDeviation);

							//-- 2012-09-21 joonho.kim
							InsertToDB( Result );
							return;
						}
		}	
	}		


	InsertItem(nItemCount, _T(""));
	SetItemText(nItemCount, 0, (LPCTSTR)pData->strIP);
	SetItemText(nItemCount, 1, (LPCTSTR)pData->strCodec);
	SetItemText(nItemCount, 2, (LPCTSTR)pData->strResolution);
	SetItemText(nItemCount, 3, (LPCTSTR)pData->strFramerate);
	SetItemText(nItemCount, 4, (LPCTSTR)pData->strColor);
	SetItemText(nItemCount, 5, (LPCTSTR)Result.strTime);
	strData.Format(_T("%3.02f %%"), Result.dbPercentage);
	SetItemText(nItemCount, 6, (LPCTSTR)strData);
	strData.Format(_T("%d"), Result.nCount);
	SetItemText(nItemCount, 7, (LPCTSTR)strData);
	SetItemText(nItemCount, 8, (LPCTSTR)Result.strDeviation);

	//-- 2012-09-21 joonho.kim
	InsertToDB( Result );
}

BOOL CTGLatencyResultListCtrl::InsertToDB(LatencyResult Result)
{
	CString strUUID		= TGGetUniqueID();
	CTGViewLatencyResult* pInfo = new CTGViewLatencyResult( m_dwSequence++, strUUID, Result);

	if(!TGSendMessageToDB(WM_TG_ODBC_VIEW_LATENCY_RESULT, (PVOID)pInfo))
	{
		delete pInfo;
		pInfo = NULL;
	}
	return TRUE;
}

void CTGLatencyResultListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	if (lplvcd->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if (lplvcd->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	{
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
	}
	else if (lplvcd->nmcd.dwDrawStage == (CDDS_ITEMPREPAINT | CDDS_SUBITEM))
	{
		CItemResultData* p = (CItemResultData*)(CListCtrl::GetItemData(lplvcd->nmcd.dwItemSpec));
		ASSERT(p != NULL);
		ASSERT(lplvcd->iSubItem >= 0 && lplvcd->iSubItem < p->aTextColors.GetSize());
		lplvcd->clrText = p->aTextColors[lplvcd->iSubItem];
		lplvcd->clrTextBk = p->aBkColors[lplvcd->iSubItem];
		*pResult = CDRF_DODEFAULT;
	}

	//*pResult = 0;
}

void CTGLatencyResultListCtrl::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	// TODO: Add your message handler code here and/or call default

	CTGReportCtrl::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

//------------------------------------------------------------------------------ 
//! @brief
//! @date     2011-6-21
//! @owner    keunbae.song
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void CTGLatencyResultListCtrl::OnDestroy()
{
	CTGReportCtrl::OnDestroy();
}

//-- 2010-12-15 keunbae.song
void CTGLatencyResultListCtrl::Clear()
{
	int nItemCount = GetItemCount();
	int nColCount  = GetColumnCount();
	for(int i=0; i<nItemCount; i++)
	{
		for(int j=1; j<=nColCount; j++)
		{
			SetItemText(i, j, _T(""));
		}
	}
}

void CTGLatencyResultListCtrl::InitData()
{
	m_dwSequence = 0;
	m_LatencyMgr.RemoveAllChild();
}

