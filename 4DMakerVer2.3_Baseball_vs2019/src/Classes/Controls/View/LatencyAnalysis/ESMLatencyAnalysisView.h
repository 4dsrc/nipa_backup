////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyAnalysisView.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "resource.h"
#include "TGLatencyAnalysisGraph.h"

using namespace std;
class CTGLatencyAnalysisView : public CDialog
{
	class CTimeCnt
	{
	public:
		CTimeCnt(long nTime, long tFrameTime) {m_nTime = nTime; m_tFrameTime = tFrameTime; m_nCnt = 1;}
		long m_nTime;
		long m_tFrameTime;
		int  m_nCnt;
	};

public:
	CTGLatencyAnalysisView(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTGLatencyAnalysisView();
	void InitImageFrameWnd();
	// Dialog Data
	//{{AFX_DATA(CTGLatencyAnalysisView)
	enum { IDD = IDD_GRAPH_NON_TOOLBAR };
	//}}AFX_DATA


private:
	
	CString		m_strTitle;
	DWORD		m_dwSequence;

	int			m_nGOPCount;
	long		m_tTime;
	long		m_tFirstTime;
	BOOL		m_bStart;
	

	CTGLatencyAnalysisGraph* m_pTGGraph;
	CRect m_rcClientFrame;		
	
	HWND m_hMainWnd;
	BOOL InsertToDB(CString srtTime, int nFrame);	

public:
	LONG InitGraph(CString strModel, CString strVer);
	LONG AddTime(CString strTime, float nPercentage);
	
	void Clear();
	void Close(){ return OnClose();}


protected:	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTGLatencyAnalysisView)
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	//}}AFX_VIRTUAL
	DECLARE_MESSAGE_MAP()

protected:
	// Generated message map functions
	//{{AFX_MSG(CTGLatencyAnalysisView)	
	//}}AFX_MSG		
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	afx_msg void OnClose();	

};