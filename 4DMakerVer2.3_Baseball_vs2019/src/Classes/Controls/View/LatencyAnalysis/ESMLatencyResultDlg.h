////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyResultDlg.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-23
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "TGLatencyResultListCtrl.h"

class CTGOption;


// CTGLatencyResultDlg 대화 상자입니다.

class CTGLatencyResultDlg : public CDialog
{

public:
	CTGLatencyResultDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTGLatencyResultDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_FRAME };

	//-- To ListView control
	CTGLatencyResultListCtrl m_ctrlResult;	

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	afx_msg void OnSize(UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
public:
	//-- 2010-12-3 keunbae.song
	void AddDetectTime(LatencyResult* pData);	//-- To add Filtering stirng
	void ClearResultView();			//Clear all list	
};
