////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyAnalysisGraph.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-22
//
////////////////////////////////////////////////////////////////////////////////



#include "stdafx.h"
#include "GlobalIndex.h"
#include "TGLatencyAnalysisGraph.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define STRING_PERCENTAGE		_T("Percentage")
#define INDEX_PERCENTAGE		0


// CTGLatencyAnalysisGraph 생성/소멸
CTGLatencyAnalysisGraph::CTGLatencyAnalysisGraph()
{	
	m_nGOPBase = 0;
}

CTGLatencyAnalysisGraph::~CTGLatencyAnalysisGraph()
{	
}

//-- 2010-3-9 hongsu.jung
//-- CreateGraph
void CTGLatencyAnalysisGraph::CreateGraph()
{
	m_Graph.SetMessageID(WM_GRAPH_MSG);	
	//-- 2012-07-31 hongsu@esmlab.com
	//-- Decimal X Data (for millisecond x data) 
	m_Graph.SetXDataType(XDataTYPE_DECIMAL);
	m_Graph.SetINIDirectory(TGGetPath(TG_PATH_HOME_CONFIG));
	m_Graph.SetMaxScrollPage(4000);
	m_Graph.SetDefaultScrollPage(2000);
	//-- 2010-2-25 hongsu.jung	
	//m_Graph.SetPopUpMenu(UNIGRAPH_POPUP_ALL);	
	m_Graph.SetEventValue(UNIGRAPH_LBUTTONDBCLK);
	//-- 2010-8-10 hongsu.jung
	//m_Graph.SetPopUpMenu(UNIGRAPH_POPUP_USAGE);
}

void CTGLatencyAnalysisGraph::AddGraph(CString strName, unsigned int unIndex)
{
	int nMask = 
		UNIGRAPH_DISPLAY_TITLE |	//** Show Title
		UNIGRAPH_DISPLAY_XTITLE | 	//** Show XTitle
		UNIGRAPH_DISPLAY_YTITLE |	//** Show YTitle
		UNIGRAPH_DISPLAY_XGRID |	//** Show XGrid
		UNIGRAPH_DISPLAY_YGRID |	//** Show YGrid
		UNIGRAPH_WND_VERT |		
		UNIGRAPH_STATUS |			//** 그래프 데이타 이름을 보여줄것인가.,
		UNIGRAPH_SELETION_VALUE |	//** 마우스 포이트 위치 값을 보여줄것인가.,
		UNIGRAPH_THRESHOLD |		//** 임계치 정보를 보여줄것인가..,
		//UNIGRAPH_SCALE |			//** 스케일 윈도우를 생성할것인가...,
		//UNIGRAPH_SAVEDATA |		//** 그래프 데이터를 파일로 저장할 것인가..,
		UNIGRAPH_POPUPMENU;			//** 팝업메뉴를 생성할 것인가..,

	GRAPH_LINE_STYLE LineStyle;
	LineStyle.dLimit = -1;
	LineStyle.nStyle = LINE_STYLE_BAR;//LINE_STYLE_3DBAR;//LINE_STYLE_LINE;
	LineStyle.nWidth = LINE_DEFAULT_WIDTH;
	LineStyle.nShow  = LINE_TYPE_SHOW;
	
	//-- Add 1st
	m_Graph.AddGraph(nMask, strName, this, GRAPH_MONMULTI_WINID+unIndex);
	m_Graph.SetYTickAttr(strName,"Percentage");

	LineStyle.strName = STRING_PERCENTAGE;
	m_Graph.SetGraphStyle(strName, INDEX_PERCENTAGE, LineStyle);	
}

void CTGLatencyAnalysisGraph::DeleteGraph(CString strName)
{
	m_Graph.DeleteGraph(strName);
}

//-- 2012-04-19 hongsu
void CTGLatencyAnalysisGraph::DeleteGraphAll()
{
	m_Graph.DeleteGraphAll();
}


void CTGLatencyAnalysisGraph::AddData(CString strName, CString strTime, float nPercentage)
{
	m_Graph.AddLineData(strName, INDEX_PERCENTAGE, strTime, nPercentage/100);
}



