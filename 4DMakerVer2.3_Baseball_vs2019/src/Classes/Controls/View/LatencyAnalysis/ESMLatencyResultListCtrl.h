////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyResultListCtrl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-23
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "TGReportCtrl.h"
#include "TGLatencyMgr.h"




class CTGLatencyResultListCtrl : public CTGReportCtrl //CListCtrl
{
public:
	DWORD m_dwSequence;
public:
	CTGLatencyResultListCtrl(void);
	~CTGLatencyResultListCtrl(void);

	void AddDetectTime(LatencyResult* pData);
	void InitData();
	void Clear();

protected:
	DECLARE_MESSAGE_MAP()

	CTGArray m_arData;
	CTGLatencyMgr m_LatencyMgr;

	void AddData(LatencyResult* pNew)	{ m_LatencyMgr.AddLatencyData(pNew);			}
	int GetDataCount()					{ return m_arData.GetSize();					}
	LatencyResult* GetData(int nIndex)	{ return (LatencyResult*)m_arData.GetAt(nIndex);}
	void CheckExistData(LatencyResult* pNew);
	BOOL InsertToDB(LatencyResult Result);


public:
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDestroy();
};
