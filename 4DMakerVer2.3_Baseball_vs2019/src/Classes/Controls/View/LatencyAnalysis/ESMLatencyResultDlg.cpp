////////////////////////////////////////////////////////////////////////////////
//
//	TGLatencyResultDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-08-23
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TG.h"
#include "TGLatencyResultDlg.h"
#include "GlobalIndex.h"

// CTGLatencyResultDlg 대화 상자입니다.

CTGLatencyResultDlg::CTGLatencyResultDlg(CWnd* pParent /*=NULL*/)
: CDialog(CTGLatencyResultDlg::IDD, pParent)
{	
}

CTGLatencyResultDlg::~CTGLatencyResultDlg()
{
}

void CTGLatencyResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RESULTLIST, m_ctrlResult);
}

BEGIN_MESSAGE_MAP(CTGLatencyResultDlg, CDialog)
	ON_WM_SIZE()
END_MESSAGE_MAP()

BOOL CTGLatencyResultDlg::OnInitDialog() 
{
	if(!CDialog::OnInitDialog())
	{
		ASSERT( FALSE );
		return FALSE;
	}

	//OSD Result View Init
	m_ctrlResult.InsertColumn(0, _T("IP")			, LVCFMT_LEFT,	 50);
	m_ctrlResult.InsertColumn(1, _T("Codec")		, LVCFMT_LEFT,	 80);
	m_ctrlResult.InsertColumn(2, _T("Resolution")	, LVCFMT_LEFT,	 55);
	m_ctrlResult.InsertColumn(3, _T("Framerate")	, LVCFMT_LEFT,   55);
	m_ctrlResult.InsertColumn(4, _T("Color")		, LVCFMT_LEFT,   55);
	m_ctrlResult.InsertColumn(5, _T("Time")			, LVCFMT_LEFT,   55);
	m_ctrlResult.InsertColumn(6, _T("%")			, LVCFMT_LEFT,   55);
	m_ctrlResult.InsertColumn(7, _T("Cnt")			, LVCFMT_LEFT,   55);
	m_ctrlResult.InsertColumn(8, _T("Dev.")			, LVCFMT_LEFT,   55);

	m_ctrlResult.SetColumnWidth( 0, 110 );
	m_ctrlResult.SetColumnWidth( 1, 80 );
	m_ctrlResult.SetColumnWidth( 2, 90 );
	m_ctrlResult.SetColumnWidth( 3, 80 );
	m_ctrlResult.SetColumnWidth( 4, 80 );
	m_ctrlResult.SetColumnWidth( 5, 90 );
	m_ctrlResult.SetColumnWidth( 6, 90 );
	m_ctrlResult.SetColumnWidth( 7, 70 );
	m_ctrlResult.SetColumnWidth( 8, 90 );
	
	m_ctrlResult.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	m_ctrlResult.SetGridLines(TRUE);
	return TRUE;
}

void CTGLatencyResultDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);	
 	if (m_ctrlResult.GetSafeHwnd())
 		m_ctrlResult.MoveWindow(0, 0, cx, cy);	
}

//-- Add the Filtered serial log
void CTGLatencyResultDlg::AddDetectTime(LatencyResult* pData)
{
	//-- Add the string to listview
	m_ctrlResult.AddDetectTime(pData);	
	UpdateData(FALSE);
}

//-- To reload the file, clear all item in listview
void CTGLatencyResultDlg::ClearResultView()
{	
	m_ctrlResult.InitData();
	m_ctrlResult.DeleteAllItems();
}
