/////////////////////////////////////////////////////////////////////////////
//
//  Zoomview.cpp : implementation file
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2009 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#pragma once

#include "cv.h"
#include "highgui.h"
// CZoomview view
#include "NXRemoteStudioFunc.h"

class CZoomview : public CView
{
	DECLARE_DYNCREATE(CZoomview)

public:
	CZoomview();           // protected constructor used by dynamic creation	
	virtual ~CZoomview();

public:
	virtual void OnDraw(CDC* pDC) {};      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	afx_msg void OnSize(UINT nType, int cx, int cy);	

protected:
	DECLARE_MESSAGE_MAP()

private:
	afx_msg void OnPaint();
	IplImage *m_pImgZoom;
	void DrawZoomview();

public:
	void CopyImage(IplImage *pImgZoom);
};

