////////////////////////////////////////////////////////////////////////////////
//
//	TGSRMGraphManager.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-10-22
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TG.h"
#include "TGSRMGraphManager.h"

#include "TGSRMCSView.h"

// CTGSRMGraphManager
IMPLEMENT_DYNCREATE(CTGSRMGraphManager, CWinThread)

CTGSRMGraphManager::CTGSRMGraphManager() :
m_bError(FALSE),
m_bStop(FALSE),
m_bRun(FALSE)
{	
	InitializeCriticalSection (&_TGSRMGraphMgr);
}


CTGSRMGraphManager::~CTGSRMGraphManager()
{
	DeleteCriticalSection (&_TGSRMGraphMgr);
}


BEGIN_MESSAGE_MAP(CTGSRMGraphManager, CWinThread)
END_MESSAGE_MAP()

BOOL CTGSRMGraphManager::InitInstance()
{
	return TRUE;
}

int CTGSRMGraphManager::ExitInstance()
{
	//-- 2012-08-02 hongsu@esmlab.com
	//-- Set Stop (Include CloseDB)
	if(m_bRun)
	{
		m_bStop = TRUE;
		int nTestTimer = 2000;
		//-- Wait Until Close 
		while(m_bRun)
		{
			TGSleep();
			nTestTimer--;
			if(!nTestTimer)
				break;
		}
	}
	return CWinThread::ExitInstance();
}

int CTGSRMGraphManager::AddMessage(TGEvent* pEvent)
{ 
	int nCnt;
	//EnterCriticalSection (&_TGSRMGraphMgr);
	nCnt = m_arMsg.Add((CObject*)pEvent);
	//LeaveCriticalSection (&_TGSRMGraphMgr);
	return nCnt;
}

int CTGSRMGraphManager::GetMessageCount()
{
	int nCnt;
	EnterCriticalSection (&_TGSRMGraphMgr);
	nCnt = m_arMsg.GetSize();
	LeaveCriticalSection (&_TGSRMGraphMgr);
	return nCnt;
}

//------------------------------------------------------------------------------ 
//! @brief    Wait Message ( in Thread )
//! @date     2010-3-4
//! @owner    hongsu.jung
//------------------------------------------------------------------------------ 
int CTGSRMGraphManager::Run()
{
	int nMsgIndex;
	TGEvent* pEvent = NULL;
	BOOL bSuccess = TRUE;

	//-- 2012-05-17 hongsu
	m_bRun = TRUE;	

	while(1) 
	{
		//-- 2012-08-02 hongsu@esmlab.com
		//-- Stop Control 
		if(m_bStop)
			break;
		nMsgIndex = m_arMsg.GetSize();
		if(nMsgIndex > 5)
			TGLog(4, _T("[SM] SRM Graph message queue count(%03d)"), nMsgIndex);
		else if(!nMsgIndex)
			TGSleep();

		//while(nMsgIndex--)     //Backward processing
		while(m_arMsg.GetSize()) //Forward processing
		{
			//-- 2012-05-21 hongsu
//			EnterCriticalSection (&_TGSRMGraphMgr);			
			pEvent = (TGEvent*)m_arMsg.GetAt(0);
			//-- 2012-05-21 hongsu Locking
//			LeaveCriticalSection (&_TGSRMGraphMgr);
				
			if(pEvent)
			{
				switch(pEvent->message)
				{
					case WM_TG_SM_CS_ADD:	
						{
							CUIntArray* pArray = NULL;
							UINT nTickCount = pEvent->nParam;

							pArray = (CUIntArray*)pEvent->pParam;
							//-- 2012-10-22 hongsu@esmlab.com
							//-- Add Array
							CTGSRMCSView* pCSView = (CTGSRMCSView*)TGGetCSView();
							if(pCSView)
								pCSView->AddTID(pArray, nTickCount);
							
							pArray ->RemoveAll();
							delete pArray;
						}				
						
						break;
					default:
						break;
				}			

				//-- 2012-05-24 hongsu
				if(pEvent)
				{
					delete pEvent;
					pEvent = NULL;
				}
				//-- 2012-05-21 hongsu
				EnterCriticalSection (&_TGSRMGraphMgr);
				m_arMsg.RemoveAt(0);					
				//-- 2012-05-21 hongsu Locking
				LeaveCriticalSection (&_TGSRMGraphMgr);
				
			}

			//-- 2012-04-06
			//-- Check Insert Error
			if(m_bError)
			{
				TGLog(0,_T("[DB] Connection Error")); 
				break;
			}
			TGSleep();
		}		
	}//-- while wait added message

	//-- 2012-05-21 hongsu
	EnterCriticalSection (&_TGSRMGraphMgr);	
	m_arMsg.RemoveAll();			
	LeaveCriticalSection (&_TGSRMGraphMgr);

	m_bRun = FALSE;
	return 0;
}