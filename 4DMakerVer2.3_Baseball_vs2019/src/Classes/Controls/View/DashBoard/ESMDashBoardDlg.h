////////////////////////////////////////////////////////////////////////////////
//
//	ESMDashBoardDlg.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "ESMDashBoardListCtrl.h"

class CESMDashBoardDlg : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	// Construction
public:
	CESMDashBoardDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMDashBoardDlg();

	// Dialog Data
	//{{AFX_DATA(CESMNetworkDlg)
	enum { IDD = IDD_VIEW_DASHBOARD };
	//}}AFX_DATA
	void InitImageFrameWnd();
	HWND GetCESMDashBoardDlgHandle(){return m_hMainWnd;}
protected:
	//seo
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	CRect m_rcClientFrame;	
	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;	
	HWND m_hMainWnd;		
	
	
public:

	//-- 2013-10-08 hongsu@esmlab.com
	//-- Load Information from File 
	void SetMakeMovieStatus(BOOL bSet);
	void LoadInfo();
	void SaveInfo();

	CESMDashBoardListCtrl m_ctrlList;
	void SetFrameCnt(int n){m_nFrameMakeCnt = n;}
	int GetFrameCnt(){return m_nFrameMakeCnt;}

	void SetTemplateNum(int n){m_nTemplateNum = n;}
	int GetTemplateNum(){return m_nTemplateNum;}
private:	
	int m_nFrameMakeCnt;
	int m_nTemplateNum;
protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	DECLARE_MESSAGE_MAP()

protected:
	// Generated message map functions
	//{{AFX_MSG(CESMNetworkDlg)	
	afx_msg void OnPaint();
	//}}AFX_MSG	
	afx_msg void OnSize(UINT nType, int cx, int cy);
	
public:
	
};
