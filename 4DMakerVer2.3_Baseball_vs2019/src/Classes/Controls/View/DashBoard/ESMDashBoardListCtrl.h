////////////////////////////////////////////////////////////////////////////////
//
//	ESMNetworkListCtrl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-10-08
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxcmn.h"
#include "ESMHeaderCtrl.h"
#include <vector>


class CESMDashBoardDlg;


enum LISTDashColumn
{
	LIST_DASH_NO = 0,
	LIST_DASH_ITEM,
	LIST_DASH_STATUS,
	LIST_DASH_TOTAL_COUNT
};

enum DASHBOARDITEM
{
	ITEM_DELAY_DIRECTION = 0,
	ITEM_POSITION_TRACKING,
	ITEM_PROPERTY_CHECK,
	ITEM_COLOR_TRANS,
	ITEM_DELAY_MODE,
	ITEM_DELAY_VALUE_A,
	ITEM_DELAY_VALUE_B,
	ITEM_ID_CHECK,
	ITEM_TEMPLATE_PAGE,
	ITEM_LOCAL_IP,
	ITEM_COMPLETION_TIME,
	ITEM_MAKE_FRAME,
	ITEM_AJA_USAGE,
	ITEM_KZONE_USAGE,
	ITEM_DASHBOARD_MAX,
};

typedef struct DASHBOARD_INFO
{
	CString strDelayDirection;
	BOOL bPosTracking;
	BOOL bPropertyCheck;
	BOOL bColorTrans;
	CString strDelayMode;
	int nDelayValue1;
	int nDelayValue2;
	BOOL bIDCheck;
	CString strTemplatePage;
	CString strIP;
	CString strCompleteTime;
	int nTotalMakeFrame;
	BOOL bAJAMaking;
	int nMakingTemplate;
	BOOL bKZoneUsage;
}_DASHBOARD_INFO;


class CESMDashBoardListCtrl : public CListCtrl
{
public:
	CESMDashBoardListCtrl(void);
public:
	~CESMDashBoardListCtrl(void);
	void Clear();
	void Init(CESMDashBoardDlg* pParent);
	void Save(CESMDashBoardDlg* pParent);
	void LoadInfo();
	void SetInfo();
	void SetStatus(int Row, CString strStatus);
	
	COLORREF m_clrText;
	COLORREF m_clrBk;
public:
	DASHBOARD_INFO m_DashBoardInfo;
	BOOL m_bRefresh;
private:
	BOOL m_bBKColor;
	CESMDashBoardDlg* m_pParent;
	HANDLE	m_hThreadHandle;
	CRITICAL_SECTION m_CrSockket;
	CESMHeaderCtrl m_HeaderCtrl;
	

public:
	
protected:
	CListCtrl m_IPList;
	afx_msg void OnCustomDrawList(NMHDR *pNMHDR, LRESULT *pResult);
	DECLARE_MESSAGE_MAP()
	
	virtual void PreSubclassWindow();
};
