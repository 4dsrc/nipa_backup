////////////////////////////////////////////////////////////////////////////////
//
//	ESMDashBoardDlg.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMDashBoardDlg.h"
#include "resource.h"
#include "MainFrm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CESMNetworkDlg dialog
CESMDashBoardDlg::CESMDashBoardDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CESMDashBoardDlg::IDD, pParent)	
{	
	WSADATA wsa;
	WSAStartup(MAKEWORD(2,2), &wsa);
	
	m_nFrameMakeCnt = 0;
	m_nTemplateNum  = 0;
}

CESMDashBoardDlg::~CESMDashBoardDlg()
{
	
}

void CESMDashBoardDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMNetworkDlg)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);	
	DDX_Control(pDX, IDC_DASHBOARD_CTRL, m_ctrlList);
}

BOOL CESMDashBoardDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg)
	{
		if(pMsg->message == WM_KEYDOWN)
		{
			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			ESMEvent* pEsmMsg = NULL;
			pEsmMsg = new ESMEvent();
			pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
			pEsmMsg->pParam = (LPARAM)pMsg;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMDashBoardDlg, CDialog)
	//{{AFX_MSG_MAP(CESMNetworkDlg)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()	
	ON_WM_PAINT()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CESMNetworkDlg message handlers

BOOL CESMDashBoardDlg::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}


	//-- SET MAIN WINDOWS HANDLE
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();		
	m_ctrlList.Init(this);
		
	return TRUE;
}

void CESMDashBoardDlg::SetMakeMovieStatus(BOOL bSet)
{
	m_ctrlList.m_bRefresh = bSet;
	if(bSet)
	{
		m_ctrlList.m_clrText		= RGB(0,0,0);
		m_ctrlList.m_clrBk	= RGB(0,155,218); //blue
	}
	else
	{
		m_ctrlList.m_clrText = RGB(0,0,0);
		m_ctrlList.m_clrBk = RGB(255,255,255);
	}
	
}

void CESMDashBoardDlg::LoadInfo()
{
	m_ctrlList.LoadInfo();
}

void CESMDashBoardDlg::OnPaint()
{	
	CDialog::OnPaint();
}

void CESMDashBoardDlg::InitImageFrameWnd()
{
	GetClientRect(m_rcClientFrame);	
}


void CESMDashBoardDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	
	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
		
	if(m_ctrlList)
		m_ctrlList.MoveWindow(m_rcClientFrame);

	Invalidate();
}
