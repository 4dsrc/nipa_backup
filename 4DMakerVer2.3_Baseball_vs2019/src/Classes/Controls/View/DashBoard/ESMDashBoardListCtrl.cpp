////////////////////////////////////////////////////////////////////////////////
//
//	ESMDashBoardListCtrl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ESMDashBoardListCtrl.h"
#include "ESMDashBoardDlg.h"
#include "ESMFunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CESMDashBoardListCtrl::CESMDashBoardListCtrl(void)
{
	m_bBKColor	= FALSE;
	m_pParent	= NULL;

	m_clrText = RGB(0,0,0);
	m_clrBk = RGB(255,255,255);
	m_bRefresh = FALSE;
	InitializeCriticalSection (&m_CrSockket);

}

CESMDashBoardListCtrl::~CESMDashBoardListCtrl(void)
{
	
}

BEGIN_MESSAGE_MAP(CESMDashBoardListCtrl, CListCtrl)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CESMDashBoardListCtrl::OnCustomDrawList)
END_MESSAGE_MAP()


void CESMDashBoardListCtrl::Clear()
{
	int nItemCount = GetItemCount();
}


void CESMDashBoardListCtrl::Init(CESMDashBoardDlg* pParent)
{
	m_pParent = pParent;

	// SetExtendedStyle(GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	SetExtendedStyle(LVS_EX_DOUBLEBUFFER | LVS_EX_FULLROWSELECT );

	// Initialize list control with some data.
	InsertColumn(LIST_DASH_NO, _T("No"), LVCFMT_LEFT, 50);
	InsertColumn(LIST_DASH_ITEM, _T("Item"), LVCFMT_LEFT, 150);
	InsertColumn(LIST_DASH_STATUS, _T("Status"), LVCFMT_LEFT, 100);

	for(int i = 0; i < ITEM_DASHBOARD_MAX; i++)
	{
		CString strNum;
		strNum.Format(_T("%d"), i +1);
		InsertItem(i, _T(""));
		SetItemText(i,0,strNum);
	}

	SetItemText(ITEM_DELAY_DIRECTION,	1,		_T("Delay Direction"));
	SetItemText(ITEM_POSITION_TRACKING,	1,		_T("Position Tracking"));
	SetItemText(ITEM_PROPERTY_CHECK,	1,		_T("Property Check"));
	SetItemText(ITEM_COLOR_TRANS,		1,		_T("Color Trans"));
	SetItemText(ITEM_DELAY_MODE,		1,		_T("Delay Mode"));
	SetItemText(ITEM_DELAY_VALUE_A,		1,		_T("Delay Value(A)"));
	SetItemText(ITEM_DELAY_VALUE_B,		1,		_T("Delay Value(B)"));
	SetItemText(ITEM_ID_CHECK,			1,		_T("ID Check"));
	SetItemText(ITEM_TEMPLATE_PAGE,		1,		_T("Template Page-Num"));
	SetItemText(ITEM_LOCAL_IP,			1,		_T("Local IP"));
	SetItemText(ITEM_COMPLETION_TIME,	1,		_T("Complete Time"));
	SetItemText(ITEM_MAKE_FRAME,		1,		_T("Make Frame"));
	SetItemText(ITEM_AJA_USAGE,			1,		_T("AJA Usage"));
	SetItemText(ITEM_KZONE_USAGE,		1,		_T("Draw AR"));
}


void CESMDashBoardListCtrl::LoadInfo()
{
	CString strData,strFrameCnt;
	m_DashBoardInfo.bPosTracking	= ESMGetValue(ESM_VALUE_TEMPLATEPOINT);
	m_DashBoardInfo.bPropertyCheck	= ESMGetPropertyCheck();
	m_DashBoardInfo.bColorTrans		= ESMGetValue(ESM_VALUE_COLORREVISION);
	m_DashBoardInfo.nDelayValue1	= ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC);
	m_DashBoardInfo.nDelayValue2	= ESMGetValue(ESM_VALUE_WAIT_SAVE_DSC_SUB);
	m_DashBoardInfo.bIDCheck		= ESMGetValue(ESM_VALUE_INSERT_CAMERAID);
	m_DashBoardInfo.strDelayMode	= ESMGetDelayMode();
	m_DashBoardInfo.strTemplatePage.Format(_T("%s-%d"),ESMGetTemplatePage(),m_pParent->GetTemplateNum());
	m_DashBoardInfo.strDelayDirection = ESMGetDelayDirection();
	m_DashBoardInfo.strCompleteTime.Format(_T("%d ms"),ESMGetMakeMovieTime());
	m_DashBoardInfo.strIP			= ESMGetLocalIP();
	m_DashBoardInfo.nTotalMakeFrame	= m_pParent->GetFrameCnt();
	m_DashBoardInfo.bAJAMaking		= ESMGetValue(ESM_VALUE_AJANETWORK);
	m_DashBoardInfo.bKZoneUsage		= ESMGetUsageKZone();

	if(m_DashBoardInfo.bPosTracking)	SetItemText(ITEM_POSITION_TRACKING,	2,		_T("ON"));	else	SetItemText(ITEM_POSITION_TRACKING,	2,		_T("OFF"));
	if(m_DashBoardInfo.bPropertyCheck)	SetItemText(ITEM_PROPERTY_CHECK,	2,		_T("PASS"));else	SetItemText(ITEM_PROPERTY_CHECK,	2,		_T("FAIL"));
	if(m_DashBoardInfo.bColorTrans)		SetItemText(ITEM_COLOR_TRANS,		2,		_T("ON"));	else	SetItemText(ITEM_COLOR_TRANS,		2,		_T("OFF"));
	if(m_DashBoardInfo.bIDCheck)		SetItemText(ITEM_ID_CHECK,			2,		_T("ON"));	else	SetItemText(ITEM_ID_CHECK,		2,		_T("OFF"));

	SetItemText(ITEM_DELAY_DIRECTION,	2,		m_DashBoardInfo.strDelayDirection);
	SetItemText(ITEM_DELAY_MODE,		2,		m_DashBoardInfo.strDelayMode);
	SetItemText(ITEM_TEMPLATE_PAGE,		2,		m_DashBoardInfo.strTemplatePage);
	SetItemText(ITEM_LOCAL_IP,			2,		m_DashBoardInfo.strIP);
	SetItemText(ITEM_COMPLETION_TIME,	2,		m_DashBoardInfo.strCompleteTime);
	strData.Format(_T("%d"), m_DashBoardInfo.nDelayValue1);
	SetItemText(ITEM_DELAY_VALUE_A,		2,		strData);
	strData.Format(_T("%d"), m_DashBoardInfo.nDelayValue2);
	SetItemText(ITEM_DELAY_VALUE_B,		2,		strData);

	strFrameCnt.Format(_T("%d frames"),m_DashBoardInfo.nTotalMakeFrame);
	SetItemText(ITEM_MAKE_FRAME,		2,		strFrameCnt);
	
	strFrameCnt = m_DashBoardInfo.bAJAMaking ? _T("O") : _T("X");
	SetItemText(ITEM_AJA_USAGE,		2,		strFrameCnt);

	strFrameCnt = ESMGetValue(ESM_VALUE_AUTOADJUST_KZONE)? _T("On") : _T("Off");
	strFrameCnt.Append(m_DashBoardInfo.bKZoneUsage? _T("(O)") : _T("(X)"));
	SetItemText(ITEM_KZONE_USAGE,		2,		strFrameCnt);
	
	RedrawItems(0,GetItemCount());
}


void CESMDashBoardListCtrl::SetInfo()
{

}



void CESMDashBoardListCtrl::OnCustomDrawList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
		// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	*pResult = 0;
	// 여기부터 추가
	LPNMLVCUSTOMDRAW pLVCD = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);
	if (pLVCD->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
#if 0
		pLVCD->clrText = m_clrText;  // 글자색 지정  
		pLVCD->clrTextBk = m_clrBk;  // 배경색 지정  
		*pResult = CDRF_NOTIFYITEMDRAW;
#endif
		CDC* pDC = CDC::FromHandle(pNMCD->hdc);
		CRect rect(0, 0, 0, 0);
		GetClientRect(&rect);
		pDC->FillSolidRect(&rect, RGB(44, 44, 44));

		*pResult = CDRF_NOTIFYITEMDRAW;

	}
	else if (pLVCD->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
	{
#if 0
		pLVCD->clrText = m_clrText;  // 글자색 지정  
		pLVCD->clrTextBk = m_clrBk;  // 배경색 지정  
		*pResult = CDRF_DODEFAULT;
#endif
		int nItem = static_cast<int>(pLVCD->nmcd.dwItemSpec);
		CString strTemp = GetItemText(nItem, 0/*pLVCD->iSubItem*/);
		if(m_bRefresh && nItem == ITEM_COMPLETION_TIME)
		{
			pLVCD->clrTextBk	= m_clrBk;//RGB(255,0,0);
			pLVCD->clrText		= m_clrText;//RGB(255,255,255);
		}
		else
		{
			if(!strTemp.IsEmpty())
			{
				if (nItem%2 == 0)
					pLVCD->clrTextBk	= RGB(35,35,35);
				else
					pLVCD->clrTextBk	= RGB(40,40,40);

				pLVCD->clrText		= RGB(255,255,255);
			}
			else
			{
				//기본색상 - 검정 배경 / 화이트 텍스트
				pLVCD->clrTextBk = RGB(44,44,44);
				pLVCD->clrText	 = RGB(255,255,255);
			}
		}
	}
}


void CESMDashBoardListCtrl::PreSubclassWindow()
{
	CListCtrl::PreSubclassWindow();
	m_HeaderCtrl.SubclassWindow(::GetDlgItem(m_hWnd,0));
}
