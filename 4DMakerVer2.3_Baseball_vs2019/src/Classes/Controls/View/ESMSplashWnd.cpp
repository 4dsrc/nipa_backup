/////////////////////////////////////////////////////////////////////////////
//
//	ESMSplashWnd.cpp : implementation file
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  ESMLab, Inc., and may not be copied
//  nor disclosed except upon written agreement
//  by ESMLab, Inc..
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2009-02-01
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMSplashWnd.h"
#include "ESMFunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CESMSplashWnd

static bool g_bESMSplashWndClassRegistered = false;

CESMSplashWnd::CESMSplashWnd(
	CWnd * pWndParent,
	UINT nBitmapID
	)
	: m_nMaxTextLines( -1 )
	, m_nLineHeight( -1 )
	//, m_clrText( RGB(255,255,255) )
	, m_clrText( RGB(0,0,0) )
	, m_rcText( 0, 0, 0, 0 )
{
	VERIFY( RegisterESMSplashWndClass() );
	VERIFY( Create( pWndParent, nBitmapID ) );
}

CESMSplashWnd::~CESMSplashWnd()
{
	if( GetSafeHwnd() != NULL )
		DestroyWindow();
}


BEGIN_MESSAGE_MAP(CESMSplashWnd, CWnd)
	//{{AFX_MSG_MAP(CESMSplashWnd)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_NCCALCSIZE()
	__EXT_MFC_ON_WM_NCHITTEST()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CESMSplashWnd message handlers

void CESMSplashWnd::AddTextLine(
	LPCTSTR sText, // = NULL
	bool bReplaceLast // = false
	)
{
	ASSERT_VALID( this );
	
	//-- 2013-04-23 ESMLog
	ESMLog(5,sText);

	if( bReplaceLast && m_arrTextLines.GetSize() > 0 )
		m_arrTextLines.SetAt( m_arrTextLines.GetSize() - 1, (sText == NULL) ? _T("") : sText );
	else
		m_arrTextLines.Add( (sText == NULL) ? _T("") : sText );
	if( GetSafeHwnd() == NULL )
		return;
	if( (GetStyle() & WS_VISIBLE) == 0 )
		return;

	Invalidate();
	UpdateWindow();
}

void CESMSplashWnd::ReplaceLastLine(
	LPCTSTR sText // = NULL
	)
{
	if( m_arrTextLines.GetSize() == 0 )
		return;
	m_arrTextLines.ElementAt( m_arrTextLines.GetSize() - 1 ) = (sText == NULL) ? _T("") : sText;
	if( GetSafeHwnd() == NULL )
		return;
	if( (GetStyle() & WS_VISIBLE) == 0 )
		return;
	Invalidate();
	UpdateWindow();
}

void CESMSplashWnd::ClearLines()
{
	ASSERT_VALID( this );
	if( m_arrTextLines.GetSize() == 0 )
		return;
	m_arrTextLines.RemoveAll();
	if( GetSafeHwnd() == NULL )
		return;
	if( (GetStyle() & WS_VISIBLE) == 0 )
		return;
	Invalidate();
	UpdateWindow();
}

bool CESMSplashWnd::Create(
	CWnd * pWndParent,
	UINT nBitmapID
	)
{
	ASSERT_VALID( this );
	ASSERT( GetSafeHwnd() == NULL );
	ASSERT( m_bitmap.GetSafeHandle() == NULL );
	if( ! RegisterESMSplashWndClass() )
	{
		ASSERT( FALSE );
		return false;
	}
	if( ! m_bitmap.LoadBitmap( nBitmapID ) )
	{
		ASSERT( FALSE );
		return false;
	}
BITMAP _bmpInfo;
	::memset( &_bmpInfo, 0, sizeof(BITMAP) );
	m_bitmap.GetBitmap( &_bmpInfo );
	m_sizeBitmap.cx = _bmpInfo.bmWidth;
	m_sizeBitmap.cy = _bmpInfo.bmHeight;
CExtPaintManager::monitor_parms_t _mp;
	CExtPaintManager::stat_GetMonitorParms( _mp, pWndParent );
CRect rcWnd( 0, 0, m_sizeBitmap.cx, m_sizeBitmap.cy );
	rcWnd.OffsetRect(
		( _mp.m_rcWorkArea.Width() - m_sizeBitmap.cx ) / 2,
		( _mp.m_rcWorkArea.Height() - m_sizeBitmap.cy ) / 2
		);
	if( ! m_wndInvisibleParent.CreateEx( 0, _T("STATIC"), _T(""), WS_POPUP, 0,0,0,0, pWndParent->GetSafeHwnd(), (HMENU)NULL )
		)
	{
		ASSERT( FALSE );
		m_bitmap.DeleteObject();
		m_wndInvisibleParent.DestroyWindow();
		return false;
	}
	if( ! CWnd::CreateEx(
			0, __SPLASH_WND_CLASS__, _T("Initizlizing ..."), WS_POPUP,
			rcWnd.left, rcWnd.top, m_sizeBitmap.cx, m_sizeBitmap.cy,
			m_wndInvisibleParent.GetSafeHwnd(), (HMENU)NULL
			)
		)
	{
		ASSERT( FALSE );
		m_bitmap.DeleteObject();
		m_wndInvisibleParent.DestroyWindow();
		return false;
	}
	VERIFY(
		::SetWindowPos(
			m_hWnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOSIZE|SWP_NOMOVE|SWP_SHOWWINDOW|SWP_NOACTIVATE|SWP_NOOWNERZORDER
			)
		);
	CExtPopupMenuWnd::PassMsgLoop( false );
	return true;
}

BOOL CESMSplashWnd::OnEraseBkgnd(CDC* pDC) 
{
	pDC;
	return TRUE;
}

void CESMSplashWnd::OnPaint() 
{
	ASSERT( m_bitmap.GetSafeHandle() != NULL );
	CPaintDC dcPaint( this );
	CExtMemoryDC dc( &dcPaint );
	CDC dcImg;
	if( ! dcImg.CreateCompatibleDC( &dcPaint ) )
	{
		ASSERT( FALSE );
		return;
	}

	CBitmap * pOldBmp = dcImg.SelectObject( &m_bitmap );
	dc.BitBlt(
		0, 0, m_sizeBitmap.cx, m_sizeBitmap.cy,
		&dcImg,
		0, 0,
		SRCCOPY
		);
	dcImg.SelectObject( pOldBmp );
	dcImg.DeleteDC();

	int nLen, i, nCount = int( m_arrTextLines.GetSize() );
	if( nCount > 0 )
	{
		if( m_rcText.IsRectEmpty() )
		{
			GetClientRect( &m_rcText );
			m_rcText.DeflateRect(
				10,
				25,
				10,
				10
				);
		}
		if( m_font.GetSafeHandle() == 0 )
		{
			LOGFONT _lf;
			::memset( &_lf, 0, sizeof(LOGFONT) );
			g_PaintManager->m_FontNormal.GetLogFont( &_lf );
			__EXT_MFC_STRCPY( _lf.lfFaceName, LF_FACESIZE, _T("Arial") );
			_lf.lfWeight = 300;
			_lf.lfItalic = FALSE;
			_lf.lfUnderline = FALSE;
			_lf.lfStrikeOut = FALSE;
			_lf.lfWidth = 0;
			_lf.lfHeight = 12;
			VERIFY( m_font.CreateFontIndirect( &_lf ) );
		}
		int nOldBkMode = dc.SetBkMode( TRANSPARENT );
		COLORREF clrTextOld = dc.SetTextColor( m_clrText );
		CFont * pOldFont = dc.SelectObject( &m_font );
		if( m_nLineHeight < 0 )
		{
			TEXTMETRIC _tm;
			::memset( &_tm, 0, sizeof(TEXTMETRIC) );
			dc.GetTextMetrics( &_tm );
			m_nLineHeight = _tm.tmHeight;
		}
		if( m_nMaxTextLines < 0 )
		{
			m_nMaxTextLines = m_rcText.Height() / m_nLineHeight;
			if( m_nMaxTextLines < 0 )
				m_nMaxTextLines = 0;
		}
		if( nCount > m_nMaxTextLines )
		{
			m_arrTextLines.RemoveAt( 0, nCount - m_nMaxTextLines );
			nCount = m_nMaxTextLines;
		}
		int nY = m_rcText.top;
		for( i = 0; i < nCount; i++ )
		{
			LPCTSTR sText = m_arrTextLines[i];
			if( sText != NULL && ( nLen = int( _tcslen(sText) ) ) > 0 )
			{
				CRect rcText = m_rcText;
				rcText.OffsetRect(
					0,
					nY - rcText.top
					);
				CExtRichContentLayout::stat_DrawText(
					dc.m_hDC,
					sText, nLen,
					&rcText,
					DT_SINGLELINE|DT_TOP|DT_RIGHT|DT_END_ELLIPSIS, 0
					);
			} // if( sText != NULL && ( nLen = int( _tcslen(sText) ) ) > 0 )
			nY += m_nLineHeight;
		} // for( i = 0; i < nCount; i++ )
		dc.SelectObject( pOldFont );
		dc.SetTextColor( clrTextOld );
		dc.SetBkMode( nOldBkMode );
	} // if( nCount > 0 )
}

void CESMSplashWnd::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp) 
{
	bCalcValidRects;
	lpncsp;
}

UINT CESMSplashWnd::OnNcHitTest(CPoint point) 
{
	point;
	return HTCLIENT;
}

void CESMSplashWnd::OnClose() 
{
}

BOOL CESMSplashWnd::PreCreateWindow(CREATESTRUCT& cs) 
{
	if( ( ! RegisterESMSplashWndClass() ) || ( ! CWnd::PreCreateWindow( cs ) ) )
	{
		ASSERT( FALSE );
		return FALSE;
	}
	cs.lpszClass = __SPLASH_WND_CLASS__;
	return TRUE;
}

bool CESMSplashWnd::RegisterESMSplashWndClass()
{
	if( g_bESMSplashWndClassRegistered )
		return true;
WNDCLASS _wndClassInfo;
HINSTANCE hInst = AfxGetInstanceHandle();
	if( ! ::GetClassInfo( hInst, __SPLASH_WND_CLASS__, &_wndClassInfo ) )
	{
		// otherwise we need to register a new class
		_wndClassInfo.style =
				CS_GLOBALCLASS
				//|CS_DBLCLKS
				|CS_HREDRAW|CS_VREDRAW
				|CS_NOCLOSE
				|CS_SAVEBITS
				;
		_wndClassInfo.lpfnWndProc = ::DefWindowProc;
		_wndClassInfo.cbClsExtra = _wndClassInfo.cbWndExtra = 0;
		_wndClassInfo.hInstance = hInst;
		_wndClassInfo.hIcon = NULL;
		_wndClassInfo.hCursor =
				::LoadCursor(
					NULL, //hInst,
					IDC_WAIT
					)
				;
		ASSERT( _wndClassInfo.hCursor != NULL );
		_wndClassInfo.hbrBackground = NULL; 
		_wndClassInfo.lpszMenuName = NULL;
		_wndClassInfo.lpszClassName = __SPLASH_WND_CLASS__;
		if( ! ::AfxRegisterClass( &_wndClassInfo ) )
		{
			ASSERT( FALSE );
			//AfxThrowResourceException();
			return false;
		}
	}
	g_bESMSplashWndClassRegistered = true;
	return true;
}

BOOL CESMSplashWnd::DestroyWindow() 
{
	ShowWindow( SW_HIDE );
	CExtPopupMenuWnd::PassMsgLoop( false );
	CWnd::DestroyWindow();
	if( m_wndInvisibleParent.GetSafeHwnd() != NULL )
		m_wndInvisibleParent.DestroyWindow();
	return TRUE;
}
