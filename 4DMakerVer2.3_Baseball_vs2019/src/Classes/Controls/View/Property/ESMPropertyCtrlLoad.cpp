////////////////////////////////////////////////////////////////////////////////
//
//	TGPropertiesLoad.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-07-24
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMPropertyCtrl.h"
#include "DSCItem.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// Load Property
void CESMPropertyCtrl::LoadDSCInfo(CDSCItem* pItem)
{
	if(!pItem)
		return;

	//EnableWindow(FALSE);	

	//-- DELETE ALL PREVIOUS POINTER
	if(m_pCategoryDSCBasic)	
		m_pCategoryDSCBasic->ItemRemove();
	if(m_pCategoryDSCOption)	
		m_pCategoryDSCOption->ItemRemove();
	if (m_pCategoryDSCSystemOption)
		m_pCategoryDSCSystemOption->ItemRemove();

	//-- Set DSC Information
	m_pItem = pItem;

	//-- Get Basic Info
	//-- Get Property from User Command
	LoadBasicInfo();

	//-- 2013-05-06 hongsu@esmlab.com
	//-- Optional Get Property 
	LoadDSCProperty();

	PropertyStoreSynchronize();
	//EnableWindow(TRUE);
	SetRedraw(TRUE);
}

void CESMPropertyCtrl::LoadBasicInfo()
{
	if(!m_pItem)
		return;
	//--------------------------------------------------------------------------
	//-- Basic Info
	//--------------------------------------------------------------------------
	m_pModelName =	new CExtPropertyValue(PROPERTY_ITEM_MODEL);
	VERIFY( m_pCategoryDSCBasic->ItemInsert( m_pModelName ) );
	m_pUniqueID = new CExtPropertyValue(PROPERTY_ITEM_UNIQUE);
	VERIFY( m_pCategoryDSCBasic->ItemInsert( m_pUniqueID ) );
	m_pDSCID = new CExtPropertyValue(PROPERTY_ITEM_ID);
	VERIFY( m_pCategoryDSCBasic->ItemInsert( m_pDSCID ) );

	if( m_pModelName )
	{
		m_pModelName->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( m_pItem->GetDeviceModel() );			
		m_pModelName->ValueActiveFromDefault();		
	}	
	if( m_pUniqueID )
	{
		m_pUniqueID->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( m_pItem->GetDeviceUniqueID() );			
		m_pUniqueID->ValueActiveFromDefault();
	}	
	if( m_pDSCID )
	{
		m_pDSCID->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( m_pItem->GetDeviceDSCID() );
		m_pDSCID->ValueActiveFromDefault();
	}	
}



//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-05-04
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMPropertyCtrl::LoadDSCProperty()
{
	if(m_pCategoryDSCOption)
		m_pCategoryDSCOption->ItemRemove();

	if (m_pCategoryDSCSystemOption)
		m_pCategoryDSCSystemOption->ItemRemove();

	m_pDSCMode		= NULL;
	m_pDSCSize		= NULL;
	m_pDSCQuality	= NULL;
	m_pDSCShutter	= NULL;
	m_pDSCAperture	= NULL;
	m_pDSCISO		= NULL;
	m_pDSCWB		= NULL;
	m_pDSCMeter		= NULL;
	m_pDSCCT		= NULL;
	m_pDSCEVC		= NULL;
	m_PictureWizard = NULL;
	m_pDSCZoom		= NULL;
	m_MovieSize		= NULL;
	m_pDSCESTabilization = NULL;
	m_pDSCPhotoStyle= NULL;
	m_pDSCCTEX1		= NULL;
	m_pDSCCTEX2		= NULL;
	m_pDSCCTEX3		= NULL;
	m_pDSCCTEX4		= NULL;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	m_Focus			= NULL;
	m_pDSCFocusGH5	= NULL;
	m_pDSCFWVersion = NULL;
	m_pDSCISOStep	= NULL;
	m_pDSCISOExpansion	= NULL;
	m_pDSCLCD		= NULL;	
	m_pDSCPeaking	= NULL;
	m_pDSCSystemFrequency	= NULL;
	m_pDSCLensPowerCtrl = NULL;
	//joonho.kim 2017.05.16 불필요 Property 제거
	//GetDSCMode();
	//GetDSCSize();
	//GetDSCQuality();
	//GetDSCAperture();
	//GetDSCMeter();
	//GetDSCEVC();
	
	CString strModel;
	if(m_pItem)
	{
		strModel = m_pItem->GetDeviceModel();
	}

	if(strModel.CompareNoCase(_T("GH5")) == 0)
	{
		GetDSCSystemFrequency();	// ver 0.D 이상 (NTSC/PAL -> MovieSize 보다 먼저 호출해야함.)

		GetDSCMovieSize();
		GetDSCShutter();
		GetDSCISO();
		GetDSCWB();
		GetDSCCTEX();
		GetDSCWBAdjustAB();
		GetDSCWBAdjustMG();
		GetDSCAperture();
		GetDSCEStabilization();
		GetDSCFocus();
		GetDSCFWVersion();
		GetDSCISOStep();
		GetDSCISOExpansion();
		
		// ver 0.D 이상
		GetDSCPhotoStyle();	
		GetDSCPSContrast();
		GetDSCPSSharpness();
		GetDSCPSNoiseReduction();
		GetDSCPSSaturation();
		GetDSCPSHue();
		GetDSCPSFilterEffect();
 		GetDSCLCD();			
 		GetDSCPeaking();	

		//GetDSCLensPowerCtrl();// ver 0.F4 이상 //  [2018/5/14/ stim]
		
	}
	else //NX Series
	{
		GetDSCPW();
		//GetDSCZoom();
		GetDSCShutter();
		GetDSCISO();
		GetDSCWB();
		GetDSCCT();
		GetDSCMovieSize();
	}
	
	
}

//------------------------------------------------------------------------------
//! @function	Get Device Property		
//! @brief				
//! @date		2013-05-04
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision	
//------------------------------------------------------------------------------
void CESMPropertyCtrl::GetPTPProperty(int nPropCode)
{
	
	SdiMessage* pMsg = new SdiMessage;
	pMsg->message = WM_SDI_OP_GET_PROPERTY_DESC;
	pMsg->nParam1 = nPropCode;
	if(m_pItem)
	{
		m_pItem->SdiAddMsg((ESMEvent*)pMsg);
	}
}

//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
void CESMPropertyCtrl::GetPTPPropertyFocus(int nPropCode)
{
	CString strModel;
	if(m_pItem)
	{
		strModel = m_pItem->GetDeviceModel();
	}

	if(strModel.CompareNoCase(_T("GH5")) == 0)
	{
		GetPTPProperty(nPropCode);
	}
	else
	{
		SdiMessage* pMsg = new SdiMessage;
		//pMsg->message = WM_SDI_OP_GET_FOCUS_VALUE;
		pMsg->message = WM_SDI_OP_GETITEMFOCUS_VALUE;

		pMsg->nParam1 = nPropCode;
		m_pItem->SdiAddMsg((ESMEvent*)pMsg);
	}
}


//------------------------------------------------------------------------------
//! @function	Load Property
//! @brief				
//! @date		2013-05-04
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMPropertyCtrl::LoadPropertyInfo(ESMEvent* pMsg)
{
	if(!pMsg)
		return;

	CSdiSingleMgr* pSdiMgr = (CSdiSingleMgr*)pMsg->pDest;
	//-- Check Property DSC
	if(!pSdiMgr)
		return;

	if(!m_pItem)
		return;

	if(m_pItem->GetDeviceDSCID() != pSdiMgr->GetDeviceDSCID())
		return;

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Load Information 
	IDeviceInfo *pDeviceInfo = (IDeviceInfo *)pMsg->pParam;
	if(!pDeviceInfo)
		return;	

	//EnableWindow(FALSE);	

	int nType = pDeviceInfo->GetPropCode();
	switch(nType)
	{
	case PTP_CODE_FUNCTIONALMODE			: LoadDSCMode		(pDeviceInfo);	break;
	case PTP_CODE_IMAGESIZE					: LoadDSCSize		(pDeviceInfo);	break;
	case PTP_CODE_COMPRESSIONSETTING		: LoadDSCQuality	(pDeviceInfo);	break;
	case PTP_CODE_SAMSUNG_SHUTTERSPEED		: LoadDSCShutter	(pDeviceInfo);	break;
	case PTP_CODE_F_NUMBER					: LoadDSCAperture	(pDeviceInfo);	break;
	case PTP_CODE_EXPOSUREINDEX				: LoadDSCISO		(pDeviceInfo);	break;
	case PTP_CODE_WHITEBALANCE				: LoadDSCWB			(pDeviceInfo);	break;
	case PTP_CODE_EXPOSUREMETERINGMODE		: LoadDSCMeter		(pDeviceInfo);	break;
	case PTP_DPC_WB_DETAIL_KELVIN			: break;
	case PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN	: LoadDSCCT			(pDeviceInfo);	break;
	case PTP_CODE_SAMSUNG_EV				: LoadDSCEVC		(pDeviceInfo);	break;
	case PTP_CODE_SAMSUNG_PICTUREWIZARD		: LoadDSCPictureWizard	(pDeviceInfo);	break;
	case PTP_CODE_SAMSUNG_ZOOM_CTRL_GET		: LoadDSCZoom		(pDeviceInfo);	break;
	case PTP_CODE_SAMSUNG_MOV_SIZE			: LoadDSCMovieSize	(pDeviceInfo);	break;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	case PTP_OC_SAMSUNG_GetFocusPosition	: LoadDSCFocus		(pDeviceInfo);	break;
	case PTP_DPC_PHOTO_STYLE				: LoadDSCPhotoStyle	(pDeviceInfo);	break;
	case PTP_DPC_PS_CONTRAST				: LoadDSCPSContrast	(pDeviceInfo);	break;
	case PTP_DPC_PS_SHARPNESS				: LoadDSCPSSharpness (pDeviceInfo);	break;
	case PTP_DPC_PS_NOISE_REDUCTION			: LoadDSCPSNoiseReduction	(pDeviceInfo);	break;
	case PTP_DPC_PS_SATURATION				: LoadDSCPSSaturation(pDeviceInfo);	break;
	case PTP_DPC_PS_HUE						: LoadDSCPSHue	(pDeviceInfo);	break;
	case PTP_DPC_PS_FILTER_EFFECT			: LoadDSCPSFilterEffect (pDeviceInfo);	break;
	case PTP_DPC_WB_ADJUST_AB				: LoadDSCWBAdjustAB	(pDeviceInfo);	break;
	case PTP_DPC_WB_ADJUST_MG				: LoadDSCWBAdjustMG	(pDeviceInfo);	break;
	case PTP_DPC_MOV_STA_E_STABILIZATION	: LoadDSCEStabilization	(pDeviceInfo);	break;
	case PTP_DPC_SAMSUNG_FW_VERSION			: LoadDSCFWVersion	(pDeviceInfo);	break;
	case PTP_DPC_SAMSUNG_ISO_STEP			: LoadDSCISOStep	(pDeviceInfo);	break;
	case PTP_DPC_SAMSUNG_ISO_EXPANSION		: LoadDSCISOExpansion	(pDeviceInfo);	break;
	case PTP_DPC_LCD						: LoadDSCLCD		(pDeviceInfo);	break;
	case PTP_DPC_PEAKING					: LoadDSCPeaking	(pDeviceInfo);	break;
	case PTP_DPC_SET_SYSTEM_FREQUENCY		: LoadDSCSystemFrequency	(pDeviceInfo);	break;	
	case PTP_DPC_LENS_POWER_CTRL			: LoadDSCLensPower	(pDeviceInfo);	break;	
	}

	PropertyStoreSynchronize();
	EnableWindow(TRUE);
	SetRedraw(TRUE);	
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-05-04
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMPropertyCtrl::LoadDSCMode(IDeviceInfo *pDeviceInfo)
{	
	if(m_pDSCMode)
		return;

	m_pDSCMode = new CExtPropertyValue(PROPERTY_ITEM_MODE);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCMode ) );

	//-- 2013-05-04 hongsu@esmlab.com
	//-- String Value
	//-- String List
	if( m_pDSCMode )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCMode, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCMode->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCSize(IDeviceInfo *pDeviceInfo)
{	
	if(m_pDSCSize)
		return;

	m_pDSCSize = new CExtPropertyValue(PROPERTY_ITEM_SIZE);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCSize ) );

	//-- 2013-05-04 hongsu@esmlab.com
	//-- String Value
	//-- String List
	if( m_pDSCSize )
	{
		SetPropertyComboBox(m_pDSCSize, (CStringArray*)pDeviceInfo->GetPropValueListStr(), pDeviceInfo->GetCurrentStr(), NULL);
		m_pDSCSize->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCQuality(IDeviceInfo *pDeviceInfo)
{
	if(m_pDSCQuality)
		return;

	m_pDSCQuality = new CExtPropertyValue(PROPERTY_ITEM_QUALITY);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCQuality ) );

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Enum Value
	//-- Enum List
	if( m_pDSCQuality )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCQuality, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCQuality->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCShutter(IDeviceInfo *pDeviceInfo)
{
	if(m_pDSCShutter)
		return;

	m_pDSCShutter = new CExtPropertyValue(PROPERTY_ITEM_SHUTTER);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCShutter ) );

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Enum Value
	//-- Enum List
	if( m_pDSCShutter )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCShutter, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCShutter->ValueActiveFromDefault();			
	}
}

void CESMPropertyCtrl::LoadDSCAperture(IDeviceInfo *pDeviceInfo)
{
	if(m_pDSCAperture)
		return;

	m_pDSCAperture = new CExtPropertyValue(PROPERTY_ITEM_APERTURE);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCAperture ) );

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Enum Value
	//-- Enum List
	if( m_pDSCAperture )
	{
// 		{
// 			//-- 2013-05-06 hongsu@esmlab.com
// 			//-- Just Shutter Mode 
// 			//-- SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCAperture, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
// 		}
// 
// 		{
// 			CString strValue;
// 			strValue = GetPropertyValueStr(pDeviceInfo->GetPropCode(),pDeviceInfo->GetCurrentEnum());						
// 			//m_pDSCAperture->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->ModifyStyle(__EGCS_NO_INPLACE_CONTROL | __EGCS_EX_NO_AUTO_EDIT);
// 			m_pDSCAperture->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->ModifyStyle(__EGCS_EX_NO_AUTO_EDIT);
// 			m_pDSCAperture->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strValue );			
// 
// 		}
		CUIntArray* pArList = (CUIntArray*)pDeviceInfo->GetPropValueListEnum();
		CString strTp;
// 		int nPropCode = PTP_DPC_F_NUMBER;
// 		vector<CString> arrData;
// 		if(pArList)
// 		{
// 			int nAll = (int)pArList->GetCount();
// 			int nVectorIndex = 0;
// 			for( int i =0 ;i< nAll; i++)
// 			{
// 				strTp = GetPropertyValueStr(nPropCode, pArList->GetAt(i));
// 				for(nVectorIndex =0 ;nVectorIndex < arrData.size(); nVectorIndex++)
// 				{
// 					if(strTp == arrData.at(nVectorIndex))
// 						break;
// 				}
// 				if( nVectorIndex == arrData.size())
// 				{
// 					arrData.push_back(strTp);
// 				}
// 				else
// 				{
// 					pArList->RemoveAt(i);
// 					nAll--;
// 					i--;
// 				}
// 			}
// 		} 
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCAperture, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCAperture->ValueActiveFromDefault();
		
	}
}

void CESMPropertyCtrl::LoadDSCPictureWizard(IDeviceInfo *pDeviceInfo)
{
	if(m_PictureWizard)
		return;

	m_PictureWizard = new CExtPropertyValue(PROPERTY_ITEM_PICTUREWIZARD);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_PictureWizard ) );

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Enum Value
	//-- Enum List
	if( m_PictureWizard )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_PictureWizard, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_PictureWizard->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCMovieSize(IDeviceInfo *pDeviceInfo)
{
	if(m_MovieSize)
		return;

	m_MovieSize = new CExtPropertyValue(PROPERTY_ITEM_MOVIESIZE);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_MovieSize ) );

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Enum Value
	//-- Enum List
	if( m_MovieSize )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_MovieSize, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_MovieSize->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCPhotoStyle(IDeviceInfo *pDeviceInfo)
{
	if(m_pDSCPhotoStyle)
		return;

	m_pDSCPhotoStyle = new CExtPropertyValue(PROPERTY_ITEM_PHOTO_STYLE);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCPhotoStyle ) );

	if( m_pDSCPhotoStyle )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCPhotoStyle, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCPhotoStyle->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCEStabilization( IDeviceInfo *pDeviceInfo )
{
	if (m_pDSCESTabilization)
		return;

	m_pDSCESTabilization = new CExtPropertyValue(PROPERTY_ITEM_E_STABILIZATION);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCESTabilization ) );

	if (m_pDSCESTabilization)
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCESTabilization, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCESTabilization->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCPSContrast( IDeviceInfo *pDeviceInfo )
{
	if (!pDeviceInfo)
		return;

	CString strTemp;
	strTemp.Format(_T("%d"), pDeviceInfo->GetCurrentEnum());

	m_pItem->SetDSCCaptureInfo(PTP_DPC_PS_CONTRAST, strTemp);
}

void CESMPropertyCtrl::LoadDSCPSSharpness( IDeviceInfo *pDeviceInfo )
{
	if (!pDeviceInfo)
		return;

	CString strTemp;
	strTemp.Format(_T("%d"), pDeviceInfo->GetCurrentEnum());

	m_pItem->SetDSCCaptureInfo(PTP_DPC_PS_SHARPNESS, strTemp);
}

void CESMPropertyCtrl::LoadDSCPSNoiseReduction( IDeviceInfo *pDeviceInfo )
{
	if (!pDeviceInfo)
		return;

	CString strTemp;
	strTemp.Format(_T("%d"), pDeviceInfo->GetCurrentEnum());

	m_pItem->SetDSCCaptureInfo(PTP_DPC_PS_NOISE_REDUCTION, strTemp);
}

void CESMPropertyCtrl::LoadDSCPSSaturation( IDeviceInfo *pDeviceInfo )
{
	if (!pDeviceInfo)
		return;

	CString strTemp;
	strTemp.Format(_T("%d"), pDeviceInfo->GetCurrentEnum());

	m_pItem->SetDSCCaptureInfo(PTP_DPC_PS_SATURATION, strTemp);
}

void CESMPropertyCtrl::LoadDSCPSHue( IDeviceInfo *pDeviceInfo )
{
	if (!pDeviceInfo)
		return;

	CString strTemp;
	strTemp.Format(_T("%d"), pDeviceInfo->GetCurrentEnum());

	m_pItem->SetDSCCaptureInfo(PTP_DPC_PS_HUE, strTemp);
}

void CESMPropertyCtrl::LoadDSCPSFilterEffect( IDeviceInfo *pDeviceInfo )
{
	if (!pDeviceInfo)
		return;

	CString strTemp;
	strTemp.Format(_T("%d"), pDeviceInfo->GetCurrentEnum());

	m_pItem->SetDSCCaptureInfo(PTP_DPC_PS_FILTER_EFFECT, strTemp);
}

void CESMPropertyCtrl::LoadDSCWBAdjustAB( IDeviceInfo *pDeviceInfo )
{
	if (!pDeviceInfo)
		return;

	CString strTemp;
	strTemp.Format(_T("%d"), pDeviceInfo->GetCurrentEnum());

	m_pItem->SetDSCCaptureInfo(PTP_DPC_WB_ADJUST_AB, strTemp);
}

void CESMPropertyCtrl::LoadDSCWBAdjustMG( IDeviceInfo *pDeviceInfo )
{
	if (!pDeviceInfo)
		return;

	CString strTemp;
	strTemp.Format(_T("%d"), pDeviceInfo->GetCurrentEnum());

	m_pItem->SetDSCCaptureInfo(PTP_DPC_WB_ADJUST_MG, strTemp);
}

void CESMPropertyCtrl::LoadDSCISO(IDeviceInfo *pDeviceInfo)
{
	if(m_pDSCISO)
		return;

	m_pDSCISO = new CExtPropertyValue(PROPERTY_ITEM_ISO);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCISO ) );

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Enum Value
	//-- Enum List
	if( m_pDSCISO )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCISO, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCISO->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCWB(IDeviceInfo *pDeviceInfo)
{
	if(m_pDSCWB)
		return;

	m_pDSCWB = new CExtPropertyValue(PROPERTY_ITEM_WB);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCWB ) );

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Enum Value
	//-- Enum List
	if( m_pDSCWB )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCWB, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCWB->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCMeter(IDeviceInfo *pDeviceInfo)
{
	if(m_pDSCMeter)
		return;

	m_pDSCMeter = new CExtPropertyValue(PROPERTY_ITEM_METER);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCMeter ) );

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Enum Value
	//-- Enum List
	if( m_pDSCMeter )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCMeter, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCMeter->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCCT(IDeviceInfo *pDeviceInfo)
{
	if(m_pDSCCT)
		return;

	m_pDSCCT = new CExtPropertyValue(PROPERTY_ITEM_CT);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCCT ) );

	CUIntArray uEnum;
	for(int i=2500 ; i < 10001 ; i+=100)
	{
		uEnum.Add(i);
		
		//jhhan 16-09-09
		//ESMLog(5, _T("Color Temperature : %d"),i);
	}
	//-- 2013-05-04 hongsu@esmlab.com
	//-- Enum Value
	//-- Enum List
	if( m_pDSCCT )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCCT, &uEnum, pDeviceInfo->GetCurrentEnum());
		m_pDSCCT->ValueActiveFromDefault();
	}
}

void CESMPropertyCtrl::LoadDSCCTEX(IDeviceInfo *pDeviceInfo,int nType)
{
	CExtPropertyValue* pCCT = NULL;
	CString strTitle;
	pCCT = m_pDSCCTEX1; 
	strTitle = PROPERTY_ITEM_CT;


	if(pCCT)
		return;

	pCCT = new CExtPropertyValue(strTitle);
	VERIFY( m_pCategoryDSCOption->ItemInsert( pCCT ) );

	
	if( pCCT )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), pCCT, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		pCCT->ValueActiveFromDefault();
	}
}

void CESMPropertyCtrl::LoadDSCEVC(IDeviceInfo *pDeviceInfo)
{
	if(m_pDSCEVC)
		return;

	m_pDSCEVC = new CExtPropertyValue(PROPERTY_ITEM_EVC);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCEVC ) );

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Enum Value
	//-- Enum List
	if( m_pDSCEVC )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCEVC, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCEVC->ValueActiveFromDefault();
	}
}

void CESMPropertyCtrl::LoadDSCZoom(IDeviceInfo *pDeviceInfo)
{
	if(m_pDSCZoom)
		return;

	m_pDSCZoom = new CExtPropertyValue(PROPERTY_ITEM_ZOOM);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCZoom ) );


	//-- 2013-05-04 hongsu@esmlab.com
	//-- Enum Value
	//-- Enum List
	if( m_pDSCZoom )
	{
		CString strValue = GetPropertyValueStr(pDeviceInfo->GetPropCode(), pDeviceInfo->GetCurrentEnum());
		m_pDSCZoom->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(strValue);			
		m_pDSCZoom->ValueActiveFromDefault();		
	}
}

void CESMPropertyCtrl::LoadDSCFWVersion( IDeviceInfo *pDeviceInfo )
{
	if(m_pDSCFWVersion)
		return;

	m_pDSCFWVersion = new CExtPropertyValue(PROPERTY_ITEM_FW_VERSION);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCFWVersion ) );

	if( m_pDSCFWVersion )
	{
		CString strValue = GetPropertyValueStr(pDeviceInfo->GetPropCode(), pDeviceInfo->GetCurrentEnum());
		m_pDSCFWVersion->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strValue );			
		m_pDSCFWVersion->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCISOStep( IDeviceInfo *pDeviceInfo )
{
	if(m_pDSCISOStep)
		return;

	m_pDSCISOStep = new CExtPropertyValue(PROPERTY_ITEM_ISO_STEP);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCISOStep ) );

	if( m_pDSCISOStep )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCISOStep, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCISOStep->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCISOExpansion( IDeviceInfo *pDeviceInfo )
{
	if(m_pDSCISOExpansion)
		return;

	m_pDSCISOExpansion = new CExtPropertyValue(PROPERTY_ITEM_ISO_EXPANSION);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCISOExpansion ) );

	if( m_pDSCISOExpansion )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCISOExpansion, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCISOExpansion->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCLCD( IDeviceInfo *pDeviceInfo )
{
	if(m_pDSCLCD)
		return;

	m_pDSCLCD = new CExtPropertyValue(PROPERTY_ITEM_LCD);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCLCD ) );

	if( m_pDSCLCD )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCLCD, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCLCD->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCPeaking( IDeviceInfo *pDeviceInfo )
{
	if(m_pDSCPeaking)
		return;

	m_pDSCPeaking = new CExtPropertyValue(PROPERTY_ITEM_PEAKING);
	VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCPeaking ) );

	if( m_pDSCPeaking )
	{
		SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_pDSCPeaking, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
		m_pDSCPeaking->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCSystemFrequency( IDeviceInfo *pDeviceInfo )
{
	if(m_pDSCSystemFrequency)
		return;

	m_pDSCSystemFrequency = new CExtPropertyValue(PROPERTY_ITEM_SYSTEM_FREQUENCY);
	VERIFY( m_pCategoryDSCSystemOption->ItemInsert( m_pDSCSystemFrequency ) );

	if( m_pDSCSystemFrequency )
	{
		CString strValue = GetPropertyValueStr(pDeviceInfo->GetPropCode(), pDeviceInfo->GetCurrentEnum());
		m_pDSCSystemFrequency->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strValue );			
		m_pDSCSystemFrequency->ValueActiveFromDefault();	
	}
}

void CESMPropertyCtrl::LoadDSCLensPower( IDeviceInfo *pDeviceInfo )
{
	if(m_pDSCLensPowerCtrl)
		return;

	m_pDSCLensPowerCtrl = new CExtPropertyValue(PROPERTY_ITEM_LENS_POWER);
	VERIFY( m_pCategoryDSCSystemOption->ItemInsert( m_pDSCLensPowerCtrl ) );

	if( m_pDSCLensPowerCtrl )
	{
		CString strValue = GetPropertyValueStr(pDeviceInfo->GetPropCode(), pDeviceInfo->GetCurrentEnum());
		m_pDSCLensPowerCtrl->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strValue );			
		m_pDSCLensPowerCtrl->ValueActiveFromDefault();	
	}
}

//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
void CESMPropertyCtrl::LoadDSCFocus(IDeviceInfo *pDeviceInfo)
{
	CString strModel;
	if(m_pItem)
	{
		strModel = m_pItem->GetDeviceModel();
	}

	if(strModel.CompareNoCase(_T("GH5")) == 0)
	{
		CString strValue;
		strValue.Format(_T("%d"), pDeviceInfo->GetCurrentEnum());

		if(m_pDSCFocusGH5)
		{
			m_pDSCFocusGH5->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strValue );			
			return;
		}

		m_pDSCFocusGH5 = new CExtPropertyValue(PROPERTY_ITEM_FOCUS);
		VERIFY( m_pCategoryDSCOption->ItemInsert( m_pDSCFocusGH5 ) );

		if( m_pDSCFocusGH5 )
		{
			m_pDSCFocusGH5->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strValue );			
			m_pDSCFocusGH5->ValueActiveFromDefault();	

			m_pItem->SetFocus(pDeviceInfo->GetCurrentEnum());
		}
	}
	else
	{
		if(m_Focus)
			return;

		m_Focus = new CExtPropertyValue(PROPERTY_ITEM_FOCUS);
		VERIFY( m_pCategoryDSCOption->ItemInsert( m_Focus ) );

		if( m_Focus )
		{
			SetPropertyComboBox(pDeviceInfo->GetPropCode(), m_Focus, (CUIntArray*)pDeviceInfo->GetPropValueListEnum(), pDeviceInfo->GetCurrentEnum());
			m_Focus->ValueActiveFromDefault();	
		}
	}
	
}
//------------------------------------------------------------------------------
//! @function	Load Property
//! @brief				
//! @date		2013-05-04
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
void CESMPropertyCtrl::LoadPropertyNetInfo(ESMEvent* pMsg)
{
	if(!pMsg)
		return;

	CString* pDSCID = (CString*)pMsg->pDest;
	int nType = (int)pMsg->nParam1;
	//-- Check Property DSC
	if(!pDSCID)
		return;
	CString strDscId = *pDSCID;
	if (pDSCID)
	{
		delete pDSCID;
		pDSCID = NULL;
	}

	if(!m_pItem)
		return;

	if(m_pItem->GetDeviceDSCID() != strDscId)
	{
		if(nType == PTP_CODE_IMAGESIZE || nType == PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN)
		{
			if (pMsg->pParam)
			{
				delete (CStringArray*)pMsg->pParam;
				pMsg->pParam = NULL;
			}
		}
		else
		{
			if (pMsg->pParam)
			{
				delete (CUIntArray*)pMsg->pParam;
				pMsg->pParam = NULL;
			}
		}
		return;
	}

	//EnableWindow(FALSE);	

	CExtPropertyValue* pDSCProp = NULL;
	CExtPropertyItem* pDSCPropItem = NULL;
	switch(nType)
	{
	case PTP_CODE_FUNCTIONALMODE			: if(m_pDSCMode) return;	pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_MODE)	; m_pDSCMode	= pDSCProp;	break;
	case PTP_CODE_IMAGESIZE					: if(m_pDSCSize) return;	pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_SIZE)	; m_pDSCSize	= pDSCProp;	break;
	case PTP_CODE_COMPRESSIONSETTING		: if(m_pDSCQuality) return;	pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_QUALITY)	; m_pDSCQuality = pDSCProp;	break;
	case PTP_CODE_SAMSUNG_SHUTTERSPEED		: if(m_pDSCShutter) return;	pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_SHUTTER)	; m_pDSCShutter = pDSCProp;	break;
	case PTP_CODE_F_NUMBER					: if(m_pDSCAperture) return;pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_APERTURE); m_pDSCAperture = pDSCProp;break;
	case PTP_CODE_EXPOSUREINDEX				: if(m_pDSCISO) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_ISO)		; m_pDSCISO		= pDSCProp; m_arrIsoData.clear();	break;
	case PTP_CODE_WHITEBALANCE				: if(m_pDSCWB) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_WB)		; m_pDSCWB		= pDSCProp;	break;
	case PTP_CODE_EXPOSUREMETERINGMODE		: if(m_pDSCMeter) return;	pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_METER)	; m_pDSCMeter	= pDSCProp;	break;
	case PTP_CODE_SAMSUNG_EV				: if(m_pDSCEVC) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_EVC)		; m_pDSCEVC		= pDSCProp;	break;
	case PTP_CODE_SAMSUNG_PICTUREWIZARD		: if(m_PictureWizard) return;pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_PICTUREWIZARD)		; m_PictureWizard		= pDSCProp;	break;
	case PTP_CODE_SAMSUNG_MOV_SIZE			: if(m_MovieSize) return;	pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_MOVIESIZE)		; m_MovieSize		= pDSCProp;	break;	
	case PTP_CODE_SAMSUNG_ZOOM_CTRL_GET		: if(m_pDSCZoom)	return;	pDSCPropItem = new CExtPropertyValue(PROPERTY_ITEM_ZOOM); m_pDSCZoom	= pDSCPropItem;	break;
	case PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN	: 
	case PTP_DPC_WB_DETAIL_KELVIN			:if(m_pDSCCT) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_CT)		; m_pDSCCT		= pDSCProp;	break;
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM1	:break;
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM2	:break;
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM3	:break;
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM1_ETC	:break;
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM2_ETC	:break;
	case PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM3_ETC	:break;		
	case PTP_DPC_PHOTO_STYLE				: if(m_pDSCPhotoStyle) return;pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_PHOTO_STYLE)		; m_pDSCPhotoStyle		= pDSCProp;	break;
	case PTP_DPC_WB_ADJUST_AB :break;
	case PTP_DPC_WB_ADJUST_MG :break;
	case PTP_DPC_MOV_STA_E_STABILIZATION	: if(m_pDSCESTabilization) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_E_STABILIZATION)		; m_pDSCESTabilization		= pDSCProp;	break;
	case PTP_DPC_SAMSUNG_FW_VERSION			: if(m_pDSCFWVersion) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_FW_VERSION)		; m_pDSCFWVersion		= pDSCProp;	break;
	case PTP_DPC_SAMSUNG_ISO_STEP			: if(m_pDSCISOStep) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_ISO_STEP)		; m_pDSCISOStep		= pDSCProp;	break;
	case PTP_DPC_SAMSUNG_ISO_EXPANSION		: if(m_pDSCISOExpansion) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_ISO_EXPANSION)		; m_pDSCISOExpansion		= pDSCProp;	break;
	case PTP_DPC_LCD						: if(m_pDSCLCD) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_LCD)		; m_pDSCLCD		= pDSCProp;	break;
	case PTP_DPC_PEAKING					: if(m_pDSCPeaking) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_PEAKING)		; m_pDSCPeaking		= pDSCProp;	break;
	case PTP_DPC_SET_SYSTEM_FREQUENCY		: if(m_pDSCSystemFrequency) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_SYSTEM_FREQUENCY)		; m_pDSCSystemFrequency		= pDSCProp;	break;
	case PTP_DPC_LENS_POWER_CTRL			: if(m_pDSCLensPowerCtrl) return;		pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_LENS_POWER)		; m_pDSCLensPowerCtrl		= pDSCProp;	break;
		//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	case PTP_CODE_SAMSUNG_GetFocusPosition	: 
		{
			CString strModel = m_pItem->GetDeviceModel();
			if(strModel.CompareNoCase(_T("GH5")) == 0)
			{
				if(m_pDSCFocusGH5) return;		
				pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_FOCUS);
				m_pDSCFocusGH5 = pDSCProp;	
			}
			else
			{
				if(m_Focus) return;		
				pDSCProp = new CExtPropertyValue(PROPERTY_ITEM_FOCUS);
				m_Focus	= pDSCProp;	
			}

			break;
		}
		return;
	}

	if( nType == PTP_CODE_SAMSUNG_ZOOM_CTRL_GET )
		VERIFY( m_pCategoryDSCOption->ItemInsert( pDSCPropItem ) );
	else if (nType == PTP_DPC_SET_SYSTEM_FREQUENCY)
		VERIFY( m_pCategoryDSCSystemOption->ItemInsert( m_pDSCSystemFrequency ) );
	else if (nType == PTP_DPC_LENS_POWER_CTRL)
		VERIFY( m_pCategoryDSCSystemOption->ItemInsert( m_pDSCLensPowerCtrl ) );
	else
	{
		if( pDSCProp )
			VERIFY( m_pCategoryDSCOption->ItemInsert( pDSCProp ) );
		else
			return;
	}

	if(nType == PTP_CODE_IMAGESIZE)
	{
		SetPropertyComboBox(pDSCProp, (CStringArray*)pMsg->pParam, "", (int)pMsg->nParam2);

		if (pMsg->pParam)
		{
			delete (CStringArray*)pMsg->pParam;
			pMsg->pParam = NULL;
		}
	}
	else if (nType == PTP_DPC_SET_SYSTEM_FREQUENCY)
	{
		CString strValue = GetPropertyValueStr(nType, (int)pMsg->nParam2);
		m_pDSCSystemFrequency->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strValue );			

		if (pMsg->pParam)
		{
			delete (CStringArray*)pMsg->pParam;
			pMsg->pParam = NULL;
		}
	}
	else if (nType == PTP_DPC_LENS_POWER_CTRL)
	{
		CString strValue = GetPropertyValueStr(nType, (int)pMsg->nParam2);
		m_pDSCLensPowerCtrl->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet( strValue );			

		if (pMsg->pParam)
		{
			delete (CStringArray*)pMsg->pParam;
			pMsg->pParam = NULL;
		}
	}
	else if(nType == PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN)
	{
		CUIntArray uEnum;
		for(int i=2500 ; i < 10001 ; i+=100)
		{
			uEnum.Add(i);
			//jhhan 16-09-09
			//ESMLog(5, _T("Color Temperature : %d"),i);
		}

		SetPropertyComboBox(nType, pDSCProp, &uEnum, (int)pMsg->nParam2);
		if (pMsg->pParam)
		{
			delete (CStringArray*)pMsg->pParam;
			pMsg->pParam = NULL;
		}
	}
	else if(nType == PTP_CODE_SAMSUNG_ZOOM_CTRL_GET)
	{
		CString strValue = GetPropertyValueStr(nType, pMsg->nParam2);
		pDSCPropItem->ValueDefaultGetByRTC( RUNTIME_CLASS(CExtGridCellString) )->TextSet(strValue);	
		pDSCPropItem->ValueActiveFromDefault();	

		PropertyStoreSynchronize();
		SetRedraw(TRUE);
		return;
	}
	else
	{
		if( PTP_CODE_SAMSUNG_MOV_SIZE == nType)
		{
			int movie_size = ESM_MOVIESIZE_FHD;
			if( (int)pMsg->nParam2 == 0x86 || (int)pMsg->nParam2 == 0x84)
			{
				if( movie_frame_per_second != 30)
				{
					ESMEvent* pMsg	= new ESMEvent;
					pMsg->message	= WM_ESM_MOVIE_SET_FPS;
					pMsg->nParam1	= 30;
					::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_MOVIE, (LPARAM)pMsg);
				}
				movie_frame_per_second = 30;
				movie_next_frame_time = 33;
				movie_size = ESM_MOVIESIZE_FHD;

				ESMEvent* pMsg2 = new ESMEvent;
				pMsg2->message = WM_ESM_MOVIE_SET_IMAGESIZE;
				pMsg2->nParam1 = movie_size;
				pMsg2->nParam2 = ESMGetMovieFlag();
				pMsg2->nParam3 = ESMGetVMCCFlag();
				::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_MOVIE, (LPARAM)pMsg2);
			}
			else if( (int)pMsg->nParam2 == 0x92)
			{
				if( movie_frame_per_second != 25)
				{
					ESMEvent* pMsg	= new ESMEvent;
					pMsg->message	= WM_ESM_MOVIE_SET_FPS;
					pMsg->nParam1	= 25;
					::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_MOVIE, (LPARAM)pMsg);
				}
				movie_frame_per_second = 25;
				movie_next_frame_time = 40;
				movie_size = ESM_MOVIESIZE_UHD;

				ESMEvent* pMsg2 = new ESMEvent;
				pMsg2->message = WM_ESM_MOVIE_SET_IMAGESIZE;
				pMsg2->nParam1 = movie_size;
				pMsg2->nParam2 = ESMGetMovieFlag();
				pMsg2->nParam3 = ESMGetVMCCFlag();
				::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_MOVIE, (LPARAM)pMsg2);
			}
			else if( (int)pMsg->nParam2 == 0x82)
			{
				if( movie_frame_per_second != 30)
				{
					ESMEvent* pMsg	= new ESMEvent;
					pMsg->message	= WM_ESM_MOVIE_SET_FPS;
					pMsg->nParam1	= 30;
					::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_MOVIE, (LPARAM)pMsg);
				}
				movie_frame_per_second = 30;
				movie_next_frame_time = 33;
				movie_size = ESM_MOVIESIZE_UHD;

				
				ESMEvent* pMsg2 = new ESMEvent;
				pMsg2->message = WM_ESM_MOVIE_SET_IMAGESIZE;
				pMsg2->nParam1 = movie_size;
				pMsg2->nParam2 = ESMGetMovieFlag();
				pMsg2->nParam3 = ESMGetVMCCFlag();
				::SendMessage(ESMGetMainWnd(), WM_ESM, WM_ESM_MOVIE, (LPARAM)pMsg2);
			}
			ESMSetMovieSize(movie_size);
		}
		SetPropertyComboBox(nType, pDSCProp, (CUIntArray*)pMsg->pParam, (int)pMsg->nParam2);

		// GH5 버전별 분기...
		if (nType == PTP_DPC_SAMSUNG_FW_VERSION)
		{
			int nVer = (int)pMsg->nParam2;	
			if (nVer >= 0x000000f4)
			{
				GetDSCLensPowerCtrl();		// F4 버전 이상..
			}
		}

		if (pMsg->pParam)
		{
			delete (CUIntArray*)pMsg->pParam;
			pMsg->pParam = NULL;
		}
	}

	pDSCProp->ValueActiveFromDefault();	

	PropertyStoreSynchronize();
	//EnableWindow(TRUE);
	SetRedraw(TRUE);
}

