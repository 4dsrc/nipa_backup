////////////////////////////////////////////////////////////////////////////////
//
//	ESMPropertyView.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-04
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include "ESMPropertyCtrl.h"
#include "SdiDefines.h"
#include "LoadFocusDlg.h"

class CSdiSingleMgr;

/////////////////////////////////////////////////////////////////////////////
// CESMPropertyView dialog

class CESMPropertyView : public CDialog
{
	class CInnerToolControlBar : public CExtToolControlBar {virtual CExtBarContentExpandButton* OnCreateBarRightBtn() {return NULL;}};

	// Construction
public:
	int m_bInitStart;
	CESMPropertyView(CWnd* pParent = NULL);   // standard constructor
	virtual ~CESMPropertyView();

	// Dialog Data
	//{{AFX_DATA(CESMPropertyView)
	enum { IDD = IDD_VIEW_ONLY_TOOLBAR };
	//}}AFX_DATA
	void InitImageFrameWnd();

protected:
	//seo
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	CRect m_rcClientFrame;	
	CExtToolControlBar/*CInnerToolControlBar*/ m_wndToolBar;	
	HWND m_hMainWnd;		
	CLoadFocusDlg* m_LoadFocusDlg;

private:	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CESMPropertyView)

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	afx_msg void OnGetProperty();
	afx_msg void OnSetProperty();
	afx_msg void OnSetPropertyAll();
	afx_msg void OnSetPropertyFormat();
	afx_msg void OnSetPropertyFWUpdate();				//-- 2014-9-4 hongsu@esmlab.com
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	afx_msg void OnSetPropertyFocusALL();
	afx_msg void OnSetPropertyFocusSet();
	afx_msg void OnSetPropertyFocusGet();
	
	afx_msg void OnSetVoiceOff();
	afx_msg void OnSetPropertyCamShutdown();
	//seo
	afx_msg void OnSetMProperty();
	afx_msg void OnSetMPropertyAll();
	afx_msg void OnSetSave();
	afx_msg void OnSetLoad();
	afx_msg void OnSetFocusSave();
	afx_msg void OnSetFocusLoad();
	afx_msg void OnSetLcdOn();
	afx_msg void OnSetLcdOff();
	afx_msg void OnSetFocusZoom();
	
	afx_msg void OnSetPropertySensorCleaning();
	afx_msg void OnSetWhiteBalance();
	afx_msg void OnSetWhiteBalanceAll();

	afx_msg void OnSetPictureWizard();
	afx_msg LRESULT OnEsmLoadFocus(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEsmLoadFocusEnd(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnSetGOPMode();
protected:
	// Generated message map functions
	//{{AFX_MSG(CESMPropertyView)	
	afx_msg void OnPaint();
	//}}AFX_MSG	
	afx_msg void OnSize(UINT nType, int cx, int cy);

public:
	void ResultFocusMoveCount();
	CESMPropertyCtrl* m_pPropertyCtrl;
	afx_msg void OnSetISOUp();
	afx_msg void OnSetISODown();
	afx_msg void OnSetShuttersSpeedUp();
	afx_msg void OnSetShuttersSpeedDown();
	afx_msg void OnSetCTUp();
	afx_msg void OnSetCTDown();
	afx_msg void OnImageSetBanksave();
	void SetFocusSave(CString str);
	afx_msg void OnImageSetFrequency();
	afx_msg void OnImageSetLensPower();
};
