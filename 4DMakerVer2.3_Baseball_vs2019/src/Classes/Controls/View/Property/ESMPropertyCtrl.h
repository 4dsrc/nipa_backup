////////////////////////////////////////////////////////////////////////////////
//
//	ESMPropertyCtrl.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-03
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#if (!defined __AFXPRIV_H__)
	#include <AfxPriv.h>
#endif

#include "ESMIndex.h"
#include "SdiDefines.h"
#include "ptpIndex.h"
#include "DSCViewDefine.h"
#include "ESMPWEditorDlgGH5.h"

class CDSCItem;

//---------------------------------------------------------------------------
//-- PROPERTYWND - CAMERA
//--------------------------------------------------------------------------
#define PROPERTY_ITEM_DSC_BASIC			_T("Basic Infomation")
#define PROPERTY_ITEM_MODEL				_T("Model Name") 
#define PROPERTY_ITEM_UNIQUE			_T("Unique ID") 
#define PROPERTY_ITEM_ID				_T("DSC ID") 

#define PROPERTY_ITEM_DSC_OPTION		_T("Capture Option")
#define PROPERTY_ITEM_MODE				_T("Mode") 
#define PROPERTY_ITEM_SIZE				_T("Size") 
#define PROPERTY_ITEM_QUALITY			_T("Quality") 
#define PROPERTY_ITEM_SHUTTER			_T("Shutter Speed") 
#define PROPERTY_ITEM_APERTURE			_T("Aperture") 
#define PROPERTY_ITEM_ISO				_T("ISO") 
#define PROPERTY_ITEM_WB				_T("White Balance") 
#define PROPERTY_ITEM_METER				_T("Exposure Metering") 
#define PROPERTY_ITEM_CT				_T("Color Temperature") 
#define PROPERTY_ITEM_EVC				_T("EVC") 
#define PROPERTY_ITEM_PICTUREWIZARD		_T("Picture Wizard") 
#define PROPERTY_ITEM_ZOOM				_T("Zoom") 
#define PROPERTY_ITEM_MOVIESIZE			_T("Movie Size") 
//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
#define PROPERTY_ITEM_FOCUS			    _T("Focus") 
#define PROPERTY_ITEM_PHOTO_STYLE		_T("Photo Style") 
#define PROPERTY_ITEM_E_STABILIZATION	_T("E-Stabilization")
#define PROPERTY_ITEM_FW_VERSION		_T("FW Version")
#define PROPERTY_ITEM_ISO_STEP			_T("ISO Step")
#define PROPERTY_ITEM_ISO_EXPANSION		_T("ISO Expansion")
#define PROPERTY_ITEM_LCD				_T("LCD")
#define PROPERTY_ITEM_PEAKING			_T("Peaking")

#define PROPERTY_ITEM_DSC_SYSTEM_OPTION	_T("System Option")
#define PROPERTY_ITEM_SYSTEM_FREQUENCY	_T("System Frequency")
#define PROPERTY_ITEM_LENS_POWER		_T("Lens Power")

/////////////////////////////////////////////////////////////////////////////
// CESMPropertyCtrl window

class CESMPropertyCtrl : public CExtPropertyGridCtrl
{
	CExtPropertyStore m_PS;
	int m_nRGB;
	int m_nPTPCode;

	int m_nFocusMoveCount;

public:
	CESMPropertyCtrl();
	~CESMPropertyCtrl();

	enum FOCUS_STEP
	{
		STEP_NEAR = 4,
		STEP_FAR = 170
	};

protected:
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam );

private:
	CESMPWEditorDlgGH5* m_PWEditorDlgGH5;
	CDSCItem* m_pItem;
	//------------------------------------------------------
	//-- Basic Execution Option
	//------------------------------------------------------
	CExtPropertyItem* m_pCategoryDSCBasic	;
	CExtPropertyItem* m_pModelName			;
	CExtPropertyItem* m_pUniqueID			; 
	CExtPropertyItem* m_pDSCID				;

	//------------------------------------------------------
	//-- Capture Option
	//------------------------------------------------------
	CExtPropertyItem* m_pCategoryDSCOption;
	//-- ComboBox
	CExtPropertyValue* m_pDSCMode		;
	CExtPropertyValue* m_pDSCSize		;
	CExtPropertyValue* m_pDSCQuality	;
	CExtPropertyValue* m_pDSCShutter	;
	CExtPropertyValue* m_pDSCAperture	;	// F Number
	CExtPropertyValue* m_pDSCISO		;
	CExtPropertyValue* m_pDSCWB			;
	CExtPropertyValue* m_pDSCMeter		;
	CExtPropertyValue* m_pDSCCT			;
	CExtPropertyValue* m_pDSCEVC		;
	CExtPropertyValue* m_MovieSize		;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	CExtPropertyValue* m_Focus		;
	CExtPropertyItem* m_pDSCFocusGH5	;
	CExtPropertyValue* m_PictureWizard	;
	//GH5
	CExtPropertyValue* m_pDSCPhotoStyle	;
	CExtPropertyValue* m_pDSCCTEX1		;
	CExtPropertyValue* m_pDSCCTEX2		;
	CExtPropertyValue* m_pDSCCTEX3		;
	CExtPropertyValue* m_pDSCCTEX4		;
	CExtPropertyValue* m_pDSCESTabilization		;
	CExtPropertyItem* m_pDSCFWVersion	;
	CExtPropertyValue* m_pDSCISOStep	;
	CExtPropertyValue* m_pDSCISOExpansion		;
	CExtPropertyValue* m_pDSCLCD		;
	CExtPropertyValue* m_pDSCPeaking	;

	//------------------------------------------------------
	//-- System Option
	//------------------------------------------------------
	CExtPropertyItem* m_pCategoryDSCSystemOption;

	CExtPropertyItem* m_pDSCSystemFrequency	;
	CExtPropertyItem* m_pDSCLensPowerCtrl	;

	CExtPropertyItem* m_pDSCZoom		;	
	vector<CString> m_arrIsoData;

	int m_nStartIndex;
	int m_nEndIndex;

	BOOL SetPropertyComboBox(CExtPropertyValue* pItem, CStringArray* pArList, CString strCurrentName, int nCurrentValue);
	BOOL SetPropertyComboBox(int nPropCode, CExtPropertyValue* pItem, CUIntArray* pArList, int nCurrentValue);
	
	CString GetPropertyValueSrttoStr(int nPropCode, CString strValue);
	int GetPropertyValueEnum(int nPropCode, CString strValue);	
	int GetPropertyValueEnumCount(int nPropCode, CString strValue);
	void GetPropertySection(int nPropCode, CStringArray& arList);
	int GetPropertyValueStep(int nPropCode,CString strValue,BOOL m_flag);

public:
	//------------------------------------------------------------------------------
	//! @brief		Load Information
	//! @date		2013-05-04
	//! @owner		Hongsu Jung (hongsu@esmlab.com)
	//! @return		void
	//------------------------------------------------------------------------------
	void LoadBasicInfo		();

	void SetStartIndex(int nIndex) { m_nStartIndex = nIndex;}
	void SetEndIndex(int nIndex) { m_nEndIndex = nIndex;}
	int GetStartIndex() { return m_nStartIndex;}
	int GetEndIndex() { return m_nEndIndex;}
	//-- 2013-05-04 hongsu@esmlab.com
	//-- Load Property 
	void LoadDSCProperty	();
	//-- Get Information
	void GetPropertyAll(int nProp);
	void GetDSCProp		();
	void GetPTPProperty		(int nPropCode);
	void GetPTPPropertyFocus(int nPropCode);
	void GetDSCMode			()	{ GetPTPProperty(PTP_CODE_FUNCTIONALMODE			); } //0X5002); }
	void GetDSCSize			()	{ GetPTPProperty(PTP_CODE_IMAGESIZE					); } //0x5003); }
	void GetDSCQuality		()	{ GetPTPProperty(PTP_CODE_COMPRESSIONSETTING		); } //0x5004); }
	void GetDSCShutter		()	{ GetPTPProperty(PTP_CODE_SAMSUNG_SHUTTERSPEED		); } //0xD815); }
	void GetDSCAperture		()	{ GetPTPProperty(PTP_CODE_F_NUMBER					); } //0X5007); }
	void GetDSCISO			()	{ GetPTPProperty(PTP_CODE_EXPOSUREINDEX				); } //0x500F); }
	void GetDSCWB			()	{ GetPTPProperty(PTP_CODE_WHITEBALANCE				); } //0x5005); }
	void GetDSCPW			()	{ GetPTPProperty(PTP_CODE_SAMSUNG_PICTUREWIZARD		); } //0XD80F); }
	//  [5/16/2013 Administrator]
	void GetDSCMeter		()	{ GetPTPProperty(PTP_CODE_EXPOSUREMETERINGMODE		); } //0X500B); }
	void GetDSCCT			()	{ GetPTPProperty(PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN	); } //0xD82A); }
	void GetDSCCTEX			()	{ GetPTPProperty(PTP_DPC_WB_DETAIL_KELVIN			); } //0xE121); }
	void GetDSCEVC			()	{ GetPTPProperty(PTP_CODE_SAMSUNG_EV				); } //0xD801); }
	void GetDSCZoom			()	{ GetPTPProperty(PTP_CODE_SAMSUNG_ZOOM_CTRL_GET		); } //0xD97F); }
	void GetDSCMovieSize	()	{ GetPTPProperty(PTP_CODE_SAMSUNG_MOV_SIZE			); } //0xD84B); }
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	void GetDSCFocus		()	{ GetPTPPropertyFocus(PTP_OC_SAMSUNG_GetFocusPosition			); } //0x9009); }
	void GetDSCFWVersion	()	{ GetPTPProperty(PTP_DPC_SAMSUNG_FW_VERSION			); } //0xD991); }
	
	void GetDSCPhotoStyle	()	{ GetPTPProperty(PTP_DPC_PHOTO_STYLE				); } //0xE506); }
	void GetDSCPSContrast	()	{ GetPTPProperty(PTP_DPC_PS_CONTRAST				); } //0xE507); }
	void GetDSCPSSharpness	()	{ GetPTPProperty(PTP_DPC_PS_SHARPNESS				); } //0xE508); }
	void GetDSCPSNoiseReduction	()	{ GetPTPProperty(PTP_DPC_PS_NOISE_REDUCTION		); } //0xE509); }
	void GetDSCPSSaturation	()	{ GetPTPProperty(PTP_DPC_PS_SATURATION				); } //0xE50A); }
	void GetDSCPSHue		()	{ GetPTPProperty(PTP_DPC_PS_HUE						); } //0xE50B); }
	void GetDSCPSFilterEffect	()	{ GetPTPProperty(PTP_DPC_PS_FILTER_EFFECT		); } //0xE50C); }

	void GetDSCWBAdjustAB	()	{ GetPTPProperty(PTP_DPC_WB_ADJUST_AB				); } //0xE50D); }
	void GetDSCWBAdjustMG	()	{ GetPTPProperty(PTP_DPC_WB_ADJUST_MG				); } //0xE50E); }
	void GetDSCEStabilization	()	{ GetPTPProperty(PTP_DPC_MOV_STA_E_STABILIZATION	); } //0xE405); }
	void GetDSCISOStep		()	{ GetPTPProperty(PTP_CODE_SAMSUNG_ISOSTEP			); } //0xD802); }
	void GetDSCISOExpansion	()	{ GetPTPProperty(PTP_CODE_SAMSUNG_ISOEXPANSION		); } //0xD808); }
	void GetDSCLCD			()	{ GetPTPProperty(PTP_DPC_LCD						); } //0xE504); }
	void GetDSCPeaking		()	{ GetPTPProperty(PTP_DPC_PEAKING					); } //0xE505); }
	void GetDSCSystemFrequency(){ GetPTPProperty(PTP_DPC_SET_SYSTEM_FREQUENCY		); } //0xE503); }
	void GetDSCLensPowerCtrl()	{ GetPTPProperty(PTP_DPC_LENS_POWER_CTRL			); } //0xD981); }
	

	//-- Load Property Info
	void LoadDSCInfo(CDSCItem* pItem);	
	void LoadPropertyInfo(ESMEvent* pMsg);
	void LoadPropertyNetInfo(ESMEvent* pMsg);
	void LoadDSCMode		(IDeviceInfo *pDeviceInfo);
	void LoadDSCSize		(IDeviceInfo *pDeviceInfo);
	void LoadDSCQuality		(IDeviceInfo *pDeviceInfo);
	void LoadDSCShutter		(IDeviceInfo *pDeviceInfo);
	void LoadDSCAperture	(IDeviceInfo *pDeviceInfo);
	void LoadDSCISO			(IDeviceInfo *pDeviceInfo);
	void LoadDSCWB			(IDeviceInfo *pDeviceInfo);
	void LoadDSCMeter		(IDeviceInfo *pDeviceInfo);
	void LoadDSCCT			(IDeviceInfo *pDeviceInfo);
	void LoadDSCEVC			(IDeviceInfo *pDeviceInfo);
	void LoadDSCPictureWizard	(IDeviceInfo *pDeviceInfo);
	void LoadDSCZoom		(IDeviceInfo *pDeviceInfo);
	void LoadDSCMovieSize	(IDeviceInfo *pDeviceInfo);
	//GH5
	void LoadDSCPhotoStyle	(IDeviceInfo *pDeviceInfo);
	void LoadDSCPSContrast	(IDeviceInfo *pDeviceInfo);
	void LoadDSCPSSharpness	(IDeviceInfo *pDeviceInfo);
	void LoadDSCPSNoiseReduction(IDeviceInfo *pDeviceInfo);
	void LoadDSCPSSaturation	(IDeviceInfo *pDeviceInfo);
	void LoadDSCPSHue		(IDeviceInfo *pDeviceInfo);
	void LoadDSCPSFilterEffect	(IDeviceInfo *pDeviceInfo);
	void LoadDSCWBAdjustAB	(IDeviceInfo *pDeviceInfo);
	void LoadDSCWBAdjustMG	(IDeviceInfo *pDeviceInfo);
	void LoadDSCCTEX		(IDeviceInfo *pDeviceInfo,int nType);
	void LoadDSCEStabilization(IDeviceInfo *pDeviceInfo);
	void LoadDSCFWVersion	(IDeviceInfo *pDeviceInfo);
	void LoadDSCISOStep		(IDeviceInfo *pDeviceInfo);
	void LoadDSCISOExpansion(IDeviceInfo *pDeviceInfo);
	void LoadDSCLCD			(IDeviceInfo *pDeviceInfo);
	void LoadDSCPeaking		(IDeviceInfo *pDeviceInfo);
	void LoadDSCSystemFrequency(IDeviceInfo *pDeviceInfo);
	void LoadDSCLensPower(IDeviceInfo *pDeviceInfo);
	

	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	void LoadDSCFocus	(IDeviceInfo *pDeviceInfo);
	
	CString GetPropertyValueStr(int nPropCode, int nValue);	

	void SetHiddenCommand(BOOL bAll = FALSE);
	void SetHiddenCommandFocus(BOOL bAll = FALSE);

	//seo
	void SaveSetting(CString strFileName);
	void LoadSetting(CString strFileName);
	void CameraSetting(int nIdx, int PtpCode, int PtpUint, int nValue, CString strValue);
	
	void GetPropertyIsoIncrease(CDSCItem* pDscItem);
	void GetPropertyIsoDecrease(CDSCItem* pDscItem);
	//------------------------------------------------------------------------------
	//! @function			
	//! @brief				
	//! @date		2013-05-04
	//! @owner		Hongsu Jung (hongsu@esmlab.com)
	//! @return			
	//! @revision		
	//------------------------------------------------------------------------------
	void SetDSCProperty(BOOL bAll = FALSE);			//-- 2013-05-04 hongsu@esmlab.com

	//seo
	void SetDSCMProperty(BOOL bAll = FALSE);
	void SetDSCVoiceOff();
	void SetDSCShutDown();
	void RemovePropertyItem();
	void SetDSCLcdOff();
	void SetDSCLcdOn();
	void SetDSCFocusZoom();
	void SetDSCModeJpgfile();
	void SetDSCModeMoviefile();
	void SetISOUp();
	void SetISODown();
	void SetSSpeedUp();
	void SetSSpeedDown();
	void SetCTUp();
	void SetCTDown();
	void SetSensorCleaning();
	void SetWhiteBalance();
	void SetWhiteBalanceAll();
	void SetPictureWizard();
	void SetLoadFocus(CString strPath, BOOL bFocusMove);
	void SetSaveFocus(CString strPath);
	void SetFrequency(BOOL bPAL);
	void SetLensPower(BOOL bOff);
	int ResultFocusMoveCount();
	//-- INPUT COMPLETE
	virtual void OnPgcInputComplete (CExtPropertyGridWnd * pPGW, CExtPropertyItem * pPropertyItem);
	
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	public:
		CDSCItem* GetItem() {return m_pItem;};

	int GetCamVersion();

	CString GetMovieSizeCompareFrequency(CString strSize, BOOL bPAL=TRUE);

	void GetPictureWizardDetail(int nPTPCode, int nValue);
	void GetPictureWizardDetailETC(int nPTPCode, int nValue);
	//jhhan 16-09-09
protected:
	CString m_strModify[DSC_PROP_CNT];
public:
	CString GetModifyProperty(int nValue) {return m_strModify[nValue];}
	void SetModifyProperty(int nValue, CString str) { m_strModify[nValue] = str; }
	afx_msg void OnTimer(UINT_PTR nIDEvent);
}; 