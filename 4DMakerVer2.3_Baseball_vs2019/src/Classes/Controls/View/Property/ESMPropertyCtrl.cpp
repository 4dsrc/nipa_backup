_commandEntries
_commandEntries
////////////////////////////////////////////////////////////////////////////////
//
//	ESMPropertyCtrl.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-03
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMPropertyCtrl.h"
#include "ESMFunc.h"
#include "ESMIni.h"
#include "ESMUtil.h"
#include "DSCItem.h"
#include "ESMPropertyEditorDlg.h"
#include "ESMPWEditorDlg.h"
#include "ESMPWEditorDlgGH5.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CESMPropertyCtrl window

CESMPropertyCtrl::CESMPropertyCtrl()
	: m_PS(_T("DSC Property"))
{
	m_pItem				= NULL;

	m_pCategoryDSCBasic		= NULL;
	m_pModelName			= NULL;
	m_pUniqueID				= NULL;

	m_pCategoryDSCOption	= NULL;
	m_pDSCMode				= NULL;
	m_pDSCSize				= NULL;
	m_pDSCQuality			= NULL;
	m_pDSCShutter			= NULL;
	m_pDSCAperture			= NULL;
	m_pDSCISO				= NULL;
	m_pDSCWB				= NULL;
	m_pDSCMeter				= NULL;
	m_pDSCCT				= NULL;
	m_pDSCEVC				= NULL;
	m_PictureWizard			= NULL;
	m_pDSCZoom				= NULL;
	m_MovieSize				= NULL;
	//GH5
	m_pDSCPhotoStyle		= NULL;
	m_pDSCCTEX1				= NULL;
	m_pDSCCTEX2				= NULL;
	m_pDSCCTEX3				= NULL;
	m_pDSCCTEX4				= NULL;
	m_pDSCESTabilization	= NULL;
	m_pDSCFWVersion			= NULL;
	m_pDSCISOStep			= NULL;
	m_pDSCISOExpansion		= NULL;
	m_pDSCLCD				= NULL;
	m_pDSCPeaking			= NULL;

	m_pCategoryDSCSystemOption = NULL;
	m_pDSCSystemFrequency	= NULL;
	m_pDSCLensPowerCtrl		= NULL;
	//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
	m_Focus				= NULL;

	for(int i=0; i<DSC_PROP_CNT; i++)
	{
		m_strModify[i] = _T("");
	}

	m_nStartIndex = 0;
	m_nEndIndex = 0;

	m_PWEditorDlgGH5 = new CESMPWEditorDlgGH5(this);	
	m_nFocusMoveCount = 0;

	m_nRGB = 0;
	m_nPTPCode = 0;
}

CESMPropertyCtrl::~CESMPropertyCtrl() 
{
	RemovePropertyItem();

 	if(m_PWEditorDlgGH5)
 	{
 		delete m_PWEditorDlgGH5;
 		m_PWEditorDlgGH5 = NULL;
 	}
}


void CESMPropertyCtrl::RemovePropertyItem()
{
	//-------------------------------------------------
	//-- DELETE Property Store
	//-------------------------------------------------
	if(m_PS.ItemGetCount ())
		m_PS.ItemRemove();	
}

BEGIN_MESSAGE_MAP(CESMPropertyCtrl, CExtPropertyGridCtrl)
	ON_WM_CREATE()
	ON_WM_TIMER()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------ 
//! @brief    Called when the user has completed editing of a property value.
//! @date     2009-08-31
//! @owner    hongsu.jung
//! @note
//! @return        
//! @revision 
//------------------------------------------------------------------------------ 
void CESMPropertyCtrl::OnPgcInputComplete(CExtPropertyGridWnd* pPGW,	CExtPropertyItem* pPropertyItem)
{
	CString strPropText;
	CString strPropName;

	/*

	UINT nType; 
	{
	//-- GET CHANGED TEST
	pPropertyItem->ValueActiveGet()->TextGet( strPropText );
	strPropName = pPropertyItem->NameGet();

	// -------------------------------------------------------------------------------------------------
	//-- TEST STEP
	// -------------------------------------------------------------------------------------------------						
	if( m_bTeststep )
	{	
	if( IsSame(strPropName , PROPERTY_ITEM_COMMENT ))
	m_pTestStep->SetComment((tstring)strPropText);

	//-- SPECIFIC PROPERTIES
	CString strItem;
	nType = m_pTestStep->GetType();

	switch(nType)
	{
	case STEP_TYPE_GOTO:					
	if(IsSame(strPropName , PROPERTY_ITEM_GOTO_OPT))
	{
	m_pTestStep->SetParam (STEP_PARAM3, (tstring)strPropText);
	if(m_pTestStep->GetParam(STEP_PARAM3) == STR_GOTO_OPT_NOTHING)
	{
	m_pTestStep->SetParam (STEP_PARAM4, _T(""));
	LoadStepData(m_pTestStep);
	return ;
	}
	}
	else if( IsSame(strPropName , PROPERTY_ITEM_GOTO_OPT_EXPECT))
	if(!m_pTestStep->SetParam (STEP_PARAM4,(tstring)strPropText))
	{
	LoadStepData(m_pTestStep);
	return;
	}
	break;
	//-- 2009-11-9 hongsu.jung
	//-- Multi Language
	case STEP_TYPE_CHK:
	if( IsSame(strPropName , PROPERTY_ITEM_MLC_OSD_PATH ))
	m_pTestStep->SetParam (STEP_PARAM2,(tstring)strPropText);
	break;		
	case STEP_TYPE_SER:	
	case STEP_TYPE_NET_NVR:
	case STEP_TYPE_NET_DVR:
	case STEP_TYPE_NET_NCAM:
	case STEP_TYPE_PTZ_NVR:
	case STEP_TYPE_PTZ_DVR:
	case STEP_TYPE_PTZ_NCAM:
	case STEP_TYPE_PTZ_ACAM:
	case STEP_TYPE_ANALYSIS:
	{
	CString strValue = m_pTestStep->GetParam(strPropName).c_str();
	m_pTestStep->SetParam(strPropName,(tstring)strPropText);

	if(((nType == STEP_TYPE_ANALYSIS) && (m_pTestStep->GetParamType(STEP_SUBJECT) == ANALYSIS_SUB_VIDEO) && 
	(m_pTestStep->GetParamType(STEP_OPT1) == ANALYSIS_SUB_VIDEO_OPT1_COMPARE) && (m_pTestStep->GetParamType(STEP_OPT2) == ANALYSIS_SUB_VIDEO_OPT1_COMPARE_OPT2_OPTION) &&
	((strPropName.Compare(PROPERTY_ITEM_RECORD_DEVICE_TYPE) == 0) || (strPropName.Compare(PROPERTY_ITEM_RECORD_ENCODING) == 0)) && 
	(strPropText.Compare(strValue) != 0)) ||

	((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
	(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
	&& (strPropName.Compare(PROPERTY_ITEM_RECORD_ENCODING) == 0) && (strPropText.Compare(strValue) != 0)) ||

	//-- 2012-07-18 joonho.kim
	((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
	(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
	&& (strPropName.Compare(PROPERTY_ITEM_RECORD_MEGA) == 0) && (strPropText.Compare(strValue) != 0)) ||

	((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
	(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
	&& (strPropName.Compare(PROPERTY_ITEM_RECORD_BIT_CTRL) == 0) && (strPropText.Compare(strValue) != 0)) ||

	((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
	(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
	&& (strPropName.Compare(PROPERTY_ITEM_RECORD_RESOLUTION) == 0) && (strPropText.Compare(strValue) != 0)) ||

	((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
	(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
	&& (strPropName.Compare(PROPERTY_ITEM_RECORD_PROFILE) == 0) && (strPropText.Compare(strValue) != 0)) ||

	((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
	(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
	&& (strPropName.Compare(PROPERTY_ITEM_RECORD_BITRATE) == 0) && (strPropText.Compare(strValue) != 0)) ||

	((nType == STEP_TYPE_NET_NCAM) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_VIDEO) && 
	(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_VIDEO_OPT1_OPTION) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_VIDEO_OPT1_OPTION_OPT2_SET) 
	&& (strPropName.Compare(PROPERTY_ITEM_RECORD_PRIORITY) == 0) && (strPropText.Compare(strValue) != 0)) ||

	/////////////////////////////////

	((nType == STEP_TYPE_NET_NVR) && (m_pTestStep->GetParamType(STEP_SUBJECT) == NET_SUB_TIME) && 
	(m_pTestStep->GetParamType(STEP_OPT1) == NET_SUB_TIME_OPT1_DEVICE) && (m_pTestStep->GetParamType(STEP_OPT2) == NET_SUB_TIME_OPT1_DEVICE_OPT2_SET) &&
	(strPropName.Compare(PROPERTY_ITEM_DATE_TIME_DST) == 0) && (strPropText.Compare(strValue) != 0)))
	{
	LoadStepData(m_pTestStep);
	return;
	}
	}
	break;		
	case STEP_TYPE_MOV:	break;						
	case STEP_TYPE_CGI: break;
	case STEP_TYPE_SRM: break;
	default:						
	break;
	}
	}

	// -------------------------------------------------------------------------------------------------
	//-- WITHOUT TESTSTEP
	// -------------------------------------------------------------------------------------------------
	else	
	{

	//-------------------------------------------------------------------------------------------------
	//-- Execute Option
	//-------------------------------------------------------------------------------------------------	
	if(IsSame(pPropertyItem->ItemParentGet()->NameGet() , PROPERTY_ITEM_DSC_OPTION) )
	{
	if( IsSame(strPropName , PROPERTY_ITEM_APERTURE ) )
	{
	if( strPropText == "True" )	m_pTestBase->SetExecuteOption(TRUE) ;	
	else						m_pTestBase->SetExecuteOption(FALSE) ;

	TGEvent* pMsg = new TGEvent;
	pMsg->message = WM_TG_PROPERTY_MODIFY_EXEC_OPT;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(),WM_TG,(WPARAM)WM_TG_PROPERTY,(LPARAM)pMsg);
	}
	else if(IsSame(strPropName , PROPERTY_ITEM_DSC_CNT ))
	{
	m_pTestBase->SetTestCount(_ttoi(strPropText));
	TGEvent* pMsg = new TGEvent;
	pMsg->message = WM_TG_PROPERTY_MODIFY_EXEC_CNT;
	pMsg->nParam = (LPARAM)m_pTestBase;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(),WM_TG,(WPARAM)WM_TG_PROPERTY,(LPARAM)pMsg);
	}
	else if( IsSame(strPropName , PROPERTY_ITEM_ISO ))
	{
	if( strPropText == TG_POSTFAIL_SYSTEM )			m_pTestBase->SetPostFail( TG_PF_SYSTEM) ;
	else if( strPropText == TG_POSTFAIL_PASS )		m_pTestBase->SetPostFail( TG_PF_PASS ) ;
	else if( strPropText == TG_POSTFAIL_RETRY )		m_pTestBase->SetPostFail( TG_PF_RETRY ) ;
	else if( strPropText == TG_POSTFAIL_STOP )		m_pTestBase->SetPostFail( TG_PF_STOP ) ;					

	CheckRetryCount();					
	SetRedraw( TRUE );
	}
	else if( IsSame(strPropName , PROPERTY_ITEM_WB ))
	{
	m_pTestBase->SetRetryCnt( _ttoi(strPropText) );
	}
	}
	//-------------------------------------------------------------------------------------------------
	//-- Property
	//-------------------------------------------------------------------------------------------------
	else if(IsSame(pPropertyItem->ItemParentGet()->NameGet() , PROPERTY_ITEM_PROPERTY ))
	{
	if( IsSame(strPropName , PROPERTY_ITEM_CREATOR ) )
	m_pTestBase->SetCreator(strPropText);	
	else if( IsSame(strPropName , PROPERTY_ITEM_DESC ))
	m_pTestBase->SetComment(strPropText);	
	}
	}	// end if(m_bTeststep)
	} // end if( pPropertyItem->IsModified() )
	*/

	CExtPropertyGridCtrl::OnPgcInputComplete(pPGW, pPropertyItem);
}
//-------------------------------------------------------------------------- 
//! @brief	  
//! @date	    2011-2-18
//! @note
//! @revision	
//--------------------------------------------------------------------------
/*
void CESMPropertyCtrl::OnPgcInputComplete_RAN()
{
CExtPropertyItem* pItem = NULL;
CString strValue = _T("");

//[PARAM2] Interval
pItem = m_pCategoryProperty->ItemGetAt(0);
pItem->ValueActiveGet()->TextGet(strValue);
if(!strValue.IsEmpty())
m_pTestStep->SetParam(STEP_PARAM2, (tstring)strValue); 

//[PARAM3] Touch Interval
pItem = m_pCategoryProperty->ItemGetAt(1);
pItem->ValueActiveGet()->TextGet(strValue);
if(!strValue.IsEmpty())
m_pTestStep->SetParam(STEP_PARAM3, (tstring)strValue); 

//[PARAM4] x
pItem = m_pCategoryProperty->ItemGetAt(2);
pItem->ValueActiveGet()->TextGet(strValue);
if(!strValue.IsEmpty())
m_pTestStep->SetParam(STEP_PARAM4, (tstring)strValue); 

//[PARAM5] y
pItem = m_pCategoryProperty->ItemGetAt(3);
pItem->ValueActiveGet()->TextGet(strValue);
if(!strValue.IsEmpty())
m_pTestStep->SetParam(STEP_PARAM5, (tstring)strValue); 

//[PARAM6] x'
pItem = m_pCategoryProperty->ItemGetAt(4);
pItem->ValueActiveGet()->TextGet(strValue);
if(!strValue.IsEmpty())
m_pTestStep->SetParam(STEP_PARAM6, (tstring)strValue); 

//[PARAM7] y'
pItem = m_pCategoryProperty->ItemGetAt(5);
pItem->ValueActiveGet()->TextGet(strValue);
if(!strValue.IsEmpty())
m_pTestStep->SetParam(STEP_PARAM7, (tstring)strValue); 


//[PARAM8] Weight
pItem = m_pCategoryProperty->ItemGetAt(6);
pItem->ValueActiveGet()->TextGet(strValue);
if(!strValue.IsEmpty())
m_pTestStep->SetParam(STEP_PARAM8, (tstring)strValue); 

//Comment
pItem = m_pCategoryProperty->ItemGetAt(7);
pItem->ValueActiveGet()->TextGet(strValue);
}
*/

LRESULT CESMPropertyCtrl::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	LRESULT lResult =	CExtPropertyGridCtrl::WindowProc( message, wParam, lParam );
	if( message == WM_CREATE )
	{
		PropertyStoreSet( &m_PS );
		CTypedPtrArray < CPtrArray, CExtPropertyGridWnd * > arrGrids;
		OnPgcQueryGrids( arrGrids );
		INT nGridIdx = 0;
		for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
		{
			CExtPropertyGridWnd * pGrid = arrGrids[ nGridIdx ];
			ASSERT_VALID( pGrid );
			pGrid->m_bThemeBackground = FALSE;  // Siseong Ahn 2019-06-04
			g_PaintManager->LoadWinXpTreeBox( pGrid->m_iconTreeBoxExpanded, true );
			g_PaintManager->LoadWinXpTreeBox( pGrid->m_iconTreeBoxCollapsed, false );
		} // for( ; nGridIdx < arrGrids.GetSize(); nGridIdx ++ )
		CExtPropertyGridComboBoxBar * pCBB = STATIC_DOWNCAST( CExtPropertyGridComboBoxBar, GetChildByRTC( RUNTIME_CLASS(CExtPropertyGridComboBoxBar) ));
		pCBB->PropertyStoreInsert( &m_PS );
	} // if( message == WM_CREATE )

	return lResult;
}

/////////////////////////////////////////////////////////////////////////////
// CESMPropertyCtrl WINDOW MESSAGE

int CESMPropertyCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CExtPropertyGridCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CExtPropertyGridToolBar * pWnd =
		STATIC_DOWNCAST(
		CExtPropertyGridToolBar,
		this->GetChildByRTC(
		RUNTIME_CLASS(CExtPropertyGridToolBar)
		)
		);
	if( pWnd != NULL )
	{
		pWnd->ShowWindow(SW_HIDE);
		this->RecalcLayout();
	}

	//-----------------------------------------------
	//-- CATEGORY BASIC INFO
	m_pCategoryDSCBasic =	new CExtPropertyCategory(PROPERTY_ITEM_DSC_BASIC);
	//-- CATEGORY COMMENT FROM STRING TABLE
	VERIFY( m_PS.ItemInsert( m_pCategoryDSCBasic ) );

	//-----------------------------------------------
	//-- CATEGORY PROPERTY
	m_pCategoryDSCOption =	new CExtPropertyCategory(PROPERTY_ITEM_DSC_OPTION);
	//-- CATEGORY COMMENT FROM STRING TABLE
	VERIFY( m_PS.ItemInsert( m_pCategoryDSCOption ) );

	//-----------------------------------------------
	//-- CATEGORY PROPERTY
	m_pCategoryDSCSystemOption =	new CExtPropertyCategory(PROPERTY_ITEM_DSC_SYSTEM_OPTION);
	//-- CATEGORY COMMENT FROM STRING TABLE
	VERIFY( m_PS.ItemInsert( m_pCategoryDSCSystemOption ) );
	
	return 0;
}

BOOL CESMPropertyCtrl::SetPropertyComboBox(CExtPropertyValue* pItem, CStringArray* pArList, CString strCurrentName, int nCurrentValue)
{
	CExtGridCellComboBox* pComboBox = NULL;
	pComboBox = STATIC_DOWNCAST(CExtGridCellComboBox, pItem->ValueDefaultGetByRTC(RUNTIME_CLASS(CExtGridCellComboBox)));
	if(pComboBox == NULL) 
		return FALSE;

	if(pArList)
	{
		int nAll = (int)pArList->GetCount();

		for(DWORD  i = 0 ; i < (DWORD)nAll ; i++ )
			pComboBox->InsertString(pArList->GetAt(i));

		nAll = strCurrentName.GetLength();
		if(!strCurrentName.GetLength())
		{
			pComboBox->SetCurSel(nCurrentValue);
			return TRUE;
		}

	}else
	{
		pComboBox->InsertString(strCurrentName);
		pComboBox->SetCurSel(0);
		return TRUE;
		/*
		int nPropCode = 0x5003;
		CString strValue;
		CStringArray arStrList;
		GetPropertySection(nPropCode, arStrList);
		int nAll = (int)arStrList.GetCount();
		for(DWORD  i = 0 ; i < (DWORD)nAll ; i++ )
		{
		strValue = arStrList.GetAt(i);
		strValue = GetPropertyValueSrttoStr(nPropCode,strValue);
		if(strValue.GetLength())
		pComboBox->InsertString(strValue);
		}
		*/
	}

	pComboBox->SetCurSel(pComboBox->FindString(strCurrentName));
	return TRUE;
}

BOOL CESMPropertyCtrl::SetPropertyComboBox(int nPropCode,CExtPropertyValue* pItem, CUIntArray* pArList, int nCurrentValue)
{
	CExtGridCellComboBox* pComboBox = NULL;

	pComboBox = STATIC_DOWNCAST(CExtGridCellComboBox, pItem->ValueDefaultGetByRTC(RUNTIME_CLASS(CExtGridCellComboBox)));
	if(pComboBox == NULL) 
		return FALSE;

	CString strFrequency;
	CString strValue;

	if(nPropCode == PTP_DPC_FUNCTIONAL_MODE)
	{
		if(pArList)
		{
			int nAll = (int)pArList->GetCount();
			for(DWORD  i = 0 ; i < (DWORD)nAll ; i++ )
			{
				strValue = GetPropertyValueStr(nPropCode,pArList->GetAt(i));
				if(strValue.GetLength())
					if(strValue == "M" || strValue == "A" || strValue == "S" || strValue == "P" || strValue == "Movie")
						pComboBox->InsertString(strValue);
			}
		}
	}
	// 셔터 스피드 리스트 거꾸로 출력
	else if(nPropCode == PTP_DPC_SAMSUNG_SHUTTER_SPEED)
	{
		if(pArList)
		{
			int nAll = (int)pArList->GetCount();
			while(nAll--)
			{
				strValue = GetPropertyValueStr(nPropCode,pArList->GetAt(nAll));
				if(strValue.GetLength())
				{
					pComboBox->InsertString(strValue);
					if( PTP_CODE_EXPOSUREINDEX == nPropCode)
						m_arrIsoData.push_back(strValue);
				}
				else
				{
					strValue.Format(_T("%d"),pArList->GetAt(nAll));
					pComboBox->InsertString(strValue);
				}
			}
		}
		else
		{
			CStringArray arStrList;
			GetPropertySection(nPropCode, arStrList);
			int nAll = (int)arStrList.GetCount();
			while(nAll--)
			{
				strValue = arStrList.GetAt(nAll);
				strValue = GetPropertyValueSrttoStr(nPropCode,strValue);
				if(strValue.GetLength())
					pComboBox->InsertString(strValue);
			}
		}
	}
	else
	{
		//////////////////////////////////////////////////////////////////////////
		// GH5 Movie size -> System Frequency 확인 후에 값을 적어준다.		
 		if (m_pItem)
 		{
 			CString strModel;			
 			strModel = m_pItem->GetDeviceModel();
 			if(nPropCode == PTP_CODE_SAMSUNG_MOV_SIZE && strModel.CompareNoCase(_T("GH5")) == 0)
 			{
 				strFrequency = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_SYSTEM_FREQUENCY);				
 			}
 		}
		//////////////////////////////////////////////////////////////////////////

		if(pArList)
		{
			int nAll = (int)pArList->GetCount();
			for(DWORD  i = 0 ; i < (DWORD)nAll ; i++ )
			{
				strValue = GetPropertyValueStr(nPropCode,pArList->GetAt(i));
				if(strValue.GetLength())
				{
					if (!strFrequency.IsEmpty() && strFrequency.Find(_T("PAL")) != -1)
					{
						strValue = GetMovieSizeCompareFrequency(strValue);						
					}

					pComboBox->InsertString(strValue);
					if( PTP_CODE_EXPOSUREINDEX == nPropCode)
						m_arrIsoData.push_back(strValue);
				}
				else
				{
					strValue.Format(_T("%d"),pArList->GetAt(i));
					pComboBox->InsertString(strValue);
				}
			}
		}
		else
		{
			/*
			strValue = GetPropertyValueStr(nPropCode,nCurrentValue);
			if(strValue.GetLength())
			pComboBox->InsertString(strValue);
			else
			{
			strValue.Format(_T("%d"),nCurrentValue);
			pComboBox->InsertString(strValue);
			}
			pComboBox->SetCurSel(0);	
			return TRUE;
			*/
			CStringArray arStrList;
			GetPropertySection(nPropCode, arStrList);
			int nAll = (int)arStrList.GetCount();
			for(DWORD  i = 0 ; i < (DWORD)nAll ; i++ )
			{
				strValue = arStrList.GetAt(i);
				strValue = GetPropertyValueSrttoStr(nPropCode,strValue);
				if(strValue.GetLength())
					pComboBox->InsertString(strValue);
			}
		}
	}
	strValue = GetPropertyValueStr(nPropCode,nCurrentValue);

	if (!strFrequency.IsEmpty() && strFrequency.Find(_T("PAL")) != -1)
	{
		strValue = GetMovieSizeCompareFrequency(strValue);						
	}

	if(strValue.GetLength())
	{
		int nAll = pComboBox->GetCount();
		CString strData;
		while(nAll--)
		{
			strData = pComboBox->GetItemString(nAll);
			if(strData == strValue)
				break;
		}

		pComboBox->SetCurSel(nAll);	
	}
	else
	{
		strValue.Format(_T("%d"),nCurrentValue);
		pComboBox->SetCurSel(pComboBox->FindString(strValue));	
	}

	return TRUE;
}

CString CESMPropertyCtrl::GetPropertyValueStr(int nPropCode, int nValue)
{
	CString strValue, strRet, strSection;
	CString strConfigFile;

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Load Config File

	CString strModel = _T("GH5");
	if (ESMGetDevice() == SDI_MODEL_NX1)
		strModel = _T("NX1");

	//strConfigFile.Format(_T("%s\\%s.info"), ESMGetPath(ESM_PATH_CONFIG),m_pItem->GetDeviceModel());
	strConfigFile.Format(_T("%s\\%s.info"), ESMGetPath(ESM_PATH_CONFIG),strModel);
	CESMIni ini;
	if(!ini.SetIniFilename (strConfigFile))
		return strRet;

	if(nPropCode == PTP_DPC_SAMSUNG_WB_DETAIL_KELVIN)
	{
		strRet.Format(_T("%d"),nValue);
	}
	else if( nPropCode == SDI_DPC_SAMSUNG_ZOOM_CTRL_GET )
	{
		nValue>>8;
		nValue&0x00FF;
		strRet.Format(_T("%d"), nValue&0x00FF);
	}	else
	{
		//CString strModel = m_pItem->GetDeviceModel();
		if(strModel.CompareNoCase(_T("GH5")) == 0)
		{
			strValue.Format(_T("%x"),nValue);
		}
		else //NX Series
		{
			if(	nPropCode == PTP_DPC_EXPOSURE_INDEX ||	nPropCode == PTP_DPC_F_NUMBER	)
				strValue.Format(_T("%d"),nValue);
			else
				strValue.Format(_T("%x"),nValue);
		}
		
		strSection.Format(_T("%x"),nPropCode);
		strRet = ini.GetString(strSection, strValue);
		//strRet = ini.GetStringValuetoKey(strSection, strValue);

	}

	return strRet;
}

CString CESMPropertyCtrl::GetPropertyValueSrttoStr(int nPropCode, CString strValue)
{
	CString strRet, strSection;
	CString strConfigFile;

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Load Config File
	strConfigFile.Format(_T("%s\\%s.info"), ESMGetPath(ESM_PATH_CONFIG),m_pItem->GetDeviceModel());
	CESMIni ini;
	if(!ini.SetIniFilename (strConfigFile))
		return strRet;

	strSection.Format(_T("%x"),nPropCode);
	strRet = ini.GetString(strSection, strValue);

	return strRet;
}

int CESMPropertyCtrl::GetPropertyValueEnumCount(int nPropCode, CString strValue)
{
	CString strSection;
	CString strConfigFile;
	DWORD nValue = 0;

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Load Config File

	strConfigFile.Format(_T("%s\\%s.info"), ESMGetPath(ESM_PATH_CONFIG),m_pItem->GetDeviceModel());
	CESMIni ini;
	if(!ini.SetIniFilename (strConfigFile))
		return -1;

	strSection.Format(_T("%x"),nPropCode);
	nValue = ini.GetStringValuetoKeyIndex(strSection, strValue);

	return nValue;
}

int CESMPropertyCtrl::GetPropertyValueEnum(int nPropCode, CString strValue)
{
	CString strRet, strSection;
	CString strConfigFile;
	int nValue;

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Load Config File

	strConfigFile.Format(_T("%s\\%s.info"), ESMGetPath(ESM_PATH_CONFIG),m_pItem->GetDeviceModel());
	CESMIni ini;
	if(!ini.SetIniFilename (strConfigFile))
		return -1;

	strSection.Format(_T("%x"),nPropCode);
	strRet = ini.GetStringValuetoKey(strSection, strValue);

	char* endptr = NULL;
	char* strTpData = NULL;
	strTpData = ESMUtil::GetCharString(strRet);

	if(m_pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		nValue = strtoul(strTpData,&endptr, 16);
	else //NX Series
	{
		if(
			nPropCode == PTP_DPC_EXPOSURE_INDEX	 ||
			nPropCode == PTP_DPC_F_NUMBER		||
			nPropCode == PTP_CODE_SAMSUNG_ZOOM_CTRL_SET	)
			nValue = strtoul(strTpData,&endptr, 10);
		else
			nValue = strtoul(strTpData,&endptr, 16);
	}
	
	if( strTpData)
	{
		delete strTpData;
		strTpData = NULL;
	}
	return nValue;
}

void CESMPropertyCtrl::GetPropertySection(int nPropCode, CStringArray& arList)
{
	CString strKey;
	CString strConfigFile;

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Load Config File
	strConfigFile.Format(_T("%s\\%s.info"), ESMGetPath(ESM_PATH_CONFIG),m_pItem->GetDeviceModel());
	CESMIni ini;
	if(!ini.SetIniFilename (strConfigFile))
		return;

	strKey.Format(_T("%x"),nPropCode);
	ini.GetSectionList(strKey, arList);
}

//------------------------------------------------------------------------------
//! @function			
//! @brief				
//! @date		2013-05-04
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @return			
//! @revision		
//------------------------------------------------------------------------------
//! PTP_CODE_FUNCTIONALMODE
//! PTP_CODE_IMAGESIZE				
//! PTP_CODE_COMPRESSIONSETTING	
//! PTP_CODE_SAMSUNG_SHUTTERSPEED	
//! PTP_CODE_F_NUMBER				
//! PTP_CODE_EXPOSUREINDEX			
//! PTP_CODE_WHITEBALANCE			
//! PTP_CODE_EXPOSUREMETERINGMODE
//! PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN
//------------------------------------------------------------------------------
void CESMPropertyCtrl::SetDSCProperty(BOOL bAll)
{
	if (!m_pItem)
		return;

	ESMSetFrameRate(MOVIE_FPS_UNKNOWN);

	if(bAll)
	{
		CESMPropertyEditorDlg EditorDlg(this); 
		if (EditorDlg.DoModal() != IDOK)
			return;
	}

	// property diff log
	ESMPropertyDiffCheck();
	ESMPropertyMinMaxInfoCheck();

	//-- 2013-05-04 hongsu@esmlab.com
	//-- Just Selected DSC Set Property
	//-- Check Change
	ESMEvent* pSdiMsg	= NULL;
	int nValue = 0;
	CString strValue;
	CDSCItem* pItem = NULL;
	CObArray arDSCList;

	int nDataSize;
	int nCCTCode;
	if(m_pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
	{
		nDataSize = PTP_DPV_UINT32;
		nCCTCode = PTP_DPC_WB_DETAIL_KELVIN;
	}
	else // NX Series
	{
		nDataSize = PTP_VALUE_UINT_16;
		nCCTCode = PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN;
	}

	UpdateData();

	/*
	if (!bAll)
	{
		if(m_pUniqueID)
		{
			m_pUniqueID->ValueActiveGet()->TextGet(strValue);

			CString uniqueId = m_pItem->GetDeviceUniqueID();

			if(!strValue.IsEmpty() && strValue != uniqueId)
			{
				m_pItem->SetDSCInfo(DSC_INFO_UNIQUEID, strValue);
				m_pItem->m_pSdi->PTP_SetDevUniqueID(strValue);
			}
		}
	}
	*/

/*
	//-- IMAGE SIZE
	if(m_pDSCSize)
	{
		m_pDSCSize->ValueActiveGet()->TextGet(strValue);

		if(!strValue.IsEmpty())
		{
			TCHAR* pStrTpData = NULL;
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_SIZE))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_CODE_IMAGESIZE;
					pSdiMsg->nParam2 = PTP_VALUE_STRING;	
					pStrTpData = new TCHAR[strValue.GetLength() + 1];
					_tcscpy(pStrTpData, (TCHAR*)(LPCTSTR)strValue);
					pStrTpData[strValue.GetLength()] = '\0';
					pSdiMsg->pParam = (LPARAM)pStrTpData;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else  //-- Set All
			{
#if 1 // Property IMAGE_SIZE 수정
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_SIZE))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_CODE_IMAGESIZE;
							pSdiMsg->nParam2 = PTP_VALUE_STRING;	
							pStrTpData = new TCHAR[strValue.GetLength() + 1];
							_tcscpy(pStrTpData, (TCHAR*)(LPCTSTR)strValue);
							pStrTpData[strValue.GetLength()] = '\0';
							pSdiMsg->pParam = (LPARAM)pStrTpData;

							TRACE(_T("%s\n"), pStrTpData);
							//ESMLog(1, _T("IMAGE_SIZE %s\n"), pStrTpData);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
							pItem->SdiAddMsg(pSdiMsg);
						}
					}
				}
				SetHiddenCommand(TRUE);
#else
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				{
				pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
				if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
				{
				if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_SIZE))
				{
				pSdiMsg = new ESMEvent;
				pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
				pSdiMsg->nParam1 = PTP_CODE_IMAGESIZE;
				pSdiMsg->nParam2 = PTP_VALUE_STRING;	
				pStrTpData = new TCHAR[strValue.GetLength() + 1];
				_tcscpy(pStrTpData, (TCHAR*)(LPCTSTR)strValue);
				pStrTpData[strValue.GetLength()] = '\0';
				pSdiMsg->pParam = (LPARAM)pStrTpData;

				TRACE(_T("%s\n"), pStrTpData);
				pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
				pItem->SdiAddMsg(pSdiMsg);


				}
				}
				Sleep(200);
				}
				SetHiddenCommand(TRUE);
#endif
			}
		}
		//m_pDSCSize->Reset();

	}
*/
	//-- PTP_CODE_COMPRESSIONSETTING
	/*if(m_pDSCQuality)
	{
		m_pDSCQuality->ValueActiveGet()->TextGet(strValue);

		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_CODE_COMPRESSIONSETTING,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_QUALITY))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_CODE_COMPRESSIONSETTING;
					pSdiMsg->nParam2 = PTP_VALUE_INT_8;	
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						CString strQuality = pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_QUALITY);
						if( strValue !=  strQuality)
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_CODE_COMPRESSIONSETTING;
							pSdiMsg->nParam2 = PTP_VALUE_INT_8;	
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}
		}
		//m_pDSCQuality->Reset();
	}
	*/
	//-- Picture Wizard
	if(m_PictureWizard)
	{
		m_PictureWizard->ValueActiveGet()->TextGet(strValue);

		//jhhan 16-09-09
		m_strModify[DSC_PROP_PW] = strValue;

		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_CODE_SAMSUNG_PICTUREWIZARD,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PICTUREW))
				{
					if(m_pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
					{
						
					}
					else
					{
						pSdiMsg = new ESMEvent;
						pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
						pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_PICTUREWIZARD;
						pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
						pSdiMsg->pParam = (LPARAM)(int)nValue;
						m_pItem->SdiAddMsg(pSdiMsg);
					}

					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					//Sleep(100);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PICTUREW))
						{
							if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
							{
							
							}
							else //NX Series
							{
								pSdiMsg = new ESMEvent;
								pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
								pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_PICTUREWIZARD;
								pSdiMsg->nParam2 = PTP_VALUE_UINT_32;	
								pSdiMsg->pParam = (LPARAM)(int)nValue;
								pItem->SdiAddMsg(pSdiMsg);
							}

							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}			
		}
		//m_MovieSize->Reset();
	}

	//-- PTP_CODE_SAMSUNG_SHUTTERSPEED
	if(m_pDSCShutter)
	{
		m_pDSCShutter->ValueActiveGet()->TextGet(strValue);

		//jhhan 16-09-09
		m_strModify[DSC_PROP_SS] = strValue;

		if(!strValue.IsEmpty())
		{
			if(m_pItem->GetDeviceModel().Compare(_T("NX3000")) == 0)
			{
				nValue = GetPropertyValueEnumCount(PTP_CODE_SAMSUNG_SHUTTERSPEED, strValue);
			}
			else
			{
				nValue = GetPropertyValueEnum(PTP_CODE_SAMSUNG_SHUTTERSPEED,strValue);
			}
			if(!bAll)
			{
				//CString strSt = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_SHUTTERSPEED);
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_SHUTTERSPEED))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_SHUTTERSPEED;
					pSdiMsg->nParam2 = PTP_VALUE_UINT_32;	
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_SHUTTERSPEED))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_SHUTTERSPEED;
							pSdiMsg->nParam2 = PTP_VALUE_UINT_32;	
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}
		}
		//m_pDSCShutter->Reset();
	}

	//-- PTP_CODE_COMPRESSIONSETTING
	if(m_pDSCISO)
	{
		m_pDSCISO->ValueActiveGet()->TextGet(strValue);

		//jhhan 16-09-09
		m_strModify[DSC_PROP_ISO] = strValue;

		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_CODE_EXPOSUREINDEX,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_CODE_EXPOSUREINDEX;
					pSdiMsg->nParam2 = nDataSize;	
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);	
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_CODE_EXPOSUREINDEX;
							pSdiMsg->nParam2 = nDataSize;	
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}
		}
		//m_pDSCISO->Reset();
		//m_pDSCISO->ValueDefaultFromActive();
	}

	//-- PTP_CODE_WHITEBALANCE
	if(m_pDSCWB)
	{
		m_pDSCWB->ValueActiveGet()->TextGet(strValue);

		//jhhan 16-09-09
		m_strModify[DSC_PROP_WB] = strValue;

		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_CODE_WHITEBALANCE,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_WHITEBALANCE))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_CODE_WHITEBALANCE;
					pSdiMsg->nParam2 = nDataSize;	
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_WHITEBALANCE))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_CODE_WHITEBALANCE;
							pSdiMsg->nParam2 = nDataSize;	
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}			
		}
		//m_pDSCWB->Reset();
	}

	//   	//-- PTP_CODE_EXPOSUREMETERINGMODE
	//   	if(m_pDSCMeter)
	//   	{
	//   		m_pDSCMeter->ValueActiveGet()->TextGet(strValue);
	//   		if(!strValue.IsEmpty())
	//   		{
	//   			nValue = GetPropertyValueEnum(PTP_CODE_EXPOSUREMETERINGMODE,strValue);
	//   			if(!bAll)
	//   			{
	//   				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_METERING))
	//   				{
	//   					pSdiMsg = new ESMEvent;
	//   					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	//   					pSdiMsg->nParam1 = PTP_CODE_EXPOSUREMETERINGMODE;
	//   					pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
	//   					pSdiMsg->pParam = (LPARAM)(int)nValue;
	//   					m_pItem->SdiAddMsg(pSdiMsg);
	//   					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
	// 					SetHiddenCommand(bAll);
	//   				}
	//   			}
	//   			else
	//   			{
	//   				ESMGetDSCList(&arDSCList);
	//   				int nDSCCnt =  arDSCList.GetCount();
	//   				for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	//   				{
	//   					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
	//   					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
	//   					{
	//   						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_METERING))
	//   						{
	//   							pSdiMsg = new ESMEvent;
	//   							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	//   							pSdiMsg->nParam1 = PTP_CODE_EXPOSUREMETERINGMODE;
	//   							pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
	//   							pSdiMsg->pParam = (LPARAM)(int)nValue;
	//   							pItem->SdiAddMsg(pSdiMsg);
	//   							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
	//   						}
	//   					}
	//   				}
	// 				SetHiddenCommand(TRUE);
	//   			}
	//   		}
	//   		//m_pDSCMeter->Reset();
	//   	}

	//-- PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN
	if(m_pDSCCT)
	{
		m_pDSCCT->ValueActiveGet()->TextGet(strValue);

		//jhhan 16-09-09
		m_strModify[DSC_PROP_COLOR_TEMPERATURE] = strValue;

		if(!strValue.IsEmpty())
		{
			nValue = _ttoi(strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_COLORTEMPERATURE))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = nCCTCode;
					pSdiMsg->nParam2 = nDataSize;	
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_COLORTEMPERATURE))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = nCCTCode;
							pSdiMsg->nParam2 = nDataSize;	
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}			
		}
		//m_pDSCCT->Reset();
	}
	//-- PTP_CODE_F_NUMBER
	if(m_pDSCAperture)
	{
		CString strAperture = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_APERTURE);
		m_pDSCAperture->ValueActiveGet()->TextGet(strValue);
		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_CODE_F_NUMBER,strValue);

			if(!bAll)
			{
				if( strValue != strAperture && nValue != 0xfffe && strAperture != _T("F(unknown)"))	// 0xfffe : F(unknown))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_CODE_F_NUMBER;
					pSdiMsg->nParam2 = PTP_VALUE_UINT_32;
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						strAperture = pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_APERTURE);
						if( strValue != strAperture && nValue != 0xfffe && strAperture != _T("F(unknown)"))	// 0xfffe : F(unknown)
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_CODE_F_NUMBER;
							pSdiMsg->nParam2 = PTP_VALUE_UINT_32;
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}
		}
		//m_pDSCAperture->Reset();
	}
	
	//-- m_MovieSize
	if(m_MovieSize)
	{
		m_MovieSize->ValueActiveGet()->TextGet(strValue);

		//jhhan 16-09-09
		m_strModify[DSC_PROP_MOVIE_SIZE] = strValue;

		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_CODE_SAMSUNG_MOV_SIZE,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_MOVIESZIE))
				{
					if(m_pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
					{
						pSdiMsg = new ESMEvent;
						pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
						pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_MOV_SIZE;
						pSdiMsg->nParam2 = PTP_DPV_UINT32;	
						pSdiMsg->pParam = (LPARAM)(int)nValue;
						m_pItem->SdiAddMsg(pSdiMsg);
					}
					else
					{
						int nMovieFormat = 0;
						if( strValue.Find(_T("NTSC")) >= 0)
							nMovieFormat = 0x81;
						else
							nMovieFormat = 0x82;

						pSdiMsg = new ESMEvent;
						pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
						pSdiMsg->nParam1 = PTP_DPC_SAMSUNG_VIDEO_OUT;
						pSdiMsg->nParam2 = PTP_VALUE_UINT_8;	
						pSdiMsg->pParam = (LPARAM)(int)nMovieFormat;
						m_pItem->SdiAddMsg(pSdiMsg);

						pSdiMsg = new ESMEvent;
						pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
						pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_MOV_SIZE;
						pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
						pSdiMsg->pParam = (LPARAM)(int)nValue;
						m_pItem->SdiAddMsg(pSdiMsg);
					}
					
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					//Sleep(100);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_MOVIESZIE))
						{
							if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
							{
								pSdiMsg = new ESMEvent;
								pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
								pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_MOV_SIZE;
								pSdiMsg->nParam2 = PTP_DPV_UINT32;	
								pSdiMsg->pParam = (LPARAM)(int)nValue;
								pItem->SdiAddMsg(pSdiMsg);
							}
							else //NX Series
							{
								int nMovieFormat = 0;
								if( strValue.Find(_T("NTSC")) >= 0)
									nMovieFormat = 0x81;
								else
									nMovieFormat = 0x82;

								pSdiMsg = new ESMEvent;
								pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
								pSdiMsg->nParam1 = PTP_DPC_SAMSUNG_VIDEO_OUT;
								pSdiMsg->nParam2 = PTP_VALUE_UINT_8;	
								pSdiMsg->pParam = (LPARAM)(int)nMovieFormat;
								pItem->SdiAddMsg(pSdiMsg);

								pSdiMsg = new ESMEvent;
								pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
								pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_MOV_SIZE;
								pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
								pSdiMsg->pParam = (LPARAM)(int)nValue;
								pItem->SdiAddMsg(pSdiMsg);
							}
							
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}			
		}
		//m_MovieSize->Reset();
	}

	// 	//-- Picture Wizard
	// 	if(m_PictureWizard)
	// 	{
	// 		m_PictureWizard->ValueActiveGet()->TextGet(strValue);
	// 		if(!strValue.IsEmpty())
	// 		{
	// 			nValue = GetPropertyValueEnum(PTP_CODE_SAMSUNG_PICTUREWIZARD,strValue);
	// 			if(!bAll)
	// 			{
	// 				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PICTUREW))
	// 				{
	// 					pSdiMsg = new ESMEvent;
	// 					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	// 					pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_PICTUREWIZARD;
	// 					pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
	// 					pSdiMsg->pParam = (LPARAM)(int)nValue;
	// 					m_pItem->SdiAddMsg(pSdiMsg);
	// 					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
	// 					SetHiddenCommand(bAll);
	// 				}
	// 			}
	// 			else
	// 			{
	// 				ESMGetDSCList(&arDSCList);
	// 				int nDSCCnt =  arDSCList.GetCount();
	// 				for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	// 				{
	// 					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
	// 					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
	// 					{
	// 						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PICTUREW))
	// 						{
	// 							pSdiMsg = new ESMEvent;
	// 							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	// 							pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_PICTUREWIZARD;
	// 							pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
	// 							pSdiMsg->pParam = (LPARAM)(int)nValue;
	// 							pItem->SdiAddMsg(pSdiMsg);
	// 							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
	// 						}
	// 					}
	// 				}
	// 				SetHiddenCommand(TRUE);
	// 			}			
	// 		}
	// 		//m_PictureWizard->Reset();
	// 	}
	//-- PTP_CODE_ZOOM
	/*if(m_pDSCZoom)
	{
		m_pDSCZoom->ValueActiveGet()->TextGet(strValue);
		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_CODE_SAMSUNG_ZOOM_CTRL_SET,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ZOOM))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_ZOOM_CTRL_GET;
					pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
					pSdiMsg->pParam = (LPARAM)(int)nValue<<8;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ZOOM))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_ZOOM_CTRL_GET;
							pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
							pSdiMsg->pParam = (LPARAM)(int)nValue<<8;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}			
		}
		//m_pDSCWB->Reset();
	}*/

	//-- PTP_DPC_PHOTO_STYLE
	if(m_pDSCPhotoStyle)
	{
		m_pDSCPhotoStyle->ValueActiveGet()->TextGet(strValue);

		//jhhan 16-09-09
		//m_strModify[DSC_PROP_PHOTO_STYLE] = strValue;
		m_strModify[DSC_PROP_PW] = strValue;

		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_DPC_PHOTO_STYLE,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PHOTO_STYLE))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_DPC_PHOTO_STYLE;
					pSdiMsg->nParam2 = nDataSize;	
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PHOTO_STYLE))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_DPC_PHOTO_STYLE;
							pSdiMsg->nParam2 = nDataSize;	
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}			
		}
		//m_pDSCCT->Reset();
	}

	//-- PTP_DPC_MOV_STA_E_STABILIZATION
	if(m_pDSCESTabilization)
	{
		m_pDSCESTabilization->ValueActiveGet()->TextGet(strValue);

		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_DPC_MOV_STA_E_STABILIZATION,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_E_STABILIZATION))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_DPC_MOV_STA_E_STABILIZATION;
					pSdiMsg->nParam2 = nDataSize;	
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_E_STABILIZATION))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_DPC_MOV_STA_E_STABILIZATION;
							pSdiMsg->nParam2 = nDataSize;	
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}			
		}
		//m_pDSCESTabilization->Reset();
	}

	//-- PTP_DPC_SAMSUNG_ISO_STEP
	if(m_pDSCISOStep)
	{
		m_pDSCISOStep->ValueActiveGet()->TextGet(strValue);

		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_DPC_SAMSUNG_ISO_STEP,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO_STEP))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_DPC_SAMSUNG_ISO_STEP;
					pSdiMsg->nParam2 = nDataSize;	
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO_STEP))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_DPC_SAMSUNG_ISO_STEP;
							pSdiMsg->nParam2 = nDataSize;	
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}			
		}
		//m_pDSCISOStep->Reset();
	}

	//-- PTP_DPC_SAMSUNG_ISO_EXPANSION
	if(m_pDSCISOExpansion)
	{
		m_pDSCISOExpansion->ValueActiveGet()->TextGet(strValue);

		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_DPC_SAMSUNG_ISO_EXPANSION,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO_EXPANSION))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_DPC_SAMSUNG_ISO_EXPANSION;
					pSdiMsg->nParam2 = nDataSize;	
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO_EXPANSION))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_DPC_SAMSUNG_ISO_EXPANSION;
							pSdiMsg->nParam2 = nDataSize;	
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}			
		}
		//m_pDSCISOExpansion->Reset();
	}

	//-- PTP_DPC_LCD
	if(m_pDSCLCD)
	{
		m_pDSCLCD->ValueActiveGet()->TextGet(strValue);

		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_DPC_LCD,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_LCD))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_DPC_LCD;
					pSdiMsg->nParam2 = nDataSize;	
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_LCD))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_DPC_LCD;
							pSdiMsg->nParam2 = nDataSize;	
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}			
		}
		//m_pDSCLCD->Reset();
	}

	//-- PTP_DPC_PEAKING
	if(m_pDSCPeaking)
	{
		m_pDSCPeaking->ValueActiveGet()->TextGet(strValue);

		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_DPC_PEAKING,strValue);
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PEAKING))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_DPC_PEAKING;
					pSdiMsg->nParam2 = nDataSize;	
					pSdiMsg->pParam = (LPARAM)(int)nValue;
					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				//for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PEAKING))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
							pSdiMsg->nParam1 = PTP_DPC_PEAKING;
							pSdiMsg->nParam2 = nDataSize;	
							pSdiMsg->pParam = (LPARAM)(int)nValue;
							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}			
		}
		//m_pDSCPeaking->Reset();
	}	

	ESMGetPropertyListView();
}
int CESMPropertyCtrl::GetPropertyValueStep(int nPropCode,CString strValue,BOOL m_flag)
{

	CString strRet, strSection;
	CString strConfigFile;
	int nValue;	


	strConfigFile.Format(_T("%s\\%s.info"), ESMGetPath(ESM_PATH_CONFIG),m_pItem->GetDeviceModel());
	CESMIni ini;
	if(!ini.SetIniFilename (strConfigFile))
		return -1;

	strSection.Format(_T("%x"),nPropCode);

	if(m_flag)
	{	
		CString nextValue = ini.GetNextStringValue(strSection,strValue);
		if(nextValue == "")
		{
			return -1;
		}
		strRet = ini.GetStringValuetoKey(strSection, nextValue);	
		char* endptr = NULL;
		char* strTpData = NULL;
		strTpData = ESMUtil::GetCharString(strRet);
	
		if(
			nPropCode == PTP_DPC_EXPOSURE_INDEX	 ||
			nPropCode == PTP_DPC_F_NUMBER		||
			nPropCode == PTP_CODE_SAMSUNG_ZOOM_CTRL_SET	)
			nValue = strtoul(strTpData,&endptr, 10);
		else
			nValue = strtoul(strTpData,&endptr, 16);
		
		
		if( strTpData)
		{
			delete strTpData;
			strTpData = NULL;
		}

	}
	else
	{
		CString beforeValue = ini.GetBeforeStringValue(strSection,strValue);
		if(beforeValue == "")
		{
			return  -1;
		}
		strRet = ini.GetStringValuetoKey(strSection, beforeValue);	
		char* endptr = NULL;
		char* strTpData = NULL;
		strTpData = ESMUtil::GetCharString(strRet);

		if(
			nPropCode == PTP_DPC_EXPOSURE_INDEX	 ||
			nPropCode == PTP_DPC_F_NUMBER		||
			nPropCode == PTP_CODE_SAMSUNG_ZOOM_CTRL_SET	)
			nValue = strtoul(strTpData,&endptr, 10);
		else
			nValue = strtoul(strTpData,&endptr, 16);


		if( strTpData)
		{
			delete strTpData;
			strTpData = NULL;
		}
	}


	return nValue;

}

void CESMPropertyCtrl::SetISOUp()
{
	/*
	ESMEvent* pSdiMsg	= NULL;

	int nValue = 0;
	int aValue = 0;

	CString strValue;
	CString sValue;

	CObArray arDSCList;
	CDSCItem* pItem = NULL;

	if(m_pDSCISO)
	{
		m_pDSCISO->ValueActiveGet()->TextGet(strValue);
		if(!strValue.IsEmpty())
		{			
			ESMGetDSCList(&arDSCList);			
			int nDSCCnt =  arDSCList.GetCount();			
			for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
			{				
				pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
				if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
				{
					strValue =pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO);
					aValue = GetPropertyValueStep(PTP_CODE_EXPOSUREINDEX,strValue,TRUE);
					sValue = GetPropertyValueStr(PTP_CODE_EXPOSUREINDEX,aValue);
					if(aValue != -1)
					{	
						pSdiMsg = new ESMEvent;
						pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
						pSdiMsg->nParam1 = PTP_CODE_EXPOSUREINDEX;
						pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
						pSdiMsg->pParam = (LPARAM)(int)aValue;
						pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,sValue);
						pItem->SdiAddMsg(pSdiMsg);


					}
				}
			}
			SetHiddenCommand(TRUE);
		}
	}
	//m_pDSCISO->Reset();
	//m_pDSCISO->ValueDefaultFromActive();
	*/

	ESMEvent* pSdiMsg	= NULL;

	int aValue = 0;

	CString strValue;
	CString sValue;

	CObArray arDSCList;
	CDSCItem* pItem = NULL;

	ESMGetDSCList(&arDSCList);			
	int nDSCCnt =  arDSCList.GetCount();			
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{				
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		int nType = pItem->m_pSdi->GetDevinfoType(PTP_CODE_EXPOSUREINDEX);
		
		if( nType == SDI_TYPE_STRING )
			pItem->m_pSdi->GetDevinfoNextStr(PTP_CODE_EXPOSUREINDEX,strValue);
		else
			pItem->m_pSdi->GetDevinfoNextEnum(PTP_CODE_EXPOSUREINDEX,aValue);
			
		sValue.Format(_T("%d"),aValue);;
		if(aValue != -1)
		{	
			pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pSdiMsg->nParam1 = PTP_CODE_EXPOSUREINDEX;
			pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
			pSdiMsg->pParam = (LPARAM)(int)aValue;
			//pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,sValue);
			pItem->SdiAddMsg(pSdiMsg);
			pItem->m_pSdi->SetDevinfoCurrentEnum(PTP_CODE_EXPOSUREINDEX,aValue);
		}
	}
	SetHiddenCommand(TRUE);

	//GetPTPProperty(PTP_CODE_EXPOSUREINDEX);
}

void CESMPropertyCtrl::SetISODown()
{
	/*
	ESMEvent* pSdiMsg	= NULL;
	int nValue = 0;
	CString strValue;
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	int aValue;
	CString sValue;

	if(m_pDSCISO)
	{
		m_pDSCISO->ValueActiveGet()->TextGet(strValue);
		if(!strValue.IsEmpty())
		{			
			ESMGetDSCList(&arDSCList);			
			int nDSCCnt =  arDSCList.GetCount();			
			for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
			{				
				pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
				if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
				{
					strValue =pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO);
					aValue = GetPropertyValueStep(PTP_CODE_EXPOSUREINDEX,strValue,FALSE);		
					sValue = GetPropertyValueStr(PTP_CODE_EXPOSUREINDEX,aValue);
					if(aValue != -1)
					{	
						pSdiMsg = new ESMEvent;
						pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
						pSdiMsg->nParam1 = PTP_CODE_EXPOSUREINDEX;
						pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
						pSdiMsg->pParam = (LPARAM)(int)aValue;
						pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,sValue);
						pItem->SdiAddMsg(pSdiMsg);	

					}
				}
			}
			SetHiddenCommand(TRUE);
		}
	}
	//m_pDSCISO->Reset();
	//m_pDSCISO->ValueDefaultFromActive();
	*/
	ESMEvent* pSdiMsg	= NULL;

	int aValue = 0;

	CString strValue;
	CString sValue;

	CObArray arDSCList;
	CDSCItem* pItem = NULL;

	ESMGetDSCList(&arDSCList);			
	int nDSCCnt =  arDSCList.GetCount();			
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{				
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		int nType = pItem->m_pSdi->GetDevinfoType(PTP_CODE_EXPOSUREINDEX);
		
		if( nType == SDI_TYPE_STRING )
			pItem->m_pSdi->GetDevinfoNextStr(PTP_CODE_EXPOSUREINDEX,strValue);
		else
			pItem->m_pSdi->GetDevinfoPrevEnum(PTP_CODE_EXPOSUREINDEX,aValue);
			
		sValue.Format(_T("%d"),aValue);;
		if(aValue != -1)
		{	
			pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pSdiMsg->nParam1 = PTP_CODE_EXPOSUREINDEX;
			pSdiMsg->nParam2 = PTP_VALUE_UINT_16;	
			pSdiMsg->pParam = (LPARAM)(int)aValue;
			//pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,sValue);
			pItem->SdiAddMsg(pSdiMsg);
			pItem->m_pSdi->SetDevinfoCurrentEnum(PTP_CODE_EXPOSUREINDEX,aValue);
		}
	}
	SetHiddenCommand(TRUE);

	//GetPTPProperty(PTP_CODE_EXPOSUREINDEX);
}

void CESMPropertyCtrl::SetSSpeedUp()
{
	/*
	ESMEvent* pSdiMsg	= NULL;
	int nValue = 0;
	CString strValue;
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	int aValue;
	CString sValue;

	if(m_pDSCShutter)
	{	
		ESMGetDSCList(&arDSCList);			
		int nDSCCnt =  arDSCList.GetCount();			
		for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
		{				
			pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
			if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
			{
				strValue =pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_SHUTTERSPEED);
				aValue = GetPropertyValueStep(PTP_CODE_SAMSUNG_SHUTTERSPEED,strValue,TRUE);		
				sValue = GetPropertyValueStr(PTP_CODE_SAMSUNG_SHUTTERSPEED,aValue);
				if(aValue != -1)
				{	
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_SHUTTERSPEED;
					pSdiMsg->nParam2 = PTP_VALUE_UINT_32;	
					pSdiMsg->pParam = (LPARAM)(int)aValue;
					pItem->SdiAddMsg(pSdiMsg);
					pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,sValue);
				}
			}

			SetHiddenCommand(TRUE);
		}
	}
	*/
	
	
	ESMEvent* pSdiMsg	= NULL;

	int aValue = 0;

	CString strValue;
	CString sValue;

	CObArray arDSCList;
	CDSCItem* pItem = NULL;

	ESMGetDSCList(&arDSCList);			
	int nDSCCnt =  arDSCList.GetCount();			
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{				
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		int nType = pItem->m_pSdi->GetDevinfoType(PTP_CODE_SAMSUNG_SHUTTERSPEED);
		
		if( nType == SDI_TYPE_STRING )
			pItem->m_pSdi->GetDevinfoNextStr(PTP_CODE_SAMSUNG_SHUTTERSPEED,strValue);
		else
			pItem->m_pSdi->GetDevinfoNextEnum(PTP_CODE_SAMSUNG_SHUTTERSPEED,aValue);
			
		sValue.Format(_T("%d"),aValue);;
		if(aValue != -1)
		{	
			pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_SHUTTERSPEED;
			pSdiMsg->nParam2 = PTP_VALUE_UINT_32;	
			pSdiMsg->pParam = (LPARAM)(int)aValue;
			//pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,sValue);
			pItem->SdiAddMsg(pSdiMsg);
			pItem->m_pSdi->SetDevinfoCurrentEnum(PTP_CODE_SAMSUNG_SHUTTERSPEED,aValue);
		}
	}
	SetHiddenCommand(TRUE);

	//GetPTPProperty(PTP_CODE_SAMSUNG_SHUTTERSPEED);
}

void CESMPropertyCtrl::SetSSpeedDown()
{
	/*
	ESMEvent* pSdiMsg	= NULL;
	int nValue = 0;
	CString strValue;
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	int aValue;
	CString sValue;

	if(m_pDSCShutter)
	{	
		ESMGetDSCList(&arDSCList);			
		int nDSCCnt =  arDSCList.GetCount();			
		for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
		{				
			pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
			if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
			{
				strValue =pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_SHUTTERSPEED);
				aValue = GetPropertyValueStep(PTP_CODE_SAMSUNG_SHUTTERSPEED,strValue,FALSE);		
				sValue = GetPropertyValueStr(PTP_CODE_SAMSUNG_SHUTTERSPEED,aValue);
				if(aValue != -1)
				{	
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
					pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_SHUTTERSPEED;
					pSdiMsg->nParam2 = PTP_VALUE_UINT_32;	
					pSdiMsg->pParam = (LPARAM)(int)aValue;
					pItem->SdiAddMsg(pSdiMsg);
					pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,sValue);
				}
			}

			SetHiddenCommand(TRUE);
		}
	}
	*/
	ESMEvent* pSdiMsg	= NULL;

	int aValue = 0;

	CString strValue;
	CString sValue;

	CObArray arDSCList;
	CDSCItem* pItem = NULL;

	ESMGetDSCList(&arDSCList);			
	int nDSCCnt =  arDSCList.GetCount();			
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{				
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		int nType = pItem->m_pSdi->GetDevinfoType(PTP_CODE_SAMSUNG_SHUTTERSPEED);
		
		if( nType == SDI_TYPE_STRING )
			pItem->m_pSdi->GetDevinfoNextStr(PTP_CODE_SAMSUNG_SHUTTERSPEED,strValue);
		else
			pItem->m_pSdi->GetDevinfoPrevEnum(PTP_CODE_SAMSUNG_SHUTTERSPEED,aValue);
			
		sValue = GetPropertyValueStr(PTP_CODE_SAMSUNG_SHUTTERSPEED,aValue);

		if(aValue != -1)
		{	
			pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pSdiMsg->nParam1 = PTP_CODE_SAMSUNG_SHUTTERSPEED;
			pSdiMsg->nParam2 = PTP_VALUE_UINT_32;	
			pSdiMsg->pParam = (LPARAM)(int)aValue;
			pItem->SdiAddMsg(pSdiMsg);

			pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,sValue);
			pItem->m_pSdi->SetDevinfoCurrentEnum(PTP_CODE_SAMSUNG_SHUTTERSPEED,aValue);
		}
	}
	SetHiddenCommand(TRUE);

	//GetPTPProperty(PTP_CODE_SAMSUNG_SHUTTERSPEED);
}

void CESMPropertyCtrl::SetCTUp()
{
	ESMEvent* pSdiMsg	= NULL;

	int aValue = 0;
	int nDataSize;
	int nCCTCode;
	CString strValue;
	CString sValue;

	CObArray arDSCList;
	CDSCItem* pItem = NULL;

	ESMGetDSCList(&arDSCList);			
	int nDSCCnt =  arDSCList.GetCount();			
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{				
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		{
			nDataSize = PTP_DPV_UINT32;
			nCCTCode = PTP_DPC_WB_DETAIL_KELVIN;
		}
		else // NX Series
		{
			nDataSize = PTP_VALUE_UINT_16;
			nCCTCode = PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN;
		}

		int nType = pItem->m_pSdi->GetDevinfoType(nCCTCode);
		
		if( nType == SDI_TYPE_STRING )
			return;
		else
			pItem->m_pSdi->GetDevinfoNextEnum(nCCTCode,aValue);
			
		sValue.Format(_T("%d"),aValue);
		
		if(aValue != -1)
		{	
			pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pSdiMsg->nParam1 = nCCTCode;
			pSdiMsg->nParam2 = nDataSize;	
			pSdiMsg->pParam = (LPARAM)(int)aValue;
			pItem->SdiAddMsg(pSdiMsg);
			pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,sValue);

			//m_strModify[DSC_PROP_COLOR_TEMPERATURE] = sValue;
			pItem->m_pSdi->SetDevinfoCurrentEnum(PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN,aValue);
		}
	}
	SetHiddenCommand(TRUE);

	//m_pDSCCT->Reset();
	//m_pDSCCT->ValueDefaultFromActive();
	//LoadDSCProperty();
}

void CESMPropertyCtrl::SetCTDown()
{
	ESMEvent* pSdiMsg	= NULL;

	int aValue = 0;
	int nDataSize;
	int nCCTCode;
	CString strValue;
	CString sValue;

	CObArray arDSCList;
	CDSCItem* pItem = NULL;

	ESMGetDSCList(&arDSCList);			
	int nDSCCnt =  arDSCList.GetCount();			
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{				
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		{
			nDataSize = PTP_DPV_UINT32;
			nCCTCode = PTP_DPC_WB_DETAIL_KELVIN;
		}
		else // NX Series
		{
			nDataSize = PTP_VALUE_UINT_16;
			nCCTCode = PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN;
		}

		int nType = pItem->m_pSdi->GetDevinfoType(nCCTCode);

		if( nType == SDI_TYPE_STRING )
			return;
		else
			pItem->m_pSdi->GetDevinfoPrevEnum(nCCTCode,aValue);

		sValue.Format(_T("%d"),aValue);

		if(aValue != -1)
		{	
			pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pSdiMsg->nParam1 = nCCTCode;
			pSdiMsg->nParam2 = nDataSize;	
			pSdiMsg->pParam = (LPARAM)(int)aValue;
			pItem->SdiAddMsg(pSdiMsg);
			pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,sValue);

			//m_strModify[DSC_PROP_COLOR_TEMPERATURE] = sValue;
			pItem->m_pSdi->SetDevinfoCurrentEnum(PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN,aValue);
		}
	}
	SetHiddenCommand(TRUE);

	//m_pDSCCT->Reset();
	//m_pDSCCT->ValueDefaultFromActive();
	//LoadDSCProperty();
}


void CESMPropertyCtrl::SetSensorCleaning()
{
	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();

	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		{
			ESMEvent* pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pSdiMsg->nParam1 = PTP_DPC_SAMSUNG_SENSOR_CLEANING_FINISH;
			pSdiMsg->pParam = 0;

			pItem->SdiAddMsg(pSdiMsg);

			SetTimer(WM_ESM_SET_HIDDEN_ALL_CMD_TIMER, 3000, NULL);
		}
		else
		{
			//
		}
	}
}

void CESMPropertyCtrl::GetPictureWizardDetail(int nPTPCode, int nValue)
{
	m_nRGB = nValue;
	m_nPTPCode = nPTPCode;
}

void CESMPropertyCtrl::GetPictureWizardDetailETC(int nPTPCode, int nValue)
{
	CESMPWEditorDlg EditorDlg(this); 
	EditorDlg.m_nPTPCode = m_nPTPCode;
	EditorDlg.m_nPTPCodeETC = nPTPCode;
	EditorDlg.m_nValue = nValue;
	EditorDlg.m_nRGB = m_nRGB;
	EditorDlg.m_pItem = m_pItem;
	EditorDlg.DoModal();
	
}

void CESMPropertyCtrl::SetPictureWizard()
{
	if (m_pItem == NULL)
		return;

	if(m_pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
	{
		CString strValue;
		int nPS = 0;
		int nAB = 0;
		int nMG = 0;
		int nContrast, nSharpness, nNoise, nSaturation, nHue, nFilter = 0;
		CESMPWEditorDlgSetInfo info;

		strValue = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PHOTO_STYLE);	
		info.strPhotoStyle = strValue;
		if (!strValue.IsEmpty())
			info.nPS = GetPropertyValueEnum(PTP_DPC_PHOTO_STYLE, strValue);

		strValue = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_WB_ADJUST_AB);		
		if (!strValue.IsEmpty())
			info.nWBAB = GetPropertyValueEnum(PTP_DPC_WB_ADJUST_AB, strValue);

		strValue = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_WB_ADJUST_MG);		
		if (!strValue.IsEmpty())
			info.nWBMG = GetPropertyValueEnum(PTP_DPC_WB_ADJUST_MG, strValue);

		strValue = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PS_CONTRAST);		
		if (!strValue.IsEmpty())
			info.nContrast = GetPropertyValueEnum(PTP_DPC_PS_CONTRAST, strValue);

		strValue = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PS_SHARPNESS);		
		if (!strValue.IsEmpty())
			info.nSharpness = GetPropertyValueEnum(PTP_DPC_PS_SHARPNESS, strValue);

		strValue = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PS_NOISE_REDUCTION);
		if (!strValue.IsEmpty())
			info.nNoise = GetPropertyValueEnum(PTP_DPC_PS_NOISE_REDUCTION, strValue);

		strValue = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PS_SATURATION);
		if (!strValue.IsEmpty())
			info.nSaturation = GetPropertyValueEnum(PTP_DPC_PS_SATURATION, strValue);

		strValue = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PS_HUE);
		if (!strValue.IsEmpty())
			info.nHue= GetPropertyValueEnum(PTP_DPC_PS_HUE, strValue);

		strValue = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PS_FILTER_EFFECT);
		if (!strValue.IsEmpty())
			info.nFilter= GetPropertyValueEnum(PTP_DPC_PS_FILTER_EFFECT, strValue);
		
		m_PWEditorDlgGH5->SetInfo(m_pItem, info);
		m_PWEditorDlgGH5->DoModal();		

		GetDSCWBAdjustAB();
		GetDSCWBAdjustMG();
		GetDSCPSContrast();
		GetDSCPSSharpness();
		GetDSCPSNoiseReduction();
		GetDSCPSSaturation();
		GetDSCPSHue();
		GetDSCPSFilterEffect();
	}
	else
	{
		CString strValue = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_PICTUREW);
		int nValue = GetPropertyValueEnum(PTP_CODE_SAMSUNG_PICTUREWIZARD,strValue);

		if(nValue == SDI_DPV_PIC_WIZARD_CUSTOM1 || nValue == SDI_DPV_PIC_WIZARD_CUSTOM2 || nValue == SDI_DPV_PIC_WIZARD_CUSTOM3)
		{
			switch(nValue)
			{
			case SDI_DPV_PIC_WIZARD_CUSTOM1:
				GetPTPProperty(PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM1);
				GetPTPProperty(PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM1_ETC);
				break;
			case SDI_DPV_PIC_WIZARD_CUSTOM2:
				GetPTPProperty(PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM2);
				GetPTPProperty(PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM2_ETC);
				break;
			case SDI_DPV_PIC_WIZARD_CUSTOM3:
				GetPTPProperty(PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM3);
				GetPTPProperty(PTP_CODE_SAMSUNG_PW_DETAIL_CUSTOM3_ETC);
				break;
			}
		}
	}
}

void CESMPropertyCtrl::SetWhiteBalance()
{
	if (m_pItem == NULL)
		return;

	if(m_pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
	{
		CString strValue = m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_WHITEBALANCE);
		int nValue = GetPropertyValueEnum(PTP_CODE_WHITEBALANCE,strValue);

		unsigned int nWb1 = (int)strtol("800B", NULL, 16);
		unsigned int nWb4 = (int)strtol("800E", NULL, 16);

		if (nValue < nWb1 || nValue > nWb4)
		{
			AfxMessageBox(_T("Please check the White Balance."));
			return;
		}

		ESMEvent* pSdiMsg = new ESMEvent;
		pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
		pSdiMsg->nParam1 = PTP_DPC_WB_DETAIL_CUSTOM;
		pSdiMsg->pParam = 0;

		m_pItem->SdiAddMsg(pSdiMsg);

		SetTimer(WM_ESM_SET_HIDDEN_ALL_CMD_TIMER, 3000, NULL);
	}
	else
	{
		//
	}
}

void CESMPropertyCtrl::SetWhiteBalanceAll()
{
	ESMGetPropertyListView();

	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();

	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		{
			CString strValue = pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_WHITEBALANCE);

			if (strValue.IsEmpty())
			{
				ESMLog(0, _T("[%s] (Property not load) White Balance Fail"), pItem->GetDeviceUniqueID());
				continue;
			}

			int nValue = GetPropertyValueEnum(PTP_CODE_WHITEBALANCE,strValue);

			unsigned int nWb1 = (int)strtol("800B", NULL, 16);
			unsigned int nWb4 = (int)strtol("800E", NULL, 16);

			if (nValue < nWb1 || nValue > nWb4)
			{
				//AfxMessageBox(_T("Please check the White Balance."));
				ESMLog(0, _T("[%s] (Please check the White Balance) White Balance Fail"), pItem->GetDeviceUniqueID());
				continue;
			}

			ESMEvent* pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pSdiMsg->nParam1 = PTP_DPC_WB_DETAIL_CUSTOM;
			pSdiMsg->pParam = 0;

			pItem->SdiAddMsg(pSdiMsg);

			Sleep(1000);

			SetTimer(WM_ESM_SET_HIDDEN_ALL_CMD_TIMER, 3000, NULL);
		}
		else
		{ 
			//
		}
	}
}

void CESMPropertyCtrl::SetDSCVoiceOff()
{
	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
		{
			ESMEvent* pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
			pSdiMsg->nParam1 = HIDDEN_COMMAND_CHANGE_MOV_VOICE;
			//pSdiMsg->nParam1 = HIDDEN_COMMAND_CHANGEJPEGANDMOVIE;
			pSdiMsg->pParam = (int)1;		//0 : Disable, 1: Use
			pItem->SdiAddMsg(pSdiMsg);
		}
	}
}

void CESMPropertyCtrl::SetDSCModeJpgfile()
{
	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
		{
			ESMEvent* pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
			pSdiMsg->nParam1 = HIDDEN_COMMAND_MOVIEANDJPG;
			pSdiMsg->pParam = (int)0;		//0: jpeg 1:file
			pItem->SdiAddMsg(pSdiMsg);
		}
	}
}

void CESMPropertyCtrl::SetDSCModeMoviefile()
{
	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
		{
			ESMEvent* pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
			pSdiMsg->nParam1 = HIDDEN_COMMAND_MOVIEANDJPG;
			pSdiMsg->pParam = (int)1;		//0: jpeg 1:file
			pItem->SdiAddMsg(pSdiMsg);
		}
	}
}

void CESMPropertyCtrl::SetDSCLcdOn()
{
	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
		{
			ESMEvent* pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
			pSdiMsg->nParam1 = HIDDEN_COMMAND_LCD_ON;
			pSdiMsg->nParam2 = PTP_VALUE_NONE;
			pItem->SdiAddMsg(pSdiMsg);
		}
	}
}


void CESMPropertyCtrl::SetDSCLcdOff()
{
	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
		{
			ESMEvent* pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
			pSdiMsg->nParam1 = HIDDEN_COMMAND_LCD_OFF;
			pSdiMsg->nParam2 = PTP_VALUE_NONE;
			pItem->SdiAddMsg(pSdiMsg);
		}
	}
}

void CESMPropertyCtrl::SetDSCFocusZoom()
{
	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
		{
			ESMEvent* pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
			pSdiMsg->nParam1 = HIDDEN_COMMAND_SAVEFOCUSZOOM;
			pSdiMsg->nParam2 = PTP_VALUE_NONE;
			pItem->SdiAddMsg(pSdiMsg);
		}
	}
}

void CESMPropertyCtrl::SetDSCShutDown()
{
	ESMEvent* pMsg;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_LIST_CAMSHUTDOWN;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
}

//seo
void CESMPropertyCtrl::SetDSCMProperty(BOOL bAll)
{
	//-- 2013-05-04 hongsu@esmlab.com
	//-- Just Selected DSC Set Property
	//-- Check Change
	ESMEvent* pSdiMsg	= NULL;
	int nValue = 0;
	CString strValue;
	CDSCItem* pItem = NULL;
	CObArray arDSCList;


	//-- MODE
	if(m_pDSCMode)
	{
		m_pDSCMode->ValueActiveGet()->TextGet(strValue);
		if(!strValue.IsEmpty())
		{
			nValue = GetPropertyValueEnum(PTP_CODE_FUNCTIONALMODE,strValue);
			nValue = nValue - 0x8000;
			if(!bAll)
			{
				if( strValue !=  m_pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_MODE))
				{
					pSdiMsg = new ESMEvent;
					pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
					pSdiMsg->nParam1 = HIDDEN_COMMAND_CHANGE_MODE;
					pSdiMsg->nParam2 = PTP_VALUE_MODE;
					if(nValue > 0 && nValue < 6)
						pSdiMsg->pParam	 = (LPARAM)nValue;

					m_pItem->SdiAddMsg(pSdiMsg);
					m_pItem->SetDSCCaptureInfo(PTP_CODE_FUNCTIONALMODE ,strValue);
					SetHiddenCommand(bAll);
				}
			}
			else //-- Set All 
			{
				ESMGetDSCList(&arDSCList);
				int nDSCCnt =  arDSCList.GetCount();
				for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
				{
					pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
					if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
					{
						if( strValue !=  pItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_MODE))
						{
							pSdiMsg = new ESMEvent;
							pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
							pSdiMsg->nParam1 = HIDDEN_COMMAND_CHANGE_MODE;
							pSdiMsg->nParam2 = PTP_VALUE_MODE;
							if(nValue > 0 && nValue < 6)
								pSdiMsg->pParam	 = (LPARAM)nValue;

							pItem->SdiAddMsg(pSdiMsg);
							pItem->SetDSCCaptureInfo(PTP_CODE_FUNCTIONALMODE ,strValue);
						}
					}
				}
				SetHiddenCommand(TRUE);
			}
		}
		m_pDSCMode->Reset();
	}

}

void CESMPropertyCtrl::SetHiddenCommand(BOOL bAll)
{
	ESMEvent* pSdiMsg	= NULL;
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	if(!bAll)
	{
		pSdiMsg = new ESMEvent;
		pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
		pSdiMsg->nParam1 = HIDDEN_COMMAND_BANK_SAVE;
		pSdiMsg->nParam2 = PTP_VALUE_NONE;
		m_pItem->SdiAddMsg(pSdiMsg);	
	}
	else //-- Set All 
	{
		ESMGetDSCList(&arDSCList);
		int nDSCCnt =  arDSCList.GetCount();
		for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
		{
			pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
			if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
			{
				pSdiMsg = new ESMEvent;
				pSdiMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
				pSdiMsg->nParam1 = HIDDEN_COMMAND_BANK_SAVE;
				pSdiMsg->nParam2 = PTP_VALUE_NONE;
				pItem->SdiAddMsg(pSdiMsg);
			}
		}
	}
}

void CESMPropertyCtrl::SetHiddenCommandFocus(BOOL bAll)
{
	ESMEvent* pSdiMsg	= NULL;
	CDSCItem* pItem = NULL;
	CObArray arDSCList;
	if(!bAll)
	{
		pSdiMsg = new ESMEvent;
		pSdiMsg->message = WM_SDI_OP_FOCUS_LOCATE_SAVE;
		m_pItem->SdiAddMsg(pSdiMsg);	
	}
	else //-- Set All 
	{
		ESMGetDSCList(&arDSCList);
		int nDSCCnt =  arDSCList.GetCount();
		for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
		{
			pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
			if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
			{
				pSdiMsg = new ESMEvent;
				pSdiMsg->message = WM_SDI_OP_FOCUS_LOCATE_SAVE;
				pItem->SdiAddMsg(pSdiMsg);
			}
		}
	}

	SetTimer(WM_ESM_SET_HIDDEN_ALL_CMD_TIMER, 1000, NULL);
}
//seo
void CESMPropertyCtrl::SaveSetting(CString strFileName)
{
	CObArray arDSCList;
	CString strSutter, strFNum, strISO, strColor, strPictureWizard;
	int nValue = 0;
	CDSCItem* pItem = NULL;
	CString strPath = strFileName + _T(".info");
	CESMIni ini;

	CFile WriteFile;
	if(WriteFile.Open(strPath, CFile::modeCreate | CFile::modeRead |CFile::modeNoTruncate))
		WriteFile.Close();
	else
		return ;

	if(!ini.SetIniFilename (strPath))
		return;

	ESMGetDSCList(&arDSCList);
	int nAll = arDSCList.GetCount();
	for(int nIdx=0; nIdx<nAll; nIdx++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		strSutter = pItem->m_strCaptureInfo[DSC_INFO_CAPTURE_SHUTTERSPEED];
		strFNum = pItem->m_strCaptureInfo[DSC_INFO_CAPTURE_APERTURE];
		strISO = pItem->m_strCaptureInfo[DSC_INFO_CAPTURE_ISO];
		strColor = pItem->m_strCaptureInfo[DSC_INFO_CAPTURE_COLORTEMPERATURE];
		strPictureWizard = pItem->m_strCaptureInfo[DSC_INFO_CAPTURE_PICTUREW];
		ini.WriteString(_T("Sutter Speed"), pItem->GetDeviceDSCID(), strSutter);
		ini.WriteString(_T("F-Num"), pItem->GetDeviceDSCID(), strFNum);
		ini.WriteString(_T("ISO"), pItem->GetDeviceDSCID(), strISO);
		ini.WriteString(_T("Color Temperature"), pItem->GetDeviceDSCID(), strColor);
		ini.WriteString(_T("Picture Wizard"), pItem->GetDeviceDSCID(), strPictureWizard);
	}
}

void CESMPropertyCtrl::LoadSetting(CString strFileName)
{
	CString strSettingForder;
	strSettingForder.Format(_T("%s"), ESMGetPath(ESM_PATH_SETUP));
	CObArray arDSCList;
	CString strSutter, strFNum, strISO, strColor, strState, strPictureWizard;
	int nSutter=0, nFnum=0, nISO=0, nColor=0, nStatus=0, nPictureWizard;
	CDSCItem* pItem = NULL;
	CESMIni ini;

	if(!ini.SetIniFilename (strFileName))
		return;

	ESMGetDSCList(&arDSCList);
	int nAll = arDSCList.GetCount();

	for(int nIdx=0; nIdx<nAll; nIdx++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		strState = pItem->GetDSCInfo(3);
		if(strState == "Unknown")
			nStatus = nStatus + 1;
	}

	if(nStatus == nAll)
		return;

	for(int nIdx=0; nIdx<nAll; nIdx++)
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		strSutter = ini.GetString(_T("Sutter Speed"), pItem->GetDeviceDSCID());
		if(m_pItem->GetDeviceModel().Compare(_T("NX3000")) == 0)
			nSutter = GetPropertyValueEnumCount(PTP_CODE_SAMSUNG_SHUTTERSPEED,strSutter);
		else
			nSutter = GetPropertyValueEnum(PTP_CODE_SAMSUNG_SHUTTERSPEED,strSutter);
		CameraSetting(nIdx, PTP_CODE_SAMSUNG_SHUTTERSPEED, PTP_VALUE_UINT_32, nSutter, strSutter);

		strFNum = ini.GetString(_T("F-Num"), pItem->GetDeviceDSCID());
		nFnum = GetPropertyValueEnum(PTP_CODE_F_NUMBER,strFNum);
		CameraSetting(nIdx, PTP_CODE_F_NUMBER, PTP_VALUE_UINT_16, nFnum, strFNum);

		strISO = ini.GetString(_T("ISO"), pItem->GetDeviceDSCID());
		nISO = GetPropertyValueEnum(PTP_CODE_EXPOSUREINDEX,strISO);
		CameraSetting(nIdx, PTP_CODE_EXPOSUREINDEX, PTP_VALUE_UINT_16, nISO, strISO);

		strColor = ini.GetString(_T("Color Temperature"), pItem->GetDeviceDSCID());
		nColor = _ttoi(strColor);
		CameraSetting(nIdx, PTP_CODE_SAMSUNG_WB_DETAIL_KELVIN, PTP_VALUE_UINT_16, nColor, strColor);

		strPictureWizard = ini.GetString(_T("Picture Wizard"), pItem->GetDeviceDSCID());
		nPictureWizard = GetPropertyValueEnum(PTP_CODE_SAMSUNG_PICTUREWIZARD, strPictureWizard);
		CameraSetting(nIdx, PTP_CODE_SAMSUNG_PICTUREWIZARD, PTP_VALUE_UINT_16, nPictureWizard, strPictureWizard);
	}
}

void CESMPropertyCtrl::CameraSetting(int nIdx, int PtpCode, int PtpUint, int nValue, CString strValue)
{
	CObArray arDSCList;
	ESMGetDSCList(&arDSCList);
	m_pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
	ESMEvent* pSdiMsg	= NULL;
	pSdiMsg = new ESMEvent;
	pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	pSdiMsg->nParam1 = PtpCode;
	pSdiMsg->nParam2 = PtpUint;	
	pSdiMsg->pParam = (LPARAM)(int)nValue;
	m_pItem->SdiAddMsg(pSdiMsg);
	m_pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
	SetHiddenCommand(FALSE);
}

void CESMPropertyCtrl::GetPropertyIsoIncrease(CDSCItem* pDscItem)
{
	if( !m_pDSCISO)
		return;

	CString strIsoData, strCurIsoData;
	int nSetIndex = 0;
	strCurIsoData =  pDscItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO);
	for ( int i =0 ; i< m_arrIsoData.size();i++)
	{
		strIsoData = m_arrIsoData.at(i);
		if(strCurIsoData == strIsoData)
		{
			if(i != 0)
			{
				nSetIndex = i;
				break;
			}
		}
	}

	nSetIndex++;

	int nArrSize = m_arrIsoData.size();
	if( nSetIndex >= nArrSize || nSetIndex < 0)
	{
		return;
	}
	
	CString strValue = m_arrIsoData.at(nSetIndex);
	int nValue = 0;
	ESMEvent* pMsg = NULL;
	if(m_pDSCISO)
	{
		nValue = GetPropertyValueEnum(PTP_CODE_EXPOSUREINDEX,strValue);
		if( strValue !=  pDscItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO))
		{
			pMsg = new ESMEvent;
			pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pMsg->nParam1 = PTP_CODE_EXPOSUREINDEX;
			pMsg->nParam2 = PTP_VALUE_UINT_16;	
			pMsg->pParam = (LPARAM)(int)nValue;
			pDscItem->SdiAddMsg(pMsg);	
			pDscItem->SetDSCCaptureInfo(pMsg->nParam1 ,strValue);
			SetHiddenCommand();
		}
	}
}

void CESMPropertyCtrl::GetPropertyIsoDecrease(CDSCItem* pDscItem)
{
	if( !m_pDSCISO)
		return;

	CString strIsoData, strCurIsoData;
	int nSetIndex = 0;
	strCurIsoData =  pDscItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO);
	for ( int i =0 ; i< m_arrIsoData.size();i++)
	{
		strIsoData = m_arrIsoData.at(i);
		if(strCurIsoData == strIsoData)
		{
			if(i > 1)
			{
				nSetIndex = i;
				break;
			}
		}
	}
	if( nSetIndex == m_arrIsoData.size())
		return;

	nSetIndex--;

	int nArrSize = m_arrIsoData.size();
	if( nSetIndex >= nArrSize || nSetIndex < 0)
	{
		return;
	}

	CString strValue = m_arrIsoData.at(nSetIndex);
	int nValue = 0;
	ESMEvent* pMsg = NULL;
	if(m_pDSCISO)
	{
		nValue = GetPropertyValueEnum(PTP_CODE_EXPOSUREINDEX,strValue);
		if( strValue !=  pDscItem->GetDSCCaptureInfo(DSC_INFO_CAPTURE_ISO))
		{
			pMsg = new ESMEvent;
			pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pMsg->nParam1 = PTP_CODE_EXPOSUREINDEX;
			pMsg->nParam2 = PTP_VALUE_UINT_16;	
			pMsg->pParam = (LPARAM)(int)nValue;
			pDscItem->SdiAddMsg(pMsg);	
			pDscItem->SetDSCCaptureInfo(pMsg->nParam1 ,strValue);
			SetHiddenCommand();
		}
	}
}

void CESMPropertyCtrl::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
	case WM_ESM_SET_HIDDEN_ALL_CMD_TIMER:
		{
			KillTimer(WM_ESM_SET_HIDDEN_ALL_CMD_TIMER);
			SetHiddenCommand(TRUE);
		}
		break;
	case WM_ESM_SET_FOCUS_LOCATE_ALL_CMD_TIMER:
		{
			KillTimer(WM_ESM_SET_FOCUS_LOCATE_ALL_CMD_TIMER);
			SetHiddenCommandFocus(TRUE);
		}
	default:
		break;
	}

	CExtPropertyGridCtrl::OnTimer(nIDEvent);
}

void CESMPropertyCtrl::SetLoadFocus(CString strPath, BOOL bFocusMove)
{
	m_nFocusMoveCount = 0;

	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt = arDSCList.GetCount();

	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
			return;

		int nLoadValue = pItem->GetLoadFocusValue(strPath);		
		int nCurValue = pItem->Focus();

		if (nLoadValue < 0 || nCurValue < 0)
			continue;

		if (nLoadValue == nCurValue)
			continue;

		int nGap = nCurValue - nLoadValue;
		int nAbsGap = abs(nGap);

		int nStepFar = 0;
		int nStepNear = 0;
		double dStepNear = 0;

		nStepFar = nAbsGap / STEP_FAR;

		dStepNear = (double)(nAbsGap % STEP_FAR) / (double)STEP_NEAR;
		nStepNear = floor(dStepNear+0.5);

		
		ESMLog(5, _T("[%s] Focus Gap : %d, Camera Focus : %d,  Load Focus : %d  (Need move >> step170: %d, step17: %d)"), 
			pItem->GetDeviceDSCID(), nAbsGap, nCurValue, nLoadValue, nStepFar, nStepNear);	

		if (bFocusMove)
		{
			if (nAbsGap < STEP_NEAR/2 + 1)
				continue;
		}

		if (bFocusMove == FALSE)
			continue;

		for (int i = 0; i < nStepFar ; i++)
		{
			int nValue = 0;
			if (nGap > 0) nValue = 1;
			else nValue = 4;
			
			m_nFocusMoveCount++;

			ESMEvent* pMsg = new ESMEvent();
			pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pMsg->nParam1 = PTP_OC_SAMSUNG_SetFocusPosition;
			pMsg->nParam2 = nValue;
			pMsg->pParam = (LPARAM) nValue;

			pItem->SdiAddMsg(pMsg);
		}

		for (int i = 0; i < nStepNear ; i++)
		{
			int nValue = 0;
			if (nGap > 0) nValue = 2;
			else nValue = 3;

			m_nFocusMoveCount++;

			ESMEvent* pMsg = new ESMEvent();
			pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pMsg->nParam1 = PTP_OC_SAMSUNG_SetFocusPosition;
			pMsg->nParam2 = nValue;
			pMsg->pParam = (LPARAM) nValue;

			pItem->SdiAddMsg(pMsg);			
		}				
	}

	if (bFocusMove == FALSE)
		return;

	if (m_nFocusMoveCount < 1)
	{
		::SendMessage(GetParent()->GetSafeHwnd(), WM_ESM_LOAD_FOCUS_END, NULL, NULL);
		return;
	}

	// Focus 이동이 끝에 끝 > 1분
	SetTimer(WM_ESM_SET_FOCUS_LOCATE_ALL_CMD_TIMER, 1000 * 60, NULL);
}

void CESMPropertyCtrl::SetSaveFocus(CString strPath)
{
	if (strPath.IsEmpty())
	{
		CString strTime;
		CTime time  = CTime::GetCurrentTime();	
		strTime.Format(_T("%04d%02d%02d_%02d%02d%02d"),
			time.GetYear(),
			time.GetMonth(),
			time.GetDay(),
			time.GetHour(),
			time.GetMinute(),
			time.GetSecond());

		strPath.Format(_T("%s\\foc\\Focus"), ESMGetPath(ESM_PATH_SETUP));
		strPath += strTime;
	}

	CObArray arDSCList;
	CDSCItem* pItem = NULL;
	ESMGetDSCList(&arDSCList);
	int nDSCCnt = arDSCList.GetCount();

	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		if(pItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
			return;

		int nCurValue = pItem->Focus();

		pItem->SetFocusData(strPath, nCurValue);
	}
}

int CESMPropertyCtrl::ResultFocusMoveCount()
{
	m_nFocusMoveCount--;

	if (m_nFocusMoveCount < 1)
	{
		m_nFocusMoveCount = 0;

		SetTimer(WM_ESM_SET_HIDDEN_ALL_CMD_TIMER, 1500, NULL);
	}

	return m_nFocusMoveCount;
}

CString CESMPropertyCtrl::GetMovieSizeCompareFrequency( CString strSize, BOOL bPAL)
{
	CString strRtn;

	if (bPAL)
	{
		if(strSize.Find(_T("4K/30p")) != -1)
			strRtn = _T("4K/25p");
		else if(strSize.Find(_T("4K/60p")) != -1)
			strRtn = _T("4K/50p");
		else if(strSize.Find(_T("FHD/30p")) != -1)
			strRtn = _T("FHD/25p");
		else if(strSize.Find(_T("FHD/60p")) != -1)
			strRtn = _T("FHD/50p");
	}
	else
	{
		if(strSize.Find(_T("4K/25p")) != -1)
			strRtn = _T("4K/30p");
		else if(strSize.Find(_T("4K/50p")) != -1)
			strRtn = _T("4K/60p");
		else if(strSize.Find(_T("FHD/25p")) != -1)
			strRtn = _T("FHD/30p");
		else if(strSize.Find(_T("FHD/50p")) != -1)
			strRtn = _T("FHD/60p");
	}

	return strRtn;
}

void CESMPropertyCtrl::SetFrequency( BOOL bPAL )
{
	//-- PTP_DPC_SET_SYSTEM_FREQUENCY

	CDSCItem* pItem = NULL;
	ESMEvent* pSdiMsg	= NULL;
	CObArray arDSCList;
	CString strValue;
	strValue = _T("NTSC");
	if (bPAL)	
		strValue = _T("PAL");

	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	//for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);

		if (pItem == NULL)
			continue;

		if (pItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
			continue;

		if(pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
		{
				pSdiMsg = new ESMEvent;
				pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
				pSdiMsg->nParam1 = PTP_DPC_SET_SYSTEM_FREQUENCY;
				pSdiMsg->nParam2 = PTP_DPV_UINT32;	
				pSdiMsg->pParam = (LPARAM)(int)bPAL;
				pItem->SdiAddMsg(pSdiMsg);
				pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
		}
	}

	CString strTemp;
	if (bPAL)
		strTemp = _T("4K/50p");
	else 
		strTemp = _T("4K/60p");

	m_strModify[DSC_PROP_MOVIE_SIZE] = strTemp;
}

void CESMPropertyCtrl::SetLensPower( BOOL bOff )
{
	CDSCItem* pItem = NULL;
	ESMEvent* pSdiMsg	= NULL;
	CObArray arDSCList;
	CString strValue;
	strValue = _T("ON");
	if (bOff)	
		strValue = _T("OFF");

	ESMLog(5, _T("Camera Lens Power control [%s]"), strValue);

	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
		//for (int nIdx = m_nStartIndex; nIdx < m_nEndIndex+1; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if (pItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
			continue;

		if( pItem && pItem->GetDSCStatus() != SDI_STATUS_UNKNOWN)
		{
			pSdiMsg = new ESMEvent;
			pSdiMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
			pSdiMsg->nParam1 = PTP_DPC_LENS_POWER_CTRL;
			pSdiMsg->nParam2 = PTP_DPV_UINT32;	
			pSdiMsg->pParam = (LPARAM)(int)bOff;
			pItem->SdiAddMsg(pSdiMsg);
			pItem->SetDSCCaptureInfo(pSdiMsg->nParam1 ,strValue);
		}
	}
}

void CESMPropertyCtrl::GetPropertyAll( int nProp )
{
	CDSCItem* pItem = NULL;
	ESMEvent* pMsg	= NULL;
	CObArray arDSCList;

	ESMGetDSCList(&arDSCList);
	int nDSCCnt =  arDSCList.GetCount();
	for (int nIdx = 0; nIdx < nDSCCnt; nIdx++ )
	{
		pItem = (CDSCItem*)arDSCList.GetAt(nIdx);
		if(!pItem)
			continue;

		if (pItem->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
			return;

		pMsg = new ESMEvent;
		pMsg->message = WM_SDI_OP_GET_PROPERTY_DESC;
		pMsg->nParam1 = nProp;
		pMsg->pDest = (LPARAM)pItem;
		pItem->SdiAddMsg(pMsg);
	}

}

int CESMPropertyCtrl::GetCamVersion()
{
	if (m_pDSCFWVersion == NULL)
		return -1;

	int nValue = 0;
	CString strValue;
	m_pDSCFWVersion->ValueActiveGet()->TextGet(strValue);

	if (strValue.IsEmpty())
		return -1;

	nValue = GetPropertyValueEnum(PTP_DPC_SAMSUNG_FW_VERSION,strValue);

	if (nValue == 0)
	{
		nValue = _ttoi(strValue);
	}

	return nValue;
}
