////////////////////////////////////////////////////////////////////////////////
//
//	ESMPropertyView.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2013-05-04
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ESMPropertyView.h"
#include "resource.h"
#include "MainFrm.h"
#include "SdiSingleMgr.h"
#include "ESMIni.h"
#include "LoadFocusDlg.h"
#include "SysFrequency.h"
#include "LensPowerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CESMPropertyView dialog
CESMPropertyView::CESMPropertyView(CWnd* pParent /*=NULL*/)
	: CDialog(CESMPropertyView::IDD, pParent)	
	, m_pPropertyCtrl	(NULL)
{	
	m_bInitStart = FALSE;
	m_LoadFocusDlg = NULL;
}

CESMPropertyView::~CESMPropertyView() 
{
	if(m_pPropertyCtrl)
	{
		delete m_pPropertyCtrl;
		m_pPropertyCtrl = NULL;
	}

	if(m_LoadFocusDlg)
	{
		delete m_LoadFocusDlg;
		m_LoadFocusDlg = NULL;
	}
}

void CESMPropertyView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CESMPropertyView)
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_FAVORITES_TOOLBAR, m_wndToolBar);	
}

BOOL CESMPropertyView::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg)
	{
		if(pMsg->message == WM_KEYDOWN)
		{
			if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
				return FALSE;

			ESMEvent* pEsmMsg = NULL;
			pEsmMsg = new ESMEvent();
			pEsmMsg->message = WM_ESM_VIEW_CONTROL_MAIN;
			pEsmMsg->pParam = (LPARAM)pMsg;
			::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_VIEW, (LPARAM)pEsmMsg);
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CESMPropertyView, CDialog)
	//{{AFX_MSG_MAP(CESMPropertyView)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()	
	ON_WM_PAINT()
	ON_COMMAND(ID_IMAGE_GET_PROP		, OnGetProperty)
	ON_COMMAND(ID_IMAGE_SET_MPROP		, OnSetMProperty)
	ON_COMMAND(ID_IMAGE_SET_MPROP_ALL	, OnSetMPropertyAll)
	ON_COMMAND(ID_IMAGE_SET_PROP		, OnSetProperty)
	ON_COMMAND(ID_IMAGE_SET_PROP_ALL	, OnSetPropertyAll)
	ON_COMMAND(ID_IMAGE_SET_ISO_UP		, OnSetISOUp)
	ON_COMMAND(ID_IMAGE_SET_ISO_DOWN	, OnSetISODown)
	ON_COMMAND(ID_IMAGE_SET_SHUTTERS_SPEED_UP		,OnSetShuttersSpeedUp)
	ON_COMMAND(ID_IMAGE_SET_SHUTTERS_SPEED_DOWN		,OnSetShuttersSpeedDown)
	ON_COMMAND(ID_IMAGE_SET_CT_UP		, OnSetCTUp)
	ON_COMMAND(ID_IMAGE_SET_CT_DOWN		, OnSetCTDown)
	ON_COMMAND(ID_IMAGE_SET_SAVE		, OnSetSave)
	ON_COMMAND(ID_IMAGE_SET_LOAD		, OnSetLoad)
	ON_COMMAND(ID_IMAGE_FOCUSZOOMSAVE	, OnSetFocusZoom)
	ON_COMMAND(ID_IMAGE_SET_FOCUS_SAVE	, OnSetFocusSave)		
	ON_COMMAND(ID_IMAGE_SET_FOCUS_LOAD	, OnSetFocusLoad)			
	ON_COMMAND(ID_IMAGE_SET_CAMSHUTDOWN	, OnSetPropertyCamShutdown)
	ON_COMMAND(ID_IMAGE_SET_PROP_FORMAT	, OnSetPropertyFormat)
	ON_COMMAND(ID_IMAGE_SET_FWUPDATE	, OnSetPropertyFWUpdate)		//-- 2014-9-4 hongsu@esmlab.com					
	ON_COMMAND(ID_IMAGE_SET_SENSOR_CLEANING	, OnSetPropertySensorCleaning)
	ON_COMMAND(ID_IMAGE_SET_WB	, OnSetWhiteBalance)
	ON_COMMAND(ID_IMAGE_SET_WB_ALL	, OnSetWhiteBalanceAll)
	ON_COMMAND(ID_IMAGE_SET_PW	, OnSetPictureWizard)
	ON_COMMAND(ID_IMAGE_SET_BANKSAVE, OnImageSetBanksave)
	//ON_COMMAND(ID_IMAGE_SET_VOICEOFF	, OnSetVoiceOff)
	//ON_COMMAND(ID_IMAGE_GET_FOCUS		, OnSetPropertyFocusGet)
	//ON_COMMAND(ID_IMAGE_SET_LCD_ON		, OnSetLcdOn)
	//ON_COMMAND(ID_IMAGE_SET_LCD_OFF		, OnSetLcdOff)
	ON_MESSAGE(WM_ESM_LOAD_FOCUS, &CESMPropertyView::OnEsmLoadFocus)
	ON_MESSAGE(WM_ESM_LOAD_FOCUS_END, &CESMPropertyView::OnEsmLoadFocusEnd)	
	ON_COMMAND(ID_IMAGE_SET_FREQUENCY, &CESMPropertyView::OnImageSetFrequency)
	ON_COMMAND(ID_IMAGE_SET_LENS_POWER, &CESMPropertyView::OnImageSetLensPower)
END_MESSAGE_MAP() 

/////////////////////////////////////////////////////////////////////////////
// CESMPropertyView message handlers

BOOL CESMPropertyView::OnInitDialog() 
{
	if( !CDialog::OnInitDialog() )
	{
		ASSERT( FALSE );
		return FALSE;
	}	

	//-- SET MAIN WINDOWS HANDLE
	m_hMainWnd = AfxGetMainWnd()->GetSafeHwnd();

	//-- Create Liveview
	m_pPropertyCtrl = new CESMPropertyCtrl();
	m_pPropertyCtrl->Create(this,IDC_PROPERTY_CTRL,CRect(CPoint(5,5),CSize(1000,1000)),WS_VISIBLE|WS_CHILD);	

	m_LoadFocusDlg = new CLoadFocusDlg(this);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CESMPropertyView

void CESMPropertyView::OnPaint()
{	
	CDialog::OnPaint();
}

void CESMPropertyView::InitImageFrameWnd()
{
	UINT arrCvTbBtns[] =
	{
		//ID_IMAGE_GET_PROP		,
		ID_SEPARATOR			,
		//ID_IMAGE_SET_MPROP,		
		//ID_IMAGE_SET_MPROP_ALL,
		ID_SEPARATOR			,
		ID_IMAGE_SET_PROP		,	
		ID_IMAGE_SET_PROP_ALL	,
		ID_SEPARATOR			,
		ID_IMAGE_SET_ISO_UP		,
		ID_IMAGE_SET_ISO_DOWN	,
		ID_SEPARATOR			,
		ID_IMAGE_SET_SHUTTERS_SPEED_UP	,
		ID_IMAGE_SET_SHUTTERS_SPEED_DOWN,
		ID_SEPARATOR			,
		ID_IMAGE_SET_CT_UP		,
		ID_IMAGE_SET_CT_DOWN	,
// 		ID_SEPARATOR			,
// 		ID_IMAGE_SET_LOAD		,
// 		ID_IMAGE_SET_SAVE		,
		ID_SEPARATOR			,
		//ID_IMAGE_FOCUSZOOMSAVE	,
		ID_IMAGE_SET_FOCUS_SAVE	,	
		ID_IMAGE_SET_FOCUS_LOAD	,
		ID_SEPARATOR			,
		//ID_IMAGE_SET_CAMSHUTDOWN,
		//ID_IMAGE_SET_PROP_FORMAT,
		ID_SEPARATOR			,
		//ID_IMAGE_SET_FWUPDATE	,
		ID_IMAGE_SET_SENSOR_CLEANING	,
		ID_IMAGE_SET_WB			,
		ID_IMAGE_SET_WB_ALL		,
		ID_IMAGE_SET_PW			,
		//ID_IMAGE_SET_BANKSAVE	,
		ID_IMAGE_SET_FREQUENCY	,
		//ID_IMAGE_SET_LENS_POWER ,

		//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
		//ID_IMAGE_GET_FOCUS_ALL	,
		//ID_IMAGE_GET_FOCUS		,
		//ID_IMAGE_SET_FOCUS		,
		//ID_IMAGE_SET_LCD_ON		, 
		//ID_IMAGE_SET_LCD_OFF		,
		//ID_IMAGE_SET_CAMSHUTDOWN	,
		
	};
	VERIFY(m_wndToolBar.SetButtons(arrCvTbBtns,sizeof(arrCvTbBtns)/sizeof(arrCvTbBtns[0])));	
	GetClientRect(m_rcClientFrame);	
	RepositionBars(0,0xFFFF,0);	

	m_wndToolBar.ShowWindow(SW_SHOW);
}


void CESMPropertyView::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);		
	RepositionBars(0,0xFFFF,0);	

	if( m_rcClientFrame.Width() == 0 && m_rcClientFrame.Height() == 0 )
		return;

	GetClientRect(m_rcClientFrame);
	//m_rcClientFrame.top	= ESM_TOOLBAR_HEIGHT;
	m_rcClientFrame.top	= 40;
	RepositionBars(0,0xFFFF,0);	

	if(m_pPropertyCtrl->GetSafeHwnd())
		m_pPropertyCtrl->MoveWindow(0, 40, cx, cy - 40);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CESMPropertyView::OnGetProperty()
{
	m_pPropertyCtrl->LoadDSCProperty();
}

void CESMPropertyView::OnSetProperty()
{
	m_pPropertyCtrl->SetDSCProperty(FALSE);
}

void CESMPropertyView::OnSetPropertyAll()
{
	//-- 2013-05-06 hongsu@esmlab.com
	//-- Send All DSC
	m_pPropertyCtrl->SetDSCProperty(TRUE);
}

void CESMPropertyView::OnSetPropertyFormat()
{
	//-- 2013-10-14 yongmin@esmlab.com
	ESMEvent* pMsg;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_LIST_FORMAT_ALL;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
}

//-- 2014-9-4 hongsu@esmlab.com
//-- F/W Update
void CESMPropertyView::OnSetPropertyFWUpdate()
{
	int nRet = MessageBox(_T("Do you want to update the firmware?"),_T("Question"), MB_YESNO);
	if (nRet == IDYES)
	{
		ESMEvent* pMsg;
		pMsg = new ESMEvent;
		pMsg->message = WM_ESM_LIST_FW_UPDATE;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
	}
}

//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
void CESMPropertyView::OnSetPropertyFocusALL()
{	
	ESMEvent* pMsg;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_LIST_GETFOCUS_TOCLIENT;
	//CMiLRe 20160204 Focus All Setting 시 Focus 저장되는 버그 수정
	pMsg->nParam3 = TRUE;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
}

//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
void CESMPropertyView::OnSetPropertyFocusGet()
{	
	ESMEvent* pMsg;
	pMsg = new ESMEvent;
	pMsg->message = WM_ESM_LIST_GETFOCUS_TOCLIENT_READ_ONLY;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
}

//CMiLRe 20160113 Focus 개별 Get/Set, Reference Value 저장/비교
#include "FocusSettingDlg.h"
void CESMPropertyView::OnSetPropertyFocusSet()
{	
	
	if(m_pPropertyCtrl->GetItem()!= NULL)
	{
		int sdfgsdfg = m_pPropertyCtrl->GetItem()->m_nMinFocus;
		CFocusSettingDlg dlg(m_pPropertyCtrl->GetItem()->m_nMinFocus, m_pPropertyCtrl->GetItem()->m_nMaxFocus, m_pPropertyCtrl->GetItem()->m_nCurFocus); ;
		if (dlg.DoModal() == IDOK);
		{
			CString strDSCID = m_pPropertyCtrl->GetItem()->GetDeviceDSCID();
			int nCurrent = dlg.m_nCurrentFocus;
			int nMin = dlg.m_nMinFocus;
			if(dlg.m_nMaxFocus == 0) return;

			CString* pStr = new CString;
			pStr->Format(_T("%s"),strDSCID);

			ESMEvent* pMsg;
			pMsg = new ESMEvent;
			pMsg->message = WM_ESM_LIST_SETFOCUS_TOCLIENT;
			pMsg->pDest		= (LPARAM)pStr;
			pMsg->nParam1 = nMin;
			pMsg->nParam2 = nCurrent;
			::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
		}
	}
}

//seo
void CESMPropertyView::OnSetMProperty()
{
	m_pPropertyCtrl->SetDSCMProperty(FALSE);
}
//seo
void CESMPropertyView::OnSetMPropertyAll()
{
	m_pPropertyCtrl->SetDSCMProperty(TRUE);
}

void CESMPropertyView::OnSetVoiceOff()
{
	m_pPropertyCtrl->SetDSCVoiceOff();
}
void CESMPropertyView::OnSetPropertyCamShutdown()
{
	int nRet = MessageBox(_T("Are you sure you want shut down the Camera?"),_T("Question"), MB_YESNO);
	if (nRet == IDYES)
	{
		ESMEvent* pMsg;
		pMsg = new ESMEvent;
		pMsg->message = WM_ESM_LIST_CAMSHUTDOWN;
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
	}
}

//seo
void CESMPropertyView::OnSetSave()
{
	CString strSettingForder;

	strSettingForder.Format(_T("%s"), ESMGetPath(ESM_PATH_SETUP));
	CreateDirectory(strSettingForder, NULL);
	CString szFilter = _T("Setting File (*.info)|*.info|");
	CString strTitle = _T("Setting Files Save");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strSettingForder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = strSettingForder;

	CString strFile;
	strFile.Format(_T("Setting"));

	int length = 1024;
	LPWSTR pwsz = strFile.GetBuffer(length);
	dlg.m_ofn.lpstrFile = pwsz;
	strFile.ReleaseBuffer();

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		//strFileName = dlg.GetFileTitle();
		strFileName = dlg.GetPathName();
		m_pPropertyCtrl->SaveSetting(strFileName);
	}
}
//seo
void CESMPropertyView::OnSetLoad()
{
	CString strSettingForder;

	strSettingForder.Format(_T("%s"), ESMGetPath(ESM_PATH_SETUP));
	CreateDirectory(strSettingForder, NULL);
	CString szFilter = _T("Setting Files (*.info)|*.info|");
	CString strTitle = _T("Setting Files Save");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strSettingForder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strSettingForder;

	if( dlg.DoModal() == IDOK )
	{
		CString strFileName;
		strFileName = dlg.GetPathName();
		m_pPropertyCtrl->LoadSetting(strFileName);
		CString strText;
		strText.LoadString(IDR_MAINFRAME);
		CString strTitle = strText + _T(" - ") + strFileName;
		SetWindowText(strTitle);
	}
}

void CESMPropertyView::OnSetGOPMode()
{
	if(!m_bInitStart)
	{
		ESMLog(1, _T("Set Record GOP Mode!!"));
		m_pPropertyCtrl->SetDSCModeMoviefile();
		m_bInitStart = TRUE;
	}
	
}

void CESMPropertyView::OnSetFocusSave()
{
	if (m_pPropertyCtrl->GetItem() == NULL)
		return;

	//Focus Save File Message
	if (m_pPropertyCtrl->GetItem()->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
	{
		HWND m_hMainWnd = ESMGetMainWnd();
		ESMEvent* pMsg	= new ESMEvent;
		pMsg->message = WM_ESM_LIST_GETFOCUS_TOCLIENT_READ_ONLY;
		::SendMessage(m_hMainWnd, WM_ESM, (WPARAM)WM_ESM_LIST, (LPARAM)pMsg);
	}
	else
	{
		m_pPropertyCtrl->GetPropertyAll(PTP_OC_SAMSUNG_GetFocusPosition);

	}

	CString strPathName;
	CString strFocForder;

	strFocForder.Format(_T("%s\\foc"), ESMGetPath(ESM_PATH_SETUP));
	CreateDirectory(strFocForder, NULL);
	CString szFilter = _T("Focus File (*.foc)|*.foc|");
	CString strTitle = _T("Focus Files Save");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strFocForder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = strFocForder;

	CString strFile;
	strFile.Format(_T("Focus"));

	int length = 1024;
	LPWSTR pwsz = strFile.GetBuffer(length);
	dlg.m_ofn.lpstrFile = pwsz;

	if( dlg.DoModal() == IDOK )
	{
		
		//AfxMessageBox(_T("OnSetFocusSave"));
		strPathName = dlg.GetPathName();

		if (m_pPropertyCtrl->GetItem()->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		{
			// GH5 focus value save
			m_pPropertyCtrl->SetSaveFocus(strPathName);
		}
		else
		{

			CString strFilePath, strFolder;
			CString strSetupForder, strFocusFile;

			strFolder = dlg.GetPathName();

			strFolder.TrimRight(_T(".foc"));					//문자열 속 .mvtm 전체삭제
			strFilePath.Format(_T("%s.foc"), strFolder);

			strSetupForder.Format(_T("%s"), ESMGetPath(ESM_PATH_SETUP));
			strFocusFile = strSetupForder + _T("\\Focus.info");

			CopyFile(strFocusFile, strFilePath, FALSE);
		}
	}

	strFile.ReleaseBuffer();
	/*
	AfxMessageBox(_T("Movie Mode"));
	m_pPropertyCtrl->SetDSCModeMoviefile();
	*/
// 	CString strSettingForder;
// 
// 	strSettingForder.Format(_T("%s"), ESMGetPath(ESM_PATH_SETUP));
// 	CreateDirectory(strSettingForder, NULL);
// 	CString szFilter = _T("Setting File (*.info)|*.info|");
// 	CString strTitle = _T("Setting Files Save");
// 
// 	//-- 2014-07-14 hongsu@esmlab.com
// 	//-- Exception 
// #ifdef _DEBUG
// 	const BOOL bVistaStyle = FALSE;  
// #else
// 	const BOOL bVistaStyle = TRUE;  
// #endif	
// 	//-- File Dialog Open
// 	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
// 	strSettingForder.Replace(_T("\\\\"), _T("\\"));
// 	dlg.m_ofn.lpstrTitle = strTitle;
// 	dlg.m_ofn.lpstrInitialDir = strSettingForder;
// 
// 	CString strFile;
// 	strFile.Format(_T("Setting"));
// 
// 	int length = 1024;
// 	LPWSTR pwsz = strFile.GetBuffer(length);
// 	dlg.m_ofn.lpstrFile = pwsz;
// 
// 	if( dlg.DoModal() == IDOK )
// 	{
// 		AfxMessageBox(_T("OnSetFocusSave"));
// 	}
}

void CESMPropertyView::OnSetFocusLoad()
{
	if (m_pPropertyCtrl->GetItem() == NULL)
		return;

	m_pPropertyCtrl->GetPropertyAll(PTP_OC_SAMSUNG_GetFocusPosition);

	CString strPathName;
	CString strFocForder;

	strFocForder.Format(_T("%s\\foc"), ESMGetPath(ESM_PATH_SETUP));
	CreateDirectory(strFocForder, NULL);
	CString szFilter = _T("Focus File (*.foc)|*.foc|");
	CString strTitle = _T("Focus Files Save");

	//-- 2014-07-14 hongsu@esmlab.com
	//-- Exception 
#ifdef _DEBUG
	const BOOL bVistaStyle = FALSE;  
#else
	const BOOL bVistaStyle = TRUE;  
#endif	
	//-- File Dialog Open
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
	strFocForder.Replace(_T("\\\\"), _T("\\"));
	dlg.m_ofn.lpstrInitialDir = strFocForder;

	if( dlg.DoModal() == IDOK )
	{
		//AfxMessageBox(_T("OnSetFocusLoad"));

		strPathName = dlg.GetPathName();

		if (m_pPropertyCtrl->GetItem()->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
		{
			// GH5 focus value load						
			m_LoadFocusDlg->SetLoadFilePath(strPathName);
			m_LoadFocusDlg->DoModal();			
		}
		else
		{
			ESMEvent* pMsg;
			pMsg = new ESMEvent();
			//-- Release Focusing
			pMsg->message = WM_ESM_LIST_SETFOCUS_LOADFILE;
			CString* pStr = new CString;		
			pStr->Format(_T("%s"),strPathName);
			pMsg->pDest		= (LPARAM)pStr;
			::SendMessage(m_hMainWnd, WM_ESM, WM_ESM_LIST, (LPARAM)pMsg);
		}
	}	

	/*AfxMessageBox(_T("JPG Mode"));
	m_pPropertyCtrl->SetDSCModeJpgfile();*/
// 	CString strSettingForder;
// 
// 	strSettingForder.Format(_T("%s"), ESMGetPath(ESM_PATH_SETUP));
// 	CreateDirectory(strSettingForder, NULL);
// 	CString szFilter = _T("Setting Files (*.info)|*.info|");
// 	CString strTitle = _T("Setting Files Save");
// 
// 	//-- 2014-07-14 hongsu@esmlab.com
// 	//-- Exception 
// #ifdef _DEBUG
// 	const BOOL bVistaStyle = FALSE;  
// #else
// 	const BOOL bVistaStyle = TRUE;  
// #endif	
// 	//-- File Dialog Open
// 	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, NULL, 0, bVistaStyle );  
// 	strSettingForder.Replace(_T("\\\\"), _T("\\"));
// 	dlg.m_ofn.lpstrInitialDir = strSettingForder;
// 
// 	if( dlg.DoModal() == IDOK )
// 	{
// 		AfxMessageBox(_T("OnSetFocusLoad"));
// 		m_pPropertyCtrl->SetHiddenCommand(TRUE);
//	}
}

void CESMPropertyView::OnSetLcdOn()
{
	m_pPropertyCtrl->SetDSCLcdOn();
}

void CESMPropertyView::OnSetLcdOff()
{
	m_pPropertyCtrl->SetDSCLcdOff();
}

void CESMPropertyView::OnSetFocusZoom()
{
	m_pPropertyCtrl->SetDSCFocusZoom();
}

void CESMPropertyView::OnSetPropertySensorCleaning()
{
	m_pPropertyCtrl->SetSensorCleaning();
}

void CESMPropertyView::OnSetWhiteBalance()
{
	m_pPropertyCtrl->SetWhiteBalance();
}

void CESMPropertyView::OnSetPictureWizard()
{
	m_pPropertyCtrl->SetPictureWizard();
}

void CESMPropertyView::OnSetWhiteBalanceAll()
{
	m_pPropertyCtrl->SetWhiteBalanceAll();
}

void CESMPropertyView::OnSetISOUp()
{
	ESMLog(5, _T("OnSetISOUp"));
	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
		m_pPropertyCtrl->SetISOUp();
	else
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_NET_PROPERTY_1STEP;
		pMsg->nParam1 = ESM_PROPERTY_1SETP_ISO_UP;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}

}


void CESMPropertyView::OnSetISODown()
{
	ESMLog(5, _T("OnSetISODown"));
	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
		m_pPropertyCtrl->SetISODown();
	else
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_NET_PROPERTY_1STEP;
		pMsg->nParam1 = ESM_PROPERTY_1SETP_ISO_DOWN;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}
}


void CESMPropertyView::OnSetShuttersSpeedUp()
{
	ESMLog(5, _T("OnSetShuttersSpeedUp"));
	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
		m_pPropertyCtrl->SetSSpeedUp();
	else
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_NET_PROPERTY_1STEP;
		pMsg->nParam1 = ESM_PROPERTY_1SETP_SS_UP;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}
}


void CESMPropertyView::OnSetShuttersSpeedDown()
{
	ESMLog(5, _T("OnSetShuttersSpeedDown"));
	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
		m_pPropertyCtrl->SetSSpeedDown();
	else
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_NET_PROPERTY_1STEP;
		pMsg->nParam1 = ESM_PROPERTY_1SETP_SS_DOWN;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}
}

void CESMPropertyView::OnSetCTUp()
{
	ESMLog(5, _T("OnSetCTUp"));
	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
		m_pPropertyCtrl->SetCTUp();
	else
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_NET_PROPERTY_1STEP;
		pMsg->nParam1 = ESM_PROPERTY_1SETP_CT_UP;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}
}

void CESMPropertyView::OnSetCTDown()
{
	ESMLog(5, _T("OnSetCTDown"));
	if(ESMGetValue(ESM_VALUE_NET_MODE) == ESM_MODESTATE_CLIENT)
		m_pPropertyCtrl->SetCTDown();
	else
	{
		ESMEvent* pMsg = NULL;
		pMsg = new ESMEvent();
		pMsg->message = WM_ESM_NET_PROPERTY_1STEP;
		pMsg->nParam1 = ESM_PROPERTY_1SETP_CT_DOWN;
		::SendMessage(ESMGetMainWnd(), WM_ESM, (WPARAM)WM_ESM_NET, (LPARAM)pMsg);
	}
}


void CESMPropertyView::OnImageSetBanksave()
{
	m_pPropertyCtrl->SetHiddenCommand(TRUE);
}

afx_msg LRESULT CESMPropertyView::OnEsmLoadFocus(WPARAM wParam, LPARAM lParam)
{
	BOOL bFocusMove = (BOOL)wParam;		// TRUE: Focus Move, FALSE: Compare
	CString* strPath = (CString*)lParam;
	CString _strPath;
	_strPath.Format(_T("%s"),*strPath);

	m_pPropertyCtrl->SetLoadFocus(_strPath, bFocusMove);

	return 0;
}

afx_msg LRESULT CESMPropertyView::OnEsmLoadFocusEnd(WPARAM wParam, LPARAM lParam)
{
	if (m_LoadFocusDlg == NULL)
		return 0;
	
	m_LoadFocusDlg->SetEndFocusMove();

	return 0;
}

void CESMPropertyView::ResultFocusMoveCount()
{
	if (m_LoadFocusDlg == NULL)
		return;

	if (m_LoadFocusDlg->IsVisible() == FALSE)
		return;

	int nCnt = m_pPropertyCtrl->ResultFocusMoveCount();

	if (nCnt < 1)
	{
		m_LoadFocusDlg->SetEndFocusMove();
	}
}

void CESMPropertyView::SetFocusSave( CString str )
{
#if 1
	m_pPropertyCtrl->SetSaveFocus(str);
#else
	if (m_pPropertyCtrl->GetItem()->GetDeviceModel().CompareNoCase(_T("GH5")) == 0)
	{
		// GH5 focus value save
		m_pPropertyCtrl->SetSaveFocus(str);
	}
#endif
}

//System Frequency Change -> reboot 된다..
void CESMPropertyView::OnImageSetFrequency()
{
	CSysFrequency dlg;
	int result = dlg.DoModal(); 
	if(result == 0)			// NTSC
	{
		m_pPropertyCtrl->SetFrequency(FALSE);
	}
	else if(result == 1)	// PAL
	{
		m_pPropertyCtrl->SetFrequency(TRUE);
	}
}


void CESMPropertyView::OnImageSetLensPower()
{
	if (m_pPropertyCtrl->GetItem() == NULL)
		return;
	
	if (m_pPropertyCtrl->GetItem()->GetDeviceModel().CompareNoCase(_T("GH5")) != 0)
		return;
	
	// 버전체크
	int nVer = m_pPropertyCtrl->GetCamVersion();	
	if (nVer < 0x000000f4 || nVer == -1 || nVer == 0)
	{
		ESMLog(1, _T("[Lens Power Control] Please check Camera version."));
		return;
	}

	CLensPowerDlg dlg;
	int result = dlg.DoModal(); 
	if(result == 0)			// OFF
	{
		m_pPropertyCtrl->SetLensPower(TRUE);
	}
	else if(result == 1)	// ON
	{
		m_pPropertyCtrl->SetLensPower(FALSE);
	}
	else
	{
		return;
	}

	ESMReStartAgent();
}
