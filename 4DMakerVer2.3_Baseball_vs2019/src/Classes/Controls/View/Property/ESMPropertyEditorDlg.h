
#pragma once
#include "resource.h"
#include "ESMPropertyCtrl.h"

// CTimeLineObjectEditorDlg 대화 상자
class CESMPropertyEditorDlg : public CDialogEx
{
	// 생성입니다.
public:
	CESMPropertyEditorDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	CESMPropertyEditorDlg(CESMPropertyCtrl* pPropertyCtrl, CWnd* pParent = NULL);

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_VIEW_PROPERTY_EDITOR_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


	// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	CESMPropertyCtrl* m_pParentWnd;

	void SetObjectInfo();
	int	 GetTotalDSCIndex(const CString& strDSC, int nCtrlID);
	int	 GetStartTime();
	int	 GetEndTime();
	int	 CheckTime(const CString& strTime);
	int	 CheckTime(int nTime);
	int	 GetKzoneIndex();

public:
	int  GetStartDSCIndex();
	int  GetEndDSCIndex();
	int  GetStartNFT();

public:

	afx_msg void OnEnKillfocusEditStarttime();
	afx_msg void OnBnClickedOk();
	afx_msg void OnEnKillfocusEditEndtime();
};
