/////////////////////////////////////////////////////////////////////////////
//
//  RSComboList.h
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "resource.h"
#include "RSChildDialog.h"

enum RS_BAR_STATUS
{
	RS_BAR_IN_NOTHING = -1,
	RS_BAR_NOR	,
	RS_BAR_INSIDE,
	RS_BAR_DOWN,
	RS_BAR_DIS,
};

enum RS_BAR_POSITION
{
	RS_BAR_FISRT = 0,
	RS_BAR_MIDDLE,
	RS_BAR_END,	
	RS_BAR_SINGLE,
};


class CRSComboItem
{
public:
	CRSComboItem(CString strTitle) { m_strTitle = strTitle; m_nStatus = RS_BAR_NOR;  }
	CRect m_rect;
	int		m_nStatus;
	int		m_nPosition;
	CString m_strTitle;
};


// CRSComboList dialog
class CRSComboList : public CRSChildDialog
{
	DECLARE_DYNAMIC(CRSComboList)
public:
	CRSComboList(int nType, CWnd* pParent = NULL);   // standard constructor
	virtual ~CRSComboList();

public:
	void SetPosition(CPoint pt) { m_WindowPosition = pt; }
	BOOL ResizeWindow(int nType);
	void AddItem(CString strTitle);
	int   GetSize()	{ return m_arList.GetSize(); }
	void RemoveItem(CString strTitle);
	void RemoveAllItem();
	CString GetSelectName(int nIndex);
	void UpdateComboList(CStringArray* pArray);
	//-- 2011-7-9 Lee JungTaek
	void SetItem(int nIndex, CString strTitle);
	CString GetItem(int nIndex);
// Dialog Data
	enum { IDD = IDD_DLG_SMARTPANEL_CHILD};
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);		
	DECLARE_MESSAGE_MAP()

	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);	
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);

private:
	int		m_nType;
	int		m_nMouseIn;	
	CPoint m_WindowPosition;	
	CRSArray m_arList;	

	void DrawBackground();
	void DrawItem(CDC* pDC, CRSComboItem* pItem);
	int PtInRect(CPoint pt, BOOL bMove = TRUE);
	void ChangeStatus(int nItem, int nStatus);
	void ChangeStatusAll();
	void MouseEvent();	
};
