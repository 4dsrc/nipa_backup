/////////////////////////////////////////////////////////////////////////////
//
//  CRSSliderCtrl.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-08
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "RSSliderCtrl.h"
#include "SmartPanelIndex.h"
#include "SmartPanelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRSSliderCtrl::CRSSliderCtrl(void)
{
	m_nPos = 0;
	m_nEVType = SMARTTPANEL_PROP_EV_3STEP;
	m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_ev_3step_bg_user.png"));
	m_strSliderIndiPath = RSGetRoot(_T("img\\02.Smart Panel\\02_ev_indi.png"));

	m_nEVValue = 0;
	m_nEVIndex = 0;

	m_bEnable = FALSE;
	m_bSelected = FALSE;

	for(int i = 0; i < EV_POS_CNT; i++)
	{
		int nX = 10 + i * 6;

		m_rectIndi[i] = CRect(CPoint(nX, 5), CSize(6, 30));
	}
}

CRSSliderCtrl::~CRSSliderCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CRSSliderCtrl, CStatic)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


BOOL CRSSliderCtrl::OnEraseBkgnd(CDC* pDC) { return FALSE;}

void CRSSliderCtrl::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CBitmapSlider::OnPaint() for painting messages
	CPaintDC* pDC = &dc;

	Rect		DesRect;
	CRect		clRect;
	//NX1 dh0.seo 2014-08-14
	CString		strModel;
	strModel= ((CSmartPanelDlg*)GetParent())->GetTargetModel();
		
	//-- 2011-6-29 Lee JungTaek
	CleanDC(pDC);
	
	//-- Set Background Image
	switch(m_nEVType)
	{
	case SMARTTPANEL_PROP_EV_2STEP:
		{
			if(!strModel.Compare(DEFAULT_MODEL))
			{
				if(m_bEnable)
				{
					if(m_bSelected)
						m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\03_ev_3step_bg_sel_user.png"));
					else
						m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\03_ev_3step_bg_user_.png"));
				}				
				else
					m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\03_ev_3step_bg_user_dim.png"));
			}				
			else
			{
				if(m_bEnable)
				{
					if(m_bSelected)
						m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_ev_2step_bg_sel_user.png"));
					else
						m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_ev_2step_bg_user.png"));
				}				
				else
					m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_ev_2step_bg_dim_user.png"));
			}
			
		}		
		break;
	case SMARTTPANEL_PROP_EV_3STEP:
		{
			//if(strModel == _T("NX1"))
			if(!strModel.Compare(DEFAULT_MODEL))
			{
				if(m_bEnable)
				{
					if(m_bSelected)
						m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\03_ev_3step_bg_sel_user.png"));
					else
						m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\03_ev_3step_bg_user_.png"));
				}
				else
					m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\03_ev_3step_bg_user_dim.png"));
			}
			else
			{
				if(m_bEnable)
				{
					if(m_bSelected)
						m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_ev_3step_bg_sel_user.png"));
					else
						m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_ev_3step_bg_user.png"));
				}
				else
					m_strSliderBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_ev_3step_bg_dim_user.png"));
			}		
		}
		break;
	}

	//-- Draw Background Image
	Image ImgBk(m_strSliderBKPath);

	this->GetClientRect(&clRect);
	DesRect.X = clRect.left;
	DesRect.Y = clRect.top;
	DesRect.Width = clRect.right - clRect.left;
	DesRect.Height = clRect.bottom - clRect.top;

	Graphics gc(pDC->GetSafeHdc());
	gc.DrawImage(&ImgBk, DesRect);

	//-- Draw Indicator
	Image ImgIndi(m_strSliderIndiPath);

	this->GetClientRect(&clRect);
	DesRect.X = (clRect.left + clRect.right)/2 - ImgIndi.GetWidth()/2 + m_nPos;
	DesRect.Y = clRect.bottom - ImgIndi.GetHeight() - 5;
	DesRect.Width = ImgIndi.GetWidth();
	DesRect.Height = ImgIndi.GetHeight();

	gc.DrawImage(&ImgIndi, DesRect);
}

//------------------------------------------------------------------------------ 
//! @brief		CleanDC
//! @date		2011-6-29
//! @author		Lee JungTaek
//! @note	 	Clean DC before Draw
//------------------------------------------------------------------------------ 
void CRSSliderCtrl::CleanDC(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);
	CBrush brush = RGB_DEFAULT_DLG;
	pDC->FillRect(rect, &brush);
}

//------------------------------------------------------------------------------ 
//! @brief		SetPos
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Set EV Index
//------------------------------------------------------------------------------ 
#define EV_2STEP_MOVE_PIXEL 9
#define EV_2STEP_MOVE_INDEX 6
#define EV_3STEP_MOVE_PIXEL 6
#define EV_3STEP_MOVE_INDEX 9
#define EV_3STEP_MOVE_INDEX_ 15
#define EV_3STEP_MOVE_PIXEL_ 4.2

void CRSSliderCtrl::SetPos(BOOL bMouseWheelUp, BOOL bSet)
{
	//NX1 dh0.seo 2014-08-14
	CString		strModel;
	strModel= ((CSmartPanelDlg*)GetParent())->GetTargetModel();
	if(!strModel.Compare(DEFAULT_MODEL)){
		if(bMouseWheelUp)
		{
			if(m_nEVType == SMARTTPANEL_PROP_EV_2STEP)	{}
			else if(m_nEVType == SMARTTPANEL_PROP_EV_3STEP)
			{
				m_nEVIndex++;

				//-- Max Index
				if(m_nEVIndex > DSC_EV_PLUS_5_0)
					m_nEVIndex = DSC_EV_PLUS_5_0;
			}
		}
		else
		{
			if(m_nEVType == SMARTTPANEL_PROP_EV_2STEP)	{}
			else if(m_nEVType == SMARTTPANEL_PROP_EV_3STEP)
			{
				m_nEVIndex--;

				if(m_nEVIndex < DSC_EV_MINUS_5_0)		
					m_nEVIndex = DSC_EV_MINUS_5_0;
			}
		}
	}
	else
	{
		if(bMouseWheelUp)
		{
			if(m_nEVType == SMARTTPANEL_PROP_EV_2STEP)	{}
			else if(m_nEVType == SMARTTPANEL_PROP_EV_3STEP)
			{
				m_nEVIndex++;

				//-- Max Index
				if(m_nEVIndex > DSC_EV_PLUS_3_0)
					m_nEVIndex = DSC_EV_PLUS_3_0;
			}
		}
		else
		{
			if(m_nEVType == SMARTTPANEL_PROP_EV_2STEP)	{}
			else if(m_nEVType == SMARTTPANEL_PROP_EV_3STEP)
			{
				m_nEVIndex--;

				if(m_nEVIndex < DSC_EV_MINUS_3_0)		
					m_nEVIndex = DSC_EV_MINUS_3_0;
			}
		}
	}
	
	//-- SendDData
	//-- 2011-5-30 Lee JungTaek
	//-- Send Selected Data
	if(bSet)
		((CSmartPanelDlg*)GetParent())->SetPropertyValue(DSC_PASM_EV, m_nEVIndex);
}

void CRSSliderCtrl::SetEVValue(INT8 nEVValue)
{
	m_nEVValue = nEVValue;
	CString		strModel;
	strModel= ((CSmartPanelDlg*)GetParent())->GetTargetModel();
	
	switch(nEVValue)
	{
	case	eUCS_CAP_EVC_m_5_0	:	m_nEVIndex = DSC_EV_MINUS_5_0	;	break;
	case	eUCS_CAP_EVC_m_4_6	:	m_nEVIndex = DSC_EV_MINUS_4_6	;	break;
	case	eUCS_CAP_EVC_m_4_3	:	m_nEVIndex = DSC_EV_MINUS_4_3	;	break;
	case	eUCS_CAP_EVC_m_4_0	:	m_nEVIndex = DSC_EV_MINUS_4_0	;	break;
	case	eUCS_CAP_EVC_m_3_6	:	m_nEVIndex = DSC_EV_MINUS_3_6	;	break;
	case	eUCS_CAP_EVC_m_3_3	:	m_nEVIndex = DSC_EV_MINUS_3_3	;	break;
	case	eUCS_CAP_EVC_m_3_0	:	m_nEVIndex = DSC_EV_MINUS_3_0	;	break;
	case	eUCS_CAP_EVC_m_2_6	:	m_nEVIndex = DSC_EV_MINUS_2_6	;	break;
	case	eUCS_CAP_EVC_m_2_3	:	m_nEVIndex = DSC_EV_MINUS_2_3	;	break;
	case	eUCS_CAP_EVC_m_2_0	:	m_nEVIndex = DSC_EV_MINUS_2_0	;	break;
	case	eUCS_CAP_EVC_m_1_6	:	m_nEVIndex = DSC_EV_MINUS_1_6	;	break;
	case	eUCS_CAP_EVC_m_1_3	:	m_nEVIndex = DSC_EV_MINUS_1_3	;	break;
	case	eUCS_CAP_EVC_m_1_0	:	m_nEVIndex = DSC_EV_MINUS_1_0	;	break;
	case	eUCS_CAP_EVC_m_0_6	:	m_nEVIndex = DSC_EV_MINUS_0_6	;	break;
	case	eUCS_CAP_EVC_m_0_3	:	m_nEVIndex = DSC_EV_MINUS_0_3	;	break;
	case	eUCS_CAP_EVC_0_0	:	m_nEVIndex = DSC_EV_MINUS_0		;	break;
	case	eUCS_CAP_EVC_p_0_3	:	m_nEVIndex = DSC_EV_PLUS_0_3	;	break;
	case	eUCS_CAP_EVC_p_0_6	:	m_nEVIndex = DSC_EV_PLUS_0_6	;	break;
	case	eUCS_CAP_EVC_p_1_0	:	m_nEVIndex = DSC_EV_PLUS_1_0	;	break;
	case	eUCS_CAP_EVC_p_1_3	:	m_nEVIndex = DSC_EV_PLUS_1_3	;	break;
	case	eUCS_CAP_EVC_p_1_6	:	m_nEVIndex = DSC_EV_PLUS_1_6	;	break;
	case	eUCS_CAP_EVC_p_2_0	:	m_nEVIndex = DSC_EV_PLUS_2_0	;	break;
	case	eUCS_CAP_EVC_p_2_3	:	m_nEVIndex = DSC_EV_PLUS_2_3	;	break;
	case	eUCS_CAP_EVC_p_2_6	:	m_nEVIndex = DSC_EV_PLUS_2_6	;	break;
	case	eUCS_CAP_EVC_p_3_0	:	m_nEVIndex = DSC_EV_PLUS_3_0	;	break;
	case	eUCS_CAP_EVC_p_3_3	:	m_nEVIndex = DSC_EV_PLUS_3_3	;	break;
	case	eUCS_CAP_EVC_p_3_6	:	m_nEVIndex = DSC_EV_PLUS_3_6	;	break;
	case	eUCS_CAP_EVC_p_4_0	:	m_nEVIndex = DSC_EV_PLUS_4_0	;	break;
	case	eUCS_CAP_EVC_p_4_3	:	m_nEVIndex = DSC_EV_PLUS_4_3	;	break;
	case	eUCS_CAP_EVC_p_4_6	:	m_nEVIndex = DSC_EV_PLUS_4_6	;	break;
	case	eUCS_CAP_EVC_p_5_0	:	m_nEVIndex = DSC_EV_PLUS_5_0	;	break;
	}

	//NX1 dh0.seo 2014-08-14
	if(!strModel.Compare(DEFAULT_MODEL))
	{
		if(m_nEVIndex < DSC_EV_MINUS_0)
			m_nPos = -EV_3STEP_MOVE_PIXEL_ * EV_3STEP_MOVE_INDEX_ + (EV_3STEP_MOVE_PIXEL_ * ((m_nEVIndex)% 15));
		else if(m_nEVIndex == DSC_EV_PLUS_5_0)
			m_nPos = EV_3STEP_MOVE_PIXEL_ * EV_3STEP_MOVE_INDEX_;
		else if(m_nEVIndex > DSC_EV_MINUS_0)
			m_nPos = EV_3STEP_MOVE_PIXEL_ * (m_nEVIndex % 15);
		else
			m_nPos = 0;
	}
	else
	{
		if(m_nEVIndex < DSC_EV_MINUS_0)
			m_nPos = -EV_3STEP_MOVE_PIXEL * EV_3STEP_MOVE_INDEX + (EV_3STEP_MOVE_PIXEL * ((m_nEVIndex)% 9));
		else if(m_nEVIndex == DSC_EV_PLUS_3_0)
			m_nPos = EV_3STEP_MOVE_PIXEL * EV_3STEP_MOVE_INDEX;
		else if(m_nEVIndex > DSC_EV_MINUS_0)
			m_nPos = EV_3STEP_MOVE_PIXEL * (m_nEVIndex % 9);
		else
			m_nPos = 0;
	}

	Invalidate(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	WM Message
//------------------------------------------------------------------------------ 
BOOL CRSSliderCtrl::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_RBUTTONDOWN)
	{
		//-- 2011-6-23 Lee JungTaek
		if(m_bEnable)
		{
			if(!m_bSelected)
			{
				GetParent()->PreTranslateMessage(pMsg);

				if(!((CSmartPanelDlg*)GetParent())->IsCapturing())
					SetSliderSelected(TRUE);
			}
			else
			{
				//-- Click Event
				CPoint point;
				GetCursorPos(&point);
				ScreenToClient(&point);
				SetPropValueIndex(PtInRectValue(point));
			}
		}	
		
		return TRUE;
	}

	return CStatic::PreTranslateMessage(pMsg);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRectValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CRSSliderCtrl::PtInRectValue(CPoint point)
{
//	int nCurIndex = m_nEVIndex;

	int i = 0;

	if(point.x >= 4 && point.x <= 132)
	{
		i = (point.x - 4)/4;
		return i;
	}

/*	for(int i = 0 ; i < EV_POS_CNT; i ++)
	{
		if(m_rectIndi[i].PtInRect(point))
		{
//			if(i != nCurIndex)
				return i;
		}			
	} */

	return EV_POS_NULL;
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropValueIndex
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSSliderCtrl::SetPropValueIndex(int nIndex)
{
	if(nIndex == EV_POS_NULL)		return;

	int nCurIndex = m_nEVIndex;

	if(nIndex > nCurIndex)
	{
		//-- Just Change
		for(int i = nCurIndex; i < nIndex - 1; i++)
			this->SetPos(TRUE, FALSE);
		//-- Set Real
		this->SetPos(TRUE);
	}
	else
	{
		//-- Just Change
		for(int i = nCurIndex; i > nIndex + 1; i--)
			this->SetPos(FALSE, FALSE);
		//-- Set Real
		this->SetPos(FALSE);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		GetPropDesc
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	Get PropDesc
//------------------------------------------------------------------------------ 
CString CRSSliderCtrl::GetPropDesc()
{
	CString strPropDesc = _T("");
	strPropDesc.Format(_T("%s : %.1f"), m_strPropName, (float)m_nEVValue / 10);

	return  strPropDesc;
}