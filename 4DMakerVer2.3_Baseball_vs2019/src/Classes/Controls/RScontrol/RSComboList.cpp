/////////////////////////////////////////////////////////////////////////////
//
//  RSComboList.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-18
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NXRemoteStudioDlg.h"
#include "RSComboList.h"
#include "SRSIndex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const int size_item_width	= 100;
static const int size_item_height	= 27;
static const int size_item_top_margin	= 3;
static const int size_item_width_margin	= 5;


// CRSComboList dialog
IMPLEMENT_DYNAMIC(CRSComboList, CRSChildDialog)

CRSComboList::CRSComboList(int nType, CWnd* pParent /*=NULL*/)
	: CRSChildDialog(CRSComboList::IDD, pParent)
{
	m_nMouseIn = -1;
	m_nType = nType;	// CMB_MULTI	/	CMB_ZOOM	/	CMB_GRID
}

CRSComboList::~CRSComboList()
{
	RemoveAllItem();
}

void CRSComboList::DoDataExchange(CDataExchange* pDX)
{
	CRSChildDialog::DoDataExchange(pDX);	
}

BEGIN_MESSAGE_MAP(CRSComboList, CRSChildDialog)	
	ON_WM_SIZE()	
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

void CRSComboList::OnPaint()
{	
	DrawBackground();
	CRSChildDialog::OnPaint();
}

BOOL CRSComboList::OnInitDialog()
{
	CRSChildDialog::OnInitDialog();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CRSComboList::OnSize(UINT nType, int cx, int cy) 
{
    CDialog::OnSize(nType, cx, cy);
	DrawBackground();
} 

void CRSComboList::AddItem(CString strTitle) 
{
	CRSComboItem* pItem = new CRSComboItem(strTitle);
	m_arList.Add((CObArray*)pItem);
} 

CString CRSComboList::GetItem(int nIndex)
{
	CRSComboItem* pItem = NULL;	
	pItem = (CRSComboItem*)m_arList.GetAt(nIndex);
	if(pItem)
		return pItem->m_strTitle;
	return _T("");
}

void CRSComboList::SetItem(int nIndex, CString strTitle) 
{
	CRSComboItem* pItem = NULL;	
	pItem = (CRSComboItem*)m_arList.GetAt(nIndex);
	pItem->m_strTitle = strTitle;
}

void CRSComboList::RemoveItem(CString strTitle)
{
	CRSComboItem* pItem = NULL;
	int nAll = m_arList.GetSize();
	while(nAll--)
	{
		pItem = (CRSComboItem*)m_arList.GetAt(nAll);
		if(pItem->m_strTitle == strTitle)
		{
			m_arList.RemoveAt(nAll);
			delete pItem;		
			return;
		}
	}	
}

CString CRSComboList::GetSelectName(int nIndex)
{
	CRSComboItem* pItem = NULL;
	int nAll = m_arList.GetSize();
	if( nIndex >= 0 && nAll > nIndex)
	{
		pItem = (CRSComboItem*)m_arList.GetAt(nIndex);
		return pItem->m_strTitle;
	}
	return _T("");
}

void CRSComboList::RemoveAllItem() 
{
	CRSComboItem* pItem = NULL;
	int nAll = m_arList.GetSize();
	while(nAll--)
	{
		pItem = (CRSComboItem*)m_arList.GetAt(nAll);
		m_arList.RemoveAt(nAll);
		delete pItem;		
	}
	m_arList.RemoveAll();
} 

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CRSComboList::DrawBackground()
{
	if(!ISSet())
		return;

	//-- 2011-8-18 Lee JungTaek
	//-- Modify
	int n, nAll = m_arList.GetSize();
	if(nAll < 1)
		return;

	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);	
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);


	DrawRoundDialogs(&mDC, TRUE);	
	CRSComboItem* pItem = NULL;
	CSize szStr;
	int nImgX		= 0;
	int nImgY		= 0;
	int nImgW	= size_item_width;
	int nImgH	= size_item_top_margin;
	//-- Get Fit Size
	for(n = 0; n < nAll ; n ++)
	{
		pItem = (CRSComboItem*)m_arList.GetAt(n);
		CSize szStr = mDC.GetOutputTextExtent(pItem->m_strTitle);
		if(nImgW < szStr.cx)
			nImgW = szStr.cx + 2 * size_item_width_margin;
	}
	
	if(nAll == 1)
	{
		pItem = (CRSComboItem*)m_arList.GetAt(0);

		pItem->m_rect = CRect(CPoint(0,0), CSize(nImgW, size_item_height + size_item_top_margin));
		pItem-> m_nPosition = RS_BAR_SINGLE;

		DrawItem(&mDC, pItem);
	}
	else
	{
		//-- More 2
		//-- Draw List	
		for(n = 0; n < nAll ; n ++)
		{
			pItem = (CRSComboItem*)m_arList.GetAt(n);
			//-- Set Position
			if(n == 0) {
				pItem->m_rect = CRect(CPoint(nImgX, n*size_item_height), CSize(nImgW, size_item_height + size_item_top_margin));
				pItem-> m_nPosition = RS_BAR_FISRT;
			}
			else if(n == nAll -1) {
				pItem->m_rect = CRect(CPoint(nImgX, n*size_item_height), CSize(nImgW, size_item_height + size_item_top_margin + 3));
				pItem-> m_nPosition = RS_BAR_END;
			}
			else {
				pItem->m_rect = CRect(CPoint(nImgX, n*size_item_height), CSize(nImgW, size_item_height ));
				pItem-> m_nPosition = RS_BAR_MIDDLE;
			}
			DrawItem(&mDC, pItem);
		}
	}

	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);	
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @modify		2011-8-18 Lee JungTaek
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
BOOL CRSComboList::ResizeWindow(int nType)
{
	int nAll = m_arList.GetSize();
	if(nAll < 2)
	{
		switch(nType)
		{
		case CMB_MULTI	:	
		case CMB_ZOOM	:	
		case CMB_GRID	:	
			{
				CheckWindowRect(m_WindowPosition.x, m_WindowPosition.y, 0, 0);
				return FALSE;
			}
		case CMB_BANK	:	break;
		default:			return FALSE;
		}		
	}
		
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);	
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);

	DrawRoundDialogs(&mDC, TRUE);

	CRSComboItem* pItem = NULL;
	CSize szStr;
	int nImgX		= 0;
	int nImgY		= 0;
	int nImgW	= size_item_width;
	int nImgH	= size_item_width_margin;
	
	//-- Get Size
	for(int n = 0; n < nAll ; n ++)
	{
		pItem = (CRSComboItem*)m_arList.GetAt(n);
		CSize szStr = mDC.GetOutputTextExtent(pItem->m_strTitle);
		if(nImgW < szStr.cx)
			nImgW = szStr.cx + 2 * size_item_width_margin ;		
	}
	nImgH = nAll*size_item_height + 2*size_item_top_margin;
	//-- Draw Background	
	CheckWindowRect(m_WindowPosition.x, m_WindowPosition.y, nImgW, nImgH);		
	//-- 2011-08-02 hongsu.jung
	// Double Buffering
	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);	
	return TRUE;
}

//------------------------------------------------------------------------------ 
//! @brief		DrawBackground
//! @date		2010-05-30
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CRSComboList::DrawItem(CDC* pDC, CRSComboItem* pItem)
{
	//-- Draw BackGround
	switch(pItem->m_nPosition)
	{
	case RS_BAR_FISRT:		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_cameraselect_popup_bg_top.png")),pItem->m_rect);  break;
	case RS_BAR_MIDDLE:	DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_cameraselect_popup_bg_center.png")),pItem->m_rect);  break;
	case RS_BAR_END:		DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_cameraselect_popup_bg_bottom.png")),pItem->m_rect);  break;
	case RS_BAR_SINGLE: DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_cameraselect_popup_bg.png")),pItem->m_rect);  break;
	}

	//-- Draw Selected BackGround
	if(pItem->m_nStatus == RS_BAR_INSIDE)	
	{
		CRect rect;
		switch(pItem->m_nPosition)
		{
		case RS_BAR_FISRT:		
			rect.CopyRect(pItem->m_rect);
			rect.top += 3;
			rect.left += 3;
			rect.right -= 3;
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_cameraselect_popup_focus_top.png")),rect);			
			break;
		case RS_BAR_MIDDLE:	
			rect.CopyRect(pItem->m_rect);
			rect.left += 3;
			rect.right -= 3;			
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_cameraselect_popup_focus_center.png")),rect);  break;			
		case RS_BAR_END:		
			rect.CopyRect(pItem->m_rect);
			rect.left += 3;
			rect.right -= 3;			
			rect.bottom -= 4;
			DrawGraphicsImage(pDC, RSGetRoot(_T("img\\01.Light Studio\\01_cameraselect_popup_focus_bottom.png")),rect);  break;
			
		}
	}	

	//-- Draw String
	switch(pItem->m_nPosition)
	{
	case RS_BAR_FISRT:		DrawStr(pDC, STR_SIZE_MIDDLE, pItem->m_strTitle, pItem->m_rect.left + 10, pItem->m_rect.top + 11, RGB(255,255,255)); break;
	case RS_BAR_MIDDLE:		DrawStr(pDC, STR_SIZE_MIDDLE, pItem->m_strTitle, pItem->m_rect.left + 10, pItem->m_rect.top + 8, RGB(255,255,255)); break;
	case RS_BAR_END:		DrawStr(pDC, STR_SIZE_MIDDLE, pItem->m_strTitle, pItem->m_rect.left + 10, pItem->m_rect.top + 8, RGB(255,255,255)); break;
	case RS_BAR_SINGLE:		DrawStr(pDC, STR_SIZE_MIDDLE, pItem->m_strTitle, pItem->m_rect.left + 10, pItem->m_rect.top + 11, RGB(255,255,255)); break;
	}
	
}

//------------------------------------------------------------------------------ 
//! @brief		OnMouseMove
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CRSComboList::OnMouseMove(UINT nFlags, CPoint point)
{
	//-- Set MouseMove
	m_nMouseIn = PtInRect(point, TRUE);
	//-- MOUSE LEAVE
	MouseEvent();
	CDialog::OnMouseMove(nFlags, point);
}

void CRSComboList::MouseEvent()
{
	TRACKMOUSEEVENT tme;
	tme.cbSize = sizeof(tme);
	tme.hwndTrack = m_hWnd;
	tme.dwFlags = TME_LEAVE;
	tme.dwHoverTime = 1;
	TrackMouseEvent(&tme);
}

LRESULT CRSComboList::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{   
	for(int i = 0; i < BTN_BAR_CNT; i ++)
		ChangeStatus(i,RS_BAR_NOR);
	return 0L;
}   

//------------------------------------------------------------------------------ 
//! @brief		OnLButtonDown
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CRSComboList::OnLButtonDown(UINT nFlags, CPoint point)
{
	int nItem = PtInRect(point, FALSE);
	if(nItem == RS_BAR_IN_NOTHING)
		return;
	//-- Set Changed Value
	switch(m_nType)
	{
	case CMB_MULTI:
		((CNXRemoteStudioDlg*)GetParent())->ChangeSelectItem(nItem);
		break;
	case CMB_ZOOM:
		{
			RSEvent* pMsg		= new RSEvent();
			pMsg->message	= WM_LIVEVIEW_ZOOM_WINDOW;
			pMsg->nParam1		= nItem;
			((CNXRemoteStudioDlg*)GetParent())->m_pDlgLiveview->SendMessage(WM_LIVEVIEW,(WPARAM)pMsg,NULL);	
		}
		break;
	case CMB_GRID:
		{
			RSEvent* pMsg		= new RSEvent();
			pMsg->message	= WM_LIVEVIEW_GRID_CHANGE;	
			pMsg->nParam1		= nItem;
			((CNXRemoteStudioDlg*)GetParent())->m_pDlgLiveview->SendMessage(WM_LIVEVIEW,(WPARAM)pMsg,NULL);				
		}
		break;
	//-- 2011-7-14 Lee JungTaek
	case CMB_BANK:
		{
			RSEvent* pMsg		= new RSEvent();
			pMsg->message	= WM_RS_LOAD_BANK;	
			pMsg->nParam1		= nItem;
			::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)pMsg->message, (LPARAM)pMsg);
		}
		break;
	}
	//-- Hide Windows
	ShowWindow(SW_HIDE);
	CDialog::OnLButtonDown(nFlags, point);	
}
//------------------------------------------------------------------------------ 
//! @brief		OnLButtonUp
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Create Button
//------------------------------------------------------------------------------ 
void CRSComboList::OnLButtonUp(UINT nFlags, CPoint point)
{
	ChangeStatusAll();	
	CDialog::OnLButtonUp(nFlags, point);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRect
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
int CRSComboList::PtInRect(CPoint pt, BOOL bMove)
{
	int nReturn = RS_BAR_IN_NOTHING;	
	CRSComboItem* pItem = NULL;
	int nAll = m_arList.GetSize();
	//-- Get Size
	while(nAll--)
	{
		pItem = (CRSComboItem*)m_arList.GetAt(nAll);	
		if(pItem->m_rect.PtInRect(pt))
		{
			nReturn = nAll;
			if(bMove)
				ChangeStatus(nAll,RS_BAR_INSIDE);
			else
				ChangeStatus(nAll,RS_BAR_DOWN); 
		}
		else
			ChangeStatus(nAll,RS_BAR_NOR); 
	}
	return nReturn;
}
//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CRSComboList::ChangeStatus(int nItem, int nStatus)
{
	CRSComboItem* pItem = (CRSComboItem*)m_arList.GetAt(nItem);	
	if(!pItem)
		return;
	if(pItem->m_nStatus != nStatus)
	{
		pItem->m_nStatus = nStatus;
		CPaintDC dc(this);
		DrawItem(&dc, pItem);
		InvalidateRect(pItem->m_rect,FALSE);
	}
	UpdateWindow();
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CRSComboList::ChangeStatusAll()
{
	CRSComboItem* pItem = NULL;
	int nAll = m_arList.GetSize();
	//-- Get Size
	while(nAll--)
	{
		pItem = (CRSComboItem*)m_arList.GetAt(nAll);	
		if(m_nMouseIn == nAll)
			ChangeStatus(nAll,RS_BAR_INSIDE); 
		else
			ChangeStatus(nAll,RS_BAR_NOR);
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStatus
//! @date		2011-06-08
//! @author	hongsu.jung
//! @note	 	Check Btu in Point
//------------------------------------------------------------------------------ 
void CRSComboList::UpdateComboList(CStringArray* pArray)
{
	//-- Remove All
	RemoveAllItem();
	//-- Create All
	CString strItem;
	int nAll = pArray->GetSize();
	for(int i = 0 ; i < nAll ; i++)
	{
		strItem = pArray->GetAt(i);
		AddItem(strItem);
	}
	/*
	CString strItem;
	CRSComboItem* pExist = NULL;
	
	BOOL bExist;
	int i,j;
	int nAll = pArray->GetSize();
	int nExistAll = m_arList.GetSize();
	
	//--
	//-- Add New ComboBox
	for(i = 0 ; i < nAll ; i++)
	{
		strItem = pArray->GetAt(i);
		bExist = FALSE;
		//-- Check Exist
		for(j = 0; j < nExistAll ; j++)
		{
			pExist = (CRSComboItem*)m_arList.GetAt(j);
			if(strItem == pExist->m_strTitle)
			{
				bExist = TRUE;
				break;			
			}
		}
		//-- Add New Item
		if(!bExist)
			AddItem(strItem);
	}
	//--
	//-- Delete Removed Item
	for(j = 0 ; j < nExistAll; j++)
	{
		pExist = (CRSComboItem*)m_arList.GetAt(j);
		if(pExist)
		{
			//-- Check Exist
			bExist = FALSE;
			for(i = 0; i < nAll ; i++)
			{
				strItem = pArray->GetAt(i);
				if(pExist->m_strTitle == strItem)
				{
					bExist = TRUE;
					break;
				}
			}		
			//-- Not Exist -> Removed
			if(!bExist)
			{
				m_arList.RemoveAt(j);
				delete pExist;	
			}
		}
	}
	*/
}