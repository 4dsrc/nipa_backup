/////////////////////////////////////////////////////////////////////////////
//
//  ThumbNailDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "afxwin.h"
#include "BtnST.h"

class CRSButton :public CButtonST
{
public:
	CRSButton(int nType);
	~CRSButton(void);
public:
	void SetBitmaps(CString strImagePath);

	//-- 2011-5-19 Lee JungTaek
	//-- Change Image
	void SetBtnStatus(int nBtnStaus, int nPosType = -1) {m_nBtnStaus = nBtnStaus; Invalidate(FALSE); m_nBtnPosType = nPosType;	}
	void SetBtnText(CString strText) {m_strText = strText;}
	
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()

private:
	void CleanDC(CDC* pDC);

	void Set2ndBKImagePath(int nBtnStatus);

public:
	int		m_nPropertyValue;
	CString m_strPropValue;
	CString m_strPropertyName;
	CString m_strImagePath;
	CString m_strBKImagePath;
		
	//-- 2011-5-18 Lee JungTaek
	//-- Background Image
	int		m_nBtnStaus;
	int		m_nBtnPosType;
	CString m_strText;
	int		m_nBtnType;
};
