/////////////////////////////////////////////////////////////////////////////
//
//  CRSSliderCtrl.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-08
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

enum
{
	EV_POS_NULL			= -1,
	EV_POS_MINUS_5_0,
	EV_POS_MINUS_4_6,
	EV_POS_MINUS_4_3,
	EV_POS_MINUS_4_0,
	EV_POS_MINUS_3_6,
	EV_POS_MINUS_3_3,
	EV_POS_MINUS_3_0,
	EV_POS_MINUS_2_6,
	EV_POS_MINUS_2_3,
	EV_POS_MINUS_2_0,
	EV_POS_MINUS_1_6,
	EV_POS_MINUS_1_3,
	EV_POS_MINUS_1_0,
	EV_POS_MINUS_0_6,
	EV_POS_MINUS_0_3,
	EV_POS_MINUS_0	,
	EV_POS_PLUS_0_3	,
	EV_POS_PLUS_0_6	,
	EV_POS_PLUS_1_0	,
	EV_POS_PLUS_1_3	,
	EV_POS_PLUS_1_6	,
	EV_POS_PLUS_2_0	,
	EV_POS_PLUS_2_3	,
	EV_POS_PLUS_2_6	,
	EV_POS_PLUS_3_0	,	
	EV_POS_PLUS_3_3	,
	EV_POS_PLUS_3_6	,
	EV_POS_PLUS_4_0	,
	EV_POS_PLUS_4_3	,
	EV_POS_PLUS_4_6	,
	EV_POS_PLUS_5_0	,
	EV_POS_CNT,
};

class CRSSliderCtrl: public CStatic
{

public:
	CRSSliderCtrl(void);
	~CRSSliderCtrl(void);

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	DECLARE_MESSAGE_MAP()

public:
	int m_nEVType;
	int	m_nEVValue;
	int m_nEVIndex;
	int m_nPos;

private:
	int m_nPropIndex;
	CString m_strPropName;
	//-- 2011-6-23 Lee JungTaek
	BOOL m_bEnable;
	BOOL m_bSelected;

	CString m_strSliderBKPath;
	CString m_strSliderIndiPath;	
	
	//-- 2011-9-16 Lee JungTaek
	CRect m_rectIndi[EV_POS_CNT];

private:
	void CleanDC(CDC* pDC);
	int PtInRectValue(CPoint point);
	void SetPropValueIndex(int nIndex);

public:
	void SetPos(BOOL bMouseWheelUp, BOOL bSet = TRUE);
	void SetEVValue(INT8 nEVValue);
	void SetEnable(BOOL bEnable)	{m_bEnable = bEnable;}
	void SetSliderSelected(BOOL bSelected) {m_bSelected = bSelected; Invalidate(FALSE);}
	void SetPropInfo(int nPropIndex, CString strPropName) {m_nPropIndex = nPropIndex;	m_strPropName = strPropName;}
	CString GetPropDesc();
};
