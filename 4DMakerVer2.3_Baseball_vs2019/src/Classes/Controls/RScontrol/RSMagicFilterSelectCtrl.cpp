/////////////////////////////////////////////////////////////////////////////
//
//  CRSMagicFilterSelectCtrl.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-08
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "RSMagicFilterSelectCtrl.h"
#include "SmartPanelIndex.h"
#include "SmartPanelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRSMagicFilterSelectCtrl::CRSMagicFilterSelectCtrl(void)
{
	m_strBarometerBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_bg_magic_01_user.png"));

	m_nPropValueIndex = 0;
	m_nPropIndex = 0;

	m_strModePath1 = _T("");
	m_strModePath2 = _T("");
	m_strModePath3 = _T("");

	m_bSelected = TRUE;
	
	//-- 2011-6-21 Lee JungTaek
	m_strPropName = _T("");

	//-- 2011-7-14 Lee JungTaek
	m_rectValue[MODE_POS_FIRST] = CRect(CPoint(60, 4),CSize(56,35));
	m_rectValue[MODE_POS_SECOND]= CRect(CPoint(116,4),CSize(56,35));
	m_rectValue[MODE_POS_THIRD] = CRect(CPoint(172,4),CSize(56,35));
}

CRSMagicFilterSelectCtrl::~CRSMagicFilterSelectCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CRSMagicFilterSelectCtrl, CStatic)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

BOOL CRSMagicFilterSelectCtrl::OnEraseBkgnd(CDC* pDC) { return FALSE;}

void CRSMagicFilterSelectCtrl::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CPaintDC* pDC = &dc;
	
	//-- 2011-6-29 Lee JungTaek
	//-- Clean DC
	CleanDC(pDC);
	//-- Draw Image
	DrawCtrlBKImage(pDC);
	//-- Draw Text
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		
	case DSC_MODE_MAGIC_SMART_FILTER	:		
		switch(m_nPropIndex)
		{
		case DSC_MAGIC_FRAME_MAGICMODECHANGE:		DrawCtrlValue(pDC);	break;
		}
		break;
	default:		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		CleanDC
//! @date		2011-6-29
//! @author		Lee JungTaek
//! @note	 	Clean DC before Draw
//------------------------------------------------------------------------------ 
void CRSMagicFilterSelectCtrl::CleanDC(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);
	CBrush brush = RGB_DEFAULT_DLG;
	pDC->FillRect(rect, &brush);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawCtrlBKImage
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Draw Image
//------------------------------------------------------------------------------ 
void CRSMagicFilterSelectCtrl::DrawCtrlBKImage(CDC* pDC)
{
	Rect		DesRect;
	CRect		clRect;

	Graphics gc(pDC->GetSafeHdc());

	//-- Draw Background Image
	if(m_bSelected)
		m_strBarometerBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_bg_magic_01_f_user.png"));
	else
		m_strBarometerBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_bg_magic_01_user.png"));
	
	Image ImgBk(m_strBarometerBKPath);

	this->GetClientRect(&clRect);
	DesRect.X = clRect.left;
	DesRect.Y = clRect.top;
	DesRect.Width = clRect.right - clRect.left;
	DesRect.Height = clRect.bottom - clRect.top;

	gc.DrawImage(&ImgBk, DesRect);
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-13
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSMagicFilterSelectCtrl::DrawCtrlValue(CDC* pDC)
{
	//-- Set Alpha Color Matrix
	ColorMatrix matrix1 = {
		1,0,0,0,0,
		0,1,0,0,0,
		0,0,1,0,0,
		0,0,0,0.7f,0,
		0,0,0,0.0,1,
	};		
	ColorMatrix matrix2 = {
		1,0,0,0,0,
		0,1,0,0,0,
		0,0,1,0,0,
		0,0,0,0.2f,0,
		0,0,0,0.0,1,
	};

	ImageAttributes   IA1;   
	IA1.SetColorMatrix(&matrix1,ColorMatrixFlagsDefault,ColorAdjustTypeBitmap);

	ImageAttributes   IA2;   
	IA2.SetColorMatrix(&matrix2,ColorMatrixFlagsDefault,ColorAdjustTypeBitmap);

	//-- Set Rect
	Rect		DesRect;
	DesRect.X = 60;
	DesRect.Y = 8;
	DesRect.Width = 37;
	DesRect.Height = 31;

	Graphics gc(pDC->GetSafeHdc());

	//-- Value 1
	Image ImgValue1(m_strModePath1);
	gc.DrawImage(&ImgValue1, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);

	//-- Value 2
	Image ImgValue2(m_strModePath2);
	DesRect.X = 122;
	gc.DrawImage(&ImgValue2, DesRect);		

	//-- Value 3
	Image ImgValue3(m_strModePath3);
	DesRect.X = 184;	
	gc.DrawImage(&ImgValue3, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);	
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateBarometerCtrl
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Update Ctrl By Property
//------------------------------------------------------------------------------ 
void CRSMagicFilterSelectCtrl::UpdateMagicFilterCtrl()
{
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		
	case DSC_MODE_MAGIC_SMART_FILTER	:		
		switch(m_nPropIndex)
		{
		case DSC_MAGIC_FRAME_MAGICMODECHANGE:		UpdateMagicFilterValue();	break;
		}
		break;
	default:				break;
	}
	
	Invalidate(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateShutterSpeedValue
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Update ShtterSpeed Value
//------------------------------------------------------------------------------ 
void CRSMagicFilterSelectCtrl::UpdateMagicFilterValue()
{
	TRACE(_T("CRSMagicFilterSelectCtrl::m_nPropValueIndex == %d\n"), m_nPropValueIndex);

	if(m_nPropValueIndex - 1 < 0)
		m_strModePath1 = _T("");
	else
		m_strModePath1 = GetMagicFilterValue(m_nPropValueIndex - 1);
	
	m_strModePath2 = GetMagicFilterValue(m_nPropValueIndex);

	
	if(m_nPropValueIndex + 1 > ARRAYSIZE(SZNX200PropMagicModeValue) -1)
		m_strModePath3 = _T("");
	else
		m_strModePath3 = GetMagicFilterValue(m_nPropValueIndex + 1);
}

//------------------------------------------------------------------------------ 
//! @brief		SetPos
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Set Shutter Speed Value
//------------------------------------------------------------------------------ 
void CRSMagicFilterSelectCtrl::SetPos(BOOL bMouseWheelUp, BOOL bSet)
{
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		
	case DSC_MODE_MAGIC_SMART_FILTER	:		
		switch(m_nPropIndex)
		{
		case DSC_MAGIC_FRAME_MAGICMODECHANGE:		
			SetMagicFilterPos(bMouseWheelUp);	
			//-- 2011-9-16 Lee JungTaek
			if(bSet)
				((CSmartPanelDlg*)GetParent())->SetPropertyValue(m_nPropIndex, m_nPropValueIndex);
			break;
		}
		break;
	default:			break;
	}

	UpdateMagicFilterCtrl();
}

//------------------------------------------------------------------------------ 
//! @brief		SetShutterSpeedPos
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Set Shutter Speed Position
//------------------------------------------------------------------------------ 
void CRSMagicFilterSelectCtrl::SetMagicFilterPos(BOOL bMouseWheelUp)
{
	if(m_nPropValueIndex <= 0)
	{
		//-- First Value
		if(bMouseWheelUp)
			m_nPropValueIndex++;
		else
			m_nPropValueIndex = 0;
	}	
	else if(m_nPropValueIndex >= ARRAYSIZE(SZNX200PropMagicModeValue) - 1)
	{
		//-- Last Value
		if(bMouseWheelUp)
			m_nPropValueIndex = ARRAYSIZE(SZNX200PropMagicModeValue) - 1;
		else
			m_nPropValueIndex--;
	}
	else
	{
		//-- Between
		if(bMouseWheelUp)
			m_nPropValueIndex++;
		else
			m_nPropValueIndex--;
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	WM Message
//------------------------------------------------------------------------------ 
BOOL CRSMagicFilterSelectCtrl::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_LBUTTONDOWN || pMsg->message == WM_RBUTTONDBLCLK)
	{
		//-- 2011-6-23 Lee JungTaek
		if(!m_bSelected)
		{
			GetParent()->PreTranslateMessage(pMsg);

			//-- 2011-8-5 Lee JungTaek
			//-- Check Capturing State
			if(!((CSmartPanelDlg*)GetParent())->IsCapturing())
				SetMagicFilterSelected(TRUE);
		}
		else
		{
			GetParent()->PreTranslateMessage(pMsg);
			
			CPoint point;
			GetCursorPos(&point);
			ScreenToClient(&point);
			SetPropValueIndex(PtInRectValue(point));			
		}	
 
 		return TRUE;
	}

	return CStatic::PreTranslateMessage(pMsg);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRectValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CRSMagicFilterSelectCtrl::PtInRectValue(CPoint point)
{
	for(int i = 0 ; i < MAGIC_POS_CNT; i ++)
	{
		if(m_rectValue[i].PtInRect(point))
		{
			if(i != MAGIC_POS_SECOND)
				return i;
		}
	}

	return MAGIC_POS_NULL;
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropValueIndex
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSMagicFilterSelectCtrl::SetPropValueIndex(int nIndex)
{
	if(nIndex == MAGIC_POS_NULL)		return;

	if(nIndex > MAGIC_POS_SECOND)
	{
		if(GetPropIndexValue() + 1 == GetPropListCnt())
			return;

		//-- Just Change
		for(int i = MAGIC_POS_SECOND; i < nIndex - 1; i++)
			this->SetPos(TRUE, FALSE);
		//-- Set Real
		this->SetPos(TRUE);
	}
	else
	{
		if(GetPropIndexValue() == 0)
			return;

		//-- Just Change
		for(int i = MAGIC_POS_SECOND; i > nIndex + 1; i--)
			this->SetPos(FALSE, FALSE);
		//-- Set Real
		this->SetPos(FALSE);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropValue
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Change Property Value To Indi Index
//------------------------------------------------------------------------------ 
void CRSMagicFilterSelectCtrl::SetPropValue(int nPropValue)
{
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		
	case DSC_MODE_MAGIC_SMART_FILTER	:	
		switch(m_nPropIndex)
		{
		case DSC_MAGIC_FRAME_MAGICMODECHANGE:		SetMagicFilterValueIndex(nPropValue);			break;
		}
		break;
	default:								break;
	}
			
	UpdateMagicFilterCtrl();
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-23
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSMagicFilterSelectCtrl::SetMagicFilterValueIndex(int nPropValue)
{
	switch(nPropValue)
	{
	case eUCS_CAP_MAGIC_MODE_MAGIC_FRMAE	:	m_nPropValueIndex = DSC_MAGIC_MODE_MAGIC_FRAME		;	break;
	case eUCS_CAP_MAGIC_MODE_SMART_FILTER	:	m_nPropValueIndex = DSC_MAGIC_MODE_SMART_FITLER		;	break;
	}
}

CString CRSMagicFilterSelectCtrl::GetMagicFilterValue(int nIndex)
{
	CString strMagicModeImage = _T("");

	int nPropValue = SZNX200PropMagicModeValue[nIndex].nValue;
	strMagicModeImage = GetMagicFilterImage(nPropValue);

	return strMagicModeImage;
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString CRSMagicFilterSelectCtrl::GetMagicFilterImage(int nPropValue)
{
	CString strImage = _T("");

	switch(nPropValue)
	{
	case eUCS_CAP_MAGIC_MODE_MAGIC_FRMAE	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_magic_magicframe.png"));	break;
	case eUCS_CAP_MAGIC_MODE_SMART_FILTER	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_magic_smartfilter.png"));	break;
	}

	return strImage;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPropDesc
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	Get PropDesc
//------------------------------------------------------------------------------ 
CString CRSMagicFilterSelectCtrl::GetPropDesc()
{
 	CString strPropDesc = _T("");
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		
	case DSC_MODE_MAGIC_SMART_FILTER	:	
		switch(m_nPropIndex)
		{
		case DSC_MAGIC_FRAME_MAGICMODECHANGE:		
			{
				for(int i = 0; i < ARRAYSIZE(SZNX200PropMagicModeValue); i++)
				{
					if(m_nPropValueIndex == SZNX200PropMagicModeValue[i].nIndex)
					{
						strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX200PropMagicModeValue[i].strDesc);
						break;
					}
				}
			}
			break;
		}
		break;
	default:		break;
	}

	return  strPropDesc;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPropStrValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString CRSMagicFilterSelectCtrl::GetPropStrValue(int nIndex)
{
	switch(nIndex)
	{
	case MAGIC_POS_FIRST		:	return m_strModePath1;
	case MAGIC_POS_SECOND		:	return m_strModePath2;
	case MAGIC_POS_THIRD		:	return m_strModePath3;
	default:	return _T("");
	}	
}