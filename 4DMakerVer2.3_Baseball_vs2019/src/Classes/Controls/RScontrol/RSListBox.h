/////////////////////////////////////////////////////////////////////////////
//
//  ThumbNailDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#include "afxwin.h"
#include "ListBoxST.h"

class CRSListBox : public CListBoxST
{
public:
	CRSListBox(void);
	~CRSListBox(void);
	DECLARE_MESSAGE_MAP()
	afx_msg void OnLbnSelchange();
	afx_msg void OnLbnDblclk();
public:
	CUIntArray m_arrCheckIndex;
	int m_nMainCamera;
	int m_cursel;
public:
	void SelectAll();
	void CancelAll();
};
