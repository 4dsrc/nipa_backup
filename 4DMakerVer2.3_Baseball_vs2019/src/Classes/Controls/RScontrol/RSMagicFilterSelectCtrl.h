/////////////////////////////////////////////////////////////////////////////
//
//  CRSMagicFilterSelectCtrl.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-08
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

enum MAGIC_POS
{
	MAGIC_POS_NULL = -1,
	MAGIC_POS_FIRST,
	MAGIC_POS_SECOND,
	MAGIC_POS_THIRD,
	MAGIC_POS_CNT,
};

class CRSMagicFilterSelectCtrl: public CStatic
{
public:
	CRSMagicFilterSelectCtrl(void);
	~CRSMagicFilterSelectCtrl(void);

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	DECLARE_MESSAGE_MAP()

private:
	int m_nPropIndex;
	int m_nPropValueIndex;
	CString m_strPropName;

	//-- Image
	CString m_strBarometerBKPath;

	//-- 2011-6-8 Lee JungTaek
	//-- Value
	CString m_strModePath1;
	CString m_strModePath2;
	CString m_strModePath3;

	//-- 2011-9-22 Lee JungTaek
	CRect m_rectValue[MAGIC_POS_CNT];
	
	BOOL m_bSelected;

	void CleanDC(CDC* pDC);
	void DrawCtrlBKImage(CDC* pDC);
	void DrawCtrlValue(CDC* pDC);
	void UpdateMagicFilterValue();

	//-- 2011-9-16 Lee JungTaek
	//-- Click Event
	int PtInRectValue(CPoint point);
	void SetPropValueIndex(int nIndex);

public:
	void UpdateMagicFilterCtrl();
	void SetPropInfo(int nPropIndex, CString strPropName) {m_nPropIndex = nPropIndex;	m_strPropName = strPropName;}

	//-- 2011-6-21 Lee JungTaek
	//-- Set Indi Position
	void SetPos(BOOL bMouseWheelUp, BOOL bSet = TRUE);
	void SetMagicFilterPos(BOOL bMouseWheelUp);

	//-- 2011-6-21 Lee JungTaek
	void SetMagicFilterSelected(BOOL bSelected) {m_bSelected = bSelected; Invalidate(FALSE);}
	void SetPropValue(int nPropValue);
	void SetMagicFilterValueIndex(int nPropValue);
	
	CString GetMagicFilterValue(int nIndex);
	CString GetMagicFilterImage(int nPropValue);
	
	CString GetPropDesc();

	//-- 2011-9-22 Lee JungTaek
	int GetPropIndex()	{return m_nPropIndex;}
	int GetPropIndexValue()	{return m_nPropValueIndex;}
	CString GetPropName()	{return m_strPropName;}
	CString GetPropStrValue(int nIndex);
	int GetPropListCnt()	{return DSC_MAGIC_MODE_SMART_FITLER + 1;};
};
