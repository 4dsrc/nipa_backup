/////////////////////////////////////////////////////////////////////////////
//
//  ThumbNailDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RSListBox.h"
#include "LBEditorwnd.h"

CRSListBox::CRSListBox(void)
{
	m_nMainCamera = 0;
	m_cursel = -1;
}

CRSListBox::~CRSListBox(void)
{
}BEGIN_MESSAGE_MAP(CRSListBox, CListBoxST)
		ON_CONTROL_REFLECT(LBN_SELCHANGE, &CRSListBox::OnLbnSelchange)
		ON_CONTROL_REFLECT(LBN_DBLCLK, &CRSListBox::OnLbnDblclk)
		END_MESSAGE_MAP()

void CRSListBox::SelectAll()
{
	for(int i = 0; i < this->GetCount(); i++)
	{
		if(!IsItemEnabled(i) || i == m_nMainCamera)
			continue;
		else
		{
			SetImage(i, 4);
			TRACE(_T("Do Check"));
		}
	}
}

void CRSListBox::CancelAll()
{
	for(int i = 0; i < this->GetCount(); i++)
	{
		if(!IsItemEnabled(i) || i == m_nMainCamera)
			continue;
		else
		{
			SetImage(i, 2);
			TRACE(_T("Do UnCheck"));
		}
	}
}

void CRSListBox::OnLbnSelchange()
{
	// TODO: Add your control notification handler code here
	int nIndex = this->GetCurSel();

	if(!IsItemEnabled(nIndex) || nIndex == m_nMainCamera)
		return;

	int imageType;
	GetImage(nIndex, &imageType);

	if(imageType == 2 || imageType == 3)
	{
		SetImage(nIndex, 4);
		TRACE(_T("Do Check"));
	}
	else if(imageType == 4)
	{
		SetImage(nIndex, 2);
		TRACE(_T("Do UnCheck"));
	}	
}

void CRSListBox::OnLbnDblclk()
{
	// TODO: Add your control notification handler code here
	int cursel = GetCurSel();

// 	if(m_cursel == cursel)
// 	{
 		CLBEditorWnd* pEditor = new CLBEditorWnd(this) ;
 		pEditor->Edit(cursel);
// 	}
// 	else
// 		m_cursel = cursel ;
}
