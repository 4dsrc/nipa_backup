/////////////////////////////////////////////////////////////////////////////
//
//  CRSBarometerCtrl.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-08
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "RSBarometerCtrl.h"
#include "SmartPanelIndex.h"
#include "SmartPanelDlg.h"
#include "SmartPanelBarometerChildDlg.h"
#include "NXRemoteStudioFunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRSBarometerCtrl::CRSBarometerCtrl(void)
{
	m_strBarometerBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_shutter_bg.png"));

	m_nPropValueIndex = 0;
	m_nPropIndex = 0;

	m_strValue1 = _T("");
	m_strValue2 = _T("");
	m_strValue3 = _T("");
	m_strValue4 = _T("");
	m_strValue5 = _T("");

	m_bEnable = FALSE;
	m_bSelected = FALSE;

	//-- 2011-6-21 Lee JungTaek
	m_parFNumber = NULL;
	m_parISO = NULL;
	m_strPropName = _T("");

	//-- 2011-7-14 Lee JungTaek
	m_rectText[TEXT_POS_FIRST] = CRect(CPoint(5,18),CSize(26,30));
	m_rectText[TEXT_POS_SECOND] = CRect(CPoint(29,18),CSize(26,30));
	m_rectText[TEXT_POS_THIRD] = CRect(CPoint(53,14),CSize(35,30));
	m_rectText[TEXT_POS_FOURTH] = CRect(CPoint(95,18),CSize(26,30));
	m_rectText[TEXT_POS_FIFTH] = CRect(CPoint(117,18),CSize(26,30));
}

CRSBarometerCtrl::~CRSBarometerCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CRSBarometerCtrl, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()

void CRSBarometerCtrl::OnPaint()
{
	CPaintDC dc(this);

	CRect rect;
	GetClientRect(&rect);

	CDC mDC;
	CBitmap mBitmap;		
	mDC.CreateCompatibleDC(&dc);
	mBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height()); 
	mDC.SelectObject(&mBitmap);
	mDC.PatBlt(0,0,rect.Width(), rect.Height(), WHITENESS);
	
	//-- 2011-6-29 Lee JungTaek
	//-- Clean DC
	CleanDC(&mDC);
	//-- Draw Image
	DrawCtrlBKImage(&mDC);
	//-- Draw Text
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_SMART					:		
	case DSC_MODE_P						:		
	case DSC_MODE_A						:		
	case DSC_MODE_S						:		
	case DSC_MODE_M						:
	case DSC_MODE_BULB					: //dh0.seo 2015-01-12 위치 이동
		switch(m_nPropIndex)
		{
		case DSC_PASM_SHUTTERSPEED:		
		case DSC_PASM_APERTURE:			DrawCtrlText(&mDC);			break;
		case DSC_PASM_ISO:				DrawCtrlValue(&mDC);			break;
		}		
		break;
	case DSC_MODE_I					 	:		
	case DSC_MODE_CAPTURE_MOVIE		 	:		
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		
	case DSC_MODE_MAGIC_SMART_FILTER	:				break;
	case DSC_MODE_PANORAMA			 	:
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:		
	case DSC_MODE_SCENE_PORTRAIT		:		
	case DSC_MODE_SCENE_CHILDREN		:		
	case DSC_MODE_SCENE_SPORTS		 	:		
	case DSC_MODE_SCENE_CLOSE_UP		:		
	case DSC_MODE_SCENE_TEXT			:		
	case DSC_MODE_SCENE_SUNSET		 	:		
	case DSC_MODE_SCENE_DAWN			:		
	case DSC_MODE_SCENE_BACKLIGHT		:		
	case DSC_MODE_SCENE_FIREWORK		:		
	case DSC_MODE_SCENE_BEACH_SNOW	 	:		
	case DSC_MODE_SCENE_SOUND_PICTURE	:		
	case DSC_MODE_SCENE_3D			 	:
	case DSC_MODE_SCENE_BEST			:
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_ACTION			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
	case DSC_MODE_SCENE_CREATIVE		:
	case DSC_MODE_SCENE_PANORAMA		:
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_BEAUTY_SHUTTERSPEED:		
		case DSC_SCENE_BEAUTY_APERTURE:			DrawCtrlText(&mDC);			break;
		}
		break;
	case DSC_MODE_MOVIE				 	:		break;
	}

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &mDC, 0, 0, SRCCOPY);
}

//------------------------------------------------------------------------------ 
//! @brief		CleanDC
//! @date		2011-6-29
//! @author		Lee JungTaek
//! @note	 	Clean DC before Draw
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::CleanDC(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);
	CBrush brush = RGB_DEFAULT_DLG;
	pDC->FillRect(rect, &brush);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawCtrlBKImage
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Draw Image
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::DrawCtrlBKImage(CDC* pDC)
{
	Rect		DesRect;
	CRect		clRect;

	Graphics gc(pDC->GetSafeHdc());

	//-- Draw Background Image
	if(m_bEnable)
	{
		if(m_bSelected)
			m_strBarometerBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_shutter_bg_sel_user.png"));
		else
			m_strBarometerBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_shutter_bg_user.png"));
	}
	else
		m_strBarometerBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_shutter_bg_dim_user.png"));
	
	Image ImgBk(m_strBarometerBKPath);

	this->GetClientRect(&clRect);
	DesRect.X = clRect.left;
	DesRect.Y = clRect.top;
	DesRect.Width = clRect.right - clRect.left;
	DesRect.Height = clRect.bottom - clRect.top;

	gc.DrawImage(&ImgBk, DesRect);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawCtrlText
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Draw Text
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::DrawCtrlText(CDC* pDC)
{
	Graphics gc(pDC->GetSafeHdc());
	CString strFontName = _T("SS_SJ_DTV_uni_20090209");
	CSize szStr;
	int x = 0, y = 0;
	
	//-- 1st Text
	Gdiplus::Font F1(strFontName, 8,FontStyleRegular,UnitPixel);
	szStr = pDC->GetOutputTextExtent(m_strValue1);
	x = (m_rectText[TEXT_POS_FIRST].left + m_rectText[TEXT_POS_FIRST].right) / 2 - (szStr.cx / 2);
	y = m_rectText[TEXT_POS_FIRST].top;
	PointF P1(x,y);
	if(m_bEnable)
	{
		SolidBrush B1(Color(50, 255,255,255));
		gc.DrawString(m_strValue1,-1,&F1,P1,&B1);
	}
	else
	{
		SolidBrush B1(Color(125, 125,125,125));
		gc.DrawString(m_strValue1,-1,&F1,P1,&B1);
	}
	
	//-- 2nd Text
	Gdiplus::Font F2(strFontName ,8,FontStyleRegular,UnitPixel);
	szStr = pDC->GetOutputTextExtent(m_strValue2);
	x = (m_rectText[TEXT_POS_SECOND].left + m_rectText[TEXT_POS_SECOND].right) / 2 - (szStr.cx / 2);
	y = m_rectText[TEXT_POS_SECOND].top;
	PointF P2(x,y);
	if(m_bEnable)
	{
		SolidBrush B2(Color(180, 255,255,255));
		gc.DrawString(m_strValue2,-1,&F2,P2,&B2);
	}
	else
	{
		SolidBrush B2(Color(125, 125,125,125));
		gc.DrawString(m_strValue2,-1,&F2,P2,&B2);
	}
	
	//-- 3rd Text (Setting Value)
	Gdiplus::Font F3(strFontName,12,FontStyleRegular,UnitPixel);
	szStr = pDC->GetOutputTextExtent(m_strValue3);
	x = (m_rectText[TEXT_POS_THIRD].left + m_rectText[TEXT_POS_THIRD].right) /2 - (szStr.cx / 2);
	y = m_rectText[TEXT_POS_THIRD].top;
	PointF P3(x,y);
	if(m_bEnable)
	{
		SolidBrush B3(Color(255,255,255));
		gc.DrawString(m_strValue3,-1,&F3,P3,&B3);
	}
	else
	{
		SolidBrush B3(Color(125,164,164,164));
		gc.DrawString(m_strValue3,-1,&F3,P3,&B3);
	}

	//-- 4th Text
	Gdiplus::Font F4(strFontName,8,FontStyleRegular,UnitPixel);
	szStr = pDC->GetOutputTextExtent(m_strValue4);
	x = (m_rectText[TEXT_POS_FOURTH].left + m_rectText[TEXT_POS_FOURTH].right) / 2 - (szStr.cx / 2);
	y = m_rectText[TEXT_POS_FOURTH].top;
	PointF P4(x,y);
	if(m_bEnable)
	{
		SolidBrush B4(Color(180, 255,255,255));
		gc.DrawString(m_strValue4,-1,&F4,P4,&B4);
	}
	else
	{
		SolidBrush B4(Color(125, 125,125,125));
		gc.DrawString(m_strValue4,-1,&F4,P4,&B4);
	}

	//-- 5th Text
	Gdiplus::Font F5(strFontName,8,FontStyleRegular,UnitPixel);
	szStr = pDC->GetOutputTextExtent(m_strValue5);
	x = (m_rectText[TEXT_POS_FIFTH].left + m_rectText[TEXT_POS_FIFTH].right) / 2 - (szStr.cx / 2);
	y = m_rectText[TEXT_POS_FIFTH].top;
	PointF P5(x,y);
	if(m_bEnable)
	{
		SolidBrush B5(Color(50, 255,255,255));
		gc.DrawString(m_strValue5,-1,&F5,P5,&B5);	
	}
	else
	{
		SolidBrush B5(Color(125, 125,125,125));
		gc.DrawString(m_strValue5,-1,&F5,P5,&B5);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-13
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::DrawCtrlValue(CDC* pDC)
{
	//-- Set Alpha Color Matrix
	ColorMatrix matrix1 = {
		1,0,0,0,0,
		0,1,0,0,0,
		0,0,1,0,0,
		0,0,0,0.7f,0,
		0,0,0,0.0,1,
	};		
	ColorMatrix matrix2 = {
		1,0,0,0,0,
		0,1,0,0,0,
		0,0,1,0,0,
		0,0,0,0.2f,0,
		0,0,0,0.0,1,
	};

	ImageAttributes   IA1;   
	IA1.SetColorMatrix(&matrix1,ColorMatrixFlagsDefault,ColorAdjustTypeBitmap);

	ImageAttributes   IA2;   
	IA2.SetColorMatrix(&matrix2,ColorMatrixFlagsDefault,ColorAdjustTypeBitmap);

	//-- Set Rect
	Rect		DesRect;
	DesRect.X = 5;
	DesRect.Y = 12;
	DesRect.Width = 26;
	DesRect.Height = 20;

	Graphics gc(pDC->GetSafeHdc());

	//-- Value 1
	Image ImgValue1(m_strValue1);
	gc.DrawImage(&ImgValue1, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA2);

	//-- Value 2
	Image ImgValue2(m_strValue2);
	DesRect.X += 26;	
	gc.DrawImage(&ImgValue2, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);	

	//-- Value 3
	Image ImgValue3(m_strValue3);
	DesRect.X += 26;
	gc.DrawImage(&ImgValue3, DesRect);

	//-- Value 4
	Image ImgValue4(m_strValue4);
	DesRect.X += 26;
	gc.DrawImage(&ImgValue4, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);		

	//-- Value 5
	Image ImgValue5(m_strValue5);
	DesRect.X += 26;
	gc.DrawImage(&ImgValue5, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA2);	
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateBarometerCtrl
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Update Ctrl By Property
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::UpdateBarometerCtrl()
{
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_SMART					:		
	case DSC_MODE_P						:		
	case DSC_MODE_A						:		
	case DSC_MODE_S						:		
	case DSC_MODE_M						:
	case DSC_MODE_BULB					: //dh0.seo 2015-01-12 위치 이동
		switch(m_nPropIndex)
		{
		case DSC_PASM_SHUTTERSPEED:		UpdateShutterSpeedValue();	break;
		case DSC_PASM_APERTURE:			UpdateApertureValue();		break;
		case DSC_PASM_ISO:				UpdateISOValue();			break;
		}
		break;
	case DSC_MODE_I					 	:		
	case DSC_MODE_CAPTURE_MOVIE		 	:		
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		
	case DSC_MODE_MAGIC_SMART_FILTER	:		break;
	case DSC_MODE_PANORAMA			 	:
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:		
	case DSC_MODE_SCENE_PORTRAIT		:		
	case DSC_MODE_SCENE_CHILDREN		:		
	case DSC_MODE_SCENE_SPORTS		 	:		
	case DSC_MODE_SCENE_CLOSE_UP		:		
	case DSC_MODE_SCENE_TEXT			:		
	case DSC_MODE_SCENE_SUNSET		 	:		
	case DSC_MODE_SCENE_DAWN			:		
	case DSC_MODE_SCENE_BACKLIGHT		:		
	case DSC_MODE_SCENE_FIREWORK		:		
	case DSC_MODE_SCENE_BEACH_SNOW	 	:		
	case DSC_MODE_SCENE_SOUND_PICTURE	:		
	case DSC_MODE_SCENE_3D			 	:	
	case DSC_MODE_SCENE_BEST			:
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_ACTION			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
	case DSC_MODE_SCENE_CREATIVE		:
	case DSC_MODE_SCENE_PANORAMA		:
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_BEAUTY_SHUTTERSPEED:		UpdateShutterSpeedValue();	break;
		case DSC_SCENE_BEAUTY_APERTURE:			UpdateApertureValue();		break;
		}
		break;
	case DSC_MODE_MOVIE				 	:		break;
	}
	
	Invalidate(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateShutterSpeedValue
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Update ShtterSpeed Value
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::UpdateShutterSpeedValue()
{
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_M:
	case DSC_MODE_BULB: //dh0.seo 2015-01-12 주석 처리
		{
			if(m_nPropValueIndex - 2 < 0)
				m_strValue1 = _T("");
			else
				m_strValue1 = SZNX200PropShutterSpeedValue[m_nPropValueIndex - 2].strDesc;

			if(m_nPropValueIndex - 1 < 0)
				m_strValue2 = _T("");
			else
				m_strValue2 = SZNX200PropShutterSpeedValue[m_nPropValueIndex - 1].strDesc;

			m_strValue3 = SZNX200PropShutterSpeedValue[m_nPropValueIndex].strDesc;
			RSSetShutterVal(m_strValue3);

			if(m_nPropValueIndex + 1 > ARRAYSIZE(SZNX200PropShutterSpeedValue) -1)
				m_strValue4 = _T("");
			else
				m_strValue4 = SZNX200PropShutterSpeedValue[m_nPropValueIndex + 1].strDesc;

			if(m_nPropValueIndex + 2 > ARRAYSIZE(SZNX200PropShutterSpeedValue) -1)
				m_strValue5 = _T("");
			else
				m_strValue5 = SZNX200PropShutterSpeedValue[m_nPropValueIndex + 2].strDesc;
		}
		break;
	default:
		{
			if(m_nPropValueIndex - 2 < 0)
				m_strValue1 = _T("");
			else
				m_strValue1 = SZNX200PropShutterSpeedValueNoBulb[m_nPropValueIndex - 2].strDesc;

			if(m_nPropValueIndex - 1 < 0)
				m_strValue2 = _T("");
			else
				m_strValue2 = SZNX200PropShutterSpeedValueNoBulb[m_nPropValueIndex - 1].strDesc;

			m_strValue3 = SZNX200PropShutterSpeedValueNoBulb[m_nPropValueIndex].strDesc;
			RSSetShutterVal(m_strValue3);

			if(m_nPropValueIndex + 1 > ARRAYSIZE(SZNX200PropShutterSpeedValueNoBulb) -1)
				m_strValue4 = _T("");
			else
				m_strValue4 = SZNX200PropShutterSpeedValueNoBulb[m_nPropValueIndex + 1].strDesc;

			if(m_nPropValueIndex + 2 > ARRAYSIZE(SZNX200PropShutterSpeedValueNoBulb) -1)
				m_strValue5 = _T("");
			else
				m_strValue5 = SZNX200PropShutterSpeedValueNoBulb[m_nPropValueIndex + 2].strDesc;
		}
		break;
	}
 	
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateApertureValue
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Update Aperture Value
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::UpdateApertureValue()
{
	if(!m_parFNumber)
		return;

	if(m_nPropValueIndex - 2 < 0)
		m_strValue1 = _T("");
	else
		m_strValue1 = GetFNumberValue(m_nPropValueIndex - 2);

	if(m_nPropValueIndex - 1 < 0)
		m_strValue2 = _T("");
	else
		m_strValue2 = GetFNumberValue(m_nPropValueIndex - 1);

	m_strValue3 = GetFNumberValue(m_nPropValueIndex);

	if(m_nPropValueIndex + 1 > m_parFNumber->GetCount() -1)
		m_strValue4 = _T("");
	else
		m_strValue4 = GetFNumberValue(m_nPropValueIndex + 1);

	if(m_nPropValueIndex + 2 > m_parFNumber->GetCount() -1)
		m_strValue5 = _T("");
	else
		m_strValue5 = GetFNumberValue(m_nPropValueIndex + 2);
}

void CRSBarometerCtrl::UpdateISOValue()
{
	if(!m_parISO)
		return;

	if(m_parISO->GetSize() <= m_nPropValueIndex)
		m_nPropValueIndex = 0;

	if(m_nPropValueIndex - 2 < 0)
		m_strValue1 = _T("");
	else
		m_strValue1 = GetISOValue(m_nPropValueIndex - 2);

	if(m_nPropValueIndex - 1 < 0)
		m_strValue2 = _T("");
	else
		m_strValue2 = GetISOValue(m_nPropValueIndex - 1);

	m_strValue3 = GetISOValue(m_nPropValueIndex);

	if(m_nPropValueIndex + 1 > m_parISO->GetCount() -1)
		m_strValue4 = _T("");
	else
	{
		m_strValue4 = GetISOValue(m_nPropValueIndex + 1);
	}

	if(m_nPropValueIndex + 2 > m_parISO->GetCount() -1)
		m_strValue5 = _T("");
	else
	{
		m_strValue5 = GetISOValue(m_nPropValueIndex + 2);
	}
}
//------------------------------------------------------------------------------ 
//! @brief		GetFNumberValue
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Change String
//------------------------------------------------------------------------------ 
CString CRSBarometerCtrl::GetFNumberValue(int nIndex)
{
	CString strFNumber = _T("");

	if(!m_parFNumber->GetCount())
		return _T("");

	int nValue = m_parFNumber->GetAt(nIndex);

	if(nValue % 100)
		strFNumber.Format(_T("F%.1f"), (float)nValue / 100);
	else
	{
		if(nValue >= 1000)
			strFNumber.Format(_T("F%.0f"), (float)nValue / 100);
		else
			strFNumber.Format(_T("F%.1f"), (float)nValue / 100);
	}

	return strFNumber;
}

CString CRSBarometerCtrl::GetISOValue(int nIndex)
{
	CString strISOImage = _T("");

	if(!m_parISO->GetCount())
		return _T("");

	int nPropValue = m_parISO->GetAt(nIndex);
	if(nIndex > 0 && nPropValue == 0xffff)
	{
		strISOImage = _T("");
		return strISOImage;
	}
	else
		strISOImage = GetISOImage(nPropValue);

	return strISOImage;
}

//------------------------------------------------------------------------------ 
//! @brief		SetPos
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Set Shutter Speed Value
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::SetPos(BOOL bMouseWheelUp, BOOL bSet)
{
	switch(m_nPropIndex)
	{
	case DSC_PASM_SHUTTERSPEED:		SetShutterSpeedPos(bMouseWheelUp);	break;
	case DSC_PASM_APERTURE:			SetFNumberPos(bMouseWheelUp);		break;
	case DSC_PASM_ISO:				SetISOPos(bMouseWheelUp);		break;
	}

	//-- Update Control
	UpdateBarometerCtrl();

	//-- 2011-9-16 Lee JungTaek
	//-- Set Data
	if(bSet)
	{
		switch(m_nPropIndex)
		{
		case DSC_PASM_SHUTTERSPEED:		((CSmartPanelDlg*)GetParent())->SetPropertyValue(m_nPropIndex, m_nPropValueIndex);	break;
		case DSC_PASM_APERTURE:			
			if(m_parFNumber->GetCount())
				((CSmartPanelDlg*)GetParent())->SetPropertyValue(m_nPropIndex, m_parFNumber->GetAt(m_nPropValueIndex));	
			break;
		case DSC_PASM_ISO:				
			if(m_parISO->GetCount())
				((CSmartPanelDlg*)GetParent())->SetPropertyValue(m_nPropIndex, m_parISO->GetAt(m_nPropValueIndex));	
			break;
		}
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		SetShutterSpeedPos
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Set Shutter Speed Position
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::SetShutterSpeedPos(BOOL bMouseWheelUp)
{
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_M:
	case DSC_MODE_BULB: //dh0.seo 2015-01-12 주석 처리
		{
			if(m_nPropValueIndex <= 0)
			{
				//-- First Value
				if(bMouseWheelUp)
					m_nPropValueIndex++;
				else
					m_nPropValueIndex = 0;
			}	
			else if(m_nPropValueIndex >= ARRAYSIZE(SZNX200PropShutterSpeedValue) - 1)
			{
				//-- Last Value
				if(bMouseWheelUp)
					m_nPropValueIndex = ARRAYSIZE(SZNX200PropShutterSpeedValue) - 1;
				else
					m_nPropValueIndex--;
			}
			else
			{
				//-- Between
				if(bMouseWheelUp)
					m_nPropValueIndex++;
				else
					m_nPropValueIndex--;
			}	
		}
		break;
	default:
		{
			if(m_nPropValueIndex <= 0)
			{
				//-- First Value
				if(bMouseWheelUp)
					m_nPropValueIndex++;
				else
					m_nPropValueIndex = 0;
			}	
			else if(m_nPropValueIndex >= ARRAYSIZE(SZNX200PropShutterSpeedValueNoBulb) - 1)
			{
				//-- Last Value
				if(bMouseWheelUp)
					m_nPropValueIndex = ARRAYSIZE(SZNX200PropShutterSpeedValueNoBulb) - 1;
				else
					m_nPropValueIndex--;
			}
			else
			{
				//-- Between
				if(bMouseWheelUp)
					m_nPropValueIndex++;
				else
					m_nPropValueIndex--;
			}	
		}
		break;
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		SetFNumberPos
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Set F Number Position
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::SetFNumberPos(BOOL bMouseWheelUp)
{
	if(m_nPropValueIndex <= 0)
	{
		//-- First Value
		if(bMouseWheelUp)
			m_nPropValueIndex++;
		else
			m_nPropValueIndex = 0;
	}	
	else if(m_nPropValueIndex >= m_parFNumber->GetCount() - 1)
	{
		//-- Last Value
		if(bMouseWheelUp)
			m_nPropValueIndex = m_parFNumber->GetCount() - 1;
		else
			m_nPropValueIndex--;
	}
	else
	{
		//-- Between
		if(bMouseWheelUp)
			m_nPropValueIndex++;
		else
			m_nPropValueIndex--;
	}	
}

void CRSBarometerCtrl::SetISOPos(BOOL bMouseWheelUp)
{
	if(m_nPropValueIndex <= 0)
	{
		//-- First Value
		if(bMouseWheelUp)
			m_nPropValueIndex++;
		else
			m_nPropValueIndex = 0;
	}	
	else if(m_nPropValueIndex >= m_parISO->GetCount() - 1)
	{
		//-- Last Value
		if(bMouseWheelUp)
			m_nPropValueIndex = m_parISO->GetCount() - 1;
		else
			m_nPropValueIndex--;
	}
	else
	{
		//-- Between
		if(bMouseWheelUp)
			m_nPropValueIndex++;
		else
			m_nPropValueIndex--;
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	WM Message
//------------------------------------------------------------------------------ 
BOOL CRSBarometerCtrl::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_RBUTTONDOWN || pMsg->message == WM_RBUTTONDBLCLK)
	{
		//-- 2011-6-23 Lee JungTaek
		if(m_bEnable)
		{
			if(!m_bSelected)
			{
				GetParent()->PreTranslateMessage(pMsg);

				//-- 2011-8-5 Lee JungTaek
				//-- Check Capturing State
				if(!((CSmartPanelDlg*)GetParent())->IsCapturing())
					SetBarometerSelected(TRUE);
			}
			else
			{
				//-- Click Event
				CPoint point;
				GetCursorPos(&point);
				ScreenToClient(&point);
				SetPropValueIndex(PtInRectValue(point));
			}			
		} 		
 
 		return TRUE;
	}

	return CStatic::PreTranslateMessage(pMsg);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRectValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CRSBarometerCtrl::PtInRectValue(CPoint point)
{
	for(int i = 0 ; i < TEXT_POS_CNT; i ++)
	{
		if(m_rectText[i].PtInRect(point))
		{
			if(i != TEXT_POS_THIRD)
				return i;
		}			
	}

	return TEXT_POS_NULL;
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropValueIndex
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::SetPropValueIndex(int nIndex)
{
	if(nIndex == TEXT_POS_NULL)		return;

	if(nIndex > TEXT_POS_THIRD)
	{
		if(GetPropIndexValue() + 1 == GetPropListCnt())
			return;

		//-- Just Change
		for(int i = TEXT_POS_THIRD; i < nIndex - 1; i++)
			this->SetPos(TRUE, FALSE);
		//-- Set Real
		this->SetPos(TRUE);
	}
	else
	{
		if(GetPropIndexValue() == 0)
			return;

		//-- Just Change
		for(int i = TEXT_POS_THIRD; i > nIndex + 1; i--)
			this->SetPos(FALSE, FALSE);
		//-- Set Real
		this->SetPos(FALSE);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropValue
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Change Property Value To Indi Index
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::SetPropValue(int nPropValue)
{
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_SMART					:		
	case DSC_MODE_P						:		
	case DSC_MODE_A						:		
	case DSC_MODE_S						:
	case DSC_MODE_BULB					: //dh0.seo 2015-01-12 위치 이동
		switch(m_nPropIndex)
		{
		case DSC_PASM_SHUTTERSPEED:		
			SetShutterSpeedPropValuIndex(nPropValue, FALSE);
			break;
		case DSC_PASM_APERTURE:
			SetFNumberPropValueIndoex(nPropValue);
			break;
		case DSC_PASM_ISO:	
			SetISOPropValueIndoex(nPropValue);
			break;
		}
		break;
	case DSC_MODE_M						:		
		switch(m_nPropIndex)
		{
		case DSC_PASM_SHUTTERSPEED:		
			SetShutterSpeedPropValuIndex(nPropValue, TRUE);
			break;
		case DSC_PASM_APERTURE:
			SetFNumberPropValueIndoex(nPropValue);
			break;
		case DSC_PASM_ISO:	
			SetISOPropValueIndoex(nPropValue);
			break;	
		}		
		break;
	case DSC_MODE_I					 	:		
	case DSC_MODE_CAPTURE_MOVIE		 	:		
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		
	case DSC_MODE_MAGIC_SMART_FILTER	:		break;
	case DSC_MODE_PANORAMA			 	:
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:		
	case DSC_MODE_SCENE_PORTRAIT		:		
	case DSC_MODE_SCENE_CHILDREN		:		
	case DSC_MODE_SCENE_SPORTS		 	:		
	case DSC_MODE_SCENE_CLOSE_UP		:		
	case DSC_MODE_SCENE_TEXT			:		
	case DSC_MODE_SCENE_SUNSET		 	:		
	case DSC_MODE_SCENE_DAWN			:		
	case DSC_MODE_SCENE_BACKLIGHT		:		
	case DSC_MODE_SCENE_FIREWORK		:		
	case DSC_MODE_SCENE_BEACH_SNOW	 	:		
	case DSC_MODE_SCENE_SOUND_PICTURE	:
	case DSC_MODE_SCENE_3D			 	:
	// dh9.seo 2013-06-25
	case DSC_MODE_SCENE_BEST			:	
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_ACTION			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
	case DSC_MODE_SCENE_CREATIVE		:
	case DSC_MODE_SCENE_PANORAMA		:
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
			
		switch(m_nPropIndex)
		{
		case DSC_SCENE_BEAUTY_SHUTTERSPEED:	
			SetShutterSpeedPropValuIndex(nPropValue, FALSE);
			break;
		case DSC_SCENE_BEAUTY_APERTURE:		
			SetFNumberPropValueIndoex(nPropValue);
			break;
		}
		break;
	case DSC_MODE_MOVIE				 	:		break;
	}
			
	UpdateBarometerCtrl();
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-23
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::SetShutterSpeedPropValuIndex(int nPropValue, BOOL bBulb)
{
	switch(bBulb)
	{
	case TRUE:
		switch(nPropValue)
		{
		case eUCS_CAP_SHUTTERSPEED_BULB		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_BULB	;	break;	
		case eUCS_CAP_SHUTTERSPEED_30_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_30_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_25_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_25_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_20_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_20_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_15_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_15_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_13_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_13_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_10_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_10_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_80_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_80_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_60_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_60_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_50_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_50_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_40_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_40_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_30_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_30_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_25_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_25_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_20_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_20_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_16_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_16_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_13_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_13_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_8_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_8_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_6_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_6_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_5_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_5_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_4_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_4_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_3_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_3_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_4		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_4	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_5		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_5	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_6		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_6	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_8		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_8	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_13		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_13	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_15		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_15	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_20		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_20	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_25		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_25	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_30		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_30	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_40		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_40	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_50		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_50	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_60		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_60	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_80		:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_80	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_100	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_100	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_125	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_125	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_160	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_160	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_200	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_200	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_250	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_250	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_320	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_320	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_400	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_400	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_500	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_500	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_640	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_640	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_800	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_800	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_1000	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_1000	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_1250	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_1250	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_1600	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_1600	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_2000	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_2000	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_2500	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_2500	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_3200	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_3200	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_4000	:	m_nPropValueIndex = DSC_SHUTTERSPEED_M_1_4000	;	break;
		}
		break;
	case FALSE:
		switch(nPropValue)
		{
		case eUCS_CAP_SHUTTERSPEED_30_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_30_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_25_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_25_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_20_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_20_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_15_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_15_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_13_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_13_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_10_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_10_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_80_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_80_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_60_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_60_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_50_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_50_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_40_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_40_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_30_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_30_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_25_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_25_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_20_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_20_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_16_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_16_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_13_10	:	m_nPropValueIndex = DSC_SHUTTERSPEED_13_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_1		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_1	;	break;
		case eUCS_CAP_SHUTTERSPEED_8_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_8_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_6_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_6_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_5_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_5_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_4_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_4_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_3_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_3_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_4		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_4	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_5		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_5	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_6		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_6	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_8		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_8	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_10		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_10	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_13		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_13	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_15		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_15	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_20		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_20	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_25		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_25	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_30		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_30	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_40		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_40	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_50		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_50	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_60		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_60	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_80		:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_80	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_100	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_100	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_125	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_125	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_160	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_160	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_200	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_200	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_250	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_250	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_320	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_320	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_400	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_400	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_500	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_500	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_640	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_640	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_800	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_800	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_1000	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_1000	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_1250	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_1250	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_1600	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_1600	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_2000	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_2000	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_2500	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_2500	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_3200	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_3200	;	break;
		case eUCS_CAP_SHUTTERSPEED_1_4000	:	m_nPropValueIndex = DSC_SHUTTERSPEED_1_4000	;	break;
		}
		break;
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-23
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::SetFNumberPropValueIndoex(int nPropValue)
{
	if(!m_parFNumber)
		return;
	for(int i = 0; i < m_parFNumber->GetCount(); i++)
	{
		if(nPropValue == m_parFNumber->GetAt(i))
		{
			m_nPropValueIndex = i;
			break;
		}			
	}
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-23
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSBarometerCtrl::SetISOPropValueIndoex(int nPropValue)
{
	if(!m_parISO)
		return;
	for(int i = 0; i < m_parISO->GetCount(); i++)
	{
		if(nPropValue == m_parISO->GetAt(i))
		{
			m_nPropValueIndex = i;
			break;
		}		
	}
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString CRSBarometerCtrl::GetISOImage(int nPropValue)
{
	CString strImage = _T("");
	CString strModel = ((CSmartPanelDlg*)GetParent())->GetModel();
	if(!strModel.Compare(_T("NX1")))
	{
		switch(nPropValue)
		{
		case	eUCS_CAP_ISO_1_3_AUTO	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_auto.png"));		break;
		case	eUCS_CAP_ISO_1_3_100	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_100.png"));		break;
		case	eUCS_CAP_ISO_1_3_125	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_125.png"));		break;
		case	eUCS_CAP_ISO_1_3_160	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_160.png"));		break;
		case	eUCS_CAP_ISO_1_3_200	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_200.png"));		break;
		case	eUCS_CAP_ISO_1_3_250	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_250.png"));		break;
		case	eUCS_CAP_ISO_1_3_320	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_320.png"));		break;
		case	eUCS_CAP_ISO_1_3_400	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_400.png"));		break;
		case	eUCS_CAP_ISO_1_3_500	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_500.png"));		break;
		case	eUCS_CAP_ISO_1_3_640	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_640.png"));		break;
		case	eUCS_CAP_ISO_1_3_800	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_800.png"));		break;
		case	eUCS_CAP_ISO_1_3_1000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_1000.png"));		break;
		case	eUCS_CAP_ISO_1_3_1250	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_1250.png"));		break;
		case	eUCS_CAP_ISO_1_3_1600	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_1600.png"));		break;
		case	eUCS_CAP_ISO_1_3_2000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_2000.png"));		break;
		case	eUCS_CAP_ISO_1_3_2500	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_2500.png"));		break;
		case	eUCS_CAP_ISO_1_3_3200	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_3200.png"));		break;
		case	eUCS_CAP_ISO_1_3_4000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_4000.png"));		break;
		case	eUCS_CAP_ISO_1_3_5000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_5000.png"));		break;
		case	eUCS_CAP_ISO_1_3_6400	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_6400.png"));		break;
		case	eUCS_CAP_ISO_1_3_8000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_8000.png"));		break;
		case	eUCS_CAP_ISO_1_3_10000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_10000.png"));		break;
		case	eUCS_CAP_ISO_1_3_12800	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_12800.png"));		break;
		case	eUCS_CAP_ISO_1_3_16000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_16000.png"));		break;
		case	eUCS_CAP_ISO_1_3_20000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_20000.png"));		break;
		case	eUCS_CAP_ISO_1_3_25600	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_25600.png"));		break;
			//NX1 dh0.seo 2014-08-22
//		case	eUCS_CAP_ISO_1_3_32000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_32000.png"));		break;
//		case	eUCS_CAP_ISO_1_3_40000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_40000.png"));		break;
		case	eUCS_CAP_ISO_1_3_51200	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\icon_iso_51200.png"));		break; 
		}
	}
	else
	{
		switch(nPropValue)
		{
		case	eUCS_CAP_ISO_1_3_AUTO	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_AUTO.png"));		break;
		case	eUCS_CAP_ISO_1_3_100	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_100.png"));		break;
		case	eUCS_CAP_ISO_1_3_125	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_125.png"));		break;
		case	eUCS_CAP_ISO_1_3_160	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_160.png"));		break;
		case	eUCS_CAP_ISO_1_3_200	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_200.png"));		break;
		case	eUCS_CAP_ISO_1_3_250	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_250.png"));		break;
		case	eUCS_CAP_ISO_1_3_320	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_320.png"));		break;
		case	eUCS_CAP_ISO_1_3_400	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_400.png"));		break;
		case	eUCS_CAP_ISO_1_3_500	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_500.png"));		break;
		case	eUCS_CAP_ISO_1_3_640	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_640.png"));		break;
		case	eUCS_CAP_ISO_1_3_800	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_800.png"));		break;
		case	eUCS_CAP_ISO_1_3_1000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_1000.png"));		break;
		case	eUCS_CAP_ISO_1_3_1250	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_1250.png"));		break;
		case	eUCS_CAP_ISO_1_3_1600	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_1600.png"));		break;
		case	eUCS_CAP_ISO_1_3_2000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_2000.png"));		break;
		case	eUCS_CAP_ISO_1_3_2500	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_2500.png"));		break;
		case	eUCS_CAP_ISO_1_3_3200	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_3200.png"));		break;
		case	eUCS_CAP_ISO_1_3_4000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_4000.png"));		break;
		case	eUCS_CAP_ISO_1_3_5000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_5000.png"));		break;
		case	eUCS_CAP_ISO_1_3_6400	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_6400.png"));		break;
		case	eUCS_CAP_ISO_1_3_8000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_8000.png"));		break;
		case	eUCS_CAP_ISO_1_3_10000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_10000.png"));		break;
		case	eUCS_CAP_ISO_1_3_12800	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_12800.png"));		break;
		case	eUCS_CAP_ISO_1_3_16000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_16000.png"));		break;
		case	eUCS_CAP_ISO_1_3_20000	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_20000.png"));		break;
		case	eUCS_CAP_ISO_1_3_25600	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\ISO\\02_ISO_25600.png"));		break;
		}
	}

	return strImage;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPropDesc
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	Get PropDesc
//------------------------------------------------------------------------------ 
CString CRSBarometerCtrl::GetPropDesc()
{
	CString strPropDesc = _T("");

	switch(m_nPropIndex)
	{
	case DSC_PASM_SHUTTERSPEED:		
	case DSC_PASM_APERTURE:			strPropDesc.Format(_T("%s : %s"), m_strPropName, m_strValue3);										break;
	case DSC_PASM_ISO:				
		{
			for(int i = 0; i < ARRAYSIZE(SZNX200PropISOValue); i++)
			{
				if(m_parISO->GetAt(m_nPropValueIndex) == SZNX200PropISOValue[i].nValue)
				{
					strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX200PropISOValue[i].strDesc);
					break;
				}
			}
		}
		break;
	}
	
	return  strPropDesc;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPropStrValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString CRSBarometerCtrl::GetPropStrValue(int nIndex)
{
	switch(nIndex)
	{
	case TEXT_POS_FIRST		:	return m_strValue1;
	case TEXT_POS_SECOND	:	return m_strValue2;
	case TEXT_POS_THIRD		:	return m_strValue3;
	case TEXT_POS_FOURTH	:	return m_strValue4;
	case TEXT_POS_FIFTH		:	return m_strValue5;
	default:	return _T("");
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CRSBarometerCtrl::GetPropListCnt()
{
	int nCnt = 0;

	switch(m_nPropIndex)
	{
	case DSC_PASM_SHUTTERSPEED:		nCnt = DSC_SHUTTERSPEED_1_4000 + 1;	break;
	case DSC_PASM_APERTURE:			nCnt = m_parFNumber->GetCount();	break;
	case DSC_PASM_ISO:				nCnt = m_parISO->GetCount();		break;
	}

	return nCnt;
}