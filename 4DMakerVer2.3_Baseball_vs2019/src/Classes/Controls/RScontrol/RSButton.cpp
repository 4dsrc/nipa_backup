/////////////////////////////////////////////////////////////////////////////
//
//  ThumbNailDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "RSButton.h"
#include "cv.h"
#include "highgui.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRSButton::CRSButton(int nType)
{
	m_strPropertyName = _T("");
	m_strPropValue = _T("");
	m_nPropertyValue = 0;
	m_strImagePath = _T("");
	m_strBKImagePath = _T("");
	//m_nBtnStaus = BTN_BK_1ST_STATUS_NORMAL;
	//m_nBtnPosType = BTN_BK_2ND_NULL;
	m_strText = _T("");
	m_nBtnType = nType;
}

CRSButton::~CRSButton(void)
{
}

BEGIN_MESSAGE_MAP(CRSButton, CButtonST)
	ON_WM_PAINT()
END_MESSAGE_MAP()

void CRSButton::SetBitmaps(CString strImagePath)
{
	m_strImagePath = strImagePath;
	CButtonST::DrawTransparent(TRUE);
	CButtonST::DrawBorder(FALSE, TRUE);
}

void CRSButton::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CPaintDC* pDC = &dc;

	//-- 2011-6-29 Lee JungTaek
	CleanDC(pDC);
	
	Rect		DesRect;
	CRect		clRect;
	
	//-- 2011-5-18 Lee JungTaek
	//-- Modify To Background Image
	switch(m_nBtnType)
	{
	case BTN_BK_TYPE_1ST :
		{
			switch(m_nBtnStaus)
			{
			case BTN_BK_1ST_STATUS_NORMAL:
				m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn01_normal.png"));
				break;
			case BTN_BK_1ST_STATUS_FOCUS:
				m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn01_focus_user.png"));
				break;
			case BTN_BK_1ST_STATUS_PUSH:
				m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn01_push.png"));
				break;
			case BTN_BK_1ST_STATUS_DISABLE:
				m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn01_disable.png"));
				break;
			}
		}
		break;
	case BTN_BK_TYPE_2ND :
		{
			switch(m_nBtnStaus)
			{
			case BTN_BK_2ND_STATUS_SELECT:
			case BTN_BK_2ND_STATUS_NORMAL:
			case BTN_BK_2ND_STATUS_FOCUS:
				Set2ndBKImagePath(m_nBtnStaus);
				break;
			}
		}
		break;
	}
	
	//-- Draw Background Image
	Image ImgBk(m_strBKImagePath);

	this->GetClientRect(&clRect);
	DesRect.X = clRect.left;
	DesRect.Y = clRect.top;
	DesRect.Width = clRect.right - clRect.left;
	DesRect.Height = clRect.bottom - clRect.top;

 	Graphics gc(pDC->GetSafeHdc());
	gc.DrawImage(&ImgBk, DesRect);

	//-- Draw Value
	Image ImgValue(m_strImagePath);

	this->GetClientRect(&clRect);
	DesRect.X = (clRect.left + clRect.right)/2 - ImgValue.GetWidth()/2;
	DesRect.Y = (clRect.top + clRect.bottom)/2 - ImgValue.GetHeight()/2;
	DesRect.Width = ImgValue.GetWidth();
	DesRect.Height = ImgValue.GetHeight();

	gc.DrawImage(&ImgValue, DesRect);

	if(m_strText.GetLength())
	{
		CString strFontName = _T("SS_SJ_DTV_uni_20090209");

		Gdiplus::Font F(strFontName, 12,FontStyleRegular,UnitPixel);
		PointF P(20,9);
		SolidBrush B(Color(255, 255, 255, 255));
		gc.DrawString(m_strText,-1,&F,P,&B);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		CleanDC
//! @date		2011-6-29
//! @author		Lee JungTaek
//! @note	 	Clean DC before Draw
//------------------------------------------------------------------------------ 
void CRSButton::CleanDC(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);

	switch(m_nBtnType)
	{
	case BTN_BK_TYPE_1ST:	
		{
			CBrush brush = RGB_DEFAULT_DLG;		
			pDC->FillRect(rect, &brush);
		}
		break;
	case BTN_BK_TYPE_2ND:
 		{
 			CBrush brush = RGB_BLACK;
 			pDC->FillRect(rect, &brush);
 		}
		break;
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-8-30
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSButton::Set2ndBKImagePath(int nBtnStatus)
{
	switch(nBtnStatus)
	{
	case BTN_BK_2ND_STATUS_NORMAL:
		{
			switch(m_nBtnPosType)
			{
			case BTN_BK_2ND_POS_NORMAL			:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_normal.png"));	break;
			case BTN_BK_2ND_POS_LEFT			:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_normal_left.png"));	break;
			case BTN_BK_2ND_POS_RIGHT			:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_normal_right.png"));	break;
			case BTN_BK_2ND_POS_TOP_LEFT		:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_normal_topleft.png"));	break;
			case BTN_BK_2ND_POS_TOP_RIGHT		:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_normal_topright.png"));	break;
			case BTN_BK_2ND_POS_BOTTOM_LEFT		:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_normal_bottomleft.png"));	break;
			case BTN_BK_2ND_POS_BOTTOM_RIGHT	:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_normal_bottomright.png"));	break;
			default:				m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_normal.png"));	break;
			}
		}
		break;
	case BTN_BK_2ND_STATUS_SELECT:
		{
			switch(m_nBtnPosType)
			{
			case BTN_BK_2ND_POS_NORMAL			:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_select.png"));	break;
			case BTN_BK_2ND_POS_LEFT			:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_select_left.png"));	break;
			case BTN_BK_2ND_POS_RIGHT			:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_select_right.png"));	break;
			case BTN_BK_2ND_POS_TOP_LEFT		:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_select_topleft.png"));	break;
			case BTN_BK_2ND_POS_TOP_RIGHT		:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_select_topright.png"));	break;
			case BTN_BK_2ND_POS_BOTTOM_LEFT		:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_select_bottomleft.png"));	break;
			case BTN_BK_2ND_POS_BOTTOM_RIGHT	:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_select_bottomright.png"));	break;
			default:				m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_2ndpopup_select.png"));	break;
			}
		}
		break;	
	//CMiLRe 20141128 스마트패널 차일드 마우스 무브 포커스 및 마우스 리브 수정
	case BTN_BK_2ND_STATUS_FOCUS:
		{
			switch(m_nBtnPosType)
			{
			case BTN_BK_2ND_POS_NORMAL			:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn02_focus.png"));	break;
			case BTN_BK_2ND_POS_LEFT			:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn02_focus.png"));	break;
			case BTN_BK_2ND_POS_RIGHT			:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn02_focus.png"));	break;
			case BTN_BK_2ND_POS_TOP_LEFT		:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn02_focus.png"));	break;
			case BTN_BK_2ND_POS_TOP_RIGHT		:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn02_focus.png"));	break;
			case BTN_BK_2ND_POS_BOTTOM_LEFT		:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn02_focus.png"));	break;
			case BTN_BK_2ND_POS_BOTTOM_RIGHT	:	m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn02_focus.png"));	break;
			default:				m_strBKImagePath = RSGetRoot(_T("img\\02.Smart Panel\\02_smartpanel_btn02_focus.png"));	break;
			}
		}
		break;
	}	
}

