/////////////////////////////////////////////////////////////////////////////
//
//  ThumbNailDlg.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-05-13
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "RSListCtrl.h"
#include "SRSIndex.h"

CRSListCtrl::CRSListCtrl(void)
{
	m_preIndex = -1;
	m_nMainCamera = 0;
}

CRSListCtrl::~CRSListCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CRSListCtrl, CReportCtrl)
	ON_NOTIFY_REFLECT(NM_CLICK, &CRSListCtrl::OnNMClick)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CRSListCtrl::OnNMCustomdraw)
END_MESSAGE_MAP()
	
void CRSListCtrl::CreateImageList()
{
	BOOL	bRetValue = FALSE;
	HICON	hIcon = NULL;

	CBitmap bmp;
	// Create image list
	bRetValue = m_ImageList.Create(24, 24, ILC_COLOR32 | ILC_MASK, 4, 1);
	ASSERT(bRetValue == TRUE);

	bmp.LoadBitmap(IDB_MAINCAMERA);
	m_ImageList.Add(&bmp, RGB(255,255,255));
	bmp.Detach();

	bmp.LoadBitmap(IDB_DIABLECAMERA);
	m_ImageList.Add(&bmp, RGB(255,255,255));
	bmp.Detach();

	bmp.LoadBitmap(IDB_UNCHECKCAMERA);
	m_ImageList.Add(&bmp, RGB(255,255,255));
	bmp.Detach();

	bmp.LoadBitmap(IDB_CHECKCAMERA);
	m_ImageList.Add(&bmp, RGB(255,255,255));
	bmp.Detach();

	SetImageList(&m_ImageList);
} // End of CreateImageList

void CRSListCtrl::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	POSITION pos = this->GetFirstSelectedItemPosition();
	int nIndex = GetNextSelectedItem(pos);

	if(nIndex == IMAGE_MUTLIAPPY_DISABLE)
		return;

	if(m_preIndex == nIndex)
		return;
	
	m_preIndex = nIndex;

	if(nIndex == m_nMainCamera)
		return;

	int imageType;
	imageType = GetItemImage(nIndex, 0);

	if(imageType == IMAGE_MUTLIAPPY_UNCHECK)
	{
		SetItemImage(nIndex, 0, IMAGE_MUTLIAPPY_CHECK);
		TRACE(_T("Do Check"));
	}
	else if(imageType == IMAGE_MUTLIAPPY_CHECK)
	{
		SetItemImage(nIndex, 0, IMAGE_MUTLIAPPY_UNCHECK);
		TRACE(_T("Do UnCheck"));
	}	
}

void CRSListCtrl::SelectAll()
{
	for(int i = 0; i < this->GetItemCount(); i++)
	{
		if(i == m_nMainCamera || GetItemImage(i,0) == IMAGE_MUTLIAPPY_DISABLE)
			continue;
		else
		{
			SetItemImage(i, 0, IMAGE_MUTLIAPPY_CHECK);
			TRACE(_T("Do Check"));
		}
	}
}

void CRSListCtrl::CancelAll()
{
	for(int i = 0; i < this->GetItemCount(); i++)
	{
		if(i == m_nMainCamera || GetItemImage(i,0) == IMAGE_MUTLIAPPY_DISABLE)
			continue;
		else
		{
			SetItemImage(i, 0, IMAGE_MUTLIAPPY_UNCHECK);
			TRACE(_T("Do UnCheck"));
		}
	}
}

void CRSListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	CRSListCtrl::OnCustomDraw(pNMHDR, pResult);
}