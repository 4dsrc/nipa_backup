/////////////////////////////////////////////////////////////////////////////
//
//  CRSBarometerCtrl.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-08
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

enum TEXT_POS
{
	TEXT_POS_NULL = -1,
	TEXT_POS_FIRST,
	TEXT_POS_SECOND,
	TEXT_POS_THIRD,
	TEXT_POS_FOURTH,
	TEXT_POS_FIFTH,
	TEXT_POS_CNT,
};

class CRSBarometerCtrl: public CStatic
{
public:
	CRSBarometerCtrl(void);
	~CRSBarometerCtrl(void);

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()

private:
	int m_nPropIndex;
	int m_nPropValueIndex;
	CString m_strPropName;

	//-- 2011-6-21 Lee JungTaek
	//-- To F_Number
	CUIntArray	*m_parFNumber;
	CUIntArray	*m_parISO;

	//-- Image
	CString m_strBarometerBKPath;

	//-- 2011-6-8 Lee JungTaek
	//-- Value
	CString m_strValue1;
	CString m_strValue2;
	CString m_strValue3;
	CString m_strValue4;
	CString m_strValue5;

	CRect m_rectText[TEXT_POS_CNT];

	BOOL m_bSelected;
	//-- 2011-6-23 Lee JungTaek
	BOOL m_bEnable;

	void CleanDC(CDC* pDC);
	void DrawCtrlBKImage(CDC* pDC);
	void DrawCtrlText(CDC* pDC);
	void DrawCtrlValue(CDC* pDC);
	void UpdateShutterSpeedValue();
	void UpdateApertureValue();
	void UpdateISOValue();

	//-- 2011-7-23 Lee JungTaek
	void SetShutterSpeedPropValuIndex(int nPropValue, BOOL bBulb);
	void SetFNumberPropValueIndoex(int nPropValue);
	void SetISOPropValueIndoex(int nPropValue);

	CString GetFNumberValue(int nIndex);	

	//-- 2011-7-13 Lee JungTaek
	CString GetISOValue(int nIndex);
	//-- 2011-7-14 Lee JungTaek
	CString GetISOImage(int nPropValue);

	//-- 2011-9-16 Lee JungTaek
	//-- Click Event
	int PtInRectValue(CPoint point);
	void SetPropValueIndex(int nIndex);

public:
	void UpdateBarometerCtrl();
	void SetPropInfo(int nPropIndex, CString strPropName) {m_nPropIndex = nPropIndex;	m_strPropName = strPropName;}

	//-- 2011-9-15 Lee JungTaek
	int GetPropIndex()	{return m_nPropIndex;}
	int GetPropIndexValue()	{return m_nPropValueIndex;}
	CString GetPropName()	{return m_strPropName;}
	CString GetPropStrValue(int nIndex);
	int GetPropListCnt();

	//-- 2011-6-21 Lee JungTaek
	//-- Set Indi Position
	void SetPos(BOOL bMouseWheelUp, BOOL bSet = TRUE);
	void SetShutterSpeedPos(BOOL bMouseWheelUp);
	void SetFNumberPos(BOOL bMouseWheelUp);
	void SetISOPos(BOOL bMouseWheelUp);
	void SetFNumberList(CUIntArray *parFNumber) {m_parFNumber = parFNumber;}
	void SetISOList(CUIntArray *parISO) {m_parISO = parISO;}

	//-- 2011-6-21 Lee JungTaek
	void SetPropValue(int nPropValue);
	void SetEnable(BOOL bEnable)	{m_bEnable = bEnable;}
	void SetBarometerSelected(BOOL bSelected) {m_bSelected = bSelected; Invalidate(FALSE);}
		
	CString GetPropDesc();
};
