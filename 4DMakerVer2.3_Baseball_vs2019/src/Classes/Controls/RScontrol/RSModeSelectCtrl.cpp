/////////////////////////////////////////////////////////////////////////////
//
//  CRSModeSelectCtrl.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-08
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "RSModeSelectCtrl.h"
#include "SmartPanelIndex.h"
#include "SmartPanelDlg.h"
#include "NXRemoteStudioDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRSModeSelectCtrl::CRSModeSelectCtrl(void)
{
	m_strBarometerBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_bg_magic_02_user.png"));

	m_nPropValueIndex = 0;
	m_nPropIndex = 0;

	m_strModePath1 = _T("");
	m_strModePath2 = _T("");
	m_strModePath3 = _T("");
	m_strModePath4 = _T("");
	m_strModePath5 = _T("");
	m_strModePath6 = _T("");
	m_strModePath7 = _T("");

	m_bSelected = TRUE;

	//-- 2011-6-21 Lee JungTaek
	m_strPropName = _T("");

	//-- 2011-7-14 Lee JungTaek
	m_rectValue[MODE_POS_FIRST] = CRect(CPoint(0, 4),CSize(40,35));
	m_rectValue[MODE_POS_SECOND]= CRect(CPoint(40,4),CSize(40,35));
	m_rectValue[MODE_POS_THIRD] = CRect(CPoint(80,4),CSize(40,35));
	m_rectValue[MODE_POS_FOURTH]= CRect(CPoint(120,4),CSize(40,35));
	m_rectValue[MODE_POS_FIFTH] = CRect(CPoint(160,4),CSize(40,35));
	m_rectValue[MODE_POS_SIXTH] = CRect(CPoint(200,4),CSize(40,35));
	m_rectValue[MODE_POS_SEVENTH] = CRect(CPoint(240,4),CSize(40,35));


	//CMiLRe 20140901 Model Name Get/Set
	int nMainCamera = 0;
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	nMainCamera = pMainWnd->GetSelectedItem();
	SetModel(pMainWnd->m_SdiMultiMgr.GetCurModel(nMainCamera));
}

CRSModeSelectCtrl::~CRSModeSelectCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CRSModeSelectCtrl, CStatic)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

BOOL CRSModeSelectCtrl::OnEraseBkgnd(CDC* pDC) { return FALSE;}

void CRSModeSelectCtrl::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CPaintDC* pDC = &dc;
	
	//-- 2011-6-29 Lee JungTaek
	//-- Clean DC
	CleanDC(pDC);
	//-- Draw Image
	DrawCtrlBKImage(pDC);
	//-- Draw Text
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_MAGIC_MAGIC_FRAME		:	
		switch(m_nPropIndex)
		{
		case DSC_MAGIC_FRAME_MAGICTYPECHANGE:		DrawCtrlValue(pDC);	break;
		}
		break;
	case DSC_MODE_MAGIC_SMART_FILTER	:		
		switch(m_nPropIndex)
		{
		case DSC_SMART_FILTER_MAGICTYPECHANGE:		DrawCtrlValue(pDC);	break;
		}
		break;
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:		
	case DSC_MODE_SCENE_PORTRAIT		:		
	case DSC_MODE_SCENE_CHILDREN		:		
	case DSC_MODE_SCENE_SPORTS		 	:		
	case DSC_MODE_SCENE_CLOSE_UP		:		
	case DSC_MODE_SCENE_TEXT			:		
	case DSC_MODE_SCENE_SUNSET		 	:		
	case DSC_MODE_SCENE_DAWN			:		
	case DSC_MODE_SCENE_BACKLIGHT		:		
	case DSC_MODE_SCENE_FIREWORK		:		
	case DSC_MODE_SCENE_BEACH_SNOW	 	:		
	case DSC_MODE_SCENE_SOUND_PICTURE	:		
	case DSC_MODE_SCENE_3D			 	:
	// dh9.seo 2013-07-11
	case DSC_MODE_SCENE_BEST			:
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_ACTION			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
	case DSC_MODE_SCENE_CREATIVE		:
	case DSC_MODE_PANORAMA		:
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_BEAUTY_SCENECHANGE:		DrawCtrlValue(pDC);	break;
		}
		break;
	default:		break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		CleanDC
//! @date		2011-6-29
//! @author		Lee JungTaek
//! @note	 	Clean DC before Draw
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::CleanDC(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);
	CBrush brush = RGB_DEFAULT_DLG;
	pDC->FillRect(rect, &brush);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawCtrlBKImage
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Draw Image
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::DrawCtrlBKImage(CDC* pDC)
{
	Rect		DesRect;
	CRect		clRect;

	Graphics gc(pDC->GetSafeHdc());

	//-- Draw Background Image
	if(m_bSelected)
		m_strBarometerBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_bg_magic_02_f_user.png"));
	else
		m_strBarometerBKPath = RSGetRoot(_T("img\\02.Smart Panel\\02_bg_magic_02_user.png"));
	
	Image ImgBk(m_strBarometerBKPath);

	this->GetClientRect(&clRect);
	DesRect.X = clRect.left;
	DesRect.Y = clRect.top;
	DesRect.Width = clRect.right - clRect.left;
	DesRect.Height = clRect.bottom - clRect.top;

	gc.DrawImage(&ImgBk, DesRect);
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-13
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::DrawCtrlValue(CDC* pDC)
{
	//-- Set Alpha Color Matrix
	ColorMatrix matrix1 = {
		1,0,0,0,0,
		0,1,0,0,0,
		0,0,1,0,0,
		0,0,0,0.7f,0,
		0,0,0,0.0,1,
	};		
	ColorMatrix matrix2 = {
		1,0,0,0,0,
		0,1,0,0,0,
		0,0,1,0,0,
		0,0,0,0.2f,0,
		0,0,0,0.0,1,
	};

	ImageAttributes   IA1;   
	IA1.SetColorMatrix(&matrix1,ColorMatrixFlagsDefault,ColorAdjustTypeBitmap);

	ImageAttributes   IA2;   
	IA2.SetColorMatrix(&matrix2,ColorMatrixFlagsDefault,ColorAdjustTypeBitmap);

	//-- Set Rect
	Rect		DesRect;
	DesRect.X = 0;
	DesRect.Y = 12;
	DesRect.Width = 30;
	DesRect.Height = 23;

	Graphics gc(pDC->GetSafeHdc());

	//-- Value 1
	Image ImgValue1(m_strModePath1);
	gc.DrawImage(&ImgValue1, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA2);

	//-- Value 2
	Image ImgValue2(m_strModePath2);
	DesRect.X += 42;	
	gc.DrawImage(&ImgValue2, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);	

	//-- Value 3
	Image ImgValue3(m_strModePath3);
	DesRect.X += 42;	
	gc.DrawImage(&ImgValue3, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);	

	//-- Value 4
	Image ImgValue4(m_strModePath4);
	DesRect.X += 42;
	gc.DrawImage(&ImgValue4, DesRect);

	//-- Value 5
	Image ImgValue5(m_strModePath5);
	DesRect.X += 42;
	gc.DrawImage(&ImgValue5, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);		

	//-- Value 6
	Image ImgValue6(m_strModePath6);
	DesRect.X += 42;
	gc.DrawImage(&ImgValue6, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA1);	

	//-- Value 7
	Image ImgValue7(m_strModePath7);
	DesRect.X += 42;
	gc.DrawImage(&ImgValue7, DesRect, 0,0, DesRect.Width, DesRect.Height, UnitPixel, &IA2);	
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateBarometerCtrl
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Update Ctrl By Property
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::UpdateModeSelectCtrl(int nMode)
{
	//int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		
		switch(m_nPropIndex)
		{
		case DSC_MAGIC_FRAME_MAGICTYPECHANGE:		UpdateMagicFrameModeValue();	break;
		}
		break;
	case DSC_MODE_MAGIC_SMART_FILTER	:		
		switch(m_nPropIndex)
		{
		case DSC_SMART_FILTER_MAGICTYPECHANGE:		UpdateSmartFilterModeValue();	break;
		}
		break;
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:		
	case DSC_MODE_SCENE_PORTRAIT		:		
	case DSC_MODE_SCENE_CHILDREN		:		
	case DSC_MODE_SCENE_SPORTS		 	:		
	case DSC_MODE_SCENE_CLOSE_UP		:		
	case DSC_MODE_SCENE_TEXT			:		
	case DSC_MODE_SCENE_SUNSET		 	:		
	case DSC_MODE_SCENE_DAWN			:		
	case DSC_MODE_SCENE_BACKLIGHT		:		
	case DSC_MODE_SCENE_FIREWORK		:		
	case DSC_MODE_SCENE_BEACH_SNOW	 	:		
	case DSC_MODE_SCENE_SOUND_PICTURE	:	
	case DSC_MODE_SCENE_3D			 	:	
	// dh9.seo 2013-06-25
	case DSC_MODE_SCENE_BEST			:		
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_ACTION			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
	case DSC_MODE_SCENE_CREATIVE		:
	case DSC_MODE_SCENE_PANORAMA		:
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
	case DSC_MODE_PANORAMA	:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_BEAUTY_SCENECHANGE:
			UpdateSceneModeValue();	
			break;
		}
		break;
	default:		break;
	}
	
	Invalidate(FALSE);
}

//------------------------------------------------------------------------------ 
//! @brief		UpdateShutterSpeedValue
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Update ShtterSpeed Value
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::UpdateSceneModeValue()
{
	//prevent
	int nSize = 0;
	TRACE(_T("CRSModeSelectCtrl::m_nPropValueIndex == %d\n"), m_nPropValueIndex);
	
	//CMiLRe 20140901 Model Name Get/Set
	if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0) // || m_strModel == _T("NX1") //-- 2014-7-28 dh0.seo
	{
		nSize = ARRAYSIZE(SZNX2000PropSceneModeValue);
		for(int i=0 ; i<nSize ; i++)
		{
			if(SZNX2000PropSceneModeValue[i].nIndex == m_nPropValueIndex)
			{
				m_nPropValueIndex = i;
				break;
			}

		}
		if(m_nPropValueIndex - 3 < 0)
			m_strModePath1 = _T("");
		else
			m_strModePath1 = GetSceneModeValue(m_nPropValueIndex - 3);
	
		if(m_nPropValueIndex - 2 < 0)
			m_strModePath2 = _T("");
		else
			m_strModePath2 = GetSceneModeValue(m_nPropValueIndex - 2);

		if(m_nPropValueIndex - 1 < 0)
			m_strModePath3 = _T("");
		else
			m_strModePath3 = GetSceneModeValue(m_nPropValueIndex - 1);

		m_strModePath4 = GetSceneModeValue(m_nPropValueIndex);
	
		if(m_strModePath4)

		if(m_nPropValueIndex + 1 > ARRAYSIZE(SZNX2000PropSceneModeValue) -1)
			m_strModePath5 = _T("");
		else
			m_strModePath5 = GetSceneModeValue(m_nPropValueIndex + 1);

		if(m_nPropValueIndex + 2 > ARRAYSIZE(SZNX2000PropSceneModeValue) -1)
			m_strModePath6 = _T("");
		else
			m_strModePath6 = GetSceneModeValue(m_nPropValueIndex + 2);

		if(m_nPropValueIndex + 3 > ARRAYSIZE(SZNX2000PropSceneModeValue) -1)
			m_strModePath7 = _T("");
		else
			m_strModePath7 = GetSceneModeValue(m_nPropValueIndex + 3);
	}
	//NX1 dh0.seo 2014-08-20
	//CMiLRe 20140901 Model Name Get/Set
	else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		nSize = ARRAYSIZE(SZNX1PropSceneModeValue);
		for(int i=0 ; i< nSize; i++)
		{
			if(SZNX1PropSceneModeValue[i].nIndex == m_nPropValueIndex)
			{
				m_nPropValueIndex = i;
				break;
			}

		}
		if(m_nPropValueIndex - 3 < 0)
			m_strModePath1 = _T("");
		else
			m_strModePath1 = GetSceneModeValue(m_nPropValueIndex - 3);

		if(m_nPropValueIndex - 2 < 0)
			m_strModePath2 = _T("");
		else
			m_strModePath2 = GetSceneModeValue(m_nPropValueIndex - 2);

		if(m_nPropValueIndex - 1 < 0)
			m_strModePath3 = _T("");
		else
			m_strModePath3 = GetSceneModeValue(m_nPropValueIndex - 1);

		m_strModePath4 = GetSceneModeValue(m_nPropValueIndex);

		if(m_strModePath4)
			if(m_nPropValueIndex + 1 > ARRAYSIZE(SZNX1PropSceneModeValue) -1)
				m_strModePath5 = _T("");
			else
				m_strModePath5 = GetSceneModeValue(m_nPropValueIndex + 1);

		if(m_nPropValueIndex + 2 > ARRAYSIZE(SZNX1PropSceneModeValue) -1)
			m_strModePath6 = _T("");
		else
			m_strModePath6 = GetSceneModeValue(m_nPropValueIndex + 2);

		if(m_nPropValueIndex + 3 > ARRAYSIZE(SZNX1PropSceneModeValue) -1)
			m_strModePath7 = _T("");
		else
			m_strModePath7 = GetSceneModeValue(m_nPropValueIndex + 3);
	}
	else
	{ 
		nSize = ARRAYSIZE(SZNX200PropSceneModeValue);
		for(int i=0 ; i< nSize; i++)
		{
			if(SZNX200PropSceneModeValue[i].nIndex == m_nPropValueIndex)
			{
				m_nPropValueIndex = i;
				break;
			}
		}
		if(m_nPropValueIndex - 3 < 0)
		m_strModePath1 = _T("");
		else
			m_strModePath1 = GetSceneModeValue(m_nPropValueIndex - 3);
	
		if(m_nPropValueIndex - 2 < 0)
			m_strModePath2 = _T("");
		else
			m_strModePath2 = GetSceneModeValue(m_nPropValueIndex - 2);

		if(m_nPropValueIndex - 1 < 0)
			m_strModePath3 = _T("");
		else
			m_strModePath3 = GetSceneModeValue(m_nPropValueIndex - 1);

		m_strModePath4 = GetSceneModeValue(m_nPropValueIndex);
	
		if(m_strModePath4)

		if(m_nPropValueIndex + 1 > ARRAYSIZE(SZNX200PropSceneModeValue) -1)
			m_strModePath5 = _T("");
		else
			m_strModePath5 = GetSceneModeValue(m_nPropValueIndex + 1);

		if(m_nPropValueIndex + 2 > ARRAYSIZE(SZNX200PropSceneModeValue) -1)
			m_strModePath6 = _T("");
		else
			m_strModePath6 = GetSceneModeValue(m_nPropValueIndex + 2);

		if(m_nPropValueIndex + 3 > ARRAYSIZE(SZNX200PropSceneModeValue) -1)
			m_strModePath7 = _T("");
		else
			m_strModePath7 = GetSceneModeValue(m_nPropValueIndex + 3);
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-27
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::UpdateMagicFrameModeValue()
{
	if(m_nPropValueIndex - 3 < 0)
		m_strModePath1 = _T("");
	else
		m_strModePath1 = GetMagicFrameModeValue(m_nPropValueIndex - 3);

	if(m_nPropValueIndex - 2 < 0)
		m_strModePath2 = _T("");
	else
		m_strModePath2 = GetMagicFrameModeValue(m_nPropValueIndex - 2);

	if(m_nPropValueIndex - 1 < 0)
		m_strModePath3 = _T("");
	else
		m_strModePath3 = GetMagicFrameModeValue(m_nPropValueIndex - 1);

	m_strModePath4 = GetMagicFrameModeValue(m_nPropValueIndex);

	if(m_nPropValueIndex + 1 > ARRAYSIZE(SZNX200PropShutterSpeedValue) -1)
		m_strModePath5 = _T("");
	else
		m_strModePath5 = GetMagicFrameModeValue(m_nPropValueIndex + 1);

	if(m_nPropValueIndex + 2 > ARRAYSIZE(SZNX200PropShutterSpeedValue) -1)
		m_strModePath6 = _T("");
	else
		m_strModePath6 = GetMagicFrameModeValue(m_nPropValueIndex + 2);

	if(m_nPropValueIndex + 3 > ARRAYSIZE(SZNX200PropShutterSpeedValue) -1)
		m_strModePath7 = _T("");
	else
		m_strModePath7 = GetMagicFrameModeValue(m_nPropValueIndex + 3);
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-27
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::UpdateSmartFilterModeValue()
{
	if(m_nPropValueIndex - 3 < 0)
		m_strModePath1 = _T("");
	else
		m_strModePath1 = GetSmartFilterModeValue(m_nPropValueIndex - 3);

	if(m_nPropValueIndex - 2 < 0)
		m_strModePath2 = _T("");
	else
		m_strModePath2 = GetSmartFilterModeValue(m_nPropValueIndex - 2);

	if(m_nPropValueIndex - 1 < 0)
		m_strModePath3 = _T("");
	else
		m_strModePath3 = GetSmartFilterModeValue(m_nPropValueIndex - 1);

	m_strModePath4 = GetSmartFilterModeValue(m_nPropValueIndex);

	if(m_nPropValueIndex + 1 > ARRAYSIZE(SZNX200PropShutterSpeedValue) -1)
		m_strModePath5 = _T("");
	else
		m_strModePath5 = GetSmartFilterModeValue(m_nPropValueIndex + 1);

	if(m_nPropValueIndex + 2 > ARRAYSIZE(SZNX200PropShutterSpeedValue) -1)
		m_strModePath6 = _T("");
	else
		m_strModePath6 = GetSmartFilterModeValue(m_nPropValueIndex + 2);

	if(m_nPropValueIndex + 3 > ARRAYSIZE(SZNX200PropShutterSpeedValue) -1)
		m_strModePath7 = _T("");
	else
		m_strModePath7 = GetSmartFilterModeValue(m_nPropValueIndex + 3);
}

//------------------------------------------------------------------------------ 
//! @brief		SetPos
//! @date		2011-6-8
//! @author		Lee JungTaek
//! @note	 	Set Shutter Speed Value
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::SetPos(BOOL bMouseWheelUp, BOOL bSet)
{
	CString strModel;
	int nMainCamera = 0;
	int nMode = 0;
	//int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();
//	CSmartPanelDlg* SmartPanelDlg = ((CSmartPanelDlg*)GetParent());
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	nMainCamera = pMainWnd->GetSelectedItem();

	strModel = pMainWnd->m_SdiMultiMgr.GetCurModel(nMainCamera);
	nMode = pMainWnd->m_pDlgSmartPanel->GetMode();

	if(strModel == "NX1000" || strModel == "NX200"){
	switch(nMode)
	{
	case DSC_MODE_MAGIC_MAGIC_FRAME		:		
		switch(m_nPropIndex)
		{
		case DSC_MAGIC_FRAME_MAGICTYPECHANGE:		
			SetMagicFrameModePos(bMouseWheelUp);	
			//-- 2011-9-16 Lee JungTaek
			if(bSet)
				pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, m_nPropValueIndex);
			break;
		}
		break;
	case DSC_MODE_MAGIC_SMART_FILTER	:		
		switch(m_nPropIndex)
		{
		case DSC_SMART_FILTER_MAGICTYPECHANGE:					
				SetSmartFilterModePos(bMouseWheelUp);	

			if(bSet)
				pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, m_nPropValueIndex);
			break;
		}
		break;
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:		
	case DSC_MODE_SCENE_PORTRAIT		:		
	case DSC_MODE_SCENE_CHILDREN		:		
	case DSC_MODE_SCENE_SPORTS		 	:		
	case DSC_MODE_SCENE_CLOSE_UP		:		
	case DSC_MODE_SCENE_TEXT			:		
	case DSC_MODE_SCENE_SUNSET		 	:		
	case DSC_MODE_SCENE_DAWN			:		
	case DSC_MODE_SCENE_BACKLIGHT		:		
	case DSC_MODE_SCENE_FIREWORK		:		
	case DSC_MODE_SCENE_BEACH_SNOW	 	:		
	case DSC_MODE_SCENE_SOUND_PICTURE	:
	case DSC_MODE_SCENE_3D			 	:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_BEAUTY_SCENECHANGE:		
			SetSceneModePos(bMouseWheelUp);	
			if(bSet)
				pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX200PropSceneModeValue[m_nPropValueIndex].nIndex);
			break;
		}
		break;
	default:		break;
	}
	}
	else{
	// dh9.seo 2013-06-25
	switch(nMode)
	{
	case DSC_MODE_SCENE_BEAUTY		 	:
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:
	case DSC_MODE_SCENE_SUNSET		 	:
	case DSC_MODE_SCENE_FIREWORK		:
		if(!strModel.Compare(_T("NX1")))
		{
			switch(m_nPropIndex)
			{
			case DSC_SCENE_BEAUTY_SCENECHANGE:		
				SetSceneModePos(bMouseWheelUp);	
				if(bSet)
				{
					pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX1PropSceneModeValue[m_nPropValueIndex].nIndex);
					GetPropDesc();
					((CSmartPanelDlg*)GetParent())->SetPropertyDescription(m_nPropIndex);
				}
				break;
			}
			break;
		}
		else
		{
			switch(m_nPropIndex)
			{
			case DSC_SCENE_BEAUTY_SCENECHANGE:		
				SetSceneModePos(bMouseWheelUp);	
				if(bSet)
					pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX2000PropSceneModeValue[m_nPropValueIndex].nIndex);
				break;
			}
			break;
		}
	case DSC_MODE_SCENE_BEST			:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_BEST_SCENECHANGE:		
			SetSceneModePos(bMouseWheelUp);	
			if(bSet)
				pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX2000PropSceneModeValue[m_nPropValueIndex].nIndex);
			break;
		}
		break;
	case DSC_MODE_SCENE_ACTION			:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_ACTION_SCENECHANGE:		
			SetSceneModePos(bMouseWheelUp);	
			if(bSet)
				pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX2000PropSceneModeValue[m_nPropValueIndex].nIndex);
			break;
		}
		break;
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
		if(!strModel.Compare(_T("NX1")))
		{
			switch(m_nPropIndex)
			{
			case DSC_SCENE_MACRO_SCENECHANGE:		
				SetSceneModePos(bMouseWheelUp);	
				if(bSet)
				{
					pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX1PropSceneModeValue[m_nPropValueIndex].nIndex);
					GetPropDesc();
					((CSmartPanelDlg*)GetParent())->SetPropertyDescription(m_nPropIndex);
				}
				break;
			}
			break;
		}
		else
		{
			switch(m_nPropIndex)
			{
			case DSC_SCENE_MACRO_SCENECHANGE:		
				SetSceneModePos(bMouseWheelUp);	
				if(bSet)
					pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX2000PropSceneModeValue[m_nPropValueIndex].nIndex);
				break;
			}
			break;
		}
	case DSC_MODE_SCENE_CREATIVE		:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_CREATIVE_SCENECHANGE:		
			SetSceneModePos(bMouseWheelUp);	
			if(bSet)
				pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX2000PropSceneModeValue[m_nPropValueIndex].nIndex);
			break;
		}
		break;
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_ACTION_FREEZE_SCENECHANGE:		
			SetSceneModePos(bMouseWheelUp);	
			if(bSet)
			{
				pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX1PropSceneModeValue[m_nPropValueIndex].nIndex);
				GetPropDesc();
				((CSmartPanelDlg*)GetParent())->SetPropertyDescription(m_nPropIndex);
			}
			break;
		}
		break;
	case DSC_MODE_SCENE_LIGHT_TRACE	:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_LIGHT_TRACE_SCENECHANGE:		
			SetSceneModePos(bMouseWheelUp);	
			if(bSet)
			{
				pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX1PropSceneModeValue[m_nPropValueIndex].nIndex);
				GetPropDesc();
				((CSmartPanelDlg*)GetParent())->SetPropertyDescription(m_nPropIndex);
			}
			break;
		}
		break;
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_MULTI_EXPOSURE_SCENECHANGE:		
			SetSceneModePos(bMouseWheelUp);	
			if(bSet)
			{
				pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX1PropSceneModeValue[m_nPropValueIndex].nIndex);
				GetPropDesc();
				((CSmartPanelDlg*)GetParent())->SetPropertyDescription(m_nPropIndex);
			}
			break;
		}
		break;
	case DSC_MODE_PANORAMA		:	
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_AUTO_SHUTTER_SCENECHANGE:		
			SetSceneModePos(bMouseWheelUp);	
			if(bSet)
			{
				pMainWnd->m_pDlgSmartPanel->SetPropertyValue(m_nPropIndex, SZNX1PropSceneModeValue[m_nPropValueIndex].nIndex);
				GetPropDesc();
				((CSmartPanelDlg*)GetParent())->SetPropertyDescription(m_nPropIndex);
			}
			break;
		}
		break;
	default:		break;
	}
}
	UpdateModeSelectCtrl(nMode);
}

//------------------------------------------------------------------------------ 
//! @brief		SetSceneModePos
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Set Shutter Speed Position
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::SetSceneModePos(BOOL bMouseWheelUp)
{
	//dh9.seo 2013-07-08
	TRACE(_T("CRSModeSelectCtrl::m_nPropValueIndex == %d\n"), m_nPropValueIndex);

	//CMiLRe 20140901 Model Name Get/Set
	if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
	{
		if(m_nPropValueIndex <= 0)
		{
			//-- First Value
			if(bMouseWheelUp)
				m_nPropValueIndex++;
 			else
 				m_nPropValueIndex = 0;
		}	
		else if(m_nPropValueIndex >= ARRAYSIZE(SZNX2000PropSceneModeValue) - 1)
		{
			//-- Last Value
			if(bMouseWheelUp)
				m_nPropValueIndex = ARRAYSIZE(SZNX2000PropSceneModeValue) - 1;
			else
				m_nPropValueIndex--;
		}
		else
		{
			//-- Between
			if(bMouseWheelUp)
				m_nPropValueIndex++;
			else
				m_nPropValueIndex--;
		}
	}
	//NX1 dh0.seo 2014-08-26
	//CMiLRe 20140901 Model Name Get/Set
	else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		if(m_nPropValueIndex <= 0)
		{
			//-- First Value
			if(bMouseWheelUp)
				m_nPropValueIndex++;
			else
				m_nPropValueIndex = 0;
		}	
		else if(m_nPropValueIndex >= ARRAYSIZE(SZNX1PropSceneModeValue) - 1)
		{
			//-- Last Value
			if(bMouseWheelUp)
				m_nPropValueIndex = ARRAYSIZE(SZNX1PropSceneModeValue) - 1;
			else
				m_nPropValueIndex--;
		}
		else
		{
			//-- Between
			if(bMouseWheelUp)
				m_nPropValueIndex++;
			else
				m_nPropValueIndex--;
		}
	}
	else
	{
		if(m_nPropValueIndex <= 0)
		{
			//-- First Value
			if(bMouseWheelUp)
				m_nPropValueIndex++;
 			else
 				m_nPropValueIndex = 0;
		}	
		else if(m_nPropValueIndex >= ARRAYSIZE(SZNX200PropSceneModeValue) - 1)
		{
			//-- Last Value
			if(bMouseWheelUp)
				m_nPropValueIndex = ARRAYSIZE(SZNX200PropSceneModeValue) - 1;
			else
				m_nPropValueIndex--;
		}
		else
		{
			//-- Between
			if(bMouseWheelUp)
				m_nPropValueIndex++;
			else
				m_nPropValueIndex--;
		}	
	}
}

//------------------------------------------------------------------------------ 
//! @brief		SetMagicFrameModePos
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Set Shutter Speed Position
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::SetMagicFrameModePos(BOOL bMouseWheelUp)
{
	if(m_nPropValueIndex <= 0)
	{
		//-- First Value
		if(bMouseWheelUp)
			m_nPropValueIndex++;
		else
			m_nPropValueIndex = 0;
	}	
	else if(m_nPropValueIndex >= ARRAYSIZE(SZNX200PropMagicFrameValue) - 1)
	{
		//-- Last Value
		if(bMouseWheelUp)
			m_nPropValueIndex = ARRAYSIZE(SZNX200PropMagicFrameValue) - 1;
		else
			m_nPropValueIndex--;
	}
	else
	{
		//-- Between
		if(bMouseWheelUp)
			m_nPropValueIndex++;
		else
			m_nPropValueIndex--;
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		SetMagicFrameModePos
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Set Shutter Speed Position
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::SetSmartFilterModePos(BOOL bMouseWheelUp)
{
	//CMiLRe 20140901 SmartFilter 모델 분리
	if(ARRAY_SIZE(modelType) !=  MODEL_TYPE_MAX) return;

	MODEL_TYPE currenType = MDOEL_NX2000;
	int PropSmartFilterValueSize = 0;

	for(int i = MDOEL_NX2000 ; i < MODEL_TYPE_MAX ; i ++)
	{
		if(GetModel().Compare(modelType[i].strModelName) == 0)
		{
			currenType = modelType[i].currentModelType;
			break;
		}
	}

	switch(currenType)
	{
	case MDOEL_NX2000:
		PropSmartFilterValueSize = ARRAYSIZE(SZNX200PropSmartFilterValue);
		break;
	case MDOEL_NX1:
		PropSmartFilterValueSize = ARRAYSIZE(SZNX1PropSmartFilterValue);
		break;
	}
	
	if(m_nPropValueIndex <= 0)
	{
		//-- First Value
		if(bMouseWheelUp)
			m_nPropValueIndex++;
		else
			m_nPropValueIndex = 0;
	}	
	else if(m_nPropValueIndex >= PropSmartFilterValueSize - 1)
	{
		//-- Last Value
		if(bMouseWheelUp)
			m_nPropValueIndex = PropSmartFilterValueSize - 1;
		else
			m_nPropValueIndex--;
	}
	else
	{
		//-- Between
		if(bMouseWheelUp)
			m_nPropValueIndex++;
		else
			m_nPropValueIndex--;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		PreTranslateMessage
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	WM Message
//------------------------------------------------------------------------------ 
BOOL CRSModeSelectCtrl::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_RBUTTONDOWN || pMsg->message == WM_RBUTTONDBLCLK)
	{
		//-- 2011-6-23 Lee JungTaek
		if(!m_bSelected)
		{
			GetParent()->PreTranslateMessage(pMsg);

			//-- 2011-8-5 Lee JungTaek
			//-- Check Capturing State
			if(!((CSmartPanelDlg*)GetParent())->IsCapturing())
				SetModeSelectSelected(TRUE);
		}
		else
		{
			GetParent()->PreTranslateMessage(pMsg);

			//-- Click Event
			CPoint point;
			GetCursorPos(&point);
			ScreenToClient(&point);
			SetPropValueIndex(PtInRectValue(point));
		}			
			
 		return TRUE;
	}

	return CStatic::PreTranslateMessage(pMsg);
}

//------------------------------------------------------------------------------ 
//! @brief		PtInRectValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CRSModeSelectCtrl::PtInRectValue(CPoint point)
{
	for(int i = 0 ; i < MODE_POS_CNT; i ++)
	{
		if(m_rectValue[i].PtInRect(point))
		{
			if(i != MODE_POS_FOURTH)
				return i;
		}			
	}

	return MODE_POS_NULL;
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropValueIndex
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::SetPropValueIndex(int nIndex)
{
	if(nIndex == MODE_POS_NULL)		return;

	if(nIndex > MODE_POS_FOURTH)
	{
		if(GetPropIndexValue() + 1 == GetPropListCnt())
			return;

		//-- Just Change
		for(int i = MODE_POS_FOURTH; i < nIndex - 1; i++)
			this->SetPos(TRUE, FALSE);
		//-- Set Real
		this->SetPos(TRUE);
	}
	else
	{
		if(GetPropIndexValue() == 0)
			return;

		//-- Just Change
		for(int i = MODE_POS_FOURTH; i > nIndex + 1; i--)
			this->SetPos(FALSE, FALSE);
		//-- Set Real
		this->SetPos(FALSE);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropValue
//! @date		2011-6-21
//! @author		Lee JungTaek
//! @note	 	Change Property Value To Indi Index
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::SetPropValue(int nPropValue)
{
	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	int nMode = pMainWnd->m_pDlgSmartPanel->GetMode();

	switch(nMode)
	{
	case DSC_MODE_MAGIC_MAGIC_FRAME		:	
		switch(m_nPropIndex)
		{
		case DSC_MAGIC_FRAME_MAGICTYPECHANGE:		SetMagicFrameModeValueIndex(nPropValue);			break;
		}
		break;
	case DSC_MODE_MAGIC_SMART_FILTER	:		
		switch(m_nPropIndex)
		{
		case DSC_SMART_FILTER_MAGICTYPECHANGE:		SetSmartFilterModeValueIndex(nPropValue);			break;
		}
		break;
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:		
	case DSC_MODE_SCENE_PORTRAIT		:		
	case DSC_MODE_SCENE_CHILDREN		:		
	case DSC_MODE_SCENE_SPORTS		 	:		
	case DSC_MODE_SCENE_CLOSE_UP		:		
	case DSC_MODE_SCENE_TEXT			:		
	case DSC_MODE_SCENE_SUNSET		 	:		
	case DSC_MODE_SCENE_DAWN			:		
	case DSC_MODE_SCENE_BACKLIGHT		:		
	case DSC_MODE_SCENE_FIREWORK		:		
	case DSC_MODE_SCENE_BEACH_SNOW	 	:		
	case DSC_MODE_SCENE_SOUND_PICTURE	:
	case DSC_MODE_SCENE_3D			 	:		
	// dh9.seo 2013-06-25
	case DSC_MODE_SCENE_BEST			:
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_ACTION			:	
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
	case DSC_MODE_SCENE_CREATIVE		:
	case DSC_MODE_SCENE_PANORAMA	 	:
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
	case DSC_MODE_PANORAMA				:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_BEAUTY_SCENECHANGE:		SetSceneModeValueIndex(nPropValue);			break;
		}
		break;
	default:		break;
	}
			
	UpdateModeSelectCtrl(nMode);
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-23
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::SetSceneModeValueIndex(int nPropValue)
{	
	//CMiLRe 20140901 Model Name Get/Set
	if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		switch(nPropValue)
		{
			case eUCS_UI_MODE_SCENE_BEAUTY			:	m_nPropValueIndex = DSC_SCENE_MODE_BEUATY_NX1			;	break;
			case eUCS_UI_MODE_SCENE_NIGHT			:	m_nPropValueIndex = DSC_SCENE_MODE_NIGHT_NX1			;	break;
			case eUCS_UI_MODE_SCENE_LANDSCAPE		:	m_nPropValueIndex = DSC_SCENE_MODE_LANDSCAPE_NX1		;	break;
			case eUCS_UI_MODE_SCENE_SUNSET			:	m_nPropValueIndex = DSC_SCENE_MODE_SUNSET_NX1			;	break;
			case eUCS_UI_MODE_SCENE_FIREWORK		:	m_nPropValueIndex = DSC_SCENE_MODE_FIREWORK_NX1		;	break;
			case eUCS_UI_MODE_SCENE_RICH			:	m_nPropValueIndex = DSC_SCENE_MODE_RICH_NX1				;	break;
			case eUCS_UI_MODE_SCENE_WATERFALL		:	m_nPropValueIndex = DSC_SCENE_MODE_WATERFALL_NX1		;	break;
			case eUCS_UI_MODE_SCENE_SILHOUETTE		:	m_nPropValueIndex = DSC_SCENE_MODE_SILHOUETTE_NX1		;	break;
			case eUCS_UI_MODE_SCENE_PANORAMA		:	m_nPropValueIndex = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
			case eUCS_UI_MODE_SCENE_MULTI_EXPOSURE	:	m_nPropValueIndex = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
			case eUCS_UI_MODE_SCENE_AUTO_SHUTTER	:	m_nPropValueIndex = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
			case eUCS_UI_MODE_ACTION_FREEZE			:	m_nPropValueIndex = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
			case eUCS_UI_MODE_LIGHT_TRACE			:	m_nPropValueIndex = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
			case eUCS_UI_MODE_PANORAMA				:	m_nPropValueIndex = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
		}
	}
	else
	{
		switch(nPropValue)
		{
		case eUCS_CAP_SCENE_MODE_BEAUTY			:	m_nPropValueIndex = DSC_SCENE_MODE_BEUATY			;	break;			
		case eUCS_CAP_SCENE_MODE_NIGHT			:	m_nPropValueIndex = DSC_SCENE_MODE_NIGHT			;	break;
		case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	m_nPropValueIndex = DSC_SCENE_MODE_LANDSCAPE		;	break;
		case eUCS_CAP_SCENE_MODE_PORTRAIT		:	m_nPropValueIndex = DSC_SCENE_MODE_PORTRAIT			;	break;
		case eUCS_CAP_SCENE_MODE_CHILDREN		:	m_nPropValueIndex = DSC_SCENE_MODE_CHILDREN			;	break;
		case eUCS_CAP_SCENE_MODE_SPORTS			:	m_nPropValueIndex = DSC_SCENE_MODE_SPORTS			;	break;
		case eUCS_CAP_SCENE_MODE_CLOSE_UP		:	m_nPropValueIndex = DSC_SCENE_MODE_CLOSEUP			;	break;
		case eUCS_CAP_SCENE_MODE_TEXT			:	m_nPropValueIndex = DSC_SCENE_MODE_TEXT				;	break;
		case eUCS_CAP_SCENE_MODE_SUNSET			:	m_nPropValueIndex = DSC_SCENE_MODE_SUNSET			;	break;
		case eUCS_CAP_SCENE_MODE_DAWN			:	m_nPropValueIndex = DSC_SCENE_MODE_DAWN				;	break;
		case eUCS_CAP_SCENE_MODE_BACKLIGHT		:	m_nPropValueIndex = DSC_SCENE_MODE_BACKLIGHT		;	break;
		case eUCS_CAP_SCENE_MODE_FIREWORK		:	m_nPropValueIndex = DSC_SCENE_MODE_FIREWORK			;	break;
		case eUCS_CAP_SCENE_MODE_BEACH_SNOW		:	m_nPropValueIndex = DSC_SCENE_MODE_BEACH_SNOW		;	break;
		case eUCS_CAP_SCENE_MODE_SOUND_PICTURE	:	m_nPropValueIndex = DSC_SCENE_MODE_SOUND_PICTURE	;	break;
		case eUCS_CAP_SCENE_MODE_3D				:	m_nPropValueIndex = DSC_SCENE_MODE_3D				;	break;
		case eUCS_CAP_SCENE_MODE_SMART_FILTER	:	m_nPropValueIndex = DSC_SCENE_MODE_SMARTFILTER		;	break;
			// dh9.seo 2013-06-24
		case eUCS_CAP_SCENE_MODE_BEST			:	m_nPropValueIndex = DSC_SCENE_MODE_BEST				;	break;
		case eUCS_CAP_SCENE_MODE_MACRO			:	m_nPropValueIndex = DSC_SCENE_MODE_MACRO			;	break;
		case eUCS_CAP_SCENE_MODE_ACTION			:	m_nPropValueIndex = DSC_SCENE_MODE_ACTION			;	break;
		case eUCS_CAP_SCENE_MODE_RICH			:	m_nPropValueIndex = DSC_SCENE_MODE_RICH				;	break;
		case eUCS_CAP_SCENE_MODE_WATERFALL		:	m_nPropValueIndex = DSC_SCENE_MODE_WATERFALL		;	break;
		case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	m_nPropValueIndex = DSC_SCENE_MODE_SILHOUETTE		;	break;
		case eUCS_CAP_SCENE_MODE_LIGHT			:	m_nPropValueIndex = DSC_SCENE_MODE_LIGHT			;	break;
		case eUCS_CAP_SCENE_MODE_CREATIVE		:	m_nPropValueIndex = DSC_SCENE_MODE_CREATIVE			;	break;
		case eUCS_CAP_SCENE_MODE_PANORAMA		:	m_nPropValueIndex = DSC_SCENE_MODE_PANORAMA			;	break;
			//NX1 dh0.seo 2014-08-19
		case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	m_nPropValueIndex = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
		case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	m_nPropValueIndex = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
			//NX1 dh0.seo 2014-08-20
		case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	m_nPropValueIndex = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
		case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	m_nPropValueIndex = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
		}
	}
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-23
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::SetMagicFrameModeValueIndex(int nPropValue)
{
	switch(nPropValue)
	{
	case eUCS_CAP_MAGIC_FRAME_YESTERDAY	:	m_nPropValueIndex = DSC_MAGIC_FRAME_YESTERDAY			;	break;
	case eUCS_CAP_MAGIC_FRAME_OLDFILM	:	m_nPropValueIndex = DSC_MAGIC_FRAME_OLDFILM				;	break;
	case eUCS_CAP_MAGIC_FRAME_RIPPLE	:	m_nPropValueIndex = DSC_MAGIC_FRAME_RIPPLE				;	break;
	case eUCS_CAP_MAGIC_FRAME_FULLMOON	:	m_nPropValueIndex = DSC_MAGIC_FRAME_FULLMOON			;	break;
	case eUCS_CAP_MAGIC_FRAME_OLDRECORD	:	m_nPropValueIndex = DSC_MAGIC_FRAME_OLDRECORD			;	break;
	case eUCS_CAP_MAGIC_FRAME_MAGAZINE	:	m_nPropValueIndex = DSC_MAGIC_FRAME_MAGAZINE			;	break;
	case eUCS_CAP_MAGIC_FRAME_SUNNYDAY	:	m_nPropValueIndex = DSC_MAGIC_FRAME_SUNNYDAY			;	break;
	case eUCS_CAP_MAGIC_FRAME_CLASSICTV	:	m_nPropValueIndex = DSC_MAGIC_FRAME_CLASSICTV			;	break;
	case eUCS_CAP_MAGIC_FRAME_WALLART	:	m_nPropValueIndex = DSC_MAGIC_FRAME_WALLART				;	break;			
	case eUCS_CAP_MAGIC_FRAME_HOLIDAY	:	m_nPropValueIndex = DSC_MAGIC_FRAME_HOLIDAY				;	break;
	case eUCS_CAP_MAGIC_FRAME_BILLBOARD1:	m_nPropValueIndex = DSC_MAGIC_FRAME_BILLBOARD1			;	break;
	case eUCS_CAP_MAGIC_FRAME_BILLBOARD2:	m_nPropValueIndex = DSC_MAGIC_FRAME_BILLBOARD2			;	break;
	case eUCS_CAP_MAGIC_FRAME_NEWSPAPER	:	m_nPropValueIndex = DSC_MAGIC_FRAME_NEWSPAPER			;	break;
	}
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-23
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
void CRSModeSelectCtrl::SetSmartFilterModeValueIndex(int nPropValue)
{
	//CMiLRe 20140901 Model Name Get/Set
	switch(nPropValue)
	{
	case eUCS_CAP_SMART_FILTER_PHOTO_VIGNETTING	:	m_nPropValueIndex = DSC_SMARTFILTER_VIGNETTING_NX2000	;	break;			
	case eUCS_CAP_SMART_FILTER_PHOTO_MINIATURE	:	m_nPropValueIndex = DSC_SMARTFILTER_MINIATURE_NX2000		;	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_FISH_EYE	:	m_nPropValueIndex = DSC_SMARTFILTER_FISHEYE_NX2000			;	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_SKETCH		:	m_nPropValueIndex = DSC_SMARTFILTER_SKETCH_NX2000			;	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_DEFOG		:	m_nPropValueIndex = DSC_SMARTFILTER_DEFOG_NX2000			;	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_HALF_TONE	:	m_nPropValueIndex = DSC_SMARTFILTER_HALFTONE_NX2000		;	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_SOFT_FOCUS	:	m_nPropValueIndex = DSC_SMARTFILTER_SOFTFOCUS_NX2000		;	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_OLD_FILM1	:	m_nPropValueIndex = DSC_SMARTFILTER_OLDFILM1_NX2000		;	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_OLD_FILM2	:	m_nPropValueIndex = DSC_SMARTFILTER_OLDFILM2_NX2000		;	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_NEGATIVE	:	m_nPropValueIndex = DSC_SMARTFILTER_NEGATIVE_NX2000		;	break;
	//case eUCS_CAP_SMART_FILTER_PHOTO_NORMAL	:	m_nPropValueIndex = DSC_SMARTFILTER_NORMAL		;	break;
	}		
}


//------------------------------------------------------------------------------ 
//! @brief		GetSceneModeValue
//! @date		2011-7-27
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString CRSModeSelectCtrl::GetSceneModeValue(int nIndex)
{
	CString strSceneModeImage = _T("");

	CString strModel;
	int nMainCamera = 0;
	int nPropValue = 0;

	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	nMainCamera = pMainWnd->GetSelectedItem();
	strModel = pMainWnd->m_SdiMultiMgr.GetCurModel(nMainCamera);
	
	//CMiLRe 20140901 Model Name Get/Set
	if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
	{
		if(nIndex < ARRAY_SIZE(SZNX2000PropSceneModeValue))
			nPropValue = SZNX2000PropSceneModeValue[nIndex].nValue;
	}
	//NX1 dh0.seo 2014-08-19
	//CMiLRe 20140901 Model Name Get/Set
	else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		if(nIndex < ARRAY_SIZE(SZNX1PropSceneModeValue))
			nPropValue = SZNX1PropSceneModeValue[nIndex].nValue;
	}
	else
	{
		if(nIndex < ARRAY_SIZE(SZNX2000PropSceneModeValue))
		nPropValue = SZNX200PropSceneModeValue[nIndex].nValue;
	}

	strSceneModeImage = GetSceneModeImage(nPropValue);

	return strSceneModeImage;
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString CRSModeSelectCtrl::GetSceneModeImage(int nPropValue)
{
	CString strImage = _T("");

	CString strModel;
	int nMainCamera = 0;

	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	nMainCamera = pMainWnd->GetSelectedItem();
	strModel = pMainWnd->m_SdiMultiMgr.GetCurModel(nMainCamera);

	//CMiLRe 20140901 Model Name Get/Set
	if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
	{
		switch(nPropValue)
		{
		case eUCS_CAP_SCENE_MODE_BEAUTY			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_beautyface.png"));	break;
		case eUCS_CAP_SCENE_MODE_NIGHT			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_night.png"));	break;
		case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_landscape.png"));	break;
		case eUCS_CAP_SCENE_MODE_SUNSET			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_sunset.png"));	break;
		case eUCS_CAP_SCENE_MODE_FIREWORK		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_fireworks.png"));	break;
		case eUCS_CAP_SCENE_MODE_RICH			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_richtone.png"));	break;
		case eUCS_CAP_SCENE_MODE_WATERFALL		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_waterfall.png"));	break;
		case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_silhouette.png"));	break;
		case eUCS_CAP_SCENE_MODE_PANORAMA		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_panorama.png"));	break;
			//NX1 dh0.seo 2014-08-20
		case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_multiexposure.png"));	break;
		case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_auto_shutter.png"));	break;
		case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_actionfreeze.png"));	break;
		case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\05_mode_icon_smart_lighttrace.png"));	break;
		}
	}
	else
	{
		switch(nPropValue)
		{
		case eUCS_CAP_SCENE_MODE_BEAUTY			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_beauty.png"));	break;
		case eUCS_CAP_SCENE_MODE_NIGHT			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_night.png"));	break;
		case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_landscape.png"));	break;
		case eUCS_CAP_SCENE_MODE_PORTRAIT		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_portrait.png"));	break;
		case eUCS_CAP_SCENE_MODE_CHILDREN		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_children.png"));	break;
		case eUCS_CAP_SCENE_MODE_SPORTS			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_sports.png"));	break;
		case eUCS_CAP_SCENE_MODE_CLOSE_UP		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_closeup.png"));	break;
		case eUCS_CAP_SCENE_MODE_TEXT			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_text.png"));	break;
		case eUCS_CAP_SCENE_MODE_SUNSET			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_sunset.png"));	break;
		case eUCS_CAP_SCENE_MODE_DAWN			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_dawn.png"));	break;
		case eUCS_CAP_SCENE_MODE_BACKLIGHT		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_backlight.png"));	break;
		case eUCS_CAP_SCENE_MODE_FIREWORK		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_fireworks.png"));	break;
		case eUCS_CAP_SCENE_MODE_BEACH_SNOW		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_beach_snow.png"));	break;
		case eUCS_CAP_SCENE_MODE_SOUND_PICTURE	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_soundpicture.png"));	break;
		case eUCS_CAP_SCENE_MODE_3D				:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_3D.png"));	break;
			// dh9.seo 2013-06-24
		case eUCS_CAP_SCENE_MODE_BEST			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_smart_best_face.png"));	break;
		case eUCS_CAP_SCENE_MODE_MACRO			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_smart_macro.png"));	break;
		case eUCS_CAP_SCENE_MODE_ACTION			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_smart_actionfreeze.png"));	break;
		case eUCS_CAP_SCENE_MODE_RICH			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_smart_rich_tone.png"));	break;
		case eUCS_CAP_SCENE_MODE_WATERFALL		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_smart_waterfalltrace.png"));	break;
		case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_smart_silhouette.png"));	break;
		case eUCS_CAP_SCENE_MODE_LIGHT			:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_smart_lighttrace.png"));	break;
		case eUCS_CAP_SCENE_MODE_PANORAMA		:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_scene_panorama.png"));	break;
		}
	}
	return strImage;
}

//------------------------------------------------------------------------------ 
//! @brief		GetMagicFrameModeValue
//! @date		2011-7-27
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString CRSModeSelectCtrl::GetMagicFrameModeValue(int nIndex)
{
	CString strMagicFrameMode = _T("");

	//prevent
	if(nIndex < ARRAYSIZE(SZNX200PropMagicFrameValue))
	{
		int nPropValue = SZNX200PropMagicFrameValue[nIndex].nValue;
		strMagicFrameMode = GetMagicFrameModeImage(nPropValue);
	}

	return strMagicFrameMode;
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString CRSModeSelectCtrl::GetMagicFrameModeImage(int nPropValue)
{
	CString strImage = _T("");

	switch(nPropValue)
	{
	case eUCS_CAP_MAGIC_FRAME_YESTERDAY	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_01.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_OLDFILM	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_02.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_RIPPLE	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_03.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_FULLMOON	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_04.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_OLDRECORD	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_05.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_MAGAZINE	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_06.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_SUNNYDAY	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_07.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_CLASSICTV	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_08.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_WALLART	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_09.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_HOLIDAY	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_10.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_BILLBOARD1:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_11.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_BILLBOARD2:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_12.png"));	break;
	case eUCS_CAP_MAGIC_FRAME_NEWSPAPER	:	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_mf_13.png"));	break;
	}

	return strImage;
}

//------------------------------------------------------------------------------ 
//! @brief		GetSmartFilterModeValue
//! @date		2011-7-27
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString CRSModeSelectCtrl::GetSmartFilterModeValue(int nIndex)
{
	CString strSmartFilterMode = _T("");

	//prevent
	if(nIndex <  ARRAYSIZE(SZNX200PropSmartFilterValue))
	{	
		int nPropValue = SZNX200PropSmartFilterValue[nIndex].nValue;
		strSmartFilterMode = GetSmartFilterModeImage(nPropValue);
	}
	return strSmartFilterMode;
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString CRSModeSelectCtrl::GetSmartFilterModeImage(int nPropValue)
{
	CString strImage = _T("");

	switch(nPropValue)
	{
	case eUCS_CAP_SMART_FILTER_PHOTO_VIGNETTING	 :	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_sf_vignetting.png"));	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_MINIATURE	 :	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_sf_miniature.png"));	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_FISH_EYE	 :	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_sf_fisheye.png"));	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_SKETCH		 :	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_sf_sketch.png"));	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_DEFOG		 :	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_sf_defog.png"));	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_HALF_TONE	 :	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_sf_halftone.png"));	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_SOFT_FOCUS	 :	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_sf_softfocus.png"));	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_OLD_FILM1	 :	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_sf_oldfilm1.png"));	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_OLD_FILM2	 :	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_sf_oldfilm2.png"));	break;
	case eUCS_CAP_SMART_FILTER_PHOTO_NEGATIVE	 :	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_sf_negative.png"));	break;
	//case eUCS_CAP_SMART_FILTER_PHOTO_NORMAL		 :	strImage = RSGetRoot(_T("img\\02.Smart Panel\\10_icon_sf_smartfilter.png"));	break;
	}

	return strImage;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPropDesc
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	Get PropDesc
//------------------------------------------------------------------------------ 
CString CRSModeSelectCtrl::GetPropDesc()
{
	//dh9.seo 2013-07-11
	TRACE(_T("CRSModeSelectCtrl::m_nPropValueIndex == %d\n"), m_nPropValueIndex);

	CString strModel;
	int nMainCamera = 0;

	CNXRemoteStudioDlg* pMainWnd = (CNXRemoteStudioDlg*)AfxGetMainWnd();
	nMainCamera = pMainWnd->GetSelectedItem();

	strModel = pMainWnd->m_SdiMultiMgr.GetCurModel(nMainCamera);

 	CString strPropDesc = _T("");
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	if(strModel == "NX1000" || strModel == "NX200"){
	switch(nMode)
	{
	
	case DSC_MODE_MAGIC_MAGIC_FRAME		:	
		switch(m_nPropIndex)
		{
		case DSC_MAGIC_FRAME_MAGICTYPECHANGE:		
			{
				for(int i = 0; i < ARRAYSIZE(SZNX200PropMagicFrameValue); i++)
				{
					if(m_nPropValueIndex == SZNX200PropMagicFrameValue[i].nIndex)
					{
						strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX200PropMagicFrameValue[i].strDesc);
						break;
					}
				}
			}
			break;
		}
		break;
	case DSC_MODE_MAGIC_SMART_FILTER	:		
		switch(m_nPropIndex)
		{
		case DSC_SMART_FILTER_MAGICTYPECHANGE:		
			{
				for(int i = 0; i < ARRAYSIZE(SZNX200PropSmartFilterValue); i++)
				{
					if(m_nPropValueIndex == SZNX200PropSmartFilterValue[i].nIndex)
					{
						strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX200PropSmartFilterValue[i].strDesc);
						break;
					}
				}
			}
			break;
		}
		break;
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:
	case DSC_MODE_SCENE_PORTRAIT		:		
	case DSC_MODE_SCENE_CHILDREN		:		
	case DSC_MODE_SCENE_SPORTS		 	:		
	case DSC_MODE_SCENE_CLOSE_UP		:		
	case DSC_MODE_SCENE_TEXT			:		
	case DSC_MODE_SCENE_SUNSET		 	:		
	case DSC_MODE_SCENE_DAWN			:		
	case DSC_MODE_SCENE_BACKLIGHT		:		
	case DSC_MODE_SCENE_FIREWORK		:		
	case DSC_MODE_SCENE_BEACH_SNOW	 	:		
	case DSC_MODE_SCENE_SOUND_PICTURE	:
	case DSC_MODE_SCENE_3D			 	:	
		switch(m_nPropIndex)
		{
		case DSC_SCENE_BEAUTY_SCENECHANGE:		
			{
				for(int i = 0; i < ARRAYSIZE(SZNX200PropSceneModeValue); i++)
				{
					if(m_nPropValueIndex == SZNX200PropSceneModeValue[i].nIndex)
					{
						strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX200PropSceneModeValue[i].strDesc);
						break;
					}
				}
			}
			break;
		}
	default:		break;
	}
	}
	else{
	switch(nMode)
	{
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:
	case DSC_MODE_SCENE_SUNSET		 	:
	case DSC_MODE_SCENE_FIREWORK		:
		//CMiLRe 20140901 Model Name Get/Set
		if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
		{
			//2014-09-20 문제 발생 지점 - SCENE NAME
			switch(m_nPropIndex)
			{
			case DSC_SCENE_BEAUTY_SCENECHANGE:		
				{
					for(int i = 0; i < ARRAYSIZE(SZNX1PropSceneModeValue); i++)
					{
						if(m_nPropValueIndex == SZNX1PropSceneModeValue[i].nIndex)
						{
							strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX1PropSceneModeValue[i].strDesc);
							break;
						}
					}
				}
				break;
			}
			break;
		}
		else
		{
			switch(m_nPropIndex)
			{
			case DSC_SCENE_BEAUTY_SCENECHANGE:		
				{
					for(int i = 0; i < ARRAYSIZE(SZNX2000PropSceneModeValue); i++)
					{
						if(m_nPropValueIndex == SZNX2000PropSceneModeValue[i].nIndex)
						{
							strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX2000PropSceneModeValue[i].strDesc);
							break;
						}
					}
				}
				break;
			}
			break;
		}
		// dh9.seo 2013-06-25
	case DSC_MODE_SCENE_BEST			:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_BEST_SCENECHANGE:		
			{
				for(int i = 0; i < ARRAYSIZE(SZNX2000PropSceneModeValue); i++)
				{
					if(m_nPropValueIndex == SZNX2000PropSceneModeValue[i].nIndex)
					{
						strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX2000PropSceneModeValue[i].strDesc);
						break;
					}
				}
			}
			break;
		}
		break;
	case DSC_MODE_SCENE_ACTION			:
		//CMiLRe 20140901 Model Name Get/Set
		if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
		{
			switch(m_nPropIndex)
			{
			case DSC_SCENE_ACTION_SCENECHANGE:		
				{
					for(int i = 0; i < ARRAYSIZE(SZNX1PropSceneModeValue); i++)
					{
						if(m_nPropValueIndex == SZNX1PropSceneModeValue[i].nIndex)
						{
							strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX1PropSceneModeValue[i].strDesc);
							break;
						}
					}
				}
				break;
			}
			break;
		}
		else
		{
			switch(m_nPropIndex)
			{
			case DSC_SCENE_ACTION_SCENECHANGE:		
				{
					for(int i = 0; i < ARRAYSIZE(SZNX2000PropSceneModeValue); i++)
					{
						if(m_nPropValueIndex == SZNX2000PropSceneModeValue[i].nIndex)
						{
							strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX2000PropSceneModeValue[i].strDesc);
							break;
						}
					}
				}
				break;
			}
			break;
		}
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
		//CMiLRe 20140901 Model Name Get/Set
		if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
		{
			switch(m_nPropIndex)
			{
			case DSC_SCENE_MACRO_SCENECHANGE:		
				{
					for(int i = 0; i < ARRAYSIZE(SZNX1PropSceneModeValue); i++)
					{
						if(m_nPropValueIndex == SZNX1PropSceneModeValue[i].nIndex)
						{
							strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX1PropSceneModeValue[i].strDesc);
							break;
						}
					}
				}
				break;
			}
			break;
		}
		else
		{
			switch(m_nPropIndex)
			{
			case DSC_SCENE_MACRO_SCENECHANGE:		
				{
					for(int i = 0; i < ARRAYSIZE(SZNX2000PropSceneModeValue); i++)
					{
						if(m_nPropValueIndex == SZNX2000PropSceneModeValue[i].nIndex)
						{
							strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX2000PropSceneModeValue[i].strDesc);
							break;
						}
					}
				}
				break;
			}
			break;
		}
	case DSC_MODE_SCENE_CREATIVE		:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_CREATIVE_SCENECHANGE:		
			{
				for(int i = 0; i < ARRAYSIZE(SZNX2000PropSceneModeValue); i++)
				{
					if(m_nPropValueIndex == SZNX2000PropSceneModeValue[i].nIndex)
					{
						strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX2000PropSceneModeValue[i].strDesc);
						break;
					}
				}
			}
			break;
		}
		break;
	case DSC_MODE_SCENE_PANORAMA	 	:
		//CMiLRe 20140901 Model Name Get/Set
		if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
		{
			switch(m_nPropIndex)
			{
			case DSC_SCENE_BEAUTY_SCENECHANGE:		
				{
					for(int i = 0; i < ARRAYSIZE(SZNX1PropSceneModeValue); i++)
					{
						if(m_nPropValueIndex == SZNX1PropSceneModeValue[i].nIndex)
						{
							strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX1PropSceneModeValue[i].strDesc);
							break;
						}
					}
				}
				break;
			}
			break;
		}
		else
		{
			switch(m_nPropIndex)
			{
			case DSC_SCENE_BEAUTY_SCENECHANGE:		
				{
					for(int i = 0; i < ARRAYSIZE(SZNX2000PropSceneModeValue); i++)
					{
						if(m_nPropValueIndex == SZNX2000PropSceneModeValue[i].nIndex)
						{
							strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX2000PropSceneModeValue[i].strDesc);
							break;
						}
					}
				}
				break;
			}
			break;
		}
		
		//NX1 dh0.seo 2014-08-19
	case DSC_MODE_PANORAMA	 	:
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_MULTI_EXPOSURE_SCENECHANGE:		
			{
				for(int i = 0; i < ARRAYSIZE(SZNX1PropSceneModeValue); i++)
				{
					if(m_nPropValueIndex == SZNX1PropSceneModeValue[i].nIndex)
					{
						strPropDesc.Format(_T("%s : %s"), m_strPropName, SZNX1PropSceneModeValue[i].strDesc);
						break;
					}
				}
			}
			break;
		}
		break;
	default:		break;
	}
}
return  strPropDesc;
}

void CRSModeSelectCtrl::SetPropInfo(int nPropIndex, CString strPropName)
{
	m_nPropIndex = nPropIndex;	
	m_strPropName = strPropName;

	if(m_nPropIndex == 0)
		m_bSelected = TRUE;
	else
		m_bSelected = FALSE;
}

//------------------------------------------------------------------------------ 
//! @brief		GetPropStrValue
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString CRSModeSelectCtrl::GetPropStrValue(int nIndex)
{
	switch(nIndex)
	{
	case MODE_POS_FIRST		:	return m_strModePath1;
	case MODE_POS_SECOND		:	return m_strModePath2;
	case MODE_POS_THIRD		:	return m_strModePath3;
	case MODE_POS_FOURTH		:	return m_strModePath4;
	case MODE_POS_FIFTH		:	return m_strModePath5;
	case MODE_POS_SIXTH		:	return m_strModePath6;
	case MODE_POS_SEVENTH		:	return m_strModePath7;
	default:	return _T("");
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		
//! @date		2011-9-15
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
int CRSModeSelectCtrl::GetPropListCnt()
{
	int nCnt = 0;
	int nMode = ((CSmartPanelDlg*)GetParent())->GetMode();

	switch(nMode)
	{
	case DSC_MODE_MAGIC_MAGIC_FRAME		:	
		switch(m_nPropIndex)
		{
		case DSC_MAGIC_FRAME_MAGICTYPECHANGE:		nCnt = DSC_MAGIC_FRAME_NEWSPAPER + 1;	break;
		}
		break;
	case DSC_MODE_MAGIC_SMART_FILTER	:		
		switch(m_nPropIndex)
		{
		case DSC_SMART_FILTER_MAGICTYPECHANGE:		nCnt = DSC_MAGIC_MODE_SMART_FITLER + 1;	break;
		}
		break;
	case DSC_MODE_SCENE_BEAUTY		 	:		
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_LANDSCAPE		:		
	case DSC_MODE_SCENE_PORTRAIT		:		
	case DSC_MODE_SCENE_CHILDREN		:		
	case DSC_MODE_SCENE_SPORTS		 	:		
	case DSC_MODE_SCENE_CLOSE_UP		:		
	case DSC_MODE_SCENE_TEXT			:		
	case DSC_MODE_SCENE_SUNSET		 	:		
	case DSC_MODE_SCENE_DAWN			:		
	case DSC_MODE_SCENE_BACKLIGHT		:		
	case DSC_MODE_SCENE_FIREWORK		:		
	case DSC_MODE_SCENE_BEACH_SNOW	 	:		
	case DSC_MODE_SCENE_SOUND_PICTURE	:
	// dh9.seo 2013-06-25
	case DSC_MODE_SCENE_BEST				:
	case DSC_MODE_SCENE_MACRO				:
	case DSC_MODE_SCENE_ACTION				:	
	case DSC_MODE_SCENE_RICH				:
	case DSC_MODE_SCENE_WATERFALL			:
	case DSC_MODE_SCENE_SILHOUETTE			:
	case DSC_MODE_SCENE_LIGHT				:
	case DSC_MODE_SCENE_CREATIVE			:
	case DSC_MODE_SCENE_PANORAMA			:
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
	case DSC_MODE_SCENE_LIGHT_TRACE		:
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		switch(m_nPropIndex)
		{
		case DSC_SCENE_BEAUTY_SCENECHANGE:		nCnt = DSC_MODE_SCENE_PANORAMA + 1;	break;
		}
		break;
	default:		break;
	}
	
	return nCnt;
}