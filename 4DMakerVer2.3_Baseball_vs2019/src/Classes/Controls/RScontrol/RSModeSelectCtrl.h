/////////////////////////////////////////////////////////////////////////////
//
//  CRSModeSelectCtrl.cpp
//
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @modified	Hongsu Jung (hongsu.jung@samsung.com)
// @Date	2011-06-08
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

enum MODE_POS
{
	MODE_POS_NULL = -1,
	MODE_POS_FIRST,
	MODE_POS_SECOND,
	MODE_POS_THIRD,
	MODE_POS_FOURTH,
	MODE_POS_FIFTH,
	MODE_POS_SIXTH,
	MODE_POS_SEVENTH,
	MODE_POS_CNT,
};

class CRSModeSelectCtrl: public CStatic
{
public:
	CRSModeSelectCtrl(void);
	~CRSModeSelectCtrl(void);

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	DECLARE_MESSAGE_MAP()

private:
	int m_nPropIndex;
	int m_nPropValueIndex;
	CString m_strPropName;
	//CMiLRe 20140901 Model Name Get/Set
	CString m_strModel;
	//-- Image
	CString m_strBarometerBKPath;

	//-- 2011-6-8 Lee JungTaek
	//-- Value
	CString m_strModePath1;
	CString m_strModePath2;
	CString m_strModePath3;
	CString m_strModePath4;
	CString m_strModePath5;
	CString m_strModePath6;
	CString m_strModePath7;

	//-- 2011-9-22 Lee JungTaek
	CRect m_rectValue[MODE_POS_CNT];

	BOOL m_bSelected;

	void CleanDC(CDC* pDC);
	void DrawCtrlBKImage(CDC* pDC);
	void DrawCtrlValue(CDC* pDC);
	void UpdateSceneModeValue();
	void UpdateMagicFrameModeValue();
	void UpdateSmartFilterModeValue();

	//-- 2011-9-16 Lee JungTaek
	//-- Click Event
	int PtInRectValue(CPoint point);
	void SetPropValueIndex(int nIndex);

public:
	void UpdateModeSelectCtrl(int nMode);
	void SetPropInfo(int nPropIndex, CString strPropName);

	//-- 2011-6-21 Lee JungTaek
	//-- Set Indi Position
	void SetPos(BOOL bMouseWheelUp, BOOL bSet = TRUE);
	void SetSceneModePos(BOOL bMouseWheelUp);
	void SetMagicFrameModePos(BOOL bMouseWheelUp);
	void SetSmartFilterModePos(BOOL bMouseWheelUp);

	//-- 2011-6-21 Lee JungTaek
	void SetModeSelectSelected(BOOL bSelected) {m_bSelected = bSelected; Invalidate(FALSE);}
	void SetPropValue(int nPropValue);
	void SetSceneModeValueIndex(int nPropValue);
	void SetMagicFrameModeValueIndex(int nPropValue);
	void SetSmartFilterModeValueIndex(int nPropValue);
	
	CString GetSceneModeValue(int nIndex);
	CString GetSceneModeImage(int nPropValue);
	
	CString GetMagicFrameModeValue(int nIndex);
	CString GetMagicFrameModeImage(int nPropValue);

	CString GetSmartFilterModeValue(int nIndex);
	CString GetSmartFilterModeImage(int nPropValue);

	CString GetPropDesc();

	//-- 2011-9-22 Lee JungTaek
	int GetPropIndex()	{return m_nPropIndex;}
	int GetPropIndexValue()	{return m_nPropValueIndex;}
	CString GetPropName()	{return m_strPropName;}
	CString GetPropStrValue(int nIndex);
	int GetPropListCnt();

	//CMiLRe 20140901 Model Name Get/Set
	CString GetModel() {return m_strModel;}
	void SetModel(CString strModel) { m_strModel = strModel;}
};
