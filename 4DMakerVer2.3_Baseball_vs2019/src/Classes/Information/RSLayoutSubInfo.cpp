/////////////////////////////////////////////////////////////////////////////
//
//  CRSLayoutSubInfo.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Ini.h"
#include "RSLayoutSubInfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRSLayoutSubInfo::CRSLayoutSubInfo(void)
{
}


CRSLayoutSubInfo::~CRSLayoutSubInfo(void)
{
	if(GetPopertyValueCount())
		RemovePopertyValue();
}

BOOL CRSLayoutSubInfo::RemovePopertyValue()
{
	CRSPropertyValue* pExist = NULL;
	int nAll = GetPopertyValueCount();

	while(nAll--)
	{
		pExist = (CRSPropertyValue*)GetPropertyValue(nAll);
		if(pExist)
		{
			delete pExist;
			pExist = NULL;
			m_arrPropertyValue.RemoveAt(nAll);
		}
		else
			return FALSE;
	}

	//-- CHECK REMOVE ALL
	m_arrPropertyValue.RemoveAll();
	return TRUE;
}

int CRSLayoutSubInfo::GetPopertyValueCount()
{
	return (int)m_arrPropertyValue.GetSize();
}

CRSPropertyValue* CRSLayoutSubInfo::GetPropertyValue(int nIndex)
{
	if(nIndex > -1 &&nIndex < GetPopertyValueCount())
		return (CRSPropertyValue*)m_arrPropertyValue.GetAt(nIndex);
	return NULL;
}

int CRSLayoutSubInfo::AddPopertyValue(CRSPropertyValue* pPropertyLayout)
{
	return (int)m_arrPropertyValue.Add((CObject*)pPropertyLayout);
}

void CRSLayoutSubInfo::LoadInfo(CString strFile, CString strModel, CString strPoperty)
{
	CIni ini;
	//-- Load Config File
	if(!ini.SetIniFilename(strFile))
		return;

	//-- Check Model First
	if(strModel == _T("NX200") || strModel == _T("NX1000") || strModel == _T("NX2000") || strModel == _T("NX1")) //-- NX1 2014-7-28 dh0.seo
	{
		CString strSection = _T("");
		strSection.Format(_T("%s/%s"), strModel, strPoperty);
			
		//-- Get Property Value Count
		int nValueCnt = ini.GetInt(strSection, INFO_PROPERTY_VALUE_COUNT);
		
		//-- Get Property Value
		for(int i = 0; i < nValueCnt; i++)
		{
			//-- Get Value and Image Info
			strSection.Format(_T("%s/%s/%d"), strModel, strPoperty, i+1);

			CString strValue = _T("");
			CString strImagePathN, strImagePathS, strImagePathD;

			//-- Get Property Value
			strValue = ini.GetString(strSection, INFO_PROPERTY_VALUE_NAME);
			strImagePathN = ini.GetString(strSection, INFO_PROPERTY_IMAGE_NORMAL);
			strImagePathS = ini.GetString(strSection, INFO_PROPERTY_IMAGE_SELECT);
			strImagePathD = ini.GetString(strSection, INFO_PROPERTY_IMAGE_DISABLE);

			//-- Get Type and Layout Info
			CRect rectItem;
			int nType = -1;

			nType = ini.GetInt(strSection, INFO_LAYOUT_TYPE);
			rectItem.left = ini.GetInt(strSection, INFO_LAYOUT_X);
			rectItem.top = ini.GetInt(strSection, INFO_LAYOUT_Y);
			rectItem.right = ini.GetInt(strSection, INFO_LAYOUT_W) + rectItem.left;
			rectItem.bottom = ini.GetInt(strSection, INFO_LAYOUT_H) + rectItem.top;				
			
			//-- Add Layout Object Array
			CRSPropertyValue* pPropertyLayout = new CRSPropertyValue(strValue, strImagePathN, strImagePathS, strImagePathD, nType, strPoperty, rectItem);
			AddPopertyValue(pPropertyLayout);
		}
	}
}