/////////////////////////////////////////////////////////////////////////////
//
//  CRSBankInfo.h
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author	Lee JungTaek
// @Date	2011-07-11
//
/////////////////////////////////////////////////////////////////////////////
#pragma once
#include "ptpindex.h"
#include "SmartPanelIndex.h"

class CRSBankInfo
{
public:
	CRSBankInfo(void);
	~CRSBankInfo(void);
};
