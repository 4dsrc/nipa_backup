/////////////////////////////////////////////////////////////////////////////
//
//  CRSLayoutInfo.cpp : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Ini.h"
#include "RSLayoutInfo.h"
#include "SmartPanelIndex.h"
#include "SdiDefines.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRSLayoutInfo::CRSLayoutInfo(void)
{
	m_nMode = -1;
}

CRSLayoutInfo::~CRSLayoutInfo(void)
{
	RemoveLayoutSubInfoClass();
 	RemoveLayoutInfo(); 	
}

BOOL CRSLayoutInfo::RemoveLayoutInfo()
{
	CRSPropertyLayout* pExist = NULL;
	int nAll = GetLayoutCount();

	while(nAll--)
	{
		pExist = (CRSPropertyLayout*)GetLayoutInfo(nAll);
		if(pExist)
		{
			delete pExist;
			pExist = NULL;
			m_arrLayout.RemoveAt(nAll);
		}
		else
			return FALSE;
	}

	//-- CHECK REMOVE ALL
	m_arrLayout.RemoveAll();
	return TRUE;
}

//-- To Get Control LayoutInfo
int CRSLayoutInfo::GetLayoutCount()
{
	return (int)m_arrLayout.GetSize();
}

CRSPropertyLayout* CRSLayoutInfo::GetLayoutInfo(int nIndex)
{
	if(nIndex < GetLayoutCount())
		return (CRSPropertyLayout*)m_arrLayout.GetAt (nIndex);
	return NULL;
}

int CRSLayoutInfo::AddLayoutInfo(CRSPropertyLayout* pPropertyLayout)
{
	return (int)m_arrLayout.Add((CObject*)pPropertyLayout);
}

//-- To RSLayoutSubInfo Class
BOOL CRSLayoutInfo::RemoveLayoutSubInfoClass()
{
	CRSLayoutSubInfo* pExist = NULL;
	int nAll = GetLayoutSubInfoClassCount();

	while(nAll--)
	{
		pExist = (CRSLayoutSubInfo*)GetLayoutSubClassInfo(nAll);
		if(pExist)
		{
			delete pExist;
			pExist = NULL;
			m_arrLayoutSub.RemoveAt(nAll);
		}
		else
			return FALSE;
	}

	//-- CHECK REMOVE ALL
	m_arrLayoutSub.RemoveAll();
	return TRUE;
}

int CRSLayoutInfo::GetLayoutSubInfoClassCount()
{
	return (int)m_arrLayoutSub.GetSize();
}

CRSLayoutSubInfo* CRSLayoutInfo::GetLayoutSubClassInfo(int nIndex)
{
	if(nIndex < GetLayoutCount())
		return (CRSLayoutSubInfo*)m_arrLayoutSub.GetAt(nIndex);
	return NULL;
}

int CRSLayoutInfo::AddLayoutSubClassInfo(CRSLayoutSubInfo* pLayoutSubInfo)
{
	return (int)m_arrLayoutSub.Add((CObject*)pLayoutSubInfo);
}

//------------------------------------------------------------------------------ 
//! @brief		LoadInfo
//! @date		2010-06-10
//! @author		Lee JungTaek
//------------------------------------------------------------------------------ 
void CRSLayoutInfo::LoadInfo(CString strFile, CString strModel, CString strMode)
{
	m_nMode = GetModeIndex(strMode);

	CString strIniMode = _T("");
	//-- Check Model First
	if(strModel == _T("NX200") || strModel == _T("NX1000") || strModel == _T("NX2000") || m_strModel == _T("NX1")) // || m_strModel == _T("NX1") //-- 2014-7-28 dh0.seo
	{
		switch(m_nMode)
		{
		case DSC_MODE_SMART					:
			{
				strIniMode = strMode;
				for(int i = 0; i < ARRAYSIZE(NX200SmartAutoItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SmartAutoItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SmartAutoItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}

			}
			break;
		case DSC_MODE_P						:	
		case DSC_MODE_A						:	
		case DSC_MODE_S						:	
		case DSC_MODE_M						:
		case DSC_MODE_BULB					: //dh0.seo 2015-01-12 위치 이동
			{
				strIniMode = _T("PASM");
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200PASMItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200PASMItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200PASMItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_I						:	break;	
		case DSC_MODE_CAPTURE_MOVIE			:	break;	
		case DSC_MODE_MAGIC_MAGIC_FRAME		:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200MagicFrameItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200MagicFrameItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200MagicFrameItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;									
		case DSC_MODE_MAGIC_SMART_FILTER	:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SmartFilterItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SmartFilterItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SmartFilterItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_PANORAMA				:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX2000ScenePanoramaItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX2000ScenePanoramaItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX2000ScenePanoramaItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_BEAUTY			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneBeautyItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneBeautyItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneBeautyItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_NIGHT			:
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneNightItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneNightItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneNightItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_LANDSCAPE		:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneLandscapeItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneLandscapeItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneLandscapeItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_PORTRAIT		:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200ScenePortraitItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200ScenePortraitItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200ScenePortraitItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_CHILDREN		:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneChildrenItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneChildrenItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneChildrenItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_SPORTS			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneSportsItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneSportsItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneSportsItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_CLOSE_UP		:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneCloseUpItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneCloseUpItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneCloseUpItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_TEXT			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneTextItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneTextItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneTextItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_SUNSET			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneSunsetItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneSunsetItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneSunsetItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_DAWN			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneDawnItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneDawnItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneDawnItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_BACKLIGHT		:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneBacklightItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneBacklightItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneBacklightItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_FIREWORK		:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneFireworkItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneFireworkItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneFireworkItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_BEACH_SNOW		:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneBeachSnowItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneBeachSnowItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneBeachSnowItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;	
		case DSC_MODE_SCENE_SOUND_PICTURE	:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200SceneSoundPictureItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200SceneSoundPictureItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200SceneSoundPictureItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}			
			break;	
		case DSC_MODE_SCENE_3D			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX200Scene3DItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX200Scene3DItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX200Scene3DItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_BEST			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX2000SceneBestItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX2000SceneBestItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX2000SceneBestItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_MACRO		:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX2000SceneMacroItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX2000SceneMacroItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX2000SceneMacroItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_ACTION			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX2000SceneActionItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX2000SceneActionItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX2000SceneActionItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_RICH			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX2000SceneRichItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX2000SceneRichItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX2000SceneRichItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_WATERFALL			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX2000SceneWaterfallItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX2000SceneWaterfallItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX2000SceneWaterfallItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_SILHOUETTE			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX2000SceneSilhouetteItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX2000SceneSilhouetteItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX2000SceneSilhouetteItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_LIGHT			:	
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX2000SceneLightItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX2000SceneLightItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX2000SceneLightItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_CREATIVE			:
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX2000SceneCreativeItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX2000SceneCreativeItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX2000SceneCreativeItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_PANORAMA		:
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX2000ScenePanoramaItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX2000ScenePanoramaItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX2000ScenePanoramaItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_ACTION_FREEZE	:
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX1SceneActionFreezeItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX1SceneActionFreezeItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX1SceneActionFreezeItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_LIGHT_TRACE		:
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX1SceneLightTraceItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX1SceneLightTraceItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX1SceneLightTraceItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_MULTI_EXPOSURE	:
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX1SceneMultiExposureItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX1SceneMultiExposureItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX1SceneMultiExposureItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_SCENE_AUTO_SHUTTER	:
			{
				strIniMode = strMode;
				//-- Load Model_Mode Item Information
				for(int i = 0; i < ARRAYSIZE(NX1SceneAutoShutterItemSet); i++)
				{
					//-- Load Item Rect
					CString strSection = _T("");
					strSection.Format(_T("%s/%s/%s"), strModel, strIniMode, NX1SceneAutoShutterItemSet[i].strProperty);

					//-- Set Item Name
					CString strProperty = NX1SceneAutoShutterItemSet[i].strProperty;
					LoadLayout(strFile, strModel, strSection, strProperty);
				}
			}
			break;
		case DSC_MODE_MOVIE					:	break;	
		}	
	}
}

//------------------------------------------------------------------------------ 
//! @brief		LoadLayout
//! @date		2011-7-22
//! @author		Lee JungTaek
//! @note	 	Load Common Function
//------------------------------------------------------------------------------ 
void CRSLayoutInfo::LoadLayout(CString strFile, CString strModel, CString strSection, CString strProperty)
{
	CIni ini;
	//-- Load Config File
	if(!ini.SetIniFilename(strFile))
		return;

	CRect rectProperty;
	int nType = -1;

	nType = ini.GetInt(strSection, INFO_LAYOUT_TYPE);
	rectProperty.left = ini.GetInt(strSection, INFO_LAYOUT_X);
	rectProperty.top = ini.GetInt(strSection, INFO_LAYOUT_Y);
	rectProperty.right = ini.GetInt(strSection, INFO_LAYOUT_W) + rectProperty.left;
	rectProperty.bottom = ini.GetInt(strSection, INFO_LAYOUT_H) + rectProperty.top;

	//-- Set Child Property Value (Sub Info)
	strFile.Format(_T("%s\\%s\\%s\\%s_%s.info"), RSGetRoot(), STR_CONFIG_LAYOUT_PATH, strModel, strModel, strProperty);
	CRSLayoutSubInfo* rsLayoutSubInfo = new CRSLayoutSubInfo;
	rsLayoutSubInfo->LoadInfo(strFile, strModel, strProperty);
	AddLayoutSubClassInfo(rsLayoutSubInfo);

	//-- Add Layout Object Array
	CRSPropertyLayout* pPropertyLayout = new CRSPropertyLayout(nType, strProperty, rectProperty);
	AddLayoutInfo(pPropertyLayout);
}

//------------------------------------------------------------------------------ 
//! @brief		DrawIcon
//! @date		2010-06-10
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CRSLayoutInfo::DrawIcon(ControlTypeInfo* pControlTypeInfo, int nPropIndex, CString strValue, int nStatus)
{
	//-- Drawed Button
	CRSButton* pRSButton = NULL;
	switch(pControlTypeInfo->nType)
	{
	case SMARTPANEL_CONTROL_BUTTON:
		//-- Get Button
		pRSButton = (CRSButton*)pControlTypeInfo->pRSCtrl;
		if(pRSButton)
			SetBtnInfo(pRSButton, nPropIndex, strValue, nStatus);
		break;
	case SMARTPANEL_CONTROL_BAROMETER:
	case SMARTPANEL_CONTROL_SLIDER:
	case SMARTPANEL_CONTROL_MODESELECT:
	case SMARTPANEL_CONTROL_MAGICMODE:
		break;
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		DrawIcon
//! @date		2010-06-10
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CRSLayoutInfo::DrawIcon(ControlTypeInfo* pControlTypeInfo, int nPropIndex, int nPropValue, int nStatus)
{
	//-- Drawed Button
	CRSButton* pRSButton = NULL;
	CRSSliderCtrl *pRSSlider = NULL;
	CRSBarometerCtrl *pRSBarometer = NULL;
	CRSModeSelectCtrl *pRSModeSelect = NULL;
	CRSMagicFilterSelectCtrl *pRSMagicMode = NULL;

	switch(pControlTypeInfo->nType)
	{
	case SMARTPANEL_CONTROL_BUTTON:
		//-- Get Button
		pRSButton = (CRSButton*)pControlTypeInfo->pRSCtrl;

		if(pRSButton)
			SetBtnInfo(pRSButton, nPropIndex, nPropValue, nStatus);
		break;
	case SMARTPANEL_CONTROL_BAROMETER:
		pRSBarometer = (CRSBarometerCtrl*)pControlTypeInfo->pRSCtrl;

		if(pRSBarometer)
		{
			pRSBarometer->SetPropValue(nPropValue);
			pRSBarometer->SetEnable(nStatus);

			//-- Set Enable
			if(nStatus)			pRSBarometer->EnableWindow(TRUE);
			else				pRSBarometer->EnableWindow(FALSE);

		}
		break;
	case SMARTPANEL_CONTROL_SLIDER:
		pRSSlider = (CRSSliderCtrl*)pControlTypeInfo->pRSCtrl;

		if(pRSSlider)
		{
			pRSSlider->SetEVValue(nPropValue);
			pRSSlider->SetEnable(nStatus);

			//-- Set Enable
			if(nStatus)			pRSSlider->EnableWindow(TRUE);
			else				pRSSlider->EnableWindow(FALSE);
		}
		break;
	case SMARTPANEL_CONTROL_MODESELECT:
		pRSModeSelect = (CRSModeSelectCtrl*)pControlTypeInfo->pRSCtrl;

		if(pRSModeSelect)
			pRSModeSelect->SetPropValue(nPropValue);
		break;
	case SMARTPANEL_CONTROL_MAGICMODE:
		pRSMagicMode = (CRSMagicFilterSelectCtrl *)pControlTypeInfo->pRSCtrl;

		if(pRSMagicMode)
			pRSMagicMode->SetPropValue(nPropValue);
		break;
	}	
}

//------------------------------------------------------------------------------ 
//! @brief		SetBtnInfo
//! @date		2011-06-10
//! @author	hongsu.jung
//! @note	 	Set Button Ctrl
//------------------------------------------------------------------------------ 
void CRSLayoutInfo::SetBtnInfo(CRSButton* ctrlButton, int nPropIndex, CString strValue, int nStatus)
{
	//-- Get Current Property
 	int nCurPropValue = ChangeStringValueToIndex(nPropIndex, strValue);
	SetBtnImageInfo(ctrlButton, nPropIndex, nCurPropValue, nStatus);
}

//------------------------------------------------------------------------------ 
//! @brief		SetBtnInfo
//! @date		2011-06-10
//! @author	hongsu.jung
//! @note	 	Set Button Status
//------------------------------------------------------------------------------ 
void CRSLayoutInfo::SetBtnInfo(CRSButton* ctrlButton, int nPropIndex, int nPropvalue, int nStatus)
{
	//-- Get Current Property
	int nCurPropValue = ChangeIntValueToIndex(nPropIndex, nPropvalue);
	SetBtnImageInfo(ctrlButton, nPropIndex, nCurPropValue, nStatus);
}

//------------------------------------------------------------------------------ 
//! @brief		SetBtnImageInfo
//! @date		2011-06-10
//! @author	hongsu.jung
//! @note	 	Set Button Image
//------------------------------------------------------------------------------ 
void CRSLayoutInfo::SetBtnImageInfo(CRSButton* ctrlButton, int nPropIndex, int nCurPropValue, int nStatus)
{
	if(!ctrlButton)
		return;

	//-- 2011-7-14 Lee JungTaek
	CString strValue = _T("");
	//dh0.seo Drive 변경 필요 2014-10-25
	if((m_nMode == 0 && nPropIndex == 0x02 && nCurPropValue == 2) || (m_nMode == 0x33 && nPropIndex == 0x07 && nCurPropValue == 2))
	{
		nStatus = PROPERTY_STATUS_DISABLE;
	}
	else if(nPropIndex == 0x0c && nCurPropValue == 0xb)
	{
		nStatus = PROPERTY_STATUS_DISABLE;
	}

	if(m_nMode == 5 && nPropIndex == 0x0f)
	{
		nStatus = PROPERTY_STATUS_DISABLE;
	}

	switch(nStatus)
	{
	case PROPERTY_STATUS_DISABLE:
		{
			ctrlButton->SetBtnStatus(BTN_BK_1ST_STATUS_DISABLE);
			ctrlButton->EnableWindow(FALSE);
		}
		break;
	case PROPERTY_STATUS_SELECTED:
		{
			ctrlButton->SetBtnStatus(BTN_BK_1ST_STATUS_FOCUS);
			ctrlButton->EnableWindow();
		}
		break;
	case PROPERTY_STATUS_NORMAL:
		{
			ctrlButton->SetBtnStatus(BTN_BK_1ST_STATUS_NORMAL);
			ctrlButton->EnableWindow();
		}
		break;	
	}
	
	CString strTemp = ChangeIndexToPropName(nPropIndex);
	//-- Set Image
	CString strImagePath = GetButtonImageByStatus(ChangeIndexToPropName(nPropIndex), nPropIndex, nCurPropValue, nStatus, strValue);
	ctrlButton->SetBitmaps(strImagePath);
	ctrlButton->m_strPropValue = strValue;

	//-- Set Data
	ctrlButton->m_strPropertyName = ChangeIndexToPropName(nPropIndex);
	ctrlButton->m_nPropertyValue = nCurPropValue;

	//-- 2014-09-30 joonho.kim
	//-- Select PhotoSize
	if(ctrlButton->m_strPropertyName.CompareNoCase(_T("PHOTO SIZE")) == 0)
	{
		RSEvent* pMsgToMain = new RSEvent();
		pMsgToMain->nParam1 = nCurPropValue;			
		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_RS, (WPARAM)WM_SDI_EVENT_SET_PHOTOSIZE, (LPARAM)pMsgToMain);
	}
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStringValueToIndex
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Change Property Value (ex.20M  -> 1, 16 -> 2..)
//------------------------------------------------------------------------------ 
CString CRSLayoutInfo::GetButtonImageByStatus(CString strPropName, int nCurPropIndex, int nCurPropValue, int nStatus, CString &strValue)
{
	CString strImagePath = _T("");
	strImagePath.Format(_T("%s\\%s\\%s\\"), RSGetRoot(), STR_IMAGE_SMARTPANNEL_ICON_PATH, strPropName);
	CRSLayoutSubInfo* pRSLayoutSubInfo = (CRSLayoutSubInfo*)GetLayoutSubClassInfo(nCurPropIndex);
	
	if(pRSLayoutSubInfo)
	{
		//-- If Property Exist
		if(pRSLayoutSubInfo->GetPopertyValueCount())
		{
			//-- Get Specific Property Value
			CRSPropertyValue* rsPropertyValue = (CRSPropertyValue*)pRSLayoutSubInfo->GetPropertyValue(nCurPropValue);

			if(rsPropertyValue)
			{
				//dh0.seo Drive 변경 필요 2014-10-25
				if(rsPropertyValue->m_strValue == _T("Continuous 6") || rsPropertyValue->m_strValue == _T("Continuous 4"))
					nStatus = PROPERTY_STATUS_DISABLE;
					strValue = rsPropertyValue->m_strValue;

				//-- Return ImagePath by Type
				switch(nStatus)
				{
				case PROPERTY_STATUS_SELECTED:
				case PROPERTY_STATUS_NORMAL:
					strImagePath.AppendFormat(_T("%s"), rsPropertyValue->m_strImagePathN); 
					break;
				case PROPERTY_STATUS_DISABLE:
					strImagePath.AppendFormat(_T("%s"), rsPropertyValue->m_strImagePathD);
					break;
				}
				return strImagePath;
			}	
			else
			{
				//-- 2011-8-11 Lee JungTaek
				//-- To Burst Size
				if(!strPropName.CompareNoCase(_T("PHOTO SIZE")))
				{
					if(nCurPropValue == DSC_SIZE_5M)
					{
						strImagePath.AppendFormat(_T("02_icon_photo_5m_dim.png"));
						return strImagePath;
					}
				}
			}
		}	
	}	

	return _T("");
}

//------------------------------------------------------------------------------ 
//! @brief		ChangePrePropImage
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Change Pre Selected Image (Blue -> Black)
//------------------------------------------------------------------------------ 
void CRSLayoutInfo::ChangePrePropImage(ControlTypeInfo* pControlTypeInfo, int nPrePropIndex)
{
	CString strImagePath = _T("");

	if(pControlTypeInfo)
	{
		switch(pControlTypeInfo->nType)
		{
		case SMARTPANEL_CONTROL_BUTTON:
			{
				CRSButton* pRSButton = (CRSButton*)pControlTypeInfo->pRSCtrl;

				CRSLayoutSubInfo* pRSLayoutSubInfo = GetLayoutSubClassInfo(nPrePropIndex);

				if(pRSLayoutSubInfo)
				{
					//-- If Property Exist
					if(pRSLayoutSubInfo->GetPopertyValueCount())
					{
						//-- Get Specific Property Value
						CRSPropertyValue* rsPropertyValue = (CRSPropertyValue*)pRSLayoutSubInfo->GetPropertyValue(pRSButton->m_nPropertyValue);

						//-- Set Image Info
						//-- 2011-7-19 Lee JungTaek
						//-- Code Sonar
						if(rsPropertyValue)					
							strImagePath.Format(_T("%s\\%s\\%s\\%s"), RSGetRoot(), STR_IMAGE_SMARTPANNEL_ICON_PATH, rsPropertyValue->m_strPropertyName, rsPropertyValue->m_strImagePathN);
					}

					//-- Set Button Ctrl
					if(pRSButton)
					{
						pRSButton->SetBitmaps(strImagePath);
						pRSButton->SetBtnStatus(BTN_BK_1ST_STATUS_NORMAL);
					}					
				}				
			}
			break;
		case SMARTPANEL_CONTROL_BAROMETER:
			{
				CRSBarometerCtrl* pRSBarometer = (CRSBarometerCtrl*)pControlTypeInfo->pRSCtrl;
				if(pRSBarometer)	pRSBarometer->SetBarometerSelected(FALSE);
			}
			break;
		case SMARTPANEL_CONTROL_SLIDER:
			{
				CRSSliderCtrl* pRSSlider = (CRSSliderCtrl*)pControlTypeInfo->pRSCtrl;
				if(pRSSlider)		pRSSlider->SetSliderSelected(FALSE);
			}
			break;
		case SMARTPANEL_CONTROL_MODESELECT:
			{
				CRSModeSelectCtrl* pRSModeSelect = (CRSModeSelectCtrl*)pControlTypeInfo->pRSCtrl;
				if(pRSModeSelect)	pRSModeSelect->SetModeSelectSelected(FALSE);
			}
			break;
		case SMARTPANEL_CONTROL_MAGICMODE:
			{
				CRSMagicFilterSelectCtrl* pRSMagicMode = (CRSMagicFilterSelectCtrl*)pControlTypeInfo->pRSCtrl;
				if(pRSMagicMode)	pRSMagicMode->SetMagicFilterSelected(FALSE);
			}
			break;
		}
	}				
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStringValueToIndex
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Change Property Value (ex.20M  -> 1, 16 -> 2..)
//------------------------------------------------------------------------------ 
int CRSLayoutInfo::ChangeStringValueToIndex(int nPropIndex, CString strPropValue)
{
	int nValueIndex = 0;

	switch(m_nMode)
	{
	case DSC_MODE_P:
	case DSC_MODE_A:
	case DSC_MODE_S:
	case DSC_MODE_M:
	case DSC_MODE_BULB					: //dh0.seo 2015-01-12 위치 이동
		{
			switch(nPropIndex)
			{
			case DSC_PASM_PHOTOSIZE:
				{
					//NX1 dh0.seo 2014-08-13
					if(m_strModel == _T("NX1"))
					{
						for(int i = 0; i < ARRAYSIZE(SZNX1PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX1PropSizeValue[i].strPropValue) == 0)
							{
								nValueIndex = SZNX1PropSizeValue[i].nIndex;
								break;
							}
						}
					}
					else
					{ 
						for(int i = 0; i < ARRAYSIZE(SZNX200PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX200PropSizeValue[i].strPropValue) == 0)
							{
								nValueIndex = SZNX200PropSizeValue[i].nIndex;
								break;
							}
						}
					}
				}
				break;		
			}
		}
		break;
	case DSC_MODE_SMART:
		{
			switch(nPropIndex)
			{
			case DSC_SMARTAUTO_PHOTOSIZE:
				{
					//NX1 dh0.seo 2014-08-14
					if(m_strModel == _T("NX1"))
					{
						for(int i = 0; i < ARRAYSIZE(SZNX1PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX1PropSizeValue[i].strPropValue) == 0)
							{
 								nValueIndex = SZNX1PropSizeValue[i].nIndex;
								break;
							}
						}
					}
					else
					{ 
						for(int i = 0; i < ARRAYSIZE(SZNX200PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX200PropSizeValue[i].strPropValue) == 0)
							{
								nValueIndex = SZNX200PropSizeValue[i].nIndex;
								break;
							}
						}
					}
				}
				break;		
			}
		}
		break;
	case DSC_MODE_I						:			
	case DSC_MODE_CAPTURE_MOVIE			:		break;
	case DSC_MODE_MAGIC_MAGIC_FRAME		:
	case DSC_MODE_MAGIC_SMART_FILTER	:
		{
			switch(nPropIndex)
			{
			case DSC_MAGIC_FRAME_PHOTOSIZE:
				{
					for(int i = 0; i < ARRAYSIZE(SZNX200PropSizeValue); i++)
					{
						if(strPropValue.CompareNoCase(SZNX200PropSizeValue[i].strPropValue) == 0)
						{
							nValueIndex = SZNX200PropSizeValue[i].nIndex;
							break;
						}
					}
				}
				break;		
			}
		}
		break;
	case DSC_MODE_PANORAMA				:
	case DSC_MODE_SCENE_BEAUTY			:
	case DSC_MODE_SCENE_NIGHT			:
	case DSC_MODE_SCENE_LANDSCAPE		:
	case DSC_MODE_SCENE_PORTRAIT		:
	case DSC_MODE_SCENE_CHILDREN		:
	case DSC_MODE_SCENE_SPORTS			:
	case DSC_MODE_SCENE_CLOSE_UP		:
	case DSC_MODE_SCENE_TEXT			:
	case DSC_MODE_SCENE_SUNSET			:
	case DSC_MODE_SCENE_DAWN			:
	case DSC_MODE_SCENE_BACKLIGHT		:
	case DSC_MODE_SCENE_FIREWORK		:
	case DSC_MODE_SCENE_BEACH_SNOW		:
	case DSC_MODE_SCENE_SOUND_PICTURE	:
	case DSC_MODE_SCENE_3D				:
		{
			if(!m_strModel.Compare(_T("NX1")))
			{
				switch(nPropIndex)
				{
				case DSC_SCENE_BEAUTY_PHOTOSIZE:
					{
						for(int i = 0; i < ARRAYSIZE(SZNX1PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX1PropSizeValue[i].strPropValue) == 0)
							{
								nValueIndex = SZNX1PropSizeValue[i].nIndex;
								break;
							}
						}
					}
					break;		
				}
			}
			else
			{
				switch(nPropIndex)
				{
				case DSC_SCENE_BEAUTY_PHOTOSIZE:
					{
						for(int i = 0; i < ARRAYSIZE(SZNX200PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX200PropSizeValue[i].strPropValue) == 0)
							{
								nValueIndex = SZNX200PropSizeValue[i].nIndex;
								break;
							}
						}
					}
					break;		
				}
			}
		}
		break;
	// dh9.seo 2013-07-02
	case DSC_MODE_SCENE_BEST			:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_BEST_PHOTOSIZE:
				{
					for(int i = 0; i < ARRAYSIZE(SZNX200PropSizeValue); i++)
					{
						if(strPropValue.CompareNoCase(SZNX200PropSizeValue[i].strPropValue) == 0)
						{
							nValueIndex = SZNX200PropSizeValue[i].nIndex;
							break;
						}
					}
				}
				break;		
			}
		}
		break;
	case DSC_MODE_SCENE_ACTION			:
		{
			if(!m_strModel.Compare(_T("NX1")))
			{
				switch(nPropIndex)
				{
				case DSC_SCENE_ACTION_PHOTOSIZE:
					{
						for(int i = 0; i < ARRAYSIZE(SZNX1PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX1PropSizeValue[i].strPropValue) == 0)
							{
								nValueIndex = SZNX1PropSizeValue[i].nIndex;
								break;
							}
						}
					}
					break;		
				}
			}
			else
			{
				switch(nPropIndex)
				{
				case DSC_SCENE_ACTION_PHOTOSIZE:
					{
						for(int i = 0; i < ARRAYSIZE(SZNX200PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX200PropSizeValue[i].strPropValue) == 0)
							{
								nValueIndex = SZNX200PropSizeValue[i].nIndex;
								break;
							}
						}
					}
					break;		
				}
			}
		}
		break;
	case DSC_MODE_SCENE_MACRO			:
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL		:
	case DSC_MODE_SCENE_SILHOUETTE		:
	case DSC_MODE_SCENE_LIGHT			:
		{
			if(!m_strModel.Compare(_T("NX1")))
			{
				switch(nPropIndex)
				{
				case DSC_SCENE_MACRO_PHOTOSIZE:
					{
						for(int i = 0; i < ARRAYSIZE(SZNX1PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX1PropSizeValue[i].strPropValue) == 0)
							{
								nValueIndex = SZNX1PropSizeValue[i].nIndex;
								break;
							}
						}
					}
					break;		
				}
			}
			else
			{
				switch(nPropIndex)
				{
				case DSC_SCENE_MACRO_PHOTOSIZE:
					{
						for(int i = 0; i < ARRAYSIZE(SZNX200PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX200PropSizeValue[i].strPropValue) == 0)
							{
								nValueIndex = SZNX200PropSizeValue[i].nIndex;
								break;
							}
						}
					}
					break;		
				}
			}
		}
		break;
	case DSC_MODE_SCENE_CREATIVE		:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_CREATIVE_PHOTOSIZE:
				{
					for(int i = 0; i < ARRAYSIZE(SZNX200PropSizeValue); i++)
					{
						if(strPropValue.CompareNoCase(SZNX200PropSizeValue[i].strPropValue) == 0)
						{
							nValueIndex = SZNX200PropSizeValue[i].nIndex;
							break;
						}
					}
				}
				break;		
			}
		}
		break;
	case DSC_MODE_SCENE_PANORAMA		:
		{
			if(!m_strModel.Compare(_T("NX1")))
			{
				switch(nPropIndex)
				{
				case DSC_SCENE_BEAUTY_PHOTOSIZE:
					{
						for(int i = 0; i < ARRAYSIZE(SZNX1PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX1PropSizeValue[i].strPropValue) == 0)
							{
								nValueIndex = SZNX1PropSizeValue[i].nIndex;
								break;
							}
						}
					}
					break;		
				}
			}
			else
			{
				switch(nPropIndex)
				{
				case DSC_SCENE_BEAUTY_PHOTOSIZE:
					{
						for(int i = 0; i < ARRAYSIZE(SZNX200PropSizeValue); i++)
						{
							if(strPropValue.CompareNoCase(SZNX200PropSizeValue[i].strPropValue) == 0)
							{
								nValueIndex = SZNX200PropSizeValue[i].nIndex;
								break;
							}
						}
					}
					break;		
				}
			}
		}
		break;
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_ACTION_FREEZE_PHOTOSIZE:
				{
					for(int i = 0; i < ARRAYSIZE(SZNX1PropSizeValue); i++)
					{
						if(strPropValue.CompareNoCase(SZNX1PropSizeValue[i].strPropValue) == 0)
						{
							nValueIndex = SZNX1PropSizeValue[i].nIndex;
							break;
						}
					}
				}
				break;		
			}
		}
		break;
	case DSC_MODE_SCENE_LIGHT_TRACE	:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_LIGHT_TRACE_PHOTOSIZE:
				{
					for(int i = 0; i < ARRAYSIZE(SZNX1PropSizeValue); i++)
					{
						if(strPropValue.CompareNoCase(SZNX1PropSizeValue[i].strPropValue) == 0)
						{
							nValueIndex = SZNX1PropSizeValue[i].nIndex;
							break;
						}
					}
				}
				break;		
			}
		}
		break;
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_MULTI_EXPOSURE_PHOTOSIZE:
				{
					for(int i = 0; i < ARRAYSIZE(SZNX1PropSizeValue); i++)
					{
						if(strPropValue.CompareNoCase(SZNX1PropSizeValue[i].strPropValue) == 0)
						{
							nValueIndex = SZNX1PropSizeValue[i].nIndex;
							break;
						}
					}
				}
				break;		
			}
		}
		break;
	case DSC_MODE_SCENE_AUTO_SHUTTER	:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_AUTO_SHUTTER_PHOTOSIZE:
				{
					for(int i = 0; i < ARRAYSIZE(SZNX1PropSizeValue); i++)
					{
						if(strPropValue.CompareNoCase(SZNX1PropSizeValue[i].strPropValue) == 0)
						{
							nValueIndex = SZNX1PropSizeValue[i].nIndex;
							break;
						}
					}
				}
				break;		
			}
		}
		break;
	case DSC_MODE_MOVIE					:		break;
	}	

	return nValueIndex;
}

//------------------------------------------------------------------------------ 
//! @brief		ChangeModelEnum
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Set Current Property Value
//------------------------------------------------------------------------------ 
int CRSLayoutInfo::ChangeValuetoKey(int nIndex, int nPTPCode, int nEnum)
{
	CIni ini;

	int nValue = -1;
	CString strFile;
	CString strSection;
	CString strValue;

	strFile.Format(_T("%s\\%s\\%s\\CHANGE_ENUM.info"),RSGetRoot(), STR_CONFIG_ENUMVALUE_PATH, GetTargetModel());

	//-- Load Config File
	if(!ini.SetIniFilename(strFile))
		return nEnum;

	strSection.Format(_T("%x"),nPTPCode);
	strValue.Format(_T("%d"),nEnum);

	nValue = ini.GetIntValuetoKey(strSection, strValue, 99);

	if(nValue == 99)
		return nEnum;
	else
		return nValue;

}

//------------------------------------------------------------------------------ 
//! @brief		ChangeStringValueToIndex
//! @date		2011-05-30
//! @author		Lee JungTaek
//! @note	 	Change Property Value (ex.20M  -> 1, 16 -> 2..)
//------------------------------------------------------------------------------ 
int CRSLayoutInfo::ChangeIntValueToIndex(int nPropIndex, int nPropValue)
{
	int nValueIndex = 0;
	int nEnum = 0;

	switch(m_nMode)
	{
	case DSC_MODE_P:
	case DSC_MODE_A:
	case DSC_MODE_S:
	case DSC_MODE_M:
	case DSC_MODE_BULB					: //dh0.seo 2015-01-12 위치 이동
		{
			switch(nPropIndex)
			{
			case DSC_PASM_OIS:
				switch(nPropValue)
				{
				case eUCS_CAP_OIS_MODE1		:	nEnum = DSC_OIS_MODE1	;	break;
				case eUCS_CAP_OIS_MODE2		:	nEnum = DSC_OIS_MODE2	;	break;
				case eUCS_CAP_OIS_MODE_OFF	:	nEnum = DSC_OIS_OFF		;	break;
				default:						nEnum = DSC_OIS_NULL	;	break;
				}
				break;
			case DSC_PASM_FLASH:
				switch(nPropValue)
				{
				case eUCS_CAP_FLASH_OFF				:	nEnum =	DSC_FLASH_OFF			;	break;			
				case eUCS_CAP_FLASH_SMART			:	nEnum =	DSC_FLASH_SMART			;	break;
				case eUCS_CAP_FLASH_AUTO			:	nEnum =	DSC_FLASH_AUTO			;	break;
				case eUCS_CAP_FLASH_RED_EYE			:	nEnum =	DSC_FLASH_RED			;	break;
				case eUCS_CAP_FLASH_FILL_IN			:	nEnum =	DSC_FLASH_FILLIN		;	break;
				case eUCS_CAP_FLASH_FILL_IN_RED		:	nEnum =	DSC_FLASH_FILLIN_RED	;	break;
				case eUCS_CAP_FLASH_1ST_CURTAIN		:	nEnum =	DSC_FLASH_1ST_CURTAIN	;	break;
				case eUCS_CAP_FLASH_2ND_CURTAIN		:	nEnum =	DSC_FLASH_2ND_CURTAIN	;	break;
				default:								nEnum = DSC_FLASH_NULL			;	break;
				}
				break;
			case DSC_PASM_METERING:
				switch(nPropValue)
				{
				case eUCS_CAP_METERING_MULTI	:		nEnum = DSC_METERING_MULTI		;	break;
				case eUCS_CAP_METERING_CENTER	:		nEnum = DSC_METERING_CENTER		;	break;
				case eUCS_CAP_METERING_SPOT		:		nEnum = DSC_METERING_SPOT		;	break;
				default:								nEnum = DSC_METERING_NULL		;	break;
				}
				break;	
			case DSC_PASM_WHITEBALANCE:
				switch(nPropValue)
				{
				case eUCS_CAP_WB_AUTO				:	nEnum =	DSC_WB_AUTO				;	break;
				case eUCS_CAP_WB_AUTO_TUNGSTEN		:	nEnum = DSC_WB_AUTO_TUNGSTEN	;	break;
				case eUCS_CAP_WB_DAYLIGHT			:	nEnum =	DSC_WB_DAYLIGHT			;	break;
				case eUCS_CAP_WB_CLOUDY				:	nEnum =	DSC_WB_CLOUDY			;	break;
				case eUCS_CAP_WB_FLUORESCENT_W		:	nEnum =	DSC_WB_FLUORESCENT_W	;	break;
				case eUCS_CAP_WB_FLUORESCENT_NW		:	nEnum =	DSC_WB_FLUORESCENT_N	;	break;
				case eUCS_CAP_WB_FLUORESCENT_D		:	nEnum =	DSC_WB_FLUORESCENT_D	;	break;
				case eUCS_CAP_WB_TUNGSTEN			:	nEnum =	DSC_WB_TUNGSTEN			;	break;
				case eUCS_CAP_WB_FLASH				:	nEnum =	DSC_WB_FLASH			;	break;
				case eUCS_CAP_WB_CUSTOM				:	nEnum =	DSC_WB_CUSTOM			;	break;
				case eUCS_CAP_WB_K					:	nEnum =	DSC_WB_K				;	break;
				default:								nEnum =	DSC_WB_NULL				;	break;
				}
				break;
			case DSC_PASM_PICTUREWIZARD:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_PIC_WIZARD_OFF			:	nEnum = DSC_PICTUREWIZARD_OFF_NX1		;	break;	
					case eUCS_CAP_PIC_WIZARD_STANDARD		:	nEnum = DSC_PICTUREWIZARD_STANDARD_NX1	;	break;
					case eUCS_CAP_PIC_WIZARD_VIVID			:	nEnum = DSC_PICTUREWIZARD_VIVID_NX1		;	break;
					case eUCS_CAP_PIC_WIZARD_PORTRAIT		:	nEnum = DSC_PICTUREWIZARD_PORTRAIT_NX1	;	break;
					case eUCS_CAP_PIC_WIZARD_LANDSCAPE		:	nEnum = DSC_PICTUREWIZARD_LANDSCAPE_NX1	;	break;
					case eUCS_CAP_PIC_WIZARD_FOREST			:	nEnum = DSC_PICTUREWIZARD_FOREST_NX1	;	break;
					case eUCS_CAP_PIC_WIZARD_RETRO			:	nEnum = DSC_PICTUREWIZARD_RETRO_NX1		;	break;
					case eUCS_CAP_PIC_WIZARD_COOL			:	nEnum = DSC_PICTUREWIZARD_COOL_NX1		;	break;
					case eUCS_CAP_PIC_WIZARD_CALM			:	nEnum = DSC_PICTUREWIZARD_CALM_NX1		;	break;
					case eUCS_CAP_PIC_WIZARD_CLASSIC		:	nEnum = DSC_PICTUREWIZARD_CLASSIC_NX1	;	break;
					case eUCS_CAP_PIC_WIZARD_CUSTOM1		:	nEnum = DSC_PICTUREWIZARD_CUSTOM1_NX1	;	break;	
					case eUCS_CAP_PIC_WIZARD_CUSTOM2		:	nEnum = DSC_PICTUREWIZARD_CUSTOM2_NX1	;	break;	
					case eUCS_CAP_PIC_WIZARD_CUSTOM3		:	nEnum = DSC_PICTUREWIZARD_CUSTOM3_NX1	;	break;	
					default:									nEnum =	DSC_PICTUREWIZARD_NULL_NX1		;	break;
					}
					break;
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_PIC_WIZARD_OFF			:	nEnum = DSC_PICTUREWIZARD_OFF		;	break;	
					case eUCS_CAP_PIC_WIZARD_STANDARD		:	nEnum = DSC_PICTUREWIZARD_STANDARD	;	break;
					case eUCS_CAP_PIC_WIZARD_VIVID			:	nEnum = DSC_PICTUREWIZARD_VIVID		;	break;
					case eUCS_CAP_PIC_WIZARD_PORTRAIT		:	nEnum = DSC_PICTUREWIZARD_PORTRAIT	;	break;
					case eUCS_CAP_PIC_WIZARD_LANDSCAPE		:	nEnum = DSC_PICTUREWIZARD_LANDSCAPE	;	break;
					case eUCS_CAP_PIC_WIZARD_FOREST			:	nEnum = DSC_PICTUREWIZARD_FOREST	;	break;
					case eUCS_CAP_PIC_WIZARD_RETRO			:	nEnum = DSC_PICTUREWIZARD_RETRO		;	break;
					case eUCS_CAP_PIC_WIZARD_COOL			:	nEnum = DSC_PICTUREWIZARD_COOL		;	break;
					case eUCS_CAP_PIC_WIZARD_CALM			:	nEnum = DSC_PICTUREWIZARD_CALM		;	break;
					case eUCS_CAP_PIC_WIZARD_CLASSIC		:	nEnum = DSC_PICTUREWIZARD_CLASSIC	;	break;
					case eUCS_CAP_PIC_WIZARD_CUSTOM1		:	nEnum = DSC_PICTUREWIZARD_CUSTOM1	;	break;	
					case eUCS_CAP_PIC_WIZARD_CUSTOM2		:	nEnum = DSC_PICTUREWIZARD_CUSTOM2	;	break;	
					case eUCS_CAP_PIC_WIZARD_CUSTOM3		:	nEnum = DSC_PICTUREWIZARD_CUSTOM3	;	break;	
					default:									nEnum =	DSC_PICTUREWIZARD_NULL		;	break;
					}
					break;
				}
				
			case DSC_PASM_COLORSPACE:
				switch(nPropValue)
				{
				case eUCS_CAP_COLOR_SPACE_SRGB		:	nEnum = DSC_COLORSPACE_SRGB	;	break;
				case eUCS_CAP_COLOR_SPACE_ADOBE_RGB	:	nEnum = DSC_COLORSPACE_ARGB	;	break;
				default:								nEnum =	DSC_COLORSPACE_NULL ;	break;
				}
				break;
			case DSC_PASM_SMARTRANGE:
				switch(nPropValue)
				{
				case eUCS_CAP_SMART_RANGE_OFF	:	nEnum = DSC_SMARTRANGE_OFF;		break;
				case eUCS_CAP_SMART_RANGE_ON	:	nEnum = DSC_SMARTRANGE_ON;		break;
				default:							nEnum =	DSC_SMARTRANGE_NULL;	break;
				}
				break;
			case DSC_PASM_AFMODE:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_MODE_SAF				:	nEnum =	DSC_AFMODE_SINGLE		;	break;
				case eUCS_CAP_AF_MODE_CAF				:	nEnum =	DSC_AFMODE_CONTINUOUS	;	break;
				case eUCS_CAP_AF_MODE_MF				:	nEnum =	DSC_AFMODE_MANUAL		;	break;
				case eUCS_CAP_AF_MODE_AAF				:	nEnum =	DSC_AFMODE_AUTOMATIC	;	break;
				default:									nEnum =	DSC_AFMODE_NULL			;	break;
				}
				break;
			case DSC_PASM_FOCUSAREA:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_AREA_SELECTION				:	nEnum =	DSC_FOCUSAREA_SELECTION	;	break;
				case eUCS_CAP_AF_AREA_MULTI					:	nEnum =	DSC_FOCUSAREA_MULTI		;	break;
				case eUCS_CAP_AF_AREA_FACE_DETECTION		:	nEnum =	DSC_FOCUSAREA_FD		;	break;
				case eUCS_CAP_AF_AREA_SELF_FACE_DETECTION	:	nEnum =	DSC_FOCUSAREA_SELFFD	;	break;
				default:										nEnum =	DSC_FOCUSAREA_NULL		;	break;
				}
				break;
			case DSC_PASM_DRIVE:
			//CMiLRe 20140902 DRIVER GET 설정
				if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
				{
					switch(nPropValue)
					{
						//case eUCS_CAP_DRIVE_OFF			:	nEnum = DSC_DRIVE_OFF			;	break;
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
					case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
				{
					switch(nPropValue)
					{
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE_NX1		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB_NX1			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH_NX1			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER_NX1;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				break;	
			case DSC_PASM_QUALITY:
				switch(nPropValue)
				{
				case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
				case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
				case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
				case eUCS_CAP_QUALITY_RAW			:	nEnum = DSC_QUALITY_RAW	;	break;
				case eUCS_CAP_QUALITY_RAW_S_FINE	:	nEnum = DSC_QUALITY_SF_RAW;	break;		
				case eUCS_CAP_QUALITY_RAW_FINE		:	nEnum = DSC_QUALITY_F_RAW	;	break;
				case eUCS_CAP_QUALITY_RAW_NORMAL	:	nEnum =	DSC_QUALITY_N_RAW	;	break;
				case eUCS_CAP_QUALITY_SRAW			:	nEnum =	DSC_QUALITY_SRAW	;	break;
				default:								nEnum = DSC_QUALITY_NULL;					break;
				}
				break;
			// dh9.seo 2013-06-21
			case DSC_PASM_SMARTFILTER:
				//CMiLRe 20140901 SmartFilter 모델 분리
				nEnum = GetSmartFilter_ModelType_RSLayoutInfo(nPropValue);
				break;
			}
		}
		break;
	case DSC_MODE_SMART:
		{
			switch(nPropIndex)
			{
			case DSC_SMARTAUTO_DRIVE:
				//CMiLRe 20140902 DRIVER GET 설정
				if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
				{
					switch(nPropValue)
					{
						//case eUCS_CAP_DRIVE_OFF			:	nEnum = DSC_DRIVE_OFF			;	break;
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
					case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
				{
					switch(nPropValue)
					{
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE_NX1		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB_NX1			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH_NX1			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER_NX1;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				break;
			case DSC_SMARTAUTO_FLASH:
				switch(nPropValue)
				{
				case eUCS_CAP_FLASH_OFF				:	nEnum =	DSC_FLASH_OFF			;	break;			
				case eUCS_CAP_FLASH_SMART			:	nEnum =	DSC_FLASH_SMART			;	break;
				case eUCS_CAP_FLASH_AUTO			:	nEnum =	DSC_FLASH_AUTO			;	break;
				case eUCS_CAP_FLASH_RED_EYE			:	nEnum =	DSC_FLASH_RED			;	break;
				case eUCS_CAP_FLASH_FILL_IN			:	nEnum =	DSC_FLASH_FILLIN		;	break;
				case eUCS_CAP_FLASH_FILL_IN_RED		:	nEnum =	DSC_FLASH_FILLIN_RED	;	break;
				case eUCS_CAP_FLASH_1ST_CURTAIN		:	nEnum =	DSC_FLASH_1ST_CURTAIN	;	break;
				case eUCS_CAP_FLASH_2ND_CURTAIN		:	nEnum =	DSC_FLASH_2ND_CURTAIN	;	break;
				default:								nEnum = DSC_FLASH_NULL			;	break;
				}
				break;	
			case DSC_SMARTAUTO_OIS:
				switch(nPropValue)
				{
				case eUCS_CAP_OIS_MODE1		:	nEnum = DSC_OIS_MODE1	;	break;
				case eUCS_CAP_OIS_MODE2		:	nEnum = DSC_OIS_MODE2	;	break;
				case eUCS_CAP_OIS_MODE_OFF	:	nEnum = DSC_OIS_OFF		;	break;
				default:						nEnum = DSC_OIS_NULL	;	break;
				}
				break;
			}
		}
		break;
	case DSC_MODE_I						:		break;
	case DSC_MODE_CAPTURE_MOVIE			:		break;
	case DSC_MODE_MAGIC_MAGIC_FRAME		:
		{
			switch(nPropIndex)
			{
			case DSC_MAGIC_FRAME_MAGICMODECHANGE	:
				switch(nPropValue)
				{
				case eUCS_CAP_MAGIC_MODE_MAGIC_FRMAE	:	nEnum = DSC_MAGIC_MODE_MAGIC_FRAME	;	break;
				case eUCS_CAP_MAGIC_MODE_SMART_FILTER	:	nEnum = DSC_MAGIC_MODE_SMART_FITLER	;	break;
				}
				break;
			case DSC_MAGIC_FRAME_MAGICTYPECHANGE	:
				switch(nPropValue)
				{
				case eUCS_CAP_MAGIC_FRAME_WALLART		:	nEnum = DSC_MAGIC_FRAME_WALLART		;	break;
				case eUCS_CAP_MAGIC_FRAME_OLDFILM		:	nEnum = DSC_MAGIC_FRAME_OLDFILM		;	break;
				case eUCS_CAP_MAGIC_FRAME_RIPPLE		:	nEnum = DSC_MAGIC_FRAME_RIPPLE		;	break;
				case eUCS_CAP_MAGIC_FRAME_FULLMOON		:	nEnum = DSC_MAGIC_FRAME_FULLMOON		;	break;
				case eUCS_CAP_MAGIC_FRAME_OLDRECORD		:	nEnum = DSC_MAGIC_FRAME_OLDRECORD	;	break;
				case eUCS_CAP_MAGIC_FRAME_MAGAZINE		:	nEnum = DSC_MAGIC_FRAME_MAGAZINE		;	break;
				case eUCS_CAP_MAGIC_FRAME_SUNNYDAY		:	nEnum = DSC_MAGIC_FRAME_SUNNYDAY		;	break;
				case eUCS_CAP_MAGIC_FRAME_CLASSICTV		:	nEnum = DSC_MAGIC_FRAME_CLASSICTV	;	break;
				case eUCS_CAP_MAGIC_FRAME_YESTERDAY		:	nEnum = DSC_MAGIC_FRAME_YESTERDAY	;	break;
				case eUCS_CAP_MAGIC_FRAME_HOLIDAY		:	nEnum = DSC_MAGIC_FRAME_HOLIDAY		;	break;
				case eUCS_CAP_MAGIC_FRAME_BILLBOARD1	:	nEnum = DSC_MAGIC_FRAME_BILLBOARD1	;	break;
				case eUCS_CAP_MAGIC_FRAME_BILLBOARD2	:	nEnum = DSC_MAGIC_FRAME_BILLBOARD2	;	break;
				case eUCS_CAP_MAGIC_FRAME_NEWSPAPER		:	nEnum = DSC_MAGIC_FRAME_NEWSPAPER	;	break;
				}	
				break;
			case DSC_MAGIC_FRAME_AFMODE:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_MODE_SAF				:	nEnum =	DSC_AFMODE_SINGLE		;	break;
				case eUCS_CAP_AF_MODE_CAF				:	nEnum =	DSC_AFMODE_CONTINUOUS	;	break;
				case eUCS_CAP_AF_MODE_MF				:	nEnum =	DSC_AFMODE_MANUAL		;	break;
				default:									nEnum =	DSC_AFMODE_NULL			;	break;
				}
				break;
			case DSC_MAGIC_FRAME_COLORSPACE:
				switch(nPropValue)
				{
				case eUCS_CAP_COLOR_SPACE_SRGB		:	nEnum = DSC_COLORSPACE_SRGB	;	break;
				case eUCS_CAP_COLOR_SPACE_ADOBE_RGB	:	nEnum = DSC_COLORSPACE_ARGB	;	break;
				default:								nEnum =	DSC_COLORSPACE_NULL ;	break;
				}
				break;
			case DSC_MAGIC_FRAME_QUALITY:
				switch(nPropValue)
				{
				case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
				case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
				case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
				case eUCS_CAP_QUALITY_RAW			:	nEnum = DSC_QUALITY_RAW	;	break;
				case eUCS_CAP_QUALITY_RAW_S_FINE	:	nEnum = DSC_QUALITY_SF_RAW;	break;		
				case eUCS_CAP_QUALITY_RAW_FINE		:	nEnum = DSC_QUALITY_F_RAW	;	break;
				case eUCS_CAP_QUALITY_RAW_NORMAL	:	nEnum =	DSC_QUALITY_N_RAW	;	break;
				case eUCS_CAP_QUALITY_SRAW			:	nEnum =	DSC_QUALITY_SRAW	;	break;
				default:								nEnum = DSC_QUALITY_NULL;					break;
				}
				break;
			case DSC_MAGIC_FRAME_DRIVE:
				//CMiLRe 20140902 DRIVER GET 설정
				if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
				{
					switch(nPropValue)
					{
						//case eUCS_CAP_DRIVE_OFF			:	nEnum = DSC_DRIVE_OFF			;	break;
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
					case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
				{
					switch(nPropValue)
					{
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE_NX1		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB_NX1			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH_NX1			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER_NX1;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				break;	
			case DSC_MAGIC_FRAME_OIS:
				switch(nPropValue)
				{
				case eUCS_CAP_OIS_MODE1		:	nEnum = DSC_OIS_MODE1	;	break;
				case eUCS_CAP_OIS_MODE2		:	nEnum = DSC_OIS_MODE2	;	break;
				case eUCS_CAP_OIS_MODE_OFF	:	nEnum = DSC_OIS_OFF		;	break;
				default:						nEnum = DSC_OIS_NULL	;	break;
				}
				break;
			}
		}
		break;
	case DSC_MODE_MAGIC_SMART_FILTER	:
		{
			switch(nPropIndex)
			{
			case DSC_SMART_FILTER_MAGICMODECHANGE	:
				switch(nPropValue)
				{
				case eUCS_CAP_MAGIC_MODE_MAGIC_FRMAE	:	nEnum = DSC_MAGIC_MODE_MAGIC_FRAME	;	break;
				case eUCS_CAP_MAGIC_MODE_SMART_FILTER	:	nEnum = DSC_MAGIC_MODE_SMART_FITLER	;	break;
				}
				break;
			case DSC_SMART_FILTER_MAGICTYPECHANGE	:
				//CMiLRe 20140901 SmartFilter 모델 분리
				switch(nPropValue)
				{
				//case	eUCS_CAP_SMART_FILTER_PHOTO_NORMAL		 :	nEnum = DSC_SMARTFILTER_NORMAL		;	break;
				case	eUCS_CAP_SMART_FILTER_PHOTO_VIGNETTING	 :	nEnum = DSC_SMARTFILTER_VIGNETTING_NX2000	;	break;
				case	eUCS_CAP_SMART_FILTER_PHOTO_MINIATURE	 :	nEnum = DSC_SMARTFILTER_MINIATURE_NX2000	;	break;
				case	eUCS_CAP_SMART_FILTER_PHOTO_FISH_EYE	 :	nEnum = DSC_SMARTFILTER_FISHEYE_NX2000		;	break;
				case	eUCS_CAP_SMART_FILTER_PHOTO_SKETCH		 :	nEnum = DSC_SMARTFILTER_SKETCH_NX2000		;	break;
				case	eUCS_CAP_SMART_FILTER_PHOTO_DEFOG		 :	nEnum = DSC_SMARTFILTER_DEFOG_NX2000		;	break;
				case	eUCS_CAP_SMART_FILTER_PHOTO_HALF_TONE	 :	nEnum = DSC_SMARTFILTER_HALFTONE_NX2000	;	break;
				case	eUCS_CAP_SMART_FILTER_PHOTO_SOFT_FOCUS	 :	nEnum = DSC_SMARTFILTER_SOFTFOCUS_NX2000	;	break;
				case	eUCS_CAP_SMART_FILTER_PHOTO_OLD_FILM1	 :	nEnum = DSC_SMARTFILTER_OLDFILM1_NX2000	;	break;
				case	eUCS_CAP_SMART_FILTER_PHOTO_OLD_FILM2	 :	nEnum = DSC_SMARTFILTER_OLDFILM2_NX2000	;	break;
				case	eUCS_CAP_SMART_FILTER_PHOTO_NEGATIVE	 :	nEnum = DSC_SMARTFILTER_NEGATIVE_NX2000	;	break;
				}	
				break;
			case DSC_SMART_FILTER_AFMODE:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_MODE_SAF				:	nEnum =	DSC_AFMODE_SINGLE		;	break;
				case eUCS_CAP_AF_MODE_CAF				:	nEnum =	DSC_AFMODE_CONTINUOUS	;	break;
				case eUCS_CAP_AF_MODE_MF				:	nEnum =	DSC_AFMODE_MANUAL		;	break;
				default:									nEnum =	DSC_AFMODE_NULL			;	break;
				}
				break;
			case DSC_SMART_FILTER_COLORSPACE:
				switch(nPropValue)
				{
				case eUCS_CAP_COLOR_SPACE_SRGB		:	nEnum = DSC_COLORSPACE_SRGB	;	break;
				case eUCS_CAP_COLOR_SPACE_ADOBE_RGB	:	nEnum = DSC_COLORSPACE_ARGB	;	break;
				default:								nEnum =	DSC_COLORSPACE_NULL ;	break;
				}
				break;
			case DSC_SMART_FILTER_QUALITY:
				switch(nPropValue)
				{
				case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
				case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
				case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
				case eUCS_CAP_QUALITY_RAW			:	nEnum = DSC_QUALITY_RAW	;	break;
				case eUCS_CAP_QUALITY_RAW_S_FINE	:	nEnum = DSC_QUALITY_SF_RAW;	break;		
				case eUCS_CAP_QUALITY_RAW_FINE		:	nEnum = DSC_QUALITY_F_RAW	;	break;
				case eUCS_CAP_QUALITY_RAW_NORMAL	:	nEnum =	DSC_QUALITY_N_RAW	;	break;
				case eUCS_CAP_QUALITY_SRAW			:	nEnum =	DSC_QUALITY_SRAW	;	break;
				default:								nEnum = DSC_QUALITY_NULL;					break;
				}
				break;
			case DSC_SMART_FILTER_DRIVE:
				switch(nPropValue)
				{
				//case eUCS_CAP_DRIVE_OFF				:	nEnum = DSC_DRIVE_OFF			;	break;
				case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
				case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
				case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
				case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
				case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
				case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
				case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
				default:								nEnum = DSC_DRIVE_NULL			;	break;
				}																			
				break;	
			case DSC_SMART_FILTER_OIS:
				switch(nPropValue)
				{
				case eUCS_CAP_OIS_MODE1		:	nEnum = DSC_OIS_MODE1	;	break;
				case eUCS_CAP_OIS_MODE2		:	nEnum = DSC_OIS_MODE2	;	break;
				case eUCS_CAP_OIS_MODE_OFF	:	nEnum = DSC_OIS_OFF		;	break;
				default:						nEnum = DSC_OIS_NULL	;	break;
				}
				break;
			}
		}
		break;
	case DSC_MODE_SCENE_BEAUTY			:		
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_BEAUTY_SCENECHANGE	:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH_NX1				;	break;
					case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	nEnum = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
					case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	nEnum = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
					case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	nEnum = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
					case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	nEnum = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
					}											
					break;
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY			;	break;
					case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT			;	break;
					case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE		;	break;
					case eUCS_CAP_SCENE_MODE_PORTRAIT		:	nEnum = DSC_SCENE_MODE_PORTRAIT			;	break;
					case eUCS_CAP_SCENE_MODE_CHILDREN		:	nEnum = DSC_SCENE_MODE_CHILDREN			;	break;
					case eUCS_CAP_SCENE_MODE_SPORTS			:	nEnum = DSC_SCENE_MODE_SPORTS			;	break;
					case eUCS_CAP_SCENE_MODE_CLOSE_UP		:	nEnum = DSC_SCENE_MODE_CLOSEUP			;	break;
					case eUCS_CAP_SCENE_MODE_TEXT			:	nEnum = DSC_SCENE_MODE_TEXT				;	break;
					case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET			;	break;
					case eUCS_CAP_SCENE_MODE_DAWN			:	nEnum = DSC_SCENE_MODE_DAWN				;	break;
					case eUCS_CAP_SCENE_MODE_BACKLIGHT		:	nEnum = DSC_SCENE_MODE_BACKLIGHT		;	break;
					case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK			;	break;
					case eUCS_CAP_SCENE_MODE_BEACH_SNOW		:	nEnum = DSC_SCENE_MODE_BEACH_SNOW		;	break;
					case eUCS_CAP_SCENE_MODE_SOUND_PICTURE	:	nEnum = DSC_SCENE_MODE_SOUND_PICTURE	;	break;
					case eUCS_CAP_SCENE_MODE_3D				:	nEnum = DSC_SCENE_MODE_3D				;	break;
					case eUCS_CAP_SCENE_MODE_SMART_FILTER	:	nEnum = DSC_SCENE_MODE_SMARTFILTER		;	break;
						// dh9.seo 2013-06-24
					case eUCS_CAP_SCENE_MODE_BEST			:	nEnum = DSC_SCENE_MODE_BEST				;	break;
					case eUCS_CAP_SCENE_MODE_MACRO			:	nEnum = DSC_SCENE_MODE_MACRO			;	break;
					case eUCS_CAP_SCENE_MODE_ACTION			:	nEnum = DSC_SCENE_MODE_ACTION			;	break;
					case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH				;	break;
					case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL		;	break;
					case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE		;	break;
					case eUCS_CAP_SCENE_MODE_LIGHT			:	nEnum = DSC_SCENE_MODE_LIGHT			;	break;
					case eUCS_CAP_SCENE_MODE_CREATIVE		:	nEnum = DSC_SCENE_MODE_CREATIVE			;	break;
					case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA			;	break;
					}											
					break;
				}
			case DSC_SCENE_BEAUTY_AFMODE:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_MODE_SAF				:	nEnum =	DSC_AFMODE_SINGLE		;	break;
				case eUCS_CAP_AF_MODE_CAF				:	nEnum =	DSC_AFMODE_CONTINUOUS	;	break;
				case eUCS_CAP_AF_MODE_MF				:	nEnum =	DSC_AFMODE_MANUAL		;	break;
				default:									nEnum =	DSC_AFMODE_NULL			;	break;
				}
				break;
			case DSC_SCENE_BEAUTY_FOCUSAREA:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_AREA_SELECTION				:	nEnum =	DSC_FOCUSAREA_SELECTION	;	break;
				case eUCS_CAP_AF_AREA_MULTI					:	nEnum =	DSC_FOCUSAREA_MULTI		;	break;
				case eUCS_CAP_AF_AREA_FACE_DETECTION		:	nEnum =	DSC_FOCUSAREA_FD		;	break;
				case eUCS_CAP_AF_AREA_SELF_FACE_DETECTION	:	nEnum =	DSC_FOCUSAREA_SELFFD	;	break;
				default:										nEnum =	DSC_FOCUSAREA_NULL		;	break;
				}
				break;
			case DSC_SCENE_BEAUTY_QUALITY:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					case eUCS_CAP_QUALITY_RAW			:	nEnum = DSC_QUALITY_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_S_FINE	:	nEnum = DSC_QUALITY_SF_RAW;	break;		
					case eUCS_CAP_QUALITY_RAW_FINE		:	nEnum = DSC_QUALITY_F_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_NORMAL	:	nEnum =	DSC_QUALITY_N_RAW	;	break;
					case eUCS_CAP_QUALITY_SRAW			:	nEnum =	DSC_QUALITY_SRAW	;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
			case DSC_SCENE_BEAUTY_DRIVE:
				//CMiLRe 20140902 DRIVER GET 설정
				if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
				{
					switch(nPropValue)
					{
						//case eUCS_CAP_DRIVE_OFF			:	nEnum = DSC_DRIVE_OFF			;	break;
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
					case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
				{
					switch(nPropValue)
					{
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE_NX1		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB_NX1			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH_NX1			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER_NX1;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				break;	
			case DSC_SCENE_BEAUTY_COLORSPACE:
				switch(nPropValue)
				{
				case eUCS_CAP_COLOR_SPACE_SRGB		:	nEnum = DSC_COLORSPACE_SRGB	;	break;
				case eUCS_CAP_COLOR_SPACE_ADOBE_RGB	:	nEnum = DSC_COLORSPACE_ARGB	;	break;
				default:								nEnum =	DSC_COLORSPACE_NULL ;	break;
				}
				break;
			case DSC_SCENE_BEAUTY_OIS:
				switch(nPropValue)
				{
				case eUCS_CAP_OIS_MODE1		:	nEnum = DSC_OIS_MODE1	;	break;
				case eUCS_CAP_OIS_MODE2		:	nEnum = DSC_OIS_MODE2	;	break;
				case eUCS_CAP_OIS_MODE_OFF	:	nEnum = DSC_OIS_OFF		;	break;
				default:						nEnum = DSC_OIS_NULL	;	break;
				}
				break;
			case DSC_SCENE_BEAUTY_FLASH:
				switch(nPropValue)
				{
				case eUCS_CAP_FLASH_OFF				:	nEnum =	DSC_FLASH_OFF			;	break;			
				case eUCS_CAP_FLASH_SMART			:	nEnum =	DSC_FLASH_SMART			;	break;
				case eUCS_CAP_FLASH_AUTO			:	nEnum =	DSC_FLASH_AUTO			;	break;
				case eUCS_CAP_FLASH_RED_EYE			:	nEnum =	DSC_FLASH_RED			;	break;
				case eUCS_CAP_FLASH_FILL_IN			:	nEnum =	DSC_FLASH_FILLIN		;	break;
				case eUCS_CAP_FLASH_FILL_IN_RED		:	nEnum =	DSC_FLASH_FILLIN_RED	;	break;
				case eUCS_CAP_FLASH_1ST_CURTAIN		:	nEnum =	DSC_FLASH_1ST_CURTAIN	;	break;
				case eUCS_CAP_FLASH_2ND_CURTAIN		:	nEnum =	DSC_FLASH_2ND_CURTAIN	;	break;
				default:								nEnum = DSC_FLASH_NULL			;	break;
				}
				break;
			case  DSC_SCENE_BEAUTY_FACETONE:
				switch(nPropValue)
				{
				case eUCS_CAP_FACE_TONE_1	:	nEnum =	DSC_SCENE_FACE_TONE_1		;	break;			
				case eUCS_CAP_FACE_TONE_2	:	nEnum =	DSC_SCENE_FACE_TONE_2		;	break;
				case eUCS_CAP_FACE_TONE_3	:	nEnum =	DSC_SCENE_FACE_TONE_3		;	break;
				}
				break;
			case  DSC_SCENE_BEAUTY_FACERETOUCH:
				switch(nPropValue)
				{
				case eUCS_CAP_FACE_RETOUCH_1	:	nEnum =	DSC_SCENE_FACE_RETOUCH_1		;	break;			
				case eUCS_CAP_FACE_RETOUCH_2	:	nEnum =	DSC_SCENE_FACE_RETOUCH_2		;	break;
				case eUCS_CAP_FACE_RETOUCH_3	:	nEnum =	DSC_SCENE_FACE_RETOUCH_3		;	break;
				}
				break;
			}
		}
		break;
	case DSC_MODE_SCENE_PORTRAIT		:
	case DSC_MODE_SCENE_NIGHT			:		
	case DSC_MODE_SCENE_CHILDREN		:
	case DSC_MODE_SCENE_SPORTS			:
	case DSC_MODE_SCENE_BACKLIGHT		:
	case DSC_MODE_SCENE_BEACH_SNOW		:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_PORTRAIT_SCENECHANGE	:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH_NX1				;	break;
					case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	nEnum = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
					case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	nEnum = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
					case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	nEnum = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
					case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	nEnum = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
					}											
					break;
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY			;	break;
					case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT			;	break;
					case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE		;	break;
					case eUCS_CAP_SCENE_MODE_PORTRAIT		:	nEnum = DSC_SCENE_MODE_PORTRAIT			;	break;
					case eUCS_CAP_SCENE_MODE_CHILDREN		:	nEnum = DSC_SCENE_MODE_CHILDREN			;	break;
					case eUCS_CAP_SCENE_MODE_SPORTS			:	nEnum = DSC_SCENE_MODE_SPORTS			;	break;
					case eUCS_CAP_SCENE_MODE_CLOSE_UP		:	nEnum = DSC_SCENE_MODE_CLOSEUP			;	break;
					case eUCS_CAP_SCENE_MODE_TEXT			:	nEnum = DSC_SCENE_MODE_TEXT				;	break;
					case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET			;	break;
					case eUCS_CAP_SCENE_MODE_DAWN			:	nEnum = DSC_SCENE_MODE_DAWN				;	break;
					case eUCS_CAP_SCENE_MODE_BACKLIGHT		:	nEnum = DSC_SCENE_MODE_BACKLIGHT		;	break;
					case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK			;	break;
					case eUCS_CAP_SCENE_MODE_BEACH_SNOW		:	nEnum = DSC_SCENE_MODE_BEACH_SNOW		;	break;
					case eUCS_CAP_SCENE_MODE_SOUND_PICTURE	:	nEnum = DSC_SCENE_MODE_SOUND_PICTURE	;	break;
					case eUCS_CAP_SCENE_MODE_3D				:	nEnum = DSC_SCENE_MODE_3D				;	break;
					case eUCS_CAP_SCENE_MODE_SMART_FILTER	:	nEnum = DSC_SCENE_MODE_SMARTFILTER		;	break;
						// dh9.seo 2013-06-24
					case eUCS_CAP_SCENE_MODE_BEST			:	nEnum = DSC_SCENE_MODE_BEST				;	break;
					case eUCS_CAP_SCENE_MODE_MACRO			:	nEnum = DSC_SCENE_MODE_MACRO			;	break;
					case eUCS_CAP_SCENE_MODE_ACTION			:	nEnum = DSC_SCENE_MODE_ACTION			;	break;
					case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH				;	break;
					case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL		;	break;
					case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE		;	break;
					case eUCS_CAP_SCENE_MODE_LIGHT			:	nEnum = DSC_SCENE_MODE_LIGHT			;	break;
					case eUCS_CAP_SCENE_MODE_CREATIVE		:	nEnum = DSC_SCENE_MODE_CREATIVE			;	break;
					case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA			;	break;
					}											
					break;
				}
			case DSC_SCENE_PORTRAIT_AFMODE:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_MODE_SAF				:	nEnum =	DSC_AFMODE_SINGLE		;	break;
				case eUCS_CAP_AF_MODE_CAF				:	nEnum =	DSC_AFMODE_CONTINUOUS	;	break;
				case eUCS_CAP_AF_MODE_MF				:	nEnum =	DSC_AFMODE_MANUAL		;	break;
				default:									nEnum =	DSC_AFMODE_NULL			;	break;
				}
				break;
			case DSC_SCENE_PORTRAIT_FOCUSAREA:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_AREA_SELECTION				:	nEnum =	DSC_FOCUSAREA_SELECTION	;	break;
				case eUCS_CAP_AF_AREA_MULTI					:	nEnum =	DSC_FOCUSAREA_MULTI		;	break;
				case eUCS_CAP_AF_AREA_FACE_DETECTION		:	nEnum =	DSC_FOCUSAREA_FD		;	break;
				case eUCS_CAP_AF_AREA_SELF_FACE_DETECTION	:	nEnum =	DSC_FOCUSAREA_SELFFD	;	break;
				default:										nEnum =	DSC_FOCUSAREA_NULL		;	break;
				}
				break;
			case DSC_SCENE_PORTRAIT_QUALITY:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					case eUCS_CAP_QUALITY_RAW			:	nEnum = DSC_QUALITY_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_S_FINE	:	nEnum = DSC_QUALITY_SF_RAW;	break;		
					case eUCS_CAP_QUALITY_RAW_FINE		:	nEnum = DSC_QUALITY_F_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_NORMAL	:	nEnum =	DSC_QUALITY_N_RAW	;	break;
					case eUCS_CAP_QUALITY_SRAW			:	nEnum =	DSC_QUALITY_SRAW	;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
			case DSC_SCENE_PORTRAIT_DRIVE:
				//CMiLRe 20140902 DRIVER GET 설정
				if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
				{
					switch(nPropValue)
					{
						//case eUCS_CAP_DRIVE_OFF			:	nEnum = DSC_DRIVE_OFF			;	break;
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
					case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
				{
					switch(nPropValue)
					{
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE_NX1		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB_NX1			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH_NX1			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER_NX1;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				break;	
			case DSC_SCENE_PORTRAIT_COLORSPACE:
				switch(nPropValue)
				{
				case eUCS_CAP_COLOR_SPACE_SRGB		:	nEnum = DSC_COLORSPACE_SRGB	;	break;
				case eUCS_CAP_COLOR_SPACE_ADOBE_RGB	:	nEnum = DSC_COLORSPACE_ARGB	;	break;
				default:								nEnum =	DSC_COLORSPACE_NULL ;	break;
				}
				break;
			case DSC_SCENE_PORTRAIT_OIS:
				switch(nPropValue)
				{
				case eUCS_CAP_OIS_MODE1		:	nEnum = DSC_OIS_MODE1	;	break;
				case eUCS_CAP_OIS_MODE2		:	nEnum = DSC_OIS_MODE2	;	break;
				case eUCS_CAP_OIS_MODE_OFF	:	nEnum = DSC_OIS_OFF		;	break;
				default:						nEnum = DSC_OIS_NULL	;	break;
				}
				break;
			case DSC_SCENE_PORTRAIT_FLASH:
				switch(nPropValue)
				{
				case eUCS_CAP_FLASH_OFF				:	nEnum =	DSC_FLASH_OFF			;	break;			
				case eUCS_CAP_FLASH_SMART			:	nEnum =	DSC_FLASH_SMART			;	break;
				case eUCS_CAP_FLASH_AUTO			:	nEnum =	DSC_FLASH_AUTO			;	break;
				case eUCS_CAP_FLASH_RED_EYE			:	nEnum =	DSC_FLASH_RED			;	break;
				case eUCS_CAP_FLASH_FILL_IN			:	nEnum =	DSC_FLASH_FILLIN		;	break;
				case eUCS_CAP_FLASH_FILL_IN_RED		:	nEnum =	DSC_FLASH_FILLIN_RED	;	break;
				case eUCS_CAP_FLASH_1ST_CURTAIN		:	nEnum =	DSC_FLASH_1ST_CURTAIN	;	break;
				case eUCS_CAP_FLASH_2ND_CURTAIN		:	nEnum =	DSC_FLASH_2ND_CURTAIN	;	break;
				default:								nEnum = DSC_FLASH_NULL			;	break;
				}
				break;
			}
		}
		break;
	case DSC_MODE_SCENE_DAWN			:
	case DSC_MODE_SCENE_SUNSET			:
	case DSC_MODE_SCENE_TEXT			:
	case DSC_MODE_SCENE_CLOSE_UP		:
	case DSC_MODE_SCENE_LANDSCAPE		:
	case DSC_MODE_SCENE_3D				:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_DAWN_SCENECHANGE	:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH_NX1				;	break;
					case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	nEnum = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
					case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	nEnum = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
					case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	nEnum = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
					case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	nEnum = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
					}											
					break;
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY			;	break;
					case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT			;	break;
					case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE		;	break;
					case eUCS_CAP_SCENE_MODE_PORTRAIT		:	nEnum = DSC_SCENE_MODE_PORTRAIT			;	break;
					case eUCS_CAP_SCENE_MODE_CHILDREN		:	nEnum = DSC_SCENE_MODE_CHILDREN			;	break;
					case eUCS_CAP_SCENE_MODE_SPORTS			:	nEnum = DSC_SCENE_MODE_SPORTS			;	break;
					case eUCS_CAP_SCENE_MODE_CLOSE_UP		:	nEnum = DSC_SCENE_MODE_CLOSEUP			;	break;
					case eUCS_CAP_SCENE_MODE_TEXT			:	nEnum = DSC_SCENE_MODE_TEXT				;	break;
					case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET			;	break;
					case eUCS_CAP_SCENE_MODE_DAWN			:	nEnum = DSC_SCENE_MODE_DAWN				;	break;
					case eUCS_CAP_SCENE_MODE_BACKLIGHT		:	nEnum = DSC_SCENE_MODE_BACKLIGHT		;	break;
					case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK			;	break;
					case eUCS_CAP_SCENE_MODE_BEACH_SNOW		:	nEnum = DSC_SCENE_MODE_BEACH_SNOW		;	break;
					case eUCS_CAP_SCENE_MODE_SOUND_PICTURE	:	nEnum = DSC_SCENE_MODE_SOUND_PICTURE	;	break;
					case eUCS_CAP_SCENE_MODE_3D				:	nEnum = DSC_SCENE_MODE_3D				;	break;
					case eUCS_CAP_SCENE_MODE_SMART_FILTER	:	nEnum = DSC_SCENE_MODE_SMARTFILTER		;	break;
						// dh9.seo 2013-06-24
					case eUCS_CAP_SCENE_MODE_BEST			:	nEnum = DSC_SCENE_MODE_BEST				;	break;
					case eUCS_CAP_SCENE_MODE_MACRO			:	nEnum = DSC_SCENE_MODE_MACRO			;	break;
					case eUCS_CAP_SCENE_MODE_ACTION			:	nEnum = DSC_SCENE_MODE_ACTION			;	break;
					case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH				;	break;
					case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL		;	break;
					case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE		;	break;
					case eUCS_CAP_SCENE_MODE_LIGHT			:	nEnum = DSC_SCENE_MODE_LIGHT			;	break;
					case eUCS_CAP_SCENE_MODE_CREATIVE		:	nEnum = DSC_SCENE_MODE_CREATIVE			;	break;
					case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA			;	break;
					}											
					break;
				}
			case DSC_SCENE_DAWN_AFMODE:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_MODE_SAF				:	nEnum =	DSC_AFMODE_SINGLE		;	break;
				case eUCS_CAP_AF_MODE_CAF				:	nEnum =	DSC_AFMODE_CONTINUOUS	;	break;
				case eUCS_CAP_AF_MODE_MF				:	nEnum =	DSC_AFMODE_MANUAL		;	break;
				default:									nEnum =	DSC_AFMODE_NULL			;	break;
				}
				break;
			case DSC_SCENE_DAWN_FOCUSAREA:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_AREA_SELECTION				:	nEnum =	DSC_FOCUSAREA_SELECTION	;	break;
				case eUCS_CAP_AF_AREA_MULTI					:	nEnum =	DSC_FOCUSAREA_MULTI		;	break;
				case eUCS_CAP_AF_AREA_FACE_DETECTION		:	nEnum =	DSC_FOCUSAREA_FD		;	break;
				case eUCS_CAP_AF_AREA_SELF_FACE_DETECTION	:	nEnum =	DSC_FOCUSAREA_SELFFD	;	break;
				default:										nEnum =	DSC_FOCUSAREA_NULL		;	break;
				}
				break;
			case DSC_SCENE_DAWN_QUALITY:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					case eUCS_CAP_QUALITY_RAW			:	nEnum = DSC_QUALITY_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_S_FINE	:	nEnum = DSC_QUALITY_SF_RAW;	break;		
					case eUCS_CAP_QUALITY_RAW_FINE		:	nEnum = DSC_QUALITY_F_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_NORMAL	:	nEnum =	DSC_QUALITY_N_RAW	;	break;
					case eUCS_CAP_QUALITY_SRAW			:	nEnum =	DSC_QUALITY_SRAW	;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
			case DSC_SCENE_DAWN_DRIVE:
				//CMiLRe 20140902 DRIVER GET 설정
				if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
				{
					switch(nPropValue)
					{
						//case eUCS_CAP_DRIVE_OFF			:	nEnum = DSC_DRIVE_OFF			;	break;
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
					case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
				{
					switch(nPropValue)
					{
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE_NX1		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB_NX1			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH_NX1			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER_NX1;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				break;	
			case DSC_SCENE_DAWN_COLORSPACE:
				switch(nPropValue)
				{
				case eUCS_CAP_COLOR_SPACE_SRGB		:	nEnum = DSC_COLORSPACE_SRGB	;	break;
				case eUCS_CAP_COLOR_SPACE_ADOBE_RGB	:	nEnum = DSC_COLORSPACE_ARGB	;	break;
				default:								nEnum =	DSC_COLORSPACE_NULL ;	break;
				}
				break;
			case DSC_SCENE_DAWN_OIS:
				switch(nPropValue)
				{
				case eUCS_CAP_OIS_MODE1		:	nEnum = DSC_OIS_MODE1	;	break;
				case eUCS_CAP_OIS_MODE2		:	nEnum = DSC_OIS_MODE2	;	break;
				case eUCS_CAP_OIS_MODE_OFF	:	nEnum = DSC_OIS_OFF		;	break;
				default:						nEnum = DSC_OIS_NULL	;	break;
				}
				break;
			}
		}
		break;	
	case DSC_MODE_SCENE_FIREWORK		:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_FIREWORK_SCENECHANGE	:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH_NX1				;	break;
					case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	nEnum = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
					case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	nEnum = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
					case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	nEnum = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
					case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	nEnum = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
					}											
					break;
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY			;	break;
					case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT			;	break;
					case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE		;	break;
					case eUCS_CAP_SCENE_MODE_PORTRAIT		:	nEnum = DSC_SCENE_MODE_PORTRAIT			;	break;
					case eUCS_CAP_SCENE_MODE_CHILDREN		:	nEnum = DSC_SCENE_MODE_CHILDREN			;	break;
					case eUCS_CAP_SCENE_MODE_SPORTS			:	nEnum = DSC_SCENE_MODE_SPORTS			;	break;
					case eUCS_CAP_SCENE_MODE_CLOSE_UP		:	nEnum = DSC_SCENE_MODE_CLOSEUP			;	break;
					case eUCS_CAP_SCENE_MODE_TEXT			:	nEnum = DSC_SCENE_MODE_TEXT				;	break;
					case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET			;	break;
					case eUCS_CAP_SCENE_MODE_DAWN			:	nEnum = DSC_SCENE_MODE_DAWN				;	break;
					case eUCS_CAP_SCENE_MODE_BACKLIGHT		:	nEnum = DSC_SCENE_MODE_BACKLIGHT		;	break;
					case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK			;	break;
					case eUCS_CAP_SCENE_MODE_BEACH_SNOW		:	nEnum = DSC_SCENE_MODE_BEACH_SNOW		;	break;
					case eUCS_CAP_SCENE_MODE_SOUND_PICTURE	:	nEnum = DSC_SCENE_MODE_SOUND_PICTURE	;	break;
					case eUCS_CAP_SCENE_MODE_3D				:	nEnum = DSC_SCENE_MODE_3D				;	break;
					case eUCS_CAP_SCENE_MODE_SMART_FILTER	:	nEnum = DSC_SCENE_MODE_SMARTFILTER		;	break;
						// dh9.seo 2013-06-24
					case eUCS_CAP_SCENE_MODE_BEST			:	nEnum = DSC_SCENE_MODE_BEST				;	break;
					case eUCS_CAP_SCENE_MODE_MACRO			:	nEnum = DSC_SCENE_MODE_MACRO			;	break;
					case eUCS_CAP_SCENE_MODE_ACTION			:	nEnum = DSC_SCENE_MODE_ACTION			;	break;
					case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH				;	break;
					case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL		;	break;
					case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE		;	break;
					case eUCS_CAP_SCENE_MODE_LIGHT			:	nEnum = DSC_SCENE_MODE_LIGHT			;	break;
					case eUCS_CAP_SCENE_MODE_CREATIVE		:	nEnum = DSC_SCENE_MODE_CREATIVE			;	break;
					case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA			;	break;
					}											
					break;
				}
			case DSC_SCENE_FIREWORK_AFMODE:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_MODE_SAF				:	nEnum =	DSC_AFMODE_SINGLE		;	break;
				case eUCS_CAP_AF_MODE_CAF				:	nEnum =	DSC_AFMODE_CONTINUOUS	;	break;
				case eUCS_CAP_AF_MODE_MF				:	nEnum =	DSC_AFMODE_MANUAL		;	break;
				default:									nEnum =	DSC_AFMODE_NULL			;	break;
				}
				break;
			case DSC_SCENE_FIREWORK_COLORSPACE:
				switch(nPropValue)
				{
				case eUCS_CAP_COLOR_SPACE_SRGB		:	nEnum = DSC_COLORSPACE_SRGB	;	break;
				case eUCS_CAP_COLOR_SPACE_ADOBE_RGB	:	nEnum = DSC_COLORSPACE_ARGB	;	break;
				default:								nEnum =	DSC_COLORSPACE_NULL ;	break;
				}
				break;
			case DSC_SCENE_FIREWORK_QUALITY:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					case eUCS_CAP_QUALITY_RAW			:	nEnum = DSC_QUALITY_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_S_FINE	:	nEnum = DSC_QUALITY_SF_RAW;	break;		
					case eUCS_CAP_QUALITY_RAW_FINE		:	nEnum = DSC_QUALITY_F_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_NORMAL	:	nEnum =	DSC_QUALITY_N_RAW	;	break;
					case eUCS_CAP_QUALITY_SRAW			:	nEnum =	DSC_QUALITY_SRAW	;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
			case DSC_SCENE_FIREWORK_DRIVE:
				//CMiLRe 20140902 DRIVER GET 설정
				if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
				{
					switch(nPropValue)
					{
						//case eUCS_CAP_DRIVE_OFF			:	nEnum = DSC_DRIVE_OFF			;	break;
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
					case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
				{
					switch(nPropValue)
					{
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE_NX1		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB_NX1			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH_NX1			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER_NX1;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				break;	
			case DSC_SCENE_FIREWORK_OIS:
				switch(nPropValue)
				{
				case eUCS_CAP_OIS_MODE1		:	nEnum = DSC_OIS_MODE1	;	break;
				case eUCS_CAP_OIS_MODE2		:	nEnum = DSC_OIS_MODE2	;	break;
				case eUCS_CAP_OIS_MODE_OFF	:	nEnum = DSC_OIS_OFF		;	break;
				default:						nEnum = DSC_OIS_NULL	;	break;
				}
				break;
			}
		}
		break;
	case DSC_MODE_SCENE_SOUND_PICTURE	:		
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_SOUNDPICTURE_SCENECHANGE	:
				switch(nPropValue)
				{
				case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY			;	break;
				case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT			;	break;
				case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE		;	break;
				case eUCS_CAP_SCENE_MODE_PORTRAIT		:	nEnum = DSC_SCENE_MODE_PORTRAIT			;	break;
				case eUCS_CAP_SCENE_MODE_CHILDREN		:	nEnum = DSC_SCENE_MODE_CHILDREN			;	break;
				case eUCS_CAP_SCENE_MODE_SPORTS			:	nEnum = DSC_SCENE_MODE_SPORTS			;	break;
				case eUCS_CAP_SCENE_MODE_CLOSE_UP		:	nEnum = DSC_SCENE_MODE_CLOSEUP			;	break;
				case eUCS_CAP_SCENE_MODE_TEXT			:	nEnum = DSC_SCENE_MODE_TEXT				;	break;
				case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET			;	break;
				case eUCS_CAP_SCENE_MODE_DAWN			:	nEnum = DSC_SCENE_MODE_DAWN				;	break;
				case eUCS_CAP_SCENE_MODE_BACKLIGHT		:	nEnum = DSC_SCENE_MODE_BACKLIGHT		;	break;
				case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK			;	break;
				case eUCS_CAP_SCENE_MODE_BEACH_SNOW		:	nEnum = DSC_SCENE_MODE_BEACH_SNOW		;	break;
				case eUCS_CAP_SCENE_MODE_SOUND_PICTURE	:	nEnum = DSC_SCENE_MODE_SOUND_PICTURE	;	break;
				case eUCS_CAP_SCENE_MODE_3D				:	nEnum = DSC_SCENE_MODE_3D				;	break;
				case eUCS_CAP_SCENE_MODE_SMART_FILTER	:	nEnum = DSC_SCENE_MODE_SMARTFILTER		;	break;
				}	
				break;
			}
		}
		break;
	case DSC_MODE_MOVIE					:		break;
	// dh9.seo 2013-06-25
	case DSC_MODE_SCENE_BEST	:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_BEST_SCENECHANGE	:
				switch(nPropValue)
				{
				case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY			;	break;
				case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT			;	break;
				case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE		;	break;
				case eUCS_CAP_SCENE_MODE_PORTRAIT		:	nEnum = DSC_SCENE_MODE_PORTRAIT			;	break;
				case eUCS_CAP_SCENE_MODE_CHILDREN		:	nEnum = DSC_SCENE_MODE_CHILDREN			;	break;
				case eUCS_CAP_SCENE_MODE_SPORTS			:	nEnum = DSC_SCENE_MODE_SPORTS			;	break;
				case eUCS_CAP_SCENE_MODE_CLOSE_UP		:	nEnum = DSC_SCENE_MODE_CLOSEUP			;	break;
				case eUCS_CAP_SCENE_MODE_TEXT			:	nEnum = DSC_SCENE_MODE_TEXT				;	break;
				case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET			;	break;
				case eUCS_CAP_SCENE_MODE_DAWN			:	nEnum = DSC_SCENE_MODE_DAWN				;	break;
				case eUCS_CAP_SCENE_MODE_BACKLIGHT		:	nEnum = DSC_SCENE_MODE_BACKLIGHT		;	break;
				case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK			;	break;
				case eUCS_CAP_SCENE_MODE_BEACH_SNOW		:	nEnum = DSC_SCENE_MODE_BEACH_SNOW		;	break;
				case eUCS_CAP_SCENE_MODE_SOUND_PICTURE	:	nEnum = DSC_SCENE_MODE_SOUND_PICTURE	;	break;
				case eUCS_CAP_SCENE_MODE_3D				:	nEnum = DSC_SCENE_MODE_3D				;	break;
				case eUCS_CAP_SCENE_MODE_SMART_FILTER	:	nEnum = DSC_SCENE_MODE_SMARTFILTER		;	break;
				// dh9.seo 2013-06-24
				case eUCS_CAP_SCENE_MODE_BEST			:	nEnum = DSC_SCENE_MODE_BEST				;	break;
				case eUCS_CAP_SCENE_MODE_MACRO			:	nEnum = DSC_SCENE_MODE_MACRO			;	break;
				case eUCS_CAP_SCENE_MODE_ACTION			:	nEnum = DSC_SCENE_MODE_ACTION			;	break;
				case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH				;	break;
				case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL		;	break;
				case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE		;	break;
				case eUCS_CAP_SCENE_MODE_LIGHT			:	nEnum = DSC_SCENE_MODE_LIGHT			;	break;
				case eUCS_CAP_SCENE_MODE_CREATIVE		:	nEnum = DSC_SCENE_MODE_CREATIVE			;	break;
				case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA			;	break;
				}	
				break;
			case DSC_SCENE_BEST_QUALITY:
				switch(nPropValue)
				{
				case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF		;	break;
				case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
				case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
				case eUCS_CAP_QUALITY_RAW			:	nEnum = DSC_QUALITY_RAW		;	break;
				case eUCS_CAP_QUALITY_RAW_S_FINE	:	nEnum = DSC_QUALITY_SF_RAW	;	break;		
				case eUCS_CAP_QUALITY_RAW_FINE		:	nEnum = DSC_QUALITY_F_RAW	;	break;
				case eUCS_CAP_QUALITY_RAW_NORMAL	:	nEnum =	DSC_QUALITY_N_RAW	;	break;
				case eUCS_CAP_QUALITY_SRAW			:	nEnum =	DSC_QUALITY_SRAW	;	break;
				default:								nEnum = DSC_QUALITY_NULL	;	break;
				}
				break; 
			case DSC_SCENE_BEST_DRIVE:
				//CMiLRe 20140902 DRIVER GET 설정
				if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
				{
					switch(nPropValue)
					{
						//case eUCS_CAP_DRIVE_OFF			:	nEnum = DSC_DRIVE_OFF			;	break;
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
					case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
				{
					switch(nPropValue)
					{
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE_NX1		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB_NX1			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH_NX1			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER_NX1;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				break;
			}
		}
		break;
	case DSC_MODE_SCENE_RICH			:
	case DSC_MODE_SCENE_WATERFALL	:	
	case DSC_MODE_SCENE_SILHOUETTE	:
	case DSC_MODE_SCENE_LIGHT		:	
	case DSC_MODE_SCENE_MACRO		:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_MACRO_SCENECHANGE	:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH_NX1				;	break;
					case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE_NX1		;	break;
					case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
					case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	nEnum = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
					case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	nEnum = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
					case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	nEnum = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
					case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	nEnum = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
					}											
					break;
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY			;	break;
					case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT			;	break;
					case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE		;	break;
					case eUCS_CAP_SCENE_MODE_PORTRAIT		:	nEnum = DSC_SCENE_MODE_PORTRAIT			;	break;
					case eUCS_CAP_SCENE_MODE_CHILDREN		:	nEnum = DSC_SCENE_MODE_CHILDREN			;	break;
					case eUCS_CAP_SCENE_MODE_SPORTS			:	nEnum = DSC_SCENE_MODE_SPORTS			;	break;
					case eUCS_CAP_SCENE_MODE_CLOSE_UP		:	nEnum = DSC_SCENE_MODE_CLOSEUP			;	break;
					case eUCS_CAP_SCENE_MODE_TEXT			:	nEnum = DSC_SCENE_MODE_TEXT				;	break;
					case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET			;	break;
					case eUCS_CAP_SCENE_MODE_DAWN			:	nEnum = DSC_SCENE_MODE_DAWN				;	break;
					case eUCS_CAP_SCENE_MODE_BACKLIGHT		:	nEnum = DSC_SCENE_MODE_BACKLIGHT		;	break;
					case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK			;	break;
					case eUCS_CAP_SCENE_MODE_BEACH_SNOW		:	nEnum = DSC_SCENE_MODE_BEACH_SNOW		;	break;
					case eUCS_CAP_SCENE_MODE_SOUND_PICTURE	:	nEnum = DSC_SCENE_MODE_SOUND_PICTURE	;	break;
					case eUCS_CAP_SCENE_MODE_3D				:	nEnum = DSC_SCENE_MODE_3D				;	break;
					case eUCS_CAP_SCENE_MODE_SMART_FILTER	:	nEnum = DSC_SCENE_MODE_SMARTFILTER		;	break;
						// dh9.seo 2013-06-24
					case eUCS_CAP_SCENE_MODE_BEST			:	nEnum = DSC_SCENE_MODE_BEST				;	break;
					case eUCS_CAP_SCENE_MODE_MACRO			:	nEnum = DSC_SCENE_MODE_MACRO			;	break;
					case eUCS_CAP_SCENE_MODE_ACTION			:	nEnum = DSC_SCENE_MODE_ACTION			;	break;
					case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH				;	break;
					case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL		;	break;
					case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE		;	break;
					case eUCS_CAP_SCENE_MODE_LIGHT			:	nEnum = DSC_SCENE_MODE_LIGHT			;	break;
					case eUCS_CAP_SCENE_MODE_CREATIVE		:	nEnum = DSC_SCENE_MODE_CREATIVE			;	break;
					case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA			;	break;
					}											
					break;
				}
			case DSC_SCENE_MACRO_FOCUSAREA:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_AREA_SELECTION				:	nEnum =	DSC_FOCUSAREA_SELECTION	;	break;
				case eUCS_CAP_AF_AREA_MULTI					:	nEnum =	DSC_FOCUSAREA_MULTI		;	break;
				case eUCS_CAP_AF_AREA_FACE_DETECTION		:	nEnum =	DSC_FOCUSAREA_FD		;	break;
				case eUCS_CAP_AF_AREA_SELF_FACE_DETECTION	:	nEnum =	DSC_FOCUSAREA_SELFFD	;	break;
				default:										nEnum =	DSC_FOCUSAREA_NULL		;	break;
				}
				break;
			case DSC_SCENE_MACRO_QUALITY:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					case eUCS_CAP_QUALITY_RAW			:	nEnum = DSC_QUALITY_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_S_FINE	:	nEnum = DSC_QUALITY_SF_RAW;	break;		
					case eUCS_CAP_QUALITY_RAW_FINE		:	nEnum = DSC_QUALITY_F_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_NORMAL	:	nEnum =	DSC_QUALITY_N_RAW	;	break;
					case eUCS_CAP_QUALITY_SRAW			:	nEnum =	DSC_QUALITY_SRAW	;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
				
			case DSC_SCENE_MACRO_DRIVE:
				//CMiLRe 20140902 DRIVER GET 설정
				if(GetModel().Compare(modelType[MDOEL_NX2000].strModelName) == 0)
				{
					switch(nPropValue)
					{
						//case eUCS_CAP_DRIVE_OFF			:	nEnum = DSC_DRIVE_OFF			;	break;
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
					case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				else if(GetModel().Compare(modelType[MDOEL_NX1].strModelName) == 0)
				{
					switch(nPropValue)
					{
					case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE_NX1		;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
					case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;					
					case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB_NX1			;	break;
					case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB_NX1			;	break;
					case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH_NX1			;	break;
					default:
						{
							if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
							{
								nEnum = DSC_DRIVE_TIMER_NX1;
							}
							else
								nEnum = DSC_DRIVE_NULL			;
						}

						break;
					}
					nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				}
				break;
			}	
		}
		break;
	case DSC_MODE_SCENE_ACTION	:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_ACTION_SCENECHANGE	:
				switch(nPropValue)
				{
				case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY			;	break;
				case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT			;	break;
				case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE		;	break;
				case eUCS_CAP_SCENE_MODE_PORTRAIT		:	nEnum = DSC_SCENE_MODE_PORTRAIT			;	break;
				case eUCS_CAP_SCENE_MODE_CHILDREN		:	nEnum = DSC_SCENE_MODE_CHILDREN			;	break;
				case eUCS_CAP_SCENE_MODE_SPORTS			:	nEnum = DSC_SCENE_MODE_SPORTS			;	break;
				case eUCS_CAP_SCENE_MODE_CLOSE_UP		:	nEnum = DSC_SCENE_MODE_CLOSEUP			;	break;
				case eUCS_CAP_SCENE_MODE_TEXT			:	nEnum = DSC_SCENE_MODE_TEXT				;	break;
				case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET			;	break;
				case eUCS_CAP_SCENE_MODE_DAWN			:	nEnum = DSC_SCENE_MODE_DAWN				;	break;
				case eUCS_CAP_SCENE_MODE_BACKLIGHT		:	nEnum = DSC_SCENE_MODE_BACKLIGHT		;	break;
				case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK			;	break;
				case eUCS_CAP_SCENE_MODE_BEACH_SNOW		:	nEnum = DSC_SCENE_MODE_BEACH_SNOW		;	break;
				case eUCS_CAP_SCENE_MODE_SOUND_PICTURE	:	nEnum = DSC_SCENE_MODE_SOUND_PICTURE	;	break;
				case eUCS_CAP_SCENE_MODE_3D				:	nEnum = DSC_SCENE_MODE_3D				;	break;
				case eUCS_CAP_SCENE_MODE_SMART_FILTER	:	nEnum = DSC_SCENE_MODE_SMARTFILTER		;	break;
				// dh9.seo 2013-06-24
				case eUCS_CAP_SCENE_MODE_BEST			:	nEnum = DSC_SCENE_MODE_BEST				;	break;
				case eUCS_CAP_SCENE_MODE_MACRO			:	nEnum = DSC_SCENE_MODE_MACRO			;	break;
				case eUCS_CAP_SCENE_MODE_ACTION			:	nEnum = DSC_SCENE_MODE_ACTION			;	break;
				case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH				;	break;
				case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL		;	break;
				case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE		;	break;
				case eUCS_CAP_SCENE_MODE_LIGHT			:	nEnum = DSC_SCENE_MODE_LIGHT			;	break;
				case eUCS_CAP_SCENE_MODE_CREATIVE		:	nEnum = DSC_SCENE_MODE_CREATIVE			;	break;
				case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA			;	break;
				//NX1 dh0.seo 2014-08-19
				case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	nEnum = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
				case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	nEnum = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
				}	
				break;
			case DSC_SCENE_ACTION_AFMODE:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_MODE_SAF				:	nEnum =	DSC_AFMODE_SINGLE		;	break;
				case eUCS_CAP_AF_MODE_CAF				:	nEnum =	DSC_AFMODE_CONTINUOUS	;	break;
				case eUCS_CAP_AF_MODE_MF				:	nEnum =	DSC_AFMODE_MANUAL		;	break;
				default:									nEnum =	DSC_AFMODE_NULL			;	break;
				}
				break;
			case DSC_SCENE_ACTION_FOCUSAREA:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_AREA_SELECTION				:	nEnum =	DSC_FOCUSAREA_SELECTION	;	break;
				case eUCS_CAP_AF_AREA_MULTI					:	nEnum =	DSC_FOCUSAREA_MULTI		;	break;
				case eUCS_CAP_AF_AREA_FACE_DETECTION		:	nEnum =	DSC_FOCUSAREA_FD		;	break;
				case eUCS_CAP_AF_AREA_SELF_FACE_DETECTION	:	nEnum =	DSC_FOCUSAREA_SELFFD	;	break;
				default:										nEnum =	DSC_FOCUSAREA_NULL		;	break;
				}
				break;
			case DSC_SCENE_ACTION_QUALITY:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					case eUCS_CAP_QUALITY_RAW			:	nEnum = DSC_QUALITY_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_S_FINE	:	nEnum = DSC_QUALITY_SF_RAW;	break;		
					case eUCS_CAP_QUALITY_RAW_FINE		:	nEnum = DSC_QUALITY_F_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_NORMAL	:	nEnum =	DSC_QUALITY_N_RAW	;	break;
					case eUCS_CAP_QUALITY_SRAW			:	nEnum =	DSC_QUALITY_SRAW	;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
			case DSC_SCENE_ACTION_DRIVE:
				switch(nPropValue)
				{
				//case eUCS_CAP_DRIVE_OFF				:	nEnum = DSC_DRIVE_OFF			;	break;
				case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
				case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
				case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
				case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
				case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
				case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
				case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
				default:								nEnum = DSC_DRIVE_NULL			;	break;
				}
				nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				break;
			}
		}
		break;
	case DSC_MODE_SCENE_CREATIVE:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_CREATIVE_SCENECHANGE	:
				switch(nPropValue)
				{
				case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY			;	break;
				case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT			;	break;
				case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE		;	break;
				case eUCS_CAP_SCENE_MODE_PORTRAIT		:	nEnum = DSC_SCENE_MODE_PORTRAIT			;	break;
				case eUCS_CAP_SCENE_MODE_CHILDREN		:	nEnum = DSC_SCENE_MODE_CHILDREN			;	break;
				case eUCS_CAP_SCENE_MODE_SPORTS			:	nEnum = DSC_SCENE_MODE_SPORTS			;	break;
				case eUCS_CAP_SCENE_MODE_CLOSE_UP		:	nEnum = DSC_SCENE_MODE_CLOSEUP			;	break;
				case eUCS_CAP_SCENE_MODE_TEXT			:	nEnum = DSC_SCENE_MODE_TEXT				;	break;
				case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET			;	break;
				case eUCS_CAP_SCENE_MODE_DAWN			:	nEnum = DSC_SCENE_MODE_DAWN				;	break;
				case eUCS_CAP_SCENE_MODE_BACKLIGHT		:	nEnum = DSC_SCENE_MODE_BACKLIGHT		;	break;
				case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK			;	break;
				case eUCS_CAP_SCENE_MODE_BEACH_SNOW		:	nEnum = DSC_SCENE_MODE_BEACH_SNOW		;	break;
				case eUCS_CAP_SCENE_MODE_SOUND_PICTURE	:	nEnum = DSC_SCENE_MODE_SOUND_PICTURE	;	break;
				case eUCS_CAP_SCENE_MODE_3D				:	nEnum = DSC_SCENE_MODE_3D				;	break;
				case eUCS_CAP_SCENE_MODE_SMART_FILTER	:	nEnum = DSC_SCENE_MODE_SMARTFILTER		;	break;
				// dh9.seo 2013-06-24
				case eUCS_CAP_SCENE_MODE_BEST			:	nEnum = DSC_SCENE_MODE_BEST				;	break;
				case eUCS_CAP_SCENE_MODE_MACRO			:	nEnum = DSC_SCENE_MODE_MACRO			;	break;
				case eUCS_CAP_SCENE_MODE_ACTION			:	nEnum = DSC_SCENE_MODE_ACTION			;	break;
				case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH				;	break;
				case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL		;	break;
				case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE		;	break;
				case eUCS_CAP_SCENE_MODE_LIGHT			:	nEnum = DSC_SCENE_MODE_LIGHT			;	break;
				case eUCS_CAP_SCENE_MODE_CREATIVE		:	nEnum = DSC_SCENE_MODE_CREATIVE			;	break;
				case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA			;	break;
				}	
				break;
			case DSC_SCENE_CREATIVE_QUALITY:
				if(!m_strModel.Compare(_T("NX1")))
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
				else
				{
					switch(nPropValue)
					{
					case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
					case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
					case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
					case eUCS_CAP_QUALITY_RAW			:	nEnum = DSC_QUALITY_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_S_FINE	:	nEnum = DSC_QUALITY_SF_RAW;	break;		
					case eUCS_CAP_QUALITY_RAW_FINE		:	nEnum = DSC_QUALITY_F_RAW	;	break;
					case eUCS_CAP_QUALITY_RAW_NORMAL	:	nEnum =	DSC_QUALITY_N_RAW	;	break;
					case eUCS_CAP_QUALITY_SRAW			:	nEnum =	DSC_QUALITY_SRAW	;	break;
					default:								nEnum = DSC_QUALITY_NULL;					break;
					}
					break; 
				}
			case DSC_SCENE_CREATIVE_DRIVE:
				switch(nPropValue)
				{
				//case eUCS_CAP_DRIVE_OFF			:	nEnum = DSC_DRIVE_OFF			;	break;
				case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE		;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_3FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_3FRAME;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_5FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_5FRAME;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS		:	nEnum = DSC_DRIVE_CONTINUOUS	;	break;
				case eUCS_CAP_DRIVE_BURST_CAPTURE	:	nEnum = DSC_DRIVE_BURST			;	break;
				case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER			;	break;
				case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB			;	break;
				case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB			;	break;
				case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB			;	break;
				case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH			;	break;
				default:								nEnum = DSC_DRIVE_NULL			;	break;
				}
				nEnum = ChangeValuetoKey(0,PTP_CODE_STILLCAPTUREMODE,nEnum);
				break;
			case DSC_SCENE_CREATIVE_FLASH:
				switch(nPropValue)
				{
				case eUCS_CAP_FLASH_OFF				:	nEnum =	DSC_FLASH_OFF			;	break;			
				case eUCS_CAP_FLASH_SMART			:	nEnum =	DSC_FLASH_SMART			;	break;
				case eUCS_CAP_FLASH_AUTO			:	nEnum =	DSC_FLASH_AUTO			;	break;
				case eUCS_CAP_FLASH_RED_EYE			:	nEnum =	DSC_FLASH_RED			;	break;
				case eUCS_CAP_FLASH_FILL_IN			:	nEnum =	DSC_FLASH_FILLIN		;	break;
				case eUCS_CAP_FLASH_FILL_IN_RED		:	nEnum =	DSC_FLASH_FILLIN_RED	;	break;
				case eUCS_CAP_FLASH_1ST_CURTAIN		:	nEnum =	DSC_FLASH_1ST_CURTAIN	;	break;
				case eUCS_CAP_FLASH_2ND_CURTAIN		:	nEnum =	DSC_FLASH_2ND_CURTAIN	;	break;
				default:								nEnum = DSC_FLASH_NULL			;	break;
				}
				break;
			}
		}
		break;
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_PANORAMA:
	case DSC_MODE_SCENE_ACTION_FREEZE :
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_ACTION_FREEZE_SCENECHANGE	:
				switch(nPropValue)
				{
				case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH_NX1				;	break;
				case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	nEnum = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
				case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	nEnum = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
				case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	nEnum = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
				case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	nEnum = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
				}											
				break;
			case DSC_SCENE_ACTION_FREEZE_QUALITY:
				switch(nPropValue)
				{
				case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
				case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
				case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
				default:								nEnum = DSC_QUALITY_NULL;					break;
				}
				break;
			case DSC_SCENE_ACTION_FREEZE_FOCUSAREA:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_AREA_SELECTION				:	nEnum =	DSC_FOCUSAREA_SELECTION	;	break;
				case eUCS_CAP_AF_AREA_MULTI					:	nEnum =	DSC_FOCUSAREA_MULTI		;	break;
				//dh0.seo 2014-10-07 주석
//				case eUCS_CAP_AF_AREA_FACE_DETECTION		:	nEnum =	DSC_FOCUSAREA_FD		;	break;
//				case eUCS_CAP_AF_AREA_SELF_FACE_DETECTION	:	nEnum =	DSC_FOCUSAREA_SELFFD	;	break;
				default:										nEnum =	DSC_FOCUSAREA_NULL		;	break;
				}
				break;
			case DSC_SCENE_ACTION_FREEZE_AFMODE:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_MODE_SAF				:	nEnum =	DSC_AFMODE_SINGLE		;	break;
				case eUCS_CAP_AF_MODE_CAF				:	nEnum =	DSC_AFMODE_CONTINUOUS	;	break;
				case eUCS_CAP_AF_MODE_AAF				:	nEnum =	DSC_AFMODE_AUTOMATIC	;	break;
				case eUCS_CAP_AF_MODE_MF				:	nEnum =	DSC_AFMODE_MANUAL		;	break;
				default:									nEnum =	DSC_AFMODE_NULL			;	break;
				}
				break;
			case DSC_SCENE_ACTION_FREEZE_DRIVE:
				switch(nPropValue)
				{
				case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE_NX1		;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1;	break;					
				case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;					
				case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER_NX1			;	break;
				case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB_NX1			;	break;
				case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB_NX1			;	break;
				case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB_NX1			;	break;
				case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH_NX1			;	break;
				default:
					{
						if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
						{
							nEnum = DSC_DRIVE_TIMER_NX1;
						}
						else
							nEnum = DSC_DRIVE_NULL			;
					}

					break;
				}
			}
		}
		break;
	case DSC_MODE_SCENE_LIGHT_TRACE :
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_MULTI_EXPOSURE_SCENECHANGE	:
				switch(nPropValue)
				{
				case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH_NX1				;	break;
				case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	nEnum = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
				case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	nEnum = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
				case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	nEnum = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
				case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	nEnum = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
				}											
				break;
			case DSC_SCENE_MULTI_EXPOSURE_QUALITY:
				switch(nPropValue)
				{
				case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
				case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
				case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
				default:								nEnum = DSC_QUALITY_NULL;					break;
				}
				break;
			case DSC_SCENE_MULTI_EXPOSURE_FOCUSAREA:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_AREA_SELECTION				:	nEnum =	DSC_FOCUSAREA_SELECTION	;	break;
				case eUCS_CAP_AF_AREA_MULTI					:	nEnum =	DSC_FOCUSAREA_MULTI		;	break;
				case eUCS_CAP_AF_AREA_FACE_DETECTION		:	nEnum =	DSC_FOCUSAREA_FD		;	break;
				case eUCS_CAP_AF_AREA_SELF_FACE_DETECTION	:	nEnum =	DSC_FOCUSAREA_SELFFD	;	break;
				default:										nEnum =	DSC_FOCUSAREA_NULL		;	break;
				}
				break;
			case DSC_SCENE_LIGHT_TRACE_DRIVE:
				switch(nPropValue)
				{
				case eUCS_CAP_DRIVE_SINGLE			:	nEnum = DSC_DRIVE_SINGLE_NX1		;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_4FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_4FRAME_NX1;	break;					
				case eUCS_CAP_DRIVE_CONTINUOUS_6FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_6FRAME_NX1;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_8FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_8FRAME_NX1;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_10FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_10FRAME_NX1;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_12FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_12FRAME_NX1;	break;
				case eUCS_CAP_DRIVE_CONTINUOUS_15FRAME:	nEnum = DSC_DRIVE_CONTINUOUS_15FRAME_NX1;	break;					
				case eUCS_CAP_DRIVE_TIMER			:	nEnum = DSC_DRIVE_TIMER_NX1			;	break;
				case eUCS_CAP_DRIVE_BRACKET_AE		:	nEnum = DSC_DRIVE_AEB_NX1			;	break;
				case eUCS_CAP_DRIVE_BRACKET_WB		:	nEnum = DSC_DRIVE_WBB_NX1			;	break;
				case eUCS_CAP_DRIVE_BRACKET_PW		:	nEnum = DSC_DRIVE_PWB_NX1			;	break;
				case eUCS_CAP_DRIVE_DEPTH_BRACKET	:	nEnum = DSC_DRIVE_DEPTH_NX1			;	break;
				default:
					{
						if(nPropValue >= 0x0200 && nPropValue <= 0x1E93)
						{
							nEnum = DSC_DRIVE_TIMER_NX1;
						}
						else
							nEnum = DSC_DRIVE_NULL			;
					}

					break;
				}
			}
		}
		break;
	case DSC_MODE_SCENE_MULTI_EXPOSURE :
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_MULTI_EXPOSURE_SCENECHANGE	:
				switch(nPropValue)
				{
				case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH_NX1				;	break;
				case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	nEnum = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
				case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	nEnum = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
				case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	nEnum = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
				case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	nEnum = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
				}											
				break;
			case DSC_SCENE_MULTI_EXPOSURE_QUALITY:
				switch(nPropValue)
				{
				case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
				case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
				case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
				default:								nEnum = DSC_QUALITY_NULL;					break;
				}
				break;
			case DSC_SCENE_MULTI_EXPOSURE_FOCUSAREA:
				switch(nPropValue)
				{
				case eUCS_CAP_AF_AREA_SELECTION				:	nEnum =	DSC_FOCUSAREA_SELECTION	;	break;
				case eUCS_CAP_AF_AREA_MULTI					:	nEnum =	DSC_FOCUSAREA_MULTI		;	break;
				case eUCS_CAP_AF_AREA_FACE_DETECTION		:	nEnum =	DSC_FOCUSAREA_FD		;	break;
				case eUCS_CAP_AF_AREA_SELF_FACE_DETECTION	:	nEnum =	DSC_FOCUSAREA_SELFFD	;	break;
				default:										nEnum =	DSC_FOCUSAREA_NULL		;	break;
				}
				break;
			}
		}
		break;
	case DSC_MODE_SCENE_AUTO_SHUTTER:
		{
			switch(nPropIndex)
			{
			case DSC_SCENE_MULTI_EXPOSURE_SCENECHANGE	:
				switch(nPropValue)
				{
				case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH_NX1				;	break;
				case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE_NX1		;	break;
				case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
				case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	nEnum = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
				case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	nEnum = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
				case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	nEnum = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
				case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	nEnum = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
				}											
				break;
			case DSC_SCENE_MULTI_EXPOSURE_QUALITY:
				switch(nPropValue)
				{
				case eUCS_CAP_QUALITY_SUPER_FINE	:	nEnum = DSC_QUALITY_SF	;	break;
				case eUCS_CAP_QUALITY_FINE			:	nEnum = DSC_QUALITY_F		;	break;
				case eUCS_CAP_QUALITY_NORMAL		:	nEnum = DSC_QUALITY_N		;	break;
				default:								nEnum = DSC_QUALITY_NULL;					break;
				}
				break;
			}
		}
		break;
	case DSC_MODE_SCENE_PANORAMA:	
		switch(nPropIndex)
		{
		case DSC_SCENE_MULTI_EXPOSURE_SCENECHANGE	:
			switch(nPropValue)
			{
			case eUCS_CAP_SCENE_MODE_BEAUTY			:	nEnum = DSC_SCENE_MODE_BEUATY_NX1			;	break;
			case eUCS_CAP_SCENE_MODE_NIGHT			:	nEnum = DSC_SCENE_MODE_NIGHT_NX1			;	break;
			case eUCS_CAP_SCENE_MODE_LANDSCAPEE		:	nEnum = DSC_SCENE_MODE_LANDSCAPE_NX1		;	break;
			case eUCS_CAP_SCENE_MODE_SUNSET			:	nEnum = DSC_SCENE_MODE_SUNSET_NX1			;	break;
			case eUCS_CAP_SCENE_MODE_FIREWORK		:	nEnum = DSC_SCENE_MODE_FIREWORK_NX1			;	break;
			case eUCS_CAP_SCENE_MODE_RICH			:	nEnum = DSC_SCENE_MODE_RICH_NX1				;	break;
			case eUCS_CAP_SCENE_MODE_WATERFALL		:	nEnum = DSC_SCENE_MODE_WATERFALL_NX1		;	break;
			case eUCS_CAP_SCENE_MODE_SILHOUETTE		:	nEnum = DSC_SCENE_MODE_SILHOUETTE_NX1		;	break;
			case eUCS_CAP_SCENE_MODE_PANORAMA		:	nEnum = DSC_SCENE_MODE_PANORAMA_NX1			;	break;
			case eUCS_CAP_SCENE_MODE_MULTI_EXPOSURE	:	nEnum = DSC_SCENE_MODE_MULTI_EXPOSURE	;	break;
			case eUCS_CAP_SCENE_MODE_AUTO_SHUTTER	:	nEnum = DSC_SCENE_MODE_AUTO_SHUTTER		;	break;
			case eUCS_CAP_SCENE_MODE_ACTION_FREEZE	:	nEnum = DSC_SCENE_MODE_ACTION_FREEZE	;	break;
			case eUCS_CAP_SCENE_MODE_LIGHT_TRACE	:	nEnum = DSC_SCENE_MODE_LIGHT_TRACE		;	break;
			}											
			break;
		}
		break;
	}
	return nEnum;
}
//------------------------------------------------------------------------------ 
//! @brief		ChangeIndexToPropName
//! @date		2011-06-10
//! @author		Lee JungTaek
//! @note	 	Change Property Value (ex.20M  -> 1, 16 -> 2..)
//------------------------------------------------------------------------------ 
CString CRSLayoutInfo::ChangeIndexToPropName(int nPropIndex)
{
	switch(m_nMode)
	{
	case DSC_MODE_P:					
	case DSC_MODE_A:
	case DSC_MODE_S:
	case DSC_MODE_M:
	case DSC_MODE_BULB					:	if(nPropIndex < ARRAY_SIZE(NX200PASMItemSet)) return NX200PASMItemSet[nPropIndex].strProperty;break; //dh0.seo 2015-01-12 위치 이동
	case DSC_MODE_SMART:					if(nPropIndex < ARRAY_SIZE(NX200SmartAutoItemSet)) return NX200SmartAutoItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_I						:	break;	
	case DSC_MODE_CAPTURE_MOVIE			:	break;
	case DSC_MODE_MAGIC_MAGIC_FRAME		:	if(nPropIndex < ARRAY_SIZE(NX200MagicFrameItemSet)) return NX200MagicFrameItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_MAGIC_SMART_FILTER	:	if(nPropIndex < ARRAY_SIZE(NX200SmartFilterItemSet)) return NX200SmartFilterItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_PANORAMA				:	if(nPropIndex < ARRAY_SIZE(NX2000ScenePanoramaItemSet)) return NX2000ScenePanoramaItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_BEAUTY			:	if(nPropIndex < ARRAY_SIZE(NX200SceneBeautyItemSet)) return NX200SceneBeautyItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_NIGHT			:	if(nPropIndex < ARRAY_SIZE(NX200SceneNightItemSet)) return NX200SceneNightItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_LANDSCAPE		:	if(nPropIndex < ARRAY_SIZE(NX200SceneLandscapeItemSet)) return NX200SceneLandscapeItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_PORTRAIT		:	if(nPropIndex < ARRAY_SIZE(NX200ScenePortraitItemSet)) return NX200ScenePortraitItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_CHILDREN		:	if(nPropIndex < ARRAY_SIZE(NX200SceneChildrenItemSet)) return NX200SceneChildrenItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_SPORTS			:	if(nPropIndex < ARRAY_SIZE(NX200SceneSportsItemSet)) return NX200SceneSportsItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_CLOSE_UP		:	if(nPropIndex < ARRAY_SIZE(NX200SceneCloseUpItemSet)) return NX200SceneCloseUpItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_TEXT			:	if(nPropIndex < ARRAY_SIZE(NX200SceneTextItemSet)) return NX200SceneTextItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_SUNSET			:	if(nPropIndex < ARRAY_SIZE(NX200SceneSunsetItemSet)) return NX200SceneSunsetItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_DAWN			:	if(nPropIndex < ARRAY_SIZE(NX200SceneDawnItemSet)) return NX200SceneDawnItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_BACKLIGHT		:	if(nPropIndex < ARRAY_SIZE(NX200SceneBacklightItemSet)) return NX200SceneBacklightItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_FIREWORK		:	if(nPropIndex < ARRAY_SIZE(NX200SceneFireworkItemSet)) return NX200SceneFireworkItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_BEACH_SNOW		:	if(nPropIndex < ARRAY_SIZE(NX200SceneBeachSnowItemSet)) return NX200SceneBeachSnowItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_SOUND_PICTURE	:	if(nPropIndex < ARRAY_SIZE(NX200SceneSoundPictureItemSet)) return NX200SceneSoundPictureItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_3D				:	if(nPropIndex < ARRAY_SIZE(NX200Scene3DItemSet)) return NX200Scene3DItemSet[nPropIndex].strProperty;break;
	// dh9.seo 2013-07-02
	//prevent
	case DSC_MODE_SCENE_BEST			:	if(nPropIndex < ARRAY_SIZE(NX2000SceneBestItemSet)) return NX2000SceneBestItemSet[nPropIndex].strProperty; break;
	case DSC_MODE_SCENE_MACRO			:	if(nPropIndex < ARRAY_SIZE(NX2000SceneMacroItemSet)) return NX2000SceneMacroItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_ACTION			:	if(nPropIndex < ARRAY_SIZE(NX2000SceneActionItemSet)) return NX2000SceneActionItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_RICH			:	if(nPropIndex < ARRAY_SIZE(NX2000SceneRichItemSet)) return NX2000SceneRichItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_WATERFALL		:	if(nPropIndex < ARRAY_SIZE(NX2000SceneWaterfallItemSet)) return NX2000SceneWaterfallItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_SILHOUETTE		:	if(nPropIndex < ARRAY_SIZE(NX2000SceneSilhouetteItemSet)) return NX2000SceneSilhouetteItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_LIGHT			:	if(nPropIndex < ARRAY_SIZE(NX2000SceneLightItemSet)) return NX2000SceneLightItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_CREATIVE		:	if(nPropIndex < ARRAY_SIZE(NX2000SceneCreativeItemSet)) return NX2000SceneCreativeItemSet[nPropIndex].strProperty;break;
	//NX1 dh0.seo 2014-08-19
	case DSC_MODE_SCENE_ACTION_FREEZE	:	if(nPropIndex < ARRAY_SIZE(NX1SceneActionFreezeItemSet)) return NX1SceneActionFreezeItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_LIGHT_TRACE		:	if(nPropIndex < ARRAY_SIZE(NX1SceneLightTraceItemSet)) return NX1SceneLightTraceItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_MULTI_EXPOSURE	:	if(nPropIndex < ARRAY_SIZE(NX1SceneMultiExposureItemSet)) return NX1SceneMultiExposureItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_AUTO_SHUTTER	:	if(nPropIndex < ARRAY_SIZE(NX1SceneAutoShutterItemSet)) return NX1SceneAutoShutterItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_SCENE_PANORAMA		:	if(nPropIndex < ARRAY_SIZE(NX2000ScenePanoramaItemSet)) return NX2000ScenePanoramaItemSet[nPropIndex].strProperty;break;
	case DSC_MODE_MOVIE					:	break;
	}
	return _T("");
}

//------------------------------------------------------------------------------ 
//! @brief		GetModeIndex
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	Change Mode String to Index
//------------------------------------------------------------------------------ 
int CRSLayoutInfo::GetModeIndex(CString strMode)
{
	int nMode = -1;
	for(int i = 0; i < ARRAYSIZE(SZNX200PropModeValue); i++)
	{
		if(strMode == SZNX200PropModeValue[i].strDesc)
		{
			nMode = SZNX200PropModeValue[i].nIndex;
			break;
		}
	}	
	return nMode;
}

//------------------------------------------------------------------------------ 
//! @brief		SetPropertyDescription
//! @date		2011-7-14
//! @author		Lee JungTaek
//! @note	 	Set Property Description
//------------------------------------------------------------------------------ 
CString CRSLayoutInfo::SetPropertyDescription(ControlTypeInfo* pControlTypeInfo, int nPropIndex)
{
	CString strPropDesc = _T("");

	switch(pControlTypeInfo->nType)
	{
	case SMARTPANEL_CONTROL_BUTTON:
		{
			CRSButton* pRSButton = NULL;
			pRSButton = (CRSButton*)pControlTypeInfo->pRSCtrl;
			strPropDesc.Format(_T("%s : %s"), pRSButton->m_strPropertyName, pRSButton->m_strPropValue);
		}		
		break;
	case SMARTPANEL_CONTROL_BAROMETER:
		{
			CRSBarometerCtrl *pRSBarometer = (CRSBarometerCtrl*)pControlTypeInfo->pRSCtrl;
			strPropDesc = pRSBarometer->GetPropDesc();
		}				
		break;
	case SMARTPANEL_CONTROL_SLIDER:
		{
			CRSSliderCtrl *pRSSlider = (CRSSliderCtrl*)pControlTypeInfo->pRSCtrl;
			strPropDesc = pRSSlider->GetPropDesc();
		}
		break;				
	case SMARTPANEL_CONTROL_MODESELECT:
		{
			CRSModeSelectCtrl *pRSModeSelect = (CRSModeSelectCtrl*)pControlTypeInfo->pRSCtrl;
			strPropDesc = pRSModeSelect->GetPropDesc();
		}
		break;
	case SMARTPANEL_CONTROL_MAGICMODE:
		{
			CRSMagicFilterSelectCtrl *pRSMagicMode = (CRSMagicFilterSelectCtrl*)pControlTypeInfo->pRSCtrl;
			strPropDesc = pRSMagicMode->GetPropDesc();
		}
		break;
	}	

	return strPropDesc;
}