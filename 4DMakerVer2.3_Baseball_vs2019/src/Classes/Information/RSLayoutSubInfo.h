/////////////////////////////////////////////////////////////////////////////
//
//  CRSLayoutSubInfo.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////
#include "SmartPanelIndex.h"

class CRSLayoutSubInfo : public CObject
{
public:
	CRSLayoutSubInfo(void);
	~CRSLayoutSubInfo(void);

private:
	CRSArray m_arrPropertyValue;

private:
	BOOL RemovePopertyValue();

public:
	//-- LOAD
	void LoadInfo(CString strFile, CString strModel, CString strPoperty);

	//-- Array
	int GetPopertyValueCount();
	int AddPopertyValue(CRSPropertyValue* pPropertyValue);
	CRSPropertyValue* GetPropertyValue(int nIndex);	

	CRSArray* GetArrayPropertyValue(){return &m_arrPropertyValue;}
};
