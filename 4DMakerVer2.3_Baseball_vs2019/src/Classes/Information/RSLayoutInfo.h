/////////////////////////////////////////////////////////////////////////////
//
//  CRSLayoutInfo.h : implementation file
//
//  SAMSUNG SDS CO.,LTD. PROPRIETARY INFORMATION.
//  The following contains information proprietary to
//  SAMSUNG SDS CO.,LTD., and may not be copied
//  nor disclosed except upon written agreement
//  by SAMSUNG SDS CO.,LTD..
//
//  Copyright (C) 2011 SAMSUNG SDS CO.,LTD. All rights reserved.
//
// @author		JungTaek Lee (jt48.lee@samsung.com)
// @Date	2011-05-16
//
/////////////////////////////////////////////////////////////////////////////

#include "SRSIndex.h"
#include "RSLayoutSubInfo.h"
#include "RSButton.h"
#include "RSBarometerCtrl.h"
#include "RSSliderCtrl.h"
#include "RSModeSelectCtrl.h"
#include "RSMagicFilterSelectCtrl.h"

class CRSLayoutInfo
{
public:
	CRSLayoutInfo(void);
	~CRSLayoutInfo(void);
	
private:
	CRSArray m_arrLayout;
	CRSArray m_arrLayoutSub;

	CString m_strModel;
	int m_nMode;
	CString m_nModeName;

public:
	BOOL RemoveLayoutInfo();
	BOOL RemoveLayoutSubInfoClass();

public:
	//-- LOAD
	void LoadInfo(CString strFile, CString strModel, CString strMode);
	void LoadLayout(CString strFile, CString strModel, CString strSection, CString strProperty);

	//-- Layout Array
	int GetLayoutCount();
	int AddLayoutInfo(CRSPropertyLayout* pPropertyLayout);
	CRSPropertyLayout* GetLayoutInfo(int nIndex);	

	//-- Array
	int GetLayoutSubInfoClassCount();
	int AddLayoutSubClassInfo(CRSLayoutSubInfo* pLayoutSubInfo);
	CRSLayoutSubInfo* GetLayoutSubClassInfo(int nIndex);	

	//-- 2011-5-18 Lee JungTaek
	//-- Control LayoutSubArray
	CString GetButtonImageByStatus(CString strPropName, int nCurPropIndex, int nCurProperty, int nStatus, CString &strValue);

	//-- 2011-06-10 hongsu.jung
	//-- Draw Icon From String or Enum
	void DrawIcon(ControlTypeInfo* pControlTypeInfo, int nPropIndex, CString strValue, int nStatus);
	void DrawIcon(ControlTypeInfo* pControlTypeInfo, int nPropIndex, int nPropValue, int nStatus);

	//-- Set Button Info
	void SetBtnInfo(CRSButton* ctrlButton, int nPropIndex, CString strPorpertyName, BOOL bEnable);
	void SetBtnInfo(CRSButton* ctrlButton, int nPropIndex, int nPropvalue, BOOL bEnable);
	void SetBtnImageInfo(CRSButton* ctrlButton, int nPropIndex, int nCurPropValue, int nStatus);

	//-- Get Poperty Name
	CString ChangeIndexToPropName(int nPropIndex);
	//-- Get Value Index
	int ChangeStringValueToIndex(int nPropIndex, CString strPropValue);
	int ChangeIntValueToIndex(int nPropIndex, int nPropValue);
	
	//-- Change Iamge
	void ChangePrePropImage(ControlTypeInfo* pControlTypeInfo, int nPrePropIndex);

	//-- Change Mode to Index
	int GetModeIndex(CString strMode);
	//-- ChangeValuetoKey
	int ChangeValuetoKey(int nIndex, int nPTPCode, int nEnum);
	CString SetPropertyDescription(ControlTypeInfo* pControlTypeInfo, int nPropIndex);

	//-- Modle Change
	void SetTargetModel(CString strModel) {m_strModel = strModel;}
	CString GetTargetModel() {return m_strModel;}

	//CMiLRe 20140901 Model Name Get/Set
	CString GetModel() {return m_strModel;}
	int GetSmartFilter_ModelType_RSLayoutInfo(int nPropValue);
};
