#include <Windows.h>
#include <iostream>
#include <string>

#include "ObjectTracking.h"

using namespace std;

double __fastcall GetTime() 
{ 
	LARGE_INTEGER liEndCounter,liFrequency ; 
	QueryPerformanceCounter(&liEndCounter); 
	QueryPerformanceFrequency(&liFrequency); 

	return (liEndCounter.QuadPart / (double)liFrequency.QuadPart) * 1000; 
} 

static Mat image;
static Rect2d boundingBox;
// static Rect2d fixedBox; // Only for the fixed example for comparison.
static bool paused;
static bool selectObject = false; // 
static bool startSelection = false;
static bool initialized = false;

static void onMouse( int event, int x, int y, int, void* )
{
	if(image.empty())
		return;
	if( !selectObject )
	{
		switch ( event )
		{
		case EVENT_LBUTTONDOWN:
			//set origin of the bounding box
			startSelection = true;
			boundingBox.x = x;
			boundingBox.y = y;
			boundingBox.width = boundingBox.height = 0;
			//TRACE("LButton : %d, %d", boundingBox.x, boundingBox.y);
			cout << "LButtonDown : " << boundingBox << endl;
			break;
		case EVENT_LBUTTONUP:
			//set with and height of the bounding box
			boundingBox.width = std::abs( x - boundingBox.x);
			boundingBox.height = std::abs( y - boundingBox.y );
			paused = false;
			selectObject = true;					

			if(boundingBox.width <= 0 || boundingBox.height <= 0)
			{
				boundingBox.width = 240;
				boundingBox.height = 135;
			}

			cout << "Location : " << 
				boundingBox.width*4<< ", " <<
				boundingBox.height*4 << ", " << endl;

			cout << "LButtonUp: " << boundingBox << endl;

			break;
		case EVENT_MOUSEMOVE:
			if( startSelection && !selectObject )
			{
				//draw the bounding box
				Mat currentFrame;				
				image.copyTo( currentFrame );
				rectangle( currentFrame, 
					Point((int)boundingBox.x, (int)boundingBox.y ), 
					Point( x, y ), 
					Scalar( 0, 0, 255 ), 2, 1 );
				imshow( "Tracking API", currentFrame );
			}
			break;				
		}
	} 
}

int OrdinaryTest(int argc, char* argv[]);

int main(int argc, char* argv[])
{
	// cout << argv[0] << endl;
	// cout << argv[1] << endl;
	// cout << argv[2] << endl;
	// cout << argc << endl;


	if (OrdinaryTest(argc, argv) != 0)
	{
		std::cout << "The test is not valid" << std::endl;
		return -1;
	}

	return 0;
}

int OrdinaryTest(int argc, char* argv[])
{
	if (argc != 1 && argc != 2 && argc != 3)
	{
		cout << "The nubmer of the variables should be 2 or 3 (that is, argc = 2 or 3)" << endl << endl
			<< "For example)" << endl << endl 
			<< "> ObjectTracking.exe sample2.mp4 200" << endl << endl
			<< "In this case, " << endl
			<< "argc = 3" << endl
			<< "argv[0] = ObjectTracking.exe" << endl
			<< "argv[1] = sample2.mp4" << endl
			<< "argv[2] = 200 (indicating the zoom is 200%)" << endl << endl
			<< "If there's no input in argv[2], the zoomScale is automatically 200(%)" << endl;
		return -1;
	}

	// the tracking starts
	boundingBox.height = 0;
	boundingBox.width = 0;
	boundingBox.x = 0;
	boundingBox.y = 0;
	///////////////////////
	selectObject = false;
	startSelection = false;
	initialized = false;

	double start, end;
	double totalStart, totalEnd;
	int count;
	int frameCount = 0;

	// zoom Factor
	float zoomFactor;

	string inputPath;
	string outputPath;

	fflush(stdin);

	if (argc == 2 || argc ==3 )
	{
		// Check the input file
		inputPath.append(argv[1]);
		cout << "The input file : " << inputPath << endl;
		
		// Create the output file by the input file.
		for (int i = 0 ; i < inputPath.size() - 4; i++)
			outputPath.push_back(inputPath[i]);

		outputPath.append("_Tracking.mp4");
		cout << "The output file : " << outputPath << endl;
	}
	else
	{
		cout << "input : ";
		getline(cin, inputPath);
		cout << "output : ";
		getline(cin, outputPath);
	}
	
	VideoCapture vc(inputPath);

	if (!vc.isOpened())
	{
		cout << inputPath << "is not a existing file" << endl;
		return -1;
	}

	ObjectTracking *objectTracking = new ObjectTracking(getImageSize((int)vc.get(cv::CAP_PROP_FRAME_WIDTH), (int)vc.get(cv::CAP_PROP_FRAME_HEIGHT)));

	/*
	if ((int)vc.get(cv::CAP_PROP_FRAME_WIDTH) > 1920 ||
		(int)vc.get(cv::CAP_PROP_FRAME_HEIGHT) > 1080)
		objectTracking = new ObjectTracking(UHD_SIZE);
	else
		objectTracking = new ObjectTracking(FHD_SIZE);
	*/

	objectTracking->_outputSize = Size(vc.get(cv::CAP_PROP_FRAME_WIDTH), vc.get(cv::CAP_PROP_FRAME_HEIGHT));

	if (argc == 2)
	{
		zoomFactor = 2.0;
		objectTracking->_templateScale = zoomFactor;
		cout << "The zoomScale is 200%" << endl;
	}
	else if (argc == 3)
	{
		zoomFactor = (float)atoi(argv[2])/100.0;
		objectTracking->_templateScale = zoomFactor;
		cout << "The zoomScale is " << argv[2] << "%" << endl;
	}
	else
	{
		zoomFactor = 2.0;
		objectTracking->_templateScale = zoomFactor;
	}

	Mat frame;
	VideoCapture cap(inputPath);
	// VideoCapture buf;
	
	if( !cap.isOpened() )
		return -1;

	paused = false;
	namedWindow( "Tracking API");
	setMouseCallback( "Tracking API", onMouse, 0 );	
	int cnt = 0;
	double filteringTime;

	count = 0;

	for ( ;; )
	{	
		{
			if(!paused)
			{					
				cap >> frame;
				if(frame.empty())
				{
					break;
				}	

				// Bi-lateral filtering 
				start = GetTime();
				objectTracking->Filtering(frame, image);														
				end = GetTime();
				filteringTime = end-start;

				if(selectObject)
				{					
					if(!initialized)
					{
						objectTracking->InitTracking(image, boundingBox);
						initialized = true;				

					}
					else
					{
						if (count == 0)
							totalStart = GetTime();
						count ++;
						std::cout << count << ". ";

						std::cout << "Tracking Speed : " << end - start << endl;

						//tracking///////////////////////////////////////////////
						start = GetTime();
						objectTracking->UpdateTracking(image, boundingBox);

						end = GetTime();

						rectangle(image, boundingBox, Scalar(0, 0, 255), 2, 1);	
						/////////////////////////////////////////////////////////

						//stabilization//////////////////////////////////////////////////////////////////////////////////////
						Mat focusImage;
						objectTracking->RoiImageOfOriginalSize(frame, focusImage, boundingBox);											
						//imshow("focus", focusImage);
						//waitKey(0);
						// 시작
						start = GetTime();
						objectTracking->imageSetForStabilization->inputImageForGeneratingTransformationMatrix(focusImage);						
						objectTracking->testImage.push_back(frame.clone());


						Rect2d tempBox = boundingBox;

						objectTracking->RoiImageOfStabilizationSize(frame, frame, tempBox);							
						rectangle(image, 
							Rect2d( tempBox.x/objectTracking->_samplingScale,
							tempBox.y/objectTracking->_samplingScale,
							tempBox.width/objectTracking->_samplingScale,
							tempBox.height/objectTracking->_samplingScale), 
							Scalar(255, 0, 0), 2, 1);

						objectTracking->imageSetForStabilization->inputImageForApplyingTransformationMatrix(frame);	
						end = GetTime();

						///////////////////////////////////////////////////////////////////////////////////////////////////////
					}									
				}			

				// stabilizer window 를 끄고 싶을 때 아래를 주석 처리 (imshow까지)
				if(cnt == 0)
				{			
					paused = !paused;
				}				
				imshow( "Tracking API", image );						
			}					

			char c = (char) waitKey( 1);
			
			// when quitting the program abruptedly.
			if( (c == 'q' || c == 27) || c == 'Q' ) // to prevent when the Caps Lock key is pressed....
				break;
			// when pausing or re-starting	
			if( c == 'p' || c == 'P') // to prevent when the Caps Lock key is pressed....
				paused = !paused;		
			// when abruptly change the status...
			if (c == 's' || c == 'S') // to prevent when the Caps Lock key is pressed...
			{
				cout << "The experimenter stops the tracking procedure. " << endl;
				cap.release();
				cap.open(inputPath);
				// cap = buf;
				paused = true;
				count -= count;
				// buf.release();

				// re-initialization of bounding box
				boundingBox.height = 0;
				boundingBox.width = 0;
				boundingBox.x = 0;
				boundingBox.y = 0;
				///////////////////////
				selectObject = false;
				startSelection = false;
				initialized = false;
				
				delete objectTracking;
				objectTracking = new ObjectTracking(getImageSize((int)vc.get(cv::CAP_PROP_FRAME_WIDTH), (int)vc.get(cv::CAP_PROP_FRAME_HEIGHT)));
				objectTracking->_templateScale = zoomFactor;
				
				// goto StartPoint;
				continue;
			}
		}

		cnt ++;
	} // end of for(::)			
	//Encoding

	objectTracking->CreateStabilizatedMovie(outputPath, 30.);
	totalEnd= GetTime();
	std::cout << "Total Stabilization Speed : " << totalEnd- totalStart<< endl;

	cv::destroyWindow("Tracking API");

	// Remove in the 4DMaker
	// objectTracking->getAdjustMoveCoordination();

	std::cout << endl;
	delete objectTracking;
	image = Mat();

	// the tracking ends.

	return 0;
}

