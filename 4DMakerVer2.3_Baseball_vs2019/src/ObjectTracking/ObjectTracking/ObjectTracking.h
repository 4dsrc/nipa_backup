#pragma once

#include <opencv2/tracking.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

// #include <opencv2/cudabgsegm.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafilters.hpp>

#include <opencv2/videostab.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video.hpp>

#include <iostream>
#include <cstring>
#include <functional>

using namespace cv;
using namespace std;

typedef enum 
{
	FHD_SIZE = 0x1,
	UHD_SIZE = 0x2,
	ETC_SIZE = 0x4,
}IMAGE_SIZE;

class ObjectTracking
{

public:
	enum FILTER_TYPE
	{
		BOX_FILTER = 0x0,
		BOX_MAX_FILTER = 0x1,
		BOX_MIN_FILTER = 0x2,
		COLUMN_SUM_FILTER = 0x3,
		GAUSSIAN_FILTER = 0x4,
		LAPLACIAN_FILTER = 0x5,
		LINEAR_FILTER = 0x6,
		MORPHOLOGY_FILTER = 0x7,
		ROW_SUM_FILTER = 0x8,
		SOBEL_FILTER = 0x9,
	};

public:
	ObjectTracking(IMAGE_SIZE imageSize);
	~ObjectTracking(void);

public:
	void Filtering(Mat& srcImage, Mat& dstImage);
	void InitTracking(Mat& image, Rect2d& trackingRegion);
	void UpdateTracking(Mat& image, Rect2d& trackingRegion);

	void RoiImageOfOriginalSize(Mat& srcImage, Mat& dstImage, Rect2d& trackingRegion);
	void RoiImageOfStabilizationSize(Mat& srcImage, Mat& dstImage, Rect2d& trackingRegion);	
		
private:
	static void SobelExtractor(const Mat img, const Rect roi, Mat& feat);
	void MeanShiftSegmentation(cuda::GpuMat& srcImage, cuda::GpuMat& dstImage);
	void BilateralFiltering(cuda::GpuMat& srcImage, cuda::GpuMat& dstImage);
	// void GaussianFiltering(cuda::GpuMat&, cuda::GpuMat&);
	bool ModifyRegionToBeValid(Mat& image, Rect2d& inputRegion, Rect2d& paddingRegion, bool cutMargin);

	Ptr<cuda::Filter> filterSelect;
	void filteringSelect(cuda::GpuMat&, cuda::GpuMat&, FILTER_TYPE);

	
public:
	int _samplingScale;
	double _templateScale;

	//Ptr<TrackerKCF> tracker;		
	Ptr<Tracker> tracker;
	TrackerKCF::Params param;	

public:
	void CreateStabilizatedMovie(string outputPath, double outputFps);	

private:
	void IncodingWithStabilizedImage(Ptr<videostab::IFrameSource> stabilizedFrames, string outputPath, double outputFps);

public:
	Ptr<videostab::IFrameSource> stabilizedFrames; 
	Ptr<videostab::ImageSetForStabilization> imageSetForStabilization;		
	Ptr<videostab::MotionEstimatorRansacL2> motionEstimation; 		
	Ptr<videostab::KeypointBasedMotionEstimatorGpu> motionEstBuilder;
	Ptr<videostab::IOutlierRejector> outlierRejector;	

	videostab::RansacParams ransac; 
	videostab::TwoPassStabilizer *twoPassStabilizer;
		
	IMAGE_SIZE _imageSize;	
	Size _outputSize;
	double _minInlierRatio;

	int _constraintTrackingDistance;

public:
	double Round(double n);

public:
	void getAdjustMoveCoordination();
	void moveImage(Mat* gMat, int nX, int nY);

	Point2f _baseLinePoint;
	vector<Point2f> _subPoint;
	vector<Mat> testImage;
};

IMAGE_SIZE getImageSize(int _width, int _height);