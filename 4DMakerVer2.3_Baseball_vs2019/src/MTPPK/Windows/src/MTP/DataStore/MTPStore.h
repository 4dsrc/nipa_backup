/*
 *  MTPStore.h
 *
 *  Contains declaration of MTP store.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPSTORE_H_
#define _MTPSTORE_H_

typedef PSLHANDLE MTPSTORE;


PSL_EXTERN_C PSLSTATUS PSL_API MTPStoreCreate(
                            MTPDATASTORE mds,
                            PSLUINT32 dwStoreId,
                            MTPSTORE* pmtpStore);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStoreDestroy(
                            MTPSTORE mtpStore);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStoreGetProp(
                            MTPSTORE mtpStore,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            PSLUINT32 dwDataFormat,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten,
                            PSLUINT32* pcbLeft);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStoreSetProp(
                            MTPSTORE mtpStore,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            PSLUINT32 dwDataFormat,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLBOOL fFromWire);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStoreDelete(
                            MTPSTORE mtpStore);

PSL_EXTERN_C PSLBOOL PSL_API MTPStoreIsValid(
                            MTPSTORE mtpStore);

PSL_EXTERN_C PSLBOOL PSL_API MTPStoreIsReadOnly(
                            MTPSTORE mtpStore);

/*
 *  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#define SAFE_MTPSTOREDESTROY(_mtps) \
if (PSLNULL != _mtps) \
{ \
    MTPStoreDestroy(_mtps); \
    _mtps = PSLNULL; \
}

#endif  /* _MTPSTORE_H_ */
