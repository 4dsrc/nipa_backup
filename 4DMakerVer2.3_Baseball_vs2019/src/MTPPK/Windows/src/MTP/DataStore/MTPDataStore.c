/*
 *  MTPDataStore.c
 *
 *  Contains implementation of the MTP data store
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPDataStorePrecomp.h"
#include "MTPDataStore.h"
#include "MTPDataStoreCommon.h"
#include "MTPStore.h"
#include "MTPObject.h"
#include "MTPDataStoreUtil.h"
#include "FileUtils.h"

#define MAX_MTPSTORES       10
#define INIT_MTPOBJECTS     1000
#define GROWBY_MTPOBJECTS   250

#define MTP_DEPTH_ALL           0xFFFFFFFF
#define MTP_DEPTH_INVALID       0x0000FFFF
#define MTP_DEPTH_UNDEFINED     0x00000000

#define MTPPK_REGKEY_LOCATION   L"Software\\Microsoft\\MTP PK"
#define MTPPK_REGVAL_DATASTORE  L"DataStore"

typedef struct _MTPDATASTOREIMPL
{
    MTPSTORE* pmsList;
    MTPOBJECT* pmoList;
    PSLUINT32 cStores;
    PSLUINT32 cObjects;
    PSLUINT32 cMaxObjects;
    PSLUINT32 dwNextStoreIndex;
    PSLUINT32 dwNextObjectIndex;
    PSLHANDLE hMutex;
}MTPDATASTOREIMPL;

/* Private Operations*/

static PSLSTATUS _MTPDataStoreInit(
                            MTPDATASTORE mds);

static PSLSTATUS _MTPDataStoreUninit(
                            MTPDATASTORE mds);

static PSLSTATUS _EnumStore(
                            MTPDATASTORE mds,
                            PSLUINT32 dwStorageID,
                            PSLUINT32 dwParentHandle,
                            PSLWCHAR* szStorePath);

static PSLSTATUS _AddObject(
                            MTPDATASTORE mds,
                            PSLUINT32 dwStorageID,
                            PSLUINT32 dwParentHandle,
                            PSLWCHAR* szFileName,
                            PSLWCHAR* szFilePath,
                            PSLUINT32 dwFileSizeHigh,
                            PSLUINT32 dwFileSizeLow,
                            FILETIME* pftCreated,
                            FILETIME* pftModified,
                            PSLUINT32 dwFileAttributes,
                            MTPOBJECT* pmtpObject,
                            PSLUINT32* pdwObjectHandle);

static PSLVOID _GetNextStoreIndex(
                            MTPDATASTORE mds,
                            PSLUINT32* pdwStoreIndex);

static PSLVOID _GetNextObjectIndex(
                            MTPDATASTORE mds,
                            PSLUINT32* pdwObjectIndex);

static PSLSTATUS _QueryStore(
                            MTPDATASTORE mds,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            MTPITEM** pItemList,
                            PSLUINT32* pcItems);

static PSLSTATUS _QueryObject(
                            MTPDATASTORE mds,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            MTPITEM** pItemList,
                            PSLUINT32* pcItems);

static PSLSTATUS _InsertObject(
                            MTPDATASTORE mds,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            MTPITEM** pItemList,
                            PSLUINT32* pcItems);

static PSLSTATUS _GetDefaultStoreId(
                            MTPDATASTORE mds,
                            PSLUINT32* pdwStoreId);

static PSLSTATUS _QueryByDepth(
                            MTPDATASTORE mds,
                            PSLUINT32 dwParentHandle,
                            PSLUINT16 wFormatCode,
                            PSLUINT32 dwDepth,
                            MTPITEM** ppItemList,
                            PSLUINT32* pcItems);

static PSLSTATUS _SetDateProperty(
                            MTPITEM mtpItem,
                            FILETIME* pDate,
                            PSLUINT16 wPropCode);

/* private variables */
static PSLUINT32 _dwRefCount = 0;
static MTPDATASTORE _Mds = PSLNULL;

/* following 2 variables are used to traverse store and object list*/
static const PSLUINT32 _dwStoreIdStart = 1;
static const PSLUINT32 _dwObjectHandleStart = 1;

/* MTP complaince test manadate Storage ID to start here */
static const PSLUINT32 _dwFirstStoreId = 0x10000;

static const WCHAR _RgchDSInitMutex[] =  L"MTPDataStore Init Mutex";

#define STOREID_STOREINDEX(x) (x - _dwFirstStoreId)
#define STOREINDEX_STOREID(x) (x + _dwFirstStoreId)

PSLSTATUS MTPDataStoreCreate(MTPDATASTORE* pmds)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi = PSLNULL;
    MTPSTORE* pmsList =PSLNULL;
    MTPOBJECT* pmoList =PSLNULL;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL != pmds)
    {
        *pmds = PSLNULL;
    }

    if (PSLNULL == pmds)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (0 < _dwRefCount)
    {
        PSLASSERT(PSLNULL != _Mds);
        *pmds = _Mds;
        PSLAtomicIncrement(&_dwRefCount);
        status = PSLSUCCESS;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)PSLMemAlloc(PSLMEM_PTR,
                                           sizeof(MTPDATASTOREIMPL));
    if (PSLNULL == pmdsi)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    pmsList = (MTPSTORE*)PSLMemReAlloc(pmsList,
                              (sizeof(MTPSTORE) * MAX_MTPSTORES),
                              (PSLMEM_FIXED | PSLMEM_RELOCATABLE));
    if (PSLNULL == pmsList)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    pmoList = (MTPOBJECT*)PSLMemReAlloc(pmoList,
                              (sizeof(MTPOBJECT) * INIT_MTPOBJECTS),
                              (PSLMEM_FIXED | PSLMEM_RELOCATABLE));
    if (PSLNULL == pmoList)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = PSLMutexOpen(PSLTHREAD_INFINITE,
                          (PSLWCHAR*)_RgchDSInitMutex,
                          &hMutexRelease);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Set the lock */
    pmdsi->hMutex = hMutexRelease;

    pmdsi->pmsList = pmsList;
    pmsList = PSLNULL;
    pmdsi->cStores = 0;

    pmdsi->pmoList = pmoList;
    pmoList = PSLNULL;
    pmdsi->cObjects = 0;

    pmdsi->cMaxObjects = INIT_MTPOBJECTS;

    pmdsi->dwNextStoreIndex = 0;
    pmdsi->dwNextObjectIndex = 0;

    status = _MTPDataStoreInit((MTPDATASTORE)pmdsi);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *pmds = _Mds = (MTPDATASTORE)pmdsi;
    pmdsi = PSLNULL;

    PSLAtomicIncrement(&_dwRefCount);
    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease)
    SAFE_PSLMEMFREE(pmsList);
    SAFE_PSLMEMFREE(pmoList);
    SAFE_PSLMEMFREE(pmdsi);
    return status;
}

PSLSTATUS MTPDataStoreDestroy(MTPDATASTORE mds)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi;
    PSLUINT32 dwRefCount;
    PSLHANDLE hMutexRelease = PSLNULL;

    pmdsi = (MTPDATASTOREIMPL*)mds;

    dwRefCount = PSLAtomicDecrement(&_dwRefCount);
    if (0 < dwRefCount)
    {
        goto Exit;
    }

    if (PSLNULL != pmdsi)
    {
        /* Acquire store level lock */
        status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
        PSLASSERT(PSL_SUCCEEDED(status));
        hMutexRelease = pmdsi->hMutex;
    }

    (PSLVOID)_MTPDataStoreUninit(mds);

    _Mds = PSLNULL;

    if (PSLNULL != pmdsi)
    {
        SAFE_PSLMEMFREE(pmdsi->pmsList);
        SAFE_PSLMEMFREE(pmdsi->pmoList);
        SAFE_PSLMUTEXRELEASECLOSE(pmdsi->hMutex);
        hMutexRelease = PSLNULL;
    }
    SAFE_PSLMEMFREE(pmdsi);

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease)
    return PSLSUCCESS;
}


PSLSTATUS _MTPDataStoreInit(MTPDATASTORE mds)
{
    PSLSTATUS status;
    MTPSTORE mtpStore = PSLNULL;
    PSLUINT32 dwStorageIndex;
    MTPDATASTOREIMPL* pmdsi;
    PSLUINT32 cchLen;

    WCHAR szStorePath[MAX_PATH]={0};

    HKEY  hkey;
    DWORD dwType;
    DWORD dwLength;

    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL == mds)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    /* Get Data Store path from registry. */
    if( ERROR_SUCCESS == RegCreateKeyEx(HKEY_CURRENT_USER,
                            MTPPK_REGKEY_LOCATION,
                            0, NULL, REG_OPTION_NON_VOLATILE,
                            KEY_ALL_ACCESS, NULL, &hkey, NULL))
    {
        dwType   = REG_SZ;
        dwLength = MAX_PATH;

        RegQueryValueEx(hkey, MTPPK_REGVAL_DATASTORE, NULL, &dwType,
                                (BYTE*)szStorePath, &dwLength);
    }

    if (INVALID_FILE_ATTRIBUTES == GetFileAttributes(szStorePath))
    {
        PSLASSERT(!L"Select Data Store first before proceed.");
        status = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /* Read the regitry to get the store paths*/
    /* PSLASSERT(!L"Read the regitry to get the store paths"); */

    /* For each store path get the store Id*/
    _GetNextStoreIndex(mds, &dwStorageIndex);

    /* Create a store object*/
    status = MTPStoreCreate(mds, STOREINDEX_STOREID(dwStorageIndex),
                            &mtpStore);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* store path */
    status = PSLStringLen((PSLWCHAR*)szStorePath, MAX_PATH, &cchLen);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPStoreSetProp(mtpStore, PSLNULL, 0,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_FILEPATH),
                         (PSLBYTE*)szStorePath,
                         (cchLen + 1) * sizeof(WCHAR), PSLFALSE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Enumerate the store*/
    status = _EnumStore(mds, STOREINDEX_STOREID(dwStorageIndex),
                        MTP_OBJECTHANDLE_ROOT,
                        (PSLWCHAR*)szStorePath);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Store enumeration failed");
        goto Exit;
    }

    /* Add the store to the store list*/
    pmdsi->pmsList[dwStorageIndex] = mtpStore;
    mtpStore = PSLNULL;

    pmdsi->cStores++;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_MTPSTOREDESTROY(mtpStore);
    return status;
}

PSLSTATUS _MTPDataStoreUninit(MTPDATASTORE mds)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi;
    PSLUINT32 dwIndex;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL == mds)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    /* Destroy all the objects */
    for (dwIndex = _dwObjectHandleStart;
         dwIndex < pmdsi->cObjects + _dwObjectHandleStart; dwIndex++)
    {
        SAFE_MTPOBJECTDESTROY(pmdsi->pmoList[dwIndex]);
    }

    /* Destroy all the stores */
    for (dwIndex = _dwStoreIdStart;
         dwIndex < pmdsi->cStores + _dwStoreIdStart; dwIndex++)
    {
        SAFE_MTPSTOREDESTROY(pmdsi->pmsList[dwIndex]);
    }

    pmdsi->dwNextStoreIndex = 0;
    pmdsi->dwNextObjectIndex = 0;
    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return status;
}

PSLSTATUS MTPDataStoreQuery(MTPDATASTORE mds,
        PSL_CONST MTPDBVARIANTPROP* pmqList, PSLUINT32 cmqList,
        PSLUINT32 dwItemType, MTPITEM** ppItemList,PSLUINT32* pcItems)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL != ppItemList)
    {
        *ppItemList = PSLNULL;
    }

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    /* pmqList can be PSLNULL &  cmqList can be zero in the case
     * where there is no query parameter to pass
     */
    if (PSLNULL == mds || PSLNULL == ppItemList || PSLNULL == pcItems)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    switch(dwItemType)
    {
        case MTPDSITEMTYPE_STORE:
            status = _QueryStore(mds, pmqList, cmqList, ppItemList,
                                 pcItems);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
            break;
        case MTPDSITEMTYPE_OBJECT:
            status = _QueryObject(mds, pmqList, cmqList, ppItemList,
                                  pcItems);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
            break;
        default:
            PSLASSERT(!L"Data Store does identify the Item type");
            status = PSLERROR_UNEXPECTED;
            goto Exit;
    }

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return status;
}

PSLSTATUS MTPDataStoreInsert(MTPDATASTORE mds,
     PSL_CONST MTPDBVARIANTPROP* pmqList,PSLUINT32 cmqList,
     PSLUINT32 dwItemType,MTPITEM** ppItemList, PSLUINT32* pcItems)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL != ppItemList)
    {
        *ppItemList = PSLNULL;
    }

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    /* pmqList can be PSLNULL &  cmqList can be zero in the case
     * where there is no query parameter to pass
     */
    if (PSLNULL == mds || PSLNULL == ppItemList || PSLNULL == pcItems)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    switch(dwItemType)
    {
        case MTPDSITEMTYPE_STORE:
            PSLASSERT(!L"Cannot insert a store through this call");
            status = PSLERROR_UNEXPECTED;
            break;
        case MTPDSITEMTYPE_OBJECT:
            status = _InsertObject(mds, pmqList, cmqList, ppItemList,
                                   pcItems);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
            break;
        default:
            PSLASSERT(!L"Data Store does identify the Item type");
            status = PSLERROR_UNEXPECTED;
            goto Exit;
    }

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return status;
}

PSLSTATUS MTPDataStoreDelete(MTPITEM mItem)
{
    PSLSTATUS status;
    MTPITEMIMPL* pmiImpl;

    if (PSLNULL == mItem)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmiImpl = (MTPITEMIMPL*)mItem;

    if (PSLNULL == pmiImpl->pfnMTPItemDelete)
    {
        /* This could happen because the object
         * might have already been deleted when its
         * parent was deleted.
         */
        status = PSLSUCCESS;
        goto Exit;
    }

    status = pmiImpl->pfnMTPItemDelete(mItem);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
Exit:
    return status;
}

PSLSTATUS MTPDataStoreGetProp(MTPITEM mItem,
        PSL_CONST MTPDBVARIANTPROP* pmqList, PSLUINT32 cmqList,
        PSLUINT32 dwDataFormat, PSLBYTE* pbBuf, PSLUINT32 cbSize,
        PSLUINT32* pcbWritten, PSLUINT32* pcbLeft)
{
    PSLSTATUS status;
    MTPITEMIMPL* pmiImpl;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /* pcbWritten, pcbLeft & pbBuf can all be PSNULL
     * and cbSize can be zero
     */
    if (PSLNULL == mItem)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmiImpl = (MTPITEMIMPL*)mItem;

    if (PSLNULL == pmiImpl->pfnMTPItemGetProp)
    {
        PSLASSERT(!L"GetProp function pointer is PSLNULL");
        status = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    status = pmiImpl->pfnMTPItemGetProp(mItem, pmqList, cmqList,
                                        dwDataFormat, pbBuf, cbSize,
                                        pcbWritten, pcbLeft);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
Exit:
    return status;
}

PSLSTATUS MTPDataStoreSetProp(MTPITEM mItem,
      PSL_CONST MTPDBVARIANTPROP* pmqList, PSLUINT32 cmqList,
      PSLUINT32 dwDataFormat, PSLBYTE* pbBuf, PSLUINT32 cbSize)
{
    PSLSTATUS status;
    MTPITEMIMPL* pmiImpl;

    /* pmqList can be PSLNULL */
    if (PSLNULL == mItem || PSLNULL == pbBuf)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmiImpl = (MTPITEMIMPL*)mItem;

    if (PSLNULL == pmiImpl->pfnMTPItemSetProp)
    {
        PSLASSERT(!L"SetProp function pointer is PSLNULL");
        status = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    status = pmiImpl->pfnMTPItemSetProp(mItem, pmqList, cmqList,
                                        dwDataFormat, pbBuf,
                                        cbSize, PSLTRUE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS MTPDataStoreAddStore(MTPDATASTORE mds, PSLWCHAR* szStorePath,
                               PSLUINT32 dwPathLen)
{
    mds;
    szStorePath;
    dwPathLen;
    PSLASSERT(!L"Not Implemented");
    return PSLERROR_NOT_IMPLEMENTED;
}

PSLSTATUS MTPDataStoreRemoveStore(MTPDATASTORE mds,
                    PSLWCHAR* szStorePath,PSLUINT32 dwPathLen)
{
    mds;
    szStorePath;
    dwPathLen;
    PSLASSERT(!L"Not Implemented");
    return PSLERROR_NOT_IMPLEMENTED;
}

PSLSTATUS MTPDataStoreGetMTPObject(MTPDATASTORE mds,
                    PSLUINT32 dwObjectHandle, MTPITEM* pmtpObject)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL != pmtpObject)
    {
        *pmtpObject = PSLNULL;
    }

    if (PSLNULL == mds || PSLNULL == pmtpObject)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    if (dwObjectHandle > pmdsi->cObjects)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if(PSLFALSE == MTPObjectIsValid(pmdsi->pmoList[dwObjectHandle]))
    {
        status = MTPERROR_DATABASE_INVALID_HANDLE;
        goto Exit;
    }

    *pmtpObject = pmdsi->pmoList[dwObjectHandle];

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return status;
}

PSLSTATUS MTPDataStoreGetMTPStore(MTPDATASTORE mds,
                      PSLUINT32 dwStoreId, MTPITEM* pmtpStore)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi;
    PSLUINT32 dwStoreIndex;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL != pmtpStore)
    {
        *pmtpStore = PSLNULL;
    }

    if (PSLNULL == mds || PSLNULL == pmtpStore)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    if (dwStoreId < _dwFirstStoreId)
    {
        status = MTPERROR_DATABASE_INVALID_STORAGEID;
        goto Exit;
    }

    dwStoreIndex = STOREID_STOREINDEX(dwStoreId);

    if (dwStoreIndex > pmdsi->cStores)
    {
        PSLASSERT(PSLFALSE);
        status = MTPERROR_DATABASE_INVALID_STORAGEID;
        goto Exit;
    }

    if(PSLFALSE == MTPStoreIsValid(pmdsi->pmsList[dwStoreIndex]))
    {
        status = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    *pmtpStore = pmdsi->pmsList[dwStoreIndex];

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return status;
}

PSLSTATUS _EnumStore(MTPDATASTORE mds, PSLUINT32 dwStorageID,
             PSLUINT32 dwParentHandle, PSLWCHAR* szStorePath)
{
    PSLSTATUS status;
    HRESULT hr;
    HANDLE hFile = INVALID_HANDLE_VALUE;
    WIN32_FIND_DATAW wfData;
    PSLWCHAR szFilePath[MAX_PATH] = {0};
    PSLBOOL fFind;
    PSLUINT32 dwFolderHandle = 0;

    hr = StringCchPrintfW((WCHAR*)szFilePath, MAX_PATH,
                           L"%s\\*.*", szStorePath);
    if(FAILED(hr))
    {
        status = hr;
        goto Exit;
    }

    hFile = FindFirstFileW((WCHAR*)szFilePath, &wfData);
    if (INVALID_HANDLE_VALUE == hFile)
    {
        /* Enumeration complete */
        status = PSLSUCCESS;
        goto Exit;
    }

    do
    {
        hr = StringCchPrintfW((WCHAR*)szFilePath, MAX_PATH, L"%s\\%s",
                              szStorePath, wfData.cFileName);
        if(FAILED(hr))
        {
            status = hr;
            goto Exit;
        }

        if (wfData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            /*  Found directory */
            /*  Ignore "." and ".." entries */
            if ((CSTR_EQUAL != CompareStringW(LOCALE_SYSTEM_DEFAULT,
                                                NORM_IGNORECASE,
                                                wfData.cFileName,
                                                -1, L".", -1))
                && (CSTR_EQUAL != CompareStringW(LOCALE_SYSTEM_DEFAULT,
                                                 NORM_IGNORECASE,
                                                 wfData.cFileName,
                                                 -1, L"..", -1)))
            {
                /* Add the folder object to data store */
                status = _AddObject(mds, dwStorageID, dwParentHandle,
                                    (PSLWCHAR*)wfData.cFileName,
                                    (PSLWCHAR*)szFilePath, 0, 0,
                                    &wfData.ftCreationTime,
                                    &wfData.ftLastWriteTime,
                                    wfData.dwFileAttributes, PSLNULL,
                                    &dwFolderHandle);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }

                /* Enumerate the subfolder for files */
                status = _EnumStore(mds, dwStorageID, dwFolderHandle,
                                        (PSLWCHAR*)szFilePath);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
            }

        }
        else
        {
            /* Found file */

            /* Add the file object to data store */
            status = _AddObject(mds, dwStorageID, dwParentHandle,
                                (PSLWCHAR*)wfData.cFileName,
                                (PSLWCHAR*)szFilePath,
                                wfData.nFileSizeHigh,
                                wfData.nFileSizeLow,
                                &wfData.ftCreationTime,
                                &wfData.ftLastWriteTime,
                                wfData.dwFileAttributes,
                                PSLNULL, PSLNULL);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
        }
        fFind = FindNextFileW(hFile, &wfData);
    }while(fFind);

    status = PSLSUCCESS;
Exit:
    if (INVALID_HANDLE_VALUE != hFile)
    {
        /* Close the search handle. */
        FindClose(hFile);
    }
    return status;
}

PSLSTATUS _AddObject(MTPDATASTORE mds, PSLUINT32 dwStorageID,
             PSLUINT32 dwParentHandle, PSLWCHAR* szFileName,
             PSLWCHAR* szFilePath, PSLUINT32 dwFileSizeHigh,
             PSLUINT32 dwFileSizeLow, FILETIME* pftCreated,
             FILETIME* pftModified, PSLUINT32 dwFileAttributes,
             MTPOBJECT* pmtpObject, PSLUINT32* pdwObjectHandle)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi;
    PSLUINT32 dwObjectHandle;
    MTPOBJECT mtpObject = PSLNULL;
    PSLBYTE PGUID[16] = {0};
    PSLUINT32 cchFileName;
    MTPDBVARIANTPROP mopQuad = {0};
    PSLUINT64 qdwFileSize;
    PSLUINT16 wProtectStatus;
    PSLUINT16 wObjectFormat;
    PSLUINT16 wAssocType;

    MTPOBJECT* pmoList = PSLNULL;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL != pdwObjectHandle)
    {
        *pdwObjectHandle = 0;
    }

    if (PSLNULL != pmtpObject)
    {
        *pmtpObject = PSLNULL;
    }

    /* szFileName, szFilePath, pfnCreated, pfnModified
     * pdwObjectHandle and pmtpObject can be PSLNULL
     */
    if (PSLNULL == mds)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    _GetNextObjectIndex(mds, &dwObjectHandle);

    status = MTPObjectCreate(mds, dwObjectHandle, &mtpObject);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Set persistent unique object identifier  */
    mopQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mopQuad.wDataType = MTP_DATATYPE_UINT32;
    mopQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PERSISTENTGUID;

    PSLCopyMemory(PGUID, &dwObjectHandle, sizeof(PSLUINT32));
    status = MTPObjectSetProp(mtpObject, &mopQuad, 1,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          PGUID, sizeof(PSLUINT64) * 2,
                          PSLFALSE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Set storage id propety */
    mopQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mopQuad.wDataType = MTP_DATATYPE_UINT32;
    mopQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

    status = MTPObjectSetProp(mtpObject, &mopQuad, 1,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)&dwStorageID, sizeof(PSLUINT32),
                          PSLFALSE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Set parent handle propety */
    mopQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mopQuad.wDataType = MTP_DATATYPE_UINT32;
    mopQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PARENT;

    status = MTPObjectSetProp(mtpObject, &mopQuad, 1,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)&dwParentHandle,
                          sizeof(PSLUINT32), PSLFALSE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* szFilePath will be PSLNULL when called from _InsertObject */
    if (PSLNULL != szFilePath)
    {
        status = PSLStringLen(szFilePath, MAX_PATH, &cchFileName);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        /* Set file path propety */
        status = MTPObjectSetProp(mtpObject, PSLNULL, 0,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_FILEPATH),
                          (PSLBYTE*)szFilePath,
                          (cchFileName + 1) * sizeof(PSLWCHAR),
                          PSLFALSE);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    /* szFileName will be PSLNULL when called from _InsertObject */
    if (PSLNULL != szFileName)
    {
        status = PSLStringLen(szFileName, MAX_PATH, &cchFileName);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        /* Set file name propety */
        mopQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mopQuad.wDataType = MTP_DATATYPE_UINT32;
        mopQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFILENAME;

        status = MTPObjectSetProp(mtpObject, &mopQuad, 1,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)szFileName,
                          (cchFileName + 1) * sizeof(PSLWCHAR),
                          PSLFALSE);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        if (INVALID_FILE_ATTRIBUTES != dwFileAttributes)
        {
            /* If not a directory use exetnsion to find the format */
            if (0 == (dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
            {
                /* Set object format property */
                status = GetMTPFormatByFileExt(
                                FindFileExtension((LPCWSTR)szFileName),
                                &wObjectFormat);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
            }
            else
            {
                wObjectFormat = (PSLUINT16)MTP_FORMATCODE_ASSOCIATION;
            }
            mopQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
            mopQuad.wDataType = MTP_DATATYPE_UINT32;
            mopQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFORMAT;

            status = MTPObjectSetProp(mtpObject, &mopQuad, 1,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)&wObjectFormat, sizeof(PSLUINT16),
                          PSLFALSE);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
        }
    }

    /* Update the date created and date modified only when this
     * function is called during file system enumeration
     */

    /* Set date created property */

    if (PSLNULL != pftCreated)
    {
        status = _SetDateProperty(mtpObject, pftCreated,
                                    MTP_OBJECTPROPCODE_DATECREATED);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    /* Set date modified property */
    if (PSLNULL != pftModified)
    {
        status = _SetDateProperty(mtpObject, pftModified,
                                    MTP_OBJECTPROPCODE_DATEMODIFIED);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    /* Set file size propety */
    /* No need to swap byte order for this value
     * the operation that can bring this value from wire
     * during InsertObject is SendObjectPropList as
     * operation request and that will be swapped by transport.
     */
    /* atleast one value shlould be less that MAXDWORD
     * if both are equal to MAXDWORD we don't want to set it
     */
    if (MAXDWORD != dwFileSizeHigh || MAXDWORD != dwFileSizeLow)
    {
        mopQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mopQuad.wDataType = MTP_DATATYPE_UINT32;
        mopQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTSIZE;

        qdwFileSize = (dwFileSizeHigh * (MAXDWORD)) + dwFileSizeLow;
        status = MTPObjectSetProp(mtpObject, &mopQuad, 1,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)&qdwFileSize, sizeof(PSLUINT64),
                          PSLFALSE);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    if (INVALID_FILE_ATTRIBUTES != dwFileAttributes)
    {
        /* Set hidden property */
        PSLUINT16 wHidden = (dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)?
                            MTP_BOOLEAN_TRUE : MTP_BOOLEAN_FALSE;

        mopQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mopQuad.wDataType = MTP_DATATYPE_UINT32;
        mopQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_HIDDEN;

        status = MTPObjectSetProp(mtpObject, &mopQuad, 1,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)&wHidden, sizeof(PSLUINT16),
                          PSLFALSE);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        /* Set protection status property */
        wProtectStatus = (dwFileAttributes & FILE_ATTRIBUTE_READONLY) ?
                            (PSLUINT16)MTP_PROTECTIONSTATUS_READONLY :
                            (PSLUINT16)MTP_PROTECTIONSTATUS_NONE;

        mopQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mopQuad.wDataType = MTP_DATATYPE_UINT32;
        mopQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PROTECTIONSTATUS;

        status = MTPObjectSetProp(mtpObject, &mopQuad, 1,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)&wProtectStatus, sizeof(PSLUINT16),
                          PSLFALSE);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        /* Set association type propety */
        wAssocType = (dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ?
                        (PSLUINT16)MTP_ASSOCIATIONTYPE_GENERICFOLDER :
                        (PSLUINT16)MTP_ASSOCIATIONTYPE_UNDEFINED;

        mopQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mopQuad.wDataType = MTP_DATATYPE_UINT32;
        mopQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_ASSOCIATIONTYPE;

        status = MTPObjectSetProp(mtpObject, &mopQuad, 1,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)&wAssocType, sizeof(PSLUINT16),
                          PSLFALSE);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    /* If the handle returned is greater than
     * the max number of objects that can be stored
     * in the object list expand the list by GROWBY_MTPOBJECTS
     */
    if (dwObjectHandle > (pmdsi->cMaxObjects - 1))
    {
        pmoList = (MTPOBJECT*)PSLMemReAlloc(pmdsi->pmoList,
                      (sizeof(MTPOBJECT) * \
                       (pmdsi->cMaxObjects + GROWBY_MTPOBJECTS)),
                      (PSLMEM_FIXED | PSLMEM_RELOCATABLE));
        if (PSLNULL == pmoList)
        {
            status = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }

        pmdsi->cMaxObjects += GROWBY_MTPOBJECTS;
        pmdsi->pmoList = pmoList;
        pmoList = PSLNULL;
    }

    /* Add the object to the object list*/
    pmdsi->pmoList[dwObjectHandle] = mtpObject;
    mtpObject = PSLNULL;

    pmdsi->cObjects++;

    PSLTraceDetail("Object Handle = %d Count = %d",
                    dwObjectHandle, pmdsi->cObjects);

    if (PSLNULL != pmtpObject)
    {
        *pmtpObject = pmdsi->pmoList[dwObjectHandle];
    }

    if (PSLNULL != pdwObjectHandle)
    {
        *pdwObjectHandle = dwObjectHandle;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMEMFREE(pmoList);
    SAFE_MTPOBJECTDESTROY(mtpObject);
    return status;
}

PSLVOID _GetNextObjectIndex(MTPDATASTORE mds, PSLUINT32* pdwObjectIndex)
{
    MTPDATASTOREIMPL* pmdsi;

    pmdsi = (MTPDATASTOREIMPL*)mds;

    if (PSLNULL != pmdsi && PSLNULL != pdwObjectIndex)
    {
        *pdwObjectIndex = PSLAtomicIncrement(&pmdsi->dwNextObjectIndex);
    }
}

PSLVOID _GetNextStoreIndex(MTPDATASTORE mds, PSLUINT32* pdwStoreIndex)
{
    MTPDATASTOREIMPL* pmdsi;

    pmdsi = (MTPDATASTOREIMPL*)mds;

    if (PSLNULL != pmdsi && PSLNULL != pdwStoreIndex)
    {
        *pdwStoreIndex = PSLAtomicIncrement(&pmdsi->dwNextStoreIndex);
    }
}

PSLSTATUS _QueryStore(MTPDATASTORE mds,
              PSL_CONST MTPDBVARIANTPROP* pmqList, PSLUINT32 cmqList,
              MTPITEM** ppItemList, PSLUINT32* pcItems)
{
    PSLSTATUS status;
    PSLUINT32 cStores;
    MTPDATASTOREIMPL* pmdsi;
    MTPSTORE* pItems = PSLNULL;
    PSLUINT32 dwIndex;
    PSLUINT32 dwStoreIndex;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL != ppItemList)
    {
        *ppItemList = PSLNULL;
    }

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    /* pmqList can be PSLNULL &  cmqList can be zero
     * when query is for all stores (GetStorageIDs)
     */
    if (PSLNULL == mds || PSLNULL == ppItemList || PSLNULL == pcItems)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (0 != cmqList)
    {
        /* Query is for a specific store and the first quad
         * should contain the storage ID
         */
        PSLASSERT(1 == cmqList);
        cStores = 1;
    }
    else
    {
        /* Query is for all stores */
        PSLASSERT(PSLNULL == pmqList);
        cStores = pmdsi->cStores;
    }

    PSLASSERT(0 < cStores);

    if (0 == cStores)
    {
        status = MTPERROR_DATABASE_INVALID_HANDLE;
        goto Exit;
    }

    pItems = (MTPITEM*)PSLMemAlloc(PSLMEM_PTR,
                                   cStores * sizeof(MTPSTORE));
    if (PSLNULL == pItems)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    if (PSLNULL != pmqList)
    {
        /* Query is for a specific store */
        PSLASSERT(1 == cStores);
        PSLASSERT(MTP_OBJECTPROPCODE_STORAGEID ==
                                            pmqList[0].wPropCode);

        if (pmqList[0].vParam.valUInt32 < _dwFirstStoreId)
        {
            status = MTPERROR_DATABASE_INVALID_STORAGEID;
            goto Exit;
        }

        dwStoreIndex = STOREID_STOREINDEX(pmqList[0].vParam.valUInt32);

        if (dwStoreIndex > pmdsi->cStores)
        {
            PSLASSERT(PSLFALSE);
            status = MTPERROR_DATABASE_INVALID_STORAGEID;
            goto Exit;
        }

        if (MTPStoreIsValid(pmdsi->pmsList[dwStoreIndex]))
        {
            pItems[0] = pmdsi->pmsList[dwStoreIndex];
        }
        else
        {
            status = MTPERROR_DATABASE_INVALID_STORAGEID;
            goto Exit;
        }
    }
    else
    {
        for (dwIndex = 0, dwStoreIndex = _dwStoreIdStart;
             dwIndex < cStores; dwIndex++, dwStoreIndex++)
        {
            /* Query is for all stores */
            if (MTPStoreIsValid(pmdsi->pmsList[dwStoreIndex]))
            {
                pItems[dwIndex] = pmdsi->pmsList[dwStoreIndex];
            }
        }
    }

    *ppItemList = pItems;
    pItems = PSLNULL;

    *pcItems = cStores;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS _QueryObject(MTPDATASTORE mds,
               PSL_CONST MTPDBVARIANTPROP* pmqList, PSLUINT32 cmqList,
               MTPITEM** ppItemList, PSLUINT32* pcItems)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi;
    MTPOBJECT* pItems = PSLNULL;
    PSLUINT32 cItems = 0;
    MTPOBJECT* pItemsTemp;
    PSLUINT32 dwIndex;
    PSLUINT32 dwItemIndex;
    PSLUINT32 dwObjIndex;

    PSLUINT32 dwObjectHandle = MTP_OBJECTHANDLE_UNDEFINED;
    PSLUINT32 dwStorageId = MTP_STORAGEID_UNDEFINED;
    PSLUINT32 dwParentHandle = MTP_OBJECTHANDLE_UNDEFINED;
    PSLUINT16 wFormatCode = MTP_FORMATCODE_NOTUSED;
    PSLUINT32 dwDepth = MTP_DEPTH_INVALID;

    PSLUINT32 dwStorageIdCmp;
    PSLUINT32 dwParentHandleCmp;
    PSLUINT16 wFormatCodeCmp;

    MTPDBVARIANTPROP mpQuad = {0};

    PSLBOOL fQueryForPropList = PSLFALSE;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL != ppItemList)
    {
        *ppItemList = PSLNULL;
    }

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    if (PSLNULL == mds || PSLNULL == pmqList || 0 == cmqList)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    for (dwIndex = 0; dwIndex < cmqList; dwIndex++)
    {
        switch(pmqList[dwIndex].wPropCode)
        {
            case MTP_OBJECTPROPCODE_OBJECTHANDLE:
                PSLASSERT(0 == dwIndex);
                dwObjectHandle = pmqList[dwIndex].vParam.valUInt32;
                break;
            case MTP_OBJECTPROPCODE_STORAGEID:
                dwStorageId = (PSLUINT32)pmqList[dwIndex].vParam.valUInt32;
                break;
            case MTP_OBJECTPROPCODE_OBJECTFORMAT:
                wFormatCode = (PSLUINT16)pmqList[dwIndex].vParam.valUInt16;
                break;
            case MTP_OBJECTPROPCODE_PARENT:
                dwParentHandle = (PSLUINT32)pmqList[dwIndex].vParam.valUInt32;
                break;
            case MTP_OBJECTPROPCODE_DEPTH:
                dwDepth = (PSLUINT32)pmqList[dwIndex].vParam.valUInt32;
                fQueryForPropList = PSLTRUE;
                break;
        }
    }

    /* If the depth is present it means the
     * command is GetObjectPropList
     */
    if (PSLTRUE == fQueryForPropList)
    {
        if (MTP_DEPTH_UNDEFINED == dwDepth)
        {
            /* If the fifth parameter contains a value of 0x00000000,
             * and the ObjectHandle in the first parameter also
             * contains a value of 0x00000000, the responder should
             * return an empty set.
             */
            if(MTP_OBJECTHANDLE_UNDEFINED == dwObjectHandle)
            {
                *pcItems = 0;
                status = PSLSUCCESS;
                goto Exit;
            }

            /* If the value of the fifth parameter is 0x00000000,
             * it indicates that properties for objects are desired
             * to a depth of 0, which returns only the head object
             * (as indicated by the first parameter).
             */

            /* Handling will happen below */

            /* At ths point dwObjectHandle will be either
             * handle of a specific object or MTP_OBJECTHANDLE_ALL
             */
        }
        else
        {
            /* If there is a valid depth parameter first parameter
             * takes the meaning of a parent handle
             */
            if (MTP_OBJECTHANDLE_ALL == dwObjectHandle ||
                (MTP_OBJECTHANDLE_UNDEFINED == dwObjectHandle &&
                 MTP_FORMATCODE_NOTUSED == wFormatCode &&
                 MTP_DEPTH_ALL == dwDepth))
            {
                /* It should be noted that a value of 0x00000000 in the
                 * first two parameters, followed by a value of
                 * 0xFFFFFFFF in the fifth parameter, is equivalent
                 * to having a value of 0xFFFFFFFF in the first
                 * parameter.
                 */
                dwObjectHandle = MTP_OBJECTHANDLE_ALL;

                /* Handling will happen below */
            }
            else
            {
                PSLASSERT(MTP_OBJECTHANDLE_ALL != dwObjectHandle);

                /* If the fifth parameter contains a non-zero value and
                 * the first parameter contains a value of 0x00000000,
                 * then the responder should return property values for
                 * objects starting from the root (having no parent
                 * object) to the desired depth.
                 */
                if (MTP_OBJECTHANDLE_UNDEFINED == dwObjectHandle)
                {
                    dwParentHandle = MTP_OBJECTHANDLE_ROOT;
                }
                else
                {
                    dwParentHandle = dwObjectHandle;
                }

                /* At this point parent handle can be
                 * the handle of a specific object or
                 * that of root
                 */

                /* If the final parameter contains a value of
                 * 0xFFFFFFFF, the responder should return all
                 * values for all objects that are contained within
                 * the folder hierarchy rooted at the object
                 * identified by the first parameter.
                 */
                status = _QueryByDepth(mds, dwParentHandle,
                                     wFormatCode,
                                     dwDepth, ppItemList,
                                     pcItems);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
                /* we are done with query nothing else to do */
                goto Exit;
            }
        }
    }

    /* If the object handlle is not zero or all
     * use the handle to get the object from the list
     */
    if (MTP_OBJECTHANDLE_UNDEFINED != dwObjectHandle &&
        MTP_OBJECTHANDLE_ALL != dwObjectHandle)
    {
        if (dwObjectHandle > pmdsi->cObjects)
        {
            status = MTPERROR_DATABASE_INVALID_HANDLE;
            goto Exit;
        }

        /* <TODO>
         * Check if the parent, storage and format code match
         */

        cItems = 1;

        /* Allocate memory for the item */
        pItems = (MTPITEM*)PSLMemAlloc(PSLMEM_PTR,
                            sizeof(MTPOBJECT) * cItems);
        if (PSLNULL == pItems)
        {
            status = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }
        if (MTPObjectIsValid(pmdsi->pmoList[dwObjectHandle]))
        {
            pItems[0] = pmdsi->pmoList[dwObjectHandle];
        }
        else
        {
            status = MTPERROR_DATABASE_INVALID_HANDLE;
            goto Exit;
        }
    }
    else
    {
        /* Iterate through the object list and for objects that
         * match the above query criterion and get the count
         */
        for (dwObjIndex = _dwObjectHandleStart, dwItemIndex = 0;
             dwObjIndex < pmdsi->cObjects + _dwObjectHandleStart;
             dwObjIndex++)
        {
            if (PSLFALSE ==
                    MTPObjectIsValid(pmdsi->pmoList[dwObjIndex]))
            {
                /* skip if the object is not valid */
                continue;
            }

            dwStorageIdCmp = MTP_STORAGEID_UNDEFINED;
            dwParentHandleCmp = MTP_OBJECTHANDLE_UNDEFINED;
            wFormatCodeCmp = MTP_FORMATCODE_NOTUSED;

            if (MTP_STORAGEID_UNDEFINED != dwStorageId)
            {
                mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
                mpQuad.wDataType = MTP_DATATYPE_UINT32;
                mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

                status = MTPObjectGetProp(pmdsi->pmoList[dwObjIndex],
                              &mpQuad, 1,
                              MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                             MTPDSDATAFORMAT_NONE),
                              (PSLBYTE*)&dwStorageIdCmp,
                              sizeof(PSLUINT32),
                              PSLNULL, PSLNULL);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
            }

            if (MTP_OBJECTHANDLE_UNDEFINED != dwParentHandle)
            {
                mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
                mpQuad.wDataType = MTP_DATATYPE_UINT32;
                mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PARENT;

                status = MTPObjectGetProp(pmdsi->pmoList[dwObjIndex],
                              &mpQuad, 1,
                              MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                             MTPDSDATAFORMAT_NONE),
                              (PSLBYTE*)&dwParentHandleCmp,
                              sizeof(PSLUINT32), PSLNULL, PSLNULL);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
            }

            if (MTP_FORMATCODE_NOTUSED != wFormatCode)
            {
                mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
                mpQuad.wDataType = MTP_DATATYPE_UINT32;
                mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFORMAT;

                status = MTPObjectGetProp(pmdsi->pmoList[dwObjIndex],
                              &mpQuad, 1,
                              MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                             MTPDSDATAFORMAT_NONE),
                              (PSLBYTE*)&wFormatCodeCmp,
                              sizeof(PSLUINT16), PSLNULL, PSLNULL);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
            }

            if ((MTP_OBJECTHANDLE_ALL == dwObjectHandle) ||
                (MTP_STORAGEID_UNDEFINED == dwStorageId ||
                 dwStorageIdCmp == dwStorageId ||
                 0xFFFFFFFF == dwStorageId) &&
                (MTP_OBJECTHANDLE_UNDEFINED == dwParentHandle ||
                 dwParentHandle == dwParentHandleCmp) &&
                (MTP_FORMATCODE_NOTUSED == wFormatCode ||
                 wFormatCode == wFormatCodeCmp))
            {
                cItems++;

                /* Allocate memory for the item */
                pItemsTemp = (MTPITEM*)PSLMemReAlloc(pItems,
                                        sizeof(MTPOBJECT) * cItems,
                                (PSLMEM_FIXED | PSLMEM_RELOCATABLE));
                if (PSLNULL == pItemsTemp)
                {
                    status = PSLERROR_OUT_OF_MEMORY;
                    goto Exit;
                }
                pItems = pItemsTemp;

                pItems[dwItemIndex] = pmdsi->pmoList[dwObjIndex];
                dwItemIndex++;
            }
        }
    }

    *ppItemList = pItems;
    pItems = PSLNULL;

    *pcItems = cItems;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS _QueryByDepth(MTPDATASTORE mds, PSLUINT32 dwParentHandle,
                        PSLUINT16 wFormatCode, PSLUINT32 dwDepth,
                        MTPITEM** ppItemList, PSLUINT32* pcItems)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi;
    PSLUINT32 dwObjIndex;
    MTPOBJECT* pItems = PSLNULL;
    PSLUINT32 cItems = 0;
    PSLUINT32 dwItemIndex = 0;
    MTPOBJECT* pItemsTemp;

    PSLUINT32 dwParentHandleCmp;
    PSLUINT16 wFormatCodeCmp;
    PSLUINT32 dwDepthCmp;
    PSLUINT32 dwDepthParent;
    PSLUINT32 dwDepthChild;

    PSLBOOL fQueryOver;
    MTPDBVARIANTPROP mpQuad = {0};
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL != ppItemList)
    {
        *ppItemList = PSLNULL;
    }

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    if (PSLNULL == mds)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    /* invalidate all the depth properties */
    for (dwObjIndex = _dwObjectHandleStart;
         dwObjIndex < pmdsi->cObjects + _dwObjectHandleStart;
         dwObjIndex++)
    {
        if (PSLFALSE ==
                MTPObjectIsValid(pmdsi->pmoList[dwObjIndex]))
        {
            /* skip if the object is not valid */
            continue;
        }

        MTPObjectSetDepth(pmdsi->pmoList[dwObjIndex],
                          MTP_DEPTH_INVALID);
    }

    if (MTP_DEPTH_ALL != dwDepth)
    {
        dwDepth += 1;
    }

    do
    {
        /* start with flag set as over */
        fQueryOver = PSLTRUE;

        for (dwObjIndex = _dwObjectHandleStart;
             dwObjIndex < pmdsi->cObjects + _dwObjectHandleStart;
             dwObjIndex++)
        {
            if (PSLFALSE ==
                    MTPObjectIsValid(pmdsi->pmoList[dwObjIndex]))
            {
                /* skip if the object is not valid */
                continue;
            }

            /* At this point parent handle will be
             * the handle of a specific object or
             * that of root
             */

            PSLASSERT(MTP_OBJECTHANDLE_UNDEFINED != dwParentHandle);

            dwParentHandleCmp = MTP_OBJECTHANDLE_UNDEFINED;

            /* Get the depth */
            MTPObjectGetDepth(pmdsi->pmoList[dwObjIndex], &dwDepthCmp);

            /* If the depth is invalid */
            if (MTP_DEPTH_INVALID == dwDepthCmp)
            {
                /* If the query starts from this object it will get
                 * the depth value that was passed in. Its children
                 * will get depths that are decremented from its depth
                 */
                if (dwObjIndex == dwParentHandle)
                {
                    MTPObjectSetDepth(pmdsi->pmoList[dwObjIndex],
                                      dwDepth);
                }
                else
                {
                    /* Get the parent */
                    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
                    mpQuad.wDataType = MTP_DATATYPE_UINT32;
                    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PARENT;

                    status = MTPObjectGetProp(pmdsi->pmoList[dwObjIndex],
                                  &mpQuad, 1,
                                  MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                                 MTPDSDATAFORMAT_NONE),
                                  (PSLBYTE*)&dwParentHandleCmp,
                                  sizeof(PSLUINT32), PSLNULL, PSLNULL);
                    if (PSL_FAILED(status))
                    {
                        goto Exit;
                    }

                    if (MTP_OBJECTHANDLE_ROOT != dwParentHandleCmp)
                    {
                        PSLASSERT(dwParentHandleCmp <= pmdsi->cObjects);
                        if (dwParentHandleCmp > pmdsi->cObjects)
                        {
                            PSLASSERT(PSLFALSE);
                            status = PSLERROR_UNEXPECTED;
                            goto Exit;
                        }

                        if (PSLFALSE ==
                                MTPObjectIsValid(
                                    pmdsi->pmoList[dwParentHandleCmp]))
                        {
                            PSLASSERT(PSLFALSE);
                            status = PSLERROR_UNEXPECTED;
                            goto Exit;
                        }
                    }

                    dwDepthParent = MTP_DEPTH_INVALID;

                    if (MTP_OBJECTHANDLE_ROOT == dwParentHandleCmp)
                    {
                        if (MTP_OBJECTHANDLE_ROOT != dwParentHandle)
                        {
                            /* If parent is root and query is not from
                             * root this object is out of the query
                             */
                            MTPObjectSetDepth(
                                        pmdsi->pmoList[dwObjIndex], 0);
                            continue;
                        }
                        else
                        {
                            /* If this objects parent is root and query
                             * is from root depth of this object will be
                             * calculated from depth that was passed in.
                             */
                            dwDepthParent = dwDepth;
                        }
                    }
                    else
                    {
                        /* If the parent is different get its depth value
                         */
                        MTPObjectGetDepth(
                                    pmdsi->pmoList[dwParentHandleCmp],
                                    &dwDepthParent);
                    }

                    /* if it is valid subtract one and set it as
                     * depth of this object
                     */
                    if (MTP_DEPTH_INVALID != dwDepthParent)
                    {
                        /* if subtractaction results in zero or
                         * less than zero set zero
                         */
                        dwDepthChild = (0 == dwDepthParent) ? 0 :
                                        dwDepthParent - 1;
                        MTPObjectSetDepth(pmdsi->pmoList[dwObjIndex],
                                          dwDepthChild);
                    }
                    else
                    {
                        /* if it is invalid go to the next item but set
                         * the flag as not over
                         */
                        fQueryOver = PSLFALSE;
                        continue;
                    }
                }
            }
            /* If the depth is valid go to next item */
        }

        /* If the flag is set to not over go back to the loop with flag
         * set as over
         */
    }while(PSLFALSE ==  fQueryOver);

    PSLASSERT(0 == cItems);
    PSLASSERT(0 == dwItemIndex);

    /* If the flag is set to over iterate through the list again and get
     * all objects that has depth greater than 0
     */
    for (dwObjIndex = _dwObjectHandleStart;
         dwObjIndex < pmdsi->cObjects + _dwObjectHandleStart;
         dwObjIndex++)
    {
        if (PSLFALSE ==
                MTPObjectIsValid(pmdsi->pmoList[dwObjIndex]))
        {
            /* skip if the object is not valid */
            continue;
        }

        wFormatCodeCmp = MTP_FORMATCODE_NOTUSED;

        if (MTP_FORMATCODE_NOTUSED != wFormatCode)
        {
            mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
            mpQuad.wDataType = MTP_DATATYPE_UINT32;
            mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFORMAT;

            status = MTPObjectGetProp(pmdsi->pmoList[dwObjIndex],
                          &mpQuad, 1,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)&wFormatCodeCmp,
                          sizeof(PSLUINT16), PSLNULL, PSLNULL);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
        }

        if (MTP_FORMATCODE_NOTUSED != wFormatCode &&
             wFormatCode != wFormatCodeCmp)
        {
            //MTPObjectSetDepth(pmdsi->pmoList[dwObjIndex], 0);
            /* skip if the format codes doesn't match */
            continue;
        }


        MTPObjectGetDepth(pmdsi->pmoList[dwObjIndex], &dwDepthCmp);

        if (0 < dwDepthCmp)
        {
            cItems++;

            /* Allocate memory for the item */
            pItemsTemp = (MTPITEM*)PSLMemReAlloc(pItems,
                                    sizeof(MTPOBJECT) * cItems,
                            (PSLMEM_FIXED | PSLMEM_RELOCATABLE));
            if (PSLNULL == pItemsTemp)
            {
                status = PSLERROR_OUT_OF_MEMORY;
                goto Exit;
            }
            pItems = pItemsTemp;

            pItems[dwItemIndex] = pmdsi->pmoList[dwObjIndex];
            dwItemIndex++;
        }
    }

    *ppItemList = pItems;
    pItems = PSLNULL;

    *pcItems = cItems;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS _InsertObject(MTPDATASTORE mds,
        PSL_CONST MTPDBVARIANTPROP* pmqList, PSLUINT32 cmqList,
        MTPITEM** ppItemList, PSLUINT32* pcItems)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi;
    PSLUINT32 dwIndex;
    PSLUINT32 dwStorageId = MTP_STORAGEID_UNDEFINED;
    PSLUINT32 dwParentHandle = MTP_OBJECTHANDLE_UNDEFINED;
    PSLUINT16 wFormatCode = MTP_FORMATCODE_NOTUSED;
    PSLUINT32 dwObjectSizeMSB = MAXDWORD;
    PSLUINT32 dwObjectSizeLSB = MAXDWORD;
    MTPOBJECT mtpObject;
    PSLUINT32 dwObjectHandle;
    MTPDBVARIANTPROP mtpQuad = {0};
    MTPOBJECT* pItems = PSLNULL;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL != ppItemList)
    {
        *ppItemList = PSLNULL;
    }

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    if (PSLNULL == mds || PSLNULL == ppItemList ||
        PSLNULL == pcItems || PSLNULL == pmqList || 0 == cmqList)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    for (dwIndex = 0; dwIndex < cmqList; dwIndex++)
    {
        switch(pmqList[dwIndex].wPropCode)
        {
            case MTP_OBJECTPROPCODE_STORAGEID:
                {
                    PSLASSERT(0 == dwIndex);
                    dwStorageId = pmqList[dwIndex].vParam.valUInt32;
                }
                break;
            case MTP_OBJECTPROPCODE_PARENT:
                {
                    PSLASSERT(1 == dwIndex);
                    dwParentHandle = pmqList[dwIndex].vParam.valUInt32;
                }
                break;
            case MTP_OBJECTPROPCODE_OBJECTFORMAT:
                {
                    PSLASSERT(2 == dwIndex);
                    wFormatCode = (PSLUINT16)pmqList[dwIndex].vParam.valUInt16;
                }
                break;
            case MTP_OBJECTPROPCODE_OBJECTSIZE:
                {
                    PSLASSERT(3 == dwIndex);
                    if (3 == dwIndex)
                    {
                        dwObjectSizeMSB = (PSLUINT32)(pmqList[dwIndex].vParam.valUInt64 >> 32);
                        dwObjectSizeLSB = (PSLUINT32)pmqList[dwIndex].vParam.valUInt64;
                    }
                }
                break;
        }
    }

    /* If the storage ID is 0 get the default store */
    if (MTP_STORAGEID_UNDEFINED == dwStorageId)
    {
        status = _GetDefaultStoreId(mds, &dwStorageId);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    /* If the parent handle is 0 get the root parent */
    if (MTP_OBJECTHANDLE_UNDEFINED == dwParentHandle)
    {
        dwParentHandle = MTP_OBJECTHANDLE_ROOT;
    }

    /* Add the object to the object list */
    status = _AddObject(mds, dwStorageId, dwParentHandle, PSLNULL,
                PSLNULL, dwObjectSizeMSB, dwObjectSizeLSB,
                PSLNULL, PSLNULL, INVALID_FILE_ATTRIBUTES,
                &mtpObject, &dwObjectHandle);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (MTP_FORMATCODE_NOTUSED != wFormatCode)
    {
        /* Set the format code */
        mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mtpQuad.wDataType = MTP_DATATYPE_UINT32;
        mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFORMAT;

        status = MTPObjectSetProp(mtpObject, &mtpQuad, 1,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)&wFormatCode, sizeof(PSLUINT16),
                          PSLFALSE);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    pItems = (MTPOBJECT*)PSLMemAlloc(PSLMEM_PTR, sizeof(MTPOBJECT));
    if (PSLNULL == pItems)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    pItems[0] = mtpObject;

    *ppItemList = pItems;
    pItems = PSLNULL;

    *pcItems = 1;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

/* Current implementation of this function returns the Id of the
 * first store, this could be modified to return the Id of the
 * store that has more free space or other such logic
 */
PSLSTATUS _GetDefaultStoreId(MTPDATASTORE mds, PSLUINT32* pdwStoreId)
{
    PSLSTATUS status;
    MTPDATASTOREIMPL* pmdsi;
    PSLUINT32 dwStoreId;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL == mds || PSLNULL == pdwStoreId)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmdsi = (MTPDATASTOREIMPL*)mds;

    /* Acquire store level lock */
    status = PSLMutexAcquire(pmdsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmdsi->hMutex;

    status = MTPStoreGetProp(pmdsi->pmsList[_dwStoreIdStart],
                            PSLNULL, 0,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_ITEMID),
                            (PSLBYTE*)&dwStoreId, sizeof(PSLUINT32),
                            PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *pdwStoreId = dwStoreId;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return  status;
}

/* _SetDateProperty
 *
 * Used to set date created and date modified object properties
 *
 */
PSLSTATUS _SetDateProperty(MTPOBJECT mtpObject, FILETIME* pDate,
                            PSLUINT16 wPropCode)
{
    PSLSTATUS status;
    HRESULT hr;
    SYSTEMTIME sysTime = {0};
    WCHAR szDateFmt[] = L"%04d%02d%02dT%02d%02d%02d.%01d";
    WCHAR szDate[MAX_MTP_STRING_LEN];
    PSLUINT32 cchDate;
    MTPDBVARIANTPROP mopQuad = {0};

    if (PSLNULL == mtpObject || PSLNULL == pDate)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    ASSERT(wPropCode == MTP_OBJECTPROPCODE_DATECREATED ||
           wPropCode == MTP_OBJECTPROPCODE_DATEMODIFIED);

    if(FALSE == FileTimeToSystemTime(pDate, &sysTime))
    {
        status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        goto Exit;
    }

    hr = StringCchPrintfW(szDate, MAX_MTP_STRING_LEN, szDateFmt,
                              sysTime.wYear, sysTime.wMonth,
                              sysTime.wDay, sysTime.wHour,
                              sysTime.wMinute, sysTime.wSecond,
                              (sysTime.wMilliseconds)/100);
    if (FAILED(hr))
    {
        status = hr;
        goto Exit;
    }

    status = PSLStringLen((PSLWCHAR*)szDate, MAX_MTP_STRING_LEN,
                          &cchDate);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    mopQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mopQuad.wDataType = MTP_DATATYPE_UINT32;
    mopQuad.vParam.valInt32 = wPropCode;

    status = MTPObjectSetProp(mtpObject, &mopQuad, 1,
                      MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                     MTPDSDATAFORMAT_NONE),
                      (PSLBYTE*)szDate,
                      (cchDate + 1) * sizeof(PSLWCHAR), PSLFALSE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
Exit:
    return status;
}
