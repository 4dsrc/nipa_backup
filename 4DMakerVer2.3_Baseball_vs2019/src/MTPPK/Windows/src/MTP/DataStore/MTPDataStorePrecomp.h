/*
 *  MTPDataStorePrecomp.h
 *
 *  Precompiled header file for data store support in windows.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPDATASTOREPRECOMP_H_
#define _MTPDATASTOREPRECOMP_H_

#include "PSLWindows.h"
#include "PSL.h"
#include "MTP.h"
#include "MTPUtils.h"

#include "PSLWinSafe.h"
#endif  /* _MTPDATASTOREPRECOMP_H_ */

