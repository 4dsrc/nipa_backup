/*
 *  MTPDataStoreUtil.c
 *
 *  Contains implementation of utility functions used by MTP datastore
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPDataStorePreComp.h"
#include "MTPDataStoreUtil.h"

typedef struct _FORMATEXTTABLE
{
    PSLUINT16   wFormatCode;
    LPCWSTR     wszFileExt;
} FORMATEXTTABLE;

static const FORMATEXTTABLE _RgFormatExtTable[] =
{
    {MTP_FORMATCODE_WAVE,                       L".wav" },
    {MTP_FORMATCODE_MP3,                        L".mp3" },
    {MTP_FORMATCODE_WMA,                        L".wma" },
    {MTP_FORMATCODE_ASF,                        L".asf" },
    {MTP_FORMATCODE_AVI,                        L".avi" },
    {MTP_FORMATCODE_WMV,                        L".wmv" },
    {MTP_FORMATCODE_MP4,                        L".mp4" },
    {MTP_FORMATCODE_IMAGE_BMP,                  L".bmp" },
    {MTP_FORMATCODE_IMAGE_JP2,                  L".jpeg"},
    {MTP_FORMATCODE_IMAGE_JPX,                  L".jpeg"},
    {MTP_FORMATCODE_IMAGE_EXIF,                 L".jpg" },
    {MTP_FORMATCODE_IMAGE_TIFF,                 L".tif" },
    {MTP_FORMATCODE_IMAGE_GIF,                  L".gif" },
    {MTP_FORMATCODE_IMAGE_PNG,                  L".png" },
    {MTP_FORMATCODE_M3UPLAYLIST,                L".m3u" },
    {MTP_FORMATCODE_ASXPLAYLIST,                L".asx" },
    {MTP_FORMATCODE_ABSTRACTAUDIOVIDEOPLAYLIST, L".pla" },
};


PSLSTATUS GetMTPFormatByFileExt(LPCWSTR szFileExt,
                                PSLUINT16* pwFormatCode)
{
    PSLSTATUS status;
    PSLUINT32 dwIndex;
    PSLUINT16 wFormatCode = MTP_FORMATCODE_UNDEFINED;

    if (PSLNULL != pwFormatCode)
    {
        *pwFormatCode = 0;
    }

    /* szFileExt can be PSLNULL */
    if (PSLNULL == pwFormatCode)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (PSLNULL == szFileExt)
    {
        *pwFormatCode = MTP_FORMATCODE_UNDEFINED;
        status = PSLSUCCESS;
        goto Exit;
    }

    status = PSLSUCCESS_NOT_FOUND;
    for (dwIndex = 0; dwIndex < PSLARRAYSIZE(_RgFormatExtTable);
         dwIndex++)
    {
        if(0 == _wcsicmp(_RgFormatExtTable[dwIndex].wszFileExt,
                         szFileExt))
        {
            wFormatCode = _RgFormatExtTable[dwIndex].wFormatCode;
            status = PSLSUCCESS;
            break;
        }
    }

    *pwFormatCode = wFormatCode;
Exit:
    return status;
}
