/*
 *  MTPObject.h
 *
 *  Contains declaration of MTP object.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPOBJECT_H_
#define _MTPOBJECT_H_

typedef PSLHANDLE MTPOBJECT;

PSL_EXTERN_C PSLSTATUS PSL_API MTPObjectCreate(
                            MTPDATASTORE mds,
                            PSLUINT32 dwObjectHandle,
                            MTPOBJECT* pmtpObject);

PSL_EXTERN_C PSLSTATUS PSL_API MTPObjectDestroy(
                            MTPOBJECT mtpObject);

PSL_EXTERN_C PSLSTATUS PSL_API MTPObjectGetProp(
                            MTPOBJECT mtpObject,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            PSLUINT32 dwDataFormat,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten,
                            PSLUINT32* pcbLeft);

PSL_EXTERN_C PSLSTATUS PSL_API MTPObjectSetProp(
                            MTPOBJECT mtpObject,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            PSLUINT32 dwDataFormat,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLBOOL fFromWire);

PSL_EXTERN_C PSLSTATUS PSL_API MTPObjectDelete(
                            MTPOBJECT mtpObject);

PSL_EXTERN_C PSLBOOL PSL_API MTPObjectIsValid(
                            MTPOBJECT mtpObject);

PSL_EXTERN_C PSLSTATUS PSL_API MTPObjectSetDepth(
                            MTPOBJECT mtpObject,
                            PSLUINT32 dwDepth);

PSL_EXTERN_C PSLSTATUS PSL_API MTPObjectGetDepth(
                            MTPOBJECT mtpObject,
                            PSLUINT32* pdwDepth);

/*
 *  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#define SAFE_MTPOBJECTDESTROY(_mtpo) \
if (PSLNULL != _mtpo) \
{ \
    MTPObjectDestroy(_mtpo); \
    _mtpo = PSLNULL; \
}

#endif  /*_MTPOBJECT_H_*/
