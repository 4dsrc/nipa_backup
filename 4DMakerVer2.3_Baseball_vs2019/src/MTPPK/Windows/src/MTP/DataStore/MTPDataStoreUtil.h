/*
 *  MTPDataStoreUtil.h
 *
 *  Contains decalartion of utility functions used by MTP datastore
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPDATASTOREUTIL_H_
#define _MTPDATASTOREUTIL_H_

PSL_EXTERN_C PSLSTATUS PSL_API GetMTPFormatByFileExt(
                            LPCWSTR szFileExt,
                            PSLUINT16* pwFormatCode);


#endif  /* _MTPDATASTOREUTIL_H_ */
