/*
 *  MTPObject.c
 *
 *  Contains implementation of the MTP object
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPDataStorePrecomp.h"
#include "MTPDataStore.h"
#include "MTPDataStoreCommon.h"
#include "MTPStore.h"
#include "MTPObject.h"
#include "FileUtils.h"
#include "MTPObjectProperty.h"

typedef struct _MTPOBJPROPFIELD
{
    PSLUINT32 cbCurVal;
    PSLBYTE* pbCurVal;
} MTPOBJPROPFIELD;

typedef struct _MTPOBJECTIMPL
{
    MTPITEMIMPL miImp;
    PSLHANDLE hMutex;
    PSLUINT32 dwObjectHandle;
    PSLBOOL fValid;
    MTPDATASTORE mds;
    PSLUINT32 dwDepth;
    PSLUINT32* pdwReferences;
    PSLUINT32 cdwReferences;
    DSSTRING mstrFilePath;
    MTPOBJPROPFIELD* pPropField;
}MTPOBJECTIMPL;

/*
 *  This function will get the values of fields that correspond
 *  to the object info data set, and pack it into the input buffer.
 *  If the input buffer is not large enough to hold the object info
 *  data set the function will fail
 */
static PSLSTATUS _SerializeObjectInfo(
                            MTPOBJECT mtpObject,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten,
                            PSLUINT32* pcbLeft);

/*
 *  This function will unpack the input buffer and update the fields
 *  that correspond to the object info data set
 */
static PSLSTATUS _DeserializeObjectInfo(
                            MTPOBJECT mtpObject,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf);

/*
 *  This function will get the prop quad and pack in the input buffer.
 *  If the input buffer is not large enough to hold the data
 *  the function will fail.
 */
static PSLSTATUS _SerializeObjectPropList(
                            MTPOBJECT mtpObject,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten,
                            PSLUINT32* pcbLeft);

/*
 *  This function will unpack the input buffer and update the field
 *  that correspond to the object prop quad that is present in
 *  buffer
 */
static PSLSTATUS _DeserializeObjectPropList(
                            MTPOBJECT mtpObject,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf);

/*
 *  This function will get the value of the field that correspond
 *  to property code that was queried and pack it into the input buffer.
 *  If the input buffer is not large enough to hold the data
 *  the function will fail.
 */
static PSLSTATUS _SerializeObjectPropValue(
                            MTPOBJECT mtpObject,
                            PSLUINT16 wPropCode,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten,
                            PSLUINT32* pcbLeft);

/*
 *  This function will unpack the input buffer and update the fields
 *  that correspond to the object propety code that were present in
 *  SetObjectPropValue
 */
static PSLSTATUS _DeserializeObjectPropValue(
                            MTPOBJECT mtpObject,
                            PSLUINT16 wPropCode,
                            PSLUINT32 dwDataFormat,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLBOOL fFromWire);

static PSLSTATUS _GetPropIndexFromPropCode(
                            PSLUINT16 wPropCode,
                            PSLUINT32* pdwIndex);

static PSLSTATUS _ReleaseObjectPropValues(
                            MTPOBJECT mtpObject);

static PSLSTATUS _MTPObjectDeleteChildren(
                            MTPOBJECT mtpObject);

static PSLSTATUS _MTPObjectIsAssociation(
                            MTPOBJECT mtpObject,
                            PSLBOOL* pfAssoc);

static PSLSTATUS _MTPObjectSetFilePath(
                            MTPOBJECT mtpObject);

/*
 *  This function will get object references.
 */
static PSLSTATUS _SerializeObjectReferences(
                            MTPOBJECT mtpObject,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten,
                            PSLUINT32* pcbLeft);

/*
 *  This function will set object references.
 */
static PSLSTATUS _DeserializeObjectReferences(
                            MTPOBJECT mtpObject,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLBOOL fFromWire);

/*
*/
static PSLSTATUS GetMtpPropTypeAndSize(WORD wPropCode,
                                       WORD* pwDataType,
                                       DWORD* pdwSize);

static const WCHAR _RgchObjectInitMutex[] =  L"MTPObject Init Mutex";

static const PSLUINT16 _RgdwPropCodes[] =
{
    MTP_OBJECTPROPCODE_STORAGEID,
    MTP_OBJECTPROPCODE_OBJECTFORMAT,
    MTP_OBJECTPROPCODE_PROTECTIONSTATUS,
    MTP_OBJECTPROPCODE_OBJECTSIZE,
    MTP_OBJECTPROPCODE_ASSOCIATIONTYPE,
    MTP_OBJECTPROPCODE_ASSOCIATIONDESC,
    MTP_OBJECTPROPCODE_OBJECTFILENAME,
    MTP_OBJECTPROPCODE_DATECREATED,
    MTP_OBJECTPROPCODE_DATEMODIFIED,
    MTP_OBJECTPROPCODE_HIDDEN,
    MTP_OBJECTPROPCODE_PARENT,
    MTP_OBJECTPROPCODE_PERSISTENTGUID,
    MTP_OBJECTPROPCODE_NAME,
    MTP_OBJECTPROPCODE_ARTIST,
    MTP_OBJECTPROPCODE_NONCONSUMABLE,
    MTP_OBJECTPROPCODE_WIDTH,
    MTP_OBJECTPROPCODE_HEIGHT,
    MTP_OBJECTPROPCODE_DURATION,
    MTP_OBJECTPROPCODE_USERRATING,
    MTP_OBJECTPROPCODE_TRACK,
    MTP_OBJECTPROPCODE_GENRE,
    MTP_OBJECTPROPCODE_USECOUNT,
    MTP_OBJECTPROPCODE_ALBUMNAME,
    MTP_OBJECTPROPCODE_ALBUMARTIST,
    MTP_OBJECTPROPCODE_DRMPROTECTION,
    MTP_OBJECTPROPCODE_DISPLAYNAME,
    MTP_OBJECTPROPCODE_BODYTEXT,
    MTP_OBJECTPROPCODE_SUBJECT,
    MTP_OBJECTPROPCODE_PRIORITY,
    MTP_OBJECTPROPCODE_GIVENNAME,
    MTP_OBJECTPROPCODE_MIDDLENAMES,
    MTP_OBJECTPROPCODE_FAMILYNAME,
    MTP_OBJECTPROPCODE_PREFIX,
    MTP_OBJECTPROPCODE_SUFFIX,
    MTP_OBJECTPROPCODE_PHONETICGIVENNAME,
    MTP_OBJECTPROPCODE_PHONETICFAMILYNAME,
    MTP_OBJECTPROPCODE_EMAILPRIMARY,
    MTP_OBJECTPROPCODE_EMAILPERSONAL1,
    MTP_OBJECTPROPCODE_EMAILPERSONAL2,
    MTP_OBJECTPROPCODE_EMAILBUSINESS1,
    MTP_OBJECTPROPCODE_EMAILBUSINESS2,
    MTP_OBJECTPROPCODE_EMAILOTHERS,
    MTP_OBJECTPROPCODE_PHONENUMBERPRIMARY,
    MTP_OBJECTPROPCODE_PHONENUMBERPERSONAL,
    MTP_OBJECTPROPCODE_PHONENUMBERPERSONAL2,
    MTP_OBJECTPROPCODE_PHONENUMBERBUSINESS,
    MTP_OBJECTPROPCODE_PHONENUMBERBUSINESS2,
    MTP_OBJECTPROPCODE_PHONENUMBERMOBILE,
    MTP_OBJECTPROPCODE_PHONENUMBERMOBILE2,
    MTP_OBJECTPROPCODE_FAXNUMBERPRIMARY,
    MTP_OBJECTPROPCODE_FAXNUMBERPERSONAL,
    MTP_OBJECTPROPCODE_FAXNUMBERBUSINESS,
    MTP_OBJECTPROPCODE_PAGERNUMBER,
    MTP_OBJECTPROPCODE_PHONENUMBEROTHERS,
    MTP_OBJECTPROPCODE_PRIMARYWEBADDRESS,
    MTP_OBJECTPROPCODE_PERSONALWEBADDRESS,
    MTP_OBJECTPROPCODE_BUSINESSWEBADDRESS,
    MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS,
    MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS2,
    MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS3,
    MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALFULL,
    MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALLINE1,
    MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALLINE2,
    MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALCITY,
    MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALREGION,
    MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALPOSTALCODE,
    MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALCOUNTRY,
    MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSFULL,
    MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSLINE1,
    MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSLINE2,
    MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSCITY,
    MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSREGION,
    MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSPOSTALCODE,
    MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSCOUNTRY,
    MTP_OBJECTPROPCODE_POSTALADDRESSOTHERFULL,
    MTP_OBJECTPROPCODE_POSTALADDRESSOTHERLINE1,
    MTP_OBJECTPROPCODE_POSTALADDRESSOTHERLINE2,
    MTP_OBJECTPROPCODE_POSTALADDRESSOTHERCITY,
    MTP_OBJECTPROPCODE_POSTALADDRESSOTHERREGION,
    MTP_OBJECTPROPCODE_POSTALADDRESSOTHERPOSTALCODE,
    MTP_OBJECTPROPCODE_POSTALADDRESSOTHERCOUNTRY,
    MTP_OBJECTPROPCODE_ORGANIZATIONNAME,
    MTP_OBJECTPROPCODE_PHONETICORGANIZATIONNAME,
    MTP_OBJECTPROPCODE_ROLE,
    MTP_OBJECTPROPCODE_BIRTHDAY,
    MTP_OBJECTPROPCODE_MESSAGETO,
    MTP_OBJECTPROPCODE_MESSAGECC,
    MTP_OBJECTPROPCODE_MESSAGEBCC,
    MTP_OBJECTPROPCODE_MESSAGEREAD,
    MTP_OBJECTPROPCODE_MESSAGERECEIVETIME,
    MTP_OBJECTPROPCODE_MESSAGESENDER,
    MTP_OBJECTPROPCODE_ACTIVITYBEGINTIME,
    MTP_OBJECTPROPCODE_ACTIVITYENDTIME,
    MTP_OBJECTPROPCODE_ACTIVITYLOCATION,
    MTP_OBJECTPROPCODE_ACTIVITYREQUIREDATTENDEES,
    MTP_OBJECTPROPCODE_ACTIVITYOPTIONALATTENDEES,
    MTP_OBJECTPROPCODE_ACTIVITYRESOURCES,
    MTP_OBJECTPROPCODE_ACTIVITYACCEPTEDED,
    MTP_OBJECTPROPCODE_ACTIVITYTENTATIVE,
    MTP_OBJECTPROPCODE_ACTIVITYDECLINED,
    MTP_OBJECTPROPCODE_ACTIVITYREMINDERTIME,
    MTP_OBJECTPROPCODE_ACTIVITYOWNER,
    MTP_OBJECTPROPCODE_ACTIVITYSTATUS,
    MTP_OBJECTPROPCODE_BITRATETYPE,
    MTP_OBJECTPROPCODE_SAMPLERATE,
    MTP_OBJECTPROPCODE_NUMBEROFCHANNELS,
    MTP_OBJECTPROPCODE_AUDIOBITDEPTH,
    MTP_OBJECTPROPCODE_SCANTYPE,
    MTP_OBJECTPROPCODE_AUDIOWAVECODEC,
    MTP_OBJECTPROPCODE_AUDIOBITRATE,
    MTP_OBJECTPROPCODE_VIDEOFOURCCCODEC,
    MTP_OBJECTPROPCODE_VIDEOBITRATE,
    MTP_OBJECTPROPCODE_FRAMESPERMILLISECOND,
    MTP_OBJECTPROPCODE_KEYFRAMEDISTANCE,
    MTP_OBJECTPROPCODE_ENCODINGQUALITY,
    MTP_OBJECTPROPCODE_ENCODINGPROFILE
};

const static PSLUINT32 OBJECTINFO_FIXED_MEMBER_SIZE =
                        sizeof(PSLUINT32) + /* dwStorageID       */
                        sizeof(PSLUINT16) + /* wObjectFormat     */
                        sizeof(PSLUINT16) + /* wProtectionStatus */
                        sizeof(PSLUINT32) + /* dwObjCompSize     */
                        sizeof(PSLUINT16) + /* wThumbFormat      */
                        sizeof(PSLUINT32) + /* dwThumbCompSize   */
                        sizeof(PSLUINT32) + /* dwThumbPixWidth   */
                        sizeof(PSLUINT32) + /* dwThumbPixHeight  */
                        sizeof(PSLUINT32) + /* dwImgPixWidth     */
                        sizeof(PSLUINT32) + /* dwImgPixHeight    */
                        sizeof(PSLUINT32) + /* dwImgBitDepth     */
                        sizeof(PSLUINT32) + /* dwParentObject    */
                        sizeof(PSLUINT16) + /* wAssocType        */
                        sizeof(PSLUINT32) + /* dwAssocDesc       */
                        sizeof(PSLUINT32);  /* wSeqNumber        */

const static PSLUINT32 MAX_MTP_STRING_SIZE = sizeof(PSLWCHAR) * \
                                             MAX_MTP_STRING_LEN + \
                                             sizeof(PSLBYTE);

/*
 *  MTPObjectCreate
 *
 *
 */
PSLSTATUS MTPObjectCreate(MTPDATASTORE mds, PSLUINT32 dwObjectHandle,
                          MTPOBJECT* pmtpObject)
{
    PSLSTATUS status;
    MTPOBJECTIMPL* pmoi = PSLNULL;
    PSLHANDLE hMutex = PSLNULL;
    PSLUINT32 idx;

    if (PSLNULL != pmtpObject)
    {
        *pmtpObject = PSLNULL;
    }

    if (PSLNULL == pmtpObject)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)PSLMemAlloc(PSLMEM_FIXED,
           sizeof(MTPOBJECTIMPL) +
           (sizeof(MTPOBJPROPFIELD) * PSLARRAYSIZE(_RgdwPropCodes)));

    if (PSLNULL == pmoi)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = PSLMutexOpen(PSLTHREAD_INFINITE,
                  (PSLWCHAR*)_RgchObjectInitMutex, &hMutex);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* assign the starting point of the property field array */
    pmoi->pPropField = (MTPOBJPROPFIELD*)(pmoi + 1);

    for (idx = 0; idx < PSLARRAYSIZE(_RgdwPropCodes); idx++)
    {
        pmoi->pPropField[idx].pbCurVal = PSLNULL;
        pmoi->pPropField[idx].cbCurVal = 0;
    }

    /* Set the function pointers*/
    pmoi->miImp.pfnMTPItemGetProp = MTPObjectGetProp;
    pmoi->miImp.pfnMTPItemSetProp = MTPObjectSetProp;
    pmoi->miImp.pfnMTPItemDelete = MTPObjectDelete;

    /* Set the Object handle*/
    pmoi->dwObjectHandle = dwObjectHandle;

    /* Set as valid*/
    pmoi->fValid = PSLTRUE;

    /* Set the lock */
    /* hMutex is not set to PSLNULL as
     * it should be released in the Exit label
     */
    pmoi->hMutex = hMutex;

    pmoi->mds = mds;
    pmoi->dwDepth = 0;
    pmoi->pdwReferences = PSLNULL;
    pmoi->cdwReferences = 0;

    *pmtpObject = (MTPDATASTORE)pmoi;
    pmoi = PSLNULL;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutex)
    SAFE_PSLMEMFREE(pmoi);
    return status;
}

/*
 *  MTPObjectDestroy
 *
 *
 */
PSLSTATUS MTPObjectDestroy(MTPOBJECT mtpObject)
{
    PSLSTATUS status;
    MTPOBJECTIMPL* pmoi;

    if(PSLNULL == mtpObject)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    SAFE_PSLMUTEXRELEASECLOSE(pmoi->hMutex);

    /* Release the object prop values */
    _ReleaseObjectPropValues(mtpObject);

    SAFE_PSLMEMFREE(pmoi->pdwReferences);

    SAFE_PSLMEMFREE(pmoi);

    status = PSLSUCCESS;

Exit:
    return status;
}

/*
 *  MTPObjectGetProp
 *
 *
 */
PSLSTATUS MTPObjectGetProp(MTPOBJECT mtpObject,
                       PSL_CONST MTPDBVARIANTPROP* pmqList,
                       PSLUINT32 cmqList, PSLUINT32 dwDataFormat,
                       PSLBYTE* pbBuf, PSLUINT32 cbBuf,
                       PSLUINT32* pcbWritten, PSLUINT32* pcbLeft)
{
    PSLSTATUS status;
    MTPOBJECTIMPL* pmoi;
    PSLHANDLE hMutexRelease = PSLNULL;
    PSLUINT32 cchPathLen;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /* pcbWritten, pcbLeft & pbBuf can be PSLNULL
     * and cbBuf can be zero
     */
    if (PSLNULL == mtpObject)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    /* Acquire record level lock */
    status = PSLMutexAcquire(pmoi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmoi->hMutex;

    switch(MTPDSDATAFORMAT(dwDataFormat))
    {
        case MTPDSDATAFORMAT_ITEMID:
            {
                PSLASSERT(0 == cmqList);
                if (PSLNULL != pcbLeft)
                {
                    *pcbLeft = sizeof(PSLUINT32);
                }
                if (PSLNULL == pbBuf)
                {
                    /* making sure the caller wants the size */
                    PSLASSERT(PSLNULL != pcbLeft);
                    status = PSLSUCCESS;
                    goto Exit;
                }

                if (cbBuf < sizeof(PSLUINT32))
                {
                    status = PSLERROR_INSUFFICIENT_BUFFER;
                    goto Exit;
                }

                MTPStoreUInt32((PSLUINT32*)pbBuf, pmoi->dwObjectHandle);
            }
            break;
        case MTPDSDATAFORMAT_NONE:
            {
                if (PSLNULL == pmqList)
                {
                    status = PSLERROR_INVALID_PARAMETER;
                    goto Exit;
                }

                PSLASSERT(1 == cmqList);
                PSLASSERT(MTP_OBJECTPROPCODE_PROPCODE == pmqList[0].wPropCode);
                PSLASSERT(MTP_DATATYPE_UINT32 == pmqList[0].wDataType);

                status = _SerializeObjectPropValue(mtpObject,
                                                   (PSLUINT16)pmqList[0].vParam.valUInt32,
                                                   pbBuf, cbBuf,
                                                   pcbWritten, pcbLeft);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }

            }
            break;
        case MTPDSDATAFORMAT_OBJECTPROPLIST:
            status = _SerializeObjectPropList(mtpObject, pmqList,
                                              cmqList, pbBuf, cbBuf,
                                              pcbWritten, pcbLeft);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
            break;
        case MTPDSDATAFORMAT_OBJECTINFO:
            PSLASSERT(0 == cmqList);
            status = _SerializeObjectInfo(mtpObject, pbBuf, cbBuf,
                                          pcbWritten, pcbLeft);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
            break;
        case MTPDSDATAFORMAT_FILEPATH:
            PSLASSERT(0 == cmqList);

            /* this length will include terminating NULL */
            cchPathLen = pmoi->mstrFilePath.cchLen;

            if (PSLNULL != pcbLeft)
            {
                *pcbLeft = cchPathLen * sizeof(PSLWCHAR);
            }

            if (PSLNULL == pbBuf)
            {
                /* making sure the caller wants the size */
                PSLASSERT(PSLNULL != pcbLeft);
                status = PSLSUCCESS;
                goto Exit;
            }

            if (cbBuf < (cchPathLen * sizeof(PSLWCHAR)))
            {
                status = PSLERROR_INSUFFICIENT_BUFFER;
                goto Exit;
            }

            pbBuf[0] = (PSLBYTE)cchPathLen;

            /* <TODO>
             * When fToWire is PSLFALSE use PSLStringCopyLen
             */
            /* 1 is subtracted from cbBuf to account for
             * BYTE memory that stores the length
             */
            status = MTPStoreUTF16String((PSLWCHAR*)&pbBuf[1],
                                (cbBuf - 1)/sizeof(PSLWCHAR), PSLNULL,
                                (PSLWCHAR*)pmoi->mstrFilePath.rgStr,
                                cchPathLen);
            if (PSL_FAILED(status))
            {
                PSLASSERT(!L"MTPObjectGetProp: File path copy failed");
                goto Exit;
            }
            break;
        default:
            PSLASSERT(!L"MTPObjectGetProp: Out format not supported");
            status = PSLERROR_UNEXPECTED;
            break;
    }

    /* Release record level lock */
    status = PSLMutexRelease(pmoi->hMutex);
    PSLASSERT(PSL_SUCCEEDED(status));
    hMutexRelease = PSLNULL;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return status;
}

/*
 *  MTPObjectSetProp
 *
 *
 */
PSLSTATUS MTPObjectSetProp(MTPOBJECT mtpObject,
                           PSL_CONST MTPDBVARIANTPROP* pmqList,
                           PSLUINT32 cmqList,
                           PSLUINT32 dwDataFormat,
                           PSLBYTE* pbBuf,
                           PSLUINT32 cbBuf, PSLBOOL fFromWire)
{
    PSLSTATUS status;
    MTPOBJECTIMPL* pmoi;
    PSLHANDLE hMutexRelease = PSLNULL;

    if (PSLNULL == mtpObject || PSLNULL == pbBuf)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    /* Acquire record level lock */
    status = PSLMutexAcquire(pmoi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmoi->hMutex;

    switch(MTPDSDATAFORMAT(dwDataFormat))
    {
        case MTPDSDATAFORMAT_NONE:
            {
                if (PSLNULL == pmqList)
                {
                    status = PSLERROR_INVALID_PARAMETER;
                    goto Exit;
                }
                PSLASSERT(1 == cmqList);
                status = _DeserializeObjectPropValue(mtpObject,
                                                 (PSLUINT16)pmqList[0].vParam.valUInt32,
                                                 dwDataFormat,
                                                 pbBuf, cbBuf,
                                                 fFromWire);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }

            }
            break;
        case MTPDSDATAFORMAT_OBJECTPROPLIST:
                PSLASSERT(0 == cmqList);
                status = _DeserializeObjectPropList(mtpObject, pbBuf,
                                                    cbBuf);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
            break;
        case MTPDSDATAFORMAT_OBJECTINFO:
                PSLASSERT(0 == cmqList);
                status = _DeserializeObjectInfo(mtpObject, pbBuf,
                                                cbBuf);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
            break;
        case MTPDSDATAFORMAT_ITEMID:
            PSLASSERT(!L"Item ID is a read only property");
            break;
        case MTPDSDATAFORMAT_FILEPATH:
            PSLASSERT(0 == cmqList);

            if (cbBuf > MAX_MTP_STRING_SIZE)
            {
                status = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            if (PSLTRUE == fFromWire)
            {
                /* should include terminating NULL */
                pmoi->mstrFilePath.cchLen = pbBuf[0];

                status = MTPLoadUTF16String(
                            (PSLWCHAR*)pmoi->mstrFilePath.rgStr,
                            MAX_MTP_STRING_LEN, PSLNULL,
                            (PSLCWSTR)pbBuf,
                            pmoi->mstrFilePath.cchLen);
                if (PSL_FAILED(status))
                {
                    PSLASSERT(!L"MTPObjectSetProp: File path");
                    goto Exit;
                }
            }
            else
            {
                pmoi->mstrFilePath.cchLen =
                                        (PSLBYTE)cbBuf/sizeof(PSLWCHAR);

                status = PSLStringCopyLen(
                                (PSLWCHAR*)pmoi->mstrFilePath.rgStr,
                                MAX_MTP_STRING_LEN, PSLNULL,
                                (PSLCWSTR)pbBuf,
                                pmoi->mstrFilePath.cchLen);
                if (PSL_FAILED(status))
                {
                    PSLASSERT(!L"MTPObjectSetProp: File path");
                    goto Exit;
                }
            }
            break;
        default:
            PSLASSERT(!L"MTPObjectSetProp: Out format not supported");
            status = PSLERROR_UNEXPECTED;
            break;
    }

    /* Release record level lock */
    status = PSLMutexRelease(pmoi->hMutex);
    PSLASSERT(PSL_SUCCEEDED(status));
    hMutexRelease = PSLNULL;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return status;
cmqList;
}

/*
 *  MTPObjectDelete
 *
 *
 */
PSLSTATUS MTPObjectDelete(MTPOBJECT mtpObject)
{
    PSLSTATUS status;
    MTPOBJECTIMPL* pmoi;
    PSLHANDLE hMutexRelease = PSLNULL;
    MTPSTORE mtpStore;
    MTPDBVARIANTPROP mtpQuad;
    PSLUINT32 dwStorageId;
    PSLUINT16 wProtectionStatus;
    PSLBOOL fIsAssoc;
    PSLBYTE rgmstrFilePath[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR szFilePath[MAX_MTP_STRING_LEN];

    if(PSLNULL == mtpObject)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(PSLTRUE == MTPObjectIsValid(mtpObject));

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    /* Acquire record level lock */
    status = PSLMutexAcquire(pmoi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmoi->hMutex;

    /* Get the store Id and from that the store */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

    status = MTPObjectGetProp(mtpObject, &mtpQuad, 1,
                              MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                             MTPDSDATAFORMAT_NONE),
                              (PSLBYTE*)&dwStorageId,
                              sizeof(PSLUINT32), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*
     *  Get the store that correpond to store ID
     */
    status = MTPDataStoreGetMTPStore(pmoi->mds, dwStorageId,
                                     &mtpStore);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* If the store is read only return  error */
    if (PSLTRUE == MTPStoreIsReadOnly(mtpStore))
    {
        status = MTPERROR_DATABASE_WRITEPROTECTED_STORE;
        goto Exit;
    }

    /* If the object is read only or read only data return error */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PROTECTIONSTATUS;

    status = MTPObjectGetProp(mtpObject, &mtpQuad, 1,
                              MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                             MTPDSDATAFORMAT_NONE),
                              (PSLBYTE*)&wProtectionStatus,
                              sizeof(PSLUINT16), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Read-only
     * This object cannot be deleted or modified, and none of the
     * properties of this object can be modified by the initiator.
     * Read-only data:
     * This object�s binary component cannot be deleted or modified,
     * however any object properties may be modified if allowed by
     * the object property constraints.
     */
    if (MTP_PROTECTIONSTATUS_READONLY == wProtectionStatus ||
        MTP_PROTECTIONSTATUS_READONLYDATA == wProtectionStatus)
    {
        status = MTPERROR_DATABASE_WRITEPROTECTED_OBJECT;
        goto Exit;
    }

    /* If the object is an association delete all its children
     * first before deleting itself
     */
    status = _MTPObjectIsAssociation(mtpObject, &fIsAssoc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (PSLTRUE == fIsAssoc)
    {
        /* Delete the children */
        status = _MTPObjectDeleteChildren(mtpObject);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    /* Get the File path */
    status = MTPObjectGetProp(mtpObject, PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                     MTPDSDATAFORMAT_FILEPATH),
                        (PSLBYTE*)&rgmstrFilePath[0],
                        sizeof(PSLWCHAR) * MAX_PATH, PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(szFilePath, PSLARRAYSIZE(szFilePath),
                      PSLNULL, (PCXMTPSTRING)&rgmstrFilePath[0],
                      (rgmstrFilePath[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* remove the directory in case of folder
     * file in case of file object
     */
    if (PSLTRUE == fIsAssoc)
    {
        if( INVALID_FILE_ATTRIBUTES !=
            GetFileAttributes((LPCWSTR)szFilePath))
        {
            RemoveDirectory((LPCWSTR)szFilePath);
        }
    }
    else
    {
        if( INVALID_FILE_ATTRIBUTES !=
            GetFileAttributes((LPCWSTR)szFilePath))
        {
            DeleteFile((LPCWSTR)szFilePath);
        }
    }

    /* Continue deletion of this object */
    pmoi->fValid = PSLFALSE;

    pmoi->miImp.pfnMTPItemGetProp = PSLNULL;

    pmoi->miImp.pfnMTPItemSetProp = PSLNULL;

    pmoi->miImp.pfnMTPItemDelete = PSLNULL;

    pmoi->dwDepth = 0;

    /* Release the object prop values */
    _ReleaseObjectPropValues(mtpObject);

    SAFE_PSLMUTEXRELEASECLOSE(pmoi->hMutex);
    hMutexRelease = PSLNULL;

    status = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return status;
}

/*
 *  MTPObjectIsValid
 *
 *
 */
PSLBOOL MTPObjectIsValid(MTPOBJECT mtpObject)
{
    PSLSTATUS status;
    MTPOBJECTIMPL* pmoi;
    PSLBOOL fValid = PSLFALSE;
    MTPSTORE mtpStore;
    PSLUINT32 dwStorageId = 0;
    MTPDBVARIANTPROP mopQuad = {0};

    if (PSLNULL == mtpObject)
    {
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    fValid = pmoi->fValid;

    if (PSLFALSE == fValid)
    {
        /* if the object is invalid no more work to do*/
        goto Exit;
    }

    /* Get the storage Id property */
    mopQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mopQuad.wDataType = MTP_DATATYPE_UINT32;
    mopQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

    status = MTPObjectGetProp(mtpObject, &mopQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                 MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&dwStorageId,
                        sizeof(PSLUINT32), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*
     *  Check if the store in which the object reside is valid
     *  if not set object as invalid and return invalid
     *  This is required as the store will become invalid with out
     *  letting the object know about it.
     *  This is how we avoid object list traversal when a store
     *  gets removed
     */

    status = MTPDataStoreGetMTPStore(pmoi->mds, dwStorageId, &mtpStore);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    fValid = MTPStoreIsValid(mtpStore);

    if (PSLFALSE == fValid)
    {
        MTPObjectDelete(mtpObject);
    }

Exit:
    return fValid;
}

PSLSTATUS MTPObjectSetDepth(MTPOBJECT mtpObject, PSLUINT32 dwDepth)
{
    PSLSTATUS status;
    MTPOBJECTIMPL* pmoi;

    if (PSLNULL == mtpObject)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    pmoi->dwDepth = dwDepth;

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS MTPObjectGetDepth(MTPOBJECT mtpObject, PSLUINT32* pdwDepth)
{
    PSLSTATUS status;
    MTPOBJECTIMPL* pmoi;

    if (PSLNULL != pdwDepth)
    {
        *pdwDepth = 0;
    }

    if (PSLNULL == mtpObject || PSLNULL == pdwDepth)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    *pdwDepth = pmoi->dwDepth;;

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS _SerializeObjectInfo(MTPOBJECT mtpObject, PSLBYTE* pbBuf,
            PSLUINT32 cbBuf, PSLUINT32* pcbWritten, PSLUINT32* pcbLeft)
{
    PSLSTATUS status;
    PSLBYTE* pb;
    PSLUINT32 cbTotal;

    MTPOBJECTIMPL* pmoi;
    MTPDBVARIANTPROP mpQuad = {0};

    PSLBYTE rgmstrFilename[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR szFilename[MAX_MTP_STRING_LEN];
    PSLUINT32 cchFilename;

    PSLBYTE rgmstrDateCreated[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR szDateCreated[MAX_MTP_STRING_LEN];
    PSLUINT32 cchDateCreated;

    PSLBYTE rgmstrDateModified[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR szDateModified[MAX_MTP_STRING_LEN];
    PSLUINT32 cchDateModified;

    PSLUINT64 qwObjCompSize;
    PSLUINT32 dwObjCompSize;

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    /* pbBuf can be PSLNULL */
    if (PSLNULL == mtpObject)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    /* Get File Name */
    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFILENAME;

    status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&rgmstrFilename[0],
                        MAX_MTP_STRING_SIZE,
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(szFilename, PSLARRAYSIZE(szFilename),
                      &cchFilename, (PCXMTPSTRING)&rgmstrFilename[0],
                      (rgmstrFilename[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (0 < rgmstrFilename[0])
    {
        cchFilename += 1;
    }

    PSLASSERT(cchFilename == rgmstrFilename[0]);

    /* Get Date Created */
    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_DATECREATED;

    status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&rgmstrDateCreated[0],
                        MAX_MTP_STRING_SIZE,
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(szDateCreated,
                    PSLARRAYSIZE(szDateCreated), &cchDateCreated,
                    (PCXMTPSTRING)&rgmstrDateCreated[0],
                    (rgmstrDateCreated[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (0 < rgmstrDateCreated[0])
    {
        cchDateCreated += 1;
    }

    PSLASSERT(cchDateCreated == rgmstrDateCreated[0]);

    /* Get Date Modified */
    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_DATEMODIFIED;

    status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&rgmstrDateModified[0],
                        MAX_MTP_STRING_SIZE,
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(szDateModified,
                    PSLARRAYSIZE(szDateModified), &cchDateModified,
                    (PCXMTPSTRING)&rgmstrDateModified[0],
                    (rgmstrDateModified[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (0 < rgmstrDateModified[0])
    {
        cchDateModified += 1;
    }

    PSLASSERT(cchDateModified == rgmstrDateModified[0]);

    /* Calculate raw buffer size */

    /* 4 is added to account for the 4 bytes of memory
     * used to store the length of 4 strings
     * (File name, date created, date modified and key word)
     */
    cbTotal = OBJECTINFO_FIXED_MEMBER_SIZE +
                (sizeof(PSLWCHAR) *
                (cchFilename + cchDateCreated +
                 cchDateModified + 3)) + 4;

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = cbTotal;
    }

    /* pbBuf will be PSLNULL when size is required by the caller */
    if (PSLNULL == pbBuf)
    {
        /* making sure the caller wants the size */
        PSLASSERT(PSLNULL != pcbLeft);
        status = PSLSUCCESS;
        goto Exit;
    }

    if (cbBuf < cbTotal)
    {
        status = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    if (PSLNULL == pbBuf)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* Serialize data to raw buffer */
    pb = pbBuf;

    /* dwStorageID */
    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

    status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        pb, sizeof(PSLUINT32), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pb += sizeof(PSLUINT32);

    /* wObjectFormat */
    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFORMAT;

    status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        pb, sizeof(PSLUINT16), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pb += sizeof(PSLUINT16);

    /* wProtectionStatus */
    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PROTECTIONSTATUS;

    status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        pb, sizeof(PSLUINT16), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pb += sizeof(PSLUINT16);

    /* dwObjCompSize */
    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTSIZE;

    status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&qwObjCompSize,
                        sizeof(PSLUINT64), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    dwObjCompSize = (PSLUINT32)qwObjCompSize;
    MTPStoreUInt32((PSLUINT32*)pb, dwObjCompSize);
    pb += sizeof(PSLUINT32);

    /* wThumbFormat */
    MTPStoreUInt16((PSLUINT16*)pb, 0);
    pb += sizeof(PSLUINT16);

    /* dwThumbCompSize */
    MTPStoreUInt32((PSLUINT32*)pb, 0);
    pb += sizeof(PSLUINT32);

    /* dwThumbPixWidth */
    MTPStoreUInt32((PSLUINT32*)pb, 0);
    pb += sizeof(PSLUINT32);

    /* dwThumbPixHeight */
    MTPStoreUInt32((PSLUINT32*)pb, 0);
    pb += sizeof(PSLUINT32);

    /* dwImgPixWidth */
    MTPStoreUInt32((PSLUINT32*)pb, 0);
    pb += sizeof(PSLUINT32);

    /* dwImgPixHeight */
    MTPStoreUInt32((PSLUINT32*)pb, 0);
    pb += sizeof(PSLUINT32);

    /* dwImgBitDepth */
    MTPStoreUInt32((PSLUINT32*)pb, 0);
    pb += sizeof(PSLUINT32);

    /* dwParentObject */
    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PARENT;

    status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        pb, sizeof(PSLUINT32), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pb += sizeof(PSLUINT32);

    /* wAssocType */
    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_ASSOCIATIONTYPE;

    status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        pb, sizeof(PSLUINT16), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pb += sizeof(PSLUINT16);

    /* dwAssocDesc */
    MTPStoreUInt32((PSLUINT32*)pb, 0);
    pb += sizeof(PSLUINT32);

    /* dwSeqNumber */
    MTPStoreUInt32((PSLUINT32*)pb, 0);
    pb += sizeof(PSLUINT32);

    PSLASSERT(MAX_MTP_STRING_LEN >= cchFilename);
    *pb = (PSLBYTE)cchFilename;
    pb += sizeof(PSLBYTE);

    /* File Name (already byte swapped by MTPObjectGetProp)*/
    if (0 != cchFilename)
    {
        /* There is no need to fail if string length is 0 */
        status = PSLStringCopy((PSLWCHAR*)pb, cchFilename,
                                (PSLWCHAR*)szFilename);
        if (PSL_FAILED(status))
        {
            PSLASSERT(!L"_SerializeObjectInfo: szFilename");
            goto Exit;
        }
    }
    pb += sizeof(PSLWCHAR) * cchFilename;

    PSLASSERT(MAX_MTP_STRING_LEN >= cchDateCreated);
    *pb = (PSLBYTE)cchDateCreated;
    pb += sizeof(PSLBYTE);

    /* Date Created (already byte swapped by MTPObjectGetProp) */
    if (0 != cchDateCreated)
    {
        /* There is no need to fail if string length is 0 */
        status = PSLStringCopy((PSLWCHAR*)pb, cchDateCreated,
                            (PSLWCHAR*)szDateCreated);
        if (PSL_FAILED(status))
        {
            PSLASSERT(!L"_SerializeObjectInfo: szDateCreated");
            goto Exit;
        }
    }
    pb += sizeof(PSLWCHAR) * cchDateCreated;

    PSLASSERT(MAX_MTP_STRING_LEN >= cchDateModified);
    *pb = (PSLBYTE)cchDateModified;
    pb += sizeof(PSLBYTE);

    /* Date Modified (already byte swapped by MTPObjectGetProp)*/
    if (0 != cchDateModified)
    {
        /* There is no need to fail if string length is 0 */
        status = PSLStringCopy((PSLWCHAR*)pb, cchDateModified,
                                (PSLWCHAR*)szDateModified);
        if (PSL_FAILED(status))
        {
            PSLASSERT(!L"_SerializeObjectInfo: szDateModified");
            goto Exit;
        }
    }
    pb += sizeof(PSLWCHAR) * cchDateModified;

    /* Key Word (An empty string would consist of a single
     * 8-bit integer containing a value of 0x00)*/
    *pb = (PSLBYTE)0;
    pb += sizeof(PSLBYTE);

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = cbTotal;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

Exit:
    return status;
}

PSLSTATUS _DeserializeObjectInfo(MTPOBJECT mtpObject, PSLBYTE* pbBuf,
                                 PSLUINT32 cbBuf)
{
    PSLSTATUS status;
    PSLUINT8 cchLen;

    MTPOBJECTIMPL* pmoi;

    PSLUINT64 qwObjCompSize;

    if (PSLNULL == mtpObject || PSLNULL == pbBuf || 0 == cbBuf)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    /* Unpack data from raw buffer */

    /* dwStorageID      */
    /* StorageID from operation request packet will be used
     * instead of the value contained in the ObjectInfo dataset
     * so skipping that
     */
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);


    /* wObjectFormat     */
    status = _DeserializeObjectPropValue(mtpObject,
                          MTP_OBJECTPROPCODE_OBJECTFORMAT,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          pbBuf, cbBuf, PSLTRUE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pbBuf += sizeof(PSLUINT16);
    cbBuf -= sizeof(PSLUINT16);

    /* wProtectionStatus */
    status = _DeserializeObjectPropValue(mtpObject,
                          MTP_OBJECTPROPCODE_PROTECTIONSTATUS,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          pbBuf, cbBuf, PSLTRUE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pbBuf += sizeof(PSLUINT16);
    cbBuf -= sizeof(PSLUINT16);

    /* dwObjCompSize     */
    qwObjCompSize = MTPLoadUInt32((PXPSLUINT32)pbBuf);
    status = _DeserializeObjectPropValue(mtpObject,
                          MTP_OBJECTPROPCODE_OBJECTSIZE,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)&qwObjCompSize, sizeof(PSLUINT64),
                          PSLTRUE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);

    /* wThumbFormat      */
    pbBuf += sizeof(PSLUINT16);
    cbBuf -= sizeof(PSLUINT16);

    /* dwThumbCompSize   */
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);

    /* dwThumbPixWidth   */
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);

    /* dwThumbPixHeight  */
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);

    /* dwImgPixWidth     */
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);

    /* dwImgPixHeight    */
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);

    /* dwImgBitDepth     */
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);

    /* dwParentObject    */
    /* ParentObject from operation request packet will be used
     * instead of the value contained in the ObjectInfo dataset
     * so skipping that
     */
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);

    /* wAssocType        */
    status = _DeserializeObjectPropValue(mtpObject,
                          MTP_OBJECTPROPCODE_ASSOCIATIONTYPE,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          pbBuf, cbBuf, PSLTRUE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pbBuf += sizeof(PSLUINT16);
    cbBuf -= sizeof(PSLUINT16);

    /* dwAssocDesc       */
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);

    /* dwSeqNumber        */
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);

    /* File Name */
    cchLen = *pbBuf;
    PSLASSERT((sizeof(PSLWCHAR) * cchLen) < cbBuf);

    status = _DeserializeObjectPropValue(mtpObject,
                          MTP_OBJECTPROPCODE_OBJECTFILENAME,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          pbBuf, cbBuf, PSLTRUE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pbBuf += (sizeof(PSLWCHAR) * cchLen) + 1;
    cbBuf -= (sizeof(PSLWCHAR) * cchLen) + 1;

    /* Date Created */
    cchLen = *pbBuf;
    PSLASSERT((sizeof(PSLWCHAR) * cchLen) < cbBuf);

    status = _DeserializeObjectPropValue(mtpObject,
                          MTP_OBJECTPROPCODE_DATECREATED,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          pbBuf, cbBuf, PSLTRUE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pbBuf += (sizeof(PSLWCHAR) * cchLen) + 1;
    cbBuf -= (sizeof(PSLWCHAR) * cchLen) + 1;

    /* Date Modified */
    cchLen = *pbBuf;
    PSLASSERT((sizeof(PSLWCHAR) * cchLen) < cbBuf);

    status = _DeserializeObjectPropValue(mtpObject,
                          MTP_OBJECTPROPCODE_DATEMODIFIED,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          pbBuf, cbBuf, PSLTRUE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pbBuf += (sizeof(PSLWCHAR) * cchLen) + 1;
    cbBuf -= (sizeof(PSLWCHAR) * cchLen) + 1;

    /* Key words (Ignored)*/
    cchLen = *pbBuf;
    PSLASSERT((sizeof(PSLWCHAR) * cchLen) < cbBuf);

    pbBuf += (sizeof(PSLWCHAR) * cchLen) + 1;
    cbBuf -= (sizeof(PSLWCHAR) * cchLen) + 1;

    PSLASSERT(0 == cbBuf);

    status = _MTPObjectSetFilePath(mtpObject);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}

PSLSTATUS _SerializeObjectPropList(MTPOBJECT mtpObject,
                                   PSL_CONST MTPDBVARIANTPROP* pmqList,
                                   PSLUINT32 cmqList,
                                   PSLBYTE* pbBuf,
                                   PSLUINT32 cbBuf,
                                   PSLUINT32* pcbWritten,
                                   PSLUINT32* pcbLeft)
{
    PSLSTATUS status;
    MTPOBJECTIMPL* pmoi;
    PSLUINT32 dwGroupCode = MTP_GROUPCODE_NONE;
    PSLUINT32 cdwItems;
    MTPDBVARIANTPROP mtpQuad;
    PSLBOOL fIgnoreSlowOnes = PSLFALSE;
    PSLUINT16 wObjectFormat;
    PSLDYNLIST dlPropCodes = PSLNULL;
    PSLUINT32 dwIndex;
    PSLUINT16 wPropCode;
    PSLUINT32 cbSize = 0;
    PSLUINT32 cbTotalSize = 0;
    WORD      wPropDataType;
    DWORD     cbPropDataSize;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /* pbBuf, pcbWritten & pcbLeft can be PSLNULL */
    if (PSLNULL == mtpObject || PSLNULL == pmqList)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    PSLASSERT(2 == cmqList);

    if (MTP_OBJECTPROPCODE_NOTUSED != pmqList[0].vParam.valUInt32 &&
        MTP_OBJECTPROPCODE_ALL != pmqList[0].vParam.valUInt32)
    {
        cdwItems = 1;
    }
    else
    {
        if((PSLUINT16)MTP_OBJECTPROPCODE_ALL == pmqList[0].vParam.valUInt32)
        {
            fIgnoreSlowOnes = PSLTRUE;
            dwGroupCode = MTP_GROUPCODE_NONE;
        }
        else if (MTP_OBJECTPROPCODE_NOTUSED == pmqList[0].vParam.valUInt32)
        {
            fIgnoreSlowOnes = PSLFALSE;
            dwGroupCode = pmqList[1].vParam.valUInt32;
        }

        /* wObjectFormat */
        mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mtpQuad.wDataType = MTP_DATATYPE_UINT32;
        mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFORMAT;

        status = MTPObjectGetProp(mtpObject, &mtpQuad, 1,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_NONE),
                            (PSLBYTE*)&wObjectFormat,
                            sizeof(PSLUINT16), PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        status = PSLDynListCreate(0, PSLFALSE, PSLDYNLIST_GROW_DEFAULT,
                                  &dlPropCodes);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        status = MTPDBGetObjectPropCodes(wObjectFormat, dwGroupCode,
                                       fIgnoreSlowOnes, dlPropCodes);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        status = PSLDynListGetSize(dlPropCodes, &cdwItems);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    for (dwIndex = 0; dwIndex < cdwItems; dwIndex++)
    {
        if (PSLNULL == dlPropCodes)
        {
            PSLASSERT(0 == dwIndex);
            wPropCode = (PSLUINT16)pmqList[0].vParam.valUInt32;
        }
        else
        {
            status = PSLDynListGetItem(dlPropCodes, dwIndex,
                                       (PSLPARAM*)&wPropCode);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
        }

        /* Get the object prop desc from prop api passing prop code*/
        status = GetMtpPropTypeAndSize(wPropCode, &wPropDataType, &cbPropDataSize);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        /* Object Handle */
        status = MTPObjectGetProp(mtpObject, PSLNULL, 0,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_ITEMID),
                            pbBuf, cbBuf, PSLNULL, &cbSize);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        cbTotalSize += cbSize;
        if (PSLNULL != pbBuf)
        {
            pbBuf += cbSize;
        }

        /* Property Code */
        if (PSLNULL != pbBuf)
        {
            MTPStoreUInt16((PSLUINT16*)pbBuf, wPropCode);
        }
        cbTotalSize += sizeof(PSLUINT16);
        if (PSLNULL != pbBuf)
        {
            pbBuf += sizeof(PSLUINT16);
        }

        /* Data Type */
        if (PSLNULL != pbBuf)
        {
            MTPStoreUInt16((PSLUINT16*)pbBuf, wPropDataType);
        }
        cbTotalSize += sizeof(PSLUINT16);
        if (PSLNULL != pbBuf)
        {
            pbBuf += sizeof(PSLUINT16);
        }

        /* Property Value */
        mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mtpQuad.wDataType = MTP_DATATYPE_UINT32;
        mtpQuad.vParam.valInt32 = wPropCode;

        status = MTPObjectGetProp(mtpObject, &mtpQuad, 1,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_NONE),
                            pbBuf, cbBuf, PSLNULL, &cbSize);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        cbTotalSize += cbSize;
        if (PSLNULL != pbBuf)
        {
            pbBuf += cbSize;
        }
    }

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = cdwItems;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = cbTotalSize;
    }

    status = PSLSUCCESS;

Exit:
    SAFE_PSLDYNLISTDESTROY(dlPropCodes);
    return status;
cmqList;
}

PSLSTATUS _DeserializeObjectPropList(MTPOBJECT mtpObject,
                                     PSLBYTE* pbBuf,
                                     PSLUINT32 cbBuf)
{
    PSLSTATUS status;
    PSLUINT32 cdwElements;
    PSLUINT32 dwIndex;
    MTPOBJECTIMPL* pmoi;
    PSLUINT32 cbValueSize;
    PSLUINT32 dwObjectHandle;
    PSLUINT16 wPropCode;
    PSLUINT16 wDataType;
    PSLUINT16 wObjectFormat;
    PSLUINT16 wAssocType;

    if (PSLNULL == pbBuf || 0 == cbBuf)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    cdwElements = MTPLoadUInt32((PXPSLUINT32)pbBuf);
    pbBuf += sizeof(PSLUINT32);
    cbBuf -= sizeof(PSLUINT32);

    for(dwIndex = 0; dwIndex < cdwElements; dwIndex++)
    {
        dwObjectHandle = MTPLoadUInt32((PXPSLUINT32)pbBuf);
        pbBuf += sizeof(PSLUINT32);
        cbBuf -= sizeof(PSLUINT32);

        wPropCode = MTPLoadUInt16((PXPSLUINT16)pbBuf);
        pbBuf += sizeof(PSLUINT16);
        cbBuf -= sizeof(PSLUINT16);

        wDataType = MTPLoadUInt16((PXPSLUINT16)pbBuf);
        pbBuf += sizeof(PSLUINT16);
        cbBuf -= sizeof(PSLUINT16);

        status = MTPPropertyReadSize(wDataType, pbBuf, cbBuf,
                                         &cbValueSize);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        status = _DeserializeObjectPropValue(mtpObject, wPropCode,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          pbBuf, cbValueSize, PSLTRUE);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        pbBuf += cbValueSize;
        cbBuf -= cbValueSize;
    }

    PSLASSERT(0 == cbBuf);

    status = _MTPObjectSetFilePath(mtpObject);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = _SerializeObjectPropValue(mtpObject,
                              MTP_OBJECTPROPCODE_OBJECTFORMAT ,
                              (PSLBYTE*)&wObjectFormat,
                              sizeof(PSLUINT16), PSLNULL,
                              PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* association type */
    wAssocType = (MTP_FORMATCODE_ASSOCIATION == wObjectFormat) ?
                    (PSLUINT16)MTP_ASSOCIATIONTYPE_GENERICFOLDER :
                    (PSLUINT16)MTP_ASSOCIATIONTYPE_UNDEFINED;

    status = _DeserializeObjectPropValue(mtpObject,
                          MTP_OBJECTPROPCODE_ASSOCIATIONTYPE,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_NONE),
                          (PSLBYTE*)&wAssocType, sizeof(PSLUINT16),
                          PSLFALSE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS _SerializeObjectPropValue(MTPOBJECT mtpObject,
                                    PSLUINT16 wPropCode,
                                    PSLBYTE* pbBuf,
                                    PSLUINT32 cbBuf,
                                    PSLUINT32* pcbWritten,
                                    PSLUINT32* pcbLeft)
{
    PSLSTATUS status;
    PSLUINT32 dwIndex;
    MTPOBJECTIMPL* pmoi;
    PSLUINT32 cbCurVal;
    WORD      wPropDataType;
    DWORD     cbPropDataSize;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /* pcbWritten, pcbLeft & pbBuf can be PSLNULL */
    if (PSLNULL == mtpObject)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    PSLASSERT(PSLNULL != pmoi->pPropField);

    /* If the request is for object references
     * get it from object references list
     */
    if (MTP_OBJECTPROPCODE_OBJECTREFERENCES == wPropCode)
    {
        status = _SerializeObjectReferences(mtpObject, pbBuf, cbBuf,
                                            pcbWritten, pcbLeft);
        goto Exit;
    }

    status = _GetPropIndexFromPropCode(wPropCode, &dwIndex);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (PSLSUCCESS_NOT_FOUND == status)
    {
        PSLASSERT(!L"_RgdwPropCodes missing property codes");
        status = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    /* Get the object property info passing prop code*/
    status = GetMtpPropTypeAndSize(wPropCode, &wPropDataType, &cbPropDataSize);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    cbCurVal = pmoi->pPropField[dwIndex].cbCurVal;

    if (PSLNULL == pmoi->pPropField[dwIndex].pbCurVal ||
        0 == pmoi->pPropField[dwIndex].cbCurVal)
    {
        if (wPropDataType == MTP_DATATYPE_STRING)
        {
            /* An empty string would consist of a single
             * 8-bit integer containing a value of 0x00.
             */
            cbCurVal = sizeof(PSLBYTE);
        }
        else if((wPropDataType & MTP_DATATYPE_ARRAYMASK) ==
                                    MTP_DATATYPE_ARRAY)
        {
            /* An empty array would consist of a single 32-bit
             * integer containing a value of 0x00000000.
             */
            cbCurVal = sizeof(PSLUINT32);
        }
        else if ((wPropDataType & MTP_DATATYPE_VALUEMASK) ==
                                        MTP_DATATYPE_VALUE)
        {
            cbCurVal = cbPropDataSize;
        }
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = cbCurVal;
    }

    /* query for size */
    if (PSLNULL == pbBuf)
    {
        PSLASSERT(PSLNULL != pcbLeft);
        goto Exit;
    }

    /* function excepts buffer that can be filled
     * in one shot
     */
    if (cbBuf < cbCurVal)
    {
        status = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /* assumption is data will be stored in the required
     * format by deserialize function
     */

    if (PSLNULL == pmoi->pPropField[dwIndex].pbCurVal ||
        0 == pmoi->pPropField[dwIndex].cbCurVal)
    {
        if (wPropDataType == MTP_DATATYPE_STRING)
        {
            /* An empty string would consist of a single
             * 8-bit integer containing a value of 0x00.
             */
            pbBuf[0] = 0;
        }
        else if((wPropDataType & MTP_DATATYPE_ARRAYMASK) ==
                                    MTP_DATATYPE_ARRAY)
        {
            /* An empty array would consist of a single 32-bit
             * integer containing a value of 0x00000000.
             */
            PSLStoreAlignUInt32((PSLUINT32*)pbBuf, 0);
        }
        else if ((wPropDataType & MTP_DATATYPE_VALUEMASK) ==
                                        MTP_DATATYPE_VALUE)
        {
            /* A zero value */
            PSLZeroMemory(pbBuf, cbCurVal);
        }
    }
    else
    {
        PSLCopyMemory(pbBuf, pmoi->pPropField[dwIndex].pbCurVal,
                        cbCurVal);
    }

    /* return amount of data written to the buffer */
    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = cbCurVal;
    }

Exit:
    return status;
}

PSLSTATUS _DeserializeObjectPropValue(MTPOBJECT mtpObject,
                                      PSLUINT16 wPropCode,
                                      PSLUINT32 dwDataFormat,
                                      PSLBYTE* pbBuf,
                                      PSLUINT32 cbBuf,
                                      PSLBOOL fFromWire)
{
    PSLSTATUS status;
    PSLUINT32 dwIndex;
    MTPOBJECTIMPL* pmoi;
    PSLUINT32 cchLen;
    PSLUINT32 cItems;
    PSLBYTE* pbCurVal = PSLNULL;
    PSLUINT32 cbCurVal = 0;
    WORD      wPropDataType;
    DWORD     cbPropDataSize;

    if (PSLNULL == mtpObject || PSLNULL == pbBuf)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    PSLASSERT(PSLNULL != pmoi->pPropField);

    /* If the request is for object references
     * set it to object references list
     */
    if (MTP_OBJECTPROPCODE_OBJECTREFERENCES == wPropCode)
    {
        status = _DeserializeObjectReferences(mtpObject, pbBuf,
                                              cbBuf, fFromWire);
        goto Exit;
    }

    status = _GetPropIndexFromPropCode(wPropCode, &dwIndex);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (PSLSUCCESS_NOT_FOUND == status)
    {
        PSLASSERT(!L"_RgdwPropCodes missing property codes");
        status = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    /* Get the object property info passing prop code*/
    status = GetMtpPropTypeAndSize(wPropCode, &wPropDataType, &cbPropDataSize);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* If asked for force update, read only properties
     * are allowed to be set, if not return error
     */
    if (MTPDSDATAFLAG(dwDataFormat) != MTPDSDATAFLAG_FORCEUPDATE)
    {
        PSLUINT8 bGetSet;

        status = MTPDBObjectPropertyGetAccess(wPropCode, &bGetSet);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        if (MTP_PROPGETSET_GETONLY == bGetSet)
        {
            status = PSLERROR_ACCESS_DENIED;
            goto Exit;
        }
    }

    //if (PSLSUCCESS != MTPObjectPropIsValidValue(wPropCode,
    //                            pbBuf, cbBuf, fFromWire))
    //{
    //    status = MTPERROR_DATABASE_INVALIDOBJECTPROPVALUE;
    //    goto Exit;
    //}

    if (PSLTRUE == fFromWire)
    {
        status = MTPPropertyReadSize(wPropDataType, pbBuf, cbBuf, &cbCurVal);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        pbCurVal = (PSLBYTE*)PSLMemAlloc(PSLMEM_FIXED, cbCurVal);
        if (PSLNULL == pbCurVal)
        {
            status = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }

        /* data store stores data in little endian
         * hence copy memory is good as data is from wire
         */
        PSLCopyMemory(pbCurVal, pbBuf, cbCurVal);
    }
    else
    {
        if (wPropDataType == MTP_DATATYPE_STRING)
        {
            /* get the string length */
            status = MTPUTF16StringLen((PSLWCHAR*)pbBuf,
                            MAX_MTP_STRING_LEN, &cchLen);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }

            /* calculate mtp string size
             * (((str len + 1) * sizeof(PSLWCHAR)) + sizeof(PSLBYTE))
             */
            cbCurVal = ((cchLen + 1) * sizeof(PSLWCHAR)) + 1;

            /* allocate string buffer */
            pbCurVal = (PSLBYTE*)PSLMemAlloc(PSLMEM_FIXED, cbCurVal);
            if (PSLNULL == pbCurVal)
            {
                status = PSLERROR_OUT_OF_MEMORY;
                goto Exit;
            }

            /* call MTPStoreMTPString to store the string */

            /* casting pbBuf to (PSLWCHAR*) is safe as the buffer
             * is allocated interally as a PSLWCHAR* and hence
             * aligned correctly
             */
            status = MTPStoreMTPString((PXMTPSTRING)pbCurVal, cbCurVal,
                                    PSLNULL, (PSLWCHAR*)pbBuf, cchLen);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }

            /* check if the string is valid */
        }
        else if((wPropDataType & MTP_DATATYPE_ARRAYMASK) ==
                                    MTP_DATATYPE_ARRAY)
        {
            /* get the number of items */

            /* casting pbBuf to (PSLUINT32*) is safe as the buffer
             * is allocated interally as a PSLUINT32* and hence
             * aligned correctly
             */
            cItems = *((PSLUINT32*)pbBuf);

            /* calculate the array size
             * ((num items * mopi.dwDTSSize) + sizeof(PSLUINT32))
             */
            cbCurVal = (cItems * cbPropDataSize) + sizeof(PSLUINT32);

            /* allocate string buffer */
            pbCurVal = (PSLBYTE*)PSLMemAlloc(PSLMEM_FIXED, cbCurVal);
            if (PSLNULL == pbCurVal)
            {
                status = PSLERROR_OUT_OF_MEMORY;
                goto Exit;
            }

            /* store num items in the first PSLUINT32 */
            *((PSLUINT32*)pbCurVal) = cItems;
            pbCurVal += sizeof(PSLUINT32);

            /* call MTPStoreUIntArray buffer offset by PSLUINT32 */
            MTPStoreUIntArray(pbCurVal, pbBuf + sizeof(PSLUINT32),
                                cItems, cbPropDataSize);
        }
        else if((wPropDataType & MTP_DATATYPE_VALUEMASK) ==
                                    MTP_DATATYPE_VALUE)
        {
            cbCurVal = cbPropDataSize;

            /* allocate value buffer, size = mopi.dwDTSSize */
            pbCurVal = (PSLBYTE*)PSLMemAlloc(PSLMEM_FIXED, cbCurVal);
            if (PSLNULL == pbCurVal)
            {
                status = PSLERROR_OUT_OF_MEMORY;
                goto Exit;
            }

            /* check if the input value is valid */

            /* call MTPStoreUIntArray */
            MTPStoreUIntArray(pbCurVal, pbBuf, 1, cbPropDataSize);
        }
    }

    if (PSLNULL != pbCurVal)
    {
        SAFE_PSLMEMFREE(pmoi->pPropField[dwIndex].pbCurVal);

        pmoi->pPropField[dwIndex].cbCurVal = cbCurVal;
        pmoi->pPropField[dwIndex].pbCurVal = pbCurVal;
    }

    pbCurVal = PSLNULL;

Exit:
    SAFE_PSLMEMFREE(pbCurVal);
    return status;
}

PSLSTATUS _GetPropIndexFromPropCode(PSLUINT16 wPropCode,
                                    PSLUINT32* pdwIndex)
{
    PSLSTATUS status;
    PSLUINT32 dwIndex;

    if (PSLNULL != pdwIndex)
    {
        *pdwIndex = 0;
    }

    if (PSLNULL == pdwIndex)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLSUCCESS_NOT_FOUND;

    for (dwIndex = 0; dwIndex < PSLARRAYSIZE(_RgdwPropCodes); dwIndex++)
    {
        if(_RgdwPropCodes[dwIndex] == wPropCode)
        {
            status = PSLSUCCESS;
            break;
        }
    }

    *pdwIndex = dwIndex;
Exit:
    return status;
}

PSLSTATUS _ReleaseObjectPropValues(MTPOBJECT mtpObject)
{
    PSLSTATUS status;
    MTPOBJECTIMPL* pmoi;
    PSLUINT32 dwIndex;

    if (PSLNULL == mtpObject)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    PSLASSERT(PSLNULL != pmoi->pPropField);

    for (dwIndex = 0; dwIndex < PSLARRAYSIZE(_RgdwPropCodes); dwIndex++)
    {
        SAFE_PSLMEMFREE(pmoi->pPropField[dwIndex].pbCurVal);
        pmoi->pPropField[dwIndex].cbCurVal = 0;
    }
    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS _MTPObjectDeleteChildren(MTPOBJECT mtpObject)
{
    PSLSTATUS status;
    MTPOBJECTIMPL* pmoi;
    PSLUINT32 dwObjectHandle;
    MTPDBVARIANTPROP mtpQuad = {0};
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 dwIndex;
    PSLBOOL fPartialDelete = PSLFALSE;

    if (PSLNULL == mtpObject)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    /* Get this object's handle */
    status = MTPObjectGetProp(mtpObject, PSLNULL, 0,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_ITEMID),
                            (PSLBYTE*)&dwObjectHandle,
                            sizeof(PSLUINT32), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Query for its children */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PARENT;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valUInt32 = dwObjectHandle;

    status = MTPDataStoreQuery(pmoi->mds, &mtpQuad,
                                1, MTPDSITEMTYPE_OBJECT,
                                &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Delete the children */
    for (dwIndex = 0; dwIndex < cItems; dwIndex++)
    {
        status = MTPDataStoreDelete(pItems[dwIndex]);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        fPartialDelete = PSLTRUE;
    }

    fPartialDelete = PSLFALSE;
Exit:
    status = (PSLFALSE == fPartialDelete) ? status :
                MTPERROR_DATABASE_PARTIAL_DELETION;
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS _MTPObjectIsAssociation(MTPOBJECT mtpObject,
                                  PSLBOOL* pfAssoc)
{
    PSLSTATUS status;
    MTPDBVARIANTPROP mtpQuad = {0};
    PSLUINT16 wAssocType;

    if (PSLNULL != pfAssoc)
    {
        *pfAssoc = PSLFALSE;
    }

    if (PSLNULL == mtpObject || PSLNULL == pfAssoc)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* Get association type */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_ASSOCIATIONTYPE;

    status = MTPObjectGetProp(mtpObject, &mtpQuad, 1,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_NONE),
                            (PSLBYTE*)&wAssocType,
                            sizeof(PSLUINT16), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *pfAssoc = (MTP_ASSOCIATIONTYPE_GENERICFOLDER == wAssocType) ?
                PSLTRUE : PSLFALSE;
Exit:
    return status;
}

PSLSTATUS _MTPObjectSetFilePath(MTPOBJECT mtpObject)
{
    PSLSTATUS status;
    PSLUINT32 dwLen;

    MTPOBJECTIMPL* pmoi;
    MTPDBVARIANTPROP mpQuad = {0};

    PSLWCHAR szFilePath[MAX_PATH] = {0};
    WCHAR szFilePathFmt[] = L"%s\\%s";
    PSLWCHAR szFileName[MAX_PATH] = {0};
    WCHAR szFileFmt[] = L"%x";

    PSLBYTE rgmstrFileName[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR rgFileName[MAX_MTP_STRING_LEN];

    PSLUINT32 dwParentHandle;
    PSLUINT32 dwObjectHandle;
    MTPOBJECT mtpParentObject;
    PSLUINT32 dwStoreId;
    MTPSTORE mtpStore;

    PSLBYTE rgmstrParentPath[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR rgParentPath[MAX_MTP_STRING_LEN];

    HRESULT hr;

    if (PSLNULL == mtpObject)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    /* File Path*/
    /* <TODO>
     * In this case MTPObjectGetProp should have been
     * called with a flag (fToWire == PSLFLASE) that
     * would tell it not to swap byte order in which
     * case remember to pass PSLWCHAR* instead of DSSTRING*
     */
    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFILENAME;

    status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_NONE),
                            (PSLBYTE*)&rgmstrFileName[0],
                            MAX_MTP_STRING_SIZE,
                            PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(rgFileName, PSLARRAYSIZE(rgFileName),
                      PSLNULL, (PCXMTPSTRING)&rgmstrFileName[0],
                      (rgmstrFileName[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* ObjectHandle*/
    status = MTPDataStoreGetProp(mtpObject, PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_ITEMID),
                        (PSLBYTE*)&dwObjectHandle,
                        sizeof(PSLUINT32),
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    hr = StringCchPrintfW((WCHAR*)&szFileName[0], ARRAYSIZE(szFileName),
                          szFileFmt, dwObjectHandle);
    if (FAILED(hr))
    {
        status = hr;
        goto Exit;
    }

    status = PSLStringCat(&szFileName[0], ARRAYSIZE(szFileName),
                  (PSLWCHAR*)FindFileExtension((WCHAR*)rgFileName));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PARENT;

    status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_NONE),
                            (PSLBYTE*)&dwParentHandle,
                            sizeof(PSLUINT32), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(MTP_OBJECTHANDLE_UNDEFINED != dwParentHandle);
    if (MTP_OBJECTHANDLE_UNDEFINED == dwParentHandle)
    {
        status = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    if (MTP_OBJECTHANDLE_ROOT == dwParentHandle)
    {
        /* If parent root get the store and ask
         * for it path instead of parent path
         */
        mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mpQuad.wDataType = MTP_DATATYPE_UINT32;
        mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

        status = MTPObjectGetProp(mtpObject, &mpQuad, 1,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_NONE),
                            (PSLBYTE*)&dwStoreId, sizeof(PSLUINT32),
                            PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            PSLASSERT(PSLFALSE);
            goto Exit;
        }

        status = MTPDataStoreGetMTPStore(pmoi->mds, dwStoreId,
                                          &mtpStore);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        status = MTPStoreGetProp(mtpStore, PSLNULL, 0,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_FILEPATH),
                            (PSLBYTE*)&rgmstrParentPath[0],
                            MAX_MTP_STRING_SIZE,
                            PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            PSLASSERT(PSLFALSE);
            goto Exit;
        }

        status = MTPLoadMTPString(rgParentPath, PSLARRAYSIZE(rgParentPath),
                          PSLNULL, (PCXMTPSTRING)&rgmstrParentPath[0],
                          (rgmstrParentPath[0] * sizeof(PSLWCHAR))+ 1);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }
    else
    {
        status = MTPDataStoreGetMTPObject(pmoi->mds, dwParentHandle,
                                          &mtpParentObject);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        /* <TODO>
         * In this case MTPObjectGetProp should have been
         * called with a flag (fToWire == PSLFLASE) that
         * would tell it not to swap byte order.
         */
        status = MTPObjectGetProp(mtpParentObject, PSLNULL, 0,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_FILEPATH),
                            (PSLBYTE*)&rgmstrParentPath[0],
                            MAX_MTP_STRING_SIZE,
                            PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        status = MTPLoadMTPString(rgParentPath, PSLARRAYSIZE(rgParentPath),
                          PSLNULL, (PCXMTPSTRING)&rgmstrParentPath[0],
                          (rgmstrParentPath[0] * sizeof(PSLWCHAR))+ 1);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    hr = StringCchPrintfW((WCHAR*)szFilePath, MAX_PATH, szFilePathFmt,
                          (WCHAR*)rgParentPath, (WCHAR*)szFileName);
    if(FAILED(hr))
    {
        status = hr;
        goto Exit;
    }

    status = PSLStringLen((PSLWCHAR*)szFilePath, MAX_PATH, &dwLen);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPObjectSetProp(mtpObject, PSLNULL, 0,
                          MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                         MTPDSDATAFORMAT_FILEPATH),
                          (PSLBYTE*)szFilePath,
                          (dwLen + 1) * sizeof(PSLWCHAR), PSLFALSE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
Exit:
    return status;
}

/*
 *  This function will get object references.
 */
PSLSTATUS _SerializeObjectReferences(MTPOBJECT mtpObject,
                                        PSLBYTE* pbBuf,
                                        PSLUINT32 cbBuf,
                                        PSLUINT32* pcbWritten,
                                        PSLUINT32* pcbLeft)
{
    PSLSTATUS status;
    PSLUINT32 dwIndex;
    MTPOBJECTIMPL* pmoi;
    PSLUINT32 cdwReferences = 0;
    PSLUINT32* pdwReferences = PSLNULL;
    PSLUINT32 dwIndexRef = 0;
    MTPOBJECT mtpObjectRef;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /* pcbWritten, pcbLeft & pbBuf can be PSLNULL hence not checked */
    if (PSLNULL == mtpObject)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    if (PSLNULL != pmoi->pdwReferences)
    {
        for(dwIndex = 0; dwIndex < pmoi->cdwReferences; dwIndex++)
        {
            status = MTPDataStoreGetMTPObject(pmoi->mds,
                                      pmoi->pdwReferences[dwIndex],
                                      &mtpObjectRef);
            if (MTPERROR_DATABASE_INVALID_HANDLE == status)
            {
                status = PSLSUCCESS;
                continue;
            }

            if (PSL_FAILED(status))
            {
                goto Exit;
            }
            cdwReferences++;
        }

        pdwReferences = (PSLUINT32*)PSLMemAlloc(PSLMEM_PTR,
                                    sizeof(PSLUINT32) * cdwReferences);
        if (PSLNULL == pdwReferences)
        {
            status = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }

        for(dwIndex = 0; dwIndex < pmoi->cdwReferences; dwIndex++)
        {
            status = MTPDataStoreGetMTPObject(pmoi->mds,
                                      pmoi->pdwReferences[dwIndex],
                                      &mtpObjectRef);
            if (MTPERROR_DATABASE_INVALID_HANDLE == status)
            {
                status = PSLSUCCESS;
                continue;
            }

            if (PSL_FAILED(status))
            {
                goto Exit;
            }

            pdwReferences[dwIndexRef] = pmoi->pdwReferences[dwIndex];
            dwIndexRef++;
        }

        PSLASSERT(cdwReferences == dwIndexRef);

        SAFE_PSLMEMFREE(pmoi->pdwReferences);
        pmoi->pdwReferences = pdwReferences;
        pdwReferences = PSLNULL;

        pmoi->cdwReferences = cdwReferences;
    }

    if (PSLNULL != pcbLeft)
    {
        /* sizeof(PSLUINT32) for number of elements */
        *pcbLeft = (sizeof(PSLUINT32) * pmoi->cdwReferences) +
                    sizeof(PSLUINT32);
    }
    if (PSLNULL == pbBuf)
    {
        /* making sure the caller wants the size */
        PSLASSERT(PSLNULL != pcbLeft);
        status = PSLSUCCESS;
        goto Exit;
    }

    PSLASSERT(cbBuf >= (sizeof(PSLUINT32) * pmoi->cdwReferences) +
                        sizeof(PSLUINT32));
    if (cbBuf < (sizeof(PSLUINT32) * pmoi->cdwReferences) +
                 sizeof(PSLUINT32))
    {
        status = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /* Count of elements */
    MTPStoreUInt32((PSLUINT32*)pbBuf, pmoi->cdwReferences);
    pbBuf += sizeof(PSLUINT32);

    if (PSLNULL == pmoi->pdwReferences)
    {
        /* return zero for count of elements */
        status = PSLSUCCESS;
        goto Exit;
    }

    for(dwIndex = 0; dwIndex < pmoi->cdwReferences; dwIndex++)
    {
        MTPStoreUInt32((PSLUINT32*)pbBuf,
                        pmoi->pdwReferences[dwIndex]);
        pbBuf += sizeof(PSLUINT32);
    }
    status = PSLSUCCESS;
Exit:
    SAFE_PSLMEMFREE(pdwReferences);
    return status;
}

/*
 *  This function will set object references.
 */
PSLSTATUS _DeserializeObjectReferences(MTPOBJECT mtpObject,
                                        PSLBYTE* pbBuf,
                                        PSLUINT32 cbBuf,
                                        PSLBOOL fFromWire)
{
    PSLSTATUS status;
    PSLUINT32 dwIndex;
    MTPOBJECTIMPL* pmoi;

    PSLUINT32* pdwReferences = PSLNULL;
    PSLUINT32 cdwReferences;

    if (PSLNULL == mtpObject || PSLNULL == pbBuf)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmoi = (MTPOBJECTIMPL*)mtpObject;

    PSLASSERT(cbBuf >= sizeof(PSLUINT32));
    if (cbBuf < sizeof(PSLUINT32))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* Count of elements */
    cdwReferences = MTPLoadUInt32((PXPSLUINT32)pbBuf);
    pbBuf += sizeof(PSLUINT32);

    pdwReferences = (PSLUINT32*)PSLMemAlloc(PSLMEM_PTR,
                                            sizeof(PSLUINT32) * \
                                            cdwReferences);
    if (PSLNULL == pdwReferences)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    for(dwIndex = 0; dwIndex < cdwReferences; dwIndex++)
    {
        if (PSLTRUE == fFromWire)
        {
            pdwReferences[dwIndex] = MTPLoadUInt32((PXPSLUINT32)pbBuf);
        }
        else
        {
            pdwReferences[dwIndex] = *((PSLUINT32*)pbBuf);
        }
        pbBuf += sizeof(PSLUINT32);
    }

    SAFE_PSLMEMFREE(pmoi->pdwReferences);
    pmoi->pdwReferences = pdwReferences;
    pdwReferences = PSLNULL;
    pmoi->cdwReferences = cdwReferences;
    status = PSLSUCCESS;
Exit:
    SAFE_PSLMEMFREE(pdwReferences);
    return status;
}

PSLSTATUS GetMtpPropTypeAndSize(WORD wPropCode, WORD* pwDataType, DWORD* pdwSize)
{
    PSLSTATUS status;
    PSL_CONST MTPPROPERTYREC* precProp;

    if (pdwSize != PSLNULL)
    {
        *pdwSize = 0;
    }

    if (pwDataType != PSLNULL)
    {
        *pwDataType = MTP_DATATYPE_UNDEFINED;
    }

    if (wPropCode == MTP_OBJECTPROPCODE_NOTUSED || pwDataType == PSLNULL)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPPropertyGetDefaultRec(wPropCode, PROPFLAGS_TYPE_OBJECT,
                            &precProp);
    if (PSLSUCCESS != status)
    {
        status = PSL_FAILED(status) ? status : MTPERROR_PROPERTY_INVALID_OBJECTPROPCODE;
        goto Exit;
    }

    if (PSLNULL != pdwSize)
    {   if (!(MTP_DATATYPE_ARRAY & precProp->wDataType) && (MTP_DATATYPE_STRING != precProp->wDataType))
        {
            status = MTPPropertyGetMinSizeFromType(precProp->wDataType, pdwSize);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
        }
        else
        {
            *pdwSize = 0;
        }
    }
    *pwDataType = precProp->wDataType;

    status = PSLSUCCESS;

Exit:
    return status;
}
