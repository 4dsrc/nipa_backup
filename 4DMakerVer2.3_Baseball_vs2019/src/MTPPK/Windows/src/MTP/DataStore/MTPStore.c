/*
 *  MTPStore.c
 *
 *  Contains implementation of the MTP store
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPDataStorePrecomp.h"
#include "MTPDataStore.h"
#include "MTPDataStoreCommon.h"
#include "MTPStore.h"

typedef struct _MTPSTOREIMPL
{
    MTPITEMIMPL miImp;
    PSLHANDLE hMutex;
    PSLUINT32 dwStoreId;
    PSLBOOL fValid;
    MTPDATASTORE mds;
    DSSTRING mstrFilePath;
}MTPSTOREIMPL;

static PSLSTATUS _SerializeStorageInfo(
                            MTPSTORE mtpStore,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten,
                            PSLUINT32* pcbLeft);

static const WCHAR _RgchStoreInitMutex[] =  L"MTPStore Init Mutex";

static const PSLUINT32 STORAGEINFO_FIXED_MEMBER_SIZE =
                        sizeof(PSLUINT16) + /* StorageType        */
                        sizeof(PSLUINT16) + /* FileSystemType     */
                        sizeof(PSLUINT16) + /* AccessCapability   */
                        sizeof(PSLUINT64) + /* MaxCapacity        */
                        sizeof(PSLUINT64) + /* FreeSpaceInBytes   */
                        sizeof(PSLUINT32);  /* FreeSpaceInObjects */

/*
 *  MTPStoreCreate
 *
 *
 */
PSLSTATUS MTPStoreCreate(MTPDATASTORE mds, PSLUINT32 dwStoreId,
                         MTPSTORE* pmtpStore)
{
    PSLSTATUS status;
    MTPSTOREIMPL* pmsi = PSLNULL;
    PSLHANDLE hMutex = PSLNULL;

    if (PSLNULL != pmtpStore)
    {
        *pmtpStore = PSLNULL;
    }

    if (PSLNULL == pmtpStore)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmsi = (MTPSTOREIMPL*)PSLMemAlloc(PSLMEM_PTR, sizeof(MTPSTOREIMPL));
    if (PSLNULL == pmsi)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = PSLMutexOpen(PSLTHREAD_INFINITE,
                          (PSLWCHAR*)_RgchStoreInitMutex, &hMutex);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Set the function pointers*/
    pmsi->miImp.pfnMTPItemGetProp = MTPStoreGetProp;
    pmsi->miImp.pfnMTPItemSetProp = MTPStoreSetProp;
    pmsi->miImp.pfnMTPItemDelete = MTPStoreDelete;

    /* Set the Object handle*/
    pmsi->dwStoreId = dwStoreId;

    /* Set as valid*/
    pmsi->fValid = PSLTRUE;

    /* Set the lock*/
    /* hMutex is not set to PSLNULL as
     * it should be released in the Exit label
     */
    pmsi->hMutex = hMutex;

    pmsi->mds = mds;

    *pmtpStore = (MTPDATASTORE)pmsi;
    pmsi = PSLNULL;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutex)
    SAFE_PSLMEMFREE(pmsi);
    return status;
}

/*
 *  MTPStoreDestroy
 *
 *
 */
PSLSTATUS MTPStoreDestroy(MTPSTORE mtpStore)
{
    PSLSTATUS status;
    MTPSTOREIMPL* pmsi;

    if(PSLNULL == mtpStore)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmsi = (MTPSTOREIMPL*)mtpStore;

    SAFE_PSLMUTEXRELEASECLOSE(pmsi->hMutex);

    SAFE_PSLMEMFREE(pmsi);

    status = PSLSUCCESS;

Exit:
    return status;
}

/*
 *  MTPStoreGetProp
 *
 *
 */
PSLSTATUS MTPStoreGetProp(MTPSTORE mtpStore,
                          PSL_CONST MTPDBVARIANTPROP* pmqList,
                          PSLUINT32 cmqList, PSLUINT32 dwDataFormat,
                          PSLBYTE* pbBuf,
                          PSLUINT32 cbBuf, PSLUINT32* pcbWritten,
                          PSLUINT32* pcbLeft)
{
    PSLSTATUS status;
    MTPSTOREIMPL* pmsi;
    PSLHANDLE hMutexRelease = PSLNULL;
    PSLUINT32 cchPathLen;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /* pmqList, cmqList, pcbLeft, pcbWritten & pbBuf
     * can be PSLNULL and cbBuf can be zero
     */
    if (PSLNULL == mtpStore)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmsi = (MTPSTOREIMPL*)mtpStore;

    /* Acquire record level lock */
    status = PSLMutexAcquire(pmsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmsi->hMutex;

    switch(MTPDSDATAFORMAT(dwDataFormat))
    {
        case MTPDSDATAFORMAT_ITEMID:
            {
                if (PSLNULL == pbBuf)
                {
                    cbBuf = sizeof(PSLUINT32);
                    status = PSLSUCCESS;
                    goto Exit;
                }

                if (cbBuf < sizeof(PSLUINT32))
                {
                    status = PSLERROR_INSUFFICIENT_BUFFER;
                    goto Exit;
                }

                MTPStoreUInt32((PSLUINT32*)pbBuf, pmsi->dwStoreId);
            }
            break;
        case MTPDSDATAFORMAT_STORAGEINFO:
            {
                status = _SerializeStorageInfo(mtpStore, pbBuf, cbBuf,
                                               pcbWritten, pcbLeft);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
            }
            break;
        case MTPDSDATAFORMAT_FILEPATH:
            PSLASSERT(0 == cmqList);

            /* this length will include terminating NULL */
            cchPathLen = pmsi->mstrFilePath.cchLen;

            if (cbBuf < (cchPathLen * sizeof(PSLWCHAR)))
            {
                status = PSLERROR_INSUFFICIENT_BUFFER;
                goto Exit;
            }

            pbBuf[0] = (PSLBYTE)cchPathLen;

            /* <TODO>
             * When fToWire is PSLFALSE use PSLStringCopyLen
             */
            status = MTPStoreUTF16String((PSLWCHAR*)&pbBuf[1],
                                (cbBuf - 1)/sizeof(PSLWCHAR), PSLNULL,
                                (PSLWCHAR*)pmsi->mstrFilePath.rgStr,
                                cchPathLen);
            if (PSL_FAILED(status))
            {
                PSLASSERT(!L"MTPStoreGetProp: File path copy failed");
                goto Exit;
            }
            break;
        default:
            status = PSLERROR_UNEXPECTED;
            PSLASSERT(!L"MTPStoreGetProp : Invalid data format");
            break;
    }

    /* Release record level lock */
    status = PSLMutexRelease(pmsi->hMutex);
    PSLASSERT(PSL_SUCCEEDED(status));
    hMutexRelease = PSLNULL;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return status;
pmqList;
cmqList;
}

/*
 *  MTPStoreSetProp
 *
 *
 */
PSLSTATUS MTPStoreSetProp(MTPSTORE mtpStore,
                          PSL_CONST MTPDBVARIANTPROP* pmqList,
                          PSLUINT32 cmqList, PSLUINT32 dwDataFormat,
                          PSLBYTE* pbBuf,
                          PSLUINT32 cbBuf, PSLBOOL fFromWire)
{
    PSLSTATUS status;
    MTPSTOREIMPL* pmsi;
    PSLHANDLE hMutexRelease = PSLNULL;

    /* pmqList can be PSNULL and cmqList can be zero */
    if (PSLNULL == mtpStore || PSLNULL == pbBuf)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmsi = (MTPSTOREIMPL*)mtpStore;

    /* Acquire record level lock */
    status = PSLMutexAcquire(pmsi->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexRelease = pmsi->hMutex;

    switch(MTPDSDATAFORMAT(dwDataFormat))
    {
        case MTPDSDATAFORMAT_FILEPATH:
            PSLASSERT(0 == cmqList);
            PSLASSERT(PSLNULL == pmqList);

            if (cbBuf > (MAX_MTP_STRING_LEN * sizeof(PSLWCHAR)))
            {
                status = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            if (PSLTRUE == fFromWire)
            {
                /* should include terminating NULL */
                pmsi->mstrFilePath.cchLen = pbBuf[0];

                status = MTPLoadUTF16String(
                            (PSLWCHAR*)pmsi->mstrFilePath.rgStr,
                            MAX_MTP_STRING_LEN, PSLNULL,
                            (PSLCWSTR)pbBuf,
                            pmsi->mstrFilePath.cchLen);
                if (PSL_FAILED(status))
                {
                    PSLASSERT(!L"MTPStoreSetProp: File path");
                    goto Exit;
                }
            }
            else
            {
                pmsi->mstrFilePath.cchLen =
                                        (PSLBYTE)cbBuf/sizeof(PSLWCHAR);

                status = PSLStringCopyLen(
                            (PSLWCHAR*)pmsi->mstrFilePath.rgStr,
                            MAX_MTP_STRING_LEN, PSLNULL,
                            (PSLCWSTR)pbBuf,
                            pmsi->mstrFilePath.cchLen);
                if (PSL_FAILED(status))
                {
                    PSLASSERT(!L"MTPStoreSetProp: File path");
                    goto Exit;
                }
            }
            break;
        default:
            PSLASSERT(!L"MTPStoreSetProp: Out format not supported");
            status = PSLERROR_UNEXPECTED;
            break;
    }

    /* Release record level lock */
    status = PSLMutexRelease(pmsi->hMutex);
    PSLASSERT(PSL_SUCCEEDED(status));
    hMutexRelease = PSLNULL;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return status;
cmqList;
pmqList;
}

/*
 *  MTPStoreDelete
 *
 *
 */
PSLSTATUS MTPStoreDelete(MTPSTORE mtpStore)
{
    PSLSTATUS status;
    MTPSTOREIMPL* pmsi;

    if(PSLNULL == mtpStore)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmsi = (MTPSTOREIMPL*)mtpStore;

    SAFE_PSLMUTEXRELEASECLOSE(pmsi->hMutex);

    pmsi->fValid = PSLFALSE;

    pmsi->miImp.pfnMTPItemGetProp = PSLNULL;

    pmsi->miImp.pfnMTPItemSetProp = PSLNULL;

    status = PSLSUCCESS;

Exit:
    return status;
}


/*
 *  MTPStoreIsValid
 *
 *
 */
PSLBOOL MTPStoreIsValid(MTPSTORE mtpStore)
{
    MTPSTOREIMPL* pmsi;
    PSLBOOL fValid = PSLFALSE;

    pmsi = (MTPSTOREIMPL*)mtpStore;

    PSLASSERT(PSLNULL != pmsi);

    if(PSLNULL != pmsi)
    {
        fValid = pmsi->fValid;
    }
    return fValid;
}

PSLBOOL MTPStoreIsReadOnly(MTPSTORE mtpStore)
{
    mtpStore;
    /* <TODO>
     * Check the file system type and make the call
     */
    return PSLFALSE;
}


PSLSTATUS _SerializeStorageInfo(MTPSTORE mtpStore, PSLBYTE* pbBuf,
                                PSLUINT32 cbBuf, PSLUINT32* pcbWritten,
                                PSLUINT32* pcbLeft)
{
    PSLSTATUS status;
    PSLBYTE* pb;
    PSLUINT32 cchLen1;
    PSLUINT32 cchLen2;
    PSLUINT32 cbTotal = 0;
    MTPSTOREIMPL* pmsi;
    WCHAR wszStorageDescFmt[] = L"MTP Store - %d";
    WCHAR wszVolIdFmt[]       = L"MTP Volume - %d";
    WCHAR wszStorageDesc[MAX_MTP_STRING_LEN];
    WCHAR wszVolId[MAX_MTP_STRING_LEN];
    HRESULT hr;
    ULARGE_INTEGER ultotalFree;
    ULARGE_INTEGER ultotalNumber;
    ULARGE_INTEGER ulavailableToCaller;
    PSLUINT64 qwMaxCapacity;
    PSLUINT64 qwFreeSpaceInBytes;
    PSLUINT32 cchDest;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /* pcbWritten, pcbLeft & pbBuf can be PSLNULL */
    if (PSLNULL == mtpStore)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmsi = (MTPSTOREIMPL*)mtpStore;

    /* Create a storage description */
    hr = StringCchPrintf(wszStorageDesc, MAX_MTP_STRING_LEN,
                         wszStorageDescFmt, pmsi->dwStoreId);
    if (FAILED(hr))
    {
        status = hr;
        goto Exit;
    }

    /* Create a volume label */
    hr = StringCchPrintf(wszVolId, MAX_MTP_STRING_LEN,
                         wszVolIdFmt, pmsi->dwStoreId);
    if (FAILED(hr))
    {
        status = hr;
        goto Exit;
    }

    /* calculate raw buffer size */
    status = PSLStringLen((PSLWCHAR*)wszStorageDesc, MAX_MTP_STRING_LEN,
                          (PSLUINT32*)&cchLen1);
    PSLASSERT(PSL_SUCCEEDED(status));

    /* 1 is added to account for the 1 bytes of memory
     * used to store the length of the string
     */
    cbTotal += (sizeof(PSLWCHAR) * (cchLen1 + 1)) + 1;

    status = PSLStringLen((PSLWCHAR*)wszVolId, MAX_MTP_STRING_LEN,
                          (PSLUINT32*)&cchLen2);
    PSLASSERT(PSL_SUCCEEDED(status));

    /* 1 is added to account for the 1 bytes of memory
     * used to store the length of the string
     */
    cbTotal += (sizeof(PSLWCHAR) * (cchLen2 + 1)) + 1;

    cbTotal += STORAGEINFO_FIXED_MEMBER_SIZE;

    /* pbBuf will be PSLNULL when size is required by the caller */
    if (PSLNULL == pbBuf && PSLNULL != pcbLeft)
    {
        *pcbLeft = cbTotal;
        status = PSLSUCCESS;
        goto Exit;
    }

    if (cbBuf < cbTotal)
    {
        status = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    if (PSLNULL == pbBuf)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* pack data into raw buffer */
    pb = pbBuf;

    /* Storage type */
    MTPStoreUInt16((PSLUINT16*)pb, MTP_STORAGETYPE_FIXEDRAM);
    pb += sizeof(PSLUINT16);

    /* File system type */
    MTPStoreUInt16((PSLUINT16*)pb, MTP_FILESYSTEMTYPE_HIERARCHICAL);
    pb += sizeof(PSLUINT16);

    /* Access capability */
    MTPStoreUInt16((PSLUINT16*)pb, MTP_STORAGEACCESSCAPABILITY_RWD);
    pb += sizeof(PSLUINT16);

    /* Max capacity & Free space in bytes */
    if (GetDiskFreeSpaceEx((LPWSTR)(pmsi->mstrFilePath).rgStr, &ulavailableToCaller,
                            &ultotalNumber, &ultotalFree))
    {
        qwMaxCapacity = ultotalNumber.QuadPart;
        qwFreeSpaceInBytes = ulavailableToCaller.QuadPart;
     }
    else
    {
        qwMaxCapacity = 0xFFFFFFFFFFFFFFFF;
        qwFreeSpaceInBytes = 0xFFFFFFFFFFFFFFFF;
    }

    MTPStoreUInt64((PSLUINT64*)pb, &qwMaxCapacity);
    pb += sizeof(PSLUINT64);

    MTPStoreUInt64((PSLUINT64*)pb, &qwFreeSpaceInBytes);
    pb += sizeof(PSLUINT64);

    /* Free space in objects */
    MTPStoreUInt32((PSLUINT32*)pb, 0);
    pb += sizeof(PSLUINT32);

    /* Storage description */
    PSLASSERT(MAX_MTP_STRING_LEN >= (cchLen1 + 1));
    *pb = (PSLBYTE)cchLen1 + 1;
    pb = pb + 1;

    status = MTPStoreUTF16String((PSLWCHAR*)pb, cchLen1 + 1, &cchDest,
                             (PSLWCHAR*)wszStorageDesc, cchLen1 + 1);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"_SerializeStorageInfo: wszStorageDesc");
        goto Exit;
    }
    pb = pb + sizeof(PSLWCHAR) * (cchLen1 + 1);

    /* Volume Identifier */
    PSLASSERT(MAX_MTP_STRING_LEN >= (cchLen2 + 1));
    *pb = (PSLBYTE)cchLen2 + 1;
    pb = pb + 1;

    status = MTPStoreUTF16String((PSLWCHAR*)pb, cchLen2 + 1, &cchDest,
                                 (PSLWCHAR*)wszVolId, cchLen2 + 1);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"_SerializeStorageInfo: wszVolId");
        goto Exit;
    }
    pb = pb + sizeof(PSLWCHAR) * (cchLen2 + 1);

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = cbTotal;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    status = PSLSUCCESS;

Exit:
    return status;
}
