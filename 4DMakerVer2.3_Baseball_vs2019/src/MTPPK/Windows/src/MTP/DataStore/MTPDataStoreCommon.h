/*
 *  MTPDataStoreCommon.h
 *
 *  Precompiled header file for data store support in windows.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPDATASTORECOMMON_H_
#define _MTPDATASTORECOMMON_H_

/*
 *  PFNMTPMSGDISPATCH
 *
 *  Callback function used by the message dispatcher to transfer
 *  processing of messages to the correct handler.
 *
 */

typedef PSLSTATUS (PSL_API* PFNMTPITEMGETPROP)(
                            MTPITEM mtpItem,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            PSLUINT32 dwDataFormat,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten,
                            PSLUINT32* pcbLeft);

typedef PSLSTATUS (PSL_API* PFNMTPITEMSETPROP)(
                            MTPITEM mtpItem,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            PSLUINT32 dwDataFormat,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbBuf,
                            PSLBOOL fFromWire);

typedef PSLSTATUS (PSL_API* PFNMTPITEMDELETE)(
                            MTPITEM mtpItem);

typedef struct _MTPITEMIMPL
{
    PFNMTPITEMGETPROP pfnMTPItemGetProp;
    PFNMTPITEMSETPROP pfnMTPItemSetProp;
    PFNMTPITEMDELETE pfnMTPItemDelete;
} MTPITEMIMPL;

typedef struct _DSSTRING
{
    PSLUINT8 cchLen;
    PSLWCHAR rgStr[MAX_MTP_STRING_LEN];
} DSSTRING;

#endif  /* _MTPDATASTORECOMMON_H_ */

