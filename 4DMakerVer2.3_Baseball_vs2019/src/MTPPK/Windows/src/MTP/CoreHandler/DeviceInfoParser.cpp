/*
 *  DeviceInfoParser.cpp
 *
 *
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */
#include "MTPHandlerPrecomp.h"
#include "IXMLElementHandler.h"
#include "DeviceInfoParser.h"
#include "DeviceInfoHandler.h"

#pragma warning(disable: 4100) /* unreferenced formal parameter */

static const WCHAR _RgchDataSet[]       = L"DataSet";
static const WCHAR _RgchDeviceInfo[]    = L"DeviceInfo";
static const WCHAR _RgchDataSetName[]   = L"DataSetName";

typedef HRESULT (*PFNXMLEHCREATOR)(
                            IXMLElementHandler** ppXMLEHandler);

typedef struct XMLELEMENTHANDLERMAP
{
    INT                 nElementHandler;
    LPCWSTR             szElementName;
    INT                 cchElementName;
    PFNXMLEHCREATOR     pfnXMLEHCreator;
}XMLELEMENTHANDLERMAP;

static const XMLELEMENTHANDLERMAP _RgEHMap[] =
{
    {
        ELEMENT_HANDLER_DEVICEINFO,
        _RgchDeviceInfo,
        PSLARRAYSIZE(_RgchDeviceInfo)-1,
        CDeviceInfoHandler_CreateInstance,
    }
};




/* Construction/Destruction */
CDeviceInfoParser::CDeviceInfoParser() :
    m_cRef(0),
    m_pElementHandler(NULL)
{
}

CDeviceInfoParser::~CDeviceInfoParser()
{
    Shutdown();
}


HRESULT CDeviceInfoParser::putDocumentLocator(ISAXLocator *pLocator)
{
    return S_OK;
}

HRESULT CDeviceInfoParser::startDocument()
{
    SAFE_RELEASE(m_pElementHandler);
    return S_OK;
}



HRESULT CDeviceInfoParser::endDocument()
{
    SAFE_RELEASE(m_pElementHandler);
    return S_OK;
}


HRESULT CDeviceInfoParser::startPrefixMapping(
            LPCWSTR pwchPrefix,
            INT cchPrefix,
            LPCWSTR pwchUri,
            INT cchUri)
{
    return S_OK;
}


HRESULT CDeviceInfoParser::endPrefixMapping(
            LPCWSTR pwchPrefix,
            INT cchPrefix)
{
    return S_OK;
}



HRESULT CDeviceInfoParser::startElement(
            LPCWSTR pwchNamespaceUri,
            INT cchNamespaceUri,
            LPCWSTR pwchLocalName,
            INT cchLocalName,
            LPCWSTR pwchQName,
            INT cchQName,
            ISAXAttributes* pAttributes)
{
    HRESULT hr;
    DWORD i = 0;

    /*
     * Make sure the string lengths are not out of bounds and that we
     * have the namespace and local name
     */
    if ((NULL == pwchNamespaceUri) || !IsValidNamespaceLen(cchNamespaceUri) ||
        (NULL == pwchLocalName) || !IsValidLocalNameLen(cchLocalName) ||
        !IsValidQNameLen(cchQName))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    /*
     *  Check to see if we are need to create an element handler
     */
    if (NULL == m_pElementHandler)
    {
        for (i = 0; i < PSLARRAYSIZE(_RgEHMap); i++)
        {
            if ( (cchLocalName == _RgEHMap[i].cchElementName) &&
                 (0 == _wcsnicmp(pwchLocalName, _RgEHMap[i].szElementName,
                                                                cchLocalName)))
            {
                hr = _RgEHMap[i].pfnXMLEHCreator(&m_pElementHandler);
                goto Exit;
            }
        }
    }

    /* if we've already have the handler */
    if (NULL != m_pElementHandler)
    {
        hr = m_pElementHandler->ElementStart(pwchNamespaceUri,
                                             cchNamespaceUri,
                                             pwchLocalName,
                                             cchLocalName,
                                             pAttributes);
        if (FAILED(hr))
        {
            goto Exit;
        }
    }

    /* move to next element */
    hr = S_OK;

Exit:
    return hr;
}


HRESULT CDeviceInfoParser::endElement(
            LPCWSTR pwchNamespaceUri,
            INT cchNamespaceUri,
            LPCWSTR pwchLocalName,
            INT cchLocalName,
            LPCWSTR pwchQName,
            INT cchQName)
{
    HRESULT hr;
    DWORD i;

    if ((NULL == pwchNamespaceUri) || !IsValidNamespaceLen(cchNamespaceUri) ||
        (NULL == pwchLocalName) || !IsValidLocalNameLen(cchLocalName) ||
        !IsValidQNameLen(cchQName))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    /*
     *  Check to see if we are done with this element handler
     */
    if (NULL != m_pElementHandler)
    {
        for (i = 0; i < PSLARRAYSIZE(_RgEHMap); i++)
        {
            if ( (cchLocalName == _RgEHMap[i].cchElementName) &&
                 (0 == _wcsnicmp(pwchLocalName, _RgEHMap[i].szElementName,
                                                                cchLocalName)))
            {
                SAFE_RELEASE(m_pElementHandler);
                hr = S_OK;
                goto Exit;
            }
        }
    }

    if (NULL != m_pElementHandler)
    {
        hr = m_pElementHandler->ElementEnd(pwchNamespaceUri,
                                           cchNamespaceUri,
                                           pwchLocalName,
                                           cchLocalName);
        if (FAILED(hr))
        {
            goto Exit;
        }
    }

    /* set to S_OK to continue... */
    hr = S_OK;

Exit:
    return hr;
}

HRESULT CDeviceInfoParser::characters(
        LPCWSTR pwchChars,
        INT cchChars)
{
    HRESULT hr;

    /* Validate input */
    if ((NULL == pwchChars) || !IsValidCharacterLen(cchChars))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }
    if (NULL != m_pElementHandler)
    {
        hr = m_pElementHandler->ElementCharacters(pwchChars, cchChars);
        if (FAILED(hr))
        {
            goto Exit;
        }
    }

    hr = S_OK;

Exit:
    return hr;
}


HRESULT CDeviceInfoParser::ignorableWhitespace(
            LPCWSTR pwchChars,
            INT cchChars)
{
    return S_OK;
}


HRESULT CDeviceInfoParser::processingInstruction(
            LPCWSTR pwchTarget,
            INT cchTarget,
            LPCWSTR pwchData,
            INT cchData)
{
    return S_OK;
}


HRESULT CDeviceInfoParser::skippedEntity(
            LPCWSTR pwchName,
            INT cchName)
{
    return S_OK;
}

HRESULT CDeviceInfoParser::Shutdown()
{
    HRESULT hr;

    SAFE_RELEASE(m_pElementHandler);

    hr = S_OK;

    return hr;
}


HRESULT CDeviceInfoParser::CreateInstance(ISAXContentHandler** ppParser)
{
    HRESULT hr;

    ISAXContentHandler* pParser = NULL;

    if( NULL == ppParser )
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    *ppParser = NULL;

    pParser = NEW CDeviceInfoParser();

    if( NULL == pParser )
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }
    SAFE_ADDREF( pParser );

    *ppParser = pParser;

    pParser = NULL;

    hr = S_OK;

Exit:
    SAFE_RELEASE( pParser );
    return hr;
}

HRESULT CDeviceInfoParser_CreateInstance(ISAXContentHandler** ppParser)
{
    return CDeviceInfoParser::CreateInstance(ppParser);
}


