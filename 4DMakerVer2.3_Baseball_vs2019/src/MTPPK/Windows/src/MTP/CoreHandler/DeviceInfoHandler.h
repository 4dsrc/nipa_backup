/*
 *  DeviceInfoHandler.h
 *
 *
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */
#ifndef _DEVICEINFOHANDLER_H_
#define _DEVICEINFOHANDLER_H_

class CDeviceInfoHandler : public IXMLElementHandler
{
public:
    virtual ~CDeviceInfoHandler( );

    /* IUnknown functions */
    STDMETHOD(QueryInterface)(REFIID riid, LPVOID* ppvObj)
    {
        HRESULT hr;

        if (NULL != ppvObj)
        {
            ppvObj = NULL;
        }

        if (NULL == ppvObj)
        {
            hr = E_INVALIDARG;
            goto Exit;
        }

        if (IID_IUnknown == riid || IID_IXMLElementHandler == riid)
        {
            *ppvObj = (IXMLElementHandler*)this;
            SAFE_ADDREF((CDeviceInfoHandler*)(*ppvObj));
            hr = S_OK;
        }
        else
        {
            hr = E_NOINTERFACE;
        }
Exit:
        return hr;
    }

    STDMETHOD_(ULONG, AddRef)()
    {
        return InterlockedIncrement(&m_cRef);
    }

    STDMETHOD_(ULONG, Release)()
    {
        LONG cRef = InterlockedDecrement(&m_cRef);
        if (cRef == 0)
        {
            delete (this);
        }
        return cRef;
    }

    /* IXMLElementHandler */
    virtual HRESULT STDMETHODCALLTYPE ElementStart(
                            LPCWSTR szNamespace,
                            DWORD cchNamespace,
                            LPCWSTR szLocalName,
                            DWORD cchLocalName,
                            ISAXAttributes* pAttributes);

    virtual HRESULT STDMETHODCALLTYPE ElementCharacters(
                            LPCWSTR szChars,
                            DWORD cchChars);

    virtual HRESULT STDMETHODCALLTYPE ElementEnd(
                            LPCWSTR szNamespace,
                            DWORD cchNamespace,
                            LPCWSTR szLocalName,
                            DWORD cchLocalName);

    virtual HRESULT STDMETHODCALLTYPE ElementTerminate();

    virtual HRESULT STDMETHODCALLTYPE ParseAttribute(
                            DWORD idxAttr,
                            DWORD dwToken,
                            LPCWSTR szValue,
                            DWORD cchValue);

    HRESULT Init( );

    HRESULT Shutdown( );

    static HRESULT OnDWordValue(
                            CDeviceInfoHandler* pParser,
                            LPCWSTR szLocalName,
                            DWORD cchLocalName);

    static HRESULT OnWordValue(
                            CDeviceInfoHandler* pParser,
                            LPCWSTR szLocalName,
                            DWORD cchLocalName);

    static HRESULT OnStringValue(
                            CDeviceInfoHandler* pParser,
                            LPCWSTR szLocalName,
                            DWORD cchLocalName);

    static HRESULT CreateInstance(
                            IXMLElementHandler** ppDIHandler);

private:
    LONG    m_cRef;
    INT     m_nPropInParsing;
    INT     m_nPropParent;

    INT     m_nNumValues;
    INT     m_cCountOffset;

    BYTE*   m_pBuf;
    UINT32  m_cbBuf;
    UINT32  m_cbRequestedBuf;
    BYTE*   m_pCurBuf;

    static CDeviceInfoHandler*  m_pHandler;

    CDeviceInfoHandler();

    HRESULT ParseExtendedAttributes(
                            DWORD dwPropType,
                            ISAXAttributes* pAttributes);


    static HRESULT ValidateArguments(
                            CDeviceInfoHandler* pParser,
                            LPCWSTR szLocalName,
                            DWORD cchLocalName);
    HRESULT ResizeBuffer(
                            DWORD dwCount,
                            DWORD dwSize,
                            DWORD dwHeaderSize);

    HRESULT PackString(
                            LPCWSTR szLocalName,
                            DWORD cchLocalName);

    HRESULT ParseAttribute(
                            ISAXAttributes* pAttributes,
                            const WCHAR* wszName,
                            int cchName,
                            PSLBYTE** ppBuf);
};

HRESULT CDeviceInfoHandler_CreateInstance(
                            IXMLElementHandler** ppDIHandler);

#endif /*_DEVICEINFOHANDLER_H_*/
