/*
 *  MTPHandlerExtensionsUtil.h
 *
 *  Precompiled header file for extension suppport.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPHANDLEREXTENSIONSUTIL_H_
#define _MTPHANDLEREXTENSIONSUTIL_H_

typedef struct _EXTENSIONINFO
{
    HMODULE             hModule;
    LPCWSTR             wszSourceBinary;
    LPCWSTR             wszFnInit;
    LPCWSTR             wszFnUninit;
} EXTENSIONINFO;

PSL_EXTERN_C PSLSTATUS PSL_API MTPExtensionAddOpItem(
                            EXTENSIONINFO* pExtInfo);

PSL_EXTERN_C PSLSTATUS PSL_API MTPExtensionAddPropItem(
                            EXTENSIONINFO* pExtInfo);

PSL_EXTERN_C PSLSTATUS PSL_API MTPExtensionIsOpItemPresent(
                            EXTENSIONINFO* pExtInfo);

PSL_EXTERN_C PSLSTATUS PSL_API MTPExtensionIsPropItemPresent(
                            EXTENSIONINFO* pExtInfo);

#endif  /* _MTPHANDLEREXTENSIONSUTIL_H_ */
