/*
 *  DeviceInfoParser.h
 *
 *
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */
#ifndef _DEVICEINFOPARSER_H_
#define _DEVICEINFOPARSER_H_

class CDeviceInfoParser : public ISAXContentHandler
{
public:
    virtual ~CDeviceInfoParser();

    static HRESULT CreateInstance(ISAXContentHandler** ppParser);

    /*
     * IUnknown functions
     */
    STDMETHOD(QueryInterface)(REFIID riid, LPVOID* ppvObj)
    {
        HRESULT hr;

        if (NULL != ppvObj)
        {
          *ppvObj = NULL;
        }

        if (ppvObj == NULL)
        {
            hr = E_INVALIDARG;
            goto Exit;
        }

        if (riid == IID_IUnknown || riid == IID_ISAXContentHandler)
        {
            *ppvObj = (ISAXContentHandler*)this;
            SAFE_ADDREF((CDeviceInfoParser*)(*ppvObj));
            hr = S_OK;
        }
        else
        {
            hr = E_NOINTERFACE;
        }
Exit:
        return hr;
    }

    STDMETHOD_(ULONG, AddRef)()
    {
        return InterlockedIncrement(&m_cRef) ;
    }

    STDMETHOD_(ULONG, Release)()
    {
        LONG cRef = InterlockedDecrement(&m_cRef);
        if (cRef == 0)
        {
            delete (this);
        }
        return cRef;
    }

    virtual HRESULT STDMETHODCALLTYPE putDocumentLocator(
                            ISAXLocator*    pLocator);

    virtual HRESULT STDMETHODCALLTYPE startDocument();

    virtual HRESULT STDMETHODCALLTYPE endDocument();

    virtual HRESULT STDMETHODCALLTYPE startPrefixMapping(
                            LPCWSTR         pwchPrefix,
                            INT             cchPrefix,
                            LPCWSTR         pwchUri,
                            INT             cchUri);

    virtual HRESULT STDMETHODCALLTYPE endPrefixMapping(
                            LPCWSTR         pwchPrefix,
                            INT             cchPrefix);

    virtual HRESULT STDMETHODCALLTYPE startElement(
                            LPCWSTR         pwchNamespaceUri,
                            INT             cchNamespaceUri,
                            LPCWSTR         pwchLocalName,
                            INT             cchLocalName,
                            LPCWSTR         pwchQName,
                            INT             cchQName,
                            ISAXAttributes* pAttributes);

    virtual HRESULT STDMETHODCALLTYPE endElement(
                            LPCWSTR         pwchNamespaceUri,
                            INT             cchNamespaceUri,
                            LPCWSTR         pwchLocalName,
                            INT             cchLocalName,
                            LPCWSTR         pwchQName,
                            INT             cchQName);

    virtual HRESULT STDMETHODCALLTYPE characters(
                            LPCWSTR         pwchChars,
                            INT             cchChars);

    virtual HRESULT STDMETHODCALLTYPE ignorableWhitespace(
                            LPCWSTR         pwchChars,
                            INT             cchChars);

    virtual HRESULT STDMETHODCALLTYPE processingInstruction(
                            LPCWSTR         pwchTarget,
                            INT             cchTarget,
                            LPCWSTR         pwchData,
                            INT             cchData);

    virtual HRESULT STDMETHODCALLTYPE skippedEntity(
                            LPCWSTR         pwchName,
                            INT             cchName);

    HRESULT Shutdown();

private:
    LONG                m_cRef;
    IXMLElementHandler* m_pElementHandler;
    CDeviceInfoParser();
};

HRESULT CDeviceInfoParser_CreateInstance(ISAXContentHandler** ppParser);

#endif /* _DEVICEINFOPARSER_H_ */
