/*
 *  MTPHandlerDeviceInfo.cpp
 *
 *
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */
#include "MTPHandlerPrecomp.h"
#include "IXMLElementHandler.h"
#include "DeviceInfoParser.h"
#include "DeviceInfoHandler.h"
#include "MTPHandlerDeviceInfo.h"

#define DEVICESETTINGSXML_SHIPDIR  L"\\Windows\\XMPSDeviceSettings.xml"
#define DEVICESETTINGSXML_DEBUGDIR L"\\Release\\DeviceSettings.xml"

#define DEVICESETTINGSXML_NAME  L"DeviceSettings.xml"

HRESULT _ParseXML(void);

BYTE*   g_pDeviceInfoBuf    = NULL;
BYTE*   g_pDeviceInfoCur    = NULL;
DWORD   g_cbDeviceInfoBuf   = 0;
DWORD   g_cbDeviceInfoLeft  = 0;

PSLSTATUS MTPGetDeviceInfoSerialize(PSLBYTE* pBuf, PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten, PSLUINT32* pcbRemaining)
{
    PSLSTATUS status;
    PSLUINT32 dwBytesCopied;
    HRESULT hr;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbRemaining)
    {
        *pcbRemaining = 0;
    }

    if ((PSLNULL == pcbRemaining) ||
        ((PSLNULL == pBuf) && (0 < cbBuf)))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (NULL == g_pDeviceInfoBuf)
    {
        hr = _ParseXML();
        if (FAILED(hr))
        {
            status = PSLERROR_PLATFORM_FAILURE;
            goto Exit;
        }
    }

    if (0 == cbBuf)
    {
        PSLASSERT(PSLNULL != g_pDeviceInfoBuf);

        g_pDeviceInfoCur    = g_pDeviceInfoBuf;
        g_cbDeviceInfoLeft  = g_cbDeviceInfoBuf;

        *pcbRemaining = (PSLUINT32)g_cbDeviceInfoLeft;
        status = PSLSUCCESS;
        goto Exit;
    }

    if (PSLNULL == pcbWritten)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    dwBytesCopied = (cbBuf < (PSLUINT32)g_cbDeviceInfoLeft) ?       \
                               cbBuf : (PSLUINT32)g_cbDeviceInfoLeft;

    PSLCopyMemory(pBuf, (PSLBYTE*)g_pDeviceInfoCur, dwBytesCopied);

    g_pDeviceInfoCur += dwBytesCopied;

    *pcbWritten = dwBytesCopied;

    g_cbDeviceInfoLeft =
              ((DWORD)dwBytesCopied > g_cbDeviceInfoLeft) ? 0 :   \
                        (g_cbDeviceInfoLeft - (DWORD)dwBytesCopied);

    *pcbRemaining = (PSLUINT32)g_cbDeviceInfoLeft;
    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS MTPGetDeviceInfoCleanup()
{
    SAFE_ARRAYDELETE(g_pDeviceInfoBuf);
    return PSLSUCCESS;
}

HRESULT _ParseXML(void)
{
    HRESULT hr;
    ISAXXMLReader* pSAXReader = NULL;
    ISAXContentHandler* pParser = NULL;
    CRITICAL_SECTION csLock;
    WCHAR wszDevInfoPath[MAX_PATH] = {0};
    DWORD dwFileAttributes;
    WCHAR* pwchar;

    if (0 == InitializeCriticalSectionAndSpinCount(&csLock,0))
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        goto Exit;
    }

    EnterCriticalSection(&csLock);

    hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = CoCreateInstance(__uuidof(SAXXMLReader), NULL, CLSCTX_ALL,
                          __uuidof(ISAXXMLReader),(void**)&pSAXReader);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = CDeviceInfoParser_CreateInstance(&pParser);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = pSAXReader->putContentHandler(pParser);
    if (FAILED(hr))
    {
        goto Exit;
    }

    if (GetModuleFileName(NULL, wszDevInfoPath, MAX_PATH) == 0)
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        goto Exit;
    }

    pwchar = wcsrchr(wszDevInfoPath, L'\\');

    if (NULL == pwchar)
    {
        ASSERT(!"Invalid Path");
        hr = E_UNEXPECTED;
        goto Exit;
    }

    *(pwchar + 1) = L'\0';

    hr = StringCchCat(wszDevInfoPath, MAX_PATH,
                        DEVICESETTINGSXML_NAME);
    if (FAILED(hr))
    {
        goto Exit;
    }

    dwFileAttributes = GetFileAttributes(wszDevInfoPath);

    if ((INVALID_FILE_ATTRIBUTES == dwFileAttributes) ||
        (0 != (FILE_ATTRIBUTE_DIRECTORY & dwFileAttributes)))
    {
        PSLASSERT(!"DeviceSettings.XML not found");
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        goto Exit;
    }

#if !defined(SHIP_BUILD) || defined(x86)

    hr = pSAXReader->parseURL(wszDevInfoPath);
    if( FAILED(hr) )
#endif // !defined(SHIP_BUILD) || defined(x86)
        hr = pSAXReader->parseURL(wszDevInfoPath);
    if (FAILED(hr))
    {
        goto Exit;
    }

Exit:
    SAFE_RELEASE(pSAXReader);
    SAFE_RELEASE(pParser);

    CoUninitialize();

    LeaveCriticalSection(&csLock);
    DeleteCriticalSection(&csLock);
    return hr;
}
