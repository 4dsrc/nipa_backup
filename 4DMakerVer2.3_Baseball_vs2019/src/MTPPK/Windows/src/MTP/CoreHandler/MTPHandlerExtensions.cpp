/*
 *  MTPHandlerExtensions.cpp
 *
 *
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPHandlerPrecomp.h"
#include "MTPExtensions.h"
#include "MTPHandlerExtensions.h"
#include "MTPHandlerExtensionsUtil.h"

static PSLDYNLIST _PdlOperations = PSLNULL;
static PSLDYNLIST _PdlDeviceProps = PSLNULL;

PSLSTATUS MTPExtensionInitialize()
{
    PSLSTATUS status;
    PSLDYNLIST pdlOperations = PSLNULL;
    PSLDYNLIST pdlDeviceProps = PSLNULL;

    PSLASSERT(PSLNULL == _PdlOperations &&
              PSLNULL == _PdlDeviceProps);

    if (PSLNULL != _PdlOperations || PSLNULL != _PdlDeviceProps)
    {
        status = PSLERROR_IN_USE;
        goto Exit;
    }

    status = PSLDynListCreate(0, PSLFALSE, PSLDYNLIST_GROW_DEFAULT,
                                &pdlOperations);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = PSLDynListCreate(0, PSLFALSE, PSLDYNLIST_GROW_DEFAULT,
                                &pdlDeviceProps);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    _PdlOperations = pdlOperations;
    pdlOperations = PSLNULL;

    _PdlDeviceProps = pdlDeviceProps;
    pdlDeviceProps = PSLNULL;

    status = PSLSUCCESS;

Exit:
    SAFE_PSLDYNLISTDESTROY(pdlOperations);
    SAFE_PSLDYNLISTDESTROY(pdlDeviceProps);
    return status;
}

PSLSTATUS MTPExtensionUninitialize()
{
    PSLSTATUS status;
    PSLUINT32 cItems;
    PSLUINT32 dwIndex;
    EXTENSIONINFO* pExtInfo;

    if (PSLNULL != _PdlOperations)
    {
        status = PSLDynListGetSize(_PdlOperations, &cItems);
        if (PSL_FAILED(status))
        {
            PSLASSERT(PSLFALSE);
            goto Exit;
        }
        for (dwIndex = 0; dwIndex < cItems; dwIndex++)
        {
            status = PSLDynListGetItem(_PdlOperations, dwIndex,
                                       (PSLPARAM*)&pExtInfo);
            if(PSL_SUCCEEDED(status))
            {
                SAFE_FREELIBRARY(pExtInfo->hModule);
                SAFE_PSLMEMFREE(pExtInfo);
            }
        }
    }

    if (PSLNULL != _PdlDeviceProps)
    {
        status = PSLDynListGetSize(_PdlDeviceProps, &cItems);
        if (PSL_FAILED(status))
        {
            PSLASSERT(PSLFALSE);
            goto Exit;
        }

        for (dwIndex = 0; dwIndex < cItems; dwIndex++)
        {
            status = PSLDynListGetItem(_PdlDeviceProps, dwIndex,
                                       (PSLPARAM*)&pExtInfo);
            if(PSL_SUCCEEDED(status))
            {
                SAFE_FREELIBRARY(pExtInfo->hModule);
                SAFE_PSLMEMFREE(pExtInfo);
            }
        }
    }

    SAFE_PSLDYNLISTDESTROY(_PdlOperations);
    SAFE_PSLDYNLISTDESTROY(_PdlDeviceProps);

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS MTPExtensionInitOps(PSLLOOKUPTABLE plt,
                              PSLUINT32 dwSesState,
                              PCXPSLUINT16 pwCodes,
                              PSLUINT32 cdwCodes)
{
    PSLSTATUS status;
    PSLUINT32 cItems;
    PSLUINT32 dwIndex;
    EXTENSIONINFO* pExtInfo;
    PFNEXTENSIONINIT pfnExtInit;
#ifndef UNDER_CE
    PSLUINT32 cchLen;
    LPSTR szFnInit[MAX_PATH] = {0};
#endif /* !UNDER_CE */

    if (PSLNULL == _PdlOperations)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    status = PSLDynListGetSize(_PdlOperations, &cItems);
    if (PSL_FAILED(status))
    {
        PSLASSERT(PSLFALSE);
        goto Exit;
    }
    for (dwIndex = 0; dwIndex < cItems; dwIndex++)
    {
        status = PSLDynListGetItem(_PdlOperations, dwIndex,
                                   (PSLPARAM*)&pExtInfo);
        if(PSL_SUCCEEDED(status))
        {
            if (PSLNULL == pExtInfo->hModule)
            {
                pExtInfo->hModule = LoadLibrary(
                                          pExtInfo->wszSourceBinary);
                if (PSLNULL == pExtInfo->hModule)
                {
                    PSLASSERT(!(L"Failed to load library %d",
                                    pExtInfo->wszSourceBinary));
                    status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                    goto Exit;
                }
            }

#ifndef UNDER_CE
            status = PSLStringLen((PSLCWSTR)pExtInfo->wszFnInit,
                                  MAX_PATH, &cchLen);
            if (PSL_FAILED(status))
            {
                PSLASSERT(!L"Lenth of Init function name \
                            cannot be more than MAX_PATH");
                goto Exit;
            }

            if ( 0 == WideCharToMultiByte(CP_ACP, 0,
                                          pExtInfo->wszFnInit,
                                          cchLen, (LPSTR)szFnInit,
                                          PSLARRAYSIZE(szFnInit),
                                          NULL, NULL))
            {
                    status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                    goto Exit;
            }

            pfnExtInit = (PFNEXTENSIONINIT)GetProcAddress(
                                                pExtInfo->hModule,
                                                (LPCSTR)szFnInit);
            if (PSLNULL == pfnExtInit)
            {
                PSLASSERT(!("Failed to GetProcAddress %s",
                                szFnInit));
                status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                goto Exit;
            }
#else
            pfnExtInit = (PFNEXTENSIONINIT)GetProcAddress(
                                                pExtInfo->hModule,
                                                pExtInfo->wszFnInit);
            if (PSLNULL == pfnExtInit)
            {
                PSLASSERT(!(L"Failed to GetProcAddress %s",
                                pExtInfo->wszFnInit));
                status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                goto Exit;
            }
#endif /* UNDER_CE */

            status = pfnExtInit(plt, dwSesState, pwCodes, cdwCodes);
            if (PSL_FAILED(status))
            {
                PSLASSERT(!(L"Init operation call failed handler: %s",
                                pExtInfo->wszSourceBinary));
                goto Exit;
            }
        }
    }
Exit:
    return status;
}

PSLSTATUS MTPExtensionUninitOps(PSLLOOKUPTABLE plt,
                                PSLUINT32 dwSesState)
{
    PSLSTATUS status;
    PSLUINT32 cItems;
    PSLUINT32 dwIndex;
    EXTENSIONINFO* pExtInfo;
    PFNEXTENSIONUNINIT pfnExtUninit;
#ifndef UNDER_CE
    PSLUINT32 cchLen;
    LPSTR szFnUninit[MAX_PATH] = {0};
#endif /* !UNDER_CE */

    if (PSLNULL == _PdlOperations)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    status = PSLDynListGetSize(_PdlOperations, &cItems);
    if (PSL_FAILED(status))
    {
        PSLASSERT(PSLFALSE);
        goto Exit;
    }
    for (dwIndex = 0; dwIndex < cItems; dwIndex++)
    {
        status = PSLDynListGetItem(_PdlOperations, dwIndex,
                                   (PSLPARAM*)&pExtInfo);
        if(PSL_SUCCEEDED(status))
        {
            if (PSLNULL == pExtInfo->hModule)
            {
                pExtInfo->hModule = LoadLibrary(
                                          pExtInfo->wszSourceBinary);
                if (PSLNULL == pExtInfo->hModule)
                {
                    PSLASSERT(!(L"Failed to load library %d",
                                    pExtInfo->wszSourceBinary));
                    status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                    goto Exit;
                }
            }

#ifndef UNDER_CE
            status = PSLStringLen((PSLCWSTR)pExtInfo->wszFnUninit,
                                  MAX_PATH, &cchLen);
            if (PSL_FAILED(status))
            {
                PSLASSERT(!L"Lenth of Uninit function name \
                            cannot be more than MAX_PATH");
                goto Exit;
            }

            if ( 0 == WideCharToMultiByte(CP_ACP, 0,
                                          pExtInfo->wszFnUninit,
                                          cchLen, (LPSTR)szFnUninit,
                                          PSLARRAYSIZE(szFnUninit),
                                          NULL, NULL))
            {
                    status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                    goto Exit;
            }

            pfnExtUninit = (PFNEXTENSIONUNINIT)GetProcAddress(
                                                pExtInfo->hModule,
                                                (LPCSTR)szFnUninit);
            if (PSLNULL == pfnExtUninit)
            {
                PSLASSERT(!("Failed to GetProcAddress %s",
                                szFnUninit));
                status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                goto Exit;
            }
#else
            pfnExtUninit = (PFNEXTENSIONUNINIT)GetProcAddress(
                                                pExtInfo->hModule,
                                                pExtInfo->wszFnUninit);
            if (PSLNULL == pfnExtUninit)
            {
                PSLASSERT(!(L"Failed to GetProcAddress %s",
                                pExtInfo->wszFnUninit));
                status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                goto Exit;
            }
#endif /* UNDER_CE */

            status = pfnExtUninit(plt, dwSesState);
            if (PSL_FAILED(status))
            {
                PSLASSERT(!(L"Init operation call failed handler: %s",
                                pExtInfo->wszSourceBinary));
                goto Exit;
            }
        }
    }
Exit:
    return status;
}

PSLSTATUS MTPExtensionInitDeviceProps(PSLLOOKUPTABLE plt,
                                      PSLUINT32 dwSesState,
                                      PCXPSLUINT16 pwCodes,
                                      PSLUINT32 cdwCodes)
{
    PSLSTATUS status;
    PSLUINT32 cItems;
    PSLUINT32 dwIndex;
    EXTENSIONINFO* pExtInfo;
    PFNEXTENSIONINIT pfnExtInit;
#ifndef UNDER_CE
    PSLUINT32 cchLen;
    LPSTR szFnInit[MAX_PATH] = {0};
#endif /* !UNDER_CE */

    if (PSLNULL == _PdlDeviceProps)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    status = PSLDynListGetSize(_PdlDeviceProps, &cItems);
    if (PSL_FAILED(status))
    {
        PSLASSERT(PSLFALSE);
        goto Exit;
    }
    for (dwIndex = 0; dwIndex < cItems; dwIndex++)
    {
        status = PSLDynListGetItem(_PdlDeviceProps, dwIndex,
                                   (PSLPARAM*)&pExtInfo);
        if(PSL_SUCCEEDED(status))
        {
            if (PSLNULL == pExtInfo->hModule)
            {
                pExtInfo->hModule = LoadLibrary(
                                          pExtInfo->wszSourceBinary);
                if (PSLNULL == pExtInfo->hModule)
                {
                    PSLASSERT(!(L"Failed to load library %d",
                                    pExtInfo->wszSourceBinary));
                    status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                    goto Exit;
                }
            }

#ifndef UNDER_CE
            status = PSLStringLen((PSLCWSTR)pExtInfo->wszFnInit,
                                  MAX_PATH, &cchLen);
            if (PSL_FAILED(status))
            {
                PSLASSERT(!L"Lenth of Init function name \
                            cannot be more than MAX_PATH");
                goto Exit;
            }

            if ( 0 == WideCharToMultiByte(CP_ACP, 0,
                                          pExtInfo->wszFnInit,
                                          cchLen, (LPSTR)szFnInit,
                                          PSLARRAYSIZE(szFnInit),
                                          NULL, NULL))
            {
                    status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                    goto Exit;
            }

            pfnExtInit = (PFNEXTENSIONINIT)GetProcAddress(
                                                pExtInfo->hModule,
                                                (LPCSTR)szFnInit);
            if (PSLNULL == pfnExtInit)
            {
                PSLASSERT(!("Failed to GetProcAddress %s",
                                szFnInit));
                status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                goto Exit;
            }
#else
            pfnExtInit = (PFNEXTENSIONINIT)GetProcAddress(
                                                pExtInfo->hModule,
                                                pExtInfo->wszFnInit);
            if (PSLNULL == pfnExtInit)
            {
                PSLASSERT(!(L"Failed to GetProcAddress %s",
                                pExtInfo->wszFnInit));
                status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                goto Exit;
            }
#endif /* UNDER_CE */

            status = pfnExtInit(plt, dwSesState, pwCodes, cdwCodes);
            if (PSL_FAILED(status))
            {
                PSLASSERT(!(L"Init operation call failed handler: %s",
                                pExtInfo->wszSourceBinary));
                goto Exit;
            }
        }
    }
Exit:
    return status;
}

PSLSTATUS MTPExtensionUninitDeviceProps(PSLLOOKUPTABLE plt,
                                        PSLUINT32 dwSesState)
{
    PSLSTATUS status;
    PSLUINT32 cItems;
    PSLUINT32 dwIndex;
    EXTENSIONINFO* pExtInfo;
    PFNEXTENSIONUNINIT pfnExtUninit;
#ifndef UNDER_CE
    PSLUINT32 cchLen;
    LPSTR szFnUninit[MAX_PATH] = {0};
#endif /* UNDER_CE */

    if (PSLNULL == _PdlDeviceProps)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    status = PSLDynListGetSize(_PdlDeviceProps, &cItems);
    if (PSL_FAILED(status))
    {
        PSLASSERT(PSLFALSE);
        goto Exit;
    }
    for (dwIndex = 0; dwIndex < cItems; dwIndex++)
    {
        status = PSLDynListGetItem(_PdlDeviceProps, dwIndex,
                                   (PSLPARAM*)&pExtInfo);
        if(PSL_SUCCEEDED(status))
        {
            if (PSLNULL == pExtInfo->hModule)
            {
                pExtInfo->hModule = LoadLibrary(
                                          pExtInfo->wszSourceBinary);
                if (PSLNULL == pExtInfo->hModule)
                {
                    PSLASSERT(!(L"Failed to load library %d",
                                    pExtInfo->wszSourceBinary));
                    status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                    goto Exit;
                }
            }

#ifndef UNDER_CE
            status = PSLStringLen((PSLCWSTR)pExtInfo->wszFnUninit,
                                  MAX_PATH, &cchLen);
            if (PSL_FAILED(status))
            {
                PSLASSERT(!L"Lenth of Uninit function name \
                            cannot be more than MAX_PATH");
                goto Exit;
            }

            if ( 0 == WideCharToMultiByte(CP_ACP, 0,
                                          pExtInfo->wszFnUninit,
                                          cchLen, (LPSTR)szFnUninit,
                                          PSLARRAYSIZE(szFnUninit),
                                          NULL, NULL))
            {
                    status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                    goto Exit;
            }

            pfnExtUninit = (PFNEXTENSIONUNINIT)GetProcAddress(
                                                pExtInfo->hModule,
                                                (LPCSTR)szFnUninit);
            if (PSLNULL == pfnExtUninit)
            {
                PSLASSERT(!("Failed to GetProcAddress %s",
                                szFnUninit));
                status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                goto Exit;
            }
#else
            pfnExtUninit = (PFNEXTENSIONUNINIT)GetProcAddress(
                                                pExtInfo->hModule,
                                                pExtInfo->wszFnUninit);
            if (PSLNULL == pfnExtUninit)
            {
                PSLASSERT(!(L"Failed to GetProcAddress %s",
                                pExtInfo->wszFnUninit));
                status = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
                goto Exit;
            }
#endif /* UNDER_CE */

            status = pfnExtUninit(plt, dwSesState);
            if (PSL_FAILED(status))
            {
                PSLASSERT(!(L"Init operation call failed handler: %s",
                                pExtInfo->wszSourceBinary));
                goto Exit;
            }
        }
    }
Exit:
    return status;
}

PSLSTATUS MTPExtensionAddOpItem(EXTENSIONINFO* pExtInfo)
{
    PSLSTATUS status;

    if (PSLNULL == _PdlOperations)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    status = PSLDynListAddItem(_PdlOperations,
                               (PSLPARAM)pExtInfo, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}

PSLSTATUS MTPExtensionAddPropItem(EXTENSIONINFO* pExtInfo)
{
    PSLSTATUS status;

    if (PSLNULL == _PdlDeviceProps)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    status = PSLDynListAddItem(_PdlDeviceProps,
                               (PSLPARAM)pExtInfo, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}

PSLSTATUS MTPExtensionIsOpItemPresent(EXTENSIONINFO* pExtInfo)
{
    PSLSTATUS status;
    PSLUINT32 cExtensions;
    PSLUINT32 dwIndex;
    EXTENSIONINFO* pCompareOp;

    if (PSLNULL == _PdlOperations)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    /* If this is not a new extension, don't bother adding it to the list */
    status = PSLDynListGetSize(_PdlOperations, &cExtensions);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = PSLSUCCESS_FALSE;

    for (dwIndex = 0; dwIndex < cExtensions; dwIndex++)
    {
        status = PSLDynListGetItem(_PdlOperations, dwIndex,
                                (PSLPARAM*)&pCompareOp);
        if (PSL_SUCCEEDED(status) &&
            0 == PSLStringCmp((PSLCWSTR)pCompareOp->wszFnInit,
                              (PSLCWSTR)pExtInfo->wszFnInit) &&
            0 == PSLStringCmp((PSLCWSTR)pCompareOp->wszSourceBinary,
                              (PSLCWSTR)pExtInfo->wszSourceBinary) &&
            0 == PSLStringCmp((PSLCWSTR)pCompareOp->wszFnUninit,
                              (PSLCWSTR)pExtInfo->wszFnUninit))
        {
            status = PSLSUCCESS;
            goto Exit;
        }
    }

Exit:
    return status;
}

PSLSTATUS MTPExtensionIsPropItemPresent(EXTENSIONINFO* pExtInfo)
{
    PSLSTATUS status;
    PSLUINT32 cExtensions;
    PSLUINT32 dwIndex;
    EXTENSIONINFO* pCompareOp;

    if (PSLNULL == _PdlDeviceProps)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    /* If this is not a new extension, don't bother adding it to the list */
    status = PSLDynListGetSize(_PdlDeviceProps, &cExtensions);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = PSLSUCCESS_FALSE;

    for (dwIndex = 0; dwIndex < cExtensions; dwIndex++)
    {
        status = PSLDynListGetItem(_PdlDeviceProps, dwIndex,
                                (PSLPARAM*)&pCompareOp);
        if (PSL_SUCCEEDED(status) &&
            0 == PSLStringCmp((PSLCWSTR)pCompareOp->wszFnInit,
                              (PSLCWSTR)pExtInfo->wszFnInit) &&
            0 == PSLStringCmp((PSLCWSTR)pCompareOp->wszSourceBinary,
                              (PSLCWSTR)pExtInfo->wszSourceBinary) &&
            0 == PSLStringCmp((PSLCWSTR)pCompareOp->wszFnUninit,
                              (PSLCWSTR)pExtInfo->wszFnUninit))
        {
            status = PSLSUCCESS;
            goto Exit;
        }
    }

Exit:
    return status;
}
