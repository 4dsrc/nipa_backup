/*
 *  MTPHandlerPrecomp.h
 *
 *  Precompiled header file for command handler support.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPHANDLERPRECOMP_H_
#define _MTPHANDLERPRECOMP_H_

#include "PSLWindows.h"
#include "MTPPrecomp.h"

#include "PSLWinSafe.h"

#include <msxml2.h>

#endif  /* _MTPHANDLERPRECOMP_H_ */

