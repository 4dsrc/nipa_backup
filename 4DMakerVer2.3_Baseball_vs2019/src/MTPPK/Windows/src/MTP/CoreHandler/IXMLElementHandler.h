/*
 *  IXMLElementHandler.h
 *
 *
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */
#ifndef _IXMLELEMENTHANDLER_H_
#define _IXMLELEMENTHANDLER_H_

#define MAX_XML_NAMESPACE_LENGTH        2048
#define MAX_XML_PREFIX_LENGTH           512
#define MAX_XML_LOCALNAME_LENGTH        512
#define MAX_XML_CHARACTER_LENGTH        8192

#define MAX_XML_QNAME_LENGTH           (MAX_XML_PREFIX_LENGTH + 1 +    \
                                       MAX_XML_LOCALNAME_LENGTH)

#define IsValidStrLen(_cch, _cchMax)   ((0 <= _cch) && (_cch < _cchMax))

#define IsValidNamespaceLen(_cch)      IsValidStrLen(_cch,             \
                                               MAX_XML_NAMESPACE_LENGTH)

#define IsValidPrefixLen(_cch)         IsValidStrLen(_cch,             \
                                               MAX_XML_PREFIX_LENGTH)

#define IsValidLocalNameLen(_cch)      IsValidStrLen(_cch,             \
                                               MAX_XML_LOCALNAME_LENGTH)

#define IsValidQNameLen(_cch)          IsValidStrLen(_cch,             \
                                               MAX_XML_QNAME_LENGTH)

#define IsValidCharacterLen(_cch)      IsValidStrLen(_cch,             \
                                               MAX_XML_CHARACTER_LENGTH)

DEFINE_GUID(IID_IXMLElementHandler,
            0xa588f88b, 0x9be2, 0x4772, 0x97, 0xd4, \
            0x4f, 0xe2, 0x7, 0x12, 0xfc, 0x3e);

enum
{
    ELEMENT_HANDLER_UNDEFINED = 0,
    ELEMENT_HANDLER_DEVICEINFO,
    ELEMENT_HANDLER_COUNT
};

/* XML Element Handler */
class IXMLElementHandler : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE ElementStart(
                            LPCWSTR         szNamespace,
                            DWORD           cchNamespace,
                            LPCWSTR         szLocalName,
                            DWORD           cchLocalName,
                            ISAXAttributes* pAttributes) = 0;

    virtual HRESULT STDMETHODCALLTYPE ElementCharacters(
                            LPCWSTR         szChars,
                            DWORD           cchChars) = 0;

    virtual HRESULT STDMETHODCALLTYPE ElementEnd(
                            LPCWSTR         szNamespace,
                            DWORD           cchNamespace,
                            LPCWSTR         szLocalName,
                            DWORD           cchLocalName) = 0;

    virtual HRESULT STDMETHODCALLTYPE ElementTerminate() = 0;

    virtual HRESULT STDMETHODCALLTYPE ParseAttribute(
                            DWORD           idxAttr,
                            DWORD           dwToken,
                            LPCWSTR         szValue,
                            DWORD           cchValue) = 0;
};
#endif /*_IXMLELEMENTHANDLER_H_*/
