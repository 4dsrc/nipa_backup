/*
 *  MTPDeviceProperty.c
 *
 *  Contains definition for functions that support
 *  MTP device property operations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPHandlerPrecomp.h"
#include "MTPDevicePropertyContext.h"

static LPCWSTR _GetDevicePropRegValueName(PSLUINT16 wPropCode);

static PSLSTATUS _ReadRegistryValue(LPCWSTR szKeyName,
                    LPCWSTR szValueName,
                    PSLUINT32* pdwType,
                    PSLVOID* pvBuf,
                    PSLUINT32* pcbSize);

static PSLSTATUS _WriteRegistryValue(LPCWSTR szKeyName,
                    LPCWSTR szValueName,
                    PSLUINT32 dwType,
                    PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbSize);

static PSLSTATUS _DeleteRegistryValue(LPCWSTR szKeyName,
                    LPCWSTR szValueName);

static PSLSTATUS _MTPGetDevicePropDefaultValue(PSLUINT16 wPropCode,
                    PSLUINT16 wPropType,
                    PSLPARAM aContext,
                    PSLPARAM aContextProp,
                    PSLVOID* pBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbSize);

static PSLSTATUS _MTPSetDevicePropValue(PSLUINT16 wPropCode,
                    PSLUINT16 wPropType,
                    PSLPARAM aContext,
                    PSLPARAM aContextProp,
                    PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS PSL_API _MTPResetDeviceProp(PSLUINT16 wPropCode);

static PSLSTATUS PSL_API _MTPDevicePropContextOpen(PSLUINT32 dwPropCode,
                    PSLUINT32 dwDataFormat,
                    PSLPARAM aData,
                    MTPDEVICEPROPCONTEXT* pmdpc);

#define MTPPK_REGKEY_ROOT         L"Software\\Microsoft\\MTP PK"
#define MTPPK_REGVAL_DEVICENAME   L"FriendlyName"
#define MTPPK_REGVAL_SYNCPARTNER  L"SyncPartner"

static PSL_CONST MTPDEVICEPROPINFO  _RgdpiProps[] =
{
   {
        {
            PSLNULL,
            MTP_DEVICEPROPCODE_BATTERYLEVEL,
            MTP_PROPGETSET_GETONLY,
            _MTPGetDevicePropDefaultValue,
            _MTPGetDevicePropDefaultValue,
            PSLNULL,
            PSLNULL,
            0,
        },
        PSLNULL,
        PSLNULL
    },
    {
        {
            PSLNULL,
            MTP_DEVICEPROPCODE_DEVICEFRIENDLYNAME,
            MTP_PROPGETSET_GETSET,
            _MTPGetDevicePropDefaultValue,
            _MTPGetDevicePropDefaultValue,
            _MTPSetDevicePropValue,
            PSLNULL,
            0,
        },
        _MTPResetDeviceProp,
        PSLNULL
    },
    {
        {
            PSLNULL,
            MTP_DEVICEPROPCODE_SYNCHRONIZATIONPARTNER,
            MTP_PROPGETSET_GETSET,
            _MTPGetDevicePropDefaultValue,
            PSLNULL,
            _MTPSetDevicePropValue,
            PSLNULL,
            0,
        },
        _MTPResetDeviceProp,
        PSLNULL
    },
};


/*  _MTPDevicePropContextOpen
 *
 *  Callback function registered in the lookup table for creating a
 *  device property context
 *
 *  Arguments:
 *      PSLUINT32       dwPropCode          Property code
 *      PSLUINT32       dwDataFormat        Data format being requested
 *      PSLPARAM        aData               Data from the lookup table
 *      MTPDEVICEPROPCONTEXT*
 *                      pmdpc               Resulting context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDevicePropContextOpen(PSLUINT32 dwPropCode,
                    PSLUINT32 dwDataFormat, PSLPARAM aData,
                    MTPDEVICEPROPCONTEXT* pmdpc)
{
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pmdpc)
    {
        *pmdpc = PSLNULL;
    }

    /*  Validate Arguments
     */

    PSLASSERT((aData < PSLARRAYSIZE(_RgdpiProps)) &&
            ((PSLUINT16)dwPropCode == _RgdpiProps[aData].infoProp.wPropCode));
    if ((PSLARRAYSIZE(_RgdpiProps) <= aData) ||
        ((PSLUINT16)dwPropCode != _RgdpiProps[aData].infoProp.wPropCode) ||
        (PSLNULL == pmdpc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create the correct context
     */

    ps = MTPDevicePropContextCreate(dwDataFormat, &(_RgdpiProps[aData]),
                            PSLNULL, PSLNULL, pmdpc);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}

/*  MTPDevicePropInitialize
 *
 *  Initializes the list of supported device properties
 *
 *  Arguments:
 *      PSLLOOKUPTABLE  ltDevProps          Lookup table for device properties
 *      PCXPSLUINT16    pwCodes             Property codes from ServiceInfo
 *      PSLUINT32       cCodes              Number of properties in ServiceInfo
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDevicePropInitialize(PSLLOOKUPTABLE ltDevProps,
                    PCXPSLUINT16 pwCodes, PSLUINT32 cCodes)
{
    PSLUINT32       idxActive;
    PSLUINT32       idxSupported;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if ((PSLNULL == ltDevProps) || ((0 < cCodes) && (PSLNULL == pwCodes)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    for (idxActive = 0; idxActive < cCodes; idxActive++)
    {
        /*  Check for a match in the supported table
         */

        for (idxSupported = 0;
                (idxSupported < PSLARRAYSIZE(_RgdpiProps)) &&
                    (_RgdpiProps[idxSupported].infoProp.wPropCode !=
                            pwCodes[idxActive]);
                idxSupported++)
        {
        }

        /*  Skip it if no match found
         */

        if (PSLARRAYSIZE(_RgdpiProps) == idxSupported)
        {
            continue;
        }

        ps = PSLLookupTableInsertEntry(ltDevProps, pwCodes[idxActive],
                            (PSLPARAM)_MTPDevicePropContextOpen,
                            idxSupported);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    ps = PSLSUCCESS;

Exit:
    return ps;
}

/*  MTPDevicePropUninitialize
 *
 *  Uninitializes the device property support
 *
 *  Arguments:
 *      Nothing
 *
 *  Returns:
 *      PSLSUCCESS
 */

PSLSTATUS PSL_API MTPDevicePropUninititialize(PSLVOID)
{
    return PSLSUCCESS;
}


/* _MTPGetDevicePropDefaultValue
 *
 * This function retrieve default value of device property indicated
 * by the wPropCode
 *
 * Parameters:
 *      PSLUINT16 wPropCode         Property code
 *      PSLUINT16 wPropType         Property type
 *      PSLPARAM aContext           Overall context
 *      PSLPARAM aContextProp       Property context
 *      PSLVOID*  pBuf              buffer to return value
 *      PSLUINT32 cbBuf             buffer size
 *      PSLUINT32* pcbSize          number of byte written.
 *
 * Return:
 *
 *      PSLSUCCESS          on success
 *      PSLSUCCESS_FALSE    property value not handled yet
 *      Other               Failure.
 *
 */

PSLSTATUS _MTPGetDevicePropDefaultValue(PSLUINT16 wPropCode,
                    PSLUINT16 wPropType, PSLPARAM aContext,
                    PSLPARAM aContextProp, PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLUINT32* pcbSize)
{
    PSLSTATUS       ps;
    PSLUINT32       cbSize;
    PSLUINT32       dwType;

    PSL_CONST MTPPROPERTYREC*   precProp;

    if (MTP_DEVICEPROPCODE_ALL == wPropCode ||
        MTP_DEVICEPROPCODE_NOTUSED == wPropCode ||
        PSLNULL != aContext ||
        PSLNULL != aContextProp ||
        PSLNULL == pcbSize)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    switch(wPropCode)
    {
    case MTP_DEVICEPROPCODE_DEVICEFRIENDLYNAME:
    case MTP_DEVICEPROPCODE_SYNCHRONIZATIONPARTNER:
        /*  Must be a string
         */

        if (MTP_DATATYPE_STRING != wPropType)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        /*  Use the Identity subsystem to retrieve the device name
         */
        cbSize = (NULL != pvBuf) ? cbBuf : 0;
        ps = _ReadRegistryValue(MTPPK_REGKEY_ROOT,
                                _GetDevicePropRegValueName(wPropCode),
                                &dwType, pvBuf, &cbSize);
        if (PSLSUCCESS != ps)
        {
            /*  Retrieve the device property record
             */

            ps = MTPPropertyGetDefaultRec(wPropCode, PROPFLAGS_TYPE_DEVICE,
                            &precProp);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  And return the default value
             */

            ps = MTPPropertySerializeDefaultValue(precProp, pvBuf, cbBuf,
                            pcbSize);
            goto Exit;
        }
        break;

    case MTP_DEVICEPROPCODE_BATTERYLEVEL:
        {
            if (PSLNULL != pvBuf)
            {
                *((PSLUINT8*)pvBuf) = (PSLUINT8)100;
            }

            cbSize = sizeof(PSLUINT8);
        }
        break;

    default:
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    *pcbSize = cbSize;

    ps = PSLSUCCESS;

Exit:
    return ps;
}

/*  _MTPSetDevicePropValue
 *
 *  Callback function for setting a device property value
 *
 *  Arguments:
 *      PSLUINT16       wPropCode           Property code to store
 *      PSLUINT16       wPropType           Type for that property code
 *      PLSPARAM        aContext            Generic context
 *      PSLPARAM        aContextProp        Property specific context
 *      PSLVOID*        pvBuf               Source of data to set
 *      PSLUINT32       cbBuf               Number of bytes in buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used from buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCESS on success
 */

PSLSTATUS _MTPSetDevicePropValue(PSLUINT16 wPropCode, PSLUINT16 wPropType,
                    PSLPARAM aContext, PSLPARAM aContextProp,
                    PSL_CONST PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed)
{
    PSLUINT32       cbSize = 0;
    PSLSTATUS       ps;

    /*  Clear results for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((NULL == pvBuf) || (0 == cbBuf) || (NULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Handle the property received
     */

    switch (wPropCode)
    {
    case MTP_DEVICEPROPCODE_SYNCHRONIZATIONPARTNER:
    case MTP_DEVICEPROPCODE_DEVICEFRIENDLYNAME:
        /*  Validate the type
         */

        if (MTP_DATATYPE_STRING != wPropType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Determine how large the data in the buffer actually is
         */

        ps = MTPPropertyReadSize(wPropType, pvBuf, cbBuf, &cbSize);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Make sure we have enough data for the string
         */

        if (cbSize < cbBuf)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Stash the data in the registry as a binary blob
         */
        ps = _WriteRegistryValue(MTPPK_REGKEY_ROOT,
                                 _GetDevicePropRegValueName(wPropCode),
                                 REG_BINARY, pvBuf, cbSize);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTP_DEVICEPROPCODE_BATTERYLEVEL:
        ps = PSLERROR_ACCESS_DENIED;
        goto Exit;

    default:
        /*  Should not get here
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Return number of bytes consumed
     */

    *pcbUsed = cbSize;
    ps = PSLSUCCESS;

Exit:
    return ps;
    aContext;
    aContextProp;
}


/*  _MTPResetDeviceProp
 *
 *  Resets the device property to it's default state
 *
 *  Arguments:
 *      PSLUINT16       wPropCode           Property code to reset
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPResetDeviceProp(PSLUINT16 wPropCode)
{
    PSLSTATUS       ps;

    /*  Look for properties that we support
     */

    switch (wPropCode)
    {
    case MTP_DEVICEPROPCODE_SYNCHRONIZATIONPARTNER:
    case MTP_DEVICEPROPCODE_DEVICEFRIENDLYNAME:
        /*  Delete the registry key- this resets back to the default
         *  behavior
         */

        ps = _DeleteRegistryValue(MTPPK_REGKEY_ROOT,
                                  _GetDevicePropRegValueName(wPropCode));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTP_DEVICEPROPCODE_BATTERYLEVEL:
    case MTP_DEVICEPROPCODE_FUNCTIONALID:
        ps = PSLERROR_ACCESS_DENIED;
        goto Exit;

    default:
        /*  Should not get here
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    ps = PSLSUCCESS;

Exit:
    return ps;
}

LPCWSTR _GetDevicePropRegValueName(PSLUINT16 wPropCode)
{
    LPCWSTR szRegValue;

    switch(wPropCode)
    {
    case MTP_DEVICEPROPCODE_DEVICEFRIENDLYNAME:
        szRegValue = L"FriendlyName";
        break;

    case MTP_DEVICEPROPCODE_SYNCHRONIZATIONPARTNER:
        szRegValue = L"SyncPartner";
        break;

    default:
        szRegValue = NULL;
        PSLASSERT(PSLFALSE);
        break;
    }

    return szRegValue;
}

/*
 * _ReadRegistryValue
 *
 * Read a value from registry, given the key name and value name.
 *
 * Parameters:
 *      LPCWSTR     szKeyName           Key Name
 *      LPCWSTR     szValueName         Value Name
 *      PSLUINT32*  dwType              Registry type
 *      PSLVOID*    pvBuf               Buffer to return
 *      PSLUINT32*  pcbSize             [In/Out] in as the buffer size,
 *                                       out as written size
 *
 * Returns:
 *      PSLSUCCESS on success
 *
 */

PSLSTATUS _ReadRegistryValue(LPCWSTR szKeyName,
                             LPCWSTR szValueName,
                             PSLUINT32* pdwType,
                             PSLVOID* pvBuf,
                             PSLUINT32* pcbSize)
{
    HKEY            hkeyMTP = NULL;
    LRESULT         lResult;
    PSLSTATUS       ps;

    if (PSLNULL == pcbSize || ((NULL != pvBuf) && (0 == *pcbSize)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    lResult = RegCreateKeyEx(HKEY_CURRENT_USER, szKeyName, 0, NULL,
                             REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE,
                             NULL, &hkeyMTP, NULL);
    if (ERROR_SUCCESS != lResult)
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    lResult = RegQueryValueEx(hkeyMTP, szValueName, NULL, pdwType,
                              (LPBYTE)pvBuf, pcbSize);
    if (ERROR_SUCCESS != lResult)
    {
        ps = ((ERROR_FILE_NOT_FOUND == lResult) ||
                (ERROR_PATH_NOT_FOUND == lResult)) ?
                            PSLSUCCESS_NOT_FOUND : PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    ps = PSLSUCCESS;

Exit:
    SAFE_REGCLOSEKEY(hkeyMTP);
    return ps;
}

PSLSTATUS _WriteRegistryValue(LPCWSTR szKeyName,
                              LPCWSTR szValueName,
                              PSLUINT32 dwType,
                              PSL_CONST PSLVOID* pvBuf,
                              PSLUINT32 cbSize)
{
    HKEY            hkeyMTP = NULL;
    LRESULT         lResult;
    PSLSTATUS       ps;

    if (PSLNULL == pvBuf || 0 == cbSize)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    lResult = RegCreateKeyEx(HKEY_CURRENT_USER, szKeyName, 0, NULL,
                             REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS,
                             NULL, &hkeyMTP, NULL);
    if (ERROR_SUCCESS != lResult)
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    lResult = RegSetValueEx(hkeyMTP, szValueName, 0, dwType,
                            (BYTE*)pvBuf, cbSize);

    if (ERROR_SUCCESS != lResult)
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    ps = PSLSUCCESS;

Exit:
    SAFE_REGCLOSEKEY(hkeyMTP);
    return ps;
}

PSLSTATUS _DeleteRegistryValue(LPCWSTR szKeyName, LPCWSTR szValueName)
{
    DWORD           dwDisposition;
    HKEY            hkeyMTP = NULL;
    LONG            lResult;
    PSLSTATUS       ps;

    //  Validate arguments

    if ((NULL == szKeyName) || (NULL == szValueName))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    //  Make sure the key exists- creating it if it does not

    if (ERROR_SUCCESS != RegCreateKeyEx(HKEY_CURRENT_USER, szKeyName, 0, NULL,
                             REG_OPTION_NON_VOLATILE, KEY_SET_VALUE,
                             NULL, &hkeyMTP, &dwDisposition))
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    //  And delete the value

    lResult = RegDeleteValue(hkeyMTP, szValueName);
    if ((ERROR_SUCCESS != lResult) && (ERROR_PATH_NOT_FOUND != lResult) &&
        (ERROR_FILE_NOT_FOUND != lResult))
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }
    ps = PSLSUCCESS;

Exit:
    SAFE_REGCLOSEKEY(hkeyMTP);
    return ps;
}
