/*
 *  MTPHandlerDeviceInfo.cpp
 *
 *
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */
#include "MTPHandlerPrecomp.h"
#include "MTPDevice.h"

PSLSTATUS MTPDeviceInvokeCommand(
                            PSLUINT32 dwType,
                            PSLUINT32* pdwParams,
                            PSLUINT32 cParams
                            )
{
    PSLSTATUS status = PSLSUCCESS;

    switch (dwType)
    {
        case MTPDEVICECOMMAND_RESETDEVICE:
            /* Device specific clean up will happen here */
            status = PSLSUCCESS;
            break;

        case MTPDEVICECOMMAND_SELFTEST:
            if (PSLNULL == pdwParams || 1 != cParams )
            {
                status = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /* TODO: implement this. */
            status = PSLERROR_NOT_IMPLEMENTED;
            break;

        case MTPDEVICECOMMAND_POWERDOWN:
            /* TODO: implement this. */
            status = PSLERROR_NOT_IMPLEMENTED;
            break;

        case MTPDEVICECOMMAND_SKIP:
            if (PSLNULL == pdwParams || 1 != cParams )
            {
                status = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /* TODO: implement this. */
            status = PSLERROR_NOT_IMPLEMENTED;
            break;

        case MTPDEVICECOMMAND_INITIATECAPTURE:
            if (PSLNULL == pdwParams || 2 != cParams )
            {
                status = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /* TODO: implement this. */
            status = PSLERROR_NOT_IMPLEMENTED;
            break;

        case MTPDEVICECOMMAND_INITIATEOPENCAPTURE:
            if (PSLNULL == pdwParams || 2 != cParams )
            {
                status = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /* TODO: implement this. */
            status = PSLERROR_NOT_IMPLEMENTED;
            break;

        case MTPDEVICECOMMAND_TERMINATEOPENCAPTURE:
            if (PSLNULL == pdwParams || 1 != cParams )
            {
                status = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /* TODO: implement this. */
            status = PSLERROR_NOT_IMPLEMENTED;
            break;
    }

Exit:
    return status;
}

