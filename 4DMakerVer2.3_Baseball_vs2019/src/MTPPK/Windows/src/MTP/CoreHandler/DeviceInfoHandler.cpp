/*
 *  DeviceInfoHandler.cpp
 *
 *
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#define INITGUID

#include "MTPHandlerPrecomp.h"
#include "IXMLElementHandler.h"
#include "DeviceInfoHandler.h"
#include "MTPHandlerExtensions.h"
#include "MTPHandlerExtensionsUtil.h"

#define DATA_FIXED_SIZE     sizeof(WORD)+ sizeof(DWORD)+ \
                            sizeof(WORD)+ sizeof(WORD)

#define MTPSTRING_MAXLEN    255

typedef HRESULT (*PFNPARSEPROPERTY)(
                            CDeviceInfoHandler* pParser,
                            LPCWSTR             szChars,
                            DWORD               cchChars);

typedef struct DIPROPMAP
{
    INT                 nDSPropIndex;
    BOOL                bNests;
    LPCWSTR             pwszPropertyName;
    DWORD               cchPropertyName;
    PFNPARSEPROPERTY    pfnParseProperty;
}DIPROPMAP;

enum
{
    DS_PROP_UNDEFINED = 0,
    DS_PROP_STANDARD_VERSION,
    DS_PROP_VENDOR_EXTENSION_ID,
    DS_PROP_VENDOR_EXTENSION_VERSION,
    DS_PROP_VENDOR_EXTENSION_DESC,
    DS_PROP_FUNCTION_MODE,
    DS_PROP_OPERATIONS_SUPPORTED,
    DS_PROP_EVENTS_SUPPORTED,
    DS_PROP_DEVICE_PROP_SUPPORTED,
    DS_PROP_CAPTURE_FORMATS,
    DS_PROP_PLAYBACK_FORMATS,
    DS_PROP_MANUFACTURER,
    DS_PROP_MODEL,
    DS_PROP_DEVICE_VERSION,
    DS_PROP_SERIAL_NUMBER,
    DS_PROP_VALUE,
    DS_PROP_BASE,
    DS_PROP_EXTENDED,
    DS_PROP_COUNTS
};

static const WCHAR wszStandardVersion[]          = L"StandardVersion";
static const WCHAR wszVendorExtensionID[]        = L"VendorExtensionID";
static const WCHAR wszVendorExtensionVersion[]   = L"VendorExtensionVersion";
static const WCHAR wszVendorExtensionDesc[]      = L"VendorExtensionDesc";
static const WCHAR wszFunctionMode[]             = L"FunctionMode";
static const WCHAR wszOperationsSupported[]      = L"OperationsSupported";
static const WCHAR wszEventsSupported[]          = L"EventsSupported";
static const WCHAR wszDevicePropertiesSupported[]= L"DevicePropertiesSupported";
static const WCHAR wszCaptureFormats[]           = L"CaptureFormats";
static const WCHAR wszPlaybackFormats[]          = L"PlaybackFormats";
static const WCHAR wszManufacturer[]             = L"Manufacturer";
static const WCHAR wszModel[]                    = L"Model";
static const WCHAR wszDeviceVersion[]            = L"DeviceVersion";
static const WCHAR wszSerialNumber[]             = L"SerialNumber";
static const WCHAR wszBase[]                     = L"Base";
static const WCHAR wszValue[]                    = L"Value";
static const WCHAR wszExtended[]                 = L"Extended";

#define MAX_ATTRIBUTE_LEN 32

static const WCHAR wszEmpty[]                    = L"";
static const WCHAR wszSource[]                   = L"Source";
static const WCHAR wszInit[]                     = L"Init";
static const WCHAR wszUninit[]                   = L"Uninit";

static const DIPROPMAP _RgDIPropMap[] =
{
    {
        DS_PROP_UNDEFINED,
        FALSE,
        NULL,
        0,
        NULL,
    },
    {
        DS_PROP_STANDARD_VERSION,
        FALSE,
        wszStandardVersion,
        PSLARRAYSIZE(wszStandardVersion) - 1,
        CDeviceInfoHandler::OnWordValue,
    },
    {
        DS_PROP_VENDOR_EXTENSION_ID,
        FALSE,
        wszVendorExtensionID,
        PSLARRAYSIZE(wszVendorExtensionID)-1,
        CDeviceInfoHandler::OnDWordValue,
    },
    {
        DS_PROP_VENDOR_EXTENSION_VERSION,
        FALSE,
        wszVendorExtensionVersion,
        PSLARRAYSIZE(wszVendorExtensionVersion)-1,
        CDeviceInfoHandler::OnWordValue,
    },
    {
        DS_PROP_VENDOR_EXTENSION_DESC,
        FALSE,
        wszVendorExtensionDesc,
        PSLARRAYSIZE(wszVendorExtensionDesc)-1,
        CDeviceInfoHandler::OnStringValue,
    },
    {
        DS_PROP_FUNCTION_MODE,
        FALSE,
        wszFunctionMode,
        PSLARRAYSIZE(wszFunctionMode)-1,
        CDeviceInfoHandler::OnWordValue,
    },
    {
        DS_PROP_OPERATIONS_SUPPORTED,
        TRUE,
        wszOperationsSupported,
        PSLARRAYSIZE(wszOperationsSupported)-1,
        NULL,
    },
    {
        DS_PROP_EVENTS_SUPPORTED,
        TRUE,
        wszEventsSupported,
        PSLARRAYSIZE(wszEventsSupported)-1,
        NULL,
    },
    {
        DS_PROP_DEVICE_PROP_SUPPORTED,
        TRUE,
        wszDevicePropertiesSupported,
        PSLARRAYSIZE(wszDevicePropertiesSupported)-1,
        NULL,
    },
    {
        DS_PROP_CAPTURE_FORMATS,
        TRUE,
        wszCaptureFormats,
        PSLARRAYSIZE(wszCaptureFormats)-1,
        NULL,
    },
    {
        DS_PROP_PLAYBACK_FORMATS,
        TRUE,
        wszPlaybackFormats,
        PSLARRAYSIZE(wszPlaybackFormats)-1,
        NULL,
    },
    {
        DS_PROP_MANUFACTURER,
        FALSE,
        wszManufacturer,
        PSLARRAYSIZE(wszManufacturer)-1,
        CDeviceInfoHandler::OnStringValue,
    },
    {
        DS_PROP_MODEL,
        FALSE,
        wszModel,
        PSLARRAYSIZE(wszModel)-1,
        CDeviceInfoHandler::OnStringValue,
    },
    {
        DS_PROP_DEVICE_VERSION,
        FALSE,
        wszDeviceVersion,
        PSLARRAYSIZE(wszDeviceVersion)-1,
        CDeviceInfoHandler::OnStringValue,
    },
    {
        DS_PROP_SERIAL_NUMBER,
        FALSE,
        wszSerialNumber,
        PSLARRAYSIZE(wszSerialNumber)-1,
        CDeviceInfoHandler::OnStringValue,
    },
    {
        DS_PROP_VALUE,
        FALSE,
        wszValue,
        PSLARRAYSIZE(wszValue)-1,
        CDeviceInfoHandler::OnWordValue,
    },
    {
        DS_PROP_BASE,
        FALSE,
        wszBase,
        PSLARRAYSIZE(wszBase)-1,
        CDeviceInfoHandler::OnWordValue,
    },
    {
        DS_PROP_EXTENDED,
        FALSE,
        wszExtended,
        PSLARRAYSIZE(wszExtended)-1,
        CDeviceInfoHandler::OnWordValue,
    },

};

extern BYTE* g_pDeviceInfoBuf;
extern BYTE* g_pDeviceInfoCur;
extern DWORD g_cbDeviceInfoBuf;
extern DWORD g_cbDeviceInfoLeft;

CDeviceInfoHandler* CDeviceInfoHandler::m_pHandler = NULL;

CDeviceInfoHandler::CDeviceInfoHandler() :
    m_cRef(0),
    m_nPropInParsing(DS_PROP_UNDEFINED),
    m_nPropParent(DS_PROP_UNDEFINED),
    m_nNumValues(0),
    m_cCountOffset(0),
    m_pBuf(NULL),
    m_cbBuf(0),
    m_cbRequestedBuf(0),
    m_pCurBuf(NULL)
{
}

CDeviceInfoHandler::~CDeviceInfoHandler()
{
    Shutdown();
    m_pHandler = NULL;
}



/*
 *  CDeviceInfoHandler:ElementStart
 *
 *  Begins an element transaction
 *
 *  Arguments:
 *      LPCWSTR         szNamespace         Namespace for element
 *      DWORD           cchNamespace        Length of the namespace
 *      LPCWSTR         szLocalName         Name of the element being started
 *      DWORD           cchLocalName        Length of the name
 *      ISAXAttributes* pAttributes         Attributes associated with element
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CDeviceInfoHandler::ElementStart(LPCWSTR szNamespace,
                                         DWORD   cchNamespace,
                                         LPCWSTR szLocalName,
                                         DWORD   cchLocalName,
                                         ISAXAttributes* pAttributes)
{
    HRESULT hr;
    DWORD i;
    DWORD nLen;

    /* Validate arguments that we are using here */
    if ((NULL == szLocalName) || !IsValidLocalNameLen(cchLocalName))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    if ((NULL != szNamespace) || (0 != cchNamespace))
    {
        /* do nothing*/
    }

    nLen = PSLARRAYSIZE(_RgDIPropMap);
    for (i = 0; i < nLen; i++)
    {
        if ((cchLocalName == _RgDIPropMap[i].cchPropertyName) &&
            (0 == _wcsnicmp(szLocalName, _RgDIPropMap[i].pwszPropertyName,
            cchLocalName)))
        {
            if (_RgDIPropMap[i].bNests)
            {
                ASSERT(0 == m_nNumValues);
                ASSERT(0 == m_cCountOffset);

                /* Add space for 'count' dword at head of upcoming list */
                hr = ResizeBuffer(0, 0, sizeof(DWORD));
                if (FAILED(hr))
                {
                    goto Exit;
                }

                /*  Save the offset to the header for the following list */
                m_cCountOffset = m_pCurBuf - m_pBuf;
                m_pCurBuf += sizeof(DWORD);
            }

            if (m_nPropInParsing != DS_PROP_UNDEFINED)
            {
                m_nPropParent = m_nPropInParsing;
            }
            m_nPropInParsing = _RgDIPropMap[i].nDSPropIndex;

            if (DS_PROP_EXTENDED == m_nPropInParsing)
            {
                hr = ParseExtendedAttributes(m_nPropParent, pAttributes);
            }

            break;
        }
    }
    hr = S_OK;
Exit:
    return hr;
}

HRESULT CDeviceInfoHandler::ElementCharacters(LPCWSTR szChars, DWORD cchChars)
{
    HRESULT hr;
    DWORD i, nLen;

    /* Validate arguments that we are using here */
    if ((NULL == szChars) || !IsValidLocalNameLen(cchChars))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    if (DS_PROP_UNDEFINED == m_nPropInParsing)
    {
        hr = S_OK;
        goto Exit;
    }

    nLen = PSLARRAYSIZE(_RgDIPropMap);
    for (i = 0; i < nLen; i++)
    {
        if (m_nPropInParsing == _RgDIPropMap[i].nDSPropIndex)
        {
            if (NULL != _RgDIPropMap[i].pfnParseProperty)
            {
                hr = _RgDIPropMap[i].pfnParseProperty(this,
                                                      szChars,
                                                      cchChars);
                goto Exit;
            }
            else
            {
                hr = S_OK;
                goto Exit;
            }
        }
    }
    hr = S_OK;
 Exit:
    return hr;
}

HRESULT CDeviceInfoHandler::ElementEnd(LPCWSTR szNamespace, DWORD cchNamespace,
                                       LPCWSTR szLocalName, DWORD cchLocalName)
{
    HRESULT hr;
    DWORD i, nLen;

    /* Validate arguments that we are using here */
    if ((NULL == szLocalName) || !IsValidLocalNameLen(cchLocalName))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    if ((NULL == szNamespace) || (0 != cchNamespace))
    {
        /* do nothing*/
    }

    nLen = PSLARRAYSIZE(_RgDIPropMap);
    for (i = 0; i < nLen; i++)
    {
        if ((cchLocalName == _RgDIPropMap[i].cchPropertyName) &&
            (0 == _wcsnicmp(szLocalName, _RgDIPropMap[i].pwszPropertyName,
                                                                cchLocalName)))
        {
            if (m_nPropParent != DS_PROP_UNDEFINED)
            {
                m_nPropInParsing = m_nPropParent;
                m_nPropParent = DS_PROP_UNDEFINED;
            }
            else
            {
                m_nPropInParsing = DS_PROP_UNDEFINED;
            }

            if (_RgDIPropMap[i].bNests)
            {
                /* closing a listed tag */
                ASSERT(0 != m_cCountOffset);

                memcpy(m_pBuf + m_cCountOffset, &m_nNumValues, sizeof(DWORD));

                m_nNumValues = 0;
                m_cCountOffset = 0;
            }

            break;
        }
    }

    hr = S_OK;

Exit:
    return S_OK;
}

HRESULT CDeviceInfoHandler::ElementTerminate()
{
    return S_OK;
}

HRESULT CDeviceInfoHandler::ParseAttribute(
                            DWORD idxAttr,
                            DWORD dwToken,
                            LPCWSTR szValue,
                            DWORD cchValue)
{
    if (idxAttr != 0 || dwToken != 0 || NULL != szValue || cchValue != 0)
    {

    }
    return S_OK;
}

HRESULT CDeviceInfoHandler::OnDWordValue(
                        CDeviceInfoHandler* pParser,
                        LPCWSTR        szLocalName,
                        DWORD          cchLocalName)
{
    HRESULT hr;
    DWORD dwValue;

    /* Validate arguments */
    hr = ValidateArguments(pParser, szLocalName, cchLocalName);
    if (FAILED(hr))
    {
        goto Exit;
    }

    dwValue = (DWORD)wcstol(szLocalName, NULL, 0);

    hr = pParser->ResizeBuffer(1, sizeof(DWORD), 0);
    if (FAILED(hr))
    {
        goto Exit;
    }

    MTPStoreUInt32((PXPSLUINT32)pParser->m_pCurBuf, dwValue);
    pParser->m_pCurBuf += sizeof(DWORD);

    if (DS_PROP_UNDEFINED != pParser->m_nPropParent)
    {
        pParser->m_nNumValues++;
    }

Exit:
    return hr;
}

HRESULT CDeviceInfoHandler::OnWordValue(
                        CDeviceInfoHandler* pParser,
                        LPCWSTR        szLocalName,
                        DWORD          cchLocalName)
{
    HRESULT hr;
    WORD wValue;

    /* Validate arguments */
    hr = ValidateArguments(pParser, szLocalName, cchLocalName);
    if (FAILED(hr))
    {
        goto Exit;
    }

    wValue = (WORD)wcstol(szLocalName, NULL, 0);

    hr = pParser->ResizeBuffer(1, sizeof(WORD), 0);
    if (FAILED(hr))
    {
        goto Exit;
    }

    MTPStoreUInt16((PXPSLUINT16)pParser->m_pCurBuf, wValue);
    pParser->m_pCurBuf += sizeof(WORD);

    if (DS_PROP_UNDEFINED != pParser->m_nPropParent)
    {
        pParser->m_nNumValues++;
    }

    hr = S_OK;
Exit:
    return hr;
}

HRESULT CDeviceInfoHandler::OnStringValue(
                        CDeviceInfoHandler* pParser,
                        LPCWSTR        szLocalName,
                        DWORD          cchLocalName)
{
    HRESULT hr;

    /* Validate arguments */
    hr = ValidateArguments(pParser, szLocalName, cchLocalName);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = pParser->PackString(szLocalName, cchLocalName);
    if (FAILED(hr))
    {
        goto Exit;
    }

Exit:
    return hr;
}


HRESULT CDeviceInfoHandler::ParseAttribute(ISAXAttributes* pAttributes,
                                           const WCHAR* wszName,
                                           int cchName, PSLBYTE** ppBuf)
{
    HRESULT hr;

    int idx, cchAttrib;
    const WCHAR* wszAttrib;


    hr = pAttributes->getIndexFromName(wszEmpty, 0, wszName, cchName, &idx);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = pAttributes->getValue(idx, &wszAttrib, &cchAttrib);
    if (FAILED(hr))
    {
        goto Exit;
    }

    PSLCopyMemory(*ppBuf, wszAttrib, cchAttrib * sizeof(WCHAR));
    *ppBuf += cchAttrib * sizeof(WCHAR);

    *((WCHAR*)(*ppBuf)) = 0;
    *ppBuf += sizeof(WCHAR);

Exit:
    return hr;
}

HRESULT CDeviceInfoHandler::ParseExtendedAttributes(DWORD dwPropType,
                                                    ISAXAttributes* pAttributes)
{
    HRESULT hr;
    PSLSTATUS ps = PSLSUCCESS;
    EXTENSIONINFO* pNewOp = NULL;
    PSLBYTE* pBuf = NULL;

    int cAttribs = 0;
    int cchAttrib = 0;
    int cchAttribs = 0;
    const WCHAR* wszAttrib;

    ASSERT(NULL != pAttributes);
    ASSERT(DS_PROP_OPERATIONS_SUPPORTED == dwPropType ||
                DS_PROP_DEVICE_PROP_SUPPORTED == dwPropType);

    /* Find the length of all the attributes */
    hr = pAttributes->getLength(&cAttribs);
    if(FAILED(hr))
    {
        goto Exit;
    }
    for (int i=0; i < cAttribs; i++)
    {
        pAttributes->getValue(i, &wszAttrib, &cchAttrib);
        if (SUCCEEDED(hr))
        {
            cchAttribs += cchAttrib;
        }
        else
        {
            goto Exit;
        }
    }

    /* Allocate space for struct plus strings */
    pNewOp = (EXTENSIONINFO*)PSLMemAlloc(PSLMEM_PTR,
                                     sizeof(EXTENSIONINFO) +
                                     (cchAttribs + cAttribs) * sizeof(WCHAR));
    if (NULL == pNewOp)
    {
        hr = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    pBuf = (PSLBYTE*)pNewOp + sizeof(EXTENSIONINFO);

    pNewOp->wszSourceBinary = (WCHAR*)pBuf;
    hr = ParseAttribute(pAttributes, wszSource, PSLARRAYSIZE(wszSource) - 1,
                        &pBuf);
    if(FAILED(hr))
    {
        goto Exit;
    }

    pNewOp->wszFnInit = (WCHAR*)pBuf;
    hr = ParseAttribute(pAttributes, wszInit, PSLARRAYSIZE(wszInit) - 1,
                        &pBuf);
    if(FAILED(hr))
    {
        goto Exit;
    }

    pNewOp->wszFnUninit= (WCHAR*)pBuf;
    hr = ParseAttribute(pAttributes, wszUninit, PSLARRAYSIZE(wszUninit) - 1,
                        &pBuf);
    if(FAILED(hr))
    {
        goto Exit;
    }

    ASSERT(pBuf == (PSLBYTE*)pNewOp + sizeof(EXTENSIONINFO) +
                   (cchAttribs + cAttribs) * sizeof(WCHAR));

    if (DS_PROP_OPERATIONS_SUPPORTED == dwPropType)
    {
        /* If this is not a new extension,
         * don't bother adding it to the list
         */
        ps = MTPExtensionIsOpItemPresent(pNewOp);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        if (PSLSUCCESS_FALSE == ps)
        {
            ps = MTPExtensionAddOpItem(pNewOp);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            pNewOp = PSLNULL;
        }
    }
    else if (DS_PROP_DEVICE_PROP_SUPPORTED == dwPropType)
    {
        /* If this is not a new extension,
         * don't bother adding it to the list
         */
        ps = MTPExtensionIsPropItemPresent(pNewOp);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        if (PSLSUCCESS_FALSE == ps)
        {
            ps = MTPExtensionAddPropItem(pNewOp);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            pNewOp = PSLNULL;
        }
    }
    else
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

Exit:
    SAFE_PSLMEMFREE(pNewOp);
    hr = ps;
    return hr;
}

HRESULT CDeviceInfoHandler::ValidateArguments(
                                            CDeviceInfoHandler* pParser,
                                            LPCWSTR        szLocalName,
                                            DWORD          cchLocalName)
{
    HRESULT hr;

    /* Validate arguments */
    if ((NULL == szLocalName) || !IsValidLocalNameLen(cchLocalName) ||
        (NULL == pParser))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }
    hr = S_OK;

Exit:
    return hr;
}

HRESULT CDeviceInfoHandler::Init()
{
    HRESULT hr;

    m_pBuf = NEW BYTE[MTPSTRING_MAXLEN];
    if (NULL == m_pBuf)
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    m_cbBuf = MTPSTRING_MAXLEN;
    m_cbRequestedBuf = 0;
    m_pCurBuf = m_pBuf;

    hr = S_OK;
Exit:
    return hr;
}

HRESULT CDeviceInfoHandler::Shutdown()
{
    g_pDeviceInfoBuf    =   m_pBuf;
    g_pDeviceInfoCur    =   m_pBuf;
    g_cbDeviceInfoBuf   =   m_cbRequestedBuf;
    g_cbDeviceInfoLeft  =   m_cbRequestedBuf;

    m_pBuf              =   NULL;
    m_cbBuf             =   0;

    return S_OK;
}

HRESULT CDeviceInfoHandler::PackString(LPCWSTR szLocalName, DWORD cchLocalName)
{
    HRESULT hr;
    DWORD dwCount;

    if (NULL == szLocalName)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    /* account for terminating NULL char */
    dwCount = cchLocalName + 1;

    /* mtp strings cannot me more than MTPSTRING_MAXLEN chars */
    if (MTPSTRING_MAXLEN <= cchLocalName)
    {
        hr = E_FAIL;
        goto Exit;
    }

    /* reallocate the buffer based on the size of the string */
    hr = ResizeBuffer(dwCount, sizeof(WCHAR), sizeof(BYTE));
    if (FAILED(hr))
    {
        goto Exit;
    }

    /* copy over the string */
    if (PSL_FAILED(MTPStoreMTPString((PXMTPSTRING)m_pCurBuf,
                            ((dwCount * sizeof(WCHAR)) + 1),
                            &dwCount, (PSLCWSTR)szLocalName,
                            cchLocalName)))
    {
        hr = E_FAIL;
        goto Exit;
    }
    m_pCurBuf += dwCount;

Exit:
    return hr;
}

HRESULT CDeviceInfoHandler::ResizeBuffer(DWORD dwCount, DWORD dwSize,
                                         DWORD dwHeaderSize)
{
    HRESULT hr;
    BYTE* pBuf = NULL;
    DWORD dwAppendSize = 0;
    DWORD dwBytesWritten = 0;

    if ((NULL != m_pCurBuf) && (NULL != m_pBuf))
    {
        dwBytesWritten = (DWORD)(m_pCurBuf - m_pBuf);
        ASSERT(dwBytesWritten <= m_cbBuf);
    }

    dwAppendSize = (dwCount * dwSize) + dwHeaderSize;

    m_cbRequestedBuf += dwAppendSize;

    if (m_cbRequestedBuf > m_cbBuf)
    {

        pBuf = NEW BYTE[max(2 * m_cbBuf, m_cbRequestedBuf)];
        if (NULL == pBuf)
        {
            hr = E_OUTOFMEMORY;
            goto Exit;
        }

        if (NULL != m_pBuf)
        {
            memcpy(pBuf, m_pBuf, m_cbBuf);
        }
        SAFE_ARRAYDELETE(m_pBuf);

        m_pBuf = pBuf;
        m_pCurBuf = m_pBuf + dwBytesWritten;
        m_cbBuf = max(2 * m_cbBuf, m_cbRequestedBuf);
    }

    pBuf = NULL;

    hr = S_OK;
Exit:
    SAFE_ARRAYDELETE(pBuf);
    return hr;
}

HRESULT CDeviceInfoHandler::CreateInstance(IXMLElementHandler** ppHandler)
{
    HRESULT             hr;
    CDeviceInfoHandler* pHandler = NULL;

    /* Clear the result for safety */
    if (NULL != ppHandler)
    {
        *ppHandler = NULL;
    }

    /* Validate Return Argument */
    if (NULL == ppHandler)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    /*
     * If we already have an instance of
     * this class just add ref and return
     */
    if (NULL != m_pHandler)
    {
        /* Use the existing instance */
        pHandler = m_pHandler;
    }
    else
    {
        /* Create a new instance to return */
        pHandler = NEW CDeviceInfoHandler();
        if (NULL == pHandler)
        {
            hr = E_OUTOFMEMORY;
            goto Exit;
        }
    }

    SAFE_ADDREF(pHandler);

    hr = pHandler->Init();
    if (FAILED(hr))
    {
        goto Exit;
    }

    *ppHandler = pHandler;
    pHandler = NULL;
Exit:
    SAFE_RELEASE(pHandler);
    return hr;
}

HRESULT CDeviceInfoHandler_CreateInstance(IXMLElementHandler** ppHandler)
{
    return CDeviceInfoHandler::CreateInstance(ppHandler);
}
