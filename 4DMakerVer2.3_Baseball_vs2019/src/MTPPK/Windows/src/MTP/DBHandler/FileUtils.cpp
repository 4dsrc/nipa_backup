/*
 *  FileUtils.cpp
 *
 *  Contains definitions of the file utility functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPDBHandlerPrecomp.h"

// Handy macro to remove front slash, if present
#define REMOVE_FRONT_SLASH(str) ((str[0] == TEXT('\\')) ? &str[1] : &str[0])


#ifndef CF_CARDS_FLAGS
#define CF_CARDS_FLAGS (FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_TEMPORARY)
#endif

static TCHAR const c_szDatapathKey[] = TEXT("System\\Platform");
static TCHAR const c_szDataPath[] = TEXT("DataPath");
static TCHAR const c_szDefDataDir[] = TEXT("\\");

#if !defined(AFS_ROOTNUM_NETWORK)
#define AFS_ROOTNUM_NETWORK     1
#endif

#if !defined(OIDFROMAFS)
#define OIDFROMAFS(iAFS)        ((CEOID)((SYSTEM_MNTVOLUME<<28)|((iAFS)&0xfffffff)))
#endif

#ifndef SYSTEM_MNTVOLUME
#define SYSTEM_MNTVOLUME         0xe
#else
    ASSERT( SYSTEM_MNTVOLUME == 0xe );
#endif

const TCHAR c_szReleaseDir[] = TEXT("Release");


LONG Reg_GetStrFromHKLM(LPCTSTR pszKey, LPCTSTR pszValueName, LPTSTR pszValue, ULONG *pcbValue)
{
    LONG lResult;
    HKEY hkey = NULL;

    lResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE, pszKey, 0,
                           KEY_ALL_ACCESS, &hkey);

    if (lResult == ERROR_SUCCESS)
    {
        DWORD dwType = 0;
        lResult = RegQueryValueEx(hkey, pszValueName, NULL,
                                  &dwType, (LPBYTE)pszValue, pcbValue);
        RegCloseKey(hkey);
    }

    return lResult;
}

DWORD GetDataDirectory
(
  __out_ecount(cchSize) LPTSTR pszBuffer,  // buffer for Data directory
  DWORD cchSize     // size of the pszBuffer string buffer in number of characters
)
{
    // Get the size of buffer in bytes (Reg_GetStrFromHKLM requires size in byte)
    ULONG cbSize = cchSize * sizeof(TCHAR);
    ULONG cchOut = 0;             // return value

    LONG lResult = Reg_GetStrFromHKLM(c_szDatapathKey, c_szDataPath, pszBuffer, &cbSize);

    // If we succeeded then return the number of characters copied.
    // If the buffer wasn't large enough then return the size of buffer (in number of characters) required
    if ((lResult == ERROR_MORE_DATA) ||
        (lResult == ERROR_SUCCESS))
    {
        // Return the number of characters copied (or the number of characters required)
        cchOut =  cbSize / sizeof(TCHAR);
        pszBuffer[cchSize - 1] = '\0';

        //
        //  Check to see if '\\' is at the end, if not, append one
        //  Only do so if there is actually space for the '\\' to fit
        //
        if  ((2 <= cchOut) && (cchOut < cchSize - 1) &&
             (pszBuffer[cchOut-2] != '\\'))
        {
            StringCchCat(pszBuffer, cchSize, TEXT("\\"));
            cchOut++;
        }
    }
    else        // copy the default directory then.
    {
        // Copy c_szDefDataDir to pszBuffer (ccSize is the buffer size)
        StringCchCopy(pszBuffer, cchSize, c_szDefDataDir);

        // Return the number of characters copied (or the number of characters required)
        cchOut = PSLARRAYSIZE(c_szDefDataDir);
    }

    return cchOut;
}

//  *************************************************************************
//  IsCompactFlashPath
//
//  Purpose:
//      Helper function to determine if the specified directory is a
//      removable storage card.
//
//  Details:
//      This helper function is created to abstract the details for what
//      determines that a directory is in fact removable storage. Prior
//      implementations depended on the file attributes to distinguish
//      removable storage, but this is not sufficient since the mount
//      directory for persistent storage has the same file attributes yet
//      it is not removable.
//
//  Parameters:
//      lpszFileName     [in]    a directory file name
//      dwFileAttributes [in]    the directory's file attributes
//
//  Returns:
//      TRUE    if the directory is a removable storage card
//      FALSE   if the directory is fixed (i.e., in RAM or persistent storage)
//
//  Side Effects:
//      none
//  *************************************************************************
BOOL IsCompactFlashPath(LPCTSTR lpszFileName, DWORD dwFileAttributes)
{
#ifdef _UNDER_CE
    BOOL bRet = FALSE;

    //
    // A storage card should not have invalid attributes.
    //
    if ((DWORD)-1 == dwFileAttributes)
        return FALSE;

#ifndef TARGET_NT

#ifdef DEBUG
    // for debugging
    // we'll allow the name CF* to be detected as a flash card.
    static const TCHAR szDebugStorageDir[] = TEXT("CF");

    if ((dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && ! _tcsncmp(lpszFileName, szDebugStorageDir, _tcslen(szDebugStorageDir)))
    {
        bRet = TRUE;
        goto Error;
    }
#endif

    // Is this a temporary directory?
    if ((dwFileAttributes & CF_CARDS_FLAGS) != CF_CARDS_FLAGS)
        goto Error;

    // Is this a known temp dir that is not flash?
    // E.g.: Persistent storage data directory, network and release directory
    TCHAR   szDataDir[MAX_PATH];
    LPCTSTR pszDataAfterSlash;
    LPCTSTR pszAfterSlash;
    LPCTSTR pszNetworksDir;
    CEOIDINFO ceOIDInfo;

    pszAfterSlash = REMOVE_FRONT_SLASH(lpszFileName);

    GetDataDirectory(szDataDir, PSLARRAYSIZE(szDataDir));
    pszDataAfterSlash = REMOVE_FRONT_SLASH(szDataDir);

#if !defined(SHIP_BUILD) || defined(x86)

    if(!_tcsicmp(pszAfterSlash, c_szReleaseDir))
        goto Error;

#endif // !defined(SHIP_BUILD) || defined(x86)

    //get the localized network path
    if(CeOidGetInfo(OIDFROMAFS(AFS_ROOTNUM_NETWORK),
                    &ceOIDInfo))
    {
        pszNetworksDir =
            REMOVE_FRONT_SLASH(ceOIDInfo.infDirectory.szDirName);

        // Make sure it's not the network dir
        if(!_tcsicmp(pszAfterSlash, pszNetworksDir))
            goto Error;
    }


    if(!_tcsicmp(pszAfterSlash, pszDataAfterSlash))
        goto Error;

    bRet = TRUE;
#else
    // This portion (TARGET_NT) is defined in many versions of IsCompactFlash, so copied
    // to main one here

    // under NT search for "Storage Card" dir
    static const TCHAR szDebugStorageDir[] = TEXT("Storage Card");

    bRet =  ((dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
        !_tcsncmp(dwFileAttributes, szDebugStorageDir, _tcslen(szDebugStorageDir)));
#endif // TARGET_NT

Error:
    return bRet;
#else
    lpszFileName;
    dwFileAttributes;
    return FALSE;
#endif /*_UNDER_CE*/
}


/////////////////////////////////////////////
//
// Is this file on the compact flash card?
//
//

BOOL IsCompactFlash(WIN32_FIND_DATA  *lpfdCF)
{
    //is it a storage card ?
    if ((lpfdCF->dwFileAttributes & CF_CARDS_FLAGS) == CF_CARDS_FLAGS)
        return TRUE;

    return FALSE;
}


///////////////////////////////////////////////////////////////////////
// IsRemovable
//
//  Returns TRUE if the path specified is on removable media.
//


BOOL IsRemovable(LPCWSTR wszPath)
{
    DWORD           cch;
    DWORD           dwFileAttributes;
    BOOL            fResult;
    HRESULT         hr;
    WCHAR           rgwchPath[MAX_PATH];
    LPCWSTR         szNextFolder;

    // We assume that the path is valid- which means it needs to start with
    //  a '\'

    if (L'\\' != *wszPath)
    {
        fResult = FALSE;
        goto Exit;
    }

    // Check the path for the next delimter

    szNextFolder = wcschr(&(wszPath[1]), L'\\');
    if (NULL != szNextFolder)
    {
        cch = szNextFolder - wszPath;
    }
    else
    {
        cch = MAX_PATH;
    }

    // Make sure the path is valid

    hr = StringCchCopyN(rgwchPath, PSLARRAYSIZE(rgwchPath), wszPath, cch);
    if (FAILED(hr))
    {
        // We were handed a file that was too long- report failure

        PSLASSERT(FALSE);
        fResult = FALSE;
        goto Exit;
    }

    // Retrieve the attributes for this file

    dwFileAttributes = GetFileAttributes(rgwchPath);
    if (INVALID_FILE_ATTRIBUTES == dwFileAttributes)
    {
        // File does not exist- report that it is not removable

        fResult = FALSE;
        goto Exit;
    }

    // And check to see if this is on a removable storage

    fResult = IsCompactFlashPath(rgwchPath, dwFileAttributes);

Exit:
    return fResult;
}

//////////////////////////////////////////////////////////////////////////////
// FindFileName
//
//  Returns a pointer to the file name in the path
//

LPCWSTR FindFileName(LPCWSTR szPath)
{
    LPCWSTR         szResult;

    // Look for a path delimeter starting from the end

    szResult = wcsrchr(szPath, L'\\');
    if (NULL == szResult)
    {
        // If we did not find a path delimeter return the whole string

        szResult = szPath;
    }
    else
    {
        szResult++;
        if (L'\0' == *szResult)
        {
            // If we had no filename return NULL

            szResult = NULL;
        }
    }
    return szResult;
}


//////////////////////////////////////////////////////////////////////////////
// FindFileExtension
//
//  Returns a pointer to the file extension in the file name.
//

LPCWSTR FindFileExtension(LPCWSTR szPath)
{
    LPCWSTR         szResult;
    const WCHAR*    pchCur;

    // Look for a period starting at the end of the string

    szResult = wcsrchr(szPath, L'.');
    if (NULL == szResult)
    {
        // Did not find a period- cannot have an extension

        goto Exit;
    }

    // Walk from the location of the period forward to make sure that the
    //  period is not in an earlier folder

    for (pchCur = szResult; L'\0' != *pchCur; pchCur++)
    {
        // If we encounter a '\' this is not at the leaf of the file system

        if (L'\\' == *pchCur)
        {
            // Report that we did not find an extension

            szResult = NULL;
            goto Exit;
        }
    }

    // All is well return the extension that we located

Exit:
    return szResult;
}

