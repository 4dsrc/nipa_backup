/*
 *  MTPDatabase.cpp
 *
 *  Contains definition of the MTP database API functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPDBHandlerPrecomp.h"
#include "MTPDatabaseUtil.h"
#include "PSLWinSafe.h"
#include "PSLDynamicList.h"
#include "PSLUnknown.h"
#include "MTPDataStore.h"
#include "DBContextWin.h"
#include "DBContextDataWin.h"
#include "MTPEventManager.h"
#include "MTPLegacyService.h"


//*****************************************************************************
//
// This function is used to open or start a DB session
//
//*****************************************************************************

PSL_EXTERN_C PSLSTATUS PSL_API MTPLegacyServiceInit(MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwServiceID, PSLUINT32 dwStorageID,
                    PSLUINT32 *pcServices)
{
    PSLSTATUS   ps;
    DBSession*  pdbSession = NULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pcServices)
    {
        *pcServices = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pcServices))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create a new DB Session
     */

    ps = DBSession::Create(&pdbSession);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And register it
     */

    ps = MTPServiceRegisterLegacyService(pmtpctx, (MTPDBSESSIONOBJ*)pdbSession);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pdbSession = NULL;

Exit:
    SAFE_RELEASE(pdbSession);
    return ps;

    dwServiceID;
    dwStorageID;
}

PSL_EXTERN_C PSLSTATUS PSL_API  MTPDBSessionGetStorageId(MTPDBSESSION hDBSession,
                                                         PSLUINT32 dwObjectHandle,
                                                         PSLUINT32* pdwStorageID)
{
    PSLSTATUS  ps;
    DBSession* pDBSession = NULL;

    if (PSLNULL != pdwStorageID)
    {
        *pdwStorageID = MTP_STORAGEID_UNDEFINED;
    }

    if (PSLNULL == hDBSession)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pDBSession = (DBSession*)((MTPDBSESSIONOBJ*)hDBSession);
    ps = pDBSession->ResolveObjectHandleToStorageId(dwObjectHandle, pdwStorageID);

Exit:
    return ps;
}



//*****************************************************************************
//
// MTPDBSESSIONVTBL _VtblDBSession
//
//*****************************************************************************

/*
 * This function is used to close a DB session.
 * It will be called from CloseSession handler.
 *
 *  Arguments:
 *      MTPDBSESSION   hDBSession         DB Session handle to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */
PSLSTATUS _MTPDBCloseSession(MTPDBSESSION hDBSession)
{
    PSLSTATUS status;

    if (PSLNULL == hDBSession )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    DBSession* pDBSession = (DBSession*)((MTPDBSESSIONOBJ*)hDBSession);
    status = pDBSession->Cleanup();

    SAFE_RELEASE( pDBSession );

Exit:
    return status;
}

/*
 * This function is used for abnormal shutting down.
 *
 *  Arguments:
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */
//PSL_EXTERN_C PSLSTATUS PSL_API MTPDBTerminate(MTPDBCONTEXT hDBContext,
//                                      MTPDBCONTEXTDATA hDBContextData)
//{
//    PSLSTATUS status;
//
//    DBContext* pDBContext;
//    DBContextData* pDBContextData;
//
//    // if no DBContext present, there's nothing to cancel
//
//    pDBContext = (DBContext*)hDBContext;
//    PSLASSERT( pDBContext != PSLNULL );
//
//    status = PSLSUCCESS;
//
//    // Copy and Move for example don't use DBContextData
//
//    pDBContextData = (DBContextData*)hDBContextData;
//    if (pDBContextData != PSLNULL)
//    {
//        status = pDBContextData->Terminate();
//    }
//
//    return status;
//}

/*
 * This function is called at the request phase of every
 * MTP operation that involves DB. This will setup the
 * context and will either perform a DB query or defer it
 * until MTPDBContextDataOpen depending on the implementation.
 *
 *  Arguments:
 *      MTPDBSESSION                 hDBSession     DB Session handle to
 *                                                      open a context
 *      PSLUINT32                    dwJoinType
 *      PSL_CONST MTPDBVARIANTPROP* pmqPropList
 *      PSLUINT32                    cmqPropList
 *      MTPDBCONTEXT*                phDBContext
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */
PSLSTATUS _MTPDBSessionOpenDBContext(MTPDBSESSION hDBSession,
                                     PSLUINT32 dwJoinType,
                                     PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                                     PSLUINT32 cmqPropList,
                                     MTPDBCONTEXT* phDBContext)
{
    PSLSTATUS status;
    DBSession* pDBSession = PSLNULL;
    DBContext* pDBContext = PSLNULL;

    if (PSLNULL != phDBContext)
    {
        *phDBContext = PSLNULL;
    }

    if (PSLNULL == hDBSession || PSLNULL == phDBContext)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pDBSession = (DBSession*)((MTPDBSESSIONOBJ*)hDBSession);

    PSLTraceProgress("MTPDBContextOpen");

    for( PSLUINT32 i = 0; i < cmqPropList; i++ )
    {
        PSLTraceProgress(
                 "MTPDBVARIANTPROP[%d] 0x%04x 0x%04x 0x%08x", i,
                 pmqPropList[i].wPropCode,
                 pmqPropList[i].wDataType,
                 pmqPropList[i].vParam.valUInt32);
    }

    status = DBContext::Create( pDBSession, &pDBContext );
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Parse the context params */

    status = pDBContext->Initialize( dwJoinType, pmqPropList,
                                     cmqPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *phDBContext = (MTPDBCONTEXTOBJ*)pDBContext;
    pDBContext = PSLNULL;

    status = PSLSUCCESS;

Exit:
    if (pDBContext != PSLNULL)
    {
        pDBContext->Cleanup();
    }
    SAFE_RELEASE(pDBContext);

    return status;
}



//*****************************************************************************
//
// MTPDBCONTEXTVTBL _VtblDBContext
//
//*****************************************************************************

/*
 * This function will close the context set up by MTPDBContextOpen
 *
 *  Arguments:
 *      MTPDBCONTEXT    hDBContext
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */
PSLSTATUS _MTPDBContextClose(MTPDBCONTEXT hDBContext)
{
    PSLSTATUS status;

    if (PSLNULL == hDBContext )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    DBContext* pDBContext = (DBContext*)((MTPDBCONTEXTOBJ*)hDBContext);
    status = pDBContext->Cleanup();

    SAFE_RELEASE(pDBContext);

Exit:
    return status;
}

/*
 *  DBContext::MTPDBCancelContext
 *
 *  Cancels the current context
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPDBContextCancel(MTPDBCONTEXT hDBContext)
{
    return PSLERROR_NOT_IMPLEMENTED;
    hDBContext;
}

/*
 * This function will be called from GetNumObjects handler to retrieve
 *  the number of objects associated with the context
 *
 *  Arguments:
 *      MTPDBCONTEXT    hDBContext
 *      PSLUINT32*      pcItems
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *                      PSLSUCCESS_NOT_AVAILABLE if the count is
 *                          available in the context
 *
 */
PSLSTATUS _MTPDBContextGetCount( MTPDBCONTEXT hDBContext,
                                 PSLUINT32* pcItems)
{
    PSLSTATUS status;
    DBContext* pDBContext = PSLNULL;

    if( hDBContext == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pDBContext = (DBContext*)((MTPDBCONTEXTOBJ*)hDBContext);
    status = pDBContext->GetHandleCount( pcItems );

Exit:
    return status;
}

/*
 * This function will be called to retrieve object handle,
 * storage Id and parent handle stored in a context. By default
 * this function will not run a query against the database to
 * retrieve these unless it is fResolveContext paramter is set
 * to PSLTRUE value. By doing that caller is forcing the function
 * to resolve the context and in turn run a query to gather the details.
 * In the case of SendObjectInfo, the handler will call this function
 * with fResolveContext set to PLSTRUE to retrieve the details
 * related to the newly created object.
 *
 *  Arguments:
 *      MTPDBCONTEXT    hDBContext
 *      PSLBOOL         fResolveContext
 *      PSLUINT32*      pdwObjectHandle
 *      PSLUINT32*      pdwParentHandle
 *      PSLUINT32*      pdwStorageID
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *                      PSLSUCCESS_NOT_AVAILABLE when fResolveContext
 *                          is set to PSLFLASE and the handles
 *                          are not available in the context
 *
 */
PSLSTATUS _MTPDBContextGetHandle(MTPDBCONTEXT hDBContext,
                                 PSLBOOL fResolveContext,
                                 PSLUINT32 idxItem,
                                 PSLUINT32* pdwObjectHandle,
                                 PSLUINT32* pdwParentHandle,
                                 PSLUINT32* pdwStorageID)
{
    PSLSTATUS status;

    if( pdwObjectHandle != PSLNULL )
    {
        *pdwObjectHandle = MTP_OBJECTHANDLE_UNDEFINED;
    }

    if( pdwParentHandle != PSLNULL )
    {
        *pdwParentHandle = MTP_OBJECTHANDLE_UNDEFINED;
    }

    if( pdwStorageID != PSLNULL )
    {
        *pdwStorageID = MTP_STORAGEID_UNDEFINED;
    }

    if( hDBContext == PSLNULL ||
        pdwObjectHandle == NULL && pdwParentHandle == NULL &&
        pdwStorageID == NULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    DBContext* pDBContext = (DBContext*)((MTPDBCONTEXTOBJ*)hDBContext);

    if( fResolveContext == PSLTRUE )
    {
        status = pDBContext->Resolve();
        if( PSL_FAILED( status ))
        {
            goto Exit;
        }
    }

    status = pDBContext->GetItemHandle( NULL,
                                        pdwObjectHandle,
                                        pdwParentHandle,
                                        pdwStorageID );
Exit:
    return status;
    idxItem;
}

/*
 * This function when called to commit all the data
 * that has been cached so far to database. This doesn't mean that
 * database won't peform commits outside of this call. It will
 * do so when required but this is the way the handler indicate
 * it is time for it to commit. This function would help
 * handlers to set multiple properties of same object and perform
 * data base commit in one shot
 *
 *  Arguments:
 *      MTPDBCONTEXT    hDBContext
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */
PSLSTATUS _MTPDBContextCommit(MTPDBCONTEXT hDBContext, PSLUINT32 dwObjectID)
{
    PSLSTATUS status;

    if( hDBContext == NULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    DBContext* pDBContext = (DBContext*)((MTPDBCONTEXTOBJ*)hDBContext);
    status = pDBContext->Commit();

Exit:
    return status;
    dwObjectID;
}

/*
 * This function will be called from CopyObject handler
 *
 *  Arguments:
 *      MTPDBCONTEXT    hDBContextDest
 *      MTPDBCONTEXT    hDBContextSrc
 *      PSLMSGQUEUE     mqHandler
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 *  Remarks:
 *      This function is asynchronous and it will send
 *      messages back to handler including the success or
 *      failure status.
 *
 */
PSLSTATUS _MTPDBContextCopy( MTPDBCONTEXT hDBContextDest,
                             MTPDBCONTEXT hDBContextSrc)
{
    PSLSTATUS status;
    DBContext *pDBCtxSrc, *pDBCtxDest;
    MTPITEM* pItems = PSLNULL;

    if( hDBContextDest == PSLNULL || hDBContextSrc == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pDBCtxDest = (DBContext*)((MTPDBCONTEXTOBJ*)hDBContextDest);
    pDBCtxSrc = (DBContext*)((MTPDBCONTEXTOBJ*)hDBContextSrc);

    status = pDBCtxDest->Copy(pDBCtxSrc, &pItems);

    // TODO pass pNewItem's ObjectHandle to GetResponseParams


Exit:
    SAFE_PSLMEMFREE(pItems);
    return status;
}

/*
 * This function will be called from MoveObject handler
 *
 *  Arguments:
 *      MTPDBCONTEXT    hDBContextDest
 *      MTPDBCONTEXT    hDBContextSrc
 *      PSLMSGQUEUE     mqHandler
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 *  Remarks:
 *      This function is asynchronous and it will send
 *      messages back to handler including the success or
 *      failure status.
 *
 */
PSLSTATUS _MTPDBContextMove( MTPDBCONTEXT hDBContextDest,
                             MTPDBCONTEXT hDBContextSrc)
{
    PSLSTATUS status;
    DBContext *pDBCtxSrc, *pDBCtxDest;

    if( hDBContextDest == PSLNULL || hDBContextSrc == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pDBCtxDest = (DBContext*)((MTPDBCONTEXTOBJ*)hDBContextDest);
    pDBCtxSrc = (DBContext*)((MTPDBCONTEXTOBJ*)hDBContextSrc);

    status = pDBCtxDest->Move( pDBCtxSrc );

Exit:

    return status;
}

/*
 * This function will be called from DeleteObject handler
 *
 *  Arguments:
 *      MTPDBCONTEXT    hDBContext
 *      PSLMSGQUEUE     mqHandler
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 *  Remarks:
 *      This function is asynchronous and it will send
 *      messages back to handler including the success or
 *      failure status.
 *
 */
PSLSTATUS _MTPDBContextDelete( MTPDBCONTEXT hDBContext )
{
    PSLSTATUS status;

    if( hDBContext == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    DBContext* pDBContext = (DBContext*)((MTPDBCONTEXTOBJ*)hDBContext);
    status = pDBContext->Delete();

Exit:
    return status;
}


/*
 *  This function will be called from FormatStore handler
 *
 *  Arguments:
 *      PSLUINT32       dwFSFormat
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */
PSLSTATUS _MTPDBContextFormat( MTPDBCONTEXT mdbc, PSLUINT32 dwFSFormat )
{
    return _MTPDBContextDelete(mdbc);
    dwFSFormat;
}

/*
 * This function is called at the request phase of every
 * MTP operation that involves DB. This will pass in the
 * format in which the operation needs the data and the
 * direction of operation. If a DB query was deferred by
 * MTPDBContextOpen it will performaed by this call.
 *
 *  Arguments:
 *      MTPDBCONTEXT                    hDBContext
 *      PSLUINT32                       dwDataType
 *      PSL_CONST MTPDBVARIANTPROP*    pmqPropList
 *      PSLUINT32                       cmqPropList
 *      PSLUINT64*                      pcbTotalSize
 *      MTPDBCONTEXTDATA*               phDBContextData
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */
PSLSTATUS _MTPDBContextDataOpen(MTPDBCONTEXT hDBContext,
                                PSLUINT32 dwDataFormat,
                                PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                                PSLUINT32 cmqPropList,
                                PSLUINT64* pcbTotalSize,
                                MTPDBCONTEXTDATA* phDBContextData)
{
    PSLSTATUS status;

    DBSession* pDBSession = NULL;
    DBContext* pDBContext;
    DBContextData* pDBContextData = NULL;
    PSLUINT64 cbTotalSize = MTP_DATA_SIZE_UNKNOWN;
    PSLUINT32 dwObjectInfoHandle;

    MTPDBVARIANTPROP mqProp;

    PSLTraceProgress( "MTPDBContextDataOpen");

    for( PSLUINT32 i = 0; i < cmqPropList; i++ )
    {
        PSLTraceProgress(
                 "MTPQUAD[%d] 0x%04x 0x%04x 0x%08x", i,
                 pmqPropList[i].wPropCode,
                 pmqPropList[i].wDataType,
                 pmqPropList[i].vParam.valUInt32);
    }

    if (PSLNULL != pcbTotalSize)
    {
        *pcbTotalSize = (PSLUINT64)0;
    }

    if (PSLNULL != phDBContextData)
    {
        *phDBContextData = PSLNULL;
    }

    if (PSLNULL == hDBContext || dwDataFormat == 0 ||
        PSLNULL == phDBContextData)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pDBContext = (DBContext*)((MTPDBCONTEXTOBJ*)hDBContext);

    /* Allocate a context data */

    status = DBContextData::Create( pDBContext, dwDataFormat,
                                    pmqPropList, cmqPropList,
                                    &pDBContextData );
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    // ContextData are useful later on for commit
    // only for UPDATE and INSERT commands
    if( MTPDBCONTEXTDATA_QUERY !=
        (MTPDBCONTEXTDATA_FLAGS_MASK &
                MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat)))
    {
        status = pDBContext->AppendContextData( pDBContextData );
        if( PSL_FAILED( status ))
        {
            goto Exit;
        }
    }

    // SendObject DBContext needs to be initialized
    // using a dwObjectHandle previously stashed at DBSession level
    // from a previous SendObjectInfo or SendObjectPropList cmd

    if(( MTPDBCONTEXTDATADESC_FORMAT(dwDataFormat) ==
         MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA ) &&
         (( MTPDBCONTEXTDATA_FLAGS_MASK &
            MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat)) ==
           MTPDBCONTEXTDATA_UPDATE ))
    {
        status = pDBContext->GetDBSession(&pDBSession);
        if( PSL_FAILED( status ))
        {
            goto Exit;
        }

        status = pDBSession->GetObjectInfoHandleAndSize(
                                 &dwObjectInfoHandle, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        if( MTP_OBJECTHANDLE_UNDEFINED == dwObjectInfoHandle)
        {
            PSLASSERT(!"No ObjectHandle set by a previous"
                       " SendObjectInfo or SendObjectPropList");
            status = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }

        mqProp.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
        mqProp.wDataType = MTP_DATATYPE_UINT32;
        mqProp.vParam.valUInt32 = dwObjectInfoHandle;

        status = pDBContext->Initialize( MTPJOINTYPE_AND,
                                         &mqProp, 1 );
        if( PSL_FAILED( status ))
        {
            goto Exit;
        }
    }

    switch (MTPDBCONTEXTDATA_FLAGS_MASK &
            MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
    {
    case MTPDBCONTEXTDATA_QUERY:
        status = pDBContextData->GetPackedSize( &cbTotalSize );
        break;

    case MTPDBCONTEXTDATA_INSERT:
    case MTPDBCONTEXTDATA_UPDATE:
        // Nothing to do at this time
        // just acknowledge them as valid flags
       break;

    default:
        /* Should never get here */
        PSLASSERT(!"Unknown context data flag");
        status = PSLERROR_INVALID_PARAMETER;
        break;
    }

    if( status != PSLSUCCESS )
    {
        goto Exit;
    }

    if( pcbTotalSize != PSLNULL )
    {
        *pcbTotalSize = cbTotalSize;
    }

    *phDBContextData = (MTPDBCONTEXTDATAOBJ*)pDBContextData;
    pDBContextData = NULL;

    status = PSLSUCCESS;

Exit:
    if (pDBContextData != PSLNULL)
    {
        pDBContextData->Cleanup();
    }
    SAFE_RELEASE( pDBContextData );
    SAFE_RELEASE( pDBSession );

    return status;
}



//*****************************************************************************
//
// MTPDBCONTEXTDATAVTBL _VtblDBContextData
//
//*****************************************************************************

/*
 * This function will close the context data set up by
 * MTPDBContextDataOpen
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA  hDBContextData
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */
PSLSTATUS _MTPDBContextDataClose(MTPDBCONTEXTDATA hDBContextData)
{
    PSLSTATUS status;

    if (PSLNULL == hDBContextData )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    DBContextData* pDBContextData = (DBContextData*)((MTPDBCONTEXTDATAOBJ*)hDBContextData);
    status = pDBContextData->Cleanup();

    SAFE_RELEASE( pDBContextData );

Exit:
    return status;
}

/*
 * This function will cancel the current context data
 *
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */
PSLSTATUS _MTPDBContextDataCancel(MTPDBCONTEXTDATA hDBContextData)
{
    return PSLERROR_NOT_IMPLEMENTED;
    hDBContextData;
}

/*
 * This function will be called from every MTP Get operation
 * handler to copy data from database to the input buffer.
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA  hDBContextData
 *      PSLVOID*        pvBuf
 *      PSLUINT32       cbBuf
 *      PSLUINT32*      pcbWritten
 *      PSLUINT64*      pcbLeft
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 *  Remarks:
 *      This call is synchronous. In case we want to handle
 *      multiple DB serialize operations DB handler has to run
 *      in seperate threads.
 *
 */
PSLSTATUS _MTPDBContextDataSerialize( MTPDBCONTEXTDATA hDBContextData,
                                      PSLVOID* pvBuf,
                                      PSLUINT32 cbBuf,
                                      PSLUINT32* pcbWritten,
                                      PSLUINT64* pcbLeft)
{
    PSLSTATUS status;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    if (PSLNULL == hDBContextData || PSLNULL == pvBuf ||
        PSLNULL == pcbWritten || PSLNULL == pcbLeft)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    DBContextData* pDBContextData = (DBContextData*)((MTPDBCONTEXTDATAOBJ*)hDBContextData);

    status = pDBContextData->Serialize( pvBuf, cbBuf, pcbWritten,
                                        pcbLeft);

Exit:
    return status;
}

/*
 *  This function when called from Set operation handlers
 *  notifies the context data of the size of the incoming
 *  data.
 *
 *  Arguments:
 *      PSLUINT64           cbData          Length of coming data stream
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPDBContextDataStartDeserialize(MTPDBCONTEXTDATA hDBContextData, PSLUINT64 cbData)
{
    return PSLSUCCESS;
    hDBContextData;
    cbData;
}

/*
 * This function when called from Set operation handlers
 * copy data from input buffer over to the database.
 * The mesage queue will allow the funtion to post a
 * message back to the calling handler for releasing
 * the buffer back to the transport.
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA    hDBContextData
 *      PSL_CONST PSLVOID*  pvBuf
 *      PSLUINT32           cbBuf
 *      PSLVOID*            pvData          DB API should set this data
 *                                          as first parameter for any
 *                                          message it sends back to
 *                                          the handler
 *      PSLMSGQUEUE         mqHandler
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 *  Remarks:
 *      This function is asynchronous and it will send
 *      messages back to handler including the success or
 *      failure status.
 *
 */
PSLSTATUS _MTPDBContextDataDeserialize( MTPDBCONTEXTDATA hDBContextData,
                                        PSL_CONST PSLVOID* pvBuf,
                                        PSLUINT32 cbBuf,
                                        PSLMSGQUEUE mqHandler,
                                        MTPCONTEXT* pmtpctx,
                                        PSLPARAM aParam,
                                        PSLUINT32 dwMsgFlags)
{
    PSLSTATUS status;
    PSLMSGPOOL mpHandle;
    PSLMSG* pMsg = PSLNULL;
    DBSession* pDBSession = NULL;
    DBContext* pDBContext = NULL;

    if( hDBContextData == PSLNULL || pvBuf == PSLNULL ||
        cbBuf == 0 || ( mqHandler != PSLNULL && pmtpctx == PSLNULL ))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    DBContextData* pDBContextData = (DBContextData*)((MTPDBCONTEXTDATAOBJ*)hDBContextData);

    status = pDBContextData->GetDBContext( &pDBContext );
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    status = pDBContext->GetDBSession( &pDBSession );
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    status = pDBSession->GetMsgPool( &mpHandle );
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    status = pDBContextData->Deserialize( pvBuf, cbBuf );
    if( PSL_FAILED( status ))
    {
        // TODO set failure message here, to be posted to msg queue
        goto Exit;
    }

    if (PSLNULL != mqHandler)
    {
        status = PSLMsgAlloc( mpHandle, &pMsg );
        if( PSL_FAILED( status ))
        {
            goto Exit;
        }

        pMsg->msgID = MTPMSG_DATA_CONSUMED;
        pMsg->aParam0 = (PSLPARAM)pmtpctx;
        pMsg->aParam1 = aParam;
        pMsg->aParam2 = (PSLPARAM)pvBuf;
        pMsg->aParam3 = MTPMSGFLAG_TERMINATE & dwMsgFlags;
        pMsg->alParam = cbBuf;

        status = PSLMsgPost( mqHandler, pMsg );
        if( PSL_FAILED( status ))
        {
            goto Exit;
        }

        pMsg = PSLNULL;
    }

Exit:
    SAFE_RELEASE( pDBContext );
    SAFE_RELEASE( pDBSession );
    SAFE_PSLMSGFREE( pMsg );

    return status;
}



//*****************************************************************************
//
// Database access function table
//
//*****************************************************************************
static PSL_CONST MTPDBSESSIONVTBL _VtblDBSession =
{
    _MTPDBCloseSession,
    _MTPDBSessionOpenDBContext
};

static PSL_CONST MTPDBCONTEXTVTBL _VtblDBContext =
{
    _MTPDBContextClose,
    _MTPDBContextCancel,
    _MTPDBContextGetCount,
    _MTPDBContextGetHandle,
    _MTPDBContextCommit,
    _MTPDBContextCopy,
    _MTPDBContextMove,
    _MTPDBContextDelete,
    _MTPDBContextFormat,
    _MTPDBContextDataOpen
};

static PSL_CONST MTPDBCONTEXTDATAVTBL _VtblDBContextData =
{
    _MTPDBContextDataClose,
    _MTPDBContextDataCancel,
    _MTPDBContextDataSerialize,
    _MTPDBContextDataStartDeserialize,
    _MTPDBContextDataDeserialize
};

PSL_CONST MTPDBSESSIONVTBL* GetDBSessionVTable()
{
    return &_VtblDBSession;
}

PSL_CONST MTPDBCONTEXTVTBL* GetDBContextVTable()
{
    return &_VtblDBContext;
}

PSL_CONST MTPDBCONTEXTDATAVTBL* GetDBContextDataVTable()
{
    return &_VtblDBContextData;
}
