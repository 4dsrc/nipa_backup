/*
 *  MTPObjectProperty.c
 *
 *  Contains definition of the object property DB API functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLWindows.h"
#include "PSL.h"
#include "MTP.h"
#include "MTPUtils.h"
#include "PSLWinSafe.h"
#include "MTPObjectProperty.h"

/*  Local defines
 */

#define FORMAT_PROPGROUP_ALL \
    MTP_OBJECTPROPCODE_STORAGEID, \
    MTP_OBJECTPROPCODE_OBJECTFORMAT, \
    MTP_OBJECTPROPCODE_PROTECTIONSTATUS, \
    MTP_OBJECTPROPCODE_OBJECTSIZE, \
    MTP_OBJECTPROPCODE_OBJECTFILENAME, \
    MTP_OBJECTPROPCODE_PARENT, \
    MTP_OBJECTPROPCODE_PERSISTENTGUID, \
    MTP_OBJECTPROPCODE_NAME, \
    MTP_OBJECTPROPCODE_NONCONSUMABLE, \
    MTP_OBJECTPROPCODE_DATECREATED, \
    MTP_OBJECTPROPCODE_DATEMODIFIED, \
    MTP_OBJECTPROPCODE_HIDDEN, \
    MTP_OBJECTPROPCODE_ASSOCIATIONTYPE, \
    MTP_OBJECTPROPCODE_ASSOCIATIONDESC

#define FORMAT_PROPGROUP_AUDIO \
    MTP_OBJECTPROPCODE_ARTIST, \
    MTP_OBJECTPROPCODE_TRACK, \
    MTP_OBJECTPROPCODE_ALBUMNAME, \
    MTP_OBJECTPROPCODE_USERRATING, \
    MTP_OBJECTPROPCODE_AUDIOBITDEPTH

#define FORMAT_PROPGROUP_AUDIOABSTRACT \
    MTP_OBJECTPROPCODE_ALBUMARTIST

#define FORMAT_PROPGROUP_VIDEO \
    MTP_OBJECTPROPCODE_SCANTYPE, \
    MTP_OBJECTPROPCODE_VIDEOFOURCCCODEC, \
    MTP_OBJECTPROPCODE_VIDEOBITRATE, \
    MTP_OBJECTPROPCODE_FRAMESPERMILLISECOND, \
    MTP_OBJECTPROPCODE_KEYFRAMEDISTANCE, \
    MTP_OBJECTPROPCODE_ENCODINGQUALITY, \
    MTP_OBJECTPROPCODE_ENCODINGPROFILE

#define FORMAT_PROPGROUP_VIDEOIMAGE \
    MTP_OBJECTPROPCODE_WIDTH, \
    MTP_OBJECTPROPCODE_HEIGHT

#define FORMAT_PROPGROUP_AUDIOVIDEO \
    MTP_OBJECTPROPCODE_USECOUNT, \
    MTP_OBJECTPROPCODE_SAMPLERATE, \
    MTP_OBJECTPROPCODE_NUMBEROFCHANNELS, \
    MTP_OBJECTPROPCODE_AUDIOWAVECODEC, \
    MTP_OBJECTPROPCODE_AUDIOBITRATE, \
    MTP_OBJECTPROPCODE_DURATION, \
    MTP_OBJECTPROPCODE_DRMPROTECTION, \
    MTP_OBJECTPROPCODE_BITRATETYPE

#define FORMAT_PROPGROUP_AUDIOVIDEOABSTRACT \
    MTP_OBJECTPROPCODE_GENRE


/*  Local Types
 */

typedef struct _ACCESSINFO
{
    PSLUINT16               wPropCode;
    PSLUINT8                bGetSet;
} ACCESSINFO;

typedef struct _XMEFORMATREC
{
    PSLUINT32               dwFormatCode;
    PSLBOOL                 fSupported;
    PSL_CONST PSLUINT16*    pwPropCodes;
    PSLUINT32               cPropCodes;
} XMEFORMATREC;


/*  Local variable
 */

static PSL_CONST ACCESSINFO     _RgAccessList[] =
{
    {
        MTP_OBJECTPROPCODE_STORAGEID,
        MTP_PROPGETSET_GETONLY,
    },
    {
        MTP_OBJECTPROPCODE_OBJECTFORMAT,
        MTP_PROPGETSET_GETONLY,
    },
    {
        MTP_OBJECTPROPCODE_PROTECTIONSTATUS,
        MTP_PROPGETSET_GETONLY,
    },
    {
        MTP_OBJECTPROPCODE_OBJECTSIZE,
        MTP_PROPGETSET_GETONLY,
    },
    {
        MTP_OBJECTPROPCODE_ASSOCIATIONTYPE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ASSOCIATIONDESC,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_OBJECTFILENAME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_DATECREATED,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_DATEMODIFIED,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PARENT,
        MTP_PROPGETSET_GETONLY,
    },
    {
        MTP_OBJECTPROPCODE_HIDDEN,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PERSISTENTGUID,
        MTP_PROPGETSET_GETONLY,
    },
    {
        MTP_OBJECTPROPCODE_NAME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ARTIST,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_NONCONSUMABLE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_WIDTH,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_HEIGHT,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_DURATION,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_USERRATING,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_TRACK,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_GENRE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_USECOUNT,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ALBUMNAME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ALBUMARTIST,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_DRMPROTECTION,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_DISPLAYNAME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_BODYTEXT,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_SUBJECT,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PRIORITY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_GIVENNAME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_MIDDLENAMES,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_FAMILYNAME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PREFIX,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_SUFFIX,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PHONETICGIVENNAME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PHONETICFAMILYNAME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_EMAILPRIMARY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_EMAILPERSONAL1,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_EMAILPERSONAL2,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_EMAILBUSINESS1,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_EMAILBUSINESS2,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_EMAILOTHERS,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERPRIMARY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERPERSONAL,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERPERSONAL2,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERBUSINESS,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERBUSINESS2,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERMOBILE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERMOBILE2,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_FAXNUMBERPRIMARY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_FAXNUMBERPERSONAL,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_FAXNUMBERBUSINESS,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PAGERNUMBER,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBEROTHERS,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PRIMARYWEBADDRESS,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PERSONALWEBADDRESS,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_BUSINESSWEBADDRESS,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS2,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS3,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALFULL,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALLINE1,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALLINE2,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALCITY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALREGION,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALPOSTALCODE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALCOUNTRY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSFULL,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSLINE1,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSLINE2,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSCITY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSREGION,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSPOSTALCODE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSCOUNTRY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERFULL,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERLINE1,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERLINE2,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERCITY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERREGION,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERPOSTALCODE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERCOUNTRY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ORGANIZATIONNAME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_PHONETICORGANIZATIONNAME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ROLE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_BIRTHDAY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_MESSAGETO,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_MESSAGECC,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_MESSAGEBCC,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_MESSAGEREAD,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_MESSAGERECEIVETIME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_MESSAGESENDER,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYBEGINTIME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYENDTIME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYLOCATION,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYREQUIREDATTENDEES,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYOPTIONALATTENDEES,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYRESOURCES,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYACCEPTEDED,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYTENTATIVE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYDECLINED,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYREMINDERTIME,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYOWNER,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYSTATUS,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_BITRATETYPE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_SAMPLERATE,
        MTP_PROPGETSET_GETONLY,
    },
    {
        MTP_OBJECTPROPCODE_NUMBEROFCHANNELS,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_AUDIOBITDEPTH,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_SCANTYPE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_AUDIOWAVECODEC,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_AUDIOBITRATE,
        MTP_PROPGETSET_GETONLY,
    },
    {
        MTP_OBJECTPROPCODE_VIDEOFOURCCCODEC,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_VIDEOBITRATE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_FRAMESPERMILLISECOND,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_KEYFRAMEDISTANCE,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ENCODINGQUALITY,
        MTP_PROPGETSET_GETSET,
    },
    {
        MTP_OBJECTPROPCODE_ENCODINGPROFILE,
        MTP_PROPGETSET_GETSET,
    },
};

static PSL_CONST PSLUINT16      _RgwCoreProps[] =
{
    FORMAT_PROPGROUP_ALL,
};

static PSL_CONST PSLUINT16      _RgwAudioProps[] =
{
    FORMAT_PROPGROUP_ALL,
    FORMAT_PROPGROUP_AUDIO,
    FORMAT_PROPGROUP_AUDIOABSTRACT,
    FORMAT_PROPGROUP_AUDIOVIDEO,
    FORMAT_PROPGROUP_AUDIOVIDEOABSTRACT
};

static PSL_CONST PSLUINT16      _RgwAudioVideoProps[] =
{
    FORMAT_PROPGROUP_ALL,
    FORMAT_PROPGROUP_AUDIO,
    FORMAT_PROPGROUP_AUDIOABSTRACT,
    FORMAT_PROPGROUP_VIDEO,
    FORMAT_PROPGROUP_VIDEOIMAGE,
    FORMAT_PROPGROUP_AUDIOVIDEO,
    FORMAT_PROPGROUP_AUDIOVIDEOABSTRACT
};

static PSL_CONST PSLUINT16      _RgwImageProps[] =
{
    FORMAT_PROPGROUP_ALL,
    FORMAT_PROPGROUP_VIDEOIMAGE,
};

static PSL_CONST PSLUINT16      _RgwAbstractProps[] =
{
    FORMAT_PROPGROUP_ALL,
    FORMAT_PROPGROUP_AUDIOABSTRACT,
    FORMAT_PROPGROUP_AUDIOVIDEOABSTRACT
};

static XMEFORMATREC _RgmFormatTbl[] =
{
    {
        MTP_FORMATCODE_UNDEFINED, PSLFALSE,
        _RgwCoreProps, PSLARRAYSIZE(_RgwCoreProps)
    },
    {
        MTP_FORMATCODE_ASSOCIATION, PSLFALSE,
        _RgwCoreProps, PSLARRAYSIZE(_RgwCoreProps)
    },
    {
        MTP_FORMATCODE_MP3, PSLFALSE,
        _RgwAudioProps, PSLARRAYSIZE(_RgwAudioProps)
    },
    {
        MTP_FORMATCODE_ASF, PSLFALSE,
        _RgwAudioVideoProps, PSLARRAYSIZE(_RgwAudioVideoProps)
    },
    {
        MTP_FORMATCODE_IMAGE_EXIF, PSLFALSE,
        _RgwImageProps, PSLARRAYSIZE(_RgwImageProps)
    },
    {
        MTP_FORMATCODE_UNDEFINEDFIRMWARE, PSLFALSE,
        _RgwCoreProps, PSLARRAYSIZE(_RgwCoreProps)
    },
    {
        MTP_FORMATCODE_WMA, PSLFALSE,
        _RgwAudioProps, PSLARRAYSIZE(_RgwAudioProps)
    },
    {
        MTP_FORMATCODE_WMV, PSLFALSE,
        _RgwAudioVideoProps, PSLARRAYSIZE(_RgwAudioVideoProps)
    },
    {
        MTP_FORMATCODE_ABSTRACTAUDIOALBUM, PSLFALSE,
        _RgwAbstractProps, PSLARRAYSIZE(_RgwAbstractProps)
    },
    {
        MTP_FORMATCODE_ABSTRACTAUDIOVIDEOPLAYLIST, PSLFALSE,
        _RgwCoreProps, PSLARRAYSIZE(_RgwCoreProps)
    },
};

static PSLUINT32                _CdwSuppFmtCodes = 0;

static const MTPFORMATREC       _RecObjectFormat =
{
    FORMATFLAGS_TYPE_OBJECT
};

static const MTPFORMATINFO*     _RgfiSupportedFormats = PSLNULL;


/* This function will provide DB to perform any
 * object property related initialization.
 */
PSLSTATUS MTPDBSessionInitialize(PCXPSLUINT16 pwFormatCodes,
                            PSLUINT32 cdwFormatCodes)
{
    PSLUINT32 cPropsSupported;
    PSLUINT32 idxTbl;
    PSLUINT32 idxFmtCode;
    PSLUINT32 idxProp;
    MTPFORMATINFO* pfiSupportedFormats = PSLNULL;
    MTPFORMATINFO* pfiCur;
    MTPOBJECTPROPINFO* popiSupportedProps;
    MTPOBJECTPROPINFO* popiCur;
    PSLSTATUS status;
    PSLUINT16 wFormatCode;

    /*  Validate Arguments */

    if (PSLNULL == pwFormatCodes)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    PSLASSERT(cdwFormatCodes <= PSLARRAYSIZE(_RgmFormatTbl));

    /* Identify valid format codes in _RgmFormatTbl*/
    for (idxFmtCode = 0, cPropsSupported = 0;
            idxFmtCode < cdwFormatCodes;
            idxFmtCode++)
    {
        for (idxTbl = 0; idxTbl < PSLARRAYSIZE(_RgmFormatTbl); idxTbl++)
        {
            wFormatCode = MTPLoadUInt16(&pwFormatCodes[idxFmtCode]);

            if (wFormatCode == _RgmFormatTbl[idxTbl].dwFormatCode)
            {
                _RgmFormatTbl[idxTbl].fSupported = PSLTRUE;
                _CdwSuppFmtCodes++;
                cPropsSupported += _RgmFormatTbl[idxTbl].cPropCodes;
                break;
            }
        }
    }

    /*  Begin the process of building up a new style MTPFORMATINFO and
     *  MTPOBJECTPROPINFO dataset for these legacy properties- since all of
     *  the data is static we do a single allocation for everything
     */

    pfiSupportedFormats = (MTPFORMATINFO*)PSLMemAlloc(PSLMEM_PTR,
                            (sizeof(MTPFORMATINFO) * _CdwSuppFmtCodes) +
                            (sizeof(MTPOBJECTPROPINFO) * cPropsSupported));

    if (PSLNULL == pfiSupportedFormats)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    popiSupportedProps = (MTPOBJECTPROPINFO*)
                            (pfiSupportedFormats + _CdwSuppFmtCodes);

    /*  Walk the supported format list again- this time we will initialize
     *  the new property data sets correctly
     */

    for (idxTbl = 0, pfiCur = pfiSupportedFormats, popiCur = popiSupportedProps;
            idxTbl < PSLARRAYSIZE(_RgmFormatTbl);
            idxTbl++)
    {
        /*  If this format is not enabled skip it
         */

        if (!_RgmFormatTbl[idxTbl].fSupported)
        {
            continue;
        }

        /*  Initialize the core format information- note that we share the
         *  format record because they are the same for all of the formats
         */

        pfiCur->precFormat = &_RecObjectFormat;
        pfiCur->wFormatCode = (PSLUINT16)_RgmFormatTbl[idxTbl].dwFormatCode;
        pfiCur->rgopiObjectProps = popiCur;
        pfiCur->cObjectProps = _RgmFormatTbl[idxTbl].cPropCodes;


        /*  Walk through and initialize each of the property blobs
         */

        for (idxProp = 0;
                idxProp < pfiCur->cObjectProps;
                idxProp++, popiCur++)
        {
            /*  Initialize the core properties- we assume all of the properties
             *  here use the default property record
             */

            PSLASSERT(PSLNULL == popiCur->infoProp.precProp);
            popiCur->infoProp.wPropCode =
                            _RgmFormatTbl[idxTbl].pwPropCodes[idxProp];
            status = MTPDBObjectPropertyGetAccess(popiCur->infoProp.wPropCode,
                            &(popiCur->infoProp.bGetSet));
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
            popiCur->dwGroupCode = MTP_GROUPCODE_OBJINFO;
        }

        /*  Make sure we copied the expected number of properties
         */

        PSLASSERT((PSLUINT32)(popiCur - pfiCur->rgopiObjectProps) ==
                            pfiCur->cObjectProps);

        /*  Point to the next format location
         */

        pfiCur++;
    }

   /*  Make sure we initialized correctly
     */

    PSLASSERT((PSLUINT32)(pfiCur - pfiSupportedFormats) == _CdwSuppFmtCodes);
    PSLASSERT((PSLUINT32)(popiCur - popiSupportedProps) == cPropsSupported);
    _RgfiSupportedFormats = pfiSupportedFormats;
    pfiSupportedFormats = PSLNULL;
    status = PSLSUCCESS;

Exit:
    if (PSL_FAILED(status))
    {
        MTPDBSessionUninitialize();
    }
    SAFE_PSLMEMFREE(pfiSupportedFormats);
    return status;
}

PSLSTATUS MTPDBSessionUninitialize()
{
   PSLVOID** ppvTemp;

    /*  Work around the compiler warning about constant
     */
    _CdwSuppFmtCodes = 0;
    ppvTemp = (PSLVOID*)&_RgfiSupportedFormats;
    SAFE_PSLMEMFREE(*ppvTemp);
    return PSLSUCCESS;
}

PSLINT32 PSL_API _AccessListCompare(PSL_CONST PSLVOID* pvKey,
                            PSL_CONST PSLVOID* pvComp)
{
    return ((PSLINT32)((PSLUINT16)((PSLSIZET)pvKey)) -
                    (PSLINT32)(((ACCESSINFO*)pvComp)->wPropCode));
}

PSLSTATUS PSL_API MTPDBObjectPropertyGetAccess(PSLUINT16 wPropCode,
                            PSLUINT8* pbGetSet)
{
    PSLSTATUS status;
    PSL_CONST ACCESSINFO* pAccess;

    if (PSLNULL != pbGetSet)
    {
        *pbGetSet = MTP_PROPGETSET_GETONLY;
    }

    if (PSLNULL == pbGetSet)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLBSearch((PSLVOID*)(PSLUINT32)wPropCode, _RgAccessList,
                            PSLARRAYSIZE(_RgAccessList),
                            sizeof(_RgAccessList[0]),
                            _AccessListCompare, (PSLVOID**)&pAccess);
    if (PSLSUCCESS != status)
    {
        PSLASSERT(MTP_PROPGETSET_GETONLY == *pbGetSet);
        goto Exit;
    }

    *pbGetSet = pAccess->bGetSet;

Exit:
    return status;
}

PSLSTATUS MTPDBObjectPropDescGet(PSLUINT32* pdwCodes, PSLUINT32 cCodes,
    PSLBYTE* pBuf, PSLUINT32 cbBuf, PSLUINT32* pcbSize)
{
    PSLSTATUS status;
    MTPOBJECTPROPINFO objPropInfo = { 0 };

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    /* pBuf & pcbSize can be PSLNULL */
    PSLASSERT(2 == cCodes);
    if ((PSLNULL == pdwCodes) || (2 != cCodes))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }


    /* Check if the object propcode is valid wrt to format code*/
    if (PSLSUCCESS != MTPDBIsObjectPropCodeValid((PSLUINT16)pdwCodes[0],
                                                    pdwCodes[1]))
    {
        status = MTPERROR_PROPERTY_INVALID_OBJECTPROPCODE;
        goto Exit;
    }

    /* Start initializing the property information */

    objPropInfo.infoProp.wPropCode = (PSLUINT16)pdwCodes[0];
    objPropInfo.dwGroupCode = MTP_GROUPCODE_OBJINFO;

    /* Retrieve the property rec */

    status = MTPPropertyGetDefaultRec(objPropInfo.infoProp.wPropCode,
                            PROPFLAGS_TYPE_OBJECT,
                            &(objPropInfo.infoProp.precProp));
    if (PSLSUCCESS != status)
    {
        status = PSL_FAILED(status) ?
                            status : MTPERROR_PROPERTY_INVALID_OBJECTPROPCODE;
        goto Exit;
    }

    /*  Retrieve the get/set information- if the property is not found access
     *  defaults to read only */

    status = MTPDBObjectPropertyGetAccess(objPropInfo.infoProp.wPropCode,
                            &(objPropInfo.infoProp.bGetSet));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*  And serialize */

    status = MTPPropertyDescSerialize((MTPPROPERTYINFO*)&objPropInfo.infoProp,
                            PROPFLAGS_TYPE_OBJECT, PSLNULL, pBuf, cbBuf,
                            pcbSize);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}

PSLSTATUS MTPDBObjectPropCodesGet(PSLUINT32 dwFormatCode,
        PSLBYTE* pBuf, PSLUINT32 cbBuf, PSLUINT32* pcbSize)
{
    PSLSTATUS status;
    PSLDYNLIST dlCodes = PSLNULL;
    PSLUINT32 cPropCodes;
    PSLUINT32 ixCodes;
    PSLUINT16 wPropCode;
    PSLBYTE* pbStart;

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    PSLASSERT(MTP_FORMATCODE_NOTUSED != dwFormatCode);

    if (MTP_FORMATCODE_NOTUSED == dwFormatCode)
    {
        status = MTPERROR_PROPERTY_FORMATCODE_UNSUPPORTED;
        goto Exit;
    }

    status = PSLDynListCreate(0, PSLFALSE, PSLDYNLIST_GROW_DEFAULT, &dlCodes);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBGetObjectPropCodes(dwFormatCode, MTP_GROUPCODE_NONE,
                                    PSLFALSE, dlCodes);

    /* If property codes are not available return empty buffer */
    if (PSLERROR_NOT_AVAILABLE == status)
    {
        if (PSLNULL != pBuf)
        {
            /* Even in the case where no property codes
             * are available an empty MTP arrray is expected.
             */
            if (cbBuf < sizeof(PSLUINT32))
            {
                status = PSLERROR_INSUFFICIENT_BUFFER;
                goto Exit;
            }

            /* An empty array would consist of a single 32-bit
             * integer containing a value of 0x00000000.
             */
            MTPStoreUInt32((PXPSLUINT32)pBuf, 0);
        }

        /* if pBuf equal to PSLNULL return size */
        if (PSLNULL != pcbSize)
        {
            *pcbSize = sizeof(PSLUINT32);
        }
        status = PSLSUCCESS;
        goto Exit;
    }

    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = PSLDynListGetSize(dlCodes, &cPropCodes);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (PSLNULL == pBuf)
    {
        if (PSLNULL != pcbSize)
        {
            *pcbSize = sizeof(PSLUINT32) +
                    (cPropCodes * sizeof(PSLUINT16));
        }
        status = PSLSUCCESS;
        goto Exit;
    }

    /* stash the start address for later use */
    pbStart = pBuf;

    MTPStoreUInt32((PXPSLUINT32)pBuf, cPropCodes);
    pBuf += sizeof(PSLUINT32);

    for (ixCodes = 0; ixCodes < cPropCodes; ixCodes++)
    {
        status = PSLDynListGetItem(dlCodes, ixCodes,
                                   (PSLPARAM*)&wPropCode);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        MTPStoreUInt16((PXPSLUINT16)pBuf, wPropCode);
        pBuf += sizeof(PSLUINT16);
    }

    if (PSLNULL != pcbSize)
    {
        *pcbSize = pBuf - pbStart;
    }

Exit:
    SAFE_PSLDYNLISTDESTROY(dlCodes);
    return status;
}

/*  This function will return an array of object prop codes
 *  given the format code and group code. If the group code
 *  is MTP_GROUPCODE_NONE and fIgnoreSlowOnes is PSLFLASE
 *  all object prop codes that correspond to dwFormatCode
 *  will be returned.
 */
PSLSTATUS MTPDBGetObjectPropCodes(PSLUINT32 dwFormatCode,
        PSLUINT32 dwGroupCode, PSLBOOL fIgnoreSlowOnes,
        PSLDYNLIST dlObjPropCodes)
{
    PSLSTATUS status;
    PSLUINT32 ixTbl;
    const PSLUINT16* pwPropCodes = PSLNULL;
    PSLUINT32 cdwPropCodes = 0;
    PSLUINT32 cItems;
    PSLUINT32 ixDL = 0;
    PSLUINT32 ixPropCode;
    PSLUINT32 dwGroupCodeCmp;

    if (PSLNULL == dlObjPropCodes)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(MTP_FORMATCODE_NOTUSED != dwFormatCode);
    if (MTP_FORMATCODE_NOTUSED == dwFormatCode)
    {
        status = MTPERROR_PROPERTY_FORMATCODE_UNSUPPORTED;
        goto Exit;
    }

    status = PSLSUCCESS_NOT_FOUND;

    for (ixTbl = 0; ixTbl < PSLARRAYSIZE(_RgmFormatTbl); ixTbl++)
    {
        if (PSLTRUE == _RgmFormatTbl[ixTbl].fSupported &&
            dwFormatCode == _RgmFormatTbl[ixTbl].dwFormatCode)
        {
            pwPropCodes = _RgmFormatTbl[ixTbl].pwPropCodes;
            cdwPropCodes = _RgmFormatTbl[ixTbl].cPropCodes;
            status = PSLSUCCESS;
            break;
        }
    }

    /* couldn't find the format code */
    if (PSLSUCCESS_NOT_FOUND == status)
    {
        status = MTPERROR_PROPERTY_FORMATCODE_UNSUPPORTED;
        goto Exit;
    }

    /* get the current size of the list */
    status = PSLDynListGetSize(dlObjPropCodes, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* add property list count to the intial item count
     * and set that as the size of the list
     */
    status = PSLDynListSetSize(dlObjPropCodes, cdwPropCodes + cItems,
                                PSLDYNLIST_GROW_UNCHANGED);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    for (ixPropCode = 0; ixPropCode < cdwPropCodes; ixPropCode++)
    {
        /*  Currently all properties are returned in the same group */
        dwGroupCodeCmp = MTP_GROUPCODE_OBJINFO;

        if (MTP_GROUPCODE_NONE == dwGroupCode ||
            (dwGroupCode == dwGroupCodeCmp))
        {
            if ((PSLTRUE == fIgnoreSlowOnes &&
                 MTP_GROUPCODE_SLOW != dwGroupCodeCmp) ||
                (PSLFALSE == fIgnoreSlowOnes))
            {
                status = PSLDynListSetItem(dlObjPropCodes, ixDL,
                                           pwPropCodes[ixPropCode]);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
                ixDL++;
            }
        }
    }

    /* set size to valid number of items present in the list */
    status = PSLDynListSetSize(dlObjPropCodes, ixDL,
                               PSLDYNLIST_GROW_UNCHANGED);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Nothing got added to the list means property codes not found */
    status = (0 < ixDL) ? status : PSLERROR_NOT_AVAILABLE;
Exit:
    return status;
}

/*
 *  This function will return PSLSUCCESS if the object propcode is
 *  valid corresponding to the format code.
 *
 */
PSLSTATUS MTPDBIsObjectPropCodeValid(PSLUINT16 wObjectPropCode,
                                   PSLUINT32 dwFormatCode)
{
    PSLSTATUS status;
    PSLUINT32 ixTbl;
    PSL_CONST PSLUINT16* pwPropCodes = PSLNULL;
    PSLUINT32 cdwPropCodes = 0;
    PSLUINT32 ixPropCode;

    PSLASSERT(dwFormatCode != MTP_FORMATCODE_NOTUSED);

    if (MTP_FORMATCODE_NOTUSED == dwFormatCode)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLSUCCESS_NOT_FOUND;

    for (ixTbl = 0; ixTbl < PSLARRAYSIZE(_RgmFormatTbl); ixTbl++)
    {
        if (PSLTRUE == _RgmFormatTbl[ixTbl].fSupported &&
            dwFormatCode == _RgmFormatTbl[ixTbl].dwFormatCode)
        {
            pwPropCodes = _RgmFormatTbl[ixTbl].pwPropCodes;
            cdwPropCodes = _RgmFormatTbl[ixTbl].cPropCodes;
            status = PSLSUCCESS;
            break;
        }
    }

    if (PSLSUCCESS_NOT_FOUND == status)
    {
        status = MTPERROR_PROPERTY_FORMATCODE_UNSUPPORTED;
        goto Exit;
    }

    status = PSLSUCCESS_FALSE;

    for(ixPropCode = 0; ixPropCode < cdwPropCodes; ixPropCode++)
    {
        if (wObjectPropCode == pwPropCodes[ixPropCode])
        {
            status = PSLSUCCESS;
            break;
        }
    }

Exit:
    return status;
}

/*
 *  This function will return PSLSUCCESS if the format code is
 *  supported by the device.
 *
 */
PSLSTATUS MTPDBIsFormatCodeSupported(PSLUINT32 dwFormatCode)
{
    PSLSTATUS status;
    PSLUINT32 ixTbl;

    status = PSLSUCCESS_FALSE;

    for (ixTbl = 0; ixTbl < PSLARRAYSIZE(_RgmFormatTbl); ixTbl++)
    {
        if (PSLTRUE == _RgmFormatTbl[ixTbl].fSupported &&
            dwFormatCode == _RgmFormatTbl[ixTbl].dwFormatCode)
        {
            status = PSLSUCCESS;
            break;
        }
    }

    return status;
}

/*
 *  This function will return the supported format codes and the count
 *  pdwFormatCodes and pcFormatCodes respectively.
 *
 */
PSLSTATUS MTPDBGetSupportedFormatCodes(PSLUINT32** ppdwFormatCodes,
                                     PSLUINT32* pcFormatCodes)
{
    PSLSTATUS status;
    PSLUINT32 ixTbl;
    PSLUINT32* pdwFmts = PSLNULL;
    PSLUINT32 ixFmt = 0;

    if (PSLNULL != ppdwFormatCodes)
    {
        *ppdwFormatCodes = PSLNULL;
    }

    if (PSLNULL != pcFormatCodes)
    {
        *pcFormatCodes = 0;
    }

    /*validate the arguments*/
    if (PSLNULL == ppdwFormatCodes || PSLNULL == pcFormatCodes)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (0 == _CdwSuppFmtCodes)
    {
        status = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    pdwFmts = PSLMemAlloc(PSLMEM_PTR, sizeof(PSLUINT32) * \
                                _CdwSuppFmtCodes);
    if (PSLNULL == pdwFmts)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    for(ixTbl = 0; ixTbl < PSLARRAYSIZE(_RgmFormatTbl); ixTbl++)
    {
        if (PSLTRUE == _RgmFormatTbl[ixTbl].fSupported)
        {
            PSLASSERT(ixFmt < _CdwSuppFmtCodes);
            pdwFmts[ixFmt++] = _RgmFormatTbl[ixTbl].dwFormatCode;
        }
    }

    *ppdwFormatCodes = pdwFmts;
    pdwFmts = PSLNULL;

    *pcFormatCodes = _CdwSuppFmtCodes;

    status = PSLSUCCESS;
Exit:

    SAFE_PSLMEMFREE(pdwFmts);
    return status;
}

PSLSTATUS MTPDBGetFormatList(PSL_CONST MTPFORMATINFO** ppfiList,
                            PSLUINT32* pcFormats)
{
    PSLSTATUS status;

    if (PSLNULL != ppfiList)
    {
        *ppfiList = PSLNULL;
    }
    if (PSLNULL != pcFormats)
    {
        *pcFormats = 0;
    }

    if ((PSLNULL == ppfiList) || (PSLNULL == pcFormats))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    *ppfiList = _RgfiSupportedFormats;
    *pcFormats = _CdwSuppFmtCodes;

    status = PSLSUCCESS;

Exit:
    return status;
}

