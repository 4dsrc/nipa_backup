/*
 *  MTPDBHandlerPrecomp.h
 *
 *  Precompiled header file for command handler support.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPDBHANDLERPRECOMP_H_
#define _MTPDBHANDLERPRECOMP_H_

#include "PSLWindows.h"
#include "PSLWinSafe.h"
#include "PSL.h"
#include "MTP.h"
#include "MTPUtils.h"
#include "PSLUnknown.h"
#include "MTPDataStore.h"
#include "DBStructs.h"

#include "MTPDataBaseUtil.h"

#ifndef PSLSTATUS_FROM_HRESULT
#define PSLSTATUS_FROM_HRESULT( hr ) \
      hr == S_OK            ? PSLSUCCESS \
    : hr == S_FALSE         ? PSLSUCCESS_FALSE \
    : SUCCEEDED( hr )       ? PSLSUCCESS \
    : hr == E_OUTOFMEMORY   ? PSLERROR_OUT_OF_MEMORY \
    : PSLERROR_PLATFORM_FAILURE
#endif /*PSLSTATUS_FROM_HRESULT*/

#endif  /* _MTPDBHANDLERPRECOMP_H_ */
