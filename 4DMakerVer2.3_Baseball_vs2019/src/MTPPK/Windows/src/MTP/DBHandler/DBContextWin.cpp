/*
 *  MTPDBUtils.cpp
 *
 *  Contains definitions of utilities functions
 *      managing MTP DB session state
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPDBHandlerPrecomp.h"
#include "FileUtils.h"
#include "DBContextWin.h"
#include "DBContextDataWin.h"
#include "MTPObjectProperty.h"


// forward declaration
// TODO expose this from DataStore instead of copy&paste it
PSLSTATUS _MTPSetFilePath(MTPDATASTORE mds, MTPITEM mtpItem);

#define DATABASE_MSG_POOL_SIZE  4
#define MAX_NUM_DBCONTEXTDATA   10

DBSession::DBSession(MTPDATASTORE mds, PSLMSGPOOL mpHandle)
    : m_mds(mds),
    m_mpHandle(mpHandle),
    m_dwObjectInfoHandle(MTP_OBJECTHANDLE_UNDEFINED),
    m_dwObjectInfoSize(0)
{
    vtbl = GetDBSessionVTable();
    PSLASSERT(PSLNULL != mds);
}

DBSession::~DBSession()
{
    PSLASSERT(PSLNULL == m_mds);
    PSLASSERT(PSLNULL == m_mpHandle);
}

PSLSTATUS DBSession::Create(DBSession** ppDBSession)
{
    PSLSTATUS status;
    DBSession* pDBSession = PSLNULL;
    PSLMSGPOOL mpHandle = PSLNULL;
    MTPDATASTORE mds = PSLNULL;

    if (PSLNULL == ppDBSession)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    *ppDBSession = PSLNULL;

    status = PSLMsgPoolCreate(DATABASE_MSG_POOL_SIZE, &mpHandle);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreCreate(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pDBSession = NEW DBSession(mds, mpHandle);
    if (PSLNULL == pDBSession)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    *ppDBSession = pDBSession;
    pDBSession = PSLNULL;
    mpHandle = PSLNULL;
    mds = PSLNULL;

Exit:
    SAFE_MTPDATASTOREDESTROY(mds);
    SAFE_PSLMSGPOOLDESTROY(mpHandle);
    if (pDBSession != PSLNULL)
    {
        pDBSession->Cleanup();
    }
    SAFE_RELEASE(pDBSession);

    return status;
}

PSLSTATUS DBSession::Cleanup()
{
    SAFE_MTPDATASTOREDESTROY(m_mds);
    SAFE_PSLMSGPOOLDESTROY(m_mpHandle);

    return PSLSUCCESS;
}

PSLSTATUS DBSession::GetDataStore(MTPDATASTORE* pmds)
{
    PSLSTATUS status;

    if (PSLNULL == pmds)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    *pmds = PSLNULL;

    if (PSLNULL == m_mds)
    {
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    *pmds = m_mds;

    status = PSLSUCCESS;

Exit:
    return status;
}

PSLSTATUS DBSession::GetMsgPool(PSLMSGPOOL* pmpHandle)
{
    PSLSTATUS status;

    if (PSLNULL == pmpHandle)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    *pmpHandle = PSLNULL;

    if (PSLNULL == m_mpHandle)
    {
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    *pmpHandle = m_mpHandle;

    status = PSLSUCCESS;

Exit:
    return status;
}

PSLSTATUS DBSession::SetObjectInfoHandleAndSize(
                        PSLUINT32 dwObjectHandle,
                        PSLUINT32 dwObjectSize)
{
    m_dwObjectInfoHandle = dwObjectHandle;
    m_dwObjectInfoSize = dwObjectSize;
    return PSLSUCCESS;
}

PSLSTATUS DBSession::GetObjectInfoHandleAndSize(
                        PSLUINT32* pdwObjectHandle,
                        PSLUINT32* pdwObjectSize)
{
    PSLSTATUS status;

    if (PSLNULL != pdwObjectHandle)
    {
        *pdwObjectHandle = MTP_OBJECTHANDLE_UNDEFINED;
    }

    if (PSLNULL != pdwObjectSize)
    {
        *pdwObjectSize = 0;
    }

    if (pdwObjectHandle == PSLNULL && pdwObjectSize == PSLNULL)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (PSLNULL != pdwObjectHandle)
    {
        *pdwObjectHandle = m_dwObjectInfoHandle;
    }

    if (PSLNULL != pdwObjectSize)
    {
        *pdwObjectSize = m_dwObjectInfoSize;
    }

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS DBSession::ResolveObjectHandleToStorageId(PSLUINT32 dwObjectHandle,
                                                    PSLUINT32* pdwStorageID)
{
    PSLSTATUS status;
    MTPDBVARIANTPROP mtpQuad = {0};
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;

    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = dwObjectHandle;
    status = MTPDataStoreQuery(m_mds, &mtpQuad, 1,
                               MTPDSITEMTYPE_OBJECT,
                               &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cItems);

    /* Get the store Id and from that the store */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

    status = MTPDataStoreGetProp(pItems[0], &mtpQuad, 1,
                              MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                             MTPDSDATAFORMAT_NONE),
                              (PSLBYTE*)pdwStorageID,
                              sizeof(PSLUINT32), PSLNULL, PSLNULL);

Exit:
    SAFE_PSLMEMFREE(pItems);
    return status;
}




DBContext::DBContext( DBSession* pDBSession )
    : m_pDBSession( pDBSession ),
    m_dwStorageID( MTP_STORAGEID_UNDEFINED ),
    m_dwObjectHandle( MTP_OBJECTHANDLE_UNDEFINED ),
    m_dwParentHandle( MTP_OBJECTHANDLE_UNDEFINED ),
    m_wFormatCode( MTP_FORMATCODE_UNDEFINED ),
    m_dwDepth(0),
    m_fInitialized(PSLFALSE),
    m_fResolved( PSLFALSE ),
    m_fCommitted(PSLFALSE),
    m_fPartiallyCommitted(PSLFALSE),
    m_pDataList(PSLNULL),
    m_queryPropQuads(PSLNULL),
    m_cQueryPropQuads(0),
    m_dwObjectHandleCopy(MTP_OBJECTHANDLE_UNDEFINED)
{
    PSLASSERT( m_pDBSession != PSLNULL );

    vtbl = GetDBContextVTable();
    SAFE_ADDREF(m_pDBSession);
}

DBContext::~DBContext()
{
    PSLUINT32 dwLen = 0;

    PSLDynListGetSize(m_pDataList, &dwLen);
    PSLASSERT(0 == dwLen);

    PSLASSERT(PSLNULL == m_pDataList);
    PSLASSERT(PSLNULL == m_queryPropQuads);
    PSLASSERT(PSLNULL == m_pDBSession);
}

PSLSTATUS DBContext::Create( DBSession* pDBSession,
                             DBContext** ppDBContext )
{
    PSLSTATUS status;
    DBContext* pDBContext = PSLNULL;

    if( ppDBContext != PSLNULL )
    {
        *ppDBContext = PSLNULL;
    }

    if( pDBSession == PSLNULL || ppDBContext == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pDBContext = NEW DBContext( pDBSession );
    if (PSLNULL == pDBContext)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = pDBContext->Initialize();
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *ppDBContext = pDBContext;
    pDBContext = PSLNULL;

    status = PSLSUCCESS;

Exit:
    if (pDBContext != PSLNULL)
    {
        pDBContext->Cleanup();
    }
    SAFE_RELEASE( pDBContext );

    return status;
}

PSLSTATUS DBContext::Initialize()
{
    PSLSTATUS status;

    status = PSLDynListCreate(0, PSLFALSE, MAX_NUM_DBCONTEXTDATA,
                              &m_pDataList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}

PSLSTATUS DBContext::Initialize(PSLUINT32 dwJoinType,
                         PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                         PSLUINT32 cmqPropList)
{
    dwJoinType;

    PSLSTATUS status;
    MTPDBVARIANTPROP* propQuadList = PSLNULL;
    DWORD dwIndex;

    /* pmqPropList can be PSLNULL and cmqPropList can be zero
     * If one is zero other should be zero as well
     */

    if ((PSLNULL == pmqPropList && 0 != cmqPropList) ||
        (0 == cmqPropList && PSLNULL != pmqPropList))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (PSLNULL != pmqPropList)
    {
        propQuadList = (MTPDBVARIANTPROP*)PSLMemAlloc(PSLMEM_PTR,
                                sizeof(MTPDBVARIANTPROP) * cmqPropList);
        if (PSLNULL == propQuadList)
        {
            status = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }
    }

    for (dwIndex = 0; dwIndex < cmqPropList; dwIndex++)
    {
        switch( pmqPropList[dwIndex].wPropCode )
        {
        case MTP_OBJECTPROPCODE_OBJECTHANDLE:

            PSLASSERT(pmqPropList[dwIndex].wDataType ==
                                        MTP_DATATYPE_UINT32);

            if( pmqPropList[dwIndex].vParam.valInt32 !=
                                            MTP_OBJECTHANDLE_ROOT)
            {
                m_dwObjectHandle = pmqPropList[0].vParam.valInt32;
            }

            break;

        case MTP_OBJECTPROPCODE_STORAGEID:

            PSLASSERT(pmqPropList[dwIndex].wDataType ==
                                            MTP_DATATYPE_UINT32 );

            m_dwStorageID = pmqPropList[dwIndex].vParam.valUInt32;

            break;

        case MTP_OBJECTPROPCODE_PARENT:

            PSLASSERT(pmqPropList[dwIndex].wDataType ==
                                            MTP_DATATYPE_UINT32 );

            m_dwParentHandle = pmqPropList[dwIndex].vParam.valUInt32;

            break;

        case MTP_OBJECTPROPCODE_OBJECTFORMAT:

            PSLASSERT(pmqPropList[dwIndex].wDataType ==
                                            MTP_DATATYPE_UINT16);

            m_wFormatCode = pmqPropList[dwIndex].vParam.valInt16;

            break;

        case MTP_OBJECTPROPCODE_DEPTH:

            PSLASSERT(pmqPropList[dwIndex].wDataType ==
                                            MTP_DATATYPE_UINT32);

            m_dwDepth = pmqPropList[dwIndex].vParam.valUInt32;

            break;

        default:
            PSLASSERT(!L"Unexpected MTPQUAD PropCode");

            PSLTraceError( "Unexpected MTPQUAD PropCode 0x%04x",
                            pmqPropList[dwIndex].wPropCode );
            break;
        }
        propQuadList[dwIndex] = pmqPropList[dwIndex];
    }

    SAFE_PSLMEMFREE(m_queryPropQuads);
    m_queryPropQuads = propQuadList;
    propQuadList = PSLNULL;

    m_cQueryPropQuads = cmqPropList;

    m_fInitialized = PSLTRUE;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMEMFREE(propQuadList);
    return status;
}

PSLSTATUS DBContext::Cleanup()
{
    PSLSTATUS status;

    PSLUINT32 dwLen = 0;
    DBContextData* pData;

    status = PSLDynListGetSize(m_pDataList, &dwLen);

    for( DWORD idx = 0; idx < dwLen; idx++ )
    {
        pData = PSLNULL;
        status |= PSLDynListGetItem(m_pDataList, idx, (PSLPARAM*)&pData);
        SAFE_RELEASE( pData );
    }

    status |= PSLDynListRemoveItem(m_pDataList, 0, dwLen);

    SAFE_PSLDYNLISTDESTROY(m_pDataList);
    SAFE_PSLMEMFREE(m_queryPropQuads);
    SAFE_RELEASE(m_pDBSession);

    return status;
}

PSLVOID DBContext::StashObjectHandle(PSLUINT32 dwObjectHandle)
{
    m_dwObjectHandleCopy = dwObjectHandle;
}

PSLSTATUS DBContext::GetDBSession( DBSession** ppDBSession )
{
    PSLSTATUS status;

    if( ppDBSession == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    *ppDBSession = PSLNULL;

    if( m_pDBSession == PSLNULL )
    {
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    *ppDBSession = m_pDBSession;
    (*ppDBSession)->AddRef();

    status = PSLSUCCESS;

Exit:

    return status;
}

PSLSTATUS DBContext::AppendContextData(DBContextData* pDBContextData)
{
    PSLSTATUS status;

    if( pDBContextData == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLDynListAddItem(m_pDataList, (PSLPARAM)pDBContextData,
                               PSLNULL);
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    pDBContextData->AddRef();

    status = PSLSUCCESS;

Exit:

    return status;
}

PSLSTATUS DBContext::Resolve()
{
    /* <TODO>
     * Need to run the query here with the
     * propquad list we have
     */

    m_fResolved = PSLTRUE;

    return PSLSUCCESS;
}

PSLSTATUS DBContext::Commit()
{
    PSLSTATUS status;

    DBContextData* pDBContextData = PSLNULL;
    PSLBOOL fGlobalCommit;
    PSLBOOL fCommitRequired;
    PSLUINT32 dwLen = 0;
    PSLUINT32 dwIndex;

    // Commit is usually called for non-Query-like data phases
    // it creates/appends to m_pCollection based on cached data

    // no ContextData? nothing to do

    status = PSLDynListGetSize(m_pDataList, &dwLen);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if( dwLen == 0 )
    {
        PSLTraceProgress( "DBContext no data phase commit." );

        PSLASSERT( m_fResolved == PSLFALSE );

        m_fResolved = PSLTRUE;

        status = PSLSUCCESS;

        goto Exit;
    }

    fGlobalCommit = PSLFALSE;
    for(dwIndex = 0; dwIndex < dwLen; dwIndex++)
    {
        status = PSLDynListGetItem(m_pDataList, dwIndex,
                                   (PSLPARAM*)&pDBContextData);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        // ObjectPropArrayData returns commit required
        // SendObjectInfo and SendObjectPropList return pNewItem

        status = pDBContextData->ResolveContext( &fCommitRequired );
        if( PSL_FAILED( status ))
        {
            goto Exit;
        }

        fGlobalCommit = fGlobalCommit | fCommitRequired;

        status = pDBContextData->Cleanup();
        SAFE_RELEASE(pDBContextData);
    }

    PSLDynListRemoveItem(m_pDataList, 0, dwLen);

    m_fResolved = PSLTRUE;

    m_fCommitted = PSLTRUE;
    m_fPartiallyCommitted = PSLFALSE;

    status = PSLSUCCESS;

Exit:
    return status;
}

PSLSTATUS DBContext::Delete()
{
    PSLSTATUS status;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 dwIndex;
    PSLBOOL fPartialDelete = PSLFALSE;

    PSLASSERT(PSLNULL != m_pDBSession);

    status = m_pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreQuery(mds, m_queryPropQuads,
                                m_cQueryPropQuads,
                                MTPDSITEMTYPE_OBJECT,
                                &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    for (dwIndex = 0; dwIndex < cItems; dwIndex++)
    {
        status = MTPDataStoreDelete(pItems[dwIndex]);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        fPartialDelete = PSLTRUE;
    }

    fPartialDelete = PSLFALSE;
Exit:
    status = (PSLFALSE == fPartialDelete) ? status :
                MTPERROR_DATABASE_PARTIAL_DELETION;
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS DBContext::Copy( DBContext* pDBContextSrc, MTPITEM** ppNewItem )
{
    PSLSTATUS status;

    MTPDBVARIANTPROP* pPropListSrc;
    PSLUINT32 cPropListSrc;
    MTPDBVARIANTPROP* pPropListDest;
    PSLUINT32 cPropListDest;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItemsSrc = PSLNULL;
    PSLUINT32 cItemsSrc;
    MTPITEM* pItemsDest = PSLNULL;
    PSLUINT32 cItemsDest;
    MTPITEM* pItemsParent = PSLNULL;
    PSLUINT32 cItemsParent;

    PSLUINT32 dwObjectHandleSrc, dwObjectHandleNew, dwParentHandle, dwStorageID;
    MTPDBVARIANTPROP mpq;
    MTPDBVARIANTPROP mpqInsert[2];
    PSLDYNLIST dlObjPropCodes = PSLNULL;
    PSLUINT32 cPropCodes;

    PSLUINT16 wFormatCode, wPropCode, wAssociationType;
    PSLBYTE* pbBuf = PSLNULL;
    PSLUINT32 cbBuf = 0;

    HRESULT hr;
    PSLUINT32 dwRet;

    PSLBYTE rgmstrSrcPath[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR szSrcPath[MAX_MTP_STRING_LEN];

    PSLBYTE rgmstrDestPath[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR szDestPath[MAX_MTP_STRING_LEN];

    MTPDBCONTEXT* pDBContextParent = PSLNULL;
    MTPDBCONTEXT* pDBContextChild = PSLNULL;
    MTPITEM* pItemsChildren = PSLNULL;
    PSLUINT32 cItemsChildren;


    if( pDBContextSrc == PSLNULL || PSLNULL == ppNewItem )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* Initiate a query */
    status = pDBContextSrc->Resolve();
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    PSLZeroMemory(&mpq, sizeof(mpq));
    PSLZeroMemory(&mpqInsert, sizeof(mpqInsert));

    // assume/TODO the source ObjectHandle was validated
    // assume/TODO the destination StorageID was validated,
    // assume/TODO the destination StorageID is not R/O and has space
    // assume/TODO the destination ParentHandle was validated

    // assume source DBSession and dest DBSession are the same
    // assume they're leading to the same DataStore

    status = pDBContextSrc->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // retrieve source DBContext QueryQuadList

    status = pDBContextSrc->GetQueryPropList(&pPropListSrc, &cPropListSrc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // retrieve source item list from source DBContext's QueryQuadList

    status = MTPDataStoreQuery(mds, pPropListSrc, cPropListSrc,
                               MTPDSITEMTYPE_OBJECT,
                               &pItemsSrc, &cItemsSrc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (1 != cItemsSrc)
    {
        PSLASSERT(PSLFALSE);
        status = MTPERROR_DATABASE_INVALID_HANDLE;
        goto Exit;
    }

    // for DEBUG purposes retrieve dest ObjectHandle

    status = MTPDataStoreGetProp(pItemsSrc[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_ITEMID),
                        (PSLBYTE*)&dwObjectHandleSrc,
                        sizeof(PSLUINT32), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // retrieve FilePath of source ObjectHandle

    status = MTPDataStoreGetProp(pItemsSrc[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_FILEPATH),
                        (PSLBYTE*)&rgmstrSrcPath[0],
                        sizeof(PSLWCHAR) * MAX_MTP_STRING_LEN,
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(szSrcPath, PSLARRAYSIZE(szSrcPath),
                      PSLNULL, (PCXMTPSTRING)&rgmstrSrcPath[0],
                      (rgmstrSrcPath[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // retrieve ObjectFormat of source ObjectHandle

    mpq.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpq.wDataType = MTP_DATATYPE_UINT32;
    mpq.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFORMAT;

    status = MTPDataStoreGetProp(pItemsSrc[0], &mpq, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&wFormatCode,
                        sizeof(PSLUINT16), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // retrieve Association type ObjectFormat of source ObjectHandle

    mpq.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpq.wDataType = MTP_DATATYPE_UINT32;
    mpq.vParam.valInt32 = MTP_OBJECTPROPCODE_ASSOCIATIONTYPE;

    status = MTPDataStoreGetProp(pItemsSrc[0], &mpq, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&wAssociationType,
                        sizeof(PSLUINT16), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // retrieve dest DBContext QueryQuadList

    status = GetQueryPropList(&pPropListDest, &cPropListDest);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (cPropListDest != 2)
    {
        status = MTPERROR_DATABASE_INVALID_PARENT;
        goto Exit;
    }

    if (pPropListDest[1].vParam.valUInt32 == MTP_OBJECTHANDLE_UNDEFINED ||
        pPropListDest[1].vParam.valUInt32 == MTP_OBJECTHANDLE_ROOT)
    {
        // it's the root we're moving to

        dwParentHandle = MTP_OBJECTHANDLE_ROOT;
        dwStorageID = pPropListDest[0].vParam.valUInt32;
    }
    else
    {
        // retrieve dest item list from dest DBContext's QueryQuadList

        // ParentHandle available with MTP_OBJECTPROPCODE_PARENT propcode
        // in QueryQuadList, need to change it to ObjectHandle
        // in order to retrieve its handle and not its children's

        mpq.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
        mpq.wDataType = MTP_DATATYPE_UINT32;
        mpq.vParam.valInt32 = pPropListDest[1].vParam.valUInt32;

        status = MTPDataStoreQuery(mds, &mpq, 1,
                                   MTPDSITEMTYPE_OBJECT,
                                   &pItemsParent, &cItemsParent);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        if (1 != cItemsParent)
        {
            PSLASSERT(PSLFALSE);
            status = MTPERROR_DATABASE_INVALID_HANDLE;
            goto Exit;
        }

        // retrieve dest ParentHandle from dest DBContext's QueryQuadList

        status = MTPDataStoreGetProp(pItemsParent[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_ITEMID),
                        (PSLBYTE*)&dwParentHandle,
                        sizeof(PSLUINT32), PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        // get StorageID of the destination ParentHandle

        mpq.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mpq.wDataType = MTP_DATATYPE_UINT32;
        mpq.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

        status = MTPDataStoreGetProp(pItemsParent[0], &mpq, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&dwStorageID,
                        sizeof(PSLUINT32), PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    PSLASSERT( dwStorageID != MTP_STORAGEID_UNDEFINED &&
               dwStorageID != MTP_STORAGEID_ALL );
    PSLASSERT( dwParentHandle != MTP_OBJECTHANDLE_UNDEFINED );

    mpqInsert[0].wPropCode = MTP_OBJECTPROPCODE_STORAGEID;
    mpqInsert[0].wDataType = MTP_DATATYPE_UINT32;
    mpqInsert[0].vParam.valUInt32 = dwStorageID;

    mpqInsert[1].wPropCode = MTP_OBJECTPROPCODE_PARENT;
    mpqInsert[1].wDataType = MTP_DATATYPE_UINT32;
    mpqInsert[1].vParam.valUInt32 = dwParentHandle;

    status = MTPDataStoreInsert(mds, mpqInsert, ARRAYSIZE(mpqInsert),
                                MTPDSITEMTYPE_OBJECT,
                                &pItemsDest, &cItemsDest);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT( cItemsDest == 1 );

    // for DEBUG purposes retrieve dest ObjectHandle

    status = MTPDataStoreGetProp(pItemsDest[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_ITEMID),
                        (PSLBYTE*)&dwObjectHandleNew,
                        sizeof(PSLUINT32), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // get the list of prop codes supported for the given format

    status = PSLDynListCreate(0, PSLFALSE, PSLDYNLIST_GROW_DEFAULT, &dlObjPropCodes);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBGetObjectPropCodes(wFormatCode, MTP_GROUPCODE_NONE, PSLFALSE,
                                   dlObjPropCodes);
     if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = PSLDynListGetSize(dlObjPropCodes, &cPropCodes);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // for each prop

    for(PSLUINT32 iPropIdx = 0; iPropIdx < cPropCodes; iPropIdx++)
    {
        status = PSLDynListGetItem(dlObjPropCodes, iPropIdx,
                                   (PSLPARAM*)&wPropCode);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        if (wPropCode == MTP_OBJECTPROPCODE_STORAGEID ||
            wPropCode == MTP_OBJECTPROPCODE_PARENT)
        {
            // don't overwrite with source values
            continue;
        }

        mpq.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mpq.wDataType = MTP_DATATYPE_UINT32;
        mpq.vParam.valInt32 = wPropCode;

        // get its size
        status = MTPDataStoreGetProp(pItemsSrc[0], &mpq, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        PSLNULL, 0, PSLNULL, &cbBuf);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        // allocate buffer
        SAFE_PSLMEMFREE(pbBuf);

        pbBuf = (PSLBYTE*)PSLMemAlloc(PSLMEM_PTR | PSLMEM_ZERO_INIT, cbBuf);
        if (pbBuf == PSLNULL)
        {
            status = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }

        // read src value
        status = MTPDataStoreGetProp(pItemsSrc[0], &mpq, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        pbBuf, cbBuf, PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        // write dest value
        status = MTPDataStoreSetProp(pItemsDest[0], &mpq, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                       MTPDSDATAFORMAT_NONE),
                        pbBuf, cbBuf);
        if (PSL_FAILED(status))
        {
            PSLASSERT(!"Could not set prop");
        }
    }

    // update Filepath of dest ObjectHandle with NewPath

    status = _MTPSetFilePath(mds, pItemsDest[0]);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // retrieve dest Filepath

    status = MTPDataStoreGetProp(pItemsDest[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_FILEPATH),
                        (PSLBYTE*)&rgmstrDestPath[0],
                        sizeof(PSLWCHAR) * MAX_MTP_STRING_LEN,
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(szDestPath, PSLARRAYSIZE(szDestPath),
                      PSLNULL, (PCXMTPSTRING)&rgmstrDestPath[0],
                      (rgmstrDestPath[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (MTP_ASSOCIATIONTYPE_GENERICFOLDER == wAssociationType)
    {
        // create the directory copy
        dwRet = CreateDirectory((LPCWSTR)szDestPath, PSLNULL);

        // now recursively copy the children
        // create the destination context for all children
        mpqInsert[0].wPropCode = MTP_OBJECTPROPCODE_STORAGEID;
        mpqInsert[0].wDataType = MTP_DATATYPE_UINT32;
        mpqInsert[0].vParam.valUInt32 = dwStorageID;

        mpqInsert[1].wPropCode = MTP_OBJECTPROPCODE_PARENT;
        mpqInsert[1].wDataType = MTP_DATATYPE_UINT32;
        mpqInsert[1].vParam.valUInt32 = dwObjectHandleNew;

        SAFE_MTPDBCONTEXTCLOSE(pDBContextParent);

        status = MTPDBSessionDBContextOpen((MTPDBSESSIONOBJ*)m_pDBSession,
                                           MTPJOINTYPE_AND, mpqInsert, 2,
                                           (MTPDBCONTEXT*)&pDBContextParent);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        // query for source ObjectHandle children

        mpq.wPropCode = MTP_OBJECTPROPCODE_PARENT;
        mpq.wDataType = MTP_DATATYPE_UINT32;
        mpq.vParam.valUInt32 = dwObjectHandleSrc;

        status = MTPDataStoreQuery(mds, &mpq, 1, MTPDSITEMTYPE_OBJECT,
                                   &pItemsChildren, &cItemsChildren);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        for(PSLUINT32 iChildIdx = 0; iChildIdx < cItemsChildren; iChildIdx++)
        {
            PSLUINT32 dwObjectHandleChild;

            status = MTPDataStoreGetProp(pItemsChildren[iChildIdx],
                        PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_ITEMID),
                        (PSLBYTE*)&dwObjectHandleChild,
                        sizeof(PSLUINT32), PSLNULL, PSLNULL);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }

            mpq.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
            mpq.wDataType = MTP_DATATYPE_UINT32;
            mpq.vParam.valInt32 = dwObjectHandleChild;

            SAFE_MTPDBCONTEXTCLOSE(pDBContextChild);

            status = MTPDBSessionDBContextOpen((MTPDBSESSIONOBJ*)m_pDBSession,
                                               MTPJOINTYPE_AND, &mpq, 1,
                                               (MTPDBCONTEXT*)&pDBContextChild);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }

            status = MTPDBContextCopy(pDBContextParent, pDBContextChild);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
        }
    }
    else
    {
        // CopyFile on the FS (OldPath -> NewPath)
        dwRet = CopyFile((LPCWSTR)szSrcPath, (LPCWSTR)szDestPath, TRUE);
    }

    if (0 == dwRet)
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        status = PSLSTATUS_FROM_HRESULT(hr);
        if( PSL_FAILED( status ))
        {
            goto Exit;
        }
    }

    *ppNewItem = pItemsDest;
    pItemsDest = PSLNULL;

    m_dwObjectHandleCopy = dwObjectHandleNew;

    status = PSLSUCCESS;

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pItemsSrc);
    SAFE_PSLMEMFREE(pItemsParent);
    SAFE_PSLMEMFREE(pItemsDest);
    SAFE_PSLDYNLISTDESTROY(dlObjPropCodes);
    SAFE_PSLMEMFREE(pbBuf);
    SAFE_MTPDBCONTEXTCLOSE(pDBContextParent);
    SAFE_MTPDBCONTEXTCLOSE(pDBContextChild);
    SAFE_PSLMEMFREE(pItemsChildren);

    return status;
}

PSLSTATUS DBContext::Move( DBContext* pDBContextSrc )
{
    PSLSTATUS status;

    MTPDBVARIANTPROP* pPropListSrc;
    PSLUINT32 cPropListSrc;
    MTPDBVARIANTPROP* pPropListDest;
    PSLUINT32 cPropListDest;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItemsSrc = PSLNULL;
    PSLUINT32 cItemsSrc;
    MTPITEM* pItemsDest = PSLNULL;
    PSLUINT32 cItemsDest;

    PSLUINT32 dwParentHandle, dwStorageID;
    MTPDBVARIANTPROP mpq;

    HRESULT hr;

    PSLBYTE rgmstrOldPath[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR szOldPath[MAX_MTP_STRING_LEN];

    PSLBYTE rgmstrNewPath[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR szNewPath[MAX_MTP_STRING_LEN];

    if( pDBContextSrc == PSLNULL)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = pDBContextSrc->Resolve();
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    // assume/TODO the source ObjectHandle was validated
    // assume/TODO the destination StorageID was validated,
    // assume/TODO the destination StorageID is not R/O and has space
    // assume/TODO the destination ParentHandle was validated

    // assume source DBSession and dest DBSession are the same
    // assume they're leading to the same DataStore

    status = pDBContextSrc->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // retrieve source DBContext QueryQuadList

    status = pDBContextSrc->GetQueryPropList(&pPropListSrc, &cPropListSrc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // retrieve source item list from source DBContext's QueryQuadList

    status = MTPDataStoreQuery(mds, pPropListSrc, cPropListSrc,
                               MTPDSITEMTYPE_OBJECT,
                               &pItemsSrc, &cItemsSrc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (1 != cItemsSrc)
    {
        PSLASSERT(PSLFALSE);
        status = MTPERROR_DATABASE_INVALID_HANDLE;
        goto Exit;
    }

    // retrieve Filename and FilePath of source ObjectHandle
    // OldPath = source Filepath

    status = MTPDataStoreGetProp(pItemsSrc[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_FILEPATH),
                        (PSLBYTE*)&rgmstrOldPath[0],
                        sizeof(PSLWCHAR) * MAX_MTP_STRING_LEN,
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(szOldPath, PSLARRAYSIZE(szOldPath),
                      PSLNULL, (PCXMTPSTRING)&rgmstrOldPath[0],
                      (rgmstrOldPath[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // retrieve dest DBContext QueryQuadList

    status = GetQueryPropList(&pPropListDest, &cPropListDest);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (cPropListDest != 2)
    {
        status = MTPERROR_DATABASE_INVALID_PARENT;
        goto Exit;
    }

    if ((PSLUINT32)pPropListDest[1].vParam.valUInt32 == MTP_OBJECTHANDLE_UNDEFINED ||
        (PSLUINT32)pPropListDest[1].vParam.valUInt32 == MTP_OBJECTHANDLE_ROOT)
    {
        // it's the root we're moving to

        dwParentHandle = MTP_OBJECTHANDLE_ROOT;
        dwStorageID = (PSLUINT32)pPropListDest[0].vParam.valUInt32;
    }
    else
    {
        // retrieve dest item list from dest DBContext's QueryQuadList

        // ParentHandle available wit MTP_OBJECTPROPCODE_PARENT
        // in QueryQuadList, need to change it to ObjectHandle
        // in order to retrieve its handle and not its children's

        mpq.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
        mpq.wDataType = MTP_DATATYPE_UINT32;
        mpq.vParam.valInt32 = pPropListDest[1].vParam.valUInt32;

        status = MTPDataStoreQuery(mds, &mpq, 1,
                                   MTPDSITEMTYPE_OBJECT,
                                   &pItemsDest, &cItemsDest);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        if (1 != cItemsDest)
        {
            PSLASSERT(PSLFALSE);
            status = MTPERROR_DATABASE_INVALID_HANDLE;
            goto Exit;
        }

        // retrieve dest ParentHandle from dest DBContext's QueryQuadList

        status = MTPDataStoreGetProp(pItemsDest[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_ITEMID),
                        (PSLBYTE*)&dwParentHandle,
                        sizeof(PSLUINT32), PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        // get StorageID of the destination ParentHandle

        mpq.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mpq.wDataType = MTP_DATATYPE_UINT32;
        mpq.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

        status = MTPDataStoreGetProp(pItemsDest[0], &mpq, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&dwStorageID,
                        sizeof(PSLUINT32), PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    PSLASSERT( dwStorageID != MTP_STORAGEID_UNDEFINED &&
               dwStorageID != MTP_STORAGEID_ALL );
    PSLASSERT( dwParentHandle != MTP_OBJECTHANDLE_UNDEFINED );

    // TODO
    // if source ObjectHandle is of ASSOCIATION type
    //   if it has children
    //     return "cannot move hierarchy error"
    //   endif
    // endif

    // update ParentHandle of source ObjectHandle with dest ParentHandle

    mpq.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpq.wDataType = MTP_DATATYPE_UINT32;
    mpq.vParam.valInt32 = MTP_OBJECTPROPCODE_PARENT;

    status = MTPDataStoreSetProp(pItemsSrc[0], &mpq, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&dwParentHandle, sizeof(PSLUINT32));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // update StorageID of source ObjectHandle with dest StorageID

    mpq.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpq.wDataType = MTP_DATATYPE_UINT32;
    mpq.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

    status = MTPDataStoreSetProp(pItemsSrc[0], &mpq, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_FORCEUPDATE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&dwStorageID, sizeof(PSLUINT32));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // update Filepath of source ObjectHandle with NewPath

    status = _MTPSetFilePath(mds, pItemsSrc[0]);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // retrieve new source Filepath

    status = MTPDataStoreGetProp(pItemsSrc[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_FILEPATH),
                        (PSLBYTE*)&rgmstrNewPath[0],
                        sizeof(PSLWCHAR) * MAX_MTP_STRING_LEN,
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(szNewPath, PSLARRAYSIZE(szNewPath),
                      PSLNULL, (PCXMTPSTRING)&rgmstrNewPath[0],
                      (rgmstrNewPath[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // MoveFile on the FS (OldPath -> NewPath)

    if( !MoveFile((LPCWSTR)szOldPath, (LPCWSTR)szNewPath))
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        status = PSLSTATUS_FROM_HRESULT(hr);
        if( PSL_FAILED( status ))
        {
            goto Exit;
        }
    }

    status = PSLSUCCESS;

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pItemsSrc);
    SAFE_PSLMEMFREE(pItemsDest);
    return status;
}

PSLSTATUS DBContext::GetItemHandle( MTPITEM** ppmItem,
                                    PSLUINT32* pdwObjectHandle,
                                    PSLUINT32* pdwParentHandle,
                                    PSLUINT32* pdwStorageID)
{
    PSLSTATUS status;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 dwObjHandle = MTP_OBJECTHANDLE_UNDEFINED;
    MTPDATASTORE mds;
    MTPDBVARIANTPROP mtpQuad = {0};

    PSLUINT32 dwStorageID = MTP_STORAGEID_UNDEFINED;
    PSLUINT32 dwParentHandle = MTP_STORAGEID_UNDEFINED;

    if( ppmItem != PSLNULL )
    {
        *ppmItem = PSLNULL;
    }

    if( pdwObjectHandle != PSLNULL )
    {
        *pdwObjectHandle = dwObjHandle;
    }

    if( pdwParentHandle != PSLNULL )
    {
        *pdwParentHandle = dwParentHandle;
    }

    if( pdwStorageID != PSLNULL )
    {
        *pdwStorageID = dwStorageID;
    }

    // at least one piece of info should be requested
    if( ppmItem == PSLNULL && pdwObjectHandle == PSLNULL &&
        pdwObjectHandle == PSLNULL && pdwStorageID == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* Get the object handle stahsed in the context */
    dwObjHandle = m_dwObjectHandleCopy;

    PSLASSERT(MTP_OBJECTHANDLE_UNDEFINED != dwObjHandle);
    if (MTP_OBJECTHANDLE_UNDEFINED == dwObjHandle)
    {
        status = MTPERROR_DATABASE_INVALID_HANDLE;
        goto Exit;
    }

    status = m_pDBSession->GetDataStore(&mds);

    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = dwObjHandle;

    status = MTPDataStoreQuery(mds, &mtpQuad, 1,
                            MTPDSITEMTYPE_OBJECT,
                            &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cItems);

    /* Storage Id*/
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

    status = MTPDataStoreGetProp(pItems[0], &mtpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&dwStorageID, sizeof(PSLUINT32),
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Parent Handle */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PARENT;

    status = MTPDataStoreGetProp(pItems[0], &mtpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&dwParentHandle, sizeof(PSLUINT32),
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (PSLNULL != pdwObjectHandle)
    {
        *pdwObjectHandle = dwObjHandle;
    }

    if (PSLNULL != pdwParentHandle)
    {
        *pdwParentHandle = dwParentHandle;
    }

    if (PSLNULL != pdwStorageID)
    {
        *pdwStorageID = dwStorageID;
    }

    if (PSLNULL != ppmItem)
    {
        *ppmItem = pItems;
        pItems = PSLNULL;
    }

    status = PSLSUCCESS;

Exit:
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS DBContext::GetHandleCount(PSLUINT32* pdwEntries)
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;

    if (PSLNULL != pdwEntries)
    {
        *pdwEntries = 0;
    }

    if (PSLNULL == pdwEntries)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreQuery(mds, pPropList, cPropList,
                               MTPDSITEMTYPE_OBJECT,
                               &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* copy the count of items */
    MTPStoreUInt32(pdwEntries, cItems);

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS DBContext::GetQueryPropList(MTPDBVARIANTPROP** ppPropList,
                                      PSLUINT32 *pcPropList)
{
    PSLSTATUS status;

    if (PSLNULL != ppPropList)
    {
        *ppPropList = PSLNULL;
    }

    if (PSLNULL != pcPropList)
    {
        *pcPropList = 0;
    }

    if (PSLNULL == ppPropList || PSLNULL == pcPropList)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    *ppPropList = m_queryPropQuads;
    *pcPropList = m_cQueryPropQuads;

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS _MTPSetFilePath(MTPDATASTORE mds, MTPITEM mtpItem)
{
    PSLSTATUS status;
    PSLUINT32 dwLen;

    MTPDBVARIANTPROP mpQuad = {0};

    PSLWCHAR szFilePath[MAX_PATH] = {0};
    WCHAR szFilePathFmt[] = L"%s\\%s";
    WCHAR szFileFmt[] = L"%x";
    PSLWCHAR szFileName[MAX_PATH] = {0};

    PSLBYTE rgmstrFileName[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR rgFileName[MAX_MTP_STRING_LEN];

    PSLUINT32 dwParentHandle;
    PSLUINT32 dwObjectHandle;
    MTPITEM* pParentItems = PSLNULL;
    PSLUINT32 dwStoreId;
    MTPITEM* pStoreItems = PSLNULL;
    PSLUINT32 cItems;

    PSLBYTE rgmstrParentPath[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR rgParentPath[MAX_MTP_STRING_LEN];

    HRESULT hr;

    if (PSLNULL == mtpItem)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* File Path*/
    /* <TODO>
     * In this case MTPObjectGetProp should have been
     * called with a flag (fToWire == PSLFLASE) that
     * would tell it not to swap byte order in which
     * case remember to pass PSLWCHAR* instead of PXMTPSTRING
     */
    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFILENAME;

    status = MTPDataStoreGetProp(mtpItem, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&rgmstrFileName[0],
                        sizeof(PSLWCHAR) * MAX_MTP_STRING_LEN,
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(rgFileName, PSLARRAYSIZE(rgFileName),
                      PSLNULL, (PCXMTPSTRING)&rgmstrFileName[0],
                      (rgmstrFileName[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* ObjectHandle*/
    status = MTPDataStoreGetProp(mtpItem, PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_ITEMID),
                        (PSLBYTE*)&dwObjectHandle,
                        sizeof(PSLUINT32),
                        PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    hr = StringCchPrintfW((WCHAR*)&szFileName[0], ARRAYSIZE(szFileName),
                          szFileFmt, dwObjectHandle);
    if (FAILED(hr))
    {
        status = hr;
        goto Exit;
    }

    status = PSLStringCat(&szFileName[0], ARRAYSIZE(szFileName),
                    (PSLWCHAR*)FindFileExtension((WCHAR*)rgFileName));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mpQuad.wDataType = MTP_DATATYPE_UINT32;
    mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PARENT;

    status = MTPDataStoreGetProp(mtpItem, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&dwParentHandle,
                        sizeof(PSLUINT32), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(MTP_OBJECTHANDLE_UNDEFINED != dwParentHandle);
    if (MTP_OBJECTHANDLE_UNDEFINED == dwParentHandle)
    {
        status = MTPERROR_DATABASE_INVALID_HANDLE;
        goto Exit;
    }

    if (MTP_OBJECTHANDLE_ROOT == dwParentHandle)
    {
        /* If parent root get the store and ask
         * for it path instead of parent path
         */
        mpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mpQuad.wDataType = MTP_DATATYPE_UINT32;
        mpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_STORAGEID;

        status = MTPDataStoreGetProp(mtpItem, &mpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&dwStoreId,
                        sizeof(PSLUINT32), PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            PSLASSERT(PSLFALSE);
            goto Exit;
        }

        /* query store object */
        mpQuad.wPropCode = MTP_OBJECTPROPCODE_STORAGEID;
        mpQuad.wDataType = MTP_DATATYPE_UINT32;
        mpQuad.vParam.valInt32 = (PSLPARAM)dwStoreId;

        status = MTPDataStoreQuery(mds, &mpQuad, 1,
                                   MTPDSITEMTYPE_STORE,
                                   &pStoreItems, &cItems);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        PSLASSERT(1 == cItems);

        /* file path */
        status = MTPDataStoreGetProp(pStoreItems[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_FILEPATH),
                        (PSLBYTE*)&rgmstrParentPath[0],
                        sizeof(PSLWCHAR) * MAX_MTP_STRING_LEN,
                        PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            PSLASSERT(PSLFALSE);
            goto Exit;
        }

        status = MTPLoadMTPString(rgParentPath, PSLARRAYSIZE(rgParentPath),
                          PSLNULL, (PCXMTPSTRING)&rgmstrParentPath[0],
                          (rgmstrParentPath[0] * sizeof(PSLWCHAR))+ 1);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }
    else
    {
        /* query parent object */
        mpQuad.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
        mpQuad.wDataType = MTP_DATATYPE_UINT32;
        mpQuad.vParam.valInt32 = dwParentHandle;

        status = MTPDataStoreQuery(mds, &mpQuad, 1,
                                   MTPDSITEMTYPE_OBJECT,
                                   &pParentItems, &cItems);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        PSLASSERT(1 == cItems);

        /* <TODO>
         * In this case MTPObjectGetProp should have been
         * called with a flag (fToWire == PSLFLASE) that
         * would tell it not to swap byte order.
         */
        status = MTPDataStoreGetProp(pParentItems[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_FILEPATH),
                        (PSLBYTE*)&rgmstrParentPath[0],
                        sizeof(PSLWCHAR) * MAX_MTP_STRING_LEN,
                        PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        status = MTPLoadMTPString(rgParentPath, PSLARRAYSIZE(rgParentPath),
                          PSLNULL, (PCXMTPSTRING)&rgmstrParentPath[0],
                          (rgmstrParentPath[0] * sizeof(PSLWCHAR))+ 1);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    hr = StringCchPrintfW((WCHAR*)szFilePath, MAX_PATH, szFilePathFmt,
                          (WCHAR*)rgParentPath, (WCHAR*)szFileName);
    if(FAILED(hr))
    {
        status = hr;
        goto Exit;
    }

    status = PSLStringLen((PSLWCHAR*)szFilePath, MAX_PATH, &dwLen);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* File Path (No swapping needed)*/
    status = MTPDataStoreSetProp(mtpItem, PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_FILEPATH),
                        (PSLBYTE*)szFilePath,
                        (dwLen + 1) * sizeof(PSLWCHAR));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
Exit:
    SAFE_PSLMEMFREE(pStoreItems);
    SAFE_PSLMEMFREE(pParentItems);
    return status;
}
