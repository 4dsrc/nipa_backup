/*
 *  DBContextData.cpp
 *
 *  Contains definitions of various DBcontextData classes
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPDBHandlerPrecomp.h"
#include "FileUtils.h"
#include "DBContextWin.h"
#include "DBContextDataWin.h"
#include "MTPDatabaseUtil.h"
#include "MTPObjectProperty.h"
#include "MTPDBContextData.h"

PSLSTATUS _RemoveDirectoryRecur(LPCWSTR wszDirPath);

DBContextData::DBContextData( DBContext* pDBContext,
                              PSLUINT32 dwDataFormat )
 : m_pDBContext( pDBContext ), m_pbBuf(PSLNULL),
   m_cbBuf(MTPDB_DATASIZE_UNKNOWN), m_qwOffset(0),
   m_dwDataFormat(dwDataFormat),
   m_pOutPropQuads(PSLNULL),
   m_cOutPropQuads(0)
{
    PSLASSERT( m_pDBContext != NULL );

    vtbl = GetDBContextDataVTable();

    if( m_pDBContext != NULL )
    {
        m_pDBContext->AddRef();
    }
}

DBContextData::~DBContextData()
{
    PSLASSERT(PSLNULL == m_pDBContext);
    PSLASSERT(PSLNULL == m_pbBuf);
    PSLASSERT(PSLNULL == m_pOutPropQuads);
}

PSLSTATUS DBContextData::Create(DBContext* pDBContext,
                                PSLUINT32 dwDataFormat,
                                PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                                PSLUINT32 cmqPropList,
                                DBContextData** ppDBContextData )
{
    PSLSTATUS status;
    DBContextData* pDBContextData = NULL;

    if( ppDBContextData != NULL )
    {
        *ppDBContextData = NULL;
    }

    if( pDBContext == NULL || dwDataFormat == 0 ||
        ppDBContextData == NULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    switch (MTPDBCONTEXTDATADESC_FORMAT(dwDataFormat))
    {
    case MTPDBCONTEXTDATAFORMAT_STORAGE:

        switch( MTPDBCONTEXTDATA_FLAGS_MASK &
                MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
        case MTPDBCONTEXTDATA_QUERY:    /* GetStorageInfo */
        case MTPDBCONTEXTDATA_UPDATE:   /* FormatStorage */
            break;
        case MTPDBCONTEXTDATA_INSERT:   /* n/a */
        default:
            status = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }

        pDBContextData = NEW StorageInfoData( pDBContext, dwDataFormat);
        break;

    case MTPDBCONTEXTDATAFORMAT_STORAGEID:

        switch( MTPDBCONTEXTDATA_FLAGS_MASK &
                MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
        case MTPDBCONTEXTDATA_QUERY:    /* GetStorageIDs */
            break;
        case MTPDBCONTEXTDATA_UPDATE:   /* n/a */
        case MTPDBCONTEXTDATA_INSERT:
        default:
            status = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }

        pDBContextData = NEW StorageInfoData( pDBContext, dwDataFormat);
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTHANDLE:

        switch( MTPDBCONTEXTDATA_FLAGS_MASK &
                MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
        case MTPDBCONTEXTDATA_QUERY:    /* GetObjectHandles */
        case MTPDBCONTEXTDATA_UPDATE:   /* Get/SetObjectReferences */
            break;

        case MTPDBCONTEXTDATA_INSERT:   /* n/a */
        default:
            status = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }

        pDBContextData = NEW ObjectHandleData(pDBContext, dwDataFormat);
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTPROTECTION:

        switch( MTPDBCONTEXTDATA_FLAGS_MASK &
                MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
        case MTPDBCONTEXTDATA_UPDATE:   /* SetProtectionStatus */
            break;
        case MTPDBCONTEXTDATA_QUERY:    /* n/a */
        case MTPDBCONTEXTDATA_INSERT:
        default:
            status = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }

        pDBContextData = NEW ObjectPropListData( pDBContext, dwDataFormat );
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPARRAY:

        switch( MTPDBCONTEXTDATA_FLAGS_MASK &
                MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
        case MTPDBCONTEXTDATA_QUERY:    /* GetStorageIDs */
                                        /* GetObjectPropValue */
                                        /* GetObjectReferences */
        case MTPDBCONTEXTDATA_UPDATE:   /* SetObjectPropValue */
                                        /* SetObjectReferences */
            break;
        case MTPDBCONTEXTDATA_INSERT:   /* n/a */
        default:
            status = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }

        pDBContextData = NEW ObjectPropArrayData( pDBContext,
                                                  dwDataFormat );
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTINFO:

        switch( MTPDBCONTEXTDATA_FLAGS_MASK &
                MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
        case MTPDBCONTEXTDATA_QUERY:    /* GetObjectInfo */
        case MTPDBCONTEXTDATA_INSERT:   /* SendObjectInfo */
            break;
        case MTPDBCONTEXTDATA_UPDATE:   /* n/a */
        default:
            status = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }

        pDBContextData = NEW ObjectInfoData( pDBContext, dwDataFormat );
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPLIST:

        switch( MTPDBCONTEXTDATA_FLAGS_MASK &
                MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
        case MTPDBCONTEXTDATA_QUERY:    /* GetObjectPropList */
        case MTPDBCONTEXTDATA_INSERT:   /* SendObjectPropList */
        case MTPDBCONTEXTDATA_UPDATE:   /* SetObjectPropList */
            break;
        default:
            status = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }

        pDBContextData = NEW ObjectPropListData( pDBContext,
                                                 dwDataFormat );
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA:

        switch( MTPDBCONTEXTDATA_FLAGS_MASK &
                MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
        case MTPDBCONTEXTDATA_QUERY:    /* GetObject */
        case MTPDBCONTEXTDATA_UPDATE:   /* SendObject */
            break;
        case MTPDBCONTEXTDATA_INSERT:   /* n/a */
        default:
            status = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }

        pDBContextData = NEW ObjectBinaryData(pDBContext, dwDataFormat);
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTTHUMBDATA:

        switch( MTPDBCONTEXTDATA_FLAGS_MASK &
                MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
        case MTPDBCONTEXTDATA_QUERY:    /* GetThumb */
            break;
        case MTPDBCONTEXTDATA_UPDATE:   /* n/a */
        case MTPDBCONTEXTDATA_INSERT:   /* n/a */
        default:
            status = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }

        pDBContextData = NEW ObjectThumbData( pDBContext, dwDataFormat);
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPDESC:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPSSUPPORTED:
        {
            switch( MTPDBCONTEXTDATA_FLAGS_MASK &
                    MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
            {
            case MTPDBCONTEXTDATA_QUERY:  /* GetObjectPropDesc */
                                          /* GetObjectPropsSupported */
                break;
            case MTPDBCONTEXTDATA_UPDATE:   /* n/a */
            case MTPDBCONTEXTDATA_INSERT:
            default:
                status = PSLERROR_NOT_IMPLEMENTED;
                goto Exit;
            }

            pDBContextData = NEW ObjectPropData(pDBContext,
                                                dwDataFormat);
        }
        break;

    default:
        /* Should never get here */

        PSLASSERT(PSLFALSE);
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if ( NULL == pDBContextData )
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = pDBContextData->Initialize(pmqPropList, cmqPropList);
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    status = pDBContextData->StashPropList(pmqPropList,cmqPropList);
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    *ppDBContextData = pDBContextData;
    pDBContextData = NULL;

    status = PSLSUCCESS;

Exit:
    if (pDBContextData != PSLNULL)
    {
        pDBContextData->Cleanup();
    }
    SAFE_RELEASE( pDBContextData );
    return status;
}

PSLSTATUS DBContextData::Cleanup()
{
    SAFE_RELEASE( m_pDBContext );
    SAFE_PSLMEMFREE( m_pbBuf );
    SAFE_PSLMEMFREE(m_pOutPropQuads);

    return PSLSUCCESS;
}

PSLSTATUS DBContextData::Terminate()
{
    return PSLSUCCESS;
}

PSLSTATUS DBContextData::GetDBContext( DBContext** ppDBContext )
{
    PSLSTATUS status;

    if( ppDBContext == NULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    *ppDBContext = NULL;

    if( m_pDBContext == NULL )
    {
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    *ppDBContext = m_pDBContext;
    (*ppDBContext)->AddRef();

    status = PSLSUCCESS;

Exit:

    return status;
}

PSLSTATUS DBContextData::GetPackedSize( PSLUINT64 * pqwSize)
{
    PSLSTATUS status;

    if( pqwSize == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = Pack( &m_pbBuf, &m_cbBuf );
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    *pqwSize = m_cbBuf;

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS DBContextData::Serialize(PSLVOID* pvBuf,
                                   PSLUINT32 cbBuf,
                                   PSLUINT32* pcbWritten,
                                   PSLUINT64* pcbLeft)
{
    PSLSTATUS status;
    PSLUINT32 cbToXfer;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    if (PSLNULL == pvBuf || PSLNULL == pcbWritten || PSLNULL == pcbLeft)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (m_cbBuf <= m_qwOffset)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    cbToXfer = min(cbBuf, m_cbBuf - m_qwOffset);

    PSLASSERT( cbToXfer <= cbBuf );

    switch (MTPDBCONTEXTDATADESC_FORMAT(m_dwDataFormat))
    {
    case MTPDBCONTEXTDATAFORMAT_STORAGE:
    case MTPDBCONTEXTDATAFORMAT_STORAGEID:
    case MTPDBCONTEXTDATAFORMAT_OBJECTHANDLE:
    case MTPDBCONTEXTDATAFORMAT_OBJECTINFO:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPLIST:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPARRAY:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPSSUPPORTED:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPDESC:

        PSLCopyMemory( pvBuf, m_pbBuf + m_qwOffset, cbToXfer );
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTPROTECTION:

        PSLASSERT( !"It is not supposed to have a data phase" );
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA:

        PSLASSERT( !"Overriden in derived class" );
        break;

    default:
        /* Should never get here */

        PSLASSERT(PSLFALSE);
        status = PSLERROR_INVALID_PARAMETER;
        break;
    }

    /* this releases the buffer when entirely consumed */

    SetOffset(m_qwOffset + cbToXfer);

    *pcbWritten = cbToXfer;

    PSLASSERT( m_cbBuf >= m_qwOffset);

    *pcbLeft = m_cbBuf - m_qwOffset;

    status = PSLSUCCESS;

Exit:
    return status;
}

PSLSTATUS DBContextData::Deserialize( PSL_CONST PSLVOID* pvBuf,
                                      PSLUINT32 cbBuf)
{
    PSLSTATUS status;

    if (PSLNULL == pvBuf || PSLNULL == cbBuf )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (m_cbBuf == MTPDB_DATASIZE_UNKNOWN || m_cbBuf < m_qwOffset + cbBuf)
    {
        m_pbBuf = (PSLBYTE*)PSLMemReAlloc(
                                m_pbBuf,
                                m_qwOffset + cbBuf,
                                (PSLMEM_FIXED | PSLMEM_RELOCATABLE));
        if( m_pbBuf == PSLNULL )
        {
            status = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }

        m_cbBuf = m_qwOffset + cbBuf;

        PSLTraceDetail( "DBContextData buffer realloc [0x%08x]",
                        m_qwOffset + cbBuf );
    }

    switch (MTPDBCONTEXTDATADESC_FORMAT(m_dwDataFormat))
    {
    case MTPDBCONTEXTDATAFORMAT_STORAGE:
    case MTPDBCONTEXTDATAFORMAT_OBJECTHANDLE:
    case MTPDBCONTEXTDATAFORMAT_OBJECTINFO:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPLIST:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPARRAY:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROTECTION:

        PSLCopyMemory( m_pbBuf + m_qwOffset, pvBuf, cbBuf );
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA:

        PSLASSERT( !"Overriden in devrived class" );
        break;

    default:
        /* Should never get here */

        PSLASSERT(PSLFALSE);
        status = PSLERROR_INVALID_PARAMETER;
        break;
    }

    /* this releases the buffer when entirely consumed */

    // Do not use SetOffset for it frees up the buffer
    m_qwOffset += cbBuf;

    status = PSLSUCCESS;

Exit:
    return status;
}

void DBContextData::SetOffset(PSLUINT32 qwOffset)
{
    PSLASSERT( qwOffset <= m_cbBuf );

    m_qwOffset = qwOffset;

    if( m_qwOffset == m_cbBuf )
    {
        SAFE_PSLMEMFREE(m_pbBuf);
    }
}

PSLSTATUS DBContextData::ResolveContext( PSLBOOL* pfCommitRequired)
{
    if( pfCommitRequired != PSLNULL )
    {
        *pfCommitRequired = PSLFALSE;
    }

    PSLASSERT(PSLFALSE);
    // no base implementation
    return PSLERROR_NOT_IMPLEMENTED;
}

PSLSTATUS DBContextData::StashPropList(
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList)
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropQuads = PSLNULL;
    PSLUINT32 dwIndex;

    /* pmqPropList can be PSLNULL and cmqPropList can be zero
     * If one is zero other should be zero as well
     */

    if ((PSLNULL == pmqPropList && 0 != cmqPropList) ||
        (0 == cmqPropList && PSLNULL != pmqPropList))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (PSLNULL != pmqPropList)
    {
        pPropQuads = (MTPDBVARIANTPROP*)PSLMemAlloc(PSLMEM_PTR,
                            sizeof(MTPDBVARIANTPROP) * cmqPropList);
        if (PSLNULL == pPropQuads)
        {
            status = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }
        for (dwIndex = 0; dwIndex < cmqPropList; dwIndex++ )
        {
            pPropQuads[dwIndex] = pmqPropList[dwIndex];
        }
    }

    SAFE_PSLMEMFREE(m_pOutPropQuads);
    m_pOutPropQuads = pPropQuads;
    pPropQuads = PSLNULL;

    m_cOutPropQuads = cmqPropList;

    status = PSLSUCCESS;

Exit:
    SAFE_PSLMEMFREE(pPropQuads);
    return status;
}

PSLSTATUS DBContextData::GetObjectFileHandle( HANDLE* phFile,
                                              PSLUINT32 cbOffset,
                                              PSLUINT32 cbTotal,
                                              PSLUINT32* pcbNewTotal )
{
    PSLSTATUS status;
    HRESULT hr;

    DBSession* pDBSession = PSLNULL;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    MTPDBVARIANTPROP mtpQuad;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT64 qwFileSize;
    DWORD dwFileSize;
    PSLBYTE rgmstrFilePath[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR szFilePath[MAX_MTP_STRING_LEN];
    HANDLE hFile = INVALID_HANDLE_VALUE;
    DWORD dwPtrLow;
    DWORD dwError;
    PSLUINT16 wProtectionStatus;

    if (PSLNULL != phFile)
    {
        *phFile = INVALID_HANDLE_VALUE;
    }

    if (PSLNULL != pcbNewTotal)
    {
        *pcbNewTotal = 0;
    }

    if (PSLNULL == phFile || PSLNULL == pcbNewTotal)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreQuery(mds, pPropList, cPropList,
                                MTPDSITEMTYPE_OBJECT,
                                &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cItems);

    /* Check if the object is write protected */
    /* protection status */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PROTECTIONSTATUS;

    status = MTPDataStoreGetProp(pItems[0], &mtpQuad, 1,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_NONE),
                            (PSLBYTE*)&wProtectionStatus,
                            sizeof(PSLUINT16), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Non-transferrable data
     * This object�s properties may be read and modified, and it may
     * be moved or deleted on the device, but this object�s binary data
     * may not be retrived from the device using a GetObject operation.
     */
    if (MTP_PROTECTIONSTATUS_NONTRANSFERRABLEDATA == wProtectionStatus)
    {
        status = PSLERROR_ACCESS_DENIED;
        goto Exit;
    }

    /* Get the file size */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTSIZE;

    status = MTPDataStoreGetProp(pItems[0], &mtpQuad, 1,
                                MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                               MTPDSDATAFORMAT_NONE),
                                (PSLBYTE*)&qwFileSize,
                                sizeof(PSLUINT64), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFileSize = (PSLUINT32)qwFileSize;

    /* Get the File path */
    status = MTPDataStoreGetProp(pItems[0], PSLNULL, 0,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_FILEPATH),
                            (PSLBYTE*)&rgmstrFilePath[0],
                            sizeof(PSLWCHAR) * MAX_PATH,
                            PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(szFilePath, PSLARRAYSIZE(szFilePath),
                      PSLNULL, (PCXMTPSTRING)&rgmstrFilePath[0],
                      (rgmstrFilePath[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    hFile = CreateFile((LPCWSTR)szFilePath, GENERIC_READ, 0,
                       PSLNULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,
                       PSLNULL );

    if (INVALID_HANDLE_VALUE == hFile)
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        status = PSLSTATUS_FROM_HRESULT(hr);
        goto Exit;
    }

    dwPtrLow  = SetFilePointer( hFile, cbOffset, PSLNULL,
                    (PSLINT32)cbOffset >= 0 ? FILE_BEGIN : FILE_END);
    if (dwPtrLow == INVALID_SET_FILE_POINTER &&
        (dwError = GetLastError()) != NO_ERROR)
    {
        hr = HRESULT_FROM_WIN32(dwError);
        status = PSLSTATUS_FROM_HRESULT(hr);
        goto Exit;
    }

    if (cbOffset >= dwFileSize ||
        (0xFFFFFFFF != cbTotal) && (cbTotal > dwFileSize) ||
        ((0xFFFFFFFF != cbTotal) && (cbOffset + cbTotal) > dwFileSize))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    *pcbNewTotal = min(cbTotal, dwFileSize - cbOffset);
    *phFile = hFile;
    hFile = INVALID_HANDLE_VALUE;

    status = PSLSUCCESS;

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pItems);
    SAFE_CLOSEFILE(hFile);

    return status;
}

PSLSTATUS DBContextData::SetObjectFileHandle( HANDLE* phFile,
                                          PSLUINT32 cbSize )
{
    PSLSTATUS status;
    DBSession* pDBSession = PSLNULL;
    PSLUINT32 dwObjHandle;
    MTPDATASTORE mds;
    MTPDBVARIANTPROP mtpQuad = {0};
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT64 qwFileSize;
    DWORD dwFileSize;
    DWORD dwAttributes;
    HRESULT hr;

    PSLBYTE rgmstrFilePath[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR szFilePath[MAX_MTP_STRING_LEN];

    HANDLE hFile = INVALID_HANDLE_VALUE;
    PSLUINT16 wProtectionStatus;

    if( phFile != PSLNULL )
    {
        *phFile = INVALID_HANDLE_VALUE;
    }

    if( phFile == PSLNULL || cbSize == 0 )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetObjectInfoHandleAndSize(&dwObjHandle,
                                                     PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(MTP_OBJECTHANDLE_UNDEFINED != dwObjHandle ||
              MTP_OBJECTHANDLE_ALL != dwObjHandle);

    if (MTP_OBJECTHANDLE_UNDEFINED == dwObjHandle ||
              MTP_OBJECTHANDLE_ALL == dwObjHandle)
    {
        status = MTPERROR_DATABASE_INVALID_HANDLE;
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = dwObjHandle;

    status = MTPDataStoreQuery(mds, &mtpQuad, 1,
                               MTPDSITEMTYPE_OBJECT,
                               &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cItems);


    /* If the object is read only or read only data return error */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PROTECTIONSTATUS;

    status = MTPDataStoreGetProp(pItems[0], &mtpQuad, 1,
                              MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                             MTPDSDATAFORMAT_NONE),
                              (PSLBYTE*)&wProtectionStatus,
                              sizeof(PSLUINT16), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Read-only
     * This object cannot be deleted or modified, and none of the properties
     * of this object can be modified by the initiator.
     * Read-only data:
     * This object�s binary component cannot be deleted or modified,
     * however any object properties may be modified if allowed by
     * the object property constraints.
     */
    if (MTP_PROTECTIONSTATUS_READONLY == wProtectionStatus ||
        MTP_PROTECTIONSTATUS_READONLYDATA == wProtectionStatus)
    {
        status = MTPERROR_DATABASE_WRITEPROTECTED_OBJECT;
        goto Exit;
    }

    /* File Size */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTSIZE;

    status = MTPDataStoreGetProp(pItems[0], &mtpQuad, 1,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_NONE),
                            (PSLBYTE*)&qwFileSize,
                            sizeof(PSLUINT64), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFileSize = (PSLUINT32)qwFileSize;

    /* File path */
    status = MTPDataStoreGetProp(pItems[0], PSLNULL, 0,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_FILEPATH),
                             (PSLBYTE*)&rgmstrFilePath[0],
                             sizeof(PSLWCHAR) * MAX_PATH,
                             PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPLoadMTPString(szFilePath, PSLARRAYSIZE(szFilePath),
                      PSLNULL, (PCXMTPSTRING)&rgmstrFilePath[0],
                      (rgmstrFilePath[0] * sizeof(PSLWCHAR))+ 1);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if(INVALID_FILE_ATTRIBUTES != (dwAttributes =
       GetFileAttributes((LPCWSTR)szFilePath)))
    {
        if (FILE_ATTRIBUTE_DIRECTORY == dwAttributes)
        {
            /*  ATTENTION: This is done as a patch.
             *  In real implementation this situation should
             *  not arise. Proting kit has this issue because
             *  it doesn't persist metadata.
             */
            /* Remove the directory recursively */
            status = _RemoveDirectoryRecur((LPCWSTR)szFilePath);
            if (PSL_FAILED(status))
            {
                goto Exit;
            }
        }
    }

    hFile = CreateFile((LPCWSTR)szFilePath, GENERIC_WRITE, 0, PSLNULL,
                        CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, PSLNULL);
    if (INVALID_HANDLE_VALUE == hFile)
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        status = (E_ACCESSDENIED == hr) ?
                    PSLERROR_ACCESS_DENIED :
                    PSLSTATUS_FROM_HRESULT(hr);
        goto Exit;
    }

    if( dwFileSize == 0 && cbSize != MTPDB_DATASIZE_UNKNOWN )
    {
        dwFileSize = cbSize;
    }

    /*
    if( dwFileSize != 0 )
    {
        // change size of file to size of incoming data
        SetFilePointer(hFile, dwFileSize, PSLNULL, FILE_BEGIN);
        SetEndOfFile(hFile);
        // return file pointer to beginning of file
        SetFilePointer(hFile, 0, PSLNULL, FILE_BEGIN);
    }
    */

    *phFile = hFile;
    hFile = INVALID_HANDLE_VALUE;

    status = PSLSUCCESS;

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pItems);
    SAFE_CLOSEFILE(hFile);

    return status;
}

PSLSTATUS DBContextData::InsertObject(PSLUINT32 dwDSDataFormat)
{
    PSLSTATUS status;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 dwObjectHandle;
    PSLUINT32 dwObjectSize;
    MTPDBVARIANTPROP mtpQuad = {0};
    PSLUINT16 wObjectFormat;

    PSLBYTE rgmstrFilePath[(2 * MAX_MTP_STRING_LEN) + 1];
    PSLWCHAR szFilePath[MAX_MTP_STRING_LEN];

    PSLUINT64 qwObjectSize;
    HRESULT hr;

    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    MTPDBVARIANTPROP rgmpq[4];
    PSLUINT32 crgmpq = 0;
    PSLUINT32 ixPropList;

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetObjectInfoHandleAndSize(&dwObjectHandle,
                                                    &dwObjectSize);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (MTP_OBJECTHANDLE_UNDEFINED != dwObjectHandle)
    {
        /* Invalidate session level stashed ObjectHandle and Size*/
        pDBSession->SetObjectInfoHandleAndSize(MTP_OBJECTHANDLE_UNDEFINED, 0);

        if (0 != dwObjectSize)
        {
            /* Previous SendObjectPropList was not followed
             * by a SendObject and the spec'd size was not 0
             * delete the object from store (per MTP spec)
             */

            /* Query object */
            mtpQuad.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
            mtpQuad.wDataType = MTP_DATATYPE_UINT32;
            mtpQuad.vParam.valInt32 = dwObjectHandle;

            status = MTPDataStoreQuery(mds, &mtpQuad, 1,
                                       MTPDSITEMTYPE_OBJECT,
                                       &pItems, &cItems);

            if (PSL_SUCCEEDED(status))
            {
                /* this will trigger even if not found
                 * case in which we won't fail deleting something inexisting
                 */

                PSLASSERT(1 == cItems);

                /* If a previous SendObjectInfo or SendObjectPropList
                 * has set this handle, delete that object
                 */
                status = MTPDataStoreDelete(pItems[0]);

                PSLASSERT(PSL_SUCCEEDED(status));
                /* Not failing the new SendObjectInfo or SendObjectPropList
                 * just because we couldn't delete the previous one
                 */
            }

            dwObjectHandle = MTP_OBJECTHANDLE_UNDEFINED;
            dwObjectSize = 0;
        }
    }

    SAFE_PSLMEMFREE(pItems);
    cItems = 0;

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(4 > cPropList);

    for (ixPropList = 0; ixPropList < cPropList; ixPropList++)
    {
        rgmpq[crgmpq++] = pPropList[ixPropList];
    }

    if (PSLNULL != m_pOutPropQuads)
    {
        PSLASSERT(1 == m_cOutPropQuads);

        rgmpq[crgmpq++] = m_pOutPropQuads[0];
    }

    status = MTPDataStoreInsert(mds, rgmpq, crgmpq,
                                MTPDSITEMTYPE_OBJECT,
                                &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cItems);

    /* Passing pPropList & cPropList as SendObjectPropList
     * passes format code and object size as part of DBContext
     */
    status = MTPDataStoreSetProp(pItems[0], PSLNULL, 0,
                                 dwDSDataFormat,
                                 m_pbBuf, m_cbBuf);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreGetProp(pItems[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_ITEMID),
                        (PSLBYTE*)&dwObjectHandle,
                        sizeof(PSLUINT32), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(MTP_OBJECTHANDLE_UNDEFINED != dwObjectHandle ||
              MTP_OBJECTHANDLE_ALL != dwObjectHandle);

    if (MTP_OBJECTHANDLE_UNDEFINED == dwObjectHandle ||
              MTP_OBJECTHANDLE_ALL == dwObjectHandle)
    {
        status = MTPERROR_DATABASE_INVALID_HANDLE;
        goto Exit;
    }

    /* Stash the object handle this will be used by
     * DBContext::GetItemHandle()
     */
    m_pDBContext->StashObjectHandle(dwObjectHandle);

    /* Object format */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTFORMAT;

    status = MTPDataStoreGetProp(pItems[0], &mtpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&wObjectFormat,
                        sizeof(PSLUINT16), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* If the object is an association create a directory right here
     * as we are not going to get an SendObject call to do that
     */
    if (MTP_FORMATCODE_ASSOCIATION == wObjectFormat)
    {
        /* Get the path */
        status = MTPDataStoreGetProp(pItems[0], PSLNULL, 0,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_FILEPATH),
                        (PSLBYTE*)&rgmstrFilePath[0],
                        sizeof(PSLWCHAR) * MAX_PATH,
                        PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        status = MTPLoadMTPString(szFilePath, PSLARRAYSIZE(szFilePath),
                          PSLNULL, (PCXMTPSTRING)&rgmstrFilePath[0],
                          (rgmstrFilePath[0] * sizeof(PSLWCHAR))+ 1);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        /* Create the directory */
        if (0 == CreateDirectory((LPCWSTR)szFilePath, PSLNULL))
        {
            hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
            status = PSLSTATUS_FROM_HRESULT(hr);
            goto Exit;
        }
    }

    /* Object size */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTSIZE;

    status = MTPDataStoreGetProp(pItems[0], &mtpQuad, 1,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        (PSLBYTE*)&qwObjectSize,
                        sizeof(PSLUINT64), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(qwObjectSize == (qwObjectSize & 0x00000000FFFFFFFF));
    dwObjectSize = (PSLUINT32)qwObjectSize;

    // next SendObject command will need this
    pDBSession->SetObjectInfoHandleAndSize(dwObjectHandle,
                                           dwObjectSize);

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS DBContextData::DeleteObject()
{
    PSLSTATUS status;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    MTPDBVARIANTPROP* pPropList = PSLNULL;
    PSLUINT32 cPropList = 0;

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Query object */
    status = MTPDataStoreQuery(mds, pPropList, cPropList,
                               MTPDSITEMTYPE_OBJECT,
                               &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (1 != cItems)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    status = MTPDataStoreDelete(pItems[0]);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMEMFREE(pItems);
    SAFE_RELEASE(pDBSession);
    return status;
}

/* StorageInfoData */

StorageInfoData::StorageInfoData( DBContext* pDBContext,
                                  PSLUINT32 dwDataFormat )
 : DBContextData( pDBContext, dwDataFormat )
{
}

StorageInfoData::~StorageInfoData()
{
}

PSLSTATUS StorageInfoData::Initialize(
                         PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                         PSLUINT32 cmqPropList )
{
    PSLSTATUS status;

    if( cmqPropList == 0 )
    {
        if( pmqPropList != PSLNULL )
        {
            status = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }
    }
    else
    {
        if( pmqPropList == PSLNULL )
        {
            status = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        PSLASSERT( cmqPropList == 1 );

        m_dwFileSystemType = pmqPropList[0].vParam.valUInt32;
    }

    status = PSLSUCCESS;
Exit:
    return status;
}
PSLSTATUS StorageInfoData::Pack( PSLBYTE** ppbBuff,
                                 PSLUINT32* pcbSize )
{
    PSLSTATUS status;

    switch (MTPDBCONTEXTDATADESC_FORMAT(m_dwDataFormat))
    {
    case MTPDBCONTEXTDATAFORMAT_STORAGE:
        status = StorageInfoPack( ppbBuff, pcbSize );
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        break;

    case MTPDBCONTEXTDATAFORMAT_STORAGEID:
        status = StorageIDsPack( ppbBuff, pcbSize );
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        break;

    default:
        PSLASSERT(PSLFALSE);
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS StorageInfoData::StorageIDsPack(PSLBYTE** ppbBuff,
                                          PSLUINT32* pcbSize)
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 cbSize;
    PSLBYTE* pbBuf = PSLNULL;
    PSLUINT32 dwIndex;
    PSLUINT32* pdwWalker;

    if (PSLNULL != ppbBuff)
    {
        *ppbBuff = PSLNULL;
    }

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    if (PSLNULL == ppbBuff || PSLNULL == pcbSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreQuery(mds, pPropList, cPropList,
                               MTPDSITEMTYPE_STORE,
                               &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* memory to store
     * (count of items + * (count of objects * size of object handle))
     */
    cbSize = sizeof(PSLUINT32) + cItems * sizeof(PSLUINT32);

    pbBuf = (PSLBYTE*)PSLMemAlloc(PSLMEM_PTR, cbSize);
    if (PSLNULL == pbBuf)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /* copy the count of items */
    MTPStoreUInt32((PSLUINT32*)pbBuf, cItems);

    pdwWalker = (PSLUINT32*)(pbBuf + sizeof(PSLUINT32));

    for (dwIndex = 0; dwIndex < cItems; dwIndex++)
    {
        status = MTPDataStoreGetProp(pItems[dwIndex], m_pOutPropQuads,
                                m_cOutPropQuads,
                                MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                               MTPDSDATAFORMAT_ITEMID),
                                (PSLBYTE*)pdwWalker, sizeof(PSLUINT32),
                                PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        pdwWalker += sizeof(PSLUINT32);
    }

    *ppbBuff = pbBuf;
    pbBuf = PSLNULL;

    *pcbSize = cbSize;

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pbBuf);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS StorageInfoData::StorageInfoPack(PSLBYTE** ppbBuff, PSLUINT32* pcbSize)
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 cbBuf;
    PSLBYTE* pbBuf = PSLNULL;

    if (PSLNULL != ppbBuff)
    {
        *ppbBuff = PSLNULL;
    }

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    if (PSLNULL == ppbBuff || PSLNULL == pcbSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreQuery(mds, pPropList, cPropList,
                               MTPDSITEMTYPE_STORE, &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cItems);

    status = MTPDataStoreGetProp(pItems[0], m_pOutPropQuads,
                        m_cOutPropQuads,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_STORAGEINFO),
                         PSLNULL, 0, PSLNULL, &cbBuf);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pbBuf = (PSLBYTE*)PSLMemAlloc(PSLMEM_PTR, cbBuf);
    if (PSLNULL == pbBuf)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = MTPDataStoreGetProp(pItems[0], m_pOutPropQuads,
                        m_cOutPropQuads,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_STORAGEINFO),
                        pbBuf, cbBuf, PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *ppbBuff = pbBuf;
    pbBuf = PSLNULL;

    *pcbSize = cbBuf;

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pbBuf);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS StorageInfoData::ResolveContext( PSLBOOL* pfCommitRequired )
{
    PSLSTATUS status;
    if( pfCommitRequired != PSLNULL )
    {
        // FormatStore is at most a DELETE operation wich has an implicit commit
        *pfCommitRequired = PSLFALSE;
    }

    if( pfCommitRequired == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // delete all objects for the StorageID passed in

    status = m_pDBContext->Delete();

    // TODO format store using the format type param

Exit:
    return status;
}

/* ObjectHandleData */

ObjectHandleData::ObjectHandleData( DBContext* pDBContext,
                                    PSLUINT32 dwDataFormat )
 : DBContextData( pDBContext, dwDataFormat )
{
}

ObjectHandleData::~ObjectHandleData()
{
}

PSLSTATUS ObjectHandleData::Initialize(
                         PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                         PSLUINT32 cmqPropList )
{
    PSLSTATUS status = PSLERROR_INVALID_OPERATION;

     /* The only opertions with properties we supports is GetObjectReferences
      * and SetObjectReferences; other than that we always expect no properties
     */
     if (cmqPropList == 0)
     {
         /*GetObjectHandles*/
         if( MTPDBCONTEXTDATA_QUERY ==
             (MTPDBCONTEXTDATA_FLAGS_MASK &
                MTPDBCONTEXTDATADESC_FLAGS(m_dwDataFormat)) )
         {
             status = PSLSUCCESS;
         }
     }
     else if(1 == cmqPropList &&
            (PSLUINT16)(pmqPropList[0].vParam.valInt32) ==
             MTP_OBJECTPROPCODE_OBJECTREFERENCES)
     {
         status = PSLSUCCESS;
     }

    return status;
}

PSLSTATUS ObjectHandleData::Pack(PSLBYTE** ppbBuff, PSLUINT32* pcbSize)
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 cbTotalSize, cbTotalSizeTemp;
    PSLBYTE* pbBuf = PSLNULL;
    PSLUINT32 dwIndex;
    PSLBYTE* pbWalker;
    PSLUINT32 dwDataFormat;

    if (PSLNULL != ppbBuff)
    {
        *ppbBuff = PSLNULL;
    }

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    if (PSLNULL == ppbBuff || PSLNULL == pcbSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreQuery(mds, pPropList, cPropList,
                               MTPDSITEMTYPE_OBJECT,
                               &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    // MTPGetObjectReferences has prop code MTP_OBJECTPROPCODE_OBJECTREFERENCES
    PSLASSERT(0 == m_cOutPropQuads || 1 == m_cOutPropQuads);
    if (PSLNULL == m_pOutPropQuads )
    {
        dwDataFormat = MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                      MTPDSDATAFORMAT_ITEMID);

        /* memory to store
         * (count of items + * (count of objects * size of object handle))
         */
        cbTotalSize = sizeof(PSLUINT32) + cItems * sizeof(PSLUINT32);
    }
    else
    {
        if ( cItems > 1)
        {
            status = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        dwDataFormat = MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                      MTPDSDATAFORMAT_NONE);

        status = MTPDataStoreGetProp(pItems[0],
                                     m_pOutPropQuads,
                                     m_cOutPropQuads,
                                     dwDataFormat,
                                     PSLNULL, 0, PSLNULL, &cbTotalSize);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    /* allocate buffer */
    pbBuf = (PSLBYTE*)PSLMemAlloc(PSLMEM_PTR, cbTotalSize);
    if (PSLNULL == pbBuf)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    cbTotalSizeTemp = cbTotalSize;

    if (PSLNULL == m_pOutPropQuads )
    {
        /* copy the count of items */
        MTPStoreUInt32((PSLUINT32*)pbBuf, cItems);

        pbWalker = pbBuf + sizeof(PSLUINT32);
        cbTotalSizeTemp -= sizeof(PSLUINT32);
    }
    else
    {
        pbWalker = pbBuf;
    }

    PSLUINT32 cbSize = 0;
    for (dwIndex = 0; dwIndex < cItems; dwIndex++)
    {
        status = MTPDataStoreGetProp(pItems[dwIndex], m_pOutPropQuads,
                                m_cOutPropQuads,
                                dwDataFormat,
                                pbWalker, cbTotalSizeTemp, PSLNULL, &cbSize);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        pbWalker += cbSize;
        cbTotalSizeTemp -= cbSize;
    }

    *ppbBuff = pbBuf;
    pbBuf = PSLNULL;

    *pcbSize = cbTotalSize;

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pbBuf);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS ObjectHandleData::ResolveContext( PSLBOOL* pfCommitRequired )
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT16 wProtectionStatus;
    MTPDBVARIANTPROP mtpQuad = {0};

    if(pfCommitRequired != PSLNULL)
    {
        *pfCommitRequired = PSLFALSE;
    }

    if (pfCommitRequired == PSLNULL)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (MTPDBCONTEXTDATA_QUERY == (MTPDBCONTEXTDATA_FLAGS_MASK & \
            MTPDBCONTEXTDATADESC_FLAGS(m_dwDataFormat)))
    {
        PSLASSERT(!"SetItemProps is not allowed for QUERY cmds");
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreQuery(mds, pPropList,
                                cPropList,
                                MTPDSITEMTYPE_OBJECT,
                                &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cItems);

    PSLASSERT(1 == m_cOutPropQuads);

    /* Check if the object is write protected */
    /* protection status */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PROTECTIONSTATUS;

    status = MTPDataStoreGetProp(pItems[0], &mtpQuad, 1,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_NONE),
                            (PSLBYTE*)&wProtectionStatus,
                            sizeof(PSLUINT16), PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* This object cannot be deleted or modified, and
     * none of the properties of this object can be
     * modified by the initiator.
     */
    if (MTP_PROTECTIONSTATUS_READONLY == wProtectionStatus)
    {
        status = PSLERROR_ACCESS_DENIED;
        goto Exit;
    }

    status = MTPDataStoreSetProp(pItems[0], m_pOutPropQuads,
                        m_cOutPropQuads,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        m_pbBuf, m_cbBuf);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pItems);
    return status;
}



/* ObjectPropArrayData */

ObjectPropArrayData::ObjectPropArrayData( DBContext* pDBContext,
                                          PSLUINT32 dwDataFormat )
 : DBContextData(pDBContext, dwDataFormat)
{
}

ObjectPropArrayData::~ObjectPropArrayData()
{
}

PSLSTATUS ObjectPropArrayData::Initialize(
                         PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                         PSLUINT32 cmqPropList )
{
    PSLSTATUS status;

    if( cmqPropList == 0 )
    {
        if( pmqPropList != PSLNULL )
        {
            status = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }
    }
    else
    {
        if( pmqPropList == PSLNULL )
        {
            status = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        PSLASSERT( cmqPropList == 1 );
    }

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS ObjectPropArrayData::Pack(PSLBYTE** ppbBuff,
                                    PSLUINT32* pcbSize)
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 cbBuf;
    PSLBYTE* pbBuf = PSLNULL;

    if (PSLNULL != ppbBuff)
    {
        *ppbBuff = PSLNULL;
    }

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    if (PSLNULL == ppbBuff || PSLNULL == pcbSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreQuery(mds, pPropList, cPropList,
                               MTPDSITEMTYPE_OBJECT,
                               &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cItems);

    status = MTPDataStoreGetProp(pItems[0], m_pOutPropQuads,
                                m_cOutPropQuads,
                                MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                               MTPDSDATAFORMAT_NONE),
                                PSLNULL, 0, PSLNULL, &cbBuf);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pbBuf = (PSLBYTE*)PSLMemAlloc(PSLMEM_PTR, cbBuf);
    if (PSLNULL == pbBuf)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = MTPDataStoreGetProp(pItems[0], m_pOutPropQuads,
                                m_cOutPropQuads,
                                MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                               MTPDSDATAFORMAT_NONE),
                                pbBuf, cbBuf, PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *ppbBuff = pbBuf;
    pbBuf = PSLNULL;

    *pcbSize = cbBuf;

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pbBuf);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS ObjectPropArrayData::ResolveContext( PSLBOOL* pfCommitRequired )
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT16 wProtectionStatus;
    MTPDBVARIANTPROP mtpQuad = {0};

    if(pfCommitRequired != PSLNULL)
    {
        /* SendPropValue, SetObjectProtection &
         * SetObjectRefernces does not have a commit
         */
        *pfCommitRequired = PSLFALSE;
    }

    if (pfCommitRequired == PSLNULL)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (MTPDBCONTEXTDATA_QUERY == (MTPDBCONTEXTDATA_FLAGS_MASK & \
            MTPDBCONTEXTDATADESC_FLAGS(m_dwDataFormat)))
    {
        PSLASSERT(!"SetItemProps is not allowed for QUERY cmds");
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreQuery(mds, pPropList,
                                cPropList,
                                MTPDSITEMTYPE_OBJECT,
                                &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cItems);

    PSLASSERT(1 == m_cOutPropQuads);

    if (MTP_OBJECTPROPCODE_PROTECTIONSTATUS != m_pOutPropQuads[0].vParam.valInt32)
    {
        /* Check if the object is write protected */
        /* protection status */
        mtpQuad.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mtpQuad.wDataType = MTP_DATATYPE_UINT32;
        mtpQuad.vParam.valInt32 = MTP_OBJECTPROPCODE_PROTECTIONSTATUS;

        status = MTPDataStoreGetProp(pItems[0], &mtpQuad, 1,
                                MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                               MTPDSDATAFORMAT_NONE),
                                (PSLBYTE*)&wProtectionStatus,
                                sizeof(PSLUINT16), PSLNULL, PSLNULL);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        /* This object cannot be deleted or modified, and
         * none of the properties of this object can be
         * modified by the initiator.
         */
        if (MTP_PROTECTIONSTATUS_READONLY == wProtectionStatus)
        {
            status = PSLERROR_ACCESS_DENIED;
            goto Exit;
        }
    }

    status = MTPDataStoreSetProp(pItems[0], m_pOutPropQuads,
                        m_cOutPropQuads,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_NONE),
                        m_pbBuf, m_cbBuf);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

/* ObjectInfoData */

ObjectInfoData::ObjectInfoData( DBContext* pDBContext,
                                PSLUINT32 dwDataFormat )
 : DBContextData( pDBContext, dwDataFormat )
{
}

ObjectInfoData::~ObjectInfoData()
{
}

PSLSTATUS ObjectInfoData::Initialize(
                         PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                         PSLUINT32 cmqPropList )
{
    PSLSTATUS status;

    if( cmqPropList == 0 )
    {
        if( pmqPropList != PSLNULL )
        {
            status = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }
    }
    else
    {
        if( pmqPropList == PSLNULL )
        {
            status = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        PSLASSERT( cmqPropList == 2 );
    }
    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS ObjectInfoData::Pack( PSLBYTE** ppbBuff, PSLUINT32* pcbSize )
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 cbBuf;
    PSLBYTE* pbBuf = PSLNULL;

    if (PSLNULL != ppbBuff)
    {
        *ppbBuff = PSLNULL;
    }

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    if (PSLNULL == ppbBuff || PSLNULL == pcbSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreQuery(mds, pPropList, cPropList,
                               MTPDSITEMTYPE_OBJECT,
                               &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cItems);

    status = MTPDataStoreGetProp(pItems[0], m_pOutPropQuads,
                            m_cOutPropQuads,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_OBJECTINFO),
                            PSLNULL, 0, PSLNULL, &cbBuf);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pbBuf = (PSLBYTE*)PSLMemAlloc(PSLMEM_PTR, cbBuf);
    if (PSLNULL == pbBuf)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = MTPDataStoreGetProp(pItems[0], m_pOutPropQuads,
                            m_cOutPropQuads,
                            MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                           MTPDSDATAFORMAT_OBJECTINFO),
                            pbBuf, cbBuf, PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *ppbBuff = pbBuf;
    pbBuf = PSLNULL;

    *pcbSize = cbBuf;

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pbBuf);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS ObjectInfoData::ResolveContext(PSLBOOL* pfCommitRequired)
{
    PSLSTATUS status;

    if(pfCommitRequired != PSLNULL)
    {
        /* SendObjectInfo is an INSERT with
         * implicit commit so no need to commit again
         */
        *pfCommitRequired = PSLFALSE;
    }

    if (pfCommitRequired == PSLNULL)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (MTPDBCONTEXTDATA_QUERY == (MTPDBCONTEXTDATA_FLAGS_MASK & \
            MTPDBCONTEXTDATADESC_FLAGS(m_dwDataFormat)))
    {
        PSLASSERT(!"SetItemProps is not allowed for QUERY cmds");
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = InsertObject(MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                   MTPDSDATAFORMAT_OBJECTINFO));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
Exit:
    return status;
}

/* ObjectPropListData */

ObjectPropListData::ObjectPropListData( DBContext* pDBContext,
                                        PSLUINT32 dwDataFormat )
 : DBContextData( pDBContext, dwDataFormat )
{
}

ObjectPropListData::~ObjectPropListData()
{
}

PSLSTATUS ObjectPropListData::Initialize(
                         PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                         PSLUINT32 cmqPropList )
{
    PSLSTATUS status;

    if( cmqPropList == 0 )
    {
        if( pmqPropList != PSLNULL )
        {
            status = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }
    }
    else
    {
        if( pmqPropList == PSLNULL )
        {
            status = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        PSLASSERT( cmqPropList == 2 || cmqPropList == 1 );
    }

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS ObjectPropListData::Pack( PSLBYTE** ppbBuff,
                                    PSLUINT32* pcbSize )
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 cbSize;
    PSLBYTE* pbBuf = PSLNULL;
    PSLUINT32 dwIndex;
    PSLUINT32 cdwProps;
    PSLUINT32 cdwTotalProps = 0;
    PSLUINT32 cbTotalSize = 0;
    PSLBYTE* pbWalker;

    if (PSLNULL != ppbBuff)
    {
        *ppbBuff = PSLNULL;
    }

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    if (PSLNULL == ppbBuff || PSLNULL == pcbSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDataStoreQuery(mds, pPropList, cPropList,
                               MTPDSITEMTYPE_OBJECT,
                               &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    for (dwIndex = 0; dwIndex < cItems; dwIndex++)
    {
        status = MTPDataStoreGetProp(pItems[dwIndex], m_pOutPropQuads,
                        m_cOutPropQuads,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_OBJECTPROPLIST),
                        PSLNULL, 0, &cdwProps, &cbSize);
        if (PSLERROR_NOT_AVAILABLE == status)
        {
            /* The object doesn't support property code
             * associated with the passed in group code
             * or the passed in property code itself
             * in which case skip this object.
             */
            status = PSLSUCCESS;
            continue;
        }

        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        cdwTotalProps += cdwProps;
        cbTotalSize += cbSize;
    }

    /* sizeof(PSLUINT32) to store the count of items*/
    cbTotalSize += sizeof(PSLUINT32);

    pbBuf = (PSLBYTE*)PSLMemAlloc(PSLMEM_PTR, cbTotalSize);
    if (PSLNULL == pbBuf)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    pbWalker = pbBuf;

    MTPStoreUInt32((PSLUINT32*)pbWalker, cdwTotalProps);
    pbWalker += sizeof(PSLUINT32);

    for (dwIndex = 0; dwIndex < cItems; dwIndex++)
    {
        status = MTPDataStoreGetProp(pItems[dwIndex], m_pOutPropQuads,
                        m_cOutPropQuads,
                        MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                       MTPDSDATAFORMAT_OBJECTPROPLIST),
                        pbWalker, cbTotalSize, PSLNULL, &cbSize);
        if (PSLERROR_NOT_AVAILABLE == status)
        {
            /* The object doesn't support property code
             * associated with the passed in group code
             * or the passed in property code itself
             * in which case skip this object.
             */
            status = PSLSUCCESS;
            continue;
        }

        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        pbWalker += cbSize;
    }

    *ppbBuff = pbBuf;
    pbBuf = PSLNULL;

    *pcbSize = cbTotalSize;

Exit:
    SAFE_PSLMEMFREE(pbBuf);
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

PSLSTATUS ObjectPropListData::ResolveContext(PSLBOOL* pfCommitRequired)
{
    PSLSTATUS status;

    if( pfCommitRequired != PSLNULL )
    {
        *pfCommitRequired = PSLFALSE;
    }

    if( pfCommitRequired == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    switch( MTPDBCONTEXTDATA_FLAGS_MASK &
            MTPDBCONTEXTDATADESC_FLAGS(m_dwDataFormat))
    {
    case MTPDBCONTEXTDATA_UPDATE:

        // SetObjectPropList is an UPDATE
        // it doesn't spawn over multiple MTPDBContextDataOpen calls
        // so a delayed commit after the last one would not improve performance
        // but rather parsing all props and group them per item
        // then commit once every item

        status = ResolveSetObjectPropList(pfCommitRequired);

        break;

    case MTPDBCONTEXTDATA_INSERT:

        // SendObjectPropList is an INSERT with implicit commit
        // so no extra commit is required

        status = ResolveSendObjectPropList(pfCommitRequired);

        break;

    case MTPDBCONTEXTDATA_QUERY:
        // intentional fall through
    default:
        status = PSLERROR_UNEXPECTED;
        goto Exit;
    }

Exit:
    return status;
}

PSLSTATUS ObjectPropListData::ResolveSendObjectPropList(
                                    PSLBOOL* pfCommitRequired)
{
    PSLSTATUS status;

    if(PSLNULL != pfCommitRequired)
    {
        /* SendObjectInfo is an INSERT with
         * implicit commit so no need to commit again
         */
        *pfCommitRequired = PSLFALSE;
    }

    if (PSLNULL == pfCommitRequired)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (MTPDBCONTEXTDATA_QUERY == (MTPDBCONTEXTDATA_FLAGS_MASK & \
                        MTPDBCONTEXTDATADESC_FLAGS(m_dwDataFormat)))
    {
        PSLASSERT(!"SetItemProps is not allowed for QUERY cmds");
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = InsertObject(MAKE_MTPDSDATA(MTPDSDATAFLAG_NONE, \
                                   MTPDSDATAFORMAT_OBJECTPROPLIST));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
Exit:
    return status;
}

PSLSTATUS ObjectPropListData::ResolveSetObjectPropList(
                                    PSLBOOL* pfCommitRequired)
{
    PSLSTATUS status;

    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPDBVARIANTPROP mpq;
    PSLUINT32 cmpq, idx;
    PSLBYTE* pbWalker = m_pbBuf;
    PSLUINT32 cbWalker = m_cbBuf;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 dwObjectHandle, dwLastObjectHandle;
    PSLUINT16 wPropCode;
    PSLUINT16 wDataType;
    PSLUINT32 cbCurVal;

    if(PSLNULL != pfCommitRequired)
    {
        /* SetObjectPropList is an UPDATE
         * it doesn't spawn over multiple MTPDBContextDataOpen calls
         * so a delayed commit after the last one would not improve performance
         * but rather parsing all props and group them per item
         * then commit once every item
         */
        *pfCommitRequired = PSLFALSE;
    }

    if( pbWalker == NULL )
    {
        PSLASSERT( PSLFALSE );
        status = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    cmpq = MTPLoadUInt32((PXPSLUINT32)pbWalker);
    pbWalker += sizeof(PSLUINT32);

    idx = 0;
    dwLastObjectHandle = MTP_OBJECTHANDLE_UNDEFINED;

    PSLZeroMemory(&mpq, sizeof(mpq));

    while((pbWalker < m_pbBuf + m_cbBuf) && (idx < cmpq))
    {
        // load a prop quad from the buffer

        if (m_pbBuf + m_cbBuf - pbWalker < sizeof(PSLUINT32) + 2 * sizeof(PSLUINT16))
        {
            status = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        dwObjectHandle = MTPLoadUInt32((PXPSLUINT32)pbWalker);
        pbWalker += sizeof(PSLUINT32);

        wPropCode = MTPLoadUInt16((PXPSLUINT16)pbWalker);
        pbWalker += sizeof(PSLUINT16);

        wDataType = MTPLoadUInt16((PXPSLUINT16)pbWalker);
        pbWalker += sizeof(PSLUINT16);

        if (dwObjectHandle == MTP_OBJECTHANDLE_UNDEFINED ||
            dwObjectHandle == MTP_OBJECTHANDLE_ROOT )
        {
            status = MTPERROR_DATABASE_INVALID_HANDLE;
            goto Exit;
        }

        // save a trip to the DataStore if same ObjectHandle

        if (dwObjectHandle != dwLastObjectHandle)
        {
            SAFE_PSLMEMFREE(pItems);
            cItems = 0;

            mpq.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
            mpq.wDataType = MTP_DATATYPE_UINT32;
            mpq.vParam.valInt32 = dwObjectHandle;

            status = MTPDataStoreQuery(mds, &mpq, 1,
                                       MTPDSITEMTYPE_OBJECT,
                                       &pItems, &cItems);
            if (PSL_FAILED(status))
            {
                // absorb DataStore error code
                status = MTPERROR_DATABASE_INVALID_HANDLE;
                goto Exit;
            }

            dwLastObjectHandle = dwObjectHandle;

            if (1 != cItems)
            {
                PSLASSERT(PSLFALSE);
                status = PSLERROR_PLATFORM_FAILURE;
                goto Exit;
            }
        }

        status = MTPPropertyReadSize(wDataType, pbWalker, cbWalker, &cbCurVal);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        // SetProp only needs the PropCode
        mpq.wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
        mpq.wDataType = MTP_DATATYPE_UINT32;
        mpq.vParam.valInt32 = wPropCode;

        DWORD dwDataFlag = MTPDSDATAFLAG_NONE;
        if (MTPDBCONTEXTDATADESC_FORMAT(m_dwDataFormat) ==
                    MTPDBCONTEXTDATAFORMAT_OBJECTPROTECTION)
        {
            dwDataFlag = MTPDSDATAFLAG_FORCEUPDATE;
        }

        status = MTPDataStoreSetProp(pItems[0], &mpq,  1,
                        MAKE_MTPDSDATA(dwDataFlag, MTPDSDATAFORMAT_NONE),
                        pbWalker, cbCurVal);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        pbWalker += cbCurVal;
        cbWalker -= cbCurVal;

        idx++;
    }

    PSLASSERT( cmpq == idx );

    // idx is the index of the quad that failed
    // on success it should be 0
    if (idx == cmpq)
    {
        idx = 0;
    }

    PSLASSERT(pbWalker == m_pbBuf + m_cbBuf);

    status = PSLSUCCESS;

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pItems);

    return status;
}

/* ObjectBinaryData */

ObjectBinaryData::ObjectBinaryData( DBContext* pDBContext,
                                    PSLUINT32 dwDataFormat )
: DBContextData( pDBContext, dwDataFormat ),
  m_hFile(INVALID_HANDLE_VALUE)
{
}

ObjectBinaryData::~ObjectBinaryData()
{
    PSLASSERT((PSLHANDLE)-1 == m_hFile);
}

PSLSTATUS ObjectBinaryData::Initialize(
                         PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                         PSLUINT32 cmqPropList )
{
    PSLSTATUS status;

    if( cmqPropList == 0 )
    {
        if( pmqPropList != PSLNULL )
        {
            status = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }
    }
    else
    {
        if( pmqPropList == PSLNULL )
        {
            status = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        PSLASSERT( cmqPropList == 2 );
        PSLASSERT( pmqPropList[0].wDataType == MTP_DATATYPE_UINT32);
        PSLASSERT( pmqPropList[1].wDataType == MTP_DATATYPE_UINT32 );

        m_qwOffset = pmqPropList[0].vParam.valUInt32;
        m_cbBuf = pmqPropList[1].vParam.valUInt32;
    }

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS ObjectBinaryData::Cleanup()
{
    PSLSTATUS status;

    SAFE_CLOSEFILE(m_hFile);

    status = DBContextData::Cleanup();

    return status;
}

PSLSTATUS ObjectBinaryData::Pack( PSLBYTE** ppbBuff, PSLUINT32* pcbSize)
{
    ppbBuff;
    pcbSize;

    PSLASSERT( !"Binary data doesn't need to be Pack-ed" );
    return PSLERROR_NOT_IMPLEMENTED;
}

PSLSTATUS ObjectBinaryData::Terminate()
{
    PSLSTATUS status;
    DBSession* pDBSession = PSLNULL;

    status = PSLSUCCESS;

    if (MTPDBCONTEXTDATA_UPDATE ==
        (MTPDBCONTEXTDATA_FLAGS_MASK &
            MTPDBCONTEXTDATADESC_FLAGS(m_dwDataFormat)))
    {
        /* if in the middle of SendObject
         * delete the object from data store
         */
        status = DeleteObject();

        PSLASSERT(m_pDBContext != PSLNULL);

        status |= m_pDBContext->GetDBSession(&pDBSession);
        if (pDBSession != PSLNULL)
        {
            /* Invalidate session level stashed ObjectHandle and Size*/
            pDBSession->SetObjectInfoHandleAndSize(MTP_OBJECTHANDLE_UNDEFINED, 0);
        }
    }

    status |= Cleanup();

    SAFE_RELEASE(pDBSession);

    return status;
}

PSLSTATUS ObjectBinaryData::GetPackedSize(PSLUINT64 * pqwSize)
{
    PSLSTATUS status;
    DWORD dwTotalSize;

    if( pqwSize == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = GetObjectFileHandle(&m_hFile, m_qwOffset,
                                 m_cbBuf, &dwTotalSize );
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    PSLASSERT( m_pbBuf == PSLNULL );

    PSLTraceDetail("#### ObjectBinaryData::GetPackedSize() \
                           File Size : 0x%08x Initial Offset: 0x%08x",
              dwTotalSize, m_qwOffset );

    m_cbBuf = m_qwOffset + dwTotalSize;

    PSLTraceDetail( "#### ObjectBinaryData::GetPackedSize() \
                           Total transfer Size: 0x%08x",
              m_cbBuf );


    *pqwSize = m_cbBuf - m_qwOffset;

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS ObjectBinaryData::Serialize(PSLVOID* pvBuf,
                                   PSLUINT32 cbBuf,
                                   PSLUINT32* pcbWritten,
                                   PSLUINT64* pcbLeft)
{
    PSLSTATUS status;
    PSLUINT32 cbToXfer;
    DWORD cbRead = 0;
    HRESULT hr;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    MTPPERFLOG_ACCESSOBJECT(MP_TYPE_READMTPOBJECT_BEGIN, cbBuf, PSLSUCCESS);

    if (PSLNULL == pvBuf || PSLNULL == pcbWritten ||
        PSLNULL == pcbLeft)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (m_cbBuf <= m_qwOffset)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    cbToXfer = min(cbBuf, m_cbBuf - m_qwOffset);

    PSLASSERT( cbToXfer <= cbBuf );

    if (0 == ReadFile(m_hFile, pvBuf, cbToXfer, &cbRead, NULL))
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        status = PSLSTATUS_FROM_HRESULT(hr);
        goto Exit;
    }

    PSLASSERT(cbRead == cbToXfer);

    /* this releases the buffer when entirely consumed */
    SetOffset(m_qwOffset + cbToXfer);

    *pcbWritten = cbToXfer;

    PSLASSERT(m_cbBuf >= m_qwOffset);

    *pcbLeft = m_cbBuf - m_qwOffset;

    PSLTraceDetail("#### ObjectBinaryData::Serialize() \
                           Datat read : 0x%08x Data left: 0x%08x",
              *pcbWritten, *pcbLeft );

    status = PSLSUCCESS;

Exit:
    MTPPERFLOG_ACCESSOBJECT(MP_TYPE_READMTPOBJECT_END, cbRead, status);
    return status;
}

PSLSTATUS ObjectBinaryData::Deserialize(PSL_CONST PSLVOID* pvBuf,
                                        PSLUINT32 cbBuf)
{
    PSLSTATUS   status;
    DWORD       cbWritten = 0;
    DWORD       dwErr;
    HRESULT     hr;

    MTPPERFLOG_ACCESSOBJECT(MP_TYPE_WRITEMTPOBJECT_BEGIN,
                               cbBuf, PSLSUCCESS);

    if (PSLNULL == pvBuf || PSLNULL == cbBuf )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if( m_hFile == INVALID_HANDLE_VALUE )
    {
        status = SetObjectFileHandle(&m_hFile, m_cbBuf);
        if( PSL_FAILED( status ))
        {
            goto Exit;
        }

        PSLASSERT( m_pbBuf == PSLNULL );
    }

    if (m_cbBuf == MTPDB_DATASIZE_UNKNOWN ||
        m_cbBuf < m_qwOffset + cbBuf)
    {
        m_cbBuf = m_qwOffset + cbBuf;

        PSLTraceDetail( "ObjectBinaryData  \
                               Transfer size: 0x%08x", m_cbBuf );
    }

    if( 0 == WriteFile( m_hFile, pvBuf, cbBuf, &cbWritten, NULL ))
    {
        dwErr = SAFE_GETLASTERROR();

        if ( ERROR_DISK_FULL == dwErr )
        {
            status = MTPERROR_DATABASE_STORAGEFULL;
        }
        else
        {
            PSLASSERT(!L"ObjectBinaryData::Deserialize WriteFile failed");

            hr = HRESULT_FROM_WIN32(dwErr);
            status = PSLSTATUS_FROM_HRESULT(hr);
        }
        goto Exit;
    }

    PSLASSERT( cbWritten == cbBuf );

    // Do not use SetOffset for it frees up the buffer
    m_qwOffset += cbBuf;

    PSLTraceDetail( "@@@@@@ ObjectBinaryData \
                            Written now: 0x%08x Total :0x%08x",
                            cbWritten, m_qwOffset );

    status = PSLSUCCESS;

Exit:
    if( PSL_FAILED( status ))
    {
        SAFE_CLOSEFILE(m_hFile);

        /* Delete the object and the associated file */
        (PSLVOID)DeleteObject();
    }

    MTPPERFLOG_ACCESSOBJECT(MP_TYPE_WRITEMTPOBJECT_END,
                               cbWritten, status);

    return status;
}

void ObjectBinaryData::SetOffset( PSLUINT32 qwOffset )
{
    PSLASSERT(qwOffset <= m_cbBuf);

    m_qwOffset = qwOffset;

    PSLTraceDetail( "#### ObjectBinaryData::SetOffset() \
                           New offset : 0x%08x",
              m_qwOffset);

    if(m_qwOffset == m_cbBuf)
    {
        PSLTraceDetail( "#### ObjectBinaryData::SetOffset() \
                               Closing file");

        PSLASSERT(PSLNULL == m_pbBuf);
        SAFE_CLOSEFILE(m_hFile);
    }
}

PSLSTATUS ObjectBinaryData::ResolveContext( PSLBOOL* pfCommitRequired )
{
    PSLSTATUS status;
    DBSession* pDBSession = PSLNULL;

    if (PSLNULL != pfCommitRequired)
    {
        /* SendObject is an UPDATE operation
         * which dosn't spawn over multiple MTPDBContextDataOpen calls
         * to need a delayed commit until last data was sent
         * it rather spawns over multiple MTPDBDeserialize calls
         */
        *pfCommitRequired = PSLFALSE;
    }

    if( pfCommitRequired == PSLNULL )
    {
        // ppNewItem not required for an UPDATE command
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    switch(( MTPDBCONTEXTDATA_FLAGS_MASK &
             MTPDBCONTEXTDATADESC_FLAGS(m_dwDataFormat)))
    {
    case MTPDBCONTEXTDATA_UPDATE:
        break;
    case MTPDBCONTEXTDATA_QUERY:
        PSLASSERT( !"DBContextData::ResolveContext not allowed "
                    "for QUERY cmds");
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
        break;
    case MTPDBCONTEXTDATA_INSERT:
        // intentional fall-through
    default:
        PSLASSERT( !"Invalid format flag for BinaryData context" );
        status = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;
    }

    PSLASSERT( MTPDBCONTEXTDATADESC_FORMAT(m_dwDataFormat) ==
                    MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA );

    // TODO transfer to temp file and rename to actual file name here

    // reset the ObjectHandle stashed during SendObjectInfo
    // only if successful
    // because Intitiator might send Object binary data again
    // if SendObject failed

    status = m_pDBContext->GetDBSession(&pDBSession);
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    pDBSession->SetObjectInfoHandleAndSize(
                    MTP_OBJECTHANDLE_UNDEFINED, 0);
Exit:
    SAFE_RELEASE(pDBSession);

    return status;
}

PSLSTATUS ObjectBinaryData::DeleteObject()
{
    PSLSTATUS status;
    DBSession* pDBSession = PSLNULL;
    MTPDATASTORE mds;
    MTPITEM* pItems = PSLNULL;
    PSLUINT32 cItems;
    PSLUINT32 dwObjectHandle;
    MTPDBVARIANTPROP mtpQuad = {0};

    status = m_pDBContext->GetDBSession(&pDBSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetDataStore(&mds);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = pDBSession->GetObjectInfoHandleAndSize(&dwObjectHandle,
                                                    PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(MTP_OBJECTHANDLE_UNDEFINED != dwObjectHandle ||
              MTP_OBJECTHANDLE_ALL != dwObjectHandle);

    /* Query object */
    mtpQuad.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    mtpQuad.wDataType = MTP_DATATYPE_UINT32;
    mtpQuad.vParam.valInt32 = dwObjectHandle;

    status = MTPDataStoreQuery(mds, &mtpQuad, 1,
                               MTPDSITEMTYPE_OBJECT,
                               &pItems, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cItems);

    /* Delete  the object
     */
    status = MTPDataStoreDelete(pItems[0]);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    SAFE_RELEASE(pDBSession);
    SAFE_PSLMEMFREE(pItems);
    return status;
}

/* ObjectThumbData */

ObjectThumbData::ObjectThumbData( DBContext* pDBContext,
                                  PSLUINT32 dwDataFormat )
: DBContextData( pDBContext, dwDataFormat ),
  m_hFile(INVALID_HANDLE_VALUE)
{
}

ObjectThumbData::~ObjectThumbData()
{
    PSLASSERT((PSLHANDLE)-1 == m_hFile);
}

PSLSTATUS ObjectThumbData::Initialize(
                         PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                         PSLUINT32 cmqPropList )
{
    PSLASSERT( pmqPropList == PSLNULL && cmqPropList == 0 );

    return PSLSUCCESS;
pmqPropList;
cmqPropList;
}

PSLSTATUS ObjectThumbData::Cleanup()
{
    PSLSTATUS status;

    SAFE_CLOSEFILE(m_hFile);

    status = DBContextData::Cleanup();

    return status;
}

PSLSTATUS ObjectThumbData::Pack( PSLBYTE** ppbBuff, PSLUINT32* pcbSize)
{
    ppbBuff;
    pcbSize;
    PSLASSERT( !"Binary data doesn't need to be Pack-ed" );
    return PSLERROR_NOT_IMPLEMENTED;
}

PSLSTATUS ObjectThumbData::GetPackedSize( PSLUINT64 * pqwSize )
{
    PSLSTATUS status;
    DWORD dwTotalSize;

    if( pqwSize == PSLNULL )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = GetObjectFileHandle(&m_hFile, m_qwOffset,
                                  m_cbBuf, &dwTotalSize );
    if( PSL_FAILED( status ))
    {
        goto Exit;
    }

    PSLASSERT( m_pbBuf == PSLNULL );

    m_cbBuf = m_qwOffset + dwTotalSize;
    *pqwSize = m_cbBuf;

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS ObjectThumbData::Serialize( PSLVOID* pvBuf,
                                      PSLUINT32 cbBuf,
                                      PSLUINT32* pcbWritten,
                                      PSLUINT64* pcbLeft)
{
    PSLSTATUS status;
    PSLUINT32 cbToXfer;
    DWORD cbRead;
    HRESULT hr;

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    if (PSLNULL == pvBuf || PSLNULL == pcbWritten || PSLNULL == pcbLeft)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (m_cbBuf <= m_qwOffset)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    cbToXfer = min(cbBuf, m_cbBuf - m_qwOffset);

    PSLASSERT( cbToXfer <= cbBuf );

    if( 0 == ReadFile( m_hFile, pvBuf, cbToXfer, &cbRead, NULL ))
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        status = PSLSTATUS_FROM_HRESULT( hr );
        goto Exit;
    }

    PSLASSERT( cbRead == cbToXfer );

    /* this releases the buffer when entirely consumed */

    SetOffset( m_qwOffset + cbToXfer );

    *pcbWritten = cbToXfer;

    PSLASSERT( m_cbBuf >= m_qwOffset);

    *pcbLeft = m_cbBuf - m_qwOffset;

    status = PSLSUCCESS;

Exit:
    return status;
}

void ObjectThumbData::SetOffset( PSLUINT32 qwOffset )
{
    PSLASSERT( qwOffset <= m_cbBuf );

    m_qwOffset = qwOffset;

    if( m_qwOffset == m_cbBuf )
    {
        PSLASSERT( m_pbBuf == PSLNULL );
        SAFE_CLOSEFILE( m_hFile );
    }
}

PSLSTATUS _RemoveDirectoryRecur(LPCWSTR wszDirPath)
{
    PSLSTATUS status;
    DWORD dwAttributes;
    SHFILEOPSTRUCT shFileOp = {0};
    WCHAR szPath[MAX_PATH + 1] = {0};
    HRESULT hr;
    int nRet;

    if (PSLNULL == wszDirPath)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    dwAttributes = GetFileAttributes((LPCWSTR)wszDirPath);

    if((INVALID_FILE_ATTRIBUTES == dwAttributes) ||
       (0 == (FILE_ATTRIBUTE_DIRECTORY & dwAttributes)))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    hr = StringCchCopy(szPath, MAX_PATH,
                       (LPCWSTR)wszDirPath);
    if (FAILED(hr))
    {
        status = PSLSTATUS_FROM_HRESULT(hr);
        goto Exit;
    }
    hr = StringCchCat(szPath, MAX_PATH, L"\\*\0");
    if (FAILED(hr))
    {
        status = PSLSTATUS_FROM_HRESULT(hr);
        goto Exit;
    }
    shFileOp.wFunc = FO_DELETE;
    shFileOp.pFrom = (LPCWSTR)szPath;
    shFileOp.fFlags = FOF_SILENT|FOF_NOCONFIRMATION;

    /* Remove the directory recursively */
    nRet = SHFileOperation(&shFileOp);
    PSLASSERT(0 == nRet);

    if (0 == RemoveDirectory((LPCWSTR)wszDirPath))
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        status = (E_ACCESSDENIED == hr) ?
                    PSLERROR_ACCESS_DENIED :
                    PSLSTATUS_FROM_HRESULT(hr);
        goto Exit;
    }

    status = PSLSUCCESS;
Exit:
    return status;
}

ObjectPropData::ObjectPropData(DBContext* pDBContext,
                               PSLUINT32 dwDataFormat)
 : DBContextData( pDBContext, dwDataFormat )
{

}

ObjectPropData::~ObjectPropData()
{

}

PSLSTATUS ObjectPropData::Initialize(PSL_CONST MTPDBVARIANTPROP* pmqpl,
                                    PSLUINT32 cmqpl)
{
    PSLSTATUS status;

    switch (MTPDBCONTEXTDATADESC_FORMAT(m_dwDataFormat))
    {
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPSSUPPORTED:
        PSLASSERT(0 == cmqpl);
        status = PSLSUCCESS;
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPDESC:
        PSLASSERT(1 == cmqpl);
        PSLASSERT(MTP_DATATYPE_UINT32 == pmqpl[0].wDataType);
        m_wPropCode = (PSLUINT16)pmqpl[0].vParam.valUInt32;
        status = PSLSUCCESS;
        break;

    default:
        PSLASSERT(PSLFALSE);
        status = PSLERROR_INVALID_PARAMETER;
        break;
    }
    return status;
    cmqpl;
}

PSLSTATUS ObjectPropData::Pack(PSLBYTE** ppbBuff, PSLUINT32* pcbSize)
{
    PSLSTATUS status;

    switch (MTPDBCONTEXTDATADESC_FORMAT(m_dwDataFormat))
    {
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPSSUPPORTED:
        status = GetObjectPropsSupportedPack(ppbBuff, pcbSize);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPDESC:
        status = GetObjectPropDescPack(ppbBuff, pcbSize);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        break;

    case MTPDBCONTEXTDATAFORMAT_FORMATCAPABILITIES:
        status = GetFormatCapabilitiesPack(ppbBuff, pcbSize);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        break;

    default:
        PSLASSERT(PSLFALSE);
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS ObjectPropData::GetObjectPropsSupportedPack(
                        PSLBYTE** ppbBuff, PSLUINT32* pcbSize)
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    PSLUINT32 cbSize;
    PSLBYTE* pbBuf = PSLNULL;

    if (PSLNULL != ppbBuff)
    {
        *ppbBuff = PSLNULL;
    }

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    if (PSLNULL == ppbBuff || PSLNULL == pcbSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cPropList);
    PSLASSERT(MTP_OBJECTPROPCODE_OBJECTFORMAT == pPropList[0].wPropCode);
    PSLASSERT(MTP_DATATYPE_UINT16 == pPropList[0].wDataType);

    status = MTPDBObjectPropCodesGet((PSLUINT32)pPropList[0].vParam.valUInt16,
                                    PSLNULL, 0, &cbSize);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pbBuf = (PSLBYTE*)PSLMemAlloc(PSLMEM_FIXED, cbSize);
    if (PSLNULL == pbBuf)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = MTPDBObjectPropCodesGet((PSLUINT32)pPropList[0].vParam.valUInt16,
                                    pbBuf, cbSize, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *ppbBuff = pbBuf;
    pbBuf = PSLNULL;

    *pcbSize = cbSize;

Exit:
    SAFE_PSLMEMFREE(pbBuf);
    return status;
}

PSLSTATUS ObjectPropData::GetObjectPropDescPack(
                        PSLBYTE** ppbBuff, PSLUINT32* pcbSize)
{
    PSLSTATUS status;
    MTPDBVARIANTPROP* pPropList;
    PSLUINT32 cPropList;
    PSLUINT32 cbSize;
    PSLBYTE* pbBuf = PSLNULL;
    PSLUINT32 rgwCodes[2];

    if (PSLNULL != ppbBuff)
    {
        *ppbBuff = PSLNULL;
    }

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    if (PSLNULL == ppbBuff || PSLNULL == pcbSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = m_pDBContext->GetQueryPropList(&pPropList, &cPropList);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(1 == cPropList);

    /* object property code */
    rgwCodes[0] = m_wPropCode;

    /* format code */
    PSLASSERT(MTP_OBJECTPROPCODE_OBJECTFORMAT == pPropList[0].wPropCode);
    PSLASSERT(MTP_DATATYPE_UINT16 == pPropList[0].wDataType);
    rgwCodes[1] = pPropList[0].vParam.valUInt16;

    status = MTPDBObjectPropDescGet(rgwCodes, 2, PSLNULL,
                                    0, &cbSize);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pbBuf = (PSLBYTE*)PSLMemAlloc(PSLMEM_PTR, cbSize);
    if (PSLNULL == pbBuf)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = MTPDBObjectPropDescGet(rgwCodes, 2, pbBuf,
                                    cbSize, pcbSize);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *ppbBuff = pbBuf;
    pbBuf = PSLNULL;

    *pcbSize = cbSize;

Exit:
    SAFE_PSLMEMFREE(pbBuf);
    return status;
}

PSLSTATUS ObjectPropData::GetFormatCapabilitiesPack(
                        PSLBYTE** ppbBuff, PSLUINT32* pcbSize)
{
    PSLSTATUS status;
    PSLUINT32 cbSize;
    PSLUINT64 qwSize;
    PSLBYTE* pbBuf = PSLNULL;
    PSLUINT16 wFormatCode;
    PSLUINT32 cFormats;
    PSL_CONST MTPFORMATINFO* rgFormats;
    MTPDBCONTEXTDATA mdbcd = PSLNULL;

    if (PSLNULL != ppbBuff)
    {
        *ppbBuff = PSLNULL;
    }

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    if (PSLNULL == ppbBuff || PSLNULL == pcbSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    wFormatCode = m_pDBContext->GetFormateCode();

    status = MTPDBGetFormatList(&rgFormats, &cFormats);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*  Use the helper function to create a "context data object" that can
     *  serialize this data set
     */

    status = MTPFormatCapabilitiesContextDataCreate(rgFormats, cFormats,
                            MAKE_MTPDBCONTEXTDATADESC(MTPDBCONTEXTDATA_QUERY,
                                    MTPDBCONTEXTDATAFORMAT_FORMATCAPABILITIES),
                            wFormatCode, 0, PSLNULL, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*  Determine how large a buffer we need
     */

    status = MTPDBContextDataSerialize(mdbcd, NULL, 0, NULL, &qwSize);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*  Allocate it
     */

    PSLASSERT((0xffffffff & qwSize) == qwSize);
    pbBuf = (PSLBYTE*)PSLMemAlloc(PSLMEM_FIXED, (PSLUINT32)qwSize);
    if (PSLNULL == pbBuf)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /*  And serialize it for real
     */

    status = MTPDBContextDataSerialize(mdbcd, pbBuf, (PSLUINT32)qwSize, &cbSize,
                            &qwSize);
    if (PSL_FAILED(status) || (0 != qwSize))
    {
        PSLASSERT(PSL_FAILED(status));
        status = PSL_FAILED(status) ? status : PSLERROR_UNEXPECTED;
        goto Exit;
    }

    *ppbBuff = pbBuf;
    pbBuf = PSLNULL;

    *pcbSize = cbSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_PSLMEMFREE(pbBuf);
    return status;
}
