/*
 *  MTPPerfLogWriter.c
 *
 *  Contains Windows implementation of MTP Performance Log
 *  Writer interfaces.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSL.h"
#include "MTP.h"
#include "MTPUtils.h"
#include "MTPPerfLogWriter.h"
#include <stdio.h>

typedef struct _WRTIEROBJ
{
    FILE                         *stream;
    PSLSTR                       sLogName;
    DEVICE_TRACE_FILE_HEADER     traceFileHeader;
} WRITEROBJ, *PWRITEROBJ;

#define WriteUint32ToFile(_val, _file) \
{ \
    PSLUINT32   Temp; \
    Temp = MTPSwapUInt32((PSLUINT32)_val); \
    fwrite(&Temp, sizeof(PSLUINT32), 1, _file); \
}

#define WriteUint64ToFile(_pval, _file) \
{ \
    PSLUINT64   Temp; \
    MTPStoreUInt64(&Temp, (PSLUINT64*)_pval); \
    fwrite(&Temp, sizeof(PSLUINT64), 1, _file); \
}

/*
 *  _MTPPerfLogWriterUpdateFileHeader
 *
 *  Update trace file headder.
 *
 *  Arguments:
 *          PWRITEROBJ    pWriterObj
 *
 *  Returns:
 *
 */
static PSLVOID _MTPPerfLogWriterUpdateFileHeader(PWRITEROBJ pWriterObj)
{
    if (PSLNULL == pWriterObj || PSLNULL == pWriterObj->stream)
    {
        goto Exit;
    }

    fseek(pWriterObj->stream, 0, SEEK_SET);

    WriteUint32ToFile(pWriterObj->traceFileHeader.dwVersion,
                      pWriterObj->stream);
    WriteUint32ToFile(pWriterObj->traceFileHeader.dwNumOfProcessors,
                      pWriterObj->stream);
    WriteUint64ToFile(&pWriterObj->traceFileHeader.HostStartTime,
                      pWriterObj->stream);
    WriteUint64ToFile(&pWriterObj->traceFileHeader.HostStopTime,
                      pWriterObj->stream);
    WriteUint32ToFile(pWriterObj->traceFileHeader.dwTimerResolution,
                      pWriterObj->stream);
    WriteUint64ToFile(&pWriterObj->traceFileHeader.CPUFrequency,
                      pWriterObj->stream);
    WriteUint64ToFile(&pWriterObj->traceFileHeader.StartTimestamp,
                      pWriterObj->stream);
    WriteUint64ToFile(&pWriterObj->traceFileHeader.StopTimestamp,
                      pWriterObj->stream);
    WriteUint32ToFile(pWriterObj->traceFileHeader.dwEventLost,
                      pWriterObj->stream);
    WriteUint32ToFile(pWriterObj->traceFileHeader.dwNumOfEvents,
                      pWriterObj->stream);

    fseek(pWriterObj->stream, 0, SEEK_END);
Exit:
    return;
}

/*
 *  MTPPerfLogWriterCreate
 *
 *  Create log writer object.
 *
 *  Arguments:
 *          MTPPERLOGWRITER    *ppWriter    pointer to hole returned
 *                                          log writer object handle
 *
 *  Returns:
 *
 */

PSLVOID MTPPerfLogWriterCreate(MTPPERLOGWRITER* ppWriter)
{
    PWRITEROBJ  pWriterObj;

    pWriterObj = (PWRITEROBJ)PSLMemAlloc(PSLMEM_PTR,
                            sizeof(WRITEROBJ));

    PSLASSERT(PSLNULL != pWriterObj);

    PSLSetMemory(pWriterObj, 0, sizeof(WRITEROBJ));

    *ppWriter = pWriterObj;
}

/*
 *  MTPPerfLogWriterCreate
 *
 *  Destroy log writer object.
 *
 *  Arguments:
 *          MTPPERLOGWRITER    pWriter    log writer object handle
 *
 *  Returns:
 *
 */

PSLVOID MTPPerfLogWriterDestroy(MTPPERLOGWRITER pWriter)
{
    MTPPerfLogWriterClose(pWriter);
    SAFE_PSLMEMFREE(pWriter);
}


/*
 *  MTPPerfLogWriterWriteBuffer
 *
 *  Write a buffer into log file.
 *
 *  Arguments:
 *          MTPPERLOGWRITER    pWriter    log writer object handle
 *          PSLVOID            *pData     Data blob
 *          PSLUIN32           Len        length of data
 *
 *  Returns:
 *
 */

PSLVOID MTPPerfLogWriterWriteBuffer(MTPPERLOGWRITER pWriter,
                                    PSLVOID* pData, PSLUINT32 Len)
{
    fwrite(pData, 1, Len, ((PWRITEROBJ)pWriter)->stream);
}


/*
 *  MTPPerfLogWriterCreate
 *
 *  Open log writer object.
 *
 *  Arguments:
 *          MTPPERLOGWRITER    pWriter    log writer object handle
 *          PSLCSTR            LogName    log objet identifier
 *          PDEVICE_TRACE_FILE_HEADER
 *                                              *ppTraceFileHeader   pointer to
 *                                               hold trace file header blob.
 *
 *  Returns:
 *
 */
PSLVOID MTPPerfLogWriterOpen(MTPPERLOGWRITER pWriter,
                                 PSLCSTR LogName,
                                 PDEVICE_TRACE_FILE_HEADER *ppTraceFileHeader)
{
    PSLUINT32   cchLen;
    PWRITEROBJ  pWriterObj = (PWRITEROBJ)pWriter;

    MTPPerfLogWriterClose(pWriter);

    PSLSetMemory(pWriterObj, 0, sizeof(WRITEROBJ));

    PSLStringLenA(LogName, PSLSTRING_MAX_CCH, &cchLen);
    pWriterObj->sLogName = (PSLSTR)PSLMemAlloc(PSLMEM_PTR, cchLen + 1);
    PSLStringCopyA(pWriterObj->sLogName, cchLen + 1, LogName);

    fopen_s(&pWriterObj->stream, LogName, "wb+");

    _MTPPerfLogWriterUpdateFileHeader(pWriterObj);

    *ppTraceFileHeader = &pWriterObj->traceFileHeader;
}

/*
 *  MTPPerfLogWriterCreate
 *
 *  Close log writer object.
 *
 *  Arguments:
 *          MTPPERLOGWRITER    pWriter    log writer object handle
 *
 *  Returns:
 *
 */
PSLVOID MTPPerfLogWriterClose(MTPPERLOGWRITER pWriter)
{
    PWRITEROBJ  pWriterObj = (PWRITEROBJ)pWriter;

    SAFE_PSLMEMFREE(pWriterObj->sLogName);

    if (NULL != pWriterObj->stream)
    {
        _MTPPerfLogWriterUpdateFileHeader(pWriterObj);
        fclose(pWriterObj->stream);
        pWriterObj->stream = PSLNULL;
    }
}

