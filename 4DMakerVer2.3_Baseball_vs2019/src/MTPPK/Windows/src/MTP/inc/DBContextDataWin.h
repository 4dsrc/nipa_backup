/*
 *  DBContextData.h
 *
 *  Contains declarations of the DBContextData class hierarchy
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _DBCONTEXTDATA_H_
#define _DBCONTEXTDATA_H_

class DBContext;

class DBContextData : public CPSLUnknown,
                      public MTPDBCONTEXTDATAOBJ
{
public:
    DBContextData(
                            DBContext* pDBContext,
                            PSLUINT32 dwDataFormat);
    virtual ~DBContextData();

    // DBContextData

    static PSLSTATUS Create(
                            DBContext* pDBContext,
                            PSLUINT32 dwDataFormat,
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList,
                            DBContextData** ppDBContextData );

    virtual PSLSTATUS Initialize(
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList ) = 0;

    virtual PSLSTATUS Cleanup();

    virtual PSLSTATUS Terminate();

    PSLSTATUS GetDBContext(
                            DBContext** ppDBContext );

    virtual PSLSTATUS Pack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize) = 0;

    virtual PSLSTATUS GetPackedSize(
                            PSLUINT64* pqwSize );

    virtual PSLSTATUS Serialize(
                            PSLVOID* pvBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten,
                            PSLUINT64* pcbLeft);

    virtual PSLSTATUS Deserialize(
                            PSL_CONST PSLVOID* pvBuf,
                            PSLUINT32 cbBuf);

    virtual void SetOffset(
                            PSLUINT32 qwOffset);

    virtual PSLSTATUS ResolveContext(
                            PSLBOOL* pfCommitRequired);

    // IUnknown
    DELEGATE_IUNKNOWN( CPSLUnknown );
protected:
    PSLSTATUS StashPropList(
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList);

    PSLSTATUS GetObjectFileHandle(
                            HANDLE* phFile,
                            PSLUINT32 cbOffset,
                            PSLUINT32 cbTotal,
                            PSLUINT32* pcbSize );

    PSLSTATUS SetObjectFileHandle(
                            HANDLE* phFile,
                            PSLUINT32 cbSize );

    PSLSTATUS InsertObject(
                            PSLUINT32 dwDSDataFormat);

    PSLSTATUS DeleteObject();

public:
    PSLUINT32 m_dwDataFormat;

protected:
    PSLBYTE* m_pbBuf;
    PSLUINT32 m_cbBuf;
    DBContext* m_pDBContext;
    PSLUINT32 m_qwOffset;
    MTPDBVARIANTPROP* m_pOutPropQuads;
    PSLUINT32 m_cOutPropQuads;
};

class StorageInfoData : public DBContextData
{
public:
    StorageInfoData(
                            DBContext* pDBContext,
                            PSLUINT32 dwDataFormat );
    ~StorageInfoData();

    virtual PSLSTATUS Initialize(
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList );

    virtual PSLSTATUS Pack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize );

    virtual PSLSTATUS ResolveContext(
                            PSLBOOL* pfCommitRequired);

private:
    PSLSTATUS StorageInfoPack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize);
    PSLSTATUS StorageIDsPack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize);


private:
    DWORD m_dwFileSystemType;
};

class ObjectHandleData : public DBContextData
{
public:
    ObjectHandleData(
                            DBContext* pDBContext,
                            PSLUINT32 dwDataFormat );
    ~ObjectHandleData();

    virtual PSLSTATUS Initialize(
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList );
    virtual PSLSTATUS Pack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize );
    virtual PSLSTATUS ResolveContext(
                            PSLBOOL* pfCommitRequired);
};

class ObjectPropArrayData : public DBContextData
{
public:
    ObjectPropArrayData(
                            DBContext* pDBContext,
                            PSLUINT32 dwDataFormat );
    ~ObjectPropArrayData();

    virtual PSLSTATUS Initialize(
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList );
    virtual PSLSTATUS Pack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize );

    virtual PSLSTATUS ResolveContext(
                            PSLBOOL* pfCommitRequired);
};

class ObjectInfoData : public DBContextData
{
public:
    ObjectInfoData(
                            DBContext* pDBContext,
                            PSLUINT32 dwDataFormat );
    ~ObjectInfoData();

    virtual PSLSTATUS Initialize(
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList );
    virtual PSLSTATUS Pack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize );

    virtual PSLSTATUS ResolveContext(
                            PSLBOOL* pfCommitRequired );
};

class ObjectPropListData : public DBContextData
{
public:
    ObjectPropListData(
                            DBContext* pDBContext,
                            PSLUINT32 dwDataFormat );
    ~ObjectPropListData();

    virtual PSLSTATUS Initialize(
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList );
    virtual PSLSTATUS Pack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize);

    virtual PSLSTATUS ResolveContext(
                            PSLBOOL* pfCommitRequired );

private:
    PSLSTATUS ResolveSetObjectPropList(
                            PSLBOOL* pfCommitRequired);
    PSLSTATUS ResolveSendObjectPropList(
                            PSLBOOL* pfCommitRequired);
};

class ObjectBinaryData : public DBContextData
{
public:
    ObjectBinaryData(
                            DBContext* pDBContext,
                            PSLUINT32 dwDataFormat );
    ~ObjectBinaryData();

    virtual PSLSTATUS GetPackedSize(
                            PSLUINT64* pqwSize );

    virtual PSLSTATUS Initialize(
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList );

    virtual PSLSTATUS Cleanup();
    virtual PSLSTATUS Terminate();

    virtual PSLSTATUS Pack(
                            PSLBYTE** ppbBuff, PSLUINT32* pcbSize );

    virtual PSLSTATUS ResolveContext(
                            PSLBOOL* pfCommitRequired );

    virtual PSLSTATUS Serialize(PSLVOID* pvBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten,
                            PSLUINT64* pcbLeft);

    virtual PSLSTATUS Deserialize(PSL_CONST PSLVOID* pvBuf,
                            PSLUINT32 cbBuf);

    virtual void SetOffset(
                            PSLUINT32 qwOffset);

private:
    PSLSTATUS DeleteObject();

private:
    HANDLE m_hFile;
};

class ObjectThumbData : public DBContextData
{
public:
    ObjectThumbData(
                            DBContext* pDBContext,
                            PSLUINT32 dwDataFormat );
    ~ObjectThumbData();

    virtual PSLSTATUS GetPackedSize(
                            PSLUINT64* pqwSize );

    virtual PSLSTATUS Initialize(
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList );

    virtual PSLSTATUS Cleanup();

    virtual PSLSTATUS Pack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize );

    virtual PSLSTATUS Serialize(PSLVOID* pvBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbWritten,
                            PSLUINT64* pcbLeft);

    virtual void SetOffset(
                            PSLUINT32 qwOffset);

private:
    HANDLE m_hFile;
};

class ObjectPropData : public DBContextData
{
public:
    ObjectPropData(
                            DBContext* pDBContext,
                            PSLUINT32 dwDataFormat);
    ~ObjectPropData();

    PSLSTATUS Initialize(
                            PSL_CONST MTPDBVARIANTPROP* pmqpl,
                            PSLUINT32 cmqpl);

    PSLSTATUS Pack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize);

private:
    PSLUINT16 m_wPropCode;

private:
    PSLSTATUS GetObjectPropsSupportedPack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize);
    PSLSTATUS GetObjectPropDescPack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize);
    PSLSTATUS GetFormatCapabilitiesPack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize);
};
#endif // _DBCONTEXTDATA_H_

