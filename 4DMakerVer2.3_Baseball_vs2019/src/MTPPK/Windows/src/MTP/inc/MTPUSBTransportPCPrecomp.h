/*
 *  MTPUSBTransportPCPrecomp.h
 *
 *  Precompiled header file for the MTP-USB Transport component on Windows
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPUSBTRANSPORTPCPRECOMP_H_
#define _MTPUSBTRANSPORTPCPRECOMP_H_

#include "PSLWindows.h"
#include "MTPUSBTransportPrecomp.h"
#include "MTPUSBLoader.h"

#endif  /* _MTPUSBTRANSPORTPCPRECOMP_H_ */

