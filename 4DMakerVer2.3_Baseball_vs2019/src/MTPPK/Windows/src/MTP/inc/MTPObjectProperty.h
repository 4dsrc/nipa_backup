/*
 *  MTPObjectProperty.h
 *
 *  Contains declaration for the fucntions that support
 *  object property related support funtions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPOBJECTPROPERTY_H_
#define _MTPOBJECTPROPERTY_H_


/*
 * This function returns the property get/set access flag
 *
 * Arguments:
 *      PSLUINT32   wPropCode
 *      PSLUINT8*   pbGetSet
 *
 * Returns:
 *      PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if not found
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBObjectPropertyGetAccess(
                            PSLUINT16 wPropCode,
                            PSLUINT8* pbGetSet);

/*
 * This function will serialize object property description.
 *
 *  Arguments:
 *      PSLUINT32*  pdwCodes
 *      PSLUINT32   cCodes
 *      PSLBYTE*    pBuf
 *      PSLUINT32   cbBuf
 *      PSLUINT32*  pcbSize
 *
 *  Returns:
 *      PSLSUCCESS on success
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPDBObjectPropDescGet(
                            PSLUINT32* pdwCodes,
                            PSLUINT32 cCodes,
                            PSLBYTE* pBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbSize);

/*
 * This function will serialize supported object property codes.
 *
 *  Arguments:
 *      PSLUINT32*  pdwCodes
 *      PSLUINT32   cCodes
 *      PSLBYTE*    pBuf
 *      PSLUINT32   cbBuf
 *      PSLUINT32*  pcbSize
 *
 *  Returns:
 *      PSLSUCCESS on success
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPDBObjectPropCodesGet(
                            PSLUINT32 dwFormatCode,
                            PSLBYTE* pBuf,
                            PSLUINT32 cbBuf,
                            PSLUINT32* pcbSize);

/*
 *
 *  This function will return an array of object prop codes
 *  given the format code and group code. If the group code
 *  is MTP_GROUPCODE_NONE and fIgnoreSlowOnes is PSLFLASE
 *  all object prop codes that correspond to dwFormatCode
 *  will be returned.
 *
 *  Returns:
 *
 *  PSLSUCCESS on success
 *  MTPERROR_PROPERTY_FORMATCODE_UNSUPPORTED if the function is unable
 *  to find the format code in the device.
 *  PSLERROR_NOT_AVAILABLE if there is no property codes associated
 *  with the format code.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPDBGetObjectPropCodes(
                            PSLUINT32 dwFormatCode,
                            PSLUINT32 dwGroupCode,
                            PSLBOOL fIgnoreSlowOnes,
                            PSLDYNLIST dlObjPropCodes);

/*
 *
 *  This function will return PSLSUCCESS if the object propcode is
 *  valid corresponding to the format code.
 *
 *  Returns:
 *
 *  PSLSUCCESS if the object property code is associated with
 *  the format code.
 *  PSLSUCCESS_FALSE if the object property code is not associated
 *  with the format code.
 *  MTPERROR_PROPERTY_FORMATCODE_UNSUPPORTED if the function is unable
 *  to find the format code in the device.
 *
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPDBIsObjectPropCodeValid(
                            PSLUINT16 wObjectPropCode,
                            PSLUINT32 dwFormatCode);

/*
 *
 *  This function will return PSLSUCCESS if the format code is
 *  supported by the device.
 *
 *  Returns:
 *
 *  PSLSUCCESS if the format code is supported.
 *  PSLSUCCESS_FALSE if the format code is not supported.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPDBIsFormatCodeSupported(
                            PSLUINT32 dwFormatCode);

/*
 *
 *  This function will return the supported format codes and the count
 *  pdwFormatCodes and pcFormatCodes respectively.
 *
 *  Returns:
 *
 *  PSLSUCCESS on success.
 *  PSLERROR_INVALID_PARAMETER if the input pointers are PSLNULL.
 *  PSLERROR_NOT_AVAILABLE if the no format codes area available
 *  PSLERROR_OUT_OF_MEMORY if the memory allocation of format code
 *  array failed.
 *
 *  Remark:
 *
 *  The caller of this API should call SAFE_PSLMEMALLOCFREE to free
 *  pointer to the format code array passed to the function.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPDBGetSupportedFormatCodes(
                            PSLUINT32** ppdwFormatCodes,
                            PSLUINT32* pcFormatCodes);

/*
 *  This function returns a pointer to the list of MTP formats supported by
 *  the service and the number of formats.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBGetFormatList(
                            PSL_CONST MTPFORMATINFO** ppfiList,
                            PSLUINT32* pcFormats);

#endif /*_MTPOBJECTPROPERTY_H_*/
