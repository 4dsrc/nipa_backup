/*
 *  MTPUSBService.h
 *
 *  Contains definitions of the MTP-USB service for running within the
 *  context of services.exe.  The primary responsibility of the service
 *  is to create a UPnP device endpoint that can be recognized by a discovering
 *  device and to allow for binding to that device.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPUSBSERVICE_H_
#define _MTPUSBSERVICE_H_

// Startup and Shutdown the Service

EXTERN_C DWORD WINAPI MTPUSB_Init(
                            DWORD dwData);

EXTERN_C BOOL WINAPI MTPUSB_Deinit(
                            DWORD dwData);

EXTERN_C BOOL WINAPI MTPUSB_QueryCanShutdown(
                            DWORD dwData);

#endif  // _MTPUSBSERVICE_H_

