/*
 *  FileUtils.h
 *
 *  Contains declarations of the file utility functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _FILEUTILS_H_
#define _FILEUTILS_H_

//BOOL IsCompactFlashPath(LPCTSTR lpszFileName, DWORD dwFileAttributes);
BOOL IsRemovable(LPCWSTR wszPath);

LPCWSTR FindFileName(LPCWSTR szPath);
LPCWSTR FindFileExtension(LPCWSTR szPath);

#endif /* _FILEUTILS_H_ */

