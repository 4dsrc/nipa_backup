/*
 *  MTPDataStore.h
 *
 *  Contains declaration of MTP data store.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPDATASTORE_H_
#define _MTPDATASTORE_H_

typedef PSLHANDLE MTPDATASTORE;

typedef PSLHANDLE MTPITEM;

enum
{
    MTPDSDATAFORMAT_NONE = 0,
    MTPDSDATAFORMAT_ITEMID,
    MTPDSDATAFORMAT_FILEPATH,
    MTPDSDATAFORMAT_STORAGEINFO,
    MTPDSDATAFORMAT_OBJECTINFO,
    MTPDSDATAFORMAT_OBJECTPROPLIST,
    MTPDSDATAFORMAT_MAX = 0xffff
};

enum
{
    MTPDSDATAFLAG_NONE = 0,
    MTPDSDATAFLAG_FORCEUPDATE,
    MTPDSDATAFLAG_MAX = 0xffff,
};

#define MTPDSDATAFORMAT_MASK    0x0000ffff
#define MTPDSDATAFLAG_MASK      0xffff0000

#define MAKE_MTPDSDATA(_flags, _format) \
        (PSLUINT32)(((MTPDSDATAFORMAT_MASK & (_flags)) << 16) | \
                    (MTPDSDATAFORMAT_MASK & (_format)))

#define MTPDSDATAFORMAT(_mdsdata) \
        (PSLUINT16)(MTPDSDATAFORMAT_MASK & _mdsdata)

#define MTPDSDATAFLAG(_mdsdata) \
        (PSLUINT16)((MTPDSDATAFLAG_MASK & _mdsdata) >> 16)

enum
{
    MTPDSITEMTYPE_STORE = 0,
    MTPDSITEMTYPE_OBJECT = 1,
};

PSL_EXTERN_C PSLSTATUS PSL_API MTPDataStoreCreate(
                            MTPDATASTORE* pmds);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDataStoreDestroy(
                            MTPDATASTORE mds);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDataStoreQuery(
                            MTPDATASTORE mds,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            PSLUINT32 dwItemType,
                            MTPITEM** ppItemList,
                            PSLUINT32* pcItems);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDataStoreInsert(
                            MTPDATASTORE mds,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            PSLUINT32 dwItemType,
                            MTPITEM** ppItemList,
                            PSLUINT32* pcItems);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDataStoreDelete(
                            MTPITEM mItem);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDataStoreGetProp(
                            MTPITEM mItem,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            PSLUINT32 dwDataFormat,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbSize,
                            PSLUINT32* pcbWritten,
                            PSLUINT32* pcbLeft);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDataStoreSetProp(
                            MTPITEM mItem,
                            PSL_CONST MTPDBVARIANTPROP* pmqList,
                            PSLUINT32 cmqList,
                            PSLUINT32 dwDataFormat,
                            PSLBYTE* pbBuf,
                            PSLUINT32 cbSize);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDataStoreAddStore(
                            MTPDATASTORE mds,
                            PSLWCHAR* szStorePath,
                            PSLUINT32 dwPathLen);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDataStoreRemoveStore(
                            MTPDATASTORE mds,
                            PSLWCHAR* szStorePath,
                            PSLUINT32 dwPathLen);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDataStoreGetMTPObject(
                            MTPDATASTORE mds,
                            PSLUINT32 dwObjectHandle,
                            MTPITEM* pmtpObject);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDataStoreGetMTPStore(
                            MTPDATASTORE mds,
                            PSLUINT32 dwStoreId,
                            MTPITEM* pmtpStore);


/*
 *  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#define SAFE_MTPDATASTOREDESTROY(_mtpds) \
if (PSLNULL != _mtpds) \
{ \
    MTPDataStoreDestroy(_mtpds); \
    _mtpds = PSLNULL; \
}

#endif  /*_MTPDATASTORE_H_*/
