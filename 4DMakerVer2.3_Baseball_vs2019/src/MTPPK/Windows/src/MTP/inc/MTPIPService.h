/*
 *  MTPIPService.h
 *
 *  Contains definitions of the MTP-IP service for running within the
 *  context of services.exe.  The primary responsibility of the service
 *  is to create a UPnP device endpoint that can be recognized by a discovering
 *  device and to allow for binding to that device.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPIPSERVICE_H_
#define _MTPIPSERVICE_H_

// MTP-IP default TCP-IP port

#define IPPORT_MTPIP        15740

// MTP-IP IOCTLs

#define IOCTL_MTPIP_REGISTER_SOCKADDR       1
#define IOCTL_MTPIP_DEREGISTER_SOCKADDR     2
#define IOCTL_MTPIP_STARTED                 3
#define IOCTL_MTPIP_STOPPING                4
#define IOCTL_MTPIP_CHECK_LOCAL_UDN         5

// Startup and Shutdown the Service

EXTERN_C DWORD WINAPI MTPIP_Init(
                            DWORD dwData);

EXTERN_C BOOL WINAPI MTPIP_Deinit(
                            DWORD dwData);

// Manage the Service

EXTERN_C BOOL WINAPI MTPIP_IOControl(
                            DWORD dwData,
                            DWORD dwCode,
                            PBYTE pBufIn,
                            DWORD dwLenIn,
                            PBYTE pBufOut,
                            DWORD dwLenOut,
                            PDWORD pdwActualOut);

// Open or close an instance of the service

EXTERN_C DWORD WINAPI MTPIP_Open(
                            DWORD dwData,
                            DWORD dwAccess,
                            DWORD dwShareMode);

EXTERN_C BOOL WINAPI MTPIP_Close(
                            DWORD dwData);

#endif  // _MTPIPSERVICE_H_

