/*
 *  MTPDatabaseUtil.h
 *
 *  Contains definitions of the MTP DB handler for various data formats
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPDATABASEUTIL_H_
#define _MTPDATABASEUTIL_H_

PSL_EXTERN_C PSLSTATUS PSL_API MTPStorageInfoPack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize,
                            PSLVOID* pvData);

PSL_EXTERN_C PSLSTATUS PSL_API MTPObjectInfoPack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize,
                            PSLVOID* pvData);

PSL_EXTERN_C PSLSTATUS PSL_API MTPObjectHandlesPack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize,
                            PSLVOID* pvData);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStorageIDsPack(
                            PSLBYTE** ppbBuff,
                            PSLUINT32* pcbSize,
                            PSLVOID* pvData);

#endif /*_MTPDATABASEUTIL_H_*/

