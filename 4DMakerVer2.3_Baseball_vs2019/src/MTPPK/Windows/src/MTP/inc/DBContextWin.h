/*
 *  DBContext.h
 *
 *  Contains declarations of the DBSession and DBContext classes
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _DBCONTEXT_H_
#define _DBCONTEXT_H_

#define MTPDB_DATASIZE_UNKNOWN      0xffffffff

PSL_EXTERN PSL_CONST MTPDBSESSIONVTBL* GetDBSessionVTable();
PSL_EXTERN PSL_CONST MTPDBCONTEXTVTBL* GetDBContextVTable();
PSL_EXTERN PSL_CONST MTPDBCONTEXTDATAVTBL* GetDBContextDataVTable();

class DBSession : public CPSLUnknown,
                  public MTPDBSESSIONOBJ
{
private:
    DBSession(
                            MTPDATASTORE mds,
                            PSLMSGPOOL mpHandle);
    ~DBSession();

public:
    static PSLSTATUS Create(
                            DBSession** ppDBSession);

    PSLSTATUS Cleanup();

    DELEGATE_IUNKNOWN(CPSLUnknown);

    PSLSTATUS GetMsgPool(
                            PSLMSGPOOL* pmpHandle);

    PSLSTATUS GetDataStore(
                            MTPDATASTORE* pmds);

    PSLSTATUS SetObjectInfoHandleAndSize(
                            PSLUINT32 dwObjectHandle,
                            PSLUINT32 dwObjectSize);

    PSLSTATUS GetObjectInfoHandleAndSize(
                            PSLUINT32* pdwObjectHandle,
                            PSLUINT32* pdwObjectSize);

    PSLSTATUS ResolveObjectHandleToStorageId(
                            PSLUINT32 dwObjectHandle,
                            PSLUINT32* pdwStorageID);

private:
    MTPDATASTORE m_mds;
    PSLMSGPOOL m_mpHandle;
    PSLUINT32 m_dwObjectInfoHandle;
    PSLUINT32 m_dwObjectInfoSize;
};

class DBContextData;

class DBContext : public CPSLUnknown,
                  public MTPDBCONTEXTOBJ
{
private:
    DBContext(
                            DBSession* pDBSession);
    ~DBContext();

public:
    static PSLSTATUS Create(
                            DBSession* pDBSession,
                            DBContext** ppDBContext );

    PSLSTATUS Initialize();
    PSLSTATUS Cleanup();

    PSLSTATUS GetDBSession(
                            DBSession** ppDBSession );

    PSLSTATUS Initialize(
                            PSLUINT32 dwJoinType,
                            PSL_CONST MTPDBVARIANTPROP* pmqPropList,
                            PSLUINT32 cmqPropList );

    PSLSTATUS AppendContextData(
                            DBContextData* pDBContextData );

    PSLSTATUS Resolve();

    PSLSTATUS Delete();

    PSLSTATUS Copy(
                            DBContext* pDBContextSrc,
                            MTPITEM** ppNewItem );

    PSLSTATUS Move(
                            DBContext* pDBContextSrc );

    PSLSTATUS GetItemHandle(
                            MTPITEM** ppmItem,
                            PSLUINT32* pdwObjectHandle,
                            PSLUINT32* pdwParentHandle,
                            PSLUINT32* pdwStorageID);

    PSLSTATUS GetHandleCount(
                            PSLUINT32* pdwEntries);

    PSLSTATUS GetQueryPropList(
                            MTPDBVARIANTPROP** ppPropList,
                            PSLUINT32 *pcPropList);

    // IUnknown

    DELEGATE_IUNKNOWN( CPSLUnknown );

    // Commit changes to the database
    HRESULT STDMETHODCALLTYPE Commit();

    PSLVOID StashObjectHandle(
                            PSLUINT32 dwObjectHandle);
    PSLUINT32 GetStorageID() { return m_dwStorageID; }
    PSLUINT16 GetFormateCode() { return m_wFormatCode; }

private:
    DBSession* m_pDBSession;
    PSLDYNLIST m_pDataList; /*(DBContextData*)*/

    PSLUINT32 m_dwStorageID;
    PSLUINT32 m_dwObjectHandle;
    PSLUINT32 m_dwParentHandle;
    PSLUINT16 m_wFormatCode;
    PSLUINT32 m_dwDepth;

    PSLBOOL m_fInitialized;
    PSLBOOL m_fResolved;
    PSLBOOL m_fCommitted;
    PSLBOOL m_fPartiallyCommitted;

    MTPDBVARIANTPROP* m_queryPropQuads;
    PSLUINT32 m_cQueryPropQuads;

    PSLUINT32 m_dwObjectHandleCopy;
};

#endif // _DBCONTEXT_H_
