/*
 *  MTPServicesInit.cpp
 *
 *  Contains definition of the MTP Services Initialization API functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLWindows.h"
#include "PSLWinSafe.h"
#include "MTPPrecomp.h"
#include "MTPLegacyService.h"

/*  Local Defines
 */

/*  Local Types
 */

typedef PSLSTATUS (PSL_API * PFNMTPSERVICEINIT)(
                            MTPCONTEXT* pmtpSession,
                            PSLUINT32 dwServiceID,
                            PSLUINT32 dwStorageID,
                            PSLUINT32* pcRegistered);


const PFNMTPSERVICEINIT g_pfnServicesInit[] =
{
    MTPLegacyServiceInit,
    // add more services
};


/*
 *  MTPServicesInitialize
 *
 *  Per session initialization point. Called during MTPSessionOpen.
 *  Similar to MTP DB APIs MTPDBSessionOpen
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx  MTP context in which services are
 *                               to be registered
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS MTPServicesInitialize(MTPCONTEXT* pmtpctx)
{
    PSLUINT32       cRegistered;
    PSLUINT32       idx;
    PSLUINT32       idxService;
    PSLSTATUS       ps;

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    idxService = MAX_MTP_LEGACY_STORAGE_COUNT + 1;

    for (idx = 0; idx < PSLARRAYSIZE(g_pfnServicesInit); idx++)
    {
        PSLASSERT (PSLNULL != g_pfnServicesInit[idx]);

        ps = g_pfnServicesInit[idx](pmtpctx, MAKE_SERVICEID(idxService),
                                    MAKE_STORAGEID(idxService), &cRegistered);
        if (PSL_FAILED(ps))
        {
            PSLTraceError("MTPServicesInitialize: Service %d failed 0x%08x",
                          idx, ps);
        }

        idxService += cRegistered;
        PSLASSERT(idxService <= MAX_MTP_SERVICE_STORAGE_INDEX);
    }

    ps = PSLSUCCESS;

Exit:
    return ps;
}

/*
 *  MTPDBResolveObjectHandleToStorageID
 *
 *  Helper function for retrieving the storage associated with a particular
 *  object hande.
 *
 *  Arguments:
 *      PSLUINT32       dwObjectHandle      Object handle to resolve
 *      PSLUINT32*      pdwStorageID        Storage ID for object
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS MTPDBResolveObjectHandleToStorageID( MTPCONTEXT* pmtpctx,
                                               PSLUINT32 dwObjectHandle,
                                               PSLUINT32* pdwStorageID)
{
    MTPDBSESSION     mtpdbLegacy;
    BASEHANDLERDATA* pbhd;
    PSLSTATUS        ps;

    if (PSLNULL != pdwStorageID)
    {
        *pdwStorageID = MTP_STORAGEID_UNDEFINED;
    }

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the handler information so that we can get to the service
     *  resolver
     */
    pbhd = (BASEHANDLERDATA*)pmtpctx->mtpHandlerState.pbHandlerData;

    /*  And then use it to retrieve the legacy service
     */
    ps = MTPServiceResolverGetLegacyService(pbhd->sesLevelData.msrServices,
                            &mtpdbLegacy);
    if (PSLSUCCESS != ps)
    {
        PSLASSERT(PSL_FAILED(ps));
        ps = PSL_FAILED(ps) ? ps : PSLERROR_UNEXPECTED;
        goto Exit;
    }

    ps = MTPDBSessionGetStorageId(mtpdbLegacy, dwObjectHandle, pdwStorageID);

Exit:
    return ps;
}

