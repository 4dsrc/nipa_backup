/*
 *  MTPDLLEntry.cpp
 *
 *  DLL Entrypoint for MTP Windows DLL
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */
#include "PSLWindows.h"
#include "MTPPrecomp.h"
#include "MTPUSBDevice.h"
#include "MTPUSBLoader.h"
#include "MTPInitializeUtil.h"

HANDLE                      g_hInst;

PSL_EXTERN_C PSLSTATUS PSL_API MTPUninitializeWin();

///////////////////////////////////////////////////////////////////////////////
// We do not implement BT for windows, so do nothing here to make compile work
//
PSL_EXTERN_C PSLSTATUS PSL_API MTPBTTransportInitialize(
                    PSLUINT32 dwInitFlags)
{
    dwInitFlags;
    return PSLERROR_NOT_IMPLEMENTED;
}

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTTransportUninitialize(PSLVOID)
{
    return PSLERROR_NOT_INITIALIZED;
}


/*
 *  DllMain
 *
 *  Standard Windows DLL entry point
 *
 *  Arguments:
 *      See MSDN
 *
 *  Returns:
 *      See MSDN
 *
 */

BOOL WINAPI DllMain(HANDLE hinstDLL, DWORD dwReason, LPVOID lpvReserved)
{
    BOOL            fResult;

    // Handle the reason we were called

    switch (dwReason)
    {
    case DLL_PROCESS_ATTACH:
        g_hInst = hinstDLL;
        fResult = TRUE;
        break;

    case DLL_PROCESS_DETACH:
        // Let the leak detection run

        PSLMemShutdown();
        g_hInst = NULL;
        fResult = TRUE;
        break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
        fResult = TRUE;
        break;

    default:
        // Should never get here

        PSLASSERT(PSLFALSE);
        fResult = FALSE;
        break;
    }

    return fResult;
    lpvReserved;
}


/*
 *  MTPUnitializeWin
 *
 *  Windows specific uninitialize function for the MTP support.
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUninitializeWin()
{
    PSLSTATUS       ps;

    /*  Call the standard MTP function
     */

    ps = MTPUninitialize();
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Unload the MTP USB DLL if they have been loaded
     */

    MTPUSBUnload();

Exit:
    return ps;
}

/* It is determined by the MACRO MTP_PERFORFANCE_TRACE -- defined in
 * header file MTPPerfLogger.h, if the MACRO is defined, the MTP perf lib
 * will be linked, otherwise it will be excluded.
 */
#ifdef MTP_PERFORFANCE_TRACE
#pragma comment(lib, "mtpperfloggerlib.lib")
#pragma comment(lib, "mtpperfloggerwin.lib")
#endif //MTP_PERFORFANCE_TRACE
