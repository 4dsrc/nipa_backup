/*
 *  MTPUSBLoader.c
 *
 *  Implements the demand load APIs for the MTP USB loader
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#define PSLLOADER_IMPLEMENT_LOADER_FUNCTIONS

#include "PSLWindows.h"
#include "PSLWinSafe.h"
#include "PSL.h"
#include "PSLSockets.h"
#include "MTP.h"
#include "MTPUSBTransport.h"
#include "MTPIPTransport.h"
#include "MTPUSBLoader.h"

/*  Local Defines
 */

/*  Local Types
 */

/*  Local Functions
 */

/*  Local Variables
 */

static const WCHAR          _RgchMTPUSBLoadMutex[] = L"MTPUSBLoader Init Mutex";
static const WCHAR          _RgchMTPDLLName[] = L"MTPUSBResponder.dll";

static HINSTANCE            _HInstMTPUSB = NULL;

/*
 *  MTPUSBLoad
 *
 *  Demand loader function responsible for loading the MTP library and
 *  important the necessary functions
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID MTPUSBLoad()
{
    HINSTANCE           hInstDLL = PSLNULL;
    PSLHANDLE           hMutexInit = PSLNULL;
    PSLSTATUS           ps;

    /*  Acquire the mutex so that we make sure there is only one attempt
     *  to load the DLL
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchMTPUSBLoadMutex, &hMutexInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  There is no guarantee that we acquired ownership of the initialization
     *  in the initial check- just a guarantee that we did not need to do
     *  it- so check again here now that we are guarded by the mutex
     */

    if (PSLNULL != _HInstMTPUSB)
    {
        goto Exit;
    }

    /*  Attempt the load the DLL
     */

    hInstDLL = LoadLibrary(_RgchMTPDLLName);
    if (NULL == hInstDLL)
    {
        goto Exit;
    }

    /*  And retrieve the requested imports
     */

    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceBind);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceUnbind);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceGetInfo);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceReset);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceRequestBuffer);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceReleaseBuffer);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceReceiveBuffer);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceSendBuffer);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceCancelRequest);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceStallPipe);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceClearPipeStall);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBDeviceSendControlHandshake);

    /*  Store the DLL instance so that we know it has been initialized
     */

    _HInstMTPUSB = hInstDLL;
    hInstDLL = NULL;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);
    SAFE_FREELIBRARY(hInstDLL);
    return;
}


/*
 *  MTPUSBUnload
 *
 *  Unloads the MTP USB DLL
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLVOID         Nothing
 *
 */

PSLVOID MTPUSBUnload()
{
    HINSTANCE           hInstMTPUSB = NULL;
    PSLHANDLE           hMutexInit = PSLNULL;
    PSLSTATUS           ps;

    /*  Acquire the mutex so that we make sure there is only one attempt
     *  to unload the DLL
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchMTPUSBLoadMutex, &hMutexInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  See if we need to do some work
     */

    if (PSLNULL == _HInstMTPUSB)
    {
        goto Exit;
    }

    /*  Reset the loader binding to the stubs
     */

    PSLLOADER_BIND_RESET(MTPUSBDeviceBind);
    PSLLOADER_BIND_RESET(MTPUSBDeviceUnbind);
    PSLLOADER_BIND_RESET(MTPUSBDeviceRequestBuffer);
    PSLLOADER_BIND_RESET(MTPUSBDeviceReleaseBuffer);
    PSLLOADER_BIND_RESET(MTPUSBDeviceReceiveBuffer);
    PSLLOADER_BIND_RESET(MTPUSBDeviceSendBuffer);
    PSLLOADER_BIND_RESET(MTPUSBDeviceStallPipe);

    /*  Clear the DLL instance handle
     */

    hInstMTPUSB = _HInstMTPUSB;
    _HInstMTPUSB = NULL;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);
    SAFE_FREELIBRARY(hInstMTPUSB);
    return;
}

