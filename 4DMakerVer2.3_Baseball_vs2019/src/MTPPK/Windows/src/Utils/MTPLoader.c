/*
 *  MTPLoader.c
 *
 *  Implements the demand load APIs for the MPT loader
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#define PSLLOADER_IMPLEMENT_LOADER_FUNCTIONS

#include "PSLWindows.h"
#include "PSLWinSafe.h"
#include "PSL.h"
#include "PSLSockets.h"
#include "MTP.h"
#include "MTPUSBTransport.h"
#include "MTPIPTransport.h"
#include "MTPLoader.h"

/*  Local Defines
 */

/*  Local Types
 */

/*  Local Functions
 */

/*  Local Variables
 */

static const WCHAR          _RgchMTPLoadMutex[] = L"MTPLoader Init Mutex";
static const WCHAR          _RgchMTPDLLName[] = L"MTP.dll";

static HINSTANCE            _HInstMTP = NULL;

/*
 *  MTPLoad
 *
 *  Demand loader function responsible for loading the MTP library and
 *  important the necessary functions
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID MTPLoad()
{
    HINSTANCE           hInstDLL = PSLNULL;
    PSLHANDLE           hMutexInit = PSLNULL;
    PSLSTATUS           ps;

    /*  Acquire the mutex so that we make sure there is only one attempt
     *  to load the DLL
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchMTPLoadMutex, &hMutexInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  There is no guarantee that we acquired ownership of the initialization
     *  in the initial check- just a guarantee that we did not need to do
     *  it- so check again here now that we are guarded by the mutex
     */

    if (PSLNULL != _HInstMTP)
    {
        goto Exit;
    }

    /*  Attempt the load the DLL
     */

    hInstDLL = LoadLibrary(_RgchMTPDLLName);
    if (NULL == hInstDLL)
    {
        goto Exit;
    }

    /*  And retrieve the requested imports
     */

    IMPORT_DLL_FUNCTION(hInstDLL, MTPInitialize);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUninitialize);

    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgQueueCreate);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgQueueDestroy);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgSend);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgPost);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgPeek);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgQueueSetCallbacks);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgEnum);

    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgPoolCreate);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgPoolDestroy);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgAlloc);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgFree);

#ifdef PSL_MSGTRACE
    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgTrace);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPMsgTraceReport);
#endif  /* PSL_MSGTRACE */

#ifdef _MTPUSBTRANSPORT_H_
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBBindInitiator);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPUSBBindResponder);
#endif /* _MTPUSBTRANSPORT_H_ */

#ifdef _MTPIPTRANSPORT_H_
    IMPORT_DLL_FUNCTION(hInstDLL, MTPIPBindInitiator);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPIPBindResponder);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPIPUDNToMACAddress);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPIPMACAddressStrToUDN);
#endif  /* _MTPIPTRANSPORT_H_ */

#ifdef MTP_PERFORMANCE_TRACE
    IMPORT_DLL_FUNCTION(hInstDLL, MTPPerfInitLogger);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPPerfDeinitLogger);

    IMPORT_DLL_FUNCTION(hInstDLL, MTPPerfStartLog);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPPerfStopLog);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPPerfSetLogObject);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPPerfSetMTPContext);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPPerfWriteLogRecord);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPPerfSetLogFilter);
    IMPORT_DLL_FUNCTION(hInstDLL, MTPPerfIsLogEnabled);
#endif //MTP_PERFORMANCE_TRACE

    /*  Store the DLL instance so that we know it has been initialized
     */

    _HInstMTP = hInstDLL;
    hInstDLL = NULL;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);
    SAFE_FREELIBRARY(hInstDLL);
    return;
}


/*
 *  MTPUnload
 *
 *  Unloads the MTP DLL
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLVOID         Nothing
 *
 */

PSLVOID MTPUnload()
{
    HINSTANCE           hInstMTP = NULL;
    PSLHANDLE           hMutexInit = PSLNULL;
    PSLSTATUS           ps;

    /*  Acquire the mutex so that we make sure there is only one attempt
     *  to unload the DLL
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchMTPLoadMutex, &hMutexInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  See if we need to do some work
     */

    if (PSLNULL == _HInstMTP)
    {
        goto Exit;
    }

    /*  Reset the loader binding to the stubs
     */

    PSLLOADER_BIND_RESET(MTPInitialize);
    PSLLOADER_BIND_RESET(MTPUninitialize);

#ifdef _MTPUSBTRANSPORT_H_
    PSLLOADER_BIND_RESET(MTPUSBBindInitiator);
    PSLLOADER_BIND_RESET(MTPUSBBindResponder);
#endif /* _MTPUSBTRANSPORT_H_ */

#ifdef _MTPIPTRANSPORT_H_
    PSLLOADER_BIND_RESET(MTPIPBindInitiator);
    PSLLOADER_BIND_RESET(MTPIPBindResponder);
    PSLLOADER_BIND_RESET(MTPIPUDNToMACAddress);
    PSLLOADER_BIND_RESET(MTPIPMACAddressStrToUDN);
#endif  /* _MTPIPTRANSPORT_H_ */

#ifdef MTP_PERFORMANCE_TRACE
    PSLLOADER_BIND_RESET(MTPPerfInitLogger);
    PSLLOADER_BIND_RESET(MTPPerfDeinitLogger);

    PSLLOADER_BIND_RESET(MTPPerfStartLog);
    PSLLOADER_BIND_RESET(MTPPerfStopLog);
    PSLLOADER_BIND_RESET(MTPPerfSetLogObject);
    PSLLOADER_BIND_RESET(MTPPerfSetMTPContext);
    PSLLOADER_BIND_RESET(MTPPerfWriteLogRecord);
    PSLLOADER_BIND_RESET(MTPPerfSetLogFilter);
    PSLLOADER_BIND_RESET(MTPPerfIsLogEnabled);
#endif //MTP_PERFORMANCE_TRACE

    /*  Clear the DLL instance handle
     */

    hInstMTP = _HInstMTP;
    _HInstMTP = NULL;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);
    SAFE_FREELIBRARY(hInstMTP);
    return;
}

