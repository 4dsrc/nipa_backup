// MTPIPResponderHost.cpp : Defines the entry point for the application.
//
//  Copyright (c) 2009 Microsoft Corporation
//  All rights reserved.
//
//  Windows reference code is licensed under the Microsoft Windows Portable
//  Device Enabling Kit for Media Transfer Protocol (MTP).

#include "stdafx.h"
#include <shlobj.h>
#include "MTPIPResponderHost.h"
#include "MTPIPService.h"
#include "MTPUSBService.h"
#include "service_ce.h"

#define MAX_LOADSTRING 100

#define MTPPK_REGKEY_LOCATION   L"Software\\Microsoft\\MTP PK"
#define MTPPK_REGVAL_DATASTORE  L"DataStore"

#define MTPPK_DS_SEL_DLG_TITLE  L"Select data store location:"
#define MTPPK_SELECT_DATASTORE  L"Select data store from File menu, before proceeding."

// Global Variables:
HINSTANCE g_hInst = NULL;               // current instance
TCHAR szTitle[MAX_LOADSTRING];          // The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];    // the main window class name

// Forward declarations of functions included in this code module:
ATOM MyRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE, int);
static VOID SelectDataStore( HWND hwnd );
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK About(HWND, UINT, WPARAM, LPARAM);
static VOID StopIP();
BOOL IsStoreSelected(HWND hWnd);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
    MSG msg;
    HACCEL hAccelTable;

    lpCmdLine;
    hPrevInstance;

    // Initialize global strings
    LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadString(hInstance, IDC_MTPIPRESPONDERHOST, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_MTPIPRESPONDERHOST);

    // Main message loop:
    while (GetMessage(&msg, NULL, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = (WNDPROC)WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, (LPCTSTR)IDI_MTPIPRESPONDERHOST);
    wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = (LPCTSTR)IDC_MTPIPRESPONDERHOST;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

    return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   g_hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
                    CW_USEDEFAULT, 0, 100, 100, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

static DWORD _DwServiceId = 0;
static DWORD _DwUsbServiceId = 0;

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND    - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY    - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int wmId, wmEvent;
    PAINTSTRUCT ps;
    HDC hdc;

    switch (message)
    {
    case WM_CREATE:
        EnableMenuItem( GetMenu(hWnd), ID_TASK_START, MF_ENABLED);
        EnableMenuItem( GetMenu(hWnd), ID_TASK_STOP, MF_GRAYED);
        EnableMenuItem( GetMenu(hWnd), ID_TASK_STARTUSB, MF_ENABLED);
        EnableMenuItem( GetMenu(hWnd), ID_TASK_STOPUSB, MF_GRAYED);
        return DefWindowProc(hWnd, message, wParam, lParam);
    case WM_COMMAND:
        wmId    = LOWORD(wParam);
        wmEvent = HIWORD(wParam);
        // Parse the menu selections:
        switch (wmId)
        {
        case IDM_SELECTDATASTORE:
            SelectDataStore(hWnd);
            break;
        case IDM_EXIT:
            DestroyWindow(hWnd);
            break;
        case ID_TASK_START:
            if (IsStoreSelected(hWnd))
            {
                _DwServiceId = MTPIP_Init(0);
                EnableMenuItem(GetMenu(hWnd), ID_TASK_START,
                        (0 == _DwServiceId) ? MF_ENABLED : MF_GRAYED);
                EnableMenuItem( GetMenu(hWnd), ID_TASK_STOP,
                        (0 != _DwServiceId) ? MF_ENABLED : MF_GRAYED);
            }
            break;
        case ID_TASK_STOP:
            StopIP();
            EnableMenuItem( GetMenu(hWnd), ID_TASK_START, MF_ENABLED);
            EnableMenuItem( GetMenu(hWnd), ID_TASK_STOP, MF_GRAYED);
            break;
        case ID_TASK_STARTUSB:
            if (IsStoreSelected(hWnd))
            {
                _DwUsbServiceId = MTPUSB_Init(0);
                EnableMenuItem( GetMenu(hWnd), ID_TASK_STARTUSB,
                    (0 == _DwUsbServiceId) ? MF_ENABLED : MF_GRAYED);
                EnableMenuItem( GetMenu(hWnd), ID_TASK_STOPUSB,
                    (0 != _DwUsbServiceId) ? MF_ENABLED : MF_GRAYED);
            }
            break;
        case ID_TASK_STOPUSB:
            MTPUSB_Deinit(_DwUsbServiceId);
            EnableMenuItem( GetMenu(hWnd), ID_TASK_STARTUSB, MF_ENABLED);
            EnableMenuItem( GetMenu(hWnd), ID_TASK_STOPUSB, MF_GRAYED);
            _DwUsbServiceId = 0;

            break;
        case ID_TASK_REFRESH:
            break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
        break;
    case WM_PAINT:
        hdc = BeginPaint(hWnd, &ps);
        EndPaint(hWnd, &ps);
        break;
    case WM_DESTROY:
        if (0 != _DwServiceId)
        {
            StopIP();
        }
        if (0 != _DwUsbServiceId)
        {
            MTPUSB_Deinit(_DwUsbServiceId);
            Sleep(300);
        }
        MTPUSB_QueryCanShutdown(_DwUsbServiceId);
        _DwUsbServiceId = 0;
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }

    return 0;
}

//
//  FUNCTION: WriteRegistry
//  pszPathSelected has to be at least MAX_PATH chars.
//
VOID WriteRegistry( LPTSTR szFolder )
{
    HKEY hkey;

    if( ERROR_SUCCESS == RegCreateKeyEx(HKEY_CURRENT_USER,
                            MTPPK_REGKEY_LOCATION,0, NULL,
                            REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS,
                            NULL, &hkey, NULL ) )
    {
        RegSetValueEx( hkey, MTPPK_REGVAL_DATASTORE, NULL, REG_SZ,
            (BYTE*)szFolder, (_tcslen(szFolder)+1)*sizeof(TCHAR));

        RegCloseKey( hkey );
    }
}

//
//  FUNCTION: BrowseFolder
//  pszPathSelected has to be at least MAX_PATH chars.
//
BOOL BrowseFolder(HWND hwnd, LPTSTR szFolderSelected)
{
    BOOL         fReturn = FALSE;
    LPMALLOC     pMalloc = NULL;
    BROWSEINFO   bi;
    LPITEMIDLIST pidlSelected = NULL;
    WCHAR szInfo[MAX_LOADSTRING];

    if (NULL == szFolderSelected || NULL == hwnd)
    {
        goto Exit;
    }

    if (FAILED(SHGetMalloc(&pMalloc)))
    {
        goto Exit;
    }

    ZeroMemory((void*)&bi, sizeof(BROWSEINFO));
    bi.hwndOwner = hwnd;

    if (0 != LoadString(g_hInst, IDS_DS_SEL_DLG_TITLE,
                        szInfo, MAX_LOADSTRING))
    {
        bi.lpszTitle = szInfo;
    }
    else
    {
        /* in case LoadString failed */
        bi.lpszTitle = MTPPK_DS_SEL_DLG_TITLE;
    }

    pidlSelected = SHBrowseForFolder(&bi);
    if (NULL == pidlSelected)
    {
        goto Exit;
    }

    if (SHGetPathFromIDList(pidlSelected, szFolderSelected))
    {
        fReturn = TRUE;
    }

Exit:
    if (NULL != pidlSelected)
    {
        pMalloc->Free(pidlSelected);
    }

    if (NULL != pMalloc)
    {
        pMalloc->Release();
    }

    return fReturn;
}

//
//  FUNCTION: SelectDataStore(HWND)
//  pszPathSelected has to be at least MAX_PATH chars.
//
VOID SelectDataStore(HWND hwnd)
{
    TCHAR szFolder[MAX_PATH];

    if (BrowseFolder(hwnd, szFolder))
    {
        WriteRegistry(szFolder);
    }
}

VOID StopIP()
{
    DWORD dwShutdown;
    /*  Query will return 0 if we are good to
     *  shutdown the service
     */
    do
    {
        MTPIP_IOControl(_DwServiceId,
                        IOCTL_SERVICE_QUERY_CAN_DEINIT,
                        NULL, NULL, NULL, NULL,
                        &dwShutdown);
    }while(0 != dwShutdown);

    MTPIP_Deinit(_DwServiceId);
    _DwServiceId = 0;
}

BOOL IsStoreSelected(HWND hWnd)
{
    BOOL bRet = FALSE;
    HKEY  hkey = NULL;
    DWORD dwType;
    DWORD dwLength;
    WCHAR wszError[MAX_LOADSTRING];
    WCHAR szStorePath[MAX_PATH];
    DWORD dwAttrib;

    if (NULL == hWnd)
    {
        goto Exit;
    }

    /* Get Data Store path from registry. */
    if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_CURRENT_USER,
                    MTPPK_REGKEY_LOCATION, 0,KEY_QUERY_VALUE, &hkey))
    {
        goto Exit;
    }

    dwType   = REG_SZ;
    dwLength = MAX_PATH;

    if(ERROR_SUCCESS != RegQueryValueEx(hkey, MTPPK_REGVAL_DATASTORE,
                        NULL, &dwType,(BYTE*)szStorePath, &dwLength))
    {
        goto Exit;
    }

    dwAttrib = GetFileAttributes(szStorePath);

    /* If it is an invalid path or not a directory path
     * show error message
     */
    if ((INVALID_FILE_ATTRIBUTES == dwAttrib) ||
        (0 == (dwAttrib & FILE_ATTRIBUTE_DIRECTORY)))
    {
        if(0 != LoadString(g_hInst, 1,
                   wszError, MAX_LOADSTRING))
        {
            MessageBox(hWnd, wszError, szTitle,MB_OK);
        }
        else
        {
            MessageBox(hWnd, MTPPK_SELECT_DATASTORE, szTitle,MB_OK);
        }
        goto Exit;
    }

    bRet = TRUE;
Exit:
    if( NULL != hkey)
    {
        RegCloseKey(hkey);
    }
    return bRet;
}
