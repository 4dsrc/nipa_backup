//  Copyright (c) 2009 Microsoft Corporation
//  All rights reserved.
//
//  Windows reference code is licensed under the Microsoft Windows Portable
//  Device Enabling Kit for Media Transfer Protocol (MTP).

//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MTPIPResponderHost.rc
//
#define IDC_MYICON                      2
#define IDD_MTPIPRESPONDERHOST_DIALOG   102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDS_DS_SEL_DLG_TITLE            104
#define IDM_EXIT                        105
#define IDS_INFORM_USER_TOSELECT_STORE  105
#define IDI_MTPIPRESPONDERHOST          107
#define IDI_SMALL                       108
#define IDC_MTPIPRESPONDERHOST          109
#define IDR_MAINFRAME                   128
#define ID_TASK_REFRESH                 32772
#define ID_TASK_START                   32776
#define ID_TASK_STOP                    32777
#define ID_TASK_STARTUSB                32778
#define ID_TASK_STOPUSB                 32779
#define IDM_SELECTDATASTORE             32781
#define IDC_STATIC                      -1

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
