/******************************************************************************

Copyright (C) 2002, NetChip Technology, Inc. (http://www.netchip.com)

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.

NC2280.H

The upper edge of the Nc2280 interface abstracts NET2280 register-level
interface to more useful functions, These functions include (but are not
limited to):
 - NET2280 initialization
 - Interrupt handlers(*)
 - USB setup request handlers
 - Data transfer routines (for arbitrary size transfers)
(*)Interrupt entry points are elsewhere. The interrupt entry points quickly
dispatch to interrupt handlers contained in this file

The lower edge of Nc2280 interfaces to the NET2280 at the register level.

******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
#ifndef NC2280_H
#define NC2280_H

///////////////////////////////////////////////////////////////////////////////
// Public macros:
///////////////////////////////////////////////////////////////////////////////

// Handy macro to make a USB endpoint descriptor (USB_ENDPOINT_DESCRIPTOR):
#define NC_MAKE_ENDPOINT_DESCRIPTOR(EpType, EpDir, EpMaxPkt, EpAddr,  EpPoll)   \
    sizeof(USB_ENDPOINT_DESCRIPTOR),    /* BYTE bLength; */                     \
    ENDPOINT_DESC,                      /* BYTE bDescriptorType; */             \
    (EpDir) | (EpAddr),                 /* BYTE bEndpointAddress; */            \
    EpType,                             /* BYTE bmAttributes; */                \
    LOBYTE(EpMaxPkt),                   /* BYTE wMaxPacketSizeLo; */            \
    HIBYTE(EpMaxPkt),                   /* BYTE wMaxPacketSizeHi; */            \
    EpPoll                              /* BYTE bInterval; */

///////////////////////////////////////////////////////////////////////////////
// Return an endpoint number from currently selected DMA endpoint (EPA or EPB)
#define EP_FROM_DMAEP() ((NETCHIP_READ(DMAREQ) & (1<<DMA_ENDPOINT_SELECT))? EPB: EPA)

///////////////////////////////////////////////////////////////////////////////
// Little Endian <--> Big Endian conversion helpers
#define NATIVECPU_BIGENDIAN FALSE   // TRUE: Native CPU is a Big Endian machine (e.g. Motorola MP860)

#define LITTLEEND_LO 0              // Offset to low byte of a Little Endian word
#define LITTLEEND_HI 1              // Offset to high byte of a Little Endian word
#define BIGEND_LO 1                 // Offset to low byte of a Big Endian word
#define BIGEND_HI 0                 // Offset to high byte of a Big Endian word

#if NATIVECPU_BIGENDIAN
#define MYEND_LO BIGEND_LO          // Offset to low byte if we are a Big Endian machine
#define MYEND_HI BIGEND_HI          // Offset to high byte if we are a Big Endian machine
#else
#define MYEND_LO LITTLEEND_LO       // Offset to low byte if we are a Little Endian machine
#define MYEND_HI LITTLEEND_HI       // Offset to high byte if we are a Little Endian machine
#endif


///////////////////////////////////////////////////////////////////////////////
// Transfer endpoints
#define MTP_BULKIN              EPA
#define MTP_BULKOUT             EPB
#define MTP_INTRIN              EPE

///////////////////////////////////////////////////////////////////////////////
typedef union WordByte
{   // WordByte helps us handle Little/Big Endian USB content across Big/Little Endian CPUs
    //  - I'm sure there's a better way...
    WORD Word;
    BYTE Byte[2];
} WORDBYTE, * PWORDBYTE;

///////////////////////////////////////////////////////////////////////////////
// Offset to NET2280 registers in local address space of PCI-RDK board
//  - NET2280 lives in PCI-RDK 'expansion' address space
//  - Applies to DMA only
#define NETCHIP_LOCAL_OFFSET    NCPCI_EXPANSION_LOCAL_OFFSET

///////////////////////////////////////////////////////////////////////////////
// State of a transfer
//  - Can be checked by any module at any time
typedef enum _TRANSFER_STATE
{   // Transfer states are *not* required for actual NET2280 transfers
    //  - They are provided for asynchronous modules to check transfer status
    TransferState_NoState = 0,  // Uninitialized
    TransferState_Ready,
    TransferState_Busy,
} TRANSFER_STATE;

///////////////////////////////////////////////////////////////////////////////
typedef struct _EPXFERINFO
{   // Describe an endpoint's data transfer information
    BYTE * pPkt;            // Pointer to the packet being transferred
    BYTE * pPktStart;       // Pointer to the start of the packet
    UINT XferRemain;        // Bytes remaining in transfer
    volatile TRANSFER_STATE State;   // State of a transfer
    BOOL CompleteXfer;      // Complete this xfer: A short packet will be sent
    volatile BOOL XferCancelled;     // Xfer cancelled by Device or Host
    BOOL UsingEpTransfer;   // IN transfer using EP_TRANSFER registers
    WORD MaxPktSize;        // Maximum packet size of this endpoint
    BYTE Dir;               // 0x00: OUT endpoint; 0x80: IN endpoint
    UINT XferCount;         // Number of transfers on this endpoint
    PVOID pvXferNotifyParam;
} EPXFERINFO, *  PEPXFERINFO;

///////////////////////////////////////////////////////////////////////////////
// Public variables
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Endpoint transfer info keeps track of each transfer's buffer, size and state
extern EPXFERINFO  EpXferInfo[];

///////////////////////////////////////////////////////////////////////////////
// Chip test
BOOL 
Nc2280_AreYouThere(
    void
    );

///////////////////////////////////////////////////////////////////////////////
// Configure LOCCTL register
//  - Should be called *after* chip reset, but *before* other chip accesses
BOOL
Nc2280_OneTimeInit(
    void
    );


///////////////////////////////////////////////////////////////////////////////
void
Nc2280_CleanUp(
    void
    );

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_TestMode(
    UINT TestMode
    );

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_HsFsDescriptorAdjust(
    void
    );

///////////////////////////////////////////////////////////////////////////////
// Primary Setup Packet interrupt handler
//  - Handle USB 'standard' requests
void
Nc2280_SetupPacketInterrupt(
    void
    );

///////////////////////////////////////////////////////////////////////////////
BOOL
Nc2280_ControlHandler(
    void
    );

///////////////////////////////////////////////////////////////////////////////
// Initialize Endpoint 0 on NET2280
void 
Nc2280_InitializeEp0(
    void
    );

///////////////////////////////////////////////////////////////////////////////
// Handle Endpoint 0 Interrupt Status
void 
Nc2280_Ep0Interrupt(
    void
    );

///////////////////////////////////////////////////////////////////////////////
// Set up a NET2280 endpoint
void
Nc2280_InitializeEndpoint(
    BYTE Ep,                    // NET2280 endpoint (EPA, EPB, ...)
    USB_ENDPOINT_DESCRIPTOR const * EpDescriptor
    );

///////////////////////////////////////////////////////////////////////////////
// Disable a NET2280 endpoint
void
Nc2280_DisableEndpoint(
    BYTE Ep                     // NET2280 endpoint (EPA, EPB, ...)
    );

///////////////////////////////////////////////////////////////////////////////
// Find a matching NET2280 local endpoint (EP0, EPA, ...) for a USB endpoint addr
BOOL
Nc2280_FindLocalEp(
    BYTE UsbEp,
    PBYTE LocalEp
    );

///////////////////////////////////////////////////////////////////////////////
// Find the USB endpoint addr for a NET2280 local endpoint (EP0, EPA, ...) 
BYTE
Nc2280_LocalEpToUsbEp(
    BYTE LocalEp
    );

///////////////////////////////////////////////////////////////////////////////
// Check if a NET2280 endpoint is stalled 
BOOL
Nc2280_IsEndpointStalled(
    BYTE Ep                     // NET2280 endpoint (EPA, EPB, ...)
    );

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_SendControlStatusHandShake(void);

///////////////////////////////////////////////////////////////////////////////
// Stall a NET2280 endpoint
void
Nc2280_StallEndpoint(
    BYTE Ep                     // NET2280 endpoint (EPA, EPB, ...)
    );

///////////////////////////////////////////////////////////////////////////////
// Clear a stalled NET2280 endpoint
void
Nc2280_UnstallEndpoint(
    BYTE Ep                     // NET2280 endpoint (EPA, EPB, ...)
    );

///////////////////////////////////////////////////////////////////////////////
// Check if Nc2280 is connected to a USB Host
BOOL
Nc2280_IsDeviceConnected(
    void
    );

///////////////////////////////////////////////////////////////////////////////
// Disconnect Nc2280 from USB Host and re-connect after 1 second
void
Nc2280_Disconnect(
    void
    );

///////////////////////////////////////////////////////////////////////////////
// Reconnect Nc2280 to USB Host
void
Nc2280_Reconnect(
    void
    );

///////////////////////////////////////////////////////////////////////////////
// Check a NET2280 OUT endpoint to see if there is any data from Host
BOOL
Nc2280_IsDataAvailable(
    BYTE Ep                     // NET2280 OUT endpoint (EPA, EPB, ...)
    );

void 
Nc2280_AbortTransfer(
    BYTE Ep
);

///////////////////////////////////////////////////////////////////////////////
// Kick a receive (USB OUT) transfer of arbitrary length
//  - Can be called (synchronously or asynchronously) to start a transfer
TRANSFER_STATE
Nc2280_KickRxXfer(
    BYTE Ep, 
    VOID * pXferBuf, 
    UINT XferLen
    );

///////////////////////////////////////////////////////////////////////////////
// Continuation of a receive transfer (at the packet level)
//  - Called by NET2280 interrupt routine (Data Packet Received Interrupt)
TRANSFER_STATE 
Nc2280_RxXferPkt(
    BYTE Ep
    );

///////////////////////////////////////////////////////////////////////////////
// Kick a transmit (USB IN) transfer of arbitrary length
//  - Can be called (synchronously or asynchronously) to start a transfer
TRANSFER_STATE
Nc2280_KickTxXfer(
    BYTE Ep, 
    VOID * pXfer, 
    UINT XferLen,
    BOOL bLastPacket
    );

///////////////////////////////////////////////////////////////////////////////
// Continuation of a transmit transfer (at the packet level)
//  - Called by NET2280 interrupt routine (Data Packet Transmit Interrupt)
TRANSFER_STATE
 Nc2280_TxXferPkt(
    BYTE Ep
    );

///////////////////////////////////////////////////////////////////////////////
#endif // NC2280_H

///////////////////////////////////////////////////////////////////////////////
//  End of file
///////////////////////////////////////////////////////////////////////////////
