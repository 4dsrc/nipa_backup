/*
 *  MTPUSBOSDescriptor.h
 *
 *  Contains definitions for the Microsoft USB OS Descriptor
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPUSBOSDESCRIPTOR_H_
#define _MTPUSBOSDESCRIPTOR_H_

/*  Force tight packing
 */

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPush1.h"
#endif

/*
 *  Microsoft OS String Descriptor
 *
 *  (See USB FAQ on Microsoft Website)
 */

struct _USB_MS_OS_STRING_DESCRIPTOR
{
    PSLBYTE                 bLength;
    PSLBYTE                 bDescriptorType;
    PSLBYTE                 rgbSignature[14];
    PSLBYTE                 bMSVendorCode;
    PSLBYTE                 bFlags;
} PSL_PACK1;

typedef struct _USB_MS_OS_STRING_DESCRIPTOR USB_MS_OS_STRING_DESCRIPTOR;

typedef struct _USB_MS_OS_STRING_DESCRIPTOR PSL_UNALIGNED*
                            PXUSB_MS_OS_STRING_DESCRIPTOR;
typedef PSL_CONST struct _USB_MS_OS_STRING_DESCRIPTOR PSL_UNALIGNED*
                            PCXUSB_MS_OS_STRING_DESCRIPTOR;

#define MSOSDESCRIPTORFLAGS_DEFAULT         0x00
#define MSOSDESCRIPTORFLAGS_RESERVED        0x01
#define MSOSDESCRIPTORFLAGS_CONTAINER_ID    0x02

#define USB_MS_OS_STRING_INDEX              0xee
#define USB_MS_OS_STRING_DESC_LENGTH        0x12

#define USB_MS_OS_STRING_DESC_SIGNATURE \
    0x4d, 0x00, 0x53, 0x00, 0x46, 0x00, 0x54, 0x00, \
    0x31, 0x00, 0x30, 0x00, 0x30, 0x00

/*
 *  Microsoft Container ID Descriptor
 *
 */

struct _USB_MS_CONTAINER_ID_DESCRIPTOR
{
    PSLUINT32               dwLength;
    PSLUINT16               bcdVersion;
    PSLUINT16               wIndex;
    PSLBYTE                 rgbContainerID[16];
} PSL_PACK1;


typedef struct _USB_MS_CONTAINER_ID_DESCRIPTOR
                            USB_MS_CONTAINER_ID_DESCRIPTOR;

typedef struct _USB_MS_CONTAINER_ID_DESCRIPTOR PSL_UNALIGNED*
                            PXUSB_MS_CONTAINER_ID_DESCRIPTOR;
typedef PSL_CONST struct _USB_MS_CONTAINER_ID_DESCRIPTOR PSL_UNALIGNED*
                            PCXUSB_MS_CONTAINER_ID_DESCRIPTOR;

#ifdef PSL_LITTLE_ENDIAN
#define USB_MS_CONTAINER_ID_DESCRIPTOR_VERSION              0x0100
#define USB_MS_CONTAINER_ID_TYPE                            0x0006
#elif defined(PSL_BIG_ENDIAN)
#define USB_MS_CONTAINER_ID_DESCRIPTOR_VERSION              0x0001
#define USB_MS_CONTAINER_ID_TYPE                            0x0600
#else
#error PSL_LITTLE_ENDIAN or PSL_BIG_ENDIAN not defined
#endif  /* Byte Order */


/*
 *  Microsoft Extended Configuration Header
 */

struct _MS_EXTENDED_CONFIGURATION_HEADER
{
    PSLUINT32               dwLength;
    PSLUINT16               bcdVersion;
    PSLUINT16               bDescriptorType;
    PSLBYTE                 bCount;
    PSLBYTE                 rgbReserved[7];
} PSL_PACK1;

typedef struct _MS_EXTENDED_CONFIGURATION_HEADER
                            MS_EXTENDED_CONFIGURATION_HEADER;

typedef struct _MS_EXTENDED_CONFIGURATION_HEADER PSL_UNALIGNED*
                            PXMS_EXTENDED_CONFIGURATION_HEADER;
typedef PSL_CONST struct _MS_EXTENDED_CONFIGURATION_HEADER PSL_UNALIGNED*
                            PCXMS_EXTENDED_CONFIGURATION_HEADER;

#define MS_EXTENDED_CONFIGURATION_HEADER_RESERVED \
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

/*
 *  Microsoft Extended Configuration Function Descriptor
 *
 */

struct _MS_EXTENDED_CONFIGURATION_FEATURE
{
    PSLBYTE                 bFirstInterfaceNumber;
    PSLBYTE                 bInterfaceCount;
    PSLBYTE                 rgbCompatibleID[8];
    PSLBYTE                 rgbSubCompatibleID[8];
    PSLBYTE                 rgbReserved[6];
} PSL_PACK1;

typedef struct _MS_EXTENDED_CONFIGURATION_FEATURE
                            MS_EXTENDED_CONFIGURATION_FEATURE;

typedef struct _MS_EXTENDED_CONFIGURATION_FEATURE PSL_UNALIGNED*
                            PXMS_EXTENDED_CONFIGURATION_FEATURE;
typedef PSL_CONST struct _MS_EXTENDED_CONFIGURATION_FEATURE PSL_UNALIGNED*
                            PCXMS_EXTENDED_CONFIGURATION_FEATURE;

#define MS_EXTENDED_CONFIGURATION_FEATURE_RESERVED \
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00

/*
 *  Microsoft Extended Configuration Descriptor
 *
 *  The extended configuration descriptor is created by having one
 *  MS_EXTENDED_CONFIGURATION_HEADER followed by zero or more
 *  MS_EXTENDED_CONFIGURATION_FEATURE entries.  The descriptor type and
 *  versions are as specified.
 */

#ifdef PSL_LITTLE_ENDIAN
#define USB_MS_EXTENDED_CONFIGURATION_DESCRIPTOR_TYPE       0x0004
#define USB_MS_EXTENDED_CONFIGURATION_DESCRIPTOR_VERSION    0x0100
#elif defined(PSL_BIG_ENDIAN)
#define USB_MS_EXTENDED_CONFIGURATION_DESCRIPTOR_TYPE       0x0400
#define USB_MS_EXTENDED_CONFIGURATION_DESCRIPTOR_VERSION    0x0001
#else
#error PSL_LITTLE_ENDIAN or PSL_BIG_ENDIAN not defined
#endif  /* Byte Order */


/*
 *  MTP USB Extended Configuration Descriptor
 *
 */

struct _MTPUSB_EXTENDED_CONFIGURATION_DESCRIPTOR
{
    MS_EXTENDED_CONFIGURATION_HEADER    header;
    MS_EXTENDED_CONFIGURATION_FEATURE   feature;
} PSL_PACK1;

typedef struct _MTPUSB_EXTENDED_CONFIGURATION_DESCRIPTOR
                            MTPUSB_EXTENDED_CONFIGURATION_DESCRIPTOR;

typedef struct _MTPUSB_EXTENDED_CONFIGURATION_DESCRIPTOR PSL_UNALIGNED*
                            PXMTPUSB_EXTENDED_CONFIGURATION_DESCRIPTOR;
typedef PSL_CONST struct _MTPUSB_EXTENDED_CONFIGURATION_DESCRIPTOR PSL_UNALIGNED*
                            PCXMTPUSB_EXTENDED_CONFIGURATION_DESCRIPTOR;

#define MTPUSB_VENDOR_EXTENSION_CODE    0xfe

#define MTPUSB_COMPATIBLE_ID \
    0x4d, 0x54, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00

#define MTPUSB_SUBCOMPATIBLE_ID_GENERIC \
    0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

#define MTPUSB_SUBCOMPATIBLE_ID_STILL_CAMERA \
    0x32, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

#define MTPUSB_SUBCOMPATIBLE_ID_MEDIA_PLAYER \
    0x33, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

#define MTPUSB_SUBCOMPATIBLE_ID_MOBILE_HANDSET \
    0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

#define MTPUSB_SUBCOMPATIBLE_ID_PDA \
    0x35, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

#define MTPUSB_SUBCOMPATIBLE_ID_VIDEO_CAMERA \
    0x36, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

#define MTPUSB_SUBCOMPATIBLE_ID_AUDIO_RECORDER \
    0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

/*
 *  Recover default packing
 */

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPop.h"
#endif

#endif  /* _MTPUSBOSDESCRIPTOR_H_ */

