//=============================================================================
//
// @module      USBSTD.h
//
// @created     20-June-2003
//
// @abstract    USB Standard Definitions
//
// @copyright   (C) COPYRIGHT MICROSOFT CORPORATION, 2003
//
//=============================================================================

#ifndef USBSTD_H
#define USBSTD_H

#include "MTPMSG.h"
#include "MTPTypes.h"
#include "MTPUSBDevice.h"

#ifdef DRIVER

/////////////////////////////////////////////////////////////////////
// Disable warning for zero-sized arrays (warning C4200)
// MSDN: Compiler Warning (levels 2 and 4) C4200: nonstandard extension used:
// zero-sized array in struct/union. A structure or union contained an array with zero size.
#pragma warning (disable: 4200)
#endif

/////////////////////////////////////////////////////////////////////
#define USBSPEC 0x0200                  // USB Specification Release (BCD)

/////////////////////////////////////////////////////////////////////
// Standard Request Codes (in bRequest)
#define GET_STATUS              0x00    // 
#define CLEAR_FEATURE           0x01    // 
#define SET_FEATURE             0x03    // 
#define SET_ADDRESS             0x05    // 
#define GET_DESCRIPTOR          0x06    // 
#define SET_DESCRIPTOR          0x07    // 
#define GET_CONFIGURATION       0x08    // 
#define SET_CONFIGURATION       0x09    // 
#define GET_INTERFACE           0x0a    // 
#define SET_INTERFACE           0x0b    // 
#define SYNCH_FRAME             0x0c    // 

/////////////////////////////////////////////////////////////////////
// Descriptor Types (in wValue) (See USB 1.1, Table 9-5)
#define DEVICE_DESC                     0x01    // 
#define CONFIGURATION_DESC              0x02    // 
#define STRING_DESC                     0x03    // 
#define INTERFACE_DESC                  0x04    // 
#define ENDPOINT_DESC                   0x05    // 
// Added for USB 2.0
#define DEVICE_QUALIFIER_DESC           0x06    //
#define OTHER_SPEED_CONFIGURATION_DESC  0x07    //
#define INTERFACE_POWER_DESC            0x08    //

//
// defined USB device classes
//


#define USB_DEVICE_CLASS_RESERVED           0x00
#define USB_DEVICE_CLASS_AUDIO              0x01
#define USB_DEVICE_CLASS_COMMUNICATIONS     0x02
#define USB_DEVICE_CLASS_HUMAN_INTERFACE    0x03
#define USB_DEVICE_CLASS_MONITOR            0x04
#define USB_DEVICE_CLASS_PHYSICAL_INTERFACE 0x05
#define USB_DEVICE_CLASS_POWER              0x06
#define USB_DEVICE_CLASS_PRINTER            0x07
#define USB_DEVICE_CLASS_STORAGE            0x08
#define USB_DEVICE_CLASS_HUB                0x09
#define USB_DEVICE_CLASS_VENDOR_SPECIFIC    0xFF

/////////////////////////////////////////////////////////////////////
// Feature selectors (in wValue) (See USB 2.0: 9.4.5)
#define DEVICE_REMOTE_WAKEUP    0x0001          // Not to be confused with Remote Wakeup in bmAttributes!
#define ENDPOINT_STALL          0x0000          //
// Added for USB 2.0
#define TEST_MODE               0x0002          //

/////////////////////////////////////////////////////////////////////
// Test mode selectors (in wIndex) (See USB 2.0: 9.4.9)
#define USB_TEST_J              0x01
#define USB_TEST_K              0x02
#define USB_TEST_SE0_NAK        0x03
#define USB_TEST_PACKET         0x04
#define USB_TEST_FORCE_ENABLE   0x05

/////////////////////////////////////////////////////////////////////
// bmRequestType bits (in bmRequestType)
#define HOST_TO_DEVICE          (0<<7)  // xfer direction OUT
#define DEVICE_TO_HOST          (1<<7)  // xfer direction IN
//
#define STANDARD                (0<<5)  // Type
#define CLASS                   (1<<5)  // Type
#define VENDOR                  (2<<5)  // Type
#define RESERVED                (3<<5)  // Type
// 
#define RECIPIENT_DEVICE        0       // Recipient
#define RECIPIENT_INTERFACE     1       // Recipient
#define RECIPIENT_ENDPOINT      2       // Recipient
#define RECIPIENT_OTHER         3       // Recipient

/////////////////////////////////////////////////////////////////////
// Transfer type (in bmAttributes of Endpoint Descriptors)
//  - Maps to matching in NetChip endpoint configuration register
#define CTRL                    0x00    // 1-0: 00:Control
#define ISOC                    0x01    // 1-0: 01:Isochronous
#define BULK                    0x02    // 1-0: 10:Bulk
#define INTR                    0x03    // 1-0: 11:Interrupt

/////////////////////////////////////////////////////////////////////
// Endpoint direction (usually OR'd with endpoint address in bits 3-0)
//  - Note: Not to be confused with endpoint direction programming in chip
#define EP_OUT                  0x00    // 7: Endpoint direction OUT
#define EP_IN                   0x80    // 7: Endpoint direction IN
#define EP_DIRECTION_IN            7


/////////////////////////////////////////////////////////////////////
// bmAttribute bits in Configuration Descriptor
//  - USB 2.0 9.6.3 (bmAttributes in Table 9-10): Bit 7 is reserved, and always set to one
#define CONFIG_SELF_POWERED        6
#define CONFIG_REMOTE_WAKEUP       5    // Not to be confused with Device Remote Wakeup feature selector!

/////////////////////////////////////////////////////////////////////
// Vendor-specific
//  - Vendor specific device, class, subclass and protocols are all 0xff
#define VENDOR_SPECIFIC                             0xff

/////////////////////////////////////////////////////////////////////
typedef struct _USB_SETUP_PACKET
{   // USB setup packet (USB spec: 9.3)
    BYTE bmRequestType;
    BYTE bRequest;
    WORD wValue;
    WORD wIndex;
    WORD wLength;
} USB_SETUP_PACKET, * PUSB_SETUP_PACKET;

// Setup packet (referenced as an array of BYTEs)
#define _bmRequestType  0
#define _bRequest       1
#define _wValueLo       2
#define _wValueHi       3
#define _wIndexLo       4
#define _wIndexHi       5
#define _wLengthLo      6
#define _wLengthHi      7

/////////////////////////////////////////////////////////////////////
// USB descriptors based on USB100.H from the Win98 DDK
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
typedef struct _USB_DEVICE_DESCRIPTOR 
{
    BYTE bLength;
    BYTE bDescriptorType;
    BYTE bcdUSBLo;
    BYTE bcdUSBHi;
    BYTE bDeviceClass;
    BYTE bDeviceSubClass;
    BYTE bDeviceProtocol;
    BYTE bMaxPacketSize0;
    BYTE idVendorLo;
    BYTE idVendorHi;
    BYTE idProductLo;
    BYTE idProductHi;
    BYTE bcdDeviceLo;
    BYTE bcdDeviceHi;
    BYTE iManufacturer;
    BYTE iProduct;
    BYTE iSerialNumber;
    BYTE bNumConfigurations;
}   USB_DEVICE_DESCRIPTOR, *   PUSB_DEVICE_DESCRIPTOR;

/////////////////////////////////////////////////////////////////////
typedef struct _USB_CONFIGURATION_DESCRIPTOR 
{
    BYTE bLength;
    BYTE bDescriptorType;
    BYTE wTotalLengthLo;
    BYTE wTotalLengthHi;
    BYTE bNumInterfaces;
    BYTE bConfigurationValue;
    BYTE iConfiguration;
    BYTE bmAttributes;
    BYTE bMaxPower;             // In 2ma increments
}   USB_CONFIGURATION_DESCRIPTOR, *   PUSB_CONFIGURATION_DESCRIPTOR;


/////////////////////////////////////////////////////////////////////
typedef struct _USB_INTERFACE_DESCRIPTOR 
{
    BYTE bLength;
    BYTE bDescriptorType;
    BYTE bInterfaceNumber;
    BYTE bAlternateSetting;
    BYTE bNumEndpoints;
    BYTE bInterfaceClass;
    BYTE bInterfaceSubClass;
    BYTE bInterfaceProtocol;
    BYTE iInterface;
}   USB_INTERFACE_DESCRIPTOR, *   PUSB_INTERFACE_DESCRIPTOR;

/////////////////////////////////////////////////////////////////////
typedef struct _USB_ENDPOINT_DESCRIPTOR 
{
    BYTE bLength;               // 0x00
    BYTE bDescriptorType;       // 0x01
    BYTE bEndpointAddress;      // 0x02 Direction in bit 7
    BYTE bmAttributes;          // 0x03 CTRL, ISOC, BULK, INTR
    BYTE wMaxPacketSizeLo;      // 0x04
    BYTE wMaxPacketSizeHi;      // 0x05
    BYTE bInterval;             // 0x06 Polling interval (Interrupt, Isoch)
}   USB_ENDPOINT_DESCRIPTOR, *   PUSB_ENDPOINT_DESCRIPTOR;

#define EP_MAXPACKETSIZE(ep) ((ep.wMaxPacketSizeHi << 8) + ep.wMaxPacketSizeLo)
/////////////////////////////////////////////////////////////////////////////
// For convenience, we define a a USB interface which contains 
// one Interface Descriptor and all its Endpoint Descriptors:
typedef struct _USB_INTERFACE
{
    USB_INTERFACE_DESCRIPTOR InterfaceDescriptor;
    USB_ENDPOINT_DESCRIPTOR EndpointDescriptor[1];
} USB_INTERFACE, * PUSB_INTERFACE;

/////////////////////////////////////////////////////////////////////
typedef struct _USB_STRING_DESCRIPTOR 
{
    BYTE bLength;
    BYTE bDescriptorType;
    WORD bString[1];
}   USB_STRING_DESCRIPTOR, *   PUSB_STRING_DESCRIPTOR;

/////////////////////////////////////////////////////////////////////
// USB descriptors added based on USB 2.00 Specification
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
typedef struct _USB_DEVICE_QUALIFIER_DESCRIPTOR 
{
    BYTE bLength;
    BYTE bDescriptorType;
    BYTE bcdUSBLo;
    BYTE bcdUSBHi;
    BYTE bDeviceClass;
    BYTE bDeviceSubClass;
    BYTE bDeviceProtocol;
    BYTE bMaxPacketSize0;
    BYTE bNumConfigurations;
    BYTE bReserved;
}   USB_DEVICE_QUALIFIER_DESCRIPTOR, *   PUSB_DEVICE_QUALIFIER_DESCRIPTOR;

/////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Link to next USB descriptor
#define NEXT_USB_DESCRIPTOR(pDescr) ((PBYTE)(pDescr)+((pDescr)->bLength))

typedef struct _USB_COMMON_DESCRIPTOR 
{
    BYTE bLength;
    BYTE bDescriptorType;
} USB_COMMON_DESCRIPTOR, *PUSB_COMMON_DESCRIPTOR;


/////////////////////////////////////////////////////////////////////
// Microsoft OS String Descriptor Extensions
/////////////////////////////////////////////////////////////////////
#define MSOS_VENDORCODE_TO_GET_MS_DESCRIPTOR    0x01
#define MSOS_FEATURE_INDEX_EXTENDED_CONFIG_DESC 0x0004
#define MSOS_FEATURE_INDEX_EXTENDED_PROP_DESC   0x0005

typedef struct _MSOS_STRING_DESCRIPTOR 
{
    BYTE bLength;           // Must be 0x12
    BYTE bDescriptorType;   // Must be 0x03
    WORD qwSignature[7];    // must be L"MSFT100"
    BYTE bMS_VendorCode;    // any
    BYTE bPad;              // Must be 0x00
}   MSOS_STRING_DESCRIPTOR, *PMSOS_STRING_DESCRIPTOR;

typedef struct _MSOS_EXT_CONFIG_DESCRIPTOR_HEADER
{
    DWORD dwLength;         // Length of the entire Extended Configuration Descriptor
    WORD  bcdVersion;       // Version info in BCD: 0x0100 ~ 1.00
    WORD  wIndex;           // must be MSOS_FEATURE_INDEX_EXTENDED_CONFIG_DESC, or 0x0004
    BYTE  bCount;           // total number of Function Sections that follow the header section
    BYTE  bReserved[7];     // RESERVED, should be 0x00
}   MSOS_EXT_CONFIG_DESCRIPTOR_HEADER , *PMSOS_EXT_CONFIG_DESCRIPTOR_HEADER ;

typedef struct _MSOS_EXT_CONFIG_DESCRIPTOR_FUNCTION
{
    BYTE  bFirstInterfaceNumber; // Starting Interface Number for this function
    BYTE  bInterfaceCount;       // Total number of interfaces 
    BYTE  compatibleID[8];       // Compatible ID as defined by Microsoft
    BYTE  subCompatibleID[8];    // The Sub Compatible ID as defined by Microsoft
    BYTE  bReserved[6];          // RESERVED, should be 0x00
}   MSOS_EXT_CONFIG_DESCRIPTOR_FUNCTION , *PMSOS_EXT_CONFIG_DESCRIPTOR_FUNCTION ;

typedef struct _MSOS_EXT_PROP_DESCRIPTOR_HEADER
{
    DWORD dwLength;         // Length of the entire Extended Property Descriptor
    WORD  bcdVersion;       // Version info in BCD: 0x0100 ~ 1.00
    WORD  wIndex;           // must be MSOS_FEATURE_INDEX_EXTENDED_PROP_DESC, or 0x0005
    WORD  wCount;           // total number of Custom Proeprty Sections that follow the header section
}   MSOS_EXT_PROP_DESCRIPTOR_HEADER , *PMSOS_EXT_PROP_DESCRIPTOR_HEADER ;

// UFN Transfer Errors
#define     UFN_NO_ERROR                        0x00000000
#define     UFN_DEVICE_NOT_RESPONDING_ERROR     0x00000005
#define     UFN_CANCELED_ERROR                  0x00000101
#define     UFN_NOT_COMPLETE_ERROR              0x00000103
#define     UFN_CLIENT_BUFFER_ERROR             0x00000104

// UFN Events 
typedef enum _UFN_BUS_EVENT {
    UFN_DETACH = 0,
    UFN_ATTACH,
    UFN_RESET,
    UFN_SUSPEND,
    UFN_RESUME,
} UFN_BUS_EVENT, *PUFN_BUS_EVENT;

typedef enum _UFN_BUS_SPEED {
    BS_UNKNOWN_SPEED = 0,
    BS_FULL_SPEED = (1 << 0),
    BS_HIGH_SPEED = (1 << 1),
} UFN_BUS_SPEED, *PUFN_BUS_SPEED;


///////////////////////////////////////////////////////////////////////////////
typedef struct _TRANSFER_CONFIGURATION
{   // A USB configuration contains all interfaces and all endpoint descriptors
    USB_CONFIGURATION_DESCRIPTOR ConfigurationDescriptor;
    USB_INTERFACE_DESCRIPTOR InterfaceDescriptor;
    USB_ENDPOINT_DESCRIPTOR EndpointDescriptor[3];
} TRANSFER_CONFIGURATION, * PTRANSFER_CONFIGURATION;

// Messages sent to the notify routine.

// dwParam = UFN_BUS_EVENT
#define UFN_MSG_BUS_EVENTS                  1

// dwParam = wValue from the device request
#define UFN_MSG_CONFIGURED                  2

// dwParam = UFN_BUS_SPEED
#define UFN_MSG_BUS_SPEED                   3

// dwParam = PUSB_SETUP_PACKET
// This message is for setup packets that are not processed by the MDD.
// The client is responsible for sending the control status handshake.
#define UFN_MSG_SETUP_PACKET                4

// dwParam = PUSB_SETUP_PACKET
// This message is for setup packets that are processed by the MDD.
// The client must not send the control status handshake.
#define UFN_MSG_PREPROCESSED_SETUP_PACKET   5

// dwParam = the USB bus address
#define UFN_MSG_SET_ADDRESS                 6

/////////////////////////////////////////////////////////////////////
// NetChip standards
//  - Vendor ID
//  - Product IDs
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// BEFORE APPLYING VENDOR AND PRODUCT ID'S TO YOUR DEVICE:
//  - CONTACT NETCHIP IF YOU INTEND TO USE NETCHIP'S VID (0x0525)
//  - CONTACT USB-IF IF YOU DO NOT INTEND TO USE NETCHIP'S VID
// Applying unique VIDs and PIDs is crucial to the success of USB as a whole.
// Failing to abide by USB's and/or NetChip's VID and PID registration
// methods could cause terrible conflicts and consequences long after
// your device is released!
// 
// NetChip offers FREE PIDs that you can use with NetChip's VID (0x0525)
//  - To get your FREE PIDs, contact   support@netchip.com
//
// Vendor ID given to NetChip by the USB-IF is 0x0525:
#define VID_NETCHIP     0x0525      // READ STATEMENT (ABOVE) REGARDING VIDs and PIDs!!!

/////////////////////////////////////////////////////////////////////
// Some NetChip standard PIDs:

// Transfer devices (0x1000 to 0x10ff)
#define PID_TRANSFER            0x1000                  // Basic transfer device
#define PID_TRANSFER_ISOCH      PID_TRANSFER + 0x40     // Isochronous transfer device
#define PID_MSMTP               0x1003                  // Basic MTP transfer device

// Loopback devices (0x1100 to 0x11ff)
#define PID_LOOPBACK    0x1100      // Basic loopback device

// Handle that identifies a transfer to the MDD.
typedef PVOID UFN_TRANSFER, *PUFN_TRANSFER;
typedef PSLHANDLE MTPUSBBUFFERMGR;

typedef struct _USBDEVICECTX
{
#ifdef PSL_ASSERTS
    DWORD                   dwCookie;
#endif  /* PSL_ASSERTS */
    DWORD                   cRefs;
    VOID*                   pvRoot;
    BOOL                    fMTPInit;
    PSLHANDLE               hMutex;
    MTPUSBBUFFERMGR         mtpusbbm;
    PSLMSGQUEUE             mqEvent;
    PSLMSGPOOL              mpEvent;
    PFNMTPUSBDEVICEHANDLECLASSREQUEST   pfnHandleClassReq;
    PSLPARAM                            aParamClassReq;
} USBDEVICECTX;

typedef struct _USBTRANSFERINFO
{
#ifdef PSL_ASSERTS
    DWORD                   dwCookie;
#endif  /* PSL_ASSERTS */
    VOID*                   pvRoot;
    DWORD                   dwTransferFlags;
    DWORD                   idxPipe;
    VOID*                   pvBuffer;
    DWORD                   cbBuffer;
    PSLPARAM                aParam;
    USBDEVICECTX*           pusbdCtx;
} USBTRANSFERINFO;

/////////////////////////////////////////////////////////////////////
#endif // USBSTD_H

/////////////////////////////////////////////////////////////////////
//  End of file
