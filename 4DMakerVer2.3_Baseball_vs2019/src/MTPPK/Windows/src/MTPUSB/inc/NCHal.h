/******************************************************************************

Copyright (C) 2002, NetChip Technology, Inc. (http://www.netchip.com)

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.

NCHAL.H

NetChip Hardware Abstraction Layer (HAL) for NET2280 register access.

********************************************************************/

/////////////////////////////////////////////////////////////////////
#ifndef NCHAL_H
#define NCHAL_H

/////////////////////////////////////////////////////////////////////
#ifndef NET2280_DIRECT_ACCESS
#pragma message ("NcHal.H: NET2280_DIRECT_ACCESS not defined!!\n")
#endif

/////////////////////////////////////////////////////////////////////
typedef BYTE NET2280_DATA_TYPE, *PNET2280_DATA_TYPE;    // 

/////////////////////////////////////////////////////////////////////
// Chip access option: Call or direct (memory mapped)?
//  - During early development and debug, the call option is preferred
//  - For best performance, apply the 'direct access' option
#if NET2280_DIRECT_ACCESS
#define NETCHIP_WRITE(reg, val)             *(PDWORD)(NETCHIP_BASEADDRESS+(reg)) = (DWORD)val
#define NETCHIP_READ(reg)                   *(PDWORD)(NETCHIP_BASEADDRESS+(reg))

#else
// Use a slower 'call' interface for device I/O
//  - Often convenient during early development and debug
#define NETCHIP_WRITE(reg, val)             NcWrite32((reg), val)
#define NETCHIP_READ(reg)                   NcRead32(reg)

/////////////////////////////////////////////////////////////////////
void 
NcWrite32(
    DWORD reg, 
    DWORD val
    );

/////////////////////////////////////////////////////////////////////
DWORD 
NcRead32(
    DWORD reg
    );

#endif // NET2280_DIRECT_ACCESS


/////////////////////////////////////////////////////////////////////
// Access to NET2280 indexed registers
/////////////////////////////////////////////////////////////////////

#define NETCHIP_WRITE_INDEXED(reg, val)     NcWriteIndexed32((reg), val)
#define NETCHIP_READ_INDEXED(reg)           NcReadIndexed32(reg)

/////////////////////////////////////////////////////////////////////
DWORD
NcReadIndexed32(
    DWORD Reg
    );

/////////////////////////////////////////////////////////////////////
void
NcWriteIndexed32(
    DWORD Reg,
    DWORD Value
    );

/////////////////////////////////////////////////////////////////////
// NET2280 register names
//  - Array of NET2280 names (ascii-text)
//  - Often useful during early development and debug
//  - For convenience, address-to-name layed out in one-to-one correspondence 
#if _DEBUG
extern char *Net2280_RegisterNames[];
#endif

/////////////////////////////////////////////////////////////////////
#endif // NCHAL_H

/////////////////////////////////////////////////////////////////////
//  End of file
