/********************************************************************

SYSTEM.H

Copyright NetChip Technology, 2000, 2001, 2002

NetChip PCI-RDK 'System' header file
 - Interfaces required for most systems
 
Changes
010514  Joey    Genesis - 'System' for NET2270 PCI-RDK
020718  Kevin   Port for NET2280 PCI-RDK
********************************************************************/

/////////////////////////////////////////////////////////////////////
#ifndef SYSTEM_H
#define SYSTEM_H

/////////////////////////////////////////////////////////////////////
#define min(a, b)  (((a) < (b)) ? (a) : (b))
#define max(a, b)  (((a) > (b)) ? (a) : (b))

/////////////////////////////////////////////////////////////////////////////
extern UINT gPrintVolume;

/////////////////////////////////////////////////////////////////////////////
typedef struct _UserBufInfo
{   // Structure describing elements of user buffers (one per endpoint)
    BYTE * pUserBuf;        // User's buffer address
    UINT UserSize;          // Size of user's buffer
    UINT Reserved;
} USERBUFINFO, * PUSERBUFINFO;

///////////////////////////////////////////////////////////////////////////////
// Return an endpoint character ('0', 'A', 'B', ... from an endpoint number)
//  - Useful when calling printf()
#define NC_ENDPOINT(Ep) ((Ep)==EP0)?'0':(Ep) + ('A' - 1)

///////////////////////////////////////////////////////////////////////////////
// If the device does not define a product string:
#define NCDEVICE_ASCII_PRODUCT_UNKNOWN  "<Product unknown>"
///////////////////////////////////////////////////////////////////
// Replace standard malloc and free with our own...
#if _DEBUG
#define MALLOC Malloc
#define FREE Free
#define DEBUG(_x_) _x_
#else
#define MALLOC malloc
#define FREE free
#define DEBUG
#endif

///////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
// The all-important base address of the NetChip chip...
#define NETCHIP_BASEADDRESS NetchipBaseAddress
extern PNET2280_DATA_TYPE NetchipBaseAddress;

extern PBYTE Net2280Program;                    // Base address of NET2280 program memory (8051)

typedef BOOL (WINAPI *LPUSB_NOTIFY_ROUTINE)(PVOID pvNotifyParameter, DWORD dwMsg, DWORD dwParam);
typedef DWORD (WINAPI *LPXFER_NOTIFY_ROUTINE)(PVOID pvXferNotifyParameter, BYTE Ep, DWORD dwXfered);

/////////////////////////////////////////////////////////////////////
// Control block for the device currently in use

/////////////////////////////////////////////////////////////////////
typedef struct _NETCHIP_DEVICE_CONTROL_BLOCK
{   
   //
    // Pointers to notification callback and parameter
    LPUSB_NOTIFY_ROUTINE    lpfnNotify;
    PVOID   pvNotifyParameter;

    LPXFER_NOTIFY_ROUTINE   lpfnXferNotify;
    
    // Pointers to project USB descriptors:
    PUSB_DEVICE_DESCRIPTOR DeviceDescriptor;
    PUSB_DEVICE_QUALIFIER_DESCRIPTOR DeviceQualifierDescriptor;
    // (Configuration content varies from project to project, so we make them void)
    void * HS_Configuration;                            // High Speed configuration
    void * FS_Configuration;                            // Full Speed configuration

    BOOL fAttached;
    BOOL fReportedSpeed;

    //
    // Project strings:
    //  - Use ASCII strings here (they are converted to USB strings at initialazation time)
    //  - Set to NULL if not used
    PCHAR Product;
    PCHAR Manufacturer;
    PCHAR Serial;

    //
    // Buffers (Including buffer for endpoint 0)
    UINT BufferCount;
    PUSERBUFINFO Buffers;

} NETCHIP_DEVICE_CONTROL_BLOCK, *PNETCHIP_DEVICE_CONTROL_BLOCK;

extern PNETCHIP_DEVICE_CONTROL_BLOCK NcDevice;

/////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
BOOL
Rdk_GetDriverVersion(
    PDWORD DriverVersion
    );

/////////////////////////////////////////////////////////////////////
void *
Malloc(
    size_t Size
    );

/////////////////////////////////////////////////////////////////////
void
Free(
    void *pBuf
    );

/////////////////////////////////////////////////////////////////////////////
// Obsolete! Provided for compatibility with CoreMON
//  - Use NCPRINT() macro instead
//  - Remove when CoreMON printing issues are resolved!
void 
Printf(
    DWORD PrintLevel,
    LPSTR String, 
    ...
    );

/////////////////////////////////////////////////////////////////////
void
History(
    UINT HistoLevel,
    PCHAR Msg,
    DWORD Arg1, 
    DWORD Arg2, 
    DWORD Arg3
    );

/////////////////////////////////////////////////////////////////////
void
HistoryFlush(
    void
    );

/////////////////////////////////////////////////////////////////////
BOOL
System_SetupDma(
    PUCHAR Buffer,
    DWORD BufferSize,
    BOOL IsTx
    );

////////////////////////////////////////////////////////////////////////////
BOOL
System_GetDmaInfo(
    PDWORD PhysicalAddress, 
    PDWORD DmaTransferLen
    );
////////////////////////////////////////////////////////////////////////////
BOOL
System_DmaCleanup(
    void
    );

////////////////////////////////////////////////////////////////////////////
BOOL
System_InitializeSystem(
    PNETCHIP_DEVICE_CONTROL_BLOCK pDevCtrlB
    );

/////////////////////////////////////////////////////////////////////
void __cdecl 
Rdk_AtExit(
    void
    );
/////////////////////////////////////////////////////////////////////////////
void
Rdk_ResetNet2280(
    void
    );
/////////////////////////////////////////////////////////////////////////////
#endif // SYSTEM_H

/////////////////////////////////////////////////////////////////////
//  End of file
/////////////////////////////////////////////////////////////////////
