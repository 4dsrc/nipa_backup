/*
 *  MTPUSBFn.c
 *
 *  Windows compatible USB Function driver used to bind the MTP-USB Router
 *  to the physical USB hardware.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPUSBFnPrecomp.h"

/*  Local Defines
 */

/*  Local Types
 */

/*  Local Functions
 */

/*  Local Variables
 */

HANDLE                      g_hInstDLL;

static const WCHAR          _RgchUSBTransportInit[] = L"MTPUSBFn Init Mutex";


/*
 *  DllMain
 *
 *  Standard Windows DLL entry point
 *
 *  Arguments:
 *      See MSDN
 *
 *  Returns:
 *      See MSDN
 *
 */

BOOL WINAPI DllMain(HANDLE hinstDLL, DWORD dwReason, LPVOID lpvReserved)
{
    BOOL            fResult;

    /*  Handle the reason we were called
     */

    switch (dwReason)
    {
    case DLL_PROCESS_ATTACH:
        /*  Remember our instance
         */

        g_hInstDLL = hinstDLL;

        /*  Make sure that asserts do not fire on a UI thread
         */
        SetPSLAssertMode(PSLAM_HALT);
        fResult = TRUE;
        break;

    case DLL_PROCESS_DETACH:
        /*  Let the leak detection run
         */

        PSLMemShutdown();
        g_hInstDLL = NULL;
        fResult = TRUE;
        break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
        fResult = TRUE;
        break;

    default:
        /*  Should never get here
         */

        PSLASSERT(PSLFALSE);
        fResult = FALSE;
        break;
    }

    return fResult;
    lpvReserved;
}


/*
 *  Init
 *
 *  Initialization function called by the USBFn driver when the driver is
 *  loaded.
 *
 *  Arguments:
 *      LPCWSTR         szActiveKey         Device registry key causing load
 *
 *  Returns:
 *      DWORD       Device context to be used on subsequent calls, 0 to report
 *                    an error
 *
 */

DWORD WINAPI MTPUSB_Init(DWORD dwData)
{
    DWORD               dwContext;
    DWORD               dwResult;
    HANDLE              hMutex = NULL;
    MTPUSBDEVICECE      usbd = PSLNULL;

	dwResult = dwData;
    /*  Validate arguments
     */

    /*  Make sure that only one initialization is happening at a time
     */

    hMutex = CreateMutexW(NULL, TRUE, _RgchUSBTransportInit);
    if (NULL == hMutex)
    {
        dwContext = 0;
        goto Exit;
    }

    /*  And make sure that we acquired the mutex
     */

    if (ERROR_ALREADY_EXISTS == GetLastError())
    {
        /*  Try to acquire the mutex
         */

        dwResult = WaitForSingleObject(hMutex, INFINITE);
        switch (dwResult)
        {
        case WAIT_OBJECT_0:
            /*  Acquired the mutex- we can continue
             */

            break;

        case WAIT_TIMEOUT:
            /*  We should never get here- this should have been an infinite
             *  wait
             */

            SetLastError(ERROR_NOT_READY);

            /*  FALL THROUGH INTENTION */

        case WAIT_FAILED:
        default:
            /*  Nothing more we can do- report failure
             */

            dwContext = 0;
            goto Exit;
        }
    }

    /*  Create a USB device object
     */

	dwResult = MTPUSBDevicePCCreate(&usbd);
    if (ERROR_SUCCESS != dwResult)
    {
        SetLastError(dwResult);
        dwContext = 0;
        goto Exit;
    }

    /*  Return the new device as the context
     */

    dwContext = (DWORD)usbd;
    usbd = NULL;

Exit:
    SAFE_MTPUSBDEVICEDESTROY(usbd);
    SAFE_RELEASECLOSEMUTEX(hMutex);
    return dwContext;
}


/*
 *  Deinit
 *
 *  Uninitialize function called by USB function driver when the driver is
 *  closed
 *
 *  Arguments:
 *      DWORD           dwContext           Context to close
 *
 *  Returns:
 *      BOOL        TRUE on success
 *
 */

BOOL WINAPI MTPUSB_Deinit(DWORD dwContext)
{
    DWORD           dwResult;
    BOOL            fResult;
    HANDLE          hMutex = NULL;

    /*  Validate arguments
     */

    if (0 == dwContext)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        fResult = FALSE;
        goto Exit;
    }

    /*  Make sure that only one initialization is happening at a time
     */

    hMutex = CreateMutexW(NULL, TRUE, _RgchUSBTransportInit);
    if (NULL == hMutex)
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  And make sure that we acquired the mutex
     */

    if (ERROR_ALREADY_EXISTS == GetLastError())
    {
        /*  Try to acquire the mutex
         */

        dwResult = WaitForSingleObject(hMutex, INFINITE);
        switch (dwResult)
        {
        case WAIT_OBJECT_0:
            /*  Acquired the mutex- we can continue
             */

            break;

        case WAIT_TIMEOUT:
            /*  We should never get here- this should have been an infinite
             *  wait
             */

            SetLastError(ERROR_NOT_READY);

            /*  FALL THROUGH INTENTION */

        case WAIT_FAILED:
        default:
            /*  Nothing more we can do- report failure
             */

            fResult = FALSE;
            goto Exit;
        }
    }

    /*  Close down the device
     */

    dwResult = MTPUSBDevicePCDestroy((MTPUSBDEVICECE)dwContext);
    if (ERROR_SUCCESS != dwResult)
    {
        SetLastError(dwResult);
        fResult = FALSE;
        goto Exit;
    }

    /*  Report success
     */

    fResult = TRUE;

Exit:
    SAFE_RELEASECLOSEMUTEX(hMutex);
    return fResult;
}

BOOL WINAPI MTPUSB_QueryCanShutdown(DWORD dwContext)
{
    DWORD           dwResult;
    BOOL            fResult;
    HANDLE          hMutex = NULL;

    dwResult = MTPUSBDevicePCShutdown((MTPUSBDEVICECE)dwContext);
    if (ERROR_SUCCESS != dwResult)
    {
        SetLastError(dwResult);
        fResult = FALSE;
        goto Exit;
    }

    /*  Report success
     */

    fResult = TRUE;

Exit:
    SAFE_RELEASECLOSEMUTEX(hMutex);
    return fResult;
}
