/*
 *  MTPUSBDescriptor.c
 *
 *  Single location for all of the pre-compiled USB device information
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPUSBFnPrecomp.h"

#define USB_FULL_HIGH_SPEED_CONTROL_MAX_PACKET_SIZE 0x040 // 64 bytes

#define USB_FULL_SPEED_CONTROL_MAX_PACKET_SIZE      0x040 // 64 bytes
#define USB_FULL_SPEED_BULK_MAX_PACKET_SIZE         0x040 // 64 bytes
#define USB_FULL_SPEED_INTERRUPT_MAX_PACKET_SIZE    0x040 // 64 bytes
#define USB_FULL_SPEED_ISOCHRONOUS_MAX_PACKET_SIZE  0x3FF // 1023 bytes

#define USB_HIGH_SPEED_CONTROL_MAX_PACKET_SIZE      0x040 // 64 bytes
#define USB_HIGH_SPEED_BULK_MAX_PACKET_SIZE         0x200 // 512 bytes
#define USB_HIGH_SPEED_INTERRUPT_MAX_PACKET_SIZE    0x400 // 1024 bytes
#define USB_HIGH_SPEED_ISOCHRONOUS_MAX_PACKET_SIZE  0x400 // 1024 bytes

#define USER_ENDPOINT_COUNT     2           // Number of endpoints on this device (not including endpoint zero)
#define MTP_ENDPOINT_COUNT      3           // Number of endpoints of MTP interface on this device (not including endpoint zero)
#define CONFIG_ATTRIBUTES       ((0<<CONFIG_REMOTE_WAKEUP) | (1<<CONFIG_SELF_POWERED) | (1<<7))
#define NUM_INTERFACES          1

/*
 *  MTP USB High Speed Descriptors
 *
 */

/*  High Speed Device Descriptor
 */

USB_DEVICE_DESCRIPTOR       g_mtpUSBHSDeviceDesc =
{
    sizeof(USB_DEVICE_DESCRIPTOR),  // 0x00 Length of this descriptor
    DEVICE_DESC,                    // 0x01 Descriptor type
    LOBYTE(USBSPEC),                // 0x02 USB Specification Release Number (LSB)
    HIBYTE(USBSPEC),                // 0x03 USB Specification Release Number (MSB)
    USB_DEVICE_CLASS_RESERVED,                // 0x04 Class Code
    0x00,                           // 0x05 Sub Class Code
    0x00,                           // 0x06 Protocol
    USB_FULL_HIGH_SPEED_CONTROL_MAX_PACKET_SIZE,            // 0x07 Maximum Endpoint 0 Packet Size
    LOBYTE(MTPUSB_VID_MICROSOFT),            // 0x08 Vendor ID (LSB)
    HIBYTE(MTPUSB_VID_MICROSOFT),            // 0x09 Vendor ID (MSB)
    LOBYTE(MTPUSB_PID_MTPPK),              // 0x0a Product ID (LSB)
    HIBYTE(MTPUSB_PID_MTPPK),              // 0x0b Product ID (MSB)
    LOBYTE(MTPUSB_DEVICE_VERSION),         // 0x0c BCD Device Release Number (LSB)
    HIBYTE(MTPUSB_DEVICE_VERSION),         // 0x0d BCD Device Release Number (MSB)
    0x00,                           // 0x0e Index of string descriptor describing manufacturer
    0x00,                           // 0x0f Index of string descriptor describing product
    0x00,                           // 0x10 Index of string descriptor describing serial number
    0x01                            // 0x11 Number of configurations
};


///////////////////////////////////////////////////////////////////////////////
USB_DEVICE_QUALIFIER_DESCRIPTOR Transfer_DeviceQualifierDescriptor =
{   // Device Qualifier descriptor
    sizeof(USB_DEVICE_QUALIFIER_DESCRIPTOR),  // 0x00 Length of this descriptor
    DEVICE_QUALIFIER_DESC,          // 0x01 Descriptor type
    LOBYTE(USBSPEC),                // 0x02 USB Specification Release Number (LSB)
    HIBYTE(USBSPEC),                // 0x03 USB Specification Release Number (MSB)
    VENDOR_SPECIFIC,                // 0x04 Class Code
    0x00,                           // 0x05 Sub Class Code
    0x00,                           // 0x06 Protocol
    EP0_MAX_PACKET_SIZE,            // 0x07 Maximum Endpoint 0 Packet Size
    0x01,                           // 0x08 Number of configurations 64
    0x00                            // 0x09 Reserve
};

///////////////////////////////////////////////////////////////////////////////
// Configurations and configuration descriptors:
//  - All configuration, interface and endpoint descriptors are returned
//    when the host issues a GET CONFIGURATION DESCRIPTOR request
//  - Make sure your compiler packs descriptor structures without gaps
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
TRANSFER_CONFIGURATION Transfer_HS_Configuration =
{   // Configuration when connected to a High Speed host
    {   // Configuration descriptor
        sizeof(USB_CONFIGURATION_DESCRIPTOR),
        // 0x00 bLength: Length of this descriptor
        CONFIGURATION_DESC,           // 0x01 Type (CONFIGURATION_DESC or OTHER_SPEED_CONFIGURATION_DESC)
        sizeof(TRANSFER_CONFIGURATION),                     // 0x02 wTotalLengthLo: Total length for this configuration (LSB)
        0x00,                   // 0x03 wTotalLengthHi: Total length for this configuration (MSB)
        NUM_INTERFACES,         // 0x04 bNumInterfaces: Number of Interfaces
        0x01,                   // 0x05 bConfigurationValue: Number of this configuration
        0x00,                   // 0x06 iConfiguration: Index of string descriptor describing this configuration
        CONFIG_ATTRIBUTES,      // 0x07 bmAttributes: 7:AlwaysSet|6:SelfPowered|5:RemoteWakeup
        0x32                    // 0x08 bMaxPower: Max USB power (in 2mA units)
    },

    {   // Interface Descriptor:
        sizeof(USB_INTERFACE_DESCRIPTOR),
        // 0x00 Size of this descriptor
        INTERFACE_DESC,         // 0x01 Type
        0x00,                   // 0x02 Number of this interface
        0x00,                   // 0x03 Alternate interface
        MTP_ENDPOINT_COUNT,     // 0x04 Number of endpoints in this interface (without EP0)
        MTPUSB_DEVICE_CLASS_IMAGE,                   // 0x05 Class Code
        MTPUSB_DEVICE_SUBCLASS_IMAGE_CAPTURE,                   // 0x06 Sub Class Code
        MTPUSB_DEVICE_PROTOCOL_PIMA15740,                   // 0x07 Device Protocol
        0x00,                   // 0x08 Index of string descriptor describing this interface
    },

    {   // Endpoint Descriptors:     Type,   Dir,     MaxPkt, UsbAddr,    Poll
        {NC_MAKE_ENDPOINT_DESCRIPTOR(BULK,  EP_OUT,     512,    1,          0)},
        {NC_MAKE_ENDPOINT_DESCRIPTOR(BULK,  EP_IN,      512,    1,          0)},
        {NC_MAKE_ENDPOINT_DESCRIPTOR(INTR,  EP_IN,       64,    2,       0x10)}
    },
};

///////////////////////////////////////////////////////////////////////////////
TRANSFER_CONFIGURATION Transfer_FS_Configuration =
{   // Configuration when connected to a Full Speed host
    {   // Configuration descriptor
        sizeof(USB_CONFIGURATION_DESCRIPTOR),
        // 0x00 Length of this descriptor
        CONFIGURATION_DESC,                   // 0x01 Type (CONFIGURATION_DESC or OTHER_SPEED_CONFIGURATION_DESC)
        sizeof(TRANSFER_CONFIGURATION),
        // 0x02 wTotalLengthLo: Total length for this configuration (LSB)
        0x00,                   // 0x03 Total length for this configuration (MSB)
        NUM_INTERFACES,         // 0x04 Number of Interfaces
        0x01,                   // 0x05 Number of this configuration
        0x00,                   // 0x06 Index of string descriptor describing this configuration
        CONFIG_ATTRIBUTES,      // 0x07 bmAttributes: 7:AlwaysSet|6:SelfPowered|5:RemoteWakeup
        0x00                    // 0x08 Max USB power (in 2mA units)
    },

    {   // Interface Descriptor:
        sizeof(USB_INTERFACE_DESCRIPTOR),
        // 0x00 Size of this descriptor
        INTERFACE_DESC,         // 0x01 Type
        0x00,                   // 0x02 Number of this interface
        0x00,                   // 0x03 Alternate interface
        MTP_ENDPOINT_COUNT,     // 0x04 Number of endpoints in this interface (without EP0)
        MTPUSB_DEVICE_CLASS_IMAGE,                   // 0x05 Class Code
        MTPUSB_DEVICE_SUBCLASS_IMAGE_CAPTURE,                   // 0x06 Sub Class Code
        MTPUSB_DEVICE_PROTOCOL_PIMA15740,                   // 0x07 Device Protocol
        0x00,                   // 0x08 Index of string descriptor describing this interface
    },

    {   // Endpoint Descriptors:     Type,   Dir,     MaxPkt, UsbAddr,    Poll
        {NC_MAKE_ENDPOINT_DESCRIPTOR(BULK,  EP_OUT,     64,    1,          0)},
        {NC_MAKE_ENDPOINT_DESCRIPTOR(BULK,  EP_IN,      64,    1,          0)},
        {NC_MAKE_ENDPOINT_DESCRIPTOR(INTR,  EP_IN,       64,    2,       0x10)}
    },
};

/*
 *  MS OS String Descriptor
 *
 */

USB_MS_OS_STRING_DESCRIPTOR g_mtpUSBMsftOSStringDesc =
{
    sizeof(USB_MS_OS_STRING_DESCRIPTOR),            /* Descriptor size      */
    STRING_DESC,                     /* Descriptor type      */
    USB_MS_OS_STRING_DESC_SIGNATURE,                /* MS String Desc Sig   */
    MTPUSB_VENDOR_EXTENSION_CODE,                   /* MTP Vendor Extension */
    0x00                                            /* Reserved             */
};


/*
 *  MTP USB Extended Configuration Descriptor
 *
 */

MTPUSB_EXTENDED_CONFIGURATION_DESCRIPTOR    g_mtpUSBExtendedConfigDesc =
{
    sizeof(MTPUSB_EXTENDED_CONFIGURATION_DESCRIPTOR),   /* Desc size        */
    USB_MS_EXTENDED_CONFIGURATION_DESCRIPTOR_VERSION,   /* Desc version     */
    USB_MS_EXTENDED_CONFIGURATION_DESCRIPTOR_TYPE,      /* Desc type        */
    0x01,                                               /* Features present */
    MS_EXTENDED_CONFIGURATION_HEADER_RESERVED,          /* Reserved         */
    0x00,                                               /* Interface start  */
    0x01,                                               /* Num interfaces   */
    MTPUSB_COMPATIBLE_ID,                               /* Compatible ID    */
    MTPUSB_SUBCOMPATIBLE_ID_GENERIC,                    /* Subcompatible ID */
    MS_EXTENDED_CONFIGURATION_FEATURE_RESERVED          /* Reserved         */
};
