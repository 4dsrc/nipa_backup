/*
 *  MTPUSBBufferMgr.c
 *
 *  Contains definitions of the MTP-USB buffer handling utilized by the
 *  MTP-USB device.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPUSBFnPrecomp.h"

/*  Local defines
 */

#define MTPUSBBUFFER_COOKIE         'usbB'

#define MTPUSBBUFFERMGROBJ_COOKIE   'bMgr'

enum
{
    MTPUSBBUFFERINDEX_COMMAND = 0,
    MTPUSBBUFFERINDEX_TRANSMIT_DATA,
    MTPUSBBUFFERINDEX_RECEIVE_DATA,
    MTPUSBBUFFERINDEX_RESPONSE,
    MTPUSBBUFFERINDEX_EVENT,
    MTPUSBBUFFERINDEX_CONTROL,
    MTPUSBBUFFERINDEX_TRANSMIT_SHORT_DATA,
    MTPUSBBUFFERINDEX_MAX_INDEX
};

#define MTPBUFFER_TO_INDEX(_bt)     ((_bt) - MTPBUFFER_COMMAND)

#define INDEX_TO_MTPBUFFER(_idxB)   ((_idxB) + MTPBUFFER_COMMAND);

#define MTPUSBBUFFERHDR_SET_INFO(_pmtph, _idxBuffer, _idxEntry) \
        (_pmtph)->dwInfo = ((_idxBuffer) << 16) | ((_idxEntry) & 0xffff)

#define MTPUSBBUFFERHDR_GET_INDEX(_pmtph) \
        ((_pmtph)->dwInfo >> 16)

#define MTPUSBBUFFERHDR_GET_ENTRY(_pmtph) \
        ((_pmtph)->dwInfo & 0xffff)


/*  Local types
 */

typedef struct _MTPUSBBUFFERHDR
{
#ifdef PSL_ASSERTS
    PSLUINT32               dwCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT32               dwInfo;
    PSLUINT32               cbSize;
    MTPUSBBUFFERMGR         mtpusbbm;
} MTPUSBBUFFERHDR;

typedef struct _MTPUSBBUFFERREQUEST
{
    PSLUINT32               cbRequest;
    PSLPARAM                aParam;
    PSLMSGQUEUE             mqNotify;
    PSLMSGPOOL              mpNotify;
} MTPUSBBUFFERREQUEST;

typedef struct _MTPUSBBUFFERINFO
{
    PSLUINT32               cBuffers;
    PSLUINT32               cbMaxBuffer;
    PSLBYTE**               rgpbBuffers;
    MTPUSBBUFFERREQUEST     brPending;
} MTPUSBBUFFERINFO;

typedef struct _MTPUSBBUFFERMGROBJ
{
#ifdef PSL_ASSERTS
    PSLUINT32               dwCookie;
#endif  /* PSL_ASSERTS */
    PSLHANDLE               hMutex;
    PSLUINT32               cbAlignment;
    MTPUSBBUFFERINFO        rgbiTypes[MTPUSBBUFFERINDEX_MAX_INDEX];
    PSLBYTE                 rgbAvailable[MTPUSBBUFFERINDEX_MAX_INDEX];
} MTPUSBBUFFERMGROBJ;

/*  Local Functions
 */

/*  Local Variables
 */


/*
 *  MTPUSBBufferMgrCreate
 *
 *  Creates a buffer object
 *
 *  Arguments:
 *      PSLUINT32       cbAlignment         Alignment of the data buffer
 *      PSLUINT32       cbMaxControlSize    Maximum size of a control packet
 *      PSLUINT32       cbMaxBulkSize       Maximum size of a bulk packet
 *      PSLUINT32       cbMaxInterruptSize  Maximum size of an interrupt packet
 *      MTPUSBBUFFERMGR*
 *                      pmtpusbbm           Return buffer for buffer object
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBBufferMgrCreate(PSLUINT32 cbAlignment,
                        PSLUINT32 cbMaxControlSize, PSLUINT32 cbMaxBulkSize,
                        PSLUINT32 cbMaxInterruptSize,
                        MTPUSBBUFFERMGR* pmtpusbbm)
{
    PSLUINT32           cbAlloc;
    PSLUINT32           cbMaxCommand;
    PSLUINT32           cbMaxData;
    PSLUINT32           cbMaxResponse;
    PSLUINT32           cbMaxEvent;
    PSLUINT32           idxBuffer;
    PSLUINT32           idxType;
    PSLSTATUS           ps;
    MTPUSBBUFFERMGROBJ* pmtpusbBO = PSLNULL;
    MTPUSBBUFFERINFO*   pbiCur;
    PSLBYTE*            pbData;

    /*  Currently we reserve only 8 bits for tracking available buffers- make
     *  sure that this is enough to handle each of the buffer counts
     */

    PSLASSERT(MTPUSB_MAX_COMMAND_BUFFERS <= MTPUSB_MAX_BUFFERS);
    PSLASSERT(MTPUSB_MAX_TRANSMIT_BUFFERS <= MTPUSB_MAX_BUFFERS);
    PSLASSERT(MTPUSB_MAX_TRANSMIT_SHORT_BUFFERS <= MTPUSB_MAX_BUFFERS);
    PSLASSERT(MTPUSB_MAX_RECEIVE_BUFFERS <= MTPUSB_MAX_BUFFERS);
    PSLASSERT(MTPUSB_MAX_RESPONSE_BUFFERS <= MTPUSB_MAX_BUFFERS);
    PSLASSERT(MTPUSB_MAX_EVENT_BUFFERS <= MTPUSB_MAX_BUFFERS);
    PSLASSERT(MTPUSB_MAX_CONTROL_BUFFERS <= MTPUSB_MAX_BUFFERS);

    /*  And validate that the buffer indices line up
     */

    PSLASSERT(MTPBUFFER_TO_INDEX(MTPBUFFER_COMMAND) ==
                            MTPUSBBUFFERINDEX_COMMAND);
    PSLASSERT(MTPBUFFER_TO_INDEX(MTPBUFFER_TRANSMIT_DATA) ==
                            MTPUSBBUFFERINDEX_TRANSMIT_DATA);
    PSLASSERT(MTPBUFFER_TO_INDEX(MTPBUFFER_RECEIVE_DATA) ==
                            MTPUSBBUFFERINDEX_RECEIVE_DATA);
    PSLASSERT(MTPBUFFER_TO_INDEX(MTPBUFFER_RESPONSE) ==
                            MTPUSBBUFFERINDEX_RESPONSE);
    PSLASSERT(MTPBUFFER_TO_INDEX(MTPBUFFER_EVENT) ==
                            MTPUSBBUFFERINDEX_EVENT);
    PSLASSERT(MTPBUFFER_TO_INDEX(MTPUSBBUFFER_CONTROL) ==
                            MTPUSBBUFFERINDEX_CONTROL);
    PSLASSERT(MTPBUFFER_TO_INDEX(MTPUSBBUFFER_TRANSMIT_SHORT_DATA) ==
                            MTPUSBBUFFERINDEX_TRANSMIT_SHORT_DATA);

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpusbbm)
    {
        *pmtpusbbm = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((0 == cbMaxControlSize) || (0 == cbMaxBulkSize) ||
        (0 == cbMaxInterruptSize) || (PSLNULL == pmtpusbbm))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  We require at least DWORD alignment on the buffers
     */

    PSLASSERT(0 == (0x3 & cbAlignment));
    if (0x3 & cbAlignment)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Determine the max size for each individual buffer and align it on
     *  an appropriate packet boundary
     */

    cbMaxCommand = PSLAllocAlign(sizeof(MTPUSBPACKET_OPERATION_REQUEST),
                            cbMaxBulkSize);
    cbMaxData = PSLAllocAlign(MTPUSB_MAX_DATA_CHUNK_SIZE, cbMaxBulkSize);
    cbMaxResponse = PSLAllocAlign(sizeof(MTPUSBPACKET_OPERATION_RESPONSE),
                            cbMaxBulkSize);
    cbMaxEvent = PSLAllocAlign(sizeof(MTPUSBPACKET_EVENT), cbMaxInterruptSize);

    /*  Determine the total allocation size necessary to host all the object
     *  and all of the buffers and then allocate the object
     *
     *  NOTE: Buffers may be very large and do not need to be zero inited
     *  so we do not do it all at once here
     */

    cbAlloc = sizeof(MTPUSBBUFFERMGROBJ) +
                (sizeof(PSLBYTE*) * MTPUSB_MAX_COMMAND_BUFFERS) +
                (cbMaxCommand * MTPUSB_MAX_COMMAND_BUFFERS) +
                (sizeof(PSLBYTE*) * MTPUSB_MAX_TRANSMIT_BUFFERS) +
                (cbMaxData * MTPUSB_MAX_TRANSMIT_BUFFERS) +
                (sizeof(PSLBYTE*) * MTPUSB_MAX_TRANSMIT_SHORT_BUFFERS) +
                (cbMaxBulkSize * MTPUSB_MAX_TRANSMIT_SHORT_BUFFERS) +
                (sizeof(PSLBYTE*) * MTPUSB_MAX_RECEIVE_BUFFERS) +
                (cbMaxData * MTPUSB_MAX_RECEIVE_BUFFERS) +
                (sizeof(PSLBYTE*) * MTPUSB_MAX_RESPONSE_BUFFERS) +
                (cbMaxResponse * MTPUSB_MAX_RESPONSE_BUFFERS) +
                (sizeof(PSLBYTE*) * MTPUSB_MAX_EVENT_BUFFERS) +
                (cbMaxEvent * MTPUSB_MAX_EVENT_BUFFERS) +
                (sizeof(PSLBYTE*) * MTPUSB_MAX_CONTROL_BUFFERS) +
                (cbMaxControlSize * MTPUSB_MAX_CONTROL_BUFFERS) +
                ((sizeof(MTPUSBBUFFERHDR) + cbAlignment) * MTPUSB_TOTAL_BUFFERS);
    pmtpusbBO = (MTPUSBBUFFERMGROBJ*)PSLMemAlloc(PSLMEM_FIXED, cbAlloc);
    if (PSLNULL == pmtpusbBO)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }
    PSLZeroMemory(pmtpusbBO, sizeof(*pmtpusbBO));

#ifdef PSL_ASSERTS
    /*  Initialize the cookie
     */

    pmtpusbBO->dwCookie = MTPUSBBUFFERMGROBJ_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Create a mutex for this object
     */

    ps = PSLMutexOpen(0, PSLNULL, &(pmtpusbBO->hMutex));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Walk through and initialize each of the buffer pointers to the start
     *  of their buffer- remember that each buffer consists of the maximum
     *  buffer for the specified type, a buffer header, and space to align
     *  the buffer on
     */

    pbData = (PSLBYTE*)pmtpusbBO + sizeof(MTPUSBBUFFERMGROBJ);
    for (idxType = 0; idxType < MTPUSBBUFFERINDEX_MAX_INDEX; idxType++)
    {
        /*  Save some redirection
         */

        pbiCur = &(pmtpusbBO->rgbiTypes[idxType]);

        /*  Based on the type initialize the data in the buffer info
         */

        switch (idxType)
        {
        case MTPUSBBUFFERINDEX_COMMAND:
            pbiCur->cBuffers = MTPUSB_MAX_COMMAND_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxCommand;
            break;

        case MTPUSBBUFFERINDEX_TRANSMIT_DATA:
            pbiCur->cBuffers = MTPUSB_MAX_TRANSMIT_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxData;
            break;

        case MTPUSBBUFFERINDEX_RECEIVE_DATA:
            pbiCur->cBuffers = MTPUSB_MAX_RECEIVE_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxData;
            break;

        case MTPUSBBUFFERINDEX_RESPONSE:
            pbiCur->cBuffers = MTPUSB_MAX_RESPONSE_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxResponse;
            break;

        case MTPUSBBUFFERINDEX_EVENT:
            pbiCur->cBuffers = MTPUSB_MAX_EVENT_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxEvent;
            break;

        case MTPUSBBUFFERINDEX_CONTROL:
            pbiCur->cBuffers = MTPUSB_MAX_CONTROL_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxControlSize;
            break;

        case MTPUSBBUFFERINDEX_TRANSMIT_SHORT_DATA:
            pbiCur->cBuffers = MTPUSB_MAX_TRANSMIT_SHORT_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxBulkSize;
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Assign the buffer list
         */

        pbiCur->rgpbBuffers = (PSLBYTE**)pbData;
        pbData += sizeof(PSLBYTE*) * pbiCur->cBuffers;

        /*  And initialize the buffer pointers
         */

        for (idxBuffer = 0;
                idxBuffer < pbiCur->cBuffers;
                idxBuffer++, pbData +=
                    pbiCur->cbMaxBuffer + sizeof(MTPUSBBUFFERHDR) + cbAlignment)
        {
            pbiCur->rgpbBuffers[idxBuffer] = pbData;
        }
    }

    /*  Mark all of the buffers as available in each class
     */

    pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_COMMAND] =
                    (PSLUINT32)(1 << MTPUSB_MAX_COMMAND_BUFFERS) - 1;
    pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_TRANSMIT_DATA] =
                    (PSLUINT32)(1 << MTPUSB_MAX_TRANSMIT_BUFFERS) - 1;
    pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_TRANSMIT_SHORT_DATA] =
                    (PSLUINT32)(1 << MTPUSB_MAX_TRANSMIT_SHORT_BUFFERS) - 1;
    pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_RECEIVE_DATA] =
                    (PSLUINT32)(1 << MTPUSB_MAX_RECEIVE_BUFFERS) - 1;
    pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_RESPONSE] =
                    (PSLUINT32)(1 << MTPUSB_MAX_RESPONSE_BUFFERS) - 1;
    pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_EVENT] =
                    (PSLUINT32)(1 << MTPUSB_MAX_EVENT_BUFFERS) - 1;
    pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_CONTROL] =
                    (PSLUINT32)(1 << MTPUSB_MAX_CONTROL_BUFFERS) - 1;

    /*  And store the data sizes
     */

    pmtpusbBO->cbAlignment = cbAlignment;

    /*  Return the new buffer object
     */

    *pmtpusbbm = pmtpusbBO;
    pmtpusbBO = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBBUFFERMGRDESTROY(pmtpusbBO);
    return ps;
}


/*
 *  MTPUSBBufferMgrDestroy
 *
 *  Destroys a buffer object
 *
 *  Arguments:
 *      MTPUSBBUFFERMGR mtpusbbm            Buffer object to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBBufferMgrDestroy(MTPUSBBUFFERMGR mtpusbbm)
{
    PSLHANDLE           hMutexClose = PSLNULL;
    MTPUSBBUFFERMGROBJ* pmtpusbBO;
    PSLSTATUS           ps;

    /*  Make sure we have work to do
     */

    if (PSLNULL == mtpusbbm)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Unpack the object
     */

    pmtpusbBO = (MTPUSBBUFFERMGROBJ*)mtpusbbm;
    PSLASSERT(MTPUSBBUFFERMGROBJ_COOKIE == pmtpusbBO->dwCookie);

    /*  Acquire the mutex if present
     */

    if (PSLNULL != pmtpusbBO->hMutex)
    {
        ps = PSLMutexAcquire(pmtpusbBO->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexClose = pmtpusbBO->hMutex;

        /*  If we have a mutex then we were fully initialized- go ahead and
         *  check to make sure that all buffers are available at this point
         */

        PSLASSERT((PSLUINT32)(1 << MTPUSB_MAX_COMMAND_BUFFERS) - 1 ==
            pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_COMMAND]);
        PSLASSERT((PSLUINT32)(1 << MTPUSB_MAX_TRANSMIT_BUFFERS) - 1 ==
            pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_TRANSMIT_DATA]);
        PSLASSERT((PSLUINT32)(1 << MTPUSB_MAX_TRANSMIT_SHORT_BUFFERS) - 1 ==
            pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_TRANSMIT_SHORT_DATA]);
        PSLASSERT((PSLUINT32)(1 << MTPUSB_MAX_RECEIVE_BUFFERS) - 1 ==
            pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_RECEIVE_DATA]);
        PSLASSERT((PSLUINT32)(1 << MTPUSB_MAX_RESPONSE_BUFFERS) - 1 ==
            pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_RESPONSE]);
        PSLASSERT((PSLUINT32)(1 << MTPUSB_MAX_EVENT_BUFFERS) - 1 ==
            pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_EVENT]);
        PSLASSERT((PSLUINT32)(1 << MTPUSB_MAX_CONTROL_BUFFERS) - 1 ==
            pmtpusbBO->rgbAvailable[MTPUSBBUFFERINDEX_CONTROL]);
    }

    /*  Release the memory
     */

    PSLMemFree(pmtpusbBO);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexClose);
    return ps;
}


/*
 *  MTPUSBBufferMgrRequest
 *
 *  Requests a buffer.  If the buffer is not available a request is recorded
 *  so that when the buffer becomes available it can be fulfilled
 *  asynchronously
 *
 *  Arguments:
 *      MTPUSBBUFFERMGR mtpusbbm            Buffer object to fulfill request
 *      PSLUINT32       dwBufferType        Type of buffer requested
 *      PSLUINT32       cbRequested         Size of buffer requested
 *      PSLPARAM        aParam              Parameter to return on an
 *                                            asynchronous completion
 *      PSLVOID*        ppvBuffer           Return location for buffer
 *      PSLUINT32*      pcbActual           Actual number of bytes available
 *      PSLMSGQUEUE     mqAsync             Message queue for async response
 *      PSLMSGPOOL      mpAsync             Message pool for async response
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_WOULD_BLOCK if the
 *                      request cannot be immediately fulfilled
 *
 */

PSLSTATUS MTPUSBBufferMgrRequest(MTPUSBBUFFERMGR mtpusbbm,
                            PSLUINT32 dwBufferType, PSLUINT32 cbRequested,
                            PSLPARAM aParam, PSLVOID** ppvBuffer,
                            PSLUINT32* pcbActual, PSLMSGQUEUE mqAsync,
                            PSLMSGPOOL mpAsync)
{
    PSLBYTE                 bAvailable;
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLUINT32               idxBuffer;
    PSLUINT32               idxEntry;
    PSLBYTE*                pbBuffer = PSLNULL;
    MTPUSBBUFFERREQUEST*    pmtpusbBR;
    MTPUSBBUFFERMGROBJ*     pmtpusbBO;
    MTPUSBBUFFERHDR*        pmtpusbh;
    PSLSTATUS               ps;
    PSLMSG*                 pMsg = PSLNULL;

    /*  Clear results for safety
     */

    if (PSLNULL != ppvBuffer)
    {
        *ppvBuffer = PSLNULL;
    }
    if (PSLNULL != pcbActual)
    {
        *pcbActual = 0;
    }

    /*  Determine the buffer index
     */

    idxBuffer = MTPBUFFER_TO_INDEX(dwBufferType);

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpusbbm) ||
        (MTPUSBBUFFERINDEX_MAX_INDEX <= idxBuffer) ||
        ((PSLNULL != ppvBuffer) && (PSLNULL == pcbActual)) ||
        ((PSLNULL == ppvBuffer) && (PSLNULL == mqAsync)) ||
        ((PSLNULL != mqAsync) && (PSLNULL == mpAsync)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the object
     */

    pmtpusbBO = (MTPUSBBUFFERMGROBJ*)mtpusbbm;
    PSLASSERT(MTPUSBBUFFERMGROBJ_COOKIE == pmtpusbBO->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmtpusbBO->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmtpusbBO->hMutex;

    /*  Check to see if all buffers are in use
     */

    if (0 == pmtpusbBO->rgbAvailable[idxBuffer])
    {
        /*  If we do not have a queue provided we cannot create a pending
         *  request- report not available as an error
         */

        if (PSLNULL == mqAsync)
        {
            ps = PSLERROR_NOT_AVAILABLE;
            goto Exit;
        }

        /*  Save some redirection
         */

        pmtpusbBR = &(pmtpusbBO->rgbiTypes[idxBuffer].brPending);

        /*  If we already have a pending request report that we have
         *  insufficient buffer to hande this
         */

        PSLASSERT(0 == pmtpusbBR->cbRequest);
        if (0 != pmtpusbBR->cbRequest)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  Record the information about the request and report that the
         *  buffer was not available
         */

        pmtpusbBR->cbRequest = cbRequested;
        pmtpusbBR->aParam = aParam;
        pmtpusbBR->mqNotify = mqAsync;
        pmtpusbBR->mpNotify = mpAsync;
        ps = PSLSUCCESS_WOULD_BLOCK;
        goto Exit;
    }

    /*  We have a buffer available- figure out which one in the list is
     *  available
     */

    for (idxEntry = 0, bAvailable = pmtpusbBO->rgbAvailable[idxBuffer];
            (idxEntry < MTPUSB_MAX_BUFFERS) && (0 == (0x1 & bAvailable));
            bAvailable >>= 1, idxEntry++)
    {
    }

    /*  Retrieve the buffer and determine the correct alignment while accounting
     *  for the header
     */

    pbBuffer = PSLMemAlign(
                (pmtpusbBO->rgbiTypes[idxBuffer].rgpbBuffers[idxEntry] +
                        sizeof(MTPUSBBUFFERHDR)),
                pmtpusbBO->cbAlignment);
    pmtpusbh = ((MTPUSBBUFFERHDR*)pbBuffer) - 1;

    /*  Initialize the header
     */

#ifdef PSL_ASSERTS
    pmtpusbh->dwCookie = MTPUSBBUFFER_COOKIE;
#endif  /* PSL_ASSERTS */
    MTPUSBBUFFERHDR_SET_INFO(pmtpusbh, idxBuffer, idxEntry);
    pmtpusbh->cbSize = min(pmtpusbBO->rgbiTypes[idxBuffer].cbMaxBuffer,
                            cbRequested);
    pmtpusbh->mtpusbbm = mtpusbbm;

    /*  Record that the buffer is in use
     */

    pmtpusbBO->rgbAvailable[idxBuffer] &= ~(1 << idxEntry);

    /*  Figure out if we can return the buffer directly or if it should be
     *  returned in a message
     */

    if (PSLNULL != ppvBuffer)
    {
        /*  Return the buffer directly
         */

        *ppvBuffer = pbBuffer;
        *pcbActual = pmtpusbh->cbSize;

        /*  Report success
         */

        ps = PSLSUCCESS;
    }
    else
    {
        /*  Allocate a message to return the buffer in
         */

        ps = MTPMsgAlloc(mpAsync, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = MTPUSBMSG_BUFFER_AVAILABLE;
        pMsg->aParam0 = INDEX_TO_MTPBUFFER(idxBuffer);
        pMsg->aParam1 = aParam;
        pMsg->aParam2 = (PSLPARAM)pbBuffer;
        pMsg->alParam = pmtpusbh->cbSize;

        ps = MTPMsgPost(mqAsync, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = PSLNULL;

        /*  Report buffer pending
         */

        ps = PSLSUCCESS_WOULD_BLOCK;
    }
    pbBuffer = PSLNULL;

Exit:
    if (PSLNULL != pbBuffer)
    {
        MTPUSBBufferMgrRelease(pbBuffer);
    }
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_MTPMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPUSBBufferMgrCancelRequest
 *
 *  Cancels any pending buffer requests for the type specified
 *
 *  Arguments:
 *      MTPUSBBUFFERMGR mtpusbbm            Buffer object to fulfill request
 *      PSLUINT32       dwBufferType        Type of buffer requested
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if no
 *                        request was pending
 *
 */

PSLSTATUS MTPUSBBufferMgrCancelRequest(MTPUSBBUFFERMGR mtpusbbm,
                            PSLUINT32 dwBufferType)
{
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLUINT32               idxBuffer;
    MTPUSBBUFFERREQUEST*    pmtpusbBR;
    MTPUSBBUFFERMGROBJ*     pmtpusbBO;
    PSLSTATUS               ps;

    /*  Determine the buffer index
     */

    idxBuffer = MTPBUFFER_TO_INDEX(dwBufferType);

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpusbbm) ||
        (MTPUSBBUFFERINDEX_MAX_INDEX <= idxBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the object
     */

    pmtpusbBO = (MTPUSBBUFFERMGROBJ*)mtpusbbm;
    PSLASSERT(MTPUSBBUFFERMGROBJ_COOKIE == pmtpusbBO->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmtpusbBO->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmtpusbBO->hMutex;

    /*  Save some redirection
     */

    pmtpusbBR = &(pmtpusbBO->rgbiTypes[idxBuffer].brPending);

    /*  Determine if we have a request and then always wipe it
     */

    ps = (0 != pmtpusbBR->cbRequest) ? PSLSUCCESS : PSLSUCCESS_NOT_FOUND;
    PSLZeroMemory(pmtpusbBR, sizeof(*pmtpusbBR));

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPUSBBufferMgrRelease
 *
 *  Releases a buffer handling any pending requests
 *
 *  Arguments:
 *      PSLVOID*        pvBuffer            Buffer to release
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBBufferMgrRelease(PSLVOID* pvBuffer)
{
    PSLUINT32               idxType;
    PSLUINT32               idxEntry;
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLMSG*                 pMsg = PSLNULL;
    MTPUSBBUFFERREQUEST*    pmtpusbBR;
    MTPUSBBUFFERMGROBJ*     pmtpusbBO;
    MTPUSBBUFFERHDR*        pmtpusbh;
    PSLSTATUS               ps;

    /*  Make sure we have work to do
     */

    if (PSLNULL == pvBuffer)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Locate the header for this buffer
     */

    pmtpusbh = ((MTPUSBBUFFERHDR*)pvBuffer) - 1;
    PSLASSERT(MTPUSBBUFFER_COOKIE == pmtpusbh->dwCookie);

    /*  And unpack it
     */

    idxType = MTPUSBBUFFERHDR_GET_INDEX(pmtpusbh);
    idxEntry = MTPUSBBUFFERHDR_GET_ENTRY(pmtpusbh);
    pmtpusbBO = (MTPUSBBUFFERMGROBJ*)(pmtpusbh->mtpusbbm);
    PSLASSERT(MTPUSBBUFFERMGROBJ_COOKIE == pmtpusbBO->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmtpusbBO->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmtpusbBO->hMutex;

    /*  Check to see if there is a pending request for this type
     */

    if (0 != pmtpusbBO->rgbiTypes[idxType].brPending.cbRequest)
    {
        /*  Save some redirection
         */

        pmtpusbBR = &(pmtpusbBO->rgbiTypes[idxType].brPending);

        /*  At this point the way the buffer is laid out will not change-
         *  we just want to update the size
         */

        pmtpusbh->cbSize = min(pmtpusbBR->cbRequest,
                            pmtpusbBO->rgbiTypes[idxType].cbMaxBuffer);

        /*  Allocate a message and forward the information along to the
         *  destination
         */

        ps = MTPMsgAlloc(pmtpusbBR->mpNotify, &pMsg);
        if (PSL_SUCCEEDED(ps))
        {
            pMsg->msgID = MTPUSBMSG_BUFFER_AVAILABLE;
            pMsg->aParam0 = INDEX_TO_MTPBUFFER(idxType);
            pMsg->aParam1 = pmtpusbBR->aParam;
            pMsg->aParam2 = (PSLPARAM)pvBuffer;
            pMsg->alParam = pmtpusbh->cbSize;

            ps = MTPMsgPost(pmtpusbBR->mqNotify, pMsg);
            if (PSL_SUCCEEDED(ps))
            {
                PSLZeroMemory(pmtpusbBR, sizeof(*pmtpusbBR));
                pMsg = PSLNULL;
                goto Exit;
            }
        }

        /*  We were unable to succesfully fulfill this request- leave it
         *  pending but go ahead and return the buffer
         */
    }

    /*  Mark the buffer as available
     */

    pmtpusbBO->rgbAvailable[idxType] |= (1 << idxEntry);

    /*  And clear the header so that we do not have a re-use issue
     */

    PSLZeroMemory(pmtpusbh, sizeof(*pmtpusbh));

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_MTPMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPUSBBufferMgrGetInfo
 *
 *  Returns information about a MTP USB buffer
 *
 *  Arguments:
 *      PSLVOID*        pvBuffer            Buffer to check
 *      MTPUSBBUFFERMGR*
 *                      pmtpusbbm           Buffer this is from
 *      PSLUINT32*      pcbSize             Size of the buffer
 *      PSLUINT32*      pdwBufferType       Buffer type
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBBufferMgrGetInfo(PSLVOID* pvBuffer, MTPUSBBUFFERMGR* pmtpusbbm,
                            PSLUINT32* pcbSize, PSLUINT32* pdwBufferType)
{
    MTPUSBBUFFERHDR*    pmtpusbh;
    PSLSTATUS           ps;

    /*  Clear results for safety
     */

    if (PSLNULL != pmtpusbbm)
    {
        *pmtpusbbm = PSLNULL;
    }
    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }
    if (PSLNULL != pdwBufferType)
    {
        *pdwBufferType = MTPBUFFER_UNKNOWN;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pvBuffer) ||
        ((PSLNULL == pmtpusbbm) && (PSLNULL == pcbSize) &&
                            (PSLNULL == pdwBufferType)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Locate the header for this buffer
     */

    pmtpusbh = ((MTPUSBBUFFERHDR*)pvBuffer) - 1;
    PSLASSERT(MTPUSBBUFFER_COOKIE == pmtpusbh->dwCookie);

    /*  And unpack the information from it
     */

    if (PSLNULL != pmtpusbbm)
    {
        *pmtpusbbm = pmtpusbh->mtpusbbm;
    }
    if (PSLNULL != pcbSize)
    {
        *pcbSize = pmtpusbh->cbSize;
    }
    if (PSLNULL != pdwBufferType)
    {
        *pdwBufferType = INDEX_TO_MTPBUFFER(MTPUSBBUFFERHDR_GET_INDEX(pmtpusbh));
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}

