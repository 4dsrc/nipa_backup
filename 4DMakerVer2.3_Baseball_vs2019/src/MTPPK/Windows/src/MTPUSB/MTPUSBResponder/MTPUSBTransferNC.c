//  Copyright (c) 2009 Microsoft Corporation
//  All rights reserved.
//
//  Windows reference code is licensed under the Microsoft Windows Portable
//  Device Enabling Kit for Media Transfer Protocol (MTP).

#include "MTPUSBFnPrecomp.h"

void MTPUSBTransferStart(PNCUSBPIPE pipe, USBTRANSFERINFO* ti)
{
    PSLSTATUS       ps;

    PSLASSERT(PSLNULL != pipe && PSLNULL != ti);

    if (PSLNULL == pipe->pusbTI)
    {
        /* Immediately start the transfer if the pipe is standing by
          */
        pipe->pusbTI = ti;
        if (( ti->dwTransferFlags & MTPTRANSFERFLAGS_IN_TRANSFER)
            == MTPTRANSFERFLAGS_IN_TRANSFER)
        {
            Nc2280_KickTxXfer(pipe->Ep, ti->pvBuffer, ti->cbBuffer, (0 == ti->cbBuffer));
        }
        else
        {
            PSLASSERT(( ti->dwTransferFlags & MTPTRANSFERFLAGS_OUT_TRANSFER)
                    == MTPTRANSFERFLAGS_OUT_TRANSFER);
            Nc2280_KickRxXfer(pipe->Ep, ti->pvBuffer, ti->cbBuffer);
        }
    }
    else
    {
        PSLASSERT(PSLNULL != pipe->pTransferList);
        if (PSLNULL != pipe->pTransferList)
        {
            /* Enqueue the request otherwise
              */
            ps = PSLDynListAddItem(pipe->pTransferList, (PSLPARAM)ti, PSLNULL);
            PSLASSERT(PSLSUCCESS == ps);
        }
    }
}

void MTPUSBTransferComplete(PNCUSBPIPE pipe)
{
    PSLUINT32       cItems;
    PSLSTATUS       ps;
    USBTRANSFERINFO* pTI = PSLNULL;

    PSLASSERT(PSLNULL != pipe);

    pipe->pusbTI = PSLNULL;

    PSLASSERT(PSLNULL != pipe->pTransferList);

    ps = PSLDynListGetSize(pipe->pTransferList, &cItems);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS == ps && 0 < cItems)
    {
        /* More transfers request pending, this is the time to start them,
          * First Come, First Serve
          */
        ps = PSLDynListGetItem(pipe->pTransferList, 0, (PSLPARAM *)&pTI);
        if (PSLSUCCESS == ps)
        {
            ps = PSLDynListRemoveItem(pipe->pTransferList, 0, 1);
            PSLASSERT(PSLSUCCESS == ps);

            MTPUSBTransferStart(pipe, pTI);
        }
    }
}

void MTPUSBTransferAbort(PNCUSBPIPE pipe, USBTRANSFERINFO* ti)
{
    PSLUINT32   idx;
    PSLSTATUS   ps;

    PSLASSERT(PSLNULL != pipe && PSLNULL != ti);

    if (pipe->pusbTI == ti)
    {
        /* NetChip API to flush FIFO, clear interrupt...
          */
        Nc2280_AbortTransfer(pipe->Ep);

        MTPUSBTransferComplete(pipe);
    }
    else
    {
        PSLASSERT(PSLNULL != pipe->pTransferList);

        ps = PSLDynListFindItem(pipe->pTransferList, 0, (PSLPARAM)ti, PSLNULL, &idx);
        if (PSLSUCCESS == ps)
        {
            /* Dequeue a transfer request if it is pending
              */
            ps = PSLDynListRemoveItem(pipe->pTransferList, idx, 1);
            PSLASSERT(PSLSUCCESS == ps);
        }
    }
}

void MTPUSBTransferInitialize(PNCUSBPIPE pipe, BYTE Ep)
{
    PSLSTATUS   ps;

    PSLASSERT(PSLNULL != pipe);

    pipe->fInitialized = TRUE;
    pipe->Ep = Ep;
    pipe->pusbTI = PSLNULL;

    ps = PSLDynListCreate(0, PSLFALSE, PSLDYNLIST_GROW_DEFAULT,&pipe->pTransferList);
    PSLASSERT(PSLSUCCESS == ps);
}

void MTPUSBTransferUninitialize(PNCUSBPIPE pipe)
{
    /* All transfers are cancelled upon this point
     */
    PSLASSERT(PSLNULL == pipe->pusbTI);

    pipe->fInitialized = FALSE;

    SAFE_PSLDYNLISTDESTROY(pipe->pTransferList);
}
