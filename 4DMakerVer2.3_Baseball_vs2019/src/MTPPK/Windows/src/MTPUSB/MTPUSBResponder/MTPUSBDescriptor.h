/*
 *  MTPUSBDescriptor.h
 *
 *  Contains declarations of the MTP USB decrriptors
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPUSBDECRIPTOR_H_
#define _MTPUSBDECRIPTOR_H_

/*
 *  MTP USB Device Information
 *
 */

#define MTPUSB_VERSION                  0x0200

#define MTPUSB_VID_MICROSOFT            0x045e
#define MTPUSB_PID_MTPPK                0x0622

#define MTPUSB_DEVICE_VERSION           0x0090

/*  Defice specific configuration information
 */

#define MTPUSB_CONFIGURATION_ID         0x01

#define MTPUSB_BULK_IN_ENDPOINT         0x81
#define MTPUSB_BULK_OUT_ENDPOINT        0x02
#define MTPUSB_INTERRUPT_ENDPOINT       0x83

#define MTPUSB_EVENT_PACKET_SIZE        0x08
#define MTPUSB_FS_INTERRUPT_POLL        0x01    /* 1 ms                     */
#define MTPUSB_HS_INTERRUPT_POLL        0x04    /* 1 ms- 2^(n-1) microframes*/

/*
 *  MTP USB High Speed Descriptors
 *
 */

#define MTPUSB_HS_INTERFACE_IMAGE_CLASS_LENGTH \
    (sizeof(USB_INTERFACE_DESCRIPTOR) + (3 * sizeof(USB_ENDPOINT_DESCRIPTOR)))

#define MTPUSB_HS_CONFIGURATION_TOTAL_LENGTH \
    (sizeof(USB_CONFIGURATION_DESCRIPTOR) + \
        MTPUSB_HS_INTERFACE_IMAGE_CLASS_LENGTH)


extern USB_DEVICE_DESCRIPTOR            g_mtpUSBHSDeviceDesc;
extern USB_DEVICE_QUALIFIER_DESCRIPTOR Transfer_DeviceQualifierDescriptor;
extern TRANSFER_CONFIGURATION           Transfer_HS_Configuration;


/*
 *  MTP USB Full Speed Descriptors
 *
 */

#define MTPUSB_FS_INTERFACE_IMAGE_CLASS_LENGTH \
    (sizeof(USB_INTERFACE_DESCRIPTOR) + (3 * sizeof(USB_ENDPOINT_DESCRIPTOR)))

#define MTPUSB_FS_CONFIGURATION_TOTAL_LENGTH \
    (sizeof(USB_CONFIGURATION_DESCRIPTOR) + \
        MTPUSB_FS_INTERFACE_IMAGE_CLASS_LENGTH)

extern TRANSFER_CONFIGURATION           Transfer_FS_Configuration;
/*
 *  MTP Microsoft OS Descriptors
 *
 */

extern USB_MS_OS_STRING_DESCRIPTOR              g_mtpUSBMsftOSStringDesc;
extern MTPUSB_EXTENDED_CONFIGURATION_DESCRIPTOR g_mtpUSBExtendedConfigDesc;

#endif  /* _MTPUSBDECRIPTOR_H_ */
