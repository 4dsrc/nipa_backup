//  Copyright (c) 2009 Microsoft Corporation
//  All rights reserved.
//
//  Windows reference code is licensed under the Microsoft Windows Portable
//  Device Enabling Kit for Media Transfer Protocol (MTP).

#ifndef __MTPUSBTRANSFERNC_INCLUDE_H_
#define __MTPUSBTRANSFERNC_INCLUDE_H_

typedef struct _NCUSBPIPE
{
    BYTE                  Ep;
    PSLDYNLIST            pTransferList;
    USBTRANSFERINFO*      pusbTI;
    PSLBOOL               fInitialized;
} NCUSBPIPE, *PNCUSBPIPE;

void MTPUSBTransferStart(PNCUSBPIPE pipe, USBTRANSFERINFO* ti);
void MTPUSBTransferComplete(PNCUSBPIPE pipe);
void MTPUSBTransferAbort(PNCUSBPIPE pipe, USBTRANSFERINFO* ti);
void MTPUSBTransferInitialize(PNCUSBPIPE pipe, BYTE Ep);
void MTPUSBTransferUninitialize(PNCUSBPIPE pipe);

#endif
