/*
 *  MTPUSBDeviceNC.h
 *
 *  Contains Windows specific USB Device helper functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPUSBDEVICECE_H_
#define _MTPUSBDEVICECE_H_

typedef HANDLE              MTPUSBDEVICECE;

/*
 *  MTPUSBDevicePCCreate
 *
 *  Creates a MTP USB device from rooted at the specified loaction in the
 *  device tree
 *
 */

PSL_EXTERN_C DWORD PSL_API MTPUSBDevicePCCreate(
                            MTPUSBDEVICECE* pusbd);

/*
 *  MTPUSBDevicePCShutdown
 */

PSL_EXTERN_C DWORD PSL_API MTPUSBDevicePCShutdown(
                            MTPUSBDEVICECE usbd);

/*
 *  MTPUSBDevicePCDestroy
 *
 *  Destroys the specified USB device
 *
 */

PSL_EXTERN_C DWORD PSL_API MTPUSBDevicePCDestroy(
                            MTPUSBDEVICECE usbd);


DWORD WINAPI MTPUSBDevicePCTransferNotify(
                            PVOID pvNotifyParam,
                            PSLBYTE Ep,
                            DWORD cbXfered);


/*
 *  Safe Helpers
 */

#define SAFE_MTPUSBDEVICEDESTROY(_d) \
if (PSLNULL != (_d)) \
{ \
    MTPUSBDevicePCDestroy(_d); \
    (_d) = PSLNULL; \
}

#define MTPTRANSFERFLAGS_OUT_TRANSFER       0x00000000
#define MTPTRANSFERFLAGS_IN_TRANSFER        0x00000001
#define MTPTRANSFERFLAGS_DIRECTION_MASK     0x00000001

#define MTPTRANSFERFLAGS_CANCEL_PENDING     0x80000000
#define MTPTRANSFERFLAGS_INTERNAL_TRANSFER  0x40000000

#endif  /* _MTPUSBDEVICECE_H_ */

