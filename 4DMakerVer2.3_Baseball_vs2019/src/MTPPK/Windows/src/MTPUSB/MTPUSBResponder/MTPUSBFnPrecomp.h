/*
 *  MTPUSBFnPrecomp.h
 *
 *  Precompiled header for the Window MTP USB function driver
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPUSBFNPRECOMP_H_
#define _MTPUSBFNPRECOMP_H_

/*  Standard Headers
 */

#define WINDOWSCEDDK
#define WINDOWSUSB

#include "PSLWindows.h"
#include "PSLWinSafe.h"
#include "PSL.h"

/*  Windows USB Function Driver Support
 */

#include "USBStd.h"

/*  MTP USB Device Headers
 */

#include "MTP.h"
#include "MTPUSB.h"
#include "MTPUSBOSDescriptor.h"
#include "MTPUSBTransport.h"
#include "MTPUSBDescriptor.h"
#include "MTPUSBBufferMgr.h"
#include "MTPUSBDeviceNC.h"
#include "MTPUSBTransferNC.h"

#define NET2280_DIRECT_ACCESS TRUE // TRUE: Use direct access (otherwise use function calls)

#include "NC2280.h"
#include "Net2280.h"
#include "NCHal.h"
#include "System.h"

/*  DLL Management
 */

#include "MTPLoader.h"

#endif  /* _MTPUSBFNPRECOMP_H_ */

