/*
 *  MTPUSBDevicePC.c
 *
 *  Implementation of the Windows MTP-USB Device abstraction
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPUSBFnPrecomp.h"
#include "MTPUSBTransport.h"

/*  Local Defines
 */

#define USBDEVICECTX_COOKIE                 'usbC'

#define USBTRANSFERINFO_COOKIE              'usbT'

#define USBDEVICEOBJ_COOKIE                 'usbO'

#define MTPUSBCE_POOL_SIZE                  16

#define MTPUSBDEVICEFLAGS_BUS_ATTACHED      0x00000001
#define MTPUSBDEVICEFLAGS_CONFIGURED        0x00000002

///////////////////////////////////////////////////////////////////////////////
// Strings
#define TRANSFER_ASCII_PRODUCT      "MTP Device Simulator"
#define TRANSFER_ASCII_MANUFACTURER "Microsoft Corporation"
#define TRANSFER_ASCII_SERIALNO     "MSMTPSIM00001"

#define SAFE_MTPUSBDEVICECEADDREF(_ud) \
if (NULL != (_ud)) \
{ \
    _MTPUSBDevicePCAddRef(_ud); \
}

#define SAFE_MTPUSBDEVICECERELEASE(_ud) \
if (NULL != (_ud)) \
{ \
    _MTPUSBDevicePCRelease(_ud); \
    (_ud) = NULL; \
}

#define SAFE_MTPDEVICEPCDELETE(_ud) \
if (NULL != (_ud)) \
{ \
    _MTPUSBDevicePCDeleteUsbObject(_ud); \
}

#define SAFE_MTPUSBDEVICECEADDREFCONTEXT(_uc) \
if (NULL != (_uc)) \
{ \
    _MTPUSBDevicePCAddRefContext(_uc); \
}

#define SAFE_MTPUSBDEVICECERELEASECONTEXT(_uc) \
if (NULL != (_uc)) \
{ \
    _MTPUSBDevicePCReleaseContext(_uc); \
    (_uc) = NULL; \
}

#define SAFE_MTPUSBDEVICECERELEASETRANSFER(_ut) \
if (NULL != (_ut)) \
{ \
    _MTPUSBDevicePCReleaseTransfer(_ut); \
    (_ut) = NULL; \
}

/*  Local Types
 */

/*
 *  PIPE Index
 *
 *  These are ordered to match the pipe definitions in the endpoint
 *  descriptor.
 *
 */
enum
{
    PIPE_BULK_IN = 0,
    PIPE_BULK_OUT,
    PIPE_EVENT,
    PIPE_CONTROL,
    PIPE_MAX_PIPES
};

typedef struct _USBDEVICEOBJ
{
#ifdef PSL_ASSERTS
    DWORD                   dwCookie;
#endif  /* PSL_ASSERTS */
    DWORD                   cRefs;
    PSLHANDLE               hMutex;
    DWORD                   dwFlags;
    DWORD                   cbAlignment;
    DWORD                   dwBusSpeed;
    DWORD                   dwTIAvailable;
    USBTRANSFERINFO         rgTransferInfo[MTPUSB_TOTAL_BUFFERS];
    USBDEVICECTX*           pusbdCtx;
    NCUSBPIPE               pipes[PIPE_MAX_PIPES];
} USBDEVICEOBJ;

/*  Local Functions
 */
static PSLUINT32 _MTPReleaseThread(
                            PSLVOID* pvParam);

static DWORD _MTPUSBDevicePCAddRef(
                            USBDEVICEOBJ* pusbdObj);

static DWORD _MTPUSBDevicePCRelease(
                            USBDEVICEOBJ* pusbdObj);

static DWORD _MTPUSBDevicePCCreateContext(
                            USBDEVICEOBJ* pusbdObj,
                            USBDEVICECTX** ppusbdCtx);

static DWORD _MTPUSBDevicePCOpenContext(
                            USBDEVICEOBJ* pusbdObj,
                            USBDEVICECTX* pusbdCtx);

static DWORD _MTPUSBDevicePCAddRefContext(
                            USBDEVICECTX* pusbdCtx);

static DWORD _MTPUSBDevicePCReleaseContext(
                            USBDEVICECTX* pusbdCtx);

static BOOL _MTPUSBDevicePCHandleBusEvent(
                            USBDEVICEOBJ* pusbdObj,
                            PSLUINT32 dwUFNBusEvent);

static BOOL _MTPUSBDevicePCHandleConfigEvent(
                            USBDEVICEOBJ* pusbdObj,
                            PSLUINT32 dwConfigParam);

static BOOL _MTPUSBDevicePCHandleSpeedEvent(
                            USBDEVICEOBJ* pusbdObj,
                            PSLUINT32 dwUFNSpeed);

static BOOL _MTPUSBDevicePCHandleStandardRequest(
                            USBDEVICEOBJ* pusbdObj,
                            PSL_CONST USB_SETUP_PACKET* pdr);

static BOOL _MTPUSBDevicePCHandleVendorRequest(
                            USBDEVICEOBJ* pusbdObj,
                            PSL_CONST USB_SETUP_PACKET* pdr);

static BOOL _MTPUSBDevicePCHandleClassRequest(
                            USBDEVICEOBJ* pusbdObj,
                            PSL_CONST USB_SETUP_PACKET* pdr);

static BOOL _MTPUSBDevicePCHandleSetupEvent(
                            USBDEVICEOBJ* pusbdObj,
                            PSLUINT32 dwEvent,
                            PSL_CONST USB_SETUP_PACKET* pdr);

static PSLSTATUS _MTPUSBDevicePCRequestTransfer(
                            USBDEVICEOBJ* pusbdObj,
                            PSLUINT32* pidxTransfer);

static PSLSTATUS _MTPUSBDevicePCReleaseTransfer(
                            USBTRANSFERINFO* pusbdTI);

static BOOL WINAPI _MTPUSBDevicePCNotify(
                            PVOID pvNotifyParameter,
                            DWORD dwMsg,
                            DWORD dwParam);

static DWORD _MTPUSBDevicePCFailControlRequest(
                            USBDEVICEOBJ* pusbdObj);

static DWORD _MTPUSBDevicePCBufferToPipe(
                            PSLUINT32 dwBufferType,
                            PSLBOOL fResponder);

static DWORD _MTPUSBDevicePCStartTransfer(
                            USBDEVICEOBJ* pusbdObj,
                            DWORD dwPipe,
                            DWORD dwTransferFlags,
                            PSL_CONST PVOID pvBuffer,
                            DWORD cbBuffer,
                            PSLPARAM aParam,
                            PVOID* phTransfer);

static DWORD _MTPUSBDevicePCCancelTransfer(USBDEVICEOBJ* pusbdObj,
                            UFN_TRANSFER hTransfer,
                            DWORD dwCancelTimeout);

static DWORD _MTPUSBDevicePCReset(USBDEVICEOBJ* pusbdObj);

/*  Local Variables
 */

static const WCHAR          _RgchMTPInitMutex[] = L"USBDevice MTP Init Mutex";

static DWORD                _CMTPRefs = 0;

static PSLHANDLE           _HReleaseThread = PSLNULL;

static PSLHANDLE           _HSignalRelease = PSLNULL;

/*
 *  _MTPReleaseThread
 *
 *  This thread is used to safely release the MTP DLL while allowing
 *  control to return to the calling routine.  It also introduces a
 *  delay so that we do not pull the DLL out of memory while someone
 *  is still using it
 *
 *  Arguments:
 *      PSLVOID*        pvParam             Ignored
 *
 *  Returns:
 *      PSLUINT32       Always 0
 *
 */

PSLUINT32 _MTPReleaseThread(PSLVOID* pvParam)
{
    PSLHANDLE       hMutexMTP;
    PSLSTATUS       ps;

    PSLASSERT(_HSignalRelease == PSLNULL);
    ps = PSLSignalOpen(PSLFALSE, PSLTRUE, PSLNULL, &_HSignalRelease);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    PSLSignalReset(_HSignalRelease);

    /*  Wait for release MTP signal for up to 20 seconds
      * to make sure we do not pull MTP out from under anything
      */
    ps = PSLSignalWait(_HSignalRelease, 20000);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Acquire the MTP initialization mutex so that we can release MTP if
     *  necessary
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchMTPInitMutex, &hMutexMTP);
    if (PSL_SUCCEEDED(ps))
    {
        /*  Close MTP if necessary
         */

        PSLASSERT(0 < _CMTPRefs);
        _CMTPRefs--;
        if (0 == _CMTPRefs)
        {
            if (PSL_SUCCEEDED(MTPUninitialize()))
            {
                MTPUnload();
            }
        }
        SAFE_PSLMUTEXRELEASECLOSE(hMutexMTP);
    }

Exit:
    PSLTraceProgress("Release thread exits");
    SAFE_PSLTHREADCLOSE(_HReleaseThread);
    SAFE_PSLSIGNALCLOSE(_HSignalRelease);

    return 0;
    pvParam;
}

/*
 *  _MTPUSBDevicePCGetDeviceInfo
 *
 *  Returns the current device descriptor and endpoint list based on the
 *  state of the device
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Device to query
 *      PSL_CONST USB_DEVICE_DESCRIPTOR**
 *                      ppDeviceDesc        USB device descriptor
 *      PSL_CONST UFN_ENDPOINT**
 *                      ppEndpointList      Endpoint list
 *
 *  Returns:
 *      BOOL            TRUE on success
 *
 */

BOOL _MTPUSBDevicePCGetDeviceInfo(USBDEVICEOBJ* pusbdObj,
                            PSL_CONST USB_DEVICE_DESCRIPTOR** ppDeviceDesc,
                            PSL_CONST USB_ENDPOINT_DESCRIPTOR** ppEndpointList)
{
    BOOL            fResult;

    /*  Clear result for safety
     */

    if (PSLNULL != ppDeviceDesc)
    {
        *ppDeviceDesc = PSLNULL;
    }
    if (PSLNULL != ppEndpointList)
    {
        *ppEndpointList = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pusbdObj) ||
        ((PSLNULL == ppDeviceDesc) && (PSLNULL == ppEndpointList)))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Return the descriptors based on the current speed
     */

    if (PSLNULL != ppDeviceDesc)
    {
        *ppDeviceDesc = &g_mtpUSBHSDeviceDesc;
    }

    if (PSLNULL != ppEndpointList)
    {
        *ppEndpointList = (BS_HIGH_SPEED == pusbdObj->dwBusSpeed) ?
            Transfer_HS_Configuration.EndpointDescriptor :
            Transfer_FS_Configuration.EndpointDescriptor;
    }

    /*  Report success
     */

    fResult = TRUE;

Exit:
    return fResult;
}

/*
 *  _MTPUSBDevicePCAddRef
 *
 *  AddRef's a USB device
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Device to AddRef
 *
 *  Returns:
 *      DWORD           ERROR_SUCCESS on success
 *
 */

DWORD _MTPUSBDevicePCAddRef(USBDEVICEOBJ* pusbdObj)
{
    DWORD           dwResult;
    PSLSTATUS       ps;
    PSLHANDLE       hMutexRelease = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == pusbdObj)
    {
        dwResult = ERROR_SUCCESS;
        goto Exit;
    }

    /*  Acquire the mutex if necessary
     */

    PSLASSERT(PSLNULL != pusbdObj->hMutex);
    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }
    hMutexRelease = pusbdObj->hMutex;

    /*  Make sure this is one of ours
     */

    PSLASSERT(USBDEVICEOBJ_COOKIE == pusbdObj->dwCookie);

    /*  Increment the reference count
     */

    PSLASSERT(0 < pusbdObj->cRefs);
    pusbdObj->cRefs++;

    /*  Report success
     */

    dwResult = ERROR_SUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return dwResult;
}

/*
 *  _MTPUSBDevicePCDeleteUsbObject
 *
 *  Destroys a USB device
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Device to Release
 *
 *  Returns:
 *      DWORD           ERROR_SUCCESS on success
 *
 */

DWORD _MTPUSBDevicePCDeleteUsbObject(USBDEVICEOBJ* pusbdObj)
{
    if (0 == pusbdObj->cRefs)
    {
        PSLTraceProgress( "Delete USBD object");
        SAFE_PSLMUTEXCLOSE(pusbdObj->hMutex);
        /*  Release the memory associated with the context
         */
        PSLMemFree(pusbdObj);
    }

    return ERROR_SUCCESS;
}

/*
 *  _MTPUSBDevicePCRelease
 *
 *  Release a USB device
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Device to Release
 *
 *  Returns:
 *      DWORD           ERROR_SUCCESS on success
 *
 */

DWORD _MTPUSBDevicePCRelease(USBDEVICEOBJ* pusbdObj)
{
    DWORD           dwResult;
    PSLBOOL         fDestroyObject = PSLFALSE;
    PSLSTATUS       ps;
    PSLHANDLE       hMutexRelease = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == pusbdObj)
    {
        dwResult = ERROR_SUCCESS;
        goto Exit;
    }

    /*  Make sure this is one of ours
     */

    PSLASSERT(USBDEVICEOBJ_COOKIE == pusbdObj->dwCookie);

    /*  Acquire the mutex if necessary
     */

    if (PSLNULL != pusbdObj->hMutex)
    {
        ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            dwResult = ERROR_NOT_READY;
            goto Exit;
        }
        hMutexRelease = pusbdObj->hMutex;
    }

    /*  Decrement the reference count
     */

    PSLASSERT(0 < pusbdObj->cRefs);
    pusbdObj->cRefs--;

    /* And determine whether or not we need to destroy the object
     */

    fDestroyObject = (0 == pusbdObj->cRefs);

    /*  Report success
     */

    dwResult = ERROR_SUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    if (fDestroyObject)
    {
        /* Delete the usb object if ite ref counter reaches 0
          */
        SAFE_MTPDEVICEPCDELETE(pusbdObj);
    }
    return dwResult;
}


/*
 *  _MTPUSBDevicePCCreateContext
 *
 *  Creates a USB context to associate with the current active USB
 *  communication session.  Note that the context is not assigned as the
 *  active context by this operation- only created.
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Device object to use
 *      USBDEVICECTX**  ppusbdCtx           Buffer for new context
 *
 *  Returns:
 *      DWORD           ERROR_SUCCESS on success
 *
 */

DWORD _MTPUSBDevicePCCreateContext(USBDEVICEOBJ* pusbdObj,
                            USBDEVICECTX** ppusbdCtx)
{
    DWORD           dwResult;
    USBDEVICECTX*   pusbdCtx = PSLNULL;

    /*  Clear results for safety
     */

    if (PSLNULL != ppusbdCtx)
    {
        *ppusbdCtx = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pusbdObj) || (PSLNULL == ppusbdCtx))
    {
        dwResult = ERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate a new device context to publish externally
     */

    pusbdCtx = (USBDEVICECTX*)PSLMemAlloc(PSLMEM_PTR,
                            sizeof(*pusbdCtx));
    if (PSLNULL == pusbdCtx)
    {
        dwResult = ERROR_OUTOFMEMORY;
        goto Exit;
    }

    /*  Initialize the context with required information
     */

#ifdef PSL_ASSERTS
    pusbdCtx->dwCookie = USBDEVICECTX_COOKIE;
#endif  /* PSL_ASSERTS */
    pusbdCtx->cRefs = 1;

    /*  Duplicate the mutex handle into the context so that we can
     *  lock the context and device at the same time
     */

    if (!DuplicateHandle(GetCurrentProcess(), pusbdObj->hMutex,
                            GetCurrentProcess(), &(pusbdCtx->hMutex),
                            DUPLICATE_SAME_ACCESS, FALSE,
                            DUPLICATE_SAME_ACCESS))
    {
        dwResult = GetLastError();
        goto Exit;
    }

    /*  Return the new context
     */

    *ppusbdCtx = pusbdCtx;
    pusbdCtx = PSLNULL;

    /*  Report success
     */

    dwResult = ERROR_SUCCESS;

Exit:
    SAFE_MTPUSBDEVICECERELEASECONTEXT(pusbdCtx);
    return dwResult;
}


/*
 *  _MTPUSBDevicePCOpenContext
 *
 *  Opens the context specified
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            USB device context lives in
 *      USBDEVICECTX*   pusbdCtx            Context to update
 *
 *  Returns:
 *      DWORD           ERROR_SUCCESS on success
 *
 */

DWORD _MTPUSBDevicePCOpenContext(USBDEVICEOBJ* pusbdObj,
                    USBDEVICECTX* pusbdCtx)
{
    DWORD                               dwResult;
    PSLHANDLE                           hMutexRelease = PSLNULL;
    PSLSTATUS                           ps;
    PSL_CONST USB_ENDPOINT_DESCRIPTOR*  pufnepList;
    PSL_CONST USB_DEVICE_DESCRIPTOR*    pusbdDesc;

    /*  Validate arguments
     */

    if ((NULL == pusbdObj) || (NULL == pusbdCtx))
    {
        dwResult = ERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure it is our object
     */

    PSLASSERT(USBDEVICEOBJ_COOKIE == pusbdObj->dwCookie);
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we do not get interrupted during
     *  configuration
     */

    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }
    hMutexRelease = pusbdObj->hMutex;

    /*  Get the correct endpoint list based on the current speed for the
     *  device
     */

    if (!_MTPUSBDevicePCGetDeviceInfo(pusbdObj, &pusbdDesc, &pufnepList))
    {
        dwResult = ERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create the appropriate sized buffers for this connection
     */

    PSLASSERT(EP_MAXPACKETSIZE(pufnepList[0]) ==
                      EP_MAXPACKETSIZE(pufnepList[1]));
    ps = MTPUSBBufferMgrCreate(pusbdObj->cbAlignment,
                    pusbdDesc->bMaxPacketSize0,
                    EP_MAXPACKETSIZE(pufnepList[0]),
                    EP_MAXPACKETSIZE(pufnepList[2]),
                    &(pusbdCtx->mtpusbbm));
    if (PSL_FAILED(ps))
    {
        dwResult = ERROR_OUTOFMEMORY;
        goto Exit;
    }

    /*  Let the context now that it is configured by assigning the back pointer
     */

    pusbdCtx->pvRoot = pusbdObj;
    SAFE_MTPUSBDEVICECEADDREF(pusbdCtx->pvRoot);

    /*  Report success
     */

    dwResult = ERROR_SUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return dwResult;
}


/*
 *  _MTPUSBDevicePCAddRefContext
 *
 *  AddRef's a USB device context
 *
 *  Arguments:
 *      USBDEVICECTX*   pusbdCtx            Device context to AddRef
 *
 *  Returns:
 *      DWORD           ERROR_SUCCESS on success
 *
 */

DWORD _MTPUSBDevicePCAddRefContext(USBDEVICECTX* pusbdCtx)
{
    DWORD           dwResult;
    PSLSTATUS       ps;
    PSLHANDLE       hMutexRelease = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == pusbdCtx)
    {
        dwResult = ERROR_SUCCESS;
        goto Exit;
    }

    /*  Acquire the mutex if necessary
     */

    PSLASSERT(PSLNULL != pusbdCtx->hMutex);
    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  Make sure this is one of ours
     */

    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Increment the reference count
     */

    PSLASSERT(0 < pusbdCtx->cRefs);
    pusbdCtx->cRefs++;

    /*  Report success
     */

    dwResult = ERROR_SUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return dwResult;
}


/*
 *  _MTPUSBDevicePCReleaseContext
 *
 *  Destroys a USB device context
 *
 *  Arguments:
 *      USBDEVICECTX*   pusbdCtx            Device context to Release
 *
 *  Returns:
 *      DWORD           ERROR_SUCCESS on success
 *
 */

DWORD _MTPUSBDevicePCReleaseContext(USBDEVICECTX* pusbdCtx)
{
    DWORD           dwResult;
    PSLHANDLE       hMutexMTP = PSLNULL;
    PSLHANDLE       hMutexClose = PSLNULL;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLHANDLE       hThreadMTP = PSLNULL;
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if (PSLNULL == pusbdCtx)
    {
        dwResult = ERROR_SUCCESS;
        goto Exit;
    }

    /*  Make sure this is one of ours
     */

    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex if necessary
     */

    if (PSLNULL != pusbdCtx->hMutex)
    {
        ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            dwResult = ERROR_NOT_READY;
            goto Exit;
        }
        hMutexRelease = pusbdCtx->hMutex;
    }

    /*  Decrement the reference count
     */

    PSLASSERT(0 < pusbdCtx->cRefs);
    pusbdCtx->cRefs--;
    if (0 < pusbdCtx->cRefs)
    {
        dwResult = ERROR_SUCCESS;
        goto Exit;
    }

    /*  Make sure that this object is no longer bound to the device and that
     *  it is no longer bound to the transport
     */

    PSLASSERT(PSLNULL == pusbdCtx->mqEvent);
    PSLASSERT(PSLNULL == pusbdCtx->pvRoot);
    if ((PSLNULL != pusbdCtx->mqEvent) || (PSLNULL != pusbdCtx->pvRoot))
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }

    /*  Release the buffers and the message pool
     */

    SAFE_MTPUSBBUFFERMGRDESTROY(pusbdCtx->mtpusbbm);
    SAFE_MTPMSGPOOLDESTROY(pusbdCtx->mpEvent);

    /*  Acquire the MTP initialization mutex so that we can release MTP if
     *  necessary
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchMTPInitMutex, &hMutexMTP);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }

    /*  Close MTP if necessary
     */

    if (pusbdCtx->fMTPInit)
    {
        if (1 == _CMTPRefs)
        {
            /*  We want to deal this release rather than pulling the DLL
             *  out immediately
             */

            ps = PSLThreadCreate(_MTPReleaseThread, PSLNULL, PSLFALSE,
                                PSLTHREAD_DEFAULT_STACK_SIZE, &hThreadMTP);
            if (PSL_FAILED(ps))
            {
                dwResult = ERROR_NOT_READY;
                goto Exit;
            }
            _HReleaseThread = hThreadMTP;
            hThreadMTP = PSLNULL;
        }
        else
        {
            PSLASSERT(0 < _CMTPRefs);
            _CMTPRefs--;
        }
    }
    SAFE_PSLMUTEXRELEASECLOSE(hMutexMTP);

    /*  And release the memory associated with the context
     */

    PSLMemFree(pusbdCtx);

    /*  Close the mutex in addition to releasing it
     */

    hMutexClose = hMutexRelease;

    /*  Report success
     */

    dwResult = ERROR_SUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexMTP);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMUTEXCLOSE(hMutexClose);
    SAFE_PSLTHREADCLOSE(hThreadMTP);
    return dwResult;
}


BOOL _MTPUSBDevicePCHandleBusEvent(USBDEVICEOBJ* pusbdObj,
                            PSLUINT32 dwUFNBusEvent)
{
    DWORD           dwResult;
    BOOL            fResult;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLMSGID        msgID = MTPMSG_UNKNOWN;
    USBDEVICECTX*   pusbdCtx = PSLNULL;
    USBDEVICECTX*   pusbdCtxMsg = PSLNULL;
    PSLMSG*         pMsg = PSLNULL;
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if (PSLNULL == pusbdObj)
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Make sure the object is updated syncronously
     */

    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        fResult = FALSE;
        goto Exit;
    }
    hMutexRelease = pusbdObj->hMutex;

    /*  Determine which event occurred
     */

    switch (dwUFNBusEvent)
    {
    case UFN_DETACH:
        /*  If we are already detached then there is no work to do
         */

        if (!(MTPUSBDEVICEFLAGS_BUS_ATTACHED & pusbdObj->dwFlags))
        {
            fResult = FALSE;
            goto Exit;
        }

        /*  Clear the bus attached flags
         */
        PSLASSERT(MTPUSBDEVICEFLAGS_BUS_ATTACHED & pusbdObj->dwFlags);
        pusbdObj->dwFlags &= ~MTPUSBDEVICEFLAGS_BUS_ATTACHED;

        /*  Provide some feedback
         */

        PSLTraceProgress("**MTPUSBFn- USB Bus Event: DETACH");

        /*  FALL THROUGH INTENTIONAL */

    case UFN_RESET:
        /*  Detach the context from the device and determine which message
         *  to send
         */

        PSLASSERT((UFN_RESET == dwUFNBusEvent) ||
                            (UFN_DETACH == dwUFNBusEvent));
        pusbdCtxMsg = pusbdObj->pusbdCtx;
        pusbdObj->pusbdCtx = PSLNULL;
        msgID = (UFN_RESET == dwUFNBusEvent) ?
                        MTPUSBMSG_DEVICE_RESET : MTPUSBMSG_DEVICE_DISCONNECT;

        /*  Reset the device
         */

        dwResult = _MTPUSBDevicePCReset(pusbdObj);
        if (ERROR_SUCCESS != dwResult)
        {
            fResult = FALSE;
            goto Exit;
        }

        /*  If we are handling a detach event we are done
         */

        if (UFN_DETACH == dwUFNBusEvent)
        {
            fResult = TRUE;
            break;
        }

        /*  Provide some feedback
         */

        PSLASSERT(UFN_RESET == dwUFNBusEvent);
        PSLTraceProgress("**MTPUSBFn- USB Bus Event: RESET");

        /*  On reset we want to create a new context for the next connection
         *  the same as if the device had just been connected
         *
         *  FALL THROUGH INTENTIONAL
         */
    case UFN_ATTACH:
        /*  If the bus is already attached or we already have an active
         *  context then we should not create a new one
         */

        if ((MTPUSBDEVICEFLAGS_BUS_ATTACHED & pusbdObj->dwFlags) &&
            (PSLNULL != pusbdObj->pusbdCtx))
        {
            /*  Rather than trusting that everything is happy treat this
             *  additional attach as a reset so that we bind to a new
             *  context
             */

            fResult = _MTPUSBDevicePCHandleBusEvent(pusbdObj, UFN_RESET);
            goto Exit;
        }

        /*  Create a new context for this connection
         */

        dwResult = _MTPUSBDevicePCCreateContext(pusbdObj, &pusbdCtx);
        if (ERROR_SUCCESS != dwResult)
        {
            fResult = FALSE;
            goto Exit;
        }

        /*  And assign it as the current connection
         */

        pusbdObj->pusbdCtx = pusbdCtx;
        SAFE_MTPUSBDEVICECEADDREFCONTEXT(pusbdObj->pusbdCtx);

        /*  Set the bus attached flag
         */

        PSLASSERT(!(MTPUSBDEVICEFLAGS_BUS_ATTACHED & pusbdObj->dwFlags) ||
                            (UFN_RESET == dwUFNBusEvent));
        pusbdObj->dwFlags |= MTPUSBDEVICEFLAGS_BUS_ATTACHED;
        fResult = TRUE;

        /*  Provide some feedback
         */

        if (UFN_ATTACH == dwUFNBusEvent)
        {
            PSLTraceProgress("**MTPUSBFn- USB Bus Event: ATTACH");
        }
        break;

    case UFN_SUSPEND:
        fResult = FALSE;
        PSLTraceError("**MTPUSBFn- Unsupported USB Bus Event: SUSPEND");
        break;

    case UFN_RESUME:
        fResult = FALSE;
        PSLTraceError("**MTPUSBFn- Unsupported USB Bus Event: RESUME");
        break;

    default:
        fResult = FALSE;
        PSLTraceError("**MTPUSBFn- Unsupported USB Bus Event");
        break;
    }

    /*  See if we need to forward a message to complete handling of this
     *  item
     */

    if ((MTPMSG_UNKNOWN != msgID) && (PSLNULL != pusbdCtxMsg) &&
        (PSLNULL != pusbdCtxMsg->mqEvent))
    {
        /*  Allocate a message to send
         */

        fResult = PSL_SUCCEEDED(MTPMsgAlloc(pusbdCtxMsg->mpEvent, &pMsg));
        if (!fResult)
        {
            goto Exit;
        }

        pMsg->msgID = msgID;
        fResult = PSL_SUCCEEDED(MTPMsgPost(pusbdCtxMsg->mqEvent, pMsg));
        if (!fResult)
        {
            goto Exit;
        }
        pMsg = PSLNULL;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_MTPUSBDEVICECERELEASECONTEXT(pusbdCtx);
    SAFE_MTPUSBDEVICECERELEASECONTEXT(pusbdCtxMsg);
    SAFE_PSLMSGFREE(pMsg);
    return fResult;
}


/*
 *  _MTPUSBDevicePCHandleConfigEvent
 *
 *  Responds to a configuration event
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Device triggering event
 *      PSLUINT32       dwConfigParam       wParam from USB_SET_CONFIGURATION
 *                                            message
 *
 *  Returns:
 *      BOOL            TRUE if handled
 *
 */

BOOL _MTPUSBDevicePCHandleConfigEvent(
                            USBDEVICEOBJ* pusbdObj,
                            PSLUINT32 dwConfigParam)
{
    DWORD           dwResult;
    BOOL            fResult;
    PSLHANDLE       hMutexMTP = PSLNULL;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    USBDEVICECTX*   pusbdCtx;

    /*  Validate arguments
     */

    if (NULL == pusbdObj)
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  We only care about the MTP configuration- all others are extra
     */

    if (MTPUSB_CONFIGURATION_ID != LOBYTE(dwConfigParam))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Acquire the mutex so that we do not get interrupted during
     *  configuration
     */

    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        fResult = FALSE;
        goto Exit;
    }
    hMutexRelease = pusbdObj->hMutex;

    /*  If we are already configured then do not accept this change
     */

    PSLASSERT(!(MTPUSBDEVICEFLAGS_CONFIGURED & pusbdObj->dwFlags));
    if (MTPUSBDEVICEFLAGS_CONFIGURED & pusbdObj->dwFlags)
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Extract the context so that we can use it later when we have
     *  released the mutex
     */

    pusbdCtx = pusbdObj->pusbdCtx;

    /*  We should have an active context as this point
     */

    PSLASSERT(PSLNULL != pusbdCtx);
    if (PSLNULL == pusbdCtx)
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Open the context
     */

    dwResult = _MTPUSBDevicePCOpenContext(pusbdObj, pusbdCtx);
    if (ERROR_SUCCESS != dwResult)
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Mark the device as configured
     */

    pusbdObj->dwFlags |= MTPUSBDEVICEFLAGS_CONFIGURED;

    /*  If the context is already bound then do not attempt to bind
     *  it again
     */

    if (PSLNULL != pusbdCtx->mqEvent)
    {
        fResult = TRUE;
        goto Exit;
    }

    /*  Release the mutex
     */

    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    /*  Make sure that we only initialize one device at a time
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchMTPInitMutex, &hMutexMTP);
    if (PSL_FAILED(ps))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Initialize MTP if necessary
     */

    if (0 == _CMTPRefs)
    {
        ps = MTPInitialize(MTPINITFLAG_RESPONDER | MTPINITFLAG_USB_TRANSPORT);
        if (PSL_FAILED(ps))
        {
            fResult = FALSE;
            goto Exit;
        }
    }
    _CMTPRefs++;
    pusbdCtx->fMTPInit = TRUE;
    if ( PSLNULL != _HSignalRelease)
    {
        PSLSignalSet(_HSignalRelease);
    }

    SAFE_PSLMUTEXRELEASECLOSE(hMutexMTP);

    /*  Context is open- go ahead and bind the initiator to MTP
     */

    ps = MTPUSBBindInitiator((MTPUSBDEVICE)pusbdCtx);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Provide some information
     */

    PSLTraceProgress("**MTPUSBFn- USB Configured: wValue = 0x%04x",
                            dwConfigParam);

    /*  Report that we handled the configuration request
     */

    fResult = TRUE;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexMTP);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return fResult;
}


/*
 *  _MTPUSBDevicePCHandleSpeedEvent
 *
 *  Responds to a notification of bus speed
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Device triggering event
 *      PSLUINT32       dwUFNSpeed          Speed determined
 *
 *  Returns:
 *      BOOL            TRUE if handled
 *
 */

BOOL _MTPUSBDevicePCHandleSpeedEvent(
                            USBDEVICEOBJ* pusbdObj,
                            PSLUINT32 dwUFNSpeed)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    BOOL            fResult;
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if (PSLNULL == pusbdObj)
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Validate speed and provide some feedback
     */

    switch (dwUFNSpeed)
    {
    case BS_FULL_SPEED:
        PSLTraceProgress("**MTPUSBFn- USB Bus Speed: FULL");
        break;

    case BS_HIGH_SPEED:
        PSLTraceProgress("**MTPUSBFn- USB Bus Speed: HIGH");
        break;

    case BS_UNKNOWN_SPEED:
        PSLTraceProgress("**MTPUSBFn- USB Bus Speed: UNKNOWN");
        break;

    default:
        /*  Unexpected speed specified
         */

        PSLASSERT(PSLFALSE);
        PSLTraceError("**MTPUSBFn- Unknown USB Bus Speed");
        fResult = FALSE;
        goto Exit;
    }

    /*  Acquire the mutex so information update is serialized
     */

    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        fResult = FALSE;
        goto Exit;
    }
    hMutexRelease = pusbdObj->hMutex;

    /*  Store the current bus speed in the device
     */

    pusbdObj->dwBusSpeed = dwUFNSpeed;

    /*  Report success
     */

    fResult = TRUE;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return fResult;
}


/*
 *  _MTPUSBDevicePCHandleStandardRequest
 *
 *  Handles a device request directed to the device
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            USB device being serviced
 *      PSL_CONST USB_SETUP_PACKET*
 *                      pdr                 Device request received
 *
 *  Returns:
 *      BOOL            TRUE if handled
 *
 */

BOOL _MTPUSBDevicePCHandleStandardRequest(
                            USBDEVICEOBJ* pusbdObj,
                            PSL_CONST USB_SETUP_PACKET* pdr)
{
    BYTE            bType;
    BYTE            bIndex;
    BOOL            fResult;
    BOOL            fHandled;

    /*  Validate arguments
     */

    if ((PSLNULL == pusbdObj) || (PSLNULL == pdr))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Make sure this is intended for us- note that
     */

    PSLASSERT(0 == ((CLASS | VENDOR) &
                            pdr->bmRequestType));
    if (0 != ((CLASS | VENDOR) & pdr->bmRequestType))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Look for requests we support
     */

    switch (pdr->bRequest)
    {
    case GET_DESCRIPTOR:
        /*  We only care if this is being requested by the host
         */

        if ((DEVICE_TO_HOST | RECIPIENT_DEVICE) !=
                ((DEVICE_TO_HOST | RECIPIENT_DEVICE) &
                            pdr->bmRequestType))
        {
            fHandled = FALSE;
            break;
        }

        /*  Extract the descriptor and the index
         */

        bType = HIBYTE(pdr->wValue);
        bIndex = LOBYTE(pdr->wValue);

        /*  Look for descriptors we handle
         */

        switch (bType)
        {
        case STRING_DESC:
            /*  And look for the desired index
             */

            switch (bIndex)
            {
            case USB_MS_OS_STRING_INDEX:
                /*  Make sure we have something of the correct length
                 */

                PSLASSERT(sizeof(USB_MS_OS_STRING_DESCRIPTOR) == pdr->wLength);

                /* And send the result
                 */

                _MTPUSBDevicePCStartTransfer(pusbdObj, PIPE_CONTROL,
                        ( MTPTRANSFERFLAGS_IN_TRANSFER |
                          MTPTRANSFERFLAGS_INTERNAL_TRANSFER),
                        &g_mtpUSBMsftOSStringDesc,
                        min(sizeof(USB_MS_OS_STRING_DESCRIPTOR), pdr->wLength),
                        0, PSLNULL);

                /*  Remember that we handled the request
                 */

                fHandled = TRUE;
                break;

            default:
                /*  Did not handle the request
                 */

                fHandled = FALSE;
                break;
            }
            break;

        default:
            /*  Did not handle the request
             */

            fHandled = FALSE;
            break;
        }
        break;

    default:
        /*  Did not handle the request
         */

        fHandled = FALSE;
        break;
    }

    /*  If we did not handle the request we need to stall the control pipe
     *  and then we need to stall the default pipe and send a control status
     *  handshake
     */

/*
    if (!fHandled)
    {
        DWORD           dwResult;

        dwResult = _MTPUSBDevicePCFailControlRequest(pusbdObj);
        PSLASSERT(ERROR_SUCCESS == dwResult);
    }
*/

    /*  Report success
     */

    fResult = fHandled;

Exit:
    return fResult;
}


/*
 *  _MTPUSBDevicePCHandleVendorRequest
 *
 *  Handles a device request directed to the vendor
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            USB device being serviced
 *      PSL_CONST USB_SETUP_PACKET*
 *                      pdr                 Device request recieved
 *
 *  Returns:
 *      BOOL            TRUE if handled
 *
 */

BOOL _MTPUSBDevicePCHandleVendorRequest(
                            USBDEVICEOBJ* pusbdObj,
                            PSL_CONST USB_SETUP_PACKET* pdr)
{
    BYTE            bInterface;
    BYTE            bPage;
    BOOL            fResult;
    BOOL            fHandled;

    /*  Validate arguments
     */

    if ((PSLNULL == pusbdObj) || (PSLNULL == pdr))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Make sure this is intended for us
     */

    PSLASSERT(VENDOR & pdr->bmRequestType);
    if (!(VENDOR & pdr->bmRequestType))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Determine the request
     */

    switch (pdr->bRequest)
    {
    case MTPUSB_VENDOR_EXTENSION_CODE:
        /*  We only care if this is being requested by the host
         */

        if ((DEVICE_TO_HOST | RECIPIENT_DEVICE) !=
                ((DEVICE_TO_HOST | RECIPIENT_DEVICE) &
                            pdr->bmRequestType))
        {
            fHandled = FALSE;
            break;
        }

        /*  Extract interface and page
         */

        bInterface = HIBYTE(pdr->wValue);
        bPage = LOBYTE(pdr->wValue);

        /*  Make sure we are retrieving the correct entry
         */

        if ((0 == bInterface) && (0 == bPage) &&
            (USB_MS_EXTENDED_CONFIGURATION_DESCRIPTOR_TYPE == pdr->wIndex))
        {
            /*  And send the descriptor
             *
             *  NOTE:
             *  We will typically be asked twice for the descriptor- once to
             *  get the 16 byte header portion and then again to get the full
             *  descriptor.
             */

            _MTPUSBDevicePCStartTransfer(pusbdObj, PIPE_CONTROL,
                        (MTPTRANSFERFLAGS_IN_TRANSFER |
                            MTPTRANSFERFLAGS_INTERNAL_TRANSFER),
                        &g_mtpUSBExtendedConfigDesc,
                        min(sizeof(MTPUSB_EXTENDED_CONFIGURATION_DESCRIPTOR),
                                pdr->wLength), 0, PSLNULL);

            /* Report that we handled the descriptor
             */

            fHandled = TRUE;
        }
        else
        {
            fHandled = FALSE;
        }
        break;

    default:
        fHandled = FALSE;
        break;
    }

    /*  If we did not handle the request we need to stall the control pipe
     *  and then we need to stall the default pipe and send a control status
     *  handshake

    if (!fHandled)
    {
        DWORD           dwResult;

        dwResult = _MTPUSBDevicePCFailControlRequest(pusbdObj);
        PSLASSERT(ERROR_SUCCESS == dwResult);
    }

    /*  Report success
     */

    fResult = fHandled;

Exit:
    return fResult;
}


/*
 *  _MTPUSBDevicePCHandleClassRequest
 *
 *  Handles a class request
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            USB device to be services
 *      PSL_CONST USB_SETUP_PACKET*
 *                      pdr                 Device request received
 *
 *  Returns:
 *      BOOL            TRUE if handled
 *
 */

BOOL _MTPUSBDevicePCHandleClassRequest(
                            USBDEVICEOBJ* pusbdObj,
                            PSL_CONST USB_SETUP_PACKET* pdr)
{
    PSLPARAM                            aParamClassReq;
    PSLUINT32                           cbActual;
    BOOL                                fResult;
    HANDLE                              hMutexRelease = PSLNULL;
    PSLSTATUS                           ps;
    PXMTPUSB_DEVICE_REQUEST             pmtpusbdr = PSLNULL;
    USBDEVICECTX*                       pusbdCtx;
    PFNMTPUSBDEVICEHANDLECLASSREQUEST   pfnHandleClassReq;

    /*  Validate arguments
     */

    if ((PSLNULL == pusbdObj) || (PSLNULL == pdr))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Make sure this is intended for us
     */

    PSLASSERT(CLASS & pdr->bmRequestType);
    if (!(CLASS & pdr->bmRequestType))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Make sure that we do not get interrupted
     */

    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        fResult = FALSE;
        goto Exit;
    }
    hMutexRelease = pusbdObj->hMutex;

    /*  Save some redirection
     */

    pusbdCtx = pusbdObj->pusbdCtx;

    /*  Make sure that we have a USB context to forward the information
     *  along to and that the context is actually bound
     */

    if ((PSLNULL == pusbdCtx) || (PSLNULL == pusbdCtx->pfnHandleClassReq) ||
        (PSLNULL == pusbdCtx->mtpusbbm))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Retrieve a control buffer so that we can copy the information
     *  over- since this command will be handled asynchronously by the
     *  transport it is not possible for us to use the information provided
     *  by the UFN layer directly
     */

    PSLASSERT(sizeof(*pdr) == sizeof(MTPUSB_DEVICE_REQUEST));
    ps = MTPUSBBufferMgrRequest(pusbdCtx->mtpusbbm,
                            MTPUSBBUFFER_CONTROL,
                            sizeof(MTPUSB_DEVICE_REQUEST), 0, &pmtpusbdr,
                            &cbActual, PSLNULL, PSLNULL);
    if (PSL_FAILED(ps) || (sizeof(MTPUSB_DEVICE_REQUEST) > cbActual))
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Copy the device request over
     */

    PSLCopyMemory(pmtpusbdr, pdr, sizeof(*pmtpusbdr));

    pfnHandleClassReq = pusbdCtx->pfnHandleClassReq;
    aParamClassReq = pusbdCtx->aParamClassReq;
    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    /*  Call the class request callback function directly
     */

    fResult = PSL_SUCCEEDED(pfnHandleClassReq(aParamClassReq, pmtpusbdr,
                            sizeof(MTPUSB_DEVICE_REQUEST)));
    pmtpusbdr = PSLNULL;

    /*  Report success
     */

    fResult = PSLTRUE;

Exit:
    /*  If we did not handle the request we need to stall the control pipe
     *  and then we need to stall the default pipe and send a control status
     *  handshake
     */

    if (!fResult)
    {
        DWORD           dwResult;

        dwResult = _MTPUSBDevicePCFailControlRequest(pusbdObj);
        PSLASSERT(ERROR_SUCCESS == dwResult);
    }
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_MTPUSBBUFFERMGRRELEASE(pmtpusbdr);
    return fResult;
}


/*
 *  _MTPUSBDevicePCHandleSetupEvent
 *
 *  Responds to a UFN setup event request
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            USB device triggering event
 *      PSLUINT32       dwEvent             Setup event type
 *      PSL_CONST USB_SETUP_PACKET*
 *                      pdr                 Device request received
 *
 *  Returns:
 *      BOOL            TRUE if handled
 *
 */

BOOL _MTPUSBDevicePCHandleSetupEvent(USBDEVICEOBJ* pusbdObj,
                            PSLUINT32 dwEvent,
                            PSL_CONST USB_SETUP_PACKET* pdr)
{
    BOOL            fResult;

    /*  Validate arguments
     */

    if (PSLNULL == pusbdObj)
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Provide some feedback
     */

    switch (dwEvent)
    {
    case UFN_MSG_SETUP_PACKET:
        /*  Report the message request
         */

        PSLTraceDetail("**MTPUSBFn- USB Device Request (Unhandled)");

        /* FALL THROUGH INTENTIONAL */

    case UFN_MSG_PREPROCESSED_SETUP_PACKET:
        /*  Report the message type if necessary
         */

        if (UFN_MSG_PREPROCESSED_SETUP_PACKET == dwEvent)
        {
            PSLTraceDetail("**MTPUSBFn- USB Device Request (Handled)");
        }

        /*  And provide some details
         */

        PSLTraceDetail("\tDirection: %s",
                (DEVICE_TO_HOST & pdr->bmRequestType) ?
                    "Device->Host" : "Host->Device");

        PSLTraceDetail("\tType: %s",
                (VENDOR & pdr->bmRequestType) ?
                    "Vendor" : (CLASS & pdr->bmRequestType) ?
                        "Class" : "Standard");

        PSLTraceDetail("\tObject: %s",
                (RECIPIENT_OTHER & pdr->bmRequestType) ?
                    "Other" : (RECIPIENT_ENDPOINT & pdr->bmRequestType) ?
                        "Endpoint" : (RECIPIENT_INTERFACE & pdr->bmRequestType) ?
                            "Interface" : "Device");

        switch ((PSLUINT32)pdr->bRequest)
        {
        case 0:
            PSLTraceDetail("\tRequest: GET_STATUS");
            break;

        case 1:
            PSLTraceDetail("\tRequest: CLEAR_FEATURE");
            break;

        case 2:
            PSLTraceDetail("\tRequest: Reserved (2)");
            break;

        case 3:
            PSLTraceDetail("\tRequest: SET_FEATURE");
            break;

        case 4:
            PSLTraceDetail("\tRequest: Reserved (4)");
            break;

        case 5:
            PSLTraceDetail("\tRequest: SET_ADDRESS");
            break;

        case 6:
            PSLTraceDetail("\tRequest: GET_DESCRIPTOR");
            break;

        case 7:
            PSLTraceDetail("\tRequest: SET_DESCRIPTOR");
            break;

        case 8:
            PSLTraceDetail("\tRequest: GET_CONFIGURATION");
            break;

        case 9:
            PSLTraceDetail("\tRequest: SET_CONFIGURATION");
            break;

        case 10:
            PSLTraceDetail("\tRequest: GET_INTERFACE");
            break;

        case 11:
            PSLTraceDetail("\tRequest: SET_INTERFACE");
            break;
        case 12:

            PSLTraceDetail("\tRequest: SYNCH_FRAME");
            break;

        default:
            PSLTraceDetail("\tRequest: Unknown (%d)", pdr->bRequest);
            break;
        }

        PSLTraceDetail("\tValue: 0x%04x\tIndex: 0x%04x\tLength: 0x%04x",
                            pdr->wValue, pdr->wIndex,
                            pdr->wLength);
        break;

    default:
        /*  Unkown setup event
         */

        PSLASSERT(PSLFALSE);
        PSLTraceError("**MTPUSBFn- USB Device Request (Unknown)");
        break;
    }

    /*  Determine what type of message we are being asked to handle
     */

    switch (dwEvent)
    {
    case UFN_MSG_PREPROCESSED_SETUP_PACKET:
        /*  Ignore this message
         */

        fResult = FALSE;
        break;

    case UFN_MSG_SETUP_PACKET:
        /*  Handle requests that the UFN driver does not support
         */

        switch ((CLASS | VENDOR) & pdr->bmRequestType)
        {
        case STANDARD:
            /*  Handle the standard request
             */

            fResult = _MTPUSBDevicePCHandleStandardRequest(pusbdObj, pdr);
            break;

        case CLASS:
            /*  Handle the class request
             */

            fResult = _MTPUSBDevicePCHandleClassRequest(pusbdObj, pdr);
            break;

        case VENDOR:
            /*  Handle the vendor request
             */

            fResult = _MTPUSBDevicePCHandleVendorRequest(pusbdObj, pdr);
            break;

        case RESERVED:
        default:
            /*  Request is not supported
             */

            _MTPUSBDevicePCFailControlRequest(pusbdObj);

            /*  And we did not handle it
             */

            fResult = FALSE;
            break;
        }
        break;

    default:
        /*  We should never get here
         */

        PSLASSERT(PSLFALSE);
        fResult = FALSE;
        goto Exit;
    }

Exit:
    return fResult;
}


/*
 *  _MTPUSBDevicePCNotify
 *
 *  Handles callbacks from the USB function driver
 *
 *  Arguments:
 *      PVOID           pvNotifyParameter   Parameter associated with this
 *                                            notification
 *      DWORD           dwMsg               Notification message
 *      DWORD           dwParam             Notification parameter
 *
 *  Returns:
 *      BOOL        TRUE if handled, FALSE if not
 *
 */

BOOL WINAPI _MTPUSBDevicePCNotify(
                            PVOID pvNotifyParameter,
                            DWORD dwMsg,
                            DWORD dwParam)
{
    BOOL            fResult;
    USBDEVICEOBJ*   pusbdObj;

    /*  Validate arguments
     */

    if (NULL == pvNotifyParameter)
    {
        fResult = FALSE;
        goto Exit;
    }

    /*  Extract the device object
     */

    pusbdObj = (USBDEVICEOBJ*)pvNotifyParameter;
    PSLASSERT(USBDEVICEOBJ_COOKIE == pusbdObj->dwCookie);

    /*  Report notifications only at this point
     */

    switch (dwMsg)
    {
    case UFN_MSG_BUS_EVENTS:
        /*  Handle the bus events
         */

        fResult = _MTPUSBDevicePCHandleBusEvent(pusbdObj, dwParam);
        break;

    case UFN_MSG_CONFIGURED:
        /*  Handle the configuration notification
         */

        fResult = _MTPUSBDevicePCHandleConfigEvent(pusbdObj, dwParam);
        break;

    case UFN_MSG_BUS_SPEED:
        /*  Handle the speed notification
         */

        fResult = _MTPUSBDevicePCHandleSpeedEvent(pusbdObj, dwParam);
        break;

    case UFN_MSG_SETUP_PACKET:
    case UFN_MSG_PREPROCESSED_SETUP_PACKET:
        /*  Handle the setup request
         */

        fResult = _MTPUSBDevicePCHandleSetupEvent(pusbdObj, dwMsg,
                            (USB_SETUP_PACKET*)dwParam);
        break;

    case UFN_MSG_SET_ADDRESS:
        /*  Report the address
         */

        PSLTraceProgress("**MTPUSBFn- USB Address Set: 0x%02x", dwParam);
        fResult = TRUE;
        break;

    default:
        /*  Report Unknown
         */

        PSLTraceError("**MTPUSBFn- Unexpected USB Notification");
        fResult = FALSE;
        break;
    }

Exit:
    return fResult;
}


/*
 *  _MTPUSBDevicePCFailControlRequest
 *
 *  Stalls the control pipe and then sends a status handshake to indicate
 *  to the host the that control request was unsuccesful
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Device to service
 *
 *  Returns:
 *      DWORD           Windows error
 *
 */

DWORD _MTPUSBDevicePCFailControlRequest(
                            USBDEVICEOBJ* pusbdObj)
{
    DWORD           dwResult;

    /*  Validate arguments
     */

    if (PSLNULL == pusbdObj)
    {
        dwResult = ERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Stall the conrol pipe to indicate a failure
     */

    Nc2280_StallEndpoint(0);
    Nc2280_SendControlStatusHandShake();

    dwResult = ERROR_SUCCESS;

Exit:
    return dwResult;
}


/*
 *  _MTPUSBDevicePCBufferToPipe
 *
 *  Determines which pipe a buffer is to be transmitted or received over
 *
 *  Arguments:
 *      PSLUINT32       dwBufferType        MTP buffer type
 *      PSLBOOL         fTransmit           PSLTRUE if transmitting, PSLFALSE
 *                                            if receiving
 *
 *  Returns:
 *      DWORD           Pipe to use- PIPES_MAX_PIPES on error
 *
 */

DWORD _MTPUSBDevicePCBufferToPipe(
                            PSLUINT32 dwBufferType,
                            PSLBOOL fResponder)
{
    DWORD           dwPipe;

    PSLASSERT(fResponder);
    switch (dwBufferType)
    {
    case MTPBUFFER_COMMAND:
        /*  Responsers always receive commands on the BULK_OUT pipe
         */

        dwPipe = PIPE_BULK_OUT;
        break;

    case MTPBUFFER_TRANSMIT_DATA:
    case MTPUSBBUFFER_TRANSMIT_SHORT_DATA:
        /*  Responders transmit data on BULK_IN
         */

        dwPipe = PIPE_BULK_IN;
        break;

    case MTPBUFFER_RECEIVE_DATA:
        /*  Responders recieve data on BULK_OUT
         */

        dwPipe = PIPE_BULK_OUT;
        break;

    case MTPBUFFER_RESPONSE:
        /*  Responders always transmit responses on BULK_IN
         */

        dwPipe = PIPE_BULK_IN;
        break;

    case MTPBUFFER_EVENT:
        /*  Responders always transmit events on EVENT
         */

        dwPipe = PIPE_EVENT;
        break;

    case MTPUSBBUFFER_CONTROL:
        /*  Control buffers always go on the control pipe
         */

        dwPipe = PIPE_CONTROL;
        break;

    default:
        /*  Unexected buffer type
         */

        PSLASSERT(PSLFALSE);
        dwPipe = PIPE_MAX_PIPES;
        break;
    }
    return dwPipe;
    fResponder;
}


/*
 *  _MTPUSBDevicePCRequestTransfer
 *
 *  Locates an available transfer in the transfer list
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Object to retrieve transfer from
 *      PSLUINT32*      pidxTransfer        Transfer index
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBDevicePCRequestTransfer(
                            USBDEVICEOBJ* pusbdObj,
                            PSLUINT32* pidxTransfer)
{
    PSLUINT32       dwAvailable;
    PSLUINT32       idxOpen;
    PSLSTATUS       ps;
    PSLHANDLE       hMutexDevice = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pidxTransfer)
    {
        *pidxTransfer = MTPUSB_TOTAL_BUFFERS;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pusbdObj) || (PSLNULL == pidxTransfer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure this is one of our objects
     */

    PSLASSERT(USBDEVICEOBJ_COOKIE == pusbdObj->dwCookie);

    /*  Acquire the device mutex
     */

    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexDevice = pusbdObj->hMutex;

    /*  Determine if a buffer is available
     *
     *  NOTE:
     *  We should always have an available transfer because the buffers that
     *  are in use equal the number of transfers that we have reserved.  If
     *  a transfer is not available that means we should figure out why we
     *  have used all of the transfers but have not used all of the buffers.
     */

    PSLASSERT(0 != pusbdObj->dwTIAvailable);
    if (0 == pusbdObj->dwTIAvailable)
    {
        ps = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Figure out which transfer is availble
     */

    for (idxOpen = 0, dwAvailable = pusbdObj->dwTIAvailable;
            !(0x1 & dwAvailable) && (idxOpen < MTPUSB_TOTAL_BUFFERS);
            dwAvailable >>= 1, idxOpen++)
    {
    }

    /*  HACK:
     *  The preferred behavior would be to clear the contents of the
     *  transfer during the release.
     *
     *  Due to a "feature" of the Windows driver wrapper it is possible for
     *  a transfer to complete on the same thread (which holds the
     *  device mutex) on which it was called before ever returning from
     *  lpStartTransfer.  The end result is that while the hTransfer is
     *  correctly passed into the completion routine, it is lost the
     *  moment the transfer is released back to the pool.  The end result
     *  is that it is not returned via lpStartTransfer.  Since we are already
     *  protecting the transfer using the pool available flags and the
     *  transfer mutex is protecting against another thread getting
     *  the same transfer immediately, we clear the transfer at this
     *  point when it is re-acquired rather than when it is released.
     *  This way we can get the transfer ID back out of lpStartTransfer always.
     */

    PSLZeroMemory(&(pusbdObj->rgTransferInfo[idxOpen]),
                            sizeof(pusbdObj->rgTransferInfo[idxOpen]));

    /*  Mark the transfer in use and return the index
     */

    PSLASSERT(idxOpen != MTPUSB_TOTAL_BUFFERS);
    pusbdObj->dwTIAvailable &= ~(1 << idxOpen);
    *pidxTransfer = idxOpen;

    /*  Set the cookie on the transfer
     */

#ifdef PSL_ASSERTS
    pusbdObj->rgTransferInfo[idxOpen].dwCookie = USBTRANSFERINFO_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexDevice);
    return ps;
}


/*
 *  _MTPUSBDevicePCReleaseTransfer
 *
 *  Locates an available transfer in the transfer list
 *
 *  Arguments:
 *      USBTRANSFERINFO*
 *                      pusbTI              Transfer to release
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBDevicePCReleaseTransfer(
                            USBTRANSFERINFO* pusbdTI)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLUINT32       idxTransfer;
    PSLSTATUS       ps;
    USBDEVICEOBJ*   pusbdObj;

    /*  Validate arguments
     */

    if (PSLNULL == pusbdTI)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure this is one of our objects
     */

    PSLASSERT(USBTRANSFERINFO_COOKIE == pusbdTI->dwCookie);

    /*  Extract the root object from the transfer
     */

    pusbdObj = (USBDEVICEOBJ*)pusbdTI->pvRoot;
    PSLASSERT(USBDEVICEOBJ_COOKIE == pusbdObj->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdObj->hMutex;

    /*  Determine what transfer item we are dealing with
     */

    idxTransfer = (PSLUINT32)(pusbdTI - &(pusbdObj->rgTransferInfo[0]));

    /*  Mark the buffer as available (this transfer should not already
    *   be marked as available)
     */

    PSLASSERT(!((0x1 << idxTransfer) & pusbdObj->dwTIAvailable));
    pusbdObj->dwTIAvailable |= 0x1 << idxTransfer;

    /*  Release and clear the existing transfer information
     */

    SAFE_MTPUSBDEVICECERELEASE(pusbdObj);
    SAFE_MTPUSBDEVICECERELEASECONTEXT(pusbdTI->pusbdCtx);
    PSLZeroMemory(pusbdTI, sizeof(*pusbdTI));

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


static PNCUSBPIPE FindPipe(USBDEVICEOBJ* pusbdObj, BYTE Ep)
{
    PSLUINT32 idx;
    for (idx = 0; idx < PIPE_MAX_PIPES && pusbdObj->pipes[idx].Ep != Ep; idx++ )
    {
    }

    if (PIPE_MAX_PIPES > idx)
    {
        return &pusbdObj->pipes[idx];
    }
    else
    {
        /* this should never happen
          */
        PSLASSERT(FALSE);
        return PSLNULL;
    }
}

/*
 *  MTPUSBDevicePCTransferNotify
 *
 *  Responds to the transfer completion notification
 *
 *  Arguments:
 *      PVOID            pvNotifyParam     Notification parameter (USB device)
 *      PSLBYTE         Ep                      Endpoint #
 *      DWORD          cbXfered             MSB = 1 indicates transfer error,
  *                                                    otherwise is the transfered size
 *
 *  Returns:
 *      DWORD           Windows error code
 *
 */

DWORD WINAPI MTPUSBDevicePCTransferNotify(
                            PVOID pvNotifyParam,
                            PSLBYTE Ep,
                            DWORD cbXfered)
{
    DWORD                       cbTransferred;
    DWORD                       dwBufferType;
    DWORD                       dwResult;
    DWORD                       dwUSBError;
    PSLHANDLE                   hMutexRelease = PSLNULL;
    MTPUSBBUFFERMGR             mtpusbbm;
    PSLSTATUS                   ps;
    PSLMSG*                     pMsg = PSLNULL;
    USBTRANSFERINFO*            pusbTI = PSLNULL;
    USBDEVICEOBJ*               pusbdObj;
    USBDEVICECTX*               pusbdCtx;
    NCUSBPIPE*                  pipe;

    /*  Validate arguments
     */

    if (PSLNULL == pvNotifyParam)
    {
        dwResult = ERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the transfer and device information
     */

    pusbdObj = (USBDEVICEOBJ*)pvNotifyParam;
    PSLASSERT((PSLNULL != pusbdObj) &&
                            (USBDEVICEOBJ_COOKIE == pusbdObj->dwCookie));

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }
    hMutexRelease = pusbdObj->hMutex;


    pipe = FindPipe(pusbdObj, Ep);
    PSLASSERT(PSLNULL != pipe);
    if (PSLNULL == pipe)
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }
    pusbTI = pipe->pusbTI;

    if (PSLNULL == pusbTI)
    {
        dwResult = ERROR_SUCCESS;
        goto Exit;
    }

    PSLASSERT(USBTRANSFERINFO_COOKIE == pusbTI->dwCookie);

    /*  Extract context and completion signal
     */

    pusbdCtx = pusbTI->pusbdCtx;

    if ( cbXfered & ( 1 << 31 ) )
    {
        cbTransferred = 0;
        dwUSBError = cbXfered & ( ~(1 << 31) );
    }
    else
    {
        cbTransferred = cbXfered;
        dwUSBError = UFN_NO_ERROR;
    }

    PSLTraceProgress("**MTPUSBfn- Transfer Complete: Transfer 0x%08x\tPipe %1d\t"
                        "Bytes: 0x%08x\tUFN Error: 0x%08x", pusbTI,
                        pusbTI->idxPipe, cbTransferred, dwUSBError);

    switch(pusbTI->idxPipe)
    {
    case PIPE_CONTROL:
        /*  If this is an internal transfer send the control status handshake
         *  and do not forward the buffer along
         */

        if (MTPTRANSFERFLAGS_INTERNAL_TRANSFER & pusbTI->dwTransferFlags)
        {
            Nc2280_SendControlStatusHandShake();
            break;
        }

    case PIPE_BULK_IN:
    case PIPE_BULK_OUT:
        /*  If this is an internal transfer we want to not worry about
         *  forwarding the message along
         */

        if (MTPTRANSFERFLAGS_INTERNAL_TRANSFER & pusbTI->dwTransferFlags)
        {
            break;
        }

    case PIPE_EVENT:
        /*  Determine what type of buffer we are dealing with and make sure
         *  we really own it
         */

        ps = MTPUSBBufferMgrGetInfo(pusbTI->pvBuffer, &mtpusbbm, PSLNULL,
        					&dwBufferType);
        if (PSL_FAILED(ps) || (mtpusbbm != pusbdCtx->mtpusbbm))
        {
        	dwResult = ERROR_INVALID_HANDLE;
        	goto Exit;
        }

        /*  We have finished sending or receiving data- return the buffer to
         *  the transport to complete handling
         */

        ps = MTPMsgAlloc(pusbdCtx->mpEvent, &pMsg);
        if (PSL_FAILED(ps))
        {
        	dwResult = ERROR_NOT_ENOUGH_MEMORY;
        	goto Exit;
        }

        /*  Determine the correct message to send
         */

        switch (dwUSBError)
        {
        case UFN_NO_ERROR:
            /*  Report buffer sent or recieved
             */

            pMsg->msgID =
                (MTPTRANSFERFLAGS_IN_TRANSFER & pusbTI->dwTransferFlags) ?
                    MTPUSBMSG_BUFFER_SENT : MTPUSBMSG_BUFFER_RECEIVED;
            break;

        case UFN_DEVICE_NOT_RESPONDING_ERROR:
            pMsg->msgID = MTPUSBMSG_NOT_RESPONDING;
            break;

        case UFN_CANCELED_ERROR:
            pMsg->msgID =
                (MTPTRANSFERFLAGS_IN_TRANSFER & pusbTI->dwTransferFlags) ?
                    MTPUSBMSG_SEND_CANCELLED : MTPUSBMSG_RECEIVE_CANCELLED;
            break;

        case UFN_NOT_COMPLETE_ERROR:
            pMsg->msgID =
                (MTPTRANSFERFLAGS_IN_TRANSFER & pusbTI->dwTransferFlags) ?
                    MTPUSBMSG_SEND_INCOMPLETE : MTPUSBMSG_RECEIVE_INCOMPLETE;
            break;

        case UFN_CLIENT_BUFFER_ERROR:
            pMsg->msgID = MTPUSBMSG_BUFFER_ERROR;
            break;

        default:
            pMsg->msgID =
                (MTPTRANSFERFLAGS_IN_TRANSFER & pusbTI->dwTransferFlags) ?
                    MTPUSBMSG_SEND_FAILED : MTPUSBMSG_RECEIVE_FAILED;
            break;
        }

        /*  And handle the remaining message information
         */

        pMsg->aParam0 = dwBufferType;
        pMsg->aParam1 = pusbTI->aParam;
        pMsg->aParam2 = (PSLPARAM)pusbTI->pvBuffer;
        pMsg->aParam3 = (PSLPARAM)pusbTI;
        pMsg->alParam = cbTransferred;

        /*  And forward the message
         */

        ps = MTPMsgPost(pusbdCtx->mqEvent, pMsg);
        if (PSL_FAILED(ps))
        {
        	dwResult = ERROR_NOT_ENOUGH_MEMORY;
        	goto Exit;
        }
        pMsg = PSLNULL;
        break;

    default:
        /*  Should never get here
         */

        PSLASSERT(FALSE);
        dwResult = ERROR_NOT_SAME_DEVICE;
        goto Exit;
    }

    MTPUSBTransferComplete(pipe);

    dwResult = ERROR_SUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_MTPUSBDEVICECERELEASETRANSFER(pusbTI);
    SAFE_MTPMSGFREE(pMsg);
    return dwResult;
}


/*
 *  MTPUSBDevicePCStartTransfer
 *
 *  Initiates a USB transfer based on the flag specified
 *
 *  NOTE:
 *  Depending on how the underlying device driver is implemented the call
 *  into the driver may immediately call the notifier on a separate thread
 *  and produce a deadlock.  This function must always be called without the
 *  mutex active.
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Device to service
 *      DWORD           dwPipe              Pipe to use for the transmit
 *      DWORD           dwTransferFlags     Transfer flags (including direction)
 *      PSL_CONST PVOID pvBuffer            Buffer to transmit
 *      DWORD           cbBuffer            Length of the buffer
 *      PSLPARAM        aParam              Parameter to include in transfer
 *      PVOID*   phTransfer          Transfer handle return location
 *
 *  Returns:
 *      DWORD           Windows error code
 *
 */

DWORD _MTPUSBDevicePCStartTransfer(USBDEVICEOBJ* pusbdObj,
                            DWORD dwPipe,
                            DWORD dwFlags,
                            PSL_CONST PVOID pvBuffer,
                            DWORD cbBuffer,
                            PSLPARAM aParam,
                            PVOID* phTransfer)
{
    DWORD                   dwResult;
    PSLHANDLE               hMutexDevice = PSLNULL;
    PSLUINT32               idxTransfer;
    PSLSTATUS               ps;
    USBTRANSFERINFO*        pusbTI = PSLNULL;
    USBDEVICECTX*           pusbdCtx;

    /*  Clear result for safety
     */

    if (PSLNULL != phTransfer)
    {
        *phTransfer = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pusbdObj) || (PIPE_MAX_PIPES <= dwPipe) ||
        ((PSLNULL == pvBuffer) && (0 != cbBuffer)))
    {
        dwResult = ERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Acquire the device mutex so that we synchronize device operations
     */

    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }
    hMutexDevice = pusbdObj->hMutex;

    if (!(MTPUSBDEVICEFLAGS_BUS_ATTACHED & pusbdObj->dwFlags))
    {
        if (!(MTPTRANSFERFLAGS_INTERNAL_TRANSFER & dwFlags))
        {
            ps = MTPUSBBufferMgrRelease(pvBuffer);
            if (PSL_FAILED(ps))
            {
                dwResult = ERROR_NOT_READY;
                goto Exit;
            }
        }

        dwResult = ERROR_SUCCESS;
        goto Exit;
    }

    /*  Save some redirection and make sure we have a context for this operation
     */

    PSLASSERT((USBDEVICEOBJ_COOKIE == pusbdObj->dwCookie) &&
                            (NULL != pusbdObj->pusbdCtx));
    pusbdCtx = pusbdObj->pusbdCtx;
    if (NULL == pusbdCtx)
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }

    /*  Request a transfer
     */

    ps = _MTPUSBDevicePCRequestTransfer(pusbdObj, &idxTransfer);
    if (PSL_FAILED(ps))
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }

    /*  Save some redirection and setup for error handling
     */

    pusbTI = &(pusbdObj->rgTransferInfo[idxTransfer]);

    /*  Initialize the transfer information
     */

    pusbTI->pvRoot = pusbdObj;
    SAFE_MTPUSBDEVICECEADDREF(pusbTI->pvRoot);
    pusbTI->dwTransferFlags = dwFlags;
    pusbTI->idxPipe = dwPipe;
    pusbTI->pvBuffer = pvBuffer;
    pusbTI->cbBuffer = cbBuffer;
    pusbTI->aParam = aParam;
    pusbTI->pusbdCtx = pusbdCtx;
    SAFE_MTPUSBDEVICECEADDREFCONTEXT(pusbdCtx);

    MTPUSBTransferStart(&pusbdObj->pipes[dwPipe], pusbTI);

    if (PSLNULL != phTransfer)
    {
        *phTransfer = pusbTI;
    }
    pusbTI = PSLNULL;

	dwResult = ERROR_SUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexDevice);
    SAFE_MTPUSBDEVICECERELEASETRANSFER(pusbTI);
    return dwResult;
}




/*
 *  _MTPUSBDevicePCCancelTransfer
 *
 *  Cancels any pending transfer on the specified pipe
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Object to service
 *      UFN_TRANSFER    hTransfer           Transfer to cancel
 *      DWORD           dwCancelTimeout     Time to wait for synchronous
 *                                            cancel, 0 for asynchronous
 *
 *  Returns:
 *      DWORD           ERROR_SUCCESS on success
 *
 */

DWORD _MTPUSBDevicePCCancelTransfer(USBDEVICEOBJ* pusbdObj,
                            UFN_TRANSFER hTransfer, DWORD dwCancelTimeout)
{
    DWORD           dwResult;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    USBTRANSFERINFO* pusbTI = PSLNULL;

    /*  Validate arguments
     */

    if ( (PSLNULL == pusbdObj) ||
         (PSLNULL == hTransfer) ||
         (0 != dwCancelTimeout))
    {
        dwResult = ERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Acquire the mutex so that we synchronize operations
     */

    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }
    hMutexRelease = pusbdObj->hMutex;

    pusbTI = (USBTRANSFERINFO*)hTransfer;

    /* Cancel the transfer synchronously
      */
    MTPUSBTransferAbort(&pusbdObj->pipes[pusbTI->idxPipe], pusbTI);

    if (PSLNULL != pusbTI->pvBuffer)
    {
        MTPUSBBufferMgrRelease(pusbTI->pvBuffer);
    }

    dwResult = ERROR_SUCCESS;
Exit:
    SAFE_MTPUSBDEVICECERELEASETRANSFER(pusbTI);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return dwResult;
}

/*
 *  _MTPUSBDevicePCReset
 *
 *  Resets the device to an idle, disconnected state
 *
 *  Arguments:
 *      USBDEVICEOBJ*   pusbdObj            Object to reset
 *
 *  Returns:
 *      DWORD       ERROR_SUCCESS on success
 *
 */

DWORD _MTPUSBDevicePCReset(USBDEVICEOBJ* pusbdObj)
{
    PSLUINT32       idxTransfer;
    DWORD           dwResult;
    PSLSTATUS       ps;
    PSLHANDLE       hMutexRelease = PSLNULL;


    /*  Validate arguments
     */

    if (PSLNULL == pusbdObj)
    {
        dwResult = ERROR_SUCCESS;
        goto Exit;
    }

    /*  Acquire the mutex if necessary
     */

    PSLASSERT(PSLNULL != pusbdObj->hMutex);
    ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        dwResult = ERROR_NOT_READY;
        goto Exit;
    }
    hMutexRelease = pusbdObj->hMutex;

    /*  Clear the configured flag
     */

    pusbdObj->dwFlags &= ~MTPUSBDEVICEFLAGS_CONFIGURED;

    /*  Release all of the pending transfers
     */

	for (idxTransfer = 0; idxTransfer < MTPUSB_TOTAL_BUFFERS; idxTransfer++)
	{
    	/*  If this transfer is not active no need to do anything
     	*/

    	if ((0x1 << idxTransfer) & pusbdObj->dwTIAvailable)
    	{
        		continue;
    	}

        dwResult = _MTPUSBDevicePCCancelTransfer(pusbdObj,
                         &pusbdObj->rgTransferInfo[idxTransfer], 0);
        PSLASSERT(ERROR_SUCCESS == dwResult);
    }

    dwResult = ERROR_SUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return dwResult;
}


/*
 *  MTPUSBDevicePCCreate
 *
 *  Opens the specified context with an MTP configuration
 *
 *  Arguments:
 *      LPCWSTR         szDeviceKey         USB device to open
 *      MTPUSBDEVICECE* pusbd               Resulting MTP USB device
 *
 *  Returns:
 *      DWORD       ERROR_SUCCESS on success
 *
 */

DWORD MTPUSBDevicePCCreate(MTPUSBDEVICECE* pusbd)
{
    DWORD           dwResult;
    PSLSTATUS       ps;
    USBDEVICEOBJ*   pusbdObj = NULL;


    static NETCHIP_DEVICE_CONTROL_BLOCK PC_NcDevCtrlB =
    {
        _MTPUSBDevicePCNotify,
        PSLNULL,

        MTPUSBDevicePCTransferNotify,

        &g_mtpUSBHSDeviceDesc,
        &Transfer_DeviceQualifierDescriptor,

        &Transfer_HS_Configuration,
        &Transfer_FS_Configuration,

        PSLFALSE,
        PSLFALSE,

        TRANSFER_ASCII_PRODUCT,                     // PCHAR Product;
        TRANSFER_ASCII_MANUFACTURER,                // PCHAR Manufacturer;
        TRANSFER_ASCII_SERIALNO,                    // PCHAR Serial;

        0,
        PSLNULL
    };

    /*  Clear result for safety
     */

    if (NULL != pusbd)
    {
        *pusbd = PSLNULL;
    }

    /*  Create a new USB device object for this device
     */

    pusbdObj = (USBDEVICEOBJ*)PSLMemAlloc(PSLMEM_PTR, sizeof(USBDEVICEOBJ) );
    if (NULL == pusbdObj)
    {
        dwResult = ERROR_OUTOFMEMORY;
        goto Exit;
    }

    PSLSetMemory(pusbdObj, 0, sizeof(USBDEVICEOBJ));


    /*  Initialize the core values
     */

#ifdef PSL_ASSERTS
    pusbdObj->dwCookie = USBDEVICEOBJ_COOKIE;
#endif  /* PSL_ASSERTS */
    pusbdObj->cRefs = 1;

    /*  Create a mutex to handle device synchronization issues
     */

    ps = PSLMutexOpen(0, PSLNULL, &(pusbdObj->hMutex));
    if (PSL_FAILED(ps))
    {
        dwResult = ERROR_OUTOFMEMORY;
        goto Exit;
    }

    /*  Mark all of the transfers as available
     */

    pusbdObj->dwTIAvailable = (0x1 << MTPUSB_TOTAL_BUFFERS) - 1;
    pusbdObj->cbAlignment = sizeof(DWORD);
    pusbdObj->pusbdCtx = PSLNULL;

    MTPUSBTransferInitialize(&pusbdObj->pipes[PIPE_CONTROL], EP0);
    MTPUSBTransferInitialize(&pusbdObj->pipes[PIPE_BULK_IN], EPA);
    MTPUSBTransferInitialize(&pusbdObj->pipes[PIPE_BULK_OUT], EPB);
    MTPUSBTransferInitialize(&pusbdObj->pipes[PIPE_EVENT], EPE);

    PC_NcDevCtrlB.pvNotifyParameter = pusbdObj;
    PC_NcDevCtrlB.fReportedSpeed = PSLFALSE;
     //
    // Initialize subsystems (interrupt, DMA, memory-mapped chip access)
    //
    if (!System_InitializeSystem( &PC_NcDevCtrlB ) )
    {
        dwResult = ERROR_OUTOFMEMORY;
        goto Exit;
    }

    /*  Return the device
     */

    *pusbd = pusbdObj;
    pusbdObj = PSLNULL;

    /*  Report success
     */
    dwResult = ERROR_SUCCESS;

Exit:
    SAFE_MTPUSBDEVICEDESTROY(pusbdObj);
    return dwResult;
}

/*
 *  MTPUSBDevicePCShutdown
 *
 *      Shutdown the release thread if it is present.
 *
 *  Arguments:
 *      MTPUSBDEVICE
 *                  usbd            USB device to Deinit
 *
 *  Returns:
 *      DWORD       ERROR_SUCCESS on success
 *
 */
DWORD PSL_API MTPUSBDevicePCShutdown(
                            MTPUSBDEVICECE usbd)
{
    usbd;
    if ( PSLNULL != _HSignalRelease)
    {
        PSLSignalSet(_HSignalRelease);

        PSLSignalWait(_HReleaseThread, PSLTHREAD_INFINITE);
    }

    return ERROR_SUCCESS;
}

/*
 *  MTPUSBDevicePCDestroy
 *
 *  Destroys the specified USB context
 *
 *  Arguments:
 *      MTPUSBDEVICE
 *                  usbd            USB device to destroy
 *
 *  Returns:
 *      DWORD       ERROR_SUCCESS on success
 *
 */

DWORD MTPUSBDevicePCDestroy(MTPUSBDEVICE usbd)
{
    DWORD           dwResult;
    PSLHANDLE       hMutexDevice = PSLNULL;
    PSLSTATUS       ps;
    USBDEVICEOBJ*   pusbdObj =PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == usbd)
    {
        dwResult = ERROR_SUCCESS;
        goto Exit;
    }

    /*  Extract the USB object
     */

    pusbdObj = (USBDEVICEOBJ*)usbd;
    PSLASSERT(USBDEVICEOBJ_COOKIE == pusbdObj->dwCookie);

    /*  Acquire the device mutex if present
     */

    if (PSLNULL != pusbdObj->hMutex)
    {
        ps = PSLMutexAcquire(pusbdObj->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            dwResult = ERROR_NOT_READY;
            goto Exit;
        }
        hMutexDevice = pusbdObj->hMutex;
    }

    /*  Close any active connection associated with this object
     */

    dwResult = _MTPUSBDevicePCReset(pusbdObj);
    if (ERROR_SUCCESS != dwResult)
    {
        goto Exit;
    }

    PSLASSERT(((1 << MTPUSB_TOTAL_BUFFERS) - 1) == pusbdObj->dwTIAvailable);

    MTPUSBTransferUninitialize(&pusbdObj->pipes[PIPE_CONTROL]);
    MTPUSBTransferUninitialize(&pusbdObj->pipes[PIPE_BULK_IN]);
    MTPUSBTransferUninitialize(&pusbdObj->pipes[PIPE_BULK_OUT]);
    MTPUSBTransferUninitialize(&pusbdObj->pipes[PIPE_EVENT]);

    Rdk_AtExit();

    /*  Report success
     */

    dwResult = ERROR_SUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexDevice);
    SAFE_MTPUSBDEVICECERELEASE(pusbdObj);

    return dwResult;
}


/*
 *  MTPUSBDeviceBind
 *
 *  Binds a queue to the USB device to receive notifications of events
 *
 *  Arguments:
 *      MTPUSBDEVICE
 *                      hUSBDecviceSrc      USB device event source
 *      PSLMSGQUEUE     mqDest              Message queue to send events to
 *      PFNMTPUSBDEVICEHANDLECLASSREQUEST
 *                      pfnHandleClassReq   Callback function for handling class
 *                                            requests
 *      PSLPARAM        aParamClassReq      Parameter for class request callback
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_EXISTS if the
 *                        same queue is being bound, PSLERROR_IN_USE if the
 *                        device is already bound.
 *
 */

PSLSTATUS MTPUSBDeviceBind(MTPUSBDEVICE hUSBDeviceSrc, PSLMSGQUEUE mqDest,
                            PFNMTPUSBDEVICEHANDLECLASSREQUEST pfnHandleClassReq,
                            PSLPARAM aParamClassReq)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    USBDEVICECTX*   pusbdCtx;

    /*  Validate arguments
     */

    if ((PSLNULL == hUSBDeviceSrc) || (PSLNULL == mqDest))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDeviceSrc;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we can update the object synchronously
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  We only allow binding to attached devices
     */

    if (PSLNULL == pusbdCtx->pvRoot)
    {
        ps = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    /*  If this object is already bound than either report failure or that it
     *  is already bound
     */

    if (PSLNULL != pusbdCtx->mqEvent)
    {
        ps = (mqDest == pusbdCtx->mqEvent) ?
                            PSLSUCCESS_EXISTS : PSLERROR_IN_USE;
        goto Exit;
    }

    /*  Attempt to allocate a message pool if necessary to use for sending
     *  events to this destination
     */

    if (PSLNULL == pusbdCtx->mpEvent)
    {
        ps = MTPMsgPoolCreate(MTPUSBCE_POOL_SIZE, &(pusbdCtx->mpEvent));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  We have a message pool- go ahead and store the queue and addref the
     *  context to complete the bind
     */

    pusbdCtx->mqEvent = mqDest;
    pusbdCtx->pfnHandleClassReq = pfnHandleClassReq;
    pusbdCtx->aParamClassReq = aParamClassReq;
    _MTPUSBDevicePCAddRefContext(pusbdCtx);
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPUSBDeviceUnbind
 *
 *  Removes an event queue from the USB device
 *
 *  Arguments:
 *      MTPUSBDEVICE
 *                      hUSBDeviceSrc       USB device event source
 *      PSLMSGQUEUE     mqDest              Message queue to unbind
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBDeviceUnbind(MTPUSBDEVICE hUSBDeviceSrc,
                            PSLMSGQUEUE mqDest)
{
    PSLBOOL         fReleaseCtx = PSLFALSE;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    USBDEVICECTX*   pusbdCtx = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == hUSBDeviceSrc) || (PSLNULL == mqDest))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDeviceSrc;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we can update the object synchronously
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  And detach the device from the context
     */

    if (PSLNULL != pusbdCtx)
    {
        SAFE_MTPUSBDEVICECERELEASE(pusbdCtx->pvRoot);
        pusbdCtx->pvRoot = PSLNULL;
    }

    /*  If this object is not bound report success
     */

    if (PSLNULL == pusbdCtx->mqEvent)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  If the objects do not match then report an error
     */

    PSLASSERT(mqDest == pusbdCtx->mqEvent);
    if (mqDest != pusbdCtx->mqEvent)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Remove the reference to the queue from the object to close the
     *  event handling and release the reference count
     */

    pusbdCtx->mqEvent = PSLNULL;
    fReleaseCtx = PSLTRUE;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    if (fReleaseCtx)
    {
        SAFE_MTPUSBDEVICECERELEASECONTEXT(pusbdCtx);
    }
    return ps;
}


/*
 *  MTPUSBDeviceGetInfo
 *
 *  Retrieves information about the USB device specified
 *
 *  Arguments:
 *      MTPUSBDEVICE    hUSBDevice          USB device to query
 *      MTPUSBDEVICEINFO*
 *                      pusbdI              Return buffer for device info
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBDeviceGetInfo(MTPUSBDEVICE hUSBDevice,
                            MTPUSBDEVICEINFO* pusbdI)
{
    PSLHANDLE                           hMutexRelease = PSLNULL;
    PSLSTATUS                           ps;
    USBDEVICECTX*                       pusbdCtx;
    USBDEVICEOBJ*                       pusbdObj;
    PSL_CONST USB_ENDPOINT_DESCRIPTOR*  pufnepList;
    PSL_CONST USB_DEVICE_DESCRIPTOR*    pusbdDesc;

    /*  Clear results for safety
     */

    if (PSLNULL != pusbdI)
    {
        PSLZeroMemory(pusbdI, sizeof(*pusbdI));
    }

    /*  Validate arguments
     */

    if ((PSLNULL == hUSBDevice) || (PSLNULL == pusbdI))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDevice;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we can update the object synchronously
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  Save some redirection
     */

    pusbdObj = (USBDEVICEOBJ*)pusbdCtx->pvRoot;

    /*  Retrieve information about the device
     */

    if (!_MTPUSBDevicePCGetDeviceInfo(pusbdObj, &pusbdDesc, &pufnepList))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Return the information requested
     */

    PSLASSERT(EP_MAXPACKETSIZE(pufnepList[0]) ==
                    EP_MAXPACKETSIZE(pufnepList[1]));

    pusbdI->cbPacketCommand = EP_MAXPACKETSIZE(pufnepList[1]);
    pusbdI->cbPacketData = EP_MAXPACKETSIZE(pufnepList[0]);
    pusbdI->cbPacketResponse = EP_MAXPACKETSIZE(pufnepList[0]);
    pusbdI->cbPacketEvent = EP_MAXPACKETSIZE(pufnepList[2]);
    pusbdI->cbPacketControl = pusbdDesc->bMaxPacketSize0;

    /* It is not implemented to send ZLP for thees packets due to
     * the sizes of EVENT/RESPONSE/COMMAND packets will not
     * equal to or larger than the MAX_PACKET_SIZE of the corresponding
     * endpoints according to the MTP spec.
     */
    PSLASSERT(sizeof(MTPUSBPACKET_EVENT) < pusbdI->cbPacketEvent);
    PSLASSERT(sizeof(MTPUSBPACKET_OPERATION_RESPONSE) <
                    pusbdI->cbPacketResponse);
    PSLASSERT(sizeof(MTPUSBPACKET_OPERATION_REQUEST) <
                    pusbdI->cbPacketCommand);


    pusbdI->cbMaxData = PSLAllocAlign(
                    MTPUSB_MAX_DATA_CHUNK_SIZE, pusbdI->cbPacketData);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPUSBDeviceReset
 *
 *  Cancels all pending transfers on non-control pipes and releases any
 *  pending buffer requests.
 *
 *  Arguments:
 *      MTPUSBDEVICE    hUSBDevice          USB device to reset
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBDeviceReset(MTPUSBDEVICE hUSBDevice)
{
    DWORD               dwAvailable;
    DWORD               dwBufferType;
    DWORD               dwResult;
    PSLHANDLE           hMutexDevice = PSLNULL;
    DWORD               idxTransfer;
    PSLSTATUS           ps;
    USBDEVICECTX*       pusbdCtx;
    USBDEVICEOBJ*       pusbdObj = PSLNULL;
    USBTRANSFERINFO*    pusbTI;

    /*  Validate arguments
     */

    if (PSLNULL == hUSBDevice)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDevice;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we guarantee that we have a reference
     *  on the object
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexDevice = pusbdCtx->hMutex;

    /*  Extract the object and addref it so that it stays around until we
     *  finish this request
     */

    pusbdObj = (USBDEVICEOBJ*)pusbdCtx->pvRoot;
    if (PSLNULL == pusbdObj)
    {
        ps = PSLERROR_CONNECTION_CLOSED;
        goto Exit;
    }
    SAFE_MTPUSBDEVICECEADDREF(pusbdObj);

    /*  Cancel any pending buffer requests for standard MTP buffer types
     */

    for (dwBufferType = (MTPBUFFER_UNKNOWN + 1);
            dwBufferType < MTPBUFFER_START_TRANSPORT_SPECIFIC;
            dwBufferType++)
    {
        ps = MTPUSBBufferMgrCancelRequest(pusbdCtx->mtpusbbm, dwBufferType);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Walk through the pending transfers looking for ones that are not
     *  on the control pipe and then issue a cancel for them
     */

    for (idxTransfer = 0, dwAvailable = pusbdObj->dwTIAvailable;
            (idxTransfer < MTPUSB_TOTAL_BUFFERS);
            dwAvailable >>= 1, idxTransfer++)
    {
        /*  If this transfer is available ignore it
         */

        if (0x1 & dwAvailable)
        {
            continue;
        }

        /*  Skip any control transfers
         */

        pusbTI = &(pusbdObj->rgTransferInfo[idxTransfer]);
        if (PIPE_CONTROL == pusbTI->idxPipe)
        {
            continue;
        }

        /*  Request the transfer be cancelled
         */

        dwResult = _MTPUSBDevicePCCancelTransfer(pusbdObj, pusbTI, 0);
        PSLASSERT(ERROR_SUCCESS == dwResult);
    }


    if (pusbdObj->pipes[PIPE_BULK_OUT].fInitialized)
    {
        /* Drain the USB buffer by issuing a data-out transfer without
         * specifying buffer and read length
         */
        Nc2280_KickRxXfer(pusbdObj->pipes[PIPE_BULK_OUT].Ep, PSLNULL, 0);
    }


    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexDevice);
    SAFE_MTPUSBDEVICECERELEASE(pusbdObj);
    return ps;
}


/*
 *  MTPUSBDeviceRequestBuffer
 *
 *  Requests a buffer of the specified type from the USB device
 *
 *  Arguments:
 *      MTPUSBDEVICE
 *                      hUSBDevice          USB Device for buffer
 *      PSLUINT32       dwBufferType        Buffer type requested
 *      PSLUINT32       cbRequested         Size of the buffer requested
 *      PSLPARAM        aParam              Parameter to include on
 *                                            asynchronous completion
 *      PSLVOID**       ppvBuffer           Return location for buffer
 *      PSLUINT32*      pcbActual           Actual size of buffer returned
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_WOULD_BLOCK if
 *                        buffer not currently available
 *
 */

PSLSTATUS MTPUSBDeviceRequestBuffer(MTPUSBDEVICE hUSBDevice,
                            PSLUINT32 dwBufferType, PSLUINT32 cbRequested,
                            PSLPARAM aParam, PSLVOID** ppvBuffer,
                            PSLUINT32* pcbActual)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    USBDEVICECTX*   pusbdCtx;

    /*  Clear results for safety
     */

    if (PSLNULL != ppvBuffer)
    {
        *ppvBuffer = PSLNULL;
    }
    if (PSLNULL != pcbActual)
    {
        *pcbActual = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == hUSBDevice) ||
        ((PSLNULL != ppvBuffer) && (PSLNULL == pcbActual)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDevice;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we can update the object synchronously
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  We only allow buffers to be requested if we configured and
     *  bound
     */

    PSLASSERT(PSLNULL != pusbdCtx->pvRoot);
    PSLASSERT(PSLNULL != pusbdCtx->mqEvent);
    if ((PSLNULL == pusbdCtx->pvRoot) || (PSLNULL == pusbdCtx->mqEvent))
    {
        ps = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Request a buffer from the buffer manager
     */

    PSLASSERT(PSLNULL != pusbdCtx->mtpusbbm);
    ps = MTPUSBBufferMgrRequest(pusbdCtx->mtpusbbm,
                            dwBufferType,
                            cbRequested,
                            aParam,
                            ppvBuffer,
                            pcbActual,
                            pusbdCtx->mqEvent,
                            pusbdCtx->mpEvent);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPUSBDeviceReleaseBuffer
 *
 *  Releases the specified buffer back to the USB device
 *
 *  Arguments:
 *      MTPUSBDEVICE
 *                      hUSBDevice          USB device for buffer
 *      PSLVOID*        pvBuffer            Buffer to release
 *
 */

PSLSTATUS MTPUSBDeviceReleaseBuffer(MTPUSBDEVICE hUSBDevice,
                            PSLVOID* pvBuffer)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    USBDEVICECTX*   pusbdCtx;

    /*  Validate arguments
     */

    if (PSLNULL == hUSBDevice)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we have work to do
     */

    if (PSLNULL == pvBuffer)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDevice;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we can update the object synchronously
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  Tell the buffer manager to release the buffer
     */

    ps = MTPUSBBufferMgrRelease(pvBuffer);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPUSBDeviceReceiveBuffer
 *
 *  Requests that the specified buffer type be recieved from the appropriate
 *  USB pipe
 *
 *  Arguments:
 *      MTPUSBDEVICE
 *                      hUSBDevice          USB Device to recieve buffer with
 *      PSLUINT32       dwBufferType        Type of buffer requested
 *      PSLUINT32       cbRequested         Amount of data requested
 *      PSLPARAM        aParam              Parameter to be returned with
 *                                            notification
 *      PSLPARAM*       paCookie            Transaction cookie
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBDeviceReceiveBuffer(MTPUSBDEVICE hUSBDevice,
                            PSLVOID* pvBuffer, PSLPARAM aParam,
                            PSLPARAM* paCookie)
{
    DWORD           cbAvailable;
    DWORD           dwResult;
    DWORD           dwBufferType;
    DWORD           dwPipe;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PVOID              hTransfer;
    MTPUSBBUFFERMGR mtpusbbm;
    PSLSTATUS       ps;
    USBDEVICECTX*   pusbdCtx;
    USBDEVICEOBJ*   pusbdObj = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != paCookie)
    {
        *paCookie = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == hUSBDevice) || (PSLNULL == pvBuffer) ||
        (PSLNULL == paCookie))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDevice;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we can update the object synchronously
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  Extract the object and addref it so that it stays around until we
     *  finish this request
     */

    pusbdObj = (USBDEVICEOBJ*)pusbdCtx->pvRoot;
    PSLASSERT(PSLNULL != pusbdObj);
    SAFE_MTPUSBDEVICECEADDREF(pusbdObj);

    /*  We only will attempt a receive if the device is configured and bound
     */

    PSLASSERT(PSLNULL != pusbdCtx->mqEvent);
    if ((PSLNULL == pusbdObj) || (PSLNULL == pusbdCtx->mqEvent))
    {
        ps = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Validate that the buffer belongs to this device and retrieve the
     *  information needed to perform the request
     */

    ps = MTPUSBBufferMgrGetInfo(pvBuffer, &mtpusbbm, &cbAvailable,
                            &dwBufferType);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    if (mtpusbbm != pusbdCtx->mtpusbbm)
    {
        ps = PSLERROR_INVALID_BUFFER;
        goto Exit;
    }

    /*  Determine which pipe we will be using
     */

    dwPipe = _MTPUSBDevicePCBufferToPipe(dwBufferType, PSLTRUE);
    if (PIPE_MAX_PIPES <= dwPipe)
    {
        ps = PSLERROR_INVALID_BUFFER;
        goto Exit;
    }

    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    /*  And initiate the receive
     */

    dwResult = _MTPUSBDevicePCStartTransfer(pusbdObj, dwPipe,
                            MTPTRANSFERFLAGS_OUT_TRANSFER,
                            pvBuffer, cbAvailable, aParam, &hTransfer);
    if (ERROR_SUCCESS != dwResult)
    {
        ps = PSLERROR_USB_FAILURE;
        goto Exit;
    }

    /*  Return the transfer handle as a cookie
     */

    *paCookie = (PSLPARAM)hTransfer;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICECERELEASE(pusbdObj);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPUSBDeviceSendBuffer
 *
 *  Sends the buffer provided over the USB transport.
 *
 *  Arguments:
 *      MTPUSBDEVICE
 *                      hUSBDevice          USB Device to send the buffer with
        PSLUINT32       dwPacketFlag        Flag if this is the last packet
 *      PSLVOID*        pvBuffer            Buffer to send
 *      PSLUINT32       cbSend              Bytes to send
 *      PSLPARAM        aParam              Parameter to include in notification
 *      PSLPARAM*       paCookie            Return buffer for cookie
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBDeviceSendBuffer(MTPUSBDEVICE hUSBDevice,
                            PSLUINT32 dwPacketFlag,
                            PSLVOID* pvBuffer, PSLUINT32 cbSend,
                            PSLPARAM aParam, PSLPARAM* paCookie)
{
    DWORD           cbAvailable;
    DWORD           dwBufferType;
    DWORD           dwPipe;
    DWORD           dwResult;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PVOID              hTransfer;
    MTPUSBBUFFERMGR mtpusbbm;
    PSLSTATUS       ps;
    USBDEVICECTX*   pusbdCtx;
    USBDEVICEOBJ*   pusbdObj = 0;
    DWORD           cbMaxPacketSize;
    PSLBOOL         fShortPacket;

    PSL_CONST USB_ENDPOINT_DESCRIPTOR*  pufnepList;
    PSL_CONST USB_DEVICE_DESCRIPTOR*    pusbdDesc;

    static DWORD    dwShortPacket = 0;

    /*  Clear results for safety
     */

    if (PSLNULL != paCookie)
    {
        *paCookie = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == hUSBDevice) || ((PSLNULL == pvBuffer) && (0 != cbSend)) ||
        (PSLNULL == paCookie))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDevice;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we can update the object synchronously
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  Validate that the buffer belongs to this device and retrieve the
     *  information needed to perform the request
     */

    ps = MTPUSBBufferMgrGetInfo(pvBuffer, &mtpusbbm, &cbAvailable,
                            &dwBufferType);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    if (mtpusbbm != pusbdCtx->mtpusbbm)
    {
        ps = PSLERROR_INVALID_BUFFER;
        goto Exit;
    }
    if (cbAvailable < cbSend)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Extract the object and addref it so that it stays around until we
     *  finish this request
     */

    pusbdObj = (USBDEVICEOBJ*)pusbdCtx->pvRoot;
    PSLASSERT(PSLNULL != pusbdObj);
    if (PSLNULL == pusbdObj)
    {
        ps = PSLERROR_CONNECTION_CLOSED;
        goto Exit;
    }
    SAFE_MTPUSBDEVICECEADDREF(pusbdObj);

    /*  We only will attempt a receive if the device is configured and bound
     */

    PSLASSERT(PSLNULL != pusbdCtx->mqEvent);
    if ((PSLNULL == pusbdObj) || (PSLNULL == pusbdCtx->mqEvent))
    {
        ps = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Determine which pipe we will be using
     */

    dwPipe = _MTPUSBDevicePCBufferToPipe(dwBufferType, PSLTRUE);
    if (PIPE_MAX_PIPES <= dwPipe)
    {
        ps = PSLERROR_INVALID_BUFFER;
        goto Exit;
    }

    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    if (!_MTPUSBDevicePCGetDeviceInfo(pusbdObj, &pusbdDesc, &pufnepList))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (PIPE_CONTROL == dwPipe)
    {
        cbMaxPacketSize = pusbdDesc->bMaxPacketSize0;
    }
    else
    {
        cbMaxPacketSize = EP_MAXPACKETSIZE(pufnepList[dwPipe]);
    }

    fShortPacket = (0 != (cbSend % cbMaxPacketSize));

    if (fShortPacket && 0 == (dwPacketFlag & MTPMSGFLAG_TERMINATE))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  And initiate the send
     */

    dwResult = _MTPUSBDevicePCStartTransfer(pusbdObj, dwPipe,
                            MTPTRANSFERFLAGS_IN_TRANSFER,
                            pvBuffer, cbSend, aParam, &hTransfer);
    if (ERROR_SUCCESS != dwResult)
    {
        ps = PSLERROR_USB_FAILURE;
        goto Exit;
    }

    /*  Return the transfer handle as a cookie
     */

    *paCookie = (PSLPARAM)hTransfer;

    if (! fShortPacket &&
        (0 < cbSend) &&
        (0 != (dwPacketFlag & MTPMSGFLAG_TERMINATE)))
    {
        /* Send the NULL-packet as per USB spec requirement.
         * the address of the static variable dwShortPacket serves as
         * a valid pointer required by the transfer function. its value
         * WILL NOT be accessed as the data length is specified as 0.
         */
        _MTPUSBDevicePCStartTransfer(pusbdObj, dwPipe,
                    (MTPTRANSFERFLAGS_IN_TRANSFER |
                        MTPTRANSFERFLAGS_INTERNAL_TRANSFER),
                    &dwShortPacket, 0, 0, PSLNULL);
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICECERELEASE(pusbdObj);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPUSBDeviceCancelRequest
 *
 *  Sends the buffer provided over the USB transport.
 *
 *  Arguments:
 *      MTPUSBDEVICE
 *                      hUSBDevice          USB Device to send the buffer with
 *      PSLPARAM        aCookie             Cookie for request to transfer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBDeviceCancelRequest(MTPUSBDEVICE hUSBDevice,
                            PSLPARAM aCookie)
{
    DWORD           dwResult;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    USBDEVICECTX*   pusbdCtx;
    USBDEVICEOBJ*   pusbdObj = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == hUSBDevice) || (PSLNULL == aCookie))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDevice;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we guarantee that we have a reference
     *  on the object
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  Extract the object and addref it so that it stays around until we
     *  finish this request
     */

    pusbdObj = (USBDEVICEOBJ*)pusbdCtx->pvRoot;
    PSLASSERT(PSLNULL != pusbdObj);
    if (PSLNULL == pusbdObj)
    {
        ps = PSLERROR_CONNECTION_CLOSED;
        goto Exit;
    }
    SAFE_MTPUSBDEVICECEADDREF(pusbdObj);

    /*  Request the tranfer be cancelled
     */

    dwResult = _MTPUSBDevicePCCancelTransfer(pusbdObj,
                            (UFN_TRANSFER)aCookie,
                            0);
    if (ERROR_SUCCESS != dwResult)
    {
        ps = PSLERROR_USB_FAILURE;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICECERELEASE(pusbdObj);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}

/*
 *  MTPUSBDeviceStallPipe
 *
 *  Stalls the specified pipe
 *
 *  Arguments:
 *      MTPUSBDEVICE    hUSBDevice          USB device to stall
 *      PSLUINT32       dwBufferType        Buffer type to stall
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBDeviceStallPipe(MTPUSBDEVICE hUSBDevice,
                            PSLUINT32 dwBufferType)
{
    DWORD           dwPipe;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    USBDEVICECTX*   pusbdCtx;
    USBDEVICEOBJ*   pusbdObj = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == hUSBDevice)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDevice;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we can update the object synchronously
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  Extract the device object to make sure that we are configured and
     *  guarantee that it sticks around while we release the mutex to
     *  deal with the hardware.
     */

    pusbdObj = (USBDEVICEOBJ*)pusbdCtx->pvRoot;
    if (PSLNULL == pusbdObj)
    {
        ps = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }
    SAFE_MTPUSBDEVICECEADDREF(pusbdObj);

    /*  Determine what pipe is associated with this buffer
     */

    dwPipe = _MTPUSBDevicePCBufferToPipe(dwBufferType, PSLTRUE);
    if (PIPE_MAX_PIPES <= dwPipe)
    {
        ps = PSLERROR_INVALID_BUFFER;
        goto Exit;
    }

    /*  Release the mutex so that driver can call back into us if necessary
     */

    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    Nc2280_StallEndpoint( (BYTE)dwPipe );

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICECERELEASE(pusbdObj);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPUSBDeviceClearPipeStall
 *
 *  Clears a stall on the pipe associated with the specified buffer type
 *
 *  Arguments:
 *      MTPUSBDEVICE    hUSBDevice          USB device to stall
 *      PSLUINT32       dwBufferType        Buffer type to stall
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBDeviceClearPipeStall(MTPUSBDEVICE hUSBDevice,
                            PSLUINT32 dwBufferType)
{
    DWORD           dwPipe;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    USBDEVICECTX*   pusbdCtx;
    USBDEVICEOBJ*   pusbdObj = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == hUSBDevice)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDevice;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that we can update the object synchronously
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  Extract the device object to make sure that we are configured and
     *  guarantee that it sticks around while we release the mutex to
     *  deal with the hardware.
     */

    pusbdObj = (USBDEVICEOBJ*)pusbdCtx->pvRoot;
    if (PSLNULL == pusbdObj)
    {
        ps = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }
    SAFE_MTPUSBDEVICECEADDREF(pusbdObj);

    /*  Determine what pipe is associated with this buffer
     */

    dwPipe = _MTPUSBDevicePCBufferToPipe(dwBufferType, PSLTRUE);
    if (PIPE_MAX_PIPES <= dwPipe)
    {
        ps = PSLERROR_INVALID_BUFFER;
        goto Exit;
    }

    /*  Release the mutex so that driver can call back into us if necessary
     */

    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    /*  And stall the specified pipe
     */

    Nc2280_UnstallEndpoint( (BYTE)dwPipe );

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICECERELEASE(pusbdObj);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPUSBDeviceSendControlHandshake
 *
 *  Sends the appropriate control handshake to indicate the end of a
 *  control transaction.
 *
 *  Arguments:
 *      MTPUSBDEVICE    hUSBDevice          USB device to stall
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBDeviceSendControlHandshake(
                            MTPUSBDEVICECE hUSBDevice)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    USBDEVICECTX*   pusbdCtx;
    USBDEVICEOBJ*   pusbdObj = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == hUSBDevice)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the device information
     */

    pusbdCtx = (USBDEVICECTX*)hUSBDevice;
    PSLASSERT(USBDEVICECTX_COOKIE == pusbdCtx->dwCookie);

    /*  Acquire the mutex so that make sure that we update the object
     *  correctly
     */

    ps = PSLMutexAcquire(pusbdCtx->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pusbdCtx->hMutex;

    /*  Extract the device object to make sure that we are configured and
     *  guarantee that it sticks around while we release the mutex to
     *  deal with the hardware.
     */

    pusbdObj = (USBDEVICEOBJ*)pusbdCtx->pvRoot;
    if (PSLNULL == pusbdObj)
    {
        ps = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }
    SAFE_MTPUSBDEVICECEADDREF(pusbdObj);

    /*  Release the mutex so that driver can call back into us if necessary
     */

    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    /*  And stall the specified pipe
     */

    Nc2280_SendControlStatusHandShake();

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICECERELEASE(pusbdObj);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}
