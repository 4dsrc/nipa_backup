/*
 *  MTPUSBBufferMgr.h
 *
 *  Contains declarations of the Windows MTP USB buffer management functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPUSBBUFFERMGR_H_
#define _MTPUSBBUFFERMGR_H_

typedef PSLHANDLE MTPUSBBUFFERMGR;

/*
 *  MTP USB Buffer Counts
 *
 */

#define MTPUSB_MAX_BUFFERS                  8

#define MTPUSB_MAX_CONTROL_BUFFERS          4
#define MTPUSB_MAX_COMMAND_BUFFERS          1
#define MTPUSB_MAX_TRANSMIT_BUFFERS         3
#define MTPUSB_MAX_TRANSMIT_SHORT_BUFFERS   2
#define MTPUSB_MAX_RECEIVE_BUFFERS          3
#define MTPUSB_MAX_RESPONSE_BUFFERS         1
#define MTPUSB_MAX_EVENT_BUFFERS            2

#define MTPUSB_TOTAL_BUFFERS    (MTPUSB_MAX_CONTROL_BUFFERS + \
                                    MTPUSB_MAX_COMMAND_BUFFERS + \
                                    MTPUSB_MAX_TRANSMIT_BUFFERS + \
                                    MTPUSB_MAX_TRANSMIT_SHORT_BUFFERS +\
                                    MTPUSB_MAX_RECEIVE_BUFFERS + \
                                    MTPUSB_MAX_RESPONSE_BUFFERS + \
                                    MTPUSB_MAX_EVENT_BUFFERS)

/*
 *  MTP USB Maximum Data Chunk Size
 *
 */

#define MTPUSB_MAX_DATA_CHUNK_SIZE      0x8000

/*
 *  MTPUSBBufferMgrCreate
 *
 *  Creates a buffer object
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBBufferMgrCreate(
                            PSLUINT32 cbAlignment,
                            PSLUINT32 cbMaxControlSize,
                            PSLUINT32 cbMaxBulkSize,
                            PSLUINT32 cbMaxInterruptSize,
                            MTPUSBBUFFERMGR* pmtpusbbm);


/*
 *  MTPUSBBufferMgrDestroy
 *
 *  Destroys a buffer object
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBBufferMgrDestroy(
                            MTPUSBBUFFERMGR mtpusbbm);


/*
 *  MTPUSBBufferMgrRequest
 *
 *  Requests a buffer
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBBufferMgrRequest(
                            MTPUSBBUFFERMGR mtpusbbm,
                            PSLUINT32 dwBufferType,
                            PSLUINT32 cbRequested,
                            PSLPARAM aParam,
                            PSLVOID** pvBuffer,
                            PSLUINT32* pcbActual,
                            PSLMSGQUEUE mqAsync,
                            PSLMSGPOOL mpAsync);


/*
 *  MTPUSBBufferMgrCancelRequest
 *
 *  Cancels any pending buffer request for the type specified
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBBufferMgrCancelRequest(
                            MTPUSBBUFFERMGR mtpusbbm,
                            PSLUINT32 dwBufferType);


/*
 *  MTPUSBBufferMgrRelease
 *
 *  Returns a buffer
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBBufferMgrRelease(
                            PSLVOID* pvBuffer);


/*
 *  MTPUSBBufferMgrGetInfo
 *
 *  Returns information about a MTP USB buffer
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBBufferMgrGetInfo(
                            PSLVOID* pvBuffer,
                            MTPUSBBUFFERMGR* pmtpusbbm,
                            PSLUINT32* pcbSize,
                            PSLUINT32* pdwBufferType);

/*
 *  Safe Helpers
 *
 */

#define SAFE_MTPUSBBUFFERMGRDESTROY(_b) \
if (PSLNULL != (_b)) \
{ \
    MTPUSBBufferMgrDestroy(_b); \
    (_b) = PSLNULL; \
}

#define SAFE_MTPUSBBUFFERMGRRELEASE(_pv) \
if (PSLNULL != (_pv)) \
{ \
    MTPUSBBufferMgrRelease(_pv); \
    (_pv) = PSLNULL; \
}

#endif  /* _MTPUSBBUFFERMGR_H_ */

