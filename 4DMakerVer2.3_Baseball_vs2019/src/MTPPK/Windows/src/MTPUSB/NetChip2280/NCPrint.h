/******************************************************************************

Copyright (C) 2002, NetChip Technology, Inc. (http://www.netchip.com)

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.

NCPRINT.H
  
NetChip NET2270 PCI-RDK output functions header file

******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
#ifndef NCPRINT_H
#define NCPRINT_H

///////////////////////////////////////////////////////////////////////////////
// Define volume levels
//  - Applies to history logging and debug print
//  - History and print each have separate volume thresholds
#define VOLUME_NONE             0   // No volume: set thresholds here to *never* print or log *anything*
#define VOLUME_MINIMUM          1   // Minimum volume: set thresholds here to see serious or fatal errors
#define VOLUME_LOW              2   // Low volume: Set print threshold here to see messages without being overwhelmed
#define VOLUME_MEDIUM           3   // Medium volume
#define VOLUME_HIGH             4   // High volume: Set history threshold here to log events without being too overwhelmed
#define VOLUME_MAXIMUM          5   // Maximum volume: Set thresholds here to see all possible messages and logs

// Default volume levels for printing and history logging
//  - Generally, we want print volume lower than history volume
#define DEFAULT_PRINT           VOLUME_LOW
#define DEFAULT_HISTORY         VOLUME_HIGH

extern PCHAR VolumeMsg[];

///////////////////////////////////////////////////////////////////////////////
#ifdef _HISTO                       // If defined, historical events are logged
#define HISTO History

#define HistoPrintFormat        "%-4.4s  %8.8x  %8.8x  %8.8x"   // Text, Arg1, Arg2, Arg3

typedef struct _HISTORY_ENTRY
{   // A history event contains these components:
    dword Text;                 // Brief text message identifying a section of code
    dword Arg1;                 // Arguments can be anything meaningful to help debug (or zero)
    dword Arg2;
    dword Arg3;
} HISTORY_ENTRY, * PHISTORY_ENTRY;

#define HISTORY_ENTRIES 1000    // Keep this many historical entries

extern UINT gHistoryVolume;
extern HISTORY_ENTRY HistoBuf[];
extern dword HistoryCount;
#else
#define HISTO
#endif

extern UINT gPrintVolume;

///////////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
// Usage: Format string and parameters must be in parenthesis:
//  - NCPRINT(DEFAULT_PRINT, ("Hello, %s\n", "world"));
#define NCPRINT(vol, _x_) {if((vol)<=gPrintVolume)printf _x_;}
#else
#define NCPRINT(vol, _x_)
#endif //_DEBUG

///////////////////////////////////////////////////////////////////////////////
// use the system default functions for CoreMon
#define GetCh() getch()
#define KbHit() kbhit()

///////////////////////////////////////////////////////////////////////////////
void
DeleteLine(
	void
	);

///////////////////////////////////////////////////////////////////////////////
void
DeleteChar(
	int nCharToDel
	);

///////////////////////////////////////////////////////////////////////////////
#endif // NCPRINT_H

///////////////////////////////////////////////////////////////////////////////
//  End of file
///////////////////////////////////////////////////////////////////////////////
