/******************************************************************************

Copyright (C) 2002, NetChip Technology, Inc. (http://www.netchip.com)

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.

NC2280.C

Nc2280.C abstracts NetChip's NET2280 register-level interface to higher
level (and more useful) functionality, These functions include (but are not
limited to):
 - NET2280 initialization
 - Interrupt handlers(*)
 - USB setup request handlers
 - Data transfer routines (for arbitrary size transfers) 
(*)Interrupt entry points are elsewhere. The interrupt entry points quickly
dispatch to interrupt handlers contained in this file

The lower edge of Nc2280 interfaces directly to NET2280 registers

As distributed from NetChip Technology Inc., this file, when compiled with 
other required NetChip files, has been functionally tested on the NET2280 PCI-RDK.

******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
#include "NCCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// USB string descriptors (USD)
USB_STRING_DESCRIPTOR LanguageUsd = {0x04, 0x03, {0x0409} };
PUSB_STRING_DESCRIPTOR ManufacturerUsd = NULL;
PUSB_STRING_DESCRIPTOR ProductUsd = NULL;
PUSB_STRING_DESCRIPTOR SerialUsd = NULL;

///////////////////////////////////////////////////////////////////////////////
// Public variables
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Currently selected USB configuration number
//  - Set by Set Configuration handler
//  - Zero: deconfigured
//  - One or more: configured 
//  - Simple devices have only one configuration
BYTE UsbConfigurationNumber = 0;

// Currently selected USB configuration descriptors (High Speed and Full Speed)
//  - Set anytime after USB speed (HS/FS) is known, but before
//    returning configuration (Get Configuration Descriptor handler)
PUSB_CONFIGURATION_DESCRIPTOR UsbConfiguration = NULL;              // HS or FS
PUSB_CONFIGURATION_DESCRIPTOR UsbConfiguration_OtherSpeed = NULL;   // FS or HS

///////////////////////////////////////////////////////////////////////////////
// Transfer information (one for each endpoint)
EPXFERINFO EpXferInfo[ENDPOINT_COUNT];

///////////////////////////////////////////////////////////////////////////////
// Current USB frame
WORD CurrUsbFrame = 0;

///////////////////////////////////////////////////////////////////////////////
// Special printf() formatting to add volume controls to setup packets
char SetupPacket_Msg[256];       // Show setup packet message (During debug)
UINT SetupPacket_PrintVolume;    // 
#define SetupPacket_LengthFormat "%-32s Len:%4.4x"

///////////////////////////////////////////////////////////////////////////////
// Helper functions:
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
PUSB_STRING_DESCRIPTOR
Nc2280_BuildUsbStr(
char * AsciiStr
)
{   // Build a USB string descriptor (USD) from a NULL-terminated ascii string
    //  - Return a pointer to the USD
    //  - See USB 2.0: 9.6.7
    // This function malloc's memory for the USD:
    //  - BE SURE to free memory allocated by this function (use free())!
    //  - (The return pointer can be passed directly to free())
    UINT AsciiLen;
    PUSB_STRING_DESCRIPTOR Usd;
    PBYTE UsdText;
    UINT ii;

    if (AsciiStr == NULL)
    {   // No string to build!
        return NULL;
    }

    AsciiLen = (UINT)strlen(AsciiStr);

    // Determine length of USD (header plus wide string)
    ii = sizeof(USB_STRING_DESCRIPTOR) + (AsciiLen*sizeof(Usd->bString[0]));

    // Get storage for USD (header plus string storage)
    Usd = (PUSB_STRING_DESCRIPTOR)MALLOC(ii);
    ASSERT(Usd);
    if (Usd == NULL) return NULL;

    // Set USD header
    Usd->bLength = (BYTE)ii;                // USD length
    Usd->bDescriptorType = STRING_DESC;     // USD type

    // Fill in wide string
    UsdText = (PBYTE)&Usd->bString;         // Point to first byte of text in USD
    for (ii = 0; ii < AsciiLen; ii++)
    {   // Fill in USB descriptor string
        *(UsdText++) = *(AsciiStr++);
        *(UsdText++) = 0;
    }

    return Usd;
}

///////////////////////////////////////////////////////////////////////////////
BOOL
Nc2280_AreYouThere(
void
)
{   // Quick test to see if CPU can communicate properly with the NET2280
    //  - Verifies connection using writes and reads to write/read and read-only registers
    UINT ii;
    DWORD Val, RefVal;

    // Verify NET2280 write/read SCRATCH register can write and read
    RefVal = NETCHIP_READ_INDEXED(SCRATCH);
    for (ii = 0; ii < 0x100; ii += 7)
    {
        NETCHIP_WRITE_INDEXED(SCRATCH, ii);
        if ((Val = NETCHIP_READ_INDEXED(SCRATCH)) != ii)
        {
            NCPRINT(VOLUME_LOW, ("AreYouThere(): write/read SCRATCH register test failed: wrote:0x%8.8x, read:0x%8.8x\n",
            ii, Val));
            return FALSE;
        }
    }
    // To be nice, we put original value back
    NETCHIP_WRITE(SCRATCH, RefVal);

    // Verify NET2280 CHIPREV register is read-only:
    RefVal = NETCHIP_READ_INDEXED(CHIPREV);
    for (ii = 0; ii < 0x100; ii += 7)
    {
        NETCHIP_WRITE_INDEXED(CHIPREV, ii);
        if ((Val = NETCHIP_READ_INDEXED(CHIPREV)) != RefVal)
        {
            NCPRINT(VOLUME_LOW, ("AreYouThere(): write/read CHIPREV register test failed: wrote 0x%8.8x, read:0x%8.8x expected:0x%8.8x\n",
            ii, Val, RefVal));
            return FALSE;
        }
    }

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
BOOL
Nc2280_FindLocalEp(
BYTE UsbEp,
PBYTE LocalEp
)
{   // Find local NET2280 endpoint matching a USB endpoint (including endpoint zero)
    // If matching endpoint found, returns local endpoint ordinal (EP0: 0, EPA: 1, ...)
    for (*LocalEp = 0; *LocalEp <= LAST_USER_ENDPOINT; (*LocalEp)++)
    {
        if ((BYTE)(NETCHIP_READ(EPPAGEOFFSET(*LocalEp) + EP_CFG)) == UsbEp)
        {   // Found USB endpoint matching local endpoint
            return TRUE;
        }
    }
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////
// Find the USB endpoint addr for a NET2280 local endpoint (EP0, EPA, ...) 
BYTE
Nc2280_LocalEpToUsbEp(
BYTE LocalEp
)
{
    // Lower 8 bits of EP_CFG contains the Endpoint address
    return(BYTE)(NETCHIP_READ(EPPAGEOFFSET(LocalEp) + EP_CFG));
}


///////////////////////////////////////////////////////////////////////////////
BOOL
Nc2280_OneTimeInit(
void
)
{   // One-time initialization for NET2280 and related systems
    // Should be called *after* chip reset, but *before* other chip accesses
    //  - Quick-verify of NET2280 chip accessibility
    //  - Builds common USB string descriptors (from simple ASCII strings)
    //
    // Make sure NET2280 is accessible
    if (!Nc2280_AreYouThere())
    {   // Unable to access NET2280
        return FALSE;
    }

    NETCHIP_WRITE(STDRSP, 0);

    // Build USB string descriptors from ASCII strings
    ASSERT(NcDevice != NULL);
    ProductUsd = Nc2280_BuildUsbStr(NcDevice->Product);
    ManufacturerUsd = Nc2280_BuildUsbStr(NcDevice->Manufacturer);
    SerialUsd = Nc2280_BuildUsbStr(NcDevice->Serial);

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
BOOL
Nc2280_SetConfiguration(
BYTE RequestedConfig
)
{   // USB host is configuring (or de-configuring) our device (See Set Configuration)
    PTRANSFER_CONFIGURATION TransferConfiguration = (PTRANSFER_CONFIGURATION)UsbConfiguration;
    BYTE Ep;
    
    if (UsbConfigurationNumber != RequestedConfig)
    {
        switch (RequestedConfig)
        {
        case 1: // Configuration 1:
            // We are being configured
            //  - A configured device can draw up to 500mA from the upstream USB connection
            // Program user (non-control) endpoints using endpoint descriptors
            //  - Endpoint descriptors are zero-based entries in configuration
            ASSERT(UsbConfiguration != NULL);

            // Disable user (non-control) endpoints
            for (Ep = FIRST_USER_ENDPOINT; Ep <= LAST_USER_ENDPOINT; Ep++)
            {   // For all user endpoints (i.e. all non-control endpoints):
                Nc2280_DisableEndpoint(Ep);
            }

            // Configure OUT endpoint
            ASSERT((TransferConfiguration->EndpointDescriptor[0].bEndpointAddress & (1<<EP_DIRECTION_IN)) == EP_OUT);
            Nc2280_InitializeEndpoint(MTP_BULKOUT, &TransferConfiguration->EndpointDescriptor[0]);
            NETCHIP_WRITE(EPPAGEOFFSET(MTP_BULKOUT) + EP_IRQENB,
            //(1<<DATA_PACKET_RECEIVED_INTERRUPT_ENABLE) |  
            0);                                             // OUT endpoint interrupt enable

            // Configure IN endpoint
            ASSERT((TransferConfiguration->EndpointDescriptor[1].bEndpointAddress & (1<<EP_DIRECTION_IN)) == EP_IN);
            Nc2280_InitializeEndpoint(MTP_BULKIN, &TransferConfiguration->EndpointDescriptor[1]);
            NETCHIP_WRITE(EPPAGEOFFSET(MTP_BULKIN) + EP_IRQENB, 
            //(1<<DATA_IN_TOKEN_INTERRUPT_ENABLE) |
            0);                                             // IN endpoint interrupt enable

            // Configure INTERRUPT endpoint
            ASSERT((TransferConfiguration->EndpointDescriptor[2].bEndpointAddress & (1<<EP_DIRECTION_IN)) == EP_IN);
            Nc2280_InitializeEndpoint(MTP_INTRIN, &TransferConfiguration->EndpointDescriptor[2]);
            NETCHIP_WRITE(EPPAGEOFFSET(MTP_INTRIN) + EP_IRQENB, 
            //(1<<DATA_IN_TOKEN_INTERRUPT_ENABLE) |
            0);       
            break;

        case 0: // Configuration 0
            // We are being de-configured
            //  - A deconfigured device can draw no more than 100mA from upstream USB connection (hub)

            // Disable user (non-control) endpoints
            for (Ep = FIRST_USER_ENDPOINT; Ep <= LAST_USER_ENDPOINT; Ep++)
            {   // For all user endpoints (i.e. all non-control endpoints):
                Nc2280_DisableEndpoint(Ep);
            }
            break;

        default:
            // No handler for requested configuration
            NCPRINT(VOLUME_MINIMUM, ("Transfer_ConfigurationHandler(): No configuration handler for configuration:%d\n", RequestedConfig));
            return FALSE;
        }

        // Successfully handled configuration request
	    ASSERT(RequestedConfig <= NcDevice->DeviceDescriptor->bNumConfigurations);
        UsbConfigurationNumber = RequestedConfig;

        NcDevice->lpfnNotify(NcDevice->pvNotifyParameter, UFN_MSG_CONFIGURED, RequestedConfig );
    }

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_CleanUp(
void
)
{   // Prepare NET2280 subsystem for shutdown (or bringup)
    UINT ii;

    Nc2280_SetConfiguration(0);

    // Remove ourselves from the USB
    NETCHIP_WRITE(USBCTL, NETCHIP_READ(USBCTL) & ~(1<<USB_DETECT_ENABLE));
    NETCHIP_WRITE(PCIIRQENB0, 0x00);
    NETCHIP_WRITE(PCIIRQENB1, 0x00);

    for (ii = 0; ii < ENDPOINT_COUNT; ii++)
    {
        EpXferInfo[ii].State = TransferState_NoState;
        EpXferInfo[ii].CompleteXfer  = FALSE;
        EpXferInfo[ii].XferCancelled = FALSE;
    }
}

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_InitializeEp0(
void
)
{   // Configure NET2280 Endpoint Zero
    //  - Enable setup packets and endpoint zero interrupts
    DWORD Ep0Page = EPPAGEOFFSET(EP0);


    // Turn off Hide Status Phase and NAK OUT Packets Mode features for EP0
    //  - NAK OUT Packets Mode is an OUT data traffic synchronization feature that does
    // not apply to EP0. (Status Phase achieves the same result.) (Note that a
    // short data phase packet of a Control Write or the Status Phase of a Control
    // Read will set the NAK OUT Packets bit.)

    NETCHIP_WRITE(Ep0Page + EP_RSP,
    (1<<CLEAR_EP_HIDE_STATUS_PHASE) |
    (1<<CLEAR_NAK_OUT_PACKETS_MODE) |
    0);


    DEBUG(EpXferInfo[EP0].XferCount = 0);
    EpXferInfo[EP0].MaxPktSize = EP0_MAX_PACKET_SIZE;
    EpXferInfo[EP0].State = TransferState_Ready;
    EpXferInfo[EP0].CompleteXfer  = FALSE;
    EpXferInfo[EP0].XferCancelled = FALSE;
    EpXferInfo[EP0].pvXferNotifyParam = NULL;

    // Finally, enable Setup Packet and Endpoint 0 interrupts at the NET2280
    //  - If a Setup Packet is pending, the NET2280 INTA# pin will assert
    //  - For Endpoint 0 to interrupt, appropriate bits in EP_IRQENB must also be set
    NETCHIP_WRITE(PCIIRQENB0,
    NETCHIP_READ(PCIIRQENB0) |
    (1<<SETUP_PACKET_INTERRUPT_ENABLE) |
    (1<<ENDPOINT_0_INTERRUPT_ENABLE) |
    0);
}

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_InitializeEndpoint(
BYTE Ep,                    // NET2280 endpoint (EPA, EPB, ...)
USB_ENDPOINT_DESCRIPTOR const * EpDescriptor
)
{   // Program a NET2280 endpoint according to a USB endpoint descriptor
    //  - Does not apply to endpoint zero
    //  - Endpoint interrupt is enabled in PCIIRQENB0, but *not* in the endpoint's EP_IRQENB

    DWORD EpPage = EPPAGEOFFSET(Ep);

    NETCHIP_WRITE(EpPage + EP_STAT,
    (1<<NAK_OUT_PACKETS) |
    (1<<SHORT_PACKET_TRANSFERRED_INTERRUPT) |
    (1<<DATA_PACKET_RECEIVED_INTERRUPT) |
    (1<<DATA_PACKET_TRANSMITTED_INTERRUPT) |
    (1<<DATA_OUT_PING_TOKEN_INTERRUPT) |
    (1<<DATA_IN_TOKEN_INTERRUPT) |
    0);

    EpXferInfo[Ep].MaxPktSize = (WORD)((EpDescriptor->wMaxPacketSizeHi<<8) + (EpDescriptor->wMaxPacketSizeLo<<0));

    // Setup endpoint's enable, type, direction, USB endpoint, etc:
    NETCHIP_WRITE(EpPage + EP_CFG,
    (1<<ENDPOINT_ENABLE) |
    (EpDescriptor->bmAttributes<<ENDPOINT_TYPE) |
    ((EpDescriptor->bEndpointAddress & EP_IN)? 
    (1<<ENDPOINT_DIRECTION): 
    (0<<ENDPOINT_DIRECTION)) |
    (EpDescriptor->bEndpointAddress & 0x0f)     // ENDPOINT_NUMBER
    );

    // Enabling NAK OUT Packets Mode causes the endpoint to automatically 
    // NAK (or NYET) packets receiving a short packet. This feature 
    // synchronizes USB traffic with firmware.
    //  - NAK OUT Packets Mode is ignored by IN endpoints
    NETCHIP_WRITE(EpPage + EP_RSP, 1<<SET_NAK_OUT_PACKETS_MODE);

    // Always set Buffer Flush after configuring endpoint
    NETCHIP_WRITE(EpPage + EP_STAT, 1<<FIFO_FLUSH);
    DEBUG(EpXferInfo[Ep].XferCount = 0);

    EpXferInfo[Ep].State = TransferState_Ready;
    EpXferInfo[Ep].Dir = (BYTE)((EpDescriptor->bEndpointAddress & EP_IN)? EP_IN: EP_OUT);
    EpXferInfo[Ep].CompleteXfer  = FALSE;
    EpXferInfo[Ep].XferCancelled = FALSE;
    EpXferInfo[Ep].pvXferNotifyParam = NULL;

    // Finally, enable endpoint's interrupt at IRQENB0
    //  - For the endpoint to interrupt, appropriate bits in 
    //    the endpoint's EP_IRQENB must also be set
    //  - Generally, it is safer to dynamically control an endpoint's 
    //    interrupt enables using the endpoint's EP_IRQENB
    NETCHIP_WRITE(PCIIRQENB0, NETCHIP_READ(PCIIRQENB0) | (1<<Ep));
}


///////////////////////////////////////////////////////////////////////////////
void
Nc2280_DisableEndpoint(
BYTE Ep                     // NET2280 endpoint (EPA, EPB, ...)
)
{
    DWORD EpPage = EPPAGEOFFSET(Ep);

    NETCHIP_WRITE(EpPage + EP_IRQENB, 0);
    NETCHIP_WRITE(EpPage + EP_CFG, 0);
    EpXferInfo[Ep].State = TransferState_NoState;
}

BOOL
Nc2280_IsEndpointStalled(
BYTE Ep                     // NET2280 endpoint (EPA, EPB, ...)
)
{
    // check the Endpoint Halt bit of EP_RSP on endpoint specified by Ep
    return((NETCHIP_READ(EPPAGEOFFSET(Ep) + EP_RSP) & (1<<CLEAR_ENDPOINT_HALT)) == 0) ? FALSE : TRUE;
}

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_SendControlStatusHandShake(void)
{
    if ( ! Nc2280_IsEndpointStalled(EP0) )
    {
        NETCHIP_WRITE(EP_RSP,
        (1<<CLEAR_CONTROL_STATUS_PHASE_HANDSHAKE) |
        0);
    }
}

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_StallEndpoint(
BYTE Ep                     // NET2280 endpoint (EPA, EPB, ...)
)
{
    // Set the Endpoint Stall on endpoint specified by Ep
    if (Nc2280_IsEndpointStalled(Ep) ==  FALSE)
    {
        NETCHIP_WRITE(EPPAGEOFFSET(Ep) + EP_RSP, 1<<SET_ENDPOINT_HALT);
    }
}

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_UnstallEndpoint(
BYTE Ep                     // NET2280 endpoint (EPA, EPB, ...)
)
{
    if (Nc2280_IsEndpointStalled(Ep))
    {
        // Clear the Endpoint Stall on endpoint specified by Ep
        NETCHIP_WRITE(EPPAGEOFFSET(Ep) + EP_RSP, (1<<CLEAR_ENDPOINT_TOGGLE) | (1<<CLEAR_ENDPOINT_HALT));
    }
}

///////////////////////////////////////////////////////////////////////////////
BOOL
Nc2280_IsDeviceConnected(void)
{
    //
    // if ((NETCHIP_READ(USBCTL) & (1<<VBUS_PIN)) == 0): VBUS Pin low -- No USB cable?
    // USB cable does not appear to be connected to a USB host
    //
    return((NETCHIP_READ(USBCTL) & (1<<VBUS_PIN)) == 0) ? FALSE : TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// Disconnect Nc2280 from USB Host and re-connect after 1 second
void
Nc2280_Disconnect(
void
)
{
    NETCHIP_WRITE(USBCTL, 1<<TIMED_DISCONNECT);
}


///////////////////////////////////////////////////////////////////////////////
// Reconnect Nc2280 to USB Host
void
Nc2280_Reconnect(
void
)
{
    NETCHIP_WRITE(USBCTL, 1<<USB_DETECT_ENABLE);
}

///////////////////////////////////////////////////////////////////////////////
BOOL
Nc2280_IsDataAvailable(
BYTE Ep                     // NET2280 endpoint (EPA, EPB, ...)
)
{
    DWORD EpPage = EPPAGEOFFSET(Ep);
    UINT  EpAvail;

    EpAvail = NETCHIP_READ(EpPage + EP_AVAIL);
    return(EpAvail != 0);
}

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_TestMode(
UINT TestMode
)
{   // Place NET2280 into a test mode (USBTEST)
    //  - USB 2.0 devices are required to support Test Modes
    //  - We are usually called from setup request handler (host-initiated test mode) 
    //    but for special testing we can also be called from main(), (i.e. with no host connection)
    static BYTE UsbTestPacket[] =
    {   // Packet data required for Test_Packet test mode
        //  - USB test packet (See TEST_MODE, TEST_PACKET, USB 2.0: 7.1.20)
        //  - See NET2280 spec "USB Test Modes" to verify correct content and length
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,                   // JKJKJKJK * 9
        0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,                         // JJKKJJKK * 8
        0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE,                         // JJJJKKKK * 8
        0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // JJJJJJJKKKKKKK * 8
        0x7F, 0xBF, 0xDF, 0xEF, 0xF7, 0xFB, 0xFD,                               // JJJJJJJK * 8
        0xFC, 0x7E, 0xBF, 0xDF, 0xEF, 0xF7, 0xFB, 0xFD, 0x7E                    // {JKKKKKKK * 10}, JK
    };
    UINT ii;
    UINT DwordCount = sizeof(UsbTestPacket)/sizeof(DWORD);
    PDWORD pTestPacket = (PDWORD)UsbTestPacket;
    PDWORD pEpData = (PDWORD)(NETCHIP_BASEADDRESS + EP_DATA);
    PWORD pEpByteCountReg = (PWORD)(NETCHIP_BASEADDRESS + EP_BYTE_COUNT);

    // Disable all NET2280 interrupts
    //  - USB 2.0: 7.1.20: "... the exit action is to power cycle the device." In other
    //    words, nothing (even a cable unplug) should stop a test mode test.
    NETCHIP_WRITE(PCIIRQENB0, 0x00);
    NETCHIP_WRITE(PCIIRQENB1, 0x00);

    NETCHIP_WRITE(EP_STAT, (1<<DATA_PACKET_TRANSMITTED_INTERRUPT));
    NETCHIP_WRITE(EP_RSP,
    (1<<CLEAR_CONTROL_STATUS_PHASE_HANDSHAKE) |
    (1<<CLEAR_EP_HIDE_STATUS_PHASE) |
    0);

    // Wait a reasonable interval for status phase to complete
    for (ii = 5; ii; ii--)
    {
        if (NETCHIP_READ(EP_STAT) & (1<<DATA_PACKET_TRANSMITTED_INTERRUPT))
        {   // Status phase complete
            break;
        }
        Sleep(100);
    }

    NETCHIP_WRITE(EP_CFG, (1<<ENDPOINT_DIRECTION)); // Force direction to IN 

    // Start USB test mode
    NETCHIP_WRITE(XCVRDIAG, NETCHIP_READ(XCVRDIAG) | (TestMode<<USB_TEST_MODE));
    NETCHIP_WRITE(EP_STAT, (1<<FIFO_FLUSH));

    if (TestMode == USB_TEST_PACKET)
    {   // Load test packet
        while (DwordCount--)
        {
            *(pEpData) = *(pTestPacket++);
        }

        // Validate test packet
        *(pEpByteCountReg) = (WORD)(sizeof(UsbTestPacket) % 4);
        *(pEpData) = *(pTestPacket++);
    }
}


///////////////////////////////////////////////////////////////////////////////
// Setup Request support routines
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_HsFsDescriptorAdjust(
void
)
{   // Adjust descriptors as necessary for High Speed (USB 2.0) or Full Speed (USB 1.1)
    UFN_BUS_SPEED FnSpeed;
    DWORD UsbSpeed = NETCHIP_READ(USBSTAT) & (BYTE)((1<<HIGH_SPEED) | (1<<FULL_SPEED));

    // We must be running at exactly one speed or the other
    ASSERT((UsbSpeed == (1<<HIGH_SPEED)) | (UsbSpeed == (1<<FULL_SPEED)));

    if (UsbSpeed & (1<<HIGH_SPEED))
    {   // USB is in High Speed mode
        UsbConfiguration = (PUSB_CONFIGURATION_DESCRIPTOR)NcDevice->HS_Configuration;
        UsbConfiguration_OtherSpeed = (PUSB_CONFIGURATION_DESCRIPTOR)NcDevice->FS_Configuration;
        FnSpeed = BS_HIGH_SPEED;
    }
    else
    {   // USB is in Full Speed mode
        UsbConfiguration = (PUSB_CONFIGURATION_DESCRIPTOR)NcDevice->FS_Configuration;
        UsbConfiguration_OtherSpeed = (PUSB_CONFIGURATION_DESCRIPTOR)NcDevice->HS_Configuration;
        FnSpeed = BS_FULL_SPEED;
    }

    // Adjust descriptor types
    UsbConfiguration->bDescriptorType = CONFIGURATION_DESC;
    UsbConfiguration_OtherSpeed->bDescriptorType = OTHER_SPEED_CONFIGURATION_DESC;

    if (! NcDevice->fReportedSpeed)
    {
        NcDevice->lpfnNotify(NcDevice->pvNotifyParameter, UFN_MSG_BUS_SPEED, FnSpeed);
        NcDevice->fReportedSpeed = TRUE;
    }

    HISTO(DEFAULT_HISTORY, "HsFs", UsbSpeed, PtrToUlong(UsbConfiguration), PtrToUlong(UsbConfiguration_OtherSpeed));
}

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_StandardUsbRequestHandler(
USB_SETUP_PACKET* pUdr
)
{   // Standard endpoint zero request handler
    // Massive 'switch()' to handle request packet
    //  - If the request is not handled, endpoint zero is stalled!
    //  - We assume Setup Packet has been copied from NET2280 to local memory
    static WORDBYTE lewStatus;    // Return status - used as a Little Endian Word
    UINT Length;
    BYTE Ep;
    lewStatus.Word = 0x0000;

    switch (pUdr->bmRequestType)
    {   // This is a reasonably complete skeleton for handling USB setup requests
        //  - Successful requests should 'return'
        //  - Failed or unhandled requests should 'break' causing endpoint zero to STALL
     case HOST_TO_DEVICE | STANDARD | RECIPIENT_DEVICE:
        switch (pUdr->bRequest)
        {
        case CLEAR_FEATURE:
            // Clear DEVICE Feature
            if (pUdr->wValue == DEVICE_REMOTE_WAKEUP)
            {   // Device Remote Wakeup
                NETCHIP_WRITE(USBCTL, NETCHIP_READ(USBCTL) & (~(1<<DEVICE_REMOTE_WAKEUP_ENABLE)));
                DEBUG(sprintf_s(SetupPacket_Msg, 256, "CLEAR_FEATURE, DEVICE_REMOTE_WAKEUP"));
                return;
            }
            break;
        case SET_FEATURE:
            // Set DEVICE Feature
            switch (pUdr->wValue)
            {
            case DEVICE_REMOTE_WAKEUP:
                DEBUG(sprintf_s(SetupPacket_Msg, 256, "SET_FEATURE, DEVICE_REMOTE_WAKEUP"));
                NETCHIP_WRITE(USBCTL, NETCHIP_READ(USBCTL) | (1<<DEVICE_REMOTE_WAKEUP_ENABLE));
                return;

            case TEST_MODE:
                // See USB 2.0: 7.1.20 and 9.4.9 
                //  - USB 2.0: 7.1.20: "The transition to test mode must complete no later than 3ms after
                //    the completion of the status stage of the request." In other words, we don't have 
                //    time to print messages before starting a test mode
                switch (HIBYTE(pUdr->wIndex))
                {
                case USB_TEST_J:
                case USB_TEST_K:
                case USB_TEST_SE0_NAK:
                case USB_TEST_FORCE_ENABLE:
                case USB_TEST_PACKET:
                    DEBUG(SetupPacket_PrintVolume = VOLUME_LOW);
                    DEBUG(sprintf_s(SetupPacket_Msg, 256, "SET_FEATURE, TEST_MODE (Mode:0x%2.2x)", HIBYTE(pUdr->wIndex)));

                    Nc2280_TestMode(HIBYTE(pUdr->wIndex));
                    return;
                default:
                    break;
                }
                break;
            default:
                break;
            }
            break;
        case SET_ADDRESS:
            // NOTE: USB address changes *after* successful status stage (See USB 1.1: 9.4.6)
            //  - The NET2280 automatically changes the address after status stage
            //  - The address we write now will not updated until *after* the status phase
            //  - USB 2.0: 9.2.6.3: Within 2ms after the status stage of Set Address, the device
            //    must be able to accept packets at the new address. (This can be a gotcha!)
            NETCHIP_WRITE(OURADDR, LOBYTE(pUdr->wValue));
            DEBUG(SetupPacket_PrintVolume = VOLUME_LOW);
			DEBUG(sprintf_s(SetupPacket_Msg, 256, "Setting OURADDR:%2.2x", LOBYTE(pUdr->wValue)));
            return;
        case SET_CONFIGURATION:
            // Set Configuration
            Nc2280_SetConfiguration( LOBYTE(pUdr->wValue) );
            return;
        default:
            break;
        }
        break;
    case HOST_TO_DEVICE | STANDARD | RECIPIENT_INTERFACE:
        switch (pUdr->bRequest)
        {
        case CLEAR_FEATURE:
            // Feature not supported - add code if desired
            break;
        case SET_FEATURE:
            // Feature not supported - add code if desired
            break;
        case SET_INTERFACE:
            // Set Interface
            break;      
        default:
            break;
        }
        break;
    case HOST_TO_DEVICE | STANDARD | RECIPIENT_ENDPOINT:
        switch (pUdr->bRequest)
        {
        case CLEAR_FEATURE:
            // Clear Endpoint Feature
            if (pUdr->wValue != ENDPOINT_STALL)
            {   // Expected wValue to be ENDPOINT_STALL
                break;
            }
            if (!Nc2280_FindLocalEp(LOBYTE(pUdr->wIndex), &Ep))
            {   // Unable to find local endpoint matching requested endpoint
                break;
            }

            // Clear the Endpoint Stall on endpoint specified by wIndex
            NETCHIP_WRITE(EPPAGEOFFSET(Ep) + EP_RSP, (1<<CLEAR_ENDPOINT_TOGGLE) | (1<<CLEAR_ENDPOINT_HALT));
			DEBUG(sprintf_s(SetupPacket_Msg, 256, "CLEAR_FEATURE(STALL): USB Ep:%2.2x)", LOBYTE(pUdr->wIndex)));
            return;
        case SET_FEATURE:
            // Set Endpoint Feature
            if (pUdr->wValue != ENDPOINT_STALL)
            {   // Expected wValue to be ENDPOINT_STALL
                break;
            }
            if (!Nc2280_FindLocalEp(LOBYTE(pUdr->wIndex), &Ep))
            {   // Unable to find local endpoint matching requested endpoint
                break;
            }

            // Set the Endpoint Stall on endpoint specified by wIndex
            NETCHIP_WRITE(EPPAGEOFFSET(Ep) + EP_RSP, 1<<SET_ENDPOINT_HALT);
            DEBUG(sprintf_s(SetupPacket_Msg, 256, "SET_FEATURE(STALL): USB Ep:%2.2x)", LOBYTE(pUdr->wIndex)));
            return;
        case SYNCH_FRAME:
            // Feature not supported - add code if desired
            break;
        default:
            break;
        }
        break;
    case DEVICE_TO_HOST | STANDARD | RECIPIENT_DEVICE:
        switch (pUdr->bRequest)
        {
        case GET_STATUS:
            // Get RECIPIENT_DEVICE Status - USB expects Little Endian status word containing:
            //  - Bit 1: Remote Wakeup enable
            //  - Bit 0: Self Powered
            lewStatus.Byte[LITTLEEND_LO] = (BYTE)(NETCHIP_READ(USBCTL) & (1<<DEVICE_REMOTE_WAKEUP_ENABLE));
            lewStatus.Byte[LITTLEEND_LO] |= (1<<0);  // For now, always enumerate as Self Powered
            Nc2280_KickTxXfer(EP0, &lewStatus, sizeof(lewStatus), TRUE);
            DEBUG(sprintf_s(SetupPacket_Msg, 256, "GET_STATUS [DEVICE]: %4.4x", lewStatus.Word));
            return;
        case GET_CONFIGURATION:
            // Return the current configuration number
            Nc2280_KickTxXfer(EP0, &UsbConfigurationNumber, sizeof(UsbConfigurationNumber), TRUE);
            DEBUG(sprintf_s(SetupPacket_Msg, 256, "GET_CONFIGURATION (%d)", UsbConfigurationNumber));
            return;
        case GET_DESCRIPTOR:
            switch (HIBYTE(pUdr->wValue))
            {   // Hi byte of wValue specifies the descriptor type (RECIPIENT_DEVICE, CONFIGURATION, STRING, etc.)
            case DEVICE_DESC:
                // "Get Device Descriptor" request
                Length = min(pUdr->wLength, NcDevice->DeviceDescriptor->bLength);
                Nc2280_KickTxXfer(EP0, NcDevice->DeviceDescriptor, Length, TRUE);
                DEBUG(sprintf_s(SetupPacket_Msg, 256, SetupPacket_LengthFormat, "GET_DESCRIPTOR [DEVICE]", Length));
                return;
            case DEVICE_QUALIFIER_DESC:
                // "Get Device Qualifier Descriptor" request
                Length = min(pUdr->wLength, NcDevice->DeviceQualifierDescriptor->bLength);
                Nc2280_KickTxXfer(EP0, NcDevice->DeviceQualifierDescriptor, Length, TRUE);
                DEBUG(sprintf_s(SetupPacket_Msg, 256, SetupPacket_LengthFormat, "GET_DESCRIPTOR [DEVICE_QUALIFIER]", Length));
                return;
            case CONFIGURATION_DESC:
                // "Get Configuration" request
                Nc2280_HsFsDescriptorAdjust();
                ASSERT(UsbConfiguration != NULL);
                ASSERT(UsbConfiguration->bDescriptorType == CONFIGURATION_DESC);
                ASSERT(UsbConfiguration->wTotalLengthHi == 0);
                Length = min(pUdr->wLength, UsbConfiguration->wTotalLengthLo);
                Nc2280_KickTxXfer(EP0, UsbConfiguration, Length, TRUE);
                DEBUG(sprintf_s(SetupPacket_Msg, 256, SetupPacket_LengthFormat, "GET_DESCRIPTOR [CONFIGURATION]", Length));
                return;
            case OTHER_SPEED_CONFIGURATION_DESC:
                // "Get Other Speed Configuration Descriptor" request
                Nc2280_HsFsDescriptorAdjust();
                ASSERT(UsbConfiguration_OtherSpeed != NULL);
                ASSERT(UsbConfiguration_OtherSpeed->bDescriptorType == OTHER_SPEED_CONFIGURATION_DESC);
                ASSERT(UsbConfiguration_OtherSpeed->wTotalLengthHi == 0);
                Length = min(pUdr->wLength, UsbConfiguration_OtherSpeed->wTotalLengthLo);
                Nc2280_KickTxXfer(EP0, UsbConfiguration_OtherSpeed, Length, TRUE);
                DEBUG(sprintf_s(SetupPacket_Msg, 256, SetupPacket_LengthFormat, "GET_DESCRIPTOR [OTHER_SPEED_CONFIGURATION]", Length));
                return;
            case STRING_DESC:
                // "Get String Descriptor" request
                if (LOBYTE(pUdr->wValue) == 0x00)
                {   // Request is for LanguageID string
                    Length = min(LanguageUsd.bLength, pUdr->wLength);
                    Nc2280_KickTxXfer(EP0, (PBYTE)&LanguageUsd, Length, TRUE);
                    DEBUG(sprintf_s(SetupPacket_Msg, 256, SetupPacket_LengthFormat, "GET_DESCRIPTOR [STRING, Lang]", Length));
                    return;
                }
                if (LOBYTE(pUdr->wValue) == NcDevice->DeviceDescriptor->iManufacturer)
                {   // Request is for Manufacturer string
                    ASSERT(ManufacturerUsd != NULL);
                    Length = min(ManufacturerUsd->bLength, pUdr->wLength);
                    Nc2280_KickTxXfer(EP0, (PBYTE)ManufacturerUsd, Length, TRUE);
                    DEBUG(sprintf_s(SetupPacket_Msg, 256, SetupPacket_LengthFormat, "GET_DESCRIPTOR [STRING, iManf]", Length));
                    return;
                }
                if (LOBYTE(pUdr->wValue) == NcDevice->DeviceDescriptor->iProduct)
                {   // Request is for Product string
                    ASSERT(ProductUsd != NULL);
                    Length = min(ProductUsd->bLength, pUdr->wLength);
                    Nc2280_KickTxXfer(EP0, (PBYTE)ProductUsd, Length, TRUE);
                    DEBUG(sprintf_s(SetupPacket_Msg, 256, SetupPacket_LengthFormat, "GET_DESCRIPTOR [STRING, iProd]", Length));
                    return;
                }
                if (LOBYTE(pUdr->wValue) == NcDevice->DeviceDescriptor->iSerialNumber)
                {   // Request is for Serial Number string
                    //  - If iSerialNumber is zero, we will never get here (see LanguageID)
                    ASSERT(SerialUsd != NULL);
                    Length = min(SerialUsd->bLength, pUdr->wLength);
                    Nc2280_KickTxXfer(EP0, (PBYTE)SerialUsd, Length, TRUE);
                    DEBUG(sprintf_s(SetupPacket_Msg, 256, SetupPacket_LengthFormat, "GET_DESCRIPTOR [STRING, iSerial]", Length));
                    return;
                }
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
        break;
    case DEVICE_TO_HOST | STANDARD | RECIPIENT_INTERFACE:
        switch (pUdr->bRequest)
        {
        case GET_STATUS:
            // Return interface status 
            //  - Always zero. See USB 2.0: 9.4.5 (fig 9-5)
            Nc2280_KickTxXfer(EP0, &lewStatus, pUdr->wLength, TRUE);
            DEBUG(sprintf_s(SetupPacket_Msg, 256, "GET_STATUS: [INTERFACE]: %4.4x", lewStatus.Word));
            return;
        case GET_INTERFACE:
            // Get Interface
			break;
        default:
            break;
        }
        break;
    case DEVICE_TO_HOST | STANDARD | RECIPIENT_ENDPOINT:
        switch (pUdr->bRequest)
        {
        case GET_STATUS:
            // Return status of endpoint specified in wIndex
			if (!Nc2280_FindLocalEp(LOBYTE(pUdr->wIndex), &Ep))
            {   // Unable to find local endpoint matching requested endpoint
                break;
            }

            // Return status of endpoint specified in wIndex
            lewStatus.Byte[LITTLEEND_LO] = (BYTE)(NETCHIP_READ(EPPAGEOFFSET(Ep) + EP_RSP) & (1<<CLEAR_ENDPOINT_HALT));
            Nc2280_KickTxXfer(EP0, &lewStatus, sizeof(lewStatus), TRUE);
            DEBUG(sprintf_s(SetupPacket_Msg, 256, "GET_STATUS: USB Ep:%2.2x: %4.4x", LOBYTE(pUdr->wIndex), lewStatus.Word));
            return;
        default:
            break;
        }
        break;
    default:
        break;
    }

    // Nobody handled the request - stall endpoint zero
    NETCHIP_WRITE(EP_RSP, 1<<SET_ENDPOINT_HALT);
    NCPRINT(VOLUME_MINIMUM, ("Nc2280_IntEp0_Request(): Stalled! Setup Packet:"
    "%2.2x %2.2x %4.4x %4.4x %4.4x\n",
    pUdr->bmRequestType,
    pUdr->bRequest,
    pUdr->wValue,
    pUdr->wIndex,
    pUdr->wLength
    ));
    HISTO(VOLUME_MINIMUM, "SpS!", (pUdr->bmRequestType<<8) | (pUdr->bRequest<<0), pUdr->wValue, pUdr->wIndex);
}

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_SetupPacketInterrupt(
void
)
{   // Handle Setup Packet Interrupt
    // Upon ACKing a Setup Packet, the NET2280 will:
    //  - Set Setup Packet Interrupt (which is how we got here)
    //  - Set EP0 Endpoint Direction to match bit 7 of bmRequestType
    //  - Set EP0 Endpoint Toggle (Toggle always set for first Data Phase packet)
    //  - Set EP0 Control Status Phase Handshake (must be cleared by firmware 
    //    to allow the Status Phase to complete)
    //  - Clear EP0 Endpoint Halt (guarantees the Setup Request will be ACK'd)
    //  - Clear EP0 data buffers (resets pointers so buffers appear empty)
    // See USB 2.0: 8.5.3 for details about setup packets and control transfers

    USB_SETUP_PACKET udr;

    PDWORD pdwUdr = (PDWORD)&udr;
    
    // Several EP_STAT bits may be set as a result of a previous control transfer.
    // They must be cleared to prevent erroneous interpretations by firmware:
    //  - Short Packet Transferred Interrupt
    //  - Data IN Token Interrupt
    //  - Data OUT Ping Token Interrupt
    NETCHIP_WRITE(EP_STAT,
    (1<<SHORT_PACKET_TRANSFERRED_INTERRUPT) |
    (1<<DATA_IN_TOKEN_INTERRUPT) |
    (1<<DATA_OUT_PING_TOKEN_INTERRUPT) |
    0);

    *pdwUdr = NETCHIP_READ(SETUP0123);
    *( pdwUdr + 1 ) = NETCHIP_READ(SETUP4567);

    //NCPRINT(VOLUME_MINIMUM, (" Setup Package received: bmRequestType=%x, bRequest=%x\n", bmRequestType, bRequest));

    // Allow another Setup Packet Interrupt
    NETCHIP_WRITE(IRQSTAT0, 1<<SETUP_PACKET_INTERRUPT);

	HISTO(DEFAULT_HISTORY, "SPk(", udr.bmRequestType, udr.bRequest, udr.wValue);
	HISTO(DEFAULT_HISTORY, "SPk2", udr.wIndex, udr.wLength, NETCHIP_READ(EP_STAT));
    HISTO(DEFAULT_HISTORY, "SPk3", NETCHIP_READ(EP_RSP), NETCHIP_READ(EP_IRQENB), NETCHIP_READ(EP_AVAIL));
    DEBUG(SetupPacket_Msg[0] = '\0');
    DEBUG(SetupPacket_PrintVolume = VOLUME_MEDIUM);

	if (! NcDevice->lpfnNotify( NcDevice->pvNotifyParameter, UFN_MSG_SETUP_PACKET, (DWORD)&udr) )
	{
		Nc2280_StandardUsbRequestHandler(&udr);
	}

    NCPRINT(SetupPacket_PrintVolume, ("Request: %2.2x %2.2x %4.4x %4.4x %4.4x  %s\n",
    udr.bmRequestType,
    udr.bRequest,
    udr.wValue,
    udr.wIndex,
    udr.wLength,
    SetupPacket_Msg
    ));

    // Set up EP0 to interrupt at the start of the Status Phase of the control transfer
    //  - The start of the Status Phase is an IN token for Control Writes, and an
    //    OUT token for Control Reads
    //  - Rx or Tx kick routines may have been called if a Data Phase is expected.
    //    Those routines may or may not have completed the Data Phase already
    //    so we must be careful not to change endpoint interrupt enables 
    //    that those kick routines have set up.
    NETCHIP_WRITE(EP_IRQENB, NETCHIP_READ(EP_IRQENB) | ((udr.bmRequestType & DEVICE_TO_HOST)?
    (1<<DATA_OUT_PING_TOKEN_INTERRUPT_ENABLE):// Control Read: interrupt on OUT tokens
    (1<<DATA_IN_TOKEN_INTERRUPT_ENABLE)));    // Control Write: interrupt on IN tokens

    HISTO(DEFAULT_HISTORY, ")SPk", NETCHIP_READ(EP_IRQENB), NETCHIP_READ(EP_STAT), EpXferInfo[EP0].XferRemain);
}

///////////////////////////////////////////////////////////////////////////////
void
Nc2280_Ep0Interrupt(
void
)
{   // Handle Endpoint 0 Interrupt Status
    // Determine and handle the following EP0 conditions:
    //  - Continuation of Control Read data phase
    //  - Continuation of Control Write data phase
    //  - Start of Status phase (may terminate data phase early - see USB 2.0: 8.5.3)
    DWORD Qual_EpStat;
    Qual_EpStat = NETCHIP_READ(EP_STAT) & NETCHIP_READ(EP_IRQENB);

    if (Qual_EpStat & (1<<DATA_PACKET_TRANSMITTED_INTERRUPT))
    {   // Continuation of Control Read data phase
        Nc2280_TxXferPkt(EP0);
        return;
    }

    if (Qual_EpStat & (1<<DATA_PACKET_RECEIVED_INTERRUPT))
    {   // Continuation of Control Write data phase
        Nc2280_RxXferPkt(EP0);
        return;
    }

    if (Qual_EpStat & ((1<<DATA_IN_TOKEN_INTERRUPT) | (1<<DATA_OUT_PING_TOKEN_INTERRUPT)))
    {   // Start of Status phase
        //  - Data phase, if any, has ended
        //  - Allow Status Phase to complete
        HISTO(DEFAULT_HISTORY, "StPh", NETCHIP_READ(EP_RSP), Qual_EpStat, NETCHIP_READ(EP_CFG));

        EpXferInfo[EP0].State = TransferState_Ready;
        NETCHIP_WRITE(EP_RSP, 1<<CLEAR_CONTROL_STATUS_PHASE_HANDSHAKE);

        NETCHIP_WRITE(EP_IRQENB, 0x00);             // Disable endpoint interrupts
        NETCHIP_WRITE(EP_STAT,
        (1<<DATA_IN_TOKEN_INTERRUPT) |
        (1<<DATA_OUT_PING_TOKEN_INTERRUPT) |
        0);
        return;
    }

#ifdef _DEBUG
    // Program Fault:
    //  - We should never get here!
    NCPRINT(VOLUME_MINIMUM, ("Nc2280_Ep0Interrupt(): Unhandled EP0 interrupt: EP_STAT0:%8.8x EP_IRQENB:%8.8x\n",
    NETCHIP_READ(EP_STAT),
    NETCHIP_READ(EP_IRQENB)
    ));
#endif
}

///////////////////////////////////////////////////////////////////////////////
BOOL
Nc2280_ControlHandler(
void
)
{   // Control Handler:
    //  - Handles setup packet, endpoint zero and control interrupts
    //  - Events tested in priority order
    //  - Only one event handled per call
    //  - Returns TRUE if an event was handled
    // Control interrupt (Suspend, Resume, Root Port Reset, etc.) handling performed
    // here may not apply for your product implementation. Make changes as required.
    DWORD IrqStat0;
    DWORD Enabled_IrqStat1;
	BOOL  bAttached;
    static DWORD PreSuspend_IrqEnb0, PreSuspend_IrqEnb1;

    IrqStat0 = NETCHIP_READ(IRQSTAT0);

    // Note: Always test and handle Setup Packet Interrupt *before* Endpoint 0 Interrupt
    if (IrqStat0 & (1<<SETUP_PACKET_INTERRUPT))
    {
        Nc2280_SetupPacketInterrupt();
        if ((NETCHIP_READ(XCVRDIAG) & (0xf<<USB_TEST_MODE)) != 0)
        {   // NET2280 is in a test mode
            //  - No further processing
            return TRUE;
        }

        if (NETCHIP_READ(IRQSTAT0) & (1<<EP0))
        {   // For better performance, handle EP0 interrupt now...
            Nc2280_Ep0Interrupt();
        }
        return TRUE;
    }
    if (IrqStat0 & (1<<EP0))
    {
        Nc2280_Ep0Interrupt();
        return TRUE;
    }

    // Handle remaining control interrupts in IRQSTAT1 (Suspend, Resume, Root Port Reset, etc.)
    //  - Handle only the events with their interrupt enables set
    Enabled_IrqStat1 = NETCHIP_READ(IRQSTAT1) & NETCHIP_READ(PCIIRQENB1);

    if (Enabled_IrqStat1 & (1<<SOF_INTERRUPT))
    {   // SOF Interrupt
        CurrUsbFrame = (WORD)(NETCHIP_READ_INDEXED(FRAME));
        HISTO(DEFAULT_HISTORY, "SOF", Enabled_IrqStat1, CurrUsbFrame, 0);
        NETCHIP_WRITE(IRQSTAT1, 1<<SOF_INTERRUPT);
        return TRUE;
    }

    if (Enabled_IrqStat1 & (1<<VBUS_INTERRUPT))
    {   // VBUS Interrupt:
        // Depending on your device power policy, you may want to reduce power
        // consumption on your board here...
        DWORD UsbCtl;

        NETCHIP_WRITE(IRQSTAT1, 1<<VBUS_INTERRUPT);

        UsbCtl = NETCHIP_READ(USBCTL);
        HISTO(VOLUME_MEDIUM, "VBus", Enabled_IrqStat1, UsbCtl, 0);

		bAttached = ( 0 != (UsbCtl & (1<<VBUS_PIN)) );
		if ( bAttached != NcDevice->fAttached )
		{
			NcDevice->fAttached = bAttached;
			NcDevice->lpfnNotify( NcDevice->pvNotifyParameter, UFN_MSG_BUS_EVENTS, (UsbCtl & (1<<VBUS_PIN)) ? UFN_ATTACH : UFN_DETACH );
		}

		return TRUE;
    }

    if (Enabled_IrqStat1 & (1<<SUSPEND_REQUEST_CHANGE_INTERRUPT))
    {   // Suspend Request Change Interrupt
        //  - Test to see if we should place NET2280 into suspend
        DWORD IrqStat1 = NETCHIP_READ(IRQSTAT1);

        HISTO(DEFAULT_PRINT, "SusC", Enabled_IrqStat1, IrqStat1, 0);

        if (IrqStat1 & (1<<SUSPEND_REQUEST_INTERRUPT))
        {   // Host is requesting suspend
            DWORD UsbCtl = NETCHIP_READ(USBCTL);

            HISTO(DEFAULT_PRINT, "Susp", UsbCtl, 0, 0);

            // Save NET2280 pre-suspend interrupt enable state:
            PreSuspend_IrqEnb0 = NETCHIP_READ(PCIIRQENB0);
            PreSuspend_IrqEnb1 = NETCHIP_READ(PCIIRQENB1);

            // Allow only a NET2280 Resume Interrupt to cause an interrupt
            NETCHIP_WRITE(PCIIRQENB0, 0x00);
            NETCHIP_WRITE(PCIIRQENB1, 1<<RESUME_INTERRUPT_ENABLE);

            // Before entering suspend, show relevant resume info:
            NCPRINT(VOLUME_LOW, (" Entering suspend:\n"));
            NCPRINT(VOLUME_LOW, ("  - VBUS pin is %s\n",
            (UsbCtl & (1<<VBUS_PIN))? "high (USB connected)": "low (USB disconnected)"));
            NCPRINT(VOLUME_LOW, ("  - Device Remote Wakeup Enable (Device wakeup) is %s\n",
            (UsbCtl & (1<<DEVICE_REMOTE_WAKEUP_ENABLE))? "enabled": "disabled"));
            NCPRINT(VOLUME_LOW, ("  - USB Root Port Wakeup Enable (Host wakeup) is %s\n",
            (UsbCtl & (1<<USB_ROOT_PORT_WAKEUP_ENABLE))? "enabled": "disabled"));

            // Depending on your device power policy, this may be an appropriate place
            // to reduce power consumption on your board:
            //  - USB 2.0: 7.1.7.6: Your device must draw no more than suspend current within 10msec of inactivity
            //  - USB 2.0: 7.2.3: In suspend mode, your device must limit its suspend current to 500 microamps
            //  - USB 2.0: 7.2.3: A high-power device, enabled for remote wakeup, may draw up to 2.5 milleamps

            NETCHIP_WRITE(IRQSTAT1,
            (1<<SUSPEND_REQUEST_INTERRUPT) |        // 1: Place NET2280 into low-power suspend mode
            (1<<SUSPEND_REQUEST_CHANGE_INTERRUPT) |
            0);
            // The NET2280 is now suspended.
            //  - The NET2280 clock has stopped
            //  - Any components relying on the NET2280 LCLKO pin will stop
            //  - If Device Remote Wakeup Enable is set (in USBCTL0) 
            //  - Asserting GENERATE_DEVICE_REMOTE_WAKEUP in USBSTAT will wakeup the NET2280

            return TRUE;
        }

        NETCHIP_WRITE(IRQSTAT1, 1<<SUSPEND_REQUEST_CHANGE_INTERRUPT);
        return TRUE;
    }

    if (Enabled_IrqStat1 & (1<<ROOT_PORT_RESET_INTERRUPT))
    {   // Root Port Reset Interrupt
        HISTO(VOLUME_HIGH, "Rst", Enabled_IrqStat1, 0, 0);
        NETCHIP_WRITE(IRQSTAT1, 1<<ROOT_PORT_RESET_INTERRUPT);
        if (!NcDevice->fAttached) {
            // Upon attach sometimes we do not get a VBUS interrupt, 
            // just the reset interrupt.
            NcDevice->fAttached = TRUE;
            NcDevice->lpfnNotify( NcDevice->pvNotifyParameter, UFN_MSG_BUS_EVENTS, UFN_ATTACH);
        }

        NcDevice->lpfnNotify( NcDevice->pvNotifyParameter, UFN_MSG_BUS_EVENTS, UFN_RESET);
        Nc2280_SetConfiguration(0);
        return TRUE;
    }

    if (Enabled_IrqStat1 & (1<<RESUME_INTERRUPT))
    {   // Resume Interrupt
        HISTO(DEFAULT_PRINT, "Resm", Enabled_IrqStat1, 0, 0);

        // Restore NET2280 pre-suspend interrupt enable state:
        NETCHIP_WRITE(PCIIRQENB0, PreSuspend_IrqEnb0);
        NETCHIP_WRITE(PCIIRQENB1, PreSuspend_IrqEnb1);

        NETCHIP_WRITE(IRQSTAT1, 1<<RESUME_INTERRUPT);
        return TRUE;
    }

    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////
#define TIMEOUT_CATCHER TRUE        // TRUE to enable and show packet transfer timeouts
// If you experience lots of timeouts, you may want to investigate your USB signal quality.

///////////////////////////////////////////////////////////////////////////////
#define MISSING_ACK_CATCHER FALSE   

void
Nc2280_AbortTransfer(BYTE Ep)
{
    DWORD EpPage = EPPAGEOFFSET(Ep);
    DWORD dwValue = NETCHIP_READ(EpPage + EP_IRQENB);
    if ( TransferState_Busy != EpXferInfo[Ep].State )
    {
        return;
    }

    if ( EPA == Ep ) 
    {
        /* Disable BULK IN interrupt */
        dwValue &= ~(1<<DATA_PACKET_TRANSMITTED_INTERRUPT_ENABLE);
        MTPPERFLOG_TRANSPORT(MP_TYPE_SENDUSB_CANCEL, EPA, 0);
    }
    else if ( EPB == Ep )
    {
        /* Disable BULK OUT interrupt */
        dwValue &= ~(1<<DATA_PACKET_RECEIVED_INTERRUPT_ENABLE);
        MTPPERFLOG_TRANSPORT(MP_TYPE_RECEIVEUSB_CANCEL, EPB, 0);
    }
    else
    {
        return;
    }
    NETCHIP_WRITE(EpPage + EP_IRQENB, dwValue );

    // Always set Buffer Flush after xfer cancelled
    NETCHIP_WRITE(EpPage + EP_STAT, 1<<FIFO_FLUSH);

    EpXferInfo[Ep].State = TransferState_Ready;
}

///////////////////////////////////////////////////////////////////////////////
// Receive (host OUT) support routines
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
TRANSFER_STATE
Nc2280_RxXferPkt(
BYTE Ep
)
{   // Handle received (USB OUT) packets (if any)
    //  - Generalized interrupt handler with performance optimizations
    //  - Specific applications may be able to optimize further
    //  - Can be (and is) called by Rx kick routine *and* NET2280 interrupt handler
    DWORD EpPage = EPPAGEOFFSET(Ep);
    DWORD EpStat;
    UINT EpAvail;
    UINT cbToRead = 0;
    DWORD DwordCount;
    DWORD cbReceived;
    PDWORD pRxBuf = (PDWORD)EpXferInfo[Ep].pPkt;
    PDWORD pEpData = (PDWORD)(NETCHIP_BASEADDRESS + EpPage + EP_DATA);

    HISTO(DEFAULT_HISTORY, "RxP(", Ep, NETCHIP_READ(EpPage + EP_STAT), EpXferInfo[Ep].XferRemain);

    do
    {
        EpAvail = NETCHIP_READ(EpPage + EP_AVAIL);
        EpStat = NETCHIP_READ(EpPage + EP_STAT);

#if TIMEOUT_CATCHER && _DEBUG
        if (NETCHIP_READ(EpPage + EP_STAT) & (1<<TIMEOUT))
        {   // A USB timeout has occured
            NCPRINT(VOLUME_LOW, ("Nc2280_RxXferPkt(): Warning: Timeout detected on EP:%d (OUT)\n", Ep));
            NETCHIP_WRITE(EpPage + EP_STAT, 1<<TIMEOUT);
        }
#endif
        if (EpStat & (1<<NAK_OUT_PACKETS))
        {
            EpAvail = NETCHIP_READ(EpPage + EP_AVAIL);
            NETCHIP_WRITE(EpPage + EP_STAT, 
                          1 << DATA_PACKET_RECEIVED_INTERRUPT |
                          1 << NAK_OUT_PACKETS |
                          0 );
        }

        if (EpAvail == 0)
        {   // No data available in endpoint
            HISTO(DEFAULT_HISTORY, "NoAv", 0, EpStat, 0);
            break;
        }

        // Re-arm endpoint for another interrupt
        NETCHIP_WRITE(EpPage + EP_STAT,
        (1<<DATA_PACKET_RECEIVED_INTERRUPT) |
        (1<<DATA_OUT_PING_TOKEN_INTERRUPT) |         // Clearing Data Out Token Interrupt is interesting, but not required
        0);

        if (NULL != pRxBuf)
        {
            cbToRead =  min(EpXferInfo[Ep].XferRemain, EpAvail);

            // Transfer accounting
            EpXferInfo[Ep].XferRemain -= cbToRead;
            EpXferInfo[Ep].pPkt += cbToRead;

            // Calculate for 32 bit accesses
            //  - Sidebar: Be sure buffer ends on 32-bit boundry, 
            //    or a bounds error may occur!
            DwordCount = (cbToRead+(sizeof(DWORD)-1))/(sizeof(DWORD));

            // Copy bytes to memory from endpoint
            //  - For best performance, this loop should be optimized and written in assembler
            while (DwordCount--)
            {
                *(pRxBuf++) = *(pEpData);
            }
        }
        else
        {
            // Flush FIFO 
            NETCHIP_WRITE(EpPage + EP_STAT, 1<<FIFO_FLUSH);
            
            // Wait a USB time frame so that the FIFO is filled
            Sleep(125);
        }

    } while (EpXferInfo[Ep].pPktStart == NULL || 
             (0 < EpXferInfo[Ep].XferRemain && 
              0 == (cbToRead % EpXferInfo[Ep].MaxPktSize)) );

    if ( 0 == EpXferInfo[Ep].XferRemain || 
         0 != (cbToRead % EpXferInfo[Ep].MaxPktSize) || 
         EpStat & (1 << SHORT_PACKET_TRANSFERRED_INTERRUPT))
    {
        NETCHIP_WRITE(EpPage + EP_IRQENB, 
                      ( NETCHIP_READ(EpPage + EP_IRQENB) & 
                      ~(1<<DATA_PACKET_RECEIVED_INTERRUPT_ENABLE) ) );

        NETCHIP_WRITE(EpPage + EP_STAT,
                      (1<<DATA_PACKET_RECEIVED_INTERRUPT) |
                      (1<<DATA_OUT_PING_TOKEN_INTERRUPT) |
                      (1<<NAK_OUT_PACKETS) |                      // Allow NET2280 to accept new OUT packets
                      (1<<SHORT_PACKET_TRANSFERRED_INTERRUPT) |   // Prepare for end of new OUT transfer
                      0);

        EpXferInfo[Ep].State = TransferState_Ready;

        if (EpXferInfo[Ep].pPktStart!= NULL && 
            NcDevice->lpfnXferNotify != NULL && 
            NcDevice->pvNotifyParameter)
        {
            cbReceived = (DWORD)(EpXferInfo[Ep].pPkt - EpXferInfo[Ep].pPktStart);
            MTPPERFLOG_TRANSPORT(MP_TYPE_RECEIVEUSB_END, Ep, cbReceived);
            NcDevice->lpfnXferNotify( NcDevice->pvNotifyParameter, 
                                      Ep, 
                                      cbReceived );
        }
    }

    return EpXferInfo[Ep].State;
}

///////////////////////////////////////////////////////////////////////////////
TRANSFER_STATE
Nc2280_KickRxXfer(
BYTE Ep,
VOID * pXfer,
UINT XferLen
)
{   // Kick an Rx transfer on an OUT endpoint:
    //  - Transfer as many packets as possible here
    //  - Remaining packets transferred using Data Packet Received Interrupts
    DWORD EpPage = EPPAGEOFFSET(Ep);
    HISTO(DEFAULT_HISTORY, "RxK(", Ep, PtrToUlong(pXfer), XferLen);

    // Starting transfer accounting; set pointer and count
    EpXferInfo[Ep].pPkt = EpXferInfo[Ep].pPktStart = (BYTE*)pXfer;
    EpXferInfo[Ep].XferRemain = XferLen;
    EpXferInfo[Ep].State = TransferState_Busy;
    DEBUG(EpXferInfo[Ep].XferCount++);
    
    MTPPERFLOG_TRANSPORT(MP_TYPE_RECEIVEUSB_BEGIN, Ep, XferLen);
    
    // Preload any packets already in the endpoint
    if (Nc2280_RxXferPkt(Ep) != TransferState_Ready)
    {   // Transfer did not complete during preload:
        //  - Enable Data Packet Received Interrupt (simultaneously disabling other endpoint interrupts)
        //  - We carefully do not enable interrupts until *after* getting preloaded packets. That
        //    way, this kick routine can be called by passive level (i.e. non-interrupt) code
        //    without being interrupted by a Data Packet Received Interrupt of another packet
        //  - Enable IN Token Interrupt so that Control Write Status Phase (an IN) can be detected
        //  - Other (non-EP0) OUT endpoints will never receive IN tokens
        HISTO(VOLUME_HIGH, "RxEn", 0, 0, 0);
        NETCHIP_WRITE(EpPage + EP_IRQENB,
        1<<DATA_PACKET_RECEIVED_INTERRUPT_ENABLE |
        1<<DATA_IN_TOKEN_INTERRUPT_ENABLE |
        0);
    }

    HISTO(DEFAULT_HISTORY, ")RxK", NETCHIP_READ(EpPage + EP_IRQENB), NETCHIP_READ(EpPage + EP_STAT), 0);
    return EpXferInfo[Ep].State;
}

///////////////////////////////////////////////////////////////////////////////
// Transmit (host IN) support routines
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
TRANSFER_STATE
Nc2280_TxXferPkt(
BYTE Ep
)
{   // Load NET2280 Tx endpoint with as much data as possible
    //  - Generalized Tx interrupt handler with performance optimizations
    //  - Specific applications may be able to optimize further
    //  - Can be (and is) called by Tx kick routine *and* NET2280 interrupt handler
    //  - Handles ZLP case
    //  - Handles Endpoint zero as well as non-zero endpoints
    DWORD EpPage = EPPAGEOFFSET(Ep);
    PDWORD pTxBuf = (PDWORD)EpXferInfo[Ep].pPkt;
    PDWORD pEpData = (PDWORD)(NETCHIP_BASEADDRESS + EpPage + EP_DATA);
    DWORD  cbSent;
    UINT EpAvail;
    UINT ByteCount;
    UINT DwordCount;

    HISTO(DEFAULT_HISTORY, "TxP(", Ep, NETCHIP_READ(EpPage + EP_STAT), EpXferInfo[Ep].XferRemain);

    for (;;)
    {   // Continue loading the endpoint until:
        //  - The final packet gets loaded into the endpoint
        //  - The endpoint has no space available

#if TIMEOUT_CATCHER && _DEBUG
        // Check for USB timeout error
        if (NETCHIP_READ(EpPage + EP_STAT) & (1<<TIMEOUT))
        {   // A USB timeout has occured
            //  - At least one retry was required to get the data correctly
            //  - If you get a lot of timeout errors, you may want to check your signal quality
            NCPRINT(VOLUME_LOW, ("Nc2280_TxXferPkt(): Warning: Timeout detected on EP:%d (IN)\n", Ep));
            NETCHIP_WRITE(EpPage + EP_STAT, 1<<TIMEOUT);
        }
#endif
        // Arm endpoint for Data Packet Transmitted Interrupt

        NETCHIP_WRITE(EpPage + EP_STAT,
        (1<<DATA_PACKET_TRANSMITTED_INTERRUPT) |
        (1<<DATA_IN_TOKEN_INTERRUPT) |
        0);


        EpAvail = NETCHIP_READ(EpPage + EP_AVAIL);
        if (!EpAvail)
        {   // No space available in FIFO
            //  - A subsequent Data Packet Transmitted Interrupt signals that the host has
            //    successfully taken at least one packet, and space has become available
            HISTO(DEFAULT_HISTORY, "NoAv", 0, NETCHIP_READ(EpPage + EP_STAT), NETCHIP_READ(EpPage + EP_CFG));
            break;
        }

        // We are now commited to loading data into the endpoint
        //  - There is data remaining to be transferred (which may be a ZLP)
        //  - There is space available in the endpoint to put that data

        // Determine how many bytes we can copy
        ByteCount = min(EpAvail, EpXferInfo[Ep].XferRemain);
        HISTO(DEFAULT_HISTORY, "TxCp", ByteCount, NETCHIP_READ(EpPage + EP_STAT), EpXferInfo[Ep].XferRemain);

        // Transfer accounting
        EpXferInfo[Ep].XferRemain -= ByteCount;
        EpXferInfo[Ep].pPkt += ByteCount;

        // Recalculate for 32 bit accesses
        //  - Tip: Memory buffer should end on 32-bit boundry, or a bounds fault may occur!
        DwordCount = (ByteCount)/(sizeof(DWORD));

        // Copy bytes to endpoint
        //  - For best performance, this loop should be optimized, and written in assembler
        while (DwordCount--)
        {
            *(pEpData) = *(pTxBuf++);
        }

        if (EpXferInfo[Ep].XferRemain == 0)
        {   // Handle final packet of the transfer
            //  - 1, 2, 3 or even zero bytes may remain to be written to EP_DATA
            //  - A Zero Length Packet (ZLP) may be needed
            if ((ByteCount % EpXferInfo[Ep].MaxPktSize) == 0)
            {   // The final packet is an exact multiple of the endpoint MaxPacketSize
                //  - The final packet has been written to the FIFO, and automatically validated
                //  - A (ZLP) is needed, but there may not be space available to write it yet
                if (EpXferInfo[Ep].CompleteXfer)
                {
                    EpAvail = NETCHIP_READ(EpPage + EP_AVAIL);
                    HISTO(DEFAULT_HISTORY, "TZlp", EpAvail, 0, 0);
                    if (EpAvail == 0)
                    {   // The FIFO does not have space available for the ZLP now
                        //  - Re-arm Packet Transmitted Interrupt. 
                        //  - When a packet is taken by the host, space becomes available, 
                        //    and the Packet Transmitted Interrupt interrupt will occur
                        continue;
                    }
                }
                else
                {
                    //
                    // Do not send ZLP: More data yet to send in next chunk
                    //
                    NETCHIP_WRITE(EpPage + EP_IRQENB, 1<<DATA_OUT_PING_TOKEN_INTERRUPT_ENABLE);
                    EpXferInfo[Ep].State = TransferState_Ready;
                    break;
                }
            }

            // Validate the final (short) packet of this transfer:
            //  - Use Endpoint Byte Count to tell the NET2280 to validate the partially-full
            //    packet, thereby starting a USB "short packet" transfer
            //  - When (ByteCount % 4) is 1, 2, or 3, Endpoint Byte Count gets set to 1, 2 or 3
            //    then the final data word containing the final 1, 2 or 3 bytes gets written to EP_DATA
            //  - When (ByteCount % 4) is zero, the final data word has already been written
            //    to EP_DATA; Endpoint Byte Count gets set to zero; and the next 'dummy' 
            //    write to EP_DATA signals the NET2280 to start the short packet transfer
            *(PWORD)(NETCHIP_BASEADDRESS + EpPage + EP_BYTE_COUNT) = (WORD)(ByteCount % 4);
            HISTO(DEFAULT_HISTORY, "TShP", NETCHIP_READ(EpPage + EP_CFG), ByteCount, ByteCount % 4);
            //*(pEpData) = *(pTxBuf++);	  // NCBUG: This could cause AV, since all data in pTXBuf might have been sent
            if ((ByteCount % 4) == 0) 
            {
                *(pEpData) = 0;	          // If the final data word has already been written, trigger a 'dummy' write
            }
            else
            {
                *(pEpData) = *(pTxBuf++); // O/W, write the final 1, 2 or 3 bytes
            }

            // Disable IN interrupts for this IN endpoint
            //  - We enable OUT Token Interrupts so that Control Read Status Phase (an OUT) can be detected
            //  - Other (non-EP0) IN endpoints will never receive OUT tokens
            NETCHIP_WRITE(EpPage + EP_IRQENB, 1<<DATA_OUT_PING_TOKEN_INTERRUPT_ENABLE);

            // Transfer is now complete (from our perspective)
            //  - The final (short) packet is in the FIFO and validated
            //  - At this moment the final packet may or may not have been taken by the host
            //  - We do *not* wait for the ACK from the final packet, because of a rare (yet perfectly
            //    legal) condition where the final ACK may not arrive (See USB 2.0: 8.6.4)
            EpXferInfo[Ep].State = TransferState_Ready;
            break;
        }
    }

    HISTO(DEFAULT_HISTORY, ")TxP", EpXferInfo[Ep].pPkt - EpXferInfo[Ep].pPktStart, NETCHIP_READ(EpPage + EP_STAT), EpXferInfo[Ep].XferRemain);

    if (EpXferInfo[Ep].State == TransferState_Ready )
    {
        DWORD EpPage = EPPAGEOFFSET(Ep);
        NETCHIP_WRITE(EpPage + EP_IRQENB, 
                                    ( NETCHIP_READ(EpPage + EP_IRQENB) & 
                                    ~(1<<DATA_PACKET_TRANSMITTED_INTERRUPT_ENABLE) ) );

        if (NcDevice->lpfnXferNotify != NULL && NcDevice->pvNotifyParameter != NULL )
        {
            cbSent = (DWORD)(EpXferInfo[Ep].pPkt - EpXferInfo[Ep].pPktStart);
            MTPPERFLOG_TRANSPORT(MP_TYPE_SENDUSB_END, Ep, cbSent);
            NcDevice->lpfnXferNotify( NcDevice->pvNotifyParameter, Ep, cbSent );
        }
    }
    return EpXferInfo[Ep].State;
}

///////////////////////////////////////////////////////////////////////////////
TRANSFER_STATE
Nc2280_KickTxXfer(
BYTE Ep,
void * pXfer,
UINT XferLen,
BOOL bLastPacket
)
{   // Kick a Tx transfer on an IN endpoint
    //  - Transfer as many packets as possible here
    //  - Remaining packets are transferred using Data Packet Transmitted interrupts
    DWORD EpPage = EPPAGEOFFSET(Ep);

    HISTO(DEFAULT_HISTORY, "TxK(", Ep, PtrToUlong(pXfer), XferLen);

#if MISSING_ACK_CATCHER && _DEBUG
    // Detect and show a missing ACK from the final packet of the previous transfer
    //  - Short Packet Transferred *should* now be set due to the completion of the previous transfer
    //  - If Short Packet Transferred is now clear, then the final packet of the previous transfer missed its ACK
    if (EpXferInfo[Ep].XferCount && !(NETCHIP_READ(EpPage + EP_STAT) & (1<<SHORT_PACKET_TRANSFERRED_INTERRUPT)))
        NCPRINT(VOLUME_LOW, ("Nc2280_KickTxXfer() Warning: Caught missing ACK on EP:%d\n", Ep));
    NETCHIP_WRITE(EpPage + EP_STAT, (1<<SHORT_PACKET_TRANSFERRED_INTERRUPT));
#endif

    // Starting transfer accounting; set pointer and count
    EpXferInfo[Ep].pPkt = EpXferInfo[Ep].pPktStart = (BYTE*)pXfer;
    EpXferInfo[Ep].XferRemain = XferLen;
    EpXferInfo[Ep].CompleteXfer = bLastPacket;
    EpXferInfo[Ep].State = TransferState_Busy;
    DEBUG(EpXferInfo[Ep].XferCount++);

    MTPPERFLOG_TRANSPORT(MP_TYPE_SENDUSB_BEGIN, Ep, XferLen);

    // Preload first packets
    if (Nc2280_TxXferPkt(Ep) != TransferState_Ready)
    {   // Transfer did not complete during preload:
        //  - Enable Data Packet Transmitted Interrupt to handle more packets
        //  - We carefully do not enable interrupts until *after* preloading. That
        //    way, this kick routine can be called by passive level (i.e. non-interrupt) code
        //    without being interrupted by a Data Packet Transmitted Interrupt of a preloaded packet
        //  - Enable OUT Ping Token Interrupt so that Control Read Status Phase (an OUT) can be detected
        //  - Other (non-EP0) IN endpoints will never receive OUT tokens
        HISTO(DEFAULT_HISTORY, "TxEn", 0, NETCHIP_READ(EpPage + EP_STAT), 0);
        NETCHIP_WRITE(EpPage + EP_IRQENB,
        1<<DATA_PACKET_TRANSMITTED_INTERRUPT_ENABLE |
        1<<DATA_OUT_PING_TOKEN_INTERRUPT_ENABLE |
        0);
    }


    HISTO(DEFAULT_HISTORY, ")TxK", NETCHIP_READ(EpPage + EP_IRQENB), NETCHIP_READ(EpPage + EP_STAT), EpXferInfo[Ep].XferCount);
    return EpXferInfo[Ep].State;
}


///////////////////////////////////////////////////////////////////////////////
//  End of file
///////////////////////////////////////////////////////////////////////////////

