/******************************************************************************

Copyright (C) 2002, NetChip Technology, Inc. (http://www.netchip.com)

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.

NCHAL.C

NetChip Hardware Abstraction Layer (HAL) for NET2280 register access.
  
The upper edge of NcHal exposes a small set of macros to access NET2280
registers. The lower edge does the actual chip accesses, and is very hardware
specific. You are invited to change the lower edge as required for your 
hardware platform. Preserving the upper edge eases portability.

NcHal includes debug and high performance "direct access" versions. During 
initial development NetChip recommends using the debug version. The high
performance version is well-optimized, but may require further optimizations
for specific platforms. 

As distributed from NetChip Technology Inc., this file, when compiled with 
other required NetChip files, has been functionally tested on the NET2280 PCI-RDK.

******************************************************************************/

/////////////////////////////////////////////////////////////////////
#if _TRANSFER
// The Transfer project is being compiled
//  - Use Transfer's local header file
    #include "NcCommon.h"
#else
// An external (non-Transfer) project is being compiled
//  - Use external project's header file
//  - MSVC: Be sure the Project Settings:
//     Project Settings | C/C++ | Preprocessor | Additional include...
//     refers to the Transfer folder as well as the external folder!
    #include "NcCommon.h"
#endif

/////////////////////////////////////////////////////////////////////
// Show chip access info (show register name, register value, read or write)
//  - Useful during early debug 
//  - When emailing support@netchip.com, including these listings can help us help you!
#define SHOW_CHIP_ACCESS FALSE           // TRUE: Print chip access info

#if _DEBUG
/////////////////////////////////////////////////////////////////////
// NET2280 register names (ASCII text)
//  - Often useful during early development and debug
//  - For convenience, these register names map directly to 
//  NET2280 addresss divided by 4
char *Net2280_RegisterNames[] =
{   // Directly addressable registers :
    "DEVINIT",      "EECTL",        "EEADDR",       "EEDATA", 
    "PCIIRQENB0",   "PCIIRQENB1",   "CPUIRQENB0",   "CPUIRQENB1",
    "USBIRQENB0",   "USBIRQENB1",   "IRQSTAT0",     "IRQSTAT1",
    "IDXADDR",      "IDXDATA",      "FIFOCTL",      "0x3c",  
    "MEMADDR",      "MEMDATA0",     "MEMDATA1",     "0x4c ", 
    "GPIOCTL",      "GPIOSTAT",     "0x58",         "0x5c",
    "0x60",         "0x64",         "0x68",         "0x6c",      
    "0x70",         "0x74",         "0x78",         "0x7c",      
    "STDRSP",       "PRODVENDID",   "RELNUM",       "USBCTL",
    "USBSTAT",      "XCVRDIAG",     "SETUP0123",    "SETUP4567",
    "0xa0",         "0xa4",         "0xa8",         "0xac", 
    "0xb0",         "0xb4",         "0xb8",         "0xbc", 
    "0xc0",         "0xc4",         "0xc8",         "0xcc", 
    "0xd0",         "0xd4",         "0xd8",         "0xdc", 
    "0xe0",         "0xe4",         "0xe8",         "0xec", 
    "0xf0",         "0xf4",         "0xf8",         "0xfc",
    "PCIMSTADDR",   "PCIMSTDATA",   "PCIMSTCTL",    "PCIMSTSTAT",
    "0x110",        "0x114",        "0x118",        "0x11c", 
    "0x120",        "0x124",        "0x128",        "0x12c", 
    "0x130",        "0x134",        "0x138",        "0x13c", 
    "0x140",        "0x144",        "0x148",        "0x14c", 
    "0x150",        "0x154",        "0x158",        "0x15c", 
    "0x160",        "0x164",        "0x168",        "0x16c", 
    "0x170",        "0x174",        "0x178",        "0x17c", 
    "DMACTL",       "DMASTAT",      "0x188",        "0x18c",             
    "DMACOUNT",     "DMAADDR",      "DMADESC",      "0x19c",             
    "DMACTL"        "DMASTAT"       "0x1a8",        "0x1ac",             
    "DMACOUNT",     "DMAADDR",      "DMADESC",      "0x1bc",             
    "DMACTL",       "DMASTAT",      "0x1c8",        "0x1cc",             
    "DMACOUNT",     "DMAADDR",      "DMADESC",      "0x1dc",             
    "DMACTL",       "DMASTAT",      "0x1e8",        "0x1ec",             
    "DMACOUNT",     "DMAADDR",      "DMADESC",      "0x1fc",             
    "DEP_CFG",      "DEP_IRQENB",   "DEP_STAT",     "0x20c",                
    "DEP_CFG",      "DEP_IRQENB",   "DEP_STAT",     "0x21c",                
    "DEP_CFG",      "DEP_IRQENB",   "DEP_STAT",     "0x22c",                
    "DEP_CFG",      "DEP_IRQENB",   "DEP_STAT",     "0x23c",                
    "DEP_CFG",      "DEP_IRQENB",   "DEP_STAT",     "0x24c",                
    "0x250",        "0x254",        "0x258",        "0x25c",                
    "0x260",        "0x264",        "0x268",        "0x26c", 
    "0x270",        "0x274",        "0x278",        "0x27c", 
    "0x280",        "0x284",        "0x288",        "0x28c", 
    "0x290",        "0x294",        "0x298",        "0x29c", 
    "0x2a0",        "0x2a4",        "0x2a8",        "0x2ac", 
    "0x2b0",        "0x2b4",        "0x2b8",        "0x2bc", 
    "0x2c0",        "0x2c4",        "0x2c8",        "0x2cc", 
    "0x2d0",        "0x2d4",        "0x2d8",        "0x2dc", 
    "0x2e0",        "0x2e4",        "0x2e8",        "0x2ec", 
    "0x2f0",        "0x2f4",        "0x2f8",        "0x2fc",                
    "EP_CFG",       "EP_RSP",       "EP_IRQENB",    "EP_STAT",   
    "EP_AVAIL",     "EP_DATA",      "0x318",        "0x31c",                
    "EP_CFG",       "EP_RSP",       "EP_IRQENB",    "EP_STAT",   
    "EP_AVAIL",     "EP_DATA",      "0x338",        "0x33c",                
    "EP_CFG",       "EP_RSP",       "EP_IRQENB",    "EP_STAT",   
    "EP_AVAIL",     "EP_DATA",      "0x358",        "0x35c",                
    "EP_CFG",       "EP_RSP",       "EP_IRQENB",    "EP_STAT",   
    "EP_AVAIL",     "EP_DATA",      "0x378",        "0x37c",                
    "EP_CFG",       "EP_RSP",       "EP_IRQENB",    "EP_STAT",   
    "EP_AVAIL",     "EP_DATA",      "0x398",        "0x39c",                
    "EP_CFG",       "EP_RSP",       "EP_IRQENB",    "EP_STAT",   
    "EP_AVAIL",     "EP_DATA",      "0x3b8",        "0x3bc",                
    "EP_CFG",       "EP_RSP",       "EP_IRQENB",    "EP_STAT",   
    "EP_AVAIL",     "EP_DATA",      "0x3d8",        "0x3dc",                
    "0x3e0",        "0x3e4",        "0x3e8",        "0x3ec",                
    "0x3f0",        "0x3f4",        "0x3f8",        "0x3fc"                 
};
#endif  // _DEBUG

/////////////////////////////////////////////////////////////////////
#ifdef _HISTO
char *Net2280_BriefRegisterNames[] =
{   // Short (four chars max) NET2280 Register names
    //  - Short names fit into History buffer entries
    "DEVI",     "ECTL",     "EADR",     "EDTA",     "PIE0",     "PIE1",     "CIE0",     "CIE1",
    "UIE0",     "UIE1",     "IST0",     "IST1",     "IDXA",     "IDXD",     "FFCT",     "0x3c",
    "MADR",     "MDA0",     "MDA1",     "0x4c",     "GPCT",     "GPST",     "0x58",     "0x5c",
    "0x60",     "0x64",     "0x68",     "0x6c",     "0x70",     "0x74",     "0x78",     "0x7c",
    "STDR",     "PRVE",     "RELN",     "USBC",     "USBS",     "XCVR",     "S012",     "S456",
    "0xa0",     "0xa4",     "0xa8",     "0xac",     "0xb0",     "0xb4",     "0xb8",     "0xbc", 
    "0xc0",     "0xc4",     "0xc8",     "0xcc",     "0xd0",     "0xd4",     "0xd8",     "0xdc", //XXXXXXXX Mix of 'H' and 0x????
    "0xe0",     "0xe4",     "0xe8",     "0xec",     "0xf0",     "0xf4",     "0xf8",     "0xfc",
    "PMAD",     "PMDA",     "PMCT",     "PMST",     "110H",     "114H",     "118H",     "11cH", 
    "120H",     "124H",     "128H",     "12cH",     "130H",     "134H",     "138H",     "13cH",
    "140H",     "144H",     "148H",     "14cH",     "150H",     "154H",     "158H",     "15cH",
    "160H",     "164H",     "168H",     "16cH",     "170H",     "174H",     "178H",     "17cH", 
    "DCTL",     "DSTT",     "188H",     "18cH",     "DCNT",     "DADR",     "DDSC",     "19cH",
    "DCTL"      "DSTT"      "1a8H",     "1acH",     "DCNT",     "DADR",     "DDSC",     "1bcH",
    "DCTL",     "DSTT",     "1c8H",     "1ccH",     "DCNT",     "DADR",     "DDSC",     "1dcH",
    "DCTL",     "DSTT",     "1e8H",     "1ecH",     "DCNT",     "DADR",     "DDSC",     "1fcH",             
    "DECF",     "DEIR",     "DEST",     "20cH",     "DECF",     "DEIR",     "DEST",     "21cH",                
    "DECF",     "DEIR",     "DEST",     "22cH",     "DECF",     "DEIR",     "DEST",     "23cH",                
    "DECF",     "DEIR",     "DEST",     "24cH",     "250H",     "254H",     "258H",     "25cH",                
    "260H",     "264H",     "268H",     "26cH",     "270H",     "274H",     "278H",     "27cH",                
    "280H",     "284H",     "288H",     "28cH",     "290H",     "294H",     "298H",     "29cH",                
    "2a0H",     "2a4H",     "2a8H",     "2acH",     "2b0H",     "2b4H",     "2b8H",     "2bcH",                
    "2c0H",     "2c4H",     "2c8H",     "2ccH",     "2d0H",     "2d4H",     "2d8H",     "2dcH",                
    "2e0H",     "2e4H",     "2e8H",     "2ecH",     "2f0H",     "2f4H",     "2f8H",     "2fcH",                
    "EPCF",     "EPRS",     "EPIR",     "EPST",     "EPAV",     "EPDA",     "318H",     "31cH",                
    "EPCF",     "EPRS",     "EPIR",     "EPST",     "EPAV",     "EPDA",     "338H",     "33cH",                
    "EPCF",     "EPRS",     "EPIR",     "EPST",     "EPAV",     "EPDA",     "358H",     "35cH",                
    "EPCF",     "EPRS",     "EPIR",     "EPST",     "EPAV",     "EPDA",     "378H",     "37cH",                
    "EPCF",     "EPRS",     "EPIR",     "EPST",     "EPAV",     "EPDA",     "398H",     "39cH",                
    "EPCF",     "EPRS",     "EPIR",     "EPST",     "EPAV",     "EPDA",     "3b8H",     "3bcH",                
    "EPCF",     "EPRS",     "EPIR",     "EPST",     "EPAV",     "EPDA",     "3d8H",     "3dcH",                
    "3e0H",     "3e4H",     "3e8H",     "3ecH",     "3f0H",     "3f4H",     "3f8H",     "3fcH"     
};
#endif  // _HISTO

/////////////////////////////////////////////////////////////////////
// Lowest level chip access primitives (32 bit read and write functions)
//  - These are the lowest level abstraction layers for chip register access
//  - We apply them as functions to aid during early development and debug.
//  - Replace or rewrite these function as appropriate for your design and development requirements
/////////////////////////////////////////////////////////////////////
void
NcWrite32(
DWORD Reg, 
DWORD Val
)
{   // Write value to 32 bit NetChip register
    //  - Performance tip: after debug, replace with in-line function
    //  - Do not try to write Indexed Registers with this function

    PDWORD pNetchipReg;

    pNetchipReg = (PDWORD)(NETCHIP_BASEADDRESS + Reg);

#if SHOW_CHIP_ACCESS
    NCPRINT(VOLUME_MAXIMUM, ("  Write  %4.4x   %-12.12s    %8.8x\n",
    Reg,
    Net2280_RegisterNames[Reg],
    Val
    ));
#elif defined(_HISTO)
    HISTO(VOLUME_MAXIMUM, Net2280_BriefRegisterNames[Reg], Reg, Val, 0x00000000);
#endif
    *pNetchipReg = Val;
}

/////////////////////////////////////////////////////////////////////
DWORD
NcRead32(
DWORD Reg
)
{   // Read 32 bit value from NET2280 register
    //  - Performance tip: after debug, replace with in-line function
    //  - Do not try to read Indexed registers with this function
    PDWORD pNetchipReg;
    DWORD Val;

    pNetchipReg = (PDWORD)(NETCHIP_BASEADDRESS + Reg);
    Val = *pNetchipReg;


#if SHOW_CHIP_ACCESS
    NCPRINT(VOLUME_MAXIMUM, ("  Read   %4.4x   %-12.12s    %8.8x %s\n",
    Reg,
    Net2280_RegisterNames[Reg],
    Val
    ));
#elif defined(_HISTO)
    HISTO(VOLUME_MAXIMUM, Net2280_BriefRegisterNames[Reg], Reg, Val, 0xffffffff);
#endif
    return Val;


}
/////////////////////////////////////////////////////////////////////
DWORD
NcReadIndexed32(
DWORD Reg
)
{
    DWORD Val;

    DWORD  tmp = *(PDWORD)(NETCHIP_BASEADDRESS + IDXADDR);

    *(PDWORD)(NETCHIP_BASEADDRESS + IDXADDR) = Reg;
    Val = *(PDWORD)(NETCHIP_BASEADDRESS + IDXDATA);
    *(PDWORD)(NETCHIP_BASEADDRESS + IDXADDR) = tmp;

    return Val;
}
/////////////////////////////////////////////////////////////////////
void
NcWriteIndexed32(
DWORD Reg,
DWORD Value
)
{

    DWORD  tmp = *(PDWORD)(NETCHIP_BASEADDRESS + IDXADDR);

    *(PDWORD)(NETCHIP_BASEADDRESS + IDXADDR) = Reg;
    *(PDWORD)(NETCHIP_BASEADDRESS + IDXDATA) = Value;
    *(PDWORD)(NETCHIP_BASEADDRESS + IDXADDR) = tmp;

}
/////////////////////////////////////////////////////////////////////
//  End of file
