/******************************************************************************

Copyright (C) 2002, NetChip Technology, Inc. (http://www.netchip.com)

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.

NCCOMMON.H

A collection of common NetChip include files.

******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
#ifndef NCCOMMON_H
#define NCCOMMON_H

///////////////////////////////////////////////////////////////////////////////
// Windows header files include definitions for common data types such 
// as BYTE, WORD, DWORD, etc. Windows-specific data types are only 
// used in files required for the RDK to operate under Windows. These
// special data types are not used in files ported to other platforms
#include <windows.h>
#include <winbase.h>
#include <winioctl.h>

///////////////////////////////////////////////////////////////////////////////
// Standard system headers
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <memory.h>
#include <conio.h>
#include <dos.h>
#include <time.h>
#include <assert.h>

///////////////////////////////////////////////////////////////////////////////
// NET2280 chip access option: direct or call interface
//  - Memory mapped: Fast and efficient - every chip access uses in-line 
//    'memory direct' coding
//  - Call interface: Slow (since every chip access results in a function call) 
//    but the call interface can be *extremely* helpful during early development
//    and debug.
// In either case, NCHAL is designed to be easily ported to your new hardware platform.
// The following macro determines which access method will be used...
#define NET2280_DIRECT_ACCESS TRUE // TRUE: Use direct access (otherwise use function calls)

///////////////////////////////////////////////////////////////////////////////
// NetChip header files
///////////////////////////////////////////////////////////////////////////////

// NET2280 and NET2280 daughterboard hardware definitions
#include "Net2280.H"        // NET2280 register and bit field definitions (from NET2280 spec)

// Other NetChip header files
#include "PSL.h"
#include "UsbStd.H"         // USB standards defined by the USB-IF
#include "NcHal.H"          // NetChip Hardware Abstraction Layer
#include "NCPrint.h"
#include "System.H"         // System functions (memory, interrupt, DMA and helpers)
#include "Nc2280.H"         // Useful NET2280 functions, types and macros (for USB transfers, etc.)

#include "PSL.h"
#include "MTP.h"
#include "MTPUtils.h"

#include "MTPPerfLogger.h"
#include "MTPLoader.h"

///////////////////////////////////////////////////////////////////////////////
#endif // NCCOMMON_H

///////////////////////////////////////////////////////////////////////////////
//  End of file
///////////////////////////////////////////////////////////////////////////////

