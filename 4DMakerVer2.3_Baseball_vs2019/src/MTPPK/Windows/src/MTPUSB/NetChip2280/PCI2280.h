// NetChip PCI-RDK
//  - Constants and structures shared by NcPci driver and user-mode apps
// Copyright (C) 1999, 2000 by Walter Oney
// All rights reserved

///////////////////////////////////////////////////////////////////////////////
#ifndef PCI2280_H
#define PCI2280_H

///////////////////////////////////////////////////////////////////////////////
// This file is owned by the NetChip PCI-RDK driver. It exposes IOCTLs and 
// interfaces for use by user mode code. 
//  - THIS FILE MUST **NEVER** BE MODIFIED BY USER-MODE APPLICATIONS!!
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
#ifndef CTL_CODE
//#pragma message("CTL_CODE undefined. Include winioctl.h or wdm.h")
#endif

///////////////////////////////////////////////////////////////////////////////
// IOCTLs
///////////////////////////////////////////////////////////////////////////////

#define IOCTL_PCI2280_GET_CONFIGURATION_MAP      CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_PCI2280_GET_PROGRAM_MAP            CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_PCI2280_ARM_INTERRUPT              CTL_CODE(FILE_DEVICE_UNKNOWN, 0x811, METHOD_BUFFERED, FILE_ANY_ACCESS)

// DMA IOCTLs
#define IOCTL_PCI2280_ABORT_DMA                  CTL_CODE(FILE_DEVICE_UNKNOWN, 0x832, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_PCI2280_GET_DMA_INFO               CTL_CODE(FILE_DEVICE_UNKNOWN, 0x833, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_PCI2280_DMA_CLEANUP                CTL_CODE(FILE_DEVICE_UNKNOWN, 0x834, METHOD_BUFFERED, FILE_ANY_ACCESS)

// For compatibility, IOCTL_PCI2280_GET_VERSION should *always* be 0x840!!!
#define IOCTL_PCI2280_GET_VERSION                  CTL_CODE(FILE_DEVICE_UNKNOWN, 0x840, METHOD_BUFFERED, FILE_ANY_ACCESS)

// IOCTL_PCI2280_GET_MESSAGE not yet supported!
#define IOCTL_PCI2280_GET_MESSAGE                  CTL_CODE(FILE_DEVICE_UNKNOWN, 0x841, METHOD_BUFFERED, FILE_ANY_ACCESS)

///////////////////////////////////////////////////////////////////////////////
// PCI-RDK driver version. 
// Note to user-mode programers: 
//  - The content of this interface file is correct for the PCI-RDK driver version shown below
//  - Do not change these version numbers
//  - Obtain a PCI-RDK driver and matching interface file from NetChip (support@netchip.com)
#define NC_INTERNAL_RELEASE	TRUE

#if NC_INTERNAL_RELEASE
#define PCI2280_MAJOR_VERSION 0x0000
#define PCI2280_MINOR_VERSION 0x0000
#else
#define PCI2280_MAJOR_VERSION 0x0000
#define PCI2280_MINOR_VERSION 0x0000
#endif
#define PCI2280_VERSION ((PCI2280_MAJOR_VERSION<<16)|(PCI2280_MINOR_VERSION))

///////////////////////////////////////////////////////////////////////////////
// Other constants and structures shared by this driver
///////////////////////////////////////////////////////////////////////////////

// Name real-mode apps use in CreateFile() system call
//  - Name must match DosDevices name (See NCPCI_DOS_DEVICES_NAME)
#define PCI2280_REALMODE_NAME               TEXT("\\\\.\\Pci2280Rdk_000")

///////////////////////////////////////////////////////////////////////////////
// DMA
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
typedef struct _PCI2280_DMA_INFO_STRUCT
{
    ULONG PhysicalAddress;
    ULONG DmaTransferLength;
} PCI2280_DMA_INFO_STRUCT, *PPCI2280_DMA_INFO_STRUCT;

#define MAXIMUM_DMA_SIZE            0x100000
///////////////////////////////////////////////////////////////////////////////
#endif // PCI2280_H

///////////////////////////////////////////////////////////////////////////////
// End of file
///////////////////////////////////////////////////////////////////////////////
