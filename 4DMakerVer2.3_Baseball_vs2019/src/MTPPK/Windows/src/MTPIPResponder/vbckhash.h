//+-------------------------------------------------------------------------
//
//  Microsoft Windows
//
//  Copyright (c) 2009 Microsoft Corporation
//  All rights reserved.
//
//  Windows reference code is licensed under the Microsoft Windows Portable
//  Device Enabling Kit for Media Transfer Protocol (MTP).
//
//  File:       vbckhash.h
//
//--------------------------------------------------------------------------

#ifndef VBCKHASH_H
#define VBCKHASH_H

#include "vcelpool.h"

typedef DWORD (*PFNHASHINGFUNCTION)( DWORD dwKey, DWORD dwTableSize, DWORD dwParam );

//////////////////////////////////////////////////////////////////////////////
class CVBucketHash
{
public:
    CVBucketHash();
   ~CVBucketHash();

   HRESULT Initialize( DWORD dwTableSize,
                       PFNHASHINGFUNCTION pfnHash = NULL,
                       DWORD dwParam = 0 );

   HRESULT Insert( DWORD Key, const void *pValue );
   HRESULT Find( DWORD Key, void **ppValue );
   HRESULT Remove( DWORD Key, void **ppValue );

   DWORD GetEntryCount() { return m_dwEntries; }
   DWORD RemoveAll();

   BOOL GetFirst( DWORD* pdwKey, void **ppValue );
   BOOL GetNext( DWORD* pdwKey, void **ppValue );

private:
    struct NODE
    {
        DWORD Key;
        void *pValue;
        NODE *pNext;
    };

    NODE **m_pTable;
    DWORD m_dwTableSize;
    DWORD m_dwEntries;
    DWORD m_dwHashParam;
    NODE *m_pCursor;

    PFNHASHINGFUNCTION m_fnHash;

    CVCellPool *m_pNodePool;

    DWORD Hash( const DWORD& Key );
};


#endif // VBCKHASH_H
