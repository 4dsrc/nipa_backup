/*
 *  MTPIPService.cpp
 *
 *  Contains the definition of the MTP-IP service functionality.  Primary
 *  functionality of the service is to open a port for MTP-IP, provide a
 *  UPnP service for discoverability, and service any connections to the
 *  MTP-IP port.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPIPResponderPrecomp.h"
#include "MTPIPService.h"
#include "MTPIPDevice.h"
#include "service_ce.h"
#include <iphlpapi.h>
#include <ipifcons.h>

// Local Defines

// Local Types

struct AddressInfo
{
    SOCKADDR_STORAGE        saAddress;
    DWORD                   dwCheck;
    AddressInfo*            paiNext;
};

// Local Functions

static HRESULT _GetServiceIDFromMTPIPUPnPDevice(
                            CMTPIPUPnPDevice* pMTPIPDevice,
                            DWORD* pdwServiceID);

static HRESULT _GetMTPIPUPnPDeviceFromServiceID(DWORD dwServiceID,
                            CMTPIPUPnPDevice** ppMTPIPDevice);

static DWORD WINAPI _IPAddressMonitorThread(LPVOID pvData);

static HRESULT _StartIPAddressMonitor();

static DWORD _SetLastErrorFromHRESULT(HRESULT hr);

// Local Variables

HINSTANCE                   _HInstDLL = NULL;

static CRITICAL_SECTION     _CsMTPIPService = { 0 };

static DWORD                _DwServiceState = SERVICE_STATE_UNKNOWN;
static DWORD                _DwOpenRefs = 0;
static HANDLE               _HThreadIPMonitor = NULL;
static HANDLE               _HEventShutdown = NULL;

static AddressInfo*         _PaiAddressList = NULL;
static DWORD                _DwCheckCount = 0;

static DWORD                _DwServiceID = 0;

static const SHORT          _RgsIPFamilies[] =
{
    AF_INET,
    AF_INET6
};

static const PSLUINT32      _RgdwIFList[] =
{
    IF_TYPE_ETHERNET_CSMACD,
    IF_TYPE_IEEE80211,
    IF_TYPE_ETHERNET_3MBIT,
    IF_TYPE_FASTETHER,
    IF_TYPE_GIGABITETHERNET
};

BOOL _DeviceIoControl(
                            HANDLE hDevice,
                            DWORD dwIoControlCode,
                            LPVOID lpInBuffer,
                            DWORD nInBufferSize,
                            LPVOID lpOutBuffer,
                            DWORD nOutBufferSize,
                            LPDWORD lpBytesReturned,
                            LPOVERLAPPED lpOverlapped)
{
    hDevice;

    lpOverlapped;

    return MTPIP_IOControl(_DwServiceID, dwIoControlCode,
                           (LPBYTE)lpInBuffer, nInBufferSize,
                           (LPBYTE)lpOutBuffer, nOutBufferSize,
                           lpBytesReturned);
}

/*
 *  _GetServiceIDFromMTPIPUPnPDevice
 *
 *  Creates a service ID based for the responder provided.
 *
 *  Arguments:
 *      CMTPIPUPnPDevice*
 *                      pMTPIPDevice        MTP-IP UPnP Device support
 *      DWORD*          pdwServiceID        Service ID
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT _GetServiceIDFromMTPIPUPnPDevice(CMTPIPUPnPDevice* pMTPIPDevice,
                            DWORD* pdwServiceID)
{
    HRESULT         hr;

    // Clear result for safety

    if (NULL != pdwServiceID)
    {
        *pdwServiceID = NULL;
    }

    // Validate arguments

    if ((NULL == pMTPIPDevice) || (NULL == pdwServiceID))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // For now just return the device itself

    *pdwServiceID = (DWORD)pMTPIPDevice;

    // Report success

    hr = S_OK;

Exit:
    return hr;
}


/*
 *  _GetMTPIPUPnPDeviceFromServiceID
 *
 *  Returns an AddRef's instance of the responder associated with the
 *  specified service ID
 *
 *  Arguments:
 *      DWORD           dwServiceID         Service ID
 *      CMTPIPUPnPDevice**
 *                      ppMTPIPDevice       Return buffer for MTP-IP device
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT _GetMTPIPUPnPDeviceFromServiceID(DWORD dwServiceID,
                            CMTPIPUPnPDevice** ppMTPIPDevice)
{
    HRESULT         hr;

    // Clear result for safety

    if (NULL != ppMTPIPDevice)
    {
        *ppMTPIPDevice = NULL;
    }

    // Validate arguments

    if ((0 == dwServiceID) || (NULL == ppMTPIPDevice))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // For now the service ID is the device

    *ppMTPIPDevice = (CMTPIPUPnPDevice*)dwServiceID;
    (*ppMTPIPDevice)->AddRef();

    // Report success

    hr = S_OK;

Exit:
    return hr;
}


/*
 *  _SetLastErrorFromHRESULT
 *
 *  Converts the HRESULT into an appropriate Win32 style error code.
 *
 *  Arguments:
 *      HRESULT         hr                  HRESULT to set
 *
 *  Returns:
 *      DWORD       The error set
 *
 */

DWORD _SetLastErrorFromHRESULT(HRESULT hr)
{
    DWORD           dwResult;

    // Determine specific facilities for errors

    switch (HRESULT_FACILITY(hr))
    {
    case FACILITY_WIN32:
        // Extract the error and set it

        dwResult = HRESULT_CODE(hr);
        break;

    default:
        // Cannot break this down any further

        dwResult = hr;
        break;
    }

    // Set the error and return it

    SetLastError(dwResult);
    return dwResult;
}


/*
 *  _IPAddressMonitorThread
 *
 *  Manages the state of the IP addresses registered with the MTP-IP
 *  device.  As addresses are added or removed the service is kept up to
 *  date.
 *
 *  Arguments:
 *      LPVOID          pvData              Handle to the shutdown event
 *
 *  Returns:
 *      DWORD       NO_ERROR on success
 *
 */

DWORD WINAPI _IPAddressMonitorThread(LPVOID pvData)
{
    DWORD               cResolved;
    DWORD               dwCheck;
    DWORD               dwResult;
    DWORD               dwState;
    DWORD               dwWaitResult;
    BOOL                fDone = FALSE;
    BOOL                fInitialized = FALSE;
    HANDLE              hService = INVALID_HANDLE_VALUE;
    DWORD               idxFamily;
    DWORD               idxAddr;
    PSLSTATUS           ps;
    SOCKADDR_STORAGE*   psasResolved = NULL;
    AddressInfo*        paiCur;
    AddressInfo*        paiLast;
    AddressInfo*        paiNew = NULL;
    AddressInfo*        paiRemove;
    HANDLE              rghWaitEvents[2] = { 0 };
    SOCKADDR_STORAGE    sasAnyAddress = { 0 };
	OVERLAPPED			overlap = {0};
	HANDLE				hFile;

    // Validate arguments

    if (NULL == pvData)
    {
        dwResult = ERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Unpack the shutdown event

    rghWaitEvents[0] = (HANDLE)pvData;

	overlap.hEvent = WSACreateEvent();

	// Register for IP address change notifications with the system

	dwResult = NotifyAddrChange(&hFile, &overlap);
    if (ERROR_IO_PENDING != dwResult)
    {
        goto Exit;
    }

	rghWaitEvents[1] = overlap.hEvent;

    // Start handling IP addresses

    while (!fDone)
    {
        // Increment the state check

        dwCheck = InterlockedIncrement((LONG*)&_DwCheckCount);
        if (0 == dwCheck)
        {
            dwCheck = InterlockedIncrement((LONG*)&_DwCheckCount);
        }

        // Initialize addresses in the desired families

        for (idxFamily = 0; idxFamily < PSLARRAYSIZE(_RgsIPFamilies); idxFamily++)
        {
            // Set the address family

            sasAnyAddress.ss_family = _RgsIPFamilies[idxFamily];

            // And locate addresses for this family

            ps = PSLResolveIPAddress(
                            (SOCKADDR*)&sasAnyAddress, sizeof(sasAnyAddress),
                            _RgdwIFList, PSLARRAYSIZE(_RgdwIFList),
                            &psasResolved, (PSLUINT32*)&cResolved);
            if (PSL_FAILED(ps))
            {
                dwResult = ERROR_GEN_FAILURE;
                goto Exit;
            }

            // We should never get back a "valid" indication on the resolve
            //  since we are hard coding an "any" address

            PSLASSERT(PSLSUCCESS_FALSE != ps);

            // Update the state of all addresses

            for (idxAddr = 0; idxAddr < cResolved; idxAddr++)
            {
                // Walk through the list of registered addresses to see if
                //  we have a match

                for (paiCur = _PaiAddressList;
                        NULL != paiCur;
                        paiCur = paiCur->paiNext)
                {
                    // See if we have a match

                    ps = PSLIsEqualIPAddress((SOCKADDR*)&(paiCur->saAddress),
                            sizeof(paiCur->saAddress),
                            (SOCKADDR*)&(psasResolved[idxAddr]),
                            sizeof(psasResolved[idxAddr]),
                            FALSE);
                    if (PSL_FAILED(ps))
                    {
                        dwResult = ERROR_GEN_FAILURE;
                        goto Exit;
                    }

                    // If no match keep looking

                    if (PSLSUCCESS_FALSE == ps)
                    {
                        continue;
                    }

                    // Update the check count so that we know this address is
                    //  still around

                    paiCur->dwCheck = dwCheck;

                    // Stop looking for matches

                    break;
                }

                // If we get all the way through the current address list
                //  and did not find an entry we know that this is new

                if (NULL == paiCur)
                {
                    // If this is an "any" address then skip it

                    ps = PSLIsIPAddressAny((SOCKADDR*)&(psasResolved[idxAddr]),
                            sizeof(psasResolved[idxAddr]));
                    if (PSLSUCCESS_FALSE != ps)
                    {
                        continue;
                    }

                    // Allocate a new entry to hold the address information

                    paiNew = (AddressInfo*)PSLMemAlloc(LPTR,
                            sizeof(AddressInfo));
                    if (NULL == paiNew)
                    {
                        // Rather then failing completely we will just skip
                        //  this address- that way if memory is more available
                        //  at another time we can add it then

                        continue;
                    }

                    // Update the information

                    PSLASSERT(sizeof(paiNew->saAddress) <=
                            sizeof(psasResolved[idxAddr]));
                    CopyMemory(&(paiNew->saAddress), &(psasResolved[idxAddr]),
                            min(sizeof(paiNew->saAddress),
                                    sizeof(psasResolved[idxAddr])));
                    paiNew->dwCheck = dwCheck;

                    // Set the correct port

                    ((sockaddr_in*)&(paiNew->saAddress))->sin_port =
                            htons(IPPORT_MTPIP);

                    // And register the address with the service

                    if (!_DeviceIoControl(hService,
                            IOCTL_MTPIP_REGISTER_SOCKADDR,
                            &(paiNew->saAddress), sizeof(paiNew->saAddress),
                            NULL, 0, NULL, NULL))
                    {
                        // Go ahead and extract the error for debugging

                        dwResult = SAFE_GETLASTERROR();

                        // Ignore this entry- we may be able to pick it up
                        //  again next time

                        SAFE_PSLMEMFREE(paiNew);
                        continue;
                    }

                    // Add the new address to the list

                    paiNew->paiNext = _PaiAddressList;
                    _PaiAddressList = paiNew;

                    // And forget about it

                    paiNew = NULL;
                }
            }

            // Release the addresses

            SAFE_FREERESOLVEDIPPORTLIST(psasResolved);
        }

        // Walk through the registered address list one more time and check
        //  for entries that need to be removed

        paiLast = NULL;
        paiCur = _PaiAddressList;
        while (NULL != paiCur)
        {
            // If we checked this address we no it is still active and should
            //  not be removed from the list

            if (dwCheck == paiCur->dwCheck)
            {
                paiLast = paiCur;
                paiCur = paiCur->paiNext;
                continue;
            }

            // Need to remove this item from the list- go ahead and reset the
            //  pointers first

            paiRemove = paiCur;
            if (NULL == paiLast)
            {
                _PaiAddressList = paiCur->paiNext;
                paiCur = _PaiAddressList;
            }
            else
            {
                paiLast->paiNext = paiCur->paiNext;
                paiCur = paiCur->paiNext;
            }

            // Tell the service to stop listening to this address

            _DeviceIoControl(hService,
                            IOCTL_MTPIP_DEREGISTER_SOCKADDR,
                            &(paiRemove->saAddress), sizeof(paiRemove->saAddress),
                            NULL, 0, NULL, NULL);

            // And delete the entry

            SAFE_PSLMEMFREE(paiRemove);
        }

        // If this is the first time through let the service now that it can
        //  transition to the start state

        if (!fInitialized)
        {
            // Tell the service to transition to the start state

            if (!_DeviceIoControl(hService, IOCTL_MTPIP_STARTED,
                            NULL, 0, NULL, 0, NULL, NULL))
            {
                // Service did not want to startup- do not do anymore work

                dwResult = SAFE_GETLASTERROR();
                goto Exit;
            }

            // Remember that we have initialized

            fInitialized = TRUE;
        }

        // Wait for the address table to change or to be told to shutdown

        dwWaitResult = WaitForMultipleObjects(PSLARRAYSIZE(rghWaitEvents),
                            rghWaitEvents, FALSE, INFINITE);
        switch (dwWaitResult)
        {
        case WAIT_OBJECT_0:
            // We are being told to shutdown- exit the loop

            fDone = TRUE;
            break;

        case (WAIT_OBJECT_0 + 1):
            // Some change has happened in the address list- nothing to
            //  do here other then start over at the top

            break;

        case WAIT_FAILED:
            // A low level failure like this points to something really bad
            //  happening in the system- stop

            PSLASSERT(FALSE);
            dwResult = SAFE_GETLASTERROR();
            goto Exit;

        case WAIT_TIMEOUT:
        default:
            // Should never get here- this is an infinite wait

            PSLASSERT(FALSE);
            dwResult = ERROR_PROCESS_ABORTED;
            goto Exit;
        }
    }

    // Report success

    dwResult = NO_ERROR;

Exit:
    // Close any open handles we have

    // Report that the service is off because we no long have this
    //  thread running

    dwState = InterlockedExchange((LONG*)&_DwServiceState,
                        SERVICE_STATE_OFF);

    // If the service was already in the off state then the shutdown
    //  was initiated by another thread and we should not call into
    //  the IOControl or we risk deadlocking.

    if (SERVICE_STATE_OFF != dwState)
    {
        // Tell the service to stop listening to any addresses

        _DeviceIoControl(hService, IOCTL_MTPIP_STOPPING,
                        NULL, 0, NULL, 0, NULL, NULL);
    }

    // And walk through and delete any entries we have in the local
    //  address table

    while (NULL != _PaiAddressList)
    {
        paiRemove = _PaiAddressList;
        _PaiAddressList = _PaiAddressList->paiNext;
        SAFE_PSLMEMFREE(paiRemove);
    }

    SAFE_CLOSEFILE(hService);
    //SAFE_CLOSEHANDLE(rghWaitEvents[1]);
    SAFE_FREERESOLVEDIPPORTLIST(psasResolved);
    SAFE_PSLMEMFREE(paiNew);
	SAFE_WSACLOSEEVENT(overlap.hEvent);
    return dwResult;
}


/*
 *  _StartIPAddressMonitor
 *
 *  Creates the thread for the IP address monitor.
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT _StartIPAddressMonitor()
{
    DWORD               dwState;
    HRESULT             hr;
    HANDLE              hThreadIPMonitor = NULL;
    HANDLE              hEventShutdown = NULL;

    // We only support starting the monitor when we are in the starting state

    dwState = InterlockedCompareExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_STARTING_UP,
                            SERVICE_STATE_STARTING_UP);
    PSLASSERT(SERVICE_STATE_STARTING_UP == dwState);
    if (SERVICE_STATE_STARTING_UP != dwState)
    {
        hr = E_UNEXPECTED;
        goto Exit;
    }

    // Make sure we are in the expected state as well

    PSLASSERT(NULL == _HThreadIPMonitor);
    PSLASSERT(NULL == _HEventShutdown);
    if ((NULL != _HThreadIPMonitor) || (NULL != _HEventShutdown))
    {
        hr = E_UNEXPECTED;
        goto Exit;
    }

    // Create a new shutdown event

    hEventShutdown = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (NULL == hEventShutdown)
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        goto Exit;
    }

    // And initialize the monitor thread

    hThreadIPMonitor = CreateThread(NULL, 0, _IPAddressMonitorThread,
                        hEventShutdown, 0, NULL);
    if (NULL == hThreadIPMonitor)
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        goto Exit;
    }

    // All is well- remember the new monitor information

    _HThreadIPMonitor = hThreadIPMonitor;
    _HEventShutdown = hEventShutdown;
    hThreadIPMonitor = NULL;
    hEventShutdown = NULL;

    // Report success

    hr = S_OK;

Exit:
    SAFE_CLOSEHANDLE(hThreadIPMonitor);
    SAFE_CLOSEHANDLE(hEventShutdown);
    return hr;
}


/*
 *  DllMain
 *
 *  Main entrypoint into the DLL
 *
 *  Arguments:
 *      HINSTANCE       hInstDLL            Instance for the DLL
 *      DWORD           dwReason            Reason for the call
 *      LPVOID          pvReserved          Reserved
 *
 *  Returns:
 *      BOOL    TRUE if everything is OK, FALSE otherwise
 *
 */

BOOL WINAPI DllMain(HANDLE hInstDLL, DWORD dwReason, LPVOID pvReserved)
{
    DWORD           dwState;
    BOOL            fResult;

    // See why we are being called

    switch (dwReason)
    {
    case DLL_PROCESS_ATTACH:
        // We only want to track the first caller- ignore all future callers

        dwState = (DWORD)InterlockedCompareExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_UNINITIALIZED,
                            SERVICE_STATE_UNKNOWN);
        ASSERT(SERVICE_STATE_UNKNOWN == dwState);
        if (dwState != SERVICE_STATE_UNKNOWN)
        {
            SetLastError(ERROR_ALREADY_INITIALIZED);
            fResult = FALSE;
        }

        // Try to initialize the critical section

        if (!InitializeCriticalSectionAndSpinCount(&_CsMTPIPService, 0))
        {
            fResult = FALSE;
            goto Exit;
        }

        // Disable thread library calls

        DisableThreadLibraryCalls((HMODULE)hInstDLL);

        // We should not have already been called

        ASSERT(NULL == _HInstDLL);

        // Stash the instance

        _HInstDLL = (HINSTANCE)hInstDLL;

        // Report all is well

        fResult = TRUE;
        break;

    case DLL_PROCESS_DETACH:
        // We should have a stashed instance

        ASSERT(NULL != _HInstDLL);

        // Forget the bound instance

        _HInstDLL = NULL;

        // Delete the critical section

        DeleteCriticalSection(&_CsMTPIPService);

        // Shutdown the memory subsystem

        PSLMemShutdown();

        // Report all is well

        fResult = TRUE;
        break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
        fResult = TRUE;
        break;

    default:
        // Unknown attachment

        ASSERT(FALSE);
        SetLastError(ERROR_INVALID_PARAMETER);
        fResult = FALSE;
        goto Exit;
    }

Exit:
    return fResult;
    pvReserved;
}


/*
 *  MTPIP_Init
 *
 *  Initializes the service.
 *
 *  Arguments:
 *      DWORD           dwData              Context data
 *
 *  Returns:
 *      DWORD       Service identifier to be used on subsequent calls, 0
 *                    if an error occurred
 *
 */

DWORD WINAPI MTPIP_Init(DWORD dwData)
{
    DWORD               dwState;
    DWORD               dwResult;
    BOOL                fInitializing = FALSE;
    HRESULT             hr;
    CMTPIPUPnPDevice*   pMTPIPDevice = NULL;

    // We only support one service initialization- and that is the one bound
    //  to the super service

    dwState = InterlockedCompareExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_STARTING_UP,
                            SERVICE_STATE_UNINITIALIZED);
    if (SERVICE_STATE_UNINITIALIZED != dwState)
    {
        // We have already initialized- do not allow additional services to be
        //  created

        SetLastError(ERROR_NO_MORE_DEVICES);
        dwResult = 0;
        goto Exit;
    }

    // Mark that we are initializing

    fInitializing = TRUE;

    // Validate arguments- currently we do not support any

    if (0 != dwData)
    {
        dwResult = 0;
        goto Exit;
    }

    // Create an instance of the MTP-IP UPnP device

    hr = CMTPIPUPnPDevice::CreateInstance(&pMTPIPDevice);
    if (FAILED(hr))
    {
        _SetLastErrorFromHRESULT(hr);
        dwResult = 0;
        goto Exit;
    }

    hr = _StartIPAddressMonitor();
    if (FAILED(hr))
    {
        _SetLastErrorFromHRESULT(hr);
        dwResult = 0;
        goto Exit;
    }

    // Return the device as the service ID

    hr = _GetServiceIDFromMTPIPUPnPDevice(pMTPIPDevice, &dwResult);
    if (FAILED(hr))
    {
        _SetLastErrorFromHRESULT(hr);
        PSLASSERT(0 == dwResult);
        goto Exit;
    }

    _DwServiceID = dwResult;

    // Device is now owned by the service ID

    pMTPIPDevice = NULL;

Exit:
    // If we failed to initialize the primary device then reset the service
    //  state correctly

    if (fInitializing && (0 == dwResult))
    {
        dwState = InterlockedCompareExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_UNINITIALIZED,
                            SERVICE_STATE_STARTING_UP);
        ASSERT(SERVICE_STATE_STARTING_UP == dwState);
    }
    SAFE_RELEASE(pMTPIPDevice);
    return dwResult;
}


/*
 *  MTPIP_Deinit
 *
 *  Deinitializes the service.
 *
 *  Arguments:
 *      DWORD           dwData              Service identifier
 *
 *  Returns:
 *      BOOL        TRUE if succesful, error returned via SetLastError
 *
 */

BOOL WINAPI MTPIP_Deinit(DWORD dwData)
{
    DWORD               dwState;
    BOOL                fResult;
    HRESULT             hr;
    CMTPIPUPnPDevice*   pMTPIPDevice = NULL;

    // If we have already shutdown then we have no work to do

    dwState = InterlockedCompareExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_UNINITIALIZED,
                            SERVICE_STATE_UNINITIALIZED);
    if (SERVICE_STATE_UNINITIALIZED == dwState)
    {
        // Service is not ininitialized- no work to do

        fResult = TRUE;
        goto Exit;
    }

    // Only one worker is allowed to unload the service- make sure this is it

    dwState = InterlockedExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_UNLOADING);
    if (SERVICE_STATE_UNLOADING == dwState)
    {
        // Some other thread has attempted to unload us- go ahead and report
        //  that it was succesful since we are still blocked on the main
        //  thread

        fResult = TRUE;
        goto Exit;
    }

    // Extract the device and validate the argument

    hr = _GetMTPIPUPnPDeviceFromServiceID(dwData, &pMTPIPDevice);
    if (FAILED(hr))
    {
        _SetLastErrorFromHRESULT(hr);
        fResult = FALSE;
        goto Exit;
    }

    // Release the extra reference count from GetMTPIPUPnPDeviceFromServiceID-
    //  the object will not actually close down until the exit from this
    //  function

    pMTPIPDevice->Release();

    // Mark the service as uninitialized

    InterlockedExchange((LONG*)&_DwServiceState, SERVICE_STATE_UNINITIALIZED);

    // Report success

    fResult = TRUE;

Exit:
    SAFE_RELEASE(pMTPIPDevice);
    return fResult;
}


/*
 *  MTPIP_IOControl
 *
 *  Method for recieving control requests from the services environment.
 *
 *  Arguments:
 *      DWORD           dwData              Service identifier
 *      DWORD           dwCode              IOCTL to perform
 *      PBYTE           pBufIn              Input buffer
 *      DWORD           dwLenIn             Input buffer length
 *      PBYTE           pBufOut             Output buffer
 *      DWORD           dwLenOut            Output buffer length
 *      PDWORD          pdwActualOut        Bytes used in output buffer
 *
 *  Returns:
 *      BOOL        TRUE if succesful, error returned via SetLastError
 *
 */

BOOL WINAPI MTPIP_IOControl(DWORD dwData, DWORD dwCode, PBYTE pBufIn, DWORD dwLenIn,
                            PBYTE pBufOut, DWORD dwLenOut, PDWORD pdwActualOut)
{
    size_t              cchUDN;
    DWORD               dwState;
    DWORD               dwWaitResult;
    BOOL                fResult;
    HRESULT             hr;
    PSLSTATUS           ps;
    CMTPIPUPnPDevice*   pMTPIPDevice = NULL;

    // For sanity only allow handling of one IOCTL at a time- note that this
    //  does not guarantee that access to _DwServiceState is atomic since it
    //  is not generally guarded by critical sections for performance.

    EnterCriticalSection(&_CsMTPIPService);

    // Clear the result for safety

    if (NULL != pdwActualOut)
    {
        *pdwActualOut = 0;
    }

    // We only accept IOCTLs if we are initialized

    dwState = InterlockedCompareExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_UNINITIALIZED,
                            SERVICE_STATE_UNINITIALIZED);
    if (SERVICE_STATE_UNINITIALIZED == dwState)
    {
        fResult = FALSE;
        goto Exit;
    }

    // Extract the device and validate the argument

    hr = _GetMTPIPUPnPDeviceFromServiceID(dwData, &pMTPIPDevice);
    if (FAILED(hr))
    {
        _SetLastErrorFromHRESULT(hr);
        fResult = FALSE;
        goto Exit;
    }

    // Handle the IOCTLs

    switch (dwCode)
    {
    case IOCTL_SERVICE_START:
        // Transition from the stop state to the starting up state

        dwState = InterlockedCompareExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_STARTING_UP,
                            SERVICE_STATE_OFF);
        fResult = (dwState == SERVICE_STATE_OFF);
        if (!fResult)
        {
            SetLastError(ERROR_SERVICE_ALREADY_RUNNING);
            goto Exit;
        }

        // Start the IP address monitor thread

        hr = _StartIPAddressMonitor();
        fResult = SUCCEEDED(hr);
        if (!fResult)
        {
            _SetLastErrorFromHRESULT(hr);
        }
        break;

    case IOCTL_SERVICE_STOP:
        // Transition from the start state to the stop state
        //
        //  NOTE: We do the transition here because we will enter a deadlock
        //  if we try to let the address monitor do an orderly cleanup of
        //  open sockets via IOCTL_MTPIP_DEREGISTER_SOCKADDR.  By setting
        //  the state to SERVICE_STATE_OFF the address monitor knows that
        //  it does not need to do socket cleanup and cause the deadlock.

        dwState = InterlockedCompareExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_OFF,
                            SERVICE_STATE_ON);
        fResult = (dwState == SERVICE_STATE_ON);
        if (!fResult)
        {
            SetLastError(ERROR_SERVICE_NOT_ACTIVE);
            goto Exit;
        }

        // FALL THROUGH INTENTIONAL

    case IOCTL_SERVICE_QUERY_CAN_DEINIT:
        // Make sure the service reports that it is in an off state

        dwState = InterlockedExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_OFF);

        // Make sure we were in an expected state

        PSLASSERT((SERVICE_STATE_STARTING_UP == dwState) ||
                (SERVICE_STATE_ON == dwState) ||
                (SERVICE_STATE_OFF == dwState));

        // Close any open connections

        hr = pMTPIPDevice->UnbindAddress(NULL, 0);
        PSLASSERT(SUCCEEDED(hr));

        // Signal the address monitor to shutdown

        SetEvent(_HEventShutdown);

        // And wait for the thread to terminate

        dwWaitResult = WaitForSingleObject(_HThreadIPMonitor, INFINITE);

        // Release references to the monitor objects

        SAFE_CLOSEHANDLE(_HThreadIPMonitor);
        SAFE_CLOSEHANDLE(_HEventShutdown);

        // See if we can shutdown

        if (IOCTL_SERVICE_QUERY_CAN_DEINIT == dwCode)
        {
            // We should always have space to return something

            PSLASSERT(NULL != pdwActualOut);

            // And report whether or not we can shutdown

            if (NULL != pdwActualOut)
            {
                *pdwActualOut = (0 == _DwOpenRefs) ? 0 : 1;
            }
        }

        // And determine the exit code

        switch (dwWaitResult)
        {
        case WAIT_OBJECT_0:
            // All is well

            fResult = 0;
            break;

        case WAIT_FAILED:
            // Report error

            fResult = FALSE;
            break;

        case WAIT_TIMEOUT:
        default:
            // We should never timeout on an infinite wait

            PSLASSERT(FALSE);
            SetLastError(ERROR_PROCESS_ABORTED);
            fResult = FALSE;
            break;
        }
        break;

    case IOCTL_SERVICE_REFRESH:
        // Rather than duplicating code handle this recursively with a
        //  service stop followed by a start

        fResult = MTPIP_IOControl(dwData, IOCTL_SERVICE_STOP, NULL, 0,
                            NULL, 0, NULL);
        if (!fResult)
        {
            goto Exit;
        }

        // And start it

        fResult = MTPIP_IOControl(dwData, IOCTL_SERVICE_START, NULL, 0,
                            NULL, 0, NULL);
        if (!fResult)
        {
            goto Exit;
        }
        break;

    case IOCTL_SERVICE_STATUS:
        // Regardless of whether we return it or not report the size of
        //  data that we need

        if (NULL != pdwActualOut)
        {
            *pdwActualOut = sizeof(DWORD);
        }

        // Make sure we have a place to return it

        if ((NULL == pBufOut) || (sizeof(DWORD) != dwLenOut))
        {
            // Return the size if requested

            SetLastError((NULL != pdwActualOut) ?
                            ERROR_INSUFFICIENT_BUFFER :
                            ERROR_INVALID_PARAMETER);
            fResult = FALSE;
            goto Exit;
        }

        // Return the current service state

        *((DWORD*)pBufOut) = _DwServiceState;
        fResult = TRUE;
        break;

    case IOCTL_MTPIP_REGISTER_SOCKADDR:
        // Make sure that the service is in the right state

        if ((SERVICE_STATE_STARTING_UP != dwState) &&
            (SERVICE_STATE_ON != dwState))
        {
            // Service is not running- do not allow this socket to be
            //  registered

            SetLastError(ERROR_SERVICE_NOT_ACTIVE);
            fResult = FALSE;
            goto Exit;
        }

        // We must be provided with an address

        if ((NULL == pBufIn) || (0 == dwLenIn))
        {
            SetLastError(ERROR_INVALID_PARAMETER);
            fResult = FALSE;
            goto Exit;
        }

        // And it cannot be an "any" address

        ps = PSLIsIPAddressAny((SOCKADDR*)pBufIn, dwLenIn);
        if (PSLSUCCESS_FALSE != ps)
        {
            hr = (PSL_FAILED(ps)) ? E_FAIL : E_INVALIDARG;
            _SetLastErrorFromHRESULT(hr);
            fResult = FALSE;
            goto Exit;
        }

        // Bind the address to the device

        hr = pMTPIPDevice->BindAddress((SOCKADDR*)pBufIn, dwLenIn);
        if (FAILED(hr))
        {
            _SetLastErrorFromHRESULT(hr);
            fResult = FALSE;
            goto Exit;
        }

        // Report all is well

        fResult = TRUE;
        break;

    case IOCTL_MTPIP_DEREGISTER_SOCKADDR:
        // Make sure we got an address to remove

        if ((NULL == pBufIn) || (0 == dwLenIn) ||
            (PSLSUCCESS == PSLIsIPAddressAny((SOCKADDR*)pBufIn, dwLenIn)))
        {
            SetLastError(ERROR_INVALID_PARAMETER);
            fResult = FALSE;
            goto Exit;
        }

        // Unbind the specified address or all addresses

        hr = pMTPIPDevice->UnbindAddress((SOCKADDR*)pBufIn, dwLenIn);
        fResult = SUCCEEDED(hr);
        if (!fResult)
        {
            _SetLastErrorFromHRESULT(hr);
            goto Exit;
        }
        break;

    case IOCTL_MTPIP_STARTED:
        // All ports are ready- transition the service to the running state

        dwState = InterlockedCompareExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_ON,
                            SERVICE_STATE_STARTING_UP);
        if (SERVICE_STATE_STARTING_UP != dwState)
        {
            SetLastError(ERROR_SERVICE_ALREADY_RUNNING);
            fResult = FALSE;
            goto Exit;
        }

        // Report all is well

        fResult = TRUE;
        break;

    case IOCTL_MTPIP_STOPPING:
        // Make sure nothing got passed in

        PSLASSERT((NULL == pBufIn) && (0 == dwLenIn));
        if ((NULL != pBufIn) || (0 != dwLenIn))
        {
            SetLastError(ERROR_INVALID_PARAMETER);
            fResult = FALSE;
            goto Exit;
        }

        // The IP monitor thread is shutting down- go ahead and release local
        //  references

        SAFE_CLOSEHANDLE(_HThreadIPMonitor);
        SAFE_CLOSEHANDLE(_HEventShutdown);

        // And clean up any open connections

        hr = pMTPIPDevice->UnbindAddress(NULL, 0);
        fResult = SUCCEEDED(hr);
        if (!fResult)
        {
            _SetLastErrorFromHRESULT(hr);
            goto Exit;
        }
        break;

    case IOCTL_MTPIP_CHECK_LOCAL_UDN:
        // Make sure we got something to check- and that we have space to
        //  return the result

        if ((NULL == pBufIn) || (0 == dwLenIn) ||
            (NULL == pBufOut) || (dwLenOut < sizeof(BOOL)))
        {
            SetLastError(ERROR_INVALID_PARAMETER);
            fResult = FALSE;
            goto Exit;
        }

        // Validate that the string is of the correct length

        hr = StringCchLength((LPWSTR)pBufIn, (dwLenIn / sizeof(WCHAR)),
                            &cchUDN);
        if (FAILED(hr))
        {
            _SetLastErrorFromHRESULT(hr);
            fResult = FALSE;
            goto Exit;
        }

        // And ask the device if it is valid

        hr = pMTPIPDevice->IsLocalUDN((LPWSTR)pBufIn);
        if (FAILED(hr))
        {
            _SetLastErrorFromHRESULT(hr);
            fResult = FALSE;
            goto Exit;
        }

        // And return the result

        *((BOOL*)pBufOut) = (S_OK == hr) ? TRUE : FALSE;
        if (NULL != pdwActualOut)
        {
            *pdwActualOut = sizeof(BOOL);
        }

        // Report success

        fResult = TRUE;
        break;

    default:
        // Report unsupported IOCTL

        SetLastError(ERROR_CALL_NOT_IMPLEMENTED);
        fResult = FALSE;
        goto Exit;
    }

Exit:
    LeaveCriticalSection(&_CsMTPIPService);
    SAFE_RELEASE(pMTPIPDevice);
    return fResult;
}


/*
 *  MTPIP_Open
 *
 *  Reports whether or not the specified service may be opened
 *
 *  Argumnents:
 *      DWORD           dwData              Service identifier
 *      DWORD           dwAccess            Access flags
 *      DWORD           dwShareMode         Sharing flags
 *
 *  Returns:
 *      DWORD       Cookie to be handed to MTPIP_Close
 *
 */

DWORD WINAPI MTPIP_Open(DWORD dwData, DWORD dwAccess, DWORD dwShareMode)
{
    DWORD               dwState;
    DWORD               dwResult;
    HRESULT             hr;
    CMTPIPUPnPDevice*   pMTPIPDevice = NULL;

    // Make sure the service is in a good state

    dwState = InterlockedCompareExchange((LONG*)&_DwServiceState,
                            SERVICE_STATE_ON,
                            SERVICE_STATE_ON);
    if ((SERVICE_STATE_ON != dwState) &&
        (SERVICE_STATE_STARTING_UP != dwState))
    {
        SetLastError(ERROR_SERVICE_NOT_ACTIVE);
        dwResult = 0;
        goto Exit;
    }

    // Validate the device

    hr = _GetMTPIPUPnPDeviceFromServiceID(dwData, &pMTPIPDevice);
    if (FAILED(hr))
    {
        _SetLastErrorFromHRESULT(hr);
        dwResult = 0;
        goto Exit;
    }

    // Disallow write and sharing

    if ((GENERIC_WRITE & dwAccess) || (FILE_SHARE_WRITE & dwShareMode))
    {
        SetLastError(ERROR_ACCESS_DENIED);
        dwResult = 0;
        goto Exit;
    }

    // Increment the reference count

    InterlockedIncrement((LONG*)&_DwOpenRefs);

    // Since devices can do nothing special go ahead and return the
    //  MTP-IP device as the handle for this entry

    dwResult = dwData;

Exit:
    SAFE_RELEASE(pMTPIPDevice);
    return dwData;
}


/*
 *  MTPIP_Close
 *
 *  Closes the open serivce
 *
 *  Arguments:
 *      DWORD           dwData              Service open identifier
 *
 *  Returns:
 *      BOOL        TRUE on success
 *
 */

BOOL WINAPI MTPIP_Close(DWORD dwData)
{
    DWORD               dwRefs;
    BOOL                fResult;

    // Make sure we actually are allowed to close this object

    dwRefs = InterlockedCompareExchange((LONG*)&_DwOpenRefs, 0, 0);
    if (0 == dwRefs)
    {
        // Close issued when we are already closed- reject it

        SetLastError(ERROR_ACCESS_DENIED);
        fResult = FALSE;
        goto Exit;
    }

    // Decrement the open count

    InterlockedDecrement((LONG*)&_DwOpenRefs);

    // Report all is well

    fResult = TRUE;

Exit:
    return fResult;
    dwData;
}

