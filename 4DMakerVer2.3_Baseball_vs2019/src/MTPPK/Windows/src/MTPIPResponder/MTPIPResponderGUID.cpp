/*
 *  MTPIPResponderGUID.cpp
 *
 *  Contains definitions of the GUIDs needed by the
 *  MTPIPResponder implementation.
 *
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPIPResponderPrecomp.h"
#include "initguid.h"
#include "MTPIPDevice.h"
