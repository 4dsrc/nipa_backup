/*
 *  MTPIPDeviceXML.h
 *
 *  Contains the XML for the device description
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPIPDEVICEXML_H_
#define _MTPIPDEVICEXML_H_

static const WCHAR _RgchDeviceXML[] =
L"<?xml version=\"1.0\"?>"
L"<root xmlns=\"urn:schemas-upnp-org:device-1-0\">"
    L"<specVersion>"
        L"<major>1</major>"
        L"<minor>0</minor>"
    L"</specVersion>"
    L"<device>"
        L"<UDN>uuid:%ws</UDN>"
        L"<friendlyName>Microsoft MTP-IP Responder (%ws)</friendlyName>"
        L"<deviceType>urn:microsoft-com:device:mtp:1</deviceType>"
        L"<pnpx:X_compatibleId xmlns:pnpx=\"http://schemas.microsoft.com/windows/pnpx/2005/11\">"
            L"urn:microsoft-com:device:mtp:1"
        L"</pnpx:X_compatibleId>"
        L"<modelDescription>MTP endpoint for Microsoft</modelDescription>"
        L"<manufacturer>Microsoft Corporation</manufacturer>"
        L"<manufacturerURL>http://www.microsoft.com/</manufacturerURL>"
        L"<modelName>MTP-IP Responder</modelName>"
        L"<modelNumber>v0.91</modelNumber>"
        L"<modelURL>http://www.microsoft.com/</modelURL>"
        L"<serialNumber>1</serialNumber>"
        L"<UPC>00000-00001</UPC>"
        L"<serviceList>"
            L"<service>"
                L"<serviceType>urn:microsoft-com:service:MtpNullService:1</serviceType>"
                L"<serviceId>urn:microsoft-com:serviceId:MtpNullServiceId</serviceId>"
                L"<SCPDURL>%ws</SCPDURL>"
                L"<controlURL></controlURL>"
                L"<eventSubURL></eventSubURL>"
            L"</service>"
        L"</serviceList>"
        L"<presentationURL></presentationURL>"
    L"</device>"
L"</root>";

#endif  /* _MTPIPDEVICEXML_H_ */
