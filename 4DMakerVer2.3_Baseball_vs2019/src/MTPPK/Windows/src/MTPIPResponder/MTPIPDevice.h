/*
 *  MTPIPDevice.h
 *
 *  Contains the declaration of the MTP-IP UPnP device support.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPIPDEVICE_H_
#define _MTPIPDEVICE_H_

#include "vbckhash.h"
#include "vfifolif.h"

#define UPNPDEVICECONTROL_PROGID     L"MtpIPResponder.UPnPDeviceControl"

// {697E75A1-2FFD-44e0-AC38-38B134CE3600}
DEFINE_GUID(GUID_CMTPIPUPnPDevice,
    0x697e75a1, 0x2ffd, 0x44e0, 0xac, 0x38, 0x38, 0xb1, 0x34, 0xce, 0x36, 0x0);

// Forward declarations

struct MTPIPDeviceCmd;
struct MTPIPDeviceInfo;
struct MTPIPEndpointInfo;

/*
 *  MTPIP Device Commands
 *
 */

enum
{
    MTPIPDEVICECMD_UNKNOWN = 0,
    MTPIPDEVICECMD_INITIALIZE,
    MTPIPDEVICECMD_ADD_DEVICE,
    MTPIPDEVICECMD_REMOVE_DEVICE,
    MTPIPDEVICECMD_SHUTDOWN,
    MTPIPDEVICECMD_MAX
};


/*
 *  CMTPIPUPnPDevice
 *
 *  Implements the IUPnPDeviceControl interface for the MTP-IP device.
 *
 */

class CMTPIPUPnPDevice : public IUPnPDeviceControl,
                            public CPSLUnknown
{
    DWORD                   m_dwDeviceState;

    BOOL                    m_fMTPInit;
    BOOL                    m_fWinSockInit;

    HANDLE                  m_hThreadControl;

    CVFifo                  m_vqCommandQueue;
    CRITICAL_SECTION        m_csCommandQueue;
    HANDLE                  m_hEventCommand;

    MTPIPDeviceInfo*        m_pmdiList;
    CRITICAL_SECTION        m_csDeviceList;

    BOOL                    m_fEnumerateEndpoints;
    DWORD                   m_cActiveEndpoints;
    DWORD                   m_cMaxEndpoints;

    // Command Allocation

    HRESULT AllocateCommand(DWORD dwCommand,
                            WPARAM wParam,
                            LPARAM lParam,
                            MTPIPDeviceCmd** ppCommand);

    BOOL DeleteCommand(MTPIPDeviceCmd* pCommand);

    // EndPoint Support

    HRESULT CreateEndpoint(const SOCKADDR* psaAddr,
                            DWORD cbAddr,
                            MTPIPEndpointInfo** ppmeiNew);

    HRESULT DestroyEndpoint(MTPIPEndpointInfo* pmeiDestroy);

    // Device support

    HRESULT FindDeviceInternal(const VOID* pvAddr,
                            DWORD cbAddr,
                            DWORD dwFlags,
                            MTPIPDeviceInfo** ppmdiResult);

    HRESULT CheckForActiveDevice(const BYTE* pbMAC,
                            DWORD cbMAC,
                            const SOCKADDR* psaAddr,
                            DWORD cbAddr,
                            BOOL fAddAddress);

    HRESULT AddDevice(const SOCKADDR* psaAddr,
                            DWORD cbAddr,
                            const BYTE* pbMAC,
                            DWORD cbMAC,
                            LPCWSTR szDeviceIdentifier);

    HRESULT DestroyDevice(MTPIPDeviceInfo* pmdiDestroy);

protected:
    CMTPIPUPnPDevice();
    ~CMTPIPUPnPDevice();

    // Initialization and Shutdown

    HRESULT Initialize();

    HRESULT Shutdown();

    // IUnknown support

    virtual HRESULT LocateInterface(REFIID riid,
                            LPVOID* ppvUnk);

    // Command Queue Support

    HRESULT SendCommand(DWORD dwCommand,
                            WPARAM wParam,
                            LPARAM lParam);

    HRESULT GetNextCommand(MTPIPDeviceCmd** ppCommand);

    HRESULT EmptyCommandQueue();

    // Command Handlers

    HRESULT CreateDevice(const SOCKADDR* psaAddr,
                            DWORD cbAddr);

    HRESULT RemoveDevice(const SOCKADDR* psaAddr,
                            DWORD cbAddr);

    // UPnP device thread

    static DWORD WINAPI DeviceThread(LPVOID pvParam);

    HRESULT DeviceControl();

public:
    // Instance creation

    static HRESULT CreateInstance(CMTPIPUPnPDevice** ppMTPIPDevice);

    // IUnknown

    DELEGATE_IUNKNOWN(CPSLUnknown);

    // IUPnPDeviceControl

    virtual HRESULT STDMETHODCALLTYPE Initialize(
                            BSTR bstrXMLDesc,
                            BSTR bstrDeviceIdentifier,
                            BSTR bstrInitString);

    virtual HRESULT STDMETHODCALLTYPE GetServiceObject(
                            BSTR bstrUDN,
                            BSTR bstrServiceId,
                            IDispatch** ppdispService);

    // CMTPIPUPnPDevice

    HRESULT STDMETHODCALLTYPE BindAddress(
                            const SOCKADDR* psaAddr,
                            DWORD cbAddr);

    HRESULT STDMETHODCALLTYPE UnbindAddress(
                            const SOCKADDR* psaAddr,
                            DWORD cbAddr);

    HRESULT STDMETHODCALLTYPE IsLocalUDN(
                            LPCWSTR szUDN);
};

#endif  // _MTPIPDEVICE_H_

