//+-------------------------------------------------------------------------
//
//  Microsoft Windows
//
//  Copyright (c) 2009 Microsoft Corporation
//  All rights reserved.
//
//  Windows reference code is licensed under the Microsoft Windows Portable
//  Device Enabling Kit for Media Transfer Protocol (MTP).
//
//  File:       vfifolif.h
//
//--------------------------------------------------------------------------

#ifndef VFIFOLIF_H
#define VFIFOLIF_H

#include "vcelpool.h"

//////////////////////////////////////////////////////////////////////////////
class CVLifo
{
public:
    CVLifo() { m_fInitialized = FALSE; m_dwCount = 0; }

    HRESULT Initialize( DWORD dwElementsPerPage = 10 );

    HRESULT Push( void *pElement );
    BOOL Pop( void **pElement );
    BOOL Peek( void **pElement );

    DWORD Count() { return( m_dwCount ); }

private:
    struct NODE
    {
        void *pData;
        NODE *pNext;
    };

    CVCellPool m_NodePool;
    NODE *m_pHead;
    DWORD m_dwCount;
    BOOL m_fInitialized;
};

//////////////////////////////////////////////////////////////////////////////
class CVFifo
{
public:
    CVFifo() { m_fInitialized = FALSE; m_dwCount = 0; }

    HRESULT Initialize( DWORD dwElementsPerPage = 10 );

    HRESULT Push( void *pElement );
    BOOL Pop( void **pElement );
    BOOL Peek( void **pElement );

    DWORD Count() { return( m_dwCount ); }

private:
    struct NODE
    {
        void *pData;
        NODE *pNext;
    };

    CVCellPool m_NodePool;
    NODE *m_pHead;
    NODE *m_pTail;
    DWORD m_dwCount;
    BOOL m_fInitialized;
};

#endif // FIFOLIFO_H
