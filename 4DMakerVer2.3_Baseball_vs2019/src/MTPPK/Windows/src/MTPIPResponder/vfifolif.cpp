//+-------------------------------------------------------------------------
//
//  Microsoft Windows
//
//  Copyright (c) 2009 Microsoft Corporation
//  All rights reserved.
//
//  Windows reference code is licensed under the Microsoft Windows Portable
//  Device Enabling Kit for Media Transfer Protocol (MTP).
//
//  File:       vfifolif.cpp
//
//--------------------------------------------------------------------------

#include "MTPIPResponderPrecomp.h"
#include "vfifolif.h"


//////////////////////////////////////////////////////////////////////////////
// CVLifo public methods

//////////////////////////////////////////////////////////////////////////////
HRESULT CVLifo::Initialize( DWORD dwElementsPerPage )
{
    if( m_fInitialized )
    {
        return( E_UNEXPECTED );
    }

    if( FAILED( m_NodePool.Initialize( sizeof(NODE), dwElementsPerPage, 1, FALSE ) ) )
    {
        return( E_OUTOFMEMORY );
    }

    m_pHead = NULL;
    m_dwCount = 0;

    m_fInitialized = TRUE;

    return( S_OK );
}

//////////////////////////////////////////////////////////////////////////////
HRESULT CVLifo::Push( void *pElement )
{
    NODE *pNext = m_pHead;

    if( FAILED( m_NodePool.AcquireCell( (void**)&m_pHead ) ) )
    {
        m_pHead = pNext;
        return( E_OUTOFMEMORY );
    }
    m_pHead->pNext = pNext;
    m_pHead->pData = pElement;
    m_dwCount++;

    return( S_OK );
}

//////////////////////////////////////////////////////////////////////////////
BOOL CVLifo::Pop( void **ppElement )
{
    if( !m_pHead )
    {
        return( FALSE );
    }

    *ppElement = m_pHead->pData;

    NODE *pNext = m_pHead->pNext;
    m_NodePool.ReleaseCell( (void*)m_pHead );
    m_pHead = pNext;

    m_dwCount--;

    return( TRUE );
}

//////////////////////////////////////////////////////////////////////////////
BOOL CVLifo::Peek( void **ppElement )
{
    if( !m_pHead )
    {
        return( FALSE );
    }

    *ppElement = m_pHead->pData;

    return( TRUE );
}


//////////////////////////////////////////////////////////////////////////////
// CVFifo public methods

//////////////////////////////////////////////////////////////////////////////
HRESULT CVFifo::Initialize( DWORD dwElementsPerPage )
{
    if( m_fInitialized )
    {
        return( E_UNEXPECTED );
    }

    if( FAILED( m_NodePool.Initialize( sizeof(NODE), dwElementsPerPage, 1, FALSE ) ) )
    {
        return( E_OUTOFMEMORY );
    }

    m_pHead = NULL;
    m_pTail = NULL;
    m_dwCount = 0;

    m_fInitialized = TRUE;

    return( S_OK );
}

//////////////////////////////////////////////////////////////////////////////
HRESULT CVFifo::Push( void *pElement )
{
    NODE *pNew = NULL;
    if( FAILED( m_NodePool.AcquireCell( (void**)&pNew ) ) )
    {
        return( E_OUTOFMEMORY );
    }

    pNew->pNext = NULL;
    pNew->pData = pElement;

    if( m_pTail )
    {
        m_pTail->pNext = pNew;
    }
    else
    {
        m_pHead = pNew;
    }

    m_pTail = pNew;
    m_dwCount++;

    return( S_OK );
}

//////////////////////////////////////////////////////////////////////////////
BOOL CVFifo::Pop( void **ppElement )
{
    if( !m_pHead )
    {
        return( FALSE );
    }

    *ppElement = m_pHead->pData;

    NODE *pNext = m_pHead->pNext;
    m_NodePool.ReleaseCell( (void*)m_pHead );
    m_pHead = pNext;

    if( !m_pHead )
    {
        m_pTail = NULL;
    }

    m_dwCount--;

    return( TRUE );
}

//////////////////////////////////////////////////////////////////////////////
BOOL CVFifo::Peek( void **ppElement )
{
    if( !m_pHead )
    {
        return( FALSE );
    }

    *ppElement = m_pHead->pData;

    return( TRUE );
}
