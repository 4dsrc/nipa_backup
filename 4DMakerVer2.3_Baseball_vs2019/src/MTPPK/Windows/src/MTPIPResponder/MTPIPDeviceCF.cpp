/*
 *  Dll.cpp
 *
 *  Contains the DLL Exports for the UPnPDeviceControl DLL
 *
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */
#include "MTPIPResponderPrecomp.h"
#include "MTPIPDevice.h"

#define UPNPDEVICECONTROL_CLASS     L"UPnPDeviceControl Class"

#define REGVAL_CLSID                L"CLSID"
#define REGVAL_PROGID               L"ProgId"
#define REGVAL_INPROCSERVER32       L"InprocServer32"

extern  HINSTANCE                   _HInstDLL;
static  DWORD                       g_dwRefCountDLL = 0;

HRESULT _WriteKey(
                            HKEY hRoot,
                            const WCHAR* lpSubKey,
                            LPCTSTR val_name,
                            DWORD dwType,
                            void* lpvData,
                            DWORD dwDataSize);

HRESULT _RegisterServer(void);
HRESULT _UnregisterServer(void);

/*
 *  CUPnPDeviceControlCF Declaration
 *
 */

class CUPnPDeviceControlCF : public IClassFactory
{
private:
    DWORD           m_cRef;
    DWORD           m_cLocks;

public:
    CUPnPDeviceControlCF();
    virtual ~CUPnPDeviceControlCF();

    /* IUnknown Methods */
    HRESULT STDMETHODCALLTYPE QueryInterface(
                            REFIID piid,
                            LPVOID* ppvObj);

    DWORD STDMETHODCALLTYPE AddRef();

    DWORD STDMETHODCALLTYPE Release();

    /* IClassFactory Methods */
    HRESULT STDMETHODCALLTYPE CreateInstance(
                            IUnknown* pUnkOuter,
                            REFIID piid,
                            LPVOID* ppvObj);

    HRESULT STDMETHODCALLTYPE LockServer(
                            BOOL fLock);
};

/*
 *  DllRegisterServer()
 *
 *  Standard COM API to register the component
 *
 *  Arguments:
 *      See COM Documentation
 *
 *  Returns:
 *      See COM Documentation
 *
 */

STDAPI DllRegisterServer()
{
    return _RegisterServer();
}

/*
 *  DllUnregisterServer()
 *
 *  Standard COM API to unregister the component.
 *
 *  Arguments:
 *      See COM Documentation
 *
 *  Returns:
 *      See COM Documentation
 *
 */

STDAPI DllUnregisterServer()
{
    return _UnregisterServer();
}


/*
 *  DllCanUnloadNow()
 *
 *  Standard COM API to report whether or not the DLL can be unloaded.
 *
 *  Arguments:
 *      See COM Documentation
 *
 *  Returns:
 *      See COM Documentation
 *
 */

STDAPI DllCanUnloadNow()
{
    return (0 == g_dwRefCountDLL) ? S_OK : S_FALSE;
}


/*
 *  DllGetClassObject
 *
 *  Standard COM API to get access to a class factory for inproc servers
 *
 *  Arguments:
 *      See COM Documentation
 *
 *  Returns:
 *      See COM Documentation
 *
 */

STDAPI DllGetClassObject(REFCLSID pclsid, REFIID piid,
                         LPVOID* ppvFactory)
{
    IClassFactory*  pCF = NULL;
    HRESULT         hr;

    // Make sure that the object being requested is one that we support

    if (!IsEqualGUID(pclsid, GUID_CMTPIPUPnPDevice))
    {
        // Report that the class is not available
        hr = CLASS_E_CLASSNOTAVAILABLE;
        goto Exit;
    }

    // And make sure that we support the interface being asked for
    if (!IsEqualIID(piid, IID_IClassFactory))
    {
        // Report that the class is not available
        hr = CLASS_E_CLASSNOTAVAILABLE;
        goto Exit;
    }

    // Create a new class factory to return
    pCF = NEW CUPnPDeviceControlCF();
    if (NULL == pCF)
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    // And return the requested interface
    hr = pCF->QueryInterface(piid, ppvFactory);
    ASSERT(ERROR_SUCCESS == hr);

Exit:
    if (NULL != pCF)
    {
        pCF->Release();
    }
    return hr;
}
/*
 *  CUPnPDeviceControlCF::CUPnPDeviceControlCF
 *
 *  Standard class constructor
 *
 */

CUPnPDeviceControlCF::CUPnPDeviceControlCF()
{
    m_cRef = 1;
    m_cLocks = 0;

    // Make sure the DLL stays loaded long enough
    InterlockedIncrement((LPLONG)&g_dwRefCountDLL);
    return;
}

/*
 *  CUPnPDeviceControlCF::~CUPnPDeviceControlCF
 *
 *  Standard class destructor
 *
 */

CUPnPDeviceControlCF::~CUPnPDeviceControlCF()
{
    // Allow the DLL to be released
    InterlockedDecrement((LPLONG)&g_dwRefCountDLL);
    return;
}

/*
 *  CUPnPDeviceControlCF::QueryInterface
 *
 *  Standard COM QueryInterface method
 *
 *  Arguments:
 *      See COM Documentation
 *
 *  Returns:
 *      See COM Documentation
 *
 */

HRESULT CUPnPDeviceControlCF::QueryInterface(REFIID piid, LPVOID* ppvObj)
{
    HRESULT         hr;

    // Default the return value to NULL
    if (NULL != ppvObj)
    {
        *ppvObj = NULL;
    }

    // Handle parameter validation
    if (NULL == ppvObj)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Check for the desired interface
    if (IsEqualIID(IID_IUnknown, piid))
    {
        *ppvObj = (IUnknown*)this;
    }
    else if (IsEqualIID(IID_IClassFactory, piid))
    {
        *ppvObj = (IClassFactory*)this;
    }
    else
    {
        // Interface not supported
        hr = E_NOINTERFACE;
        goto Exit;
    }

    // Up the reference count
    SAFE_ADDREF(this);

    // Report Success
    hr = ERROR_SUCCESS;

Exit:
    return hr;
}


/*
 *  CUPnPDeviceControlCF::AddRef
 *
 *  Standard COM AddRef method
 *
 *  Arguments:
 *      See COM Documentation
 *
 *  Returns:
 *      See COM Documentation
 *
 */

DWORD CUPnPDeviceControlCF::AddRef()
{
    InterlockedIncrement((LPLONG)&m_cRef);
    return m_cRef;
}


/*
 *  CUPnPDeviceControlCF::Release
 *
 *  Standard COM Release method
 *
 *  Arguments:
 *      See COM Documentation
 *
 *  Returns:
 *      See COM Documenation
 *
 */

DWORD CUPnPDeviceControlCF::Release()
{
    DWORD           dwRefCount;

    // Decrement the reference count
    dwRefCount = InterlockedDecrement((LPLONG)&m_cRef);

    // If both the reference count and the lock count are 0 than we can
    //  delete this instance of the object
    if ((0 == m_cRef) && (0 == m_cLocks))
    {
        delete this;
    }
    return dwRefCount;
}


/*
 *  CUPnPDeviceControlCF::CreateInstance
 *
 *  Primary IClassFactory method used to create a new instance of a particular
 *  class.
 *
 *  Arguments:
 *      See COM Documenation
 *
 *  Returns:
 *      See COM Documenation
 *
 */

HRESULT CUPnPDeviceControlCF::CreateInstance(IUnknown* pUnkOuter,
                                             REFIID piid,
                                             LPVOID* ppvObj)
{
    HRESULT         hr;
    CMTPIPUPnPDevice* pMTPIPUPnPDevice = NULL;

    // Validate the arguments- we do not support aggregation

    if (NULL == ppvObj)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }
    if (NULL != pUnkOuter)
    {
        hr = CLASS_E_NOAGGREGATION;
        goto Exit;
    }

    // Create a new CMTPIPUPnPDevice instance

    hr = CMTPIPUPnPDevice::CreateInstance(&pMTPIPUPnPDevice);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // And request the interface the user is asking for

    hr = pMTPIPUPnPDevice->QueryInterface(piid, ppvObj);
    ASSERT(ERROR_SUCCESS == hr);

Exit:
    SAFE_RELEASE(pMTPIPUPnPDevice);
    return hr;
}


/*
 *  CUPnPDeviceControlCF::LockServer
 *
 *  Locks a particular server class factory in memory.  Will release the
 *  object if both the lock count and the reference count become zero.
 *
 *  Arguments:
 *      See COM Documenation
 *
 *  Returns:
 *      See COM Documentation
 *
 */

 HRESULT CUPnPDeviceControlCF::LockServer(BOOL fLock)
 {
    // Manage the lock count correctly

    if (fLock)
    {
        // Increment the lock count

        m_cLocks++;
    }
    else
    {
        // Decrement the lock count

        m_cLocks--;

        // See if it is time to release the object

        if ((0 == m_cRef) && (0 == m_cLocks))
        {
            delete this;
        }
    }
    return ERROR_SUCCESS;
}

 HRESULT _WriteKey(HKEY hRoot, LPCWSTR lpSubKey, LPCWSTR val_name,
                  DWORD dwType, void* lpvData, DWORD dwDataSize)
{
    HRESULT hr;
    HKEY hKey;
    LONG lResult;

    lResult = RegCreateKeyEx(hRoot,lpSubKey,
                                    NULL, NULL,
                                    REG_OPTION_NON_VOLATILE,
                                    KEY_CREATE_SUB_KEY|KEY_SET_VALUE,
                                    NULL, &hKey, NULL);
    if (ERROR_SUCCESS != lResult)
    {
        hr = HRESULT_FROM_WIN32(lResult);
        goto Exit;
    }

    lResult = RegSetValueEx(hKey, val_name, 0, dwType,
                            (CONST BYTE*)lpvData, dwDataSize);
    if (ERROR_SUCCESS != lResult)
    {
        hr = HRESULT_FROM_WIN32(lResult);
        goto Exit;
    }

    lResult = RegCloseKey(hKey);
    if (ERROR_SUCCESS != lResult)
    {
        hr = HRESULT_FROM_WIN32(lResult);
        goto Exit;
    }
    hr = S_OK;
Exit:
    return hr;
}

HRESULT _RegisterServer(void)
{
    HRESULT hr;
    DWORD dwRet;
    WCHAR* lpwszClsid;
    WCHAR  szBuff[MAX_PATH]=L"";
    WCHAR  szClsid[MAX_PATH]=L"";
    WCHAR  szInproc[MAX_PATH]=L"";
    WCHAR  szProgId[MAX_PATH];
    WCHAR  szDescriptionVal[MAX_PATH]=L"";

    hr = StringFromCLSID(GUID_CMTPIPUPnPDevice, &lpwszClsid);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = StringCchPrintf(szClsid, MAX_PATH, L"%s", lpwszClsid);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = StringCchPrintf(szInproc, MAX_PATH, L"%s\\%s\\%s",
                         REGVAL_CLSID, szClsid,
                         REGVAL_INPROCSERVER32);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = StringCchPrintf(szProgId, MAX_PATH, L"%s\\%s\\%s",
                         REGVAL_CLSID, szClsid, REGVAL_PROGID);
    if (FAILED(hr))
    {
        goto Exit;
    }

    //
    //write the default value
    //
    hr = StringCchPrintf(szBuff, MAX_PATH, L"%s",
                         UPNPDEVICECONTROL_CLASS);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = StringCchPrintf(szDescriptionVal, MAX_PATH, L"%s\\%s",
                         REGVAL_CLSID, szClsid);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = _WriteKey(
                    HKEY_CLASSES_ROOT,
                    szDescriptionVal,
                    NULL,//write to the "default" value
                    REG_SZ,
                    (void*)szBuff,
                    lstrlen(szBuff) * sizeof(WCHAR));
    if (FAILED(hr))
    {
        goto Exit;
    }

    ASSERT(NULL != _HInstDLL);

    //write the "InprocServer32" key data
    dwRet = GetModuleFileName(_HInstDLL,
                              szBuff,
                              sizeof(szBuff));
    if (dwRet == 0)
    {
        hr = HRESULT_FROM_WIN32(dwRet);
        goto Exit;
    }

    hr = _WriteKey(
                    HKEY_CLASSES_ROOT,
                    szInproc,
                    NULL,//write to the "default" value
                    REG_SZ,
                    (void*)szBuff,
                    lstrlen(szBuff) * sizeof(WCHAR));
    if (FAILED(hr))
    {
        goto Exit;
    }


    //write the "ProgId" key data under HKCR\clsid\{---}\ProgId
    hr = StringCchCopy(szBuff, MAX_PATH, UPNPDEVICECONTROL_PROGID);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = _WriteKey(
                    HKEY_CLASSES_ROOT,
                    szProgId,
                    NULL,
                    REG_SZ,
                    (void*)szBuff,
                    lstrlen(szBuff) * sizeof(WCHAR));
    if (FAILED(hr))
    {
        goto Exit;
    }

    //write the "ProgId" data under HKCR\CodeGuru.FastAddition
    hr = StringCchPrintf(szBuff, MAX_PATH, L"%s",
                    UPNPDEVICECONTROL_CLASS);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = _WriteKey(
                    HKEY_CLASSES_ROOT,
                    UPNPDEVICECONTROL_PROGID,
                    NULL,
                    REG_SZ,
                    (void*)szBuff,
                    lstrlen(szBuff) * sizeof(WCHAR));
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = StringCchPrintf(szProgId, MAX_PATH, L"%s\\%s",
                    UPNPDEVICECONTROL_PROGID,
                    REGVAL_CLSID);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = _WriteKey(
                    HKEY_CLASSES_ROOT,
                    szProgId,
                    NULL,
                    REG_SZ,
                    (void*)szClsid,
                    lstrlen(szClsid) * sizeof(WCHAR));
    if (FAILED(hr))
    {
        goto Exit;
    }
Exit:
    return hr;
}

HRESULT _UnregisterServer(void)
{
    HRESULT hr;
    WCHAR   szKeyName[MAX_PATH]=L"";
    WCHAR   szClsid[MAX_PATH]=L"";
    WCHAR*  lpwszClsid;

    //
    //delete the ProgId entry
    //
    hr = StringCchPrintf(szKeyName, MAX_PATH, L"%s\\%s",
                    UPNPDEVICECONTROL_PROGID,
                    REGVAL_CLSID);
    if (FAILED(hr))
    {
        goto Exit;
    }

    RegDeleteKey(HKEY_CLASSES_ROOT,szKeyName);

    RegDeleteKey(HKEY_CLASSES_ROOT,UPNPDEVICECONTROL_PROGID);


    //
    //delete the CLSID entry for this COM object
    //
    hr = StringFromCLSID(GUID_CMTPIPUPnPDevice,
                         &lpwszClsid);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = StringCchPrintf(szClsid, MAX_PATH, L"%s", lpwszClsid);
    if (FAILED(hr))
    {
        goto Exit;
    }

    hr = StringCchPrintf(szKeyName, MAX_PATH, L"%s\\%s\\%s",
                         REGVAL_CLSID, szClsid,
                         REGVAL_INPROCSERVER32);
    if (FAILED(hr))
    {
        goto Exit;
    }

    RegDeleteKey(HKEY_CLASSES_ROOT,szKeyName);

    hr = StringCchPrintf(szKeyName, MAX_PATH, L"%s\\%s\\%s",
                         REGVAL_CLSID, szClsid, REGVAL_PROGID);
    if (FAILED(hr))
    {
        goto Exit;
    }

    RegDeleteKey(HKEY_CLASSES_ROOT,szKeyName);

    hr = StringCchPrintf(szKeyName, MAX_PATH, L"%s\\%s",
                         REGVAL_CLSID, szClsid);
    if (FAILED(hr))
    {
        goto Exit;
    }

    RegDeleteKey(HKEY_CLASSES_ROOT,szKeyName);
Exit:
    return hr;
}
