//+-------------------------------------------------------------------------
//
//  Copyright (c) 2009 Microsoft Corporation
//  All rights reserved.
//
//  Windows reference code is licensed under the Microsoft Windows Portable
//  Device Enabling Kit for Media Transfer Protocol (MTP).
//
//  File:       vcelpool.h
//
//  Contents:
//
//  History:    10/5/98            Created
//
//--------------------------------------------------------------------------

#ifndef _VCELPOOL_H
#define _VCELPOOL_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Turn this on for retail/free when you need to
// debug bugs in free builds. On shipping version
// this should be off. It's always on for DBG builds.
#if DBG
#define VCELLPOOL_USE_SIGNATURE          1
#else
#define VCELLPOOL_USE_SIGNATURE          0
#endif

#define VCELLPOOL_NUM_PAGE_LIST          3

//////////////////////////////////////////////////////////////////////////////
// CVCellPool
//////////////////////////////////////////////////////////////////////////////
class CVCellPool
{
public:
    //////////////////////////////////////////////////////////////////////
    // Public Methods
    CVCellPool();
   ~CVCellPool();

    HRESULT Initialize( DWORD cbCellSize,
                        DWORD nCellsPerPage,
                        DWORD nInitialPages,
                        BOOL fFreeUnusedPages,
#if defined(_X86_)
                        DWORD cbCellAlignment = 4
#else
                        DWORD cbCellAlignment = 8
#endif
            );

    HRESULT AcquireCell( void **ppCell );
    HRESULT ReleaseCell( void *pCell );
    HRESULT FindCellContaining( void *pvLocation, void **ppCell );
    HRESULT GetInfo( DWORD *pnTotalPages, DWORD *pnPagesOccupied, DWORD *pnCellsOccupied );
    DWORD   GetTotalMemoryUsed() { return( m_cbTotalMemoryUsed ); }

private:
    struct CELLHEADER;

    //////////////////////////////////////////////////////////////////////
    struct PAGEHEADER
    {
#if VCELLPOOL_USE_SIGNATURE
        DWORD           fccSignature;   // signature
#endif
        DWORD           nCellsInUse;    // cells currently allocated
        CELLHEADER     *pFreeChain;     // list of free cells
        PAGEHEADER     *pNextPage;      // next page of cells
        PAGEHEADER     *pPrevPage;      // prev page of cells
#if DBG
        CELLHEADER     *pEndFreeChain;  // list of free cells
        CVCellPool     *pOwnerPool;
#endif
    };
    typedef PAGEHEADER *PPAGEHEADER;

    //////////////////////////////////////////////////////////////////////
    struct CELLHEADER
    {
#if VCELLPOOL_USE_SIGNATURE
        DWORD           fccSignature;   // signature
#endif
        PPAGEHEADER     pPageHeader;    // pointer to this cell's page
        CELLHEADER *    pFreeLink;      // used to link cell in free chain
    };
    typedef CELLHEADER *PCELLHEADER;

    //////////////////////////////////////////////////////////////////////
    // Private Methods
    PPAGEHEADER _AcquireCleanPage();
    void _ReleasePage( PPAGEHEADER pPage );
    void _Cleanup();
    BOOL _IsValidPage( PPAGEHEADER pPage );
    BOOL _IsValidCell( PCELLHEADER pCell );
    BOOL _FreeAnUnusedPage();
    BOOL _CellIsOnPage( PCELLHEADER pCell, PPAGEHEADER pPage );
    void _RemoveFromOldList( PPAGEHEADER & pOldList, PPAGEHEADER & pItem );
    void _AddToNewList( PPAGEHEADER & pNewList, PPAGEHEADER & pItem );

    //////////////////////////////////////////////////////////////////////
    PPAGEHEADER   m_rgPages[VCELLPOOL_NUM_PAGE_LIST];
    //PPAGEHEADER   m_pFreePages = m_rgPages[0];
#if DBG
    DWORD         m_cFreePages;
    //PPAGEHEADER   m_pPartiallyFilledPages = m_rgPages[1];
    DWORD         m_cPartiallyFilledPages;
    //PPAGEHEADER   m_pCompletelyFilledPages = m_rgPages[2];
    DWORD         m_cCompletelyFilledPages;
#endif
    DWORD  m_cbActualPageSize;
    DWORD  m_nCellsPerPage;
    DWORD  m_cbCellSize;
    DWORD  m_cbActualCellSize;
    DWORD  m_nPageCount;
    DWORD  m_fFreeUnusedPages;
    DWORD  m_fInitialized;
    DWORD  m_cbPageHeader;
    DWORD  m_cbCellHeader;
    DWORD  m_cbTotalMemoryUsed;

#if DBG
    BOOL   m_fInUse;
    DWORD  m_AllocPattern;
#endif // DBG
};

#endif // _VCELPOOL_H
