/*
 *  MTPIPDevice.cpp
 *
 *  Contains definitions of the MTP-IP UPnP device support.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "MTPIPResponderPrecomp.h"
#include "MTPIPDevice.h"
#include "MTPIPDeviceXML.h"
#include <Iphlpapi.h>

// Local Defines

#define MTPIPTRANSPORTPARAM             'MIPx'
#define MTPIPFINDFLAGS_SOCKET_ADDRESS   0x00000001
#define MTPIPFINDFLAGS_MAC_ADDRESS      0x00000002
#define MTPIPFINDFLAGS_SEARCH_MASK      0x0000000f

#define MTPIPFINDFLAGS_REMOVE           0x80000000

#define UPNPDEVICECONTROL_LIFETIME      3200
#define UPNPDEVICECONTROL_CONTAINERID   L"Container"
#define UPNPDEVICECONTROL_INITSTRING    L"MtpIP Responder"

enum
{
    MTPIPDEVICESTATE_UNKNOWN = 0,
    MTPIPDEVICESTATE_INITIALIZING,
    MTPIPDEVICESTATE_READY,
    MTPIPDEVICESTATE_SHUTDOWN,
    MTPIPDEVICESTATE_ERROR
};

#define SAFE_DELETECOMMAND(p) \
if( (p) != NULL )       \
{                       \
    DeleteCommand(p);   \
    (p) = NULL;         \
}

#define SAFE_DESTROYENDPOINT(p) \
if( (p) != NULL )       \
{                       \
    DestroyEndpoint(p); \
    (p) = NULL;         \
}

// Local Types

/*
 *  MTPIPDeviceCmd
 *
 *  Command structure for the MTP-IP device class
 *
 */

struct MTPIPDeviceCmd
{
    DWORD                   dwCommand;
    WPARAM                  wParam;
    LPARAM                  lParam;
    HANDLE                  hEventCompletion;
    HRESULT                 hrCommand;
};


/*
 *  MTPIPEndpointInfo
 *
 *  Tracking strcture for registered MTP-IP addresses
 *
 */

struct MTPIPDeviceInfo;

struct MTPIPEndpointInfo
{
    MTPIPDeviceInfo*        pmdiDevice;
    SOCKADDR_STORAGE        saAddress;
    DWORD                   cbAddress;
    SOCKET                  sock;
    WSAEVENT                hEvent;
    MTPIPEndpointInfo*      pmeiNext;
};

/*
 *  MTPIPDeviceInfo
 *
 *  Tracking structure for registered MTP-IP devices
 *
 */

struct MTPIPDeviceInfo
{
    MTPIPEndpointInfo*      pmeiList;
    LPBYTE                  pbMAC;
    DWORD                   cbMAC;
    LPWSTR                  szDeviceID;
    MTPIPDeviceInfo*        pmdiNext;
};

// Local Functions

static HRESULT _MACStringToBytes(LPCWSTR szMAC,
                            LPBYTE pbMACDest,
                            DWORD cbMACDest,
                            DWORD* cbMACDestUsed);
HRESULT _RegisterDevice(
							IUPnPRegistrar* pRegistrar,
							WCHAR* szOrgDeviceId,
							BSTR bstrXMLDesc,
							BSTR bstrResourcePath,
							BSTR* pbstrDeviceId);

HRESULT _GetResourcePath(
                            BSTR* pbstrResourcePath);

// Local Variables

static const WCHAR _RgchServiceDesc[] = L"MTPIPService.xml";

static const WCHAR _RgchUDNMappingKeyFormat[] =
L"SOFTWARE\\Microsoft\\UPnP Device Host\\Description\\%ws\\UDN Mappings\\uuid:%ws";

static const WCHAR _RgchUDNMappingKeyValueFormat[] = L"uuid:%ws";

/*
 *  _MACStringToBytes
 *
 *  Converts a string representing a MAC address into an array of bytes.
 *
 *  Arguments:
 *      LPCWSTR             szMAC           MAC string to covnert
 *      LPBYTE              pbMACDest       Destination for resulting MAC
 *      DWORD               cbMACDest       Size of MAC buffer
 *      DWORD*              pcbMACDestUsed  Actual bytes used
 *
 *  Returns:
 *      HRESULT     S_OK on succes
 *
 */

HRESULT _MACStringToBytes(LPCWSTR szMAC, LPBYTE pbMACDest, DWORD cbMACDest,
                            DWORD* pcbMACDestUsed)
{
    BYTE            bValue;
    DWORD           cchMAC;
    DWORD           cbMAC;
    HRESULT         hr;
    LPBYTE          pbDest;
    const WCHAR*    pchCur;

    // Clear result for safety

    if (NULL != pcbMACDestUsed)
    {
        *pcbMACDestUsed = 0;
    }

    // Validate arguments

    if ((NULL == szMAC) || ((NULL != pbMACDest) && (0 == cbMACDest)))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Determine the length of the resulting MAC- we should have either a
    //  6 byte (12 char) or 8 byte (16 char) MAC address

    cchMAC = wcslen(szMAC);
    if ((12 != cchMAC) && (16 != cchMAC))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }
    cbMAC = cchMAC >> 1;

    // If we were not handed a destination return the size of the MAC

    if (NULL == pbMACDest)
    {
        // Must be able to return the size

        if (NULL == pcbMACDestUsed)
        {
            hr = E_INVALIDARG;
            goto Exit;
        }

        // Return the size of the MAC

        *pcbMACDestUsed = cbMAC;
        hr = S_OK;
        goto Exit;
    }

    // Make sure we have enough space in the buffer provided to return the
    //  full MAC address

    if (cbMACDest < cbMAC)
    {
        // Return required size if possible

        if (NULL != pcbMACDestUsed)
        {
            *pcbMACDestUsed = cbMAC;
        }

        // Report not enough space

        hr = HRESULT_FROM_WIN32(ERROR_INSUFFICIENT_BUFFER);
        goto Exit;
    }

    // Then we transfer in the MAC information directly

    for (pchCur = szMAC, pbDest = pbMACDest; L'\0' != *pchCur; pbDest++)
    {
        // Validate the first nibble character is a valid hex digit

        if (!iswxdigit(*pchCur))
        {
            hr = E_INVALIDARG;
            goto Exit;
        }

        // Pull out the first nibble

        bValue = (BYTE)((((L'0' <= *pchCur) && (*pchCur <= L'9')) ?
                            (*pchCur - '0') :
                            (towlower(*pchCur) - 'a') + 0xa) << 4);
        pchCur++;

        // Validate the second nibble character is a valid hex digit

        if (!iswxdigit(*pchCur))
        {
            hr = E_INVALIDARG;
            goto Exit;
        }

        // And the second

        bValue |= ((L'0' <= *pchCur) && (*pchCur <= L'9')) ?
                            (*pchCur - '0') :
                            (towlower(*pchCur) - 'a') + 0xa;
        pchCur++;

        // And stash it in the destination

        *pbDest = bValue;
    }

    // Return the number of bytes used

    if (NULL != pcbMACDestUsed)
    {
        *pcbMACDestUsed = cbMAC;
    }

    // Report success

    hr = S_OK;

Exit:
    return hr;
}


/*
 *  CMTPIPUPnPDevice::CMTPIPUPnPDevice
 *
 *  Standard class constructor
 *
 */

CMTPIPUPnPDevice::CMTPIPUPnPDevice() :
    m_dwDeviceState(MTPIPDEVICESTATE_UNKNOWN),
    m_fWinSockInit(FALSE),
    m_fMTPInit(FALSE),
    m_hThreadControl(NULL),
    m_hEventCommand(NULL),
    m_pmdiList(NULL),
    m_fEnumerateEndpoints(TRUE),
    m_cActiveEndpoints(0),
    m_cMaxEndpoints(16)
{
    ZeroMemory(&m_csCommandQueue, sizeof(m_csCommandQueue));
    ZeroMemory(&m_csDeviceList, sizeof(m_csDeviceList));
}


/*
 *  CMTPIPUPnPDevice::~CMTPIPUPnPDevice
 *
 *  Standard class destructor
 *
 */

CMTPIPUPnPDevice::~CMTPIPUPnPDevice()
{
    HRESULT         hr;

    // Make sure the object is shutdown cleanly

    hr = Shutdown();
    PSLASSERT(SUCCEEDED(hr));

    // Destroy the critical sections

    DeleteCriticalSection(&m_csCommandQueue);
    DeleteCriticalSection(&m_csDeviceList);
}


/*
 *  CMTPIPUPnPDevice::Initialize
 *
 *  Initializes this instance of the class
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::Initialize()
{
    DWORD           dwState;
    HRESULT         hr;
    HANDLE          hThreadControl = NULL;
    WSADATA         wsaData;
    INT             wsErr;

    // Set the state to initializing

    dwState = InterlockedCompareExchange((LONG*)&m_dwDeviceState,
                            MTPIPDEVICESTATE_INITIALIZING,
                            MTPIPDEVICESTATE_UNKNOWN);
    PSLASSERT(MTPIPDEVICESTATE_UNKNOWN == dwState);
    if (MTPIPDEVICESTATE_UNKNOWN != dwState)
    {
        hr = E_UNEXPECTED;
        goto Exit;
    }

    // Initialize the critical sections

    if (!InitializeCriticalSectionAndSpinCount(&m_csCommandQueue, 0))
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    if (!InitializeCriticalSectionAndSpinCount(&m_csDeviceList, 0))
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    // Initialize the command queue

    hr = m_vqCommandQueue.Initialize(16);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // Initialize WinSock so that we can use it

    wsErr = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (NO_ERROR != wsErr)
    {
        hr = HRESULT_FROM_WIN32(wsErr);
        goto Exit;
    }
    m_fWinSockInit = TRUE;

    // Create the command event

    m_hEventCommand = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (NULL == m_hEventCommand)
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        goto Exit;
    }

    // And start up the device control thread

    hThreadControl = CreateThread(NULL, 0, DeviceThread, this, 0, NULL);
    if (NULL == hThreadControl)
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        goto Exit;
    }

    // Send the initialization command to the thread- this has the benefit
    //  of blocking until the thread is correctly initialized or an error
    //  occurs

    hr = SendCommand(MTPIPDEVICECMD_INITIALIZE, 0, 0);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // Stash the thread handle now that it is ready

    m_hThreadControl = hThreadControl;
    hThreadControl = NULL;

    // Report success

    PSLASSERT(S_OK == hr);

Exit:
    // Make sure we leave the state in the correct mode

    if (FAILED(hr))
    {
        InterlockedExchange((LONG*)&m_dwDeviceState, MTPIPDEVICESTATE_ERROR);
    }
    SAFE_CLOSEHANDLE(hThreadControl);
    return hr;
}


/*
 *  CMTPIPUPnPDevice::Shutdown
 *
 *  Shuts down this instance of the class
 *
 *  Arguments:
 *      None
 *
 *  Returns;
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::Shutdown()
{
    INT             wsErr;
    HRESULT         hr;

    // Release all of the adapters

    hr = UnbindAddress(NULL, 0);
    PSLASSERT(SUCCEEDED(hr));

    // Close down the thread

    if (NULL != m_hThreadControl)
    {
        // Send the shutdown command

        hr = SendCommand(MTPIPDEVICECMD_SHUTDOWN, 0, 0);
        PSLASSERT(SUCCEEDED(hr) || (MTPIPDEVICESTATE_ERROR == m_dwDeviceState));

        // And wait for the thread to terminate

        WaitForSingleObject(m_hThreadControl, INFINITE);

        // Release the thread

        SAFE_CLOSEHANDLE(m_hThreadControl);
    }

    // Release the shutdown event

    SAFE_CLOSEHANDLE(m_hEventCommand);

    // Close MTP

    if (m_fMTPInit)
    {
        MTPUninitialize();
        MTPUnload();
    }

    // Uninitialize WinSock

    if (m_fWinSockInit)
    {
        wsErr = WSACleanup();
        PSLASSERT(NO_ERROR == wsErr);
    }

    // Report success

    return S_OK;
}


/*
 *  CMTPIPUPnPDevice::LocateInterface
 *
 *  QueryInterface callback function
 *
 *  Arguments:
 *      REFIID          riid                Interface requested
 *      LPVOID*         ppvUnk              Buffer to return interface pointer
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::LocateInterface(REFIID riid, LPVOID* ppvUnk)
{
    HRESULT         hr;

    // Make sure the base class has done its work

    PSLASSERT(NULL != ppvUnk);
    PSLASSERT(NULL == *ppvUnk);

    // Look for supported interfaces

    if (IsEqualIID(riid, IID_IUPnPDeviceControl))
    {
        *ppvUnk = (IUPnPDeviceControl*)this;
        ((IUPnPDeviceControl*)*ppvUnk)->AddRef();
        hr = S_OK;
    }
    else if (IsEqualIID(riid, IID_IUPnPDeviceControl))
    {
        *ppvUnk = (IUPnPDeviceControl*)this;
        ((IUPnPDeviceControl*)*ppvUnk)->AddRef();
        hr = S_OK;
    }
    else
    {
        // Interface not supported

        hr = E_NOINTERFACE;
    }
    return hr;
}


/*
 *  CMTPIPUPnPDevice::AllocateCommand
 *
 *  Allocates and initializes the basic command structure
 *
 *  Arguments:
 *      DWORD           dwCommand           Command to set
 *      WPARAM          wParam              Command wParam
 *      LPARAM          lParam              Command lParam
 *      MTPIPDeviceCmd**
 *                      ppCommand           Return buffer for allocated command
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::AllocateCommand(DWORD dwCommand, WPARAM wParam,
                            LPARAM lParam, MTPIPDeviceCmd** ppCommand)
{
    HRESULT         hr;
    MTPIPDeviceCmd* pCommand = NULL;

    // Clear result for safety

    if (NULL != ppCommand)
    {
        *ppCommand = NULL;
    }

    // Validate arguments

    if ((NULL == ppCommand) || (MTPIPDEVICECMD_MAX <= dwCommand))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Allocate a new command

    pCommand = (MTPIPDeviceCmd*)PSLMemAlloc(LPTR, sizeof(MTPIPDeviceCmd));
    if (NULL == pCommand)
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    // Initialize the command

    pCommand->dwCommand = dwCommand;
    pCommand->wParam = wParam;
    pCommand->lParam = lParam;

    // Return the initialized command

    *ppCommand = pCommand;
    pCommand = NULL;

    // Report success

    hr = S_OK;

Exit:
    SAFE_DELETECOMMAND(pCommand);
    return hr;
}


/*
 *  CMTPIPUPnPDevice::DeleteCommand
 *
 *  Deletes an allocated command
 *
 *  Arguments:
 *      MTPIPDeviceCmd* pCommand            Command to delete
 *
 *  Returns:
 *      BOOL    TRUE if deleted succesfully
 *
 */

BOOL CMTPIPUPnPDevice::DeleteCommand(MTPIPDeviceCmd* pCommand)
{
    BOOL            fResult;

    // Make sure we have work to do

    if (NULL == pCommand)
    {
        fResult = TRUE;
        goto Exit;
    }

    // Free the memory associated with the command

    fResult = (NULL == PSLMemFree(pCommand));

Exit:
    return fResult;
}


/*
 *  CMTPIPUPnPDevice::SendCommand
 *
 *  Sends a command to the device control system and waits for a response
 *
 *  Arguments:
 *      DWORD           dwCommand           Command to send
 *      WPARAM          wParam              Command wParam
 *      LPARAM          lParam              Command lParam
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::SendCommand(DWORD dwCommand, WPARAM wParam,
                            LPARAM lParam)
{
    DWORD           dwState;
    DWORD           dwWaitResult;
    HRESULT         hr;
    HANDLE          hEventCompletion = NULL;
    MTPIPDeviceCmd* pCommand = NULL;

    // Make sure that the engine is in the correct state

    dwState = (DWORD)InterlockedCompareExchange((LPLONG)&m_dwDeviceState,
                            MTPIPDEVICESTATE_UNKNOWN,
                            MTPIPDEVICESTATE_UNKNOWN);
    switch (dwState)
    {
    case MTPIPDEVICESTATE_INITIALIZING:
    case MTPIPDEVICESTATE_READY:
        // Commands are allowed in these states- go ahead and allocate

        hr = AllocateCommand(dwCommand, wParam, lParam, &pCommand);
        break;

    case MTPIPDEVICESTATE_SHUTDOWN:
        // Device is no longer available

        hr = HRESULT_FROM_WIN32(ERROR_DEV_NOT_EXIST);
        break;

    case MTPIPDEVICESTATE_ERROR:
        // Device has entered an error state and is no longer accepted commands

        hr = HRESULT_FROM_WIN32(ERROR_DEVICE_REINITIALIZATION_NEEDED);
        break;

    default:
        // Do not expect to get here

        PSLASSERT(FALSE);
        hr = E_UNEXPECTED;
        break;
    }

    // Handle errors

    if (FAILED(hr))
    {
        goto Exit;
    }

    // Create a completion event

    hEventCompletion = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (NULL == hEventCompletion)
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        goto Exit;
    }

    // And record the information in the command

    pCommand->hEventCompletion = hEventCompletion;

    // Add the command to the queue safely

    EnterCriticalSection(&m_csCommandQueue);
    hr = m_vqCommandQueue.Push(pCommand);
    LeaveCriticalSection(&m_csCommandQueue);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // Let the device control thread know that it has work to do

    SetEvent(m_hEventCommand);

    // And wait for the completion event

    dwWaitResult = WaitForSingleObject(hEventCompletion, INFINITE);
    if (WAIT_OBJECT_0 != dwWaitResult)
    {
        hr = (WAIT_FAILED == dwWaitResult) ?
                            HRESULT_FROM_WIN32(SAFE_GETLASTERROR()) :
                            E_UNEXPECTED;
        goto Exit;
    }

    // Retrieve the command result and let the command cleanup on exit

    hr = pCommand->hrCommand;

Exit:
    SAFE_CLOSEHANDLE(hEventCompletion);
    SAFE_DELETECOMMAND(pCommand);
    return hr;
}


/*
 *  CMTPIPUPnPDevice::GetNextCommand
 *
 *  Retrieves the next command from the queue.
 *
 *  Arguments:
 *      MTPIPDeviceCmd**
 *                      ppCommand           Return buffer for command
 *
 *  Returns:
 *      HRESULT     S_OK on success, S_FALSE if no command available
 *
 */

HRESULT CMTPIPUPnPDevice::GetNextCommand(MTPIPDeviceCmd** ppCommand)
{
    BOOL            fLeaveCS = FALSE;
    HRESULT         hr;

    // Clear result for safety

    if (NULL != ppCommand)
    {
        *ppCommand = NULL;
    }

    // Validate arguments

    if (NULL == ppCommand)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Enter critical section for thread safety

    EnterCriticalSection(&m_csCommandQueue);
    fLeaveCS = TRUE;

    // Figure out if we have something to return

    if (0 == m_vqCommandQueue.Count())
    {
        // Nothing to return

        hr = S_FALSE;
        goto Exit;
    }

    // Safely retrieve the command from the command queue

    if (!m_vqCommandQueue.Pop((LPVOID*)ppCommand))
    {
        // We should always be able to get the command

        PSLASSERT(FALSE);
        hr = E_UNEXPECTED;
        goto Exit;
    }

    // Report success

    hr = S_OK;

Exit:
    if (fLeaveCS)
    {
        LeaveCriticalSection(&m_csCommandQueue);
    }
    return hr;
}


/*
 *  CMTPIPUPnPDevice::EmptyCommandQueue
 *
 *  Empties the command queue
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::EmptyCommandQueue()
{
    HRESULT         hr;
    MTPIPDeviceCmd* pCommand = NULL;

    // Loop through and pop off all of the commands

    do
    {
        // Pop the next command

        hr = GetNextCommand(&pCommand);
        if (FAILED(hr))
        {
            goto Exit;
        }

        // If we are done stop

        if (S_OK != hr)
        {
            break;
        }

        // If this command has a completion event make sure that we do not
        //  leave it blocked permanently

        if (NULL != pCommand->hEventCompletion)
        {
            // Report that the device is no longer available

            pCommand->hrCommand = HRESULT_FROM_WIN32(ERROR_DEV_NOT_EXIST);

            // And signal the event to unblock the calling thread so that
            //  it can retrieve the result and delete the command

            SetEvent(pCommand->hEventCompletion);
        }
        else
        {
            // Delete the command since we are done with it

            SAFE_DELETECOMMAND(pCommand);
        }

    }
    while (S_OK == hr);

    // Report success

    hr = S_OK;

Exit:
    return hr;
}


/*
 *  CMTPIPUPnPDevice::FindDeviceInternal
 *
 *  Locates and potentially removes a device
 *
 *  Arguments:
 *      const VOID*     pvAddr              Address to find
 *      DWORD           cbAddr              Length of the address
 *      DWORD           dwFlags             Flags
 *      MTPIPDeviceInfo**
 *                      ppmdiResult         Return buffer for found item
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::FindDeviceInternal(const VOID* pvAddr,
                            DWORD cbAddr, DWORD dwFlags,
                            MTPIPDeviceInfo** ppmdiResult)
{
    HRESULT             hr;
    BOOL                fRemove;
    MTPIPEndpointInfo*  pmeiCur;
    MTPIPEndpointInfo*  pmeiLast;
    MTPIPDeviceInfo*    pmdiCur;
    MTPIPDeviceInfo*    pmdiLast;

    // Make sure that we are not interrupted

    EnterCriticalSection(&m_csDeviceList);

    // Clear result for safety

    if (NULL != ppmdiResult)
    {
        *ppmdiResult = NULL;
    }

    // See if we are going to be removing this object- note that remove is
    //  only valid if we are searching by address

    fRemove = (MTPIPFINDFLAGS_REMOVE & dwFlags);
    PSLASSERT(!fRemove || (MTPIPFINDFLAGS_SOCKET_ADDRESS ==
                            (MTPIPFINDFLAGS_SEARCH_MASK & dwFlags)));
    if (fRemove && (MTPIPFINDFLAGS_SOCKET_ADDRESS !=
                            (MTPIPFINDFLAGS_SEARCH_MASK & dwFlags)))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Validate arguments- we will not return device information if we are
    //  told to remove it

    PSLASSERT(!fRemove || (NULL == ppmdiResult));
    if (fRemove && (NULL != ppmdiResult))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // If we are handed a NULL address then we want to destroy all of the
    //  devices- handle this separately

    if (NULL == pvAddr)
    {
        // This is only valid with remove specified

        if (!fRemove)
        {
            hr = E_INVALIDARG;
            goto Exit;
        }

        // Loop through and remove each of the registered items

        for (pmdiCur = m_pmdiList; NULL != pmdiCur; pmdiCur = m_pmdiList)
        {
            // Remove this item from the list

            m_pmdiList = pmdiCur->pmdiNext;

            // And destroy it

            hr = DestroyDevice(pmdiCur);
            PSLASSERT(SUCCEEDED(hr));
        }

        // Report success

        hr = S_OK;
        goto Exit;
    }

    // Make sure we have a length

    if (0 == cbAddr)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Assume we do not have an address match

    pmeiCur = NULL;
    pmeiLast = NULL;

    // Walk through the device list looking for a match

    for (pmdiLast = NULL, pmdiCur = m_pmdiList;
            NULL != pmdiCur;
            pmdiLast = pmdiCur, pmdiCur = pmdiCur->pmdiNext)
    {
        // Determine what type of comparison we are using

        switch (MTPIPFINDFLAGS_SEARCH_MASK & dwFlags)
        {
        case MTPIPFINDFLAGS_SOCKET_ADDRESS:
            // Assume the address is not found

            hr = S_FALSE;

            // Walk through and check each of the addresses registered with
            //  this device

            for (pmeiLast = NULL, pmeiCur = pmdiCur->pmeiList;
                    NULL != pmeiCur;
                    pmeiLast = pmeiCur, pmeiCur = pmeiCur->pmeiNext)
            {
                // See if we have a match

                hr = PSLIsEqualIPAddress((SOCKADDR*)pvAddr, cbAddr,
                            (SOCKADDR*)&(pmeiCur->saAddress),
                            pmeiCur->cbAddress, TRUE);
                if (FAILED(hr))
                {
                    goto Exit;
                }

                // Stop looking if we found a match

                if (S_OK == hr)
                {
                    break;
                }
            }
            break;

        case MTPIPFINDFLAGS_MAC_ADDRESS:
            // Check the MAC address for equality

            hr = ((cbAddr == pmdiCur->cbMAC) &&
                    (0 == memcmp(pvAddr, pmdiCur->pbMAC, cbAddr))) ?
                            S_OK : S_FALSE;
            break;

        default:
            // Unexpected search type

            PSLASSERT(FALSE);
            hr = S_OK;
            goto Exit;
        }

        // See if we found a match

        if (S_OK == hr)
        {
            // Found it

            break;
        }
    }

    // See if we found a match

    if (NULL == pmdiCur)
    {
        hr = S_FALSE;
        goto Exit;
    }

    // Handle the simple case first- we are just locating the entry

    if (!fRemove)
    {
        // Return the entry if requested

        if (NULL != ppmdiResult)
        {
            *ppmdiResult = pmdiCur;
        }

        // Report the item as found

        hr = S_OK;
        goto Exit;
    }

    // We should only get here on address searches

    PSLASSERT(MTPIPFINDFLAGS_SOCKET_ADDRESS ==
                            (MTPIPFINDFLAGS_SEARCH_MASK & dwFlags));

    // Remove the address from the list

    PSLASSERT(NULL != pmeiCur);
    if (NULL != pmeiLast)
    {
        pmeiLast->pmeiNext = pmeiCur->pmeiNext;
    }
    else
    {
        pmdiCur->pmeiList = pmeiCur->pmeiNext;
    }

    // And delete it

    SAFE_DESTROYENDPOINT(pmeiCur);

    // See if we are done

    if (NULL != pmdiCur->pmeiList)
    {
        // Report the item as found

        hr = S_OK;
        goto Exit;
    }

    // This was the last reference, remove the device

    if (NULL != pmdiLast)
    {
        pmdiLast->pmdiNext = pmdiCur->pmdiNext;
    }
    else
    {
        m_pmdiList = pmdiCur->pmdiNext;
    }

    // And destroy the device

    hr = DestroyDevice(pmdiCur);
    if (FAILED(hr))
    {
        goto Exit;
    }

Exit:
    LeaveCriticalSection(&m_csDeviceList);
    return hr;
}


/*
 *  CMTPIPUPnPDevice::CheckForActiveDevice
 *
 *  Uses the MAC address to check the list for an active device.  Regardless
 *  of the number of addresses associated with a particular endpoint, only
 *  one device is created.
 *
 *  Arguments:
 *      const BYTE*     pbMAC               MAC address to check
 *      DWORD           cbMAC               Length of MAC address
 *      const SOCKADDR* psaAddr             Address bound to MAC
 *      DWORD           cbAddr              Length of address
 *      BOOL            fAddAddress         TRUE if address should be added
 *                                            to a located entry
 *
 *  Returns:
 *      HRESULT     S_OK if device is active, S_FALSE if not
 *
 */

HRESULT CMTPIPUPnPDevice::CheckForActiveDevice(const BYTE* pbMAC, DWORD cbMAC,
                            const SOCKADDR* psaAddr, DWORD cbAddr,
                            BOOL fAddAddress)
{
    HRESULT             hr;
    MTPIPEndpointInfo*  pmeiCur;
    MTPIPEndpointInfo*  pmeiNew = NULL;
    MTPIPDeviceInfo*    pmdiMatch;

    // Make sure we do not have thread issues

    EnterCriticalSection(&m_csDeviceList);

    // MAC address is required- if address is to be added socket address is
    //  required as well

    if ((NULL == pbMAC) || (0 == cbMAC) ||
        (fAddAddress && ((NULL == psaAddr) || (0 == cbAddr))))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Try to locate a matching device

    hr = FindDeviceInternal(pbMAC, cbMAC, MTPIPFINDFLAGS_MAC_ADDRESS,
                            &pmdiMatch);
    if (S_OK != hr)
    {
        goto Exit;
    }

    // Assume we do not have a matching address

    pmeiCur = NULL;

    // We have a matching device- see if we also already have a matching
    //  address

    if (NULL != psaAddr)
    {
        // Loop through and check each address

        for (pmeiCur = pmdiMatch->pmeiList;
                NULL != pmeiCur;
                pmeiCur = pmeiCur->pmeiNext)
        {
            // See if we have a match

            hr = PSLIsEqualIPAddress(psaAddr, cbAddr,
                            (SOCKADDR*)&(pmeiCur->saAddress),
                            pmeiCur->cbAddress,
                            TRUE);
            if (FAILED(hr))
            {
                goto Exit;
            }

            // Stop searching if we found one

            if (S_OK == hr)
            {
                break;
            }
        }
    }

    // If we do not need to add the address or we already have a matching
    //  address in the list we are done

    if (!fAddAddress || (NULL != pmeiCur))
    {
        // Figure out how to report wheter or not the entry was found

        if (NULL != psaAddr)
        {
            hr = (NULL != pmeiCur) ? S_OK : S_FALSE;
        }
        else
        {
            hr = S_OK;
        }
        goto Exit;
    }

    // We should not have found a matching address

    PSLASSERT(NULL == pmeiCur);

    // Create an endpoint to handle this address

    hr = CreateEndpoint(psaAddr, cbAddr, &pmeiNew);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // And add it to the list of addresses

    pmeiNew->pmdiDevice = pmdiMatch;
    pmeiNew->pmeiNext = pmdiMatch->pmeiList;
    pmdiMatch->pmeiList = pmeiNew;
    pmeiNew = NULL;

    // Report found

    hr = S_OK;

Exit:
    LeaveCriticalSection(&m_csDeviceList);
    SAFE_DESTROYENDPOINT(pmeiNew);
    return hr;
}


/*
 *  CMTPIPUPnPDevice::CreateEndpoint
 *
 *  Creates an endpoint and the socket associated with it
 *
 *  Arguments:
 *      const SOCKADDR* psaAddr             Address to find
 *      DWORD           cbAddr              Length of the address
 *      MTPIPEndpointInfo**
 *                      ppmeiNew            Resulting endpoint info
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::CreateEndpoint(const SOCKADDR* psaAddr, DWORD cbAddr,
                            MTPIPEndpointInfo** ppmeiNew)
{
    INT                 errSock;
    WSAEVENT            hEvent = WSA_INVALID_EVENT;
    HRESULT             hr;
    MTPIPEndpointInfo*  pmeiNew = NULL;
    SOCKET              sock = INVALID_SOCKET;

    // Make sure we do not get interrupted

    EnterCriticalSection(&m_csDeviceList);

    // Clear result for safety

    if (NULL != ppmeiNew)
    {
        *ppmeiNew = NULL;
    }

    // Validate arguments

    if ((NULL == psaAddr) || (0 == cbAddr) || (NULL == ppmeiNew))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Make sure we have space to store the address

    PSLASSERT(sizeof(SOCKADDR_STORAGE) >= cbAddr);
    if (sizeof(SOCKADDR_STORAGE) < cbAddr)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Create a socket on this destination so that we can listen for connections

    sock = socket(psaAddr->sa_family, SOCK_STREAM, IPPROTO_TCP);
    if (INVALID_SOCKET == sock)
    {
        hr = HRESULT_FROM_WIN32(WSAGetLastError());
        goto Exit;
    }

    // Bind it to the desired address

    errSock = bind(sock, psaAddr, cbAddr);
    if (SOCKET_ERROR == errSock)
    {
        hr = HRESULT_FROM_WIN32(WSAGetLastError());
        goto Exit;
    }

    // Create an event so that we can receive notifications from this
    //  socket

    hEvent = WSACreateEvent();
    if (WSA_INVALID_EVENT == hEvent)
    {
        hr = HRESULT_FROM_WIN32(WSAGetLastError());
        goto Exit;
    }

    // Request listen notifications from the socket

    errSock = WSAEventSelect(sock, hEvent, FD_ACCEPT);
    if (SOCKET_ERROR == errSock)
    {
        hr = HRESULT_FROM_WIN32(WSAGetLastError());
        goto Exit;
    }

    // And get it started

    errSock = listen(sock, SOMAXCONN);
    if (SOCKET_ERROR == errSock)
    {
        hr = HRESULT_FROM_WIN32(WSAGetLastError());
        goto Exit;
    }

    // Create a new endpoint entry

    pmeiNew = NEW MTPIPEndpointInfo;
    if (NULL == pmeiNew)
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }
    ZeroMemory(pmeiNew, sizeof(*pmeiNew));
    pmeiNew->sock = INVALID_SOCKET;
    pmeiNew->hEvent = WSA_INVALID_EVENT;

    // And initialize it

    CopyMemory(&(pmeiNew->saAddress), psaAddr, cbAddr);
    pmeiNew->cbAddress = cbAddr;

    // Add the socket to the endpoint information

    pmeiNew->sock = sock;
    pmeiNew->hEvent = hEvent;
    sock = INVALID_SOCKET;
    hEvent= WSA_INVALID_EVENT;

    // Return the endpoint

    *ppmeiNew = pmeiNew;
    pmeiNew = NULL;

    // Update the endpoint list

    m_fEnumerateEndpoints = TRUE;
    m_cActiveEndpoints++;
    m_cMaxEndpoints = max(m_cActiveEndpoints, m_cMaxEndpoints);

    // Report success

    hr = S_OK;

Exit:
    LeaveCriticalSection(&m_csDeviceList);
    SAFE_DESTROYENDPOINT(pmeiNew);
    SAFE_CLOSESOCKET(sock);
    SAFE_WSACLOSEEVENT(hEvent);
    return hr;
}


/*
 *  CMTPIPUPnPDevice::DestroyEndpoint
 *
 *  Destroys an endpoint structure
 *
 *  Arguments:
 *      MTPIPEndpointInfo*
 *                      pmeiDestroy         Endpoint to destroy
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::DestroyEndpoint(MTPIPEndpointInfo* pmeiDestroy)
{
    INT             errSock;
    HRESULT         hr;

    // Make sure we do not get interrupted

    EnterCriticalSection(&m_csDeviceList);

    // Make sure we have work to do

    if (NULL == pmeiDestroy)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Remember that we need to enumerate sockets

    m_fEnumerateEndpoints = TRUE;
    PSLASSERT(0 < m_cActiveEndpoints);
    m_cActiveEndpoints--;

    // See if we need to handle the socket

    if (INVALID_SOCKET != pmeiDestroy->sock)
    {
        // If we have a socket we better have an event

        PSLASSERT(WSA_INVALID_EVENT != pmeiDestroy->hEvent);

        // Stop receiving event notifications for this socket

        errSock = WSAEventSelect(pmeiDestroy->sock, pmeiDestroy->hEvent, 0);
        if (SOCKET_ERROR == errSock)
        {
            hr = HRESULT_FROM_WIN32(WSAGetLastError());
            goto Exit;
        }

        // Close the socket and the event

        closesocket(pmeiDestroy->sock);
        WSACloseEvent(pmeiDestroy->hEvent);
    }

    // Destroy the remining content

    delete pmeiDestroy;

    //  Report success

    hr = S_OK;

Exit:
    LeaveCriticalSection(&m_csDeviceList);
    return hr;
}


/*
 *  CMTPIPUPnPDevice::AddDevice
 *
 *  Adds a created device to the list of devices.
 *
 *  Arguments:
 *      const SOCKADDR* psaAddr             Address to find
 *      DWORD           cbAddr              Length of the address
 *      const BYTE*     pbMAC               MAC address associated with address
 *      DWORD           cbMAC
 *      LPCWSTR         szDeviceIdentifier  UPnP device identifier
 *
 *  Returns:
 *      HRESULT     S_OK if added, S_FALSE if already present
 *
 */

HRESULT CMTPIPUPnPDevice::AddDevice(const SOCKADDR* psaAddr, DWORD cbAddr,
                            const BYTE* pbMAC, DWORD cbMAC,
                            LPCWSTR szDeviceIdentifier)
{
    DWORD               cch;
    HRESULT             hr;
    MTPIPEndpointInfo*  pmeiNew = NULL;
    MTPIPDeviceInfo*    pmdiNew = NULL;

    // Make sure we do not get interrupted

    EnterCriticalSection(&m_csDeviceList);

    // Validate arguments

    if ((NULL == psaAddr) || (0 == cbAddr) || (NULL == pbMAC) || (0 == cbMAC))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // See if the device already exists

    hr = CheckForActiveDevice(pbMAC, cbMAC, psaAddr, cbAddr, TRUE);
    if (S_FALSE != hr)
    {
        hr = (S_OK == hr) ? S_FALSE : hr;
        goto Exit;
    }

    // Make sure we have space to store the address

    PSLASSERT(sizeof(SOCKADDR_STORAGE) >= cbAddr);
    if (sizeof(SOCKADDR_STORAGE) < cbAddr)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Create a new endpoint

    hr = CreateEndpoint(psaAddr, cbAddr, &pmeiNew);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // Figure out how long the identifier is

    cch = (NULL != szDeviceIdentifier) ? wcslen(szDeviceIdentifier) : 0;

    // Create a new device entry

    pmdiNew = (MTPIPDeviceInfo*)(NEW BYTE[sizeof(MTPIPDeviceInfo) +
                            ((cch + 1) * sizeof(WCHAR)) + cbMAC]);
    if (NULL == pmdiNew)
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }
    ZeroMemory(pmdiNew, sizeof(*pmdiNew));

    // And initialize the pointers

    pmdiNew->szDeviceID = (WCHAR*)((LPBYTE)pmdiNew + sizeof(MTPIPDeviceInfo));
    pmdiNew->pbMAC = (LPBYTE)(pmdiNew->szDeviceID) + ((cch + 1) * sizeof(WCHAR));

    // And fill up the info

    if (NULL != szDeviceIdentifier)
    {
        hr = StringCchCopy(pmdiNew->szDeviceID, cch + 1, szDeviceIdentifier);
    }
    else
    {
        pmdiNew->szDeviceID = L'\0';
    }
    CopyMemory(pmdiNew->pbMAC, pbMAC, cbMAC);
    pmdiNew->cbMAC = cbMAC;

    // Add in the endpoint information

    pmeiNew->pmdiDevice = pmdiNew;
    pmdiNew->pmeiList = pmeiNew;
    pmeiNew = NULL;

    // Add it at the front of the list

    pmdiNew->pmdiNext = m_pmdiList;
    m_pmdiList = pmdiNew;
    pmdiNew = NULL;

    // Report success

    hr = S_OK;

Exit:
    LeaveCriticalSection(&m_csDeviceList);
    if (NULL != pmdiNew)
    {
        DestroyDevice(pmdiNew);
    }
    SAFE_DESTROYENDPOINT(pmeiNew);
    return hr;
}


/*
 *  CMTPIPUPnPDevice::DestroyDevice
 *
 *  Destroys the device specified.
 *
 *  Arguments:
 *      MTPIPDeviceInfo*
 *                      pmdiDevice          Device to destroy
 *
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::DestroyDevice(MTPIPDeviceInfo* pmdiDevice)
{
    HRESULT             hr;
    MTPIPEndpointInfo*  pmeiCur;
    IUPnPRegistrar*     pUPnPRegistrar = NULL;
	BSTR				bstrDeviceId = NULL;

    // Make sure we do not get interrupted

    EnterCriticalSection(&m_csDeviceList);

    // Validate arguments

    if (NULL == pmdiDevice)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Unregister the device if necessary

    if (NULL != pmdiDevice->szDeviceID)
    {
        // Retrieve the UPnP Registrar

        hr = CoCreateInstance(CLSID_UPnPRegistrar, NULL, CLSCTX_LOCAL_SERVER,
                            IID_IUPnPRegistrar, (LPVOID*)&pUPnPRegistrar);
        if (FAILED(hr))
        {
            goto Exit;
        }

		bstrDeviceId = SysAllocString(pmdiDevice->szDeviceID);
		if (NULL == bstrDeviceId)
		{
			ASSERT(FALSE);
			hr = E_OUTOFMEMORY;
			goto Exit;
		}

        // And unregister this device

        hr = pUPnPRegistrar->UnregisterDevice(bstrDeviceId, TRUE);
        if (FAILED(hr))
        {
            goto Exit;
        }
    }

    // Walk through and delete any endpoints associated with this device

    for (pmeiCur = pmdiDevice->pmeiList;
            NULL != pmeiCur;
            pmeiCur = pmdiDevice->pmeiList)
    {
        // Move the list to the next element

        pmdiDevice->pmeiList = pmeiCur->pmeiNext;

        // And delete the current entry

        DestroyEndpoint(pmeiCur);
    }

    // Delete the device information

    SAFE_ARRAYDELETE(pmdiDevice);

    // Report success

    hr = S_OK;

Exit:
    LeaveCriticalSection(&m_csDeviceList);
    SAFE_RELEASE(pUPnPRegistrar);
	SAFE_SYSFREESTRING(bstrDeviceId);
    return hr;
}


/*
 *  CMTPIPUPnPDevice::CreateDevice
 *
 *  Creates and registers a UPnP device bound to the specified address
 *
 *  Arguments:
 *      const SOCKADDR* psaAddr             Address to find
 *      DWORD           cbAddr              Length of the address
 *
 *  Returns:
 *      HRESULT     S_OK on success, S_FALSE if already running
 *
 */

HRESULT CMTPIPUPnPDevice::CreateDevice(const SOCKADDR* psaAddr, DWORD cbAddr)
{
    BSTR                bstrDeviceIdentifier = NULL;
    BSTR                bstrMACAddress = NULL;
    BSTR                bstrResources = NULL;
    BSTR                bstrXMLDesc = NULL;
    DWORD               cbMAC;
    DWORD               cbDesc;
    DWORD               dwState;
    HRESULT             hr;
    DWORD               idxByte;
    BYTE*               pbMAC = NULL;
    IUPnPRegistrar*     pUPnPRegistrar = NULL;
	WCHAR				wszUUID[MAX_PATH] = {0};

    // Validate arguments- addresses must not be larger than a SOCKADDR_STORAGE

    if ((NULL == psaAddr) || (sizeof(SOCKADDR_STORAGE) < cbAddr))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // We only support IP addresses at this point

    if ((AF_INET != psaAddr->sa_family) && (AF_INET6 != psaAddr->sa_family))
    {
        hr = E_NOTIMPL;
        goto Exit;
    }

    // Make sure we were not handed an "any" address

    hr = PSLIsIPAddressAny(psaAddr, cbAddr);
    if (S_FALSE != hr)
    {
        hr = (S_OK == hr) ? E_INVALIDARG : hr;
        goto Exit;
    }

    // Validate state of the object

    dwState = (DWORD)InterlockedCompareExchange((LPLONG)&m_dwDeviceState,
                            MTPIPDEVICESTATE_READY,
                            MTPIPDEVICESTATE_UNKNOWN);
    PSLASSERT(MTPIPDEVICESTATE_READY == dwState);
    if (MTPIPDEVICESTATE_READY != dwState)
    {
        hr = E_UNEXPECTED;
        goto Exit;
    }

    // Retrieve the MAC address for the adapter to which this address is
    //  currently bound

    hr = PSLResolveMACAddress(psaAddr, cbAddr, &pbMAC, (PSLUINT32*)&cbMAC);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // If the device is already active then we do not want to do anymore work

    hr = CheckForActiveDevice(pbMAC, cbMAC, psaAddr, cbAddr, TRUE);
    if (S_FALSE != hr)
    {
        hr = (S_OK == hr) ? S_FALSE : hr;
        goto Exit;
    }

    // Retrieve the UPnP Registrar

    hr = CoCreateInstance(CLSID_UPnPRegistrar, NULL, CLSCTX_LOCAL_SERVER,
                            IID_IUPnPRegistrar, (LPVOID*)&pUPnPRegistrar);
    if (FAILED(hr))
    {
        PSLASSERT(FALSE);
        goto Exit;
    }

    // Generate a BSTR that can encodes the MAC address of this endpoint

    bstrMACAddress = SysAllocStringLen(NULL, ((cbMAC * 2) + 1));
    if (NULL == bstrMACAddress)
    {
        PSLASSERT(FALSE);
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    // And copy it over

    for (idxByte = 0; idxByte < cbMAC; idxByte++)
    {
        // Convert the upper and lower nibbles to strings

        bstrMACAddress[2 * idxByte] = ((pbMAC[idxByte] >> 4) < 0xa) ?
                            (pbMAC[idxByte] >> 4) + L'0' :
                            ((pbMAC[idxByte] >> 4) - 0xa) + L'a';
        bstrMACAddress[(2 * idxByte) + 1] = ((pbMAC[idxByte] & 0xf) < 0xa) ?
                            (pbMAC[idxByte] & 0xf) + L'0' :
                            ((pbMAC[idxByte] & 0xf) - 0xa) + L'a';
    }
    bstrMACAddress[2 * idxByte] = L'\0';

	hr = StringCchPrintfW(wszUUID, MAX_PATH,
            L"00000000-0000-0000-FFFF-%02X%02X%02X%02X%02X%02X",
            *(pbMAC+0), *(pbMAC+1), *(pbMAC+2), *(pbMAC+3),
            *(pbMAC+4), *(pbMAC+5));
    if (FAILED(hr))
    {
        goto Exit;
    }

    // Allocate a string to hold the device description

    cbDesc = PSLSTRARRAYSIZE(_RgchDeviceXML) + 1;
    cbDesc += wcslen(wszUUID) + 1 + wcslen(bstrMACAddress) + 1 +
              PSLSTRARRAYSIZE(_RgchServiceDesc) + 1;

    bstrXMLDesc = SysAllocStringLen(NULL, cbDesc);
    if (NULL == bstrXMLDesc)
    {
        ASSERT(FALSE);
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    //  And add the MAC into it
	hr = StringCchPrintf(bstrXMLDesc, cbDesc, _RgchDeviceXML,
                         wszUUID, bstrMACAddress,
                         _RgchServiceDesc);
    if (FAILED(hr))
    {
        ASSERT(FALSE);
        goto Exit;
    }

//    PSLDebugPrintfW(bstrXMLDesc);

	// Initialize the resource path

    hr = _GetResourcePath(&bstrResources);
    if (FAILED(hr))
    {
        goto Exit;
    }

	// Register the MTP-IP UPnP device with the UPnP service

    hr = _RegisterDevice(pUPnPRegistrar, wszUUID, bstrXMLDesc,
						 bstrResources, &bstrDeviceIdentifier);

    if (FAILED(hr))
    {
        PSLASSERT(FALSE);
        goto Exit;
    }

    // Add this device to our list of known devices

    hr = AddDevice(psaAddr, cbAddr, pbMAC, cbMAC, bstrDeviceIdentifier);
    if (FAILED(hr))
    {
        PSLASSERT(FALSE);
        // Make sure we unregister the device first

        pUPnPRegistrar->UnregisterDevice(bstrDeviceIdentifier, TRUE);

        // And then exit

        goto Exit;
    }

Exit:
    // Handle clean-up

    SAFE_SYSFREESTRING(bstrDeviceIdentifier);
    SAFE_SYSFREESTRING(bstrResources);
    SAFE_SYSFREESTRING(bstrXMLDesc);
    SAFE_FREERESOVLEDMACADDRESS(pbMAC);
    SAFE_SYSFREESTRING(bstrMACAddress);
    SAFE_RELEASE(pUPnPRegistrar);
    return hr;
}


/*
 *  CMTPIPUPnPDevice::RemoveDevice
 *
 *  Removes a reference to a device in the device list
 *
 *  Arguments:
 *      const SOCKADDR* psaAddr             Address to find
 *      DWORD           cbAddr              Length of the address
 *
 *  Returns:
 *      HRESULT     S_OK on success, S_FALSE if not found
 *
 */

HRESULT CMTPIPUPnPDevice::RemoveDevice(const SOCKADDR* psaAddr, DWORD cbAddr)
{
    return FindDeviceInternal(psaAddr, cbAddr,
                        (MTPIPFINDFLAGS_REMOVE | MTPIPFINDFLAGS_SOCKET_ADDRESS),
                        NULL);
}


/*
 *  CMTPIPUPnPDevice::DeviceThread
 *
 *  ThreadProp for managing and initializing the device independent of the
 *  hosting service thread.
 *
 *  Arguments:
 *      LPVOID          pvData              Thread data (this pointer)
 *
 *  Returns:
 *      (DWORD)HRESULT  S_OK on success
 *
 */

DWORD CMTPIPUPnPDevice::DeviceThread(LPVOID pvData)
{
    HRESULT         hr;

    // Validate arguments

    if (NULL == pvData)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Call back into the class scope

    hr = ((CMTPIPUPnPDevice*)pvData)->DeviceControl();

Exit:
    return (DWORD)hr;
}


/*
 *  CMTPIPUPnPDevice::DeviceControl
 *
 *  Core thread for controlling up the MTP-IP UPnP device.
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::DeviceControl()
{
    INT                 cbAddr;
    DWORD               cEvents;
    DWORD               cMaxEvents;
    DWORD               dwState;
    DWORD               dwWaitResult;
    BOOL                fDone = FALSE;
    BOOL                fShutdownCOM = FALSE;
    BOOL                fLeaveDeviceCS = FALSE;
    HRESULT             hr;
    MTPIPDeviceCmd*     pCommand = NULL;
    HANDLE*             phEvents = NULL;
    PSLSTATUS           ps;
    MTPIPDeviceInfo*    pmdiCur;
    MTPIPEndpointInfo*  pmeiEvent;
    MTPIPEndpointInfo** ppmeiActive = NULL;
    SOCKADDR_STORAGE    saAddr;
    SOCKET              sock = INVALID_SOCKET;

    // Validate state of the object

    dwState = (DWORD)InterlockedCompareExchange((LPLONG)&m_dwDeviceState,
                            MTPIPDEVICESTATE_INITIALIZING,
                            MTPIPDEVICESTATE_INITIALIZING);
    PSLASSERT(MTPIPDEVICESTATE_INITIALIZING == dwState);
    if (MTPIPDEVICESTATE_INITIALIZING != dwState)
    {
        hr = E_UNEXPECTED;
        goto Exit;
    }

    // Initialize OLE

    hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
    if (FAILED(hr))
    {
        goto Exit;
    }
    fShutdownCOM = TRUE;

    // All is well- change the state to running

    dwState = (DWORD)InterlockedExchange((LPLONG)&m_dwDeviceState,
                            MTPIPDEVICESTATE_READY);
    PSLASSERT(MTPIPDEVICESTATE_INITIALIZING == dwState);

    // We have no events right now

    cMaxEvents = 0;
    cEvents = 0;

    // Start the main command handling loop

    do
    {
        // Acquire the device critical section

        EnterCriticalSection(&m_csDeviceList);
        fLeaveDeviceCS = TRUE;

        // See if we need to re-enumerate the device list

        if (m_fEnumerateEndpoints)
        {
            // Figure out if we need to make new lists

            if ((cMaxEvents < (m_cMaxEndpoints + 1)) ||
                (NULL == phEvents) || (NULL == ppmeiActive))
            {
                // Destroy the old lists

                SAFE_ARRAYDELETE(phEvents);
                SAFE_ARRAYDELETE(ppmeiActive);

                // Allocate a new handle array

                phEvents = NEW HANDLE[m_cMaxEndpoints + 1];
                if (NULL == phEvents)
                {
                    hr = E_OUTOFMEMORY;
                    goto Exit;
                }

                // And a new endpoint list

                ppmeiActive = NEW MTPIPEndpointInfo*[m_cMaxEndpoints];
                if (NULL == ppmeiActive)
                {
                    hr = E_OUTOFMEMORY;
                    goto Exit;
                }

                // Stash the counters

                cMaxEvents = m_cMaxEndpoints + 1;
            }

            // Add the command event to the list

            phEvents[0] = m_hEventCommand;
            cEvents = 1;

            // Walk through each device in the list

            for (pmdiCur = m_pmdiList;
                    NULL != pmdiCur;
                    pmdiCur = pmdiCur->pmdiNext)
            {
                // And record the information for each endpoint

                for (pmeiEvent = pmdiCur->pmeiList;
                        NULL != pmeiEvent;
                        pmeiEvent = pmeiEvent->pmeiNext, cEvents++)
                {
                    // Add the event and a pointer to the information

                    phEvents[cEvents] = pmeiEvent->hEvent;
                    PSLASSERT(0 < cEvents);
                    ppmeiActive[cEvents - 1] = pmeiEvent;
                }
            }

            // Report that sockets are enumerated

            m_fEnumerateEndpoints = FALSE;
        }

        // Exit the critical section

        LeaveCriticalSection(&m_csDeviceList);
        fLeaveDeviceCS = FALSE;

        // Wait for the command event or a socket event to occur

        PSLASSERT(NULL != m_hEventCommand);
        dwWaitResult = WaitForMultipleObjects(cEvents, phEvents, FALSE,
                            INFINITE);
        switch (dwWaitResult)
        {
        case WAIT_FAILED:
            // Report the error

            hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
            goto Exit;

        case WAIT_TIMEOUT:
            // Should never timeout on an infinite wait

            PSLASSERT(FALSE);
            hr = E_UNEXPECTED;
            goto Exit;

        case WAIT_OBJECT_0:
            // Command pending- retrieve it

            while (!fDone)
            {
                // Retrieve the next command

                hr = GetNextCommand(&pCommand);
                if (FAILED(hr))
                {
                    goto Exit;
                }

                // Stop working if we have no more commands to process

                if (S_OK != hr)
                {
                    break;
                }

                // And figure out what to do
                //
                //  NOTE: Command cleanup is handled following the switch
                //  so any jump out of the current context may leave a
                //  command active and a SendCommand blocked.

                switch (pCommand->dwCommand)
                {
                case MTPIPDEVICECMD_INITIALIZE:
                    // Report all is well

                    hr = S_OK;
                    break;

                case MTPIPDEVICECMD_ADD_DEVICE:
                    // Create wew device entry

                    hr = CreateDevice((SOCKADDR*)pCommand->lParam,
                            pCommand->wParam);
                    PSLASSERT(SUCCEEDED(hr));
                    break;

                case MTPIPDEVICECMD_REMOVE_DEVICE:
                    // Remove the device

                    hr = RemoveDevice((SOCKADDR*)pCommand->lParam,
                            pCommand->wParam);
                    PSLASSERT(SUCCEEDED(hr));
                    break;

                case MTPIPDEVICECMD_SHUTDOWN:
                    // Stop handling commands

                    hr = S_OK;
                    fDone = TRUE;
                    break;
                }

                 // If the command has a completion event set the result
                 //  and signal completion

                if (NULL != pCommand->hEventCompletion)
                {
                    // Store the result as requested

                    pCommand->hrCommand = hr;

                    // And signal the completion

                    SetEvent(pCommand->hEventCompletion);
                }
                else
                {
                    // Delete the command

                    SAFE_DELETECOMMAND(pCommand);
                }
            }
            break;

        default:
            // Received a notification on one of the sockets- make sure
            //  we have valid information

            PSLASSERT((dwWaitResult - WAIT_OBJECT_0 - 1) <= m_cActiveEndpoints);

            // Extract the socket information

            pmeiEvent = ppmeiActive[dwWaitResult - WAIT_OBJECT_0 - 1];

            // Reset the event

            WSAResetEvent(pmeiEvent->hEvent);

            // Accept the next connection

            cbAddr = sizeof(saAddr);
            sock = accept(pmeiEvent->sock, (SOCKADDR*)&saAddr, &cbAddr);
            if (INVALID_SOCKET == sock)
            {
                break;
            }

            // Initialize MTP if necessary

            if (!m_fMTPInit)
            {
                // Initialize the MTP system

                ps = MTPInitialize(
                            MTPINITFLAG_RESPONDER | MTPINITFLAG_IP_TRANSPORT);
                if (PSL_FAILED(ps))
                {
                    closesocket(sock);
                    sock = INVALID_SOCKET;
                    break;
                }
                m_fMTPInit = TRUE;
            }

            // Bind the initiator connection

            PSLASSERT(0 < cbAddr);
            ps = MTPIPBindInitiator(sock, (SOCKADDR*)&saAddr,
                            (PSLUINT32)cbAddr, MTPIPTRANSPORTPARAM);
            if (PSL_FAILED(ps))
            {
                closesocket(sock);
            }
            sock = INVALID_SOCKET;
            break;
        }
    }
    while (!fDone);

    // Report success

    hr = S_OK;

Exit:
    // Set state correctly to stop additional operations

    InterlockedExchange((LPLONG)&m_dwDeviceState, SUCCEEDED(hr) ?
                            MTPIPDEVICESTATE_SHUTDOWN : MTPIPDEVICESTATE_ERROR);

    // Handle clean-up

    if (fLeaveDeviceCS)
    {
        LeaveCriticalSection(&m_csDeviceList);
    }
    EmptyCommandQueue();
    RemoveDevice(NULL, 0);
    SAFE_ARRAYDELETE(phEvents);
    SAFE_ARRAYDELETE(ppmeiActive);
    SAFE_CLOSESOCKET(sock);
    if (fShutdownCOM)
    {
        CoUninitialize();
    }
    return hr;
}


/*
 *  CMTPIPUPnPDevice::CreateInstance
 *
 *  Creates an instance of the object.
 *
 *  Arguments:
 *      CMTPIPUPnPDevice**
 *                      ppMTPIPDevice       Return buffer for instance
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::CreateInstance(CMTPIPUPnPDevice** ppMTPIPDevice)
{
    HRESULT             hr;
    CMTPIPUPnPDevice*   pNewDevice = NULL;

    // Clear result for safety

    if (NULL != ppMTPIPDevice)
    {
        *ppMTPIPDevice = NULL;
    }

    // Validate arguments

    if (NULL == ppMTPIPDevice)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Create a new instance of the object

    pNewDevice = NEW CMTPIPUPnPDevice;
    if (NULL == pNewDevice)
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    // And initialize it

    hr = pNewDevice->Initialize();
    if (FAILED(hr))
    {
        goto Exit;
    }

    // Return the new device

    *ppMTPIPDevice = pNewDevice;
    pNewDevice = NULL;

    // Report success

    hr = S_OK;

Exit:
    SAFE_RELEASE(pNewDevice);
    return hr;
}


/*
 *  CMTPIPUPnPDevice::Initialize
 *
 *  IUPnPDeviceControl Initialize method.  Called when device registration is
 *  complete.
 *
 *  Arguments:
 *      BSTR            bstrXMLDesc         XML description for device
 *      BSTR            bstrDeviceIdentifier
 *                                          Device specific initialization
 *                                            string
 *      BSTR            bstrInitString      Device identifier from
 *                                            IUPnPRegistrar::RegisterDevice
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::Initialize(BSTR bstrXMLDesc,
                            BSTR bstrDeviceIdentifier, BSTR bstrInitString)
{
    return S_OK;
    bstrXMLDesc;
    bstrDeviceIdentifier;
    bstrInitString;
}


/*
 *  CMTPIPUPnPDevice::GetServiceObject
 *
 *  IUPnPDeviceControl callback to retrieve the service object for the
 *  specified service.
 *
 *  Arguments:
 *      BSTR            bstrUDN             Device UDN
 *      BSTR            bstrServiceID       Service ID requested
 *      IDispatch**     ppdispService       Service IDispatch interface
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::GetServiceObject(BSTR bstrUDN, BSTR bstrServiceId,
                            IDispatch** ppdispService)
{
    HRESULT         hr;

    // Clear result for safety

    if (NULL != ppdispService)
    {
        *ppdispService = NULL;
    }

    // Validate Arguments

    if ((NULL == bstrUDN) || (NULL == bstrServiceId) || (NULL == ppdispService))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Report not implemented for now

    hr = E_NOTIMPL;

Exit:
    return hr;
}

/*
 *  CMTPIPUPnPDevice::BindAddress
 *
 *  Binds the provided address to a UPnP device.
 *
 *  Arguments:
 *      const SOCKADDR* psaAddr             Address to find
 *      DWORD           cbAddr              Length of the address
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::BindAddress(const SOCKADDR* psaAddr, DWORD cbAddr)
{
    HRESULT         hr;

    // Validate Arguments- addresses must fit in a SOCKADDR_STORAGE

    if ((NULL == psaAddr) || (sizeof(SOCKADDR_STORAGE) < cbAddr))
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // We only support IP addresses at this point

    if ((AF_INET != psaAddr->sa_family) && (AF_INET6 != psaAddr->sa_family))
    {
        hr = E_NOTIMPL;
        goto Exit;
    }

    // Issue a create device command

    hr = SendCommand(MTPIPDEVICECMD_ADD_DEVICE, cbAddr, (LPARAM)psaAddr);
    if (FAILED(hr))
    {
        goto Exit;
    }

Exit:
    return hr;
}


/*
 *  CMTPIPUPnPDevice::UnbindAddress
 *
 *  Unbinds one or all of the known addresses
 *
 *  Arguments:
 *      const SOCKADDR* psaAddr             Address to find
 *      DWORD           cbAddr              Length of the address
 *
 *  Returns:
 *      HRESULT     S_OK on success
 *
 */

HRESULT CMTPIPUPnPDevice::UnbindAddress(const SOCKADDR* psaAddr, DWORD cbAddr)
{
    // Issue a destroy device command- note that psaAddr and cbAddr may be
    //  NULL (unbind all)

    return SendCommand(MTPIPDEVICECMD_REMOVE_DEVICE, cbAddr, (LPARAM)psaAddr);
}


/*
 *  CMTPIPUPnPDevice::IsLocalUDN
 *
 *  Returns S_OK if the UDN provided is to a local device.
 *
 *  Arguments:
 *      LPCWSTR         szUDN               UDN to check
 *
 *  Returns:
 *      HRESULT     S_OK if local, S_FALSE if not
 *
 */

HRESULT CMTPIPUPnPDevice::IsLocalUDN(LPCWSTR szUDN)
{
    DWORD               cbMAC;
    DWORD               cchMAC;
    HRESULT             hr;
    WCHAR               rgchMAC[17];
    BYTE                rgbMAC[8];

    // Validate arguments

    if (NULL == szUDN)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Convert to a MAC string

    hr = MTPIPUDNToMACAddress((PSLCWSTR)szUDN, (PSLWCHAR*)rgchMAC,
                            PSLARRAYSIZE(rgchMAC), (PSLUINT32*)&cchMAC);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // And the string to a MAC

    hr = _MACStringToBytes(rgchMAC, rgbMAC, PSLARRAYSIZE(rgbMAC), &cbMAC);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // See if the device is known

    hr = CheckForActiveDevice(rgbMAC, cbMAC, NULL, 0, FALSE);
    if (FAILED(hr))
    {
        goto Exit;
    }

Exit:
    return hr;
}

//
// UPnPHost always overrides the supplied UUID in the discovery XML
// with a generated one. This function works-around this behavior and
// sets up UPnP to report the original UUID as the UDN.
// We do this by registering and unregistering the device temporarily
// and then manipulating the registry directly with the original UUID.
// Re-registering the device then picks up the original UUID.
//

HRESULT _RegisterDevice(
            IUPnPRegistrar* pRegistrar,
            WCHAR* szOrgDeviceId,
            BSTR bstrXMLDesc,
            BSTR bstrResourcePath,
            BSTR* pbstrDeviceId)
{
    HRESULT hr;
    BSTR bstrDeviceId = NULL;
	BSTR bstrProgID = NULL;
	BSTR bstrContainerId = NULL;
    WCHAR wszMappingKey[MAX_PATH] = {0};
    WCHAR wszNewMapping[MAX_PATH] = {0};
    HKEY hKeyMapping = NULL;
    IUPnPReregistrar* pReregistrar = NULL;
    LONG lResult;
	BSTR bstrInitString = NULL;

	if (NULL == pRegistrar || NULL == szOrgDeviceId)
    {
        hr = E_POINTER;
        goto Exit;
    }

    bstrProgID = SysAllocString(UPNPDEVICECONTROL_PROGID);
    if (NULL == bstrProgID)
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    bstrInitString = SysAllocString(UPNPDEVICECONTROL_INITSTRING);
    if (NULL == bstrInitString)
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    bstrContainerId = SysAllocString(UPNPDEVICECONTROL_CONTAINERID);
    if (NULL == bstrContainerId)
    {
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    // Register the device so that the registry keys are created

    hr = pRegistrar->RegisterDevice(bstrXMLDesc,
                                    bstrProgID,
                                    bstrInitString,
                                    bstrContainerId,
                                    bstrResourcePath,
                                    UPNPDEVICECONTROL_LIFETIME,
                                    &bstrDeviceId);
    if (FAILED(hr))
    {
        goto Exit;
    }

    //  Unregister the device temporarily so we can access
    //  the registry entries created for it
    hr = pRegistrar->UnregisterDevice(bstrDeviceId, FALSE);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // Create registry key path
    hr = StringCchPrintf(wszMappingKey, MAX_PATH,
                         _RgchUDNMappingKeyFormat,
                         (LPWSTR)bstrDeviceId,
                         szOrgDeviceId);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // Create new value to place in registry
    hr = StringCchPrintfW(wszNewMapping, MAX_PATH,
                          _RgchUDNMappingKeyValueFormat,
                          szOrgDeviceId);
    if (FAILED(hr))
    {
        goto Exit;
    }

    //  Manipulate the registry at
    //  HKLM\Software\UPnP Device Host\Description\deviceid
    //  We'll change the udn mapping under
    //  'UDN Mappings\uuid:orgdeviceid' from the
    //  new one back to the old one

    //
    // Open the key
    //
    lResult = RegOpenKeyExW(HKEY_LOCAL_MACHINE,
                            wszMappingKey, 0,
                            KEY_QUERY_VALUE | KEY_SET_VALUE,
                            &hKeyMapping);

    if (lResult != ERROR_SUCCESS)
    {
        hr = HRESULT_FROM_WIN32(lResult);
        goto Exit;
    }

    // Set value to new value
    lResult = RegSetValueExW(hKeyMapping, NULL, 0, REG_SZ,
                (const BYTE*) wszNewMapping,
                (DWORD)((wcslen(wszNewMapping) + 1) * sizeof(WCHAR)));
    if (lResult != ERROR_SUCCESS)
    {
        hr = HRESULT_FROM_WIN32(lResult);
        goto Exit;
    }

    // Close the open key
    SAFE_REGCLOSEKEY(hKeyMapping);

    // Get interface to re-register the device
    hr = pRegistrar->QueryInterface(IID_IUPnPReregistrar,
                                    (void**) &pReregistrar);
    if (FAILED(hr))
    {
        goto Exit;
    }

    // Get interface to re-register the device
    hr = pReregistrar->ReregisterDevice(
                            bstrDeviceId,
                            bstrXMLDesc,
                            bstrProgID,
                            bstrInitString,
                            bstrContainerId,
                            bstrResourcePath,
                            UPNPDEVICECONTROL_LIFETIME);
    if (FAILED(hr))
    {
        // If there was a failure, provide hints to the user on how to fix it
        if (hr == E_INVALIDARG)
        {
            PSLTraceError("IUPnPReregistrar::ReregisterDevice failed with hr=0x%08x", hr);
            PSLTraceError("------------------------------------------------------------");
            PSLTraceError("This problem may have occurred due to an incorrect shutdown");
            PSLTraceError("Please try the following steps to recover:");
            PSLTraceError("1. sc stop upnphost");
            PSLTraceError("2. reg delete \"HKLM\\Software\\Microsoft\\UPnP Device Host\\Description\" /f");
            PSLTraceError("3. reg delete \"HKLM\\Software\\Microsoft\\UPnP Device Host\\Devices\" /f");
            PSLTraceError("4. sc start upnphost");
            PSLTraceError("------------------------------------------------------------");
        }
        goto Exit;
    }

    // Return the identifier back to the caller if everything went ok
    *pbstrDeviceId = bstrDeviceId;

Exit:
    if (FAILED(hr) && bstrDeviceId)
    {
        // Since we failed, we unregister the
        // device permanently to clean up correctly
        (void) pRegistrar->UnregisterDevice(bstrDeviceId, TRUE);
    }
    SAFE_REGCLOSEKEY(hKeyMapping);
    SAFE_RELEASE(pReregistrar);
	SAFE_SYSFREESTRING(bstrProgID);
	SAFE_SYSFREESTRING(bstrContainerId);
	SAFE_SYSFREESTRING(bstrInitString);
    return hr;
}

HRESULT _GetResourcePath(BSTR* pbstrResourcePath)
{
    HRESULT hr;
    WCHAR* pwchar;
    DWORD dwFileAttributes;
    BSTR bstrResourcePath = NULL;
    WCHAR wszResourcePath[MAX_PATH] = {0};

    if (NULL != pbstrResourcePath)
    {
        *pbstrResourcePath = NULL;
    }

    if (NULL == pbstrResourcePath)
    {
        hr = E_INVALIDARG;
        goto Exit;
    }

    // Compute resource path for service description XML file -
    if (GetModuleFileName(NULL, wszResourcePath, MAX_PATH) == 0)
    {
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        goto Exit;
    }

    pwchar = wcsrchr(wszResourcePath, L'\\');
    if (NULL == pwchar)
    {
        ASSERT(!"Invalid module Path");
        hr = E_UNEXPECTED;
        goto Exit;
    }

    *(pwchar + 1) = L'\0';

    dwFileAttributes = GetFileAttributes(wszResourcePath);
    if (INVALID_FILE_ATTRIBUTES == dwFileAttributes)
    {
        PSLASSERT(!"MTPIPService.XML not found");
        hr = HRESULT_FROM_WIN32(SAFE_GETLASTERROR());
        goto Exit;
    }

	// Initialize the resource path

    bstrResourcePath = SysAllocString(wszResourcePath);
    if (NULL == bstrResourcePath)
    {
        ASSERT(FALSE);
        hr = E_OUTOFMEMORY;
        goto Exit;
    }

    *pbstrResourcePath = bstrResourcePath;
    bstrResourcePath = NULL;

    hr = S_OK;
Exit:
    SAFE_SYSFREESTRING(bstrResourcePath);
    return hr;
}
