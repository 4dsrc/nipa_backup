/*
 *  MTPIPResponderPrecomp.h
 *
 *  Precompiled header file for MTP-IP Service support.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPIPRESPONDERPRECOMP_H_
#define _MTPIPRESPONDERPRECOMP_H_

// PSL Helpers

#include "PSLWindows.h"
#include "PSL.h"

// Additional Windows APIs

#pragma warning(push)
#pragma warning(disable: 4995) /* name was marked as #pragma deprecated */
#pragma warning(disable: 4127) /* conditional expression is constant */

#include <winsock2.h>
#include <ws2tcpip.h>

#pragma warning(pop)

#include <upnp.h>
#include <upnphost.h>

// MTP-IP Support

#include "MTP.h"
#include "MTPIPTransport.h"
#include "MTPLoader.h"

// Helper Functions

#include "PSLUnknown.h"
#include "PSLWinSafe.h"
#include "PSLIPHelper.h"

#endif  // _MTPIPRESPONDERPRECOMP_H_

