//+-------------------------------------------------------------------------
//
//  Microsoft Windows
//
//  Copyright (c) 2009 Microsoft Corporation
//  All rights reserved.
//
//  Windows reference code is licensed under the Microsoft Windows Portable
//  Device Enabling Kit for Media Transfer Protocol (MTP).
//
//  File:       vbckhash.cpp
//
//--------------------------------------------------------------------------

#include "MTPIPResponderPrecomp.h"
#include "vbckhash.h"

//////////////////////////////////////////////////////////////////////////////
DWORD CVBucketHash::Hash( const DWORD& Key )
{
    if( !m_fnHash )
    {
        return( Key % m_dwTableSize );
    }
    else
    {
        return( m_fnHash( Key, m_dwTableSize, m_dwHashParam ) );
    }
}

//////////////////////////////////////////////////////////////////////////////
CVBucketHash::CVBucketHash()
{
    m_pTable = NULL;
    m_dwTableSize = 0;
    m_dwEntries = 0;
    m_pNodePool = NULL;
    m_fnHash = NULL;
}

//////////////////////////////////////////////////////////////////////////////
CVBucketHash::~CVBucketHash()
{
    delete [] m_pTable;
    delete m_pNodePool;
}

//////////////////////////////////////////////////////////////////////////////
HRESULT CVBucketHash::Initialize( DWORD dwTableSize, PFNHASHINGFUNCTION pfnHashingFunction, DWORD dwParam )
{
    if( m_pTable )
    {
        return( E_UNEXPECTED );
    }
    if( !dwTableSize )
    {
        return( E_INVALIDARG );
    }

    m_pTable = NEW NODE *[ dwTableSize ];
    if( !m_pTable )
    {
        return( E_OUTOFMEMORY );
    }

    ZeroMemory( m_pTable, ( sizeof(NODE *) * dwTableSize ) );

    m_pNodePool = NEW CVCellPool;
    if( !m_pNodePool ||
        FAILED( m_pNodePool->Initialize( sizeof(NODE), dwTableSize, 1, FALSE ) ) )
    {
        delete m_pNodePool;
        m_pNodePool = NULL;
        return( E_OUTOFMEMORY );
    }

    m_fnHash = pfnHashingFunction;
    m_dwTableSize = dwTableSize;
    m_dwEntries = 0;
    m_dwHashParam = dwParam;
    m_pCursor = NULL;

    return( S_OK );
}

//////////////////////////////////////////////////////////////////////////////
HRESULT CVBucketHash::Insert( DWORD Key, const void *pValue )
{
    //
    // Find a node to use in the bucket
    //
    NODE *pNew;

    if( FAILED( m_pNodePool->AcquireCell( (void**)&pNew ) ) )
    {
        return( E_OUTOFMEMORY );
    }

    pNew->Key = Key;
    pNew->pValue = (void*)pValue;

    //
    // Find the right bucket to use
    //
    DWORD dwHash = Hash( Key );

    pNew->pNext = m_pTable[dwHash];
    m_pTable[dwHash] = pNew;

    m_dwEntries++;

    return( S_OK );
}

//////////////////////////////////////////////////////////////////////////////
DWORD CVBucketHash::RemoveAll()
{
    if( !m_pTable )
    {
        return( 0 );
    }

    m_pCursor = NULL;

    if( FAILED( m_pNodePool->Initialize( sizeof(NODE), m_dwTableSize, 1, FALSE ) ) )
    {
        delete m_pNodePool;
        m_pNodePool = NULL;
        return( 0 );
    }

    ZeroMemory( m_pTable, ( sizeof(NODE *) * m_dwTableSize ) );

    DWORD dwRemoved = m_dwEntries;
    m_dwEntries = 0;

    return( dwRemoved );
}

//////////////////////////////////////////////////////////////////////////////
HRESULT CVBucketHash::Find( DWORD Key, void **ppValue )
{
    if( !m_pTable )
    {
        return( E_UNEXPECTED );
    }

    DWORD dwHash = Hash( Key );

    NODE *pNode = m_pTable[dwHash];

    while( pNode )
    {
        if( pNode->Key == Key )
        {
            *ppValue = pNode->pValue;
            break;
        }
        pNode = pNode->pNext;
    }

    return( pNode ? S_OK : E_FAIL );
}

//////////////////////////////////////////////////////////////////////////////
HRESULT CVBucketHash::Remove( DWORD Key, void **ppValue )
{
    if( !m_pTable )
    {
        return( E_UNEXPECTED );
    }

    DWORD dwHash = Hash( Key );

    NODE *pPrev = NULL;
    NODE *pNode = m_pTable[dwHash];

    while( pNode )
    {
        if( pNode->Key == Key )
        {
            m_pCursor = NULL;
            *ppValue = pNode->pValue;
            m_dwEntries--;

            if( pPrev )
            {
                pPrev->pNext = pNode->pNext;
            }
            else
            {
                m_pTable[dwHash] = pNode->pNext;
            }

            m_pNodePool->ReleaseCell( (void*)pNode );
            break;
        }

        pPrev = pNode;
        pNode = pNode->pNext;
    }

    return( pNode ? S_OK : E_FAIL );
}

//////////////////////////////////////////////////////////////////////////////
BOOL CVBucketHash::GetFirst( DWORD* pKey, void **ppValue )
{
    PSLASSERT( pKey && ppValue );
    if( !pKey || !ppValue || !m_pTable )
    {
        return( FALSE );
    }

    BOOL fLookingForNext = FALSE;

    for( DWORD i = 0; i < m_dwTableSize; i++ )
    {
        if( NULL == m_pTable[i] )
        {
            continue;
        }
        if( fLookingForNext )
        {
            m_pCursor = m_pTable[i];
            return( TRUE );
        }

        *pKey = m_pTable[i]->Key;
        *ppValue = m_pTable[i]->pValue;

        m_pCursor = m_pTable[i]->pNext;

        if( !m_pCursor )
        {
            fLookingForNext = TRUE;
            continue;
        }

        return( TRUE );
    }

    return( fLookingForNext );
}

//////////////////////////////////////////////////////////////////////////////
BOOL CVBucketHash::GetNext( DWORD* pKey, void **ppValue )
{
    PSLASSERT( pKey && ppValue );
    if( !pKey || !ppValue || !m_pTable || !m_pCursor )
    {
        return( FALSE );
    }

    *pKey = m_pCursor->Key;
    *ppValue = m_pCursor->pValue;

    m_pCursor = m_pCursor->pNext;

    if( !m_pCursor )
    {
        for( DWORD i = Hash( *pKey ) + 1; i < m_dwTableSize; i++ )
        {
            if( m_pTable[i] )
            {
                m_pCursor = m_pTable[i];
                break;
            }
        }
    }

    return( TRUE );
}
