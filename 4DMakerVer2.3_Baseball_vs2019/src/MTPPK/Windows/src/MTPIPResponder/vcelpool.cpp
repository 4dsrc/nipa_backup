//  Copyright (c) 2009 Microsoft Corporation
//  All rights reserved.
//
//  Windows reference code is licensed under the Microsoft Windows Portable
//  Device Enabling Kit for Media Transfer Protocol (MTP).

#include "MTPIPResponderPrecomp.h"
#include "vcelpool.h"

#ifndef MAKESIG
#define MAKESIG( ch0, ch1, ch2, ch3 )                           \
                ( (DWORD)(BYTE)(ch0) | ( (DWORD)(BYTE)(ch1) << 8 ) |    \
                ( (DWORD)(BYTE)(ch2) << 16 ) | ( (DWORD)(BYTE)(ch3) << 24 ) )
#endif

#define SIG_FREE_CELL               g_sigFreeCell
#define SIG_USED_CELL               g_sigUsedCell
#define SIG_FREE_PAGE               g_sigFreePage
#define SIG_PARTIALLY_FILLED_PAGE   g_sigPFPage
#define SIG_COMPLETELY_FILLED_PAGE  g_sigCFPage

DWORD g_sigFreeCell = MAKESIG( 'F','R','E','E' );
DWORD g_sigUsedCell = MAKESIG( 'U','S','E','D' );
DWORD g_sigFreePage = MAKESIG( 'F','R','P','G' );
DWORD g_sigPFPage   = MAKESIG( 'P','F','P','G' );
DWORD g_sigCFPage   = MAKESIG( 'C','F','P','G' );

// setting this to 1 will place cells at end of free list which degrades performance
#define PUT_CELLS_AT_TAIL_OF_LIST       0   // only in DBG builds

// seeting this to 1 will use system's new and delete instead of cell pools.
#define DONT_USE_CELLPOOL               0   // only in DBG builds

// setting this to 1 will generate random alloc pattern
#define GENERATE_RANDOM_ALLOC_PATTERN   0   // only in DBG builds

#if DBG
static LONG g_NextAllocPattern = 1;         // only in DBG builds
#endif

#define CELLPOOL_PARANOIA               1   // only in DBG builds
#define FILL_CELL_WITH_DEBUG_PATTERN    1   // only in DBG builds
#define PATTERN_PAGES                   1   // only in DBG builds


#define ALLOC_PATTERN                   0xDD    // only in DBG builds
#define DELETE_PATTERN                  0xFF    // only in DBG builds
#define ALLOC_BUFFER_PATTERN            0xCD    // only in DBG builds
#define DELETE_BUFFER_PATTERN           0xEF    // only in DBG builds


#define m_pFreePages                    m_rgPages[0]
#define m_pPartiallyFilledPages         m_rgPages[1]
#define m_pCompletelyFilledPages        m_rgPages[2]

#if DBG
#define VCELLPOOLASSERT( exp )   if( !( exp ) ) { DWORD *p = NULL; *p = 0; }
#else
#define VCELLPOOLASSERT( exp )
#endif

//////////////////////////////////////////////////////////////////////
// Public Methods

//////////////////////////////////////////////////////////////////////////////
CVCellPool::CVCellPool()
{
    ZeroMemory( this, sizeof(*this) );
    m_fInitialized = FALSE;
    m_cbTotalMemoryUsed = 0;
#if DBG
    m_AllocPattern = ALLOC_PATTERN;
    m_fInUse = FALSE;
#endif // DBG
}

//////////////////////////////////////////////////////////////////////////////
CVCellPool::~CVCellPool()
{
    _Cleanup();
}

//////////////////////////////////////////////////////////////////////////////
HRESULT CVCellPool::Initialize( DWORD cbCellSize,
                                DWORD nCellsPerPage,
                                DWORD nInitialPages,
                                BOOL  fFreeUnusedPages,
                                DWORD cbCellAlignment )
{
    if( ( cbCellSize < 1 )
    ||  ( nCellsPerPage < 1 )
    ||  ( cbCellAlignment & ( cbCellAlignment - 1 ) )
    ||  ( 0 == cbCellAlignment )
    ||  ( nInitialPages < 1 )
#ifndef WIN32
    ||  ( ( cbCellSize + sizeof(CELLHEADER) + sizeof(PAGEHEADER) ) > 65535 )
#endif  //!WIN32
      )
    {
        // brain-dead request
        VCELLPOOLASSERT( 0 );
        return( E_INVALIDARG );
    }

#if DBG
    VCELLPOOLASSERT( !m_fInUse );
    m_fInUse = TRUE;
#endif // DBG

    if( m_fInitialized )
    {
        // the client had better know what they're doing here...
        _Cleanup();
    }

#if DBG && GENERATE_RANDOM_ALLOC_PATTERN
    LONG lTemp = InterlockedIncrement( &g_NextAllocPattern );
    // Make sure it is odd so we can fail when we use it as a pointer.
    m_AllocPattern = ( ( lTemp * 2 ) + 1 ) & 0x000000FF;
#endif

    m_cbPageHeader = cbCellAlignment * ( ( sizeof(PAGEHEADER) + cbCellAlignment - 1 ) / cbCellAlignment );
    m_cbCellHeader = cbCellAlignment * ( ( sizeof(CELLHEADER) + cbCellAlignment - 1 ) / cbCellAlignment );

    m_fFreeUnusedPages = fFreeUnusedPages;

    if( fFreeUnusedPages && ( nInitialPages > 1 ) )
    {
        // the additional pages would just be freed
        nInitialPages = 1;
    }

    m_cbCellSize = cbCellSize;

    m_cbActualCellSize = cbCellAlignment * ( ( cbCellSize + m_cbCellHeader + cbCellAlignment - 1 ) / cbCellAlignment );

#ifdef  WIN32
    m_nCellsPerPage = nCellsPerPage;
#else
    m_nCellsPerPage = min( nCellsPerPage, ( 65535 - m_cbPageHeader ) / m_cbActualCellSize );
#endif  //WIN32

    m_nPageCount = 0;
    m_cbActualPageSize = m_cbPageHeader + m_nCellsPerPage * m_cbActualCellSize;
    m_fInitialized = 1;
#if DBG
    VCELLPOOLASSERT( m_cFreePages == 0 );
#endif
    PPAGEHEADER pNewPage;

    while( nInitialPages-- )
    {
        pNewPage = _AcquireCleanPage();
        if( NULL == pNewPage )
        {
            // this will set m_fInitialized to 0
            _Cleanup();
            break;
        }

        _AddToNewList( m_pFreePages, pNewPage );
#if DBG
        m_cFreePages++;
#endif
    }
#if DBG
    VCELLPOOLASSERT( m_nPageCount == ( m_cFreePages + m_cPartiallyFilledPages + m_cCompletelyFilledPages ) );
#endif

#if DBG
    m_fInUse = FALSE;
#endif // DBG

    return( m_fInitialized ? S_OK : E_OUTOFMEMORY );
}




//////////////////////////////////////////////////////////////////////////////
HRESULT CVCellPool::AcquireCell( void **ppCell )
{
#if DBG && DONT_USE_CELLPOOL
    *ppCell = ::NEW BYTE[ m_cbCellSize ];
    return( S_OK );
#endif // DONT_USE_CELLPOOL

    HRESULT hr = S_OK;
    PCELLHEADER pNewCell;
    PPAGEHEADER pPage;

#if DBG
    VCELLPOOLASSERT( !m_fInUse );
    m_fInUse = TRUE;
#endif // DBG

    if( !m_fInitialized )
    {
        VCELLPOOLASSERT(0);
        hr = E_UNEXPECTED;
        goto abort;
    }
    if( !ppCell )
    {
        VCELLPOOLASSERT(0);
        hr = E_INVALIDARG;
        goto abort;
    }

#if DBG
    VCELLPOOLASSERT( m_nPageCount == ( m_cFreePages + m_cPartiallyFilledPages + m_cCompletelyFilledPages ) );
#endif

    pPage = ( NULL != m_pPartiallyFilledPages ) ? m_pPartiallyFilledPages : m_pFreePages;


    //
    // Keep track of where we got the page to allocate the cell from
    //

    if( pPage == NULL )
    {
        pPage = _AcquireCleanPage();
        if( NULL == pPage )
        {
            VCELLPOOLASSERT(0);
            hr = E_OUTOFMEMORY;
            goto abort;
        }

#if VCELLPOOL_USE_SIGNATURE
        VCELLPOOLASSERT( SIG_FREE_PAGE == pPage->fccSignature );
#endif
        VCELLPOOLASSERT( pPage->nCellsInUse == 0 );

        pPage->nCellsInUse++;

        if( pPage->nCellsInUse == m_nCellsPerPage )
        {
#if VCELLPOOL_USE_SIGNATURE
            pPage->fccSignature = SIG_COMPLETELY_FILLED_PAGE;
#endif
            _AddToNewList( m_pCompletelyFilledPages, pPage );
#if DBG
            m_cCompletelyFilledPages++;
#endif
        }
        else
        {
#if VCELLPOOL_USE_SIGNATURE
            pPage->fccSignature = SIG_PARTIALLY_FILLED_PAGE;
#endif
            _AddToNewList( m_pPartiallyFilledPages, pPage );
#if DBG
            m_cPartiallyFilledPages++;
#endif
        }
    }
    else if( pPage == m_pPartiallyFilledPages )
    {
        pPage->nCellsInUse++;

        //
        // we are going to use a partially filled page. Allocating one cell
        // might put this past the limit & into the completely filled page list.
        //
#if VCELLPOOL_USE_SIGNATURE
        VCELLPOOLASSERT( SIG_PARTIALLY_FILLED_PAGE == pPage->fccSignature );
#endif

        if( pPage->nCellsInUse == m_nCellsPerPage )
        {
#if VCELLPOOL_USE_SIGNATURE
            pPage->fccSignature = SIG_COMPLETELY_FILLED_PAGE;
#endif
            _RemoveFromOldList( m_pPartiallyFilledPages, pPage );
#if DBG
            m_cPartiallyFilledPages--;
#endif
            _AddToNewList( m_pCompletelyFilledPages, pPage );
#if DBG
            m_cCompletelyFilledPages++;
#endif
        }
    }
    else
    {

        //
        // we are going to use a free page and allocate atleast
        // one cell. So set this page to be a partially filled page
        // or completely filled page depending on the CellsPerPage limit.
        //
#if VCELLPOOL_USE_SIGNATURE
        VCELLPOOLASSERT( SIG_FREE_PAGE == pPage->fccSignature );
#endif
        VCELLPOOLASSERT( pPage->nCellsInUse == 0 );

        pPage->nCellsInUse++;

        _RemoveFromOldList( m_pFreePages, pPage );
#if DBG
        m_cFreePages--;
#endif

        if( pPage->nCellsInUse == m_nCellsPerPage )
        {
#if VCELLPOOL_USE_SIGNATURE
            pPage->fccSignature = SIG_COMPLETELY_FILLED_PAGE;
#endif
            _AddToNewList( m_pCompletelyFilledPages, pPage );
#if DBG
            m_cCompletelyFilledPages++;
#endif

        }
        else
        {
#if VCELLPOOL_USE_SIGNATURE
            pPage->fccSignature = SIG_PARTIALLY_FILLED_PAGE;
#endif
            _AddToNewList( m_pPartiallyFilledPages, pPage );
#if DBG
            m_cPartiallyFilledPages++;
#endif
        }
    }

    pNewCell = pPage->pFreeChain;
    pPage->pFreeChain = pNewCell->pFreeLink;
#if DBG
    if ( pPage->pEndFreeChain == pNewCell )
    {
        pPage->pEndFreeChain = NULL;
    }
#endif

    pNewCell->pFreeLink = NULL;

#if VCELLPOOL_USE_SIGNATURE
    VCELLPOOLASSERT( SIG_FREE_CELL == pNewCell->fccSignature )
    pNewCell->fccSignature = SIG_USED_CELL;
#endif

    *ppCell = (void *)( (char*)pNewCell + m_cbCellHeader );

#if DBG && FILL_CELL_WITH_DEBUG_PATTERN
    VCELLPOOLASSERT( m_cbCellSize > 2 );

    FillMemory( *ppCell, m_cbCellSize, (BYTE)( m_AllocPattern & 0x000000FF ) );
#endif // DBG && FILL_CELL_WITH_DEBUG_PATTERN

#if DBG
    VCELLPOOLASSERT( m_nPageCount == ( m_cFreePages + m_cPartiallyFilledPages + m_cCompletelyFilledPages ) );
#endif

abort:
#if DBG
    m_fInUse = FALSE;
#endif // DBG

    return( hr );
}





//////////////////////////////////////////////////////////////////////////////
HRESULT CVCellPool::ReleaseCell( void *pClientCell )
{
#if DBG && DONT_USE_CELLPOOL
    ::delete [] pClientCell;
    return( S_OK );
#endif // DONT_USE_CELLPOOL

    HRESULT hr = S_OK;
    PCELLHEADER pCell;
    PPAGEHEADER pPage;

#if DBG
    VCELLPOOLASSERT( !m_fInUse );
    m_fInUse = TRUE;
#endif // DBG

    if( !m_fInitialized )
    {
        VCELLPOOLASSERT(0);
        hr = E_UNEXPECTED;
        goto abort;
    }

    pCell = (PCELLHEADER)( (char*)pClientCell - m_cbCellHeader );

#if DBG
    if( !_IsValidCell( pCell ) )
    {
        VCELLPOOLASSERT(0);
        hr = E_INVALIDARG;
        goto abort;
    }
#endif

    pPage = pCell->pPageHeader;

#if DBG
    if( !_IsValidPage( pPage )
    ||  !_CellIsOnPage( pCell, pPage )
#if VCELLPOOL_USE_SIGNATURE
    ||  ( SIG_USED_CELL != pCell->fccSignature )
#endif  // VCELLPOOL_USE_SIGNATURE
    )
    {
        VCELLPOOLASSERT(0);
        hr = E_FAIL;
        goto abort;
    }
#endif // DBG

#if DBG
    if( this != pPage->pOwnerPool )
    {
        VCELLPOOLASSERT(0);
        hr = E_FAIL;
        goto abort;
    }
#endif

#if DBG
    VCELLPOOLASSERT( m_nPageCount == ( m_cFreePages + m_cPartiallyFilledPages + m_cCompletelyFilledPages ) );
#endif

#if DBG && FILL_CELL_WITH_DEBUG_PATTERN
    FillMemory( pClientCell, m_cbCellSize, DELETE_PATTERN );
#endif // DBG && FILL_CELL_WITH_DEBUG_PATTERN

#if VCELLPOOL_USE_SIGNATURE
    pCell->fccSignature = SIG_FREE_CELL;
#endif


#if DBG && PUT_CELLS_AT_TAIL_OF_LIST
    // Put it at the tail of the free list.
    pCell->pFreeLink = NULL;
    if ( pPage->pEndFreeChain )
    {
        pPage->pEndFreeChain->pFreeLink = pCell;
    }
    pPage->pEndFreeChain = pCell;
    if ( NULL == pPage->pFreeChain )
    {
        pPage->pFreeChain = pCell;
    }
#else
    // Put it at the head of the free list.
    pCell->pFreeLink = pPage->pFreeChain;
    pPage->pFreeChain = pCell;
#endif


#if VCELLPOOL_USE_SIGNATURE
    VCELLPOOLASSERT( pPage->fccSignature != SIG_FREE_PAGE );
#endif

    if(  pPage->nCellsInUse < m_nCellsPerPage )
    {
#if VCELLPOOL_USE_SIGNATURE
        VCELLPOOLASSERT( pPage->fccSignature == SIG_PARTIALLY_FILLED_PAGE );
#endif
        pPage->nCellsInUse--;

        if( pPage->nCellsInUse == 0 )
        {
#if VCELLPOOL_USE_SIGNATURE
            pPage->fccSignature = SIG_FREE_PAGE;
#endif
            _RemoveFromOldList( m_pPartiallyFilledPages, pPage );
#if DBG
            m_cPartiallyFilledPages--;
#endif
            _AddToNewList( m_pFreePages, pPage );
#if DBG
            m_cFreePages++;
#endif
        }
    }
    else
    {
#if VCELLPOOL_USE_SIGNATURE
        VCELLPOOLASSERT( pPage->fccSignature == SIG_COMPLETELY_FILLED_PAGE );
#endif
        pPage->nCellsInUse--;
        _RemoveFromOldList( m_pCompletelyFilledPages, pPage );

#if DBG
        m_cCompletelyFilledPages--;
#endif

        if( pPage->nCellsInUse == 0 )
        {
#if VCELLPOOL_USE_SIGNATURE
            pPage->fccSignature = SIG_FREE_PAGE;
#endif
            _AddToNewList( m_pFreePages, pPage );
#if DBG
            m_cFreePages++;
#endif

        }
        else
        {
#if VCELLPOOL_USE_SIGNATURE
            pPage->fccSignature = SIG_PARTIALLY_FILLED_PAGE;
#endif
            _AddToNewList( m_pPartiallyFilledPages, pPage );
#if DBG
            m_cPartiallyFilledPages++;
#endif

        }
    }

    if( m_fFreeUnusedPages && ( 0 == pPage->nCellsInUse ) )
    {
        _FreeAnUnusedPage();
    }

#if DBG
    VCELLPOOLASSERT( m_nPageCount == ( m_cFreePages + m_cPartiallyFilledPages + m_cCompletelyFilledPages ) );
#endif

abort:
#if DBG
    m_fInUse = FALSE;
#endif // DBG

    return( hr );
}

//////////////////////////////////////////////////////////////////////////////
HRESULT CVCellPool::FindCellContaining( void *pvLocation, void **ppCell )
{
    HRESULT hr = S_OK;
    BOOL fFound = FALSE;
    DWORD nOffset;
    PPAGEHEADER pPage = NULL;

#if DBG
    VCELLPOOLASSERT( !m_fInUse );
    m_fInUse = TRUE;
#endif // DBG

    //
    // we look for only in Partially filled & completely filled pages
    //

    for( int i = 1; ( i < VCELLPOOL_NUM_PAGE_LIST ) && ( !fFound ) ; i++ )
    {
        pPage = m_rgPages[i];
        while( NULL != pPage )
        {
            if( 0 < pPage->nCellsInUse )
            {
                DWORD nCell = (DWORD)pvLocation;
                DWORD nPage = (DWORD)pPage;

                if( ( nCell >= ( nPage + m_cbPageHeader ) )
                &&  ( nCell < ( nPage + m_cbActualPageSize ) ) )
                {
                    fFound = TRUE;
                    break;
                }
            }
            pPage = pPage->pNextPage;
        }
    }

    if( !pPage )
    {
        VCELLPOOLASSERT(0);
        hr = E_INVALIDARG;
        goto abort;
    }

    nOffset = (DWORD)pvLocation - (DWORD)pPage;

    if( nOffset < m_cbPageHeader )
    {
        VCELLPOOLASSERT(0);
        hr = E_FAIL;
        goto abort;
    }

    nOffset -= m_cbPageHeader;
    nOffset -= nOffset % m_cbActualCellSize;
    nOffset += m_cbPageHeader + m_cbCellHeader;

    if( ppCell )
    {
        *ppCell = (void*)( (char*)pPage + nOffset );
    }

abort:
#if DBG
    m_fInUse = FALSE;
#endif // DBG

    return( hr );
}

//////////////////////////////////////////////////////////////////////////////
HRESULT CVCellPool::GetInfo( DWORD *pTotalPages, DWORD *pPagesOccupied, DWORD *pCellsOccupied )
{
#if DBG
    HRESULT hr = S_OK;
    DWORD nCellsInUse = 0;
    DWORD nPagesInUse = 0;
    PPAGEHEADER pPage;
    int i;

    VCELLPOOLASSERT( !m_fInUse );
    m_fInUse = TRUE;

    if( !m_fInitialized )
    {
        VCELLPOOLASSERT(0);
        hr = E_UNEXPECTED;
        goto abort;
    }

    //
    // Calculate cells and pages in use
    //

    for( i = 0; i < VCELLPOOL_NUM_PAGE_LIST; i++ )
    {
        pPage = m_rgPages[i];
        while( NULL != pPage )
        {
            if( 0 < pPage->nCellsInUse )
            {
                nPagesInUse++;
                nCellsInUse += pPage->nCellsInUse;
            }
            pPage = pPage->pNextPage;
        }
    }

    //
    // Return the requested values
    //
    if( pTotalPages )
    {
        *pTotalPages = m_nPageCount;
    }
    if( pPagesOccupied )
    {
        *pPagesOccupied = nPagesInUse;
    }
    if( pCellsOccupied )
    {
        *pCellsOccupied = nCellsInUse;
    }

abort:
    m_fInUse = FALSE;
    return( S_OK );
#else
    return( E_NOTIMPL );
    pCellsOccupied;
    pPagesOccupied;
    pTotalPages;
#endif
}


//////////////////////////////////////////////////////////////////////
// Private Methods

//////////////////////////////////////////////////////////////////////////////
CVCellPool::PPAGEHEADER CVCellPool::_AcquireCleanPage()
{
#ifdef  WIN32
    DWORD   dwTempSize  = m_cbActualPageSize / 4 + ((m_cbActualPageSize % 4) ? 1 : 0);
    PPAGEHEADER pNewPage = (PPAGEHEADER) ((DWORD *)NEW DWORD[dwTempSize]);
#else
    PPAGEHEADER pNewPage = (PPAGEHEADER) GlobalAllocPtr( GMEM_MOVEABLE, m_cbActualPageSize );
#endif  // WIN32

    if( NULL == pNewPage )
    {
        return( NULL );
    }

#if DBG && PATTERN_PAGES
    FillMemory( ( char * ) pNewPage, m_cbActualPageSize, ALLOC_BUFFER_PATTERN );
#endif

    m_cbTotalMemoryUsed += m_cbActualPageSize;


    PCELLHEADER pNewCell;
    PCELLHEADER pFreeLink = NULL;
#if DBG
    pNewPage->pEndFreeChain = NULL;
#endif
    for( DWORD ix = 0; ix < m_nCellsPerPage; ix++ )
    {
        pNewCell = (PCELLHEADER)( (char*)pNewPage + m_cbPageHeader + ix * m_cbActualCellSize );
#if VCELLPOOL_USE_SIGNATURE
        pNewCell->fccSignature = SIG_FREE_CELL;
#endif
        pNewCell->pPageHeader = pNewPage;
        pNewCell->pFreeLink = pFreeLink;
        pFreeLink = pNewCell;
#if DBG
        // If this is the end of the free chain, then put it at the tail of
        // the free list.
        if ( NULL == pNewPage->pEndFreeChain )
        {
            pNewPage->pEndFreeChain = pNewCell;
        }
#endif
    }
#if VCELLPOOL_USE_SIGNATURE
    pNewPage->fccSignature = SIG_FREE_PAGE;
#endif
    pNewPage->pNextPage = NULL;
    pNewPage->pPrevPage = NULL;
    pNewPage->nCellsInUse = 0;
    pNewPage->pFreeChain = pFreeLink;
#if DBG
    pNewPage->pOwnerPool = this;
#endif

    m_nPageCount++;

    return( pNewPage );
}

//////////////////////////////////////////////////////////////////////////////
void CVCellPool::_ReleasePage( PPAGEHEADER pPage )
{
#if DBG && PATTERN_PAGES
    if ( NULL != pPage )
    {
        FillMemory( ( char * ) pPage, m_cbActualPageSize, DELETE_BUFFER_PATTERN );
    }
#endif

#ifdef  WIN32
    delete [] (char*)pPage;
#else
    GlobalFreePtr( (char*)pPage );
#endif

    m_nPageCount--;
}

//////////////////////////////////////////////////////////////////////////////
void CVCellPool::_Cleanup()
{
    PPAGEHEADER pNextPage;
    BOOL fQuit = FALSE;

    for( int i = 0; ( i < VCELLPOOL_NUM_PAGE_LIST ) && ( !fQuit ) ; i++ )
    {
        PPAGEHEADER pPage = m_rgPages[i];
        while( NULL != pPage )
        {
            pNextPage = pPage->pNextPage;
#if DBG
            if( pPage->nCellsInUse == 0 )
            {
#if VCELLPOOL_USE_SIGNATURE
                VCELLPOOLASSERT( pPage->fccSignature == SIG_FREE_PAGE );
#endif
                m_cFreePages--;
            }
            else if( pPage->nCellsInUse < m_nCellsPerPage )
            {
#if VCELLPOOL_USE_SIGNATURE
                VCELLPOOLASSERT( pPage->fccSignature == SIG_PARTIALLY_FILLED_PAGE );
#endif
                m_cPartiallyFilledPages--;
            }
            else
            {
#if VCELLPOOL_USE_SIGNATURE
                VCELLPOOLASSERT( pPage->fccSignature == SIG_COMPLETELY_FILLED_PAGE );
#endif
                m_cCompletelyFilledPages--;
            }
#endif   //DBG

            _ReleasePage( pPage );

            pPage = pNextPage;
        }
    }

    ZeroMemory( this, sizeof(*this) );
}

//////////////////////////////////////////////////////////////////////////////
BOOL CVCellPool::_IsValidPage( PPAGEHEADER pPage )
{
    BOOL fValid = FALSE;

    for(;;)
    {
#if VCELLPOOL_USE_SIGNATURE
        if( ( SIG_PARTIALLY_FILLED_PAGE != *(DWORD*)pPage ) && ( SIG_COMPLETELY_FILLED_PAGE != *(DWORD*)pPage ) )
        {
            // this isn't a page header (or its corrupted)
            VCELLPOOLASSERT( !"How did this happen?" );
            break;
        }
#endif

#if DBG && CELLPOOL_PARANOIA
        PPAGEHEADER pTestPage;
        BOOL fFound = FALSE;

        for( int i = 0; ( i < VCELLPOOL_NUM_PAGE_LIST ) && ( !fFound ) ; i++ )
        {
            pTestPage = m_rgPages[i];

            while( pTestPage )
            {
                if( pTestPage == pPage )
                {
                    fFound = TRUE;
                    break;
                }
                pTestPage = pTestPage->pNextPage;
            }
        }
        if( pTestPage != pPage )
        {
            // this isn't one of our pages
            VCELLPOOLASSERT( !"this isn't one of our pages" );
            break;
        }
        // TODO: maybe traverse the page's free chain to look for corruption
#endif // CELLPOOL_PARANOIA

        fValid = TRUE;
        break;
    }

    return( fValid );
    pPage;
}

//////////////////////////////////////////////////////////////////////////////
BOOL CVCellPool::_IsValidCell( PCELLHEADER pCell )
{
    BOOL fValid = FALSE;

    for(;;)
    {
#if VCELLPOOL_USE_SIGNATURE
        if( ( SIG_FREE_CELL != *(DWORD*)pCell ) && ( SIG_USED_CELL != *(DWORD*)pCell ) )
        {
            // this isn't a cell header (or its corrupted)
            VCELLPOOLASSERT( !"this isn't a cell header (or its corrupted)" );
            break;
        }
#endif

        fValid = TRUE;
        break;
    }

    return( fValid );
    pCell;
}

//////////////////////////////////////////////////////////////////////////////
BOOL CVCellPool::_FreeAnUnusedPage()
{
    PPAGEHEADER pCandidatePage = m_pFreePages;
    DWORD cFreePages = 0;

    //
    // This while loop is not very expensive as we atmost loop it twice.
    // Also most of the server code does not free unused pages,
    // so this code does not get called often.
    //
    while( pCandidatePage )
    {
        cFreePages++;
        if( cFreePages > 1 )
        {
            break;
        }

        pCandidatePage = pCandidatePage->pNextPage;
    }

    pCandidatePage = m_pFreePages;

    if( cFreePages > 1 )
    {
        _RemoveFromOldList( m_pFreePages, pCandidatePage );
#if DBG
        m_cFreePages--;
#endif
        _ReleasePage( pCandidatePage );
        return( TRUE );
    }

    return( FALSE );


}

//////////////////////////////////////////////////////////////////////////////
BOOL CVCellPool::_CellIsOnPage( PCELLHEADER pCell, PPAGEHEADER pPage )
{
    DWORD nCell = (DWORD)pCell;
    DWORD nPage = (DWORD)pPage;

    if( ( nCell >= ( nPage + m_cbPageHeader ) ) &&
        ( nCell <= ( nPage + m_cbActualPageSize - m_cbActualCellSize ) ) )
    {
        return( TRUE );
    }
    VCELLPOOLASSERT(0);
    return( FALSE );
}

//////////////////////////////////////////////////////////////////////////////
void CVCellPool::_RemoveFromOldList( PPAGEHEADER & pOldList, PPAGEHEADER & pPage )
{
    if( pPage == pOldList )
    {
        pOldList = pPage->pNextPage;
    }

    if( pPage->pPrevPage )
    {
        VCELLPOOLASSERT( pPage != pOldList );
        pPage->pPrevPage->pNextPage = pPage->pNextPage;
    }

    if( pPage->pNextPage )
    {
        pPage->pNextPage->pPrevPage = pPage->pPrevPage;
    }
}

//////////////////////////////////////////////////////////////////////////////
void CVCellPool::_AddToNewList( PPAGEHEADER & pNewList, PPAGEHEADER & pPage )
{
    if( pNewList )
    {
        VCELLPOOLASSERT( NULL == pNewList->pPrevPage );
        pNewList->pPrevPage = pPage;
        pPage->pNextPage = pNewList;
        pPage->pPrevPage = NULL;
        pNewList = pPage;
    }
    else
    {
        pNewList = pPage;
        pPage->pPrevPage = NULL;
        pPage->pNextPage = NULL;
    }
}
