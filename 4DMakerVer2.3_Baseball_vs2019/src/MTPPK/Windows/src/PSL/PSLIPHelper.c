/*
 *  PSLIPHelper.c
 *
 *  Definition of TCP/IP helper routines.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLPrecomp.h"
#include "BSDSockets.h"
#include "PSLIPHelper.h"

#pragma warning(push)
#pragma warning(disable: 4214) /* bit field types other than int */

#include <iphlpapi.h>
#include <ipifcons.h>

#pragma warning(pop)

// Local Defines

// Local Types

// Local Functions

// Local Variables

/*
 *  PSLIsIPAddressAny
 *
 *  Given a pointer to a SOCKADDR checks to see if it is an unspecified
 *  address and reports PSLSUCCESS if it is and PSLSUCCESS_FALSE if it is not.
 *
 *  Arguments:
 *      PSL_CONST SOCKADDR*
 *                      psaAddr             Address to resove
 *      PSLUINT32       cbAddr              Length of the address
 *
 *  Returns:
 *      PSLSTATUS     PSLSUCCESS if "any" IP address; PSLSUCCESS_FALSE if not
 *
 */

PSLSTATUS PSLIsIPAddressAny(PSL_CONST SOCKADDR* psaAddr, PSLUINT32 cbAddr)
{
    PSLSTATUS         ps;

    // Make sure we got something to do

    if ((PSLNULL == psaAddr) || (0 == cbAddr))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // If the address is specified in any form then we will not attempt to
    //  resolve it further

    switch (psaAddr->sa_family)
    {
    case AF_INET:
        // Make sure that the address is the correct length

        if (sizeof(SOCKADDR_IN) > cbAddr)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        // If we have a fully qualified name do not attempt further resolution

        if (INADDR_ANY != ((SOCKADDR_IN*)psaAddr)->sin_addr.S_un.S_addr)
        {
            ps = PSLSUCCESS_FALSE;
            goto Exit;
        }
        break;

    case AF_INET6:
        // Make sure that the address is the correct length

        if (sizeof(SOCKADDR_IN6) > cbAddr)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        // If we have a fully qualified name do not attempt further resolution

        if (!IN6_IS_ADDR_UNSPECIFIED(&(((SOCKADDR_IN6*)psaAddr)->sin6_addr)))
        {
            ps = PSLSUCCESS_FALSE;
            goto Exit;
        }
        break;

    default:
        // Only supported for IP addresses

        PSLASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Address is an "any" address

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  PSLIsEqualIPAddress
 *
 *  Returns PSLSUCCESS if the two addresses are equal, PSLSUCCESS_FALSE if not
 *
 *  Arguments:
 *      PSL_CONST SOCKADDR*
 *                      psaAddr1            Address 1 to compare
 *      PSLUINT32       cbAddr1             Length of address 1
 *      PSL_CONST SOCKADDR*
 *                      psaAddr2            Address 2 to compare
 *      PSLUINT32       cbAddr2             Length of address 2
 *      PSLBOOL         fMatchingPorts      TRUE if ports should match as well
 *
 *  Returns:
 *      PSLSTATUS     PSLSUCCESS if same, PSLSUCCESS_FALSE if not
 *
 */

PSLSTATUS PSLIsEqualIPAddress(PSL_CONST SOCKADDR* psaAddr1, PSLUINT32 cbAddr1,
                            PSL_CONST SOCKADDR* psaAddr2, PSLUINT32 cbAddr2,
                            PSLBOOL fMatchingPorts)
{
    PSLBOOL                 fMatch;
    PSLSTATUS               ps;
    PSL_CONST SOCKADDR_IN*  psaIPv4Addr1;
    PSL_CONST SOCKADDR_IN*  psaIPv4Addr2;
    PSL_CONST SOCKADDR_IN6* psaIPv6Addr1;
    PSL_CONST SOCKADDR_IN6* psaIPv6Addr2;

    // Make sure we have two things to compare

    if ((PSLNULL == psaAddr1) || (sizeof(psaAddr1->sa_family) > cbAddr1) ||
        (PSLNULL == psaAddr2) || (sizeof(psaAddr2->sa_family) > cbAddr2))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // If the two addresses are not from the same address family the cannot
    //  be the same

    if (psaAddr1->sa_family != psaAddr2->sa_family)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    // Handle things based on address family

    switch (psaAddr1->sa_family)
    {
    case AF_INET:
        // Make sure that the addresses are at least the minimum length

        if ((sizeof(SOCKADDR_IN) > cbAddr1) ||
            (sizeof(SOCKADDR_IN) > cbAddr2))
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        // Save some redirection

        psaIPv4Addr1 = (SOCKADDR_IN*)psaAddr1;
        psaIPv4Addr2 = (SOCKADDR_IN*)psaAddr2;

        // Compare the actual address portion to see if we have a match

        fMatch = (psaIPv4Addr1->sin_addr.S_un.S_addr ==
                            psaIPv4Addr2->sin_addr.S_un.S_addr);

        // And check the port if requested

        if (fMatchingPorts)
        {
            fMatch &= (psaIPv4Addr1->sin_port == psaIPv4Addr2->sin_port);
        }
        break;

    case AF_INET6:
        // Make sure that the address is the correct length

        if ((sizeof(SOCKADDR_IN6) > cbAddr1) ||
            (sizeof(SOCKADDR_IN6) > cbAddr2))
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        // Save some redirection

        psaIPv6Addr1 = (SOCKADDR_IN6*)psaAddr1;
        psaIPv6Addr2 = (SOCKADDR_IN6*)psaAddr2;

        // Check that the port matches
        // And see if we have a matching address

        fMatch = IN6_ADDR_EQUAL(&(psaIPv6Addr1->sin6_addr),
                            &(psaIPv6Addr2->sin6_addr));

        // And check the port if requested

        if (fMatchingPorts)
        {
            fMatch &= (psaIPv6Addr1->sin6_port == psaIPv6Addr2->sin6_port);
        }
        break;

    default:
        // Only supported for IP addresses

        PSLASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Report match or not

    ps = fMatch ? PSLSUCCESS : PSLSUCCESS_FALSE;

Exit:
    return ps;
}


/*
 *  PSLResolveIPAddress
 *
 *  Given a pointer to a SOCKADDR checks to see if the specified address is
 *  not fully qualified (in other words is INET_ANY).  If it is then all
 *  IP bound adapters that meet the specified flags are bound.  If the address
 *  is fully qualified then no work is done.
 *
 *  Arguments:
 *      PSL_CONST SOCKADDR*
 *                      psaPort             Address to resolve
 *      PSLUINT32       cbPort              Length of the address
 *      PSL_CONST PSLUINT32*
 *                      rgdwIFList          Interface type to bind to
 *      PSLUINT32       cIFList             Number of interface types
 *      SOCKADDR_STORAGE**
 *                      ppsasResolved       Pointer to the location to
 *                                            return the resolved addresses
 *      PSLUINT32*      pcResolved          Number of resolved addresses
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if port is resolved, PSLSUCCESS_FALSE if
 *                        original port valid
 *
 */

PSLSTATUS PSLResolveIPAddress(PSL_CONST SOCKADDR* psaAddr, PSLUINT32 cbAddr,
                            PSL_CONST PSLUINT32* rgdwIFList, PSLUINT32 cIFList,
                            SOCKADDR_STORAGE** ppsasResolved,
                            PSLUINT32* pcResolved)
{
    PSLUINT32                   cbAddresses;
    PSLUINT32                   cbCur;
    PSLUINT32                   dwError;
    PSLSTATUS                   ps;
    PSLUINT32                   idxState;
    PSLUINT32                   idxAddress;
    PSLUINT32                   idxIF;
    PIP_ADAPTER_ADDRESSES       pAddresses = PSLNULL;
    PIP_ADAPTER_ADDRESSES       pAddressCur;
    PIP_ADAPTER_UNICAST_ADDRESS pUnicast;
    SOCKADDR*                   psaCur;
    SOCKADDR_STORAGE*           psasResult = PSLNULL;

    // Clear results for safety

    if (PSLNULL != ppsasResolved)
    {
        *ppsasResolved = PSLNULL;
    }

    if (PSLNULL != pcResolved)
    {
        *pcResolved = 0;
    }

    // Validate arguments

    if ((PSLNULL == psaAddr) || (sizeof(SOCKADDR_STORAGE) < cbAddr) ||
        (PSLNULL == ppsasResolved) || (PSLNULL == pcResolved))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Check to see if we have an "any" IP address- note that this will also
    //  validate the length of the address so it may fail

    ps = PSLIsIPAddressAny(psaAddr, cbAddr);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    // OK- now we have to do some real work.  We have an unspecified IPv4 or
    //  IPv6 address- get information for the available addresses

    cbAddresses = 0;
    dwError = GetAdaptersAddresses(psaAddr->sa_family,
                            (GAA_FLAG_SKIP_ANYCAST | GAA_FLAG_SKIP_MULTICAST |
                                GAA_FLAG_SKIP_DNS_SERVER),
                            PSLNULL, PSLNULL, (PULONG)&cbAddresses);
    switch (dwError)
    {
    case NO_ERROR:
    case ERROR_BUFFER_OVERFLOW:
        // All is well- keep going

        break;

    case ERROR_NO_DATA:
        // No addresses for this family exist- report success with no results

        PSLASSERT(PSLNULL == *ppsasResolved);
        PSLASSERT(0 == *pcResolved);
        ps = PSLSUCCESS;
        goto Exit;

    default:
        // Report the error

        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // Allocate space to hold the addresses

    pAddresses = (PIP_ADAPTER_ADDRESSES)PSLMemAlloc(LPTR, cbAddresses);
    if (PSLNULL == pAddresses)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    // And request them officially

    dwError = GetAdaptersAddresses(psaAddr->sa_family,
                            (GAA_FLAG_SKIP_ANYCAST | GAA_FLAG_SKIP_MULTICAST |
                                GAA_FLAG_SKIP_DNS_SERVER),
                            PSLNULL, pAddresses, (PULONG)&cbAddresses);
    if (NO_ERROR != dwError)
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // We walk the address list twice- once to determine the number of valid
    //  addresses we can generate and a second time to actually fill out the
    //  structure.

    for (idxState = 0, idxAddress = 0; idxState < 2; idxState++)
    {
        // Walk through each of the addresses

        for (pAddressCur = pAddresses, idxAddress = 0;
                PSLNULL != pAddressCur;
                pAddressCur = pAddressCur->Next)
        {
            // Walk through the list of interfaces that we want to support
            //  and see if we have a match

            for (idxIF = 0; idxIF < cIFList; idxIF++)
            {
                // See if we have a match

                if (rgdwIFList[idxIF] == pAddressCur->IfType)
                {
                    // Interface is on the supported list

                    break;
                }
            }

            // If we did not find a match then skip this address

            if (idxIF == cIFList)
            {
                continue;
            }

            // If the interface is not active skip this entry

            if (IfOperStatusUp != pAddressCur->OperStatus)
            {
                continue;
            }

            // If we do not have a unicast address skip this entry

            for (pUnicast = pAddressCur->FirstUnicastAddress;
                    PSLNULL != pUnicast;
                    pUnicast = pUnicast->Next)
            {
                // Save some redirection

                psaCur = pUnicast->Address.lpSockaddr;
                cbCur = pUnicast->Address.iSockaddrLength;

                // Make sure the address will fit in the destination

                PSLASSERT(cbCur <= sizeof(SOCKADDR_STORAGE));
                if (sizeof(SOCKADDR_STORAGE) < cbCur)
                {
                    continue;
                }

                // Make sure that we do not try to register on the loopback
                //  address

                switch (psaCur->sa_family)
                {
                case AF_INET:
                    // Make sure we do not have the loopback address

                    if (ntohl(INADDR_LOOPBACK) ==
                            ((SOCKADDR_IN*)psaCur)->sin_addr.S_un.S_addr)
                    {
                        continue;
                    }
                    break;

                case AF_INET6:
                    // Make sure we do not have the loopback address

                    if (IN6_IS_ADDR_LOOPBACK(&(((SOCKADDR_IN6*)psaCur)->
                            sin6_addr)))
                    {
                        continue;
                    }
                    break;

                default:
                    // Should not get here

                    PSLASSERT(FALSE);
                    ps = PSLERROR_UNEXPECTED;
                    goto Exit;
                }

                // Figure out what we are doing

                switch (idxState)
                {
                case 0:
                    // Just counting the number of addresses

                    idxAddress++;
                    break;

                case 1:
                    // Actually fill in the address information

                    CopyMemory(&(psasResult[idxAddress]), psaCur, cbCur);

                    // And stamp the source port from the resolve entry- for
                    //  both IPv4 and IPv6 this is the same size and the same
                    //  location

                    ((SOCKADDR_IN*)&(psasResult[idxAddress]))->sin_port =
                            ((SOCKADDR_IN*)psaAddr)->sin_port;

                    // Point to the next address

                    idxAddress++;
                    break;

                default:
                    // Should never get here

                    PSLASSERT(FALSE);
                    ps = PSLERROR_UNEXPECTED;
                    goto Exit;
                }
            }
        }

        // See if we need to allocate the result

        if (0 == idxState)
        {
            // See if we have addresses that match

            if (0 == idxAddress)
            {
                // No matching addresses so no more work to do

                PSLASSERT(PSLNULL == *ppsasResolved);
                PSLASSERT(0 == *pcResolved);
                ps = PSLSUCCESS;
                goto Exit;
            }

            // Allocate space to hold the result

            psasResult = (SOCKADDR_STORAGE*)PSLMemAlloc(LPTR,
                            (idxAddress * sizeof(SOCKADDR_STORAGE)));
            if (PSLNULL == psasResult)
            {
                ps = PSLERROR_OUT_OF_MEMORY;
                goto Exit;
            }
        }
    }

    // Resolution is complete- return the result

    PSLASSERT(0 < idxAddress);
    PSLASSERT(PSLNULL != psasResult);
    *ppsasResolved = psasResult;
    *pcResolved = idxAddress;
    psasResult = PSLNULL;

    // Report success

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMEMFREE(psasResult);
    SAFE_PSLMEMFREE(pAddresses);
    return ps;
}


/*
 *  PSLResolveMACAddress
 *
 *  Given a socket address on the local machine the MAC address is determined.
 *
 *  Arguments:
 *      PSL_CONST SOCKADDR*
 *                      psaAddr             Local socket address
 *      PSLUINT32       cbAddr              Length of the local address
 *      PSLBYTE**       ppbMAC              Return buffer for MAC address
 *      PSLUINT32*      pcbMAC              Return buffer for MAC address length
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLResolveMACAddress(PSL_CONST SOCKADDR* psaAddr, PSLUINT32 cbAddr,
                            PSLBYTE** ppbMAC, PSLUINT32* pcbMAC)
{
    PSLUINT32                   cbAddresses;
    PSLUINT32                   dwError;
    PSLBOOL                     fFound;
    PSLSTATUS                   ps;
    PIP_ADAPTER_ADDRESSES       pAddresses = PSLNULL;
    PIP_ADAPTER_ADDRESSES       pAddressCur;
    PSLBYTE*                    pbMAC = PSLNULL;
    PIP_ADAPTER_UNICAST_ADDRESS pUnicastCur;

    // Clear results for safety

    if (PSLNULL != ppbMAC)
    {
        *ppbMAC = PSLNULL;
    }
    if (PSLNULL != pcbMAC)
    {
        *pcbMAC = 0;
    }

    // Validate arguments

    if ((PSLNULL == psaAddr) || (sizeof(SOCKADDR_STORAGE) < cbAddr) ||
        (PSLNULL == ppbMAC) || (PSLNULL == pcbMAC))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Determine the number of adapters that are bound to this address
    //  family

    cbAddresses = 0;
    dwError = GetAdaptersAddresses(psaAddr->sa_family,
                            (GAA_FLAG_SKIP_ANYCAST | GAA_FLAG_SKIP_MULTICAST |
                                GAA_FLAG_SKIP_DNS_SERVER),
                            PSLNULL, PSLNULL, (PULONG)&cbAddresses);
    if ((NO_ERROR != dwError) && (ERROR_BUFFER_OVERFLOW != dwError))
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // Allocate space to hold the addresses

    pAddresses = (PIP_ADAPTER_ADDRESSES)PSLMemAlloc(LPTR, cbAddresses);
    if (PSLNULL == pAddresses)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    // And request them officially

    dwError = GetAdaptersAddresses(psaAddr->sa_family,
                            (GAA_FLAG_SKIP_ANYCAST | GAA_FLAG_SKIP_MULTICAST |
                                GAA_FLAG_SKIP_DNS_SERVER),
                            PSLNULL, pAddresses, (PULONG)&cbAddresses);
    if (NO_ERROR != dwError)
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // Walk through each of the adapters looking for the address we were
    //  provided

    for (pAddressCur = pAddresses, fFound = FALSE;
            PSLNULL != pAddressCur;
            pAddressCur = pAddressCur->Next)
    {
        // If no physical address is associated with this entry we cannot
        //  use it

        if (0 == pAddressCur->PhysicalAddressLength)
        {
            continue;
        }

        // Walk through the unicast addresses for this adapter to see if
        //  we have a match

        for (pUnicastCur = pAddressCur->FirstUnicastAddress;
                PSLNULL != pUnicastCur;
                pUnicastCur = pUnicastCur->Next)
        {
            // Since we asked for addresses in a specific family we should
            //  have got back an address that matches

            PSLASSERT(psaAddr->sa_family ==
                            pUnicastCur->Address.lpSockaddr->sa_family);

            // Compare the addresses- note that the address we were given
            //  may contain a port number so we cannot just use memcmp here

            ps = PSLIsEqualIPAddress(psaAddr, cbAddr,
                            pUnicastCur->Address.lpSockaddr,
                            pUnicastCur->Address.iSockaddrLength,
                            FALSE);
            if (FAILED(ps))
            {
                goto Exit;
            }

            // Stop looking if we found the match

            fFound = (PSLSUCCESS == ps);
            if (fFound)
            {
                break;
            }
        }

        // Stop looking if we found the match

        if (fFound)
        {
            break;
        }
    }

    // If we did not find a matching address report failure

    if (!fFound)
    {
        ps = PSLERROR_INVALID_ADDRESS;
        goto Exit;
    }

    // Make sure we got a MAC address

    PSLASSERT(0 < pAddressCur->PhysicalAddressLength);
    if (0 == pAddressCur->PhysicalAddressLength)
    {
        ps = PSLERROR_INVALID_ADDRESS;
        goto Exit;
    }

    // Allocate a buffer to hold it

    pbMAC = (PSLBYTE*)PSLMemAlloc(LPTR, pAddressCur->PhysicalAddressLength);
    if (PSLNULL == pbMAC)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    // And copy things over

    CopyMemory(pbMAC, pAddressCur->PhysicalAddress,
                            pAddressCur->PhysicalAddressLength);

    // All is well return the results

    *ppbMAC = pbMAC;
    *pcbMAC = pAddressCur->PhysicalAddressLength;
    pbMAC = PSLNULL;

    // Report success

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMEMFREE(pbMAC);
    SAFE_PSLMEMFREE(pAddresses);
    return ps;
}

