/*
 *  PSLMemory.cpp
 *
 *  Contains memory manager and leak detection code for PSL memory
 *  management.  It is expected that this file will be platform dependent.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLPrecomp.h"

#undef FORCE_MEM_FAILURES

//  Include a reference to the heap compaction Windows OS call so that
//  we can handle higher bit-rate video better

EXTERN_C void CompactAllHeaps();

// Local defines

#define PSLMEM_OOM_ATTEMPTS             2

#define DEFAULT_ALIGNMENT               8

#define PSLSHAREDHEAP_MIN_SIZE          0
#define PSLSHAREDHEAP_MAX_SIZE          (1536 * 1024)

// Unmap default allocator functions

#undef LocalAlloc
#undef LocalReAlloc
#undef LocalFree
#undef LocalSize

#define MAX_HEAPS                       256

#define PSLMEMFLAGS_CPP_ALLOCATION      0x00000001
#define PSLMEMFLAGS_CPP_ARRAYALLOC      0x00000002

#define PSLMEMFLAGS_REMOVE              0x80000000

#define PSL_MEMORY_SENTINAL1            'PSL1'
#define PSL_MEMORY_SENTINAL2            'psl2'

#define PSL_MEDIAHEAP_SIGNATURE         'pPSH'

// Local Types

typedef struct tagPSLMemoryHeader
{
    DWORD                                   dwSentinal1;
    DWORD                                   dwSentinal2;
    HANDLE                                  hHeap;
    PSLHMEM                                 hMem;
    DWORD                                   dwSize;
    DWORD                                   dwMemFlags;
    DWORD                                   dwAllocationID;
    UNALIGNED struct tagPSLMemoryHeader*    pHeaderNext;
    CHAR                                    rgchFile[MAX_PATH];
    INT                                     nLine;
} PSLMemoryHeader;

typedef struct
{
    DWORD                                   dwSentinal2;
    DWORD                                   dwSentinal1;
    UNALIGNED PSLMemoryHeader*              pHeader;
    UNALIGNED PSLMemoryHeader*              pHeaderNext;
} PSLMemoryFooter;

typedef struct tagPSLHeapInfo
{
    DWORD                                   dwSignature;
    HANDLE                                  hHeapBase;
    DWORD                                   cRefs;
#ifdef PSL_MEMORY_TRACK
    UNALIGNED PSLMemoryHeader*              pPSLMemHeaders;
#endif  // PSL_MEMORY_TRACK
    struct tagPSLHeapInfo*                  pHeapInfoNext;
} PSLHeapInfo;

// Local Functions

static BOOL _PSLMemForceFailure();

static VOID _PSLMemTraceError(LPCSTR szError, ...);

static CRITICAL_SECTION* _PSLMemCritSec();

static DWORD _PSLMemHandleOOM(DWORD idxAttempt, DWORD cbDesired);

static BOOL _ValidatePSLHeapInfo(HANDLE hHeap);

static PSLSTATUS _PSLHeapDestroy(HANDLE hHeap, BOOL fForceClosed);

static PSLUINT32 _PSLTranslatePSLMEMtoLMEMFlags(PSLUINT32 dwFlags);

#ifdef PSL_MEMORY_TRACK

static DWORD _PSLSizeAlloc(DWORD dwSize);

static UNALIGNED PSLMemoryHeader* _GetRootPSLMemHeaderForHeap(HANDLE hHeap);

static BOOL _SetRootPSLMemHeaderForHeap(HANDLE hHeap,
                            UNALIGNED PSLMemoryHeader* pRoot);

static PSLHMEM _PSLMemStoreAlloc(HANDLE hHeap, PSLHMEM hMem, DWORD dwSize,
                            DWORD dwMemFlags, LPCSTR szFile, INT nLine);

static UNALIGNED PSLMemoryHeader* _PSLMemFindAlloc(HANDLE hHeap, LPCVOID hMem,
                            DWORD dwMemFlags);

static VOID _PSLMemDumpLeaks(HANDLE hHeap, BOOL fFree);

static PSLHMEM _PSLMemTrackedAlloc(HANDLE hHeap, DWORD dwFlags, DWORD dwBytes,
                            DWORD dwMemFlags, LPCSTR szFile, INT nLine);

static PSLHMEM _PSLMemTrackedReAlloc(HANDLE hHeap, DWORD dwFlags, PSLHMEM hMem,
                            DWORD dwBytes, DWORD dwMemFlags,
                            LPCSTR szFile, INT nLine);

static PSLHMEM _PSLMemTrackedFree(HANDLE hHeap, DWORD dwFlags, PSLHMEM hMem,
                            DWORD dwMemFlags);

#endif  // PSL_MEMORY_TRACK

// Local Variables

static CRITICAL_SECTION             _CSPSLMem = { 0 };
static DWORD                        _DwPageSize = 0;
static DWORD                        _DwHeapRefCount = 0;
static BOOL                         _FHaltOnMemErrors = TRUE;
static BOOL                         _FTrackCPPArrays = TRUE;
static BOOL                         _FCSPSLMemInitialized = FALSE;
static HANDLE                       _HHeapShared = NULL;
static PFNPSLOOMHANDLER             _PFNPSLOOMHandler = NULL;
static PSLHeapInfo*                 _PPSLHeapSharedInfo = NULL;
static PSLHeapInfo*                 _PPSLHeapList = NULL;

static const CHAR   _RgchPSLPointerLeakDesc[] =
    "PSL Leak- Pointer: 0x%08x Size: %d Count: %d File: %s Line: %d\n";

static const CHAR   _RgchPSLHeapLeakDesc[] =
    "PSL Leak- Heap: 0x%08x Pointer: 0x%08x Size: %d Count %d File: %s Line: %d\n";

#ifdef PSL_MEMORY_TRACK
static DWORD                        _DwAllocationID = 0;
static DWORD                        _DwStopAtAllocationID = 0;

static UNALIGNED PSLMemoryHeader*   _PPSLMemHeadersLocal = NULL;
#endif  // PSL_MEMORY_TRACK

#ifdef FORCE_MEM_FAILURES
static DWORD                        _DwMemFailStartTick = 0;
static DWORD                        _DwMemFailTicksToFailure = 0;
#endif  // FORCE_MEM_FAILURES


/*
 *  _PSLMemForceFailure
 *
 *  Debugging support which returns TRUE if we should artificially fail the
 *  allocation.
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      BOOL        TRUE if allocation should fail
 *
 */

BOOL _PSLMemForceFailure()
{
#ifdef FORCE_MEM_FAILURES
    // See if it is time to force a failure- note that we will currently
    //  only fail if we have an OOM handler

    if ((NULL != _PFNPSLOOMHandler) &&
        (_DwMemFailTicksToFailure < (GetTickCount() - _DwMemFailStartTick)))
    {
        // Reset the counters

        _DwMemFailStartTick = GetTickCount();
        CeGenRandom(2, (LPBYTE)&_DwMemFailTicksToFailure);

        // Report time to fail

        return TRUE;
    }
#endif  // FORCE_MEM_FAILURES
    return FALSE;
}


/*
 *  _PSLMemTraceError
 *
 *  Handles reporting errors from the memory system.
 *
 *  Arguments:
 *      LPCSTR          szFormat            printf style format string
 *      ...                                 Optional arguments
 *
 *  Returns:
 *      VOID
 *
 */

VOID _PSLMemTraceError(LPCSTR szFormat, ...)
{
    DWORD           cchTemp;
    CHAR            rgchTemp[1024];
    WCHAR           rgwchTemp[1024];
    va_list         vargs;

    // Point to the args

    va_start(vargs, szFormat);

    // Safely format the output- StringCchPrintf guarantees termination

    StringCchVPrintfA(rgchTemp, PSLARRAYSIZE(rgchTemp), szFormat, vargs);

    // Convert to a WCHAR for OutputDebugString- remember that
    //  MultiByteToWideChar does not guarantee termination

    cchTemp = MultiByteToWideChar(CP_ACP, 0, rgchTemp, -1, rgwchTemp,
                            (PSLARRAYSIZE(rgwchTemp) - 1));
    rgwchTemp[cchTemp] = L'\0';

    // And emit

    OutputDebugString(rgwchTemp);

    // If the user has requested that we halt on errors do so

    if (_FHaltOnMemErrors)
    {
        DebugBreak();
    }
}


/*
 *  _PSLMemCritSec
 *
 *  Allocates a critical section to make sure that we are thread safe when
 *  manipulating the track list.
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      CRITICAL_SECTION*   Pointer to critical section
 *
 */

CRITICAL_SECTION* _PSLMemCritSec()
{
    // See if we need to initialize the critical section

    if (!_FCSPSLMemInitialized)
    {
        // Attempt to initialize the critical section

        if (!InitializeCriticalSectionAndSpinCount(&_CSPSLMem, 0))
        {
            // We failed to initialize the critical section- at this point
            //  we cannot trust any of the memory operations.  Warn the
            //  user and then terminate the process

            _FHaltOnMemErrors = TRUE;
            _PSLMemTraceError("PSL MEMORY- NO CRITSEC ** TERMINATING **");
            TerminateProcess(GetCurrentProcess(), ERROR_NOT_ENOUGH_MEMORY);
        }

        // Report that we are initialized

        _FCSPSLMemInitialized = TRUE;

#ifdef FORCE_MEM_FAILURES
        // Take advantage of this initialization point to initialize our
        //  memory failure numbers

        _DwMemFailStartTick = GetTickCount();
        CeGenRandom(2, (LPBYTE)&_DwMemFailTicksToFailure);
#endif  // FORCE_MEM_FAILURES
    }
    return &_CSPSLMem;
}


/*
 *  _PSLMemHandleOOM
 *
 *  Helper function to make it easier to deal with the OOM handler.
 *
 *  NOTE: This function assumes that the caller is currently holding the
 *  _PSLMemCritSec and will release and re-acquire it if a callback occurs
 *
 *  Arguments:
 *      DWORD           idxAttempt          Current count of attempts to
 *                                            perform this allocation
 *      DWORD           cbDesired           Amount of memory desired
 *
 *  Returns:
 *      DWORD       PSLOOM value indicating how to respond
 *
 */

DWORD _PSLMemHandleOOM(DWORD idxAttempt, DWORD cbDesired)
{
    BOOL            fHaltOnErrors;
    DWORD           dwResult;

    // If we do not have a registered handler then we are done

    if (NULL == _PFNPSLOOMHandler)
    {
        dwResult = PSLOOM_FAIL;
        goto Exit;
    }

    // Do not halt on these errors

    fHaltOnErrors = _FHaltOnMemErrors;
    _FHaltOnMemErrors = FALSE;

    // Provide some debug feedback

    _PSLMemTraceError("**** PSLMem Handling Out of Memory Condition ****");
    _PSLMemTraceError("Bytes Desired = %u  In Proc Free = %u  Global Free %u",
                            cbDesired, PSLGetAvailableMemory(),
                            PSLGetFreeMemory());

    // Restore the original halt on errors state

    _FHaltOnMemErrors = fHaltOnErrors;

    // This could take a while- release the critical section

    LeaveCriticalSection(_PSLMemCritSec());

    // Call the handler

    dwResult = _PFNPSLOOMHandler(idxAttempt, cbDesired);

    // Look for requests that we need to handle here

    switch (dwResult)
    {
    case PSLOOM_COMPACT_FAIL:
    case PSLOOM_COMPACT_RETRY:
        // Attempt to compact all the heaps

        PSLHeapCompactAll();

        // And figure out what the basic operation is

        dwResult = (PSLOOM_COMPACT_RETRY == dwResult) ?
                            PSLOOM_RETRY : PSLOOM_FAIL;
        break;

    default:
        // No work to do

        break;
    }

    // And re-acquire the critical section

    EnterCriticalSection(_PSLMemCritSec());

Exit:
    return dwResult;
}


/*
 *  PSLMemShutdown
 *
 *  Performs shutdown tasks on the memory manager.
 *
 *  Arguments:
 *      NONE
 *
 *  Returns:
 *      VOID    Nothing
 *
 */

PSLVOID PSLMemShutdown()
{
    // Walk through and make sure that the media heaps close down correctly

    while (NULL != _PPSLHeapList)
    {
        // Destroy the heap- note that this will update the _PPSLHeapList
        //  under the surface so while this looks like it is doing nothing
        //  it really is iterating down the known heaps.

        _PSLHeapDestroy((HANDLE)_PPSLHeapList, TRUE);
    }

#ifdef PSL_MEMORY_TRACK
    // Report leaks if necessary

    _PSLMemDumpLeaks(NULL, TRUE);
#endif  // PSL_MEMORY_TRACK

    // Destroy the critical section

    DeleteCriticalSection(&_CSPSLMem);
    ZeroMemory(&_CSPSLMem, sizeof(_CSPSLMem));
    _FCSPSLMemInitialized = FALSE;
    return;
}


/*
 *  PSLMemRegisterOOMHandler
 *
 *  Registers a callback function to be called whenever an allocation fails
 *  to allocate memory.
 *
 *  Arguments:
 *      PFNPSLOOMHANDLER
 *                      pfnOOMHandler           Callback function
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success, PSLSUCCESS_EXISTS if already
 *                    registered
 *
 */

PSLSTATUS PSLMemRegisterOOMHandler(PFNPSLOOMHANDLER pfnHandler)
{
    PSLSTATUS       ps;

    // Avoid threading issues

    EnterCriticalSection(_PSLMemCritSec());

    // We only support one function at a time- see if we already have a handler

    ASSERT(NULL == _PFNPSLOOMHandler);
    if (NULL != _PFNPSLOOMHandler)
    {
        // Report S_FALSE if it is the same handler

        ps = (pfnHandler == _PFNPSLOOMHandler) ?
                            PSLSUCCESS_EXISTS : PSLERROR_INVALID_PARAMETER;
        ASSERT(PSL_SUCCEEDED(ps));
        goto Exit;
    }

    // Stash the new handler

    _PFNPSLOOMHandler = pfnHandler;

    // Report success

    ps = PSLSUCCESS;

Exit:
    LeaveCriticalSection(_PSLMemCritSec());
    return ps;
}


/*
 *  PSLMemUnregisterOOMHandler
 *
 *  Unregisters the Out of Memory callback function
 *
 *  Arguments:
 *      PFNPSLOOMHANDLER
 *                      pfnPSLOOMHandler        Callback function
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if not
 *                    registered
 *
 */

PSLSTATUS PSLMemUnregisterOOMHandler(PFNPSLOOMHANDLER pfnHandler)
{
    PSLSTATUS       ps;

    // Avoid threading issues

    EnterCriticalSection(_PSLMemCritSec());

    // We only support one function at a time- and we expect it to already
    //  be registered

    ASSERT(NULL != _PFNPSLOOMHandler);
    ASSERT(pfnHandler == _PFNPSLOOMHandler);

    // If the handlers do not match then do not clear it

    if (pfnHandler != _PFNPSLOOMHandler)
    {
        // Report S_FALSE if no handler is registered because this handler
        //  was not found

        ps = (NULL == _PFNPSLOOMHandler) ?
                            PSLSUCCESS_NOT_FOUND : PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Clear the handler

    _PFNPSLOOMHandler = NULL;

    // Report success

    ps = PSLSUCCESS;

Exit:
    LeaveCriticalSection(_PSLMemCritSec());
    return ps;
}


/*
 *  PSLMemAlign
 *
 *  Memory alignment helper function
 *
 *  Arguments:
 *      PSLBYTE*        pb                  Memory to align
 *      PSLUINT32       dwBoundary          Boundary to align to
 *
 *  Returns:
 *      PSLBYTE*        Aligned location
 *
 */

PSLBYTE* PSLMemAlign(PSLBYTE* pb, PSLUINT32 dwBoundary)
{
    PSLUINT32           dwAlign;

    if (0 < dwBoundary)
    {
#ifdef DEBUG
        // Alignment is only supported on power of 2 boundaries

        for (dwAlign = dwBoundary; !(dwAlign & 0x01); dwAlign >>= 1);
        ASSERT(1 == dwAlign);
#endif  // DEBUG

        // Perform the alignment

        dwAlign = dwBoundary - ((ULONG)pb & (dwBoundary - 1));
        dwAlign = (dwBoundary == dwAlign) ? 0 : dwAlign;
    }
    else
    {
        dwAlign = 0;
    }
    return pb + dwAlign;
}

#ifdef PSL_MEMORY_TRACK

/*
 *  PSLHaltOnMemErrors
 *
 *  Sets the flag to halt or not halt on memory errors.
 *
 *  Arguments:
 *      PSLBOOL         fHalt               TRUE if debugger should be called
 *                                            on memory traces
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLHaltOnMemErrors(PSLBOOL fHalt)
{
    EnterCriticalSection(_PSLMemCritSec());
    _FHaltOnMemErrors = fHalt;
    LeaveCriticalSection(_PSLMemCritSec());
    return;
}


/*
 *  _GetRootPSLMemHeaderForHeap
 *
 *  Returns the root of the memory header list for the specified heap.
 *  Passing NULL returns the default process heap.
 *
 *  Arguments:
 *      HANDLE          hHeap               Heap to return memory header for-
 *                                            NULL for the process heap
 *
 *  Returns:
 *      UNALIGNED PSLMemoryHeader*      Root header
 *
 */

UNALIGNED PSLMemoryHeader* _GetRootPSLMemHeaderForHeap(HANDLE hHeap)
{
    UNALIGNED PSLMemoryHeader*  pRootHeader;

    // Check to see if this is for the local process heap

    if (NULL == hHeap)
    {
        // User wants the process heap- return the head of the list

        pRootHeader = _PPSLMemHeadersLocal;
        goto Exit;
    }

    // We think this is a heap- validate it

    if (!_ValidatePSLHeapInfo(hHeap))
    {
        _PSLMemTraceError("_GetRootPSLMemHeaderForHeap: Invalid heap 0x%08x",
                            hHeap);
        pRootHeader = NULL;
        goto Exit;
    }

    // And extract the header information

    pRootHeader = ((PSLHeapInfo*)hHeap)->pPSLMemHeaders;

Exit:
    return pRootHeader;
}


/*
 *  _SetRootPSLMemHeaderForHeap
 *
 *  Sets the pointer to the root memory tracking header for a particular heap.
 *  Pass in NULL to reference the process heap.
 *
 *  Arguments:
 *      HANDLE          hHeap               Heap to track allocation in-
 *                                            NULL for the process heap
 *      UNALIGNED PSLMemoryHeader*
 *                      pRoot               New root pointer
 *
 *  Returns:
 *      BOOL        TRUE on success
 *
 */

BOOL _SetRootPSLMemHeaderForHeap(HANDLE hHeap,
                            UNALIGNED PSLMemoryHeader* pRoot)
{
    BOOL            fResult;

    // Handle the process heap first

    if (NULL == hHeap)
    {
        // Set the process memory list to the new root pointer- note that
        //  this function assumes the caller is managing allocation and freeing
        //  of the items in the list

        _PPSLMemHeadersLocal = pRoot;
        fResult = TRUE;
        goto Exit;
    }

    // We think this is a heap- validate it

    if (!_ValidatePSLHeapInfo(hHeap))
    {
        _PSLMemTraceError("_SetRootPSLMemHeaderForHeap: Invalid heap 0x%08x",
                            hHeap);
        fResult = FALSE;
        goto Exit;
    }

    // And update the header pointer

    ((PSLHeapInfo*)hHeap)->pPSLMemHeaders = pRoot;

    // Report success

    fResult = FALSE;

Exit:
    return fResult;
}


/*
 *  _PSLSizeAlloc
 *
 *  Inline function used to modify the size of the allocation to deal with
 *  the header and footer additions.
 *
 *  Arguments:
 *      DWORD           dwSize          User's desired size for allocation
 *
 *  Returns:
 *      DWORD       Size of allocation needed to handle memory tracking
 *
 */

DWORD _PSLSizeAlloc(DWORD dwSize)
{
    // For now we will make sure that we always guarantee that the allocated
    //  memory starts on a DWORD boundary so we should size the result
    //  accordingly and that the footer does as well- so size the whole
    //  structure to account for this extra DEFAULT_ALIGNMENT-bytes of alignment

    return PSLAllocAlign(
                (dwSize + sizeof(PSLMemoryHeader) + sizeof(PSLMemoryFooter)),
                DEFAULT_ALIGNMENT);
}


/*
 *  _PSLMemStoreAlloc
 *
 *  Adds an allocation to the stack of allocations for tracking.
 *
 *  Arguments:
 *      HANDLE          hHeap               Heap this is allocated from (NULL
 *                                            for local allocations
 *      PSLHMEM         hMem                Pointer to the memory being
 *                                            allocated
 *      DWORD           dwSize              Size of the allocation
 *      DWORD           dwMemFlags          Flags
 *      LPCSTR          szFile              Location of the allocation
 *      INT             nLine               Line number fo allocation
 *
 *  Returns:
 *      PSLHMEM     Modified hMem to return to the user
 *
 */

PSLHMEM _PSLMemStoreAlloc(HANDLE hHeap, PSLHMEM hMem, DWORD dwSize,
                            DWORD dwMemFlags, LPCSTR szFile, INT nLine)
{
    UNALIGNED PSLMemoryFooter*  pFooter;
    UNALIGNED PSLMemoryHeader*  pHeader;
    PSLHMEM                     pResult;
    UNALIGNED PSLMemoryHeader*  pRoot;

    // Make sure that we do not have thread synchronization issues

    EnterCriticalSection(_PSLMemCritSec());

    // Set the header and footer pointers to avoid redirect

    pHeader = (PSLMemoryHeader*)hMem;
    pResult = (PSLHMEM)PSLMemAlign((LPBYTE)hMem + sizeof(PSLMemoryHeader),
                            sizeof(DWORD));
    pFooter = (PSLMemoryFooter*)((LPBYTE)pResult + dwSize);

    // Fill the information in for the header

    pHeader->dwSentinal1 = PSL_MEMORY_SENTINAL1;
    pHeader->dwSentinal2 = PSL_MEMORY_SENTINAL2;
    pHeader->hHeap = hHeap;
    pHeader->hMem = pResult;
    pHeader->dwSize = dwSize;
    pHeader->dwMemFlags = dwMemFlags;

    // Add the allocation ID

    _DwAllocationID++;
    pHeader->dwAllocationID = _DwAllocationID;
    if (_DwStopAtAllocationID == _DwAllocationID)
    {
        DebugBreak();
    }

    // Copy over the allocation source information

    ASSERT(strlen(szFile) < PSLARRAYSIZE(pHeader->rgchFile));
    strncpy(pHeader->rgchFile, szFile, PSLARRAYSIZE(pHeader->rgchFile));
    pHeader->nLine = nLine;

    // And fill in the footer information

    pFooter->dwSentinal2 = PSL_MEMORY_SENTINAL2;
    pFooter->dwSentinal1 = PSL_MEMORY_SENTINAL1;
    pFooter->pHeader = pHeader;

    // And link the information into the allocation chain

    pRoot = _GetRootPSLMemHeaderForHeap(hHeap);
    pHeader->pHeaderNext = pRoot;
    pFooter->pHeaderNext = pRoot;
    _SetRootPSLMemHeaderForHeap(hHeap, pHeader);

    // Done with the critical section

    LeaveCriticalSection(_PSLMemCritSec());

    // And return the resulting user allocation

    return pResult;
}


/*
 *  _PSLMemFindAlloc
 *
 *  Removes an allocation from the tracked list
 *
 *  Arguments:
 *      HANDLE          hHeap               Heap this allocation was supposed
 *                                            to be in (NULL for local
 *                                            allocation)
 *      LPCVOID         hMem               Pointer to the memory
 *      DWORD           dwMemFlags         Flags
 *
 *  Returns:
 *      PSLMemoryHeader*    Pointer to the memory header
 *
 */

UNALIGNED PSLMemoryHeader* _PSLMemFindAlloc(HANDLE hHeap, LPCVOID hMem,
                            DWORD dwMemFlags)
{
    UNALIGNED PSLMemoryFooter*  pFooter;
    UNALIGNED PSLMemoryHeader*  pHeader;
    UNALIGNED PSLMemoryHeader*  pHeaderCur;
    UNALIGNED PSLMemoryHeader*  pHeaderPrev;
    UNALIGNED PSLMemoryHeader*  pResult;

    // Make sure that we do not have thread synchronization issues

    EnterCriticalSection(_PSLMemCritSec());

    // From the user's pointer find the header

    pHeader = (PSLMemoryHeader*)((LPBYTE)hMem - sizeof(PSLMemoryHeader));
    if (0 != ((DWORD)pHeader & 0x03))
    {
        pHeader = (PSLMemoryHeader*)((LPBYTE)pHeader - ((DWORD)pHeader & 0x03));
    }

    // Validate the header

    if ((PSL_MEMORY_SENTINAL1 != pHeader->dwSentinal1) ||
        (PSL_MEMORY_SENTINAL2 != pHeader->dwSentinal2))
    {
        _PSLMemTraceError("PSLMemFindAlloc: 0x%08x fails header sentinal check",
                            hMem);
        pResult = NULL;
        goto Exit;
    }

    // Figure out where the footer is now that we have a size

    pFooter = (PSLMemoryFooter*)((LPBYTE)hMem + pHeader->dwSize);

    // And check it's sentinals

    if (PSL_MEMORY_SENTINAL2 != pFooter->dwSentinal2)
    {
        _PSLMemTraceError("PSLMemFindAlloc: 0x%08x footer sentinal two fails",
                            hMem);
    }
    if (PSL_MEMORY_SENTINAL1 != pFooter->dwSentinal1)
    {
        _PSLMemTraceError("PSLMemFindAlloc: 0x%08x footer sentinal one fails",
                            hMem);
    }

    // Now check to make sure the heaps match

    if (hHeap != pHeader->hHeap)
    {
        _PSLMemTraceError("PSLMemFindAlloc: Invalid heap specified for 0x%08x",
                            hMem);
        pResult = NULL;
        goto Exit;
    }

    // And make sure we have no pointer mismatch

    if (hMem != pHeader->hMem)
    {
        _PSLMemTraceError("PSLMemFindAlloc: Pointer mismatch for 0x%08x",
                            hMem);
        pResult = NULL;
        goto Exit;
    }

    // We are happy that this allocation is clean- go ahead and pull it off
    //  the stack

    pHeaderCur = _GetRootPSLMemHeaderForHeap(hHeap);
    for (pHeaderPrev = NULL;
            pHeaderCur != NULL;
            pHeaderPrev = pHeaderCur, pHeaderCur = pHeaderCur->pHeaderNext)
    {
        // Check for issues as we walk the list

        if ((PSL_MEMORY_SENTINAL1 != pHeaderCur->dwSentinal1) ||
            (PSL_MEMORY_SENTINAL2 != pHeaderCur->dwSentinal2))
        {
            _PSLMemTraceError(
                    "PSLMemFindAlloc: 0x%08x fails header sentinal check",
                    pHeaderCur->hMem);
        }

        // Figure out where the footer is now that we have a size

        pFooter = (PSLMemoryFooter*)
                            ((LPBYTE)pHeaderCur->hMem + pHeaderCur->dwSize);

        // And check it's sentinals

        if (PSL_MEMORY_SENTINAL2 != pFooter->dwSentinal2)
        {
            _PSLMemTraceError(
                    "PSLMemFindAlloc: 0x%08x footer sentinal two fails",
                    pHeaderCur->hMem);
        }
        if (PSL_MEMORY_SENTINAL1 != pFooter->dwSentinal1)
        {
            _PSLMemTraceError(
                    "PSLMemFindAlloc: 0x%08x footer sentinal one fails",
                    pHeaderCur->hMem);
        }

        // See if we have found the allocation

        if (hMem == pHeaderCur->hMem)
        {
            // Found it

            break;
        }
    }

    // If we get out with a NULL iterator than we did not find the allocation

    if (NULL == pHeaderCur)
    {
        _PSLMemTraceError("PSLMemFindAlloc: 0x%08x not in allocation list",
                            hMem);
        pResult = NULL;
        goto Exit;
    }

    // See if we are supposed to remove the allocation from the list if found

    if (PSLMEMFLAGS_REMOVE & dwMemFlags)
    {
        // We found the allocation- pull it out of the list

        if (NULL == pHeaderPrev)
        {
            // Pulling off the front of the list

            _SetRootPSLMemHeaderForHeap(hHeap, pHeader->pHeaderNext);
        }
        else
        {
            // Pulling out of the middle of the list

            pHeaderPrev->pHeaderNext = pHeader->pHeaderNext;
        }
    }

    // Return the header

    pResult = pHeader;

Exit:
    LeaveCriticalSection(_PSLMemCritSec());
    return pResult;
}


/*
 *  _PSLMemDumpLeaks
 *
 *  Walks the list of allocations and reports anything there.
 *
 *  Arguments:
 *      HANDLE          hHeap               Heap to dump leaks for
 *      BOOL            fFree               TRUE if memory should be freed
 *                                            after reporting
 *
 *  Returns:
 *      None
 *
 */

VOID _PSLMemDumpLeaks(HANDLE hHeap, BOOL fFree)
{
    DWORD                       cLeaks;
    BOOL                        fHaltOnErrors;
    UNALIGNED PSLMemoryHeader*  pHeader;
    UNALIGNED PSLMemoryHeader*  pHeaderNext;

    // Make sure that we do not have synchronization issues

    EnterCriticalSection(_PSLMemCritSec());

    // Figure out if we are halting on errors and turn it off during
    //  leak detection

    fHaltOnErrors = _FHaltOnMemErrors;
    _FHaltOnMemErrors = FALSE;

    // Walk the list

    for (pHeader = _GetRootPSLMemHeaderForHeap(hHeap), cLeaks = 0;
            NULL != pHeader;
            pHeader = pHeaderNext, cLeaks++)
    {
        // Dump information about the leak

        if (NULL == pHeader->hHeap)
        {
            // Report a local allocation leak

            _PSLMemTraceError(_RgchPSLPointerLeakDesc,
                pHeader->hMem, pHeader->dwSize, pHeader->dwAllocationID,
                pHeader->rgchFile, pHeader->nLine);
        }
        else
        {
            // Report a heap allocation leak

            _PSLMemTraceError(_RgchPSLHeapLeakDesc,
                pHeader->hHeap, pHeader->hMem, pHeader->dwSize,
                pHeader->dwAllocationID, pHeader->rgchFile, pHeader->nLine);
        }

        // Figure out what the next header to look at is

        pHeaderNext = pHeader->pHeaderNext;

        // If we have been asked to free it do so

        if (fFree)
        {
            // Remove this entry from the allocation list

            ASSERT(pHeader == _GetRootPSLMemHeaderForHeap(hHeap));
            _SetRootPSLMemHeaderForHeap(hHeap, pHeaderNext);

            // And free the entry

            if (NULL == pHeader->hHeap)
            {
                // Free the entry

                LocalFree(pHeader);
            }
            else
            {
                // Free the entry

                HeapFree(pHeader->hHeap, 0, pHeader);
            }
        }
    }

    // Restore the halt on errors

    _FHaltOnMemErrors = fHaltOnErrors;
    if (_FHaltOnMemErrors && (0 != cLeaks))
    {
        _PSLMemTraceError("\t***** Detected %d PSL Memory Leaks *****", cLeaks);
    }

    // Leave the safety

    LeaveCriticalSection(_PSLMemCritSec());
    return;
}

/*
 *  _PSLMemTrackedAlloc
 *
 *  Debug local memory allocation
 *
 *  Arguments:
 *      HEAP            hHeap           Heap to perform allocation in (NULL for
 *                                        local heap)
 *      DWORD           dwFlags         Flags
 *      DWORD           dwBytes         Bytes requested for allocation
 *      DWORD           dwMemFlags      Internal allocation flags
 *      LPCSTR          szFile          File where allocation was made
 *      INT             nLine           Line where allocation was made
 *
 *  Returns:
 *      PSLHMEM     Handle or pointer to memory
 *
 */

PSLHMEM _PSLMemTrackedAlloc(HANDLE hHeap, DWORD dwFlags, DWORD dwBytes,
                            DWORD dwMemFlags, LPCSTR szFile, INT nLine)
{
    DWORD           idxAttempt;
    DWORD           dwSize;
    PSLHMEM         hMem;

    // Make sure that we do not have thread synchronization issues

    EnterCriticalSection(_PSLMemCritSec());

    // Parameter validation- currently we do not support allocating moveable
    //  memory through this interface

    ASSERT(!(PSLMEM_RELOCATABLE & dwFlags));
    if (PSLMEM_RELOCATABLE & dwFlags)
    {
        hMem = NULL;
        goto Exit;
    }

    // First figure out how large the memory needs to be

    dwSize = _PSLSizeAlloc(dwBytes);

    // Loop so that we can handle out of memory conditions

    for (hMem = NULL, idxAttempt = 0;
            (NULL == hMem) && (idxAttempt < PSLMEM_OOM_ATTEMPTS);
            idxAttempt++)
    {
        // Now do the allocation for the full memory block if permitted

        if (!_PSLMemForceFailure())
        {
            if (NULL == hHeap)
            {
                hMem = LocalAlloc(
                            _PSLTranslatePSLMEMtoLMEMFlags(dwFlags),
                            dwSize);
            }
            else
            {
                ASSERT(_ValidatePSLHeapInfo(hHeap));
                hMem = HeapAlloc(((PSLHeapInfo*)hHeap)->hHeapBase, dwFlags,
                                dwSize);
            }
        }

        // Handle the OOM condition if necessary

        if (NULL == hMem)
        {
            // Only want to keep trying it retry is allowed

            if (PSLOOM_RETRY != _PSLMemHandleOOM(idxAttempt, dwSize))
            {
                break;
            }
        }
    }

    // Fail if allocation did not succeed

    if (NULL == hMem)
    {
        // Memory failure- no more work to do

        goto Exit;
    }

    // Memory allocated- initialize the memory tracking and add it to our
    //  allocation list

    hMem = _PSLMemStoreAlloc(hHeap, hMem, dwBytes, dwMemFlags, szFile, nLine);

Exit:
    LeaveCriticalSection(_PSLMemCritSec());
    return hMem;
}


/*
 *  _PSLMemTrackedReAlloc
 *
 *  Debug local memory reallocation
 *
 *  Arguments:
 *      HANDLE          hHeap           Handle to heap (NULL if we should
 *                                        use the local heap)
 *      DWORD           dwFlags         Allocation flags
 *      PSLHMEM         hMem            Handle to memory to reallocate
 *      UINT            uBytes          Bytes requested for allocation
 *      DWORD           dwMemFlags      Internal flags
 *      LPCSTR          szFile          File where allocation was made
 *      INT             nLine           Line where allocation was made
 *
 *  Returns:
 *      PSLHMEM     Handle or pointer to memory
 *
 */

PSLHMEM _PSLMemTrackedReAlloc(HANDLE hHeap, DWORD dwFlags, PSLHMEM hMem,
                            DWORD dwBytes, DWORD dwMemFlags,
                            LPCSTR szFile, INT nLine)
{
    PSLHMEM                     hMemResult;
    UNALIGNED PSLMemoryHeader*  pHeader;

    // Make sure we do not have thread synchronization issues

    EnterCriticalSection(_PSLMemCritSec());

    // Handle the case that we have been handed a NULL initial allocation

    if (NULL == hMem)
    {
        hMemResult = _PSLMemTrackedAlloc(hHeap, (dwFlags & ~PSLMEM_RELOCATABLE),
                            dwBytes, dwMemFlags, szFile, nLine);
        goto Exit;
    }

    // Retrieve the header for the current allocation and remove it from the
    //  list

    pHeader = _PSLMemFindAlloc(hHeap, hMem, PSLMEMFLAGS_REMOVE);
    if (NULL == pHeader)
    {
        hMemResult = NULL;
        goto Exit;
    }

    // Check the allocation to make sure we are not doing something that
    //  we should not do like try and realloc a 'new' allocation

    if (PSLMEMFLAGS_CPP_ALLOCATION & pHeader->dwMemFlags)
    {
        // Allocation performed with C++ allocator but freed with stand
        //  alone allocator

        _PSLMemTraceError("PSLMemReAlloc: C/C++ Allocator mismatch for 0x%08x",
                            hMem);
    }

    // Also we currently do not support using ReAlloc to transfer between
    //  heaps

    ASSERT(hHeap == pHeader->hHeap);
    if (hHeap != pHeader->hHeap)
    {
        // Report failure

        _PSLMemTraceError("PSLMemRealloc: Cannot do cross-heap reallocations");
        hMemResult = NULL;
        goto Exit;
    }

    // Check to see if we can just update the size

    if (dwBytes < pHeader->dwSize)
    {
        // We can re-size the existing allocation- note that for cheapness at
        //  this point we do not actually call the LocalReAlloc implementation
        //  to resize the buffer- we just track it locally

        hMemResult = _PSLMemStoreAlloc(hHeap, hMem, dwBytes, dwMemFlags,
                            szFile, nLine);
        goto Exit;

    }

    // Need to allocate a new blob of memory to hold the contents- make sure
    //  we are allowed to move the memory

    if (!(PSLMEM_RELOCATABLE & dwFlags))
    {
        // Report failure

        _PSLMemTraceError("PSLMemReAlloc: must move allocation to succeed");
        hMemResult = NULL;
        goto Exit;
    }

    // Allocate a new blob of memory

    hMemResult = _PSLMemTrackedAlloc(hHeap, (dwFlags & ~PSLMEM_RELOCATABLE), dwBytes,
                            dwMemFlags, szFile, nLine);
    if (NULL == hMemResult)
    {
        goto Exit;
    }

    // And now we want to copy the original contents from the old location
    //  to the new location

    CopyMemory(hMemResult, hMem, pHeader->dwSize);

    // And free the original location- remember we have already pulled it from
    //  the list so we should just call LocalFree here

    if (NULL == hHeap)
    {
        LocalFree(pHeader);
    }
    else
    {
        ASSERT(_ValidatePSLHeapInfo(hHeap));
        HeapFree(((PSLHeapInfo*)hHeap)->hHeapBase, dwFlags, pHeader);
    }

Exit:
    LeaveCriticalSection(_PSLMemCritSec());
    return hMemResult;
}




/*
 *  _PSLMemTrackedFree
 *
 *  Debug local memory deallocator
 *
 *  Arguments:
 *      HANDLE          hHeap           Handle to heap (NULL for local free)
 *      DWORD           dwHeapFlags     Flags used if this is a heap free
 *      PSLHMEM         hMem            Handle to memory to reallocate
 *      DWORD           dwMemFlags      Internal flags
 *
 *  Returns:
 *      PSLHMEM     Handle or pointer to memory
 *
 */

PSLHMEM _PSLMemTrackedFree(HANDLE hHeap, DWORD dwHeapFlags, PSLHMEM hMem,
                            DWORD dwMemFlags)
{
    PSLHMEM                     pResult;
    UNALIGNED PSLMemoryHeader*  pHeader;

    // Make sure we do not have thread synchronization issues

    EnterCriticalSection(_PSLMemCritSec());

    // If we were handed NULL than no work to do

    if (NULL == hMem)
    {
        pResult = NULL;
        goto Exit;
    }

    // Locate the allocation information

    pHeader = _PSLMemFindAlloc(hHeap, hMem, PSLMEMFLAGS_REMOVE);
    if (NULL == pHeader)
    {
        pResult = NULL;
        goto Exit;
    }

    // And check the flags to see if anything is out of order

    if (((PSLMEMFLAGS_CPP_ALLOCATION & pHeader->dwMemFlags) &&
        !(PSLMEMFLAGS_CPP_ALLOCATION & dwMemFlags)) ||
        (!(PSLMEMFLAGS_CPP_ALLOCATION & pHeader->dwMemFlags) &&
         (PSLMEMFLAGS_CPP_ALLOCATION & dwMemFlags)))
    {
        // Allocation performed with C++ allocator but freed with stand
        //  alone allocator

        _PSLMemTraceError("PSLTrackFree: C/C++ Allocator mismatch for 0x%08x",
                            hMem);
    }
    if (((PSLMEMFLAGS_CPP_ARRAYALLOC & pHeader->dwMemFlags) &&
        !(PSLMEMFLAGS_CPP_ARRAYALLOC & dwMemFlags)) ||
        (!(PSLMEMFLAGS_CPP_ARRAYALLOC & pHeader->dwMemFlags) &&
         (PSLMEMFLAGS_CPP_ARRAYALLOC & dwMemFlags)))
    {
        // Allocation performed with C++ array allocator but corresponding
        //  delete was not

        _PSLMemTraceError(
            "PSLTrackFree: C++ Array Allocator mismatch for 0x08%x", hMem);
    }

    // Everything is in shape- go ahead and free the allocation

    if (NULL == hHeap)
    {
        FillMemory(pHeader, LocalSize(pHeader), 0xdf);
        pResult = LocalFree(pHeader);
    }
    else
    {
        ASSERT(_ValidatePSLHeapInfo(hHeap));
        FillMemory(pHeader,
                    HeapSize(((PSLHeapInfo*)hHeap)->hHeapBase, 0, pHeader),
                    0xfd);
        pResult = HeapFree(((PSLHeapInfo*)hHeap)->hHeapBase,
                            dwHeapFlags, pHeader) ? NULL : pHeader;
    }

Exit:
    LeaveCriticalSection(_PSLMemCritSec());
    return pResult;
}


/*
 *  PSLMemTrackedAlloc
 *
 *  Debug local memory allocation
 *
 *  Arguments:
 *      PSLUINT32       dwFlags         Flags
 *      PSLUINT32       cbRequested     Bytes requested for allocation
 *      PSLCSTR         szFile          File where allocation was made
 *      PSLINT32        nLine           Line where allocation was made
 *
 *  Returns:
 *      PSLHMEM     Handle or pointer to memory
 *
 */

PSLHMEM PSLMemTrackedAlloc(PSLUINT32 dwFlags, PSLUINT32 cbRequested,
                            PSLCSTR szFile, PSLINT32 nLine)
{
    // Parameter validation- currently we do not support allocating moveable
    //  memory through this interface

    ASSERT(!(PSLMEM_RELOCATABLE & dwFlags));
    if (PSLMEM_RELOCATABLE & dwFlags)
    {
        return NULL;
    }

    // Call the core implementation

    return _PSLMemTrackedAlloc(NULL, dwFlags, cbRequested, 0, szFile, nLine);
}


/*
 *  PSLMemTrackedReAlloc
 *
 *  Debug local memory reallocation
 *
 *  Arguments:
 *      PSLHMEM         hMem            Handle to memory to reallocate
 *      PSLUINT32       cbRequested     Bytes requested for allocation
 *      PSLUINT32       dwFlags         Flags
 *      PSLCSTR         szFile          File where allocation was made
 *      PSLINT32        nLine           Line where allocation was made
 *
 *  Returns:
 *      PSLHMEM     Handle or pointer to memory
 *
 */

PSLHMEM PSLMemTrackedReAlloc(PSLHMEM hMem, PSLUINT32 cbRequested,
                            PSLUINT32 dwFlags, PSLCSTR szFile, PSLINT32 nLine)
{
    // Call the core implementation

    return _PSLMemTrackedReAlloc(NULL, dwFlags, hMem, cbRequested, 0,
                            szFile, nLine);
}


/*
 *  PSLMemFree
 *
 *  Debug local memory deallocator
 *
 *  Arguments:
 *      PSLHMEM         hMem            Handle to memory to reallocate
 *
 *  Returns:
 *      PSLHMEM     Handle or pointer to memory
 *
 */

PSLHMEM PSLMemFree(PSLHMEM hMem)
{
    return _PSLMemTrackedFree(NULL, 0, hMem, 0);
}


/*
 *  PSLMemSize
 *
 *  Returns the size of a memory allocation.
 *
 *  Arguments:
 *      PSLHMEM         hMem            Handle to the memory to size
 *
 *  Returns:
 *      PSLUINT32   Size of the allocation
 *
 */

PSLUINT32 PSLMemSize(PSLHMEM hMem)
{
    DWORD                       dwSize;
    UNALIGNED PSLMemoryHeader*  pHeader;

    // Parameter validation

    ASSERT(NULL != hMem);

    // Locate the memory header

    pHeader = _PSLMemFindAlloc(NULL, hMem, 0);
    if (NULL == pHeader)
    {
        dwSize = 0;
        goto Exit;
    }

    // And retrieve the size

    dwSize = pHeader->dwSize;

Exit:
    return dwSize;
}


/*
 *  PSLMemCheckSentinals
 *
 *  Checks the current state of the sentinals on this item.
 *
 *  Arguments:
 *      PSLHMEM         hMem            Memory to check
 *
 *  Returns:
 *      PSLBOOL     TRUE if valid
 *
 */

PSLBOOL PSLMemCheckSentinals(PSLHMEM hMem)
{
    UNALIGNED PSLMemoryHeader*    pHeader;

    // Locate the allocation information

    pHeader = _PSLMemFindAlloc(NULL, hMem, 0);
    return (NULL != pHeader);
}


#ifdef __cplusplus

/*
 *  PSLTrackCPPArrayAllocators
 *
 *  Sets the flag to report whether or not C++ array allocations are being
 *  done correctly.
 *
 *  Arguments:
 *      PSLBOOL         fTrack              TRUE to track C++ array allocations
 *
 *  Returns:
 *      Nothing
 *
 */

VOID PSLTrackCPPArrayAllocators(PSLBOOL fTrack)
{
    _FTrackCPPArrays = fTrack;
    return;
}


/*
 *  C++ Memory allocators
 *
 *  Override the C++ memory allocators to use the memory tracking support
 *
 *  Arguments:
 *      See C++ Spec
 *
 *  Returns:
 *      See C++ Spec
 *
 */

void* operator new(size_t uSize)
{
   _PSLMemTraceError("PSL Mem: Use NEW instead of new for memory tracking");
    return _PSLMemTrackedAlloc(NULL, PSLMEM_FIXED, uSize,
                            PSLMEMFLAGS_CPP_ALLOCATION, __FILE__, __LINE__);
}

void* operator new[](size_t uSize)
{
    DWORD       dwFlags;

    _PSLMemTraceError("PSL Mem: Use NEW instead of new for memory tracking");
    dwFlags = PSLMEMFLAGS_CPP_ALLOCATION;
    dwFlags |= (_FTrackCPPArrays) ? PSLMEMFLAGS_CPP_ARRAYALLOC : 0;
    return _PSLMemTrackedAlloc(NULL, PSLMEM_FIXED, uSize, dwFlags,
                            __FILE__, __LINE__);
}

void operator delete(void* pv)
{
    _PSLMemTrackedFree(NULL, 0, pv, PSLMEMFLAGS_CPP_ALLOCATION);
    return;
}

void operator delete[](void* pv)
{
    DWORD       dwFlags;

    dwFlags = PSLMEMFLAGS_CPP_ALLOCATION;
    dwFlags |= (_FTrackCPPArrays) ? PSLMEMFLAGS_CPP_ARRAYALLOC : 0;
    _PSLMemTrackedFree(NULL, 0, pv, dwFlags);
    return;
}

void* operator new(size_t uSize, PSLCSTR szFile, PSLINT32 nLine)
{
    return _PSLMemTrackedAlloc(NULL, PSLMEM_FIXED, uSize,
                            PSLMEMFLAGS_CPP_ALLOCATION, szFile, nLine);
}

void* operator new[](size_t uSize, PSLCSTR szFile, PSLINT32 nLine)
{
    DWORD       dwFlags;

    dwFlags = PSLMEMFLAGS_CPP_ALLOCATION;
    dwFlags |= (_FTrackCPPArrays) ? PSLMEMFLAGS_CPP_ARRAYALLOC : 0;
    return _PSLMemTrackedAlloc(NULL, PSLMEM_FIXED, uSize, dwFlags, szFile, nLine);
}

#endif  // __cplusplus

#else   // !PSL_MEMORY_TRACK

/*
 *  PSLMemAlloc
 *
 *  Local memory allocation
 *
 *  Arguments:
 *      PSLUINT32       dwFlags             Flags
 *      PSLUINT32       cbRequested         Bytes requested for allocation
 *
 *  Returns:
 *      PSLHMEM     Handle or pointer to memory
 *
 */

PSLHMEM PSLMemAlloc(PSLUINT32 dwFlags, PSLUINT32 cbRequested)
{
    DWORD           idxAttempt;
    PSLHMEM         hPSLMem;

    // Avoid threading issues

    EnterCriticalSection(_PSLMemCritSec());

    // Parameter validation- currently we do not support allocating moveable
    //  memory through this interface

    ASSERT(!(PSLMEM_RELOCATABLE & dwFlags));
    if (PSLMEM_RELOCATABLE & dwFlags)
    {
        hPSLMem = NULL;
        goto Exit;
    }

    // Loop so that we can handle out of memory conditions

    for (hPSLMem = NULL, idxAttempt = 0;
            (NULL == hPSLMem) && (idxAttempt < PSLMEM_OOM_ATTEMPTS);
            idxAttempt++)
    {
        // Try the allocation if we are not forcing a failure

        if (!_PSLMemForceFailure())
        {
            // Call the core implementation

            hPSLMem = LocalAlloc(
                            _PSLTranslatePSLMEMtoLMEMFlags(dwFlags),
                            cbRequested);
        }

        // Handle the OOM condition if necessary

        if (NULL == hPSLMem)
        {
            // Only want to keep trying it retry is allowed

            if (PSLOOM_RETRY != _PSLMemHandleOOM(idxAttempt, cbRequested))
            {
                break;
            }
        }
    }

Exit:
    LeaveCriticalSection(_PSLMemCritSec());
    return hPSLMem;
}


/*
 *  PSLMemReAlloc
 *
 *  Debug local memory reallocation
 *
 *  Arguments:
 *      PSLHMEM         hMem            Handle to memory to reallocate
 *      PSLUINT32       cbRequested     Bytes requested for allocation
 *      PSLUINT32       dwFlags         Flags
 *
 *  Returns:
 *      PSLHMEM     Handle or pointer to memory
 *
 */

PSLHMEM PSLMemReAlloc(PSLHMEM hMem, PSLUINT32 cbRequested, PSLUINT32 dwFlags)
{
    BOOL            fHaveCS = FALSE;
    DWORD           idxAttempt;
    PSLHMEM         hPSLMem;

    // Handle the case that we have been handed a NULL initial allocation

    if (NULL == hMem)
    {
        hPSLMem = PSLMemAlloc((dwFlags & ~PSLMEM_RELOCATABLE), cbRequested);
        goto Exit;
    }

    // Avoid threading issues

    EnterCriticalSection(_PSLMemCritSec());
    fHaveCS = TRUE;

    // Loop so that we can handle out of memory conditions

    for (hPSLMem = NULL, idxAttempt = 0;
            (NULL == hPSLMem) && (idxAttempt < PSLMEM_OOM_ATTEMPTS);
            idxAttempt++)
    {
        // See if we are forcing a failure

        if (!_PSLMemForceFailure())
        {
            // Call the core implementation

            hPSLMem = LocalReAlloc(hMem, cbRequested,
                                _PSLTranslatePSLMEMtoLMEMFlags(dwFlags));
        }

        // Handle the OOM condition if necessary

        if (NULL == hPSLMem)
        {
            // Only want to keep trying it retry is allowed

            if (PSLOOM_RETRY != _PSLMemHandleOOM(idxAttempt, cbRequested))
            {
                break;
            }
        }
    }

Exit:
    if (fHaveCS)
    {
        LeaveCriticalSection(_PSLMemCritSec());
    }
    return hPSLMem;
}


/*
 *  PSLMemFree
 *
 *  Debug local memory deallocator
 *
 *  Arguments:
 *      PSLHMEM         hMem            Handle to memory to reallocate
 *
 *  Returns:
 *      PSLHMEM     Handle or pointer to memory
 *
 */

PSLHMEM PSLMemFree(PSLHMEM hMem)
{
    return LocalFree(hMem);
}


/*
 *  PSLMemSize
 *
 *  Returns the size of a memory allocation.
 *
 *  Arguments:
 *      PSLHMEM         hMem            Handle to the memory to size
 *
 *  Returns:
 *      PSLUINT32        Size of the allocation
 *
 */

PSLUINT32 PSLMemSize(PSLHMEM hMem)
{
    return LocalSize(hMem);
}


#ifdef __cplusplus

/*
 *  C++ Memory allocators
 *
 *  Override the C++ memory allocators to use the memory tracking support
 *
 *  Arguments:
 *      See C++ Spec
 *
 *  Returns:
 *      See C++ Spec
 *
 */

void* operator new(size_t uSize)
{
    return PSLMemAlloc(PSLMEM_FIXED, uSize);
}

void* operator new[](size_t uSize)
{
    return PSLMemAlloc(PSLMEM_FIXED, uSize);
}

void operator delete(void* pv)
{
    PSLMemFree(pv);
}

void operator delete[](void* pv)
{
    PSLMemFree(pv);
}

#endif  // __cplusplus

#endif  // PSL_MEMORY_TRACK

/*
 *  _PSLTranslatePSLMEMtoLMEMFlag
 *
 *  Internal function that translated PSLMEM flags
 *  passed to allocation fucntions to LMEM flags
 *  which LocalAlloc and LocalRealloc will understand
 *
 *  Arguments:
 *      dwFlags         flag to translate
 *
 *  Returns:
 *      PSLUINT32       translated flag
 *
 */
PSLUINT32 _PSLTranslatePSLMEMtoLMEMFlags(PSLUINT32 dwFlags)
{
    if (dwFlags == PSLMEM_FIXED)
    {
        dwFlags &= ~PSLMEM_FIXED;
        dwFlags |= LMEM_FIXED;
    }
    if (dwFlags & PSLMEM_RELOCATABLE)
    {
        dwFlags &= ~PSLMEM_RELOCATABLE;
        dwFlags |= LMEM_MOVEABLE;
    }
    if (dwFlags & PSLMEM_ZERO_INIT)
    {
        dwFlags &= ~PSLMEM_ZERO_INIT;
        dwFlags |= LMEM_ZEROINIT;
    }
    return dwFlags;
}

/*
 *  _ValidatePSLHeapInfo
 *
 *  Given an heap validates that it is one of ours
 *
 *  Arguments:
 *      HANDLE          hHeap               Heap to validate
 *
 *  Returns:
 *      BOOL        TRUE if the heap appears valid
 *
 */

BOOL _ValidatePSLHeapInfo(HANDLE hHeap)
{
    BOOL            fResult;
    PSLHeapInfo*    pPSLHeapInfo;

    // This function only supports PSL Media Heaps- not the NULL for process
    //  heaps

    if (NULL == hHeap)
    {
        fResult = FALSE;
        goto Exit;
    }

    // Save some redirection

    pPSLHeapInfo = (PSLHeapInfo*)hHeap;

    // Check for values that must be present

    fResult = ((PSL_MEDIAHEAP_SIGNATURE == pPSLHeapInfo->dwSignature) &&
                            (NULL != pPSLHeapInfo->hHeapBase));

Exit:
    return fResult;
}


/*
 *  PSLHeapCreate
 *
 *  Create a media heap
 *
 *  Arguments:
 *      PSLUINT32       dwFlags             Media heap type flags
 *      PSLUINT32       cbMinSize           Minimum size for the heap
 *      PSLUINT32       cbMaxSize           Maximum size for the heap
 *      PSLHEAP*        phHeap              Return location for heap
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLHeapCreate(PSLUINT32 dwFlags, PSLUINT32 cbMinSize,
                            PSLUINT32 cbMaxSize, PSLHEAP* phHeap)
{
    HANDLE          hHeapBase = NULL;
    PSLHeapInfo*    pPSLHeapInfo;
    PSLSTATUS       ps;

    // Clear result for safety

    if (NULL != phHeap)
    {
        *phHeap = NULL;
    }

    // Validate arguments

    if (NULL == phHeap)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // On Windows if a maximum cache size is specified it is still possible
    //  to have an allocation fail if less than the maximum has been allocated
    //  due to heap overhead.  To work around this problem we always buffer
    //  the maximum size up a little to account for this "feature".  The
    //  number here is based on experience with the file cache and may need
    //  to be tuned later- it basically says that the heap has roughly 3%
    //  overhead.

    if (0 == (PSLHEAP_SHARED & dwFlags))
    {
        cbMaxSize += cbMaxSize >> 5;
    }

    // Perform the requested allocation

    switch (PSLHEAP_CREATE_FLAGS_MASK & dwFlags)
    {
    case PSLHEAP_CACHE:
        // Currently not implemented

        ASSERT(FALSE);
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;

    case PSLHEAP_DEFAULT:
        // Just map this heap to a standard underlying heap

        hHeapBase = HeapCreate(0, cbMinSize, cbMaxSize);
        break;

    case PSLHEAP_SHARED:
        // Validate arguments

        ASSERT(PSLSHAREDHEAP_MIN_SIZE == cbMinSize);
        ASSERT(PSLSHAREDHEAP_MAX_SIZE == cbMaxSize);
        if ((PSLSHAREDHEAP_MIN_SIZE != cbMinSize) ||
            (PSLSHAREDHEAP_MAX_SIZE != cbMaxSize))
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        // See if we need to create the heap before we return it

        if (NULL == _HHeapShared)
        {
            // We have not already created the heap so we need to create it now-
            //  note that we will let the heap manager deal with the maximum
            //  size as recommended

            _HHeapShared = HeapCreate(0, cbMinSize, cbMaxSize);
            if (NULL == _HHeapShared)
            {
                // In the case of the shared heap we support falling back to
                //  the process heap if we had a problem

                _HHeapShared = GetProcessHeap();
            }
        }

        // Use the shared heap as the base heap

        hHeapBase = _HHeapShared;
        break;

    default:
        // We should not get here- we must have a valid type

        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // If we did not get a heap allocated than make sure we handle things

    if (NULL == hHeapBase)
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // If this is the shared heap than we also want to share the heap info

    if (hHeapBase == _HHeapShared)
    {
        // See if we already have a heap info structure available

        if (NULL == _PPSLHeapSharedInfo)
        {
            // Allocate a new one

            _PPSLHeapSharedInfo = (PSLHeapInfo*)PSLMemAlloc(LPTR,
                            sizeof(*_PPSLHeapSharedInfo));
            if (NULL == _PPSLHeapSharedInfo)
            {
                ps = PSLERROR_OUT_OF_MEMORY;
                goto Exit;
            }

            // Initialize the heap

            _PPSLHeapSharedInfo->hHeapBase = hHeapBase;
            hHeapBase = NULL;

            // And add this heap to the list of heaps

            _PPSLHeapSharedInfo->pHeapInfoNext = _PPSLHeapList;
            _PPSLHeapList = _PPSLHeapSharedInfo;
        }

        // Use the shared info

        pPSLHeapInfo = _PPSLHeapSharedInfo;
    }
    else
    {
        // We always allocate a new heap info for non-shared heaps because
        //  we just created the heap

        pPSLHeapInfo = (PSLHeapInfo*)PSLMemAlloc(LPTR,
                            sizeof(*pPSLHeapInfo));
        if (NULL == pPSLHeapInfo)
        {
            ps = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }

        // Set the heap

        pPSLHeapInfo->hHeapBase = hHeapBase;
        hHeapBase = NULL;

        // And add this heap to the list of heaps

        pPSLHeapInfo->pHeapInfoNext = _PPSLHeapList;
        _PPSLHeapList = pPSLHeapInfo;
    }

    // Make sure the signature is valud

    pPSLHeapInfo->dwSignature = PSL_MEDIAHEAP_SIGNATURE;

    // Increment the reference count

    pPSLHeapInfo->cRefs++;

    // Return the heap

    *phHeap = (PSLHEAP)pPSLHeapInfo;

    // Report success

    ps = PSLSUCCESS;

Exit:
    if (NULL != hHeapBase)
    {
        // If this is the shared heap we only want to free it if there is no
        //  shared heap info present- otherwise we want to leave it alone

        if ((hHeapBase != _HHeapShared) ||
            (NULL == _PPSLHeapSharedInfo))
        {
            // If we are destroying the shared heap clear the cached value

            if (hHeapBase == _HHeapShared)
            {
                _HHeapShared = NULL;
            }

            // And destroy the heap

            HeapDestroy(hHeapBase);
        }
    }
    return ps;
}


/*
 *  _PSLHeapDestroy
 *
 *  Internal function that cleans up the media heap
 *
 *  Arguments:
 *      PSLHEAP         hHeap               Heap to close
 *      BOOL            fForcedClosed       Force heap closed regardless of
 *                                              reference count
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE if heap is
 *                        still active
 *
 */

PSLSTATUS _PSLHeapDestroy(PSLHEAP hHeap, BOOL fForceClosed)
{
    PSLHeapInfo*    pPSLHeapInfo;
    PSLHeapInfo*    pHeapInfoCur;
    PSLHeapInfo*    pHeapInfoPrev;
    PSLSTATUS       ps;

    // Parameter validation

    ASSERT(NULL != hHeap);
    if ((NULL == hHeap) || !_ValidatePSLHeapInfo(hHeap))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Save some redirection

    pPSLHeapInfo = (PSLHeapInfo*)hHeap;

    // Decrement the reference count on the heap

    ASSERT(0 != pPSLHeapInfo->cRefs);
    pPSLHeapInfo->cRefs--;

    // If we still have references on the heap and we are not being forced
    //  close than we are done

    if ((0 < pPSLHeapInfo->cRefs) && !fForceClosed)
    {
        // The heap still has references on it- go ahead and report success

        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

#ifdef PSL_MEMORY_TRACK
    // If we are tracking memory report leaks in the heap

    _PSLMemDumpLeaks(hHeap, TRUE);
#endif  // PSL_MEMORY_TRACK

    // Walk through the list of valid heaps to remove this one from the list

    for (pHeapInfoPrev = NULL, pHeapInfoCur = _PPSLHeapList;
            (NULL != pHeapInfoCur) && (pHeapInfoCur != pPSLHeapInfo);
            pHeapInfoPrev = pHeapInfoCur,
            pHeapInfoCur = pHeapInfoCur->pHeapInfoNext)
    {
        // The for loop handles all of the work
    }

    // Make sure we found it

    ASSERT(NULL != pHeapInfoCur);
    if (NULL == pHeapInfoCur)
    {
        // We did not find it in the list- go ahead and report failure

        _PSLMemTraceError("PSLHeapDestroy- heap 0x%08X not found", hHeap);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Pull it out of the list

    if (NULL == pHeapInfoPrev)
    {
        // Heap is at the beginning of the list

        _PPSLHeapList = pPSLHeapInfo->pHeapInfoNext;
    }
    else
    {
        // Pull it out of the middle

        pHeapInfoPrev->pHeapInfoNext = pPSLHeapInfo->pHeapInfoNext;
    }

    // If this is the media heap clear the stashed pointers to it

    if (pPSLHeapInfo->hHeapBase == _HHeapShared)
    {
        _HHeapShared = NULL;
        _PPSLHeapSharedInfo = NULL;
    }

    // Destroy the base heap

    if (!HeapDestroy(pPSLHeapInfo->hHeapBase))
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // And clear the heap information for safety

    ZeroMemory(pPSLHeapInfo, sizeof(*pPSLHeapInfo));

    // And delete it

    PSLMemFree(pPSLHeapInfo);

    // Report success

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  PSLHeapDestroy
 *
 *  Internal function that cleans up the media heap
 *
 *  Arguments:
 *      PSLHEAP         hHeap               Heap to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLHeapDestroy(PSLHEAP hHeap)
{
    return _PSLHeapDestroy(hHeap, FALSE);
}

#ifdef PSL_MEMORY_TRACK

/*
 *  PSLHeapTrackedAlloc
 *
 *  Debug heap allocation routine
 *
 *  Arguments:
 *      PSLHEAP         hHeap               Pointer to the media heap
 *      PSLUINT32       dwFlags             Allocation flags
 *      PSLUINT32       cbRequested         Bytes to allocate
 *      PSLVOID**       ppvMem           Resulting allocation
 *      PSLCSTR         szFile              Filename for allocation
 *      PSLINT32        nLine               Line number
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLHeapTrackedAlloc(PSLHEAP hHeap, PSLUINT32 dwFlags,
                            PSLUINT32 cbRequested, PSLVOID** ppvMem,
                            PSLCSTR szFile, PSLINT32 nLine)
{
    PSLSTATUS       ps;
    PSLVOID*        pvResult;

    // Clear result for safety

    if (NULL != ppvMem)
    {
        *ppvMem = NULL;
    }

    // Validate arguments

    if ((NULL == hHeap) || !_ValidatePSLHeapInfo(hHeap) || (NULL == ppvMem))
    {
        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Do the real work

    pvResult = _PSLMemTrackedAlloc(hHeap, dwFlags, cbRequested, 0,
                            szFile, nLine);

    // Report out of memory

    if (NULL == pvResult)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Return allocation

    *ppvMem = pvResult;

    // Report success

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  PSLHeapTrackedReAlloc
 *
 *  Debug heap reallocation function
 *
 *  Arguments:
 *      PSLHEAP          hHeap              Pointer to the media heap
 *      PSLUINT32        dwFlags            Allocation flags
 *      PSLVOID*         pvMem              Pointer to original allocation
 *      PSLUINT32        cbRequested        Bytes to allocate
 *      PSLVOID**        ppvMem             Resulting allocation
 *      PSLCSTR          szFile             Filename for allocation
 *      PSLINT32         nLine              Line number
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLHeapTrackedReAlloc(PSLHEAP hHeap, PSLUINT32 dwFlags,
                            PSLVOID* pvMem, PSLUINT32 cbRequested,
                            PSLVOID** ppvMem, PSLCSTR szFile, PSLINT32 nLine)
{
    PSLVOID*        pvResult;
    PSLSTATUS       ps;

    // Clear result for safety

    if (NULL != ppvMem)
    {
        *ppvMem = NULL;
    }

    // Validate arguments

    if ((NULL == hHeap) || !_ValidatePSLHeapInfo(hHeap) || (NULL == ppvMem))
    {
        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Handle the case that we have been handed a NULL initial allocation

    if (NULL == pvMem)
    {
        ps = PSLHeapTrackedAlloc(hHeap, dwFlags, cbRequested, &pvResult,
                            szFile, nLine);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }
    else
    {
        pvResult = _PSLMemTrackedReAlloc(hHeap, dwFlags, pvMem, cbRequested, 0,
                            szFile, nLine);
    }

    // Handle out of memory

    if (NULL == pvResult)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    // Return allocation

    *ppvMem = pvResult;

    // Report success

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  PSLHeapTrackedFree
 *
 *  Debug heap free function
 *
 *  Arguments:
 *      PSLHEAP         hHeap               Pointer to the media heap
 *      PSLUINT32       dwFlags             Flags
 *      PSLVOID*        pvMem               Pointer to allocation to free
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

BOOL PSLHeapTrackedFree(PSLHEAP hHeap, PSLUINT32 dwFlags, PSLVOID* pvMem)
{
    PSLSTATUS       ps;

    // Validate arguments

    if ((NULL == hHeap) || !_ValidatePSLHeapInfo(hHeap))
    {
        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Redirect to the tracking function

    ps = (NULL == _PSLMemTrackedFree(hHeap, dwFlags, pvMem, 0)) ?
                            PSLSUCCESS : PSLERROR_PLATFORM_FAILURE;

Exit:
    return ps;
}


/*
 *  PSLHeapSize
 *
 *  Size calculation for items on the media heap.
 *
 *  Arguments:
 *      PSLHEAP         hHeap               Pointer to the media heap
 *      PSLUINT32       dwFlags             Heap flags
 *      PSL_CONST PSLVOID*       pvMem               Pointer to get the size of
 *      PSLUINT32*      pcbSize             Size of the allocation
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLHeapSize(PSLHEAP hHeap, PSLUINT32 dwFlags, PSL_CONST PSLVOID* pvMem,
                            PSLUINT32* pcbSize)
{
    UNALIGNED PSLMemoryHeader*  pHeader;
    PSLSTATUS                   ps;

    // Clear result for safety

    if (NULL != pcbSize)
    {
        *pcbSize = 0;
    }

    // Validate arguments

    if ((NULL == hHeap) || !_ValidatePSLHeapInfo(hHeap) || (0 != dwFlags) ||
        (NULL == pvMem) || (NULL == pcbSize))
    {
        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Locate the memory header

    pHeader = _PSLMemFindAlloc(hHeap, pvMem, 0);
    if (NULL == pHeader)
    {
        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // And retrieve the size

    *pcbSize = pHeader->dwSize;

    // Report success

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  PSLHeapCheckSentinals
 *
 *  Checks the current state of the sentinals on this item.
 *
 *  Arguments:
 *      PSLHEAP         hHeap               Pointer to the media heap
 *      PSL_CONST PSLVOID*       pvMem               Pointer to get the size of
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE if sentinals
 *                        do not match
 *
 */

PSLSTATUS PSLHeapCheckSentinals(PSLHEAP hHeap, LPCVOID pvMem)
{
    PSLSTATUS       ps;

    // Validate arguments

    if ((NULL == hHeap) || !_ValidatePSLHeapInfo(hHeap) || (NULL == pvMem))
    {
        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Locate the allocation information

    ps = (NULL != _PSLMemFindAlloc(hHeap, pvMem, 0)) ?
                                PSLSUCCESS : PSLSUCCESS_FALSE;

Exit:
    return ps;
}

#else   // !PSL_MEMORY_TRACK

/*
 *  PSLHeapAlloc
 *
 *  Heap allocation routine
 *
 *  Arguments:
 *      PSLHEAP         hHeap               Pointer to the media heap
 *      PSLUINT32       dwFlags             Allocation flags
 *      PSLUINT32       cbRequested         Bytes to allocate
 *      PSLVOID**       ppvMem              Resulting allocation
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLHeapAlloc(PSLHEAP hHeap, PSLUINT32 dwFlags, PSLUINT32 cbRequested,
                            PSLVOID** ppvMem)
{
    DWORD           idxAttempt;
    PVOID           pvResult;
    PSLSTATUS       ps;

    // Avoid threading issues

    EnterCriticalSection(_PSLMemCritSec());

    // Clear result for safety

    if (NULL != ppvMem)
    {
        *ppvMem = NULL;
    }

    // Validate arguments

    if ((NULL == hHeap) || !_ValidatePSLHeapInfo(hHeap) || (NULL == ppvMem))
    {
        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Loop so that we can handle out of memory conditions

    for (pvResult = NULL, idxAttempt = 0;
            (NULL == pvResult) && (idxAttempt < PSLMEM_OOM_ATTEMPTS);
            idxAttempt++)
    {
        // See if we are forcing a failure

        if (!_PSLMemForceFailure())
        {
            // Do the real work

            pvResult = HeapAlloc((((PSLHeapInfo*)hHeap)->hHeapBase),
                            dwFlags, cbRequested);
        }

        // Handle the OOM condition if necessary

        if (NULL == pvResult)
        {
            // Only want to keep trying it retry is allowed

            if (PSLOOM_RETRY != _PSLMemHandleOOM(idxAttempt, cbRequested))
            {
                break;
            }
        }
    }

    // See if the allocation failed

    if (NULL == pvResult)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    // Return allocation

    *ppvMem = pvResult;

    // Report success

    ps = PSLSUCCESS;

Exit:
    LeaveCriticalSection(_PSLMemCritSec());
    return ps;
}


/*
 *  PSLHeapReAlloc
 *
 *  Heap reallocation function
 *
 *  Arguments:
 *      PSLHEAP         hHeap               Pointer to the media heap
 *      PSLUINT32       dwFlags             Allocation flags
 *      PSLVOID*        pvMem               Pointer to original allocation
 *      PSLUINT32       cbRequested         Bytes to allocate
 *      PSLVOID**       ppvMem              Resulting allocation
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLHeapReAlloc(PSLHEAP hHeap, PSLUINT32 dwFlags, PSLVOID* pvMem,
                            PSLUINT32 cbRequested, PSLVOID** ppvMem)
{
    BOOL            fHaveCS;
    DWORD           idxAttempt;
    PVOID           pvResult;
    PSLSTATUS       ps;

    // Avoid threading issues

    EnterCriticalSection(_PSLMemCritSec());
    fHaveCS = TRUE;

    // Clear result for safety

    if (NULL != ppvMem)
    {
        *ppvMem = NULL;
    }

    // Validate arguments

    if ((NULL == hHeap) || !_ValidatePSLHeapInfo(hHeap) || (NULL == ppvMem))
    {
        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Handle the case that we have been handed a NULL initial allocation

    if (NULL == pvMem)
    {
        // Release the critical section so that we only have it once and
        //  the OOM handler can release on the callback

        LeaveCriticalSection(_PSLMemCritSec());
        fHaveCS = FALSE;

        // Perform the base allocation

        ps = PSLHeapAlloc(hHeap, dwFlags, cbRequested, ppvMem);
        goto Exit;
    }

    // Loop so that we can handle out of memory conditions

    for (pvResult = NULL, idxAttempt = 0;
            (NULL == pvResult) && (idxAttempt < PSLMEM_OOM_ATTEMPTS);
            idxAttempt++)
    {
        if (!_PSLMemForceFailure())
        {
            // Do the real work

            pvResult = HeapReAlloc((((PSLHeapInfo*)hHeap)->hHeapBase),
                            dwFlags, pvMem, cbRequested);
        }

        // Handle the OOM condition if necessary

        if (NULL == pvResult)
        {
            // Only want to keep trying it retry is allowed

            if (PSLOOM_RETRY != _PSLMemHandleOOM(idxAttempt, cbRequested))
            {
                break;
            }
        }
    }

    // Make sure we have an allocation

    if (NULL == pvResult)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    // Return allocation

    *ppvMem = pvResult;

    // Report success

    ps = PSLSUCCESS;

Exit:
    if (fHaveCS)
    {
        LeaveCriticalSection(_PSLMemCritSec());
    }
    return ps;
}


/*
 *  PSLHeapFree
 *
 *  Debug heap free function
 *
 *  Arguments:
 *      PSLHEAP         hHeap               Pointer to the media heap
 *      PSLUINT32       dwFlags             Flags
 *      PSLVOID*        pvMem               Pointer to allocation to free
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLHeapFree(PSLHEAP hHeap, PSLUINT32 dwFlags, PSLVOID* pvMem)
{
    PSLSTATUS       ps;

    // Validate arguments

    if ((NULL == hHeap) || !_ValidatePSLHeapInfo(hHeap))
    {
        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Redirect to the real function

    ps = HeapFree((((PSLHeapInfo*)hHeap)->hHeapBase), dwFlags, pvMem) ?
                            PSLSUCCESS : PSLERROR_PLATFORM_FAILURE;

Exit:
    return ps;
}


/*
 *  PSLHeapSize
 *
 *  Size calculation for items on the media heap.
 *
 *  Arguments:
 *      PSLHEAP         hHeap               Pointer to the media heap
 *      PSLUINT32       dwFlags             Heap flags
 *      LPCVOID         pvMem               Pointer to get the size of
 *      PSLUINT32*      pcbSize             Size of the allocation
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLHeapSize(PSLHEAP hHeap, PSLUINT32 dwFlags, PSL_CONST PSLVOID* pvMem,
                            PSLUINT32* pcbSize)
{
    DWORD           dwSize;
    PSLSTATUS       ps;

    // Clear result for safety

    if (NULL != pcbSize)
    {
        *pcbSize = 0;
    }

    // Validate arguments

    if ((NULL == hHeap) || !_ValidatePSLHeapInfo(hHeap) || (NULL == pcbSize))
    {
        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Handle the case that we have been handed a NULL initial allocation

    dwSize = HeapSize((((PSLHeapInfo*)hHeap)->hHeapBase), dwFlags, pvMem);
    if (INVALID_HANDLE_SIZE == dwSize)
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // Return the size

    *pcbSize = dwSize;

    // Report success

    ps = PSLSUCCESS;

Exit:
    return ps;
}

#endif  // PSL_MEMORY_TRACK


/*
 *  PSLHeapCompact
 *
 *  Compacts the specified heap.
 *
 *  Arguments:
 *      PSLHEAP         hHeap               Pointer to the media heap
 *      PSLUINT32       dwFlags             Heap flags
 *      PSLUINT32*      pcbLargest          Largest remaining allocation block
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLHeapCompact(PSLHEAP hHeap, PSLUINT32 dwFlags,
                            PSLUINT32* pcbLargest)
{
    PSLSTATUS       ps;
    UINT            uiResult;


    // Clear result for safety

    if (NULL != pcbLargest)
    {
        *pcbLargest = 0;
    }

    // Validate arguments

    if ((NULL == hHeap) || !_ValidatePSLHeapInfo(hHeap))
    {
        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Compact the heap

    uiResult = HeapCompact((((PSLHeapInfo*)hHeap)->hHeapBase), dwFlags);
    if (0 == uiResult)
    {
        // If there is no space left in the heap GetLastError will be NO_ERROR-
        //  report success

        ps = (NO_ERROR != GetLastError()) ?
                            PSLERROR_PLATFORM_FAILURE : PSLSUCCESS;
        goto Exit;
    }

    // Return the size if requested

    if (NULL != pcbLargest)
    {
        *pcbLargest = uiResult;
    }

    // Report success

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  PSLHeapCompactAll
 *
 *  Compacts all heaps in the system
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLHeapCompactAll()
{
    // Call the system API
#ifdef UNDER_CE
    CompactAllHeaps();
#endif /*UNDER_CE*/
    return;
}


/*
 *  PSLGetAlignment
 *
 *  Returns the default alignment for the system
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLUINT32   Alignment
 *
 */

PSLUINT32 PSLGetAlignment()
{
    return sizeof(DWORD);
}


/*
 *  PSLGetPageSize
 *
 *  Returns the default page size
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLUINT32   Page size
 *
 */

PSLUINT32 PSLGetPageSize()
{
    SYSTEM_INFO     si;

    // See if we need to retrieve the page size from the system

    if (0 == _DwPageSize)
    {
        // Retrieve the system information

        GetSystemInfo(&si);

        // And grab the page size

        _DwPageSize = si.dwPageSize;
    }

    // Return the page size

    return _DwPageSize;
}


/*
 *  PSLAlignAllocSizeToPage
 *
 *  Takes an allocation size and returns the number of bytes necessary to
 *  align it on a page size
 *
 *  Arguments:
 *      PSLUINT32       cbSize              Allocation size
 *
 *  Returns:
 *      PSLUINT32   Allocation bytes aligned to page bytes
 *
 */

PSLUINT32 PSLAlignAllocSizeToPage(PSLUINT32 cbSize)
{
    DWORD           dwPageSize;

    // Retrieve the page size

    dwPageSize = PSLGetPageSize();

    // And perform a least integer greator or equal function

    return PSLAllocAlign(cbSize, dwPageSize);
}


/*
 *  PSLGetAvailableMemory
 *
 *  Returns the amount of memory available to the process
 *
 *  Arguments:
 *      None
 *
 *  Returns;
 *      PSLUINT32   Amount of available memory
 *
 */

PSLUINT32 PSLGetAvailableMemory()
{
    MEMORYSTATUS    mst;

    mst.dwLength = sizeof(MEMORYSTATUS);
    GlobalMemoryStatus(&mst);
    return min(mst.dwAvailPhys, mst.dwAvailVirtual);
}


/*
 *  PSLGetTotalMemory
 *
 *  Returns the total amount of memory available to the process
 *
 *  Arguments:
 *      None
 *
 *  Returns;
 *      PSLUINT32   Amount of available memory
 *
 */

PSLUINT32 PSLGetTotalMemory()
{
     MEMORYSTATUS    mst;

     mst.dwLength = sizeof(MEMORYSTATUS);
     GlobalMemoryStatus(&mst);
     return min(mst.dwTotalPhys, mst.dwTotalVirtual);
}


/*
 *  PSLGetFreeMemory
 *
 *  Returns the total amount of memory available on the platform- this
 *  includes within the process and through any means that can access
 *  memory outside of the process.
 *
 *  Arguments:
 *      None
 *
 *  Returns;
 *      PSLUINT32   Amount of available memory
 *
 */

PSLUINT32 PSLGetFreeMemory()
{
     MEMORYSTATUS    mst;

     mst.dwLength = sizeof(MEMORYSTATUS);
     GlobalMemoryStatus(&mst);
     return mst.dwAvailPhys;
}


/*
 *  PSLCryptClearMemory
 *
 *  Clears the contents of memory in a way that hides crypto information.
 *
 *  Arguments:
 *      PSLVOID*        pvData              Memory to clear
 *      PSLUINT32       cbData              Size of memory to clear
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLCryptClearMemory(PSLVOID* pvData, PSLUINT32 cbData)
{
    LPBYTE          pbCur;

    // Validate arguments

    if (NULL != pvData)
    {
        // Loop through and clear each entry

        for (pbCur = (LPBYTE)pvData; 0 < cbData; pbCur++, cbData--)
        {
            *pbCur = 0;
        }
    }
    return;
}


/*
 *  PSLSetMemory
 *
 *  Sets memory at pvDest with a length of cbDest with the specifed byte.
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Destination for set
 *      BYTE            bData               Data to set
 *      PSLUINT32       cbDest              Number of bytes to set
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLSetMemory(PSLVOID* pvDest, PSLBYTE bData, PSLUINT32 cbDest)
{
    memset(pvDest, bData, cbDest);
}


/*
 *  PSLCopyMemory
 *
 *  Copies memory from pvSrc to pvDest.  Assumes that pvSrc and pvDest do
 *  not overlap.
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Destination for copy
 *      PSL_CONST PSLVOID**
 *                      pvSrc               Source for copy
 *      PSLUINT32       cbCopy              Number of bytes to copy
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLCopyMemory(PSLVOID* pvDest, PSL_CONST PSLVOID* pvSrc,
                            PSLUINT32 cbCopy)
{
    CopyMemory(pvDest, pvSrc, cbCopy);
}


/*
 *  PSLMoveMemory
 *
 *  Moves memory from pvSrc to pvDest.  Assumes that pvSrc and pvDest overlap.
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Destination for Move
 *      PSL_CONST PSLVOID**
 *                      pvSrc               Source for Move
 *      PSLUINT32       cbMove              Number of bytes to Move
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLMoveMemory(PSLVOID* pvDest, PSL_CONST PSLVOID* pvSrc,
                            PSLUINT32 cbMove)
{
    MoveMemory(pvDest, pvSrc, cbMove);
}


/*
 *  PSLCompareMemory
 *
 *  Compares memory.
 *
 *  Arguments:
 *      PSL_CONST PSLVOID**
 *                      pvMem1              First memory block to compare
 *      PSL_CONST PSLVOID**
 *                      pvMem2              Second memory block to compare
 *      PSLUINT32       cbCompare           Number of bytes to compare
 *
 *  Returns:
 *      PSLINT32    < 0 == pvMem1 < pvMem2
 *                  = 0 == pvMem1 == pvMem2
 *                  > 0 == pvMem1 > pvMem2
 *
 */

PSLINT32 PSLCompareMemory(PSL_CONST PSLVOID* pvMem1, PSL_CONST PSLVOID* pvMem2,
                            PSLUINT32 cbCompare)
{
    return memcmp(pvMem1, pvMem2, cbCompare);
}
