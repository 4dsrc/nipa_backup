/*
 *  PSLTimer.c
 *
 *  Contains Windows implementation of the PSLTimer utilities
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLPrecomp.h"

/*
 *  PSLQueryPerformanceCounter
 *
 *  retrieves the current value of the high-resolution performance counter
 *
 *  Arguments:
 *      PSLUINT64        *lpPerformanceCount        Pointer to a variable that
 *                                                 receives the current
 *                                                 performance-counter value,
 *                                                 in counts
 *
 *  Returns:
 *      PSLSTATUS     If the function succeeds, returns PSLSUCCESS.
 *                    If the function fails, returns PSLERROR_PLATFORM_FAILURE.
 */

PSLSTATUS PSL_API PSLQueryPerformanceCounter(
                PSLUINT64 *lpPerformanceCount)
{
    return QueryPerformanceCounter(
                (LARGE_INTEGER *)lpPerformanceCount)
           ? PSLSUCCESS
           : PSLERROR_PLATFORM_FAILURE;
}

/*
 *  PSLQueryPerformanceFrequency
 *
 *  retrieves the frequency of the high-resolution performance counter.
 *
 *  Arguments:
 *      PSLUINT64        *lpPerformanceFrequency    Pointer to a variable that
 *                                                 receives the current
 *                                                 performance-counter frequency,
 *                                                 in counts per second. If the
 *                                                 installed hardware does not
 *                                                 support a high-resolution
 *                                                 performance counter, this
 *                                                 parameter can be zero.
 *
 *  Returns:
 *      PSLSTATUS     If the installed hardware supports a high-resolution
 *                    performance counter, returns PSLSUCCESS.
 *                    If the function fails, returns PSLERROR_PLATFORM_FAILURE.
 */

PSLSTATUS PSL_API PSLQueryPerformanceFrequency(
                PSLUINT64 *lpPerformanceFrequency)
{
    return QueryPerformanceFrequency(
                (LARGE_INTEGER *)lpPerformanceFrequency)
           ? PSLSUCCESS
           : PSLERROR_PLATFORM_FAILURE;
}
