/*
 *  PSLAtomic.c
 *
 *  Contains Windows API implementations of the PSL atomic functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLPrecomp.h"

// Local Defines

// Local Types

// Local Functions

// Local Variables

/*
 *  PSLAtomicIncrement
 *
 *  Increments the value specified in an atomic fashion
 *
 *  Arguments:
 *      PSLUINT32*      pui32               Value to increment
 *
 *  Returns:
 *      PSLUINT32   Resulting value after the increment
 *
 */

PSLUINT32 PSLAtomicIncrement(PSLUINT32* pui32)
{
    return InterlockedIncrement((PLONG)pui32);
}


/*
 *  PSLAtomicDecrement
 *
 *  Decrements the value specified in an atomic fashion.
 *
 *  Arguments:
 *      PLSUINT32*      pui32               Value to decrement
 *
 *  Returns:
 *      PSLUINT32   Resulting value after the decrement
 *
 */

PSLUINT32 PSLAtomicDecrement(PSLUINT32* pui32)
{
    return InterlockedDecrement((PLONG)pui32);
}


/*
 *  PSLAtomicAdd
 *
 *  Adds the value of ui32Addend to the value in pui32 in an atomic fashion.
 *
 *  Arguments:
 *      PSLUINT32*      pui32               Value to add to
 *      PSLUINT32       ui32Addend          Value to add
 *
 *  Returns:
 *      PSLUINT32   Resulting value after the add
 *
 */

PSLUINT32 PSLAtomicAdd(PSLUINT32* pui32, PSLUINT32 ui32Addend)
{
    ULONG           ulOriginal;

    ulOriginal = InterlockedExchangeAdd((PLONG)pui32, ui32Addend);
    return (ulOriginal + ui32Addend);
}


/*
 *  PSLAtomicSubract
 *
 *  Subtracts the value of ui32Operand  from pui32 in an atomic fashion
 *
 *  Arguments:
 *      PSLUINT32*      pui32               Value to subtract from
 *      PSLUINT32       ui32Operand         Value to subtract
 *
 *  Returns:
 *      PSLUIN32    Resulting value after the subtract
 *
 */

PSLUINT32 PSLAtomicSubtract(PSLUINT32* pui32, PSLUINT32 ui32Operand)
{
    ULONG           ulTemp;

    ulTemp = (ULONG)-((LONG)ui32Operand);
    ulTemp = InterlockedExchangeAdd((PLONG)pui32, ulTemp);
    return (ulTemp - ui32Operand);
}


/*
 *  PSLAtomicExchange
 *
 *  Exchanges the value of pui32Dest with the value of ul32Value in an
 *  atomic fashion.
 *
 *  Arguments:
 *      PSLUINT32*      pui32Dest           Value to update
 *      PSLUIN32        ui32Value           Value to store
 *
 *  Returns:
 *      PSLUINT32   Contents of pui32Dest before the exchange
 *
 */

PSLUINT32 PSLAtomicExchange(PSLUINT32* pui32Dest, PSLUINT32 ui32Value)
{
    return InterlockedExchange((PLONG)pui32Dest, ui32Value);
}


/*
 *  PSLAtomicCompareExchange
 *
 *  Compares the contents of pui32Dest with ui32Compereand and if they are
 *  equal updates pui32Dest with the value of ui32Exchange in an atomic
 *  fashion.
 *
 *  Arguments:
 *      PSLUINT32*      pui32Dest           Value to be updated
 *      PSLUINT32       ui32Exchange        Value to store
 *      PSLUINT32       ui32Comperand       Value to compare
 *
 *  Returns:
 *      PSLUINT32   Original value of pui32Dest
 *
 */

PSLUINT32 PSLAtomicCompareExchange(PSLUINT32* pui32Dest, PSLUINT32 ui32Exchange,
                            PSLUINT32 ui32Comperand)
{
    return InterlockedCompareExchange((PLONG)pui32Dest, ui32Exchange,
                            ui32Comperand);
}


/*
 *  PSLAtomicExchangePointer
 *
 *  Exchange the value of the ppvDest pointer with pvValue and in an atomic
 *  fashion.
 *
 *  Arguments:
 *      PSLVOID**       ppvDest             Pointer to update
 *      PSLVOID*        pvValue             Pointer to store
 *
 *  Returns:
 *      PSLVOID*    Original value of ppvDest
 *
 */

PSLVOID* PSLAtomicExchangePointer(PSLVOID** ppvDest, PSLVOID* pvValue)
{
    return InterlockedExchangePointer(ppvDest, pvValue);
}


/*
 *  PSLAtomicCompareExchangePointer
 *
 *  Compares the contents of ppvDest with the value of pvComperand and if
 *  they are equal updates ppvDest with the value of pvExchange in an atomic
 *  fashion
 *
 *  Arguments:
 *      PSLVOID**       ppvDest             Pointer to update
 *      PSLVOID*        pvExchange          Pointer to store
 *      PSL_CONST PSLVOID*
 *                      pvComperand         Pointer to compare
 *
 *  Returns:
 *      PSLVOID*    Original value of ppvDest
 *
 */

PSLVOID* PSLAtomicCompareExchangePointer(PSLVOID** ppvDest, PSLVOID* pvExchange,
                            PSL_CONST PSLVOID* pvComperand)
{
    return InterlockedCompareExchangePointer(ppvDest, pvExchange,
                            pvComperand);
}

