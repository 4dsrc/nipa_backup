/*
 *  PSLUnknown.cpp
 *
 *  Definitions of the PSL IUnknown class
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLPrecomp.h"
#include "PSLUnknown.h"


/*
 *  CPSLUnknown::CPSLUnknown
 *
 *  Class constrctor.
 *
 */

CPSLUnknown::CPSLUnknown() :
    m_lInShutdown(FALSE),
    m_cRef(1)
{
}

/*
 *  CPSLUnknown::~CPSLUnknown
 *
 *  Class destructor.  Note that it exists purely to guarantee destructor
 *  virtualization.
 *
 */

CPSLUnknown::~CPSLUnknown()
{
}

/*
 *  CPSLUnknown::OnFinalRelease
 *
 *  Called before the object is deleted.
 *
 */

void CPSLUnknown::OnFinalRelease()
{
}


/*
 *  CPSLUnknown::ShouldDelete
 *
 *  Returns TRUE if delete should be called on the object
 *
 */

BOOL CPSLUnknown::ShouldDelete()
{
    return TRUE;
}


/*
 *  CPSLUnknown::LocateInterface
 *
 *  Helper function for determining inherited interfaces.
 *
 */

HRESULT CPSLUnknown::LocateInterface(REFIID riid, LPVOID* ppvInterface)
{
    return E_NOINTERFACE;
    riid;
    ppvInterface;
}


/*
 *  CPSLUnknown::QueryInterface
 *
 *  Queries the object for the requested interface.
 *
 */

HRESULT CPSLUnknown::QueryInterface( REFIID riid, void **ppvInterface )
{
    HRESULT         hr;

    // Validate Arguments

    if (NULL == ppvInterface)
    {
        hr = E_POINTER;
        goto Exit;
    }

    // Clear result for safety

    *ppvInterface = NULL;

    // Check for IUnknown

    if (IsEqualIID(riid, IID_IUnknown))
    {
        *ppvInterface = (IUnknown*)this;
        ((IUnknown*)*ppvInterface)->AddRef();
        hr = S_OK;
    }
    else
    {
        // Check for alternate interfaces

        hr = LocateInterface( riid, ppvInterface );
    }

Exit:
    return hr;
}

/*
 *  CPSLUnknown::AddRef
 *
 *  Increments reference count.
 *
 */

DWORD CPSLUnknown::AddRef()
{
    return (DWORD)InterlockedIncrement((LPLONG)&m_cRef);
}


/*
 *  CPSLUnknown::CPSLUnknown
 *
 *  Decrements reference count.
 *
 */

DWORD CPSLUnknown::Release()
{
    DWORD           dwResult;

    dwResult = (DWORD)InterlockedDecrement((LPLONG)&m_cRef);
    if( 0 == dwResult )
    {
        BOOL            fShouldDelete;
        BOOL            fInShutdown;

        // In some cases AddRef/Release may be called during a final release
        //  or a delete

        fInShutdown = InterlockedCompareExchange(&m_lInShutdown, TRUE, FALSE);
        if (!fInShutdown)
        {
            // Determine if delete should be called on this instance

            fShouldDelete = ShouldDelete();

            // Perform any cleanup on this object

            OnFinalRelease();

            // Delete the object if we are supposed to

            if (fShouldDelete)
            {
                delete this;
            }
        }
    }
    return dwResult;
}

