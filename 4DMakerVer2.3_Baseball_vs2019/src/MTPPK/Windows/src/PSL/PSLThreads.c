/*
 *  PSLThreads.c
 *
 *  Contains Windows implementation of the PSLThreads API
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef PSL_SINGLE_THREADED

#include "PSLPrecomp.h"

// Ignore invalid pragmas if PREfast is not enabled

#ifndef _PREFAST_
#pragma warning(disable:4068)
#endif

// Local Defines

// Turn off PSLMutex* macros if we support tracing

#if defined (PSL_TRACE) && defined(PSL_TRACE_MUTEX)
#undef PSLMutexOpen
#undef PSLMutexClose
#undef PSLMutexAcquire
#undef PSLMutexRelease
#endif  // PSL_TRACE && PSL_TRACE_MUTEX

// Local Types

typedef struct
{
    PFNPSLTHREADFUNC    pfnThreadFunc;
    PSLVOID*            pvData;
} WindowsThreadData;

// Local Functions

static DWORD WINAPI _WindowsThreadProc(LPVOID pvParam);

static PSLSTATUS _WaitForObject(HANDLE hObject,
                            DWORD dwMSTimeout);

// Local Contstants


/*
 *  _WindowsThreadProc
 *
 *  Thread procedure that meets the Windows calling conventions.
 *
 *  Arguments:
 *      LPVOID          pvParam             Thread param- WindowsThreadData
 *
 *  Returns:
 *      DWORD   Result from PSL thread procedure
 *
 */

DWORD WINAPI _WindowsThreadProc(LPVOID pvParam)
{
    PFNPSLTHREADFUNC    pfnThreadFunc;
    LPVOID              pvData;

    // We should always have a parameter

    ASSERT(NULL != pvParam);

    // Unpack it

    pfnThreadFunc = ((WindowsThreadData*)pvParam)->pfnThreadFunc;
    pvData = ((WindowsThreadData*)pvParam)->pvData;

    // Free the memory

    PSLMemFree(pvParam);

    // And call into the PSL thread function

    return pfnThreadFunc(pvData);
}


/*
 *  _WaitForObject
 *
 *  Helper function for waiting on Windows synchronization objects.
 *
 *  Arguments:
 *      HANDLE          hObject             Windows syncronization object
 *                                            to wait on
 *      DWORD           dwMSTimeout         Period of time that the wait
 *                                            should block before timing out
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *                  PSLSUCCESS_WOULD_BLOCK if 0 == dwMSTimeout and object
 *                    is not in a signalled state
 *                  PSLSUCCESS_TIMEOUT if dwMSTimeout expired
 *
 */

PSLSTATUS _WaitForObject(HANDLE hObject, DWORD dwMSTimeout)
{
    DWORD           dwWaitResult;
    PSLSTATUS       ps;

    // Validate arguments

    if (NULL == hObject)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Attempt to wait for the mutex

    dwWaitResult = WaitForSingleObject(hObject, dwMSTimeout);
    switch (dwWaitResult)
    {
    case WAIT_OBJECT_0:
        // We acquired the mutex succesfully- report success

        ps = PSLSUCCESS;
        break;

    case WAIT_TIMEOUT:
        // Report that the desired object is not signaled

        ps = (0 == dwMSTimeout) ? PSLSUCCESS_WOULD_BLOCK : PSLSUCCESS_TIMEOUT;
        break;

    case WAIT_FAILED:
    default:
        // For some reason we were not able to wait for the mutex- report
        //  failure

        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

Exit:
    return ps;
}
#endif /*!PSL_SINGLE_THREADED*/

#ifdef UNDER_CE
/*
 *  InitializeCriticalSectionAndSpinCount
 *
 *  Wrapper function in the style of the new desktop Win32 API to hide the
 *  need for supporting structured exception handling from people trying
 *  initialize critical sections.  Currently the SpinCount functionality is
 *  not supported.
 *
 *  Arguments:
 *      LPCRITICAL_SECTION
 *                      pCS                 Critical section to initialize
 *      DWORD           dwSpinCount         Desired spin count- must be 0
 *
 *  Returns:
 *      BOOL        TRUE on success, GetLastError for errors
 *
 */

BOOL InitializeCriticalSectionAndSpinCount(LPCRITICAL_SECTION pCS,
                            DWORD dwSpinCount)
{
    BOOL            fResult;

    // Clear result for safety

    if (NULL != pCS)
    {
        ZeroMemory(pCS, sizeof(&pCS));
    }

    // Validate arguments

    if ((NULL == pCS) || (0 != dwSpinCount))
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        fResult = FALSE;
        goto Exit;
    }

    // Setup the exeception handling

    __try
    {
        // Call the base API

        InitializeCriticalSection(pCS);

        // Report success

        fResult = TRUE;
    }
    __except (EXCEPTION_EXECUTE_HANDLER)
    {
        // We are going to assume that this is an out of memory error

        SetLastError(ERROR_NOT_ENOUGH_MEMORY);

        // Clear the critical section again

        ZeroMemory(pCS, sizeof(*pCS));

        // And report failure

        fResult = FALSE;
    }

Exit:
    return fResult;
}
#endif /*UNDER_CE*/

#ifndef PSL_SINGLE_THREADED

/*
 *  PSLThreadCreate
 *
 *  Creates a thread and starts execution at the specified thread function
 *
 *  Arguments:
 *      PFNPSLTHREADFUNC
 *                      pfnThreadFunc       Location thread execution should
 *                                            start
 *      PSLVOID*        pvData              Data to pass to thread function
 *      PSLBOOL         fCreateSuspended    PSLTRUE to create suspended
 *      PSLUINT32       cbStack             Stack size
 *      PSLHANDLE*      phThread            Return location for thread
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadCreate(PFNPSLTHREADFUNC pfnThreadFunc, PSLVOID* pvData,
                            PSLBOOL fCreateSuspended, PSLUINT32 cbStack,
                            PSLHANDLE* phThread)
{
    PSLUINT32           dwFlags;
    HANDLE              hThread = NULL;
    PSLSTATUS           ps;
    WindowsThreadData*  pwtdData = NULL;

    // Clear result for safety

    if (NULL != phThread)
    {
        *phThread = NULL;
    }

    // Validate Arguments

    if ((NULL == pfnThreadFunc) || (NULL == phThread))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Allocate space to hold the information for the Windows compatible
    //  thread procedure

    pwtdData = (WindowsThreadData*)PSLMemAlloc(PSLMEM_FIXED,
                            sizeof(WindowsThreadData));
    if (NULL == pwtdData)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    // And initialize

    pwtdData->pfnThreadFunc = pfnThreadFunc;
    pwtdData->pvData = pvData;

    // Determine the flags

    dwFlags = 0;
    if (cbStack != PSLTHREAD_DEFAULT_STACK_SIZE)
    {
        dwFlags |= STACK_SIZE_PARAM_IS_A_RESERVATION;
    }
    else
    {
        cbStack = 0;
    }
    if (fCreateSuspended)
    {
        dwFlags |= CREATE_SUSPENDED;
    }

    // Create the thread

    hThread = CreateThread(NULL, cbStack, _WindowsThreadProc, pwtdData,
                            dwFlags, NULL);
    if (NULL == hThread)
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // Thread now owns the data

    pwtdData = NULL;

    // Return the thread

    *phThread = hThread;
    hThread = NULL;

    // Report success

    ps = PSLSUCCESS;

Exit:
    if (NULL != hThread)
    {
        CloseHandle(hThread);
    }
    if (NULL != pwtdData)
    {
        PSLMemFree(pwtdData);
    }
    return ps;
}


/*
 *  PSLThreadClose
 *
 *  Closes a thread handle.
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to close
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadClose(PSLHANDLE hThread)
{
    PSLSTATUS       ps;

    // Validate arguments

    if (NULL == hThread)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Close the handle

    ps = CloseHandle(hThread) ? PSLSUCCESS : PSLERROR_PLATFORM_FAILURE;

Exit:
    return ps;
}


/*
 *  PSLThreadTerminate
 *
 *  Forcibly terminates a running thread and sets the threads exit code to the
 *  given value.
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to terminate
 *      PSLUINT32       dwResult            Thread result
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadTerminate(PSLHANDLE hThread, PSLUINT32 dwResult)
{
    PSLSTATUS       ps;

    // Validate arguments

    if (NULL == hThread)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Terminate the thread

#pragma prefast(push)
#pragma prefast(disable:258, "TerminateThread is appropriate at this point")
    ps = TerminateThread(hThread, dwResult) ?
                            PSLSUCCESS : PSLERROR_PLATFORM_FAILURE;
#pragma prefast(pop)

Exit:
    return ps;
}

/*
 *  PSLThreadGetCurrentThreadID
 *
 *  Returns a unique identifier for the current thread
 *
 *  Arguments:
 *      Nothing
 *
 *  Returns:
 *      PSLUINT32       Unique thread identifier
 *
 */

PSLUINT32 PSLThreadGetCurrentThreadID(PSLVOID)
{
    return GetCurrentThreadId();
}

/*
 *  PSLThreadGetResult
 *
 *  Returns the result code from the thread routine.  This code is set via
 *  a call to TerminateThread or by the thread function returning.
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to get exit code from
 *      PSLUINT32       dwMSTimeout         Period to wait for a result
 *                                            in milliseconds
 *      PSLUINT32*      pdwResult           Thread result code
 *
 */

PSLSTATUS PSLThreadGetResult(PSLHANDLE hThread, PSLUINT32 dwMSTimeout,
                            PSLUINT32* pdwResult)
{
    DWORD           dwExitCode;
    DWORD           dwWaitResult;
    BOOL            fExited;
    PSLSTATUS       ps;

    // Clear result for safety

    if (NULL != pdwResult)
    {
        *pdwResult = 0;
    }

    // Validate Arguments

    if ((NULL == hThread) || (NULL == pdwResult))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Assume thread is still running

    fExited = FALSE;
    dwExitCode = 0;

    // Repeat until we get an exit code

    while (!fExited)
    {
        // Retrieve the exit code for the thread

        if (!GetExitCodeThread(hThread, &dwExitCode))
        {
            ps = PSLERROR_PLATFORM_FAILURE;
            goto Exit;
        }

        // Check to see has terminated

        if (STILL_ACTIVE != dwExitCode)
        {
            fExited = TRUE;
            break;
        }

        // Check to see if the thread is signaled- if it is then the
        //  STILL_ACTIVE code is a valid result from the thread and not a
        //  message from GetExitCodeThread

        dwWaitResult = WaitForSingleObject(hThread, 0);
        switch (dwWaitResult)
        {
        case WAIT_OBJECT_0:
            // The thread has terminated- treat this as a valid result code

            fExited = TRUE;
            break;

        case WAIT_TIMEOUT:
            // The thread is still executing- report would block if we are
            //  not supposed to wait

            if (0 == dwMSTimeout)
            {
                ps = PSLSUCCESS_WOULD_BLOCK;
                goto Exit;
            }

            // Wait for the thread to terminate

            dwWaitResult = WaitForSingleObject(hThread, dwMSTimeout);
            switch (dwWaitResult)
            {
            case WAIT_OBJECT_0:
                // Thread has exited- no more work to do

                break;

            case WAIT_TIMEOUT:
                // Inform everyone that we encountered a timeout

                ps = PSLSUCCESS_TIMEOUT;
                goto Exit;

            case WAIT_FAILED:
            default:
                // Encountered a platform error

                ps = PSLERROR_PLATFORM_FAILURE;
                goto Exit;
            }
            break;

        default:
            // Unexpected error condition

            ps = PSLERROR_PLATFORM_FAILURE;
            goto Exit;
        }
    }

    // We should have a valid exit code at this point

    ASSERT(fExited);

    // Return the result code

    *pdwResult = dwExitCode;

    // Report success

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  PSLSleep
 *
 *  Puts the current thread to sleep for the specified amount of time.
 *  PSLSleep(0) allows any other thread to be released and if no process
 *  is available returns immediately to this thread.
 *  PSLSleep(PSLTHREAD_INFINITE) is a synonym for PSLThreadSuspend.
 *
 *  Arguments:
 *      PSLUINT32       dwMSSleep           Milliseconds to sleep
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLSleep(PSLUINT32 dwMSSleep)
{
    PSLSTATUS       ps;

    // If we are called for an infinite sleep then we need to suspend instead
    //  of sleeping

    if (PSLTHREAD_INFINITE == dwMSSleep)
    {
        ps = PSLThreadSuspend(GetCurrentThread());
        goto Exit;
    }

    // Perform the sleep

    Sleep(dwMSSleep);

    // Report success

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  PSLThreadSetPriority
 *
 *  Sets the thread priority base on the specified priority value
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to adjust priority
 *      PSLUINT32       dwPriority          Priority
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadSetPriority(PSLHANDLE hThread, PSLUINT32 dwPriority)
{
    INT             nPriority;
    PSLSTATUS       ps;

    // Determine the correct priority

    switch (dwPriority)
    {
    case PSLTHREADPRIORITY_TIME_CRITICAL:
        nPriority = THREAD_PRIORITY_TIME_CRITICAL;
        break;

    case PSLTHREADPRIORITY_HIGHEST:
        nPriority = THREAD_PRIORITY_HIGHEST;
        break;

    case PSLTHREADPRIORITY_ABOVE_NORMAL:
        nPriority = THREAD_PRIORITY_ABOVE_NORMAL;
        break;

    case PSLTHREADPRIORITY_NORMAL:
        nPriority = THREAD_PRIORITY_NORMAL;
        break;

    case PSLTHREADPRIORITY_BELOW_NORMAL:
        nPriority = THREAD_PRIORITY_BELOW_NORMAL;
        break;

    case PSLTHREADPRIORITY_LOWEST:
        nPriority = THREAD_PRIORITY_LOWEST;
        break;

#ifdef UNDER_CE
    case PSLTHREADPRIORITY_ABOVE_IDLE:
        nPriority = THREAD_PRIORITY_ABOVE_IDLE;
        break;
#endif /*UNDER_CE*/
    case PSLTHREADPRIORITY_IDLE:
        nPriority = THREAD_PRIORITY_IDLE;
        break;

    default:
        // Unknown priority

        ASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // If we were handed a NULL thread handle use the current thread

    if (NULL == hThread)
    {
        hThread = GetCurrentThread();
    }

    // Set the priority

    ps = SetThreadPriority(hThread, nPriority) ?
                            PSLSUCCESS : PSLERROR_PLATFORM_FAILURE;

Exit:
    return ps;
}


/*
 *  PSLThreadGetPriority
 *
 *  Returns the priority of the thread indicated
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread priority to retrieve
 *      PSLUINT32*      pdwPriority         Return buffer for priority
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadGetPriority(PSLHANDLE hThread, PSLUINT32* pdwPriority)
{
    INT             nPriority;
    PSLSTATUS       ps;

    // Clear result for safety

    if (NULL != pdwPriority)
    {
        *pdwPriority = PSLTHREADPRIORITY_UNKNOWN;
    }

    // Validate Arguments

    if (NULL == pdwPriority)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // If we were handed a NULL thread handle use the current thread

    if (NULL == hThread)
    {
        hThread = GetCurrentThread();
    }

    // Retrieve the priority

    nPriority = GetThreadPriority(hThread);
    if (THREAD_PRIORITY_ERROR_RETURN == nPriority)
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // Map Windows priorities to PSL priorities

    switch (nPriority)
    {
    case THREAD_PRIORITY_TIME_CRITICAL:
        *pdwPriority = PSLTHREADPRIORITY_TIME_CRITICAL;
        break;

    case THREAD_PRIORITY_HIGHEST:
        *pdwPriority = PSLTHREADPRIORITY_HIGHEST;
        break;

    case THREAD_PRIORITY_ABOVE_NORMAL:
        *pdwPriority = PSLTHREADPRIORITY_ABOVE_NORMAL;
        break;

    case THREAD_PRIORITY_NORMAL:
        *pdwPriority = PSLTHREADPRIORITY_NORMAL;
        break;

    case THREAD_PRIORITY_BELOW_NORMAL:
        *pdwPriority = PSLTHREADPRIORITY_BELOW_NORMAL;
        break;

    case THREAD_PRIORITY_LOWEST:
        *pdwPriority = PSLTHREADPRIORITY_LOWEST;
        break;

#ifdef UNDER_CE
    case THREAD_PRIORITY_ABOVE_IDLE:
        *pdwPriority = PSLTHREADPRIORITY_ABOVE_IDLE;
        break;
#endif /*UNDER_CE*/

    case THREAD_PRIORITY_IDLE:
        *pdwPriority = PSLTHREADPRIORITY_IDLE;
        break;

    default:
        // Unknown priority

        ASSERT(FALSE);
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // Report success

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  PSLThreadSuspend
 *
 *  Supsends the thread specified.  Suspend/Resume are reference counted so
 *  it is not sufficient to call Suspend twice and resume once to get a
 *  thread executing again.
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to suspend
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadSuspend(PSLHANDLE hThread)
{
    // If we were handed a NULL thread handle use the current thread

    if (NULL == hThread)
    {
        hThread = GetCurrentThread();
    }

    // And suspend the thread

    return SuspendThread(hThread) ? PSLSUCCESS : PSLERROR_PLATFORM_FAILURE;
}


/*
 *  PSLThreadResume
 *
 *  Resumes a suspended thread.
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to suspend
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadResume(PSLHANDLE hThread)
{
    // If we were handed a NULL thread handle use the current thread

    if (NULL == hThread)
    {
        hThread = GetCurrentThread();
    }

    // And suspend the thread

    return (ResumeThread(hThread) !=  0xFFFFFFFF)?
                                PSLSUCCESS : PSLERROR_PLATFORM_FAILURE;
}


/*
 *  PSLMutexOpen
 *
 *  Creates a mutex, or acquires an existing mutex of the same name.
 *
 *  Arguments:
 *      PSLUINT32       dwWaitToAcquire     0 if the mutex is not to be
 *                                            immediately acquired, non-zero
 *                                            time to it is to be acquired
 *      PSLBOOL         fAcquire            TRUE if the mutex should be created
 *                                            and acquired
 *      PSLCWSTR        szName              Name for the mutex
 *      PSLHANDLE*      phPSLMutex          Return buffer for mutex
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success, PSLSUCCESS_EXISTS if mutex
 *                    already existed
 *
 */

PSLSTATUS PSLMutexOpen(PSLUINT32 dwWaitToAcquire, PSLCWSTR szName,
                            PSLHANDLE* phPSLMutex)
{
    DWORD           dwWaitResult;
    BOOL            fCreated;
    HANDLE          hMutex = NULL;
    PSLSTATUS       ps;

    // Clear result for safety

    if (NULL != phPSLMutex)
    {
        *phPSLMutex = NULL;
    }

    // Validate Arguments

    if (NULL == phPSLMutex)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Create the mutex

    hMutex = CreateMutex(NULL, (0 != dwWaitToAcquire), szName);
    if (NULL == hMutex)
    {
        // Some problem in the platform prevented us from acquiring the mutex

        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // Figure out if the mutex already existed

    fCreated = (ERROR_ALREADY_EXISTS != GetLastError());

    // Determine what we believe the correct result is

    ps = fCreated ? PSLSUCCESS : PSLSUCCESS_EXISTS;

    // Find out if we need to acquire the mutex before we can continue

    if ((0 < dwWaitToAcquire) && !fCreated)
    {
        // The mutex existed prior to the call to create- and because it
        //  existed an acquire was not attempted.  Go ahead and wait for the
        //  mutex here.

        dwWaitResult = WaitForSingleObject(hMutex, dwWaitToAcquire);
        switch (dwWaitResult)
        {
        case WAIT_OBJECT_0:
            /*  We have the mutex- no work to do
             */

            PSLASSERT((PSLSUCCESS == ps) || (PSLSUCCESS_EXISTS == ps));
            break;

        case WAIT_TIMEOUT:
            /*  We timed out while waiting to acquire the mutex- report that
             *  it is not available but that we succeeded in creating the
             *  mutex
             */

            ps = PSLSUCCESS_TIMEOUT;
            break;

        case WAIT_FAILED:
        default:
            ps = PSLERROR_PLATFORM_FAILURE;
            goto Exit;
        }
    }

    // Return the mutex

    *phPSLMutex = hMutex;
    hMutex = NULL;

Exit:
    if (NULL != hMutex)
    {
        CloseHandle(hMutex);
    }
    return ps;
}


/*
 *  PSLMutexClose
 *
 *  Closes a mutex object acquired with PSLMutexOpen
 *
 *  Arguments:
 *      PSLHANDLE       hPSLMutex           Mutex to close
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLMutexClose(PSLHANDLE hPSLMutex)
{
    PSLSTATUS       ps;

    // Validate arguments

    if (NULL == hPSLMutex)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Close the mutex

    ps = CloseHandle(hPSLMutex) ? PSLSUCCESS : PSLERROR_PLATFORM_FAILURE;

Exit:
    return ps;
}


/*
 *  PSLMutexAcquire
 *
 *  Attempts to acquire access to the mutex.  The call will be block for
 *  up to dwMSTimeout milliseconds.
 *
 *  Arguments:
 *      PSLHANDLE       hPSLMutex           Mutex to acquire
 *      PSLUINT32       dwMSTimeout         Period of time that the wait
 *                                            should block before timing out
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *                  PSLSUCCESS_WOULD_BLOCK if 0 == dwMSTimeout and object
 *                    is not in a signalled state
 *                  PSLSUCCESS_TIMEOUT if dwMSTimeout expired
 *
 */

PSLSTATUS PSLMutexAcquire(PSLHANDLE hPSLMutex, PSLUINT32 dwMSTimeout)
{
    return _WaitForObject(hPSLMutex, dwMSTimeout);
}


/*
 *  PSLMutexRelease
 *
 *  Releases the held mutex.
 *
 *  Arguments:
 *      PSLHANDLE       hPSLMutex           Mutex to release
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLMutexRelease(PSLHANDLE hPSLMutex)
{
    PSLSTATUS       ps;

    // Validate arguments

    if (NULL == hPSLMutex)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Release the mutex

    ps = ReleaseMutex(hPSLMutex) ? PSLSUCCESS : PSLERROR_PLATFORM_FAILURE;

Exit:
    return ps;
}


#if defined(PSL_TRACE) && defined(PSL_TRACE_MUTEX)

/*
 *  PSLMutexTraceOpen
 *
 *  Creates a mutex, or acquires an existing mutex of the same name.
 *
 *  Arguments:
 *      PSLUINT32       dwWaitToAcquire     0 if the mutex is not to be
 *                                            immediately acquired, non-zero
 *                                            time to it is to be acquired
 *      PSLBOOL         fAcquire            TRUE if the mutex should be created
 *                                            and acquired
 *      PSLCWSTR        szName              Name for the mutex
 *      PSLHANDLE*      phPSLMutex          Return buffer for mutex
 *      PSLCSTR         szFile              File where call was made
 *      PSLUINT32       dwLine              Line in file where call was made
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success, PSLSUCCESS_EXISTS if mutex
 *                    already existed
 *
 */

PSLSTATUS PSLMutexTraceOpen(PSLUINT32 dwWaitToAcquire, PSLCWSTR szName,
                            PSLHANDLE* phPSLMutex, PSLCSTR szFile,
                            PSLUINT32 dwLine)
{
    PSLSTATUS       ps;

    // Use the standard API to do the real work and then provide some
    //  information

    PSLTrace(PSLTP_MUTEX,
            "PSLMutexOpen- Wait: %u\tName: %s\tLine: %u\tFile: %s",
                            dwWaitToAcquire, szName, dwLine, szFile);
    ps = PSLMutexOpen(dwWaitToAcquire, szName, phPSLMutex);
    if (PSL_SUCCEEDED(ps))
    {
        PSLTrace(PSLTP_MUTEX, "PSLMutexOpen- Mutex: 0x%08x", *phPSLMutex);
    }
    else
    {
        PSLTrace(PSLTP_MUTEX, "PSLMutexOpen- Failed: %d", ps);
    }
    return ps;
}


/*
 *  PSLMutexTraceClose
 *
 *  Closes a mutex object acquired with PSLMutexTraceOpen
 *
 *  Arguments:
 *      PSLHANDLE       hPSLMutex           Mutex to close
 *      PSLCSTR         szFile              File where call was made
 *      PSLUINT32       dwLine              Line in file where call was made
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLMutexTraceClose(PSLHANDLE hPSLMutex, PSLCSTR szFile,
                            PSLUINT32 dwLine)
{
    PSLSTATUS       ps;

    // Use the standard API to do the real work and then provide some
    //  information

    ps = PSLMutexClose(hPSLMutex);
    if (PSL_SUCCEEDED(ps))
    {
        PSLTrace(PSLTP_MUTEX, "PSLMutexClose- Mutex: 0x%08x\tLine: %u\tFile: %s",
                            hPSLMutex, dwLine, szFile);
    }
    else
    {
        PSLTrace(PSLTP_MUTEX,
            "PSLMutexClose- Failed: %d\tMutex: 0x%08x\tLine: %u\tFile: %s",
                            ps, hPSLMutex, dwLine, szFile);
    }
    return ps;
}


/*
 *  PSLMutexTraceAcquire
 *
 *  Attempts to acquire access to the mutex.  The call will be block for
 *  up to dwMSTimeout milliseconds.
 *
 *  Arguments:
 *      PSLHANDLE       hPSLMutex           Mutex to acquire
 *      PSLUINT32       dwMSTimeout         Period of time that the wait
 *                                            should block before timing out
 *      PSLCSTR         szFile              File where call was made
 *      PSLUINT32       dwLine              Line in file where call was made
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *                  PSLSUCCESS_WOULD_BLOCK if 0 == dwMSTimeout and object
 *                    is not in a signalled state
 *                  PSLSUCCESS_TIMEOUT if dwMSTimeout expired
 *
 */

PSLSTATUS PSLMutexTraceAcquire(PSLHANDLE hPSLMutex, PSLUINT32 dwMSTimeout,
                            PSLCSTR szFile, PSLUINT32 dwLine)
{
    PSLSTATUS       ps;

    // Use the standard API to do the real work and then provide some
    //  information

    PSLTrace(PSLTP_MUTEX,
            "PSLMutexAcquire- Wait: %u\tMutex: 0x%08x\tLine: %u\tFile: %s",
                            dwMSTimeout, hPSLMutex, dwLine, szFile);
    ps = PSLMutexAcquire(hPSLMutex, dwMSTimeout);
    if (PSL_SUCCEEDED(ps))
    {
        PSLTrace(PSLTP_MUTEX, "PSLMutexAcquire- Succeeded");
    }
    else
    {
        PSLTrace(PSLTP_MUTEX, "PSLMutexAcquire- Failed: %d", ps);
    }
    return ps;
}


/*
 *  PSLMutexTraceRelease
 *
 *  Releases the held mutex.
 *
 *  Arguments:
 *      PSLHANDLE       hPSLMutex           Mutex to release
 *      PSLCSTR         szFile              File where call was made
 *      PSLUINT32       dwLine              Line in file where call was made
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLMutexTraceRelease(PSLHANDLE hPSLMutex, PSLCSTR szFile,
                            PSLUINT32 dwLine)
{
    PSLSTATUS       ps;

    // Use the standard API to do the real work and then provide some
    //  information

    ps = PSLMutexRelease(hPSLMutex);
    if (PSL_SUCCEEDED(ps))
    {
        PSLTrace(PSLTP_MUTEX,
                "PSLMutexRelease- Mutex: 0x%08x\tLine: %u\tFile: %s",
                            hPSLMutex, dwLine, szFile);
    }
    else
    {
        PSLTrace(PSLTP_MUTEX,
            "PSLMutexRelease- Failed: %d\tMutex: 0x%08x\tLine: %u\tFile: %s",
                            ps, hPSLMutex, dwLine, szFile);
    }
    return ps;
}

#endif  // PSL_TRACE && PSL_TRACE_MUTEX


/*
 *  PSLSignalOpen
 *
 *  Opens a signal object (Windows event) for cross thread syncrhonization
 *
 *  Arguments:
 *      PSLBOOL         fSignalled          TRUE if the signal should be
 *                                            initialized in a signalled state
 *      PSLBOOL         fManualReset        TRUE of the signal should be
 *                                            reset manually
 *      PSLCWSTR        szName              Signal name
 *      PSLHANDLE*      phPSLSignal         Return buffer for the signal
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success, PSLSUCCESS_EXISTS if signal
 *                    already existed
 *
 */

PSLSTATUS PSLSignalOpen(PSLBOOL fSignalled, PSLBOOL fManualReset,
                                PSLCWSTR szName, PSLHANDLE* phPSLSignal)
{
    HANDLE          hEvent = NULL;
    PSLSTATUS       ps;

    // Clear result for safety

    if (NULL != phPSLSignal)
    {
        *phPSLSignal = NULL;
    }

    // Validate arguments

    if (NULL == phPSLSignal)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Create the signal

    hEvent = CreateEvent(NULL, fManualReset, fSignalled, szName);
    if (NULL == hEvent)
    {
        // Report a failure in the platform

        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    // Return the event as the signal

    *phPSLSignal = hEvent;
    hEvent = NULL;

    // Determine if we created the event or not for the result

    ps = (ERROR_ALREADY_EXISTS != GetLastError()) ?
                            PSLSUCCESS : PSLSUCCESS_EXISTS;

Exit:
    if (NULL != hEvent)
    {
        CloseHandle(hEvent);
    }
    return ps;
}


/*
 *  PSLSignalClose
 *
 *  Closes the specified signal object
 *
 *  Arguments:
 *      PSLHANDLE       phPSLSignal         Signal to close
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLSignalClose(PSLHANDLE phPSLSignal)
{
    PSLSTATUS       ps;

    // Validate arguments

    if (NULL == phPSLSignal)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Close the event

    ps = CloseHandle(phPSLSignal) ? PSLSUCCESS: PSLERROR_PLATFORM_FAILURE;

Exit:
    return ps;
}


/*
 *  PSLSignalSet
 *
 *  Sets the specified signal to the signalled state
 *
 *  Arguments:
 *      PSLHANDLE       phPSLSignal         Signal to set
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLSignalSet(PSLHANDLE phPSLSignal)
{
    PSLSTATUS       ps;

    // Validate arguments

    if (NULL == phPSLSignal)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Set the event

    ps = SetEvent(phPSLSignal) ? PSLSUCCESS: PSLERROR_PLATFORM_FAILURE;

Exit:
    return ps;
}


/*
 *  PSLSignalReset
 *
 *  Resets the specified signal object to non-signalled
 *
 *  Arguments:
 *      PSLHANDLE       phPSLSignal         Signal to reset
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLSignalReset(PSLHANDLE phPSLSignal)
{
    PSLSTATUS       ps;

    // Validate arguments

    if (NULL == phPSLSignal)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Reset the event

    ps = ResetEvent(phPSLSignal) ? PSLSUCCESS: PSLERROR_PLATFORM_FAILURE;

Exit:
    return ps;
}


/*
 *  PSLSignalWait
 *
 *  Waits for the specified signal to enter the signalled state.  The call
 *  will block for the specified timeout value.
 *
 *  Arguments:
 *      PSLHANDLE       phPSLSignal         Signal to wait on
 *      PSLUINT32       dwMSTimeout         Period of time that the wait
 *                                            should block before timing out
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *                  PSLSUCCESS_WOULD_BLOCK if 0 == dwMSTimeout and object
 *                    is not in a signalled state
 *                  PSLSUCCESS_TIMEOUT if dwMSTimeout expired
 *
 */

PSLSTATUS PSLSignalWait(PSLHANDLE phPSLSignal, PSLUINT32 dwMSTimeout)
{
    return _WaitForObject(phPSLSignal, dwMSTimeout);
}

#endif /*!PSL_SINGLE_THREADED*/
