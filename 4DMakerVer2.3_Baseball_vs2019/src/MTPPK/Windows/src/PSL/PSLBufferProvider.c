/*
 *  PSLBufferProvider.c
 *
 *  Contains the message queue provider implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSL.h"
#include "PSLBufferProvider.h"

PSLSTATUS PSLBufferProviderGet(PSLRESPROVIDERINFO* pProviderInfo,
                                 PSLUINT32 dwHint, PSLVOID** ppvRes,
                                 PSLUINT32* pdwSize)
{
    PSLSTATUS status;
    PSLBUFFERPROVIDERINFO* pBufInfo = PSLNULL;
    PSLBYTE* pBuf = PSLNULL;
    PSLBOOL fAcquired = PSLFALSE;

    if (PSLNULL != ppvRes)
    {
        *ppvRes = PSLNULL;
    }

    if (PSLNULL != pdwSize)
    {
        *pdwSize = 0;
    }

    if (PSLNULL == pProviderInfo || 0 == dwHint ||
        PSLNULL == ppvRes || PSLNULL == pdwSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pBufInfo = (PSLBUFFERPROVIDERINFO*)pProviderInfo;

    status = PSLMutexAcquire(pBufInfo->baseInfo.hProviderLock,
                                PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    fAcquired = PSLTRUE;

    pBuf = PSLMemAlloc(PSLMEM_PTR, dwHint * sizeof(PSLBYTE));
    if (PSLNULL == pBuf)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    *pdwSize = dwHint * sizeof(PSLBYTE);

    *ppvRes = pBuf;
    pBuf = PSLNULL;

Exit:

    if (PSLNULL != pBuf)
    {
        PSLMemFree(pBuf);
    }

    if ((PSLTRUE == fAcquired) && (PSLNULL != pBufInfo))
    {
        PSLMutexRelease(pBufInfo->baseInfo.hProviderLock);
    }
    return status;
}

PSLSTATUS PSLBufferProviderRelease(PSLRESPROVIDERINFO* pProviderInfo,
                                     PSLVOID* pvRes)
{
    PSLSTATUS status;
    PSLBUFFERPROVIDERINFO* pBufInfo = PSLNULL;
    PSLBOOL fAcquired = PSLFALSE;

    if (PSLNULL == pProviderInfo || PSLNULL == pvRes)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pBufInfo = (PSLBUFFERPROVIDERINFO*)pProviderInfo;

    status = PSLMutexAcquire(pBufInfo->baseInfo.hProviderLock,
                                PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    fAcquired = PSLTRUE;

    PSLMemFree((PSLBYTE*)pvRes);

Exit:
    if ((PSLTRUE == fAcquired) && (PSLNULL != pBufInfo))
    {
        PSLMutexRelease(pBufInfo->baseInfo.hProviderLock);
    }
    return status;
}

PSLSTATUS PSLBufferProviderCreate(PSLBUFFERPROVIDERINFO** ppBufInfo)
{
    PSLSTATUS status;
    PSLBUFFERPROVIDERINFO* pBufInfo = PSLNULL;

    if (PSLNULL != ppBufInfo)
    {
        *ppBufInfo = PSLNULL;
    }

    if (PSLNULL == ppBufInfo)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pBufInfo = PSLMemAlloc(PSLMEM_PTR,
                            sizeof(PSLBUFFERPROVIDERINFO));

    if (PSLNULL == pBufInfo)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = PSLMutexOpen(PSLFALSE, PSLNULL,
                            &pBufInfo->baseInfo.hProviderLock);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pBufInfo->baseInfo.pfnProviderGet = PSLBufferProviderGet;

    pBufInfo->baseInfo.pfnProviderInit = PSLNULL;

    pBufInfo->baseInfo.pfnProviderRelease = PSLBufferProviderRelease;

    pBufInfo->baseInfo.pfnProviderUninit = PSLNULL;

    *ppBufInfo = pBufInfo;
    pBufInfo = PSLNULL;

Exit:
    if (PSLNULL != pBufInfo)
    {
        PSLMemFree(pBufInfo);
    }
    return status;
}

PSLSTATUS PSLBufferProviderDestroy(PSLBUFFERPROVIDERINFO* pBufInfo)
{
    PSLSTATUS status;

    if (PSLNULL == pBufInfo)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLMutexClose(pBufInfo->baseInfo.hProviderLock);

    PSLMemFree(pBufInfo);

    status = PSLSUCCESS;

Exit:
    return status;
}

