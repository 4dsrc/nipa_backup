/*
 *  PSLLocale.c
 *
 *  Contains definitions of the locale support functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLPrecomp.h"

/*
 *  PSLIsHexDigitA
 *
 *  Returns TRUE if the specified character is a valid character representing
 *  a hex digit.
 *
 *  Arguments:
 *      PSLCHAR         ch                  Character to examine
 *
 *  Returns:
 *      PSLBOOL     PSLTRUE if it is, PSLFALSE otherwise
 *
 */

PSLBOOL PSLIsHexDigitA(PSLCHAR ch)
{
    return (('0' <= ch) && (ch <= '9')) ||
            (('a' <= PSLToLower(ch)) && (PSLToLower(ch) <= 'f'));
}


/*
 *  PSLIsHexDigit
 *
 *  Returns TRUE if the specified character is a valid character representing
 *  a hex digit.
 *
 *  Arguments:
 *      PSLWCHAR        ch                  Character to examine
 *
 *  Returns:
 *      PSLBOOL     PSLTRUE if it is, PSLFALSE otherwise
 *
 */

PSLBOOL PSLIsHexDigit(PSLWCHAR ch)
{
    return ((L'0' <= ch) && (ch <= L'9')) ||
            ((L'a' <= PSLToLower(ch)) && (PSLToLower(ch) <= L'f'));
}


/*
 *  PSLToLowerA
 *
 *  If necessary converts the specified character from it's uppercase for
 *  to it's lower case form.
 *
 *  Arguments:
 *      PSLCHAR         ch                  Character to convert
 *
 *  Returns:
 *      PSLCHAR         Converted character if necessary
 *
 */

PSLCHAR PSLToLowerA(PSLCHAR ch)
{
    return ((ch < 'A') || ('Z' < ch)) ? ch : ch + ('a' - 'A');
}


/*
 *  PSLToLower
 *
 *  If necessary converts the specified character from it's uppercase for
 *  to it's lower case form.
 *
 *  Arguments:
 *      PSLWCHAR        ch                  Character to convert
 *
 *  Returns:
 *      PSLWCHAR        Converted character if necessary
 *
 */

PSLWCHAR PSLToLower(PSLWCHAR ch)
{
    return ((ch < L'A') || (L'Z' < ch)) ? ch : ch + (L'a' - L'A');
}


