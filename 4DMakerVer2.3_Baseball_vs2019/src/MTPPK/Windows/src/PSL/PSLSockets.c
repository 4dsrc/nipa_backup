/*
 *  PSLSockets.h
 *
 *  Contains Windows Sockets specific implementation of the PSLSockets
 *  APIs.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLWindows.h"
#include "PSLWinSafe.h"
#include "PSL.h"
#include "PSLWinMsgQueue.h"
#include "PSLSockets.h"

/*  Local Defines
 */

#define BINDTRANSPORT_MSG_POOL_SIZE     64

#define MAX_SOCKETS                     32

#define SOCKETSELECT_CLOSE              0x80000000

/*  Local Types
 */

/*
 *  SOCKETSELECT
 *
 *  Tracks information about the socket events
 *
 */

enum
{
    SOCKETSELECTSTATE_UNKNOWN = 0,
    SOCKETSELECTSTATE_ACTIVE,
    SOCKETSELECTSTATE_CLOSED,
};

typedef struct _SOCKETSELECT
{
    PSLUINT32               dwState;
    SOCKET                  sock;
    WSAEVENT                hEvent;
    PSLMSGQUEUE             mqEvent;
    PSLMSGPOOL              mpEvent;
    PSLPARAM                pvParam;
    PSLUINT32               dwEvents;
    PSLUINT32               dwEventMask;
} SOCKETSELECT;

enum
{
    SOCKETTHREADSTATE_UNKNOWN = 0,
    SOCKETTHREADSTATE_INITIALIZING,
    SOCKETTHREADSTATE_RUNNING,
    SOCKETTHREADSTATE_CLOSING,
    SOCKETTHREADSTATE_CLOSED
};

enum
{
    SOCKETTHREADMSG_ADD_SOCKET =            MAKE_PSL_NORMAL_MSGID(PSL_USER, 1),
    SOCKETTHREADMSG_REMOVE_SOCKET =         MAKE_PSL_NORMAL_MSGID(PSL_USER, 2),
    SOCKETTHREADMSG_RESET_SOCKET =          MAKE_PSL_NORMAL_MSGID(PSL_USER, 3),
    SOCKETTHREADMSG_SHUTDOWN =              MAKE_PSL_NORMAL_MSGID(PSL_USER, 4),
};


/*  Local Functions
 */

static PSLCSTR _GetEventName(PSLUINT32 dwEvent);

static PSLSTATUS _AddSocketToList(SOCKETSELECT* rgssList,
                            PSLUINT32 cMaxSockets,
                            PSLUINT32* pcSockets,
                            SOCKET sock,
                            PSLMSGQUEUE mqEvent,
                            PSLMSGPOOL mpEvent,
                            PSLPARAM pvParam,
                            PSLUINT32 dwEvents);

static PSLSTATUS _RemoveSocketFromList(SOCKETSELECT* rgssList,
                            PSLUINT32* pcSockets,
                            SOCKET sock);

static PSLSTATUS _ResetSocket(SOCKETSELECT* rgssList,
                            PSLUINT32 cSockets,
                            SOCKET sock,
                            PSLBOOL fResetMask,
                            PSLUINT32 dwData);

static PSLSTATUS _SendSocketEvents(PSLUINT32 dwNetworkEvents,
                            PSLUINT32* pdwEventMask,
                            SOCKET sock,
                            PSLMSGQUEUE mqEvent,
                            PSLMSGPOOL mpEvent,
                            PSLPARAM pvParam);

static PSLUINT32 _SocketSelectThread(PSLVOID* pvParam);

static PSLSTATUS _CheckSocketThreadState(PSLBOOL fMustBeRunning);

/*  Local Variables
 */

static PSLBOOL              _FWinSockRunning = PSLFALSE;

static const PSLWCHAR       _RgchSocketInitSignal[] = L"PSLSocket Init Signal";
static const PSLWCHAR       _RgchSocketInitMutex[] = L"PSLSocket Init Mutex";

static PSLUINT32            _DwSocketState = SOCKETTHREADSTATE_UNKNOWN;
static PSLUINT32            _DwSocketRefs = 0;
static PSLHANDLE            _HThreadSocket = PSLNULL;
static PSLMSGQUEUE          _MqSocket = PSLNULL;
static PSLMSGPOOL           _MpSocket = PSLNULL;

static const PSLUINT32      _RgdwOrderedNetworkEvents[FD_MAX_EVENTS] =
{
    FD_ACCEPT,
    FD_CONNECT,
    FD_READ,
    FD_WRITE,
    FD_OOB,
    FD_CLOSE,
    FD_QOS,
    FD_GROUP_QOS,
    FD_ROUTING_INTERFACE_CHANGE,
    FD_ADDRESS_LIST_CHANGE
};

static const PSLCHAR        _RgchAccept[] = "FD_ACCEPT";
static const PSLCHAR        _RgchConnect[] = "FD_CONNECT";
static const PSLCHAR        _RgchRead[] = "FD_READ";
static const PSLCHAR        _RgchWrite[] = "FD_WRITE";
static const PSLCHAR        _RgchOOB[] = "FD_OOB";
static const PSLCHAR        _RgchClose[] = "FD_CLOSE";
static const PSLCHAR        _RgchQOS[] = "FD_QOS";
static const PSLCHAR        _RgchGroupQOS[] = "FD_GROUP_QOS";
static const PSLCHAR        _RgchRIChange[] = "FD_ROUTING_INTERFACE_CHANGE";
static const PSLCHAR        _RgchALChange[] = "FD_ADDRESS_LIST_CHANGE";
static const PSLCHAR        _RgchMultiple [] = "Multiple Events";

static const PSLCHAR        _RgchMsgUnknown[] = "UNKNOWN";
static const PSLCHAR        _RgchMsgDataAvailable[] = "DATA_AVAILABLE";
static const PSLCHAR        _RgchMsgReadyToSend[] = "READY_TO_SEND";
static const PSLCHAR        _RgchMsgOOB[] = "OUT_OF_BAND";
static const PSLCHAR        _RgchMsgAccept[] = "ACCEPT";
static const PSLCHAR        _RgchMsgConnect[] = "CONNECT";
static const PSLCHAR        _RgchMsgClose[] = "CLOSE";
static const PSLCHAR        _RgchMsgQOS[] = "QOS";
static const PSLCHAR        _RgchMsgGroupQOS[] = "GROUP_QOS";
static const PSLCHAR        _RgchMsgRIChange[] = "ROUTE_IFACE_CHANGE";
static const PSLCHAR        _RgchMsgALChange[] = "ADDRESS_LIST_CHANGE";

/*
 *  _GetEventName
 *
 *  Helper function that maps between FD_* bits and strings for debugging
 *  purposes.
 *
 *  Arguments:
 *      PSLUINT32       dwEvent             Event to map
 *
 *  Returns:
 *      PSLCSTR         ANSI string representing event
 *
 */

PSLCSTR _GetEventName(PSLUINT32 dwEvent)
{
    PSLCSTR         szResult;

    /*  Determine which event we have
     */

    switch (dwEvent)
    {
    case FD_ACCEPT:
        szResult = _RgchAccept;
        break;

    case FD_CONNECT:
        szResult = _RgchConnect;
        break;

    case FD_READ:
        szResult = _RgchRead;
        break;

    case FD_WRITE:
        szResult = _RgchWrite;
        break;

    case FD_OOB:
        szResult = _RgchOOB;
        break;

    case FD_CLOSE:
        szResult = _RgchClose;
        break;

    case FD_QOS:
        szResult = _RgchQOS;
        break;

    case FD_GROUP_QOS:
        szResult = _RgchGroupQOS;
        break;

    case FD_ROUTING_INTERFACE_CHANGE:
        szResult = _RgchRIChange;
        break;

    case FD_ADDRESS_LIST_CHANGE:
        szResult = _RgchALChange;
        break;

    default:
        /*  No matching event- assume this is a multiple
         */

        szResult = _RgchMultiple;
        break;
    }
    return szResult;
}


/*
 *  _AddSocketToList
 *
 *  Adds a socket to an array containing the currently tracked sockets
 *
 *  Arguments:
 *      SOCKETSELECT*   rgssList            Array to add socket to
 *      PSLUINT32       cMaxSockets         Maximum number of sockets in list
 *      PSLUINT32*      pcSockets           Number of sockets in list [in/out]
 *      SOCKET          sock                Socket to add
 *      PSLMSGQUEUE     mqEvent             Message queue to send event to
 *      PSLMSGPOOL      mpEvent             Message pool to use for messages
 *      PSLUINT32       dwEvents            Events
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _AddSocketToList(SOCKETSELECT* rgssList, PSLUINT32 cMaxSockets,
                            PSLUINT32* pcSockets, SOCKET sock,
                            PSLMSGQUEUE mqEvent, PSLMSGPOOL mpEvent,
                            PSLPARAM pvParam, PSLUINT32 dwEvents)
{
    PSLUINT32           dwNetworkEvents;
    INT                 errSock;
    WSAEVENT            hEvent;
    PSLUINT32           idxSocket;
    PSLSTATUS           ps;

    /*  Validate arguments
     */

    if ((PSLNULL == rgssList) || (0 == cMaxSockets) || (PSLNULL == pcSockets) ||
        (INVALID_SOCKET == sock) || (PSLNULL == mqEvent) ||
        (PSLNULL == mpEvent) || (0 == dwEvents))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Start walking through the list of sockets looking for a match.  For
     *  performance reasons we keep the list in sorted order.
     */

    for (idxSocket = 0;
            (idxSocket < *pcSockets) && (sock != rgssList[idxSocket].sock);
            idxSocket++)
    {
    }

    /*  See if we need to insert a new socket into the list
     */

    if ((idxSocket == *pcSockets) || (sock != rgssList[idxSocket].sock))
    {
        /*  Make sure there is space for the new socket
         */

        if ((*pcSockets + 1) >= cMaxSockets)
        {
            ps = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }

        /*  Allocate an event to track receive notifications on
         */

        hEvent = WSACreateEvent();
        if (WSA_INVALID_EVENT == hEvent)
        {
            ps = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }

        /*  Make space for the new entry in the array
         */

        PSLMoveMemory(&(rgssList[idxSocket + 1]), &(rgssList[idxSocket]),
                            ((*pcSockets - idxSocket) * sizeof(rgssList[0])));

        /*  Fill in the core portion of the entry
         *
         *  NOTE: We always set the queues at the end so we only initialize
         *  the event and the event mask at this point to make sure that
         *  initialization completes correctly.
         */

        rgssList[idxSocket].sock = sock;
        rgssList[idxSocket].hEvent = hEvent;
        rgssList[idxSocket].dwEvents = 0;
        rgssList[idxSocket].dwEventMask = 0;
        hEvent = WSA_INVALID_EVENT;

        /*  Mark the socket as active
         */

        rgssList[idxSocket].dwState = SOCKETSELECTSTATE_ACTIVE;

        /*  And increment the count
         */

        (*pcSockets)++;

        /*  Provide some feedback
         */

        PSLTraceProgress("PSLSockets: Socket 0x%08x added- MQ: 0x%08x MP: 0x%08x",
                            sock, mqEvent, mpEvent);
    }

    /*  If the socket is closed report failure
     */

    if (SOCKETSELECTSTATE_CLOSED & rgssList[idxSocket].dwState)
    {
        ps = PSLERROR_CONNECTION_CLOSED;
        goto Exit;
    }

    /*  Make sure we have work to do
     */

    if ((mqEvent == rgssList[idxSocket].mqEvent) &&
        (mpEvent == rgssList[idxSocket].mpEvent) &&
        (pvParam == rgssList[idxSocket].pvParam) &&
        (dwEvents == rgssList[idxSocket].dwEvents))
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Change the event management only if necessary
     */

    if (dwEvents != rgssList[idxSocket].dwEvents)
    {
        /*  Determine exaclty which network events are required- switching
         *  a SOCKETSELECT_CLOSE to an FD_CLOSE in the process
         */

        dwNetworkEvents = (~SOCKETSELECT_CLOSE & dwEvents) |
                            ((SOCKETSELECT_CLOSE & dwEvents) ? FD_CLOSE : 0);

        /*  And tell WinSock to notify us on events
         */

        errSock = WSAEventSelect(rgssList[idxSocket].sock,
                            rgssList[idxSocket].hEvent, dwNetworkEvents);
        if (SOCKET_ERROR == errSock)
        {
            ps = PSLSocketErrorToPSLStatus(rgssList[idxSocket].sock, errSock);
            goto Exit;
        }

        /*  Provide some feedback
         */

        PSLTraceProgress("PSLSockets: Socket 0x%08x enabled- EV: 0x%08x",
                            sock, dwEvents);
    }

    /*  If we change queues resend all of the events in the mask that are
     *  still being requested- making sure to specify a mask that allows
     *  them to all be sent
     */

    if (mqEvent != rgssList[idxSocket].mqEvent)
    {
        dwNetworkEvents = 0;
        ps = _SendSocketEvents((rgssList[idxSocket].dwEventMask & dwEvents),
                                &dwNetworkEvents, rgssList[idxSocket].sock,
                                mqEvent, mpEvent, pvParam);
        PSLASSERT(PSL_SUCCEEDED(ps));

        /*  Provide some feedback
         */

        PSLTraceProgress("PSLSockets: Socket 0x%08x queue- Old: 0x%08x "
                            "New: 0x%08x", sock, rgssList[idxSocket].mqEvent,
                            mqEvent);
    }

    /*  Update the state
     */

    rgssList[idxSocket].mqEvent = mqEvent;
    rgssList[idxSocket].mpEvent = mpEvent;
    rgssList[idxSocket].pvParam = pvParam;
    rgssList[idxSocket].dwEvents = dwEvents;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _RemoveSocketFromList
 *
 *  Removes a socket from the list of watched sockets.
 *
 *  Arguments:
 *      SOCKETSELECT*   rgssList            List of sockets to manipulate
 *      PSLUINT32*      pcSockets           Number of sockets in list [in/out]
 *      SOCKET          sock                Socket to remove
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if no
 *                        match
 *
 */

PSLSTATUS _RemoveSocketFromList(SOCKETSELECT* rgssList, PSLUINT32* pcSockets,
                            SOCKET sock)
{
    PSLUINT32           idxSocket;
    PSLSTATUS           ps;

    /*  Validate arguments
     */

    if ((PSLNULL == rgssList) || (PSLNULL == pcSockets) ||
        (INVALID_SOCKET == sock))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Start walking through the list of sockets looking for a match.  For
     *  performance reasons we keep the list in sorted order.
     */

    for (idxSocket = 0;
            (idxSocket < *pcSockets) && (sock != rgssList[idxSocket].sock);
            idxSocket++)
    {
    }

    /*  If we did not find a match then we are done
     */

    if ((idxSocket == *pcSockets) || (sock != rgssList[idxSocket].sock))
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Turn of eventing for this socket if it is not closed
     */

    if (SOCKETSELECTSTATE_CLOSED != rgssList[idxSocket].dwState)
    {
        /*  Ignore the error- the socket could have closed and we not
         *  received a notification yet
         */

        WSAEventSelect(rgssList[idxSocket].sock, rgssList[idxSocket].hEvent, 0);
    }

    /*  Release the event
     */

    WSACloseEvent(rgssList[idxSocket].hEvent);

    /*  And remove the socket from the list
     */

    PSLMoveMemory(&(rgssList[idxSocket]), &(rgssList[idxSocket + 1]),
                        ((*pcSockets - idxSocket - 1) * sizeof(rgssList[0])));
    (*pcSockets)--;

    /*  Provide some feedback
     */

    PSLTraceProgress("PSLSockets: Socket 0x%08x Removed", sock);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _ResetSocket
 *
 *  Resets the mask for the specified socket.
 *
 *  Arguments:
 *      SOCKETSELECT*   rgssList            List of sockets to manipulate
 *      PSLUINT32       cSockets            Number of sockets in list
 *      SOCKET          sock                Socket to reset
 *      PSLBOOL         fResetMask          PSLTRUE if data is a mask
 *      PSLUINT32       dwData              Data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if no
 *                        match
 *
 */

PSLSTATUS _ResetSocket(SOCKETSELECT* rgssList, PSLUINT32 cSockets,
                            SOCKET sock, PSLBOOL fResetMask, PSLUINT32 dwData)
{
    PSLINT32            errSock;
    PSLUINT32           idxSocket;
    WSANETWORKEVENTS    neCurrent;
    PSLSTATUS           ps;

    /*  Validate arguments
     */

    if ((PSLNULL == rgssList) || (INVALID_SOCKET == sock))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  We need to determine the mask if we do not already have one
     */

    if (!fResetMask)
    {
        /*  Map the message to a mask entry
         */

        switch (dwData)
        {
        case PSLSOCKETMSG_ACCEPT:
            dwData = FD_ACCEPT;
            break;

        case PSLSOCKETMSG_CONNECT:
            dwData = FD_CONNECT;
            break;

        case PSLSOCKETMSG_DATA_AVAILABLE:
            dwData = FD_READ;
            break;

        case PSLSOCKETMSG_READY_TO_SEND:
            dwData = FD_WRITE;
            break;

        case PSLSOCKETMSG_OUT_OF_BAND:
            dwData = FD_OOB;
            break;

        case PSLSOCKETMSG_CLOSE:
            dwData = FD_CLOSE;
            break;

        case PSLSOCKETMSG_QOS:
            dwData = FD_QOS;
            break;

        case PSLSOCKETMSG_GROUP_QOS:
            dwData = FD_GROUP_QOS;
            break;

        case PSLSOCKETMSG_ROUTE_IFACE_CHANGE:
            dwData = FD_ROUTING_INTERFACE_CHANGE;
            break;

        case PSLSOCKETMSG_ADDRESS_LIST_CHANGE:
            dwData = FD_ADDRESS_LIST_CHANGE;
            break;

        default:
            /*  Unknown message
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }
    }

    /*  Start walking through the list of sockets looking for a match.  For
     *  performance reasons we keep the list in sorted order.
     */

    for (idxSocket = 0;
            (idxSocket < cSockets) && (sock != rgssList[idxSocket].sock);
            idxSocket++)
    {
    }

    /*  If we did not find a match then we are done
     */

    if ((idxSocket == cSockets) || (sock != rgssList[idxSocket].sock))
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Update the mask
     */

    rgssList[idxSocket].dwEventMask &= ~dwData;

    /*  Make sure that we do not have a pending event that was masked and
     *  now needs to be sent
     */

    errSock = WSAEnumNetworkEvents(rgssList[idxSocket].sock,
                            rgssList[idxSocket].hEvent,
                            &neCurrent);
    if ((SOCKET_ERROR != errSock) && (dwData & neCurrent.lNetworkEvents))
    {
        dwData &= neCurrent.lNetworkEvents;
        ps = _SendSocketEvents(dwData, &(rgssList[idxSocket].dwEventMask),
                            rgssList[idxSocket].sock,
                            rgssList[idxSocket].mqEvent,
                            rgssList[idxSocket].mpEvent,
                            rgssList[idxSocket].pvParam);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Provide some feedback
     */

    PSLTraceProgress("PSLSockets: Socket 0x%08x reset- Events: %s",
                            sock, _GetEventName(dwData));

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _SendSocketEvents
 *
 *  Sends the specified socket events to the message queue indicated.
 *
 *  Arguments:
 *      PSLUINT32       dwNetworkEvents     Network events to send
 *      PSLUINT32*      pdwEventMask        Network event mask (in/out)
 *      SOCKET          sock                Socket sending events
 *      PSLMSGQUEUE     mqEvent             Message queue to send events to
 *      PSLMSGPOOL      mpEvent             Message pool to get messages from
 *      PSLPARAM        pvParam             Param to send along with message
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _SendSocketEvents(PSLUINT32 dwNetworkEvents, PSLUINT32* pdwEventMask,
                            SOCKET sock, PSLMSGQUEUE mqEvent,
                            PSLMSGPOOL mpEvent, PSLPARAM pvParam)
{
    PSLUINT32           cbData;
    PSLINT32            errSock;
    PSLBOOL             fSentEvent = PSLFALSE;
    PSLUINT32           idxEvent;
    PSLMSGID            msgID;
    PSLMSG*             pMsg = PSLNULL;
    PSLSTATUS           ps;

    /*  Validate arguments
     */

    if ((PSLNULL == pdwEventMask) || (INVALID_SOCKET == sock) ||
        (PSLNULL == mqEvent) || (PSLNULL == mpEvent))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine which events we can send
     */

    dwNetworkEvents &= ~(*pdwEventMask);

    /*  Make sure we have work to do
     */

    if (0 == dwNetworkEvents)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Send the events- we have a predetermined order here so that we make
     *  sure connection events arrive before data available events
     */

    for (idxEvent = 0;
            idxEvent < PSLARRAYSIZE(_RgdwOrderedNetworkEvents);
            idxEvent++)
    {
        /*  We should not have a message at this point
         */

        PSLASSERT(PSLNULL == pMsg);

        /*  No need to send a message if the event is not set or if it is
         *  currently masked
         */

        if (0 == (_RgdwOrderedNetworkEvents[idxEvent] & dwNetworkEvents))
        {
            continue;
        }

        /*  Handle any event specific operations and determine what message
         *  to send
         */

        switch (_RgdwOrderedNetworkEvents[idxEvent])
        {
        case FD_ACCEPT:
            msgID = PSLSOCKETMSG_ACCEPT;
            break;

        case FD_CONNECT:
            msgID = PSLSOCKETMSG_CONNECT;
            break;

        case FD_READ:
            /*  Make sure the socket really has data available to read before
             *  the event is sent
             */

            errSock = ioctlsocket(sock, FIONREAD, (u_long*)&cbData);
            if (SOCKET_ERROR == errSock)
            {
                ps = PSLSocketErrorToPSLStatus(sock, errSock);
                goto Exit;
            }
            if (0 == cbData)
            {
                continue;
            }

            msgID = PSLSOCKETMSG_DATA_AVAILABLE;
            break;

        case FD_WRITE:
            msgID = PSLSOCKETMSG_READY_TO_SEND;
            break;

        case FD_OOB:
            msgID = PSLSOCKETMSG_OUT_OF_BAND;
            break;

        case FD_CLOSE:
            msgID = PSLSOCKETMSG_CLOSE;
            break;

        case FD_QOS:
            msgID = PSLSOCKETMSG_QOS;
            break;

        case FD_GROUP_QOS:
            msgID = PSLSOCKETMSG_GROUP_QOS;
            break;

        case FD_ROUTING_INTERFACE_CHANGE:
            msgID = PSLSOCKETMSG_ROUTE_IFACE_CHANGE;
            break;

        case FD_ADDRESS_LIST_CHANGE:
            msgID = PSLSOCKETMSG_ADDRESS_LIST_CHANGE;
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Send the event to the destination
         */

        ps = PSLMsgAlloc(mpEvent, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = msgID;
        pMsg->aParam0 = (PSLPARAM)sock;
        pMsg->aParam1 = pvParam;

        ps = PSLMsgPost(mqEvent, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = PSLNULL;

        /*  Update the event mask
         */

        *pdwEventMask |= _RgdwOrderedNetworkEvents[idxEvent];

        /*  Provide some feedback
         */

        PSLTraceProgress("PSLSockets: Socket 0x%08x- Destination: 0x%08x "
                            "Event: %s", sock, mqEvent,
                            _GetEventName(_RgdwOrderedNetworkEvents[idxEvent]));

        /*  Remember that we have sent at least one event
         */

        fSentEvent = PSLTRUE;
    }

    /*  Report success
     */

    ps = fSentEvent ? PSLSUCCESS : PSLSUCCESS_FALSE;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  _SocketSelectThread
 *
 *  Main thread that handles receiving events from WinSock and re-routing them
 *  as thread messages.
 *
 *  Arguments:
 *      PSLVOID*        pvParam             Parameter- contains signal used
 *                                            by message queue to know that
 *                                            a message is available
 *
 *  Returns:
 *      PSLUINT32       Error code (PSLSTATUS)
 *
 */

PSLUINT32 _SocketSelectThread(PSLVOID* pvParam)
{
    PSLUINT32           cEvents;
    PSLUINT32           cSockets = 0;
    PSLUINT32           dwState;
    PSLUINT32           dwWaitResult;
    PSLINT32            errSock;
    PSLBOOL             fLoop;
    PSLBOOL             fUpdateList;
    HANDLE              hMsgEvent = NULL;
    PSLHANDLE           hSignalInit = NULL;
    PSLUINT32           idxSocket;
    WSANETWORKEVENTS    neCurrent;
    PSLMSG*             pMsg = PSLNULL;
    PSLSTATUS           ps;
    SOCKETSELECT*       pssAdd;
    HANDLE              rghEvents[MAX_SOCKETS + 1];
    SOCKETSELECT        rgssSockets[MAX_SOCKETS];

    /*  Validate arguments
     */

    if (PSLNULL == pvParam)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    hMsgEvent = (HANDLE)pvParam;

    /*  Retrieve the event handle for the messages and track it in the
     *  first entry in the handle list
     */

    cEvents = 1;
    rghEvents[0] = hMsgEvent;

    /*  Assume that we are going to handle messages
     */

    fLoop = PSLTRUE;

    /*  Report that we are up and running- and make sure that we did not have
     *  a problem in the intervening time
     */

    dwState = PSLAtomicCompareExchange(&_DwSocketState,
                            SOCKETTHREADSTATE_RUNNING,
                            SOCKETTHREADSTATE_INITIALIZING);
    PSLASSERT(SOCKETTHREADSTATE_INITIALIZING == dwState);

    /*  Create and set the "initialized" signal so any waitng threads can be
     *  released
     */

    ps = PSLSignalOpen(PSLFALSE, PSLFALSE, _RgchSocketInitSignal,
                            &hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    ps = PSLSignalSet(hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure this thread gets priority so that events are forwarded as
     *  quickly as possible
     */

    ps = PSLThreadSetPriority(NULL, PSLTHREADPRIORITY_TIME_CRITICAL);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Loop and handle the events we receive
     */

    fUpdateList = PSLFALSE;
    while (fLoop)
    {
        /*  Update the event list if necessary
         */

        if (fUpdateList)
        {
            /*  Loop through and add the socket events to the list
             */

            for (cEvents = 1;
                    (cEvents <= cSockets) && (cEvents < PSLARRAYSIZE(rghEvents));
                    cEvents++)
            {
                /*  Store the event- make sure that we reserve
                 *  space for the message queue event
                 */

                rghEvents[cEvents] = rgssSockets[cEvents - 1].hEvent;
            }
            fUpdateList = PSLFALSE;
        }

        /*  Wait for events to happen- since this code is platform specific
         *  we can take advantage of waiting for multiple events
         */

        dwWaitResult = WaitForMultipleObjects(cEvents, rghEvents, FALSE,
                            INFINITE);
        switch (dwWaitResult)
        {
        case WAIT_OBJECT_0:
            /*  Retrieve and handle the next message
             */

            while (fLoop && (PSLSUCCESS == PSLMsgPeek(_MqSocket, PSLNULL)))
            {
                /*  Release any message we might have picked up
                 */

                SAFE_PSLMSGFREE(pMsg);

                /*  Retrieve the message
                 */

                ps = PSLMsgGet(_MqSocket, &pMsg);
                if (PSLSUCCESS != ps)
                {
                    PSLASSERT(PSL_SUCCEEDED(ps));
                    continue;
                }

                /*  And handle it
                 */

                switch (pMsg->msgID)
                {
                case SOCKETTHREADMSG_ADD_SOCKET:
                    /*  Extract the socket details from the message
                     *
                     *  NOTE: Because this message is sent synchronously it is
                     *  assumed that the socket select info lifetime is
                     *  managed by the caller.
                     */

                    pssAdd = (SOCKETSELECT*)pMsg->aParam0;
                    PSLASSERT(PSLNULL != pssAdd);

                    /*  Make sure we have space to hold the socket
                     */

                    if ((PSLNULL != pssAdd) &&
                        (PSLARRAYSIZE(rghEvents) > cEvents))
                    {
                        /*  Add the socket to the list
                         */

                        ps = _AddSocketToList(rgssSockets,
                            PSLARRAYSIZE(rgssSockets), &cSockets,
                            pssAdd->sock, pssAdd->mqEvent,
                            pssAdd->mpEvent, pssAdd->pvParam,
                            (SOCKETSELECT_CLOSE | pssAdd->dwEvents));

                        /*  Update the event list
                         */

                        fUpdateList = PSLTRUE;
                    }
                    else
                    {
                        ps = PSLERROR_OUT_OF_MEMORY;
                    }

                    /*  Report the status of the operation back to the waiting
                     *  thread
                     */

                    *((PSLSTATUS*)((PSLUINT32)pMsg->alParam)) = ps;
                    break;

                case SOCKETTHREADMSG_REMOVE_SOCKET:
                    /*  Remove the socket from the list- report status back to
                     *  waiting thread
                     */

                    *((PSLSTATUS*)((PSLUINT32)pMsg->alParam)) =
                            _RemoveSocketFromList(rgssSockets, &cSockets,
                                    (SOCKET)pMsg->aParam0);

                    /*  Update the event list
                     */

                    fUpdateList = PSLTRUE;
                    break;

                case SOCKETTHREADMSG_RESET_SOCKET:
                    /*  Reset the specified socket- report status back to the
                     *  waiting thread
                     */

                    *((PSLSTATUS*)((PSLUINT32)pMsg->alParam)) = _ResetSocket(
                            rgssSockets, cSockets, (SOCKET)pMsg->aParam0,
                            (PSLBOOL)pMsg->aParam1, (PSLUINT32)pMsg->aParam2);
                    break;

                case SOCKETTHREADMSG_SHUTDOWN:
                    /*  Report that it is time to shutdown
                     */

                    fLoop = PSLFALSE;
                    break;

                default:
                    /*  Unexpected message
                     */

                    PSLASSERT(PSLFALSE);
                    break;
                }
            }
            SAFE_PSLMSGFREE(pMsg);
            break;

        case WAIT_TIMEOUT:
            /*  We should never timeout- if we do just keep going
             */

            PSLASSERT(PSLFALSE);
            continue;

        case WAIT_FAILED:
            /*  If we fail to wait for some reason something is very unhappy-
             *  go ahead and fail
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;

        default:
            /*  We should only get here with a real event
             */

            PSLASSERT((WAIT_OBJECT_0 < dwWaitResult) &&
                (dwWaitResult < (WAIT_OBJECT_0 + cEvents)));

            /*  Determine which socket we care about
             */

            PSLASSERT((dwWaitResult - WAIT_OBJECT_0 - 1) < cSockets);
            idxSocket = (dwWaitResult - WAIT_OBJECT_0 - 1);

            /*  If the socket is closed skip the events
             */

            if (SOCKETSELECTSTATE_CLOSED == rgssSockets[idxSocket].dwState)
            {
                continue;
            }

            /*  Determine the events that need to be sent
             */

            errSock = WSAEnumNetworkEvents(rgssSockets[idxSocket].sock,
                            rgssSockets[idxSocket].hEvent, &neCurrent);
            PSLASSERT(SOCKET_ERROR != errSock);
            if (SOCKET_ERROR == errSock)
            {
                continue;
            }

            /*  Send the events
             */

            ps = _SendSocketEvents(neCurrent.lNetworkEvents,
                            &(rgssSockets[idxSocket].dwEventMask),
                            rgssSockets[idxSocket].sock,
                            rgssSockets[idxSocket].mqEvent,
                            rgssSockets[idxSocket].mpEvent,
                            rgssSockets[idxSocket].pvParam);
            PSLASSERT(PSL_SUCCEEDED(ps));

            /*  See if we need to handle the closed event locally
             */

            if ((FD_CLOSE & neCurrent.lNetworkEvents) &&
                (SOCKETSELECT_CLOSE & rgssSockets[idxSocket].dwEvents))
            {
                /*  Mark the socket as closed
                 */

                rgssSockets[idxSocket].dwState = SOCKETSELECTSTATE_CLOSED;
            }
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    /*  Mark the thread as closed
     */

    PSLAtomicCompareExchange(&_DwSocketState,
                            SOCKETTHREADSTATE_CLOSED,
                            SOCKETTHREADSTATE_RUNNING);

    /*  Walk through the socket list and release any events
     */

    for (idxSocket = cSockets; 0 < idxSocket; idxSocket--)
    {
        WSACloseEvent(rgssSockets[idxSocket - 1].hEvent);
    }

    /*  And check messages to make sure that we do not have any pending requests
     *  that need to be stopped
     */

    while (PSLSUCCESS == PSLMsgPeek(_MqSocket, PSLNULL))
    {
        /*  Release the existing message
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  And retrieve the next one
         */

        if (PSL_FAILED(PSLMsgGet(_MqSocket, &pMsg)))
        {
            break;
        }

        /*  If the message contains an operation request report an error
         */

        switch (pMsg->msgID)
        {
        case SOCKETTHREADMSG_ADD_SOCKET:
        case SOCKETTHREADMSG_REMOVE_SOCKET:
        case SOCKETTHREADMSG_RESET_SOCKET:
            /*  Return an error
             */

            *((PSLSTATUS*)((PSLUINT32)pMsg->alParam)) = PSLERROR_SHUTTING_DOWN;
            break;

        default:
            break;
        }
    }
    SAFE_PSLMSGFREE(pMsg);
    SAFE_PSLSIGNALCLOSE(hSignalInit);
    SAFE_CLOSEHANDLE(hMsgEvent);
    return ps;
}


/*
 *  _CheckSocketThreadState
 *
 *  Checks the status of the socket thread to make sure that it is up and
 *  running.
 *
 *  Arguments:
 *      PSLBOOL         fMustBeRunning      PSLTRUE if the thread must be
 *                                            running
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if thread is running, an error if
 *                        the thread must be running and is not,
 *                        PSLSUCCESS_NOT_AVAILABLE if the thread does not
 *                        have to be running and is not
 *
 */

PSLSTATUS _CheckSocketThreadState(PSLBOOL fMustBeRunning)
{
    PSLUINT32       dwState;
    PSLHANDLE       hSignalInit = PSLFALSE;
    PSLSTATUS       ps;

    /*  Loop here to determine what state the socket thread is in- unless
     *  the thread is active we want to loop here until we can get it into
     *  an active state.
     */

    do
    {
        /*  Determine the current state of the thread
         */

        dwState = PSLAtomicCompareExchange(&_DwSocketState,
                            SOCKETTHREADSTATE_RUNNING,
                            SOCKETTHREADSTATE_RUNNING);
        switch (dwState)
        {
        case SOCKETTHREADSTATE_RUNNING:
            /*  Thread is active
             */

            ps = PSLSUCCESS;
            break;

        case SOCKETTHREADSTATE_UNKNOWN:
            /*  The thread has never been initialized
             */

            ps = fMustBeRunning ?
                            PSLERROR_NOT_INITIALIZED : PSLSUCCESS_NOT_AVAILABLE;
            break;

        case SOCKETTHREADSTATE_CLOSING:
        case SOCKETTHREADSTATE_CLOSED:
            /*  The thread is either not running or shortly will not be-
             *  report the correct error
             */

            ps = fMustBeRunning ?
                            PSLERROR_NETWORK_FAILURE : PSLSUCCESS_NOT_AVAILABLE;
            break;

        case SOCKETTHREADSTATE_INITIALIZING:
            /*  Another thread is initializing the router- give it a chance
             *  to complete the work
             */

            PSLASSERT(PSLNULL == hSignalInit);
            ps = PSLSignalOpen(PSLFALSE, PSLFALSE, _RgchSocketInitSignal,
                            &hSignalInit);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            ps = PSLSignalWait(hSignalInit, PSLTHREAD_INFINITE);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        default:
            /*  Unknown init router state
             */

            PSLASSERT(FALSE);
            ps = PSLERROR_UNEXPECTED;
            break;
        }
    }
    while (SOCKETTHREADSTATE_INITIALIZING == dwState);

Exit:
    SAFE_PSLSIGNALCLOSE(hSignalInit);
    return ps;
}


/*
 *  PSLSocketInitialize
 *
 *  Initializes the PSLSocket environment for operation on top of WinSock
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success, PSLSUCCESS_EXISTS if already running
 *
 */

PSLSTATUS PSLSocketInitialize()
{
    PSLUINT32       dwState = SOCKETTHREADSTATE_RUNNING;
    PSLBOOL         fWinSockRunning;
    HANDLE          hThread = PSLNULL;
    HANDLE          hMsgEvent = PSLNULL;
    PSLHANDLE       hMutexInit = PSLNULL;
    PSLHANDLE       hSignalInit = PSLNULL;
    PSLMSGPOOL      mpSocket = PSLNULL;
    PSLMSGQUEUE     mqSocket = PSLNULL;
    PSLSTATUS       ps;
    PSLVOID*        pvParam = PSLNULL;
    WSADATA         wsaData;

    /*  Create the initialization mutex- since we are reference counting this
     *  object we need to make sure that we can atomically deal with the
     *  reference count operations
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchSocketInitMutex, &hMutexInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create the initialization event
     */

    ps = PSLSignalOpen(PSLFALSE, PSLTRUE, _RgchSocketInitSignal,
                    &hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Loop here to determine what state the socket thread is in- unless
     *  the thread is active we want to loop here until we can get it into
     *  an active state.
     */

    do
    {
        /*  Try and set the router to initializing state
         */

        dwState = PSLAtomicCompareExchange(&_DwSocketState,
                            SOCKETTHREADSTATE_INITIALIZING,
                            SOCKETTHREADSTATE_UNKNOWN);
        switch (dwState)
        {
        case SOCKETTHREADSTATE_RUNNING:
            /*  Thread is already active- just increment the reference count
             */

            _DwSocketRefs++;
            ps = PSLSUCCESS_EXISTS;
            goto Exit;

        case SOCKETTHREADSTATE_CLOSING:
        case SOCKETTHREADSTATE_CLOSED:
            /*  The thread is in the process of shutting down or has already-
             *  exited do not attempt to initialize the subsystem
             */

            ps = PSLERROR_SHUTTING_DOWN;
            goto Exit;

        case SOCKETTHREADSTATE_INITIALIZING:
            /*  Another thread is initializing the router- give it a chance
             *  to complete the work
             */

            PSLASSERT(PSLNULL != hSignalInit);
            ps = PSLSignalWait(hSignalInit, PSLTHREAD_INFINITE);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        case SOCKETTHREADSTATE_UNKNOWN:
            /*  We own initializing the router- nothing to do here
             */

            break;

        default:
            /*  Unknown init router state
             */

            PSLASSERT(FALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }
    }
    while (SOCKETTHREADSTATE_UNKNOWN != dwState);

    /*  Validate our invariants before starting the initialization
     */

    PSLASSERT(0 == _DwSocketRefs);
    PSLASSERT(PSLNULL == _HThreadSocket);
    PSLASSERT(PSLNULL == _MqSocket);
    PSLASSERT(PSLNULL == _MpSocket);

    /*  Make sure the signal is not set
     */

    ps = PSLSignalReset(hSignalInit);
    PSLASSERT(PSL_SUCCEEDED(ps));

    /*  Make sure that WinSock is initialized before we do anything
     */

    fWinSockRunning = PSLAtomicCompareExchange(&_FWinSockRunning,
                            PSLTRUE,
                            PSLFALSE);
    if (!fWinSockRunning)
    {
        /*  We own starting up WinSock- do it
         */

        if (NO_ERROR != WSAStartup(MAKEWORD(2, 2), &wsaData))
        {
            /*  Reset both the WinSock state and the thread state
             */

            PSLAtomicExchange(&_FWinSockRunning, PSLFALSE);

            /*  Report failure
             */

            ps = PSLERROR_NETWORK_FAILURE;
            goto Exit;
        }
    }

    /*  Retrieve the queue param for the message queue- the Windows PSL
     *  implementation always returns an event object
     */

    ps = PSLWinMsgGetQueueParam(&pvParam);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Dupicate the event so that we can give a copy to the thread as
     *  well and do not have to worry about lifetime issues.
     */

    if (!DuplicateHandle(GetCurrentProcess(), pvParam,
                            GetCurrentProcess(), &hMsgEvent,
                            DUPLICATE_SAME_ACCESS, FALSE,
                            DUPLICATE_SAME_ACCESS))
    {
        ps = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    /*  Create the message queue and message pool for the init router
     *  thread.  These pools will be used internally to handle each new
     *  socket as it is bound until it is determined what type of socket
     *  it is (command/data or event) and it can be bound to a connection
     *  specific handler.
     */

    ps = PSLMsgQueueCreate(pvParam, PSLWinMsgOnPost, PSLWinMsgOnWait,
                            PSLWinMsgOnDestroy, &mqSocket);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pvParam = PSLNULL;

    ps = PSLMsgPoolCreate(BINDTRANSPORT_MSG_POOL_SIZE, &mpSocket);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create the thread that will handle the messages for the router
     *  initialization function- note that the thread will block until
     *  the initialization goes to "running" so that we do not have to worry
     *  about contention issues with the data we have created.
     */

    ps = PSLThreadCreate(_SocketSelectThread, hMsgEvent, PSLTRUE,
                            PSLTHREAD_DEFAULT_STACK_SIZE, &hThread);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMsgEvent = NULL;

    /*  Initialize the static variables
     */

    _MqSocket = mqSocket;
    _MpSocket = mpSocket;
    _HThreadSocket = hThread;

    /*  Resume the thread so it can start executing
     */

    ps = PSLThreadResume(hThread);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        /*  Make sure the thread is terminated
         */

        PSLThreadTerminate(hThread, (PSLUINT32)PSLERROR_UNEXPECTED);
        goto Exit;
    }

    /*  Thread now has ownership of the static objects- forget them here
     */

    mqSocket = PSLNULL;
    mpSocket = PSLNULL;
    hThread = PSLNULL;

    /*  Increment the reference count
     */

    _DwSocketRefs++;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);

    /*  If initialization failed for any reason we need to make sure that we
     *  set the init signal so that anyone waiting for the initialization to
     *  complete can respond
     */

    if (PSL_FAILED(ps) && (SOCKETTHREADSTATE_UNKNOWN == dwState))
    {
        /*  We should be initializing at this point
         */

        PSLASSERT(SOCKETTHREADSTATE_INITIALIZING == dwState);

        /*  Reset the state back to unknown if necessary
         */

        PSLAtomicExchange(&_DwSocketState, SOCKETTHREADSTATE_UNKNOWN);

        /*  And wake up anyone waiting
         */

        if (PSLNULL != hSignalInit)
        {
            PSLSignalSet(hSignalInit);
        }
    }
    SAFE_PSLTHREADCLOSE(hThread);
    SAFE_PSLMSGPOOLDESTROY(mpSocket);
    SAFE_PSLMSGQUEUEDESTROY(mqSocket);
    SAFE_PSLWINMSGRELEASEQUEUEPARAM(pvParam);
    SAFE_CLOSEHANDLE(hMsgEvent);
    SAFE_PSLSIGNALCLOSE(hSignalInit);
    return ps;
}


/*
 *  PSLSocketUninitialize
 *
 *  Called to close down the PSLSocket support
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE if other clients
 *                        are still active
 *
 */

PSLSTATUS PSLSocketUninitialize()
{
    PSLUINT32       dwResult;
    PSLUINT32       dwState;
    PSLHANDLE       hMutex = PSLNULL;
    PSLSTATUS       ps;
    PSLMSG*         pMsg = PSLNULL;

    /*  Acquire a mutex to make sure that we do not run into problems with the
     *  reference counting
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchSocketInitMutex, &hMutex);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Give the thread a chance to complete initialization
     */

    ps = _CheckSocketThreadState(PSLFALSE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we are actually running
     */

    if (0 == _DwSocketRefs)
    {
        ps = PSLSUCCESS_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Decrement the reference count first- no sense trying to shutdown
     *  something that is still in use
     */

    PSLASSERT(0 < _DwSocketRefs);
    _DwSocketRefs--;
    if (0 < _DwSocketRefs)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Make sure that we own the shutdown process
     */

    dwState = PSLAtomicCompareExchange(&_DwSocketState,
                        SOCKETTHREADSTATE_CLOSING,
                        SOCKETTHREADSTATE_RUNNING);
    switch (dwState)
    {
    case SOCKETTHREADSTATE_RUNNING:
    case SOCKETTHREADSTATE_CLOSED:
        /*  No work to do here- we are responsible for shutting down the
         *  bind router thread
         */

        break;

    case SOCKETTHREADSTATE_UNKNOWN:
    case SOCKETTHREADSTATE_INITIALIZING:
    case SOCKETTHREADSTATE_CLOSING:
        /*  We should only ever initialize once and shutdown once- if
         *  we get here this is an error
         *
         *  FALL THROUGH INTENTIONAL
         */

    default:
        /*  Should never get here
         */

        PSLASSERT(FALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  If the thread has not already exited then we need to tell it to
     *  shut down
     */

    if (SOCKETTHREADSTATE_CLOSED != dwState)
    {
        /*  Notify the thread that it needs to shutdown
         */

        ps = PSLMsgAlloc(_MpSocket, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = SOCKETTHREADMSG_SHUTDOWN;

        /*  Post the message and transfer ownership of the message to the queue
         */

        ps = PSLMsgPost(_MqSocket, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = PSLNULL;
    }

    /*  If we have a thread then close it
     */

    if (PSLNULL != _HThreadSocket)
    {
        /*  Wait for the thread to shutdown
         */

        ps = PSLThreadGetResult(_HThreadSocket, PSLTHREAD_INFINITE, &dwResult);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        SAFE_PSLTHREADCLOSE(_HThreadSocket);
    }

    /*  Release the message queue
     */

    SAFE_PSLMSGQUEUEDESTROY(_MqSocket);
    SAFE_PSLMSGPOOLDESTROY(_MpSocket);

    /*  Reset the state of the router to unknown
     */

    dwState = PSLAtomicExchange(&_DwSocketState,
                            SOCKETTHREADSTATE_UNKNOWN);
    PSLASSERT(SOCKETTHREADSTATE_CLOSING == dwState);

    /*  Release WinSock
     */

    PSLASSERT(_FWinSockRunning);
    WSACleanup();
    PSLAtomicExchange(&_FWinSockRunning, PSLFALSE);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutex);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  PSLSocketMsgToString
 *
 *  Converts a socket message to a string for debugging purposes
 *
 *  Arguments:
 *      PSLMSGID        msgID               Message ID to convert
 *
 *  Returns:
 *      PSLCSTR         Resulting string
 *
 */

PSLCSTR PSLSocketMsgToString(PSLMSGID msgID)
{
    PSLCSTR         szResult;

    /*  Determine the correct string
     */

    switch (msgID)
    {
    case PSLSOCKETMSG_DATA_AVAILABLE:
        szResult = _RgchMsgDataAvailable;
        break;

    case PSLSOCKETMSG_READY_TO_SEND:
        szResult = _RgchMsgReadyToSend;
        break;

    case PSLSOCKETMSG_OUT_OF_BAND:
        szResult = _RgchMsgOOB;
        break;

    case PSLSOCKETMSG_ACCEPT:
        szResult = _RgchMsgAccept;
        break;

    case PSLSOCKETMSG_CONNECT:
        szResult = _RgchMsgConnect;
        break;

    case PSLSOCKETMSG_CLOSE:
        szResult = _RgchMsgClose;
        break;

    case PSLSOCKETMSG_QOS:
        szResult = _RgchMsgQOS;
        break;

    case PSLSOCKETMSG_GROUP_QOS:
        szResult = _RgchMsgGroupQOS;
        break;

    case PSLSOCKETMSG_ROUTE_IFACE_CHANGE:
        szResult = _RgchMsgRIChange;
        break;

    case PSLSOCKETMSG_ADDRESS_LIST_CHANGE:
        szResult = _RgchMsgALChange;
        break;

    case PSLSOCKETMSG_UNKNOWN:
    default:
        szResult = _RgchMsgUnknown;
        break;
    }
    return szResult;
}


/*
 *  PSLSocketErrorToPSLStatus
 *
 *  Maps socket errors to PSL status errors
 *
 *  Arguments:
 *      SOCKET          sock                Socket for which the error occurred
 *      PSLINT32        errSock             Error code returned from the
 *                                            socket call
 *
 *  Returns:
 *      PSLSTATUS
 *
 */

PSLSTATUS PSLSocketErrorToPSLStatus(SOCKET socket, PSLINT32 errSock)
{
    PSLINT32        cbResult;
    PSLINT32        errWSA;
    PSLSTATUS       ps;

    /*  Make sure we have an error to deal with
     */

    if (SOCKET_ERROR != errSock)
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Attempt to retrieve the error directly from the socket rather than
     *  use WSAGetLastError because it may not return the error associated
     *  with the socket indicated
     */

    errWSA = WSAGetLastError();
    cbResult = sizeof(errSock);
    if ((SOCKET_ERROR == getsockopt(socket, SOL_SOCKET, SO_ERROR,
                            (char*)&errSock, (int*)&cbResult)) ||
        (ERROR_SUCCESS == errSock))
    {
        errSock = errWSA;
    }

    /*  Determine the correct PSLSTATUS
     */
    switch (errSock)
    {
    case WSANOTINITIALISED:
    case WSAEINVAL:
        ps = PSLERROR_NOT_INITIALIZED;
        break;

    case WSAENETDOWN:
        ps = PSLERROR_NETWORK_FAILURE;
        break;

    case WSAEFAULT:
        ps = PSLERROR_INVALID_BUFFER;
        break;

    case WSAENOTCONN:
        ps = PSLERROR_CONNECTION_NOT_FOUND;
        break;

    case WSAEINTR:
    case WSAENETRESET:
    case WSAESHUTDOWN:
    case WSAECONNABORTED:
    case WSAECONNRESET:
    case WSAETIMEDOUT:
        ps = PSLERROR_CONNECTION_CLOSED;
        break;

    case WSAEINPROGRESS:
        ps = PSLERROR_IN_USE;
        break;

    case WSAENOTSOCK:
        ps = PSLERROR_INVALID_PARAMETER;
        break;

    case WSAEOPNOTSUPP:
        ps = PSLERROR_NOT_AVAILABLE;
        break;

    case WSAEWOULDBLOCK:
        ps = PSLSUCCESS_WOULD_BLOCK;
        break;

    case WSAEMSGSIZE:
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        break;

    default:
        /*  Report a general network failure
         *
         *  NOTE: We do not special case the success case because we
         *  were sent here with an error code so we want to make sure
         *  that we actually return an error code
         */

        ps = PSLERROR_NETWORK_FAILURE;
        break;
    }

Exit:
    return ps;
}


/*
 *  PSLSocketMsgSelect
 *
 *  Registers the specified socket to send messages to the queue specified
 *  when the network events specified by select flags occur.
 *
 *  Arguments:
 *      SOCKET          sockSource          Socket to register
 *      PSLMSGQUEUE     mqDest              Event destination
 *      PSLMSGPOOL      mpDest              Message pool for events
 *      PSLPARAM        pvParam             Parameter to include in message
 *      PSLUINT32       dwSelectFlags       Events being requested, 0 to stop
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLSocketMsgSelect(SOCKET sockSource, PSLMSGQUEUE mqDest,
                            PSLMSGPOOL mpDest, PSLPARAM pvParam,
                            PSLUINT32 dwSelectFlags)
{
    PSLMSG*             pMsg = PSLNULL;
    PSLSTATUS           ps;
    PSLSTATUS           psMsg = { 0 };
    SOCKETSELECT        ssAdd;

    /*  Validate arguments
     */

    if ((INVALID_SOCKET == sockSource) || (PSLNULL == mqDest) ||
        (PSLNULL == mpDest) || (0 == dwSelectFlags))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we have a thread capable of handling the events
     */

    ps = _CheckSocketThreadState(PSLTRUE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Fill out the data structure- since we are sending this message
     *  synchronously we can just use stack space to hold the data rather
     *  than allocating
     */

    ssAdd.sock = sockSource;
    ssAdd.mqEvent = mqDest;
    ssAdd.mpEvent = mpDest;
    ssAdd.pvParam = pvParam;
    ssAdd.dwEvents = dwSelectFlags;

    /*  Create a message to send to the worker thread and initialize
     *  the arguments.
     */

    ps = PSLMsgAlloc(_MpSocket, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg->msgID = SOCKETTHREADMSG_ADD_SOCKET;
    pMsg->aParam0 = (PSLPARAM)&ssAdd;
    pMsg->alParam = (PSLPARAM)&psMsg;

    /*  Send the message to the thread synchronously so that we can report
     *  true success or failure
     */

    ps = PSLMsgSend(_MqSocket, pMsg, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg = PSLNULL;

    /*  Extract the result from the message handling and return it
     */

    ps = psMsg;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  PSLSocketMsgSelectClose
 *
 *  Unregisters the specified socket from recieving asynchronous notifications
 *  via the message queue.
 *
 *  Arguments:
 *      SOCKET          sockSource          Socket to unregister
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLSocketMsgSelectClose(SOCKET sockSource)
{
    PSLMSG*             pMsg = PSLNULL;
    PSLSTATUS           ps;
    PSLSTATUS           psMsg;

    /*  Validate arguments
     */

    if (INVALID_SOCKET == sockSource)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we are initialized
     */

    ps = _CheckSocketThreadState(PSLTRUE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create a message to send to the worker thread and initialize
     *  the arguments.
     */

    ps = PSLMsgAlloc(_MpSocket, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg->msgID = SOCKETTHREADMSG_REMOVE_SOCKET;
    pMsg->aParam0 = (PSLPARAM)sockSource;
    pMsg->alParam = (PSLPARAM)&psMsg;

    /*  Send the message to the thread synchronously so that we can report
     *  true success or failure
     */

    ps = PSLMsgSend(_MqSocket, pMsg, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg = PSLNULL;

    /*  Extract the result from the message handling and return it
     */

    ps = psMsg;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  PSLSocketMsgReset
 *
 *  Resets the event mask so that the next event that is processed for this
 *  socket will be sent.
 *
 *  Arguments:
 *      SOCKET          sockSource          Socket to reset mask for
 *      PSLBOOL         fResetMask          PSLTRUE if data contains a mask
 *                                            instead of a message ID
 *      PSLUINT32       dwData              Data to send
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSLSocketMsgReset(SOCKET sockSource, PSLBOOL fResetMask,
                            PSLUINT32 dwData)
{
    PSLMSG*             pMsg = PSLNULL;
    PSLSTATUS           ps;
    PSLSTATUS           psMsg;

    /*  Validate arguments
     */

    if ((INVALID_SOCKET == sockSource) || (0 == dwData))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we have a thread capable of handling the events
     */

    ps = _CheckSocketThreadState(PSLTRUE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create a message to send to the worker thread and initialize
     *  the arguments.
     */

    ps = PSLMsgAlloc(_MpSocket, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg->msgID = SOCKETTHREADMSG_RESET_SOCKET;
    pMsg->aParam0 = (PSLPARAM)sockSource;
    pMsg->aParam1 = (PSLPARAM)fResetMask;
    pMsg->aParam2 = (PSLPARAM)dwData;
    pMsg->alParam = (PSLPARAM)&psMsg;

    /*  Send the message to the thread synchronously so that we can report
     *  true success or failure
     */

    ps = PSLMsgSend(_MqSocket, pMsg, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg = PSLNULL;

    /*  Extract the result from the message handling and return it
     */

    ps = psMsg;

Exit:
    return ps;
}


/*
 *  PSLSocketClose
 *
 *  Closes the socket after making sure that it is no longer registered
 *  with PSLSocketMsgSelect
 *
 *  Arguments:
 *      SOCKET          sock                Socket to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

int FAR PASCAL PSLSocketClose(SOCKET sock)
{
    PSLINT32        errSock;
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if (INVALID_SOCKET == sock)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Remove socket registrations with PSLSocketMsgSelect
     */

    ps = PSLSocketMsgSelectClose(sock);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And close the socket
     */

    errSock = closesocket(sock);
    if (SOCKET_ERROR == errSock)
    {
        ps = PSLSocketErrorToPSLStatus(sock, errSock);
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}

