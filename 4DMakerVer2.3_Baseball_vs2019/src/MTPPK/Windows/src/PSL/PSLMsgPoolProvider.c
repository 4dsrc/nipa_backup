/*
 *  PSLMsgQueueProvider.c
 *
 *  Contains the message queue provider implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSL.h"
#include "PSLMsgQueueUtil.h"
#include "PSLMsgPoolUtil.h"
#include "PSLMsgPoolProvider.h"

PSLSTATUS PSLMsgPoolProviderGet(PSLRESPROVIDERINFO* pProviderInfo,
                                 PSLUINT32 dwHint, PSLVOID** ppvRes,
                                 PSLUINT32* pdwSize)
{
    PSLSTATUS status;
    PSLMSGPOOLPROVIDERINFO* pmpInfo = PSLNULL;
    PSLMSGPOOLOBJ* pmpObj = PSLNULL;
    PSLBOOL fAcquired = PSLFALSE;
    PSLUINT32 dwSize = 0;

    if (PSLNULL != ppvRes)
    {
        *ppvRes = PSLNULL;
    }

    if (PSLNULL != pdwSize)
    {
        *pdwSize = 0;
    }

    if (PSLNULL == pProviderInfo || 0 == dwHint ||
        PSLNULL == ppvRes || PSLNULL == pdwSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmpInfo = (PSLMSGPOOLPROVIDERINFO*)pProviderInfo;

    status = PSLMutexAcquire(pmpInfo->baseInfo.hProviderLock,
                                PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    fAcquired = PSLTRUE;

    dwSize = sizeof(PSLMSGPOOLOBJ)+(dwHint * sizeof(PSLMSGNODE));

    pmpObj = (PSLMSGPOOLOBJ*)PSLMemAlloc(PSLMEM_PTR, dwSize);
    if (PSLNULL == pmpObj)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    *ppvRes = pmpObj;
    *pdwSize = dwSize;

Exit:
    if (PSLNULL != pmpObj)
    {
        PSLMemFree(pmpObj);
    }

    if ((PSLTRUE == fAcquired) && (PSLNULL != pmpInfo))
    {
        PSLMutexRelease(pmpInfo->baseInfo.hProviderLock);
    }
    return status;
}

PSLSTATUS PSLMsgPoolProviderRelease(PSLRESPROVIDERINFO* pProviderInfo,
                                     PSLVOID* pvRes)
{
    PSLSTATUS status;
    PSLMSGPOOLPROVIDERINFO* pmpInfo = PSLNULL;
    PSLBOOL fAcquired = PSLFALSE;

    if (PSLNULL == pProviderInfo || PSLNULL == pvRes)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmpInfo = (PSLMSGPOOLPROVIDERINFO*)pProviderInfo;

    status = PSLMutexAcquire(pmpInfo->baseInfo.hProviderLock,
                                PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    fAcquired = PSLTRUE;

    PSLMemFree((PSLMSGPOOLOBJ*)pvRes);

Exit:
    if ((PSLTRUE == fAcquired) && (PSLNULL != pmpInfo))
    {
        PSLMutexRelease(pmpInfo->baseInfo.hProviderLock);
    }
    return status;
}

PSLSTATUS PSLMsgPoolProviderCreate(
                                PSLMSGPOOLPROVIDERINFO** ppmpInfo)
{
    PSLSTATUS status;
    PSLMSGPOOLPROVIDERINFO* pmpInfo = PSLNULL;

    if (PSLNULL != ppmpInfo)
    {
        *ppmpInfo = PSLNULL;
    }

    if (PSLNULL == ppmpInfo)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmpInfo = PSLMemAlloc(PSLMEM_PTR,
                            sizeof(PSLMSGPOOLPROVIDERINFO));

    if (PSLNULL == pmpInfo)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = PSLMutexOpen(PSLFALSE, PSLNULL,
                            &pmpInfo->baseInfo.hProviderLock);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pmpInfo->baseInfo.pfnProviderGet = PSLMsgPoolProviderGet;

    pmpInfo->baseInfo.pfnProviderInit = PSLNULL;

    pmpInfo->baseInfo.pfnProviderRelease = PSLMsgPoolProviderRelease;

    pmpInfo->baseInfo.pfnProviderUninit = PSLNULL;

    *ppmpInfo = pmpInfo;
    pmpInfo = PSLNULL;

Exit:
    if (PSLNULL != pmpInfo)
    {
        PSLMemFree(pmpInfo);
    }
    return status;
}

PSLSTATUS PSLMsgPoolProviderDestroy(
                            PSLMSGPOOLPROVIDERINFO* pmpInfo)
{
    PSLSTATUS status;

    if (PSLNULL == pmpInfo)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLMutexClose(pmpInfo->baseInfo.hProviderLock);

    PSLMemFree(pmpInfo);

    status = PSLSUCCESS;

Exit:
    return status;
}
