/*
 *  PSLLookupTableProvider.c
 *
 *  Contains the message queue provider implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLPrecomp.h"
#include "PSLResourceManager.h"
#include "PSLLookupTableProvider.h"

PSLSTATUS PSLLookupTableProviderInit(PSLRESPROVIDERINFO* pProviderInfo,
                                  PSLVOID* pvParam1, PSLVOID* pvParam2)
{
    PSLSTATUS status;
    PSLLOOKUPTABLEOBJ* pltObj = PSLNULL;
    PSLLOOKUPTABLEPROVIDERINFO* pltInfo;

    if (PSLNULL == pProviderInfo || PSLNULL == pvParam1 ||
        PSLNULL == pvParam2)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pltInfo = (PSLLOOKUPTABLEPROVIDERINFO*)pProviderInfo;

    PSLASSERT(0 != pltInfo->cdwItems);

    pltObj = (PSLLOOKUPTABLEOBJ*)PSLMemAlloc(PSLMEM_PTR,
                        sizeof(PSLLOOKUPTABLEOBJ) +
                        (sizeof(PSLLOOKUPRECORD) * pltInfo->cdwItems));
    if (PSLNULL == pltObj)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    pltInfo->pltObj = pltObj;
    pltObj = PSLNULL;

    status = PSLSUCCESS;

Exit:
    if (PSLNULL != pltObj)
    {
        PSLMemFree(pltObj);
    }
    return status;
}

PSLSTATUS PSLLookupTableProviderUninit(PSLRESPROVIDERINFO* pProviderInfo)
{
    PSLSTATUS status;
    PSLLOOKUPTABLEPROVIDERINFO* pltInfo;

    if (PSLNULL == pProviderInfo)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pltInfo = (PSLLOOKUPTABLEPROVIDERINFO*)pProviderInfo;

    PSLMemFree(pltInfo->pltObj);

    status = PSLSUCCESS;

Exit:
    return status;
}

PSLSTATUS PSLLookupTableProviderGet(PSLRESPROVIDERINFO* pProviderInfo,
                                 PSLUINT32 dwHint, PSLVOID** ppvRes,
                                 PSLUINT32* pdwSize)
{
    PSLSTATUS status;
    PSLLOOKUPTABLEPROVIDERINFO* pltInfo = PSLNULL;
    PSLBOOL fAcquired = PSLFALSE;

    /*Ignore dwHint parameter*/
    dwHint = 0;

    if (PSLNULL != ppvRes)
    {
        *ppvRes = PSLNULL;
    }

    if (PSLNULL != pdwSize)
    {
        *pdwSize = 0;
    }

    if (PSLNULL == pProviderInfo || PSLNULL == ppvRes ||
        PSLNULL == pdwSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pltInfo = (PSLLOOKUPTABLEPROVIDERINFO*)pProviderInfo;

    status = PSLMutexAcquire(pltInfo->baseInfo.hProviderLock,
                                PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    fAcquired = PSLTRUE;

    *ppvRes = pltInfo->pltObj;
    *pdwSize = sizeof(PSLLOOKUPTABLEOBJ) +
                (sizeof(PSLLOOKUPRECORD) * pltInfo->cdwItems);

Exit:

    if ((PSLTRUE == fAcquired) && (PSLNULL != pltInfo))
    {
        PSLMutexRelease(pltInfo->baseInfo.hProviderLock);
    }
    return status;
}

PSLSTATUS PSLLookupTableProviderCreate(
                            PSLUINT32 cdwItems,
                            PSLLOOKUPTABLEPROVIDERINFO** ppltInfo)
{
    PSLSTATUS status;
    PSLLOOKUPTABLEPROVIDERINFO* pltInfo = PSLNULL;

    if (PSLNULL != ppltInfo)
    {
        *ppltInfo = PSLNULL;
    }

    if (PSLNULL == ppltInfo)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pltInfo = PSLMemAlloc(PSLMEM_PTR,
                            sizeof(PSLLOOKUPTABLEPROVIDERINFO));

    if (PSLNULL == pltInfo)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = PSLMutexOpen(PSLFALSE, PSLNULL,
                            &pltInfo->baseInfo.hProviderLock);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pltInfo->baseInfo.pfnProviderGet = PSLLookupTableProviderGet;

    pltInfo->baseInfo.pfnProviderInit = PSLLookupTableProviderInit;

    pltInfo->baseInfo.pfnProviderRelease = PSLNULL;

    pltInfo->baseInfo.pfnProviderUninit = PSLLookupTableProviderUninit;

    pltInfo->cdwItems = cdwItems;

    *ppltInfo = pltInfo;
    pltInfo = PSLNULL;

Exit:
    if (PSLNULL != pltInfo)
    {
        PSLMemFree(pltInfo);
    }
    return status;
}

PSLSTATUS PSLLookupTableProviderDestroy(
                            PSLLOOKUPTABLEPROVIDERINFO* pltInfo)
{
    PSLSTATUS status;

    if (PSLNULL == pltInfo)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLMutexClose(pltInfo->baseInfo.hProviderLock);

    PSLMemFree(pltInfo);

    status = PSLSUCCESS;

Exit:
    return status;
}

