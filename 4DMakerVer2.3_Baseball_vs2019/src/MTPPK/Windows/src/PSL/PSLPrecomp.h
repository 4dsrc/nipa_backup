/*
 *  PSLPrecomp.h
 *
 *  Precompiled header for the PSL implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _PSLPRECOMP_H_
#define _PSLPRECOMP_H_

#include "PSLWindows.h"
#include "PSL.h"

#endif  /* !_PSLPRECOMP_H_ */

