/*
 *  PSLDebug.cpp
 *
 *  Contains Windows implementation of the PSLDebug utilities
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLPrecomp.h"

static void _PSLDebugPrintf(LPCSTR szFormat, va_list vargs);

#ifdef PSL_ASSERTS

static DWORD WINAPI _PSLAssertThread(LPVOID pvParam);

static PSL_ASSERT_MODE      _NAssertMode = PSLAM_MESSAGE_BOX;

typedef struct
{
    WCHAR       rgchText[1024];
    int         nResult;
} AssertInfo;


/*
 *  _PSLAssertThread
 *
 *  Assert implementation
 *
 *  Arguments:
 *      LPVOID          pvParam         Assert information
 *
 *  Returns:
 *      DWORD
 *
 */

DWORD WINAPI _PSLAssertThread(LPVOID pvParam)
{
    if( NULL != pvParam )
    {
        AssertInfo *pai = (AssertInfo *)pvParam;

        // Display the string

        pai->nResult = MessageBox(NULL, pai->rgchText, L"PSL Assert Failed",
                        MB_ICONEXCLAMATION | MB_OKCANCEL | MB_SETFOREGROUND);
    }

    return 0;
}


/*
 *  PSLAssertFailed
 *
 *  Assert implementation
 *
 *  Arguments:
 *      PSLCSTR         szExp           Expression evaluated
 *      PSLCSTR         szFile          File name where assert failed
 *      PSLINT32        nLine           Line where assert failed
 *
 *  Returns:
 *      BOOL    TRUE if the caller should execute a breakpoint
 *
 */

PSLBOOL PSLAssertFailed(PSLCSTR szExp, PSLCSTR szFile, PSLINT32 nLine)
{
    AssertInfo      ai = {0};
    BOOL            fResult;
    DWORD           dwThreadID = 0;
    HANDLE          hThread;

    // If we are told we can ignore asserts do not work

    if (PSLAM_IGNORE == _NAssertMode)
    {
        fResult = FALSE;
        goto Exit;

    }

    // Prepare output

    StringCchPrintf(ai.rgchText, PSLARRAYSIZE(ai.rgchText),
                            L"Expression: %S\n\tFile: %S\n\tLine: %d\n",
                            szExp, szFile, nLine);

    // Dump it to the debugger

    OutputDebugString(L"PSL Assert Failed:\n");
    OutputDebugString(ai.rgchText);

    // If we are supposed to halt then we do not want to show the message box

    if ((PSLAM_HALT == _NAssertMode) || (PSLAM_MESSAGE_BOX != _NAssertMode))
    {
        fResult = TRUE;
        goto Exit;
    }

    // Create a blocking message box

    hThread = CreateThread(NULL, 0, _PSLAssertThread, &ai, 0, &dwThreadID);
    if( NULL != hThread )
    {
        WaitForSingleObject( hThread, INFINITE );
        CloseHandle( hThread );
    }
    else
    {
        // Go ahead and just show the message

        _PSLAssertThread( &ai );
    }

    // Return whether or not the user hit cancel

    fResult = (IDCANCEL == ai.nResult);

Exit:
    return fResult;
}


/*
 *  SetPSLAssertMode
 *
 *  Set the assert mode
 *
 *  Arguments:
 *      PSL_ASSERT_MODE nMode               New mode
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID SetPSLAssertMode(PSL_ASSERT_MODE nMode)
{
    // Set the new priority

    _NAssertMode = nMode;
    return;
}


/*
 *  GetPSLAssertMode
 *
 *  Return the current trace level
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSL_ASSERT_MODE     The current assert mode
 *
 */

PSL_ASSERT_MODE GetPSLAssertMode()
{
    return _NAssertMode;
}

#endif  // PSL_ASSERTS

#ifdef PSL_TRACE

static PSL_TRACE_PRIORITY   _NTracePriority = PSLTP_LOW;

/*
 *  SetPSLTraceLevel
 *
 *  Set the trace level
 *
 *  Arguments:
 *      PSL_TRACE_PRIORITY
 *                      nPriority           New priority
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID SetPSLTraceLevel(PSL_TRACE_PRIORITY nPriority)
{
    // Set the new priority

    _NTracePriority = nPriority;
    return;
}


/*
 *  GetPSLTraceLevel
 *
 *  Return the current trace level
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSL_TRACE_PRIORITY   Returns the current priority
 *
 */

PSL_TRACE_PRIORITY GetPSLTraceLevel()
{
    return _NTracePriority;
}


/*
 *  PSLTrace
 *
 *  Trace implementation
 *
 *  Arguments:
 *      PSL_TRACE_PRIORITY
 *                      nPriority       Priority for trace
 *      PSLCSTR         szFormat        printf style format string
 *      ...             ...             printf style variables
 *
 *  Returns:
 *      Nothing
 */

PSLVOID PSLTrace(PSL_TRACE_PRIORITY nPriority, PSLCSTR szFormat, ...)
{
    va_list         vargs;

    // Check priority

    if (nPriority < _NTracePriority)
    {
        goto Exit;
    }

    // Locate the variable array list

    va_start(vargs, szFormat);

    // Format the trace output

    _PSLDebugPrintf(szFormat, vargs);

Exit:
    return;
}


/*
 *  PSLTraceDetail
 *
 *  Dumps out trace information if the trace level is set to LOWEST
 *
 *  Arguments:
 *      PSLCSTR         szFormat        printf format string
 *      ...             ...             printf style variables
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLTraceDetail(PSLCSTR szFormat, ...)
{
    va_list         vargs;

    // Detail only gets emitted if we are at the lowest trace priority

    if (PSLTP_LOWEST < _NTracePriority)
    {
        goto Exit;
    }

    // Locate the variable array list

    va_start(vargs, szFormat);

    // Format the trace output

    _PSLDebugPrintf(szFormat, vargs);

Exit:
    return;
}


/*
 *  PSLTraceProgress
 *
 *  Dumps out trace information if the trace level is set to LOW
 *
 *  Arguments:
 *      PSLCSTR         szFormat        printf format string
 *      ...             ...             printf style variables
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLTraceProgress(PSLCSTR szFormat, ...)
{
    va_list         vargs;

    // Progress only gets emitted if we are at the lowest trace priority

    if (PSLTP_LOW < _NTracePriority)
    {
        goto Exit;
    }

    // Locate the variable array list

    va_start(vargs, szFormat);

    // Format the trace output

    _PSLDebugPrintf(szFormat, vargs);

Exit:
    return;
}


/*
 *  PSLTraceWarning
 *
 *  Dumps out trace information if the trace level is set to at least LOW
 *
 *  Arguments:
 *      PSLCSTR         szFormat        printf format string
 *      ...             ...             printf style variables
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLTraceWarning(PSLCSTR szFormat, ...)
{
    va_list         vargs;

    // Warning only gets emitted if we are at the low trace priority

    if (PSLTP_NORMAL < _NTracePriority)
    {
        goto Exit;
    }

    // Locate the variable array list

    va_start(vargs, szFormat);

    // Format the trace output

    _PSLDebugPrintf(szFormat, vargs);

Exit:
    return;
}


/*
 *  PSLTraceError
 *
 *  Dumps out trace information if the trace level is set to NORMAL
 *
 *  Arguments:
 *      PSLCSTR         szFormat        printf format string
 *      ...             ...             printf style variables
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLTraceError(PSLCSTR szFormat, ...)
{
    va_list         vargs;

    // Errors only gets emitted if we are at the normal trace priority

    if (PSLTP_HIGH < _NTracePriority)
    {
        goto Exit;
    }

    // Locate the variable array list

    va_start(vargs, szFormat);

    // Format the trace output

    _PSLDebugPrintf(szFormat, vargs);

Exit:
    return;
}

#endif  // PSL_ASSERTS


/*
 *  _PSLDebugPrintf
 *
 *  Internal helper function to output variable argument list to debugger
 *
 *  Arguments:
 *      LPCSTR          szFormat        printf format string
 *      va_list         vargs           Variable argument list
 *
 *  Returns:
 *      Nothing
 *
 */

void _PSLDebugPrintf(LPCSTR szFormat, va_list vargs)
{
    DWORD           cwchTemp;
    CHAR            rgchTemp[1024];
    WCHAR           rgwchTemp[1024];

    // Try to fill the output- note that we will let it truncate here
    //  since some output is better than none

    StringCchVPrintfA(rgchTemp, PSLARRAYSIZE(rgchTemp), szFormat, vargs);

    // Do the real conversion- and remember that MultiByteToWideChar is
    //  not guaranteed to NULL terminate

    cwchTemp = MultiByteToWideChar(CP_ACP, 0, rgchTemp, -1, rgwchTemp,
                            (PSLARRAYSIZE(rgwchTemp) - 1));
    rgwchTemp[cwchTemp] = L'\0';

    StringCchCatW(&rgwchTemp[cwchTemp-1], cwchTemp, L"\r\n\0");

	// Emit the string

    OutputDebugString(rgwchTemp);
    return;
}


/*
 *  PSLDebugPrintfA
 *
 *  Helper routine to make printing out debug information more efficient
 *
 *  Arguments:
 *      PSLCSTR         szFormat        printf style format string
 *      ...             ...             printf style variables
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLDebugPrintf(PSLCSTR szFormat, ...)
{
    va_list         vargs;

    // Locate the variable array list

    va_start(vargs, szFormat);

    // Format the trace output

    _PSLDebugPrintf(szFormat, vargs);
    return;
}


/*
 *  PSLDebugPrintfW
 *
 *  Helper routine to make printing out debug information more efficient
 *
 *  Arguments:
 *      PSLCWSTR        szFormat        printf style format string
 *      ...             ...             printf style variables
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSLDebugPrintfW(PSLCWSTR szFormat, ...)
{
    WCHAR           rgwchTemp[1024];
    va_list         vargs;

    // Locate the variable array list

    va_start(vargs, szFormat);

    // Safely fill the output buffer- note that we let it truncate here because
    //  some output is better than no output

    StringCchVPrintfW(rgwchTemp, PSLARRAYSIZE(rgwchTemp), szFormat, vargs);

    // Emit it- StringCchVPrintf guarantees termination so this is safe

    OutputDebugString(rgwchTemp);
    return;
}

