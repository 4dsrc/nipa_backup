/*
 *  PSLWinMsgQueue.c
 *
 *  Contains the Windows PSL message queue implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSLWindows.h"
#include "PSL.h"
#include "PSLWinMsgQueue.h"

PSLSTATUS PSL_API PSLWinMsgGetQueueParam(PSLVOID** ppvResult)
{
    PSLSTATUS status;
    HANDLE    hSignal = NULL;

    if (PSLNULL != ppvResult)
    {
        *ppvResult = PSLNULL;
    }

    if (PSLNULL == ppvResult)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    hSignal = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (NULL == hSignal)
    {
        status = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

    *ppvResult = hSignal;
    hSignal = NULL;

    status = PSLSUCCESS;

Exit:
    SAFE_PSLWINMSGRELEASEQUEUEPARAM(hSignal)
    return status;
}

PSLSTATUS PSL_API PSLWinMsgOnDestroy(PSLVOID* pvResult)
{
    PSLSTATUS status;

    if (PSLNULL == pvResult)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = CloseHandle((HANDLE)pvResult) ?
                            PSLSUCCESS : PSLERROR_PLATFORM_FAILURE;
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}

/*
 *
 *  This is the implementation of OnPost fucntion
 *  in a multi threaded implementation of MTP system.
 *  It will signal the object that was passed in during create
 *
 */
PSLSTATUS PSL_API PSLWinMsgOnPost(PSLVOID* pvParam)
{
    PSLSTATUS status;

    if (PSLNULL == pvParam)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = SetEvent((HANDLE)pvParam) ?
                            PSLSUCCESS : PSLERROR_PLATFORM_FAILURE;
    PSLASSERT(PSL_SUCCEEDED(status));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}

/*
 *
 *  This is the implementation of OnWait function
 *  for a multi threaded implementation of MTP system.
 *  It will wait on the object that was passed in during create
 *
 */
PSLSTATUS PSL_API PSLWinMsgOnWait(PSLVOID* pvParam)
{
    DWORD dwWaitResult;
    PSLSTATUS status;

    if (PSLNULL == pvParam)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    dwWaitResult = WaitForSingleObject((HANDLE)pvParam, INFINITE);
    switch (dwWaitResult)
    {
    case WAIT_OBJECT_0:
        /*
         * This fucntion will always return following
         * success status which will allow queue to wait
         * till a message is available.
         *
         */
        status = PSLSUCCESS_BLOCKING;
        break;

    case WAIT_TIMEOUT:
    case WAIT_FAILED:
    default:
        /*  Never expect to get here
         */

        PSLASSERT(PSLFALSE);
        status = PSLERROR_PLATFORM_FAILURE;
        goto Exit;
    }

Exit:
    return status;
}

