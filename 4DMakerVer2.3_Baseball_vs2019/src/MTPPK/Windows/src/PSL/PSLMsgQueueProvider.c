/*
 *  PSLMsgQueueProvider.c
 *
 *  Contains the message queue provider implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#include "PSL.h"
#include "PSLMsgQueueUtil.h"
#include "PSLMsgQueueProvider.h"

PSLSTATUS PSLMsgQueueProviderGet(PSLRESPROVIDERINFO* pProviderInfo,
                                 PSLUINT32 dwHint, PSLVOID** ppvRes,
                                 PSLUINT32* pdwSize)
{
    PSLSTATUS status;
    PSLMSGQUEUEPROVIDERINFO* pmqInfo = PSLNULL;
    PSLMSGQUEUEOBJ* pmqObj = PSLNULL;
    PSLBOOL fAcquired = PSLFALSE;

    /*Ignore dwHint parameter*/
    dwHint = 0;

    if (PSLNULL != ppvRes)
    {
        *ppvRes = PSLNULL;
    }

    if (PSLNULL != pdwSize)
    {
        *pdwSize = 0;
    }

    if (PSLNULL == pProviderInfo || PSLNULL == ppvRes ||
        PSLNULL == pdwSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmqInfo = (PSLMSGQUEUEPROVIDERINFO*)pProviderInfo;

    status = PSLMutexAcquire(pmqInfo->baseInfo.hProviderLock,
                                PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    fAcquired = PSLTRUE;

    pmqObj = PSLMemAlloc(PSLMEM_PTR, sizeof(PSLMSGQUEUEOBJ));
    if (PSLNULL == pmqObj)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    *pdwSize = sizeof(PSLMSGQUEUEOBJ);

    *ppvRes = pmqObj;
    pmqObj = PSLNULL;

Exit:

    if (PSLNULL != pmqObj)
    {
        PSLMemFree(pmqObj);
    }

    if ((PSLTRUE == fAcquired) && (PSLNULL != pmqInfo))
    {
        PSLMutexRelease(pmqInfo->baseInfo.hProviderLock);
    }
    return status;
}

PSLSTATUS PSLMsgQueueProviderRelease(PSLRESPROVIDERINFO* pProviderInfo,
                                     PSLVOID* pvRes)
{
    PSLSTATUS status;
    PSLMSGQUEUEPROVIDERINFO* pmqInfo = PSLNULL;
    PSLBOOL fAcquired = PSLFALSE;

    if (PSLNULL == pProviderInfo || PSLNULL == pvRes)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmqInfo = (PSLMSGQUEUEPROVIDERINFO*)pProviderInfo;

    status = PSLMutexAcquire(pmqInfo->baseInfo.hProviderLock,
                                PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    fAcquired = PSLTRUE;

    PSLMemFree((PSLMSGQUEUEOBJ*)pvRes);

Exit:
    if ((PSLTRUE == fAcquired) && (PSLNULL != pmqInfo))
    {
        PSLMutexRelease(pmqInfo->baseInfo.hProviderLock);
    }
    return status;
}

PSLSTATUS PSLMsgQueueProviderCreate(PSLMSGQUEUEPROVIDERINFO** ppmqInfo)
{
    PSLSTATUS status;
    PSLMSGQUEUEPROVIDERINFO* pmqInfo = PSLNULL;

    if (PSLNULL != ppmqInfo)
    {
        *ppmqInfo = PSLNULL;
    }

    if (PSLNULL == ppmqInfo)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmqInfo = PSLMemAlloc(PSLMEM_PTR,
                            sizeof(PSLMSGQUEUEPROVIDERINFO));

    if (PSLNULL == pmqInfo)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    status = PSLMutexOpen(PSLFALSE, PSLNULL,
                            &pmqInfo->baseInfo.hProviderLock);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pmqInfo->baseInfo.pfnProviderGet = PSLMsgQueueProviderGet;

    pmqInfo->baseInfo.pfnProviderInit = PSLNULL;

    pmqInfo->baseInfo.pfnProviderRelease = PSLMsgQueueProviderRelease;

    pmqInfo->baseInfo.pfnProviderUninit = PSLNULL;

    *ppmqInfo = pmqInfo;
    pmqInfo = PSLNULL;

Exit:
    if (PSLNULL != pmqInfo)
    {
        PSLMemFree(pmqInfo);
    }
    return status;
}

PSLSTATUS PSLMsgQueueProviderDestroy(PSLMSGQUEUEPROVIDERINFO* pmqInfo)
{
    PSLSTATUS status;

    if (PSLNULL == pmqInfo)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLMutexClose(pmqInfo->baseInfo.hProviderLock);

    PSLMemFree(pmqInfo);

    status = PSLSUCCESS;

Exit:
    return status;
}
