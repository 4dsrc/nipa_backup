/*
 *  PSLLookupTableProvider.h
 *
 *  Contains the lookup table provider declarations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _PSLLOOKUPTABLEPROVIDER_H_
#define _PSLLOOKUPTABLEPROVIDER_H_

typedef struct
{
    PSLRESPROVIDERINFO  baseInfo;
    PSLUINT32           cdwItems;
    PSLLOOKUPTABLEOBJ*  pltObj;
}PSLLOOKUPTABLEPROVIDERINFO;

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableProviderInit(
                            PSLRESPROVIDERINFO* pProviderInfo,
                            PSLVOID* pvParam1,
                            PSLVOID* pvParam2);

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableProviderUninit(
                            PSLRESPROVIDERINFO* pProviderInfo);

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableProviderGet(
                            PSLRESPROVIDERINFO* pProviderInfo,
                            PSLUINT32 dwHint,
                            PSLVOID** ppvRes,
                            PSLUINT32* pdwSize);

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableProviderRelease(
                            PSLRESPROVIDERINFO* pProviderInfo,
                            PSLVOID* pvRes);

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableProviderCreate(
                            PSLUINT32 cdwItems,
                            PSLLOOKUPTABLEPROVIDERINFO** ppltInfo);

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableProviderDestroy(
                            PSLLOOKUPTABLEPROVIDERINFO* pltInfo);

#endif  /*_PSLLOOKUPTABLEPROVIDER_H_*/

