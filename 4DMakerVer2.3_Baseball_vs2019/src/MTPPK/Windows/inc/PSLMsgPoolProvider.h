/*
 *  PSLMsgPoolProvider.h
 *
 *  Contains the message pool provider declarations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _PSLMSGPOOLPROVIDER_H_
#define _PSLMSGPOOLPROVIDER_H_

typedef struct
{
    PSLRESPROVIDERINFO  baseInfo;
}PSLMSGPOOLPROVIDERINFO;

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgPoolProviderGet(
                            PSLRESPROVIDERINFO* pProviderInfo,
                            PSLUINT32 dwHint,
                            PSLVOID** ppvRes,
                            PSLUINT32* pdwSize);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgPoolProviderRelease(
                            PSLRESPROVIDERINFO* pProviderInfo,
                            PSLVOID* pvRes);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgPoolProviderCreate(
                            PSLMSGPOOLPROVIDERINFO** ppmpInfo);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgPoolProviderDestroy(
                            PSLMSGPOOLPROVIDERINFO* pmpInfo);

#endif  /*_PSLMSGQUEUEPROVIDER_H_*/

