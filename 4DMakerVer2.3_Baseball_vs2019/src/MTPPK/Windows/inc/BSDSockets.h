/*
 *  BSDSockets.h
 *
 *  Includes the Windows BSD Socket definitions in the PSL to allow
 *  access to BSD functionality.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _BSDSOCKETS_H_
#define _BSDSOCKETS_H_

/*
 *  Remove the definition of ASSERT so that it does not collide
 */

#ifdef ASSERT
#undef ASSERT
#endif  /* ASSERT */

/*
 *  Exclude rarely-used stuff from Windows headers
 */

#ifndef WIN32_LEAN_AND_MEAN
#define     WIN32_LEAN_AND_MEAN
#endif  /* WIN32_LEAN_AND_MEAN */

/*
 *  Windows Header Files
 */

#pragma warning(push)
#pragma warning(disable: 4115) /* named type definition in parentheses */
#pragma warning(disable: 4142) /* benign redefinition of type */
#pragma warning(disable: 4201) /* nameless struct/union */
#pragma warning(disable: 4204) /* non-constant aggregate initializer */
#pragma warning(disable: 4214) /* bit field types other than int */
#pragma warning(disable: 4218) /* must specify at least storage class or type */
#pragma warning(disable: 4995) /* name was marked as #pragma deprecated */
#pragma warning(disable: 4127) /* conditional expression is constant */

#include <winsock2.h>
#include <ws2tcpip.h>

#pragma warning(pop)

/*
 *  Disable ASSERT macro in general builds
 */

#ifdef ASSERT
#undef ASSERT
#endif  /* ASSERT */

#if defined(XMEASSERT)
#define ASSERT          XMEASSERT
#elif defined(PSLASSERT)
#define ASSERT          PSLASSERT
#endif  /* XMEASSERT */

#define BSDSOCKETS_API  WSAAPI

#endif  /* _BSDSOCKETS_H_ */

