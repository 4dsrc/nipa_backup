/*
 *  PSLBufferProvider.h
 *
 *  Contains the buffer provider declarations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _PSLBUFFERPROVIDER_H_
#define _PSLBUFFERPROVIDER_H_

typedef struct
{
    PSLRESPROVIDERINFO  baseInfo;
}PSLBUFFERPROVIDERINFO;

PSL_EXTERN_C PSLSTATUS PSL_API PSLBufferProviderGet(
                            PSLRESPROVIDERINFO* pProviderInfo,
                            PSLUINT32 dwHint,
                            PSLVOID** ppvRes,
                            PSLUINT32* pdwSize);

PSL_EXTERN_C PSLSTATUS PSL_API PSLBufferProviderRelease(
                            PSLRESPROVIDERINFO* pProviderInfo,
                            PSLVOID* pvRes);

PSL_EXTERN_C PSLSTATUS PSL_API PSLBufferProviderCreate(
                            PSLBUFFERPROVIDERINFO** ppBufInfo);

PSL_EXTERN_C PSLSTATUS PSL_API PSLBufferProviderDestroy(
                            PSLBUFFERPROVIDERINFO* pBufInfo);

#endif  /*_PSLMSGQUEUEPROVIDER_H_*/

