/*
 *  PSLWinDebugBreak.h
 *
 *  Helper file that forces PSLDebugBreak to point directly to the Windows
 *  DebugBreak call
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _PSLWINDEBUGBREAK_H_
#define _PSLWINDEBUGBREAK_H_

#ifdef __cplusplus
extern "C"
{
#endif  /* __cplusplus */

#ifndef MTP_RESPONDER
#define MTP_RESPONDER
#endif

#ifndef EXCLUDE_DEBUGBREAK_DEF

#pragma warning (disable : 4127)

#if defined(_M_IX86)

#if _MSC_VER < 1300
_inline void WinDebugBreak() {
	__asm int 3
}
#else
#define WinDebugBreak() __debugbreak()
#endif

#elif defined(_M_ARM)

#if _MSC_VER < 1300
void WinDebugBreak(void);
#else
#define WinDebugBreak() __debugbreak()
#endif

#endif

#endif /*!EXCLUDE_DEBUGBREAK_DEF*/

#ifndef WinDebugBreak
#define WinDebugBreak() DebugBreak()
#endif

#ifndef PSLDebugBreak
#define PSLDebugBreak   WinDebugBreak
#endif  /* PSLDebugBreak */

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* _PSLWINDEBUGBREAK_H_ */

