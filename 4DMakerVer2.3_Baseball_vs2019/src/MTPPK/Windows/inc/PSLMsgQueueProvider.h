/*
 *  PSLMsgQueueProvider.h
 *
 *  Contains the message queue provider declarations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _PSLMSGQUEUEPROVIDER_H_
#define _PSLMSGQUEUEPROVIDER_H_

typedef struct
{
    PSLRESPROVIDERINFO  baseInfo;
}PSLMSGQUEUEPROVIDERINFO;

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgQueueProviderGet(
                            PSLRESPROVIDERINFO* pProviderInfo,
                            PSLUINT32 dwHint,
                            PSLVOID** ppvRes,
                            PSLUINT32* pdwSize);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgQueueProviderRelease(
                            PSLRESPROVIDERINFO* pProviderInfo,
                            PSLVOID* pvRes);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgQueueProviderCreate(
                            PSLMSGQUEUEPROVIDERINFO** ppmqInfo);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgQueueProviderDestroy(
                            PSLMSGQUEUEPROVIDERINFO* pmqInfo);

#endif  /*_PSLMSGQUEUEPROVIDER_H_*/
