/*
 *  MTPXMELegacyService.h
 *
 *  Defines entry-point for registering the XME based legacy service
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPLEGACYSERVICE_H_
#define _MTPLEGACYSERVICE_H_

/*  MTPLegacyServiceInit
 *
 *  Initiales the Legacy MTP service
 */

const PSLUINT32 MAX_MTP_LEGACY_STORAGE_COUNT = 0xF;
const PSLUINT32 MAX_MTP_SERVICE_STORAGE_INDEX = 0xFF;

#define MAKE_SERVICEID(_idx) \
    (PACK_OBJDBID((_idx) + 1) | 0x80000000)

PSL_EXTERN_C PSLSTATUS PSL_API MTPLegacyServiceInit(
                    MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwServiceID,
                    PSLUINT32 dwStorageID,
                    PSLUINT32 *pcServices);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBSessionGetStorageId(
                    MTPDBSESSION hDBSession,
                    PSLUINT32 dwObjectHandle,
                    PSLUINT32* pdwStorageID);


#endif  /* _MTPLEGACYSERVICE_H_ */

