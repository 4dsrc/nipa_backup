/*
 *  PSLWindows.h
 *
 *  Header file that wraps calling standard Windows header files to allow
 *  warnings and other issues to be managed in one location.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _PSLWINDOWS_H_
#define _PSLWINDOWS_H_

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif

/*  Exclude rarely-used stuff from Windows headers
 */
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

/*
 *  Windows Header Files
 *
 */

#pragma warning(push)
#pragma warning(disable: 4115) /* named type definition in parentheses */
#pragma warning(disable: 4142) /* benign redefinition of type */
#pragma warning(disable: 4201) /* nameless struct/union */
#pragma warning(disable: 4204) /* non-constant aggregate initializer */
#pragma warning(disable: 4214) /* bit field types other than int */
#pragma warning(disable: 4218) /* must specify at least storage class or type */

#ifndef _WINSOCKAPI_
#define _WINSOCKAPI_   /* Prevent inclusion of winsock.h in windows.h */
#endif

#include <windows.h>
#include <objbase.h>
#include <strsafe.h>
#include <shellapi.h>

#pragma warning(pop)

// Helper macros

#define INVALID_HANDLE_SIZE         (0xffffffff)

#endif  /* _PSLWINDOWS_H_ */
