/*
 *  PSLPackPop.h
 *
 *  Performs a platform/compiler dependant packing operation- replaces
 *  the current packing by popping it off the packing stack
 *
 *  NOTE: This header may be included mutliple times and should perform
 *  operations each time.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#pragma pack(pop)

