/*
 *  MTPLoader.h
 *
 *  Windows specific header used to initialize a demand loaded version of MTP
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPLOADER_H_
#define _MTPLOADER_H_

#include "PSLWindows.h"
#include "PSLWinDemandLoader.h"

/*
 *  Declare the loader callback
 */

#define MTPLoad PSLLOADER_LOAD(MTP)

PSL_EXTERN_C PSLVOID PSL_API MTPLoad(PSLVOID);

PSL_EXTERN_C PSLVOID PSL_API MTPUnload(PSLVOID);

/*
 *  Declare loader functions for the MTP exports that we care about
 */

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPInitialize,
    (PSLUINT32 dwInitFlags),
    (dwInitFlags),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUninitialize,
    (),
    (),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgQueueCreate,
    (PSLVOID* pvParam, PFNONPOST pfnOnPost, PFNONWAIT pfnOnWait,
                            PFNONDESTROY pfnOnDestroy, PSLMSGQUEUE* pmqID),
    (pvParam, pfnOnPost, pfnOnWait, pfnOnDestroy, pmqID),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgQueueDestroy,
    (PSLMSGQUEUE mqID),
    (mqID),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgSend,
    (PSLMSGQUEUE mqID, PSLMSG* pMsg, PSLUINT32 dwMSTimeout),
    (mqID, pMsg, dwMSTimeout),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgPost,
    (PSLMSGQUEUE mqID, PSLMSG* pMsg),
    (mqID, pMsg),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgPeek,
    (PSLMSGQUEUE mqID, PSLMSG* pMsg),
    (mqID, pMsg),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgQueueSetCallbacks,
    (PSLMSGQUEUE mqDispatcher, PSLVOID* pvParam, PFNONPOST pfnOnPost,
                            PFNONWAIT pfnOnWait, PFNONDESTROY pfnOnDestroy),
    (mqDispatcher, pvParam, pfnOnPost, pfnOnWait, pfnOnDestroy),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgEnum,
    (PSLMSGQUEUE mqEnum, PFNMSGENUM pfnMsgEnum, PSLPARAM aParam),
    (mqEnum, pfnMsgEnum, aParam),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgPoolCreate,
    (PSLUINT32 cdwMsgs, PSLMSGPOOL* pmpID),
    (cdwMsgs, pmpID),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgPoolDestroy,
    (PSLMSGPOOL mpID),
    (mpID),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgAlloc,
    (PSLMSGPOOL mpID, PSLMSG** ppMsg),
    (mpID, ppMsg),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgFree,
    (PSLMSG* pMsg),
    (pMsg),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

#ifdef PSL_MSGTRACE

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgTrace,
    (PSLMSG* pMsg),
    (pMsg),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPMsgTraceReport,
    (PSLMSGPOOL mpID, PSLBOOL fReset),
    (mpID, fReset),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

#endif  /* PSL_MSGTRACE */

#ifdef _MTPUSBTRANSPORT_H_

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBBindInitiator,
    (MTPUSBDEVICE mtpusbdInitiator),
    (mtpusbdInitiator),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBBindResponder,
    (MTPUSBDEVICE mtpusbdInitiator),
    (mtpusbdInitiator),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

#endif  /* _MTPUSBTRANSPORT_H_ */

#ifdef _MTPIPTRANSPORT_H_

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPIPBindInitiator,
    (SOCKET sockInitiator, PSL_CONST struct sockaddr* psaInitiator,
                            PSLUINT32 cbAddr, PSLPARAM aParamTransport),
    (sockInitiator, psaInitiator, cbAddr, aParamTransport),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPIPBindResponder,
    (PSL_CONST SOCKADDR_STORAGE* psaResponder, PSL_CONST PSLBYTE* pbMAC,
                            PSLUINT32 cbMAC),
    (psaResponder, pbMAC, cbMAC),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPIPUDNToMACAddress,
    (PSLCWSTR szUDN, PSLWCHAR* rgchMACDest, PSLUINT32 cchMACDest,
                            PSLUINT32* pcchMACUsed),
    (szUDN, rgchMACDest, cchMACDest, pcchMACUsed),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPIPMACAddressStrToUDN,
    (PSLCWSTR szMAC, PSLWCHAR* rgchUDNDest, PSLUINT32 cchUDNDest,
                            PSLUINT32* pcchUDNUsed),
    (szMAC, rgchUDNDest, cchUDNDest, pcchUDNUsed),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

#endif  /* _MTPIPTRANSPORT_H_ */

#ifdef MTP_PERFORMANCE_TRACE
PSLLOADER_FUNCTION_VOID(PSL_API, MTPPerfInitLogger,
    (),
    (),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION_VOID(PSL_API, MTPPerfDeinitLogger,
    (),
    (),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION_VOID(PSL_API, MTPPerfStartLog,
    (),
    (),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION_VOID(PSL_API, MTPPerfStopLog,
    (),
    (),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION_VOID(PSL_API, MTPPerfSetLogObject,
    (PSLCSTR LogName),
    (LogName),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION_VOID(PSL_API, MTPPerfSetMTPContext,
    (PSLUINT32 dwTransactionID, PSLUINT16 wOpcode),
    (dwTransactionID, wOpcode),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION_VOID(PSL_API, MTPPerfWriteLogRecord,
    (MTPPERFEVENTCODE EventCode, PSLUINT8 EventType, PSLVOID *pPayload,
                             PSLUINT16 PayloadLen),
    (EventCode, EventType, pPayload, PayloadLen),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION_VOID(PSL_API, MTPPerfSetLogFilter,
    (PSLUINT8 FilterFlag, PSLUINT8 FilterLevel),
    (FilterFlag, FilterLevel),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPPerfIsLogEnabled,
    (PSLUINT8 FilterFlag, PSLUINT8 FilterLevel),
    (FilterFlag, FilterLevel),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTP);
#endif //MTP_PERFORMANCE_TRACE

/*
 *  Declare macros to map from the standard MTP functions to the loader
 *  functions
 */

#ifndef PSLLOADER_IMPLEMENT_LOADER_FUNCTIONS

#define MTPInitialize           PSLLOADER_IMPORT(MTPInitialize)
#define MTPUninitialize         PSLLOADER_IMPORT(MTPUninitialize)

#define MTPMsgQueueCreate       PSLLOADER_IMPORT(MTPMsgQueueCreate)
#define MTPMsgQueueDestroy      PSLLOADER_IMPORT(MTPMsgQueueDestroy)
#define MTPMsgSend              PSLLOADER_IMPORT(MTPMsgSend)
#define MTPMsgPost              PSLLOADER_IMPORT(MTPMsgPost)
#define MTPMsgPeek              PSLLOADER_IMPORT(MTPMsgPeek)
#define MTPMsgQueueSetCallbacks PSLLOADER_IMPORT(MTPMsgQueueSetCallbacks)
#define MTPMsgEnum              PSLLOADER_IMPORT(MTPMsgEnum)

#define MTPMsgPoolCreate        PSLLOADER_IMPORT(MTPMsgPoolCreate)
#define MTPMsgPoolDestroy       PSLLOADER_IMPORT(MTPMsgPoolDestroy)
#define MTPMsgAlloc             PSLLOADER_IMPORT(MTPMsgAlloc)
#define MTPMsgFree              PSLLOADER_IMPORT(MTPMsgFree)

#ifdef PSL_MSGTRACE
#define MTPMsgTrace             PSLLOADER_IMPORT(MTPMsgTrace)
#define MTPMsgTraceReport       PSLLOADER_IMPORT(MTPMsgTraceReport)
#endif  /* PSL_MSGTRACE */

#ifdef _MTPUSBTRANSPORT_H_
#define MTPUSBBindInitiator     PSLLOADER_IMPORT(MTPUSBBindInitiator)
#define MTPUSBBindResponder     PSLLOADER_IMPORT(MTPUSBBindResponder)
#endif /* _MTPUSBTRANSPORT_H_ */

#ifdef _MTPIPTRANSPORT_H_
#define MTPIPBindInitiator      PSLLOADER_IMPORT(MTPIPBindInitiator)
#define MTPIPBindResponder      PSLLOADER_IMPORT(MTPIPBindResponder)
#define MTPIPUDNToMACAddress    PSLLOADER_IMPORT(MTPIPUDNToMACAddress)
#define MTPIPMACAddressStrToUDN PSLLOADER_IMPORT(MTPIPMACAddressStrToUDN)
#endif  /* _MTPIPTRANSPORT_H_ */

#endif  /* !PSLLOADER_IMPLEMENT_LOADER_FUNCTIONS */

/*
 *  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#define SAFE_MTPMSGQUEUEDESTROY(_mq) \
if (PSLNULL != _mq) \
{ \
    MTPMsgQueueDestroy(_mq); \
    _mq = PSLNULL; \
}

#define SAFE_MTPMSGPOOLDESTROY(_mp) \
if (PSLNULL != _mp) \
{ \
    MTPMsgPoolDestroy(_mp); \
    _mp = PSLNULL; \
}

#define SAFE_MTPMSGFREE(_msg) \
if (PSLNULL != _msg) \
{ \
    MTPMsgFree(_msg); \
    _msg = PSLNULL; \
}

#endif  /* _MTPLOADER_H_ */

