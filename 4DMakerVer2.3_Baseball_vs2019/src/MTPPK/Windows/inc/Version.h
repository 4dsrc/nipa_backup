/*
 *  Version.h
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _VERSION_H_
#define _VERSION_H_

#define VERSION_MAJOR 12
#define VERSION_MINOR 0
#define VERSION_BUILD 0924

#define VERSTR_HELPER(X) #X
#define VERSTR(X) VERSTR_HELPER(X)

#define VERSION_STR   VERSTR(VERSION_MAJOR) L"." \
                      VERSTR(VERSION_MINOR) L"." \
                      VERSTR(VERSION_BUILD) L".0"

#endif //_VERSION_H_

