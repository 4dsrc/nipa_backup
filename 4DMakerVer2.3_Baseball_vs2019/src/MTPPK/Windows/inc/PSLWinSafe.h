/*
 *  PSLWinSafe.h
 *
 *  Safe Helpers for Windows APIs
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _PSLWINSAFE_H_
#define _PSLWINSAFE_H_

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(x) \
if( NULL != (x) ) \
{ \
    (x)->Release(); \
    (x) = NULL; \
}
#endif

#ifndef SAFE_ADDREF
#define SAFE_ADDREF(x) \
if( NULL != (x) ) \
{ \
    (x)->AddRef(); \
}
#endif

#ifndef SAFE_CLOSEHANDLE
#define SAFE_CLOSEHANDLE(x) \
if( NULL != (x) ) \
{ \
    CloseHandle(x); \
    (x) = NULL; \
}
#endif

#ifndef SAFE_RELEASECLOSEMUTEX
#define SAFE_RELEASECLOSEMUTEX(x) \
if (NULL != (x)) \
{ \
    ReleaseMutex(x); \
    CloseHandle(x); \
}
#endif

#ifndef SAFE_CLOSEFILE
#define SAFE_CLOSEFILE(x) \
if( INVALID_HANDLE_VALUE != (x) ) \
{ \
    CloseHandle(x); \
    (x) = INVALID_HANDLE_VALUE; \
}
#endif

#ifndef SAFE_CLOSESOCKET
#define SAFE_CLOSESOCKET(x) \
if( INVALID_SOCKET != (x) ) \
{ \
    closesocket(x); \
    (x) = INVALID_SOCKET; \
}
#endif

#ifndef SAFE_WSACLOSEEVENT
#define SAFE_WSACLOSEEVENT(x) \
if( WSA_INVALID_EVENT != (x) ) \
{ \
    WSACloseEvent(x); \
    (x) = WSA_INVALID_EVENT; \
}
#endif

#ifndef SAFE_REGCLOSEKEY
#define SAFE_REGCLOSEKEY(x) \
if( NULL != (x) ) \
{ \
    RegCloseKey(x); \
    (x) = NULL; \
}
#endif

#ifndef SAFE_FREELIBRARY
#define SAFE_FREELIBRARY(x) \
if( NULL != (x) ) \
{ \
    FreeLibrary(x); \
    (x) = NULL; \
}
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(p) \
if( (p) != NULL )   \
{                   \
    delete (p);     \
    (p) = NULL;     \
}
#endif

#ifndef SAFE_ARRAYDELETE
#define SAFE_ARRAYDELETE(p) \
if( (p) != NULL )   \
{                   \
    delete[] (p);   \
    (p) = NULL;     \
}
#endif

#ifndef SAFE_SYSFREESTRING
#define SAFE_SYSFREESTRING(x) \
if( NULL != (x) ) \
{ \
    SysFreeString(x); \
    (x) = NULL; \
}
#endif

#ifndef SAFE_COTASKMEMFREE
#define SAFE_COTASKMEMFREE(p) \
if( (p) != NULL )     \
{                     \
    CoTaskMemFree(p); \
    (p) = NULL;       \
}
#endif

#ifndef SAFE_CRYPTDELETE
#define SAFE_CRYPTDELETE(p, l)  \
if( (p) != NULL )               \
{                               \
    PSLCryptClearMemory(p, l);  \
    delete (p);                 \
    (p) = NULL;                 \
}
#endif

#ifndef SAFE_CRYPTARRAYDELETE
#define SAFE_CRYPTARRAYDELETE(p, l) \
if( (p) != NULL )               \
{                               \
    PSLCryptClearMemory(p, l);  \
    delete[] (p);               \
    (p) = NULL;                 \
}
#endif

#ifdef __cplusplus

inline DWORD SAFE_GETLASTERROR()
{
    DWORD dwErr = ::GetLastError();
    if (dwErr == 0)
    {
#ifdef _DEBUG
        DebugBreak();
#endif // _DEBUG
        dwErr = ERROR_INVALID_PARAMETER;
    }
    return dwErr;
}

#else   /* !__cplusplus */

EXTERN_C DWORD WINAPI SafeGetLastError();

#define SAFE_GETLASTERROR       SafeGetLastError

#endif  /* __cplusplus */

#endif  /* _PSLWINSAFE_H_ */

