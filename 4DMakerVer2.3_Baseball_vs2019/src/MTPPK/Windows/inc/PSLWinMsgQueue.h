/*
 *  PSLWinMsgQueue.h
 *
 *  Contains the message queue declarations for the Windows PSL
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _PSLWINMSGQUEUE_H_
#define _PSLWINMSGQUEUE_H_

PSL_EXTERN_C PSLSTATUS PSL_API PSLWinMsgGetQueueParam(
                            PSLVOID** ppvResult);

PSL_EXTERN_C PSLSTATUS PSL_API PSLWinMsgOnPost(
                            PSLVOID* pvParam);

PSL_EXTERN_C PSLSTATUS PSL_API PSLWinMsgOnWait(
                            PSLVOID* pvParam);

PSL_EXTERN_C PSLSTATUS PSL_API PSLWinMsgOnDestroy(
                            PSLVOID* pvParam);

/*
 *  Safe Helpers
 *
 */

#define SAFE_PSLWINMSGRELEASEQUEUEPARAM(_pv) \
if (PSLNULL != _pv) \
{ \
    PSLWinMsgOnDestroy(_pv); \
    _pv = PSLNULL; \
}

#endif /*_PSLWINMSGQUEUE_H_*/

