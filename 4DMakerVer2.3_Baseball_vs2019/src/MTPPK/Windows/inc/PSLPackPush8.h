/*
 *  PSLPackPush8.h
 *
 *  Performs a platform/compiler dependant packing operation- pushes the
 *  current packing and changes it to 8 byte packing.
 *
 *  NOTE: This header may be included mutliple times and should perform
 *  operations each time.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#pragma warning(disable:4103)   /* Packing changed across headers */

#pragma pack(push, 8)

