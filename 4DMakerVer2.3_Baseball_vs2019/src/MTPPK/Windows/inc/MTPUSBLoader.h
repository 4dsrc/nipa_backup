/*
 *  MTPUSBLoader.h
 *
 *  Windows specific header used to initialize a demand loaded version of the
 *  MTP USB device
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPUSBLOADER_H_
#define _MTPUSBLOADER_H_

#include "PSLWindows.h"
#include "PSLWinDemandLoader.h"

/*
 *  Declare the loader callback
 */

#define MTPUSBLoad PSLLOADER_LOAD(MTPUSBResponder)

PSL_EXTERN_C PSLVOID PSL_API MTPUSBLoad(PSLVOID);

PSL_EXTERN_C PSLVOID PSL_API MTPUSBUnload(PSLVOID);

/*
 *  Declare loader functions for the MTP USB exports that we care about
 */


PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceBind,
    (MTPUSBDEVICE hUSBDeviceSrc, PSLMSGQUEUE mqDest,
                            PFNMTPUSBDEVICEHANDLECLASSREQUEST pfnHandleClassReq,
                            PSLPARAM aParamClassReq),
    (hUSBDeviceSrc, mqDest, pfnHandleClassReq, aParamClassReq),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceUnbind,
    (MTPUSBDEVICE hUSBDeviceSrc, PSLMSGQUEUE mqDest),
    (hUSBDeviceSrc, mqDest),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceGetInfo,
    (MTPUSBDEVICE hUSBDevice, MTPUSBDEVICEINFO* pusbdI),
    (hUSBDevice, pusbdI),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceReset,
    (MTPUSBDEVICE hUSBDevice),
    (hUSBDevice),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceRequestBuffer,
    (MTPUSBDEVICE hUSBDevice, PSLUINT32 dwBufferType, PSLUINT32 cbRequested,
                            PSLPARAM aParam, PSLVOID** ppvBuffer,
                            PSLUINT32* cbActual),
    (hUSBDevice, dwBufferType, cbRequested, aParam, ppvBuffer, cbActual),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceReleaseBuffer,
    (MTPUSBDEVICE hUSBDevice, PSLVOID* pvBuffer),
    (hUSBDevice, pvBuffer),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceReceiveBuffer,
    (MTPUSBDEVICE hUSBDevice, PSLVOID* pvBuffer, PSLPARAM aParam,
                            PSLPARAM* paCookie),
    (hUSBDevice, pvBuffer, aParam, paCookie),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceSendBuffer,
    (MTPUSBDEVICE hUSBDevice, PSLUINT32 dwPacketFlag, PSLVOID* pvBuffer,
     PSLUINT32 cbSend, PSLPARAM aParam, PSLPARAM* paCookie),
    (hUSBDevice, dwPacketFlag, pvBuffer, cbSend, aParam, paCookie),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceCancelRequest,
    (MTPUSBDEVICE hUSBDevice, PSLPARAM aCookie),
    (hUSBDevice, aCookie),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceStallPipe,
    (MTPUSBDEVICE hUSBDevice, PSLUINT32 dwBufferType),
    (hUSBDevice, dwBufferType),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceClearPipeStall,
    (MTPUSBDEVICE hUSBDevice, PSLUINT32 dwBufferType),
    (hUSBDevice, dwBufferType),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

PSLLOADER_FUNCTION(PSLSTATUS, PSL_API, MTPUSBDeviceSendControlHandshake,
    (MTPUSBDEVICE hUSBDevice),
    (hUSBDevice),
    PSLERROR_LIBRARY_NOT_FOUND,
    MTPUSBResponder);

/*
 *  Declare macros to map from the standard MTP functions to the loader
 *  functions
 */

#ifndef PSLLOADER_IMPLEMENT_LOADER_FUNCTIONS

#define MTPUSBDeviceBind            PSLLOADER_IMPORT(MTPUSBDeviceBind)
#define MTPUSBDeviceUnbind          PSLLOADER_IMPORT(MTPUSBDeviceUnbind)
#define MTPUSBDeviceGetInfo         PSLLOADER_IMPORT(MTPUSBDeviceGetInfo)
#define MTPUSBDeviceReset           PSLLOADER_IMPORT(MTPUSBDeviceReset)
#define MTPUSBDeviceRequestBuffer   PSLLOADER_IMPORT(MTPUSBDeviceRequestBuffer)
#define MTPUSBDeviceReleaseBuffer   PSLLOADER_IMPORT(MTPUSBDeviceReleaseBuffer)
#define MTPUSBDeviceReceiveBuffer   PSLLOADER_IMPORT(MTPUSBDeviceReceiveBuffer)
#define MTPUSBDeviceSendBuffer      PSLLOADER_IMPORT(MTPUSBDeviceSendBuffer)
#define MTPUSBDeviceCancelRequest   PSLLOADER_IMPORT(MTPUSBDeviceCancelRequest)
#define MTPUSBDeviceStallPipe       PSLLOADER_IMPORT(MTPUSBDeviceStallPipe)
#define MTPUSBDeviceClearPipeStall  PSLLOADER_IMPORT(MTPUSBDeviceClearPipeStall)

#define MTPUSBDeviceSendControlHandshake \
        PSLLOADER_IMPORT(MTPUSBDeviceSendControlHandshake)

#endif  /* !PSLLOADER_IMPLEMENT_LOADER_FUNCTIONS */

#endif  /* _MTPUSBLOADER_H_ */

