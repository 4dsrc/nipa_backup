/*
 *  PSLUnknown.h
 *
 *  Contains declaration for a generic IUnknown implementation that may be
 *  used as a base class for all COM style classes.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _PSLUNKNOWN_H
#define _PSLUNKNOWN_H

/*
 *  IUnknown Definition
 *
 *  On platforms where IUnknown is not already defined make sure it get's
 *  defined.
 *
 */

#ifndef __IUnknown_INTERFACE_DEFINED__

DEFINE_GUID(IID_IUnknown,
    0x00000000, 0x0000, 0x0000, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46);

class IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE QueryInterface(
                            REFIID riid,
                            LPVOID* ppvInterface ) = 0;

    virtual DWORD STDMETHODCALLTYPE AddRef() = 0;
    virtual DWORD STDMETHODCALLTYPE Release() = 0;
};

#endif // __IUnknown_INTERFACE_DEFINED__


/*
 *  DELEGATE_IUNKNOWN
 *
 *  Helper macro to handle polymorphism issues and route all IUnknown APIs to
 *  the specified base class
 *
 */

#define DELEGATE_IUNKNOWN( base )   \
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void **ppv) \
        { return base::QueryInterface(riid, ppv); } \
    ULONG STDMETHODCALLTYPE AddRef() { return base::AddRef(); } \
    ULONG STDMETHODCALLTYPE Release() { return base::Release(); }


/*
 *  CPSLUnknown
 *
 *  Declaration of the CPSLUnknown class.
 *
 */

class CPSLUnknown : public IUnknown
{
private:
    LONG                    m_lInShutdown;

protected:
    DWORD                   m_cRef;

    CPSLUnknown();
    virtual ~CPSLUnknown();

    virtual HRESULT LocateInterface(REFIID riid,
                            LPVOID* ppvInterface);

    virtual void OnFinalRelease();

    virtual BOOL ShouldDelete();

public:
    // IUnknown

    virtual HRESULT STDMETHODCALLTYPE QueryInterface(
                            REFIID riid,
                            void **ppvInterface);

    virtual ULONG STDMETHODCALLTYPE AddRef();

    virtual ULONG STDMETHODCALLTYPE Release();
};


/*
 *  PSLUnkAutoRef
 *
 *  Class and macro for automatically handling references being placed on
 *  an IUnknown object when the object is declared (PSLUNK_AUTOREF macro) and
 *  released when it goes out of scope.
 *
 */

class PSLUnkAutoRef
{
private:
    CPSLUnknown*            m_pUnk;

public:
    PSLUnkAutoRef(CPSLUnknown* pUnk)
    {
        m_pUnk = pUnk;
        if (NULL != m_pUnk)
        {
            m_pUnk->AddRef();
        }
    }

    ~PSLUnkAutoRef()
    {
        if (NULL != m_pUnk)
        {
            m_pUnk->Release();
        }
    }
};

#define PSLUNK_AUTOREF(pUnk)  PSLUnkAutoRef PSLUnkAutoRef_##pUnk(pUnk)

#endif // _PSLUNKNOWN_H

