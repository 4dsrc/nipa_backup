/*
 *  MTPDLL.h
 *
 *  Defines the public functions exported from the Windows MTP DLL
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _MTPDLL_H_
#define _MTPDLL_H_

#include "PSLTypes.h"
#include "PSLErrors.h"
#include "PSLUtils.h"
#include "PSLDynamicList.h"
#include "PSLMsgQueue.h"
#include "PSLMsgPool.h"
#include "PSLLookupTable.h"

#include "MTP.h"
#include "MTPProxy.h"
#include "MTPInitialize.h"
#include "MTPUSBTransport.h"
#include "MTPIPTransport.h"

#endif  /* _MTPDLL_H_ */

