/*
 *  PSLWinDemandLoader.h
 *
 *  Contains macros useful for using the PSLDemandLoader functionality from
 *  a Windows environment.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Windows reference code is licensed under the Microsoft Windows Portable
 *  Device Enabling Kit for Media Transfer Protocol (MTP).
 *
 */

#ifndef _PSLWINDEMANDLOADER_H_
#define _PSLWINDEMANDLOADER_H_

#include "PSLDemandLoader.h"

#define IMPORT_DLL_FUNCTION_CORE(_hDLL, _name, _imp, _onErr)                \
        {                                                                   \
            FARPROC         _pfn;                                           \
            _pfn = GetProcAddress(_hDLL, (#_name));                     \
            PSLLOADER_BIND_IMPORT(_imp, _pfn, _onErr);                      \
        }


#define IMPORT_DLL_FUNCTION_RENAME(_hDLL, _name, _imp)                      \
        IMPORT_DLL_FUNCTION_CORE(_hDLL, _name, _imp, PSLASSERT(PSLFALSE))

#define IMPORT_DLL_FUNCTION(_hDLL, _imp)                                    \
        IMPORT_DLL_FUNCTION_CORE(_hDLL, _imp, _imp, PSLASSERT(PSLFALSE))

#endif  /* _PSLWINDEMANDLOADER_H_ */

