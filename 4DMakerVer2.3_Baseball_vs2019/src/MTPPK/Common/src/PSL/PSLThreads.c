/*
 *  PSLThreads.c
 *
 *  Contains single threaded implementation of the PSLThreads API
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"

#ifdef PSL_SINGLE_THREADED

/*
 * VC compiler will show warning if the input paramters to following
 * functions are not used or referenced. To supress this warning
 * folowing compiler directive is used
 *
 */
#ifdef _MSC_VER
#pragma warning(disable:4100)
#endif /*_MSC_VER*/

/*
 *  PSLThreadCreate
 *
 *  Creates a thread and starts execution at the specified thread
 *  function
 *
 *  Arguments:
 *      PFNPSLTHREADFUNC
 *                      pfnThreadFunc       Location thread execution
 *                                          should start
 *      PSLVOID*        pvData              Data to pass to thread
 *                                          function
 *      PSLBOOL         fCreateSuspended    PSLTRUE if thread should be
 *                                            created in a suspended state
 *      PSLUINT32       cbStack             Stack size
 *      PSLHANDLE*      phThread            Return location for thread
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadCreate(PFNPSLTHREADFUNC pfnThreadFunc,
                            PSLVOID* pvData, PSLBOOL fCreateSuspended,
                            PSLUINT32 cbStack, PSLHANDLE* phThread)
{
    if (PSLNULL != phThread)
    {
        *phThread = PSLNULL;
    }
    return ((PSLNULL != pfnThreadProc) && (PSLNULL != phThread)) ?
                            PSLSUCCESS_SINGLE_THREADED :
                            PSLERROR_INVALID_PARAMETER;
}


/*
 *  PSLThreadClose
 *
 *  Closes a thread handle.
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to close
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadClose(PSLHANDLE hThread)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLThreadTerminate
 *
 *  Forcibly terminates a running thread and sets the threads exit
 *  code to the given value.
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to terminate
 *      PSLUINT32       dwResult            Thread result
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadTerminate(PSLHANDLE hThread, PSLUINT32 dwResult)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLThreadGetCurrentThreadID
 *
 *  Returns a unique identifier for the current thread
 *
 *  Arguments:
 *      Nothing
 *
 *  Returns:
 *      PSLUINT32       Unique thread identifier
 *
 */

PSLUINT32 PSLThreadGetCurrentThreadID(PSLVOID)
{
    return 0;
}


/*
 *  PSLThreadGetResult
 *
 *  Returns the result code from the thread routine.  This code is set
 *  via a call to TerminateThread or by the thread function returning.
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to get exit code from
 *      PSLUINT32       dwMSTimeout         Period to wait for a result
 *                                            in milliseconds
 *      PSLUINT32*      pdwResult           Thread result code
 *
 */

PSLSTATUS PSLThreadGetResult(PSLHANDLE hThread, PSLUINT32 dwMSTimeout,
                            PSLUINT32* pdwResult)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLSleep
 *
 *  Puts the current thread to sleep for the specified amount of time.
 *  PSLSleep(0) allows any other thread to be released and if no process
 *  is available returns immediately to this thread.
 *  PSLSleep(PSLTHREAD_INFINITE) is a synonym for PSLThreadSuspend.
 *
 *  Arguments:
 *      PSLUINT32       dwMSSleep           Milliseconds to sleep
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLSleep(PSLUINT32 dwMSSleep)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLThreadSetPriority
 *
 *  Sets the thread priority base on the specified priority value
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to adjust priority
 *      PSLUINT32       dwPriority          Priority
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadSetPriority(PSLHANDLE hThread, PSLUINT32 dwPriority)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLThreadGetPriority
 *
 *  Returns the priority of the thread indicated
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread priority to retrieve
 *      PSLUINT32*      pdwPriority         Return buffer for priority
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadGetPriority(PSLHANDLE hThread,
                               PSLUINT32* pdwPriority)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLThreadSuspend
 *
 *  Supsends the thread specified.  Suspend/Resume are reference counted
 *  so it is not sufficient to call Suspend twice and resume once to get
 *  a thread executing again.
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to suspend
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadSuspend(PSLHANDLE hThread)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLThreadResume
 *
 *  Resumes a suspended thread.
 *
 *  Arguments:
 *      PSLHANDLE       hThread             Thread to suspend
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLThreadResume(PSLHANDLE hThread)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLMutexOpen
 *
 *  Creates a mutex, or acquires an existing mutex of the same name.
 *
 *  Arguments:
 *      PSLUINT32       dwWaitToAcquire     0 if the mutex is not to be
 *                                            immediately acquired, non-zero
 *                                            time to it is to be acquired
 *      PSLCWSTR        szName              Name for the mutex
 *      PSLHANDLE*      phPSLMutex          Return buffer for mutex
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success, PSLSUCCESS_EXISTS if mutex
 *                    already existed
 *
 */

PSLSTATUS PSLMutexOpen(PSLUINT32 dwWaitToAcquire, PSLCWSTR szName,
                            PSLHANDLE* phPSLMutex)
{
    if (PSLNULL != phPSLMutex)
    {
        *phPSLMutex = PSLNULL;
    }
    return (PSLNULL != phPSLMutex) ? PSLSUCCESS_SINGLE_THREADED :
                            PSLERROR_INVALID_PARAMETER;
}


/*
 *  PSLMutexClose
 *
 *  Closes a mutex object acquired with PSLMutexOpen
 *
 *  Arguments:
 *      PSLHANDLE       hPSLMutex           Mutex to close
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLMutexClose(PSLHANDLE hPSLMutex)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLMutexAcquire
 *
 *  Attempts to acquire access to the mutex.  The call will be block for
 *  up to dwMSTimeout milliseconds.
 *
 *  Arguments:
 *      PSLHANDLE       hPSLMutex           Mutex to acquire
 *      PSLUINT32       dwMSTimeout         Period of time that the wait
 *                                          should block before timing
 *                                          out
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *                  PSLSUCCESS_WOULD_BLOCK if 0 == dwMSTimeout and
 *                  object is not in a signalled state
 *                  PSLSUCCESS_TIMEOUT if dwMSTimeout expired
 *
 */

PSLSTATUS PSLMutexAcquire(PSLHANDLE hPSLMutex, PSLUINT32 dwMSTimeout)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLMutexRelease
 *
 *  Releases the held mutex.
 *
 *  Arguments:
 *      PSLHANDLE       hPSLMutex           Mutex to release
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLMutexRelease(PSLHANDLE hPSLMutex)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLSignalOpen
 *
 *  Opens a signal object (Windows event) for cross thread
 *  syncrhonization
 *
 *  Arguments:
 *      PSLBOOL         fSignalled          TRUE if the signal should be
 *                                          initialized in a signalled
 *                                          state
 *      PSLBOOL         fManualReset        TRUE of the signal should be
 *                                          reset manually
 *      PSLCWSTR        szName              Signal name
 *      PSLHANDLE*      phPSLSignal         Return buffer for the signal
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success, PSLSUCCESS_EXISTS if signal
 *                    already existed
 *
 */

PSLSTATUS PSLSignalOpen(PSLBOOL fSignalled, PSLBOOL fManualReset,
                                PSLCWSTR szName, PSLHANDLE* phPSLSignal)
{
    if (PSLNULL != phPSLSignal)
    {
        *phPSLSignal = PSLNULL;
    }
    return (PSLNULL != phPSLSignal) ? PSLSUCCESS_SINGLE_THREADED :
                            PSLERROR_INVALID_PARAMETER;
}


/*
 *  PSLSignalClose
 *
 *  Closes the specified signal object
 *
 *  Arguments:
 *      PSLHANDLE       hPSLSignal          Signal to close
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLSignalClose(PSLHANDLE hPSLSignal)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLSignalSet
 *
 *  Sets the specified signal to the signalled state
 *
 *  Arguments:
 *      PSLHANDLE       hPSLSignal          Signal to set
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLSignalSet(PSLHANDLE hPSLSignal)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLSignalReset
 *
 *  Resets the specified signal object to non-signalled
 *
 *  Arguments:
 *      PSLHANDLE       hPSLSignal          Signal to reset
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSLSignalReset(PSLHANDLE hPSLSignal)
{
    return PSLSUCCESS_SINGLE_THREADED;
}


/*
 *  PSLSignalWait
 *
 *  Waits for the specified signal to enter the signalled state. The
 *  call will block for the specified timeout value.
 *
 *  Arguments:
 *      PSLHANDLE       hPSLSignal          Signal to wait on
 *      PSLUINT32       dwMSTimeout         Period of time that the wait
 *                                          should block before timing
 *                                          out
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *                  PSLSUCCESS_WOULD_BLOCK if 0 == dwMSTimeout and
 *                  object is not in a signalled state
 *                  PSLSUCCESS_TIMEOUT if dwMSTimeout expired
 *
 */

PSLSTATUS PSLSignalWait(PSLHANDLE hPSLSignal, PSLUINT32 dwMSTimeout)
{
    return PSLSUCCESS_SINGLE_THREADED;
}
#endif /*PSL_SINGLE_THREADED*/
