/*
 *  PSLString.c
 *
 *  Contains definitions for the PSL string APIs
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"


/*
 *  PSLStringLenA
 *
 *  Determines the length of a string up to the specified maximum length.
 *
 *  Arguments:
 *      PSLCSTR         szSrc               String to determine length of
 *      PSLUINT32       cchMaxSrc           Maximum length of the string
 *      PSLUINT32*      pcchSrc             Resulting length of the string
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLStringLenA(PSLCSTR szSrc, PSLUINT32 cchMaxSrc,
                            PSLUINT32* pcchSrc)
{
    PSL_CONST PSLCHAR*  pch;
    PSLSTATUS           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcchSrc)
    {
        *pcchSrc = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == szSrc) || (PSLSTRING_MAX_CCH < cchMaxSrc) ||
        (PSLNULL == pcchSrc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine the length
     */

    for (pch = szSrc; ('\0' != *pch) && (0 < cchMaxSrc); pch++, cchMaxSrc--)
    {
    }

    /*  Return the length
     */

    *pcchSrc = pch - szSrc;

    /*  If we encountered the maximum length of the string but not a
     *  terminating character report the user
     */

    ps = ('\0' == *pch) ? PSLSUCCESS : PSLSUCCESS_NOT_FOUND;

Exit:
    return ps;
}


/*
 *  PSLStringLen
 *
 *  Determines the length of a string up to the specified maximum length.
 *
 *  Arguments:
 *      PSLCWSTR        szSrc               String to determine length of
 *      PSLUINT32       cchMaxSrc           Maximum length of the string
 *      PSLUINT32*      pcchSrc             Resulting length of the string
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLStringLen(PSLCWSTR szSrc, PSLUINT32 cchMaxSrc,
                            PSLUINT32* pcchSrc)
{
    PSL_CONST PSLWCHAR* pch;
    PSLSTATUS           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcchSrc)
    {
        *pcchSrc = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == szSrc) || (PSLSTRING_MAX_CCH < cchMaxSrc) ||
        (PSLNULL == pcchSrc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine the length
     */

    for (pch = szSrc; (L'\0' != *pch) && (0 < cchMaxSrc); pch++, cchMaxSrc--)
    {
    }

    /*  Return the length
     */

    *pcchSrc = pch - szSrc;

    /*  If we encountered the maximum length of the string but not a
     *  terminating character report the user
     */

    ps = (L'\0' == *pch) ? PSLSUCCESS : PSLSUCCESS_NOT_FOUND;

Exit:
    return ps;
}


/*
 *  PSLStringCopyA
 *
 *  Copies up to PSLSTRING_MAX_CCH characters from szSrc to szDest.
 *
 *  Arguments:
 *      PSLCHAR*        szDest              Destination for the copy
 *      PSLUINT32       cchMaxDest          Size of the destination buffer
 *      PSLCSTR         szSrc               Source string
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLStringCopyA(PSLCHAR* szDest, PSLUINT32 cchMaxDest,
                            PSLCSTR szSrc)
{
    /*  Call the full function to do the work- leaving an actual entry point
     *  so that people can overload with an intrinsic/platform optimized
     *  version if preferred.
     */

    return PSLStringCopyLenA(szDest, cchMaxDest, PSLNULL, szSrc,
                            PSLSTRING_MAX_CCH);
}


/*
 *  PSLStringCopy
 *
 *  Copies up to PSLSTRING_MAX_CCH characters from szSrc to szDest.
 *
 *  Arguments:
 *      PSLWCHAR*       szDest              Destination for the copy
 *      PSLUINT32       cchMaxDest          Size of the destination buffer
 *      PSLCWSTR        szSrc               Source string
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLStringCopy(PSLWCHAR* szDest, PSLUINT32 cchMaxDest,
                            PSLCWSTR szSrc)
{
    /*  Call the full function to do the work- leaving an actual entry point
     *  so that people can overload with an intrinsic/platform optimized
     *  version if preferred.
     */

    return PSLStringCopyLen(szDest, cchMaxDest, PSLNULL, szSrc,
                            PSLSTRING_MAX_CCH);
}


/*
 *  PSLStringCopyLenA
 *
 *  Copies up to cchSrc characters to szDest while making sure that szDest does
 *  not overflow.  Optionally returns the number of characters copied.
 *  Guarantees that szDest is terminated.
 *
 *  Arguments:
 *      PSLCHAR*        szDest              Destination for the copy
 *      PSLUINT32       cchMaxDest          Size of the destination buffer
 *      PSLUINT32*      pcchDest            Optional buffer to return number
 *                                            of characters copied
 *      PSLCSTR         szSrc               Source string
 *      PSLUINT32       cchSrc              Number of bytes to copy,
 *                                            PSLSTRING_MAX_CCH to copy all
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLStringCopyLenA(PSLCHAR* szDest, PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest, PSLCSTR szSrc,
                            PSLUINT32 cchSrc)
{
    PSLSTATUS           ps;
    PSL_CONST PSLCHAR*  pchSrc;
    PSLCHAR*            pchDest;

    /*  Clear results for safety
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = 0;
    }
    if ((PSLNULL != szDest) && (0 < cchMaxDest))
    {
        *szDest = '\0';
    }

    /*  Validate arguments
     */

    if ((PSLNULL == szDest) || (0 == cchMaxDest) ||
        (PSLSTRING_MAX_CCH < cchMaxDest))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we have work to do
     */

    if ((PSLNULL == szSrc) || (0 == cchSrc))
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Start copying the string over- note that we are reserving space in
     *  the destination for the terminating character
     */

    for (pchSrc = szSrc, pchDest = szDest;
        ('\0' != *pchSrc) && (1 < cchMaxDest) && (0 < cchSrc);
        *pchDest = *pchSrc, pchSrc++, pchDest++, cchMaxDest--, cchSrc--)
    {
    }

    /*  Guarantee termination
     */

    *pchDest = '\0';

    /*  Return characters copied if requested
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = pchDest - szDest;
    }

    /*  Determine whether or not we ran out of space while performing the
     *  copy- remember that we always reserve one character for the terminator
     */

    ps = ((1 < cchMaxDest) || (('\0' == *pchSrc) || (0 == cchSrc))) ?
                            PSLSUCCESS : PSLERROR_INSUFFICIENT_BUFFER;

Exit:
    return ps;
}


/*
 *  PSLStringCopyLen
 *
 *  Copies up to cchSrc characters to szDest while making sure that szDest does
 *  not overflow.  Optionally returns the number of characters copied.
 *  Guarantees that szDest is terminated.
 *
 *  Arguments:
 *      PSLWCHAR*       szDest              Destination for the copy
 *      PSLUINT32       cchMaxDest          Size of the destination buffer
 *      PSLUINT32*      pcchDest            Optional buffer to return number
 *                                            of characters copied
 *      PSLCWSTR        szSrc               Source string
 *      PSLUINT32       cchSrc              Number of bytes to copy,
 *                                            PSLSTRING_MAX_CCH to copy all
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLStringCopyLen(PSLWCHAR* szDest, PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest, PSLCWSTR szSrc,
                            PSLUINT32 cchSrc)
{
    PSLSTATUS           ps;
    PSL_CONST PSLWCHAR* pchSrc;
    PSLWCHAR*           pchDest;

    /*  Clear results for safety
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = 0;
    }
    if ((PSLNULL != szDest) && (0 < cchMaxDest))
    {
        *szDest = L'\0';
    }

    /*  Validate arguments
     */

    if ((PSLNULL == szDest) || (0 == cchMaxDest) ||
        (PSLSTRING_MAX_CCH < cchMaxDest))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we have work to do
     */

    if ((PSLNULL == szSrc) || (0 == cchSrc))
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Start copying the string over- note that we are reserving space in
     *  the destination for the terminating character
     */

    for (pchSrc = szSrc, pchDest = szDest;
        (L'\0' != *pchSrc) && (1 < cchMaxDest) && (0 < cchSrc);
        *pchDest = *pchSrc, pchSrc++, pchDest++, cchMaxDest--, cchSrc--)
    {
    }

    /*  Guarantee termination
     */

    *pchDest = L'\0';

    /*  Return characters copied if requested
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = pchDest - szDest;
    }

    /*  Determine whether or not we ran out of space while performing the
     *  copy
     */

    ps = ((1 < cchMaxDest) || ((L'\0' == *pchSrc) || (0 == cchSrc))) ?
                            PSLSUCCESS : PSLERROR_INSUFFICIENT_BUFFER;

Exit:
    return ps;
}


/*
 *  PSLStringCatA
 *
 *  Concatonates the contents of szSrc at the end of szDest guaranteeing
 *  termination.
 *
 *  Arguments:
 *      PSLCHAR*        szDest              String to add on to
 *      PSLUINT32       cchMaxDest          Total size of the destination-
 *                                            must accomidate what is in
 *                                            szDest initially plus what is
 *                                            to be added
 *      PSLCSTR         szSrc               String to concatonate
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLStringCatA(PSLCHAR* szDest, PSLUINT32 cchMaxDest,
                            PSLCSTR szSrc)
{
    /*  Call the full function to do the work- leaving an actual entry point
     *  so that people can overload with an intrinsic/platform optimized
     *  version if preferred.
     */

    return PSLStringCatLenA(szDest, cchMaxDest, PSLNULL, szSrc,
                            PSLSTRING_MAX_CCH);
}


/*
 *  PSLStringCat
 *
 *  Concatonates the contents of szSrc at the end of szDest guaranteeing
 *  termination.
 *
 *  Arguments:
 *      PSLWCHAR*        szDest              String to add on to
 *      PSLUINT32       cchMaxDest          Total size of the destination-
 *                                            must accomidate what is in
 *                                            szDest initially plus what is
 *                                            to be added
 *      PSLCWSTR        szSrc               String to concatonate
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLStringCat(PSLWCHAR* szDest, PSLUINT32 cchMaxDest,
                            PSLCWSTR szSrc)
{
    /*  Call the full function to do the work- leaving an actual entry point
     *  so that people can overload with an intrinsic/platform optimized
     *  version if preferred.
     */

    return PSLStringCatLen(szDest, cchMaxDest, PSLNULL, szSrc,
                            PSLSTRING_MAX_CCH);
}


/*
 *  PSLStringCatLenA
 *
 *  Concatonates the contents of szSrc at the end of szDest guaranteeing
 *  termination.
 *
 *  Arguments:
 *      PSLCHAR*        szDest              String to add on to
 *      PSLUINT32       cchMaxDest          Total size of the destination-
 *                                            must accomidate what is in
 *                                            szDest initially plus what is
 *                                            to be added
 *      PSLUINT32*      pcchDest            Resulting string length
 *      PSLCSTR         szSrc               String to concatonate
 *      PSLUINT32       cchSrc              Source string length
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLStringCatLenA(PSLCHAR* szDest, PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest, PSLCSTR szSrc,
                            PSLUINT32 cchSrc)
{
    PSLUINT32       cchCopied;
    PSLUINT32       cchDest;
    PSLSTATUS       ps;

    /*  Clear results for safety
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == szDest) || (0 == cchMaxDest) ||
        (PSLSTRING_MAX_CCH <= cchMaxDest))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine the length of the existing string- report error if the
     *  specified buffer is not long enough to hold even the source string
     */

    ps = PSLStringLenA(szDest, cchMaxDest, &cchDest);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  And add the source string on to the destination
     */

    ps = PSLStringCopyLenA((szDest + cchDest), (cchMaxDest - cchDest),
                            &cchCopied, szSrc, cchSrc);

    /*  Make sure the length is returned if requested
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = cchDest + cchCopied;
    }

Exit:
    return ps;
}


/*
 *  PSLStringCatLen
 *
 *  Concatonates the contents of szSrc at the end of szDest guaranteeing
 *  termination.
 *
 *  Arguments:
 *      PSLWCHAR*       szDest              String to add on to
 *      PSLUINT32       cchMaxDest          Total size of the destination-
 *                                            must accomidate what is in
 *                                            szDest initially plus what is
 *                                            to be added
 *      PSLUINT32*      pcchDest            Resulting string length
 *      PSLCWSTR        szSrc               String to concatonate
 *      PSLUINT32       cchSrc              Source string length
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLStringCatLen(PSLWCHAR* szDest, PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest, PSLCWSTR szSrc,
                            PSLUINT32 cchSrc)
{
    PSLUINT32       cchCopied;
    PSLUINT32       cchDest;
    PSLSTATUS       ps;

    /*  Clear results for safety
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == szDest) || (0 == cchMaxDest) ||
        (PSLSTRING_MAX_CCH <= cchMaxDest))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine the length of the existing string- report error if the
     *  specified buffer is not long enough to hold even the source string
     */

    ps = PSLStringLen(szDest, cchMaxDest, &cchDest);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  And add the source string on to the destination
     */

    ps = PSLStringCopyLen((szDest + cchDest), (cchMaxDest - cchDest),
                            &cchCopied, szSrc, cchSrc);

    /*  Make sure the length is returned if requested
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = cchDest + cchCopied;
    }

Exit:
    return ps;
}


/*
 *  PSLStringCmpA
 *
 *  Compares szStr1 and szStr2
 *
 *  Arguments:
 *      PSLCSTR         szStr1              String 1
 *      PSLCSTR         szStr2              String 2
 *
 *  Returns:
 *      PSLINT32        < 0 - String1 < String2
 *                      = 0 - String1 == String2
 *                      > 0 - String1 > String2
 *
 */

PSLINT32 PSL_API PSLStringCmpA(PSLCSTR szStr1, PSLCSTR szStr2)
{
    PSLINT32            nResult;
    PSL_CONST PSLCHAR*  pch1;
    PSL_CONST PSLCHAR*  pch2;

    /*  Make sure we have work to do- we treat a NULL string as less then
     *  an empty string
     */

    if (PSLNULL == szStr1)
    {
        nResult = (PSLNULL == szStr2) ? 0 : -1;
        goto Exit;
    }
    if (PSLNULL == szStr2)
    {
        nResult = (PSLNULL == szStr1) ? 0 : 1;
        goto Exit;
    }

    /*  Walk through the string looking for a difference
     */

    pch1 = szStr1;
    pch2 = szStr2;
    for (nResult = (PSLINT32)*pch1 - (PSLINT32)*pch2;
            (0 == nResult) && ('\0' != *pch1) && ('\0' != *pch2);
            nResult = (PSLINT32)*(++pch1) - (PSLINT32)*(++pch2))
    {
    }

Exit:
    return nResult;
}


/*
 *  PSLStringICmpA
 *
 *  Compares szStr1 and szStr2 in a case insensitive fashion.
 *
 *  Arguments:
 *      PSLCSTR         szStr1              String 1
 *      PSLCSTR         szStr2              String 2
 *
 *  Returns:
 *      PSLINT32        < 0 - String1 < String2
 *                      = 0 - String1 == String2
 *                      > 0 - String1 > String2
 *
 */

PSLINT32 PSL_API PSLStringICmpA(PSLCSTR szStr1, PSLCSTR szStr2)
{
    PSLINT32            nResult;
    PSL_CONST PSLCHAR*  pch1;
    PSL_CONST PSLCHAR*  pch2;

    /*  Make sure we have work to do- we treat a NULL string as less then
     *  an empty string
     */

    if (PSLNULL == szStr1)
    {
        nResult = (PSLNULL == szStr2) ? 0 : -1;
        goto Exit;
    }
    if (PSLNULL == szStr2)
    {
        nResult = (PSLNULL == szStr1) ? 0 : 1;
        goto Exit;
    }

    /*  Walk through the string looking for a difference
     */

    pch1 = szStr1;
    pch2 = szStr2;
    for (nResult = (PSLINT32)PSLToLowerA(*pch1) - (PSLINT32)PSLToLowerA(*pch2);
            (0 == nResult) && ('\0' != *pch1) && ('\0' != *pch2);
            nResult = (PSLINT32)PSLToLowerA(*(++pch1)) -
                            (PSLINT32)PSLToLowerA(*(++pch2)))
    {
    }

Exit:
    return nResult;
}


/*
 *  PSLStringCmp
 *
 *  Compares szStr1 and szStr2
 *
 *  Arguments:
 *      PSLCWSTR        szStr1              String 1
 *      PSLCWSTR        szStr2              String 2
 *
 *  Returns:
 *      PSLINT32        < 0 - String1 < String2
 *                      = 0 - String1 == String2
 *                      > 0 - String1 > String2
 *
 */

PSLINT32 PSL_API PSLStringCmp(PSLCWSTR szStr1, PSLCWSTR szStr2)
{
    PSLINT32            nResult;
    PSL_CONST PSLWCHAR* pch1;
    PSL_CONST PSLWCHAR* pch2;

    /*  Make sure we have work to do- we treat a NULL string as less then
     *  an empty string
     */

    if (PSLNULL == szStr1)
    {
        nResult = (PSLNULL == szStr2) ? 0 : -1;
        goto Exit;
    }
    if (PSLNULL == szStr2)
    {
        nResult = (PSLNULL == szStr1) ? 0 : 1;
        goto Exit;
    }

    /*  Walk through the string looking for a difference
     */

    pch1 = szStr1;
    pch2 = szStr2;
    for (nResult = (PSLINT32)*pch1 - (PSLINT32)*pch2;
            (0 == nResult) && (L'\0' != *pch1) && (L'\0' != *pch2);
            nResult = (PSLINT32)*(++pch1) - (PSLINT32)*(++pch2))
    {
    }

Exit:
    return nResult;
}


/*
 *  PSLStringICmp
 *
 *  Compares szStr1 and szStr2 in a case insensitive fashion.
 *
 *  Arguments:
 *      PSLCWSTR        szStr1              String 1
 *      PSLCWSTR        szStr2              String 2
 *
 *  Returns:
 *      PSLINT32        < 0 - String1 < String2
 *                      = 0 - String1 == String2
 *                      > 0 - String1 > String2
 *
 */

PSLINT32 PSL_API PSLStringICmp(PSLCWSTR szStr1, PSLCWSTR szStr2)
{
    PSLINT32            nResult;
    PSL_CONST PSLWCHAR* pch1;
    PSL_CONST PSLWCHAR* pch2;

    /*  Make sure we have work to do- we treat a NULL string as less then
     *  an empty string
     */

    if (PSLNULL == szStr1)
    {
        nResult = (PSLNULL == szStr2) ? 0 : -1;
        goto Exit;
    }
    if (PSLNULL == szStr2)
    {
        nResult = (PSLNULL == szStr1) ? 0 : 1;
        goto Exit;
    }

    /*  Walk through the string looking for a difference
     */

    pch1 = szStr1;
    pch2 = szStr2;
    for (nResult = (PSLINT32)PSLToLower(*pch1) - (PSLINT32)PSLToLower(*pch2);
            (0 == nResult) && (L'\0' != *pch1) && (L'\0' != *pch2);
            nResult = (PSLINT32)PSLToLower(*(++pch1)) -
                            (PSLINT32)PSLToLower(*(++pch2)))
    {
    }

Exit:
    return nResult;
}


/*
 *  PSLStringCmpLenA
 *
 *  Compares szStr1 and szStr2 up to cchCmp characters
 *
 *  Arguments:
 *      PSLCSTR         szStr1              String 1
 *      PSLCSTR         szStr2              String 2
 *      PSLUINT32       cchCmp              Number of characters to compare
 *
 *  Returns:
 *      PSLINT32        < 0 - String1 < String2
 *                      = 0 - String1 == String2
 *                      > 0 - String1 > String2
 *
 */

PSLINT32 PSL_API PSLStringCmpLenA(PSLCSTR szStr1, PSLCSTR szStr2,
                    PSLUINT32 cchCmp)
{
    PSLINT32            nResult;
    PSL_CONST PSLCHAR*  pch1;
    PSL_CONST PSLCHAR*  pch2;

    /*  Make sure we have work to do- we treat a NULL string as less then
     *  an empty string
     */

    if (PSLNULL == szStr1)
    {
        nResult = (PSLNULL == szStr2) ? 0 : -1;
        goto Exit;
    }
    if (PSLNULL == szStr2)
    {
        nResult = (PSLNULL == szStr1) ? 0 : 1;
        goto Exit;
    }

    /*  Walk through the string looking for a difference
     */

    pch1 = szStr1;
    pch2 = szStr2;
    for (nResult = (PSLINT32)*pch1 - (PSLINT32)*pch2;
            (0 < cchCmp) && (0 == nResult) && ('\0' != *pch1) && ('\0' != *pch2);
            nResult = (PSLINT32)*(++pch1) - (PSLINT32)*(++pch2), cchCmp--)
    {
    }

Exit:
    return nResult;
}


/*
 *  PSLStringICmpLenA
 *
 *  Compares szStr1 and szStr2 in a case insensitive fashion up to cchCmp
 *  characters
 *
 *  Arguments:
 *      PSLCSTR         szStr1              String 1
 *      PSLCSTR         szStr2              String 2
 *      PSLUINT32       cchCmp              Number of characters to compare
 *
 *  Returns:
 *      PSLINT32        < 0 - String1 < String2
 *                      = 0 - String1 == String2
 *                      > 0 - String1 > String2
 *
 */

PSLINT32 PSL_API PSLStringICmpLenA(PSLCSTR szStr1, PSLCSTR szStr2,
                    PSLUINT32 cchCmp)
{
    PSLINT32            nResult;
    PSL_CONST PSLCHAR*  pch1;
    PSL_CONST PSLCHAR*  pch2;

    /*  Make sure we have work to do- we treat a NULL string as less then
     *  an empty string
     */

    if (PSLNULL == szStr1)
    {
        nResult = (PSLNULL == szStr2) ? 0 : -1;
        goto Exit;
    }
    if (PSLNULL == szStr2)
    {
        nResult = (PSLNULL == szStr1) ? 0 : 1;
        goto Exit;
    }

    /*  Walk through the string looking for a difference
     */

    pch1 = szStr1;
    pch2 = szStr2;
    for (nResult = (PSLINT32)PSLToLowerA(*pch1) - (PSLINT32)PSLToLowerA(*pch2);
            (0 < cchCmp) && (0 == nResult) && ('\0' != *pch1) && ('\0' != *pch2);
            nResult = (PSLINT32)PSLToLowerA(*(++pch1)) -
                            (PSLINT32)PSLToLowerA(*(++pch2)), cchCmp--)
    {
    }

Exit:
    return nResult;
}


/*
 *  PSLStringCmpLen
 *
 *  Compares szStr1 and szStr2 up to cchCmp characters
 *
 *  Arguments:
 *      PSLCWSTR        szStr1              String 1
 *      PSLCWSTR        szStr2              String 2
 *      PSLUINT32       cchCmp              Number of characters to compare
 *
 *  Returns:
 *      PSLINT32        < 0 - String1 < String2
 *                      = 0 - String1 == String2
 *                      > 0 - String1 > String2
 *
 */

PSLINT32 PSL_API PSLStringCmpLen(PSLCWSTR szStr1, PSLCWSTR szStr2,
                    PSLUINT32 cchCmp)
{
    PSLINT32            nResult;
    PSL_CONST PSLWCHAR* pch1;
    PSL_CONST PSLWCHAR* pch2;

    /*  Make sure we have work to do- we treat a NULL string as less then
     *  an empty string
     */

    if (PSLNULL == szStr1)
    {
        nResult = (PSLNULL == szStr2) ? 0 : -1;
        goto Exit;
    }
    if (PSLNULL == szStr2)
    {
        nResult = (PSLNULL == szStr1) ? 0 : 1;
        goto Exit;
    }

    /*  Walk through the string looking for a difference
     */

    pch1 = szStr1;
    pch2 = szStr2;
    for (nResult = (PSLINT32)*pch1 - (PSLINT32)*pch2;
            (0 < cchCmp) && (0 == nResult) && (L'\0' != *pch1) && (L'\0' != *pch2);
            nResult = (PSLINT32)*(++pch1) - (PSLINT32)*(++pch2), cchCmp--)
    {
    }

Exit:
    return nResult;
}


/*
 *  PSLStringICmpLen
 *
 *  Compares szStr1 and szStr2 in a case insensitive fashion up to cchCmp
 *  characters
 *
 *  Arguments:
 *      PSLCWSTR        szStr1              String 1
 *      PSLCWSTR        szStr2              String 2
 *      PSLUINT32       cchCmp              Number of characters to compare
 *
 *  Returns:
 *      PSLINT32        < 0 - String1 < String2
 *                      = 0 - String1 == String2
 *                      > 0 - String1 > String2
 *
 */

PSLINT32 PSL_API PSLStringICmpLen(PSLCWSTR szStr1, PSLCWSTR szStr2,
                    PSLUINT32 cchCmp)
{
    PSLINT32            nResult;
    PSL_CONST PSLWCHAR* pch1;
    PSL_CONST PSLWCHAR* pch2;

    /*  Make sure we have work to do- we treat a NULL string as less then
     *  an empty string
     */

    if (PSLNULL == szStr1)
    {
        nResult = (PSLNULL == szStr2) ? 0 : -1;
        goto Exit;
    }
    if (PSLNULL == szStr2)
    {
        nResult = (PSLNULL == szStr1) ? 0 : 1;
        goto Exit;
    }

    /*  Walk through the string looking for a difference
     */

    pch1 = szStr1;
    pch2 = szStr2;
    for (nResult = (PSLINT32)PSLToLower(*pch1) - (PSLINT32)PSLToLower(*pch2);
            (0 < cchCmp) && (0 == nResult) && (L'\0' != *pch1) && (L'\0' != *pch2);
            nResult = (PSLINT32)PSLToLower(*(++pch1)) -
                            (PSLINT32)PSLToLower(*(++pch2)), cchCmp--)
    {
    }

Exit:
    return nResult;
}

