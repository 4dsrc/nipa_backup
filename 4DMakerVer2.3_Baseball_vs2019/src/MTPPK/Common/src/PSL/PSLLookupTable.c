/*
 *  PSLLookupTable.c
 *
 *  Contains the message queue declarations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "PSL.h"
#include "PSLLookupTable.h"

typedef struct
{
    PSLHANDLE           hMutex;
    PSLUINT32           cRecordsTotal;
    PSLUINT32           cRecordsFree;
    PSLLOOKUPRECORD*    pRecords;
    PSLUINT32           idxToCmpForInsert;
} PSLLOOKUPTABLEOBJ;


PSLSTATUS PSL_API PSLLookupTableCreate(PSLUINT32 cItems,
                               PSLLOOKUPTABLE* pmlt)
{
    PSLSTATUS status;
    PSLLOOKUPTABLEOBJ* pmltObj = PSLNULL;
    if (pmlt)
    {
        *pmlt = PSLNULL;
    }

    if (PSLNULL == pmlt)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmltObj = (PSLLOOKUPTABLEOBJ*)PSLMemAlloc(PSLMEM_PTR,
                                   sizeof(PSLLOOKUPTABLEOBJ) +
                                   (sizeof(PSLLOOKUPRECORD) * cItems));
    if (PSLNULL == pmltObj)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /*  Create a mutex so that we guarantee thread safe access to the table
     */

    status = PSLMutexOpen(0, PSLNULL, &(pmltObj->hMutex));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pmltObj->cRecordsTotal  = cItems;
    pmltObj->cRecordsFree   = cItems;
    pmltObj->pRecords       = (PSLLOOKUPRECORD*)(pmltObj + 1);

    pmltObj->idxToCmpForInsert = 0;

    *pmlt = (PSLLOOKUPTABLE)pmltObj;
    pmltObj = PSLNULL;

    status = PSLSUCCESS;

Exit:
    SAFE_PSLLOOKUPTABLEDESTROY(pmltObj);
    return status;
}

PSLSTATUS PSL_API PSLLookupTableDestroy(PSLLOOKUPTABLE mlt)
{
    PSLHANDLE hMutex = PSLNULL;
    PSLSTATUS status;

    if (PSLNULL == mlt)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLMutexAcquire(((PSLLOOKUPTABLEOBJ*)mlt)->hMutex,
                            PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutex = ((PSLLOOKUPTABLEOBJ*)mlt)->hMutex;

    (PSLVOID)PSLMemFree((PSLLOOKUPTABLEOBJ*)mlt);

    status = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutex);
    return status;
}

PSLSTATUS PSL_API PSLLookupTableFindEntry(PSLLOOKUPTABLE mlt,
                                          PFNCOMPARE pfnCompare,
                                          PSLPARAM pvKey,
                                          PSLPARAM* ppvValue,
                                          PSLPARAM* ppvData)
{
    PSLHANDLE hMutex = PSLNULL;
    PSLSTATUS status;
    PSLLOOKUPRECORD* pRecord;
    PSLLOOKUPTABLEOBJ* pmltObj;

    if (ppvData)
    {
        *ppvData = PSLNULL;
    }

    if (ppvValue)
    {
        *ppvValue = PSLNULL;
    }

    if (PSLNULL == mlt || PSLNULL == ppvValue ||
        PSLNULL == ppvData || PSLNULL == pfnCompare)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmltObj = (PSLLOOKUPTABLEOBJ*)mlt;

    status = PSLMutexAcquire(pmltObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutex = pmltObj->hMutex;

    status = PSLBSearch((PSL_CONST PSLVOID*)pvKey,
                     pmltObj->pRecords,
                     pmltObj->cRecordsTotal - pmltObj->cRecordsFree,
                     sizeof(PSLLOOKUPRECORD),
                     pfnCompare,
                     (PSLVOID*)&pRecord);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* The record could be PSLNULL in case the entry
     * cannot be found in the table in which case
     * PSLSUCESS_NOT_FOUND will be the return status
     */
    if (PSLNULL != pRecord)
    {
        *ppvData    = pRecord->pvData;
        *ppvValue   = pRecord->pvValue;;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutex);
    return status;
}

PSLSTATUS PSL_API PSLLookupTableSetEntry(PSLLOOKUPTABLE mlt,
                                         PFNCOMPARE pfnCompare,
                                         PSLPARAM pvKey,
                                         PSLPARAM pvValue,
                                         PSLPARAM pvData)
{
    PSLHANDLE hMutex = PSLNULL;
    PSLSTATUS status;
    PSLLOOKUPRECORD* pRecord = PSLNULL;
    PSLLOOKUPTABLEOBJ* pmltObj = PSLNULL;

    /*
     *  mqHandler and pvData could be PSNULL in cases
     *  where only one of them need to be set
     */
    if (PSLNULL == mlt || PSLNULL == pfnCompare)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmltObj = (PSLLOOKUPTABLEOBJ*)mlt;
    PSLASSERT(PSLNULL != pmltObj);

    status = PSLMutexAcquire(pmltObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutex = pmltObj->hMutex;

    status = PSLBSearch((PSL_CONST PSLVOID*)pvKey,
                        pmltObj->pRecords,
                        pmltObj->cRecordsTotal,
                        sizeof(PSLLOOKUPRECORD),
                        pfnCompare,
                        &pRecord);

    if (PSLSUCCESS != status)
    {
        goto Exit;
    }

    if (PSLNULL != pvData)
    {
        pRecord->pvData = pvData;
    }

    if (PSLNULL != pvValue)
    {
        pRecord->pvValue = pvValue;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutex);
    return status;
}

PSLSTATUS PSL_API PSLLookupTableInsertEntry(PSLLOOKUPTABLE mlt,
                PSLPARAM pvKey, PSLPARAM pvValue,
                PSLPARAM pvData)
{
    PSLHANDLE hMutex = PSLNULL;
    PSLSTATUS status;
    PSLLOOKUPTABLEOBJ* pmltObj;
    PSLUINT32 dwIndex = 0;
    PSLUINT32 idxEntry;
    PSLUINT32 idxStartEntry;

    /* Intialization of pidxNext is intentionally avoided
     * as it is an in/out parameter
     */

    /* Param Checking */
    if (PSLNULL == mlt)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmltObj = (PSLLOOKUPTABLEOBJ*)mlt;

    status = PSLMutexAcquire(pmltObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutex = pmltObj->hMutex;

    if (pmltObj->idxToCmpForInsert > pmltObj->cRecordsTotal)
    {
        PSLASSERT(PSLFALSE);
        status = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    PSLASSERT(0 != pvKey);

    idxEntry = pmltObj->idxToCmpForInsert;

    if (0 == pmltObj->pRecords[idxEntry].pvKey)
    {
        pmltObj->pRecords[idxEntry].pvKey = pvKey;
        pmltObj->pRecords[idxEntry].pvValue = pvValue;
        pmltObj->pRecords[idxEntry].pvData = pvData;
        pmltObj->idxToCmpForInsert = idxEntry;
    }
    else if (pmltObj->pRecords[idxEntry].pvKey > pvKey)
    {
        PSLASSERT((idxEntry + 1) < pmltObj->cRecordsTotal);

        if ((idxEntry + 1) >= pmltObj->cRecordsTotal)
        {
            PSLASSERT(PSLFALSE);
            status = PSLERROR_INVALID_RANGE;
            goto Exit;
        }

        idxStartEntry = 0;

        /* Find the entry that has a key value that
         * is just greater than pvKey
         */

        for (dwIndex = idxEntry; dwIndex >= 0; dwIndex--)
        {
            if (pmltObj->pRecords[dwIndex].pvKey < pvKey)
            {
                idxStartEntry = dwIndex + 1;
                break;
            }
        }

        /* Move the entries from idxStartEntry to idxEntry + 1
         */

        for (dwIndex = idxEntry + 1; dwIndex > idxStartEntry; dwIndex--)
        {
            pmltObj->pRecords[dwIndex].pvKey =
                pmltObj->pRecords[dwIndex - 1].pvKey;
            pmltObj->pRecords[dwIndex].pvValue =
                pmltObj->pRecords[dwIndex - 1].pvValue;
            pmltObj->pRecords[dwIndex].pvData =
                pmltObj->pRecords[dwIndex - 1].pvData;
        }

        /* Add the new entry to idxStartEntry
         */

        pmltObj->pRecords[idxStartEntry].pvKey = pvKey;
        pmltObj->pRecords[idxStartEntry].pvValue = pvValue;
        pmltObj->pRecords[idxStartEntry].pvData = pvData;
        pmltObj->idxToCmpForInsert = idxEntry + 1;
    }
    else
    {
        PSLASSERT((idxEntry + 1) < pmltObj->cRecordsTotal);

        if ((idxEntry + 1) >= pmltObj->cRecordsTotal)
        {
            PSLASSERT(PSLFALSE);
            status = PSLERROR_INVALID_RANGE;
            goto Exit;
        }

        pmltObj->pRecords[idxEntry + 1].pvKey = pvKey;
        pmltObj->pRecords[idxEntry + 1].pvValue = pvValue;
        pmltObj->pRecords[idxEntry + 1].pvData = pvData;
        pmltObj->idxToCmpForInsert = idxEntry + 1;
    }

    pmltObj->cRecordsFree--;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLMUTEXRELEASE(hMutex);
    return status;
}


PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableEnum(
                            PSLLOOKUPTABLE          mlt,
                            PFNENUMLOOKUPTBLPROC    pfnEnumLookupTblProc,
                            PSLPARAM                aParamEnum)
{
    PSLHANDLE hMutex = PSLNULL;
    PSLSTATUS status;
    PSLLOOKUPTABLEOBJ* pmltObj;
    PSLUINT32 idxRecord;

    if (PSLNULL == mlt || PSLNULL == pfnEnumLookupTblProc)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmltObj = (PSLLOOKUPTABLEOBJ*)mlt;
    PSLASSERT(PSLNULL != pmltObj);

    status = PSLMutexAcquire(pmltObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutex = pmltObj->hMutex;

    for (idxRecord = 0; idxRecord < pmltObj->cRecordsTotal; idxRecord++)
    {
        status = pfnEnumLookupTblProc(pmltObj->pRecords[idxRecord].pvKey,
                            pmltObj->pRecords[idxRecord].pvValue,
                            pmltObj->pRecords[idxRecord].pvData,
                            aParamEnum);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        if (PSLSUCCESS != status)
        {
            break;
        }
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutex);
    return status;
}

