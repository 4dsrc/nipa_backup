/*
 *  PSLUtils.c
 *
 *  Contains the message queue implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"

PSLSTATUS PSL_API PSLBSearch(PSL_CONST PSLVOID*  pvKey, PSL_CONST PSLVOID*  pvStart,
                          PSLUINT32  dwNumItems, PSLUINT32  dwSizeItem,
                          PFNCOMPARE pfnCompare, PSLVOID**  ppvReturn)
{
    PSLSTATUS status;
    PSLBYTE* pbLeft;
    PSLBYTE* pbRight;
    PSLBYTE* pbMid;
    PSLUINT32 dwHalf;
    PSLINT32 nResult;
    PSLVOID* pvReturn = PSLNULL;

    if (ppvReturn)
    {
        *ppvReturn = PSLNULL;
    }

    if (PSLNULL == pvStart || PSLNULL == ppvReturn)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pbLeft = (PSLBYTE*)pvStart;
    pbRight = (PSLBYTE*)pvStart + ((dwNumItems - 1) * dwSizeItem);

    while (pbLeft <= pbRight)
    {
        if (0 != (dwHalf = (dwNumItems >> 1)))
        {
            pbMid = pbLeft + ((dwNumItems & 1 ? dwHalf : (dwHalf - 1))
                                * dwSizeItem);
            if (0 == (nResult = pfnCompare(pvKey,pbMid)))
            {
                pvReturn = pbMid;
                break;
            }
            else if (nResult < 0)
            {
                pbRight = pbMid - dwSizeItem;
                dwNumItems = (dwNumItems & 1) ? dwHalf : dwHalf - 1;
            }
            else
            {
                pbLeft = pbMid + dwSizeItem;
                dwNumItems = dwHalf;
            }
        }
        else if (dwNumItems)
        {
            pvReturn = pfnCompare(pvKey,pbLeft) ? PSLNULL : pbLeft;
            break;
        }
        else
        {
            break;
        }
    }

    *ppvReturn = pvReturn;
    pvReturn = PSLNULL;

    status = (PSLNULL == *ppvReturn) ?
                    PSLSUCCESS_NOT_FOUND : PSLSUCCESS;
Exit:
    return status;
}

