/*
 *  PSLDynamicList.c
 *
 *  Implements the PSL Dynamic List functionality
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"

/*  Local Defines
 */

#define PSLDYNLIST_COOKIE                   'DynL'

#define PSLDYNLIST_DEFAULT_GROWTH_FACTOR    32

/*  Local Types
 */

typedef struct _PSLDYNLISTOBJ
{
#ifdef PSL_ASSERTS
    PSLUINT32               dwCookie;
#endif  // PSL_ASSERTS
    PSLHANDLE               hMutex;
    PSLUINT32               cItemsMax;
    PSLUINT32               cItems;
    PSLUINT32               dwGrowBy;
    PSLPARAM*               paData;
} PSLDYNLISTOBJ;



/*
 *  _PSLDynListCompare
 *
 *  Default PSL Dynamic List comparison function.  Uses compiler provided
 *  boolean operations to determine the results
 *
 *  Arguments:
 *      PSLPARAM        aKey                Value to compare to
 *      PSLPARAM*       paItem              Value to compare
 *
 *  Returns:
 *      PSLINT32        Result of comparison:
 *                          < 0 : aKey < *paItem
 *                          = 0 : aKey == *paItem
 *                          > 0 : aKey > *paItem
 *
 */

PSLINT32 PSL_API _PSLDynListCompare(PSLPARAM aKey, PSL_CONST PSLPARAM* paItem)
{
    return (*paItem == aKey) ? 0 : ((aKey < *paItem) ? -1 : 1);
}


/*
 *  PSLDynListCreate
 *
 *  Creates a dynamic list
 *
 *  Arguments:
 *      PSLUINT32       cItems              Initial size
 *      PSLBOOL         fReserve            Reserves the number of items
 *                                            specified rather than setting
 *                                            the length of the list
 *      PSLUINT32       dwGrowBy            Growth rate
 *      PSLDYNLIST*     pdl                 Buffer for resulting list
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLDynListCreate(PSLUINT32 cItems, PSLBOOL fReserve,
                    PSLUINT32 dwGrowBy, PSLDYNLIST* pdl)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLDYNLISTOBJ*  pdloNew = PSLNULL;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pdl)
    {
        *pdl = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pdl) || (0 == dwGrowBy) ||
        (PSLDYNLIST_GROW_UNCHANGED == dwGrowBy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create a new list object
     */

    pdloNew = (PSLDYNLISTOBJ*)PSLMemAlloc(PSLMEM_PTR, sizeof(PSLDYNLISTOBJ));
    if (PSLNULL == pdloNew)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }
#ifdef PSL_ASSERTS
    pdloNew->dwCookie = PSLDYNLIST_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Create a mutex for thread safety
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, PSLNULL, &pdloNew->hMutex);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pdloNew->hMutex;

    /*  Set the initial size
     */

    ps = PSLDynListSetSize(pdloNew, !fReserve ? cItems : 0, dwGrowBy);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Reserve space if requested
     */

    if (fReserve)
    {
        ps = PSLDynListReserveSize(pdloNew,
                            (0 != cItems) ? cItems : pdloNew->dwGrowBy,
                            PSLTRUE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Return the list and report success
     */

    *pdl = pdloNew;
    pdloNew = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLDYNLISTDESTROY(pdloNew);
    return ps;
}


/*
 *  PSLDynListDestroy
 *
 *  Destroys a dynamic list
 *
 *  Arguments:
 *      PSLDYNLIST      dl                  List to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLDynListDestroy(PSLDYNLIST dlDestroy)
{
    PSLHANDLE       hMutexClose = PSLNULL;
    PSLSTATUS       ps;
    PSLDYNLISTOBJ*  pdlo;

    /*  Validate arguments
     */

    if (PSLNULL == dlDestroy)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pdlo = (PSLDYNLISTOBJ*)dlDestroy;
    PSLASSERT(PSLDYNLIST_COOKIE == pdlo->dwCookie);

    /*  Acquire the mutex for thread safety
     */

    ps = PSLMutexAcquire(pdlo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexClose = pdlo->hMutex;

    /*  And release
     */

    SAFE_PSLMEMFREE(pdlo->paData);
    SAFE_PSLMEMFREE(pdlo);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXCLOSE(hMutexClose);
    return ps;
}


/*
 *  PSLDynListGetSize
 *
 *  Returns the current size of the list
 *
 *  Arguments:
 *      PSLDYNLIST      dl                  List to return size of
 *      PSLUINT32*      pcItems             Return buffer for size
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLDynListGetSize(PSLDYNLIST dl, PSLUINT32* pcItems)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    PSLDYNLISTOBJ*  pdlo;

    /*  Clear result for safety
     */

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == dl) || (PSLNULL == pcItems))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pdlo = (PSLDYNLISTOBJ*)dl;
    PSLASSERT(PSLDYNLIST_COOKIE == pdlo->dwCookie);

    /*  Acquire the mutex for thread safety
     */

    ps = PSLMutexAcquire(pdlo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pdlo->hMutex;

    /*  Return the size
     */

    *pcItems = pdlo->cItems;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLDynListSetSize
 *
 *  Sets the size of the list and optionally modifies the the grow by
 *  size.
 *
 *  Arguments:
 *      PSLDYNLIST      dl                  List to modify
 *      PSLUINT32       cItems              New count of items
 *      PSLUINT32       dwGrowBy            Growth rate
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API PSLDynListSetSize(PSLDYNLIST dl, PSLUINT32 cItems,
                    PSLUINT32 dwGrowBy)
{
    PSLUINT32       cItemsDesired;
    PSLUINT32       cItemsMax;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    PSLDYNLISTOBJ*  pdlo;
    PSLPARAM*       paData = PSLNULL;

    /*  Validate Arguments
     */

    if ((PSLNULL == dl) || (0 == dwGrowBy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Reject indices that are out of range
     */

    if (PSLDYNLIST_MAX_ITEMS <= cItems)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Save some redirection
     */

    pdlo = (PSLDYNLISTOBJ*)dl;
    PSLASSERT(PSLDYNLIST_COOKIE == pdlo->dwCookie);

    /*  Acquire the mutex for thread safety
     */

    ps = PSLMutexAcquire(pdlo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pdlo->hMutex;

    /*  Determine the actual growth factor
     */

    switch (dwGrowBy)
    {
    case PSLDYNLIST_GROW_DEFAULT:
        dwGrowBy = PSLDYNLIST_DEFAULT_GROWTH_FACTOR;
        break;

    case PSLDYNLIST_GROW_UNCHANGED:
        /*  This is not allowed if we do not have a list
         */

        if (PSLNULL == pdlo->paData)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        /*  Use the existing grow by factor
         */

        dwGrowBy = pdlo->dwGrowBy;
        break;

    default:
        /*  Make sure the growth rate is less then the maximum number of items
         *  allowed in the list
         */

        if (PSLDYNLIST_MAX_ITEMS <= dwGrowBy)
        {
            ps = PSLERROR_INVALID_RANGE;
            goto Exit;
        }
        break;
    }

    /*  Determine the number of items desired in the list and the maximum
     *  number of items that would be in the list
     *
     *  NOTE: We do not force the list to grow unless it must to accomidate
     *  the new item count unless there is no list at all.
     */

    cItemsMax = max(cItems, pdlo->cItems) + dwGrowBy;
    cItemsDesired = cItems + ((PSLNULL != pdlo->paData) ? 0 : dwGrowBy);
    if ((max(cItems, pdlo->cItems) > cItemsMax) || (cItems > cItemsDesired))
    {
        cItemsMax = PSLDYNLIST_MAX_ITEMS;
        cItemsDesired = cItems;
    }

    /*  See if we need to grow the array to accomidate the new item
     */

    PSLASSERT((PSLNULL != pdlo->paData) || (0 == pdlo->cItemsMax));
    if (cItemsDesired > pdlo->cItemsMax)
    {
        /*  Need to update the size
         */

        paData = PSLMemReAlloc(pdlo->paData, (cItemsMax * sizeof(PSLPARAM)),
                            (PSLMEM_FIXED | PSLMEM_RELOCATABLE));
        if (PSLNULL == paData)
        {
            ps = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }
        pdlo->paData = paData;
        pdlo->cItemsMax = cItemsMax;
        paData = PSLNULL;
    }

    /*  And complete the set
     */

    pdlo->cItems = cItems;
    pdlo->dwGrowBy = dwGrowBy;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMEMFREE(paData);
    return ps;
}


/*
 *  PSLDynListReserveSize
 *
 *  Reserves space in the array for the specified number of items but
 *  does not force the length of the array to be that size
 *
 *  Arguments:
 *      PSLDYNLIST      dl                  List to modify
 *      PSLUINT32       cReserve            Items to reserve
 *      PSLBOOL         fTotal              PSLTRUE if reservation represents
 *                                            a total count, PSLFALSE if
 *                                            it is additive
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLDynListReserveSize(PSLDYNLIST dl, PSLUINT32 cReserve,
                    PSLBOOL fTotal)
{
    PSLUINT32       cItemsDesired;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    PSLDYNLISTOBJ*  pdlo;
    PSLPARAM*       paData = PSLNULL;

    /*  Validate Arguments
     */

    if ((PSLNULL == dl) || (0 == cReserve))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Reject indices that are out of range
     */

    if (fTotal && (PSLDYNLIST_MAX_ITEMS <= cReserve))
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Save some redirection
     */

    pdlo = (PSLDYNLISTOBJ*)dl;
    PSLASSERT(PSLDYNLIST_COOKIE == pdlo->dwCookie);

    /*  Acquire the mutex for thread safety
     */

    ps = PSLMutexAcquire(pdlo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pdlo->hMutex;

    /*  Determine the number of desired items
     */

    cItemsDesired = fTotal ? cReserve : (pdlo->cItems + cReserve);

    /*  Check additive reserves to make sure that we do not exceed the
     *  maximum allowed if we are doing an additive reserve
     */

    if (!fTotal && ((cItemsDesired < pdlo->cItems) ||
                            (PSLDYNLIST_MAX_ITEMS <= cItemsDesired)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  See if we have any work to do
     */

    if (cItemsDesired < pdlo->cItemsMax)
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Align the number of desired items to the next multiple of the growth
     *  factor to keep things nice
     */

    cItemsDesired = PSLAllocAlign(cItemsDesired, pdlo->dwGrowBy);

    /*  And update the size of the array
     */

    paData = PSLMemReAlloc(pdlo->paData, (cItemsDesired* sizeof(PSLPARAM)),
                        (PSLMEM_FIXED | PSLMEM_RELOCATABLE));
    if (PSLNULL == paData)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }
    pdlo->paData = paData;
    pdlo->cItemsMax = cItemsDesired;
    paData = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMEMFREE(paData);
    return ps;
}


/*
 *  PSLDynListGetItem
 *
 *  Retrieves an item from the list
 *
 *  Arguments:
 *      PSLDYNLIST      dl                  List to retrieve item from
 *      PSLUINT32       idxItem             Index of item to retrieve
 *      PSLPARAM*       paItem              Return buffer for item
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLDynListGetItem(PSLDYNLIST dl, PSLUINT32 idxItem,
                    PSLPARAM* paItem)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    PSLDYNLISTOBJ*  pdlo;

    /*  Clear result for safety
     */

    if (PSLNULL != paItem)
    {
        *paItem = PSLNULL;

    }

    /*  Validate Arguments
     */

    if ((PSLNULL == dl) || (PSLNULL == paItem))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pdlo = (PSLDYNLISTOBJ*)dl;
    PSLASSERT(PSLDYNLIST_COOKIE == pdlo->dwCookie);

    /*  Acquire the mutex for thread safety
     */

    ps = PSLMutexAcquire(pdlo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pdlo->hMutex;

    /*  Validate indices
     */

    if (idxItem >= pdlo->cItems)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Return the desired entry
     */

    *paItem = pdlo->paData[idxItem];
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLDynListAddItem
 *
 *  Adds the specified item at the end of the list
 *
 *  Arguments:
 *      PSLDYNLIST      dl                  List to add item to
 *      PSLPARAM        paItem              Item to add
 *      PSLUINT32       pidxItem            Return buffer for resulting item
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLDynListAddItem(PSLDYNLIST dl, PSLPARAM paItem,
                    PSLUINT32* pidxItem)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLUINT32       idxItem;
    PSLSTATUS       ps;
    PSLDYNLISTOBJ*  pdlo;

    /*  Clear result for safety
     */

    if (PSLNULL != pidxItem)
    {
        *pidxItem = 0;
    }

    /*  Validate Arguments
     */

    if (PSLNULL == dl)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pdlo = (PSLDYNLISTOBJ*)dl;
    PSLASSERT(PSLDYNLIST_COOKIE == pdlo->dwCookie);

    /*  Acquire the mutex for thread safety
     */

    ps = PSLMutexAcquire(pdlo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pdlo->hMutex;

    /*  Set the new size as the new item
     */

    idxItem = pdlo->cItems;
    ps = PSLDynListSetSize(dl, (idxItem + 1), PSLDYNLIST_GROW_UNCHANGED);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /* And assign it
     */

    pdlo->paData[idxItem] = paItem;

    /*  Return index if requested
     */

    if (PSLNULL != pidxItem)
    {
        *pidxItem = idxItem;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLDynListSetItem
 *
 *  Sets the specified item
 *
 *  Arguments:
 *      PSLDYNLIST      dl                  List to set item in
 *      PSLUINT32       idxItem             Index of item to set
 *      PSLPARAM        aItem              Item to set
 *
 *  Returns:
 *      PSLSUCCESS      PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLDynListSetItem(PSLDYNLIST dl, PSLUINT32 idxItem,
                    PSLPARAM aItem)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    PSLDYNLISTOBJ*  pdlo;

    /*  Validate Arguments
     */

    if (PSLNULL == dl)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pdlo = (PSLDYNLISTOBJ*)dl;
    PSLASSERT(PSLDYNLIST_COOKIE == pdlo->dwCookie);

    /*  Acquire the mutex for thread safety
     */

    ps = PSLMutexAcquire(pdlo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pdlo->hMutex;

    /*  Make sure there is space in the list to hold the item
     */

    ps = PSLDynListSetSize(dl, max(pdlo->cItems, idxItem),
                            PSLDYNLIST_GROW_UNCHANGED);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /* And assign it
     */

    pdlo->paData[idxItem] = aItem;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLDynListInsertItem
 *
 *  Inserts an item into the list
 *
 *  Arguments:
 *      PSLDYNLIST      dl                  List to insert into
 *      PSLUINT32       idxItem             Location to insert item at
 *      PSLPARAM        aItem               Item to insert
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API PSLDynListInsertItem(PSLDYNLIST dl, PSLUINT32 idxItem,
                    PSLPARAM aItem)
{
    PSLUINT32       cItemsMove;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    PSLDYNLISTOBJ*  pdlo;

    /*  Validate Arguments
     */

    if (PSLNULL == dl)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pdlo = (PSLDYNLISTOBJ*)dl;
    PSLASSERT(PSLDYNLIST_COOKIE == pdlo->dwCookie);

    /*  Acquire the mutex for thread safety
     */

    ps = PSLMutexAcquire(pdlo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pdlo->hMutex;

    /*  Make sure there is space in the list to hold the item
     */

    ps = PSLDynListSetSize(dl, (pdlo->cItems + 1), PSLDYNLIST_GROW_UNCHANGED);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make space in the data field for the item
     *
     *  NOTE- size has already been adjusted for the new item so we need
     *  to ignore it in the move
     */

    cItemsMove = (idxItem < pdlo->cItems) ? (pdlo->cItems - idxItem) : 0;
    PSLMoveMemory(&(pdlo->paData[idxItem + 1]), &(pdlo->paData[idxItem]),
                            (cItemsMove * sizeof(pdlo->paData[0])));

    /*  And assign it
     */

    pdlo->paData[idxItem] = aItem;
    pdlo->cItems++;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLDynListRemoveItem
 *
 *  Removes items from the list
 *
 *  Arguments:
 *      PSLDYNLIST      dl                  List to remove items from
 *      PSLUINT32       idxItem             Starting item to remove
 *      PSLUINT32       cItems              Number of items to remove
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLDynListRemoveItem(PSLDYNLIST dl, PSLUINT32 idxItem,
                    PSLUINT32 cItems)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLUINT32       idxEnd;
    PSLSTATUS       ps;
    PSLDYNLISTOBJ*  pdlo;

    /*  Validate Arguments
     */

    if ((PSLNULL == dl) || (0 == cItems))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pdlo = (PSLDYNLISTOBJ*)dl;
    PSLASSERT(PSLDYNLIST_COOKIE == pdlo->dwCookie);

    /*  Acquire the mutex for thread safety
     */

    ps = PSLMutexAcquire(pdlo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pdlo->hMutex;

    /*  Validate indices
     */

    idxEnd = idxItem + cItems;
    if ((idxItem >= pdlo->cItems) || (idxEnd < idxItem) ||
        (idxEnd > pdlo->cItems) || (PSLDYNLIST_MAX_ITEMS <= idxEnd))
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Remove the entries
     */

    PSLMoveMemory(&(pdlo->paData[idxItem]), &(pdlo->paData[idxEnd]),
                            ((pdlo->cItems - idxEnd) * sizeof(pdlo->paData[0])));
    pdlo->cItems -= cItems;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLDynListFindItem
 *
 *  Performs a linear search on the list for the specified item
 *
 *
 *  Arguments:
 *      PSLDYNLIST      dl                  List to search
 *      PSLUINT32       idxStart            Location to start search
 *      PSLPARAM        aKey                Value to match
 *      PFNDYNLISTCOMPARE
 *                      pfnCompare          Comparison function to use
 *      PSLUINT32*      pidxItem            Location of item
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if found, PSLSUCCESS_NOT_FOUND if not
 *
 */

PSLSTATUS PSL_API PSLDynListFindItem(PSLDYNLIST dl, PSLUINT32 idxStart,
                    PSLPARAM aKey, PFNDYNLISTCOMPARE pfnCompare,
                    PSLUINT32* pidxItem)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLUINT32       idxCur;
    PSLSTATUS       ps;
    PSLDYNLISTOBJ*  pdlo;

    /*  Clear result for safety
     */

    if (PSLNULL != pidxItem)
    {
        *pidxItem = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == dl) || (PSLNULL == pidxItem))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If the no comparison function was provided than use the default
     *  comparison function which is numeric in nature
     */

    if (PSLNULL == pfnCompare)
    {
        pfnCompare = _PSLDynListCompare;
    }

    /*  Save some redirection
     */

    pdlo = (PSLDYNLISTOBJ*)dl;
    PSLASSERT(PSLDYNLIST_COOKIE == pdlo->dwCookie);

    /*  Acquire the mutex for thread safety
     */

    ps = PSLMutexAcquire(pdlo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pdlo->hMutex;

    /*  Validate the starting location
     */

    if (idxStart >= pdlo->cItems)
    {
        ps = (0 == pdlo->cItems) ? PSLSUCCESS_NOT_FOUND : PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Walk through the list looking for the matching entry
     */

    for (idxCur = idxStart;
            (idxCur < pdlo->cItems) &&
                    (0 != pfnCompare(aKey, &(pdlo->paData[idxCur])));
            idxCur++)
    {
    }

    /*  If we did not find a match report it
     */

    if (idxCur == pdlo->cItems)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Return the location
     */

    *pidxItem = idxCur;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLDynListBSearch
 *
 *  Performas a binary search on the items in the list
 *
 *  Arguments:
 *      PSLDYNLIST      dl                  Dynamic list to search
 *      PSLPARAM        aKey                Key to search for in list
 *      PFNDYNLISTCOMPARE
 *                      pfnCompare          Item comparison function
 *      PSLUINT32*      pidxItem            Resulting item in the list
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if found, PSLSUCCESS_NOT_FOUND if not
 *
 */

PSLSTATUS PSL_API PSLDynListBSearch(PSLDYNLIST dl, PSLPARAM aKey,
                    PFNDYNLISTCOMPARE pfnCompare, PSLUINT32* pidxItem)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLPARAM*       paResult;
    PSLSTATUS       ps;
    PSLDYNLISTOBJ*  pdlo;

    /*  Clear result for safety
     */

    if (PSLNULL != pidxItem)
    {
        *pidxItem = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == dl) || (PSLNULL == pidxItem))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If the no comparison function was provided than use the default
     *  comparison function which is numeric in nature
     */

    if (PSLNULL == pfnCompare)
    {
        pfnCompare = _PSLDynListCompare;
    }

    /*  Save some redirection
     */

    pdlo = (PSLDYNLISTOBJ*)dl;
    PSLASSERT(PSLDYNLIST_COOKIE == pdlo->dwCookie);

    /*  Acquire the mutex for thread safety
     */

    ps = PSLMutexAcquire(pdlo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pdlo->hMutex;

    /*  Perform the binary search for the item
     */

    ps = PSLBSearch((PSLVOID*)aKey, pdlo->paData, pdlo->cItems,
                            sizeof(pdlo->paData[0]), (PFNCOMPARE)pfnCompare,
                            (PSLVOID*)&paResult);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the location
     */

    *pidxItem = paResult - pdlo->paData;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}

