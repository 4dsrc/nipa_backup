/*
 *  PSLContextList.c
 *
 *  Contains definitions of the PSL Context List engine
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"

/*  Local defines
 */

#define PSLCTXLIST_COOKIE           'CtxL'

#define PSLCTXLISTCONTEXT_UNKNOWN   0

#define SAFE_PSLCTXLISTCONTEXTDESTROY(_pclo, _ac) \
if ((PSLNULL != (_pclo)) && (PSLCTXLISTCONTEXT_UNKNOWN != (_ac))) \
{ \
    _PSLCtxListContextDestroy(_pclo, _ac); \
    _ac = PSLCTXLISTCONTEXT_UNKNOWN; \
}


/*  Local Types
 */

typedef struct _PSLCTXLISTOBJ
{
#ifdef PSL_ASSERTS
    PSLUINT32               dwCookie;
#endif  /* PSL_ASSERTS */
    PSLHANDLE               hMutex;
    PSLDYNLIST              dlDestList;
    PSLBKTHASH              bkhContexts;
} PSLCTXLISTOBJ;

/*  Local Functions
 */

static PSLPARAM _PSLCtxListGetNextContextID();

static PSLSTATUS _PSLCtxListObjectDestroy(PSLCTXLISTOBJ* pclo);

static PSLSTATUS _PSLCtxListContextCreate(PSLCTXLISTOBJ* pclo,
                    PSLPARAM* paContext);

static PSLSTATUS _PSLCtxListContextDestroy(PSLCTXLISTOBJ* pclo,
                    PSLPARAM aContext);

static PSLSTATUS _PSLCtxListContextGetNextEntry(PSLCTXLISTOBJ* pclo,
                    PSLPARAM aContext,
                    PSLPARAM* paParamNext);

static PSLSTATUS _PSLCtxListCleanContexts(PSLCTXLISTOBJ* pclo,
                    PSLPARAM aParamRemoved);

/*  Local Variables
 */

static PSLUINT32            _DwNextContextID = 1;


/*
 *  _PSLCtxListGetNextContextID
 *
 *  Returns the next context ID to use
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLPARAM        Next Context ID
 *
 */

PSLPARAM _PSLCtxListGetNextContextID()
{
    PSLPARAM        aResult;

    aResult = _DwNextContextID;
    _DwNextContextID++;
    if (PSLCTXLISTCONTEXT_UNKNOWN == _DwNextContextID)
    {
        _DwNextContextID++;
    }
    return aResult;
}


/*
 *  _PSLCtxListObjectDestroy
 *
 *  Destroys a context list object
 *
 *  Arguments:
 *      PSLCTXLISTOBJ*  pclo                 Object to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _PSLCtxListObjectDestroy(PSLCTXLISTOBJ* pclo)
{
    PSLPARAM        aKey;
    PSLDYNLIST      dlContext;
#ifdef PSL_ASSERTS
    PSLUINT32       cItems;
#endif  // PSL_ASSERTS
    PSLHANDLE       hMutexClose = PSLNULL;
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if (PSLNULL == pclo)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Acquire the mutex
     */

    if (PSLNULL != pclo->hMutex)
    {
        ps = PSLMutexAcquire(pclo->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexClose = pclo->hMutex;
    }

    /*  See if we have a context collection to deal with
     */

    if (PSLNULL != pclo->bkhContexts)
    {
        /*  Walk through the context list and release each context
         *
         *  NOTE: The hash cursor resets every time an object is removed.
         *  Since we are pulling out objects we can always just get the top
         *  of the list to deal with the next entry that needs to be released.
         */

        for (ps = PSLBktHashGetFirstEntry(pclo->bkhContexts, &aKey, PSLNULL);
                PSLSUCCESS == ps;
                ps = PSLBktHashGetFirstEntry(pclo->bkhContexts, &aKey, PSLNULL))
        {
            /*  Remove the entry from the hash
             */

            ps = PSLBktHashRemove(pclo->bkhContexts, aKey,
                            (PSLPARAM*)&dlContext);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  And release the context
             */

            ps = PSLDynListDestroy(dlContext);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
    }

    /*  Release the destination list and the hash- both should be empty at this
     *  point
     */

    PSLASSERT((PSLNULL == pclo->dlDestList) ||
        (PSL_SUCCEEDED(PSLDynListGetSize(pclo->dlDestList, &cItems)) &&
            (0 == cItems)));
    PSLASSERT((PSLNULL == pclo->bkhContexts) ||
        (PSL_SUCCEEDED(PSLBktHashGetSize(pclo->bkhContexts, &cItems)) &&
            (0 == cItems)));
    SAFE_PSLDYNLISTDESTROY(pclo->dlDestList);
    SAFE_PSLBKTHASHDESTROY(pclo->bkhContexts);

    /*  And delete the object
     */

    PSLMemFree(pclo);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXCLOSE(hMutexClose);
    return ps;
}


/*
 *  _PSLCtxListContextCreate
 *
 *  Creates a context.  A context represents the state of the list at a
 *  particular point in time.  While the context is alive any destinations
 *  that get removed are also removed from the context if they are still
 *  present in the context.  This provides a thread safe way to manage
 *  the state of a list
 *
 *  Arguments:
 *      PSLCTXLISTOBJ*  pclo                ContextList object to create
 *                                            a context in
 *      PSLPARAM*       paContext           Return buffer for the ID of the
 *                                            context that was created
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_NOT_AVAILABLE if
 *                        no destinations are present
 *
 */

PSLSTATUS _PSLCtxListContextCreate(PSLCTXLISTOBJ* pclo,
                    PSLPARAM* paContext)
{
    PSLPARAM        aContextID;
    PSLPARAM        aDest;
    PSLUINT32       cItems;
    PSLDYNLIST      dlContext = PSLNULL;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLUINT32       idxDest;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != paContext)
    {
        *paContext = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pclo) || (PSLNULL == paContext))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Acquire the mutex for this object
     */

    ps = PSLMutexAcquire(pclo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pclo->hMutex;

    /*  Determine the number of items currently registered
     */

    ps = PSLDynListGetSize(pclo->dlDestList, &cItems);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we can create a context
     */

    if (0 == cItems)
    {
        ps = PSLSUCCESS_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Create a new context
     */

    ps = PSLDynListCreate(cItems, PSLFALSE, PSLDYNLIST_GROW_DEFAULT, &dlContext);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Add the current destinations to the newly created context
     */

    for (idxDest = 0; idxDest < cItems; idxDest++)
    {
        ps = PSLDynListGetItem(pclo->dlDestList, idxDest, &aDest);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        ps = PSLDynListSetItem(dlContext, idxDest, aDest);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Determine the context id and add the list to the context hash
     */

    aContextID = _PSLCtxListGetNextContextID();
    ps = PSLBktHashInsert(pclo->bkhContexts, aContextID, (PSLPARAM)dlContext);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Transfer ownership of the context to the hash
     */

    dlContext = PSLNULL;

    /*  Return the context ID and report success
     */

    *paContext = aContextID;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLDYNLISTDESTROY(dlContext);
    return ps;
}


/*
 *  _PSLCtxListContextDestroy
 *
 *  Destroys the context specified
 *
 *  Arguments:
 *      PSLCTXLISTOBJ*  pclo                ContextList object context lives in
 *      PSLPARAM        aContext            Context ID
 *
 *  Returns:
 *      PSLSUCCESS      PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if not found
 *
 */

PSLSTATUS _PSLCtxListContextDestroy(PSLCTXLISTOBJ* pclo, PSLPARAM aContext)
{
    PSLDYNLIST      dlContext = PSLNULL;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if ((PSLNULL == pclo) || (PSLCTXLISTCONTEXT_UNKNOWN == aContext))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Acquire the mutex for this object
     */

    ps = PSLMutexAcquire(pclo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pclo->hMutex;

    /*  Locate the context in the hash
     */

    ps = PSLBktHashRemove(pclo->bkhContexts, aContext, (PSLPARAM*)&dlContext);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    /*  There is no work to do at the moment other than destroying the list
     *
     *  NOTE: We do not destroy the list here since it may cause a heap
     *  compaction- we let it get cleaned up on exit after the mutex has
     *  been released.
     */

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLDYNLISTDESTROY(dlContext);
    return ps;
}


/*
 *  _PSLCtxListContextGetNextEntry
 *
 *  Returns the next entry in the context
 *
 *  Arguments:
 *      PSLCTXLISTOBJ*  pclo                ContextList object context lives in
 *      PSLPARAM        aParam              Context to change
 *      PSLPARAM*       paParamNext         Next param in the list
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if
 *                        no entries remain
 *
 */

PSLSTATUS _PSLCtxListContextGetNextEntry(PSLCTXLISTOBJ* pclo, PSLPARAM aContext,
                    PSLPARAM* paParamNext)
{
    PSLUINT32       cItems;
    PSLDYNLIST      dlContext;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if ((PSLNULL == pclo) || (PSLCTXLISTCONTEXT_UNKNOWN == aContext))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Acquire the mutex for this object
     */

    ps = PSLMutexAcquire(pclo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pclo->hMutex;

    /*  Locate the context in the hash
     */

    ps = PSLBktHashFind(pclo->bkhContexts, aContext, (PSLPARAM*)&dlContext);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    /*  Retrieve the size of the context list
     */

    ps = PSLDynListGetSize(dlContext, &cItems);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  If no entries remain in the context report not found
     */

    if (0 == cItems)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Return the next context at the end of the list- we do it in this order
     *  since we are going to remove the entry from the list and this avoids
     *  the list having to shift items if we remove one from the front of the
     *  list
     */

    ps = PSLDynListGetItem(dlContext, (cItems - 1), paParamNext);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And remove it
     */

    ps = PSLDynListRemoveItem(dlContext, (cItems - 1), 1);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  _PSLCtxListCleanContexts
 *
 *  Searches all of the current context for the given message queue and
 *  removes it if found
 *
 *  Arguments:
 *      PSLCTXLISTOBJ*  pclo                ContextList object to operate in
 *      PSLPARAM        aParamRemoved       Message queue to remove
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _PSLCtxListCleanContexts(PSLCTXLISTOBJ* pclo, PSLPARAM aParamRemoved)
{
    PSLPARAM        aKey;
    PSLDYNLIST      dlContext;
    PSLUINT32       idxItem;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if ((PSLNULL == pclo) || (PSLNULL == aParamRemoved))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Acquire the mutex for this object
     */

    ps = PSLMutexAcquire(pclo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pclo->hMutex;

    /*  Loop through each of the active contexts to search for the parameter
     */

    for (ps = PSLBktHashGetFirstEntry(pclo->bkhContexts, &aKey,
                                (PSLPARAM*)&dlContext);
            PSLSUCCESS == ps;
            ps = PSLBktHashGetNextEntry(pclo->bkhContexts, &aKey,
                                (PSLPARAM*)&dlContext))
    {
        /*  Search the context list for the param that has been removed
         */

        ps = PSLDynListFindItem(dlContext, 0, aParamRemoved, PSLNULL, &idxItem);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSLSUCCESS != ps)
        {
            continue;
        }

        /*  And remove the entry from the context
         */

        ps = PSLDynListRemoveItem(dlContext, idxItem, 1);
        PSLASSERT(PSL_SUCCEEDED(ps));
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLCtxListCreate
 *
 *  Create a notifier
 *
 *  Arguments:
 *      PSLCTXLIST*     pclist              Return buffer for notifier
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLCtxListCreate(PSLCTXLIST* pclist)
{
    PSLCTXLISTOBJ*  pcloNew = PSLNULL;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pclist)
    {
        *pclist = PSLNULL;
    }

    /*  Validate arguments
     */

    if (PSLNULL == pclist)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate a new context list object
     */

    pcloNew = (PSLCTXLISTOBJ*)PSLMemAlloc(PSLMEM_PTR, sizeof(*pcloNew));
    if (PSLNULL == pcloNew)
    {
         ps = PSLERROR_OUT_OF_MEMORY;
         goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Add the cookie
     */

    pcloNew->dwCookie = PSLCTXLIST_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  And create a mutex
     */

    ps = PSLMutexOpen(PSLFALSE, PSLNULL, &(pcloNew->hMutex));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create the destination list and the context hash
     */

    ps = PSLDynListCreate(0, PSLFALSE, PSLDYNLIST_GROW_DEFAULT,
                            &(pcloNew->dlDestList));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = PSLBktHashCreate(16, PSLNULL, 0, &(pcloNew->bkhContexts));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Transfer ownership of the context list to the caller
     */

    *pclist = pcloNew;
    pcloNew = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLCTXLISTDESTROY(pcloNew);
    return ps;
}


/*
 *  PSLCtxListDestroy
 *
 *  Closes a notifier
 *
 *  Arguments:
 *      PSLCTXLIST      clist               Context List to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLCtxListDestroy(PSLCTXLIST clist)
{
    PSLCTXLISTOBJ*  pclo;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == clist)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the clist object
     */

    pclo = (PSLCTXLISTOBJ*)clist;
    PSLASSERT(PSLCTXLIST_COOKIE == pclo->dwCookie);

    /*  And handle the destroy
     */

    ps = _PSLCtxListObjectDestroy(pclo);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  PSLCtxListRegister
 *
 *  Registers a valid with the context list
 *
 *  Arguments:
 *      PSLCTXLIST      clist               Context List to add queue to
 *      PSLPARAM        aParam              Value to add to the list
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_EXISTS if already
 *                        registered
 *
 */

PSLSTATUS PSL_API PSLCtxListRegister(PSLCTXLIST clist, PSLPARAM aParam)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLUINT32       idxItem;
    PSLCTXLISTOBJ*  pclo;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == clist)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the context list object
     */

    pclo = (PSLCTXLISTOBJ*)clist;
    PSLASSERT(PSLCTXLIST_COOKIE == pclo->dwCookie);

    /*  And acquire the mutex
     */

    ps = PSLMutexAcquire(pclo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pclo->hMutex;

    /*  See if this value is already registered
     */

    ps = PSLDynListFindItem(pclo->dlDestList, 0, aParam, PSLNULL, &idxItem);
    if (PSLSUCCESS_NOT_FOUND != ps)
    {
        /*  Make sure we remap found to PSLSUCCESS_EXISTS if we do no work
         */

        ps = (PSL_FAILED(ps)) ? ps : PSLSUCCESS_EXISTS;
        goto Exit;
    }

    /*  Add the value to the list
     */

    ps = PSLDynListAddItem(pclo->dlDestList, aParam, PSLNULL);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLCtxListUnregister
 *
 *  Removes the specified value from the list
 *
 *  Arguments:
 *      PSLCTXLIST      clist               Context List to remove queue from
 *      PSLPARAM        aParam              Queue to remove
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if removed, PSLSUCCESS_NOT_FOUND if not found
 *
 */

PSLSTATUS PSL_API PSLCtxListUnregister(PSLCTXLIST clist, PSLPARAM aParam)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLUINT32       idxItem;
    PSLCTXLISTOBJ*  pclo;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == clist)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the context list object
     */

    pclo = (PSLCTXLISTOBJ*)clist;
    PSLASSERT(PSLCTXLIST_COOKIE == pclo->dwCookie);

    /*  And acquire the mutex
     */

    ps = PSLMutexAcquire(pclo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pclo->hMutex;

    /*  Attempt to locate the value
     */

    ps = PSLDynListFindItem(pclo->dlDestList, 0, aParam, PSLNULL, &idxItem);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    /*  Remove the value from the list of registered entries
     */

    ps = PSLDynListRemoveItem(pclo->dlDestList, idxItem, 1);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And update any active contexts
     */

    ps = _PSLCtxListCleanContexts(pclo, aParam);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLCtxListEnum
 *
 *  Enumerates the elements in a context list while preserving the state of
 *  the list at the time the call to PSLCtxListEnum was made
 *
 *  Arguments:
 *      PSLCTXLIST      clist               Context List to enumerate
 *      PFNENUMCTXLISTPROC
 *                      pfnEnumCtxListProc  Function to call for each item
 *                                            in the context
 *      PSLPARAM        aEnumParam          Enumeration parameter
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE if no calls
 *                        succeeded
 *
 */

PSLSTATUS PSL_API PSLCtxListEnum(PSLCTXLIST clist,
                    PFNENUMCTXLISTPROC pfnEnumCtxListProc,
                    PSLPARAM aEnumParam)
{
    PSLPARAM        aContext = PSLCTXLISTCONTEXT_UNKNOWN;
    PSLBOOL         fCallSucceeded = PSLFALSE;
    PSLBOOL         fDone;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLPARAM        aParam;
    PSLCTXLISTOBJ*  pclo = PSLNULL;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == clist || PSLNULL == pfnEnumCtxListProc)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the context list object
     */

    pclo = (PSLCTXLISTOBJ*)clist;
    PSLASSERT(PSLCTXLIST_COOKIE == pclo->dwCookie);

    /*  And acquire the mutex
     */

    ps = PSLMutexAcquire(pclo->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pclo->hMutex;

    /*  Create a context for this enumeration
     */

    ps = _PSLCtxListContextCreate(pclo, &aContext);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    /*  Walk through the context and enumerate the values
     */

    for (fDone = PSLFALSE,
                ps = _PSLCtxListContextGetNextEntry(pclo, aContext, &aParam);
            !fDone && (PSLSUCCESS == ps);
            ps = _PSLCtxListContextGetNextEntry(pclo, aContext, &aParam))
    {
        /*  Release the mutex so that we do not cause deadlocks
         */

        SAFE_PSLMUTEXRELEASE(hMutexRelease);

        /*  Call the enumeration procedure
         */

        ps = pfnEnumCtxListProc(aParam, aEnumParam);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Remember that we have at least one call that succeeded and see
         *  if we are to stop enumeration
         */

        fCallSucceeded = PSLTRUE;
        fDone = PSLSUCCESS_CANCEL == ps;

        /*  Re-acqiure the mutex before moving on to the next entry in the
         *  context
         */

        ps = PSLMutexAcquire(pclo->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexRelease = pclo->hMutex;
    }

    /*  Report errors
     */

    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Report whether or not we successfully called the proc
     */

    ps = fCallSucceeded ? PSLSUCCESS : PSLSUCCESS_FALSE;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLCTXLISTCONTEXTDESTROY(pclo, aContext);
    return ps;
}



