/*
 *  PSLAlignment.c
 *
 *  Containes definition of alignment support for CPUs with alignment
 *  sensitivity
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"

#ifndef PSL_ALIGNMENT_INTRINSIC

/*
 *  PSLLoadAlignUInt16
 *
 *  Returns the value pointed to in an alignment insensitive way
 *
 *  Arguments:
 *      PSL_CONST PSLUINT16 PSL_UNALIGNED*
 *                      pSrc                Pointer to value requested
 *
 *  Returns:
 *      PSLUINT16       Aligned version of value requested
 *
 */

PSLUINT16 PSL_API PSLLoadAlignUInt16(PSL_CONST PSLUINT16 PSL_UNALIGNED* pSrc)
{
    PSLUINT16       wValue;

    PSLCopyMemory(&wValue, pSrc, sizeof(wValue));
    return wValue;
}


/*
 *  PSLStoreAlignUInt16
 *
 *  Stores the aligned value in an alignment insenstive way
 *
 *  Arguments:
 *      PSLUINT16 PSL_UNALIGNED*
 *                      pDest               Location to store the value
 *      PSLUINT16       wValue              Value to store
 *
 *  Returns:
 *      PSLUINT16       Value stored
 *
 */

PSLUINT16 PSL_API PSLStoreAlignUInt16(PSLUINT16 PSL_UNALIGNED* pDest,
                            PSLUINT16 wValue)
{
    PSLCopyMemory(pDest, &wValue, sizeof(*pDest));
    return wValue;
}


/*
 *  PSLCopyAlignUInt16
 *
 *  Copies one unaligned location to another unaligned location
 *
 *  Arguments:
 *      PSLUINT16 PSL_UNALIGNED*
 *                      pDest               Destination to copy to
 *      PSL_CONST PSLUINT16 PSL_UNALIGNED*
 *                      pSrc                Source to copy from
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API PSLCopyAlignUInt16(PSLUINT16 PSL_UNALIGNED* pDest,
                            PSL_CONST PSLUINT16 PSL_UNALIGNED* pSrc)
{
    PSLCopyMemory(pDest, pSrc, sizeof(*pDest));
    return;
}


/*
 *  PSLLoadAlignUInt32
 *
 *  Returns the value pointed to in an alignment insensitive way
 *
 *  Arguments:
 *      PSL_CONST PSLUINT32 PSL_UNALIGNED*
 *                      pSrc                Pointer to value requested
 *
 *  Returns:
 *      PSLUINT32       Aligned version of value requested
 *
 */

PSLUINT32 PSL_API PSLLoadAlignUInt32(PSL_CONST PSLUINT32 PSL_UNALIGNED* pSrc)
{
    PSLUINT32       dwValue;

    PSLCopyMemory(&dwValue, pSrc, sizeof(dwValue));
    return dwValue;
}


/*
 *  PSLStoreAlignUInt32
 *
 *  Stores the aligned value in an alignment insenstive way
 *
 *  Arguments:
 *      PSLUINT32 PSL_UNALIGNED*
 *                      pDest               Location to store the value
 *      PSLUINT32       dwValue             Value to store
 *
 *  Returns:
 *      PSLUINT32       Value stored
 *
 */

PSLUINT32 PSL_API PSLStoreAlignUInt32(PSLUINT32 PSL_UNALIGNED* pDest,
                            PSLUINT32 dwValue)
{
    PSLCopyMemory(pDest, &dwValue, sizeof(*pDest));
    return dwValue;
}


/*
 *  PSLCopyAlignUInt32
 *
 *  Copies one unaligned location to another unaligned location
 *
 *  Arguments:
 *      PSLUINT32 PSL_UNALIGNED*
 *                      pDest               Destination to copy to
 *      PSL_CONST PSLUINT32 PSL_UNALIGNED*
 *                      pSrc                Source to copy from
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API PSLCopyAlignUInt32(PSLUINT32 PSL_UNALIGNED* pDest,
                            PSL_CONST PSLUINT32 PSL_UNALIGNED* pSrc)
{
    PSLCopyMemory(pDest, pSrc, sizeof(*pDest));
    return;
}


/*
 *  PSLLoadAlignUInt64
 *
 *  Returns the value pointed to in an alignment insensitive way
 *
 *  Arguments:
 *      PSL_CONST PSLUINT64 PSL_UNALIGNED*
 *                      pSrc                Pointer to value requested
 *
 *  Returns:
 *      PSLUINT64       Aligned version of value requested
 *
 */

PSLUINT64 PSL_API PSLLoadAlignUInt64(PSL_CONST PSLUINT64 PSL_UNALIGNED* pSrc)
{
    PSLUINT64       qwValue;

    PSLCopyMemory(&qwValue, pSrc, sizeof(qwValue));
    return qwValue;
}


/*
 *  PSLStoreAlignUInt64
 *
 *  Stores the aligned value in an alignment insenstive way
 *
 *  Arguments:
 *      PSLUINT64 PSL_UNALIGNED*
 *                      pDest               Location to store the value
 *      PSLUINT64       qwValue             Value to store
 *
 *  Returns:
 *      PSLUINT64       Value stored
 *
 */

PSLUINT64 PSL_API PSLStoreAlignUInt64(PSLUINT64 PSL_UNALIGNED* pDest,
                            PSLUINT64 qwValue)
{
    PSLCopyMemory(pDest, &qwValue, sizeof(*pDest));
    return qwValue;
}


/*
 *  PSLCopyAlignUInt64
 *
 *  Copies one unaligned location to another unaligned location
 *
 *  Arguments:
 *      PSLUINT64 PSL_UNALIGNED*
 *                      pDest               Destination to copy to
 *      PSL_CONST PSLUINT64 PSL_UNALIGNED*
 *                      pSrc                Source to copy from
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API PSLCopyAlignUInt64(PSLUINT64 PSL_UNALIGNED* pDest,
                            PSL_CONST PSLUINT64 PSL_UNALIGNED* pSrc)
{
    PSLCopyMemory(pDest, pSrc, sizeof(*pDest));
    return;
}


/*
 *  PSLLoadAlignDouble
 *
 *  Returns the value pointed to in an alignment insensitive way
 *
 *  Arguments:
 *      PSL_CONST PSLDOUBLE PSL_UNALIGNED*
 *                      pSrc                Pointer to value requested
 *
 *  Returns:
 *      PSLDOUBLE       Aligned version of value requested
 *
 */

PSLDOUBLE PSL_API PSLLoadAlignDouble(PSL_CONST PSLDOUBLE PSL_UNALIGNED* pSrc)
{
    PSLDOUBLE       dValue;

    PSLCopyMemory(&dValue, pSrc, sizeof(dValue));
    return dValue;
}


/*
 *  PSLStoreAlignDouble
 *
 *  Stores the aligned value in an alignment insenstive way
 *
 *  Arguments:
 *      PSLDOUBLE PSL_UNALIGNED*
 *                      pDest               Location to store the value
 *      PSLDOUBLE       dValue              Value to store
 *
 *  Returns:
 *      PSLDOUBLE       Value stored
 *
 */

PSLDOUBLE PSL_API PSLStoreAlignDouble(PSLDOUBLE PSL_UNALIGNED* pDest,
                            PSLDOUBLE dValue)
{
    PSLCopyMemory(pDest, &dValue, sizeof(*pDest));
    return dValue;
}


/*
 *  PSLCopyAlignDouble
 *
 *  Copies one unaligned location to another unaligned location
 *
 *  Arguments:
 *      PSLDOUBLE PSL_UNALIGNED*
 *                      pDest               Destination to copy to
 *      PSL_CONST PSLDOUBLE PSL_UNALIGNED*
 *                      pSrc                Source to copy from
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API PSLCopyAlignDouble(PSLDOUBLE PSL_UNALIGNED* pDest,
                            PSL_CONST PSLDOUBLE PSL_UNALIGNED* pSrc)
{
    PSLCopyMemory(pDest, pSrc, sizeof(*pDest));
    return;
}

#endif  /* PSL_ALIGNMENT_INTRINSIC */


/*
 *  PSLCopyAlignUInt128
 *
 *  Copies one unaligned location to another unaligned location
 *
 *  Arguments:
 *      PSLUINT128 PSL_UNALIGNED*
 *                      pDest               Destination to copy to
 *      PSL_CONST PSLUINT128 PSL_UNALIGNED*
 *                      pSrc                Source to copy from
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API PSLCopyAlignUInt128(PSLUINT128 PSL_UNALIGNED* pDest,
                            PSL_CONST PSLUINT128 PSL_UNALIGNED* pSrc)
{
    PSLCopyMemory(pDest, pSrc, sizeof(*pDest));
    return;
}

