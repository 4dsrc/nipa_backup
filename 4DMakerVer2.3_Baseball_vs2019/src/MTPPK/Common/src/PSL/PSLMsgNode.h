/*
 *  PSLMsgNode.h
 *
 *  Contains the message node declarations that are private
 *  and used only within the queue implementation.
 *
 *  Copyright (c) M *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

#ifndef _PSLMSGNODE_H_
#define _PSLMSGNODE_H_

#define PSLMSG_COOKIE       'msgM'

#define PSLPMSGNODE_TO_PSLPMSG(x) (&((x)->msg))
#define PSLPMSG_TO_PSLPMSGNODE(x) \
            ((PSLMSGNODE*)((PSLBYTE*)(x) - PSLOFFSETOF(PSLMSGNODE, msg)))


typedef struct _PSLMSGNODE* PSLPMSGNODE;

typedef struct _PSLMSGHDR
{
#ifdef PSL_ASSERTS
    PSLUINT32           dwCookie;
#endif  /* PSL_ASSERTS */
    PSLPMSGNODE         pNext;
    PSLHANDLE           hSignal;
#ifdef PSL_MSG_TRACE
    PSLMSGPOOL          mpID;
    PSLBOOL             fMsgTrace;
#endif  /* PSL_MSG_TRACE */
} PSLMSGHDR;


typedef struct _PSLMSGNODE
{
    PSLMSGHDR           msgHdr;
    PSLMSG              msg;
} PSLMSGNODE;

#endif  /* _PSLMSGNODE_H_ */

