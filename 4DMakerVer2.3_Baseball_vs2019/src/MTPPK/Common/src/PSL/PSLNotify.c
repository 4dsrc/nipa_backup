/*
 *  PSLNotify.c
 *
 *  Contains definitions of the PSL Notification engine
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"

/*  Local defines
 */

#define PSLNOTIFY_COOKIE                'ntyP'

/*  Local Types
 */

typedef struct _PSLNOTIFYOBJ
{
#ifdef PSL_ASSERTS
    PSLUINT32               dwCookie;
#endif  /* PSL_ASSERTS */
    PSLCTXLIST              clist;
} PSLNOTIFYOBJ;

typedef struct _NOTFYMSGINFO
{
    PSLBOOL                 fMsgPost;
    PFNNOTIFYENUM           pfnNotifyEnum;
    PSLMSGID                msgID;
    PSLPARAM                aParam0;
    PSLPARAM                aParam1;
    PSLPARAM                aParam2;
    PSLPARAM                aParam3;
    PSLLPARAM               alParam;
    PSLUINT32               dwTimeOut;
} NOTIFYMSGINFO;

/*  Local Functions
 */

static PSLSTATUS PSL_API _PSLNotifyEnumCtxListProc(
                    PSLPARAM aParam,
                    PSLPARAM aEnumParam);

/*  Local Variables
 */


/*  _PSLNotifyEnumCtxListProc
 *
 *  Posts a message to all of the queues represented in the context
 *
 *  NOTE:
 *  On first look one might wonder why the extra work is done here to deal
 *  with contexts for message posts when they are typically handled
 *  asynchronously on another thread.  The key is "typically".  The code
 *  as written deals correctly with both multithreaded and singlethreaded
 *  message queues.
 *
 *  Arguments:
 *      PSLPARAM        aParam              Parameter in context (MSGQUEUE)
 *      PSLPARAM        aEnumParam          Enumeration parameter
 *                                            (NOTIFYMSGINFO)
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _PSLNotifyEnumCtxListProc(PSLPARAM aParam,
                    PSLPARAM aEnumParam)
{
    PSLMSGQUEUE     mqDest;
    PSLMSG*         pMsg = PSLNULL;
    PSLSTATUS       ps;
    NOTIFYMSGINFO*  pnmi;

    /*  Validate Arguments
     */

    if ((PSLNULL == aParam) || (PSLNULL == aEnumParam))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the message queue and the notify message information
     */

    mqDest = (PSLMSGQUEUE)aParam;
    pnmi = (NOTIFYMSGINFO*)aEnumParam;

    /*  Allocate a message
     *
     *  NOTE: Given that broadcasts are not expected to be "instantaneous"
     *  and that the number and type of destinations may vary wildly,
     *  the messages are allocated out of the "NULL pool" rather than out
     *  of a specific pool.  This way we do not have to attempt to allocate
     *  pools that are large enough all of the time.
     */

    ps = PSLMsgAlloc(PSLNULL, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And initialize it
     */

    pMsg->msgID = pnmi->msgID;
    pMsg->aParam0 = pnmi->aParam0;
    pMsg->aParam1 = pnmi->aParam1;
    pMsg->aParam2 = pnmi->aParam2;
    pMsg->aParam3 = pnmi->aParam3;
    pMsg->alParam = pnmi->alParam;

    /*  If an enumeration callback is present call it
     */

    if (PSLNULL != pnmi->pfnNotifyEnum)
    {
        ps = pnmi->pfnNotifyEnum(pMsg);
        if (PSLSUCCESS != ps)
        {
            /*  Ignore non successful failures- just skip the message
             */

            ps = PSL_FAILED(ps) ? ps : PSLSUCCESS;
            goto Exit;
        }
    }

    /*  And deliver the message as desired
     */

    if (pnmi->fMsgPost)
    {
        ps = PSLMsgPost(mqDest, pMsg);
    }
    else
    {
        ps = PSLMsgSend(mqDest, pMsg, pnmi->dwTimeOut);
    }
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg = PSLNULL;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*  PSLNotifyCreate
 *
 *  Create a notifier
 *
 *  Arguments:
 *      PSLNOTIFY*      pnotify             Return buffer for notifier
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API PSLNotifyCreate(PSLNOTIFY* pnotify)
{
    PSLNOTIFYOBJ*   pnoNew = PSLNULL;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pnotify)
    {
        *pnotify = PSLNULL;
    }

    /*  Validate arguments
     */

    if (PSLNULL == pnotify)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate a new notify object
     */

    pnoNew = (PSLNOTIFYOBJ*)PSLMemAlloc(PSLMEM_PTR, sizeof(*pnoNew));
    if (PSLNULL == pnoNew)
    {
         ps = PSLERROR_OUT_OF_MEMORY;
         goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Add the cookie
     */

    pnoNew->dwCookie = PSLNOTIFY_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Create a context list
     */

    ps = PSLCtxListCreate(&(pnoNew->clist));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Transfer ownership of the notifier to the caller
     */

    *pnotify = pnoNew;
    pnoNew = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLNOTIFYDESTROY(pnoNew);
    return ps;
}


/*  PSLNotifyDestroy
 *
 *  Closes a notifier
 *
 *  Arguments:
 *      PSLNOTIFY       notify              Notifier to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API PSLNotifyDestroy(PSLNOTIFY notify)
{
    PSLNOTIFYOBJ*   pno;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == notify)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the notify object
     */

    pno = (PSLNOTIFYOBJ*)notify;
    PSLASSERT(PSLNOTIFY_COOKIE == pno->dwCookie);

    /*  And handle the destroy
     */

    SAFE_PSLCTXLISTDESTROY(pno->clist);
    PSLMemFree(pno);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  PSLNotifyRegister
 *
 *  Registers a message queue to receive broadcast notifications from the
 *  specified notifier
 *
 *  Arguments:
 *      PSLNOTIFY       notify              Notifier to add queue to
 *      PSLMSGQUEUE     mqDest              Message queue to send notifications
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_EXISTS if already
 *                        registered
 */

PSLSTATUS PSL_API PSLNotifyRegister(PSLNOTIFY notify, PSLMSGQUEUE mqDest)
{
    PSLNOTIFYOBJ*   pno;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == notify)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the notify object
     */

    pno = (PSLNOTIFYOBJ*)notify;
    PSLASSERT(PSLNOTIFY_COOKIE == pno->dwCookie);

    /*  Add the queue to the context list
     */

    ps = PSLCtxListRegister(pno->clist, (PSLPARAM)mqDest);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  PSLNotifyUnregister
 *
 *  Removes the specified queue from the broadcast notification list
 *
 *  Arguments:
 *      PSLNOTIFY       notify              Notifier to remove queue from
 *      PSLMSGQUEUE     mqDest              Queue to remove
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if removed, PSLSUCCESS_NOT_FOUND if not found
  */

PSLSTATUS PSL_API PSLNotifyUnregister(PSLNOTIFY notify, PSLMSGQUEUE mqDest)
{
    PSLNOTIFYOBJ*   pno;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == notify || PSLNULL == mqDest)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the notify object
     */

    pno = (PSLNOTIFYOBJ*)notify;
    PSLASSERT(PSLNOTIFY_COOKIE == pno->dwCookie);

    /*  Remove the item from the context list
     */

    ps = PSLCtxListUnregister(pno->clist, (PSLPARAM)mqDest);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*   PSLNotifyMsgPost
 *
 *  Posts a message to all of the queues represented in the context
 *
 *  Arguments:
 *      PSLNOTIFY       notify              Notifier to send messages to
 *      PFNNOTIFYENUM   pfnNotifyEnum       Function to call prior to post
 *      PSLMSGID        msgID               Message ID to send
 *      PSLPARAM        aParam0             Message Param 0
 *      PSLPARAM        aParam1             Message Param 1
 *      PSLPARAM        aParam2             Message Param 2
 *      PSLPARAM        aParam3             Message Param 3
 *      PSLLPARAM       alParam             Message Long Param
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API PSLNotifyMsgPost(PSLNOTIFY notify, PFNNOTIFYENUM pfnNotifyEnum,
                    PSLMSGID msgID, PSLPARAM aParam0, PSLPARAM aParam1,
                    PSLPARAM aParam2, PSLPARAM aParam3, PSLLPARAM alParam)
{
    NOTIFYMSGINFO   nmi;
    PSLNOTIFYOBJ*   pno;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == notify)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the notify object
     */

    pno = (PSLNOTIFYOBJ*)notify;
    PSLASSERT(PSLNOTIFY_COOKIE == pno->dwCookie);

    /*  Initialize the message information
     */

    nmi.fMsgPost = PSLTRUE;
    nmi.pfnNotifyEnum = pfnNotifyEnum;
    nmi.msgID = msgID;
    nmi.aParam0 = aParam0;
    nmi.aParam1 = aParam1;
    nmi.aParam2 = aParam2;
    nmi.aParam3 = aParam3;
    nmi.alParam = alParam;
    nmi.dwTimeOut = 0;

    /*  Enumerate the context using the message post callback
     */

    ps = PSLCtxListEnum(pno->clist, _PSLNotifyEnumCtxListProc, (PSLPARAM)&nmi);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  PSLNotifyMsgSend
 *
 *  Sends a message to all of the queues represented in the context
 *
 *  Arguments:
 *      PSLNOTIFY       notify              Notifier to send messages to
 *      PFNNOTIFYENUM   pfnNotifyEnum       Function to call prior to post
 *      PSLMSGID        msgID               Message ID to send
 *      PSLPARAM        aParam0             Message Param 0
 *      PSLPARAM        aParam1             Message Param 1
 *      PSLPARAM        aParam2             Message Param 2
 *      PSLPARAM        aParam3             Message Param 3
 *      PSLLPARAM       alParam             Message Long Param
 *      PSLUINT32       dwTimeOut           Time to wait for response
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API PSLNotifyMsgSend(PSLNOTIFY notify, PFNNOTIFYENUM pfnNotifyEnum,
                    PSLMSGID msgID, PSLPARAM aParam0, PSLPARAM aParam1,
                    PSLPARAM aParam2, PSLPARAM aParam3, PSLLPARAM alParam,
                    PSLUINT32 dwTimeOut)
{
    NOTIFYMSGINFO   nmi;
    PSLNOTIFYOBJ*   pno;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == notify)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the notify object
     */

    pno = (PSLNOTIFYOBJ*)notify;
    PSLASSERT(PSLNOTIFY_COOKIE == pno->dwCookie);

    /*  Initialize the message information
     */

    nmi.fMsgPost = PSLFALSE;
    nmi.pfnNotifyEnum = pfnNotifyEnum;
    nmi.msgID = msgID;
    nmi.aParam0 = aParam0;
    nmi.aParam1 = aParam1;
    nmi.aParam2 = aParam2;
    nmi.aParam3 = aParam3;
    nmi.alParam = alParam;
    nmi.dwTimeOut = dwTimeOut;

    /*  Enumerate the context using the message post callback
     */

    ps = PSLCtxListEnum(pno->clist, _PSLNotifyEnumCtxListProc, (PSLPARAM)&nmi);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}

