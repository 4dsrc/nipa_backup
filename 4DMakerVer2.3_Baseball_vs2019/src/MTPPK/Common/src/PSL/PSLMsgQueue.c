/*
 *  PSLMsgQueue.c
 *
 *  Contains the message queue implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"
#include "PSLMsgNode.h"

#define PSLMSGQUEUE_COOKIE      'msgQ'

typedef struct PSLMSGQUEUEOBJ *PSLPMSGQUEUEOBJ;

typedef struct PSLMSGQUEUEOBJ
{
#ifdef PSL_ASSERTS
    PSLUINT32           dwCookie;
#endif  /*  PSL_ASSERTS */
    PSLVOID*            pvParam;      /*OnPost,OnWait,OnDestroy param*/
    PFNONPOST           pfnOnPost;    /*Signals message availablity*/
    PFNONWAIT           pfnOnWait;    /*Waits for message availablity*/
    PFNONDESTROY        pfnOnDestroy; /*Cleanup pvParam*/
    PSLHANDLE           hLock;        /*Lock message queue*/
    PSLMSGNODE*         pHead;        /*Message head*/
    PSLMSGNODE*         pTail;        /*Message tail*/
    PSLMSGNODE*         pPriority;    /*Priority message tail*/
    PSLPMSGQUEUEOBJ     pNext;        /*Pointer to next queue*/
} PSLMSGQUEUEOBJ;


static PSLSTATUS _Dequeue(
                    PSLMSGQUEUEOBJ* pQueueObj,
                    PSLMSGNODE**    ppMsgNode);

static PSLSTATUS _Enqueue(
                    PSLMSGQUEUEOBJ* pQueueObj,
                    PSLMSGNODE*     pMsgNode);

/*
 *  Queue creation
 *  The function will create a queue object and will return its
 *  identifier. The caller of this function can pass in the
 *  handle to a signalling object or a call back function pointer
 *  as pvParam and queue will pass that pvParam back to the user when
 *  calling the three call back functions.
 *  pfnOnPost is called when a message is posted to the queue to allow
 *  the user to signal the availability of message
 *  pfnOnWait is called to allow the user of a queue to wait for
 *  the availability of a message and
 *  pfnOnDestroy is called when destroying the queue to allow the
 *  user to clean up pvParam.
 *
 */
PSLSTATUS PSL_API PSLMsgQueueCreate (PSLVOID*  pvParam,
                                     PFNONPOST pfnOnPost,
                                     PFNONWAIT pfnOnWait,
                                     PFNONDESTROY pfnOnDestroy,
                                     PSLMSGQUEUE* pmqID)
{
    PSLSTATUS       status;
    PSLMSGQUEUEOBJ* pmqObj = PSLNULL;

    if (PSLNULL != pmqID)
    {
        *pmqID = PSLNULL;
    }

    /*
     * pvParam is not checked as this could have
     * PSLNULL value in  a single threaded system
     *
     */
    if (PSLNULL == pmqID || PSLNULL == pfnOnPost ||
        PSLNULL == pfnOnWait || PSLNULL == pfnOnDestroy)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmqObj = PSLMemAlloc(PSLMEM_PTR, sizeof(PSLMSGQUEUEOBJ));
    if (PSLNULL == pmqObj)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

#ifdef PSL_ASSERTS
    pmqObj->dwCookie = PSLMSGQUEUE_COOKIE;
#endif  /* PSL_ASSERTS */

    status = PSLMutexOpen(PSLFALSE, PSLNULL, &pmqObj->hLock);
    if (PSL_FAILED(status))
    {
        status = PSLERROR_QUEUE_CREATE;
        goto Exit;
    }

    pmqObj->pfnOnPost       = pfnOnPost;
    pmqObj->pfnOnWait       = pfnOnWait;
    pmqObj->pfnOnDestroy    = pfnOnDestroy;
    pmqObj->pvParam         = pvParam;
    pmqObj->pHead           = PSLNULL;
    pmqObj->pTail           = PSLNULL;
    pmqObj->pPriority       = PSLNULL;
    pmqObj->pNext           = PSLNULL;

    *pmqID = (PSLMSGQUEUE)pmqObj;
    pmqObj = PSLNULL;
Exit:
    SAFE_PSLMEMFREE(pmqObj);
    return status;
}

/*
 *  Queue destruction
 *  The fucntion will destroy the queue object.It take queue
 *  identifier returned by PSLMsgQueueCreate as input parameter.
 *
 */
PSLSTATUS PSL_API PSLMsgQueueDestroy (PSLMSGQUEUE mqID)
{
    PSLSTATUS       status;
    PSLMSGQUEUEOBJ* pmqObj;

    /*check for ppvParam is intentionally avoided*/

    if (PSLNULL == mqID)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmqObj = (PSLMSGQUEUEOBJ*)mqID;
    PSLASSERT(PSLMSGQUEUE_COOKIE == pmqObj->dwCookie);

    /*
     * We will clean up queue even when queue is not empty
     * This won't cause any memory leak as pool allocates messages
     *
     */
    PSLASSERT (PSLNULL == pmqObj->pHead);

    (PSLVOID)pmqObj->pfnOnDestroy(pmqObj->pvParam);

    SAFE_PSLMEMFREE(pmqObj);

    status = PSLSUCCESS;
Exit:
    return status;
}

/*
 *  The method will put a message to the head of the queue and will
 *  wait on a signalling object which will be signalled by PSLMsgFree.
 *  Caller of this function needs to allocate the message buffer using
 *  PSLMsgAlloc. Also user of this fucntion need to call PSlMsgFree to
 *  free the message.
 *
 */
PSLSTATUS PSL_API PSLMsgSend(PSLMSGQUEUE mqQID, PSLMSG* pMsg,
                             PSLUINT32 dwMSTimeout)
{
    PSLSTATUS       status;
    PSLMSGQUEUEOBJ* pQueueObj;
    PSLMSGNODE*     pMsgNode;
    PSLHANDLE       hMutexRelease   = PSLNULL;
    PSLHANDLE       hSignal         = PSLNULL;

    if (PSLNULL == mqQID || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pQueueObj = (PSLMSGQUEUEOBJ*)(mqQID);
    PSLASSERT(PSLMSGQUEUE_COOKIE == pQueueObj->dwCookie);

    pMsgNode = PSLPMSG_TO_PSLPMSGNODE(pMsg);

    status = PSLMutexAcquire(pQueueObj->hLock, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        status = PSLERROR_QUEUE_LOCK;
        PSLTraceError("PSLMsgSend(%d)::Locking failed", pQueueObj);
        goto Exit;
    }
    hMutexRelease = pQueueObj->hLock;

    /*
     * The check is used to ensure that user of PSLMSGSend has used
     * PSLMSGAlloc to allocate message node structure. PSLMsgAlloc will
     * set this cookie to the appropriate value.
     * This is intended to prevent accidental coding errors.
     *
     */
    PSLASSERT(PSLMSG_COOKIE == pMsgNode->msgHdr.dwCookie);

    /*
     *  The reason the signal is created here instead of
     *  PSLMsgPoolCreate or PSLMsgAlloc is because this
     *  signalling object is only used by this PSLMsgSend.
     */
    status = PSLSignalOpen(PSLFALSE, PSLFALSE, PSLNULL, &hSignal);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pMsgNode->msgHdr.hSignal = hSignal;

    /*  Add the message node to the head */
    pMsgNode->msgHdr.pNext = pQueueObj->pHead;
    pQueueObj->pHead = pMsgNode;
    if (PSLNULL == pQueueObj->pTail)
    {
        pQueueObj->pTail = pQueueObj->pHead;
    }

    PSLTraceDetail("PSLMsgSend(%d)- Message ID = 0x%08x",
                    pQueueObj,
                    pMsgNode->msg.msgID);

    status = pQueueObj->pfnOnPost(pQueueObj->pvParam);
    if (PSL_FAILED(status))
    {
        status = PSLERROR_QUEUE_ONPOST;
        goto Exit;
    }

    /*  Release the mutex so that other threads can do work
     */

    PSLMutexRelease(pQueueObj->hLock);
    hMutexRelease = PSLNULL;

    /*  Wait for the object which will be signaled by PSLMsgFree.
     */
    status = PSLSignalWait(hSignal, dwMSTimeout);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLSIGNALCLOSE(hSignal);

    return status;
}

/*
 *  The method will post a message to the queue. Caller of this function
 *  needs to allocate the message buffer using PSLMsgAlloc.
 *
 */
PSLSTATUS PSL_API PSLMsgPost(PSLMSGQUEUE mqQID, PSLMSG* pMsg)
{
    PSLSTATUS       status;
    PSLMSGQUEUEOBJ* pQueueObj;

    if (PSLNULL == mqQID || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pQueueObj = (PSLMSGQUEUEOBJ*)(mqQID);
    PSLASSERT(PSLMSGQUEUE_COOKIE == pQueueObj->dwCookie);

    status = _Enqueue(pQueueObj, PSLPMSG_TO_PSLPMSGNODE(pMsg));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
Exit:
    return status;
}

/*
 *  Method will return a message from the queue. The message will be
 *  removed from the queue once it is read. Caller of this function
 *  needs to release the buffers back to the pool using PSLMsgFree.
 *  This method will block until a message is available in the queue.
 *
 */
PSLSTATUS PSL_API PSLMsgGet(PSLMSGQUEUE mqID, PSLMSG** ppMsg)
{
    PSLSTATUS       status;
    PSLMSGQUEUEOBJ* pQueueObj;
    PSLMSGNODE*     pMsgNode = PSLNULL;

    if (ppMsg)
    {
        *ppMsg = PSLNULL;
    }

    if (PSLNULL == mqID || PSLNULL == ppMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pQueueObj = (PSLMSGQUEUEOBJ*)(mqID);
    PSLASSERT(PSLMSGQUEUE_COOKIE == pQueueObj->dwCookie);

    status = _Dequeue(pQueueObj, &pMsgNode);
    if (PSL_FAILED(status))
    {
        PSLTraceError("PSLMsgGet _Dequeue call failed");
        goto Exit;
    }

    *ppMsg = PSLPMSGNODE_TO_PSLPMSG(pMsgNode);

    pMsgNode = PSLNULL;
Exit:

    return status;
}

/*
 *  Method will return a message from the queue without actually
 *  removing it from the queue. Caller of this function wil pass a
 *  pointer to a message structure and the function will copy the
 *  message from the queue on to the message buffer. This is done
 *  to avoid race conditions. Caller of this function should not call
 *  PSLMsgFree. Caller can also pass PSLNULL value for the pMsg
 *  parameter to check if message is avilable in the queue with out
 *  even performing any copy
 *
 */
PSLSTATUS PSL_API PSLMsgPeek(PSLMSGQUEUE mqID, PSLMSG* pMsg)
{
    PSLSTATUS       status;
    PSLMSGQUEUEOBJ* pQueueObj;
    PSLHANDLE       hMutexRelease = PSLNULL;

    /*
     * pMsg's validity is not checked as it can be PSLNULL,
     * in which case function will return PSLSUCCESS
     * if the queue has a message. In this case caller wants
     * to know if a message is available in the queue
     *
     */
    if (PSLNULL == mqID)
    {
        PSLTraceError("PSLMsgPeek(%d)::Parameter validation failed",
                        mqID);
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pQueueObj = (PSLMSGQUEUEOBJ*)(mqID);
    PSLASSERT(PSLMSGQUEUE_COOKIE == pQueueObj->dwCookie);

    status = PSLMutexAcquire(pQueueObj->hLock, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        status = PSLERROR_QUEUE_LOCK;
        PSLTraceError("PSLMsgPeek(%d)::Locking failed", pQueueObj);
        goto Exit;
    }
    hMutexRelease = pQueueObj->hLock;

    if (PSLNULL != pQueueObj->pHead)
    {
        if ( PSLNULL != pMsg)
        {
            PSLCopyMemory(pMsg, pQueueObj->pHead, sizeof(PSLMSG));
        }
        status = PSLSUCCESS;
    }
    else
    {
        status = PSLSUCCESS_NOT_FOUND;
    }
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    return status;
}

/*
 *  This function will be called by the disptacher
 *  when it finds that the system is single threaded
 *  The function sets the call back fucntion pointer
 *  to the dispatcher which will be called by OnPost function
 *  when a message gets posted to the queue.
 *
 */
PSLSTATUS PSL_API PSLMsgQueueSetCallbacks(PSLMSGQUEUE mqDispatch,
                                  PSLVOID* pvParam,
                                  PFNONPOST pfnOnPost,
                                  PFNONWAIT pfnOnWait,
                                  PFNONDESTROY pfnOnDestroy)
{
    PSLSTATUS status;
    PSLMSGQUEUEOBJ* pmqObj;

    if (PSLNULL == mqDispatch || PSLNULL == pvParam ||
        PSLNULL == pfnOnPost || PSLNULL == pfnOnWait ||
        PSLNULL == pfnOnDestroy)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmqObj = (PSLMSGQUEUEOBJ*)mqDispatch;
    PSLASSERT(PSLNULL != pmqObj);

    pmqObj->pfnOnPost       =   pfnOnPost;
    pmqObj->pfnOnWait       =   pfnOnWait;
    pmqObj->pfnOnDestroy    =   pfnOnDestroy;
    pmqObj->pvParam         =   pvParam;

    status = PSLSUCCESS;

Exit:
    return status;
}


/*
 *  This function enumerates the messages in the queue.  For each message
 *  the callback function provided is called and depending on the result
 *  of the callback action is taken.  If the callback returns PSLSUCCESS the
 *  message is left in the queue.  Returning PSLSUCCESS_FALSE from the
 *  callback causes the message to be removed from the queue.  Returning
 *  PSLSUCCESS_CANCEL causes enumeration to stop and the enumeration function
 *  to report success.  Returning any error from the callback causes enumeration
 *  to stop and the error to be returned.
 *
 */

PSLSTATUS PSL_API PSLMsgEnum(PSLMSGQUEUE mqEnum, PFNMSGENUM pfnMsgEnum,
                            PSLPARAM aParam)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       status;
    PSLMSGQUEUEOBJ* pQueueObj;
    PSLMSGNODE*     pMsgNodeCur;
    PSLMSGNODE*     pMsgNodeLast;
    PSLMSG*         pMsgFree;

    /*  Validate Arguments
     */

    if ((PSLNULL == mqEnum) || (PSLNULL == pfnMsgEnum))
    {
        PSLTraceError("PSLMsgEnum(%d)::Parameter validation failed",
                        mqEnum);
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pQueueObj = (PSLMSGQUEUEOBJ*)mqEnum;
    PSLASSERT(PSLMSGQUEUE_COOKIE == pQueueObj->dwCookie);

    /*  Acquire the mutex
     *
     *  NOTE: We need to make sure that no one can change the queue during the
     *  enumeration process so we will hold the lock on this thread until
     *  it completes.
     */

    status = PSLMutexAcquire(pQueueObj->hLock, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        status = PSLERROR_QUEUE_LOCK;
        PSLTraceError("PSLMsgPeek(%d)::Locking failed", pQueueObj);
        goto Exit;
    }
    hMutexRelease = pQueueObj->hLock;

    /*  Walk through and enumerate each of the messages
     */

    pMsgNodeLast = PSLNULL;
    pMsgNodeCur = pQueueObj->pHead;
    while (PSLNULL != pMsgNodeCur)
    {
        /*  Forward the message to the callback
         */

        status = pfnMsgEnum(PSLPMSGNODE_TO_PSLPMSG(pMsgNodeCur), aParam);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        /*  Figure out if we need to do anything special
         */

        switch (status)
        {
        case PSLSUCCESS_FALSE:
            /*  Remove the message from the list
             */

            if (PSLNULL != pMsgNodeLast)
            {
                pMsgNodeLast->msgHdr.pNext = pMsgNodeCur->msgHdr.pNext;
            }
            else
            {
                pQueueObj->pHead = pMsgNodeCur->msgHdr.pNext;
            }

            if (pMsgNodeCur == pQueueObj->pPriority)
            {
                pQueueObj->pPriority = pMsgNodeLast;
            }
            if (PSLNULL == pQueueObj->pHead)
            {
                PSLASSERT(pQueueObj->pTail == pMsgNodeCur);
                pQueueObj->pTail = PSLNULL;
            }

            /*  Release the message
             */

            pMsgFree = PSLPMSGNODE_TO_PSLPMSG(pMsgNodeCur);
            pMsgNodeCur = pMsgNodeCur->msgHdr.pNext;
            PSLMsgFree(pMsgFree);
            break;

        case PSLSUCCESS_CANCEL:
            /*  Force enumeration to stop
             */

            pMsgNodeCur = PSLNULL;
            break;

        default:
            /*  Continue enumeration
             */

            pMsgNodeLast = pMsgNodeCur;
            pMsgNodeCur = pMsgNodeCur->msgHdr.pNext;
        }
    }

    /*  Report success
     */

    status = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    return status;

}


/*
 *  The function performs the operation of adding a message node
 *  to the queue. If the input message is of high priority
 *  it will added to the head of the queue or at the end
 *  of the priority message list and the start of normal message list.
 *  If the message is normal priority it will always be added to the
 *  tail of the queue.
 *
 */
static PSLSTATUS _Enqueue (PSLMSGQUEUEOBJ* pQueueObj,
                           PSLMSGNODE* pMsgNode)
{
    PSLSTATUS status;
    PSLBOOL fAcquired = PSLFALSE;

    if (PSLNULL == pMsgNode || PSLNULL == pQueueObj)
    {
        PSLTraceError("\t_Enqueue(%d)- Parameter validation failed",
                  pQueueObj);
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLMutexAcquire(pQueueObj->hLock, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        status = PSLERROR_QUEUE_LOCK;
        PSLTraceError("\t_Enqueue(%d)- Locking failed", pQueueObj);
        goto Exit;
    }
    fAcquired = PSLTRUE;

    /*
    * The check is used to ensure that user of PSLMSGPost has used
    * PSLMSGAlloc to allocate message node structure. PSLMsgAlloc will
    * set this cookie to the appropriate value.
    * This is intended to prevent accidental coding errors.
    *
    */
    PSLASSERT(PSLMSG_COOKIE == pMsgNode->msgHdr.dwCookie);

    pMsgNode->msgHdr.pNext = PSLNULL;

    PSLTraceDetail("\t_Enqueue(%d)- Message ID = 0x%08x\tPriority = %s",
                            pQueueObj, pMsgNode->msg.msgID,
                            (PSLMSGID_PRIORITY & pMsgNode->msg.msgID) ?
                                "High" : "Normal");

    if (PSLMSGID_PRIORITY & pMsgNode->msg.msgID)
    {
        if (PSLNULL == pQueueObj->pPriority)
        {
            /* make input node the head */
            pMsgNode->msgHdr.pNext = pQueueObj->pHead;
            pQueueObj->pHead = pMsgNode;
        }
        else
        {
            /*
             * add input node between the tail of priority list
             * and start of normal list
             */
            pMsgNode->msgHdr.pNext =
                                pQueueObj->pPriority->msgHdr.pNext;
            pQueueObj->pPriority->msgHdr.pNext = pMsgNode;
        }

        /* if the normal tail is the same as priority tail
         * update it as well
         */
        if (pQueueObj->pPriority == pQueueObj->pTail)
        {
            pQueueObj->pTail = pMsgNode;
        }

		/* update priority tail pointer */
        pQueueObj->pPriority = pMsgNode;

    }
    else
    {
        if (PSLNULL == pQueueObj->pTail)
        {
            PSLASSERT(PSLNULL == pQueueObj->pHead);
            PSLASSERT(PSLNULL == pQueueObj->pPriority);
            if (PSLNULL != pQueueObj->pHead ||
                PSLNULL != pQueueObj->pPriority )
            {
                PSLTraceError(
                    "\t_Enqueue(%d)- Tail is NULL, \
                      Head and Priority tail are  \
                      not NULL as expected", pQueueObj);
                status = PSLERROR_UNEXPECTED;
                goto Exit;
            }
            pQueueObj->pHead = pMsgNode;
        }
        else
        {
            pQueueObj->pTail->msgHdr.pNext = pMsgNode;
        }
        pQueueObj->pTail = pMsgNode;
    }

    status = pQueueObj->pfnOnPost(pQueueObj->pvParam);
    if (PSL_FAILED(status))
    {
        status = PSLERROR_QUEUE_ONPOST;
        goto Exit;
    }
Exit:
    if (PSLTRUE == fAcquired)
    {
        (PSLVOID)PSLMutexRelease(pQueueObj->hLock);
    }
    return status;
}

/*
 *  The function performs the operation of remving a message node
 *  from the queue. Always the message at the head is returned.
 *
 */
static PSLSTATUS _Dequeue (PSLMSGQUEUEOBJ* pQueueObj,
                           PSLMSGNODE** ppMsgNode)
{
    PSLSTATUS status;
    PSLSTATUS statusWait;
    PSLMSGNODE* pMsgNodeRet = PSLNULL;
    PSLBOOL fAquiredLock = PSLFALSE;
    PSLBOOL fBlock;

    if (PSLNULL != ppMsgNode)
    {
        *ppMsgNode = PSLNULL;
    }

    if (PSLNULL == pQueueObj || PSLNULL == ppMsgNode)
    {
        PSLTraceError("\t_Dequeue(%d)- Parameter validation failed",
                        pQueueObj);
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLMutexAcquire(pQueueObj->hLock,PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        status = PSLERROR_QUEUE_LOCK;
        PSLTraceError(
            "\t_Dequeue(%d)- Locking failed after wait",
            pQueueObj);
        goto Exit;
    }

    fAquiredLock = PSLTRUE;

    if (PSLNULL == pQueueObj->pHead)
    {
        do
        {
            status = PSLMutexRelease(pQueueObj->hLock);
            if (PSL_FAILED(status))
            {
                status = PSLERROR_QUEUE_UNLOCK;
                PSLTraceError(
                    "\t_Dequeue(%d)- Unlocking failed before wait",
                    pQueueObj);
                goto Exit;
            }

            fAquiredLock = PSLFALSE;

            /*
             * On Wait ficntion in a multi-threaded system must
             * return PSLSUCCESS_BLOCKING instead of PSLSUCCESS
             * otherwise the PSLMsgGet logic will fail.
             *
             */

            statusWait = pQueueObj->pfnOnWait(pQueueObj->pvParam);
            if (PSL_FAILED(statusWait))
            {
                status = PSLERROR_QUEUE_ONWAIT;
                goto Exit;
            }

            status = PSLMutexAcquire(pQueueObj->hLock,PSLTHREAD_INFINITE);
            if (PSL_FAILED(status))
            {
                status = PSLERROR_QUEUE_LOCK;
                PSLTraceError(
                    "\t_Dequeue(%d)- Locking failed after wait",
                    pQueueObj);
                goto Exit;
            }

            fAquiredLock = PSLTRUE;

            fBlock = (PSLNULL == pQueueObj->pHead) &&
                    (statusWait == PSLSUCCESS_BLOCKING);
        }
        while(fBlock);
    }

    if (PSLNULL == pQueueObj->pHead)
    {
        status = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    pMsgNodeRet = pQueueObj->pHead;

    pQueueObj->pHead = pQueueObj->pHead->msgHdr.pNext;

    if (pMsgNodeRet == pQueueObj->pPriority)
    {
        pQueueObj->pPriority = PSLNULL;
    }
    if (PSLNULL == pQueueObj->pHead)
    {
        PSLASSERT(pQueueObj->pTail == pMsgNodeRet);
        pQueueObj->pTail = PSLNULL;
    }

    PSLTraceDetail("\t_Dequeue(%d)- Message ID = 0x%08x",
                    pQueueObj,
                    pMsgNodeRet->msg.msgID);

    *ppMsgNode = pMsgNodeRet;
    pMsgNodeRet = PSLNULL;

    /*
     * In a single threaded system thread functions like PSLMutexAcquire
     * could return PSLSUCCESS_SINGLE_THREADED.Since that indicate
     * success and to avoid this fucntion from returning non-zero value
     * forcing the return value to PSLSUCCESS
     *
     */
    status = PSLSUCCESS;

Exit:
    if (pQueueObj)
    {
        if( fAquiredLock )
        {
            (PSLVOID)PSLMutexRelease(pQueueObj->hLock);
        }
    }

    return status;
}
