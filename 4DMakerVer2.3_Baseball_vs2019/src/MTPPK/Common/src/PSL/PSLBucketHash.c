/*
 *  PSLBucketHash.c
 *
 *  Contains definition of the bucket hash functionality.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"

/*  Local Defines
 */

#define PSLBKTHASH_COOKIE       'BktH'

#define SAFE_FREEHASHNODE       SAFE_PSLMEMFREE

/*  Local Types
 */

typedef struct _HASHNODE
{
    PSLPARAM                aKey;
    PSLPARAM                aValue;
    struct _HASHNODE*       phnNext;
} HASHNODE;

typedef struct _PSLBKTHASHOBJ
{
#ifdef PSL_ASSERTS
    PSLUINT32               dwCookie;
#endif  /* PSL_ASSERTS */
    PSLHANDLE               hMutex;
    PSLUINT32               cTableEntries;
    HASHNODE**              pphnTable;
    PSLUINT32               cEntries;
    HASHNODE*               phnCursor;
    PSLPARAM                aHashParam;
    PFNPSLBKTHASHFN         pfnHash;
} PSLBKTHASHOBJ;

/*  Local Functions
 */

static PSLUINT32 PSL_API _DefaultHash(PSLPARAM aKey,
                            PSLUINT32 cTableEntries,
                            PSLPARAM aHashParam);

/*  Local Variables
 */

/*
 *  _DefaultHash
 *
 *  Default hash used for all tables
 *
 *  Arguments:
 *      PSLPARAM        aKey                Key to hash
 *      PSLUINT32       cTableEntries       Size of the hash table
 *      PSLPARAM        aHashParam          Hash parameter
 *
 *  Returns:
 *      PSLUINT32       Hash of the provided key
 *
 */

PSLUINT32 PSL_API _DefaultHash(PSLPARAM aKey, PSLUINT32 cTableEntries,
                            PSLPARAM aHashParam)
{
    return (PSLUINT32)(aKey % cTableEntries);
    aHashParam;
}


/*
 *  PSLBktHashCreate
 *
 *  Creates a bucket hash object
 *
 *  Arguments:
 *      PSLUINT32       cTableEntries       Size of the hash table
 *      PFNPSLBKTHASHFN pfnHash             Hash function, NULL for default
 *      PSLUINT32       aHashParam          Parameter for hash function
 *      PSLBKTHASH*     pbh                 Return location for hash created
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLBktHashCreate(PSLUINT32 cTableEntries,
                    PFNPSLBKTHASHFN pfnHash, PSLUINT32 aHashParam,
                    PSLBKTHASH* pbh)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    PSLBKTHASHOBJ*  pbhoNew = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pbh)
    {
        *pbh = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((0 == cTableEntries) || (PSLNULL == pbh))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create a new hash object and allocate the table at the same time
     */

    pbhoNew = (PSLBKTHASHOBJ*)PSLMemAlloc(PSLMEM_PTR,
                (sizeof(PSLBKTHASHOBJ) + (cTableEntries * sizeof(HASHNODE*))));
    if (PSLNULL == pbhoNew)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }
#ifdef PSL_ASSERTS
    pbhoNew->dwCookie = PSLBKTHASH_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Create a mutex for thread safety
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, PSLNULL, &(pbhoNew->hMutex));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pbhoNew->hMutex;

    /*  Stash the information we need- make sure that we do not set the hash
     *  parameter if we are using the default function
     */

    pbhoNew->cTableEntries = cTableEntries;
    pbhoNew->pphnTable = (HASHNODE**)
                            ((PSLBYTE*)pbhoNew + sizeof(PSLBKTHASHOBJ));
    pbhoNew->pfnHash = (PSLNULL != pfnHash) ? pfnHash : _DefaultHash;
    pbhoNew->aHashParam = (PSLNULL != pfnHash) ? aHashParam : 0;

    /*  Return the new objcet
     */

    *pbh = pbhoNew;
    pbhoNew = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLBKTHASHDESTROY(pbhoNew);
    return ps;
}


/*
 *  PSLBktHashDestroy
 *
 *  Destroys the bucket hash object
 *
 *  Arguments:
 *      PSLBKTHASH      bh                  Bucket hash to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLBktHashDestroy(PSLBKTHASH bh)
{
    PSLHANDLE       hMutexClose = PSLNULL;
    PSLUINT32       idxHash;
    HASHNODE*       phnCur;
    HASHNODE*       phnNext;
    PSLSTATUS       ps;
    PSLBKTHASHOBJ*  pbho;

    /*  Validate arguments
     */

    if (PSLNULL == bh)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pbho = (PSLBKTHASHOBJ*)bh;
    PSLASSERT(PSLBKTHASH_COOKIE == pbho->dwCookie);

    /*  Acquire the mutex if available
     */

    if (PSLNULL != pbho->hMutex)
    {
        ps = PSLMutexAcquire(pbho->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexClose = pbho->hMutex;
    }

    /*  Walk through each of the items in the hash table and release them
     */

    for (idxHash = 0; idxHash < pbho->cTableEntries; idxHash++)
    {
        phnCur = pbho->pphnTable[idxHash];
        while (PSLNULL != phnCur)
        {
            phnNext = phnCur->phnNext;
            SAFE_FREEHASHNODE(phnCur);
            phnCur = phnNext;
        }
    }

    /*  Destroy the object
     */

    PSLMemFree(pbho);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXCLOSE(hMutexClose);
    return ps;
}


/*
 *  PSLBktHashGetSize
 *
 *  Returns the number of items in the hash
 *
 *  Arguments:
 *      PSLBKTHASH      bh                  Hash to query
 *      PSLUINT32*      pcEntries           Return buffer for size
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLBktHashGetSize(PSLBKTHASH bh, PSLUINT32* pcEntries)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    PSLBKTHASHOBJ*  pbho;

    /*  Clear result for safety
     */

    if (PSLNULL != pcEntries)
    {
        *pcEntries = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == bh) || (PSLNULL == pcEntries))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pbho = (PSLBKTHASHOBJ*)bh;
    PSLASSERT(PSLBKTHASH_COOKIE == pbho->dwCookie);

    /*  Acquire the mutex
     */

    PSLASSERT(PSLNULL != pbho->hMutex);
    ps = PSLMutexAcquire(pbho->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pbho->hMutex;

    /*  Return the number of items in the hash
     */

    *pcEntries = pbho->cEntries;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLBktHashInsert
 *
 *  Inserts an entry into the hash object
 *
 *  Arguments:
 *      PSLBKTHASH      bh                  Hash to add item to
 *      PSLPARAM        aKey                Key for the item
 *      PSLPARAM        aValue              Value for the item
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_EXISTS if item exists
 *                        and was modified
 *
 */

PSLSTATUS PSL_API PSLBktHashInsert(PSLBKTHASH bh, PSLPARAM aKey,
                    PSLPARAM aValue)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLUINT32       idxHash;
    HASHNODE*       phnCur;
    HASHNODE*       phnLast;
    HASHNODE*       phnNew = PSLNULL;
    PSLSTATUS       ps;
    PSLBKTHASHOBJ*  pbho;

    /*  Validate arguments
     */

    if (PSLNULL == bh)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pbho = (PSLBKTHASHOBJ*)bh;
    PSLASSERT(PSLBKTHASH_COOKIE == pbho->dwCookie);

    /*  Acquire the mutex
     */

    PSLASSERT(PSLNULL != pbho->hMutex);
    ps = PSLMutexAcquire(pbho->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pbho->hMutex;

    /*  Determine the hash value
     */

    PSLASSERT(PSLNULL != pbho->pfnHash);
    idxHash = pbho->pfnHash(aKey, pbho->cTableEntries, pbho->aHashParam);
    if (idxHash >= pbho->cTableEntries)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  And locate the correct location in the table for this entry.  We have
     *  to walk the entries here because we want to avoid to entries with the
     *  same key in the list.
     *
     *  NOTE: We cannot optimize this with a sort or something else because
     *  aKey is opaque to us.  The best we can do is detect equality of value
     *  and handle it that way.
     */

    for (phnLast = PSLNULL, phnCur = pbho->pphnTable[idxHash];
            (PSLNULL != phnCur) && (aKey != phnCur->aKey);
            phnLast = phnCur, phnCur = phnCur->phnNext)
    {
    }

    /*  If we found a match replace the item in the table
     */

    if ((PSLNULL != phnCur) && (aKey == phnCur->aKey))
    {
        /*  Store the new value and report PSLSUCCESS_EXISTS to indicate that
         *  an existing entry was updated
         */

        phnCur->aValue = aValue;
        ps = PSLSUCCESS_EXISTS;
        goto Exit;
    }

    /*  Allocate a new node
     */

    phnNew = (HASHNODE*)PSLMemAlloc(PSLMEM_PTR, sizeof(*phnNew));
    if (PSLNULL == phnNew)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /*  Store the information
     */

    phnNew->aKey = aKey;
    phnNew->aValue = aValue;

    /*  And add the entry to the list
     */

    if (PSLNULL != phnLast)
    {
        phnLast->phnNext = phnNew;
    }
    else
    {
        pbho->pphnTable[idxHash] = phnNew;
    }
    phnNew->phnNext = phnCur;
    phnNew = PSLNULL;

    /*  Increment the entry count and reset the cursor on this change
     */

    pbho->cEntries++;
    pbho->phnCursor = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_FREEHASHNODE(phnNew);
    return ps;
}


/*
 *  PSLBktHashFind
 *
 *  Locates a value in the hash
 *
 *  Arguments:
 *      PSLBKTHASH      bh                  Hash to search
 *      PSLPARAM        aKey                Key to look for
 *      PSLPARAM*       paValue             Resulting value
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if found, PSLSUCCESS_NOT_FOUND if not
 *
 */

PSLSTATUS PSL_API PSLBktHashFind(PSLBKTHASH bh, PSLPARAM aKey,
                    PSLPARAM* paValue)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLUINT32       idxHash;
    HASHNODE*       phnCur;
    PSLSTATUS       ps;
    PSLBKTHASHOBJ*  pbho;

    /*  Clear result for safety
     */

    if (PSLNULL != paValue)
    {
        *paValue = 0;
    }

    /*  Validate arguments
     */

    if (PSLNULL == bh)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pbho = (PSLBKTHASHOBJ*)bh;
    PSLASSERT(PSLBKTHASH_COOKIE == pbho->dwCookie);

    /*  Acquire the mutex
     */

    PSLASSERT(PSLNULL != pbho->hMutex);
    ps = PSLMutexAcquire(pbho->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pbho->hMutex;

    /*  Make sure we have work to do in the first place
     */

    if (0 == pbho->cEntries)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Determine what bucket to search
     */

    idxHash = pbho->pfnHash(aKey, pbho->cTableEntries, pbho->aHashParam);
    if (idxHash >= pbho->cTableEntries)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  And walk through it looking for the match
     */

    for (phnCur = pbho->pphnTable[idxHash];
            (PSLNULL != phnCur) && (aKey != phnCur->aKey);
            phnCur = phnCur->phnNext)
    {
    }

    /*  If we did not find the entry report it
     */

    if (PSLNULL == phnCur)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Return the value if requested
     */

    if (PSLNULL != paValue)
    {
        *paValue = phnCur->aValue;
    }
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLBktHashRemove
 *
 *  Removes an entry from the hash
 *
 *  Arguments:
 *      PSLBKTHASH      bh                  Hash to modify
 *      PSLPARAM        aKey                Key to remove
 *      PSLPARAM*       paValue             Return buffer for value if requested
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if found, PSLSUCCESS_NOT_FOUND if not
 */

PSLSTATUS PSL_API PSLBktHashRemove(PSLBKTHASH bh, PSLPARAM aKey,
                    PSLPARAM* paValue)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLUINT32       idxHash;
    HASHNODE*       phnCur;
    HASHNODE*       phnLast;
    PSLSTATUS       ps;
    PSLBKTHASHOBJ*  pbho;

    /*  Clear result for safety
     */

    if (PSLNULL != paValue)
    {
        *paValue = 0;
    }

    /*  Validate arguments
     */

    if (PSLNULL == bh)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pbho = (PSLBKTHASHOBJ*)bh;
    PSLASSERT(PSLBKTHASH_COOKIE == pbho->dwCookie);

    /*  Acquire the mutex
     */

    PSLASSERT(PSLNULL != pbho->hMutex);
    ps = PSLMutexAcquire(pbho->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pbho->hMutex;

    /*  Make sure we have work to do in the first place
     */

    if (0 == pbho->cEntries)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Determine what bucket to search
     */

    idxHash = pbho->pfnHash(aKey, pbho->cTableEntries, pbho->aHashParam);
    if (idxHash >= pbho->cTableEntries)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  And walk through it looking for the match
     */

    for (phnLast = PSLNULL, phnCur = pbho->pphnTable[idxHash];
            (PSLNULL != phnCur) && (aKey != phnCur->aKey);
            phnLast = phnCur, phnCur = phnCur->phnNext)
    {
    }

    /*  If we did not find the entry report it
     */

    if (PSLNULL == phnCur)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }
    PSLASSERT(0 < pbho->cEntries);

    /*  Return the value if requested
     */

    if (PSLNULL != paValue)
    {
        *paValue = phnCur->aValue;
    }

    /*  Remove the entry from the list
     */

    if (PSLNULL != phnLast)
    {
        phnLast->phnNext = phnCur->phnNext;
    }
    else
    {
        pbho->pphnTable[idxHash] = phnCur->phnNext;
    }

    /*  Decrement the entry count and reset the cursor on the change
     */

    pbho->cEntries--;
    pbho->phnCursor = PSLNULL;

    /*  And destroy it
     */

    SAFE_FREEHASHNODE(phnCur);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLBktHashGetFirstEntry
 *
 *  Returns the first entry in the hash
 *
 *  Arguments:
 *      PSLBKTHASH      bh                  Bucket hash to use
 *      PSLPARAM*       paKey               First key
 *      PSLPARAM*       paValue             First value
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if present, PSLSUCCESS_NOT_FOUND if not
 */

PSLSTATUS PSL_API PSLBktHashGetFirstEntry(PSLBKTHASH bh, PSLPARAM* paKey,
                    PSLPARAM* paValue)
{
    PSLHANDLE      hMutexRelease = PSLNULL;
    PSLUINT32      idxHash;
    PSLSTATUS      ps;
    PSLBKTHASHOBJ* pbho;

    /*  Clear result for safety
     */

    if (PSLNULL != paKey)
    {
        *paKey = 0;
    }
    if (PSLNULL != paValue)
    {
        *paValue = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == bh) || (PSLNULL == paKey))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pbho = (PSLBKTHASHOBJ*)bh;
    PSLASSERT(PSLBKTHASH_COOKIE == pbho->dwCookie);

    /*  Acquire the mutex
     */

    PSLASSERT(PSLNULL != pbho->hMutex);
    ps = PSLMutexAcquire(pbho->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pbho->hMutex;

    /*  Make sure we have work to do in the first place
     */

    if (0 == pbho->cEntries)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Start walking through the table until we encounter a non-empty bucket
     */

    for (idxHash = 0;
            (idxHash < pbho->cTableEntries) &&
                            (PSLNULL == pbho->pphnTable[idxHash]);
            idxHash++)
    {
    }

    /*  If we get through to this point and we did not find an entry then
     *  something has gone horribly wrong- we should have never gotten here
     *  because we already checked to make sure the hash had items in it.
     */

    PSLASSERT(idxHash < pbho->cTableEntries);
    if (idxHash >= pbho->cTableEntries)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Set the cursor to the first non-empty bucket
     */

    pbho->phnCursor = pbho->pphnTable[idxHash];

    /*  And return the key and value (if requested)
     */

    *paKey = pbho->phnCursor->aKey;
    if (PSLNULL != paValue)
    {
        *paValue = pbho->phnCursor->aValue;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLBktHashGetNextEntry
 *
 *  Returns the next entry in the hash
 *
 *  Arguments:
 *      PSLBKTHASH      bh                  Hash to enumerate
 *      PSLPARAM*       paKey               Return buffer for key
 *      PSLPARAM*       paValue             Return buffer for value
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if available, PSLSUCCESS_NOT_FOUND if at
 *                        end of enumeration, PSLERROR_INVALID_OPERATION if
 *                        cursor is invalid
 */

PSLSTATUS PSL_API PSLBktHashGetNextEntry(PSLBKTHASH bh, PSLPARAM* paKey,
                    PSLPARAM* paValue)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLUINT32       idxHash;
    PSLSTATUS       ps;
    PSLBKTHASHOBJ*  pbho;

    /*  Clear result for safety
     */

    if (PSLNULL != paKey)
    {
        *paKey = 0;
    }
    if (PSLNULL != paValue)
    {
        *paValue = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == bh) || (PSLNULL == paKey))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pbho = (PSLBKTHASHOBJ*)bh;
    PSLASSERT(PSLBKTHASH_COOKIE == pbho->dwCookie);

    /*  Acquire the mutex
     */

    PSLASSERT(PSLNULL != pbho->hMutex);
    ps = PSLMutexAcquire(pbho->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pbho->hMutex;

    /*  If we have no cursor report an error
     */

    if (PSLNULL == pbho->phnCursor)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Handle the simple case- we have more entries in this bucket
     */

    if (PSLNULL != pbho->phnCursor->phnNext)
    {
        pbho->phnCursor = pbho->phnCursor->phnNext;
    }
    else
    {
        /*  We need to look for the next non-empty bucket in the hash-
         *  note that we did not store the hash bucket but we have enough
         *  information in the current key to figure out where we are at
         *  in the table.  Once we know that, we just start looking in the
         *  next bucket
         */

        idxHash = pbho->pfnHash(pbho->phnCursor->aKey, pbho->cTableEntries,
                            pbho->aHashParam);
        if ((idxHash >= pbho->cTableEntries) || ((idxHash + 1) < idxHash))
        {
            ps = PSLERROR_INVALID_RANGE;
            goto Exit;
        }
        for (idxHash++;
                (idxHash < pbho->cTableEntries) &&
                            (PSLNULL == pbho->pphnTable[idxHash]);
                idxHash++)
        {
        }

        /*  If we found the end of the list we are done
         */

        if (idxHash >= pbho->cTableEntries)
        {
            ps = PSLSUCCESS_NOT_FOUND;
            goto Exit;
        }

        /*  Update the cursor to point to the new item
         */

        pbho->phnCursor = pbho->pphnTable[idxHash];
    }

    /*  And return the key and value (if requested)
     */

    *paKey = pbho->phnCursor->aKey;
    if (PSLNULL != paValue)
    {
        *paValue = pbho->phnCursor->aValue;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}

