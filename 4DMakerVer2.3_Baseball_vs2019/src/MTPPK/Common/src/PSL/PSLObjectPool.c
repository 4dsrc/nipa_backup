/*
 *  PSLObjectPool.c
 *
 *  Implements the PSL Object Pool.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"
#include "PSLObjectNode.h"

/*  Local Defines
 */

#define PSLOBJPOOL_COOKIE           'objP'

#define PSLOBJNODE_COOKIE           'objN'

#define PSLPOBJNODE_TO_PSLPOBJ(_p)  (PSLVOID*)((PSLOBJNODE*)(_p) + 1 )

#define PSLPOBJ_TO_PSLPOBJNODE(_p)  (PSLOBJNODE*)((PSLOBJNODE*)(_p) - 1)

#define SAFE_PSLOBJECTPOOLRELEASE(_p) \
if (PSLNULL != (_p)) \
{ \
    _PSLObjectPoolRelease(_p); \
    (_p) = PSLNULL; \
}

/*  Local Types
 */

typedef struct _PSLOBJNODE* PSLPOBJNODE;

typedef struct _PSLOBJNODE
{
#ifdef PSL_ASSERTS
    PSLUINT32               dwCookie;
#endif /*PSL_ASSERTS*/
    PSLPOBJNODE             pObjNext;
    PSLOBJPOOL              objPool;
} PSLOBJNODE;

typedef struct PSLOBJPOOLOBJ
{
#ifdef PSL_ASSERTS
   PSLUINT32        dwCookie;
#endif  /* PSL_ASSERTS */
   PSLHANDLE        hLock;
   PSLUINT32        cRefs;
   PSLUINT32        cObjs;
   PSLUINT32        cbObj;
   PSLUINT32        dwFlags;
   PSLVOID*         pStart;
   PSLVOID*         pCurr;
   PSLOBJNODE*      pStack;
} PSLOBJPOOLOBJ;

/*  Local Functions
 */

static PSLSTATUS _PSLObjectPoolRelease(PSLOBJPOOL objPool);

/*  Local Variables
 */


/*
 *  _PSLObjectPoolRelease
 *
 *  Releasees a reference count on the specified object pool, destroying the
 *  pool when all references are removed
 *
 *  Arguments:
 *      PSLOBJPOOL      objPool             Pool to release
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _PSLObjectPoolRelease(PSLOBJPOOL objPool)
{
    PSLHANDLE      hMutexRelease = PSLNULL;
    PSLHANDLE      hMutexClose = PSLNULL;
    PSLSTATUS      ps;
    PSLOBJPOOLOBJ* pPoolObj;

    /*  Validate arguments
     */

    if (PSLNULL == objPool)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get object pool details from the pool handle
     */

    pPoolObj = (PSLOBJPOOLOBJ*)objPool;
    PSLASSERT(PSLOBJPOOL_COOKIE == pPoolObj->dwCookie);

    /*  Lock the pool if we have a mutex
     */

    if (PSLNULL != pPoolObj->hLock)
    {
        ps = PSLMutexAcquire(pPoolObj->hLock, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexRelease = pPoolObj->hLock;
    }

    /* Determine if the pool can be released
     */

    PSLASSERT(0 < pPoolObj->cRefs);
    pPoolObj->cRefs--;
    if (0 < pPoolObj->cRefs)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Remember we also need to close the lock
     */

    hMutexClose = hMutexRelease;

    /* Free message pool
     */

    PSLMemFree(pPoolObj);
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMUTEXCLOSE(hMutexClose);
    return ps;
}


/*
 *  PSLObjectPoolCreate
 *
 *  Creates an object pool capable of holding the specifed number of objects.
 *
 *  Arguments:
 *      PSLUINT32       cObject             Number of objects in the pool
 *      PSLUINT32       cbObectSize         Size of each object in the pool
 *      PSLUINT32       dwFlags             Flags
 *      PSLOBJBOOL*     pobjPool            Return buffer for pool
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLObjectPoolCreate(PSLUINT32 cObjects,
                    PSLUINT32 cbObjectSize, PSLUINT32 dwFlags,
                    PSLOBJPOOL* pobjPool)
{
    PSLUINT32       cbSize;
    PSLSTATUS       ps;
    PSLOBJPOOLOBJ*  pPoolObj = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pobjPool)
    {
        *pobjPool = PSLNULL;
    }

    /* Validate arguments
     */

    if (((cObjects == 0) && !(PSLOBJPOOLFLAG_ALLOW_ALLOC & dwFlags)) ||
        (cbObjectSize == 0) || (PSLNULL == pobjPool))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine the size of the objects in the pool
     */

    cbSize = PSLAllocAlign(cbObjectSize + sizeof(PSLOBJNODE), PSLGetAlignment());
    if (cbSize < cbObjectSize)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Allocate memory for the pool- include the pool object, and the
     *  desired number of object nodes
     */

    pPoolObj = (PSLOBJPOOLOBJ*)PSLMemAlloc(
                            (PSLOBJPOOLFLAG_ZERO_MEMORY & dwFlags) ?
                                    PSLMEM_PTR : PSLMEM_FIXED,
                            (sizeof(PSLOBJPOOLOBJ) + (cObjects * cbSize)));
    if (PSLNULL == pPoolObj)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /* Stash the cookie in the pool */

    pPoolObj->dwCookie = PSLOBJPOOL_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Fill object pool structure with object information
     *
     *  WARNING: Object pool may not be allocated with zero initialization- make
     *  sure that everything is initialized here before failure could occur
     */

    pPoolObj->cRefs = 1;
    pPoolObj->cObjs = cObjects;
    pPoolObj->cbObj = cbSize;
    pPoolObj->dwFlags = dwFlags;
    pPoolObj->pStart = (PSLOBJNODE*)(pPoolObj + 1);
    pPoolObj->pCurr = pPoolObj->pStart;
    pPoolObj->pStack = PSLNULL;

    /*  Create lock object for created message pool
     */

    ps = PSLMutexOpen(PSLFALSE, PSLNULL, &pPoolObj->hLock);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /* Return object pool object
     */

    *pobjPool = (PSLOBJPOOL)pPoolObj;
    pPoolObj = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLOBJECTPOOLDESTROY(pPoolObj);
    return ps;
}


/*
 *  PSLObjectPoolDestroy
 *
 *  Public API for destroying the object pool- the pool itself will not
 *  destroy until all objects in the pool are removed
 *
 *  Arguments:
 *      PSLOBJPOOL      objPool             Object pool to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLObjectPoolDestroy(PSLOBJPOOL objPool)
{
    return _PSLObjectPoolRelease(objPool);
}


/*
 *  PSLObjectPoolAlloc
 *
 *  Allocates an item out of the object pool- actually doing a full allocation
 *  if neccessary
 *
 *  Arguments:
 *      PSLOBJPOOL      objPool             Object pool to allocate from
 *      PSLVOID**       ppvObj              Return buffer for object pointer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLObjectPoolAlloc(PSLOBJPOOL objPool, PSLVOID** ppvObj)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    PSLOBJPOOLOBJ*  pPoolObj;
    PSLOBJNODE*     pObjAlloc = PSLNULL;
    PSLOBJNODE*     pObjNode;

    /*  Clear result for safety
     */

    if (ppvObj)
    {
        *ppvObj = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == objPool) || (PSLNULL == ppvObj))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the object pool information
     */

    pPoolObj = (PSLOBJPOOLOBJ*)objPool;
    PSLASSERT(PSLOBJPOOL_COOKIE == pPoolObj->dwCookie);

    /*  And lock the pool
     */

    ps = PSLMutexAcquire(pPoolObj->hLock, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pPoolObj->hLock;

    /*  Check the stack for available objects, if so then retrive the
     *  object from it, otherwise retrive the message from the pool.
     */

    if (PSLNULL != pPoolObj->pStack)
    {
        /*  Retrieve message from the stack
         */

        pObjNode = pPoolObj->pStack;
        pPoolObj->pStack = pObjNode->pObjNext;
        pObjNode->pObjNext = PSLNULL;
    }
    else if (((PSLSIZET)pPoolObj->pCurr - (PSLSIZET)pPoolObj->pStart) <
                            (pPoolObj->cObjs * pPoolObj->cbObj))
    {
        /*  Space for the object remains in the pool- extract the next one
         */

        pObjNode = (PSLOBJNODE*)pPoolObj->pCurr;
        pPoolObj->pCurr = (PSLBYTE*)pPoolObj->pCurr + pPoolObj->cbObj;
    }
    else if (PSLOBJPOOLFLAG_ZERO_MEMORY & pPoolObj->dwFlags)
    {
        /*  Allocate a new object to fulfill the request
         */

        pObjAlloc = (PSLOBJNODE*)PSLMemAlloc(PSLMEM_PTR, pPoolObj->cbObj);
        if (PSLNULL == pObjAlloc)
        {
            ps = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }
        pObjNode = pObjAlloc;
    }
    else
    {
        /*  Report not available
         */

        PSLTraceError("PSLObjectPoolAlloc- Object Pool 0x%08x exhausted",
                            objPool);
        ps = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Increment the reference count on the message pool
     *
     *  WARNING:
     *  If the function exits early for any reason beyond this point
     *  we will have an invalid reference count on the pool.
     */

#ifdef PSL_ASSERTS
    pObjNode->dwCookie = PSLOBJNODE_COOKIE;
#endif /*PSL_ASSERTS*/

    /*  Initialize the Pool ID so that the message can be returned to the
     *  correct pool- if we allocated the object then just allow it to be
     *  released directly
     */

    if (PSLNULL == pObjAlloc)
    {
        pPoolObj->cRefs++;
        pObjNode->objPool = objPool;
    }
    else
    {
        /*  Objects are cleared upon free- this should already be PSLNULL
         */

        PSLASSERT(PSLNULL == pObjNode->objPool);
    }

    /*  Return the object
     */

    *ppvObj = PSLPOBJNODE_TO_PSLPOBJ(pObjNode);
    pObjAlloc = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMEMFREE(pObjAlloc);
    return ps;
}


/*
 *  PSLObjectPoolFree
 *
 *  Releases an object back into the pool.
 *
 *  Arguments:
 *      PSLVOID*        pvObj               Object to free
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLObjectPoolFree(PSLVOID* pvObj)
{
    PSLHANDLE      hMutexRelease = PSLNULL;
    PSLSTATUS      ps;
    PSLOBJNODE*    pObjNode;
    PSLOBJPOOLOBJ* pPoolObj;
    PSLOBJPOOLOBJ* pPoolObjRelease = PSLNULL;
#ifdef PSL_ASSERTS
    PSLOBJNODE*    pObjTest;
#endif  /* PSL_ASSERTS */

    /*  Validate arguments
     */

    if (PSLNULL == pvObj)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get reference to object node so that we can extract information
     */

    pObjNode = PSLPOBJ_TO_PSLPOBJNODE(pvObj);
    PSLASSERT(PSLOBJNODE_COOKIE == pObjNode->dwCookie);

    /*  Get pool ID associated with the object node
     */

    pPoolObj = (PSLOBJPOOLOBJ*)(pObjNode->objPool);

    /*  Determine if message came from a pool or was allocated directly
     */

    if (PSLNULL != pPoolObj)
    {
        /*  Validate message pool
         */

        PSLASSERT(PSLOBJPOOL_COOKIE == pPoolObj->dwCookie);

        /*  Lock message pool
         */

        ps = PSLMutexAcquire(pPoolObj->hLock, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexRelease = pPoolObj->hLock;

#ifdef PSL_ASSERTS
        /*  Make sure this message has not been double free'd
         */

        for (pObjTest = pPoolObj->pStack;
                PSLNULL != pObjTest;
                pObjTest = pObjTest->pObjNext)
        {
            PSLASSERT(pObjTest != pObjNode);
        }
#endif  /* PSL_ASSERTS */

        /*  Clear the node if requested
         */

        if (PSLOBJPOOLFLAG_ZERO_MEMORY & pPoolObj->dwFlags)
        {
            PSLZeroMemory(pObjNode, pPoolObj->cbObj);
        }

        pObjNode->pObjNext = pPoolObj->pStack;
        pPoolObj->pStack = pObjNode;

        /*  Remember that we need to release the message pool
         */

        pPoolObjRelease = pPoolObj;
    }
    else
    {
        PSLMemFree(pObjNode);
    }

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLOBJECTPOOLRELEASE(pPoolObjRelease);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  PSLObjectNodeAlloc
 *
 *  Allocates an object node compatible with the object pool system but
 *  does not actually associate it with an object pool.
 *
 *  Arguments:
 *      PSLUINT32       cbObj               Size of the object to allocate
 *      PSLVOID**       ppvObj              Resulting object
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLObjectNodeAlloc(PSLUINT32 cbObj, PSLVOID** ppvObj)
{
    PSLUINT32       cbSize;
    PSLSTATUS       ps;
    PSLOBJNODE*     pObjNode = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != ppvObj)
    {
        *ppvObj = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((0 == cbObj) || (PSLNULL == ppvObj))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate the object and the associated header
     */

    cbSize = cbObj + sizeof(PSLOBJNODE);
    if (cbSize < cbObj)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Allocate a new object to fulfill the request
     */

    pObjNode = (PSLOBJNODE*)PSLMemAlloc(PSLMEM_PTR, cbSize);
    if (PSLNULL == pObjNode)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /*  Increment the reference count on the message pool
     *
     *  WARNING:
     *  If the function exits early for any reason beyond this point
     *  we will have an invalid reference count on the pool.
     */

#ifdef PSL_ASSERTS
    pObjNode->dwCookie = PSLOBJNODE_COOKIE;
#endif /*PSL_ASSERTS*/
    pObjNode->objPool = PSLNULL;

    /*  Return the object
     */

    *ppvObj = PSLPOBJNODE_TO_PSLPOBJ(pObjNode);
    pObjNode = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMEMFREE(pObjNode);
    return ps;
}

