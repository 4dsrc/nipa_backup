/*
 *  PSLMsgPool.c
 *
 *  Contains the message pool functions implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"
#include "PSLObjectNode.h"
#include "PSLMsgNode.h"

/*  Local Defines
 */

#define PSLMSGPOOL_COOKIE       'msgP'

/*  Local Types
 */

/*  Local Functions
 */

/*  Local Variables
 */

#ifdef PSL_MSG_TRACE
static const PSLWCHAR       _RgchTraceMutex[] = L"PSLMsgTrace Mutex";
static PSLBKTHASH           _BkhTraceMsgs = PSLNULL;
#endif  /* PSL_MSG_TRACE */

/*
 *  PSLMsgPoolCreate
 *
 *  Creates an instance of a message pool capapble of holding the specified
 *  number of messages.
 *
 *  Arguments:
 *      PSLUINT32       cMsg                Number of messages in the pool
 *      PSLMSGPOOL*     pmpID               Message pool ID
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLMsgPoolCreate(PSLUINT32 cMsg, PSLMSGPOOL* pmpID)
{
    /*  This is just a standard object pool- no need to do any additional
     *  work here other then provide the size
     */

    return PSLObjectPoolCreate(cMsg, sizeof(PSLMSGNODE),
                            PSLOBJPOOLFLAG_ZERO_MEMORY, pmpID);
}


/*
 *  PSLMsgPoolDestroy
 *
 *  Destroys the message pool object
 *
 */

PSLSTATUS PSL_API PSLMsgPoolDestroy (PSLMSGPOOL mpID)
{
    return PSLObjectPoolDestroy(mpID);
}


/*
 *  PSLMsgAlloc
 *
 *  Allocates a message from the specified pool.  If no pool is specified,
 *  the message is simply allocated.
 *
 *  Arguments:
 *      PSLMSGPOOL      mpID                Message pool to allocate from
 *      PSLMSG**        ppMsg               Message allocated
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLMsgAlloc(PSLMSGPOOL mpID, PSLMSG** ppMsg)
{
    PSLSTATUS       ps;
    PSLMSGNODE*     pMsgNode;
    PSLMSGNODE*     pMsgAlloc = PSLNULL;

    /*  Clear result for safety
     */

    if (ppMsg)
    {
        *ppMsg = PSLNULL;
    }

    /*  Validate arguments
     */

    if (PSLNULL == ppMsg)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  See if we have a pool to pull from or if we are just creating an
     *  unassociated message
     */

    if (PSLNULL != mpID)
    {
        /*  We were provided a pool to use- retrieve the object from the
         *  pool
         */

        ps = PSLObjectPoolAlloc(mpID, &pMsgNode);
        if (PSL_FAILED(ps))
        {
            /*  Provide some information on the failure
             */

            PSLTraceError("PSLMsgAlloc- Pool 0x%08x failed to return object"
                            "(ps = 0x%08x)", mpID, ps);
#ifdef PSL_MSG_TRACE
            /*  If message tracing is available see if we can provide
             *  some details about where the messages are at
             */

            PSLMsgTraceReport(mpID, PSLFALSE);
#endif  /* PSL_MSG_TRACE */
            goto Exit;
        }
    }
    else
    {
        /*  Dynamically allocate the message and an object pool header so
         *  that release operates correctly
         */

        ps = PSLObjectNodeAlloc(sizeof(*pMsgNode), &pMsgAlloc);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsgNode = pMsgAlloc;
    }

    /*  Add in the message header information
     */

#ifdef PSL_ASSERTS
    pMsgNode->msgHdr.dwCookie = PSLMSG_COOKIE;
#endif  /* PSL_ASSERTS */
#ifdef PSL_MSG_TRACE
    pMsgNode->msgHdr.mpID = mpID;
#endif  /* PSL_MSG_TRACE */

    /*  Return message
     */

    *ppMsg = PSLPMSGNODE_TO_PSLPMSG(pMsgNode);
    pMsgAlloc = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLOBJECTPOOLFREE(pMsgAlloc);
    return ps;
}


/*
 *  PSLMsgFree
 *
 *  Releases a message back to the message pool after performing any necessary
 *  cleanup operations (including message tracking and releasing any PSLMsgSend
 *  queues).
 *
 *  Arguments:
 *      PSLMSG          pMsg                Message to free
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLMsgFree(PSLMSG* pMsg)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    PSLMSGNODE*     pMsgNode;

    /*  Validate arguments
     */

    if (PSLNULL == pMsg)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get reference to message node
     */

    pMsgNode = PSLPMSG_TO_PSLPMSGNODE(pMsg);
    PSLASSERT(PSLMSG_COOKIE == pMsgNode->msgHdr.dwCookie);

    /*  Regardless of whether we succesfully free the message or not we want
     *  to make sure that we release any blocked message queues so signal
     *  the end of message handling if necessary
     */

    if (PSLNULL != pMsgNode->msgHdr.hSignal)
    {
        PSLSignalSet(pMsgNode->msgHdr.hSignal);
    }

#ifdef PSL_MSG_TRACE
    /*  Check to see if this item is being traced
     */

    if (pMsgNode->msgHdr.fMsgTrace)
    {
        PSLHANDLE           hMutexTrace = PSLNULL;
        PSLSTATUS           ps;

        /*  Acquire the trace mutex
         */

        ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchTraceMutex,
                            &hMutexTrace);
        if (PSL_SUCCEEDED(ps) && (PSLNULL != _BkhTraceMsgs))
        {
            /*  Remove the item from the hash
             */

            ps = PSLBktHashRemove(_BkhTraceMsgs, (PSLPARAM)pMsg, PSLNULL);
            PSLASSERT(PSLSUCCESS == ps);
        }
        SAFE_PSLMUTEXRELEASECLOSE(hMutexTrace);
        pMsgNode->msgHdr.fMsgTrace = PSLFALSE;
    }
#endif  /* PSL_MSG_TRACE */

    /*  Release the object
     */

    ps = PSLObjectPoolFree(pMsgNode);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


#ifdef PSL_MSG_TRACE

/*
 *  PSLMsgTrace
 *
 *  Enables tracing for the specified message
 *
 *  Arguments:
 *      PSLMSG*         pMsg                Message to trace
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLMsgTrace(PSLMSG* pMsg)
{
    PSLHANDLE       hMutexTrace = PSLNULL;
    PSLSTATUS       ps;
    PSLMSGNODE*     pMsgNode;

    /*  Validate arguments
     */

    if (PSLNULL == pMsg)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Acquire the trace mutex
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchTraceMutex, &hMutexTrace);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Determine if we need to initialize the tracking table
     */

    if (PSLNULL == _BkhTraceMsgs)
    {
        ps = PSLBktHashCreate(16, PSLNULL, PSLNULL, &_BkhTraceMsgs);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Extract the message node
     */

    pMsgNode = PSLPMSG_TO_PSLPMSGNODE(pMsg);
    PSLASSERT(PSLMSG_COOKIE == pMsgNode->msgHdr.dwCookie);

    /*  If the message is already traced then we have no work to do
     */

    PSLASSERT(!pMsgNode->msgHdr.fMsgTrace);
    if (pMsgNode->msgHdr.fMsgTrace)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Add the message to the hash using the message pool as the value so
     *  that we can dump for particular pools
     */

    ps = PSLBktHashInsert(_BkhTraceMsgs, (PSLPARAM)pMsg,
                            (PSLPARAM)pMsgNode->msgHdr.mpID);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Mark the message as traced
     */

    pMsgNode->msgHdr.fMsgTrace = PSLTRUE;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexTrace);
    return ps;
}


/*
 *  PSLMsgTraceReport
 *
 *  Reports the current status of traced messages.  If fReset is provided
 *  the messages indicated are removed from the list as well.
 *
 *  Arguments:
 *      PSLMSGPOOL      mpID                Message pool to trace messages
 *                                            for or PSLNULL to trace all
 *                                            messages
 *      PSLBOOL         fReset              PSLTRUE to remove the messages
 *                                            from the trace
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSLMsgTraceReport(PSLMSGPOOL mpID, PSLBOOL fReset)
{
    PSLUINT32       cItems;
    PSLDYNLIST      dlFree = PSLNULL;
    PSLBOOL         fFirstLine = PSLTRUE;
    PSLHANDLE       hMutexTrace = PSLNULL;
    PSLUINT32       idxFree;
    PSLMSGPOOL      mpIDMsg;
    PSLMSG*         pMsg;
    PSLSTATUS       ps;

    /*  Acquire the trace mutex
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchTraceMutex, &hMutexTrace);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  If we do not have a hash then there are no messages in the trace
     */

    if (PSLNULL == _BkhTraceMsgs)
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Retrieve the number of messages in the trace
     */

    ps = PSLBktHashGetSize(_BkhTraceMsgs, &cItems);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we have items to check
     */

    if (0 == cItems)
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  If we will be resetting the messages removed create a dynamic list
     *  to hold them
     */

    if (fReset && (PSLNULL != mpID))
    {
        ps = PSLDynListCreate(0, PSLFALSE, cItems, &dlFree);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Start walking through the trace list looking for items to report
     */

    for(ps = PSLBktHashGetFirstEntry(_BkhTraceMsgs, (PSLPARAM*)&pMsg,
                            (PSLPARAM*)&mpIDMsg);
            PSLSUCCESS == ps;
            ps = PSLBktHashGetNextEntry(_BkhTraceMsgs, (PSLPARAM*)&pMsg,
                            (PSLPARAM*)&mpIDMsg))
    {
        /*  See if this matches the desired pool to report
         */

        if ((PSLNULL != mpID) && (mpIDMsg != mpID))
        {
            continue;
        }

        /*  Report the message
         */

        if (fFirstLine)
        {
            PSLDebugPrintf("**PSLMsgTrace Tracked Messages**");
            fFirstLine = PSLFALSE;
        }
        PSLDebugPrintf("\tMsg: 0x%08x\tMsgPool: 0x%08x\tMsgID: 0x%08x",
                            pMsg, mpIDMsg, pMsg->msgID);
        PSLDebugPrintf("\t\ta0: 0x%08x\ta1: 0x%08x\ta2: 0x%08x\ta3: 0x%08x",
                            pMsg->aParam0, pMsg->aParam1, pMsg->aParam2,
                            pMsg->aParam3);
        PSLDebugPrintf("\t\tal: 0x%016I64x", pMsg->alParam);

        /*  If we are resetting the contents remember this message
         */

        if (fReset && (PSLNULL != mpID))
        {
            ps = PSLDynListAddItem(dlFree, (PSLPARAM)pMsg, PSLNULL);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
    }

    /*  See if we need to go back and remove some entries from the list
     */

    if (fReset)
    {
        /*  See if we need to clear specific entries or all entries from the
         *  list
         */

        if (PSLNULL != dlFree)
        {
            /*  Walk through the items we have stashed that need to be removed
             *  and pull them out of the hash
             */

            ps = PSLDynListGetSize(dlFree, &idxFree);
            if (idxFree != cItems)
            {
                for (idxFree; (0 < idxFree); idxFree--)
                {
                    /*  Retrieve the item out of the free list
                     */

                    ps = PSLDynListGetItem(dlFree, (idxFree - 1),
                            (PSLPARAM*)&pMsg);
                    if (PSL_FAILED(ps))
                    {
                        /*  This should just be a simple array access
                         */

                        PSLASSERT(PSLFALSE);
                        continue;
                    }

                    /*  And free the item from the hash
                     */

                    ps = PSLBktHashRemove(_BkhTraceMsgs, (PSLPARAM)pMsg,
                            PSLNULL);
                    PSLASSERT(PSL_SUCCEEDED(ps));
                }
            }
            else
            {
                /*  We are freeing everything- no sense walking through when
                 *  we can just free the list at once
                 */

                SAFE_PSLDYNLISTDESTROY(dlFree);
            }
        }

        /*  If we do not have a list of items to free then release the
         *  entire hash to free everything
         */

        if (PSLNULL == dlFree)
        {
            SAFE_PSLBKTHASHDESTROY(_BkhTraceMsgs);
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexTrace);
    SAFE_PSLDYNLISTDESTROY(dlFree);
    return ps;
}

#endif  /* PSL_MSG_TRACE */

