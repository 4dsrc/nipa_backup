/*
 *  MTPFullEnumSyncService.c
 *
 *  Contains the implementation of full enumeration sync service
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPSyncServicePrecomp.h"
#include "MTPServiceContext.h"

/* Local Defintions */

#define GET_FULLENUMSYNC_PROPINDEX(_pc) \
        ((_pc) - FULLENUMSYNCSVC_MIN_SERVICE_PROPERTIES)

#define GET_FULLENUMSYNC_FORMATINDEX(_pc) \
        ((_pc) - FULLENUMSYNCSVC_MIN_SERVICE_FORMATS)

/* Local Types */

/* Local Functions */

/* Local Variables */

/*  Keep the property enumerations in sync with the property list in the header
 *  and with each other.
 */

static PSL_CONST PSLUINT8               _RgbSyncObjectRefs[] =
{
    ENUM_SyncSvc_SyncObjectReferencesDisabled,
    ENUM_SyncSvc_SyncObjectReferencesEnabled
};

static PSL_CONST MTPENUMFORM            _EfSyncObjectRefs =
{
    PSLARRAYSIZE(_RgbSyncObjectRefs),
    _RgbSyncObjectRefs
};

static PSL_CONST MTPSERVICEPROPERTYREC  _RgFESyncSvcProps[] =
{
    {
        /*  FULLENUMSYNCPROP_FORMAT */
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT128,
            MTP_FORMFLAG_NONE,
            PSLNULL
        },
        &PKEY_FullEnumSyncSvc_SyncFormat,
        NAME_FullEnumSyncSvc_SyncFormat
    },
    {
        /*  FULLENUMSYNCPROP_VERSION_PROPS */
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_AUINT8,
            MTP_FORMFLAG_NONE,
            PSLNULL
        },
        &PKEY_FullEnumSyncSvc_VersionProps,
        NAME_FullEnumSyncSvc_VersionProps
    },
    {
        /* FULLENUMSYNCPROP_LOCAL_ONLY_DELETE */
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT8,
            MTP_FORMFLAG_NONE,
            0
        },
        &PKEY_FullEnumSyncSvc_LocalOnlyDelete,
        NAME_FullEnumSyncSvc_LocalOnlyDelete
    },
    {
        /* FULLENUMSYNCPROP_SYNC_FILTER */
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT8,
            MTP_FORMFLAG_NONE,
            PSLNULL
        },
        &PKEY_FullEnumSyncSvc_FilterType,
        NAME_FullEnumSyncSvc_FilterType
    },
    {
        /* FULLENUMSYNCPROP_REPLICA_ID */
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT128,
            MTP_FORMFLAG_NONE,
            PSLNULL
        },
        &PKEY_FullEnumSyncSvc_ReplicaID,
        NAME_FullEnumSyncSvc_ReplicaID
    },
    {
        /* FULLENUMSYNCPROP_KNOWLEDGE_OBJECT_ID */
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT32,
            MTP_FORMFLAG_OBJECTID,
            PSLNULL
        },
        &PKEY_FullEnumSyncSvc_KnowledgeObjectID,
        NAME_FullEnumSyncSvc_KnowledgeObjectID
    },
    {
        /* FULLENUMSYNCPROP_LAST_SYNC_PROXY_ID */
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT128,
            MTP_FORMFLAG_NONE,
            PSLNULL
        },
        &PKEY_FullEnumSyncSvc_LastSyncProxyID,
        NAME_FullEnumSyncSvc_LastSyncProxyID
    },
    {
        /* FULLENUMSYNCPROP_SYNC_OBJECT_REFERENCES */
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT8,
            MTP_FORMFLAG_ENUM,
            &_EfSyncObjectRefs
        },
        &PKEY_SyncSvc_SyncObjectReferences,
        NAME_SyncSvc_SyncObjectReferences
    }
};

static PSL_CONST MTPSERVICEPROPERTYINFO _RgFESyncSvcPropInfo[] =
{
    {
        (PSL_CONST MTPPROPERTYREC*)&(_RgFESyncSvcProps[
            GET_FULLENUMSYNC_PROPINDEX(FULLENUMSYNCPROP_FORMAT)]),
        FULLENUMSYNCPROP_FORMAT,
        MTP_PROPGETSET_GETONLY,
        PSLNULL,
        PSLNULL,
        PSLNULL,
        PSLNULL,
        0,
    },
    {
        (PSL_CONST MTPPROPERTYREC*)&(_RgFESyncSvcProps[
            GET_FULLENUMSYNC_PROPINDEX(FULLENUMSYNCPROP_VERSION_PROPS)]),
        FULLENUMSYNCPROP_VERSION_PROPS,
        MTP_PROPGETSET_GETONLY,
        PSLNULL,
        PSLNULL,
        PSLNULL,
        PSLNULL,
        0,
    },
    {
        (PSL_CONST MTPPROPERTYREC*)&(_RgFESyncSvcProps[
            GET_FULLENUMSYNC_PROPINDEX(FULLENUMSYNCPROP_LOCAL_ONLY_DELETE)]),
        FULLENUMSYNCPROP_LOCAL_ONLY_DELETE,
        MTP_PROPGETSET_GETSET,
        PSLNULL,
        PSLNULL,
        PSLNULL,
        0,
    },
    {
        (PSL_CONST MTPPROPERTYREC*)&(_RgFESyncSvcProps[
            GET_FULLENUMSYNC_PROPINDEX(FULLENUMSYNCPROP_SYNC_FILTER)]),
        FULLENUMSYNCPROP_SYNC_FILTER,
        MTP_PROPGETSET_GETSET,
        PSLNULL,
        PSLNULL,
        PSLNULL,
        0,
    },
    {
        (PSL_CONST MTPPROPERTYREC*)&(_RgFESyncSvcProps[
            GET_FULLENUMSYNC_PROPINDEX(FULLENUMSYNCPROP_REPLICA_ID)]),
        FULLENUMSYNCPROP_REPLICA_ID,
        MTP_PROPGETSET_GETSET,
        PSLNULL,
        PSLNULL,
        PSLNULL,
        0,
    },
    {
        (PSL_CONST MTPPROPERTYREC*)&(_RgFESyncSvcProps[
            GET_FULLENUMSYNC_PROPINDEX(FULLENUMSYNCPROP_KNOWLEDGE_OBJECT_ID)]),
        FULLENUMSYNCPROP_KNOWLEDGE_OBJECT_ID,
        MTP_PROPGETSET_GETONLY,
        PSLNULL,
        PSLNULL,
        PSLNULL,
        0,
    },
    {
        (PSL_CONST MTPPROPERTYREC*)&(_RgFESyncSvcProps[
            GET_FULLENUMSYNC_PROPINDEX(FULLENUMSYNCPROP_LAST_SYNC_PROXY_ID)]),
        FULLENUMSYNCPROP_LAST_SYNC_PROXY_ID,
        MTP_PROPGETSET_GETSET,
        PSLNULL,
        PSLNULL,
        PSLNULL,
        0,
    },
    {
        (PSL_CONST MTPPROPERTYREC*)&(_RgFESyncSvcProps[
            GET_FULLENUMSYNC_PROPINDEX(FULLENUMSYNCPROP_SYNC_OBJECT_REFERENCES)]),
        FULLENUMSYNCPROP_SYNC_OBJECT_REFERENCES,
        MTP_PROPGETSET_GETONLY,
        PSLNULL,
        PSLNULL,
        PSLNULL,
        0,
    }
};

//  Last Author Proxy ID Definition

const MTPSERVICEPROPERTYREC RecLastAuthorProxyID =
{
    {
        PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
        MTP_DATATYPE_UINT128,
        MTP_FORMFLAG_NONE,
        PSLNULL
    },
    &PKEY_SyncObj_LastAuthorProxyID,
    NAME_SyncObj_LastAuthorProxyID
};

//  Required object property support for the knowledge format

static PSL_CONST MTPOBJECTPROPINFO      _RgFEKnowledgeObjProp[] =
{
    {
        {
            (const MTPPROPERTYREC*)&RecLastAuthorProxyID,
            MTP_OBJECTPROPCODE_LASTAUTHORPROXYID,
            MTP_PROPGETSET_GETSET,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_STORAGEID,
            MTP_PROPGETSET_GETONLY,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_OBJECTFORMAT,
            MTP_PROPGETSET_GETONLY,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_PROTECTIONSTATUS,
            MTP_PROPGETSET_GETONLY,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_OBJECTSIZE,
            MTP_PROPGETSET_GETSET,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_DATECREATED,
            MTP_PROPGETSET_GETONLY,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_DATEMODIFIED,
            MTP_PROPGETSET_GETONLY,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_PARENT,
            MTP_PROPGETSET_GETONLY,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_PERSISTENTGUID,
            MTP_PROPGETSET_GETONLY,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_NAME,
            MTP_PROPGETSET_GETONLY,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_NONCONSUMABLE,
            MTP_PROPGETSET_GETONLY,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0
        },
        MTP_GROUPCODE_OBJINFO,
    },
};


/*  Keep the format enumeration in sync with the format enumeration
 */

static PSL_CONST MTPSERVICEFORMATREC    _RgFESyncSvcFormatRec[] =
{
    {
        {
            FORMATFLAGS_TYPE_SERVICEOBJECT,
        },
        &FORMAT_FullEnumSyncKnowledge,
        NAME_FullEnumSyncKnowledge,
        L""
    }
};

static PSL_CONST MTPSERVICEFORMATINFO   _RgFESyncSvcFormatInfo[] =
{
    {
        {
            (PSL_CONST MTPFORMATREC*)
                &(_RgFESyncSvcFormatRec[GET_FULLENUMSYNC_FORMATINDEX(
                            FULLENUMSYNCFORMAT_KNOWLEDGE)]),
            FULLENUMSYNCFORMAT_KNOWLEDGE,
            _RgFEKnowledgeObjProp,
            PSLARRAYSIZE(_RgFEKnowledgeObjProp),
            PSLNULL,
            PSLNULL
        },
        0
    }
};


/*  MTP Full Enumeration Service Info Dataset
 */

static PSL_CONST MTPSERVICEINFO         _FullEnumSyncServiceInfo =
{
    MTPSERVICEINFO_VERSION_1,
    &SERVICE_FullEnumSync,
    NAME_FullEnumSyncSvc,
    TYPE_FullEnumSyncSvc,
    PSLNULL,
    0,
    _RgFESyncSvcPropInfo,
    PSLARRAYSIZE(_RgFESyncSvcPropInfo),
    _RgFESyncSvcFormatInfo,
    PSLARRAYSIZE(_RgFESyncSvcFormatInfo),
    PSLNULL,
    0,
    PSLNULL,
    0,
    PSLNULL,
    0
};


static PSL_CONST MTPSERVICEBASEINFO     _FullEnumSyncServiceBaseInfo =
{
    &_FullEnumSyncServiceInfo
};


/*  MTPDeviceFullEnumSyncServiceInit
 *
 *  Creates an instance of the device metadata service and associates it with
 *  the specified context.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             Context to register service in
 *      PSLUINT32       dwServiceID         Service ID to assign to service
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceFullEnumSyncServiceInit(
                    MTPCONTEXT* pmtpctx, PSLUINT32 dwServiceID)
{
    PSLSTATUS           ps;
    MTPSERVICEBASEOBJ*  pso = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (MTP_SERVICEID_UNDEFINED == dwServiceID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create an instance of the MTP Full Enum Sync Service using the
     *  MTPServiceBase implementation
     */

    ps = MTPServiceBaseCreate(dwServiceID, MTP_STORAGEID_UNDEFINED,
                            &SVCPUID_MTPFullEnumSyncService,
                            &_FullEnumSyncServiceBaseInfo,
                            sizeof(*pso), &pso);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Register the service with the context
     */

    ps = MTPServiceRegister(pmtpctx, pso);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pso = PSLNULL;

Exit:
    SAFE_MTPSERVICECLOSE(pso);
    return ps;
}


/*  MTPFullEnumSyncSvcGetServicePropInfo
 *
 *  Retrieve service property decription info.
 *
 *  Arguments:
 *      PSLUINT16       wPropCode           Property ID, MTP_SERVICEPROPCODE_ALL
 *                                            to retrieve all
 *      MTPSERVICEPROPERTYINFO**
 *                      ppServProps         Pointer to return the service
 *                                            property info
 *      PSLUINT32*      pcServProps         Number of properties returned
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if not
 *                      located
 */

PSLSTATUS PSL_API MTPFullEnumSyncSvcGetServicePropInfo(PSLUINT16 wPropCode,
                        PSL_CONST MTPSERVICEPROPERTYINFO** ppServProps,
                        PSLUINT32* pcServProps)
{
    PSLUINT32       idxProp;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != ppServProps)
    {
        *ppServProps = PSLNULL;
    }
    if (PSLNULL != pcServProps)
    {
        *pcServProps = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == ppServProps) || (PSLNULL == pcServProps))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Handle the easy case- they are looking for all properties
     */

    if (MTP_SERVICEPROPCODE_ALL == wPropCode)
    {
        *ppServProps = _RgFESyncSvcPropInfo;
        *pcServProps = PSLARRAYSIZE(_RgFESyncSvcPropInfo);
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  See if we know about the property
     */

    idxProp = GET_FULLENUMSYNC_PROPINDEX(wPropCode);
    if (PSLARRAYSIZE(_RgFESyncSvcPropInfo) <= idxProp)
    {
        /*  Property not located- report not found
         */

        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Make sure we get the right property
     */

    PSLASSERT(_RgFESyncSvcPropInfo[idxProp].wPropCode == wPropCode);
    if (_RgFESyncSvcPropInfo[idxProp].wPropCode != wPropCode)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  And return it
     */

    *ppServProps = &(_RgFESyncSvcPropInfo[idxProp]);
    *pcServProps = 1;
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPFullEnumSyncSvcGetFormatInfo
 *
 *  Retrieve service format decription info.
 *
 *  Arguments:
 *      PSLUINT16       wFormatCode         Format code being requested,
 *                                            MTP_FORMATCODE_NOTUSED to get all
 *      MTPSERVICEFORMATINFO**
 *                      ppServFormats       Pointer to return format information
 *      PSLUINT32*      pcServFormats       Number of formats returned

 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if requested
 *                        property not found
 */

PSLSTATUS PSL_API MTPFullEnumSyncSvcGetFormatInfo(PSLUINT16 wFormatCode,
                        PSL_CONST MTPSERVICEFORMATINFO** ppServFormats,
                        PSLUINT32* pcServFormats)
{
    PSLUINT32       idxFormat;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != ppServFormats)
    {
        *ppServFormats = PSLNULL;
    }
    if (PSLNULL != pcServFormats)
    {
        *pcServFormats = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == ppServFormats) || (PSLNULL == pcServFormats))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Handle the easy case- they are looking for all formats
     */

    if (MTP_FORMATCODE_NOTUSED == wFormatCode)
    {
        *ppServFormats = _RgFESyncSvcFormatInfo;
        *pcServFormats = PSLARRAYSIZE(_RgFESyncSvcFormatInfo);
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  See if we know about the format
     */

    idxFormat = GET_FULLENUMSYNC_FORMATINDEX(wFormatCode);
    if (PSLARRAYSIZE(_RgFESyncSvcFormatInfo) <= idxFormat)
    {
        /*  Format not located- report not found
         */

        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Make sure we get the right format
     */

    PSLASSERT(_RgFESyncSvcFormatInfo[idxFormat].infoFormat.wFormatCode ==
                            wFormatCode);
    if (_RgFESyncSvcFormatInfo[idxFormat].infoFormat.wFormatCode != wFormatCode)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  And return it
     */

    *ppServFormats = &(_RgFESyncSvcFormatInfo[idxFormat]);
    *pcServFormats = 1;
    ps = PSLSUCCESS;

Exit:
    return ps;
}

