/*
 *  MTPStatusService.c
 *
 *  Contains implementations of MTP Status Service utility functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPServiceContext.h"
#include "StatusDeviceService.h"
#include "MTPDeviceStatusService.h"
#include "MTPEventManager.h"

/*  Local Defines
 */

#define MTPSTATUSSVCBASEINFO_COOKIE     'stsS'
#define MTPSTATUSSVCOBJ_COOKIE          'stsO'

enum
{
    STATUSSVCPROP_SIGNALSTRENGTH = 0,
    STATUSSVCPROP_TEXTMESSAGES,
    STATUSSVCPROP_NEWPICTURES,
    STATUSSVCPROP_MISSEDCALLS,
    STATUSSVCPROP_VOICEMAIL,
    STATUSSVCPROP_NETWORKNAME,
    STATUSSVCPROP_NETWORKTYPE,
    STATUSSVCPROP_ROAMING,
    STATUSSVCPROP_BATTERYLIFE,
    STATUSSVCPROP_CHARGINGSTATE,
    STATUSSVCPROP_STORAGECAPACITY,
    STATUSSVCPROP_STORAGEFREESPACE,
    STATUSSVCPROP_MAXPROPS
};

/*  Local Types
 */

typedef struct _MTPSTATUSSVCBASEINFO
{
    MTPSERVICEBASEINFO                  mtpSBI;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    MTPSERVICEINFO                      siStatus;
} MTPSTATUSSVCBASEINFO;

typedef struct _MTPSTATUSSVCOBJ
{
    MTPSERVICEBASEOBJ                   mtpSBO;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    MTPCONTEXT*                         pmtpctx;
    PSLHANDLE                           hMutex;
    PSLBOOL                             fEnableNotifications;
} MTPSTATUSSVCOBJ;

/*  Local Functions
 */

static PSLSTATUS PSL_API _MTPDeviceStatusServiceClose(
                    MTPSERVICE mtpService);

static PSLSTATUS PSL_API _MTPDeviceStatusServiceGetProp(
                    PSLUINT16 wPropCode,
                    PSLUINT16 wPropType,
                    PSLPARAM aContext,
                    PSLPARAM aContextProp,
                    PSLVOID* pBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);


/*  Local Variables
 */

static PSL_CONST MTPSERVICEVTBL         _VtblMTPStatusService =
{
    _MTPDeviceStatusServiceClose,
    MTPServiceBaseGetServiceID,
    MTPServiceBaseGetStorageID,
    MTPServiceBaseGetFormatsSupported,
    MTPServiceBaseIsFormatSupported,
    MTPServiceBaseContextDataOpen,
    MTPServiceDBContextOpenBase,
};


static PSL_CONST PSLUINT8               _RgbSignalStrength[] =
{
    RANGEMIN_StatusSvc_SignalStrength,
    RANGEMAX_StatusSvc_SignalStrength,
    RANGESTEP_StatusSvc_SignalStrength
};

static PSL_CONST PSLUINT8               _RgbRoaming[] =
{
    ENUM_StatusSvc_RoamingUnknown,
    ENUM_StatusSvc_RoamingInactive,
    ENUM_StatusSvc_RoamingActive,
};

static PSL_CONST MTPENUMFORM            _EfRoaming =
{
    PSLARRAYSIZE(_RgbRoaming),
    _RgbRoaming
};

static PSL_CONST PSLUINT8               _RgbBatteryLife[] =
{
    RANGEMIN_StatusSvc_BatteryLife,
    RANGEMAX_StatusSvc_BatteryLife,
    RANGESTEP_StatusSvc_BatteryLife,
};

static PSL_CONST PSLUINT8               _RgbCharging[] =
{
    ENUM_StatusSvc_ChargingUnknown,
    ENUM_StatusSvc_ChargingInactive,
    ENUM_StatusSvc_ChargingActive,
};

static PSL_CONST MTPENUMFORM            _EfCharging =
{
    PSLARRAYSIZE(_RgbCharging),
    _RgbCharging
};

/*  NOTE: Keep in sync with the STATUSSVCPROP_* enum
 */

static PSL_CONST MTPSERVICEPROPERTYREC  _RgDefaultStatusProps[STATUSSVCPROP_MAXPROPS] =
{
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT8,
            MTP_FORMFLAG_RANGE,
            _RgbSignalStrength,
        },
        &PKEY_StatusSvc_SignalStrength,
        NAME_StatusSvc_SignalStrength
    },
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT8,
            MTP_FORMFLAG_NONE,
            PSLNULL
        },
        &PKEY_StatusSvc_TextMessages,
        NAME_StatusSvc_TextMessages
    },
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT16,
            MTP_FORMFLAG_NONE,
            PSLNULL,
        },
        &PKEY_StatusSvc_NewPictures,
        NAME_StatusSvc_NewPictures
    },
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT8,
            MTP_FORMFLAG_NONE,
            PSLNULL,
        },
        &PKEY_StatusSvc_MissedCalls,
        NAME_StatusSvc_MissedCalls
    },
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT8,
            MTP_FORMFLAG_NONE,
            PSLNULL,
        },
        &PKEY_StatusSvc_VoiceMail,
        NAME_StatusSvc_VoiceMail
    },
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_STRING,
            MTP_FORMFLAG_NONE,
            PSLNULL,
        },
        &PKEY_StatusSvc_NetworkName,
        NAME_StatusSvc_NetworkName
    },
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_STRING,
            MTP_FORMFLAG_NONE,
            PSLNULL,
        },
        &PKEY_StatusSvc_NetworkType,
        NAME_StatusSvc_NetworkType
    },
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT8,
            MTP_FORMFLAG_ENUM,
            &_EfRoaming,
        },
        &PKEY_StatusSvc_Roaming,
        NAME_StatusSvc_Roaming
    },
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT8,
            MTP_FORMFLAG_RANGE,
            _RgbBatteryLife,
        },
        &PKEY_StatusSvc_BatteryLife,
        NAME_StatusSvc_BatteryLife
    },
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT8,
            MTP_FORMFLAG_ENUM,
            &_EfCharging,
        },
        &PKEY_StatusSvc_ChargingState,
        NAME_StatusSvc_ChargingState
    },
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT64,
            MTP_FORMFLAG_NONE,
            PSLNULL,
        },
        &PKEY_StatusSvc_StorageCapacity,
        NAME_StatusSvc_StorageCapacity
    },
    {
        {
            PROPFLAGS_TYPE_SERVICE,
            MTP_DATATYPE_UINT64,
            MTP_FORMFLAG_NONE,
            PSLNULL,
        },
        &PKEY_StatusSvc_StorageFreeSpace,
        NAME_StatusSvc_StorageFreeSpace
    },
};

static PSL_CONST MTPSERVICEINFO         _StatusServiceInfoBase =
{
    MTPSERVICEINFO_VERSION_1,
    &SERVICE_Status,
    NAME_StatusSvc,
    TYPE_StatusSvc,
    PSLNULL,
    0,
    PSLNULL,
    0,
    PSLNULL,
    0,
    PSLNULL,
    0,
    PSLNULL,
    0,
    PSLNULL,
    0,
};


/*  _MTPDeviceStatusServiceClose
 *
 *  Closes the device status service releasing all references to properties
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          Service to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDeviceStatusServiceClose(MTPSERVICE mtpService)
{
    PSLHANDLE               hMutex = PSLNULL;
    PSLUINT32               idxProp;
    PSLSTATUS               ps;
    MTPSTATUSSVCOBJ*        psso;
    MTPSTATUSSVCBASEINFO*   pssbi = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == mtpService)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    psso = (MTPSTATUSSVCOBJ*)mtpService;
    PSLASSERT(MTPSTATUSSVCOBJ_COOKIE == psso->dwCookie);

    /*  Take the mutex so that we can update the service list
     */

    if (PSLNULL != psso->hMutex)
    {
        ps = PSLMutexAcquire(psso->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutex = psso->hMutex;
    }

    /*  Turn off notifications
     */

    psso->fEnableNotifications = PSLFALSE;

    /*  Walk through the registered properties and release the watch on
     *  each one of them
     */

    for (idxProp = 0;
            idxProp < psso->mtpSBO.pServiceBaseInfo->pServiceInfo->cServiceProps;
            idxProp++)
    {
        ps = MTPStatusServiceRemoveWatch(mtpService, idxProp, (PSLHANDLE)
                            psso->mtpSBO.pServiceBaseInfo->pServiceInfo->
                                rgspiServiceProps[idxProp].aContextProp);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Grab objects that we need to free on exit
     */

    pssbi = (MTPSTATUSSVCBASEINFO*)psso->mtpSBO.pServiceBaseInfo;

    /*  And call the base implementation
     */

    ps = MTPServiceBaseClose(mtpService);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutex);
    SAFE_PSLMEMFREE(pssbi);
    return ps;
}


/*  _MTPDeviceStatusServiceGetProp
 *
 *  Returns a property value from the status service
 *
 *  Arguments:
 *      PSLUINT16       wPropCode           Property code requested
 *      PSLUINT16       wPropType           Data type for property
 *      PSLPARAM        aContext            Context for property request
 *      PSLPARAM        aContextProp        Context for the property data
 *      PSLVOID*        pvBuf               Destination buffer for property
 *      PSLUINT32       cbBuf               Size of the destination buffer
 *      PSLUINT32*      pcbUsed             Amount of destination buffer used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDeviceStatusServiceGetProp(PSLUINT16 wPropCode,
                    PSLUINT16 wPropType, PSLPARAM aContext,
                    PSLPARAM aContextProp, PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed)
{
    PSLSTATUS           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContextProp) || ((0 < cbBuf) && (PSLNULL == pvBuf)) ||
        (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Call directly into the helper function
     */

    ps = MTPStatusPropSerializeValue((PSLHANDLE)aContextProp, wPropType,
                            pvBuf, cbBuf, pcbUsed);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
    wPropCode;
    aContext;
}


/*  MTPDeviceStatusServiceInit
 *
 *  Initializes an instance of the device status service.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             Current MTP session
 *      PSLUINT32       dwServiceID         Service ID to assign to this
 *                                            service
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDeviceStatusServiceInit(MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwServiceID)
{
    PSLUINT32               cProps;
    PSLHANDLE               hMutex = PSLNULL;
    PSLHANDLE               hPropNotify = PSLNULL;
    PSLUINT32               idxProp;
    PSLSTATUS               ps;
    MTPSTATUSSVCBASEINFO*   pssbio;
    MTPSTATUSSVCBASEINFO*   pssbioNew = PSLNULL;
    MTPSTATUSSVCOBJ*        pssNew = PSLNULL;
    MTPSERVICEPROPERTYINFO* pspiCur;

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpctx) || (0 == dwServiceID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate the service base information so that we can generate the
     *  service info data set based on the actual list of supported
     *  properties
     */

    pssbioNew = (MTPSTATUSSVCBASEINFO*)PSLMemAlloc(PSLMEM_PTR,
        sizeof(MTPSTATUSSVCBASEINFO) + (PSLARRAYSIZE(_RgDefaultStatusProps) *
                            sizeof(MTPSERVICEPROPERTYINFO)));
    if (PSLNULL == pssbioNew)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /*  Update the pointers and initialize the base service info data
     */

#ifdef PSL_ASSERTS
    pssbioNew->dwCookie = MTPSTATUSSVCBASEINFO_COOKIE;
#endif  /PSL_ASSERTS
    PSLCopyMemory(&(pssbioNew->siStatus), &_StatusServiceInfoBase,
                            sizeof(pssbioNew->siStatus));
    pssbioNew->mtpSBI.pServiceInfo = &(pssbioNew->siStatus);
    pssbioNew->siStatus.rgspiServiceProps = (MTPSERVICEPROPERTYINFO*)(
                            (PSLBYTE*)pssbioNew + sizeof(MTPSTATUSSVCBASEINFO));

    /*  Create the base service so that we can hand it out to notified
     *  properties
     */

    ps = MTPServiceBaseCreate(dwServiceID, 0, &SERVICE_StatusPersistentID,
                            (MTPSERVICEBASEINFO*)pssbioNew, sizeof(*pssNew),
                            &pssNew);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Update with our function table so that we can handle the close and
     *  transfer ownership of the base service information into the service
     */

#ifdef PSL_ASSERTS
    pssNew->dwCookie = MTPSTATUSSVCOBJ_COOKIE;
#endif  /* PSL_ASSERTS */
    pssNew->mtpSBO.mtpSO.vtbl = &_VtblMTPStatusService;
    pssNew->pmtpctx = pmtpctx;
    PSLASSERT((PSL_CONST MTPSERVICEBASEINFO*)pssbioNew ==
                            pssNew->mtpSBO.pServiceBaseInfo);
    pssbioNew = PSLNULL;
    pssbio = (MTPSTATUSSVCBASEINFO*)pssNew->mtpSBO.pServiceBaseInfo;

    /*  Create a mutex for this service
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, PSLNULL, &pssNew->hMutex);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutex = pssNew->hMutex;

    /*  Walk through and determine which default properties are supproted
     */

    pspiCur = (MTPSERVICEPROPERTYINFO*)pssbio->siStatus.rgspiServiceProps;
    for (idxProp = 0, cProps = 0;
            idxProp < PSLARRAYSIZE(_RgDefaultStatusProps);
            idxProp++)
    {
        /*  Attempt to create a notification for this property
         */
        ps = MTPStatusServiceAddWatch(pssNew,
                            (pspiCur - pssbio->siStatus.rgspiServiceProps),
                            _RgDefaultStatusProps[idxProp].pkeyProp,
                            &hPropNotify);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  If we did not add a notification skip the property
         */

        if (PSLSUCCESS != ps)
        {
            continue;
        }

        /*  We have a valid property- initialize the property information
         *  completely
         */

        pspiCur->wPropCode =
                (PSLUINT16)(pspiCur - pssbio->siStatus.rgspiServiceProps) + 1;
        pspiCur->bGetSet = MTP_PROPGETSET_GETONLY;
        pspiCur->precProp = (PSL_CONST MTPPROPERTYREC*)
                            &(_RgDefaultStatusProps[idxProp]);
        pspiCur->pfnMTPPropGetValue = _MTPDeviceStatusServiceGetProp;
        pspiCur->aContextProp = (PSLPARAM)hPropNotify;
        pssbio->siStatus.cServiceProps = pspiCur->wPropCode;
        pspiCur++;
        hPropNotify = PSLNULL;
    }

    /*  Register the service completely
     */

    ps = MTPServiceRegister(pmtpctx, pssNew);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Enable notifications before handing ownership over to the Service
     *  Resolver
     */

    pssNew->fEnableNotifications = PSLTRUE;
    pssNew = PSLNULL;

Exit:
    /*  Currently there does not appear to be a way for property notifiers
     *  to leak- just asserting here to make sure
     */

    PSLASSERT(PSLNULL == hPropNotify);
    SAFE_PSLMUTEXRELEASE(hMutex);
    SAFE_PSLMEMFREE(pssbioNew);
    SAFE_MTPSERVICECLOSE(pssNew);
    return ps;
}


/*  MTPStatusServiceNotify
 *
 *  Notifies the status service of a property change
 *
 *  Arguments:
 *      MTPSERVICE      mtpsStatus          Status service being notified
 *      PSLPARAM        aParamNotify        Notification parameter
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPStatusServiceNotifyChange(MTPSERVICE mtpsStatus,
                    PSLPARAM aParamNotify)
{
    MTPEVENTOBJECT                      evObj = PSLNULL;
    PSLHANDLE                           hMutex = PSLNULL;
    PSLUINT32                           idxProp;
    PSLSTATUS                           ps;
    MTPSTATUSSVCOBJ*                    psso;
    MTPSTATUSSVCBASEINFO*               pssbio;
    PSL_CONST MTPSERVICEPROPERTYINFO*   pspiCur;

    /*  Validate arguments
     */

    if (PSLNULL == mtpsStatus)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    psso = (MTPSTATUSSVCOBJ*)mtpsStatus;
    PSLASSERT(MTPSTATUSSVCOBJ_COOKIE == psso->dwCookie);
    pssbio = (MTPSTATUSSVCBASEINFO*)psso->mtpSBO.pServiceBaseInfo;

    /*  Acquire the mutex while we send the event
     */

    ps = PSLMutexAcquire(psso->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutex = psso->hMutex;

    /*  Make sure sending events is enabled
     */

    if (!psso->fEnableNotifications)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Determine which property changed and validate the range
     */

    idxProp = (PSLUINT32)aParamNotify;
    PSLASSERT(pssbio->mtpSBI.pServiceInfo->cServiceProps >= idxProp);
    if (pssbio->mtpSBI.pServiceInfo->cServiceProps <= idxProp)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Save some redirection
     */

    pspiCur = &(pssbio->mtpSBI.pServiceInfo->rgspiServiceProps[idxProp]);

    /*  Create the event
     */

    ps = MTPEventObjectCreate(MTP_EVENTCODE_SERVICEPROPCHANGED, 0,
                            psso->mtpSBO.dwServiceID, pspiCur->wPropCode, 0,
                            &evObj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And broadcast the event only on this session
     */

    PSLTraceProgress("MTPStatusServiceNotifyChange: Sending property changed "
                            "notification- Service: 0x%08x\tProperty: %d",
                            psso->mtpSBO.dwServiceID, idxProp);
    ps = MTPEventManagerBroadcastEvent(evObj,
                            MTPEVENTBROADCASTMODE_ONLY_MTPSESSION,
                            psso->pmtpctx);
    if (PSL_FAILED(ps))
    {
        PSLTraceError("MTPStatusServiceNotifyChange: Notification failed 0x%08x",
                            ps);
        goto Exit;
    }
    evObj = PSLNULL;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutex);
    SAFE_MTPEVENTOBJECTDESTROY(evObj);
    return ps;
}

