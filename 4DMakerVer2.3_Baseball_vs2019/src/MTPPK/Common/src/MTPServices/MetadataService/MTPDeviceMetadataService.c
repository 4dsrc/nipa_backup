/*
 *  MTPDeviceMetadataService.c
 *
 *  Contains definition of the MTP Device Metadata Service
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MTPPrecomp.h"
#include "MTPServiceContext.h"
#include "MetadataDeviceService.h"
#include "MTPDeviceMetadataService.h"

/*  Local Defines
 */

#define MTPMETADATASERVICEOBJ_COOKIE        'srvM'

enum
{
    MTP_OBJECTPROPCODE_METADATACONTENTID =
                            MTP_OBJECTPROPCODE_VENDOREXTENSION_FIRST,
    MTP_OBJECTPROPCODE_METADATADEFAULT,
    MTP_OBJECTPROPCODE_METADATALOCALE
};

/*  Local Types
 */

typedef struct _MTPMETADATASERVICEOBJ
{
    MTPDBSERVICEBASEOBJ                 mtpDBSBO;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT32                           idxCur;
    PSL_CONST MTPDEVICEMETADATACABINFO* pCABCur;
    PSLPARAM                            aCABContext;
} MTPMETADATASERVICEOBJ;


/*  Local Functions
 */

static PSLSTATUS PSL_API _MTPDeviceMetadataServiceGetCount(MTPDBCONTEXT mdbc,
                    PSLUINT32* pcItems);

static PSLSTATUS PSL_API _MTPDeviceMetadataServiceGetHandle(MTPDBCONTEXT mdbc,
                    PSLBOOL fResolveContext,
                    PSLUINT32 idxItem,
                    PSLUINT32* pdwObjectHandle,
                    PSLUINT32* pdwParentHandle,
                    PSLUINT32* pdwStorageID);

static PSLSTATUS PSL_API _MTPDeviceMetadataServiceGetItem(MTPDBCONTEXT mdbc,
                    PSLUINT32 idxItem,
                    PSLUINT32 dwFormat,
                    PSLUINT32* pdwObjectID,
                    PSLPARAM* paParamProps,
                    PSL_CONST MTPFORMATINFO** ppfiObject);

static PSLSTATUS PSL_API _MTPDeviceMetadataServiceCloseItem(MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem);

static PSLSTATUS PSL_API _MTPDeviceMetadataServiceCommitItem(MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem);

static PSLSTATUS PSL_API _MTPDeviceMetadataServiceIsPropSupported(
                    MTPDBCONTEXT mdbc,
                    PSLPARAM aParamProps,
                    PSLUINT16 wPropCode,
                    PSLPARAM aContextProp);

static PSLSTATUS PSL_API _MTPDeviceMetadataSerializePropValue(
                    PSLUINT16 wPropCode,
                    PSLUINT16 wPropType,
                    PSLPARAM aContext,
                    PSLPARAM aContextProp,
                    PSLVOID* pBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS PSL_API _MTPDeviceMetadataSerializeObjectBinary(
                    PSLPARAM aContext,
                    PSLUINT16 wFormat,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);

/*  Local Variables
 */

static PSL_CONST MTPDBCONTEXTBASEVTBL   _VtblMTPMetadataServiceDBContext =
{
    MTPDBServiceBaseDBContextClose,
    MTPDBContextCancelBase,
    _MTPDeviceMetadataServiceGetCount,
    _MTPDeviceMetadataServiceGetHandle,
    MTPDBContextCommitBase,
    MTPDBContextCopyBase,
    MTPDBContextMoveBase,
    MTPDBContextDeleteBase,
    MTPDBContextFormatBase,
    MTPDBServiceBaseDBContextDataOpen,
    _MTPDeviceMetadataServiceGetItem,
    _MTPDeviceMetadataServiceCloseItem,
    _MTPDeviceMetadataServiceCommitItem,
    _MTPDeviceMetadataServiceIsPropSupported
};

static PSL_CONST PSLUINT8               _RgbDefaultCAB[] =
{
    ENUM_DeviceMetadataObj_DefaultCABFalse,
    ENUM_DeviceMetadataObj_DefaultCABTrue
};

static PSL_CONST MTPENUMFORM            _EfDefaultCAB =
{
    PSLARRAYSIZE(_RgbDefaultCAB),
    _RgbDefaultCAB
};

static PSL_CONST MTPSERVICEPROPERTYREC  _SrecMetadataContentID =
{
    {
        PROPFLAGS_TYPE_SERVICEOBJECT,
        MTP_DATATYPE_UINT128,
        MTP_FORMFLAG_NONE,
        PSLNULL
    },
    &PKEY_DeviceMetadataObj_ContentID,
    NAME_DeviceMetadataObj_ContentID
};

static PSL_CONST MTPSERVICEPROPERTYREC  _SrecMetadataDefault =
{
    {
        PROPFLAGS_TYPE_SERVICEOBJECT,
        MTP_DATATYPE_UINT8,
        MTP_FORMFLAG_ENUM,
        &_EfDefaultCAB
    },
    &PKEY_DeviceMetadataObj_DefaultCAB,
    NAME_DeviceMetadataObj_DefaultCAB
};

static PSL_CONST MTPOBJECTPROPINFO      _RgMetadataProps[] =
{
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_STORAGEID,
            MTP_PROPGETSET_GETONLY,
            _MTPDeviceMetadataSerializePropValue,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0,
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_OBJECTFORMAT,
            MTP_PROPGETSET_GETONLY,
            _MTPDeviceMetadataSerializePropValue,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0,
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_PROTECTIONSTATUS,
            MTP_PROPGETSET_GETONLY,
            _MTPDeviceMetadataSerializePropValue,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0,
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_OBJECTSIZE,
            MTP_PROPGETSET_GETONLY,
            _MTPDeviceMetadataSerializePropValue,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0,
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_PARENT,
            MTP_PROPGETSET_GETONLY,
            _MTPDeviceMetadataSerializePropValue,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0,
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_PERSISTENTGUID,
            MTP_PROPGETSET_GETONLY,
            _MTPDeviceMetadataSerializePropValue,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0,
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_NAME,
            MTP_PROPGETSET_GETONLY,
            _MTPDeviceMetadataSerializePropValue,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0,
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            PSLNULL,
            MTP_OBJECTPROPCODE_LANGUAGELOCALE,
            MTP_PROPGETSET_GETONLY,
            _MTPDeviceMetadataSerializePropValue,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0,
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            (PSL_CONST MTPPROPERTYREC*)&_SrecMetadataContentID,
            MTP_OBJECTPROPCODE_METADATACONTENTID,
            MTP_PROPGETSET_GETONLY,
            _MTPDeviceMetadataSerializePropValue,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0,
        },
        MTP_GROUPCODE_OBJINFO,
    },
    {
        {
            (PSL_CONST MTPPROPERTYREC*)&_SrecMetadataDefault,
            MTP_OBJECTPROPCODE_METADATADEFAULT,
            MTP_PROPGETSET_GETONLY,
            _MTPDeviceMetadataSerializePropValue,
            PSLNULL,
            PSLNULL,
            PSLNULL,
            0,
        },
        MTP_GROUPCODE_OBJINFO,
    },
};

static PSL_CONST MTPSERVICEFORMATREC    _RgMetadataFormatRec[] =
{
    {
        {
            FORMATFLAGS_TYPE_SERVICEOBJECT,
        },
        &FORMAT_DeviceMetadataCAB,
        NAME_DeviceMetadataCAB,
        L"application/ms-metadata-dxp",
    }
};

static PSL_CONST MTPSERVICEFORMATINFO   _RgMetadataFormats[] =
{
    {
        {
            (PSL_CONST MTPFORMATREC*)&(_RgMetadataFormatRec[0]),
            MTPMETADATASERVICE_FORMATCODE_METADATA,
            _RgMetadataProps,
            PSLARRAYSIZE(_RgMetadataProps),
            _MTPDeviceMetadataSerializeObjectBinary,
            PSLNULL
        },
        0
    }
};

static PSL_CONST MTPSERVICEINFO         _MetadataServiceInfo =
{
    MTPSERVICEINFO_VERSION_1,
    &SERVICE_DeviceMetadata,
    NAME_DeviceMetadataSvc,
    TYPE_DeviceMetadataSvc,
    PSLNULL,
    0,
    PSLNULL,
    0,
    _RgMetadataFormats,
    PSLARRAYSIZE(_RgMetadataFormats),
    PSLNULL,
    0,
    PSLNULL,
    0,
    PSLNULL,
    0
};

static PSL_CONST MTPSTORAGEINFO         _MetadataStorageInfo =
{
    MTP_STORAGETYPE_FIXEDROM,
    MTP_FILESYSTEMTYPE_FLAT,
    MTP_STORAGEACCESSCAPABILITY_R,
    0,
    L"Device Metadata",
    L"Device Metadata"
};

static PSL_CONST MTPDBSERVICEBASEINFO   _MetadataServiceBaseInfo =
{
    &_MetadataServiceInfo,
    &_MetadataStorageInfo,
};


/*  _MTPDeviceMetadataServiceGetCount
 *
 *  Returns the number of objects in the metadata service
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context for this call
 *      PSLUINT32*      pcItems             Number of items
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDeviceMetadataServiceGetCount(MTPDBCONTEXT mdbc,
                    PSLUINT32* pcItems)
{
    PSLUINT32               cItems;
    PSLSTATUS               ps;
    MTPMETADATASERVICEOBJ*  pmdso;

    /*  Clear result for safety
     */

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mdbc) || (PSLNULL == pcItems))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmdso = (MTPMETADATASERVICEOBJ*)MTPDBSERVICEBASEOBJ_FROM_MTPDBCONTEXT(mdbc);
    PSLASSERT(MTPMETADATASERVICEOBJ_COOKIE == pmdso->dwCookie);

    /*  Call the helper function to get the total item count
     */

    ps = MTPDeviceMetadataGetCABCount(&cItems);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Validate the context- this checks to see if we have the correct format
     *  and the correct parent
     */

    if (((MTP_FORMATCODE_NOTUSED != pmdso->mtpDBSBO.mtpDBCO.wCtxFormat) &&
            (MTPMETADATASERVICE_FORMATCODE_METADATA !=
                pmdso->mtpDBSBO.mtpDBCO.wCtxFormat)) ||
        ((MTP_OBJECTHANDLE_ROOT != pmdso->mtpDBSBO.mtpDBCO.dwCtxParentID) &&
            (MTP_OBJECTHANDLE_UNDEFINED !=
                pmdso->mtpDBSBO.mtpDBCO.dwCtxParentID)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  The number of items is either all or 1 depending on the value of
     *  the object ID context
     */

    if ((MTP_OBJECTHANDLE_ALL == pmdso->mtpDBSBO.mtpDBCO.dwCtxObjectID) ||
        (MTP_OBJECTHANDLE_UNDEFINED == pmdso->mtpDBSBO.mtpDBCO.dwCtxObjectID))
    {
        /*  Return all items
         */

        *pcItems = cItems;
    }
    else if (UNPACK_OBJDBID(pmdso->mtpDBSBO.mtpDBCO.dwCtxObjectID) < cItems)
    {
        /*  Everything validates- we have an exact item we care about
         */

        *pcItems = 1;
    }
    else
    {
        /*  Invalid handle specified
         */

        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  _MTPDeviceMetadataServiceGetHandle
 *
 *  Returns object handle information for the specified object
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                MTP DB Context to work in
 *      PSLBOOL         fResolveContext     PSLTRUE to resolve the context
 *      PSLUINT32       idxItem             Index of item being requested
 *      PSLUINT32*      pdwObjectHandle     Object handle for the item
 *      PSLUINT32*      pdwParentHandle     Parent handle for the item
 *      PSLUINT32*      pdwStorageID        Storage ID for the item
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDeviceMetadataServiceGetHandle(MTPDBCONTEXT mdbc,
                    PSLBOOL fResolveContext, PSLUINT32 idxItem,
                    PSLUINT32* pdwObjectHandle, PSLUINT32* pdwParentHandle,
                    PSLUINT32* pdwStorageID)
{
    PSLUINT32               cItems;
    PSLSTATUS               ps;
    MTPMETADATASERVICEOBJ*  pmdso;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwObjectHandle)
    {
        *pdwObjectHandle = MTP_OBJECTHANDLE_UNDEFINED;
    }
    if (PSLNULL != pdwParentHandle)
    {
        *pdwParentHandle = MTP_OBJECTHANDLE_UNDEFINED;
    }
    if (PSLNULL != pdwStorageID)
    {
        *pdwStorageID = MTP_STORAGEID_UNDEFINED;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mdbc) ||
        ((PSLNULL == pdwObjectHandle) && (PSLNULL == pdwParentHandle) &&
                (PSLNULL == pdwStorageID)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmdso = (MTPMETADATASERVICEOBJ*)MTPDBSERVICEBASEOBJ_FROM_MTPDBCONTEXT(mdbc);
    PSLASSERT(MTPMETADATASERVICEOBJ_COOKIE == pmdso->dwCookie);

    /*  Retrieve the number of items in the context and validate the index
     */

    ps = MTPDBContextGetCount(mdbc, &cItems);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    if (cItems <= idxItem)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Return the object handle
     */

    if (PSLNULL != pdwObjectHandle)
    {
        /*  We play some games here- if the count we got back from the
         *  context is larger than 1 than we know we are returning all items
         *  so we just generate the handle directly from the item index
         *  being requested.  Otherwise we have to figure out if we have an
         *  exact object or just all objects with 1 item in the list
         */

        if ((1 < cItems) ||
            (MTP_OBJECTHANDLE_ALL == pmdso->mtpDBSBO.mtpDBCO.dwCtxObjectID) ||
            (MTP_OBJECTHANDLE_UNDEFINED == pmdso->mtpDBSBO.mtpDBCO.dwCtxObjectID))
        {
            /*  Generate the handle from the item count
             */

            *pdwObjectHandle = MAKE_OBJHANDLE(idxItem,
                        GET_STORAGEINDEX(pmdso->mtpDBSBO.mtpSBO.dwStorageID));
        }
        else
        {
            /*  Use the handle we were given
             */

            *pdwObjectHandle = pmdso->mtpDBSBO.mtpDBCO.dwCtxObjectID;
        }
    }

    /*  Objects in the metadata store are always at the root
     */

    PSLASSERT((PSLNULL == pdwParentHandle) ||
            (MTP_OBJECTHANDLE_UNDEFINED == *pdwParentHandle));

    /*  Return the storage ID if requested
     */

    if (PSLNULL != pdwStorageID)
    {
        *pdwStorageID = pmdso->mtpDBSBO.mtpSBO.dwStorageID;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
    fResolveContext;
}


/*  _MTPDeviceMetadataServiceGetItem
 *
 *  Returns the next item in the list
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context for this call
 *      PSLUINT32       idxItem             Index of item in context requested
 *      PSLUINT32       dwFormat            Format of item being requested
 *      PSLUINT32*      pdwObjectID         Object ID of requested item
 *      PSLPARAM*       paParamProps        Property context for callbacks
 *      MTPFORMATINFO*  ppfiObject          Format information for object
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDeviceMetadataServiceGetItem(MTPDBCONTEXT mdbc,
                    PSLUINT32 idxItem, PSLUINT32 dwFormat,
                    PSLUINT32* pdwObjectID, PSLPARAM* paParamProps,
                    PSL_CONST MTPFORMATINFO** ppfiObject)
{
    PSLUINT32               dwObjectID;
    PSLSTATUS               ps;
    MTPMETADATASERVICEOBJ*  pmdso;

    /*  Clear results for safety
     */

    if (PSLNULL != pdwObjectID)
    {
        *pdwObjectID = MTP_OBJECTHANDLE_UNDEFINED;
    }
    if (PSLNULL != paParamProps)
    {
        *paParamProps = PSLNULL;
    }
    if (PSLNULL != ppfiObject)
    {
        *ppfiObject = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mdbc) || (PSLNULL == pdwObjectID) ||
        (PSLNULL == paParamProps) || (PSLNULL == ppfiObject))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  We only support querying for formats
     */

    PSLASSERT(MTPDBCONTEXTDATA_QUERY == MTPDBCONTEXTDATADESC_FLAGS(dwFormat));
    if (MTPDBCONTEXTDATA_QUERY != MTPDBCONTEXTDATADESC_FLAGS(dwFormat))
    {
        ps = PSLERROR_ACCESS_DENIED;
        goto Exit;
    }

    /*  Extract our object
     */

    pmdso = (MTPMETADATASERVICEOBJ*)MTPDBSERVICEBASEOBJ_FROM_MTPDBCONTEXT(mdbc);
    PSLASSERT(MTPMETADATASERVICEOBJ_COOKIE == pmdso->dwCookie);

    /*  We handle validation of the item index when we retrieve the object ID
     *  via a call to *GetHandle so use it here
     */

    ps = _MTPDeviceMetadataServiceGetHandle(mdbc, PSLTRUE, idxItem,
                            &dwObjectID, PSLNULL, PSLNULL);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  We use the ID directly as the index into the metadata store to
     *  retrieve the rest of the information
     */

    ps = MTPDeviceMetadataGetCABInfo(UNPACK_OBJDBID(dwObjectID),
                            &pmdso->pCABCur);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pmdso->idxCur = UNPACK_OBJDBID(dwObjectID);

    /*  All is well- return the information
     */

    *pdwObjectID = dwObjectID;
    *paParamProps = (PSLPARAM)pmdso;
    *ppfiObject = (MTPFORMATINFO*)&(_RgMetadataFormats[0]);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  _MTPDeviceMetadataServiceCloseItem
 *
 *  Closes the specified item
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context for this call
 *      PSLPARAM        aParamItem          Item param to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDeviceMetadataServiceCloseItem(MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem)
{
    PSLSTATUS               ps;
    MTPMETADATASERVICEOBJ*  pmdso;

    /*  Validate Arguments- our item parameters are always the same as the
     *  context object
     */

    if ((PSLNULL == mdbc) ||
        (aParamItem != (PSLPARAM)MTPDBSERVICEBASEOBJ_FROM_MTPDBCONTEXT(mdbc)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmdso = (MTPMETADATASERVICEOBJ*)MTPDBSERVICEBASEOBJ_FROM_MTPDBCONTEXT(mdbc);
    PSLASSERT(MTPMETADATASERVICEOBJ_COOKIE == pmdso->dwCookie);

    /*  Clear our current item pointers
     */

    pmdso->idxCur = 0;
    pmdso->pCABCur = PSLNULL;
    SAFE_MTPDEVICEMETADATACLOSECABOBJECT(pmdso->aCABContext);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  _MTPDeviceMetadataServiceCommitItem
 *
 *  Commits the specified item
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context for this call
 *      PSLPARAM        aParamItem          Item param to commit
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDeviceMetadataServiceCommitItem(MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem)
{
    return ((PSLNULL != mdbc) &&
        (aParamItem == (PSLPARAM)MTPDBSERVICEBASEOBJ_FROM_MTPDBCONTEXT(mdbc))) ?
                            PSLERROR_ACCESS_DENIED : PSLERROR_INVALID_PARAMETER;
}


/*  MTPDBContextBaseIsPropSupportedBase
 *
 *  Reports whether or not the requested property in supported within the
 *  property context current.
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DBContext for call
 *      PSLPARAM        aParamProps         Runtime property context
 *      PSLUINT16       wPropCode           Property code
 *      PSLPARAM        aContextProp        Static property context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if supported, PSLSUCCESS_FALSE if not
 */

PSLSTATUS PSL_API _MTPDeviceMetadataServiceIsPropSupported(MTPDBCONTEXT mdbc,
                    PSLPARAM aParamProps, PSLUINT16 wPropCode,
                    PSLPARAM aContextProp)
{
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if ((PSLNULL == mdbc) || (PSLNULL == aParamProps))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  All requested properties are always supported by the service
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
    wPropCode;
    aContextProp;
}


/*  _MTPDeviceMetadataSerializePropValue
 *
 *  Serializes the requested property value
 *
 *  Arguments:
 *      PSLUINT16       wPropCode           Property to serialize
 *      PSLUINT16       wPropType           Property data type
 *      PSLPARAM        aContext            Runtime context for property
 *      PSLPARAM        aContextProp        Static context for property
 *      PSLVOID*        pvBuf               Destination buffer for property
 *      PSLUINT32       cbBuf               Size of destination buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDeviceMetadataSerializePropValue(PSLUINT16 wPropCode,
                    PSLUINT16 wPropType, PSLPARAM aContext,
                    PSLPARAM aContextProp, PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed)
{
    PSLBOOL                     bSource;
    PSLUINT32                   cbItem;
    static PSL_CONST PSLUINT32  dwParentID = 0;
    PSLSTATUS                   ps;
    MTPMETADATASERVICEOBJ*      pmdso;
    PSLUINT16                   wSource;
    PSL_CONST PSLVOID*          pvSource;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContext) || ((0 < cbBuf) && (PSLNULL == pvBuf)) ||
        (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmdso = (MTPMETADATASERVICEOBJ*)aContext;
    PSLASSERT(MTPMETADATASERVICEOBJ_COOKIE == pmdso->dwCookie);

    /*  Determine which property is being requested
     */

    switch (wPropCode)
    {
    case MTP_OBJECTPROPCODE_METADATADEFAULT:
        /*  Validate type
         */

        if (MTP_DATATYPE_UINT8 != wPropType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Get size and source
         */

        cbItem = sizeof(PSLUINT8);
        bSource = (MTPMETADATASERVICEFLAGS_DEFAULT & pmdso->pCABCur->dwFlags) ?
                            PSLTRUE : PSLFALSE;
        pvSource = &bSource;
        break;

    case MTP_OBJECTPROPCODE_OBJECTFORMAT:
    case MTP_OBJECTPROPCODE_PROTECTIONSTATUS:
        /*  Validate type
         */

        if (MTP_DATATYPE_UINT16 != wPropType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Get size and source
         */

        cbItem = sizeof(PSLUINT16);
        wSource = (PSLUINT16)((MTP_OBJECTPROPCODE_OBJECTFORMAT == wPropCode) ?
                            MTPMETADATASERVICE_FORMATCODE_METADATA :
                            MTP_PROTECTIONSTATUS_READONLY);
        pvSource = &wSource;
        break;

    case MTP_OBJECTPROPCODE_STORAGEID:
    case MTP_OBJECTPROPCODE_PARENT:
        /*  Validate Type
         */

        if (MTP_DATATYPE_UINT32 != wPropType)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        /*  Get size and source
         */

        cbItem = sizeof(PSLUINT32);
        pvSource = (MTP_OBJECTPROPCODE_STORAGEID == wPropCode) ?
                            &(pmdso->mtpDBSBO.mtpSBO.dwStorageID) :
                            &dwParentID;
        break;

    case MTP_OBJECTPROPCODE_OBJECTSIZE:
        /*  Validate Type
         */

        if (MTP_DATATYPE_UINT64 != wPropType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Get size and source
         */

        cbItem = sizeof(PSLUINT64);
        pvSource = &(pmdso->pCABCur->cbCABLength);
        break;

    case MTP_OBJECTPROPCODE_PERSISTENTGUID:
    case MTP_OBJECTPROPCODE_METADATACONTENTID:
        /*  Validate type
         */

        if (MTP_DATATYPE_UINT128 != wPropType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Get size and source
         */

        cbItem = sizeof(PSLUINT128);
        pvSource = pmdso->pCABCur->pguidContentID;
        break;

    case MTP_OBJECTPROPCODE_NAME:
    case MTP_OBJECTPROPCODE_LANGUAGELOCALE:
    case MTP_OBJECTPROPCODE_METADATALOCALE:
        /*  Validate type
         */

        if (MTP_DATATYPE_STRING != wPropType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Set size to 0 (must be calculated) and get source
         */

        cbItem = 0;
        pvSource = pmdso->pCABCur->szLocale;
        break;

    default:
        /*  Unknown property requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Special case handling for special types
     */

    switch (wPropType)
    {
    case MTP_DATATYPE_STRING:
        /*  Serialize the string
         */

        ps = MTPStoreMTPString((PXMTPSTRING)pvBuf, cbBuf, pcbUsed,
                            (PSLCWSTR)pvSource, MAX_MTP_STRING_LEN);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    default:
        /*  Return just size if requested
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = cbItem;
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure we have space to store the item
         */

        if (cbItem > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  Store the value- note that UINT128 values are treated specially
         *  as GUIDs
         */

        if (MTP_DATATYPE_UINT128 != wPropType)
        {
            /*  Store the type
             */

            ps = MTPStoreValue(pvBuf, cbBuf, wPropType, pvSource);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        else
        {
            /*  Store the GUIDs
             */

            PSLASSERT((MTP_OBJECTPROPCODE_PERSISTENTGUID == wPropCode) ||
                    (MTP_OBJECTPROPCODE_METADATACONTENTID == wPropCode));
            MTPStoreGUID((PXPSLGUID)pvBuf, (PSLGUID*)pvSource);
        }

        /*  And report the number of bytes used
         */

        *pcbUsed = cbItem;
        ps = PSLSUCCESS;
        break;
    }

Exit:
    return ps;
    aContextProp;
}


/*  _MTPDeviceMetadataSerializeObjectBinary
 *
 *  Returns the binary data associated with a particular object format.
 *
 *  Arguments:
 *      PSLPARAM        aContext            Item parameter for object being
 *                                            serialized
 *      PSLUINT16       wFormat             Format being serialized
 *      PSLVOID*        pvBuf               Destination for item
 *      PSLUINT32       cbBuf               Size of the serialization buffer
 *      PSLUINT32*      pcbWritten          Number of bytes written
 *      PSLUINT64*      pcbLeft             Number of bytes remaining
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDeviceMetadataSerializeObjectBinary(PSLPARAM aContext,
                    PSLUINT16 wFormat, PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten, PSLUINT64* pcbLeft)
{
    PSLSTATUS               ps;
    MTPMETADATASERVICEOBJ*  pmdso;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }
    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContext) ||
        ((0 < cbBuf) && ((PSLNULL == pvBuf) || (PSLNULL == pcbWritten))) ||
        (PSLNULL == pcbLeft))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmdso = (MTPMETADATASERVICEOBJ*)aContext;
    PSLASSERT(MTPMETADATASERVICEOBJ_COOKIE == pmdso->dwCookie);

    /*  Determine which format is being requested
     */

    switch (wFormat)
    {
    case MTPMETADATASERVICE_FORMATCODE_METADATA:
        /*  See if we need to open the CAB context
         */

        if (MTPMETADATASERVICE_CABCONTEXT_UNKNOWN == pmdso->aCABContext)
        {
            /*  Open the context for the desired CAB
             */

            ps = MTPDeviceMetadataOpenCABObject(pmdso->idxCur,
                            &(pmdso->aCABContext));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }

        /*  Read the next block of data for the desired CAB
         */

        PSLASSERT(MTPMETADATASERVICE_CABCONTEXT_UNKNOWN != pmdso->aCABContext);
        ps = MTPDeviceMetadataReadCABObject(pmdso->aCABContext, pvBuf, cbBuf,
                            pcbWritten, pcbLeft);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    default:
        /*  Unexpected format requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPDeviceMetadataServiceInit
 *
 *  Creates an instance of the device metadata service and associates it with
 *  the specified context.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             Context to register service in
 *      PSLUINT32       dwServiceID         Service ID to assign to service
 *      PSLUINT32       dwStorageID         Storage ID to assign to service
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceMetadataServiceInit(MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwServiceID, PSLUINT32 dwStorageID)
{
    PSLSTATUS               ps;
    MTPMETADATASERVICEOBJ*  pmdso = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (MTP_SERVICEID_UNDEFINED == dwServiceID) ||
        (MTP_STORAGEID_UNDEFINED == dwStorageID) ||
        (MTP_STORAGEID_ALL == dwStorageID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create an instance of the MTP Metadata Service using the MTPServiceBase
     *  implementation
     */

    ps = MTPDBServiceBaseCreate(dwServiceID, dwStorageID,
                            &SVCPUID_MTPDeviceMetadata,
                            &_MetadataServiceBaseInfo,
                            sizeof(*pmdso), &pmdso);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize our data
     */

#ifdef PSL_ASSERTS
    pmdso->dwCookie = MTPMETADATASERVICEOBJ_COOKIE;
#endif  /* PSL_ASSERTS */
    pmdso->aCABContext = MTPMETADATASERVICE_CABCONTEXT_UNKNOWN;

    /*  Add any VTables that we care about
     */

    pmdso->mtpDBSBO.mtpDBCO.mtpDBCBO.vtbl = (PSL_CONST MTPDBCONTEXTVTBL*)
                            &_VtblMTPMetadataServiceDBContext;

    /*  Register the service with the context
     */

    ps = MTPServiceRegister(pmtpctx, pmdso);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pmdso = PSLNULL;

Exit:
    SAFE_MTPSERVICECLOSE(pmdso);
    return ps;
}

