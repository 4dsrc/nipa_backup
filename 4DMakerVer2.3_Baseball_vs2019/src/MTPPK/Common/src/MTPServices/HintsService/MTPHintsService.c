/*
 *  MTPHintsService.c
 *
 *  Contains implementation of MTP Hints Service
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MTPPrecomp.h"
#include "HintsDeviceService.h"
#include "MTPDeviceHintsService.h"
#include "MTPServiceContext.h"

/*  Local Defines
 */

#define MTPHINTSSVCBASEINFO_COOKIE      'htsS'
#define MTPHINTSSVCOBJ_COOKIE           'htsO'

/*  Local Types
 */

typedef struct _MTPHINTSSVCBASEINFO
{
    MTPSERVICEBASEINFO                  mtpSBI;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    MTPSERVICEINFO                      siHints;
} MTPHINTSSVCBASEINFO;

typedef struct _MTPHINTSSVCOBJ
{
    MTPSERVICEBASEOBJ                   mtpSBO;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
} MTPHINTSSVCOBJ;

/*  Local Functions
 */

static PSLSTATUS PSL_API _MTPDeviceHintsServiceClose(
                    MTPSERVICE mtpService);

/*  Local Variables
 */

static PSL_CONST MTPSERVICEVTBL         _VtblMTPHintsService =
{
    _MTPDeviceHintsServiceClose,
    MTPServiceBaseGetServiceID,
    MTPServiceBaseGetStorageID,
    MTPServiceBaseGetFormatsSupported,
    MTPServiceBaseIsFormatSupported,
    MTPServiceBaseContextDataOpen,
    MTPServiceDBContextOpenBase,
};

static PSL_CONST MTPSERVICEINFO         _HintsServiceInfoBase =
{
    MTPSERVICEINFO_VERSION_1,
    &SERVICE_Hints,
    NAME_HintsSvc,
    TYPE_HintsSvc,
    PSLNULL,
    0,
    PSLNULL,
    0,
    PSLNULL,
    0,
    PSLNULL,
    0,
    PSLNULL,
    0,
    PSLNULL,
    0,
};


/*  _MTPDeviceHintsServiceClose
 *
 *  Closes the device Hints service releasing all references to properties
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          Service to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDeviceHintsServiceClose(MTPSERVICE mtpService)
{
    PSLSTATUS               ps;
    MTPHINTSSVCOBJ*         phso;
    MTPHINTSSVCBASEINFO*    phsbi = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == mtpService)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    phso = (MTPHINTSSVCOBJ*)mtpService;
    PSLASSERT(MTPHINTSSVCOBJ_COOKIE == phso->dwCookie);

    /*  Grab objects that we need to free on exit
     */

    phsbi = (MTPHINTSSVCBASEINFO*)phso->mtpSBO.pServiceBaseInfo;

    /*  And call the base implementation
     */

    ps = MTPServiceBaseClose(mtpService);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMEMFREE(phsbi);
    return ps;
}


/*  MTPDeviceHintsServiceInit
 *
 *  Initializes an instance of the device HINTS service.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             Current MTP session
 *      PSLUINT32       dwServiceID         Service ID to assign to this service
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDeviceHintsServiceInit(MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwServiceID)
{
    PSLUINT32               cbData;
    PSLSTATUS               ps;
    MTPHINTSSVCBASEINFO*    phsbioNew = PSLNULL;
    MTPHINTSSVCOBJ*         phsNew = PSLNULL;
    PSLBYTE*                pbData;

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpctx) || (MTP_SERVICEID_UNDEFINED== dwServiceID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  We need to get the size of Hints data to allocate memory
     */

    ps = MTPDeviceHintsServiceGetHints(pmtpctx, PSLNULL, 0, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Allocate a block of data to hold the instance correct version of
     *  the ServiceInfo and the associated data
     */

    phsbioNew = (MTPHINTSSVCBASEINFO*)PSLMemAlloc(PSLMEM_PTR,
                            sizeof(MTPHINTSSVCBASEINFO) + cbData);
    if (PSLNULL == phsbioNew)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /*  Update the pointers and initialize the base service info data
     */

#ifdef PSL_ASSERTS
    phsbioNew->dwCookie = MTPHINTSSVCBASEINFO_COOKIE;
#endif  /* PSL_ASSERTS */
    PSLCopyMemory(&(phsbioNew->siHints), &_HintsServiceInfoBase,
                            sizeof(phsbioNew->siHints));
    phsbioNew->mtpSBI.pServiceInfo = &(phsbioNew->siHints);
    pbData = (PSLBYTE*)phsbioNew + sizeof(MTPHINTSSVCBASEINFO);

    /*  Retrieve the hints data
     */

    ps = MTPDeviceHintsServiceGetHints(pmtpctx, pbData, cbData, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And store it in the ServiceInfo
     */

    phsbioNew->siHints.pbData = pbData;
    phsbioNew->siHints.cbData = cbData;

    /*  Use the service base class to create the actual service
     */

    ps = MTPServiceBaseCreate(dwServiceID, MTP_STORAGEID_UNDEFINED,
                            &SERVICE_HintsPersistentID,
                            (MTPSERVICEBASEINFO*)phsbioNew,
                            sizeof(*phsNew), &phsNew);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Update with our function table so that we can handle the close and
     *  transfer ownership of the base service information into the service
     */

#ifdef PSL_ASSERTS
    phsNew->dwCookie = MTPHINTSSVCOBJ_COOKIE;
#endif  /* PSL_ASSERTS */
    phsNew->mtpSBO.mtpSO.vtbl = &_VtblMTPHintsService;
    PSLASSERT((PSL_CONST MTPSERVICEBASEINFO*)phsbioNew ==
                            phsNew->mtpSBO.pServiceBaseInfo);
    phsbioNew = PSLNULL;

    /*  Register the service with the context
     */

    ps = MTPServiceRegister(pmtpctx, phsNew);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    phsNew = PSLNULL;

Exit:
    SAFE_PSLMEMFREE(phsbioNew);
    SAFE_MTPSERVICECLOSE(phsNew);
    return ps;
}



