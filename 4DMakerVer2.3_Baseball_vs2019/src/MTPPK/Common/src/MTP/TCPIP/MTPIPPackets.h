/*
 *  MTPIPPackets.h
 *
 *  Contains declaration of the different MTP/IP packets.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPIPPACKETS_H_
#define _MTPIPPACKETS_H_

#define MTPIP_MAX_MAC                               8

#define MTPIP_BINARY_PROTOCOL_VERSION               0x00010000

#define MTPIP_TRANSPORT_ID                          0x00

/*  MTP/IP Packet IDs
 *
 *  Type enumeration for MTP/IP packets
 */

enum
{
    MTPIPPACKETID_UNKNOWN =                         0x00,
    MTPIPPACKETID_INIT_COMMAND_REQUEST =            0x01,
    MTPIPPACKETID_INIT_COMMAND_ACK =                0x02,
    MTPIPPACKETID_INIT_EVENT_REQUEST =              0x03,
    MTPIPPACKETID_INIT_EVENT_ACK =                  0x04,
    MTPIPPACKETID_INIT_FAIL =                       0x05,
    MTPIPPACKETID_OPERATION_REQUEST =               0x06,
    MTPIPPACKETID_OPERATION_RESPONSE =              0x07,
    MTPIPPACKETID_EVENT =                           0x08,
    MTPIPPACKETID_START_DATA =                      0x09,
    MTPIPPACKETID_DATA =                            0x0a,
    MTPIPPACKETID_CANCEL =                          0x0b,
    MTPIPPACKETID_END_DATA =                        0x0c,
    MTPIPPACKETID_PROBE_REQUEST =                   0x0d,
    MTPIPPACKETID_PROBE_RESPONSE =                  0x0e,
    MTPIPPACKETID_RESERVED_START =                  0x0f
};


#ifdef PSL_PACK_PRAGMA
#include "PSLPackPush1.h"
#endif

/*  MTPIPINITCOMMANDREQUEST
 *
 *  MTP/IP Init Command Request Packet
 */

struct _MTPIPINITCOMMANDREQUEST
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLGUID                 guidInitiator;
    union
    {
        struct
        {
            PSLWCHAR        szInitiator[MTPIP_MAX_NAME_LENGTH];
            PSLUINT32       dwProtVer;
        } maxFields;
        PSLWCHAR            szInitiatorStart[1];
    } varFields;
} PSL_PACK1;

typedef struct _MTPIPINITCOMMANDREQUEST MTPIPINITCOMMANDREQUEST;

typedef struct _MTPIPINITCOMMANDREQUEST PSL_UNALIGNED*
                            PXMTPIPINITCOMMANDREQUEST;
typedef PSL_CONST struct _MTPIPINITCOMMANDREQUEST PSL_UNALIGNED*
                            PCXMTPIPINITCOMMANDREQUEST;

#define MTPIPINITCOMMANDREQUEST_MIN_SIZE \
    (PSLOFFSETOF(MTPIPINITCOMMANDREQUEST, varFields) + sizeof(PSLWCHAR) + \
                            sizeof(PSLUINT32))

/*  MTPIPINITCOMMANDACK
 *
 *  MTP/IP Init Command Acknowledgement Packet
 */

struct _MTPIPINITCOMMANDACK
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwConnID;
    PSLGUID                 guidResponder;
    union
    {
        struct
        {
            PSLWCHAR        szResponder[MTPIP_MAX_NAME_LENGTH];
            PSLWCHAR        dwProtVer;
        } maxFields;
        PSLWCHAR            szResponderStart[1];
    } varFields;
} PSL_PACK1;

typedef struct _MTPIPINITCOMMANDACK MTPIPINITCOMMANDACK;

typedef struct _MTPIPINITCOMMANDACK PSL_UNALIGNED* PXMTPIPINITCOMMANDACK;
typedef PSL_CONST struct _MTPIPINITCOMMANDACK PSL_UNALIGNED*
                            PCXMTPIPINITCOMMANDACK;

#define MTPIPINITCOMMANDACK_MIN_SIZE \
    (PSLOFFSETOF(MTPIPINITCOMMANDACK, varFields) + sizeof(PSLWCHAR) + \
                            sizeof(PSLUINT32))


/*  MTPIPINITEVENTREQUEST
 *
 *  MTP/IP Init Event Request Packet
 */

struct _MTPIPINITEVENTREQUEST
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwConnID;
} PSL_PACK1;

typedef struct _MTPIPINITEVENTREQUEST MTPIPINITEVENTREQUEST;

typedef struct _MTPIPINITEVENTREQUEST PSL_UNALIGNED* PXMTPIPINITEVENTREQUEST;
typedef PSL_CONST struct _MTPIPINITEVENTREQUEST PSL_UNALIGNED*
                                PCXMTPIPINITEVENTREQUEST;

/*  MTPIPINITEVENTACK
 *
 *  MTP/IP Init Event Acknowledgement Packet
 */

struct _MTPIPINITEVENTACK
{
    MTPSTREAMHEADER         mtpstrmHeader;
} PSL_PACK1;

typedef struct _MTPIPINITEVENTACK MTPIPINITEVENTACK;

typedef struct _MTPIPINITEVENTACK PSL_UNALIGNED* PXMTPIPINITEVENTACK;
typedef PSL_CONST struct _MTPIPINITEVENTACK PSL_UNALIGNED*
                                PCXMTPIPINITEVENTACK;

/*  MTPIPINITFAIL
 *
 *  MTP/IP Init Fail Packet
 */

struct _MTPIPINITFAIL
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwReason;
} PSL_PACK1;

typedef struct _MTPIPINITFAIL MTPIPINITFAIL;

typedef struct _MTPIPINITFAIL PSL_UNALIGNED* PXMTPIPINITFAIL;
typedef PSL_CONST struct _MTPIPINITFAIL PSL_UNALIGNED* PCXMTPIPINITFAIL;


#ifdef PSL_PACK_PRAGMA
#include "PSLPackPop.h"
#endif

#endif  /* _MTPIPPACKETS_H_ */

