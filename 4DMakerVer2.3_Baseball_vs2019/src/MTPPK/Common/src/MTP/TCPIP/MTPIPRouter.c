/*
 *  MTPIPRouter.c
 *
 *  Contains definitions of the MTP/IP Router functionality.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPIPTransportPrecomp.h"
#include "MTPStreamPackets.h"

/*  Local Defines
 */

#define MTPIPROUTEROBJ_COOKIE       'mipR'

/*  Local Types
 */

/*  Local Functions
 */

static PSLSTATUS PSL_API _MTPIPRouterDestroy(MTPIPROUTER mtpip);

static PSLSTATUS PSL_API _MTPIPRouterCreateContext(MTPIPROUTER mtpip,
                    MTPCONTEXT** ppmtpctx);

static PSLSTATUS PSL_API _MTPIPRouterResetDataAvailable(MTPIPROUTER mtpip,
                    MTPSTREAMPARSER mtpstrm);

/*  Local Data
 */

static PSL_CONST PSLWCHAR   _RgchResponderName[] = L"MTP PK Responder";

static MTPSTREAMROUTERVTBL  _VtblMTPIPRouter =
{
    _MTPIPRouterDestroy,
    _MTPIPRouterCreateContext,
    MTPStreamRouterRoutePacketReceivedBase,
    _MTPIPRouterResetDataAvailable
};


/*
 *  _MTPIPRouterDestroy
 *
 *  Destroys the specified MTP/IP Router object
 *
 *  Arguments:
 *      MTPIPROUTER     mtpip               Router object to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPIPRouterDestroy(MTPIPROUTER mtpip)
{
    PSLSTATUS           ps;
    MTPIPROUTEROBJ*     pmtpip;

    /*  Validate arguments
     */

    if (PSLNULL == mtpip)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack our object
     */

    pmtpip = (MTPIPROUTEROBJ*)mtpip;
    PSLASSERT(MTPIPROUTEROBJ_COOKIE == pmtpip->dwIPCookie);

    /*  Call the core implementation that can handle thread aware
     *  scenarios
     */

    ps = MTPIPRouterRelease(mtpip);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  _MTPIPRouterCreateContext
 *
 *  Creates a route after completing initialization of the transport
 *  information
 *
 *  Arguments:
 *      MTPIPROUTER     mtpip               MTP/IP router information
 *      MTPCONTEXT**    ppmtpctx            Resulting MTP context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPIPRouterCreateContext(MTPIPROUTER mtpip, MTPCONTEXT** ppmtpctx)
{
    PSLSTATUS           ps;
    MTPIPROUTEROBJ*     pmtpip;
    MTPIPPARSERINFO     mtpipPICmd;
    MTPIPPARSERINFO     mtpipPIEvent;

    /*  Clear result for safety
     */

    if (PSLNULL != ppmtpctx)
    {
        *ppmtpctx = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpip) || (PSLNULL == ppmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack our object
     */

    pmtpip = (MTPIPROUTEROBJ*)mtpip;
    PSLASSERT(MTPIPROUTEROBJ_COOKIE == pmtpip->dwIPCookie);

    /*  Extract the transport information
     */

    if ((PSLNULL == pmtpip->mtpstrmRO.mtpstrmCommand) ||
        (PSLNULL == pmtpip->mtpstrmRO.mtpstrmEvent))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Retrieve information about the command and event connections
     */

    ps = MTPStreamParserGetParserInfo(pmtpip->mtpstrmRO.mtpstrmCommand,
                            &mtpipPICmd, sizeof(mtpipPICmd));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = MTPStreamParserGetParserInfo(pmtpip->mtpstrmRO.mtpstrmEvent,
                            &mtpipPIEvent, sizeof(mtpipPICmd));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And create a buffer set for this connection
     */

    SAFE_MTPSTREAMBUFFERMGRDESTROY(pmtpip->mtpstrmRO.mtpstrmbm);
    ps = MTPStreamBufferMgrCreate(PSLGetAlignment(),
                    mtpipPICmd.cbMaxTransmit, mtpipPICmd.cbMaxReceive,
                    mtpipPIEvent.cbMaxTransmit, mtpipPIEvent.cbMaxReceive,
                    sizeof(MTPSTREAMDATA), &(pmtpip->mtpstrmRO.mtpstrmbm));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Allow the base class to complete the initialization
     */

    ps = MTPStreamRouterCreateContextBase(mtpip, ppmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  _MTPIPRouterResetDataAvailable
 *
 *  Arguments:
 *      MTPIPROUTER     mtpip               Router object associated with parser
 *      MTPSTREAMPARSER mtpstrm             Parser to reset
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPIPRouterResetDataAvailable(MTPIPROUTER mtpip,
                    MTPSTREAMPARSER mtpstrm)
{
    PSLSTATUS           ps;

    /*  Validate arguments
     */

    if (PSLNULL == mtpip)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Validate our object
     */

    PSLASSERT(MTPIPROUTEROBJ_COOKIE == ((MTPIPROUTEROBJ*)mtpip)->dwIPCookie);

    /*  Call the MTP/IP Parser to do the real work
     */

    ps = MTPIPParserSocketMsgReset(mtpstrm, PSLFALSE,
                            PSLSOCKETMSG_DATA_AVAILABLE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPIPRouterCreate
 *
 *  Creates an instance of the MTP/IP router
 *
 *  Arguments:
 *      MTPIPROUTER*    pmtpipParser        Return location for handle
 *
 *  Returns:
 *      PSLSTATUS   PSL_SUCCESS on success
 *
 */

PSLSTATUS MTPIPRouterCreate(MTPIPROUTER* pmtpipRouter)
{
    PSLHANDLE       hMutex = PSLNULL;
    PSLSTATUS       ps;
    MTPIPROUTEROBJ* pmtpipRO = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpipRouter)
    {
        *pmtpipRouter = PSLNULL;
    }

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpipRouter)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create a mutex to handle synchronization issues- note that we
     *  want to keep track of the mutex separately so that it can be
     *  released on exit if necessary.
     */

    ps = PSLMutexOpen(0, NULL, &hMutex);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Allocate a new router using the MTP Stream Router to create
     *  and initialize the base object
     */

    ps = MTPStreamRouterCreate(sizeof(MTPIPROUTEROBJ),
                            (MTPSTREAMPARSER*)&pmtpipRO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Set the cookie so that we can validate this is one of our objects
     */

    pmtpipRO->dwIPCookie = MTPIPROUTEROBJ_COOKIE;
#endif  /*  PSL_ASSERTS */

    /*  Set the proper VTBL and mark the sockets as unbound
     */

    pmtpipRO->mtpstrmRO.vtbl = &_VtblMTPIPRouter;
    pmtpipRO->cRef = 1;
    pmtpipRO->hMutex = hMutex;
    hMutex = NULL;

    /*  Return the parser handle
     */

    *pmtpipRouter = pmtpipRO;
    pmtpipRO = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPIPROUTERRELEASE(pmtpipRO);
    SAFE_PSLMUTEXCLOSE(hMutex);
    return ps;
}


/*
 *  MTPIPRouterAddRef
 *
 *  Increments the reference count on the router in a thread safe fashion
 *
 *  Arguments:
 *      MTPIPROUTER     mtpip               Router to AddRef
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPRouterAddRef(MTPIPROUTER mtpip)
{
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLSTATUS       ps;
    MTPIPROUTEROBJ* pmtpip;

    /*  Validate arguments
     */

    if (PSLNULL == mtpip)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we have one of our objects
     */

    pmtpip = (MTPIPROUTEROBJ*)mtpip;
    PSLASSERT(MTPIPROUTEROBJ_COOKIE == pmtpip->dwIPCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmtpip->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmtpip->hMutex;

    /*  And increment the reference count
     */

    pmtpip->cRef++;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPIPRouterRelease
 *
 *  Releases the contents of the MTP/IP router information structure.
 *
 *  Arguments:
 *      MTPIPROUTER     mtpip               Router to release
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPRouterRelease(MTPIPROUTER mtpip)
{
    PSLUINT32       dwResult;
    PSLHANDLE       hMutexClose = PSLNULL;
    PSLHANDLE       hMutexRelease = PSLNULL;
    PSLHANDLE       mqShutdown = PSLNULL;
    PSLSTATUS       ps;
    MTPIPROUTEROBJ* pmtpip;
    PSLMSG*         pMsg = PSLNULL;

    /*  Make sure we have work to do
     */

    if (PSLNULL == mtpip)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Make sure we have one of our objects
     */

    pmtpip = (MTPIPROUTEROBJ*)mtpip;
    PSLASSERT(MTPIPROUTEROBJ_COOKIE == pmtpip->dwIPCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmtpip->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmtpip->hMutex;

    /*  Decrement the reference count on the structure
     */

    PSLASSERT(0 < pmtpip->cRef);
    pmtpip->cRef--;
    if (0 < pmtpip->cRef)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  This entry should no longer be on the bind transport list
     */

    PSLASSERT(PSLNULL == pmtpip->pmtpipNext);

    /*  Make sure the thread terminates completely
     *
     *  $TODO- need to make sure that this code can actually execute.  The
     *  way we are currently configured the reference count held by the
     *  transport thread may prevent it from ever happening.
     */

    if ((PSLThreadGetCurrentThreadID() != pmtpip->dwThreadID) &&
        (PSLNULL != pmtpip->hThread))
    {
        /*  See if the thread is still running
         */

        ps = PSLThreadGetResult(pmtpip->hThread, 0, &dwResult);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  If the thread is still running attempt a graceful shutdown
         */

        if (PSLSUCCESS_WOULD_BLOCK == ps)
        {
            /*  We should have both a message queue and pool at this point
             */

            PSLASSERT(PSLNULL != pmtpip->mtpstrmRO.mqTransport);
            PSLASSERT(PSLNULL != pmtpip->mtpstrmRO.mpTransport);

            /*  Tell the message thread to do a graceful shutdown
             */

            ps = PSLMsgAlloc(pmtpip->mtpstrmRO.mpTransport, &pMsg);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            pMsg->msgID = MTPIPTRANSPORTMSG_SHUTDOWN;

            ps = PSLMsgPost(pmtpip->mtpstrmRO.mqTransport, pMsg);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            pMsg = PSLNULL;

            /*  Release the mutex
             */

            ps = PSLMutexRelease(pmtpip->hMutex);
            hMutexRelease = PSLNULL;
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Wait for the thread to terminate gracefully
             */

            ps = PSLThreadGetResult(pmtpip->hThread, 60000, &dwResult);
            if (PSLSUCCESS != ps)
            {
                /*  Forcefully terminate the thread so that it does not cause
                 *  problems
                 */

                ps = PSLThreadTerminate(pmtpip->hThread,
                            (PSLUINT32)PSLERROR_UNEXPECTED);
            }

            /*  Re-acquire the mutex
             */

            ps = PSLMutexAcquire(pmtpip->hThread, PSLTHREAD_INFINITE);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            hMutexRelease = pmtpip->hThread;
        }
    }

    /*  Close the thread handle
     */

    SAFE_PSLTHREADCLOSE(pmtpip->hThread);

    /*  Release the transport message pool and stash the message queue- we
     *  want to make sure that we complete cleanup before we release the
     *  queue so that it can be correctly unregistered from the shutdown
     *  service.
     */

    SAFE_PSLMSGPOOLDESTROY(pmtpip->mtpstrmRO.mpTransport);
    mqShutdown = pmtpip->mtpstrmRO.mqTransport;
    pmtpip->mtpstrmRO.mqTransport = PSLNULL;

    /*  Retrieve the mutex so that it can be closed
     */

    hMutexClose = pmtpip->hMutex;

    /*  If we have an assigned transport ID send the notification that the
     *  transport has closed
     */

    CHECK_MTPTRANSPORTNOTIFY(MTPTRANSPORTEVENT_CLOSED, pmtpip->aParamTransport);

    /*  Call the base class so that it can do its work
     */

    ps = MTPStreamRouterDestroyBase(mtpip);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Clean out any messages remaining in the shutdown queue- we may have
     *  had a close message posted by a transport.  This is normally handled
     *  by MTPStreamRouterDestroyBase but because we pull it out it has to
     *  be taken care of here.
     */

    if (PSLNULL != mqShutdown)
    {
        PSLMsgEnum(mqShutdown, MTPStreamRouterCleanQueueEnum, 0);
    }

Exit:
    SAFE_PSLMSGFREE(pMsg);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMUTEXCLOSE(hMutexClose);
    if (PSLNULL != mqShutdown)
    {
        /*  Unregister the queue from the shutdown service and release it
         */

        MTPShutdownUnregister(mqShutdown);
        PSLMsgQueueDestroy(mqShutdown);
    }
    return ps;
}


/*
 *  MTPIPRouterThread
 *
 *  The main thread for handling MTP/IP transport operations for objects that
 *  are bound to a router.
 *
 *  Arguments:
 *      PSLVOID*        pvParam             Parameter information- contains
 *                                            the transport info to which this
 *                                            thread is bound
 *
 *  Returns:
 *      PSLUINT32       Thread result (PSLSTATUS assumed)
 *
 */

PSLUINT32 PSL_API MTPIPRouterThread(PSLVOID* pvParam)
{
    PSLUINT32       cbMAC;
    PSLBOOL         fDone;
    PSLBOOL         fShutdownRegistered = PSLTRUE;
    PSLGUID         guidResponder;
    PSLHANDLE       hMutexRelease = PSLNULL;
    MTPIPPARSER     mtpipParser;
    MTPIPPARSERINFO mtpippi;
    PSLSTATUS       ps;
    PSLBYTE*        pbMAC = PSLNULL;
    MTPCONTEXT*     pmtpctx = PSLNULL;
    MTPIPROUTEROBJ* pmtpip = PSLNULL;
    PSLMSG*         pMsg = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == pvParam)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the transport information- the reference count used on the
     *  object is transferred from the creating function so we do not need
     *  to increment the reference count here...
     */

    pmtpip = (MTPIPROUTEROBJ*)pvParam;
    PSLASSERT(MTPIPROUTEROBJ_COOKIE == pmtpip->dwIPCookie);

    /*  Initialize the thread ID
     */

    pmtpip->dwThreadID = PSLThreadGetCurrentThreadID();

    /*  Make sure this thread gets a higher priority so that events are
     *  handled quickly- we avoid setting the highest priority level
     *  here since there are some operations on this thread which might take
     *  more time- we want to let event handlers push events over handling
     *  them.
     */

    ps = PSLThreadSetPriority(NULL, PSLTHREADPRIORITY_HIGHEST);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Register our queue with the shutdown service so that we shutdown
     *  correctly
     */

    ps = MTPShutdownRegister(pmtpip->mtpstrmRO.mqTransport);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    fShutdownRegistered = PSLTRUE;

    /*  Acquire the mutex- the mutex is held by the creating thread why it
     *  finishes setting things up.  By design we want this thread to have
     *  primary ownership of the mutex so that we guarantee quick, uninterrupted
     *  message handling.
     */

    ps = PSLMutexAcquire(pmtpip->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmtpip->hMutex;

    /*  Start waiting for messages to handle
     */

    fDone = PSLFALSE;
    while (!fDone)
    {
        /*  Release any existing message
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  Release the mutex so that we let other threads have a chance to
         *  manipulate the transport
         */

        PSLASSERT(PSLNULL != hMutexRelease);
        SAFE_PSLMUTEXRELEASE(hMutexRelease);

        /*  Retrieve the next message
         */

        ps = PSLMsgGet(pmtpip->mtpstrmRO.mqTransport, &pMsg);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Re-acquire the mutex so that messages are handled without
         *  interruption
         */

        ps = PSLMutexAcquire(pmtpip->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexRelease = pmtpip->hMutex;

        /*  And handle it
         */

        switch (pMsg->msgID)
        {
        case PSLSOCKETMSG_CLOSE:
            /*  Remove this entry from the list of available entries if we
             *  are not yet fully bound
             */

            ps = MTPIPUnbindRouterTransport(pmtpip);
            PSLASSERT(PSL_SUCCEEDED(ps));

            /* FALL THROUGH INTENTIONAL */

        case MTPMSG_SHUTDOWN:
        case MTPIPTRANSPORTMSG_SHUTDOWN:
            /*  Shutdown the route if it is initialized
             */

            if (PSLNULL != pmtpctx)
            {
                ps = MTPStreamRouteTerminate(pmtpctx,
                            (MTPMSG_SHUTDOWN == pMsg->msgID) ?
                                MTPMSG_SHUTDOWN : MTPMSG_DISCONNECT, 0);
                PSLASSERT(PSL_SUCCEEDED(ps));
                if (PSL_FAILED(ps))
                {
                    goto Exit;
                }
            }

            /*  We are ready to close down
             */

            PSLTraceProgress("MTPIP Transport 0x%08x closing", pmtpip);
            fDone = PSLTRUE;
            break;

        case MTPIPROUTERMSG_ACK_COMMAND:
            /*  This should only ever happen once
             */

            PSLASSERT(PSLNULL == pbMAC);

            /*  We should have a registered command parser- retrieve the
             *  socket from it
             */

            PSLASSERT(PSLNULL != pmtpip->mtpstrmRO.mtpstrmCommand);
            ps = MTPStreamParserGetParserInfo(pmtpip->mtpstrmRO.mtpstrmCommand,
                            &mtpippi, sizeof(mtpippi));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  And determine the MAC and GUID for this connection
             */

            ps = PSLResolveMACAddress((SOCKADDR*)&(mtpippi.saAddr),
                            mtpippi.cbAddr, &pbMAC, &cbMAC);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            ps = MTPIPGenerateGUID(pbMAC, cbMAC, &guidResponder);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Reply to the initiator to indicate that we have received
             *  the init command string
             */

            ps = MTPIPParserSendInitCmdACK(pmtpip->mtpstrmRO.mtpstrmCommand,
                            pmtpip->dwConnID, &guidResponder,
                            _RgchResponderName);
            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Provide some feedback
             */

            PSLTraceProgress("MTPIP Transport 0x%08x- Init Command Ack");
            break;

        case MTPIPROUTERMSG_ACK_EVENT:
            /*  We should have a registered event parser
             */

            PSLASSERT(PSLNULL != pmtpip->mtpstrmRO.mtpstrmEvent);

            /*  Initialize the transport
             */

            ps = MTPStreamRouterCreateContext(pmtpip, &pmtpctx);

            /*  Reply back to the initiator whether we are ready or not
             */

            if (PSL_SUCCEEDED(ps))
            {
                /*  Acknowledge establishment of the event connection
                 */

                ps = MTPIPParserSendInitEventACK(pmtpip->mtpstrmRO.mtpstrmEvent);
            }
            else
            {
                /*  Report failure of the event connection
                 */

                ps = MTPIPParserSendInitFail(pmtpip->mtpstrmRO.mtpstrmEvent,
                            MTPIP_FAIL_UNSPECIFIED);
            }

            /*  Halt if we encountered an error
             */

            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Provide some feedback
             */

            PSLTraceProgress("MTPIP Transport 0x%08x- Init Event ACK");
            break;

        case PSLSOCKETMSG_DATA_AVAILABLE:
        case PSLSOCKETMSG_RESET:
            /*  Determine which parser the event is for
             */

            switch (pMsg->aParam1)
            {
            case MTPSTREAMPARSERTYPE_COMMAND:
                mtpipParser = pmtpip->mtpstrmRO.mtpstrmCommand;
                break;

            case MTPSTREAMPARSERTYPE_EVENT:
                mtpipParser = pmtpip->mtpstrmRO.mtpstrmEvent;
                break;

            default:
                /*  Should never get here- we should have either an event
                 *  or a command socket
                 */

                PSLASSERT(PSLFALSE);
                continue;
            }

            /*  Reset the socket event- we want to make sure that we
             *  get notified if data arrives while we are processing
             *  all of the available data to make sure that we do
             *  not miss a notification
             */

            ps = MTPIPParserSocketMsgReset(mtpipParser, PSLFALSE, pMsg->msgID);
            PSLASSERT(PSL_SUCCEEDED(ps));

            /*  Switch the message to the generic stream router version
             */

            switch (pMsg->msgID)
            {
            case PSLSOCKETMSG_DATA_AVAILABLE:
                pMsg->msgID = MTPSTREAMROUTERMSG_DATA_AVAILABLE;
                break;

            case PSLSOCKETMSG_RESET:
                pMsg->msgID = MTPSTREAMROUTERMSG_RESET;
                break;

            default:
                PSLASSERT(PSLFALSE);
                ps = PSLERROR_UNEXPECTED;
                goto Exit;
            }

            /*  FALL THROUGH INTENTIONAL */

        default:
            /*  Call the generic helper function to handle the rest of the
             *  messages
             */

            ps = MTPStreamRouteMsg(pmtpctx, pMsg);
            if (PSL_FAILED(ps))
            {
                /*  Abort the route if it is initialized
                 */

                MTPStreamRouteTerminate(pmtpctx, MTPMSG_ABORT, 0);

                /*  Report an unsafe termination
                 */

                PSLTraceError("MTPIP Transport 0x%08x aborting on error 0x%08x",
                            pmtpip, ps);
                goto Exit;
            }
            break;
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    SAFE_MTPROUTEDESTROY(pmtpctx);
    SAFE_FREERESOVLEDMACADDRESS(pbMAC);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    MTPIPRouterRelease(pmtpip);
    return ps;
}

