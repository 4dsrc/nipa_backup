/*
 *  MTPIPRouter.h
 *
 *  Contains MTP/IP router declarations
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPIPROUTER_H_
#define _MTPIPROUTER_H_

#define MTPIPROUTER_TRANSPORT_SIZE          sizeof(MTPSTREAMROUTERCONTEXT)

#define MTPIPTRANSPORT_MSG_POOL_SIZE        16

#define MTPIPTRANSPORTFLAGS_ROUTER          0x00000001
#define MTPIPTRANSPORTFLAGS_PROXY           0x00000002
#define MTPIPTRANSPORTFLAGS_TYPE_MASK       0x0000000f

typedef MTPSTREAMROUTER MTPIPROUTER;

/*  MTP/IP Router Messages
 *
 *  Messages defined for use by the MTP/IP internal message queues.
 */

#define MAKE_MTPIPROUTER_MSGID(_id, _flags) \
    MAKE_PSL_MSGID(MTP_TRANSPORT_PRIVATE, (_id), (_flags))

enum
{
    MTPIPROUTERMSG_ACK_COMMAND =            MAKE_MTPIPROUTER_MSGID(1, 0),
    MTPIPROUTERMSG_ACK_EVENT =              MAKE_MTPIPROUTER_MSGID(2, 0),
};


/*  MTPIPROUTEROBJ
 */

typedef struct _MTPIPROUTEROBJ
{
    MTPSTREAMROUTEROBJ          mtpstrmRO;
#ifdef PSL_ASSERTS
    PSLUINT32                   dwIPCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT32                   cRef;
    PSLPARAM                    aParamTransport;
    PSLUINT32                   dwFlags;
    PSLUINT32                   dwThreadID;
    PSLHANDLE                   hThread;
    PSLHANDLE                   hMutex;
    PSLUINT32                   dwConnID;
    PSLGUID                     guidClient;
    PSLWCHAR                    rgwchClientName[MTPIP_MAX_NAME_LENGTH];
    PSLUINT32                   cchClientName;
    PSLUINT32                   dwClientProtVer;
    struct _MTPIPROUTEROBJ*     pmtpipNext;
} MTPIPROUTEROBJ;

/*  Object Lifetime
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPRouterCreate(
                            MTPIPROUTER* pmtpip);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPRouterAddRef(
                            MTPIPROUTER mtpip);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPRouterRelease(
                            MTPIPROUTER mtpip);

/*  MTPIPRouterThread
 *
 *  Core thread proc for the transport- a valid MTP/IP Router must be passed
 *  as the object.
 */

PSL_EXTERN_C PSLUINT32 PSL_API MTPIPRouterThread(
                            PSLVOID* pvParam);


/*  Safe Helpers
 */

#define SAFE_MTPIPROUTERRELEASE(_p) \
if (PSLNULL != _p) \
{ \
    MTPIPRouterRelease(_p); \
    _p = PSLNULL; \
}

#endif  /* _MTPIPROUTER_H_ */

