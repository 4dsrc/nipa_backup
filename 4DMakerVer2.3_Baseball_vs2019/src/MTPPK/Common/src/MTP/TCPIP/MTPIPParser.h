/*
 *  MTPIPParser.h
 *
 *  Contains declaration of the different MTP/IP parsers and constants.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPIPPARSER_H_
#define _MTPIPPARSER_H_


/*  MTP/IP Packet Types
 *
 *  Type enumeration for MTP/IP packet types.  Note that these types are
 *  in addition to the existing MTP Stream Parser types.
 */

enum
{
    MTPIPPACKET_INIT_COMMAND_REQUEST = MTPSTREAMPACKET_SUBCLASS_BEGIN,
    MTPIPPACKET_INIT_COMMAND_ACK,
    MTPIPPACKET_INIT_EVENT_REQUEST,
    MTPIPPACKET_INIT_EVENT_ACK,
    MTPIPPACKET_INIT_FAIL
};


/*  MTP/IP Initialization Failure Codes
 */

enum
{
    MTPIP_FAIL_REJECTED_INITIATOR =                 0x00000001,
    MTPIP_FAIL_BUSY =                               0x00000002,
    MTPIP_FAIL_UNSPECIFIED =                        0x00000003
};


/*  MTPIPPARSER
 *
 *  The MTP/IP Parser inherits a large portion of it's implementation from
 *  the MTP Stream Parser.  The functionality described here is specific
 *  to MTP/IP solutions.
 */

typedef PSLHANDLE MTPIPPARSER;

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserCreate(
                            PSLUINT32 dwMode,
                            MTPIPPARSER* pmtpipParser);

/*  MTP/IP Parser Information
 *
 *  Use ths object when calling MTPStreamParserGetParserInfo with an
 *  MTPIPPARSER object.
 */

typedef struct _MTPIPPARSERINFO
{
    MTPSTREAMPARSERINFO     mtpstrmInfo;
    PSLUINT32               cbMaxTransmit;
    PSLUINT32               cbMaxReceive;
    PSLUINT32               cbAddr;
    SOCKADDR_STORAGE        saAddr;
} MTPIPPARSERINFO;


/*  MTP/IP Parser Bindings
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserBindSocket(
                    MTPIPPARSER mtpipParser,
                    SOCKET sock);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserUnbindSocket(
                    MTPIPPARSER mtpipParser,
                    SOCKET* psock);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserIsBoundSocket(
                    MTPIPPARSER mtpipParser,
                    SOCKET socket);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserSocketMsgReset(
                    MTPIPPARSER mtpstrmParser,
                    PSLBOOL fResetMask,
                    PSLUINT32 dwData);


/*  Serialization and De-Serialization Routines
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserSendInitCmdRequest(
                    MTPIPPARSER mtpipParser,
                    PSL_CONST PSLGUID* pguidInitiator,
                    PSLCWSTR szInitiator,
                    PSLUINT32 dwProtocolVer);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserReadInitCmdRequest(
                    MTPIPPARSER mtpipParser,
                    PSLGUID* pguidInitiator,
                    PSLWSTR rgchInitiator,
                    PSLUINT32* pchInitiator,
                    PSLUINT32 cchMaxInitiator);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserSendInitCmdACK(
                    MTPIPPARSER mtpipParser,
                    PSLUINT32 dwConnID,
                    PSL_CONST PSLGUID* pguidResponder,
                    PSLCWSTR szResponder);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserReadInitCmdACK(
                    MTPIPPARSER mtpipParser,
                    PSLUINT32* pdwConnID,
                    PSLGUID* pguidResponder,
                    PSLWSTR szResponder,
                    PSLUINT32* pchResponder,
                    PSLUINT32 cchMaxResponder);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserSendInitEventRequest(
                    MTPIPPARSER mtpipParser,
                    PSLUINT32 dwConnID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserReadInitEventRequest(
                    MTPIPPARSER mtpipParser,
                    PSLUINT32* pdwConnID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserSendInitEventACK(
                    MTPIPPARSER mtpipParser);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserReadInitEventACK(
                    MTPIPPARSER mtpipParser);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserSendInitFail(
                    MTPIPPARSER mtpipParser,
                    PSLUINT32 dwReason);

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPParserReadInitFail(
                    MTPIPPARSER mtpipParser,
                    PSLUINT32* pdwReason);

/*  MTPIPSendInitFail
*
*  Non-parser bound API for sending an initialization failed packet
*/

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPSendInitFailed(
                    SOCKET sock,
                    PSLUINT32 dwReason);

/*  Safe Helpers
 */

#define SAFE_MTPIPPARSERDESTROY(_p)     SAFE_MTPSTREAMPARSERDESTROY(_p)

#endif  /* _MTPIPPARSER_H_ */

