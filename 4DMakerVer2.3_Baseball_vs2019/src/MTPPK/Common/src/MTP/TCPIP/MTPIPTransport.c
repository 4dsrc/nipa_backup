/*
 *  MTPIPTransport.c
 *
 *  Definition of the MTPIPTransport implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPIPTransportPrecomp.h"
#include "MTPInitializeUtil.h"

/*
 *  NOTE:
 *
 *  Since getting data off the network and into processing can be one of the
 *  core performance bottlenecks, this file is intentionally written to avoid
 *  using synchronization objects despite the threaded architecture.  To safely
 *  deal with multiple 'simultaneous' threads of execution, the following
 *  assumptions/conventions are employed:
 *
 *  1. The MTP/IP binding process is assumed to be a request/acknowledge model
 *      with a predictable syncrhonous behavior.  The simplest example of this
 *      is the fact that connections to the event socket will not occur until
 *      the initial command/data connection has been ACK'ed.  This external
 *      synchronization is leveraged to make sure that the binding thread and
 *      the transport threads synchronize correctly.
 *  2. While multiple binds may be in process at the same time, the are all
 *      handled by a single binding thread.  Access to the binding static state
 *      information is only valid from the binding thread.
 *  3. During the initial binding, the binding thread will create the context
 *      which holds the MTPIPROUTEROBJ.  Once the command/data connection
 *      is accepted, ownership for the transport state information transfers
 *      to the transport thread EXCEPT for one short period while binding
 *      completes.  During the binding of the event socket to the transport,
 *      the binding thread will again modify the MTPIPROUTEROBJ structure.
 *  4. Socket lifetimes are managed by the socket.  The PSLSOCKETMSG_CLOSE
 *      notification is used to manage when the socket data should be cleaned
 *      up rather than relying on state stored is statics.
 *  5. Transport lifetime is owned by the transport thread, and each transport
 *      thread has direct access to it's data without requiring a global
 *      lookup table of any form.  It is assumed that when a socket closes
 *      down, or the transport is told to shutdown, that it will detect
 *      the shutdown case and perform all cleanup necessary for that
 *      connection.
 *
 */

/*  Local Defines
 */

#define SOCKETINFO_COOKIE                   'mipS'

#define BINDTRANSPORT_MSG_POOL_SIZE         16

#define SAFE_SOCKETINFODESTROY(_p) \
if (PSLNULL != (_p)) \
{ \
    _SocketInfoDestroy(_p); \
    (_p) = PSLNULL; \
}

/*  Local Types
 */

/*
 *  SOCKETINFO
 *
 *  Contains information about a particular socket
 *
 */

typedef struct _SOCKETINFO
{
#ifdef PSL_ASSERTS
    PSLUINT32                   dwCookie;
#endif  /* PSL_ASSERTS */
    SOCKET                      sock;
    SOCKADDR_STORAGE            saClient;
    PSLPARAM                    aParamTransport;
    struct _SOCKETINFO*         psiNext;
} SOCKETINFO;


/*
 *  BINDTRANSPORTSTATE
 *
 *  State information for the bind router thread
 *
 */

enum
{
    BINDTRANSPORTSTATE_UNKNOWN = 0,
    BINDTRANSPORTSTATE_INITIALIZING,
    BINDTRANSPORTSTATE_RUNNING,
    BINDTRANSPORTSTATE_SHUTDOWN,
    BINDTRANSPORTSTATE_SHUTTING_DOWN
};


/*
 *  Bind Transport message
 *
 *  Messages defined for use by the MTP/IP internal message queues.
 *
 */

enum
{
    BINDTRANSPORTMSG_BIND_SOCKET =          MAKE_PSL_NORMAL_MSGID(PSL_USER, 1),
    BINDTRANSPORTMSG_TRANSPORT_CLOSE =      MAKE_PSL_NORMAL_MSGID(PSL_USER, 2),
    BINDTRANSPORTMSG_SHUTDOWN =             MAKE_PSL_NORMAL_MSGID(PSL_USER, 3),
};


/*  Local Functions
 */

static PSLUINT32 _GetNextConnID();

static PSLSTATUS _SocketInfoDestroy(SOCKETINFO* psi);

static PSLSTATUS _BindSocketToRouterTransport(SOCKETINFO* psi);

static PSLSTATUS _UnbindSocketFromRouterTransport(SOCKETINFO* psi);

static PSLUINT32 PSL_API _BindRouterThread(PSLVOID* pvParam);

static PSLSTATUS _CheckBindRouterThreadState(PSLBOOL fMustBeRunning);

static PSLSTATUS _MTPIPRouterInitialize(PSLUINT32 dwInitFlags);

/*  Local Variables
 */

static PSLUINT32            _DwNextConnID = PSLNULL;

static SOCKETINFO*          _PsiBindList = PSLNULL;
static MTPIPROUTEROBJ*      _PmtpipBindList = PSLNULL;

static PSLBOOL              _FPSLSocketRunning = PSLFALSE;

static const PSLWCHAR       _RgchBindRouterInitSignal[] =
    L"Bind Transport Init Signal";

static PSLUINT32            _DwBindTransportState = BINDTRANSPORTSTATE_UNKNOWN;
static PSLHANDLE            _HThreadBindTransport = PSLNULL;
static PSLMSGQUEUE          _MqBindTransport = PSLNULL;
static PSLMSGPOOL           _MpBindTransport = PSLNULL;

static const PSLWCHAR       _RgchUUID[] = L"uuid:";
static const PSLWCHAR       _RgchUUIDFixed[] = L"00000000-0000-0000-";
static const PSLWCHAR       _RgchEUI48[] = L"ffff-";

/*
 *  _GetNextConnID
 *
 *  Returns the next connection ID to use
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLUINT32       Next connection ID to use
 *
 */

PSLUINT32 _GetNextConnID()
{
    PSLUINT32       dwResult;

    /*  Return and increment the ID in a thread safe manner
     */

    dwResult = PSLAtomicIncrement(&_DwNextConnID);

    /*  Handle rollover
     */

    while (0 == dwResult)
    {
        dwResult = PSLAtomicIncrement(&_DwNextConnID);
    }
    return dwResult;
}


/*
 *  _SocketInfoDestroy
 *
 *  Destroys an instance of a socket info
 *
 *  Arguments:
 *      SOCKETINFO*     psi                 Socket info to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _SocketInfoDestroy(SOCKETINFO* psi)
{
    PSLSTATUS       ps;

    /*  Make sure we have work to do
     */

    if (PSLNULL == psi)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }
    PSLASSERT(SOCKETINFO_COOKIE == psi->dwCookie);

    /*  If we still have a socket make sure that we report failure (ignoring
     *  the error code since it may already be destroyed) and then close it
     */

    if (INVALID_SOCKET != psi->sock)
    {
        MTPIPSendInitFailed(psi->sock, MTPIP_FAIL_UNSPECIFIED);
        PSLSocketClose(psi->sock);
    }

    /*  Report the transport as closed
     */

    CHECK_MTPTRANSPORTNOTIFY(MTPTRANSPORTEVENT_CLOSED, psi->aParamTransport);

    /*  Release the entry
     */

    PSLMemFree(psi);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _BindSocketToRouterTransport
 *
 *  Binds the given socket to a router transport.  The socket is assumed
 *  to have data available to enable determination of whether or not the
 *  socket is a command/data connection or an event connection.  Once the
 *  transport is bound to both sockets, it will be bound to a router for
 *  standard MTP/IP operation.
 *
 *  Arguments:
 *      SOCKETINFO*     psi                 Socket information for socket to
 *                                            bind
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS _BindSocketToRouterTransport(SOCKETINFO* psi)
{
    PSLUINT32           dwConnID;
    PSLUINT32           dwPacketType;
    PSLHANDLE           hMutexRelease = PSLNULL;
    PSLMSGQUEUE         mqShutdown = PSLNULL;
    PSLMSGPOOL          mpShutdown = PSLNULL;
    PSLMSGQUEUE         mqTransport = PSLNULL;
    PSLMSGPOOL          mpTransport = PSLNULL;
    MTPIPPARSER         mtpipParser = PSLNULL;
    PSLSTATUS           ps;
    MTPIPROUTEROBJ*     pmtpipTransport = PSLNULL;
    MTPIPROUTEROBJ*     pmtpipCur;
    MTPIPROUTEROBJ*     pmtpipLast;
    PSLMSG*             pMsg = PSLNULL;
    PSLVOID*            pvParam = PSLNULL;

    /*  Validate Arguments
     */

    if ((PSLNULL == psi) || (INVALID_SOCKET == psi->sock))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Retrieve a parser to bind this socket to
     */

    ps = MTPIPParserCreate(MTPSTREAMPARSERMODE_RESPONDER, &mtpipParser);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And bind the socket
     */

    ps = MTPIPParserBindSocket(mtpipParser, psi->sock);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Determine what type of socket we have received
     */

    ps = MTPStreamParserGetPacketType(mtpipParser, &dwPacketType, PSLNULL);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  If we are told that the request would block then we need to wait
     *  for more data- the socket was reset before getting here so we should
     *  just exit at this point
     */

    if (PSLSUCCESS_WOULD_BLOCK == ps)
    {
        goto Exit;
    }

    /*  Determine what type of packet we are dealing with
     */

    switch (dwPacketType)
    {
    case MTPIPPACKET_INIT_COMMAND_REQUEST:
        /*  Set the parser type as command
         */

        ps = MTPStreamParserSetType(mtpipParser, MTPSTREAMPARSERTYPE_COMMAND);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Conversation with an MTP Initiator is beginning on this socket for
         *  command/data.  Create an MTP/IP Router to manage the connection.
         */

        ps = MTPIPRouterCreate(&pmtpipTransport);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Acquire the mutex so that we have ownership while we complete the
         *  initialization
         */

        ps = PSLMutexAcquire(pmtpipTransport->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexRelease = pmtpipTransport->hMutex;

        /*  Create a new message queue for the transport
         */

        ps = MTPMsgGetQueueParam(&pvParam);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        ps = PSLMsgQueueCreate(pvParam, MTPMsgOnPost, MTPMsgOnWait,
                            MTPMsgOnDestroy, &mqTransport);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pvParam = NULL;

        ps = PSLMsgPoolCreate(MTPIPTRANSPORT_MSG_POOL_SIZE, &mpTransport);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Parse the rest of the information out of the packet
         */

        ps = MTPIPParserReadInitCmdRequest(mtpipParser,
                            &(pmtpipTransport->guidClient),
                            pmtpipTransport->rgwchClientName,
                            &(pmtpipTransport->cchClientName),
                            PSLSTRARRAYSIZE(pmtpipTransport->rgwchClientName));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Switch the socket events from the bind transport queue to the
         *  the router transport queue
         */

        ps = PSLSocketMsgSelect(psi->sock, mqTransport, mpTransport,
                            MTPSTREAMPARSERTYPE_COMMAND, (FD_READ | FD_CLOSE));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Post a message to the transport thread to have it ACK the
         *  init command request
         */

        ps = PSLMsgAlloc(mpTransport, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = MTPIPROUTERMSG_ACK_COMMAND;

        ps = PSLMsgPost(mqTransport, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = NULL;

        /*  Assign a connection ID to the transport
         */

        pmtpipTransport->dwConnID = _GetNextConnID();

        /*  Mark this transport as a router bound transport
         */

        pmtpipTransport->dwFlags = MTPIPTRANSPORTFLAGS_ROUTER;

        /*  Transfer the socket ownership to the transport via the parser
         */

        pmtpipTransport->mtpstrmRO.mtpstrmCommand = mtpipParser;
        pmtpipTransport->aParamTransport = psi->aParamTransport;
        psi->sock = INVALID_SOCKET;
        psi->aParamTransport = MTPTRANSPORTPARAM_UNKNOWN;
        mtpipParser = PSLNULL;

        /*  Transfer ownership of remaining objects to the thread
         */

        pmtpipTransport->mtpstrmRO.mqTransport = mqTransport;
        mqTransport = PSLNULL;
        pmtpipTransport->mtpstrmRO.mpTransport = mpTransport;
        mpTransport = PSLNULL;

        /*  Create a thread for the transport- note that the thread is
         *  currently blocked on the mutex.  Make sure that we appropriately
         *  handle the reference counting in the transport information-
         *  we assume that the thread will start up so it needs a reference
         *  and we remove that reference if a problem arises.
         */

        MTPIPRouterAddRef(pmtpipTransport);
        ps = PSLThreadCreate(MTPIPRouterThread, pmtpipTransport,
                            PSLFALSE, PSLTHREAD_DEFAULT_STACK_SIZE,
                            &pmtpipTransport->hThread);
        if (PSL_FAILED(ps))
        {
            MTPIPRouterRelease(pmtpipTransport);
            goto Exit;
        }

        /*  And add the partially complete transport to the list so that we
         *  can bind the event socket when it arrives.
         *
         *  NOTE: We do not use a lock here because we assume that adding
         *  and removing entries from the list only occurs within this
         *  function and all on the same thread context.
         */

        pmtpipTransport->pmtpipNext = _PmtpipBindList;
        _PmtpipBindList = pmtpipTransport;

        /*  We want to keep the extra reference count associated with the list-
         *  go ahead and transfer ownership to the list
         */

        pmtpipTransport = NULL;

        /*  Report success
         */

        ps = PSLSUCCESS;
        break;

    case MTPIPPACKET_INIT_EVENT_REQUEST:
        /*  Set the parser type as event
         */

        ps = MTPStreamParserSetType(mtpipParser, MTPSTREAMPARSERTYPE_EVENT);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Parse the packet to retrieve the connection ID
         */

        ps = MTPIPParserReadInitEventRequest(mtpipParser, &dwConnID);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Locate the matching connection ID
         */

        for (pmtpipLast = PSLNULL, pmtpipCur = _PmtpipBindList;
                (NULL != pmtpipCur) && (dwConnID != pmtpipCur->dwConnID);
                pmtpipLast = pmtpipCur, pmtpipCur = pmtpipCur->pmtpipNext)
        {
        }

        /*  If we did not locate a matching connection ID report that
         *  the connection is invalid
         */

        if (NULL == pmtpipCur)
        {
            ps = PSLERROR_CONNECTION_NOT_FOUND;
            goto Exit;
        }

        /*  Acquire the mutex so that we can operate on this transport
         *  safely
         */

        ps = PSLMutexAcquire(pmtpipCur->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexRelease = pmtpipCur->hMutex;

        /*  Stash the queue and pool in the shutdown state so that if we exit
         *  exit early here we can tell the transport to close
         */

        mqShutdown = pmtpipCur->mtpstrmRO.mqTransport;
        mpShutdown = pmtpipCur->mtpstrmRO.mpTransport;

        /*  Switch socket events to the transport message queue
         */

        ps = PSLSocketMsgSelect(psi->sock, pmtpipCur->mtpstrmRO.mqTransport,
                            pmtpipCur->mtpstrmRO.mpTransport,
                            MTPSTREAMPARSERTYPE_EVENT,
                            (FD_READ | FD_CLOSE));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Post a message to the transport thread to have it ACK the
         *  init command request
         */

        ps = PSLMsgAlloc(pmtpipCur->mtpstrmRO.mpTransport, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = MTPIPROUTERMSG_ACK_EVENT;

        ps = PSLMsgPost(pmtpipCur->mtpstrmRO.mqTransport, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = NULL;

        /*  Remove the connection information from the binding list
         */

        if (NULL != pmtpipLast)
        {
            pmtpipLast->pmtpipNext = pmtpipCur->pmtpipNext;
        }
        else
        {
            _PmtpipBindList = pmtpipCur->pmtpipNext;
        }
        pmtpipCur->pmtpipNext = PSLNULL;

        /*  Transfer the socket ownership to the transport via the parser
         */

        pmtpipCur->mtpstrmRO.mtpstrmEvent = mtpipParser;
        mtpipParser = PSLNULL;
        psi->sock = INVALID_SOCKET;

        /*  Release the mutex
         */

        SAFE_PSLMUTEXRELEASE(hMutexRelease);

        /*  Make sure that the reference count gets properly released on
         *  exit
         */

        pmtpipTransport = pmtpipCur;

        /*  Report success
         */

        ps = PSLSUCCESS;
        break;

    default:
        /*  During bind all other packets are out of sequence and should
         *  be rejected
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

Exit:
    /*  Terminate the initialization if necessary
     */

    if (PSL_FAILED(ps))
    {
        /*  Send a message to the shutdown thread if necessary
         */

        if (PSLNULL != mqShutdown)
        {
            /*  We should also have a message pool
             */

            PSLASSERT(PSLNULL != mpShutdown);

            /*  Send a shutdown message the transport
             */

            SAFE_PSLMSGFREE(pMsg);
            if (PSL_SUCCEEDED(PSLMsgAlloc(mpShutdown, &pMsg)))
            {
                pMsg->msgID = MTPIPTRANSPORTMSG_SHUTDOWN;
                if (PSL_SUCCEEDED(PSLMsgPost(mqShutdown, pMsg)))
                {
                    pMsg = PSLNULL;
                }
            }
        }
    }

    /*  And cleanup local objects
     */

    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMSGQUEUEDESTROY(mqTransport);
    SAFE_PSLMSGPOOLDESTROY(mpTransport);
    SAFE_PSLMSGFREE(pMsg);
    SAFE_MTPIPROUTERRELEASE(pmtpipTransport);
    SAFE_MTPMSGRELEASEQUEUEPARAM(pvParam);
    SAFE_MTPIPPARSERDESTROY(mtpipParser);
    return ps;
}


/*
 *  _UnbindSocketToRouterTransport
 *
 *  Unbinds the socket specified from the router transport thread
 *
 *  Arguments:
 *      SOCKETINFO*     psi                 Socket information for socket to
 *                                            remove from the list
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _UnbindSocketFromRouterTransport(SOCKETINFO* psi)
{
    PSLSTATUS       ps;
    MTPIPROUTEROBJ* pmtpipCur;
    MTPIPROUTEROBJ* pmtpipLast;

    /*  Validate arguments
     */

    if ((PSLNULL == psi) || (INVALID_SOCKET == psi->sock))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Start walking through the list of transports being bound looking for
     *  a matching socket
     */

    for (pmtpipLast = PSLNULL, pmtpipCur = _PmtpipBindList;
            PSLNULL != pmtpipCur;
            pmtpipLast = pmtpipCur, pmtpipCur = pmtpipCur->pmtpipNext)
    {
        /*  As long as this entry is on the bind transport thread it should
         *  not have bound a socket to the event entry
         */

        PSLASSERT((PSLNULL == pmtpipCur->mtpstrmRO.mtpstrmEvent) ||
                            (PSLSUCCESS_FALSE == MTPIPParserIsBoundSocket(
                                    pmtpipCur->mtpstrmRO.mtpstrmEvent,
                                    psi->sock)));

        /*  Check the command socket to see if this matches
         */

        ps = MTPIPParserIsBoundSocket(pmtpipCur->mtpstrmRO.mtpstrmCommand,
                            psi->sock);
        PSLASSERT(PSL_SUCCEEDED(ps));

        /*  If this did not match keep looking (and skipping the error)
         */

        if (PSLSUCCESS != ps)
        {
            continue;
        }

        /*  Acquire the mutex for safety
         */

        ps = PSLMutexAcquire(pmtpipCur->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Remove the entry from the list
         */

        if (PSLNULL != pmtpipLast)
        {
            pmtpipLast->pmtpipNext = pmtpipCur->pmtpipNext;
        }
        else
        {
            _PmtpipBindList = pmtpipCur->pmtpipNext;
        }
        pmtpipCur->pmtpipNext = PSLNULL;

        /*  Release the mutex
         */

        ps = PSLMutexRelease(pmtpipCur->hMutex);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  And release the structure
         */

        MTPIPRouterRelease(pmtpipCur);

        /*  Stop looking
         *
         *  NOTE: We do not actually clean up the bound entry at this point
         *  because we likely received this noticiation from the transport
         *  thread and not the router bind thread.  We will let the thread
         *  continue to own the entry and do the shutdown
         */

        break;
    }

    /*  Report success or not found
     */

    ps = (PSLNULL == pmtpipCur) ? PSLSUCCESS_NOT_FOUND : PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _BindRouterThread
 *
 *  Main thread used to bind a socket to a router to create a MTP/IP
 *  responder.  This thread is responsible for allocating the transport,
 *  creating a message queue for it, and correctly associating the command/data
 *  and event sockets with it.  Once the association is complete, the
 *  transport is bound to a router and the transport takes over responsibility
 *  for further bytes received.
 *
 *  Arguments:
 *      PSLVOID*        pvParam             Ignored
 *
 *  Returns:
 *      PSLUINT32       PSLSTATUS code
 *
 */

PSLUINT32 PSL_API _BindRouterThread(PSLVOID* pvParam)
{
    PSLUINT32       dwState;
    PSLBOOL         fLoop;
    PSLBOOL         fShuttingDown = PSLFALSE;
    PSLBOOL         fEnable = PSLTRUE;
    PSLINT32        errSock;
    PSLHANDLE       hSignalInit = PSLNULL;
    PSLSTATUS       ps;
    PSLMSG*         pMsg = PSLNULL;
    MTPIPROUTEROBJ* pmtpipCur;
    MTPIPROUTEROBJ* pmtpipLast;
    SOCKETINFO*     psiCur;
    SOCKETINFO*     psiLast;

    /*  Report the binding router as running
     */

    dwState = PSLAtomicCompareExchange(&_DwBindTransportState,
                            BINDTRANSPORTSTATE_RUNNING,
                            BINDTRANSPORTSTATE_INITIALIZING);
    PSLASSERT(BINDTRANSPORTSTATE_INITIALIZING == dwState);
    switch (dwState)
    {
    case BINDTRANSPORTSTATE_INITIALIZING:
        break;

    case BINDTRANSPORTSTATE_SHUTTING_DOWN:
        /*  We have alread been asked to shutdown- indicate that we
         *  are transitioning into shutdown mode so that we correctly
         *  ignore pending messages.
         *
         */

        fShuttingDown = PSLTRUE;
        break;

    case BINDTRANSPORTSTATE_RUNNING:
    case BINDTRANSPORTSTATE_UNKNOWN:
    default:
        /*  Should not get here in these states
         */

        PSLASSERT(FALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Signal any waiting threads that we have completed initialization
     */

    ps = PSLSignalOpen(PSLFALSE, PSLTRUE, _RgchBindRouterInitSignal,
                            &hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    ps = PSLSignalSet(hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure this thread gets a higher priority so that events are
     *  handled quickly- we avoid setting the highest priority level
     *  here since there are some operations on this thread which might take
     *  more time- we want to let event handlers push events over handling
     *  them.
     */

    ps = PSLThreadSetPriority(NULL, PSLTHREADPRIORITY_HIGHEST);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Start handling messages
     */

    fLoop = PSLTRUE;
    while (fLoop && !fShuttingDown)
    {
        /*  Release any message we are handling
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  Retrieve the next message
         */

        ps = PSLMsgGet(_MqBindTransport, &pMsg);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            /*  Skip the message- and try again.
             */

            continue;
        }

        /*  Handle the message
         */

        switch (pMsg->msgID)
        {
        case BINDTRANSPORTMSG_BIND_SOCKET:
            /*  New socket to track- extract it from the message
             */

            psiCur = (SOCKETINFO*)pMsg->aParam0;
            PSLASSERT(SOCKETINFO_COOKIE == psiCur->dwCookie);

            /*  Set the required socket options- disable the Nagle algoritm
             *  by setting the TCP_NODELAY option and enable keep alive packets
             */

            errSock = setsockopt(psiCur->sock, IPPROTO_TCP, TCP_NODELAY,
                            (char*)&fEnable, sizeof(fEnable));
            if (SOCKET_ERROR != errSock)
            {
                errSock = setsockopt(psiCur->sock, SOL_SOCKET, SO_KEEPALIVE,
                            (char*)&fEnable, sizeof(fEnable));
                if (SOCKET_ERROR != errSock)
                {
                    /*  Tell the socket to send notifications to the bind
                     *  transport thread
                     */

                    ps = PSLSocketMsgSelect(psiCur->sock, _MqBindTransport,
                                            _MpBindTransport, 0,
                                            (FD_READ | FD_CLOSE));
                    if (PSL_SUCCEEDED(ps))
                    {
                        /*  We are able to get events so add it to the list.
                         *  Since we are the only one managing the list we do
                         *  not need to worry about syncrhonization objects.
                         */

                        psiCur->psiNext = _PsiBindList;
                        _PsiBindList = psiCur;
                    }
                }
                else
                {
                    ps = PSLSocketErrorToPSLStatus(psiCur->sock, errSock);
                    goto Exit;
                }
            }
            else
            {
                ps = PSLSocketErrorToPSLStatus(psiCur->sock, errSock);
                goto Exit;
            }

            /*  Return the result of the operation to the waiting caller
             */

            *((PSLSTATUS*)((PSLUINT32)pMsg->alParam)) = ps;
            break;

        case BINDTRANSPORTMSG_SHUTDOWN:
            /*  We are shutting down- stop processing messages
             */

            fLoop = PSLFALSE;
            break;

        case BINDTRANSPORTMSG_TRANSPORT_CLOSE:
            /*  Transports that were not fully initialized will post a
             *  transport closed notification to indicate that any remaining
             *  references to the transport should be removed.
             */

            for (pmtpipLast = NULL, pmtpipCur = _PmtpipBindList;
                    NULL != pmtpipCur;
                    pmtpipLast = pmtpipCur, pmtpipCur = pmtpipCur->pmtpipNext)
            {
                /*  Keep looking if we do not have a matching entry
                 */

                if ((MTPIPROUTEROBJ*)pMsg->aParam0 != pmtpipCur)
                {
                    continue;
                }

                /*  Pull the match out of the list
                 */

                if (NULL != pmtpipLast)
                {
                    pmtpipLast->pmtpipNext = pmtpipCur->pmtpipNext;
                }
                else
                {
                    _PmtpipBindList = pmtpipCur->pmtpipNext;
                }

                /*  And release the transport
                 */

                MTPIPRouterRelease(pmtpipCur);

                /*  No need to keep looking for matches
                 */

                break;
            }

            /*  Report success
             */

            ps = PSLSUCCESS;
            break;

        case PSLSOCKETMSG_DATA_AVAILABLE:
        case PSLSOCKETMSG_CLOSE:
            /*  Provide some feedback
             */

            PSLTraceProgress("MTPIP BindRouterThread Event Received- "
                            "Socket: 0x%08x  Event: %s", pMsg->aParam0,
                            PSLSocketMsgToString(pMsg->msgID));

            /*  Reset the socket message so that notifications start to
             *  flow again
             */

            ps = PSLSocketMsgReset((SOCKET)pMsg->aParam0, PSLFALSE,
                            pMsg->msgID);
            PSLASSERT(PSL_SUCCEEDED(ps));

            /*  Locate the socket information from the list
             */

            for (psiLast = PSLNULL, psiCur = _PsiBindList;
                    PSLNULL != psiCur;
                    psiLast = psiCur, psiCur = psiCur->psiNext)
            {
                /*  Look for a matching socket
                 */

                if ((SOCKET)pMsg->aParam0 == psiCur->sock)
                {
                    break;
                }
            }

            /*  We may receive notifications for sockets that we have
             *  already transferred- just ignore them
             */

            if (PSLNULL == psiCur)
            {
                continue;
            }

            /*  Handle the notification
             */

            switch (pMsg->msgID)
            {
            case PSLSOCKETMSG_DATA_AVAILABLE:
                /*  Socket has received data- pull it off and start managing it
                 */

                ps = _BindSocketToRouterTransport(psiCur);
                ASSERT(PSL_SUCCEEDED(ps));
                break;

            case PSLSOCKETMSG_CLOSE:
                /*  Socket has closed- unbind the socket
                 */

                ps = _UnbindSocketFromRouterTransport(psiCur);
                PSLASSERT(PSL_SUCCEEDED(ps));
                break;

            default:
                /*  Should never get here
                 */

                PSLASSERT(FALSE);
                continue;
            }

            /*  We are done with this entry unless we did not receive enough
             *  data to complete the bind
             */

            if (PSLSUCCESS_WOULD_BLOCK != ps)
            {
                /*  And remove the entry from the list
                 */

                if (PSLNULL != psiLast)
                {
                    psiLast->psiNext = psiCur->psiNext;
                }
                else
                {
                    _PsiBindList = psiCur->psiNext;
                }

                /*  And release it
                 */

                SAFE_SOCKETINFODESTROY(psiCur);
            }
        }
    }

    /*  Report Success
     */

    ps = PSLSUCCESS;

Exit:
    /*  Mark the thread as shutdown
     */

    PSLAtomicCompareExchange(&_DwBindTransportState,
                            BINDTRANSPORTSTATE_SHUTDOWN,
                            BINDTRANSPORTSTATE_RUNNING);

    /*  Walk through and free any pending sockets and transports
     */

    while (PSLNULL != _PsiBindList)
    {
        psiCur = _PsiBindList;
        _PsiBindList = psiCur->psiNext;
        SAFE_SOCKETINFODESTROY(psiCur);
    }
    while (PSLNULL != _PmtpipBindList)
    {
        pmtpipCur = _PmtpipBindList;
        _PmtpipBindList = pmtpipCur->pmtpipNext;
        MTPIPRouterRelease(pmtpipCur);
    }

    /*  And check messages to make sure that we did not have a pending request
     *  that can no longer be serviced
     */

    while (PSLSUCCESS == PSLMsgPeek(_MqBindTransport, PSLNULL))
    {
        /*  Release any current message
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  And retrieve the next message
         */

        if (SUCCEEDED(PSLMsgGet(_MqBindTransport, &pMsg)))
        {
            /*  Check to see if this is an operation request
             */

            if (BINDTRANSPORTMSG_BIND_SOCKET == pMsg->msgID)
            {
                *((PSLSTATUS*)((PSLUINT32)pMsg->alParam)) =
                            PSLERROR_SHUTTING_DOWN;
            }
        }
    }
    SAFE_PSLMSGFREE(pMsg);
    SAFE_PSLSIGNALCLOSE(hSignalInit);
    return ps;
    pvParam;
}


/*
 *  _CheckBindRouterThreadState
 *
 *  Checks the status of the socket thread to make sure that it is up and
 *  running.
 *
 *  Arguments:
 *      PSLBOOL         fMustBeRunning      PSLTRUE if the thread must be
 *                                            running
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if thread is running, an error if
 *                        the thread must be running and is not,
 *                        PSLSUCCESS_NOT_AVAILABLE if the thread does not
 *                        have to be running and is not
 *
 */

PSLSTATUS _CheckBindRouterThreadState(PSLBOOL fMustBeRunning)
{
    PSLUINT32       dwState;
    PSLHANDLE       hSignalInit = PSLFALSE;
    PSLSTATUS       ps;

    /*  Loop here to determine what state the socket thread is in- unless
     *  the thread is active we want to loop here until we can get it into
     *  an active state.
     */

    do
    {
        /*  Determine the current state of the thread
         */

        dwState = PSLAtomicCompareExchange(&_DwBindTransportState,
                            BINDTRANSPORTSTATE_RUNNING,
                            BINDTRANSPORTSTATE_RUNNING);
        switch (dwState)
        {
        case BINDTRANSPORTSTATE_RUNNING:
            /*  Thread is active
             */

            ps = PSLSUCCESS;
            break;

        case BINDTRANSPORTSTATE_UNKNOWN:
            /*  The thread has never been initialized
             */

            ps = fMustBeRunning ?
                            PSLERROR_NOT_INITIALIZED : PSLSUCCESS_NOT_AVAILABLE;
            break;

        case BINDTRANSPORTSTATE_SHUTTING_DOWN:
        case BINDTRANSPORTSTATE_SHUTDOWN:
            /*  The thread is either not running or shortly will not be-
             *  report the correct error
             */

            ps = fMustBeRunning ?
                            PSLERROR_NOT_AVAILABLE : PSLSUCCESS_NOT_AVAILABLE;
            break;

        case BINDTRANSPORTSTATE_INITIALIZING:
            /*  Another thread is initializing the router- give it a chance
             *  to complete the work
             */

            PSLASSERT(PSLNULL == hSignalInit);
            ps = PSLSignalOpen(PSLFALSE, PSLFALSE, _RgchBindRouterInitSignal,
                            &hSignalInit);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            ps = PSLSignalWait(hSignalInit, PSLTHREAD_INFINITE);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        default:
            /*  Unknown init router state
             */

            PSLASSERT(FALSE);
            ps = PSLERROR_UNEXPECTED;
            break;
        }
    }
    while (BINDTRANSPORTSTATE_INITIALIZING == dwState);

Exit:
    SAFE_PSLSIGNALCLOSE(hSignalInit);
    return ps;
}


/*
 *  _MTPIPRouterInitialize
 *
 *  Creates the thread used to handle initialization and binding of a socket
 *  to a router.  This thread is a service thread that enables the bind
 *  initiatore thread to asyncrhounously determine whether the given socket
 *  is for a command/data connection or an event connection before
 *  completing the bind between the router and the transport.
 *
 *  Arguments:
 *      PSLUINT32       dwInitFlags         Initialization flags (see
 *                                            MTPInitialize)
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success, PSLERROR_INVALID_OPERATION if
 *                    already running
 *
 */

PSLSTATUS _MTPIPRouterInitialize(PSLUINT32 dwInitFlags)
{
    PSLUINT32       dwState = BINDTRANSPORTSTATE_RUNNING;
    PSLHANDLE       hThread = PSLNULL;
    PSLHANDLE       hSignalInit = PSLNULL;
    PSLMSGPOOL      mpBindTransport = PSLNULL;
    PSLMSGQUEUE     mqBindTransport = PSLNULL;
    PSLSTATUS       ps;
    PSLVOID*        pvParam = PSLNULL;

    /*  Make sure we are supposed to initialize
     */

    if (!(MTPINITFLAG_RESPONDER & dwInitFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Reserve enough bytes in contexts to handle our transport needs
     */

    ps = MTPContextReserveTransportBytes(MTPIPROUTER_TRANSPORT_SIZE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create the initialized signal
     */

    ps = PSLSignalOpen(PSLFALSE, PSLTRUE, _RgchBindRouterInitSignal,
                            &hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Loop here to determine what state the router is in- unless the router
     *  is active we want to loop here until we can get it into an active
     *  state.
     */

    do
    {
        /*  Try and set the router to initializing state
         */

        dwState = PSLAtomicCompareExchange(&_DwBindTransportState,
                            BINDTRANSPORTSTATE_INITIALIZING,
                            BINDTRANSPORTSTATE_UNKNOWN);
        switch (dwState)
        {
        case BINDTRANSPORTSTATE_RUNNING:
            /*  Router is already active- only one initialization is allowed
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;

        case BINDTRANSPORTSTATE_SHUTTING_DOWN:
        case BINDTRANSPORTSTATE_SHUTDOWN:
            /*  The entire transport is in the process of shutting down- do
             *  not attempt to initialize the router
             */

            ps = PSLERROR_SHUTTING_DOWN;
            goto Exit;

        case BINDTRANSPORTSTATE_INITIALIZING:
            /*  Another thread is initializing the router- give it a chance
             *  to complete the work
             */

            PSLASSERT(PSLNULL != hSignalInit);
            ps = PSLSignalWait(hSignalInit, PSLTHREAD_INFINITE);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        case BINDTRANSPORTSTATE_UNKNOWN:
            /*  We own initializing the router- nothing to do here
             */

            break;

        default:
            /*  Unknown init router state
             */

            PSLASSERT(FALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }
    }
    while (BINDTRANSPORTSTATE_UNKNOWN != dwState);

    /*  Validate our invariants before starting the initialization
     */

    PSLASSERT(PSLNULL == _HThreadBindTransport);
    PSLASSERT(PSLNULL == _MqBindTransport);
    PSLASSERT(PSLNULL == _MpBindTransport);

    /*  Make sure the signal is reset before we continue
     */

    ps = PSLSignalReset(hSignalInit);
    PSLASSERT(PSL_SUCCEEDED(ps));

    /*  Create the message queue and message pool for the init router
     *  thread.  These pools will be used internally to handle each new
     *  socket as it is bound until it is determined what type of socket
     *  it is (command/data or event) and it can be bound to a connection
     *  specific handler.
     */

    ps = MTPMsgGetQueueParam(&pvParam);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = PSLMsgQueueCreate(pvParam, MTPMsgOnPost, MTPMsgOnWait, MTPMsgOnDestroy,
                            &mqBindTransport);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pvParam = NULL;

    ps = PSLMsgPoolCreate(BINDTRANSPORT_MSG_POOL_SIZE, &mpBindTransport);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create the thread that will handle the messages for the router
     *  initialization function- note that the thread will block until
     *  the initialization goes to "running" so that we do not have to worry
     *  about contention issues with the data we have created.
     */

    ps = PSLThreadCreate(_BindRouterThread, NULL, PSLTRUE,
                            PSLTHREAD_DEFAULT_STACK_SIZE, &hThread);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize the static variables
     */

    _MqBindTransport = mqBindTransport;
    _MpBindTransport = mpBindTransport;
    _HThreadBindTransport = hThread;

    /*  Resume the thread so it can start executing
     */

    ps = PSLThreadResume(hThread);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        /*  Make sure the thread is terminated
         */

        PSLThreadTerminate(hThread, (PSLUINT32)PSLERROR_UNEXPECTED);
        goto Exit;
    }

    /*  Thread now has ownership of the static objects- forget them here
     */

    mqBindTransport = PSLNULL;
    mpBindTransport = PSLNULL;
    hThread = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    /*  If we failed to initialize for any reason we want to reset the
     *  initialization state and set the event to unblock any othre threads
     *  that might be waiting
     */

    if (PSL_FAILED(ps) && (BINDTRANSPORTSTATE_UNKNOWN == dwState))
    {
        /*  We should only get here in an initializing state
         */

        PSLASSERT(BINDTRANSPORTSTATE_INITIALIZING == _DwBindTransportState);

        /*  Reset the state
         */

        PSLAtomicExchange(&_DwBindTransportState, BINDTRANSPORTSTATE_UNKNOWN);

        /*  And signal any other threads
         */

        if (PSLNULL != hSignalInit)
        {
            PSLSignalSet(hSignalInit);
        }
    }
    SAFE_PSLTHREADCLOSE(hThread);
    SAFE_PSLMSGPOOLDESTROY(mpBindTransport);
    SAFE_PSLMSGQUEUEDESTROY(mqBindTransport);
    SAFE_MTPMSGRELEASEQUEUEPARAM(pvParam);
    SAFE_PSLSIGNALCLOSE(hSignalInit);
    return ps;
}


/*
 *  MTPIPTransportInitialize
 *
 *  Creates the thread used to handle initialization and binding of a socket
 *  to a router.  This thread is a service thread that enables the bind
 *  initiatore thread to asyncrhounously determine whether the given socket
 *  is for a command/data connection or an event connection before
 *  completing the bind between the router and the transport.
 *
 *  Arguments:
 *      PSLUINT32       dwInitFlags         Initialization flags (see
 *                                            MTPInitialize)
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success, PSLERROR_INVALID_OPERATION if
 *                    already running
 *
 */

PSLSTATUS MTPIPTransportInitialize(PSLUINT32 dwInitFlags)
{
    PSLSTATUS       ps;

    /*  Make sure that we are supposed to initialize
     */

    if (!(MTPINITFLAG_IP_TRANSPORT & dwInitFlags))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If sockets have already been initialize, we have been previously
     *  initialized- only one initialization is allowed
     */

    if (_FPSLSocketRunning)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Initialize the PSLSockets environment
     */

    ps = PSLSocketInitialize();
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    _FPSLSocketRunning = PSLTRUE;

    /*  Handle the router and proxy initialization as appropriate
     */

    if (MTPINITFLAG_RESPONDER & dwInitFlags)
    {
        ps = _MTPIPRouterInitialize(dwInitFlags);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPIPTransportUninitialize
 *
 *  Called to uninitialize the MTP/IP Transport
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPTransportUninitialize()
{
    PSLUINT32       dwResult;
    PSLUINT32       dwInitialState;
    PSLUINT32       dwState;
    PSLMSGQUEUE     mqShutdown = PSLNULL;
    PSLSTATUS       ps;
    PSLMSG*         pMsg = PSLNULL;

    /*  Check the state of the thread- blocking if initialization is pending
     */

    ps = _CheckBindRouterThreadState(PSLFALSE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Tack ownership of shutting down the thread- we assume that thread is
     *  in a good state initially and that we are just shutting down normally.
     *  In the event the thread has already exited abnormally we need to do
     *  an extra check to make sure that we really own the shutdown.
     */

    dwInitialState = BINDTRANSPORTSTATE_RUNNING;
    do
    {
        /*  Make sure that we own the shutdown
         */

        dwState = PSLAtomicCompareExchange(&_DwBindTransportState,
                            BINDTRANSPORTSTATE_SHUTTING_DOWN,
                            dwInitialState);
        switch (dwState)
        {
        case BINDTRANSPORTSTATE_RUNNING:
            /*  No work to do here- we are responsible for shutting down the
             *  bind router thread
             */

            PSLASSERT(BINDTRANSPORTSTATE_RUNNING == dwInitialState);
            break;

        case BINDTRANSPORTSTATE_SHUTDOWN:
            /*  If the old state and the initial state are the same then
             *  we succesfully got ownership of the shutdown- stop working
             */

            if (dwInitialState == dwState)
            {
                dwInitialState = BINDTRANSPORTSTATE_RUNNING;
                break;
            }

            /*  We want to take ownership of the shutdown process at this point
             *  so switch the initial state and try again
             */

            dwInitialState = BINDTRANSPORTSTATE_SHUTDOWN;
            break;

        case BINDTRANSPORTSTATE_UNKNOWN:
            /*  If we never attempted to initialize then we should just
             *  shutdown quietly
             */

            ps = PSLERROR_NOT_INITIALIZED;
            goto Exit;

        case BINDTRANSPORTSTATE_SHUTTING_DOWN:
        case BINDTRANSPORTSTATE_INITIALIZING:
            /*  We should only ever initialize once and shutdown once- if
             *  we get here this is an error
             *
             *  FALL THROUGH INTENTIONAL
             */

        default:
            /*  Should never get here
             */

            PSLASSERT(FALSE);
            ps = PSLERROR_NOT_INITIALIZED;
            goto Exit;
        }
    } while (BINDTRANSPORTSTATE_SHUTDOWN == dwInitialState);

    /*  See if we need to tell the thread to shutdown
     */

    if (BINDTRANSPORTSTATE_RUNNING == dwState)
    {
        /*  Notify the thread that it needs to shutdown
         */

        ps = PSLMsgAlloc(_MpBindTransport, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = BINDTRANSPORTMSG_SHUTDOWN;

        /*  Post the message and transfer ownership of the message to the queue
         */

        ps = PSLMsgPost(_MqBindTransport, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = PSLNULL;
    }

    /*  Make sure that the thread is closed
     */

    if (PSLNULL != _HThreadBindTransport)
    {
        /*  Wait for the thread to shutdown
         */

        ps = PSLThreadGetResult(_HThreadBindTransport, INFINITE, &dwResult);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Close the thread
         */

        SAFE_PSLTHREADCLOSE(_HThreadBindTransport);
    }

    /*  Release the message pool and remove the queue from the state so
     *  that we can unregister it from the shutdown service
     */

    SAFE_PSLMSGPOOLDESTROY(_MpBindTransport);
    mqShutdown = _MqBindTransport;
    _MqBindTransport = PSLNULL;

    /*  Close down the sockets support
     */

    if (_FPSLSocketRunning)
    {
        ps = PSLSocketUninitialize();
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        _FPSLSocketRunning = PSLFALSE;
    }

    /*  Reset the state of the router to unknown
     */

    dwState = PSLAtomicExchange(&_DwBindTransportState,
                            BINDTRANSPORTSTATE_UNKNOWN);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMSGQUEUEDESTROY(mqShutdown);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPIPUnbindRouterTransport
 *
 *  Notifies the transport bind thread that a specific router transport is
 *  no longer bound.
 *
 *  Arguments:
 *      MTPIPROUTEROBJ* pmtpip              Transport to unbind
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPUnbindRouterTransport(MTPIPROUTEROBJ* pmtpip)
{
    PSLMSG*             pMsg = PSLNULL;
    PSLSTATUS           ps;

    /*  Validate arguments
     */

    if (PSLNULL == pmtpip)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure the thread is able to accept the command- if we discover that
     *  it is not available we do not need to treat this as catastrophic
     */

    ps = _CheckBindRouterThreadState(PSLFALSE);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    /*  Notify the bind transport thread that we have closed down so that it
     *  can release the transport info if it is still tracking it
     */

    ps = PSLMsgAlloc(_MpBindTransport, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    pMsg->msgID = BINDTRANSPORTMSG_TRANSPORT_CLOSE;
    pMsg->aParam0 = (PSLPARAM)pmtpip;

    ps = PSLMsgPost(_MqBindTransport, pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg = PSLNULL;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPIPBindInitiator
 *
 *  Binds a socket from an initiator and sets up a responder MTP/IP transport.
 *  The socket may be either the command/data connection or the event
 *  connection.
 *
 *  Arguments:
 *      SOCKET          sockInitiator       Socket resulting from accepting
 *                                            the initiator connection
 *      PSL_CONST struct sockaddr*
 *                      psaInitiator        Initiators network address
 *      PSLUINT32       cbAddr              Length of the address
 *      PSLPARAM        aParamTransport     Transport specific parameter used
 *                                            during transport monitor callbacks
 *
 */

PSLSTATUS MTPIPBindInitiator(SOCKET sockInitiator,
                            PSL_CONST struct sockaddr* psaInitiator,
                            PSLUINT32 cbAddr, PSLPARAM aParamTransport)
{
    PSLSTATUS           ps;
    PSLSTATUS           psMsg;
    SOCKETINFO*         psi = PSLNULL;
    PSLMSG*             pMsg = PSLNULL;

    /*  Validate arguments
     */

    if ((INVALID_SOCKET == sockInitiator) || (PSLNULL == psaInitiator) ||
        (sizeof(SOCKADDR_STORAGE) < cbAddr))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure the bind transport thread is running
     */

    ps = _CheckBindRouterThreadState(PSLTRUE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create an entry to hold the socket information
     */

    psi = (SOCKETINFO*)PSLMemAlloc(PSLMEM_PTR, sizeof(SOCKETINFO));
    if (PSLNULL == psi)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /*  And initialize it
     */

#ifdef PSL_ASSERTS
    psi->dwCookie = SOCKETINFO_COOKIE;
#endif  /* PSL_ASSERTS */
    psi->sock = sockInitiator;
    PSLCopyMemory(&(psi->saClient), psaInitiator, sizeof(psi->saClient));
    psi->aParamTransport = aParamTransport;

    /*  Send a message to notify the bind thread of the socket- note that we
     *  have to do this before we start up notifications so that the thread
     *  learns of the socket before it gets any notifications.
     */

    ps = PSLMsgAlloc(_MpBindTransport, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    pMsg->msgID = BINDTRANSPORTMSG_BIND_SOCKET;
    pMsg->aParam0 = (PSLPARAM)psi;
    pMsg->alParam = (PSLLPARAM)&psMsg;

    /*  Post the message and transfer ownership of the socket information to
     *  the message queue
     */

    ps = PSLMsgSend(_MqBindTransport, pMsg, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg = PSLNULL;
    aParamTransport = MTPTRANSPORTPARAM_UNKNOWN;
    if (PSL_FAILED(psMsg))
    {
        ps = psMsg;
        goto Exit;
    }
    psi = PSLNULL;
    sockInitiator = INVALID_SOCKET;

Exit:
    /*  Notify the other end of the connection that we failed to initialize
     *  if there was a problem
     */

    if (INVALID_SOCKET != sockInitiator)
    {
        PSLASSERT(PSL_FAILED(ps));
        MTPIPSendInitFailed(sockInitiator, MTPIP_FAIL_UNSPECIFIED);
        PSLSocketClose(sockInitiator);
    }
    CHECK_MTPTRANSPORTNOTIFY(MTPTRANSPORTEVENT_CLOSED, aParamTransport);
    SAFE_PSLMSGFREE(pMsg);
    SAFE_SOCKETINFODESTROY(psi);
    return ps;
}


/*
 *  MTPIPBindResponder
 *
 *  Binds the MTP/IP transport to the specifed responder.
 *
 *  Arguments:
 *      PSLCWSTR        szInitiatorName     The initiator name
 *      PSLLOOKUPTABLE  hHandlerTable       Handler table containing the
 *                                            callback handlers
 *      PSL_CONST struct sockaddr*
 *                      psaResponder        Address of responder to contact
 *      PSLUINT32       cbResponderAddr     Length of the responder
 *      PSLUINT32       dwMSTimeout         Time to wait for a connection
 *      MTPCONTEXT**    ppmtpctx            Resulting context on a succesful
 *                                            bind
 */

PSLSTATUS MTPIPBindResponder(PSLCWSTR szInitiatorName,
                            PSLMSGQUEUE mqEventHandler,
                            PSLLOOKUPTABLE hHandlerTable,
                            PSL_CONST struct sockaddr* psaResponder,
                            PSLUINT32 cbResponderAddr,
                            PSLUINT32 dwMSTimeout,
                            MTPCONTEXT** ppmtpctx)
{
    PSLSTATUS               ps;

    /*  Clear result for safety
     */

    if (PSLNULL != ppmtpctx)
    {
        *ppmtpctx = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((NULL == psaResponder) || (cbResponderAddr < sizeof(SOCKADDR)) ||
        (NULL == ppmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Currently not implemented
     */

    ps = PSLERROR_NOT_IMPLEMENTED;

Exit:
    return ps;
    dwMSTimeout;
    hHandlerTable;
    mqEventHandler;
    szInitiatorName;
}


/*
 *  MTPIPUDNToMACAddressStr
 *
 *  Converts a UPnP UDN for an MTP/IP endpoint to a MAC address
 *
 *  Arguments:
 *      PSLCWSTR        szUDN               UDN to convert
 *      PSLWCHAR*       rgchMACDest         Destination for MAC address
 *      PSLUINT32       cchMACDest          Size of destination MAC
 *      PSLUINT32*      pcchMACDest         Number of characters actually used
 *
 *  Returns:
 *      PSLSTATUS     PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPUDNToMACAddress(PSLCWSTR szUDN, PSLWCHAR* rgchMACDest,
                            PSLUINT32 cchMACDest, PSLUINT32* pcchMACUsed)
{
    PSLUINT32       cbMAC;
    PSLUINT32       cchMAC;
    PSLSTATUS       ps;
    PSLUINT32       idxMAC;
    PSLWCHAR*       pchCur;
    PSLCWSTR        szMAC;

    // Clear result for safety

    if (PSLNULL != rgchMACDest)
    {
        *rgchMACDest = L'\0';
    }
    if (PSLNULL != pcchMACUsed)
    {
        *pcchMACUsed = 0;
    }

    // Validate arguments

    if ((PSLNULL == szUDN) || ((PSLNULL != rgchMACDest) && (0 == cchMACDest)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // Check to see if we have UDN at the beginning

    if (0 == PSLStringICmpLen(_RgchUUID, szUDN, PSLSTRARRAYSIZE(_RgchUUID)))
    {
        szMAC = szUDN + PSLSTRARRAYSIZE(_RgchUUID);
    }
    else
    {
        szMAC = szUDN;
    }

    // The first part of the UDN is required to be fixed by the MTP/IP
    //  specification

    if (0 != PSLStringICmpLen(_RgchUUIDFixed, szMAC,
                            PSLSTRARRAYSIZE(_RgchUUIDFixed)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    szMAC += PSLSTRARRAYSIZE(_RgchUUIDFixed);

    // Now check to see if we have a 64- or 48-bit UDN

    if (0 == wcsnicmp(_RgchEUI48, szMAC, PSLSTRARRAYSIZE(_RgchEUI48)))
    {
        // Skip the extra characaters

        szMAC += PSLSTRARRAYSIZE(_RgchEUI48);

        // This is a 6 byte MAC

        cbMAC = 6;
    }
    else
    {
        cbMAC = 8;
    }
    cchMAC = cbMAC * sizeof(PSLWCHAR);

    // If we were only asked for the length of the string stop here

    if (PSLNULL == rgchMACDest)
    {
        // Must be able to return the length

        if (PSLNULL == pcchMACUsed)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        // Return the number of characters required to hold the MAC (two
        //  characters required per byte)

        *pcchMACUsed = cchMAC;
        ps = PSLSUCCESS;
        goto Exit;
    }

    // Make sure we have enough space to hold the string and the terminator

    if (cchMACDest < (cchMAC + 1))
    {
        // Try to return the correct length

        if (PSLNULL != pcchMACUsed)
        {
            *pcchMACUsed = cchMAC;
        }

        // Report insufficient buffer

        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    // Extract the MAC address from the UDN

    for (idxMAC = 0, pchCur = rgchMACDest; idxMAC < cchMAC; idxMAC++, pchCur++)
    {
        // If we have hit the '-' in a 64-bit MAC skip it

        if ((8 == cbMAC) && (L'-' == *szMAC) && (2 == idxMAC))
        {
            szMAC++;
        }

        // We only accept hex digits

        if (!PSLIsHexDigit(*szMAC))
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        // Copy it over to the destination string

        *pchCur = *szMAC;
        szMAC++;
    }

    // And terminate

    *pchCur = L'\0';

    // Return size if requested

    if (PSLNULL != pcchMACUsed)
    {
        *pcchMACUsed = cchMAC;
    }

    // Report success

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPIPMACAddressStrToUDN
 *
 *  Converts a MAC address to the correct format for an MTP/IP fully
 *  qualified UDN.
 *
 *  Arguments:
 *      PSLCWSTR         szMAC               MAC address to convert
 *      PSLWCHAR*          rgchUDNDest         Destination for UDN
 *      PSLUINT32           cchUDNDest          Size of destination UDN
 *      PSLUINT32*          pcchUDNUsed         Number of characters actually used
 *
 *  Returns:
 *      PSLSTATUS     PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPMACAddressStrToUDN(PSLCWSTR szMAC, PSLWCHAR* rgchUDNDest,
                            PSLUINT32 cchUDNDest, PSLUINT32* pcchUDNUsed)
{
    PSLUINT32       cchMAC;
    PSLSTATUS       ps;
    PSLUINT32       idxMAC;
    PSLWCHAR*       pchDest;

    // Clear result for safety

    if (PSLNULL != pcchUDNUsed)
    {
        *pcchUDNUsed = 0;
    }

    // Validate arguments

    if ((PSLNULL == szMAC) || ((PSLNULL != rgchUDNDest) && (0 == cchUDNDest)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // MAC addresses are either 12 or 16 characters in length

    ps = PSLStringLen(szMAC, 16, &cchMAC);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    if ((12 != cchMAC) && (16 != cchMAC))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    // If no destination was provided just return the fixed length for the UDN

    if (PSLNULL == rgchUDNDest)
    {
        // We must have space to return the size

        if (PSLNULL == pcchUDNUsed)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        *pcchUDNUsed = MTPIP_UDN_LENGTH;
        ps = PSLSUCCESS;
        goto Exit;
    }

    // If destination was provided make sure it is large enough to hold the
    //  UDN and the terminating character

    if (cchUDNDest <= MTPIP_UDN_LENGTH)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    // Initialize our workspace with the template portions

    pchDest = rgchUDNDest;
    ps = PSLStringCopy(pchDest, cchUDNDest, _RgchUUID);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pchDest += PSLSTRARRAYSIZE(_RgchUUID);
    cchUDNDest -= PSLSTRARRAYSIZE(_RgchUUID);
    ps = PSLStringCopy(pchDest, cchUDNDest, _RgchUUIDFixed);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pchDest += PSLSTRARRAYSIZE(_RgchUUIDFixed);

    // Walk through the host string validating the digits and packing it
    //  into the appropriate field of the UDN

    for (idxMAC = 0; idxMAC < 17; idxMAC++, pchDest++)
    {
        // If we are dealing with a short MAC make sure we initialize the
        //  first WORD as 0xffff

        if ((12 == cchMAC) && (idxMAC < 4))
        {
            *pchDest = L'f';
            continue;
        }

        // Insert the hyphen if necessary

        if (4 == idxMAC)
        {
            *pchDest = L'-';
            continue;
        }

        // Make sure the digits in the MAC address are valid

        if (!PSLIsHexDigit(*szMAC))
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        // And copy them over

        *pchDest = *szMAC;
        szMAC++;
    }
    *pchDest = L'\0';

    // Return number of characters used

    if (PSLNULL != pcchUDNUsed)
    {
        *pcchUDNUsed = MTPIP_UDN_LENGTH;
    }

    // Report success

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPIPGenerateGUID
 *
 *  Generates an MTP/IP compatible GUID from a MAC address
 *
 *  Arguments:
 *      PSL_CONST PSLBYTE*
 *                      pbMAC               Mac address to use
 *      PSLUINT32       cbMAC               Length of the MAC address
 *      PSLGUID*        pguidResult         Resulting GUID
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on succcess
 *
 */

PSLSTATUS MTPIPGenerateGUID(PSL_CONST PSLBYTE* pbMAC, PSLUINT32 cbMAC,
                            PSLGUID* pguidResult)
{
    PSLSTATUS       ps;
    PSLBYTE*        pbDest;

    /*  Clear result for safety
     */

    if (PSLNULL != pguidResult)
    {
        PSLZeroMemory(pguidResult, sizeof(*pguidResult));
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pbMAC) || ((6 != cbMAC) && (8 != cbMAC)) ||
        (PSLNULL == pguidResult))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine whether or not we need to promote a 4-byte MAC address
     *  to six bytes and then add the remaining portion of the GUID
     */

    pbDest = (PSLBYTE*)&(pguidResult->Data4);
    if (6 == cbMAC)
    {
        PSLSetMemory(pbDest, 0xff, sizeof(PSLUINT16));
        pbDest += sizeof(PSLUINT16);
    }
    PSLCopyMemory(pbDest, pbMAC, cbMAC);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


