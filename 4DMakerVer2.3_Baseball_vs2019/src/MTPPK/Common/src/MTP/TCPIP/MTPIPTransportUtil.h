/*
 *  MTPIPTransportUtil.h
 *
 *  Contains declarations of internal MTP/IP Tranport structures and functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPIPTRANSPORTUTIL_H_
#define _MTPIPTRANSPORTUTIL_H_

/*  Common MTP/IP transport messages
 */

#define MAKE_MTPIPTRANSPORT_MSGID(_id, _flags) \
    MAKE_PSL_MSGID(MTP_TRANSPORT_PRIVATE, \
            (MTPIPTRANSPORT_CUSTOM_MSG_BASE + _id), \
            (_flags))

#define MTPIPTRANSPORT_CUSTOM_MSG_BASE      100

enum
{
    MTPIPTRANSPORTMSG_UNKNOWN =             MAKE_MTPIPTRANSPORT_MSGID(0, 0),
    MTPIPTRANSPORTMSG_SHUTDOWN =            MAKE_MTPIPTRANSPORT_MSGID(1, 0),
};

/*  MTPIPUnbindRouterTransport
 *
 *  Unbinds a router transport from the binding thread.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPUnbindRouterTransport(
                            MTPIPROUTEROBJ* pmtpip);


/*  MTPIPGenerateGUID
 *
 *  Generates an MTP/IP compatible GUID from the MAC address provided
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPGenerateGUID(
                            PSL_CONST PSLBYTE* pbMAC,
                            PSLUINT32 cbMAC,
                            PSLGUID* pguidResult);

#endif  /* _MTPIPTRANSPORTUTIL_H_ */

