/*
 *  MTPIPParser.c
 *
 *  Contains implementations of the MTP/IP packet serialization/de-serialization
 *  routines to convert MTP/IP packet formats to internal MTP packet formats.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPIPTransportPrecomp.h"
#include "MTPIPPackets.h"

/*  Local Defines
 */

#define MTPIPPARSER_COOKIE      'mipP'

#define MTPIP_MIN_SOCKET_BUFFER 0x4000

/*  Local Types
 */

typedef struct _MTPIPPACKETINFO
{
    PSLUINT32               dwPacketType;
    PSLBYTE                 bPacketID;
} MTPIPPACKETINFO;

/*
 *  MTPIPPARSEROBJ
 *
 *  Internal structure which handles the parser information
 *
 */

typedef struct _MTPIPPARSEROBJ
{
    MTPSTREAMPARSEROBJ      mtpstrmPO;
#ifdef PSL_ASSERTS
    PSLUINT32               dwIPCookie;
#endif  /* PSL_ASSERTS */
    SOCKET                  sock;
    PSLUINT32               cbMaxPacket;
} MTPIPPARSEROBJ;

/*  Local Functions
 */

static PSLSTATUS PSL_API _MTPIPParserGetBytesAvailable(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pcbAvailable);

static PSLSTATUS PSL_API _MTPIPParserRead(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLVOID* pvDest,
                    PSLUINT32 cbDest,
                    PSLUINT32* pcbRead);

static PSLSTATUS PSL_API _MTPIPParserSend(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData,
                    PSLUINT32* pcbSent);

static PSLSTATUS PSL_API _MTPIPParserParseHeader(
                    MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPSTREAMHEADER pmtpstrmHdr,
                    PSLUINT32* pdwPacketType,
                    PSLUINT32* pcbPacketSize);

static PSLSTATUS PSL_API _MTPIPParserStoreHeader(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwPacketType,
                    PSLUINT32 cbPacketSize,
                    PXMTPSTREAMHEADER pmtpstrmHdr);

static PSLSTATUS PSL_API _MTPIPParserDestroy(
                    MTPSTREAMPARSER mtpstrmParser);


static PSLSTATUS PSL_API _MTPIPParserSetType(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwSocketType);

static PSLSTATUS PSL_API _MTPIPParserGetParserInfo(
                    MTPSTREAMPARSER mtpstrmParser,
                    MTPSTREAMPARSERINFO* pmtpstrmInfo,
                    PSLUINT32 cbInfo);

/*  Local Variables
 */

MTPSTREAMPARSERVTBL         _VtblMTPIPParser =
{
    _MTPIPParserGetBytesAvailable,
    _MTPIPParserRead,
    _MTPIPParserSend,
    _MTPIPParserParseHeader,
    _MTPIPParserStoreHeader,
    MTPStreamParserGetDataHeaderBase,
    _MTPIPParserDestroy,
    _MTPIPParserSetType,
    _MTPIPParserGetParserInfo,
    MTPStreamParserResetBase,
#ifdef MTP_INITIATOR
    MTPStreamParserSendOpRequestBase,
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    MTPStreamParserReadOpRequestBase,
    MTPStreamParserSendOpResponseBase,
#endif  /* MTP_RESPONDER */
#ifdef MTP_INITIATOR
    MTPStreamParserReadOpResponseBase,
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    MTPStreamParserSendEventBase,
#endif  /* MTP_RESPONDER */
#ifdef MTP_INITIATOR
    MTPStreamParserReadEventBase,
#endif  /* MTP_INITIATOR */
    MTPStreamParserSendStartDataBase,
    MTPStreamParserReadStartDataBase,
    MTPStreamParserSendDataBase,
    MTPStreamParserReadDataBase,
    MTPStreamParserSendEndDataBase,
    MTPStreamParserReadEndDataBase,
#ifdef MTP_INITIATOR
    MTPStreamParserSendCancelBase,
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    MTPStreamParserReadCancelBase,
#endif  /* MTP_RESPONDER */
    MTPStreamParserSendProbeRequestBase,
    MTPStreamParserReadProbeRequestBase,
    MTPStreamParserSendProbeResponseBase,
    MTPStreamParserReadProbeResponseBase,
};


static const MTPIPPACKETINFO    _RgpiPacketType[] =
{
    {
        MTPSTREAMPACKET_UNKNOWN,
        MTPIPPACKETID_UNKNOWN
    },
    {
        MTPSTREAMPACKET_OPERATION_REQUEST,
        MTPIPPACKETID_OPERATION_REQUEST
    },
    {
        MTPSTREAMPACKET_OPERATION_RESPONSE,
        MTPIPPACKETID_OPERATION_RESPONSE
    },
    {
        MTPSTREAMPACKET_EVENT,
        MTPIPPACKETID_EVENT
    },
    {
        MTPSTREAMPACKET_START_DATA,
        MTPIPPACKETID_START_DATA
    },
    {
        MTPSTREAMPACKET_DATA,
        MTPIPPACKETID_DATA
    },
    {
        MTPSTREAMPACKET_CANCEL,
        MTPIPPACKETID_CANCEL
    },
    {
        MTPSTREAMPACKET_END_DATA,
        MTPIPPACKETID_END_DATA
    },
    {
        MTPSTREAMPACKET_PROBE_REQUEST,
        MTPIPPACKETID_PROBE_REQUEST
    },
    {
        MTPSTREAMPACKET_PROBE_RESPONSE,
        MTPIPPACKETID_PROBE_RESPONSE
    },
    {
        MTPIPPACKET_INIT_COMMAND_REQUEST,
        MTPIPPACKETID_INIT_COMMAND_REQUEST
    },
    {
        MTPIPPACKET_INIT_COMMAND_ACK,
        MTPIPPACKETID_INIT_COMMAND_ACK
    },
    {
        MTPIPPACKET_INIT_EVENT_REQUEST,
        MTPIPPACKETID_INIT_EVENT_REQUEST
    },
    {
        MTPIPPACKET_INIT_EVENT_ACK,
        MTPIPPACKETID_INIT_EVENT_ACK
    },
    {
        MTPIPPACKET_INIT_FAIL,
        MTPIPPACKETID_INIT_FAIL
    }
};


/*
 *  _MTPIPParserGetBytesAvailable
 *
 *  Returns the number of bytes currently available in the stream.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Stream parser object
 *      PSLUINT32*      pcbAvailable        Return buffer for number of bytes
 *                                            currently available in the stream
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPIPParserGetBytesAvailable(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pcbAvailable)
{
    PSLINT32            errSock;
    MTPIPPARSEROBJ*     pmtpipPO;
    PSLSTATUS           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbAvailable)
    {
        *pcbAvailable = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpstrmParser) || (NULL == pcbAvailable))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Ask the socket directly for the result
     */

    errSock = ioctlsocket(pmtpipPO->sock, FIONREAD, pcbAvailable);
    if (SOCKET_ERROR == errSock)
    {
        ps = PSLSocketErrorToPSLStatus(pmtpipPO->sock, errSock);
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPIPParserRead
 *
 *  Reads bytes off the socket stream up to the number provided
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to read from
 *      PSLVOID*        pvDest              Destination to read into
 *      PSLUINT32       cbDest              Size of the destination
 *      PSLUINT32*      pcbRead             Number of bytes actually read
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPIPParserRead(MTPSTREAMPARSER mtpstrmParser,
                    PSLVOID* pvDest, PSLUINT32 cbDest, PSLUINT32* pcbRead)
{
    PSLINT32            cbRead;
    MTPIPPARSEROBJ*     pmtpipPO;
    PSLSTATUS           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbRead)
    {
        *pcbRead = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpstrmParser) || (NULL == pvDest) || (NULL == pcbRead))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Ask the socket directly for the result
     */

    cbRead = recv(pmtpipPO->sock, (char*)pvDest,
                            (int)min(cbDest, MTPSTREAM_MAX_DATA_PACKET), 0);
    if (SOCKET_ERROR == cbRead)
    {
        ps = PSLSocketErrorToPSLStatus(pmtpipPO->sock, cbRead);
        goto Exit;
    }

    /*  Return the number of bytes read
     */

    *pcbRead = cbRead;
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPIPParserSend
 *
 *  Sends the specified data on the specified socket blocking until
 *  all the data has been accepted.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Stream parser to send data on
 *      PSL_CONST PSLVOID*
 *                      pvData              Data to be sent
 *      PSLUINT32       cbData              Amount of data to send
 *      PSLUINT32*      pcbSent             Number of bytes actually sent
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPIPParserSend(MTPSTREAMPARSER mtpstrmParser,
                    PSL_CONST PSLVOID* pvData, PSLUINT32 cbData,
                    PSLUINT32* pcbSent)
{
    PSLUINT32           cbCurrent;
    PSLUINT32           cbSent;
    MTPIPPARSEROBJ*     pmtpipPO;
    fd_set              fdWrite;
    PSLINT32            nSelect;
    PSLSTATUS           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbSent)
    {
        *pcbSent = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpstrmParser) || ((0 < cbData) && (PSLNULL == pvData)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Make sure that we will not have a problem sending too large
     *  an object
     */

    PSLASSERT(MTPSTREAM_MAX_DATA_PACKET >= cbData);
    if (MTPSTREAM_MAX_DATA_PACKET < cbData)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Start sending the data- we may have to pause to wait for
     *  the socket to free up enough space for it
     */

    for (cbSent = 0, cbCurrent = 0;
            cbSent < cbData;
            cbSent += cbCurrent)
    {
        /*  If we have already sent a portion of the packet force
         *  blocking
         */

        while (0 < cbSent)
        {
            /*  Wait for the socket to be ready for writing
             */

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4127)  /* conditional expression is constant */
#endif  /* _MSC_VER */
            FD_ZERO(&fdWrite);
            FD_SET(pmtpipPO->sock, &fdWrite);
#ifdef _MSC_VER
#pragma warning(pop)
#endif  /* _MSC_VER */
            nSelect = select(0, NULL, &fdWrite, NULL, 0);
            if (SOCKET_ERROR == nSelect)
            {
                ps = PSLSocketErrorToPSLStatus(pmtpipPO->sock, nSelect);
                PSLASSERT(PSL_FAILED(ps));
                goto Exit;
            }

            /*  If one socket is ready (the one we requested) stop
             *  waiting
             */

            if (1 == nSelect)
            {
                PSLASSERT(FD_ISSET(pmtpipPO->sock, &fdWrite));
                break;
            }
        }

        /*  Send as much of the packet as we can
         */

        cbCurrent = send(pmtpipPO->sock, (char*)pvData + cbSent,
                            (cbData - cbSent), 0);
        if (SOCKET_ERROR == cbCurrent)
        {
            ps = PSLSocketErrorToPSLStatus(pmtpipPO->sock, cbCurrent);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            cbCurrent = 0;
        }
    }

    /*  Return number of bytes actually sent
     */

    if (PSLNULL != pcbSent)
    {
        *pcbSent = cbSent;
    }
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPIPParserParseHeader
 *
 *  Parses the header from the stream and returns the packet type and packet
 *  size.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PCXMTPSTREAMHEADER
 *                      pmtpstrmHdr         Header to parse
 *      PSLUINT32*      pdwPacketType       Header type
 *      PSLUINT32*      pcbPacketSize       Size of the header
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPIPParserParseHeader(MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPSTREAMHEADER pmtpstrmHdr, PSLUINT32* pdwPacketType,
                    PSLUINT32* pcbPacketSize)
{
    PSLUINT32           idxType;
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwPacketType)
    {
        *pdwPacketType = MTPSTREAMPACKET_UNKNOWN;
    }
    if (PSLNULL != pcbPacketSize)
    {
        *pcbPacketSize = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pmtpstrmHdr) ||
        (PSLNULL == pdwPacketType) || (PSLNULL == pcbPacketSize))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Validate that this data really is for our transport
     */

    if ((MTPIP_TRANSPORT_ID != pmtpstrmHdr->bTransportID) ||
        (0 != MTPLoadUInt16(&(pmtpstrmHdr->wTransportData))))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Determine the packet type from the packet ID
     */

    for (idxType = 0;
            (idxType < PSLARRAYSIZE(_RgpiPacketType)) &&
                pmtpstrmHdr->bPacketID != _RgpiPacketType[idxType].bPacketID;
            idxType++)
    {
    }
    if (PSLARRAYSIZE(_RgpiPacketType) <= idxType)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }
    *pdwPacketType = _RgpiPacketType[idxType].dwPacketType;

    /*  And pull out the size
     */

    *pcbPacketSize = MTPLoadUInt32(&(pmtpstrmHdr->cbPacket));
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPIPParserStoreHeader
 *
 *  Stores information in an outbound packet header.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32       dwPacketType        Stream parser packet type
 *      PSLUINT32       cbPacketSize        Size of the packet
 *      PXMTPSTREAMHEADER
 *                      pmtpstrmHdr         Destination header location
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPIPParserStoreHeader(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwPacketType, PSLUINT32 cbPacketSize,
                    PXMTPSTREAMHEADER pmtpstrmHdr)
{
    PSLUINT32           idxType;
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpstrmHdr)
    {
        PSLZeroMemory(pmtpstrmHdr, sizeof(*pmtpstrmHdr));
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pmtpstrmHdr))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure this packet conforms to our size requirements
     */

    PSLASSERT(MTPSTREAM_MAX_DATA_PACKET >= cbPacketSize);
    if (MTPSTREAM_MAX_DATA_PACKET < cbPacketSize)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Store the packet ID based on the type
     */

    for (idxType = 0;
            (idxType < PSLARRAYSIZE(_RgpiPacketType)) &&
                    dwPacketType != _RgpiPacketType[idxType].dwPacketType;
            idxType++)
    {
    }
    if (PSLARRAYSIZE(_RgpiPacketType) <= idxType)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }
    pmtpstrmHdr->bPacketID = _RgpiPacketType[idxType].bPacketID;
    pmtpstrmHdr->bTransportID = MTPIP_TRANSPORT_ID;
    PSLASSERT(0 == MTPLoadUInt16(&(pmtpstrmHdr->wTransportData)));

    /*  And add the length
     */

    MTPStoreUInt32(&(pmtpstrmHdr->cbPacket), cbPacketSize);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPIPParserDestroy
 *
 *  Releases the specified MTP/IP Parser.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to destroy
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPIPParserDestroy(MTPSTREAMPARSER mtpstrmParser)
{
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Make sure we have work to do
     */

    if (PSLNULL == mtpstrmParser)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure all of the sockets have been release
     */

    SAFE_PSLSOCKETCLOSE(pmtpipPO->sock);

    /*  And let the base class do the rest work
     */

    ps = MTPStreamParserDestroyBase(mtpstrmParser);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  _MTPIPParserSetSocketType
 *
 *  Determines what type the socket is
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to set type for
 *      PSLUINT32       dwSocketType        Type for socket
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPIPParserSetType(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwSocketType)
{
    PSLUINT32           cbDesired;
    PSLUINT32           cbMaxDesired;
    PSLBOOL             fSetSize;
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpstrmParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Use the base class to validate and initialize the type
     */

    ps = MTPStreamParserSetTypeBase(mtpstrmParser, dwSocketType);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Determine the maximum buffer size for this socket type
     */

    switch (dwSocketType)
    {
    case MTPSTREAMPARSERTYPE_COMMAND:
        /*  Set this as a command socket
         */

        cbMaxDesired = 2 * MTPIP_MAX_COMMAND_PACKET;
        break;

    case MTPSTREAMPARSERTYPE_EVENT:
        /*  Set this as an event socket
         */

        cbMaxDesired = 2 * MTPIP_MAX_EVENT_PACKET;
        break;

    default:
        /*  Unknown type specified
         */

        PSLASSERT(FALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Attempt to update the buffer for both incoming and outgoing
     *  packets so that we can get more data moving through the
     *  system with less chunking
     */

    for (cbDesired = cbMaxDesired, fSetSize = PSLFALSE;
            !fSetSize && (cbDesired > MTPIP_MIN_SOCKET_BUFFER);
            cbDesired >>= 1)
    {
        fSetSize = (0 == setsockopt(pmtpipPO->sock, SOL_SOCKET,
                            SO_RCVBUF, (char*)&cbDesired,
                            sizeof(cbDesired)));
    }

    for (cbDesired = cbMaxDesired, fSetSize = PSLFALSE;
            !fSetSize && (cbDesired > MTPIP_MIN_SOCKET_BUFFER);
            cbDesired >>= 1)
    {
        fSetSize = (0 == setsockopt(pmtpipPO->sock, SOL_SOCKET,
                            SO_SNDBUF, (char*)&cbDesired,
                            sizeof(cbDesired)));
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPIPParserGetParserInfo
 *
 *  Retrieves information about the parser state
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to query
 *      MTPSTREAMPARSERINFO
 *                      pmtpstrmInfo        Parser information to return
 *      PSLUINT32       cbInfo              Size of the information object
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPIPParserGetParserInfo(MTPSTREAMPARSER mtpstrmParser,
                    MTPSTREAMPARSERINFO* pmtpstrmInfo,
                    PSLUINT32 cbInfo)
{
    PSLINT32            cbDesired;
    PSLINT32            errSock;
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;
    MTPIPPARSERINFO*    pmtpipInfo;

    /*  Clear result for safety
     */

    if ((PSLNULL != pmtpstrmInfo) && (0 < cbInfo))
    {
        PSLZeroMemory(pmtpstrmInfo, cbInfo);
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pmtpstrmInfo) ||
        (sizeof(MTPIPPARSERINFO) != cbInfo))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);
    pmtpipInfo = (MTPIPPARSERINFO*)pmtpstrmInfo;

    /*  Use the base class to initialize the core information
     */

    ps = MTPStreamParserGetParserInfoBase(mtpstrmParser,
                            &(pmtpipInfo->mtpstrmInfo),
                            sizeof(pmtpipInfo->mtpstrmInfo));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Extract the requested information
     */

    if (MTPSTREAMPARSERTYPE_UNKNOWN != pmtpipInfo->mtpstrmInfo.dwParserType)
    {
        /*  We should have a socket at this point
         */

        PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);

        /*  And report the maximum sizes directly from the socket
         */

        cbDesired = sizeof(pmtpipInfo->cbMaxTransmit);
        errSock = getsockopt(pmtpipPO->sock, SOL_SOCKET, SO_SNDBUF,
                                (char*)&(pmtpipInfo->cbMaxTransmit),
                                (int*)&cbDesired);
        if (SOCKET_ERROR == errSock)
        {
            ps = PSLSocketErrorToPSLStatus(pmtpipPO->sock, errSock);
            goto Exit;
        }

        cbDesired = sizeof(pmtpipInfo->cbMaxReceive);
        errSock = getsockopt(pmtpipPO->sock, SOL_SOCKET, SO_RCVBUF,
                                (char*)&(pmtpipInfo->cbMaxReceive),
                                (int*)&cbDesired);
        if (SOCKET_ERROR == errSock)
        {
            ps = PSLSocketErrorToPSLStatus(pmtpipPO->sock, errSock);
            goto Exit;
        }

        /*  Retrieve the local address of the socket
         */

        pmtpipInfo->cbAddr = sizeof(pmtpipInfo->saAddr);
        errSock = getsockname(pmtpipPO->sock,
                            (SOCKADDR*)&(pmtpipInfo->saAddr),
                            (int*)&(pmtpipInfo->cbAddr));
        if (SOCKET_ERROR == errSock)
        {
            ps = PSLSocketErrorToPSLStatus(pmtpipPO->sock, errSock);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPIPParserCreate
 *
 *  Creates an instance of the MTP/IP parser.  Each parser is created in a
 *  specific mode (responder/initiator) and manages retrieving data off/
 *  placing it on the network interface.
 *
 *  Arguments:
 *      PSLUINT32       dwMode              Parser mode
 *      MTPIPPARSER*    pmtpipParser        Return location for handle
 *
 *  Returns:
 *      PSLSTATUS   PSL_SUCCESS on success
 *
 */

PSLSTATUS MTPIPParserCreate(PSLUINT32 dwMode, MTPIPPARSER* pmtpipParser)
{
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpipParser)
    {
        *pmtpipParser = PSLNULL;
    }

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpipParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate a new parser using the MTP Stream Parser to create
     *  and initialize the base object
     */

    ps = MTPStreamParserCreate(dwMode, sizeof(MTPIPPARSEROBJ),
                            (MTPSTREAMPARSER*)&pmtpipPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Set the cookie so that we can validate this is one of our objects
     */

    pmtpipPO->dwIPCookie = MTPIPPARSER_COOKIE;
#endif  /*  PSL_ASSERTS */

    /*  Set the proper VTBL and mark the sockets as unbound
     */

    pmtpipPO->mtpstrmPO.vtbl = &_VtblMTPIPParser;
    pmtpipPO->sock = INVALID_SOCKET;

    /*  Return the parser handle
     */

    *pmtpipParser = pmtpipPO;
    pmtpipPO = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPIPPARSERDESTROY(pmtpipPO);
    return ps;
}


/*
 *  MTPIPParserBindSocket
 *
 *  Binds the specified socket for use as the given socket type
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser to bind socket to
 *      SOCKET          sock                Socket to bind to the parser
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserBindSocket(MTPIPPARSER mtpipParser, SOCKET sock)
{
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpipParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Validate that the socket is not already bound
     */

    PSLASSERT(INVALID_SOCKET == pmtpipPO->sock);
    if (INVALID_SOCKET != pmtpipPO->sock)
    {
        ps = PSLERROR_IN_USE;
        goto Exit;
    }

    /*  Stash the socket provided and mark the parser as ready
     */

    pmtpipPO->sock = sock;
    pmtpipPO->mtpstrmPO.dwFlags |= MTPSTREAMPARSERFLAGS_PARSER_READY;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPIPParserUnbindSocket
 *
 *  Unbinds the specified socket from the parser.
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser to unbind socket from
 *      SOCKET*         psock               Return buffer for socket
 *
 *  Returns:
 *      PSLTATUS    PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserUnbindSocket(MTPIPPARSER mtpipParser, SOCKET* psock)
{
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Clear result for safety
     */

    if (PSLNULL != psock)
    {
        *psock = INVALID_SOCKET;
    }

    /*  Validate Arguments
     */

    if (PSLNULL == mtpipParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  If the socket is not bound report not found
     */

    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Return the socket if desired
     */

    if (PSLNULL != psock)
    {
        *psock = pmtpipPO->sock;
    }

    /*  Disable the socket indicated
     */

    pmtpipPO->sock = INVALID_SOCKET;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPIParserIsBoundSocket
 *
 *  Reports whether or not the specified socket is currently bound to the
 *  parser provided.
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser to check
 *      SOCKET          sock                Socket to check
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if match, PSLSUCCESS_NOT_FOUND if not
 *
 */

PSLSTATUS MTPIPParserIsBoundSocket(MTPIPPARSER mtpipParser, SOCKET sock)
{
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Validate Arguments- assume that an invalid parser does not match
     *  the socket provided.
     */

    if (PSLNULL == mtpipParser)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  And determine whether or not we have a match
     */

    ps = (pmtpipPO->sock == sock) ? PSLSUCCESS : PSLSUCCESS_NOT_FOUND;

Exit:
    return ps;
}


/*
 *  MTPIPParserSocketMsgReset
 *
 *  Resets the message select event state for the socket bound to this parser
 *
 *  Arguments:
 *      MTPIPPARSER     mtpstrmParser       Parser to reset
 *      PSLBOOL         fResetMask          PSLTRUE if data is an event mask
 *      PSLUINT32       dwData              Event mask or message to reset
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserSocketMsgReset(MTPIPPARSER mtpstrmParser,
                            PSLBOOL fResetMask, PSLUINT32 dwData)
{
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpstrmParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  And determine whether or not we have a match
     */

    ps = PSLSocketMsgReset(pmtpipPO->sock, fResetMask, dwData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPIPParserSendInitCmdRequest
 *
 *  Sends an MTP/IP InitCommandRequest packet to the responder connected
 *  to the socket.
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser to use
 *      PSL_CONST PSLGUID*
 *                      pguidInitiator      Initator GUID
 *      PSLCWSTR        szInitiatorName     Initator name
 *      PSLUINT32       dwProtocolVer       Initiator protocol version
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserSendInitCmdRequest(MTPIPPARSER mtpipParser,
                            PSL_CONST PSLGUID* pguidInitiator,
                            PSLCWSTR szInitiatorName, PSLUINT32 dwProtocolVer)
{
    PSLUINT32               cbPacket;
    PSLUINT32               cbSent;
    PSLUINT32               cchInitiator;
    MTPIPINITCOMMANDREQUEST mtpipCmdReq;
    PSLSTATUS               ps;
    MTPIPPARSEROBJ*         pmtpipPO;

    /*  Validate Arguments- command request is only valid on the command
     *  socket so no need to specify the socket here
     */

    if (PSLNULL == mtpipParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only initiators may send this on a command socket
     */

    ps = MTPStreamParserCheckType(mtpipParser,
                            MTPSTREAMPARSERMODE_INITIATOR,
                            MTPSTREAMPARSERTYPE_COMMAND);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Make sure we support the protocol desired
     */

    if (MTPIP_BINARY_PROTOCOL_VERSION != dwProtocolVer)
    {
        ps = PSLERROR_UNSUPPORTED_VERSION;
        goto Exit;
    }

    /*  Fill out the simple information
     */

    mtpipCmdReq.mtpstrmHeader.bPacketID = MTPIPPACKETID_INIT_COMMAND_REQUEST;
    mtpipCmdReq.mtpstrmHeader.bTransportID = MTPIP_TRANSPORT_ID;
    MTPStoreUInt16(&(mtpipCmdReq.mtpstrmHeader.wTransportData), 0);
    if (PSLNULL != pguidInitiator)
    {
        MTPStoreGUID(&(mtpipCmdReq.guidInitiator), pguidInitiator);
    }
    else
    {
        ZeroMemory(&(mtpipCmdReq.guidInitiator),
                            sizeof(mtpipCmdReq.guidInitiator));
    }

    /*  Copy over the responder name
     */

    ps = MTPStoreUTF16String(mtpipCmdReq.varFields.szInitiatorStart,
            sizeof(mtpipCmdReq.varFields.maxFields.szInitiator), &cchInitiator,
            szInitiatorName, PSLSTRING_MAX_CCH);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And add in the version number
     */

    MTPStoreUInt32((PXPSLUINT32)
        (&(mtpipCmdReq.varFields.szInitiatorStart) + cchInitiator + 1),
        dwProtocolVer);

    /*  Set the packet size- remember min size already accounts for the
     *  terminating character and the protocol version
     */

    cbPacket = MTPIPINITCOMMANDREQUEST_MIN_SIZE +
                            (cchInitiator * sizeof(PSLWCHAR));
    MTPStoreUInt32(&(mtpipCmdReq.mtpstrmHeader.cbPacket), cbPacket);

    /*  And send it over the network
     */

    ps = _MTPIPParserSend(mtpipParser, &mtpipCmdReq, cbPacket, &cbSent);
    if (PSL_FAILED(ps) || (cbSent != cbPacket))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPIPParserReadInitCmdRequest
 *
 *  Reads an InitCommandRequest packet from the specified socket.
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser instance to use
 *      PSLGUID*        pguidInitiator      Return buffer for initiator GUID
 *      PSLWSTR         rgchInitiator       Return buffer for initiator name
 *      PSLUINT32       cchMaxInitiator     Size of initator name buffer
 *      PSLUINT32*      pchInitiator        Return buffer for length of name
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserReadInitCmdRequest(MTPIPPARSER mtpipParser,
                            PSLGUID* pguidInitiator, PSLWSTR rgchInitiator,
                            PSLUINT32* pchInitiator, PSLUINT32 cchMaxInitiator)
{
    PSLUINT32               cbData;
    PSLUINT32               cchMax;
    PSLUINT32               cchInitiator;
    PSLUINT32               dwProtVer;
    MTPIPINITCOMMANDREQUEST mtpipInitCmdReq;
    PSLSTATUS               ps;
    MTPIPPARSEROBJ*         pmtpipPO;

    /*  Clear results for safety
     */

    if (PSLNULL != pguidInitiator)
    {
        PSLZeroMemory(pguidInitiator, sizeof(*pguidInitiator));
    }
    if (PSLNULL != rgchInitiator)
    {
        *rgchInitiator = L'\0';
    }
    if (PSLNULL != pchInitiator)
    {
        *pchInitiator = 0;
    }

    /*  Validate Arguments- command request is only valid on the command
     *  socket so no need to specify the socket here
     */

    if ((PSLNULL == mtpipParser) ||
        (PSLNULL != rgchInitiator) & (0 == cchMaxInitiator))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);


    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may read this packet from the command socket
     */

    ps = MTPStreamParserCheckType(mtpipParser,
                            MTPSTREAMPARSERMODE_RESPONDER,
                            MTPSTREAMPARSERTYPE_COMMAND);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Standard operation requires that the header is already known before
     *  we get called
     */

    PSLASSERT(MTPIPPACKET_INIT_COMMAND_REQUEST == pmtpipPO->mtpstrmPO.dwTypeCur);
    if (MTPIPPACKET_INIT_COMMAND_REQUEST != pmtpipPO->mtpstrmPO.dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Make sure that packet is large enough to hold the key structures- note
     *  that since the size is variable we have to calculate the estimated
     *  packet size rather than just use the size of the structure
     */

    PSLASSERT(GET_PACKET_SIZE(mtpipInitCmdReq) >=
                            pmtpipPO->mtpstrmPO.cbAvailable);
    if ((MTPIPINITCOMMANDREQUEST_MIN_SIZE > pmtpipPO->mtpstrmPO.cbSizeCur) ||
        (GET_PACKET_SIZE(mtpipInitCmdReq) < pmtpipPO->mtpstrmPO.cbAvailable))
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = _MTPIPParserRead(mtpipParser, GET_PACKET_START(mtpipInitCmdReq),
                            pmtpipPO->mtpstrmPO.cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet- since this should have been sent
     *  together by the source we will reject partial packets as invalid
     */

    if (pmtpipPO->mtpstrmPO.cbAvailable != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Copy the name over
     *
     *  NOTE:
     *  YOU MUST CLEAR rgchInitiator before exiting with a failure from
     *  here on.
     *
     *  It would be preferred to validate the protocol version before actually
     *  copying any data to the results.  Given the variable length of
     *  the name field it is more efficient to copy the data while determining
     *  where the name actually ends so we go ahead and do it first.
     */

    cchMax = min(sizeof(mtpipInitCmdReq.varFields.maxFields.szInitiator),
                            (cbData / sizeof(PSLWCHAR)));
    if (PSLNULL != rgchInitiator)
    {
        /*  Copy the string to the destination- but preserve the length of
         *  the string locally so that we can use it to locate the protocol
         *  version
         */

        ps = MTPLoadUTF16String(rgchInitiator, cchMaxInitiator, &cchInitiator,
                mtpipInitCmdReq.varFields.szInitiatorStart, cchMax);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }
    else
    {
        /*  Determine the length of the string so that we can get at the
         *  protocol version
         */

        ps = MTPUTF16StringLen(mtpipInitCmdReq.varFields.szInitiatorStart,
                cchMax, &cchInitiator);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Make sure that the Initiator is NULL terminated
     */

    if (0 != MTPLoadUInt16(
                &(mtpipInitCmdReq.varFields.szInitiatorStart[cchInitiator])))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve and validate the protocol version
     */

    cbData -= (cchInitiator + 1) * sizeof(PSLWCHAR);
    if (sizeof(dwProtVer) > cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }
    dwProtVer = MTPLoadUInt32((PXPSLUINT32)
        (&(mtpipInitCmdReq.varFields.szInitiatorStart) + cchInitiator + 1));
    if (MTPIP_BINARY_PROTOCOL_VERSION != dwProtVer)
    {
        /*  Clear the output if necessary
         */

        if (PSLNULL != rgchInitiator)
        {
            *rgchInitiator = L'\0';
        }
        ps = PSLERROR_UNSUPPORTED_VERSION;
        goto Exit;
    }

    /*  Unpack the request into the return results
     */

    if (PSLNULL != pguidInitiator)
    {
        MTPLoadGUID(pguidInitiator, &(mtpipInitCmdReq.guidInitiator));
    }

    /*  Return the length if necessary
     */

    if (PSLNULL != pchInitiator)
    {
        *pchInitiator = cchInitiator;
    }

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(pmtpipPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPIPParserSendInitCmdACK
 *
 *  Sends an init command response back to the initiator
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser to send command from
 *      PSLUINT32       dwConnID            Connection ID to return
 *      PSL_CONST PSLGUID*
 *                      pguidResponder      Responder GUID
 *      PSLCWSTR        szResponder         Responder Name to return
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserSendInitCmdACK(MTPIPPARSER mtpipParser, PSLUINT32 dwConnID,
                            PSL_CONST PSLGUID* pguidResponder,
                            PSLCWSTR szResponder)
{
    PSLUINT32           cbPacket;
    PSLUINT32           cbSent;
    PSLUINT32           cchResponder;
    MTPIPINITCOMMANDACK mtpipCmdACK;
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Validate Arguments- command response is only valid on the command
     *  socket so no need to specify the socket here
     */

    if (PSLNULL == mtpipParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may send this packet from the command socket
     */

    ps = MTPStreamParserCheckType(mtpipParser,
                            MTPSTREAMPARSERMODE_RESPONDER,
                            MTPSTREAMPARSERTYPE_COMMAND);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Fill out the simple information
     */

    mtpipCmdACK.mtpstrmHeader.bPacketID = MTPIPPACKETID_INIT_COMMAND_ACK;
    mtpipCmdACK.mtpstrmHeader.bTransportID = MTPIP_TRANSPORT_ID;
    MTPStoreUInt16(&(mtpipCmdACK.mtpstrmHeader.wTransportData), 0);
    MTPStoreUInt32(&(mtpipCmdACK.dwConnID), dwConnID);
    if (PSLNULL != pguidResponder)
    {
        MTPStoreGUID(&(mtpipCmdACK.guidResponder), pguidResponder);
    }
    else
    {
        ZeroMemory(&(mtpipCmdACK.guidResponder),
                            sizeof(mtpipCmdACK.guidResponder));
    }

    /*  Copy over the responder name
     */

    ps = MTPStoreUTF16String(mtpipCmdACK.varFields.szResponderStart,
            sizeof(mtpipCmdACK.varFields.maxFields.szResponder), &cchResponder,
            szResponder, PSLSTRING_MAX_CCH);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And add in the version number
     */

    MTPStoreUInt32((PXPSLUINT32)
        (&(mtpipCmdACK.varFields.szResponderStart) + cchResponder + 1),
        MTPIP_BINARY_PROTOCOL_VERSION);

    /*  Set the packet size- remember min size already accounts for the
     *  terminating character and the protocol version
     */

    cbPacket = MTPIPINITCOMMANDACK_MIN_SIZE + (cchResponder * sizeof(PSLWCHAR));
    MTPStoreUInt32(&(mtpipCmdACK.mtpstrmHeader.cbPacket), cbPacket);

    /*  And send it over the network
     */

    ps = _MTPIPParserSend(mtpipParser, &mtpipCmdACK, cbPacket, &cbSent);
    if (PSL_FAILED(ps) || (cbSent != cbPacket))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPIPParserReadInitCmdACK
 *
 *  Retrieves the init command response from the socket.
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser to use
 *      PSLUINT32*      pdwConnID           Connection ID returned
 *      PSLGUID*        pguidResponder      Return buffer for responder GUID
 *      PSLWSTR         rgchResponder       Return buffer for responder name
 *      PSLUINT32*      pchResponder        Return buffer for responder name len
 *      PSLUINT32       cchMaxResponder     Size of the responder name buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserReadInitCmdACK(MTPIPPARSER mtpipParser,
                            PSLUINT32* pdwConnID, PSLGUID* pguidResponder,
                            PSLWSTR rgchResponder, PSLUINT32* pchResponder,
                            PSLUINT32 cchMaxResponder)
{
    PSLUINT32               cbData;
    PSLUINT32               cchResponder;
    PSLUINT32               dwProtVer;
    MTPIPINITCOMMANDACK     mtpipInitCmdACK;
    PSLSTATUS               ps;
    MTPIPPARSEROBJ*         pmtpipPO;

    /*  Clear results for safety
     */

    if (PSLNULL != pdwConnID)
    {
        *pdwConnID = 0;
    }
    if (PSLNULL != pguidResponder)
    {
        PSLZeroMemory(pguidResponder, sizeof(*pguidResponder));
    }
    if (PSLNULL != pchResponder)
    {
        *pchResponder = 0;
    }

    /*  Validate Arguments- command response is only valid on the command
     *  socket so no need to specify socket
     */

    if ((PSLNULL == mtpipParser) || (NULL == pdwConnID) ||
        ((PSLNULL != rgchResponder) && (0 == cchMaxResponder)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only initiators may read this packet from the command socket
     */

    ps = MTPStreamParserCheckType(mtpipParser,
                            MTPSTREAMPARSERMODE_INITIATOR,
                            MTPSTREAMPARSERTYPE_COMMAND);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Standard operation requires that the header is already known before
     *  we get called
     */

    PSLASSERT(MTPIPPACKET_INIT_COMMAND_ACK == pmtpipPO->mtpstrmPO.dwTypeCur);
    if (MTPIPPACKET_INIT_COMMAND_ACK != pmtpipPO->mtpstrmPO.dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Make sure that packet is large enough to hold the key structures- note
     *  that since the size is variable we have to calculate the estimated
     *  packet size rather than just use the size of the structure
     */

    PSLASSERT(GET_PACKET_SIZE(mtpipInitCmdACK) >=
                            pmtpipPO->mtpstrmPO.cbAvailable);
    if ((MTPIPINITCOMMANDACK_MIN_SIZE > pmtpipPO->mtpstrmPO.cbSizeCur) ||
        (GET_PACKET_SIZE(mtpipInitCmdACK) < pmtpipPO->mtpstrmPO.cbAvailable))
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = _MTPIPParserRead(pmtpipPO, GET_PACKET_START(mtpipInitCmdACK),
                            pmtpipPO->mtpstrmPO.cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet- since this should have been sent
     *  together by the source we will reject partial packets as invalid
     */

    if (pmtpipPO->mtpstrmPO.cbAvailable != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Copy the name over
     *
     *  NOTE:
     *  YOU MUST CLEAR rgchResponder before exiting with a failure from
     *  here on.
     *
     *  It would be preferred to validate the protocol version before actually
     *  copying any data to the results.  Given the variable length of
     *  the name field it is more efficient to copy the data while determining
     *  where the name actually ends so we go ahead and do it first.
     */

    if (PSLNULL != rgchResponder)
    {
        /*  Copy the string to the destination- but preserve the length of
         *  the string locally so that we can use it to locate the protocol
         *  version
         */

        ps = MTPLoadUTF16String(rgchResponder, cchMaxResponder, &cchResponder,
                mtpipInitCmdACK.varFields.szResponderStart,
                sizeof(mtpipInitCmdACK.varFields.maxFields.szResponder));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }
    else
    {
        /*  Determine the length of the string so that we can get at the
         *  protocol version
         */

        ps = MTPUTF16StringLen(mtpipInitCmdACK.varFields.szResponderStart,
                PSLARRAYSIZE(mtpipInitCmdACK.varFields.maxFields.szResponder),
                &cchResponder);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Retrieve and validate the protocol version
     */

    dwProtVer = *((PSL_UNALIGNED PSLUINT32*)
        (&(mtpipInitCmdACK.varFields.szResponderStart) + cchResponder + 1));
    if (MTPIP_BINARY_PROTOCOL_VERSION != dwProtVer)
    {
        /*  Clear the output if necessary
         */

        if (PSLNULL != rgchResponder)
        {
            *rgchResponder = L'\0';
        }
        ps = PSLERROR_UNSUPPORTED_VERSION;
        goto Exit;
    }

    /*  Unpack the protocol version
     */

    *pdwConnID = MTPLoadUInt32(&(mtpipInitCmdACK.dwConnID));

    /*  Unpack the Initiator GUID into the return results
     */

    if (PSLNULL != pguidResponder)
    {
        MTPLoadGUID(pguidResponder, &(mtpipInitCmdACK.guidResponder));
    }

    /*  Return the name length if necessary
     */

    if (PSLNULL != pchResponder)
    {
        *pchResponder = cchResponder;
    }

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(pmtpipPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPIPParserSendInitEventRequest
 *
 *  Sends an init event request from the initiator to the responder.
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser to use
 *      PSLUINT32       dwConnID            Connection ID to send
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserSendInitEventRequest(MTPIPPARSER mtpipParser,
                            PSLUINT32 dwConnID)
{
    PSLUINT32               cbPacket;
    PSLUINT32               cbSent;
    MTPIPINITEVENTREQUEST   mtpipInitEventReq;
    PSLSTATUS               ps;
    MTPIPPARSEROBJ*         pmtpipPO;

    /*  Validate Arguments- event request is only valid on the event socket
    *   so no need to specify socket
     */

    if (PSLNULL == mtpipParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only initiators may sned this packet from the event socket
     */

    ps = MTPStreamParserCheckType(mtpipParser,
                            MTPSTREAMPARSERMODE_INITIATOR,
                            MTPSTREAMPARSERTYPE_EVENT);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Initialize the packet
     */

    cbPacket = sizeof(mtpipInitEventReq);
    MTPStoreUInt32(&(mtpipInitEventReq.mtpstrmHeader.cbPacket),
                            cbPacket);
    mtpipInitEventReq.mtpstrmHeader.bPacketID =
                            MTPIPPACKETID_INIT_EVENT_REQUEST;
    mtpipInitEventReq.mtpstrmHeader.bTransportID = MTPIP_TRANSPORT_ID;
    MTPStoreUInt16(&(mtpipInitEventReq.mtpstrmHeader.wTransportData), 0);
    MTPStoreUInt32(&(mtpipInitEventReq.dwConnID), dwConnID);

    /*  And sent it
     */

    ps = _MTPIPParserSend(mtpipParser, &mtpipInitEventReq,
                            cbPacket, &cbSent);
    if (PSL_FAILED(ps) || (cbSent != cbPacket))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPIPParserReadInitEventRequest
 *
 *  Reads an init event request using the specified parser from the given
 *  socket.
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser to use
 *      PSLUINT32*      pdwConnID           Connection ID specified
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserReadInitEventRequest(MTPIPPARSER mtpipParser,
                            PSLUINT32* pdwConnID)
{
    PSLUINT32               cbData;
    MTPIPINITEVENTREQUEST   mtpipInitEventReq;
    PSLSTATUS               ps;
    MTPIPPARSEROBJ*         pmtpipPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwConnID)
    {
        *pdwConnID = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpipParser) || (PSLNULL == pdwConnID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may read this packet from the event socket
     */

    ps = MTPStreamParserCheckType(mtpipParser,
                            MTPSTREAMPARSERMODE_RESPONDER,
                            MTPSTREAMPARSERTYPE_EVENT);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Standard operation model requires that the header has already been
     *  read.
     */

    PSLASSERT(MTPIPPACKET_INIT_EVENT_REQUEST == pmtpipPO->mtpstrmPO.dwTypeCur);
    if (MTPIPPACKET_INIT_EVENT_REQUEST != pmtpipPO->mtpstrmPO.dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Make sure that packet is large enough to hold the key structures- note
     *  that since the size is variable we have to calculate the estimated
     *  packet size rather than just use the size of the structure
     */

    PSLASSERT(GET_PACKET_SIZE(mtpipInitEventReq) ==
                            pmtpipPO->mtpstrmPO.cbAvailable);
    if (GET_PACKET_SIZE(mtpipInitEventReq) != pmtpipPO->mtpstrmPO.cbAvailable)
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = _MTPIPParserRead(mtpipParser, GET_PACKET_START(mtpipInitEventReq),
                            pmtpipPO->mtpstrmPO.cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet- since this should have been sent
     *  together by the source we will reject partial packets as invalid
     */

    if (GET_PACKET_SIZE(mtpipInitEventReq) != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Extract the connection ID out of the entry
     */

    *pdwConnID = MTPLoadUInt32(&(mtpipInitEventReq.dwConnID));

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(pmtpipPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPIPParserSendInitEventACK
 *
 *  Sends an acknowledgement that the event socket is connected and ready
 *  to go.
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser to use
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserSendInitEventACK(MTPIPPARSER mtpipParser)
{
    PSLUINT32           cbSent;
    MTPIPINITEVENTACK   mtpipEventACK;
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Validate Arguments- event response is only valid on the event
     *  socket so no need to specify the socket here
     */

    if (PSLNULL == mtpipParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may send this packet from the event socket
     */

    ps = MTPStreamParserCheckType(mtpipParser,
                            MTPSTREAMPARSERMODE_RESPONDER,
                            MTPSTREAMPARSERTYPE_EVENT);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Fill out the header
     */

    mtpipEventACK.mtpstrmHeader.bPacketID = MTPIPPACKETID_INIT_EVENT_ACK;
    mtpipEventACK.mtpstrmHeader.bTransportID = MTPIP_TRANSPORT_ID;
    MTPStoreUInt16(&(mtpipEventACK.mtpstrmHeader.wTransportData), 0);
    MTPStoreUInt32(&(mtpipEventACK.mtpstrmHeader.cbPacket),
                            sizeof(mtpipEventACK));

    /*  And send it over the network
     */

    ps = _MTPIPParserSend(mtpipParser, &mtpipEventACK,
                            sizeof(mtpipEventACK), &cbSent);
    if (PSL_FAILED(ps) || (cbSent != sizeof(mtpipEventACK)))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPIPParserReadInitEventACK
 *
 *  Reads the init event acknowledgement from the responder.
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser to use
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserReadInitEventACK(MTPIPPARSER mtpipParser)
{
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Validate Arguments- command request is only valid on the event
     *  socket
     */

    if (PSLNULL == mtpipParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only initiators may read this packet from the event socket
     */

    ps = MTPStreamParserCheckType(mtpipParser,
                            MTPSTREAMPARSERMODE_INITIATOR,
                            MTPSTREAMPARSERTYPE_EVENT);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Standard operation model requires that the header has already been
     *  read.
     */

    ASSERT(MTPIPPACKET_INIT_EVENT_ACK == pmtpipPO->mtpstrmPO.dwTypeCur);
    if (MTPIPPACKET_INIT_EVENT_ACK != pmtpipPO->mtpstrmPO.dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Make sure the remaining data is enough to hold the rest of the
     *  packet
     */

    PSLASSERT(GET_PACKET_SIZE(MTPIPINITEVENTACK) ==
                            pmtpipPO->mtpstrmPO.cbAvailable);
    if (GET_PACKET_SIZE(MTPIPINITEVENTACK) != pmtpipPO->mtpstrmPO.cbAvailable)
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  We have the entire packet at this point- reset the parser for the
     *  next packet
     */

    PSLASSERT(0 == pmtpipPO->mtpstrmPO.cbAvailable);
    ps = MTPStreamParserReset(pmtpipPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPIPParaserSendInitFail
 *
 *  Sends a initialization failed notification to the destination socket.
 *
 *  Arguments:
 *      MTPIPPARSPER    mtpipParser         Parser to use
 *      PSLUINT32       dwReason            Reason for failure
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserSendInitFail(MTPIPPARSER mtpipParser, PSLUINT32 dwReason)
{
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Validate Arguments- failure can be sent on either the command or
     *  event socket.
     */

    if (PSLNULL == mtpipParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may send this packet- note there is no requirement
     *  that the socket be correctly typed before this packet may be sent
     */

    ps = MTPStreamParserCheckType(mtpipParser,
                                MTPSTREAMPARSERMODE_RESPONDER,
                                MTPSTREAMPARSERTYPE_UNKNOWN);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Use the unbound function to do the real work
     */

    ps = MTPIPSendInitFailed(pmtpipPO->sock, dwReason);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPIPParserReadInitFail
 *
 *  Reads an initialization failure message from the specified socket.
 *
 *  Arguments:
 *      MTPIPPARSER     mtpipParser         Parser to use
 *      PSLUINT32       dwSocketSrc         Socket to read from
 *      PSLUINT32*      pdwReason           Reason for failure
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPParserReadInitFail(MTPIPPARSER mtpipParser, PSLUINT32* pdwReason)
{
    PSLUINT32           cbData;
    MTPIPINITFAIL       mtpipInitFail;
    PSLSTATUS           ps;
    MTPIPPARSEROBJ*     pmtpipPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwReason)
    {
        *pdwReason = MTPIP_FAIL_UNSPECIFIED;
    }

    /*  Validate Arguments- failure can be sent on either the command or
     *  event socket.
     */

    if ((PSLNULL == mtpipParser) || (PSLNULL == pdwReason))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpipPO = (MTPIPPARSEROBJ*)mtpipParser;
    PSLASSERT(MTPIPPARSER_COOKIE == pmtpipPO->dwIPCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpipPO->sock);
    if (INVALID_SOCKET == pmtpipPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only initiators may read this packet on a properly typed connection
     */

    ps = MTPStreamParserCheckType(mtpipParser,
                            MTPSTREAMPARSERMODE_INITIATOR,
                            MTPSTREAMPARSERTYPE_ANY);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Validate that we have an correct sized packet
     */

    PSLASSERT(GET_PACKET_SIZE(mtpipInitFail) == pmtpipPO->mtpstrmPO.cbAvailable);
    if (GET_PACKET_SIZE(mtpipInitFail) != pmtpipPO->mtpstrmPO.cbAvailable)
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = _MTPIPParserRead(mtpipParser, GET_PACKET_START(mtpipInitFail),
                            pmtpipPO->mtpstrmPO.cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet- since this should have been sent
     *  together by the source we will reject partial packets as invalid
     */

    if (GET_PACKET_SIZE(mtpipInitFail) != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Extract the connection ID out of the entry
     */

    *pdwReason = MTPLoadUInt32(&(mtpipInitFail.dwReason));

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(pmtpipPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPIPSendInitFailed
 *
 *  API for sending an init failed message directly to the socket rather than
 *  using the parser.
 *
 *  Arguments:
 *      SOCKET          sock                Socket to send packet on
 *      PSLUINT32       dwReason            Reason for failure
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPIPSendInitFailed(SOCKET sock, PSLUINT32 dwReason)
{
    PSLINT32            cbSent;
    MTPIPINITFAIL       mtpipEventFail;
    PSLSTATUS           ps;

    /*  Validate arguments
     */

    if (INVALID_SOCKET == sock)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Fill out the header
     */

    mtpipEventFail.mtpstrmHeader.bPacketID = MTPIPPACKETID_INIT_FAIL;
    mtpipEventFail.mtpstrmHeader.bTransportID = MTPIP_TRANSPORT_ID;
    MTPStoreUInt16(&(mtpipEventFail.mtpstrmHeader.wTransportData), 0);
    MTPStoreUInt32(&(mtpipEventFail.mtpstrmHeader.cbPacket),
                            sizeof(mtpipEventFail));

    /*  Add in the result code
     */

    MTPStoreUInt32(&(mtpipEventFail.dwReason), dwReason);

    /*  And send it over the network
     */

    cbSent = send(sock, (char*)&mtpipEventFail, sizeof(mtpipEventFail), 0);
    if (SOCKET_ERROR == cbSent)
    {
        ps = PSLSocketErrorToPSLStatus(sock, cbSent);
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


