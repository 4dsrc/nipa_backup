/*
 *  MTPBTParser.h
 *
 *  Contains declaration of the different MTP/BT parsers and constants.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPBTPARSER_H_
#define _MTPBTPARSER_H_


/*  MTP/BT Packet Types
 *
 *  Type enumeration for MTP/BT packet types.  Note that these types are
 *  in addition to the existing MTP Stream Parser types.
 */

enum
{
    MTPBTPACKET_INIT_COMMAND_REQUEST = MTPSTREAMPACKET_SUBCLASS_BEGIN,
    MTPBTPACKET_INIT_EVENT_REQUEST,
    MTPBTPACKET_INIT_LINK_FAIL,
    MTPBTPACKET_INIT_LINK_ACK,
    MTPBTPACKET_TRANSPORT_ERROR,
    MTPBTPACKET_TRANSPORT_READY,
    MTPBTPACKET_REQUEST_LINK_DESTROY
};


/*  MTP/BT Initialization Failure Codes
 */

enum
{
    MTPBT_FAIL_REJECTED_INITIATOR =                 0x00000001,
    MTPBT_FAIL_BUSY =                               0x00000002,
    MTPBT_FAIL_UNSPECIFIED =                        0x00000003
};


/*  MTP/BT Transport Failure Codes
 */

enum
{
    MTPBT_TRANSPORT_FAIL_INVALID_ERROR_DETECTION =  0x00000000,
    MTPBT_TRANSPORT_FAIL_MISSING_PACKET =           0x00000001,
    MTPBT_TRANSPORT_FAIL_INVALID_DATA =             0x00000002
};


/*  MTPBTPARSER
 *
 *  The MTP/BT Parser inherits a large portion of it's implementation from
 *  the MTP Stream Parser.  The functionality described here is specific
 *  to MTP/BT solutions.
 */

typedef PSLHANDLE MTPBTPARSER;

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserCreate(
                            PSLUINT32 dwMode,
                            SOCKET sock,
                            PSLUINT32 dwType,
                            MTPBTPARSER* pmtpbtParser);

/*  MTP/BT Parser Information
 *
 *  Use ths object when calling MTPStreamParserGetParserInfo with an
 *  MTPBTPARSER object.
 */

typedef struct _MTPBTPARSERINFO
{
    MTPSTREAMPARSERINFO     mtpstrmInfo;
    PSLUINT32               cbAddrLocal;
    SOCKADDR_STORAGE        saAddrLocal;
    PSLUINT32               cbAddrDest;
    SOCKADDR_STORAGE        saAddrDest;
    PSLUINT32               cbMaxSend;
    PSLUINT32               cbMaxReceive;
} MTPBTPARSERINFO;


/*  MTP/BT Parser Helpers
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserSocketMsgSelect(
                    MTPBTPARSER mtpstrmParser,
                    PSLMSGQUEUE mqDest,
                    PSLMSGPOOL mpDest,
                    PSLPARAM pvParam,
                    PSLUINT32 dwSelectFlags);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserSocketMsgReset(
                    MTPBTPARSER mtpstrmParser,
                    PSLBOOL fResetMask,
                    PSLUINT32 dwData);


/*  Serialization and De-Serialization Routines
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserReadInitCmdRequest(
                    MTPBTPARSER mtpbtParser,
                    PSLGUID* pguidLinkID,
                    PSLUINT16* pwPSM);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserSendInitEventRequest(
                    MTPBTPARSER mtpbtParser,
                    PSL_CONST PSLGUID* pguidLinkID,
                    PSLUINT16 wPSM);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserSendInitLinkACK(
                    MTPBTPARSER mtpbtParser,
                    PSL_CONST PSLGUID* pguidLinkID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserReadInitLinkACK(
                    MTPBTPARSER mtpbtParser,
                    PSLGUID* pguidLinkID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserSendInitFail(
                    MTPBTPARSER mtpbtParser,
                    PSLUINT32 dwReason);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserReadInitFail(
                    MTPBTPARSER mtpbtParser,
                    PSLUINT32* pdwReason);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserSendTransportError(
                    MTPBTPARSER mtpbtParser,
                    PSLUINT32 dwTransactionID,
                    PSL_CONST PSLGUID* pguidLinkID,
                    PSLUINT32 dwReason);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserReadTransportError(
                    MTPBTPARSER mtpbtParser,
                    PSLUINT32* pdwTransactionID,
                    PSLGUID* pguidLinkID,
                    PSLUINT32* pdwReason);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserSendTransportReady(
                    MTPBTPARSER mtpbtParser,
                    PSL_CONST PSLGUID* pguidLinkID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserReadTransportReady(
                    MTPBTPARSER mtpbtParser,
                    MTPSTREAMBUFFERMGR mtpstrmbm,
                    PSL_CONST PSLGUID* pguidLinkID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTParserSendRequestLinkDestroy(
                    MTPBTPARSER mtpbtParser,
                    PSL_CONST PSLGUID* pguidLinkID);

/*  Safe Helpers
 */

#define SAFE_MTPBTPARSERDESTROY(_p)     SAFE_MTPSTREAMPARSERDESTROY(_p)

#endif  /* _MTPBTPARSER_H_ */

