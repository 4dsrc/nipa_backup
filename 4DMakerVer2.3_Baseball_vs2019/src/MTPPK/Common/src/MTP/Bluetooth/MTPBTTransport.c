/*
 *  MTPBTTransport.c
 *
 *  Contains definition of the APIs used to bind MTP Bluetooth sockets to
 *  a particular Router or Proxy Transport
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPBTTransportPrecomp.h"

/*  Local Defines
 */

#define BTCONNECTINFO_COOKIE            'mbtA'

#define CONNECTMGR_MSG_POOL_SIZE        16

enum
{
    CONNECTMGRSTATE_UNKNOWN = 0,
    CONNECTMGRSTATE_INITIALIZING,
    CONNECTMGRSTATE_RUNNING,
    CONNECTMGRSTATE_SHUTDOWN,
    CONNECTMGRSTATE_SHUTTING_DOWN
};

enum
{
    CONNECTMGRMSG_CONNECT_INITIATOR =   MAKE_PSL_NORMAL_MSGID(PSL_USER, 1),
    CONNECTMGRMSG_SHUTDOWN =            MAKE_PSL_NORMAL_MSGID(PSL_USER, 2),
};

enum
{
    BTCONNECTINFO_FIND_EVENTPARSER = 0,
    BTCONNECTINFO_FIND_LINKID
};

#define SAFE_BTCONNECTINFODESTROY(_p) \
if (PSLNULL != (_p)) \
{ \
    _BTConnectInfoDestroy(_p); \
}

/*  Local Types
 */

typedef struct _BTCONNECTINFO
{
#ifdef PSL_ASSERTS
    PSLUINT32                   dwCookie;
#endif  /* PSL_ASSERTS */
    SOCKADDR_STORAGE            saClient;
    PSLUINT32                   cbAddr;
    PSLUINT16                   wPSMResponder;
    PSLPARAM                    aParamTransport;
    PSLGUID                     guidLinkID;
    MTPBTPARSER                 mtpbtEvent;
    struct _BTCONNECTINFO*      pciNext;
} BTCONNECTINFO;

/*  Local Functions
 */

static PSLSTATUS _BTConnectInfoDestroy(BTCONNECTINFO* pci);

static PSLSTATUS _BTConnectInfoAdd(BTCONNECTINFO* pci);

static PSLSTATUS _BTConnectInfoFind(PSLUINT32 dwFindType,
                    PSLPARAM aParam,
                    PSLBOOL fRemove,
                    BTCONNECTINFO** ppci);

static PSLSTATUS _BTConnectInfoRemoveAll();

static PSLSTATUS _CheckConnectMgrThreadState(PSLBOOL fMustBeRunning);

static PSLSTATUS _ConnectInitiator(PSL_CONST struct sockaddr* psaInitiator,
                    PSLUINT32 cbAddr,
                    PSLMSGQUEUE mqConnect,
                    PSLMSGPOOL mpConnect,
                    MTPBTPARSER* pmtpbtEvent);

static PSLUINT32 PSL_API _BTConnectMgrThread(PSLVOID* pvParam);

/*  Local Variables
 */

static const PSLWCHAR       _RgchConnectMgrInitSignal[] =
    L"Bind Transport Init Signal";

static PSLBOOL              _FMTPBTSocketInit = PSLFALSE;

static PSLUINT32            _DwConnectMgrState = CONNECTMGRSTATE_UNKNOWN;
static PSLHANDLE            _HThreadConnectMgr = PSLNULL;
static PSLMSGQUEUE          _MqConnectMgr = PSLNULL;
static PSLMSGPOOL           _MpConnectMgr = PSLNULL;

static PSLHANDLE            _HMutexConnectList = PSLNULL;
static BTCONNECTINFO*       _PciConnectList = PSLNULL;


/*
 *  _BTConnectInfoDestroy
 *
 *  Cleans a BTCONNECTINFO structure
 *
 *  Arguments:
 *      BTCONNECTINFO*  pci                 BTCONNECTINFO structure to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _BTConnectInfoDestroy(BTCONNECTINFO* pci)
{
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if (PSLNULL == pci)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }
    PSLASSERT(BTCONNECTINFO_COOKIE == pci->dwCookie);

    /*  Release internal objects
     */

    SAFE_MTPBTPARSERDESTROY(pci->mtpbtEvent);

    /*  Report the transport as closed
     */

    CHECK_MTPTRANSPORTNOTIFY(MTPTRANSPORTEVENT_CLOSED, pci->aParamTransport);

    /*  Free the memory associated with the object
     */

    SAFE_PSLMEMFREE(pci);

    /*  Reports success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _BTConnectInfoAdd
 *
 *  Adds a new element to the Bluetooth connection list dealing with thread
 *  safety issues.
 *
 *  Arguments:
 *      BTCONNECTINFO*  pci                 Item to add
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _BTConnectInfoAdd(BTCONNECTINFO* pci)
{
    PSLHANDLE       hMutex = PSLNULL;
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if (PSLNULL == pci)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Acquire the connection list mutex
     */

    if (PSLNULL == _HMutexConnectList)
    {
        ps = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }
    ps = PSLMutexAcquire(_HMutexConnectList, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutex = _HMutexConnectList;

    /*  And add the new item
     */

    pci->pciNext = _PciConnectList;
    _PciConnectList = pci;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutex);
    return ps;
}


/*
 *  _BTConnectInfoFind
 *
 *  Finds an instance of the connection info according to the key value
 *  specifed and returns it.  The item is optionally removed from the list.
 *
 *  Arguments:
 *      PSLUINT32       dwFindType          Key type to look for
 *      PSLPARAM        aKey                Key value to search for
 *      PSLBOOL         fRemove             PSLTRUE to remove item from list
 *      BTCONNECTINFO** ppci                Return location for information
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if found, PSLSUCCES_NOT_FOUND if not
 *
 */

PSLSTATUS _BTConnectInfoFind(PSLUINT32 dwFindType, PSLPARAM aKey,
                    PSLBOOL fRemove, BTCONNECTINFO** ppci)
{
    PSLHANDLE       hMutex = PSLNULL;
    PSLSTATUS       ps;
    BTCONNECTINFO*  pciCur;
    BTCONNECTINFO*  pciLast;

    /*  Clear results for safety
     */

    if (PSLNULL != ppci)
    {
        *ppci = PSLNULL;
    }

    /*  Validate arguments
     */

    if (PSLNULL == ppci)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Acquire the connection list mutex
     */

    if (PSLNULL == _HMutexConnectList)
    {
        ps = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }
    ps = PSLMutexAcquire(_HMutexConnectList, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutex = _HMutexConnectList;

    /*  Determine what sort of find we are doing
     */

    switch (dwFindType)
    {
    case BTCONNECTINFO_FIND_EVENTPARSER:
        /*  Walk through the list looking for a match on the event parser
         */

        for (pciCur = _PciConnectList, pciLast = PSLNULL;
                (PSLNULL != pciCur) && ((MTPBTPARSER)aKey != pciCur->mtpbtEvent);
                pciLast = pciCur, pciCur = pciCur->pciNext)
        {
        }
        break;

    case BTCONNECTINFO_FIND_LINKID:
        /*  Walk through the list looking for a match on the Link ID
         */

        for (pciCur = _PciConnectList, pciLast = PSLNULL;
                (PSLNULL != pciCur) &&
                    (0 != PSLCompareMemory((PSLVOID*)aKey, &(pciCur->guidLinkID),
                            sizeof(PSLGUID)));
                pciLast = pciCur, pciCur = pciCur->pciNext)
        {
        }
        break;

    default:
        /*  Unknown comparison type
         */

        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If a match was not found report it
     */

    if (PSLNULL == pciCur)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Remove the item if necessary
     */

    if (fRemove)
    {
        if (PSLNULL != pciLast)
        {
            pciLast->pciNext = pciCur->pciNext;
        }
        else
        {
            _PciConnectList = pciCur->pciNext;
        }
    }

    /*  And return the item
     */

    *ppci = pciCur;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutex);
    return ps;
}


/*
 *  _BTConnectInfoRemoveAll
 *
 *  Remove all connection information
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _BTConnectInfoRemoveAll()
{
    PSLHANDLE       hMutex = PSLNULL;
    PSLSTATUS       ps;
    BTCONNECTINFO*  pciCur;

    /*  Acquire the connection list mutex
     */

    if (PSLNULL == _HMutexConnectList)
    {
        ps = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }
    ps = PSLMutexAcquire(_HMutexConnectList, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutex = _HMutexConnectList;

    /*  Walk through the list and remove each item
     */

    while (PSLNULL != _PciConnectList)
    {
        pciCur = _PciConnectList;
        _PciConnectList = pciCur->pciNext;

        /*  And destroy it
         */

        _BTConnectInfoDestroy(pciCur);
    }
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutex);
    return ps;
}


/*
 *  _CheckConnectMgrThreadState
 *
 *  Checks the status of the socket thread to make sure that it is up and
 *  running.
 *
 *  Arguments:
 *      PSLBOOL         fMustBeRunning      PSLTRUE if the thread must be
 *                                            running
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if thread is running, an error if
 *                        the thread must be running and is not,
 *                        PSLSUCCESS_NOT_AVAILABLE if the thread does not
 *                        have to be running and is not
 *
 */

PSLSTATUS _CheckConnectMgrThreadState(PSLBOOL fMustBeRunning)
{
    PSLUINT32       dwState;
    PSLHANDLE       hSignalInit = PSLFALSE;
    PSLSTATUS       ps;

    /*  Loop here to determine what state the socket thread is in- unless
     *  the thread is active we want to loop here until we can get it into
     *  an active state.
     */

    do
    {
        /*  Determine the current state of the thread
         */

        dwState = PSLAtomicCompareExchange(&_DwConnectMgrState,
                            CONNECTMGRSTATE_RUNNING,
                            CONNECTMGRSTATE_RUNNING);
        switch (dwState)
        {
        case CONNECTMGRSTATE_RUNNING:
            /*  Thread is active
             */

            ps = PSLSUCCESS;
            break;

        case CONNECTMGRSTATE_UNKNOWN:
            /*  The thread has never been initialized
             */

            ps = fMustBeRunning ?
                            PSLERROR_NOT_INITIALIZED : PSLSUCCESS_NOT_AVAILABLE;
            break;

        case CONNECTMGRSTATE_SHUTTING_DOWN:
        case CONNECTMGRSTATE_SHUTDOWN:
            /*  The thread is either not running or shortly will not be-
             *  report the correct error
             */

            ps = fMustBeRunning ?
                            PSLERROR_NOT_AVAILABLE : PSLSUCCESS_NOT_AVAILABLE;
            break;

        case CONNECTMGRSTATE_INITIALIZING:
            /*  Another thread is initializing the router- give it a chance
             *  to complete the work
             */

            PSLASSERT(PSLNULL == hSignalInit);
            ps = PSLSignalOpen(PSLFALSE, PSLFALSE, _RgchConnectMgrInitSignal,
                            &hSignalInit);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            ps = PSLSignalWait(hSignalInit, PSLTHREAD_INFINITE);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        default:
            /*  Unknown init router state
             */

            PSLASSERT(FALSE);
            ps = PSLERROR_UNEXPECTED;
            break;
        }
    }
    while (CONNECTMGRSTATE_INITIALIZING == dwState);

Exit:
    SAFE_PSLSIGNALCLOSE(hSignalInit);
    return ps;
}


/*
 *  _ConnectInitiator
 *
 *  Initiates a connection with an Initiator via the ConnectMgr thread.
 *
 *  Arguments:
 *      struct sockaddr*
 *                      psaInitiator    Initiator address
 *      PSLUINT32       cbAddr          Size of the address
 *      PSLMSGQUEUE     mqEvents        Message queue for events
 *      PSLMSGPOOL      mpEvents        Message pool for events
 *      MTPBTPARSER*    pmtpbtEvent     Event connection parser
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _ConnectInitiator(PSL_CONST struct sockaddr* psaInitiator,
                    PSLUINT32 cbAddr, PSLMSGQUEUE mqEvent, PSLMSGPOOL mpEvent,
                    MTPBTPARSER* pmtpbtEvent)
{
    mtpbtsockaddr_l2cap addrLocal = {
                                        sizeof(mtpbtsockaddr_l2cap),
                                        MTPBT_AF_BLUETOOTH,
                                        { 0 }
                                    };
    PSLINT32            errSock;
    MTPBTPARSER         mtpbtEvent = PSLNULL;
    PSLSTATUS           ps;
    SOCKET              sockEvent = INVALID_SOCKET;
    SOCKET              sockConnect;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpbtEvent)
    {
        *pmtpbtEvent = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == psaInitiator) || (sizeof(mtpbtsockaddr_l2cap) != cbAddr) ||
        (MTPBT_AF_BLUETOOTH_L2CAP != psaInitiator->sa_family) ||
        (PSLNULL == mqEvent) || (PSLNULL == mpEvent) ||
        (PSLNULL == pmtpbtEvent))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create a new Bluetooth socket for the event connection and bind it
     *  to the local address
     */

    sockEvent = MTPBTSocketCreate(MTPBT_AF_BLUETOOTH, MTPBT_SOCK_SEQPACKET,
                            MTPBT_PROTO_L2CAP);
    if (INVALID_SOCKET == sockEvent)
    {
        ps = MTPBTSocketErrorToPSLStatus(sockEvent, sockEvent);
        goto Exit;
    }

    errSock = MTPBTSocketBind(sockEvent, (struct sockaddr*)&addrLocal,
                            sizeof(addrLocal));
    if (SOCKET_ERROR  == errSock)
    {
        ps = MTPBTSocketErrorToPSLStatus(sockEvent, errSock);
        goto Exit;
    }

    /*  Bind the socket to a MTP/BT event parser
     */

    ps = MTPBTParserCreate(MTPSTREAMPARSERMODE_RESPONDER, sockEvent,
                            MTPSTREAMPARSERTYPE_EVENT, &mtpbtEvent);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    sockConnect = sockEvent;
    sockEvent = INVALID_SOCKET;

    /*  Place the socket in asynchronous mode
     */

    ps = MTPBTParserSocketMsgSelect(mtpbtEvent, mqEvent,
                            mpEvent, (PSLPARAM)mtpbtEvent,
                            (FD_READ | FD_CLOSE | FD_CONNECT));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And issue the connection request to the remote host
     */

    errSock = MTPBTSocketConnect(sockConnect, psaInitiator, cbAddr);
    if (SOCKET_ERROR == errSock)
    {
        ps = MTPBTSocketErrorToPSLStatus(sockEvent, errSock);
        goto Exit;
    }

    /*  Return the result
     */

    *pmtpbtEvent = mtpbtEvent;
    mtpbtEvent = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPBTSOCKETCLOSE(sockEvent);
    SAFE_MTPBTPARSERDESTROY(mtpbtEvent);
    return ps;
}


/*
 *  _BTConnectMgrThread
 *
 *  Main thread used to bind a socket to a router to handle device initiated
 *  connections.  This thread creates and sends the appropriate initialization
 *  sequence to the remote device and then waits for a connection to be
 *  received back from the host.
 *
 *  Arguments:
 *      PSLVOID*        pvParam             Ignored
 *
 *  Returns:
 *      PSLUINT32       PSLSTATUS code
 *
 */

PSLUINT32 PSL_API _BTConnectMgrThread(PSLVOID* pvParam)
{
    PSLUINT32       dwState;
    PSLUINT32       dwReason;
    PSLUINT32       dwPacketType;
    PSLBOOL         fLoop;
    PSLBOOL         fShuttingDown = PSLFALSE;
    PSLHANDLE       hSignalInit = PSLNULL;
    PSLSTATUS       ps;
    PSLMSG*         pMsg = PSLNULL;
    BTCONNECTINFO*  pciCur;

    /*  Report the binding router as running
     */

    dwState = PSLAtomicCompareExchange(&_DwConnectMgrState,
                            CONNECTMGRSTATE_RUNNING,
                            CONNECTMGRSTATE_INITIALIZING);
    PSLASSERT(CONNECTMGRSTATE_INITIALIZING == dwState);
    switch (dwState)
    {
    case CONNECTMGRSTATE_INITIALIZING:
        break;

    case CONNECTMGRSTATE_SHUTTING_DOWN:
        /*  We have alread been asked to shutdown- indicate that we
         *  are transitioning into shutdown mode so that we correctly
         *  ignore pending messages.
         *
         */

        fShuttingDown = PSLTRUE;
        break;

    case CONNECTMGRSTATE_RUNNING:
    case CONNECTMGRSTATE_UNKNOWN:
    default:
        /*  Should not get here in these states
         */

        PSLASSERT(FALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Signal any waiting threads that we have completed initialization
     */

    ps = PSLSignalOpen(PSLFALSE, PSLTRUE, _RgchConnectMgrInitSignal,
                            &hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    ps = PSLSignalSet(hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Start handling messages
     */

    fLoop = PSLTRUE;
    while (fLoop && !fShuttingDown)
    {
        /*  Release any message we are handling
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  Retrieve the next message
         */

        ps = PSLMsgGet(_MqConnectMgr, &pMsg);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            /*  Skip the message- and try again.
             */

            continue;
        }

        /*  Handle the message
         */

        switch (pMsg->msgID)
        {
        case CONNECTMGRMSG_CONNECT_INITIATOR:
            /*  New socket to track- extract it from the message
             */

            pciCur = (BTCONNECTINFO*)pMsg->aParam0;
            PSLASSERT(BTCONNECTINFO_COOKIE == pciCur->dwCookie);

            /*  Create a new LinkID for this connection
             */

            ps = PSLGUIDGenerate(&(pciCur->guidLinkID));
            if (PSL_SUCCEEDED(ps))
            {
                /*  Connect to the initiator on the address provided
                 */

                ps = _ConnectInitiator((struct sockaddr*)&(pciCur->saClient),
                            pciCur->cbAddr, _MqConnectMgr, _MpConnectMgr,
                            &(pciCur->mtpbtEvent));
                if (PSL_SUCCEEDED(ps))
                {
                    /*  Add this connection to the list of pending connections
                     */

                    ps = _BTConnectInfoAdd(pciCur);
                    if (PSL_FAILED(ps))
                    {
                        SAFE_BTCONNECTINFODESTROY(pciCur);
                    }
                }
            }

            /*  Return the result of the operation to the waiting caller
             */

            *((PSLSTATUS*)((PSLUINT32)pMsg->alParam)) = ps;
            break;

        case CONNECTMGRMSG_SHUTDOWN:
            /*  We are shutting down- stop processing messages
             */

            fLoop = PSLFALSE;
            break;

        case PSLSOCKETMSG_CONNECT:
        case PSLSOCKETMSG_DATA_AVAILABLE:
        case PSLSOCKETMSG_CLOSE:
            /*  Locate the socket information from the list
             */

            ps = _BTConnectInfoFind(BTCONNECTINFO_FIND_EVENTPARSER,
                            pMsg->aParam0, PSLFALSE, &pciCur);

            /*  We may receive notifications for sockets that we have
             *  already transferred- just ignore them
             */

            if (PSLSUCCESS != ps)
            {
                continue;
            }

            /*  Handle the notification
             */

            switch (pMsg->msgID)
            {
            case PSLSOCKETMSG_CONNECT:
                /*  We have connected to the remote device- go ahead and
                 *  send our initialization
                 */

                ps = MTPBTParserSendInitEventRequest(pciCur->mtpbtEvent,
                            &(pciCur->guidLinkID), pciCur->wPSMResponder);
                if (PSL_FAILED(ps))
                {
                    break;
                }
                break;

            case PSLSOCKETMSG_DATA_AVAILABLE:
                /*  Determine the packet type- we should only ever see an
                 *  InitFailed packet here
                 */

                ps = MTPStreamParserGetPacketType(pciCur->mtpbtEvent,
                            &dwPacketType, PSLNULL);
                if (PSL_FAILED(ps) ||
                    (MTPBTPACKET_INIT_LINK_FAIL != dwPacketType))
                {
                    ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_PACKET;
                    break;
                }

                /*  Retrieve the failure code
                 */

                ps = MTPBTParserReadInitFail(pciCur->mtpbtEvent, &dwReason);
                if (PSL_FAILED(ps))
                {
                    break;
                }

                /*  FALL THROUGH INTETIONAL */

            case PSLSOCKETMSG_CLOSE:
                /*  Set the connection closed error so that we just pull this
                 *  entry out of the list
                 */

                ps = PSLERROR_CONNECTION_CLOSED;
                break;

            default:
                /*  Should never get here
                 */

                PSLASSERT(FALSE);
                continue;
            }

            /*  If we handled the notification and encountered an error
             */

            if (PSL_FAILED(ps))
            {
                ps = _BTConnectInfoFind(BTCONNECTINFO_FIND_EVENTPARSER,
                            (PSLPARAM)pciCur->mtpbtEvent, PSLTRUE,
                            &pciCur);
                if (PSLSUCCESS == ps)
                {
                    SAFE_BTCONNECTINFODESTROY(pciCur);
                }
            }
        }
    }

    /*  Report Success
     */

    ps = PSLSUCCESS;

Exit:
    /*  Mark the thread as shutdown
     */

    PSLAtomicCompareExchange(&_DwConnectMgrState,
                            CONNECTMGRSTATE_SHUTDOWN,
                            CONNECTMGRSTATE_RUNNING);

    /*  Release any pending connections
     */

    _BTConnectInfoRemoveAll();

    /*  And check messages to make sure that we did not have a pending request
     *  that can no longer be serviced
     */

    while (PSLSUCCESS == PSLMsgPeek(_MqConnectMgr, PSLNULL))
    {
        /*  Release any current message
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  And retrieve the next message
         */

        if (SUCCEEDED(PSLMsgGet(_MqConnectMgr, &pMsg)))
        {
            /*  Check to see if this is an operation request
             */

            if (CONNECTMGRMSG_CONNECT_INITIATOR == pMsg->msgID)
            {
                *((PSLSTATUS*)((PSLUINT32)pMsg->alParam)) =
                            PSLERROR_SHUTTING_DOWN;
            }
        }
    }
    SAFE_PSLMSGFREE(pMsg);
    SAFE_PSLSIGNALCLOSE(hSignalInit);
    return ps;
    pvParam;
}


/*  MTPBTTransportGetEventConnection
 *
 *  Locates the Event Connection associated with the specified link ID
 *  and removes it from the list
 *
 *  Arguments:
 *      PSLGUID*        pguidLinkID         Link ID to match
 *      MTPBTPARSER*    pmtpbtEvent         Return buffer for parser
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if found, PSLSUCCESS_NOT_FOUND if not
 *
 */

PSLSTATUS MTPBTTransportGetEventParser(PSL_CONST PSLGUID* pguidLinkID,
                    MTPBTPARSER* pmtpbtEvent)
{
    PSLSTATUS       ps;
    BTCONNECTINFO*  pci = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpbtEvent)
    {
        *pmtpbtEvent = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pguidLinkID) || (PSLNULL == pmtpbtEvent))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Locate the item and remove it if present
     */

    ps = _BTConnectInfoFind(BTCONNECTINFO_FIND_LINKID, (PSLPARAM)pguidLinkID,
                            PSLTRUE, &pci);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    /*  Return the parser and let the information be cleaned up on exit
     */

    *pmtpbtEvent = pci->mtpbtEvent;

Exit:
    SAFE_BTCONNECTINFODESTROY(pci);
    return ps;
}


/*
 *  MTPBTTransportInitialize
 *
 *  Initializes the MTP/BT transport
 *
 *  Arguments:
 *      PSLUINT32       dwInitFlags         Initialization flags
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPBTTransportInitialize(PSLUINT32 dwInitFlags)
{
    PSLUINT32       dwState = CONNECTMGRSTATE_RUNNING;
    PSLBOOL         fCloseMTPBTSocket = PSLFALSE;
    PSLHANDLE       hThread = PSLNULL;
    PSLHANDLE       hSignalInit = PSLNULL;
    PSLMSGPOOL      mpConnectMgr = PSLNULL;
    PSLMSGQUEUE     mqConnectMgr = PSLNULL;
    PSLSTATUS       ps;
    PSLVOID*        pvParam = PSLNULL;

    /*  Make sure that we are supposed to initialize
     */

    if (!(MTPINITFLAG_BT_TRANSPORT & dwInitFlags))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Currently only the router is supported
     */

    if (MTPINITFLAG_INITIATOR & dwInitFlags)
    {
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;
    }

    /*  Only one initialization is allowed
     */

    if (_FMTPBTSocketInit)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Initialize the MTP BT Socket API
     */

    ps = MTPBTSocketInitialize();
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    _FMTPBTSocketInit = PSLTRUE;
    fCloseMTPBTSocket = PSLTRUE;

    /*  Reserve enough bytes in contexts to handle our transport needs
     */

    ps = MTPContextReserveTransportBytes(MTPBTROUTER_TRANSPORT_SIZE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create the initialized signal
     */

    ps = PSLSignalOpen(PSLFALSE, PSLTRUE, _RgchConnectMgrInitSignal,
                            &hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Loop here to determine what state the router is in- unless the router
     *  is active we want to loop here until we can get it into an active
     *  state.
     */

    do
    {
        /*  Try and set the router to initializing state
         */

        dwState = PSLAtomicCompareExchange(&_DwConnectMgrState,
                            CONNECTMGRSTATE_INITIALIZING,
                            CONNECTMGRSTATE_UNKNOWN);
        switch (dwState)
        {
        case CONNECTMGRSTATE_RUNNING:
            /*  Router is already active- only one initialization is allowed
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;

        case CONNECTMGRSTATE_SHUTTING_DOWN:
        case CONNECTMGRSTATE_SHUTDOWN:
            /*  The entire transport is in the process of shutting down- do
             *  not attempt to initialize the router
             */

            ps = PSLERROR_SHUTTING_DOWN;
            goto Exit;

        case CONNECTMGRSTATE_INITIALIZING:
            /*  Another thread is initializing the router- give it a chance
             *  to complete the work
             */

            PSLASSERT(PSLNULL != hSignalInit);
            ps = PSLSignalWait(hSignalInit, PSLTHREAD_INFINITE);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        case CONNECTMGRSTATE_UNKNOWN:
            /*  We own initializing the router- nothing to do here
             */

            break;

        default:
            /*  Unknown init router state
             */

            PSLASSERT(FALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }
    }
    while (CONNECTMGRSTATE_UNKNOWN != dwState);

    /*  Validate our invariants before starting the initialization
     */

    PSLASSERT(PSLNULL == _HThreadConnectMgr);
    PSLASSERT(PSLNULL == _MqConnectMgr);
    PSLASSERT(PSLNULL == _MpConnectMgr);
    PSLASSERT(PSLNULL == _HMutexConnectList);

    /*  Create the connection list mutex
     */

    ps = PSLMutexOpen(0, PSLNULL, &_HMutexConnectList);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure the signal is reset before we continue
     */

    ps = PSLSignalReset(hSignalInit);
    PSLASSERT(PSL_SUCCEEDED(ps));

    /*  Create the message queue and message pool for the init router
     *  thread.  These pools will be used internally to handle each new
     *  socket as it is bound until it is determined what type of socket
     *  it is (command/data or event) and it can be bound to a connection
     *  specific handler.
     */

    ps = MTPMsgGetQueueParam(&pvParam);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = PSLMsgQueueCreate(pvParam, MTPMsgOnPost, MTPMsgOnWait, MTPMsgOnDestroy,
                            &mqConnectMgr);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pvParam = NULL;

    ps = PSLMsgPoolCreate(CONNECTMGR_MSG_POOL_SIZE, &mpConnectMgr);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create the thread that will handle the messages for the router
     *  initialization function- note that the thread will block until
     *  the initialization goes to "running" so that we do not have to worry
     *  about contention issues with the data we have created.
     */

    ps = PSLThreadCreate(_BTConnectMgrThread, NULL, PSLTRUE,
                            PSLTHREAD_DEFAULT_STACK_SIZE, &hThread);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize the static variables
     */

    _MqConnectMgr = mqConnectMgr;
    _MpConnectMgr = mpConnectMgr;
    _HThreadConnectMgr = hThread;

    /*  Resume the thread so it can start executing
     */

    ps = PSLThreadResume(hThread);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        /*  Make sure the thread is terminated
         */

        PSLThreadTerminate(hThread, (PSLUINT32)PSLERROR_UNEXPECTED);
        goto Exit;
    }

    /*  Thread now has ownership of the static objects- forget them here
     */

    mqConnectMgr = PSLNULL;
    mpConnectMgr = PSLNULL;
    hThread = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    /*  If we failed to initialize for any reason we want to reset the
     *  initialization state and set the event to unblock any othre threads
     *  that might be waiting
     */

    if (PSL_FAILED(ps))
    {
        /*  Make sure the thread shuts down correctly
         */

        if (CONNECTMGRSTATE_UNKNOWN == dwState)
        {
            /*  We should only get here in an initializing state
             */

            PSLASSERT(CONNECTMGRSTATE_INITIALIZING == _DwConnectMgrState);

            /*  Reset the state
             */

            PSLAtomicExchange(&_DwConnectMgrState, CONNECTMGRSTATE_UNKNOWN);

            /*  And signal any other threads
             */

            if (PSLNULL != hSignalInit)
            {
                PSLSignalSet(hSignalInit);
            }
        }

        /*  And close the MTP Bluetooth Socket interface
         */

        if (fCloseMTPBTSocket)
        {
            MTPBTSocketUninitialize();
            _FMTPBTSocketInit = PSLFALSE;
        }
    }
    SAFE_PSLTHREADCLOSE(hThread);
    SAFE_PSLMSGPOOLDESTROY(mpConnectMgr);
    SAFE_PSLMSGQUEUEDESTROY(mqConnectMgr);
    SAFE_MTPMSGRELEASEQUEUEPARAM(pvParam);
    SAFE_PSLSIGNALCLOSE(hSignalInit);
    return ps;
}


/*
 *  MTPBTTransportUninitialize
 *
 *  Uninitializes the MTP/BT transport
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPBTTransportUninitialize()
{
    PSLUINT32       dwResult;
    PSLUINT32       dwInitialState;
    PSLUINT32       dwState;
    PSLMSGQUEUE     mqShutdown = PSLNULL;
    PSLSTATUS       ps;
    PSLMSG*         pMsg = PSLNULL;

    /*  Check the state of the thread- blocking if initialization is pending
     */

    ps = _CheckConnectMgrThreadState(PSLFALSE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Tack ownership of shutting down the thread- we assume that thread is
     *  in a good state initially and that we are just shutting down normally.
     *  In the event the thread has already exited abnormally we need to do
     *  an extra check to make sure that we really own the shutdown.
     */

    dwInitialState = CONNECTMGRSTATE_RUNNING;
    do
    {
        /*  Make sure that we own the shutdown
         */

        dwState = PSLAtomicCompareExchange(&_DwConnectMgrState,
                            CONNECTMGRSTATE_SHUTTING_DOWN,
                            dwInitialState);
        switch (dwState)
        {
        case CONNECTMGRSTATE_RUNNING:
            /*  No work to do here- we are responsible for shutting down the
             *  bind router thread
             */

            PSLASSERT(CONNECTMGRSTATE_RUNNING == dwInitialState);
            break;

        case CONNECTMGRSTATE_SHUTDOWN:
            /*  If the old state and the initial state are the same then
             *  we succesfully got ownership of the shutdown- stop working
             */

            if (dwInitialState == dwState)
            {
                dwInitialState = CONNECTMGRSTATE_RUNNING;
                break;
            }

            /*  We want to take ownership of the shutdown process at this point
             *  so switch the initial state and try again
             */

            dwInitialState = CONNECTMGRSTATE_SHUTDOWN;
            break;

        case CONNECTMGRSTATE_UNKNOWN:
            /*  If we never attempted to initialize then we should just
             *  shutdown quietly
             */

            ps = PSLERROR_NOT_INITIALIZED;
            goto Exit;

        case CONNECTMGRSTATE_SHUTTING_DOWN:
        case CONNECTMGRSTATE_INITIALIZING:
            /*  We should only ever initialize once and shutdown once- if
             *  we get here this is an error
             *
             *  FALL THROUGH INTENTIONAL
             */

        default:
            /*  Should never get here
             */

            PSLASSERT(FALSE);
            ps = PSLERROR_NOT_INITIALIZED;
            goto Exit;
        }
    } while (CONNECTMGRSTATE_SHUTDOWN == dwInitialState);

    /*  See if we need to tell the thread to shutdown
     */

    if (CONNECTMGRSTATE_RUNNING == dwState)
    {
        /*  Notify the thread that it needs to shutdown
         */

        ps = PSLMsgAlloc(_MpConnectMgr, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = CONNECTMGRMSG_SHUTDOWN;

        /*  Post the message and transfer ownership of the message to the queue
         */

        ps = PSLMsgPost(_MqConnectMgr, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = PSLNULL;
    }

    /*  Make sure that the thread is closed
     */

    if (PSLNULL != _HThreadConnectMgr)
    {
        /*  Wait for the thread to shutdown
         */

        ps = PSLThreadGetResult(_HThreadConnectMgr, INFINITE, &dwResult);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Close the thread
         */

        SAFE_PSLTHREADCLOSE(_HThreadConnectMgr);
    }

    /*  Release the message pool and remove the queue from the state so
     *  that we can unregister it from the shutdown service
     */

    SAFE_PSLMSGPOOLDESTROY(_MpConnectMgr);
    mqShutdown = _MqConnectMgr;
    _MqConnectMgr = PSLNULL;

    /*  Close down and remove all connection entries that might be pending
     */

    _BTConnectInfoRemoveAll();
    SAFE_PSLMUTEXCLOSE(_HMutexConnectList);

    /*  Reset the state of the router to unknown
     */

    dwState = PSLAtomicExchange(&_DwConnectMgrState,
                            CONNECTMGRSTATE_UNKNOWN);

    /*  Close the MTP BT Socket API
     */

    if (_FMTPBTSocketInit)
    {
        MTPBTSocketUninitialize();
        _FMTPBTSocketInit = PSLFALSE;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMSGQUEUEDESTROY(mqShutdown);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPBTBindInitiator
 *
 *  Binds a connection from an initiator to the MTP/BT Transport.
 *
 *  Arguments:
 *      SOCKET          sock                Bluetooth socket to bind to
 *      struct sockaddr*
 *                      psaInitiator        Socket address of remote initiator
 *      PSLUINT32       cbAddr              Length of the address
 *      PSLUINT16       wPSMResponder       PSM specified in the responder SDP
 *                                            record (if published)
 *      PSLPARAM        aParamTransport     Transport specific parameter used
 *                                            during transport monitor callbacks
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPBTBindInitiator(SOCKET sock,
                    PSL_CONST struct sockaddr* psaInitiator,
                    PSLUINT32 cbAddr,  PSLUINT16 wPSMResponder,
                    PSLPARAM aParamTransport)
{
    PSLHANDLE       hSignalComplete = PSLNULL;
    PSLHANDLE       hThreadRouter = PSLNULL;
    MTPBTROUTERINIT mtpbtri = { 0 };
    PSLSTATUS       ps;
    PSLSTATUS       psMsg;
    BTCONNECTINFO*  pci = PSLNULL;
    PSLMSG*         pMsg = PSLNULL;

    /*  Validate Arguments
     */

    if ((PSLNULL == psaInitiator) || (sizeof(SOCKADDR_STORAGE) < cbAddr))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure that the transport has been initialized
     */

    ps = _CheckConnectMgrThreadState(PSLTRUE);
    if (PSL_FAILED(ps))
    {
        ps = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    /*  If no socket was provided then we need to perform a connection with
     *  link rendezvous ourselves rather than just binding directly to what
     *  we have
     */

    if (INVALID_SOCKET == sock)
    {
        /*  Create an entry to hold the socket information
         */

        pci = (BTCONNECTINFO*)PSLMemAlloc(PSLMEM_PTR, sizeof(BTCONNECTINFO));
        if (PSLNULL == pci)
        {
            ps = PSLERROR_OUT_OF_MEMORY;
            goto Exit;
        }

        /*  And initialize it
         */

#ifdef PSL_ASSERTS
        pci->dwCookie = BTCONNECTINFO_COOKIE;
#endif  /* PSL_ASSERTS */
        PSLCopyMemory(&(pci->saClient), psaInitiator, sizeof(pci->saClient));
        pci->cbAddr = cbAddr;
        pci->wPSMResponder;
        pci->aParamTransport = aParamTransport;

        /*  Send a message to notify the bind thread of the socket- note that we
         *  have to do this before we start up notifications so that the thread
         *  learns of the socket before it gets any notifications.
         */

        ps = PSLMsgAlloc(_MpConnectMgr, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = CONNECTMGRMSG_CONNECT_INITIATOR;
        pMsg->aParam0 = (PSLPARAM)pci;
        pMsg->alParam = (PSLLPARAM)&psMsg;

        /*  Send the message and transfer ownership of the socket information to
         *  the message queue
         */

        ps = PSLMsgSend(_MqConnectMgr, pMsg, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = PSLNULL;
        aParamTransport = MTPTRANSPORTPARAM_UNKNOWN;
        if (PSL_FAILED(psMsg))
        {
            ps = psMsg;
            goto Exit;
        }
        pci = PSLNULL;
        goto Exit;
    }

    /*  Initialize the router initialization info with the full socket
     */

    ps = PSLSignalOpen(PSLFALSE, PSLFALSE, PSLNULL, &hSignalComplete);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize the startup data
     */

    mtpbtri.hSignalComplete = hSignalComplete;
    mtpbtri.sockInitiator = sock;
    PSLCopyMemory(&(mtpbtri.saInitiator), psaInitiator, cbAddr);
    mtpbtri.cbAddrInitiator = cbAddr;
    mtpbtri.wPSMResponder = wPSMResponder;
    mtpbtri.aParamTransport = aParamTransport;

    /*  Start a new MTP/BT router thread to handle this initiator
     */

    ps = PSLThreadCreate(MTPBTRouterThread, &mtpbtri, PSLFALSE,
                        PSLTHREAD_DEFAULT_STACK_SIZE, &hThreadRouter);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    aParamTransport = MTPTRANSPORTPARAM_UNKNOWN;

    /*  Wait for the thread to complete initialization
     */

    ps = PSLSignalWait(hSignalComplete, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the result from the initialization
     */

    ps = mtpbtri.psResult;

Exit:
    /*  Mark the transport as closed if we were unable to hand it off
     */

    CHECK_MTPTRANSPORTNOTIFY(MTPTRANSPORTEVENT_CLOSED, aParamTransport);
    SAFE_PSLTHREADCLOSE(hThreadRouter);
    SAFE_PSLSIGNALCLOSE(hSignalComplete);
    SAFE_PSLMSGFREE(pMsg);
    SAFE_PSLMEMFREE(pci);
    return ps;
}


/*
 *  MTPBTBindResponder
 *
 *  Binds the MTP/BT transport to the specifed responder.
 *
 *  Arguments:
 *      SOCKET          sock                Bluetooth socket to bind to
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPBTBindResponder(SOCKET sock)
{
    PSLSTATUS           ps;

    /*  Validate arguments
     */

    if (INVALID_SOCKET == sock)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure that the transport has been initialized
     */

    ps = _CheckConnectMgrThreadState(PSLTRUE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Not currently implemented
     */

    ps = PSLERROR_NOT_IMPLEMENTED;

Exit:
    return ps;
}


