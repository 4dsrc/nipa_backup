/*
 *  MTPBTParser.c
 *
 *  Contains implementations of the MTP/BT packet serialization/de-serialization
 *  routines to convert MTP/BT packet formats to internal MTP packet formats.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPBTTransportPrecomp.h"

/*  Local Defines
 */

#define MTPBTPARSER_COOKIE          'mbtP'

#define GET_NEXT_SEQUENCE_ID(_w) \
        (((0 != _w) && (0xfffe != _w)) ? (_w + 1) : 1)

#define CRC_INITIAL_VALUE           0xffffffff

#define MTPBT_FAULT_MAX_BYTES       0xfff

/*  Local Types
 */

typedef struct _MTPBTPACKETINFO
{
    PSLUINT32               dwPacketType;
    PSLBYTE                 bPacketID;
} MTPBTPACKETINFO;

/*
 *  MTPBTPARSEROBJ
 *
 *  Internal structure which handles the parser information
 *
 *  NOTE:
 *  Turn on MTPBT_TRANSPORT_FAULT_INJECTION to enable the MTP/BT Parser to
 *  inject fault behavior at the transport level to enable testing of the
 *  MTP/BT Transport Error detection and reset.  The actual use of fault
 *  injection is controlled at runtime by a call to
 *  IsMTPBTTransportFaultInjectionEnabled() so that test versions may be
 *  developped to either enable or disable the fault injection behaviors based
 *  on some platform specific trigger.
 */

typedef struct _MTPBTPARSEROBJ
{
    MTPSTREAMPARSEROBJ      mtpstrmPO;
#ifdef PSL_ASSERTS
    PSLUINT32               dwBTCookie;
#endif  /* PSL_ASSERTS */
    SOCKET                  sock;
    PSLUINT16               wSequenceIDIn;
    PSLUINT16               wSequenceIDOut;
    PSLUINT32               dwCRC32;
    MTPBTTRANSPORTREADY     mtptr;
#ifdef MTPBT_TRANSPORT_FAULT_INJECTION
    PSLUINT16               cbFailOutbound;
    PSLUINT16               cbFailInbound;
    PSLUINT32               cbOutbound;
    PSLUINT32               cbInbound;
#endif  /* MTPBT_TRANSPORT_FAULT_INJECTION */
} MTPBTPARSEROBJ;

/*  Local Functions
 */

static PSLUINT32 _CRCInit(PSLVOID);

static PSLUINT32 _CRCUpdate(PSLUINT32 dwCRC,
                    PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf);

static PSLUINT32 _CRCFinal(PSLUINT32 dwCRC,
                    PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf);

static PSLSTATUS PSL_API _MTPBTParserGetBytesAvailable(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pcbAvailable);

static PSLSTATUS PSL_API _MTPBTParserRead(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLVOID* pvDest,
                    PSLUINT32 cbDest,
                    PSLUINT32* pcbRead);

static PSLSTATUS PSL_API _MTPBTParserSend(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData,
                    PSLUINT32* pcbSent);

static PSLSTATUS PSL_API _MTPBTParserParseHeader(
                    MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPSTREAMHEADER pmtpstrmHdr,
                    PSLUINT32* pdwPacketType,
                    PSLUINT32* pcbPacketSize);

static PSLSTATUS PSL_API _MTPBTParserStoreHeader(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwPacketType,
                    PSLUINT32 cbPacketSize,
                    PXMTPSTREAMHEADER pmtpstrmHdr);

static PSLSTATUS PSL_API _MTPBTParserGetDataHeader(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSL_CONST PSLVOID* pvData,
                    PXMTPSTREAMDATA* ppstrmHeader,
                    PSLUINT32* pcbHeader);

static PSLSTATUS PSL_API _MTPBTParserDestroy(
                    MTPSTREAMPARSER mtpstrmParser);

static PSLSTATUS PSL_API _MTPBTParserGetParserInfo(
                    MTPSTREAMPARSER mtpstrmParser,
                    MTPSTREAMPARSERINFO* pmtpstrmInfo,
                    PSLUINT32 cbInfo);

static PSLSTATUS PSL_API _MTPBTParserSendStartData(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID,
                    PSLUINT64 qwTotalSize);

static PSLSTATUS PSL_API _MTPBTParserReadStartData(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID,
                    PSLUINT64* pqwTotalSize);

static PSLSTATUS PSL_API _MTPBTParserSendData(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID,
                    PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData);

static PSLSTATUS PSL_API _MTPBTParserReadData(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID,
                    PSLVOID* pvData,
                    PSLUINT32 cbMaxData,
                    PSLUINT32* cbRead);

static PSLSTATUS PSL_API _MTPBTParserSendEndData(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID,
                    PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData);

static PSLSTATUS PSL_API _MTPBTParserReadEndData(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID,
                    PSLVOID* pvData,
                    PSLUINT32 cbMaxData,
                    PSLUINT32* cbRead);

static PSLSTATUS PSL_API _MTPBTParserSendProbeResponse(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwDeviceStatus);

/*  Local Variables
 */

MTPSTREAMPARSERVTBL         _VtblMTPBTParser =
{
    _MTPBTParserGetBytesAvailable,
    _MTPBTParserRead,
    _MTPBTParserSend,
    _MTPBTParserParseHeader,
    _MTPBTParserStoreHeader,
    _MTPBTParserGetDataHeader,
    _MTPBTParserDestroy,
    MTPStreamParserSetTypeBase,
    _MTPBTParserGetParserInfo,
    MTPStreamParserResetBase,
#ifdef MTP_INITIATOR
    MTPStreamParserSendOpRequestBase,
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    MTPStreamParserReadOpRequestBase,
    MTPStreamParserSendOpResponseBase,
#endif  /* MTP_RESPONDER */
#ifdef MTP_INITIATOR
    MTPStreamParserReadOpResponseBase,
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    MTPStreamParserSendEventBase,
#endif  /* MTP_RESPONDER */
#ifdef MTP_INITIATOR
    MTPStreamParserReadEventBase,
#endif  /* MTP_INITIATOR */
    _MTPBTParserSendStartData,
    _MTPBTParserReadStartData,
    _MTPBTParserSendData,
    _MTPBTParserReadData,
    _MTPBTParserSendEndData,
    _MTPBTParserReadEndData,
#ifdef MTP_INITIATOR
    MTPStreamParserSendCancelBase,
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    MTPStreamParserReadCancelBase,
#endif  /* MTP_RESPONDER */
    MTPStreamParserSendProbeRequestBase,
    MTPStreamParserReadProbeRequestBase,
    _MTPBTParserSendProbeResponse,
    MTPStreamParserReadProbeResponseBase,
};


static const MTPBTPACKETINFO    _RgpiPacketType[] =
{
    {
        MTPSTREAMPACKET_UNKNOWN,
        MTPBTPACKETID_UNKNOWN
    },
    {
        MTPSTREAMPACKET_OPERATION_REQUEST,
        MTPBTPACKETID_OPERATION_REQUEST
    },
    {
        MTPSTREAMPACKET_OPERATION_RESPONSE,
        MTPBTPACKETID_OPERATION_RESPONSE
    },
    {
        MTPSTREAMPACKET_EVENT,
        MTPBTPACKETID_EVENT
    },
    {
        MTPSTREAMPACKET_START_DATA,
        MTPBTPACKETID_START_DATA
    },
    {
        MTPSTREAMPACKET_DATA,
        MTPBTPACKETID_DATA
    },
    {
        MTPSTREAMPACKET_CANCEL,
        MTPBTPACKETID_CANCEL
    },
    {
        MTPSTREAMPACKET_END_DATA,
        MTPBTPACKETID_END_DATA
    },
    {
        MTPSTREAMPACKET_PROBE_REQUEST,
        MTPBTPACKETID_PROBE_REQUEST
    },
    {
        MTPSTREAMPACKET_PROBE_RESPONSE,
        MTPBTPACKETID_PROBE_RESPONSE
    },
    {
        MTPBTPACKET_INIT_COMMAND_REQUEST,
        MTPBTPACKETID_INIT_COMMAND_REQUEST
    },
    {
        MTPBTPACKET_INIT_EVENT_REQUEST,
        MTPBTPACKETID_INIT_EVENT_REQUEST
    },
    {
        MTPBTPACKET_INIT_LINK_FAIL,
        MTPBTPACKETID_INIT_LINK_FAIL
    },
    {
        MTPBTPACKET_INIT_LINK_ACK,
        MTPBTPACKETID_INIT_LINK_ACK
    },
    {
        MTPBTPACKET_TRANSPORT_ERROR,
        MTPBTPACKETID_TRANSPORT_ERROR
    },
    {
        MTPBTPACKET_TRANSPORT_READY,
        MTPBTPACKETID_TRANSPORT_READY,
    },
    {
        MTPBTPACKET_REQUEST_LINK_DESTROY,
        MTPBTPACKETID_REQUEST_LINK_DESTROY
    }
};


static PSLBOOL              _FCRCInit = PSLFALSE;
static PSLUINT32            _RgdwCRC[256] = { 0 };


/*
 *  _CRCInit
 *
 *  Returns the initial value for a CRC-32 after performing table initialization
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLUINT32       Initial value for CRC
 *
 */

PSLUINT32 _CRCInit(PSLVOID)
{
    PSLUINT32       dwVal;
    PSLBOOL         fInitialized;
    PSLUINT32       idx1;
    PSLUINT32       idx2;

    /*  See if we need to initialize the table
     */

    fInitialized = PSLAtomicCompareExchange(&_FCRCInit, PSLTRUE, PSLFALSE);
    if (!fInitialized)
    {
        /*  Initialize if necessary
         *
         *  NOTE: Initialization is not guarded here by a mutex so while
         *  only one initialization will occur there is no guarantee that it
         *  is completed by the time the table is actually needed.  However,
         *  given the very tight loop here vs. the time to manage a mutex the
         *  desire is to run "risky".
         */

        for (idx1 = 0; idx1 < 256; idx1++)
        {
            for (dwVal = idx1, idx2 = 0; idx2 < 8; idx2++)
            {
                dwVal = (dwVal & 1) ?
                            (0xedb88320L ^ (dwVal >> 1)) : (dwVal >> 1);
            }
            _RgdwCRC[idx1] = dwVal;
        }
    }
    return CRC_INITIAL_VALUE;
}


/*
 *  _CRCUpdate
 *
 *  Updates the value of the CRC-32 provided based on the contents of the
 *  specified buffer
 *
 *  Arguements:
 *      PSLUINT32       dwCRC               CRC to update
 *      PSLVOID*        pvBuf               Buffer to calculate CRC on
 *      PSLUINT32       cbBuf               Number of characters in buffer
 *
 *  Returns:
 *      PSLUINT32       Updated CRC-32 value
 *
 */

PSLUINT32 _CRCUpdate(PSLUINT32 dwCRC, PSL_CONST PSLVOID* pvBuf, PSLUINT32 cbBuf)
{
    PSLUINT32       idxVal;
    PSLBYTE*        pbData;

    /*  We must be initialized by now
     */

    PSLASSERT(_FCRCInit);
    PSLASSERT((0 == cbBuf) || (PSLNULL != pvBuf));

    /*  Walk through and update the value
     */

    for (pbData = (BYTE*)pvBuf, idxVal = 0; idxVal < cbBuf; idxVal++)
    {
        dwCRC = _RgdwCRC[(dwCRC ^ pbData[idxVal]) & 0xff] ^ (dwCRC >> 8);
    }
    return dwCRC;
}


/*
 *  _CRCFinal
 *
 *  Updates the value of the CRC-32 provided based on the contents of the
 *  specified buffer and then finalizes the CRC-32
 *
 *  Arguements:
 *      PSLUINT32       dwCRC               CRC to update
 *      PSLVOID*        pvBuf               Buffer to calculate CRC on
 *      PSLUINT32       cbBuf               Number of characters in buffer
 *
 *  Returns:
 *      PSLUINT32       Updated CRC-32 value
 *
 */

PSLUINT32 _CRCFinal(PSLUINT32 dwCRC, PSL_CONST PSLVOID* pvBuf, PSLUINT32 cbBuf)
{
    /*  Do an update if necessary
     */

    if (0 < cbBuf)
    {
        dwCRC = _CRCUpdate(dwCRC, pvBuf, cbBuf);
    }
    return (dwCRC ^ CRC_INITIAL_VALUE);
}


/*
 *  _MTPBTParserGetBytesAvailable
 *
 *  Returns the number of bytes currently available in the stream.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Stream parser object
 *      PSLUINT32*      pcbAvailable        Return buffer for number of bytes
 *                                            currently available in the stream
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserGetBytesAvailable(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pcbAvailable)
{
    PSLINT32            errSock;
    MTPBTPARSEROBJ*     pmtpbtPO;
    PSLSTATUS           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbAvailable)
    {
        *pcbAvailable = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpstrmParser) || (NULL == pcbAvailable))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Ask the socket directly for the result
     */

    errSock = MTPBTSocketIOCTL(pmtpbtPO->sock, FIONREAD, pcbAvailable);
    if (SOCKET_ERROR == errSock)
    {
        ps = MTPBTSocketErrorToPSLStatus(pmtpbtPO->sock, errSock);
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPBTParserRead
 *
 *  Reads bytes off the socket stream up to the number provided
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to read from
 *      PSLVOID*        pvDest              Destination to read into
 *      PSLUINT32       cbDest              Size of the destination
 *      PSLUINT32*      pcbRead             Number of bytes actually read
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API PSL_API _MTPBTParserRead(MTPSTREAMPARSER mtpstrmParser,
                    PSLVOID* pvDest, PSLUINT32 cbDest, PSLUINT32* pcbRead)
{
    PSLINT32            cbRead;
    MTPBTPARSEROBJ*     pmtpbtPO;
    PSLSTATUS           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbRead)
    {
        *pcbRead = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpstrmParser) || (NULL == pvDest) || (NULL == pcbRead))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Ask the socket directly for the result
     */

    cbRead = MTPBTSocketRecv(pmtpbtPO->sock, (char*)pvDest,
                            (int)min(cbDest, MTPSTREAM_MAX_DATA_PACKET), 0);
    if (SOCKET_ERROR == cbRead)
    {
        ps = MTPBTSocketErrorToPSLStatus(pmtpbtPO->sock, cbRead);
        goto Exit;
    }

    /*  Return the number of bytes read
     */

    *pcbRead = cbRead;
#ifdef MTPBT_TRANSPORT_FAULT_INJECTION
    pmtpbtPO->cbInbound += cbRead;
#endif  /* MTPBT_TRANSPORT_FAULT_INJECTION */
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPBTParserSend
 *
 *  Sends the specified data on the specified socket blocking until
 *  all the data has been accepted.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Stream parser to send data on
 *      PSL_CONST PSLVOID*
 *                      pvData              Data to be sent
 *      PSLUINT32       cbData              Amount of data to send
 *      PSLUINT32*      pcbSent             Number of bytes actually sent
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserSend(MTPSTREAMPARSER mtpstrmParser,
                    PSL_CONST PSLVOID* pvData, PSLUINT32 cbData,
                    PSLUINT32* pcbSent)
{
    PSLUINT32           cbSent;
    MTPBTPARSEROBJ*     pmtpbtPO;
    PSLSTATUS           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbSent)
    {
        *pcbSent = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpstrmParser) || ((0 < cbData) && (PSLNULL == pvData)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Make sure that we will not have a problem sending too large
     *  an object
     */

    PSLASSERT(MTPSTREAM_MAX_DATA_PACKET >= cbData);
    if (MTPSTREAM_MAX_DATA_PACKET < cbData)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Send as much of the packet as we can
     */

    cbSent = MTPBTSocketSend(pmtpbtPO->sock, (char*)pvData, cbData, 0);
    if (SOCKET_ERROR == cbSent)
    {
        ps = MTPBTSocketErrorToPSLStatus(pmtpbtPO->sock, cbSent);
        goto Exit;
    }

    /*  Return number of bytes actually sent
     */

    if (PSLNULL != pcbSent)
    {
        *pcbSent = cbSent;
    }
#ifdef MTPBT_TRANSPORT_FAULT_INJECTION
    pmtpbtPO->cbOutbound += cbSent;
#endif  /* MTPBT_TRANSPORT_FAULT_INJECTION */
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPBTParserParseHeader
 *
 *  Parses the header from the stream and returns the packet type and packet
 *  size.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PCXMTPSTREAMHEADER
 *                      pmtpstrmHdr         Header to parse
 *      PSLUINT32*      pdwPacketType       Header type
 *      PSLUINT32*      pcbPacketSize       Size of the header
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserParseHeader(MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPSTREAMHEADER pmtpstrmHdr, PSLUINT32* pdwPacketType,
                    PSLUINT32* pcbPacketSize)
{
    PSLUINT32           idxType;
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;
    PSLUINT16           wSequenceID;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwPacketType)
    {
        *pdwPacketType = MTPSTREAMPACKET_UNKNOWN;
    }
    if (PSLNULL != pcbPacketSize)
    {
        *pcbPacketSize = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pmtpstrmHdr) ||
        (PSLNULL == pdwPacketType) || (PSLNULL == pcbPacketSize))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Validate that this data really is for our transport
     */

    if (MTPBT_TRANSPORT_ID != pmtpstrmHdr->bTransportID)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

#ifdef MTPBT_TRANSPORT_FAULT_INJECTION
    /*  Determine if it is time to inject fault behavior into the transport
     *  stream to test transport error handling
     */

    if (pmtpbtPO->cbFailInbound <= pmtpbtPO->cbInbound)
    {
        PSLBYTE         bData;

        /*  Do the runtime check to see if we should actually cause the failure
         */

        if (PSLSUCCESS == IsMTPBTTransportFaultInjectionEnabled(PSLTRUE))
        {
            /*  Determine which failure to inject- either a CRC failure or a
             *  sequence ID failure
             */

            ps = PSLGenRandom(&bData, sizeof(bData));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  We just use the simple odd/even approach to deciding which
             *  failure to attempt to inject.  Note that we may not always hit
             *  a CRC-32 failure because of the fact that we cannot guarantee
             *  that the data phase is active.
             */

            if (0x1 & bData)
            {
                /*  Force a failure to occur on the sequence ID
                 */

                pmtpbtPO->wSequenceIDIn =
                            GET_NEXT_SEQUENCE_ID(pmtpbtPO->wSequenceIDIn);
            }
            else
            {
                /*  Force a failure to occur on the CRC
                 */

                pmtpbtPO->dwCRC32++;
            }
        }

        /*  Reset the inbound failure counts
         */

        ps = PSLGenRandom(&(pmtpbtPO->cbFailInbound),
                            sizeof(pmtpbtPO->cbFailInbound));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pmtpbtPO->cbFailInbound &= MTPBT_FAULT_MAX_BYTES;
        pmtpbtPO->cbInbound = 0;
    }
#endif  /* MTPBT_TRANSPORT_FAULT_INJECTION */

    /*  Validate the sequence ID
     */

    wSequenceID = MTPLoadUInt16(&(pmtpstrmHdr->wTransportData));
    if ((0 != pmtpbtPO->wSequenceIDIn) &&
        (GET_NEXT_SEQUENCE_ID(pmtpbtPO->wSequenceIDIn) != wSequenceID))
    {
        PSLTraceError("\t\t********  MTP BT Sequence ID Mismatch ********");
        PSLTraceError("MTPBTParserParseHeader: SeqID Recv = 0x%08x\tExp= 0x%08x",
                            wSequenceID,
                            GET_NEXT_SEQUENCE_ID(pmtpbtPO->wSequenceIDIn));
        ps = PSLERROR_CONNECTION_CORRUPT;
        goto Exit;
    }
    else
    {
        PSLTraceProgress(
                    "MTPBTParserParseHeader: SeqID Recv = 0x%08x\tExp= 0x%08x",
                            wSequenceID,
                            GET_NEXT_SEQUENCE_ID(pmtpbtPO->wSequenceIDIn));
    }

    pmtpbtPO->wSequenceIDIn = wSequenceID;

    /*  Determine the packet type from the packet ID
     */

    for (idxType = 0;
            (idxType < PSLARRAYSIZE(_RgpiPacketType)) &&
                pmtpstrmHdr->bPacketID != _RgpiPacketType[idxType].bPacketID;
            idxType++)
    {
    }
    if (PSLARRAYSIZE(_RgpiPacketType) <= idxType)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }
    *pdwPacketType = _RgpiPacketType[idxType].dwPacketType;

    PSLTraceProgress("MTPBTParserParseHeader: PacketID = 0x%02x\t"
                            "PacketType = 0x%04x", pmtpstrmHdr->bPacketID,
                            *pdwPacketType);

    /*  And pull out the size
     */

    *pcbPacketSize = MTPLoadUInt32(&(pmtpstrmHdr->cbPacket));
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPBTParserStoreHeader
 *
 *  Stores information in an outbound packet header.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32       dwPacketType        Stream parser packet type
 *      PSLUINT32       cbPacketSize        Size of the packet
 *      PXMTPSTREAMHEADER
 *                      pmtpstrmHdr         Destination header location
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserStoreHeader(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwPacketType, PSLUINT32 cbPacketSize,
                    PXMTPSTREAMHEADER pmtpstrmHdr)
{
    PSLUINT32           idxType;
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpstrmHdr)
    {
        PSLZeroMemory(pmtpstrmHdr, sizeof(*pmtpstrmHdr));
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pmtpstrmHdr))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure this packet conforms to our size requirements
     */

    PSLASSERT(MTPSTREAM_MAX_DATA_PACKET >= cbPacketSize);
    if (MTPSTREAM_MAX_DATA_PACKET < cbPacketSize)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Store the packet ID based on the type
     */

    for (idxType = 0;
            (idxType < PSLARRAYSIZE(_RgpiPacketType)) &&
                    dwPacketType != _RgpiPacketType[idxType].dwPacketType;
            idxType++)
    {
    }
    if (PSLARRAYSIZE(_RgpiPacketType) <= idxType)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }
    pmtpstrmHdr->bPacketID = _RgpiPacketType[idxType].bPacketID;
    pmtpstrmHdr->bTransportID = MTPBT_TRANSPORT_ID;

#ifdef MTPBT_TRANSPORT_FAULT_INJECTION
    /*  Determine if it is time to inject fault behavior into the transport
     *  stream to test transport error handling
     */

    if (pmtpbtPO->cbFailOutbound <= pmtpbtPO->cbOutbound)
    {
        PSLBYTE         bData;

        /*  Perform the runtime check to see if we are really supposed to
         *  inject the fault
         */

        if (PSLSUCCESS == IsMTPBTTransportFaultInjectionEnabled(PSLFALSE))
        {
            /*  Determine which failure to inject- either a CRC failure or a
             *  sequence ID failure
             */

            ps = PSLGenRandom(&bData, sizeof(bData));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  We just use the simple odd/even approach to deciding which
             *  failure to attempt to inject.  Note that we may not always hit
             *  a CRC-32 failure because of the fact that we cannot guarantee
             *  that the data phase is active.
             */

            if (0x1 & bData)
            {
                /*  Force a failure to occur on the sequence ID
                 */

                pmtpbtPO->wSequenceIDOut =
                            GET_NEXT_SEQUENCE_ID(pmtpbtPO->wSequenceIDOut);
            }
            else
            {
                /*  Force a failure to occur on the CRC
                 */

                pmtpbtPO->dwCRC32++;
            }
        }

        /*  Reset the outbound failure counts
         */

        ps = PSLGenRandom(&(pmtpbtPO->cbFailOutbound),
                            sizeof(pmtpbtPO->cbFailOutbound));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pmtpbtPO->cbFailOutbound &= MTPBT_FAULT_MAX_BYTES;
        pmtpbtPO->cbOutbound = 0;
    }
#endif  /* MTPBT_TRANSPORT_FAULT_INJECTION */

    /*  Add the sequence ID
     */

    MTPStoreUInt16(&(pmtpstrmHdr->wTransportData), pmtpbtPO->wSequenceIDOut);
    pmtpbtPO->wSequenceIDOut = GET_NEXT_SEQUENCE_ID(pmtpbtPO->wSequenceIDOut);

    /*  And add the length
     */

    MTPStoreUInt32(&(pmtpstrmHdr->cbPacket), cbPacketSize);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPBTParserGetDataHeader
 *
 *  To avoid copying data the data header is assumed to be prepended to the
 *  beginning of the data block- however not all MTP stream implementations
 *  use headers of the same length.  This function returns a standard stream
 *  header from the beginning of the header block.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLVOID*        pvData              Data buffer to get header for
 *      PXMTPSTREAMDATA*
 *                      ppstrmHeader        Return location for the header
 *      PSLUINT32       pcbHeader           Size of the entire header (stream
 *                                            data header and additional fields)
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPBTParserGetDataHeader(MTPSTREAMPARSER mtpstrmParser,
                    PSL_CONST PSLVOID* pvData, PXMTPSTREAMDATA* ppstrmHeader,
                    PSLUINT32* pcbHeader)
{
    PSLUINT32           cbHeader;
    PSLVOID*            pvHeader;
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;
    PXMTPBTDATA         pmtpbtData;

    /*  Clear results for safety
     */

    if (PSLNULL != ppstrmHeader)
    {
        *ppstrmHeader = PSLNULL;
    }
    if (PSLNULL != pcbHeader)
    {
        *pcbHeader = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pvData) ||
        (PSLNULL == ppstrmHeader) || (PSLNULL == pcbHeader))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Retrieve the header buffer from the data packet- and make sure that it
     *  is large enough for the stock header.
     */

    ps = MTPStreamBufferMgrGetBufferHeader(pvData, &pvHeader, &cbHeader);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    PSLASSERT(sizeof(*pmtpbtData) <= cbHeader);
    if (sizeof(*pmtpbtData) > cbHeader)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Determine how much of the header is needed- the rules state that
     *  the header is physically contiguous with the data so we just need
     *  to make sure that we add the necessary amount to the header pointer
     *  so that we get the header followed by the data as specified in the
     *  MTP Stream protocol
     */

    pmtpbtData = (PXMTPBTDATA)((PSLBYTE*)pvHeader +
                            (cbHeader - sizeof(*pmtpbtData)));
    *ppstrmHeader = &(pmtpbtData->mtpstrmData);
    *pcbHeader = sizeof(*pmtpbtData);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPBTParserDestroy
 *
 *  Releases the specified MTP/BT Parser.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to destroy
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserDestroy(MTPSTREAMPARSER mtpstrmParser)
{
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;

    /*  Make sure we have work to do
     */

    if (PSLNULL == mtpstrmParser)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure all of the sockets have been release
     */

    SAFE_MTPBTSOCKETCLOSE(pmtpbtPO->sock);

    /*  And let the base class do the rest work
     */

    ps = MTPStreamParserDestroyBase(mtpstrmParser);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  _MTPBTParserGetParserInfo
 *
 *  Retrieves information about the parser state
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to query
 *      MTPSTREAMPARSERINFO
 *                      pmtpstrmInfo        Parser information to return
 *      PSLUINT32       cbInfo              Size of the information object
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserGetParserInfo(MTPSTREAMPARSER mtpstrmParser,
                    MTPSTREAMPARSERINFO* pmtpstrmInfo,
                    PSLUINT32 cbInfo)
{
    int                 cbData;
    PSLINT32            errSock;
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;
    MTPBTPARSERINFO*    pmtpbtInfo;
    PSLUINT16           wVal;

    /*  Clear result for safety
     */

    if ((PSLNULL != pmtpstrmInfo) && (0 < cbInfo))
    {
        PSLZeroMemory(pmtpstrmInfo, cbInfo);
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pmtpstrmInfo) ||
        (sizeof(MTPBTPARSERINFO) != cbInfo))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);
    pmtpbtInfo = (MTPBTPARSERINFO*)pmtpstrmInfo;

    /*  Use the base class to initialize the core information
     */

    ps = MTPStreamParserGetParserInfoBase(mtpstrmParser,
                            &(pmtpbtInfo->mtpstrmInfo),
                            sizeof(pmtpbtInfo->mtpstrmInfo));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Extract the requested information
     */

    if (MTPSTREAMPARSERTYPE_UNKNOWN != pmtpbtInfo->mtpstrmInfo.dwParserType)
    {
        /*  We should have a socket at this point
         */

        PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);

        /*  Retrieve the local address of the socket
         */

        pmtpbtInfo->cbAddrLocal = sizeof(pmtpbtInfo->saAddrLocal);
        errSock = MTPBTSocketGetSockName(pmtpbtPO->sock,
                            (SOCKADDR*)&(pmtpbtInfo->saAddrLocal),
                            (int*)&(pmtpbtInfo->cbAddrLocal));
        if (SOCKET_ERROR == errSock)
        {
            ps = MTPBTSocketErrorToPSLStatus(pmtpbtPO->sock, errSock);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }

        /*  Retrieve the destination address of the socket
         */

        pmtpbtInfo->cbAddrDest = sizeof(pmtpbtInfo->saAddrDest);
        errSock = MTPBTSocketGetPeerName(pmtpbtPO->sock,
                            (SOCKADDR*)&(pmtpbtInfo->saAddrDest),
                            (int*)&(pmtpbtInfo->cbAddrDest));
        if (SOCKET_ERROR == errSock)
        {
            ps = MTPBTSocketErrorToPSLStatus(pmtpbtPO->sock, errSock);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }

        /*  Retrieve the maximum data size that we can send and receive
         */

        cbData = sizeof(wVal);
        errSock = MTPBTSocketGetSockOpt(pmtpbtPO->sock, MTPBT_PROTO_L2CAP,
                            MTPBT_SO_L2CAP_OMTU, (char*)&wVal, &cbData);
        if (SOCKET_ERROR == errSock)
        {
            ps = MTPBTSocketErrorToPSLStatus(pmtpbtPO->sock, errSock);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        pmtpbtInfo->cbMaxSend = wVal;

        cbData = sizeof(wVal);
        errSock = MTPBTSocketGetSockOpt(pmtpbtPO->sock, MTPBT_PROTO_L2CAP,
                            MTPBT_SO_L2CAP_IMTU, (char*)&wVal, &cbData);
        if (SOCKET_ERROR == errSock)
        {
            ps = MTPBTSocketErrorToPSLStatus(pmtpbtPO->sock, errSock);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        pmtpbtInfo->cbMaxReceive = wVal;
     }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPBTParserSendStartData
 *
 *  Sends a start data packet to the destination
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32       dwTransactionID     Transaction ID
 *      PSLUINT64       qwTotalSize         Total size of the data to be sent
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserSendStartData(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID, PSLUINT64 qwTotalSize)
{
    PSLSTATUS       ps;
    MTPBTPARSEROBJ* pmtpbt;

    /*  Validate arguments
     */

    if (NULL == mtpstrmParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpbt = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbt->dwBTCookie);

    /*  Initialize the CRC-32 to the correct state
     */

    pmtpbt->dwCRC32 = _CRCInit();

    /*  And use the base class to send the packet
     */

    ps = MTPStreamParserSendStartDataBase(mtpstrmParser, dwTransactionID,
                            qwTotalSize);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  _MTPBTParserReadStartData
 *
 *  Reads a start data packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32*      pdwTransactionID    Transaction ID
 *      PSLUINT64*      pqwTotalSize        Total size of data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserReadStartData(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID,
                    PSLUINT64* pqwTotalSize)
{
    PSLSTATUS       ps;
    MTPBTPARSEROBJ* pmtpbt;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwTransactionID)
    {
        *pdwTransactionID = MTPTRANSACTION_UNKNOWN;
    }
    if (PSLNULL != pqwTotalSize)
    {
        *pqwTotalSize = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pdwTransactionID) ||
        (PSLNULL == pqwTotalSize))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpbt = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbt->dwBTCookie);

    /*  Initialize the CRC-32 to the correct state
     */

    pmtpbt->dwCRC32 = _CRCInit();

    /*  And use the base class to read the packet
     */

    ps = MTPStreamParserReadStartDataBase(mtpstrmParser, pdwTransactionID,
                            pqwTotalSize);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  _MTPBTParserSendData
 *
 *  Sends the data specifed and then calculates the CRC-32 value
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use for sendning data
 *      PSLUINT32       dwTransactionID     Transaction ID for the send
 *      PSLVOID*        pvData              Data to send
 *      PSLUINT32       cbData              Size of data to send
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserSendData(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID, PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData)
{
    PSLUINT32       cbHeader;
    PSLSTATUS       ps;
    MTPBTPARSEROBJ* pmtpbt;
    MTPBTDATA*      pmtpbtData;

    /*  Validate arguments
     */

    if ((NULL == mtpstrmParser) || ((0 < cbData) && (NULL == pvData)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpbt = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbt->dwBTCookie);

    /*  Retrieve the header from the data so that we can add the running CRC32
     *  value into the header
     */

    ps = MTPStreamBufferMgrGetBufferHeader(pvData, &pmtpbtData, &cbHeader);
    if (PSL_FAILED(ps) || (sizeof(*pmtpbtData) != cbHeader))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Add the running CRC-32 value into the packet before it gets sent
     */

    PSLTraceProgress("_MTPBTParserSendData: Running CRC32 Sent = 0x%08x\t",
                            pmtpbt->dwCRC32);
    MTPStoreUInt32(&(pmtpbtData->dwRunningCRC32), pmtpbt->dwCRC32);

    /*  Use the base class to actually send the data
     */

    ps = MTPStreamParserSendDataBase(mtpstrmParser, dwTransactionID, pvData,
                            cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And calculate the CRC-32 over the data
     */

    pmtpbt->dwCRC32 = _CRCUpdate(pmtpbt->dwCRC32, pvData, cbData);

Exit:
    return ps;
}


/*
 *  _MTPBTParserReadData
 *
 *  Reads a data packet from the specified parser and then calculates the
 *  CRC-32 of the data.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to read data with
 *      PSLUINT32*      pdwTransactionID    Transaction ID
 *      PSLVOID*        pvData              Buffer to read data into
 *      PSLUINT32       cbMaxData           Size of the buffer
 *      PSLUINT32*      pcbRead             Amount of data read
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserReadData(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID, PSLVOID* pvData,
                    PSLUINT32 cbMaxData, PSLUINT32* pcbRead)
{
    PSLUINT32       cbRead;
    PSLUINT32       cbHeader;
    PSLUINT32       dwRunningCRC32;
    PSLSTATUS       ps;
    MTPBTPARSEROBJ* pmtpbt;
    MTPBTDATA*      pmtpbtData;

    /*  Clear results for safety
     */

    if (PSLNULL != pdwTransactionID)
    {
        *pdwTransactionID = 0;
    }
    if (PSLNULL != pcbRead)
    {
        *pcbRead = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpstrmParser) || ((0 < cbMaxData) && (NULL == pvData)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpbt = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbt->dwBTCookie);

    /*  Use the base class to actually send the data
     */

    ps = MTPStreamParserReadDataBase(mtpstrmParser, pdwTransactionID, pvData,
                            cbMaxData, &cbRead);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Grab the full header
     */

    ps = MTPStreamBufferMgrGetBufferHeader(pvData, &pmtpbtData, &cbHeader);
    if (PSL_FAILED(ps) || (sizeof(*pmtpbtData) != cbHeader))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  And validate the running CRC-32 prior to updating it
     */

    dwRunningCRC32 = MTPLoadUInt32(&pmtpbtData->dwRunningCRC32);
    if (dwRunningCRC32 != pmtpbt->dwCRC32)
    {
        PSLTraceError("\t\t************  MTP BT CRC Mismatch ************");
        PSLTraceError(
            "_MTPBTParserReadData: Running CRC32 Recv = 0x%08x\tCalc = 0x%08x",
                            dwRunningCRC32, pmtpbt->dwCRC32);
        ps = PSLERROR_DATA_CORRUPT;
        goto Exit;
    }
    else
    {
        PSLTraceProgress("_MTPBTParserReadData: Running CRC32 Recv = 0x%08x\t"
                            "Calc = 0x%08x", dwRunningCRC32, pmtpbt->dwCRC32);
    }

    /*  And calculate the CRC-32 over the data
     */

    pmtpbt->dwCRC32 = _CRCUpdate(pmtpbt->dwCRC32, pvData, cbRead);

    /*  Return the amount of data read if requested
     */

    if (PSLNULL != pcbRead)
    {
        *pcbRead = cbRead;
    }

Exit:
    return ps;
}


/*
 *  _MTPBTParserSendEndData
 *
 *  Sends the specified data packet and then sends the end data packet with
 *  the CRC-32 value.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to send data with
 *      PSLUINT32       dwTransactionID     Transaction ID for this packet
 *      PSL_CONST PSLVOID*
 *                      pvData              Data to send
 *      PSLUINT32       cbData              Length of data to send
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserSendEndData(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID, PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData)
{
    PSLUINT32       cbSent;
    MTPBTENDDATA    mtpbtEndData;
    PSLSTATUS       ps;
    MTPBTPARSEROBJ* pmtpbt;

    /*  Validate arguments
     */

    if ((NULL == mtpstrmParser) || ((0 < cbData) && (NULL == pvData)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpbt = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbt->dwBTCookie);

    /*  Send data if necessary
     */

    if (0 < cbData)
    {
        /*  Send the last data packet as an actual data packet rather than
         *  an End Data packet with associated data since MTP/BT specifies
         *  that the EndData packet contains only the CRC-32 if needed
         */

        ps = _MTPBTParserSendData(mtpstrmParser, dwTransactionID, pvData,
                            cbData);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Account for the fact that the data has already had the CRC-32
         *  calculated for it
         */

        pvData = PSLNULL;
        cbData = 0;
    }

    /*  Set the header information on the end data packet
     */

    ps = MTPStreamParserStoreHeader(mtpstrmParser, MTPSTREAMPACKET_END_DATA,
                            sizeof(mtpbtEndData), &(mtpbtEndData.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Calculate the final CRC-32
     */

    pmtpbt->dwCRC32 = _CRCFinal(pmtpbt->dwCRC32, pvData, cbData);
    PSLTraceProgress("_MTPBTParserSendEndData: CRC32 = 0x%08x",
                            pmtpbt->dwCRC32);

    /*  Add in the packet specific information
     */

    MTPStoreUInt32(&(mtpbtEndData.dwTransactionID), dwTransactionID);
    MTPStoreUInt32(&(mtpbtEndData.dwCRC32), pmtpbt->dwCRC32);

    /*  And send it over the network
     */

    ps = MTPStreamParserSend(mtpstrmParser, &mtpbtEndData, sizeof(mtpbtEndData),
                            &cbSent);
    if (PSL_FAILED(ps) || (sizeof(mtpbtEndData) != cbSent))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPBTParserReadEndData
 *
 *  Reads an end data packet from the specified parser and validates the
 *  CRC-32 value
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to read data with
 *      PSLUINT32*      pdwTransactionID    Transaction ID
 *      PSLVOID*        pvData              Buffer to read data into
 *      PSLUINT32       cbMaxData           Size of the buffer
 *      PSLUINT32*      pcbRead             Amount of data read
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPBTParserReadEndData(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID, PSLVOID* pvData,
                    PSLUINT32 cbMaxData, PSLUINT32* pcbRead)
{
    PSLUINT32       cbData;
    PSLUINT32       dwCRC32;
    MTPBTENDDATA    mtpbtEndData;
    PSLSTATUS       ps;
    MTPBTPARSEROBJ* pmtpbtPO = PSLNULL;

    /*  Clear results for safety
     */

    if (PSLNULL != pdwTransactionID)
    {
        *pdwTransactionID = 0;
    }
    if (PSLNULL != pcbRead)
    {
        *pcbRead = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpstrmParser) || ((0 < cbMaxData) && (PSLNULL == pvData)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may read this packet from the command socket
     */

    ps = MTPStreamParserCheckType(mtpstrmParser,
                            MTPSTREAMPARSERMODE_RESPONDER,
                            MTPSTREAMPARSERTYPE_COMMAND);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Standard operation requires that the header is already known before
     *  we get called
     */

    PSLASSERT(MTPSTREAMPACKET_END_DATA == pmtpbtPO->mtpstrmPO.dwTypeCur);
    if (MTPSTREAMPACKET_END_DATA != pmtpbtPO->mtpstrmPO.dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Make sure that the packet size matches
     */

    PSLASSERT(GET_PACKET_SIZE(mtpbtEndData) == pmtpbtPO->mtpstrmPO.cbAvailable);
    if (GET_PACKET_SIZE(mtpbtEndData) != pmtpbtPO->mtpstrmPO.cbAvailable)
    {
        /*  The size of the received packet is incorrect- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = MTPStreamParserRead(mtpstrmParser, GET_PACKET_START(mtpbtEndData),
                            pmtpbtPO->mtpstrmPO.cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet- since this should have been sent
     *  together by the source we will reject partial packets as invalid
     */

    if (GET_PACKET_SIZE(mtpbtEndData) != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Extract the CRC-32 and validate that all of the data sent correctly
     */

    dwCRC32 = MTPLoadUInt32(&(mtpbtEndData.dwCRC32));
    pmtpbtPO->dwCRC32 = _CRCFinal(pmtpbtPO->dwCRC32, NULL, 0);
    if (dwCRC32 != pmtpbtPO->dwCRC32)
    {
        PSLTraceError("\t\t************  MTP BT CRC Mismatch ************");
        PSLTraceError(
            "_MTPBTParserReadEndData: CRC32 Recv = 0x%08x\tCalc = 0x%08x",
                            dwCRC32, pmtpbtPO->dwCRC32);
        ps = PSLERROR_CONNECTION_CORRUPT;
        goto Exit;
    }
    else
    {
        PSLTraceProgress(
            "_MTPBTParserReadEndData: CRC32 Recv = 0x%08x\tCalc = 0x%08x",
                            dwCRC32, pmtpbtPO->dwCRC32);
    }

    /*  Read the transaction ID if necessary
     */

    if (PSLNULL != pdwTransactionID)
    {
        *pdwTransactionID = MTPLoadUInt32(&(mtpbtEndData.dwTransactionID));
    }
    PSLASSERT((PSLNULL == pcbRead) || (0 == *pcbRead));

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(mtpstrmParser);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPBTParserSendProbeResponse
 *
 *  Sends a probe response packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32       dwDeviceStatus      Device Status
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

static PSLSTATUS PSL_API _MTPBTParserSendProbeResponse(
                    MTPSTREAMPARSER mtpstrmParser, PSLUINT32 dwDeviceStatus)
{
    PSLSTATUS                   ps;
    PSLUINT32                   cbPacket;
    PSLUINT32                   cbSent;
    MTPSTREAMPARSEROBJ*         pmtpspo;
    MTPBTPARSEROBJ*             pmtpbtpo;
    MTPBTPROBERESPONSE          mtpbtprp;

    /*  Validate Argument
     */

    if (PSLNULL == mtpstrmParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure the parser is ready to go
     */

    pmtpspo = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpspo->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpspo->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may send this packet from the event socket
     */

    ps = MTPStreamParserCheckType(mtpstrmParser,
                            MTPSTREAMPARSERMODE_RESPONDER,
                            MTPSTREAMPARSERTYPE_EVENT);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps)? ps: PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpbtpo= (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtpo->dwBTCookie);

    /*  Set the header information on the probe response packet
     */

    cbPacket = sizeof(MTPBTPROBERESPONSE);
    ps = MTPStreamParserStoreHeader(mtpstrmParser,
                            MTPSTREAMPACKET_PROBE_RESPONSE,
                            cbPacket,&(mtpbtprp.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /* Add the device status to the probe response packet
     */

    MTPStoreUInt32(&(mtpbtprp.dwDeviceStatus), dwDeviceStatus);

    /*  And send it over the network
     */

    ps = MTPStreamParserSend(mtpstrmParser, &mtpbtprp, cbPacket, &cbSent);
    if (PSL_FAILED(ps) || (cbPacket != cbSent))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;

}


/*
 *  MTPBTParserCreate
 *
 *  Creates an instance of the MTP/BT parser.  Each parser is created in a
 *  specific mode (responder/initiator) and manages retrieving data off/
 *  placing it on the network interface.
 *
 *  Arguments:
 *      PSLUINT32       dwMode              Parser mode
 *      SOCKET          sock                Socket to bind
 *      PSLUINT32       dwType              Type of socket bound
 *      MTPBTPARSER*    pmtpbtParser        Return location for handle
 *
 *  Returns:
 *      PSLSTATUS   PSL_SUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserCreate(PSLUINT32 dwMode, SOCKET sock,
                    PSLUINT32 dwType,
                    MTPBTPARSER* pmtpbtParser)
{
    PSLSTATUS       ps;
    MTPBTPARSEROBJ* pmtpbtPO = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpbtParser)
    {
        *pmtpbtParser = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpbtParser) || (INVALID_SOCKET == sock))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate a new parser using the MTP Stream Parser to create
     *  and initialize the base object
     */

    ps = MTPStreamParserCreate(dwMode, sizeof(MTPBTPARSEROBJ),
                            (MTPSTREAMPARSER*)&pmtpbtPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Set the cookie so that we can validate this is one of our objects
     */

    pmtpbtPO->dwBTCookie = MTPBTPARSER_COOKIE;
#endif  /*  PSL_ASSERTS */

    /*  Set the proper VTBL
     */

    pmtpbtPO->mtpstrmPO.vtbl = &_VtblMTPBTParser;

    /*  Set the type to validate the parameter
     */

    ps = MTPStreamParserSetType(pmtpbtPO, dwType);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Remember the socket that we are bound to
     */

    pmtpbtPO->sock = sock;
    pmtpbtPO->wSequenceIDOut = 1;
    pmtpbtPO->dwCRC32 = _CRCInit();

#ifdef MTPBT_TRANSPORT_FAULT_INJECTION
    /*  Initialize the fault injection thresholds so that we start triggering
     *  errors on this transport
     */

    ps = PSLGenRandom(&(pmtpbtPO->cbFailInbound),
                            sizeof(pmtpbtPO->cbFailInbound));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pmtpbtPO->cbFailInbound &= MTPBT_FAULT_MAX_BYTES;

    ps = PSLGenRandom(&(pmtpbtPO->cbFailOutbound),
                            sizeof(pmtpbtPO->cbFailOutbound));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pmtpbtPO->cbFailOutbound &= MTPBT_FAULT_MAX_BYTES;
#endif  /* MTPBT_TRANSPORT_FAULT_INJECTION */

    /*  Mark the parser as ready
     */

    pmtpbtPO->mtpstrmPO.dwFlags |= MTPSTREAMPARSERFLAGS_PARSER_READY;

    /*  Return the parser handle
     */

    *pmtpbtParser = pmtpbtPO;
    pmtpbtPO = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPBTPARSERDESTROY(pmtpbtPO);
    return ps;
}


/*
 *  MTPBTParserSocketMsgSelect
 *
 *  Puts the socket associated with this parser in the requested asynchronous
 *  notification mode.
 *
 *  Arguments:
 *      MTPBTPARSER     mtpstrmParser       Parser to put into async mode
 *      PSLMSGQUEUE     mqDest              Destination to send event messages
 *      PSLMSGPOOL      mpDest              Destination message pool
 *      PSLPARAM        pvParam             Destination parameter
 *      PSLUINT32       dwSelectFlags       Event flags
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserSocketMsgSelect(MTPBTPARSER mtpstrmParser,
                    PSLMSGQUEUE mqDest, PSLMSGPOOL mpDest,
                    PSLPARAM pvParam, PSLUINT32 dwSelectFlags)
{
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == mqDest) ||
        (PSLNULL == mpDest))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  And determine whether or not we have a match
     */

    ps = MTPBTSocketMsgSelect(pmtpbtPO->sock, mqDest, mpDest, pvParam,
                            dwSelectFlags);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPBTParserSocketMsgReset
 *
 *  Resets the message select event state for the socket bound to this parser
 *
 *  Arguments:
 *      MTPBTPARSER     mtpstrmParser       Parser to reset
 *      PSLBOOL         fResetMask          PSLTRUE if data is an event mask
 *      PSLUINT32       dwData              Event mask or message to reset
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserSocketMsgReset(MTPBTPARSER mtpstrmParser,
                    PSLBOOL fResetMask, PSLUINT32 dwData)
{
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpstrmParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  And determine whether or not we have a match
     */

    ps = MTPBTSocketMsgReset(pmtpbtPO->sock, fResetMask, dwData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPBTParserReadInitCmdRequest
 *
 *  Reads an InitCommandRequest packet from the specified socket.
 *
 *  Arguments:
 *      MTPBTPARSER     mtpbtParser         Parser instance to use
 *      PSLGUID*        pguidLinkID         Return buffer for Link ID GUID
 *      PSLUINT16*      pwPSM               Return buffer for event PSM
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserReadInitCmdRequest(MTPBTPARSER mtpbtParser,
                    PSLGUID* pguidLinkID, PSLUINT16* pwPSM)
{
    PSLUINT32               cbData;
    MTPBTINITCOMMANDREQUEST mtpbtInitCmdReq;
    PSLSTATUS               ps;
    MTPBTPARSEROBJ*         pmtpbtPO;

    /*  Clear results for safety
     */

    if (PSLNULL != pguidLinkID)
    {
        PSLZeroMemory(pguidLinkID, sizeof(*pguidLinkID));
    }
    if (PSLNULL != pwPSM)
    {
        *pwPSM = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpbtParser) || (PSLNULL == pguidLinkID) ||
        (PSLNULL == pwPSM))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpbtParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may read this packet from the command socket
     */

    ps = MTPStreamParserCheckType(mtpbtParser,
                            MTPSTREAMPARSERMODE_RESPONDER,
                            MTPSTREAMPARSERTYPE_COMMAND);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Standard operation requires that the header is already known before
     *  we get called
     */

    PSLASSERT(MTPBTPACKET_INIT_COMMAND_REQUEST == pmtpbtPO->mtpstrmPO.dwTypeCur);
    if (MTPBTPACKET_INIT_COMMAND_REQUEST != pmtpbtPO->mtpstrmPO.dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Make sure that the packet size is correct
     */

    PSLASSERT(GET_PACKET_SIZE(mtpbtInitCmdReq) ==
                            pmtpbtPO->mtpstrmPO.cbAvailable);
    if (GET_PACKET_SIZE(mtpbtInitCmdReq) != pmtpbtPO->mtpstrmPO.cbAvailable)
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = MTPStreamParserRead(mtpbtParser, GET_PACKET_START(mtpbtInitCmdReq),
                            pmtpbtPO->mtpstrmPO.cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet- since this should have been sent
     *  together by the source we will reject partial packets as invalid
     */

    if (GET_PACKET_SIZE(mtpbtInitCmdReq) != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Extract the Link ID and the PSM
     */

    MTPLoadGUID(pguidLinkID, &(mtpbtInitCmdReq.guidLinkID));
    *pwPSM = MTPLoadUInt16(&(mtpbtInitCmdReq.wPSM));

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(pmtpbtPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPBTParserSendInitEventRequest
 *
 *  Sends an init event request from the initiator to the responder.
 *
 *  Arguments:
 *      MTPBTPARSER     mtpbtParser         Parser to use
 *      PSLGUIND*       pguidLinkID         LinkID for this connection
 *      PSLUINT16       wPSM                PSM for Initiator connection
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserSendInitEventRequest(MTPBTPARSER mtpbtParser,
                    PSL_CONST PSLGUID* pguidLinkID, PSLUINT16 wPSM)
{
    PSLUINT32               cbSent;
    MTPBTINITEVENTREQUEST   mtpbtInitEventReq;
    PSLSTATUS               ps;
    MTPBTPARSEROBJ*         pmtpbtPO;

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpbtParser) || (PSLNULL == pguidLinkID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpbtParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only initiators may sned this packet from the event socket
     */

    ps = MTPStreamParserCheckType(mtpbtParser,
                            MTPSTREAMPARSERMODE_RESPONDER,
                            MTPSTREAMPARSERTYPE_EVENT);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Initialize the header
     */

    ps = MTPStreamParserStoreHeader(mtpbtParser, MTPBTPACKET_INIT_EVENT_REQUEST,
                            sizeof(mtpbtInitEventReq),
                            &(mtpbtInitEventReq.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Add the packet data
     */

    MTPStoreGUID(&(mtpbtInitEventReq.guidLinkID), pguidLinkID);
    MTPStoreUInt16(&(mtpbtInitEventReq.wPSM), wPSM);

    /*  And send it
     */

    ps = MTPStreamParserSend(mtpbtParser, &mtpbtInitEventReq,
                            sizeof(mtpbtInitEventReq), &cbSent);
    if (PSL_FAILED(ps) || (sizeof(mtpbtInitEventReq) != cbSent))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPBTParserSendInitLinkACK
 *
 *  Sends an acknowledgement that the link is connected and ready to go.
 *
 *  Arguments:
 *      MTPBTPARSER     mtpbtParser         Parser to use
 *      PSLGUID*        pguidLinkID         Link ID for the connection
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserSendInitLinkACK(MTPBTPARSER mtpbtParser,
                    PSL_CONST PSLGUID* pguidLinkID)
{
    PSLUINT32           cbSent;
    MTPBTINITLINKACK    mtpbtLinkAck;
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpbtParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpbtParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may send this packet from the event socket
     */

    ps = MTPStreamParserCheckType(mtpbtParser,
                            MTPSTREAMPARSERMODE_RESPONDER,
                            MTPSTREAMPARSERTYPE_EVENT);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Fill out the header
     */

    ps = MTPStreamParserStoreHeader(mtpbtParser, MTPBTPACKET_INIT_LINK_ACK,
                            sizeof(mtpbtLinkAck), &(mtpbtLinkAck.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Store the packet specific data
     */

    MTPStoreGUID(&(mtpbtLinkAck.guidLinkID), pguidLinkID);

    /*  And send it over the network
     */

    ps = MTPStreamParserSend(mtpbtParser, &mtpbtLinkAck, sizeof(mtpbtLinkAck),
                            &cbSent);
    if (PSL_FAILED(ps) || (sizeof(mtpbtLinkAck) != cbSent))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPBTParserReadInitLinkACK
 *
 *  Reads the init link acknowledgement
 *
 *  Arguments:
 *      MTPBTPARSER     mtpbtParser         Parser to use
 *      PSLGUID*        pguidLinkID         ID of the link established
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserReadInitLinkACK(MTPBTPARSER mtpbtParser,
                    PSLGUID* pguidLinkID)
{
    PSLUINT32           cbData;
    MTPBTINITLINKACK    mtpbtLinkAck;
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;

    /*  Clear results for safety
     */

    if (PSLNULL != pguidLinkID)
    {
        PSLZeroMemory(pguidLinkID, sizeof(*pguidLinkID));
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpbtParser) || (PSLNULL == pguidLinkID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpbtParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Standard operation model requires that the header has already been
     *  read.
     */

    ASSERT(MTPBTPACKET_INIT_LINK_ACK == pmtpbtPO->mtpstrmPO.dwTypeCur);
    if (MTPBTPACKET_INIT_LINK_ACK != pmtpbtPO->mtpstrmPO.dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Make sure that packet size is correct
     */

    PSLASSERT(GET_PACKET_SIZE(mtpbtLinkAck) == pmtpbtPO->mtpstrmPO.cbAvailable);
    if (GET_PACKET_SIZE(mtpbtLinkAck) != pmtpbtPO->mtpstrmPO.cbAvailable)
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = MTPStreamParserRead(mtpbtParser, GET_PACKET_START(mtpbtLinkAck),
                            pmtpbtPO->mtpstrmPO.cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet- since this should have been sent
     *  together by the source we will reject partial packets as invalid
     */

    if (GET_PACKET_SIZE(mtpbtLinkAck) != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Extract the Link ID
     */

    MTPLoadGUID(pguidLinkID, &(mtpbtLinkAck.guidLinkID));

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(pmtpbtPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPBTParaserSendInitFail
 *
 *  Sends a initialization failed notification to the destination socket.
 *
 *  Arguments:
 *      MTPBTPARSPER    mtpbtParser         Parser to use
 *      PSLUINT32       dwReason            Reason for failure
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserSendInitFail(MTPBTPARSER mtpbtParser,
                    PSLUINT32 dwReason)
{
    PSLUINT32       cbSent;
    MTPBTINITFAIL   mtpbtInitFail;
    PSLSTATUS       ps;
    MTPBTPARSEROBJ* pmtpbtPO;

    /*  Validate Arguments- failure can be sent on either the command or
     *  event socket.
     */

    if (PSLNULL == mtpbtParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Stop the system so that we can figure out why we failed
     */

    PSLTraceError("MTPBTParserSendInitFail: Failing connection- Reason = 0x%08x",
                            dwReason);
    PSLASSERT(PSLFALSE);

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpbtParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may send this packet- note there is no requirement
     *  that the socket be correctly typed before this packet may be sent
     */

    ps = MTPStreamParserCheckType(mtpbtParser,
                                MTPSTREAMPARSERMODE_RESPONDER,
                                MTPSTREAMPARSERTYPE_UNKNOWN);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Fill out the header
     */

    ps = MTPStreamParserStoreHeader(mtpbtParser, MTPBTPACKET_INIT_LINK_FAIL,
                            sizeof(mtpbtInitFail),
                            &(mtpbtInitFail.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Add in the result code
     */

    MTPStoreUInt32(&(mtpbtInitFail.dwReason), dwReason);

    /*  And send it over the network
     */

    ps = MTPStreamParserSend(mtpbtParser, &mtpbtInitFail,
                            sizeof(mtpbtInitFail), &cbSent);
    if (PSL_FAILED(ps) || (sizeof(mtpbtInitFail) != cbSent))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPBTParserReadInitFail
 *
 *  Reads an initialization failure message from the specified socket.
 *
 *  Arguments:
 *      MTPBTPARSER     mtpbtParser         Parser to use
 *      PSLUINT32       dwSocketSrc         Socket to read from
 *      PSLUINT32*      pdwReason           Reason for failure
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserReadInitFail(MTPBTPARSER mtpbtParser,
                    PSLUINT32* pdwReason)
{
    PSLUINT32           cbData;
    MTPBTINITFAIL       mtpbtInitFail;
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwReason)
    {
        *pdwReason = MTPBT_FAIL_UNSPECIFIED;
    }

    /*  Validate Arguments- failure can be sent on either the command or
     *  event socket.
     */

    if ((PSLNULL == mtpbtParser) || (PSLNULL == pdwReason))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpbtParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Make sure that packet size os correct
     */

    PSLASSERT(GET_PACKET_SIZE(mtpbtInitFail) == pmtpbtPO->mtpstrmPO.cbAvailable);
    if (GET_PACKET_SIZE(mtpbtInitFail) != pmtpbtPO->mtpstrmPO.cbAvailable)
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = MTPStreamParserRead(mtpbtParser, GET_PACKET_START(mtpbtInitFail),
                            pmtpbtPO->mtpstrmPO.cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet- since this should have been sent
     *  together by the source we will reject partial packets as invalid
     */

    if (GET_PACKET_SIZE(mtpbtInitFail) != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Extract the connection ID out of the entry
     */

    *pdwReason = MTPLoadUInt32(&(mtpbtInitFail.dwReason));

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(pmtpbtPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPBTParaserSendTransportError
 *
 *  Sends a transport error to the destination socket.
 *
 *  Arguments:
 *      MTPBTPARSPER    mtpbtParser         Parser to use
 *      PSLUINT32       dwTransactionID     Transaction ID
 *      PSLGUID*        pguidLinkID         Link ID
 *      PSLUINT32       dwReason            Reason for failure
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserSendTransportError(MTPBTPARSER mtpbtParser,
                    PSLUINT32 dwTransactionID, PSL_CONST PSLGUID* pguidLinkID,
                    PSLUINT32 dwReason)
{
    PSLUINT32           cbSent;
    MTPBTTRANSPORTERROR mtpbtTransportErr;
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpbtParser) || (PSLNULL == pguidLinkID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpbtParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Fill out the header
     */

    ps = MTPStreamParserStoreHeader(mtpbtParser,
                            MTPBTPACKET_TRANSPORT_ERROR,
                            sizeof(mtpbtTransportErr),
                            &(mtpbtTransportErr.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Add in the packet information
     */

    MTPStoreUInt32(&(mtpbtTransportErr.dwTransactionID), dwTransactionID);
    MTPStoreGUID(&(mtpbtTransportErr.guidLinkID), pguidLinkID);
    MTPStoreUInt32(&(mtpbtTransportErr.dwReason), dwReason);

    /*  And send it over the network
     */

    ps = MTPStreamParserSend(mtpbtParser, &mtpbtTransportErr,
                            sizeof(mtpbtTransportErr), &cbSent);
    if (PSL_FAILED(ps) || (sizeof(mtpbtTransportErr) != cbSent))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPBTParserReadTransportError
 *
 *  Reads an initialization failure message from the specified socket.
 *
 *  Arguments:
 *      MTPBTPARSER     mtpbtParser         Parser to use
 *      PSLUINT32*      pdwTransactionID    TransactionID
 *      PSLGUID*        pguidLinkID         Link ID
 *      PSLUINT32*      pdwReason           Reason for failure
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserReadTransportError(MTPBTPARSER mtpbtParser,
                    PSLUINT32* pdwTransactionID, PSLGUID* pguidLinkID,
                    PSLUINT32* pdwReason)
{
    PSLUINT32           cbData;
    MTPBTTRANSPORTERROR mtpbtTransportErr;
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwTransactionID)
    {
        *pdwTransactionID = 0;
    }
    if (PSLNULL != pguidLinkID)
    {
        PSLZeroMemory(pguidLinkID, sizeof(*pguidLinkID));
    }
    if (PSLNULL != pdwReason)
    {
        *pdwReason = MTPBT_FAIL_UNSPECIFIED;
    }

    /*  Validate Arguments- failure can be sent on either the command or
     *  event socket.
     */

    if ((PSLNULL == mtpbtParser) || (PSLNULL == pguidLinkID) ||
        (PSLNULL == pdwReason))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpbtParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Make sure that packet size os correct
     */

    PSLASSERT(GET_PACKET_SIZE(mtpbtTransportErr) ==
                            pmtpbtPO->mtpstrmPO.cbAvailable);
    if (GET_PACKET_SIZE(mtpbtTransportErr) != pmtpbtPO->mtpstrmPO.cbAvailable)
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = MTPStreamParserRead(mtpbtParser, GET_PACKET_START(mtpbtTransportErr),
                            pmtpbtPO->mtpstrmPO.cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet- since this should have been sent
     *  together by the source we will reject partial packets as invalid
     */

    if (GET_PACKET_SIZE(mtpbtTransportErr) != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Extract the information from the packet
     */

    if (PSLNULL != pdwTransactionID)
    {
        *pdwTransactionID = MTPLoadUInt32(&(mtpbtTransportErr.dwReason));
    }
    MTPLoadGUID(pguidLinkID, &(mtpbtTransportErr.guidLinkID));
    *pdwReason = MTPLoadUInt32(&(mtpbtTransportErr.dwReason));

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(pmtpbtPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPBTParaserSendTransportReady
 *
 *  Sends a transport ready to the destination socket.
 *
 *  Arguments:
 *      MTPBTPARSPER    mtpbtParser         Parser to use
 *      PSLGUID*        pguidLinkID         Link ID
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserSendTransportReady(MTPBTPARSER mtpbtParser,
                    PSL_CONST PSLGUID* pguidLinkID)
{
    PSLUINT32           cbSent;
    MTPBTTRANSPORTREADY mtpbtTransportRdy;
    PSLSTATUS           ps;
    MTPBTPARSEROBJ*     pmtpbtPO;

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpbtParser) || (PSLNULL == pguidLinkID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpbtParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Fill out the header
     */

    ps = MTPStreamParserStoreHeader(mtpbtParser,
                            MTPBTPACKET_TRANSPORT_READY,
                            sizeof(mtpbtTransportRdy),
                            &(mtpbtTransportRdy.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Add in the packet information
     */

    MTPStoreGUID(&(mtpbtTransportRdy.guidLinkID), pguidLinkID);

    /*  And send it over the network
     */

    ps = MTPStreamParserSend(mtpbtParser, &mtpbtTransportRdy,
                            sizeof(mtpbtTransportRdy), &cbSent);
    if (PSL_FAILED(ps) || (sizeof(mtpbtTransportRdy) != cbSent))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPBTParserReadTransportReady
 *
 *  Reads all available bytes from a stream looking for a transport ready
 *  packet.
 *
 *  Arguments:
 *      MTPBTPARSER     mtpbtParser         Parser to use
 *      MTPSTREAMBUFFERMGR
 *                      mtpstrmbm           Buffer manager to use
 *      PSLGUID*        pguidLinkID         Link ID to match
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPBTParserReadTransportReady(MTPBTPARSER mtpbtParser,
                    MTPSTREAMBUFFERMGR mtpstrmbm,
                    PSL_CONST PSLGUID* pguidLinkID)
{
    PSLUINT32           cbActual;
    PSLUINT32           cbAvailable;
    PSLUINT32           cbData;
    PSLUINT32           cbStore;
    PSLGUID             guidCheck;
    PSLSTATUS           ps;
    PSLBYTE*            pbStore;
    PSLVOID*            pvBuffer = PSLNULL;
    MTPBTPARSEROBJ*     pmtpbtPO;

    /*  Validate Arguments- ready can be read on either the command or
     *  event socket.
     */

    if ((PSLNULL == mtpbtParser) || (PSLNULL == mtpstrmbm) ||
        (PSLNULL == pguidLinkID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpbtParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Check to see if we have data available on the socket in the first place
     */

    ps = MTPStreamParserGetBytesAvailable(mtpbtParser, &cbAvailable);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    if (0 == cbAvailable)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Retrieve a flush buffer from the buffer manager provided so that we
     *  have a place to dump the data
     */

    ps = MTPStreamBufferMgrRequest(mtpstrmbm, MTPSTREAMBUFFER_FLUSH_DATA,
                            MTPSTREAM_FLUSH_BUFFER_SIZE, PSLNULL,
                            &pvBuffer, &cbActual, PSLNULL, PSLNULL);

    /*  We should get a buffer back synchronously FLUSH_DATA buffer should
     *  always be available
     */

    if (PSLSUCCESS != ps)
    {
        PSLASSERT(PSL_FAILED(ps));
        ps = PSL_FAILED(ps) ? ps : PSLERROR_UNEXPECTED;
        goto Exit;
    }
    PSLASSERT(PSLNULL != pvBuffer);
    PSLASSERT(MTPSTREAM_FLUSH_BUFFER_SIZE == cbActual);

    /*  Retrieve all of the available data on the sockee
     */

    while (0 < cbAvailable)
    {
        /*  Retrieve as much of the buffer as we can
         */

        ps = MTPStreamParserRead(mtpbtParser, pvBuffer,
                            min(cbActual, cbAvailable), &cbData);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  See if we need to stash any of the data from this read to check
         *  for a transport ready packet.
         *
         *  NOTE:
         *  We take advantage of the fact that all traffic on the channel
         *  is halted after the transport ready packet is sent.  Because we
         *  know it is the last thing we can just keep checking the last
         *  sizeof(MTPBTRANSPORTREADY) bytes of each data block for a match.
         *  In addition we have to be ready to deal with the fact that we may
         *  get only a portion of the packet in the current buffer which is why
         *  there is a somewhat elaborate scheme here for caching potential
         *  matches
         */

        if ((cbAvailable - cbData) < sizeof(pmtpbtPO->mtptr))
        {
            /*  Figure out how much we want to shift into the "accumulator"
             *  and where we need to store it
             */

            cbStore = sizeof(pmtpbtPO->mtptr) - (cbAvailable - cbData);
            pbStore = (PSLBYTE*)&(pmtpbtPO->mtptr) + sizeof(pmtpbtPO->mtptr) -
                            cbStore;

            /*  Make space for it
             */

            PSLMoveMemory(&(pmtpbtPO->mtptr),
                            (PSLBYTE*)&(pmtpbtPO->mtptr) + cbStore,
                            pbStore - (PSLBYTE*)&(pmtpbtPO->mtptr));

            /*  And shift the new data in
             */

            PSLCopyMemory(pbStore, (PSLBYTE*)pvBuffer + cbData - cbStore,
                            cbStore);
        }

        /*  Figure out how much data is still pending on the socket
         */

        cbAvailable -= cbData;
    }

    /*  At this point we want to check the transport ready accumulator to see
     *  if we have actually received a valid packet- we check the following
     *  fields here:
     *      1. Packet size
     *      2. Packet Type
     *      3. Transport ID
     */

    if ((sizeof(pmtpbtPO->mtptr) != MTPLoadUInt32(
                            &(pmtpbtPO->mtptr.mtpstrmHeader.cbPacket))) ||
        (MTPBTPACKETID_TRANSPORT_READY !=
                            pmtpbtPO->mtptr.mtpstrmHeader.bPacketID) ||
        (MTPBT_TRANSPORT_ID != pmtpbtPO->mtptr.mtpstrmHeader.bTransportID))
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  We have what could be a valid packet- perform the final check on the
     *  GUID
     */

    MTPLoadGUID(&guidCheck, &(pmtpbtPO->mtptr.guidLinkID));
    if (0 != PSLCompareMemory(&guidCheck, pguidLinkID, sizeof(guidCheck)))
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  We have encountered a transport ready packet- go ahead and reset the
     *  parser so that we can resume normal operation
     */

    ps = MTPStreamParserReset(pmtpbtPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Reset the sequence ID based on the current value in the transport
     *  ready.  Also clear the CRC-32 accumulator for safety
     */

    pmtpbtPO->wSequenceIDIn = MTPLoadUInt16(
                            &(pmtpbtPO->mtptr.mtpstrmHeader.wTransportData));
    pmtpbtPO->dwCRC32 = _CRCInit();

    /*  Make sure the accumulator is empty to avoid false positives if we have
     *  to handle this case again
     */

    PSLZeroMemory(&(pmtpbtPO->mtptr), sizeof(pmtpbtPO->mtptr));
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);
    return ps;
}


/*
 *  MTPBTParaserSendRequestLinkDestroy
 *
 *  Sends a disconnection request to the initiator
 *
 *  Arguments:
 *      MTPBTPARSPER    mtpbtParser         Parser to use
 *      PSLGUID         pguidLinkID         Link ID to disconnect
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPBTParserSendRequestLinkDestroy(MTPBTPARSER mtpbtParser,
                    PSL_CONST PSLGUID* pguidLinkID)
{
    PSLUINT32               cbSent;
    MTPBTREQUESTLINKDESTROY mtpbtDisconnect;
    PSLSTATUS               ps;
    MTPBTPARSEROBJ*         pmtpbtPO;

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpbtParser) || (PSLNULL == pguidLinkID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpbtPO = (MTPBTPARSEROBJ*)mtpbtParser;
    PSLASSERT(MTPBTPARSER_COOKIE == pmtpbtPO->dwBTCookie);

    /*  Make sure we have a valid parser with a bound socket
     */

    PSLASSERT(INVALID_SOCKET != pmtpbtPO->sock);
    if (INVALID_SOCKET == pmtpbtPO->sock)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Fill out the header
     */

    ps = MTPStreamParserStoreHeader(mtpbtParser,
                            MTPBTPACKET_REQUEST_LINK_DESTROY,
                            sizeof(mtpbtDisconnect),
                            &(mtpbtDisconnect.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Add in the packet information
     */

    MTPStoreGUID(&(mtpbtDisconnect.guidLinkID), pguidLinkID);

    /*  And send it over the network
     */

    ps = MTPStreamParserSend(mtpbtParser, &mtpbtDisconnect,
                            sizeof(mtpbtDisconnect), &cbSent);
    if (PSL_FAILED(ps) || (sizeof(mtpbtDisconnect) != cbSent))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}



