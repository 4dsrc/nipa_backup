/*
 *  MTPBTPackets.h
 *
 *  Contains declaration of the different MTP/BT packets.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPBTPACKETS_H_
#define _MTPBTPACKETS_H_

#define MTPBT_TRANSPORT_ID                          0x01

/*  MTP/BT Packet IDs
 *
 *  Type enumeration for MTP/BT packets
 */

enum
{
    MTPBTPACKETID_UNKNOWN =                         0x00,
    MTPBTPACKETID_INIT_LINK_FAIL =                  0x05,
    MTPBTPACKETID_OPERATION_REQUEST =               0x06,
    MTPBTPACKETID_OPERATION_RESPONSE =              0x07,
    MTPBTPACKETID_EVENT =                           0x08,
    MTPBTPACKETID_START_DATA =                      0x09,
    MTPBTPACKETID_CANCEL =                          0x0b,
    MTPBTPACKETID_PROBE_REQUEST =                   0x0d,
    MTPBTPACKETID_PROBE_RESPONSE =                  0x0e,
    MTPBTPACKETID_INIT_COMMAND_REQUEST =            0x11,
    MTPBTPACKETID_INIT_EVENT_REQUEST =              0x12,
    MTPBTPACKETID_INIT_LINK_ACK =                   0x13,
    MTPBTPACKETID_END_DATA =                        0x14,
    MTPBTPACKETID_TRANSPORT_ERROR =                 0x15,
    MTPBTPACKETID_TRANSPORT_READY =                 0x16,
    MTPBTPACKETID_REQUEST_LINK_DESTROY =            0x17,
    MTPBTPACKETID_DATA =                            0x18,
};


#ifdef PSL_PACK_PRAGMA
#include "PSLPackPush1.h"
#endif

/*  MTPBTINITCOMMANDREQUEST
 *
 *  MTP/BT Init Command Request Packet
 */

struct _MTPBTINITCOMMANDREQUEST
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLGUID                 guidLinkID;
    PSLUINT16               wPSM;
} PSL_PACK1;

typedef struct _MTPBTINITCOMMANDREQUEST MTPBTINITCOMMANDREQUEST;

typedef struct _MTPBTINITCOMMANDREQUEST PSL_UNALIGNED*
                            PXMTPBTINITCOMMANDREQUEST;
typedef PSL_CONST struct _MTPBTINITCOMMANDREQUEST PSL_UNALIGNED*
                            PCXMTPBTINITCOMMANDREQUEST;


/*  MTPBTINITEVENTREQUEST
 *
 *  MTP/BT Init Event Request Packet
 */

struct _MTPBTINITEVENTREQUEST
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLGUID                 guidLinkID;
    PSLUINT16               wPSM;
} PSL_PACK1;

typedef struct _MTPBTINITEVENTREQUEST MTPBTINITEVENTREQUEST;

typedef struct _MTPBTINITEVENTREQUEST PSL_UNALIGNED* PXMTPBTINITEVENTREQUEST;
typedef PSL_CONST struct _MTPBTINITEVENTREQUEST PSL_UNALIGNED*
                                PCXMTPBTINITEVENTREQUEST;

/*  MTPBTINITLINKACK
 *
 *  MTP/BT Init Event Acknowledgement Packet
 */

struct _MTPBTINITLINKACK
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLGUID                 guidLinkID;
} PSL_PACK1;

typedef struct _MTPBTINITLINKACK MTPBTINITLINKACK;

typedef struct _MTPBTINITLINKACK PSL_UNALIGNED* PXMTPBTINITLINKACK;
typedef PSL_CONST struct _MTPBTINITLINKACK PSL_UNALIGNED*
                                PCXMTPBTINITLINKACK;

/*  MTPBTINITFAIL
 *
 *  MTP/BT Init Fail Packet
 */

struct _MTPBTINITFAIL
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwReason;
} PSL_PACK1;

typedef struct _MTPBTINITFAIL MTPBTINITFAIL;

typedef struct _MTPBTINITFAIL PSL_UNALIGNED* PXMTPBTINITFAIL;
typedef PSL_CONST struct _MTPBTINITFAIL PSL_UNALIGNED* PCXMTPBTINITFAIL;


/*  MTPBTDATA
 *
 *  MTP/BT Data Packet
 */

struct _MTPBTDATA
{
    MTPSTREAMDATA           mtpstrmData;
    PSLUINT32               dwRunningCRC32;
} PSL_PACK1;

typedef struct _MTPBTDATA MTPBTDATA;

typedef struct _MTPBTDATA PSL_UNALIGNED* PXMTPBTDATA;
typedef PSL_CONST struct _MTPBTDATA PSL_UNALIGNED* PCXMTPBTDATA;


/*  MTPBTENDDATA
 *
 *  MTP/BT End Data Packet
 */

struct _MTPBTENDDATA
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwTransactionID;
    PSLUINT32               dwCRC32;
} PSL_PACK1;

typedef struct _MTPBTENDDATA MTPBTENDDATA;

typedef struct _MTPBTENDDATA PSL_UNALIGNED* PXMTPBTENDDATA;
typedef PSL_CONST struct _MTPBTENDDATA PSL_UNALIGNED* PCXMTPBTENDDATA;


/*  MTPBTPROBERESPONSE
 *
 *  MTP/BT Probe Response Packet
 */

struct _MTPBTPROBERESPONSE
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwDeviceStatus;
} PSL_PACK1;

typedef struct _MTPBTPROBERESPONSE MTPBTPROBERESPONSE;

typedef struct _MTPBTPROBERESPONSE PSL_UNALIGNED* PXMTPBTPROBERESPONSE;
typedef PSL_CONST struct _MTPBTPROBERESPONSE PSL_UNALIGNED*
                            PCXMTPBTPROBERESPONSE;


/*  MTPBTTRANSPORTERROR
 *
 *  MTP/BT Transport Error Packet
 */

struct _MTPBTTRANSPORTERROR
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwTransactionID;
    PSLGUID                 guidLinkID;
    PSLUINT32               dwReason;
} PSL_PACK1;

typedef struct _MTPBTTRANSPORTERROR MTPBTTRANSPORTERROR;

typedef struct _MTPBTTRANSPORTERROR PSL_UNALIGNED* PXMTPBTTRANSPORTERROR;
typedef PSL_CONST struct _MTPBTTRANSPORTERROR PSL_UNALIGNED*
                            PCXMTPBTTRANSPORTERROR;


/*  MTPBTTRANSPORTREADY
 *
 *  MTP/BT Transport Ready Packet
 */

struct _MTPBTTRANSPORTREADY
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLGUID                 guidLinkID;
} PSL_PACK1;

typedef struct _MTPBTTRANSPORTREADY MTPBTTRANSPORTREADY;

typedef struct _MTPBTTRANSPORTREADY PSL_UNALIGNED* PXMTPBTTRANSPORTREADY;
typedef PSL_CONST struct _MTPBTTRANSPORTREADY PSL_UNALIGNED*
                            PCXMTPBTTRANSPORTREADY;


/*  MTPBTREQUESTLINKDESTROY
 *
 *  MTP/BT Request Link Destroy
 *
 */

struct _MTPBTREQUESTLINKDESTROY
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLGUID                 guidLinkID;
} PSL_PACK1;

typedef struct _MTPBTREQUESTLINKDESTROY MTPBTREQUESTLINKDESTROY;

typedef struct _MTPBTREQUESTLINKDESTROY PSL_UNALIGNED*
                            PXMTPBTREQUESTLINKDESTROY;
typedef PSL_CONST struct _MTPBTREQUESTLINKDESTROY PSL_UNALIGNED*
                            PCXMTPBTREQUESTLINKDESTROY;

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPop.h"
#endif

#endif  /* _MTPBTPACKETS_H_ */

