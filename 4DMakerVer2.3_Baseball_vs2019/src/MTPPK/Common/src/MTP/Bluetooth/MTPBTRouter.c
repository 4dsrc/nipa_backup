/*
 *  MTPBTRouter.c
 *
 *  Contains definitions of the MTP/BT Router functionality.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPBTTransportPrecomp.h"
#include "MTPStreamPackets.h"

/*  Local Defines
 */

#define MTPBTROUTEROBJ_COOKIE       'mbtR'

#define MTPBTROUTER_POOL_SIZE       16

#define MTPBTTRANSPORT_COMMAND_READY    0x01
#define MTPBTTRANSPORT_EVENT_READY      0x02

#define MTPBTTRANSPORT_READY \
        (MTPBTTRANSPORT_COMMAND_READY | MTPBTTRANSPORT_EVENT_READY)

/*  MTP/BT Router Messages
 *
 *  Messages defined for use by the MTP/BT internal message queues.
 */

#define MAKE_MTPBTROUTER_MSGID(_id, _flags) \
    MAKE_PSL_MSGID(MTP_TRANSPORT_PRIVATE, (_id), (_flags))

enum
{
    MTPBTROUTERMSG_TERMINATE =              MAKE_MTPBTROUTER_MSGID(1, MTPF_P)
};


/*  Local Types
 */

typedef struct _MTPBTROUTEROBJ
{
    MTPSTREAMROUTEROBJ              mtpstrmRO;
#ifdef PSL_ASSERTS
    PSLUINT32                       dwBTCookie;
#endif  /* PSL_ASSERTS */
    PSLPARAM                        aParamTransport;
    PSLGUID                         guidLink;
} MTPBTROUTEROBJ;


/*  Local Functions
 */

static PSLSTATUS _MTPBTRouterDestroy(MTPBTROUTER mtpbt);

static PSLSTATUS PSL_API _MTPBTRouterCreateContext(MTPBTROUTER mtpbt,
                    MTPCONTEXT** ppmtpctx);

static PSLSTATUS PSL_API _MTPBTRouterRoutePacketReceived(MTPSTREAMROUTER mtpbt,
                    MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc,
                    PSLUINT32 dwPacketType,
                    PSLUINT32 cbPacket);

static PSLSTATUS PSL_API _MTPBTRouterResetDataAvailable(MTPBTROUTER mtpbt,
                    MTPSTREAMPARSER mtpstrm);

static PSLSTATUS _MTPBTRouterCreate(MTPBTROUTER* pmtpbtRouter);

static PSLSTATUS _MTPBTRouterBindInitiator(MTPBTROUTER mtpbt,
                    SOCKET sock,
                    MTPCONTEXT** ppmtpctx);

static PSLSTATUS _MTPBTRouterHandleTransportError(MTPCONTEXT* pmtpctx,
                    MTPBTPARSER mtpbtParser,
                    PSLUINT32 dwReason);


/*  Local Data
 */

static MTPSTREAMROUTERVTBL  _VtblMTPBTRouter =
{
    _MTPBTRouterDestroy,
    _MTPBTRouterCreateContext,
    _MTPBTRouterRoutePacketReceived,
    _MTPBTRouterResetDataAvailable
};


/*  _MTPBTRouterDestroy
 *
 *  Destroys the specified MTP/BT Router object
 *
 *  Arguments:
 *      MTPBTROUTER     mtpbt               Router object to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPBTRouterDestroy(MTPBTROUTER mtpbt)
{
    PSLSTATUS           ps;
    MTPBTROUTEROBJ*     pmtpbt;

    /*  Validate arguments
     */

    if (PSLNULL == mtpbt)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack our object
     */

    pmtpbt = (MTPBTROUTEROBJ*)mtpbt;
    PSLASSERT(MTPBTROUTEROBJ_COOKIE == pmtpbt->dwBTCookie);

    /*  Send a notification that the transport is shutting down if necessary
     */

    CHECK_MTPTRANSPORTNOTIFY(MTPTRANSPORTEVENT_CLOSED, pmtpbt->aParamTransport);

    /*  Call the base implementation to do the rest of the work
     */

    ps = MTPStreamRouterDestroyBase(mtpbt);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPBTRouterCreateContext
 *
 *  Creates a route after completing initialization of the transport
 *  information
 *
 *  Arguments:
 *      MTPBTROUTER     mtpbt               MTP/BT router information
 *      MTPCONTEXT**    ppmtpctx            Resulting MTP context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPBTRouterCreateContext(MTPBTROUTER mtpbt, MTPCONTEXT** ppmtpctx)
{
    PSLSTATUS           ps;
    MTPBTROUTEROBJ*     pmtpbt;
    MTPBTPARSERINFO     mtpbtiCommand;
    MTPBTPARSERINFO     mtpbtiEvent;

    /*  Clear result for safety
     */

    if (PSLNULL != ppmtpctx)
    {
        *ppmtpctx = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpbt) || (PSLNULL == ppmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack our object
     */

    pmtpbt = (MTPBTROUTEROBJ*)mtpbt;
    PSLASSERT(MTPBTROUTEROBJ_COOKIE == pmtpbt->dwBTCookie);

    /*  Extract the transport information
     */

    if ((PSLNULL == pmtpbt->mtpstrmRO.mtpstrmCommand) ||
        (PSLNULL == pmtpbt->mtpstrmRO.mtpstrmEvent))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Retrieve details about this connection from the parser
     */

    ps = MTPStreamParserGetParserInfo(pmtpbt->mtpstrmRO.mtpstrmCommand,
                            &mtpbtiCommand, sizeof(mtpbtiCommand));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = MTPStreamParserGetParserInfo(pmtpbt->mtpstrmRO.mtpstrmEvent,
                            &mtpbtiEvent, sizeof(mtpbtiEvent));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And create a buffer set for this connection
     */

    SAFE_MTPSTREAMBUFFERMGRDESTROY(pmtpbt->mtpstrmRO.mtpstrmbm);
    ps = MTPStreamBufferMgrCreate(PSLGetAlignment(),
                    mtpbtiCommand.cbMaxSend, mtpbtiCommand.cbMaxReceive,
                    mtpbtiEvent.cbMaxSend, mtpbtiEvent.cbMaxReceive,
                    sizeof(MTPBTDATA), &(pmtpbt->mtpstrmRO.mtpstrmbm));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Allow the base class to complete the initialization
     */

    ps = MTPStreamRouterCreateContextBase(mtpbt, ppmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPBTRouterRoutePacketReceived
 *
 *  Routes a received packet to any subclasses of the general stream router
 *  class.  If a class handles the packet specified it returns PSLSUCCESS,
 *  if it does not it return PSLSUCCESS_FALSE.
 *
 *  Arguments:
 *      MTPSTREAMROUTER mtpbt               Router being used
 *      MTPCONTEXT*     pmtpctx             Context in which packet was received
 *      MTPSTREAMPARSER mtpstrmSrc          Parser on which the packet was
 *                                            received
 *      PSLUINT32       dwPacketType        Type of packet received
 *      PSLUINT32       cbPacket            Size of the packet received
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if handled, PSLSUCCESS_FALSE if not
 *
 */

PSLSTATUS PSL_API _MTPBTRouterRoutePacketReceived(MTPSTREAMROUTER mtpbt,
                    MTPCONTEXT* pmtpctx, MTPSTREAMPARSER mtpstrmSrc,
                    PSLUINT32 dwPacketType, PSLUINT32 cbPacket)
{
    PSLSTATUS       ps;
    MTPBTROUTEROBJ* pmtpbt;

    /*  Validate arguments
     */

    if ((PSLNULL == mtpbt) || (PSLNULL == pmtpctx) || (PSLNULL == mtpstrmSrc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack our object
     */

    pmtpbt = (MTPBTROUTEROBJ*)mtpbt;
    PSLASSERT(MTPBTROUTEROBJ_COOKIE == pmtpbt->dwBTCookie);

    /*  Look for packets that we support
     */

    switch (dwPacketType)
    {
    case MTPBTPACKET_TRANSPORT_ERROR:
        /*  Handle the transport error packet
         */

        ps = _MTPBTRouterHandleTransportError(pmtpctx, mtpstrmSrc,
                            MTPBT_TRANSPORT_FAIL_INVALID_DATA);
        if (PSLSUCCESS != ps)
        {
            PSLASSERT(PSL_FAILED(ps));
            ps = PSL_FAILED(ps) ? ps : PSLERROR_UNEXPECTED;
            goto Exit;
        }
        break;

    default:
        /*  Let the standard processing work for all of the other messages
         */

        ps = PSLSUCCESS_FALSE;
        break;
    }

Exit:
    return ps;
    cbPacket;
}


/*  _MTPBTRouterResetDataAvailable
 *
 *  Arguments:
 *      MTPBTROUTER     mtpbt               Router object associated with parser
 *      MTPSTREAMPARSER mtpstrm             Parser to reset
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPBTRouterResetDataAvailable(MTPBTROUTER mtpbt,
                    MTPSTREAMPARSER mtpstrm)
{
    PSLSTATUS           ps;

    /*  Validate arguments
     */

    if (PSLNULL == mtpbt)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Validate our object
     */

    PSLASSERT(MTPBTROUTEROBJ_COOKIE == ((MTPBTROUTEROBJ*)mtpbt)->dwBTCookie);

    /*  Call the MTP/BT Parser to do the real work
     */

    ps = MTPBTParserSocketMsgReset(mtpstrm, PSLFALSE,
                            PSLSOCKETMSG_DATA_AVAILABLE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPBTRouterCreate
 *
 *  Creates an instance of the MTP/BT router
 *
 *  Arguments:
 *      MTPBTROUTER*    pmtpbtParser        Return location for handle
 *
 *  Returns:
 *      PSLSTATUS   PSL_SUCCESS on success
 */

PSLSTATUS _MTPBTRouterCreate(MTPBTROUTER* pmtpbtRouter)
{
    PSLMSGQUEUE     mqTransport = PSLNULL;
    PSLMSGPOOL      mpTransport = PSLNULL;
    PSLSTATUS       ps;
    MTPBTROUTEROBJ* pmtpbtRO = PSLNULL;
    PSLVOID*        pvParam = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpbtRouter)
    {
        *pmtpbtRouter = PSLNULL;
    }

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpbtRouter)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate our queue and pool
     */

    ps = MTPMsgGetQueueParam(&pvParam);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = PSLMsgQueueCreate(pvParam, MTPMsgOnPost, MTPMsgOnWait, MTPMsgOnDestroy,
                            &mqTransport);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pvParam = PSLNULL;

    ps = PSLMsgPoolCreate(MTPBTROUTER_POOL_SIZE, &mpTransport);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialze the basic information in the context
     */


    /*  Allocate a new router using the MTP Stream Router to create
     *  and initialize the base object
     */

    ps = MTPStreamRouterCreate(sizeof(MTPBTROUTEROBJ),
                            (MTPSTREAMPARSER*)&pmtpbtRO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Set the cookie so that we can validate this is one of our objects
     */

    pmtpbtRO->dwBTCookie = MTPBTROUTEROBJ_COOKIE;
#endif  /*  PSL_ASSERTS */

    /*  Set the proper VTBL and initialize the rest of the state
     */

    pmtpbtRO->mtpstrmRO.vtbl = &_VtblMTPBTRouter;
    pmtpbtRO->mtpstrmRO.mqTransport = mqTransport;
    pmtpbtRO->mtpstrmRO.mpTransport = mpTransport;
    mqTransport = PSLNULL;
    mpTransport = PSLNULL;
    pvParam = PSLNULL;

    /*  Return the parser handle
     */

    *pmtpbtRouter = pmtpbtRO;
    pmtpbtRO = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPBTROUTERDESTROY(pmtpbtRO);
    SAFE_PSLMSGQUEUEDESTROY(mqTransport);
    SAFE_PSLMSGPOOLDESTROY(mpTransport);
    SAFE_MTPMSGRELEASEQUEUEPARAM(pvParam);
    return ps;
}


/*  _MTPBTRouteInitCmdRequest
 *
 *  Handles an InitCmdRequest packet recieved from the remote initiator
 *
 *  Arguments:
 *      MTPBTROUTER     mtpbt               Router to handle request on
 *      MTPBTPARSER     mtpbtCmd            Command parser to use to
 *                                            receive the request
 *      PSLBOOL*        pfLinkInit          PSLTRUE if the link is fully
 *                                            initialized
 *      MTPBTPARSER*    pmtpbtEvent         Resulting event parser
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPBTRouteInitCmdRequest(MTPBTROUTER mtpbt, MTPBTPARSER mtpbtCmd,
                    PSLBOOL* pfLinkInit, MTPBTPARSER* pmtpbtEvent)
{
    mtpbtsockaddr_l2cap addrEvent;
    mtpbtsockaddr_l2cap addrAny =   {
                                        sizeof(mtpbtsockaddr_l2cap),
                                        MTPBT_AF_BLUETOOTH,
                                        { 0 },
                                    };
    PSLINT32            errSock;
    MTPBTPARSER         mtpbtEvent = PSLNULL;
    MTPBTPARSERINFO     mtpbtParserInfo;
    PSLSTATUS           ps;
    MTPBTROUTEROBJ*     pmtpbt;
    SOCKET              sockEvent = INVALID_SOCKET;
    PSLUINT16           wPSMEvent;

    /*  Clear result for safety
     */

    if (PSLNULL != pfLinkInit)
    {
        *pfLinkInit = PSLFALSE;
    }
    if (PSLNULL != pmtpbtEvent)
    {
        *pmtpbtEvent = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpbt) || (PSLNULL == mtpbtCmd) || (PSLNULL == pmtpbtEvent))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpbt = (MTPBTROUTEROBJ*)mtpbt;
    PSLASSERT(MTPBTROUTEROBJ_COOKIE == pmtpbt->dwBTCookie);

    /*  Parse the packet to retrieve the link ID and the PSM of the event
     *  channel that we are to establish
     */

    ps = MTPBTParserReadInitCmdRequest(mtpbtCmd, &(pmtpbt->guidLink),
                            &wPSMEvent);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Check to see if we are responding to an Initiator started link or
     *  receiving a callback for a link we started.
     */

    if (0 != wPSMEvent)
    {
        /*  Initiator started the link- create a new Bluetooth socket for the
         *  event connection and bind it to the local address
         */

        sockEvent = MTPBTSocketCreate(MTPBT_AF_BLUETOOTH, MTPBT_SOCK_SEQPACKET,
                            MTPBT_PROTO_L2CAP);
        if (INVALID_SOCKET == sockEvent)
        {
            ps = MTPBTSocketErrorToPSLStatus(sockEvent, sockEvent);
            goto Exit;
        }

        errSock = MTPBTSocketBind(sockEvent, (struct sockaddr*)&addrAny,
                            sizeof(addrAny));
        if (SOCKET_ERROR  == errSock)
        {
            ps = MTPBTSocketErrorToPSLStatus(sockEvent, errSock);
            goto Exit;
        }

        /*  Place the socket in asynchronous mode
         */

        ps = MTPBTSocketMsgSelect(sockEvent, pmtpbt->mtpstrmRO.mqTransport,
                            pmtpbt->mtpstrmRO.mpTransport,
                            MTPSTREAMPARSERTYPE_EVENT,
                            (FD_READ | FD_CONNECT | FD_CLOSE));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Retrieve the address information for the remote connection from
         *  the parser information and generate the address for the connection
         */

        ps = MTPStreamParserGetParserInfo(mtpbtCmd, &mtpbtParserInfo,
                            sizeof(mtpbtParserInfo));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        PSLASSERT(sizeof(mtpbtsockaddr_l2cap) == mtpbtParserInfo.cbAddrDest);
        PSLCopyMemory(&addrEvent, &(mtpbtParserInfo.saAddrDest),
                            sizeof(addrEvent));
        addrEvent.l2cap_psm = wPSMEvent;

        /*  And issue the connection request to the remote PSM
         */

        errSock = MTPBTSocketConnect(sockEvent, (struct sockaddr*)&addrEvent,
                            sizeof(addrEvent));
        if (SOCKET_ERROR == errSock)
        {
            ps = MTPBTSocketErrorToPSLStatus(sockEvent, errSock);
            goto Exit;
        }

        /*  Bind the socket to a MTP/BT event parser
         */

        ps = MTPBTParserCreate(MTPSTREAMPARSERMODE_RESPONDER, sockEvent,
                            MTPSTREAMPARSERTYPE_EVENT, &mtpbtEvent);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        sockEvent = INVALID_SOCKET;
    }
    else
    {
        /*  This connection is in response to a link request that we started-
         *  retrieve the event channel from the transport instead of creating
         *  a new one.
         */

        ps = MTPBTTransportGetEventParser(&(pmtpbt->guidLink), &mtpbtEvent);
        if (PSLSUCCESS != ps)
        {
            goto Exit;
        }

        /*  Transfer ownership for the events from the transport to this
         *  router
         */

        ps = MTPBTParserSocketMsgSelect(mtpbtEvent, pmtpbt->mtpstrmRO.mqTransport,
                            pmtpbt->mtpstrmRO.mpTransport,
                            MTPSTREAMPARSERTYPE_EVENT,
                            (FD_READ | FD_CLOSE));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  And send the init link acknowledgement
         */

        ps = MTPBTParserSendInitLinkACK(mtpbtEvent, &(pmtpbt->guidLink));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Remember that the link is initialized
         */


        *pfLinkInit = PSLTRUE;
    }

    /*  Return the result
     */

    *pmtpbtEvent = mtpbtEvent;
    mtpbtEvent = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPBTSOCKETCLOSE(sockEvent);
    SAFE_MTPBTPARSERDESTROY(mtpbtEvent);
    return ps;
}


/*  _MTPBTRouterBindInitiator
 *
 *  Binds the specified socket connected to an initiator to the MTP router
 *  and returns a fully bound context as the result.
 *
 *  Arguments:
 *      MTPBTROUTER     mtpbt               Router to bind to
 *      SOCKET          sock                Connection to the initiator
 *      MTPCONTEXT**    ppmtpctx            Resulting context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPBTRouterBindInitiator(MTPBTROUTER mtpbt, SOCKET sock,
                    MTPCONTEXT** ppmtpctx)
{
    PSLUINT32           dwPacketType;
    PSLUINT32           dwReason;
    PSLBOOL             fDone;
    PSLBOOL             fSendFail = PSLTRUE;
    PSLGUID             guidLink;
    MTPBTPARSER         mtpbtCommand = PSLNULL;
    MTPBTPARSER         mtpbtEvent = PSLNULL;
    PSLSTATUS           ps;
    PSLMSG*             pMsg = PSLNULL;
    MTPBTROUTEROBJ*     pmtpbt;

    /*  Clear result for safety
     */

    if (PSLNULL != ppmtpctx)
    {
        *ppmtpctx = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpbt) || (INVALID_SOCKET == sock) || (PSLNULL == ppmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpbt = (MTPBTROUTEROBJ*)mtpbt;
    PSLASSERT(MTPBTROUTEROBJ_COOKIE == pmtpbt->dwBTCookie);

    /*  Create a new Bluetooth stream command parser and bind it to the socket
     *  connection that we have
     */

    ps = MTPBTParserCreate(MTPSTREAMPARSERMODE_RESPONDER, sock,
                            MTPSTREAMPARSERTYPE_COMMAND, &mtpbtCommand);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Put the socket into asynchronous opreation
     */

    ps = MTPBTSocketMsgSelect(sock, pmtpbt->mtpstrmRO.mqTransport,
                            pmtpbt->mtpstrmRO.mpTransport,
                            MTPSTREAMPARSERTYPE_COMMAND, (FD_READ | FD_CLOSE));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And start handling messages
     */

    fDone = PSLFALSE;
    while (!fDone)
    {
        /*  Release any existing message
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  Retrieve the next message
         */

        ps = PSLMsgGet(pmtpbt->mtpstrmRO.mqTransport, &pMsg);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  And handle it
         */

        switch (pMsg->msgID)
        {
        case PSLSOCKETMSG_CLOSE:
        case MTPMSG_SHUTDOWN:
            /*  Either one of the sockets closed or we are being told to
             *  shutdown- go ahead and stop our attempt to create this context
             */

            ps = PSLERROR_CONNECTION_CLOSED;
            goto Exit;

        case PSLSOCKETMSG_DATA_AVAILABLE:
            /*  Let the eventing subsystem know that we have seen this event
             */

            ps = MTPBTParserSocketMsgReset(
                            (MTPSTREAMPARSERTYPE_COMMAND == pMsg->aParam1) ?
                                mtpbtCommand : mtpbtEvent,
                            PSLFALSE, pMsg->msgID);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  We have received data- determine which socket it came from
             */

            switch (pMsg->aParam1)
            {
            case MTPSTREAMPARSERTYPE_COMMAND:
                /*  Data is available on the command pipe- this is either the
                 *  initial InitCmdRequest, a link complete, or an init failed
                 */

                ps = MTPStreamParserGetPacketType(mtpbtCommand, &dwPacketType,
                            NULL);
                if (PSL_FAILED(ps))
                {
                    goto Exit;
                }

                /*  And handle the packet
                 */

                switch (dwPacketType)
                {
                case MTPBTPACKET_INIT_COMMAND_REQUEST:
                    /*  If we already have an event socket then we cannot
                     *  receive another command request
                     */

                    if (PSLNULL != mtpbtEvent)
                    {
                        ps = PSLERROR_INVALID_PACKET;
                        goto Exit;
                    }

                    /*  Handle the packet- note that if the link is
                     *  fully established when this packet is parsed then
                     *  we have no more work to do here.
                     */

                    ps = _MTPBTRouteInitCmdRequest(mtpbt, mtpbtCommand,
                            &fDone, &mtpbtEvent);
                    if (PSL_FAILED(ps))
                    {
                        goto Exit;
                    }
                    break;

                case MTPBTPACKET_INIT_LINK_ACK:
                    /*  Parse the packet
                     */

                    ps = MTPBTParserReadInitLinkACK(mtpbtCommand, &guidLink);
                    if (PSL_FAILED(ps))
                    {
                        goto Exit;
                    }

                    /*  Validate that the links are correct
                     */

                    if (0 != PSLCompareMemory(&guidLink, &(pmtpbt->guidLink),
                            sizeof(guidLink)))
                    {
                        ps = PSLERROR_INVALID_PACKET;
                        goto Exit;
                    }

                    /*  We are good...  Stop handling messages here
                     */

                    fDone = PSLTRUE;
                    break;

                default:
                    ps = PSLERROR_INVALID_PACKET;
                    goto Exit;
                }
                break;

            case MTPSTREAMPARSERTYPE_EVENT:
                /*  Read the packet type- the only thing valid is an
                 *  init fail notification
                 */

                ps = MTPStreamParserGetPacketType(mtpbtEvent, &dwPacketType,
                            PSLNULL);
                if (PSL_FAILED(ps))
                {
                    goto Exit;
                }

                if (MTPBTPACKET_INIT_LINK_FAIL != dwPacketType)
                {
                    ps = PSLERROR_INVALID_PACKET;
                    goto Exit;
                }

                /*  Retrieve the reason
                 */

                ps = MTPBTParserReadInitFail(mtpbtEvent, &dwReason);
                if (PSL_FAILED(ps))
                {
                    goto Exit;
                }

                /*  Forward the packet back along the command connection
                 */

                ps = MTPBTParserSendInitFail(mtpbtCommand, dwReason);
                if (PSL_FAILED(ps))
                {
                    goto Exit;
                }
                fSendFail = PSLFALSE;
                ps = PSLERROR_NETWORK_FAILURE;
                goto Exit;

            default:
                PSLASSERT(PSLFALSE);
                ps = PSLERROR_UNEXPECTED;
                goto Exit;
            }
            break;

        case PSLSOCKETMSG_CONNECT:
            /*  We should only ever receive this on the connect socket
             */

            PSLASSERT(MTPSTREAMPARSERTYPE_EVENT == pMsg->aParam1);
            if (MTPSTREAMPARSERTYPE_EVENT != pMsg->aParam1)
            {
                ps = PSLERROR_UNEXPECTED;
                goto Exit;
            }

            /*  Let the eventing subsystem know that we have seen this event
             */

            ps = MTPBTParserSocketMsgReset(mtpbtEvent, PSLFALSE, pMsg->msgID);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Send the InitEventRequest packet now that we are connected
             */

            ps = MTPBTParserSendInitEventRequest(mtpbtEvent,
                            &(pmtpbt->guidLink), 0);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        default:
            /*  We should not receive any other network events
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }
    }

    /*  We have succesfully established the link- go ahead and set the parsers
     *  and complete the binding of our context
     */

    pmtpbt->mtpstrmRO.mtpstrmCommand = mtpbtCommand;
    mtpbtCommand = PSLNULL;
    pmtpbt->mtpstrmRO.mtpstrmEvent = mtpbtEvent;
    mtpbtEvent = PSLNULL;
    ps = MTPStreamRouterCreateContext(pmtpbt, ppmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    if (fSendFail && PSL_FAILED(ps) && (PSLNULL != mtpbtCommand))
    {
        MTPBTParserSendInitFail(mtpbtCommand, MTPBT_FAIL_UNSPECIFIED);
    }
    SAFE_MTPBTPARSERDESTROY(mtpbtCommand);
    SAFE_MTPBTPARSERDESTROY(mtpbtEvent);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*  _MTPBTRouterHandleTransportError
 *
 *  This is the core handler for the transport error behaviors.  It is
 *  called both to handle local and remote error conditions.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             Context to on which error occurred
 *      MTPBTPARSER     mtpbtParser         Parser on which the error was
 *                                            received, NULL if the error is to
 *                                            be sent
 *      PSLUINT32       dwReason            Reason failure occurred
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPBTRouterHandleTransportError(MTPCONTEXT* pmtpctx,
                    MTPBTPARSER mtpbtParser, PSLUINT32 dwReason)
{
    PSLUINT32           dwTransactionID;
    PSLUINT32           dwTransportReady = 0;
    PSLUINT32           dwType;
    PSLBOOL             fClosing;
    PSLGUID             guidLinkID;
    PSLSTATUS           ps;
    MTPBTROUTEROBJ*     pmtpbt;
    PSLMSG*             pMsg = PSLNULL;
    PSLVOID*            pvBuffer;

    /*  Validate arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpbt = (MTPBTROUTEROBJ*)MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    ASSERT(MTPBTROUTEROBJ_COOKIE == pmtpbt->dwBTCookie);

    /*  Determine if we are sending or receiving the transport error
     */

    if (PSLNULL != mtpbtParser)
    {
        /*  We have received a transport error- go ahead and retrieve it
         */

        ps = MTPBTParserReadTransportError(mtpbtParser, &dwTransactionID,
                            &guidLinkID, &dwReason);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Make sure the link ID matches before we continue
         */

        if (0 != PSLCompareMemory(&pmtpbt->guidLink, &guidLinkID,
                            sizeof(pmtpbt->guidLink)))
        {
            PSLASSERT(PSLFALSE);
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  We need to make sure that we look for transport ready on this
         *  interface so that we do not miss receiving it because the data
         *  arrives before the event was reset.
         */

        ps = MTPBTParserReadTransportReady(mtpbtParser,
                    pmtpbt->mtpstrmRO.mtpstrmbm, &(pmtpbt->guidLink));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  If the data contains the transport ready message then remember
         *  that we have seen it
         */

        if (PSLSUCCESS == ps)
        {
            /*  Determine which parser was reset so that we can exit when
             *  appropriate
             */

            dwTransportReady |=
                        (mtpbtParser == pmtpbt->mtpstrmRO.mtpstrmCommand) ?
                                MTPBTTRANSPORT_COMMAND_READY :
                                MTPBTTRANSPORT_EVENT_READY;
        }
    }
    else
    {
        /*  We need to send a transport error notification on both streams to
         *  notify the host that we have encountered a problem in the transport-
         *  send the event side first since that channel should have the
         *  least amount of data on it
         */

        ps = MTPBTParserSendTransportError(pmtpbt->mtpstrmRO.mtpstrmEvent,
                            pmtpbt->mtpstrmRO.dwTransactionID,
                            &(pmtpbt->guidLink), dwReason);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        ps = MTPBTParserSendTransportError(pmtpbt->mtpstrmRO.mtpstrmCommand,
                            pmtpbt->mtpstrmRO.dwTransactionID,
                            &(pmtpbt->guidLink), dwReason);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Clear the pending buffer flag
     */

    pmtpbt->mtpstrmRO.fReceivePending = PSLFALSE;

    /*  Stop any pending buffer requests- this has the side effect of halting
     *  processing in the handlers.  We do not send the cancellation now
     *  because we need to make sure that we keep any pending socket events
     *  in the queue and the cancellation handling will wipe them out.
     */

    for (dwType = MTPBUFFER_UNKNOWN + 1;
            dwType < MTPBUFFER_START_TRANSPORT_SPECIFIC;
            dwType++)
    {
        ps = MTPStreamBufferMgrCancelRequest(pmtpbt->mtpstrmRO.mtpstrmbm,
                            dwType);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Reset the parsers so that we begin receiving data again.
     */

    ps = MTPStreamRouterResetDataAvailable(pmtpbt,
                            pmtpbt->mtpstrmRO.mtpstrmCommand);
    PSLASSERT(PSL_SUCCEEDED(ps));
    ps = MTPStreamRouterResetDataAvailable(pmtpbt,
                            pmtpbt->mtpstrmRO.mtpstrmEvent);
    PSLASSERT(PSL_SUCCEEDED(ps));

    /*  Now we need to enter a loop where we flush all of the pending data
     *  on the transports until we receive a transport ready request
     */

    fClosing = PSLFALSE;
    while (MTPBTTRANSPORT_READY != dwTransportReady)
    {
        /*  Release any existing message
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  Retrieve the next message
         */

        ps = PSLMsgGet(pmtpbt->mtpstrmRO.mqTransport, &pMsg);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  And handle it
         */

        switch (pMsg->msgID)
        {
        case PSLSOCKETMSG_CLOSE:
        case MTPMSG_SHUTDOWN:
            /*  Shutdown the route if it is initialized
             */

            ps = MTPStreamRouteTerminate(pmtpctx,
                            (PSLSOCKETMSG_CLOSE == pMsg->msgID) ?
                                MTPMSG_DISCONNECT : MTPMSG_SHUTDOWN, 0);
            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  We are ready to close down
             */

            PSLTraceProgress("MTPBT Transport 0x%08x closing", pmtpbt);
            fClosing = PSLTRUE;
            dwTransportReady = MTPBTTRANSPORT_READY;
            break;

        case PSLSOCKETMSG_DATA_AVAILABLE:
        case PSLSOCKETMSG_RESET:
            /*  Determine which parser the event is for
             */

            switch (pMsg->aParam1)
            {
            case MTPSTREAMPARSERTYPE_COMMAND:
                mtpbtParser = pmtpbt->mtpstrmRO.mtpstrmCommand;
                break;

            case MTPSTREAMPARSERTYPE_EVENT:
                mtpbtParser = pmtpbt->mtpstrmRO.mtpstrmEvent;
                break;

            default:
                /*  Should never get here- we should have either an event
                 *  or a command socket
                 */

                PSLASSERT(PSLFALSE);
                continue;
            }

            /*  Reset the socket event- we want to make sure that we
             *  get notified if data arrives while we are processing
             *  all of the available data to make sure that we do
             *  not miss a notification
             */

            ps = MTPBTParserSocketMsgReset(mtpbtParser, PSLFALSE, pMsg->msgID);
            PSLASSERT(PSL_SUCCEEDED(ps));

            /*  Switch the message to the generic stream router version
             */

            switch (pMsg->msgID)
            {
            case PSLSOCKETMSG_DATA_AVAILABLE:
                /*  Attempt to read the transport ready message from the
                 *  socket that has data
                 */

                ps = MTPBTParserReadTransportReady(mtpbtParser,
                            pmtpbt->mtpstrmRO.mtpstrmbm, &(pmtpbt->guidLink));
                if (PSL_FAILED(ps))
                {
                    goto Exit;
                }

                /*  If the data does not contain the transport ready message
                 *  then keep looking
                 */

                if (PSLSUCCESS != ps)
                {
                    break;
                }

                /*  Determine which parser was reset so that we can exit when
                 *  appropriate
                 */

                dwTransportReady |=
                            (MTPSTREAMPARSERTYPE_COMMAND == pMsg->aParam1) ?
                                MTPBTTRANSPORT_COMMAND_READY :
                                MTPBTTRANSPORT_EVENT_READY;
                break;

            case PSLSOCKETMSG_RESET:
                /*  Reset the route
                 */

                ps = MTPStreamRouteReset(&pmtpctx);
                PSLASSERT(PSL_SUCCEEDED(ps));
                if (PSL_FAILED(ps))
                {
                    goto Exit;
                }
                break;

            default:
                /*  Release any buffers in the message
                 */

                if (MTPMSGID_BUFFER_PRESENT & pMsg->msgID)
                {
                    pvBuffer = (PSLVOID*)pMsg->aParam2;
                    SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);
                }

                /*  Once we enter the transport error state we only care about
                 *  flushing data off the transport until we can reset the
                 *  command handling so we just ignore these messages
                 */

                PSLTraceWarning("_MTPBTRouterHandleTransportError: Ignoring "
                            "message 0x%08x", pMsg->msgID);
                break;
            }
        }
    }

    /*  If the transport was closed queue a terminate message so we can
     *  complete the shutdown
     */

    if (fClosing)
    {
        /*  Release the existing message
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  Allocate a new message and post the terminate request
         */

        ps = PSLMsgAlloc(pmtpbt->mtpstrmRO.mpTransport, &pMsg);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = MTPBTROUTERMSG_TERMINATE;

        ps = PSLMsgPost(pmtpbt->mtpstrmRO.mqTransport, pMsg);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = PSLNULL;
        goto Exit;
    }

    /*  At this point we know we have a transport error and that all of the
     *  pending data has safely been flushed.  We can now send the cancel
     *  notification to any active handlers so that we stop further data
     *  operations
     */

    if (PSLNULL != pmtpctx->mtpContextState.mqHandler)
    {
        /*  Terminate the route
         */

        ps = MTPStreamRouteTerminate(pmtpctx, MTPMSG_CANCEL,
                            pmtpbt->mtpstrmRO.dwTransactionID);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Respond to the host with the transport ready notification on both
     *  channels
     */

    ps = MTPBTParserSendTransportReady(pmtpbt->mtpstrmRO.mtpstrmEvent,
                            &(pmtpbt->guidLink));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = MTPBTParserSendTransportReady(pmtpbt->mtpstrmRO.mtpstrmCommand,
                        &(pmtpbt->guidLink));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  All is well- resume regular command processing
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*  MTPBTRouterThread
 *
 *  The main thread for handling MTP/BT transport operations for objects that
 *  are bound to a router.
 *
 *  Arguments:
 *      PSLVOID*        pvParam             Parameter information- contains
 *                                            the transport info to which this
 *                                            thread is bound
 *
 *  Returns:
 *      PSLUINT32       Thread result (PSLSTATUS assumed)
 */

PSLUINT32 PSL_API MTPBTRouterThread(PSLVOID* pvParam)
{
    PSLPARAM                aParamTransport = MTPTRANSPORTPARAM_UNKNOWN;
    PSLBOOL                 fDone;
    PSLHANDLE               hSignalInit = PSLNULL;
    MTPBTPARSER             mtpbtParser;
    PSLMSGQUEUE             mqShutdown = PSLNULL;
    PSLSTATUS               ps;
    PSLMSG*                 pMsg = PSLNULL;
    MTPBTROUTEROBJ*         pmtpbt = PSLNULL;
    MTPCONTEXT*             pmtpctx = PSLNULL;
    MTPBTROUTERINIT*        pmtpri = PSLNULL;
    SOCKET                  sockInitiator;

    /*  Validate arguments
     */

    if (PSLNULL == pvParam)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get at the router initialization info
     */

    pmtpri = (MTPBTROUTERINIT*)pvParam;
    hSignalInit = pmtpri->hSignalComplete;

    /*  Remember the transport param so we can send a close if necessary
     */

    aParamTransport = pmtpri->aParamTransport;

    /*  Validate arguments
     */

    if (INVALID_SOCKET == pmtpri->sockInitiator)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    sockInitiator = pmtpri->sockInitiator;

    /*  Create the MTP Bluetooth router object
     */

    ps = _MTPBTRouterCreate(&pmtpbt);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Register our queue with the shutdown service so that we make sure
     *  we are forced close if shutdown occurs
     */

    ps = MTPShutdownRegister(pmtpbt->mtpstrmRO.mqTransport);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    mqShutdown = pmtpbt->mtpstrmRO.mqTransport;

    /*  Report that we have succesfully initialized
     */

    pmtpri->psResult = PSLSUCCESS;
    ps = PSLSignalSet(hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pmtpri = PSLNULL;
    hSignalInit = PSLNULL;

    /*  From here on out closing the MTP/BT router should cause the transport
     *  to be notified of closure
     */

    pmtpbt->aParamTransport = aParamTransport;
    aParamTransport = MTPTRANSPORTPARAM_UNKNOWN;

    /*  Handle the rest of the connection handshake
     */

    ps = _MTPBTRouterBindInitiator(pmtpbt, sockInitiator, &pmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure that any pending data waiting on the command socket is
     *  handled
     */

    ps = MTPStreamRouteDataAvailable(pmtpctx, pmtpbt->mtpstrmRO.mtpstrmCommand);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Start looping to handle messages
     */

    fDone = PSLFALSE;
    while (!fDone)
    {
        /*  Release any existing message
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  Retrieve the next message
         */

        ps = PSLMsgGet(pmtpbt->mtpstrmRO.mqTransport, &pMsg);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  And handle it
         */

        switch (pMsg->msgID)
        {
        case PSLSOCKETMSG_CLOSE:
        case MTPMSG_SHUTDOWN:
        case MTPBTROUTERMSG_TERMINATE:
            /*  Shutdown the route if it is initialized
             */

            ps = MTPStreamRouteTerminate(pmtpctx,
                            (PSLSOCKETMSG_CLOSE == pMsg->msgID) ?
                                MTPMSG_DISCONNECT : MTPMSG_SHUTDOWN, 0);
            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  We are ready to close down
             */

            PSLTraceProgress("MTPBT Transport 0x%08x closing", pmtpbt);
            fDone = PSLTRUE;
            break;

        case PSLSOCKETMSG_DATA_AVAILABLE:
        case PSLSOCKETMSG_RESET:
            /*  Determine which parser the event is for
             */

            switch (pMsg->aParam1)
            {
            case MTPSTREAMPARSERTYPE_COMMAND:
                mtpbtParser = pmtpbt->mtpstrmRO.mtpstrmCommand;
                break;

            case MTPSTREAMPARSERTYPE_EVENT:
                mtpbtParser = pmtpbt->mtpstrmRO.mtpstrmEvent;
                break;

            default:
                /*  Should never get here- we should have either an event
                 *  or a command socket
                 */

                PSLASSERT(PSLFALSE);
                continue;
            }

            /*  Reset the socket event- we want to make sure that we
             *  get notified if data arrives while we are processing
             *  all of the available data to make sure that we do
             *  not miss a notification
             */

            ps = MTPBTParserSocketMsgReset(mtpbtParser, PSLFALSE, pMsg->msgID);
            PSLASSERT(PSL_SUCCEEDED(ps));

            /*  Switch the message to the generic stream router version
             */

            switch (pMsg->msgID)
            {
            case PSLSOCKETMSG_DATA_AVAILABLE:
                pMsg->msgID = MTPSTREAMROUTERMSG_DATA_AVAILABLE;
                break;

            case PSLSOCKETMSG_RESET:
                pMsg->msgID = MTPSTREAMROUTERMSG_RESET;
                break;

            default:
                PSLASSERT(PSLFALSE);
                ps = PSLERROR_UNEXPECTED;
                goto Exit;
            }

            /*  FALL THROUGH INTENTIONAL */

        default:
            /*  Call the generic helper function to handle the rest of the
             *  messages
             */

            ps = MTPStreamRouteMsg(pmtpctx, pMsg);
            if (PSL_FAILED(ps))
            {
                /*  Check to see if these are one of the well defined transport
                 *  error codes that we can recover from- if so do it
                 */

                if (((MTPSTREAMROUTERMSG_DATA_AVAILABLE == pMsg->msgID) ||
                            (MTPSTREAMROUTERMSG_CHECK_DATA_AVAILABLE ==
                                    pMsg->msgID)) &&
                    ((PSLERROR_DATA_CORRUPT == ps) ||
                            (PSLERROR_CONNECTION_CORRUPT == ps)))
                {
                    /*  Attempt to reset the transport- remember to pass the
                     *  "unknown" streamer flag to trigger sending a transport
                     *  error packet
                     */

                    ps = _MTPBTRouterHandleTransportError(pmtpctx, PSLNULL,
                            (PSLERROR_DATA_CORRUPT == ps) ?
                                    MTPBT_TRANSPORT_FAIL_INVALID_DATA :
                                    MTPBT_TRANSPORT_FAIL_MISSING_PACKET);
                }

                /*  If we still have an error condition terminate the route
                 */

                if (PSL_FAILED(ps))
                {
                    /*  Abort the route if it is initialized
                     */

                    MTPStreamRouteTerminate(pmtpctx, MTPMSG_ABORT, 0);

                    /*  Report an unsafe termination
                     */

                    PSLTraceError(
                            "MTPBT Transport 0x%08x aborting on error 0x%08x",
                            pmtpbt, ps);
                    goto Exit;
                }
            }
            break;
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    SAFE_MTPROUTEDESTROY(pmtpctx);
    PSLASSERT((PSLNULL == hSignalInit) || (PSLNULL != pmtpri));
    if ((PSLNULL != hSignalInit) && (PSLNULL != pmtpri))
    {
        pmtpri->psResult = ps;
        PSLSignalSet(hSignalInit);
    }
    SAFE_MTPBTROUTERDESTROY(pmtpbt);

    /*  Send a transport close if it was not already handled by the route
     */

    CHECK_MTPTRANSPORTNOTIFY(MTPTRANSPORTEVENT_CLOSED, aParamTransport);

    /*  Finally unregister with the shutdown service after everything else
     *  is closed- this guarantees that our code stays in memory up to
     *  at least this point
     */

    if (PSLNULL != mqShutdown)
    {
        MTPShutdownUnregister(mqShutdown);
    }
    return ps;
}

