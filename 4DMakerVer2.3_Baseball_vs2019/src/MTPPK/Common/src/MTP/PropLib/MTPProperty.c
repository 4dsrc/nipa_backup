/*
 *  MTPProperty.c
 *
 *  Contains definition for the fucntions that support
 *  property related MTP operations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPropertyPrecomp.h"

/*  Local Defines
 */

#define PROPDESCSERIALIZEINFO_COOKIE            'pdsI'

#define PROPDESCSERIALIZEINFO_POOL_DEFAULT      MTP_DEFAULT_SESSIONS
#define PROPDESCSERIALIZEINFO_POOL_MULTIPLIER   2

enum
{
    MTPPROPDESCVALUE_DEFAULT_VALUE = 0,
    MTPPROPDESCVALUE_CURRENT_VALUE,
    MTPPROPDESCVALUE_FORM,
};

#define CALCREMAIN(_pbS, _cbM, _pbC) \
    ((PSLNULL != (_pbS)) ? ((_cbM) - ((_pbC) - (_pbS))) : 0)


/*  Local Types
 */

typedef struct _PROPDESCSERIALIZEINFO
{
#ifdef PSL_ASSERTS
    PSLUINT32                       dwCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT16                       wPropFlags;
    union
    {
        MTPPROPERTYINFO                 infoProp;
        MTPDEVICEPROPINFO               infoDevice;
        MTPSERVICEPROPERTYINFO          infoService;
        MTPOBJECTPROPINFO               infoObject;
    } info;
    PSLPARAM                        aContext;
} PROPDESCSERIALIZEINFO;

/*  Local Functions
 */

static PSLVOID PSL_API _MTPPropDescContextDestroy(
                    PSL_CONST MTPSERIALIZEINFO* rgmsiTemplate,
                    PSLPARAM aContext,
                    PSL_CONST PSLVOID* pvOffset);

static PSLSTATUS PSL_API _MTPPropDescSerializeValue(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

/*  Local Variables
 */

static PSLOBJPOOL                   _OpPropDescInfo = PSLNULL;

static PSL_CONST MTPSERIALIZEINFO   _RgmsiDevicePropDesc[] =
{
    /*  Property Code
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.wPropCode),
        0
    },

    /*  Data Type
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.precProp),
        PSLOFFSETOF(MTPPROPERTYREC, wDataType)
    },

    /*  Access Mode
     */

    {
        MTP_DATATYPE_UINT8,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.bGetSet),
        0
    },

    /*  Default Value
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPPropDescSerializeValue,
        MTPPROPDESCVALUE_DEFAULT_VALUE
    },

    /*  Current Value
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPPropDescSerializeValue,
        MTPPROPDESCVALUE_CURRENT_VALUE,
    },

    /*  Form Flag
     */

    {
        MTP_DATATYPE_UINT8,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.precProp),
        PSLOFFSETOF(MTPPROPERTYREC, bForm)
    },

    /*  Form Data
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPPropDescSerializeValue,
        MTPPROPDESCVALUE_FORM,
    }
};


static PSL_CONST MTPSERIALIZEINFO   _RgmsiObjectPropDesc[] =
{
    /*  Property Code
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.wPropCode),
        0
    },

    /*  Data Type
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.precProp),
        PSLOFFSETOF(MTPPROPERTYREC, wDataType)
    },

    /*  Access Mode
     */

    {
        MTP_DATATYPE_UINT8,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.bGetSet),
        0
    },

    /*  Default Value
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPPropDescSerializeValue,
        MTPPROPDESCVALUE_DEFAULT_VALUE
    },

    /*  Group Code
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoObject.dwGroupCode),
        0
    },

    /*  Form Flag
     */

    {
        MTP_DATATYPE_UINT8,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.precProp),
        PSLOFFSETOF(MTPPROPERTYREC, bForm)
    },

    /*  Form Data
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPPropDescSerializeValue,
        MTPPROPDESCVALUE_FORM,
    }
};


static PSL_CONST MTPSERIALIZEINFO   _RgmsiServicePropDesc[] =
{
    /*  Property Code
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.wPropCode),
        0
    },

    /*  Data Type
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.precProp),
        PSLOFFSETOF(MTPPROPERTYREC, wDataType)
    },

    /*  Access Mode
     */

    {
        MTP_DATATYPE_UINT8,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.bGetSet),
        0
    },

    /*  Form Flag
     */

    {
        MTP_DATATYPE_UINT8,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.precProp),
        PSLOFFSETOF(MTPPROPERTYREC, bForm)
    },

    /*  Form Data
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPPropDescSerializeValue,
        MTPPROPDESCVALUE_FORM,
    }
};


static PSL_CONST MTPSERIALIZEINFO   _RgmsiServiceObjectPropDesc[] =
{
    /*  Property Code
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.wPropCode),
        0
    },

    /*  Property Type
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.precProp),
        PSLOFFSETOF(MTPPROPERTYREC, wDataType)
    },

    /*  Property Get/Set
     */

    {
        MTP_DATATYPE_UINT8,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.bGetSet),
        0
    },

    /*  Default Value
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPPropDescSerializeValue,
        MTPPROPDESCVALUE_DEFAULT_VALUE
    },

    /*  Group Code
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoObject.dwGroupCode),
        0
    },

    /*  Service Form Specification
     */

    {
        MTP_DATATYPE_UINT8,
        MTPSERIALIZEFLAG_BYVALUE,
        MTP_FORMFLAG_SERVICEEXT,
        0
    },

    /*  Service Property Key
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_REFOFFSET |
            MTPSERIALIZEFLAG_PSLPKEY,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.precProp),
        PSLOFFSETOF(MTPSERVICEPROPERTYREC, pkeyProp)
    },

    /*  Service Property Name
     */

    {
        MTP_DATATYPE_STRING,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_REFOFFSET,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.precProp),
        PSLOFFSETOF(MTPSERVICEPROPERTYREC, szName)
    },

    /*  Object Property Form Flag
     */

    {
        MTP_DATATYPE_UINT8,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(PROPDESCSERIALIZEINFO, info.infoProp.precProp),
        PSLOFFSETOF(MTPPROPERTYREC, bForm)
    },

    /*  Object Property Form
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPPropDescSerializeValue,
        MTPPROPDESCVALUE_FORM,
    }
};


/*  MTPPropertyInitialize
 *
 *  Initializes the MTPProperty subsystem.
 *
 *  Arguments:
 *      PSLUINT32       cSessionMax         Maximum number of simultaneous
 *                                            sessions (0 = unlimited)
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPPropertyInitialize(PSLUINT32 cSessionMax)
{
    PSLUINT32       cItems;
    PSLSTATUS       ps;

    /*  Make sure we are not already initialized
     *
     *  NOTE: MTPInitialize currently handles synchronization of initialize
     *  calls so we do not do additional work here.
     */

    if (PSLNULL != _OpPropDescInfo)
    {
        ps = PSLERROR_IN_USE;
        goto Exit;
    }

    /*  Initialize the property schema
     */

    ps = InitPropSchema();
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Determine the actual number of objects we want to reserve- we always
     *  want to keep some around even when we are running in an "unlimited"
     *  session model.  Also make sure that we account for the fact that we
     *  could have more than one active serialization going on per session.
     */

    cItems = PROPDESCSERIALIZEINFO_POOL_MULTIPLIER * ((0 != cSessionMax) ?
                            cSessionMax : PROPDESCSERIALIZEINFO_POOL_DEFAULT);

    /*  Create the object pool so that we avoid allocations
     */

    ps = PSLObjectPoolCreate(cItems, sizeof(PROPDESCSERIALIZEINFO),
                            PSLOBJPOOLFLAG_ZERO_MEMORY | ((0 != cSessionMax) ?
                                    0 : PSLOBJPOOLFLAG_ALLOW_ALLOC),
                            &_OpPropDescInfo);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPPropertyUninitialize
 *
 *  Performs cleanup operations on the MTPProperty sub-system
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPPropertyUninitialize(PSLVOID)
{
    /*  Release the object pool
     *
     *  NOTE: MTPUninitialize currently handles thread synchronization so we
     *  do not do additional work here
     */

    SAFE_PSLOBJECTPOOLDESTROY(_OpPropDescInfo);
    return PSLSUCCESS;
}


/*  MTPPropertyReadSize
 *
 *  Reads the size of the property that follows.  Handles arrays as well as
 *  simple types.
 *
 *  Arguments:
 *      PSLUINT16       wDataType           Property size to retrieve
 *      PSLVOID*        pvBuf               Buffer for additional size info
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbSize             Size of the property
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPPropertyReadSize(PSLUINT16 wDataType,
                    PSL_CONST PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLUINT32* pcbSize)
{
    PSLUINT32 cchString;
    PSLSTATUS status;
    PSLUINT32 cbSize;

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    if (PSLNULL == pvBuf || PSLNULL == pcbSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    switch(wDataType)
    {
    case MTP_DATATYPE_STRING:
        //  Do all of the extra string validation that we can

        status = MTPLoadMTPString(PSLNULL, 0, &cchString,
                        (PXMTPSTRING)pvBuf, cbBuf);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        //  While MTPLoadMTPString validates the quality of the string
        //  we actually need the number of bytes that are supposed to
        //  be in the packet- this would be the length byte followed by
        //  the number of bytes in the string

        cbSize = sizeof(PSLUINT8) +
                        (*((PSLBYTE*)pvBuf) * sizeof(PSLUINT16));
        break;

    case MTP_DATATYPE_INT8:
    case MTP_DATATYPE_UINT8:
        cbSize = sizeof(PSLUINT8);
        break;

    case MTP_DATATYPE_INT16:
    case MTP_DATATYPE_UINT16:
        cbSize = sizeof(PSLUINT16);
        break;

    case MTP_DATATYPE_INT32:
    case MTP_DATATYPE_UINT32:
        cbSize = sizeof(PSLUINT32);
        break;

    case MTP_DATATYPE_INT64:
    case MTP_DATATYPE_UINT64:
        cbSize = sizeof(PSLUINT64);
        break;

    case MTP_DATATYPE_INT128:
    case MTP_DATATYPE_UINT128:
        cbSize = sizeof(PSLUINT128);
        break;

    case MTP_DATATYPE_AINT8:
    case MTP_DATATYPE_AUINT8:
        if (cbBuf < sizeof(PSLUINT32))
        {
            status = PSLERROR_INVALID_DATA;
            goto Exit;
        }
        cbSize = (MTPLoadUInt32((PXPSLUINT32)pvBuf) *
                        sizeof(PSLUINT8)) + sizeof(PSLUINT32);
        break;

    case MTP_DATATYPE_AINT16:
    case MTP_DATATYPE_AUINT16:
        if (cbBuf < sizeof(PSLUINT32))
        {
            status = PSLERROR_INVALID_DATA;
            goto Exit;
        }
        cbSize = (MTPLoadUInt32((PXPSLUINT32)pvBuf) *
                        sizeof(PSLUINT16)) + sizeof(PSLUINT32);
        break;

    case MTP_DATATYPE_AINT32:
    case MTP_DATATYPE_AUINT32:
        if (cbBuf < sizeof(PSLUINT32))
        {
            status = PSLERROR_INVALID_DATA;
            goto Exit;
        }
        cbSize = (MTPLoadUInt32((PXPSLUINT32)pvBuf) *
                        sizeof(PSLUINT32)) + sizeof(PSLUINT32);
        break;

    case MTP_DATATYPE_AINT64:
    case MTP_DATATYPE_AUINT64:
        if (cbBuf < sizeof(PSLUINT32))
        {
            status = PSLERROR_INVALID_DATA;
            goto Exit;
        }
        cbSize = (MTPLoadUInt32((PXPSLUINT32)pvBuf) *
                        sizeof(PSLUINT64)) + sizeof(PSLUINT32);
        break;

    case MTP_DATATYPE_AINT128:
    case MTP_DATATYPE_AUINT128:
        if (cbBuf < sizeof(PSLUINT32))
        {
            status = PSLERROR_INVALID_DATA;
            goto Exit;
        }
        cbSize = (MTPLoadUInt32((PXPSLUINT32)pvBuf) *
                        sizeof(PSLUINT128)) + sizeof(PSLUINT32);
        break;

    default:
        PSLASSERT(PSLFALSE);
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    *pcbSize = cbSize;
    status = PSLSUCCESS;

Exit:
    return status;
}


/*  MTPPropertyGetMinSizeFromType
 *
 *  Returns the minimum size for an element of the specified data type.
 *  For Strings and Array types only the size of the count is returned.
 *
 *  Arguments:
 *      PSLUINT16       wDataType           Data type to query
 *      PSLUINT32*      pcbSize             Size return buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPPropertyGetMinSizeFromType(PSLUINT16 wDataType,
                            PSLUINT32* pcbSize)
{
    PSLSTATUS status;

    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }

    if (PSLNULL == pcbSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    switch (wDataType)
    {
    case MTP_DATATYPE_UINT8:
    case MTP_DATATYPE_INT8:
        *pcbSize = sizeof(PSLUINT8);
        break;

    case MTP_DATATYPE_UINT16:
    case MTP_DATATYPE_INT16:
        *pcbSize = sizeof(PSLUINT16);
        break;

    case MTP_DATATYPE_UINT32:
    case MTP_DATATYPE_INT32:
        *pcbSize = sizeof(PSLUINT32);
        break;

    case MTP_DATATYPE_UINT64:
    case MTP_DATATYPE_INT64:
        *pcbSize = sizeof(PSLUINT64);
        break;

    case MTP_DATATYPE_UINT128:
    case MTP_DATATYPE_INT128:
        *pcbSize = sizeof(PSLUINT128);
        break;

    case MTP_DATATYPE_AINT8:
    case MTP_DATATYPE_AUINT8:
    case MTP_DATATYPE_AINT16:
    case MTP_DATATYPE_AUINT16:
    case MTP_DATATYPE_AINT32:
    case MTP_DATATYPE_AUINT32:
    case MTP_DATATYPE_AINT64:
    case MTP_DATATYPE_AUINT64:
    case MTP_DATATYPE_AINT128:
    case MTP_DATATYPE_AUINT128:
        *pcbSize = sizeof(PSLUINT32);
        break;

    case MTP_DATATYPE_STRING:
        *pcbSize = sizeof(PSLUINT8);
        break;

    default:
        PSLASSERT(PSLFALSE);
        status = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    status = PSLSUCCESS;
Exit:
    return status;
}


/*  MTPPropertyGetDefaultRec
 *
 *  Returns the default property record for the specified property code.
 *
 *  Arguments:
 *      PSLUINT16       wPropCode           Property code requested
 *      PSLUINT16       wPropType           Property record type requested
 *      MTPPROPERTYREC**
 *                      pprecProp           Resulting property record
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if found, PSLSUCCESS_NOT_FOUND if not
 */

PSLSTATUS PSL_API MTPPropertyGetDefaultRec(PSLUINT16 wPropCode,
                    PSLUINT16 wPropType, PSL_CONST MTPPROPERTYREC** pprecProp)
{
    PSLSTATUS           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pprecProp)
    {
        *pprecProp = PSLNULL;
    }

    /*  Validate arguments
     */

    if (PSLNULL == pprecProp)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Locate the property record
     */

    ps = GetPropRecByCode(wPropCode, wPropType, pprecProp);

Exit:
    return ps;
}


/*  MTPPropertySerializeDefaultValue
 *
 *  Serializes the a generic default property value.  For most standard types
 *  without explict forms this is the "empty" property (e.g. 0 or empty string).
 *  If either the range form or the enum form is specified the default value
 *  is the first entry in the form.
 *
 *  Arguments:
 *      MTPPROPERTYREC* precProp            Property record for property
 *      PSLVOID*        pvBuf               Location to serialize to
 *      PSLUINT32       cbBuf               Size of the serialization buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPPropertySerializeDefaultValue(
                    PSL_CONST MTPPROPERTYREC* precProp, PSLVOID* pvBuf,
                    PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLUINT32               cbSize;
    PSLUINT32               cItems;
    PSLUINT32               cbItem;
    PSLSTATUS               ps;
    PSL_CONST MTPENUMFORM*  pefProp;
    PSL_CONST PSLVOID*      pvValue;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == precProp) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Use the form to determine what the value should be
     */

    switch (precProp->bForm)
    {
    case MTP_FORMFLAG_NONE:
    case MTP_FORMFLAG_DATETIME:
    case MTP_FORMFLAG_REGEXP:
    case MTP_FORMFLAG_OBJECTID:
        /*  First determine the size of the value- remember strings and arrays
         *  are tricky because the "empty" form may not be the same size as
         *  the data type
         */

        switch (precProp->wDataType)
        {
        case MTP_DATATYPE_INT8:
        case MTP_DATATYPE_UINT8:
        case MTP_DATATYPE_INT16:
        case MTP_DATATYPE_UINT16:
        case MTP_DATATYPE_INT32:
        case MTP_DATATYPE_UINT32:
        case MTP_DATATYPE_INT64:
        case MTP_DATATYPE_UINT64:
        case MTP_DATATYPE_INT128:
        case MTP_DATATYPE_UINT128:
        case MTP_DATATYPE_AINT8:
        case MTP_DATATYPE_AUINT8:
        case MTP_DATATYPE_AINT16:
        case MTP_DATATYPE_AUINT16:
        case MTP_DATATYPE_AINT32:
        case MTP_DATATYPE_AUINT32:
        case MTP_DATATYPE_AINT64:
        case MTP_DATATYPE_AUINT64:
        case MTP_DATATYPE_AINT128:
        case MTP_DATATYPE_AUINT128:
        case MTP_DATATYPE_STRING:
            /*  Determine the actual minimum size of the data type
             */

            ps = MTPPropertyGetMinSizeFromType(precProp->wDataType, &cbSize);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Return the size if that is all that is needed
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = cbSize;
            ps = PSLSUCCESS;
            break;
        }

        /*  Store the value
         */

        PSLZeroMemory(pvBuf, cbSize);
        *pcbUsed = cbSize;
        ps = PSLSUCCESS;
        break;

    case MTP_FORMFLAG_RANGE:
    case MTP_FORMFLAG_ENUM:
        /*  Extract the "value" that we will be encoding
         */

        switch (precProp->bForm)
        {
        case MTP_FORMFLAG_RANGE:
            /*  For range values we serialize the first value from the form
             *  enumeration (the minimum value)
             */

            pvValue = precProp->pvForm;
            break;

        case MTP_FORMFLAG_ENUM:
            /*  For enum values we serialize the first enumerated value from
             *  the form
             */

            pefProp = (PSL_CONST MTPENUMFORM*)precProp->pvForm;
            pvValue = pefProp->rgValues;
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Make sure we have a value to serialize
         */

        if (PSLNULL == pvValue)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  The rest of the handling is type specific- so determine what to do
         */

        switch (precProp->wDataType)
        {
        case MTP_DATATYPE_INT8:
        case MTP_DATATYPE_UINT8:
        case MTP_DATATYPE_INT16:
        case MTP_DATATYPE_UINT16:
        case MTP_DATATYPE_INT32:
        case MTP_DATATYPE_UINT32:
        case MTP_DATATYPE_INT64:
        case MTP_DATATYPE_UINT64:
        case MTP_DATATYPE_INT128:
        case MTP_DATATYPE_UINT128:
            /*  Determine the actual minimum size based on the data type
             */

            ps = MTPPropertyGetMinSizeFromType(precProp->wDataType, &cbSize);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Return the size if that is all that is needed
             */

            if (PSLNULL == pvBuf)
            {
                *pcbUsed = cbSize;
                ps = PSLSUCCESS;
                break;
            }

            /*  Store the value
             */

            ps = MTPStoreValue(pvBuf, cbBuf, precProp->wDataType, pvValue);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            *pcbUsed = cbSize;
            break;

        case MTP_DATATYPE_AINT8:
        case MTP_DATATYPE_AUINT8:
        case MTP_DATATYPE_AINT16:
        case MTP_DATATYPE_AUINT16:
        case MTP_DATATYPE_AINT32:
        case MTP_DATATYPE_AUINT32:
        case MTP_DATATYPE_AINT64:
        case MTP_DATATYPE_AUINT64:
        case MTP_DATATYPE_AINT128:
        case MTP_DATATYPE_AUINT128:
            /*  For array elements the value actually is a redirection- so
             *  make sure we are looking at the real value first
             */

            pvValue = *((PSLBYTE**)pvValue);

            /*  The first item in the array must be the number of elements
             */

            cItems = *((PSLUINT32*)pvValue);

            /*  And calculate the size based on the actual data size
             */

            switch (precProp->wDataType)
            {
            case MTP_DATATYPE_AINT8:
            case MTP_DATATYPE_AUINT8:
                cbItem = sizeof(PSLUINT8);
                break;

            case MTP_DATATYPE_AINT16:
            case MTP_DATATYPE_AUINT16:
                cbItem = sizeof(PSLUINT16);
                break;

            case MTP_DATATYPE_AINT32:
            case MTP_DATATYPE_AUINT32:
                cbItem = sizeof(PSLUINT32);
                break;

            case MTP_DATATYPE_AINT64:
            case MTP_DATATYPE_AUINT64:
                cbItem = sizeof(PSLUINT64);
                break;

            case MTP_DATATYPE_AINT128:
            case MTP_DATATYPE_AUINT128:
                cbItem = sizeof(PSLUINT128);
                break;

            default:
                PSLASSERT(PSLFALSE);
                ps = PSLERROR_UNEXPECTED;
                goto Exit;
            }

            /*  Calculate the full size of the array- it includes the number
             *  of elements followed by the actual data
             */

            cbSize = sizeof(PSLUINT32) + (cItems * cbItem);

            /*  See if we are just being asked for the size
             */

            if (PSLNULL == pvBuf)
            {
                *pcbUsed = cbSize;
                ps = PSLSUCCESS;
                break;
            }

            /*  Make sure there is space to store the data
             */

            if (cbBuf < cbSize)
            {
                ps = PSLERROR_INSUFFICIENT_BUFFER;
                goto Exit;
            }

            /*  Store the number of items
             */

            MTPStoreUInt32((PXPSLUINT32)pvBuf, cItems);
            MTPStoreUIntArray((PSLBYTE*)pvBuf + sizeof(PSLUINT32),
                            (PSLBYTE*)pvValue + sizeof(PSLUINT32), cItems,
                            cbItem);
            *pcbUsed = cbSize;
            ps = PSLSUCCESS;
            break;

        case MTP_DATATYPE_STRING:
            /*  Store the string value- remember that the pvValue points to
             *  the string pointer
             */

            ps = MTPStoreMTPString((PXMTPSTRING)pvBuf, cbBuf, pcbUsed,
                            *((PSLCWSTR*)pvValue), MAX_MTP_STRING_LEN);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }
        break;

    case MTP_FORMFLAG_FIXEDARRAY:
    case MTP_FORMFLAG_BYTEARRAY:
    case MTP_FORMFLAG_LONGSTRING:
        /*  Validate that the type is correct
         */

        PSLASSERT(MTP_DATATYPE_ARRAY & precProp->wDataType);
        if (!(MTP_DATATYPE_ARRAY & precProp->wDataType))
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Default value for arrays is empty- just serialize a count of
         *  zero bytes
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = sizeof(PSLUINT32);
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure there is enough space to hold the value
         */

        if (sizeof(PSLUINT32) > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  Do the actual work
         */

        MTPStoreUInt32((PXPSLUINT32)pvBuf, 0);
        *pcbUsed = sizeof(PSLUINT32);
        ps = PSLSUCCESS;
        break;

    case MTP_FORMFLAG_SERVICEEXT:
    case MTP_FORMFLAG_SERVICEMETHOD:
        /*  These types apply only to the service form which does not include
         *  a default value,
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;

    default:
        /*  Should never get here
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPPropertySerializeStringForm
 *
 *  Serializes string based property forms
 *
 *  Arguments:
 *      MTPPROPERTYREC* precProp            Record for form to serialize
 *      PSLVOID*        pvBuf               Destination buffer
 *      PSLUINT32       cbBuf               Size of the destination buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPPropertySerializeStringForm(
                    PSL_CONST MTPPROPERTYREC* precProp, PSLVOID* pvBuf,
                    PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLUINT32       cItems;
    PSLUINT32       cbUsed;
    PSLUINT32       idxItem;
    PSLSTATUS       ps;
    PSLBYTE*        pbCur;
    PSLBYTE**       ppbDest;
    PSLCWSTR*       rgszSrc;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == precProp) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Only string property forms are serialized here
     */

    if (MTP_DATATYPE_STRING != precProp->wDataType)
    {
        ps = PSLERROR_INVALID_DATA;
        goto Exit;
    }

    /*  And handle the form itself
     */

    switch (precProp->bForm)
    {
    case MTP_FORMFLAG_RANGE:
    case MTP_FORMFLAG_ENUM:
        /*  Range and enum forms require form data
         */

        if (PSLNULL == precProp->pvForm)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Determine the total number of items that need to be serialized based
         *  on the form type
         */

        ppbDest = (PSLNULL != pvBuf) ? &pbCur : &((PSLBYTE*)pvBuf);
        pbCur = pvBuf;
        switch (precProp->bForm)
        {
        case MTP_FORMFLAG_RANGE:
            /*  We return three values (min, max, step) in the data type size
             */

            cItems = 3;
            rgszSrc = (PSLCWSTR*)precProp->pvForm;
            break;

        case MTP_FORMFLAG_ENUM:
            /*  The number of elements is specified in the form itself- and
             *  we add a PSLUINT16 for the count
             */

            cItems = ((PSL_CONST MTPENUMFORM*)precProp->pvForm)->cValues;
            rgszSrc = ((PSLCWSTR*)((PSL_CONST MTPENUMFORM*)precProp->pvForm)->
                            rgValues);

            /*  Serialize the count if necessary
             */

            if (PSLNULL != pvBuf)
            {
                /*  Make sure there is enough space
                 */

                if (sizeof(PSLUINT16) >
                            CALCREMAIN((PSLBYTE*)pvBuf, cbBuf, pbCur))
                {
                    ps = PSLERROR_INSUFFICIENT_BUFFER;
                    goto Exit;
                }

                /*  And store the value
                 */

                MTPStoreUInt16((PXPSLUINT16)pbCur, (PSLUINT16)cItems);
            }
            pbCur += sizeof(PSLUINT16);
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Loop through and store each of the strings- we do not worry about
         *  checking for the buffer to be exceeded because it is handled by
         *  MTPStoreMTPString directly.
         */

        for (idxItem = 0; (idxItem < cItems); idxItem++)
        {
            /*  Either serialize or determine the length
             */

            ps = MTPStoreMTPString((PXMTPSTRING)*ppbDest,
                            CALCREMAIN((PSLBYTE*)pvBuf, cbBuf, pbCur), &cbUsed,
                            rgszSrc[idxItem], MAX_MTP_STRING_LEN);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            pbCur += cbUsed;
        }

        /*  Report number of bytes used/required
         */

        *pcbUsed = pbCur - (PSLBYTE*)pvBuf;
        ps = PSLSUCCESS;
        break;

    case MTP_FORMFLAG_REGEXP:
        /*  And handle the serialization
         */

        ps = MTPStoreMTPString((PXMTPSTRING)pvBuf, cbBuf, pcbUsed,
                            (PSLCWSTR)precProp->pvForm, MAX_MTP_STRING_LEN);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTP_FORMFLAG_FIXEDARRAY:
        /*  Form is a single UINT32 value indicating the maximum length
         */

        cbUsed = sizeof(PSLUINT32);

        /*  Return length if that is all that is requested
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = cbUsed;
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure there is enough space to store the result
         */

        if (cbUsed > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  And send the data
         */

        MTPStoreUInt32((PXPSLUINT32)pvBuf, (PSLUINT32)precProp->pvForm);
        *pcbUsed = cbUsed;
        ps = PSLSUCCESS;
        break;

    case MTP_FORMFLAG_NONE:
    case MTP_FORMFLAG_DATETIME:
        /*  No form data is associated with these forms
         */

        PSLASSERT(0 == *pcbUsed);
        ps = PSLSUCCESS;
        break;

    case MTP_FORMFLAG_BYTEARRAY:
    case MTP_FORMFLAG_LONGSTRING:
    case MTP_FORMFLAG_OBJECTID:
    case MTP_FORMFLAG_SERVICEEXT:
    case MTP_FORMFLAG_SERVICEMETHOD:
        /*  These forms do not apply to strings or cannot be used with
         *  general datatypes
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;

    default:
        /*  Unknown form type
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPPropertySerializeForm
 *
 *  Serializes the property form based on the property record.
 *
 *  Arguments:
 *      MTPPROPERTYREC* precProp            Record for form to serialize
 *      PSLVOID*        pvBuf               Destination buffer
 *      PSLUINT32       cbBuf               Size of the destination buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPPropertySerializeForm(PSL_CONST MTPPROPERTYREC* precProp,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLUINT32       cbItem;
    PSLUINT32       cbSize;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == precProp) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Look for data types that we handle specially
     */

    if (MTP_DATATYPE_STRING == precProp->wDataType)
    {
        ps = _MTPPropertySerializeStringForm(precProp, pvBuf, cbBuf, pcbUsed);
        goto Exit;
    }

    /*  Decide what type of form to serialize
     */

    switch (precProp->bForm)
    {
    case MTP_FORMFLAG_RANGE:
    case MTP_FORMFLAG_ENUM:
        /*  Make sure that we have a form
         */

        if (PSLNULL == precProp->pvForm)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Currently we do not support this form for array values
         */

        PSLASSERT(!(MTP_DATATYPE_ARRAY & precProp->wDataType));
        if (MTP_DATATYPE_ARRAY & precProp->wDataType)
        {
            ps = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }

        /*  Determine the size of the base data element
         */

        ps = MTPPropertyGetMinSizeFromType(precProp->wDataType, &cbItem);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Determine the size of the data
         */

        switch (precProp->bForm)
        {
        case MTP_FORMFLAG_RANGE:
            /*  We return three values (min, max, step) in the data type size
             */

            cbSize = 3 * cbItem;
            break;

        case MTP_FORMFLAG_ENUM:
            /*  The number of elements is specified in the form itself- and
             *  we add a PSLUINT16 for the count
             */

            cbSize = sizeof(PSLUINT16) + (
                ((PSL_CONST MTPENUMFORM*)precProp->pvForm)->cValues * cbItem);
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Report the size if that is all that is requested
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = cbSize;
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure there is enough space to store the result
         */

        if (cbSize > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  And output the values
         */

        switch (precProp->bForm)
        {
        case MTP_FORMFLAG_RANGE:
            /*  We return three values (min, max, step) in the data type size
             */

            MTPStoreUIntArray(pvBuf, precProp->pvForm, 3, cbItem);
            break;

        case MTP_FORMFLAG_ENUM:
            /*  Output the number of items as PSLUINT16
             */

            MTPStoreUInt16((PXPSLUINT16)pvBuf,
                        ((PSL_CONST MTPENUMFORM*)precProp->pvForm)->cValues);
            pvBuf = (PSLBYTE*)pvBuf + sizeof(PSLUINT16);
            MTPStoreUIntArray(pvBuf,
                        ((PSL_CONST MTPENUMFORM*)precProp->pvForm)->rgValues,
                        ((PSL_CONST MTPENUMFORM*)precProp->pvForm)->cValues,
                        cbItem);
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }
        *pcbUsed = cbSize;
        ps = PSLSUCCESS;
        break;

    case MTP_FORMFLAG_FIXEDARRAY:
    case MTP_FORMFLAG_BYTEARRAY:
    case MTP_FORMFLAG_LONGSTRING:
        /*  Form only applies to array types
         */

        if (!(MTP_DATATYPE_ARRAY & precProp->wDataType))
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Form is a single UINT32 value indicating the maximum length
         */

        cbSize = sizeof(PSLUINT32);

        /*  Return length if that is all that is requested
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = cbSize;
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure there is enough space to store the result
         */

        if (cbSize > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  And send the data
         */

        MTPStoreUInt32((PXPSLUINT32)pvBuf, (PSLUINT32)precProp->pvForm);
        *pcbUsed = cbSize;
        ps = PSLSUCCESS;
        break;

    case MTP_FORMFLAG_NONE:
    case MTP_FORMFLAG_DATETIME:
    case MTP_FORMFLAG_OBJECTID:
        /*  No form data is associated with these forms
         */

        PSLASSERT(0 == *pcbUsed);
        ps = PSLSUCCESS;
        break;

    case MTP_FORMFLAG_REGEXP:
    case MTP_FORMFLAG_SERVICEEXT:
    case MTP_FORMFLAG_SERVICEMETHOD:
        /*  These are special forms which cannot be serialized by a general
         *  routine
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;

    default:
        /*  Unknown form type
         */

        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPPropDescContextDestroy
 *
 *  Destroys the serialization context used by the property description
 *  serializer.  Note that this will release the context of the wrapped
 *  PROPDESCSERIALIZEINFO so it should only be used for the top-most
 *  context in the serialization stack.
 *
 *  Arguments:
 *      MTPSERIALIZEINFO*
 *                      rgmsiTemplate       Template being destroyed
 *      PSLPARAM        aContext            Context being destroyed
 *      PSLVOID*        pvOffset            Offset being destroyed
 *
 *  Returns:
 *      Nothing
 */

PSLVOID PSL_API _MTPPropDescContextDestroy(
                    PSL_CONST MTPSERIALIZEINFO* rgmsiTemplate,
                    PSLPARAM aContext, PSL_CONST PSLVOID* pvOffset)
{
    PROPDESCSERIALIZEINFO*  pdiProp;

    /*  Validate arguments- note that we do not reject the PSLNULL case so
     *  make sure to protect all accesses through pdiProp.
     */

    PSLASSERT((PSLNULL != rgmsiTemplate) && (PSLNULL != aContext) &&
            (aContext == (PSLPARAM)pvOffset));

    /*  Extract our object
     */

    pdiProp = (PROPDESCSERIALIZEINFO*)aContext;
    PSLASSERT(PROPDESCSERIALIZEINFO_COOKIE == pdiProp->dwCookie);

    /*  And return it to the object pool
     */

    SAFE_PSLOBJECTPOOLFREE(pdiProp);
    return;
    rgmsiTemplate;
    pvOffset;
}


/*  _MTPPropDescSerializeValue
 *
 *  Callback for serializing values in property descriptions
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context for the callback
 *      PSLPARAM        aValueContext       Context for the value
 *      PSLUINT16       wType               Type of the value
 *      PSLVOID*        pvBuf               Location to return value
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPPropDescSerializeValue( PSLPARAM aContext,
                    PSLPARAM aValueContext,  PSLUINT16 wType,  PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,  PSLUINT32* pcbUsed)
{
    PSLSTATUS                   ps;
    PROPDESCSERIALIZEINFO*      pdiProp;
    PSL_CONST MTPPROPERTYINFO*  pinfoProp;
    PFNMTPPROPGETVALUE          pfnGetProperty;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pdiProp = (PROPDESCSERIALIZEINFO*)aContext;
    PSLASSERT(PROPDESCSERIALIZEINFO_COOKIE == pdiProp->dwCookie);
    pinfoProp = &(pdiProp->info.infoProp);

    /*  Determine which value is being requested
     */

    switch (aValueContext)
    {
    case MTPPROPDESCVALUE_DEFAULT_VALUE:
    case MTPPROPDESCVALUE_CURRENT_VALUE:
        /*  Validate the type
         */

        if ((MTP_DATATYPE_UNDEFINED != wType) &&
            (wType != pinfoProp->precProp->wDataType))
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Retrieve the appropriate function
         */

        switch (aValueContext)
        {
        case MTPPROPDESCVALUE_DEFAULT_VALUE:
            pfnGetProperty = pinfoProp->pfnMTPPropGetDefaultValue;
            break;

        case MTPPROPDESCVALUE_CURRENT_VALUE:
            pfnGetProperty = pinfoProp->pfnMTPPropGetValue;
            break;

        default:
            pfnGetProperty = PSLNULL;
            break;
        }

        /*  If a function is provided use it to retrieve the value- otherwise
         *  use the default functionality
         */

        if (PSLNULL != pfnGetProperty)
        {
            /*  Use the callback
             */

            ps = pfnGetProperty(pinfoProp->wPropCode,
                            pinfoProp->precProp->wDataType, pdiProp->aContext,
                            pinfoProp->aContextProp, pvBuf, cbBuf, pcbUsed);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        else
        {
            /*  Use the helper function
             */

            ps = MTPPropertySerializeDefaultValue(pinfoProp->precProp, pvBuf,
                            cbBuf, pcbUsed);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        break;

    case MTPPROPDESCVALUE_FORM:
        /*  Validate the type
         */

        if (MTP_DATATYPE_UNDEFINED != wType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  And use the helper to generate the form
         */

        ps = MTPPropertySerializeForm(pinfoProp->precProp, pvBuf, cbBuf,
                    pcbUsed);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    default:
        /*  Unknown property requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPPropertyDescSerializeCreate
 *
 *  Creates a serialization object capable of serializing the specified
 *  property description.
 *
 *  Arguments:
 *      MTPPROPERTYINFO*
 *                      pinfoProp           Property info
 *      PSLUINT16       wPropFlags          Property flags
 *      PSLPARAM        aContext            Context to pass to GetValue function
 *      MTPSERIALIZE*   pmso                Resulting serializer
 */

PSLSTATUS PSL_API MTPPropertyDescSerializeCreate(
                    PSL_CONST MTPPROPERTYINFO* pinfoProp, PSLUINT16 wPropFlags,
                    PSLPARAM aContext, MTPSERIALIZE* pmso)
{
    PSLUINT32                   cbInfo;
    PSLUINT32                   cTemplate;
    PSLSTATUS                   ps;
    PROPDESCSERIALIZEINFO*      pdiProp = PSLNULL;
    PSL_CONST MTPSERIALIZEINFO* rgmsiTemplate;

    /*  Clear results for safety
     */

    if (PSLNULL != pmso)
    {
        *pmso = PSLNULL;
    }

    /*  Validate arguments- we make sure that if we were handed a property
     *  record to serialize it is valid for the requested serialization.
     */

    if ((PSLNULL == pinfoProp) || ((PSLNULL != pinfoProp->precProp) &&
        !((PROPFLAGS_TYPE_MASK & wPropFlags) & pinfoProp->precProp->wFlags)) ||
        (PSLNULL == pmso))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine which template to use
     */

    switch (wPropFlags)
    {
    case PROPFLAGS_TYPE_DEVICE:
        /*  Serializing a Device Property Description
         */

        rgmsiTemplate = _RgmsiDevicePropDesc;
        cTemplate = PSLARRAYSIZE(_RgmsiDevicePropDesc);
        cbInfo = sizeof(MTPDEVICEPROPINFO);
        break;

    case PROPFLAGS_TYPE_OBJECT:
        /*  Serializing an Object Property Description
         */

        rgmsiTemplate = _RgmsiObjectPropDesc;
        cTemplate = PSLARRAYSIZE(_RgmsiObjectPropDesc);
        cbInfo = sizeof(MTPOBJECTPROPINFO);
        break;

    case PROPFLAGS_TYPE_SERVICE:
        /*  Serializing a Service Property Description
         */

        rgmsiTemplate = _RgmsiServicePropDesc;
        cTemplate = PSLARRAYSIZE(_RgmsiServicePropDesc);
        cbInfo = sizeof(MTPSERVICEPROPERTYINFO);
        break;

    case PROPFLAGS_TYPE_SERVICEOBJECT:
        /*  Serializing a Service Object Property Description
         */

        rgmsiTemplate = _RgmsiServiceObjectPropDesc;
        cTemplate = PSLARRAYSIZE(_RgmsiServiceObjectPropDesc);
        cbInfo = sizeof(MTPOBJECTPROPINFO);
        break;

    case PROPFLAGS_TYPE_METHODOBJECT:
        /*  Serializing a Method Object Description
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;

    default:
        /*  Unknown property description
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Retrieve a property description object to store the temporary data
     *  inside of
     */

    ps = PSLObjectPoolAlloc(_OpPropDescInfo, &pdiProp);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And initialize it
     */

#ifdef PSL_ASSERTS
    pdiProp->dwCookie = PROPDESCSERIALIZEINFO_COOKIE;
#endif  /* PSL_ASSERTS */
    pdiProp->aContext = aContext;

    /*  Copy the property information locally so that we do not have to
     *  worry about callbacks
     */

    PSLCopyMemory(&(pdiProp->info), pinfoProp, cbInfo);

    /*  Lookup the default property entry if required
     */

    if (PSLNULL == pdiProp->info.infoProp.precProp)
    {
        /*  Use the default entry
         */

        ps = MTPPropertyGetDefaultRec(pinfoProp->wPropCode, wPropFlags,
                            &(pdiProp->info.infoProp.precProp));
        if (PSLSUCCESS != ps)
        {
            PSLASSERT(PSL_FAILED(ps));
            ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }
    }

    /*  Create the serializer
     */

    ps = MTPSerializeCreate(rgmsiTemplate, cTemplate, (PSLPARAM)pdiProp,
                            pdiProp, _MTPPropDescContextDestroy, pmso);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pdiProp = PSLNULL;

Exit:
    SAFE_PSLOBJECTPOOLFREE(pdiProp);
    return ps;
}


/*  MTPPropertyDescSerialize
 *
 *  Serializes a property description into the buffer provided
 *
 *  Arguments:
 *      MTPPROPERTYINFO*
 *                      pinfoProp           Property info
 *      PSLUINT16       wPropFlags          Property flags
 *      PSLPARAM        aContext            Context to pass to GetValue function
 *      PSLVOID*        pvBuf               Buffer to serialize to
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPPropertyDescSerialize(
                    PSL_CONST MTPPROPERTYINFO* pinfoProp, PSLUINT16 wPropFlags,
                    PSLPARAM aContext, PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed)
{
    PSLUINT32       cbWritten;
    PSLUINT64       cbLeft;
    MTPSERIALIZE    msoPropDesc = PSLNULL;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments- we only check the pcbUsed requirement here because
     *  the rest will be validated when we create the serializer
     */

    if (PSLNULL == pcbUsed)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create a serializer for this property description
     */

    ps = MTPPropertyDescSerializeCreate(pinfoProp, wPropFlags, aContext,
                            &msoPropDesc);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And do the actual serialization
     */

    ps = MTPSerialize(msoPropDesc, pvBuf, cbBuf, &cbWritten, &cbLeft);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  If we did not get the entire object report a buffer size issue
     */

    if ((PSLNULL != pvBuf) && (0 != cbLeft))
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Return the number of bytes used
     */

    *pcbUsed = (PSLNULL != pvBuf) ? cbWritten : (PSLUINT32)cbLeft;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSERIALIZEDESTROY(msoPropDesc);
    return ps;
}

