/*
 *  MTPPropertySchema.c
 *
 *  Contains definition of the MTP property schema
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPropertyPrecomp.h"
#include "ContactDeviceService.h"
#include "CalendarDeviceService.h"
#include "TaskDeviceService.h"

/*  Local Defines
 */

#define BATTERYLEVEL_RANGEMIN           0
#define BATTERYLEVEL_RANGEMAX           100
#define BATTERYLEVEL_RANGESTEP          10

#define MTP_NONCONSUMABLE_TRUE          0x1
#define MTP_NONCONSUMABLE_FALSE         0x0

#define AUDIOSAMPLERATE_RANGEMIN        0L
#define AUDIOSAMPLERATE_RANGESTEP       50L

#define AUDIOBITRATE_RANGEMIN           0L
#define AUDIOBITRATE_RANGEMAX           5000000
#define AUDIOBITRATE_RANGESTEP          1
#define AUDIOBITRATE_RANGESTEP1         1000L

#define RATING_RANGEMIN                 0
#define RATING_RANGEMAX                 100
#define RATING_RANGESTEP                1

#define DURATION_RANGEMIN               0
#define DURATION_RANGEMAX               0xFFFFFFFF /* 4 byte max */
#define DURATION_RANGESTEP              1

#define WIDTH_RANGEMIN                  0
#define WIDTH_RANGEMAX                  2000
#define WIDTH_RANGESTEP                 1

#define HEIGHT_RANGEMIN                 0
#define HEIGHT_RANGEMAX                 1500
#define HEIGHT_RANGESTEP                1

#define MTP_DRMPROTECTION_TRUE          1
#define MTP_DRMPROTECTION_FALSE         0

#define MTP_BITRATETYPE_NONE            0x0000
#define MTP_BITRATETYPE_1               0x0001
#define MTP_BITRATETYPE_2               0x0002
#define MTP_BITRATETYPE_3               0x0003

#define MTP_AUDIOBITDEPTH_NONE          0x0000
#define MTP_AUDIOBITDEPTH_1             0x0008
#define MTP_AUDIOBITDEPTH_2             0x000A
#define MTP_AUDIOBITDEPTH_3             0x000C
#define MTP_AUDIOBITDEPTH_4             0x0010
#define MTP_AUDIOBITDEPTH_5             0x0018
#define MTP_AUDIOBITDEPTH_6             0x0020

#define MTP_VIDEOFOURCC_NONE            0x0000

#define MTP_FRAMESPERMILLISECOND_NONE   0
#define MTP_FRAMESPERMILLISECOND_1      15000
#define MTP_FRAMESPERMILLISECOND_2      25000
#define MTP_FRAMESPERMILLISECOND_3      29970
#define MTP_FRAMESPERMILLISECOND_4      60000

#define MTP_ENCODINGPROFILE_SP          L"SP"
#define MTP_ENCODINGPROFILE_MP          L"MP"

#define MTP_VIDEOBITRATE_RANGEMIN       1
#define MTP_VIDEOBITRATE_RANGEMAX       100000000
#define MTP_VIDEOBITRATE_RANGESTEP      1

#define MTP_KEYFRAMEDISTANCE_RANGEMIN   100
#define MTP_KEYFRAMEDISTANCE_RANGEMAX   300
#define MTP_KEYFRAMEDISTANCE_RANGESTEP  1

#define MTP_ENCODINGQUALITY_RANGEMIN    0
#define MTP_ENCODINGQUALITY_RANGEMAX    100
#define MTP_ENCODINGQUALITY_RANGESTEP   1

/*  Local Types
 */

typedef struct _PROPERTYSCHEMA
{
    PSLUINT16               wPropCode;
    MTPPROPERTYREC          recProp;
} PROPERTYSCHEMA;

typedef struct  _OBJPROPERTYSCHEMA
{
    PSLUINT16               wPropCode;
    MTPSERVICEPROPERTYREC   recProp;
} OBJPROPERTYSCHEMA;


/*  Local Functions
 */

#ifdef PSL_ASSERTS
PSLBOOL _ValidateTable(PSLUINT16 wPropFlags,
                    PSL_CONST PSLVOID* pvTable,
                    PSLUINT32 cItems);
#endif /*PSL_ASSERTS*/

PSLINT32 PSL_API _PropCompareCode(PSL_CONST PSLVOID* pvKey,
                    PSL_CONST PSLVOID* pvComp);

/*  Local Variables
 */

static PSL_CONST PSLUINT16      _RgwBoolean16[] =
{
    MTP_BOOLEAN_FALSE,
    MTP_BOOLEAN_TRUE
};

static PSL_CONST MTPENUMFORM    _EfBoolean16 =
{
    PSLARRAYSIZE(_RgwBoolean16),
    _RgwBoolean16
};

static PSL_CONST PSLUINT8       _RgbBoolean[] =
{
    MTP_BOOLEAN_TRUE,
    MTP_BOOLEAN_FALSE,
};

static PSL_CONST MTPENUMFORM    _EfBoolean =
{
    PSLARRAYSIZE(_RgbBoolean),
    _RgbBoolean
};

static PSL_CONST PSLUINT8       _RgbBatteryLevel[] =
{
    BATTERYLEVEL_RANGEMIN,
    BATTERYLEVEL_RANGEMAX,
    BATTERYLEVEL_RANGESTEP
};

static PSL_CONST PSLUINT16      _RgwProtectionStatus[] =
{
    MTP_PROTECTIONSTATUS_NONE,
    MTP_PROTECTIONSTATUS_READONLY,
    MTP_PROTECTIONSTATUS_READONLYDATA,
    MTP_PROTECTIONSTATUS_NONTRANSFERRABLEDATA
};

static PSL_CONST MTPENUMFORM    _EfProtectionStatus =
{
    PSLARRAYSIZE(_RgwProtectionStatus),
    _RgwProtectionStatus
};

static PSL_CONST PSLUINT8       _RgbNonConsumable[] =
{
    MTP_NONCONSUMABLE_TRUE,
    MTP_NONCONSUMABLE_FALSE
};

static PSL_CONST MTPENUMFORM    _EfNonConsumable[] =
{
    PSLARRAYSIZE(_RgbNonConsumable),
    _RgbNonConsumable
};

static PSL_CONST PSLUINT16      _RgwMetaGenre[] =
{
    MTP_METAGENRE_NOT_USED,
    MTP_METAGENRE_GENERIC_MUSIC_AUDIO_FILE,
    MTP_METAGENRE_GENERIC_NONMUSIC_AUDIO_FILE,
    MTP_METAGENRE_SPOKEN_WORD_AUDIO_BOOK_FILES,
    MTP_METAGENRE_SPOKEN_WORD_NONAUDIO_BOOK_FILES,
    MTP_METAGENRE_SPOKEN_WORD_NEWS,
    MTP_METAGENRE_SPOKEN_WORD_TALK_SHOWS,
    MTP_METAGENRE_GENERIC_VIDEO_FILE,
    MTP_METAGENRE_NEWS_VIDEO_FILE,
    MTP_METAGENRE_MUSIC_VIDEO_FILE,
    MTP_METAGENRE_HOME_VIDEO_FILE,
    MTP_METAGENRE_FEATURE_FILM_VIDEO_FILE,
    MTP_METAGENRE_TV_SHOW_VIDEO_FILE,
    MTP_METAGENRE_TRAINING_VIDEO_FILE,
    MTP_METAGENRE_PHOTO_MONTAGE_VIDEO_FILE,
    MTP_METAGENRE_GENERIC_NONAUDIO_NONVIDEO_FILE,
};

static PSL_CONST MTPENUMFORM    _EfMetaGenre =
{
    PSLARRAYSIZE(_RgwMetaGenre),
    _RgwMetaGenre
};

static PSL_CONST PSLUINT32      _RgdwAudioSample[]=
{
    AUDIOSAMPLERATE_RANGEMIN,
    MTP_AUDIO_SAMPLERATE_48K,
    AUDIOSAMPLERATE_RANGESTEP
};

static PSL_CONST PSLUINT16      _RgwChannelsUsed[] =
{
    MTP_CHANNELS_NOT_USED,
    MTP_CHANNELS_MONO,
    MTP_CHANNELS_STEREO
};

static PSL_CONST MTPENUMFORM    _EfChannelsUsed =
{
    PSLARRAYSIZE(_RgwChannelsUsed),
    _RgwChannelsUsed
};

static PSL_CONST PSLUINT32      _RgdwAudioWaveCodec[] =
{
    MTP_WAVEFORMAT_UNKNOWN,
    MTP_WAVEFORMAT_PCM,
    MTP_WAVEFORMAT_ADPCM,
    MTP_WAVEFORMAT_IEEEFLOAT,
    MTP_WAVEFORMAT_DTS,
    MTP_WAVEFORMAT_DRM,
    MTP_WAVEFORMAT_WMSP2,
    MTP_WAVEFORMAT_GSM610,
    MTP_WAVEFORMAT_MSNAUDIO,
    MTP_WAVEFORMAT_MPEG,
    MTP_WAVEFORMAT_MPEGLAYER3,
    MTP_WAVEFORMAT_MSAUDIO1,
    MTP_WAVEFORMAT_MSAUDIO2,
    MTP_WAVEFORMAT_MSAUDIO3,
    MTP_WAVEFORMAT_WMAUDIOLOSSLESS,
    MTP_WAVEFORMAT_WMASPDIF,
    MTP_WAVEFORMAT_AAC
};

static PSL_CONST MTPENUMFORM    _EfAudioWaveCodec[] =
{
    PSLARRAYSIZE(_RgdwAudioWaveCodec),
    _RgdwAudioWaveCodec
};

static PSL_CONST PSLUINT32      _RgdwAudioBitrate[] =
{
    AUDIOBITRATE_RANGEMIN,
    AUDIOBITRATE_RANGEMAX,
    AUDIOBITRATE_RANGESTEP
};

static PSL_CONST PSLUINT16      _RgwRating[] =
{
    RATING_RANGEMIN,
    RATING_RANGEMAX,
    RATING_RANGESTEP
};

static PSL_CONST PSLUINT32      _RgdwWidth[] =
{
    WIDTH_RANGEMIN,
    WIDTH_RANGEMAX,
    WIDTH_RANGESTEP
};

static PSL_CONST PSLUINT32      _RgdwHeight[] =
{
    HEIGHT_RANGEMIN,
    HEIGHT_RANGEMAX,
    HEIGHT_RANGESTEP
};

static PSL_CONST PSLUINT32      _RgdwDuration[] =
{
    DURATION_RANGEMIN,
    DURATION_RANGEMAX,
    DURATION_RANGESTEP
};

static PSL_CONST PSLUINT16      _RgwDRMProtection[] =
{
    MTP_DRMPROTECTION_TRUE,
    MTP_DRMPROTECTION_FALSE
};

static PSL_CONST MTPENUMFORM    _EfDRMProtection =
{
    PSLARRAYSIZE(_RgwDRMProtection),
    _RgwDRMProtection
};

static PSL_CONST PSLUINT16      _RgwBitrateType[] =
{
    MTP_BITRATETYPE_NONE,
    MTP_BITRATETYPE_1,
    MTP_BITRATETYPE_2,
    MTP_BITRATETYPE_3
};

static PSL_CONST MTPENUMFORM    _EfBitrateType =
{
    PSLARRAYSIZE(_RgwBitrateType),
    _RgwBitrateType
};

static PSL_CONST PSLUINT32      _RgdwAudioBitDepth[] =
{
    MTP_AUDIOBITDEPTH_NONE,
    MTP_AUDIOBITDEPTH_1,
    MTP_AUDIOBITDEPTH_2,
    MTP_AUDIOBITDEPTH_3,
    MTP_AUDIOBITDEPTH_4,
    MTP_AUDIOBITDEPTH_5,
    MTP_AUDIOBITDEPTH_6,
};

static PSL_CONST MTPENUMFORM    _EfAudioBitDepth[] =
{
    PSLARRAYSIZE(_RgdwAudioBitDepth),
    _RgdwAudioBitDepth
};

static PSL_CONST PSLUINT16      _RgwScanType[] =
{
    MTP_SCANTYPE_NOT_USED,
    MTP_SCANTYPE_PROGESSIVE,
    MTP_SCANTYPE_FIELDINTERLEAVEDUPPERFIRST,
    MTP_SCANTYPE_FIELDINTERLEAVEDLOWERFIRST,
    MTP_SCANTYPE_FIELDSINGLEUPPERFIRST,
    MTP_SCANTYPE_FIELDSINGLELOWERFIRST,
    MTP_SCANTYPE_MIXEDINTERLACE,
    MTP_SCANTYPE_MIXEDINTERLACEANDPROGRESSIVE,
};

static PSL_CONST MTPENUMFORM    _EfScanType =
{
    PSLARRAYSIZE(_RgwScanType),
    _RgwScanType
};

static PSL_CONST PSLUINT32      _RgdwVideoFourCC[] =
{
    MTP_VIDEOFOURCC_NONE,
    MTP_VIDEOFOURCC_H263,
    MTP_VIDEOFOURCC_H269,
    MTP_VIDEOFOURCC_MPEG,
    MTP_VIDEOFOURCC_WMV1,
    MTP_VIDEOFOURCC_WMV2,
    MTP_VIDEOFOURCC_WMV3
};

static PSL_CONST MTPENUMFORM    _EfVideoFourCC =
{
    PSLARRAYSIZE(_RgdwVideoFourCC),
    _RgdwVideoFourCC
};

static PSL_CONST PSLUINT32      _RgdwVideoBitrate[] =
{
    MTP_VIDEOBITRATE_RANGEMIN,
    MTP_VIDEOBITRATE_RANGEMAX,
    MTP_VIDEOBITRATE_RANGESTEP
};

static PSL_CONST PSLUINT32      _RgdwFramesPerMS[] =
{
    MTP_FRAMESPERMILLISECOND_NONE,
    MTP_FRAMESPERMILLISECOND_1,
    MTP_FRAMESPERMILLISECOND_2,
    MTP_FRAMESPERMILLISECOND_3,
    MTP_FRAMESPERMILLISECOND_4
};

static PSL_CONST MTPENUMFORM    _EfFramesPerMS =
{
    PSLARRAYSIZE(_RgdwFramesPerMS),
    _RgdwFramesPerMS
};

static PSL_CONST PSLUINT32      _RgdwKeyFrameDist[] =
{
    MTP_KEYFRAMEDISTANCE_RANGEMIN,
    MTP_KEYFRAMEDISTANCE_RANGEMAX,
    MTP_KEYFRAMEDISTANCE_RANGESTEP
};

static PSL_CONST PSLUINT32      _RgdwEncodingQlty[] =
{
    MTP_ENCODINGQUALITY_RANGEMIN,
    MTP_ENCODINGQUALITY_RANGEMAX,
    MTP_ENCODINGQUALITY_RANGESTEP
};

static PSL_CONST PSLUINT16      _RgwAssociationTypes[] =
{
    MTP_ASSOCIATIONTYPE_GENERICFOLDER,
    MTP_ASSOCIATIONTYPE_UNDEFINED
};

static PSL_CONST MTPENUMFORM    _EfAssociationTypes =
{
    PSLARRAYSIZE(_RgwAssociationTypes),
    _RgwAssociationTypes
};

static PSL_CONST PSLWCHAR*      _RgEncodingProfiles[] =
{
    MTP_ENCODINGPROFILE_SP,
    MTP_ENCODINGPROFILE_MP
};

static PSL_CONST MTPENUMFORM    _EfEncodingProfiles =
{
    PSLARRAYSIZE(_RgEncodingProfiles),
    _RgEncodingProfiles
};

static PSL_CONST PSLUINT32      _RgdwSampleRate[] =
{
    MTP_AUDIO_SAMPLERATE_32K,
    MTP_AUDIO_SAMPLERATE_CD
};

static PSL_CONST MTPENUMFORM    _EfSampleRate =
{
    PSLARRAYSIZE(_RgdwSampleRate),
    _RgdwSampleRate
};

static PSL_CONST PSLWCHAR       _RgwchRegExpAll[] = L".*";

static PSL_CONST PSLWCHAR       _RgwchRegExpLoc[] =
                                    L"[a-zA-Z]{2,3}(-[a-zA-Z]{2})?";

static PSL_CONST PSLUINT16      _RgwMessageRead[] =
{
    ENUM_MessageObj_ReadFalse,
    ENUM_MessageObj_ReadTrue,
};

static PSL_CONST MTPENUMFORM    _EfMessageRead =
{
    PSLARRAYSIZE(_RgwMessageRead),
    _RgwMessageRead
};

static PROPERTYSCHEMA           _RgmDevPropertyTbl[] =
{
    {
        MTP_DEVICEPROPCODE_BATTERYLEVEL,
        {
            PROPFLAGS_TYPE_DEVICE,
            MTP_DATATYPE_UINT8,
            MTP_FORMFLAG_RANGE,
            _RgbBatteryLevel
        },
    },
    {
        MTP_DEVICEPROPCODE_FUNCTIONALID,
        {
            PROPFLAGS_TYPE_DEVICE,
            MTP_DATATYPE_UINT128,
            MTP_FORMFLAG_NONE,
            PSLNULL
        },
    },
    {
        MTP_DEVICEPROPCODE_MODELID,
        {
            PROPFLAGS_TYPE_DEVICE,
            MTP_DATATYPE_UINT128,
            MTP_FORMFLAG_NONE,
            PSLNULL
        },
    },
    {
        MTP_DEVICEPROPCODE_SYNCHRONIZATIONPARTNER,
        {
            PROPFLAGS_TYPE_DEVICE,
            MTP_DATATYPE_STRING,
            MTP_FORMFLAG_NONE,
            PSLNULL
        },
    },
    {
        MTP_DEVICEPROPCODE_DEVICEFRIENDLYNAME,
        {
            PROPFLAGS_TYPE_DEVICE,
            MTP_DATATYPE_STRING,
            MTP_FORMFLAG_NONE,
            PSLNULL
        }
    },
};

static OBJPROPERTYSCHEMA        _RgmObjPropertyTbl[] =
{
    {
        MTP_OBJECTPROPCODE_STORAGEID,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_StorageID,
            NAME_GenericObj_StorageID
        },
    },
    {
        MTP_OBJECTPROPCODE_OBJECTFORMAT,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_ObjectFormat,
            NAME_GenericObj_ObjectFormat
        },
    },
    {
        MTP_OBJECTPROPCODE_PROTECTIONSTATUS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfProtectionStatus,
            },
            &PKEY_GenericObj_ProtectionStatus,
            NAME_GenericObj_ProtectionStatus,
        },
    },
    {
        MTP_OBJECTPROPCODE_OBJECTSIZE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT64,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_ObjectSize,
            NAME_GenericObj_ObjectSize,
        },
    },
    {
        MTP_OBJECTPROPCODE_ASSOCIATIONTYPE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfAssociationTypes
            },
            &PKEY_GenericObj_AssociationType,
            NAME_GenericObj_AssociationType,
        },
    },
    {
        MTP_OBJECTPROPCODE_ASSOCIATIONDESC,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_AssociationDesc,
            NAME_GenericObj_AssociationDesc,
        },
    },
    {
        MTP_OBJECTPROPCODE_OBJECTFILENAME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_ObjectFileName,
            NAME_GenericObj_ObjectFileName,
        },
    },
    {
        MTP_OBJECTPROPCODE_DATECREATED,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            &PKEY_GenericObj_DateCreated,
            NAME_GenericObj_DateCreated,
        },
    },
    {
        MTP_OBJECTPROPCODE_DATEMODIFIED,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            &PKEY_GenericObj_DateModified,
            NAME_GenericObj_DateModified,
        },
    },
    {
        MTP_OBJECTPROPCODE_KEYWORDS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_Keywords,
            NAME_GenericObj_Keywords,
        },
    },
    {
        MTP_OBJECTPROPCODE_PARENT,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_ParentID,
            NAME_GenericObj_ParentID,
        },
    },
    {
        MTP_OBJECTPROPCODE_ALLOWEDFOLDERCONTENTS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_AllowedFolderContents,
            NAME_GenericObj_AllowedFolderContents,
        },
    },
    {
        MTP_OBJECTPROPCODE_HIDDEN,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfBoolean16
            },
            &PKEY_GenericObj_Hidden,
            NAME_GenericObj_Hidden,
        },
    },
    {
        MTP_OBJECTPROPCODE_SYSTEMOBJECT,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfBoolean16
            },
            &PKEY_GenericObj_SystemObject,
            NAME_GenericObj_SystemObject,
        },
    },
    {
        MTP_OBJECTPROPCODE_PERSISTENTGUID,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT128,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_PersistentUID,
            NAME_GenericObj_PersistentUID,
        },
    },
    {
        MTP_OBJECTPROPCODE_SYNCID,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_SyncID,
            NAME_GenericObj_SyncID,
        },
    },
    {
        MTP_OBJECTPROPCODE_PROPERTYBAG,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_GenericObj_PropertyBag,
            NAME_GenericObj_PropertyBag,
        },
    },
    {
        MTP_OBJECTPROPCODE_NAME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_Name,
            NAME_GenericObj_Name,
        },
    },
    {
        MTP_OBJECTPROPCODE_ARTIST,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_Artist,
            NAME_MediaObj_Artist,
        },
    },
    {
        MTP_OBJECTPROPCODE_DATEAUTHORED,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            &PKEY_GenericObj_LanguageLocale,
            NAME_GenericObj_LanguageLocale
        }
    },
    {
        MTP_OBJECTPROPCODE_DESCRIPTION,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT,
            },
            &PKEY_GenericObj_Description,
            NAME_GenericObj_Description
        }
    },
    {
        MTP_OBJECTPROPCODE_LANGUAGELOCALE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpLoc
            },
            &PKEY_GenericObj_LanguageLocale,
            NAME_GenericObj_LanguageLocale
        }
    },
    {
        MTP_OBJECTPROPCODE_COPYRIGHTINFORMATION,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT,
            },
            &PKEY_GenericObj_Copyright,
            NAME_GenericObj_Copyright
        }
    },
    {
        MTP_OBJECTPROPCODE_SOURCE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_VideoObj_Source,
            NAME_VideoObj_Source
        }
    },
    {
        MTP_OBJECTPROPCODE_ORIGINLOCATION,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_GeographicOrigin,
            NAME_MediaObj_GeographicOrigin
        }
    },
    {
        MTP_OBJECTPROPCODE_DATEADDED,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            &PKEY_GenericObj_DateAdded,
            NAME_GenericObj_DateAdded
        }
    },
    {
        MTP_OBJECTPROPCODE_NONCONSUMABLE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT8,
                MTP_FORMFLAG_ENUM,
                &_EfNonConsumable
            },
            &PKEY_GenericObj_NonConsumable,
            NAME_GenericObj_NonConsumable,
        },
    },
    {
        MTP_OBJECTPROPCODE_CORRUPTUNPLAYABLE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT8,
                MTP_FORMFLAG_ENUM,
                &_EfBoolean
            },
            &PKEY_GenericObj_Corrupt,
            NAME_GenericObj_Corrupt,
        },
    },
    {
        MTP_OBJECTPROPCODE_WIDTH,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_RANGE,
                _RgdwWidth
            },
            &PKEY_MediaObj_Width,
            NAME_MediaObj_Width,
        },
    },
    {
        MTP_OBJECTPROPCODE_HEIGHT,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_RANGE,
                _RgdwHeight
            },
            &PKEY_MediaObj_Height,
            NAME_MediaObj_Height,
        },
    },
    {
        MTP_OBJECTPROPCODE_DURATION,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_RANGE,
                _RgdwDuration
            },
            &PKEY_MediaObj_Duration,
            NAME_MediaObj_Duration,
        },
    },
    {
        MTP_OBJECTPROPCODE_USERRATING,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_RANGE,
                _RgwRating
            },
            &PKEY_MediaObj_UserRating,
            NAME_MediaObj_UserRating,
        },
    },
    {
        MTP_OBJECTPROPCODE_TRACK,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_Track,
            NAME_MediaObj_Track,
        },
    },
    {
        MTP_OBJECTPROPCODE_GENRE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_Genre,
            NAME_MediaObj_Genre,
        },
    },
    {
        MTP_OBJECTPROPCODE_CREDITS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_MediaObj_Credits,
            NAME_MediaObj_Credits,
        },
    },
    {
        MTP_OBJECTPROPCODE_LYRICS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_AudioObj_Lyrics,
            NAME_AudioObj_Lyrics,
        },
    },
    {
        MTP_OBJECTPROPCODE_SUBSCRIPTIONCONTENTID,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_SubscriptionContentID,
            NAME_MediaObj_SubscriptionContentID,
        },
    },
    {
        MTP_OBJECTPROPCODE_PRODUCEDBY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_Producer,
            NAME_MediaObj_Producer,
        },
    },
    {
        MTP_OBJECTPROPCODE_USECOUNT,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_UseCount,
            NAME_MediaObj_UseCount,
        },
    },
    {
        MTP_OBJECTPROPCODE_SKIPCOUNT,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_SkipCount,
            NAME_MediaObj_SkipCount,
        },
    },
    {
        MTP_OBJECTPROPCODE_LASTACCESSED,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            &PKEY_GenericObj_DateAccessed,
            NAME_GenericObj_DateAccessed,
        },
    },
    {
        MTP_OBJECTPROPCODE_PARENTALRATING,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_ParentalRating,
            NAME_MediaObj_ParentalRating,
        },
    },
    {
        MTP_OBJECTPROPCODE_METAGENRE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfMetaGenre
            },
            &PKEY_MediaObj_MediaType,
            NAME_MediaObj_MediaType,
        },
    },
    {
        MTP_OBJECTPROPCODE_COMPOSER,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_Composer,
            NAME_MediaObj_Composer,
        },
    },
    {
        MTP_OBJECTPROPCODE_EFFECTIVERATING,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_RANGE,
                _RgwRating
            },
            &PKEY_MediaObj_EffectiveRating,
            NAME_MediaObj_EffectiveRating,
        },
    },
    {
        MTP_OBJECTPROPCODE_SUBTITLE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_Subtitle,
            NAME_MediaObj_Subtitle,
        },
    },
    {
        MTP_OBJECTPROPCODE_ORIGINALRELEASEDATE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            &PKEY_MediaObj_DateOriginalRelease,
            NAME_MediaObj_DateOriginalRelease,
        },
    },
    {
        MTP_OBJECTPROPCODE_ALBUMNAME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_AlbumName,
            NAME_MediaObj_AlbumName,
        },
    },
    {
        MTP_OBJECTPROPCODE_ALBUMARTIST,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_AlbumArtist,
            NAME_MediaObj_AlbumArtist,
        },
    },
    {
        MTP_OBJECTPROPCODE_MOOD,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_Mood,
            NAME_MediaObj_Mood,
        },
    },
    {
        MTP_OBJECTPROPCODE_DRMPROTECTION,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfDRMProtection
            },
            &PKEY_GenericObj_DRMStatus,
            NAME_GenericObj_DRMStatus
        },
    },
    {
        MTP_OBJECTPROPCODE_SUBDESCRIPTION,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_GenericObj_SubDescription,
            NAME_GenericObj_SubDescription,
        },
    },
    {
        MTP_OBJECTPROPCODE_ISCROPPED,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfBoolean16
            },
            &PKEY_ImageObj_IsCropped,
            NAME_ImageObj_IsCropped,
        },
    },
    {
        MTP_OBJECTPROPCODE_ISCOLOURCORRECTED,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfBoolean16
            },
            &PKEY_ImageObj_IsColorCorrected,
            NAME_ImageObj_IsColorCorrected,
        },
    },
    {
        MTP_OBJECTPROPCODE_IMAGEBITDEPTH,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ImageObj_ImageBitDepth,
            NAME_ImageObj_ImageBitDepth,
        },
    },
    {
        MTP_OBJECTPROPCODE_FNUMBER,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_NONE,
                PSLNULL
             },
            &PKEY_ImageObj_Aperature,
            NAME_ImageObj_Aperature
        },
    },
    {
        MTP_OBJECTPROPCODE_EXPOSURETIME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_NONE,
                PSLNULL
             },
            &PKEY_ImageObj_Exposure,
            NAME_ImageObj_Exposure
        },
    },
    {
        MTP_OBJECTPROPCODE_EXPOSUREINDEX,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_NONE,
                PSLNULL
             },
            &PKEY_ImageObj_ISOSpeed,
            NAME_ImageObj_ISOSpeed
        },
    },
    {
        MTP_OBJECTPROPCODE_DISPLAYNAME,
        {
            {
                PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            PSLNULL,
            PSLNULL,
        },
    },
    {
        MTP_OBJECTPROPCODE_BODYTEXT,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_MessageObj_Body,
            NAME_MessageObj_Body
        },
    },
    {
        MTP_OBJECTPROPCODE_SUBJECT,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MessageObj_Subject,
            NAME_MessageObj_Subject,
        },
    },
    {
        MTP_OBJECTPROPCODE_PRIORITY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            PSLNULL,
            PSLNULL
        },
    },
    {
        MTP_OBJECTPROPCODE_GIVENNAME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_GivenName,
            NAME_ContactObj_GivenName
        },
    },
    {
        MTP_OBJECTPROPCODE_MIDDLENAMES,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_MiddleNames,
            NAME_ContactObj_MiddleNames,
        },
    },
    {
        MTP_OBJECTPROPCODE_FAMILYNAME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_FamilyName,
            NAME_ContactObj_FamilyName,
        },
    },
    {
        MTP_OBJECTPROPCODE_PREFIX,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_Title,
            NAME_ContactObj_Title,
        },
    },
    {
        MTP_OBJECTPROPCODE_SUFFIX,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_Suffix,
            NAME_ContactObj_Suffix,
        },
    },
    {
        MTP_OBJECTPROPCODE_PHONETICGIVENNAME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_PhoneticGivenName,
            NAME_ContactObj_PhoneticGivenName,
        },
    },
    {
        MTP_OBJECTPROPCODE_PHONETICFAMILYNAME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_PhoneticFamilyName,
            NAME_ContactObj_PhoneticFamilyName,
        },
    },
    {
        MTP_OBJECTPROPCODE_EMAILPRIMARY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_Email,
            NAME_ContactObj_Email,
        },
    },
    {
        MTP_OBJECTPROPCODE_EMAILPERSONAL1,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_PersonalEmail,
            NAME_ContactObj_PersonalEmail,
        },
    },
    {
        MTP_OBJECTPROPCODE_EMAILPERSONAL2,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_PersonalEmail2,
            NAME_ContactObj_PersonalEmail2
        },
    },
    {
        MTP_OBJECTPROPCODE_EMAILBUSINESS1,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_BusinessEmail,
            NAME_ContactObj_BusinessEmail,
        },
    },
    {
        MTP_OBJECTPROPCODE_EMAILBUSINESS2,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_BusinessEmail2,
            NAME_ContactObj_BusinessEmail
        },
    },
    {
        MTP_OBJECTPROPCODE_EMAILOTHERS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_ContactObj_OtherEmail,
            NAME_ContactObj_OtherEmail
        },
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERPRIMARY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_Phone,
            NAME_ContactObj_Phone,
        },
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERPERSONAL,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_PersonalPhone,
            NAME_ContactObj_PersonalPhone,
        },
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERPERSONAL2,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_PersonalPhone2,
            NAME_ContactObj_PersonalPhone2
        },
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERBUSINESS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_BusinessPhone,
            NAME_ContactObj_BusinessPhone,
        },
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERBUSINESS2,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_BusinessPhone2,
            NAME_ContactObj_BusinessPhone2
        },
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERMOBILE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_MobilePhone,
            NAME_ContactObj_MobilePhone,
        },
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBERMOBILE2,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_MobilePhone2,
            NAME_ContactObj_MobilePhone2
        },
    },
    {
        MTP_OBJECTPROPCODE_FAXNUMBERPRIMARY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_Fax,
            NAME_ContactObj_Fax,
        },
    },
    {
        MTP_OBJECTPROPCODE_FAXNUMBERPERSONAL,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_PersonalFax,
            NAME_ContactObj_PersonalFax,
        },
    },
    {
        MTP_OBJECTPROPCODE_FAXNUMBERBUSINESS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_BusinessFax,
            NAME_ContactObj_BusinessFax,
        },
    },
    {
        MTP_OBJECTPROPCODE_PAGERNUMBER,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_Pager,
            NAME_ContactObj_Pager,
        },
    },
    {
        MTP_OBJECTPROPCODE_PHONENUMBEROTHERS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_ContactObj_OtherPhone,
            NAME_ContactObj_OtherPhone
        },
    },
    {
        MTP_OBJECTPROPCODE_PRIMARYWEBADDRESS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_WebAddress,
            NAME_ContactObj_WebAddress,
        },
    },
    {
        MTP_OBJECTPROPCODE_PERSONALWEBADDRESS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_PersonalWebAddress,
            NAME_ContactObj_PersonalWebAddress,
        },
    },
    {
        MTP_OBJECTPROPCODE_BUSINESSWEBADDRESS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_BusinessWebAddress,
            NAME_ContactObj_BusinessWebAddress,
        },
    },
    {
        MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_IMAddress,
            NAME_ContactObj_IMAddress,
        },
    },
    {
        MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS2,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_IMAddress2,
            NAME_ContactObj_IMAddress2,
        },
    },
    {
        MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS3,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_REGEXP,
                _RgwchRegExpAll
            },
            &PKEY_ContactObj_IMAddress3,
            NAME_ContactObj_IMAddress3,
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALFULL,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_ContactObj_PersonalAddressFull,
            NAME_ContactObj_PersonalAddressFull
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALLINE1,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_PersonalAddressStreet,
            NAME_ContactObj_PersonalAddressStreet,
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALLINE2,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_PersonalAddressLine2,
            NAME_ContactObj_PersonalAddressLine2,
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALCITY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_PersonalAddressCity,
            NAME_ContactObj_PersonalAddressCity,
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALREGION,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_PersonalAddressRegion,
            NAME_ContactObj_PersonalAddressRegion
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALPOSTALCODE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_PersonalAddressPostalCode,
            NAME_ContactObj_PersonalAddressPostalCode
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALCOUNTRY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_PersonalAddressCountry,
            NAME_ContactObj_PersonalAddressCountry
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSFULL,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_ContactObj_BusinessAddressFull,
            NAME_ContactObj_BusinessAddressFull
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSLINE1,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_BusinessAddressStreet,
            NAME_ContactObj_BusinessAddressStreet
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSLINE2,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_BusinessAddressLine2,
            NAME_ContactObj_BusinessAddressLine2
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSCITY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_BusinessAddressCity,
            NAME_ContactObj_BusinessAddressCity
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSREGION,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_BusinessAddressRegion,
            NAME_ContactObj_BusinessAddressRegion
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSPOSTALCODE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_BusinessAddressPostalCode,
            NAME_ContactObj_BusinessAddressPostalCode
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSCOUNTRY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_BusinessAddressCountry,
            NAME_ContactObj_BusinessAddressCountry
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERFULL,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_ContactObj_OtherAddressFull,
            NAME_ContactObj_OtherAddressFull
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERLINE1,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_OtherAddressStreet,
            NAME_ContactObj_OtherAddressStreet
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERLINE2,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_OtherAddressLine2,
            NAME_ContactObj_OtherAddressLine2
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERCITY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_OtherAddressCity,
            NAME_ContactObj_OtherAddressCity
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERREGION,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_OtherAddressRegion,
            NAME_ContactObj_OtherAddressRegion
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERPOSTALCODE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_OtherAddressPostalCode,
            NAME_ContactObj_OtherAddressPostalCode
        },
    },
    {
        MTP_OBJECTPROPCODE_POSTALADDRESSOTHERCOUNTRY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_OtherAddressCountry,
            NAME_ContactObj_OtherAddressCountry
        },
    },
    {
        MTP_OBJECTPROPCODE_ORGANIZATIONNAME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_Organization,
            NAME_ContactObj_Organization,
        },
    },
    {
        MTP_OBJECTPROPCODE_PHONETICORGANIZATIONNAME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_PhoneticOrganization,
            NAME_ContactObj_PhoneticOrganization
        },
    },
    {
        MTP_OBJECTPROPCODE_ROLE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_ContactObj_Role,
            NAME_ContactObj_Role,
        },
    },
    {
        MTP_OBJECTPROPCODE_BIRTHDAY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            &PKEY_ContactObj_Birthdate,
            NAME_ContactObj_Birthdate,
        },
    },
    {
        MTP_OBJECTPROPCODE_MESSAGETO,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_MessageObj_To,
            NAME_MessageObj_To
        },
    },
    {
        MTP_OBJECTPROPCODE_MESSAGECC,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_MessageObj_CC,
            NAME_MessageObj_To
        },
    },
    {
        MTP_OBJECTPROPCODE_MESSAGEBCC,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_MessageObj_BCC,
            NAME_MessageObj_BCC
        },
    },
    {
        MTP_OBJECTPROPCODE_MESSAGEREAD,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfMessageRead
            },
            &PKEY_MessageObj_Read,
            NAME_MessageObj_Read
        },
    },
    {
        MTP_OBJECTPROPCODE_MESSAGERECEIVETIME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            &PKEY_MessageObj_ReceivedTime,
            NAME_MessageObj_ReceivedTime
        },
    },
    {
        MTP_OBJECTPROPCODE_MESSAGESENDER,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MessageObj_Sender,
            NAME_MessageObj_Sender
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYBEGINTIME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            PSLNULL,
            PSLNULL
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYENDTIME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            PSLNULL,
            PSLNULL
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYLOCATION,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_CalendarObj_Location,
            NAME_CalendarObj_Location
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYREQUIREDATTENDEES,
        {
            {
                PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            PSLNULL,
            PSLNULL
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYOPTIONALATTENDEES,
        {
            {
                PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            PSLNULL,
            PSLNULL
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYRESOURCES,
        {
            {
                PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            PSLNULL,
            PSLNULL
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYACCEPTEDED,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_CalendarObj_Accepted,
            NAME_CalendarObj_Accepted
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYTENTATIVE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_CalendarObj_Tentative,
            NAME_CalendarObj_Tentative
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYDECLINED,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_AUINT16,
                MTP_FORMFLAG_LONGSTRING,
                (PSLVOID*)MTP_LONGSTRING_DEFAULT
            },
            &PKEY_CalendarObj_Declined,
            NAME_CalendarObj_Declined
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYREMINDERTIME,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            PSLNULL,
            PSLNULL
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYOWNER,
        {
            {
                PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            PSLNULL,
            PSLNULL,
        },
    },
    {
        MTP_OBJECTPROPCODE_ACTIVITYSTATUS,
        {
            {
                PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            PSLNULL,
            PSLNULL,
        },
    },
    {
        MTP_OBJECTPROPCODE_OWNER,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_Owner,
            NAME_MediaObj_Owner
        }
    },
    {
        MTP_OBJECTPROPCODE_EDITOR,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_Editor,
            NAME_MediaObj_Editor
        }
    },
    {
        MTP_OBJECTPROPCODE_WEBMASTER,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_WebMaster,
            NAME_MediaObj_WebMaster
        }
    },
    {
        MTP_OBJECTPROPCODE_URLSOURCE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_URLSource,
            NAME_MediaObj_URLSource
        }
    },
    {
        MTP_OBJECTPROPCODE_URLDESTINATION,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_URLLink,
            NAME_MediaObj_URLLink
        }
    },
    {
        MTP_OBJECTPROPCODE_TIMEBOOKMARK,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_BookmarkTime,
            NAME_MediaObj_BookmarkTime
        }
    },
    {
        MTP_OBJECTPROPCODE_OBJECTBOOKMARK,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_BookmarkObject,
            NAME_MediaObj_BookmarkObject
        }
    },
    {
        MTP_OBJECTPROPCODE_BYTEBOOKMARK,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT64,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_BookmarkByte,
            NAME_MediaObj_BookmarkByte
        }
    },
    {
        MTP_OBJECTPROPCODE_LASTBUILDDATE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_DATETIME,
                PSLNULL
            },
            &PKEY_GenericObj_DateRevised,
            NAME_GenericObj_DateRevised
        }
    },
    {
        MTP_OBJECTPROPCODE_TIMETOLIVE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT64,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_GenericObj_TimeToLive,
            NAME_GenericObj_TimeToLive
        }
    },
    {
        MTP_OBJECTPROPCODE_MEDIAGUID,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_MediaUID,
            NAME_MediaObj_MediaUID
        }
    },
    {
        MTP_OBJECTPROPCODE_TOTALBITRATE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_TotalBitRate,
            NAME_MediaObj_TotalBitRate,
        },
    },
    {
        MTP_OBJECTPROPCODE_BITRATETYPE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfBitrateType
            },
            &PKEY_MediaObj_BitRateType,
            NAME_MediaObj_BitRateType,
        },
    },
    {
        MTP_OBJECTPROPCODE_SAMPLERATE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_RANGE,
                _RgdwAudioSample
            },
            &PKEY_MediaObj_SampleRate,
            NAME_MediaObj_SampleRate,
        },
    },
    {
        MTP_OBJECTPROPCODE_NUMBEROFCHANNELS,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfChannelsUsed
            },
            &PKEY_AudioObj_Channels,
            NAME_AudioObj_Channels,
        },
    },
    {
        MTP_OBJECTPROPCODE_AUDIOBITDEPTH,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_ENUM,
                &_EfAudioBitDepth
            },
            &PKEY_AudioObj_AudioBitDepth,
            NAME_AudioObj_AudioBitDepth,
        },
    },
    {
        MTP_OBJECTPROPCODE_SCANTYPE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT16,
                MTP_FORMFLAG_ENUM,
                &_EfScanType
            },
            &PKEY_VideoObj_ScanType,
            NAME_VideoObj_ScanType
        },
    },
    {
        MTP_OBJECTPROPCODE_AUDIOWAVECODEC,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_ENUM,
                &_EfAudioWaveCodec
            },
            &PKEY_AudioObj_AudioFormatCode,
            NAME_AudioObj_AudioFormatCode,
        },
    },
    {
        MTP_OBJECTPROPCODE_AUDIOBITRATE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_RANGE,
                _RgdwAudioBitrate
            },
            &PKEY_AudioObj_AudioBitRate,
            NAME_AudioObj_AudioBitRate,
        },
    },
    {
        MTP_OBJECTPROPCODE_VIDEOFOURCCCODEC,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_ENUM,
                &_EfVideoFourCC
            },
            &PKEY_VideoObj_VideoFormatCode,
            NAME_VideoObj_VideoFormatCode,
        },
    },
    {
        MTP_OBJECTPROPCODE_VIDEOBITRATE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_RANGE,
                _RgdwVideoBitrate
            },
            &PKEY_VideoObj_VideoBitRate,
            NAME_VideoObj_VideoBitRate,
        },
    },
    {
        MTP_OBJECTPROPCODE_FRAMESPERMILLISECOND,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_ENUM,
                &_EfFramesPerMS
            },
            &PKEY_VideoObj_VideoFrameRate,
            NAME_VideoObj_VideoFrameRate,
        },
    },
    {
        MTP_OBJECTPROPCODE_KEYFRAMEDISTANCE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_RANGE,
                _RgdwKeyFrameDist
            },
            &PKEY_VideoObj_KeyFrameDistance,
            NAME_VideoObj_KeyFrameDistance,
        },
    },
    {
        MTP_OBJECTPROPCODE_BUFFERSIZE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_NONE,
                PSLNULL
            },
            &PKEY_MediaObj_BufferSize,
            NAME_MediaObj_BufferSize,
        },
    },
    {
        MTP_OBJECTPROPCODE_ENCODINGQUALITY,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_UINT32,
                MTP_FORMFLAG_RANGE,
                _RgdwEncodingQlty
            },
            &PKEY_MediaObj_EncodingQuality,
            NAME_MediaObj_EncodingQuality,
        },
    },
    {
        MTP_OBJECTPROPCODE_ENCODINGPROFILE,
        {
            {
                PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_OBJECT,
                MTP_DATATYPE_STRING,
                MTP_FORMFLAG_ENUM,
                &_EfEncodingProfiles
            },
            &PKEY_MediaObj_EncodingProfile,
            NAME_MediaObj_EncodingProfile,
        },
    },
};


/*  _PropCopareCode
 *
 *  Compares the property code in pvKey to the property code in the schema entry
 *
 *  Arguments:
 *      PSLVOID*        pvKey               Property code to compare
 *      PSLVOID*        pvComp              Table entry to compare to
 *
 *  Returns:
 *      PSLINT32        Comparison result:
 *                          < 0 : pvKey < pvComp
 *                          = 0 : pvKey == pvComp
 *                          > 0 : pvKey > pvComp
 */

PSLINT32 PSL_API _PropCompareCode(PSL_CONST PSLVOID* pvKey,
                            PSL_CONST PSLVOID* pvComp)
{
    return ((PSLINT32)((PSLUINT16)((PSLSIZET)pvKey)) -
                    (PSLINT32)(((PROPERTYSCHEMA*)pvComp)->wPropCode));
}


/*  InitPropSchema
 *
 *  Initializes the property schema
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS InitPropSchema()
{
    PSLBOOL         fTablesValid = PSLTRUE;

#ifdef PSL_ASSERTS
    {
        static PSLBOOL fValidated = PSLFALSE;

        /*  Validate the tables if necessary
         */

        if (!fValidated)
        {
            fTablesValid = _ValidateTable(PROPFLAGS_TYPE_DEVICE,
                            _RgmDevPropertyTbl,
                            PSLARRAYSIZE(_RgmDevPropertyTbl));
            if (fTablesValid)
            {
                fTablesValid = _ValidateTable(PROPFLAGS_TYPE_SERVICEOBJECT,
                            _RgmObjPropertyTbl,
                            PSLARRAYSIZE(_RgmObjPropertyTbl));
                fValidated = fTablesValid;
            }
        }
    }
#endif  /* PSL_ASSERTS */

    return fTablesValid ? PSLSUCCESS : PSLERROR_INVALID_DATA;
}


/*  GetPropRecByCode
 *
 *  Searches the tables for the property code specified and returns a
 *  pointer to the property record.
 *
 *  Arguments:
 *      PSLUINT16       wPropCode           Code to search for
 *      PSLUINT16       wPropFlags          Property flags (including type)
 *      MTPPROPERTYREC**
 *                      ppRec               Return buffer for property
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if found, PSLSUCCESS_NOT_FOUND if not
 */

PSLSTATUS GetPropRecByCode(PSLUINT16 wPropCode, PSLUINT16 wPropFlags,
                    PSL_CONST MTPPROPERTYREC** ppRec)
{
    PSLSTATUS       status;
    PROPERTYSCHEMA* pItem;

    if (PSLNULL != ppRec)
    {
        *ppRec = 0;
    }

    if (PSLNULL == ppRec)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    switch (wPropFlags)
    {
    case PROPFLAGS_TYPE_DEVICE:
        status = PSLBSearch((PSLVOID*)(PSLUINT32)wPropCode, _RgmDevPropertyTbl,
                            PSLARRAYSIZE(_RgmDevPropertyTbl),
                            sizeof(PROPERTYSCHEMA), _PropCompareCode,
                            &pItem);
        break;

    case PROPFLAGS_TYPE_OBJECT:
    case PROPFLAGS_TYPE_SERVICEOBJECT:
        status = PSLBSearch((PSLVOID*)(PSLUINT32)wPropCode, _RgmObjPropertyTbl,
                            PSLARRAYSIZE(_RgmObjPropertyTbl),
                            sizeof(OBJPROPERTYSCHEMA), _PropCompareCode,
                            &pItem);
        break;

    default:
        PSLASSERT(!L"Table type unsupported");
        status = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;
    }

    /*  If we did not find a match stop working
     */

    if (PSLSUCCESS != status)
    {
        goto Exit;
    }

    /*  Return the property record only
     */

    *ppRec = &(pItem->recProp);

Exit:
    return status;
}


/*  GetPropRecByIndex
 *
 *  Retrieves the property index for the specified property type from the
 *  table and returns it
 *
 *  Arguments:
 *      PSLUINT32       idxProp             Property index to retrieve
 *      PSLUINT16       wPropFlags          Property flags including type
 *      MTPPROPERTYREC**
 *                      ppRec               Return buffer for rec
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS GetPropRecByIndex(PSLUINT32 idxProp, PSLUINT16 wPropFlags,
                    PSL_CONST MTPPROPERTYREC** ppRec)
{
    PSLSTATUS status;

    if (PSLNULL != ppRec)
    {
        *ppRec = 0;
    }

    if (PSLNULL == ppRec)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    switch (wPropFlags)
    {
    case PROPFLAGS_TYPE_DEVICE:
        if (PSLARRAYSIZE(_RgmDevPropertyTbl) <= idxProp)
        {
            status = PSLERROR_INVALID_RANGE;
            goto Exit;
        }
        *ppRec = &(_RgmDevPropertyTbl[idxProp].recProp);
        status = PSLSUCCESS;
        break;

    case PROPFLAGS_TYPE_OBJECT:
    case PROPFLAGS_TYPE_SERVICEOBJECT:
        if (PSLARRAYSIZE(_RgmObjPropertyTbl) <= idxProp)
        {
            status = PSLERROR_INVALID_RANGE;
            goto Exit;
        }
        *ppRec = (PSL_CONST MTPPROPERTYREC*)
                            &(_RgmObjPropertyTbl[idxProp].recProp);
        status = PSLSUCCESS;
        break;

    default:
        PSLASSERT(!L"Property type unsupported");
        status = PSLERROR_UNEXPECTED;
        break;
    }

Exit:
    return status;
}


/*  GetPropRecCount
 *
 *  Returns the number of property records available for the specified type
 *
 *  Arguments:
 *      PSLUINT16       wPropFlags          Property flags including type
 *
 *  Returns:
 *      PSLUINT32       Number of records
 */

PSLUINT32 GetPropRecCount(PSLUINT16 wPropFlags)
{
    PSLUINT32 cRecs;

    switch (wPropFlags)
    {
    case PROPFLAGS_TYPE_DEVICE:
        cRecs = PSLARRAYSIZE(_RgmDevPropertyTbl);
        break;

    case PROPFLAGS_TYPE_OBJECT:
        cRecs = PSLARRAYSIZE(_RgmObjPropertyTbl);
        break;

    default:
        PSLASSERT(!L"Table type unsupported");
        cRecs = 0;
        break;
    }
    return cRecs;
}

#ifdef PSL_ASSERTS
/*  _ValidateTable
 *
 *  Walks through both of the tables and makes sure that they are
 *  correctly sorted so that we know our lookups will work.
 *
 *  Arguments:
 *      PSLUINT16       wPropFlags          Property flags including type
 *      PSLVOID*        pvTable             Table to check
 *      PSLUINT32       cItems              Number of items in the table
 *
 *  Returns:
 *      PSLBOOL         PSLTRUEE
  */

PSLBOOL _ValidateTable(PSLUINT16 wPropFlags, PSL_CONST PSLVOID* pvTable,
                    PSLUINT32 cItems)
{
    PSLUINT32                   cbSize;
    PSLBOOL                     fResult;
    PSLUINT32                   idxProp;
    PSL_CONST PROPERTYSCHEMA*   pEntry;
    PSL_CONST PROPERTYSCHEMA*   pEntryLast;

    /*  Validate Arguments
     */

    PSLASSERT((PSLNULL != pvTable) && (0 < cItems));

    /*  Determine the size
     */

    switch (wPropFlags)
    {
    case PROPFLAGS_TYPE_DEVICE:
        cbSize = sizeof(_RgmDevPropertyTbl[0]);
        break;

    case PROPFLAGS_TYPE_OBJECT:
    case PROPFLAGS_TYPE_SERVICEOBJECT:
        cbSize = sizeof(_RgmObjPropertyTbl[0]);
        break;

    default:
        /*  Unsupported type
         */

        PSLASSERT(PSLFALSE);
        fResult = PSLFALSE;
        goto Exit;
    }

    /*  Walk through and validate that the items are sorted
     */

    pEntryLast = (PSL_CONST PROPERTYSCHEMA*)pvTable;
    pEntry = (PSL_CONST PROPERTYSCHEMA*)((PSLBYTE*)pvTable + cbSize);
    for (idxProp = 1;
            (idxProp < cItems) && (pEntryLast->wPropCode < pEntry->wPropCode);
            idxProp++, pEntryLast = pEntry,
                pEntry = (PSL_CONST PROPERTYSCHEMA*)((PSLBYTE*)pEntry + cbSize))
    {
    }

    /*  Report whether the validation succeeded
     */

    fResult = (cItems <= idxProp);
    if (!fResult)
    {
        PSLDebugPrintf("Property Schema Table 0x%08x fails sort validation",
                            pvTable);
        PSLASSERT(PSLFALSE);
    }

Exit:
    return fResult;
}
#endif /* PSL_ASSERTS */

