/*
 *  MTPFormat.c
 *
 *  Contains definitions of the MTP Format functions which are part of the
 *  MTP Property library.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"

/*  Local Defines
 */

/*  Local Types
 */

/*  Local Functions
 */

/*  Local Variables
 */


/*  MTPFormatInfoFromList
 *
 *  Since the MTPFORMATINFO structures are of different length this function
 *  provides access to the specified index directly from the list
 *
 *  Arguments:
 *      PSLVOID*        rgvFormatList       List to extract format from
 *      PSLUINT32       cFormats            Number of formats in the list
 *      PSLUINT32       idxFormat           Desired format from the list
 *      PSLUINT16       wFlags              Format type
 *      MTPFORMATINFO** ppfiResult          Resulting format info
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPFormatInfoFromList(PSL_CONST PSLVOID* rgvFormatList,
                    PSLUINT32 cFormats, PSLUINT32 idxFormat, PSLUINT16 wFlags,
                    PSL_CONST MTPFORMATINFO** ppfiResult)
{
    PSLUINT32                   cbItem;
    PSLSTATUS                   ps;
    PSL_CONST MTPFORMATINFO*    pfiResult;

    /*  Clear result for safety
     */

    if (PSLNULL != ppfiResult)
    {
        *ppfiResult = PSLNULL;
    }

    /*  Validate arguments
     */

    if (((0 < cFormats) && (PSLNULL == rgvFormatList)) ||
        (PSLNULL == ppfiResult))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure the item is valid
     */

    if (cFormats <= idxFormat)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Determine how large the item in the list is based on the type
     *  information passed in
     */

    switch (wFlags)
    {
    case FORMATFLAGS_TYPE_OBJECT:
        cbItem = sizeof(MTPFORMATINFO);
        break;

    case FORMATFLAGS_TYPE_SERVICEOBJECT:
        cbItem = sizeof(MTPSERVICEFORMATINFO);
        break;

    case FORMATFLAGS_TYPE_METHODOBJECT:
        cbItem = sizeof(MTPSERVICEMETHODINFO);
        break;

    default:
        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine where we think the desired item is at
     */

    pfiResult = (PSL_CONST MTPFORMATINFO*)
                            ((PSLBYTE*)rgvFormatList + (idxFormat * cbItem));

    /*  And check to make sure that the item really is of the correct type
     */

    if ((PSLNULL == pfiResult->precFormat) ||
        !(pfiResult->precFormat->wFlags & wFlags))
    {
        ps = PSLERROR_INVALID_DATA;
        goto Exit;
    }

    /*  Return the item
     */

    *ppfiResult = pfiResult;
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPFormatInfoNext
 *
 *  Returns the next format info from a list of format info's based on the
 *  specified type.
 *
 *  Arguments:
 *      MTPFORMATINFO*  pfiCur              The current format info
 *      PSLUINT16       wFlags              Type of format list being iterated
 *
 *  Returns:
 *      MTPFORMATINFO*  The next item in the list
 */

PSL_CONST MTPFORMATINFO* PSL_API MTPFormatInfoNext(
                    PSL_CONST MTPFORMATINFO* pfiCur, PSLUINT16 wFlags)
{
    PSLUINT32                   cbItem;
    PSL_CONST MTPFORMATINFO*    pfiResult;

    /*  We should be enumerating through items which are of the correct type
     */

    PSLASSERT((PSLNULL != pfiCur) && (PSLNULL != pfiCur->precFormat) &&
            (pfiCur->precFormat->wFlags & wFlags));

    /*  Figure out how large the item is that we need to skip
     */

    switch (wFlags)
    {
    case FORMATFLAGS_TYPE_OBJECT:
        cbItem = sizeof(MTPFORMATINFO);
        break;

    case FORMATFLAGS_TYPE_SERVICEOBJECT:
        cbItem = sizeof(MTPSERVICEFORMATINFO);
        break;

    case FORMATFLAGS_TYPE_METHODOBJECT:
        cbItem = sizeof(MTPSERVICEMETHODINFO);
        break;

    default:
        /*  We should not get into this scenario- if we do we force the result
         *  to be PSLNULL
         */

        PSLASSERT(PSLFALSE);
        pfiResult = PSLNULL;
        goto Exit;
    }

    /*  Return the next location- we do not validate because we may actually
     *  be moving off the end of the list and that would only result in
     *  spurious asserts.
     */

    pfiResult = (PSL_CONST MTPFORMATINFO*)((PSLBYTE*)pfiCur + cbItem);

Exit:
    return pfiResult;
}

