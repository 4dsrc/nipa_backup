/*
 *  MTPRouter.c
 *
 *  Contains the implementation of router component.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"

#ifdef _DEBUG
#include "MTPHandlerUtil.h"
#endif /*_DEBUG*/

#define MTPROUTECONTEXT_COOKIE  'mRtC'


/*
 *  MTPRouteCreate
 *
 *  Creates a route
 *
 *  Arguments:
 *      PSLMSGQUEUE     mqTransport         Transport queue to bind route to
 *      MTPCONTEXT**    Resulting context   Context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPRouteCreate(PSLMSGQUEUE mqTransport, MTPCONTEXT** ppmtpc)
{
    PSLSTATUS status;
    MTPCONTEXT* pmtpc = PSLNULL;
    PSLLOOKUPTABLE hHandlerTable;
    PSLUINT32 cbDesired;
    PSLUINT32 cbHandlerData;
    PSLUINT32 cbTransportData;

    if (PSLNULL != ppmtpc)
    {
        *ppmtpc = PSLNULL;
    }

    if ((PSLNULL == mqTransport) || (PSLNULL == ppmtpc))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }


    /* get handler data size, this will be provider by core handler
     * which is already up and running. It will get the sizes of
     * other handlers and will return the max value
     */
    status = MTPContextGetHandlerReserve(&cbHandlerData);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }


    status = MTPLookupTablePreOpenGet(&hHandlerTable);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* get transport data size */
    status = MTPContextGetTransportReserve(&cbTransportData);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    cbDesired = sizeof(MTPCONTEXT) + cbHandlerData + cbTransportData;
#ifdef PSL_ASSERTS
    cbDesired += sizeof(PSLUINT32);
#endif  /* PSL_ASSERTS */

    pmtpc = PSLMemAlloc(PSLMEM_PTR, cbDesired);
    if (PSLNULL == pmtpc)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

#ifdef PSL_ASSERTS
    *((PSLUINT32*)pmtpc) = MTPROUTECONTEXT_COOKIE;
    pmtpc = (MTPCONTEXT*)((PSLBYTE*)pmtpc + sizeof(PSLUINT32));
#endif  /* PSL_ASSERTS */

    pmtpc->mtpContextState.mqHandler = PSLNULL;
    pmtpc->mtpContextState.mqTransport = mqTransport;
    pmtpc->mtpContextState.hHandlerTable = hHandlerTable;

    pmtpc->mtpTransportState.cbTransportData = cbTransportData;
    pmtpc->mtpTransportState.pbTransportData =
                            (PSLBYTE*)pmtpc + sizeof(MTPCONTEXT);
    pmtpc->mtpHandlerState.cbHandlerData = cbHandlerData;
    pmtpc->mtpHandlerState.pbHandlerData =
                            pmtpc->mtpTransportState.pbTransportData +
                            pmtpc->mtpTransportState.cbTransportData;

    status = MTPEventHandlerAddContext(pmtpc,
                            &(pmtpc->mtpContextState.mqEventHandler));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *ppmtpc = pmtpc;

    pmtpc   = PSLNULL;
    status = PSLSUCCESS;

Exit:
    SAFE_MTPROUTEDESTROY(pmtpc);
    return status;
}


/*
 *  MTPRouteDestroy
 *
 *  Destroys the specified route
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpc               Route to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPRouteDestroy(MTPCONTEXT* pmtpc)
{
    PSLSTATUS status;

    if (PSLNULL == pmtpc)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(MTPROUTECONTEXT_COOKIE == *((PSLUINT32*)pmtpc - 1));

    (PSLVOID)MTPHandlerForceCloseSession(pmtpc);

    (PSLVOID)MTPEventHandlerRemoveContext(pmtpc);

#ifdef PSL_ASSERTS
    pmtpc = (MTPCONTEXT*)((PSLUINT32*)pmtpc - 1);
#endif  /* PSL_ASSERTS */
    SAFE_PSLMEMFREE(pmtpc);

    status = PSLSUCCESS;
Exit:
    return status;

}


/*
 *  MTPRouteBegin
 *
 *  Begins a route from the transport to the handler.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpc               Context to use
 *      PSLMSGPOOL      mqTransport         Transport message pool
 *      PCXMTPOPERATION pmtpOp              MTP operation
 *      PSLUINT32       cmtpOp              Size of the MTP operation
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPRouteBegin(MTPCONTEXT* pmtpc, PSLMSGPOOL mpTransport,
                     PCXMTPOPERATION pmtpOp, PSLUINT32 cmtpOp)
{
    PSLBOOL fResetHandler = PSLFALSE;
    PSLMSGQUEUE mqCurrent;
    PSLMSGQUEUE mqHandler;
    PSLPARAM pvData;
    PSLMSG* pMsg = PSLNULL;
    PSLSTATUS status;

    if (PSLNULL == pmtpc || PSLNULL == pmtpOp || PSLNULL == mpTransport)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(MTPROUTECONTEXT_COOKIE == *((PSLUINT32*)pmtpc - 1));

    MTPPERFLOG_OPERATION(
        MP_TYPE_ELAPSETIME_BEGIN,
        &pmtpOp->dwParam1,
        (cmtpOp - ((PSLUINT8*)&pmtpOp->dwParam1 - (PSLUINT8*)pmtpOp)));

    PSLTraceProgress("MTPRouteBegin- Handling %s\tdwID = 0x%08x",
            MTPOpCodeToString(MTPLoadUInt16(&(pmtpOp->wOpCode))),
            MTPLoadUInt32(&(pmtpOp->dwTransactionID)));
    PSLTraceDetail("\tdwParam1 = 0x%08x\tdwParam2 = 0x%08x\t"
            "dwParam3 = 0x%08x\tdwParam4 = 0x%08x\tdwParam5 = 0x%08x",
            MTPLoadUInt32(&(pmtpOp->dwParam1)),
            MTPLoadUInt32(&(pmtpOp->dwParam2)),
            MTPLoadUInt32(&(pmtpOp->dwParam3)),
            MTPLoadUInt32(&(pmtpOp->dwParam4)),
            MTPLoadUInt32(&(pmtpOp->dwParam5)));


    /* Get handler queue id and data from loopkup table*/

    status = MTPLookupTableFindEntry(
                            pmtpc->mtpContextState.hHandlerTable,
                            MTPLoadUInt16(&(pmtpOp->wOpCode)), &mqHandler,
                            &pvData);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*  Attempt to set this as the current command
     */

    mqCurrent = PSLAtomicCompareExchangePointer(
                            &(pmtpc->mtpContextState.mqHandler),
                            mqHandler,
                            PSLNULL);
    PSLASSERT(PSLNULL == mqCurrent);
    if (PSLNULL != mqCurrent)
    {
        /*  Currently handling another command- this is an
         *  invalid operation at this point
         */

        status = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }
    fResetHandler = PSLTRUE;

    /*  Notify the handler that a command is available
     */

    status = PSLMsgAlloc(mpTransport, &pMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(PSLNULL != pMsg);

    pMsg->msgID     =   MTPMSG_CMD_AVAILABLE;
    pMsg->aParam0  =   (PSLPARAM)pmtpc;
    pMsg->aParam2  =   (PSLPARAM)pmtpOp;
    pMsg->aParam3  =   pvData;
    pMsg->alParam  =   cmtpOp;

    status = PSLMsgPost(mqHandler,pMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pMsg = PSLNULL;

    /*  No need to reset the handler- all has succeeded
     */

    fResetHandler = PSLFALSE;

Exit:
    if (fResetHandler)
    {
        PSLAtomicExchangePointer((pmtpc->mtpContextState.mqHandler),
                            PSLNULL);
    }
    SAFE_PSLMSGFREE(pMsg);
    return status;
}


/*
 *  MTPRouteEnd
 *
 *  Called to end the route between the tranport and the handler
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpc               MTP context to operate on
 *      MTPROUTEUPDATE* pmtpcu              Route update information
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if
 *                        no active route
 *
 */

PSLSTATUS PSL_API MTPRouteEnd(MTPCONTEXT* pmtpc, MTPROUTEUPDATE* pmtpcu)
{
    PSLSTATUS status;
    PSLVOID* pvOld;

    if (PSLNULL == pmtpc || PSLNULL == pmtpcu)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(MTPROUTECONTEXT_COOKIE == *((PSLUINT32*)pmtpc - 1));

    /*Don't modify the transport queue ID as it is still valid*/

    if (PSLNULL != pmtpcu->hHandlerTableNew)
    {
        PSLAtomicExchangePointer(&(pmtpc->mtpContextState.hHandlerTable),
                            pmtpcu->hHandlerTableNew);
    }

    pvOld = PSLAtomicExchangePointer(&(pmtpc->mtpContextState.mqHandler),
                            PSLNULL);

    status = (PSLNULL != pvOld) ? PSLSUCCESS : PSLSUCCESS_NOT_FOUND;

Exit:
    MTPPERFLOG_OPERATION(MP_TYPE_ELAPSETIME_END, PSLNULL, 0);

    return status;
}

/*
 *  MTPRouteMsgToTransport
 *
 *  Routes a message to the transport specified in the context
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpc               Context to route within
 *      PSLMSG*         pmsg                Message to route
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPRouteMsgToTransport(MTPCONTEXT* pmtpc, PSLMSG* pmsg)
{
    PSLSTATUS status;

    if (PSLNULL == pmtpc || PSLNULL == pmsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(MTPROUTECONTEXT_COOKIE == *((PSLUINT32*)pmtpc - 1));

    /*  Contexts must always contain a handler queue- if we do not have
     *  one treat as an error
     */

    PSLASSERT(PSLNULL != pmtpc->mtpContextState.mqTransport);
    if (PSLNULL == pmtpc->mtpContextState.mqTransport)
    {
        status = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  If the message is supposed to have a context make sure it is
     *  present and correct
     */

    PSLASSERT(!(MTPMSGID_CONTEXT_PRESENT & pmsg->msgID) ||
                            ((PSLPARAM)pmtpc == pmsg->aParam0));
    if ((MTPMSGID_CONTEXT_PRESENT & pmsg->msgID) &&
        ((PSLPARAM)pmtpc != pmsg->aParam0))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  And post the message
     */

    status = PSLMsgPost(pmtpc->mtpContextState.mqTransport, pmsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}


/*
 *  MTPRouteMsgToHandler
 *
 *  Routes a message to a handler.  If one is currently not available
 *  message is consumed and success reported.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpc               Context to route within
 *      PSLMSG*         pmsg                Message to route
 *
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCES_NOT_AVAILABLE
 *                        if message could not be sent because no
 *                        handler was availble (message is released by
 *                        this function)
 *
 */

PSLSTATUS PSL_API MTPRouteMsgToHandler(MTPCONTEXT* pmtpc, PSLMSG* pmsg)
{
    PSLSTATUS status;
    PSLMSGQUEUE mqDest;

    if (PSLNULL == pmtpc || PSLNULL == pmsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(MTPROUTECONTEXT_COOKIE == *((PSLUINT32*)pmtpc - 1));

    /*  If the message is supposed to have a context make sure it is
     *  present and correct
     */

    PSLASSERT(!(MTPMSGID_CONTEXT_PRESENT & pmsg->msgID) ||
                            ((PSLPARAM)pmtpc == pmsg->aParam0));
    if ((MTPMSGID_CONTEXT_PRESENT & pmsg->msgID) &&
        ((PSLPARAM)pmtpc != pmsg->aParam0))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (MTP_EVENT == PSL_MSG_CLASS(pmsg->msgID))
    {
        mqDest = pmtpc->mtpContextState.mqEventHandler;
    }
    else
    {
        mqDest = pmtpc->mtpContextState.mqHandler;
    }

    /*  If we do not have a context report that the route is not
     *  available and consume the message
     */

    if (PSLNULL == mqDest)
    {
        PSLMsgFree(pmsg);
        status = PSLSUCCESS_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Post the message to the desired handler
     */

    status = PSLMsgPost(mqDest, pmsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}


/*
 *  MTPRouteTransportError
 *
 *  Routes an error in the transport to the context and performs any
 *  neccessary cleanup.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpc           Context to route error for
 *      PSLMSGPOOL      mpSource        Source message pool for message
 *      PSLMSGID        msgID           Message that caused the error
 *      PSLUINT32       dwSequenceID    Sequence ID for message in error
 *      PSLSTATUS       psErr           Error that occurred
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPRouteTransportError(MTPCONTEXT* pmtpc, PSLMSGPOOL mpSource,
                            PSLMSGID msgID, PSLUINT32 dwSequenceID,
                            PSLSTATUS psErr)
{
    PSLMSG*         pMsg = PSLNULL;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    PSLASSERT(PSL_FAILED(psErr));
    if ((PSLNULL == pmtpc) || (PSL_SUCCEEDED(psErr)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    PSLASSERT(MTPROUTECONTEXT_COOKIE == *((PSLUINT32*)pmtpc - 1));

    /*  Allocate a message from the specified pool and forward the error
     */

    ps = PSLMsgAlloc(mpSource, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    pMsg->msgID = MTPMSG_ERROR;
    pMsg->aParam0 = (PSLPARAM)pmtpc;
    pMsg->aParam1 = dwSequenceID;
    pMsg->aParam2 = msgID;
    pMsg->aParam3 = psErr;

    /*  And route the message
     */

    ps = MTPRouteMsgToHandler(pmtpc, pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg = PSLNULL;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}
