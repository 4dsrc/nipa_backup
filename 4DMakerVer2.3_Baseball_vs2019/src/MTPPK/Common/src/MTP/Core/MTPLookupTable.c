/*
 *  MTPLookupTable.c
 *
 *  Contains the implementation of lookup table fucntions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerCommon.h"
#include "MTPHandlerCore.h"
#include "MTPHandlerDeviceInfo.h"
#include "MTPHandlerDatabase.h"
#include "MTPHandlerProp.h"

static PSLLOOKUPTABLE _LtPreOpenSession = PSLNULL;
static PSLLOOKUPTABLE _LtProperty = PSLNULL;

/*
 *  MTPLookupTablePreOpenSet
 *
 *  Stashes PreOpenSession lookup table
 *
 */
PSLSTATUS PSL_API MTPLookupTablePreOpenSet(PSLLOOKUPTABLE plt)
{
    PSLSTATUS status;

    if (PSLNULL == plt)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    _LtPreOpenSession = plt;
    status = PSLSUCCESS;
Exit:
    return status;
}

/*
 *  MTPLookupTablePreOpenGet
 *
 *  Returns PreOpenSession lookup table
 *
 */
PSLSTATUS PSL_API MTPLookupTablePreOpenGet(PSLLOOKUPTABLE* pplt)
{
    PSLSTATUS status;

    if (PSLNULL == pplt)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    *pplt = _LtPreOpenSession;
    status = PSLSUCCESS;
Exit:
    return status;
}

/*
 *  MTPLookupTablePreOpenDestroy
 *
 *  Destroys PreOpenSession lookup table
 *
 */
PSLVOID PSL_API MTPLookupTablePreOpenDestroy()
{
    SAFE_PSLLOOKUPTABLEDESTROY(_LtPreOpenSession);
}

/*
 *  MTPLookupTableDevicePropSet
 *
 *  Stashes device property lookup table
 *
 */
PSLSTATUS PSL_API MTPLookupTableDevicePropSet(PSLLOOKUPTABLE plt)
{
    PSLSTATUS status;

    if (PSLNULL == plt)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    _LtProperty = plt;
    status = PSLSUCCESS;
Exit:
    return status;
}

/*
 *  MTPLookupTableDevicePropGet
 *
 *  Returns device property lookup table
 *
 */
PSLSTATUS PSL_API MTPLookupTableDevicePropGet(PSLLOOKUPTABLE* pplt)
{
    PSLSTATUS status;

    if (PSLNULL == pplt)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    *pplt = _LtProperty;
    status = PSLSUCCESS;
Exit:
    return status;
}

/*
 *  MTPLookupTablePropertyDestroy
 *
 *  Destroys device property lookup table
 *
 */
PSLVOID PSL_API MTPLookupTableDevicePropDestroy()
{
    SAFE_PSLLOOKUPTABLEDESTROY(_LtProperty);
}


/*
 *  MTPLookupTableFindEntry
 *
 *  Returns an entry from the requested table
 *
 */
PSLSTATUS PSL_API MTPLookupTableFindEntry(PSLLOOKUPTABLE mlt,
                                  PSLUINT32 dwKey,
                                  PSLMSGQUEUE* pmqHandler,
                                  PSLPARAM* ppvData)
{
    PSLSTATUS status;
    PSLPARAM pvData;
    PSLMSGQUEUE mqHandler;

    if (ppvData)
    {
        *ppvData = PSLNULL;
    }

    if (pmqHandler)
    {
        *pmqHandler = PSLNULL;
    }

    if (PSLNULL == mlt || PSLNULL == pmqHandler ||
        PSLNULL == ppvData )
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLLookupTableFindEntry(mlt, MTPLookupTableCompareOpCode,
                                     (PSLPARAM)dwKey,
                                     (PSLPARAM*)&mqHandler, &pvData);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    if (PSLSUCCESS_NOT_FOUND == status)
    {
        /*  if binary search couldn't find the handler
         *  use the default handler
         */
        status = PSLLookupTableFindEntry(mlt, MTPLookupTableCompareOpCode,
                            MTP_OPCODE_UNDEFINED, (PSLPARAM*)&mqHandler,
                            &pvData);
        PSLASSERT(PSLSUCCESS == status);
        if (PSLSUCCESS != status)
        {
            status = PSL_FAILED(status) ? status : PSLERROR_UNEXPECTED;
            goto Exit;
        }
    }

    PSLASSERT(PSLNULL != pvData);
    PSLASSERT(PSLNULL != mqHandler);

    *ppvData = pvData;
    *pmqHandler = mqHandler;

Exit:
    return status;
}

/*
 *  MTPLookupTableCompareOpCode
 *
 *  Helper function used to compare two OpCodes
 *
 */
PSLINT32 PSL_API MTPLookupTableCompareOpCode(PSL_CONST PSLVOID* pvKey,
                        PSL_CONST PSLVOID* pvCompare)
{
    return ((PSLUINT32)pvKey - ((PSLLOOKUPRECORD*)pvCompare)->pvKey);
}
