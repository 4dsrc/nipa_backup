/*
 *  MTPInitialize.c
 *
 *  Contains the implementation of system initialize function
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPInitializeUtil.h"
#include "MTPTransportNotify.h"

/*  Local Defines
 */

/*  Local Types
 */

/*  Local Functions
 */

static PSLSTATUS _MTPInitializeResponder(PSLUINT32 dwInitFlags,
                    PSLBOOL fFirstTime);

static PSLSTATUS _MTPInitializeInitiator(PSLUINT32 dwInitFlags,
                    PSLBOOL fFirstTime);

/*  Local Variables
 */

static PSLUINT32            _CRefMTPInitialize = 0;
static PSLUINT32            _DwInitFlags = 0;

static const PSLWCHAR       _RgchMTPInitializeMutex[] = L"MTP Initialize Mutex";

/*
 *  _MTPInitializeResponder
 *
 *  Initializes the responder support
 *
 *  Arguments:
 *      PSLUINT32       dwInitFlags         Initialization flags
 *      PSLBOOL         fFirstTime          TRUE if first time initialization
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPInitializeResponder(PSLUINT32 dwInitFlags, PSLBOOL fFirstTime)
{
    PSLSTATUS       status;

    /*  Validate arguments
     */

    if (!(MTPINITFLAG_RESPONDER & dwInitFlags))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (0 == (MTPINITFLAG_TRANSPORT_MASK & dwInitFlags))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Handle first time initialization
     */

    if (fFirstTime)
    {
        status = MTPHandlerInitialize();
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    /*  Initialize any transports that are not already running
     */

    dwInitFlags &= ~(MTPINITFLAG_TRANSPORT_MASK & _DwInitFlags);

    if (MTPINITFLAG_USB_TRANSPORT & dwInitFlags)
    {
        status = MTPUSBTransportInitialize(dwInitFlags);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    if (MTPINITFLAG_IP_TRANSPORT & dwInitFlags)
    {
        status = MTPIPTransportInitialize(dwInitFlags);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    if (MTPINITFLAG_BT_TRANSPORT & dwInitFlags)
    {
        status = MTPBTTransportInitialize(dwInitFlags);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    status = PSLSUCCESS;

Exit:
    return status;
}


/*
 *  _MTPInitializeInitiator
 *
 *  Initializes the initiator support
 *
 *  Arguments:
 *      PSLUINT32       dwInitFlags         Initialization flags
 *      PSLBOOL         fFirstTime          TRUE if first time initialization
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPInitializeInitiator(PSLUINT32 dwInitFlags, PSLBOOL fFirstTime)
{
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if (!(MTPINITFLAG_INITIATOR & dwInitFlags))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Validate transport
     */

    if ((MTPINITFLAG_USB_TRANSPORT | MTPINITFLAG_BT_TRANSPORT) & dwInitFlags)
    {
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;
    }

    if (0 == (MTPINITFLAG_TRANSPORT_MASK & dwInitFlags))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* Initialize any new transports as requested
     */

    dwInitFlags &= ~(MTPINITFLAG_TRANSPORT_MASK & _DwInitFlags);

    if (MTPINITFLAG_IP_TRANSPORT & dwInitFlags)
    {
        ps = MTPIPTransportInitialize(dwInitFlags);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
    fFirstTime;
}


/*
 *  MTPInitialize
 *
 *  Core function for initializing the MTP system
 *
 *  Arguments:
 *      PSLUINT32       dwInitFlags         Initialization flags
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */
PSLSTATUS PSL_API MTPInitialize(PSLUINT32 dwInitFlags)
{
    PSLUINT32       dwValue;
    PSLBOOL         fFirstTime = PSLFALSE;
    PSLHANDLE       hMutexInit = PSLNULL;
    PSLSTATUS       ps;

    /*  Acquire the initialization mutex so that we synchronize paths through
     *  this code
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchMTPInitializeMutex, &hMutexInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Determine whether or not this is the first attempt an initializing
     */

    fFirstTime = (0 == _CRefMTPInitialize);

    /*  Make sure that we have some mode defined
     */

    if (0 == (MTPINITFLAG_MODE_MASK & dwInitFlags))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Currently we do not support simultaneous Initiator and Responder
     *  modes- reject it if both are going to be set
     */

    dwValue = dwInitFlags | _DwInitFlags;
    if ((MTPINITFLAG_INITIATOR | MTPINITFLAG_RESPONDER) ==
        ((MTPINITFLAG_INITIATOR | MTPINITFLAG_RESPONDER) & dwValue))
    {
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;
    }

    /*  Handle any first time initialization required
     */

    if (fFirstTime)
    {
        /*  Initialize the serialization subsystem
         */

        ps = MTPSerializeInitialize(MTP_MAX_SESSIONS);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Initialize the property subsystem
         */

        ps = MTPPropertyInitialize(MTP_MAX_SESSIONS);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Initialize the Service Resolver subsystem
         */

        ps = MTPServiceResolverInitialize(MTP_MAX_SESSIONS);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Initialize the Event Manager subsystem
         */

        ps = MTPEventManagerInitialize(MTP_MAX_SESSIONS);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Initialize the Transort Notify subsystem
         */

        ps = MTPTransportNotifyInitialize();
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Initialize the responder operation if requested
     */

    if (MTPINITFLAG_RESPONDER & dwInitFlags)
    {
        ps = _MTPInitializeResponder(dwInitFlags, fFirstTime);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Initialize the initiator operation if requested
     */

    if (MTPINITFLAG_INITIATOR & dwInitFlags)
    {
        ps = _MTPInitializeInitiator(dwInitFlags, fFirstTime);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Everything has succesfully initialized- increment the reference count
     *  and store the new initialization flags
     */

    _CRefMTPInitialize++;
    _DwInitFlags |= dwInitFlags;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    /*  Make sure we undo anything we tried to do if we encountered
     *  a problem
     */

    if (PSL_FAILED(ps) && fFirstTime)
    {
        MTPUninitialize();
    }
    SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);
    return ps;
}


/*
 *  MTPUninitialize
 *
 *  Public entry-point to uninitialize MTP
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE if not last
 *                        initialization
 *
 */

PSLSTATUS PSL_API MTPUninitialize()
{
    PSLHANDLE       hMutexInit = PSLNULL;
    PSLSTATUS       ps;

    /*  Acquire the initialization mutex so that we synchronize paths through
     *  this code
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchMTPInitializeMutex, &hMutexInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Determine if it is time to shutdown
     */

    if (0 < _CRefMTPInitialize)
    {
        _CRefMTPInitialize--;
    }
    if (0 < _CRefMTPInitialize)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Close down the MTP components that are running
     */

    ps = MTPShutdown();
    PSLASSERT(PSL_SUCCEEDED(ps));

    ps = MTPHandlerUninitialize();
    PSLASSERT(PSL_SUCCEEDED(ps));

    /*  And the transports
     */

    ps = MTPUSBTransportUninitialize();
    PSLASSERT(PSL_SUCCEEDED(ps) || (PSLERROR_NOT_INITIALIZED == ps));

    ps = MTPIPTransportUninitialize();
    PSLASSERT(PSL_SUCCEEDED(ps) || (PSLERROR_NOT_INITIALIZED == ps));

    ps = MTPBTTransportUninitialize();
    PSLASSERT(PSL_SUCCEEDED(ps) || (PSLERROR_NOT_INITIALIZED == ps));

    /*  And any other base services
     */

    ps = MTPTransportNotifyUninitialize();
    PSLASSERT(PSL_SUCCEEDED(ps));

    ps = MTPEventManagerUninitialize();
    PSLASSERT(PSL_SUCCEEDED(ps));

    ps = MTPServiceResolverUninitialize();
    PSLASSERT(PSL_SUCCEEDED(ps));

    ps = MTPPropertyUninitialize();
    PSLASSERT(PSL_SUCCEEDED(ps));

    ps = MTPSerializeUninitialize();
    PSLASSERT(PSL_SUCCEEDED(ps));

    /*  Clear the init flags so we can re-initialize correctly next time
     */

    _DwInitFlags = 0;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);
    return ps;
}

