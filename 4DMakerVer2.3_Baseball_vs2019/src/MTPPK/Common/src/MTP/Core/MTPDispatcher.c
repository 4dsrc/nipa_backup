/*
 *  MTPDispatcher.c
 *
 *  Contains definition of the MTP Dispatcher helper
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"

#define MTPDISPATCHER_COOKIE    'mtpD'

typedef struct _MTPDISPATCHERINFO
{
    PSLBOOL                     fThreaded;
    PSLBOOL                     fShutdown;
    PSLUINT32                   cActive;
    PSLMSGQUEUE                 mqDispatch;
    PSLMSGPOOL                  mpDispatch;
    PFNMTPONCMDDISPATCH         pfnOnCmdDispatch;
    PSLHANDLE                   hThread;
    PSLHANDLE                   hLock;
} MTPDISPATCHERINFO;

typedef struct _MTPDISPATCHEROBJ
{
    MTPSINGLETHREADEDCALLBACK   stcb;
    MTPDISPATCHERINFO           mdi;
#ifdef PSL_ASSERTS
    PSLUINT32                   dwCookie;
#endif /*PSL_ASSERTS*/
} MTPDISPATCHEROBJ;

typedef struct _MTPDISPATCHCLEANQUEUEINFO
{
    MTPDISPATCHERINFO*          pdi;
    MTPCONTEXT*                 pmtpctx;
} MTPDISPATCHCLEANQUEUEINFO;

enum
{
    MTPDISPATCHERMSG_END =      MAKE_PSL_NORMAL_MSGID(PSL_USER, 0),
};


/* Function prototypes */

static PSLSTATUS PSL_API _MTPDispatcherCleanQueueEnum(
                            PSL_CONST PSLMSG* pMsg,
                            PSLPARAM aParam);

static PSLSTATUS PSL_API _MTPDispatcherCleanQueueEnumOnDestroy(
                            PSL_CONST PSLMSG* pMsg,
                            PSLPARAM aParam);

static PSLSTATUS PSL_API _MTPDispatcherCallback(PSLVOID* pvParam);

static PSLSTATUS _MTPDispatcherCore(PSLVOID* pvParam);


/*
 *  MTPDispatcherCreate
 *
 *  Creates an instance of the MTP dispatcher
 *
 *  Arguments:
 *      PSLUINT32       cdwMsgs             Number of messages to place in pool
 *      PFNMTPONCMDDISPATCH
 *                      pfnOnCmd            Callback function to allow work to
 *                                            be done following the command
 *                                            dispatch
 *      MTPDISPATCHER*  pmd                 Resulting dispatcher
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPDispatcherCreate(PSLUINT32 cdwMsgs,
                    PFNMTPONCMDDISPATCH pfnOnCmd, MTPDISPATCHER* pmd)
{
    PSLSTATUS           status;
    PSLMSGQUEUE         mqDispatch      = PSLNULL;
    MTPDISPATCHEROBJ*   pDispObj        = PSLNULL;
    PSLHANDLE           hDispThread     = PSLNULL;
    PSLHANDLE           hMutex          = PSLNULL;
    PSLMSGPOOL          mpDispatch      = PSLNULL;
    PSLVOID*            pvParam         = PSLNULL;

    if (PSLNULL != pmd)
    {
        *pmd = PSLNULL;
    }

    /* Validate argument */

    if (PSLNULL == pmd)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLTraceDetail("MTPDispatcherCreate");

    /* Allocate information for the dispatcher thread */

    pDispObj = (MTPDISPATCHEROBJ*) PSLMemAlloc(PSLMEM_PTR,
                                             sizeof(MTPDISPATCHEROBJ));

    if (PSLNULL == pDispObj)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

#ifdef PSL_ASSERTS
    pDispObj->dwCookie       = MTPDISPATCHER_COOKIE;
#endif /*PSL_ASSERTS*/

    status = MTPMsgGetQueueParam(&pvParam);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = PSLMutexOpen(PSLFALSE, PSLNULL, &hMutex);
    if (PSL_FAILED(status))
    {
        status = PSLERROR_QUEUE_CREATE;
        goto Exit;
    }

    status = PSLMsgQueueCreate(pvParam, MTPMsgOnPost, MTPMsgOnWait,
                               MTPMsgOnDestroy, &mqDispatch);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Added one to count of messages to account for DISPATCH_END msg*/

    status = PSLMsgPoolCreate(cdwMsgs + 1, &mpDispatch);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* And initialize it */

    pDispObj->mdi.mqDispatch        = mqDispatch;
    pDispObj->mdi.mpDispatch        = mpDispatch;
    pDispObj->mdi.pfnOnCmdDispatch  = pfnOnCmd;
    pDispObj->mdi.fThreaded         = PSLTRUE;
    pDispObj->mdi.hLock             = hMutex;

    /* Create a thread to dispatch the messages */

    status = PSLThreadCreate((PFNPSLTHREADFUNC)_MTPDispatcherCore,
                             pDispObj, PSLFALSE,
                             PSLTHREAD_DEFAULT_STACK_SIZE,
                             &hDispThread);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (PSLSUCCESS_SINGLE_THREADED == status)
    {
        /* Mark the dispatcher as not threaded */

        pDispObj->mdi.fThreaded = PSLFALSE;

        pDispObj->stcb.pfnSingleThreadedCallback =
                                            _MTPDispatcherCallback;

        /* Set the correct dispatch functions */

        status = PSLMsgQueueSetCallbacks(mqDispatch,
                                         pDispObj,
                                         MTPMsgOnPost,
                                         MTPMsgOnWait,
                                         MTPMsgOnDestroy);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    pDispObj->mdi.hThread = hDispThread;

    /* Make sure all is well */

    *pmd = (MTPDISPATCHER)pDispObj;

    mqDispatch      = PSLNULL;
    mpDispatch      = PSLNULL;
    pvParam         = PSLNULL;
    pDispObj        = PSLNULL;
    hDispThread     = PSLNULL;
    hMutex          = PSLNULL;

Exit:
    SAFE_PSLMSGQUEUEDESTROY(mqDispatch);
    SAFE_PSLMSGPOOLDESTROY(mpDispatch);
    SAFE_MTPMSGRELEASEQUEUEPARAM(pvParam);
    SAFE_PSLMEMFREE(pDispObj);
    SAFE_PSLTHREADCLOSE(hDispThread);
    SAFE_PSLMUTEXCLOSE(hMutex);

    /* Generic clean-up code */

    return (status);
}


/*
 *  MTPDispatcherDestroy
 *
 *  Called to close a dispather
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  Dispatcher to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPDispatcherDestroy(MTPDISPATCHER md)
{
    PSLUINT32                   dwThreadResult;
    PSLHANDLE                   hMutexClose = PSLNULL;
    MTPDISPATCHEROBJ*           pDispObj = PSLNULL;
    PSLMSG*                     pMsg = PSLNULL;
    PSLSTATUS                   status;
    MTPDISPATCHCLEANQUEUEINFO   cqi;

    /* Validate argument */

    if (PSLNULL == md)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pDispObj = (MTPDISPATCHEROBJ*)md;

    PSLASSERT(MTPDISPATCHER_COOKIE == pDispObj->dwCookie);

    status = PSLMutexAcquire(pDispObj->mdi.hLock, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    hMutexClose = pDispObj->mdi.hLock;

    status = PSLMsgAlloc(pDispObj->mdi.mpDispatch, &pMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(PSLNULL != pMsg);

    pMsg->msgID     =   MTPDISPATCHERMSG_END;

    status = PSLMsgPost(pDispObj->mdi.mqDispatch, pMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    pMsg = PSLNULL;

    if (PSLNULL != pDispObj->mdi.hThread)
    {
        (PSLVOID)PSLThreadGetResult(pDispObj->mdi.hThread,
                               PSLTHREAD_INFINITE,
                               &dwThreadResult);

        (PSLVOID)PSLThreadClose(pDispObj->mdi.hThread);
    }

    /*  There's still a chance unprocessed messages are in the queue
     *  so empty the dispatcher queue
     */

    /*  Provide the information the clean queue callback needs
     *  to locate messages and respond with buffer unused
     *  notifications
     */

    cqi.pdi = &pDispObj->mdi;
    cqi.pmtpctx = PSLNULL;
    status = PSLMsgEnum(pDispObj->mdi.mqDispatch,
                        _MTPDispatcherCleanQueueEnumOnDestroy,
                        (PSLPARAM)&cqi);
    PSLASSERT(PSL_SUCCEEDED(status));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    SAFE_PSLMSGQUEUEDESTROY(pDispObj->mdi.mqDispatch);
    SAFE_PSLMSGPOOLDESTROY(pDispObj->mdi.mpDispatch);

    SAFE_PSLMEMFREE(pDispObj);

    status = PSLSUCCESS;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    SAFE_PSLMUTEXCLOSE(hMutexClose);
    return (status);
}


/*
 *  MTPDispatcherGetMsgQueue
 *
 *  Retrieves the message queue bound to this dispatcher
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  Dispatcher to query
 *      PSLMSGQUEUE*    pmqDispatch         Return buffer for dispatcher
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPDispatcherGetMsgQueue(MTPDISPATCHER md,
                    PSLMSGQUEUE* pmqDispatch)
{
    PSLSTATUS status;
    MTPDISPATCHEROBJ* pDispObj = PSLNULL;

    if (pmqDispatch)
    {
        *pmqDispatch = PSLNULL;
    }

    if (PSLNULL == md ||
        PSLNULL == pmqDispatch)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pDispObj = (MTPDISPATCHEROBJ*)md;

    PSLASSERT(MTPDISPATCHER_COOKIE == pDispObj->dwCookie);

    *pmqDispatch = PSLAtomicCompareExchangePointer(&pDispObj->mdi.mqDispatch,
                            PSLNULL, PSLNULL);

    status = PSLSUCCESS;

Exit:
    return status;
}


/*
 *  MTPDispatcherGetMsgPool
 *
 *  Returns the mesage pool bound to this dispatcher
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  MTP dispatcher to queury
 *      PSLMSGPOOL*     pmpDispatch         Return buffer for message pool
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPDispatcherGetMsgPool(MTPDISPATCHER md,
                    PSLMSGPOOL* pmpDispatch)
{
    PSLSTATUS status;
    MTPDISPATCHEROBJ* pDispObj = PSLNULL;

    if (pmpDispatch)
    {
        *pmpDispatch = PSLNULL;
    }

    if (PSLNULL == md ||
        PSLNULL == pmpDispatch)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pDispObj = (MTPDISPATCHEROBJ*)md;

    PSLASSERT(MTPDISPATCHER_COOKIE == pDispObj->dwCookie);

    *pmpDispatch = PSLAtomicCompareExchangePointer(&pDispObj->mdi.mpDispatch,
                            PSLNULL, PSLNULL);

    status = PSLSUCCESS;

Exit:
    return status;
}


/*
 *  MTPDisptacherRouteEnd
 *
 *  This is called when a particular route ends to notify the dispatcher.
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  Dispatcher being notified
 *      MTPCONTEXT*     pmtpctx             MTP context representing the route
 *                                            that is closing
 *
 *  Returns:
 *      PSLSTATUS       ps
 *
 */

PSLSTATUS PSL_API MTPDispatcherRouteEnd(MTPDISPATCHER md, MTPCONTEXT* pmtpctx)
{
    PSLSTATUS           ps;
    MTPDISPATCHEROBJ*   pDispObj;
    MTPMSGDISPATCHER*   pmd;

    /*  Validate arguments
     */

    if ((PSLNULL == md) || (PSLNULL == pmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the dispatcher information
     */

    pDispObj = (MTPDISPATCHEROBJ*)md;
    PSLASSERT(MTPDISPATCHER_COOKIE == pDispObj->dwCookie);

    /*  Clear the function pointer in the context
     */

    pmd = (MTPMSGDISPATCHER*)pmtpctx->mtpHandlerState.pbHandlerData;
    PSLASSERT(PSLNULL != pmd);
    if (PSLNULL == pmd)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }
    pmd->pfnMsgDispatch = PSLNULL;

    /*  Decrement the count of active contexts
     */

    if (0 < pDispObj->mdi.cActive)
    {
        pDispObj->mdi.cActive--;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPDispatcherRouteTerminate
 *
 *  This is called by the dispatcher client whenever the current route is
 *  to be terminated.  The function will clear any messages still pending
 *  for the route and then respond with the appropriate termination message.
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  Dispatcher servicing route to
 *                                            be terminated
 *      MTPCONTEXT*     pmtpctx             MTP context being terminated
 *      PSLMSGID        msgIDTerminate      Message that caused the
 *                                            termination
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPDispatcherRouteTerminate(MTPDISPATCHER md,
                    MTPCONTEXT* pmtpctx, PSLMSGID msgIDTerminate)
{
    MTPDISPATCHCLEANQUEUEINFO   cqi;
    PSLSTATUS                   ps;
    MTPDISPATCHEROBJ*           pDispObj;
    MTPDISPATCHERINFO*          pdi;
    PSLMSG*                     pMsgSend = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == md) || (PSLNULL == pmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine what the complete message should be while validating that
     *  the terminate message is accepatble
     */

    switch (msgIDTerminate)
    {
    case MTPMSG_SHUTDOWN:
        msgIDTerminate = MTPMSG_SHUTDOWN_COMPLETE;
        break;

    case MTPMSG_DISCONNECT:
        msgIDTerminate = MTPMSG_DISCONNECT_COMPLETE;
        break;

    case MTPMSG_RESET:
        msgIDTerminate = MTPMSG_RESET_COMPLETE;
        break;

    case MTPMSG_CANCEL:
        msgIDTerminate = MTPMSG_CANCEL_COMPLETE;
        break;

    case MTPMSG_ABORT:
        msgIDTerminate = MTPMSG_ABORT_COMPLETE;
        break;

    default:
        /*  Unknown termination message- report invalid parameter
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the dispatcher information
     */

    pDispObj = (MTPDISPATCHEROBJ*)md;
    PSLASSERT(MTPDISPATCHER_COOKIE == pDispObj->dwCookie);
    pdi = &(pDispObj->mdi);

    /*  Provide the information that the clean queue callback needs
     *  to locate messages and respond with buffer unused notifications
     */

    cqi.pdi = pdi;
    cqi.pmtpctx = pmtpctx;
    ps = PSLMsgEnum(pdi->mqDispatch, _MTPDispatcherCleanQueueEnum,
                    (PSLPARAM)&cqi);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  End the route as well
     */

    ps = MTPDispatcherRouteEnd(md, pmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Set the shutdown flag here if we have no remaining active
     *  contexts
     */

    if (MTPMSG_SHUTDOWN_COMPLETE == msgIDTerminate)
    {
        pdi->fShutdown = (0 == pdi->cActive);
    }

    /*  And send a message back to let the transport know that we are
     *  done
     */

    ps = PSLMsgAlloc(pdi->mpDispatch, &pMsgSend);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    pMsgSend->msgID = msgIDTerminate;
    pMsgSend->aParam0 = (PSLPARAM)pmtpctx;
    ps = MTPRouteMsgToTransport(pmtpctx, pMsgSend);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsgSend = PSLNULL;

Exit:
    SAFE_PSLMSGFREE(pMsgSend);
    return ps;
}


/*
 *  _MTPDispatcherCleanQueueEnum
 *
 *  Callback function used by PSLMsgEnum to help clear the queue of any
 *  pending messages when the dispatcher is closing down the current context
 *  for a disconnect or shutdown message.
 *
 *  Arguments:
 *      PSL_CONST PSLMSG*
 *                      pMsg                Message to check
 *      PSLPARAM        aParam              Callback parameter
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE to have
 *                        message removed, PSLSUCCESS_CANCEL to stop
 *                        enumeration
 *
 */

PSLSTATUS PSL_API _MTPDispatcherCleanQueueEnum(PSL_CONST PSLMSG* pMsg,
                    PSLPARAM aParam)
{
    MTPDISPATCHCLEANQUEUEINFO*  pcqi;
    PSLMSG*                     pMsgSend = PSLNULL;
    PSLSTATUS                   ps;

    /*  Validate arguments
     */

    if ((PSLNULL == pMsg) || (0 == aParam))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the information we need to check this message
     */

    pcqi = (MTPDISPATCHCLEANQUEUEINFO*)aParam;

    /*  First check to see if this message has no context or is for another
     *  context then the one specified- if so we just want to ignore it
     */

    if (!(MTPMSGID_CONTEXT_PRESENT & pMsg->msgID) ||
         ((MTPCONTEXT*)pMsg->aParam0 != pcqi->pmtpctx))
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  If this message contains a buffer make sure we return it to the
     *  transport
     */

    if (MTPMSGID_BUFFER_PRESENT & pMsg->msgID)
    {
        /*  Allocate a message to return to the transport
         */

        ps = PSLMsgAlloc(pcqi->pdi->mpDispatch, &pMsgSend);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsgSend->msgID = MTPMSG_BUFFER_IGNORED;
        pMsgSend->aParam0 = pMsg->aParam0;
        pMsgSend->aParam1 = pMsg->aParam1;
        pMsgSend->aParam2 = pMsg->aParam2;
        pMsgSend->aParam3 = pMsg->msgID;
        pMsgSend->alParam = pMsg->alParam;

        /*  And route the message
         */

        ps = MTPRouteMsgToTransport((MTPCONTEXT*)pMsgSend->aParam0,
                        pMsgSend);
        if (PSL_SUCCEEDED(ps))
        {
            pMsgSend = PSLNULL;
        }
    }

    /*  Remove this message from the queue
     */

    ps = PSLSUCCESS_FALSE;

Exit:
    SAFE_PSLMSGFREE(pMsgSend);
    return ps;
}

/*
 *  _MTPDispatcherCleanQueueEnumOnDestroy
 *
 *  Callback function used by PSLMsgEnum to help clear the queue of any
 *  pending messages when the dispatcher is getting destroyed
 *  This callback function won't ignore the messages w/o a context
 *
 *  Arguments:
 *      PSL_CONST PSLMSG*
 *                      pMsg                Message to check
 *      PSLPARAM        aParam              Callback parameter
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE to have
 *                        message removed, PSLSUCCESS_CANCEL to stop
 *                        enumeration
 *
 */

PSLSTATUS PSL_API _MTPDispatcherCleanQueueEnumOnDestroy(PSL_CONST PSLMSG* pMsg,
                                                PSLPARAM aParam)
{
    MTPDISPATCHCLEANQUEUEINFO*  pcqi;
    MTPDISPATCHCLEANQUEUEINFO   cqi;
    PSLMSG*                     pMsgSend = PSLNULL;
    PSLSTATUS                   ps;

    /*  Validate arguments
     */

    if ((PSLNULL == pMsg) || (0 == aParam))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the information we need to check this message
     */

    pcqi = (MTPDISPATCHCLEANQUEUEINFO*)aParam;
    PSLASSERT(PSLNULL == pcqi->pmtpctx);

    /*  First check to see if this message has a context
     *  and fwd to CleanQueueEnum function
     */

    if (MTPMSGID_CONTEXT_PRESENT & pMsg->msgID)
    {
        /* set the info clean queue callback needs */
        cqi.pdi = pcqi->pdi;
        cqi.pmtpctx = (MTPCONTEXT*)pMsg->aParam0;

        ps = _MTPDispatcherCleanQueueEnum(pMsg, (PSLPARAM)&cqi);

        goto Exit;
    }

    /*  If this message contains a buffer
     *  but it does not have a context present
     *  make sure we return it to the transport
     */

    if (MTPMSGID_BUFFER_PRESENT & pMsg->msgID)
    {
        /*  Allocate a message to return to the transport
         */

        ps = PSLMsgAlloc(pcqi->pdi->mpDispatch, &pMsgSend);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsgSend->msgID = MTPMSG_BUFFER_IGNORED;
        pMsgSend->aParam0 = pMsg->aParam0;
        pMsgSend->aParam1 = pMsg->aParam1;
        pMsgSend->aParam2 = pMsg->aParam2;
        pMsgSend->aParam3 = pMsg->msgID;
        pMsgSend->alParam = pMsg->alParam;

        /*  And route the message
         */

        ps = MTPRouteMsgToTransport((MTPCONTEXT*)pMsgSend->aParam0,
                        pMsgSend);
        if (PSL_SUCCEEDED(ps))
        {
            pMsgSend = PSLNULL;
        }
    }

    /*  Remove this message from the queue
     */

    ps = PSLSUCCESS_FALSE;

Exit:
    SAFE_PSLMSGFREE(pMsgSend);
    return ps;
}

/*
 *  _MTPDispatcherCallback
 *
 *  This function will be called by the message queue when a message is posted
 *  in a single threaded system.
 *
 *  Argumments:
 *      PSLVOID*        pvParam             Generic handle callback
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPDispatcherCallback(PSLVOID* pvParam)
{
    /*  The function will call dispatcher core fucntion
     */

    return _MTPDispatcherCore(pvParam);
}


/*
 *  _MTPDispatcherCore
 *
 *  This is the core dispatcher proc.  It is used tor retrieve and forward
 *  messages to handlers.
 *
 *  Arguments:
 *      PSLPOVID*       pvParam             Paramater
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPDispatcherCore(PSLVOID* pvParam)
{
    PSLBOOL             fDispatch;
    PSLBOOL             fDone = PSLFALSE;
    PSLBOOL             fShuttingDown = PSLFALSE;
    PSLMSG*             pMsg = PSLNULL;
    PFNMTPONCMDDISPATCH pfnOnCmd = PSLNULL;
    MTPDISPATCHERINFO*  pdi = PSLNULL;
    MTPMSGDISPATCHER*   pmd;
    MTPCONTEXT*         pmc;
    PSLSTATUS           status;

    /* Validate arguments */

    if (PSLNULL == pvParam)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLTraceDetail("_MTPDispatcherCore");

    /* Extract dispatch information */

    PSLASSERT(MTPDISPATCHER_COOKIE == ((MTPDISPATCHEROBJ*)pvParam)->dwCookie);
    pdi = &((MTPDISPATCHEROBJ*)pvParam)->mdi;
    PSLASSERT(PSLNULL != pdi);

    /*
     * How many times we need to loop :
     *
     * In a THREADED mode of operation - default - : until an exit
     * msg. is received.
     * In this mode, the Dispatcher will return to a "Waiting for
     * Message" model by calling PSLGetMessage.
     *
     * In a SINGLE THREADED mode of operation : only call GetMessage
     * once.  In this mode, the Dispatcher will need to exit immediately so
     * that control can be returned to the calling queue.
     */

    /* Loop to handle messages */

    do
    {
        /*  Release any existing message
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  Retrieve the next message
         */

        status = PSLMsgGet(pdi->mqDispatch, &pMsg);
        if (PSL_FAILED(status))
        {
            continue;
        }

        /*  Retrieve the context from the message
         *
         *  NOTE: Not all messages carry a context.  So we cannot assume
         *  that we have a context as this point.
         */

        pmc = PSLNULL;
        pmd = PSLNULL;
        if (MTPMSGID_CONTEXT_PRESENT & pMsg->msgID)
        {
            pmc = (MTPCONTEXT*)pMsg->aParam0;
            PSLASSERT(PSLNULL != pmc);
            if (PSLNULL != pmc)
            {
                /*  Save some redirection
                 */

                pmd = (MTPMSGDISPATCHER*)pmc->mtpHandlerState.pbHandlerData;
            }
        }

        /*  Handle messages- assume that they will be dispatched
         */

        fDispatch = PSLTRUE;
        switch (pMsg->msgID)
        {
        case MTPMSG_CMD_AVAILABLE:
            /*  For command available we want to call the command dispatched
             *  event if one is indicated
             */

            pfnOnCmd = pdi->pfnOnCmdDispatch;

            /*  FALL THROUGH INTENTIONAL */

        case MTPMSG_EVENT_AVAILABLE:
            /*  We should not be in the process of shutting down
             */

            PSLASSERT(!fShuttingDown);

            /*  We must have a context and handler data to handle this
             */

            PSLASSERT((PSLNULL != pmc) && (PSLNULL != pmd));
            if ((PSLNULL == pmc) || (PSLNULL == pmd))
            {
                status = PSLERROR_UNEXPECTED;
                goto Exit;
            }

            /*  We are beginning a new operation-need to initialize
             */

            ASSERT(PSLNULL == pmd->pfnMsgDispatch);
            if (PSLNULL != pmd->pfnMsgDispatch)
            {
                continue;
            }

            /*  Set the new handler functions
             */

            pmd->pfnMsgDispatch = (PFNMTPMSGDISPATCH)(pMsg->aParam3);
            PSLASSERT(PSLNULL != pmd->pfnMsgDispatch);

            /*  Increment the active count
             */

            pdi->cActive++;

            /*  And make sure we disptach the message
             */

            PSLASSERT(fDispatch);
            break;

        case MTPMSG_SHUTDOWN:
            /*  Remember that we are shutting down so that we make sure that
             *  no other contexts activate.  Because the shutdown message is
             *  a priority message we should get it before any other messages
             *  targetted for this context and then should immediately clear
             *  the queue of the pending messages so we should not have a
             *  problem with this.  However an incorrectly designed transport
             *  that posts additional messages following the shutdown
             *  notification could cause problems so we at least want to watch
             *  for the issue.
             */

            PSLASSERT(fDispatch);
            fShuttingDown = PSLTRUE;
            break;

        case MTPDISPATCHERMSG_END:
            /*  Create of dispatcher needs to send this message
             *  before it calls MTPDispatcherDestroy. This message
             *  needs to be send through the router so that the
             *  message will have the context as the first param
             *  if this message is not called MTPDispatcherDestroy
             *  will block indefinitely for the dispatcher thread.
             *
             *  It is assumed that the dispatcher is not currently handling
             *  an active context when this message is received.
             *
             *  NOTE: This is not a dispatchable message
             */

            PSLASSERT(0 == pdi->cActive);
            fDispatch = PSLFALSE;
            fDone = PSLTRUE;
            break;

        default:
            /* Dispatch this message
             */

            PSLASSERT(fDispatch);
            break;
        }

        /* Dispatch the message if requested and possible
         */
        if (fDispatch && (PSLNULL != pmc))
        {
            if ((PSLNULL != pmd) && (PSLNULL != pmd->pfnMsgDispatch))
            {
                pmd->pfnMsgDispatch((MTPDISPATCHER)pvParam, pMsg);

                if (PSLNULL != pfnOnCmd)
                {
                    pfnOnCmd(pMsg);
                }
            }
            else if (MTPMSGID_OUT_OF_BAND & PSL_MSG_FLAGS(pMsg->msgID))
            {
                /* This is the default handler for some out-of-band messages,
                 * these messages needs to be handled so that the transport can be
                 * dismissed from the status waiting for a COMPLETE message.
                 * Any other message passed to the function will hit an ASSERT, which
                 * is a valid unit test.
                 */
                MTPDispatcherRouteTerminate((MTPDISPATCHER)pvParam, pmc, pMsg->msgID);
            }
            else
            {
                PSLASSERT(MTPMSG_DATA_SENT == pMsg->msgID);
            }
        }
        pfnOnCmd = PSLNULL;
    }
    while (pdi->fThreaded && !pdi->fShutdown && !fDone);

    /*  Report success
     */

    status = PSLSUCCESS;

Exit:
    PSLASSERT((PSLNULL == pdi) ||
            ((pdi->fThreaded) && (pdi->fShutdown || fDone)));
    SAFE_PSLMSGFREE(pMsg);
    return (status);
}

