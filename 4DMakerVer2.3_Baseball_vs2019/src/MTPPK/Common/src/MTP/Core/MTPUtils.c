/*
 *  MTPUtils.c
 *
 *  Contains definition of MTP utility functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"

#ifndef MTP_STORAGE_INLINE

/*
 *  MTPLoadUInt16
 *
 *  Retrieves and swaps a UInt16 from the source
 *
 *  Arguments:
 *      PSL_CONST PSLUINT16 PSL_UNALIGNED*
 *                      pwSrc               Source
 *
 *  Returns:
 *      PSLUINT16       Value retrieved
 *
 */

PSLUINT16 PSL_API MTPLoadUInt16(PSL_CONST PSLUINT16 PSL_UNALIGNED* pwSrc)
{
    return MTPSwapUInt16(PSLLoadAlignUInt16(pwSrc));
}


/*
 *  MTPStoreUInt16
 *
 *  Swaps and stores a UInt16 from the source to the destination
 *
 *  Arguments:
 *      PSLUINT16 PSL_UNALIGNED*
 *                      pwDest              Destination
 *      PSLUINT16       wSrc                Source
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API MTPStoreUInt16(PSLUINT16 PSL_UNALIGNED* pwDest, PSLUINT16 wSrc)
{
    PSLStoreAlignUInt16(pwDest, MTPSwapUInt16(wSrc));
    return;
}


/*
 *  MTPCopyUInt16
 *
 *  Retrieves and swaps a UInt16 from the source to the destination
 *
 *  Arguments:
 *      PSLUINT16 PSL_UNALIGNED*
 *                      pwDest              Destination
 *      PSL_CONST PSLUINT16 PSL_UNALIGNED*
 *                      pwSrc               Source
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API MTPCopyUInt16(PSLUINT16 PSL_UNALIGNED* pwDest,
                            PSL_CONST PSLUINT16 PSL_UNALIGNED* pwSrc)
{
    PSLStoreAlignUInt16(pwDest, MTPSwapUInt16(PSLLoadAlignUInt16(pwSrc)));
    return;
}


/*
 *  MTPLoadUInt32
 *
 *  Retrieves and swaps a UInt32 from the source
 *
 *  Arguments:
 *      PSL_CONST PSLUINT32 PSL_UNALIGNED*
 *                      pdwSrc              Source
 *
 *  Returns:
 *      PSLUINT32       Value retrieved
 *
 */

PSLUINT32 PSL_API MTPLoadUInt32(PSL_CONST PSLUINT32 PSL_UNALIGNED* pdwSrc)
{
    return MTPSwapUInt32(PSLLoadAlignUInt32(pdwSrc));
}


/*
 *  MTPStoreUInt32
 *
 *  Swaps and stores a UInt32 from the source to the destination
 *
 *  Arguments:
 *      PSLUINT32 PSL_UNALIGNED*
 *                      pdwDest             Destination
 *      PSLUINT32       dwSrc               Source
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API MTPStoreUInt32(PSLUINT32 PSL_UNALIGNED* pdwDest,
                    PSLUINT32 dwSrc)
{
    PSLStoreAlignUInt32(pdwDest, MTPSwapUInt32(dwSrc));
    return;
}


/*
 *  MTPCopyUInt32
 *
 *  Retrieves and swaps a UInt32 from the source to the destination
 *
 *  Arguments:
 *      PSLUINT32 PSL_UNALIGNED*
 *                      pdwDest             Destination
 *      PSL_CONST PSLUINT32 PSL_UNALIGNED*
 *                      pdwSrc              Source
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API MTPCopyUInt32(PSLUINT32 PSL_UNALIGNED* pdwDest,
                            PSL_CONST PSLUINT32 PSL_UNALIGNED* pdwSrc)
{
    PSLStoreAlignUInt32(pdwDest, MTPSwapUInt32(PSLLoadAlignUInt32(pdwSrc)));
    return;
}


/*
 *  MTPStoreUInt64
 *
 *  Swaps and stores a UInt64 from the source to the destination
 *
 *  Arguments:
 *      PSLUINT64 PSL_UNALIGNED*
 *                      pqwDest             Destination
 *      PSL_CONST PSLUINT64*
 *                      pqwSrc              Source
 *
 *  Returns:
 *      Nothing
 *
 */


PSLVOID PSL_API MTPStoreUInt64(PSLUINT64 PSL_UNALIGNED* pqwDest,
                            PSL_CONST PSLUINT64* pqwSrc)
{
    MTPStoreUIntArray(pqwDest, pqwSrc, 1, sizeof(PSLUINT64));
    return;
}


/*
 *  MTPCopyUInt64
 *
 *  Retrieves and swaps a UInt64 from the source to the destination
 *
 *  Arguments:
 *      PSLUINT64 PSL_UNALIGNED*
 *                      pqwDest             Destination
 *      PSL_CONST PSLUINT64  PSL_UNALIGNED*
 *                      pqwSrc              Source
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API MTPCopyUInt64(PSLUINT64 PSL_UNALIGNED* pqwDest,
                            PSL_CONST PSLUINT64 PSL_UNALIGNED* pqwSrc)
{
    MTPLoadUIntArray(pqwDest, pqwSrc, 1, sizeof(PSLUINT64));
    return;
}


/*
 *  MTPStoreUInt128
 *
 *  Swaps and stores a UInt128 from the source to the destination
 *
 *  Arguments:
 *      PSLUINT128 PSL_UNALIGNED*
 *                      pdqwDest            Destination
 *      PSL_CONST PSLUINT128*
 *                      pdqwSrc             Source
 *
 *  Returns:
 *      Nothing
 *
 */


PSLVOID PSL_API MTPStoreUInt128(PSLUINT128 PSL_UNALIGNED* pdqwDest,
                            PSL_CONST PSLUINT128* pdqwSrc)
{
    MTPStoreUIntArray(pdqwDest, pdqwSrc, 1, sizeof(PSLUINT128));
    return;
}


/*
 *  MTPCopyUInt128
 *
 *  Retrieves and swaps a UInt128 from the source to the destination
 *
 *  Arguments:
 *      PSLUINT128 PSL_UNALIGNED*
 *                      pdqwDest            Destination
 *      PSL_CONST PSLUINT128  PSL_UNALIGNED*
 *                      pdqwSrc             Source
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API MTPCopyUInt128(PSLUINT128* pdqwDest,
                            PSL_CONST PSLUINT128 PSL_UNALIGNED* pdqwSrc)
{
    MTPLoadUIntArray(pdqwDest, pdqwSrc, 1, sizeof(PSLUINT128));
    return;
}

#endif  /* MTP_STORAGE_INLINE */

/*
 *  MTPLoadUInt64
 *
 *  Retrieves and swaps a UInt64 from the source
 *
 *  Arguments:
 *      PSL_CONST PSLUINT64 PSL_UNALIGNED*
 *                      pqwSrc              Source
 *
 *  Returns:
 *      PSLUINT64       Value retrieved
 *
 */

PSLUINT64 PSL_API MTPLoadUInt64(PSL_CONST PSLUINT64 PSL_UNALIGNED* pqwSrc)
{
    PSLUINT64       qwValue;

    MTPCopyUInt64(&qwValue, pqwSrc);
    return qwValue;
}


/*
 *  MTPLoadUIntArray
 *
 *  Loads and swaps an array of integers from the source
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Destination to load array to
 *      PSL_CONST PSLVOID*
 *                      pvSrc               Source to load array from
 *      PSLUINT32       cItems              Number of items to load
 *      PSLUINT32       cbItem              Size of each item
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API MTPLoadUIntArray(PSLVOID* pvDest, PSL_CONST PSLVOID* pvSrc,
                            PSLUINT32 cItems, PSLUINT32 cbItem)
{
#ifdef PSL_BIG_ENDIAN
    PSLBYTE*        pbDest;
    PSLBYTE*        pbSrc;
#endif  /* PSL_BIG_ENDIAN */

    /*  Must have a destination and a source
     */

    PSLASSERT((PSLNULL != pvDest) && (PSLNULL != pvSrc));

    /*  Determine what size of item we are dealing with- we only support
     *  integer types
     */

    switch (cbItem)
    {
    case 1:
        /*  We are copying bytes- this just degenerates to a PSLCopyMemory
         */

        PSLCopyMemory(pvDest, pvSrc, cItems);
        break;

    case 2:
    case 4:
    case 8:
    case 16:
        /*  Directly handle swapping if necessary to try and get the most
         *  optimal performance
         */

#ifdef PSL_BIG_ENDIAN
        /*  Reverse each entry in the list
         */

        pbSrc = pvSrc;
        pbDest = pvDest;
        for (idxItem = cItems; 0 < idxItem; idxItem--)
        {
            for (idxByte = cbItem; 0 < idxByte; idxByte--, pbSrc++)
            {
                *pbSrc = pbDest[idxByte - 1];
            }
            pbDest += cbItem;
        }
#else   /* !PSL_BIG_ENDIAN */
        /*  This degenerates to a PSLCopyMemory- just use it
         */

        PSLCopyMemory(pvDest, pvSrc, (cItems * cbItem));
#endif  /*  PSL_BIG_ENDIAN */
        break;

    default:
        /*  Should never get here
         */

        PSLASSERT(PSLFALSE);
        PSLZeroMemory(pvDest, (cItems* cbItem));
        break;
    }
    return;
}


/*
 *  MTPStoreUIntArray
 *
 *  Swaps and stores an array of integers from the source
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Destination to load array to
 *      PSL_CONST PSLVOID*
 *                      pvSrc               Source to load array from
 *      PSLUINT32       cItems              Number of items to load
 *      PSLUINT32       cbItem              Size of each item
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID PSL_API MTPStoreUIntArray(PSLVOID* pvDest, PSL_CONST PSLVOID* pvSrc,
                            PSLUINT32 cItems, PSLUINT32 cbItem)
{
    MTPLoadUIntArray(pvDest, pvSrc, cItems, cbItem);
    return;
}


/*  MTPLoadValue
 *
 *  Loads a value of the specified type into the destination
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Location to store the result
 *      PSLUINT32       cbDest              Size of the destination buffer
 *      PSLUINT16       wDataType           Type of data to load
 *      PSLVOID*        pvSrc               Source for the data
 *      PSLUINT32       cbSrc               Size of the source buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPLoadValue(PSLVOID* pvDest, PSLUINT32 cbDest,
                    PSLUINT16 wDataType, PSL_CONST PSLVOID* pvSrc,
                    PSLUINT32 cbSrc)
{
    PSLUINT32       cItems;
    PSLUINT32       cbSize;
    PSLSTATUS       ps;

    /*  Make sure we have a source and a destination
     */

    if ((PSLNULL == pvDest) || (PSLNULL == pvSrc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine how to load the value
     */

    switch (wDataType)
    {
    case MTP_DATATYPE_INT8:
    case MTP_DATATYPE_UINT8:
        cItems = 1;
        cbSize = sizeof(PSLUINT8);
        break;

    case MTP_DATATYPE_INT16:
    case MTP_DATATYPE_UINT16:
        cItems = 1;
        cbSize = sizeof(PSLUINT16);
        break;

    case MTP_DATATYPE_INT32:
    case MTP_DATATYPE_UINT32:
        cItems = 1;
        cbSize = sizeof(PSLUINT32);
        break;


    case MTP_DATATYPE_INT64:
    case MTP_DATATYPE_UINT64:
        cItems = 1;
        cbSize = sizeof(PSLUINT64);
        break;


    case MTP_DATATYPE_INT128:
    case MTP_DATATYPE_UINT128:
        cItems = 1;
        cbSize = sizeof(PSLUINT128);
        break;

    case MTP_DATATYPE_AINT8:
    case MTP_DATATYPE_AUINT8:
    case MTP_DATATYPE_AINT16:
    case MTP_DATATYPE_AUINT16:
    case MTP_DATATYPE_AINT32:
    case MTP_DATATYPE_AUINT32:
    case MTP_DATATYPE_AINT64:
    case MTP_DATATYPE_AUINT64:
    case MTP_DATATYPE_AINT128:
    case MTP_DATATYPE_AUINT128:
        /*  Make sure there is enough space to retrieve the data
         */

        if (sizeof(PSLUINT32) > cbSrc)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Retrieve the length from the data
         */

        cItems = MTPLoadUInt32((PCXPSLUINT32)pvSrc);
        pvSrc = (PSLBYTE*)pvSrc + sizeof(PSLUINT32);
        cbSrc -= sizeof(PSLUINT32);

        /*  And determine the size
         */

        switch (wDataType)
        {
        case MTP_DATATYPE_AINT8:
        case MTP_DATATYPE_AUINT8:
            cbSize = sizeof(PSLUINT8);
            break;

        case MTP_DATATYPE_AINT16:
        case MTP_DATATYPE_AUINT16:
            cbSize = sizeof(PSLUINT16);
            break;

        case MTP_DATATYPE_AINT32:
        case MTP_DATATYPE_AUINT32:
            cbSize = sizeof(PSLUINT32);
            break;

        case MTP_DATATYPE_AINT64:
        case MTP_DATATYPE_AUINT64:
            cbSize = sizeof(PSLUINT64);
            break;

        case MTP_DATATYPE_AINT128:
        case MTP_DATATYPE_AUINT128:
            cbSize = sizeof(PSLUINT128);
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }
        break;

    case MTP_DATATYPE_STRING:
        /*  Handle the string directly
         */

        ps = MTPLoadMTPString((PSLWSTR)pvDest, (cbDest >> 1), PSLNULL,
                            (PCXMTPSTRING)pvSrc, cbSrc);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Report no items available so that we do not try to do any more
         *  work
         */

        cItems = 0;
        cbSize = 0;
        break;

    default:
        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If we have no items to load we are done
     */

    if (0 == cItems)
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Figure out if we have enough space to load the items- and if we have
     *  enough space to store them
     */

    if ((cItems * cbSize) > cbSrc)
    {
        ps = PSLERROR_INVALID_DATA;
        goto Exit;
    }

    if ((cItems * cbSize) > cbDest)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Go ahead and retrieve the items
     */

    MTPLoadUIntArray(pvDest, pvSrc, cItems, cbSize);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPStoreValue
 *
 *  Store the value based on the specified type
 *
 *  Arguments:
 *      PSLVOID         pvDest              Destination to store the value
 *      PSLUINT32       cbDest              Size of the destination buffer
 *      PSLUINT16       wDataType           Type of data to store
 *      PSLVOID*        pvSrc               Source data to store
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPStoreValue(PSLVOID* pvDest, PSLUINT32 cbDest,
                            PSLUINT16 wDataType, PSL_CONST PSLVOID* pvSrc)
{
    PSLUINT32       cItems;
    PSLUINT32       cbSize;
    PSLSTATUS       ps;

    /*  Must have a source and destination
     */

    if ((PSLNULL == pvDest) || (PSLNULL == pvSrc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine how to store the value
     */

    switch (wDataType)
    {
    case MTP_DATATYPE_INT8:
    case MTP_DATATYPE_UINT8:
        cItems = 1;
        cbSize = sizeof(PSLUINT8);
        break;

    case MTP_DATATYPE_INT16:
    case MTP_DATATYPE_UINT16:
        cItems = 1;
        cbSize = sizeof(PSLUINT16);
        break;

    case MTP_DATATYPE_INT32:
    case MTP_DATATYPE_UINT32:
        cItems = 1;
        cbSize = sizeof(PSLUINT32);
        break;


    case MTP_DATATYPE_INT64:
    case MTP_DATATYPE_UINT64:
        cItems = 1;
        cbSize = sizeof(PSLUINT64);
        break;


    case MTP_DATATYPE_INT128:
    case MTP_DATATYPE_UINT128:
        cItems = 1;
        cbSize = sizeof(PSLUINT128);
        break;

    case MTP_DATATYPE_AINT8:
    case MTP_DATATYPE_AUINT8:
    case MTP_DATATYPE_AINT16:
    case MTP_DATATYPE_AUINT16:
    case MTP_DATATYPE_AINT32:
    case MTP_DATATYPE_AUINT32:
    case MTP_DATATYPE_AINT64:
    case MTP_DATATYPE_AUINT64:
    case MTP_DATATYPE_AINT128:
    case MTP_DATATYPE_AUINT128:
        /*  Arrays are not currently supported
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;

    case MTP_DATATYPE_STRING:
        /*  Handle the string directly
         */

        ps = MTPStoreMTPString((PXMTPSTRING)pvDest, cbDest, PSLNULL,
                            (PSLCWSTR)pvSrc, MAX_MTP_STRING_LEN);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Report no items available so that we do not try to do any more
         *  work
         */

        cItems = 0;
        cbSize = 0;
        break;

    default:
        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If we have no items to load we are done
     */

    if (0 == cItems)
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Figure out if we have enough space to store the items
     */

    if ((cItems * cbSize) > cbDest)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Go ahead and store the items
     */

    MTPStoreUIntArray(pvDest, pvSrc, cItems, cbSize);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPUTF16StringLen
 *
 *  Determines the length of a string up to the specified maximum length.
 *
 *  Arguments:
 *      PSL_CONST PSLWCHAR PSL_UNALIGNED*
 *                      szSrc               String to determine length of
 *      PSLUINT32       cchMaxSrc           Maximum length of the string
 *      PSLUINT32*      pcchSrc             Resulting length of the string
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPUTF16StringLen(PSL_CONST PSLWCHAR PSL_UNALIGNED* szSrc,
                            PSLUINT32 cchMaxSrc, PSLUINT32* pcchSrc)
{
    PSL_CONST PSLWCHAR PSL_UNALIGNED*   pch;
    PSLSTATUS                           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcchSrc)
    {
        *pcchSrc = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == szSrc) || (PSLSTRING_MAX_CCH < cchMaxSrc) ||
        (PSLNULL == pcchSrc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine the length
     */

    for (pch = szSrc;
            (L'\0' != MTPLoadUInt16(pch)) && (0 < cchMaxSrc);
            pch++, cchMaxSrc--)
    {
    }

    /*  Return the length
     */

    *pcchSrc = pch - szSrc;

    /*  If we encountered the maximum length of the string but not a
     *  terminating character report the user
     */

    ps = (L'\0' == MTPLoadUInt16(pch)) ? PSLSUCCESS : PSLSUCCESS_NOT_FOUND;

Exit:
    return ps;
}


/*
 *  MTPLoadUTF16String
 *
 *  Loads up to cchSrc characters to szDest while making sure that szDest does
 *  not overflow.  Optionally returns the number of characters copied.
 *  Guarantees that szDest is terminated.  Handles any byte swapping necessary.
 *
 *  Arguments:
 *      PSLWCHAR*       szDest              Destination for the string
 *      PSLUINT32       cchMaxDest          Size of the destination buffer
 *      PSLUINT32*      pcchDest            Optional buffer to return number
 *                                            of characters copied
 *      PSL_CONST PSLWCHAR PSL_UNALIGNED*
 *                      szSrc               Source string MTP string location
 *      PSLUINT32       cchSrc              Number of bytes to copy,
 *                                            PSLSTRING_MAX_CCH to copy all
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPLoadUTF16String(PSLWCHAR* szDest, PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest,
                            PSL_CONST PSLWCHAR PSL_UNALIGNED* szSrc,
                            PSLUINT32 cchSrc)
{
    PSLSTATUS                           ps;
    PSL_CONST PSLWCHAR PSL_UNALIGNED*   pchSrc;
    PSLWCHAR*                           pchDest;

    /*  Clear results for safety
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = 0;
    }
    if ((PSLNULL != szDest) && (0 < cchMaxDest))
    {
        *szDest = L'\0';
    }

    /*  Validate arguments
     */

    if ((PSLNULL == szDest) || (PSLSTRING_MAX_CCH < cchMaxDest))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Report insufficient buffer if we do not have space to even store a
     *  terminator
     */

    if (0 == cchMaxDest)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Make sure we have work to do
     */

    if ((PSLNULL == szSrc) || (0 == cchSrc))
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Start copying the string over- note that we are reserving space in
     *  the destination for the terminating character
     */

    for (pchSrc = szSrc, pchDest = szDest;
            (L'\0' != MTPLoadUInt16(pchSrc)) && (1 < cchMaxDest) &&
                (0 < cchSrc);
            MTPCopyUInt16(pchDest, pchSrc), pchSrc++, pchDest++,
                cchMaxDest--, cchSrc--)
    {
    }

    /*  Guarantee termination
     */

    *pchDest = L'\0';

    /*  Return characters copied if requested
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = pchDest - szDest;
    }

    /*  Determine whether or not we ran out of space while performing the
     *  copy
     */

    ps = ((1 < cchMaxDest) ||
            ((L'\0' == MTPLoadUInt16(pchSrc)) || (0 == cchSrc))) ?
                            PSLSUCCESS : PSLERROR_INSUFFICIENT_BUFFER;

Exit:
    return ps;
}


/*
 *  MTPStoreUTF16String
 *
 *  Copies up to cchSrc characters to szDest while making sure that szDest does
 *  not overflow.  Optionally returns the number of characters copied.
 *  Guarantees that szDest is terminated.
 *
 *  Arguments:
 *      PSLWCHAR PSL_UNALIGNED*
 *                      szDest              Destination for the copy
 *      PSLUINT32       cchMaxDest          Size of the destination buffer
 *      PSLUINT32*      pcchDest            Optional buffer to return number
 *                                            of characters copied
 *      PSLCWSTR        szSrc               Source string
 *      PSLUINT32       cchSrc              Number of bytes to copy,
 *                                            PSLSTRING_MAX_CCH to copy all
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStoreUTF16String(PSLWCHAR PSL_UNALIGNED* szDest,
                            PSLUINT32 cchMaxDest, PSLUINT32* pcchDest,
                            PSLCWSTR szSrc, PSLUINT32 cchSrc)
{
    PSLSTATUS               ps;
    PSL_CONST PSLWCHAR*     pchSrc;
    PSLWCHAR PSL_UNALIGNED* pchDest;

    /*  Clear results for safety
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = 0;
    }
    if ((PSLNULL != szDest) && (0 < cchMaxDest))
    {
        MTPStoreUInt16(szDest, L'\0');
    }

    /*  Validate arguments
     */

    if ((PSLNULL == szDest) || (PSLSTRING_MAX_CCH < cchMaxDest))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Report insufficient buffer if we do not have space to even store a
     *  terminator
     */

    if (0 == cchMaxDest)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Make sure we have work to do
     */

    if ((PSLNULL == szSrc) || (0 == cchSrc))
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Start copying the string over- note that we are reserving space in
     *  the destination for the terminating character
     */

    for (pchSrc = szSrc, pchDest = szDest;
            (L'\0' != *pchSrc) && (1 < cchMaxDest) && (0 < cchSrc);
            MTPStoreUInt16(pchDest, *pchSrc), pchSrc++, pchDest++,
                cchMaxDest--, cchSrc--)
    {
    }

    /*  Guarantee termination
     */

    MTPStoreUInt16(pchDest, L'\0');

    /*  Return characters copied if requested
     */

    if (PSLNULL != pcchDest)
    {
        *pcchDest = pchDest - szDest;
    }

    /*  Determine whether or not we ran out of space while performing the
     *  copy
     */

    ps = ((1 < cchMaxDest) || ((L'\0' == *pchSrc) || (0 == cchSrc))) ?
                            PSLSUCCESS : PSLERROR_INSUFFICIENT_BUFFER;

Exit:
    return ps;
}


/*
 *  MTPLoadMTPString
 *
 *  Reads an MTPSTRING out of an MTP packet.  Returns a standard terminated
 *  UTF-16 string in the appropriate byte order.
 *
 *  Arguments:
 *      PSLWCHAR*       szDest              Destination string location
 *      PSLUINT32       cchMaxDest          Amount of space in the destination
 *      PSLUINT32*      pcchDest            Number of characters copied not
 *                                            including NULL terminator
 *      PCMTPSTRING     pmstrSrc            Source MTP string
 *      PSLUINT32       cbSrc               Number of bytes available in the
 *                                            source
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLERROR_INSUFFICIENT_BUFFER if
 *                        destination is too small
 *
 */

PSLSTATUS PSL_API MTPLoadMTPString(PSLWCHAR* szDest, PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest, PCXMTPSTRING pmstrSrc,
                            PSLUINT32 cbSrc)
{
    PSLUINT32       cchDest;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != szDest)
    {
        *szDest = L'\0';
    }
    if (PSLNULL != pcchDest)
    {
        *pcchDest = 0;
    }

    /*  Validate arguments
     */

    if (((PSLNULL == szDest) && (PSLNULL == pcchDest)) ||
        ((0 < cchMaxDest) && (PSLNULL == szDest)) ||
        (PSLNULL == pmstrSrc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure that the string length specified is valid for the amount of
     *  space provided- remember that the length is specified in the number
     *  of bytes for the MTPSTRING not the number of characters in the
     *  string.
     */

    if ((0 == cbSrc) || (((cbSrc - 1) >> 1) < pmstrSrc->cchLen))
    {
        ps = PSLERROR_INVALID_DATA;
        goto Exit;
    }

    /*  If we are being asked to determine the length only then we want
     *  to walk the string and validate the length
     */

    if (PSLNULL == szDest)
    {
        //  Validate the length of the string

        ps = MTPUTF16StringLen(pmstrSrc->rgchStr, pmstrSrc->cchLen, &cchDest);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        //  Return the length if the data is valid

        if ((PSLSUCCESS == ps) ||
            ((PSLSUCCESS_NOT_FOUND == ps) && (0 == pmstrSrc->cchLen)))
        {
            PSLASSERT((0xff & cchDest) == cchDest);
            *pcchDest = (PSLBYTE)cchDest;
        }
        else
        {
            ps = PSLERROR_INVALID_DATA;
        }
        goto Exit;
    }

    /*  Read the string using the standard load routine- according to the
     *  MTP spec these strings must be terminated so we can just use the
     *  length as a delimeter.
     *
     *  SECURITY NOTE:
     *  Given the design of MTPLoadUTF16String this provides the safest way
     *  to read the string.  If pmstrSrc->cchLen is incorrect (either too small
     *  or too large) we are still guaranteed to get a safe string because
     *  the NULL terminator will be checked and we are limited to at most 255
     *  characters.  In addition a bug in the Microsoft MTP Initiator
     *  sometimes causes the length of an MTP string to be improperly sent-
     *  it is too large (e.g. the NULL termination occurs in the string
     *  prior to the length specified).  It is preferred here to reject as
     *  bad data any string whose terminator is not found at the location
     *  specified by the length but due to this bug it is not possible to
     *  enforce.
     */

    ps = MTPLoadUTF16String(szDest, cchMaxDest, &cchDest, pmstrSrc->rgchStr,
                            pmstrSrc->cchLen);
    if (PSL_FAILED(ps))
    {
        *szDest = L'\0';
        PSLASSERT((PSLNULL == pcchDest) || (0 == *pcchDest));
        goto Exit;
    }

    /*  Return length if requested
     */

    if (PSLNULL != pcchDest)
    {
        PSLASSERT((0xff & cchDest) == cchDest);
        *pcchDest = cchDest;
    }

Exit:
    return ps;
}


/*
 *  MTPStoreMTPString
 *
 *  Stores a standard UTF-16 string as an MTPSTRING performing any necessary
 *  byte swapping.
 *
 *  Arguments:
 *      PXMTPSTRING     pmstrDest           Destination MTP string
 *      PSLUINT32       cbDest              Space available in the destination
 *      PSLUINT32*      pcbWritten          Number of bytes written to the
 *                                            destination
 *      PSLCWSTR        szSrc               Source string
 *      PSLUINT32       cchSrc              Length of the source string
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLERROR_INSUFFICIENT_BUFFER
 *                        if not enough space
 *
 */

PSLSTATUS PSL_API MTPStoreMTPString(PXMTPSTRING pmstrDest, PSLUINT32 cbDest,
                            PSLUINT32* pcbWritten, PSLCWSTR szSrc,
                            PSLUINT32 cchSrc)
{
    PSLUINT32       cchDest;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if ((PSLNULL != pmstrDest) && (0 < cbDest))
    {
        pmstrDest->cchLen = 0;

        /*  Also insert a terminator in the string if there is space-
         *  remember that we have to account for a 1 byte length and a
         *  2 byte terminator or a minimum of 3 bytes
         */

        if (2 < cbDest)
        {
            MTPStoreUInt16(pmstrDest->rgchStr, L'\0');
        }
    }
    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }

    /*  Validate arguments
     */

    if (((0 < cbDest) && (PSLNULL == pmstrDest)) ||
        ((PSLNULL == pmstrDest) && (PSLNULL == pcbWritten)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If the source string is empty we do not want to copy anything
     *  over- just let the empty string stand
     */

    if ((0 == cchSrc) || (PSLNULL == szSrc) || (L'\0' == *szSrc))
    {
        if (PSLNULL != pcbWritten)
        {
            *pcbWritten = 1;
        }
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  If we were asked to just determine the storage length of the string
     *  then calculate it
     */

    if (PSLNULL == pmstrDest)
    {
        //  Determine the length of the source string

        ps = PSLStringLen(szSrc, MAX_MTP_STRING_LEN, &cchDest);
        if (PSLSUCCESS != ps)
        {
            ps = (PSL_FAILED(ps)) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        //  String is of the appropriate length- calculate the number of
        //  bytes required to hold it

        *pcbWritten = sizeof(PSLBYTE) + ((cchDest + 1) * sizeof(PSLWCHAR));
        goto Exit;
    }

    /*  We must have space for at least the length
     */

    if (0 == cbDest)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Use the standard string to store the value- note that the space
     *  provided is in bytes to store the MTPSTRING not characters in
     *  the string so we need to account for the length.
     */

    ps = MTPStoreUTF16String(pmstrDest->rgchStr,
                            min(((cbDest - 1) >> 1), MAX_MTP_STRING_LEN),
                            &cchDest, szSrc, cchSrc);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And add the length into the string- note that the length specified
     *  here is to include the NULL terminator
     */

    PSLASSERT((cchDest + 1) <= MAX_MTP_STRING_LEN);
    pmstrDest->cchLen = (PSLBYTE)(cchDest + 1);

    /*  And return the number of bytes consumed- this is the length of the
     *  string plus the length byte
     */

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = sizeof(pmstrDest->cchLen) +
                            (pmstrDest->cchLen * sizeof(PSLWCHAR));
    }

Exit:
    return ps;
}


/*
 *  MTPStringCopy
 *
 *  Copies the source MTP string to the destination string and guarantees
 *  NULL Termination.
 *
 *  Arguments:
 *      PXMTPSTRING     pmstrDest           Destination MTP string
 *      PSLUINT32       cbDest              Size (in bytes) for the MTPString
 *      PCXMTPSTRING    pmstrSrc            Source string
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if copy succeeded
 *
 */

PSLSTATUS PSL_API MTPStringCopy(PXMTPSTRING pmstrDest, PSLUINT32 cbDest,
                            PCXMTPSTRING pmstrSrc)
{
    PSLUINT32                           cchMaxDest;
    PSLUINT32                           cchSrc;
    PSLSTATUS                           ps;
    PSL_CONST PSLWCHAR PSL_UNALIGNED*   pchSrc;
    PSLWCHAR PSL_UNALIGNED*             pchDest;

    /*  Clear results for safety
     */

    if ((PSLNULL != pmstrDest) && (0 < cbDest))
    {
        pmstrDest->cchLen = 0;

        /*  Also insert a terminator in the string if there is space-
         *  remember that we have to account for a 1 byte length and a
         *  2 byte terminator or a minimum of 3 bytes
         */

        if (2 < cbDest)
        {
            MTPStoreUInt16(pmstrDest->rgchStr, L'\0');
        }
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmstrDest) || (0 == cbDest))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we have work to do
     */

    if ((PSLNULL == pmstrSrc) || (0 == pmstrSrc->cchLen))
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  We have at least something to copy- make sure we have space for
     *  at least one character
     */

    cchMaxDest = (cbDest - 1) >> 1;
    if (0 == cchMaxDest)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Start copying the string over- note that we are reserving space in
     *  the destination for the terminating character
     */

    for (cchSrc = pmstrSrc->cchLen, pchSrc = pmstrSrc->rgchStr,
                pchDest = pmstrDest->rgchStr;
            (L'\0' != MTPLoadUInt16(pchSrc)) &&
                (1 < cchMaxDest) && (0 < cchSrc);
            PSLCopyAlignUInt16(pchDest, pchSrc), pchSrc++, pchDest++,
                cchMaxDest--, cchSrc--)
    {
    }

    /*  Guarantee termination
     */

    MTPStoreUInt16(pchDest, L'\0');

    /*  Set the size of the string- remember that according to the MTP
     *  spec the length includes the NULL terminator
     */

    pmstrDest->cchLen = (PSLBYTE)(pchDest - pmstrDest->rgchStr) + 1;

    /*  Determine whether or not we ran out of space while performing the
     *  copy
     */

    ps = ((1 < cchMaxDest) ||
            ((L'\0' == MTPLoadUInt16(pchSrc)) || (0 == cchSrc))) ?
                            PSLSUCCESS : PSLERROR_INSUFFICIENT_BUFFER;

Exit:
    return ps;
}



/*
 *  MTPStringCmp
 *
 *  Compares the contents of two strings in a case sensitive manner and
 *  reports the result
 *
 *  Arguments:
 *      PXCMTPSTRING    pmstr1              First string to compare
 *      PXCMTPSTRING    pmstr2              Second string to compare
 *
 *  Returns:
 *      PSLINT32        < 0 - pmstr1 < pmstr2
 *                      = 0 - pmstr1 == pmstr2
 *                      > 0 - pmstr1 > pmstr2
 *
 */

PSLINT32 PSL_API MTPStringCmp(PCXMTPSTRING pmstr1, PCXMTPSTRING pmstr2)
{
    PSLUINT32                           cch1;
    PSLUINT32                           cch2;
    PSLINT32                            nResult;
    PSL_CONST PSLWCHAR PSL_UNALIGNED*   pch1;
    PSL_CONST PSLWCHAR PSL_UNALIGNED*   pch2;

    /*  Make sure we have work to do- we treat a NULL string as less then
     *  an empty string
     */

    if ((PSLNULL == pmstr1) || (0 == pmstr1->cchLen))
    {
        nResult = ((PSLNULL == pmstr2) || (0 == pmstr2->cchLen)) ? 0 : -1;
        goto Exit;
    }
    if ((PSLNULL == pmstr2) || (0 == pmstr2->cchLen))
    {
        nResult = ((PSLNULL == pmstr1) || (0 == pmstr1->cchLen)) ? 0 : 1;
        goto Exit;
    }

    /*  We should have strings at this point because we have lengths
     */

    PSLASSERT((PSLNULL != pmstr1->rgchStr) && (PSLNULL != pmstr2->rgchStr));

    /*  Walk through the string looking for a difference
     */

    cch1 = pmstr1->cchLen;
    pch1 = pmstr1->rgchStr;
    cch2 = pmstr1->cchLen;
    pch2 = pmstr2->rgchStr;
    for (nResult = (PSLINT32)MTPLoadUInt16(pch1) -
                (PSLINT32)MTPLoadUInt16(pch2);
            (0 == nResult) && (0 < cch1) && (0 < cch2) &&
                (L'\0' != MTPLoadUInt16(pch1)) && (L'\0' != MTPLoadUInt16(pch2));
            nResult = ((PSLINT32)MTPLoadUInt16(++pch1) -
                (PSLINT32)MTPLoadUInt16(++pch2)), cch1--, cch2--)
    {
    }

Exit:
    return nResult;
}


/*
 *  MTPStringICmp
 *
 *  Compares the contents of two strings in a case insensitive manner and
 *  reports the result
 *
 *  Arguments:
 *      PCXMTPSTRING    pmstr1              First string to compare
 *      PCXMTPSTRING    pmstr2              Second string to compare
 *
 *  Returns:
 *      PSLINT32        < 0 - pmstr1 < pmstr2
 *                      = 0 - pmstr1 == pmstr2
 *                      > 0 - pmstr1 > pmstr2
 *
 */

PSLINT32 PSL_API MTPStringICmp(PCXMTPSTRING pmstr1, PCXMTPSTRING pmstr2)
{
    PSLUINT32                           cch1;
    PSLUINT32                           cch2;
    PSLINT32                            nResult;
    PSL_CONST PSLWCHAR PSL_UNALIGNED*   pch1;
    PSL_CONST PSLWCHAR PSL_UNALIGNED*   pch2;

    /*  Make sure we have work to do- we treat a NULL string as less then
     *  an empty string
     */

    if ((PSLNULL == pmstr1) || (0 == pmstr1->cchLen))
    {
        nResult = ((PSLNULL == pmstr2) || (0 == pmstr2->cchLen)) ? 0 : -1;
        goto Exit;
    }
    if ((PSLNULL == pmstr2) || (0 == pmstr2->cchLen))
    {
        nResult = ((PSLNULL == pmstr1) || (0 == pmstr1->cchLen)) ? 0 : 1;
        goto Exit;
    }

    /*  We should have strings at this point because we have lengths
     */

    PSLASSERT((PSLNULL != pmstr1->rgchStr) && (PSLNULL != pmstr2->rgchStr));

    /*  Walk through the string looking for a difference
     */

    cch1 = pmstr1->cchLen;
    pch1 = pmstr1->rgchStr;
    cch2 = pmstr1->cchLen;
    pch2 = pmstr2->rgchStr;
    for (nResult = (PSLINT32)PSLToLower(MTPLoadUInt16(pch1)) -
                (PSLINT32)PSLToLower(MTPLoadUInt16(pch2));
            (0 == nResult) && (0 < cch1) && (0 < cch2) &&
                (L'\0' != MTPLoadUInt16(pch1)) && (L'\0' != MTPLoadUInt16(pch2));
            nResult = ((PSLINT32)PSLToLower(MTPLoadUInt16(++pch1)) -
                (PSLINT32)PSLToLower(MTPLoadUInt16(++pch2))), cch1--, cch2--)
    {
    }

Exit:
    return nResult;
}




