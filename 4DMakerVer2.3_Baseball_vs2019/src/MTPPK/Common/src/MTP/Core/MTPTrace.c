/*
 *  MTPTrace.c
 *
 *  Contains implementations of helpful MTP trace and debug functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"

/*  Local Defines
 */

/*  Local Types
 */

typedef struct _STRINGTABLE
{
    PSLUINT16               wCode;
    PSLCSTR                 szName;
} STRINGTABLE;

/*  Local Functions
 */

static PSLINT32 PSL_API _StringTableCompare(
                            PSL_CONST PSLVOID* pvItem,
                            PSL_CONST PSLVOID* pvKey);

static PSLVOID _StringTableValidate(
                            PSL_CONST STRINGTABLE* pstbl,
                            PSLUINT32 cItems);

/*  Local Variables
 */

/*  NOTE:
 *  Lookup implementation assumes the tables are sorted so that BSearch
 *  can be used instead of linear search.
 */

static PSL_CONST STRINGTABLE    _RgstblOpCode[] =
{
    {   0x1000,     "MTP_OPCODE_UNDEFINED"                              },
    {   0x1001,     "MTP_OPCODE_GETDEVICEINFO"                          },
    {   0x1002,     "MTP_OPCODE_OPENSESSION"                            },
    {   0x1003,     "MTP_OPCODE_CLOSESESSION"                           },
    {   0x1004,     "MTP_OPCODE_GETSTORAGEIDS"                          },
    {   0x1005,     "MTP_OPCODE_GETSTORAGEINFO"                         },
    {   0x1006,     "MTP_OPCODE_GETNUMOBJECTS"                          },
    {   0x1007,     "MTP_OPCODE_GETOBJECTHANDLES"                       },
    {   0x1008,     "MTP_OPCODE_GETOBJECTINFO"                          },
    {   0x1009,     "MTP_OPCODE_GETOBJECT"                              },
    {   0x100A,     "MTP_OPCODE_GETTHUMB"                               },
    {   0x100B,     "MTP_OPCODE_DELETEOBJECT"                           },
    {   0x100C,     "MTP_OPCODE_SENDOBJECTINFO"                         },
    {   0x100D,     "MTP_OPCODE_SENDOBJECT"                             },
    {   0x100E,     "MTP_OPCODE_INITIATECAPTURE"                        },
    {   0x100F,     "MTP_OPCODE_FORMATSTORE"                            },
    {   0x1010,     "MTP_OPCODE_RESETDEVICE"                            },
    {   0x1011,     "MTP_OPCODE_SELFTEST"                               },
    {   0x1012,     "MTP_OPCODE_SETOBJECTPROTECTION"                    },
    {   0x1013,     "MTP_OPCODE_POWERDOWN"                              },
    {   0x1014,     "MTP_OPCODE_GETDEVICEPROPDESC"                      },
    {   0x1015,     "MTP_OPCODE_GETDEVICEPROPVALUE"                     },
    {   0x1016,     "MTP_OPCODE_SETDEVICEPROPVALUE"                     },
    {   0x1017,     "MTP_OPCODE_RESETDEVICEPROPVALUE"                   },
    {   0x1018,     "MTP_OPCODE_TERMINATECAPTURE"                       },
    {   0x1019,     "MTP_OPCODE_MOVEOBJECT"                             },
    {   0x101A,     "MTP_OPCODE_COPYOBJECT"                             },
    {   0x101B,     "MTP_OPCODE_GETPARTIALOBJECT"                       },
    {   0x101C,     "MTP_OPCODE_INITIATEOPENCAPTURE"                    },
    {   0x9101,     "JAN_OPCODE_GETSECURETIMECHALLENGE"                 },
    {   0x9102,     "JAN_OPCODE_SETSECURETIMERESPONSE"                  },
    {   0x9103,     "JAN_OPCODE_SETLICENSERESPONSE"                     },
    {   0x9104,     "JAN_OPCODE_GETSYNCLIST"                            },
    {   0x9105,     "JAN_OPCODE_SENDMETERCHALLENGEQUERY"                },
    {   0x9106,     "JAN_OPCODE_GETMETERCHALLENGE"                      },
    {   0x9107,     "JAN_OPCODE_SETMETERRESPONSE"                       },
    {   0x9108,     "JAN_OPCODE_CLEANDATASTORE"                         },
    {   0x9109,     "JAN_OPCODE_GETLICENSESTATE"                        },
    {   0x910A,     "JAN_OPCODE_SENDJANUSCOMMAND"                       },
    {   0x910B,     "JAN_OPCODE_SENDJANUSREQUEST"                       },
    {   0x9201,     "WMP_OPCODE_GETMODIFIEDPUIDS"                       },
    {   0x9202,     "WMP_OPCODE_GETACQUIREDCONTENT"                     },
    {   0x9301,     "MTP_OPCODE_GETSERVICEIDS"                          },
    {   0x9302,     "MTP_OPCODE_GETSERVICEINFO"                         },
    {   0x9303,     "MTP_OPCODE_GETSERVICECAPABILITIES"                 },
    {   0x9304,     "MTP_OPCODE_GETSERVICEPROPDESC"                     },
    {   0x9305,     "MTP_OPCODE_GETSERVICEPROPLIST"                     },
    {   0x9306,     "MTP_OPCODE_SETSERVICEPROPLIST"                     },
    {   0x9307,     "MTP_OPCODE_UPDATEOBJECTPROPLIST"                   },
    {   0x9308,     "MTP_OPCODE_DELETEOBJECTPROPLIST"                   },
    {   0x9309,     "MTP_OPCODE_DELETESERVICEPROPLIST"                  },
    {   0x9801,     "MTP_OPCODE_GETOBJECTPROPSSUPPORTED"                },
    {   0x9802,     "MTP_OPCODE_GETOBJECTPROPDESC"                      },
    {   0x9803,     "MTP_OPCODE_GETOBJECTPROPVALUE"                     },
    {   0x9804,     "MTP_OPCODE_SETOBJECTPROPVALUE"                     },
    {   0x9805,     "MTP_OPCODE_GETOBJECTPROPLIST"                      },
    {   0x9806,     "MTP_OPCODE_SETOBJECTPROPLIST"                      },
    {   0x9807,     "MTP_OPCODE_GETINTERDEPENDENTPROPDESC"              },
    {   0x9808,     "MTP_OPCODE_SENDOBJECTPROPLIST"                     },
    {   0x9810,     "MTP_OPCODE_GETOBJECTREFERENCES"                    },
    {   0x9811,     "MTP_OPCODE_SETOBJECTREFERENCES"                    },
    {   0x9812,     "MTP_OPCODE_UPDATEDEVICEFIRMWARE"                   },
    {   0x9813,     "MTP_OPCODE_RESETOBJECTPROPVALUE"                   },
};

static PSL_CONST STRINGTABLE    _RgstblResponseCode[] =
{
    {   0x2000,     "MTP_RESPONSECODE_UNDEFINED"                        },
    {   0x2001,     "MTP_RESPONSECODE_OK"                               },
    {   0x2002,     "MTP_RESPONSECODE_GENERALERROR"                     },
    {   0x2003,     "MTP_RESPONSECODE_SESSIONNOTOPEN"                   },
    {   0x2004,     "MTP_RESPONSECODE_INVALIDTRANSACTIONID"             },
    {   0x2005,     "MTP_RESPONSECODE_OPERATIONNOTSUPPORTED"            },
    {   0x2006,     "MTP_RESPONSECODE_PARAMETERNOTSUPPORTED"            },
    {   0x2007,     "MTP_RESPONSECODE_INCOMPLETETRANSFER"               },
    {   0x2008,     "MTP_RESPONSECODE_INVALIDSTORAGEID"                 },
    {   0x2009,     "MTP_RESPONSECODE_INVALIDOBJECTHANDLE"              },
    {   0x200A,     "MTP_RESPONSECODE_INVALIDPROPERTYCODE"              },
    {   0x200B,     "MTP_RESPONSECODE_INVALIDOBJECTFORMATCODE"          },
    {   0x200C,     "MTP_RESPONSECODE_STOREFULL"                        },
    {   0x200D,     "MTP_RESPONSECODE_OBJECTWRITEPROTECTED"             },
    {   0x200E,     "MTP_RESPONSECODE_STOREWRITEPROTECTED"              },
    {   0x200F,     "MTP_RESPONSECODE_ACCESSDENIED"                     },
    {   0x2010,     "MTP_RESPONSECODE_NOTHUMBNAILPRESENT"               },
    {   0x2011,     "MTP_RESPONSECODE_SELFTESTFAILED"                   },
    {   0x2012,     "MTP_RESPONSECODE_PARTIALDELETION"                  },
    {   0x2013,     "MTP_RESPONSECODE_STORENOTAVAILABLE"                },
    {   0x2014,     "MTP_RESPONSECODE_NOSPECIFICATIONBYFORMAT"          },
    {   0x2015,     "MTP_RESPONSECODE_NOVALIDOBJECTINFO"                },
    {   0x2016,     "MTP_RESPONSECODE_INVALIDCODEFORMAT"                },
    {   0x2017,     "MTP_RESPONSECODE_UNKNOWNVENDORCODE"                },
    {   0x2018,     "MTP_RESPONSECODE_CAPTUREALREADYTERMINATED"         },
    {   0x2019,     "MTP_RESPONSECODE_DEVICEBUSY"                       },
    {   0x201A,     "MTP_RESPONSECODE_INVALIDPARENT"                    },
    {   0x201B,     "MTP_RESPONSECODE_INVALIDPROPFORMAT"                },
    {   0x201C,     "MTP_RESPONSECODE_INVALIDPROPVALUE"                 },
    {   0x201D,     "MTP_RESPONSECODE_INVALIDPARAMETER"                 },
    {   0x201E,     "MTP_RESPONSECODE_SESSIONALREADYOPENED"             },
    {   0x201F,     "MTP_RESPONSECODE_TRANSACTIONCANCELLED"             },
    {   0x2020,     "MTP_RESPONSECODE_SPECIFICATIONOFDESTINATIONUNSUPPORTED"    },
    {   0xA101,     "JAN_RESPONSECODE_JANUSFAIL"                        },
    {   0xA801,     "MTP_RESPONSECODE_INVALIDOBJECTPROPCODE"            },
    {   0xA802,     "MTP_RESPONSECODE_INVALIDOBJECTPROPFORMAT"          },
    {   0xA803,     "MTP_RESPONSECODE_INVALIDOBJECTPROPVALUE"           },
    {   0xA804,     "MTP_RESPONSECODE_INVALIDOBJECTREFERENCE"           },
    {   0xA805,     "MTP_RESPONSECODE_INVALIDOBJECTGROUPCODE"           },
    {   0xA806,     "MTP_RESPONSECODE_INVALIDDATASET"                   },
    {   0xA807,     "MTP_RESPONSECODE_SPECIFICATIONBYGROUPUNSUPPORTED"  },
    {   0xA808,     "MTP_RESPONSECODE_SPECIFICATIONBYDEPTHUNSUPPORTED"  },
    {   0xA809,     "MTP_RESPONSECODE_OBJECTTOOLARGE"                   },
};


static PSL_CONST STRINGTABLE    _RgstblEventCode[] =
{
    {   0x4000,     "MTP_EVENTCODE_UNDEFINED"                           },
    {   0x4001,     "MTP_EVENTCODE_CANCELTRANSACTION"                   },
    {   0x4002,     "MTP_EVENTCODE_OBJECTADDED"                         },
    {   0x4003,     "MTP_EVENTCODE_OBJECTREMOVED"                       },
    {   0x4004,     "MTP_EVENTCODE_STOREADDED"                          },
    {   0x4005,     "MTP_EVENTCODE_STOREREMOVED"                        },
    {   0x4006,     "MTP_EVENTCODE_DEVICEPROPCHANGED"                   },
    {   0x4007,     "MTP_EVENTCODE_OBJECTINFOCHANGED"                   },
    {   0x4008,     "MTP_EVENTCODE_DEVICEINFOCHANGED"                   },
    {   0x4009,     "MTP_EVENTCODE_REQUESTOBJECTTRANSFER"               },
    {   0x400A,     "MTP_EVENTCODE_STOREFULL"                           },
    {   0x400B,     "MTP_EVENTCODE_DEVICERESET"                         },
    {   0x400C,     "MTP_EVENTCODE_STORAGEINFOCHANGED"                  },
    {   0x400D,     "MTP_EVENTCODE_CAPTURECOMPLETE"                     },
    {   0x400E,     "MTP_EVENTCODE_UNREPORTEDSTATUS"                    },
    {   0xC001,     "MTP_EVENTCODE_VENDOREXTENTION1"                    },
    {   0xC002,     "MTP_EVENTCODE_VENDOREXTENTION2"                    },
    {   0xC801,     "MTP_EVENTCODE_OBJECTPROPCHANGED"                   },
    {   0xC802,     "MTP_EVENTCODE_OBJECTPROPDESCCHANGED"               },
    {   0xC803,     "MTP_EVENTCODE_OBJECTREFERENCESCHANGED"             },
};


static PSL_CONST STRINGTABLE    _RgstblPropCode[] =
{
    {   0x0000,     "MTP_OBJECTPROPCODE_NOTUSED"                        },
    {   0xD000,     "MTP_OBJECTPROPCODE_UNDEFINED"                      },
    {   0xDC01,     "MTP_OBJECTPROPCODE_STORAGEID"                      },
    {   0xDC02,     "MTP_OBJECTPROPCODE_OBJECTFORMAT"                   },
    {   0xDC03,     "MTP_OBJECTPROPCODE_PROTECTIONSTATUS"               },
    {   0xDC04,     "MTP_OBJECTPROPCODE_OBJECTSIZE"                     },
    {   0xDC05,     "MTP_OBJECTPROPCODE_ASSOCIATIONTYPE"                },
    {   0xDC06,     "MTP_OBJECTPROPCODE_ASSOCIATIONDESC"                },
    {   0xDC07,     "MTP_OBJECTPROPCODE_OBJECTFILENAME"                 },
    {   0xDC08,     "MTP_OBJECTPROPCODE_DATECREATED"                    },
    {   0xDC09,     "MTP_OBJECTPROPCODE_DATEMODIFIED"                   },
    {   0xDC0A,     "MTP_OBJECTPROPCODE_KEYWORDS"                       },
    {   0xDC0B,     "MTP_OBJECTPROPCODE_PARENT"                         },
    {   0xDC0C,     "MTP_OBJECTPROPCODE_ALLOWEDFOLDERCONTENTS"          },
    {   0xDC0D,     "MTP_OBJECTPROPCODE_HIDDEN"                         },
    {   0xDC0E,     "MTP_OBJECTPROPCODE_SYSTEMOBJECT"                   },
    {   0xDC41,     "MTP_OBJECTPROPCODE_PERSISTENTGUID"                 },
    {   0xDC42,     "MTP_OBJECTPROPCODE_SYNCID"                         },
    {   0xDC43,     "MTP_OBJECTPROPCODE_PROPERTYBAG"                    },
    {   0xDC44,     "MTP_OBJECTPROPCODE_NAME"                           },
    {   0xDC45,     "MTP_OBJECTPROPCODE_CREATEDBY"                      },
    {   0xDC46,     "MTP_OBJECTPROPCODE_ARTIST"                         },
    {   0xDC47,     "MTP_OBJECTPROPCODE_DATEAUTHORED"                   },
    {   0xDC48,     "MTP_OBJECTPROPCODE_DESCRIPTION"                    },
    {   0xDC49,     "MTP_OBJECTPROPCODE_URLREFERENCE"                   },
    {   0xDC4A,     "MTP_OBJECTPROPCODE_LANGUAGELOCALE"                 },
    {   0xDC4B,     "MTP_OBJECTPROPCODE_COPYRIGHTINFORMATION"           },
    {   0xDC4C,     "MTP_OBJECTPROPCODE_SOURCE"                         },
    {   0xDC4D,     "MTP_OBJECTPROPCODE_ORIGINLOCATION"                 },
    {   0xDC4E,     "MTP_OBJECTPROPCODE_DATEADDED"                      },
    {   0xDC4F,     "MTP_OBJECTPROPCODE_NONCONSUMABLE"                  },
    {   0xDC50,     "MTP_OBJECTPROPCODE_CORRUPTUNPLAYABLE"              },
    {   0xDC51,     "MTP_OBJECTPROPCODE_PRODUCERSERIALNUMBER"           },
    {   0xDC81,     "MTP_OBJECTPROPCODE_REPRESENTATIVESAMPLEFORMAT"     },
    {   0xDC82,     "MTP_OBJECTPROPCODE_REPRESENTATIVESAMPLESIZE"       },
    {   0xDC83,     "MTP_OBJECTPROPCODE_REPRESENTATIVESAMPLEHEIGHT"     },
    {   0xDC84,     "MTP_OBJECTPROPCODE_REPRESENTATIVESAMPLEWIDTH"      },
    {   0xDC85,     "MTP_OBJECTPROPCODE_REPRESENTATIVESAMPLEDURATION"   },
    {   0xDC86,     "MTP_OBJECTPROPCODE_REPRESENTATIVESAMPLEDATA"       },
    {   0xDC87,     "MTP_OBJECTPROPCODE_WIDTH"                          },
    {   0xDC88,     "MTP_OBJECTPROPCODE_HEIGHT"                         },
    {   0xDC89,     "MTP_OBJECTPROPCODE_DURATION"                       },
    {   0xDC8A,     "MTP_OBJECTPROPCODE_USERRATING"                     },
    {   0xDC8B,     "MTP_OBJECTPROPCODE_TRACK"                          },
    {   0xDC8C,     "MTP_OBJECTPROPCODE_GENRE"                          },
    {   0xDC8D,     "MTP_OBJECTPROPCODE_CREDITS"                        },
    {   0xDC8E,     "MTP_OBJECTPROPCODE_LYRICS"                         },
    {   0xDC8F,     "MTP_OBJECTPROPCODE_SUBSCRIPTIONCONTENTID"          },
    {   0xDC90,     "MTP_OBJECTPROPCODE_PRODUCEDBY"                     },
    {   0xDC91,     "MTP_OBJECTPROPCODE_USECOUNT"                       },
    {   0xDC92,     "MTP_OBJECTPROPCODE_SKIPCOUNT"                      },
    {   0xDC93,     "MTP_OBJECTPROPCODE_LASTACCESSED"                   },
    {   0xDC94,     "MTP_OBJECTPROPCODE_PARENTALRATING"                 },
    {   0xDC95,     "MTP_OBJECTPROPCODE_METAGENRE"                      },
    {   0xDC96,     "MTP_OBJECTPROPCODE_COMPOSER"                       },
    {   0xDC97,     "MTP_OBJECTPROPCODE_EFFECTIVERATING"                },
    {   0xDC98,     "MTP_OBJECTPROPCODE_SUBTITLE"                       },
    {   0xDC99,     "MTP_OBJECTPROPCODE_ORIGINALRELEASEDATE"            },
    {   0xDC9A,     "MTP_OBJECTPROPCODE_ALBUMNAME"                      },
    {   0xDC9B,     "MTP_OBJECTPROPCODE_ALBUMARTIST"                    },
    {   0xDC9C,     "MTP_OBJECTPROPCODE_MOOD"                           },
    {   0xDC9D,     "MTP_OBJECTPROPCODE_DRMPROTECTION"                  },
    {   0xDC9E,     "MTP_OBJECTPROPCODE_SUBDESCRIPTION"                 },
    {   0xDCD1,     "MTP_OBJECTPROPCODE_ISCROPPED"                      },
    {   0xDCD2,     "MTP_OBJECTPROPCODE_ISCOLOURCORRECTED"              },
    {   0xDCD3,     "MTP_OBJECTPROPCODE_IMAGEBITDEPTH"                  },
    {   0xDCD4,     "MTP_OBJECTPROPCODE_FNUMBER"                        },
    {   0xDCD5,     "MTP_OBJECTPROPCODE_EXPOSURETIME"                   },
    {   0xDCD6,     "MTP_OBJECTPROPCODE_EXPOSUREINDEX"                  },
    {   0xDCE0,     "MTP_OBJECTPROPCODE_DISPLAYNAME"                    },
    {   0xDCE1,     "MTP_OBJECTPROPCODE_BODYTEXT"                       },
    {   0xDCE2,     "MTP_OBJECTPROPCODE_SUBJECT"                        },
    {   0xDCE3,     "MTP_OBJECTPROPCODE_PRIORITY"                       },
    {   0xDD00,     "MTP_OBJECTPROPCODE_GIVENNAME"                      },
    {   0xDD01,     "MTP_OBJECTPROPCODE_MIDDLENAMES"                    },
    {   0xDD02,     "MTP_OBJECTPROPCODE_FAMILYNAME"                     },
    {   0xDD03,     "MTP_OBJECTPROPCODE_PREFIX"                         },
    {   0xDD04,     "MTP_OBJECTPROPCODE_SUFFIX"                         },
    {   0xDD05,     "MTP_OBJECTPROPCODE_PHONETICGIVENNAME"              },
    {   0xDD06,     "MTP_OBJECTPROPCODE_PHONETICFAMILYNAME"             },
    {   0xDD07,     "MTP_OBJECTPROPCODE_EMAILPRIMARY"                   },
    {   0xDD08,     "MTP_OBJECTPROPCODE_EMAILPERSONAL1"                 },
    {   0xDD09,     "MTP_OBJECTPROPCODE_EMAILPERSONAL2"                 },
    {   0xDD0A,     "MTP_OBJECTPROPCODE_EMAILBUSINESS1"                 },
    {   0xDD0B,     "MTP_OBJECTPROPCODE_EMAILBUSINESS2"                 },
    {   0xDD0C,     "MTP_OBJECTPROPCODE_EMAILOTHERS"                    },
    {   0xDD0D,     "MTP_OBJECTPROPCODE_PHONENUMBERPRIMARY"             },
    {   0xDD0E,     "MTP_OBJECTPROPCODE_PHONENUMBERPERSONAL"            },
    {   0xDD0F,     "MTP_OBJECTPROPCODE_PHONENUMBERPERSONAL2"           },
    {   0xDD10,     "MTP_OBJECTPROPCODE_PHONENUMBERBUSINESS"            },
    {   0xDD11,     "MTP_OBJECTPROPCODE_PHONENUMBERBUSINESS2"           },
    {   0xDD12,     "MTP_OBJECTPROPCODE_PHONENUMBERMOBIL"               },
    {   0xDD13,     "MTP_OBJECTPROPCODE_PHONENUMBERMOBIL2"              },
    {   0xDD14,     "MTP_OBJECTPROPCODE_FAXNUMBERPRIMARY"               },
    {   0xDD15,     "MTP_OBJECTPROPCODE_FAXNUMBERPERSONAL"              },
    {   0xDD16,     "MTP_OBJECTPROPCODE_FAXNUMBERBUSINESS"              },
    {   0xDD17,     "MTP_OBJECTPROPCODE_PAGERNUMBER"                    },
    {   0xDD18,     "MTP_OBJECTPROPCODE_PHONENUMBEROTHERS"              },
    {   0xDD19,     "MTP_OBJECTPROPCODE_PRIMARYWEBADDRESS"              },
    {   0xDD1A,     "MTP_OBJECTPROPCODE_PERSONALWEBADDRESS"             },
    {   0xDD1B,     "MTP_OBJECTPROPCODE_BUSINESSWEBADDRESS"             },
    {   0xDD1C,     "MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS"       },
    {   0xDD1D,     "MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS2"      },
    {   0xDD1E,     "MTP_OBJECTPROPCODE_INSTANTMESSENGERADDRESS3"      },
    {   0xDD1F,     "MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALFULL"      },
    {   0xDD20,     "MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALLINE1"     },
    {   0xDD21,     "MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALLINE2"     },
    {   0xDD22,     "MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALCITY"      },
    {   0xDD23,     "MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALREGION"    },
    {   0xDD24,     "MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALPOSTALCODE"},
    {   0xDD25,     "MTP_OBJECTPROPCODE_POSTALADDRESSPERSONALCOUNTRY"   },
    {   0xDD26,     "MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSFULL"      },
    {   0xDD27,     "MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSLINE1"     },
    {   0xDD28,     "MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSLINE2"     },
    {   0xDD29,     "MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSCITY"      },
    {   0xDD2A,     "MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSREGION"    },
    {   0xDD2B,     "MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSPOSTALCODE"},
    {   0xDD2C,     "MTP_OBJECTPROPCODE_POSTALADDRESSBUSINESSCOUNTRY"   },
    {   0xDD2D,     "MTP_OBJECTPROPCODE_POSTALADDRESSOTHERFULL"         },
    {   0xDD2E,     "MTP_OBJECTPROPCODE_POSTALADDRESSOTHERLINE1"        },
    {   0xDD2F,     "MTP_OBJECTPROPCODE_POSTALADDRESSOTHERLINE2"        },
    {   0xDD30,     "MTP_OBJECTPROPCODE_POSTALADDRESSOTHERCITY"         },
    {   0xDD31,     "MTP_OBJECTPROPCODE_POSTALADDRESSOTHERREGION"       },
    {   0xDD32,     "MTP_OBJECTPROPCODE_POSTALADDRESSOTHERPOSTALCODE"   },
    {   0xDD33,     "MTP_OBJECTPROPCODE_POSTALADDRESSOTHERCOUNTRY"      },
    {   0xDD34,     "MTP_OBJECTPROPCODE_ORGANIZATIONNAME"               },
    {   0xDD35,     "MTP_OBJECTPROPCODE_PHONETICORGANIZATIONNAME"       },
    {   0xDD36,     "MTP_OBJECTPROPCODE_ROLE"                           },
    {   0xDD37,     "MTP_OBJECTPROPCODE_BIRTHDAY"                       },
    {   0xDD40,     "MTP_OBJECTPROPCODE_MESSAGETO"                      },
    {   0xDD41,     "MTP_OBJECTPROPCODE_MESSAGECC"                      },
    {   0xDD42,     "MTP_OBJECTPROPCODE_MESSAGEBCC"                     },
    {   0xDD43,     "MTP_OBJECTPROPCODE_MESSAGEREAD"                    },
    {   0xDD44,     "MTP_OBJECTPROPCODE_MESSAGERECEIVETIME"             },
    {   0xDD45,     "MTP_OBJECTPROPCODE_MESSAGESENDER"                  },
    {   0xDD50,     "MTP_OBJECTPROPCODE_ACTIVITYBEGINTIME"              },
    {   0xDD51,     "MTP_OBJECTPROPCODE_ACTIVITYENDTIME"                },
    {   0xDD52,     "MTP_OBJECTPROPCODE_ACTIVITYLOCATION"               },
    {   0xDD54,     "MTP_OBJECTPROPCODE_ACTIVITYREQUIREDATTENDEES"      },
    {   0xDD55,     "MTP_OBJECTPROPCODE_ACTIVITYOPTIONALATTENDEES"      },
    {   0xDD56,     "MTP_OBJECTPROPCODE_ACTIVITYRESOURCES"              },
    {   0xDD57,     "MTP_OBJECTPROPCODE_ACTIVITYACCEPTEDED"             },
    {   0xDD58,     "MTP_OBJECTPROPCODE_ACTIVITYTENTATIVE"              },
    {   0xDD59,     "MTP_OBJECTPROPCODE_ACTIVITYDECLINED"               },
    {   0xDD5A,     "MTP_OBJECTPROPCODE_ACTIVITYREMINDERTIME"           },
    {   0xDD5B,     "MTP_OBJECTPROPCODE_ACTIVITYOWNER"                  },
    {   0xDD5C,     "MTP_OBJECTPROPCODE_ACTIVITYSTATUS"                 },
    {   0xDE91,     "MTP_OBJECTPROPCODE_TOTALBITRATE"                   },
    {   0xDE92,     "MTP_OBJECTPROPCODE_BITRATETYPE"                    },
    {   0xDE93,     "MTP_OBJECTPROPCODE_SAMPLERATE"                     },
    {   0xDE94,     "MTP_OBJECTPROPCODE_NUMBEROFCHANNELS"               },
    {   0xDE95,     "MTP_OBJECTPROPCODE_AUDIOBITDEPTH"                  },
    {   0xDE96,     "MTP_OBJECTPROPCODE_BLOCKALIGNMENT"                 },
    {   0xDE97,     "MTP_OBJECTPROPCODE_SCANTYPE"                       },
    {   0xDE98,     "MTP_OBJECTPROPCODE_COLOURRANGE"                    },
    {   0xDE99,     "MTP_OBJECTPROPCODE_AUDIOWAVECODEC"                 },
    {   0xDE9A,     "MTP_OBJECTPROPCODE_AUDIOBITRATE"                   },
    {   0xDE9B,     "MTP_OBJECTPROPCODE_VIDEOFOURCCCODEC"               },
    {   0xDE9C,     "MTP_OBJECTPROPCODE_VIDEOBITRATE"                   },
    {   0xDE9D,     "MTP_OBJECTPROPCODE_FRAMESPERMILLISECOND"           },
    {   0xDE9E,     "MTP_OBJECTPROPCODE_KEYFRAMEDISTANCE"               },
    {   0xDE9F,     "MTP_OBJECTPROPCODE_BUFFERSIZE"                     },
    {   0xDEA0,     "MTP_OBJECTPROPCODE_ENCODINGQUALITY"                },
    {   0xDEA1,     "MTP_OBJECTPROPCODE_ENCODINGPROFILE"                },
};

static PSL_CONST PSLCHAR    _RgchUnknownMTPOpCode[] =
        "MTP OPCODE UNKNOWN";

static PSL_CONST PSLCHAR    _RgchUnknownVendorOpCode[] =
        "VENDOR OPCODE UNKNOWN";

static PSL_CONST PSLCHAR    _RgchInvalidOpCode[] =
        "INVALID OPCODE";

static PSL_CONST PSLCHAR    _RgchUnknownMTPResponseCode[] =
        "MTP RESPONSECODE UNKNOWN";

static PSL_CONST PSLCHAR    _RgchUnknownVendorResponseCode[] =
        "VENDOR RESPONSECODE UNKNOWN";

static PSL_CONST PSLCHAR    _RgchInvalidResponseCode[] =
        "INVALID RESPONSECODE";

static PSL_CONST PSLCHAR    _RgchUnknownMTPEventCode[] =
        "MTP EVENTCODE UNKNOWN";

static PSL_CONST PSLCHAR    _RgchUnknownVendorEventCode[] =
        "VENDOR EVENTCODE UNKNOWN";

static PSL_CONST PSLCHAR    _RgchInvalidEventCode[] =
        "INVALID EVENTCODE";

static PSL_CONST PSLCHAR    _RgchAllProperties[] =
        "ALL PROPERTIES";

static PSL_CONST PSLCHAR    _RgchUnknownVendorPropCode[] =
        "VENDER OBJECTPROPCODE UNKNOWN";

static PSL_CONST PSLCHAR    _RgchInvalidPropCode[] =
        "INVALID OBJECTPROPCODE";


/*
 *  _StringTableCompare
 *
 *  Callback function for PSLBSearch to compare entries in the string
 *  table
 *
 *  Arguments:
 *      PSL_CONST PSLVOID*
 *                      pvKey               Item to compare with
 *      PSL_CONST PSLVOID*
 *                      pvItem              Item to compare
 *
 *  Returns:
 *      PSLINT32        < 0 -> pvItem < pvKey
 *                      == 0 -> pvItem == pvKey
 *                      > 0 -> pvItem > pvKey
 *
 */

PSLINT32 PSL_API _StringTableCompare(PSL_CONST PSLVOID* pvKey,
                            PSL_CONST PSLVOID* pvItem)
{
    PSL_CONST STRINGTABLE*  pstblItem;

    /*  We should have received an item- the key is "packed" so it may be
     *  PSLNULL
     */

    PSLASSERT(PSLNULL != pvItem);
    pstblItem = (STRINGTABLE*)pvItem;

    /*  Return the comparison
     */

    return ((PSLINT32)pvKey - (PSLINT32)pstblItem->wCode);
}


/*
 *  _StringTableValidate
 *
 *  Quick helper function that makes sure the tables are sorted
 *
 *  Arguments:
 *      PSL_CONST STRINGTABLE*
 *                          pstbl           Table to validate
 *      PSLUINT32           cItems          Number of items in table
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID _StringTableValidate(PSL_CONST STRINGTABLE* pstbl, PSLUINT32 cItems)
{
    PSLUINT32       idxItem;

    /*  Walk through and validate that the items are sorted
     */

    for (idxItem = 1;
            (idxItem < cItems) &&
                    (pstbl[idxItem - 1].wCode < pstbl[idxItem].wCode);
            idxItem++)
    {
    }

    /*  If we did not make it all the way through the list then we have
     *  an unsorted table
     */

    if (idxItem < cItems)
    {
        PSLDebugPrintf("STRINGTABLE fails sort validation");
        PSLASSERT(PSLFALSE);
    }
    return;
}


/*
 *  MTPOpCodeToString
 *
 *  Converts a numeric MTP OpCode to a string
 *
 *  Arguments:
 *      PSLUINT16       wOpCode             OpCode to translate
 *
 *  Returns:
 *      PSLCSTR     Static string representation of OpCode
 */

PSLCSTR PSL_API MTPOpCodeToString(PSLUINT16 wOpCode)
{
    PSLSTATUS               ps;
    PSL_CONST STRINGTABLE*  pstblMatch;
    PSLCSTR                 szResult;
#ifdef PSL_ASSERTS
    static PSLBOOL          fValidate = PSLTRUE;

    /*  See if we need to validate the table first
     */

    if (fValidate)
    {
        _StringTableValidate(_RgstblOpCode, PSLARRAYSIZE(_RgstblOpCode));
        fValidate = PSLFALSE;
    }
#endif  /*  PSL_ASSERTS */

    /*  Perform a BSearch to locate the op-code
     */

    ps = PSLBSearch((PSLVOID*)(PSLUINT32)wOpCode, _RgstblOpCode,
                            PSLARRAYSIZE(_RgstblOpCode),
                            sizeof(_RgstblOpCode[0]),
                            _StringTableCompare,
                            (PSLVOID*)&pstblMatch);
    if (PSLSUCCESS == ps)
    {
        /*  Return the name of the matching op-code
         */

        szResult = pstblMatch->szName;
    }
    else
    {
        /*  Look for some common ranges to provide a little more detail
         */

        if ((MTP_OPCODE_RESERVED_FIRST <= wOpCode) &&
            (wOpCode <= MTP_OPCODE_RESERVED_LAST))
        {
            szResult = _RgchUnknownMTPOpCode;
        }
        else if ((MTP_OPCODE_VENDOREXTENSION_FIRST <= wOpCode) &&
            (wOpCode <= MTP_OPCODE_VENDOREXTENSION_LAST))
        {
            szResult = _RgchUnknownVendorOpCode;
        }
        else
        {
            szResult = _RgchInvalidOpCode;
        }
    }
    return szResult;
}


/*
 *  MTPResponseCodeToString
 *
 *  Converts a numeric MTP response code to a string
 *
 *  Arguments:
 *      PSLUINT16       wResponseCode       wResponseCode to translate
 *
 *  Returns:
 *      PSLCSTR     Static string representation of OpCode
 *
 */

PSLCSTR PSL_API MTPResponseCodeToString(PSLUINT16 wResponseCode)
{
    PSLSTATUS               ps;
    PSL_CONST STRINGTABLE*  pstblMatch;
    PSLCSTR                 szResult;
#ifdef PSL_ASSERTS
    static PSLBOOL          fValidate = PSLTRUE;

    /*  See if we need to validate the table first
     */

    if (fValidate)
    {
        _StringTableValidate(_RgstblResponseCode,
                            PSLARRAYSIZE(_RgstblResponseCode));
        fValidate = PSLFALSE;
    }
#endif  /*  PSL_ASSERTS */

    /*  Perform a BSearch to locate the response code
     */

    ps = PSLBSearch((PSLVOID*)(PSLUINT32)wResponseCode, _RgstblResponseCode,
                            PSLARRAYSIZE(_RgstblResponseCode),
                            sizeof(_RgstblResponseCode[0]),
                            _StringTableCompare,
                            (PSLVOID*)&pstblMatch);
    if (PSLSUCCESS == ps)
    {
        /*  Return the name of the matching response code
         */

        szResult = pstblMatch->szName;
    }
    else
    {
        /*  Look for some common ranges to provide a little more detail
         */

        if ((MTP_RESPONSECODE_RESERVED_FIRST <= wResponseCode) &&
            (wResponseCode <= MTP_RESPONSECODE_RESERVED_LAST))
        {
            szResult = _RgchUnknownMTPResponseCode;
        }
        else if ((MTP_RESPONSECODE_VENDOREXTENSION_FIRST <= wResponseCode) &&
            (wResponseCode <= MTP_RESPONSECODE_VENDOREXTENSION_LAST))
        {
            szResult = _RgchUnknownVendorResponseCode;
        }
        else
        {
            szResult = _RgchInvalidResponseCode;
        }
    }
    return szResult;
}


/*
 *  MTPEventCodeToString
 *
 *  Converts a numeric MTP event code to a string
 *
 *  Arguments:
 *      PSLUINT16       wEventCode          Event code to translate
 *
 *  Returns:
 *      PSLCSTR     Static string representation of OpCode
 *
 */

PSLCSTR PSL_API MTPEventCodeToString(PSLUINT16 wEventCode)
{
    PSLSTATUS               ps;
    PSL_CONST STRINGTABLE*  pstblMatch;
    PSLCSTR                 szResult;
#ifdef PSL_ASSERTS
    static PSLBOOL          fValidate = PSLTRUE;

    /*  See if we need to validate the table first
     */

    if (fValidate)
    {
        _StringTableValidate(_RgstblEventCode, PSLARRAYSIZE(_RgstblEventCode));
        fValidate = PSLFALSE;
    }
#endif  /*  PSL_ASSERTS */

    /*  Perform a BSearch to locate the event code
     */

    ps = PSLBSearch((PSLVOID*)(PSLUINT32)wEventCode, _RgstblEventCode,
                            PSLARRAYSIZE(_RgstblEventCode),
                            sizeof(_RgstblEventCode[0]),
                            _StringTableCompare,
                            (PSLVOID*)&pstblMatch);
    if (PSLSUCCESS == ps)
    {
        /*  Return the name of the matching event code
         */

        szResult = pstblMatch->szName;
    }
    else
    {
        /*  Look for some common ranges to provide a little more detail
         */

        if ((MTP_EVENTCODE_RESERVED_FIRST <= wEventCode) &&
            (wEventCode <= MTP_EVENTCODE_RESERVED_LAST))
        {
            szResult = _RgchUnknownMTPEventCode;
        }
        else if ((MTP_EVENTCODE_VENDOREXTENSION_FIRST <= wEventCode) &&
            (wEventCode <= MTP_EVENTCODE_VENDOREXTENSION_LAST))
        {
            szResult = _RgchUnknownVendorEventCode;
        }
        else
        {
            szResult = _RgchInvalidEventCode;
        }
    }
    return szResult;
}


/*
 *  MTPObjectPropToString
 *
 *  Converts a numeric MTP object property code to a string
 *
 *  Arguments:
 *      PSLUINT16       wPropCode           wPropCode to translate
 *
 *  Returns:
 *      PSLCSTR     Static string representation of OpCode
 *
 */

PSLCSTR PSL_API MTPObjectPropToString(PSLUINT16 wPropCode)
{
    PSLSTATUS               ps;
    PSL_CONST STRINGTABLE*  pstblMatch;
    PSLCSTR                 szResult;
#ifdef PSL_ASSERTS
    static PSLBOOL          fValidate = PSLTRUE;

    /*  See if we need to validate the table first
     */

    if (fValidate)
    {
        _StringTableValidate(_RgstblPropCode, PSLARRAYSIZE(_RgstblPropCode));
        fValidate = PSLFALSE;
    }
#endif  /*  PSL_ASSERTS */

    /*  Perform a BSearch to locate the prop code
     */

    ps = PSLBSearch((PSLVOID*)(PSLUINT32)wPropCode, _RgstblPropCode,
                            PSLARRAYSIZE(_RgstblPropCode),
                            sizeof(_RgstblPropCode[0]),
                            _StringTableCompare,
                            (PSLVOID*)&pstblMatch);
    if (PSLSUCCESS == ps)
    {
        /*  Return the name of the matching prop code
         */

        szResult = pstblMatch->szName;
    }
    else
    {
        /*  Look for some common ranges to provide a little more detail
         */

        if (MTP_OBJECTPROPCODE_ALL == wPropCode)
        {
            szResult = _RgchAllProperties;
        }
        else if ((MTP_OBJECTPROPCODE_VENDOREXTENSION_FIRST <= wPropCode) &&
            (wPropCode <= MTP_OBJECTPROPCODE_VENDOREXTENSION_LAST))
        {
            szResult = _RgchUnknownVendorPropCode;
        }
        else
        {
            szResult = _RgchInvalidPropCode;
        }
    }
    return szResult;
}


