/*
 *  MTPHandlerPropUtil.c
 *
 *  Contains definition for the utility fucntions that support
 *  device level handler functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerUtil.h"
#include "MTPHandlerPropUtil.h"

/*
 *  Utils used by MTPHandlePropOperations
 */
PSLSTATUS _MTPPropHandleRequest(
                MTPDISPATCHER md, PSLMSG* pMsg,
                PSLUINT32 dwHandlerState,
                PFNPROPCONTEXTPREPARE pfnPropContextPrepare)
{
    PSLSTATUS           status;
    PSLUINT16           wRespCode = MTP_RESPONSECODE_OK;

    MTPCONTEXT*         pmc     = PSLNULL;
    PROPHANDLERDATA*    pData   = PSLNULL;
    PSLMSG*             pRetMsg = PSLNULL;

    PXMTPOPERATION      pOpRequest = PSLNULL;
    PSLUINT64           qwBufSize;

    PSLUINT32           dwMsgFlags;

    if (PSLNULL == md || PSLNULL == pMsg ||
        PSLNULL == pfnPropContextPrepare)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPHandlerBegin(md, pMsg, &pmc, (BASEHANDLERDATA**)&pData,
                            &pRetMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* the data should already be cleared */

    PSLASSERT(PSLNULL == pData->mpc);

    /* init response message */
    pData->bhd.opLevelData.msgResponse = MTPMSG_RESPONSE;

    pOpRequest = (PXMTPOPERATION)pMsg->aParam2;
    PSLASSERT(PSLNULL != pOpRequest);

    /* init response */
    (PSLVOID)FillResponse(
                &pData->bhd.opLevelData.mtpResponse,
                wRespCode, MTPLoadUInt32(&(pOpRequest->dwTransactionID)),
                PSLNULL, PSLNULL, PSLNULL, PSLNULL, PSLNULL);

    /*
     * call for prepare function, some of which will save context in
     * pData while some others will fill response directly.
     */
    status = pfnPropContextPrepare(pOpRequest, pMsg->alParam,
                            (BASEHANDLERDATA*)pData, &qwBufSize);

Exit:
    if (PSLNULL != pRetMsg)
    {
       (PSLVOID)MTPHandlerGetResponseCodeFromStatus(status, &wRespCode);

        /* reset reponse on failure. */
        if ((PSLNULL != pData) && (PSLNULL != pOpRequest) &&
            (MTP_RESPONSECODE_OK != wRespCode))
        {
            (PSLVOID)FillResponse(
                &pData->bhd.opLevelData.mtpResponse,
                wRespCode, MTPLoadUInt32(&(pOpRequest->dwTransactionID)),
                PSLNULL, PSLNULL, PSLNULL, PSLNULL, PSLNULL);

            pData->bhd.opLevelData.dwNumRespParams = 0;
        }

        /* prepare for next message. */
        (PSLVOID)MTPHandlerSetState(pmc, wRespCode, dwHandlerState,
                                     &dwMsgFlags, &qwBufSize);

        /* notify transport */
        if ((PSLNULL != pmc) && (PSLNULL != pMsg))
        {
            status = MTPHandlerRouteMsgToTransport(pmc, pRetMsg, pMsg,
                                       MTPMSG_CMD_CONSUMED,
                                       (PSLBYTE*)pMsg->aParam2,
                                       qwBufSize, dwMsgFlags);
            if (PSL_SUCCEEDED(status))
            {
                pRetMsg = PSLNULL;
            }
        }
    }
    else
    {
        /* we did not alloc return msg, no much we can do. */
        if (PSLNULL != pData)
        {
            SAFE_MTPDEVICEPROPCONTEXTCLOSE(pData->mpc);
        }
        (PSLVOID)GetReadyForNextCommand(md, pmc);
    }

    MTPHandlerEnd(pRetMsg);
    return status;
}

PSLSTATUS _MTPPropHandleGetData(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS           status;
    PSLUINT16           wRespCode = MTP_RESPONSECODE_OK;

    MTPCONTEXT*         pmc     = PSLNULL;
    BASEHANDLERDATA*    pData   = PSLNULL;
    PSLMSG*             pRetMsg = PSLNULL;

    PSLUINT32           dwBufSize     = 0;
    PSLUINT32           dwDataWritten = 0;
    PSLUINT64           dwDataLeft    = 0;
    PSLUINT32           dwMsgFlags    = 0;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPHandlerBegin(md, pMsg, &pmc, &pData, &pRetMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(MTPHANDLERSTATE_DATA_OUT ==
                        pmc->mtpHandlerState.dwHandlerState);
    PSLASSERT(MTP_RESPONSECODE_OK ==
                    pData->opLevelData.mtpResponse.wRespCode);
    PSLASSERT(PSLNULL != (PSLVOID*)pMsg->aParam2);
    PSLASSERT(0 != (PSLUINT32)pMsg->alParam);

    /* Serialize the device property */
    status = MTPDevicePropContextSerialize(
                        ((PROPHANDLERDATA*)pData)->mpc,
                        (PSLBYTE*)pMsg->aParam2,
                        (PSLUINT32)pMsg->alParam,
                        &dwDataWritten, &dwDataLeft);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* amount of data filled in the buffer */
    PSLASSERT(0 != dwDataWritten);
    dwBufSize = dwDataWritten;

Exit:
    if (PSLNULL != pRetMsg)
    {
        (PSLVOID)MTPHandlerGetResponseCodeFromStatus(status,
                                                        &wRespCode);

        /* save the new resp code if we were OK before.*/
        if ((PSLNULL != pData) && (MTP_RESPONSECODE_OK ==
            pData->opLevelData.mtpResponse.wRespCode))
        {
            pData->opLevelData.mtpResponse.wRespCode = wRespCode;
        }

        if ((PSLNULL != pmc) && (PSLNULL != pData) && (PSLNULL != pMsg))
        {
            if (MTP_RESPONSECODE_OK !=
                pData->opLevelData.mtpResponse.wRespCode)
            {
                /* if we failed somehow, switch to response phase. */
                pmc->mtpHandlerState.dwHandlerState =
                                        MTPHANDLERSTATE_RESPONSE;

                /* Note: dwBufSize being 0 indicates we failed. */
                PSLASSERT(0 == dwBufSize);
                dwMsgFlags = MTPMSGFLAG_RESPONSE_BUFFER_REQUEST |
                                MTPMSGFLAG_TERMINATE;

            }
            else if (0 == dwDataLeft)
            {
                /* if the data remaining is zero or if the buffer
                 * size from transport is big enough to hold the
                 * entire data we are done with data phase and
                 * ready to send response
                 */
                PSLASSERT(dwDataWritten <= (PSLUINT32)pMsg->alParam);

                pmc->mtpHandlerState.dwHandlerState =
                                        MTPHANDLERSTATE_RESPONSE;
                dwMsgFlags = MTPMSGFLAG_RESPONSE_BUFFER_REQUEST|
                               MTPMSGFLAG_TERMINATE;
            }
            else
            {
                /* request addtional data buffer, the size
                 * of the buffer would still be the same as
                 * the size of initial buffer requested.
                 */
                PSLASSERT(0 != dwDataLeft);
                PSLASSERT(MTPHANDLERSTATE_DATA_OUT ==
                                pmc->mtpHandlerState.dwHandlerState);

                dwMsgFlags  = MTPMSGFLAG_DATA_BUFFER_REQUEST;
            }

            status = MTPHandlerRouteMsgToTransport(pmc, pRetMsg, pMsg,
                                    MTPMSG_DATA_READY_TO_SEND,
                                    (PSLBYTE*)pMsg->aParam2,
                                    dwBufSize, dwMsgFlags);
            if (PSL_SUCCEEDED(status))
            {
                pRetMsg = PSLNULL;
            }
        }
        else
        {
            /* we cannot construct return msg, no much we can do. */
            SAFE_MTPDEVICEPROPCONTEXTCLOSE(
                                ((PROPHANDLERDATA*)pData)->mpc);
            (PSLVOID)GetReadyForNextCommand(md, pmc);
        }
    }
    else
    {
        /* we did not alloc return msg, no much we can do. */
        SAFE_MTPDEVICEPROPCONTEXTCLOSE(((PROPHANDLERDATA*)pData)->mpc);
        (PSLVOID)GetReadyForNextCommand(md, pmc);
    }

    MTPHandlerEnd(pRetMsg);
    return status;
}

PSLSTATUS _MTPPropHandleResponse(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS           status;

    MTPCONTEXT*         pmc     = PSLNULL;
    BASEHANDLERDATA*    pData   = PSLNULL;
    PSLMSG*             pRetMsg = PSLNULL;

    PSLUINT32           dwBufSize  = 0;
    PSLUINT32           dwMsgFlags = 0;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPHandlerBegin(md, pMsg, &pmc, &pData, &pRetMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(MTPHANDLERSTATE_RESPONSE ==
                        pmc->mtpHandlerState.dwHandlerState);

    PSLASSERT((PSLUINT32)pMsg->alParam >=
                                    sizeof(MTPRESPONSE));

    if (PSLNULL != (PSLVOID*)pMsg->aParam2)
    {
        PSLCopyMemory((PSLVOID*)pMsg->aParam2,
                        &pData->opLevelData.mtpResponse,
                        (PSLUINT32)pMsg->alParam);

        /* amount of data filled in response buffer */
        dwBufSize = sizeof(MTPRESPONSE) -
                            ((MTPRESPONSE_NUMPARAMS_MAX - \
                            pData->opLevelData.dwNumRespParams) * \
                            sizeof(PSLUINT32));
    }

Exit:

    if (PSLNULL != pData)
    {
        SAFE_MTPDEVICEPROPCONTEXTCLOSE(((PROPHANDLERDATA*)pData)->mpc);
    }
    (PSLVOID)GetReadyForNextCommand(md, pmc);

    if ((PSLNULL != pRetMsg) && (PSLNULL != pmc) &&
        (PSLNULL != pData) && (PSLNULL != pMsg))
    {
        status = MTPHandlerRouteMsgToTransport(pmc, pRetMsg, pMsg,
                                   pData->opLevelData.msgResponse,
                                   (PSLBYTE*)pMsg->aParam2,
                                   dwBufSize, dwMsgFlags);
        if (PSL_SUCCEEDED(status))
        {
            pRetMsg = PSLNULL;
        }
    }

    MTPHandlerEnd(pRetMsg);
    return status;
}

PSLSTATUS _MTPPropHandleSetData(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS           status            = PSLSUCCESS;
    PSLUINT16           wRespCode;

    MTPCONTEXT*         pmc     = PSLNULL;
    BASEHANDLERDATA*    pData   = PSLNULL;
    PSLMSG*             pRetMsg = PSLNULL;
    PSLUINT32           dwFlags = 0;

    PSLMSGQUEUE         mqHandler;
    PSLBOOL             bDeserialized = PSLFALSE;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPHandlerBegin(md, pMsg, &pmc, &pData, &pRetMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(MTPHANDLERSTATE_DATA_IN ==
                pmc->mtpHandlerState.dwHandlerState);

    if ( MTP_RESPONSECODE_OK !=
                pData->opLevelData.mtpResponse.wRespCode )
    {
        /* if we already failed in previous message for this command,
         * we should just drop the data.
         */
        goto Exit;
    }

    /* make sure that we received valid data */

    if (((PSLNULL == (PSLVOID*)pMsg->aParam2) || (0 == pMsg->alParam)) &&
        !(pMsg->aParam3 & MTPMSGFLAG_TERMINATE))
    {
        status = PSLERROR_INVALID_DATA;
        goto Exit;
    }

    status = MTPDispatcherGetMsgQueue(md, &mqHandler);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if ((PSLNULL != (PSLVOID*)pMsg->aParam2) && (0 != pMsg->alParam))
    {
        status = MTPDevicePropContextDeserialize(
                                ((PROPHANDLERDATA*)pData)->mpc,
                                (PSLBYTE*)pMsg->aParam2,
                                (PSLUINT32)pMsg->alParam,
                                mqHandler, pmc, 0, pMsg->aParam3);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        bDeserialized = PSLTRUE;
    }
    else if (pMsg->aParam3 & MTPMSGFLAG_TERMINATE)
    {
        /* Commit the context because there will be no Data Consumed message */

        status = MTPDevicePropContextCommit(((PROPHANDLERDATA*)pData)->mpc);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

Exit:
    (PSLVOID)MTPHandlerGetResponseCodeFromStatus(status,&wRespCode);

    /* save the new resp code if we were OK before.*/
    if ((PSLNULL != pData) && (MTP_RESPONSECODE_OK ==
        pData->opLevelData.mtpResponse.wRespCode))
    {
        pData->opLevelData.mtpResponse.wRespCode = wRespCode;
    }

    /*
     * we need to send message if we did not get to deserialize, which
     * could be because error happened or an empty buffer. In which
     * case we need to drop buffer and send data consumed. otherwise,
     * we need to wait for data consumed message from deserializer.
     */
    if (PSLTRUE != bDeserialized)
    {
        if ((PSLNULL != pRetMsg) && (PSLNULL != pmc) &&
            (PSLNULL != pData) && (PSLNULL != pMsg))
        {
            /* last buffer, we move on to response phase.*/
            if (pMsg->aParam3 & MTPMSGFLAG_TERMINATE)
            {
                pmc->mtpHandlerState.dwHandlerState =
                                         MTPHANDLERSTATE_RESPONSE;

                dwFlags =  MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
            }

            /*
             * tell transport we are done with this data buffer,
             * if this is the last buffer, we request response
             * buffer.
             */
            status = MTPHandlerRouteMsgToTransport(
                        pmc, pRetMsg, pMsg,
                        MTPMSG_DATA_CONSUMED,
                        (PSLBYTE*)pMsg->aParam2,
                        (PSLLPARAM)pMsg->alParam,
                        dwFlags);
            if (PSL_SUCCEEDED(status))
            {
                pRetMsg = PSLNULL;
            }
        }
        else
        {
            /* no msg for response, just get ready for next command*/
            if ( PSLNULL != pData )
            {
                SAFE_MTPDEVICEPROPCONTEXTCLOSE(((PROPHANDLERDATA*)pData)->mpc);
            }
            (PSLVOID)GetReadyForNextCommand(md, pmc);
        }
    }


    MTPHandlerEnd(pRetMsg);
    return status;
}

PSLSTATUS _MTPPropHandleDataConsumed(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS           status;
    PSLUINT16           wRespCode;

    MTPCONTEXT*         pmc     = PSLNULL;
    BASEHANDLERDATA*    pData   = PSLNULL;
    PSLMSG*             pRetMsg = PSLNULL;
    PSLUINT32           dwFlags = 0;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPHandlerBegin(md, pMsg, &pmc, &pData, &pRetMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    if (pMsg->aParam3 & MTPMSGFLAG_TERMINATE)
    {
        /* We have consumed the last of the data- go ahead and commit any
         *  changes
         */

        status = MTPDevicePropContextCommit(((PROPHANDLERDATA*)pData)->mpc);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }
Exit:
    if ((PSLNULL != pRetMsg) && (PSLNULL != pmc) &&
        (PSLNULL != pData) && (PSLNULL != pMsg))
    {
        (PSLVOID)MTPHandlerGetResponseCodeFromStatus(status,&wRespCode);

        if ((PSLNULL != pData) && (MTP_RESPONSECODE_OK ==
            pData->opLevelData.mtpResponse.wRespCode))
        {
            /* if we are in good state before this, update resp code.*/
            if (PSL_FAILED(status))
            {
                pData->opLevelData.mtpResponse.wRespCode = wRespCode;
            }
        }

        /* if this is the last buffer, we move on to response phase.*/
        if (pMsg->aParam3 & MTPMSGFLAG_TERMINATE)
        {
            pmc->mtpHandlerState.dwHandlerState =
                                     MTPHANDLERSTATE_RESPONSE;

            dwFlags =  MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
        }

        /*
         * tell transport we are done with this data buffer, if this
         * is the last buffer, we request response buffer.
         */
        status = MTPHandlerRouteMsgToTransport(
                    pmc, pRetMsg, pMsg,
                    MTPMSG_DATA_CONSUMED,
                    (PSLBYTE*)pMsg->aParam2,
                    (PSLLPARAM)pMsg->alParam,
                    dwFlags);
        if (PSL_SUCCEEDED(status))
        {
            pRetMsg = PSLNULL;
        }
    }
    else
    {
        /* no msg for response, just get ready for next command*/
        if ( PSLNULL != pData )
        {
            SAFE_MTPDEVICEPROPCONTEXTCLOSE(((PROPHANDLERDATA*)pData)->mpc);
        }
        (PSLVOID)GetReadyForNextCommand(md, pmc);
    }

    MTPHandlerEnd(pRetMsg);
    return status;
}

/*
 *  MTPHandlePropOperations
 *
 *  This function is the frame called by all prop related
 *  operations.
 *
 */

PSLSTATUS MTPHandlePropOperations(
              MTPDISPATCHER md, PSLMSG* pMsg,
              PSLUINT32 dwHandlerState,
              PFNPROPCONTEXTPREPARE pfnPropContextPrepare)
{
    PSLSTATUS status = PSLSUCCESS;
    MTPCONTEXT* pmc;
    PROPHANDLERDATA* pData;

    if (PSLNULL == md || PSLNULL == pMsg ||
        PSLNULL == pfnPropContextPrepare)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    switch (pMsg->msgID)
    {
        case MTPMSG_CMD_AVAILABLE:
            status = _MTPPropHandleRequest(md, pMsg,
                            dwHandlerState, pfnPropContextPrepare);
            break;

        case MTPMSG_DATA_BUFFER_AVAILABLE:
            status = _MTPPropHandleGetData(md, pMsg);
            break;

        case MTPMSG_DATA_SENT:
            /* do nothing */
            status = PSLSUCCESS;
            break;

        case MTPMSG_DATA_AVAILABLE:
            status = _MTPPropHandleSetData(md, pMsg);
            break;

        case MTPMSG_DATA_CONSUMED:
            status = _MTPPropHandleDataConsumed(md, pMsg);
            break;

        case MTPMSG_RESPONSE_BUFFER_AVAILABLE:
            status = _MTPPropHandleResponse(md, pMsg);
            break;

        case MTPMSG_CANCEL:
        case MTPMSG_SHUTDOWN:
        case MTPMSG_DISCONNECT:
        case MTPMSG_RESET:
        case MTPMSG_ABORT:
            pmc = (MTPCONTEXT*)(pMsg->aParam0);
            if (PSLNULL != pmc)
            {
                pData = (PROPHANDLERDATA*) \
                                pmc->mtpHandlerState.pbHandlerData;
                if (PSLNULL != pData)
                {
                    MTPDevicePropContextCancel(pData->mpc);
                }
            }

            status = MTPDispatcherRouteTerminate(md,
                        (MTPCONTEXT*)(pMsg->aParam0), pMsg->msgID);
            break;

        default:
            status = PSLSUCCESS;
            break;
    }
Exit:
    return status;
}
