/*
 *  MTPShutdown.c
 *
 *  Contains the definition of the MTP Shutdown Service
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"

/*  Local Defines
 */

/*  Local Types
 */

/*  Local Functions
 */

/*  Local Variables
 */

static PSLUINT32            _CShutdownQueues = 0;
static PSLBOOL              _FShuttingDown = PSLFALSE;
static PSLHANDLE            _HSignalShutdown = PSLNULL;
static PSLHANDLE            _HMutexShutdown = PSLNULL;
static PSLNOTIFY            _NotifyShutdown = PSLNULL;
static const PSLWCHAR       _RgchShutdownMutex[] = L"MTP Shutdown Mutex";

/*
 *  MTPShutdownRegister
 *
 *  Called to register a message queue to receive the MTP shutdown message
 *
 *  NOTE: The shutdown service will halt until all registered clients have
 *  unregistered so if you call MTPShutdownRegister you must also call
 *  MTPShutdownUnregister
 *
 *  Arguments:
 *      PSLMSGQUEUE     mqNotify            Message queue to notify when
 *                                            shutdown occurs
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if added, PSLSUCCESS_EXISTS if already
 *                        present in list
 */

PSLSTATUS PSL_API MTPShutdownRegister(PSLMSGQUEUE mqNotify)
{
    PSLHANDLE           hMutex = PSLNULL;
    PSLHANDLE           hMutexRelease = PSLNULL;
    PSLHANDLE           hSignal = PSLNULL;
    PSLNOTIFY           notify = PSLNULL;
    PSLSTATUS           ps;

    /*  Validate arguments
     */

    if (PSLNULL == mqNotify)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create a named mutex to make sure that we do not get re-entered while
     *  handling initialization
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchShutdownMutex, &hMutex);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = hMutex;

    /*  If we are in the process of shutting down do not allow any additional
     *  registrations
     */

    if (_FShuttingDown)
    {
        ps = PSLERROR_SHUTTING_DOWN;
        goto Exit;
    }

    /*  Check to see if we need to initialize
     */

    if (PSLNULL == _NotifyShutdown)
    {
        /*  Create a notifier to use
         */

        ps = PSLNotifyCreate(&notify);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  And a signal to wait on when shutdown is executed
         */

        ps = PSLSignalOpen(PSLFALSE, PSLTRUE, PSLNULL, &hSignal);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  All is initialized go ahead and store the values
         */

        _HMutexShutdown = hMutex;
        hMutex = PSLNULL;
        _NotifyShutdown = notify;
        notify = PSLNULL;
        _HSignalShutdown = hSignal;
        hSignal = PSLNULL;
    }

    /*  Add this message to the shutdown queue- remember to deal with the
     *  fact that it may already be in the list
     */

    PSLASSERT(PSLNULL != _NotifyShutdown);
    ps = PSLNotifyRegister(_NotifyShutdown, mqNotify);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    /*  Reset the signal and increment the count of registered queues
     */

    PSLSignalReset(_HSignalShutdown);
    PSLAtomicIncrement(&_CShutdownQueues);

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMUTEXCLOSE(hMutex);
    SAFE_PSLSIGNALCLOSE(hSignal);
    SAFE_PSLNOTIFYDESTROY(notify);
    return ps;
}


/*
 *  MTPShutdownUnregister
 *
 *  Called to unregister a message queue from the MTP Shutdown service
 *
 *  Arguments:
 *      PSLMSGQUEUE     mqNotify            Message queue to remove
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if removed, PSLSUCCESS_NOT_FOUND if not
 *                        present
 */

PSLSTATUS PSL_API MTPShutdownUnregister(PSLMSGQUEUE mqNotify)
{
    PSLUINT32       cDest;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == mqNotify)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Check to see if we have any registered destinations- if not no need
     *  to do any work
     */

    cDest = PSLAtomicCompareExchange(&_CShutdownQueues, 0, 0);
    if (0 == cDest)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Try to remove the entry from the list- note that since we have entries
     *  in the list we must have a notifier to use
     */

    PSLASSERT(PSLNULL != _NotifyShutdown);
    ps = PSLNotifyUnregister(_NotifyShutdown, mqNotify);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    /*  Decrement the count of queues being tracked
     */

    cDest = PSLAtomicDecrement(&_CShutdownQueues);

    /*  If there are no queues left signal the object so that we wake up
     *  the shutdown process if it is running
     */

    if (0 == cDest)
    {
        ps = PSLSignalSet(_HSignalShutdown);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

Exit:
    return ps;
}


/*
 *  MTPShutdown
 *
 *  Called to initiate an MTP shutdown.  This function will block until all
 *  registered clients of the MTP Shutdown Service have unregistered.
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLTATUS        PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPShutdown()
{
    PSLUINT32           cDest;
    PSLHANDLE           hMutex = PSLNULL;
    PSLHANDLE           hMutexRelease = PSLNULL;
    PSLSTATUS           ps;

    /*  Acquire the named mutex that is being used to manage initialization
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchShutdownMutex, &hMutex);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = hMutex;

    /*  Set the shutting down flag so that other queues cannot get registered
     */

    _FShuttingDown = PSLTRUE;
    ps = PSLMutexRelease(hMutex);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = PSLNULL;

    /*  See if we need to notify anyone that we are shutting down
     */

    cDest = PSLAtomicCompareExchange(&_CShutdownQueues, 0, 0);
    if (0 < cDest)
    {
        /*  Broadcast the shutdown message to all registered queues- because
         *  we have queues we must have a notifier
         */

        PSLASSERT(PSLNULL != _NotifyShutdown);
        ps = PSLNotifyMsgPost(_NotifyShutdown, PSLNULL, MTPMSG_SHUTDOWN, 0, 0,
                            0, 0, 0);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Wait to be signaled that all of the entries have been removed
         */

        ps = PSLSignalWait(_HSignalShutdown, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Re-acquire the mutex so that we can close down the shutdown objects
     */

    ps = PSLMutexAcquire(hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = hMutex;

    /*  Release the objects
     */

    SAFE_PSLSIGNALCLOSE(_HSignalShutdown);
    SAFE_PSLMUTEXCLOSE(_HMutexShutdown);
    SAFE_PSLNOTIFYDESTROY(_NotifyShutdown);
    _FShuttingDown = PSLFALSE;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMUTEXCLOSE(hMutex);
    return ps;
}

