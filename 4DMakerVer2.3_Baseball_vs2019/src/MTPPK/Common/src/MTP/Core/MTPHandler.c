/*
 *  MTPHandler.c
 *
 *  Contains the implementation of public handler functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerCore.h"
#include "MTPExtensions.h"
#include "MTPHandlerExtensions.h"
#include "MTPHandlerUtil.h"
#include "MTPHandlerDeviceInfo.h"

#define MTPHANDLERDATA_MAXSIZE  256

enum
{
    MTPHANDLERMSG_FORCECLOSESESSION =
                    MAKE_PSL_MSGID(MTP_HANDLER_PRIVATE, 1, MTPF_CO),
};

static MTPDISPATCHER  _MdCore = PSLNULL;
static const PSLWCHAR _RgchFCInitMutex[] = L"ForceClose Init Mutex";

/*
 *  MTPHandlerInitialize
 *
 *  This function is called by the MTPInitialize.
 *  It will do resources allocation required for the
 *  functioning of command handlers
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  values less than zero indicates failure
 *  values greater than zero indicate success with more information
 *  for the user of the function
 *
 */
PSLSTATUS PSL_API MTPHandlerInitialize()
{
    PSLSTATUS       status;
    PSLMSGQUEUE     mqCore;
    MTPDISPATCHER   mdCore = PSLNULL;

    /* reserve space to hold the handler data */

    status = MTPContextReserveHandlerBytes(MTPHANDLERDATA_MAXSIZE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* create a dispatcher for core command handlers */

    status = MTPDispatcherCreate(MTPDISPATCHER_MSGCOUNT,
                            MTPOnCommandDispatch, &mdCore);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* initialize the custom handler extension component */

    status = MTPExtensionInitialize();
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPCreateSessionIDList();
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* get the queue coresponding to core dispatcher */

    status = MTPDispatcherGetMsgQueue(mdCore, &mqCore);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* perform property related initialization */

    status = MTPHandlerPropInitialize();
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Property intialization failed");
        goto Exit;
    }

    /* pre-open session lookup table only needs core
     * dispatcher's queue handle
     */

    status = MTPPreOpenLookupTableInit(mqCore);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Pre-Open lookup table creation failed");
        goto Exit;
    }

    _MdCore = mdCore;
    mdCore   = PSLNULL;

Exit:
    SAFE_MTPDISPATCHERDESTROY(mdCore);
    return status;
}

/*
 *  MTPHandlerUninitialize
 *
 *  This function is called by the MTPUninitialize.
 *  It will release the resources used by command handlers.
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  values less than zero indicates failure
 *  values greater than zero indicate success with more information
 *  for the user of the function
 *
 */
PSLSTATUS PSL_API MTPHandlerUninitialize()
{
    PSLSTATUS status;

    status = MTPHandlerPropUninitialize();
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Property related uninitialize failed");
    }

    status = MTPPreOpenLookupTableUninit();
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Pre-Open lookup table Uninitialize failed");
    }

    MTPDestroySessionIDList();

    status = MTPGetDeviceInfoCleanup();
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Devive info cleanup failed");
    }

    status = MTPExtensionUninitialize();
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Extension component uninitialize failed");
    }

    SAFE_MTPDISPATCHERDESTROY(_MdCore);

    return PSLSUCCESS;
}

PSLSTATUS PSL_API MTPHandlerForceCloseSession(MTPCONTEXT* pmc)
{
    PSLSTATUS status;
    BASEHANDLERDATA* pData;
    PSLMSG* pMsg = PSLNULL;
    PSLMSGQUEUE mqCore = PSLNULL;
    PSLMSGPOOL mpCore = PSLNULL;
    static PSLHANDLE hMutex = PSLNULL;
    PSLHANDLE hMutexRel = PSLNULL;

    pData = (BASEHANDLERDATA*)pmc->mtpHandlerState.pbHandlerData;

    /* Check to see if we have work to do- no session, no work
     */

    if (0 == pData->sesLevelData.dwSessionID)
    {
        status = PSLSUCCESS;
        goto Exit;
    }

    /*  Start cleaning up the session
     */

    if (PSLNULL == hMutex)
    {
        status = PSLMutexOpen(PSLTHREAD_INFINITE,
                              (PSLWCHAR*)_RgchFCInitMutex,
                              &hMutex);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        hMutexRel = hMutex;
    }
    else
    {
        status = PSLMutexAcquire(hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
        hMutexRel = hMutex;
    }

    PSLASSERT(PSLNULL != _MdCore);

    /* ASSERT that the function pointer in MTPMSGDISPATCHER
     * is PSLNULL
     */

    PSLASSERT(PSLNULL == pData->msgDispatch.pfnMsgDispatch);

    /* Assign MTPForceCloseSession as the function pointer
     */

    pData->msgDispatch.pfnMsgDispatch = MTPForceCloseSession;

    status = MTPDispatcherGetMsgPool(_MdCore, &mpCore);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Send a message to the core handler queue
     * MTPForceCloseSession will call _MTPSessionUninitialize
     */

    status = PSLMsgAlloc(mpCore, &pMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pMsg->msgID = MTPHANDLERMSG_FORCECLOSESESSION;
    pMsg->aParam0 = (PSLPARAM)pmc;

    status = MTPDispatcherGetMsgQueue(_MdCore, &mqCore);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = PSLMsgSend(mqCore, pMsg, PSLTHREAD_INFINITE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    SAFE_PSLMUTEXRELEASE(hMutexRel);

    pMsg = PSLNULL;
    status = PSLSUCCESS;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    SAFE_PSLMUTEXRELEASECLOSE(hMutex);
    return status;
}
