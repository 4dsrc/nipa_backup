/*
 *  MTPContext.c
 *
 *  Contains utility functions used to support the MTP context.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"

/*  Local Defines
 */

/*  Local Types
 */

/*  Local Functions
 */

PSLSTATUS _MTPContextReserveBytes(PSLUINT32 cbReserve,
                            PSLUINT32* pcbReserved);

PSLSTATUS _MTPContextGetReserve(PSLUINT32* pcbReserved,
                            PSLUINT32* pcbReserve);

/*  Local Variables
 */

static PSLUINT32          _CbHandlerReserve = 0;
static PSLUINT32          _CbTransportReserve = 0;

static PSL_CONST PSLWCHAR _RgchContextInit[] = L"MTP Context Init Mutex";


/*
 *  _MTPContextReserveBytes
 *
 *  Helper function for handling the reservation of bytes
 *
 *  Arguments:
 *      PSLUINT32       cbReserve     Number of bytes to reserve
 *      PSLUINT32*      pcbReserved   Number of bytes currently reserved
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success,
 *                      PSLSUCCESS_FALSE if reserve
 *                      is already larger than requested
 *
 */

PSLSTATUS _MTPContextReserveBytes(PSLUINT32 cbReserve,
                                  PSLUINT32* pcbReserved)
{
    PSLHANDLE       hMutexInit = PSLNULL;
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if (PSLNULL == pcbReserved)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure that we have the mutex so that no one interrupts us
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchContextInit, &hMutexInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Align the request to a PSLUINT32 boundary to guarantee at least
     *  4-byte alignment
     */

    cbReserve = PSLAllocAlign(cbReserve, sizeof(PSLUINT32));

    /*  If this is smaller then the current reserve then we have no work
     */

    if (cbReserve <= *pcbReserved)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Update the new value and report success
     */

    *pcbReserved = cbReserve;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);
    return ps;
}


/*
 *  _MTPContextGetReserve
 *
 *  Helper function for retrieving the number of bytes reserved
 *
 *  Arguments:
 *      PSLUINT32*      pcbReserved Number of bytes currently reserved
 *      PSLUINT32*      pcbReserve  Number of bytes reserved
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPContextGetReserve(PSLUINT32* pcbReserved,
                                PSLUINT32* pcbReserve)
{
    PSLHANDLE       hMutexInit = PSLNULL;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbReserve)
    {
        *pcbReserve = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pcbReserved) || (PSLNULL == pcbReserve))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure that we have the mutex so that no one interrupts us
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _RgchContextInit, &hMutexInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the reserve and mark it as committed
     */

    *pcbReserve = *pcbReserved;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);
    return ps;
}


/*
 *  MTPContextReserveTransportBytes
 *
 *  Specifies the number of bytes to be reserved in the context for the
 *  transport.
 *
 *  Arguments:
 *      PSLUINT32       cbReserve   Number of bytes to reserve
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success,
 *                      PSLSUCCESS_FALSE if reserve
 *                      is already larger than requested
 *
 */

PSLSTATUS PSL_API MTPContextReserveTransportBytes(PSLUINT32 cbReserve)
{
    return _MTPContextReserveBytes(cbReserve, &_CbTransportReserve);
}


/*
 *  MTPContextGetTransportReserve
 *
 *  Returns the number of bytes reserved by the transports for use
 *  in the context.
 *
 *  Arguments:
 *      PSLUINT32*      pcbReserve  Number of bytes reserved
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success,
 *                      PSLSUCCESS_FALSE if reserve
 *                      is already larger than requested
 *
 */

PSLSTATUS PSL_API MTPContextGetTransportReserve(PSLUINT32* pcbReserve)
{
    return _MTPContextGetReserve(&_CbTransportReserve, pcbReserve);
}


/*
 *  MTPContextReserveHandlerBytes
 *
 *  Specifies the number of bytes to be reserved in the context for the
 *  handler.
 *
 *  Arguments:
 *      PSLUINT32       cbReserve   Number of bytes to reserve
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success,
 *                      PSLSUCCESS_FALSE if reserve
 *                      is already larger than requested
 *
 */

PSLSTATUS PSL_API MTPContextReserveHandlerBytes(PSLUINT32 cbReserve)
{
    return _MTPContextReserveBytes(cbReserve, &_CbHandlerReserve);
}

/*
 *  MTPContextGetHandlerReserve
 *
 *  Returns the number of bytes reserved by the handlers for use
 *  in the context.
 *
 *  Arguments:
 *      PSLUINT32*      pcbReserve  Number of bytes reserved
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success,
 *                      PSLSUCCESS_FALSE if reserve
 *                      is already larger than requested
 *
 */

PSLSTATUS PSL_API MTPContextGetHandlerReserve(PSLUINT32* pcbReserve)
{
    return _MTPContextGetReserve(&_CbHandlerReserve, pcbReserve);
}

