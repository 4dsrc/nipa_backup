/*
 *  MTPHandlerCommon.c
 *
 *  Contains the implementation of common command handler functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerUtil.h"
#include "MTPHandlerCommon.h"

/*
 *  MTPPreOpenSession
 *
 *  This handler function is stored in the pre-opensession
 *  lookup table which will be called by the router when it
 *  recieves a command that is not expected to be handled
 *  before the session is open. The function will always
 *  send session not open response message to the transport.
 *
 */
PSLSTATUS PSL_API MTPPreOpenSession(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS   status;
    MTPCONTEXT* pmc;
    PSLMSG*     pRetMsg     = PSLNULL;
    PSLUINT32   dwMsgID     = 0;
    PSLUINT32   dwMsgFlags  = 0;
    PSLUINT32   dwBufSize   = 0;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmc = (MTPCONTEXT*)(pMsg->aParam0);
    PSLASSERT(PSLNULL != pmc);

    /* Allocate message to return to transport*/
    status = AllocateReturnMsg(md, &pRetMsg);
    if (PSL_FAILED (status))
    {
        goto Exit;
    }

    switch (pMsg->msgID)
    {
        case MTPMSG_CMD_AVAILABLE:
            /* read to send response*/
            pmc->mtpHandlerState.dwHandlerState =
                                               MTPHANDLERSTATE_RESPONSE;

            /* request a response buffer */
            dwBufSize   = sizeof(MTPRESPONSE);
            dwMsgFlags  = MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
            dwMsgID     = MTPMSG_CMD_CONSUMED;
            break;

        case MTPMSG_RESPONSE_BUFFER_AVAILABLE:
            PSLASSERT(MTPHANDLERSTATE_RESPONSE ==
                                   pmc->mtpHandlerState.dwHandlerState);

            PSLASSERT((PSLUINT32)pMsg->alParam >= sizeof(MTPRESPONSE));

            if (PSLNULL != (PSLVOID*)pMsg->aParam2)
            {
                status = FillResponse((PXMTPRESPONSE)pMsg->aParam2,
                                        MTP_RESPONSECODE_SESSIONNOTOPEN,
                                        0, PSLNULL, PSLNULL,
                                        PSLNULL, PSLNULL, PSLNULL);
                if (PSL_FAILED (status))
                {
                    goto Exit;
                }

                /* amount of data filled in response buffer */
                dwBufSize = sizeof(MTPRESPONSE) -
                            ((MTPRESPONSE_NUMPARAMS_MAX - 0) * \
                            sizeof(PSLUINT32));

                dwMsgID = MTPMSG_RESPONSE;
            }
            else
            {
                /* explicitly set the message ID to zero to prevent
                 * handler from trying to send any message to transport
                 */
                dwMsgID = 0;
            }
            (PSLVOID)GetReadyForNextCommand(md, pmc);
            break;
    }

    if (0 != dwMsgID)
    {
        status = FillMessage(pRetMsg, pMsg, dwMsgID,
                             (PSLBYTE*)pMsg->aParam2,
                             dwBufSize, dwMsgFlags);
        if (PSL_FAILED (status))
        {
            goto Exit;
        }

        /* Send message */
        status = MTPRouteMsgToTransport(pmc, pRetMsg);
        if (PSL_FAILED (status))
        {
            goto Exit;
        }
        pRetMsg = PSLNULL;
    }

Exit:

    SAFE_PSLMSGFREE(pRetMsg);

    return status;
}

/*
 *  MTPPostOpenSession
 *
 *  This handler function is stored in the post-opensession
 *  lookup table which will be called by the router when it
 *  recieves a command that is not handled by the MTP system
 *  after the session is open. The function will always
 *  send openration not supported response message to the transport.
 *
 */
PSLSTATUS PSL_API MTPPostOpenSession(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS   status;
    MTPCONTEXT* pmc;
    PSLMSG*     pRetMsg     = PSLNULL;
    PSLUINT32   dwMsgID     = 0;
    PSLUINT32   dwMsgFlags  = 0;
    PSLUINT32   dwBufSize   = 0;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmc = (MTPCONTEXT*)(pMsg->aParam0);
    PSLASSERT(PSLNULL != pmc);

    /* Allocate message to return to transport*/
    status = AllocateReturnMsg(md, &pRetMsg);
    if (PSL_FAILED (status))
    {
        goto Exit;
    }

    switch (pMsg->msgID)
    {
        case MTPMSG_CMD_AVAILABLE:
            /* read to send response*/
            pmc->mtpHandlerState.dwHandlerState =
                                               MTPHANDLERSTATE_RESPONSE;

            /* request a response buffer and
             * return the buffer we recieved
             */
            dwBufSize   = sizeof(MTPRESPONSE);
            dwMsgFlags  = MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
            dwMsgID     = MTPMSG_CMD_CONSUMED;
            break;

        case MTPMSG_RESPONSE_BUFFER_AVAILABLE:
            PSLASSERT(MTPHANDLERSTATE_RESPONSE ==
                                   pmc->mtpHandlerState.dwHandlerState);

            PSLASSERT(pMsg->alParam >= sizeof(MTPRESPONSE));

            if (PSLNULL != (PSLVOID*)pMsg->aParam2)
            {
                status = FillResponse(
                                 (PXMTPRESPONSE)pMsg->aParam2,
                                 MTP_RESPONSECODE_OPERATIONNOTSUPPORTED,
                                 0, PSLNULL, PSLNULL,
                                 PSLNULL, PSLNULL, PSLNULL);

                if (PSL_FAILED (status))
                {
                    goto Exit;
                }

                /* amount of data filled in the response buffer */
                dwBufSize = sizeof(MTPRESPONSE) -
                                ((MTPRESPONSE_NUMPARAMS_MAX - 0) * \
                                sizeof(PSLUINT32));

                dwMsgID = MTPMSG_RESPONSE;
            }
            else
            {
                /* explicitly set the message ID to zero to prevent
                 * handler from trying to send any message to transport
                 */
                dwMsgID  = 0;
            }
            (PSLVOID)GetReadyForNextCommand(md, pmc);
            break;
    }

    if (0 != dwMsgID)
    {
        status = FillMessage(pRetMsg, pMsg, dwMsgID,
                             (PSLBYTE*)pMsg->aParam2,
                             dwBufSize,dwMsgFlags);
        if (PSL_FAILED (status))
        {
            goto Exit;
        }

        /* Send message */
        status = MTPRouteMsgToTransport(pmc, pRetMsg);
        if (PSL_FAILED (status))
        {
            goto Exit;
        }
        pRetMsg = PSLNULL;
    }
Exit:
    SAFE_PSLMSGFREE(pRetMsg);
    return status;
}
