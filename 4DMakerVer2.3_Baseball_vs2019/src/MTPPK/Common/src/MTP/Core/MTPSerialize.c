/*
 *  MTPSerialize.c
 *
 *  Implementation of the core MTP Serialization functionality
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"

/*  Local Defines
 */

#define MTPSERIALIZE_COOKIE             'msoS'

#define DEFAULT_SERIALIZE_POOL          MTP_DEFAULT_SESSIONS

#define SERIALIZESTATE_POOL_MULTIPLIER  2

#define CALCREMAIN(_pbS, _cbM, _pbC) \
    ((PSLNULL != (_pbS)) ? ((_cbM) - ((_pbC) - (_pbS))) : 0)

/*  Local Types
 */

typedef struct _SERIALIZESTATE* PSERIALIZESTATE;

typedef struct _SERIALIZESTATE
{
    PSERIALIZESTATE             pssNext;
    PSL_CONST MTPSERIALIZEINFO* rgmsiTemplate;
    PSLPARAM                    aContext;
    PSL_CONST PSLVOID*          pvOffset;
    PSLUINT32                   cItems;
    PSLUINT32                   idxItem;
    PFNONMTPSERIALIZEDESTROY    pfnOnDestroy;
    PSLUINT32                   cRepeat;
    PSLUINT32                   idxRepeat;
} SERIALIZESTATE;

typedef struct _MTPSERIALIZEOBJ
{
#ifdef PSL_ASSERTS
    PSLUINT32                   dwCookie;
#endif  /* PSL_ASSERTS */
    PSLHANDLE                   hMutex;
    PSLBYTE*                    pbOverflow;
    PSLUINT32                   cbOverflow;
    PSLUINT32                   cbOverflowMax;
    PSLUINT64                   cbSizeTotal;
    PSLUINT64                   cbWritten;
    PSERIALIZESTATE             pssTop;
} MTPSERIALIZEOBJ;

typedef PSLVOID (*PFNSTOREVALUE)(
                    PSLVOID* pvDest,
                    PSLPARAM aValue);

typedef PSLVOID (*PFNXFERVALUE)(
                    PSLVOID* pvDest,
                    PSLVOID* pvSrc);

typedef PSLSTATUS (*PFNSERIALIZE)(
                    MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

/*  Local Functions
 */

static PSLVOID _MTPStoreUInt8(PSLVOID* pvDest,
                    PSLPARAM aValue);

static PSLVOID _MTPXFerUInt8(PSLVOID* pvDest,
                    PSLVOID* pvSrc);

static PSLVOID _MTPStoreUInt16(PSLVOID* pvDest,
                    PSLPARAM aValue);

static PSLVOID _MTPXFerUInt16(PSLVOID* pvDest,
                    PSLVOID* pvSrc);

static PSLVOID _MTPStoreUInt32(PSLVOID* pvDest,
                    PSLPARAM aValue);

static PSLVOID _MTPXFerUInt32(PSLVOID* pvDest,
                    PSLVOID* pvSrc);

static PSLVOID _MTPXFerUInt64(PSLVOID* pvDest,
                    PSLVOID* pvSrc);

static PSLVOID _MTPXFerUInt128(PSLVOID* pvDest,
                    PSLVOID* pvSrc);

static PSLSTATUS _MTPSerializeValue(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLUINT32 cbValue,
                    PFNSTOREVALUE pfnStoreValue,
                    PFNXFERVALUE pfnXFerValue,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeArray(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLUINT32 cbValue,
                    PFNXFERVALUE pfnXFerValue,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeSpecial(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeUInt8(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeUInt16(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeUInt32(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeUInt64(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeUInt128(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeUInt8Array(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeUInt16Array(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeUInt32Array(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeUInt64Array(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeUInt128Array(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS _MTPSerializeString(MTPSERIALIZEOBJ* pmso,
                    PSLUINT16 wType,
                    PSLUINT16 wFlags,
                    PSLPARAM aValue,
                    PSLPARAM aValueContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

/*  Local Variables
 */

static PSLOBJPOOL               _OpSerialize = PSLNULL;
static PSLOBJPOOL               _OpSerializeState = PSLNULL;

static const PFNSERIALIZE       _RgpfnSerializeSimple[] =
{
    _MTPSerializeSpecial,
    _MTPSerializeUInt8,
    _MTPSerializeUInt8,
    _MTPSerializeUInt16,
    _MTPSerializeUInt16,
    _MTPSerializeUInt32,
    _MTPSerializeUInt32,
    _MTPSerializeUInt64,
    _MTPSerializeUInt64,
    _MTPSerializeUInt128,
    _MTPSerializeUInt128
};

static const PFNSERIALIZE       _RgpfnSerializeArray[] =
{
    PSLNULL,
    _MTPSerializeUInt8Array,
    _MTPSerializeUInt8Array,
    _MTPSerializeUInt16Array,
    _MTPSerializeUInt16Array,
    _MTPSerializeUInt32Array,
    _MTPSerializeUInt32Array,
    _MTPSerializeUInt64Array,
    _MTPSerializeUInt64Array,
    _MTPSerializeUInt128Array,
    _MTPSerializeUInt128Array
};


/*  _MTPStoreUInt8
 *
 *  Stores a UInt8 value
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Location to store value
 *      PSLPARAM        aValue              Value to store
 *
 *  Returns:
 *      Nothing
 */

PSLVOID _MTPStoreUInt8(PSLVOID* pvDest, PSLPARAM aValue)
{
    *((PSLUINT8*)pvDest) = (PSLUINT8)aValue;
}


/*  _MTPXFerUInt8
 *
 *  Copies a UInt8 value
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Location to transfer to
 *      PSLVOID*        pvSrc               Location to transfer from
 *
 *  Returns:
 *      Nothing
 */

PSLVOID _MTPXFerUInt8(PSLVOID* pvDest, PSLVOID* pvSrc)
{
    *((PSLUINT8*)pvDest) = *((PSLUINT8*)pvSrc);
}


/*  _MTPStoreUInt16
 *
 *  Stores a UInt16 value
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Location to store value
 *      PSLPARAM        aValue              Value to store
 *
 *  Returns:
 *      Nothing
 */

PSLVOID _MTPStoreUInt16(PSLVOID* pvDest, PSLPARAM aValue)
{
    MTPStoreUInt16((PXPSLUINT16)pvDest, (PSLUINT16)aValue);
}


/*  _MTPXFerUInt16
 *
 *  Copies a UInt16 value
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Location to transfer to
 *      PSLVOID*        pvSrc               Location to transfer from
 *
 *  Returns:
 *      Nothing
 */

PSLVOID _MTPXFerUInt16(PSLVOID* pvDest, PSLVOID* pvSrc)
{
    MTPStoreUInt16((PXPSLUINT16)pvDest, *((PSLUINT16*)pvSrc));
}


/*  _MTPStoreUInt32
 *
 *  Stores a UInt32 value
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Location to store value
 *      PSLPARAM        aValue              Value to store
 *
 *  Returns:
 *      Nothing
 */

PSLVOID _MTPStoreUInt32(PSLVOID* pvDest, PSLPARAM aValue)
{
    MTPStoreUInt32((PXPSLUINT32)pvDest, (PSLUINT32)aValue);
}


/*  _MTPXFerUInt32
 *
 *  Copies a UInt32 value
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Location to transfer to
 *      PSLVOID*        pvSrc               Location to transfer from
 *
 *  Returns:
 *      Nothing
 */

PSLVOID _MTPXFerUInt32(PSLVOID* pvDest, PSLVOID* pvSrc)
{
    MTPStoreUInt32((PXPSLUINT32)pvDest, *((PSLUINT32*)pvSrc));
}


/*  _MTPXFerUInt64
 *
 *  Copies a UInt64 value
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Location to transfer to
 *      PSLVOID*        pvSrc               Location to transfer from
 *
 *  Returns:
 *      Nothing
 */

PSLVOID _MTPXFerUInt64(PSLVOID* pvDest, PSLVOID* pvSrc)
{
    MTPStoreUInt64((PXPSLUINT64)pvDest, (PSLUINT64*)pvSrc);
}


/*  _MTPXFerUInt128
 *
 *  Copies a UInt128 value
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Location to transfer to
 *      PSLVOID*        pvSrc               Location to transfer from
 *
 *  Returns:
 *      Nothing
 */

PSLVOID _MTPXFerUInt128(PSLVOID* pvDest, PSLVOID* pvSrc)
{
    MTPStoreUInt128((PXPSLUINT128)pvDest, (PSLUINT128*)pvSrc);
}


/*  _MTPXFerGUID
 *
 *  Copies a PSLGUID value
 *
 *  Arguments:
 *      PSLVOID*        pvDest              Location to transfer to
 *      PSLVOID*        pvSrc               Location to transfer from
 *
 *  Returns:
 *      Nothing
 */

PSLVOID _MTPXFerGUID(PSLVOID* pvDest, PSLVOID* pvSrc)
{
    MTPStoreGUID((PXPSLGUID)pvDest, (PSLGUID*)pvSrc);
}


/*  _MTPSerializeValue
 *
 *  Serializes a value into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLUINT32       cbValue             Size of the value
 *      PFNSTOREVALUE   pfnStoreValue       Function to store the value
 *      PFNXFERVALUE    pfnXFerValue        Function to transfer the value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeValue(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLUINT32 cbValue, PFNSTOREVALUE pfnStoreValue,
                    PFNXFERVALUE pfnXFerValue, PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed)
{
    PSLSTATUS               ps;
    PFNMTPSERIALIZEVALUE    pfnCallback;
    PSLVOID*                pvValue;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmso) || ((0 < cbBuf) && (PSLNULL == pvBuf)) ||
        (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If we are just being asked for a size return it
     */

    if (PSLNULL == pvBuf)
    {
        *pcbUsed = cbValue;
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Make sure we have space to hold the value
     */

    if (cbValue > cbBuf)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Determine how the value is encoded so that we can store it in
     *  the serialize buffer
     */

    switch (MTPSERIALIZEFLAG_ENCODING_MASK & wFlags)
    {
    case MTPSERIALIZEFLAG_BYVALUE:
        /*  Pull the value from the PSLPARAM via a cast
         */

        if (PSLNULL == pfnStoreValue)
        {
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }
        pfnStoreValue(pvBuf, aValue);
        break;

    case MTPSERIALIZEFLAG_BYREFERENCE:
    case MTPSERIALIZEFLAG_BYOFFSETVAL:
    case MTPSERIALIZEFLAG_BYOFFSETREF:
        /*  Make sure that we can perform a transfer
         */

        if (PSLNULL == pfnXFerValue)
        {
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }

        /*  Determine where the reference to the data actually comes from
         */

        switch (MTPSERIALIZEFLAG_ENCODING_MASK & wFlags)
        {
        case MTPSERIALIZEFLAG_BYREFERENCE:
            /*  Pull the value from the location specifed in the aValue location
             */

            pvValue = (PSLVOID*)aValue;
            break;

        case MTPSERIALIZEFLAG_BYOFFSETVAL:
            /*  Pull the value from the offset specified off of the current
             *  pvOffset location
             */

            pvValue = (PSLBYTE*)(pmso->pssTop->pvOffset) + aValue;
            break;

        case MTPSERIALIZEFLAG_BYOFFSETREF:
            /*  Pull the location for the value from the offset specified off
             *  of the current pvOffset location
             */

            pvValue = *((PSLVOID**)
                            ((PSLBYTE*)(pmso->pssTop->pvOffset) + aValue));
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  In the case of reference behaviors a secondary offset off the
         *  initial location may be specified- adjust for this as necessary
         */

        switch (MTPSERIALIZEFLAG_CONTEXTOFFSET_MASK & wFlags)
        {
        case MTPSERIALIZEFLAG_VALOFFSET:
            /*  An additional offset is to be applied to the location specified
             *  as the value to get the final location
             */

            pvValue = (PSLBYTE*)pvValue + aValueContext;
            break;

        case MTPSERIALIZEFLAG_REFOFFSET:
            /*  An additional offset is to be applied to the location specified
             *  and then the memory location pointed to by that location used
             *  as the source for the value
             */

            pvValue = *((PSLVOID**)((PSLBYTE*)pvValue + aValueContext));
            break;

        default:
            /*  No additional offset specified
             */

            break;
        }

        /*  Make sure we have something to transfer
         */

        if (PSLNULL == pvValue)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Do the actual transfer
         */

        pfnXFerValue(pvBuf, pvValue);
        break;

    case MTPSERIALIZEFLAG_BYFUNCTION:
        /*  Call the function specified in aValue to retrieve the result
         */

        pfnCallback = (PFNMTPSERIALIZEVALUE)aValue;
        if (PSLNULL == pfnCallback)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        ps = pfnCallback(pmso->pssTop->aContext, aValueContext, wType, pvBuf,
                            cbValue, pcbUsed);
        if (PSL_FAILED(ps) || (cbValue != *pcbUsed))
        {
            PSLASSERT(PSL_FAILED(ps));
            ps = PSL_FAILED(ps) ? ps : PSLERROR_UNEXPECTED;
            goto Exit;
        }
        break;

    default:
        /*  Unknown encoding
         */

        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Report the number of bytes used
     */

    *pcbUsed = cbValue;
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  _MTPSerializeArray
 *
 *  Serializes an array into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLUINT32       cbValue             Size of the value
 *      PFNXFERVALUE    pfnXFerValue        Function to transfer the value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeArray(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLUINT32 cbValue, PFNXFERVALUE pfnXFerValue,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLUINT32               cbSize;
    PSLUINT32               cbUsed;
    PSLUINT32               cItems;
    PSLUINT32               idxItem;
    PSLSTATUS               ps;
    PSLBYTE*                pbSrc = PSLNULL;
    MTPSERIALIZEARRAYVTBL*  pvtblCallback = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmso) || ((0 < cbBuf) && (PSLNULL == pvBuf)) ||
        (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Since the number of items must be specified in aValueContext arrays
     *  cannot support augmentation via the MTPSERIALIZEFLAG_VALOFFSET or
     *  MTPSERIALZEFLAG_REFOFFSET modifiers
     */

    if (0 != (MTPSERIALIZEFLAG_CONTEXTOFFSET_MASK & wFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Determine the number of items in the array
     */

    switch (MTPSERIALIZEFLAG_ENCODING_MASK & wFlags)
    {
    case MTPSERIALIZEFLAG_BYVALUE:
        /*  Arrays cannot be serialized by value
         */

        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;

    case MTPSERIALIZEFLAG_BYREFERENCE:
        /*  Location for the array is in aValue, count is in aValueContext
         */

        pbSrc = (PSLBYTE*)aValue;
        cItems = aValueContext;
        break;

    case MTPSERIALIZEFLAG_BYOFFSETVAL:
        /*  Location for the array is an offset from the root offset
         *  specified in aValue, count is in aValueContext
         */

        pbSrc = (PSLBYTE*)pmso->pssTop->pvOffset + aValue;
        cItems = aValueContext;
        break;

    case MTPSERIALIZEFLAG_BYOFFSETREF:
        /*  Location for the array is pointed to by an offset from the
         *  root offset specified in aValue, count is in aValueContext
         */

        pbSrc = *((PSLBYTE**)((PSLBYTE*)pmso->pssTop->pvOffset + aValue));
        cItems = aValueContext;
        break;

    case MTPSERIALIZEFLAG_BYFUNCTION:
        /*  We must call to get both the count and the items
         */

        pvtblCallback = (MTPSERIALIZEARRAYVTBL*)aValue;
        if (PSLNULL == pvtblCallback)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Retrieve the count
         */

        ps = pvtblCallback->pfnMTPSerializeGetCount(pmso->pssTop->aContext,
                            aValueContext, &cItems);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    default:
        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Calculate the size required- remember to include the length of the
     *  array
     */

    cbSize = sizeof(PSLUINT32) + (cItems * cbValue);

    /*  If we are just being asked for a size return it
     */

    if (PSLNULL == pvBuf)
    {
        *pcbUsed = cbSize;
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Make sure we have space to hold the value
     */

    if (cbSize > cbBuf)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Store the length of the array
     */

    MTPStoreUInt32((PXPSLUINT32)pvBuf, cItems);
    pvBuf = (PSLBYTE*)pvBuf + sizeof(PSLUINT32);

    /*  Walk through each item and store it in the output buffer
     */

    for (idxItem = 0;
            idxItem < cItems;
            idxItem++, pvBuf = (PSLBYTE*)pvBuf + cbValue)
    {
        /*  Transfer the values from the source to the destination according to
         *  the encoding in the table
         */

        switch (MTPSERIALIZEFLAG_ENCODING_MASK & wFlags)
        {
        case MTPSERIALIZEFLAG_BYREFERENCE:
        case MTPSERIALIZEFLAG_BYOFFSETVAL:
        case MTPSERIALIZEFLAG_BYOFFSETREF:
            /*  Transfer the data from the source specified in the table
             */

            if (PSLNULL == pfnXFerValue)
            {
                ps = PSLERROR_INVALID_OPERATION;
                goto Exit;
            }

            /*  Make sure we have something to transfer
             */

            if (PSLNULL == pbSrc)
            {
                ps = PSLERROR_INVALID_DATA;
                goto Exit;
            }

            /*  And do the actual transfer
             */

            pfnXFerValue(pvBuf, pbSrc);
            pbSrc += cbValue;
            break;

        case MTPSERIALIZEFLAG_BYFUNCTION:
            /*  Call the function specified in aValue to retrieve the result
             */

            PSLASSERT(PSLNULL != pvtblCallback);
            ps = pvtblCallback->pfnMTPSerializeGetItem(idxItem,
                            pmso->pssTop->aContext, aValueContext,
                            (PSLUINT16)(wType & ~MTP_DATATYPE_ARRAY),
                            pvBuf, cbValue, &cbUsed);
            if (PSL_FAILED(ps) || (cbValue != cbUsed))
            {
                PSLASSERT(PSL_FAILED(ps));
                ps = PSL_FAILED(ps) ? ps : PSLERROR_UNEXPECTED;
                goto Exit;
            }
            break;

        default:
            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }
    }

    /*  Report the number of bytes used
     */

    *pcbUsed = cbSize;
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  _MTPSerializeSpecial
 *
 *  Serializes special case modes- these are all triggered off the "undefined"
 *  MTP data type and typically require the use of a callback for correct
 *  handling.
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeSpecial(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLSTATUS                       ps;
    SERIALIZESTATE*                 pssNew = PSLNULL;
    PFNMTPSERIALIZEVALUE            pfnCallback;
    PSL_CONST PSLPKEY*              pkeyVal;
    MTPSERIALIZETEMPLATEVTBL*       vtblTemplate;
    MTPSERIALIZETEMPLATEREPEATVTBL* vtblTemplateRepeat = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmso) || (MTP_DATATYPE_UNDEFINED != wType) ||
        ((0 < cbBuf) && (PSLNULL == pvBuf)) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Look at the flags to determine what type of serialization we are doing
     */

    switch (MTPSERIALIZEFLAG_ENCODING_MASK & wFlags)
    {
    case MTPSERIALIZEFLAG_BYREFERENCE:
    case MTPSERIALIZEFLAG_BYOFFSETVAL:
    case MTPSERIALIZEFLAG_BYOFFSETREF:
        /*  Custom handling is provided for the PSLPKEY structure given it's
         *  frequent appearance in service operations.  Only this mode is
         *  supported here.
         */

        if (!(MTPSERIALIZEFLAG_PSLPKEY & wFlags))
        {
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }

        /*  If we are just determining size then report it
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = sizeof(PSLPKEY);
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure we have enough space to store the PKEY
         */

        if (sizeof(PSLPKEY) > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  Retrieve the location of the PSLPKEY*
         */

        switch (MTPSERIALIZEFLAG_ENCODING_MASK & wFlags)
        {
        case MTPSERIALIZEFLAG_BYREFERENCE:
            /*  PSLPKEY pointer is provided directly in the data
             */

            pkeyVal = (PSL_CONST PSLPKEY*)aValue;
            break;

        case MTPSERIALIZEFLAG_BYOFFSETVAL:
            /*  PSLPKEY pointer is provided directly at the byte offset from
             *  pvOffset
             */

            pkeyVal = (PSL_CONST PSLPKEY*)((PSLBYTE*)pmso->pssTop->pvOffset +
                            aValue);
            break;

        case MTPSERIALIZEFLAG_BYOFFSETREF:
            /*  PSLPKEY pointer is read from the location determined by adding
             *  the byte offset in aValue to pvContext
             */

            pkeyVal = (PSL_CONST PSLPKEY*)(*((PSLVOID**)
                            ((PSLBYTE*)pmso->pssTop->pvOffset + aValue)));
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Handle any additional offsets
         */

        switch (MTPSERIALIZEFLAG_CONTEXTOFFSET_MASK & wFlags)
        {
        case MTPSERIALIZEFLAG_VALOFFSET:
            /*  Adjust by the additional offset
             */

            pkeyVal = (PSL_CONST PSLPKEY*)((PSLBYTE*)pkeyVal + aValueContext);
            break;

        case MTPSERIALIZEFLAG_REFOFFSET:
            /*  Use the memory location specified at the additional offset
             */

            pkeyVal = (PSL_CONST PSLPKEY*)(*((PSLVOID**)
                            ((PSLBYTE*)pkeyVal + aValueContext)));
            break;

        default:
            break;
        }

        /*  Make sure we have something to transfer
         */

        if (PSLNULL == pkeyVal)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Serialize the namespace GUID
         */

        MTPStoreGUID((PXPSLGUID)pvBuf, &(pkeyVal->nsid));
        pvBuf = (PSLBYTE*)pvBuf + sizeof(PSLGUID);
        MTPStoreUInt32((PXPSLUINT32)pvBuf, pkeyVal->pid);
        *pcbUsed = sizeof(PSLPKEY);
        ps = PSLSUCCESS;
        break;

    case MTPSERIALIZEFLAG_BYFUNCTION:
        /*  Extract the callback function and just call it directly- we do not
         *  need to do any additional work here
         */

        pfnCallback = (PFNMTPSERIALIZEVALUE)aValue;
        if (PSLNULL == pfnCallback)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        ps = pfnCallback(pmso->pssTop->aContext, aValueContext, wType, pvBuf,
                            cbBuf, pcbUsed);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPSERIALIZEFLAG_BYTEMPLATEREPEAT:
        /*  Extract the full VTable from the value
         */

        vtblTemplateRepeat = (MTPSERIALIZETEMPLATEREPEATVTBL*)aValue;

        /*  FALL THROUGH INTENTIONAL */

    case MTPSERIALIZEFLAG_BYTEMPLATE:
        /*  Extract just the base template vtbl from the value
         */

        vtblTemplate = (MTPSERIALIZETEMPLATEVTBL*)aValue;
        if (PSLNULL == vtblTemplate)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Retrieve a new node for this serialization
         */

        ps = PSLObjectPoolAlloc(_OpSerializeState, &pssNew);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  If we are repeating the template retrieve the repeat count
         */

        if (MTPSERIALIZEFLAG_BYTEMPLATEREPEAT ==
                            (MTPSERIALIZEFLAG_ENCODING_MASK & wFlags))
        {
            /*  Call to ask for the count
             */

            PSLASSERT(PSLNULL != vtblTemplateRepeat);
            ps = vtblTemplateRepeat->pfnMTPSerializeGetCount(
                            pmso->pssTop->aContext, aValueContext,
                            &(pssNew->cRepeat));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  If this template will be repeated zero times skip the
             *  rest of the work
             */

            if (0 == pssNew->cRepeat)
            {
                ps = PSLSUCCESS;
                break;
            }
        }

        /*  Retrieve the new template
         */

        ps = vtblTemplate->pfnMTPSerializeGetTemplate(pmso->pssTop->aContext,
                            aValueContext, &(pssNew->rgmsiTemplate),
                            &(pssNew->cItems), &(pssNew->pfnOnDestroy));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  And retrieve the initial context
         */

        ps = vtblTemplate->pfnMTPSerializeGetContext(0, pmso->pssTop->aContext,
                            aValueContext, &(pssNew->aContext),
                            &(pssNew->pvOffset));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  See if we have finished an infinite repeat
         */

        if (PSLSUCCESS != ps)
        {
            /*  Check to see if we were handed an infinite repeat- if so we
             *  are done serializing
             */

            if ((MTPSERIALIZE_INFINITE_REPEAT == pssNew->cRepeat) &&
                (PSLSUCCESS_NOT_AVAILABLE == ps))
            {
                /*  There are no items to repeat in this template- stop working
                 */

                ps = PSLSUCCESS;
                break;
            }

            /*  If we get here for any other reason than something has gone
             *  wrong
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  We have initialized the new state- go ahead and push it onto the
         *  stack so we start execution from here
         */

        pssNew->pssNext = pmso->pssTop;
        pmso->pssTop = pssNew;
        pssNew = PSLNULL;
        ps = PSLSUCCESS;
        break;

    default:
        /*  We do not support any other form of encoding flags
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    SAFE_PSLOBJECTPOOLFREE(pssNew);
    return ps;
}


/*  _MTPSerializeUInt8
 *
 *  Serializes a UInt8 into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeUInt8(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    return _MTPSerializeValue(pmso, wType, wFlags, aValue, aValueContext,
                            sizeof(PSLUINT8), _MTPStoreUInt8, _MTPXFerUInt8,
                            pvBuf, cbBuf, pcbUsed);
}


/*  _MTPSerializeUInt16
 *
 *  Serializes a UInt16 into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeUInt16(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    return _MTPSerializeValue(pmso, wType, wFlags, aValue, aValueContext,
                            sizeof(PSLUINT16), _MTPStoreUInt16, _MTPXFerUInt16,
                            pvBuf, cbBuf, pcbUsed);
}


/*  _MTPSerializeUInt32
 *
 *  Serializes a UInt32 into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeUInt32(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    return _MTPSerializeValue(pmso, wType, wFlags, aValue, aValueContext,
                            sizeof(PSLUINT32), _MTPStoreUInt32, _MTPXFerUInt32,
                            pvBuf, cbBuf, pcbUsed);
}


/*  _MTPSerializeUInt64
 *
 *  Serializes a UInt64 into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeUInt64(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    return _MTPSerializeValue(pmso, wType, wFlags, aValue, aValueContext,
                            sizeof(PSLUINT64), PSLNULL, _MTPXFerUInt64,
                            pvBuf, cbBuf, pcbUsed);
}


/*  _MTPSerializeUInt128
 *
 *  Serializes a UInt128 into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeUInt128(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLBOOL         fGUID;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Determine if we are serializing a GUID
     */

    fGUID = (MTPSERIALIZEFLAG_PSLGUID & wFlags);
    if (fGUID && (MTP_DATATYPE_UINT128 != wType))
    {
        ps = PSLERROR_INVALID_DATA;
        goto Exit;
    }

    /*  Handle the serialization
     */

    ps = _MTPSerializeValue(pmso, wType, wFlags, aValue, aValueContext,
                            sizeof(PSLUINT128), PSLNULL,
                            fGUID ? _MTPXFerGUID : _MTPXFerUInt128,
                            pvBuf, cbBuf, pcbUsed);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPSerializeUInt8Array
 *
 *  Serializes a UInt8 Array into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeUInt8Array(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    return _MTPSerializeArray(pmso, wType, wFlags, aValue, aValueContext,
                    sizeof(PSLUINT8), _MTPXFerUInt8, pvBuf, cbBuf, pcbUsed);
}


/*  _MTPSerializeUInt16Array
 *
 *  Serializes a UInt16 Array into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeUInt16Array(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    return _MTPSerializeArray(pmso, wType, wFlags, aValue, aValueContext,
                            sizeof(PSLUINT16), _MTPXFerUInt16,
                            pvBuf, cbBuf, pcbUsed);
}


/*  _MTPSerializeUInt32Array
 *
 *  Serializes a UInt32 Array into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeUInt32Array(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    return _MTPSerializeArray(pmso, wType, wFlags, aValue, aValueContext,
                            sizeof(PSLUINT32), _MTPXFerUInt32,
                            pvBuf, cbBuf, pcbUsed);
}


/*  _MTPSerializeUInt64Array
 *
 *  Serializes a UInt64 Array into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeUInt64Array(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    return _MTPSerializeArray(pmso, wType, wFlags, aValue, aValueContext,
                            sizeof(PSLUINT64), _MTPXFerUInt64,
                            pvBuf, cbBuf, pcbUsed);
}


/*  _MTPSerializeUInt128Array
 *
 *  Serializes a UInt128 Array into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeUInt128Array(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLBOOL         fGUID;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Determine if we are serializing a GUID
     */

    fGUID = (MTPSERIALIZEFLAG_PSLGUID & wFlags);
    if (fGUID && (MTP_DATATYPE_AUINT128 != wType))
    {
        ps = PSLERROR_INVALID_DATA;
        goto Exit;
    }

    /*  Handle the serialization
     */

    ps = _MTPSerializeArray(pmso, wType, wFlags, aValue, aValueContext,
                            sizeof(PSLUINT128),
                            fGUID ? _MTPXFerGUID : _MTPXFerUInt128,
                            pvBuf, cbBuf, pcbUsed);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPSerializeString
 *
 *  Serializes a String into the output buffer
 *
 *  Arguments:
 *      MTPSERIALIZEOBJ*
 *                      pmso                Serialization object
 *      PSLUINT16       wType               Type to be serialized
 *      PSLUINT16       wFlags              Flags
 *      PSLPARAM        aValue              Value to serialize
 *      PSLPARAM        aValueContext       Context associated with value
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Lenght of data buffer
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPSerializeString(MTPSERIALIZEOBJ* pmso, PSLUINT16 wType,
                    PSLUINT16 wFlags, PSLPARAM aValue, PSLPARAM aValueContext,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLUINT32               cbSize;
    PSLUINT32               cchSrc = 0;
    MTPSTRING               mstrTemp;
    PSLSTATUS               ps;
    PFNMTPSERIALIZEVALUE    pfnCallback = PSLNULL;
    PSLCWSTR                szSrc = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmso) || ((PSLNULL != pvBuf) && (0 == cbBuf)) ||
        (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Strings can never be serialized by value
     */

    if (MTPSERIALIZEFLAG_BYVALUE & wFlags)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Determine the length of the string
     */

    switch (MTPSERIALIZEFLAG_ENCODING_MASK & wFlags)
    {
    case MTPSERIALIZEFLAG_BYREFERENCE:
    case MTPSERIALIZEFLAG_BYOFFSETVAL:
    case MTPSERIALIZEFLAG_BYOFFSETREF:
        /*  Grab the source for just the offset string
         */

        switch (MTPSERIALIZEFLAG_ENCODING_MASK & wFlags)
        {
        case MTPSERIALIZEFLAG_BYREFERENCE:
            /*  String is located at the location specified in the value
             */

            szSrc = (PSLCWSTR)aValue;
            break;

        case MTPSERIALIZEFLAG_BYOFFSETVAL:
            /*  String is located at the specified offset from the value of
             *  the root offset
             */

            szSrc = (PSLCWSTR)((PSLBYTE*)pmso->pssTop->pvOffset + aValue);
            break;

        case MTPSERIALIZEFLAG_BYOFFSETREF:
            /*  Pointer to the string is located at the specified offset
             *  from the value of the root offset
             */

            szSrc = *((PSLCWSTR*)((PSLBYTE*)pmso->pssTop->pvOffset + aValue));
            break;

        default:
            /*  Unexpected encoding
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  See if an adjustment offset has been provided
         */

        switch (MTPSERIALIZEFLAG_CONTEXTOFFSET_MASK & wFlags)
        {
        case MTPSERIALIZEFLAG_VALOFFSET:
            /*  Adjust the source by the additional offset
             */

            szSrc = (PSLCWSTR)((PSL_CONST PSLBYTE*)szSrc + aValueContext);
            break;

        case MTPSERIALIZEFLAG_REFOFFSET:
            /*  Adjust the source by the additional offset and then use the
             *  pointer at that location to actually extract the data
             */

            szSrc = *((PSLCWSTR*)((PSL_CONST PSLBYTE*)szSrc + aValueContext));
            break;

        default:
            /*  Check to see if the length has already been provided so that we
             *  do not have to walk the string to determine it
             */

            if (0 != aValueContext)
            {
                /*  Just calculate the length directly based on the size to
                 *  optimize performance
                 */

                if (MAX_MTP_STRING_LEN <= aValueContext)
                {
                    ps = PSLERROR_INVALID_DATA;
                    goto Exit;
                }
                cchSrc = aValueContext;
            }
            break;
        }

        /*  Determine the byte length of the string
         */

        ps = MTPStoreMTPString(PSLNULL, 0, &cbSize, szSrc,
                            (0 != cchSrc) ? cchSrc : MAX_MTP_STRING_LEN);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPSERIALIZEFLAG_BYFUNCTION:
        /*  We must call to get both the count and the items
         */

        pfnCallback = (PFNMTPSERIALIZEVALUE)aValue;
        if (PSLNULL == pfnCallback)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        ps = pfnCallback(pmso->pssTop->aContext, aValueContext, wType,
                            &mstrTemp, sizeof(mstrTemp), &cbSize);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    default:
        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  If we are just being asked for a size return it
     */

    if (PSLNULL == pvBuf)
    {
        *pcbUsed = cbSize;
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Make sure we have space to hold the value
     */

    if (cbSize > cbBuf)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Do the work to actually transfer the string
     */

    switch (MTPSERIALIZEFLAG_ENCODING_MASK & wFlags)
    {
    case MTPSERIALIZEFLAG_BYREFERENCE:
    case MTPSERIALIZEFLAG_BYOFFSETVAL:
    case MTPSERIALIZEFLAG_BYOFFSETREF:
        /*  Transfer the data from the source specified in the table
         */

        ps = MTPStoreMTPString((PXMTPSTRING)pvBuf, cbBuf, &cbSize, szSrc,
                            (0 != cchSrc) ? cchSrc : MAX_MTP_STRING_LEN);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPSERIALIZEFLAG_BYFUNCTION:
        /*  Copy the temporary string that we already retrieved
         */

        PSLCopyMemory(pvBuf, &mstrTemp, cbSize);
        break;

    default:
        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Report the number of bytes used
     */

    *pcbUsed = cbSize;
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPSerializeInitialize
 *
 *  Initializes the MTP Serializer functionality.
 *
 *  Arguments:
 *      PSLUINT32       cSessionMax         Maximum number of sessions
 *                                            to support simultaneously,
 *                                            0 for unlimited
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPSerializeInitialize(PSLUINT32 cSessionMax)
{
    PSLUINT32       cItems;
    PSLSTATUS       ps;

    /*  Make sure we are not already initialized
     *
     *  NOTE: MTPInitialize currently handles synchronization of initialize
     *  calls so we do not do additional work here.
     */

    if ((PSLNULL != _OpSerialize) || (PSLNULL != _OpSerializeState))
    {
        ps = PSLERROR_IN_USE;
        goto Exit;
    }

    /*  Determine the actual number of objects we want to reserve- we always
     *  want to keep some around even when we are running in an "unlimited"
     *  session model
     */

    cItems = (0 != cSessionMax) ? cSessionMax : DEFAULT_SERIALIZE_POOL;

    /*  Create the object pool so that we avoid allocations
     */

    ps = PSLObjectPoolCreate(cItems, sizeof(MTPSERIALIZEOBJ),
                            PSLOBJPOOLFLAG_ZERO_MEMORY | ((0 != cSessionMax) ?
                                    0 : PSLOBJPOOLFLAG_ALLOW_ALLOC),
                            &_OpSerialize);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And a pool of serialize states so that we avoid allocations here as
     *  well
     */

    cItems *= SERIALIZESTATE_POOL_MULTIPLIER;
    ps = PSLObjectPoolCreate(cItems, sizeof(SERIALIZESTATE),
                            PSLOBJPOOLFLAG_ZERO_MEMORY |
                                    PSLOBJPOOLFLAG_ALLOW_ALLOC,
                            &_OpSerializeState);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPSerializeUninitialize
 *
 *  Performs cleanup operations on the MTPSerialize sub-system
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPSerializeUninitialize(PSLVOID)
{
    /*  Release the object pool
     *
     *  NOTE: MTPUninitialize currently handles thread synchronization so we
     *  do not do additional work here
     */

    SAFE_PSLOBJECTPOOLDESTROY(_OpSerialize);
    SAFE_PSLOBJECTPOOLDESTROY(_OpSerializeState);
    return PSLSUCCESS;
}


/*  MTPSerializeCreate
 *
 *  Creates an instance of the MTP Serializer for the provided template.
 *
 *  Arguments:
 *      MTPSERIALIZEINFO*
 *                      rgmsiTemplate       Serialization template for this
 *                                            object
 *      PSLUINT32       cItems              Number of items in the template
 *      PSLPARAM        aRootContext        Context for use in callbacks
 *      PSLVOID*        pvRootOffset        Root offset for items specified
 *                                            via offset
 *      PFNONMTPSERIALIZEDESTROY
 *                      pfnOnDestroy        Callback function if template must
 *                                            be cleaned up
 *      MTPSERIALIZE*   pmso                Resulting serialization object
 */

PSLSTATUS PSL_API MTPSerializeCreate(
                    PSL_CONST MTPSERIALIZEINFO* rgmsiTemplate, PSLUINT32 cItems,
                    PSLPARAM aRootContext, PSL_CONST PSLVOID* pvRootOffset,
                    PFNONMTPSERIALIZEDESTROY pfnOnDestroy, MTPSERIALIZE* pmso)
{
    PSLSTATUS           ps;
    MTPSERIALIZEOBJ*    pmsoNew = PSLNULL;
    SERIALIZESTATE*     pssNew = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmso)
    {
        *pmso = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == rgmsiTemplate) || (0 == cItems) || (PSLNULL == pmso))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we are initialized
     */

    PSLASSERT((PSLNULL != _OpSerialize) && (PSLNULL != _OpSerializeState));
    if ((PSLNULL == _OpSerialize) || (PSLNULL == _OpSerializeState))
    {
        ps = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    /*  Retrieve new serialization objects
     */

    ps = PSLObjectPoolAlloc(_OpSerialize, &pmsoNew);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = PSLObjectPoolAlloc(_OpSerializeState, &pssNew);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Set the cookie if necessary
     */

    pmsoNew->dwCookie = MTPSERIALIZE_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Allocate a mutex for this object
     */

    ps = PSLMutexOpen(0, PSLNULL, &pmsoNew->hMutex);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize the state and associate it with the pool
     */

    pssNew->cItems = cItems;
    pssNew->rgmsiTemplate = rgmsiTemplate;
    pssNew->aContext = aRootContext;
    pssNew->pvOffset = pvRootOffset;
    pssNew->pfnOnDestroy = pfnOnDestroy;

    pmsoNew->pssTop = pssNew;
    pssNew = PSLNULL;

    /*  Return the new object
     */

    *pmso = pmsoNew;
    pmsoNew = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSERIALIZEDESTROY(pmsoNew);
    SAFE_PSLOBJECTPOOLFREE(pssNew);
    return ps;
}


/*  MTPSerializeDestroy
 *
 *  Destroys and instance of the MTP Serialization object
 *
 *  Arguments:
 *      MTPSERIALIZE    mso                 Object to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPSerializeDestroy(MTPSERIALIZE mso)
{
    PSLHANDLE           hMutexClose = PSLNULL;
    PSLSTATUS           ps;
    MTPSERIALIZEOBJ*    pmso;
    SERIALIZESTATE*     pssRelease;

    /*  Validate arguments
     */

    if (PSLNULL == mso)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Extract the object
     */

    pmso = (MTPSERIALIZEOBJ*)mso;
    PSLASSERT(MTPSERIALIZE_COOKIE == pmso->dwCookie);

    /*  Acquire the mutex if one is present
     */

    if (PSLNULL != pmso->hMutex)
    {
        ps = PSLMutexAcquire(pmso->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexClose = pmso->hMutex;
    }

    /*  Release the current serialization stack
     */

    while (PSLNULL != pmso->pssTop)
    {
        /*  Remove the item from the stack
         */

        pssRelease = pmso->pssTop;
        pmso->pssTop = pssRelease->pssNext;

        /*  Call the notification function if one exists
         */

        if (PSLNULL != pssRelease->pfnOnDestroy)
        {
            pssRelease->pfnOnDestroy(pssRelease->rgmsiTemplate,
                            pssRelease->aContext,
                            pssRelease->pvOffset);
        }

        /*  And return the object to the pool
         */

        PSLObjectPoolFree(pssRelease);
    }

    /*  Release any overflow buffer
     */

    SAFE_PSLMEMFREE(pmso->pbOverflow);

    /*  Release the object back to the pool
     */

    ps = PSLObjectPoolFree(pmso);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexClose);
    return ps;
}


/*  MTPSerialize
 *
 *  Walks through the serialization template sending out the information
 *  as requested.
 *
 *  Arguments:
 *      MTPSERIALIZE    mso                 Serialization object
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of the serialization buffer
 *      PSLUINT32*      pcbWritten          Number of bytes written
 *      PSLUINT64*      pcbLeft             Number of bytes remaining
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPSerialize(MTPSERIALIZE mso, PSLVOID* pvBuf,
                    PSLUINT32 cbBuf, PSLUINT32* pcbWritten, PSLUINT64* pcbLeft)
{
    PSLUINT32                       cbUsed;
    PSLUINT64                       cbWritten;
    PSLBOOL                         fComplete = PSLFALSE;
    PSLBOOL                         fOverflow;
    PSLHANDLE                       hMutexRelease = PSLNULL;
    PSLUINT32                       idxType;
    PSLSTATUS                       ps;
    SERIALIZESTATE*                 pssCur;
    SERIALIZESTATE*                 pssParent;
    MTPSERIALIZEOBJ*                pmso;
    PFNSERIALIZE                    pfnSerialize;
    PSLBYTE*                        pbCur;
    PSLBYTE**                       ppbDest;
    MTPSERIALIZETEMPLATEREPEATVTBL* vtblRepeat;

    /*  Clear results for safety
     */

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }
    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mso) || ((0 < cbBuf) && (PSLNULL == pvBuf)) ||
        ((PSLNULL != pvBuf) && (PSLNULL == pcbWritten)) ||
        ((PSLNULL == pvBuf) && (PSLNULL == pcbLeft)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the object
     */

    pmso = (MTPSERIALIZEOBJ*)mso;
    PSLASSERT(MTPSERIALIZE_COOKIE == pmso->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmso->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmso->hMutex;

    /*  We only support an initial length calculation- make sure we are not
     *  being called again
     */

    PSLASSERT((PSLNULL != pvBuf) || (0 == pmso->cbSizeTotal));
    if ((PSLNULL == pvBuf) && (0 != pmso->cbSizeTotal))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Rather than having two separate code paths for dealing with counting
     *  the size of the data or actually serializing data into it we play some
     *  games with the code.  We dereference ppbDest to to point to the
     *  location we are using.  If we are counting size this will always point
     *  to a PSLNULL value- but if we are actually serializing data it will
     *  increment as we add data to the location
     */

    ppbDest = (PSLNULL != pvBuf) ? &pbCur : &((PSLBYTE*)pvBuf);
    pbCur = pvBuf;

    /*  Continue any previously serialized data
     */

    if (0 != pmso->cbOverflow)
    {
        /*  To generate overflow we should have been in the process of
         *  serialization- we should not get a NULL buffer at this point
         */

        PSLASSERT(PSLNULL != pbCur);
        if (PSLNULL == pbCur)
        {
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }

        /*  Emit as mush of the overflow buffer as we can
         */

        cbUsed = min(cbBuf, pmso->cbOverflow);
        PSLCopyMemory(pbCur, pmso->pbOverflow, cbUsed);
        pbCur += cbUsed;
        pmso->cbOverflow -= cbUsed;
    }

    /*  The serialization process is table driven and supports in-place
     *  recursion through the special "template" data type specification.
     *  The general process is to pick up serialization "where we left off"
     *  (which could be the beginning, near the end, or multiple layers deep
     *  in a child template) and serialize as much as possible until either
     *  the buffer is full or all of the data has been serialized.  Because
     *  the serialization may be "two-pass" (one for length calculation and one
     *  to actually fill the buffer) care is taken to make sure that the root
     *  template is never released.
     */

    do
    {
        /*  Pick up the serialization where we left off and serialize as far
         *  as possible until the buffer is full
         */

        pssCur = pmso->pssTop;
        fOverflow = PSLFALSE;
        while (!fOverflow && (pssCur->idxItem < pssCur->cItems))
        {
            /*  Determine whether we are encoding an array or a single item
             */

            switch (pssCur->rgmsiTemplate[pssCur->idxItem].wType)
            {
            case MTP_DATATYPE_STRING:
                pfnSerialize = _MTPSerializeString;
                break;

            default:
                /*  We leverage the ordering of the data types to directly
                 *  determine the serialization function
                 */

                idxType = ~MTP_DATATYPE_ARRAY &
                            pssCur->rgmsiTemplate[pssCur->idxItem].wType;
                if (!(MTP_DATATYPE_ARRAY &
                            pssCur->rgmsiTemplate[pssCur->idxItem].wType))
                {
                    PSLASSERT(PSLARRAYSIZE(_RgpfnSerializeSimple) > idxType);
                    if (PSLARRAYSIZE(_RgpfnSerializeSimple) <= idxType)
                    {
                        ps = PSLERROR_INVALID_DATA;
                        goto Exit;
                    }
                    pfnSerialize = _RgpfnSerializeSimple[idxType];
                }
                else
                {
                    PSLASSERT(PSLARRAYSIZE(_RgpfnSerializeArray) > idxType);
                    if (PSLARRAYSIZE(_RgpfnSerializeArray) <= idxType)
                    {
                        ps = PSLERROR_INVALID_DATA;
                        goto Exit;
                    }
                    pfnSerialize = _RgpfnSerializeArray[idxType];
                }
                break;
            }

            /*  Make sure we picked up a value
             */

            PSLASSERT(PSLNULL != pfnSerialize);
            if (PSLNULL == pfnSerialize)
            {
                ps = PSLERROR_INVALID_DATA;
                goto Exit;
            }

            /*  Handle the serialization
             */

            ps = pfnSerialize(pmso,
                            pssCur->rgmsiTemplate[pssCur->idxItem].wType,
                            pssCur->rgmsiTemplate[pssCur->idxItem].wFlags,
                            pssCur->rgmsiTemplate[pssCur->idxItem].aValue,
                            pssCur->rgmsiTemplate[pssCur->idxItem].aValueContext,
                            *ppbDest, CALCREMAIN((PSLBYTE*)pvBuf, cbBuf, pbCur),
                            &cbUsed);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            pbCur += cbUsed;
            fOverflow = (PSLSUCCESS_WOULD_BLOCK == ps);

            /*  We only want to increment the item count if the top of the
             *  serialization stack is the same as it was for the item that
             *  was serialized
             */

            if (pssCur == pmso->pssTop)
            {
                pssCur->idxItem++;
            }
            else
            {
                pssCur = pmso->pssTop;
            }
        }

        /*  Determine if we completed the serialization- this happens if
         *  there is only one item on the stack and its entire contents
         *  have been serialized
         */

        fComplete = (PSLNULL == pmso->pssTop->pssNext) &&
                            (pmso->pssTop->cItems <= pmso->pssTop->idxItem);

        /*  If we filled the buffer then stop serializing
         */

        if ((PSLNULL != pvBuf) &&
            ((0 == CALCREMAIN((PSLBYTE*)pvBuf, cbBuf, pbCur) || fOverflow)))
        {
            break;
        }

        /*  If we have more templates left then we must either handle a repeat
         *  or move back to the previous template in the stack
         */

        if (!fComplete)
        {
            /*  Increment the repeat count on the template to account for
             *  the item that was just serialized
             */

            pssCur->idxRepeat++;

            /*  Now that we know for sure whether we are repeating or not
             *  handle the release in a single call
             */

            if (PSLNULL != pssCur->pfnOnDestroy)
            {
                /*  Only include the template in the release call if we are
                 *  finished with it
                 */

                pssCur->pfnOnDestroy((pssCur->idxRepeat < pssCur->cRepeat) ?
                                    PSLNULL : pssCur->rgmsiTemplate,
                            pssCur->aContext, pssCur->pvOffset);
            }

            /*  See if we are repeating- if so see if we just need to reset
             *  this instance of the template or if we have completed the
             *  whole set
             */

            if (pssCur->idxRepeat < pssCur->cRepeat)
            {
                /*  And acquire the next context in the list using- remember
                 *  that we need to grab it using information from the parent
                 *  template
                 */

                pssParent = pssCur->pssNext;
                vtblRepeat = (MTPSERIALIZETEMPLATEREPEATVTBL*)
                            pssParent->rgmsiTemplate[pssParent->idxItem].aValue;
                ps = vtblRepeat->vtblTemplate.pfnMTPSerializeGetContext(
                    pssCur->idxRepeat, pssParent->aContext,
                    pssParent->rgmsiTemplate[pssParent->idxItem].aValueContext,
                    &(pssCur->aContext), &(pssCur->pvOffset));
                if (PSL_FAILED(ps))
                {
                    goto Exit;
                }

                /*  If we are doing an infinite repeat make sure that we
                 *  do not need to stop
                 */

                if (MTPSERIALIZE_INFINITE_REPEAT == pssCur->cRepeat)
                {
                    /*  If we need to stop set the number of repeats so that
                     *  we will- this also has the side effect of making sure
                     *  that we really do repeat infinitely
                     */

                    pssCur->idxRepeat = (PSLSUCCESS_NOT_AVAILABLE != ps) ?
                            pssCur->idxRepeat : MTPSERIALIZE_INFINITE_REPEAT;

                    /*  Make sure we send the destroy notification if necessary
                     */

                    if ((pssCur->cRepeat <= pssCur->idxRepeat) &&
                        (PSLNULL != pssCur->pfnOnDestroy))
                    {
                        pssCur->pfnOnDestroy(pssCur->rgmsiTemplate,
                            pssCur->aContext, pssCur->pvOffset);
                    }
                }

                /*  Reset the state variables so that we restart at the top of
                 *  the template
                 */

                pssCur->idxItem = 0;
            }

            /*  See if we are done with the template and ready to move on
             */

            if (pssCur->cRepeat <= pssCur->idxRepeat)
            {
                /*  We are completely finished with this template- remove it
                 *  from the list
                 */

                PSLASSERT(pssCur == pmso->pssTop);
                pmso->pssTop = pmso->pssTop->pssNext;
                PSLObjectPoolFree(pssCur);

                /*  Increment the location in the previous templat to account
                 *  for what we just finished serializing
                 */

                pmso->pssTop->idxItem++;
            }
        }
    } while (!fComplete);

    /*  If we are just calculating the length make sure that we reset the
     *  serializer to it's initial state for the next call
     */

    if (PSLNULL == pvBuf)
    {
        PSLASSERT(fComplete);
        pmso->pssTop->idxItem = 0;
    }

    /*  Determine the number of bytes "written" to the string
     */

    cbWritten = pbCur - (PSLBYTE*)pvBuf;

    /*  Update our internal serialization length counters
     */

    if (0 == pmso->cbSizeTotal)
    {
        pmso->cbSizeTotal = (PSLNULL == pvBuf) ?
                            cbWritten : MTP_DATA_SIZE_UNKNOWN;
    }
    pmso->cbWritten += (PSLNULL != pvBuf) ? cbWritten : 0;

    /*  Return details about what is used and what remains
     */

    if ((PSLNULL != pvBuf) && (PSLNULL != pcbWritten))
    {
        /*  We should never have written more into the buffer then was
         *  available- and it should always be a UInt32 since buffers are
         *  restricted in size
         */

        PSLASSERT((cbWritten <= cbBuf) || ((0xffffffff & cbWritten) == cbWritten));
        *pcbWritten = (PSLUINT32)cbWritten;
    }
    if ((!fComplete || (PSLNULL == pvBuf)) && (PSLNULL != pcbLeft))
    {
        /*  If the data size is unknown then we need to make sure that we
         *  always return unknown
         */

        *pcbLeft = (MTP_DATA_SIZE_UNKNOWN != pmso->cbSizeTotal) ?
                            pmso->cbSizeTotal - pmso->cbWritten :
                            MTP_DATA_SIZE_UNKNOWN;
    }
    else
    {
        /*  Just make sure that we return zero if we are complete
         */

        PSLASSERT((PSLNULL == pcbLeft) || (fComplete && (0 == *pcbLeft)));
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}

