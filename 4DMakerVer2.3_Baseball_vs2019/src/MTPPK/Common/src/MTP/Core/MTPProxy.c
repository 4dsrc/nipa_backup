/*
 *  MTPProxy.c
 *
 *  Contains the implementation of proxy component.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "MTPPrecomp.h"

#ifdef _DEBUG
#include "MTPHandlerUtil.h"
#endif /*_DEBUG*/

#define MTPPROXYCONTEXT_COOKIE  'mPxC'


/*
 *  MTPProxyCreate
 *
 *  Creates a proxy
 *
 *  Arguments:
 *      PSLMSGQUEUE     mqTransport         Transport queue to bind proxy to
 *      PSLMSGQUEUE     mqEventHandler      Event handler to receive event
 *                                            notifications
 *      PSLLOOKUPTABLE  hHandlerTable       Initial handler table to be used
 *                                            by the proxy
 *      MTPCONTEXT**    Resulting context   Context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPProxyCreate(PSLMSGQUEUE mqTransport,
                    PSLMSGQUEUE mqEventHandler, PSLLOOKUPTABLE hHandlerTable,
                    MTPCONTEXT** ppmtpctx)
{
    PSLUINT32       cbDesired;
    PSLUINT32       cbHandlerData;
    PSLUINT32       cbTransportData;
    PSLSTATUS       ps;
    MTPCONTEXT*     pmtpctx = PSLNULL;

    /*  Clear results for safety
     */

    if (PSLNULL != ppmtpctx)
    {
        *ppmtpctx = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mqTransport) || (PSLNULL == mqEventHandler) ||
        (PSLNULL == hHandlerTable) || (PSLNULL == ppmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get callback handler data size
     */

    ps = MTPContextGetHandlerReserve(&cbHandlerData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Get transport data size
     */

    ps = MTPContextGetTransportReserve(&cbTransportData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Determine the total size of the structure- add extra space for
     *  a cookie if asserts are enabled
     */

    cbDesired = sizeof(MTPCONTEXT) + cbHandlerData + cbTransportData;
#ifdef PSL_ASSERTS
    cbDesired += sizeof(PSLUINT32);
#endif  /* PSL_ASSERTS */

    /*  And allocate the new context
     */

    pmtpctx = PSLMemAlloc(PSLMEM_PTR, cbDesired);
    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

#ifdef PSL_ASSERTS
    *((PSLUINT32*)pmtpctx) = MTPPROXYCONTEXT_COOKIE;
    pmtpctx = (MTPCONTEXT*)((PSLBYTE*)pmtpctx + sizeof(PSLUINT32));
#endif  /* PSL_ASSERTS */

    pmtpctx->mtpContextState.mqHandler = PSLNULL;
    pmtpctx->mtpContextState.mqTransport = mqTransport;
    pmtpctx->mtpContextState.hHandlerTable = hHandlerTable;

    pmtpctx->mtpTransportState.cbTransportData = cbTransportData;
    pmtpctx->mtpTransportState.pbTransportData =
                            (PSLBYTE*)pmtpctx + sizeof(MTPCONTEXT);
    pmtpctx->mtpHandlerState.cbHandlerData = cbHandlerData;
    pmtpctx->mtpHandlerState.pbHandlerData =
                            pmtpctx->mtpTransportState.pbTransportData +
                            pmtpctx->mtpTransportState.cbTransportData;
    pmtpctx->mtpContextState.mqEventHandler = mqEventHandler;

    /*  Return the resulting context
     */

    *ppmtpctx = pmtpctx;
    pmtpctx   = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMEMFREE(pmtpctx);
    return ps;
}


/*
 *  MTPProxyDestroy
 *
 *  Destroys the specified proxy
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx               Proxy to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPProxyDestroy(MTPCONTEXT* pmtpctx)
{
    PSLSTATUS ps;

    /*  Validate arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure this is a proxy
     */

    PSLASSERT(MTPPROXYCONTEXT_COOKIE == *((PSLUINT32*)pmtpctx - 1));

    /*  And release it- dealing with the extra cookie if necessary
     */

#ifdef PSL_ASSERTS
    pmtpctx = (MTPCONTEXT*)((PSLUINT32*)pmtpctx - 1);
#endif  /* PSL_ASSERTS */
    SAFE_PSLMEMFREE(pmtpctx);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPProxyBegin
 *
 *  Begins a proxy from the transport to the handler.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             Context to use
 *      PSLUINT16       wOpCode             Operation to proxy
 *      PSLVOID*        pvOpData            Operation data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPProxyBegin(MTPCONTEXT* pmtpctx, PSLUINT16 wOpCode,
                         PSLVOID* pvOpData)
{
    PSLBOOL             fResetHandler = PSLFALSE;
    PSLMSGQUEUE         mqCurrent;
    PSLMSGQUEUE         mqHandler;
    PFNMTPPROXYBEGINOP  pfnProxyBeginOp;
    PSLSTATUS           ps;

    /*  Validate arguments- we will check the op code in the lookup table
     *  and we have no control over the data so we cannot validate
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(MTPPROXYCONTEXT_COOKIE == *((PSLUINT32*)pmtpctx - 1));

    /*  Get handler queue id and data from loopkup table
     */

    ps = MTPLookupTableFindEntry(pmtpctx->mtpContextState.hHandlerTable,
                            wOpCode, &mqHandler,
                            (PSLPARAM*)&pfnProxyBeginOp);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Attempt to set this as the current operation
     */

    mqCurrent = PSLAtomicCompareExchangePointer(
                            &(pmtpctx->mtpContextState.mqHandler),
                            mqHandler,
                            PSLNULL);
    PSLASSERT(PSLNULL == mqCurrent);
    if (PSLNULL != mqCurrent)
    {
        /*  Currently handling another command- this is an invalid operation
         *  at this point
         */

        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }
    fResetHandler = PSLTRUE;

    /*  Begin the operation
     */

    ps = pfnProxyBeginOp(pmtpctx, wOpCode, pvOpData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  No need to reset the handler- all has succeeded
     */

    fResetHandler = PSLFALSE;

Exit:
    if (fResetHandler)
    {
        PSLAtomicExchangePointer((pmtpctx->mtpContextState.mqHandler),
                            PSLNULL);
    }
    return ps;
}


/*
 *  MTPProxyEnd
 *
 *  Called to end the proxy between the tranport and the handler
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx               MTP context to operate on
 *      MTPPROXYUPDATE* pmtpctxu              Proxy update information
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPProxyEnd(MTPCONTEXT* pmtpctx, MTPPROXYUPDATE* pmtpctxu)
{
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pmtpctxu))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(MTPPROXYCONTEXT_COOKIE == *((PSLUINT32*)pmtpctx - 1));

    /*  Update the handler table if necessary and clear the current handler
     */

    if (PSLNULL != pmtpctxu->hHandlerTableNew)
    {
        PSLAtomicExchangePointer(&(pmtpctx->mtpContextState.hHandlerTable),
                            pmtpctxu->hHandlerTableNew);
    }

    PSLAtomicExchangePointer(&(pmtpctx->mtpContextState.mqHandler), PSLNULL);

    ps = PSLSUCCESS;
Exit:
    return ps;
}

/*
 *  MTPProxyMsgToTransport
 *
 *  Proxys a message to the transport specified in the context
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             Context to proxy within
 *      PSLMSG*         pMsg                Message to proxy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPProxyMsgToTransport(MTPCONTEXT* pmtpctx, PSLMSG* pMsg)
{
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pMsg))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(MTPPROXYCONTEXT_COOKIE == *((PSLUINT32*)pmtpctx - 1));

    /*  Contexts must always contain a handler queue- if we do not have one
     *  treat as an error
     */

    PSLASSERT(PSLNULL != pmtpctx->mtpContextState.mqTransport);
    if (PSLNULL == pmtpctx->mtpContextState.mqTransport)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  If the message is supposed to have a context make sure it is present
     *  and correct
     */

    PSLASSERT(!(MTPMSGID_CONTEXT_PRESENT & pMsg->msgID) ||
                            ((PSLPARAM)pmtpctx == pMsg->aParam0));
    if ((MTPMSGID_CONTEXT_PRESENT & pMsg->msgID) &&
        ((PSLPARAM)pmtpctx != pMsg->aParam0))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  And post the message
     */

    ps = PSLMsgPost(pmtpctx->mtpContextState.mqTransport, pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPProxyMsgToHandler
 *
 *  Proxys a message to a handler.  If one is currently not available message
 *  is consumed and success reported.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx               Context to proxy within
 *      PSLMSG*         pMsg                Message to proxy
 *
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCES_NOT_AVAILABLE if
 *                        message could not be sent because no handler was
 *                        availble (message is released by this function)
 *
 */

PSLSTATUS PSL_API MTPProxyMsgToHandler(MTPCONTEXT* pmtpctx, PSLMSG* pMsg)
{
    PSLSTATUS       ps;
    PSLMSGQUEUE     mqDest;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pMsg))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(MTPPROXYCONTEXT_COOKIE == *((PSLUINT32*)pmtpctx - 1));

    /*  If the message is supposed to have a context make sure it is present
     *  and correct
     */

    PSLASSERT(!(MTPMSGID_CONTEXT_PRESENT & pMsg->msgID) ||
                            ((PSLPARAM)pmtpctx == pMsg->aParam0));
    if ((MTPMSGID_CONTEXT_PRESENT & pMsg->msgID) &&
        ((PSLPARAM)pmtpctx != pMsg->aParam0))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine which handler to forward the message to- right now all
     *  events are forwarded to the event handler
     */

    if (MTP_EVENT != PSL_MSG_CLASS(pMsg->msgID))
    {
        mqDest = pmtpctx->mtpContextState.mqHandler;
    }
    else
    {
        mqDest = pmtpctx->mtpContextState.mqEventHandler;
    }

    /*  If we do not have a context report that the proxy is not
     *  available and consume the message
     */

    if (PSLNULL == mqDest)
    {
        PSLMsgFree(pMsg);
        ps = PSLSUCCESS_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Post the message to the desired handler
     */

    ps = PSLMsgPost(mqDest, pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPProxyTransportError
 *
 *  Proxys an error in the transport to the context and performs any neccessary
 *  cleanup.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx               Context to proxy error for
 *      PSLMSGPOOL      mpSource            Source message pool for message
 *      PSLMSGID        msgID               Message that caused the error
 *      PSLUINT32       dwSequenceID        Sequence ID for message in error
 *      PSLSTATUS       psErr               Error that occurred
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPProxyTransportError(MTPCONTEXT* pmtpctx,
                    PSLMSGPOOL mpSource, PSLMSGID msgID, PSLUINT32 dwSequenceID,
                    PSLSTATUS psErr)
{
    PSLMSG*         pMsg = PSLNULL;
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    PSLASSERT(PSL_FAILED(psErr));
    if ((PSLNULL == pmtpctx) || (PSL_SUCCEEDED(psErr)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    PSLASSERT(MTPPROXYCONTEXT_COOKIE == *((PSLUINT32*)pmtpctx - 1));

    /*  Allocate a message from the specified pool and forward the error
     */

    ps = PSLMsgAlloc(mpSource, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    pMsg->msgID = MTPMSG_ERROR;
    pMsg->aParam0 = (PSLPARAM)pmtpctx;
    pMsg->aParam1 = dwSequenceID;
    pMsg->aParam2 = msgID;
    pMsg->aParam3 = psErr;

    /*  And proxy the message
     */

    ps = MTPProxyMsgToHandler(pmtpctx, pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg = PSLNULL;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}

