/*
 *  MTPHandlerUtil.c
 *
 *  Contains the implementation of utility functions used by
 *  command handlers.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerUtil.h"
#include "MTPHandlerCommon.h"
#include "MTPHandlerCore.h"
#include "MTPHandlerDevice.h"
#include "MTPHandlerDeviceInfo.h"
#include "MTPHandlerDatabase.h"
#include "MTPHandlerServices.h"
#include "MTPHandlerProp.h"
#include "MTPExtensions.h"
#include "MTPHandlerExtensions.h"

/*  Local Defines
 */

enum
{
    MTPQ_CORE,
    MTPQ_DB,
    MTPQ_MAX
};

/*  Local Types
 */

typedef struct _MTPHANDLERINITINFO
{
    PSLUINT16               wOpCode;
    PSLUINT32               idxQueue;
    PFNMTPMSGDISPATCH       pfnDispatch;
} MTPHANDLERINITINFO;

/*  Local Functions
 */

static PSLVOID _ValidateTable(
                            PSL_CONST MTPHANDLERINITINFO* pmhiiCheck,
                            PSLUINT32 cItems);

static PSLSTATUS _MTPDevicePropLookupTableInit(
                            PCXPSLUINT16 pwCodes,
                            PSLUINT32 cdwCodes);

static PSLSTATUS _MTPDevicePropLookupTableUninit();


/*  Local Variables
 */

static PSL_CONST MTPHANDLERINITINFO _RgmhiiPreSession[] =
{
    {
        MTP_OPCODE_UNDEFINED,
        MTPQ_CORE,
        MTPPreOpenSession
    },
    {
        MTP_OPCODE_GETDEVICEINFO,
        MTPQ_CORE,
        MTPGetDeviceInfo
    },
    {
        MTP_OPCODE_OPENSESSION,
        MTPQ_CORE,
        MTPOpenSession
    },
};

static PSL_CONST MTPHANDLERINITINFO _RgmhiiOpenSession[] =
{
    {
        MTP_OPCODE_UNDEFINED,
        MTPQ_CORE,
        MTPPostOpenSession
    },
    {
        MTP_OPCODE_GETDEVICEINFO,
        MTPQ_CORE,
        MTPGetDeviceInfo
    },
    {
        MTP_OPCODE_OPENSESSION,
        MTPQ_CORE,
        MTPOpenSession
    },
    {
        MTP_OPCODE_CLOSESESSION,
        MTPQ_CORE,
        MTPCloseSession
    },
    {
        MTP_OPCODE_GETSTORAGEIDS,
        MTPQ_DB,
        MTPGetStorageIDs
    },
    {
        MTP_OPCODE_GETSTORAGEINFO,
        MTPQ_DB,
        MTPGetStorageInfo
    },
    {
        MTP_OPCODE_GETNUMOBJECTS,
        MTPQ_DB,
        MTPGetNumObjects
    },
    {
        MTP_OPCODE_GETOBJECTHANDLES,
        MTPQ_DB,
        MTPGetObjectHandles
    },
    {
        MTP_OPCODE_GETOBJECTINFO,
        MTPQ_DB,
        MTPGetObjectInfo
    },
    {
        MTP_OPCODE_GETOBJECT,
        MTPQ_DB,
        MTPGetObject
    },
    {
        MTP_OPCODE_GETTHUMB,
        MTPQ_DB,
        MTPGetThumb
    },
    {
        MTP_OPCODE_DELETEOBJECT,
        MTPQ_DB,
        MTPDeleteObject
    },
    {
        MTP_OPCODE_SENDOBJECTINFO,
        MTPQ_DB,
        MTPSendObjectInfo
    },
    {
        MTP_OPCODE_SENDOBJECT,
        MTPQ_DB,
        MTPSendObject
    },
    {
        MTP_OPCODE_INITIATECAPTURE,
        MTPQ_CORE,
        MTPInitiateCapture
    },
    {
        MTP_OPCODE_FORMATSTORE,
        MTPQ_CORE,
        MTPFormatStore
    },
    {
        MTP_OPCODE_RESETDEVICE,
        MTPQ_CORE,
        MTPResetDevice
    },
    {
        MTP_OPCODE_SELFTEST,
        MTPQ_CORE,
        MTPSelfTest
    },
    {
        MTP_OPCODE_SETOBJECTPROTECTION,
        MTPQ_DB,
        MTPSetObjectProtection
    },
    {
        MTP_OPCODE_POWERDOWN,
        MTPQ_CORE,
        MTPPowerDown
    },
    {
        MTP_OPCODE_GETDEVICEPROPDESC,
        MTPQ_CORE,
        MTPGetDevicePropDesc
    },
    {
        MTP_OPCODE_GETDEVICEPROPVALUE,
        MTPQ_CORE,
        MTPGetDevicePropValue
    },
    {
        MTP_OPCODE_SETDEVICEPROPVALUE,
        MTPQ_CORE,
        MTPSetDevicePropValue
    },
    {
        MTP_OPCODE_RESETDEVICEPROPVALUE,
        MTPQ_CORE,
        MTPResetDevicePropValue
    },
    {
        MTP_OPCODE_TERMINATECAPTURE,
        MTPQ_CORE,
        MTPTerminateOpenCapture
    },
    {
        MTP_OPCODE_MOVEOBJECT,
        MTPQ_DB,
        MTPMoveObject
    },
    {
        MTP_OPCODE_COPYOBJECT,
        MTPQ_DB,
        MTPCopyObject
    },
    {
        MTP_OPCODE_GETPARTIALOBJECT,
        MTPQ_DB,
        MTPGetPartialObject
    },
    {
        MTP_OPCODE_INITIATEOPENCAPTURE,
        MTPQ_CORE,
        MTPInitiateOpenCapture
    },
#ifndef MTP_DISABLE_EXTENDED_OPERATIONS
    {
        MTP_OPCODE_GETSERVICEIDS,
        MTPQ_DB,
        MTPGetServiceIDs
    },
    {
        MTP_OPCODE_GETSERVICEINFO,
        MTPQ_DB,
        MTPGetServiceInfo
    },
    {
        MTP_OPCODE_GETSERVICECAPABILITIES,
        MTPQ_DB,
        MTPGetServiceCapabilities
    },
    {
        MTP_OPCODE_GETSERVICEPROPERTIES,
        MTPQ_DB,
        MTPGetServicePropDesc
    },
    {
        MTP_OPCODE_GETSERVICEPROPERTYLIST,
        MTPQ_DB,
        MTPGetServicePropList
    },
    {
        MTP_OPCODE_SETSERVICEPROPERTYLIST,
        MTPQ_DB,
        MTPSetServicePropList
    },
    {
        MTP_OPCODE_UPDATEOBJECTPROPERTYLIST,
        MTPQ_DB,
        MTPUpdateObjectPropList
    },
    {
        MTP_OPCODE_DELETEOBJECTPROPLIST,
        MTPQ_DB,
        MTPDeleteObjectPropList
    },
    {
        MTP_OPCODE_DELETESERVICEPROPLIST,
        MTPQ_DB,
        MTPDeleteServicePropList
    },
    {
        MTP_OPCODE_GETFORMATCAPABILITIES,
        MTPQ_DB,
        MTPGetFormatCapabilities
    },
    {
        MTP_OPCODE_GETOBJECTPROPSSUPPORTED,
        MTPQ_DB,
        MTPGetObjectPropsSupported
    },
    {
        MTP_OPCODE_GETOBJECTPROPDESC,
        MTPQ_DB,
        MTPGetObjectPropDesc
    },
    {
        MTP_OPCODE_GETOBJECTPROPVALUE,
        MTPQ_DB,
        MTPGetObjectPropValue
    },
    {
        MTP_OPCODE_SETOBJECTPROPVALUE,
        MTPQ_DB,
        MTPSetObjectPropValue
    },
    {
        MTP_OPCODE_GETOBJECTPROPLIST,
        MTPQ_DB,
        MTPGetObjectPropList
    },
    {
        MTP_OPCODE_SETOBJECTPROPLIST,
        MTPQ_DB,
        MTPSetObjectPropList
    },
    {
        MTP_OPCODE_GETINTERDEPENDENTPROPDESC,
        MTPQ_CORE,
        MTPGetInterdependentPropDesc
    },
    {
        MTP_OPCODE_SENDOBJECTPROPLIST,
        MTPQ_DB,
        MTPSendObjectPropList
    },
    {
        MTP_OPCODE_GETOBJECTREFERENCES,
        MTPQ_DB,
        MTPGetObjectReferences
    },
    {
        MTP_OPCODE_SETOBJECTREFERENCES,
        MTPQ_DB,
        MTPSetObjectReferences
    },
    {
        MTP_OPCODE_SKIP,
        MTPQ_CORE,
        MTPSkip
    },
#endif  /* MTP_DISABLE_EXTENDED_OPERATIONS */
};


/*
 *  _ValidateTable
 *
 *  Walks through both of the tables and makes sure that they are
 *  correctly sorted so that we know our lookups will work.
 *
 *  Arguments:
 *      PSL_CONST MTPHANDLERINITINFO*
 *                      pmhiiCheck          Table to check
 *      PSLUINT32       cItems              Number of items in the table
 *
 *  Returns:
 *      Nothing
 *
 */

PSLVOID _ValidateTable(PSL_CONST MTPHANDLERINITINFO* pmhiiCheck,
                            PSLUINT32 cItems)
{
    PSLUINT32       idxItem;

    /*  Validate Arguments
     */

    PSLASSERT((PSLNULL != pmhiiCheck) && (0 < cItems));

    /*  Walk through and validate that the items are sorted
     */

    for (idxItem = 1;
            (idxItem < cItems) && (pmhiiCheck[idxItem - 1].wOpCode <
                                   pmhiiCheck[idxItem].wOpCode);
            idxItem++)
    {
    }

    /*  If we did not make it all the way through the list then we have
     *  an unsorted table
     */

    if (idxItem < cItems)
    {
        PSLDebugPrintf("MTPHANDLERINITINFO fails sort validation");
        PSLASSERT(PSLFALSE);
    }
    return;
}

PSLSTATUS PSL_API MTPOnCommandDispatch(PSLMSG* pMsg)
{
    PSLSTATUS           ps;
    MTPCONTEXT*         pmc;

    /*  We should only get here on a command available message
     */

    PSLASSERT(MTPMSG_CMD_AVAILABLE == pMsg->msgID);
    if (MTPMSG_CMD_AVAILABLE != pMsg->msgID)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Extract the context
     */

    pmc = (MTPCONTEXT*)(pMsg->aParam0);

    /*  Now that a next command has been initiated we want to clear
     *  any information stashed in the context about the last
     *  object ID that we saw for a SendObjectInfo or a
     *  SendObjectPropList
     */

    ((BASEHANDLERDATA*)pmc->mtpHandlerState.pbHandlerData)->
            sesLevelData.dwLastSendInfoID = MTP_OBJECTHANDLE_UNDEFINED;

    ps = PSLSUCCESS;

Exit:
    return ps;
}


PSLSTATUS PSL_API MTPHandlerBegin(MTPDISPATCHER md, PSLMSG* pMsg,
                            MTPCONTEXT** ppmc,
                            BASEHANDLERDATA** ppData,
                            PSLMSG** ppMsg)
{
    PSLSTATUS status;
    PSLMSG* pRetMsg = PSLNULL;

    if (PSLNULL != ppmc)
    {
        *ppmc = PSLNULL;
    }

    if (PSLNULL != ppData)
    {
        *ppData = PSLNULL;
    }

    if (PSLNULL != ppMsg)
    {
        *ppMsg = PSLNULL;
    }

    if (PSLNULL == md || PSLNULL == pMsg || PSLNULL == ppmc ||
        PSLNULL == ppData || PSLNULL == ppMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* Allocate message to return to transport*/
    status = AllocateReturnMsg(md, &pRetMsg);
    if (PSL_FAILED (status))
    {
        PSLASSERT(PSLFALSE);
        goto Exit;
    }

    *ppmc = (MTPCONTEXT*)(pMsg->aParam0);
    PSLASSERT(PSLNULL != *ppmc);

    PSLASSERT(PSLNULL != (*ppmc)->mtpHandlerState.pbHandlerData);
    /*ensure handler has enough room to store its data*/
    PSLASSERT((*ppmc)->mtpHandlerState.cbHandlerData >=               \
                                              sizeof(BASEHANDLERDATA));

    *ppData = (BASEHANDLERDATA*)(*ppmc)->mtpHandlerState.pbHandlerData;

    *ppMsg = pRetMsg;
    pRetMsg = PSLNULL;
Exit:
    SAFE_PSLMSGFREE(pRetMsg);
    return status;
}

PSLSTATUS PSL_API MTPHandlerEnd(PSLMSG* pMsg)
{
    SAFE_PSLMSGFREE(pMsg);
    return PSLSUCCESS;
}

PSLSTATUS PSL_API MTPHandlerSetState(MTPCONTEXT* pmc,
                              PSLUINT16 wRespCode,
                              PSLUINT32 dwHandlerState,
                              PSLUINT32* pdwMsgFlags,
                              PSLUINT64* pqwDataSize)
{
    PSLSTATUS status;

    if (PSLNULL != pdwMsgFlags)
    {
        *pdwMsgFlags = 0;
    }

    /* this will already have a value when the
     * response code is MTP_RESPONSECODE_OK
     * so don't set the value to zero
     */
    PSLASSERT(PSLNULL != pqwDataSize);

    if (PSLNULL == pmc || PSLNULL == pdwMsgFlags ||
        PSLNULL == pqwDataSize)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (MTPHANDLERSTATE_DATA_IN == dwHandlerState)
    {
        pmc->mtpHandlerState.dwHandlerState = dwHandlerState;

        PSLASSERT(0 == *pdwMsgFlags);
    }
    else if (MTPHANDLERSTATE_DATA_OUT == dwHandlerState &&
            MTP_RESPONSECODE_OK == wRespCode)
    {
        /* request for a buffer to send data */
        pmc->mtpHandlerState.dwHandlerState = dwHandlerState;

        PSLASSERT(0 != *pqwDataSize);
        *pdwMsgFlags = MTPMSGFLAG_DATA_BUFFER_REQUEST |
                    ((MTP_DATA_SIZE_UNKNOWN != *pqwDataSize) ?
                            MTPMSGFLAG_COMPLETE : 0);
    }
    else
    {
        /* request for a response buffer on failure or no data op. */
        pmc->mtpHandlerState.dwHandlerState =
                                    MTPHANDLERSTATE_RESPONSE;

        *pdwMsgFlags = MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
        *pqwDataSize  = sizeof(MTPRESPONSE);
    }

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS PSL_API MTPHandlerRouteMsgToTransport(
                                MTPCONTEXT* pmc, PSLMSG* pRetMsg,
                                PSLMSG* pSrcMsg,PSLUINT32 msgID,
                                PSLBYTE* pbBuf, PSLLPARAM cbBuf,
                                PSLUINT32 dwFlags)
{
    PSLSTATUS status;

    if (PSLNULL == pmc || PSLNULL == pRetMsg || PSLNULL == pSrcMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (0 != msgID)
    {
        status = FillMessage(pRetMsg, pSrcMsg, msgID,
                            pbBuf, cbBuf, dwFlags);
        if (PSL_FAILED (status))
        {
            PSLASSERT(PSLFALSE);
            goto Exit;
        }

        /* send the message */
        status = MTPRouteMsgToTransport(pmc, pRetMsg);
        if (PSL_FAILED (status))
        {
            PSLASSERT(PSLFALSE);
            goto Exit;
        }
    }

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS PSL_API MTPHandlerGetResponseCodeFromStatus(PSLSTATUS dwStatus,
                                        PSLUINT16* pwRespCode)
{
    PSLSTATUS status;
    PSLUINT16 wRespCode;

    if (PSLNULL != pwRespCode)
    {
        *pwRespCode = MTP_RESPONSECODE_GENERALERROR;
    }

    if (PSLNULL == pwRespCode)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (PSL_FAILED(dwStatus))
    {
        switch(dwStatus)
        {
        case PSLERROR_INVALID_PARAMETER:
            wRespCode = MTP_RESPONSECODE_INVALIDPARAMETER;
            break;

        case PSLERROR_ACCESS_DENIED:
            wRespCode = MTP_RESPONSECODE_ACCESSDENIED;
            break;

        case PSLERROR_NOT_AVAILABLE:
            wRespCode = MTP_RESPONSECODE_STORENOTAVAILABLE;
            break;

        case PSLERROR_INVALID_OPERATION:
        case PSLERROR_NOT_IMPLEMENTED:
            wRespCode = MTP_RESPONSECODE_OPERATIONNOTSUPPORTED;
            break;

        case MTPERROR_DATABASE_INVALID_FORMATCODE:
        case MTPERROR_PROPERTY_FORMATCODE_UNSUPPORTED:
            wRespCode = MTP_RESPONSECODE_INVALIDOBJECTFORMATCODE;
            break;

        case MTPERROR_DATABASE_INVALID_HANDLE:
            wRespCode = MTP_RESPONSECODE_INVALIDOBJECTHANDLE;
            break;

        case MTPERROR_DATABASE_INVALID_STORAGEID:
            wRespCode = MTP_RESPONSECODE_INVALIDSTORAGEID;
            break;

        case MTPERROR_DATABASE_INVALID_PARENT:
            wRespCode = MTP_RESPONSECODE_INVALIDPARENT;
            break;

        case MTPERROR_DATABASE_SPEC_BY_GROUP_UNSUPPORTED:
            wRespCode = MTP_RESPONSECODE_SPECIFICATIONBYGROUPUNSUPPORTED;
            break;

        case MTPERROR_DATABASE_SPEC_BY_DEPTH_UNSUPPORTED:
            wRespCode = MTP_RESPONSECODE_SPECIFICATIONBYDEPTHUNSUPPORTED;
            break;

        case MTPERROR_DATABASE_INVALID_OBJECTGROUPCODE:
            wRespCode = MTP_RESPONSECODE_INVALIDOBJECTGROUPCODE;
            break;

        case MTPERROR_DATABASE_INVALID_OBJECTPROPCODE:
            wRespCode = MTP_RESPONSECODE_INVALIDOBJECTPROPCODE;
            break;

        case MTPERROR_DATABASE_WRITEPROTECTED_STORE:
            wRespCode = MTP_RESPONSECODE_STOREWRITEPROTECTED;
            break;

        case MTPERROR_DATABASE_WRITEPROTECTED_OBJECT:
            wRespCode = MTP_RESPONSECODE_OBJECTWRITEPROTECTED;
            break;

        case MTPERROR_DATABASE_PARTIAL_DELETION:
            wRespCode = MTP_RESPONSECODE_PARTIALDELETION;
            break;

        case MTPERROR_DATABASE_STORAGEFULL:
            wRespCode = MTP_RESPONSECODE_STOREFULL;
            break;

        case MTPERROR_DATABASE_INVALIDOBJECTPROPVALUE:
            wRespCode = MTP_RESPONSECODE_INVALIDOBJECTPROPVALUE;
            break;

        case MTPERROR_PROPERTY_INVALID_DEVICEPROPCODE:
            wRespCode = MTP_RESPONSECODE_INVALIDPROPERTYCODE;
            break;

        case MTPERROR_PROPERTY_INVALID_OBJECTPROPCODE:
            wRespCode = MTP_RESPONSECODE_OBJECTPROPNOTSUPPORTED;
            break;

        case MTPERROR_PROPERTY_INVALID_PROPVALUE:
            wRespCode = MTP_RESPONSECODE_INVALIDPROPVALUE;
            break;

        case MTPERROR_SERVICE_INVALID_SERVICEID:
            wRespCode = MTP_RESPONSECODE_INVALIDSERVICEID;
            break;

        case MTPERROR_SERVICE_INVALID_PROPCODE:
            wRespCode = MTP_RESPONSECODE_INVALIDSERVICEPROPCODE;
            break;

        case PSLERROR_INVALID_DATA:
            wRespCode = MTP_RESPONSECODE_INVALIDDATASET;
            break;

        default:
            wRespCode = MTP_RESPONSECODE_GENERALERROR;
            break;
        }
    }
    else
    {
        switch(dwStatus)
        {
        case PSLSUCCESS:
            wRespCode = MTP_RESPONSECODE_OK;
            break;
        default:
            {
                PSLASSERT(PSLFALSE);
                wRespCode = MTP_RESPONSECODE_GENERALERROR;
            }
            break;
        }
    }

    *pwRespCode = wRespCode;
    status = PSLSUCCESS;

Exit:
    return status;
}

PSLSTATUS PSL_API FillMessage(PSLMSG* pDesMsg, PSLMSG* pSrcMsg,
                      PSLUINT32 msgID, PSLBYTE* pbBuf,
                      PSLLPARAM cbBuf, PSLUINT32 dwFlags)
{
    PSLSTATUS status;

    if (PSLNULL == pDesMsg || PSLNULL == pSrcMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pDesMsg->msgID     =   msgID;
    pDesMsg->aParam0  =   pSrcMsg->aParam0;
    pDesMsg->aParam1  =   pSrcMsg->aParam1;
    pDesMsg->aParam2  =   (PSLPARAM)pbBuf;
    pDesMsg->aParam3  =   dwFlags;
    pDesMsg->alParam  =   cbBuf;

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS PSL_API FillResponse( PXMTPRESPONSE pResponse, PSLUINT16 wRespCode,
                            PSLUINT32 dwTransactionID,
                            PSLUINT32 dwParam1, PSLUINT32 dwParam2,
                            PSLUINT32 dwParam3, PSLUINT32 dwParam4,
                            PSLUINT32 dwParam5)
{
    PSLSTATUS status;

    if (PSLNULL == pResponse)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    MTPStoreUInt16(&(pResponse->wRespCode), wRespCode);
    MTPStoreUInt32(&(pResponse->dwTransactionID), dwTransactionID);
    MTPStoreUInt32(&(pResponse->dwParam1), dwParam1);
    MTPStoreUInt32(&(pResponse->dwParam2), dwParam2);
    MTPStoreUInt32(&(pResponse->dwParam3), dwParam3);
    MTPStoreUInt32(&(pResponse->dwParam4), dwParam4);
    MTPStoreUInt32(&(pResponse->dwParam5), dwParam5);

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS PSL_API AllocateReturnMsg(MTPDISPATCHER md, PSLMSG** ppMsg)
{
    PSLSTATUS   status;
    PSLMSGPOOL  mp;
    PSLMSG*     pRetMsg = PSLNULL;

    if (PSLNULL != ppMsg)
    {
        *ppMsg = PSLNULL;
    }

    if (PSLNULL == md || PSLNULL == ppMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* Allocate message to return to transport*/
    status = MTPDispatcherGetMsgPool(md, &mp);
    if (PSL_FAILED (status))
    {
        goto Exit;
    }

    status = PSLMsgAlloc(mp, &pRetMsg);
    if (PSL_FAILED (status))
    {
        goto Exit;
    }
    *ppMsg  = pRetMsg;
    pRetMsg = PSLNULL;
Exit:
    SAFE_PSLMSGFREE(pRetMsg);
    return status;
}

PSLSTATUS PSL_API GetReadyForNextCommand(MTPDISPATCHER md, MTPCONTEXT* pmc)
{
    PSLSTATUS           status;
    BASEHANDLERDATA*    pData;

    if (PSLNULL == pmc)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* set state to initialized */
    pmc->mtpHandlerState.dwHandlerState =
                                MTPHANDLERSTATE_INITIALIZED;

    pData = (BASEHANDLERDATA*)pmc->mtpHandlerState.pbHandlerData;

    /* clear handler specific data */
    PSLZeroMemory(&pData->opLevelData.mtpResponse,
                                            sizeof(MTPRESPONSE));
    pData->opLevelData.dwNumRespParams = 0;
    pData->opLevelData.dwObjectID = MTP_OBJECTHANDLE_UNDEFINED;

    /* reset the dispatcher */

    MTPDispatcherRouteEnd(md, pmc);

    /* <TODO> we may have to reset the DB dispatcher
     * we will do that when we start using DB diapatcher
     */

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS PSL_API MTPPreOpenLookupTableInit(PSLMSGQUEUE mqCore)
{
    PSLSTATUS status;
    PSLUINT32 idxOp;
    PSLLOOKUPTABLE plt = PSLNULL;

    if (PSLNULL == mqCore)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

#ifdef PSL_ASSERTS
    {
        static PSLBOOL fValidated = PSLFALSE;

        /*  Validate the tables if necessary
         */

        if (!fValidated)
        {
            _ValidateTable(_RgmhiiPreSession,
                            PSLARRAYSIZE(_RgmhiiPreSession));
            fValidated = PSLTRUE;
        }
    }
#endif  /* PSL_ASSERTS */

    /*  Handle the pre-open session table- start by sizing it
     *  appropriately to handle all of the commands specified
     *  and then add them
     */

    status = PSLLookupTableCreate(PSLARRAYSIZE(_RgmhiiPreSession),
                              &plt);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    for (idxOp = 0; idxOp < PSLARRAYSIZE(_RgmhiiPreSession); idxOp++)
    {
        status = PSLLookupTableInsertEntry(plt,
                    _RgmhiiPreSession[idxOp].wOpCode, (PSLPARAM)mqCore,
                    (PSLPARAM)_RgmhiiPreSession[idxOp].pfnDispatch);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    /* Let custom handlers initailize their pre-opensession
     * lookup table
     */

    status = MTPExtensionInitOps(plt, SESSIONSTATE_OUTSESSION,
                                 PSLNULL, 0);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Pre-Open lookup table initialization \
                     by a custom handler failed");
        status = PSLSUCCESS;
    }


    (PSLVOID)MTPLookupTablePreOpenSet(plt);
    plt = PSLNULL;

Exit:
    SAFE_PSLLOOKUPTABLEDESTROY(plt);
    return status;
}

PSLSTATUS PSL_API MTPPostOpenLookupTableInit(PSLMSGQUEUE mqCore,
                                     PSLMSGQUEUE mqDB,
                                     PSLMSGQUEUE mqDRM,
                                     PSLLOOKUPTABLE* pplt)
{
    PSLSTATUS status;
    PSLMSGQUEUE mq;
    PSLUINT32 cbDeviceInfo;
    PSLUINT32 cbWritten;
    PSLUINT32 cbRemaining;
    PSLUINT32 cOps;
    PSLUINT32 idxOp;
    PSL_CONST PSLUINT16 PSL_UNALIGNED* rgwOpList;
    PSLBYTE* pbDeviceInfo = PSLNULL;
    PSLLOOKUPTABLE plt = PSLNULL;
    MTPHANDLERINITINFO* pmhiiMatch;

    if (PSLNULL != pplt)
    {
        *pplt = PSLNULL;
    }

    if (PSLNULL == mqCore || PSLNULL == mqDB ||
        PSLNULL == mqDRM || PSLNULL == pplt)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

#ifdef PSL_ASSERTS
    {
        static PSLBOOL  fValidated = PSLFALSE;

        /*  Validate the tables if necessary
         */

        if (!fValidated)
        {
            _ValidateTable(_RgmhiiOpenSession,
                           PSLARRAYSIZE(_RgmhiiOpenSession));
            fValidated = PSLTRUE;
        }
    }
#endif  /* PSL_ASSERTS */

    /*  Retrieve the device info for this device
     */

    status = MTPGetDeviceInfoSerialize(PSLNULL, 0, PSLNULL,
                                   &cbDeviceInfo);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*  Allocate a buffer large enough to hold the device info
     */

    pbDeviceInfo = (PSLBYTE*)PSLMemAlloc(PSLMEM_FIXED, cbDeviceInfo);
    if (PSLNULL == pbDeviceInfo)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /*  And retrieve it
     */

    status = MTPGetDeviceInfoSerialize(pbDeviceInfo, cbDeviceInfo,
                                   &cbWritten, &cbRemaining);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    PSLASSERT((cbWritten == cbDeviceInfo) && (0 == cbRemaining));

    /*  Determine where the operation list starts- this is done
     *  by skipping the fields indicated:
     *  1. Standard Version (PSLUINT16)
     *  2. MTP Vendor Extension ID (PSLUINT32)
     *  3. MTP Version (PSLUINT16)
     *  4. MTP Extensions (PSLBYTE for length + string data)
     *  5. MTP Functional Mode (PSLUINT16)
     */

    rgwOpList = (PSLUINT16*)(pbDeviceInfo + sizeof(PSLUINT16) +
                                sizeof(PSLUINT32) +
                                sizeof(PSLUINT16) + sizeof(PSLBYTE) +
                                (pbDeviceInfo[8] * sizeof(PSLWCHAR)) +
                                sizeof(PSLUINT16));

    /*  Retrieve the number of operations indicated and size the lookup
     *  table
     */

    cOps = MTPLoadUInt32((PXPSLUINT32)rgwOpList);
    rgwOpList = (PSLUINT16*)((PSLBYTE*)rgwOpList + sizeof(PSLUINT32));

    /* One added to account for the MTPPostOpenSession handler
    */

    status = PSLLookupTableCreate(cOps + 1, &plt);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*  Load MTPPostOpenSession into the lookup table
     */

    status = PSLLookupTableInsertEntry(plt,
                        MTP_OPCODE_UNDEFINED, (PSLPARAM)mqCore,
                        (PSLPARAM)MTPPostOpenSession);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*  And walk through each of the operations specified in the
     *  device info to load them into the lookup table
     */

    for (idxOp = 0; idxOp < cOps; idxOp++)
    {
        /*  Locate the entry in our list to get the data necessary
         */

        status = PSLBSearch(
                (PSLVOID*)(PSLUINT32)MTPLoadUInt16(&(rgwOpList[idxOp])),
                _RgmhiiOpenSession, PSLARRAYSIZE(_RgmhiiOpenSession),
                sizeof(_RgmhiiOpenSession[0]), MTPLookupTableCompareOpCode,
                (PSLVOID**)&pmhiiMatch);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        if (PSLSUCCESS != status)
        {
            /*  Ignore unrecognized operations
             */
            continue;
        }

        PSLASSERT(MTPQ_MAX > pmhiiMatch->idxQueue);

        switch(pmhiiMatch->idxQueue)
        {
            case MTPQ_CORE:
                mq = mqCore;
                break;
            case MTPQ_DB:
                mq = mqDB;
                break;
            default:
                mq = PSLNULL;
                PSLASSERT(!L"Invalid queue index");
                break;
        }

        /*  Load the information into the lookup table
         */

        status = PSLLookupTableInsertEntry(plt, pmhiiMatch->wOpCode,
                    (PSLPARAM)mq, (PSLPARAM)pmhiiMatch->pfnDispatch);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    /* Let custom handlers initailize their post opensession
     * lookup table
     */

    status = MTPExtensionInitOps(plt, SESSIONSTATE_INSESSION,
                                 rgwOpList, cOps);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Post-Open lookup table initialization \
                     by a custom handler failed");
        goto Exit;
    }

    *pplt = plt;
    plt = PSLNULL;

    status = PSLSUCCESS;
Exit:
    SAFE_PSLLOOKUPTABLEDESTROY(plt);
    SAFE_PSLMEMFREE(pbDeviceInfo);
    return status;
}

PSLSTATUS PSL_API MTPPreOpenLookupTableUninit()
{
    PSLSTATUS status;
    PSLLOOKUPTABLE plt;

    /* Let custom handlers uninitailize the device property
     * lookup table
     */

    status = MTPLookupTableDevicePropGet(&plt);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Couldn't access the pre-open table");
        status = PSLSUCCESS;
    }

    status = MTPExtensionUninitDeviceProps(plt, SESSIONSTATE_OUTSESSION);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Device property lookup table uninitialization \
                     by a custom handler failed");
        status = PSLSUCCESS;
    }

    MTPLookupTablePreOpenDestroy();

    return status;
}

PSLSTATUS PSL_API MTPPostOpenLookupTableUninit(PSLLOOKUPTABLE plt)
{
    PSLSTATUS status;

    if (PSLNULL == plt)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    /* Let custom handlers uninitailize the post opensession
     * lookup table
     */
    status = MTPExtensionUninitOps(plt, SESSIONSTATE_INSESSION);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Post-Open lookup table initialization \
                     by a custom handler failed");
        status = PSLSUCCESS;
    }

    SAFE_PSLLOOKUPTABLEDESTROY(plt);
Exit:
    return status;
}

PSLSTATUS PSL_API MTPHandlerPropInitialize()
{
    PSLSTATUS status;
    PSLUINT32 cbDeviceInfo;
    PSLUINT32 cbWritten;
    PSLUINT32 cbRemaining;
    PSL_CONST PSLBYTE* pbWalker;
    PSLUINT32 cItems;
    PSL_CONST PSLUINT16 PSL_UNALIGNED* pwPropCodes;
    PSLUINT32 cdwPropCodes;
    PSLBYTE* pbDeviceInfo = PSLNULL;

    /*  Retrieve the device info for this device
     */

    status = MTPGetDeviceInfoSerialize(PSLNULL, 0, PSLNULL,
                                   &cbDeviceInfo);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*  Allocate a buffer large enough to hold the device info
     */

    pbDeviceInfo = (PSLBYTE*)PSLMemAlloc(PSLMEM_FIXED, cbDeviceInfo);
    if (PSLNULL == pbDeviceInfo)
    {
        status = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /*  And retrieve it
     */

    status = MTPGetDeviceInfoSerialize(pbDeviceInfo, cbDeviceInfo,
                                   &cbWritten, &cbRemaining);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }
    PSLASSERT((cbWritten == cbDeviceInfo) && (0 == cbRemaining));

    /*  Determine where the property list starts- this is done
     *  by skipping the fields indicated:
     *  1. Standard Version (PSLUINT16)
     *  2. MTP Vendor Extension ID (PSLUINT32)
     *  3. MTP Version (PSLUINT16)
     *  4. MTP Extensions (PSLBYTE for length + string data)
     *  5. MTP Functional Mode (PSLUINT16)
     *  6. MTP Operations (PSLUINT32 for count + list of opcodes)
     *  7. MTP Events (PSLUINT32 for count + list of event codes)
     */

    pbWalker = pbDeviceInfo + sizeof(PSLUINT16) +
                            sizeof(PSLUINT32) +
                            sizeof(PSLUINT16) + sizeof(PSLBYTE) +
                            (pbDeviceInfo[8] * sizeof(PSLWCHAR)) +
                            sizeof(PSLUINT16);

    /*  Skip MTP operations */

    pbWalker += sizeof(PSLUINT32) +
        (sizeof(PSLUINT16) * MTPLoadUInt32((PXPSLUINT32)pbWalker));

    /*  Skip MTP events */

    pbWalker += sizeof(PSLUINT32) +
        (sizeof(PSLUINT16) * MTPLoadUInt32((PXPSLUINT32)pbWalker));

    /*  Get device property codes */

    cdwPropCodes = MTPLoadUInt32((PXPSLUINT32)pbWalker);
    pwPropCodes = (PSLUINT16*)(pbWalker + sizeof(PSLUINT32));

    /*  Skip device property codes */

    pbWalker += sizeof(PSLUINT32) +
        (sizeof(PSLUINT16) * MTPLoadUInt32((PXPSLUINT32)pbWalker));

    /*  Skip capture formats */

    pbWalker += sizeof(PSLUINT32) +
        (sizeof(PSLUINT16) * MTPLoadUInt32((PXPSLUINT32)pbWalker));

    /*  Get playback formats */

    cItems = MTPLoadUInt32((PXPSLUINT32)pbWalker);
    pbWalker += sizeof(PSLUINT32);

    /*  Initialize the DB Session with the format codes supported on the device
     */

    status = MTPDBSessionInitialize((PSLUINT16*)pbWalker, cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Initialize the device property lookup table */

    status = _MTPDevicePropLookupTableInit(pwPropCodes, cdwPropCodes);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Device-Property lookup table creation failed");
        goto Exit;
    }

Exit:
    SAFE_PSLMEMFREE(pbDeviceInfo);
    return status;
}

PSLSTATUS PSL_API MTPHandlerPropUninitialize()
{
    PSLSTATUS status;

    status = _MTPDevicePropLookupTableUninit();
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Device-Prop lookup table Uninitialize failed");
        status = PSLSUCCESS;
    }

    MTPDBSessionUninitialize();

    return status;
}

PSLSTATUS _MTPDevicePropLookupTableInit(
        PCXPSLUINT16 pwCodes, PSLUINT32 cdwCodes)
{
    PSLSTATUS status;
    PSLLOOKUPTABLE plt = PSLNULL;

    /* Create device prop lookup table */
    status = PSLLookupTableCreate(cdwCodes, &plt);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*  Initialize the device properties
     */

    status = MTPDevicePropInitialize(plt, pwCodes, cdwCodes);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Let custom handlers initailize the device property
     * lookup table
     */

    status = MTPExtensionInitDeviceProps(plt, SESSIONSTATE_OUTSESSION,
                                   pwCodes, cdwCodes);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Device Property lookup table initialization \
                     by a custom handler failed");
        status = PSLSUCCESS;
    }

    MTPLookupTableDevicePropSet(plt);
    plt = PSLNULL;

Exit:
    SAFE_PSLLOOKUPTABLEDESTROY(plt);
    return status;
}

PSLSTATUS _MTPDevicePropLookupTableUninit()
{
    PSLSTATUS status;
    PSLLOOKUPTABLE plt;

    /* Let custom handlers uninitailize the pre opensession
     * lookup table
     */

    status = MTPLookupTablePreOpenGet(&plt);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Couldn't access the pre-open table");
    }

    status = MTPExtensionUninitOps(plt, SESSIONSTATE_OUTSESSION);
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Pre-Open lookup table uninitialization \
                     by a custom handler failed");
    }

    status = MTPDevicePropUninititialize();
    if (PSL_FAILED(status))
    {
        PSLASSERT(!L"Device property uninitialization failed");
    }

    MTPLookupTableDevicePropDestroy();

    return PSLSUCCESS;
}
