/*
 *  MTPMsgQueue.c
 *
 *  Contains the message queue implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"


PSLSTATUS PSL_API MTPMsgGetQueueParamThreaded(PSLVOID** ppvResult)
{
    PSLSTATUS status;
    PSLHANDLE hSignal = PSLNULL;

    if (PSLNULL != ppvResult)
    {
        *ppvResult = PSLNULL;
    }

    if (PSLNULL == ppvResult)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLSignalOpen(PSLFALSE, PSLFALSE, PSLNULL, &hSignal);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    *ppvResult = hSignal;
    hSignal = PSLNULL;

Exit:
    if (PSLNULL != hSignal)
    {
        PSLSignalClose(hSignal);
    }
    return status;
}

PSLSTATUS PSL_API MTPMsgReleaseQueueParamThreaded(PSLVOID* pvResult)
{
    PSLSTATUS status;

    if (PSLNULL == pvResult)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLSignalClose((PSLHANDLE)pvResult);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}

/*
 *
 *  This is the implementation of OnPost fucntion
 *  in a multi threaded implementation of MTP system.
 *  It will signal the object that was passed in during create
 *
 */
PSLSTATUS PSL_API MTPMsgOnPostThreaded(PSLVOID* pvParam)
{
    PSLSTATUS status;

    if (PSLNULL == pvParam)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLSignalSet((PSLHANDLE)pvParam);
    PSLASSERT(PSL_SUCCEEDED(status));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}

/*
 *
 *  This is the implementation of OnWait function
 *  for a multi threaded implementation of MTP system.
 *  It will wait on the object that was passed in during create
 *
 */
PSLSTATUS PSL_API MTPMsgOnWaitThreaded(PSLVOID* pvParam)
{
    PSLSTATUS status;

    if (PSLNULL == pvParam)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLSignalWait((PSLHANDLE)pvParam, PSLTHREAD_INFINITE);
    PSLASSERT(PSL_SUCCEEDED(status));
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*
     * This fucntion will always return following
     * success status which will allow queue to wait
     * till a message is available.
     *
     */
    status = PSLSUCCESS_BLOCKING;

Exit:
    return status;
}

PSLSTATUS PSL_API MTPMsgGetQueueParamSingleThreaded(PSLVOID** ppvResult)
{
    if (PSLNULL != ppvResult)
    {
        *ppvResult = PSLNULL;
    }
    return PSLSUCCESS;
}

PSLSTATUS PSL_API MTPMsgReleaseQueueParamSingleThreaded(
                                                    PSLVOID* pvResult)
{
    if (pvResult)
    {
        pvResult = PSLNULL;
    }
    return PSLSUCCESS;
}

/*
 * Implemenation of OnPost in a single threaded system
 *
 */
PSLSTATUS PSL_API MTPMsgOnPostSingleThreaded(PSLVOID* pvParam)
{
    PSLSTATUS status;

    if (PSLNULL == pvParam)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*
     * It is assumed that this call back function will call
     * dispatcher's core routine with dispatcher info
     *
     */
    status =
    ((MTPSINGLETHREADEDCALLBACK*)pvParam)->pfnSingleThreadedCallback(
                                                            pvParam);
Exit:
    return status;
}

/*
 *
 * Implemenation of OnWait in a single threaded system
 *
 */
PSLSTATUS PSL_API MTPMsgOnWaitSingleThreaded(PSLVOID* pvParam)
{
    if (pvParam)
    {
        pvParam = PSLNULL;
    }
    return PSLSUCCESS;
}

