/*
 *  MTPPerfLoggerLib.c
 *
 *  Contains implementation of MTP Performance Lib.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPPerfLogger.h"
#include "MTPPerfLogWriter.h"
#include "MTPPerfLoggerBufferQueue.h"

/* undefine the MACROs to avoid the functions to be renamed.
 */
#undef MTPPerfInitLogger
#undef MTPPerfDeinitLogger
#undef MTPPerfStartLog
#undef MTPPerfStopLog
#undef MTPPerfSetLogObject
#undef MTPPerfWriteLogRecord
#undef MTPPerfSetMTPContext
#undef MTPPerfSetLogFilter

#define DEVICE_EVENT_TRACE_HEADER_SIZE      24
#define MTPPERF_EVENT_HEADER_SIZE           8

#define MTPPERF_LOGGER_POOL_SIZE           4
#define MTPPERF_LOGRECORD_SIGNATURE        'RL'
#define MTPPERF_MAX_RECORD_NUM             0x00020000

enum
{
    MTPMSG_LOGGER_SHUTDOWN   =       MAKE_PSL_MSGID(MTP_PERFLOGGER, 1, 0),
    MTPMSG_LOGGER_WRITE      =       MAKE_PSL_MSGID(MTP_PERFLOGGER, 2, 0),
};

typedef PSLHANDLE   MTPLOGGER;

typedef struct _LOGGEROBJ
{
    PBUFFERQUEUE                 pEmptyList;
    PBUFFERQUEUE                 pPendingList;

    PLOGGERBUFFER                pCurBlock;

    PSLMSGQUEUE                  mqEvent;
    PSLMSGPOOL                   mpEvent;

    MTPPERLOGWRITER              mtpWriter;
    PSLUINT32                    RefCount;

    PSLUINT32                    dwTransactionID;
    PSLUINT16                    wOpcode;

    PSLHANDLE                    hMutex;

    PDEVICE_TRACE_FILE_HEADER    pTraceFileHeader;

} LOGGEROBJ, *PLOGGEROBJ;

static MTPLOGGER    g_hLogger = PSLNULL;

static MTPPERF_LOG_FILTER      g_wLogFilter = {MPF_FILTER_ALL, MFL_FILTER_ADMIN} ;

static PSLUINT32 PSL_API _MTPPerfLoggerWriteThread(
                            PSLVOID* pvParam);

/*
 *  _MTPPerfSendLoggerMessage
 *
 *  Helper function to send a message to LOGGER thread messsage queue
 *
 *  Arguments:
 *          PLOGGEROBJ      pLoggerObj      the handle of Logger instance
 *          PSLMSGID        msgID           message ID to be sent
 *          PSLPARAM        param           optianal parameter for for message
 *
 *  Returns:
 *
 */
static PSLVOID _MTPPerfSendLoggerMessage(PLOGGEROBJ pLoggerObj,
                                  PSLMSGID msgID, PSLPARAM param)
{
    PSLSTATUS                   ps;
    PSLMSG*                     pMsg = PSLNULL;

    ps = PSLMsgAlloc(pLoggerObj->mpEvent, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg->alParam = 0;
    pMsg->aParam0 = param;
    pMsg->aParam1 = 0;
    pMsg->aParam2 = 0;
    pMsg->aParam3 = 0;

    pMsg->msgID = msgID;

    ps = PSLMsgPost(pLoggerObj->mqEvent, pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg = PSLNULL;

Exit:
    return;
}

/*
 *  _MTPPerfAllocateBuffer
 *
 *  Helper function to allocate log buffer.
 *
 *  Arguments:
 *          PLOGGEROBJ      pLoggerObj      the handle of Logger instance
 *          PLOGGERBUFFER   *ppBuffer       pointer to hold returned buffer
 *
 *  Returns:
 *          NONE
 */

static PSLVOID _MTPPerfAllocateBuffer(PLOGGEROBJ pLoggerObj,
                               PLOGGERBUFFER* ppBuffer)
{
    PSLASSERT(PSLNULL != ppBuffer);
    PSLASSERT(PSLNULL != pLoggerObj->pEmptyList);

    MTPPerfDeQueue(pLoggerObj->pEmptyList, ppBuffer);

    if (PSLNULL == *ppBuffer)
    {
        *ppBuffer = (PLOGGERBUFFER)PSLMemAlloc(PSLMEM_PTR,
                                        sizeof(LOGGERBUFFER));
    }

    if (PSLNULL != *ppBuffer)
    {
        (*ppBuffer)->Offset = 0;
    }
}

/*
 *  _MTPPerfReleaseBuffer
 *
 *  Helper function to release log buffer.
 *
 *  Arguments:
 *          PLOGGEROBJ      pLoggerObj      the handle of Logger instance
 *          PLOGGERBUFFER   pBuffer         log buffer to be released
 *
 *  Returns:
 *          NONE
 */

static PSLVOID _MTPPerfReleaseBuffer(PLOGGEROBJ pLoggerObj,
                              PLOGGERBUFFER pBuffer)
{
    PSLASSERT(PSLNULL != pLoggerObj->pEmptyList);
    PSLASSERT(PSLNULL != pLoggerObj->pPendingList);

    if (MTPPerfGetQueueSize(pLoggerObj->pPendingList) <
        MTPPerfGetQueueSize(pLoggerObj->pEmptyList))
    {
        SAFE_PSLMEMFREE(pBuffer);
    }
    else
    {
        MTPPerfEnQueue(pLoggerObj->pEmptyList, pBuffer);
    }
}

/*
 *  MTPPerfInitLogger
 *
 *  Export function to initialize perf logger.
 *
 *  Arguments:
 *          NONE
 *
 *  Returns:
 *          NONE
 */

PSLVOID PSL_API MTPPerfInitLogger()
{
    PLOGGEROBJ      pLoggerObj;
    PSLSTATUS       ps;
    PSLHANDLE       hMutexRelease = PSLNULL;

    if (PSLNULL == g_hLogger)
    {
        pLoggerObj = (PLOGGEROBJ)PSLMemAlloc(PSLMEM_PTR,
                                sizeof(LOGGEROBJ));

        PSLASSERT(PSLNULL != pLoggerObj);
        if (PSLNULL == pLoggerObj)
        {
            goto Exit;
        }

        /*  Create a mutex to handle Logger synchronization issues
         */

        ps = PSLMutexOpen(0, PSLNULL, &(pLoggerObj->hMutex));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pLoggerObj->RefCount = 0;
        pLoggerObj->pCurBlock = PSLNULL;
        pLoggerObj->dwTransactionID = 0;
        pLoggerObj->wOpcode = 0;
        MTPPerfBufferQueueCreate(&pLoggerObj->pEmptyList);
        MTPPerfBufferQueueCreate(&pLoggerObj->pPendingList);

        MTPPerfLogWriterCreate(&(pLoggerObj->mtpWriter));

        g_hLogger = (MTPLOGGER)pLoggerObj;
    }
    else
    {
        pLoggerObj = (PLOGGEROBJ)g_hLogger;
    }

    pLoggerObj->RefCount++;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    return;
}

/*
 *  MTPPerfDeinitLogger
 *
 *  Export function to shutdown perf logger.
 *
 *  Arguments:
 *          NONE
 *
 *  Returns:
 *          NONE
 */

PSLVOID PSL_API MTPPerfDeinitLogger()
{
    PLOGGEROBJ      pLoggerObj = (PLOGGEROBJ)g_hLogger;
    PSLHANDLE       hMutexRelease = PSLNULL;

    if (PSLNULL == pLoggerObj)
    {
        goto Exit;
    }

    if (1 >= pLoggerObj->RefCount)
    {
        SAFE_MTPPERFLOGWRITERDESTROY(pLoggerObj->mtpWriter);
        SAFE_BUFFERQUEUEDESTROY(pLoggerObj->pEmptyList);
        SAFE_BUFFERQUEUEDESTROY(pLoggerObj->pPendingList);
        SAFE_PSLMUTEXCLOSE(pLoggerObj->hMutex);
        SAFE_PSLMEMFREE(pLoggerObj);

        g_hLogger = PSLNULL;
    }
    else
    {
        pLoggerObj->RefCount--;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    return;
}

/*
 *  MTPPerfInitLogger
 *
 *  Export function to start perf logger thread.
 *
 *  Arguments:
 *          NONE
 *
 *  Returns:
 *          NONE
 */

PSLVOID PSL_API MTPPerfStartLog()
{
    PSLSTATUS       ps;
    PSLVOID*        pvParam = PSLNULL;
    PLOGGEROBJ      pLoggerObj = (PLOGGEROBJ)g_hLogger;
    PSLHANDLE       hThreadLogger;
    PSLHANDLE       hMutexRelease = PSLNULL;

    if (PSLNULL == pLoggerObj)
    {
        goto Exit;
    }

    /*  Allocate our queue and pool
     */

    ps = PSLMsgPoolCreate(MTPPERF_LOGGER_POOL_SIZE, &(pLoggerObj->mpEvent));

    ps = MTPMsgGetQueueParam(&pvParam);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = PSLMsgQueueCreate(pvParam, MTPMsgOnPost, MTPMsgOnWait,
                           MTPMsgOnDestroy, &(pLoggerObj->mqEvent));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pvParam = PSLNULL;

    ps = PSLThreadCreate(_MTPPerfLoggerWriteThread, pLoggerObj, PSLFALSE,
                        PSLTHREAD_DEFAULT_STACK_SIZE, &hThreadLogger);

Exit:
    SAFE_MTPMSGRELEASEQUEUEPARAM(pvParam);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
}

/*
 *  MTPPerfStopLog
 *
 *  Export function to stop perf logger thread.
 *
 *  Arguments:
 *          NONE
 *
 *  Returns:
 *          NONE
 */

PSLVOID PSL_API MTPPerfStopLog()
{
    PLOGGEROBJ      pLoggerObj = (PLOGGEROBJ)g_hLogger;
    PSLHANDLE       hMutexRelease = PSLNULL;

    if (PSLNULL == pLoggerObj)
    {
        goto Exit;
    }

    if (PSLNULL != pLoggerObj->pCurBlock)
    {
        MTPPerfEnQueue(pLoggerObj->pPendingList, pLoggerObj->pCurBlock);
    }
    _MTPPerfSendLoggerMessage(pLoggerObj, MTPMSG_LOGGER_SHUTDOWN, 0);

    while(PSLNULL != pLoggerObj->mpEvent)
        PSLSleep(100);

    PSLQueryPerformanceCounter(&pLoggerObj->pTraceFileHeader->StopTimestamp);

    MTPPerfLogWriterClose(pLoggerObj->mtpWriter);
Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return;
}

/*
 *  MTPPerfSetLogObject
 *
 *  Export function to open or create an object to store the LOG.
 *
 *  Arguments:
 *          PSLCSTR     LogName     Log name to identify the log object.
 *
 *  Returns:
 *          NONE
 */

PSLVOID PSL_API MTPPerfSetLogObject(PSLCSTR LogName)
{
    PLOGGEROBJ      pLoggerObj = (PLOGGEROBJ)g_hLogger;
    PSLHANDLE       hMutexRelease = PSLNULL;

    if (PSLNULL == pLoggerObj)
    {
        goto Exit;
    }

    MTPPerfLogWriterOpen(pLoggerObj->mtpWriter,
                                           LogName,
                                           &pLoggerObj->pTraceFileHeader);


    PSLQueryPerformanceFrequency(&pLoggerObj->pTraceFileHeader->CPUFrequency);
    PSLQueryPerformanceCounter(&pLoggerObj->pTraceFileHeader->StartTimestamp);
    pLoggerObj->pTraceFileHeader->dwNumOfProcessors = 1;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return;
}

/*
 *  MTPPerfWriteLogRecord
 *
 *  Export function to write a log record to buffer.
 *
 *  Arguments:
 *        MTPPERFEVENTCODE  EventCode
 *        PSLUINT8          EventType
 *        PSLVOID           *pPayload
 *        PSLUINT16         PayloadLen
 *
 *  Returns:
 *          NONE
 */
PSLVOID PSL_API MTPPerfWriteLogRecord(
                     MTPPERFEVENTCODE EventCode,
                     PSLUINT8 EventType,
                     PSLVOID *pPayload,
                     PSLUINT16 PayloadLen)
{
    PLOGGEROBJ                 pLoggerObj = (PLOGGEROBJ)g_hLogger;
    PSLHANDLE                  hMutexRelease = PSLNULL;
    PLOGGERBUFFER              pBuffer;
    PSLUINT16                  wLen;
    PSLSTATUS                  ps;
    PSLUINT64                  CurTicks;
    PSLUINT8                   *pCurRec;
    PSLBOOL                    fSuccess = PSLFALSE;

    if (PSLNULL == pLoggerObj ||
        PSLNULL == pLoggerObj->pTraceFileHeader ||
        MTPPERF_MAX_RECORD_NUM < pLoggerObj->pTraceFileHeader->dwNumOfEvents)
    {
        goto Exit;
    }

    if (0 == pLoggerObj->dwTransactionID &&
        0 == pLoggerObj->wOpcode)
    {
        goto Exit;
    }

    /* Get performance tick counter
     */
    ps = PSLQueryPerformanceCounter(&CurTicks);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = PSLMutexAcquire(pLoggerObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pLoggerObj->hMutex;

    pLoggerObj->pTraceFileHeader->dwNumOfEvents++;

    wLen = PayloadLen +
           DEVICE_EVENT_TRACE_HEADER_SIZE +
           MTPPERF_EVENT_HEADER_SIZE;

    pBuffer = pLoggerObj->pCurBlock;
    if (LOGGER_BUFFER_SIZE < wLen)
    {
        /* record exceeds the maximum length
         */
        goto Exit;
    }

    if (PSLNULL != pBuffer)
    {
        if (LOGGER_BUFFER_SIZE < pBuffer->Offset + wLen )
        {
            PSLASSERT(PSLNULL != pLoggerObj->pPendingList);
            if (PSLNULL != pLoggerObj->pPendingList)
            {
                /* Enqueue the writing request
                  */
                if (1 >= MTPPerfEnQueue(pLoggerObj->pPendingList, pBuffer))
                {
                    _MTPPerfSendLoggerMessage(pLoggerObj,
                                              MTPMSG_LOGGER_WRITE, 0);
                }
            }
            pLoggerObj->pCurBlock = PSLNULL;
        }
    }

    if (PSLNULL == pLoggerObj->pCurBlock)
    {
        _MTPPerfAllocateBuffer(pLoggerObj, &(pLoggerObj->pCurBlock));
        pBuffer = pLoggerObj->pCurBlock;
    }

    if (PSLNULL != pBuffer)
    {
        pCurRec = (PSLUINT8 *)&(pBuffer->Data[pBuffer->Offset]);

        /* Writing DEVICE_EVENT_TRACE_HEADER
         */
        MTPStoreUInt16((PSLUINT16 *)&pCurRec[0], MTPPERF_LOGRECORD_SIGNATURE);
        MTPStoreUInt16((PSLUINT16 *)&pCurRec[2],
            (PSLUINT16)pLoggerObj->pTraceFileHeader->dwNumOfEvents);
        MTPStoreUInt64((PSLUINT64 *)&pCurRec[4], &CurTicks);
        MTPStoreUInt32((PSLUINT32 *)&pCurRec[12], 0);
        MTPStoreUInt16((PSLUINT16 *)&pCurRec[16], wLen);
        MTPStoreUInt16((PSLUINT16 *)&pCurRec[18], 0);
        MTPStoreUInt16((PSLUINT16 *)&pCurRec[20], EventCode);
        pCurRec[22] = EventType;
        pCurRec[23] = 0;

        /* Writing MTPPERF_EVENT_HEADER
         */
        MTPStoreUInt32((PSLUINT32 *)&pCurRec[24], pLoggerObj->dwTransactionID);
        MTPStoreUInt16((PSLUINT16 *)&pCurRec[28], pLoggerObj->wOpcode);
        MTPStoreUInt16((PSLUINT16 *)&pCurRec[30], 0);

        if (0 < PayloadLen)
        {
            PSLCopyMemory(&pCurRec[32], pPayload, PayloadLen);
        }

        pBuffer->Offset += wLen;

        fSuccess = PSLTRUE;
    }

    if (! fSuccess)
    {
        pLoggerObj->pTraceFileHeader->dwEventLost++;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return;
}

/*
 *  MTPPerfSetMTPContext
 *
 *  Export function to persist transaction ID and operation code.
 *
 *  Arguments:
 *          PSLUINT32           dwTransactionID
 *          PSLUINT16           wOpcode
 *
 *  Returns:
 *          NONE
 */
PSLVOID PSL_API MTPPerfSetMTPContext(PSLUINT32 dwTransactionID,
                                     PSLUINT16 wOpcode)
{
    PLOGGEROBJ              pLoggerObj = (PLOGGEROBJ)g_hLogger;
    PSLHANDLE               hMutexRelease = PSLNULL;

    if (PSLNULL == pLoggerObj)
    {
        goto Exit;
    }
    pLoggerObj->dwTransactionID = dwTransactionID;
    pLoggerObj->wOpcode = wOpcode;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return;
}

/*
 *  _MTPPerfLoggerWriteThread
 *
 *  Main function for the Perf Logger thread.
 *
 *  Arguments:
 *          PSLVOID*    pvThreadParam           Perf Logger pointer
 *                                              to create thuis thread
 *
 *  Returns:
 *          PSLUINT32   exit code of the thread.
 */
PSLUINT32 PSL_API _MTPPerfLoggerWriteThread(PSLVOID* pvThreadParam)
{
    PLOGGEROBJ          pLoggerObj = PSLNULL;
    PSLBOOL             fDone;
    PSLMSG*             pMsg = PSLNULL;
    PSLSTATUS           ps = 0;
    PSLUINT32           cItems;
    PLOGGERBUFFER       pBuffer;

    /*  Validate arguments
     */

    if (PSLNULL == pvThreadParam)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    pLoggerObj = (PLOGGEROBJ)pvThreadParam;

    /*  Start looping to handle messages
     */

    fDone = PSLFALSE;
    while (!fDone)
    {
        /*  Release any existing message and buffer that we received
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  And wait for the next message to be recieved
         */

        ps = PSLMsgGet(pLoggerObj->mqEvent, &pMsg);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Determine what we need to do
         */

        switch (pMsg->msgID)
        {
        case MTPMSG_LOGGER_SHUTDOWN:
            fDone = PSLTRUE;
            /* fall intentionally, write the log buffer upon shuting down
             */
        case MTPMSG_LOGGER_WRITE:
            cItems = 1;
            while(0 < cItems)
            {
                PSLASSERT(PSLNULL != pLoggerObj->pPendingList);

                cItems = MTPPerfGetQueueSize(pLoggerObj->pPendingList);
                if (0 < cItems)
                {
                    MTPPerfDeQueue(pLoggerObj->pPendingList, &pBuffer);

                    if (PSLNULL != pBuffer)
                    {
                        MTPPerfLogWriterWriteBuffer(pLoggerObj->mtpWriter,
                                            pBuffer->Data, pBuffer->Offset);
                        _MTPPerfReleaseBuffer(pLoggerObj, pBuffer);
                    }
                }
            }
            break;
        default:
            PSLASSERT(PSLFALSE);
            break;
        }
    }

Exit:
    SAFE_PSLMSGFREE(pMsg);
    SAFE_PSLMSGQUEUEDESTROY(pLoggerObj->mqEvent);
    SAFE_PSLMSGPOOLDESTROY(pLoggerObj->mpEvent);

    return ps;
}

/*
 *  MTPPerfSetLogFilter
 *
 *  Set Perf Logger filter.
 *
 *  Arguments:
 *          PSLUINT8 FilterFlag  Flags indicate that a layer is enabled for Perf logging
 *          PSLUINT8 FilterLevel Perf logging level
 *
 *  Returns:
 *          NONE.
 */
PSLVOID PSL_API MTPPerfSetLogFilter(PSLUINT8 FilterFlag, PSLUINT8 FilterLevel)
{
    g_wLogFilter.chFlag = FilterFlag;
    g_wLogFilter.chLevel = FilterLevel;
}

/*
 *  MTPPerfIsLogEnabled
 *
 *  Check Logger filter if the layer is enabled for perf logging.
 *
 *  Arguments:
 *          PSLUINT8 FilterFlag  Perf logging flags to be checked
 *          PSLUINT8 FilterLevel Perf logging level to be checked
 *
 *  Returns:
 *          NONE.
 */
PSLSTATUS PSL_API MTPPerfIsLogEnabled(PSLUINT8 FilterFlag, PSLUINT8 FilterLevel)
{
    return (0 != (g_wLogFilter.chFlag & FilterFlag) &&
            g_wLogFilter.chLevel >= FilterLevel)
            ? PSLSUCCESS
            : PSLSUCCESS_FALSE;
}
