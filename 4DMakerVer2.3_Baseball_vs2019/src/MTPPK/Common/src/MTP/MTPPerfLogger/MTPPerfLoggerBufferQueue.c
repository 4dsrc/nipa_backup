/*
 *  MTPPerfLoggerBufferQueue.c
 *
 *  Contains implementation of buffer management for MTP
 *  Performance Logger.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "PSL.h"
#include "MTPPerfLoggerBufferQueue.h"

/*
 *  MTPPerfBufferQueueCreate
 *
 *  Create buffer queue manager object.
 *
 *  Arguments:
 *          PBUFFERQUEUE    *ppBufferQueue  pointer to hole returned buffer queue
 *                                          object handle
 *
 *  Returns:
 *
 */
PSLVOID MTPPerfBufferQueueCreate(PBUFFERQUEUE *ppBufferQueue)
{
    PBUFFERQUEUE    pBufferQueue = PSLNULL;
    PSLSTATUS       ps;

    if (PSLNULL == ppBufferQueue)
    {
        goto Exit;
    }

    pBufferQueue = (PBUFFERQUEUE)PSLMemAlloc(PSLMEM_PTR,
                            sizeof(BUFFERQUEUE));

    PSLASSERT(PSLNULL != pBufferQueue);
    if (PSLNULL == pBufferQueue)
    {
        goto Exit;
    }

    /*  Create a mutex to handle Logger buffer management issues
     */

    ps = PSLMutexOpen(0, PSLNULL, &(pBufferQueue->hMutex));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    pBufferQueue->dwNumElem= 0;
    pBufferQueue->Head = PSLNULL;
    pBufferQueue->Tail = PSLNULL;

    *ppBufferQueue = pBufferQueue;
    pBufferQueue = PSLNULL;
Exit:
    return;
}

/*
 *  MTPPerfBufferQueueDestroy
 *
 *  Destroy buffer queue manager object.
 *
 *  Arguments:
 *          PBUFFERQUEUE    pBufferQueue  buffer queue object handle
 *
 *  Returns:
 *
 */
PSLVOID MTPPerfBufferQueueDestroy(PBUFFERQUEUE pBufferQueue)
{
    PLOGGERBUFFER   pLoggerBuffer = PSLNULL;
    PSLUINT32       cItems;

    if (PSLNULL == pBufferQueue)
    {
        goto Exit;
    }

    do
    {
        cItems = MTPPerfDeQueue(pBufferQueue, &pLoggerBuffer);
        SAFE_PSLMEMFREE(pLoggerBuffer);
    } while(0 < cItems);

    PSLASSERT(0 == pBufferQueue->dwNumElem);
    PSLASSERT(PSLNULL == pBufferQueue->Head);
    PSLASSERT(PSLNULL == pBufferQueue->Tail);

    SAFE_PSLMUTEXCLOSE(pBufferQueue->hMutex);

Exit:
    SAFE_PSLMEMFREE(pBufferQueue);
}

/*
 *  MTPPerfBufferQueueDestroy
 *
 *  retrieve queue sisze
 *
 *  Arguments:
 *          PBUFFERQUEUE    pBufferQueue    buffer queue object handle
 *
 *  Returns:
 *          PSLUINT32       size of buffer list.
 */

PSLUINT32 MTPPerfGetQueueSize(PBUFFERQUEUE pBufferQueue)
{
    PSLUINT32               dwResult = 0;
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLSTATUS               ps;

    ps = PSLMutexAcquire(pBufferQueue->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pBufferQueue->hMutex;


    dwResult = pBufferQueue->dwNumElem;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    return dwResult;
}

/*
 *  MTPPerfEnQueue
 *
 *  Put a buffer into buffer queue.
 *
 *  Arguments:
 *          PBUFFERQUEUE    pBufferQueue  buffer queue object handle
 *          PLOGGERBUFFER   pBuffer       log buffer to put in the list.
 *  Returns:
 *
 */

PSLUINT32 MTPPerfEnQueue(PBUFFERQUEUE pBufferQueue, PLOGGERBUFFER pBuffer)
{
    PSLUINT32               dwResult = 0;
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLSTATUS               ps;

    ps = PSLMutexAcquire(pBufferQueue->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pBufferQueue->hMutex;

    if (PSLNULL == pBufferQueue->Head)
    {
        pBufferQueue->Tail = pBuffer;
    }
    else
    {
        pBufferQueue->Head->Next = pBuffer;
    }
    pBufferQueue->dwNumElem++;
    pBufferQueue->Head = pBuffer;

    dwResult = pBufferQueue->dwNumElem;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    return dwResult;
}

/*
 *  MTPPerfDnQueue
 *
 *  Get a buffer from the buffer queue.
 *
 *  Arguments:
 *          PBUFFERQUEUE    pBufferQueue  buffer queue object handle
 *          PLOGGERBUFFER   *ppBuffer     pointer to hold the returned buffer.
 *  Returns:
 *
 */

PSLUINT32 MTPPerfDeQueue(PBUFFERQUEUE pBufferQueue, PLOGGERBUFFER* ppBuffer)
{
    PSLUINT32               dwResult = 0;
    PLOGGERBUFFER           pTemp = pBufferQueue->Tail;
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLSTATUS               ps;

    ps = PSLMutexAcquire(pBufferQueue->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pBufferQueue->hMutex;

    if (PSLNULL != pBufferQueue->Tail)
    {
        pBufferQueue->Tail = pTemp->Next;
        if (PSLNULL == pBufferQueue->Tail)
        {
            pBufferQueue->Head = PSLNULL;
        }
        pBufferQueue->dwNumElem--;
    }
    *ppBuffer = pTemp;

    dwResult = pBufferQueue->dwNumElem;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);

    return dwResult;
}

