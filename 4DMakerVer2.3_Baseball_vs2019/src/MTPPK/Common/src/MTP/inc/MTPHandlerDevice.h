/*
 *  MTPHandlerProp.h
 *
 *  Contains declaration for the device level handler functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPHANDLERDEVICE_H_
#define _MTPHANDLERDEVICE_H_

/*
 *  MTPResetDevice
 *
 *  This handler function for ResetDevice MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPResetDevice(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

/*
 *  MTPSelfTest
 *
 *  This handler function for SelfTest MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSelfTest(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

/*
 *  MTPPowerDown
 *
 *  This handler function for PowerDown MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPPowerDown(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

/*
 *  MTPSkip
 *
 *  This handler function for Skip MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSkip(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

/*
 *  MTPInitiateCapture
 *
 *  This handler function for InitiateCapture MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPInitiateCapture(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

/*
 *  MTPInitiateOpenCapture
 *
 *  This handler function for InitiateOpenCapture MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPInitiateOpenCapture(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

/*
 *  MTPTerminateOpenCapture
 *
 *  This handler function for TerminateOpenCapture MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPTerminateOpenCapture(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

#endif /*_MTPHANDLERDEVICE_H_*/

