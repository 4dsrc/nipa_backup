/*
 *  MTPStreamParser.h
 *
 *  Contains declaration of the different MTP Stream parsers and constants.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPSTREAMPARSER_H_
#define _MTPSTREAMPARSER_H_

#define MTPSTREAM_MAX_DATA_PACKET           0x7fffffff
#define MTPSTREAM_MAX_CHUNK_SIZE            0xfe00

#define MTPSTREAMPARSERFLAGS_PUBLIC_MASK    0x0000ffff

#define MTPSTREAMPARSERFLAGS_PARSER_READY   0x00000001

typedef PSLHANDLE MTPSTREAMPARSER;


#ifdef PSL_PACK_PRAGMA
#include "PSLPackPush1.h"
#endif

/*  MTPSTREAMHEADER
 *
 *  Standard header for all MTP Stream packets
 */

struct _MTPSTREAMHEADER
{
    PSLUINT32               cbPacket;
    PSLUINT8                bPacketID;
    PSLUINT8                bTransportID;
    PSLUINT16               wTransportData;
} PSL_PACK1;

typedef struct _MTPSTREAMHEADER MTPSTREAMHEADER;

typedef struct _MTPSTREAMHEADER PSL_UNALIGNED* PXMTPSTREAMHEADER;
typedef PSL_CONST struct _MTPSTREAMHEADER PSL_UNALIGNED* PCXMTPSTREAMHEADER;

#define GET_PACKET_START(_mtpstrm) \
        ((PSLBYTE*)&(_mtpstrm) + sizeof(MTPSTREAMHEADER))

#define GET_PACKET_SIZE(_mtpstrm) \
        (sizeof(_mtpstrm) - sizeof(MTPSTREAMHEADER))


/*  MTPSTREAMDATA
 *
 *  Standard header for all MTP Stream Data packets
 */

struct _MTPSTREAMDATA
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwTransactionID;
    /*  Data follows immediately */
} PSL_PACK1;

typedef struct _MTPSTREAMDATA MTPSTREAMDATA;

typedef struct _MTPSTREAMDATA PSL_UNALIGNED* PXMTPSTREAMDATA;
typedef PSL_CONST struct _MTPSTREAMDATA PSL_UNALIGNED* PCXMTPSTREAMDATA;

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPop.h"
#endif


/*  MTP Stream Packet Types
 *
 *  Type enumeration for MTP Stream packets- note that sub-classed
 *  implementations may add their own types
 */

enum
{
    MTPSTREAMPACKET_UNKNOWN = 0,
    MTPSTREAMPACKET_OPERATION_REQUEST,
    MTPSTREAMPACKET_OPERATION_RESPONSE,
    MTPSTREAMPACKET_EVENT,
    MTPSTREAMPACKET_START_DATA,
    MTPSTREAMPACKET_DATA,
    MTPSTREAMPACKET_CANCEL,
    MTPSTREAMPACKET_END_DATA,
    MTPSTREAMPACKET_PROBE_REQUEST,
    MTPSTREAMPACKET_PROBE_RESPONSE,
    MTPSTREAMPACKET_SUBCLASS_BEGIN
};


/*  MTP Stream Data Phase
 *
 *  Data phases defined in the MTP Stream transport model
 */

enum
{
    MTPSTREAM_DATA_PHASE_UNKNOWN =      0x00000000,
    MTPSTREAM_NO_DATA_OR_DATA_IN =      0x00000001,
    MTPSTREAM_DATA_OUT_PHASE =          0x00000002
};


/*  MTP Stream Parser
 *
 *  This is the core parser used for pulling MTP packetized data out of a
 *  data stream.  It is not complete- "sub-classes" of this parser should
 *  be created to handle transport specific operations.
 */

/*  MTP Stream Parser Virtual Functions
 *
 *  Each of the function prototypes below defines a virtualized parser function
 *  that is either provided by the base implementation or a sub-class.
 *  Functions that are called out as PURE must be implemented in the sub-class.
 *
 */

/*  pfnMTPStreamParserGetBytesAvailable - PURE
 *
 *  Returns the number of bytes available to be parsed
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERGETBYTESAVAILABLE)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pcbAvailable);


/*  pfnMTPStreamParserRead - PURE
 *
 *  Returns the specified number of bytes from the stream.
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERREAD)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLVOID* pvDest,
                    PSLUINT32 cbDest,
                    PSLUINT32* pcbRead);


/*  pfnMTPStreamParserSend - PURE
 *
 *  Sends the specified number of bytes to the stream
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSEND)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLVOID* pvDest,
                    PSLUINT32 cbDest,
                    PSLUINT32* pcbRead);


/*  pfnMTPStreamParserParseHeader - PURE
 *
 *  Parses the header in a stream specific manner
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERPARSEHEADER)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPSTREAMHEADER pmtpstrmHdr,
                    PSLUINT32* pdwPacketType,
                    PSLUINT32* pcbPacketSize);


/*  pfnMTPStreamParserStoreHeader - PURE
 *
 *  Stores information in the header in a stream specific manner
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSTOREHEADER)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwPacketType,
                    PSLUINT32 cbPacketSize,
                    PXMTPSTREAMHEADER pmtpstrmHdr);

/*  pfnMTPStreamParserGetDataHeader
 *
 *  Returns a pointer to the start of the MTP Stream Data header
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERGETDATAHEADER)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSL_CONST PSLVOID* pvData,
                    PXMTPSTREAMDATA* ppstrmHeader,
                    PSLUINT32* pcbHeader);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserGetDataHeaderBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSL_CONST PSLVOID* pvData,
                    PXMTPSTREAMDATA* ppstrmHeader,
                    PSLUINT32* pcbHeader);


/*  pfnMTPStreamParserDestroy
 *
 *  Destroys an instance of the stream parsrer
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERDESTROY)(
                    MTPSTREAMPARSER mtpstrmParser);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserDestroyBase(
                    MTPSTREAMPARSER mtpstrmParser);


/*  pfnMTPStreamParserSetType
 *
 *  Sets the type of parser
 */

enum
{
    MTPSTREAMPARSERTYPE_UNKNOWN = 0,
    MTPSTREAMPARSERTYPE_ANY,
    MTPSTREAMPARSERTYPE_COMMAND,
    MTPSTREAMPARSERTYPE_EVENT,
};

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSETTYPE)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwType);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserSetTypeBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwType);


/*  pfnMTPStreamParserGetParserInfo
 *
 *  Retrieves information about the current parser
 */

typedef struct _MTPSTREAMPARSERINFO
{
    PSLUINT32               dwParserMode;
    PSLUINT32               dwParserType;
} MTPSTREAMPARSERINFO;

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERGETPARSERINFO)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLVOID* pmtpstrmInfo,
                    PSLUINT32 cbInfo);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserGetParserInfoBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLVOID* pmtpstrmInfo,
                    PSLUINT32 cbInfo);


/*  pfnMTPStreamParserReset
 *
 *  Resets the parser to its initial state
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERRESET)(
                    MTPSTREAMPARSER mtpstrmParser);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserResetBase(
                    MTPSTREAMPARSER mtpstrmParser);

/*  pfnMTPStreamParserSendOpRequest
 *
 *  Sends an operation request - Initiator only
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSENDOPREQUEST)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPOPERATION pmtpOp);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserSendOpRequestBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPOPERATION pmtpOp);


/*  pfnMTPStreamParserReadOpRequest
 *
 *  Reads an operation request - Responder only
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERREADOPREQUEST)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwDataPhase,
                    PXMTPOPERATION pmtpOp);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserReadOpRequestBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwDataPhase,
                    PXMTPOPERATION pmtpOp);

/*  pfnMTPStreamParserSendOpResponse
 *
 *  Sends an operation response - Responder only
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSENDOPRESPONSE)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPRESPONSE pmtpResponse,
                    PSLUINT32 cbResponse);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserSendOpResponseBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPRESPONSE pmtpResponse,
                    PSLUINT32 cbResponse);


/*  pfnMTPStreamParserReadOpResponse
 *
 *  Reads an operation response - Initiator only
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERREADOPRESPONSE)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PXMTPRESPONSE pmtpResponse);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserReadOpResponseBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PXMTPRESPONSE pmtpResponse);


/*  pfnMTPStreamParserSendEvent
 *
 *  Sends an event - Responder only
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSENDEVENT)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPEVENT pmtpEvent,
                    PSLUINT32 cbEvent);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserSendEventBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPEVENT pmtpEvent,
                    PSLUINT32 cbEvent);


/*   pfnMTPStreamParserReadEvent
 *
 *  Reads an event - Initiator only
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERREADEVENT)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PXMTPEVENT pmtpEvent);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserReadEventBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PXMTPEVENT pmtpEvent);


/*   pfnMTPStreamParserSendStartData
 *
 *  Sends a start data packet - Initiator and Responder
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSENDSTARTDATA)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID,
                    PSLUINT64 qwTotalSize);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserSendStartDataBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID,
                    PSLUINT64 qwTotalSize);


/*  pfnMTPStreamParserReadStartData
 *
 *  Reads a start data packet - Initiator and Responder
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERREADSTARTDATA)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID,
                    PSLUINT64* pqwTotalSize);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserReadStartDataBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID,
                    PSLUINT64* pqwTotalSize);


/*  pfnMTPStreamParserSendData
 *
 *  Sends a data packet - Initiator and Responder
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSENDDATA)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID,
                    PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserSendDataBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID,
                    PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData);


/*  pfnMTPStreamParserReadData
 *
 *  Reads a data packet - Initator and Responder
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERREADDATA)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID,
                    PSLVOID* pvData,
                    PSLUINT32 cbMaxData,
                    PSLUINT32* cbRead);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserReadDataBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID,
                    PSLVOID* pvData,
                    PSLUINT32 cbMaxData,
                    PSLUINT32* pcbRead);


/*  pfnMTPStreamParserSendEndData
 *
 *  Sends an end data packet - Initiator and Responder
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSENDENDDATA)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID,
                    PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserSendEndDataBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID,
                    PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData);


/*  pfnMTPStreamParserReadEndData
 *
 *  Reads an end data packet - Initiator and Responder
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERREADENDDATA)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID,
                    PSLVOID* pvData,
                    PSLUINT32 cbMaxData,
                    PSLUINT32* cbRead);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserReadEndDataBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID,
                    PSLVOID* pvData,
                    PSLUINT32 cbMaxData,
                    PSLUINT32* pcbRead);


/*  pfnMTPStreamParserSendCancel
 *
 *  Sends a cancel request - Initiator only
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSENDCANCEL)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserSendCancelBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID);


/*  pfnMTPStreamParserReadCancel
 *
 *  Reads a cancel request - Responder only
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERREADCANCEL)(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserReadCancelBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID);


/*  pfnMTPStreamParserSendProbeRequest
 *
 *  Sends a probe request - Initiator and Responder
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSENDPROBEREQUEST)(
                        MTPSTREAMPARSER mtpstrmParser);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserSendProbeRequestBase(
                        MTPSTREAMPARSER mtpstrmParser);


/*  pfnMTPStreamParserReadProbeRequest
 *
 *  Reads a probe request - Initiator and Responder
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERREADPROBEREQUEST)(
                        MTPSTREAMPARSER mtpstrmParser);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserReadProbeRequestBase(
                        MTPSTREAMPARSER mtpstrmParser);


/*  pfnMTPStreamParserSendProbeResponse
 *
 *  Sends a probe response - Initator and Responder
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERSENDPROBERESPONSE)(
                        MTPSTREAMPARSER mtpstrmParser,
                        PSLUINT32 dwDeviceStatus);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserSendProbeResponseBase(
                        MTPSTREAMPARSER mtpstrmParser,
                        PSLUINT32 dwDeviceStatus);


/*  pfnMTPStreamParserReadProbeResponse
 *
 *  Reads a probe response - Initiator and Responder
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMPARSERREADPROBERESPONSE)(
                        MTPSTREAMPARSER mtpstrmParser);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserReadProbeResponseBase(
                        MTPSTREAMPARSER mtpstrmParser);


/*  MTPSTREAMPARSERVTBL
 *
 *  Virtual Function Table for the stream parser object
 */

typedef struct _MTPSTREAMPARSERVTBL
{
    PFNMTPSTREAMPARSERGETBYTESAVAILABLE pfnMTPStreamParserGetBytesAvailable;
    PFNMTPSTREAMPARSERREAD              pfnMTPStreamParserRead;
    PFNMTPSTREAMPARSERSEND              pfnMTPStreamParserSend;
    PFNMTPSTREAMPARSERPARSEHEADER       pfnMTPStreamParserParseHeader;
    PFNMTPSTREAMPARSERSTOREHEADER       pfnMTPStreamParserStoreHeader;
    PFNMTPSTREAMPARSERGETDATAHEADER     pfnMTPStreamParserGetDataHeader;
    PFNMTPSTREAMPARSERDESTROY           pfnMTPStreamParserDestroy;
    PFNMTPSTREAMPARSERSETTYPE           pfnMTPStreamParserSetType;
    PFNMTPSTREAMPARSERGETPARSERINFO     pfnMTPStreamParserGetParserInfo;
    PFNMTPSTREAMPARSERRESET             pfnMTPStreamParserReset;
#ifdef MTP_INITIATOR
    PFNMTPSTREAMPARSERSENDOPREQUEST     pfnMTPStreamParserSendOpRequest;
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    PFNMTPSTREAMPARSERREADOPREQUEST     pfnMTPStreamParserReadOpRequest;
    PFNMTPSTREAMPARSERSENDOPRESPONSE    pfnMTPStreamParserSendOpResponse;
#endif  /* MTP_RESPONDER */
#ifdef MTP_INITIATOR
    PFNMTPSTREAMPARSERREADOPRESPONSE    pfnMTPStreamParserReadOpResponse;
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    PFNMTPSTREAMPARSERSENDEVENT         pfnMTPStreamParserSendEvent;
#endif  /* MTP_RESPONDER */
#ifdef MTP_INITIATOR
    PFNMTPSTREAMPARSERREADEVENT         pfnMTPStreamParserReadEvent;
#endif  /* MTP_INITIATOR */
    PFNMTPSTREAMPARSERSENDSTARTDATA     pfnMTPStreamParserSendStartData;
    PFNMTPSTREAMPARSERREADSTARTDATA     pfnMTPStreamParserReadStartData;
    PFNMTPSTREAMPARSERSENDDATA          pfnMTPStreamParserSendData;
    PFNMTPSTREAMPARSERREADDATA          pfnMTPStreamParserReadData;
    PFNMTPSTREAMPARSERSENDENDDATA       pfnMTPStreamParserSendEndData;
    PFNMTPSTREAMPARSERREADENDDATA       pfnMTPStreamParserReadEndData;
#ifdef MTP_INITIATOR
    PFNMTPSTREAMPARSERSENDCANCEL        pfnMTPStreamParserSendCancel;
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    PFNMTPSTREAMPARSERREADCANCEL        pfnMTPStreamParserReadCancel;
#endif  /* MTP_RESPONDER */
    PFNMTPSTREAMPARSERSENDPROBEREQUEST  pfnMTPStreamParserSendProbeRequest;
    PFNMTPSTREAMPARSERREADPROBEREQUEST  pfnMTPStreamParserReadProbeRequest;
    PFNMTPSTREAMPARSERSENDPROBERESPONSE pfnMTPStreamParserSendProbeResponse;
    PFNMTPSTREAMPARSERREADPROBERESPONSE pfnMTPStreamParserReadProbeResponse;
} MTPSTREAMPARSERVTBL;


/*  MTPSTREAMPARSEROBJ
 *
 *  Core format for the parser object- this data should be treated as
 *  read-only by all but transport specific "sub-class" parsers.
 *
 */

typedef struct _MTPSTREAMPARSEROBJ
{
    PSL_CONST MTPSTREAMPARSERVTBL*  vtbl;
#ifdef PSL_ASSERTS
    PSLUINT32                       dwCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT32                       dwFlags;
    PSLUINT32                       dwTypeCur;
    PSLUINT32                       cbSizeCur;
    PSLUINT32                       cbAvailable;
    PSLBOOL                         fFirstChunk;
} MTPSTREAMPARSEROBJ;


typedef PSL_CONST MTPSTREAMPARSEROBJ* PMTPSTREAMPARSEROBJ;


/*  MTP Stream Parser Creation
 */

enum
{
    MTPSTREAMPARSERMODE_UNKNOWN = 0,
    MTPSTREAMPARSERMODE_ANY,
    MTPSTREAMPARSERMODE_RESPONDER,
    MTPSTREAMPARSERMODE_INITIATOR,
};

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserCreate(
                    PSLUINT32 dwMode,
                    PSLUINT32 cbExtra,
                    MTPSTREAMPARSER* pmtpstrmParser);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserCheckType(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwMode,
                    PSLUINT32 dwType);

/*  Packet Type Determination
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamParserGetPacketType(
                    MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwPacketType,
                    PSLUINT32* pcbPacket);

/*  Virtual Function Helper Macros
 */

#define GET_MTPSTREAMPARSERVTBL(_po)    (((PMTPSTREAMPARSEROBJ)(_po))->vtbl)

#define MTPStreamParserGetBytesAvailable(_po, _cb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserGetBytesAvailable(_po, _cb)

#define MTPStreamParserRead(_po, _pv, _cb, _pcb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserRead(_po, _pv, _cb, _pcb)

#define MTPStreamParserSend(_po, _pv, _cb, _pcb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserSend(_po, _pv, _cb, _pcb)

#define MTPStreamParserParseHeader(_po, _ph, _pdw, _pcb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserParseHeader(_po, _ph, _pdw, _pcb)

#define MTPStreamParserStoreHeader(_po, _dw, _cb, _ph) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserStoreHeader(_po, _dw, _cb, _ph)

#define MTPStreamParserGetDataHeader(_po, _pv, _pps, _pcb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserGetDataHeader(_po, _pv, _pps, _pcb)

#define MTPStreamParserDestroy(_po) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserDestroy(_po)

#define MTPStreamParserSetType(_po, _dw) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserSetType(_po, _dw)

#define MTPStreamParserGetParserInfo(_po, pi, _cb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserGetParserInfo(_po, pi, _cb)

#define MTPStreamParserReset(_po) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserReset(_po)

#define MTPStreamParserSendOpRequest(_po, _pop, _cb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserSendOpRequest(_po, _pop, _cb)

#define MTPStreamParserReadOpRequest(_po, _pdw, _pop) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserReadOpRequest(_po, _pdw, _pop)

#define MTPStreamParserSendOpResponse(_po, _pr, _cb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserSendOpResponse(_po, _pr, _cb)

#define MTPStreamParserReadOpResponse(_po, _pr) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserReadOpResponse(_po, _pr)

#define MTPStreamParserSendEvent(_po, _pe, _cb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserSendEvent(_po, _pe, _cb)

#define MTPStreamParserReadEvent(_po, _pe) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserReadEvent(_po, _pe)

#define MTPStreamParserSendStartData(_po, _dw, _qw) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserSendStartData(_po, _dw, _qw)

#define MTPStreamParserReadStartData(_po, _pdw, _pqw) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserReadStartData(_po, _pdw, _pqw)

#define MTPStreamParserSendData(_po, _dw, _pv, _cb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserSendData(_po, _dw, _pv, _cb)

#define MTPStreamParserReadData(_po, _pdw, _pv, _cb, _pcb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserReadData(_po, _pdw, _pv, _cb, _pcb)

#define MTPStreamParserSendEndData(_po, _dw, _pv, _cb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserSendEndData(_po, _dw, _pv, _cb)

#define MTPStreamParserReadEndData(_po, _pdw, _pv, _cb, _pcb) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserReadEndData(_po, _pdw, _pv, _cb, _pcb)

#define MTPStreamParserSendCancel(_po, _dw) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserSendCancel(_po, _dw)

#define MTPStreamParserReadCancel(_po, _pdw) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserReadCancel(_po, _pdw)

#define MTPStreamParserSendProbeRequest(_po) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserSendProbeRequest(_po)

#define MTPStreamParserReadProbeRequest(_po) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserReadProbeRequest(_po)

#define MTPStreamParserSendProbeResponse(_po, _dw) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserSendProbeResponse(_po, _dw)

#define MTPStreamParserReadProbeResponse(_po) \
    GET_MTPSTREAMPARSERVTBL(_po)->pfnMTPStreamParserReadProbeResponse(_po)


/*  Safe Helpers
 */

#define SAFE_MTPSTREAMPARSERDESTROY(_p) \
if (PSLNULL != _p) \
{ \
    MTPStreamParserDestroy(_p); \
    _p = PSLNULL; \
}

#endif  /* _MTPSTREAMPARSER_H_ */

