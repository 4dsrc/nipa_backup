/*
 *  MTPMsgQueue.h
 *
 *  Contains the message queue declarations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPMSGQUEUE_H_
#define _MTPMSGQUEUE_H_

#include "MTPMsg.h"

/*
 *  Internal Message Classes
 *
 */

enum
{
    MTP_CORE_PRIVATE = MTP_MAX_MSG_CLASS,
    MTP_STREAMROUTER_PRIVATE,
    MTP_BUFFERMGR_PRIVATE,
    MTP_TRANSPORT_PRIVATE,
    MTP_HANDLER_PRIVATE,
    MTP_COMMAND_PRIVATE,
    MTP_DATA_PRIVATE,
    MTP_EVENT_PRIVATE,
    MTP_RESPONSE_PRIVATE,
    MTP_MAX_PRIVATE_MSG_CLASS
};

/*
 *  PFNSINGLETHREADEDCALLBACK
 *
 *  Callback function used by the message queue used by the
 *  message queue during OnPost when a message is posted,
 *  in a single threaded system.
 *
 */

typedef PSLSTATUS (PSL_API *PFNSINGLETHREADEDCALLBACK)(
                            PSLVOID* pvParam);

/*
 *  MTPSINGLETHREADEDCALLBACK
 *
 *  This structure wraps the callback function used by the
 *  message queue during OnPost when a message is posted,
 *  in a single threaded system.
 *
 */

typedef struct
{
    PFNSINGLETHREADEDCALLBACK pfnSingleThreadedCallback;
}MTPSINGLETHREADEDCALLBACK;

PSL_EXTERN_C PSLSTATUS PSL_API MTPMsgGetQueueParamThreaded(
                            PSLVOID** ppvResult);

PSL_EXTERN_C PSLSTATUS PSL_API MTPMsgReleaseQueueParamThreaded(
                            PSLVOID* pvResult);

PSL_EXTERN_C PSLSTATUS PSL_API MTPMsgOnPostThreaded(
                            PSLVOID* pvParam);

PSL_EXTERN_C PSLSTATUS PSL_API MTPMsgOnWaitThreaded(
                            PSLVOID* pvParam);

PSL_EXTERN_C PSLSTATUS PSL_API MTPMsgOnDestroyThreaded(
                            PSLVOID* pvParam);

PSL_EXTERN_C PSLSTATUS PSL_API MTPMsgGetQueueParamSingleThreaded(
                            PSLVOID** ppvResult);

PSL_EXTERN_C PSLSTATUS PSL_API MTPMsgReleaseQueueParamSingleThreaded(
                            PSLVOID* pvResult);

PSL_EXTERN_C PSLSTATUS PSL_API MTPMsgOnPostSingleThreaded(
                            PSLVOID* pvParam);

PSL_EXTERN_C PSLSTATUS PSL_API MTPMsgOnWaitSingleThreaded(
                            PSLVOID* pvParam);

PSL_EXTERN_C PSLSTATUS PSL_API MTPMsgOnDestroySingleThreaded(
                            PSLVOID* pvParam);

#ifdef PSL_SINGLE_THREADED

#define MTPMsgGetQueueParam     MTPMsgGetQueueParamSingleThreaded
#define MTPMsgReleaseQueueParam MTPMsgReleaseQueueParamSingleThreaded
#define MTPMsgOnPost            MTPMsgOnPostSingleThreaded
#define MTPMsgOnWait            MTPMsgOnWaitSingleThreaded
#define MTPMsgOnDestroy         MTPMsgReleaseQueueParamSingleThreaded

#else   /* !PSL_SINGLE_THREADED */

#define MTPMsgGetQueueParam     MTPMsgGetQueueParamThreaded
#define MTPMsgReleaseQueueParam MTPMsgReleaseQueueParamThreaded
#define MTPMsgOnPost            MTPMsgOnPostThreaded
#define MTPMsgOnWait            MTPMsgOnWaitThreaded
#define MTPMsgOnDestroy         MTPMsgReleaseQueueParamThreaded

#endif  /* PSL_SINGLE_THREADED */

/*
 *  Safe Helpers
 *
 */

#define SAFE_MTPMSGRELEASEQUEUEPARAM(_pv) \
if (PSLNULL != _pv) \
{ \
    MTPMsgReleaseQueueParam(_pv); \
    _pv = PSLNULL; \
}

#endif /*_MTPMSGQUEUE_H_*/
