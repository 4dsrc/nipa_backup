/*
 *  MTPHandler.h
 *
 *  Contains declaration for the public handler functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPHANDLER_H_
#define _MTPHANDLER_H_

/* <TODO> this count will either be decided dynamically  or
 * changed based on the maximum number of messages
 * the dispatchers need to send.
 */
#define MTPDISPATCHER_MSGCOUNT  5

/*
 *  GLOBALHANDLERDATA
 *
 *
 */
typedef struct _SESSIONLEVELDATA
{
    PSLUINT32           dwSessionID;
    MTPDISPATCHER       mdDB;
    MTPSERVICERESOLVER  msrServices;
    PSLUINT32           dwLastSendInfoID;
    PSLBYTE*            pSessionBuf;
    PSLUINT32           cbSessionBuf;
    PSLLOOKUPTABLE      ltPostOpenSession;
    PSLHANDLE           hExtSesData;
} SESSIONLEVELDATA;

/*
 *  LOCALHANDLERDATA
 *
 *
 */
typedef struct _OPERATIONLEVELDATA
{
    PSLUINT32           dwNumRespParams;
    PSLUINT32           dwObjectID;
    PSLMSGID            msgResponse;
    MTPRESPONSE         mtpResponse;
} OPERATIONLEVELDATA;

/*
 *  BASEHANDLERDATA
 *
 *  This structure will be the first entry in the
 *  handlers private data is the context structure.
 *
 */
typedef struct _BASEHANDLERDATA
{
    MTPMSGDISPATCHER    msgDispatch;
    SESSIONLEVELDATA    sesLevelData;
    OPERATIONLEVELDATA  opLevelData;
} BASEHANDLERDATA;


/*
 *  MTPHandlerInitialize
 *
 *  This function is called by the MTPInitialize.
 *  It will do resources allocation required for the
 *  functioning of command handlers
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  values less than zero indicates failure
 *  values greater than zero indicate success with more information
 *  for the user of the function
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPHandlerInitialize();

/*
 *  MTPHandlerUninitialize
 *
 *  This function is called by the MTPUninitialize.
 *  It will release the resources used by command handlers.
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  values less than zero indicates failure
 *  values greater than zero indicate success with more information
 *  for the user of the function
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPHandlerUninitialize();

/*
 *  MTPHandlerDataGetSize
 *
 *  This function is called by MTPRouteCreate to find out
 *  the maximum size of memory that need to reserved for
 *  command handlers in the context structure.
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  values less than zero indicates failure
 *  values greater than zero indicate success with more information
 *  for the user of the function
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPHandlerDataGetSize(
                            PSLUINT32* pdwSize);


/*
 *  MTPHandlerForceCloseSession
 *
 *  This function is called by MTPRouteDestroy.
 *  It will intiate a forced close session operation.
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  values less than zero indicates failure
 *  values greater than zero indicate success with more information
 *  for the user of the function
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPHandlerForceCloseSession(
                            MTPCONTEXT* pmc);

#endif _MTPHANDLER_H_
