/*
 *  MTPHandlerServices.h
 *
 *  Contains declaration for the Services related handler functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPHANDLERSERVICES_H_
#define _MTPHANDLERSERVICES_H_

/*
 *  MTPGetServiceIDs
 *
 *  The handler function for the GetServiceIDs MTP operation
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetServiceIDs(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);


/*
 *  MTPGetServiceInfo
 *
 *  The handler function for the GetServiceInfo MTP operation
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetServiceInfo(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetServicePropDesc
 *
 *  The handler function for the GetServiceProperties MTP operation
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetServicePropDesc(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);


/*
 *  MTPGetServiceCapabilities
 *
 *  The handler function for the GetServiceCapabilities MTP operation
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetServiceCapabilities(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetServicePropList
 *
 *  The handler function for the GetServicePropList MTP operation
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetServicePropList(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPSetServicePropList
 *
 *  The handler function for the SetServicePropList MTP operation
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSetServicePropList(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPDeleteServicePropList
 *
 *  The handler function for the DeleteServicePropList MTP operation
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPDeleteServicePropList(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

#endif _MTPHANDLERSERVICES_H_
