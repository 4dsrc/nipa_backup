/*
 *  MTPStreamBufferMgr.h
 *
 *  Contains declarations of the Windows MTP stream buffer management functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPSTREAMBUFFERMGR_H_
#define _MTPSTREAMBUFFERMGR_H_

typedef PSLHANDLE MTPSTREAMBUFFERMGR;

/*
 *  MTP Stream Buffer Counts
 *
 */

#define MTPSTREAM_MAX_BUFFERS               8

#define MTPSTREAM_MAX_COMMAND_BUFFERS       1
#define MTPSTREAM_MAX_TRANSMIT_BUFFERS      2
#define MTPSTREAM_MAX_RECEIVE_BUFFERS       4
#define MTPSTREAM_MAX_RESPONSE_BUFFERS      1
#define MTPSTREAM_MAX_EVENT_BUFFERS         2
#define MTPSTREAM_MAX_FLUSH_BUFFERS         1

#define MTPSTREAM_TOTAL_BUFFERS         (MTPSTREAM_MAX_COMMAND_BUFFERS + \
                                            MTPSTREAM_MAX_TRANSMIT_BUFFERS + \
                                            MTPSTREAM_MAX_RECEIVE_BUFFERS + \
                                            MTPSTREAM_MAX_RESPONSE_BUFFERS + \
                                            MTPSTREAM_MAX_EVENT_BUFFERS + \
                                            MTPSTREAM_MAX_FLUSH_BUFFERS)

/*
 *  MTP STream Buffer Header Size
 *
 */

#define MTPSTREAM_HEADER_SIZE               16
/*
 *  MTP Stream Flush Buffer Size
 *
 */

#define MTPSTREAM_FLUSH_BUFFER_SIZE         0x1000

/*
 *  MTP Stream Buffers
 *
 */

enum
{
    MTPSTREAMBUFFER_FLUSH_DATA =           MTPBUFFER_START_TRANSPORT_SPECIFIC,
    MTPSTREAMBUFFER_START_TRANSPORT_SPECIFIC
};

/*
 *  MTP Stream Buffer Manager Messages
 *
 */

#define MAKE_MTPSTREAMBUFFERMGR_MSGID(_id, _flags) \
    MAKE_PSL_MSGID(MTP_BUFFERMGR_PRIVATE, (_id), (_flags))

enum
{
    MTPSTREAMBUFFERMGRMSG_UNKNOWN =             MAKE_MTPSTREAMBUFFERMGR_MSGID(0, 0),
    MTPSTREAMBUFFERMGRMSG_BUFFER_AVAILABLE =    MAKE_MTPSTREAMBUFFERMGR_MSGID(1, MTPF_B),
};

/*
 *  MTPStreamBufferMgrCreate
 *
 *  Creates a buffer object
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamBufferMgrCreate(
                            PSLUINT32 cbAlignment,
                            PSLUINT32 cbMaxCommandTransmit,
                            PSLUINT32 cbMaxCommandReceive,
                            PSLUINT32 cbMaxEventTransmit,
                            PSLUINT32 cbMaxEventReceive,
                            PSLUINT32 cbDataHeader,
                            MTPSTREAMBUFFERMGR* pmtpstrmbm);


/*
 *  MTPStreamBufferMgrDestroy
 *
 *  Destroys a buffer object
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamBufferMgrDestroy(
                            MTPSTREAMBUFFERMGR mtpstrmbm);


/*
 *  MTPStreamBufferMgrRequest
 *
 *  Requests a buffer
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamBufferMgrRequest(
                            MTPSTREAMBUFFERMGR mtpstrmbm,
                            PSLUINT32 dwBufferType,
                            PSLUINT32 cbRequested,
                            PSLPARAM aParam,
                            PSLVOID** pvBuffer,
                            PSLUINT32* pcbActual,
                            PSLMSGQUEUE mqAsync,
                            PSLMSGPOOL mpAsync);


/*
 *  MTPStreamBufferMgrCancelRequest
 *
 *  Cancels any pending request for a buffer of the specified type
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamBufferMgrCancelRequest(
                            MTPSTREAMBUFFERMGR mtpstrmbm,
                            PSLUINT32 dwBufferType);


/*
 *  MTPStreamBufferMgrRelease
 *
 *  Returns a buffer
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamBufferMgrRelease(
                            PSLVOID* pvBuffer);


/*
 *  MTPStreamBufferMgrGetInfo
 *
 *  Returns information about a MTP Stream buffer
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamBufferMgrGetInfo(
                            PSLVOID* pvBuffer,
                            MTPSTREAMBUFFERMGR* pmtpstrmbm,
                            PSLUINT32* pcbSize,
                            PSLUINT32* pdwBufferType);

/*
 *  MTPStreamBufferMgrGetBufferHeader
 *
 *  Returns a pointer to the buffer's header.  The header precedes the
 *  actual buffer pointer and is present to enable large data packets to
 *  have the MTP Stream data packet header attached without having to require
 *  a copy to create a contiquous packet for sending.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamBufferMgrGetBufferHeader(
                            PSL_CONST PSLVOID* pvBuffer,
                            PSLVOID** ppvHeader,
                            PSLUINT32* pcbHeader);

/*
 *  Safe Helpers
 *
 */

#define SAFE_MTPSTREAMBUFFERMGRDESTROY(_b) \
if (PSLNULL != (_b)) \
{ \
    MTPStreamBufferMgrDestroy(_b); \
    (_b) = PSLNULL; \
}

#define SAFE_MTPSTREAMBUFFERMGRRELEASE(_pv) \
if (PSLNULL != (_pv)) \
{ \
    MTPStreamBufferMgrRelease(_pv); \
    (_pv) = PSLNULL; \
}

#endif  /* _MTPSTREAMBUFFERMGR_H_ */

