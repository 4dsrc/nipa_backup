/*
 *  MTPHandlerDatabase.h
 *
 *  Contains declaration for the Database related handler functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPHANDLERDATABASE_H_
#define _MTPHANDLERDATABASE_H_

typedef struct _DBHANDLERDATA
{
    BASEHANDLERDATA     bhd;
    MTPDBCONTEXT        mdbc;
    MTPDBCONTEXTDATA    mdbcd;
}DBHANDLERDATA;


/*
 *  MTPFormatStore
 *
 *  This handler function for FormatStore MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPFormatStore (
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetStorageIDs
 *
 *  This handler function for GetStorageIDs MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetStorageIDs (
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetStorageInfo
 *
 *  This handler function for GetStorageInfo MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetStorageInfo (
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetNumObjects
 *
 *  This handler function for GetNumObjects MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetNumObjects (
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetObjectHandles
 *
 *  This handler function for GetObjectHandles MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetObjectHandles (
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetObject
 *
 *  This handler function for GetObject MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetObject (
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPSendObject
 *
 *  This handler function for SendObject MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSendObject (
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetPartialObject
 *
 *  This handler function for GetPartialObject MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetPartialObject(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPDeleteObject
 *
 *  This handler function for DeleteObject MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPDeleteObject(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPCopyObject
 *
 *  This handler function for CopyObject MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPCopyObject(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPMoveObject
 *
 *  This handler function for MoveObject MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPMoveObject(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetThumb
 *
 *  This handler function for GetThumb MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetThumb(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetObjectInfo
 *
 *  This handler function for GetObjectInfo MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetObjectInfo (
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPSendObjectInfo
 *
 *  This handler function for SendObjectInfo MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSendObjectInfo (
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetObjectPropValue
 *
 *  This handler function for GetObjectPropValue MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetObjectPropValue(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPSetObjectPropValue
 *
 *  This handler function for SetObjectPropValue MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSetObjectPropValue(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetObjectReferences
 *
 *  This handler function for GetObjectReference MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetObjectReferences(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPSetObjectReferences
 *
 *  This handler function for SetObjectReference MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSetObjectReferences(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetObjectPropList
 *
 *  This handler function for GetObjectPropList MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetObjectPropList(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPSetObjectPropList
 *
 *  This handler function for SetObjectPropList MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSetObjectPropList(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPSendObjectPropList
 *
 *  This handler function for SendObjectPropList MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSendObjectPropList(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPUpdateObjectPropList
 *
 *  This handler function for SendObjectPropList MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPUpdateObjectPropList(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPDeleteObjectPropList
 *
 *  This handler function for MTPDeleteObjectPropList MTP operation
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPDeleteObjectPropList(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPSetObjectProtection
 *
 *  This handler function for SetObjectProtection MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSetObjectProtection(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);


/*
 *  MTPSetObjectPropValue
 *
 *  This handler function for SetObjectPropValue MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSetObjectPropValue(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetObjectPropList
 *
 *  This handler function for GetObjectPropList MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetObjectPropList(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPMoveObject
 *
 *  This handler function for MoveObject MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPMoveObject(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPCopyObject
 *
 *  This handler function for CopyObject MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPCopyObject(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPWMPAcceleratedMetadataTransfer
 *
 *  This handler function for WMPAcceleratedMetadataTransfer MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPWMPAcceleratedMetadataTransfer(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPWMPReportingAcquiredContent
 *
 *  This handler function for WMPReportingAcquiredContent MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPWMPReportingAcquiredContent(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg);

/*
 *  MTPGetObjectPropsSupported
 *
 *  This handler function for GetObjectPropsSupported MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetObjectPropsSupported(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

/*
 *  MTPGetObjectPropDesc
 *
 *  This handler function for GetObjectPropDesc MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetObjectPropDesc(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);


/*
 *  MTPGetFormatCapabilities
 *
 *  Handles the GetFormatCapabilities MTP operation.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPGetFormatCapabilities(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

#endif _MTPHANDLERDATABASE_H_
