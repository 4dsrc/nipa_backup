/*
 *  MTPStreamRouter.h
 *
 *  Contains declarations of MTP Stream Router structures and functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPSTREAMROUTER_H_
#define _MTPSTREAMROUTER_H_

#include "MTPMsgQueue.h"
#include "MTPStreamParser.h"

#define MTPSTREAMROUTER_MSG_POOL_SIZE        16

typedef PSLHANDLE MTPSTREAMROUTER;

/*  MTP Stream Router Data States
 *
 *  These states are used to manage when the router sends data start and data
 *  end packets.
 */

enum
{
    MTPSTREAMDATASTATE_PENDING = 0,
    MTPSTREAMDATASTATE_OUT_READY,
    MTPSTREAMDATASTATE_OUT_START,
    MTPSTREAMDATASTATE_OUT_END,
    MTPSTREAMDATASTATE_IN_READY,
    MTPSTREAMDATASTATE_IN_START,
    MTPSTREAMDATASTATE_IN_END
};


/*  MTP Stream Router Messages
 */

#define MAKE_MTPSTREAMROUTER_MSGID(_id, _flags) \
    MAKE_PSL_MSGID(MTP_STREAMROUTER_PRIVATE, (_id), (_flags))

enum
{
    MTPSTREAMROUTERMSG_UNKNOWN =                MAKE_MTPSTREAMROUTER_MSGID(0, 0),
    MTPSTREAMROUTERMSG_DATA_AVAILABLE =         MAKE_MTPSTREAMROUTER_MSGID(1, 0),
    MTPSTREAMROUTERMSG_CHECK_DATA_AVAILABLE =   MAKE_MTPSTREAMROUTER_MSGID(2, 0),
    MTPSTREAMROUTERMSG_RESET =                  MAKE_MTPSTREAMROUTER_MSGID(3, 0),
};


/*  MTP Stream Router Objcet
 *
 *  Contains the information necessary to manage the stream router.
 */

/*  MTP Stream Router Virtual Functions
 *
 *  Each of the function prototypes below defines a virtualized parser function
 *  that is either provided by the base implementation or a sub-class.
 *  Functions that are called out as PURE must be implemented in the sub-class.
 */

/*  pfnMTPStreamRouterDestroy
 *
 *  Destroys an instance of the stream routert.
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMROUTERDESTROY)(
                    MTPSTREAMROUTER mtpstrm);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouterDestroyBase(
                    MTPSTREAMROUTER mtpstrm);

/*  pfnMTPStreamRouterCreateContext
 *
 *  Creates a new MTP Context for this stream router
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMROUTERCREATECONTEXT)(
                    MTPSTREAMROUTER mtpstrm,
                    MTPCONTEXT** ppmtpctx);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouterCreateContextBase(
                    MTPSTREAMROUTER mtpstrm,
                    MTPCONTEXT** ppmtpctx);

/*  pfnMTPStreamRouterRoutePacketReceived
 *
 *  Forwards custom packet types onto handlers
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMROUTERROUTEPACKETRECEIVED)(
                    MTPSTREAMROUTER mtpstrm,
                    MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc,
                    PSLUINT32 dwPacketType,
                    PSLUINT32 cbPacket);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouterRoutePacketReceivedBase(
                    MTPSTREAMROUTER mtpstrm,
                    MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc,
                    PSLUINT32 dwPacketType,
                    PSLUINT32 cbPacket);

/*  pfnMTPStreamRouterResetDataAvailable - PURE
 *
 *  Resets the data available event for the specified parser
 */

typedef PSLSTATUS (PSL_API* PFNMTPSTREAMROUTERRESETDATAAVAILABLE)(
                    MTPSTREAMROUTER mtpstrm,
                    MTPSTREAMPARSER mtpstrmParser);


/*  MTPSTREAMROUTERVTBL
 *
 *  MTP Stream Router virtual function table
 */

typedef struct _MTPSTREAMROUTERVTBL
{
    PFNMTPSTREAMROUTERDESTROY               pfnMTPStreamRouterDestroy;
    PFNMTPSTREAMROUTERCREATECONTEXT         pfnMTPStreamRouterCreateContext;
    PFNMTPSTREAMROUTERROUTEPACKETRECEIVED   pfnMTPStreamRouterRoutePacketReceived;
    PFNMTPSTREAMROUTERRESETDATAAVAILABLE    pfnMTPStreamRouterResetDataAvailable;
} MTPSTREAMROUTERVTBL;


/*  MTPSTREAMROUTEROBJ
 *
 *  Contains all of the state necessary for the MTP Stream Router.
 *  The state is stored in the transport portion of the MTPCONTEXT
 */

typedef struct _MTPSTREAMROUTEROBJ
{
    PSL_CONST MTPSTREAMROUTERVTBL*  vtbl;
#ifdef PSL_ASSERTS
    PSLUINT32                       dwCookie;
#endif  /* PSL_ASSERTS */
    MTPSTREAMPARSER                 mtpstrmCommand;
    MTPSTREAMPARSER                 mtpstrmEvent;
    MTPSTREAMBUFFERMGR              mtpstrmbm;
    PSLMSGQUEUE                     mqTransport;
    PSLMSGPOOL                      mpTransport;
    PSLUINT32                       dwDataPhase;
    PSLUINT32                       dwTransactionID;
    PSLUINT32                       dwDataState;
    PSLUINT64                       qwTotalSize;
    PSLBOOL                         fReceivePending;
} MTPSTREAMROUTEROBJ;

typedef PSL_CONST MTPSTREAMROUTEROBJ* PMTPSTREAMROUTEROBJ;


/*  MTP Context Packaging
 */

typedef struct _MTPSTREAMROUTERCONTEXT
{
    MTPSTREAMROUTEROBJ*         pmtpstrm;
} MTPSTREAMROUTERCONTEXT;

#define MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(_pctx) \
    ((MTPSTREAMROUTERCONTEXT*) \
                ((_pctx)->mtpTransportState.pbTransportData))->pmtpstrm

/*  Creation Functions
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouterCreate(
                    PSLUINT32 cbObject,
                    MTPSTREAMROUTER* ppmtpstrm);

/*  Routing Functions
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteCmdReceived(
                    MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc,
                    PSLVOID* pvBuffer,
                    PSLUINT64 cbBuffer);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteDataReceived(
                    MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc,
                    PSLVOID* pvBuffer,
                    PSLUINT64 cbBuffer);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteCancelReceived(
                    MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteDataAvailable(
                    MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteDataSend(
                    MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwSequenceID,
                    PSLVOID* pvData,
                    PSLUINT64 cbData,
                    PSLUINT32 dwDataInfo);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteBufferAvailable(
                    MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwBufferType,
                    PSLPARAM aParam,
                    PSLVOID* pvBuffer,
                    PSLUINT64 cbBuffer);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteBufferRequest(
                    MTPCONTEXT* pmtpctx,
                    PSLMSGID msgID,
                    PSLUINT32 dwSequenceID,
                    PSLUINT64 cbRequested,
                    PSLUINT32 dwDataInfo);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteBufferRequestFlags(
                    MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwSequenceID,
                    PSLUINT64 cbRequest,
                    PSLUINT32 dwFlags);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteBufferConsumed(
                    MTPCONTEXT* pmtpctx,
                    PSLMSGID msgID,
                    PSLVOID* pvBuffer);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteBufferRelease(
                    PSL_CONST PSLMSG* pMsg);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteTerminate(
                    MTPCONTEXT* pmtpctx,
                    PSLMSGID msgID,
                    PSLPARAM aParam);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteReset(
                    MTPCONTEXT** ppmtpctx);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouteMsg(
                    MTPCONTEXT* pmtpctx,
                    PSL_CONST PSLMSG* pMsg);

/*  Helper Functions
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStreamRouterCleanQueueEnum(
                    PSL_CONST PSLMSG* pMsg,
                    PSLPARAM aParam);

/*  Virtual Function Helper Macros
 */

#define GET_MTPSTREAMROUTERVTBL(_po)    (((PMTPSTREAMROUTEROBJ)(_po))->vtbl)

#define MTPStreamRouterDestroy(_po) \
    GET_MTPSTREAMROUTERVTBL(_po)->pfnMTPStreamRouterDestroy(_po)

#define MTPStreamRouterCreateContext(_po, _ppc) \
    GET_MTPSTREAMROUTERVTBL(_po)->pfnMTPStreamRouterCreateContext(_po, _ppc)

#define MTPStreamRouterRoutePacketReceived(_po, _pc, _src, _dwT, _cb) \
    GET_MTPSTREAMROUTERVTBL(_po)->pfnMTPStreamRouterRoutePacketReceived(_po, _pc, _src, _dwT, _cb)

#define MTPStreamRouterResetDataAvailable(_po, _sp) \
    GET_MTPSTREAMROUTERVTBL(_po)->pfnMTPStreamRouterResetDataAvailable(_po, _sp)

/*  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 */

#define SAFE_MTPSTREAMROUTERDESTROY(_pmtpstrm) \
if (PSLNULL != _pmtpstrm) \
{ \
    MTPStreamRouterDestroy(_pmtpstrm); \
    _pmtpstrm = PSLNULL; \
}

#endif  /* _MTPSTREAMROUTER_H_ */

