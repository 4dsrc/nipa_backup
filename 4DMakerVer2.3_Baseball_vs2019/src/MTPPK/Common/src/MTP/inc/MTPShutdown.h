/*
 *  MTPShutdown.h
 *
 *  Contains declarations for the MTP Shutdown service.  This is an internal
 *  service triggered by MTPUninitialized that notifies all registered clients
 *  that it is time to shutdown.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPSHUTDOWN_H_
#define _MTPSHUTDOWN_H_

/*
 *  MTPShutdownRegister
 *
 *  Called to register a message queue to receive the MTP shutdown message
 *
 *  NOTE: The shutdown service will halt until all registered clients have
 *  unregistered so if you call MTPShutdownRegister you must also call
 *  MTPShutdownUnregister
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPShutdownRegister(
                            PSLMSGQUEUE mqNotify);


/*
 *  MTPShutdownUnregister
 *
 *  Called to unregister a message queue from the MTP Shutdown service
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPShutdownUnregister(
                            PSLMSGQUEUE mqNotify);


/*
 *  MTPShutdown
 *
 *  Called to initiate an MTP shutdown.  This function will block until all
 *  registered clients of the MTP Shutdown Service have unregistered.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPShutdown(PSLVOID);

#endif  /* _MTPSHUTDOWN_H_ */

