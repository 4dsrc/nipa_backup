/*
 *  MTPHandlerUtil.h
 *
 *  Contains declaration for utility functions used by
 *  command handlers.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPCOMMANDHANDLERUTIL_H_
#define _MTPCOMMANDHANDLERUTIL_H_

PSL_EXTERN_C PSLSTATUS PSL_API MTPOnCommandDispatch(
                            PSLMSG* pMsg);

PSL_EXTERN_C PSLSTATUS PSL_API MTPHandlerBegin(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg,
                            MTPCONTEXT** ppmc,
                            BASEHANDLERDATA** ppData,
                            PSLMSG** ppMsg);

PSL_EXTERN_C PSLSTATUS PSL_API MTPHandlerEnd(
                            PSLMSG* pMsg);

PSL_EXTERN_C PSLSTATUS PSL_API MTPHandlerSetState(
                            MTPCONTEXT* pmc,
                            PSLUINT16 wRespCode,
                            PSLUINT32 dwHandlerState,
                            PSLUINT32* pdwMsgFlags,
                            PSLUINT64* pqwDataSize);

PSL_EXTERN_C PSLSTATUS PSL_API MTPHandlerRouteMsgToTransport(
                            MTPCONTEXT* pmc, PSLMSG* pRetMsg,
                            PSLMSG* pSrcMsg,PSLUINT32 msgID,
                            PSLBYTE* pbBuf, PSLLPARAM cbBuf,
                            PSLUINT32 dwFlags);

PSL_EXTERN_C PSLSTATUS PSL_API MTPHandlerGetResponseCodeFromStatus(
                            PSLSTATUS dwStatus,
                            PSLUINT16* pwRespCode);

PSL_EXTERN_C PSLSTATUS PSL_API FillMessage(
                            PSLMSG* pDesMsg,
                            PSLMSG* pSrcMsg,
                            PSLUINT32 msgID,
                            PSLBYTE* pbBuf,
                            PSLLPARAM cbBuf,
                            PSLUINT32 dwFlags);

PSL_EXTERN_C PSLSTATUS PSL_API FillResponse(
                            PXMTPRESPONSE pResponse,
                            PSLUINT16 wRespCode,
                            PSLUINT32 dwTransactionID,
                            PSLUINT32 dwParam1,
                            PSLUINT32 dwParam2,
                            PSLUINT32 dwParam3,
                            PSLUINT32 dwParam4,
                            PSLUINT32 dwParam5);

PSL_EXTERN_C PSLSTATUS PSL_API AllocateReturnMsg(
                            MTPDISPATCHER md,
                            PSLMSG** ppMsg);

PSL_EXTERN_C PSLSTATUS PSL_API GetReadyForNextCommand(
                            MTPDISPATCHER md,
                            MTPCONTEXT* pmc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPPreOpenLookupTableInit(
                            PSLMSGQUEUE mqCore);

PSL_EXTERN_C PSLSTATUS PSL_API MTPPostOpenLookupTableInit(
                            PSLMSGQUEUE mqCore,
                            PSLMSGQUEUE mqDB,
                            PSLMSGQUEUE mqDRM,
                            PSLLOOKUPTABLE* pplt);

PSL_EXTERN_C PSLSTATUS PSL_API MTPPreOpenLookupTableUninit();

PSL_EXTERN_C PSLSTATUS PSL_API MTPPostOpenLookupTableUninit(
                            PSLLOOKUPTABLE plt);

PSL_EXTERN_C PSLSTATUS PSL_API MTPHandlerPropInitialize();

PSL_EXTERN_C PSLSTATUS PSL_API MTPHandlerPropUninitialize();

#define InitPropQuad(_pmq)  PSLZeroMemory((_pmq), sizeof(*(_pmq)))

#endif _MTPCOMMANDHANDLERUTIL_H_
