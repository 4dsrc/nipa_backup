/*
 *  MTPHandlerProp.h
 *
 *  Contains declaration for the device level handler functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPHANDLERPROP_H_
#define _MTPHANDLERPROP_H_

/*
 *  MTPGetDevicePropDesc
 *
 *  This handler function for GetDevicePropDesc MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetDevicePropDesc(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

/*
 *  MTPGetDevicePropValue
 *
 *  This handler function for GetDevicePropValue MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetDevicePropValue(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);
/*
 *  MTPSetDevicePropValue
 *
 *  This handler function for SetDevicePropValue MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPSetDevicePropValue(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);
/*
 *  MTPResetDevicePropValue
 *
 *  This handler function for ResetDevicePropValue MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPResetDevicePropValue(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

/*
 *  MTPGetInterdependentPropDesc
 *
 *  This handler function for GetInterdependentPropDesc MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPGetInterdependentPropDesc(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

#endif /*_MTPHANDLERPROP_H_*/
