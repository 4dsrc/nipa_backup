/*
 *  MTPStreamPackets.h
 *
 *  Contains declaration of the different MTP Stream packets and associated
 *  constants.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPSTREAMPACKETS_H_
#define _MTPSTREAMPACKETS_H_

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPush1.h"
#endif

/*  MTPSTREAMOPERATIONREQUEST
 *
 *  MTP Stream Operation Request Packet
 */

struct _MTPSTREAMOPERATIONREQUEST
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwDataPhase;
    PSLUINT16               wOpCode;
    PSLUINT32               dwTransactionID;
    PSLUINT32               dwParam1;
    PSLUINT32               dwParam2;
    PSLUINT32               dwParam3;
    PSLUINT32               dwParam4;
    PSLUINT32               dwParam5;
} PSL_PACK1;

typedef struct _MTPSTREAMOPERATIONREQUEST MTPSTREAMOPERATIONREQUEST;

typedef struct _MTPSTREAMOPERATIONREQUEST PSL_UNALIGNED*
                            PXMTPSTREAMOPERATIONREQUEST;
typedef PSL_CONST struct _MTPSTREAMOPERATIONREQUEST PSL_UNALIGNED*
                            PCXMTPSTREAMOPERATIONREQUEST;

#define MTPSTREAMOPERATIONREQUEST_MIN_SIZE \
        PSLOFFSETOF(MTPSTREAMOPERATIONREQUEST, dwParam1)

/*  MTPSTREAMOPERATIONRESPONSE
 *
 *  MTP Stream Operation Response Packet
 */

struct _MTPSTREAMOPERATIONRESPONSE
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT16               wResponseCode;
    PSLUINT32               dwTransactionID;
    PSLUINT32               dwParam1;
    PSLUINT32               dwParam2;
    PSLUINT32               dwParam3;
    PSLUINT32               dwParam4;
    PSLUINT32               dwParam5;
} PSL_PACK1;

typedef struct _MTPSTREAMOPERATIONRESPONSE MTPSTREAMOPERATIONRESPONSE;

typedef struct _MTPSTREAMOPERATIONRESPONSE PSL_UNALIGNED*
                            PXMTPSTREAMOPERATIONRESPONSE;
typedef PSL_CONST struct _MTPSTREAMOPERATIONRESPONSE PSL_UNALIGNED*
                            PCXMTPSTREAMOPERATIONRESPONSE;

#define MTPSTREAMOPERATIONRESPONSE_MIN_SIZE \
        PSLOFFSETOF(MTPSTREAMOPERATIONRESPONSE, dwParam1)

/*  MTPSTREAMEVENT
 *
 *  MTP Stream Event Packet
 */

struct _MTPSTREAMEVENT
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT16               wEventCode;
    PSLUINT32               dwTransactionID;
    PSLUINT32               dwParam1;
    PSLUINT32               dwParam2;
    PSLUINT32               dwParam3;
} PSL_PACK1;

typedef struct _MTPSTREAMEVENT MTPSTREAMEVENT;

typedef struct _MTPSTREAMEVENT PSL_UNALIGNED* PXMTPSTREAMEVENT;
typedef PSL_CONST struct _MTPSTREAMEVENT PSL_UNALIGNED* PCXMTPSTREAMEVENT;

#define MTPSTREAMEVENT_MIN_SIZE \
        PSLOFFSETOF(MTPSTREAMEVENT, dwParam1)


/*  MTPSTREAMSTARTDATA
 *
 *  MTP Stream Start Data Packet
 */

struct _MTPSTREAMSTARTDATA
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwTransactionID;
    PSLUINT64               cbTotal;
} PSL_PACK1;

typedef struct _MTPSTREAMSTARTDATA MTPSTREAMSTARTDATA;

typedef struct _MTPSTREAMSTARTDATA PSL_UNALIGNED* PXMTPSTREAMSTARTDATA;
typedef PSL_CONST struct _MTPSTREAMSTARTDATA PSL_UNALIGNED*
                            PCXMTPSTREAMSTARTDATA;

/*  MTPSTREAMENDDATA
 *
 *  MTP Stream End Data Packet
 */

struct _MTPSTREAMENDDATA
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwTransactionID;
    /*  Data follows immediately */
} PSL_PACK1;

typedef struct _MTPSTREAMENDDATA MTPSTREAMENDDATA;

typedef struct _MTPSTREAMENDDATA PSL_UNALIGNED* PXMTPSTREAMENDDATA;
typedef PSL_CONST struct _MTPSTREAMENDDATA PSL_UNALIGNED* PCXMTPSTREAMENDDATA;

/*  MTPSTREAMCANCEL
 *
 *  MTP Stream Cancel Packet
 */

struct _MTPSTREAMCANCEL
{
    MTPSTREAMHEADER         mtpstrmHeader;
    PSLUINT32               dwTransactionID;
} PSL_PACK1;

typedef struct _MTPSTREAMCANCEL MTPSTREAMCANCEL;

typedef struct _MTPSTREAMCANCEL PSL_UNALIGNED* PXMTPSTREAMCANCEL;
typedef PSL_CONST struct _MTPSTREAMCANCEL PSL_UNALIGNED* PCXMTPSTREAMCANCEL;

/*  MTPSTREAMPROBE
 *
 *  MTP Stream Probe
 */

struct _MTPSTREAMPROBE
{
    MTPSTREAMHEADER         mtpstrmHeader;
} PSL_PACK1;

typedef struct _MTPSTREAMPROBE MTPSTREAMPROBE;

typedef struct _MTPSTREAMPROBE PSL_UNALIGNED* PXMTPSTREAMPROBE;
typedef PSL_CONST struct _MTPSTREAMPROBE PSL_UNALIGNED* PCXMTPSTREAMPROBE;

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPop.h"
#endif

#endif  /* _MTPSTREAMPACKETS_H_ */

