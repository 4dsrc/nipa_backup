/*
 *  MTPHandlerCore.h
 *
 *  Contains declaration for the core handler functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPHANDLERCORE_H_
#define _MTPHANDLERCORE_H_

/*
 *  MTPOpenSession
 *
 *  This handler function for OpenSession MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPOpenSession(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

/*
 *  MTPCloseSession
 *
 *  This handler function for CloseSession MTP operation.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPCloseSession(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

/*
 *  MTPCreateSessionIDList
 *
 *  Creates a signleton session ID list.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPCreateSessionIDList(PSLVOID);

/*
 *  MTPDestroySessionIDList
 *
 *  Destroys the session ID list created by MTPCreateSessionIDList.
 *
 */
PSL_EXTERN_C PSLVOID PSL_API MTPDestroySessionIDList(PSLVOID);

/*
 *  MTPForceCloseSession
 *
 *  Cleans up the resources that were intialized during OpenSession.
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPForceCloseSession(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

#endif _MTPHANDLERCORE_H_
