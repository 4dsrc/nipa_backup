/*
 *  MTPLookupTable.c
 *
 *  Contains the declaration of lookup table fucntions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPLOOKUPTABLE_H_
#define _MTPLOOKUPTABLE_H_

PSL_EXTERN_C PSLSTATUS PSL_API MTPLookupTablePreOpenSet(
                            PSLLOOKUPTABLE plt);

PSL_EXTERN_C PSLSTATUS PSL_API MTPLookupTablePreOpenGet(
                            PSLLOOKUPTABLE* pplt);

PSL_EXTERN_C PSLVOID PSL_API MTPLookupTablePreOpenDestroy();

PSL_EXTERN_C PSLSTATUS PSL_API MTPLookupTableDevicePropSet(
                            PSLLOOKUPTABLE plt);

PSL_EXTERN_C PSLSTATUS PSL_API MTPLookupTableDevicePropGet(
                            PSLLOOKUPTABLE* pplt);

PSL_EXTERN_C PSLVOID PSL_API MTPLookupTableDevicePropDestroy();

PSL_EXTERN_C PSLSTATUS PSL_API MTPLookupTableFindEntry(
                            PSLLOOKUPTABLE  mlt,
                            PSLUINT32       dwKey,
                            PSLMSGQUEUE*    pmqHandler,
                            PSLPARAM*       ppvData);

PSL_EXTERN_C PSLINT32 PSL_API MTPLookupTableCompareOpCode(
                            PSL_CONST PSLVOID* pvKey,
                            PSL_CONST PSLVOID* pvCompare);

#endif /*_MTPLOOKUPTABLE_H_*/
