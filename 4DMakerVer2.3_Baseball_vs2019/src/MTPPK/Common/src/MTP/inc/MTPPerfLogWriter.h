/*
 *  MTPPerfLogWriter.h
 *
 *  Header file to define MTP Performance Log Writer interfaces.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPPERLOGWRITER_H_
#define _MTPPERLOGWRITER_H_

typedef PSLHANDLE   MTPPERLOGWRITER;

typedef struct _DEVICE_TRACE_FILE_HEADER
{
    PSLUINT32       dwVersion;
    PSLUINT32       dwNumOfProcessors;
    PSLUINT64       HostStartTime;
    PSLUINT64       HostStopTime;
    PSLUINT32       dwTimerResolution;
    PSLUINT64       CPUFrequency;
    PSLUINT64       StartTimestamp;
    PSLUINT64       StopTimestamp;
    PSLUINT32       dwEventLost;
    PSLUINT32       dwNumOfEvents;
} DEVICE_TRACE_FILE_HEADER, *PDEVICE_TRACE_FILE_HEADER;

PSLVOID MTPPerfLogWriterCreate(MTPPERLOGWRITER* ppWriter);
PSLVOID MTPPerfLogWriterDestroy(MTPPERLOGWRITER pWriter);
PSLVOID MTPPerfLogWriterWriteBuffer(MTPPERLOGWRITER pWriter,
                                    PSLVOID* pData,
                                    PSLUINT32 Len);
PSLVOID MTPPerfLogWriterOpen(MTPPERLOGWRITER pWriter,
                             PSLCSTR LogName,
                             PDEVICE_TRACE_FILE_HEADER *ppTraceFileHeader);
PSLVOID MTPPerfLogWriterClose(MTPPERLOGWRITER pWriter);

#define SAFE_MTPPERFLOGWRITERDESTROY(_lw)\
if (PSLNULL != (_lw)) \
{ \
    MTPPerfLogWriterDestroy(_lw); \
    (_lw) = PSLNULL; \
}


#endif //_MTPPERLOGWRITER_H_
