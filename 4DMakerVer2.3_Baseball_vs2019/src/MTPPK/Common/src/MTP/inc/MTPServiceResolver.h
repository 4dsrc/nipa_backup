/*
 *  MTPServiceResolver.h
 *
 *  Contains definitions of MTP Service Resolver API
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPSERVICERESOLVER_H_
#define _MTPSERVICERESOLVER_H_

typedef PSLHANDLE MTPSERVICERESOLVER;

/*  MTPServiceResolverCreate/MTPServiceResolverDestroy
 *
 *  Creates and destroys instances of the service resolver
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceResolverCreate(
                    MTPCONTEXT* pmtpctx,
                    MTPSERVICERESOLVER* pmsr);

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceResolverDestroy(
                    MTPSERVICERESOLVER msr);

/*  MTPServiceResolverFindService
 *
 *  Locates the service associated with the MTP DB Variant properties
 *  which define the Context
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceResolverFindService(
                    MTPSERVICERESOLVER msr,
                    PSLUINT32 dwJoinType,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    MTPSERVICE* pmtpService);


/*  MTPServiceResolverGetLegacyService
 *
 *  Returns a pointer to the legacy service associated with this resolver.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceResolverGetLegacyService(
                    MTPSERVICERESOLVER msr,
                    MTPDBSESSION* pmtpdbLegacy);


/*  MTPServiceResolverServiceContextDataOpen
 *
 *  Locates the service based on the MTP DB Variant properties and opens
 *  the appropriate context data.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceResolverServiceContextDataOpen(
                    MTPSERVICERESOLVER msr,
                    PSLUINT32 dwDataFormat,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    PSLUINT64* pqwBufSize,
                    MTPSERVICECONTEXTDATA* pmscd);


/*  MTPServiceResolverDBContextOpen
 *
 *  Opens a the DB Context based on the MTP DB variant properties which
 *  define it.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceResolverDBContextOpen(
                    MTPSERVICERESOLVER msr,
                    PSLUINT32 dwJoinType,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    MTPDBCONTEXT* pmtpDBContext);


/*  Safe Helpers
 */

#define SAFE_MTPSERVICERESOLVERDESTROY(_pv) \
if (PSLNULL != (_pv)) \
{ \
    MTPServiceResolverDestroy(_pv); \
    (_pv) = PSLNULL; \
}


#endif  /* _MTPSERVICERESOLVER_H_ */

