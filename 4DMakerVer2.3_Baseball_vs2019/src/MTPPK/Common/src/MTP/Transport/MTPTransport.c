/*
 *  MTPTransport.c
 *
 *  Contains definitions of useful functions applicable to all transports.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPTransportPrecomp.h"


/*  Local Defines
 */

/*  Local Types
 */

typedef struct _NOTIFYEVENTINFO
{
    PSLMSGID                eventID;
    PSLPARAM                aParamTransport;
} NOTIFYEVENTINFO;

/*  Local Functions
 */

static PSLSTATUS PSL_API _MTPTransportMonitorEnumCtxListProc(
                    PSLPARAM aParam,
                    PSLPARAM aEnumParam);

/*  Local Variables
 */

static PSLWCHAR             _RgchMontorMutex[] = L"MTP Transport Monitor Mutex";

PSLCTXLIST                  _CtxMonitor = PSLNULL;


/*
 *  _MTPTransportMonitorEnumCtxListProc
 *
 *  Calls the monitor function registered and passes the event information.
 *
 *  NOTE:
 *  On first look one might wonder why the extra work is done here to deal
 *  with contexts for message posts when they are typically handled
 *  asynchronously on another thread.  The key is "typically".  The code
 *  as written deals correctly with both multithreaded and singlethreaded
 *  message queues.
 *
 *  Arguments:
 *      PSLPARAM        aParam              Parameter in context (callback)
 *      PSLPARAM        aEnumParam          Enumeration parameter
 *                                            (NOTIFYEVENTINFO)
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPTransportMonitorEnumCtxListProc(PSLPARAM aParam,
                    PSLPARAM aEnumParam)
{
    PFNMTPTRANSPORTMONITOR  pfnMTPTransportMonitor;
    PSLSTATUS               ps;
    NOTIFYEVENTINFO*        pnei;

    /*  Validate Arguments
     */

    if ((PSLNULL == aParam) || (PSLNULL == aEnumParam))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the function and the event information
     */

    pfnMTPTransportMonitor = (PFNMTPTRANSPORTMONITOR)aParam;
    pnei = (NOTIFYEVENTINFO*)aEnumParam;

    /*  Perform the callback into the transport
     */

    pfnMTPTransportMonitor(pnei->eventID, pnei->aParamTransport);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPTransportNotifyInitialize
 *
 *  Creates the transport notifier context
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE if initialized
 *
 */

PSLSTATUS PSL_API MTPTransportNotifyInitialize(PSLVOID)
{
    PSLSTATUS       ps;

    /*  Create the context list- note we assume that we are being called from
     *  the initialization function which is already guarded by a re-entrancy
     *  mutex so we do not do any additional work here.
     */

    ps = PSLCtxListCreate(&_CtxMonitor);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPTransportNotifyUninitialize
 *
 *  Closes a notifier
 *
 *  Arguments:
 *      Nothing
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPTransportNotifyUninitialize(PSLVOID)
{
    /*  Free the context list- note we assume that we are being called from
     *  the MTPUninitialize function which is already guarded by a re-entrancy
     *  mutex so we do not do any additional work here.
     */

    SAFE_PSLCTXLISTDESTROY(_CtxMonitor);
    return PSLSUCCESS;
}


/*
 *  MTPTransportMonitorRegister
 *
 *  Registers a callback function to receive broadcast notifications from the
 *  transports
 *
 *  Arguments:
 *      PFNMTPTRANSPORTMONITOR pfnMTPTransportMonitor
 *                                          Monitor to register
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_EXISTS if already
 *                        registered
 *
 */

PSLSTATUS PSL_API MTPTransportMonitorRegister(
                    PFNMTPTRANSPORTMONITOR pfnMTPTransportMonitor)
{
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == pfnMTPTransportMonitor)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Add the callback to the context list
     */

    ps = PSLCtxListRegister(_CtxMonitor, (PSLPARAM)pfnMTPTransportMonitor);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPTransportMonitorUnregister
 *
 *  Removes the specified monitor from the broadcast notification list
 *
 *  Arguments:
 *      PFNMTPTRANSPORTMONITOR pfnMTPTransportMonitor
 *                                          Monitor to unregister
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if removed, PSLSUCCESS_NOT_FOUND if not found
 *
 */

PSLSTATUS PSL_API MTPTransportMonitorUnregister(
                    PFNMTPTRANSPORTMONITOR pfnMTPTransportMonitor)
{
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == pfnMTPTransportMonitor)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Remove the item from the context list
     */

    ps = PSLCtxListUnregister(_CtxMonitor, (PSLPARAM)pfnMTPTransportMonitor);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPTransportNotify
 *
 *  Broadcasts a transport notification event to all registered transport
 *  monitors.
 *
 *  Arguments:
 *      PSLMSGID        eventID             Event to broadcast
 *      PSLPARAM        aParamTransport     Transport specific parameter
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPTransportNotify(PSLMSGID eventID, PSLPARAM aParamTransport)
{
    NOTIFYEVENTINFO nei;
    PSLSTATUS       ps;

    /*  Only send notifications if we have a valid transport param
     */

    if (MTPTRANSPORTPARAM_UNKNOWN == aParamTransport)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Initialize the event information
     */

    nei.eventID = eventID;
    nei.aParamTransport = aParamTransport;

    /*  Provide some feedback
     */

    PSLTraceProgress(
                "MTPTransportNotify- Broadcasting: EID = 0x%08x\tAPT = 0x%08x",
                            eventID, aParamTransport);

    /*  Enumerate the context using the message post callback
     */

    ps = PSLCtxListEnum(_CtxMonitor, _MTPTransportMonitorEnumCtxListProc,
                            (PSLPARAM)&nei);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


