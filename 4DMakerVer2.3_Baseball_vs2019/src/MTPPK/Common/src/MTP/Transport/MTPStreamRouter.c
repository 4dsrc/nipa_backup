/*
 *  MTPStreamRouter.c
 *
 *  Contains definitions of the MTP Stream Router functionality.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPTransportPrecomp.h"
#include "MTPStreamRouter.h"

/*  Local Defines
 */

#define MTPSTREAMROUTEROBJ_COOKIE       'mstR'

/*  Local Types
 */

/*  Local Functions
 */

static PSLSTATUS _MTPStreamRouteRequestCheckData(MTPCONTEXT* pmtpctx,
                    PSLPARAM aParamParser);

/*  Local Data
 */

static PSL_CONST PSLUINT32  _RgdwRequestFlags[] =
{
    MTPMSGFLAG_EVENT_BUFFER_REQUEST,
    MTPMSGFLAG_DATA_BUFFER_REQUEST,
    MTPMSGFLAG_RESPONSE_BUFFER_REQUEST,
    MTPMSGFLAG_CMD_BUFFER_REQUEST
};

static MTPSTREAMROUTERVTBL  _VtblMTPStreamRouter =
{
    MTPStreamRouterDestroyBase,
    MTPStreamRouterCreateContextBase,
    MTPStreamRouterRoutePacketReceivedBase,
    PSLNULL,
};


/*
 *  MTPStreamRouterCreate
 *
 *  Creates an instance of the MTP Stream parser.  As the core stream parser
 *  object is expected to function as a base class additional space is
 *  reserved by the initialization function as reqeuested.
 *
 *  Arguments:
 *      PSLUINT32       cbObject            Total size of object requested
 *      MTPSTREAMROUTER*
 *                      pmtpstrmRouter      Return location for object
 *
 *  Returns:
 *      PSLSTATUS   PSL_SUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouterCreate(PSLUINT32 cbObject,
                    MTPSTREAMROUTER* pmtpstrmRouter)
{
    PSLSTATUS           ps;
    MTPSTREAMROUTEROBJ* pmtpstrmRO = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpstrmRouter)
    {
        *pmtpstrmRouter = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((sizeof(MTPSTREAMROUTEROBJ) > cbObject) || (PSLNULL == pmtpstrmRouter))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate a new parser
     */

    pmtpstrmRO = (MTPSTREAMROUTEROBJ*)PSLMemAlloc(PSLMEM_PTR, cbObject);
    if (PSLNULL == pmtpstrmRO)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Set the cookie so that we can validate this is one of our objects
     */

    pmtpstrmRO->dwCookie = MTPSTREAMROUTEROBJ_COOKIE;
#endif  /*  PSL_ASSERTS */

    /*  Add the VTBL information
     */

    pmtpstrmRO->vtbl = &_VtblMTPStreamRouter;

    /*  Return the parser handle
     */

    *pmtpstrmRouter = pmtpstrmRO;
    pmtpstrmRO = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSTREAMROUTERDESTROY(pmtpstrmRO);
    return ps;
}


/*
 *  MTPStreamRouterDestroyBase
 *
 *  Releases the specified MTP Stream Router.
 *
 *  Arguments:
 *      MTPSTREAMROUTER mtpstrmRouter       Router to destroy
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouterDestroyBase(MTPSTREAMROUTER mtpstrmRouter)
{
    PSLMSGQUEUE         mqClean;
    PSLSTATUS           ps;
    MTPSTREAMROUTEROBJ* pmtpstrmRO;

    /*  Make sure we have work to do
     */

    if (PSLNULL == mtpstrmRouter)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmRO = (MTPSTREAMROUTEROBJ*)mtpstrmRouter;
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrmRO->dwCookie);

    /*  Release internal objects
     */

    SAFE_MTPSTREAMPARSERDESTROY(pmtpstrmRO->mtpstrmCommand);
    SAFE_MTPSTREAMPARSERDESTROY(pmtpstrmRO->mtpstrmEvent);
    SAFE_MTPSTREAMBUFFERMGRDESTROY(pmtpstrmRO->mtpstrmbm);
    SAFE_PSLMSGPOOLDESTROY(pmtpstrmRO->mpTransport);

    /*  Extract the queue after everything has been closed down
     */

    mqClean = pmtpstrmRO->mqTransport;

    /*  And free the object
     */

    SAFE_PSLMEMFREE(pmtpstrmRO);

    /*  Clean up any messages remaining in the queue
     */

    if (PSLNULL != mqClean)
    {
        PSLMsgEnum(mqClean, MTPStreamRouterCleanQueueEnum, 0);
        PSLMsgQueueDestroy(mqClean);
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamRouterCreateContextBase
 *
 *  Creates a route after completing initialization of the transport
 *  information
 *
 *  Arguments:
 *      MTPSTREAMROUTER mtpstrm             MTP Stream Router object
 *      MTPCONTEXT**    ppmtpctx            Resulting MTP context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouterCreateContextBase(MTPSTREAMROUTER mtpstrm,
                    MTPCONTEXT** ppmtpctx)
{
    PSLSTATUS           ps;
    MTPCONTEXT*         pmtpctx = PSLNULL;
    MTPSTREAMROUTEROBJ* pmtpstrm;

    /*  Clear result for safety
     */

    if (PSLNULL != ppmtpctx)
    {
        *ppmtpctx = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpstrm) || (PSLNULL == ppmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpstrm = (MTPSTREAMROUTEROBJ*)mtpstrm;
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  Make sure that the stream object is sufficiently initialized
     */

    if ((PSLNULL == pmtpstrm->mtpstrmCommand) ||
        (PSLNULL == pmtpstrm->mtpstrmEvent) ||
        (PSLNULL == pmtpstrm->mtpstrmbm) ||
        (PSLNULL == pmtpstrm->mqTransport) ||
        (PSLNULL == pmtpstrm->mpTransport))
    {
        ps = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    /*  Allocate a new context for this item
     */

    ps = MTPRouteCreate(pmtpstrm->mqTransport, &pmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize the transport portion of the context
     */

    PSLASSERT(sizeof(MTPSTREAMROUTERCONTEXT) <=
                            pmtpctx->mtpTransportState.cbTransportData);
    ((MTPSTREAMROUTERCONTEXT*)
        (pmtpctx->mtpTransportState.pbTransportData))->pmtpstrm = pmtpstrm;

    /*  Return the new context
     */

    *ppmtpctx = pmtpctx;
    pmtpctx = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPROUTEDESTROY(pmtpctx);
    return ps;
}


/*
 *  MTPStreamRouterRoutePacketReceivedBase
 *
 *  Routes a received packet to any subclasses of the general stream router
 *  class.  If a class handles the packet specified it returns PSLSUCCESS,
 *  if it does not it return PSLSUCCESS_FALSE.
 *
 *  Arguments:
 *      MTPSTREAMROUTER mtpstrm             Router being used
 *      MTPCONTEXT*     pmtpctx             Context in which packet was received
 *      MTPSTREAMPARSER mtpstrmSrc          Parser on which the packet was
 *                                            received
 *      PSLUINT32       dwPacketType        Type of packet received
 *      PSLUINT32       cbPacket            Size of the packet received
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if handled, PSLSUCCESS_FALSE if not
 *
 */

PSLSTATUS PSL_API MTPStreamRouterRoutePacketReceivedBase(
                    MTPSTREAMROUTER mtpstrm, MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc, PSLUINT32 dwPacketType,
                    PSLUINT32 cbPacket)
{
    /*  Validate arguments- and if successful report that the base class does
     *  not support any special packet handling
     */

    return ((PSLNULL != mtpstrm) && (PSLNULL != pmtpctx) &&
                (PSLNULL != mtpstrmSrc)) ?
                            PSLSUCCESS_FALSE : PSLERROR_INVALID_PARAMETER;
    dwPacketType;
    cbPacket;
}


/*
 *  _MTPStreamRouteRequestCheckData
 *
 *  Queues a "request check data" message to the queue in the event that we
 *  have to make check to make sure that notifications are not lost if all
 *  events are ready
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to operate within
 *      PSLPARAM        aParamParser        Parser to check for data on
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPStreamRouteRequestCheckData(MTPCONTEXT* pmtpctx,
                    PSLPARAM aParamParser)
{
    PSLSTATUS           ps;
    PSLMSG*             pMsg = PSLNULL;
    MTPSTREAMROUTEROBJ* pmtpstrm;

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  We should not get here with pending data
     */

    PSLASSERT(!pmtpstrm->fReceivePending);

    /*  Allocate a new message
     */

    ps = PSLMsgAlloc(pmtpstrm->mpTransport, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And tell the core handler to check for available data
     */

    pMsg->msgID = MTPSTREAMROUTERMSG_CHECK_DATA_AVAILABLE;
    pMsg->aParam0 = (PSLPARAM)pmtpctx;
    pMsg->aParam1 = aParamParser;

    ps = PSLMsgPost(pmtpstrm->mqTransport, pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg = PSLNULL;
    ps = (PSLSUCCESS == ps) ? ps : PSLERROR_NOT_AVAILABLE;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPStreamRouteCmdReceived
 *
 *  Routes a command buffer to the appropriate handler after parsing
 *  the commmand into it
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to operate within
 *      MTPSTREAMPARSER mtpstrmSrc          Source connection for command
 *      PSLVOID*        pvBuffer            Buffer to parse command into
 *      PSLUINT64       cbBuffer            Length of buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteCmdReceived(MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc, PSLVOID* pvBuffer,
                    PSLUINT64 cbBuffer)
{
    PSLUINT32           cbPacket;
    PSLUINT32           dwType;
    PSLBOOL             fReceivePending;
    PSLSTATUS           ps;
    PSLMSG*             pMsg = PSLNULL;
    MTPSTREAMROUTEROBJ* pmtpstrm = PSLNULL;
    PXMTPOPERATION      pmtpOp;

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pvBuffer) ||
        (sizeof(MTPOPERATION) < cbBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (sizeof(MTPOPERATION) > cbBuffer)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  This may be an asynchronous handling of a buffer request- make sure the
     *  parsers are the same
     */

    if (mtpstrmSrc != pmtpstrm->mtpstrmCommand)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Ask the parser for the message type
     */

    ps = MTPStreamParserGetPacketType(mtpstrmSrc, &dwType, &cbPacket);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  At this point we have already validated the packet type and
     *  know it is a command- any other type or any other issue means
     *  that we have encountered a problem
     */

    PSLASSERT(MTPSTREAMPACKET_OPERATION_REQUEST == dwType);
    PSLASSERT(PSLNULL == pmtpctx->mtpContextState.mqHandler);
    if ((MTPSTREAMPACKET_OPERATION_REQUEST != dwType) ||
        (PSLNULL != pmtpctx->mtpContextState.mqHandler))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Clear the receive pending flag
     */

    fReceivePending = pmtpstrm->fReceivePending;
    pmtpstrm->fReceivePending = PSLFALSE;

    /*  Parse the operation
     */

    pmtpOp = (PXMTPOPERATION)pvBuffer;
    ps = MTPStreamParserReadOpRequest(mtpstrmSrc, &(pmtpstrm->dwDataPhase),
                        pmtpOp);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    if (PSLSUCCESS_WOULD_BLOCK == ps)
    {
        /*  Allocate a message to "fake" a buffer available so that we
         *  can get called again
         */

        ps = PSLMsgAlloc(pmtpstrm->mpTransport, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = MTPSTREAMBUFFERMGRMSG_BUFFER_AVAILABLE;
        pMsg->aParam0 = MTPBUFFER_COMMAND;
        pMsg->aParam1 = (PSLPARAM)mtpstrmSrc;
        pMsg->aParam2 = (PSLPARAM)pvBuffer;
        pMsg->alParam = cbBuffer;

        ps = PSLMsgPost(pmtpstrm->mqTransport, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = PSLNULL;
        goto Exit;
    }

    /*  Initialize the data state
     */

    pmtpstrm->dwTransactionID = MTPLoadUInt32(&(pmtpOp->dwTransactionID));
    pmtpstrm->dwDataState = MTPSTREAMDATASTATE_PENDING;
    pmtpstrm->qwTotalSize = MTP_DATA_SIZE_UNKNOWN;

    /*  And begin the route
     */

    ps = MTPRouteBegin(pmtpctx, pmtpstrm->mpTransport, pmtpOp,
                        sizeof(*pmtpOp));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pvBuffer = PSLNULL;

    /*  If we entered this thinking that we had a pending receive we may have
     *  lost a data available notification if the host has already stopped
     *  sending data- for example the pending receive occurred on the last
     *  chunk of data and the end data has already been sent.  In this case
     *  we may need to force the packet handling to restart.  To do so we
     *  queue a "CHECK_DATA_AVAILABLE" message on the stream in question to
     *  get things moving again
     */

    if (fReceivePending)
    {
        /*  Request a check for pending data on the command channel
         */

        ps = _MTPStreamRouteRequestCheckData(pmtpctx,
                            MTPSTREAMPARSERTYPE_COMMAND);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);
    return ps;
}


/*
 *  MTPStreamRouteDataReceived
 *
 *  Routes data received on the connection to the handler using the
 *  buffer provided
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to operate within
 *      MTPSTREAMPARSER mtpstrmSrc          Source connection for data
 *      PSLVOID*        pvBuffer            Buffer to use
 *      PSLUINT64       cbBuffer            Size of the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteDataReceived(MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc, PSLVOID* pvBuffer,
                    PSLUINT64 cbBuffer)
{
    PSLUINT32           cbActual;
    PSLUINT32           cbDesired;
    PSLUINT32           cbRead;
    PSLUINT32           cbPacket;
    PSLUINT32           cbReceived;
    PSLUINT32           dwType;
    PSLUINT32           dwTransactionID;
    PSLBOOL             fReceivePending;
    PSLSTATUS           ps;
    PSLSTATUS           psRead;
    PSLMSG*             pMsg = PSLNULL;
    MTPSTREAMROUTEROBJ* pmtpstrm = PSLNULL;

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == mtpstrmSrc) ||
        (PSLNULL == pvBuffer) || (0xffffffff <= cbBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (0 == cbBuffer)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  This may be an asynchronous handling of a buffer request- make sure the
     *  parsers are the same
     */

    if (mtpstrmSrc != pmtpstrm->mtpstrmCommand)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  If we have not received a start packet or already started receiving
     *  data than this packet is incorrect
     */

    PSLASSERT((MTPSTREAMDATASTATE_IN_READY == pmtpstrm->dwDataState) ||
        (MTPSTREAMDATASTATE_IN_START == pmtpstrm->dwDataState));
    if ((MTPSTREAMDATASTATE_IN_READY != pmtpstrm->dwDataState) &&
        (MTPSTREAMDATASTATE_IN_START != pmtpstrm->dwDataState))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Ask the parser for the message type
     */

    ps = MTPStreamParserGetPacketType(mtpstrmSrc, &dwType, &cbPacket);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  At this point we have already validated the packet type and
     *  know it is inbound data- any other type or lack of a handler
     *  indicates a problem with the packet
     */

    PSLASSERT((MTPSTREAMPACKET_DATA == dwType) ||
                            (MTPSTREAMPACKET_END_DATA == dwType));
    PSLASSERT(PSLNULL != pmtpctx->mtpContextState.mqHandler);
    if (((MTPSTREAMPACKET_DATA != dwType) &&
                (MTPSTREAMPACKET_END_DATA != dwType)) ||
        (PSLNULL == pmtpctx->mtpContextState.mqHandler))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Reset the receive pending flag after we cache its value so that we
     *  can check for pending receives on exit
     */

    fReceivePending = pmtpstrm->fReceivePending;
    pmtpstrm->fReceivePending = PSLFALSE;

    /*  We want to fill in the entire data buffer so initialize
     *  the sizes
     */

    cbActual = (PSLUINT32)cbBuffer;
    cbReceived = 0;

    /*  And read the data in chunks until done
     */

    do
    {
        /*  See if we need to allocate a new chunk
         */

        if (PSLNULL == pvBuffer)
        {
            /*  Determine the size of buffer to use
             */

            cbDesired = min((PSLUINT32)cbBuffer, cbPacket);

            /*  Allocate a buffer to receive the data into
             */

            ps = MTPStreamBufferMgrRequest(pmtpstrm->mtpstrmbm,
                            MTPBUFFER_TRANSMIT_DATA, cbDesired,
                            (PSLPARAM)mtpstrmSrc, &pvBuffer, &cbActual,
                            pmtpstrm->mqTransport, pmtpstrm->mpTransport);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  If we did not get a buffer back remember that data
             *  is pending and wait for the buffer to be returned
             */

            if (PSLSUCCESS != ps)
            {
                /*  Mark data as pending
                 */

                pmtpstrm->fReceivePending = PSLTRUE;
                goto Exit;
            }
            cbReceived = 0;
        }

        /*  Read the next chunk of data
         */

        switch (dwType)
        {
        case MTPSTREAMPACKET_DATA:
            /*  Retrieve the data
             */

            psRead = MTPStreamParserReadData(mtpstrmSrc, &dwTransactionID,
                            (PSLBYTE*)pvBuffer + cbReceived,
                            (cbActual - cbReceived), &cbRead);
            if (PSL_FAILED(psRead))
            {
                ps = psRead;
                goto Exit;
            }

            /*  Update the data state
             */

            pmtpstrm->dwDataState = MTPSTREAMDATASTATE_IN_START;
            break;

        case MTPSTREAMPACKET_END_DATA:
            /*  Retrieve the data
             */

            psRead = MTPStreamParserReadEndData(mtpstrmSrc, &dwTransactionID,
                            (PSLBYTE*)pvBuffer + cbReceived,
                            (cbActual - cbReceived), &cbRead);
            if (PSL_FAILED(psRead))
            {
                ps = psRead;
                goto Exit;
            }

            /*  Update the data state
             */

            pmtpstrm->dwDataState = MTPSTREAMDATASTATE_IN_END;
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Validate the transaction ID- it should either match or
         *  be not applicable if we are chunking
         */

        if ((PSLSUCCESS == psRead) &&
            (dwTransactionID != pmtpstrm->dwTransactionID) &&
            (MTPTRANSACTION_NOT_APPLICABLE != dwTransactionID))
        {
            ps = PSLERROR_INVALID_PACKET;
            goto Exit;
        }

        /*  If we have not filled the chunk entirely and data
         *  is still available keep reading
         */

        cbReceived += cbRead;
        if ((PSLSUCCESS_DATA_AVAILABLE == psRead) &&
            (0 < (cbActual - cbReceived)))
        {
            continue;
        }

        /*  If we received no actual data then we want to send NULL
         *  as the buffer rather than locking this buffer for the
         *  duration of the transaction
         */

        if (0 == cbReceived)
        {
            SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);

            /*  If this is not an end data packet then we should not
             *  forward the message
             */

            if (MTPSTREAMPACKET_END_DATA != dwType)
            {
                PSLASSERT(PSLSUCCESS_DATA_AVAILABLE != psRead);
                ps = psRead;
                break;
            }
        }

        /*  Forward the buffer to the handler
         */

        ps = PSLMsgAlloc(pmtpstrm->mpTransport, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = MTPMSG_DATA_AVAILABLE;
        pMsg->aParam0 = (PSLPARAM)pmtpctx;
        pMsg->aParam2 = (PSLPARAM)pvBuffer;
        pMsg->aParam3 = (MTPSTREAMPACKET_END_DATA != dwType) ?
                    0 : MTPMSGFLAG_TERMINATE;
        pMsg->alParam = cbReceived;

        ps = MTPRouteMsgToHandler(pmtpctx, pMsg);
        if (PSLSUCCESS != ps)
        {
            if (PSLSUCCESS_NOT_AVAILABLE == ps)
            {
                /* router couldn't find a destination queue
                 * to post the message
                 */
                ps = PSLERROR_NOT_AVAILABLE;
                pMsg = PSLNULL;
            }
            goto Exit;
        }

        /*  Done with the message and the buffer
         */

        pvBuffer = PSLNULL;
        pMsg = PSLNULL;
    }
    while (PSLSUCCESS_DATA_AVAILABLE == psRead);

    /*  If we entered this thinking that we had a pending receive we may have
     *  lost a data available notification if the host has already stopped
     *  sending data- for example the pending receive occurred on the last
     *  chunk of data and the end data has already been sent.  In this case
     *  we may need to force the packet handling to restart.  To do so we
     *  queue a "CHECK_DATA_AVAILABLE" message on the stream in question to
     *  get things moving again
     */

    if (fReceivePending)
    {
        /*  Request a check for pending data on the command channel
         */

        ps = _MTPStreamRouteRequestCheckData(pmtpctx,
                            MTPSTREAMPARSERTYPE_COMMAND);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

Exit:
    SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPStreamRouteCancelReceived
 *
 *  Handles the reception of a cancel packet from the initiator
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to operated within
 *      MTPSTREAMPARSER mtpstrmSrc          Source connection with data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteCancelReceived(MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc)
{
    PSLUINT32           cbPacket;
    PSLUINT32           cbRead;
    PSLUINT32           dwType;
    PSLUINT32           dwTransactionID;
    PSLUINT32           dwCancelID;
    PSLUINT32           fDone;
    MTPRESPONSE         mtpResponse;
    MTPSTREAMPARSER     mtpstrmAlt;
    PSLSTATUS           ps;
    MTPSTREAMROUTEROBJ* pmtpstrm;
    PSLUINT32           cbActual = 0;
    PSLVOID*            pvBuffer = PSLNULL;

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == mtpstrmSrc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  Retrieve the cancel request from the parser
     */

    ps = MTPStreamParserReadCancel(mtpstrmSrc, &dwCancelID);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  If the transaction does not match the current transaction fail the
     *  request
     */

    if (dwCancelID != pmtpstrm->dwTransactionID)
    {
        /*  Currently handling of asynchronous transacation cancelling is
         *  not supported
         */

        MTPStoreUInt16(&(mtpResponse.wRespCode),
                            MTP_RESPONSECODE_INVALIDTRANSACTIONID);
        MTPStoreUInt32(&(mtpResponse.dwTransactionID), dwCancelID);
        ps = MTPStreamParserSendOpResponse(pmtpstrm->mtpstrmCommand,
                            &mtpResponse, MTPRESPONSE_MIN_SIZE);
        goto Exit;
    }

    /*  We have received the cancel request on either the command
     *  or the event pipe- depending upon which one we received we
     *  need to go check the other pipe for work to do
     */

    if (mtpstrmSrc == pmtpstrm->mtpstrmCommand)
    {
        mtpstrmAlt = pmtpstrm->mtpstrmEvent;
    }
    else
    {
        PSLASSERT(mtpstrmSrc == pmtpstrm->mtpstrmEvent);
        mtpstrmAlt = pmtpstrm->mtpstrmCommand;
    }

    /*  We need to make sure that the command/data pipe has been emptied
     *  at this point so start looking specifically for data that needs to
     *  be flushed off that pipe
     */

    cbActual = 0;
    fDone = PSLFALSE;
    while (!fDone)
    {
        /*  The cancellation rules state that we need to wait for
         *  the matching cancel on the other pipe before we
         *  continue processing- loop here until we get data
         */

        do
        {
            /*  Ask the command parser for the message type
             */

            ps = MTPStreamParserGetPacketType(mtpstrmAlt, &dwType, &cbPacket);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  If there is no packet pending then we have no work to do
             */

            if (PSLSUCCESS_WOULD_BLOCK == ps)
            {
                /*  With the socket running in non-blocking mode we
                 *  really just need to wait for the packet to arrive
                 */

                PSLSleep(250);
            }
        }
        while (PSLSUCCESS_WOULD_BLOCK == ps);

        /*  Determine how to handle the packet we just recieved
         */

        switch (dwType)
        {
        case MTPSTREAMPACKET_CANCEL:
            /*  We have found the packet on the alternate channel-
             *  read it
             */

            ps = MTPStreamParserReadCancel(mtpstrmAlt, &dwTransactionID);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  If we got a different cancellation then report an
             *  invalid packet
             */

            PSLASSERT(dwTransactionID == dwCancelID);
            if (dwTransactionID != dwCancelID)
            {
                ps = PSLERROR_INVALID_PACKET;
                goto Exit;
            }

            /*  We are done looking for data
             */

            fDone = PSLTRUE;
            break;

        case MTPSTREAMPACKET_START_DATA:
            ps = MTPStreamParserReadStartData(mtpstrmAlt, &dwTransactionID,
                            &(pmtpstrm->qwTotalSize));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        case MTPSTREAMPACKET_DATA:
        case MTPSTREAMPACKET_END_DATA:
            /*  Retrieve a flush buffer to receive the data into if we
             *  do not already have one
             */

            if ((PSLNULL == pvBuffer) || (0 == cbActual))
            {
                PSLASSERT((PSLNULL == pvBuffer) && (0 == cbActual));
                ps = MTPStreamBufferMgrRequest(pmtpstrm->mtpstrmbm,
                            MTPSTREAMBUFFER_FLUSH_DATA,
                            MTPSTREAM_FLUSH_BUFFER_SIZE,
                            PSLNULL,
                            &pvBuffer, &cbActual,
                            PSLNULL,
                            PSLNULL);
                if (PSL_FAILED(ps))
                {
                    goto Exit;
                }
            }
            PSLASSERT(PSLNULL != pvBuffer);
            PSLASSERT(MTPSTREAM_FLUSH_BUFFER_SIZE == cbActual);

            /*  We should get a buffer back synchronously
             *  FLUSH_DATA buffer should always be available
             */

            if (PSLSUCCESS != ps)
            {
                PSLASSERT(PSLFALSE);
                goto Exit;
            }

            /*  Read the data in chunks until done
             */

            do
            {
                /*  Read the next chunk of data
                 */

                switch (dwType)
                {
                case MTPSTREAMPACKET_DATA:
                    /*  Retrieve the data
                     */

                    ps = MTPStreamParserReadData(mtpstrmAlt, &dwTransactionID,
                            pvBuffer, cbActual, &cbRead);
                    if (PSL_FAILED(ps))
                    {
                        goto Exit;
                    }
                    break;

                case MTPSTREAMPACKET_END_DATA:
                    /*  Retrieve the data
                     */

                    ps = MTPStreamParserReadEndData(mtpstrmAlt,
                            &dwTransactionID, pvBuffer, cbActual,
                            &cbRead);
                    if (PSL_FAILED(ps))
                    {
                        goto Exit;
                    }
                    break;

                default:
                    /*  Should never get here
                     */

                    PSLASSERT(PSLFALSE);
                    ps = PSLERROR_UNEXPECTED;
                    goto Exit;
                }

                /*  Validate the transaction ID- it should either match or
                 *  be not applicable if we are chunking
                 */

                if ((PSLSUCCESS == ps) &&
                    (dwTransactionID != pmtpstrm->dwTransactionID) &&
                    (MTPTRANSACTION_NOT_APPLICABLE != dwTransactionID))
                {
                    ps = PSLERROR_INVALID_PACKET;
                    goto Exit;
                }
            }
            while (PSLSUCCESS_DATA_AVAILABLE == ps);
            break;

        case MTPSTREAMPACKET_OPERATION_REQUEST:
        case MTPSTREAMPACKET_PROBE_REQUEST:
        case MTPSTREAMPACKET_UNKNOWN:
        case MTPSTREAMPACKET_OPERATION_RESPONSE:
        case MTPSTREAMPACKET_PROBE_RESPONSE:
        case MTPSTREAMPACKET_EVENT:
            /*  These packets are all invalid to recieve within the context of
             *  handling a cancel operation- report a problem
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_INVALID_PACKET;
            goto Exit;

        default:
            /*  Do not expect to get here- we should have accounted for all of the
             *  packets
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }
    }

    /*  While we are cancelling the "current transaction" we may have
     *  already finished the operation- ignore the cancellation
     *  request now that we have completed.
     */

    if (PSLNULL == pmtpctx->mtpContextState.mqHandler)
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  And terminate the route
     */

    ps = MTPStreamRouteTerminate(pmtpctx, MTPMSG_CANCEL, dwCancelID);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Clear the pending buffer flag
     */

    pmtpstrm->fReceivePending = PSLFALSE;

    /*  Make sure that the command and event socket has been reset
     *  so that any data following the cancel will be received
     */

    ps = MTPStreamRouterResetDataAvailable(pmtpstrm, pmtpstrm->mtpstrmCommand);
    PSLASSERT(PSL_SUCCEEDED(ps));
    ps = MTPStreamRouterResetDataAvailable(pmtpstrm, pmtpstrm->mtpstrmEvent);
    PSLASSERT(PSL_SUCCEEDED(ps));

    /*  Fill out the response code
     */

    MTPStoreUInt16(&(mtpResponse.wRespCode),
                            MTP_RESPONSECODE_TRANSACTIONCANCELLED);
    MTPStoreUInt32(&(mtpResponse.dwTransactionID), dwCancelID);

    /*  And send a response packet letting the everyone know that the response
     *  was cancelled
     */

    ps = MTPStreamParserSendOpResponse(pmtpstrm->mtpstrmCommand, &mtpResponse,
                            MTPRESPONSE_MIN_SIZE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);
    return ps;
}



/*
 *  MTPStreamRouteDataAvailable
 *
 *  Routes data available messages from a socket to the appropriate
 *  destination handler.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to operate within
 *      MTPIPPARSER     mtpipSrc            Source connection for the data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteDataAvailable(MTPCONTEXT* pmtpctx,
                    MTPSTREAMPARSER mtpstrmSrc)
{
    PSLUINT32           cbActual;
    PSLUINT32           cbPacket;
    PSLUINT32           dwType;
    PSLUINT32           dwTransactionID;
    PSLUINT32           fHandled = PSLFALSE;
    PSLSTATUS           ps;
    PSLMSG*             pMsg = PSLNULL;
    MTPSTREAMROUTEROBJ* pmtpstrm;
    PSLVOID*            pvBuffer = PSLNULL;

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == mtpstrmSrc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  If we have a pending receive we are waiting for a buffer to
     *  become available- ignore this notification
     */

    if (pmtpstrm->fReceivePending)
    {
        ps = PSLSUCCESS_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Loop so that we process all available packets on each notification
     */

    do
    {
        /*  Ask the parser for the message type
         */

        ps = MTPStreamParserGetPacketType(mtpstrmSrc, &dwType, &cbPacket);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Make sure we had enough data to determine the packet type
         */

        if (PSLSUCCESS_WOULD_BLOCK == ps)
        {
            ps = fHandled ? PSLSUCCESS : PSLSUCCESS_WOULD_BLOCK;
            break;
        }

        /*  Pass off handling to any subclasses
         */

        ps = MTPStreamRouterRoutePacketReceived(pmtpstrm, pmtpctx,
                            mtpstrmSrc, dwType, cbPacket);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Determine if the packet was handled or not
         */

        if (PSLSUCCESS == ps)
        {
            /*  Subclass handled the packet- start looking for the next
             *  packet
             */

            fHandled = PSLTRUE;
            continue;
        }

        /*  Determine how to handle the packet we just recieved
         */

        switch (dwType)
        {
        case MTPSTREAMPACKET_OPERATION_REQUEST:
            /*  We should only receive a command if we do not have a currently
             *  active route
             */

            PSLASSERT(PSLNULL == pmtpctx->mtpContextState.mqHandler);
            if (PSLNULL != pmtpctx->mtpContextState.mqHandler)
            {
                ps = PSLERROR_INVALID_PACKET;
                goto Exit;
            }

            /*  Retrieve a command buffer
             */

            ps = MTPStreamBufferMgrRequest(pmtpstrm->mtpstrmbm,
                            MTPBUFFER_COMMAND, sizeof(MTPOPERATION),
                            (PSLPARAM)mtpstrmSrc, &pvBuffer, &cbActual,
                            pmtpstrm->mqTransport, pmtpstrm->mpTransport);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  If the buffer was not available remember that a receive is
             *  pending and wait for the buffer
             */

            if (PSLSUCCESS != ps)
            {
                pmtpstrm->fReceivePending = PSLTRUE;
                goto Exit;
            }

            /*  Handle the buffer we received- remember we could still
             *  block trying to read all of the data so we only want to
             *  continue on a success
             */

            ps = MTPStreamRouteCmdReceived(pmtpctx, mtpstrmSrc, pvBuffer,
                            cbActual);
            pvBuffer = PSLNULL;
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Determine if we have handled the packet
             */

            fHandled = (PSLSUCCESS == ps);
            break;

        case MTPSTREAMPACKET_START_DATA:
            /*  We are beginning a data operation- for this to be valid we
             *  should have a destination
             */

            PSLASSERT(PSLNULL != pmtpctx->mtpContextState.mqHandler);
            if (PSLNULL == pmtpctx->mtpContextState.mqHandler)
            {
                ps = PSLERROR_INVALID_PACKET;
                goto Exit;
            }

            /*  If we are not in the appropriate state reject the data request
             */

            PSLASSERT(MTPSTREAMDATASTATE_PENDING == pmtpstrm->dwDataState);
            if (MTPSTREAMDATASTATE_PENDING != pmtpstrm->dwDataState)
            {
                ps = PSLERROR_INVALID_PACKET;
                goto Exit;
            }

            /*  Retrieve the data size
             */

            ps = MTPStreamParserReadStartData(mtpstrmSrc, &dwTransactionID,
                                &(pmtpstrm->qwTotalSize));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  If the transaction does not match we have an error
             */

            if (dwTransactionID != pmtpstrm->dwTransactionID)
            {
                ps = PSLERROR_INVALID_PACKET;
                goto Exit;
            }

            /*  Notify the handler of the upcoming data size
             */

            ps = PSLMsgAlloc(pmtpstrm->mpTransport, &pMsg);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            pMsg->msgID = MTPMSG_DATA_START;
            pMsg->aParam0 = (PSLPARAM)pmtpctx;
            pMsg->alParam = pmtpstrm->qwTotalSize;

            ps = MTPRouteMsgToHandler(pmtpctx, pMsg);
            if (PSLSUCCESS != ps)
            {
                if (PSLSUCCESS_NOT_AVAILABLE == ps)
                {
                    /* router couldn't find a destination queue
                     * to post the message
                     */
                    ps = PSLERROR_NOT_AVAILABLE;
                    pMsg = PSLNULL;
                }
                goto Exit;
            }
            pMsg = PSLNULL;

            /*  Remember that we have the size
             */

            pmtpstrm->dwDataState = MTPSTREAMDATASTATE_IN_READY;

            /*  Remember that we have handled the packet
             */

            fHandled = PSLTRUE;
            break;

        case MTPSTREAMPACKET_DATA:
        case MTPSTREAMPACKET_END_DATA:
            /*  We are receiving data- we should have a destination
             */

            PSLASSERT(PSLNULL != pmtpctx->mtpContextState.mqHandler);
            if (PSLNULL == pmtpctx->mtpContextState.mqHandler)
            {
                ps = PSLERROR_INVALID_PACKET;
                goto Exit;
            }

            /*  Retrieve a data buffer
             */

            PSLASSERT(mtpstrmSrc == pmtpstrm->mtpstrmCommand);
            ps = MTPStreamBufferMgrRequest(pmtpstrm->mtpstrmbm,
                                MTPBUFFER_RECEIVE_DATA,
                                cbPacket, (PSLPARAM)mtpstrmSrc,
                                &pvBuffer, &cbActual,
                                pmtpstrm->mqTransport,
                                pmtpstrm->mpTransport);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  If the buffer was not available remember that a receive is
             *  pending and wait for the buffer
             */

            if (PSLSUCCESS != ps)
            {
                pmtpstrm->fReceivePending = PSLTRUE;
                goto Exit;
            }

            /*  Handle the buffer we received- remember we could still
             *  block trying to read all of the data so we only want to
             *  continue on a success
             */

            ps = MTPStreamRouteDataReceived(pmtpctx, mtpstrmSrc, pvBuffer,
                            cbActual);
            pvBuffer = PSLNULL;
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Determine if we have handled the packet
             */

            fHandled = (PSLSUCCESS == ps);
            break;

        case MTPSTREAMPACKET_CANCEL:
            /*  Handle the cancel packet
             */

            ps = MTPStreamRouteCancelReceived(pmtpctx, mtpstrmSrc);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Report the packet handled
             */

            fHandled = PSLTRUE;
            break;

        case MTPSTREAMPACKET_PROBE_REQUEST:
            /*  Read the probe request
             */

            ps = MTPStreamParserReadProbeRequest(mtpstrmSrc);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  And send the response
             */

            ps = MTPStreamParserSendProbeResponse(mtpstrmSrc,
                        ((PSLNULL == pmtpctx->mtpContextState.mqHandler) ?
                            MTP_RESPONSECODE_OK : MTP_RESPONSECODE_DEVICEBUSY));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Report the packet handled
             */

            fHandled = PSLTRUE;
            break;

        case MTPSTREAMPACKET_UNKNOWN:
        case MTPSTREAMPACKET_OPERATION_RESPONSE:
        case MTPSTREAMPACKET_PROBE_RESPONSE:
        case MTPSTREAMPACKET_EVENT:
            /*  These packets are all invalid to recieve within the context of a
             *  bound, functional router
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_INVALID_PACKET;
            goto Exit;

        default:
            /*  Do not expect to get here- we should have accounted for all
             *  of the packets
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_INVALID_PACKET;
            goto Exit;
        }
    }
    while (fHandled);

Exit:
    SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPStreamRouteDataSend
 *
 *  Sends data from the handler over the transport
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to operate within
 *      PSLUINT32       dwSequenceID        Sequence ID for this operation
 *      PSLVOID*        pvData              Data to send
 *      PSLUINT64       cbData              Size of data to send
 *      PSLUINT32       dwDataInfo          MTP data info flags
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteDataSend(MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwSequenceID, PSLVOID* pvData, PSLUINT64 cbData,
                    PSLUINT32 dwDataInfo)
{
    PSLMSG*             pMsg = PSLNULL;
    PSLSTATUS           ps;
    MTPSTREAMROUTEROBJ* pmtpstrm;

    /*  Validate Arguments- we only support 32-bit data sends through this
     *  interface
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pvData) ||
        ((0 == cbData) && !(MTPMSGFLAG_TERMINATE & dwDataInfo)) ||
        ((0xffffffff & cbData) != cbData))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  Make sure that we are sending data in the correct phase
     */

    if ((MTPSTREAM_NO_DATA_OR_DATA_IN != pmtpstrm->dwDataPhase) &&
        (MTPSTREAM_DATA_PHASE_UNKNOWN != pmtpstrm->dwDataPhase))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  The start data packet should have already been sent by this point-
     *  any other data state makes this an invlid request
     */

    PSLASSERT(MTPSTREAMDATASTATE_OUT_START == pmtpstrm->dwDataState);
    if (MTPSTREAMDATASTATE_OUT_START != pmtpstrm->dwDataState)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Allocate a message so that we can notify the handler that the data
     *  was sent successfully
     */

    ps = PSLMsgAlloc(pmtpstrm->mpTransport, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Send the data- if the terminate flag is set use the end data option
     *  instead
     */

    if (MTPMSGFLAG_TERMINATE & dwDataInfo)
    {
        ps = MTPStreamParserSendEndData(pmtpstrm->mtpstrmCommand,
                            pmtpstrm->dwTransactionID, pvData,
                            (PSLUINT32)cbData);
    }
    else
    {
        ps = MTPStreamParserSendData(pmtpstrm->mtpstrmCommand,
                            pmtpstrm->dwTransactionID, pvData,
                            (PSLUINT32)cbData);
    }
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Notify the handler that the data was sent succesfully
     */

    pMsg->msgID = MTPMSG_DATA_SENT;
    pMsg->aParam0 = (PSLPARAM)pmtpctx;
    pMsg->aParam1 = dwSequenceID;
    pMsg->alParam = cbData;
    ps = MTPRouteMsgToHandler(pmtpctx, pMsg);
    if (PSLSUCCESS != ps)
    {
        if (PSLSUCCESS_NOT_AVAILABLE == ps)
        {
            /* router couldn't find a destination queue
             * to post the message
             */
            ps = PSLERROR_NOT_AVAILABLE;
            pMsg = PSLNULL;
        }
        goto Exit;
    }
    pMsg = PSLNULL;

Exit:
    SAFE_MTPSTREAMBUFFERMGRRELEASE(pvData);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPStreamRouteBufferAvailable
 *
 *  Forwards a buffer available message from the transport to the handler
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context for this operation
 *      PSLUINT32       dwBufferType        Buffer type
 *      PSLPARAM        aParam              Parameter associated with buffer
 *      PSLVOID*        pvBuffer            Buffer that is available
 *      PSLUINT64       cbBuffer            Size of the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteBufferAvailable(MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwBufferType, PSLPARAM aParam,
                    PSLVOID* pvBuffer, PSLUINT64 cbBuffer)
{
    PSLMSGID            msgID;
    PSLSTATUS           ps;
    PSLMSG*             pMsg = PSLNULL;
    MTPSTREAMROUTEROBJ* pmtpstrm;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pvBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  Determine the correct message to send
     */

    switch (dwBufferType)
    {
    case MTPBUFFER_COMMAND:
        /*  Notify the socket that it should start sending data available
         *  messages again
         */

        ps = MTPStreamRouterResetDataAvailable(pmtpstrm,
                            (MTPSTREAMPARSER)aParam);
        PSLASSERT(PSL_SUCCEEDED(ps));

        /*  We requested a buffer to parse a command but one was not
         *  available- parse the command now
         */

        ps = MTPStreamRouteCmdReceived(pmtpctx, (MTPSTREAMPARSER)aParam,
                            pvBuffer, cbBuffer);
        pvBuffer = PSLNULL;
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        msgID = MTPMSG_UNKNOWN;
        break;

    case MTPBUFFER_RECEIVE_DATA:
        /*  Notify the socket that it should start sending data available
         *  messages again
         */

        ps = MTPStreamRouterResetDataAvailable(pmtpstrm,
                            (MTPSTREAMPARSER)aParam);
        PSLASSERT(PSL_SUCCEEDED(ps));

        /*  We requested a buffer to receive data into but one was
         *  not available- parse the command now
         */

        ps = MTPStreamRouteDataReceived(pmtpctx, (MTPSTREAMPARSER)aParam,
                            pvBuffer, cbBuffer);
        pvBuffer = PSLNULL;
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        msgID = MTPMSG_UNKNOWN;
        break;

    case MTPBUFFER_TRANSMIT_DATA:
        msgID = MTPMSG_DATA_BUFFER_AVAILABLE;
        break;

    case MTPBUFFER_RESPONSE:
        msgID = MTPMSG_RESPONSE_BUFFER_AVAILABLE;
        break;

    case MTPBUFFER_EVENT:
        msgID = MTPMSG_EVENT_BUFFER_AVAILABLE;
        break;

    default:
        /*  Should never get here
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  If we do not have a message to send we are done
     */

    if (MTPMSG_UNKNOWN == msgID)
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Allocate a message to forward the buffer with
     */

    ps = PSLMsgAlloc(pmtpstrm->mpTransport, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    pMsg->msgID = msgID;
    pMsg->aParam0 = (PSLPARAM)pmtpctx;
    pMsg->aParam1 = aParam;
    pMsg->aParam2 = (PSLPARAM)pvBuffer;
    pMsg->aParam3 = 0;
    pMsg->alParam = cbBuffer;
    ps = MTPRouteMsgToHandler(pmtpctx, pMsg);
    if (PSLSUCCESS != ps)
    {
        if (PSLSUCCESS_NOT_AVAILABLE == ps)
        {
            /* router couldn't find a destination queue
             * to post the message
             */
            ps = PSLERROR_NOT_AVAILABLE;
            pMsg = PSLNULL;
        }
        goto Exit;
    }

    pMsg = PSLNULL;
    pvBuffer = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);
    return ps;
}


/*
 *  MTPStreamRouteBufferRequest
 *
 *  Attempts to fulfill a buffer request and routes the resulting buffer
 *  to the context
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpCtx             MTP context for the request
 *      PSLMSGID        msgID               MTP message for the request
 *      PSLUINT32       dwSequenceID        Sequence ID for the request
 *      PSLUINT64       cbRequested         Bytes requested
 *      PSLUINT32       dwDataInfo          MTP data info flags
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteBufferRequest(MTPCONTEXT* pmtpctx,
                    PSLMSGID msgID, PSLUINT32 dwSequenceID,
                    PSLUINT64 cbRequested, PSLUINT32 dwDataInfo)
{
    PSLUINT32           cbActual;
    PSLUINT32           cbDesired;
    PSLUINT32           dwBuffer;
    PSLMSGID            msgDest;
    PSLSTATUS           ps;
    PSLMSG*             pMsg = PSLNULL;
    MTPSTREAMROUTEROBJ* pmtpstrm;
    PSLVOID*            pvBuffer = PSLNULL;

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  Make sure the request is valid in the router context
     */

    switch (msgID)
    {
    case MTPMSG_DATA_BUFFER_REQUEST:
        /*  Make sure that we are sending data in the correct phase
         */

        if ((MTPSTREAM_NO_DATA_OR_DATA_IN != pmtpstrm->dwDataPhase) &&
            (MTPSTREAM_DATA_PHASE_UNKNOWN != pmtpstrm->dwDataPhase))
        {
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }

        /*  First determine where we are at in our data handling- start by
         *  figuring out if we need to store this request as the total size
         */

        if (MTPMSGFLAG_COMPLETE & dwDataInfo)
        {
            /*  The complete flag is only valid when the data state is pending
             */

            PSLASSERT(MTPSTREAMDATASTATE_PENDING == pmtpstrm->dwDataState);
            if (MTPSTREAMDATASTATE_PENDING != pmtpstrm->dwDataState)
            {
                ps = PSLERROR_INVALID_OPERATION;
                goto Exit;
            }

            /*  We should never get a complete flag with unknown data size
             */

            PSLASSERT(MTP_DATA_SIZE_UNKNOWN != cbRequested);
            if (MTP_DATA_SIZE_UNKNOWN == cbRequested)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  Stash the total size of the transmission and remember that
             *  the length is known
             */

            pmtpstrm->qwTotalSize = cbRequested;
            pmtpstrm->dwDataState = MTPSTREAMDATASTATE_OUT_READY;
        }
        else if (MTPSTREAMDATASTATE_PENDING == pmtpstrm->dwDataState)
        {
            /*  This is the first request for a data buffer and a total size
             *  is not indicated- accept unknown size as the data size
             */

            PSLASSERT(MTP_DATA_SIZE_UNKNOWN == pmtpstrm->qwTotalSize);
            pmtpstrm->dwDataState = MTPSTREAMDATASTATE_OUT_READY;
        }

        /*  Send the start data packet if necessary
         */

        if (MTPSTREAMDATASTATE_OUT_READY == pmtpstrm->dwDataState)
        {
            ps = MTPStreamParserSendStartData(pmtpstrm->mtpstrmCommand,
                            pmtpstrm->dwTransactionID,
                            pmtpstrm->qwTotalSize);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Remember that data start has been sent
             */

            pmtpstrm->dwDataState = MTPSTREAMDATASTATE_OUT_START;
        }

        /*  Request the full buffer at once
         */

        cbDesired = (PSLUINT32)cbRequested;

        /*  Return a data buffer
         */

        dwBuffer = MTPBUFFER_TRANSMIT_DATA;
        msgDest = MTPMSG_DATA_BUFFER_AVAILABLE;
        break;

    case MTPMSG_EVENT_BUFFER_REQUEST:
        /*  Return an event buffer
         */

        cbDesired = sizeof(MTPEVENT);
        dwBuffer = MTPBUFFER_EVENT;
        msgDest = MTPMSG_EVENT_BUFFER_AVAILABLE;
        break;

    case MTPMSG_RESPONSE_BUFFER_REQUEST:
        /*  Return a response buffer
         */

        cbDesired = sizeof(MTPRESPONSE);
        dwBuffer = MTPBUFFER_RESPONSE;
        msgDest = MTPMSG_RESPONSE_BUFFER_AVAILABLE;
        break;

    case MTPMSG_CMD_BUFFER_REQUEST:
        /*  Command buffers are only created by the router, never by
         *  anyone else.  Reject the request.
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;

    default:
        /*  Should never get here
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Retrieve the buffer requested- if the buffer was not available
     *  we will wait for a notification that it is
     */

    ps = MTPStreamBufferMgrRequest(pmtpstrm->mtpstrmbm, dwBuffer,
                            cbDesired, 0, &pvBuffer, &cbActual,
                            pmtpstrm->mqTransport, pmtpstrm->mpTransport);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    /*  Send a message response to the handler
     */

    ps = PSLMsgAlloc(pmtpstrm->mpTransport, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    pMsg->msgID = msgDest;
    pMsg->aParam0 = (PSLPARAM)pmtpctx;
    pMsg->aParam1 = dwSequenceID;
    pMsg->aParam2 = (PSLPARAM)pvBuffer;
    pMsg->alParam = cbActual;

    ps = MTPRouteMsgToHandler(pmtpctx, pMsg);
    if (PSLSUCCESS != ps)
    {
        if (PSLSUCCESS_NOT_AVAILABLE == ps)
        {
            /* router couldn't find a destination queue
             * to post the message
             */
            ps = PSLERROR_NOT_AVAILABLE;
            pMsg = PSLNULL;
        }
        goto Exit;
    }

    /*  Transfer ownership of the buffer to the handler
     */

    pvBuffer = PSLNULL;
    pMsg = PSLNULL;

Exit:
    SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPStreamRouteBufferRequestFlags
 *
 *  Requests a buffer be returned based on the buffer request flags
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to operate within
 *      PSLUINT32       dwSequenceID        Sequence ID for the request
 *      PSLUINT64       cbRequest           Size of the request
 *      PSLUINT32       dwFlags             Flags to check
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE if no request
 *                        made
 *
 */

PSLSTATUS PSL_API MTPStreamRouteBufferRequestFlags(MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwSequenceID, PSLUINT64 cbRequest,
                    PSLUINT32 dwFlags)
{
    PSLUINT64           cbDesired;
    PSLUINT32           dwRequest;
    PSLUINT32           idxFlag;
    PSLMSGID            msgID;
    PSLSTATUS           ps;
    MTPSTREAMROUTEROBJ* pmtpstrm;

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we have a request to fulfill
     */

    dwRequest = dwFlags & MTPMSGFLAG_BUFFER_REQUEST_MASK;
    if (0 == dwRequest)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  Loop while we still have requests to handle
     */

    for (idxFlag = 0;
            (0 != dwRequest) && (idxFlag < PSLARRAYSIZE(_RgdwRequestFlags));
            idxFlag++)
    {
        /*  See if this type of buffer is being requested
         */

        if (0 == (_RgdwRequestFlags[idxFlag] & dwRequest))
        {
            continue;
        }

        /*  Determine what type of request to issue along with the size
         */

        switch (_RgdwRequestFlags[idxFlag])
        {
        case MTPMSGFLAG_DATA_BUFFER_REQUEST:
            PSLASSERT(0 != cbRequest);
            if (0 == cbRequest)
            {
                ps = PSLERROR_INVALID_OPERATION;
                goto Exit;
            }
            cbDesired = cbRequest;
            msgID = MTPMSG_DATA_BUFFER_REQUEST;
            break;

        case MTPMSGFLAG_CMD_BUFFER_REQUEST:
            cbDesired = sizeof(MTPOPERATION);
            msgID = MTPMSG_CMD_BUFFER_REQUEST;
            break;

        case MTPMSGFLAG_RESPONSE_BUFFER_REQUEST:
            cbDesired = sizeof(MTPRESPONSE);
            msgID = MTPMSG_RESPONSE_BUFFER_REQUEST;
            break;

        case MTPMSGFLAG_EVENT_BUFFER_REQUEST:
            cbDesired = sizeof(MTPEVENT);
            msgID = MTPMSG_EVENT_BUFFER_REQUEST;
            break;

        default:
            /*  We should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Request the buffer
         */

        ps = MTPStreamRouteBufferRequest(pmtpctx, msgID, dwSequenceID,
                            cbDesired, (MTPMSGFLAG_DATA_INFO_MASK & dwFlags));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Remove the request from the list we are servicing
         */

        dwRequest &= ~_RgdwRequestFlags[idxFlag];
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamRouteBufferConsumed
 *
 *  Releases the buffer specified
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context
 *      PSLMSGID        msgID               Message ID
 *      PSLVOID*        pvBuffer            Buffer to release
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteBufferConsumed(MTPCONTEXT* pmtpctx,
                    PSLMSGID msgID, PSLVOID* pvBuffer)
{
    PSLSTATUS           ps;

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure the request is valid in the router context
     */

    switch (msgID)
    {
    case MTPMSG_CMD_CONSUMED:
    case MTPMSG_DATA_CONSUMED:
    case MTPMSG_DATA_READY_TO_SEND:
    case MTPMSG_EVENT_READY_TO_SEND:
    case MTPMSG_RESPONSE:
        /*  Buffer is acceptable to be released
         */

        break;

    case MTPMSG_CMD_READY_TO_SEND:
    case MTPMSG_EVENT_CONSUMED:
    case MTPMSG_RESPONSE_CONSUMED:
        /*  These messages are only valid in the context of a proxy
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;

    default:
        /*  Should never get here
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Release the buffer
     */

    SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamRouteBufferRelease
 *
 *  Releases a buffer if present in the message
 *
 *  Arguments:
 *      PSL_CONST PSLMSG*
 *                      pMsg                Message to check for a buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteBufferRelease(PSL_CONST PSLMSG* pMsg)
{
    PSLSTATUS       ps;
    PSLVOID*        pvBuffer;

    /*  Validate Arguments
     */

    if (PSLNULL == pMsg)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If no buffer is present then we are done
     */

    if (!(MTPMSGID_BUFFER_PRESENT & pMsg->msgID))
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  If the buffer is not present then we have no work to do
     */

    pvBuffer = (PSLVOID*)pMsg->aParam2;
    if (PSLNULL == pvBuffer)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Release the buffer in the message
     */

    SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamRouteTerminate
 *
 *  Handles routing a disconnect or shutdown message to the handlers and
 *  then removes all messages from the queue until the handlers shutdown
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to disconnect
 *      PSLMSGID        msgID               Message ID to send for the
 *                                            terminate
 *      PSLPARAM        aParam              Additional param to include
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteTerminate(MTPCONTEXT* pmtpctx, PSLMSGID msgID,
                            PSLPARAM aParam)
{
    PSLUINT32           dwType;
    PSLBOOL             fDone;
    PSLMSGID            msgIDComplete;
    MTPROUTEUPDATE      mtpRU = { 0 };
    PSLSTATUS           ps;
    PSLMSG*             pMsg = PSLNULL;
    MTPSTREAMROUTEROBJ* pmtpstrm;
    PSLVOID*            pvBuffer;

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  Make sure that we do not have the receive pending flag set
     */

    pmtpstrm->fReceivePending = PSLFALSE;

    /*  Stop any pending buffer requests
     */

    for (dwType = MTPBUFFER_UNKNOWN + 1;
            dwType < MTPBUFFER_START_TRANSPORT_SPECIFIC;
            dwType++)
    {
        ps = MTPStreamBufferMgrCancelRequest(pmtpstrm->mtpstrmbm, dwType);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Determine what message to wait for
     */

    switch (msgID)
    {
    case MTPMSG_CANCEL:
        msgIDComplete = MTPMSG_CANCEL_COMPLETE;
        break;

    case MTPMSG_RESET:
        msgIDComplete = MTPMSG_RESET_COMPLETE;
        break;

    case MTPMSG_DISCONNECT:
        msgIDComplete = MTPMSG_DISCONNECT_COMPLETE;
        break;

    case MTPMSG_SHUTDOWN:
        msgIDComplete = MTPMSG_SHUTDOWN_COMPLETE;
        break;

    case MTPMSG_ABORT:
        msgIDComplete = MTPMSG_ABORT_COMPLETE;
        break;

    default:
        /*  Unknown message
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Route the message to any active handlers
     */

    ps = PSLMsgAlloc(pmtpstrm->mpTransport, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg->msgID = msgID;
    pMsg->aParam0 = (PSLPARAM)pmtpctx;
    pMsg->aParam1 = aParam;
    ps = MTPRouteMsgToHandler(pmtpctx, pMsg);
    if (PSL_SUCCEEDED(ps))
    {
        pMsg = PSLNULL;
    }

    /*  If we successfully sent a message via the router we need to
     *  block here and wait for a complete message back from the
     *  handler so that we know we can complete the shutdown.  All
     *  other messages need to be ignored.
     */

    if (PSLSUCCESS == ps)
    {
        /*  Loop until we get a complete message back
         */

        fDone = PSLFALSE;
        while (!fDone)
        {
            /*  Release any existing message
             */

            SAFE_PSLMSGFREE(pMsg);

            /*  Wait for the message
             */

            ps = PSLMsgGet(pmtpstrm->mqTransport, &pMsg);
            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_FAILED(ps))
            {
                /*  Go ahead and exit with the error
                 */

                break;
            }

            /*  Release any buffers in the message
             */

            if (MTPMSGID_BUFFER_PRESENT & pMsg->msgID)
            {
                pvBuffer = (PSLVOID*)pMsg->aParam2;
                SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);
            }

            /*  See if we have received the complete message
             */

            if (msgIDComplete == pMsg->msgID)
            {
                /*  If we are handling a cancel we want to end the route as
                 *  well
                 */

                if (MTPMSG_CANCEL_COMPLETE == msgIDComplete)
                {
                    /*  Retrieve a new lookup table if provided
                     */

                    mtpRU.hHandlerTableNew = (PSLLOOKUPTABLE)pMsg->aParam3;
                }

                /*  Stop waiting for messages
                 */

                fDone = PSLTRUE;
            }
        }
    }

    /*  End the route if we are handling a cancel operation
     */

    if (MTPMSG_CANCEL_COMPLETE == msgIDComplete)
    {
        ps = MTPRouteEnd(pmtpctx, &mtpRU);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPStreamRouteReset
 *
 *  Handles a device reset operation- note that context will change during
 *  the reset.
 *
 *  Arguments:
 *      MTPCONTEXT**    ppmtpctx            Context to reset
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteReset(MTPCONTEXT** ppmtpctx)
{
    PSLSTATUS           ps;
    MTPSTREAMROUTEROBJ* pmtpstrm;

    /*  Validate Arguments
     */

    if ((PSLNULL == ppmtpctx) || (PSLNULL == *ppmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(*ppmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  Disconnect the route and make sure that all "pending"
     *  messages have been removed from the queue
     */

    ps = MTPStreamRouteTerminate(*ppmtpctx, MTPMSG_RESET, 0);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    ps = PSLMsgEnum(pmtpstrm->mqTransport, MTPStreamRouterCleanQueueEnum, 0);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Recreate a new route to get us back to an initialized state
     */

    SAFE_MTPROUTEDESTROY(*ppmtpctx);
    ps = MTPStreamRouterCreateContext(pmtpstrm, ppmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Reset the data and event sockets to make sure they are
     *  propery handling events
     */

    ps = MTPStreamRouterResetDataAvailable(pmtpstrm, pmtpstrm->mtpstrmCommand);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    ps = MTPStreamRouterResetDataAvailable(pmtpstrm, pmtpstrm->mtpstrmEvent);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPStreamRouteMsg
 *
 *  Helper function which handles most of the core work for the stream router
 *  implementation
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             Context to handle message within
 *      PSLMSG*         pMsg                Message to handle
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamRouteMsg(MTPCONTEXT* pmtpctx, PSL_CONST PSLMSG* pMsg)
{
    MTPROUTEUPDATE      mtpRU;
    MTPSTREAMPARSER     mtpstrmSrc;
    PSLSTATUS           ps;
    MTPSTREAMROUTEROBJ* pmtpstrm;
    PSLVOID*            pvBuffer;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pMsg))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP Stream Router information from the context
     */

    pmtpstrm = MTPCONTEXT_TO_PMTPSTREAMROUTEROBJ(pmtpctx);
    PSLASSERT(MTPSTREAMROUTEROBJ_COOKIE == pmtpstrm->dwCookie);

    /*  And handle the message
     */

    switch (pMsg->msgID)
    {
    case MTPSTREAMBUFFERMGRMSG_BUFFER_AVAILABLE:
        /*  Route the buffer accordingly
         */

        ps = MTPStreamRouteBufferAvailable(pmtpctx, pMsg->aParam0,
                            pMsg->aParam1, (PSLVOID*)pMsg->aParam2,
                            pMsg->alParam);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPSTREAMROUTERMSG_DATA_AVAILABLE:
    case MTPSTREAMROUTERMSG_CHECK_DATA_AVAILABLE:
    case MTPSTREAMROUTERMSG_RESET:
        /*  Determine which parser the event is for
         */

        switch (pMsg->aParam1)
        {
        case MTPSTREAMPARSERTYPE_COMMAND:
            mtpstrmSrc = pmtpstrm->mtpstrmCommand;
            break;

        case MTPSTREAMPARSERTYPE_EVENT:
            mtpstrmSrc = pmtpstrm->mtpstrmEvent;
            break;

        default:
            /*  Should never get here- we should have either an event
             *  or a command socket
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Handle the specific event
         */

        switch (pMsg->msgID)
        {
        case MTPSTREAMROUTERMSG_DATA_AVAILABLE:
        case MTPSTREAMROUTERMSG_CHECK_DATA_AVAILABLE:
            /*  Route the data available request
             */

            ps = MTPStreamRouteDataAvailable(pmtpctx, mtpstrmSrc);
            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        case MTPSTREAMROUTERMSG_RESET:
            /*  Reset the route
             */

            ps = MTPStreamRouteReset(&pmtpctx);
            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        default:
            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }
        break;

    case MTPMSG_CMD_BUFFER_REQUEST:
    case MTPMSG_DATA_BUFFER_REQUEST:
    case MTPMSG_EVENT_BUFFER_REQUEST:
    case MTPMSG_RESPONSE_BUFFER_REQUEST:
        /*  Make sure the context matches
         */

        PSLASSERT((MTPCONTEXT*)pMsg->aParam0 == pmtpctx);

        /*  Handle buffer requests
         */

        ps = MTPStreamRouteBufferRequest(pmtpctx, pMsg->msgID,
                            pMsg->aParam1, pMsg->alParam,
                            (MTPMSGFLAG_DATA_INFO_MASK & pMsg->aParam3));
        if (PSL_FAILED(ps))
        {
            ps = MTPRouteTransportError(pmtpctx, pmtpstrm->mpTransport,
                            pMsg->msgID, pMsg->aParam1, ps);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        break;

    case MTPMSG_CMD_CONSUMED:
    case MTPMSG_DATA_CONSUMED:
    case MTPMSG_EVENT_CONSUMED:
    case MTPMSG_RESPONSE_CONSUMED:
        /*  Make sure the context matches
         */

        PSLASSERT((MTPCONTEXT*)pMsg->aParam0 == pmtpctx);

        /*  Handle buffer consumed notifications
         */

        ps = MTPStreamRouteBufferConsumed(pmtpctx, pMsg->msgID,
                            (PSLVOID*)pMsg->aParam2);
        if (PSL_FAILED(ps))
        {
            ps = MTPRouteTransportError(pmtpctx, pmtpstrm->mpTransport,
                            pMsg->msgID, pMsg->aParam1, ps);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }

        /*  Handle any buffer request flags
         */

        ps = MTPStreamRouteBufferRequestFlags(pmtpctx, pMsg->aParam1,
                            pMsg->alParam, pMsg->aParam3);
        if (PSL_FAILED(ps))
        {
            ps = MTPRouteTransportError(pmtpctx, pmtpstrm->mpTransport,
                            pMsg->msgID, pMsg->aParam1, ps);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        break;

    case MTPMSG_BUFFER_IGNORED:
        /*  Release the buffer specified in the message
         */

        ps = MTPStreamRouteBufferRelease(pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPMSG_DATA_READY_TO_SEND:
        /*  Make sure the context matches
         */

        PSLASSERT((MTPCONTEXT*)pMsg->aParam0 == pmtpctx);

        /*  Handle sending the buffer
         */

        ps = MTPStreamRouteDataSend(pmtpctx, pMsg->aParam1,
                            (PSLVOID*)pMsg->aParam2, pMsg->alParam,
                            (MTPMSGFLAG_DATA_INFO_MASK & pMsg->aParam3));
        if (PSL_FAILED(ps))
        {
            ps = MTPRouteTransportError(pmtpctx, pmtpstrm->mpTransport,
                            pMsg->msgID, pMsg->aParam1, ps);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;
        }

        /*  Check to see if we need to respond with a buffer while
         *  determining the standard message for the request
         */

        ps = MTPStreamRouteBufferRequestFlags(pmtpctx, pMsg->aParam1,
                            pMsg->alParam, pMsg->aParam3);
        if (PSL_FAILED(ps))
        {
            ps = MTPRouteTransportError(pmtpctx, pmtpstrm->mpTransport,
                            pMsg->msgID, pMsg->aParam1, ps);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;
        }
        break;

    case MTPMSG_RESPONSE_RESET:
        /*  We need to reset the transport prior to sending the response
         */

        ps = MTPStreamRouteReset(&pmtpctx);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  FALL THROUGH INTENTIONAL */

    case MTPMSG_RESPONSE:
        /*  Save some redirection
         */

        pvBuffer = (PSLVOID*)pMsg->aParam2;

        /*  And transmit the response
         */

        ps = MTPStreamParserSendOpResponse(pmtpstrm->mtpstrmCommand,
                            (PXMTPRESPONSE)pvBuffer, (PSLUINT32)pMsg->alParam);
        if (PSL_FAILED(ps))
        {
            ps = MTPRouteTransportError(pmtpctx, pmtpstrm->mpTransport,
                            pMsg->msgID, pMsg->aParam1, ps);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }

        /*  Provide some feedback
         */

        PSLTraceProgress(
            "MTPStreamRouteMsg: Response- dwID = 0x%08x\tResult = 0x%04x",
            MTPLoadUInt32(&((PXMTPRESPONSE)pvBuffer)->dwTransactionID),
            MTPLoadUInt16(&((PXMTPRESPONSE)pvBuffer)->wRespCode));

        /*  Release the buffer
         */

        SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);

        /*  End the current route
         */

        PSLZeroMemory(&mtpRU, sizeof(mtpRU));
        mtpRU.hHandlerTableNew = (PSLLOOKUPTABLE)pMsg->aParam3;
        ps = MTPRouteEnd(pmtpctx, &mtpRU);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPMSG_EVENT_READY_TO_SEND:
        /*  Save some redirection
         */

        pvBuffer = (PSLVOID*)pMsg->aParam2;

        /*  And transmit the response
         */

        ps = MTPStreamParserSendEvent(pmtpstrm->mtpstrmEvent,
                            (PXMTPEVENT)pvBuffer, (PSLUINT32)pMsg->alParam);
        if (PSL_FAILED(ps))
        {
            ps = MTPRouteTransportError(pmtpctx, pmtpstrm->mpTransport,
                            pMsg->msgID, pMsg->aParam1, ps);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }

        /*  Release the buffer
         */

        SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);

        /*  Check to see if we need to respond with a buffer
         */

        ps = MTPStreamRouteBufferRequestFlags(pmtpctx, pMsg->aParam1, 0,
                            pMsg->aParam3);
        if (PSL_FAILED(ps))
        {
            ps = MTPRouteTransportError(pmtpctx, pmtpstrm->mpTransport,
                            pMsg->msgID, pMsg->aParam1, ps);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;
        }
        break;

    case MTPMSG_CMD_READY_TO_SEND:
    case MTPMSG_CMD_AVAILABLE:
    case MTPMSG_CMD_BUFFER_AVAILABLE:
    case MTPMSG_DATA_AVAILABLE:
    case MTPMSG_DATA_BUFFER_AVAILABLE:
    case MTPMSG_EVENT_AVAILABLE:
    case MTPMSG_EVENT_BUFFER_AVAILABLE:
    case MTPMSG_RESPONSE_BUFFER_AVAILABLE:
    case MTPMSG_RESPONSE_AVAILABLE:
    case MTPMSG_CMD_SENT:
    case MTPMSG_DATA_SENT:
    case MTPMSG_EVENT_SENT:
        /*  These messages are all sent by the transport and should never
         *  be received by one
         */

        PSLASSERT(PSLFALSE);
        ps = MTPRouteTransportError(pmtpctx, pmtpstrm->mpTransport, pMsg->msgID,
                            pMsg->aParam1, PSLERROR_INVALID_OPERATION);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    default:
        ps = PSLSUCCESS_FALSE;
        break;
    }

Exit:
    return ps;
}


/*
 *  MTPStreamRouterCleanQueueEnum
 *
 *  Callback function used with PSLMsgEnum when when the router is shutting
 *  down to make sure that all messages have been removed from the queue
 *
 *  Arguments:
 *      PSL_CONST PSLMSG*
 *                      pMsg                Message to check
 *      PSLPARAM        aParam              Callback parameter
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE to have
 *                        message removed, PSLSUCCESS_CANCEL to stop
 *                        enumeration
 *
 */

PSLSTATUS PSL_API MTPStreamRouterCleanQueueEnum(PSL_CONST PSLMSG* pMsg,
                    PSLPARAM aParam)
{
    PSLSTATUS       ps;
    PSLVOID*        pvBuffer;

    /*  Validate arguments
     */

    if (PSLNULL == pMsg)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Look to see if the message has a buffer
     */

    if (MTPMSGID_BUFFER_PRESENT & pMsg->msgID)
    {
        /*  Each of these commands carries a buffer in aParam2- it needs
         *  to be released
         */

        pvBuffer = (PSLVOID*)pMsg->aParam2;
        SAFE_MTPSTREAMBUFFERMGRRELEASE(pvBuffer);
    }

    /*  Remove the message from the queue
     */

    ps = PSLSUCCESS_FALSE;

Exit:
    return ps;
    aParam;
}

