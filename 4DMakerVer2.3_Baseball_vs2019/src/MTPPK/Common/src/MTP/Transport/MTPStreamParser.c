/*
 *  MTPStreamParser.c
 *
 *  Contains implementations of the MTP Stream packet serialization/
 *  de-serialization routines to convert MTP Stream packet formats to
 *  internal MTP packet formats.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPTransportPrecomp.h"
#include "MTPStreamPackets.h"

/*  Local Defines
 */

#define MTPSTREAMPARSER_COOKIE              'mstP'

#define MTPSTREAMPARSER_CHECK_TYPE(_pi, _t) \
    (((_t) == ((_t) & _pi->dwFlags)) ? PSLSUCCESS : PSLERROR_INVALID_OPERATION)

#define MTPSTREAMPARSERFLAGS_RESPONDER      0x00010000
#define MTPSTREAMPARSERFLAGS_INITIATOR      0x00020000
#define MTPSTREAMPARSERFLAGS_MODE_MASK      0x000f0000

#define MTPSTREAMPARSERFLAGS_COMMAND        0x00100000
#define MTPSTREAMPARSERFLAGS_EVENT          0x00200000
#define MTPSTREAMPARSERFLAGS_TYPE_MASK      0x00f00000

/*  Local Types
 */

/*  Local Functions
 */

static PSLSTATUS _MTPStreamParserSendDataInternal(MTPSTREAMPARSER mtpstrmParser,
                            PSLUINT32 dwPacketType,
                            PSLUINT32 dwTransactionID,
                            PSL_CONST PSLVOID* pvData,
                            PSLUINT32 cbData);

static PSLSTATUS _MTPStreamParserReadDataInternal(MTPSTREAMPARSER mtpstrmParser,
                            PSLUINT32 dwPacketType,
                            PSLUINT32* pdwTransactionID,
                            PSLVOID* pvData,
                            PSLUINT32 cbMaxData,
                            PSLUINT32* pcbRead);

static PSLSTATUS _MTPStreamParserSendProbe(MTPSTREAMPARSER mtpstrmParser,
                            PSLUINT32 dwPacketType);

static PSLSTATUS _MTPStreamParserReadProbe(MTPSTREAMPARSER mtpstrmParser,
                            PSLUINT32 dwPacketType);


/*  Local Variables
 */

MTPSTREAMPARSERVTBL         _VtblMTPStreamParser =
{
    PSLNULL,
    PSLNULL,
    PSLNULL,
    PSLNULL,
    PSLNULL,
    MTPStreamParserGetDataHeaderBase,
    MTPStreamParserDestroyBase,
    MTPStreamParserSetTypeBase,
    MTPStreamParserGetParserInfoBase,
    MTPStreamParserResetBase,
#ifdef MTP_INITIATOR
    MTPStreamParserSendOpRequestBase,
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    MTPStreamParserReadOpRequestBase,
    MTPStreamParserSendOpResponseBase,
#endif  /* MTP_RESPONDER */
#ifdef MTP_INITIATOR
    MTPStreamParserReadOpResponseBase,
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    MTPStreamParserSendEventBase,
#endif  /* MTP_RESPONDER */
#ifdef MTP_INITIATOR
    MTPStreamParserReadEventBase,
#endif  /* MTP_INITIATOR */
    MTPStreamParserSendStartDataBase,
    MTPStreamParserReadStartDataBase,
    MTPStreamParserSendDataBase,
    MTPStreamParserReadDataBase,
    MTPStreamParserSendEndDataBase,
    MTPStreamParserReadEndDataBase,
#ifdef MTP_INITIATOR
    MTPStreamParserSendCancelBase,
#endif  /* MTP_INITIATOR */
#ifdef MTP_RESPONDER
    MTPStreamParserReadCancelBase,
#endif  /* MTP_RESPONDER */
    MTPStreamParserSendProbeRequestBase,
    MTPStreamParserReadProbeRequestBase,
    MTPStreamParserSendProbeResponseBase,
    MTPStreamParserReadProbeResponseBase,
};


/*
 *  MTPStreamParserCreate
 *
 *  Creates an instance of the MTP Stream parser.  Each parser is created in a
 *  specific mode (responder/initiator) and manages retrieving data off/
 *  placing it on the network interface.  As the core stream parser object is
 *  expected to function as a base class additional space is reserved by the
 *  initialization function as reqeuested.
 *
 *  Arguments:
 *      PSLUINT32       dwMode              Parser mode
 *      PSLUINT32       cbObject            Total size of the object
 *      MTPSTREAMPARSER*
 *                      pmtpstrmParser      Return location for object
 *
 *  Returns:
 *      PSLSTATUS   PSL_SUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserCreate(PSLUINT32 dwMode, PSLUINT32 cbObject,
                    MTPSTREAMPARSER* pmtpstrmParser)
{
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpstrmParser)
    {
        *pmtpstrmParser = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((sizeof(MTPSTREAMPARSEROBJ) > cbObject) || (PSLNULL == pmtpstrmParser))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate a new parser
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)PSLMemAlloc(PSLMEM_PTR, cbObject);
    if (PSLNULL == pmtpstrmPO)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /*  Set the mode while validating it is supported
     */

    switch (dwMode)
    {
    case MTPSTREAMPARSERMODE_RESPONDER:
        /*  Set the responder mode
         */

        pmtpstrmPO->dwFlags = MTPSTREAMPARSERFLAGS_RESPONDER;
        break;

    case MTPSTREAMPARSERFLAGS_INITIATOR:
        /*  Set initiator operation
         */

        pmtpstrmPO->dwFlags = MTPSTREAMPARSERFLAGS_INITIATOR;
        break;

    default:
        /*  Unknown parser mode
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Set the cookie so that we can validate this is one of our objects
     */

    pmtpstrmPO->dwCookie = MTPSTREAMPARSER_COOKIE;
#endif  /*  PSL_ASSERTS */

    /*  Add the VTBL information
     */

    pmtpstrmPO->vtbl = &_VtblMTPStreamParser;

    /*  Reset the parser to its default state
     */

    ps = MTPStreamParserReset(pmtpstrmPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the parser handle
     */

    *pmtpstrmParser = pmtpstrmPO;
    pmtpstrmPO = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSTREAMPARSERDESTROY(pmtpstrmPO);
    return ps;
}


/*
 *  MTPStreamParserGetDataHeaderBase
 *
 *  To avoid copying data the data header is assumed to be prepended to the
 *  beginning of the data block- however not all MTP stream implementations
 *  use headers of the same length.  This function returns a standard stream
 *  header from the beginning of the header block.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLVOID*        pvData              Data buffer to get header for
 *      PXMTPSTREAMDATA*
 *                      ppstrmHeader        Return location for the header
 *      PSLUINT32       pcbHeader           Size of the entire header (stream
 *                                            data header and additional fields)
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPStreamParserGetDataHeaderBase(MTPSTREAMPARSER mtpstrmParser,
                    PSL_CONST PSLVOID* pvData, PXMTPSTREAMDATA* ppstrmHeader,
                    PSLUINT32* pcbHeader)
{
    PSLUINT32           cbHeader;
    PSLVOID*            pvHeader;
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Clear results for safety
     */

    if (PSLNULL != ppstrmHeader)
    {
        *ppstrmHeader = PSLNULL;
    }
    if (PSLNULL != pcbHeader)
    {
        *pcbHeader = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pvData) ||
        (PSLNULL == ppstrmHeader) || (PSLNULL == pcbHeader))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Retrieve the header buffer from the data packet- and make sure that it
     *  is large enough for the stock header.
     */

    ps = MTPStreamBufferMgrGetBufferHeader(pvData, &pvHeader, &cbHeader);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    PSLASSERT(sizeof(**ppstrmHeader) <= cbHeader);
    if (sizeof(**ppstrmHeader) > cbHeader)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Determine how much of the header is needed- the rules state that
     *  the header is physically contiguous with the data so we just need
     *  to make sure that we add the necessary amount to the header pointer
     *  so that we get the header followed by the data as specified in the
     *  MTP Stream protocol
     */

    *ppstrmHeader = (PXMTPSTREAMDATA)((PSLBYTE*)pvHeader +
                            (cbHeader - sizeof(**ppstrmHeader)));
    *pcbHeader = sizeof(**ppstrmHeader);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamParserDestroyBase
 *
 *  Releases the specified MTP Stream Parser.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to destroy
 *
 *  Returns:
 *      PSLSTATUS   PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserDestroyBase(MTPSTREAMPARSER mtpstrmParser)
{
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Make sure we have work to do
     */

    if (PSLNULL == mtpstrmParser)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  And free the object
     */

    SAFE_PSLMEMFREE(pmtpstrmPO);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamParserSetTypeBase
 *
 *  Informs the parser what type it is
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to set type for
 *      PSLUINT32       dwType              Parser type
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserSetTypeBase(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwType)
{
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpstrmParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure we have no type defined
     */

    PSLASSERT(0 == (MTPSTREAMPARSERFLAGS_TYPE_MASK & pmtpstrmPO->dwFlags));
    if (0 != (MTPSTREAMPARSERFLAGS_TYPE_MASK & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_IN_USE;
        goto Exit;
    }

    /*  Validate the type and initialize the type
     */

    switch (dwType)
    {
    case MTPSTREAMPARSERTYPE_COMMAND:
        /*  Set this as a command parser
         */

        pmtpstrmPO->dwFlags |= MTPSTREAMPARSERFLAGS_COMMAND;
        break;

    case MTPSTREAMPARSERTYPE_EVENT:
        /*  Set this as an event parser
         */

        pmtpstrmPO->dwFlags |= MTPSTREAMPARSERFLAGS_EVENT;
        break;

    default:
        /*  Unknown type specified
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamParserCheckType
 *
 *  Returns PSLSUCCESS if the type constraints match, PSLSUCCESS_FALSE if
 *  they do not
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to check
 *      PSLUINT32       dwMode              Parser mode to compare against
 *      PSLUINT32       dwType              Parser type to compare against
 *
 */

PSLSTATUS PSL_API MTPStreamParserCheckType(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwMode, PSLUINT32 dwType)
{
    PSLUINT32           dwFlags;
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpstrmParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Validate the mode
     */

    dwFlags = 0;
    switch (dwMode)
    {
    case MTPSTREAMPARSERMODE_INITIATOR:
        /*  Check the initiator flag
         */

        dwFlags |= MTPSTREAMPARSERFLAGS_INITIATOR;
        break;

    case MTPSTREAMPARSERMODE_RESPONDER:
        /*  Check the responder flag
         */

        dwFlags |= MTPSTREAMPARSERFLAGS_RESPONDER;
        break;

    case MTPSTREAMPARSERMODE_ANY:
        /*  Make sure that we have a mode specified
         */

        if (0 == (MTPSTREAMPARSERFLAGS_MODE_MASK & pmtpstrmPO->dwFlags))
        {
            ps = PSLSUCCESS_FALSE;
            goto Exit;
        }
        break;

    case MTPSTREAMPARSERMODE_UNKNOWN:
        /*  No nead to check flags- anything is valid
         */

        break;

    default:
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Validate the type
     */

    switch (dwType)
    {
    case MTPSTREAMPARSERTYPE_COMMAND:
        /*  Check the initiator flag
         */

        dwFlags |= MTPSTREAMPARSERFLAGS_COMMAND;
        break;

    case MTPSTREAMPARSERTYPE_EVENT:
        /*  Check the responder flag
         */

        dwFlags |= MTPSTREAMPARSERFLAGS_EVENT;
        break;

    case MTPSTREAMPARSERTYPE_ANY:
        /*  Make sure that we have a mode specified
         */

        if (0 == (MTPSTREAMPARSERFLAGS_TYPE_MASK & pmtpstrmPO->dwFlags))
        {
            ps = PSLSUCCESS_FALSE;
            goto Exit;
        }
        break;

    case MTPSTREAMPARSERTYPE_UNKNOWN:
        /*  No nead to check flags- anything is valid
         */

        break;

    default:
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  And determine whether the type is appropriate or not
     */

    ps = ((dwFlags & pmtpstrmPO->dwFlags) == dwFlags) ?
                            PSLSUCCESS : PSLSUCCESS_FALSE;

Exit:
    return ps;
}


/*
 *  MTPStreamParserGetParserInfoBase
 *
 *  Retrieves information about the parser state
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to query
 *      MTPSTREAMPARSERINFO*
 *                      pmtpstrmInfo        Parser information to return
 *      PSLUINT32       cbInfo              Size of the information blob
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserGetParserInfoBase(MTPSTREAMPARSER mtpstrmParser,
                    MTPSTREAMPARSERINFO* pmtpstrmInfo, PSLUINT32 cbInfo)
{
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Clear result for safety
     */

    if ((PSLNULL != pmtpstrmInfo) && (0 < cbInfo))
    {
        PSLZeroMemory(pmtpstrmInfo, sizeof(*pmtpstrmInfo));
        PSLASSERT(MTPSTREAMPARSERMODE_UNKNOWN == pmtpstrmInfo->dwParserMode);
        PSLASSERT(MTPSTREAMPARSERTYPE_UNKNOWN == pmtpstrmInfo->dwParserType);
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pmtpstrmInfo) ||
        (sizeof(MTPSTREAMPARSERINFO) != cbInfo))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Extract the requested information
     */

    pmtpstrmInfo->dwParserMode =
            (MTPSTREAMPARSERFLAGS_RESPONDER & pmtpstrmPO->dwFlags) ?
                MTPSTREAMPARSERMODE_RESPONDER : MTPSTREAMPARSERMODE_INITIATOR;
    if (0 != (MTPSTREAMPARSERFLAGS_TYPE_MASK & pmtpstrmPO->dwFlags))
    {
        /*  Provide information about the type of parser this is
         */

        pmtpstrmInfo->dwParserType =
            (MTPSTREAMPARSERFLAGS_COMMAND & pmtpstrmPO->dwFlags) ?
                MTPSTREAMPARSERTYPE_COMMAND : MTPSTREAMPARSERTYPE_EVENT;
    }
    else
    {
        PSLASSERT(MTPSTREAMPARSERTYPE_UNKNOWN == pmtpstrmInfo->dwParserType);
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamParserResetBase
 *
 *  Called to end parsing of a particular packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to reset
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserResetBase(MTPSTREAMPARSER mtpstrmParser)
{
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpstrmParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Reset the parser state
     */

    pmtpstrmPO->dwTypeCur = MTPSTREAMPACKET_UNKNOWN;
    pmtpstrmPO->cbSizeCur = 0;
    pmtpstrmPO->cbAvailable = 0;
    pmtpstrmPO->fFirstChunk = PSLTRUE;
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamParserGetPacketType
 *
 *  Retrieves the type of the next packet from the stream.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to check
 *      PSLUINT32*      pdwPacketType       Packet type
 *      PSLUINT32*      pcbPacket           Packet size
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserGetPacketType(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwPacketType, PSLUINT32* pcbPacket)
{
    PSLUINT32           cbAvailable;
    PSLUINT32           cbData;
    MTPSTREAMHEADER     mtpstrmHeader;
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwPacketType)
    {
        *pdwPacketType = MTPSTREAMPACKET_UNKNOWN;
    }
    if (PSLNULL != pcbPacket)
    {
        *pcbPacket = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pdwPacketType))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure that the parser is ready to be used
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Make sure we are in the correct state before we do anything- the rule
     *  is that the the current packet type is valid until the packet has been
     *  consumed, it is then reset to unknown.
     */

    if (MTPSTREAMPACKET_UNKNOWN != pmtpstrmPO->dwTypeCur)
    {
        /*  If we get here with no data remaining then we have an error
         *  or someone forgot to reset the parser
         */

        PSLASSERT(0 < pmtpstrmPO->cbAvailable);
        if (0 == pmtpstrmPO->cbAvailable)
        {
            ps = PSLERROR_INVALID_PACKET;
            goto Exit;
        }

        /*  We are still in the process of handling a the current packet
         */

        *pdwPacketType = pmtpstrmPO->dwTypeCur;
        if (PSLNULL != pcbPacket)
        {
            *pcbPacket = pmtpstrmPO->cbAvailable;
        }
        ps = PSLSUCCESS_DATA_AVAILABLE;
        goto Exit;
    }

    /*  Make sure there is enough data available in the stream to fulfill the
     *  request
     */

    ps = MTPStreamParserGetBytesAvailable(mtpstrmParser, &cbAvailable);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    if (cbAvailable < sizeof(MTPSTREAMHEADER))
    {
        ps = PSLSUCCESS_WOULD_BLOCK;
        goto Exit;
    }

    /*  Retrieve the MTP Stream header off the stream
     */

    ps = MTPStreamParserRead(mtpstrmParser, &mtpstrmHeader,
                            sizeof(mtpstrmHeader), &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got back the header bytes
     */

    if (sizeof(mtpstrmHeader) != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Parse the header
     */

    ps = MTPStreamParserParseHeader(mtpstrmParser, &mtpstrmHeader,
                            &(pmtpstrmPO->dwTypeCur), &(pmtpstrmPO->cbSizeCur));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure that the packet is at least as big as the header
     */

    if (sizeof(mtpstrmHeader) > pmtpstrmPO->cbSizeCur)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }
    PSLASSERT(cbData <= pmtpstrmPO->cbSizeCur);
    pmtpstrmPO->cbAvailable = pmtpstrmPO->cbSizeCur - cbData;

    /*  Return the packet type and size (if requested)
     */

    *pdwPacketType = pmtpstrmPO->dwTypeCur;
    if (PSLNULL != pcbPacket)
    {
        *pcbPacket = pmtpstrmPO->cbSizeCur;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamParserSendOpRequestBase
 *
 *  Sends an operation reqeust packet to the responder.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PCXMTPOPERATION pmtpOp              Operation to send
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserSendOpRequestBase(MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPOPERATION pmtpOp)
{
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Validate Arguments- operation request is only valid on the command
     *  stream
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pmtpOp))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only initiators may send this packet from the command stream
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_INITIATOR | MTPSTREAMPARSERFLAGS_COMMAND));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_INITIATOR | MTPSTREAMPARSERFLAGS_COMMAND);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Currently not implemented
     */

    ps = PSLERROR_NOT_IMPLEMENTED;

Exit:
    return ps;
}


/*
 *  MTPStreamParserReadOpRequestsBase
 *
 *  Reads an operation request packet from the initiator.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32*      pdwDataPhase        Data phase for operation
 *      PXMTPOPERATION  pmptpOp             Resulting MTP operation
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserReadOpRequestBase(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwDataPhase, PXMTPOPERATION pmtpOp)
{
    PSLUINT32                   cbData;
    PSLUINT32                   dwDataPhase;
    MTPSTREAMOPERATIONREQUEST   mtpstrmOpRequest;
    PSLUINT32 PSL_UNALIGNED*    pdwDest;
    PSLUINT32 PSL_UNALIGNED*    pdwSrc;
    PSLSTATUS                   ps;
    MTPSTREAMPARSEROBJ*         pmtpstrmPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwDataPhase)
    {
        *pdwDataPhase = MTPSTREAM_DATA_PHASE_UNKNOWN;
    }
    if (PSLNULL != pmtpOp)
    {
        PSLZeroMemory(pmtpOp, sizeof(*pmtpOp));
    }

    /*  Validate Arguments- operation request is only valid on the command
     *  stream
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pmtpOp))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may read this packet from the command stream
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_RESPONDER | MTPSTREAMPARSERFLAGS_COMMAND));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_RESPONDER | MTPSTREAMPARSERFLAGS_COMMAND);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Standard operation requires that the header is already known before
     *  we get called
     */

    PSLASSERT(MTPSTREAMPACKET_OPERATION_REQUEST == pmtpstrmPO->dwTypeCur);
    if (MTPSTREAMPACKET_OPERATION_REQUEST != pmtpstrmPO->dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Make sure that packet is large enough to hold the request
     */

    PSLASSERT(GET_PACKET_SIZE(mtpstrmOpRequest) >= pmtpstrmPO->cbAvailable);
    if ((MTPSTREAMOPERATIONREQUEST_MIN_SIZE > pmtpstrmPO->cbSizeCur) ||
        (GET_PACKET_SIZE(mtpstrmOpRequest) < pmtpstrmPO->cbAvailable))
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = MTPStreamParserRead(mtpstrmParser, GET_PACKET_START(mtpstrmOpRequest),
                            pmtpstrmPO->cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet- since this should have been sent
     *  together by the source we will reject partial packets as invalid
     */

    if (pmtpstrmPO->cbAvailable != (PSLUINT32)cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the data phase information
     */

    dwDataPhase = MTPLoadUInt32(&(mtpstrmOpRequest.dwDataPhase));
    switch (dwDataPhase)
    {
    case MTPSTREAM_DATA_PHASE_UNKNOWN:
    case MTPSTREAM_NO_DATA_OR_DATA_IN:
    case MTPSTREAM_DATA_OUT_PHASE:
        /*  The phase is valid- accept it
         */

        if (PSLNULL != pdwDataPhase)
        {
            *pdwDataPhase = dwDataPhase;
        }
        break;

    default:
        /*  Invalid data phase- reject the packet
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Parse out and transfer the information from the network packet into
     *  the MTPOPERATION packet
     */

    PSLCopyAlignUInt16(&(pmtpOp->wOpCode), &(mtpstrmOpRequest.wOpCode));
    PSLCopyAlignUInt32(&(pmtpOp->dwTransactionID),
                            &(mtpstrmOpRequest.dwTransactionID));

    /*  Make sure that we were sent enough data to hold full parameters-
     *  if a parameter is set the full parameter must be sent, not a portion
     */

    if (0 != ((cbData - (PSLOFFSETOF(MTPSTREAMOPERATIONREQUEST, dwParam1) -
                    sizeof(mtpstrmOpRequest.mtpstrmHeader))) %
                            sizeof(mtpstrmOpRequest.dwParam1)))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Parameters are optional in the packet- only load the ones that were
     *  transmitted- clear the rest
     *
     *  NOTE: We take advantage of the fact that the parameters appear in
     *  each structure in a sequential fashion.  While they are not stored
     *  as an array we will treat them as such.  We do a simple check here
     *  to try and catch if anything has gotten out of order but there is
     *  no guarantee that this will always work.
     */

    PSLASSERT(&(mtpstrmOpRequest.dwParam5) == &((&(mtpstrmOpRequest.dwParam1))[4]));
    PSLASSERT(&(pmtpOp->dwParam5) == &((&(pmtpOp->dwParam1))[4]));
    for (pdwSrc = &(mtpstrmOpRequest.dwParam1), pdwDest = &(pmtpOp->dwParam1);
            pdwDest < &((&(pmtpOp->dwParam1))[5]);
            pdwSrc++, pdwDest++)
    {
        /*  Check to see if the parameter is available in the packet
         */

        if ((GET_PACKET_START(mtpstrmOpRequest) + cbData) <= (PSLBYTE*)pdwSrc)
        {
            PSLStoreAlignUInt32(pdwDest, 0);
            continue;
        }

        /*  Load the parameter from the packet
         */

        PSLCopyAlignUInt32(pdwDest, pdwSrc);
    }

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(pmtpstrmPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPStreamParserSendOpResponseBase
 *
 *  Sends an operation response to the initiator from the responder.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PCXMTPRESPONSE  pmtpResponse        Response to send
 *      PSLUINT32       cbResponse          Length of the response- used to
 *                                            determine how many parameters
 *                                            to send
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserSendOpResponseBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPRESPONSE pmtpResponse,
                    PSLUINT32 cbResponse)
{
    PSLUINT32                           cbPacket;
    PSLUINT32                           cbSent;
    PSL_CONST PSLUINT32 PSL_UNALIGNED*  pdwSrc;
    PSLUINT32 PSL_UNALIGNED*            pdwDest;
    MTPSTREAMOPERATIONRESPONSE          mtpstrmOpResponse;
    PSLSTATUS                           ps;
    MTPSTREAMPARSEROBJ*                 pmtpstrmPO;

    /*  Validate Arguments- the response buffer must be large enough to hold
     *  at least the minimum parameters
     */

    if ((PSLNULL == mtpstrmParser) || (MTPRESPONSE_MIN_SIZE > cbResponse) ||
        (sizeof(*pmtpResponse) < cbResponse))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may send this packet from the command stream
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_RESPONDER | MTPSTREAMPARSERFLAGS_COMMAND));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_RESPONDER | MTPSTREAMPARSERFLAGS_COMMAND);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Fill out the simple information
     */

    PSLCopyAlignUInt16(&(mtpstrmOpResponse.wResponseCode),
                            &(pmtpResponse->wRespCode));
    PSLCopyAlignUInt32(&(mtpstrmOpResponse.dwTransactionID),
                            &(pmtpResponse->dwTransactionID));
    cbPacket = MTPSTREAMOPERATIONRESPONSE_MIN_SIZE;

    /*  Parameters are optional in the packet- only load the ones that were
     *  transmitted- clear the rest
     *
     *  NOTE: We take advantage of the fact that the parameters appear in
     *  each structure in a sequential fashion.  While they are not stored
     *  as an array we will treat them as such.  We do a simple check here
     *  to try and catch if anything has gotten out of order but there is
     *  no guarantee that this will always work.
     */

    PSLASSERT(&(pmtpResponse->dwParam5) == &((&(pmtpResponse->dwParam1))[4]));
    PSLASSERT(&(mtpstrmOpResponse.dwParam5) == &((&(mtpstrmOpResponse.dwParam1))[4]));
    for (pdwSrc = &(pmtpResponse->dwParam1), pdwDest = &(mtpstrmOpResponse.dwParam1);
            pdwSrc < (PCXPSLUINT32)((PSL_CONST PSLBYTE*)pmtpResponse + cbResponse);
            pdwSrc++, pdwDest++, cbPacket += sizeof(PSLUINT32))
    {
        /*  Store the parameter in the packet
         */

        PSLCopyAlignUInt32(pdwDest, pdwSrc);
    }

    /*  Store the header
     */

    ps = MTPStreamParserStoreHeader(mtpstrmParser,
                            MTPSTREAMPACKET_OPERATION_RESPONSE, cbPacket,
                            &(mtpstrmOpResponse.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And send it over the network
     */

    ps = MTPStreamParserSend(mtpstrmParser, &mtpstrmOpResponse,
                            cbPacket, &cbSent);
    if (PSL_FAILED(ps) || (cbPacket != cbSent))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamParserReadOpResponseBase
 *
 *  Reads an operation response from the responder
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PXMTPRESPONSE   pmtpResponse        Response read
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserReadOpResponseBase(
                    MTPSTREAMPARSER mtpstrmParser,
                    PXMTPRESPONSE pmtpResponse)
{
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Clear results for safety
     */

    if (PSLNULL != pmtpResponse)
    {
        PSLZeroMemory(pmtpResponse, sizeof(*pmtpResponse));
    }

    /*  Validate Arguments- command response is only valid on the command
     *  stream
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pmtpResponse))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only initiators may read this packet from the command stream
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_INITIATOR | MTPSTREAMPARSERFLAGS_COMMAND));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_INITIATOR | MTPSTREAMPARSERFLAGS_COMMAND);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Currently not implemented
     */

    ps = PSLERROR_NOT_IMPLEMENTED;

Exit:
    return ps;
}


/*
 *  MTPStreamParserSendEventBase
 *
 *  Sends an operation response to the initiator from the responder.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PXMTPEVENT      pmtpEvent           Response to send
 *      PSLUINT32       cbEvent             Length of the event- used to
 *                                            determine how many parameters
 *                                            to send
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserSendEventBase(MTPSTREAMPARSER mtpstrmParser,
                    PCXMTPEVENT pmtpEvent, PSLUINT32 cbEvent)
{
    PSLUINT32                           cbData;
    PSLUINT32                           cbPacket;
    PSL_CONST PSLUINT32 PSL_UNALIGNED*  pdwSrc;
    PSLUINT32 PSL_UNALIGNED*            pdwDest;
    MTPSTREAMEVENT                      mtpstrmEvent;
    PSLSTATUS                           ps;
    MTPSTREAMPARSEROBJ*                 pmtpstrmPO;

    /*  Validate Arguments- the response buffer must be large enough to hold
     *  at least the minimum parameters
     */

    if ((PSLNULL == mtpstrmParser) || (MTPEVENT_MIN_SIZE > cbEvent) ||
        (sizeof(*pmtpEvent) < cbEvent))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may send this packet from the command stream
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_RESPONDER | MTPSTREAMPARSERFLAGS_EVENT));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_RESPONDER | MTPSTREAMPARSERFLAGS_EVENT);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Fill out the simple information
     */

    PSLCopyAlignUInt16(&(mtpstrmEvent.wEventCode), &(pmtpEvent->wEventCode));
    PSLCopyAlignUInt32(&(mtpstrmEvent.dwTransactionID),
                            &(pmtpEvent->dwTransactionID));
    cbPacket = MTPSTREAMEVENT_MIN_SIZE;

    /*  Parameters are optional in the packet- only load the ones that were
     *  transmitted- clear the rest
     *
     *  NOTE: We take advantage of the fact that the parameters appear in
     *  each structure in a sequential fashion.  While they are not stored
     *  as an array we will treat them as such.  We do a simple check here
     *  to try and catch if anything has gotten out of order but there is
     *  no guarantee that this will always work.
     */

    PSLASSERT(&(pmtpEvent->dwParam3) == &((&(pmtpEvent->dwParam1))[2]));
    PSLASSERT(&(mtpstrmEvent.dwParam3) == &((&(mtpstrmEvent.dwParam1))[2]));
    for (pdwSrc = &(pmtpEvent->dwParam1), pdwDest = &(mtpstrmEvent.dwParam1);
            pdwSrc < (PCXPSLUINT32)((PSL_CONST PSLBYTE*)pmtpEvent + cbEvent);
            pdwSrc++, pdwDest++, cbPacket += sizeof(PSLUINT32))
    {
        /*  Store the parameter in the packet
         */

        PSLCopyAlignUInt32(pdwDest, pdwSrc);
    }

    /*  Store the header
     */

    ps = MTPStreamParserStoreHeader(mtpstrmParser,
                            MTPSTREAMPACKET_EVENT, cbPacket,
                            &(mtpstrmEvent.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And send it over the network
     */

    ps = MTPStreamParserSend(mtpstrmParser, &mtpstrmEvent,
                            cbPacket, &cbData);
    if (PSL_FAILED(ps) || (cbPacket != cbData))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamParserReadEventBase
 *
 *  Reads an event from the responder
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      MTPRESPONSE*    pmtpEvent           Event read
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserReadEventBase(MTPSTREAMPARSER mtpstrmParser,
                    PXMTPEVENT pmtpEvent)
{
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Clear results for safety
     */

    if (PSLNULL != pmtpEvent)
    {
        PSLZeroMemory(pmtpEvent, sizeof(*pmtpEvent));
    }

    /*  Validate Arguments- command response is only valid on the command
     *  stream
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pmtpEvent))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only initiators may read this packet from the event stream
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_INITIATOR | MTPSTREAMPARSERFLAGS_EVENT));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_INITIATOR | MTPSTREAMPARSERFLAGS_EVENT);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Currently not implemented
     */

    ps = PSLERROR_NOT_IMPLEMENTED;

Exit:
    return ps;
}


/*
 *  MTPStreamParserSendStartDataBase
 *
 *  Sends a start data packet to the destination
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32       dwTransactionID     Transaction ID
 *      PSLUINT64       qwTotalSize         Total size of the data to be sent
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserSendStartDataBase(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID, PSLUINT64 qwTotalSize)
{
    PSLUINT32               cbSent;
    MTPSTREAMSTARTDATA      mtpstrmStartData;
    PSLSTATUS               ps;
    MTPSTREAMPARSEROBJ*     pmtpstrmPO;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpstrmParser)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Start data is sent by either responders or initiators but always over
     *  the command connection
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                            MTPSTREAMPARSERFLAGS_COMMAND));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO, MTPSTREAMPARSERFLAGS_COMMAND);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Fill out the information
     */

    MTPStoreUInt32(&(mtpstrmStartData.dwTransactionID), dwTransactionID);
    MTPStoreUInt64(&(mtpstrmStartData.cbTotal), &qwTotalSize);

    /*  Store the header
     */

    ps = MTPStreamParserStoreHeader(mtpstrmParser,
                            MTPSTREAMPACKET_START_DATA,
                            sizeof(mtpstrmStartData),
                            &(mtpstrmStartData.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And send the packet
     */

    ps = MTPStreamParserSend(mtpstrmParser, &mtpstrmStartData,
                            sizeof(mtpstrmStartData), &cbSent);
    if (PSL_FAILED(ps) || (cbSent != sizeof(mtpstrmStartData)))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamParserReadStartDataBase
 *
 *  Reads a start data packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32*      pdwTransactionID    Transaction ID
 *      PSLUINT64*      pqwTotalSize        Total size of data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserReadStartDataBase(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID,
                    PSLUINT64* pqwTotalSize)
{
    PSLUINT32           cbData;
    MTPSTREAMSTARTDATA  mtpstrmStartData;
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwTransactionID)
    {
        *pdwTransactionID = MTPTRANSACTION_UNKNOWN;
    }
    if (PSLNULL != pqwTotalSize)
    {
        *pqwTotalSize = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pdwTransactionID) ||
        (PSLNULL == pqwTotalSize))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Data is only valid on the command stream
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                        MTPSTREAMPARSERFLAGS_COMMAND));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO, MTPSTREAMPARSERFLAGS_COMMAND);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Standard operation requires that the header is already known before
     *  we get called
     */

    PSLASSERT(MTPSTREAMPACKET_START_DATA == pmtpstrmPO->dwTypeCur);
    if (MTPSTREAMPACKET_START_DATA != pmtpstrmPO->dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Make sure that packet is large enough to hold the request
     */

    PSLASSERT(GET_PACKET_SIZE(mtpstrmStartData) == pmtpstrmPO->cbAvailable);
    if (GET_PACKET_SIZE(mtpstrmStartData) != pmtpstrmPO->cbAvailable)
    {
        /*  The size of the received packet is incorrect
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = MTPStreamParserRead(mtpstrmParser, GET_PACKET_START(mtpstrmStartData),
                            pmtpstrmPO->cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet
     */

    if (GET_PACKET_SIZE(mtpstrmStartData) != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet contents
     */

    *pdwTransactionID = MTPLoadUInt32(&(mtpstrmStartData.dwTransactionID));
    MTPCopyUInt64(pqwTotalSize, &(mtpstrmStartData.cbTotal));

    /*  Reset the parser
     */

    ps = MTPStreamParserReset(pmtpstrmPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  _MTPStreamParserInternalSendData
 *
 *  Sends the specified data packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to send data with
 *      PSLUINT32       dwPacketType        Type of data packet to send
 *      PSLUINT32       dwTransactionID     Transaction ID for this packet
 *      PSL_CONST PSLVOID*
 *                      pvData              Data to send
 *      PSLUINT32       cbData              Size of data to send
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPStreamParserSendDataInternal(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwPacketType, PSLUINT32 dwTransactionID,
                    PSL_CONST PSLVOID* pvData, PSLUINT32 cbData)
{
    PSLUINT32           cbPacket;
    PSLUINT32           cbHeader;
    PSLUINT32           cbSent;
    PXMTPSTREAMDATA     pmtpstrmData;
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Under version 1.0 of the MTP Stream protocol the Data Packet and the
     *  End Data Packet only differ by the packet type specified in the
     *  header.  For simplicy we use the same function to send both packets.
     *  The only thing we check here is that the packet sizes are the same
     *  so that if someone adds something to one of the two packets it will
     *  at least cause an assert
     */

    PSLASSERT(sizeof(MTPSTREAMDATA) == sizeof(MTPSTREAMENDDATA));

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || ((MTPSTREAMPACKET_DATA != dwPacketType) &&
        (MTPSTREAMPACKET_END_DATA != dwPacketType)) || (PSLNULL == pvData))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure that this lives within our size restrictions
     */

    if (MTPSTREAM_MAX_DATA_PACKET <= cbData)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Data is sent by either responders or initiators but always over
     *  the command connection
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                            MTPSTREAMPARSERFLAGS_COMMAND));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO, MTPSTREAMPARSERFLAGS_COMMAND);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Retrieve the pointer to the stream header
     */

    ps = MTPStreamParserGetDataHeader(mtpstrmParser, pvData, &pmtpstrmData,
                            &cbHeader);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    PSLASSERT(sizeof(*pmtpstrmData) <= cbHeader);
    if (sizeof(*pmtpstrmData) > cbHeader)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Fill out the information
     */

    MTPStoreUInt32(&(pmtpstrmData->dwTransactionID), dwTransactionID);

    /*  Store the header
     */

    cbPacket = cbHeader + cbData;
    ps = MTPStreamParserStoreHeader(mtpstrmParser, dwPacketType, cbPacket,
                            &(pmtpstrmData->mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And send the packet
     */

    ps = MTPStreamParserSend(mtpstrmParser, pmtpstrmData, cbPacket, &cbSent);
    if (PSL_FAILED(ps) || (cbSent != cbPacket))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPStreamParserReadDataInternal
 *
 *  Internal function that supports reading data buffers from either a
 *  MTPSTREAMPACKET_DATA or MTPSTREAMPACKET_END_DATA packet.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32       dwPacketType        Type of packet to read data from
 *      PLSUINT32*      pdwTransactionID    Return buffer for transaction ID
 *      PSLVOID*        pvData              Buffer for data
 *      PSLUINT32       cbMaxData           Maximum amount of data in the
 *                                            buffer
 *      PSLUINT32*      pcbRead             Amount of data read
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPStreamParserReadDataInternal(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwPacketType, PSLUINT32* pdwTransactionID,
                    PSLVOID* pvData, PSLUINT32 cbMaxData,
                    PSLUINT32* pcbRead)
{
    PSLUINT32           cbAvailable;
    PSLUINT32           cbDesired;
    PSLUINT32           cbExtra;
    PSLUINT32           cbHeader;
    PSLUINT32           cbRead;
    PXMTPSTREAMDATA     pmtpstrmData;
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwTransactionID)
    {
        *pdwTransactionID = MTPTRANSACTION_UNKNOWN;
    }
    if (PSLNULL != pcbRead)
    {
        *pcbRead = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pdwTransactionID) ||
        (PSLNULL == pvData) || (0 == cbMaxData))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Data is only valid on the command stream
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                        MTPSTREAMPARSERFLAGS_COMMAND));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO, MTPSTREAMPARSERFLAGS_COMMAND);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Standard operation requires that the header is already known before
     *  we get called
     */

    PSLASSERT(dwPacketType == pmtpstrmPO->dwTypeCur);
    if (dwPacketType != pmtpstrmPO->dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Data may be read in one of two forms- either all at once or in
     *  chunks.  See if we are reading the first "chunk" so we know whether
     *  or not we need to read the transaction ID
     */

    if (pmtpstrmPO->fFirstChunk)
    {
        /*  Performing the first read of this data packet- need to read the
         *  packet header as well
         *
         *  Retrieve the buffer header and make sure it is sufficient to hold
         *  at least the stream header
         */

        ps = MTPStreamParserGetDataHeader(mtpstrmParser, pvData, &pmtpstrmData,
                            &cbHeader);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        PSLASSERT(sizeof(*pmtpstrmData) <= cbHeader);
        if (sizeof(*pmtpstrmData) > cbHeader)
        {
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Because we have backed up the data pointer to account for
         *  the transaction ID we are able to add extra data to the data
         *  size as well- but make sure we do not overflow
         */

        cbExtra = cbHeader - sizeof(MTPSTREAMHEADER);
        if ((cbMaxData + (cbHeader - cbExtra)) < cbMaxData)
        {
            ps = PSLERROR_INVALID_RANGE;
            goto Exit;
        }

        /*  Start reading at the start of the packet
         */

        PSLASSERT(((PSLBYTE*)GET_PACKET_START(*pmtpstrmData) + cbExtra) ==
                            pvData);
        pvData = GET_PACKET_START(*pmtpstrmData);
        cbMaxData = cbMaxData + cbExtra;
    }
    else
    {
        /*  We do not have a header to read
         */

        pmtpstrmData = PSLNULL;
        cbExtra = 0;
    }

    /*  And determine how much data to read
     */

    cbDesired = min(cbMaxData, MTPSTREAM_MAX_DATA_PACKET);
    cbAvailable = min(pmtpstrmPO->cbAvailable, MTPSTREAM_MAX_DATA_PACKET);
    cbDesired = min(cbDesired, cbAvailable);

    /*  Attempt to retrieve the data
     */

    ps = MTPStreamParserRead(mtpstrmParser, pvData, cbDesired, &cbRead);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Remember that we have read the first chunk
     */

    pmtpstrmPO->fFirstChunk = PSLFALSE;

    /*  Retrieve the transaction ID if available
     */

    if (PSLNULL != pmtpstrmData)
    {
        /*  Pull the transaction out of the data packet
         */

        *pdwTransactionID = MTPLoadUInt32(&(pmtpstrmData->dwTransactionID));

        /*  Update the amount of data read to remove the MTP Stream data header
         */

        pmtpstrmPO->cbAvailable -= cbExtra;
        cbRead -= cbExtra;
    }
    else
    {
        /*  For subsequent chunking we do not return the transaction ID
         */

        *pdwTransactionID = MTPTRANSACTION_NOT_APPLICABLE;
    }

    /*  Update the number of bytes that remain
     */

    pmtpstrmPO->cbAvailable -= cbRead;
    if (0 == pmtpstrmPO->cbAvailable)
    {
        ps = MTPStreamParserReset(pmtpstrmPO);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  And return the number of bytes read if requested
     */

    if (PSLNULL != pcbRead)
    {
        *pcbRead = cbRead;
    }

    /*  Report success
     */

    ps = (0 == pmtpstrmPO->cbAvailable) ? PSLSUCCESS : PSLSUCCESS_DATA_AVAILABLE;

Exit:
    return ps;
}


/*
 *  MTPStreamParserSendDataBase
 *
 *  Sends the specified data packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to send data with
 *      PSLUINT32       dwTransactionID     Transaction ID for this packet
 *      PSL_CONST PSLVOID*
 *                      pvData              Data to send
 *      PSLUINT32       cbData              Length of data to send
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserSendDataBase(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID, PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData)
{
    PSLSTATUS           ps;

    /*  Make sure we have data to send
     */

    if (0 == cbData)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  And do the real work
     */

    ps = _MTPStreamParserSendDataInternal(mtpstrmParser, MTPSTREAMPACKET_DATA,
                            dwTransactionID, pvData, cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPStreamParserReadDataBase
 *
 *  Reads a data packet from the specified parser
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to read data with
 *      PSLUINT32*      pdwTransactionID    Transaction ID
 *      PSLVOID*        pvData              Buffer to read data into
 *      PSLUINT32       cbMaxData           Size of the buffer
 *      PSLUINT32*      pcbRead             Amount of data read
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserReadDataBase(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID, PSLVOID* pvData,
                    PSLUINT32 cbMaxData, PSLUINT32* pcbRead)
{
    return _MTPStreamParserReadDataInternal(mtpstrmParser, MTPSTREAMPACKET_DATA,
                            pdwTransactionID, pvData, cbMaxData, pcbRead);
}


/*
 *  MTPStreamParserSendEndDataBase
 *
 *  Sends the specified data packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to send data with
 *      PSLUINT32       dwTransactionID     Transaction ID for this packet
 *      PSL_CONST PSLVOID*
 *                      pvData              Data to send
 *      PSLUINT32       cbData
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserSendEndDataBase(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwTransactionID, PSL_CONST PSLVOID* pvData,
                    PSLUINT32 cbData)
{
    return _MTPStreamParserSendDataInternal(mtpstrmParser,
                            MTPSTREAMPACKET_END_DATA, dwTransactionID,
                            pvData, cbData);
}


/*
 *  MTPStreamParserReadEndDataBase
 *
 *  Reads a data packet from the specified parser
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to read data with
 *      PSLUINT32*      pdwTransactionID    Transaction ID
 *      PSLVOID*        pvData              Buffer to read data into
 *      PSLUINT32       cbMaxData           Size of the buffer
 *      PSLUINT32*      pcbRead             Amount of data read
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserReadEndDataBase(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32* pdwTransactionID, PSLVOID* pvData,
                    PSLUINT32 cbMaxData, PSLUINT32* pcbRead)
{
    return _MTPStreamParserReadDataInternal(mtpstrmParser,
                            MTPSTREAMPACKET_END_DATA, pdwTransactionID,
                            pvData, cbMaxData, pcbRead);
}


/*
 *  MTPStreamParserSendCancelBase
 *
 *  Sends a cancel packet to the responder.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32       dwTransactionID     Transaction to cancel
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserSendCancelBase(MTPSTREAMPARSER mtpstrmParser,
                            PSLUINT32 dwTransactionID)
{
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) ||
        (MTP_TRANSACTIONID_MIN > dwTransactionID) ||
        (MTP_TRANSACTIONID_MAX < dwTransactionID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only initiators may send this packet from the command stream
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_INITIATOR | MTPSTREAMPARSERFLAGS_COMMAND));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                MTPSTREAMPARSERFLAGS_INITIATOR | MTPSTREAMPARSERFLAGS_COMMAND);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Currently not implemented
     */

    ps = PSLERROR_NOT_IMPLEMENTED;

Exit:
    return ps;
}


/*
 *  MTPStreamParserReadCancelBase
 *
 *  Reads a cancel request using the specified parser from the given
 *  stream.
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32*      pdwTransactionID    Transaction to cancel
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserReadCancelBase(MTPSTREAMPARSER mtpstrmParser,
                            PSLUINT32* pdwTransactionID)
{
    PSLUINT32           cbData;
    MTPSTREAMCANCEL     mtpstrmCancel;
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwTransactionID)
    {
        *pdwTransactionID = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) || (PSLNULL == pdwTransactionID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Only responders may read this packet
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                        MTPSTREAMPARSERFLAGS_RESPONDER));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO, MTPSTREAMPARSERFLAGS_RESPONDER);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Standard operation model requires that the header has already been
     *  read.
     */

    PSLASSERT(MTPSTREAMPACKET_CANCEL == pmtpstrmPO->dwTypeCur);
    if (MTPSTREAMPACKET_CANCEL != pmtpstrmPO->dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Make sure that packet is large enough to hold the key structures
     */

    PSLASSERT(GET_PACKET_SIZE(mtpstrmCancel) == pmtpstrmPO->cbAvailable);
    if (GET_PACKET_SIZE(mtpstrmCancel) != pmtpstrmPO->cbAvailable)
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Retrieve the packet- note that we already removed the header so we
     *  do not want to read it again
     */

    ps = MTPStreamParserRead(mtpstrmParser, GET_PACKET_START(mtpstrmCancel),
                            pmtpstrmPO->cbAvailable, &cbData);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got the entire packet- since this should have been sent
     *  together by the source we will reject partial packets as invalid
     */

    if (GET_PACKET_SIZE(mtpstrmCancel) != cbData)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Extract the connection ID out of the entry
     */

    *pdwTransactionID = MTPLoadUInt32(&(mtpstrmCancel.dwTransactionID));

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(pmtpstrmPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  _MTPStreamParserSendProbe
 *
 *  Sends a probe packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32       dwPacketType        Type of packet to send
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPStreamParserSendProbe(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwPacketType)
{
    PSLUINT32           cbPacket;
    PSLUINT32           cbSent;
    MTPSTREAMPROBE      mtpstrmProbe;
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) ||
        ((MTPSTREAMPACKET_PROBE_REQUEST != dwPacketType) &&
                            (MTPSTREAMPACKET_PROBE_RESPONSE != dwPacketType)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Probes always happen on the event channel
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                            MTPSTREAMPARSERFLAGS_EVENT));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO, MTPSTREAMPARSERFLAGS_EVENT);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Store the header
     */

    cbPacket = sizeof(MTPSTREAMPROBE);
    ps = MTPStreamParserStoreHeader(mtpstrmParser, dwPacketType, cbPacket,
                            &(mtpstrmProbe.mtpstrmHeader));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And send it over the network
     */

    ps = MTPStreamParserSend(mtpstrmParser, &mtpstrmProbe, cbPacket, &cbSent);
    if (PSL_FAILED(ps) || (cbPacket != cbSent))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPStreamParserReadProbe
 *
 *  Reads a probe packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32       dwPacketType        Type of packet to send
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPStreamParserReadProbe(MTPSTREAMPARSER mtpstrmParser,
                    PSLUINT32 dwPacketType)
{
    PSLSTATUS           ps;
    MTPSTREAMPARSEROBJ* pmtpstrmPO;

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmParser) ||
        ((MTPSTREAMPACKET_PROBE_REQUEST != dwPacketType) &&
                            (MTPSTREAMPACKET_PROBE_RESPONSE != dwPacketType)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the handle- check to make sure it is one of ours
     */

    pmtpstrmPO = (MTPSTREAMPARSEROBJ*)mtpstrmParser;
    PSLASSERT(MTPSTREAMPARSER_COOKIE == pmtpstrmPO->dwCookie);

    /*  Make sure the parser is ready to go
     */

    PSLASSERT(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags);
    if (!(MTPSTREAMPARSERFLAGS_PARSER_READY & pmtpstrmPO->dwFlags))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Probes always happen on the event channel
     */

    PSLASSERT(PSLSUCCESS == MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO,
                            MTPSTREAMPARSERFLAGS_EVENT));
    ps = MTPSTREAMPARSER_CHECK_TYPE(pmtpstrmPO, MTPSTREAMPARSERFLAGS_EVENT);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Standard operation model requires that the header has already been
     *  received- in the case of the probe packet this also represents the
     *  entire packet
     */

    PSLASSERT(dwPacketType == pmtpstrmPO->dwTypeCur);
    if (dwPacketType != pmtpstrmPO->dwTypeCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Probes consist of only a header so there is no need to read any more
     *  data.
     */

    if (sizeof(MTPSTREAMPROBE) != pmtpstrmPO->cbSizeCur)
    {
        /*  The size of the received packet is too small- this is not a
         *  'we have not received enough bytes' issue.  This is an actual
         *  error in the packet and should be reported as such.
         */

        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Clear the current packet information and report success
     */

    ps = MTPStreamParserReset(pmtpstrmPO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPStreamParserSendProbeRequestBase
 *
 *  Sends a probe request packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */


PSLSTATUS PSL_API MTPStreamParserSendProbeRequestBase(
                    MTPSTREAMPARSER mtpstrmParser)
{
    return _MTPStreamParserSendProbe(mtpstrmParser,
                            MTPSTREAMPACKET_PROBE_REQUEST);
}


/*
 *  MTPStreamParserReadProbeRequestBase
 *
 *  Reads a probe request packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserReadProbeRequestBase(
                    MTPSTREAMPARSER mtpstrmParser)
{
    return _MTPStreamParserReadProbe(mtpstrmParser,
                            MTPSTREAMPACKET_PROBE_REQUEST);
}


/*
 *  MTPStreamParserSendProbeResponseBase
 *
 *  Sends a probe response packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *      PSLUINT32       dwDeviceStatus      Device Status
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */


PSLSTATUS PSL_API MTPStreamParserSendProbeResponseBase(
                    MTPSTREAMPARSER mtpstrmParser, PSLUINT32 dwDeviceStatus)
{
    return _MTPStreamParserSendProbe(mtpstrmParser,
                            MTPSTREAMPACKET_PROBE_RESPONSE);
    dwDeviceStatus;
}

/*
 *  _MTPStreamParserReadProbeResponseBase
 *
 *  Reads a probe response packet
 *
 *  Arguments:
 *      MTPSTREAMPARSER mtpstrmParser       Parser to use
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamParserReadProbeResponseBase(
                    MTPSTREAMPARSER mtpstrmParser)
{
    return _MTPStreamParserReadProbe(mtpstrmParser,
                            MTPSTREAMPACKET_PROBE_RESPONSE);
}

