/*
 *  MTPStreamBufferMgr.c
 *
 *  Contains definitions of the MTP Stream buffer handling utilized by the
 *  MTP Stream transports.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPTransportPrecomp.h"

/*  Local defines
 */

#define MTPSTREAMBUFFER_COOKIE          'smBF'

#define MTPSTREAMBUFFERMGROBJ_COOKIE    'smBM'

enum
{
    MTPSTREAMBUFFERINDEX_COMMAND = 0,
    MTPSTREAMBUFFERINDEX_TRANSMIT_DATA,
    MTPSTREAMBUFFERINDEX_RECEIVE_DATA,
    MTPSTREAMBUFFERINDEX_RESPONSE,
    MTPSTREAMBUFFERINDEX_EVENT,
    MTPSTREAMBUFFERINDEX_FLUSH_DATA,
    MTPSTREAMBUFFERINDEX_MAX_INDEX
};

#define MTPBUFFER_TO_INDEX(_bt)     ((_bt) - MTPBUFFER_COMMAND)

#define INDEX_TO_MTPBUFFER(_idxB)   ((_idxB) + MTPBUFFER_COMMAND);

#define MTPSTREAMBUFFERHDR_SET_INFO(_pmtph, _idxBuffer, _idxEntry) \
        (_pmtph)->dwInfo = ((_idxBuffer) << 16) | ((_idxEntry) & 0xffff)

#define MTPSTREAMBUFFERHDR_GET_INDEX(_pmtph) \
        ((_pmtph)->dwInfo >> 16)

#define MTPSTREAMBUFFERHDR_GET_ENTRY(_pmtph) \
        ((_pmtph)->dwInfo & 0xffff)

#define MTPSTREAM_BUFFER_TO_HEADER(_pv) \
        ((MTPSTREAMBUFFERHDR*)(_pv) - 1)
#define MTPSTREAM_HEADER_TO_BUFFER(_pv) \
        (PSLVOID**)((MTPSTREAMBUFFERHDR*)(_pv) + 1)

/*  Local types
 */

typedef struct _MTPSTREAMBUFFERHDR
{
#ifdef PSL_ASSERTS
    PSLUINT32               dwCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT32               dwInfo;
    PSLUINT32               cbSize;
    MTPSTREAMBUFFERMGR      mtpstrmbm;

    /*  The header portion is reserved for use by the system to make reading
     *  and writing single packets possible.  It is assumed that the header
     *  and the data portion of the buffer are contiguous.
     */

    PSLBYTE                 rgbHeader[MTPSTREAM_HEADER_SIZE];
} MTPSTREAMBUFFERHDR;

typedef struct _MTPSTREAMBUFFERREQUEST
{
    PSLUINT32               cbRequest;
    PSLPARAM                aParam;
    PSLMSGQUEUE             mqNotify;
    PSLMSGPOOL              mpNotify;
} MTPSTREAMBUFFERREQUEST;

typedef struct _MTPSTREAMBUFFERINFO
{
    PSLUINT32               cBuffers;
    PSLUINT32               cbMaxBuffer;
    PSLBYTE**               rgpbBuffers;
    MTPSTREAMBUFFERREQUEST  brPending;
} MTPSTREAMBUFFERINFO;

typedef struct _MTPSTREAMBUFFERMGROBJ
{
#ifdef PSL_ASSERTS
    PSLUINT32               dwCookie;
#endif  /* PSL_ASSERTS */
    PSLHANDLE               hMutex;
    PSLUINT32               cbAlignment;
    MTPSTREAMBUFFERINFO     rgbiTypes[MTPSTREAMBUFFERINDEX_MAX_INDEX];
    PSLBYTE                 rgbAvailable[MTPSTREAMBUFFERINDEX_MAX_INDEX];
} MTPSTREAMBUFFERMGROBJ;

/*  Local Functions
 */

/*  Local Variables
 */


/*
 *  MTPStreamBufferMgrCreate
 *
 *  Creates a buffer object
 *
 *  Arguments:
 *      PSLUINT32       cbAlignment         Alignment of the data buffer
 *      PSLUINT32       cbMaxCmdTransmit    Maximum packet size for sending
 *                                            on the command pipe
 *      PSLUINT32       cbMaxCmdReceive     Maximum packet size for receiving
 *                                            on the command pipe
 *      PSLUINT32       cbMaxEventTransmit  Maximum packet size for sending
 *                                            on the event pipe
 *      PSLUINT32       cbMaxEventReceive   Maximum packet size for receiving
 *                                            on the event pipe
 *      PSLUINT32       cbDataHeader        Size of the data header
 *      MTPSTREAMBUFFERMGR*
 *                      pmtpstrmbm          Return buffer for buffer manager
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamBufferMgrCreate(PSLUINT32 cbAlignment,
                            PSLUINT32 cbMaxCmdTransmit,
                            PSLUINT32 cbMaxCmdReceive,
                            PSLUINT32 cbMaxEventTransmit,
                            PSLUINT32 cbMaxEventReceive,
                            PSLUINT32 cbDataHeader,
                            MTPSTREAMBUFFERMGR* pmtpstrmbm)
{
    PSLUINT32               cbAlloc;
    PSLUINT32               cbMaxCommand;
    PSLUINT32               cbMaxDataTransmit;
    PSLUINT32               cbMaxDataReceive;
    PSLUINT32               cbMaxResponse;
    PSLUINT32               cbMaxEvent;
    PSLUINT32               cbMaxDataFlush;
    PSLUINT32               idxBuffer;
    PSLUINT32               idxType;
    PSLSTATUS               ps;
    MTPSTREAMBUFFERMGROBJ*  pmtpstrmBO = PSLNULL;
    MTPSTREAMBUFFERINFO*    pbiCur;
    PSLBYTE*                pbData;

    /*  Currently we reserve only 8 bits for tracking available buffers-
     *  make sure that this is enough to handle each of the buffer
     *  counts
     */

    PSLASSERT(MTPSTREAM_MAX_COMMAND_BUFFERS <= MTPSTREAM_MAX_BUFFERS);
    PSLASSERT(MTPSTREAM_MAX_TRANSMIT_BUFFERS <= MTPSTREAM_MAX_BUFFERS);
    PSLASSERT(MTPSTREAM_MAX_RECEIVE_BUFFERS <= MTPSTREAM_MAX_BUFFERS);
    PSLASSERT(MTPSTREAM_MAX_RESPONSE_BUFFERS <= MTPSTREAM_MAX_BUFFERS);
    PSLASSERT(MTPSTREAM_MAX_EVENT_BUFFERS <= MTPSTREAM_MAX_BUFFERS);
    PSLASSERT(MTPSTREAM_MAX_FLUSH_BUFFERS <= MTPSTREAM_MAX_BUFFERS);

    /*  And validate that the buffer indices line up
     */

    PSLASSERT(MTPBUFFER_TO_INDEX(MTPBUFFER_COMMAND) ==
                            MTPSTREAMBUFFERINDEX_COMMAND);
    PSLASSERT(MTPBUFFER_TO_INDEX(MTPBUFFER_TRANSMIT_DATA) ==
                            MTPSTREAMBUFFERINDEX_TRANSMIT_DATA);
    PSLASSERT(MTPBUFFER_TO_INDEX(MTPBUFFER_RECEIVE_DATA) ==
                            MTPSTREAMBUFFERINDEX_RECEIVE_DATA);
    PSLASSERT(MTPBUFFER_TO_INDEX(MTPBUFFER_RESPONSE) ==
                            MTPSTREAMBUFFERINDEX_RESPONSE);
    PSLASSERT(MTPBUFFER_TO_INDEX(MTPBUFFER_EVENT) ==
                            MTPSTREAMBUFFERINDEX_EVENT);
    PSLASSERT(MTPBUFFER_TO_INDEX(MTPSTREAMBUFFER_FLUSH_DATA) ==
                            MTPSTREAMBUFFERINDEX_FLUSH_DATA);

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpstrmbm)
    {
        *pmtpstrmbm = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((0 == cbMaxCmdTransmit) || (0 == cbMaxCmdReceive) ||
        (0 == cbMaxEventTransmit) || (0 == cbMaxEventReceive) ||
        (PSLNULL == pmtpstrmbm))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  We need to send all of the standard MTP elements out of a single buffer
     */

    if ((sizeof(MTPOPERATION) > cbMaxCmdReceive) ||
        (sizeof(MTPOPERATION) > cbMaxCmdTransmit) ||
        (sizeof(MTPRESPONSE) > cbMaxCmdReceive) ||
        (sizeof(MTPRESPONSE) > cbMaxCmdTransmit) ||
        (sizeof(MTPEVENT) > cbMaxEventReceive) ||
        (sizeof(MTPEVENT) > cbMaxEventTransmit))
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  We require at least DWORD alignment on the buffers
     */

    PSLASSERT(0 == (0x3 & cbAlignment));
    if (0x3 & cbAlignment)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Determine the max size for each individual buffer and align it on
     *  an appropriate packet boundary
     */

    cbMaxCommand = sizeof(MTPOPERATION);
    cbMaxDataTransmit = cbMaxCmdTransmit - cbDataHeader;
    cbMaxDataReceive = cbMaxCmdReceive - cbDataHeader;
    cbMaxResponse = sizeof(MTPRESPONSE);
    cbMaxEvent = sizeof(MTPEVENT);
    cbMaxDataFlush = MTPSTREAM_FLUSH_BUFFER_SIZE;

    /*  Determine the total allocation size necessary to host all the object
     *  and all of the buffers and then allocate the object
     *
     *  NOTE: Buffers may be very large and do not need to be zero inited
     *  so we do not do it all at once here
     */

    cbAlloc = sizeof(MTPSTREAMBUFFERMGROBJ) + cbAlignment +
                (sizeof(PSLBYTE*) * MTPSTREAM_MAX_COMMAND_BUFFERS) +
                (cbMaxCommand * MTPSTREAM_MAX_COMMAND_BUFFERS) +
                (sizeof(PSLBYTE*) * MTPSTREAM_MAX_TRANSMIT_BUFFERS) +
                (cbMaxDataTransmit * MTPSTREAM_MAX_TRANSMIT_BUFFERS) +
                (sizeof(PSLBYTE*) * MTPSTREAM_MAX_RECEIVE_BUFFERS) +
                (cbMaxDataReceive * MTPSTREAM_MAX_RECEIVE_BUFFERS) +
                (sizeof(PSLBYTE*) * MTPSTREAM_MAX_RESPONSE_BUFFERS) +
                (cbMaxResponse * MTPSTREAM_MAX_RESPONSE_BUFFERS) +
                (sizeof(PSLBYTE*) * MTPSTREAM_MAX_EVENT_BUFFERS) +
                (cbMaxEvent * MTPSTREAM_MAX_EVENT_BUFFERS) +
                (sizeof(PSLBYTE*) * MTPSTREAM_MAX_FLUSH_BUFFERS) +
                (cbMaxDataFlush * MTPSTREAM_MAX_FLUSH_BUFFERS) +
                ((sizeof(MTPSTREAMBUFFERHDR) + cbAlignment) *
                            MTPSTREAM_TOTAL_BUFFERS);
    pmtpstrmBO = (MTPSTREAMBUFFERMGROBJ*)PSLMemAlloc(PSLMEM_FIXED, cbAlloc);
    if (PSLNULL == pmtpstrmBO)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }
    PSLZeroMemory(pmtpstrmBO, sizeof(*pmtpstrmBO));

#ifdef PSL_ASSERTS
    /*  Initialize the cookie
     */

    pmtpstrmBO->dwCookie = MTPSTREAMBUFFERMGROBJ_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Create a mutex for this object
     */

    ps = PSLMutexOpen(0, PSLNULL, &(pmtpstrmBO->hMutex));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Walk through and initialize each of the buffer pointers to the start
     *  of their buffer- remember that each buffer consists of the maximum
     *  buffer for the specified type, a buffer header, and space to align
     *  the buffer on
     */

    pbData = PSLMemAlign((PSLBYTE*)pmtpstrmBO + sizeof(MTPSTREAMBUFFERMGROBJ),
                            cbAlignment);
    for (idxType = 0; idxType < MTPSTREAMBUFFERINDEX_MAX_INDEX; idxType++)
    {
        /*  Save some redirection
         */

        pbiCur = &(pmtpstrmBO->rgbiTypes[idxType]);

        /*  Based on the type initialize the data in the buffer info
         */

        switch (idxType)
        {
        case MTPSTREAMBUFFERINDEX_COMMAND:
            pbiCur->cBuffers = MTPSTREAM_MAX_COMMAND_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxCommand;
            break;

        case MTPSTREAMBUFFERINDEX_TRANSMIT_DATA:
            pbiCur->cBuffers = MTPSTREAM_MAX_TRANSMIT_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxDataTransmit;
            break;

        case MTPSTREAMBUFFERINDEX_RECEIVE_DATA:
            pbiCur->cBuffers = MTPSTREAM_MAX_RECEIVE_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxDataReceive;
            break;

        case MTPSTREAMBUFFERINDEX_RESPONSE:
            pbiCur->cBuffers = MTPSTREAM_MAX_RESPONSE_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxResponse;
            break;

        case MTPSTREAMBUFFERINDEX_EVENT:
            pbiCur->cBuffers = MTPSTREAM_MAX_EVENT_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxEvent;
            break;

        case MTPSTREAMBUFFERINDEX_FLUSH_DATA:
            pbiCur->cBuffers = MTPSTREAM_MAX_FLUSH_BUFFERS;
            pbiCur->cbMaxBuffer = cbMaxDataFlush;
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Assign the buffer list
         */

        pbiCur->rgpbBuffers = (PSLBYTE**)pbData;
        pbData += sizeof(PSLBYTE*) * pbiCur->cBuffers;

        /*  And initialize the buffer pointers
         */

        for (idxBuffer = 0;
                idxBuffer < pbiCur->cBuffers;
                idxBuffer++, pbData = PSLMemAlign((pbData +
                            pbiCur->cbMaxBuffer + sizeof(MTPSTREAMBUFFERHDR)),
                            cbAlignment))
        {
            pbiCur->rgpbBuffers[idxBuffer] = pbData;
        }
        PSLASSERT(pbData < ((PSLBYTE*)pmtpstrmBO + cbAlloc));
    }

    /*  Mark all of the buffers as available in each class
     */

    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_COMMAND] =
                    (PSLUINT32)(1 << MTPSTREAM_MAX_COMMAND_BUFFERS) - 1;
    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_TRANSMIT_DATA] =
                    (PSLUINT32)(1 << MTPSTREAM_MAX_TRANSMIT_BUFFERS) - 1;
    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_RECEIVE_DATA] =
                    (PSLUINT32)(1 << MTPSTREAM_MAX_RECEIVE_BUFFERS) - 1;
    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_RESPONSE] =
                    (PSLUINT32)(1 << MTPSTREAM_MAX_RESPONSE_BUFFERS) - 1;
    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_EVENT] =
                    (PSLUINT32)(1 << MTPSTREAM_MAX_EVENT_BUFFERS) - 1;
    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_FLUSH_DATA] =
                    (PSLUINT32)(1 << MTPSTREAM_MAX_FLUSH_BUFFERS) - 1;

    /*  And store the data sizes
     */

    pmtpstrmBO->cbAlignment = cbAlignment;

    /*  Return the new buffer object
     */

    *pmtpstrmbm = pmtpstrmBO;
    pmtpstrmBO = PSLNULL;

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSTREAMBUFFERMGRDESTROY(pmtpstrmBO);
    return ps;
}


/*
 *  MTPStreamBufferMgrDestroy
 *
 *  Destroys a buffer object
 *
 *  Arguments:
 *      MTPSTREAMBUFFERMGR  mtpstrmbm             Buffer object to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamBufferMgrDestroy(MTPSTREAMBUFFERMGR mtpstrmbm)
{
    PSLHANDLE           hMutexClose = PSLNULL;
    MTPSTREAMBUFFERMGROBJ*  pmtpstrmBO;
    PSLSTATUS           ps;

    /*  Make sure we have work to do
     */

    if (PSLNULL == mtpstrmbm)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Unpack the object
     */

    pmtpstrmBO = (MTPSTREAMBUFFERMGROBJ*)mtpstrmbm;
    PSLASSERT(MTPSTREAMBUFFERMGROBJ_COOKIE == pmtpstrmBO->dwCookie);

    /*  Acquire the mutex if present
     */

    if (PSLNULL != pmtpstrmBO->hMutex)
    {
        ps = PSLMutexAcquire(pmtpstrmBO->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexClose = pmtpstrmBO->hMutex;

        /*  If we have a mutex then we were fully initialized-
         *  go ahead and check to make sure that all buffers
         *  are available at this point
         */

        PSLASSERT((PSLUINT32)(1 << MTPSTREAM_MAX_COMMAND_BUFFERS) - 1 ==
                    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_COMMAND]);
        PSLASSERT((PSLUINT32)(1 << MTPSTREAM_MAX_TRANSMIT_BUFFERS) - 1 ==
                    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_TRANSMIT_DATA]);
        PSLASSERT((PSLUINT32)(1 << MTPSTREAM_MAX_RECEIVE_BUFFERS) - 1 ==
                    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_RECEIVE_DATA]);
        PSLASSERT((PSLUINT32)(1 << MTPSTREAM_MAX_RESPONSE_BUFFERS) - 1 ==
                    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_RESPONSE]);
        PSLASSERT((PSLUINT32)(1 << MTPSTREAM_MAX_EVENT_BUFFERS) - 1 ==
                    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_EVENT]);
        PSLASSERT((PSLUINT32)(1 << MTPSTREAM_MAX_FLUSH_BUFFERS) - 1 ==
                    pmtpstrmBO->rgbAvailable[MTPSTREAMBUFFERINDEX_FLUSH_DATA]);
    }

    /*  Release the memory
     */

    PSLMemFree(pmtpstrmBO);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexClose);
    return ps;
}


/*
 *  MTPStreamBufferMgrRequest
 *
 *  Requests a buffer.  If the buffer is not available a request is recorded
 *  so that when the buffer becomes available it can be fulfilled
 *  asynchronously
 *
 *  Arguments:
 *      MTPSTREAMBUFFERMGR
 *                      mtpstrmbm           Buffer object to fulfill request
 *      PSLUINT32       dwBufferType        Type of buffer requested
 *      PSLUINT32       cbRequested         Size of buffer requested
 *      PSLPARAM        aParam              Parameter to return on an
 *                                            asynchronous completion
 *      PSLVOID*        ppvBuffer           Return location for buffer
 *      PSLUINT32*      pcbActual           Actual number of bytes available
 *      PSLMSGQUEUE     mqAsync             Message queue for async response
 *      PSLMSGPOOL      mpAsync             Message pool for async response
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_WOULD_BLOCK if the
 *                      request cannot be immediately fulfilled
 *
 */

PSLSTATUS PSL_API MTPStreamBufferMgrRequest(MTPSTREAMBUFFERMGR mtpstrmbm,
                            PSLUINT32 dwBufferType, PSLUINT32 cbRequested,
                            PSLPARAM aParam, PSLVOID** ppvBuffer,
                            PSLUINT32* pcbActual, PSLMSGQUEUE mqAsync,
                            PSLMSGPOOL mpAsync)
{
    PSLBYTE                 bAvailable;
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLUINT32               idxBuffer;
    PSLUINT32               idxEntry;
    PSLBYTE*                pbBuffer = PSLNULL;
    MTPSTREAMBUFFERREQUEST* pmtpstrmBR;
    MTPSTREAMBUFFERMGROBJ*  pmtpstrmBO;
    MTPSTREAMBUFFERHDR*     pmtpstrmh;
    PSLSTATUS               ps;
    PSLMSG*                 pMsg = PSLNULL;

    /*  Clear results for safety
     */

    if (PSLNULL != ppvBuffer)
    {
        *ppvBuffer = PSLNULL;
    }
    if (PSLNULL != pcbActual)
    {
        *pcbActual = 0;
    }

    /*  Determine the buffer index
     */

    idxBuffer = MTPBUFFER_TO_INDEX(dwBufferType);

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmbm) ||
        (MTPSTREAMBUFFERINDEX_MAX_INDEX <= idxBuffer) ||
        ((PSLNULL != ppvBuffer) && (PSLNULL == pcbActual)) ||
        ((PSLNULL == ppvBuffer) && (PSLNULL == mqAsync)) ||
        ((PSLNULL != mqAsync) && (PSLNULL == mpAsync)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the object
     */

    pmtpstrmBO = (MTPSTREAMBUFFERMGROBJ*)mtpstrmbm;
    PSLASSERT(MTPSTREAMBUFFERMGROBJ_COOKIE == pmtpstrmBO->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmtpstrmBO->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmtpstrmBO->hMutex;

    /*  Check to see if all buffers are in use
     */

    if (0 == pmtpstrmBO->rgbAvailable[idxBuffer])
    {
        /*  If we do not have a queue provided we cannot create a pending
         *  request- report not available as an error
         */

        if (PSLNULL == mqAsync)
        {
            ps = PSLERROR_NOT_AVAILABLE;
            goto Exit;
        }

        /*  Save some redirection
         */

        pmtpstrmBR = &(pmtpstrmBO->rgbiTypes[idxBuffer].brPending);

        /*  If we already have a pending request report that we have
         *  insufficient buffer to hande this
         */

        PSLASSERT(0 == pmtpstrmBR->cbRequest);
        if (0 != pmtpstrmBR->cbRequest)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  Record the information about the request and report that the
         *  buffer was not available
         */

        pmtpstrmBR->cbRequest = cbRequested;
        pmtpstrmBR->aParam = aParam;
        pmtpstrmBR->mqNotify = mqAsync;
        pmtpstrmBR->mpNotify = mpAsync;
        ps = PSLSUCCESS_WOULD_BLOCK;
        goto Exit;
    }

    /*  We have a buffer available- figure out which one in the list is
     *  available
     */

    for (idxEntry = 0, bAvailable = pmtpstrmBO->rgbAvailable[idxBuffer];
            (idxEntry < MTPSTREAM_MAX_BUFFERS) && (0 == (0x1 & bAvailable));
            bAvailable >>= 1, idxEntry++)
    {
    }

    /*  Retrieve the buffer and determine the correct alignment while accounting
     *  for the header
     */

    pbBuffer = PSLMemAlign(
                (pmtpstrmBO->rgbiTypes[idxBuffer].rgpbBuffers[idxEntry] +
                        sizeof(MTPSTREAMBUFFERHDR)),
                pmtpstrmBO->cbAlignment);
    pmtpstrmh = ((MTPSTREAMBUFFERHDR*)pbBuffer) - 1;

    /*  Initialize the header
     */

#ifdef PSL_ASSERTS
    pmtpstrmh->dwCookie = MTPSTREAMBUFFER_COOKIE;
#endif  /* PSL_ASSERTS */
    MTPSTREAMBUFFERHDR_SET_INFO(pmtpstrmh, idxBuffer, idxEntry);
    pmtpstrmh->cbSize = min(pmtpstrmBO->rgbiTypes[idxBuffer].cbMaxBuffer,
                            cbRequested);
    pmtpstrmh->mtpstrmbm = mtpstrmbm;

    /*  Record that the buffer is in use
     */

    pmtpstrmBO->rgbAvailable[idxBuffer] &= ~(1 << idxEntry);

    /*  Figure out if we can return the buffer directly or if it should be
     *  returned in a message
     */

    if (PSLNULL != ppvBuffer)
    {
        /*  Return the buffer directly
         */

        *ppvBuffer = pbBuffer;
        *pcbActual = pmtpstrmh->cbSize;

        /*  Report success
         */

        ps = PSLSUCCESS;
    }
    else
    {
        /*  Allocate a message to return the buffer in
         */

        ps = PSLMsgAlloc(mpAsync, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = MTPSTREAMBUFFERMGRMSG_BUFFER_AVAILABLE;
        pMsg->aParam0 = INDEX_TO_MTPBUFFER(idxBuffer);
        pMsg->aParam1 = aParam;
        pMsg->aParam2 = (PSLPARAM)pbBuffer;
        pMsg->alParam = pmtpstrmh->cbSize;

        ps = PSLMsgPost(mqAsync, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = PSLNULL;

        /*  Report that the buffer is not available
         */

        ps = PSLSUCCESS_WOULD_BLOCK;
    }
    pbBuffer = PSLNULL;

Exit:
    SAFE_MTPSTREAMBUFFERMGRRELEASE(pbBuffer);
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPStreamBufferMgrCancelRequest
 *
 *  Cancels any pending buffer requests for the type specified
 *
 *  Arguments:
 *      MTPSTREAMBUFFERMGR
 *                      mtpstrmbm           Buffer object to fulfill request
 *      PSLUINT32       dwBufferType        Type of buffer requested
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if no
 *                        request was pending
 *
 */

PSLSTATUS PSL_API MTPStreamBufferMgrCancelRequest(MTPSTREAMBUFFERMGR mtpstrmbm,
                            PSLUINT32 dwBufferType)
{
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLUINT32               idxBuffer;
    MTPSTREAMBUFFERREQUEST* pmtpstrmBR;
    MTPSTREAMBUFFERMGROBJ*  pmtpstrmBO;
    PSLSTATUS               ps;

    /*  Determine the buffer index
     */

    idxBuffer = MTPBUFFER_TO_INDEX(dwBufferType);

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpstrmbm) ||
        (MTPSTREAMBUFFERINDEX_MAX_INDEX <= idxBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the object
     */

    pmtpstrmBO = (MTPSTREAMBUFFERMGROBJ*)mtpstrmbm;
    PSLASSERT(MTPSTREAMBUFFERMGROBJ_COOKIE == pmtpstrmBO->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmtpstrmBO->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmtpstrmBO->hMutex;

    /*  Save some redirection
     */

    pmtpstrmBR = &(pmtpstrmBO->rgbiTypes[idxBuffer].brPending);

    /*  Determine if we have a request and then always wipe it
     */

    ps = (0 != pmtpstrmBR->cbRequest) ? PSLSUCCESS : PSLSUCCESS_NOT_FOUND;
    PSLZeroMemory(pmtpstrmBR, sizeof(*pmtpstrmBR));

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPStreamBufferMgrRelease
 *
 *  Releases a buffer handling any pending requests
 *
 *  Arguments:
 *      PSLVOID*        pvBuffer            Buffer to release
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamBufferMgrRelease(PSLVOID* pvBuffer)
{
    PSLUINT32               idxType;
    PSLUINT32               idxEntry;
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLMSG*                 pMsg = PSLNULL;
    MTPSTREAMBUFFERREQUEST* pmtpstrmBR;
    MTPSTREAMBUFFERMGROBJ*  pmtpstrmBO;
    MTPSTREAMBUFFERHDR*     pmtpstrmh;
    PSLSTATUS               ps;

    /*  Make sure we have work to do
     */

    if (PSLNULL == pvBuffer)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Locate the header for this buffer
     */

    pmtpstrmh = ((MTPSTREAMBUFFERHDR*)pvBuffer) - 1;
    PSLASSERT(MTPSTREAMBUFFER_COOKIE == pmtpstrmh->dwCookie);

    /*  And unpack it
     */

    idxType = MTPSTREAMBUFFERHDR_GET_INDEX(pmtpstrmh);
    idxEntry = MTPSTREAMBUFFERHDR_GET_ENTRY(pmtpstrmh);
    pmtpstrmBO = (MTPSTREAMBUFFERMGROBJ*)(pmtpstrmh->mtpstrmbm);
    PSLASSERT(MTPSTREAMBUFFERMGROBJ_COOKIE == pmtpstrmBO->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmtpstrmBO->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmtpstrmBO->hMutex;

    /*  Check to see if there is a pending request for this type
     */

    if (0 != pmtpstrmBO->rgbiTypes[idxType].brPending.cbRequest)
    {
        /*  Save some redirection
         */

        pmtpstrmBR = &(pmtpstrmBO->rgbiTypes[idxType].brPending);

        /*  At this point the way the buffer is laid out will not change-
         *  we just want to update the size
         */

        pmtpstrmh->cbSize = min(pmtpstrmBR->cbRequest,
                            pmtpstrmBO->rgbiTypes[idxType].cbMaxBuffer);

        /*  Allocate a message and forward the information along to the
         *  destination
         */

        ps = PSLMsgAlloc(pmtpstrmBR->mpNotify, &pMsg);
        if (PSL_SUCCEEDED(ps))
        {
            pMsg->msgID = MTPSTREAMBUFFERMGRMSG_BUFFER_AVAILABLE;
            pMsg->aParam0 = INDEX_TO_MTPBUFFER(idxType);
            pMsg->aParam1 = pmtpstrmBR->aParam;
            pMsg->aParam2 = (PSLPARAM)pvBuffer;
            pMsg->alParam = pmtpstrmh->cbSize;

            ps = PSLMsgPost(pmtpstrmBR->mqNotify, pMsg);
            if (PSL_SUCCEEDED(ps))
            {
                PSLZeroMemory(pmtpstrmBR, sizeof(*pmtpstrmBR));
                pMsg = PSLNULL;
                goto Exit;
            }
        }

        /*  We were unable to succesfully fulfill this request- leave it
         *  pending but go ahead and return the buffer
         */
    }

    /*  Mark the buffer as available
     */

    pmtpstrmBO->rgbAvailable[idxType] |= (1 << idxEntry);

    /*  And clear the header so that we do not have a re-use issue
     */

    PSLZeroMemory(pmtpstrmh, sizeof(*pmtpstrmh));

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  MTPStreamBufferMgrGetInfo
 *
 *  Returns information about an MTP Stream buffer
 *
 *  Arguments:
 *      PSLVOID*        pvBuffer            Buffer to check
 *      MTPSTREAMBUFFERMGR* pmtpstrmbm            Buffer this is from
 *      PSLUINT32*      pcbSize             Size of the buffer
 *      PSLUINT32*      pdwBufferType       Buffer type
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamBufferMgrGetInfo(PSLVOID* pvBuffer,
                            MTPSTREAMBUFFERMGR* pmtpstrmbm, PSLUINT32* pcbSize,
                            PSLUINT32* pdwBufferType)
{
    MTPSTREAMBUFFERHDR* pmtpstrmh;
    PSLSTATUS           ps;

    /*  Clear results for safety
     */

    if (PSLNULL != pmtpstrmbm)
    {
        *pmtpstrmbm = PSLNULL;
    }
    if (PSLNULL != pcbSize)
    {
        *pcbSize = 0;
    }
    if (PSLNULL != pdwBufferType)
    {
        *pdwBufferType = MTPBUFFER_UNKNOWN;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pvBuffer) ||
        ((PSLNULL == pmtpstrmbm) && (PSLNULL == pcbSize) &&
                            (PSLNULL == pdwBufferType)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Locate the header for this buffer
     */

    pmtpstrmh = MTPSTREAM_BUFFER_TO_HEADER(pvBuffer);
    PSLASSERT(MTPSTREAMBUFFER_COOKIE == pmtpstrmh->dwCookie);

    /*  And unpack the information from it
     */

    if (PSLNULL != pmtpstrmbm)
    {
        *pmtpstrmbm = pmtpstrmh->mtpstrmbm;
    }
    if (PSLNULL != pcbSize)
    {
        *pcbSize = pmtpstrmh->cbSize;
    }
    if (PSLNULL != pdwBufferType)
    {
        *pdwBufferType =
                INDEX_TO_MTPBUFFER(MTPSTREAMBUFFERHDR_GET_INDEX(pmtpstrmh));
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPStreamBufferMgrGetBufferHeader
 *
 *  Returns a pointer to the buffer's header.  The header precedes the
 *  actual buffer pointer and is present to enable large data packets to
 *  have the MTP Stream data packet header attached without having to require
 *  a copy to create a contiquous packet for sending.
 *
 *  Arguments:
 *      PSL_CONST PSLVOID*
 *                      pvBuffer            Buffer to use
 *      PSLVOID**       ppvHeader           Return buffer for header
 *      PSLUINT32*      pcbHeader           Size of the header block on the
 *                                          buffer.
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPStreamBufferMgrGetBufferHeader(PSL_CONST PSLVOID* pvBuffer,
                            PSLVOID** ppvHeader, PSLUINT32* pcbHeader)
{
    PSLSTATUS                   ps;
    PSL_CONST MTPSTREAMBUFFERHDR*   pmtpstrmh;

    /*  Clear result for safety
     */

    if (PSLNULL != ppvHeader)
    {
        *ppvHeader = PSLNULL;
    }
    if (PSLNULL != pcbHeader)
    {
        *pcbHeader = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pvBuffer) ||
        ((PSLNULL == ppvHeader) && (PSLNULL == pcbHeader)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the header and make sure it is one of ours
     */

    pmtpstrmh = MTPSTREAM_BUFFER_TO_HEADER(pvBuffer);
    PSLASSERT(MTPSTREAMBUFFER_COOKIE == pmtpstrmh->dwCookie);

    /*  And return the size of the header
     */

    if (PSLNULL != pcbHeader)
    {
        *pcbHeader = PSLARRAYSIZE(pmtpstrmh->rgbHeader);
    }
    if (PSLNULL != ppvHeader)
    {
        *ppvHeader = (PSLVOID*)pmtpstrmh->rgbHeader;
    }
    ps = PSLSUCCESS;

Exit:
    return ps;
}


