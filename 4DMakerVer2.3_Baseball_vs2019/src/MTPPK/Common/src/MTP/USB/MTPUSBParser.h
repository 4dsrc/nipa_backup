/*
 *  MTPUSBParser.h
 *
 *  Contains declartions for the MTPUSB parser support
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPUSBPARSER_H_
#define _MTPUSBPARSER_H_

/*
 *  Parser Flags
 *
 */

#define MTPUSBPARSERFLAGS_EMPTY         0x00000001
#define MTPUSBPARSERFLAGS_FIRST_PACKET  0x00000002

/*
 *  MTPUSBParserUSBBufferToMTPOperation
 *
 *  Converts a USB buffer to a MTPOPERATION
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBParserUSBBufferToMTPOperation(
                            PSLVOID* pvBuffer,
                            PSLUINT32 cbBuffer,
                            PSLUINT32 dwFlags,
                            PXMTPOPERATION* ppmtpOp,
                            PSLUINT32* pcbOperation);

/*
 *  MTPUSBParserMTPOperationToUSBBuffer
 *
 *  Converts a MTPOPERATION request to a USB buffer
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBParserMTPOperationToUSBBuffer(
                            PXMTPOPERATION pmtpOp,
                            PSLUINT32 cbOperation,
                            PSLUINT32 dwFlags,
                            PSLVOID** ppvBuffer,
                            PSLUINT32* pcbBuffer);


/*
 *  MTPUSBParserUSBBufferToMTPDataBuffer
 *
 *  Converts a USB buffer to a data buffer
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBParserUSBBufferToMTPDataBuffer(
                            PSLVOID* pvBuffer,
                            PSLUINT32 cbBuffer,
                            PSLUINT32 dwFlags,
                            PSLVOID** ppvDataBuffer,
                            PSLUINT32* pcbDataBuffer);


/*
 *  MTPUSBParserMTPDataBufferToUSBBuffer
 *
 *  Converts a MTP data buffer to a USB data buffer
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBParserMTPDataBufferToUSBBuffer(
                            PSLVOID* pvDataBuffer,
                            PSLUINT32 cbDataBuffer,
                            PSLUINT32 dwFlags,
                            PSLVOID** ppvDataBuffer,
                            PSLUINT32* pcbDataBuffer);

/*
 *  MTPUSBParserUSBBufferToMTPResponse
 *
 *  Converts a USB formatted buffer to an MTPRESPONSE
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBParserUSBBufferToMTPResponse(
                            PSLVOID* pvBuffer,
                            PSLUINT32 cbBuffer,
                            PSLUINT32 dwFlags,
                            PXMTPRESPONSE* ppmtpResponse,
                            PSLUINT32* pcbResponse);


/*
 *  MTPUSBParserMTPResponseToUSBBuffer
 *
 *  Converts an MTPRESPONSE to a USB formatted buffer
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBParserMTPResponseToUSBBuffer(
                            PXMTPRESPONSE pmtpResponse,
                            PSLUINT32 cbResponse,
                            PSLUINT32 dwFlags,
                            PSLVOID* pvBuffer,
                            PSLUINT32* pcbBuffer);


/*
 *  MTPUSBParserUSBBufferToMTPEvent
 *
 *  Converts a USB formatted buffer to an MTPEVENT
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBParserUSBBufferToMTPEvent(
                            PSLVOID* pvBuffer,
                            PSLUINT32 cbBuffer,
                            PSLUINT32 dwFlags,
                            PXMTPEVENT* ppmtpEvent,
                            PSLUINT32* pcbEvent);


/*
 *  MTPUSBParserMTPEventToUSBBuffer
 *
 *  Converts an MTPEVENT to a USB formatted buffer
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBParserMTPEventToUSBBuffer(
                            PXMTPEVENT pmtpEvent,
                            PSLUINT32 cbEvent,
                            PSLUINT32 dwFlags,
                            PSLVOID* pvBuffer,
                            PSLUINT32* pcbBuffer);

#endif  /* _MTPUSBPARSER_H_ */

