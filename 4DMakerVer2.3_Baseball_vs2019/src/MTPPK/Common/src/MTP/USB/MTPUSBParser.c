/*
 *  MTPUSBParser.c
 *
 *  Contains definitions of the MTP/USB parser utilities
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPUSBTransportPrecomp.h"

/*  Local Defines
 */

#define MTPUSBOPERATIONREQUEST_MIN_SIZE \
    PSLOFFSETOF(MTPUSBPACKET_OPERATION_REQUEST, mtpOp.dwParam1)

#define MTPUSBOPERATIONRESPONSE_MIN_SIZE \
    PSLOFFSETOF(MTPUSBPACKET_OPERATION_RESPONSE, mtpResponse.dwParam1)

#define MTPUSBEVENT_MIN_SIZE \
    PSLOFFSETOF(MTPUSBPACKET_EVENT, mtpEvent.dwParam1)

/*  Local Types
 */

/*  Local Functions
 */

/*  Local Variables
 */


/*
 *  MTPUSBParserUSBBufferToMTPOperation
 *
 *  Converts a USB buffer to a MTPOPERATION in place.
 *
 *  Arguments:
 *      PSLVOID*        pvBuffer            Buffer from USB device
 *      PSLUINT32       cbBuffer            Size of the USB buffer
 *      PSLUINT32       dwFlags             Parser flags
 *      PXMTPOPERATION* ppmtpOp             MTPOPERATION in the buffer
 *      PSLUINT32*      pcbOperation        Length of the operation
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBParserUSBBufferToMTPOperation(PSLVOID* pvBuffer,
                            PSLUINT32 cbBuffer, PSLUINT32 dwFlags,
                            PXMTPOPERATION* ppmtpOp, PSLUINT32* pcbOperation)
{
    PSLUINT32                           cbPacket;
    PSLSTATUS                           ps;
    PXMTPUSBPACKET_OPERATION_REQUEST    pmtpusbOp;
    PSLUINT16                           wContainerType;

    /*  Clear results for safety
     */

    if (PSLNULL != ppmtpOp)
    {
        *ppmtpOp = PSLNULL;
    }
    if (PSLNULL != pcbOperation)
    {
        *pcbOperation = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pvBuffer) ||
        (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags) && (0 == cbBuffer)) ||
        (PSLNULL == ppmtpOp) || ((0 < cbBuffer) && (PSLNULL == pcbOperation)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pmtpusbOp = (PXMTPUSBPACKET_OPERATION_REQUEST)pvBuffer;

    /*  If the length is specified it must match minimum and maximum sizes
     */

    if ((0 < cbBuffer) &&
        ((sizeof(MTPUSBPACKET_OPERATION_REQUEST) < cbBuffer) ||
            (cbBuffer < ((MTPUSBPARSERFLAGS_EMPTY & dwFlags) ?
                    sizeof(MTPUSBPACKET_OPERATION_REQUEST) :
                    MTPUSBOPERATIONREQUEST_MIN_SIZE))))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Parse the contents of the packet if required
     */

    if (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags))
    {
        /*  Validate the packet by checking for
         *      -Maximum length
         *      -Minimum length
         *      -Complete packet received
         *      -Container type
         */

        PSLASSERT(0 != cbBuffer);
        cbPacket = MTPLoadUInt32(&(pmtpusbOp->header.cbPacket));
        wContainerType = MTPLoadUInt16(&(pmtpusbOp->header.wContainerType));
        if ((MTPUSBCONTAINER_COMMAND != wContainerType) ||
            (cbBuffer != cbPacket))
        {
            ps = PSLERROR_INVALID_PACKET;
            goto Exit;
        }
    }

    /*  And return the result
     */

    *ppmtpOp = &(pmtpusbOp->mtpOp);
    if ((PSLNULL != pcbOperation) && (0 < cbBuffer))
    {
        PSLASSERT(sizeof(MTPUSBPACKET_HEADER) <= cbBuffer);
        *pcbOperation = cbBuffer - sizeof(MTPUSBPACKET_HEADER);
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPUSBParserMTPOperationToUSBBuffer
 *
 *  Converts a MTPOPERATION request back to a USB buffer in place
 *
 *  Arguments:
 *      PXMTPOPERATION  pmtpOp              Operation to update
 *      PSLUINT32       cbOperation         Length of the operation
 *      PSLUINT32       dwFlags             Parser flags
 *      PSLVOID**       ppvBuffer           Return location for USB buffer
 *      PSLUINT32*      pcbBuffer           Return location for length
 *
 */

PSLSTATUS MTPUSBParserMTPOperationToUSBBuffer(PXMTPOPERATION pmtpOp,
                            PSLUINT32 cbOperation, PSLUINT32 dwFlags,
                            PSLVOID** ppvBuffer, PSLUINT32* pcbBuffer)
{
    PSLUINT32                           cbPacket;
    PSLSTATUS                           ps;
    PXMTPUSBPACKET_OPERATION_REQUEST    pmtpusbOp;

    /*  Clear results for safety
     */

    if (PSLNULL != ppvBuffer)
    {
        *ppvBuffer = PSLNULL;
    }
    if (PSLNULL != pcbBuffer)
    {
        *pcbBuffer = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpOp) || (PSLNULL == ppvBuffer) ||
        (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags) && (0 == cbOperation)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine the root of the USB buffer
     */

    pmtpusbOp = (PXMTPUSBPACKET_OPERATION_REQUEST)
                            ((PXMTPUSBPACKET_HEADER)pmtpOp - 1);
    /*  Validate length if specified
     */

    if ((0 < cbOperation) &&
            ((MTPOPERATION_MIN_SIZE > cbOperation) ||
                (sizeof(*pmtpOp) < cbOperation)))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }
    cbPacket = cbOperation + sizeof(MTPUSBPACKET_HEADER);

    /*  If the buffer has data update it
     */

    if (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags))
    {
        /*  Update the information in the USB buffer with the data here
         */

        MTPStoreUInt32(&(pmtpusbOp->header.cbPacket), cbPacket);
        MTPStoreUInt16(&(pmtpusbOp->header.wContainerType),
                            MTPUSBCONTAINER_COMMAND);
    }

    /*  Return the information about the buffer
     */

    *ppvBuffer = pmtpusbOp;
    if ((PSLNULL != pcbBuffer) && (0 < cbOperation))
    {
        *pcbBuffer = cbPacket;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPUSBParserUSBBufferToMTPDataBuffer
 *
 *  Converts a USB buffer to an MT data buffer in place
 *
 *  Arguments:
 *      PSLVOID*        pvBuffer            USB buffer to convert
 *      PSLUINT32       cbBuffer            Length of the buffer
 *      PSLUINT32       dwFlags             Parser flags
 *      PSLVOID**       ppvDataBuffer       Return for MTP Data buffer
 *      PSLUINT32*      pcbDataBuffer       Length of MTP Data buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBParserUSBBufferToMTPDataBuffer(PSLVOID* pvBuffer,
                            PSLUINT32 cbBuffer, PSLUINT32 dwFlags,
                            PSLVOID** ppvDataBuffer, PSLUINT32* pcbDataBuffer)
{
    PSLSTATUS                   ps;
    PXMTPUSBPACKET_DATA_HEADER  pmtpusbData;
    PSLUINT16                   wContainerType;

    /*  Clear results for safety
     */

    if (PSLNULL != ppvDataBuffer)
    {
        *ppvDataBuffer = PSLNULL;
    }
    if (PSLNULL != pcbDataBuffer)
    {
        *pcbDataBuffer = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pvBuffer) || (PSLNULL == ppvDataBuffer) ||
        ((0 < cbBuffer) && (PSLNULL == pcbDataBuffer)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If this is not the first data packet then we just want to pass the
     *  buffer through untouched
     */

    if (!(MTPUSBPARSERFLAGS_FIRST_PACKET & dwFlags))
    {
        *ppvDataBuffer = pvBuffer;
        if (PSLNULL != pcbDataBuffer)
        {
            *pcbDataBuffer = cbBuffer;
        }
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  If we get here and the buffer is not marked as empty we have to have
     *  a length specified
     */

    if (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags) && (0 == cbBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pmtpusbData = (PXMTPUSBPACKET_DATA_HEADER)pvBuffer;

    /*  If the length is specified it must match minimum and maximum sizes
     */

    if ((0 < cbBuffer) && (sizeof(MTPUSBPACKET_DATA_HEADER) > cbBuffer))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Parse the contents of the packet if required
     */

    if (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags))
    {
        /*  Unpack the USB header and validate the container type
         */

        PSLASSERT(0 != cbBuffer);
        wContainerType = MTPLoadUInt16(&(pmtpusbData->header.wContainerType));
        if (MTPUSBCONTAINER_DATA != wContainerType)
        {
            ps = PSLERROR_INVALID_PACKET;
            goto Exit;
        }
    }

    /*  And return the result
     */

    *ppvDataBuffer = (PSLBYTE*)pmtpusbData + sizeof(MTPUSBPACKET_DATA_HEADER);
    if ((PSLNULL != pcbDataBuffer) && (0 < cbBuffer))
    {
        PSLASSERT(sizeof(MTPUSBPACKET_DATA_HEADER) <= cbBuffer);
        *pcbDataBuffer = cbBuffer - sizeof(MTPUSBPACKET_DATA_HEADER);
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPUSBParserMTPDataBufferToUSBBuffer
 *
 *  Converts a MTP data buffer to a USB data buffer in place
 *
 *  Arguments:
 *      PSLVOID*        pvDataBuffer        MTP data buffer
 *      PSLUINT32       cbDataBuffer        Length of the data buffer
 *      PSLUINT32       dwFlags             Parser flags
 *      PSLVOID**       ppvBuffer           Return for USB buffer
 *      PSLUINT32*      pcbBuffer           Return for length of USB buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBParserMTPDataBufferToUSBBuffer(PSLVOID* pvDataBuffer,
                            PSLUINT32 cbDataBuffer, PSLUINT32 dwFlags,
                            PSLVOID** ppvBuffer, PSLUINT32* pcbBuffer)
{
    PSLUINT32                   cbPacket;
    PSLSTATUS                   ps;
    PXMTPUSBPACKET_DATA_HEADER  pmtpusbData;

    /*  Clear results for safety
     */

    if (PSLNULL != ppvBuffer)
    {
        *ppvBuffer = PSLNULL;
    }
    if (PSLNULL != pcbBuffer)
    {
        *pcbBuffer = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pvDataBuffer) || (PSLNULL == ppvBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If this is not the first packet then we just pass the data through
     *  untouched
     */

    if (!(MTPUSBPARSERFLAGS_FIRST_PACKET & dwFlags))
    {
        *ppvBuffer = pvDataBuffer;
        if (PSLNULL != pcbBuffer)
        {
            *pcbBuffer = cbDataBuffer;
        }
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Determine the root of the USB buffer
     */

    pmtpusbData = (PXMTPUSBPACKET_DATA_HEADER)pvDataBuffer - 1;
    cbPacket = cbDataBuffer + sizeof(MTPUSBPACKET_DATA_HEADER);

    /*  If the buffer has data update it
     */

    if (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags))
    {
        /*  Update the information in the USB buffer with the data here
         */

        MTPStoreUInt32(&(pmtpusbData->header.cbPacket), cbPacket);
        MTPStoreUInt16(&(pmtpusbData->header.wContainerType),
                            MTPUSBCONTAINER_DATA);

        /*  Clear the operation and transaction ID
         */

        MTPStoreUInt16(&(pmtpusbData->wOpCode), 0);
        MTPStoreUInt32(&(pmtpusbData->dwTransactionID), 0);
    }

    /*  Return the information about the buffer
     */

    *ppvBuffer = pmtpusbData;
    if (PSLNULL != pcbBuffer)
    {
        *pcbBuffer = cbPacket;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPUSBParserUSBBufferToMTPResponse
 *
 *  Converts a USB buffer to a MTPRESPONSE in place.
 *
 *  Arguments:
 *      PSLVOID*        pvBuffer            Buffer from USB device
 *      PSLUINT32       cbBuffer            Size of the USB buffer
 *      PSLUINT32       dwFlags             Parser flags
 *      PXMTPRESPONSE*  ppmtpResponse       MTPRESPONSE in the buffer
 *      PSLUINT32*      pcbResponse         Length of the response
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBParserUSBBufferToMTPResponse(PSLVOID* pvBuffer,
                            PSLUINT32 cbBuffer, PSLUINT32 dwFlags,
                            PXMTPRESPONSE* ppmtpResponse,
                            PSLUINT32* pcbResponse)
{
    PSLUINT32                           cbPacket;
    PSLSTATUS                           ps;
    PXMTPUSBPACKET_OPERATION_RESPONSE   pmtpusbResponse;
    PSLUINT16                           wContainerType;

    /*  Clear results for safety
     */

    if (PSLNULL != ppmtpResponse)
    {
        *ppmtpResponse = PSLNULL;
    }
    if (PSLNULL != pcbResponse)
    {
        *pcbResponse = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pvBuffer) ||
        (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags) && (0 == cbBuffer)) ||
        (PSLNULL == ppmtpResponse) ||
        ((0 < cbBuffer) && (PSLNULL == pcbResponse)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pmtpusbResponse = (PXMTPUSBPACKET_OPERATION_RESPONSE)pvBuffer;

    /*  If the length is specified it must match minimum and maximum sizes
     */

    if ((0 < cbBuffer) &&
        ((sizeof(MTPUSBPACKET_OPERATION_RESPONSE) < cbBuffer) ||
            (cbBuffer < ((MTPUSBPARSERFLAGS_EMPTY & dwFlags) ?
                    sizeof(MTPUSBPACKET_OPERATION_RESPONSE) :
                    MTPUSBOPERATIONRESPONSE_MIN_SIZE))))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Parse the contents of the packet if required
     */

    if (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags))
    {
        /*  Validate the packet by checking for
         *      -Maximum length
         *      -Minimum length
         *      -Complete packet received
         *      -Container type
         */

        PSLASSERT(0 != cbBuffer);
        cbPacket = MTPLoadUInt32(&(pmtpusbResponse->header.cbPacket));
        wContainerType = MTPLoadUInt16(
                            &(pmtpusbResponse->header.wContainerType));
        if ((MTPUSBCONTAINER_RESPONSE != wContainerType) ||
            (cbBuffer != cbPacket))
        {
            ps = PSLERROR_INVALID_PACKET;
            goto Exit;
        }
    }

    /*  And return the result
     */

    *ppmtpResponse = &(pmtpusbResponse->mtpResponse);
    if ((PSLNULL != pcbResponse) && (0 < cbBuffer))
    {
        PSLASSERT(sizeof(MTPUSBPACKET_HEADER) <= cbBuffer);
        *pcbResponse = cbBuffer - sizeof(MTPUSBPACKET_HEADER);
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPUSBParserMTPResponseToUSBBuffer
 *
 *  Converts a MTPRESPONSE request back to a USB buffer in place
 *
 *  Arguments:
 *      PXMTPRESPONSE   pmtpResponse        Response to update
 *      PSLUINT32       cbResponse          Length of the response
 *      PSLUINT32       dwFlags             Parser flags
 *      PSLVOID**       ppvBuffer           Return location for USB buffer
 *      PSLUINT32*      pcbBuffer           Return location for length
 *
 */

PSLSTATUS MTPUSBParserMTPResponseToUSBBuffer(PXMTPRESPONSE pmtpResponse,
                            PSLUINT32 cbResponse, PSLUINT32 dwFlags,
                            PSLVOID** ppvBuffer, PSLUINT32* pcbBuffer)
{
    PSLUINT32                           cbPacket;
    PSLSTATUS                           ps;
    PXMTPUSBPACKET_OPERATION_RESPONSE   pmtpusbResponse;

    /*  Clear results for safety
     */

    if (PSLNULL != ppvBuffer)
    {
        *ppvBuffer = PSLNULL;
    }
    if (PSLNULL != pcbBuffer)
    {
        *pcbBuffer = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpResponse) || (PSLNULL == ppvBuffer) ||
        (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags) && (0 == cbResponse)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine the root of the USB buffer
     */

    pmtpusbResponse = (PXMTPUSBPACKET_OPERATION_RESPONSE)
                            ((PXMTPUSBPACKET_HEADER)pmtpResponse - 1);
    /*  Validate length if specified
     */

    if ((0 < cbResponse) &&
            ((MTPRESPONSE_MIN_SIZE > cbResponse) ||
                (sizeof(*pmtpResponse) < cbResponse)))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }
    cbPacket = cbResponse + sizeof(MTPUSBPACKET_HEADER);

    /*  If the buffer has data update it
     */

    if (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags))
    {
        /*  Update the information in the USB buffer with the data here
         */

        MTPStoreUInt32(&(pmtpusbResponse->header.cbPacket), cbPacket);
        MTPStoreUInt16(&(pmtpusbResponse->header.wContainerType),
                            MTPUSBCONTAINER_RESPONSE);
    }

    /*  Return the information about the buffer
     */

    *ppvBuffer = pmtpusbResponse;
    if ((PSLNULL != pcbBuffer) && (0 < cbResponse))
    {
        *pcbBuffer = cbPacket;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPUSBParserUSBBufferToMTPEvent
 *
 *  Converts a USB buffer to a MTPEVENT in place.
 *
 *  Arguments:
 *      PSLVOID*        pvBuffer            Buffer from USB device
 *      PSLUINT32       cbBuffer            Size of the USB buffer
 *      PSLUINT32       dwFlags             Parser flags
 *      PXMTPEVENT*     ppmtpEvent          MTPEVENT in the buffer
 *      PSLUINT32*      pcbEvent            Length of the event
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBParserUSBBufferToMTPEvent(PSLVOID* pvBuffer,
                            PSLUINT32 cbBuffer, PSLUINT32 dwFlags,
                            PXMTPEVENT* ppmtpEvent, PSLUINT32* pcbEvent)
{
    PSLUINT32               cbPacket;
    PSLSTATUS               ps;
    PXMTPUSBPACKET_EVENT    pmtpusbEvent;
    PSLUINT16               wContainerType;

    /*  Clear results for safety
     */

    if (PSLNULL != ppmtpEvent)
    {
        *ppmtpEvent = PSLNULL;
    }
    if (PSLNULL != pcbEvent)
    {
        *pcbEvent = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pvBuffer) ||
        (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags) && (0 == cbBuffer)) ||
        (PSLNULL == ppmtpEvent) || ((0 < cbBuffer) && (PSLNULL == pcbEvent)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Save some redirection
     */

    pmtpusbEvent = (PXMTPUSBPACKET_EVENT)pvBuffer;

    /*  If the length is specified it must match minimum and maximum sizes
     */

    if ((0 < cbBuffer) &&
        ((sizeof(MTPUSBPACKET_EVENT) < cbBuffer) ||
            (cbBuffer < ((MTPUSBPARSERFLAGS_EMPTY & dwFlags) ?
                    sizeof(MTPUSBPACKET_EVENT) :
                    MTPUSBEVENT_MIN_SIZE))))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Parse the contents of the packet if required
     */

    if (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags))
    {
        /*  Validate the packet by checking for
         *      -Maximum length
         *      -Minimum length
         *      -Complete packet received
         *      -Container type
         */

        PSLASSERT(0 != cbBuffer);
        cbPacket = MTPLoadUInt32(&(pmtpusbEvent->header.cbPacket));
        wContainerType = MTPLoadUInt16(&(pmtpusbEvent->header.wContainerType));
        if ((MTPUSBCONTAINER_EVENT != wContainerType) ||
            (cbBuffer != cbPacket))
        {
            ps = PSLERROR_INVALID_PACKET;
            goto Exit;
        }
    }

    /*  And return the result
     */

    *ppmtpEvent = &(pmtpusbEvent->mtpEvent);
    if ((PSLNULL != pcbEvent) && (0 < cbBuffer))
    {
        PSLASSERT(sizeof(MTPUSBPACKET_HEADER) <= cbBuffer);
        *pcbEvent = cbBuffer - sizeof(MTPUSBPACKET_HEADER);
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  MTPUSBParserMTPEventToUSBBuffer
 *
 *  Converts a MTPEVENT back to a USB buffer in place
 *
 *  Arguments:
 *      PXMTPEVENT      pmtpEvent           Event to update
 *      PSLUINT32       cbEvent             Length of the event
 *      PSLUINT32       dwFlags             Parser flags
 *      PSLVOID**       ppvBuffer           Return location for USB buffer
 *      PSLUINT32*      pcbBuffer           Return location for length
 *
 */

PSLSTATUS MTPUSBParserMTPEventToUSBBuffer(PXMTPEVENT pmtpEvent,
                            PSLUINT32 cbEvent, PSLUINT32 dwFlags,
                            PSLVOID** ppvBuffer, PSLUINT32* pcbBuffer)
{
    PSLUINT32               cbPacket;
    PSLSTATUS               ps;
    PXMTPUSBPACKET_EVENT    pmtpusbEvent;

    /*  Clear results for safety
     */

    if (PSLNULL != ppvBuffer)
    {
        *ppvBuffer = PSLNULL;
    }
    if (PSLNULL != pcbBuffer)
    {
        *pcbBuffer = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpEvent) || (PSLNULL == ppvBuffer) ||
        (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags) && (0 == cbEvent)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine the root of the USB buffer
     */

    pmtpusbEvent = (PXMTPUSBPACKET_EVENT)
                            ((PXMTPUSBPACKET_HEADER)pmtpEvent - 1);

    /*  Validate length if specified
     */

    if ((0 < cbEvent) &&
            ((MTPEVENT_MIN_SIZE > cbEvent) ||
                (sizeof(*pmtpEvent) < cbEvent)))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }
    cbPacket = cbEvent + sizeof(MTPUSBPACKET_HEADER);

    /*  If the buffer has data update it
     */

    if (!(MTPUSBPARSERFLAGS_EMPTY & dwFlags))
    {
        /*  Update the information in the USB buffer with the data here
         */

        MTPStoreUInt32(&(pmtpusbEvent->header.cbPacket), cbPacket);
        MTPStoreUInt16(&(pmtpusbEvent->header.wContainerType),
                            MTPUSBCONTAINER_EVENT);
    }

    /*  Return the information about the buffer
     */

    *ppvBuffer = pmtpusbEvent;
    if ((PSLNULL != pcbBuffer) && (0 < cbEvent))
    {
        *pcbBuffer = cbPacket;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}

