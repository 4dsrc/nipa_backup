/*
 *  MTPUSBTransport.c
 *
 *  Contains definition of the APIs used to support MTPUSBDEVICEs to a
 *  specific MTP USB router or proxy.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPUSBTransportPrecomp.h"

/*  Local Defines
 */

/*  Local Types
 */

/*  Local Functions
 */

/*  Local Variables
 */

static PSLBOOL              _FMTPUSBInitialized = PSLFALSE;


/*
 *  MTPUSBTransportInitialize
 *
 *  Initializes the MTP/USB transport
 *
 *  Arguments:
 *      PSLUINT32       dwInitFlags         Initialization flags
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBTransportInitialize(PSLUINT32 dwInitFlags)
{
    PSLBOOL         fInitialized;
    PSLSTATUS       ps;

    /*  Make sure that we are supposed to initialize
     */

    if (!(MTPINITFLAG_USB_TRANSPORT & dwInitFlags))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Currently only the router is supported
     */

    if (MTPINITFLAG_INITIATOR & dwInitFlags)
    {
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;
    }

    /*  Reserve enough bytes in contexts to handle our transport needs
     */

    ps = MTPContextReserveTransportBytes(MTPUSBROUTER_TRANSPORT_SIZE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Attempt to mark the transport as initialized
     */

    fInitialized = PSLAtomicCompareExchange(&_FMTPUSBInitialized,
                            PSLTRUE, PSLFALSE);

    /*  If the transport was already initialized report an error- we only
     *  support a single initialization
     */

    ps = !fInitialized ? PSLSUCCESS : PSLERROR_INVALID_OPERATION;

Exit:
    return ps;
}


/*
 *  MTPUSBTransportUninitialize
 *
 *  Uninitializes the MTP/USB transport
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBTransportUninitialize()
{
    PSLBOOL         fInitialized;

    /*  Mark the transport as disabled
     */

    fInitialized = PSLAtomicCompareExchange(&_FMTPUSBInitialized,
                            PSLFALSE, PSLTRUE);
    return fInitialized ? PSLSUCCESS : PSLERROR_NOT_INITIALIZED;
}


/*
 *  MTPUSBBindInitiator
 *
 *  Binds a connection from an initiator to the MTP/USB Transport.
 *
 *  Arguments:
 *      MTPUSBDEVICE    mtpusbdInitiator    MTPUSBDevice representing initiator
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBBindInitiator(MTPUSBDEVICE mtpusbdInitiator)
{
    PSLHANDLE           hThreadRouter = PSLNULL;
    MTPUSBROUTERINIT    mtpusbri = { 0 };
    PSLSTATUS           ps;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpusbdInitiator)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure that the transport has been initialized
     */

    if (!_FMTPUSBInitialized)
    {
        ps = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    /*  Initialize the router initialization info
     */

    ps = PSLSignalOpen(PSLFALSE, PSLFALSE, PSLNULL, &(mtpusbri.hSignalComplete));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    mtpusbri.mtpusbdInitiator = mtpusbdInitiator;

    /*  Create a new instance of the MTP/USB router thread to handle this
     *  initiator
     */

    ps = PSLThreadCreate(MTPUSBRouterThread, &mtpusbri, PSLFALSE,
                        PSLTHREAD_DEFAULT_STACK_SIZE, &hThreadRouter);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Wait for the thread to complete initialization
     */

    ps = PSLSignalWait(mtpusbri.hSignalComplete, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the result from the initialization
     */

    ps = mtpusbri.psResult;

Exit:
    SAFE_PSLTHREADCLOSE(hThreadRouter);
    SAFE_PSLSIGNALCLOSE(mtpusbri.hSignalComplete);
    return ps;
}


/*
 *  MTPUSBBindResponder
 *
 *  Binds the MTP/USB transport to the specifed responder.
 *
 *  Arguments:
 *      MTPUSBDEVICE    mtpusbdInitiator   MTPUSBDevice representing initiator
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS MTPUSBBindResponder(PSL_CONST MTPUSBDEVICE* mtpusbdResponder)
{
    PSLSTATUS           ps;

    /*  Validate arguments
     */

    if (PSLNULL == mtpusbdResponder)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure that the transport has been initialized
     */

    if (!_FMTPUSBInitialized)
    {
        ps = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    /*  Not currently implemented
     */

    ps = PSLERROR_NOT_IMPLEMENTED;

Exit:
    return ps;
}


