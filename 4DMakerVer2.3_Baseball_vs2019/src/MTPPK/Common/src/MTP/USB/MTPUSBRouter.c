/*
 *  MTPUSBRouter.c
 *
 *  Contains definition of the MTP/USB Router
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPUSBTransportPrecomp.h"

/*  Local Defines
 */

#define MTPUSBCONTEXT_COOKIE            'msbR'

#define DEVICE_READY_TIMEOUT            5000

#define MTPUSBROUTER_POOL_SIZE          16

#define MTPCONTEXT_TO_PMTPUSBCONTEXT(_pctx) \
        *((MTPUSBCONTEXT**)((_pctx)->mtpTransportState.pbTransportData))

#define SAFE_MTPUSBROUTEDESTROY(_r) \
if (PSLNULL != (_r)) \
{ \
    _MTPUSBRouteDestroy(_r); \
    (_r) = PSLNULL; \
}

#define SAFE_MTPUSBDEVICERELEASEBUFFER(_pdev, _pv) \
PSLASSERT((PSLNULL == (_pv)) || (PSLNULL != (_pdev))); \
if ((PSLNULL != (_pdev)) && (PSLNULL != (_pv))) \
{ \
    MTPUSBDeviceReleaseBuffer(_pdev, _pv); \
    (_pv) = PSLNULL; \
}

/*  MTP/USB Router Data Phases and States
 *
 *  These phases and states are used to manage how the router deals with data
 *  packets
 */

enum
{
    MTPUSBDATAPHASE_UNKNOWN = 0,
    MTPUSBDATAPHASE_SEND,
    MTPUSBDATAPHASE_RECEIVE,
};

enum
{
    MTPUSBDATASTATE_PENDING = 0,
    MTPUSBDATASTATE_OUT_READY,
    MTPUSBDATASTATE_IN_READY,
    MTPUSBDATASTATE_IN_COMPLETE
};

/*  MTP/USB Router Messages
 */

#define MAKE_MTPUSBROUTER_MSGID(_id, _flags) \
    MAKE_PSL_MSGID(MTP_TRANSPORT_PRIVATE, (_id), (_flags))

enum
{
    MTPUSBROUTERMSG_RESET =     MAKE_MTPUSBROUTER_MSGID(1, PSLMSGID_PRIORITY),
};

/*  Local Types
 */

typedef struct _MTPUSBCONTEXT
{
#ifdef PSL_ASSERTS
    PSLUINT32               dwCookie;
#endif  /* PSL_ASSERTS */
    MTPCONTEXT*             pmtpctx;
    PSLMSGQUEUE             mqTransport;
    PSLMSGPOOL              mpTransport;
    MTPUSBDEVICE            mtpusbd;
    PSLHANDLE               hSignalDeviceReady;
    PSLUINT32               cbPacketData;
    PSLUINT32               cbMaxData;
    PSLUINT32               dwDataPhase;
    PSLUINT32               dwDataState;
    PSLUINT64               cbDataTotal;
    PSLUINT64               cbDataPending;
    PSLUINT64               cbDataCurrent;
    PSLPARAM                aCookiePendingRcv;
    PSLVOID*                pvBufferFirst;
    PSLUINT32               dwTransactionID;
    PSLUINT16               wOpCode;
} MTPUSBCONTEXT;

/*  Local Functions
 */

static PSLSTATUS _MTPUSBRouteDestroy(MTPCONTEXT* pmtpctx);

static PSLSTATUS _MTPUSBRequestReceiveData(MTPCONTEXT* pmtpctx);

static PSLSTATUS _MTPUSBRouteCmdRecieved(MTPCONTEXT* pmtpctx,
                            PSLPARAM aParam,
                            PSLVOID* pvBuffer,
                            PSLUINT64 cbBuffer);

static PSLSTATUS _MTPUSBRouteDataReceived(MTPCONTEXT* pmtpctx,
                            PSLPARAM aParam,
                            PSLVOID* pvBuffer,
                            PSLUINT64 cbBuffer,
                            PSLUINT32 aCookie);

static PSLSTATUS _MTPUSBRouteControlReceived(MTPCONTEXT* pmtpctx,
                            PSLPARAM aParam,
                            PSLVOID* pvBuffer,
                            PSLUINT64 cbBuffer);

static PSLSTATUS _MTPUSBRouteBufferReceived(MTPCONTEXT* pmtpctx,
                            PSLUINT32 dwBufferType,
                            PSLPARAM aParam,
                            PSLVOID* pvBuffer,
                            PSLUINT64 cbBuffer,
                            PSLPARAM aCookie);

static PSLSTATUS _MTPUSBRouteBufferSent(MTPCONTEXT* pmtpctx,
                            PSLUINT32 dwBufferType,
                            PSLPARAM aParam,
                            PSLVOID* pvBuffer,
                            PSLUINT64 cbBuffer,
                            PSLPARAM aCookie);

static PSLSTATUS _MTPUSBRouteBufferAvailable(MTPCONTEXT* pmtpctx,
                            PSLUINT32 dwBufferType,
                            PSLPARAM aParam,
                            PSLVOID* pvBuffer,
                            PSLUINT64 cbBuffer);

static PSLSTATUS _MTPUSBRouteControlBuffer(MTPCONTEXT* pmtpctx,
                            PSLPARAM aParam,
                            PSLVOID* pvBuffer,
                            PSLUINT64 cbBuffer);

static PSLSTATUS PSL_API _MTPUSBRouteClassRequest(PSLPARAM aParamClassReq,
                            PSLVOID* pvRequest,
                            PSLUINT64 cbRequest);

static PSLSTATUS _MTPUSBRouteDataSend(MTPCONTEXT* pmtpctx,
                            PSLUINT32 dwSequenceID,
                            PSL_CONST PSLVOID* pvData,
                            PSLUINT64 cbData,
                            PSLUINT32 dwDataInfo);

static PSLSTATUS _MTPUSBRouteBufferRequest(MTPCONTEXT* pmtpctx,
                            PSLMSGID msgID,
                            PSLUINT32 dwSequenceID,
                            PSLUINT64 cbRequested,
                            PSLUINT32 dwDataInfo);

static PSLSTATUS _MTPUSBRouteBufferRequestFlags(MTPCONTEXT* pmtpctx,
                            PSLUINT32 dwSequenceID,
                            PSLUINT64 cbRequest,
                            PSLUINT32 dwFlags);

static PSLSTATUS _MTPUSBRouteBufferConsumed(MTPCONTEXT* pmtpctx,
                            PSLMSGID msgID,
                            PSLVOID* pvBuffer);

static PSLSTATUS _MTPUSBRouteBufferRelease(PSL_CONST MTPUSBDEVICE mtpusbd,
                            PSL_CONST PSLMSG* pMsg);

static PSLSTATUS _MTPUSBRouteCreate(MTPUSBCONTEXT* pmtpusbctx,
                            MTPCONTEXT** ppmtpctx);

static PSLSTATUS _MTPUSBRouteTerminate(MTPCONTEXT* pmtpctx,
                            PSLMSGID msgID,
                            PSLPARAM aParam);

static PSLSTATUS _MTPUSBRouteReset(MTPCONTEXT** ppmtpctx);

static PSLSTATUS _MTPUSBRouterCleanQueueEnum(PSL_CONST PSLMSG* pMsg,
                            PSLPARAM aParam);

static PSLSTATUS _MTPUSBRouterResetQueueEnum(PSL_CONST PSLMSG* pMsg,
                            PSLPARAM aParam);

/*  Local Variables
 */

static PSL_CONST PSLUINT32  _RgdwRequestFlags[] =
{
    MTPMSGFLAG_EVENT_BUFFER_REQUEST,
    MTPMSGFLAG_DATA_BUFFER_REQUEST,
    MTPMSGFLAG_RESPONSE_BUFFER_REQUEST,
    MTPMSGFLAG_CMD_BUFFER_REQUEST
};


/*
 *  _MTPUSBRouteDestroy
 *
 *  Destroys the MTP USB route
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             Context to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteDestroy(MTPCONTEXT* pmtpctx)
{
    MTPUSBCONTEXT*  pmtpusbctx;
    PSLSTATUS       ps;

    //  Validate arguments

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    //  Extract the MTPUSBCONTEXT so that we can remove the back reference

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    if (PSLNULL != pmtpusbctx)
    {
        PSLAtomicExchangePointer(&(pmtpusbctx->pmtpctx), PSLNULL);
    }

    //  And perform the standard cleanup

    ps = MTPRouteDestroy(pmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  _MTPUSBRequestReceiveData
 *
 *  Requests a data buffer be received over the USB connection
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             Context to request data on
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRequestReceiveData(MTPCONTEXT* pmtpctx)
{
    PSLUINT32       cbActual;
    PSLUINT32       cbRequest;
    MTPUSBDEVICE    mtpusbd = PSLNULL;
    PSLSTATUS       ps;
    MTPUSBCONTEXT*  pmtpusbctx;
    PSLVOID*        pvBuffer = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Determine the size of buffer to request
     */

#ifdef MTPUSB_SPLIT_HEADER_MODE
    switch (pmtpusbctx->dwDataState)
    {
    case MTPUSBDATASTATE_PENDING:
        cbRequest = sizeof(MTPUSBPACKET_DATA_HEADER);
        break;

    case MTPUSBDATASTATE_IN_READY:
        cbRequest = (PSLUINT32)min((PSLUINT64)pmtpusbctx->cbMaxData,
                            pmtpusbctx->cbDataPending);
        break;

    case MTPUSBDATASTATE_OUT_READY:
    case MTPUSBDATASTATE_IN_COMPLETE:
        cbRequest = pmtpusbctx->cbMaxData;
        break;

    default:
        /*  Unexpected state
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }
#else   /* !MTPUSB_SPLIT_HEADER_MODE */
    cbRequest = pmtpusbctx->cbMaxData;
#endif  /* MTPUSB_SPLIT_HEADER_MODE */

    /*  Start by requesting a buffer
     */

    ps = MTPUSBDeviceRequestBuffer(mtpusbd, MTPBUFFER_RECEIVE_DATA,
                        cbRequest, 0, &pvBuffer, &cbActual);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Start the receive if we got back the buffer
     */

    if (PSLSUCCESS == ps)
    {
        /*  Forward the request to the handler
         */

        ps = _MTPUSBRouteBufferAvailable(pmtpctx, MTPBUFFER_RECEIVE_DATA,
                        0, pvBuffer, cbActual);
        pvBuffer = PSLNULL;
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

Exit:
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBuffer);
    return ps;
}


/*
 *  _MTPUSBRouteCmdReceived
 *
 *  Handles reception of a command buffer over the USB connection
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context for this operation
 *      PSLPARAM        aParam              Parameter associated with buffer
 *      PSLVOID*        pvBuffer            Buffer received
 *      PSLUINT64       cbBuffer            Length of buffer received
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteCmdRecieved(MTPCONTEXT* pmtpctx, PSLPARAM aParam,
                            PSLVOID* pvBuffer, PSLUINT64 cbBuffer)
{
    PSLUINT32       cbMTPBuffer;
    MTPUSBDEVICE    mtpusbd = PSLNULL;
    PSLSTATUS       ps;
    MTPUSBCONTEXT*  pmtpusbctx;
    PXMTPOPERATION  pmtpOp;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pvBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Make sure we are not currently handling a command and that
     *  the buffer is large enough to hold a command
     */

    PSLASSERT(PSLNULL == pmtpctx->mtpContextState.mqHandler);
    if ((PSLNULL != pmtpctx->mtpContextState.mqHandler) ||
        (sizeof(MTPUSBPACKET_OPERATION_REQUEST) < cbBuffer))
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Extract the command
     */

    ps = MTPUSBParserUSBBufferToMTPOperation(pvBuffer,
                        (PSLUINT32)cbBuffer, 0, &pmtpOp, &cbMTPBuffer);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Remember the current op code and transaction ID
     */

    pmtpusbctx->wOpCode = MTPLoadUInt16(&(pmtpOp->wOpCode));
    pmtpusbctx->dwTransactionID = MTPLoadUInt32(&(pmtpOp->dwTransactionID));
    pmtpusbctx->pvBufferFirst = PSLNULL;

    MTPPerfSetMTPContext(pmtpusbctx->dwTransactionID,
                         pmtpusbctx->wOpCode);

    /*  And begin the route
     */

    ps = MTPRouteBegin(pmtpctx, pmtpusbctx->mpTransport, pmtpOp,
                        cbMTPBuffer);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pvBuffer = PSLNULL;

    MTPPERFLOG_COMMANDPHASE(MP_TYPE_ELAPSETIME_BEGIN);

    /*  For optimum performance we always attempt to read data off
     *  the incoming pipe
     */

    ps = _MTPUSBRequestReceiveData(pmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBuffer);
    return ps;
    aParam;
}


/*
 *  _MTPUSBRouteDataReceived
 *
 *  Handles reception of a data buffer over the USB connection
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context for this operation
 *      PSLPARAM        aParam              Parameter associated with buffer
 *      PSLVOID*        pvBuffer            Buffer received
 *      PSLUINT64       cbBuffer            Length of buffer received
 *      PSLUINT32       aCookie             Cookie associated with this
 *                                            buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteDataReceived(MTPCONTEXT* pmtpctx, PSLPARAM aParam,
                            PSLVOID* pvBuffer, PSLUINT64 cbBuffer,
                            PSLUINT32 aCookie)
{
    PSLUINT32       cbData;
    PSLUINT32       cbMTPBuffer;
    PSLBOOL         fFirstPacket;
    MTPUSBDEVICE    mtpusbd = PSLNULL;
    PSLSTATUS       ps;
    PSLMSG*         pMsg = PSLNULL;
    MTPUSBCONTEXT*  pmtpusbctx;
    PSLVOID*        pvMTPBuffer;
    PSLBOOL         fLastPacket;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pvBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Validate that this is the packet we were expected
     */

    PSLASSERT(aCookie == pmtpusbctx->aCookiePendingRcv);
    if (aCookie != pmtpusbctx->aCookiePendingRcv)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }
    pmtpusbctx->aCookiePendingRcv = 0;

    /*  We have received data from the initiator- we should only get
     *  here if we have yet to determine the data phase or already know that
     *  we are receiving data
     */

    PSLASSERT((MTPUSBDATAPHASE_UNKNOWN == pmtpusbctx->dwDataPhase) ||
            (MTPUSBDATAPHASE_RECEIVE == pmtpusbctx->dwDataPhase));
    if (MTPUSBDATAPHASE_UNKNOWN == pmtpusbctx->dwDataPhase)
    {
        /*  Remember that we are receiving data
         */

        pmtpusbctx->dwDataPhase = MTPUSBDATAPHASE_RECEIVE;
    }
    if (MTPUSBDATAPHASE_RECEIVE != pmtpusbctx->dwDataPhase)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  The Windows 7 initiator may send a NULL packet when it determines that
     *  the end of a transfer occurred on a data packet boundary- to account
     *  for this we continue to listen for available data and ignore it as
     *  long as it is a NULL packet
     */

    if (MTPUSBDATASTATE_IN_COMPLETE == pmtpusbctx->dwDataState)
    {
        PSLTraceWarning("MTPUSBRouter: 0x%08x bytes of data received after "
                            "data phase", cbBuffer);
        ps = (0 == cbBuffer) ? PSLSUCCESS_FALSE : PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Determine the MTP buffer
     */

    fFirstPacket = !(MTPUSBDATASTATE_IN_READY == pmtpusbctx->dwDataState);
    ps = MTPUSBParserUSBBufferToMTPDataBuffer(pvBuffer, (PSLUINT32)cbBuffer,
                    fFirstPacket ? MTPUSBPARSERFLAGS_FIRST_PACKET : 0,
                    &pvMTPBuffer, &cbMTPBuffer);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  If this is the first packet we want to see if the size is
     *  available
     */

    if (fFirstPacket)
    {
        /*  Create a message so we can notify the handlers of the
         *  incoming data size
         */

        ps = PSLMsgAlloc(pmtpusbctx->mpTransport, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = MTPMSG_DATA_START;
        pMsg->aParam0 = (PSLPARAM)pmtpctx;

        /*  Extract the size of the data from the USB buffer
         */

        cbData = ((PXMTPUSBPACKET_DATA_HEADER)pvBuffer)->header.cbPacket;
        if (cbData < 0xffffffff)
        {
            /*  Track the total size (including USB header) in the
             *  transport
             */

            pmtpusbctx->cbDataTotal = cbData;

            /*  Report only the actual data size to the handler
             */

            pMsg->alParam = cbData - sizeof(MTPUSBPACKET_DATA_HEADER);
        }
        else
        {
            /*  Remember and report size as unknown
             */

            pmtpusbctx->cbDataTotal = MTP_DATA_SIZE_UNKNOWN;
            pMsg->alParam = MTP_DATA_SIZE_UNKNOWN;
        }
        pmtpusbctx->cbDataPending = pmtpusbctx->cbDataTotal;

        /* Log MTPPERF_DATAPHASE::TYPE_RECEIVEBEGIN.
         */
        MTPPERFLOG_DATAPHASE(MP_TYPE_RECEIVEDATA_BEGIN,
                             pmtpusbctx->cbDataPending);

        /*  Notify the handler of the upcoming data size
         */

        ps = MTPRouteMsgToHandler(pmtpctx, pMsg);
        if (PSLSUCCESS != ps)
        {
            if (PSLSUCCESS_NOT_AVAILABLE == ps)
            {
                /*  A currently active handler has not been defined- this is
                 *  an invalid packet- and remember that the message was
                 *  eaten by the router
                 */

                ps = PSLERROR_INVALID_PACKET;
                pMsg = PSLNULL;
            }
            goto Exit;
        }
        pMsg = PSLNULL;

        /*  Update the state to acknowledge that we have received the
         *  first packet
         */

        pmtpusbctx->dwDataState = MTPUSBDATASTATE_IN_READY;
    }

    /*  If we have received more data than we are expecting treat this
     *  packet as an error
     */

    if (cbBuffer > pmtpusbctx->cbDataPending)
    {
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;
    }

    /*  Update the data counters
     */

    pmtpusbctx->cbDataCurrent += cbBuffer;
    pmtpusbctx->cbDataPending -= cbBuffer;

#ifdef MTPUSB_SPLIT_HEADER_MODE
    /*  In split header mode if we received just a header than go back
     *  and wait for the first real data packet
     */

    if (fFirstPacket && (0 == cbMTPBuffer) && (0 < pmtpusbctx->cbDataPending))
    {
        /*  Request reception of the next buffer of data
         */

        ps = _MTPUSBRequestReceiveData(pmtpctx);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        goto Exit;
    }
    fFirstPacket = PSLFALSE;
#endif  /* MTPUSB_SPLIT_HEADER_MODE */

    /*  The last packet in the data phase must be a short packet or a NULL
     *  packet
     */

    fLastPacket = (0 == cbMTPBuffer) ||
                            (0 != (cbBuffer % pmtpusbctx->cbPacketData)) ||
                            (0 == pmtpusbctx->cbDataPending);
    if (fLastPacket)
    {
        /*  We have seen a terminating packet- make sure that we expect no
         *  more data
         */

        pmtpusbctx->cbDataPending = 0;
    }

    /*  Allocate a message to send the next data packet
     */

    ps = PSLMsgAlloc(pmtpusbctx->mpTransport, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    pMsg->msgID = MTPMSG_DATA_AVAILABLE;
    pMsg->aParam0 = (PSLPARAM)pmtpctx;
    pMsg->aParam2 = (0 != cbMTPBuffer) ? (PSLPARAM)pvMTPBuffer : PSLNULL;
    pMsg->aParam3 = fLastPacket ? MTPMSGFLAG_TERMINATE : 0;
    pMsg->alParam = cbMTPBuffer;

    ps = MTPRouteMsgToHandler(pmtpctx, pMsg);
    if (PSLSUCCESS != ps)
    {
        if (PSLSUCCESS_NOT_AVAILABLE == ps)
        {
            /*  We should have an active handler- mark this packet as invalid
             */

            ps = PSLERROR_INVALID_PACKET;
            pMsg = PSLNULL;
        }
        goto Exit;
    }
    pMsg = PSLNULL;

    /*  If this is the first data buffer we have received remember it so
     *  that we can clear it correctly
     */

    if (fFirstPacket && (PSLNULL == pmtpusbctx->pvBufferFirst))
    {
        PSLASSERT(0 != cbMTPBuffer);
        pmtpusbctx->pvBufferFirst = pvMTPBuffer;
    }

    /*  Release the buffer if necessary
     */

    if (0 == cbMTPBuffer)
    {
        MTPUSBDeviceReleaseBuffer(mtpusbd, pvBuffer);
        pmtpusbctx->pvBufferFirst = PSLNULL;
    }
    pvBuffer = PSLNULL;

    /*  If no more data is remaining then we are done
     */

    if (fLastPacket)
    {
        MTPPERFLOG_DATAPHASE(MP_TYPE_RECEIVEDATA_END, pmtpusbctx->cbDataCurrent);
        pmtpusbctx->dwDataState = MTPUSBDATASTATE_IN_COMPLETE;
    }

    /*  Request reception of the next buffer of data
     */

    ps = _MTPUSBRequestReceiveData(pmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBuffer);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
    aParam;
}


/*
 *  _MTPUSBRouteControlReceived
 *
 *  Handles a control buffer recieved message from the USB device
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context for this operation
 *      PSLPARAM        aParam              Parameter associated with buffer
 *      PSLVOID*        pvBuffer            Buffer received
 *      PSLUINT64       cbBuffer            Length of buffer received
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteControlReceived(MTPCONTEXT* pmtpctx, PSLPARAM aParam,
                            PSLVOID* pvBuffer, PSLUINT64 cbBuffer)
{
    PSLUINT32                       dwTransactionID;
    PSLBOOL                         fHandled = PSLFALSE;
    MTPUSBDEVICE                    mtpusbd = PSLNULL;
    PSLSTATUS                       ps;
    MTPUSBCONTEXT*                  pmtpusbctx;
    PXMTPUSB_CANCEL_REQUEST_DATA    pmcr;
    PXMTPUSB_DEVICE_REQUEST         pmdr = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pvBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Extract the control request that is passed in the parameter
     */

    pmdr = (PXMTPUSB_DEVICE_REQUEST)aParam;
    if (PSLNULL == pmdr)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Determine what type of control request we are servicing
     */

    switch (pmdr->bRequest)
    {
    case MTPUSB_CANCEL_REQUEST:
        /*  Make sure we received the correct packet
         */

        PSLASSERT(sizeof(MTPUSB_CANCEL_REQUEST_DATA) == cbBuffer);
        if (cbBuffer != sizeof(MTPUSB_CANCEL_REQUEST_DATA))
        {
            ps = PSLERROR_INVALID_PACKET;
            goto Exit;
        }

        /*  Unpack and validate the cancel request
         */

        pmcr = (PXMTPUSB_CANCEL_REQUEST_DATA)pvBuffer;
        PSLASSERT(MTP_EVENTCODE_CANCELTRANSACTION ==
                            MTPLoadUInt16(&(pmcr->wCancellationCode)));
        if (MTP_EVENTCODE_CANCELTRANSACTION !=
                            MTPLoadUInt16(&(pmcr->wCancellationCode)))
        {
            ps = PSLERROR_INVALID_PACKET;
            goto Exit;
        }

        /*  Extract the transaction ID
         */

        dwTransactionID = MTPLoadUInt32(&(pmcr->dwTransactionID));

        /*  If the transaction ID specified does not match our transaction
         *  ID then we are dealing with an asynchronous cancel which is
         *  not currently supported
         */

        if (dwTransactionID != pmtpusbctx->dwTransactionID)
        {
            PSLASSERT(PSLFALSE);
            ps = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }

        /*  It is possible that the current transaction has already
         *  completed- if it has do not worry about cancelling the
         *  current route
         */

        if (PSLNULL == pmtpctx->mtpContextState.mqHandler)
        {
            fHandled = PSLTRUE;
            break;
        }

        /*  Clean-up the current route:
         *  1. Terminate the current route with a cancel
         *  2. Remove any remaining messages from the queue
         */

        ps = _MTPUSBRouteTerminate(pmtpctx, MTPMSG_CANCEL, dwTransactionID);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        ps = PSLMsgEnum(pmtpusbctx->mqTransport, _MTPUSBRouterResetQueueEnum,
                        (PSLPARAM)pmtpctx);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Request a new command buffer to begin handling the next command
         */

        ps = MTPUSBDeviceRequestBuffer(pmtpusbctx->mtpusbd, MTPBUFFER_COMMAND,
                                sizeof(MTPUSBPACKET_OPERATION_REQUEST), 0,
                                PSLNULL, PSLNULL);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Report the control request as handled
         */

        fHandled = PSLTRUE;
        break;

    case MTPUSB_GET_EXTENDED_EVENT_DATA_REQUEST:
    case MTPUSB_DEVICE_STATUS_REQUEST:
    case MTPUSB_DEVICE_RESET_REQUEST:
        /*  None of these requests currently support additional buffers
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PACKET;
        goto Exit;

    default:
        /*  Unknown command request
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBuffer);
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pmdr);
    if (PSLNULL != mtpusbd)
    {
        /*  If we failed the operation we need to stall the control endpoint
         *  to report it back to the host
         */

        if (PSL_FAILED(ps))
        {
            MTPUSBDeviceStallPipe(mtpusbd, MTPUSBBUFFER_CONTROL);
            fHandled = PSLTRUE;
        }

        /*  If we handled the control request then send status
         */

        if (fHandled)
        {
            MTPUSBDeviceSendControlHandshake(mtpusbd);
        }
    }
    return ps;
}



/*
 *  _MTPUSBRouteBufferReceived
 *
 *  Handles a buffer recieved message from the USB device
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context for this operation
 *      PSLUINT32       dwBufferType        Type of buffer received
 *      PSLPARAM        aParam              Parameter associated with buffer
 *      PSLVOID*        pvBuffer            Buffer received
 *      PSLUINT64       cbBuffer            Length of buffer received
 *      PSLPARAM        aCookie             Cookie associated with receive
 *                                            request
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteBufferReceived(MTPCONTEXT* pmtpctx,
                            PSLUINT32 dwBufferType, PSLPARAM aParam,
                            PSLVOID* pvBuffer, PSLUINT64 cbBuffer,
                            PSLPARAM aCookie)
{
    MTPUSBDEVICE        mtpusbd = PSLNULL;
    PSLSTATUS           ps;
    MTPUSBCONTEXT*      pmtpusbctx;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pvBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Determine how to handle the buffer
     */

    switch (dwBufferType)
    {
    case MTPBUFFER_COMMAND:
        /*  Handle the command directly
         */

        ps = _MTPUSBRouteCmdRecieved(pmtpctx, aParam, pvBuffer, cbBuffer);
        pvBuffer = PSLNULL;
        if (PSL_FAILED(ps))
        {
            /*  Stall both of the bulk pipes to report a problem
             */

            MTPUSBDeviceStallPipe(mtpusbd, MTPBUFFER_TRANSMIT_DATA);
            MTPUSBDeviceStallPipe(mtpusbd, MTPBUFFER_RECEIVE_DATA);

            /*  And request a new command buffer
             */

            ps = MTPUSBDeviceRequestBuffer(pmtpusbctx->mtpusbd,
                                    MTPBUFFER_COMMAND,
                                    sizeof(MTPUSBPACKET_OPERATION_REQUEST), 0,
                                    PSLNULL, PSLNULL);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        break;

    case MTPBUFFER_TRANSMIT_DATA:
        /*  Currently not implemented
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;

    case MTPBUFFER_RECEIVE_DATA:
        /*  Handle the command directly
         */

        ps = _MTPUSBRouteDataReceived(pmtpctx, aParam, pvBuffer, cbBuffer,
                            aCookie);
        pvBuffer = PSLNULL;
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPUSBBUFFER_CONTROL:
        /*  Handle the control buffer we received
         */

        ps = _MTPUSBRouteControlReceived(pmtpctx, aParam, pvBuffer, cbBuffer);
        pvBuffer = PSLNULL;
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPBUFFER_RESPONSE:
    case MTPBUFFER_EVENT:
    default:
        /*  We should never recieve a response or an event buffer-
         *  ignore them
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBuffer);
    return ps;
}


/*
 *  _MTPUSBRouteBufferSent
 *
 *  Forwards a buffer sent message received from the USB device to the
 *  handler
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context for this operation
 *      PSLUINT32       dwBufferType        Buffer type
 *      PSLPARAM        aParam              Parameter associated with buffer
 *      PSLVOID*        pvBuffer            Buffer that was sent
 *      PSLUINT64       cbBuffer            Number of bytes sent
 *      PSLPARAM        aCookie             Cookie associated with this
 *                                            operation
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if message forwarded
 *
 */

PSLSTATUS _MTPUSBRouteBufferSent(MTPCONTEXT* pmtpctx,
                            PSLUINT32 dwBufferType, PSLPARAM aParam,
                            PSLVOID* pvBuffer, PSLUINT64 cbBuffer,
                            PSLPARAM aCookie)
{
    MTPROUTEUPDATE  mtpRU;
    MTPUSBDEVICE    mtpusbd = PSLNULL;
    PSLSTATUS       ps;
    PSLMSG*         pMsg = PSLNULL;
    MTPUSBCONTEXT*  pmtpusbctx;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pvBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Determine what to do based on the buffer type
     */

    switch (dwBufferType)
    {
    case MTPBUFFER_TRANSMIT_DATA:
    case MTPBUFFER_EVENT:
        /*  Notify the handler of the data being sent
         *
         *  NOTE:
         *  We want pvBuffer to be released so do not set it to PSLNULL
         */

        ps = PSLMsgAlloc(pmtpusbctx->mpTransport, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg->msgID = (MTPBUFFER_TRANSMIT_DATA == dwBufferType) ?
                    MTPMSG_DATA_SENT : MTPMSG_EVENT_SENT;
        pMsg->aParam0 = (PSLPARAM)pmtpctx;
        pMsg->aParam1 = aParam;
        pMsg->alParam = cbBuffer;
        ps = MTPRouteMsgToHandler(pmtpctx, pMsg);
        if (PSLSUCCESS != ps)
        {
            if (PSLSUCCESS_NOT_AVAILABLE == ps)
            {
                /* router couldn't find a destination queue
                 * to post the message
                 */
                ps = PSLERROR_NOT_AVAILABLE;
                pMsg = PSLNULL;
            }
            goto Exit;
        }
        pMsg = PSLNULL;
        break;

    case MTPUSBBUFFER_TRANSMIT_SHORT_DATA:
        /*  No work to do here- we use the short data packets internally
         *  to the router so we just want to let it get released
         */

        break;

    case MTPBUFFER_RESPONSE:
        /*  End the current route- the lookup table is available in aParam
         */

        MTPPERFLOG_RESPONSEPHASE(MP_TYPE_ELAPSETIME_END,
            MTPLoadUInt16(&(((PXMTPUSBPACKET_OPERATION_RESPONSE)pvBuffer)->
            mtpResponse.wRespCode)));

        PSLZeroMemory(&mtpRU, sizeof(mtpRU));
        mtpRU.hHandlerTableNew = (PSLLOOKUPTABLE)aParam;
        ps = MTPRouteEnd(pmtpctx, &mtpRU);
        PSLASSERT(PSL_SUCCEEDED(ps));

        PSLTraceDetail("_MTPUSBRouteBufferSent: Route reset");

        /*  Request a new command buffer
         */

        MTPPerfSetMTPContext(0, 0);

        ps = MTPUSBDeviceRequestBuffer(mtpusbd, MTPBUFFER_COMMAND,
                        sizeof(MTPUSBPACKET_OPERATION_REQUEST), 0,
                        PSLNULL, PSLNULL);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPUSBBUFFER_CONTROL:
        /*  Consume both the device request in aParam and the buffer that
         *  was sent
         */

        SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBuffer);
        pvBuffer = (PSLVOID*)aParam;

        /*  Send the control status notification per USB spec
         */

        ps = MTPUSBDeviceSendControlHandshake(mtpusbd);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPBUFFER_COMMAND:
    default:
        /*  We should never receive a sent notification for a command
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBuffer);
    SAFE_PSLMSGFREE(pMsg);
    return ps;
    aCookie;
}


/*
 *  _MTPUSBRouteBufferAvailable
 *
 *  Forwards a buffer available message from the USB device to the handler
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context for this operation
 *      PSLUINT32       dwBufferType        Buffer type
 *      PSLPARAM        aParam              Parameter associated with buffer
 *      PSLVOID*        pvBuffer            Buffer that is available
 *      PSLUINT64       cbBuffer            Size of the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteBufferAvailable(MTPCONTEXT* pmtpctx,
                            PSLUINT32 dwBufferType, PSLPARAM aParam,
                            PSLVOID* pvUSBBuffer, PSLUINT64 cbUSBBuffer)
{
    PSLPARAM        aCookie;
    PSLUINT32       cbBuffer;
    PSLBOOL         fFirstPacket = PSLFALSE;
    PSLMSGID        msgID;
    MTPUSBDEVICE    mtpusbd = PSLNULL;
    PSLSTATUS       ps;
    PSLMSG*         pMsg = PSLNULL;
    PSLVOID*        pvBuffer;
    MTPUSBCONTEXT*  pmtpusbctx;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pvUSBBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get at the USB context information
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Determine the correct message to send
     */

    switch (dwBufferType)
    {
    case MTPBUFFER_COMMAND:
        /*  Make sure our states are correct
         */

        PSLASSERT(PSLNULL == pmtpctx->mtpContextState.mqHandler);
        pmtpusbctx->dwDataPhase = MTPUSBDATAPHASE_UNKNOWN;
        pmtpusbctx->dwDataState = MTPUSBDATASTATE_PENDING;
        pmtpusbctx->cbDataTotal = MTP_DATA_SIZE_UNKNOWN;
        pmtpusbctx->cbDataCurrent = 0;

        /*  We have recieved a new command buffer- request that it be filled
         */

        ps = MTPUSBDeviceReceiveBuffer(mtpusbd, pvUSBBuffer, 0, &aCookie);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        msgID = MTPMSG_UNKNOWN;
        pvBuffer = PSLNULL;
        cbBuffer = 0;
        pvUSBBuffer = PSLNULL;
        break;

    case MTPBUFFER_TRANSMIT_DATA:
        /*  Determine the actual size of the buffer if we need to reserve space
         *  for the USB header
         */

#ifndef MTPUSB_SPLIT_HEADER_MODE
        fFirstPacket = (pmtpusbctx->cbDataPending == pmtpusbctx->cbDataTotal);
#endif  /* MTPUSB_SPLIT_HEADER_MODE */
        ps = MTPUSBParserUSBBufferToMTPDataBuffer(pvUSBBuffer,
                    (PSLUINT32)cbUSBBuffer,
                    (fFirstPacket ? MTPUSBPARSERFLAGS_FIRST_PACKET : 0) |
                        MTPUSBPARSERFLAGS_EMPTY,
                    &pvBuffer, &cbBuffer);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        msgID = MTPMSG_DATA_BUFFER_AVAILABLE;
        break;

    case MTPBUFFER_RECEIVE_DATA:
        /*  We have a buffer to receive data into- request the buffer
         *  be filled
         */

        PSLASSERT(0 == pmtpusbctx->aCookiePendingRcv);
        ps = MTPUSBDeviceReceiveBuffer(mtpusbd, pvUSBBuffer, 0,
                            &pmtpusbctx->aCookiePendingRcv);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        msgID = MTPMSG_UNKNOWN;
        pvBuffer = PSLNULL;
        cbBuffer = 0;
        pvUSBBuffer = PSLNULL;
        break;

    case MTPBUFFER_RESPONSE:
        /*  Map the USB buffer to a response buffer
         */

        ps = MTPUSBParserUSBBufferToMTPResponse(pvUSBBuffer,
                        (PSLUINT32)cbUSBBuffer, MTPUSBPARSERFLAGS_EMPTY,
                        (PXMTPRESPONSE*)&pvBuffer, &cbBuffer);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        msgID = MTPMSG_RESPONSE_BUFFER_AVAILABLE;
        break;

    case MTPBUFFER_EVENT:
        /*  Map the USB buffer to an event buffer
         */

        ps = MTPUSBParserUSBBufferToMTPEvent((PSLVOID*)pvUSBBuffer,
                        (PSLUINT32)cbUSBBuffer, MTPUSBPARSERFLAGS_EMPTY,
                        (PXMTPEVENT*)&pvBuffer, &cbBuffer);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        msgID = MTPMSG_EVENT_BUFFER_AVAILABLE;
        break;

    case MTPUSBBUFFER_CONTROL:
        /*  Route the control buffer
         */

        ps = _MTPUSBRouteControlBuffer(pmtpctx, aParam, pvUSBBuffer,
                            cbUSBBuffer);
        pvUSBBuffer = PSLNULL;
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        msgID = MTPMSG_UNKNOWN;
        pvBuffer = PSLNULL;
        cbBuffer = 0;
        break;

    default:
        /*  Should never get here
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  If we do not have a message to send we are done
     */

    if (MTPMSG_UNKNOWN == msgID)
    {
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Allocate a message to forward the buffer with
     */

    ps = PSLMsgAlloc(pmtpusbctx->mpTransport, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    pMsg->msgID = msgID;
    pMsg->aParam0 = (PSLPARAM)pmtpctx;
    pMsg->aParam1 = aParam;
    pMsg->aParam2 = (PSLPARAM)pvBuffer;
    pMsg->aParam3 = 0;
    pMsg->alParam = cbBuffer;
    ps = MTPRouteMsgToHandler(pmtpctx, pMsg);
    if (PSLSUCCESS != ps)
    {
        if (PSLSUCCESS_NOT_AVAILABLE == ps)
        {
            /* router couldn't find a destination queue
             * to post the message
             */
            ps = PSLERROR_NOT_AVAILABLE;
            pMsg = PSLNULL;
        }
        goto Exit;
    }
    pMsg = PSLNULL;
    pvUSBBuffer = PSLNULL;

    /*  Remember that this was the first buffer we forwarded if necessary
     */

    if (fFirstPacket)
    {
        pmtpusbctx->pvBufferFirst = pvBuffer;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvUSBBuffer);
    return ps;
}


/*
 *  _MTPUSBRouteControlBuffer
 *
 *  Handles reception of a USB control buffer available
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context for this operation
 *      PSLPARAM        aParam              Parameter associated with buffer
 *      PSLVOID*        pvBuffer            Buffer that is available
 *      PSLUINT64       cbBuffer            Size of the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteControlBuffer(MTPCONTEXT* pmtpctx, PSLPARAM aParam,
                            PSLVOID* pvBuffer, PSLUINT64 cbBuffer)
{
    PSLPARAM                        aCookie;
    PSLUINT32                       dwResult;
    PSLBOOL                         fHandled = PSLFALSE;
    MTPUSBDEVICE                    mtpusbd = PSLNULL;
    PSLSTATUS                       ps;
    PXMTPUSB_DEVICE_STATUS_HEADER   pdsd;
    MTPUSBCONTEXT*                  pmtpusbctx;
    PXMTPUSB_DEVICE_REQUEST         pmdr = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pvBuffer))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get at the USB context information
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Extract the original device request and the buffer from the message
     *  so that we can free it if necessary
     */

    pmdr = (PXMTPUSB_DEVICE_REQUEST)aParam;

    /*  This message should be a class request- it was previously validated
     *  when the buffer was requested
     */

    PSLASSERT(MTPUSBDEVICEREQ_CHECK_FLAGS(pmdr,
                        MTPUSBDEVICEREQ_CLASS | MTPUSBDEVICEREQ_FOR_INTERFACE));

    /*  Determine how to handle the request
     */

    switch (pmdr->bRequest)
    {
    case MTPUSB_CANCEL_REQUEST:
        /*  Make sure the buffer is large enough to retrieve the request
         */

        if (sizeof(MTPUSB_CANCEL_REQUEST_DATA) > cbBuffer)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  And request the buffer be received
         */

        ps = MTPUSBDeviceReceiveBuffer(mtpusbd, pvBuffer, (PSLPARAM)pmdr,
                            &aCookie);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pmdr = PSLNULL;
        pvBuffer = PSLNULL;

        /*  Provide some information
         */

        PSLTraceProgress("_MTPUSBRouteControlBuffer: Waiting to receive cancel");
        break;

    case MTPUSB_GET_EXTENDED_EVENT_DATA_REQUEST:
        /*  Not currently supported
         */

        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;

    case MTPUSB_DEVICE_STATUS_REQUEST:
        /*  Make sure there is enough space to hold the status header
         */

        if (sizeof(MTPUSB_DEVICE_STATUS_HEADER) > cbBuffer)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  Fill out the buffer
         */

        pdsd = (PXMTPUSB_DEVICE_STATUS_HEADER)pvBuffer;
        MTPStoreUInt16(&(pdsd->wLength), sizeof(MTPUSB_DEVICE_STATUS_HEADER));
        dwResult = (PSLNULL == pmtpctx->mtpContextState.mqHandler) ?
                            MTP_RESPONSECODE_OK : MTP_RESPONSECODE_DEVICEBUSY;
        MTPStoreUInt16(&(pdsd->wStatusCode), (PSLUINT16)dwResult);

        /*  And transmit it
         */

        ps = MTPUSBDeviceSendBuffer(mtpusbd, MTPMSGFLAG_TERMINATE, pvBuffer,
                            sizeof(MTPUSB_DEVICE_STATUS_HEADER), 0, &aCookie);
        PSLTraceProgress("_MTPUSBRouteControlBuffer: Device status 0x%04x sent"
                            " (ps = %d)", dwResult, ps);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pvBuffer = PSLNULL;
        break;

    case MTPUSB_DEVICE_RESET_REQUEST:
    default:
        /*  Undefined request
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Release the message
     */

    PSLASSERT(PSL_SUCCEEDED(ps));

Exit:
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pmdr);
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBuffer);

    /*  If we did not handle the buffer close the control transaction
     */

    if (PSLNULL != mtpusbd)
    {
        /*  Stall the pipe if an error occurred
         */

        if (PSL_FAILED(ps))
        {
            MTPUSBDeviceStallPipe(mtpusbd, MTPUSBBUFFER_CONTROL);
            fHandled = PSLTRUE;
        }

        /*  And send the status handshake
         */

        if (fHandled)
        {
            MTPUSBDeviceSendControlHandshake(mtpusbd);
        }
    }
    return ps;
}


/*
 *  _MTPUSBRouteClassRequest
 *
 *  Handles notification of a USB class request from the USB device
 *
 *  Arguments:
 *      PSLPARAM        aParamClassReq      Class request paramter registered
 *                                            during device bind
 *      PSLVOID*        pvRequest           Request that is available
 *      PSLUINT64       cbRequest           Size of the buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteClassRequest(PSLPARAM aParamClassReq, PSLVOID* pvRequest,
                            PSLUINT64 cbRequest)
{
    PSLUINT32                   cbActual;
    PSLUINT32                   cbDesired;
    PSLBOOL                     fHandled = PSLFALSE;
    MTPUSBDEVICE                mtpusbd = PSLNULL;
    PSLSTATUS                   ps;
    PSLMSG*                     pMsg = PSLNULL;
    MTPCONTEXT*                 pmtpctx;
    MTPUSBCONTEXT*              pmtpusbctx;
    PSLVOID*                    pvBuffer = PSLNULL;
    PCXMTPUSB_DEVICE_REQUEST    pmdr;

    /*  Validate arguments
     */

    if ((PSLNULL == aParamClassReq) || (PSLNULL == pvRequest))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get at the MTP context information
     */

    pmtpusbctx = (MTPUSBCONTEXT*)aParamClassReq;
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);

    /*  We do not want to respond to any device class requests while we
     *  are not sure that the device is ready- as long as the device
     *  is ready the state of the signal will be good such that this
     *  does not block
     */

    ps = PSLSignalWait(pmtpusbctx->hSignalDeviceReady, DEVICE_READY_TIMEOUT);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  If we timed out then reject the control request
     */

    if (PSLSUCCESS != ps)
    {
        ps = PSLERROR_IN_USE;
        goto Exit;
    }

    /*  Get the MTP Context information now that the device is ready
     */

    pmtpctx = PSLAtomicCompareExchangePointer(&(pmtpusbctx->pmtpctx),
                            PSLNULL, PSLNULL);
    PSLASSERT((PSLNULL != pmtpctx) &&
        (MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx) == pmtpusbctx));
    if ((PSLNULL == pmtpctx) ||
        (MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx) != pmtpusbctx))
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Validate the length of the device request and extract it
     */

    if (sizeof(*pmdr) != cbRequest)
    {
        ps = PSLERROR_INVALID_DATA;
        goto Exit;
    }
    pmdr = (PCXMTPUSB_DEVICE_REQUEST)pvRequest;

    /*  Make sure that this is a request meant for us
     */

    if (!MTPUSBDEVICEREQ_CHECK_FLAGS(pmdr,
                        MTPUSBDEVICEREQ_CLASS | MTPUSBDEVICEREQ_FOR_INTERFACE))
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Determine how to handle the request
     */

    PSLTraceProgress("_MTPUSBRouteClassRequest: Request 0x%02x received",
                            pmdr->bRequest);
    switch (pmdr->bRequest)
    {
    case MTPUSB_CANCEL_REQUEST:
        /*  Request a control buffer to hold the cancel request
         */

        PSLASSERT(sizeof(MTPUSB_CANCEL_REQUEST_DATA) <=
                            MTPLoadUInt16(&(pmdr->wLength)));
        if (sizeof(MTPUSB_CANCEL_REQUEST_DATA) > MTPLoadUInt16(&pmdr->wLength))
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }
        cbDesired = sizeof(MTPUSB_CANCEL_REQUEST_DATA);
        break;

    case MTPUSB_GET_EXTENDED_EVENT_DATA_REQUEST:
        /*  Not currently supported
         */

        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;

    case MTPUSB_DEVICE_STATUS_REQUEST:
        /*  Request a control buffer to send the status response
         */

        PSLASSERT(sizeof(MTPUSB_DEVICE_STATUS_HEADER) <=
                            MTPLoadUInt16(&(pmdr->wLength)));
        if (sizeof(MTPUSB_DEVICE_STATUS_HEADER) > MTPLoadUInt16(&pmdr->wLength))
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }
        cbDesired = sizeof(MTPUSB_DEVICE_STATUS_HEADER);
        break;

    case MTPUSB_DEVICE_RESET_REQUEST:
        /*  Allocate a message to send to the main router to cause the reset
         *  to occur
         */

        ps = PSLMsgAlloc(pmtpusbctx->mpTransport, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg->msgID = MTPUSBROUTERMSG_RESET;
        pMsg->aParam0 = (PSLPARAM)pmtpctx;
        pMsg->aParam1 = PSLTRUE;
        ps = PSLMsgPost(pmtpusbctx->mqTransport, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = PSLNULL;

        /*  Clear the device ready flag so that we do not respond to the
         *  status request until the command completes
         */

        PSLSignalReset(pmtpusbctx->hSignalDeviceReady);

        /*  No more work to do here to handle the request- go ahead and exit
         *  immediately making sure that the fHandled flag is not set since
         *  we need to wait for the reset to occur before we report it as
         *  done.
         */

        PSLASSERT(!fHandled);
        goto Exit;

    default:
        /*  Undefined request
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  We need to request a control buffer to either send or receive
     *  the data
     */

    ps = MTPUSBDeviceRequestBuffer(mtpusbd, MTPUSBBUFFER_CONTROL,
                        cbDesired, (PSLPARAM)pvRequest, &pvBuffer, &cbActual);
    if (PSL_FAILED(ps) || (cbActual < cbDesired))
    {
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  If the buffer is ever unavailable fail the request due to control pipe
     *  timing issues- we do not want this to be an asyncrhonous buffer
     *  fulfillment
     */

    PSLASSERT(PSLSUCCESS == ps);
    if (PSLSUCCESS != ps)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Handle the buffer
     */

    ps = _MTPUSBRouteControlBuffer(pmtpctx, (PSLPARAM)pvRequest,
                        pvBuffer, cbActual);
    pvBuffer = PSLNULL;
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Device request is owned by buffer request now
     */

    pvRequest = PSLNULL;

Exit:
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBuffer);
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvRequest);
    SAFE_PSLMSGFREE(pMsg);

    /*  Send the status handshake if we have handled the entire message
     */

    if ((PSLNULL != mtpusbd) && fHandled)
    {
        /*  We should not send the handshake if we have an error- that will
         *  be handled by the USB driver
         */

        PSLASSERT(PSL_SUCCEEDED(ps));
        MTPUSBDeviceSendControlHandshake(mtpusbd);
    }

    /*  Provide some feedback
     */

    PSLTraceProgress("_MTPUSBRouteClassRequest: Class request %s (ps = %d)",
                            fHandled ? "handled" : "not handled", ps);
    return ps;
}


/*
 *  _MTPUSBRouteDataSend
 *
 *  Sends data from the handler over the transport
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to operate within
 *      PSLUINT32       dwSequenceID        Sequence ID for this operation
 *      PSL_CONST PSLVOID*
 *                      pvData              Data to send
 *      PSLUINT64       cbData              Size of data to send
 *      PSLUINT32       dwDataInfo          MTP data info flags
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteDataSend(MTPCONTEXT* pmtpctx, PSLUINT32 dwSequenceID,
                            PSL_CONST PSLVOID* pvData, PSLUINT64 cbData,
                            PSLUINT32 dwDataInfo)
{
    PSLPARAM                    aCookie;
    PSLUINT32                   cbBuffer;
    PSLBOOL                     fFirstPacket;
    PSLBOOL                     fSendHeader;
    MTPUSBDEVICE                mtpusbd = PSLNULL;
    PSLMSG*                     pMsg = PSLNULL;
    PSLSTATUS                   ps;
    MTPUSBCONTEXT*              pmtpusbctx;
    PXMTPUSBPACKET_DATA_HEADER  pmtpusbData;
    PSLVOID*                    pvBuffer = PSLNULL;

#ifdef MTPUSB_SPLIT_HEADER_MODE
    PSLUINT32                   cbBufferShort;
    PSLVOID*                    pvBufferShort = PSLNULL;
#endif  /* MTPUSB_SPLIT_HEADER_MODE */

    /*  Validate Arguments- we only support 32-bit data sends through this
     *  interface
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pvData) ||
        ((0 == cbData) && !(MTPMSGFLAG_TERMINATE & dwDataInfo)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we can send this
     */

    if ((0xffffffff & cbData) != cbData)
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  Extract the MTP USB transport information from the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Determine whether or not this is the first packet
     */

#ifdef MTPUSB_SPLIT_HEADER_MODE
    /*  In split header mode the first packet is like all of the other
     *  packets- which means that pvBufferFirst is not set.  Therefore
     *  we detect that we need to send the header based on the fact that
     *  we have not yet sent any data
     */

    fFirstPacket = PSLFALSE;
    fSendHeader = (pmtpusbctx->cbDataPending == pmtpusbctx->cbDataTotal);
#else   /* !MTPUSB_SPLIT_HEADER_MODE */
    /*  Check to see if this is the first buffer that we forwarded on to
     *  the handler.  Remember that we also need to send the header if this
     *  is the first packet.
     */

    fFirstPacket = (pvData == pmtpusbctx->pvBufferFirst);
    fSendHeader = fFirstPacket;
#endif  /* MTPUSB_SPLIT_HEADER_MODE */

    /*  Extract a USB buffer from the data
     */

    ps = MTPUSBParserMTPDataBufferToUSBBuffer(
                            (PSLVOID*)pvData, (PSLUINT32)cbData,
                            (fFirstPacket ? MTPUSBPARSERFLAGS_FIRST_PACKET : 0),
                            &pvBuffer, &cbBuffer);
    PSLASSERT(PSL_SUCCEEDED(ps));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Validate that this buffer size is legal
     */

    PSLASSERT((pmtpusbctx->cbDataPending - cbBuffer) <=
                            pmtpusbctx->cbDataPending);
    if ((pmtpusbctx->cbDataPending - cbBuffer) > pmtpusbctx->cbDataPending)
    {
        ps = PSLERROR_INVALID_BUFFER;
        goto Exit;
    }

    /*  If this is the first data packet we need to make sure that the
     *  container size specified is for the entire data phase- not just the
     *  size of the current packet.  Also we need to add in the op code
     *  and transaction ID that we have stashed in the context
     */

    if (fSendHeader)
    {
        /*  Make sure state is correct
         */

        PSLASSERT(0 == pmtpusbctx->cbDataCurrent);

#ifdef MTPUSB_SPLIT_HEADER_MODE
        /*  Retrieve a short transmit buffer to send the header in
         */

        ps = MTPUSBDeviceRequestBuffer(mtpusbd,
                            MTPUSBBUFFER_TRANSMIT_SHORT_DATA,
                            sizeof(MTPUSBPACKET_DATA_HEADER), 0,
                            &pvBufferShort, &cbBufferShort);
        PSLASSERT(PSLSUCCESS == ps);
        if (PSLSUCCESS != ps)
        {
            ps = PSL_FAILED(ps) ? ps : PSLERROR_UNEXPECTED;
            goto Exit;
        }

        pmtpusbData = (PXMTPUSBPACKET_DATA_HEADER)pvBufferShort;
#else   /* !MTPUSB_SPLIT_HEADER_MODE
        /*  Save some redirection
         */

        pmtpusbData = (PXMTPUSBPACKET_DATA_HEADER)pvBuffer;
#endif  /* MTPUSB_SPLIT_HEADER_MODE */

        /*  If we now have a valid size and this is a termination packet
         *  go ahead and set the real data length instead of sending an
         *  "unknown length" packet
         */

        if ((MTP_DATA_SIZE_UNKNOWN == pmtpusbctx->cbDataTotal) &&
            (MTPMSGFLAG_TERMINATE & dwDataInfo))
        {
            pmtpusbctx->cbDataTotal = cbBuffer;
#ifdef MTPUSB_SPLIT_HEADER_MODE
            pmtpusbctx->cbDataTotal += cbBufferShort;
#endif  /* MTPUSB_SPLIT_HEADER_MODE */
            pmtpusbctx->cbDataPending = pmtpusbctx->cbDataTotal;
        }

        /*  Set the size to 0xffffffff if the total size of the object will
         *  be larger than 4 GB- otherwise use the size we have plus the
         *  number of bytes in the header
         */

        MTPStoreUInt32(&(pmtpusbData->header.cbPacket),
                (PSLUINT32)min((PSLUINT64)0xffffffff, pmtpusbctx->cbDataTotal));
        MTPStoreUInt16(&(pmtpusbData->header.wContainerType),
                            MTPUSBCONTAINER_DATA);
        MTPStoreUInt16(&(pmtpusbData->wOpCode), pmtpusbctx->wOpCode);
        MTPStoreUInt32(&(pmtpusbData->dwTransactionID),
                            pmtpusbctx->dwTransactionID);

#ifdef MTPUSB_SPLIT_HEADER_MODE
        /*  Send the short header
         */

        ps = MTPUSBDeviceSendBuffer(mtpusbd, MTPMSGFLAG_TERMINATE,
                                    pvBufferShort, cbBufferShort,
                                    0, &aCookie);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pmtpusbctx->cbDataPending -= cbBufferShort;
        pvBufferShort = PSLNULL;
#endif  /* MTPUSB_SPLIT_HEADER_MODE */
    }

    /*  Account for this data being sent- note that if the terminate flag
     *  is set we want to force the remaining buffer to zero
     */

    if (!(MTPMSGFLAG_TERMINATE & dwDataInfo))
    {
        pmtpusbctx->cbDataPending -= cbBuffer;
    }
    else
    {
        pmtpusbctx->cbDataPending = 0;
    }

#ifdef MTPUSB_SPLIT_HEADER_MODE
    /*  If the data buffer is empty and we sent the header do not attempt to
     *  send the rest of the buffer
     */

    if (fSendHeader && (0 == cbBuffer))
    {
        /*  The terminate flag must be set
         */

        PSLASSERT(MTPMSGFLAG_TERMINATE & dwDataInfo);

        /*  Go ahead and report the buffer as sent
         */

        ps = _MTPUSBRouteBufferSent(pmtpctx, MTPBUFFER_TRANSMIT_DATA,
                            dwSequenceID, pvBuffer, cbBuffer, 0);
        if (PSL_SUCCEEDED(ps))
        {
            pvBuffer = PSLNULL;
        }
        goto Exit;
    }
#endif  /* MTPUSB_SPLIT_HEADER_MODE */

    /*  And send it
     */

    ps = MTPUSBDeviceSendBuffer(mtpusbd, (MTPMSGFLAG_TERMINATE & dwDataInfo),
                                pvBuffer, cbBuffer,
                                dwSequenceID, &aCookie);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pvBuffer = PSLNULL;

    /*  Account for the data that has been sent
     */

    pmtpusbctx->cbDataCurrent += cbBuffer;

    /*  If we have sent all of the data check to see if we need to
     *  follow-up with a short packet
     */

    if (0 == pmtpusbctx->cbDataPending)
    {
        /* No more data to send, log MTPPERF_DATAPHASE::TYPE_SENDEND
         */
		MTPPERFLOG_DATAPHASE(MP_TYPE_SENDDATA_END, pmtpusbctx->cbDataTotal);
    }

Exit:
    SAFE_PSLMSGFREE(pMsg);
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBuffer);
#ifdef MTPUSB_SPLIT_HEADER_MODE
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBufferShort);
#endif  /* MTPUSB_SPLIT_HEADER_MODE */
    return ps;
}


/*
 *  _MTPUSBRouteBufferRequest
 *
 *  Attempts to fulfill a buffer request and routes the resulting buffer
 *  to the context
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpCtx             MTP context for the request
 *      PSLMSGID        msgID               MTP message for the request
 *      PSLUINT32       dwSequenceID        Sequence ID for the request
 *      PSLUINT64       cbRequested         Bytes requested
 *      PSLUINT32       dwDataInfo          MTP data info flags
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteBufferRequest(MTPCONTEXT* pmtpctx, PSLMSGID msgID,
                            PSLUINT32 dwSequenceID, PSLUINT64 cbRequested,
                            PSLUINT32 dwDataInfo)
{
    PSLUINT32       cbDesired;
    PSLUINT32       cbActual;
    PSLUINT32       dwBuffer;
    PSLSTATUS       ps;
    MTPUSBDEVICE    mtpusbd = PSLNULL;
    PSLVOID*        pvBuffer = PSLNULL;
    MTPUSBCONTEXT*  pmtpusbctx;

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP USB transport information from the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Make sure the request is valid in the router context
     */

    switch (msgID)
    {
    case MTPMSG_DATA_BUFFER_REQUEST:
        /*  Make sure that we are sending data in the correct phase-
         *  we will only fulfill buffer requests if the phase has not yet
         *  been determined or we are already sending data
         */

        if ((MTPUSBDATAPHASE_UNKNOWN != pmtpusbctx->dwDataPhase) &&
            (MTPUSBDATAPHASE_SEND != pmtpusbctx->dwDataPhase))
        {
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }

        /* If the phase is not determined, this is the first data buffer,
         * log MTPPERF_DATAPHASE::TYPE_SENDBEGIN
         */
        if (MTPUSBDATAPHASE_UNKNOWN == pmtpusbctx->dwDataPhase)
        {
            MTPPERFLOG_DATAPHASE(MP_TYPE_SENDDATA_BEGIN,
                                 MTP_DATA_SIZE_UNKNOWN);
        }

        /*  Mark the data phase as send
         */

        pmtpusbctx->dwDataPhase = MTPUSBDATAPHASE_SEND;

        /*  First determine where we are at in our data handling- start by
         *  figuring out if we need to store this request as the total size
         */

        if (MTPMSGFLAG_COMPLETE & dwDataInfo)
        {
            /*  The complete flag is only valid when the data state is pending
             */

            PSLASSERT(MTPUSBDATASTATE_PENDING == pmtpusbctx->dwDataState);
            if (MTPUSBDATASTATE_PENDING != pmtpusbctx->dwDataState)
            {
                ps = PSLERROR_INVALID_OPERATION;
                goto Exit;
            }

            /*  We should never get a complete flag with unknown data size
             */

            PSLASSERT(MTP_DATA_SIZE_UNKNOWN != cbRequested);
            if (MTP_DATA_SIZE_UNKNOWN == cbRequested)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  Validate the length- we will not allow overflow of a 64-bit
             *  value here once the data packet header is added
             */

            PSLASSERT((cbRequested + sizeof(MTPUSBPACKET_DATA_HEADER)) >=
                            cbRequested);
            if ((cbRequested + sizeof(MTPUSBPACKET_DATA_HEADER)) < cbRequested)
            {
                ps = PSLERROR_INVALID_RANGE;
                goto Exit;
            }

            /*  Stash the total size of the transmission and remember that
             *  the length is known
             */

            pmtpusbctx->cbDataTotal = cbRequested +
                            sizeof(MTPUSBPACKET_DATA_HEADER);
            pmtpusbctx->cbDataPending = pmtpusbctx->cbDataTotal;
            pmtpusbctx->dwDataState = MTPUSBDATASTATE_OUT_READY;
        }
        else if (MTPUSBDATASTATE_PENDING == pmtpusbctx->dwDataState)
        {
            /*  This is the first request for a data buffer and a total size
             *  is not indicated- accept unknown size as the data size
             */

            PSLASSERT(MTP_DATA_SIZE_UNKNOWN == pmtpusbctx->cbDataTotal);
            pmtpusbctx->cbDataPending = pmtpusbctx->cbDataTotal;
            pmtpusbctx->dwDataState = MTPUSBDATASTATE_OUT_READY;
        }

        /*  Determine the size of a buffer to request
         */

        cbDesired = (PSLUINT32)(pmtpusbctx->cbDataPending & 0xffffffff);

        /*  Return a data buffer
         */

        dwBuffer = MTPBUFFER_TRANSMIT_DATA;
        break;

    case MTPMSG_EVENT_BUFFER_REQUEST:
        /*  Return an event buffer
         */

        cbDesired = sizeof(MTPUSBPACKET_EVENT);
        dwBuffer = MTPBUFFER_EVENT;
        break;

    case MTPMSG_RESPONSE_BUFFER_REQUEST:
        /*  Return a response buffer
         */

        MTPPERFLOG_RESPONSEPHASE(MP_TYPE_ELAPSETIME_BEGIN, 0);

        cbDesired = sizeof(MTPUSBPACKET_OPERATION_RESPONSE);
        dwBuffer = MTPBUFFER_RESPONSE;
        break;

    case MTPMSG_CMD_BUFFER_REQUEST:
        /*  Command buffers are only created by the router, never by
         *  anyone else.  Reject the request.
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;

    default:
        /*  Should never get here
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Try to retrieve a buffer from the USB device- if the buffer is not
     *  currently available wait for it to be provided
     */

    ps = MTPUSBDeviceRequestBuffer(mtpusbd, dwBuffer, cbDesired, dwSequenceID,
                            &pvBuffer, &cbActual);
    if (PSL_FAILED(ps) || (PSLSUCCESS_WOULD_BLOCK == ps))
    {
        goto Exit;
    }

    /*  And forward the message to the destination
     */

    ps = _MTPUSBRouteBufferAvailable(pmtpctx, dwBuffer, dwSequenceID,
                            pvBuffer, cbActual);
    pvBuffer = PSLNULL;
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbd, pvBuffer);
    return ps;
}


/*
 *  _MTPUSBRouteBufferRequestFlags
 *
 *  Requests a buffer be returned based on the buffer request flags
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to operate within
 *      PSLUINT32       dwSequenceID        Sequence ID for the request
 *      PSLUINT64       cbRequest           Size of the request
 *      PSLUINT32       dwFlags             Flags to check
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE if no request
 *                        made
 *
 */

PSLSTATUS _MTPUSBRouteBufferRequestFlags(MTPCONTEXT* pmtpctx,
                            PSLUINT32 dwSequenceID, PSLUINT64 cbRequest,
                            PSLUINT32 dwFlags)
{
    PSLUINT64       cbDesired;
    PSLUINT32       dwRequest;
    PSLUINT32       idxFlag;
    PSLMSGID        msgID;
    PSLSTATUS       ps;
    MTPUSBCONTEXT*  pmtpusbctx;

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure we have a request to fulfill
     */

    dwRequest = dwFlags & MTPMSGFLAG_BUFFER_REQUEST_MASK;
    if (0 == dwRequest)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Extract the MTP USB transport information from the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);

    /*  Loop while we still have requests to handle
     */

    for (idxFlag = 0;
            (0 != dwRequest) && (idxFlag < PSLARRAYSIZE(_RgdwRequestFlags));
            idxFlag++)
    {
        /*  See if this type of buffer is being requested
         */

        if (0 == (_RgdwRequestFlags[idxFlag] & dwRequest))
        {
            continue;
        }

        /*  Determine what type of request to issue along with the size
         */

        switch (_RgdwRequestFlags[idxFlag])
        {
        case MTPMSGFLAG_DATA_BUFFER_REQUEST:
            PSLASSERT(0 != cbRequest);
            if (0 == cbRequest)
            {
                ps = PSLERROR_INVALID_OPERATION;
                goto Exit;
            }
            cbDesired = cbRequest;
            msgID = MTPMSG_DATA_BUFFER_REQUEST;
            break;

        case MTPMSGFLAG_CMD_BUFFER_REQUEST:
            cbDesired = sizeof(MTPOPERATION);
            msgID = MTPMSG_CMD_BUFFER_REQUEST;
            break;

        case MTPMSGFLAG_RESPONSE_BUFFER_REQUEST:
            cbDesired = sizeof(MTPRESPONSE);
            msgID = MTPMSG_RESPONSE_BUFFER_REQUEST;
            break;

        case MTPMSGFLAG_EVENT_BUFFER_REQUEST:
            cbDesired = sizeof(MTPEVENT);
            msgID = MTPMSG_EVENT_BUFFER_REQUEST;
            break;

        default:
            /*  We should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Request the buffer
         */

        ps = _MTPUSBRouteBufferRequest(pmtpctx, msgID, dwSequenceID,
                            cbDesired, (MTPMSGFLAG_DATA_INFO_MASK & dwFlags));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Remove the request from the list we are servicing
         */

        dwRequest &= ~_RgdwRequestFlags[idxFlag];
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPUSBRouteBufferConsumed
 *
 *  Releases the buffer specified
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context
 *      PSLMSGID        msgID               Message ID
 *      PSLVOID*        pvBuffer            Buffer to release
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteBufferConsumed(MTPCONTEXT* pmtpctx, PSLMSGID msgID,
                            PSLVOID* pvBuffer)
{
    PSLSTATUS       ps;
    MTPUSBCONTEXT*  pmtpusbctx;

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP USB transport information from the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);

    /*  If there is no buffer in the message then we are done
     */

    if (PSLNULL == pvBuffer)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Make sure the request is valid in the router context
     */

    switch (msgID)
    {
    case MTPMSG_CMD_CONSUMED:
        /*  Map back to the USB buffer
         */

        MTPPERFLOG_COMMANDPHASE(MP_TYPE_ELAPSETIME_END);

        ps = MTPUSBParserMTPOperationToUSBBuffer(pvBuffer, 0,
                            MTPUSBPARSERFLAGS_EMPTY, &pvBuffer, PSLNULL);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPMSG_RESPONSE:
        /*  Buffer is acceptable to be released
         */

        ps = MTPUSBParserMTPResponseToUSBBuffer(pvBuffer, 0,
                            MTPUSBPARSERFLAGS_EMPTY, &pvBuffer, PSLNULL);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPMSG_DATA_CONSUMED:
        /*  Map back to the USB buffer for the release
         */

        ps = MTPUSBParserMTPDataBufferToUSBBuffer(pvBuffer, 0,
                            MTPUSBPARSERFLAGS_EMPTY |
                                ((pmtpusbctx->pvBufferFirst == pvBuffer) ?
                                        MTPUSBPARSERFLAGS_FIRST_PACKET : 0),
                            &pvBuffer, PSLNULL);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPMSG_DATA_READY_TO_SEND:
        /*  Need to figure out how to deal with this one
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;

    case MTPMSG_CMD_READY_TO_SEND:
    case MTPMSG_EVENT_CONSUMED:
    case MTPMSG_RESPONSE_CONSUMED:
        /*  These messages are only valid in the context of a proxy
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;

    default:
        /*  Should never get here
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Release the buffer
     */

    SAFE_MTPUSBDEVICERELEASEBUFFER(pmtpusbctx->mtpusbd, pvBuffer);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPUSBRouteBufferRelease
 *
 *  Releases a buffer if present in the message
 *
 *  Arguments:
 *      PSL_CONST MTPUSBCONTEXT*
 *                      pmtpusbctx          MTP context to use
 *      PSL_CONST PSLMSG*
 *                      pMsg                Message to check for a buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteBufferRelease(PSL_CONST MTPUSBCONTEXT* pmtpusbctx,
                            PSL_CONST PSLMSG* pMsg)
{
    PSLSTATUS       ps;
    PSLVOID*        pvBuffer;
    PSLMSGID        msgID;

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpusbctx) || (PSLNULL == pMsg))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If no buffer is present then we are done
     */

    if (!(MTPMSGID_BUFFER_PRESENT & pMsg->msgID))
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  If the buffer is not present then we have no work to do
     */

    if (PSLNULL == (PSLVOID*)pMsg->aParam2)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /* If the message is MTPMSG_BUFFER_IGNORED, the buffer type varies,
     * but the original message ID is in the Param #3
     */
    msgID = (MTPMSG_BUFFER_IGNORED != pMsg->msgID) ? pMsg->msgID
                                                   : (PSLMSGID)pMsg->aParam3;

    /*  Extract the correct MTP USB buffer from the message
     */

    switch (msgID)
    {
    case MTPMSG_CMD_AVAILABLE:
    case MTPMSG_CMD_CONSUMED:
    case MTPMSG_CMD_BUFFER_REQUEST:
    case MTPMSG_CMD_BUFFER_AVAILABLE:
    case MTPMSG_CMD_READY_TO_SEND:
        /*  Unpack the MTP form of the command buffer back to the USB form
         */

        ps = MTPUSBParserMTPOperationToUSBBuffer((PXMTPOPERATION)pMsg->aParam2,
                            0, MTPUSBPARSERFLAGS_EMPTY, &pvBuffer, PSLNULL);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPMSG_DATA_AVAILABLE:
    case MTPMSG_DATA_CONSUMED:
    case MTPMSG_DATA_BUFFER_REQUEST:
    case MTPMSG_DATA_BUFFER_AVAILABLE:
    case MTPMSG_DATA_READY_TO_SEND:
        /*  Unpack the MTP form of the data buffer back to the USB form
         */

        ps = MTPUSBParserMTPDataBufferToUSBBuffer((PSLVOID*)pMsg->aParam2, 0,
                ((((PSLVOID*)pMsg->aParam2) == pmtpusbctx->pvBufferFirst) ?
                            MTPUSBPARSERFLAGS_FIRST_PACKET : 0 ) |
                    MTPUSBPARSERFLAGS_EMPTY, &pvBuffer, PSLNULL);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPMSG_EVENT_AVAILABLE:
    case MTPMSG_EVENT_CONSUMED:
    case MTPMSG_EVENT_BUFFER_REQUEST:
    case MTPMSG_EVENT_BUFFER_AVAILABLE:
    case MTPMSG_EVENT_READY_TO_SEND:
        /*  Unpack the MTP form of the event buffer back to the USB form
         */

        ps = MTPUSBParserMTPEventToUSBBuffer((PXMTPEVENT)pMsg->aParam2, 0,
                            MTPUSBPARSERFLAGS_EMPTY, &pvBuffer, PSLNULL);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPMSG_RESPONSE_AVAILABLE:
    case MTPMSG_RESPONSE_CONSUMED:
    case MTPMSG_RESPONSE_BUFFER_REQUEST:
    case MTPMSG_RESPONSE_BUFFER_AVAILABLE:
    case MTPMSG_RESPONSE:
        /*  Unpack the MTP form of the response buffer back to the USB form
         */

        ps = MTPUSBParserMTPResponseToUSBBuffer((PXMTPRESPONSE)pMsg->aParam2, 0,
                            MTPUSBPARSERFLAGS_EMPTY, &pvBuffer, PSLNULL);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPUSBMSG_BUFFER_AVAILABLE:
    case MTPUSBMSG_BUFFER_RECEIVED:
        /*  Control messages coming from the USB driver also contain an
         *  extra buffer (the class request) as the optional parameter-
         *  make sure we release those buffers as well
         */

        if (MTPUSBBUFFER_CONTROL == pMsg->aParam0)
        {
            /*  Release the original control request
             */

            pvBuffer = (PSLVOID*)pMsg->aParam1;
            SAFE_MTPUSBDEVICERELEASEBUFFER(pmtpusbctx->mtpusbd, pvBuffer);
        }

        /*  FALL THROUGH INTENTIONAL */

    default:
        /*  Point to the actual buffer
         */

        pvBuffer = (PSLVOID*)pMsg->aParam2;
        break;
    }

    /*  Release the buffer in the message
     */

    SAFE_MTPUSBDEVICERELEASEBUFFER(pmtpusbctx->mtpusbd, pvBuffer);

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*
 *  _MTPUSBRouteCreate
 *
 *  Creates an MTPCONTEXT for the route using the provided USB information
 *  and requests a command buffer on the route.
 *
 *  Arguments:
 *      MTPUSBCONTEXT*  pmtpusbctx          MTP USB context for this connection
 *      MTPCONTEXT**    ppmtpctx            Resulting MTPCONTEXT
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteCreate(MTPUSBCONTEXT* pmtpusbctx,
                            MTPCONTEXT** ppmtpctx)
{
    PSLSTATUS       ps;
    MTPCONTEXT*     pmtpctxNew = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != ppmtpctx)
    {
        *ppmtpctx = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpusbctx) || (PSLNULL == ppmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create a new context
     */

    ps = MTPRouteCreate(pmtpusbctx->mqTransport, &pmtpctxNew);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    PSLASSERT(sizeof(MTPUSBCONTEXT*) <=
                            pmtpctxNew->mtpTransportState.cbTransportData);
    if (sizeof(MTPUSBCONTEXT*) > pmtpctxNew->mtpTransportState.cbTransportData)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }
    *((PSL_CONST MTPUSBCONTEXT**)
            pmtpctxNew->mtpTransportState.pbTransportData) = pmtpusbctx;

    /*  Request a USB command buffer to get started
     */

    ps = MTPUSBDeviceRequestBuffer(pmtpusbctx->mtpusbd, MTPBUFFER_COMMAND,
                            sizeof(MTPUSBPACKET_OPERATION_REQUEST), 0,
                            PSLNULL, PSLNULL);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Add the MTP Context back pointer to the USB context
     */

    PSLAtomicExchangePointer(&(pmtpusbctx->pmtpctx), pmtpctxNew);

    /*  Return the new context
     */

    *ppmtpctx = pmtpctxNew;
    pmtpctxNew = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPUSBROUTEDESTROY(pmtpctxNew);
    return ps;
}


/*
 *  _MTPUSBRouteTerminate
 *
 *  Handles routing a disconnect or shutdown message to the handlers and
 *  then removes all messages from the queue until the handlers shutdown
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to disconnect
 *      PSLMSGID        msgID               Message causing the disconnect
 *      PSLPARAM        aParam              Parameter to add to the message
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteTerminate(MTPCONTEXT* pmtpctx, PSLMSGID msgID,
                            PSLPARAM aParam)
{
    PSLBOOL         fDone;
    PSLBOOL         fHandleClassReq = PSLFALSE;
    PSLMSGID        msgIDComplete;
    MTPUSBDEVICE    mtpusbd;
    MTPROUTEUPDATE  mtpRU = { 0 };
    PSLSTATUS       ps;
    PSLMSG*         pMsg = PSLNULL;
    MTPUSBCONTEXT*  pmtpusbctx;

    /*  Validate Arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP/USB transport information from the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Determine the correct message to forward
     */

    switch (msgID)
    {
    case MTPUSBMSG_DEVICE_DISCONNECT:
    case MTPUSBMSG_DEVICE_RESET:
        /*  From the prespective of the handler a USB bus reset is the same
         *  as a device disconnect- the device is released and the system
         *  resets to a "disconnected" state
         */

        msgID = MTPMSG_DISCONNECT;
        msgIDComplete = MTPMSG_DISCONNECT_COMPLETE;
        break;

    case MTPMSG_CANCEL:
        msgIDComplete = MTPMSG_CANCEL_COMPLETE;
        fHandleClassReq = PSLTRUE;
        break;

    case MTPMSG_RESET:
        msgIDComplete = MTPMSG_RESET_COMPLETE;
        fHandleClassReq = PSLTRUE;
        break;

    case MTPMSG_SHUTDOWN:
        msgIDComplete = MTPMSG_SHUTDOWN_COMPLETE;
        break;

    default:
        /*  Unexpected message
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Reset the device so that any pending transfers and buffer requests
     *  are cancelled
     *
     *  NOTE: We ignore the error code here because if the device has actually
     *  disconnected we will get an error
     */

    MTPUSBDeviceReset(mtpusbd);
    pmtpusbctx->aCookiePendingRcv = 0;

    /*  Route the message to any active handlers
     */

    ps = PSLMsgAlloc(pmtpusbctx->mpTransport, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg->msgID = msgID;
    pMsg->aParam0 = (PSLPARAM)pmtpctx;
    pMsg->aParam1 = aParam;
    ps = MTPRouteMsgToHandler(pmtpctx, pMsg);
    if (PSL_SUCCEEDED(ps))
    {
        pMsg = PSLNULL;
    }

    /*  If we successfully sent a message via the router we need to
     *  block here and wait for a complete message back from the
     *  handler so that we know we can complete the shutdown.  All
     *  other messages need to be ignored.
     */

    if (PSLSUCCESS == ps)
    {
        /*  Loop until we get a complete message back
         */

        fDone = PSLFALSE;
        while (!fDone)
        {
            /*  Release any existing message
             */

            SAFE_PSLMSGFREE(pMsg);

            /*  Wait for the message
             */

            ps = PSLMsgGet(pmtpusbctx->mqTransport, &pMsg);
            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_FAILED(ps))
            {
                /*  Go ahead and exit with the error
                 */

                goto Exit;
            }

            /*  Handle any messages that we may need to
             */

            switch (pMsg->msgID)
            {
            case MTPUSBMSG_BUFFER_AVAILABLE:
            case MTPUSBMSG_BUFFER_RECEIVED:
                /*  If we are not handling a reset then we want to ignore
                 *  release the buffer immediately
                 */

                if (!fHandleClassReq ||
                    (MTPUSBBUFFER_CONTROL != pMsg->aParam0))
                {
                    ps = _MTPUSBRouteBufferRelease(pmtpusbctx, pMsg);
                    if (PSL_FAILED(ps))
                    {
                        goto Exit;
                    }
                    break;
                }

                /*  Handle the buffer message
                 */

                if (MTPUSBMSG_BUFFER_AVAILABLE == pMsg->msgID)
                {
                    ps = _MTPUSBRouteBufferAvailable(pmtpctx, pMsg->aParam0,
                            pMsg->aParam1, (PSLVOID*)pMsg->aParam2,
                            pMsg->alParam);
                }
                else
                {
                    PSLASSERT(MTPUSBMSG_BUFFER_RECEIVED == pMsg->msgID);
                    ps = _MTPUSBRouteBufferReceived(pmtpctx, pMsg->aParam0,
                            pMsg->aParam1, (PSLVOID*)pMsg->aParam2,
                            pMsg->alParam, pMsg->aParam3);
                }
                if (PSL_FAILED(ps))
                {
                    goto Exit;
                }
                break;

            default:
                /*  Release any buffers in the message
                 */

                ps = _MTPUSBRouteBufferRelease(pmtpusbctx, pMsg);
                if (PSL_FAILED(ps))
                {
                    goto Exit;
                }
                break;
            }

            /*  See if we have received the complete message
             */

            if (msgIDComplete == pMsg->msgID)
            {
                /*  If we are handling a cancel we want to end the route as
                 *  well
                 */

                if (MTPMSG_CANCEL_COMPLETE == msgIDComplete)
                {
                    /*  Extract the new lookup table from aParam3
                     */

                    mtpRU.hHandlerTableNew = (PSLLOOKUPTABLE)pMsg->aParam3;
                }

                /*  Stop handling messages
                 */

                fDone = PSLTRUE;
            }
        }
    }

    /*  If we are handling a cancel message then we need to end the
     *  current route
     */

    if (MTPMSG_CANCEL_COMPLETE == msgIDComplete)
    {
        ps = MTPRouteEnd(pmtpctx, &mtpRU);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*
 *  _MTPUSBRouteReset
 *
 *  Handles a device reset operation- note that context will change during
 *  the reset.
 *
 *  Arguments:
 *      MTPCONTEXT**    ppmtpctx            Context to reset
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS _MTPUSBRouteReset(MTPCONTEXT** ppmtpctx)
{
    PSLSTATUS       ps;
    MTPUSBCONTEXT*  pmtpusbctx;

    /*  Validate Arguments
     */

    if ((PSLNULL == ppmtpctx) || (PSLNULL == *ppmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP/USB transport information from the context
     */

    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(*ppmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);

    /*  Disconnect the route and make sure that all "pending"
     *  messages have been removed from the queue
     */

    ps = _MTPUSBRouteTerminate(*ppmtpctx, MTPMSG_RESET, 0);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    ps = PSLMsgEnum(pmtpusbctx->mqTransport, _MTPUSBRouterResetQueueEnum,
                            (PSLPARAM)*ppmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Recreate a new route to get us back to an initialized state
     */

    SAFE_MTPUSBROUTEDESTROY(*ppmtpctx);
    ps = _MTPUSBRouteCreate(pmtpusbctx, ppmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  _MTPUSBRouterCleanQueueEnum
 *
 *  Callback function used with PSLMsgEnum when when the router is shutting
 *  down to make sure that all messages have been removed from the queue
 *
 *  Arguments:
 *      PSL_CONST PSLMSG*
 *                      pMsg                Message to check
 *      PSLPARAM        aParam              Callback parameter
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE to have
 *                        message removed, PSLSUCCESS_CANCEL to stop
 *                        enumeration
 *
 */

PSLSTATUS _MTPUSBRouterCleanQueueEnum(PSL_CONST PSLMSG* pMsg, PSLPARAM aParam)
{
    PSLSTATUS       ps;
    MTPUSBCONTEXT*  pmtpusbctx;

    /*  Validate Arguments
     */

    if ((PSLNULL == pMsg) || (PSLNULL == aParam))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP/USB device from the parameter
     */

    pmtpusbctx = (MTPUSBDEVICE)aParam;

    /*  Look to see if the message has a buffer
     */

    ps = _MTPUSBRouteBufferRelease(pmtpusbctx, pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Remove the message from the queue
     */

    ps = PSLSUCCESS_FALSE;

Exit:
    return ps;
}



/*
 *  _MTPUSBRouterResetQueueEnum
 *
 *  Callback function used with PSLMsgEnum when when the router is
 *  performing a device reset to make sure that all messages have been
 *  removed from the queue
 *
 *  Arguments:
 *      PSL_CONST PSLMSG*
 *                      pMsg                Message to check
 *      PSLPARAM        aParam              Callback parameter
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE to have
 *                        message removed, PSLSUCCESS_CANCEL to stop
 *                        enumeration
 *
 */

PSLSTATUS _MTPUSBRouterResetQueueEnum(PSL_CONST PSLMSG* pMsg, PSLPARAM aParam)
{
    MTPUSBDEVICE    mtpusbd;
    PSLSTATUS       ps;
    MTPCONTEXT*     pmtpctx;
    MTPUSBCONTEXT*  pmtpusbctx;

    /*  Validate Arguments
     */

    if ((PSLNULL == pMsg) || (PSLNULL == aParam))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the MTP/USB transport information from the parameter
     */

    pmtpctx = (MTPCONTEXT*)aParam;
    pmtpusbctx = MTPCONTEXT_TO_PMTPUSBCONTEXT(pmtpctx);
    PSLASSERT(MTPUSBCONTEXT_COOKIE == pmtpusbctx->dwCookie);
    mtpusbd = pmtpusbctx->mtpusbd;

    /*  Handle any messages that we may need to
     */

    switch (pMsg->msgID)
    {
    case MTPUSBMSG_BUFFER_AVAILABLE:
    case MTPUSBMSG_BUFFER_RECEIVED:
        /*  If we are not dealing with a control buffer we want to ignore
         *  this and release it
         */

        if (MTPUSBBUFFER_CONTROL != pMsg->aParam0)
        {
            ps = _MTPUSBRouteBufferRelease(pmtpusbctx, pMsg);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;
        }

        /*  Handle the buffer message
         */

        if (MTPUSBMSG_BUFFER_AVAILABLE == pMsg->msgID)
        {
            ps = _MTPUSBRouteBufferAvailable(pmtpctx, pMsg->aParam0,
                    pMsg->aParam1, (PSLVOID*)pMsg->aParam2,
                    pMsg->alParam);
        }
        else
        {
            PSLASSERT(MTPUSBMSG_BUFFER_RECEIVED == pMsg->msgID);
            ps = _MTPUSBRouteBufferReceived(pmtpctx, pMsg->aParam0,
                    pMsg->aParam1, (PSLVOID*)pMsg->aParam2,
                    pMsg->alParam, pMsg->aParam3);
        }
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    default:
        /*  Release any buffers in the message
         */

        ps = _MTPUSBRouteBufferRelease(pmtpusbctx, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;
    }

    /*  Remove the message from the queue
     */

    ps = PSLSUCCESS_FALSE;

Exit:
    return ps;
}


/*
 *  MTPUSBRouterThread
 *
 *  Main thread for an MTP/USB Router
 *
 *  Arguments:
 *      PSLVOID*        pvThreadParam       Thread parameter (MTPUSBROUTERINIT)
 *
 *  Returns:
 *      PSLUINT32       PSLSTATUS (PSLSUCCESS on success)
 *
 */

PSLUINT32 MTPUSBRouterThread(PSLVOID* pvThreadParam)
{
    PSLPARAM            aCookie;
    PSLUINT32           cbActual;
    PSLBOOL             fDone;
    PSLHANDLE           hSignalInit = PSLNULL;
    PSLMSGQUEUE         mqTransport = PSLNULL;
    PSLMSGPOOL          mpTransport = PSLNULL;
    PSLMSGQUEUE         mqShutdown = PSLNULL;
    MTPUSBDEVICEINFO    mtpusbdI;
    MTPUSBCONTEXT       mtpusbctx = { 0 };
    PSLSTATUS           ps;
    PSLMSG*             pMsg = PSLNULL;
    MTPCONTEXT*         pmtpctx = PSLNULL;
    MTPUSBROUTERINIT*   pmtpri = PSLNULL;
    PSLVOID*            pvParam = PSLNULL;
    PSLVOID*            pvBuffer = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == pvThreadParam)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get at the router initialization info
     */

    pmtpri = (MTPUSBROUTERINIT*)pvThreadParam;
    hSignalInit = pmtpri->hSignalComplete;

    /*  Validate that we have a good device
     */

    if (PSLNULL == pmtpri->mtpusbdInitiator)
    {
        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate our queue and pool
     */

    ps = MTPMsgGetQueueParam(&pvParam);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = PSLMsgQueueCreate(pvParam, MTPMsgOnPost, MTPMsgOnWait, MTPMsgOnDestroy,
                            &mqTransport);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pvParam = PSLNULL;

    ps = PSLMsgPoolCreate(MTPUSBROUTER_POOL_SIZE, &mpTransport);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Register our queue with the shutdown service so that we make sure
     *  we are forced close if shutdown occurs
     */

    ps = MTPShutdownRegister(mqTransport);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    mqShutdown = mqTransport;

    /*  Initialze the basic information in the context
     */

#ifdef PSL_ASSERTS
    mtpusbctx.dwCookie = MTPUSBCONTEXT_COOKIE;
#endif  /* PSL_ASSERTS */
    mtpusbctx.mqTransport = mqTransport;
    mtpusbctx.mpTransport = mpTransport;

    /*  Allocate the device ready signal to make sure that we do not respond
     *  to a device ready request prior to being ready
     */

    ps = PSLSignalOpen(PSLFALSE, PSLTRUE, PSLNULL,
                            &(mtpusbctx.hSignalDeviceReady));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Attempt to bind this transport to the USB device provided
     */

    ps = MTPUSBDeviceBind(pmtpri->mtpusbdInitiator, mqTransport,
                            _MTPUSBRouteClassRequest, (PSLPARAM)&mtpusbctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  We have succesfully bound to the device- go ahead and copy the
     *  information into the context and then save some redirection
     */

    mtpusbctx.mtpusbd = pmtpri->mtpusbdInitiator;

    /*  Request USB device information and stash the data size in the context
     */

    ps = MTPUSBDeviceGetInfo(mtpusbctx.mtpusbd, &mtpusbdI);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    mtpusbctx.cbPacketData = mtpusbdI.cbPacketData;
    mtpusbctx.cbMaxData = mtpusbdI.cbMaxData;

    /*  Now that the USB is all initialized go ahead and create a route
     */

    ps = _MTPUSBRouteCreate(&mtpusbctx, &pmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Report that we have succesfully initialized
     */

    pmtpri->psResult = PSLSUCCESS;
    ps = PSLSignalSet(hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pmtpri = PSLNULL;
    hSignalInit = PSLNULL;

    /*  Report that the device is ready
     */

    ps = PSLSignalSet(mtpusbctx.hSignalDeviceReady);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Start looping to handle messages
     */

    fDone = PSLFALSE;
    while (!fDone)
    {
        /*  Release any existing message and buffer that we received
         */

        SAFE_PSLMSGFREE(pMsg);

        /*  And wait for the next message to be recieved
         */

        ps = PSLMsgGet(mqTransport, &pMsg);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Determine what we need to do
         */

        switch (pMsg->msgID)
        {
        case MTPUSBMSG_BUFFER_RECEIVED:
            /*  Route the buffer
             */

            ps = _MTPUSBRouteBufferReceived(pmtpctx, pMsg->aParam0,
                            pMsg->aParam1, (PSLVOID*)pMsg->aParam2,
                            pMsg->alParam, pMsg->aParam3);
            if (PSL_FAILED(ps))
            {
                break;
            }
            break;

        case MTPUSBMSG_BUFFER_SENT:
            /*  Forward the buffer sent message and release the buffer if
             *  appropriate
             */

            ps = _MTPUSBRouteBufferSent(pmtpctx, pMsg->aParam0,
                            pMsg->aParam1, (PSLVOID*)pMsg->aParam2,
                            pMsg->alParam, pMsg->aParam3);
            if (PSL_FAILED(ps))
            {
                break;
            }
            break;

        case MTPUSBMSG_BUFFER_AVAILABLE:
            /*  Forward the buffer along to the handlers
             */

            ps = _MTPUSBRouteBufferAvailable(pmtpctx, pMsg->aParam0,
                            pMsg->aParam1, (PSLVOID*)pMsg->aParam2,
                            pMsg->alParam);
            if (PSL_FAILED(ps))
            {
                break;
            }
            break;

        case MTPMSG_SHUTDOWN:
        case MTPUSBMSG_DEVICE_DISCONNECT:
        case MTPUSBMSG_DEVICE_RESET:
            /*  Disconnect the handlers
             */

            ps = _MTPUSBRouteTerminate(pmtpctx, pMsg->msgID, 0);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  Stop handling messages
             */

            fDone = PSLTRUE;
            break;

        case MTPUSBMSG_SEND_FAILED:
        case MTPUSBMSG_RECEIVE_FAILED:
        case MTPUSBMSG_SEND_INCOMPLETE:
        case MTPUSBMSG_RECEIVE_INCOMPLETE:
        case MTPUSBMSG_NOT_RESPONDING:
        case MTPUSBMSG_BUFFER_ERROR:
        case MTPUSBMSG_SEND_CANCELLED:
        case MTPUSBMSG_RECEIVE_CANCELLED:
            /*  Currently there is not a good way to deal with these
             *  messages- make sure we at least release the buffer
             *  in them
             */

            if (MTPMSGID_BUFFER_PRESENT & pMsg->msgID)
            {
                /*  Release the default buffer
                 */

                pvBuffer = (PSLVOID*)pMsg->aParam2;
                SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbctx.mtpusbd,
                            pvBuffer);
            }

            /*  If this is for a control transfer we also have
             *  a buffer sitting in the optional parameter
             */

            if (MTPUSBBUFFER_CONTROL == pMsg->aParam0)
            {
                /*  Release the buffer
                 */

                pvBuffer = (PSLVOID*)pMsg->aParam1;
                SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbctx.mtpusbd,
                            pvBuffer);

                /*  And fail the request
                 */

                MTPUSBDeviceStallPipe(mtpusbctx.mtpusbd,
                            MTPUSBBUFFER_CONTROL);
                MTPUSBDeviceSendControlHandshake(mtpusbctx.mtpusbd);
            }

            /*  Remember the error conditions
             */

            ps = ((MTPUSBMSG_SEND_CANCELLED == pMsg->msgID) ||
                  (MTPUSBMSG_RECEIVE_CANCELLED == pMsg->msgID)) ?
                            PSLSUCCESS : PSLERROR_USB_FAILURE;
            break;

        case MTPUSBROUTERMSG_RESET:
            /*  Reset the route specified
             */

            PSLASSERT((MTPCONTEXT*)pMsg->aParam0 == pmtpctx);
            ps = _MTPUSBRouteReset(&pmtpctx);

            /*  See if we are responding to a class request- if so we need
             *  to make sure that we notify the host of our state
             */

            if (pMsg->aParam1)
            {
                /*  If an error occurred we need to stall the pipe
                 */

                if (PSL_FAILED(ps))
                {
                    MTPUSBDeviceStallPipe(mtpusbctx.mtpusbd,
                            MTPUSBBUFFER_CONTROL);
                }

                /*  And send the status message
                 */

                MTPUSBDeviceSendControlHandshake(mtpusbctx.mtpusbd);

                /*  Unblock the device status response
                 */

                PSLSignalSet(mtpusbctx.hSignalDeviceReady);
            }

            /*  If we failed to reset then close this connection regardless
             */

            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        case MTPMSG_CMD_BUFFER_REQUEST:
        case MTPMSG_DATA_BUFFER_REQUEST:
        case MTPMSG_EVENT_BUFFER_REQUEST:
        case MTPMSG_RESPONSE_BUFFER_REQUEST:
            /*  Make sure the context matches
             */

            PSLASSERT((MTPCONTEXT*)pMsg->aParam0 == pmtpctx);

            /*  Handle buffer requests
             */

            ps = _MTPUSBRouteBufferRequest(pmtpctx, pMsg->msgID,
                            pMsg->aParam1, pMsg->alParam,
                            (MTPMSGFLAG_DATA_INFO_MASK & pMsg->aParam3));
            if (PSL_FAILED(ps))
            {
                ps = MTPRouteTransportError(pmtpctx, mpTransport, pMsg->msgID,
                            pMsg->aParam1, ps);
            }
            break;

        case MTPMSG_CMD_CONSUMED:
        case MTPMSG_DATA_CONSUMED:
        case MTPMSG_EVENT_CONSUMED:
        case MTPMSG_RESPONSE_CONSUMED:
            /*  Make sure the context matches
             */

            PSLASSERT((MTPCONTEXT*)pMsg->aParam0 == pmtpctx);

            /*  Handle buffer consumed notifications
             */

            ps = _MTPUSBRouteBufferConsumed(pmtpctx, pMsg->msgID,
                            (PSLVOID*)pMsg->aParam2);
            if (PSL_FAILED(ps))
            {
                ps = MTPRouteTransportError(pmtpctx, mpTransport, pMsg->msgID,
                            pMsg->aParam1, ps);
            }

            /*  Handle any buffer request flags
             */

            ps = _MTPUSBRouteBufferRequestFlags(pmtpctx, pMsg->aParam1,
                            pMsg->alParam, pMsg->aParam3);
            if (PSL_FAILED(ps))
            {
                ps = MTPRouteTransportError(pmtpctx, mpTransport, pMsg->msgID,
                            pMsg->aParam1, ps);
            }
            break;

        case MTPMSG_BUFFER_IGNORED:
            /*  Release the buffer specified in the message
             */

            ps = _MTPUSBRouteBufferRelease(&mtpusbctx, pMsg);
            if (PSL_FAILED(ps))
            {
                break;
            }
            break;

        case MTPMSG_DATA_READY_TO_SEND:
            /*  Make sure the context matches
             */

            PSLASSERT((MTPCONTEXT*)pMsg->aParam0 == pmtpctx);

            /*  Handle sending the buffer
             */

            ps = _MTPUSBRouteDataSend(pmtpctx, pMsg->aParam1,
                            (PSLVOID*)pMsg->aParam2, pMsg->alParam,
                            (MTPMSGFLAG_DATA_INFO_MASK & pMsg->aParam3));
            if (PSL_FAILED(ps))
            {
                ps = MTPRouteTransportError(pmtpctx, mpTransport, pMsg->msgID,
                            pMsg->aParam1, ps);
                break;
            }

            /*  Check to see if we need to respond with a buffer while
             *  determining the standard message for the request
             */

            ps = _MTPUSBRouteBufferRequestFlags(pmtpctx, pMsg->aParam1,
                            pMsg->alParam, pMsg->aParam3);
            if (PSL_FAILED(ps))
            {
                ps = MTPRouteTransportError(pmtpctx, mpTransport, pMsg->msgID,
                            pMsg->aParam1, ps);
                break;
            }
            break;

        case MTPMSG_RESPONSE:
            /*  If we still have a pending receive active cancel it
             */

            if (0 != mtpusbctx.aCookiePendingRcv)
            {
                ps = MTPUSBDeviceCancelRequest(mtpusbctx.mtpusbd,
                                mtpusbctx.aCookiePendingRcv);
                PSLASSERT(PSL_SUCCEEDED(ps));
                if (PSL_FAILED(ps))
                {
                    break;
                }
                mtpusbctx.aCookiePendingRcv = 0;
            }

            /*  Validate the length
             */

            if ((0xffffffff & pMsg->alParam) == pMsg->alParam)
            {
                /*  Extract the full buffer from the response buffer
                 */

                ps = MTPUSBParserMTPResponseToUSBBuffer(
                        (PXMTPRESPONSE)pMsg->aParam2, (PSLUINT32)pMsg->alParam,
                        0, &pvBuffer, &cbActual);
                PSLASSERT(PSL_SUCCEEDED(ps));
                if (PSL_SUCCEEDED(ps))
                {
                    /*  Transmit the buffer
                     */

                    ps = MTPUSBDeviceSendBuffer(mtpusbctx.mtpusbd,
                            MTPMSGFLAG_TERMINATE,
                            pvBuffer, cbActual, pMsg->aParam3, &aCookie);
                    if (PSL_SUCCEEDED(ps))
                    {
                        pvBuffer = PSLNULL;
                    }
                }
            }
            else
            {
                ps = PSLERROR_INVALID_PARAMETER;
            }

            /*  Provide some feedback
             */

            PSLTraceProgress(
                "MTPUSBRouterThread: Response- dwID = 0x%08x\tResult = 0x%04x"
                    "\tps = 0x%08x",
                MTPLoadUInt32(&((PXMTPRESPONSE)pMsg->aParam2)->dwTransactionID),
                MTPLoadUInt16(&((PXMTPRESPONSE)pMsg->aParam2)->wRespCode),
                ps);

            /*  Report any errors
             */

            if (PSL_FAILED(ps))
            {
                ps = MTPRouteTransportError(pmtpctx, mpTransport, pMsg->msgID,
                                pMsg->aParam1, ps);
            }
            SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbctx.mtpusbd, pvBuffer);
            break;

        case MTPMSG_EVENT_READY_TO_SEND:

            /*  Extract the full buffer from the event buffer
             */

            ps = MTPUSBParserMTPEventToUSBBuffer(
                    (PXMTPEVENT)pMsg->aParam2, (PSLUINT32)pMsg->alParam,
                    0, &pvBuffer, &cbActual);
            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_SUCCEEDED(ps))
            {
                /*  Transmit the buffer
                 */

                ps = MTPUSBDeviceSendBuffer(mtpusbctx.mtpusbd,
                        MTPMSGFLAG_TERMINATE,
                        pvBuffer, cbActual, pMsg->aParam3, &aCookie);
                if (PSL_SUCCEEDED(ps))
                {
                    pvBuffer = PSLNULL;
                }
            }
            /*  Report any errors
             */

            if (PSL_FAILED(ps))
            {
                ps = MTPRouteTransportError(pmtpctx, mpTransport, pMsg->msgID,
                                pMsg->aParam1, ps);
            }
            SAFE_MTPUSBDEVICERELEASEBUFFER(mtpusbctx.mtpusbd, pvBuffer);

            /*  Check to see if we need to respond with another buffer
             */

            ps = _MTPUSBRouteBufferRequestFlags(pmtpctx, pMsg->aParam1, 0,
                            pMsg->aParam3);
            if (PSL_FAILED(ps))
            {
                ps = MTPRouteTransportError(pmtpctx, mpTransport, pMsg->msgID,
                            pMsg->aParam1, ps);
                break;
            }
            break;

        case MTPMSG_CMD_READY_TO_SEND:
        case MTPMSG_CMD_AVAILABLE:
        case MTPMSG_CMD_BUFFER_AVAILABLE:
        case MTPMSG_DATA_AVAILABLE:
        case MTPMSG_DATA_BUFFER_AVAILABLE:
        case MTPMSG_EVENT_AVAILABLE:
        case MTPMSG_EVENT_BUFFER_AVAILABLE:
        case MTPMSG_RESPONSE_BUFFER_AVAILABLE:
        case MTPMSG_RESPONSE_AVAILABLE:
        case MTPMSG_CMD_SENT:
        case MTPMSG_DATA_SENT:
        case MTPMSG_EVENT_SENT:
            /*  These messages are all sent by the transport and should never
             *  be received by one
             */

            PSLASSERT(PSLFALSE);
            ps = MTPRouteTransportError(pmtpctx, mpTransport, pMsg->msgID,
                            pMsg->aParam1, PSLERROR_INVALID_OPERATION);
            break;

        default:
            break;
        }

        /*  If we encountered an error condition while handling a particular
         *  message go ahead and trigger a notification to the Initiator
         *  that we have entered a bad state
         */

        if (PSL_FAILED(ps))
        {
            /*  Stall both the data in and data out pipes
             */

            ps = MTPUSBDeviceStallPipe(mtpusbctx.mtpusbd,
                            MTPBUFFER_TRANSMIT_DATA);
            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            ps = MTPUSBDeviceStallPipe(mtpusbctx.mtpusbd,
                            MTPBUFFER_RECEIVE_DATA);
            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    /*  Close the connection if necessary
     */

    if ((PSLNULL != mqTransport) && (PSLNULL != mtpusbctx.mtpusbd))
    {
        /*  Remove all pending messages and unbind the device
         */

        PSLMsgEnum(mqTransport, _MTPUSBRouterCleanQueueEnum,
                            (PSLPARAM)&mtpusbctx);
        MTPUSBDeviceUnbind(mtpusbctx.mtpusbd, mqTransport);
    }
    SAFE_PSLMSGFREE(pMsg);
    SAFE_MTPUSBROUTEDESTROY(pmtpctx);
    SAFE_PSLMSGQUEUEDESTROY(mqTransport);
    SAFE_PSLMSGPOOLDESTROY(mpTransport);
    SAFE_MTPMSGRELEASEQUEUEPARAM(pvParam);
    SAFE_PSLSIGNALCLOSE(mtpusbctx.hSignalDeviceReady);
    PSLASSERT((PSLNULL == hSignalInit) || (PSLNULL != pmtpri));
    if ((PSLNULL != hSignalInit) && (PSLNULL != pmtpri))
    {
        pmtpri->psResult = ps;
        PSLSignalSet(hSignalInit);
    }

    /*  Finally unregister with the shutdown service after everything else
     *  is closed- this guarantees that our code stays in memory up to
     *  at least this point
     */

    if (PSLNULL != mqShutdown)
    {
        MTPShutdownUnregister(mqShutdown);
    }
    return ps;
}


