/*
 *  MTPServiceContext.c
 *
 *  Contains common service context functionality
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPServicePrecomp.h"
#include "MTPDBContextPool.h"
#include "MTPServiceContext.h"
#include "MTPDBContextUtils.h"

/*  Local Defines
 */

#define MTPSERVICEINFOCONTEXTDATA_COOKIE        'scdI'
#define MTPSERVICEPROPDESCCONTEXTDATA_COOKIE    'scdP'
#define MTPSERVICEPROPLISTCTXDATA_COOKIE        'scdL'

enum
{
    MTPSERVICEINFO_SERVICE_ID = 0,
    MTPSERVICEINFO_SERVICE_STORAGEID,
    MTPSERVICEINFO_SERVICE_PUID,
    MTPSERVICEINFO_SERVICE_DATA,
    MTPSERVICEINFO_SERVICEUSES_LIST,
    MTPSERVICEINFO_SERVICEPROP_TEMPLATE,
    MTPSERVICEINFO_SERVICEPROP_LIST,
    MTPSERVICEINFO_FORMAT_TEMPLATE,
    MTPSERVICEINFO_FORMAT_LIST
};

enum
{
    MTPSERVICEPROPDESC_PROPERTY_LIST = 0,
    MTPSERVICEPROPDESC_COUNT,
    MTPSERVICEPROPDESC_PROPDESC
};

enum
{
    MTPSERVICEPROPLIST_COUNT = 0,
    MTPSERVICEPROPLIST_PROPERTY_LIST,
    MTPSERVICEPROPLIST_SERVICEID,
    MTPSERVICEPROPLIST_PROPVALUE
};

/*  Local Types
 */

typedef struct _MTPSERVICEINFOCONTEXTDATAOBJ
{
    MTPDBCONTEXTDATABASEOBJ             mtpCDB;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT32                           dwServiceID;
    PSLUINT32                           dwStorageID;
    PSLGUID                             guidServicePUID;
} MTPSERVICEINFOCONTEXTDATAOBJ;

typedef struct _MTPSERVICEPROPDESCCONTEXTDATAOBJ
{
    MTPDBCONTEXTDATABASEOBJ             mtpCDB;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT16                           wPropCode;
    PSL_CONST MTPSERVICEPROPERTYINFO*   pspiCur;
    MTPSERVICEPROPERTYINFO              spiCache;
} MTPSERVICEPROPDESCCONTEXTDATAOBJ;

typedef struct _MTPSERVICEPROPLISTCTXDATAOBJ
{
    MTPDBCONTEXTDATABASEOBJ             mtpCDB;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT32                           dwServiceID;
    PSLUINT32                           wPropCode;
    PSLUINT32                           idxProp;
    PSLUINT32                           cProps;
    PSL_CONST MTPSERVICEPROPERTYINFO*   pspiCur;
} MTPSERVICEPROPLISTCTXDATAOBJ;

/*  Local Functions
 */

static PSLSTATUS PSL_API _MTPServiceInfoCtxSerializeValue(
                    PSLPARAM aRootContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbBuf);

static PSLSTATUS PSL_API _MTPServiceInfoCtxGetCount(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT32* pcItems);

static PSLSTATUS PSL_API _MTPServiceInfoCtxGetArrayValue(
                    PSLUINT32 idxItem,
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS PSL_API _MTPServiceInfoCtxGetTemplate(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSL_CONST MTPSERIALIZEINFO** prgmsiTemplate,
                    PSLUINT32* pcItems,
                    PFNONMTPSERIALIZEDESTROY* ppfnOnDestroy);

static PSLSTATUS PSL_API _MTPServiceInfoCtxGetContext(
                    PSLUINT32 idxItem,
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLPARAM* paItemContext,
                    PSL_CONST PSLVOID** ppvItemOffset);

static PSLSTATUS PSL_API _MTPServiceInfoContextDataClose(
                    MTPSERVICECONTEXTDATA msdc);

static PSLSTATUS PSL_API _MTPServiceInfoContextDataSerialize(
                    MTPSERVICECONTEXTDATA msdc,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);

static PSLSTATUS PSL_API _MTPServicePropDescCtxSerializeValue(
                    PSLPARAM aRootContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbBuf);

static PSLSTATUS PSL_API _MTPServicePropDescCtxGetCount(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT32* pcItems);

static PSLSTATUS PSL_API _MTPServicePropDescCtxGetTemplate(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSL_CONST MTPSERIALIZEINFO** prgmsiTemplate,
                    PSLUINT32* pcItems,
                    PFNONMTPSERIALIZEDESTROY* ppfnOnDestroy);

static PSLSTATUS PSL_API _MTPServicePropDescCtxGetContext(
                    PSLUINT32 idxItem,
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLPARAM* paItemContext,
                    PSL_CONST PSLVOID** ppvItemOffset);

static PSLSTATUS PSL_API _MTPServicePropListCtxSerializeValue(
                    PSLPARAM aRootContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbBuf);

static PSLSTATUS PSL_API _MTPServicePropListCtxGetCount(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT32* pcItems);

static PSLSTATUS PSL_API _MTPServicePropListCtxGetTemplate(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSL_CONST MTPSERIALIZEINFO** prgmsiTemplate,
                    PSLUINT32* pcItems,
                    PFNONMTPSERIALIZEDESTROY* ppfnOnDestroy);

static PSLSTATUS PSL_API _MTPServicePropListCtxGetContext(
                    PSLUINT32 idxItem,
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLPARAM* paItemContext,
                    PSL_CONST PSLVOID** ppvItemOffset);

/*  Local Variables
 */

static PSL_CONST MTPSERIALIZEARRAYVTBL      _VtblSerializeSIArray =
{
    _MTPServiceInfoCtxGetCount,
    _MTPServiceInfoCtxGetArrayValue
};

static PSL_CONST MTPSERIALIZETEMPLATEVTBL   _VtblSerializeSITemplate =
{
    _MTPServiceInfoCtxGetTemplate,
    _MTPServiceInfoCtxGetContext
};

static PSL_CONST MTPSERIALIZETEMPLATEREPEATVTBL _VtblSerializeSITemplateRepeat =
{
    _MTPServiceInfoCtxGetTemplate,
    _MTPServiceInfoCtxGetContext,
    _MTPServiceInfoCtxGetCount
};

static PSL_CONST MTPSERIALIZETEMPLATEREPEATVTBL _VtblSerializeSPDTemplateRepeat =
{
    _MTPServicePropDescCtxGetTemplate,
    _MTPServicePropDescCtxGetContext,
    _MTPServicePropDescCtxGetCount
};

static PSL_CONST MTPSERIALIZETEMPLATEREPEATVTBL _VtblSerializeSPLTemplateRepeat =
{
    _MTPServicePropListCtxGetTemplate,
    _MTPServicePropListCtxGetContext,
    _MTPServicePropListCtxGetCount
};

static PSL_CONST MTPSERIALIZEINFO   _RgmsiServiceProp[] =
{
    /*  Service Property Code
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPSERVICEPROPERTYINFO, wPropCode),
        0
    },

    /*  Service Property Key
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_REFOFFSET |
            MTPSERIALIZEFLAG_PSLPKEY,
        PSLOFFSETOF(MTPSERVICEPROPERTYINFO, precProp),
        PSLOFFSETOF(MTPSERVICEPROPERTYREC, pkeyProp),
    },

    /*  Service Property Name
     */

    {
        MTP_DATATYPE_STRING,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_REFOFFSET,
        PSLOFFSETOF(MTPSERVICEPROPERTYINFO, precProp),
        PSLOFFSETOF(MTPSERVICEPROPERTYREC, szName),
    }
};

static PSL_CONST MTPSERIALIZEINFO   _RgmsiServicePropArray[] =
{
    /*  Service Property Count
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPSERVICEINFO, cServiceProps),
        0
    },

    /*  Service Property Information
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYTEMPLATEREPEAT,
        (PSLPARAM)&_VtblSerializeSITemplateRepeat,
        MTPSERVICEINFO_SERVICEPROP_TEMPLATE
    }
};

static PSL_CONST MTPSERIALIZEINFO   _RgmsiFormat[] =
{
    /*  Format Code
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPSERVICEFORMATINFO, infoFormat.wFormatCode),
        0
    },

    /*  Format GUID
     */

    {
        MTP_DATATYPE_UINT128,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_REFOFFSET |
                            MTPSERIALIZEFLAG_PSLGUID,
        PSLOFFSETOF(MTPSERVICEFORMATINFO, infoFormat.precFormat),
        PSLOFFSETOF(MTPSERVICEFORMATREC, pguidFormatGUID)
    },

    /*  Format Name
     */

    {
        MTP_DATATYPE_STRING,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_REFOFFSET,
        PSLOFFSETOF(MTPSERVICEFORMATINFO, infoFormat.precFormat),
        PSLOFFSETOF(MTPSERVICEFORMATREC, szFormatName),
    },

    /*  Base Format Code- Currently Reserved
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYVALUE,
        MTPSERVICE_BASE_FORMAT_CODE,
        0
    },

    /*  Format MIME Type
     */

    {
        MTP_DATATYPE_STRING,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_REFOFFSET,
        PSLOFFSETOF(MTPSERVICEFORMATINFO, infoFormat.precFormat),
        PSLOFFSETOF(MTPSERVICEFORMATREC, szMIMEType),
    }
};

static PSL_CONST MTPSERIALIZEINFO   _RgmsiFormatArray[] =
{
    /*  Format Count
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPSERVICEINFO, cServiceFormats),
        0
    },

    /*  Format Information
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYTEMPLATEREPEAT,
        (PSLPARAM)&_VtblSerializeSITemplateRepeat,
        MTPSERVICEINFO_FORMAT_TEMPLATE
    }
};


static PSL_CONST MTPSERIALIZEINFO   _RgmsiServiceInfo[] =
{
    /*  Service ID
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPServiceInfoCtxSerializeValue,
        MTPSERVICEINFO_SERVICE_ID
    },

    /*  Storage ID
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPServiceInfoCtxSerializeValue,
        MTPSERVICEINFO_SERVICE_STORAGEID
    },

    /*  Service PUID
     */

    {
        MTP_DATATYPE_UINT128,
        MTPSERIALIZEFLAG_BYFUNCTION | MTPSERIALIZEFLAG_PSLGUID,
        (PSLPARAM)_MTPServiceInfoCtxSerializeValue,
        MTPSERVICEINFO_SERVICE_PUID
    },

    /*  Service Version- Currently only version 1.0 is supported
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPSERVICEINFO, dwServiceVersion),
        0
    },

    /*  Service GUID
     */

    {
        MTP_DATATYPE_UINT128,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_PSLGUID,
        PSLOFFSETOF(MTPSERVICEINFO, pguidServiceGUID),
        0
    },

    /*  Service Name
     */

    {
        MTP_DATATYPE_STRING,
        MTPSERIALIZEFLAG_BYOFFSETREF,
        PSLOFFSETOF(MTPSERVICEINFO, szServiceName),
        0
    },

    /*  Service Flags
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPSERVICEINFO, dwServiceFlags),
        0
    },

    /*  Base Service ID- Reserved, must be 0 in current implementation
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYVALUE,
        MTPSERVICE_BASE_SERVICE_ID,
        0
    },

    /*  Uses Service PUIDs
     */

    {
        MTP_DATATYPE_AUINT128,
        MTPSERIALIZEFLAG_BYFUNCTION | MTPSERIALIZEFLAG_PSLGUID,
        (PSLPARAM)&_VtblSerializeSIArray,
        MTPSERVICEINFO_SERVICEUSES_LIST
    },

    /*  Service Properties
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYTEMPLATE,
        (PSLPARAM)&_VtblSerializeSITemplate,
        MTPSERVICEINFO_SERVICEPROP_LIST
    },

    /*  Service Formats
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYTEMPLATE,
        (PSLPARAM)&_VtblSerializeSITemplate,
        MTPSERVICEINFO_FORMAT_LIST
    },

    /*  Service Methods
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYVALUE,
        0,
        0
    },

    /*  Service Events
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYVALUE,
        0,
        0
    },

    /*  Service Data Size
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPSERVICEINFO, cbData),
        0
    },

    /*  Service Data
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPServiceInfoCtxSerializeValue,
        MTPSERVICEINFO_SERVICE_DATA
    }
};


static PSL_CONST MTPSERIALIZEINFO   _RgmsiServicePropDesc[] =
{
    /*  Service Service Property Description
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPServicePropDescCtxSerializeValue,
        MTPSERVICEPROPDESC_PROPDESC
    },
};

static PSL_CONST MTPSERIALIZEINFO   _RgmsiServicePropDescArray[] =
{
    /*  Count of Properties
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPServicePropDescCtxSerializeValue,
        MTPSERVICEPROPDESC_COUNT
    },

    /*  Property Information
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYTEMPLATEREPEAT,
        (PSLPARAM)&_VtblSerializeSPDTemplateRepeat,
        MTPSERVICEPROPDESC_PROPERTY_LIST
    },
};

static PSL_CONST MTPSERIALIZEINFO   _RgmsiServicePropListPropertyValue[] =
{
    /*  Service ID */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPServicePropListCtxSerializeValue,
        MTPSERVICEPROPLIST_SERVICEID
    },

    /*  Property Code */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPOBJECTPROPINFO, infoProp.wPropCode),
        0
    },

    /*  Property Type */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(MTPSERVICEPROPERTYINFO, precProp),
        PSLOFFSETOF(MTPPROPERTYREC, wDataType)
    },

    /*  Property Value */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPServicePropListCtxSerializeValue,
        MTPSERVICEPROPLIST_PROPVALUE
    },
};

static PSL_CONST MTPSERIALIZEINFO       _RgmsiServicePropList[] =
{
    /*  Number of objects */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPServicePropListCtxSerializeValue,
        MTPSERVICEPROPLIST_COUNT
    },

    /*  Property List */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYTEMPLATEREPEAT,
        (PSLPARAM)&_VtblSerializeSPLTemplateRepeat,
        MTPSERVICEPROPLIST_PROPERTY_LIST
    }
};




/*  MTPServiceGetStorageIDBase
 *
 *  Returns the storage ID for this service.  The base implementation always
 *  returns MTP_STORAGEID_UNDEFINED.
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLUINT32       Storage ID associated with this service
 */

PSLUINT32 PSL_API MTPServiceGetStorageIDBase(MTPSERVICE mtpService)
{
    PSLASSERT(PSLNULL != mtpService);
    return MTP_STORAGEID_UNDEFINED;
    mtpService;
}


/*  MTPServiceGetFormatsSupportedBase
 *
 *  Returns the list of formats supported by this service.  The base
 *  implementation always reports and returns zero entries.
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          MTP Service being used
 *      PSLUINT16*      pwFormats           Buffer to return formats in
 *      PSLUINT32       cFormats            Size of format buffer
 *      PSLUINT32*      pcFormats           Number of formats available
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServiceGetFormatsSupportedBase(MTPSERVICE mtpService,
                    PSLUINT16* pwFormats, PSLUINT32 cFormats,
                    PSLUINT32* pcFormats)
{
    /*  Clear results for safety
     */

    if (PSLNULL != pcFormats)
    {
        *pcFormats = 0;
    }

    /*  Validate arguments
     */

    return ((PSLNULL != mtpService) &&
            ((PSLNULL == pwFormats) || (0 != cFormats)) &&
            (PSLNULL != pcFormats)) ? PSLSUCCESS : PSLERROR_INVALID_PARAMETER;
}


/*  MTPServiceIsFormatSupportedBase
 *
 *  Returns PSLSUCCESS if format code is supported by service, PSLSUCCESS_FALSE
 *  if it is not.  The base implementation always returns not supported.
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          MTP service being used
 *      PSLUINT16       wFormat             Format being queried
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS  if supported, PSLSUCCESS_FALSE if not
 */

PSLSTATUS PSL_API MTPServiceIsFormatSupportedBase(MTPSERVICE mtpService,
                    PSLUINT16 wFormat)
{
    return (PSLNULL != mtpService) ?
                            PSLSUCCESS_FALSE : PSLERROR_INVALID_PARAMETER;
    wFormat;
}


/*  MTPServiceDBContextOpenBase
 *
 *  Opens the MTP Database Context associated with this object.  The base
 *  implementation always returns not implemented.
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          MTP service being used
 *      PSLUINT32       dwJoinType          How property array should be joined
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              MTP variant props defining context
 *      PSLUINT32       cmdbv               Number of MTP variants
 *      MTPDBCONTEXT*   pmtpDBContext       Resulting context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServiceDBContextOpenBase(MTPSERVICE mtpService,
                    PSLUINT32 dwJoinType, PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv, MTPDBCONTEXT* pmtpDBContext)
{
    /*  Clear result for safety
     */

    if (PSLNULL != pmtpDBContext)
    {
        *pmtpDBContext = PSLNULL;
    }

    /*  Validate arguments
     */

    return ((PSLNULL != mtpService) && (MTPJOINTYPE_AND == dwJoinType) &&
            ((PSLNULL == rgmdbv) || (0 != cmdbv))) ?
                    PSLERROR_NOT_IMPLEMENTED : PSLERROR_INVALID_PARAMETER;
}


/*  _MTPServiceInfoCtxSerializeValue
 *
 *  Returns the requested value in a serialize form
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context for this call
 *      PSLPARAM        aValueContext       Context for this value
 *      PSLUINT16       wType               Type to serialize
 *      PSLVOID*        pvBuf               Destination buffer
 *      PSLUINT32       cbBuf               Size of destination buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServiceInfoCtxSerializeValue(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT16 wType,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLSTATUS                       ps;
    MTPSERVICEINFOCONTEXTDATAOBJ*   psicdObj;
    PSL_CONST MTPSERVICEINFO*       pServiceInfo;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    psicdObj = (MTPSERVICEINFOCONTEXTDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEINFOCONTEXTDATA_COOKIE == psicdObj->dwCookie);
    pServiceInfo = (PSL_CONST MTPSERVICEINFO*)psicdObj->mtpCDB.pvData;

    /*  Determine what count is being returned
     */

    switch (aValueContext)
    {
    case MTPSERVICEINFO_SERVICE_ID:
    case MTPSERVICEINFO_SERVICE_STORAGEID:
        /*  Validate type
         */

        if (MTP_DATATYPE_UINT32 != wType)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        /*  Return length if requested
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = sizeof(PSLUINT32);
            ps = PSLSUCCESS;
            break;
        }

        /*  Validate that we have enough space to return the data
         */

        if (sizeof(PSLUINT32) > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  Return the value
         */

        MTPStoreUInt32((PXPSLUINT32)pvBuf,
                            (MTPSERVICEINFO_SERVICE_ID == aValueContext) ?
                                psicdObj->dwServiceID : psicdObj->dwStorageID);
        *pcbUsed = sizeof(PSLUINT32);
        ps = PSLSUCCESS;
        break;

    case MTPSERVICEINFO_SERVICE_PUID:
        /*  Validate type
         */

        if (MTP_DATATYPE_UINT128 != wType)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        /*  Return size if it was requested
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = sizeof(PSLUINT128);
            ps = PSLSUCCESS;
            break;
        }

        /*  Validate that we have enough space to return the data
         */

        if (sizeof(PSLUINT128) > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  Return the value
         */

        MTPStoreGUID((PXPSLGUID)pvBuf, &(psicdObj->guidServicePUID));
        *pcbUsed = sizeof(PSLUINT128);
        ps = PSLSUCCESS;
        break;

    case MTPSERVICEINFO_SERVICE_DATA:
        /*  Validate the type
         */

        if (MTP_DATATYPE_UNDEFINED != wType)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        /*  Return the size if it was requested
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = pServiceInfo->cbData;
            ps = PSLSUCCESS;
            break;
        }

        /*  Validate that we have enough space to return the data
         */

        if (cbBuf < pServiceInfo->cbData)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  And return the data itself
         */

        PSLCopyMemory(pvBuf, pServiceInfo->pbData, pServiceInfo->cbData);
        *pcbUsed = pServiceInfo->cbData;
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected value requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServiceInfoCtxGetCount
 *
 *  Returns the count of items to return or the number of times to repeat
 *  the specified template
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context to work within
 *      PSLPARAM        aValueContext       Context for the specific value
 *      PSLUINT32*      pcItems             Number of items/repeats to perform
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServiceInfoCtxGetCount(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT32* pcItems)
{
    PSLSTATUS                       ps;
    MTPSERVICEINFOCONTEXTDATAOBJ*   psicdObj;
    PSL_CONST MTPSERVICEINFO*       pServiceInfo;

    /*  Clear result for safety
     */

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcItems))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    psicdObj = (MTPSERVICEINFOCONTEXTDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEINFOCONTEXTDATA_COOKIE == psicdObj->dwCookie);
    pServiceInfo = (PSL_CONST MTPSERVICEINFO*)psicdObj->mtpCDB.pvData;

    /*  Determine what count is being returned
     */

    switch (aValueContext)
    {
    case MTPSERVICEINFO_SERVICEUSES_LIST:
        /*  Return the number of items in the service uses list
         */

        *pcItems = pServiceInfo->cUsesServicePUID;
        ps = PSLSUCCESS;
        break;

    case MTPSERVICEINFO_SERVICEPROP_TEMPLATE:
        /*  Return the number of items in the service property list
         */

        *pcItems = pServiceInfo->cServiceProps;
        ps = PSLSUCCESS;
        break;

    case MTPSERVICEINFO_FORMAT_TEMPLATE:
        /*  Return the number of items in the service format list
         */

        *pcItems = pServiceInfo->cServiceFormats;
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected property request
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServiceInfoCtxGetArrayValue
 *
 *  Returns an array value from the specified context
 *
 *  Arguments:
 *      PSLUINT32       idxItem             Index of item to return
 *      PSLPARAM        aContext            Context to operate within
 *      PSLPARAM        aValueContext       Value context
 *      PSLUINT16       wType               Type of data to be returned
 *      PSLVOID*        pvBuf               Destination for the data
 *      PSLUINT32       cbBuf               Size of the buffer
 *      PSLUINT32*      pcbUsed             Number of bytes returned
 *
 *  Results:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServiceInfoCtxGetArrayValue(PSLUINT32 idxItem,
                    PSLPARAM aContext, PSLPARAM aValueContext,
                    PSLUINT16 wType, PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed)
{
    PSLSTATUS                       ps;
    MTPSERVICEINFOCONTEXTDATAOBJ*   psicdObj;
    PSL_CONST MTPSERVICEINFO*       pServiceInfo;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    psicdObj = (MTPSERVICEINFOCONTEXTDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEINFOCONTEXTDATA_COOKIE == psicdObj->dwCookie);
    pServiceInfo = (PSL_CONST MTPSERVICEINFO*)psicdObj->mtpCDB.pvData;

    /*  Determine what count is being returned
     */

    switch (aValueContext)
    {
    case MTPSERVICEINFO_SERVICEUSES_LIST:
        /*  Validate the type
         */

        if (MTP_DATATYPE_UINT128 != wType)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        /*  See if only the size is requested
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = sizeof(PSLUINT128);
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure there is space to hold the item
         */

        if (sizeof(PSLGUID) > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  Make sure the index is in range
         */

        if (idxItem >= pServiceInfo->cUsesServicePUID)
        {
            ps = PSLERROR_INVALID_RANGE;
            goto Exit;
        }

        /*  All is well- store the GUID in the destination buffer
         */

        MTPStoreGUID((PXPSLGUID)pvBuf,
                        pServiceInfo->rgpguidUsesServicePUID[idxItem]);
        *pcbUsed = sizeof(PSLUINT128);
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unknown array item requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServiceInfoCtxGetTemplate
 *
 *  Returns the requested serialization template
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context to use in serialization
 *      PSLPARAM        aValueContext       Value context for template
 *      MTPSERIALIZEINFO**
 *                      prgmsiTemplate      Return location for new template
 *      PSLUINT32*      pcItems             Number of items in template
 *      PFNONMTPSERIALIZEDESTROY*
 *                      pfnOnDestroy        Function to call when template
 *                                            or item is to be release
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServiceInfoCtxGetTemplate(PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSL_CONST MTPSERIALIZEINFO** prgmsiTemplate,
                    PSLUINT32* pcItems,
                    PFNONMTPSERIALIZEDESTROY* ppfnOnDestroy)
{
    PSLSTATUS                       ps;
    MTPSERVICEINFOCONTEXTDATAOBJ*   psicdObj;

    /*  Clear result for safety
     */

    if (PSLNULL != prgmsiTemplate)
    {
        *prgmsiTemplate = PSLNULL;
    }
    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }
    if (PSLNULL != ppfnOnDestroy)
    {
        *ppfnOnDestroy = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == prgmsiTemplate) ||
        (PSLNULL == pcItems) || (PSLNULL == ppfnOnDestroy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    psicdObj = (MTPSERVICEINFOCONTEXTDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEINFOCONTEXTDATA_COOKIE == psicdObj->dwCookie);

    /*  Determine what template is being requested
     */

    switch (aValueContext)
    {
    case MTPSERVICEINFO_SERVICEPROP_LIST:
        *prgmsiTemplate = _RgmsiServicePropArray;
        *pcItems = PSLARRAYSIZE(_RgmsiServicePropArray);
        ps = PSLSUCCESS;
        break;

    case MTPSERVICEINFO_SERVICEPROP_TEMPLATE:
        *prgmsiTemplate = _RgmsiServiceProp;
        *pcItems = PSLARRAYSIZE(_RgmsiServiceProp);
        ps = PSLSUCCESS;
        break;

    case MTPSERVICEINFO_FORMAT_LIST:
        *prgmsiTemplate = _RgmsiFormatArray;
        *pcItems = PSLARRAYSIZE(_RgmsiFormatArray);
        ps = PSLSUCCESS;
        break;

    case MTPSERVICEINFO_FORMAT_TEMPLATE:
        *prgmsiTemplate = _RgmsiFormat;
        *pcItems = PSLARRAYSIZE(_RgmsiFormat);
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected template requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServiceInfoCtxGetContext
 *
 *  Returns contextual information for the requested item in a child
 *  serialization template
 *
 *  Arguments:
 *      PSLUINT32       idxItem             Item being requested
 *      PSLPARAM        aContext            Context for the request
 *      PSLPARAM        aValueContext       Context for the value requested
 *      PSLPARAM*       paItemContext       Context to use for this template
 *      PSLVOID**       ppvItemOffset       Root offset for this template
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServiceInfoCtxGetContext(PSLUINT32 idxItem,
                    PSLPARAM aContext, PSLPARAM aValueContext,
                    PSLPARAM* paItemContext, PSL_CONST PSLVOID** ppvItemOffset)
{
    PSLSTATUS                       ps;
    MTPSERVICEINFOCONTEXTDATAOBJ*   psicdObj;
    PSL_CONST MTPSERVICEINFO*       pServiceInfo;

    /*  Clear result for safety
     */

    if (PSLNULL != paItemContext)
    {
        *paItemContext = 0;
    }
    if (PSLNULL != ppvItemOffset)
    {
        *ppvItemOffset = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == paItemContext) ||
        (PSLNULL == ppvItemOffset))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    psicdObj = (MTPSERVICEINFOCONTEXTDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEINFOCONTEXTDATA_COOKIE == psicdObj->dwCookie);
    pServiceInfo = (PSL_CONST MTPSERVICEINFO*)psicdObj->mtpCDB.pvData;

    /*  Determine what template context is being requested
     */

    switch (aValueContext)
    {
    case MTPSERVICEINFO_SERVICEPROP_LIST:
    case MTPSERVICEINFO_FORMAT_LIST:
        /*  There is always just one instance of the context for the service
         *  property list and the format list
         */

        if (0 != idxItem)
        {
            ps = PSLERROR_INVALID_RANGE;
            goto Exit;
        }

        /*  Return the same context and use the service info as the offset
         */

        *paItemContext = aContext;
        *ppvItemOffset = pServiceInfo;
        ps = PSLSUCCESS;
        break;

    case MTPSERVICEINFO_SERVICEPROP_TEMPLATE:
        /*  Validate the range of the item requested
         */

        if (pServiceInfo->cServiceProps <= idxItem)
        {
            ps = PSLERROR_INVALID_RANGE;
            goto Exit;
        }

        /*  Return the same context but use the next service property entry
         *  as the offset data
         */

        *paItemContext = aContext;
        *ppvItemOffset = &(pServiceInfo->rgspiServiceProps[idxItem]);
        ps = PSLSUCCESS;
        break;

    case MTPSERVICEINFO_FORMAT_TEMPLATE:
        /*  Validate the range of the item requested
         */

        if (pServiceInfo->cServiceFormats <= idxItem)
        {
            ps = PSLERROR_INVALID_RANGE;
            goto Exit;
        }

        /*  Return the same context but use the next format entry as the
         *  offset data
         */

        *paItemContext = aContext;
        *ppvItemOffset = &(pServiceInfo->rgsfiServiceFormats[idxItem]);
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected template requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPServiceInfoContextDataCreate
 *
 *  Creates an instance of the MTP Service Info Context Data
 *
 *  Arguments:
 *      PSLUINT32       dwServiceID         Service ID for this service
 *      PSLUINT32       dwStorageID         Storage ID for this service
 *      PSLGUID*        pguidServicePUID    Persistent Servuce GUID
 *      MTPSERVICEINFO* pServiceInfo        Service Info to be serialized
 *      PSLPARAM        aParam              Callback function parameter
 *      PFNONMTPDBCONTEXTDATABASECLOSE
 *                      pfnOnClose          Callback function
 *      MTPSERVICECONTEXTDATA*
 *                      pmscd               Resulting service context data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServiceInfoContextDataCreate(PSLUINT32 dwServiceID,
                    PSLUINT32 dwStorageID, PSL_CONST PSLGUID* pguidServicePUID,
                    PSL_CONST MTPSERVICEINFO* pServiceInfo,
                    PSLPARAM aParam, PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPSERVICECONTEXTDATA* pmscd)
{
    PSLSTATUS                       ps;
    MTPSERVICEINFOCONTEXTDATAOBJ*   psicdObj = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmscd)
    {
        *pmscd = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((MTP_SERVICEID_UNDEFINED == dwServiceID) ||
        (PSLNULL == pguidServicePUID) || (PSLNULL == pServiceInfo) ||
        (PSLNULL == pmscd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  We only support serialization of v1 ServiceInfo datasets
     */

    if (MTPSERVICEINFO_VERSION_1 != pServiceInfo->dwServiceVersion)
    {
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;
    }

    /*  Allocate the base object
     */

    ps = MTPDBContextDataBaseCreate(sizeof(MTPSERVICEINFOCONTEXTDATAOBJ),
                            &psicdObj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Stamp the object for validation
     */

    psicdObj->dwCookie = MTPSERVICEINFOCONTEXTDATA_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Initialize the base object
     */

    psicdObj->mtpCDB.pvData = pServiceInfo;
    psicdObj->mtpCDB.pfnOnClose = pfnOnClose;
    psicdObj->mtpCDB.aParam = aParam;

    /*  And initialize our data
     */

    psicdObj->dwServiceID = dwServiceID;
    psicdObj->dwStorageID = dwStorageID;
    PSLCopyMemory(&(psicdObj->guidServicePUID), pguidServicePUID,
                            sizeof(psicdObj->guidServicePUID));

    /*  And allocate the serializer
     */

    ps = MTPSerializeCreate(_RgmsiServiceInfo, PSLARRAYSIZE(_RgmsiServiceInfo),
                    (PSLPARAM)psicdObj, psicdObj->mtpCDB.pvData,
                    PSLNULL, &(psicdObj->mtpCDB.msSource));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the service context data
     */

    *pmscd = psicdObj;
    psicdObj = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSERVICECONTEXTDATACLOSE(psicdObj);
    return ps;
}


/*  _MTPServicePropDescCtxSerializeValue
 *
 *  Returns the requested value in a serialized form
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context for this call
 *      PSLPARAM        aValueContext       Context for this value
 *      PSLUINT16       wType               Type to serialize
 *      PSLVOID*        pvBuf               Destination buffer
 *      PSLUINT32       cbBuf               Size of destination buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServicePropDescCtxSerializeValue(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT16 wType,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLSTATUS                           ps;
    MTPSERVICEPROPDESCCONTEXTDATAOBJ*   pspdcdObj;
    PSL_CONST MTPSERVICEINFO*           pServiceInfo;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pspdcdObj = (MTPSERVICEPROPDESCCONTEXTDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEPROPDESCCONTEXTDATA_COOKIE == pspdcdObj->dwCookie);
    pServiceInfo = (PSL_CONST MTPSERVICEINFO*)pspdcdObj->mtpCDB.pvData;

    /*  Determine what count is being returned
     */

    switch (aValueContext)
    {
    case MTPSERVICEPROPDESC_PROPDESC:
        /*  Validate type
         */

        if (MTP_DATATYPE_UNDEFINED != wType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Call the generic helper to do the real work
         */

        ps = MTPPropertyDescSerialize(pspdcdObj->pspiCur,
                            PROPFLAGS_TYPE_SERVICE, pspdcdObj->mtpCDB.aParam,
                            pvBuf, cbBuf, pcbUsed);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPSERVICEPROPDESC_COUNT:
        /*  Use the property requested to determine the number of properties
         *  that will be returned
         */

        if (MTP_DATATYPE_UINT32 != wType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Return the size of requested
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = sizeof(PSLUINT32);
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure there is space to store the value
         */

        if (sizeof(PSLUINT32) > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  And serialize the value- it will either be 1 (if one property
         *   requested) or the number of properties supported in ServiceInfo
         */

        MTPStoreUInt32((PXPSLUINT32)pvBuf,
                            (MTP_SERVICEPROPCODE_ALL == pspdcdObj->wPropCode) ?
                                    pServiceInfo->cServiceProps : 1);
        *pcbUsed = sizeof(PSLUINT32);
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected value requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServicePropDescCtxGetCount
 *
 *  Returns the count of items to return or the number of times to repeat
 *  the specified template
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context to work within
 *      PSLPARAM        aValueContext       Context for the specific value
 *      PSLUINT32*      pcItems             Number of items/repeats to perform
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServicePropDescCtxGetCount(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT32* pcItems)
{
    PSLSTATUS                           ps;
    MTPSERVICEPROPDESCCONTEXTDATAOBJ*   pspdcdObj;
    PSL_CONST MTPSERVICEINFO*           pServiceInfo;

    /*  Clear result for safety
     */

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcItems))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pspdcdObj = (MTPSERVICEPROPDESCCONTEXTDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEPROPDESCCONTEXTDATA_COOKIE == pspdcdObj->dwCookie);
    pServiceInfo = (PSL_CONST MTPSERVICEINFO*)pspdcdObj->mtpCDB.pvData;

    /*  Determine what count is being returned
     */

    switch (aValueContext)
    {
    case MTPSERVICEPROPDESC_PROPERTY_LIST:
        /*  Return the number of property descriptions to serialize- if a
         *  specific property has been requested this will be 1 otherwise
         *  it will be the number of service properties supported
         */

        *pcItems = (MTP_SERVICEPROPCODE_ALL == pspdcdObj->wPropCode) ?
                            pServiceInfo->cServiceProps : 1;
        ps = PSLSUCCESS;
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected property request
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServicePropDescCtxGetTemplate
 *
 *  Returns the requested serialization template
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context to use in serialization
 *      PSLPARAM        aValueContext       Value context for template
 *      MTPSERIALIZEINFO**
 *                      prgmsiTemplate      Return location for new template
 *      PSLUINT32*      pcItems             Number of items in template
 *      PFNONMTPSERIALIZEDESTROY*
 *                      pfnOnDestroy        Function to call when template
 *                                            or item is to be release
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServicePropDescCtxGetTemplate(PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSL_CONST MTPSERIALIZEINFO** prgmsiTemplate,
                    PSLUINT32* pcItems,
                    PFNONMTPSERIALIZEDESTROY* ppfnOnDestroy)
{
    PSLSTATUS                           ps;
    MTPSERVICEPROPDESCCONTEXTDATAOBJ*   pspdcdObj;

    /*  Clear result for safety
     */

    if (PSLNULL != prgmsiTemplate)
    {
        *prgmsiTemplate = PSLNULL;
    }
    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }
    if (PSLNULL != ppfnOnDestroy)
    {
        *ppfnOnDestroy = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == prgmsiTemplate) ||
        (PSLNULL == pcItems) || (PSLNULL == ppfnOnDestroy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pspdcdObj = (MTPSERVICEPROPDESCCONTEXTDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEPROPDESCCONTEXTDATA_COOKIE == pspdcdObj->dwCookie);

    /*  Determine what template is being requested
     */

    switch (aValueContext)
    {
    case MTPSERVICEPROPDESC_PROPERTY_LIST:
        *prgmsiTemplate = _RgmsiServicePropDesc;
        *pcItems = PSLARRAYSIZE(_RgmsiServicePropDesc);
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected template requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServicePropDescCtxGetContext
 *
 *  Returns contextual information for the requested item in a child
 *  serialization template
 *
 *  Arguments:
 *      PSLUINT32       idxItem             Item being requested
 *      PSLPARAM        aContext            Context for the request
 *      PSLPARAM        aValueContext       Context for the value requested
 *      PSLPARAM*       paItemContext       Context to use for this template
 *      PSLVOID**       ppvItemOffset       Root offset for this template
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServicePropDescCtxGetContext(PSLUINT32 idxItem,
                    PSLPARAM aContext, PSLPARAM aValueContext,
                    PSLPARAM* paItemContext, PSL_CONST PSLVOID** ppvItemOffset)
{
    PSLUINT32                           idxProp;
    PSLSTATUS                           ps;
    MTPSERVICEPROPDESCCONTEXTDATAOBJ*   pspdcdObj;
    PSL_CONST MTPSERVICEINFO*           pServiceInfo;

    /*  Clear result for safety
     */

    if (PSLNULL != paItemContext)
    {
        *paItemContext = 0;
    }
    if (PSLNULL != ppvItemOffset)
    {
        *ppvItemOffset = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == paItemContext) ||
        (PSLNULL == ppvItemOffset))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pspdcdObj = (MTPSERVICEPROPDESCCONTEXTDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEPROPDESCCONTEXTDATA_COOKIE == pspdcdObj->dwCookie);
    pServiceInfo = (PSL_CONST MTPSERVICEINFO*)pspdcdObj->mtpCDB.pvData;

    /*  Determine what template context is being requested
     */

    switch (aValueContext)
    {
    case MTPSERVICEPROPDESC_PROPERTY_LIST:
        /*  Determine if we are returning all properties or just one
         */

        if (MTP_SERVICEPROPCODE_ALL == pspdcdObj->wPropCode)
        {
            /*  Make sure we still have properties left to give
             */

            if (pServiceInfo->cServiceProps <= idxItem)
            {
                ps = PSLERROR_INVALID_RANGE;
                goto Exit;
            }

            /*  Return the next format
             */

            idxProp = idxItem;
        }
        else
        {
            /*  We only support a single request
             */

            if (0 != idxItem)
            {
                ps = PSLERROR_INVALID_RANGE;
                goto Exit;
            }

            /*  Locate the correct property
             */

            for (idxProp = 0;
                    (idxProp < pServiceInfo->cServiceProps) &&
                        ((pServiceInfo->rgspiServiceProps[idxProp].wPropCode) !=
                                    pspdcdObj->wPropCode);
                    idxProp++)
            {
            }

            /*  If we did not find a match report an error
             */

            if (pServiceInfo->cServiceProps <= idxProp)
            {
                ps = MTPERROR_SERVICE_INVALID_PROPCODE;
                goto Exit;
            }
        }

        /*  Determine whether or not we can use this item directly or if we
         *  need to pick up default information to use it
         */

        if (PSLNULL != pServiceInfo->rgspiServiceProps[idxProp].precProp)
        {
            /*  Information is complete- just use it directly
             */

            pspdcdObj->pspiCur = &(pServiceInfo->rgspiServiceProps[idxProp]);
        }
        else
        {
            /*  Information is incomplete- copy it to the cache so we can update
             *  it
             */

            PSLCopyMemory(&(pspdcdObj->spiCache),
                            &(pServiceInfo->rgspiServiceProps[idxProp]),
                            sizeof(pspdcdObj->spiCache));

            /*  And retrieve the default property entry
             */

            ps = MTPPropertyGetDefaultRec(pspdcdObj->spiCache.wPropCode,
                            PROPFLAGS_TYPE_SERVICE,
                            (PSL_CONST MTPPROPERTYREC**)
                                    &(pspdcdObj->spiCache.precProp));
            if (PSLSUCCESS != ps)
            {
                PSLASSERT(PSL_FAILED(ps));
                ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_DATA;
                goto Exit;
            }

            /*  Use the cache version of the property information
             */

            pspdcdObj->pspiCur = &(pspdcdObj->spiCache);
        }

        /*  Return a pointer to the property record
         */

        *paItemContext = aContext;
        *ppvItemOffset = pspdcdObj->pspiCur;
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected template requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPServicePropDescContextDataCreate
 *
 *  Creates an instance of the MTP Service Capabilities Context Data
 *
 *  Arguments:
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              MTP DB ContextData configuration
 *      PSLUINT32       cmdbv               Number of configuration args
 *      MTPSERVICEINFO* pServiceInfo        Service Info with format info
 *      PSLPARAM        aParam              Callback function parameter
 *      PFNONMTPDBCONTEXTDATABASECLOSE
 *                      pfnOnClose          Callback function
 *      MTPSERVICECONTEXTDATA*
 *                      pmscd               Resulting service context data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServicePropDescContextDataCreate(
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv, PSLUINT32 cmdbv,
                    PSL_CONST MTPSERVICEINFO* pServiceInfo, PSLPARAM aParam,
                    PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPSERVICECONTEXTDATA* pmscd)
{
    PSLUINT32                           idxProp;
    PSLSTATUS                           ps;
    MTPSERVICEPROPDESCCONTEXTDATAOBJ*   pspdcdObj = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmscd)
    {
        *pmscd = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pServiceInfo) || (PSLNULL == pmscd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate the base object
     */

    ps = MTPDBContextDataBaseCreate(sizeof(MTPSERVICEPROPDESCCONTEXTDATAOBJ),
                            &pspdcdObj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Stamp the object for validation
     */

    pspdcdObj->dwCookie = MTPSERVICEPROPDESCCONTEXTDATA_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Initialize the base object
     */

    pspdcdObj->mtpCDB.pvData = pServiceInfo;
    pspdcdObj->mtpCDB.pfnOnClose = pfnOnClose;
    pspdcdObj->mtpCDB.aParam = aParam;

    /*  And initialize our data
     */

    for (idxProp = 0; idxProp < cmdbv; idxProp++)
    {
        /*  Check the configuration properties we were handed
         */

        switch (rgmdbv[idxProp].wPropCode)
        {
        case MTP_SERVICEPROPCODE_PROPCODE:
            /*  Validate type
             */

            if (MTP_DATATYPE_UINT16 != rgmdbv[idxProp].wDataType)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  Extract the format code to return- it may also be all
             */

            pspdcdObj->wPropCode = rgmdbv[idxProp].vParam.valUInt16;
            break;

        case MTP_OBJECTPROPCODE_SERVICEID:
            /*  Ignore the Service ID
             */

            break;

        default:
            PSLTraceError("MTPServicePropDescContextDataCreate: Ignoring prop 0x%04x",
                            rgmdbv[idxProp].wPropCode);
            break;
        }
    }

    /*  And allocate the serializer
     */

    ps = MTPSerializeCreate(_RgmsiServicePropDescArray,
                    PSLARRAYSIZE(_RgmsiServicePropDescArray),
                    (PSLPARAM)pspdcdObj, pspdcdObj->mtpCDB.pvData,
                    PSLNULL, &(pspdcdObj->mtpCDB.msSource));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the service context data
     */

    *pmscd = pspdcdObj;
    pspdcdObj = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSERVICECONTEXTDATACLOSE(pspdcdObj);
    return ps;
}


/*  MTPServiceCapsContextDataCreate
 *
 *  Creates an instance of the MTP Service Capabilities Context Data
 *
 *  Arguments:
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              MTP DB ContextData configuration
 *      PSLUINT32       cmdbv               Number of configuration args
 *      MTPSERVICEINFO* pServiceInfo        Service Info with format info
 *      PSLPARAM        aParam              Callback function parameter
 *      PFNONMTPDBCONTEXTDATABASECLOSE
 *                      pfnOnClose          Callback function
 *      MTPSERVICECONTEXTDATA*
 *                      pmscd               Resulting service context data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServiceCapsContextDataCreate(
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv, PSLUINT32 cmdbv,
                    PSL_CONST MTPSERVICEINFO* pServiceInfo, PSLPARAM aParam,
                    PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPSERVICECONTEXTDATA* pmscd)
{
    PSLUINT32       idxProp;
    PSLSTATUS       ps;
    PSLUINT16       wFormat;

    /*  Clear result for safety
     */

    if (PSLNULL != pmscd)
    {
        *pmscd = PSLNULL;
    }

    /*  Validate arguments
     */

    if (((0 < cmdbv) && (PSLNULL == rgmdbv)) || (PSLNULL == pServiceInfo) ||
        (PSLNULL == pmscd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Figure out the desired format code
     */

    for (idxProp = 0, wFormat = MTP_FORMATCODE_NOTUSED;
            idxProp < cmdbv;
            idxProp++)
    {
        /*  Check the configuration properties we were handed
         */

        switch (rgmdbv[idxProp].wPropCode)
        {
        case MTP_OBJECTPROPCODE_OBJECTFORMAT:
            /*  Validate type- it should be a UInt32 since it is a parameter
             *  value
             */

            if (MTP_DATATYPE_UINT16 != rgmdbv[idxProp].wDataType)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  Extract the format code to return- it may also be all
             */

            wFormat = rgmdbv[idxProp].vParam.valUInt16;
            break;

        case MTP_OBJECTPROPCODE_SERVICEID:
            /*  Ignore the Service ID
             */

            break;

        default:
            PSLTraceError("MTPServiceCapsContextDataCreate: Ignoring prop 0x%04x",
                            rgmdbv[idxProp].wPropCode);
            break;
        }
    }

    /*  And call the core format capabilities implementation to handle the
     *  real dirty work
     */

    ps = MTPObjectCapsContextDataCreate((PSL_CONST MTPFORMATINFO*)
                                    pServiceInfo->rgsfiServiceFormats,
                            pServiceInfo->cServiceFormats,
                            pServiceInfo->rgsmiServiceMethods,
                            pServiceInfo->cServiceMethods,
                            MAKE_MTPDBCONTEXTDATADESC(MTPDBCONTEXTDATA_QUERY,
                                    MTPDBCONTEXTDATAFORMAT_SERVICECAPABILITIES),
                            wFormat, aParam, pfnOnClose, pmscd);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServicePropListCtxSerializeValue
 *
 *  Returns the requested value in a serialized form
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context for this call
 *      PSLPARAM        aValueContext       Context for this value
 *      PSLUINT16       wType               Type to serialize
 *      PSLVOID*        pvBuf               Destination buffer
 *      PSLUINT32       cbBuf               Size of destination buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */


PSLSTATUS PSL_API _MTPServicePropListCtxSerializeValue(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT16 wType,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLSTATUS                       ps;
    MTPSERVICEPROPLISTCTXDATAOBJ*   psplcdObj;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    psplcdObj = (MTPSERVICEPROPLISTCTXDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEPROPLISTCTXDATA_COOKIE == psplcdObj->dwCookie);

    /*  Determine what count is being returned
     */

    switch (aValueContext)
    {
    case MTPSERVICEPROPLIST_COUNT:
    case MTPSERVICEPROPLIST_SERVICEID:
        /*  Use the format requested to determine the number of formats that
         *  will be returned
         */

        if (MTP_DATATYPE_UINT32 != wType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Return the size if requested
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = sizeof(PSLUINT32);
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure there is space to store the value
         */

        if (sizeof(PSLUINT32) > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  And serialize the value- we actually counted the number of
         *  properties that we have available so return it
         */

        switch (aValueContext)
        {
        case MTPSERVICEPROPLIST_COUNT:
            MTPStoreUInt32((PXPSLUINT32)pvBuf, psplcdObj->cProps);
            break;

        case MTPSERVICEPROPLIST_SERVICEID:
            MTPStoreUInt32((PXPSLUINT32)pvBuf, psplcdObj->dwServiceID);
            break;

        default:
            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }
        *pcbUsed = sizeof(PSLUINT32);
        ps = PSLSUCCESS;
        break;

    case MTPSERVICEPROPLIST_PROPVALUE:
        /*  Make sure we have a function to encode the value and that the
         *  we are being asked for an undefined type
         */

        if ((PSLNULL == psplcdObj->pspiCur->pfnMTPPropGetValue) ||
            (MTP_DATATYPE_UNDEFINED != wType))
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Call directly to ask for the information
         */

        ps = psplcdObj->pspiCur->pfnMTPPropGetValue(
                            psplcdObj->pspiCur->wPropCode,
                            psplcdObj->pspiCur->precProp->wDataType,
                            psplcdObj->mtpCDB.aParam,
                            psplcdObj->pspiCur->aContextProp,
                            pvBuf, cbBuf, pcbUsed);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  If we are just doing a length check then return what we recieved
         */

        if (PSLNULL == pvBuf)
        {
            break;
        }

        /*  We did not get data back- we are either looking for a length that
         *  we cannot guess or the data has changed and the property is no
         *  longer available- in either case fail the operation so that we
         *  do not return invalid data
         */

        if (0 == *pcbUsed)
        {
            /*  When we selected this property it had a value but no longer
             *  does- go ahead and fail the serialization indicating that a
             *  property is in error
             */

            PSLASSERT(PSLFALSE);
            ps = MTPERROR_PROPERTY_INVALID_PROPVALUE;
            goto Exit;
        }
        break;

    default:
        /*  Unexpected value requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServicePropListCtxGetCount
 *
 *  Returns the count of items to return or the number of times to repeat
 *  the specified template
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context to work within
 *      PSLPARAM        aValueContext       Context for the specific value
 *      PSLUINT32*      pcItems             Number of items/repeats to perform
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServicePropListCtxGetCount(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT32* pcItems)
{
    PSLSTATUS                       ps;
    MTPSERVICEPROPLISTCTXDATAOBJ*   psplcdObj;

    /*  Clear result for safety
     */

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcItems))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    psplcdObj = (MTPSERVICEPROPLISTCTXDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEPROPLISTCTXDATA_COOKIE == psplcdObj->dwCookie);

    /*  Determine what count is being returned
     */

    switch (aValueContext)
    {
    case MTPSERVICEPROPLIST_PROPERTY_LIST:
        /*  Since we do not know how many properties we actually have
         *  available we always assume this is an infinite release list- we
         *  may have none, we may have them all
         */

        *pcItems = MTPSERIALIZE_INFINITE_REPEAT;
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected property request
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServicePropListCtxGetTemplate
 *
 *  Returns the requested serialization template
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context to use in serialization
 *      PSLPARAM        aValueContext       Value context for template
 *      MTPSERIALIZEINFO**
 *                      prgmsiTemplate      Return location for new template
 *      PSLUINT32*      pcItems             Number of items in template
 *      PFNONMTPSERIALIZEDESTROY*
 *                      pfnOnDestroy        Function to call when template
 *                                            or item is to be release
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServicePropListCtxGetTemplate(PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSL_CONST MTPSERIALIZEINFO** prgmsiTemplate,
                    PSLUINT32* pcItems,
                    PFNONMTPSERIALIZEDESTROY* ppfnOnDestroy)
{
    PSLSTATUS                       ps;
    MTPSERVICEPROPLISTCTXDATAOBJ*   psplcdObj;

    /*  Clear result for safety
     */

    if (PSLNULL != prgmsiTemplate)
    {
        *prgmsiTemplate = PSLNULL;
    }
    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }
    if (PSLNULL != ppfnOnDestroy)
    {
        *ppfnOnDestroy = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == prgmsiTemplate) ||
        (PSLNULL == pcItems) || (PSLNULL == ppfnOnDestroy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    psplcdObj = (MTPSERVICEPROPLISTCTXDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEPROPLISTCTXDATA_COOKIE == psplcdObj->dwCookie);

    /*  Determine what template is being requested
     */

    switch (aValueContext)
    {
    case MTPSERVICEPROPLIST_PROPERTY_LIST:
        *prgmsiTemplate = _RgmsiServicePropListPropertyValue;
        *pcItems = PSLARRAYSIZE(_RgmsiServicePropListPropertyValue);
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected template requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServicePropListCtxGetContext
 *
 *  Returns contextual information for the requested item in a child
 *  serialization template
 *
 *  Arguments:
 *      PSLUINT32       idxItem             Item being requested
 *      PSLPARAM        aContext            Context for the request
 *      PSLPARAM        aValueContext       Context for the value requested
 *      PSLPARAM*       paItemContext       Context to use for this template
 *      PSLVOID**       ppvItemOffset       Root offset for this template
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServicePropListCtxGetContext(PSLUINT32 idxItem,
                    PSLPARAM aContext, PSLPARAM aValueContext,
                    PSLPARAM* paItemContext, PSL_CONST PSLVOID** ppvItemOffset)
{
    PSLUINT32                           cbData;
    PSLBOOL                             fFound;
    PSLUINT32                           idxProp;
    PSLSTATUS                           ps;
    PSL_CONST MTPSERVICEPROPERTYINFO*   pspiCur = PSLNULL;
    MTPSERVICEPROPLISTCTXDATAOBJ*       psplcdObj;
    PSL_CONST MTPSERVICEINFO*           pServiceInfo;

    /*  Clear result for safety
     */

    if (PSLNULL != paItemContext)
    {
        *paItemContext = 0;
    }
    if (PSLNULL != ppvItemOffset)
    {
        *ppvItemOffset = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == paItemContext) ||
        (PSLNULL == ppvItemOffset))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    psplcdObj = (MTPSERVICEPROPLISTCTXDATAOBJ*)aContext;
    PSLASSERT(MTPSERVICEPROPLISTCTXDATA_COOKIE == psplcdObj->dwCookie);
    pServiceInfo = (PSL_CONST MTPSERVICEINFO*)psplcdObj->mtpCDB.pvData;

    /*  Determine what template context is being requested
     */

    switch (aValueContext)
    {
    case MTPSERVICEPROPLIST_PROPERTY_LIST:
        /*  Reset the index counter if this is a request for the first
         *  property in the list
         */

        if (0 == idxItem)
        {
            psplcdObj->idxProp = 0;
        }

        /*  We want to loop until we find a matching property that has a
         *  value
         */

        fFound = PSLFALSE;
        do
        {
            /*  We repeat the list infinitely- if we have already examined all
             *  the properties then stop.  Note that if we are asking for the
             *  second property and we are returning a specific property we
             *  are also done.
             */

            if (((MTP_SERVICEPROPCODE_ALL != psplcdObj->wPropCode) &&
                                (0 < idxItem)) ||
                (pServiceInfo->cServiceProps <= psplcdObj->idxProp))
            {
                break;
            }

            /*  See if we are looking for a specific property
             */

            if (MTP_SERVICEPROPCODE_ALL != psplcdObj->wPropCode)
            {
                /*  Walk through the list and look for a match
                 */

                for (idxProp = 0;
                        (idxProp < pServiceInfo->cServiceProps) &&
                            (psplcdObj->wPropCode !=
                                pServiceInfo->rgspiServiceProps[idxProp].wPropCode);
                        idxProp++)
                {
                }

                /*  If we did not find a match report failure
                 */

                if (pServiceInfo->cServiceProps <= idxProp)
                {
                    ps = MTPERROR_SERVICE_INVALID_PROPCODE;
                    goto Exit;
                }
            }
            else
            {
                /*  Pull the next index to check out of the structure since we
                 *  skip empty properties
                 */

                idxProp = psplcdObj->idxProp;
                psplcdObj->idxProp++;
            }

            /*  Save some redirection
             */

            pspiCur = &(pServiceInfo->rgspiServiceProps[idxProp]);

            /*  Make sure we have all the information we need- and remember
             *  that service properties do not support default property
             *  records
             */

            if ((PSLNULL == pspiCur->precProp) ||
                (PSLNULL == pspiCur->pfnMTPPropGetValue))
            {
                ps = PSLERROR_INVALID_DATA;
                goto Exit;
            }

            /*  Call the serialization function to see if we have data
             */

            ps = pspiCur->pfnMTPPropGetValue(pspiCur->wPropCode,
                            pspiCur->precProp->wDataType,
                            psplcdObj->mtpCDB.aParam, pspiCur->aContextProp,
                            PSLNULL, 0, &cbData);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  If data is available then we are done looking for a property
             *  or if we are looking for a specific property and it is not
             *  there then we are done
             */

            fFound = (0 != cbData);
        }
        while (!fFound && (MTP_OBJECTPROPCODE_ALL == psplcdObj->wPropCode));

        /*  If we did not find a match than we are done looking
         */

        if ((!fFound) || (PSLNULL == pspiCur))
        {
            ps = PSLSUCCESS_NOT_AVAILABLE;
            break;
        }

        /*  Remember the property information
         */

        psplcdObj->pspiCur = pspiCur;
        psplcdObj->cProps++;

        /*  And return the context information
         */

        *paItemContext = (PSLPARAM)psplcdObj;
        *ppvItemOffset = psplcdObj->pspiCur;
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected template requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPServicePropListContextDataCreate
 *
 *  Creates an instance of the MTP Service PropList Context Data
 *
 *  Arguments:
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              MTP DB ContextData configuration
 *      PSLUINT32       cmdbv               Number of configuration args
 *      MTPSERVICEINFO* pServiceInfo        Service Info with format info
 *      PSLPARAM        aParam              Callback function parameter
 *      PFNONMTPDBCONTEXTDATABASECLOSE
 *                      pfnOnClose          Callback function
 *      MTPSERVICECONTEXTDATA*
 *                      pmscd               Resulting service context data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServicePropListContextDataCreate(
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv, PSLUINT32 cmdbv,
                    PSL_CONST MTPSERVICEINFO* pServiceInfo, PSLPARAM aParam,
                    PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPSERVICECONTEXTDATA* pmscd)
{
    PSLUINT32                       idxProp;
    PSLSTATUS                       ps;
    MTPSERVICEPROPLISTCTXDATAOBJ*   psplcdObj = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmscd)
    {
        *pmscd = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pServiceInfo) || (PSLNULL == pmscd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate the base object
     */

    ps = MTPDBContextDataBaseCreate(sizeof(MTPSERVICEPROPLISTCTXDATAOBJ),
                            &psplcdObj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Stamp the object for validation
     */

    psplcdObj->dwCookie = MTPSERVICEPROPLISTCTXDATA_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Initialize the base object
     */

    psplcdObj->mtpCDB.pvData = pServiceInfo;
    psplcdObj->mtpCDB.pfnOnClose = pfnOnClose;
    psplcdObj->mtpCDB.aParam = aParam;

    /*  And initialize our data
     */

    for (idxProp = 0; idxProp < cmdbv; idxProp++)
    {
        /*  Check the configuration properties we were handed
         */

        switch (rgmdbv[idxProp].wPropCode)
        {
        case MTP_SERVICEPROPCODE_PROPCODE:
            /*  Validate type- it should be a UInt32 since it is a parameter
             *  value
             */

            if (MTP_DATATYPE_UINT16 != rgmdbv[idxProp].wDataType)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  Extract the format code to return- it may also be all
             */

            psplcdObj->wPropCode = rgmdbv[idxProp].vParam.valUInt16;
            break;

        case MTP_OBJECTPROPCODE_SERVICEID:
            /*  Validate the type
             */

            if (MTP_DATATYPE_UINT32 != rgmdbv[idxProp].wDataType)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  Store the service ID so that we can emit it as part of the
             *  dataset
             */

            psplcdObj->dwServiceID = rgmdbv[idxProp].vParam.valUInt32;
            break;

        default:
            PSLTraceError("MTPServicePropListContextDataCreate: Ignoring prop 0x%04x",
                            rgmdbv[idxProp].wPropCode);
            break;
        }
    }

    /*  And allocate the serializer
     */

    ps = MTPSerializeCreate(_RgmsiServicePropList,
                            PSLARRAYSIZE(_RgmsiServicePropList),
                            (PSLPARAM)psplcdObj, psplcdObj->mtpCDB.pvData,
                            PSLNULL, &(psplcdObj->mtpCDB.msSource));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the service context data
     */

    *pmscd = psplcdObj;
    psplcdObj = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSERVICECONTEXTDATACLOSE(psplcdObj);
    return ps;
}




