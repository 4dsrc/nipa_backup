/*
 *  MTPServiceBase.c
 *
 *  Implements the generic base service implementation that integrates with
 *  the MTPServiceContext base implementations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPServiceContext.h"
#include "DeviceServices.h"

/*  Local Defines
 */

#define MTPSERVICEBASEOBJ_COOKIE        'srvB'
#define MTPDBSERVICEBASEOBJ_COOKIE      'srvD'

/*  Local Types
 */

/*  Local Functions
 */

/*  Local Variables
 */

static PSL_CONST MTPSERVICEVTBL         _VtblMTPServiceBase =
{
    MTPServiceBaseClose,
    MTPServiceBaseGetServiceID,
    MTPServiceBaseGetStorageID,
    MTPServiceBaseGetFormatsSupported,
    MTPServiceBaseIsFormatSupported,
    MTPServiceBaseContextDataOpen,
    MTPServiceDBContextOpenBase,
};


static PSL_CONST MTPDBSERVICEBASEVTBL   _VtblMTPDBServiceBase =
{
    MTPDBServiceBaseClose,
    MTPServiceBaseGetServiceID,
    MTPServiceBaseGetStorageID,
    MTPServiceBaseGetFormatsSupported,
    MTPServiceBaseIsFormatSupported,
    MTPServiceBaseContextDataOpen,
    MTPDBServiceBaseDBContextOpen,
    MTPDBServiceBaseGetFreeSpaceBase
};

static PSL_CONST MTPDBCONTEXTBASEVTBL   _VtblMTPDBServiceBaseDBContextBase =
{
    MTPDBServiceBaseDBContextClose,
    MTPDBContextCancelBase,
    MTPDBContextGetCountBase,
    MTPDBContextGetHandleBase,
    MTPDBContextCommitBase,
    MTPDBContextCopyBase,
    MTPDBContextMoveBase,
    MTPDBContextDeleteBase,
    MTPDBContextFormatBase,
    MTPDBServiceBaseDBContextDataOpen,
    MTPDBContextBaseGetItemBase,
    MTPDBContextBaseCloseItemBase,
    MTPDBContextBaseCommitItemBase,
    MTPDBContextBaseIsPropSupportedBase
};

static PSL_CONST MTPDBCONTEXTDATAVTBL   _VtblMTPDBServiceBaseDBContextData =
{
    MTPDBServiceBaseDBContextDataClose,
    MTPDBContextDataCancelBase,
    MTPDBContextDataSerializeBase,
    MTPDBContextDataStartDeserializeBase,
    MTPDBContextDataDeserializeBase
};


/*  MTPServiceInfoGetFormatsSupported
 *
 *  Returns a list of the formats supported by this MTPSERVICEINFO.
 *
 *  Arguments:
 *      MTPSERVICEINFO* pServiceInfo        Service info dataset to use
 *      PSLUINT16*      pwFormats           Return buffer for supported formats
 *      PSLUINT32       cFormats            Size of the format buffer
 *      PSLUINT32*      pcFormats           Number of formats returned
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServiceInfoGetFormatsSupported(
                    PSL_CONST MTPSERVICEINFO* pServiceInfo,
                    PSLUINT16* pwFormats, PSLUINT32 cFormats,
                    PSLUINT32* pcFormats)
{
    PSLUINT32           idxFormat;
    PSLSTATUS           ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcFormats)
    {
        *pcFormats = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == pServiceInfo) || (PSLNULL == pcFormats))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If we are just being asked for the number of formats then return it
     */

    if (PSLNULL == pwFormats)
    {
        *pcFormats = pServiceInfo->cServiceFormats;
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Make sure we have enough space to return all the formats
     */

    if (cFormats < pServiceInfo->cServiceFormats)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Walk through and copy each of the format codes supported by this
     *  service into the output buffer
     */

    for (idxFormat = 0;
            idxFormat < pServiceInfo->cServiceFormats;
            idxFormat++, pwFormats++)
    {
        *pwFormats =
            pServiceInfo->rgsfiServiceFormats[idxFormat].infoFormat.wFormatCode;
    }
    *pcFormats = pServiceInfo->cServiceFormats;
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPServiceInfoIsFormatSupported
 *
 *  Walks through the list of supported formats to determine if the specified
 *  format is supported by this MTPSERVICEINFO.
 *
 *  Arguments:
 *      MTPSERVICEINFO* pServiceInfo        Service info dataset to use
 *      PSLUINT16       wFormat             Format to query
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if matches, PSLSUCCESS_FALSE if not
 */

PSLSTATUS PSL_API MTPServiceInfoIsFormatSupported(
                    PSL_CONST MTPSERVICEINFO* pServiceInfo, PSLUINT16 wFormat)
{
    PSLUINT32                       idxFormat;
    PSLSTATUS                       ps;
    PSL_CONST MTPSERVICEFORMATINFO* rgsfi;

    /*  Validate Arguments
     */

    if (PSLNULL == pServiceInfo)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Walk through and copy each of the format codes supported by this
     *  service to check for a match
     */

    for (idxFormat = 0, rgsfi = pServiceInfo->rgsfiServiceFormats;
            (idxFormat < pServiceInfo->cServiceFormats) &&
                    (wFormat != rgsfi[idxFormat].infoFormat.wFormatCode);
            idxFormat++)
    {
    }
    ps = (idxFormat != pServiceInfo->cServiceFormats) ?
                            PSLSUCCESS : PSLSUCCESS_FALSE;

Exit:
    return ps;
}


/*  MTPServiceInfoContextDataOpen
 *
 *  Opens a ServiceContextData object based on the information provided
 *  in the MTPSERVICEINFO.
 *
 *  Arguments:
 *      MTPSERVICEINFO* pServiceInfo        Service info dataset to use
 *      PSLUINT32       dwServiceID         Service ID for the service
 *      PSLUINT32       dwStorageID         Storage ID for the service
 *      PSLGUID*        pguidServicePUID    Service PUID
 *      PSLUINT32       dwDataFormat        Data being requested
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              Context information
 *      PSLUINT32       cmdbv               Number of contexts
 *      PSLPARAM        aParam              Callback function parameter
 *      PFNONMTPDBCONTEXTDATABASECLOSE
 *                      pfnOnClose          Callback function
 *      PSLUINT64*      pqwBufSize          Size of the buffer needed to
 *                                            hold this dataset
 *      MTPSERVICECONTEXTDATA*
 *                      pmscd               Resulting context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServiceInfoContextDataOpen(
                    PSL_CONST MTPSERVICEINFO* pServiceInfo,
                    PSLUINT32 dwServiceID, PSLUINT32 dwStorageID,
                    PSL_CONST PSLGUID* pguidServicePUID,
                    PSLUINT32 dwDataFormat, PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv, PSLPARAM aParam,
                    PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    PSLUINT64* pqwBufSize, MTPSERVICECONTEXTDATA* pmscd)
{
    PSLSTATUS               ps;
    MTPSERVICECONTEXTDATA   mscdResult = PSLNULL;

    /*  Clear results for safety
     */

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }
    if (PSLNULL != pmscd)
    {
        *pmscd = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == pServiceInfo) || (MTP_SERVICEID_UNDEFINED == dwServiceID) ||
        (PSLNULL == pmscd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure that the Service ID we were handed matches the one in
     *  the context list
     */

    if ((1 > cmdbv) ||
        (MTP_OBJECTPROPCODE_SERVICEID != rgmdbv[0].wPropCode) ||
        (MTP_DATATYPE_UINT32 != rgmdbv[0].wDataType) ||
        (rgmdbv[0].vParam.valUInt32 != dwServiceID))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    switch (MTPDBCONTEXTDATADESC_FORMAT(dwDataFormat))
    {
    case MTPDBCONTEXTDATAFORMAT_SERVICEINFO:
        /*  Only query is supported for service info
         */

        if (MTPDBCONTEXTDATA_QUERY != MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }

        /*  Make sure we have a service PUID
         */

        if (PSLNULL == pguidServicePUID)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        /*  Create the context data based on the service info
         */

        ps = MTPServiceInfoContextDataCreate(dwServiceID, dwStorageID,
                            pguidServicePUID, pServiceInfo, aParam, pfnOnClose,
                            &mscdResult);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPDBCONTEXTDATAFORMAT_SERVICECAPABILITIES:
        /*  Not supported on abstract services or if no formats/methods are
         *  present and only supported for query
         */

        if ((DEVSVCTYPE_ABSTRACT && pServiceInfo->dwServiceFlags) ||
            ((0 == pServiceInfo->cServiceFormats) &&
                            (0 == pServiceInfo->cServiceMethods)) ||
            (MTPDBCONTEXTDATA_QUERY != MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat)))
        {
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }

        /*  Create a service context data for this request
         */

        ps = MTPServiceCapsContextDataCreate(rgmdbv, cmdbv, pServiceInfo,
                            aParam, pfnOnClose, &mscdResult);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPDBCONTEXTDATAFORMAT_SERVICEPROPERTYDESC:
        /*  Not supported on abstract services of if no properties are
         *  available and only supported for query
         */

        if ((DEVSVCTYPE_ABSTRACT && pServiceInfo->dwServiceFlags) ||
            (0 == pServiceInfo->cServiceProps) ||
            (MTPDBCONTEXTDATA_QUERY != MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat)))
        {
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }

        /*  Create a service context data for this request
         */

        ps = MTPServicePropDescContextDataCreate(rgmdbv, cmdbv, pServiceInfo,
                            aParam, pfnOnClose, &mscdResult);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPDBCONTEXTDATAFORMAT_SERVICEPROPLIST:
        /*  Not supported for abstract services or if no properties are
         *  available
         */

        if ((DEVSVCTYPE_ABSTRACT && pServiceInfo->dwServiceFlags) ||
            (0 == pServiceInfo->cServiceProps))
        {
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }

        /*  Determine what mode we are operating in
         */

        switch (MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
        case MTPDBCONTEXTDATA_QUERY:
            /*  Create a service context data for this request
             */

            ps = MTPServicePropListContextDataCreate(rgmdbv, cmdbv,
                                pServiceInfo, aParam, pfnOnClose, &mscdResult);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            break;

        case MTPDBCONTEXTDATA_INSERT:
        case MTPDBCONTEXTDATA_UPDATE:
        case MTPDBCONTEXTDATA_DELETE:
        default:
            PSLASSERT(PSLFALSE);
            ps = PSLERROR_NOT_IMPLEMENTED;
            goto Exit;
        }
        break;

    default:
        /*  Unsupported context requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If we were asked to return the length calculate it now
     */

    if (PSLNULL != pqwBufSize)
    {
        ps = MTPServiceContextDataSerialize(mscdResult, PSLNULL, 0, PSLNULL,
                            pqwBufSize);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Return the created context
     */

    *pmscd = mscdResult;
    mscdResult = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSERVICECONTEXTDATACLOSE(mscdResult);
    return ps;
}


/*  MTPServiceBaseClose
 *
 *  Closes the MTPSERVICEBASEOBJ based service.
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          Service to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServiceBaseClose(MTPSERVICE mtpService)
{
    PSLSTATUS           ps;
    MTPSERVICEBASEOBJ*  pmtpsb;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpService)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpsb = (MTPSERVICEBASEOBJ*)mtpService;
    PSLASSERT(MTPSERVICEBASEOBJ_COOKIE == pmtpsb->dwCookie);

    /*  No work to do at the base class level- just release the object
     */

    PSLMemFree(pmtpsb);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPServiceBaseGetServiceID
 *
 *  Returns the service ID for this service
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          Service to query
 *
 *  Returns:
 *      PSLUINT32       Service ID for object
 */

PSLUINT32 PSL_API MTPServiceBaseGetServiceID(MTPSERVICE mtpService)
{
    PSLUINT32           dwResult;
    MTPSERVICEBASEOBJ*  pmtpsb;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpService)
    {
        dwResult = MTP_SERVICEID_UNDEFINED;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpsb = (MTPSERVICEBASEOBJ*)mtpService;
    PSLASSERT(MTPSERVICEBASEOBJ_COOKIE == pmtpsb->dwCookie);

    /*  And return the service ID
     */

    PSLASSERT(MTP_SERVICEID_UNDEFINED != pmtpsb->dwServiceID);
    dwResult = pmtpsb->dwServiceID;

Exit:
    return dwResult;
}


/*  MTPServiceBaseGetStorageID
 *
 *  Returns the storage ID for this service
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          Service to query
 *
 *  Returns:
 *      PSLUINT32       Storage ID for object
 */

PSLUINT32 PSL_API MTPServiceBaseGetStorageID(MTPSERVICE mtpService)
{
    PSLUINT32           dwResult;
    MTPSERVICEBASEOBJ*  pmtpsb;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpService)
    {
        dwResult = MTP_STORAGEID_UNDEFINED;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpsb = (MTPSERVICEBASEOBJ*)mtpService;
    PSLASSERT(MTPSERVICEBASEOBJ_COOKIE == pmtpsb->dwCookie);

    /*  And return the storage ID
     */

    dwResult = pmtpsb->dwStorageID;

Exit:
    return dwResult;
}


/*  MTPServiceBaseGetFormatsSupported
 *
 *  Returns a list of the formats supported by this service.  Note that the
 *  list is generated directly from the contents of the MTPSERVICEINFO
 *  dataset.
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          Service to query
 *      PSLUINT16*      pwFormats           Return buffer for supported formats
 *      PSLUINT32       cFormats            Size of the format buffer
 *      PSLUINT32*      pcFormats           Number of formats returned
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServiceBaseGetFormatsSupported(MTPSERVICE mtpService,
                    PSLUINT16* pwFormats, PSLUINT32 cFormats,
                    PSLUINT32* pcFormats)
{
    PSLSTATUS           ps;
    MTPSERVICEBASEOBJ*  pmtpsb;

    /*  Clear result for safety
     */

    if (PSLNULL != pcFormats)
    {
        *pcFormats = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpService) || (PSLNULL == pcFormats))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpsb = (MTPSERVICEBASEOBJ*)mtpService;
    PSLASSERT(MTPSERVICEBASEOBJ_COOKIE == pmtpsb->dwCookie);

    /*  And use the MTPSERVICEINFO directly to get the formats
     */

    ps = MTPServiceInfoGetFormatsSupported(
                            pmtpsb->pServiceBaseInfo->pServiceInfo,
                            pwFormats, cFormats, pcFormats);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPServiceBaseIsFormatSupported
 *
 *  Walks through the list of supported formats to determine if the specified
 *  format is supported by this service.  The validation is performed directly
 *  against the formats indicated in the MTPSERVICEINFO dataset.
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          Service to query
 *      PSLUINT16       wFormat             Format to query
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if matches, PSLSUCCESS_FALSE if not
 */

PSLSTATUS PSL_API MTPServiceBaseIsFormatSupported(MTPSERVICE mtpService,
                    PSLUINT16 wFormat)
{
    PSLSTATUS           ps;
    MTPSERVICEBASEOBJ*  pmtpsb;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpService)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpsb = (MTPSERVICEBASEOBJ*)mtpService;
    PSLASSERT(MTPSERVICEBASEOBJ_COOKIE == pmtpsb->dwCookie);

    /*  And use the MTPSERVICEINFO directly to get the result
     */

    ps = MTPServiceInfoIsFormatSupported(pmtpsb->pServiceBaseInfo->pServiceInfo,
                            wFormat);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPServiceBaseContextDataOpen
 *
 *  Opens a ServiceContextData object based on the information provided
 *  in the MTPSERVICEINFO dataset.
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          Service to query
 *      PSLUINT32       dwDataFormat        Data being requested
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              Context information
 *      PSLUINT32       cmdbv               Number of contexts
 *      PSLUINT64*      pqwBufSize          Size of the buffer needed to
 *                                            hold this dataset
 *      MTPSERVICECONTEXTDATA*
 *                      pmscd               Resulting context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServiceBaseContextDataOpen(MTPSERVICE mtpService,
                    PSLUINT32 dwDataFormat, PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv, PSLUINT64* pqwBufSize,
                    MTPSERVICECONTEXTDATA* pmscd)
{
    PSLSTATUS           ps;
    MTPSERVICEBASEOBJ*  pmtpsb;

    /*  Clear results for safety
     */

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }
    if (PSLNULL != pmscd)
    {
        *pmscd = PSLNULL;
    }

    /*  Validate Arguments
     */

    if (PSLNULL == mtpService)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpsb = (MTPSERVICEBASEOBJ*)mtpService;
    PSLASSERT(MTPSERVICEBASEOBJ_COOKIE == pmtpsb->dwCookie);

    /*  And use the MTPSERVICEINFO directly to get the context
     */

    ps = MTPServiceInfoContextDataOpen(pmtpsb->pServiceBaseInfo->pServiceInfo,
                            pmtpsb->dwServiceID, pmtpsb->dwStorageID,
                            &(pmtpsb->guidServicePUID), dwDataFormat,
                            rgmdbv, cmdbv, (PSLPARAM)mtpService, PSLNULL,
                            pqwBufSize, pmscd);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPServiceBaseCreate
 *
 *  Creates an instance of an MTP Service using the MTPServiceBase helpers.
 *
 *  Arguments:
 *      PSLUINT32       dwServiceID         Service ID for the service
 *      PSLUINT32       dwStorageID         Storage ID for the service
 *      PSLGUID*        pguidServicePUID    Service PUID
 *      MTPSERVICEBASEINFO*
 *                      pServiceBaseInfo    Service info for the service
 *      PSLUINT32       cbObject            Size of the service object to
 *                                            create
 *      MTPSERVICE*     pmtpService         Resulting service
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPServiceBaseCreate(PSLUINT32 dwServiceID,
                    PSLUINT32 dwStorageID, PSL_CONST PSLGUID* pguidServicePUID,
                    PSL_CONST MTPSERVICEBASEINFO* pServiceBaseInfo,
                    PSLUINT32 cbObject, MTPSERVICE* pmtpService)
{
    PSLSTATUS           ps;
    MTPSERVICEBASEOBJ*  pmtpsb = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpService)
    {
        *pmtpService = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pguidServicePUID) || (PSLNULL == pServiceBaseInfo) ||
        (sizeof(MTPSERVICEBASEOBJ) > cbObject) || (PSLNULL == pmtpService))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate a new service object of the size requested
     */

    pmtpsb = (MTPSERVICEBASEOBJ*)PSLMemAlloc(PSLMEM_PTR, cbObject);
    if (PSLNULL == pmtpsb)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    /*  Initialize the object
     */

#ifdef PSL_ASSERTS
    pmtpsb->dwCookie = MTPSERVICEBASEOBJ_COOKIE;
#endif  /* PSL_ASSERTS */
    pmtpsb->mtpSO.vtbl = &_VtblMTPServiceBase;
    pmtpsb->dwServiceID = dwServiceID;
    pmtpsb->dwStorageID = dwStorageID;
    PSLCopyMemory(&(pmtpsb->guidServicePUID), pguidServicePUID,
                            sizeof(pmtpsb->guidServicePUID));
    pmtpsb->pServiceBaseInfo = pServiceBaseInfo;

    /*  And return it
     */

    *pmtpService = pmtpsb;
    pmtpsb = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSERVICECLOSE(pmtpsb);
    return ps;
}


/*  MTPDBServiceBaseClose
 *
 *  Closes the MTP DB Service Base object by properly handing reference counts
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          Service context to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBServiceBaseClose(MTPSERVICE mtpService)
{
    PSLSTATUS               ps;
    MTPDBSERVICEBASEOBJ*    pmtpdbsb;

    /*  Validate Arguments
     */

    if (PSLNULL == mtpService)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpdbsb = (MTPDBSERVICEBASEOBJ*)mtpService;
    PSLASSERT(MTPDBSERVICEBASEOBJ_COOKIE == pmtpdbsb->dwCookie);

    /*  Decrement the reference count
     */

    PSLASSERT(0 < pmtpdbsb->cRef);
    pmtpdbsb->cRef--;
    if (0 != pmtpdbsb->cRef)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  No work to do at this level- go ahead and call the base function
     */

    ps = MTPServiceBaseClose(mtpService);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPDBServiceBaseDBContextOpen
 *
 *  Opens a DB Context for the Base Service implementation
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          Service context to use
 *      PSLUINT32       dwJoinType          Join behavior for properties
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              Variant properties defining context
 *      PSLUINT32       cmdbv               Number of variant properties
 *      MTPDBCONTEXT*   pmtpdbc             Resulting DBContext
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBServiceBaseDBContextOpen(MTPSERVICE mtpService,
                    PSLUINT32 dwJoinType, PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv, MTPDBCONTEXT* pmtpdbc)
{
    PSLUINT32               idxProp;
    PSLSTATUS               ps;
    MTPDBSERVICEBASEOBJ*    pmtpdbsbo;
    MTPDBCONTEXTBASEOBJ*    pmtpdbcbo;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpdbc)
    {
        *pmtpdbc = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mtpService) || (MTPJOINTYPE_AND != dwJoinType) ||
        ((0 < cmdbv) && (PSLNULL == rgmdbv)) || (PSLNULL == pmtpdbc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpdbsbo = (MTPDBSERVICEBASEOBJ*)mtpService;
    PSLASSERT(MTPDBSERVICEBASEOBJ_COOKIE == pmtpdbsbo->dwCookie);
    pmtpdbcbo = &(pmtpdbsbo->mtpDBCO);

    /*  Walk through the context description looking for anything that
     *  we care about
     */

    for (idxProp = 0; idxProp < cmdbv; idxProp++)
    {
        /*  Examine the current property in the description
         */

        switch (rgmdbv[idxProp].wPropCode)
        {
        case MTP_OBJECTPROPCODE_PARENT:
        case MTP_OBJECTPROPCODE_OBJECTHANDLE:
            /*  Validate the type
             */

            if (MTP_DATATYPE_UINT32 != rgmdbv[idxProp].wDataType)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  If the handle is not provided then ignore the property
             */

            if (MTP_OBJECTHANDLE_UNDEFINED == rgmdbv[idxProp].vParam.valUInt32)
            {
                break;
            }

            /*  Allow the "all" or "root" case to succeeed- but validate
             *  everything else to make sure that it belongs to us
             */

            PSLASSERT(MTP_OBJECTHANDLE_ALL == MTP_OBJECTHANDLE_ROOT);
            if (MTP_OBJECTHANDLE_ALL != rgmdbv[idxProp].vParam.valUInt32)
            {
                /*  Validate the storage index to make sure it matches ours
                 */

                ASSERT(UNPACK_STORAGEINDEX(rgmdbv[idxProp].vParam.valUInt32) ==
                        GET_STORAGEINDEX(pmtpdbsbo->mtpSBO.dwStorageID));
                if (UNPACK_STORAGEINDEX(rgmdbv[idxProp].vParam.valUInt32) !=
                            GET_STORAGEINDEX(pmtpdbsbo->mtpSBO.dwStorageID))
                {
                    ps = PSLERROR_INVALID_PARAMETER;
                    goto Exit;
                }
            }

            /*  Remember the object ID that we were handed
             */

            switch (rgmdbv[idxProp].wPropCode)
            {
            case MTP_OBJECTPROPCODE_PARENT:
                pmtpdbcbo->dwCtxParentID = rgmdbv[idxProp].vParam.valUInt32;
                break;

            case MTP_OBJECTPROPCODE_OBJECTHANDLE:
                pmtpdbcbo->dwCtxObjectID = rgmdbv[idxProp].vParam.valUInt32;
                break;

            default:
                /*  Should never get here
                 */

                PSLASSERT(PSLFALSE);
                ps = PSLERROR_UNEXPECTED;
                goto Exit;
            }
            break;

        case MTP_OBJECTPROPCODE_STORAGEID:
            /*  Validate the type
             */

            if (MTP_DATATYPE_UINT32 != rgmdbv[idxProp].wDataType)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  Make sure the storage ID is valid
             */

            ASSERT(rgmdbv[idxProp].vParam.valUInt32 ==
                            pmtpdbsbo->mtpSBO.dwStorageID);
            if (rgmdbv[idxProp].vParam.valUInt32 != pmtpdbsbo->mtpSBO.dwStorageID)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }
            break;

        case MTP_OBJECTPROPCODE_OBJECTFORMAT:
            /*  Validate the type
             */

            if (MTP_DATATYPE_UINT16 != rgmdbv[idxProp].wDataType)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  Ignore the unused case
             */

            if (MTP_FORMATCODE_NOTUSED == rgmdbv[idxProp].vParam.valUInt16)
            {
                break;
            }

            /*  Make sure the format is supported
             */

            ps = MTPServiceIsFormatSupported(mtpService,
                            rgmdbv[idxProp].vParam.valUInt16);
            if (PSLSUCCESS != ps)
            {
                ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  Stash the format
             */

            pmtpdbcbo->wCtxFormat = rgmdbv[idxProp].vParam.valUInt16;
            break;

        case MTP_OBJECTPROPCODE_DEPTH:
            /*  Validate type
             */

            if (MTP_DATATYPE_UINT32 != rgmdbv[idxProp].wDataType)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  Ignore the undefined case
             */

            if (MTP_OBJECTDEPTH_UNDEFINED == rgmdbv[idxProp].vParam.valUInt32)
            {
                break;
            }

            /*  And store the desired depth
             */

            pmtpdbcbo->dwCtxDepth = rgmdbv[idxProp].vParam.valUInt32;
            break;

        default:
            /*  Property not currently handled
             */

            PSLTraceError("MTPDBContextBaseOpen: Unsupported property 0x%04x",
                            rgmdbv[idxProp].wPropCode);
            PSLASSERT(PSLFALSE);
            break;
        }
    }

    /*  Context information is set- return the DBContext portion of the
     *  object
     */

    pmtpdbsbo->cRef++;
    *pmtpdbc = MTPDBSERVICEBASEOBJ_TO_MTPDBCONTEXT(pmtpdbsbo);
    ps = PSLSUCCESS;

Exit:
    return PSLSUCCESS;
}


/*  MTPDBServiceBaseDBContextClose
 *
 *  Closes an instance of the MTP DB Service Base DBContext handler
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBServiceBaseDBContextClose(MTPDBCONTEXT mdbc)
{
    PSLSTATUS       ps;

    /*  Cancel any pending DB Context operations
     */

    ps = MTPDBContextCancel(mdbc);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And close the core
     */

    ps = MTPServiceClose(MTPDBSERVICEBASEOBJ_FROM_MTPDBCONTEXT(mdbc));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPDBServiceBaseDBContextDataOpen
 *
 *  Opens a DB Context Data for this object.  The context data is the "what"
 *  portion of the query.  This method always reports not implemented.
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context to associate with the
 *                                            DB Context Data
 *      PSLUINT32       dwDataFormat        Format of the data
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              DB Variant Props defining data
 *                                            context
 *      PSLUINT32       cmdbv               Number of properties
 *      PSLUINT64*      pcbTotalSize        Size of the expected buffer
 *      MTPDBCONTEXTDATA*
 *                      pmdbcd              Return buffer for context data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBServiceBaseDBContextDataOpen(MTPDBCONTEXT mdbc,
                    PSLUINT32 dwDataFormat, PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv, PSLUINT64* pcbTotalSize,
                    MTPDBCONTEXTDATA* pmdbcd)
{
    PSLUINT32                       dwFreeObjects;
    MTPDBCONTEXTDATA                mdbcdNew = PSLNULL;
    PSLSTATUS                       ps;
    MTPDBSERVICEBASEOBJ*            pmtpdbsbo;
    PSL_CONST MTPDBSERVICEBASEINFO* pServiceBaseInfo;
    PSLUINT64                       qwFreeSpace;

    /*  Clear results for safety
     */

    if (PSLNULL != pcbTotalSize)
    {
        *pcbTotalSize = 0;
    }
    if (PSLNULL != pmdbcd)
    {
        *pmdbcd = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == mdbc) || ((0 < cmdbv) && (PSLNULL == rgmdbv)) ||
        (PSLNULL == pmdbcd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpdbsbo = MTPDBSERVICEBASEOBJ_FROM_MTPDBCONTEXT(mdbc);
    PSLASSERT(MTPDBSERVICEBASEOBJ_COOKIE == pmtpdbsbo->dwCookie);
    pServiceBaseInfo = (PSL_CONST MTPDBSERVICEBASEINFO*)
                            pmtpdbsbo->mtpSBO.pServiceBaseInfo;

    /*  Look for types we can handle
     */

    switch (MTPDBCONTEXTDATADESC_FORMAT(dwDataFormat))
    {
    case MTPDBCONTEXTDATAFORMAT_STORAGEID:
    case MTPDBCONTEXTDATAFORMAT_FORMATID:
    case MTPDBCONTEXTDATAFORMAT_DEVICEPROPDESC:
    case MTPDBCONTEXTDATAFORMAT_DEVICEPROPLIST:
    case MTPDBCONTEXTDATAFORMAT_OBJECTINFO:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROTECTION:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPARRAY:
    case MTPDBCONTEXTDATAFORMAT_OBJECTTHUMBDATA:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPSSUPPORTED:
    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPDESC:
    case MTPDBCONTEXTDATAFORMAT_SERVICEID:
    case MTPDBCONTEXTDATAFORMAT_SERVICEINFO:
    case MTPDBCONTEXTDATAFORMAT_SERVICECAPABILITIES:
    case MTPDBCONTEXTDATAFORMAT_SERVICEPROPLIST:
    case MTPDBCONTEXTDATAFORMAT_SERVICEPROPERTYDESC:
        /*  These formats are not supported in the context of a service's
         *  DB Context
         */

        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;


    case MTPDBCONTEXTDATAFORMAT_STORAGE:
        /*  Retrieve the current free space information
         */

        ps = MTPDBServiceBaseGetFreeSpace(pmtpdbsbo, &qwFreeSpace,
                            &dwFreeObjects);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Create a storage info context data serializer
         */

        ps = MTPStorageInfoDBContextDataCreate(pServiceBaseInfo->pStorageInfo,
                            dwDataFormat, qwFreeSpace, dwFreeObjects,
                            &mdbcdNew);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTHANDLE:
        /*  Create an object handles context data serializer
         */

        ps = MTPObjectHandlesContextDataCreate(mdbc, dwDataFormat, &mdbcdNew);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTPROPLIST:
        /*  Create an object property list context data serializer
         */

        ps = MTPObjectPropListContextDataCreate(mdbc, dwDataFormat, rgmdbv,
                            cmdbv, &mdbcdNew);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA:
        /*  Create an object binary context data serializer
         */

        ps = MTPObjectBinaryContextDataCreate(mdbc, dwDataFormat, 0,
                            &mdbcdNew);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;


    default:
        /*  Unknown format
         */

        PSLTraceError("MTPDBServiceBaseDBContextOpen: Unknown format specified"
                            "0x%08x", dwDataFormat);
        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Return the size if requested
     */

    if (PSLNULL != pcbTotalSize)
    {
        ps = MTPDBContextDataSerialize(mdbcdNew, PSLNULL, 0, PSLNULL,
                            pcbTotalSize);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  All is well- return the new context data object
     */

    *pmdbcd = mdbcdNew;
    mdbcdNew = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcdNew);
    return ps;
}


/*  MTPDBServiceBaseDBContextDataClose
 *
 *  Closes an instance of the MTP DB Service Base DBContextData handler
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               DB Context Data to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBServiceBaseDBContextDataClose(MTPDBCONTEXTDATA mdbcd)
{
    PSLSTATUS       ps;

    /*  Cancel any pending DB Context Data operations
     */

    ps = MTPDBContextDataCancel(mdbcd);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And close the core
     */

    ps = MTPServiceClose(MTPDBSERVICEBASEOBJ_FROM_MTPDBCONTEXTDATA(mdbcd));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPDBServiceBaseGetFreeSpace
 *
 *  Returns the amount of free space available on the service
 *
 *  Arguments:
 *      MTPSERVICE      mtpService          Service context being called
 *      PSLUINT64*      pqwFreeSpace        Free space in bytes
 *      PSLUINT32*      pdwFreeObjects      Free space in objects
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBServiceBaseGetFreeSpaceBase(MTPSERVICE mtpService,
                    PSLUINT64* pqwFreeSpace, PSLUINT32* pdwFreeObjects)
{
    PSLSTATUS               ps;
    MTPDBSERVICEBASEOBJ*    pmtpdbsbo;

    /*  Clear results for safety
     */

    if (PSLNULL != pqwFreeSpace)
    {
        *pqwFreeSpace = 0;
    }
    if (PSLNULL != pdwFreeObjects)
    {
        *pdwFreeObjects = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mtpService) ||
        ((PSLNULL == pqwFreeSpace) && (PSLNULL == pdwFreeObjects)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpdbsbo = (MTPDBSERVICEBASEOBJ*)mtpService;
    PSLASSERT(MTPDBSERVICEBASEOBJ_COOKIE == pmtpdbsbo->dwCookie);

    /*  Our work is done- this service is read only
     */

    PSLASSERT((PSLNULL == pqwFreeSpace) || (0 == *pqwFreeSpace));
    PSLASSERT((PSLNULL == pdwFreeObjects) || (0 == *pdwFreeObjects));
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPDBServiceBaseCreate
 *
 *  Creates an instance of an MTP Service using the MTPServiceBase and
 *  MTPDBServiceBase helpers.
 *
 *  Arguments:
 *      PSLUINT32       dwServiceID         Service ID for the service
 *      PSLUINT32       dwStorageID         Storage ID for the service
 *      PSLGUID*        pguidServicePUID    Service PUID
 *      MTPDBSERVICEBASEINFO*
 *                      pServiceBaseInfo    Service info for the service
 *      PSLUINT32       cbObject            Size of the service object to
 *                                            create
 *      MTPSERVICE*     pmtpService         Resulting service
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBServiceBaseCreate(PSLUINT32 dwServiceID,
                    PSLUINT32 dwStorageID, PSL_CONST PSLGUID* pguidServicePUID,
                    PSL_CONST MTPDBSERVICEBASEINFO* pServiceBaseInfo,
                    PSLUINT32 cbObject, MTPSERVICE* pmtpService)
{
    PSLSTATUS               ps;
    MTPDBSERVICEBASEOBJ*    pmtpdbsb = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpService)
    {
        *pmtpService = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pguidServicePUID) || (PSLNULL == pServiceBaseInfo) ||
        (sizeof(MTPDBSERVICEBASEOBJ) > cbObject) || (PSLNULL == pmtpService))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Retrieve and initialize the base service object
     */

    ps = MTPServiceBaseCreate(dwServiceID, dwStorageID, pguidServicePUID,
                            (PSL_CONST MTPSERVICEBASEINFO*)pServiceBaseInfo,
                            cbObject, &pmtpdbsb);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize the object
     */

#ifdef PSL_ASSERTS
    pmtpdbsb->dwCookie = MTPDBSERVICEBASEOBJ_COOKIE;
#endif  /* PSL_ASSERTS */
    pmtpdbsb->mtpSBO.mtpSO.vtbl = (PSL_CONST MTPSERVICEVTBL*)
                            &_VtblMTPDBServiceBase;
    pmtpdbsb->mtpDBCO.mtpDBCBO.vtbl = (PSL_CONST MTPDBCONTEXTVTBL*)
                            &_VtblMTPDBServiceBaseDBContextBase;
    pmtpdbsb->mtpDBCDO.vtbl = &_VtblMTPDBServiceBaseDBContextData;
    pmtpdbsb->cRef = 1;

    /*  And return it
     */

    *pmtpService = pmtpdbsb;
    pmtpdbsb = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSERVICECLOSE(pmtpdbsb);
    return ps;
}


