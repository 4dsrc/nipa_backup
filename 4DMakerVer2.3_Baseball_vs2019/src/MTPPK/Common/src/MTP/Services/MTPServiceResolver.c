/*
 *  MTPServiceResolver.c
 *
 *  Contains definition of the MTP Service Resolver API functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPServicePrecomp.h"
#include "MTPDBContextPool.h"

/*  Local Defines
 */

#define MTPSERVICERESOLVER_COOKIE   'msrO'

#define MTPSERVICEIDCONTEXTDATA_COOKIE  'dSID'

#define DEFAULT_RESOLVER_POOL       MTP_DEFAULT_SESSIONS

#define RESOLVER_CACHE_SIZE         16

/*  Local Types
 */

typedef struct _MTPSERVICERESOLVEROBJ
{
#ifdef PSL_ASSERTS
    PSLUINT32                       dwCookie;
#endif  /* PSL_ASSERTS */
    PSLHANDLE                       hMutex;
    MTPCONTEXT*                     pmtpctx;
    MTPDBSESSION                    mtpdbLegacy;
    PSLDYNLIST                      dlServices;
    PSLBKTHASH                      bhServiceID;
    PSLBKTHASH                      bhStorageID;
    PSLBKTHASH                      bhFormatCode;
} MTPSERVICERESOLVEROBJ;


typedef struct _MTPSERVICEIDCONTEXTDATAOBJ
{
    MTPSERVICECONTEXTDATAOBJ    mtpSCDO;
#ifdef PSL_ASSERTS
    PSLUINT32                   dwCookie;
#endif  /* PSL_ASSERTS */
    MTPSERIALIZE                msServiceID;
} MTPSERVICEIDCONTEXTDATAOBJ;


/*  Local Functions
 */

static PSLINT32 PSL_API _CompareServices(PSLPARAM aKey,
                    PSL_CONST PSLPARAM* paItem);

static PSLINT32 PSL_API _CompareServiceID(PSLPARAM aKey,
                    PSL_CONST PSLPARAM* paItem);

static PSLINT32 PSL_API _CompareStorageID(PSLPARAM aKey,
                    PSL_CONST PSLPARAM* paItem);

static PSLINT32 PSL_API _CompareFormatCode(PSLPARAM aKey,
                    PSL_CONST PSLPARAM* paItem);

static PSLSTATUS PSL_API _MTPServiceIDContextDataClose(
                    MTPSERVICECONTEXTDATA hServiceContextData);

static PSLSTATUS PSL_API _MTPServiceIDGetCount(PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT32* pcItems);

static PSLSTATUS PSL_API _MTPServiceIDGetItem(PSLUINT32 idxItem,
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS PSL_API _MTPServiceIDContextDataSerialize(
                    MTPSERVICECONTEXTDATA hServiceContextData,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);

static PSLSTATUS _MTPServiceIDContextDataCreate(PSLDYNLIST dlService,
                    MTPSERVICECONTEXTDATA* pServiceContextData);

static PSLSTATUS _MTPServiceResolverAddService(MTPSERVICERESOLVER msr,
                    MTPSERVICE mtpService);

static PSLSTATUS _MTPServiceResolverRemoveService(MTPSERVICERESOLVER msr,
                    MTPSERVICE mtpService);

static PSLSTATUS _MTPServiceResolverAddDBSession(MTPSERVICERESOLVER msr,
                    MTPDBSESSION mtpdbLegacy);

static PSLSTATUS _MTPServiceResolverRemoveDBSession(MTPSERVICERESOLVER msr,
                    MTPDBSESSION mtpdbLegacy);

/*  Local Variables
 */

static PSLUINT32                    _CServicesMax = 0;

static PSLOBJPOOL                   _OpResolver = PSLNULL;

MTPSERVICECONTEXTDATAVTBL       _VtblServiceIDContextData =
{
    _MTPServiceIDContextDataClose,
    MTPDBContextDataCancelBase,
    _MTPServiceIDContextDataSerialize,
    MTPDBContextDataStartDeserializeBase,
    MTPDBContextDataDeserializeBase
};

MTPSERIALIZEARRAYVTBL           _VtblServiceIDSerialize =
{
    _MTPServiceIDGetCount,
    _MTPServiceIDGetItem
};

MTPSERIALIZEINFO                _RgmsiServiceIDTemplate[] =
{
    {
        MTP_DATATYPE_AUINT32,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)&_VtblServiceIDSerialize,
        PSLNULL
    }
};

/*
 *  _CompareServices
 *
 *  Comparison callback for comparing two services.
 *
 *  WARNING: This comparison is only useful for equality as "less then" and
 *  "greater then" have no real meaning in this context.
 *
 *  Arguments:
 *      PSLPARAM        aKey                Service to compare against
 *      PSLPARAM*       paItem              Item to compare to
 *
 *  Returns:
 *      PSLINT32        Result of comparison:
 *                          < 0 : aKey < *paItem
 *                          = 0 : aKey == *paItem
 *                          > 0 : aKey > *paItem
 */

PSLINT32 PSL_API _CompareServices(PSLPARAM aKey, PSL_CONST PSLPARAM* paItem)
{
    MTPSERVICEOBJ*  psrvKey;
    MTPSERVICEOBJ*  psrvItem;
    PSLUINT32       dwStorageIDKey;
    PSLUINT32       dwStorageIDItem;

    /*  Unwrap the data
     */

    psrvKey = (MTPSERVICEOBJ*)aKey;
    psrvItem = (MTPSERVICEOBJ*)*paItem;

    /*  Grab the storage IDs so that we can do an additional comparison in case
     *  the storage ID is not supported
     */

    dwStorageIDKey = MTPServiceGetStorageID(psrvKey);
    dwStorageIDItem = MTPServiceGetStorageID(psrvItem);

    /*  And determine if we have a match
     */

    PSLASSERT((PSLNULL != psrvKey) && (PSLNULL != psrvItem));
    return ((psrvKey == psrvItem) ||
            (MTPServiceGetServiceID(psrvKey) ==
                    MTPServiceGetServiceID(psrvItem)) ||
            ((dwStorageIDKey == dwStorageIDItem) &&
                    (MTP_STORAGEID_UNDEFINED != dwStorageIDKey))) ? 0 : -1;
}


/*
 *  _CompareServiceID
 *
 *  Compares the ServiceID in aKey to the service ID for the provided
 *  service
 *
 *  Arguments:
 *      PSLPARAM        aKey                Service ID to compare against
 *      PSLPARAM*       paItem              Item to compare to
 *
 *  Returns:
 *      PSLINT32        Result of comparison:
 *                          < 0 : aKey < *paItem
 *                          = 0 : aKey == *paItem
 *                          > 0 : aKey > *paItem
 */

PSLINT32 PSL_API _CompareServiceID(PSLPARAM aKey, PSL_CONST PSLPARAM* paItem)
{
    PSLUINT32       dwServiceID;
    MTPSERVICEOBJ*  psrvItem;

    /*  Unwrap the data
     */

    dwServiceID = aKey;
    psrvItem = (MTPSERVICEOBJ*)*paItem;

    /*  And determine if we have a match
     */

    PSLASSERT(PSLNULL != psrvItem);
    return (PSLINT32)(dwServiceID - MTPServiceGetServiceID(psrvItem));
}


/*
 *  _CompareStorageID
 *
 *  Compares the StorageID in aKey to the service ID for the provided
 *  service
 *
 *  Arguments:
 *      PSLPARAM        aKey                StorageID to compare against
 *      PSLPARAM*       paItem              Item to compare to
 *
 *  Returns:
 *      PSLINT32        Result of comparison:
 *                          < 0 : aKey < *paItem
 *                          = 0 : aKey == *paItem
 *                          > 0 : aKey > *paItem
 */

PSLINT32 PSL_API _CompareStorageID(PSLPARAM aKey, PSL_CONST PSLPARAM* paItem)
{
    PSLUINT32       dwStorageID;
    MTPSERVICEOBJ*  psrvItem;

    /*  Unwrap the data
     */

    dwStorageID = aKey;
    psrvItem = (MTPSERVICEOBJ*)*paItem;

    /*  And determine if we have a match
     */

    PSLASSERT(PSLNULL != psrvItem);
    return (PSLINT32)(dwStorageID - MTPServiceGetStorageID(psrvItem));
}


/*
 *  _CompareFormatCode
 *
 *  Returns whether or not the format code is supported by this service.
 *
 *  WARNING: This comparison is only useful for equality as "less then" and
 *  "greater then" have no real meaning in this context.
 *
 *  Arguments:
 *      PSLPARAM        aKey                Format code to check
 *      PSLPARAM*       paItem              Item to compare to
 *
 *  Returns:
 *      PSLINT32        Result of comparison:
 *                          < 0 : aKey < *paItem
 *                          = 0 : aKey == *paItem
 *                          > 0 : aKey > *paItem
 */

PSLINT32 PSL_API _CompareFormatCode(PSLPARAM aKey, PSL_CONST PSLPARAM* paItem)
{
    PSLUINT16       wFormatCode;
    MTPSERVICE      srvItem;

    /*  Unwrap the data
     */

    wFormatCode = (PSLUINT16)aKey;
    srvItem = (MTPSERVICE)*paItem;

    /*  And determine if we have a match
     */

    PSLASSERT(PSLNULL != srvItem);
    return (PSLSUCCESS == MTPServiceIsFormatSupported(srvItem, wFormatCode)) ?
                            0 : 1;
}


/*  _MTPServiceIDContextDataClose
 *
 *  Closes the Service ID context data object
 *
 *  Arguments:
 *      MTPSERVICECONTEXTDATA hServiceContextData
 *                                          Context data object to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPServiceIDContextDataClose(
                    MTPSERVICECONTEXTDATA hServiceContextData)
{
    PSLSTATUS                   ps;
    MTPSERVICEIDCONTEXTDATAOBJ* pServiceIDCDO;

    /*  Valdiate arguments
     */

    if (PSLNULL == hServiceContextData)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pServiceIDCDO = (MTPSERVICEIDCONTEXTDATAOBJ*)hServiceContextData;
    PSLASSERT(MTPSERVICEIDCONTEXTDATA_COOKIE == pServiceIDCDO->dwCookie);

    /*  Release internal objects
     */

    SAFE_MTPSERIALIZEDESTROY(pServiceIDCDO->msServiceID);

    /*  And the object itself
     */

    SAFE_MTPDBCONTEXTDATAPOOLFREE(pServiceIDCDO);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  _MTPServiceIDGetCount
 *
 *  Callback function for use with the MTP Serialization routines for
 *  retrieving the count of service IDs
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context for callback
 *      PSLPARAM        aValueContext       Value context for array item
 *      PSLUINT32*      pcItems             Count of items to serialize
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServiceIDGetCount(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT32* pcItems)
{
    PSLDYNLIST      dlServices;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcItems))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unwrap the service list
     */

    dlServices = (PSLDYNLIST)aContext;

    /*  Retrieve the number of items in the array
     */

    ps = PSLDynListGetSize(dlServices, pcItems);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
    aValueContext;
}


/*  _MTPServiceIDGetItem
 *
 *  Callback function for use with the MTP Serialization routines for
 *  retrieving service ID information
 *
 *  Arguments:
 *      PSLUINT32       idxItem             Index of item being used
 *      PSLPARAM        aContext            Context for callback
 *      PSLPARAM        aValueContext       Value context for array item
 *      PSLUINT16       wType               Type of data to serialize
 *      PSLVOID*        pvBuf               Destination for serialization
 *      PSLUINT32       cbBuf               Size of definitation buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServiceIDGetItem(PSLUINT32 idxItem,
                    PSLPARAM aContext, PSLPARAM aValueContext,
                    PSLUINT16 wType, PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed)
{
    PSLDYNLIST      dlServices;
    MTPSERVICE      mtpService;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments- both the length and the items in the array are
     *  PSLUINT32 values so we check the size here as well
     */

    if ((PSLNULL == aContext) || (MTP_DATATYPE_UINT32 != wType) ||
        ((0 < cbBuf) && (PSLNULL == pvBuf)) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  If we were provided a buffer make sure that we can fit everything
     */

    if ((PSLNULL != pvBuf) && (sizeof(PSLUINT32) > cbBuf))
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Unwrap the service list
     */

    dlServices = (PSLDYNLIST)aContext;

    /*  Retrieve the requested item
     */

    ps = PSLDynListGetItem(dlServices, idxItem, (PSLPARAM*)&mtpService);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And store the service ID
     */

    MTPStoreUInt32((PXPSLUINT32)pvBuf, MTPServiceGetServiceID(mtpService));
    *pcbUsed = sizeof(PSLUINT32);
    ps = PSLSUCCESS;

Exit:
    return ps;
    aValueContext;
}


/*  _MTPServiceIDContextDataSerialize
 *
 *  Serialization routine for Service ID lists
 *
 *  Arguments:
 *      MTPSERVICECONTEXTDATA hServiceContextData
 *                                          Service context data object to use
 *      PSLVOID*        pvBuf               Destination for serialized data
 *      PSLUINT32       cbBuf               Size of the destination
 *      PSLUINT32*      pcbWritten          Return buffer for number of bytes
 *                                            written
 *      PSLUINT64*      pcbLeft             Return buffer for number of bytes
 *                                            left
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPServiceIDContextDataSerialize(
                    MTPSERVICECONTEXTDATA hServiceContextData,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft)
{
    PSLSTATUS                   ps;
    MTPSERVICEIDCONTEXTDATAOBJ* pServiceIDCDO;

    /*  Clear results for safety
     */

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }
    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /*  Valdiate arguments
     */

    if (PSLNULL == hServiceContextData)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pServiceIDCDO = (MTPSERVICEIDCONTEXTDATAOBJ*)hServiceContextData;
    PSLASSERT(MTPSERVICEIDCONTEXTDATA_COOKIE == pServiceIDCDO->dwCookie);

    /*  Call the serializer to do the real work
     */

    ps = MTPSerialize(pServiceIDCDO->msServiceID, pvBuf, cbBuf, pcbWritten,
                            pcbLeft);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPServiceIDContextDataCreate
 *
 *  Creates an instance of a generic service context data object used to
 *  serialize service IDs
 *
 *  Arguments:
 *      PSLDYNLIST      dlService           PSL Dynamic List containing the
 *                                            list of services
 *      MTPSERVICECONTEXTDATA*
 *                      pServiceContextData Return buffer for created context
 *                                            data object
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPServiceIDContextDataCreate(PSLDYNLIST dlService,
                    MTPSERVICECONTEXTDATA* pServiceContextData)
{
    PSLSTATUS                   ps;
    MTPSERVICEIDCONTEXTDATAOBJ* pServiceIDCDO = PSLNULL;

    /*  Clear results for safety
     */

    if (PSLNULL != pServiceContextData)
    {
        *pServiceContextData = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == dlService) || (PSLNULL == pServiceContextData))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Retrieve a new context data object
     */

    ps = MTPDBContextDataPoolAlloc(sizeof(MTPSERVICEIDCONTEXTDATAOBJ),
                            &pServiceIDCDO);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize the core object
     */

#ifdef PSL_ASSERTS
    pServiceIDCDO->dwCookie = MTPSERVICEIDCONTEXTDATA_COOKIE;
#endif  /* PSL_ASSERTS */
    pServiceIDCDO->mtpSCDO.vtbl = &_VtblServiceIDContextData;

    /*  Create a serializer to handle the work
     */

    ps = MTPSerializeCreate(_RgmsiServiceIDTemplate,
                            PSLARRAYSIZE(_RgmsiServiceIDTemplate),
                            (PSLPARAM)dlService, PSLNULL, PSLNULL,
                            &(pServiceIDCDO->msServiceID));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the serializer
     */

    *pServiceContextData = pServiceIDCDO;
    pServiceIDCDO = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPSERVICECONTEXTDATACLOSE(pServiceIDCDO);
    return ps;
}




/*
 *  _MTPServiceResolverAddService
 *
 *  Adds the specified service to the service resolver
 *
 *  Arguments:
 *      MTPSERVICERESOLVER
 *                      msr                 MTP Service Resolver to update
 *      MTPSERVICE      mtpService          Service to add
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLERROR_NOT_AVAILABLE if
 *                        service ID or storage ID already allocated
 *
 */

PSLSTATUS _MTPServiceResolverAddService(MTPSERVICERESOLVER msr,
                    MTPSERVICE mtpService)
{
    PSLUINT32               cServicesMax;
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLUINT32               idxItem;
    MTPSERVICE              mtpMatch;
    PSLSTATUS               ps;
    MTPSERVICERESOLVEROBJ*  pmsrObj;

    /*  Validate arguments
     */

    if ((PSLNULL == msr) || (PSLNULL == mtpService))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the resolver information
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)msr;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmsrObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmsrObj->hMutex;

    /*  Create the services list and the hashes so that we avoid searching
     *  on every service resolution.
     */

    if (PSLNULL == pmsrObj->dlServices)
    {
        /*  We create the list reserving the number of entries that we saw
         *  the last time we ran in an attempt to avoid extra allocations
         */

        cServicesMax = PSLAtomicCompareExchange(&_CServicesMax, 0, 0);
        ps = PSLDynListCreate(cServicesMax, PSLTRUE, PSLDYNLIST_GROW_DEFAULT,
                            &(pmsrObj->dlServices));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    if (PSLNULL == pmsrObj->bhServiceID)
    {
        ps = PSLBktHashCreate(RESOLVER_CACHE_SIZE, PSLNULL, PSLNULL,
                            &(pmsrObj->bhServiceID));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    if (PSLNULL == pmsrObj->bhStorageID)
    {
        ps = PSLBktHashCreate(RESOLVER_CACHE_SIZE, PSLNULL, PSLNULL,
                            &(pmsrObj->bhStorageID));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    if (PSLNULL == pmsrObj->bhFormatCode)
    {
        ps = PSLBktHashCreate(RESOLVER_CACHE_SIZE, PSLNULL, PSLNULL,
                            &(pmsrObj->bhFormatCode));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  See if we already have this service ID or storage ID registered
     */

    ps = PSLDynListFindItem(pmsrObj->dlServices, 0, (PSLPARAM)mtpService,
                            _CompareServices, &idxItem);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  If we found a match we need to figure out what matched and provide
     *  the appropriate error
     */

    if (PSLSUCCESS == ps)
    {
        /*  Retrieve the matching service
         */

        ps = PSLDynListGetItem(pmsrObj->dlServices, idxItem,
                            (PSLPARAM*)&mtpMatch);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  The match occurred either because the service was already
         *  registered or because the storage ID or service ID was in
         *  use.  If the service was already registered the pointer would
         *  match so we just check for pointers matching and report the
         *  error otherwise
         */

        ps = (mtpMatch == mtpService) ?
                            PSLSUCCESS_FALSE : PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Go ahead and add the service to the list of supported services
     */

    ps = PSLDynListAddItem(pmsrObj->dlServices, (PSLPARAM)mtpService, &idxItem);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Update the maximum number of services if this new service is larger
     *  then anything we have seen before
     */

    PSLAtomicCompareExchange(&_CServicesMax, (idxItem + 1), idxItem);

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  _MTPServiceResolverRemoveService
 *
 *  Removes a service from the list of known services
 *
 *  Arguments:
 *      MTPSERVICERESOLVER
 *                      msr                     Service resolver to remove
 *                                                service from
 *      MTPSERVICE      mtpService              Service to remove
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on removal, PSLSUCCES_NOT_FOUND if not
 *                        present
 *
 */

PSLSTATUS _MTPServiceResolverRemoveService(MTPSERVICERESOLVER msr,
                    MTPSERVICE mtpService)
{
    PSLUINT32               cFormats;
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLUINT32               idxService;
    PSLUINT32               idxFormat;
    PSLSTATUS               ps;
    MTPSERVICERESOLVEROBJ*  pmsrObj;
    PSLUINT16*              prgwFormats = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == msr) || (PSLNULL == mtpService))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the resolver information
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)msr;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmsrObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmsrObj->hMutex;

    /*  If we have no registered services we cannot find this service
     */

    if (PSLNULL == pmsrObj->dlServices)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  See if we already have this service ID or storage ID registered
     */

    ps = PSLDynListFindItem(pmsrObj->dlServices, 0, (PSLPARAM)mtpService,
                            _CompareServices, &idxService);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    /*  We need to remove all references to this service from the cache lists-
     *  start with service ID
     */

    ps = PSLBktHashRemove(pmsrObj->bhServiceID,
                            MTPServiceGetServiceID(mtpService), PSLNULL);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Next remove the storage ID
     */

    ps = PSLBktHashRemove(pmsrObj->bhStorageID,
                            MTPServiceGetStorageID(mtpService), PSLNULL);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Retrieve all of the format codes associated with this service
     */

    ps = MTPServiceGetFormatsSupported(mtpService, PSLNULL, 0, &cFormats);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    prgwFormats = (PSLUINT16*)PSLMemAlloc(PSLMEM_FIXED,
                            (sizeof(PSLUINT16) * cFormats));
    if (PSLNULL == prgwFormats)
    {
        ps = PSLERROR_OUT_OF_MEMORY;
        goto Exit;
    }

    ps = MTPServiceGetFormatsSupported(mtpService, prgwFormats, cFormats,
                            &cFormats);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And walk through and remove each of them from the cache.  Note that
     *  formats are not necessarily unique per service so we me accidentally
     *  remove the wrong format here.  The next time we resolve for this format
     *  we should search for and re-assign the format to the same service,
     */

    for (idxFormat = 0; idxFormat < cFormats; idxFormat++)
    {
        ps = PSLBktHashRemove(pmsrObj->bhFormatCode, prgwFormats[idxFormat],
                            PSLNULL);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  Caches are emptied- go ahead and remove the service from the list
     */

    ps = PSLDynListRemoveItem(pmsrObj->dlServices, idxService, 1);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMEMFREE(prgwFormats);
    return ps;
}


/*
 *  _MTPServiceResolverAddDBSession
 *
 *  Adds the specified DBSession to the resolver
 *
 *  Arguments:
 *      MTPSERVICERESOLVER
 *                      msr                 MTP Service Resolver to update
 *      MTPDBSESSION    mtpdbLegacy         MTP DB Session to add
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLERROR_NOT_AVAILABLE if
 *                        service ID or storage ID already allocated
 *
 */

PSLSTATUS _MTPServiceResolverAddDBSession(MTPSERVICERESOLVER msr,
                    MTPDBSESSION mtpdbLegacy)
{
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLSTATUS               ps;
    MTPSERVICERESOLVEROBJ*  pmsrObj;

    /*  Validate arguments
     */

    if ((PSLNULL == msr) || (PSLNULL == mtpdbLegacy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the resolver information
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)msr;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmsrObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmsrObj->hMutex;

    /*  See if we already have a legacy DB Session
     */

    if (PSLNULL != pmsrObj->mtpdbLegacy)
    {
        ps = (mtpdbLegacy != pmsrObj->mtpdbLegacy) ? PSLERROR_NOT_AVAILABLE :
                            PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Remember the legacy DB provided
     */

    pmsrObj->mtpdbLegacy = mtpdbLegacy;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  _MTPServiceResolverRemoveDBSession
 *
 *  Removes the specified DBSession from the resolver
 *
 *  Arguments:
 *      MTPSERVICERESOLVER
 *                      msr                     Service resolver to remove
 *                                                DB session from
 *      MTPDBSESSION    mtpdbLegacy             DB Session to remove
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on removal, PSLSUCCES_NOT_FOUND if not
 *                        present
 *
 */

PSLSTATUS _MTPServiceResolverRemoveDBSession(MTPSERVICERESOLVER msr,
                    MTPDBSESSION mtpdbLegacy)
{
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLSTATUS               ps;
    MTPSERVICERESOLVEROBJ*  pmsrObj;

    /*  Validate arguments
     */

    if ((PSLNULL == msr) || (PSLNULL == mtpdbLegacy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the resolver information
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)msr;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmsrObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmsrObj->hMutex;

    /*  Determine the correct result based on the currently registered
     *  session- if we have a match it is PSLSUCCESS, if we do not have a
     *  session it is PSLSUCCESS_FALSE, and if the two sessions don't match
     *  it is PSLERROR_NOT_AVAILABLE
     */

    ps = (mtpdbLegacy == pmsrObj->mtpdbLegacy) ? PSLSUCCESS :
                            ((PSLNULL == pmsrObj->mtpdbLegacy) ?
                                PSLSUCCESS_FALSE : PSLERROR_NOT_AVAILABLE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we clear the registered entry- but do not modify the result
     */

    pmsrObj->mtpdbLegacy = PSLNULL;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPServiceResolverInitialize
 *
 *  Initializes the MTP Service Resolver subsystem
 *
 *  Arguments:
 *      PSLUINT32       cSessionMax         Maximum number of sessions to
 *                                            support or 0 for infinite
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPServiceResolverInitialize(PSLUINT32 cSessionMax)
{
    PSLUINT32       cItems;
    PSLSTATUS       ps;

    /*  Make sure we are not already initialized
     *
     *  NOTE: MTPInitialize currently handles synchronization of initialize
     *  calls so we do not do additional work here.
     */

    if (PSLNULL != _OpResolver)
    {
        ps = PSLERROR_IN_USE;
        goto Exit;
    }

    /*  Determine the actual number of objects we want to reserve- we always
     *  want to keep some around even when we are running in an "unlimited"
     *  session model
     */

    cItems = (0 != cSessionMax) ? cSessionMax : DEFAULT_RESOLVER_POOL;

    /*  Create the object pool so that we avoid allocations
     */

    ps = PSLObjectPoolCreate(cItems, sizeof(MTPSERVICERESOLVEROBJ),
                            PSLOBJPOOLFLAG_ZERO_MEMORY | ((0 != cSessionMax) ?
                                    0 : PSLOBJPOOLFLAG_ALLOW_ALLOC),
                            &_OpResolver);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPServiceResolverUninitialize
 *
 *  Performs cleanup operations on the MTP Service Resolver sub-system
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPServiceResolverUninitialize(PSLVOID)
{
    /*  Release the object pool
     *
     *  NOTE: MTPUninitialize currently handles thread synchronization so we
     *  do not do additional work here
     */

    SAFE_PSLOBJECTPOOLDESTROY(_OpResolver);
    return PSLSUCCESS;
}


/*
 *  MTPServiceResolverCreate
 *
 *  Creates an instance of the MTP Service Resolver
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP Context this resolver lives in
 *      MTPSERVICERESOLVER*
 *                      pmsr                Return buffer for the resolver
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPServiceResolverCreate(MTPCONTEXT* pmtpctx,
                            MTPSERVICERESOLVER* pmsr)
{
    PSLSTATUS               ps;
    MTPSERVICERESOLVEROBJ*  pmsrObj = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmsr)
    {
        *pmsr = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pmsr))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Make sure that we have an object pool to work with
     */

    PSLASSERT(PSLNULL != _OpResolver);
    if (PSLNULL == _OpResolver)
    {
        ps = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    /*  Retrieve a new object
     */

    ps = PSLObjectPoolAlloc(_OpResolver, &pmsrObj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

#ifdef PSL_ASSERTS
    /*  Stamp the object ID
     */

    pmsrObj->dwCookie = MTPSERVICERESOLVER_COOKIE;
#endif  /* PSL_ASSERTS */

    /*  Create a mutex to synchronize access to the service list
     */

    ps = PSLMutexOpen(0, PSLNULL, &(pmsrObj->hMutex));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Stash the MTP Context
     */

    pmsrObj->pmtpctx = pmtpctx;

    /*  Return the new object
     */

    *pmsr = pmsrObj;
    pmsrObj = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLOBJECTPOOLFREE(pmsrObj);
    return ps;
}


/*
 *  MTPServiceResolverDestroy
 *
 *  Destroys the service resolver
 *
 *  Arguments:
 *      MTPSERVICERESOLVER
 *                      msr                 Resolver to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPServiceResolverDestroy(MTPSERVICERESOLVER msr)
{
    PSLUINT32               cServices;
    PSLHANDLE               hMutexClose = PSLNULL;
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLUINT32               idxService;
    MTPSERVICE              mtpService;
    PSLSTATUS               ps;
    MTPSERVICERESOLVEROBJ*  pmsrObj;

    /*  Validate Arguments
     */

    if (PSLNULL == msr)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Extract our object
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)msr;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);

    /*  Acquire the mutex if we have one
     */

    if (PSLNULL != pmsrObj->hMutex)
    {
        ps = PSLMutexAcquire(pmsrObj->hMutex, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        hMutexRelease = pmsrObj->hMutex;
    }

    /*  Deal with services if we have some
     */

    if (PSLNULL != pmsrObj->dlServices)
    {
        /*  We should be guarded by a mutex
         */

        PSLASSERT(PSLNULL != hMutexRelease);

        /*  Retrieve the number of entries in the list
         */

        ps = PSLDynListGetSize(pmsrObj->dlServices, &cServices);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Walk through the services from the end to unregister the
         *  services.  Service shutdown may cause the item to be removed
         *  from the list before we expect it to be and would cause an
         *  infinite loop or double-removes if we went the other way.
         */

        for (idxService = cServices; idxService > 0; idxService--)
        {
            /*  Retrieve the next service to close
             */

            ps = PSLDynListGetItem(pmsrObj->dlServices, (idxService - 1),
                            (PSLPARAM*)&mtpService);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  And close the service- we ignore the error here as we
             *  will forget about the service anyway
             */

            ps = MTPServiceClose(mtpService);
            if (PSL_FAILED(ps))
            {
                PSLTraceError("MTPServiceClose failed for service id 0x%08x",
                            MTPServiceGetServiceID(mtpService));
            }
        }
    }

    /*  Release the service list- we assert if the list is not empty
     */

    SAFE_PSLDYNLISTDESTROY(pmsrObj->dlServices);
    SAFE_PSLBKTHASHDESTROY(pmsrObj->bhServiceID);
    SAFE_PSLBKTHASHDESTROY(pmsrObj->bhStorageID);
    SAFE_PSLBKTHASHDESTROY(pmsrObj->bhFormatCode);

    /*  Release the legacy DB session if we have one
     */

    SAFE_MTPDBSESSIONCLOSE(pmsrObj->mtpdbLegacy);

    /*  Remember that we need to close the mutex as well
     */

    hMutexClose = hMutexRelease;

    /*  And release the object
     */

    PSLObjectPoolFree(pmsrObj);
    ps = PSLSUCCESS;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_PSLMUTEXCLOSE(hMutexClose);
    return ps;
}


/*
 *  MTPServiceResolverFindService
 *
 *  Resolves the information provided to the specific service it is associated
 *  with.
 *
 *  Arguments:
 *      MTPSERVICERESOLVER
 *                      msr                 Service resolver to use
 *      PSLUINT32       dwJoinType          Join operation to perform on
 *                                            the properties
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              Properties defining the service
 *      PSLUINT32       cmdbv               Number of properties
 *      MTPSERVICE*     pmtpService         Resulting service
 *
 *  Returns:
 *      PSLSUCCESS      PSLSUCCESS if found, PSLSUCCESS_NOT_FOUND otherwise
 *
 */

PSLSTATUS PSL_API MTPServiceResolverFindService(MTPSERVICERESOLVER msr,
                    PSLUINT32 dwJoinType, PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv, MTPSERVICE* pmtpService)
{
    PSLPARAM                aKey;
    PSLBKTHASH              bhCache;
    PSLUINT32               dwServiceID = MTP_SERVICEID_UNDEFINED;
    PSLUINT32               dwStorageID = MTP_STORAGEID_UNDEFINED;
    PSLUINT32               dwStorageIDTemp;
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLUINT32               idx;
    PSLUINT32               idxService = 0;
    PSLSTATUS               ps;
    MTPSERVICERESOLVEROBJ*  pmsrObj;
    PFNDYNLISTCOMPARE       pfnCompare;
    PSLUINT16               wFormatCode = 0;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpService)
    {
        *pmtpService = PSLNULL;
    }

    /*  Parameter Validation
     */

    if ((PSLNULL == msr) || (MTPJOINTYPE_AND != dwJoinType) ||
        (PSLNULL == pmtpService))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the resolver information
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)msr;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);

    /*  Walk through the provided properties looking for information we can
     *  use to resolve the service
     */

    for (idx = 0; idx < cmdbv; idx++)
    {
        /*  Check each property for information we can use to resolve the
         *  service
         */

        switch(rgmdbv[idx].wPropCode)
        {
        case MTP_OBJECTPROPCODE_SERVICEID:
            PSLASSERT(MTP_SERVICEID_UNDEFINED == dwServiceID);
            dwServiceID = rgmdbv[idx].vParam.valUInt32;
            if (MTP_SERVICEID_UNDEFINED == dwServiceID)
            {
                PSLASSERT(PSLFALSE);
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }
            break;

        case MTP_OBJECTPROPCODE_STORAGEID:
            /*  Ignore the storage ID all case- it does not help resolve
             *  the service
             */

            if (MTP_STORAGEID_ALL == rgmdbv[idx].vParam.valUInt32)
            {
                break;
            }

            /*  We may have already retrieved a storage ID from an object
             *  handle- if we did we should make sure that they match
             */

            PSLASSERT((MTP_STORAGEID_UNDEFINED == dwStorageID) ||
                            (rgmdbv[idx].vParam.valUInt32 == dwStorageID));
            if ((MTP_STORAGEID_UNDEFINED != dwStorageID) &&
                            (rgmdbv[idx].vParam.valUInt32 != dwStorageID))
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }
            dwStorageID = rgmdbv[idx].vParam.valUInt32;
            break;

        case MTP_OBJECTPROPCODE_OBJECTHANDLE:
            /*  If we have the "all" handle then this is not going to help
             *  resolve the location- skip it
             */

            if (MTP_OBJECTHANDLE_ALL == rgmdbv[idx].vParam.valUInt32)
            {
                break;
            }

            /*  FALL THROUGH INTENTIONAL */

        case MTP_OBJECTPROPCODE_PARENT:
            /*  If this is the "parent" object handle then it is not going
             *  to provide valid data and we should skip it
             */

            if ((MTP_OBJECTPROPCODE_PARENT == rgmdbv[idx].wPropCode) &&
                (MTP_OBJECTHANDLE_ROOT == rgmdbv[idx].vParam.valUInt32))
            {
                break;
            }

            /*  If we do not have a defined object handle then this is
             *  not going to help
             */

            if (MTP_OBJECTHANDLE_UNDEFINED == rgmdbv[idx].vParam.valUInt32)
            {
                break;
            }

            /*  Figure out what the storage ID for this object is
             */

            ps = MTPDBResolveObjectHandleToStorageID(pmsrObj->pmtpctx,
                    rgmdbv[idx].vParam.valUInt32, &dwStorageIDTemp);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }

            /*  If we did not find a storage ID then ignore this entry
             */

            if (PSLSUCCESS != ps)
            {
                break;
            }

            /*  Make sure that the storage ID we retrieved from the object
             *  handle matches any storage ID we may have already been
             *  handed
             */

            PSLASSERT((MTP_STORAGEID_UNDEFINED == dwStorageID) ||
                            (dwStorageIDTemp == dwStorageID));
            if ((MTP_STORAGEID_UNDEFINED != dwStorageID) &&
                            (dwStorageIDTemp != dwStorageID))
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }
            dwStorageID = dwStorageIDTemp;
            break;

        case MTP_OBJECTPROPCODE_OBJECTFORMAT:
            PSLASSERT(0 == wFormatCode);
            wFormatCode = rgmdbv[idx].vParam.valUInt16;
            break;

        case MTP_OBJECTPROPCODE_DEPTH:
        case MTP_SERVICEPROPCODE_PROPCODE:
            /*  These values do not help isolate the service- ignore them
             */

            break;

        default:
            PSLTraceWarning(
                "MTPServiceResolverFindService - Skipping PropCode 0x%04x",
                rgmdbv[idx].wPropCode);
            break;
        }
    }

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmsrObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmsrObj->hMutex;

    /*  If we do not have a service list we are not going to locate a service
     */

    if (PSLNULL == pmsrObj->dlServices)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Walk through what we found to figure out what we need to do
     *
     *  NOTE: The code assumes that the result of the search is in ps
     *  and idxService by the end of this if statement
     */

    if (MTP_SERVICEID_UNDEFINED != dwServiceID)
    {
        /*  We were handed a service ID- use the service ID functions
         */

        aKey = dwServiceID;
        bhCache = pmsrObj->bhServiceID;
        pfnCompare = _CompareServiceID;
    }
    else if ((MTP_STORAGEID_UNDEFINED != dwStorageID) &&
                            (MTP_STORAGEID_ALL != dwStorageID))

    {
        /*  We found a storage ID- use the storage ID functions
         */

        aKey = dwStorageID;
        bhCache = pmsrObj->bhStorageID;
        pfnCompare = _CompareStorageID;
    }
    else if (0 != wFormatCode)
    {
        /*  We found a format code- use the format code functions
         */

        aKey = wFormatCode;
        bhCache = pmsrObj->bhFormatCode;
        pfnCompare = _CompareFormatCode;
    }
    else
    {
        /*  We did not find anything that identifies the service associated
         *  with the specified properties- report not found and quit working
         */

        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  First check the cache to see if we have already done the hard work
     */

    ps = PSLBktHashFind(bhCache, aKey, (PSLPARAM*)pmtpService);
    if ((PSLSUCCESS == ps) || PSL_FAILED(ps))
    {
        /*  We either found it or encountered an error- either way no more
         *  work to do
         */

        goto Exit;
    }

    /*  Search the services list for it
     */

    ps = PSLDynListFindItem(pmsrObj->dlServices, 0, aKey, pfnCompare,
                        &idxService);
    if (PSLSUCCESS != ps)
    {
        /*  It's either not there or we ran into a problem- either way no
         *  more work to do
         */

        goto Exit;
    }

    /*  Retrieve the service from the list
     */

    ps = PSLDynListGetItem(pmsrObj->dlServices, idxService,
                            (PSLPARAM*)pmtpService);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  All is well- attempt to cache the service so we do not have to
     *  search through everything next time.  We ignore the error return
     *  here because only performance will suffer if we cannot add the item
     *  to the cache.
     */

    PSLBktHashInsert(bhCache, aKey, (PSLPARAM)*pmtpService);

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPServiceResolverGetLegacyService
 *
 *  Retrieves the registered legacy service associated with this resolver.
 *
 *  Arguments:
 *      MTPSERVICERESOLVER
 *                      msr                 Service resolver to use
 *      MTPDBSESSION*   pmtpdbLegacy        Resulting legacy service
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPServiceResolverGetLegacyService(MTPSERVICERESOLVER msr,
                    MTPDBSESSION* pmtpdbLegacy)
{
    PSLHANDLE               hMutexRelease = PSLNULL;
    PSLSTATUS               ps;
    MTPSERVICERESOLVEROBJ*  pmsrObj;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpdbLegacy)
    {
        *pmtpdbLegacy = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == msr) || (PSLNULL == pmtpdbLegacy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)msr;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmsrObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmsrObj->hMutex;

    /*  And extract the handle to the legacy DB
     */

    *pmtpdbLegacy = pmsrObj->mtpdbLegacy;
    ps = (PSLNULL != *pmtpdbLegacy) ? PSLSUCCESS : PSLSUCCESS_FALSE;

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPServiceResolverDBContextOpen
 *
 *  Opens an MTP DB Context based on the information provided
 *
 *  Arguments:
 *      MTPSERVICERESOLVER
 *                      msr                 Service resolver to use
 *      PSLUINT32       dwJoinType          Join operation to perform on
 *                                            the properties
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              Properties defining the service
 *      PSLUINT32       cmdbv               Number of properties
 *      MTPDBCONTEXT*   pmtpdbContext       Resulting DB Context
 *
 *  Returns:
 *      PSLSUCCESS      PSLSUCCESS if found, PSLSUCCESS_NOT_FOUND otherwise
 *
 */


PSLSTATUS PSL_API MTPServiceResolverDBContextOpen(MTPSERVICERESOLVER msr,
                    PSLUINT32 dwJoinType, PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv, MTPDBCONTEXT* pmtpDBContext)
{
    PSLHANDLE               hMutexRelease = PSLNULL;
    MTPSERVICE              mtpService = PSLNULL;
    PSLSTATUS               ps;
    MTPSERVICERESOLVEROBJ*  pmsrObj;

    /*  Clear result for safety
     */

    if (PSLNULL != pmtpDBContext)
    {
        *pmtpDBContext = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == msr) || (PSLNULL == pmtpDBContext))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)msr;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmsrObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmsrObj->hMutex;

    /*  Perform the general resolve to try and get the service
     */

    ps = MTPServiceResolverFindService(msr, dwJoinType, rgmdbv, cmdbv,
                            &mtpService);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  If we got a service back use it to retrieve the correct DB context
     */

    if (PSLSUCCESS == ps)
    {
        /*  Use the service to retrieve a context
         */

        ps = MTPServiceDBContextOpen(mtpService, dwJoinType, rgmdbv, cmdbv,
                            pmtpDBContext);
        goto Exit;
    }

    /*  We did not locate a service- we need to use the legacy service to
     *  retrieve the context.  Make sure we have one
     */

    if (PSLNULL == pmsrObj->mtpdbLegacy)
    {
        ps = PSLERROR_NOT_AVAILABLE;
        goto Exit;
    }

    /*  Open the legacy DB Context
     */

    ps = MTPDBSessionDBContextOpen(pmsrObj->mtpdbLegacy, dwJoinType, rgmdbv,
                            cmdbv, pmtpDBContext);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    return ps;
}


/*
 *  MTPServiceResolverServiceContextDataOpen
 *
 *  Opens an MTP Service Context Data based on the information provided
 *
 *  Arguments:
 *      MTPSERVICERESOLVER
 *                      msr                 Service resolver to use
 *      PSLUINT32       dwDataFormat        Format of the data to return
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              Properties defining the service
 *      PSLUINT32       cmdbv               Number of properties
 *      PSLUINT64*      pqwBufSize          Size of buffer to hold data
 *      MTPSERVICECONTEXTDATA*
 *                      pmscd               Resulting Service Context Data
 *
 *  Returns:
 *      PSLSUCCESS      PSLSUCCESS if found, PSLSUCCESS_NOT_FOUND otherwise
 *
 */


PSLSTATUS PSL_API MTPServiceResolverServiceContextDataOpen(
                    MTPSERVICERESOLVER msr, PSLUINT32 dwDataFormat,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv, PSLUINT32 cmdbv,
                    PSLUINT64* pqwBufSize, MTPSERVICECONTEXTDATA* pmscd)
{
    PSLHANDLE               hMutexRelease = PSLNULL;
    MTPSERVICE              mtpService = PSLNULL;
    MTPSERVICECONTEXTDATA   mscdServiceID = PSLNULL;
    PSLSTATUS               ps;
    MTPSERVICERESOLVEROBJ*  pmsrObj;

    /*  Clear result for safety
     */

    if (PSLNULL != pmscd)
    {
        *pmscd = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == msr) || (PSLNULL == pmscd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)msr;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);

    /*  Acquire the mutex
     */

    ps = PSLMutexAcquire(pmsrObj->hMutex, PSLTHREAD_INFINITE);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    hMutexRelease = pmsrObj->hMutex;

    /*  Check for the easy case- the service ID data format is always handled
     *  by a custom context data and not forwarded to each service
     */

    if (MTPDBCONTEXTDATAFORMAT_SERVICEID ==
                            MTPDBCONTEXTDATADESC_FORMAT(dwDataFormat))
    {
        /*  We only support query requests
         */

        if (MTPDBCONTEXTDATA_QUERY != MTPDBCONTEXTDATADESC_FLAGS(dwDataFormat))
        {
            ps = PSLERROR_INVALID_OPERATION;
            goto Exit;
        }

        /*  Create a context data for this operation
         */

        ps = _MTPServiceIDContextDataCreate(pmsrObj->dlServices,
                            &mscdServiceID);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  And determine the size of the data that we will be returning
         */

        ps = MTPServiceContextDataSerialize(mscdServiceID, PSLNULL, 0,
                            PSLNULL, pqwBufSize);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Return the context data
         */

        *pmscd = mscdServiceID;
        mscdServiceID = PSLNULL;
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Perform the general resolve to try and get the service
     */

    ps = MTPServiceResolverFindService(msr, MTPJOINTYPE_AND, rgmdbv, cmdbv,
                            &mtpService);
    if (PSLSUCCESS != ps)
    {
        ps = PSL_FAILED(ps) ? ps : MTPERROR_SERVICE_INVALID_SERVICEID;
        goto Exit;
    }

    /*  Use the service to retrieve a service context data
     */

    ps = MTPServiceContextDataOpen(mtpService, dwDataFormat, rgmdbv, cmdbv,
                        pqwBufSize, pmscd);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    SAFE_PSLMUTEXRELEASE(hMutexRelease);
    SAFE_MTPSERVICECONTEXTDATACLOSE(mscdServiceID);
    return ps;
}


/*
 *  MTPServiceRegister
 *
 *  Registers an MTP Device Service
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP Context to register service
 *                                            within
 *      MTPSERVICE      mtpService          Context handle for service to
 *                                            be registered
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE if already
 *                        registered
 *
 */

PSLSTATUS PSL_API MTPServiceRegister(MTPCONTEXT* pmtpctx, MTPSERVICE mtpService)
{
    PSLSTATUS               ps;
    BASEHANDLERDATA*        pData;
    MTPSERVICERESOLVEROBJ*  pmsrObj;

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == mtpService))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get access to the core per-session data
     */

    pData = (BASEHANDLERDATA*)pmtpctx->mtpHandlerState.pbHandlerData;

    /*  The resolver is created when the context is created- if it is not
     *  there we have a problem
     */

    PSLASSERT(PSLNULL != pData->sesLevelData.msrServices);
    if (PSLNULL == pData->sesLevelData.msrServices)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Extract our object
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)pData->sesLevelData.msrServices;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);
    PSLASSERT(pmtpctx == pmsrObj->pmtpctx);
    if (pmtpctx != pmsrObj->pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  And add the service to the resolver
     */

    ps = _MTPServiceResolverAddService(pmsrObj, mtpService);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPServiceUnregister
 *
 *  Unregisters an MTP Service
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP Context to unregister service
 *                                            within
 *      MTPSERVICE      mtpService          Context handle for service to
 *                                            be unregistered
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if not
 *                        found
 *
 */


PSLSTATUS PSL_API MTPServiceUnregister(MTPCONTEXT* pmtpctx,
                    MTPSERVICE mtpService)
{
    PSLSTATUS               ps;
    BASEHANDLERDATA*        pData;
    MTPSERVICERESOLVEROBJ*  pmsrObj;

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == mtpService))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get access to the core per-session data
     */

    pData = (BASEHANDLERDATA*)pmtpctx->mtpHandlerState.pbHandlerData;

    /*  If we do not have a service resolver than we cannot locate the service
     */

    if (PSLNULL == pData->sesLevelData.msrServices)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Extract our object
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)pData->sesLevelData.msrServices;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);
    PSLASSERT(pmtpctx == pmsrObj->pmtpctx);
    if (pmtpctx != pmsrObj->pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  And remove the service from the resolver
     */

    ps = _MTPServiceResolverRemoveService(pmsrObj, mtpService);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPServiceRegisterLegacyService
 *
 *  Registers the legacy service DB Session
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP Context to register DB Session
 *      MTPDBSESSION    mtpdbLegacy         DB Session to register
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLERROR_NOT_AVAILABLE if
 *                        already registered
 */

PSLSTATUS PSL_API MTPServiceRegisterLegacyService(MTPCONTEXT* pmtpctx,
                    MTPDBSESSION mtpdbLegacy)
{
    PSLSTATUS               ps;
    BASEHANDLERDATA*        pData;
    MTPSERVICERESOLVEROBJ*  pmsrObj;

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == mtpdbLegacy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get access to the core per-session data
     */

    pData = (BASEHANDLERDATA*)pmtpctx->mtpHandlerState.pbHandlerData;

    /*  The resolver is created when the context is created- if it is not
     *  there we have a problem
     */

    PSLASSERT(PSLNULL != pData->sesLevelData.msrServices);
    if (PSLNULL == pData->sesLevelData.msrServices)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Extract our object
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)pData->sesLevelData.msrServices;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);
    PSLASSERT(pmtpctx == pmsrObj->pmtpctx);
    if (pmtpctx != pmsrObj->pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  And add the legacy DB Session to the resolver
     */

    ps = _MTPServiceResolverAddDBSession(pmsrObj, mtpdbLegacy);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPServiceUnregisterLegacyService
 *
 *  Unregisters the legacy service DB Session
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP Context to unregister DB Session
 *      MTPDBSESSION    mtpdbLegacy         DB Session to unregister
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_NOT_FOUND if
 *                        not found
 */

PSLSTATUS PSL_API MTPServiceUnregisterLegacyService(MTPCONTEXT* pmtpctx,
                    MTPDBSESSION mtpdbLegacy)
{
    PSLSTATUS               ps;
    BASEHANDLERDATA*        pData;
    MTPSERVICERESOLVEROBJ*  pmsrObj;

    /*  Validate Arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == mtpdbLegacy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Get access to the core per-session data
     */

    pData = (BASEHANDLERDATA*)pmtpctx->mtpHandlerState.pbHandlerData;

    /*  If we do not have a service resolver than we cannot locate the service
     */

    if (PSLNULL == pData->sesLevelData.msrServices)
    {
        ps = PSLSUCCESS_NOT_FOUND;
        goto Exit;
    }

    /*  Extract our object
     */

    pmsrObj = (MTPSERVICERESOLVEROBJ*)pData->sesLevelData.msrServices;
    PSLASSERT(MTPSERVICERESOLVER_COOKIE == pmsrObj->dwCookie);
    PSLASSERT(pmtpctx == pmsrObj->pmtpctx);
    if (pmtpctx != pmsrObj->pmtpctx)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Remove the DBSession from the resolver
     */

    ps = _MTPServiceResolverRemoveDBSession(pmsrObj, mtpdbLegacy);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}



