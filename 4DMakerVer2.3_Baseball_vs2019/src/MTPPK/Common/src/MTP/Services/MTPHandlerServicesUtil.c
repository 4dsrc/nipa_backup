/*
 *  MTPHandlerServicesUtil.c
 *
 *  Contains the implementation of utility functions used by
 *  service handlers.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPServicePrecomp.h"
#include "MTPServiceHandlerUtil.h"
#include "MTPHandlerUtil.h"

PSLSTATUS _MTPServiceHandleRequestPhase(MTPDISPATCHER md, PSLMSG* pMsg,
                            PSLUINT32 dwHandlerState,
                            PFNSERVICECONTEXTPREPARE pfnContextPrepare)
{
    PSLSTATUS           status;
    PSLUINT16           wRespCode = MTP_RESPONSECODE_OK;

    MTPCONTEXT*         pmc     = PSLNULL;
    SERVICEHANDLERDATA* pData   = PSLNULL;
    PSLMSG*             pRetMsg = PSLNULL;

    PXMTPOPERATION      pOpRequest = PSLNULL;
    PSLUINT64           qwBufSize = 0;

    PSLUINT32           dwMsgFlags;

    if (PSLNULL == md || PSLNULL == pMsg ||
        PSLNULL == pfnContextPrepare)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPHandlerBegin(md, pMsg, &pmc, (BASEHANDLERDATA**)&pData,
                               &pRetMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* clear our data */

    pData->mscd = PSLNULL;

    pOpRequest = (PXMTPOPERATION)pMsg->aParam2;
    PSLASSERT(PSLNULL != pOpRequest);

    /* init response */
    (PSLVOID)FillResponse(
                &pData->bhd.opLevelData.mtpResponse,
                wRespCode, MTPLoadUInt32(&(pOpRequest->dwTransactionID)),
                PSLNULL, PSLNULL, PSLNULL, PSLNULL, PSLNULL);

    /*
     * call for prepare function, some of which will save context in
     * pData while some others will fill response directly.
     */
    status = pfnContextPrepare(pOpRequest, pMsg->alParam,
                            (BASEHANDLERDATA*)pData, &qwBufSize);

Exit:
    if (PSLNULL != pRetMsg)
    {
       (PSLVOID)MTPHandlerGetResponseCodeFromStatus(status, &wRespCode);

        /* reset reponse on failure. */
        if ((PSLNULL != pData) && (PSLNULL != pOpRequest) &&
            (MTP_RESPONSECODE_OK != wRespCode))
        {
            (PSLVOID)FillResponse(
                    &pData->bhd.opLevelData.mtpResponse,
                    wRespCode, MTPLoadUInt32(&(pOpRequest->dwTransactionID)),
                    PSLNULL, PSLNULL, PSLNULL, PSLNULL, PSLNULL);

            pData->bhd.opLevelData.dwNumRespParams = 0;
        }

        /* prepare for next message. */
        (PSLVOID)MTPHandlerSetState(pmc, wRespCode, dwHandlerState,
                                     &dwMsgFlags, &qwBufSize);

        /* notify transport */
        if ((PSLNULL != pmc) && (PSLNULL != pMsg))
        {
            status = MTPHandlerRouteMsgToTransport(pmc, pRetMsg, pMsg,
                                       MTPMSG_CMD_CONSUMED,
                                       (PSLBYTE*)pMsg->aParam2,
                                       qwBufSize, dwMsgFlags);
            if (PSL_SUCCEEDED(status))
            {
                pRetMsg = PSLNULL;
            }
        }
    }
    else
    {
        /* we did not alloc return msg, no much we can do. */
        if (PSLNULL != pData)
        {
            SAFE_MTPSERVICECONTEXTDATACLOSE(pData->mscd);
        }
        (PSLVOID)GetReadyForNextCommand(md, pmc);
    }

    MTPHandlerEnd(pRetMsg);
    return status;
}

PSLSTATUS _MTPServiceHandleGetData(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS status;
    MTPCONTEXT* pmc = PSLNULL;
    SERVICEHANDLERDATA* pData = PSLNULL;
    PSLMSG* pRetMsg = PSLNULL;
    PSLUINT32 dwBufSize = 0;
    PSLUINT32 dwDataWritten;
    PSLUINT64 dwDataLeft;
    PSLUINT32 dwMsgFlags = 0;
    PSLUINT16 wRespCode = MTP_RESPONSECODE_OK;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPHandlerBegin(md, pMsg, &pmc, (BASEHANDLERDATA**)&pData,
                               &pRetMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(MTPHANDLERSTATE_DATA_OUT ==
                        pmc->mtpHandlerState.dwHandlerState);

    PSLASSERT(MTP_RESPONSECODE_OK ==
                    pData->bhd.opLevelData.mtpResponse.wRespCode);

    if (PSLNULL != (PSLVOID*)pMsg->aParam2)
    {
        dwBufSize = (PSLUINT32)pMsg->alParam;

        /* copy ObjectHandles to the buffer from transport */
        status = MTPServiceContextDataSerialize(pData->mscd,
                                           (PSLVOID*)pMsg->aParam2,
                                           dwBufSize, &dwDataWritten,
                                           &dwDataLeft);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        if (0 == dwDataLeft)
        {
            /* if the data remaining is zero or if the buffer
             * size from transport is big enough to hold the
             * entire data we are done with data phase and
             * ready to send response
             */
            PSLASSERT(dwDataWritten <= (PSLUINT32)pMsg->alParam);

            pmc->mtpHandlerState.dwHandlerState =
                                    MTPHANDLERSTATE_RESPONSE;
            dwMsgFlags  = MTPMSGFLAG_TERMINATE|
                        MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
        }
        else
        {
            /* request addtional data buffer, the size
             * of the buffer would still be the same as
             * the size of initial buffer requested.
             */
            PSLASSERT(0 != dwDataLeft);

            pmc->mtpHandlerState.dwHandlerState =
                                    MTPHANDLERSTATE_DATA_OUT;
            dwMsgFlags  = MTPMSGFLAG_DATA_BUFFER_REQUEST;
        }

        PSLASSERT(0 != dwDataWritten);

        /* amount of data filled in the buffer */
        dwBufSize = dwDataWritten;
    }
    else
    {
        /* close the context data */
        SAFE_MTPSERVICECONTEXTDATACLOSE(pData->mscd);

        /* if the buffer provided by the tranport is PSLNULL
         * that means it is in a state where it is not able to
         * provide any buffer in which case we should cleanup
         * everything and get to a state to handler next command
         */
        (PSLVOID)GetReadyForNextCommand(md, pmc);
    }
Exit:

    if ((PSLNULL != pMsg) && (PSLNULL != (PSLVOID*)pMsg->aParam2))
    {
        if (PSL_FAILED(status))
        {
            (PSLVOID)MTPHandlerGetResponseCodeFromStatus(status, &wRespCode);

            if (PSLNULL != pmc)
            {
                /* request for a response buffer on failure */
                pmc->mtpHandlerState.dwHandlerState =
                                            MTPHANDLERSTATE_RESPONSE;
            }
            dwMsgFlags = MTPMSGFLAG_RESPONSE_BUFFER_REQUEST |
                            MTPMSGFLAG_TERMINATE;
            dwBufSize  = 0;
            if (PSLNULL != pData)
            {
                pData->bhd.opLevelData.mtpResponse.wRespCode = wRespCode;
            }
        }


        if ((PSLNULL != pmc) && (PSLNULL != pMsg))
        {
            status = MTPHandlerRouteMsgToTransport(pmc, pRetMsg, pMsg,
                                        MTPMSG_DATA_READY_TO_SEND,
                                        (PSLBYTE*)pMsg->aParam2,
                                        dwBufSize, dwMsgFlags);
            if (PSL_SUCCEEDED(status))
            {
                pRetMsg = PSLNULL;
            }
        }
    }

    MTPHandlerEnd(pRetMsg);
    return status;
}

PSLSTATUS _MTPServiceHandleGetResponse(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS status;
    MTPCONTEXT* pmc;
    SERVICEHANDLERDATA* pData;
    PSLMSG* pRetMsg = PSLNULL;
    PSLUINT32 dwBufSize = 0;
    PSLUINT32 dwMsgFlags = 0;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPHandlerBegin(md, pMsg, &pmc, (BASEHANDLERDATA**)&pData,
                               &pRetMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* this msg is expected after we send out a msg with flag
     * MTPMSGFLAG_RESPONSE_BUFFER_REQUEST.
     */
    PSLASSERT(MTPHANDLERSTATE_RESPONSE ==
                        pmc->mtpHandlerState.dwHandlerState);

    PSLASSERT((PSLUINT32)pMsg->alParam >=
                                    sizeof(MTPRESPONSE));

    if (PSLNULL != (PSLVOID*)pMsg->aParam2)
    {
        PSLCopyMemory((PSLVOID*)pMsg->aParam2,
                        &pData->bhd.opLevelData.mtpResponse,
                        (PSLUINT32)pMsg->alParam);

        /* amount of data filled in response buffer */
        dwBufSize = sizeof(MTPRESPONSE) -
                            ((MTPRESPONSE_NUMPARAMS_MAX - \
                            pData->bhd.opLevelData.dwNumRespParams) * \
                            sizeof(PSLUINT32));
    }

    (PSLVOID)GetReadyForNextCommand(md, pmc);

    /* close the context data */
    SAFE_MTPSERVICECONTEXTDATACLOSE(pData->mscd);

    status = MTPHandlerRouteMsgToTransport(pmc, pRetMsg, pMsg,
                                   MTPMSG_RESPONSE,
                                   (PSLBYTE*)pMsg->aParam2,
                                   dwBufSize, dwMsgFlags);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pRetMsg = PSLNULL;

Exit:
    MTPHandlerEnd(pRetMsg);
    return status;
}

PSLSTATUS _MTPServiceHandleSetData(MTPDISPATCHER md, PSLMSG* pMsg,
                PFNSERVICECONTEXTDATACONSUMED pfnContextDataConsumed)
{
    PSLSTATUS           status    = PSLSUCCESS;
    PSLUINT16           wRespCode;

    MTPCONTEXT*         pmc     = PSLNULL;
    BASEHANDLERDATA*    pData   = PSLNULL;
    PSLMSG*             pRetMsg = PSLNULL;
    PSLUINT32           dwFlags = 0;

    PSLMSGQUEUE         mqHandler;
    PSLBOOL             bDeserialized = PSLFALSE;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPHandlerBegin(md, pMsg, &pmc, &pData, &pRetMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    PSLASSERT(MTPHANDLERSTATE_DATA_IN ==
                pmc->mtpHandlerState.dwHandlerState);

    if ( MTP_RESPONSECODE_OK !=
                pData->opLevelData.mtpResponse.wRespCode )
    {
        /* if we already failed in previous message for this command,
         * we should just drop the data.
         */
        goto Exit;
    }

    /* make sure that we received valid data */

    if (((PSLNULL == (PSLVOID*)pMsg->aParam2) || (0 == pMsg->alParam)) &&
        !(pMsg->aParam3 & MTPMSGFLAG_TERMINATE))
    {
        status = PSLERROR_INVALID_DATA;
        goto Exit;
    }

    status = MTPDispatcherGetMsgQueue(md, &mqHandler);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* pass buffer to deserializer */
    if ((PSLNULL != (PSLVOID*)pMsg->aParam2) && (0 != pMsg->alParam))
    {
        status = MTPServiceContextDataDeserialize(
                                ((SERVICEHANDLERDATA*)pData)->mscd,
                                (PSLBYTE*)pMsg->aParam2,
                                (PSLUINT32)pMsg->alParam,
                                mqHandler, pmc, 0, pMsg->aParam3);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }

        bDeserialized = PSLTRUE;
    }
    else if (pMsg->aParam3 & MTPMSGFLAG_TERMINATE)
    {
        /* Handle the data consumed since there will be no consumed message */

        status = pfnContextDataConsumed(pData);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

Exit:
    (PSLVOID)MTPHandlerGetResponseCodeFromStatus(status, &wRespCode);

    /* save the new resp code if we were OK before.*/
    if ((PSLNULL != pData) && (MTP_RESPONSECODE_OK ==
        pData->opLevelData.mtpResponse.wRespCode))
    {
        pData->opLevelData.mtpResponse.wRespCode = wRespCode;
    }

    /*
     * we need to send message if we did not get to deserialize, which
     * could be because error happened or an empty buffer. In which
     * case we need to drop buffer and send data consumed. otherwise,
     * we need to wait for data consumed message from deserializer.
     */
    if (PSLTRUE != bDeserialized)
    {
        if ((PSLNULL != pRetMsg) && (PSLNULL != pmc) &&
            (PSLNULL != pData) && (PSLNULL != pMsg))
        {
            /* last buffer, we move on to response phase.*/
            if (pMsg->aParam3 & MTPMSGFLAG_TERMINATE)
            {
                pmc->mtpHandlerState.dwHandlerState =
                                         MTPHANDLERSTATE_RESPONSE;

                dwFlags =  MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
            }

            /*
             * tell transport we are done with this data buffer,
             * if this is the last buffer, we request response
             * buffer.
             */
            status = MTPHandlerRouteMsgToTransport(
                        pmc, pRetMsg, pMsg,
                        MTPMSG_DATA_CONSUMED,
                        (PSLBYTE*)pMsg->aParam2,
                        (PSLLPARAM)pMsg->alParam,
                        dwFlags);
            if (PSL_SUCCEEDED(status))
            {
                pRetMsg = PSLNULL;
            }
        }
        else
        {
            /* no msg for response, just get ready for next command*/
            if ( PSLNULL != pData )
            {
                SAFE_MTPSERVICECONTEXTDATACLOSE(
                    ((SERVICEHANDLERDATA*)pData)->mscd);
            }
            (PSLVOID)GetReadyForNextCommand(md, pmc);
        }
    }

    MTPHandlerEnd(pRetMsg);
    return status;
}

PSLSTATUS _MTPServiceHandleDataConsumed(MTPDISPATCHER md, PSLMSG* pMsg,
                 PFNSERVICECONTEXTDATACONSUMED pfnContextDataConsumed)
{
    PSLSTATUS           status;
    PSLSTATUS           statusDeserialize = PSLSUCCESS;
    PSLUINT16           wRespCode;

    MTPCONTEXT*         pmc     = PSLNULL;
    BASEHANDLERDATA*    pData   = PSLNULL;
    PSLMSG*             pRetMsg = PSLNULL;
    PSLUINT32           dwFlags = 0;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPHandlerBegin(md, pMsg, &pmc, &pData, &pRetMsg);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* if this is last data transfer, commit changes to database.*/
    if (pMsg->aParam3 & MTPMSGFLAG_TERMINATE)
    {
        status = pfnContextDataConsumed(pData);
    }

Exit:
    if ((PSLNULL != pRetMsg) && (PSLNULL != pmc) &&
        (PSLNULL != pData) && (PSLNULL != pMsg))
    {
        (PSLVOID)MTPHandlerGetResponseCodeFromStatus(status,&wRespCode);

        if ((PSLNULL != pData) && (MTP_RESPONSECODE_OK ==
            pData->opLevelData.mtpResponse.wRespCode))
        {
            /* if we are in good state before this, update resp code.*/
            if (PSL_FAILED(status))
            {
                pData->opLevelData.mtpResponse.wRespCode = wRespCode;
            }
            else if (PSL_FAILED(statusDeserialize))
            {
               (PSLVOID)MTPHandlerGetResponseCodeFromStatus(
                                        statusDeserialize, &wRespCode);
                pData->opLevelData.mtpResponse.wRespCode = wRespCode;
            }
        }

        /* if this is the last buffer, we move on to response phase.*/
        if (pMsg->aParam3 & MTPMSGFLAG_TERMINATE)
        {
            pmc->mtpHandlerState.dwHandlerState =
                                     MTPHANDLERSTATE_RESPONSE;

            dwFlags =  MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
        }

        /*
         * tell transport we are done with this data buffer, if this
         * is the last buffer, we request response buffer.
         */
        status = MTPHandlerRouteMsgToTransport(
                    pmc, pRetMsg, pMsg,
                    MTPMSG_DATA_CONSUMED,
                    (PSLBYTE*)pMsg->aParam2,
                    (PSLLPARAM)pMsg->alParam,
                    dwFlags);
        if (PSL_SUCCEEDED(status))
        {
            pRetMsg = PSLNULL;
        }
    }
    else
    {
        /* no msg for response, just get ready for next command*/
        if ( PSLNULL != pData )
        {
            SAFE_MTPDBCONTEXTDATACLOSE(
                ((SERVICEHANDLERDATA*)pData)->mscd);
        }
        (PSLVOID)GetReadyForNextCommand(md, pmc);
    }

    MTPHandlerEnd(pRetMsg);
    return status;
}

PSLSTATUS MTPHandleServiceOperations(
                MTPDISPATCHER md,
                PSLMSG* pMsg,
                PSLUINT32 dwHandlerState,
                PFNSERVICECONTEXTPREPARE pfnContextPrepare,
                PFNSERVICECONTEXTDATACONSUMED pfnContextDataConsumed)
{
    PSLSTATUS status = PSLSUCCESS;
    MTPCONTEXT* pmc;
    SERVICEHANDLERDATA* pData;

    if (PSLNULL == md || PSLNULL == pMsg ||
        PSLNULL == pfnContextPrepare)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    switch (pMsg->msgID)
    {
        case MTPMSG_CMD_AVAILABLE:
            status = _MTPServiceHandleRequestPhase(md, pMsg,
                                           dwHandlerState,
                                           pfnContextPrepare);
            break;

        case MTPMSG_DATA_BUFFER_AVAILABLE:
            status = _MTPServiceHandleGetData(md, pMsg);
            break;

        case MTPMSG_DATA_SENT:
            /* do nothing */
            status = PSLSUCCESS;
            break;

        case MTPMSG_DATA_AVAILABLE:
            status = _MTPServiceHandleSetData(md, pMsg,
                                            pfnContextDataConsumed);
            break;

        case MTPMSG_DATA_CONSUMED:
            status = _MTPServiceHandleDataConsumed(md, pMsg,
                                            pfnContextDataConsumed);
            break;

        case MTPMSG_RESPONSE_BUFFER_AVAILABLE:
            status = _MTPServiceHandleGetResponse(md, pMsg);
            break;

        case MTPMSG_CANCEL:
        case MTPMSG_SHUTDOWN:
        case MTPMSG_DISCONNECT:
        case MTPMSG_RESET:
        case MTPMSG_ABORT:
            pmc = (MTPCONTEXT*)(pMsg->aParam0);
            if (PSLNULL != pmc)
            {
                pData = (SERVICEHANDLERDATA*) \
                                pmc->mtpHandlerState.pbHandlerData;
                if (PSLNULL != pData)
                {
                    SAFE_MTPSERVICECONTEXTDATACLOSE(pData->mscd);
                }
            }

            status = MTPDispatcherRouteTerminate(md,
                        (MTPCONTEXT*)(pMsg->aParam0), pMsg->msgID);
            break;

        default:
            status = PSLSUCCESS;
            break;
    }
Exit:
    return status;
}


