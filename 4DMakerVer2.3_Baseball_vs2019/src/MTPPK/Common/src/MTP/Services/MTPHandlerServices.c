/*
 *  MTPHandlerServices.c
 *
 *  Contains definition of the MTP services support
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPServicePrecomp.h"
#include "MTPServiceHandlerUtil.h"

/*  Local Functions
 */

static PSLSTATUS PSL_API _MTPContextPrepareServiceQuery(
                            PSLUINT32  dwDataFormat,
                            PXMTPOPERATION pOpRequest,
                            PSLUINT64 cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64* pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetServiceIDs(
                            PXMTPOPERATION pOpRequest,
                            PSLUINT64 cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64* pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetServiceInfo(
                            PXMTPOPERATION pOpRequest,
                            PSLUINT64 cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64* pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetServicePropDesc(
                            PXMTPOPERATION pOpRequest,
                            PSLUINT64 cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64* pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetServiceCapabilities(
                            PXMTPOPERATION pOpRequest,
                            PSLUINT64 cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64* pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetServicePropList(
                            PXMTPOPERATION pOpRequest,
                            PSLUINT64 cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64* pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareSetServicePropList(
                            PXMTPOPERATION pOpRequest,
                            PSLUINT64 cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64* pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareDeleteServicePropList(
                            PXMTPOPERATION pOpRequest,
                            PSLUINT64 cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64* pqwBufSize);

static PSLSTATUS PSL_API _MTPContextDataConsumedServicePropList(
                            BASEHANDLERDATA* pData);

/*
 *  _MTPContextPrepareServiceQuery
 *
 *  Prepares the context for querying a particular service data format
 *
 *  Arguments:
 *      PSLUINT32       dwDataFormat        Data format
 *      PXMTPOPERATION  pOpRequest          Operation details
 *      PSLUINT64       cbOpRequest         Size of the operation
 *      BASEHANDLERDATA*
 *                      pData               Base handler data
 *      PSLUINT64*      pqwBufSize          Size of return buffer requested
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API _MTPContextPrepareServiceQuery(
                    PSLUINT32  dwDataFormat,
                    PXMTPOPERATION pOpRequest, PSLUINT64 cbOpRequest,
                    BASEHANDLERDATA* pData, PSLUINT64* pqwBufSize)
{
    PSLUINT32               cmdbv;
    MTPSERVICECONTEXTDATA   mscd = PSLNULL;
    PSLSTATUS               ps;
    MTPDBVARIANTPROP        rgmdbv[2];

    /*  Clear results for safety
     */

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }
    ((SERVICEHANDLERDATA*)pData)->mscd = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == pOpRequest) || (PSLNULL == pData) ||
        (PSLNULL == pqwBufSize))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Initialze the context according to the data format requested
     */

    switch (dwDataFormat)
    {
    case MTPDBCONTEXTDATAFORMAT_SERVICEID:
        /*  No parameters required- validate length
         */

        if (cbOpRequest < MTPOPERATION_SIZE(0))
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }
        cmdbv = 0;
        break;

    case MTPDBCONTEXTDATAFORMAT_SERVICEINFO:
        /*  One parameter- service ID
         */

        if (cbOpRequest < MTPOPERATION_SIZE(1))
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        rgmdbv[0].wPropCode = MTP_OBJECTPROPCODE_SERVICEID;
        rgmdbv[0].wDataType = MTP_DATATYPE_UINT32;
        rgmdbv[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
        cmdbv = 1;
        break;

    case MTPDBCONTEXTDATAFORMAT_SERVICEPROPERTYDESC:
        /*  Two parameters- service ID and property code
         */

        if (cbOpRequest < MTPOPERATION_SIZE(2))
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        rgmdbv[0].wPropCode = MTP_OBJECTPROPCODE_SERVICEID;
        rgmdbv[0].wDataType = MTP_DATATYPE_UINT32;
        rgmdbv[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

        rgmdbv[1].wPropCode = MTP_SERVICEPROPCODE_PROPCODE;
        rgmdbv[1].wDataType = MTP_DATATYPE_UINT16;
        rgmdbv[1].vParam.valUInt16 = (PSLUINT16)MTPLoadUInt32(
                            &(pOpRequest->dwParam2));
        cmdbv = 2;
        break;


    case MTPDBCONTEXTDATAFORMAT_SERVICECAPABILITIES:
        /*  Two parameters- service ID and format code
         */

        if (cbOpRequest < MTPOPERATION_SIZE(2))
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        rgmdbv[0].wPropCode = MTP_OBJECTPROPCODE_SERVICEID;
        rgmdbv[0].wDataType = MTP_DATATYPE_UINT32;
        rgmdbv[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

        rgmdbv[1].wPropCode = MTP_OBJECTPROPCODE_OBJECTFORMAT;
        rgmdbv[1].wDataType = MTP_DATATYPE_UINT16;
        rgmdbv[1].vParam.valUInt16 = (PSLUINT16)MTPLoadUInt32(
                            &(pOpRequest->dwParam2));
        cmdbv = 2;
        break;

    case MTPDBCONTEXTDATAFORMAT_SERVICEPROPLIST:
        /*  Two parameters- service ID and property code
         */

        if (cbOpRequest < MTPOPERATION_SIZE(2))
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        rgmdbv[0].wPropCode = MTP_OBJECTPROPCODE_SERVICEID;
        rgmdbv[0].wDataType = MTP_DATATYPE_UINT32;
        rgmdbv[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

        rgmdbv[1].wPropCode = MTP_SERVICEPROPCODE_PROPCODE;
        rgmdbv[1].wDataType = MTP_DATATYPE_UINT16;
        rgmdbv[1].vParam.valUInt16 = (PSLUINT16)MTPLoadUInt32(
                            &(pOpRequest->dwParam2));
        cmdbv = 2;
        break;

    default:
        /*  Unknown format specified
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Generate the full format and open the data context
     */

    dwDataFormat = MAKE_MTPDBCONTEXTDATADESC(MTPDBCONTEXTDATA_QUERY,
                            dwDataFormat);

    /*  Locate the service
     */

    ps = MTPServiceResolverServiceContextDataOpen(
                            pData->sesLevelData.msrServices, dwDataFormat,
                            rgmdbv, cmdbv, pqwBufSize, &mscd);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Stash the context data
     */

    ((SERVICEHANDLERDATA*)pData)->mscd = mscd;
    mscd = PSLNULL;

Exit:
    SAFE_MTPSERVICECONTEXTDATACLOSE(mscd);
    return ps;
}


/*
 *  _MTPContextPrepareGetServiceIDs
 *
 *  Prepares the context for querying the service IDs
 *
 *  Arguments:
 *      PXMTPOPERATION  pOpRequest          Operation details
 *      PSLUINT64       cbOpRequest         Size of the operation
 *      BASEHANDLERDATA*
 *                      pData               Base handler data
 *      PSLUINT64*      pqwBufSize          Size of return buffer requested
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */


PSLSTATUS PSL_API _MTPContextPrepareGetServiceIDs(PXMTPOPERATION pOpRequest,
                            PSLUINT64 cbOpRequest, BASEHANDLERDATA* pData,
                            PSLUINT64* pqwBufSize)
{
    return _MTPContextPrepareServiceQuery(MTPDBCONTEXTDATAFORMAT_SERVICEID,
                            pOpRequest, cbOpRequest, pData, pqwBufSize);
}


/*
 *  _MTPContextPrepareGetServiceInfo
 *
 *  Prepares the context for querying information about a particular service
 *
 *  Arguments:
 *      PXMTPOPERATION  pOpRequest          Operation details
 *      PSLUINT64       cbOpRequest         Size of the operation
 *      BASEHANDLERDATA*
 *                      pData               Base handler data
 *      PSLUINT64*      pqwBufSize          Size of return buffer requested
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */


PSLSTATUS PSL_API _MTPContextPrepareGetServiceInfo(PXMTPOPERATION pOpRequest,
                            PSLUINT64 cbOpRequest, BASEHANDLERDATA* pData,
                            PSLUINT64* pqwBufSize)
{
    return _MTPContextPrepareServiceQuery(MTPDBCONTEXTDATAFORMAT_SERVICEINFO,
                            pOpRequest, cbOpRequest, pData, pqwBufSize);
}

/*
 *  _MTPContextPrepareGetServicePropDesc
 *
 *  Prepares the context for querying information about a particular service
 *
 *  Arguments:
 *      PXMTPOPERATION  pOpRequest          Operation details
 *      PSLUINT64       cbOpRequest         Size of the operation
 *      BASEHANDLERDATA*
 *                      pData               Base handler data
 *      PSLUINT64*      pqwBufSize          Size of return buffer requested
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */


PSLSTATUS PSL_API _MTPContextPrepareGetServicePropDesc(
                    PXMTPOPERATION pOpRequest, PSLUINT64 cbOpRequest,
                    BASEHANDLERDATA* pData, PSLUINT64* pqwBufSize)
{
    return _MTPContextPrepareServiceQuery(
                            MTPDBCONTEXTDATAFORMAT_SERVICEPROPERTYDESC,
                            pOpRequest, cbOpRequest, pData, pqwBufSize);
}


/*
 *  _MTPContextPrepareGetServiceCapabilities
 *
 *  Prepares the context for querying a services capabilities
 *
 *  Arguments:
 *      PXMTPOPERATION  pOpRequest          Operation details
 *      PSLUINT64       cbOpRequest         Size of the operation
 *      BASEHANDLERDATA*
 *                      pData               Base handler data
 *      PSLUINT64*      pqwBufSize          Size of return buffer requested
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */


PSLSTATUS PSL_API _MTPContextPrepareGetServiceCapabilities(
                    PXMTPOPERATION pOpRequest, PSLUINT64 cbOpRequest,
                    BASEHANDLERDATA* pData, PSLUINT64* pqwBufSize)
{
    return _MTPContextPrepareServiceQuery(
                            MTPDBCONTEXTDATAFORMAT_SERVICECAPABILITIES,
                            pOpRequest, cbOpRequest, pData, pqwBufSize);
}


/*
 *  _MTPContextPrepareGetServicePropList
 *
 *  Prepares the context for querying service property list
 *
 *  Arguments:
 *      PXMTPOPERATION  pOpRequest          Operation details
 *      PSLUINT64       cbOpRequest         Size of the operation
 *      BASEHANDLERDATA*
 *                      pData               Base handler data
 *      PSLUINT64*      pqwBufSize          Size of return buffer requested
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */


PSLSTATUS PSL_API _MTPContextPrepareGetServicePropList(
                    PXMTPOPERATION pOpRequest, PSLUINT64 cbOpRequest,
                    BASEHANDLERDATA* pData, PSLUINT64* pqwBufSize)
{
    return _MTPContextPrepareServiceQuery(
                            MTPDBCONTEXTDATAFORMAT_SERVICEPROPLIST,
                            pOpRequest, cbOpRequest, pData, pqwBufSize);
}

/*
 *  _MTPContextPrepareSetServicePropList
 *
 *  Prepares the context for setting service property list
 *
 *  Arguments:
 *      PXMTPOPERATION  pOpRequest          Operation details
 *      PSLUINT64       cbOpRequest         Size of the operation
 *      BASEHANDLERDATA*
 *                      pData               Base handler data
 *      PSLUINT64*      pqwBufSize          Size of return buffer requested
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */


PSLSTATUS PSL_API _MTPContextPrepareSetServicePropList(
                    PXMTPOPERATION pOpRequest, PSLUINT64 cbOpRequest,
                    BASEHANDLERDATA* pData, PSLUINT64* pqwBufSize)
{
    PSLUINT32               cmdbv;
    MTPSERVICECONTEXTDATA   mscd = PSLNULL;
    PSLSTATUS               ps;
    MTPDBVARIANTPROP        rgmdbv[2];
    PSLUINT32               dwDataFormat;

    /*  Clear results for safety
     */

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }
    ((SERVICEHANDLERDATA*)pData)->mscd = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == pOpRequest) || (PSLNULL == pData) ||
        (PSLNULL == pqwBufSize))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Initialze the context
     */

    /*  One parameter- service ID
     */

    if (cbOpRequest < MTPOPERATION_SIZE(1))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmdbv[0].wPropCode = MTP_OBJECTPROPCODE_SERVICEID;
    rgmdbv[0].wDataType = MTP_DATATYPE_UINT32;
    rgmdbv[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
    cmdbv = 1;

    /*  Generate the format and open the data context
     */

    dwDataFormat = MAKE_MTPDBCONTEXTDATADESC(MTPDBCONTEXTDATA_UPDATE,
                            MTPDBCONTEXTDATAFORMAT_SERVICEPROPLIST);

    ps = MTPServiceResolverServiceContextDataOpen(
                            pData->sesLevelData.msrServices, dwDataFormat,
                            rgmdbv, cmdbv, pqwBufSize, &mscd);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Stash the context data
     */

    ((SERVICEHANDLERDATA*)pData)->mscd = mscd;
    mscd = PSLNULL;

Exit:
    SAFE_MTPSERVICECONTEXTDATACLOSE(mscd);
    return ps;
}


/*
 *  _MTPContextPrepareDeleteServicePropList
 *
 *  Prepares the context for deleting service property list
 *
 *  Arguments:
 *      PXMTPOPERATION  pOpRequest          Operation details
 *      PSLUINT64       cbOpRequest         Size of the operation
 *      BASEHANDLERDATA*
 *                      pData               Base handler data
 *      PSLUINT64*      pqwBufSize          Size of return buffer requested
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */


PSLSTATUS PSL_API _MTPContextPrepareDeleteServicePropList(
                    PXMTPOPERATION pOpRequest, PSLUINT64 cbOpRequest,
                    BASEHANDLERDATA* pData, PSLUINT64* pqwBufSize)
{
    PSLUINT32               cmdbv;
    MTPSERVICECONTEXTDATA   mscd = PSLNULL;
    PSLSTATUS               ps;
    MTPDBVARIANTPROP        rgmdbv[2];
    PSLUINT32               dwDataFormat;

    /*  Clear results for safety
     */

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }
    ((SERVICEHANDLERDATA*)pData)->mscd = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == pOpRequest) || (PSLNULL == pData) ||
        (PSLNULL == pqwBufSize))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Initialze the context
     */

    /*  One parameter- service ID
     */

    if (cbOpRequest < MTPOPERATION_SIZE(1))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmdbv[0].wPropCode = MTP_OBJECTPROPCODE_SERVICEID;
    rgmdbv[0].wDataType = MTP_DATATYPE_UINT32;
    rgmdbv[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
    cmdbv = 1;

    /*  Generate the format and open the data context
     */

    dwDataFormat = MAKE_MTPDBCONTEXTDATADESC(MTPDBCONTEXTDATA_DELETE,
                            MTPDBCONTEXTDATAFORMAT_SERVICEPROPLIST);

    ps = MTPServiceResolverServiceContextDataOpen(
                            pData->sesLevelData.msrServices, dwDataFormat,
                            rgmdbv, cmdbv, pqwBufSize, &mscd);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Stash the context data
     */

    ((SERVICEHANDLERDATA*)pData)->mscd = mscd;
    mscd = PSLNULL;

Exit:
    SAFE_MTPSERVICECONTEXTDATACLOSE(mscd);
    return ps;
}


/*
 *  _MTPContextDataConsumedServicePropList
 *
 *  Commit Data when the last packet of data is transfered
 *
 *  Arguments:
 *      BASEHANDLERDATA*
 *                      pData               Base handler data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */
PSLSTATUS PSL_API _MTPContextDataConsumedServicePropList(
                    BASEHANDLERDATA* pData)
{

    PSLSTATUS status;

    if (PSLNULL == pData)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = PSLSUCCESS;

Exit:
    return status;
}

/*
 *  MTPGetServiceIDs
 *
 *  The handler function for the GetServiceIDs MTP operation
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  Dispatcher for this operation
 *      PSLMSG*         pMsg                Message being dispatched
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPGetServiceIDs(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleServiceOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                            _MTPContextPrepareGetServiceIDs,
                            PSLNULL);
}


/*
 *  MTPGetServiceInfo
 *
 *  The handler function for the GetServiceInfo MTP operation
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  Dispatcher for this operation
 *      PSLMSG*         pMsg                Message being dispatched
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPGetServiceInfo(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleServiceOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                            _MTPContextPrepareGetServiceInfo,
                            PSLNULL);
}

/*
 *  MTPGetServicePropDesc
 *
 *  The handler function for the GetServiceProperties MTP operation
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  Dispatcher for this operation
 *      PSLMSG*         pMsg                Message being dispatched
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPGetServicePropDesc(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleServiceOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                            _MTPContextPrepareGetServicePropDesc,
                            PSLNULL);
}


/*
 *  MTPGetServiceCapabilities
 *
 *  The handler function for the GetServiceCapabilities MTP operation
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  Dispatcher for this operation
 *      PSLMSG*         pMsg                Message being dispatched
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPGetServiceCapabilities(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleServiceOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                            _MTPContextPrepareGetServiceCapabilities,
                            PSLNULL);
}

/*
 *  MTPGetServicePropList
 *
 *  The handler function for the GetServicePropList MTP operation
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  Dispatcher for this operation
 *      PSLMSG*         pMsg                Message being dispatched
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPGetServicePropList(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleServiceOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                            _MTPContextPrepareGetServicePropList,
                            PSLNULL);
}

/*
 *  MTPSetServicePropList
 *
 *  The handler function for the SetServicePropList MTP operation
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  Dispatcher for this operation
 *      PSLMSG*         pMsg                Message being dispatched
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPSetServicePropList(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleServiceOperations(md, pMsg, MTPHANDLERSTATE_DATA_IN,
                            _MTPContextPrepareSetServicePropList,
                            _MTPContextDataConsumedServicePropList);
}


/*
 *  MTPDeleteServicePropList
 *
 *  The handler function for the DeleteServicePropList MTP operation
 *
 *  Arguments:
 *      MTPDISPATCHER   md                  Dispatcher for this operation
 *      PSLMSG*         pMsg                Message being dispatched
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSLSTATUS PSL_API MTPDeleteServicePropList(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleServiceOperations(md, pMsg, MTPHANDLERSTATE_DATA_IN,
                            _MTPContextPrepareDeleteServicePropList,
                            _MTPContextDataConsumedServicePropList);
}

