/*
 *  MTPEventHandler.c
 *
 *  Contains the implementation of the per-session MTP Event Handler
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPEventObject.h"

/*  Local Defines
 */

#define MTPEVENT_DEFAULT_QUEUE_SIZE     8

#define EVENTHANDLER_MSG_POOL_SIZE      16

/*  Local Types
 */

typedef struct _MTPEVENTCONTEXT
{
    MTPEVENTOBJECT      evObj;
    PSLUINT32           dwBroadcastMode;
    MTPCONTEXT*         pmtpctx;
    PSLMSGPOOL          mpEventHandler;
} MTPEVENTCONTEXT;

/*  Local Functions
 */

static PSLSTATUS _MTPEventHandlerEventBufferRequest(MTPCONTEXT* pmtpctx,
                    MTPEVENTOBJECT evObj,
                    PSLMSGPOOL mpSource);

static PSLSTATUS PSL_API _MTPEventHandlerSessionListEnumProc(PSLPARAM aParam,
                    PSLPARAM aEnumParam);

static PSLSTATUS PSL_API _MTPEventHandlerCleanQueueEnum(
                    PSL_CONST PSLMSG* pMsg,
                    PSLPARAM aParam);

static PSLUINT32 PSL_API _MTPEventHandlerThread(PSLVOID* pvParam);

/*  Local Variables
 */

static PSLCTXLIST       _CtxSessionList = PSLNULL;
static PSLBKTHASH       _BkhEventQueue = PSLNULL;
static PSLMSGQUEUE      _MqEventHandler = PSLNULL;

static const PSLWCHAR   _SzInitSignal[] = L"MTP EventHandler Init Signal";
static const PSLWCHAR   _SzInitMutex[] = L"MTP EventHandler Init Mutex";


/*  _MTPEventHandlerEventBufferRequest
 *
 *  Routes a request for an event buffer to the transport specified
 *
 *  Arguements:
 *      MTPCONTEXT*     pmtpctx             MTP context to route request to
 *      MTPEVENTOBJECT  evObj               Event to request buffer for
 *      PSLMSGPOOL      mpSource            Source message pool
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPEventHandlerEventBufferRequest(MTPCONTEXT* pmtpctx,
                    MTPEVENTOBJECT evObj, PSLMSGPOOL mpSource)
{
    PSLDYNLIST      dlEventList;
    MTPEVENTOBJECT  evObjTemp = PSLNULL;
    PSLUINT32       idxItem;
    PSLSTATUS       ps;
    PSLMSG*         pMsg = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == evObj) || (PSLNULL == mpSource))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Retrieve the event list associated with this context
     */

    ps = PSLBktHashFind(_BkhEventQueue, (PSLPARAM)pmtpctx,
                            (PSLPARAM*)&dlEventList);
    if ((PSLSUCCESS != ps) || (PSLNULL == dlEventList))
    {
        PSLASSERT(PSL_FAILED(ps));
        ps = PSL_FAILED(ps) ? ps : PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Increment the reference count on the event object before we add it
     *  to the list
     */

    evObjTemp = evObj;
    ps = MTPEventObjectAddRef(evObjTemp);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And add the new event to the event list
     */

    ps = PSLDynListAddItem(dlEventList, (PSLPARAM)evObjTemp, &idxItem);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  List now owns the extra reference count
     */

    evObjTemp = PSLNULL;

    /*  If this was not the first item in the list then we do not want to
     *  send a buffer request here
     */

    if (0 < idxItem)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Retrieve a message to use for the buffer request
     */

    ps = PSLMsgAlloc(mpSource, &pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize the message and route it to the transport
     */

    pMsg->msgID = MTPMSG_EVENT_BUFFER_REQUEST;
    pMsg->aParam0 = (PSLPARAM)pmtpctx;
    pMsg->aParam3 = MTPMSGFLAG_EVENT_BUFFER_REQUEST;
    pMsg->alParam = sizeof(MTPEVENT);

    ps = MTPRouteMsgToTransport(pmtpctx, pMsg);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pMsg = PSLNULL;

Exit:
    SAFE_PSLMSGFREE(pMsg);
    SAFE_MTPEVENTOBJECTDESTROY(evObjTemp);
    return ps;
}


/*  _MTPEventHandlerSessionListEnumProc
 *
 *  Callback used by the session context list enumeration to perform work on
 *  each of the sessions in the list
 *
 *  Arguments:
 *      PSLPARAM        aParam              Enumerated session from the context
 *                                            list
 *      PSLPARAM        aEnumParam          Enumeration parameter
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE to halt
 */

PSLSTATUS PSL_API _MTPEventHandlerSessionListEnumProc(PSLPARAM aParam,
                    PSLPARAM aEnumParam)
{
    PSLBOOL             fSendEvent;
    PSLSTATUS           ps;
    MTPCONTEXT*         pmtpctx;
    MTPEVENTCONTEXT*    pevCtx;

    /*  Validate arguments
     */

    if ((PSLNULL == aParam) || (PSLNULL == aEnumParam))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract the event context and determine if we need to send this event
     */

    pmtpctx = (MTPCONTEXT*)aParam;
    pevCtx = (MTPEVENTCONTEXT*)aEnumParam;
    switch (pevCtx->dwBroadcastMode)
    {
    case MTPEVENTBROADCASTMODE_ALL:
        fSendEvent = PSLTRUE;
        break;

    case MTPEVENTBROADCASTMODE_EXCLUDE_MTPSESSION:
        fSendEvent = (pmtpctx != pevCtx->pmtpctx);
        break;

    case MTPEVENTBROADCASTMODE_ONLY_MTPSESSION:
    default:
        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Send the event if necessary
     */

    ps = fSendEvent ? _MTPEventHandlerEventBufferRequest(pmtpctx, pevCtx->evObj,
                            pevCtx->mpEventHandler) :
                    PSLSUCCESS;
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPStreamRouterCleanQueueEnum
 *
 *  Callback function used with PSLMsgEnum when when the event handler
 *  thread is shutting down to make sure that all messages have been
 *  removed from the queue
 *
 *  Arguments:
 *      PSLMSG*         pMsg                Message to check
 *      PSLPARAM        aParam              Callback parameter
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success, PSLSUCCESS_FALSE to have
 *                        message removed, PSLSUCCESS_CANCEL to stop
 *                        enumeration
 *
 */

PSLSTATUS PSL_API _MTPEventHandlerCleanQueueEnum(PSL_CONST PSLMSG* pMsg,
                    PSLPARAM aParam)
{
    PSLSTATUS       ps;
    PSLMSG*         pMsgSend = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == pMsg)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Look for messages which carry an event object
     */

    switch (pMsg->msgID)
    {
    case MTPMSG_EVENT:
        /*  Release the event
         */

        MTPEventObjectDestroy((MTPEVENTOBJECT)pMsg->aParam0);
        break;

    default:
        break;
    }

    /*  Look to see if the message has a buffer
     */

    if (MTPMSGID_BUFFER_PRESENT & pMsg->msgID)
    {
        /*  Allocate a message to return to the transport
         */

        ps = PSLMsgAlloc((PSLMSGPOOL)aParam, &pMsgSend);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsgSend->msgID = MTPMSG_BUFFER_IGNORED;
        pMsgSend->aParam0 = pMsg->aParam0;
        pMsgSend->aParam1 = pMsg->aParam1;
        pMsgSend->aParam2 = pMsg->aParam2;
        pMsgSend->aParam3 = pMsg->msgID;
        pMsgSend->alParam = pMsg->alParam;

        /*  And route the message
         */

        ps = MTPRouteMsgToTransport((MTPCONTEXT*)pMsgSend->aParam0,
                        pMsgSend);
        if (PSL_SUCCEEDED(ps))
        {
            pMsgSend = PSLNULL;
        }
    }

    /*  Remove the message from the queue
     */

    ps = PSLSUCCESS_FALSE;

Exit:
    SAFE_PSLMSGFREE(pMsgSend);
    return ps;
}


/*  _MTPEventHandlerThread
 *
 *  Main thread that handles receiving messages from event manager
 *
 *  Arguments:
 *      PSLVOID*        pvParam         Parameter- contains a PSLSTATUS* which
 *                                        is used to report succesful or
 *                                        unsuccesful initialization of the
 *                                        thread
 *
 *  Returns:
 *      PSLUINT32       Error code (PSLSTATUS)
 */

PSLUINT32 PSL_API _MTPEventHandlerThread(PSLVOID* pvParam)
{
    PSLPARAM        aKey;
    PSLLPARAM       alParam;
    PSLUINT32       cbUsed;
    PSLUINT32       cEvents;
    PSLUINT32       dwMsgFlags;
    PSLDYNLIST      dlEventList;
    MTPEVENTCONTEXT evCtx;
    MTPEVENTOBJECT  evObj = PSLNULL;
    PSLBOOL         fLoop;
    PSLHANDLE       hSignalInit = PSLNULL;
    PSLHANDLE       hMutexInit;
    PSLUINT32       idxEvent;
    PSLMSGID        msgID;
    PSLMSGPOOL      mpEventHandler = PSLNULL;
    PSLMSGQUEUE     mqShutdown = PSLNULL;
    PSLSTATUS       ps;
    PSLSTATUS*      ppsInit = PSLNULL;
    PSLMSG*         pMsg = PSLNULL;
    PSLMSG*         pMsgRTS = PSLNULL;
    PSLVOID*        pvQueueParam = PSLNULL;

    /*  Validate arguments
     */

    if (PSLNULL == pvParam)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    ppsInit = (PSLSTATUS*)pvParam;

    /*  Create a message queue to receive event information on
     */

    ps = MTPMsgGetQueueParam(&pvQueueParam);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    ps = PSLMsgQueueCreate(pvQueueParam, MTPMsgOnPost, MTPMsgOnWait,
                        MTPMsgOnDestroy, &_MqEventHandler);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    pvQueueParam = PSLNULL;

    ps = PSLMsgPoolCreate(EVENTHANDLER_MSG_POOL_SIZE, &mpEventHandler);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Request shutdown notification on the queue
     */

    ps = MTPShutdownRegister(_MqEventHandler);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    mqShutdown = _MqEventHandler;

    /*  And register the queue to receive event information
     */

    ps = MTPEventManagerAddMonitor(_MqEventHandler);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create a hash to hold the event queue for all sessions
     */

    cEvents = (MTP_MAX_SESSIONS == 0) ?
                            MTPEVENT_DEFAULT_QUEUE_SIZE : MTP_MAX_SESSIONS;
    ps = PSLBktHashCreate(cEvents, PSLNULL, 0, &_BkhEventQueue);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Create a context list to track sessions
     */

    ps = PSLCtxListCreate(&_CtxSessionList);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Report that we have succesfully initialized
     */

    *ppsInit = PSLSUCCESS;

    /*  Create and set the "initialized" signal so any waitng threads
     *  can be released
     */

    ps = PSLSignalOpen(PSLFALSE, PSLFALSE, _SzInitSignal,
                            &hSignalInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    ps = PSLSignalSet(hSignalInit);
    PSLASSERT(PSLSUCCESS == ps);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }
    ppsInit = PSLNULL;

    /*  Loop and handle the messages we receive
     *
     *  NOTE: The code assumes that everything we are doing here is around
     *  events and that "losing" an event is an acceptable risk.  For that
     *  reason no "error conditions" trigger a failure and stop the loop
     *  from executing.  In addition no work is done to attempt to restart
     *  the event handler thread if it were to die.
     */

    fLoop = PSLTRUE;
    while (fLoop)
    {

        /*  Release any message and any event we might have picked up
         */

        SAFE_PSLMSGFREE(pMsg);
        SAFE_PSLMSGFREE(pMsgRTS);
        SAFE_MTPEVENTOBJECTDESTROY(evObj);

        /*  Retrieve the message
         */

        ps = PSLMsgGet(_MqEventHandler, &pMsg);
        PSLASSERT(PSL_SUCCEEDED(ps));
        if (PSLSUCCESS != ps)
        {
            continue;
        }

        /*  And handle it
         */

        switch (pMsg->msgID)
        {
        case MTPMSG_EVENT:
            /*  Extract the event object from the message
             */

            evObj = (MTPEVENTOBJECT)pMsg->aParam0;
            PSLASSERT(PSLNULL != evObj);
            if (PSLNULL == evObj)
            {
                PSLTraceError("_MTPEventHandlerThread: Invalid event handle");
                break;
            }

            /*  And retrieve the broadcast information
             */

            ps = MTPEventObjectGetBroadcastMode(evObj, &evCtx.dwBroadcastMode,
                            &evCtx.pmtpctx);
            PSLASSERT(PSL_SUCCEEDED(ps));
            if (PSL_FAILED(ps))
            {
                PSLTraceError("_MTPEventHandlerThread: Could not retrieve "
                            "broadcast mode (0x%08x)", ps);
                break;
            }

            /*  Determine how to broadcast the message
             */

            switch (evCtx.dwBroadcastMode)
            {
            case MTPEVENTBROADCASTMODE_ALL:
            case MTPEVENTBROADCASTMODE_EXCLUDE_MTPSESSION:
                /*  Event will be broadcast to all active sessions (possibly
                 *  excluding the source session)- enumerate through the
                 *  context list to initiate buffer requests on each of the
                 *  sessions
                 */

                evCtx.evObj = evObj;
                evCtx.mpEventHandler = mpEventHandler;
                ps = PSLCtxListEnum(_CtxSessionList,
                            _MTPEventHandlerSessionListEnumProc,
                            (PSLPARAM)&evCtx);
                PSLASSERT(PSL_SUCCEEDED(ps));
                if (PSL_FAILED(ps))
                {
                    PSLTraceError("_MTPEventHandlerThread: Could not enumerate "
                            "session context list (0x%08x)", ps);
                    break;
                }
                break;

            case MTPEVENTBROADCASTMODE_ONLY_MTPSESSION:
                /*  We only have one MTP Session that needs to receive this-
                 *  just start the buffer process directly
                 */

                ps = _MTPEventHandlerEventBufferRequest(evCtx.pmtpctx, evObj,
                            mpEventHandler);
                if (PSL_FAILED(ps))
                {
                    PSLTraceError("_MTPEventHandlerThread: Could not route "
                            "buffer request (0x%08x)", ps);
                    break;
                }
                break;

            default:
                /*  Unknown broadcast mode- should never get here
                 */

                PSLASSERT(PSLFALSE);
                break;
            }
            break;

        case MTPMSG_EVENT_BUFFER_AVAILABLE:
            /*  Retrieve the event list for the context of the message
             */

            ps = PSLBktHashFind(_BkhEventQueue, pMsg->aParam0,
                            (PSLPARAM*)&dlEventList);
            if ((PSLSUCCESS != ps) || (PSLNULL == dlEventList))
            {
                PSLASSERT(PSL_FAILED(ps));
                PSLTraceError("_MTPEventHandlerThread: Event List not found");
                break;
            }

            /*  Retrieve the event object from the event list
             */

            ps = PSLDynListGetItem(dlEventList, 0, (PSLPARAM*)&evObj);
            if (PSL_FAILED(ps))
            {
                PSLTraceError("_MTPEventHandlerThread: Error retrieving Event");
                break;
            }

            /*  See if we actually retrieved the item
             */

            if (PSLSUCCESS == ps)
            {
                /*  Remove the item from the list
                 */

                ps = PSLDynListRemoveItem(dlEventList, 0, 1);
                if (PSL_FAILED(ps))
                {
                    PSLTraceError("_MTPEventHandlerThread: Error updating list");
                    break;
                }

                /*  Validate the event
                 */

                PSLASSERT(PSLNULL != evObj);
                if (PSLNULL == evObj)
                {
                    PSLTraceError("_MTPEventHandlerThread: Event Object not "
                            "present in event list");
                    break;
                }

                /*  Extract the buffer and fill it with the event information
                 */

                PSLASSERT(PSLNULL != pMsg->aParam2);
                ps = MTPEventObjectStore(evObj, (PXMTPEVENT)pMsg->aParam2,
                                (PSLUINT32)pMsg->alParam, &cbUsed);
                if (PSL_FAILED(ps))
                {
                    PSLTraceError("_MTPEventHandlerThread: Failed to store event "
                                "(0x%08x)", ps);
                    break;
                }

                /*  Tell the transport to send the event
                 */

                msgID = MTPMSG_EVENT_READY_TO_SEND;
                alParam = cbUsed;

                /*  Find out if we have another event to send
                 */

                ps = PSLDynListGetSize(dlEventList, &cEvents);
                PSLASSERT(PSL_SUCCEEDED(ps));
                dwMsgFlags = MTPMSGFLAG_TERMINATE | ((0 < cEvents) ?
                            MTPMSGFLAG_EVENT_BUFFER_REQUEST : 0);
            }
            else
            {
                /*  We should not have any more events in the list
                 */

                PSLASSERT(
                    PSL_SUCCEEDED(PSLDynListGetSize(dlEventList, &cEvents)) &&
                    (0 == cEvents));

                /*  Just hand this buffer back since we do not need it
                 */

                msgID = MTPMSG_BUFFER_IGNORED;
                dwMsgFlags = pMsg->msgID;
                alParam = pMsg->alParam;
            }

            /*  Route the ready to send message to the transport
             */

            ps = PSLMsgAlloc(mpEventHandler, &pMsgRTS);
            if (PSL_FAILED(ps))
            {
                PSLTraceError("_MTPEventHandlerThread: Failed to acquire RTS "
                            "message (0x%08x)", ps);
                break;
            }

            pMsgRTS->msgID = msgID;
            pMsgRTS->aParam0 = pMsg->aParam0;
            pMsgRTS->aParam1 = pMsg->aParam1;
            pMsgRTS->aParam2 = pMsg->aParam2;
            pMsgRTS->aParam3 = dwMsgFlags;
            pMsgRTS->alParam = alParam;

            ps = MTPRouteMsgToTransport((MTPCONTEXT*)pMsg->aParam0, pMsgRTS);
            if (PSL_FAILED(ps))
            {
                PSLTraceError("_MTPEventHandlerThread: Failed to post RTS "
                            "message (0x%08x)", ps);
            }
            pMsgRTS = PSLNULL;
            break;

        case MTPMSG_EVENT_SENT:
            break;

        case MTPMSG_SHUTDOWN:
            /*  Report that it is time to shutdown
             */
            fLoop = PSLFALSE;
            break;

        default:
            /*  Ignore unknown messages
             */
            break;
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    /*  See if we already signalled initialization completed
     */

    if (PSLNULL == hSignalInit)
    {
        /*  We never sent the initialization signal which means that we
         *  failed to initialize- create it and send it so that we unblock
         *  execution
         */

        PSLASSERT(PSL_FAILED(ps));
        if (PSLNULL != ppsInit)
        {
            *ppsInit = ps;
        }

        if (PSL_SUCCEEDED(PSLSignalOpen(PSLFALSE, PSLFALSE, _SzInitSignal,
                            &hSignalInit)))
        {
            PSLSignalSet(hSignalInit);
        }
    }

    /*  Acquire the initialization mutex before cleaning up any of these
     *  shared objects
     */

    if (PSL_SUCCEEDED(PSLMutexOpen(PSLTHREAD_INFINITE, _SzInitMutex,
                            &hMutexInit)))
    {
        /*  See if we need to stop receiving notifications
         */

        if (PSLNULL != _MqEventHandler)
        {
            /*  Stop receiving event notifications
             */

            MTPEventManagerRemoveMonitor(_MqEventHandler);

            /*  Walk through the event queue and remove any unhandled messages and
             *  then close the queue
             */

            PSLMsgEnum(_MqEventHandler, _MTPEventHandlerCleanQueueEnum,
                                (PSLPARAM)mpEventHandler);
            SAFE_PSLMSGQUEUEDESTROY(_MqEventHandler);
        }
        SAFE_PSLMSGPOOLDESTROY(mpEventHandler);
        SAFE_MTPMSGRELEASEQUEUEPARAM(pvQueueParam);
        if (PSLSUCCESS == PSLBktHashGetFirstEntry(_BkhEventQueue, &aKey,
                            (PSLPARAM*)&dlEventList))
        {
            /*  Release any pending events that may still be waiting
             */

            do
            {
                /*  Walk through the dynamic list and remove events
                 */

                if (PSL_SUCCEEDED(PSLDynListGetSize(dlEventList, &cEvents)))
                {
                    for (idxEvent = cEvents; 0 < idxEvent; idxEvent--)
                    {
                        if (PSL_SUCCEEDED(PSLDynListGetItem(dlEventList,
                                idxEvent, (PSLPARAM*)&evObj)))
                        {
                            SAFE_MTPEVENTOBJECTDESTROY(evObj);
                        }
                    }
                }

                /*  Release the dynamic list itself
                 */

                SAFE_PSLDYNLISTDESTROY(dlEventList);
            } while (PSLSUCCESS == PSLBktHashGetNextEntry(_BkhEventQueue,
                            &aKey, (PSLPARAM*)&dlEventList));
        }
        SAFE_PSLBKTHASHDESTROY(_BkhEventQueue);
        SAFE_PSLCTXLISTDESTROY(_CtxSessionList);
        SAFE_PSLMSGFREE(pMsg);
        SAFE_PSLMSGFREE(pMsgRTS);
        SAFE_PSLSIGNALCLOSE(hSignalInit);
        if (PSLNULL != mqShutdown)
        {
            MTPShutdownUnregister(mqShutdown);
        }
        SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);
    }
    else
    {
        PSLTraceError("_MTPEventHandlerThred: Failed to uninitialize properly");
        PSLASSERT(PSLFALSE);
    }
    return ps;
}


/*  MTPEventHandlerAddContext
 *
 *  Initializes the MTP Event Handler and associates it with the specified
 *  session.
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             Context to be added
 *      PSLMSGQUEUE*    pmqEventHandler     Resulting handler message queue
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPEventHandlerAddContext(MTPCONTEXT* pmtpctx,
                    PSLMSGQUEUE* pmqEventHandler)
{
    PSLDYNLIST      dlEventList = PSLNULL;
    PSLBOOL         fCleanHash = PSLFALSE;
    PSLHANDLE       hThread = PSLNULL;
    PSLHANDLE       hMutexInit = PSLNULL;
    PSLHANDLE       hSignalInit = PSLNULL;
    PSLSTATUS       ps;
    PSLSTATUS       psEventThread;

    /*  Clear results for safety
     */

    if (PSLNULL != pmqEventHandler)
    {
        *pmqEventHandler =  PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pmtpctx) || (PSLNULL == pmqEventHandler))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Create the initialization mutex- once the Event Handler thread has been
     *  initialized we will not attempt again unless it closes down for some
     *  reason and clears the context list and message queue.
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _SzInitMutex, &hMutexInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Check to see if we need to initialize the thread- we must have both
     *  a session context and a message queue to proceed
     */

    if (PSLNULL == _CtxSessionList)
    {
        /*  Make sure that we do not have partial initialization somehow
         */

        PSLASSERT(PSLNULL == _MqEventHandler);
        PSLASSERT(PSLNULL == _BkhEventQueue);
        if ((PSLNULL != _CtxSessionList) || (PSLNULL != _MqEventHandler) ||
            (PSLNULL != _BkhEventQueue))
        {
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Create the thread
         */

        ps= PSLThreadCreate(_MTPEventHandlerThread, &psEventThread, PSLFALSE,
                                PSLTHREAD_DEFAULT_STACK_SIZE, &hThread);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Create the initialization event
         */

        ps = PSLSignalOpen(PSLFALSE, PSLFALSE, _SzInitSignal, &hSignalInit);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  And wait for the thread to initialize
         */

        ps = PSLSignalWait(hSignalInit, PSLTHREAD_INFINITE);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  See if initialization was succesful
         */

        if (PSL_FAILED(psEventThread))
        {
            PSLASSERT((PSLNULL == _CtxSessionList) &&
                            (PSLNULL == _MqEventHandler) &&
                            (PSLNULL == _BkhEventQueue));
            ps = psEventThread;
            goto Exit;
        }

        /*  We should have both the context list and the message queue at
         *  this point
         */

        PSLASSERT((PSLNULL != _CtxSessionList) && (PSLNULL != _MqEventHandler));
    }

    /*  Create a new dynamic list to hold pending events on this queue
     */

    ps = PSLDynListCreate(EVENTOBJ_PER_SESSION, PSLTRUE,
                            PSLDYNLIST_GROW_DEFAULT, &dlEventList);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Add the dynamic list to the hash- we do not track these together in
     *  the context because we do not always have the context list object in
     *  the callbacks
     */

    ps = PSLBktHashInsert(_BkhEventQueue, (PSLPARAM)pmtpctx,
                            (PSLPARAM)dlEventList);
    if (PSLSUCCESS != ps)
    {
        PSLASSERT(PSL_FAILED(ps));
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }
    dlEventList = PSLNULL;
    fCleanHash = PSLTRUE;

    /*  Add the context to the notifier queue.  Contexts should only be
     *  registered once- treat additional registrations as an error as that
     *  implies a session did not shutdown correctly.
     */

    ps = PSLCtxListRegister(_CtxSessionList, (PSLPARAM)pmtpctx);
    if (PSLSUCCESS != ps)
    {
        PSLASSERT(PSL_FAILED(ps));
        ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_OPERATION;
        goto Exit;
    }
    fCleanHash = PSLFALSE;

    /*  Return the message queue to post events messages to
     */

    *pmqEventHandler = _MqEventHandler;
    ps = PSLSUCCESS;

Exit:
    if (fCleanHash)
    {
        PSLBktHashRemove(_BkhEventQueue, (PSLPARAM)pmtpctx,
                            (PSLPARAM*)&dlEventList);
    }
    SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);
    SAFE_PSLTHREADCLOSE(hThread);
    SAFE_PSLSIGNALCLOSE(hSignalInit);
    SAFE_PSLDYNLISTDESTROY(dlEventList);
    return ps;
}


/*  MTPEventHandlerRemoveContext
 *
 *  Removes the specified context from being associated with the event queue
 *
 *  Arguments:
 *      MTPCONTEXT*     pmtpctx             MTP context to remove
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPEventHandlerRemoveContext(MTPCONTEXT* pmtpctx)
{
    PSLUINT32       cEvents;
    PSLDYNLIST      dlEventList = PSLNULL;
    MTPEVENTOBJECT  evObj;
    PSLHANDLE       hMutexInit = PSLNULL;
    PSLUINT32       idxEvent;
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if (PSLNULL == pmtpctx)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Create the initialization mutex
     */

    ps = PSLMutexOpen(PSLTHREAD_INFINITE, _SzInitMutex, &hMutexInit);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Depending upon the shutdown order it is possible for the context list
     *  to be destroyed before we get to this point.  If the context list is
     *  gone then we cannot do anything so just report that nothing was done
     */

    if (PSLNULL == _CtxSessionList)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  We just need to remove the context from the session list
     */

    ps = PSLCtxListUnregister(_CtxSessionList, (PSLPARAM)pmtpctx);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And remove the event list from the event queue
     */

    ps = PSLBktHashRemove(_BkhEventQueue, (PSLPARAM)pmtpctx,
                            (PSLPARAM*)&dlEventList);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

 Exit:
    SAFE_PSLMUTEXRELEASECLOSE(hMutexInit);
    if (PSLNULL != dlEventList)
    {
        if (PSL_SUCCEEDED(PSLDynListGetSize(dlEventList, &cEvents)))
        {
            for (idxEvent = cEvents; 0 < idxEvent; idxEvent--)
            {
                if (PSL_SUCCEEDED(PSLDynListGetItem(dlEventList, idxEvent,
                            (PSLPARAM*)&evObj)))
                {
                    SAFE_MTPEVENTOBJECTDESTROY(evObj);
                }
            }
        }
        SAFE_PSLDYNLISTDESTROY(dlEventList);
    }
    return ps;
}

