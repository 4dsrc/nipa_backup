/*
 *  MTPEventManager.c
 *
 *  Contains the definition of the MTP event manager
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPEventObject.h"

/*  Local Defines
 */

#define MTPEVENTOBJ_COOKIE              'mtpE'

#define DEFAULT_EVENTOBJ_POOL \
        (MTP_DEFAULT_SESSIONS * EVENTOBJ_PER_SESSION)

/*  Local Types
 */

/*  Local Functions
 */

static PSLSTATUS PSL_API _MTPEventManagerNotifyEnum(PSLMSG* pMsg);

/*  Local Variables
 */

static PSLOBJPOOL           _OpEventObj = PSLNULL;

static PSLNOTIFY            _NtfyEventManager = PSLNULL;



/*  MTPEventObjectCreate
 *
 *  Creates an instance of an event object from the event pool
 *
 *  Arguments:
 *      PSLUINT16       wEventCode          Event code to associate with object
 *      PSLUINT32       dwTransactionID     Associated transaction ID
 *      PSLUINT32       dwParam1            First event parameter
 *      PSLUINT32       dwParam2            Second event parameter
 *      PSLUINT32       dwParam3            Third event parameter
 *      MTPEVENTOBJECT* pevObj              Resulting event object
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPEventObjectCreate(PSLUINT16 wEventCode,
                    PSLUINT32 dwTransactionID, PSLUINT32 dwParam1,
                    PSLUINT32 dwParam2, PSLUINT32 dwParam3,
                    MTPEVENTOBJECT* pevObj)
{
    MTPEVENTOBJ*    pmtpevobj = PSLNULL;
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != pevObj)
    {
        *pevObj = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pevObj) || (MTP_EVENTCODE_UNDEFINED == wEventCode))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Retrieve a new event from the object pool
     */

    PSLASSERT(PSLNULL != _OpEventObj);
    ps = PSLObjectPoolAlloc(_OpEventObj, (PSLVOID**)&pmtpevobj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And intialize it
     *
     *  NOTE: Objects are not cleared by the pool so they should be initialized
     *  entirely here.
     */

#ifdef PSL_ASSERTS
    pmtpevobj->dwCookie = MTPEVENTOBJ_COOKIE;
#endif  /* PSL_ASSERTS */
    pmtpevobj->cRef = 1;
    pmtpevobj->mtpEvent.wEventCode = wEventCode;
    pmtpevobj->mtpEvent.dwTransactionID = dwTransactionID;
    pmtpevobj->mtpEvent.dwParam1 = dwParam1;
    pmtpevobj->mtpEvent.dwParam2 = dwParam2;
    pmtpevobj->mtpEvent.dwParam3 = dwParam3;
    pmtpevobj->dwBroadcastMode = MTPEVENTBROADCASTMODE_UNKNOWN;

    /*  Return the object
     */

    *pevObj = pmtpevobj;
    pmtpevobj = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPEVENTOBJECTDESTROY(pmtpevobj);
    return ps;
}


/*  MTPEventObjectAddRef
 *
 *  Increments the reference count on an event object
 *
 *  Arguments:
 *      MTPEVENTOBJECT  evObj               Object to update
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS MTPEventObjectAddRef(MTPEVENTOBJECT evObj)
{
    PSLSTATUS       ps;
    MTPEVENTOBJ*    pmtpevobj;

    /*  Make sure we have work to do
     */

    if (PSLNULL == evObj)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract out object
     */

    pmtpevobj = (MTPEVENTOBJ*)evObj;
    PSLASSERT(MTPEVENTOBJ_COOKIE == pmtpevobj->dwCookie);

    /*  Decrement the reference count to see if we need to do the work
     */

    PSLAtomicIncrement(&(pmtpevobj->cRef));
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPEventObjectDestroy
 *
 *  Destroys an MTP Event Object once all references have been removed
 *
 *  Arguments:
 *      MTPEVENTOBJECT  evObj               Object to destroy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPEventObjectDestroy(MTPEVENTOBJECT evObj)
{
    PSLUINT32       cRef;
    PSLSTATUS       ps;
    MTPEVENTOBJ*    pmtpevobj;

    /*  Make sure we have work to do
     */

    if (PSLNULL == evObj)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Extract out object
     */

    pmtpevobj = (MTPEVENTOBJ*)evObj;
    PSLASSERT(MTPEVENTOBJ_COOKIE == pmtpevobj->dwCookie);

    /*  Decrement the reference count to see if we need to do the work
     */

    cRef = PSLAtomicDecrement(&(pmtpevobj->cRef));
    if (0 < cRef)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Object is no longer in use- release it back to the object pool
     */

    ps = PSLObjectPoolFree(pmtpevobj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPEventObjectStore
 *
 *  Stores the event information in the provided MTPEVENT structure
 *
 *  Arguments:
 *      MTPEVENTOBJECT  evObj               Event to store
 *      PXMTPEVENT      pmtpEvent           Buffer to store event in
 *      PSLUINT32       cbEvent             Size of the event buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPEventObjectStore(MTPEVENTOBJECT evObj,
                    PXMTPEVENT pmtpEvent, PSLUINT32 cbEvent,
                    PSLUINT32* pcbUsed)
{
    PSLSTATUS       ps;
    MTPEVENTOBJ*    pmtpevobj;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == evObj) || (PSLNULL == pmtpEvent) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Validate length of the destination buffer
     */

    if (sizeof(*pmtpEvent) < cbEvent)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpevobj = (MTPEVENTOBJ*)evObj;
    PSLASSERT(MTPEVENTOBJ_COOKIE == pmtpevobj->dwCookie);

    /*  And store the information
     */

    MTPStoreUInt16(&(pmtpEvent->wEventCode), pmtpevobj->mtpEvent.wEventCode);
    MTPStoreUInt32(&(pmtpEvent->dwTransactionID),
                            pmtpevobj->mtpEvent.dwTransactionID);
    MTPStoreUInt32(&(pmtpEvent->dwParam1), pmtpevobj->mtpEvent.dwParam1);
    MTPStoreUInt32(&(pmtpEvent->dwParam2), pmtpevobj->mtpEvent.dwParam2);
    MTPStoreUInt32(&(pmtpEvent->dwParam3), pmtpevobj->mtpEvent.dwParam3);
    *pcbUsed = sizeof(*pmtpEvent);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPEventObjectGetEventCode
 *
 *  Returns the event code associated with this event object
 *
 *  Arguments:
 *      MTPEVENTOBJ     evObj               Event object to query
 *
 *  Returns:
 *      PSLUINT16       Event code associated with object
 */

PSLUINT16 PSL_API MTPEventObjectGetEventCode(MTPEVENTOBJECT evObj)
{
    MTPEVENTOBJ*    pmtpevobj;
    PSLUINT16       wEventCode;

    /*  Make sure we have work to do
     */

    PSLASSERT(PSLNULL != evObj);
    if (PSLNULL == evObj)
    {
        wEventCode = MTP_EVENTCODE_UNDEFINED;
        goto Exit;
    }

    /*  Extract out object
     */

    pmtpevobj = (MTPEVENTOBJ*)evObj;
    PSLASSERT(MTPEVENTOBJ_COOKIE == pmtpevobj->dwCookie);

    /*  And retrieve the associated event code
     */

    wEventCode = pmtpevobj->mtpEvent.wEventCode;

Exit:
    return wEventCode;
}


/*  MTPEventObjectGetParams
 *
 *  Returns the event parameters associated with this event object
 *
 *  Arguments:
 *      MTPEVENTOBJECT  evObj               Event object to query
 *      PSLUINT32*      pdwTransactionID    Event Transaction ID
 *      PSLUINT32*      pdwParam1           Event Param 1
 *      PSLUINT32*      pdwParam2           Event Param 2
 *      PSLUINT32*      pdwParam3           Event Param 3
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPEventObjectGetParams(MTPEVENTOBJECT evObj,
                    PSLUINT32* pdwTransactionID, PSLUINT32* pdwParam1,
                    PSLUINT32* pdwParam2, PSLUINT32* pdwParam3)
{
    PSLSTATUS       ps;
    MTPEVENTOBJ*    pmtpevobj;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwTransactionID)
    {
        *pdwTransactionID = MTP_TRANSACTIONID_NOSESSION;
    }
    if (PSLNULL != pdwParam1)
    {
        *pdwParam1 = 0;
    }
    if (PSLNULL != pdwParam2)
    {
        *pdwParam2 = 0;
    }
    if (PSLNULL != pdwParam3)
    {
        *pdwParam3 = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == evObj) ||
        ((PSLNULL == pdwTransactionID) && (PSLNULL == pdwParam1) &&
                (PSLNULL == pdwParam2) && (PSLNULL == pdwParam3)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpevobj = (MTPEVENTOBJ*)evObj;
    PSLASSERT(MTPEVENTOBJ_COOKIE == pmtpevobj->dwCookie);

    /*  Return the requested information
     */

    if (PSLNULL != pdwTransactionID)
    {
        *pdwTransactionID = pmtpevobj->mtpEvent.dwTransactionID;
    }
    if (PSLNULL != pdwParam1)
    {
        *pdwParam1 = pmtpevobj->mtpEvent.dwParam1;
    }
    if (PSLNULL != pdwParam2)
    {
        *pdwParam2 = pmtpevobj->mtpEvent.dwParam2;
    }
    if (PSLNULL != pdwParam3)
    {
        *pdwParam3 = pmtpevobj->mtpEvent.dwParam3;
    }
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPEventObjectGetBroadcastMode
 *
 *  Retrieves details about how the event is being broadcast around the
 *  system
 *
 *  Arguments:
 *      MTPEVENTOBJECT  evObj               Event object to query
 *      PSLUINT32*      pdwBroadcastMode    Event broadcast mode
 *      MTPCONTEXT**    ppmtpctx            Context associated with broadcast
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPEventObjectGetBroadcastMode(MTPEVENTOBJECT evObj,
                    PSLUINT32* pdwBroadcastMode, MTPCONTEXT** ppmtpctx)
{
    PSLSTATUS       ps;
    MTPEVENTOBJ*    pmtpevobj;

    /*  Clear result for safety
     */

    if (PSLNULL != pdwBroadcastMode)
    {
        *pdwBroadcastMode = MTPEVENTBROADCASTMODE_UNKNOWN;
    }
    if (PSLNULL != ppmtpctx)
    {
        *ppmtpctx = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == evObj) || (PSLNULL == pdwBroadcastMode) ||
        (PSLNULL == ppmtpctx))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmtpevobj = (MTPEVENTOBJ*)evObj;
    PSLASSERT(MTPEVENTOBJ_COOKIE == pmtpevobj->dwCookie);

    /*  If the object has no broadcast mode report failure- we only want to
     *  return inforamtion for broadcast events not for one's pending broadcast
     */

    PSLASSERT(MTPEVENTBROADCASTMODE_UNKNOWN != pmtpevobj->dwBroadcastMode);
    if (MTPEVENTBROADCASTMODE_UNKNOWN == pmtpevobj->dwBroadcastMode)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Return the broadcast mode
     */

    *pdwBroadcastMode = pmtpevobj->dwBroadcastMode;
    switch (*pdwBroadcastMode)
    {
    case MTPEVENTBROADCASTMODE_EXCLUDE_MTPSESSION:
    case MTPEVENTBROADCASTMODE_ONLY_MTPSESSION:
        /*  And the context
         */

        *ppmtpctx = pmtpevobj->pmtpctx;
        break;

    default:
        PSLASSERT(PSLNULL == *ppmtpctx);
        break;
    }
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  _MTPEventManagerNotifyEnum
 *
 *  PSLNotify enumeration callback function.  Used to add a reference count
 *  to the event object prior to a message being posted.
 *
 *  Arguments:
 *      PSLMSG*         pMsg                Message to be posted
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPEventManagerNotifyEnum(PSLMSG* pMsg)
{
    PSLSTATUS       ps;

    /*  Validate arguments
     */

    if (PSLNULL == pMsg)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Add a reference count to the event object located in aParam0
     */

    ps = MTPEventObjectAddRef((MTPEVENTOBJECT)pMsg->aParam0);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPEventManagerInitialize
 *
 *  Initializes the MTP Event Manager
 *
 *  Arguments:
 *      PSLUINT32       cSessionMax         Maximum number of sessions enabled
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPEventManagerInitialize(PSLUINT32 cSessionMax)
{
    PSLUINT32       cEvents;
    PSLSTATUS       ps;

    /*  NOTE: The initialization code is currently guarded to guarantee only
     *  a single thread of execuation- no need to do any additional management
     *  here.
     *
     *  Make sure that we are not already initialized
     */

    PSLASSERT((PSLNULL == _NtfyEventManager) && (PSLNULL == _OpEventObj));
    if ((PSLNULL != _NtfyEventManager) || (PSLNULL != _OpEventObj))
    {
        ps = PSLERROR_IN_USE;
        goto Exit;
    }

    /*  Create a new notifier to manage the registered event queues
     */

    ps = PSLNotifyCreate(&_NtfyEventManager);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Determine the number of events that we will allow- we allow
     *  EVENTOBJ_PER_SESSION events for each session
     */

    cEvents = (0 != cSessionMax) ? (EVENTOBJ_PER_SESSION * cSessionMax) :
                            DEFAULT_EVENTOBJ_POOL;

    /*  And an object pool to handle the events- while we try to allow for
     *  EVENTOBJ_PER_SESSION active events per session this may be insufficient
     *  so we allow the pool to allocate instead of failing.
     */

    ps = PSLObjectPoolCreate(cEvents, sizeof(MTPEVENTOBJ),
                            PSLOBJPOOLFLAG_ALLOW_ALLOC, &_OpEventObj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPEventManagerUninitialize
 *
 *  Destroys the event manager
 *
 *  Arguments:
 *      None
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPEventManagerUninitialize(PSLVOID)
{
    /*  NOTE: Currently the initialization code is designed to guarantee only a
     *  a single active thread of execution- no need to do additional work here
     *  to protect it.
     */

    SAFE_PSLNOTIFYDESTROY(_NtfyEventManager);
    SAFE_PSLOBJECTPOOLDESTROY(_OpEventObj);
    return PSLSUCCESS;
}


/*
 *  MTPEventManagerAddMonitor
 *
 *  Called to register a message queue to receive MTP event notfications
 *
 *
 *  Arguments:
 *      PSLMSGQUEUE     mqNotify            Message queue to notify when
 *                                            event occurs
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if added,
 *                      PSLSUCCESS_EXISTS if already present in list
 */

PSLSTATUS PSL_API MTPEventManagerAddMonitor(PSLMSGQUEUE mqNotify)
{
    PSLSTATUS           ps;

    /*  Validate Arguments
     */

    if (PSLNULL == mqNotify)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Add this message to the event queue- remember to deal with the
     *  fact that it may already be in the list. If it already exist
     *  following function will return PSLSUCCESS_EXISTS
     */

    PSLASSERT(PSLNULL != _NtfyEventManager);
    ps = PSLNotifyRegister(_NtfyEventManager, mqNotify);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*
 *  MTPEventManagerRemoveMonitor
 *
 *  Called to unregister a message queue from MTP event manager
 *
 *  Arguments:
 *      PSLMSGQUEUE     mqNotify            Message queue to remove
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if removed,
 *                      PSLSUCCESS_NOT_FOUND if not present
 */

PSLSTATUS PSL_API MTPEventManagerRemoveMonitor(PSLMSGQUEUE mqNotify)
{
    PSLSTATUS       ps;

    /*  Validate Arguments
     */

    if (PSLNULL == mqNotify)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Try to remove the entry from the list
     */

    PSLASSERT(PSLNULL != _NtfyEventManager);
    ps = PSLNotifyUnregister(_NtfyEventManager, mqNotify);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPEventManagerBroadcastEvent
 *
 *  Called to initiate an MTP event broadcast.
 *
 *  Arguments:
 *      MTPEVENTOBJECT  evObj               Event object to broacast
 *      PSLUINT32       dwBroadcastMode     Broadcast mode for event
 *      MTPCONTEXT*     pmtpctx             Context associated with event
 *
 *  Returns:
 *      PSLTATUS        PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPEventManagerBroadcastEvent(MTPEVENTOBJECT evObj,
                    PSLUINT32 dwBroadcastMode, MTPCONTEXT* pmtpctx)
{
    PSLSTATUS           ps;
    MTPEVENTOBJ*        pmtpevobj;

    /*  Validate arguments
     */

    if (PSLNULL == evObj)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    switch (dwBroadcastMode)
    {
    case MTPEVENTBROADCASTMODE_ALL:
        /*  No additional validation required- context must be ignored
         */

        pmtpctx = PSLNULL;
        break;

    case MTPEVENTBROADCASTMODE_EXCLUDE_MTPSESSION:
    case MTPEVENTBROADCASTMODE_ONLY_MTPSESSION:
        /*  Context is required for these broadcast modes
         */

        if (PSLNULL == pmtpctx)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }
        break;

    default:
        /*  Unknown broadcast mode
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Unpack the event object
     */

    pmtpevobj = (MTPEVENTOBJ*)evObj;
    PSLASSERT(MTPEVENTOBJ_COOKIE == pmtpevobj->dwCookie);

    /*  Validate that we are not already broadcasting this event
     */

    PSLASSERT(MTPEVENTBROADCASTMODE_UNKNOWN == pmtpevobj->dwBroadcastMode);
    if (MTPEVENTBROADCASTMODE_UNKNOWN != pmtpevobj->dwBroadcastMode)
    {
        ps = PSLERROR_INVALID_OPERATION;
        goto Exit;
    }

    /*  Initialize the event parameters
     */

    pmtpevobj->dwBroadcastMode = dwBroadcastMode;
    pmtpevobj->pmtpctx = pmtpctx;

    /*  Handle the broadcast
     */

    PSLASSERT(PSLNULL != _NtfyEventManager);
    ps = PSLNotifyMsgPost(_NtfyEventManager, _MTPEventManagerNotifyEnum,
                            MTPMSG_EVENT, (PSLPARAM)evObj, 0, 0, 0, 0);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  The caller assumes that we just took ownership of the event object.
     *  Since we add a reference count for every registered queue the process
     *  of sending the notifications will add an extra reference count to the
     *  object.  Pull it off here so that we "take ownership" of the object
     *  entirely.
     */

    MTPEventObjectDestroy(evObj);

Exit:
    return ps;
}

