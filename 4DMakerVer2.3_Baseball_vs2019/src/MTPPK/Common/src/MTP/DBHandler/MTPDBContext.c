/*
 *  MTPDBContext.c
 *
 *  Contains implementations of the shared DB Context behaviors.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MTPPrecomp.h"

/*  Local Defines
 */

/*  Local Types
 */

/*  Local Functions
 */

/*  Local Variables
 */

/*  MTPDBContextCancelBase
 *
 *  Called when an in progress operation is cancelled.  Always reports
 *  successful cancellation.
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context being cancelled
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextCancelBase(MTPDBCONTEXT mdbc)
{
    /*  Validate Arguments
     */

    return (PSLNULL != mdbc) ? PSLSUCCESS : PSLERROR_INVALID_PARAMETER;
}


/*  MTPDBContextGetCountBase
 *
 *  Returns the count of items in the current context.  This method always
 *  reports no items are available.
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context being queried
 *      PSLUINT32*      pcItems             Return for count of items
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextGetCountBase(MTPDBCONTEXT mdbc,
                    PSLUINT32* pcItems)
{
    /*  Clear result for safety
     */

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }
    return ((PSLNULL != mdbc) && (PSLNULL != pcItems)) ?
                            PSLSUCCESS : PSLERROR_INVALID_PARAMETER;
}


/*  MTPDBContextGetHandleBase
 *
 *  Returns one of the object handles associated with the context.  Always
 *  fails with invalid range.
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context being queried
 *      PSLBOOL         fResolveContext     PSLTRUE to resolve the context
 *                                            to get an accurate count
 *      PSLUINT32       idxItem             Item being requiested
 *      PSLUINT32*      pdwObjectHandle     Object handle return buffer
 *      PSLUINT32*      pdwParanetHandle    Parent object handle return buffer
 *      PSLUINT32*      pdwStorageID        Storage ID return buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextGetHandleBase(MTPDBCONTEXT mdbc,
                    PSLBOOL fResolveContext, PSLUINT32 idxItem,
                    PSLUINT32* pdwObjectHandle, PSLUINT32* pdwParentHandle,
                    PSLUINT32* pdwStorageID)
{
    /*  Clear results for safety
     */

    if (PSLNULL != pdwObjectHandle)
    {
        *pdwObjectHandle = MTP_OBJECTHANDLE_UNDEFINED;
    }
    if (PSLNULL != pdwParentHandle)
    {
        *pdwParentHandle = MTP_OBJECTHANDLE_UNDEFINED;
    }
    if (PSLNULL != pdwStorageID)
    {
        *pdwStorageID = MTP_STORAGEID_UNDEFINED;
    }
    return ((PSLNULL != mdbc) && ((PSLNULL != pdwObjectHandle) ||
            (PSLNULL != pdwParentHandle) || (PSLNULL != pdwStorageID))) ?
                            PSLERROR_INVALID_RANGE : PSLERROR_INVALID_PARAMETER;
    fResolveContext;
    idxItem;
}


/*  MTPDBContextCommitBase
 *
 *  This function when called to commit all the data that may have been
 *  cached within this context to the database. This implementation always
 *  reports success.
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context being committed
 *      PSLUINT32       dwObjectID          Object ID of item to commit
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextCommitBase(MTPDBCONTEXT mdbc,
                    PSLUINT32 dwObjectID)
{
    /*  Validate Arguments
     */

    return (PSLNULL != mdbc) ? PSLSUCCESS : PSLERROR_INVALID_PARAMETER;
    dwObjectID;
}


/*  MTPDBContextCopyBase
 *
 *  Copys the contents of the source DB Context to the destination DB Context.
 *  This implementation always reports not implemented
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbcDest            Destination context for copy
 *      MTPDBCONTEXT    mdbcSrc             Source context for copy
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextCopyBase(MTPDBCONTEXT mdbcDest,
                    MTPDBCONTEXT mdbcSrc)
{
    /*  Validate Arguments
     */

    return ((PSLNULL != mdbcDest) && (PSLNULL != mdbcSrc)) ?
                        PSLERROR_NOT_IMPLEMENTED : PSLERROR_INVALID_PARAMETER;
}


/*  MTPDBContextMoveBase
 *
 *  Moves the contents of the source DB Context to the destination DB Context.
 *  This implementation always reports not implemented
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbcDest            Destination context for move
 *      MTPDBCONTEXT    mdbcSrc             Source context for move
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextMoveBase(MTPDBCONTEXT mdbcDest,
                    MTPDBCONTEXT mdbcSrc)
{
    /*  Validate Arguments
     */

    return ((PSLNULL != mdbcDest) && (PSLNULL != mdbcSrc)) ?
                        PSLERROR_NOT_IMPLEMENTED : PSLERROR_INVALID_PARAMETER;
}


/*  MTPDBContextDeleteBase
 *
 *  Deletes the contents of the DB Context.  This implementation always
 *  reports not implemented
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context to be deleted
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDeleteBase(MTPDBCONTEXT mdbc)
{
    /*  Validate Arguments
     */

    return (PSLNULL != mdbc) ?
                        PSLERROR_NOT_IMPLEMENTED : PSLERROR_INVALID_PARAMETER;
}


/*  MTPDBContextFormatBase
 *
 *  Formats the contents of the storage represented in the DB Context.  This
 *  implementation always reports not implemented
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context to be deleted
 *      PSLUINT32       dwFSFormat          File system format
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextFormatBase(MTPDBCONTEXT mdbc,
                    PSLUINT32 dwFSFormat)
{
    /*  Validate Arguments
     */

    return (PSLNULL != mdbc) ?
                        PSLERROR_NOT_IMPLEMENTED : PSLERROR_INVALID_PARAMETER;
    dwFSFormat;
}


/*  MTPDBContextDataOpenBase
 *
 *  Opens a DB Context Data for this object.  The context data is the "what"
 *  portion of the query.  This method always reports not implemented.
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context to associate with the
 *                                            DB Context Data
 *      PSLUINT32       dwDataFormat        Format of the data
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              DB Variant Props defining data
 *                                            context
 *      PSLUINT32       cmdbv               Number of properties
 *      PSLUINT64*      pcbTotalSize        Size of the expected buffer
 *      MTPDBCONTEXTDATA*
 *                      pmdbcd              Return buffer for context data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataOpenBase(MTPDBCONTEXT mdbc,
                    PSLUINT32 dwDataFormat, PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv, PSLUINT64* pcbTotalSize,
                    MTPDBCONTEXTDATA* pmdbcd)
{
    /*  Clear results for safety
     */

    if (PSLNULL != pcbTotalSize)
    {
        *pcbTotalSize = 0;
    }
    if (PSLNULL != pmdbcd)
    {
        *pmdbcd = PSLNULL;
    }

    /*  Validate Arguments
     */

    return ((PSLNULL != mdbc) && ((0 == cmdbv) || (PSLNULL != rgmdbv)) &&
            (PSLNULL != pmdbcd)) ?
                        PSLERROR_NOT_IMPLEMENTED : PSLERROR_INVALID_PARAMETER;
    dwDataFormat;
}


