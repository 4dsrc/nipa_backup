/*
 *  MTPHandlerObjectHandles.c
 *
 *  Contains the implementation of core command handlers
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerUtil.h"
#include "MTPHandlerDatabase.h"
#include "MTPDBHandlerUtil.h"

static PSLSTATUS PSL_API _MTPContextPrepareGetNumObjects(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetObjectHandles(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetPartialObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareSendObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareSetObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextDataConsumedSendObject(
                    BASEHANDLERDATA* pData);

static PSLSTATUS PSL_API _MTPContextDataConsumedSetObject(
                    BASEHANDLERDATA* pData);

static PSLSTATUS PSL_API _MTPContextPrepareDeleteObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareCopyObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareMoveObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetThumb(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

/*
 *  MTPGetNumObjects
 *
 *  This handler function for GetNumObjects MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetNumObjects (
                        MTPDISPATCHER md,
                        PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_RESPONSE,
                                  _MTPContextPrepareGetNumObjects,
                                  PSLNULL);
}

/*
 *  MTPGetObjectHandles
 *
 *  This handler function for GetObjectHandles MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetObjectHandles(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                                  _MTPContextPrepareGetObjectHandles,
                                  PSLNULL);
}

/*
 *  MTPGetObject
 *
 *  This handler function for GetObject MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetObject(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                                  _MTPContextPrepareGetObject,
                                  PSLNULL);
}

/*
 *  MTPGetPartialObject
 *
 *  This handler function for GetPartialObject MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetPartialObject(MTPDISPATCHER md, PSLMSG* pMsg)
{
    /* TODO: we need a way to get result from DB side for Get ops. */
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                                  _MTPContextPrepareGetPartialObject,
                                  PSLNULL);
}

/*
 *  MTPSendObject
 *
 *  This handler function for SendObject MTP operation.
 *
 */
PSLSTATUS PSL_API MTPSendObject (MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_IN,
                                  _MTPContextPrepareSendObject,
                                  _MTPContextDataConsumedSendObject);
}

/*
 *  MTPDeleteObject
 *
 *  This handler function for DeleteObject MTP operation.
 *
 */
PSLSTATUS PSL_API MTPDeleteObject (MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_RESPONSE,
                                  _MTPContextPrepareDeleteObject,
                                  PSLNULL);
}

/*
 *  MTPCopyObject
 *
 *  This handler function for CopyObject MTP operation.
 *
 */
PSLSTATUS PSL_API MTPCopyObject (MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_RESPONSE,
                                  _MTPContextPrepareCopyObject,
                                  PSLNULL);
}

/*
 *  MTPMoveObject
 *
 *  This handler function for MoveObject MTP operation.
 *
 */
PSLSTATUS PSL_API MTPMoveObject (MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_RESPONSE,
                                  _MTPContextPrepareMoveObject,
                                  PSLNULL);
}

/*
 *  MTPGetThumb
 *
 *  This handler function for MTPGetThumb MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetThumb(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                                  _MTPContextPrepareGetThumb,
                                  PSLNULL);
}

/*
 * Utilities.
 */
PSLSTATUS PSL_API _MTPContextPrepareGetNumObjects(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[3];
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT32           cItems;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(3))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[0].wPropCode = MTP_OBJECTPROPCODE_STORAGEID;
    rgmpq[0].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    rgmpq[1].wPropCode = MTP_OBJECTPROPCODE_OBJECTFORMAT;
    rgmpq[1].wDataType = MTP_DATATYPE_UINT16;
    rgmpq[1].vParam.valUInt16 = (PSLUINT16)MTPLoadUInt32(
                            &(pOpRequest->dwParam2));

    rgmpq[2].wPropCode = MTP_OBJECTPROPCODE_PARENT;
    rgmpq[2].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[2].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam3));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 3, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBContextGetCount(mdbc, &cItems);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* all goes well, fill in the response */
    pData->opLevelData.mtpResponse.dwParam1 = cItems;
    pData->opLevelData.dwNumRespParams = 1;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareGetObjectHandles(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[3];
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(3))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[0].wPropCode = MTP_OBJECTPROPCODE_STORAGEID;
    rgmpq[0].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    rgmpq[1].wPropCode = MTP_OBJECTPROPCODE_OBJECTFORMAT;
    rgmpq[1].wDataType = MTP_DATATYPE_UINT16;
    rgmpq[1].vParam.valUInt32 = (PSLUINT16)MTPLoadUInt32(
                            &(pOpRequest->dwParam2));

    rgmpq[2].wPropCode = MTP_OBJECTPROPCODE_PARENT;
    rgmpq[2].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[2].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam3));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 3, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                           MTPDBCONTEXTDATA_QUERY,
                           MTPDBCONTEXTDATAFORMAT_OBJECTHANDLE);

    status = MTPDBContextDataOpen(mdbc, dwFormat, PSLNULL, 0,
                            &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareGetObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    mpq;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    mpq.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    mpq.wDataType = MTP_DATATYPE_UINT32;
    mpq.vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, &mpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                           MTPDBCONTEXTDATA_QUERY,
                           MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA);

    status = MTPDBContextDataOpen(mdbc, dwFormat, PSLNULL, 0,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareGetPartialObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[3];
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(3))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[0].wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    rgmpq[0].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    rgmpq[1].wPropCode = MTP_OBJECTPROPCODE_DATAOFFSET;
    rgmpq[1].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[1].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam2));

    rgmpq[2].wPropCode = MTP_OBJECTPROPCODE_DATALENGTH;
    rgmpq[2].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[2].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam3));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                           MTPDBCONTEXTDATA_QUERY,
                           MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA);

    status = MTPDBContextDataOpen(mdbc, dwFormat, (rgmpq + 1), 2,
                            &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareSendObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    MTPDBVARIANTPROP    mpq;
    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(0))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    PSLASSERT(MTP_OBJECTHANDLE_UNDEFINED !=
                            pData->sesLevelData.dwLastSendInfoID);
    if (MTP_OBJECTHANDLE_UNDEFINED == pData->sesLevelData.dwLastSendInfoID)
    {
        status = PSLERROR_NOT_INITIALIZED;
        goto Exit;
    }

    mpq.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    mpq.wDataType = MTP_DATATYPE_UINT32;
    mpq.vParam.valUInt32 = pData->sesLevelData.dwLastSendInfoID;

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, &mpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                       MTPDBCONTEXTDATA_UPDATE,
                       MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA);

    status = MTPDBContextDataOpen(mdbc, dwFormat, PSLNULL, 0,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->bhd.opLevelData.dwObjectID =
                            pData->sesLevelData.dwLastSendInfoID;

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextDataConsumedSendObject(BASEHANDLERDATA* pData)
{
    PSLSTATUS status;

    if (PSLNULL == pData)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPDBContextCommit(((DBHANDLERDATA*)pData)->mdbc,
                            pData->opLevelData.dwObjectID);

Exit:
    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareDeleteObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[2];
    MTPDBCONTEXT        mdbc = PSLNULL;
    PSLUINT32           cmpq = 0;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(2))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
    cmpq++;

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_OBJECTFORMAT;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT16;
    rgmpq[cmpq].vParam.valUInt16 = (PSLUINT16)MTPLoadUInt32(
                            &(pOpRequest->dwParam2));
    cmpq++;

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, cmpq, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBContextDelete(mdbc);

Exit:
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareCopyObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpqSrc[1], rgmpqDest[2];
    MTPDBCONTEXT        mdbcSrc  = PSLNULL;
    MTPDBCONTEXT        mdbcDest = PSLNULL;

    PSLUINT32           dwNewObjHandle;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(3))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpqSrc[0].wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    rgmpqSrc[0].wDataType = MTP_DATATYPE_UINT32;
    rgmpqSrc[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    rgmpqDest[0].wPropCode = MTP_OBJECTPROPCODE_STORAGEID;
    rgmpqDest[0].wDataType = MTP_DATATYPE_UINT32;
    rgmpqDest[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam2));

    rgmpqDest[1].wPropCode = MTP_OBJECTPROPCODE_PARENT;
    rgmpqDest[1].wDataType = MTP_DATATYPE_UINT32;
    rgmpqDest[1].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam3));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpqSrc, 1, &mdbcSrc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpqDest, 2, &mdbcDest);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBContextCopy(mdbcDest, mdbcSrc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBContextCommit(mdbcDest, MTP_OBJECTHANDLE_UNDEFINED);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBContextGetHandle(mdbcDest, PSLFALSE, 0,
                            &dwNewObjHandle, PSLNULL, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pData->opLevelData.mtpResponse.dwParam1 = dwNewObjHandle;
    pData->opLevelData.dwNumRespParams = 1;

Exit:
    SAFE_MTPDBCONTEXTCLOSE(mdbcDest);
    SAFE_MTPDBCONTEXTCLOSE(mdbcSrc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareMoveObject(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpqSrc[1], rgmpqDest[2];
    MTPDBCONTEXT        mdbcSrc  = PSLNULL;
    MTPDBCONTEXT        mdbcDest = PSLNULL;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(3))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpqSrc[0].wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    rgmpqSrc[0].wDataType = MTP_DATATYPE_UINT32;
    rgmpqSrc[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    rgmpqDest[0].wPropCode = MTP_OBJECTPROPCODE_STORAGEID;
    rgmpqDest[0].wDataType = MTP_DATATYPE_UINT32;
    rgmpqDest[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam2));

    rgmpqDest[1].wPropCode = MTP_OBJECTPROPCODE_PARENT;
    rgmpqDest[1].wDataType = MTP_DATATYPE_UINT32;
    rgmpqDest[1].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam3));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpqSrc, 1, &mdbcSrc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpqDest, 2, &mdbcDest);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBContextMove(mdbcDest, mdbcSrc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBContextCommit(mdbcDest, MTP_OBJECTHANDLE_UNDEFINED);

Exit:
    SAFE_MTPDBCONTEXTCLOSE(mdbcDest);
    SAFE_MTPDBCONTEXTCLOSE(mdbcSrc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareGetThumb(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    mpq;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    mpq.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    mpq.wDataType = MTP_DATATYPE_UINT32;
    mpq.vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, &mpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                           MTPDBCONTEXTDATA_QUERY,
                           MTPDBCONTEXTDATAFORMAT_OBJECTTHUMBDATA);

    status = MTPDBContextDataOpen(mdbc, dwFormat, PSLNULL, 0,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}



