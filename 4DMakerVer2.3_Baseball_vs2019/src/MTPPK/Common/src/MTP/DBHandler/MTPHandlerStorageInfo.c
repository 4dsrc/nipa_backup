/*
 *  MTPHandlerStorageInfo.c
 *
 *  Contains the implementation of GetStorageInfo command handlers
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerUtil.h"
#include "MTPHandlerDatabase.h"
#include "MTPDBHandlerUtil.h"

PSLSTATUS PSL_API _MTPContextPrepareGetStorageInfo(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

/*
 *  MTPGetStorageInfo
 *
 *  This handler function for GetStorageInfo MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetStorageInfo(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                                  _MTPContextPrepareGetStorageInfo,
                                  PSLNULL);
}

PSLSTATUS PSL_API _MTPContextPrepareGetStorageInfo(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;
    MTPDBVARIANTPROP    mpq;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    mpq.wPropCode = MTP_OBJECTPROPCODE_STORAGEID;
    mpq.wDataType = MTP_DATATYPE_UINT32;
    mpq.vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, &mpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                                    MTPDBCONTEXTDATA_QUERY,
                                    MTPDBCONTEXTDATAFORMAT_STORAGE);

    status = MTPDBContextDataOpen(mdbc, dwFormat, PSLNULL, 0,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}
