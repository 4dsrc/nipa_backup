/*
 *  MTPDBContextPool.c
 *
 *  Contains implementation of the DB Context Pool
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPDBContextPool.h"

/*  Local Defines
 */

/*  Local Types
 */

/*  Local Functions
 */

/*  Local Variables
 */

/*  MTPDBContextDataPoolAlloc
 *
 *  Allocates a new context blob out of the collective context pool
 *
 *  Arguments:
 *      PSLUINT32       cbObject            Size of object to allocate
 *      PSLVOID**       ppvContext          Return buffer for object
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataPoolAlloc(PSLUINT32 cbObject,
                    PSLVOID** ppvContext)
{
    PSLSTATUS       ps;

    /*  Clear result for safety
     */

    if (PSLNULL != ppvContext)
    {
        *ppvContext = PSLNULL;
    }

    /*  Valdiate arguments
     */

    if ((0 == cbObject) || (PSLNULL == ppvContext))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  For now just allocate the object directly
     */

    *ppvContext = PSLMemAlloc(PSLMEM_PTR, cbObject);
    if (PSLNULL == *ppvContext)
    {
         ps = PSLERROR_OUT_OF_MEMORY;
         goto Exit;
    }
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPDBContextDataPoolFree
 *
 *  Frees the context pool object
 *
 *  Arguments:
 *      PSLVOID*        pvObject            Object to free
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataPoolFree(PSLVOID* pvContext)
{
    return (PSLNULL == PSLMemFree(pvContext)) ?
                            PSLSUCCESS : PSLERROR_INVALID_BUFFER;
}

