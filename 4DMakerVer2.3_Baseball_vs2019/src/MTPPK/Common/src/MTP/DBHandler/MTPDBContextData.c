/*
 *  MTPDBContextData.c
 *
 *  Contains implementations of the base MTPDBContextData support
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPDBContextPool.h"
#include "MTPDBContextData.h"
#include "MTPDBContextUtils.h"

/*  Local Defines
 */

#define MTPDBCONTEXTDATABASEOBJ_COOKIE  'dcdB'

#define MTPSTORAGEINFOCTXOBJ_COOKIE     'stCD'

#define MTPOBJECTCAPSCTXOBJ_COOKIE      'ocCD'

#define MTPOBJECTHANDLESCTXOBJ_COOKIE   'ohCD'

#define MTPOBJECTPROPLISTCTXOBJ_COOKIE  'opCD'

#define MTPOBJECTBINARYCTXOBJ_COOKIE    'obCD'

enum
{
    MTPOBJECTCAPS_OBJECT_PROPDESC = 0,
    MTPOBJECTCAPS_PROPDESC_LIST,
    MTPOBJECTCAPS_FORMAT_COUNT,
    MTPOBJECTCAPS_FORMAT_LIST
};

enum
{
    MTPOBJECTPROPLIST_PROPERTY_HANDLE = 0,
    MTPOBJECTPROPLIST_PROPERTY_TYPE,
    MTPOBJECTPROPLIST_PROPERTY_VALUE,
    MTPOBJECTPROPLIST_PROPERTYLIST_COUNT,
    MTPOBJECTPROPLIST_PROPERTYLIST_VALUES,
};

/*  Local Types
 */

typedef struct _MTPSTORAGEINFOCTXOBJ
{
    MTPDBCONTEXTDATABASEOBJ             mtpCDB;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    PSL_CONST MTPSTORAGEINFO*           pStorageInfo;
    PSLUINT64                           qwFreeSpace;
    PSLUINT32                           dwFreeObjects;
} MTPSTORAGEINFOCTXOBJ;

typedef struct _MTPOBJECTCAPSCTXOBJ
{
    MTPDBCONTEXTDATABASEOBJ             mtpCDB;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    PSL_CONST PSLVOID*                  rgvFormatList;
    PSLUINT32                           cFormats;
    PSL_CONST PSLVOID*                  rgvMethodList;
    PSLUINT32                           cMethods;
    PSLUINT32                           wFormat;
    PSLUINT16                           wFlags;
    PSLUINT16                           wPropFlags;
    PSLUINT32                           idxFormat;
    PSL_CONST MTPOBJECTPROPINFO*        popiCur;
    MTPOBJECTPROPINFO                   opiCache;
} MTPOBJECTCAPSCTXOBJ;

typedef struct _MTPOBJECTHANDLESCTXOBJ
{
    MTPDBCONTEXTDATABASEOBJ             mtpCDB;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    MTPDBCONTEXT                        mdbc;
} MTPOBJECTHANDLESCTXOBJ;

typedef struct _MTPOBJECTPROPLISTCTXOBJ
{
    MTPDBCONTEXTDATABASEOBJ             mtpCDB;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    MTPDBCONTEXTBASE                    mdbc;
    PSLUINT32                           dwPropCtx;
    PSLUINT32                           dwGroupCodeCtx;
    PSLUINT32                           cPropsTotal;
    PSLBOOL                             fCountTotal;
    PSLUINT32                           idxItem;
    PSL_CONST MTPFORMATINFO*            pfiCur;
    PSLUINT32                           dwObjectID;
    PSLPARAM                            aParamItem;
    PSL_CONST MTPOBJECTPROPINFO*        pobjProp;
    PSL_CONST MTPPROPERTYREC*           precProp;
    PSLUINT32                           idxProp;
} MTPOBJECTPROPLISTCTXOBJ;

typedef struct _MTPOBJECTBINARYCTXOBJ
{
    MTPDBCONTEXTDATAOBJ                 mtpCDB;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERTS */
    MTPDBCONTEXTBASE                    mdbc;
    PSLBOOL                             fCommit;
    PSL_CONST MTPFORMATINFO*            pfiCur;
    PSLUINT32                           dwObjectID;
    PSLPARAM                            aParamItem;
} MTPOBJECTBINARYCTXOBJ;

/*  Local Functions
 */

static PSLSTATUS PSL_API _MTPDBContextDataBaseClose(MTPDBCONTEXTDATA mdbcd);

static PSLSTATUS PSL_API _MTPDBContextDataBaseSerialize(MTPDBCONTEXTDATA mdbcd,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);

static PSLSTATUS PSL_API _MTPObjectCapsCtxSerializeValue(
                    PSLPARAM aRootContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbBuf);

static PSLSTATUS PSL_API _MTPObjectCapsCtxGetCount(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT32* pcItems);

static PSLSTATUS PSL_API _MTPObjectCapsCtxGetTemplate(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSL_CONST MTPSERIALIZEINFO** prgmsiTemplate,
                    PSLUINT32* pcItems,
                    PFNONMTPSERIALIZEDESTROY* ppfnOnDestroy);

static PSLSTATUS PSL_API _MTPObjectCapsCtxGetContext(
                    PSLUINT32 idxItem,
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLPARAM* paItemContext,
                    PSL_CONST PSLVOID** ppvItemOffset);

static PSLSTATUS PSL_API _MTPObjectHandlesCtxGetCount(PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT32* pcItems);

static PSLSTATUS PSL_API _MTPObjectHandlesCtxGetItem(PSLUINT32 idxItem,
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS PSL_API _MTPObjectPropListCtxGetValue(PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS PSL_API _MTPObjectPropListCtxOnSerializeDestroy(
                    PSL_CONST MTPSERIALIZEINFO* rgmsiTemplate,
                    PSLPARAM aContext,
                    PSL_CONST PSLVOID* pvOffset);

static PSLSTATUS PSL_API _MTPObjectPropListCtxGetTemplate(PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSL_CONST MTPSERIALIZEINFO** prgmsiTemplate,
                    PSLUINT32* pcItems,
                    PFNONMTPSERIALIZEDESTROY* ppfnOnDestroy);

static PSLSTATUS PSL_API _MTPObjectPropListCtxGetContext(PSLUINT32 idxItem,
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLPARAM* paItemContext,
                    PSL_CONST PSLVOID** ppvItemOffset);

static PSLSTATUS PSL_API _MTPObjectPropListCtxGetCount(PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT32* pcItems);

static PSLSTATUS PSL_API _MTPObjectPropListCtxSerialize(MTPDBCONTEXTDATA mdbcd,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);

static PSLSTATUS PSL_API _MTPDBObjectBinaryCtxClose(MTPDBCONTEXTDATA mdbcd);

static PSLSTATUS PSL_API _MTPDBObjectBinaryCtxCancel(MTPDBCONTEXTDATA mdbcd);

static PSLSTATUS PSL_API _MTPDBObjectBinaryCtxSerialize(MTPDBCONTEXTDATA mdbcd,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);

/*  Local Variables
 */

/*  Generic Context Data
 */

static PSL_CONST MTPDBCONTEXTDATAVTBL   _VtblContextDataBase =
{
    MTPDBContextDataBaseClose,
    MTPDBContextDataBaseCancel,
    MTPDBContextDataBaseSerialize,
    MTPDBContextDataBaseStartDeserialize,
    MTPDBContextDataBaseDeserialize
};


/*  MTP Storage Info
 */

static PSL_CONST MTPSERIALIZEINFO       _RgmsiStorageInfo[] =
{
    /*  Storage Type */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(MTPSTORAGEINFOCTXOBJ, pStorageInfo),
        PSLOFFSETOF(MTPSTORAGEINFO, wStorageType)
    },

    /*  File System Type
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(MTPSTORAGEINFOCTXOBJ, pStorageInfo),
        PSLOFFSETOF(MTPSTORAGEINFO, wFileSystemType)
    },

    /*  Access Capability
     */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(MTPSTORAGEINFOCTXOBJ, pStorageInfo),
        PSLOFFSETOF(MTPSTORAGEINFO, wAccessCapability)
    },

    /*  Max Capacity
     */

    {
        MTP_DATATYPE_UINT64,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_VALOFFSET,
        PSLOFFSETOF(MTPSTORAGEINFOCTXOBJ, pStorageInfo),
        PSLOFFSETOF(MTPSTORAGEINFO, qwMaxCapacity)
    },

    /*  Free Space in Bytes
     */

    {
        MTP_DATATYPE_UINT64,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPSTORAGEINFOCTXOBJ, qwFreeSpace),
        0
    },

    /*  Free Space in Objects
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPSTORAGEINFOCTXOBJ, dwFreeObjects),
        0
    },

    /*  Storage Description
     */

    {
        MTP_DATATYPE_STRING,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_REFOFFSET,
        PSLOFFSETOF(MTPSTORAGEINFOCTXOBJ, pStorageInfo),
        PSLOFFSETOF(MTPSTORAGEINFO, szStorageDescription)
    },

    /*  Volume Identifier
     */

    {
        MTP_DATATYPE_STRING,
        MTPSERIALIZEFLAG_BYOFFSETREF | MTPSERIALIZEFLAG_REFOFFSET,
        PSLOFFSETOF(MTPSTORAGEINFOCTXOBJ, pStorageInfo),
        PSLOFFSETOF(MTPSTORAGEINFO, szVolumeIdentifier)
    }
};


/*  MTP Format Capabilities
 */

static PSL_CONST MTPSERIALIZETEMPLATEVTBL   _VtblSerializeOCTemplate =
{
    _MTPObjectCapsCtxGetTemplate,
    _MTPObjectCapsCtxGetContext
};

static PSL_CONST MTPSERIALIZETEMPLATEREPEATVTBL _VtblSerializeOCTemplateRepeat =
{
    _MTPObjectCapsCtxGetTemplate,
    _MTPObjectCapsCtxGetContext,
    _MTPObjectCapsCtxGetCount
};

static PSL_CONST MTPSERIALIZEINFO   _RgmsiObjectCapsPropDesc[] =
{
    /*  Service Object Property Description
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPObjectCapsCtxSerializeValue,
        MTPOBJECTCAPS_OBJECT_PROPDESC
    },
};

static PSL_CONST MTPSERIALIZEINFO   _RgmsiObjectCapsFormatArray[] =
{
    /*  Format Code */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPFORMATINFO, wFormatCode),
        0
    },

    /*  Number of Object Property Descriptions
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPFORMATINFO, cObjectProps),
        0
    },

    /*  Object Property Descriptions
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYTEMPLATEREPEAT,
        (PSLPARAM)&_VtblSerializeOCTemplateRepeat,
        MTPOBJECTCAPS_PROPDESC_LIST
    },

    /*  Interdependent Property Descriptions
     */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYVALUE,
        0,
        0
    }
};

static PSL_CONST MTPSERIALIZEINFO   _RgmsiObjectCaps[] =
{
    /*  Number of Formats */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPObjectCapsCtxSerializeValue,
        MTPOBJECTCAPS_FORMAT_COUNT
    },

    /*  Serialize Format
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYTEMPLATEREPEAT,
        (PSLPARAM)&_VtblSerializeOCTemplateRepeat,
        MTPOBJECTCAPS_FORMAT_LIST
    }
};


/*  MTP Object Handles
 */

static PSL_CONST MTPSERIALIZEARRAYVTBL  _VtblObjectHandlesArray =
{
    _MTPObjectHandlesCtxGetCount,
    _MTPObjectHandlesCtxGetItem
};

static PSL_CONST MTPSERIALIZEINFO       _RgmsiObjectHandles[] =
{
    /*  Array of Object Handles */

    {
        MTP_DATATYPE_AUINT32,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)&_VtblObjectHandlesArray,
        0
    }
};


/*  MTP Get Object Prop List
 */

static PSL_CONST MTPDBCONTEXTDATAVTBL   _VtblContextDataObjectPropList =
{
    MTPDBContextDataBaseClose,
    MTPDBContextDataBaseCancel,
    _MTPObjectPropListCtxSerialize,
    MTPDBContextDataBaseStartDeserialize,
    MTPDBContextDataBaseDeserialize
};

static PSL_CONST MTPSERIALIZETEMPLATEREPEATVTBL _VtblObjectPropListTemplateRepeat =
{
    _MTPObjectPropListCtxGetTemplate,
    _MTPObjectPropListCtxGetContext,
    _MTPObjectPropListCtxGetCount,
};

static PSL_CONST MTPSERIALIZEINFO       _RgmsiObjectPropListPropertyValue[] =
{
    /*  Object Handle */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPObjectPropListCtxGetValue,
        MTPOBJECTPROPLIST_PROPERTY_HANDLE
    },

    /*  Property Code */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYOFFSETVAL,
        PSLOFFSETOF(MTPOBJECTPROPINFO, infoProp.wPropCode),
        0
    },

    /*  Property Type */

    {
        MTP_DATATYPE_UINT16,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPObjectPropListCtxGetValue,
        MTPOBJECTPROPLIST_PROPERTY_TYPE
    },

    /*  Property Value */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPObjectPropListCtxGetValue,
        MTPOBJECTPROPLIST_PROPERTY_VALUE
    },
};

static PSL_CONST MTPSERIALIZEINFO       _RgmsiObjectPropList[] =
{
    /*  Number of objects */

    {
        MTP_DATATYPE_UINT32,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPObjectPropListCtxGetValue,
        MTPOBJECTPROPLIST_PROPERTYLIST_COUNT
    },

    /*  Property List */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYTEMPLATEREPEAT,
        (PSLPARAM)&_VtblObjectPropListTemplateRepeat,
        MTPOBJECTPROPLIST_PROPERTYLIST_VALUES
    }
};


/*  MTP Get Object Binary
 */

static PSL_CONST MTPDBCONTEXTDATAVTBL   _VtblContextDataObjectBinary =
{
    _MTPDBObjectBinaryCtxClose,
    _MTPDBObjectBinaryCtxCancel,
    _MTPDBObjectBinaryCtxSerialize,
    MTPDBContextDataStartDeserializeBase,
    MTPDBContextDataDeserializeBase
};


/*  MTPDBContextBaseGetItemBase
 *
 *  Returns the next item in the list
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context for this call
 *      PSLUINT32       idxItem             Index of item in context requested
 *      PSLUINT32       dwFormat            Format being requested
 *      PSLUINT32*      pdwObjectID         Object ID of requested item
 *      PSLPARAM*       paParamItem         Property context for callbacks
 *      MTPFORMATINFO*  ppfiObject          Format information for object
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextBaseGetItemBase(MTPDBCONTEXT mdbc,
                    PSLUINT32 idxItem, PSLUINT32 dwFormat,
                    PSLUINT32* pdwObjectID, PSLPARAM* paParamItem,
                    PSL_CONST MTPFORMATINFO** ppfiObject)
{
    /*  Clear results for safety
     */

    if (PSLNULL != pdwObjectID)
    {
        *pdwObjectID = MTP_OBJECTHANDLE_UNDEFINED;
    }
    if (PSLNULL != paParamItem)
    {
        *paParamItem = PSLNULL;
    }
    if (PSLNULL != ppfiObject)
    {
        *ppfiObject = PSLNULL;
    }
    return ((PSLNULL != mdbc) && (PSLNULL != pdwObjectID) &&
            (PSLNULL != paParamItem) && (PSLNULL != ppfiObject)) ?
                        PSLERROR_NOT_IMPLEMENTED : PSLERROR_INVALID_PARAMETER;
    idxItem;
    dwFormat;
}


/*  MTPDBContextBaseCloseItemBase
 *
 *  Closes the specified item
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context for this call
 *      PSLPARAM        aParamItem          Item param to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextBaseCloseItemBase(MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem)
{
    return (PSLNULL != mdbc) ? PSLERROR_NOT_IMPLEMENTED :
                            PSLERROR_INVALID_PARAMETER;
    aParamItem;
}


/*  MTPDBContextBaseCommitItemBase
 *
 *  Commits the specified item
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DB Context for this call
 *      PSLPARAM        aParamItem          Item param to commit
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextBaseCommitItemBase(MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem)
{
    return (PSLNULL != mdbc) ? PSLSUCCESS_FALSE : PSLERROR_INVALID_PARAMETER;
    aParamItem;
}


/*  MTPDBContextBaseIsPropSupportedBase
 *
 *  Reports whether or not the requested property in supported within the
 *  property context current.
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                DBContext for call
 *      PSLPARAM        aParamItem          Runtime property context
 *      PSLUINT16       wPropCode           Property code
 *      PSLPARAM        aContextProp        Static property context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS if supported, PSLSUCCESS_FALSE if not
 */

PSLSTATUS PSL_API MTPDBContextBaseIsPropSupportedBase(MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem, PSLUINT16 wPropCode,
                    PSLPARAM aContextProp)
{
    return (PSLNULL != mdbc) ? PSLSUCCESS_FALSE : PSLERROR_INVALID_PARAMETER;
    aParamItem;
    wPropCode;
    aContextProp;
}


/*  MTPDBContextDataCancelBase
 *
 *  Cancels the current context data call.  This method always reports
 *  success.
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Context data to cancel
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataCancelBase(MTPDBCONTEXTDATA mdbcd)
{
    /*  Validate arguments
     */

    return (PSLNULL != mdbcd) ?
                            PSLSUCCESS : PSLERROR_INVALID_PARAMETER;
}


/*  MTPDBContextDataSerializeBase
 *
 *  Serializes data from the context.  Always reports not implemented.
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Content data to serialize
 *      PSLVOID*        pvBuf               Data to serialize into
 *      PSLUINT32       cbBuf               Size of the serialization buffer
 *      PSLUINT32*      pcbWritten          Number of bytes written
 *      PSLUINT64*      pcbLeft             Number of bytes left
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataSerializeBase(MTPDBCONTEXTDATA mdbcd,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft)
{
    /*  Clear results for safety
     */

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }
    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /*  Validate arguments
     */

    return ((PSLNULL != mdbcd) &&
            ((PSLNULL == pvBuf) || (0 != cbBuf))) ?
                    PSLERROR_NOT_IMPLEMENTED : PSLERROR_INVALID_PARAMETER;
}


/*  MTPDBContextDataStartDeserializeBase
 *
 *  Called to inform the context data object that deserialization is about
 *  to begin.  Alwyas reports success.
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Context data to deserialize
 *      PSLUINT64       cbData              Size of incoming data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataStartDeserializeBase(
                    MTPDBCONTEXTDATA mdbcd, PSLUINT64 cbData)
{
    /*  Validate arguments
     */

    return (PSLNULL != mdbcd) ?
                            PSLSUCCESS : PSLERROR_INVALID_PARAMETER;
    cbData;
}


/*  MTPDBContextDataDeserializeBase
 *
 *  Called to deserialize the incmoing MTP data buffers.  Always returns
 *  not implemented.
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Context data to deserialized
 *      PSLVOID*        pvBuf               Buffer to deserialize
 *      PSLUINT32       cbBuf               Number of bytes in buffer
 *      PSLMSGQUEUE     mqHandler           Handler message queue for sending
 *                                            messages
 *      MTPCONTEXT*     pmtpctx             MTP Context for deserialization
 *      PSLPARAM        aParam              Parameter data
 *      PSLUINT32       dwMsgFlags          Message flags associated with buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataDeserializeBase(
                    MTPDBCONTEXTDATA mdbcd, PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf, PSLMSGQUEUE mqHandler, MTPCONTEXT* pmtpctx,
                    PSLPARAM aParam, PSLUINT32 dwMsgFlags)
{
    /*  Validate Arguments
     */

    return ((PSLNULL != mdbcd) && (PSLNULL != pvBuf) &&
            (PSLNULL != mqHandler) && (PSLNULL != pmtpctx)) ?
                        PSLERROR_NOT_IMPLEMENTED : PSLERROR_INVALID_PARAMETER;
    cbBuf;
    aParam;
    dwMsgFlags;
}


/*  MTPDBContextDataBaseClose
 *
 *  Closes the provided base context data object.
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Service Info context data to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataBaseClose(MTPDBCONTEXTDATA mdbcd)
{
    PSLSTATUS                   ps;
    MTPDBCONTEXTDATABASEOBJ*    pmdbcdBase;

    /*  Make sure we have work to do
     */

    if (PSLNULL == mdbcd)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Extract our object
     */

    pmdbcdBase = (MTPDBCONTEXTDATABASEOBJ*)mdbcd;
    PSLASSERT(MTPDBCONTEXTDATABASEOBJ_COOKIE  == pmdbcdBase->dwCookie);

    /*  If we have a notification function call it
     */

    if (PSLNULL != pmdbcdBase->pfnOnClose)
    {
        pmdbcdBase->pfnOnClose(pmdbcdBase->aParam, pmdbcdBase->pvData);
    }

    /*  Release the serializer and the object
     */

    SAFE_MTPSERIALIZEDESTROY(pmdbcdBase->msSource);
    SAFE_MTPDBCONTEXTDATAPOOLFREE(pmdbcdBase);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPDBContextDataBaseCancel
 *
 *  Cancels the current context data call.  This method always reports
 *  success.
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd           Context data to cancel
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataBaseCancel(MTPDBCONTEXTDATA mdbcd)
{
    return MTPDBContextDataCancelBase(mdbcd);
}


/*  MTPDBContextDataBaseSerialize
 *
 *  Called to serialize data into the provided buffer
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Object to serialize
 *      PSLVOID*        pvBuf               Destination for serialized data
 *      PSLUINT32       cbBuf               Size of the destination
 *      PSLUINT32*      pcbWritten          Return buffer for number of bytes
 *                                            written
 *      PSLUINT64*      pcbLeft             Return buffer for number of bytes
 *                                            left
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataBaseSerialize(
                    MTPDBCONTEXTDATA mdbcd, PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten, PSLUINT64* pcbLeft)
{
    PSLSTATUS                   ps;
    MTPDBCONTEXTDATABASEOBJ*    pmdbcdBase;

    /*  Clear results for safety
     */

    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /*  Valdiate Context Argument- remaining validation handled by the
     *  serializer
     */

    if (PSLNULL == mdbcd)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmdbcdBase = (MTPDBCONTEXTDATABASEOBJ*)mdbcd;
    PSLASSERT(MTPDBCONTEXTDATABASEOBJ_COOKIE  == pmdbcdBase->dwCookie);

    /*  And call the serializer to do the real work
     */

    ps = MTPSerialize(pmdbcdBase->msSource, pvBuf, cbBuf, pcbWritten,
                            pcbLeft);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPDBContextDataBaseStartDeserialize
 *
 *  Called to inform the context data object that deserialization is about
 *  to begin.  Alwyas reports success.
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Context data to deserialize
 *      PSLUINT64       cbData              Size of incoming data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataBaseStartDeserialize(
                    MTPDBCONTEXTDATA mdbcd, PSLUINT64 cbData)
{
    return MTPDBContextDataStartDeserializeBase(mdbcd, cbData);
}


/*  MTPDBContextDataBaseDeserializeBase
 *
 *  Called to deserialize the incmoing MTP data buffers.  Always returns
 *  not implemented.
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Context data to deserialized
 *      PSLVOID*        pvBuf               Buffer to deserialize
 *      PSLUINT32       cbBuf               Number of bytes in buffer
 *      PSLMSGQUEUE     mqHandler           Handler message queue for sending
 *                                            messages
 *      MTPCONTEXT*     pmtpctx             MTP Context for deserialization
 *      PSLPARAM        aParam              Parameter data
 *      PSLUINT32       dwMsgFlags          Message flags associated with buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataBaseDeserialize(
                    MTPDBCONTEXTDATA mdbcd, PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf, PSLMSGQUEUE mqHandler, MTPCONTEXT* pmtpctx,
                    PSLPARAM aParam, PSLUINT32 dwMsgFlags)
{
    return MTPDBContextDataDeserializeBase(mdbcd, pvBuf, cbBuf, mqHandler,
                            pmtpctx, aParam, dwMsgFlags);
}


/*  MTPDBContextDataBaseCreate
 *
 *  Creates an instance of the base DB Context Data object implementation.
 *  Provide the desired size for the sub-classed object to request additional
 *  storage
 *
 *  Arguments:
 *      PSLUINT32       cbObject            Total size of the object
 *      MTPDBCONTEXTDATA*
 *                      pmdbcd              Return buffer for base class
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDBContextDataBaseCreate(PSLUINT32 cbObject,
                    MTPDBCONTEXTDATA* pmdbcd)
{
    PSLSTATUS                   ps;
    MTPDBCONTEXTDATABASEOBJ*    pmdbcdBase = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmdbcd)
    {
        *pmdbcd = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((sizeof(MTPDBCONTEXTDATABASEOBJ) > cbObject) || (PSLNULL == pmdbcd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Acquire a new context object to use
     */

    ps = MTPDBContextDataPoolAlloc(cbObject, &pmdbcdBase);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize the object
     */

#ifdef PSL_ASSERTS
    pmdbcdBase->dwCookie = MTPDBCONTEXTDATABASEOBJ_COOKIE;
#endif  /* PSL_ASSERTS */
    pmdbcdBase->mdbcdObj.vtbl = &_VtblContextDataBase;

    /*  Return the base object
     */

    *pmdbcd = pmdbcdBase;
    pmdbcdBase = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(pmdbcdBase);
    return ps;
}


/*  MTPStorageInfoDBContextDataCreate
 *
 *  Creates an instance of an MTP Storage Info DBContextData
 *
 *  Arguments:
 *      MTPSTORAGEINFO* pStorageInfo        Storage info to serialize
 *      PSLUINT32       dwFormat            Requested data format
 *      PSLUINT64       qwFreeSpace         Current free space on the storage
 *      PSLUINT32       dwFreeObjects       Current free objects on the storage
 *      MTPDBCONTEXTDATA*
 *                      pmdbcd              Resulting DB Context data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPStorageInfoDBContextDataCreate(
                    PSL_CONST MTPSTORAGEINFO* pStorageInfo, PSLUINT32 dwFormat,
                    PSLUINT64 qwFreeSpace, PSLUINT32 dwFreeObjects,
                    MTPDBCONTEXTDATA* pmdbcd)
{
    PSLSTATUS               ps;
    MTPSTORAGEINFOCTXOBJ*   psicObj = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmdbcd)
    {
        *pmdbcd = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pStorageInfo) ||
        (MTPDBCONTEXTDATADESC_FORMAT(dwFormat) !=
                MTPDBCONTEXTDATAFORMAT_STORAGE) ||
        (PSLNULL == pmdbcd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  We only support querying this information
     */

    if (MTPDBCONTEXTDATA_QUERY != MTPDBCONTEXTDATADESC_FLAGS(dwFormat))
    {
        ps = PSLERROR_ACCESS_DENIED;
        goto Exit;
    }

    /*  Allocate a base context data object to do the work in
     */

    ps = MTPDBContextDataBaseCreate(sizeof(MTPSTORAGEINFOCTXOBJ),
                            &psicObj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize our state
     */

#ifdef PSL_ASSERTS
    psicObj->dwCookie = MTPSTORAGEINFOCTXOBJ_COOKIE;
#endif  /*  PSL_ASSERTS */
    psicObj->pStorageInfo = pStorageInfo;
    psicObj->qwFreeSpace = qwFreeSpace;
    psicObj->dwFreeObjects = dwFreeObjects;

    /*  And create the serializer
     */

    psicObj->mtpCDB.pvData = psicObj;
    ps = MTPSerializeCreate(_RgmsiStorageInfo, PSLARRAYSIZE(_RgmsiStorageInfo),
                            (PSLPARAM)psicObj, psicObj->mtpCDB.pvData, PSLNULL,
                            &(psicObj->mtpCDB.msSource));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the context data object
     */

    *pmdbcd = psicObj;
    psicObj = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(psicObj);
    return ps;
}


/*  _MTPObjectCapsCtxSerializeValue
 *
 *  Returns the requested value in a serialized form
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context for this call
 *      PSLPARAM        aValueContext       Context for this value
 *      PSLUINT16       wType               Type to serialize
 *      PSLVOID*        pvBuf               Destination buffer
 *      PSLUINT32       cbBuf               Size of destination buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPObjectCapsCtxSerializeValue(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT16 wType,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLSTATUS               ps;
    MTPOBJECTCAPSCTXOBJ*    pocObj;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pocObj = (MTPOBJECTCAPSCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTCAPSCTXOBJ_COOKIE == pocObj->dwCookie);

    /*  Determine what count is being returned
     */

    switch (aValueContext)
    {
    case MTPOBJECTCAPS_OBJECT_PROPDESC:
        /*  Validate type
         */

        if (MTP_DATATYPE_UNDEFINED != wType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Call the generic helper to do the real work
         */

        ps = MTPPropertyDescSerialize(
                            (PSL_CONST MTPPROPERTYINFO*)pocObj->popiCur,
                            (pocObj->popiCur->infoProp.precProp->wFlags &
                                pocObj->wPropFlags),
                            pocObj->mtpCDB.aParam, pvBuf, cbBuf, pcbUsed);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPOBJECTCAPS_FORMAT_COUNT:
        /*  Use the format requested to determine the number of formats that
         *  will be returned
         */

        if (MTP_DATATYPE_UINT32 != wType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Return the size of requested
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = sizeof(PSLUINT32);
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure there is space to store the value
         */

        if (sizeof(PSLUINT32) > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  And serialize the value- it will either be 1
         *  (if one format requested) or the number of formats supported in
         *  ServiceInfo
         */

        MTPStoreUInt32((PXPSLUINT32)pvBuf,
                            (MTP_FORMATCODE_NOTUSED == pocObj->wFormat) ?
                                    (pocObj->cFormats + pocObj->cMethods) : 1);
        *pcbUsed = sizeof(PSLUINT32);
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected value requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPObjectCapsCtxGetFormatInfo
 *
 *  Returns the format info requested from the object caps context
 *
 *  Arguments:
 *      MTPOBJECTCAPSCTXOBJ*
 *                          pocObj          Context to pull data from
 *      PSLUINT32           idxFormat       Format index requested
 *      MTPFORMATINFO*      ppfiResult      Resulting format info
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS _MTPObjectCapsCtxGetFormatInfo(PSL_CONST MTPOBJECTCAPSCTXOBJ* pocObj,
                    PSLUINT32 idxFormat, PSL_CONST MTPFORMATINFO** ppfiResult)
{
    PSLSTATUS       ps;

    /*  Clear results for safety
     */

    if (PSLNULL != ppfiResult)
    {
        *ppfiResult = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pocObj) || (PSLNULL == ppfiResult))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine which list we need to pull the format from based on the index
     */

    if (idxFormat < pocObj->cFormats)
    {
        /*  Retrieve the info from the list of object formats
         */

        ps = MTPFormatInfoFromList(pocObj->rgvFormatList, pocObj->cFormats,
                            idxFormat, (pocObj->wFlags &
                                    ~FORMATFLAGS_TYPE_METHODOBJECT),
                            ppfiResult);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }
    else
    {
        /*  We only support methods for service serializations
         */

        PSLASSERT((0 == pocObj->cFormats) ||
                (FORMATFLAGS_TYPE_METHODOBJECT & pocObj->wFlags));

        /*  Retrieve the info from the list of method formats- we will correctly
         *  get "out of range" if the list is empty
         */

        ps = MTPFormatInfoFromList(pocObj->rgvMethodList, pocObj->cMethods,
                            (idxFormat - pocObj->cFormats),
                            FORMATFLAGS_TYPE_METHODOBJECT, ppfiResult);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

Exit:
    return ps;
}


/*  _MTPObjectCapsCtxGetCount
 *
 *  Returns the count of items to return or the number of times to repeat
 *  the specified template
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context to work within
 *      PSLPARAM        aValueContext       Context for the specific value
 *      PSLUINT32*      pcItems             Number of items/repeats to perform
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPObjectCapsCtxGetCount(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT32* pcItems)
{
    PSLSTATUS               ps;
    MTPOBJECTCAPSCTXOBJ*    pocObj;
    MTPFORMATINFO*          pfiCur;

    /*  Clear result for safety
     */

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcItems))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pocObj = (MTPOBJECTCAPSCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTCAPSCTXOBJ_COOKIE == pocObj->dwCookie);

    /*  Determine what count is being returned
     */

    switch (aValueContext)
    {
    case MTPOBJECTCAPS_PROPDESC_LIST:
        /*  Retrieve the current format from the list
         */

        ps = _MTPObjectCapsCtxGetFormatInfo(pocObj, pocObj->idxFormat, &pfiCur);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        /*  Return the number of properties supported by the current format
         */

        *pcItems = pfiCur->cObjectProps;
        ps = PSLSUCCESS;
        break;

    case MTPOBJECTCAPS_FORMAT_LIST:
        /*  Return the number of formats to serialize- if a specific format
         *  has been requested this will be 1 otherwise it will be the number
         *  of formats supported
         */

        *pcItems = (MTP_FORMATCODE_NOTUSED == pocObj->wFormat) ?
                            (pocObj->cFormats + pocObj->cMethods) : 1;
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected property request
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPObjectCapsCtxGetTemplate
 *
 *  Returns the requested serialization template
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context to use in serialization
 *      PSLPARAM        aValueContext       Value context for template
 *      MTPSERIALIZEINFO**
 *                      prgmsiTemplate      Return location for new template
 *      PSLUINT32*      pcItems             Number of items in template
 *      PFNONMTPSERIALIZEDESTROY*
 *                      pfnOnDestroy        Function to call when template
 *                                            or item is to be release
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPObjectCapsCtxGetTemplate(PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSL_CONST MTPSERIALIZEINFO** prgmsiTemplate,
                    PSLUINT32* pcItems,
                    PFNONMTPSERIALIZEDESTROY* ppfnOnDestroy)
{
    PSLSTATUS               ps;
    MTPOBJECTCAPSCTXOBJ*    pocObj;

    /*  Clear result for safety
     */

    if (PSLNULL != prgmsiTemplate)
    {
        *prgmsiTemplate = PSLNULL;
    }
    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }
    if (PSLNULL != ppfnOnDestroy)
    {
        *ppfnOnDestroy = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == prgmsiTemplate) ||
        (PSLNULL == pcItems) || (PSLNULL == ppfnOnDestroy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pocObj = (MTPOBJECTCAPSCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTCAPSCTXOBJ_COOKIE == pocObj->dwCookie);

    /*  Determine what template is being requested
     */

    switch (aValueContext)
    {
    case MTPOBJECTCAPS_PROPDESC_LIST:
        *prgmsiTemplate = _RgmsiObjectCapsPropDesc;
        *pcItems = PSLARRAYSIZE(_RgmsiObjectCapsPropDesc);
        ps = PSLSUCCESS;
        break;

    case MTPOBJECTCAPS_FORMAT_LIST:
        *prgmsiTemplate = _RgmsiObjectCapsFormatArray;
        *pcItems = PSLARRAYSIZE(_RgmsiObjectCapsFormatArray);
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected template requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPObjectCapsCtxGetContext
 *
 *  Returns contextual information for the requested item in a child
 *  serialization template
 *
 *  Arguments:
 *      PSLUINT32       idxItem             Item being requested
 *      PSLPARAM        aContext            Context for the request
 *      PSLPARAM        aValueContext       Context for the value requested
 *      PSLPARAM*       paItemContext       Context to use for this template
 *      PSLVOID**       ppvItemOffset       Root offset for this template
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPObjectCapsCtxGetContext(PSLUINT32 idxItem,
                    PSLPARAM aContext, PSLPARAM aValueContext,
                    PSLPARAM* paItemContext, PSL_CONST PSLVOID** ppvItemOffset)
{
    PSLUINT32                   idxFormat;
    PSLSTATUS                   ps;
    MTPOBJECTCAPSCTXOBJ*        pocObj;
    PSL_CONST MTPFORMATINFO*    pfiCur;

    /*  Clear result for safety
     */

    if (PSLNULL != paItemContext)
    {
        *paItemContext = 0;
    }
    if (PSLNULL != ppvItemOffset)
    {
        *ppvItemOffset = PSLNULL;
    }

    /*  Validate Arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == paItemContext) ||
        (PSLNULL == ppvItemOffset))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pocObj = (MTPOBJECTCAPSCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTCAPSCTXOBJ_COOKIE == pocObj->dwCookie);

    /*  Determine what template context is being requested
     */

    switch (aValueContext)
    {
    case MTPOBJECTCAPS_PROPDESC_LIST:
        /*  Make sure we have not exceeded the property count
         */

        ps = _MTPObjectCapsCtxGetFormatInfo(pocObj, pocObj->idxFormat, &pfiCur);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        if (pfiCur->cObjectProps <= idxItem)
        {
            ps = PSLERROR_INVALID_RANGE;
            goto Exit;
        }

        /*  Make sure that this is a valid format for the type requested
         */

        PSLASSERT(PSLNULL != pfiCur->precFormat);
        if (!(pocObj->wFlags & pfiCur->precFormat->wFlags))
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Determine whether or not we can use this item directly or if we
         *  need to pick up default information to use it
         */

        if (PSLNULL != pfiCur->rgopiObjectProps[idxItem].infoProp.precProp)
        {
            /*  Information is complete- just use it directly
             */

            pocObj->popiCur = &(pfiCur->rgopiObjectProps[idxItem]);
        }
        else
        {
            /*  Information is incomplete- copy it to the cache so we can update
             *  it
             */

            PSLCopyMemory(&(pocObj->opiCache),
                            &(pfiCur->rgopiObjectProps[idxItem]),
                            sizeof(pocObj->opiCache));

            /*  And retrieve the default property entry
             */

            ps = MTPPropertyGetDefaultRec(pocObj->opiCache.infoProp.wPropCode,
                            PROPFLAGS_TYPE_SERVICEOBJECT,
                            (PSL_CONST MTPPROPERTYREC**)
                                    &(pocObj->opiCache.infoProp.precProp));
            if (PSLSUCCESS != ps)
            {
                PSLASSERT(PSL_FAILED(ps));
                ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_DATA;
                goto Exit;
            }

            /*  Use the cache version of the property information
             */

            pocObj->popiCur = &(pocObj->opiCache);
        }

        /*  Validate that object property type matches the current format type
         */

        switch (pfiCur->precFormat->wFlags & pocObj->wFlags)
        {
        case FORMATFLAGS_TYPE_OBJECT:
            if (!(PROPFLAGS_TYPE_OBJECT &
                                pocObj->popiCur->infoProp.precProp->wFlags))
            {
                ps = PSLERROR_INVALID_DATA;
                goto Exit;
            }
            break;

        case FORMATFLAGS_TYPE_SERVICEOBJECT:
            if (!(PROPFLAGS_TYPE_SERVICEOBJECT &
                                pocObj->popiCur->infoProp.precProp->wFlags))
            {
                ps = PSLERROR_INVALID_DATA;
                goto Exit;
            }
            break;

        case FORMATFLAGS_TYPE_METHODOBJECT:
            if (!(PROPFLAGS_TYPE_METHODOBJECT &
                                pocObj->popiCur->infoProp.precProp->wFlags))
            {
                ps = PSLERROR_INVALID_DATA;
                goto Exit;
            }
            break;

        default:
            /*  Unknown format type
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  Return a pointer to the property record
         */

        *paItemContext = aContext;
        *ppvItemOffset = pocObj->popiCur;
        ps = PSLSUCCESS;
        break;

    case MTPOBJECTCAPS_FORMAT_LIST:
        /*  Determine if we are returning all formats or just one
         */

        if (MTP_FORMATCODE_NOTUSED == pocObj->wFormat)
        {
            /*  Retrieve the next format
             */

            ps = _MTPObjectCapsCtxGetFormatInfo(pocObj, idxItem, &pfiCur);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            pocObj->idxFormat = idxItem;
        }
        else
        {
            /*  We only support a single request
             */

            if (0 != idxItem)
            {
                ps = PSLERROR_INVALID_RANGE;
                goto Exit;
            }

            /*  Locate the correct format
             */

            for (idxFormat = 0, pfiCur = pocObj->rgvFormatList;
                    (idxFormat < pocObj->cFormats) &&
                            (pfiCur->wFormatCode != pocObj->wFormat);
                    idxFormat++, pfiCur = MTPFormatInfoNext(pfiCur,
                            (pocObj->wFlags & pfiCur->precFormat->wFlags)))
            {
            }

            /*  If we did not find a match in the format list check the method
             *  list
             */

            if (pocObj->cFormats <= idxFormat)
            {
                /*  We only support methods for service serializations
                 */

                PSLASSERT((0 == pocObj->cFormats) ||
                        (FORMATFLAGS_TYPE_METHODOBJECT & pocObj->wFlags));

                /*  Walk the method list looking for the desired format
                 */

                for (idxFormat = 0, pfiCur = pocObj->rgvMethodList;
                        (idxFormat < pocObj->cMethods) &&
                            (pfiCur->wFormatCode != pocObj->wFormat);
                        idxFormat++, pfiCur = MTPFormatInfoNext(pfiCur,
                            FORMATFLAGS_TYPE_METHODOBJECT))
                {
                }

                /*  If we still did not locate the format then we are done
                 */

                if (pocObj->cMethods <= idxFormat)
                {
                    ps = MTPERROR_DATABASE_INVALID_FORMATCODE;
                    goto Exit;
                }
            }

            /*  Remember the desired format
             */

            pocObj->idxFormat = idxFormat;
        }

        /*  Make sure that the format type is correct
         */

        if ((PSLNULL == pfiCur->precFormat) ||
            !(pocObj->wFlags & pfiCur->precFormat->wFlags))
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  Reset the property pointer and return the format information
         */

        *paItemContext = aContext;
        *ppvItemOffset = pfiCur;
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unexpected template requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPObjectCapsContextDataCreate
 *
 *  Internal helper function that generates a capabilities data set.  The
 *  code has been generalized to handle either "legacy" or service versions.
 *
 *  Arguments:
 *      MTPFORMATINFO*  pFormatInfo         Formats to serialize capabilities
 *      PSLUINT32       cFormats            Number of formats
 *      MTPSERVICEMETHODINFO*
 *                      pMethodInfo         Methods to serialize capabilities
 *      PSLUINT32       cMethods            Number of methods
 *      PSLUINT32       dwFormat            Context Data format
 *      PSLUINT16       wFormat             Format code being requested
 *      PSLPARAM        aParam              Paramater for callback function
 *      PFNONMTPDBCONTEXTDATABASECLOSE
 *                      pfnOnClose          Callback function when context
 *                                            closes
 *      MTPDBCONTEXTDATA*
 *                      pmdbcd              Resulting DB Context data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPObjectCapsContextDataCreate(
                    PSL_CONST MTPFORMATINFO* pFormatInfo, PSLUINT32 cFormats,
                    PSL_CONST MTPSERVICEMETHODINFO* pMethodInfo,
                    PSLUINT32 cMethods, PSLUINT32 dwFormat, PSLUINT16 wFormat,
                    PSLPARAM aParam, PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPDBCONTEXTDATA* pmdbcd)
{
    PSLSTATUS               ps;
    MTPOBJECTCAPSCTXOBJ*    pocObj = PSLNULL;
    PSLUINT16               wFormatFlags;
    PSLUINT16               wPropFlags;

    /*  Clear result for safety
     */

    if (PSLNULL != pmdbcd)
    {
        *pmdbcd = PSLNULL;
    }

    /*  Validate arguments
     */

    if (((0 < cFormats) && (PSLNULL == pFormatInfo)) ||
        ((0 < cMethods) && (PSLNULL == pMethodInfo)) ||
        (PSLNULL == pmdbcd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine the correct property flag to use based on the format type
     */

    switch (MTPDBCONTEXTDATADESC_FORMAT(dwFormat))
    {
    case MTPDBCONTEXTDATAFORMAT_FORMATCAPABILITIES:
        /*  Make sure we were not handed any methods data
         */

        if ((0 != cMethods) || (PSLNULL != pMethodInfo))
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        /*  Make sure we validate for non-service aware data
         */

        wFormatFlags = FORMATFLAGS_TYPE_OBJECT;
        wPropFlags = PROPFLAGS_TYPE_OBJECT;
        break;

    case MTPDBCONTEXTDATAFORMAT_SERVICECAPABILITIES:
        /*  Enable service aware validation
         */

        wFormatFlags = FORMATFLAGS_TYPE_SERVICEOBJECT |
                            FORMATFLAGS_TYPE_METHODOBJECT;
        wPropFlags = PROPFLAGS_TYPE_SERVICEOBJECT | PROPFLAGS_TYPE_METHODOBJECT;
        break;

    default:
        /*  Unknown type
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  We only support querying this information
     */

    if (MTPDBCONTEXTDATA_QUERY != MTPDBCONTEXTDATADESC_FLAGS(dwFormat))
    {
        ps = PSLERROR_ACCESS_DENIED;
        goto Exit;
    }

    /*  Allocate a base context data object to do the work in
     */

    ps = MTPDBContextDataBaseCreate(sizeof(MTPOBJECTCAPSCTXOBJ), &pocObj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize our state
     */

#ifdef PSL_ASSERTS
    pocObj->dwCookie = MTPOBJECTCAPSCTXOBJ_COOKIE;
#endif  /*  PSL_ASSERTS */
    pocObj->rgvFormatList = pFormatInfo;
    pocObj->cFormats = cFormats;
    pocObj->rgvMethodList = pMethodInfo;
    pocObj->cMethods = cMethods;
    pocObj->wFormat = wFormat;
    pocObj->wFlags = wFormatFlags;
    pocObj->wPropFlags = wPropFlags;
    pocObj->mtpCDB.aParam = aParam;
    pocObj->mtpCDB.pfnOnClose = pfnOnClose;

    /*  And create the serializer
     */

    pocObj->mtpCDB.pvData = pocObj;
    ps = MTPSerializeCreate(_RgmsiObjectCaps, PSLARRAYSIZE(_RgmsiObjectCaps),
                            (PSLPARAM)pocObj, pocObj->mtpCDB.pvData, PSLNULL,
                            &(pocObj->mtpCDB.msSource));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the context data object
     */

    *pmdbcd = pocObj;
    pocObj = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(pocObj);
    return ps;
}


/*  MTPFormatCapabilitiesContextDataCreate
 *
 *  Creates an instance of an MTP Format Capabilites DBContextData
 *
 *  Arguments:
 *      MTPFORMATINFO*  pFormatInfo         Formats to serialize capabilities
 *      PSLUINT32       cFormats            Number of formats
 *      PSLUINT32       dwFormat            Context Data format
 *      PSLUINT16       wFormat             Format code being requested
 *      PSLPARAM        aParam              Paramater for callback function
 *      PFNONMTPDBCONTEXTDATABASECLOSE
 *                      pfnOnClose          Callback function when context
 *                                            closes
 *      MTPDBCONTEXTDATA*
 *                      pmdbcd              Resulting DB Context data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPFormatCapabilitiesContextDataCreate(
                    PSL_CONST MTPFORMATINFO* pFormatInfo, PSLUINT32 cFormats,
                    PSLUINT32 dwFormat, PSLUINT16 wFormat, PSLPARAM aParam,
                    PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPDBCONTEXTDATA* pmdbcd)
{
    return MTPObjectCapsContextDataCreate(pFormatInfo, cFormats, PSLNULL, 0,
                            dwFormat, wFormat, aParam, pfnOnClose, pmdbcd);
}


/*  _MTPObjectHandlesCtxGetCount
 *
 *  Returns the count of object handles in the current context.
 *
 *  Arguments:
 *      PSLPARAM        aContext            Serialization context
 *      PSLPARAM        aValueContext       Value context
 *      PSLUINT32       pcItems             Item count
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPObjectHandlesCtxGetCount(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT32* pcItems)
{
    PSLSTATUS               ps;
    MTPOBJECTHANDLESCTXOBJ* pohcObj;

    /*  Clear result for safety
     */

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContext) || (0 != aValueContext) || (PSLNULL == pcItems))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pohcObj = (MTPOBJECTHANDLESCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTHANDLESCTXOBJ_COOKIE == pohcObj->dwCookie);

    /*  And use the MTPDBContext to retrieve the count
     */

    ps = MTPDBContextGetCount(pohcObj->mdbc, pcItems);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPObjectHandlesCtxGetItem
 *
 *  Returns the requested object handle.
 *
 *  Arguments:
 *      PSLUINT32       idxItem             Item requested
 *      PSLPARAM        aContext            Serialization context
 *      PSLPARAM        aValueContext       Value context
 *      PSLUINT16       wType               Type of item being requested
 *      PSLVOID*        pvBuf               Return buffer for item
 *      PSLUINT32       cbBuf               Size of the return buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPObjectHandlesCtxGetItem(PSLUINT32 idxItem,
                    PSLPARAM aContext, PSLPARAM aValueContext, PSLUINT16 wType,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLUINT32               dwObjectID;
    PSLSTATUS               ps;
    MTPOBJECTHANDLESCTXOBJ* pohcObj;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContext) || (0 != aValueContext) ||
        (MTP_DATATYPE_UINT32 != wType) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pohcObj = (MTPOBJECTHANDLESCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTHANDLESCTXOBJ_COOKIE == pohcObj->dwCookie);

    /*  If we are just being asked for size return it
     */

    if (PSLNULL == pvBuf)
    {
        *pcbUsed = sizeof(PSLUINT32);
        ps = PSLSUCCESS;
        goto Exit;
    }

    /*  Make sure there is enough space to store the result
     */

    if (sizeof(PSLUINT32) > cbBuf)
    {
        ps = PSLERROR_INSUFFICIENT_BUFFER;
        goto Exit;
    }

    /*  Retrieve the actual value- we have to do this in two steps because
     *  the destination buffer may not be property aligned
     */

    ps = MTPDBContextGetHandle(pohcObj->mdbc, PSLFALSE, idxItem, &dwObjectID,
                            PSLNULL, PSLNULL);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  And store it in the serialization buffer
     */

    MTPStoreUInt32((PXPSLUINT32)pvBuf, dwObjectID);
    *pcbUsed = sizeof(PSLUINT32);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  MTPObjectHandlesContextDataCreate
 *
 *  Creates an instance of an MTP Object Handles DBContextData
 *
 *  Arguments:
 *      MTPDBCONTEXT    mdbc                MTP DB Context to serialize
 *      PSLUINT32       dwFormat            Requested data format
 *      MTPDBCONTEXTDATA*
 *                      pmdbcd              Resulting DB Context data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPObjectHandlesContextDataCreate(MTPDBCONTEXT mdbc,
                    PSLUINT32 dwFormat, MTPDBCONTEXTDATA* pmdbcd)
{
    PSLSTATUS               ps;
    MTPOBJECTHANDLESCTXOBJ* pohcObj = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmdbcd)
    {
        *pmdbcd = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mdbc) ||
        (MTPDBCONTEXTDATADESC_FORMAT(dwFormat) !=
                MTPDBCONTEXTDATAFORMAT_OBJECTHANDLE) ||
        (PSLNULL == pmdbcd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  We only support querying this information
     */

    if (MTPDBCONTEXTDATA_QUERY != MTPDBCONTEXTDATADESC_FLAGS(dwFormat))
    {
        ps = PSLERROR_ACCESS_DENIED;
        goto Exit;
    }

    /*  Allocate a base context data object to do the work in
     */

    ps = MTPDBContextDataBaseCreate(sizeof(MTPOBJECTHANDLESCTXOBJ),
                            &pohcObj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize our state
     */

#ifdef PSL_ASSERTS
    pohcObj->dwCookie = MTPOBJECTHANDLESCTXOBJ_COOKIE;
#endif  /*  PSL_ASSERTS */
    pohcObj->mdbc = mdbc;

    /*  And create the serializer
     */

    ps = MTPSerializeCreate(_RgmsiObjectHandles,
                            PSLARRAYSIZE(_RgmsiObjectHandles),
                            (PSLPARAM)pohcObj, PSLNULL, PSLNULL,
                            &(pohcObj->mtpCDB.msSource));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the context data object
     */

    *pmdbcd = pohcObj;
    pohcObj = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(pohcObj);
    return ps;
}


/*  _MTPObjectPropListCtxGetValue
 *
 *  Retrieves the requested value for serialization
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context for this call
 *      PSLPARAM        aValueContext       Context for this value
 *      PSLUINT32       wType               Type of data being requested
 *      PSLVOID*        pvBuf               Return buffer for requested data
 *      PSLUINT32       cbBuf               Size of return buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used in buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

 PSLSTATUS PSL_API _MTPObjectPropListCtxGetValue(PSLPARAM aContext,
                    PSLPARAM aValueContext,  PSLUINT16 wType,  PSLVOID* pvBuf,
                    PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLSTATUS                       ps;
    MTPOBJECTPROPLISTCTXOBJ*        popcObj;
    PFNMTPPROPGETVALUE              pfnGetProp;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    popcObj = (MTPOBJECTPROPLISTCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTPROPLISTCTXOBJ_COOKIE == popcObj->dwCookie);

    /*  And figure out what property is being requested
     */

    switch (aValueContext)
    {
    case MTPOBJECTPROPLIST_PROPERTYLIST_COUNT:
        /*  We should only be asked for this value when we are not counting
         */

        PSLASSERT(!popcObj->fCountTotal);
        if (popcObj->fCountTotal)
        {
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }

        /*  FALL THROUGH INTENTIONAL */

    case MTPOBJECTPROPLIST_PROPERTY_HANDLE:
        /*  Validate type
         */

        if (MTP_DATATYPE_UINT32 != wType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  If only size is requested return it
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = sizeof(PSLUINT32);
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure there is enough space to hold the property
         */

        if (sizeof(PSLUINT32) > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  Store the correct value
         */

        switch (aValueContext)
        {
        case MTPOBJECTPROPLIST_PROPERTYLIST_COUNT:
            MTPStoreUInt32((PXPSLUINT32)pvBuf, popcObj->cPropsTotal);
            break;

        case MTPOBJECTPROPLIST_PROPERTY_HANDLE:
            MTPStoreUInt32((PXPSLUINT32)pvBuf, popcObj->dwObjectID);
            break;

        default:
            /*  Should never get here
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_UNEXPECTED;
            goto Exit;
        }
        *pcbUsed = sizeof(PSLUINT32);
        ps = PSLSUCCESS;
        break;

    case MTPOBJECTPROPLIST_PROPERTY_TYPE:
        /*  Validate type
         */

        if (MTP_DATATYPE_UINT16 != wType)
        {
            ps = PSLERROR_INVALID_DATA;
            goto Exit;
        }

        /*  If only size is requested return it
         */

        if (PSLNULL == pvBuf)
        {
            *pcbUsed = sizeof(PSLUINT16);
            ps = PSLSUCCESS;
            break;
        }

        /*  Make sure there is enough space to hold the property
         */

        if (sizeof(PSLUINT16) > cbBuf)
        {
            ps = PSLERROR_INSUFFICIENT_BUFFER;
            goto Exit;
        }

        /*  And store it
         */

        MTPStoreUInt16((PXPSLUINT16)pvBuf, popcObj->precProp->wDataType);
        *pcbUsed = sizeof(PSLUINT16);
        ps = PSLSUCCESS;
        break;

    case MTPOBJECTPROPLIST_PROPERTY_VALUE:
        /*  Extract the property function
         */

        pfnGetProp = popcObj->pobjProp->infoProp.pfnMTPPropGetValue;

        /*  And determine if we need to lookup the default property information
         */

        /*  And determine if we are using it or calling the generic "default"
         *  property function
         */

        if (PSLNULL != pfnGetProp)
        {
            /*  Request the actual property value
             */

            ps = pfnGetProp(popcObj->pobjProp->infoProp.wPropCode,
                            popcObj->precProp->wDataType, popcObj->aParamItem,
                            popcObj->pobjProp->infoProp.aContextProp,
                            pvBuf, cbBuf, pcbUsed);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        else
        {
            /*  Used the default value
             */

            ps = MTPPropertySerializeDefaultValue(popcObj->precProp, pvBuf,
                            cbBuf, pcbUsed);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        break;

    default:
        /*  Unsupported property
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPObjectPropListCtxOnSerializeDestroy
 *
 *  Callback function for use when a template or context is released
 *
 *  Arguments:
 *      MTPSERIALIZEINFO*
 *                      rgmsiTemplate       Serialization template being
 *                                            destroyed
 *      PSLPARAM        aContext            Context being destroyed
 *      PSLVOID*        pvOffset            Offset being destroyed
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPObjectPropListCtxOnSerializeDestroy(
                    PSL_CONST MTPSERIALIZEINFO* rgmsiTemplate,
                    PSLPARAM aContext, PSL_CONST PSLVOID* pvOffset)
{
    PSLSTATUS                   ps;
    MTPOBJECTPROPLISTCTXOBJ*    popcObj;

    /*  Validate arguments
     */

    if (PSLNULL == aContext)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    popcObj = (MTPOBJECTPROPLISTCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTPROPLISTCTXOBJ_COOKIE == popcObj->dwCookie);

    /*  We always clear the current property and property record information
     */

    popcObj->pobjProp = PSLNULL;
    popcObj->precProp = PSLNULL;

    /*  Next we need to check to see if we need to close the current item
     *  context
     */

    if (((PSLNULL != popcObj->pfiCur) &&
                (popcObj->pfiCur->cObjectProps <= popcObj->idxProp)) ||
        (_RgmsiObjectPropListPropertyValue == rgmsiTemplate))
    {
        /*  Close the object
         */

        if (PSLNULL != popcObj->pfiCur)
        {
            ps = MTPDBContextBaseCloseItem(popcObj->mdbc, popcObj->aParamItem);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            popcObj->pfiCur = PSLNULL;
        }

        /*  Handle general cleanup
         */

        popcObj->dwObjectID = MTP_OBJECTHANDLE_UNDEFINED;
        popcObj->aParamItem = 0;
        popcObj->idxProp = 0;

        /*  When we are done with the repeat template make sure that we reset
         *  everything for the next pass
         */

        if (_RgmsiObjectPropListPropertyValue == rgmsiTemplate)
        {
            popcObj->idxItem = 0;
        }
    }

    /*  Report success
     */

    ps = PSLSUCCESS;

Exit:
    return ps;
    pvOffset;
}


/*  _MTPObjectPropListCtxGetTemplate
 *
 *  Returns the requested template to the serializer
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context for this request
 *      PSLPARAM        aValueContext       Context for this serialization item
 *      MTPSERIALIZEINFO**
 *                      prgmsiTemplate      Return buffer for the template
 *      PSLUINT32*      pcItems             Return buffer for the count of items
 *      PFNONMTPSERIALIZEDESTROY*
 *                      ppfnOnDestroy       Callback function when template is
 *                                            released
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPObjectPropListCtxGetTemplate(PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSL_CONST MTPSERIALIZEINFO** prgmsiTemplate,
                    PSLUINT32* pcItems, PFNONMTPSERIALIZEDESTROY* ppfnOnDestroy)
{
    PSLSTATUS                   ps;
    MTPOBJECTPROPLISTCTXOBJ*    popcObj;

    /*  Clear result for safety
     */

    if (PSLNULL != prgmsiTemplate)
    {
        *prgmsiTemplate = 0;
    }
    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }
    if (PSLNULL != ppfnOnDestroy)
    {
        *ppfnOnDestroy = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == prgmsiTemplate) ||
        (PSLNULL == pcItems) || (PSLNULL == ppfnOnDestroy))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    popcObj = (MTPOBJECTPROPLISTCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTPROPLISTCTXOBJ_COOKIE == popcObj->dwCookie);

    /*  And figure out what property is being requested
     */

    switch (aValueContext)
    {
    case MTPOBJECTPROPLIST_PROPERTYLIST_VALUES:
        /*  Return the property list formatting information
         */

        *prgmsiTemplate = _RgmsiObjectPropListPropertyValue;
        *pcItems = PSLARRAYSIZE(_RgmsiObjectPropListPropertyValue);
        *ppfnOnDestroy = _MTPObjectPropListCtxOnSerializeDestroy;
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unknown template requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPObjectPropListGetNextPropContext
 *
 *  Returns the next property context in the list
 *
 *  Arguments:
 *      PSLUINT32       idxItem             Index of item being requested
 *      PSLPARAM        aContext            Context for this call
 *      PSLPARAM        aValueContext       Context for the value requested
 *      PSLPARAM*       paItemContext       Return buffer for item context
 *      PSLVOID**       ppvItemOffset       Return buffer for item offset
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPObjectPropListGetNextPropContext(PSLUINT32 idxItem,
                    PSLPARAM aContext,  PSLPARAM* paItemContext,
                    PSL_CONST PSLVOID** ppvItemOffset)
{
    PSLUINT32                       cItems;
    PSLBOOL                         fFound;
    PSLSTATUS                       ps;
    MTPOBJECTPROPLISTCTXOBJ*        popcObj;
    PSL_CONST MTPOBJECTPROPINFO*    pobjProp;

    /*  Clear result for safety
     */

    if (PSLNULL != paItemContext)
    {
        *paItemContext = PSLNULL;
    }
    if (PSLNULL != ppvItemOffset)
    {
        *ppvItemOffset = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == paItemContext) ||
        (PSLNULL == ppvItemOffset))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    popcObj = (MTPOBJECTPROPLISTCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTPROPLISTCTXOBJ_COOKIE == popcObj->dwCookie);

    /*  Retrieve the total number of items we have to examine
     */

    ps = MTPDBContextGetCount(popcObj->mdbc, &cItems);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Start up the main loop- we will continue here until we find a
     *  valid property to serialize
     */

    do
    {
        /*  Step 1- Retrieve the next object in the context and its format if
         *  required
         */

        if ((PSLNULL == popcObj->pfiCur) ||
               (popcObj->pfiCur->cObjectProps <= popcObj->idxProp))
        {
            /*  Make sure we can retrieve the next item
             */

            if (cItems <= popcObj->idxItem)
            {
                /*  Report not available if we are in counting mode so
                 *  that the iteration stops
                 */

                if (!popcObj->fCountTotal)
                {
                    /*  We are doing the actual serialization- this is an
                     *  error
                     */

                    ps = PSLERROR_INVALID_RANGE;
                }
                else
                {
                    /*  We are counting items to determine length- report that
                     *  we have reached the end.  We still need to return the
                     *  context here because we will get called to destroy
                     *  the template and we need the context
                     */

                    *paItemContext = aContext;
                    ps = PSLSUCCESS_NOT_AVAILABLE;
                }
                goto Exit;
            }

            /*  Ask for the next object and format
             */

            ps = MTPDBContextBaseGetItem(popcObj->mdbc, popcObj->idxItem,
                            MAKE_MTPDBCONTEXTDATADESC(
                                    MTPDBCONTEXTDATA_QUERY,
                                    MTPDBCONTEXTDATAFORMAT_OBJECTPROPLIST),
                            &(popcObj->dwObjectID), &(popcObj->aParamItem),
                            &(popcObj->pfiCur));
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            popcObj->idxItem++;

            /*  Reset our item dependent counters
             */

            popcObj->idxProp = 0;
            popcObj->pobjProp = PSLNULL;
            popcObj->precProp = PSLNULL;
        }

        /*  Loop through each of the the properties on the object picking up
         *  where we left off last time
         */

        for (fFound = PSLFALSE;
                !fFound && (popcObj->idxProp < popcObj->pfiCur->cObjectProps);
                popcObj->idxProp++)
        {
            /*  Save some redirection
             */

            pobjProp = &(popcObj->pfiCur->rgopiObjectProps[popcObj->idxProp]);

            /*  Step 2- Check the object property to see if it matches the
             *  current restriction
             */

            switch (popcObj->dwPropCtx)
            {
            case MTP_OBJECTPROPCODE_ALL:
                /*  We are returning all properties except the slow properties-
                 *  check the group code and exclude them
                 */

                if (MTP_GROUPCODE_SLOW == pobjProp->dwGroupCode)
                {
                    continue;
                }
                break;

            case MTP_OBJECTPROPCODE_NOTUSED:
                /*  We are specifying properties by group code- check for
                 *  a match
                 */

                if (pobjProp->dwGroupCode != popcObj->dwGroupCodeCtx)
                {
                    continue;
                }
                break;

            default:
                /*  We are only looking for a specific property
                 */

                if (pobjProp->infoProp.wPropCode != popcObj->dwPropCtx)
                {
                    continue;
                }
                break;
            }

            /*  Step 3- Check to see if the requested property is actually
             *  supported on the object
             */

            ps = MTPDBContextBaseIsPropSupported(popcObj->mdbc,
                            popcObj->aParamItem, pobjProp->infoProp.wPropCode,
                            pobjProp->infoProp.aContextProp);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            fFound = PSLSUCCESS == ps;
            if (fFound)
            {
                break;
            }
        }

        /*  If we have checked all the properties without finding a match
         *  try with the next object
         */

        PSLASSERT(fFound || (popcObj->pfiCur->cObjectProps <= popcObj->idxProp));
    } while (!fFound);

    /*  We have found a valid property on an object- make sure we have a
     *  complete property description so that we do not have to look it up
     */

    popcObj->pobjProp = &(popcObj->pfiCur->rgopiObjectProps[popcObj->idxProp]);
    if (PSLNULL != popcObj->pobjProp->infoProp.precProp)
    {
        /*  Property description provided
         */

        popcObj->precProp = popcObj->pobjProp->infoProp.precProp;
    }
    else
    {
        /*  Request the default description
         */

        ps = MTPPropertyGetDefaultRec(popcObj->pobjProp->infoProp.wPropCode,
                            PROPFLAGS_TYPE_OBJECT, &(popcObj->precProp));
        if (PSLSUCCESS != ps)
        {
            PSLASSERT(PSL_FAILED(ps));
            ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_DATA;
            goto Exit;
        }
    }

    /*  Return the context and offset information- the context is the same
     *  and the offset is the object info dataset
     */

    *paItemContext = aContext;
    *ppvItemOffset = popcObj->pobjProp;

    /*  Increment the property count so that we start searching again with
     *  the next property in the list
     */

    popcObj->idxProp++;

    /*  If we are counting items increment the count
     */

    if (popcObj->fCountTotal)
    {
        popcObj->cPropsTotal = idxItem + 1;
    }
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  _MTPObjectPropListCtxGetContext
 *
 *  Returns the next context information for the repeat of the template.
 *
 *  Arguments:
 *      PSLUINT32       idxItem             Index of item being requested
 *      PSLPARAM        aContext            Context for this call
 *      PSLPARAM        aValueContext       Context for the value requested
 *      PSLPARAM*       paItemContext       Return buffer for item context
 *      PSLVOID**       ppvItemOffset       Return buffer for item offset
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPObjectPropListCtxGetContext(PSLUINT32 idxItem,
                    PSLPARAM aContext,  PSLPARAM aValueContext,
                    PSLPARAM* paItemContext,  PSL_CONST PSLVOID** ppvItemOffset)
{
    PSLSTATUS                       ps;
    MTPOBJECTPROPLISTCTXOBJ*        popcObj;

    /*  Clear result for safety
     */

    if (PSLNULL != paItemContext)
    {
        *paItemContext = PSLNULL;
    }
     if (PSLNULL != ppvItemOffset)
    {
        *ppvItemOffset = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == paItemContext) ||
        (PSLNULL == ppvItemOffset))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    popcObj = (MTPOBJECTPROPLISTCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTPROPLISTCTXOBJ_COOKIE == popcObj->dwCookie);

    /*  Determine which context is being requested
     */

    switch (aValueContext)
    {
    case MTPOBJECTPROPLIST_PROPERTYLIST_VALUES:
        /*  Call the helper funtcion to do the real work
         */

        ps = _MTPObjectPropListGetNextPropContext(idxItem, aContext,
                            paItemContext, ppvItemOffset);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    default:
        /*  Unexpected context type requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPObjectPropListCtxGetCount
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context to operate within
 *      PSLPARAM        aValueContext       Value context being requested
 *      PSLUINT32*      pcItems             Return buffer for count of items
 *
 *  Returns;
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPObjectPropListCtxGetCount(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT32* pcItems)
{
    PSLSTATUS                       ps;
    MTPOBJECTPROPLISTCTXOBJ*        popcObj;

    /*  Clear result for safety
     */

    if (PSLNULL != pcItems)
    {
        *pcItems = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == aContext) || (PSLNULL == pcItems))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    popcObj = (MTPOBJECTPROPLISTCTXOBJ*)aContext;
    PSLASSERT(MTPOBJECTPROPLISTCTXOBJ_COOKIE == popcObj->dwCookie);

    /*  Determine which value is being returned
     */

    switch (aValueContext)
    {
    case MTPOBJECTPROPLIST_PROPERTYLIST_VALUES:
        /*  If we are counting items (meaning we are just determining the
         *  length) then we want to report infinite serialization.  If we
         *  are actually serializing then we want to report the real value.
         */

        *pcItems = popcObj->fCountTotal ?
                            MTPSERIALIZE_INFINITE_REPEAT : popcObj->cPropsTotal;
        ps = PSLSUCCESS;
        break;

    default:
        /*  Unknown property requested
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPObjectPropListCtxSerialize
 *
 *  Called to serialize data into the provided buffer
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Object to serialize
 *      PSLVOID*        pvBuf               Destination for serialized data
 *      PSLUINT32       cbBuf               Size of the destination
 *      PSLUINT32*      pcbWritten          Return buffer for number of bytes
 *                                            written
 *      PSLUINT64*      pcbLeft             Return buffer for number of bytes
 *                                            left
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

 PSLSTATUS PSL_API _MTPObjectPropListCtxSerialize(MTPDBCONTEXTDATA mdbcd,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft)
{
    PSLSTATUS                   ps;
    MTPOBJECTPROPLISTCTXOBJ*    popcObj;

    /*  Clear result for safety
     */

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }
    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /*  Validate arguments
     */

    if (PSLNULL == mdbcd)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    popcObj = (MTPOBJECTPROPLISTCTXOBJ*)mdbcd;
    PSLASSERT(MTPOBJECTPROPLISTCTXOBJ_COOKIE == popcObj->dwCookie);

    /*  Set the count flag based on whether or not we are actually writing
     *  data
     */

    popcObj->fCountTotal = (PSLNULL == pvBuf);

    /*  And do the real work
     */

    ps = MTPDBContextDataBaseSerialize(mdbcd, pvBuf, cbBuf, pcbWritten,
                            pcbLeft);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPObjectHandlesContextDataCreate
 *
 *  Creates an instance of an MTP Object Prop List DBContextData
 *
 *  Arguments:
 *      MTPDBCONTEXTBASE
 *                      mdbc                MTP DB Context Base to serialize
 *      PSLUINT32       dwFormat            Requested data format
 *      MTPDBVARIANTPROP*
 *                      rgmdbv              Properties associated with this
 *      PSLUINT32       cmdv                Count of properties
 *      MTPDBCONTEXTDATA*
 *                      pmdbcd              Resulting DB Context data
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPObjectPropListContextDataCreate(MTPDBCONTEXTBASE mdbc,
                    PSLUINT32 dwFormat, PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv, MTPDBCONTEXTDATA* pmdbcd)
{
    PSLUINT32                   idxProp;
    PSLSTATUS                   ps;
    MTPOBJECTPROPLISTCTXOBJ*    popcObj = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmdbcd)
    {
        *pmdbcd = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mdbc) ||
        (MTPDBCONTEXTDATADESC_FORMAT(dwFormat) !=
                MTPDBCONTEXTDATAFORMAT_OBJECTPROPLIST) ||
        (PSLNULL == rgmdbv) || (2 != cmdbv) || (PSLNULL == pmdbcd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  We only support querying this information
     */

    if (MTPDBCONTEXTDATA_QUERY != MTPDBCONTEXTDATADESC_FLAGS(dwFormat))
    {
        ps = PSLERROR_ACCESS_DENIED;
        goto Exit;
    }

    /*  Allocate a base context data object to do the work in
     */

    ps = MTPDBContextDataBaseCreate(sizeof(MTPOBJECTPROPLISTCTXOBJ), &popcObj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize our state
     */

#ifdef PSL_ASSERTS
    popcObj->dwCookie = MTPOBJECTPROPLISTCTXOBJ_COOKIE;
#endif  /*  PSL_ASSERTS */
    popcObj->mtpCDB.mdbcdObj.vtbl = &_VtblContextDataObjectPropList;
    popcObj->mdbc = mdbc;

    /*  Walk through and unpack the interesting arguments
     */

    for (idxProp = 0; idxProp < cmdbv; idxProp++)
    {
        /*  Examine the properties
         */

        switch (rgmdbv[idxProp].wPropCode)
        {
        case MTP_OBJECTPROPCODE_PROPCODE:
            /*  Validate type
             */

            if (MTP_DATATYPE_UINT32 != rgmdbv[idxProp].wDataType)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  And stash it
             */

            popcObj->dwPropCtx = rgmdbv[idxProp].vParam.valUInt32;
            break;

        case MTP_OBJECTPROPCODE_GROUPCODE:
            /*  Validate type
             */

            if (MTP_DATATYPE_UINT32 != rgmdbv[idxProp].wDataType)
            {
                ps = PSLERROR_INVALID_PARAMETER;
                goto Exit;
            }

            /*  And stash it
             */

            popcObj->dwGroupCodeCtx = rgmdbv[idxProp].vParam.valUInt32;
            break;

        default:
            /*  Unsupported parameter
             */

            PSLASSERT(PSLFALSE);
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }
    }

    /*  And create the serializer
     */

    ps = MTPSerializeCreate(_RgmsiObjectPropList,
                            PSLARRAYSIZE(_RgmsiObjectPropList),
                            (PSLPARAM)popcObj, PSLNULL, PSLNULL,
                            &(popcObj->mtpCDB.msSource));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the context data object
     */

    *pmdbcd = popcObj;
    popcObj = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(popcObj);
    return ps;
}


/*  _MTPDBObjectBinaryCtxClose
 *
 *  Virtual function for closing the object binary data request
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Context data to close
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDBObjectBinaryCtxClose(MTPDBCONTEXTDATA mdbcd)
{
    PSLSTATUS               ps;
    MTPOBJECTBINARYCTXOBJ*  pobcObj;

    /*  Validate arguments
     */

    if (PSLNULL == mdbcd)
    {
        ps = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /*  Extract our object
     */

    pobcObj = (MTPOBJECTBINARYCTXOBJ*)mdbcd;
    PSLASSERT(MTPOBJECTBINARYCTXOBJ_COOKIE == pobcObj->dwCookie);

    /*  If we have a current format use it to cancel handing of the binary
     *  object
     */

    if (PSLNULL != pobcObj->pfiCur)
    {
        /*  Commit the object if necessary
         */

        if (pobcObj->fCommit)
        {
            /*  Commit the context
             */

            ps = MTPDBContextBaseCommitItem(pobcObj->mdbc, pobcObj->aParamItem);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
            pobcObj->fCommit = PSLFALSE;
        }

        /*  Close the object
         */

        ps = MTPDBContextBaseCloseItem(pobcObj->mdbc, pobcObj->aParamItem);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

    /*  And release the object
     */

    SAFE_MTPDBCONTEXTDATAPOOLFREE(pobcObj);
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  _MTPDBObjectBinaryCtxCancel
 *
 *  Cancels committing any pending binary changes on close
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Context data to cancel
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDBObjectBinaryCtxCancel(MTPDBCONTEXTDATA mdbcd)
{
    PSLSTATUS               ps;
    MTPOBJECTBINARYCTXOBJ*  pobcObj;

    /*  Validate arguments
     */

    if (PSLNULL == mdbcd)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pobcObj = (MTPOBJECTBINARYCTXOBJ*)mdbcd;
    PSLASSERT(MTPOBJECTBINARYCTXOBJ_COOKIE == pobcObj->dwCookie);

    /*  Clear the commit flag
     */

    pobcObj->fCommit = PSLFALSE;
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  _MTPDBObjectBinaryCtxSerialize
 *
 *  Serializes the contents of an object's binary data
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Context data to serialize
 *      PSLVOID*        pvBuf               Buffer to serialize into
 *      PSLUINT32       cbBuf               Size of buffer
 *      PSLUINT32*      pcbWritten          Number of bytes written
 *      PSLUINT64*      pcbLeft             Number of bytes left
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDBObjectBinaryCtxSerialize(MTPDBCONTEXTDATA mdbcd,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft)
{
    PSLSTATUS               ps;
    MTPOBJECTBINARYCTXOBJ*  pobcObj;

    /*  Clear results for safety
     */

    if (PSLNULL != pcbWritten)
    {
        *pcbWritten = 0;
    }
    if (PSLNULL != pcbLeft)
    {
        *pcbLeft = 0;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mdbcd) || ((0 < cbBuf) && (PSLNULL == pvBuf)) ||
        (PSLNULL == pcbLeft))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pobcObj = (MTPOBJECTBINARYCTXOBJ*)mdbcd;
    PSLASSERT(MTPOBJECTBINARYCTXOBJ_COOKIE == pobcObj->dwCookie);

    /*  Validate state
     */

    PSLASSERT(PSLNULL != pobcObj->pfiCur);
    if (PSLNULL == pobcObj->pfiCur)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  And that we have a correctly formatted description
     */

    PSLASSERT(PSLNULL != pobcObj->pfiCur->pfnMTPGetObjectBinary);
    if (PSLNULL == pobcObj->pfiCur->pfnMTPGetObjectBinary)
    {
        ps = PSLERROR_INVALID_DATA;
        goto Exit;
    }

    /*  Call the format function to serialize the data
     */

    ps = pobcObj->pfiCur->pfnMTPGetObjectBinary(pobcObj->aParamItem,
                            pobcObj->pfiCur->wFormatCode, pvBuf, cbBuf,
                            pcbWritten, pcbLeft);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPObjectBinaryContextDataCreate
 *
 *  Creates an instance of the object binary context data.  Since this object
 *  just passes data through it does not use the Context Data Base
 *  implementation.
 *
 *  Arguments:
 *      MTPDBCONTEXTBASE
 *                      mdbc                ContextBase object to retrieve the
 *                                            object from
 *      PSLUINT32       dwFormat            Requested data format
 *      PSLUINT32       idxObj              Index of the object to return
 *      MTPDBCONTEXTDATA*
 *                      pmdbcd              Return buffer context data object
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPObjectBinaryContextDataCreate(MTPDBCONTEXTBASE mdbc,
                    PSLUINT32 dwFormat, PSLUINT32 idxObj,
                    MTPDBCONTEXTDATA* pmdbcd)
{
    PSLSTATUS               ps;
    MTPOBJECTBINARYCTXOBJ*  pobcNew = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmdbcd)
    {
        *pmdbcd = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == mdbc) ||
        (MTPDBCONTEXTDATADESC_FORMAT(dwFormat) !=
                MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA) ||
        (PSLNULL == pmdbcd))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  We only support querying this information
     */

    if (MTPDBCONTEXTDATA_QUERY != MTPDBCONTEXTDATADESC_FLAGS(dwFormat))
    {
        ps = PSLERROR_ACCESS_DENIED;
        goto Exit;
    }

    /*  Acquire a new context object to use
     */

    ps = MTPDBContextDataPoolAlloc(sizeof(*pobcNew), &pobcNew);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize the object
     */

#ifdef PSL_ASSERTS
    pobcNew->dwCookie = MTPOBJECTBINARYCTXOBJ_COOKIE;
#endif  /* PSL_ASSERTS */
    pobcNew->mtpCDB.vtbl = &_VtblContextDataObjectBinary;
    pobcNew->fCommit =
            (MTPDBCONTEXTDATA_QUERY != MTPDBCONTEXTDATADESC_FLAGS(dwFormat));
    pobcNew->mdbc = mdbc;

    /*  Retrieve the core details for the object that will be serialized
     */

    ps = MTPDBContextBaseGetItem(mdbc, idxObj,
                            MAKE_MTPDBCONTEXTDATADESC(
                                    MTPDBCONTEXTDATA_QUERY,
                                    MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA),
                            &(pobcNew->dwObjectID), &(pobcNew->aParamItem),
                            &(pobcNew->pfiCur));
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Return the base object
     */

    *pmdbcd = pobcNew;
    pobcNew = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(pobcNew);
    return ps;
}

