/*
 *  MTPHandlerObjectInfo.c
 *
 *  Contains the implementation of core command handlers
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerUtil.h"
#include "MTPHandlerDatabase.h"
#include "MTPDBHandlerUtil.h"

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPush1.h"
#endif

/*  MTPSETOBJECTPROTECTION
 */

struct _MTPSETOBJECTPROTECTION
{
    PSLUINT32               cProps;
    PSLUINT32               dwObjectID;
    PSLUINT16               wPropCode;
    PSLUINT16               wPropType;
    PSLUINT16               wPropValue;
} PSL_PACK1;

typedef struct _MTPSETOBJECTPROTECTION MTPSETOBJECTPROTECTION;

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPop.h"
#endif

static PSLSTATUS PSL_API _MTPContextPrepareGetObjectInfo(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareSendObjectInfo(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextDataConsumedSendObjectInfo(
                    BASEHANDLERDATA* pData);

static PSLSTATUS PSL_API _MTPContextPrepareGetObjectPropValue(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareSetObjectPropValue(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextDataConsumedSetObjectPropValue(
                    BASEHANDLERDATA* pData);

static PSLSTATUS PSL_API _MTPContextPrepareGetObjectReference(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareSetObjectReference(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextDataConsumedSetObjectReference(
                    BASEHANDLERDATA* pData);

static PSLSTATUS PSL_API _MTPContextPrepareGetObjectPropList(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareSetObjectPropList(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextDataConsumedSetObjectPropList(
                    BASEHANDLERDATA* pData);

static PSLSTATUS PSL_API _MTPContextPrepareSendObjectPropList(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextDataConsumedSendObjectPropList(
                    BASEHANDLERDATA* pData);

static PSLSTATUS PSL_API _MTPContextPrepareUpdateObjectPropList(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextDataConsumedUpdateObjectPropList(
                    BASEHANDLERDATA* pData);

static PSLSTATUS PSL_API _MTPContextPrepareDeleteObjectPropList(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextDataConsumedDeleteObjectPropList(
                    BASEHANDLERDATA* pData);

static PSLSTATUS PSL_API _MTPContextPrepareSetObjectProtection(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareWMPAcceleratedMetadataTransfer(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareWMPReportingAcquiredContent(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetObjectPropsSupported(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetObjectPropDesc(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPContextPrepareGetFormatCapabilities(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize);

/*
 *  MTPGetObjectInfo
 *
 *  This handler function for GetObjectInfo MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetObjectInfo(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                                  _MTPContextPrepareGetObjectInfo,
                                  PSLNULL);
}

/*
 *  MTPSendObjectInfo
 *
 *  This handler function for GetObjectInfo MTP operation.
 *
 */
PSLSTATUS PSL_API MTPSendObjectInfo(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_IN,
                              _MTPContextPrepareSendObjectInfo,
                              _MTPContextDataConsumedSendObjectInfo);
}

/*
 *  MTPGetObjectPropValue
 *
 *  This handler function for GetObjectPropValue MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetObjectPropValue(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                                  _MTPContextPrepareGetObjectPropValue,
                                  PSLNULL);
}

/*
 *  MTPSetObjectPropValue
 *
 *  This handler function for SetObjectPropValue MTP operation.
 *
 */
PSLSTATUS PSL_API MTPSetObjectPropValue(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_IN,
                              _MTPContextPrepareSetObjectPropValue,
                              _MTPContextDataConsumedSetObjectPropValue);
}

/*
 *  MTPGetObjectReferences
 *
 *  This handler function for GetObjectReference MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetObjectReferences(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                                  _MTPContextPrepareGetObjectReference,
                                  PSLNULL);
}

/*
 *  MTPSetObjectReferences
 *
 *  This handler function for SetObjectReferences MTP operation.
 *
 */
PSLSTATUS PSL_API MTPSetObjectReferences(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_IN,
                              _MTPContextPrepareSetObjectReference,
                              _MTPContextDataConsumedSetObjectReference);
}

/*
 *  MTPGetObjectPropList
 *
 *  This handler function for GetObjectPropList MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetObjectPropList(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                                  _MTPContextPrepareGetObjectPropList,
                                  PSLNULL);
}

/*
 *  MTPSetObjectPropList
 *
 *  This handler function for SetObjectPropList MTP operation.
 *
 */
PSLSTATUS PSL_API MTPSetObjectPropList(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleSetObjectPropList(md, pMsg, MTPHANDLERSTATE_DATA_IN,
                              _MTPContextPrepareSetObjectPropList,
                              _MTPContextDataConsumedSetObjectPropList);
}

/*
 *  MTPSendObjectPropList
 *
 *  This handler function for SendObjectPropList MTP operation.
 *
 */
PSLSTATUS PSL_API MTPSendObjectPropList(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_IN,
                              _MTPContextPrepareSendObjectPropList,
                              _MTPContextDataConsumedSendObjectPropList);
}

/*
 *  MTPUpdateObjectPropList
 *
 *  This handler function for UpdateObjectPropList MTP operation.
 *
 */
PSLSTATUS PSL_API MTPUpdateObjectPropList(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_IN,
                              _MTPContextPrepareUpdateObjectPropList,
                              _MTPContextDataConsumedUpdateObjectPropList);
}

/*
 *  MTPDeleteObjectPropList
 *
 *  This handler function for DeleteObjectPropList MTP operation.
 *
 */
PSLSTATUS PSL_API MTPDeleteObjectPropList(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_IN,
                              _MTPContextPrepareDeleteObjectPropList,
                              _MTPContextDataConsumedDeleteObjectPropList);
}

/*
 *  MTPSetObjectProtection
 *
 *  This handler function for SendObjectPropList MTP operation.
 *
 */
PSLSTATUS PSL_API MTPSetObjectProtection(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_RESPONSE,
                              _MTPContextPrepareSetObjectProtection,
                              PSLNULL);
}

/*
 *  MTPWMPAcceleratedMetadataTransfer
 *
 *  This handler function for WMPAcceleratedMetadataTransfer MTP operation.
 *
 */
PSLSTATUS PSL_API MTPWMPAcceleratedMetadataTransfer(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                  _MTPContextPrepareWMPAcceleratedMetadataTransfer,
                  PSLNULL);
}

/*
 *  MTPWMPReportingAcquiredContent
 *
 *  This handler function for WMPReportingAcquiredContent MTP operation.
 *
 */
PSLSTATUS PSL_API MTPWMPReportingAcquiredContent(
                        MTPDISPATCHER md,
                        PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                  _MTPContextPrepareWMPReportingAcquiredContent,
                  PSLNULL);
}

/*
 *  MTPGetObjectPropsSupported
 *
 *  This handler function for GetObjectPropsSupported MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetObjectPropsSupported(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                  _MTPContextPrepareGetObjectPropsSupported, PSLNULL);
}


/*
 *  MTPGetObjectPropDesc
 *
 *  This handler function for GetObjectPropDesc MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetObjectPropDesc(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                    _MTPContextPrepareGetObjectPropDesc, PSLNULL);
}


/*
 *  MTPGetFormatCapabilities
 *
 *  Handles the GetFormatCapabilities MTP operation
 *
 */

PSLSTATUS PSL_API MTPGetFormatCapabilities(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandleDBOperations(md, pMsg, MTPHANDLERSTATE_DATA_OUT,
                    _MTPContextPrepareGetFormatCapabilities, PSLNULL);
}

/*
 * Utilities.
 */
PSLSTATUS PSL_API _MTPContextPrepareGetObjectInfo(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    mpq;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    mpq.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    mpq.wDataType = MTP_DATATYPE_UINT32;
    mpq.vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, &mpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_QUERY,
                    MTPDBCONTEXTDATAFORMAT_OBJECTINFO);

    status = MTPDBContextDataOpen(mdbc, dwFormat, PSLNULL, 0,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareSendObjectInfo(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[2];
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(2))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[0].wPropCode = MTP_OBJECTPROPCODE_STORAGEID;
    rgmpq[0].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    rgmpq[1].wPropCode = MTP_OBJECTPROPCODE_PARENT;
    rgmpq[1].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[1].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam2));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 2, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_INSERT,
                    MTPDBCONTEXTDATAFORMAT_OBJECTINFO);

    status = MTPDBContextDataOpen(mdbc, dwFormat, PSLNULL, 0,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextDataConsumedSendObjectInfo(BASEHANDLERDATA* pData)
{
    PSLSTATUS status;
    PSLUINT32 dwObjectHandle;
    PSLUINT32 dwParentHandle;
    PSLUINT32 dwStorageID;


    if (PSLNULL == pData)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPDBContextCommit(((DBHANDLERDATA*)pData)->mdbc,
                            MTP_OBJECTHANDLE_UNDEFINED);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBContextGetHandle(((DBHANDLERDATA*)pData)->mdbc,
                                    PSLTRUE,
                                    0,
                                    &dwObjectHandle,
                                    &dwParentHandle,
                                    &dwStorageID);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pData->opLevelData.mtpResponse.dwParam1 = dwStorageID;
    pData->opLevelData.mtpResponse.dwParam2 = dwParentHandle;
    pData->opLevelData.mtpResponse.dwParam3 = dwObjectHandle;
    pData->opLevelData.dwNumRespParams = 3;

    /*  Also stash the object ID for the last object sent so that we
     *  can use it on a subsequent SendObject call
     */

    pData->sesLevelData.dwLastSendInfoID = dwObjectHandle;

Exit:
    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareGetObjectPropValue(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[2];
    PSLUINT32           cmpq = 0;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(2))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
    cmpq++;

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam2));
    cmpq++;

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_QUERY,
                    MTPDBCONTEXTDATAFORMAT_OBJECTPROPARRAY);

    status = MTPDBContextDataOpen(mdbc, dwFormat, rgmpq + 1, 1,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareSetObjectPropValue(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[2];
    PSLUINT32           cmpq = 0;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(2))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
    cmpq++;

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam2));
    cmpq++;

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_UPDATE,
                    MTPDBCONTEXTDATAFORMAT_OBJECTPROPARRAY);

    status = MTPDBContextDataOpen(mdbc, dwFormat, rgmpq + 1, 1,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->bhd.opLevelData.dwObjectID =
                            MTPLoadUInt32(&(pOpRequest->dwParam1));

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextDataConsumedSetObjectPropValue(
                    BASEHANDLERDATA* pData)
{
    PSLSTATUS status;

    if (PSLNULL == pData)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPDBContextCommit(((DBHANDLERDATA*)pData)->mdbc,
                        pData->opLevelData.dwObjectID);

Exit:
    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareGetObjectReference(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[2];
    PSLUINT32           cmpq = 0;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
    cmpq++;

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTREFERENCES;
    cmpq++;

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_QUERY,
                    MTPDBCONTEXTDATAFORMAT_OBJECTHANDLE);

    status = MTPDBContextDataOpen(mdbc, dwFormat, rgmpq + 1, 1,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareSetObjectReference(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[2];
    PSLUINT32           cmpq = 0;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
    cmpq++;

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valInt32 = MTP_OBJECTPROPCODE_OBJECTREFERENCES;
    cmpq++;

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_UPDATE,
                    MTPDBCONTEXTDATAFORMAT_OBJECTHANDLE);

    status = MTPDBContextDataOpen(mdbc, dwFormat, rgmpq + 1, 1,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->bhd.opLevelData.dwObjectID =
                            MTPLoadUInt32(&(pOpRequest->dwParam1));

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}


PSLSTATUS PSL_API _MTPContextDataConsumedSetObjectReference(
                    BASEHANDLERDATA* pData)
{
    PSLSTATUS status;

    if (PSLNULL == pData)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPDBContextCommit(((DBHANDLERDATA*)pData)->mdbc,
                            pData->opLevelData.dwObjectID);

Exit:
    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareGetObjectPropList(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[5];
    PSLUINT32           cmpq  = 0;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(5))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
    cmpq++;

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_OBJECTFORMAT;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT16;
    rgmpq[cmpq].vParam.valUInt16 = (PSLUINT16)MTPLoadUInt32(
                            &(pOpRequest->dwParam2));
    cmpq++;

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_DEPTH;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam5));
    cmpq++;

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, cmpq, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam3));
    cmpq++;

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_GROUPCODE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam4));
    cmpq++;

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_QUERY,
                    MTPDBCONTEXTDATAFORMAT_OBJECTPROPLIST);

    status = MTPDBContextDataOpen(mdbc, dwFormat, (rgmpq + 3), 2,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareSetObjectPropList(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;
    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }
    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(0))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }
    status = PSLSUCCESS;
Exit:
    return status;
}

PSLSTATUS PSL_API _MTPContextDataConsumedSetObjectPropList(
                    BASEHANDLERDATA* pData)
{
    PSLSTATUS status;

    if (PSLNULL == pData)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPDBContextCommit(((DBHANDLERDATA*)pData)->mdbc,
                            MTP_OBJECTHANDLE_ALL);

Exit:
    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareSendObjectPropList(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[4];
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(5))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[0].wPropCode = MTP_OBJECTPROPCODE_STORAGEID;
    rgmpq[0].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    rgmpq[1].wPropCode = MTP_OBJECTPROPCODE_PARENT;
    rgmpq[1].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[1].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam2));

    rgmpq[2].wPropCode = MTP_OBJECTPROPCODE_OBJECTFORMAT;
    rgmpq[2].wDataType = MTP_DATATYPE_UINT16;
    rgmpq[2].vParam.valUInt16 = (PSLUINT16)MTPLoadUInt32(
                            &(pOpRequest->dwParam3));


    rgmpq[3].wPropCode = MTP_OBJECTPROPCODE_OBJECTSIZE;
    rgmpq[3].wDataType = MTP_DATATYPE_UINT64;
    rgmpq[3].vParam.valUInt64 =
            (((PSLUINT64)MTPLoadUInt32(&(pOpRequest->dwParam4))) << 32) |
            (PSLUINT64)MTPLoadUInt32(&(pOpRequest->dwParam5));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 3, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_INSERT,
                    MTPDBCONTEXTDATAFORMAT_OBJECTPROPLIST);

    status = MTPDBContextDataOpen(mdbc, dwFormat, &(rgmpq[3]), 1,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextDataConsumedSendObjectPropList(
                    BASEHANDLERDATA* pData)
{
    PSLSTATUS status;
    PSLUINT32 dwObjectHandle;
    PSLUINT32 dwParentHandle;
    PSLUINT32 dwStorageID;


    if (PSLNULL == pData)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPDBContextCommit(((DBHANDLERDATA*)pData)->mdbc,
                                MTP_OBJECTHANDLE_UNDEFINED);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBContextGetHandle(((DBHANDLERDATA*)pData)->mdbc,
                                    PSLTRUE,
                                    0,
                                    &dwObjectHandle,
                                    &dwParentHandle,
                                    &dwStorageID);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pData->opLevelData.mtpResponse.dwParam1 = dwStorageID;
    pData->opLevelData.mtpResponse.dwParam2 = dwParentHandle;
    pData->opLevelData.mtpResponse.dwParam3 = dwObjectHandle;
    pData->opLevelData.mtpResponse.dwParam4 = 0;
    pData->opLevelData.dwNumRespParams = 4;

    /*  Also stash the object ID for the last object sent so that we
     *  can use it on a subsequent SendObject call
     */

    pData->sesLevelData.dwLastSendInfoID = dwObjectHandle;

Exit:
    return status;
}


PSLSTATUS PSL_API _MTPContextPrepareUpdateObjectPropList(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[1];
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[0].wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    rgmpq[0].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_UPDATE,
                    MTPDBCONTEXTDATAFORMAT_OBJECTPROPLIST);

    status = MTPDBContextDataOpen(mdbc, dwFormat, PSLNULL, 0,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->bhd.opLevelData.dwObjectID =
                            MTPLoadUInt32(&(pOpRequest->dwParam1));

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}


PSLSTATUS PSL_API _MTPContextDataConsumedUpdateObjectPropList(
                    BASEHANDLERDATA* pData)
{
    PSLSTATUS status;

    if (PSLNULL == pData)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPDBContextCommit(((DBHANDLERDATA*)pData)->mdbc,
                            pData->opLevelData.dwObjectID);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    pData->opLevelData.mtpResponse.dwParam1 = 0;
    pData->opLevelData.dwNumRespParams = 1;

    /*  Also stash the object ID for the last object sent so that we
     *  can use it on a subsequent SendObject call
     */

    pData->sesLevelData.dwLastSendInfoID = pData->opLevelData.dwObjectID;

Exit:
    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareDeleteObjectPropList(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[1];
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[0].wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    rgmpq[0].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[0].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_DELETE,
                    MTPDBCONTEXTDATAFORMAT_OBJECTPROPLIST);

    status = MTPDBContextDataOpen(mdbc, dwFormat, PSLNULL, 0,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextDataConsumedDeleteObjectPropList(
                    BASEHANDLERDATA* pData)
{
    PSLSTATUS status;

    if (PSLNULL == pData)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPDBContextCommit(((DBHANDLERDATA*)pData)->mdbc,
                            MTP_OBJECTHANDLE_ALL);

Exit:
    return status;
}


PSLSTATUS PSL_API _MTPContextPrepareSetObjectProtection(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;
    MTPDBVARIANTPROP    mtpq;
    PSLUINT32           dwDataFormat;

    MTPSETOBJECTPROTECTION  sopData;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(2))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    mtpq.wPropCode = MTP_OBJECTPROPCODE_OBJECTHANDLE;
    mtpq.wDataType = MTP_DATATYPE_UINT32;
    mtpq.vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));

    status = MTPServiceResolverDBContextOpen(
                          pData->sesLevelData.msrServices, MTPJOINTYPE_AND,
                          &mtpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwDataFormat = MAKE_MTPDBCONTEXTDATADESC(
                        MTPDBCONTEXTDATA_UPDATE,
                        MTPDBCONTEXTDATAFORMAT_OBJECTPROTECTION);

    status = MTPDBContextDataOpen(mdbc, dwDataFormat,
                        PSLNULL, 0, PSLNULL, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /*  Generate a "data buffer" to hand off with the necessary behavior
     *  so that this looks like a simple "SetObjectPropList"
     */

    MTPStoreUInt32(&(sopData.cProps), 1);
    MTPStoreUInt32(&(sopData.dwObjectID), mtpq.vParam.valUInt32);
    MTPStoreUInt16(&(sopData.wPropCode), MTP_OBJECTPROPCODE_PROTECTIONSTATUS);
    MTPStoreUInt16(&(sopData.wPropType), MTP_DATATYPE_UINT16);
    MTPStoreUInt16(&(sopData.wPropValue),
                            (PSLUINT16)MTPLoadUInt32(&(pOpRequest->dwParam2)));

    /* pass the "buffer" to deserializer */

    status = MTPDBContextDataDeserialize(mdbcd, &sopData, sizeof(sopData),
                            PSLNULL, PSLNULL, 0, 0);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDBContextCommit(mdbc, MTPLoadUInt32(&(pOpRequest->dwParam1)));

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareWMPAcceleratedMetadataTransfer(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[3];
    PSLUINT32           cmpq = 0;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(3))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    InitPropQuad(&rgmpq[cmpq]);
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
    cmpq++;

    InitPropQuad(&rgmpq[cmpq]);
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam2));
    cmpq++;

    InitPropQuad(&rgmpq[cmpq]);
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam3));
    cmpq++;


    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, PSLNULL, 0, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_QUERY,
                    MTPDBCONTEXTDATAFORMAT_OBJECTHANDLE);

    status = MTPDBContextDataOpen(mdbc, dwFormat, rgmpq, 3,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareWMPReportingAcquiredContent(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[3];
    PSLUINT32           cmpq = 0;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(3))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    InitPropQuad(&rgmpq[cmpq]);
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
    cmpq++;

    InitPropQuad(&rgmpq[cmpq]);
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam2));
    cmpq++;

    InitPropQuad(&rgmpq[cmpq]);
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam3));
    cmpq++;


    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, PSLNULL, 0, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_QUERY,
                    MTPDBCONTEXTDATAFORMAT_OBJECTHANDLE);

    status = MTPDBContextDataOpen(mdbc, dwFormat, rgmpq, 3,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareGetObjectPropsSupported(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[1];
    PSLUINT32           cmpq = 0;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_OBJECTFORMAT;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT16;
    rgmpq[cmpq].vParam.valUInt16 = (PSLUINT16)MTPLoadUInt32(
                                               &(pOpRequest->dwParam1));
    cmpq++;

    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_QUERY,
                    MTPDBCONTEXTDATAFORMAT_OBJECTPROPSSUPPORTED);

    status = MTPDBContextDataOpen(mdbc, dwFormat, PSLNULL, 0,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}

PSLSTATUS PSL_API _MTPContextPrepareGetObjectPropDesc(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[2];
    PSLUINT32           cmpq = 0;
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(2))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_PROPCODE;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT32;
    rgmpq[cmpq].vParam.valUInt32 = MTPLoadUInt32(&(pOpRequest->dwParam1));
    cmpq++;

    rgmpq[cmpq].wPropCode = MTP_OBJECTPROPCODE_OBJECTFORMAT;
    rgmpq[cmpq].wDataType = MTP_DATATYPE_UINT16;
    rgmpq[cmpq].vParam.valUInt16 = (PSLUINT16)MTPLoadUInt32(
                                               &(pOpRequest->dwParam2));
    cmpq++;

    /* Pass format code as a part of DB context*/
    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, &rgmpq[1], 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_QUERY,
                    MTPDBCONTEXTDATAFORMAT_OBJECTPROPDESC);

    /* Pass property code as a part of DB data context*/
    status = MTPDBContextDataOpen(mdbc, dwFormat, &rgmpq[0], 1,
                                  &qwBufSize, &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}


PSLSTATUS PSL_API _MTPContextPrepareGetFormatCapabilities(
                    PXMTPOPERATION   pOpRequest,
                    PSLUINT64        cbOpRequest,
                    BASEHANDLERDATA* pData,
                    PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    MTPDBVARIANTPROP    rgmpq[1];
    MTPDBCONTEXT        mdbc  = PSLNULL;
    MTPDBCONTEXTDATA    mdbcd = PSLNULL;

    PSLUINT64           qwBufSize;
    PSLUINT32           dwFormat;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgmpq[0].wPropCode = MTP_OBJECTPROPCODE_OBJECTFORMAT;
    rgmpq[0].wDataType = MTP_DATATYPE_UINT16;
    rgmpq[0].vParam.valUInt16 = (PSLUINT16)MTPLoadUInt32(
                                               &(pOpRequest->dwParam1));

    /* Pass format code as a part of DB context*/
    status = MTPServiceResolverDBContextOpen(pData->sesLevelData.msrServices,
                              MTPJOINTYPE_AND, rgmpq, 1, &mdbc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    dwFormat = MAKE_MTPDBCONTEXTDATADESC(
                    MTPDBCONTEXTDATA_QUERY,
                    MTPDBCONTEXTDATAFORMAT_FORMATCAPABILITIES);

    /* Pass property code as a part of DB data context*/
    status = MTPDBContextDataOpen(mdbc, dwFormat, PSLNULL, 0, &qwBufSize,
                            &mdbcd);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((DBHANDLERDATA*)pData)->mdbc = mdbc;
    mdbc = PSLNULL;

    ((DBHANDLERDATA*)pData)->mdbcd = mdbcd;
    mdbcd = PSLNULL;

    *pqwBufSize = qwBufSize;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(mdbcd);
    SAFE_MTPDBCONTEXTCLOSE(mdbc);

    return status;
}


