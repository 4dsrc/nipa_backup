/*
 *  MTPDevicePropertyContext.c
 *
 *  Contains definitions of the MTP Device Property Context helpers.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPDBContextData.h"
#include "MTPDevicePropertyContext.h"

/*  Local Defines
 */

#define MTPDEVICEPROPCTXOBJ_COOKIE      'mdpc'

enum
{
    MTPDEVICEPROPCTXVALUE_DEFAULT_VALUE = 0,
    MTPDEVICEPROPCTXVALUE_CURRENT_VALUE,
};

/*  Local Types
 */

typedef struct _MTPDEVICEPROPCTXOBJ
{
    MTPDBCONTEXTDATABASEOBJ             mtpCDB;
#ifdef PSL_ASSERTS
    PSLUINT32                           dwCookie;
#endif  /* PSL_ASSERT */
    PSLUINT32                           dwDataFormat;
    PSL_CONST MTPPROPERTYREC*           precDevice;
} MTPDEVICEPROPCTXOBJ;

/*  Local Functions
 */

static PSLSTATUS PSL_API _MTPDevicePropDescGetDefaultValue(
                    PSLUINT16 wPropCode,
                    PSLPARAM aParam,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS PSL_API _MTPDevicePropDescGetCurrentValue(
                    PSLUINT16 wPropCode,
                    PSLPARAM aParam,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS PSL_API _MTPDevicePropCtxSerializeValue(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

static PSLSTATUS PSL_API _MTPDevicePropCtxDeserialize(
                    MTPDBCONTEXTDATA mdbcd,
                    PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLMSGQUEUE mqHandler,
                    MTPCONTEXT* pmtpctx,
                    PSLPARAM aParam,
                    PSLUINT32 dwMsgFlags);

static PSLSTATUS PSL_API _MTPDevicePropCtxCommit(
                    MTPDEVICEPROPCONTEXT mdpc);

static PSLSTATUS PSL_API _MTPDevicePropCtxReset(
                    MTPDEVICEPROPCONTEXT mdpc);

/*  Local Variables
 */

static PSL_CONST MTPDEVICEPROPCONTEXTVTBL   _VtblDevProp =
{
    MTPDBContextDataBaseClose,
    MTPDBContextDataBaseCancel,
    MTPDBContextDataBaseSerialize,
    MTPDBContextDataBaseStartDeserialize,
    _MTPDevicePropCtxDeserialize,
    _MTPDevicePropCtxCommit,
    _MTPDevicePropCtxReset
};

static PSL_CONST MTPSERIALIZEINFO       _RgmsiDevicePropValue[] =
{
    /*  Property Value
     */

    {
        MTP_DATATYPE_UNDEFINED,
        MTPSERIALIZEFLAG_BYFUNCTION,
        (PSLPARAM)_MTPDevicePropCtxSerializeValue,
        MTPDEVICEPROPCTXVALUE_CURRENT_VALUE
    }
};


/*  _MTPDevicePropCtxSerializeValue
 *
 *  Retrieves the property requested by the serializer
 *
 *  Arguments:
 *      PSLPARAM        aContext            Context for callback
 *      PSLPARAM        aValueContext       Value context for callback
 *      PSLUINT16       wType               Property type
 *      PSLVOID*        pvBuf               Buffer to return property in
 *      PSLUINT32       cbBuf               Size of buffer
 *      PSLUINT32*      pcbUsed             Number of bytes used
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDevicePropCtxSerializeValue(PSLPARAM aContext,
                    PSLPARAM aValueContext, PSLUINT16 wType,
                    PSLVOID* pvBuf, PSLUINT32 cbBuf, PSLUINT32* pcbUsed)
{
    PSLSTATUS                       ps;
    MTPDEVICEPROPCTXOBJ*            pmdpcObj;
    PSL_CONST MTPDEVICEPROPINFO*    pdpiCur;
    PFNMTPPROPGETVALUE              pfnGetValue;

    /*  Clear results for safety
     */

    if (PSLNULL != pcbUsed)
    {
        *pcbUsed = 0;
    }

    /*  Validate arguments- we only accept a PSLNULL buffer if we are
     *  handling unknown datatypes
     */

    if ((PSLNULL == aContext) ||
        ((PSLNULL == pvBuf) && (MTP_DATATYPE_UNDEFINED != wType)) ||
        (PSLNULL == pcbUsed))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object and the property information
     */

    pmdpcObj = (MTPDEVICEPROPCTXOBJ*)aContext;
    PSLASSERT(MTPDEVICEPROPCTXOBJ_COOKIE == pmdpcObj->dwCookie);

    pdpiCur = (PSL_CONST MTPDEVICEPROPINFO*)pmdpcObj->mtpCDB.pvData;
    PSLASSERT(PSLNULL != pmdpcObj->precDevice);
    if (PSLNULL == pmdpcObj->precDevice)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  And handle the requested properties
     */

    switch (aValueContext)
    {
    case MTPDEVICEPROPCTXVALUE_DEFAULT_VALUE:
    case MTPDEVICEPROPCTXVALUE_CURRENT_VALUE:
        /*  Validate the type- it should always be undefined
         */

        if (MTP_DATATYPE_UNDEFINED != wType)
        {
            ps = PSLERROR_INVALID_PARAMETER;
            goto Exit;
        }

        /*  Determine the function to use
         */

        switch (aValueContext)
        {
        case MTPDEVICEPROPCTXVALUE_DEFAULT_VALUE:
            /*   Use the default value function
             */

            pfnGetValue = pdpiCur->infoProp.pfnMTPPropGetDefaultValue;
            break;

        case MTPDEVICEPROPCTXVALUE_CURRENT_VALUE:
            /*  Use the current value function
             */

            pfnGetValue = pdpiCur->infoProp.pfnMTPPropGetValue;
            break;

        default:
            /*  Use the default function
             */

            pfnGetValue = PSLNULL;
            break;
        }

        /*  And make the call using the correct property type based on the
         *  property record
         */

        if (PSLNULL != pfnGetValue)
        {
            ps = pfnGetValue(pdpiCur->infoProp.wPropCode,
                            pmdpcObj->precDevice->wDataType,
                            pmdpcObj->mtpCDB.aParam,
                            pdpiCur->infoProp.aContextProp,
                            pvBuf, cbBuf, pcbUsed);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        else
        {
            ps = MTPPropertySerializeDefaultValue(pmdpcObj->precDevice,
                            pvBuf, cbBuf, pcbUsed);
            if (PSL_FAILED(ps))
            {
                goto Exit;
            }
        }
        break;

    default:
        /*  Should never get here
         */

        PSLASSERT(PSLFALSE);
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

Exit:
    return ps;
}


/*  _MTPDevicePropCtxDeserialize
 *
 *  Deserializes the data stream for the Device Property
 *
 *  Arguments:
 *      MTPDBCONTEXTDATA
 *                      mdbcd               Context data to deserialized
 *      PSLVOID*        pvBuf               Buffer to deserialize
 *      PSLUINT32       cbBuf               Number of bytes in buffer
 *      PSLMSGQUEUE     mqHandler           Handler message queue for sending
 *                                            messages
 *      MTPCONTEXT*     pmtpctx             MTP Context for deserialization
 *      PSLPARAM        aParam              Parameter data
 *      PSLUINT32       dwMsgFlags          Message flags associated with buffer
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDevicePropCtxDeserialize(MTPDBCONTEXTDATA mdbcd,
                    PSL_CONST PSLVOID* pvBuf, PSLUINT32 cbBuf,
                    PSLMSGQUEUE mqHandler, MTPCONTEXT* pmtpctx,
                    PSLPARAM aParam, PSLUINT32 dwMsgFlags)
{
    PSLUINT32                       cbSize;
    PSLSTATUS                       ps;
    MTPDEVICEPROPCTXOBJ*            pmdpcObj;
    PSL_CONST MTPDEVICEPROPINFO*    pdpiCur;
    PSLMSG*                         pMsg = PSLNULL;

    /*  Validate arguments
     */

    if ((PSLNULL == mdbcd) || ((PSLNULL != mqHandler) && (PSLNULL == pmtpctx)))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmdpcObj = (MTPDEVICEPROPCTXOBJ*)mdbcd;
    PSLASSERT(MTPDEVICEPROPCTXOBJ_COOKIE == pmdpcObj->dwCookie);

    /*  Get at the property information
     */

    pdpiCur = (PSL_CONST MTPDEVICEPROPINFO*)pmdpcObj->mtpCDB.pvData;
    PSLASSERT(PSLNULL != pmdpcObj->precDevice);
    if (PSLNULL == pmdpcObj->precDevice)
    {
        ps = PSLERROR_UNEXPECTED;
        goto Exit;
    }

    /*  Check the property access
     */

    if (MTP_PROPGETSET_GETSET != pdpiCur->infoProp.bGetSet)
    {
        ps = PSLERROR_ACCESS_DENIED;
        goto Exit;
    }

    /*  Make sure we have a function to call to do the real work
     */

    PSLASSERT(PSLNULL != pdpiCur->infoProp.pfnMTPPropSetValue);
    if (PSLNULL == pdpiCur->infoProp.pfnMTPPropSetValue)
    {
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;
    }

    /*  Make sure this is really a device property
     */

    PSLASSERT(PROPFLAGS_TYPE_DEVICE & pmdpcObj->precDevice->wFlags);
    if (!(PROPFLAGS_TYPE_DEVICE & pmdpcObj->precDevice->wFlags))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Determine the number of bytes we should be expecting in the data
     */

    ps = MTPPropertyReadSize(pmdpcObj->precDevice->wDataType, pvBuf, cbBuf,
                            &cbSize);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure we got all of the data we expect
     */

    if (cbSize != cbBuf)
    {
        ps = PSLERROR_INVALID_DATA;
        goto Exit;
    }

    /*  Call the function to do the real deserialization
     */

    ps = pdpiCur->infoProp.pfnMTPPropSetValue(pdpiCur->infoProp.wPropCode,
                        pmdpcObj->precDevice->wDataType, pmdpcObj->mtpCDB.aParam,
                        pdpiCur->infoProp.aContextProp, pvBuf, cbBuf, &cbSize);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Make sure the whole buffer was consumed
     */

    if (cbSize != cbBuf)
    {
        ps = PSLERROR_INVALID_DATA;
        goto Exit;
    }

    /*  Report that the data was consumed if necessary
     */

    if (PSLNULL != mqHandler)
    {
        ps = PSLMsgAlloc(PSLNULL, &pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }

        pMsg->msgID = MTPMSG_DATA_CONSUMED;
        pMsg->aParam0 = (PSLPARAM)pmtpctx;
        pMsg->aParam1 = aParam;
        pMsg->aParam2 = (PSLPARAM)pvBuf;
        pMsg->aParam3 = MTPMSGFLAG_TERMINATE & dwMsgFlags;
        pMsg->alParam = cbBuf;

        ps = PSLMsgPost(mqHandler, pMsg);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        pMsg = PSLNULL;
    }

Exit:
    SAFE_PSLMSGFREE(pMsg);
    return ps;
}


/*  _MTPDevicePropCtxCommit
 *
 *  Commits the device property specified
 *
 *  Arguments:
 *
 *      MTPDEVICEPROPCONTEXT
 *                      mdpc                Device property context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDevicePropCtxCommit(MTPDEVICEPROPCONTEXT mdpc)
{
    PSLSTATUS                       ps;
    MTPDEVICEPROPCTXOBJ*            pmdpcObj;
    PSL_CONST MTPDEVICEPROPINFO*    pdpiProp;

    /*  Validate arguments
     */

    if (PSLNULL == mdpc)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmdpcObj = (MTPDEVICEPROPCTXOBJ*)mdpc;
    PSLASSERT(MTPDEVICEPROPCTXOBJ_COOKIE == pmdpcObj->dwCookie);

    /*  Get at the property information
     */

    pdpiProp = (PSL_CONST MTPDEVICEPROPINFO*)pmdpcObj->mtpCDB.pvData;

    /*  And call commit on it if a function is provided
     */

    if (PSLNULL != pdpiProp->pfnCommit)
    {
        ps = pdpiProp->pfnCommit(pdpiProp->infoProp.wPropCode);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }
    ps = PSLSUCCESS;

Exit:
    return ps;
}


/*  _MTPDevicePropCtxReset
 *
 *  Resets the device property specified
 *
 *  Arguments:
 *
 *      MTPDEVICEPROPCONTEXT
 *                      mdpc                Device property context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API _MTPDevicePropCtxReset(MTPDEVICEPROPCONTEXT mdpc)
{
    PSLSTATUS                       ps;
    MTPDEVICEPROPCTXOBJ*            pmdpcObj;
    PSL_CONST MTPDEVICEPROPINFO*    pdpiProp;

    /*  Validate arguments
     */

    if (PSLNULL == mdpc)
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Extract our object
     */

    pmdpcObj = (MTPDEVICEPROPCTXOBJ*)mdpc;
    PSLASSERT(MTPDEVICEPROPCTXOBJ_COOKIE == pmdpcObj->dwCookie);

    /*  Get at the property information and call the commit operation
     */

    pdpiProp = (PSL_CONST MTPDEVICEPROPINFO*)pmdpcObj->mtpCDB.pvData;

    if (PSLNULL == pdpiProp->pfnResetValue)
    {
        ps = PSLERROR_NOT_IMPLEMENTED;
        goto Exit;
    }

    ps = pdpiProp->pfnResetValue(pdpiProp->infoProp.wPropCode);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}


/*  MTPDevicePropContextCreate
 *
 *  Creates an instance of the generic Device Property Context handler
 *
 *  Arguments:
 *      PSLUINT32       dwDataFormat        Data format for the device
 *      MTPDEVICEPROPINFO*
 *                      pdpiProp            Device property information
 *      PSLPARAM        aParam              Callback parameter
 *      PFNONMTPDBCONTEXTDATABASECLOSE
 *                      pfnOnClose          Callback function on close
 *      MTPDEVICEPROPCONTEXT*
 *                      pmdpc               Resulting context
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 */

PSLSTATUS PSL_API MTPDevicePropContextCreate(PSLUINT32 dwDataFormat,
                    PSL_CONST MTPDEVICEPROPINFO* pdpiProp,
                    PSLPARAM aParam, PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPDEVICEPROPCONTEXT* pmdpc)
{
    PSLSTATUS                   ps;
    MTPDEVICEPROPCTXOBJ*        pmdpcObj = PSLNULL;

    /*  Clear result for safety
     */

    if (PSLNULL != pmdpc)
    {
        *pmdpc = PSLNULL;
    }

    /*  Validate arguments
     */

    if ((PSLNULL == pdpiProp) || (PSLNULL == pmdpc))
    {
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Allocate a new base context data handler object
     */

    ps = MTPDBContextDataBaseCreate(sizeof(*pmdpcObj), &pmdpcObj);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

    /*  Initialize our object
     */

#ifdef PSL_ASSERTS
    pmdpcObj->dwCookie = MTPDEVICEPROPCTXOBJ_COOKIE;
#endif  /* PSL_ASSERTS */
    pmdpcObj->dwDataFormat = dwDataFormat;
    pmdpcObj->mtpCDB.mdbcdObj.vtbl = (MTPDBCONTEXTDATAVTBL*)&_VtblDevProp;
    pmdpcObj->mtpCDB.pfnOnClose = pfnOnClose;
    pmdpcObj->mtpCDB.aParam = aParam;
    pmdpcObj->mtpCDB.pvData = pdpiProp;

    /*  If the user did not provide the full property rec retrieve the default
     *  one from the schema table
     */

    if (PSLNULL != pdpiProp->infoProp.precProp)
    {
        /*  Use the record that we were provided
         */

        pmdpcObj->precDevice = pdpiProp->infoProp.precProp;
    }
    else
    {
        /*  Retrieve the default property
         */

        ps = MTPPropertyGetDefaultRec(pdpiProp->infoProp.wPropCode,
                            PROPFLAGS_TYPE_DEVICE,
                            (PSL_CONST MTPPROPERTYREC**)&(pmdpcObj->precDevice));
        if (PSLSUCCESS != ps)
        {
            ps = PSL_FAILED(ps) ? ps : PSLERROR_INVALID_DATA;
            goto Exit;
        }
    }

    /*  And allocate a serializer
     */

    /*  Determine which serializer needs to be created
     */

    switch (dwDataFormat)
    {
    case MTPDBCONTEXTDATAFORMAT_DEVICEPROPLIST:
        /*  Handling just the property- use the local serializer
         */

        ps = MTPSerializeCreate(_RgmsiDevicePropValue,
                            PSLARRAYSIZE(_RgmsiDevicePropValue),
                            (PSLPARAM)pmdpcObj, pmdpcObj->mtpCDB.pvData,
                            PSLNULL, &(pmdpcObj->mtpCDB.msSource));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    case MTPDBCONTEXTDATAFORMAT_DEVICEPROPDESC:
        /*  Description is handled via the generic property helper- use
         *  it's serializer
         */

        ps = MTPPropertyDescSerializeCreate(&(pdpiProp->infoProp),
                            PROPFLAGS_TYPE_DEVICE, aParam,
                            &(pmdpcObj->mtpCDB.msSource));
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
        break;

    default:
        ps = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /*  Return the context
     */

    *pmdpc = pmdpcObj;
    pmdpcObj = PSLNULL;
    ps = PSLSUCCESS;

Exit:
    SAFE_MTPDBCONTEXTDATACLOSE(pmdpcObj);
    return ps;
}


