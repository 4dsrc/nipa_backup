/*
 *  MTPHandlerDevice.c
 *
 *  Contains declaration for the device level  handler functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerPropUtil.h"
#include "MTPDevice.h"

static PSLSTATUS PSL_API _MTPDevicePrepareResetDevice(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);


static PSLSTATUS PSL_API _MTPDevicePrepareSelfTest(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);


static PSLSTATUS PSL_API _MTPDevicePreparePowerDown(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);


static PSLSTATUS PSL_API _MTPDevicePrepareSkip(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPDevicePrepareInitiateCapture(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPDevicePrepareInitiateOpenCapture(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPDevicePrepareTerminateOpenCapture(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);

/*
 *  MTPResetDevice
 *
 *  This handler function for ResetDevice MTP operation.
 *
 */
PSLSTATUS PSL_API MTPResetDevice(MTPDISPATCHER md, PSLMSG* pMsg)
{

    return MTPHandlePropOperations(md, pMsg,
                MTPHANDLERSTATE_RESPONSE,
                _MTPDevicePrepareResetDevice);
}

/*
 *  MTPSelfTest
 *
 *  This handler function for SelfTest MTP operation.
 *
 */
PSLSTATUS PSL_API MTPSelfTest(MTPDISPATCHER md, PSLMSG* pMsg)
{

    return MTPHandlePropOperations(md, pMsg,
                MTPHANDLERSTATE_RESPONSE,
                _MTPDevicePrepareSelfTest);
}

/*
 *  MTPPowerDown
 *
 *  This handler function for PowerDown MTP operation.
 *
 */
PSLSTATUS PSL_API MTPPowerDown(MTPDISPATCHER md, PSLMSG* pMsg)
{

    return MTPHandlePropOperations(md, pMsg,
                MTPHANDLERSTATE_RESPONSE,
                _MTPDevicePreparePowerDown);
}

/*
 *  MTPSkip
 *
 *  This handler function for Skip MTP operation.
 *
 */
PSLSTATUS PSL_API MTPSkip(MTPDISPATCHER md, PSLMSG* pMsg)
{

    return MTPHandlePropOperations(md, pMsg,
                MTPHANDLERSTATE_RESPONSE,
                _MTPDevicePrepareSkip);
}

/*
 *  MTPInitiateCapture
 *
 *  This handler function for InitiateCapture MTP operation.
 *
 */
PSLSTATUS PSL_API MTPInitiateCapture(MTPDISPATCHER md, PSLMSG* pMsg)
{

    return MTPHandlePropOperations(md, pMsg,
                MTPHANDLERSTATE_RESPONSE,
                _MTPDevicePrepareInitiateCapture);
}

/*
 *  MTPInitiateOpenCapture
 *
 *  This handler function for InitiateOpenCapture MTP operation.
 *
 */
PSLSTATUS PSL_API MTPInitiateOpenCapture(MTPDISPATCHER md, PSLMSG* pMsg)
{

    return MTPHandlePropOperations(md, pMsg,
                MTPHANDLERSTATE_RESPONSE,
                _MTPDevicePrepareInitiateOpenCapture);
}

/*
 *  MTPTerminateOpenCapture
 *
 *  This handler function for TerminateOpenCapture MTP operation.
 *
 */
PSLSTATUS PSL_API MTPTerminateOpenCapture(MTPDISPATCHER md, PSLMSG* pMsg)
{

    return MTPHandlePropOperations(md, pMsg,
                MTPHANDLERSTATE_RESPONSE,
                _MTPDevicePrepareTerminateOpenCapture);
}

/*
 * Utilities.
 */

PSLSTATUS PSL_API _MTPDevicePrepareResetDevice(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(0))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    /* Set this flag for response to notify transport to reset. */
    pData->opLevelData.msgResponse = MTPMSG_RESPONSE_RESET;

    status =  MTPDeviceInvokeCommand(MTPDEVICECOMMAND_RESETDEVICE,
                        PSLNULL, 0);

Exit:
    return status;
}

PSLSTATUS PSL_API _MTPDevicePrepareSelfTest(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;
    PSLUINT32           dwParam;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    dwParam = MTPLoadUInt32(&(pOpRequest->dwParam1));

    status =  MTPDeviceInvokeCommand(MTPDEVICECOMMAND_SELFTEST,
                        &dwParam, 1);

Exit:
    return status;
}


PSLSTATUS PSL_API _MTPDevicePreparePowerDown(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(0))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status =  MTPDeviceInvokeCommand(MTPDEVICECOMMAND_POWERDOWN,
                        PSLNULL, 0);

Exit:
    return status;
}

PSLSTATUS PSL_API _MTPDevicePrepareSkip(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;
    PSLUINT32           dwParam;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    dwParam = MTPLoadUInt32(&(pOpRequest->dwParam1));

    status =  MTPDeviceInvokeCommand(MTPDEVICECOMMAND_SKIP,
                            &dwParam, 1);

Exit:
    return status;
}

PSLSTATUS PSL_API _MTPDevicePrepareInitiateCapture(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;
    PSLUINT32           rgdwParam[2];

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(2))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgdwParam[0] = MTPLoadUInt32(&(pOpRequest->dwParam1));
    rgdwParam[1] = MTPLoadUInt32(&(pOpRequest->dwParam2));

    status =  MTPDeviceInvokeCommand(MTPDEVICECOMMAND_INITIATECAPTURE,
                            rgdwParam, 2);

Exit:
    return status;
}

PSLSTATUS PSL_API _MTPDevicePrepareInitiateOpenCapture(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;
    PSLUINT32           rgdwParam[2];

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(2))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    rgdwParam[0] = MTPLoadUInt32(&(pOpRequest->dwParam1));
    rgdwParam[1] = MTPLoadUInt32(&(pOpRequest->dwParam2));

    status =  MTPDeviceInvokeCommand(MTPDEVICECOMMAND_INITIATEOPENCAPTURE,
                            rgdwParam, 2);

Exit:
    return status;
}

PSLSTATUS PSL_API _MTPDevicePrepareTerminateOpenCapture(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;
    PSLUINT32           dwParam;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    dwParam = MTPLoadUInt32(&(pOpRequest->dwParam1));

    status =  MTPDeviceInvokeCommand(MTPDEVICECOMMAND_TERMINATEOPENCAPTURE,
                            &dwParam, 1);

Exit:
    return status;
}


