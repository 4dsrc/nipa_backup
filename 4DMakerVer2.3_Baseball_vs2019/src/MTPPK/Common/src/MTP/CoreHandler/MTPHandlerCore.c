/*
 *  MTPHandlerCore.c
 *
 *  Contains the implementation of core command handlers
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerUtil.h"
#include "MTPHandlerCore.h"
#include "MTPExtensions.h"
#include "MTPHandlerExtensions.h"

typedef struct COREHANDLERDATA
{
    BASEHANDLERDATA bhd;
}COREHANDLERDATA;

PSLSTATUS _MTPSessionInitialize(
                            MTPDISPATCHER md,
                            PSLUINT32 dwSessionID,
                            MTPCONTEXT* pmtpc);

PSLSTATUS _MTPSessionUninitialize(
                            MTPCONTEXT* pmtpc);

PSLSTATUS _FindSessionID(
                            PSLUINT32 dwSessionID,
                            PSLBOOL fInitialize);

PSLSTATUS _CacheSessionID(
                            PSLUINT32 dwSessionID);

static PSLDYNLIST      g_pdlSesIdList       = PSLNULL;
static PSLUINT32       g_pdwSesIdListRefCnt = 0;

static MTPDISPATCHER   g_mdDB               = PSLNULL;
static MTPDISPATCHER   g_mdDRM              = PSLNULL;
static PSLLOOKUPTABLE  g_ltPostOpenSession  = PSLNULL;

static PSLUINT32       g_dwmdDBRefCount     = 0;
static PSLUINT32       g_dwmdDRMRefCount    = 0;
static PSLUINT32       g_dwltRefCount       = 0;

/*
 *  MTPOpenSession
 *
 *  This handler function for OpenSession MTP operation.
 *
 */
PSLSTATUS PSL_API MTPOpenSession(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS        status;
    MTPCONTEXT*      pmc;
    PXMTPOPERATION   pOpRequest;
    COREHANDLERDATA* pData;
    PSLUINT16        wRespCode;
    PSLUINT32        dwSessionID;
    PSLMSG*          pRetMsg        = PSLNULL;
    PSLUINT32        msgID          = 0;
    PSLUINT32        dwMsgFlags     = 0;
    PSLUINT32        dwBufSize      = 0;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmc = (MTPCONTEXT*)(pMsg->aParam0);
    PSLASSERT(PSLNULL != pmc);

    /*ensure handler has enough room to store its data*/
    PSLASSERT(PSLNULL != pmc->mtpHandlerState.pbHandlerData);
    PSLASSERT(pmc->mtpHandlerState.cbHandlerData >=             \
                                        sizeof(COREHANDLERDATA));

    /* Allocate message to return to transport*/
    status = AllocateReturnMsg(md, &pRetMsg);
    if (PSL_FAILED (status))
    {
        goto Exit;
    }

    pData = (COREHANDLERDATA*)pmc->mtpHandlerState.pbHandlerData;

    switch (pMsg->msgID)
    {
        case MTPMSG_CMD_AVAILABLE:

            pOpRequest = (PXMTPOPERATION)pMsg->aParam2;

            PSLASSERT(PSLNULL != pOpRequest);

            wRespCode   = MTP_RESPONSECODE_OK;

            if (pMsg->alParam < MTPOPERATION_SIZE(1))
            {
                dwSessionID = 0;
            }
            else
            {
                dwSessionID = MTPLoadUInt32(&(pOpRequest->dwParam1));
            }

            /* If sessionID is 0x00000000 return invalid parameter */
            if (0 == dwSessionID)
            {
                wRespCode = MTP_RESPONSECODE_INVALIDPARAMETER;
            }
            else
            {
                /* Check if session ID already in use */
                status = _FindSessionID((PSLUINT32)pmc, PSLFALSE);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
                if (PSLSUCCESS == status)
                {
                    wRespCode = MTP_RESPONSECODE_SESSIONALREADYOPENED;
                }
                else
                {
                    PSLASSERT(PSLSUCCESS_NOT_FOUND == status);

                    status = _CacheSessionID((PSLUINT32)pmc);
                    /* we have crossed the limit of number of sessions
                     * we can support so return device busy to let
                     * initiator try again later
                     */
                    if (status == PSLERROR_INVALID_RANGE)
                    {
                        wRespCode = MTP_RESPONSECODE_DEVICEBUSY;
                    }
                }
            }

            if (MTP_RESPONSECODE_OK == wRespCode)
            {
                status = _MTPSessionInitialize(md, dwSessionID, pmc);
                if (PSL_FAILED (status))
                {
                    PSLTraceError(
                        "MTPOpenSession: _MTPSessionInitialize failed %d",
                        status);
                    wRespCode = MTP_RESPONSECODE_GENERALERROR;
                }
            }

            (PSLVOID)FillResponse(
                    &pData->bhd.opLevelData.mtpResponse,
                    wRespCode, MTPLoadUInt32(&(pOpRequest->dwTransactionID)),
                    PSLNULL, PSLNULL, PSLNULL, PSLNULL, PSLNULL);

            pData->bhd.opLevelData.dwNumRespParams = 0;

            /* read to send response*/
            pmc->mtpHandlerState.dwHandlerState =
                                               MTPHANDLERSTATE_RESPONSE;

            /* request a response buffer and return the sequence ID and
             * buffer that was recieved from CMD_AVAILABLE as
             * aParam1 and aParam2.
             */
            dwBufSize       = sizeof(MTPRESPONSE);
            dwMsgFlags      = MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
            msgID           = MTPMSG_CMD_CONSUMED;
            break;

        case MTPMSG_RESPONSE_BUFFER_AVAILABLE:
            PSLASSERT(MTPHANDLERSTATE_RESPONSE ==
                                   pmc->mtpHandlerState.dwHandlerState);

            PSLASSERT((PSLUINT32)pMsg->alParam >= sizeof(MTPRESPONSE));

            if (PSLNULL != (PSLVOID*)pMsg->aParam2)
            {
                /* copy response buffer to transport supplied buffer as
                * fourth parameter of input message
                */
                PSLCopyMemory((PSLVOID*)pMsg->aParam2,
                            &pData->bhd.opLevelData.mtpResponse,
                            (PSLUINT32)pMsg->alParam);

                /* amount of data filled in response buffer */
                dwBufSize   = sizeof(MTPRESPONSE) -
                        ((MTPRESPONSE_NUMPARAMS_MAX - \
                        pData->bhd.opLevelData.dwNumRespParams) * \
                        sizeof(PSLUINT32));

               /* assign post open-session lookuptable handle to
                * fifth parameter of message.
                */
                dwMsgFlags  =
                    (PSLPARAM)pData->bhd.sesLevelData.ltPostOpenSession;
                msgID       = MTPMSG_RESPONSE;
            }
            else
            {
                /* explicitly set the message ID to zero to prevent
                 * handler from trying to send any message to transport
                 */
                msgID       = 0;
                dwMsgFlags  = 0;
            }
            (PSLVOID)GetReadyForNextCommand(md, pmc);
            break;

        case MTPMSG_CANCEL:
        case MTPMSG_SHUTDOWN:
        case MTPMSG_DISCONNECT:
        case MTPMSG_RESET:
        case MTPMSG_ABORT:
            msgID = 0;

            status = MTPDispatcherRouteTerminate(md,
                        (MTPCONTEXT*)(pMsg->aParam0), pMsg->msgID);
            break;
    }

    if (0 != msgID)
    {
        status = FillMessage(pRetMsg, pMsg, msgID,
                            (PSLBYTE*)pMsg->aParam2,
                            dwBufSize, dwMsgFlags);
        if (PSL_FAILED (status))
        {
            goto Exit;
        }

        /* Send message */
        status = MTPRouteMsgToTransport(pmc, pRetMsg);
        if (PSL_FAILED (status))
        {
            goto Exit;
        }
        pRetMsg = PSLNULL;
    }

Exit:

    SAFE_PSLMSGFREE(pRetMsg);

    return status;
}

/*
 *  MTPCloseSession
 *
 *  This handler function for CloseSession MTP operation.
 *
 */
PSLSTATUS PSL_API MTPCloseSession(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS        status;
    MTPCONTEXT*      pmc;
    PXMTPOPERATION   pOpRequest;
    COREHANDLERDATA* pData;
    PSLUINT16        wRespCode;
    PSLUINT32        dwSessionID;
    PSLMSG*          pRetMsg        = PSLNULL;
    PSLUINT32        msgID          = 0;
    PSLUINT32        dwMsgFlags     = 0;
    PSLUINT32        dwBufSize      = 0;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmc = (MTPCONTEXT*)(pMsg->aParam0);
    PSLASSERT(PSLNULL != pmc);

    /*ensure handler has enough room to store its data*/
    PSLASSERT(PSLNULL != pmc->mtpHandlerState.pbHandlerData);
    PSLASSERT(pmc->mtpHandlerState.cbHandlerData >=                 \
                                            sizeof(COREHANDLERDATA));

    /* Allocate message to return to transport*/
    status = AllocateReturnMsg(md, &pRetMsg);
    if (PSL_FAILED (status))
    {
        goto Exit;
    }

    pData = (COREHANDLERDATA*)pmc->mtpHandlerState.pbHandlerData;

    switch (pMsg->msgID)
    {
        case MTPMSG_CMD_AVAILABLE:
            pOpRequest = (PXMTPOPERATION)pMsg->aParam2;
            PSLASSERT(PSLNULL != pOpRequest);

            if (pMsg->alParam < MTPOPERATION_SIZE(0))
            {
                wRespCode = MTP_RESPONSECODE_INVALIDPARAMETER;
            }
            else
            {
                wRespCode   = MTP_RESPONSECODE_OK;
                dwSessionID = pData->bhd.sesLevelData.dwSessionID;

                /* Check if session ID already in use and if so reset it */
                status = _FindSessionID((PSLUINT32)pmc, PSLTRUE);
                if (PSLSUCCESS_NOT_FOUND == status)
                {
                    wRespCode = MTP_RESPONSECODE_SESSIONNOTOPEN;
                }
            }

            if (MTP_RESPONSECODE_OK == wRespCode)
            {
                PSLASSERT(pmc->mtpContextState.hHandlerTable ==
                            pData->bhd.sesLevelData.ltPostOpenSession);
                /* In a single DB dispatcher scenario
                 * we could pass the global g_mdDB to
                 * _MTPSessionUninitialize, instead the mdDB stashsed
                 * in the context is used to make the code
                 * remain same for multi instance and single instance
                 * DB dispatcher scenario
                 */
                (PSLVOID)_MTPSessionUninitialize(
                                pmc);
            }

            (PSLVOID)FillResponse(
                    &pData->bhd.opLevelData.mtpResponse,
                    wRespCode, MTPLoadUInt32(&(pOpRequest->dwTransactionID)),
                    PSLNULL, PSLNULL, PSLNULL, PSLNULL, PSLNULL);

            pData->bhd.opLevelData.dwNumRespParams = 0;

            /* read to send response*/
            pmc->mtpHandlerState.dwHandlerState =
                                               MTPHANDLERSTATE_RESPONSE;

            /* request a response buffer and return the sequence ID and
             * buffer that was recieved from CMD_AVAILABLE as
             * aParam1 and aParam2.
             */
            dwBufSize       = sizeof(MTPRESPONSE);
            dwMsgFlags      = MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
            msgID           = MTPMSG_CMD_CONSUMED;
            break;

        case MTPMSG_RESPONSE_BUFFER_AVAILABLE:
            PSLASSERT(MTPHANDLERSTATE_RESPONSE ==
                                   pmc->mtpHandlerState.dwHandlerState);

            PSLASSERT((PSLUINT32)pMsg->alParam >= sizeof(MTPRESPONSE));

            if (PSLNULL != (PSLVOID*)pMsg->aParam2)
            {
                PSLCopyMemory((PSLVOID*)pMsg->aParam2,
                            &pData->bhd.opLevelData.mtpResponse,
                            (PSLUINT32)pMsg->alParam);

                /* amount of data filled in the response buffer*/
                dwBufSize   = sizeof(MTPRESPONSE) -
                        ((MTPRESPONSE_NUMPARAMS_MAX - \
                        pData->bhd.opLevelData.dwNumRespParams) * \
                        sizeof(PSLUINT32));

                /* copy the pre open-session lookuptable handle to
                * fifth parameter of message.
                */
                status = MTPLookupTablePreOpenGet(
                                      (PSLLOOKUPTABLE*)&dwMsgFlags);
                if (PSL_FAILED(status))
                {
                    goto Exit;
                }
                msgID       = MTPMSG_RESPONSE;
            }
            else
            {
                /* explicitly set the message ID to zero to prevent
                 * handler from trying to send any message to transport
                 */
                msgID       = 0;
                dwMsgFlags  = 0;
            }
            (PSLVOID)GetReadyForNextCommand(md, pmc);
            break;
    }

    if (0 != msgID)
    {
        status = FillMessage(pRetMsg, pMsg, msgID,
                            (PSLBYTE*)pMsg->aParam2,
                            dwBufSize, dwMsgFlags);
        if (PSL_FAILED (status))
        {
            goto Exit;
        }

        /* Send message to transport */
        status = MTPRouteMsgToTransport(pmc, pRetMsg);
        if (PSL_FAILED (status))
        {
            goto Exit;
        }
        pRetMsg = PSLNULL;
    }

Exit:

    SAFE_PSLMSGFREE(pRetMsg);

    return status;
}

PSLSTATUS PSL_API MTPCreateSessionIDList()
{
    PSLSTATUS status;
    /* singleton */
    if (PSLNULL == g_pdlSesIdList)
    {
        /* allocate an array to store the session IDs */

        status = PSLDynListCreate(MTP_MAX_SESSIONS, PSLTRUE, MTP_MAX_SESSIONS ?
                            MTP_MAX_SESSIONS : PSLDYNLIST_GROW_DEFAULT,
                            &g_pdlSesIdList);
        if (PSL_FAILED(status))
        {
            goto Exit;
        }
    }

    PSLAtomicIncrement(&g_pdwSesIdListRefCnt);

    status = PSLSUCCESS;
Exit:
    return status;
}

PSLVOID PSL_API MTPDestroySessionIDList()
{
    PSLUINT32 dwResult;

    dwResult = PSLAtomicDecrement(&g_pdwSesIdListRefCnt);
    if (0 == dwResult)
    {
        SAFE_PSLDYNLISTDESTROY(g_pdlSesIdList);
    }
}

PSLSTATUS PSL_API MTPForceCloseSession(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS status;
    MTPCONTEXT* pmc;
    COREHANDLERDATA* pData;
    PSLUINT32 dwSessionID;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmc = (MTPCONTEXT*)pMsg->aParam0;

    PSLASSERT(PSLNULL != pmc);

    pData = (COREHANDLERDATA*)pmc->mtpHandlerState.pbHandlerData;

    PSLASSERT(PSLNULL != pData);

    dwSessionID = pData->bhd.sesLevelData.dwSessionID;

    /* Check if session ID already in use and if so reset it */
    status = _FindSessionID((PSLUINT32)pmc, PSLTRUE);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = _MTPSessionUninitialize(pmc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}


PSLSTATUS _MTPSessionInitialize(MTPDISPATCHER md, PSLUINT32 dwSessionID,
                                MTPCONTEXT* pmtpc)
{
    PSLSTATUS           status;
    PSLLOOKUPTABLE      ltPostOpenSession;
    MTPDISPATCHER       mdDB;
    MTPDISPATCHER       mdDRM;
    PSLMSGQUEUE         mqDB;
    PSLMSGQUEUE         mqDRM;
    PSLMSGQUEUE         mqCore;
    COREHANDLERDATA*    pData;
    MTPSERVICERESOLVER  msrServices = PSLNULL;

    if (PSLNULL == pmtpc)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPDispatcherGetMsgQueue(md, &mqCore);
    if (PSL_FAILED (status))
    {
        goto Exit;
    }

    /* Singleton, make sure to increment refernce count that
     * way we can release it in the close session when ref count
     * is zero
     */
    if (PSLNULL == g_mdDRM)
    {
        /* create a dispatcher for DRM command handlers */
        status = MTPDispatcherCreate(MTPDISPATCHER_MSGCOUNT,
                            MTPOnCommandDispatch, &mdDRM);
        if (PSL_FAILED(status))
        {
            PSLTraceError(
                "_MTPSessionInitialize: DRM MTPDispatcherCreate failed %d",
                status);
            goto Exit;
        }
        g_mdDRM = mdDRM;
    }
    else
    {
        mdDRM = g_mdDRM;
    }
    (PSLVOID)PSLAtomicIncrement(&g_dwmdDRMRefCount);

    /* Singleton if we decide to have one DB dispatcher,
     * make sure to increment refernce count that way we can
     * release it in the close session when ref count is zero
     */
    /* In the future if we decide to go with multi-DB dispatcher
     * in a multi-session scenario we should remove this check
     * and instead create a sperate instance each time and at that
     * time there is not need for ref count.
     */
    if (PSLNULL == g_mdDB)
    {
        /* create a dispatcher for DB command handlers */
        status = MTPDispatcherCreate(MTPDISPATCHER_MSGCOUNT,
                            MTPOnCommandDispatch, &mdDB);
        if (PSL_FAILED(status))
        {
            PSLTraceError(
                "_MTPSessionInitialize: DB MTPDispatcherCreate failed %d",
                status);
            goto Exit;
        }
        g_mdDB = mdDB;
    }
    else
    {
        mdDB = g_mdDB;
    }
    (PSLVOID)PSLAtomicIncrement(&g_dwmdDBRefCount);

    /* get the queue coresponding to core dispatcher */
    status = MTPDispatcherGetMsgQueue(mdDB, &mqDB);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* get the queue coresponding to core dispatcher */
    status = MTPDispatcherGetMsgQueue(mdDRM, &mqDRM);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    /* Singleton if we decide to have one DB dispatcher,
     * make sure to increment reference count that way we can
     * release it in the close session when ref count is zero
     */
    /* In the future if we decide to go with multi-DB dispatcher
     * in a multi-session scenario we should remove this check
     * and instead create a sperate instance each time and at that
     * time there is no need for ref count.
     */
    if (PSLNULL == g_ltPostOpenSession)
    {
        status = MTPPostOpenLookupTableInit(mqCore, mqDB, mqDRM,
                                         &g_ltPostOpenSession);
        if (PSL_FAILED(status))
        {
            PSLTraceError(
                "_MTPSessionInitialize: MTPPostOpenLookupTableInit failed %d",
                status);
            goto Exit;
        }
    }

    ltPostOpenSession = g_ltPostOpenSession;

    (PSLVOID)PSLAtomicIncrement(&g_dwltRefCount);

    pData = (COREHANDLERDATA*)pmtpc->mtpHandlerState.pbHandlerData;
    PSLASSERT( PSLNULL != pData);

    PSLASSERT(PSLNULL == pData->bhd.sesLevelData.msrServices);
    status = MTPServiceResolverCreate(pmtpc, &msrServices);
    if (PSL_FAILED (status))
    {
        PSLTraceError(
                "_MTPSessionInitialize: MTPDBSessionOpen failed %d",
                status);
        goto Exit;
    }


    /* stash DB dispatcher, DB session and lookup table,
     * stashing DB dispatcher is not required in a single
     * DB dispatcher scenario but is done to make handler code
     * remain the same for multi dispatcher and single dispatcher
     * scenarios.
     */

    pData->bhd.sesLevelData.dwSessionID = dwSessionID;
    pData->bhd.sesLevelData.mdDB = mdDB;
    pData->bhd.sesLevelData.msrServices = msrServices;
    pData->bhd.sesLevelData.ltPostOpenSession = ltPostOpenSession;
    msrServices = PSLNULL;

    status = MTPServicesInitialize(pmtpc);
    if (PSL_FAILED (status))
    {
        PSLTraceError(
                "_MTPSessionInitialize: MTPServicesInitialize failed %d",
                status);
        goto Exit;
    }

Exit:
    SAFE_MTPSERVICERESOLVERDESTROY(msrServices);
    return status;
}

PSLSTATUS _MTPSessionUninitialize(MTPCONTEXT* pmc)
{
    PSLUINT32 dwResult;
    COREHANDLERDATA* pData;

    PSLASSERT(PSLNULL != pmc);
    pData = (COREHANDLERDATA*)pmc->mtpHandlerState.pbHandlerData;

    /* global lookup table handle is not use to keep
     * the implementation same for multi DB dispatcher
     * and single DB dispatcher scenarios.
     */
    /*
     * the ref count should be removed in a
     * multi DB dispatcher scenario.
     */
    dwResult = PSLAtomicDecrement(&g_dwltRefCount);
    if (0 == dwResult)
    {
        MTPPostOpenLookupTableUninit(g_ltPostOpenSession);
        g_ltPostOpenSession = PSLNULL;
    }

    /* global DB dispatcher handle is not use to keep
     * the implementation same for multi DB dispatcher
     * and single DB dispatcher scenarios.
     */
    /*
     * the ref count should be removed in a
     * multi DB dispatcher scenario.
     */
    dwResult = PSLAtomicDecrement(&g_dwmdDBRefCount);
    if (0 == dwResult)
    {
        SAFE_MTPDISPATCHERDESTROY(g_mdDB);
    }

    dwResult = PSLAtomicDecrement(&g_dwmdDRMRefCount);
    if (0 == dwResult)
    {
        SAFE_MTPDISPATCHERDESTROY(g_mdDRM);
    }

    SAFE_MTPSERVICERESOLVERDESTROY(pData->bhd.sesLevelData.msrServices);

    pData->bhd.sesLevelData.dwSessionID = 0;
    pData->bhd.sesLevelData.mdDB = PSLNULL;
    return PSLSUCCESS;
}

PSLSTATUS _FindSessionID(PSLUINT32 dwSessionID, PSLBOOL fRemove)
{
    PSLSTATUS ps;
    PSLUINT32 idxMatch;

    ps = PSLDynListFindItem(g_pdlSesIdList, 0, dwSessionID, PSLNULL, &idxMatch);
    if (PSLSUCCESS != ps)
    {
        goto Exit;
    }

    if (fRemove)
    {
        ps = PSLDynListRemoveItem(g_pdlSesIdList, idxMatch, 1);
        if (PSL_FAILED(ps))
        {
            goto Exit;
        }
    }

Exit:
    return ps;
}

PSLSTATUS _CacheSessionID(PSLUINT32 dwSessionID)
{
    PSLSTATUS ps;
    PSLUINT32 cItems;

    /*  Make sure there is enough space for the new item
     */

    ps = PSLDynListGetSize(g_pdlSesIdList, &cItems);
    if (MTP_MAX_SESSIONS && (cItems == MTP_MAX_SESSIONS))
    {
        ps = PSLERROR_INVALID_RANGE;
        goto Exit;
    }

    /*  And add it to the list
     */

    ps = PSLDynListAddItem(g_pdlSesIdList, dwSessionID, PSLNULL);
    if (PSL_FAILED(ps))
    {
        goto Exit;
    }

Exit:
    return ps;
}
