/*
 *  MTPHandlerProp.c
 *
 *  Contains declaration for the device level  handler functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerProp.h"
#include "MTPHandlerPropUtil.h"
#include "MTPExtensions.h"
#include "MTPHandlerExtensions.h"

enum
{
    MTPPROPOPERATION_SET = 0,
    MTPPROPOPERATION_GET,
    MTPPROPOPERATION_RESET,
    MTPPROPOPERATION_GETDESC,
};

static PSLINT32 PSL_API _ComparePropCode(PSL_CONST PSLVOID* pvKey,
                            PSL_CONST PSLVOID* pvCompare);

static PSLSTATUS PSL_API _DevicePropResetEnumProc(PSLPARAM aParamKey,
                            PSLPARAM aParamValue,
                            PSLPARAM aParamData,
                            PSLPARAM aParamEnum);

static PSLSTATUS PSL_API _MTPPropContextPrepareDevicePropDesc(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPPropContextPrepareDevicePropValueGet(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPPropContextPrepareDevicePropValueSet(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPPropContextPrepareDeviceValueReset(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);

static PSLSTATUS PSL_API _MTPPropContextPrepareDeviceValueResetAll(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize);

static PSLSTATUS _MTPRouteDevicePropOps(MTPDISPATCHER md,
                            PSLMSG* pMsg,
                            PSLUINT32 dwOpType);

static PSLSTATUS PSL_API _MTPGetCoreDevicePropDesc(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

static PSLSTATUS PSL_API _MTPGetCoreDevicePropValue(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

static PSLSTATUS PSL_API _MTPSetCoreDevicePropValue(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

static PSLSTATUS PSL_API _MTPResetCoreDevicePropValue(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);

static PSLSTATUS PSL_API _MTPResetAllDevicePropValue(
                            MTPDISPATCHER md,
                            PSLMSG* pMsg);


/*
 *  _MTPPropContextPrepareDevicePropDesc
 *
 *  Utility for GetDevicePropDesc.
 *
 */

PSLSTATUS PSL_API _MTPPropContextPrepareDevicePropDesc(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;
    PSLUINT32           dwPropCode;
    MTPDEVICEPROPCONTEXT   mdpc = PSLNULL;
    PSLUINT64           cbSize;

    PFNMTPDEVICEPROPCONTEXTOPEN pfnMTPDevicePropContextOpen;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    pfnMTPDevicePropContextOpen =
                        ((PROPHANDLERDATA*)pData)->pfnMTPDevicePropContextOpen;

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (PSLNULL == pfnMTPDevicePropContextOpen)
    {
        status = MTPERROR_PROPERTY_INVALID_DEVICEPROPCODE;
        goto Exit;
    }

    dwPropCode = MTPLoadUInt32(&(pOpRequest->dwParam1));
    status = pfnMTPDevicePropContextOpen(dwPropCode,
                            MTPDBCONTEXTDATAFORMAT_DEVICEPROPDESC,
                            ((PROPHANDLERDATA*)pData)->aData,
                            &mdpc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDevicePropContextSerialize(mdpc, PSLNULL, 0, PSLNULL, &cbSize);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((PROPHANDLERDATA*)pData)->mpc = mdpc;
    mdpc = PSLNULL;

    *pqwBufSize = cbSize;

    status = PSLSUCCESS;

Exit:
    SAFE_MTPDEVICEPROPCONTEXTCLOSE(mdpc);

    return status;
}

/*
 *  _MTPPropContextPrepareDevicePropValue
 *
 *  Utility for GetDevicePropValue.
 *
 */

PSLSTATUS PSL_API _MTPPropContextPrepareDevicePropValueGet(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;
    PSLUINT32           dwPropCode;
    MTPDEVICEPROPCONTEXT   mdpc = PSLNULL;
    PSLUINT64           cbSize;

    PFNMTPDEVICEPROPCONTEXTOPEN pfnMTPDevicePropContextOpen;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    pfnMTPDevicePropContextOpen =
                        ((PROPHANDLERDATA*)pData)->pfnMTPDevicePropContextOpen;

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        PSLNULL == pqwBufSize || cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (PSLNULL == pfnMTPDevicePropContextOpen)
    {
        status = MTPERROR_PROPERTY_INVALID_DEVICEPROPCODE;
        goto Exit;
    }

    dwPropCode = MTPLoadUInt32(&(pOpRequest->dwParam1));
    status =  pfnMTPDevicePropContextOpen(dwPropCode,
                            MTPDBCONTEXTDATAFORMAT_DEVICEPROPLIST,
                            ((PROPHANDLERDATA*)pData)->aData,
                            &mdpc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDevicePropContextSerialize(mdpc, PSLNULL, 0, PSLNULL, &cbSize);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((PROPHANDLERDATA*)pData)->mpc = mdpc;
    mdpc = PSLNULL;

    *pqwBufSize = cbSize;

    status = PSLSUCCESS;

Exit:
    SAFE_MTPDEVICEPROPCONTEXTCLOSE(mdpc);

    return status;
}

/*
 *  _MTPPrepContextPropareDevicePropValueSet
 *
 *  Utility for SetDevicePropValue.
 *
 */

PSLSTATUS PSL_API _MTPPropContextPrepareDevicePropValueSet(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;
    PSLUINT32           dwPropCode;
    MTPDEVICEPROPCONTEXT   mdpc = PSLNULL;

    PFNMTPDEVICEPROPCONTEXTOPEN pfnMTPDevicePropContextOpen;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    pfnMTPDevicePropContextOpen =
                        ((PROPHANDLERDATA*)pData)->pfnMTPDevicePropContextOpen;

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        cbOpRequest < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (PSLNULL == pfnMTPDevicePropContextOpen)
    {
        status = MTPERROR_PROPERTY_INVALID_DEVICEPROPCODE;
        goto Exit;
    }

    dwPropCode = MTPLoadUInt32(&(pOpRequest->dwParam1));
    status = pfnMTPDevicePropContextOpen(dwPropCode,
                            MTPDBCONTEXTDATAFORMAT_DEVICEPROPLIST,
                            ((PROPHANDLERDATA*)pData)->aData,
                            &mdpc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((PROPHANDLERDATA*)pData)->mpc = mdpc;
    mdpc = PSLNULL;

    status = PSLSUCCESS;

Exit:
    SAFE_MTPDEVICEPROPCONTEXTCLOSE(mdpc);

    return status;
}

/*
 *  _MTPPropContextPrepareDeviceValueReset
 *
 *  Utility for ResetDevicePropValue.
 *
 */

PSLSTATUS PSL_API _MTPPropContextPrepareDeviceValueReset(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS           status;
    PSLUINT32           dwPropCode;
    MTPDEVICEPROPCONTEXT   mdpc = PSLNULL;

    PFNMTPDEVICEPROPCONTEXTOPEN pfnMTPDevicePropContextOpen;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    pfnMTPDevicePropContextOpen =
                        ((PROPHANDLERDATA*)pData)->pfnMTPDevicePropContextOpen;

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        cbOpRequest < MTPOPERATION_SIZE(1))

    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if (PSLNULL == pfnMTPDevicePropContextOpen)
    {
        status = MTPERROR_PROPERTY_INVALID_DEVICEPROPCODE;
        goto Exit;
    }

    dwPropCode = MTPLoadUInt32(&(pOpRequest->dwParam1));

    status = pfnMTPDevicePropContextOpen(dwPropCode,
                            MTPDBCONTEXTDATAFORMAT_DEVICEPROPLIST,
                            ((PROPHANDLERDATA*)pData)->aData,
                            &mdpc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDevicePropContextReset(mdpc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDevicePropContextCommit(mdpc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    ((PROPHANDLERDATA*)pData)->mpc = mdpc;
    mdpc = PSLNULL;

Exit:
    SAFE_MTPDEVICEPROPCONTEXTCLOSE(mdpc);

    return status;
}


/*
 *  _DevicePropResetEnumProc
 *
 *  LookupTable enumeration procedure that resets each valid property in
 *  the table
 *
 */

PSLSTATUS PSL_API _DevicePropResetEnumProc(PSLPARAM aParamKey,
                    PSLPARAM aParamValue, PSLPARAM aParamData,
                    PSLPARAM aParamEnum)
{
    PSLSTATUS status;
    MTPDEVICEPROPCONTEXT mdpc = PSLNULL;

    PFNMTPDEVICEPROPCONTEXTOPEN pfnMTPDevicePropContextOpen;

    pfnMTPDevicePropContextOpen = (PFNMTPDEVICEPROPCONTEXTOPEN)aParamValue;
    if (PSLNULL == pfnMTPDevicePropContextOpen)
    {
        status = MTPERROR_PROPERTY_INVALID_DEVICEPROPCODE;
        goto Exit;
    }

    status = pfnMTPDevicePropContextOpen(aParamKey,
                            MTPDBCONTEXTDATAFORMAT_DEVICEPROPLIST,
                            aParamData, &mdpc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

    status = MTPDevicePropContextReset(mdpc);
    if (PSL_FAILED(status))
    {
        /*  Remap not implemented to success so that the enumeration continues
         */

        status = (PSLERROR_NOT_IMPLEMENTED == status) ? PSLSUCCESS : status;
        goto Exit;
    }

    status = MTPDevicePropContextCommit(mdpc);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    SAFE_MTPDEVICEPROPCONTEXTCLOSE(mdpc);
    return status;
    aParamEnum;
}


/*
 *  _MTPPropContextPrepareDeviceValueResetAll
 *
 *  Utility for ResetDevicePropValue.
 *
 */

PSLSTATUS PSL_API _MTPPropContextPrepareDeviceValueResetAll(
                            PXMTPOPERATION   pOpRequest,
                            PSLUINT64        cbOpRequest,
                            BASEHANDLERDATA* pData,
                            PSLUINT64*       pqwBufSize)
{
    PSLSTATUS       status;
    PSLLOOKUPTABLE  plt;

    if (PSLNULL != pqwBufSize)
    {
        *pqwBufSize = 0;
    }

    if (PSLNULL == pOpRequest || PSLNULL == pData ||
        cbOpRequest < MTPOPERATION_SIZE(1))

    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    status = MTPLookupTableDevicePropGet(&plt);
    if (PSL_FAILED(status))
    {
        PSLASSERT(PSLFALSE);
        goto Exit;
    }

    status = PSLLookupTableEnum(plt, _DevicePropResetEnumProc, PSLNULL);
    if (PSL_FAILED(status))
    {
        goto Exit;
    }

Exit:
    return status;
}

/*  This handler function for GetDevicePropDesc MTP operation.
 *  It will look up the handler function to handle the request.
 */
PSLSTATUS PSL_API MTPGetDevicePropDesc(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return _MTPRouteDevicePropOps(md, pMsg, MTPPROPOPERATION_GETDESC);
}

/*  This handler function for GetDevicePropValue MTP operation.
 *  It will look up the handler function to handle the request.
 */
PSLSTATUS PSL_API MTPGetDevicePropValue(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return _MTPRouteDevicePropOps(md, pMsg, MTPPROPOPERATION_GET);
}

/*  This handler function for SetDevicePropValue MTP operation.
 *  It will look up the handler function to handle the request.
 */
PSLSTATUS PSL_API MTPSetDevicePropValue(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return _MTPRouteDevicePropOps(md, pMsg, MTPPROPOPERATION_SET);
}

/*  This handler function for ResetDevicePropValue MTP operation.
 *  It will look up the handler function to handle the request.
 */
PSLSTATUS PSL_API MTPResetDevicePropValue(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return _MTPRouteDevicePropOps(md, pMsg, MTPPROPOPERATION_RESET);
}

/*
 *  MTPGetInterdependentPropDesc
 *
 *  This handler function for GetInterdependentPropDesc MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetInterdependentPropDesc(MTPDISPATCHER md, PSLMSG* pMsg)
{
md;
pMsg;
    PSLASSERT(PSLFALSE);
    return PSLERROR_NOT_IMPLEMENTED;
}

PSLSTATUS _MTPRouteDevicePropOps(MTPDISPATCHER md, PSLMSG* pMsg,
                                            PSLUINT32 dwOpType)
{
    PSLSTATUS               status;
    PXMTPOPERATION          pOpRequest;
    MTPCONTEXT*             pmc;
    PROPHANDLERDATA*        pHandlerData;
    PSLPARAM                aValue = PSLNULL;
    PSLPARAM                aData = PSLNULL;
    PSLUINT32               dwPropCode;
    PSLLOOKUPTABLE          plt;
    PFNMTPMSGDISPATCH       pfnMsgDispatch;
    PSLUINT16               wOpCode;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    if(MTPMSG_CMD_AVAILABLE != pMsg->msgID)
    {
        status = PSLSUCCESS_FALSE;
        goto Exit;
    }

    pOpRequest = (PXMTPOPERATION)pMsg->aParam2;

    if (PSLNULL == pOpRequest)
    {
        status = PSLSUCCESS_FALSE;
        goto Exit;
    }

    /* One parameter is expected, if not present error */
    if (pMsg->alParam < MTPOPERATION_SIZE(1))
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    dwPropCode = MTPLoadUInt32(&(pOpRequest->dwParam1));
    wOpCode = MTPLoadUInt16(&(pOpRequest->wOpCode));

    /* ResetDevicePropValue can have Prop code for
     * all properties in which case the handler
     * cannot be found in the lookup table
     */
    if (MTP_DEVICEPROPCODE_ALL == (PSLUINT16)dwPropCode &&
        MTP_OPCODE_RESETDEVICEPROPVALUE == wOpCode)
    {
        pfnMsgDispatch = _MTPResetAllDevicePropValue;
    }
    else
    {
        status = MTPLookupTableDevicePropGet(&plt);
        if (PSL_FAILED(status))
        {
            PSLASSERT(PSLFALSE);
            goto Exit;
        }

        status = PSLLookupTableFindEntry(plt, _ComparePropCode,
                                         dwPropCode, &aValue, &aData);
        if (PSL_FAILED(status))
        {
            PSLASSERT(PSLFALSE);
            goto Exit;
        }

        switch(dwOpType)
        {
        case MTPPROPOPERATION_SET:
            pfnMsgDispatch = _MTPSetCoreDevicePropValue;
            break;
        case  MTPPROPOPERATION_GET:
            pfnMsgDispatch = _MTPGetCoreDevicePropValue;
            break;
        case  MTPPROPOPERATION_RESET:
            pfnMsgDispatch = _MTPResetCoreDevicePropValue;
            break;
        case  MTPPROPOPERATION_GETDESC:
            pfnMsgDispatch = _MTPGetCoreDevicePropDesc;
            break;
        default:
            PSLASSERT(PSLFALSE);
            status = PSLERROR_UNEXPECTED;
            goto Exit;
        }
    }

    PSLASSERT(PSLNULL != pfnMsgDispatch);

    pmc = (MTPCONTEXT*)(pMsg->aParam0);
    ASSERT(PSLNULL != pmc);

    pHandlerData = (PROPHANDLERDATA*)
                        pmc->mtpHandlerState.pbHandlerData;
    ASSERT(PSLNULL != pHandlerData);

    pHandlerData->mpc = PSLNULL;
    pHandlerData->bhd.msgDispatch.pfnMsgDispatch = pfnMsgDispatch;
    pHandlerData->pfnMTPDevicePropContextOpen =
                            (PFNMTPDEVICEPROPCONTEXTOPEN)aValue;
    pHandlerData->aData = aData;
    status = pfnMsgDispatch(md, pMsg);
    if (PSL_FAILED(status))
    {
        PSLASSERT(PSLFALSE);
        goto Exit;
    }

Exit:
    return status;
}

/*  This handler function for GetDevicePropDesc MTP operation.
 *  for core device properties.
 */
PSLSTATUS PSL_API _MTPGetCoreDevicePropDesc(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandlePropOperations(md, pMsg,
                MTPHANDLERSTATE_DATA_OUT,
                _MTPPropContextPrepareDevicePropDesc);
}

/*  This handler function for GetDevicePropValue MTP operation
 *  for core device properties.
 */
PSLSTATUS PSL_API _MTPGetCoreDevicePropValue(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandlePropOperations(md, pMsg,
                            MTPHANDLERSTATE_DATA_OUT,
                            _MTPPropContextPrepareDevicePropValueGet);
}

/*  This handler function for SetDevicePropValue MTP operation.
 *  for core device properties.
 */
PSLSTATUS PSL_API _MTPSetCoreDevicePropValue(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandlePropOperations(md, pMsg,
                MTPHANDLERSTATE_DATA_IN,
                _MTPPropContextPrepareDevicePropValueSet);
}

/*  This handler function for ResetDevicePropValue MTP operation.
 *  for core device properties.
 */
PSLSTATUS PSL_API _MTPResetCoreDevicePropValue(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandlePropOperations(md, pMsg,
                MTPHANDLERSTATE_RESPONSE,
                _MTPPropContextPrepareDeviceValueReset);
}

/*  This handler function for ResetDevicePropValue MTP operation.
 *  for all device properties.
 */
PSLSTATUS PSL_API _MTPResetAllDevicePropValue(MTPDISPATCHER md, PSLMSG* pMsg)
{
    return MTPHandlePropOperations(md, pMsg,
                MTPHANDLERSTATE_RESPONSE,
                _MTPPropContextPrepareDeviceValueResetAll);
}

/*
 *  _ComparePropCode
 *
 *  Helper function used to compare two OpCodes
 *
 */
PSLINT32 PSL_API _ComparePropCode(PSL_CONST PSLVOID* pvKey,
                        PSL_CONST PSLVOID* pvCompare)
{
    return ((PSLUINT32)pvKey - ((PSLLOOKUPRECORD*)pvCompare)->pvKey);
}
