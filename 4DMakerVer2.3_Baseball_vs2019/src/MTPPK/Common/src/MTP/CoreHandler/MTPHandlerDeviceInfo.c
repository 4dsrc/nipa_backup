/*
 *  MTPHandlerDeviceInfo.c
 *
 *  Contains the implementation of core command handlers
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "MTPPrecomp.h"
#include "MTPHandlerUtil.h"
#include "MTPHandlerDeviceInfo.h"

/*
 *  MTPGetDeviceInfo
 *
 *  This handler function for GetDeviceInfo MTP operation.
 *
 */
PSLSTATUS PSL_API MTPGetDeviceInfo(MTPDISPATCHER md, PSLMSG* pMsg)
{
    PSLSTATUS           status;
    MTPCONTEXT*         pmc;
    PXMTPOPERATION      pOpRequest;
    BASEHANDLERDATA*    pData;
    PSLUINT16           wRespCode;
    PSLMSG*             pRetMsg         = PSLNULL;
    PSLUINT32           msgID           = 0;
    PSLUINT32           dwMsgFlags      = 0;
    PSLUINT32           dwBufSize       = 0;
    PSLUINT32           dwDataSize      = 0;
    PSLUINT32           dwRemainingSize = 0;

    if (PSLNULL == md || PSLNULL == pMsg)
    {
        status = PSLERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pmc = (MTPCONTEXT*)(pMsg->aParam0);
    PSLASSERT(PSLNULL != pmc);

    /*ensure handler has enough room to store its data*/
    PSLASSERT(PSLNULL != pmc->mtpHandlerState.pbHandlerData);
    PSLASSERT(pmc->mtpHandlerState.cbHandlerData >=                  \
                                              sizeof(BASEHANDLERDATA));

    pData = (BASEHANDLERDATA*)pmc->mtpHandlerState.pbHandlerData;

    /* Allocate message to return to transport*/
    status = AllocateReturnMsg(md, &pRetMsg);
    if (PSL_FAILED (status))
    {
        goto Exit;
    }

    switch (pMsg->msgID)
    {
        case MTPMSG_CMD_AVAILABLE:
        {
            /* this msg is expected whenever a MTPRouteBegin is called.
             */

            wRespCode   = MTP_RESPONSECODE_OK;

            pOpRequest = (PXMTPOPERATION)pMsg->aParam2;
            PSLASSERT(PSLNULL != pOpRequest);

            if (pMsg->alParam < MTPOPERATION_SIZE(0))
            {
                wRespCode = MTP_RESPONSECODE_INVALIDPARAMETER;
            }
            else if (0 == pData->sesLevelData.dwSessionID &&
                0 != MTPLoadUInt32(&(pOpRequest->dwTransactionID)))
            {
                /* If session ID is 0x00000000 and transaction ID
                 * is non-zero return error
                 */
                wRespCode = MTP_RESPONSECODE_PARAMETERNOTSUPPORTED;
            }

            if (MTP_RESPONSECODE_OK == wRespCode)
            {
                /* get the size of device info structure */
                status = MTPGetDeviceInfoSerialize(
                                PSLNULL, 0, PSLNULL, &dwBufSize);
                if(PSL_FAILED(status))
                {
                    /* send general error response back to initiator */
                    wRespCode = MTP_RESPONSECODE_GENERALERROR;
                }
            }

            if (MTP_RESPONSECODE_OK == wRespCode)
            {
                /* request for a buffer to send data */
                pmc->mtpHandlerState.dwHandlerState =
                                            MTPHANDLERSTATE_DATA_OUT;
                dwMsgFlags = MTPMSGFLAG_DATA_BUFFER_REQUEST |
                                                MTPMSGFLAG_COMPLETE;
                PSLASSERT(0 != dwBufSize);
            }
            else
            {
                /* request for a response buffer on failure */
                pmc->mtpHandlerState.dwHandlerState =
                                          MTPHANDLERSTATE_RESPONSE;
                dwMsgFlags = MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
                dwBufSize  = sizeof(MTPRESPONSE);
            }

            /* stash the response*/
            (PSLVOID)FillResponse(
                    &pData->opLevelData.mtpResponse,
                    wRespCode, MTPLoadUInt32(&(pOpRequest->dwTransactionID)),
                    PSLNULL, PSLNULL, PSLNULL, PSLNULL, PSLNULL);

            /* This command doesn't have response parameters. */
            pData->opLevelData.dwNumRespParams = 0;

            msgID = MTPMSG_CMD_CONSUMED;
        }
        break;

        case MTPMSG_DATA_BUFFER_AVAILABLE:
        {
            /* this msg is expected after we send out a msg with flag
             * MTPMSGFLAG_DATA_BUFFER_REQUEST.
             */
            PSLASSERT(MTPHANDLERSTATE_DATA_OUT ==
                               pmc->mtpHandlerState.dwHandlerState);

            PSLASSERT(MTP_RESPONSECODE_OK ==
                            pData->opLevelData.mtpResponse.wRespCode);
            wRespCode = MTP_RESPONSECODE_OK;

            if (PSLNULL != (PSLVOID*)pMsg->aParam2)
            {
                dwDataSize = (PSLUINT32)pMsg->alParam;

                /* copy deviceinfo to the buffer from transport */
                status = MTPGetDeviceInfoSerialize(
                                (PSLBYTE*)pMsg->aParam2,
                                (PSLUINT32)pMsg->alParam,
                                &dwDataSize, &dwRemainingSize);
                if (PSL_FAILED(status))
                {
                    /* request for a response buffer on failure */
                    pmc->mtpHandlerState.dwHandlerState =
                                            MTPHANDLERSTATE_RESPONSE;
                    dwMsgFlags  = MTPMSGFLAG_RESPONSE_BUFFER_REQUEST |
                                    MTPMSGFLAG_TERMINATE;
                    dwBufSize = 0;

                    wRespCode = MTP_RESPONSECODE_GENERALERROR;
                }
                else
                {
                    if (0 == dwRemainingSize )
                    {
                        /* if the data remaining is zero or if the buffer
                         * size from transport is big enough to hold the
                         * entire data we are done with data phase and
                         * ready to send response
                         */
                        PSLASSERT(dwDataSize <=
                                            (PSLUINT32)pMsg->alParam);

                        pmc->mtpHandlerState.dwHandlerState =
                                              MTPHANDLERSTATE_RESPONSE;
                        dwMsgFlags  = MTPMSGFLAG_TERMINATE|
                                    MTPMSGFLAG_RESPONSE_BUFFER_REQUEST;
                    }
                    else
                    {
                        /* request addtional data buffer, the size
                         * of the buffer would still be the same as
                         * the size of initial buffer requested.
                         */
                        PSLASSERT(0 != dwRemainingSize);

                        pmc->mtpHandlerState.dwHandlerState =
                                              MTPHANDLERSTATE_DATA_OUT;
                        dwMsgFlags  = MTPMSGFLAG_DATA_BUFFER_REQUEST;
                    }

                    PSLASSERT(0 != dwDataSize);

                    /* amount of data filled in the buffer */
                    dwBufSize = dwDataSize;
                }

                pData->opLevelData.mtpResponse.wRespCode = wRespCode;
                msgID = MTPMSG_DATA_READY_TO_SEND;
            }
            else
            {
                /* explicitly set the message ID to zero to prevent
                 * handler from trying to send any message to transport
                 */
                PSLASSERT(0 == msgID);

                /* if the buffer provided by the tranport is PSLNULL
                 * that means it is in a state where it is not able to
                 * provide any buffer in which case we should cleanup
                 * everything and get to a state to handler next command
                 */
                (PSLVOID)GetReadyForNextCommand(md, pmc);
            }
        }
        break;

        case MTPMSG_RESPONSE_BUFFER_AVAILABLE:
        {
            /* this msg is expected after we send out a msg with flag
             * MTPMSGFLAG_RESPONSE_BUFFER_REQUEST.
             */

            PSLASSERT(MTPHANDLERSTATE_RESPONSE ==
                                pmc->mtpHandlerState.dwHandlerState);

            PSLASSERT((PSLUINT32)pMsg->alParam >=
                                            sizeof(MTPRESPONSE));

            if (PSLNULL != (PSLVOID*)pMsg->aParam2)
            {
                PSLCopyMemory((PSLVOID*)pMsg->aParam2,
                                &pData->opLevelData.mtpResponse,
                                (PSLUINT32)pMsg->alParam);

                /* amount of data filled in response buffer */
                dwBufSize = sizeof(MTPRESPONSE) -
                        ((MTPRESPONSE_NUMPARAMS_MAX - \
                        pData->opLevelData.dwNumRespParams) * \
                        sizeof(PSLUINT32));

                msgID     = MTPMSG_RESPONSE;
            }
            else
            {
                /* explicitly set the message ID to zero to prevent
                 * handler from trying to send any message to transport
                 */
                PSLASSERT(0 == msgID);
            }

            (PSLVOID)GetReadyForNextCommand(md, pmc);
        }
        break;

        case MTPMSG_DATA_SENT:
        {
            /* do nothing */
            PSLASSERT(0 == msgID);
        }
        break;

        case MTPMSG_CANCEL:
        case MTPMSG_SHUTDOWN:
        case MTPMSG_DISCONNECT:
        case MTPMSG_RESET:
        case MTPMSG_ABORT:
        msgID = 0;

        status = MTPDispatcherRouteTerminate(md,
                    (MTPCONTEXT*)(pMsg->aParam0), pMsg->msgID);
        break;

        default:
            PSLASSERT(PSLFALSE);
            break;
    }

    if (0 != msgID)
    {
        status = FillMessage(pRetMsg, pMsg, msgID,
                            (PSLBYTE*)pMsg->aParam2,
                            dwBufSize, dwMsgFlags);
        if (PSL_FAILED (status))
        {
            goto Exit;
        }

        /* send the message */
        status = MTPRouteMsgToTransport(pmc, pRetMsg);
        if (PSL_FAILED (status))
        {
            goto Exit;
        }
        pRetMsg = PSLNULL;
    }
Exit:
    SAFE_PSLMSGFREE(pRetMsg);
    return status;
}
