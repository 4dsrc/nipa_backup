/*
 *  PSLMsgPool.h
 *
 *  Contains the message pool declarations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLMSGPOOL_H_
#define _PSLMSGPOOL_H_

typedef PSLOBJPOOL      PSLMSGPOOL;

/*
 *  This method will be used by the message orginator to reserve a
 *  pool of memory for messages. PSLMsgAlloc will return message
 *  buffers from these pools while PSLMsgFree will release them back
 *  to the pool.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgPoolCreate(
        PSLUINT32       cdwMsgs,  /* count of messages */
        PSLMSGPOOL*     pmpID);   /* message pool identifier*/


/*
 *  This will let the message orginator to release the reserved
 *  memory pool.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgPoolDestroy(
        PSLMSGPOOL      mpID);  /* message pool identifier*/


/*
 *  The method will return a message from the memory pool created
 *  using PSLMsgPoolCreate. The orginator of a message will first
 *  call this function to get message buffer, fill it and then
 *  calls PSLMsgPost.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgAlloc(
        PSLMSGPOOL      mpID,   /* message pool identifier*/
        PSLMSG**        ppMsg); /* message buffer to retrieve*/


/*
 *  The method will return the message back to the pool from which it
 *  was retrieved. The consumer of a message will first call PSLMsgGet
 *  and then call this fucntion. Pool indentifier is not passed as
 *  input parameter as the message node buffer will contain the pool
 *  identifier.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgFree(
        PSLMSG*         pMsg);  /* message buffer to release */


#ifdef PSL_MSG_TRACE

/*
 *  These methods turn on tracing on the message so that message leaks
 *  can be tracked.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgTrace(
        PSLMSG*         pMsg);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgTraceReport(
        PSLMSGPOOL      mpID,
        PSLBOOL         fReset);

#endif  /* PSL_MSG_TRACE */

/*
 *  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#define SAFE_PSLMSGPOOLDESTROY(_mp) \
if (PSLNULL != _mp) \
{ \
    PSLMsgPoolDestroy(_mp); \
    _mp = PSLNULL; \
}

#define SAFE_PSLMSGFREE(_msg) \
if (PSLNULL != _msg) \
{ \
    PSLMsgFree(_msg); \
    _msg = PSLNULL; \
}


#endif /* _PSLMSGPOOL_H_ */

