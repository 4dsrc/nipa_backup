/*
 *  MTPService.h
 *
 *  Contains definitions of MTP Service Registration API
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MTPSERVICES_H_
#define _MTPSERVICES_H_

#ifndef DEFINE_DEVSVCGUID
#define DEFINE_DEVSVCGUID       DEFINE_PSLGUID
#endif  /* DEFINE_DEVSVCGUID */

#ifndef DEFINE_DEVSVCPROPKEY
#define DEFINE_DEVSVCPROPKEY    DEFINE_PSLPROPERTYKEY
#endif  /* DEFINE_DEVSVCPROPKEY */

/*  Service Handles
 */

typedef PSLHANDLE MTPSERVICE;

typedef MTPDBCONTEXTDATA MTPSERVICECONTEXTDATA;

/*  MTPSERVICEPROPERTYREC and MTPSERVICEPROPERTYINFO
 *
 *  Describes a service property
 */

typedef struct _MTPSERVICEPROPERTYREC
{
    MTPPROPERTYREC              recProp;
    PSL_CONST PSLPKEY*          pkeyProp;
    PSLWSTR                     szName;
} MTPSERVICEPROPERTYREC;

typedef MTPPROPERTYINFO MTPSERVICEPROPERTYINFO;


/*  MTPSERVICEPARAMREC
 *
 *  Describes a service method parameter
 */

typedef struct _MTPSERVICEPARAMREC
{
    MTPSERVICEPROPERTYREC       recServiceProp;
    PSLUINT32                   idxArg;
} MTPSERVICEPARAMREC;


/*  MTPSERVICEFORMATINFO
 *
 *  Structure containing information about the MTP formats supported
 */

typedef struct _MTPSERVICEFORMATREC
{
    MTPFORMATREC                recFormat;
    PSL_CONST PSLGUID*          pguidFormatGUID;
    PSLCWSTR                    szFormatName;
    PSLCWSTR                    szMIMEType;
} MTPSERVICEFORMATREC;

typedef struct _MTPSERVICEFORMATINFO
{
    MTPFORMATINFO               infoFormat;
    PSLUINT16                   wBaseFormatCode;
} MTPSERVICEFORMATINFO;


/*  MTPSERVICEMETHODINFO
 *
 *  Structure containing information about the MTP methods supported
 */

typedef struct _MTPSERVICEMETHODREC
{
    MTPFORMATREC                recFormat;
    PSL_CONST PSLGUID*          pguidMethodGUID;
    PSLCWSTR                    szMethodName;
} MTPSERVICEMETHODREC;

typedef MTPSERVICEFORMATINFO MTPSERVICEMETHODINFO;

typedef MTPPROPERTYINFO MTPSERVICEPARAMINFO;


/*  MTPSERVICEEVENTINFO
 *
 *  Structure containing information about MTP Service Events
 */

typedef struct _MTPSERVICEEVENTINFO
{
    PSLUINT16                   wEventCode;
    PSL_CONST PSLGUID*          pguidEventGUID;
    PSLCWSTR                    szEventName;
} MTPSERVICEEVENTINFO;


/*  MTP Service Objects
 *
 *  The MTP Service Object is the core object for all of the service behaviors.
 *  All service functionality is accessed via the functions associated with
 *  the service object.
 */

/*  pfnMTPServiceClose - PURE
 *
 *  Closes the MTP Service
 */

typedef PSLSTATUS (PSL_API* PFNMTPSERVICECLOSE)(
                    MTPSERVICE mtpService);


/*  pfnMTPServiceGetServiceID - PURE
 *
 *  Returns the service ID for this service
 */

typedef PSLUINT32 (PSL_API* PFNMTPSERVICEGETSERVICEID)(
                    MTPSERVICE mtpService);


/*  pfnMTPServiceGetStorageID - PURE
 *
 *  Returns the storage ID for this service
 */

typedef PSLUINT32 (PSL_API* PFNMTPSERVICEGETSTORAGEID)(
                    MTPSERVICE mtpService);

PSL_EXTERN_C PSLUINT32 PSL_API MTPServiceGetStorageIDBase(
                    MTPSERVICE mtpService);


/*  pfnMTPServiceGetFormatsSupported - PURE
 *
 *  Returns the list of formats supported by this service
 */

typedef PSLSTATUS (PSL_API* PFNMTPSERVICEGETFORMATSSUPPORTED)(
                    MTPSERVICE mtpService,
                    PSLUINT16* pwFormats,
                    PSLUINT32 cFormats,
                    PSLUINT32* pcFormats);

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceGetFormatsSupportedBase(
                    MTPSERVICE mtpService,
                    PSLUINT16* pwFormats,
                    PSLUINT32 cFormats,
                    PSLUINT32* pcFormats);


/*  pfnMTPServiceIsFormatSupported - PURE
 *
 *  Returns PSLSUCCESS if format code is supported by service, PSLSUCCESS_FALSE
 *  if it is not
 */

typedef PSLSTATUS (PSL_API* PFNMTPSERVICEISFORMATSUPPORTED)(
                    MTPSERVICE mtpService,
                    PSLUINT16 wFormat);

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceIsFormatSupportedBase(
                    MTPSERVICE mtpService,
                    PSLUINT16 wFormat);

/*  pfnMTPServiceContextDataOpen - PURE
 *
 *  Opens the Service Context Data associated with the specified service.
 */

typedef PSLSTATUS (PSL_API* PFNMTPSERVICECONTEXTDATAOPEN)(
                    MTPSERVICE mtpService,
                    PSLUINT32 dwDataFormat,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    PSLUINT64* pqwBufSize,
                    MTPSERVICECONTEXTDATA* pmtpsContextData);


/*  pfnMTPServiceDBContextOpen - PURE
 *
 *  Opens the MTP Database Context associated with this object
 */

typedef PSLSTATUS (PSL_API* PFNMTPSERVICEDBCONTEXTOPEN)(
                    MTPSERVICE mtpService,
                    PSLUINT32 dwJoinType,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    MTPDBCONTEXT* pmtpDBContext);

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceDBContextOpenBase(
                    MTPSERVICE mtpService,
                    PSLUINT32 dwJoinType,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    MTPDBCONTEXT* pmtpDBContext);

/*  MTPSERVICEVTBL
 *
 *  Virtual Function Table for the MTP Service Object
 */

typedef struct _MTPSERVICEVTBL
{
    PFNMTPSERVICECLOSE                  pfnMTPServiceClose;
    PFNMTPSERVICEGETSERVICEID           pfnMTPServiceGetServiceID;
    PFNMTPSERVICEGETSTORAGEID           pfnMTPServiceGetStorageID;
    PFNMTPSERVICEGETFORMATSSUPPORTED    pfnMTPServiceGetFormatsSupported;
    PFNMTPSERVICEISFORMATSUPPORTED      pfnMTPServiceIsFormatSupported;
    PFNMTPSERVICECONTEXTDATAOPEN        pfnMTPServiceContextDataOpen;
    PFNMTPSERVICEDBCONTEXTOPEN          pfnMTPServiceDBContextOpen;
} MTPSERVICEVTBL;


/*  MTPSERVICEOBJ
 *
 *  The root MTP Service object.  The MTP Service handle registered via a
 *  call to MTPServiceRegister must point to the MTPSERVICEOBJ associated
 *  with this service.
 */

typedef struct _MTPSERVICEOBJ
{
    PSL_CONST MTPSERVICEVTBL*           vtbl;
} MTPSERVICEOBJ;

typedef PSL_CONST MTPSERVICEOBJ* PMTPSERVICEOBJ;

/*  MTP Service Virtual Function Helpers
 */

#define GET_MTPSERVICEVTBL(_po)         (((PMTPSERVICEOBJ)_po)->vtbl)

#define MTPServiceClose(_po) \
    GET_MTPSERVICEVTBL(_po)->pfnMTPServiceClose(_po)

#define MTPServiceGetServiceID(_po) \
    GET_MTPSERVICEVTBL(_po)->pfnMTPServiceGetServiceID(_po)

#define MTPServiceGetStorageID(_po) \
    GET_MTPSERVICEVTBL(_po)->pfnMTPServiceGetStorageID(_po)

#define MTPServiceGetFormatsSupported(_po, _pwF, _cF, _pcF) \
    GET_MTPSERVICEVTBL(_po)->pfnMTPServiceGetFormatsSupported(_po, _pwF, _cF, _pcF)

#define MTPServiceIsFormatSupported(_po, _wF) \
    GET_MTPSERVICEVTBL(_po)->pfnMTPServiceIsFormatSupported(_po, _wF)

#define MTPServiceContextDataOpen(_po, _dwF, _rgm, _cm, _pqw, _pctxd) \
    GET_MTPSERVICEVTBL(_po)->pfnMTPServiceContextDataOpen(_po, _dwF, _rgm, _cm, _pqw, _pctxd)

#define MTPServiceDBContextOpen(_po, _dwJ, _rgm, _cm, _pdbc) \
    GET_MTPSERVICEVTBL(_po)->pfnMTPServiceDBContextOpen(_po, _dwJ, _rgm, _cm, _pdbc)


/*  MTPServicesInitialize - PLATFORM
 *
 *  Per session initialization point. Called during MTPSessionOpen.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServicesInitialize(
                    MTPCONTEXT* pmtpctx);


/*  MTPServiceRegister
 *
 *  Registers an MTP Device Service
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceRegister(
                    MTPCONTEXT* pmtpctx,
                    MTPSERVICE mtpService);


/*  MTPServiceUnregister
 *
 *  Unregisters an MTP Device Service
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceUnregister(
                    MTPCONTEXT* pmtpctx,
                    MTPSERVICE mtpService);


/*  MTPServiceRegisterLegacyService
 *
 *  Registers the legacy service
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceRegisterLegacyService(
                    MTPCONTEXT* pmtpctx,
                    MTPDBSESSION mtpdbLegacy);


/*  MTPServiceUnregisterLegacyService
 *
 *  Unregisters the legacy service
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceUnregisterLegacyService(
                    MTPCONTEXT* pmtpctx,
                    MTPDBSESSION mtpdbLegacy);


/*  MTP Service Context Data
 *
 *  The MTP Service Context Data object is used to manage the data
 *  serialization functionality for service operations.  Currently the
 *  MTP Service Context Data is a direct inheritor of the DB Context Data
 *  behaviors so no additional functionality is provided.
 */

typedef MTPDBCONTEXTDATAVTBL MTPSERVICECONTEXTDATAVTBL;

typedef MTPDBCONTEXTDATAOBJ MTPSERVICECONTEXTDATAOBJ;

#define MTPServiceContextDataClose(_po) \
    MTPDBContextDataClose(_po)

#define MTPServiceContextDataCancel(_po) \
    MTPDBContextDataCancel(_po)

#define MTPServiceContextDataSerialize(_po, _pv, _cb, _pcbW, _pcbL) \
    MTPDBContextDataSerialize(_po, _pv, _cb, _pcbW, _pcbL)

#define MTPServiceContextDataStartDeserialize(_po, _cb) \
    MTPDBContextDataStartDeserialize(_po, _cb)

#define MTPServiceContextDataDeserialize(_po, _pv, _cb, _mq, _pctx, _aP, _dwM) \
    MTPDBContextDataDeserialize(_po, _pv, _cb, _mq, _pctx, _aP, _dwM)

/*  Safe Helpers
 */

#define SAFE_MTPSERVICECLOSE(_pv) \
if (PSLNULL != (_pv)) \
{ \
    MTPServiceClose(_pv); \
    (_pv) = PSLNULL; \
}

#define SAFE_MTPSERVICECONTEXTDATACLOSE(_pv) \
if (PSLNULL != (_pv)) \
{ \
    MTPServiceContextDataClose(_pv); \
    (_pv) = PSLNULL; \
}

#endif  /* _MTPSERVICES_H_ */

