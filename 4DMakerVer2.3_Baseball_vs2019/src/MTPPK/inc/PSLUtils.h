/*
 *  PSLUtils.h
 *
 *  Contains a collection of helpful macros and utility functions.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLUTILS_H_
#define _PSLUTILS_H_

/*
 *  General method for determining the byte offset of an item in a structure
 */

#ifndef PSLOFFSETOF
#define PSLOFFSETOF(_s, _m)     (PSLSIZET)(&(((_s*)0)->_m))
#endif  /* PSLOFFSETOF */

/*
 *  General array sizing for static arrays
 */

#ifndef PSLARRAYSIZE
#define PSLARRAYSIZE(_rg)       (sizeof(_rg) / sizeof(_rg[0]))
#endif  /* PSLARRAYSIZE */

/*
 *  String array sizing routine for static arrays (accounts for
 *  terminator)
 */

#ifndef PSLSTRARRAYSIZE
#define PSLSTRARRAYSIZE(_rg)    (PSLARRAYSIZE(_rg) - 1)
#endif  /* PSLSTRARRAYSIZE */

/*
 *  max/min Macros
 *
 */

#ifndef max
#define max(_1, _2)             (((_1) >= (_2)) ? (_1) : (_2))
#endif  /* max */

#ifndef min
#define min(_1, _2)             (((_1) <= (_2)) ? (_1) : (_2))
#endif  /* min */


/*  GUID Generation
 *
 *  Generates a GUID
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLGUIDGenerate(
                    PSLGUID* pguidResult);


/*  Random Number Support
 *
 *  Generates a random value of the specified size
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLGenRandom(
                    PSLVOID* pvDest,
                    PSLUINT32 cbDest);


/*  Binary Search
 *
 *  Compare routine provided by user of PSLBSearch to compare the
 *  input search key with the key of each item in the array
 */

typedef PSLINT32 (PSL_API *PFNCOMPARE)(
                    PSL_CONST PSLVOID* pvKey,
                    PSL_CONST PSLVOID* pvItem);

PSL_EXTERN_C PSLSTATUS PSL_API PSLBSearch(
                    PSL_CONST PSLVOID*  pvKey,   /*key to compare*/
                    PSL_CONST PSLVOID*  pvStart, /*start of the array to search*/
                    PSLUINT32  dwNumItems,       /*number of items in the array*/
                    PSLUINT32  dwSizeItem,       /*size of individual item*/
                    PFNCOMPARE pfnCompare,       /*pointer to compare routine*/
                    PSLVOID**  ppvReturn);       /*search result*/

#endif  /* _PSLUTILS_H_ */

