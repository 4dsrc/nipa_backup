/*
 *  MTPServiceContextData.h
 *
 *  Contains declarations of structures and functions for the generalized
 *  MTP Service Context Data serialization and deserialization routines.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPSERVICECONTEXTDATA_H_
#define _MTPSERVICECONTEXTDATA_H_

#include "MTPDBContextData.h"

/*  MTP Service Info
 *
 *  Supports serialization of the MTP Service Info dataset
 */

#define MTPSERVICEFLAG_STANDARD     0x00000000
#define MTPSERVICEFLAG_ABSTRACT     0x00000001

#define MTPSERVICEINFO_VERSION_1    0x64

#define MTPSERVICE_BASE_SERVICE_ID  0

#define MTPSERVICE_BASE_FORMAT_CODE 0

#define MTPSERVICE_METHOD_SERVICE   0

typedef struct _MTPSERVICEINFO
{
    PSLUINT32                           dwServiceVersion;
    PSL_CONST PSLGUID*                  pguidServiceGUID;
    PSLCWSTR                            szServiceName;
    PSLUINT32                           dwServiceFlags;
    PSL_CONST PSLGUID**                 rgpguidUsesServicePUID;
    PSLUINT32                           cUsesServicePUID;
    PSL_CONST MTPSERVICEPROPERTYINFO*   rgspiServiceProps;
    PSLUINT32                           cServiceProps;
    PSL_CONST MTPSERVICEFORMATINFO*     rgsfiServiceFormats;
    PSLUINT32                           cServiceFormats;
    PSL_CONST MTPSERVICEMETHODINFO*     rgsmiServiceMethods;
    PSLUINT32                           cServiceMethods;
    PSL_CONST MTPSERVICEEVENTINFO*      rgseiServiceEvents;
    PSLUINT32                           cServiceEvents;
    PSL_CONST PSLBYTE*                  pbData;
    PSLUINT32                           cbData;
} MTPSERVICEINFO;


/*  MTPServiceInfoContextDataCreate
 *
 *  Creates an MTPSERVICECONTEXTDATA for use in serializing the service info
 *  dataset.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceInfoContextDataCreate(
                    PSLUINT32 dwServiceID,
                    PSLUINT32 dwStorageID,
                    PSL_CONST PSLGUID* pguidServicePUID,
                    PSL_CONST MTPSERVICEINFO* pServiceInfo,
                    PSLPARAM aParam,
                    PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPSERVICECONTEXTDATA* pmscd);


/*  MTP ServiceInfo Service Helpers
 *
 *  Set of helper functions to perform basic service operations based on
 *  the contents of the MTPSERVICEINFO dataset
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceInfoGetFormatsSupported(
                    PSL_CONST MTPSERVICEINFO* pServiceInfo,
                    PSLUINT16* pwFormats,
                    PSLUINT32 cFormats,
                    PSLUINT32* pcFormats);

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceInfoIsFormatSupported(
                    PSL_CONST MTPSERVICEINFO* pServiceInfo,
                    PSLUINT16 wFormat);

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceInfoContextDataOpen(
                    PSL_CONST MTPSERVICEINFO* pServiceInfo,
                    PSLUINT32 dwServiceID,
                    PSLUINT32 dwStorageID,
                    PSL_CONST PSLGUID* pguidServicePUID,
                    PSLUINT32 dwDataFormat,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    PSLPARAM aParam,
                    PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    PSLUINT64* pqwBufSize,
                    MTPSERVICECONTEXTDATA* pmscd);


/*  MTP Service Property Helpers
 *
 *  Helper functions for creating a MTPSERVICECONTEXTDATA to serialize
 *  service properties based on the information provided in an MTPSERVICEINFO
 *  dataset.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServicePropDescContextDataCreate(
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    PSL_CONST MTPSERVICEINFO* pServiceInfo,
                    PSLPARAM aParam,
                    PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPSERVICECONTEXTDATA* pmscd);

PSL_EXTERN_C PSLSTATUS PSL_API MTPServicePropListContextDataCreate(
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    PSL_CONST MTPSERVICEINFO* pServiceInfo,
                    PSLPARAM aParam,
                    PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPSERVICECONTEXTDATA* pmscd);


/*  MTP Service Capabilities Helpers
 *
 *  Helper function for creating a MTPSERVICECONTEXTDATA to serialize
 *  service capabilities based on the information provided in an MTPSERVICEINFO
 *  dataset.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceCapsContextDataCreate(
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    PSL_CONST MTPSERVICEINFO* pServiceInfo,
                    PSLPARAM aParam,
                    PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPSERVICECONTEXTDATA* pmscd);

/*  MTP Service Base
 *
 *  This generic service implementation leverages the design of the
 *  MTPSERVICEINFO dataset to handle many of the common tasks required.
 *  of a service.  Most core functionality of service implementations may
 *  be abstracted by simply using this implementation and subclassing where
 *  appropraite.
 *
 *  By design the MTPServiceBase implementation does not provide support
 *  for DBContextData operations.  Subclasses must implement this functionality
 *  and update the VTable accordingly.
 *
 *  NOTE:
 *  MTPServiceBase* and MTPService*Base implementations are not to be confused.
 *  The former assumes the present of an MTPSERVICEBASEOBJ and is dependent
 *  upon it.  The latter are generic helper functions that assume nothing
 *  about the MTPSERVICE parameter.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceBaseClose(
                    MTPSERVICE mtpService);

PSL_EXTERN_C PSLUINT32 PSL_API MTPServiceBaseGetServiceID(
                    MTPSERVICE mtpService);

PSL_EXTERN_C PSLUINT32 PSL_API MTPServiceBaseGetStorageID(
                    MTPSERVICE mtpService);

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceBaseGetFormatsSupported(
                    MTPSERVICE mtpService,
                    PSLUINT16* pwFormats,
                    PSLUINT32 cFormats,
                    PSLUINT32* pcFormats);

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceBaseIsFormatSupported(
                    MTPSERVICE mtpService,
                    PSLUINT16 wFormat);

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceBaseContextDataOpen(
                    MTPSERVICE mtpService,
                    PSLUINT32 dwDataFormat,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    PSLUINT64* pqwBufSize,
                    MTPSERVICECONTEXTDATA* pmscd);

/*  MTPServiceBaseCreate
 *
 *  Creates an instance of the generic service handler which does not include
 *  handling of the MTP DB Context operations
 */

typedef struct _MTPSERVICEBASEINFO
{
    PSL_CONST MTPSERVICEINFO*       pServiceInfo;
} MTPSERVICEBASEINFO;

typedef struct _MTPSERVICEBASEOBJ
{
    MTPSERVICEOBJ                   mtpSO;
#ifdef PSL_ASSERTS
    PSLUINT32                       dwCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT32                       dwServiceID;
    PSLUINT32                       dwStorageID;
    PSLGUID                         guidServicePUID;
    PSL_CONST MTPSERVICEBASEINFO*   pServiceBaseInfo;
} MTPSERVICEBASEOBJ;

PSL_EXTERN_C PSLSTATUS PSL_API MTPServiceBaseCreate(
                    PSLUINT32 dwServiceID,
                    PSLUINT32 dwStorageID,
                    PSL_CONST PSLGUID* pguidServicePUID,
                    PSL_CONST MTPSERVICEBASEINFO* pServiceBaseInfo,
                    PSLUINT32 cbObject,
                    MTPSERVICE* pmtpService);


/*  MTP DB Base Service
 *
 *  Leverages the functionality of the MTP Base Service and adds support for
 *  Storage ID handling.
 */

/*  pfnMTPDBServiceBaseGetFreeSpace - PURE
 *
 *  Returns the current free space information for this service
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBSERVICEBASEGETFREESPACE)(
                    MTPSERVICE mtpService,
                    PSLUINT64* pqwFreeSpace,
                    PSLUINT32* pdwFreeObjects);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBServiceBaseGetFreeSpaceBase(
                    MTPSERVICE mtpService,
                    PSLUINT64* pqwFreeSpace,
                    PSLUINT32* pdwFreeObjects);

/*  Other Base Implementations
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBServiceBaseClose(
                    MTPSERVICE mtpService);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBServiceBaseDBContextOpen(
                    MTPSERVICE mtpService,
                    PSLUINT32 dwJoinType,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    MTPDBCONTEXT* pmtpdbc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBServiceBaseDBContextClose(
                    MTPDBCONTEXT mdbc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBServiceBaseDBContextDataOpen(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32 dwDataFormat,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    PSLUINT64* pcbTotalSize,
                    MTPDBCONTEXTDATA * pmdbcd);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBServiceBaseDBContextDataClose(
                    MTPDBCONTEXTDATA mdbcd);


/*  MTPDBSERVICEBASEVTBL
 *
 *  Virtual Function Table for the MTP DB Service Base class- extends the
 *  standard MTP Service VTable
 */

typedef struct _MTPDBSERVICEBASEVTBL
{
    MTPSERVICEVTBL                  vtblService;
    PFNMTPDBSERVICEBASEGETFREESPACE pfnMTPDBServiceBaseGetFreeSpace;
} MTPDBSERVICEBASEVTBL;


/*  MTPDBSERVICEBASEOBJ
 *
 *  Base object for the MTP DB Service Class
 */

typedef struct _MTPDBSERVICEBASEOBJ
{
    MTPSERVICEBASEOBJ               mtpSBO;
    MTPDBCONTEXTBASEOBJ             mtpDBCO;
    MTPDBCONTEXTDATAOBJ             mtpDBCDO;
#ifdef PSL_ASSERTS
    PSLUINT32                       dwCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT32                       cRef;
} MTPDBSERVICEBASEOBJ;

typedef PSL_CONST MTPDBSERVICEBASEOBJ* PMTPDBSERVICEBASEOBJ;

/*  MTPDBServiceBaseCreate
 *
 *  Creates an instance of the generic service hander which includes handling
 *  of the MDP DB Context operations
 */

typedef struct _MTPDBSERVICEBASEINFO
{
    MTPSERVICEBASEINFO              pServiceInfo;
    PSL_CONST MTPSTORAGEINFO*       pStorageInfo;
} MTPDBSERVICEBASEINFO;

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBServiceBaseCreate(
                    PSLUINT32 dwServiceID,
                    PSLUINT32 dwStorageID,
                    PSL_CONST PSLGUID* pguidServicePUID,
                    PSL_CONST MTPDBSERVICEBASEINFO* pServiceBaseInfo,
                    PSLUINT32 cbObject,
                    MTPSERVICE* pmtpService);

/*  Inheritance Macros
 */

#define MTPDBSERVICEBASEOBJ_TO_MTPDBCONTEXT(_pmtpDBSBO) \
    &((_pmtpDBSBO)->mtpDBCO)

#define MTPDBSERVICEBASEOBJ_FROM_MTPDBCONTEXT(_mdbc) \
    (MTPDBSERVICEBASEOBJ*)((PSLBYTE*) \
            (_mdbc) - PSLOFFSETOF(MTPDBSERVICEBASEOBJ, mtpDBCO))

#define MTPDBSERVICEBASEOBJ_TO_MTPDBCONTEXTDATA(_pmtpDBSBO) \
    &((_pmtpDBSBO)->mtpDBCDO)

#define MTPDBSERVICEBASEOBJ_FROM_MTPDBCONTEXTDATA(_mdbcd) \
    (MTPDBSERVICEBASEOBJ*)((PSLBYTE*) \
            (_mdbcd) - PSLOFFSETOF(MTPDBSERVICEBASEOBJ, mtpDBCDO))

/*  MTP DB Service Base Virtual Function Helpers
 */

#define GET_MTPDBSERVICEBASEVTBL(_po) \
    ((MTPDBSERVICEBASEVTBL*)(((PMTPDBSERVICEBASEOBJ)_po)->mtpSBO.mtpSO.vtbl))

#define MTPDBServiceBaseGetFreeSpace(_po, _pqw, _pdw) \
    GET_MTPDBSERVICEBASEVTBL(_po)->pfnMTPDBServiceBaseGetFreeSpace(_po, _pqw, _pdw)


#endif  /* _MTPSERVICECONTEXTDATA_H_ */
