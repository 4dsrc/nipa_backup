/*
 *  MTP.h
 *
 *  Root header for the MTP Layer (MTP).
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTP_H_
#define _MTP_H_

/*  Core Headers
 */

#include "MTPTypes.h"
#include "MTPStructs.h"
#include "MTPErrors.h"
#include "MTPSerialize.h"
#include "MTPContext.h"
#include "MTPMsg.h"
#include "MTPInitialize.h"
#include "MTPTransport.h"
#include "MTPIdentity.h"
#include "MTPProperty.h"
#include "MTPFormat.h"
#include "MTPDatabase.h"
#include "MTPDeviceProperty.h"
#include "MTPServices.h"
#include "MTPPerfLogger.h"

/*  Core Defines
 */

/*  Maximum Number of Simultaneous Sessions
 *
 *  Specifies the maximum number of simultaneous sessions that will be allowed.
 *  0 indicates that there is no restriction on the number of sessions.
 */

#ifndef MTP_MAX_SESSIONS
#define MTP_MAX_SESSIONS            0
#endif  /* MTP_MAX_SESSIONS */

#ifndef MTP_DEFAULT_SESSIONS
#define MTP_DEFAULT_SESSIONS        8
#endif  /*  MTP_DEFAULT_SESSIONS */

#endif  /* _MTP_H_ */

