/*
 *  PSLLookupTable.h
 *
 *  Contains the lookup table declarations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLLOOKUPTABLE_H_
#define _PSLLOOKUPTABLE_H_

typedef PSLHANDLE PSLLOOKUPTABLE;

typedef struct _PSLLOOKUPRECORD
{
    PSLPARAM    pvKey;
    PSLPARAM    pvValue;
    PSLPARAM    pvData;
} PSLLOOKUPRECORD;

typedef PSLSTATUS (PSL_API* PFNENUMLOOKUPTBLPROC)(
                            PSLPARAM            aParamKey,
                            PSLPARAM            aParamValue,
                            PSLPARAM            aParamData,
                            PSLPARAM            aParamEnum);

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableCreate(
                            PSLUINT32           cItems,
                            PSLLOOKUPTABLE*     pmlt);

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableDestroy(
                            PSLLOOKUPTABLE      mlt);

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableFindEntry(
                            PSLLOOKUPTABLE      mlt,
                            PFNCOMPARE          pfnCompare,
                            PSLPARAM            pvKey,
                            PSLPARAM*           ppvValue,
                            PSLPARAM*           ppvData);

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableSetEntry(
                            PSLLOOKUPTABLE      mlt,
                            PFNCOMPARE          pfnCompare,
                            PSLPARAM            pvKey,
                            PSLPARAM            pvValue,
                            PSLPARAM            pvData);

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableInsertEntry(
                            PSLLOOKUPTABLE      mlt,
                            PSLPARAM            pvKey,
                            PSLPARAM            pvValue,
                            PSLPARAM            pvData);

PSL_EXTERN_C PSLSTATUS PSL_API PSLLookupTableEnum(
                            PSLLOOKUPTABLE          mlt,
                            PFNENUMLOOKUPTBLPROC    pfnEnumLookupTblProc,
                            PSLPARAM                aParamEnum);

/*
 *  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#define SAFE_PSLLOOKUPTABLEDESTROY(_pslt) \
if (PSLNULL != _pslt) \
{ \
    PSLLookupTableDestroy(_pslt); \
    _pslt = PSLNULL; \
}

#endif  /*_PSLLOOKUPTABLE_H_*/
