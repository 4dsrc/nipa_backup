/*
 *  PSLDebug.h
 *
 *  Contains debug utlitity functions for the Windows Media Cross Platform
 *  Engine.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLDEBUG_H_
#define _PSLDEBUG_H_

/*
 *  Debugger Entry
 *
 */


PSL_EXTERN_C PSLVOID PSL_API PSLDebugBreakFn(PSLVOID);

#ifndef PSLDebugBreak
#define PSLDebugBreak PSLDebugBreakFn
#endif  /* PSLDebugBreak */


/*
 *  Assert Support
 *
 */

#ifdef PSL_ASSERTS

/*
 *  Assert Mode
 *
 */

typedef enum
{
    PSLAM_MESSAGE_BOX = 0,
    PSLAM_HALT,
    PSLAM_IGNORE
} PSL_ASSERT_MODE;


/*
 *  Core Assert Implementation
 *
 *  If PSLAssertFailed returns TRUE the default assert macro will break into
 *  the debugger.
 *
 */

PSL_EXTERN_C PSLBOOL PSL_API PSLAssertFailed(
                            PSLCSTR szExp,
                            PSLCSTR szFile,
                            PSLINT32 nLine);

#define PSLASSERT(_exp)                                                     \
            ((_exp) ?                                                       \
                (PSLVOID)0 : (PSLAssertFailed(#_exp, __FILE__, __LINE__) ?  \
                                PSLDebugBreak() : (PSLVOID)0))


/*
 *  Use PSL_ASSERT_MODE to alter how asserts behave
 *
 */

PSL_EXTERN_C PSLVOID PSL_API SetPSLAssertMode(
                            PSL_ASSERT_MODE nMode);

PSL_EXTERN_C PSL_ASSERT_MODE PSL_API GetPSLAssertMode(PSLVOID);

#else   /* !PSL_ASSERTS */

/*
 *  Disable Asserts
 *
 */

#define PSLASSERT(_exp)

#define SetPSLAssertMode(_mode)

#define GetPSLAssertMode()  PSLAM_IGNORE

#endif  /* PSL_ASSERTS */


/*
 *  Trace Support
 *
 */

/*
 *  Generic tracing levels
 *
 */

typedef enum
{
    PSLTP_LOWEST = 0,
    PSLTP_LOW,
    PSLTP_NORMAL,
    PSLTP_HIGH,
    PSLTP_HIGHEST
} PSL_TRACE_PRIORITY;

#ifdef PSL_TRACE

/*
 *  Core Trace Implementation
 *
 *  Use the specified trace implementations to trigger trace output according
 *  to the current trace priority.
 *
 */

PSL_EXTERN_C PSLVOID PSL_API PSLTrace(
                            PSL_TRACE_PRIORITY nPriority,
                            PSLCSTR szFormat,
                            ...);

PSL_EXTERN_C PSLVOID PSL_API PSLTraceDetail(
                            PSLCSTR szFormat,
                            ...);

PSL_EXTERN_C PSLVOID PSL_API PSLTraceProgress(
                            PSLCSTR szFormat,
                            ...);

PSL_EXTERN_C PSLVOID PSL_API PSLTraceWarning(
                            PSLCSTR szFormat,
                            ...);

PSL_EXTERN_C PSLVOID PSL_API PSLTraceError(
                            PSLCSTR szFormat,
                            ...);

/*
 *  Use PSL_TRACE_PRIORITY to manage the level of tracing that is reported
 *
 */

PSL_EXTERN_C PSLVOID PSL_API SetPSLTraceLevel(
                            PSL_TRACE_PRIORITY nPriority);

PSL_EXTERN_C PSL_TRACE_PRIORITY PSL_API GetPSLTraceLevel(PSLVOID);

#else   /* !PSL_TRACE */

/*
 *  Disable Tracing
 *
 */
#ifdef PSLTRACE_EMPTY

#define PSLTrace
#define PSLTraceDetail
#define PSLTraceProgress
#define PSLTraceWarning
#define PSLTraceError

#else /* !PSLTRACE_EMPTY */

#define PSLTrace            while(0)
#define PSLTraceDetail      while(0)
#define PSLTraceProgress    while(0)
#define PSLTraceWarning     while(0)
#define PSLTraceError       while(0)

#endif /* PSLTRACE_EMPTY */

#define SetPSLTraceLevel(_x)

#define GetPSLTraceLevel()  PSLTP_HIGHEST

#endif  /* PSL_TRACE */

/*
 *  Make sure the default ASSERT macro always uses the PSL implementation
 *
 */

#ifdef ASSERT
#undef ASSERT
#endif  // ASSERT

#define ASSERT          PSLASSERT


/*
 *  PSLDebugPrintf*
 *
 *  General functions for generating debug output on the debugger console
 *
 */

PSL_EXTERN_C PSLVOID PSL_API PSLDebugPrintf(
                            PSLCSTR szFormat,
                            ...);

PSL_EXTERN_C PSLVOID PSL_API PSLDebugPrintfW(
                            PSLCWSTR wszFormat,
                            ...);

#endif  /* _PSLDEBUG_H_ */

