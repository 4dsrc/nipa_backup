/*
 *  SyncDeviceService.h
 *
 *  Contains definitions for the general sync properties and formats
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The service definition specified in this file has been designed for use
 *  with Microsoft Windows.  To request additions to this file, including new
 *  service properties, object formats, object properties, methods, and events,
 *  please contact Microsoft at askmtp@microsoft.com with your request to
 *  maintain interoperability.
 *
 *  Custom service properties, object formats, object properties, methods,
 *  and events may also be added by generating new format, method, or event
 *  GUIDs or by creating new PKeys in a privately generated and managed
 *  namespace.
 *
 */

#ifndef _SYNCDEVICESERVICE_H_
#define _SYNCDEVICESERVICE_H_

/*****************************************************************************/
/*  Sync Service Properties                                                  */
/*****************************************************************************/

DEFINE_DEVSVCGUID(NAMESPACE_SyncSvc,
    0x703d392c, 0x532c, 0x4607, 0x91, 0x58, 0x9c, 0xea, 0x74, 0x2f, 0x3a, 0x16);


/*  PKEY_SyncSvc_SyncFormat
 *
 *  Indicates the format GUID for the object format that is to be used in the
 *  sync operation.
 *
 *  Type: UInt128
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_SyncSvc_SyncFormat,
    0x703d392c, 0x532c, 0x4607, 0x91, 0x58, 0x9c, 0xea, 0x74, 0x2f, 0x3a, 0x16,
    2);

#define NAME_SyncSvc_SyncFormat             L"SyncFormat"


/*  PKEY_SyncSvc_LocalOnlyDelete
 *
 *  Boolean flag indicating whether deletes of objects on the service host
 *  should be treated as "local only" and not propogated to other sync
 *  participants.  The alternative is "true sync" in which deletes on the
 *  service host are propogated to all other sync participants.
 *
 *  Type: UInt8
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_SyncSvc_LocalOnlyDelete,
    0x703d392c, 0x532c, 0x4607, 0x91, 0x58, 0x9c, 0xea, 0x74, 0x2f, 0x3a, 0x16,
    3);

#define NAME_SyncSvc_LocalOnlyDelete        L"LocalOnlyDelete"


/*  PKEY_SyncSvc_FilterType
 *
 *  Value describing type of the filter
 *
 *  Type: UInt8
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_SyncSvc_FilterType,
    0x703d392c, 0x532c, 0x4607, 0x91, 0x58, 0x9c, 0xea, 0x74, 0x2f, 0x3a, 0x16,
    4);

#define NAME_SyncSvc_FilterType             L"FilterType"

#define SYNCSVC_FILTER_NONE                             0
#define SYNCSVC_FILTER_CONTACTS_WITH_PHONE              1
#define SYNCSVC_FILTER_TASK_ACTIVE                      2
#define SYNCSVC_FILTER_CALENDAR_WINDOW_WITH_RECURRENCE  3


/*  PKEY_SyncSvc_SyncObjectReferences
 *
 *  Value describing whether object references should be included as part of
 *  the sync process or not
 *
 *  Type: UInt8
 *  Form: Enum
 */

DEFINE_DEVSVCPROPKEY(PKEY_SyncSvc_SyncObjectReferences,
    0x703d392c, 0x532c, 0x4607, 0x91, 0x58, 0x9c, 0xea, 0x74, 0x2f, 0x3a, 0x16,
    5);

#define NAME_SyncSvc_SyncObjectReferences   L"SyncObjectReferences"

#define ENUM_SyncSvc_SyncObjectReferencesDisabled       0x00
#define ENUM_SyncSvc_SyncObjectReferencesEnabled        0xff


/*****************************************************************************/
/*  Sync Service Object Properties                                           */
/*****************************************************************************/

DEFINE_DEVSVCGUID(NAMESPACE_SyncObj,
    0x37364f58, 0x2f74, 0x4981, 0x99, 0xa5, 0x7a, 0xe2, 0x8a, 0xee, 0xe3, 0x19);

/*  PKEY_SyncObj_LastAuthorProxyID
 *
 *  Contains a GUID inidcating the proxy ID of the last proxy to author the
 *  object
 *
 *  Type: UInt128
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_SyncObj_LastAuthorProxyID,
    0x37364f58, 0x2f74, 0x4981, 0x99, 0xa5, 0x7a, 0xe2, 0x8a, 0xee, 0xe3, 0x19,
    2);

#define NAME_SyncObj_LastAuthorProxyID      L"LastAuthorProxyID"

/*****************************************************************************/
/*  Sync Service Methods                                                     */
/*****************************************************************************/

/*  METHOD_SyncSvc_BeginSync
 */

DEFINE_DEVSVCGUID(METHOD_SyncSvc_BeginSync,
    0x63803e07, 0xc713, 0x45d3, 0x81, 0x19, 0x34, 0x79, 0xb3, 0x1d, 0x35, 0x92);

#define NAME_SyncSvc_BeginSync              L"BeginSync"

/*  METHOD_SyncSvc_EndSync
 */

DEFINE_DEVSVCGUID(METHOD_SyncSvc_EndSync,
    0x40f3f0f7, 0xa539, 0x422e, 0x98, 0xdd, 0xfd, 0x8d, 0x38, 0x5c, 0x88, 0x49);

#define NAME_SyncSvc_EndSync                L"EndSync"

#endif  /* _SYNCDEVICESERVICE_H_ */

