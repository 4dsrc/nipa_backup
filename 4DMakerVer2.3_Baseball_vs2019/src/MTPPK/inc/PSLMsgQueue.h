/*
 *  PSLMsgQueue.h
 *
 *  Contains the message queue declarations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLMSGQUEUE_H_
#define _PSLMSGQUEUE_H_

/*
 *  Ensure NUM_MSGPRIORITY_LEVELS is always
 *  the last element.
 *
 */

typedef PSLHANDLE PSLMSGQUEUE;

typedef PSLUINT32 PSLMSGID;

/*
 *  PSL Message Classes
 *
 *  Each PSL message is made up of a message class and an ID.  This
 *  makes it easier to add messages to the system without having
 *  to worry about causing a collision.  Currently the system uses
 *  a packed PSLUINT32 to hold the information.  The upper 16-bits
 *  contains the class, the lower 16-bits contains the actual
 *  message ID.  Use MAKE_PSL_MSGID, MAKE_PSL_NORMAL_MSGID or
 *  MAKE_PSL_PRIORITY_MSGID to create message IDs.
 *
 */

enum
{
    PSL_UNKNOWN = 0,
    PSL_USER,
    PSL_APPLICATION,
    PSL_SOCKET,
    PSL_MAX_MSG_CLASS
};

#define PSLMSGID_PRIORITY           0x40000000

#define PSLMSGID_PSL_FLAGS_MASK     0xc0000000
#define PSLMSGID_USER_FLAGS_MASK    0x3f000000
#define PSLMSGID_FLAGS_MASK         0xff000000

#define PSLMSGID_MSG_CLASS_MASK     0x00fff000
#define PSLMSGID_MSG_ID_MASK        0x00000fff

#define PSL_MSG_CLASS(_msg)         ((PSLMSGID_MSG_CLASS_MASK & _msg ) >> 12)
#define PSL_MSG_ID(_msg)            (PSLMSGID_MSG_ID_MASK & _msg)
#define PSL_MSG_FLAGS(_msg)         (PSLMSGID_FLAGS_MASK & _msg)

#define MAKE_PSL_MSGID(_class, _id, _flags) \
    ((PSLMSGID)((PSLMSGID_FLAGS_MASK & (_flags)) | \
        (PSLMSGID_MSG_CLASS_MASK & ((_class) << 12)) | \
        (PSLMSGID_MSG_ID_MASK & (_id))))


#define MAKE_PSL_NORMAL_MSGID(_class, _id)  \
    MAKE_PSL_MSGID(_class, _id, 0)

#define MAKE_PSL_PRIORITY_MSGID(_class, _id) \
    MAKE_PSL_MSGID(_class, _id, PSLMSGID_PRIORITY)


/*
 *  PSLMSG
 *
 *  PSLMSG structure holds the message that is queued
 *
 */

typedef struct
{
    PSLMSGID            msgID;      /* message identifier*/
    PSLPARAM            aParam0;    /* first parameter*/
    PSLPARAM            aParam1;    /* second parameter*/
    PSLPARAM            aParam2;    /* third parameter*/
    PSLPARAM            aParam3;    /* fourth parameter*/
    PSLLPARAM           alParam;    /* fifth parameter*/
} PSLMSG;

typedef PSLMSG* PSLPMSG;

/* type declaration of  OnPost and OnWait functions */

typedef PSLSTATUS (PSL_API *PFNONPOST)(
                            PSLVOID* pvParam);

typedef PSLSTATUS (PSL_API *PFNONWAIT)(
                            PSLVOID* pvParam);

typedef PSLSTATUS (PSL_API *PFNONDESTROY)(
                            PSLVOID* pvParam);


/*
 *  Queue specific operations
 *
 */

/*
 *  Queue creation
 *  The function will create a queue object and will return its
 *  identifier. The caller of this function can pass in the
 *  handle to a signalling object or a call back function pointer
 *  as pvParam and queue will pass that pvParam back to the user when
 *  calling the three call back functions.
 *  pfnOnPost is called when a message is posted to the queue to allow
 *  the user to signal the availability of message
 *  pfnOnWait is called to allow the user of a queue to wait for
 *  the availability of a message and
 *  pfnOnDestroy is called when destroying the queue to allow the
 *  user to clean up pvParam.
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  PSLERROR_INVALID_PARAMETER : pmqID or pvParam has PSNULL value
 *  PSLERROR_NOT_AVAILABLE : No queues are available
 *  PSLERROR_QUEUE_CREATE : initialization of queue resources failed
 *
 *  Remarks
 *  If the fucntion returned PSLERROR_NOT_AVAILABLE caller
 *  could retry this fucntion at a later time as queues could
 *  be freed later.
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgQueueCreate(
                            PSLVOID*            pvParam,
                            PFNONPOST           pfnOnPost,
                            PFNONWAIT           pfnOnWait,
                            PFNONDESTROY        pfnOnDestroy,
                            PSLMSGQUEUE*        pmqID);

/*
 *  Queue destruction
 *  The function will destroy the queue object.It take queue
 *  identifier returned by PSLMsgQueueCreate as input parameter.
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  PSLERROR_INVALID_PARAMETER : mqID is invalid
 *  PSLERROR_QUEUE_DESTROY : uninitialization of queue resources failed
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgQueueDestroy(
                            PSLMSGQUEUE         mqID);

/*
 *  The method will put a message to the head of the queue and will
 *  wait on a signalling object which will be signalled by PSLMsgFree.
 *  Caller of this function needs to allocate the message buffer using
 *  PSLMsgAlloc. Also user of this function must call PSlMsgFree to
 *  free the message and unblock it. If PSLMsgFree is not called
 *  the function will return after the timeout value expire.
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  PSLERROR_INVALID_PARAMETER : mqID is invalid
 *  PSLERROR_QUEUE_LOCK : attempt to lock the queue to perform
 *                        post operation failed
 *  PSLERROR_QUEUE_UNLOCK : attempt to unlock the queue after
 *                          post operation failed
 *  PSLERROR_QUEUE_ONPOST : OnPost function failed
 *  PSLERROR_PLATFORM_FAILURE : attempt to open a system resource failed
 *                              could be because system is running out
 *                              of memory or some other resources.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgSend(
                            PSLMSGQUEUE     mqID,
                            PSLMSG*         pMsg,
                            PSLUINT32       dwMSTimeout);

/*
 *  The method will post a message to the queue. Caller of this function
 *  needs to allocate the message buffer using PSLMsgAlloc.
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  PSLERROR_INVALID_PARAMETER : mqID is invalid
 *  PSLERROR_QUEUE_LOCK : attempt to lock the queue to perform
 *                        post operation failed
 *  PSLERROR_QUEUE_UNLOCK : attempt to unlock the queue after
 *                          post operation failed
 *  PSLERROR_QUEUE_ONPOST : OnPost function failed
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgPost(
                            PSLMSGQUEUE     mqID,
                            PSLMSG*         pMsg);

/*
 *  Method will return a message from the queue. The message will be
 *  removed from the queue once it is read. Caller of this function
 *  needs to release the buffers back to the pool using PSLMsgFree.
 *  This method will block until a message is available in the queue.
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  PSLERROR_INVALID_PARAMETER : mqID is invalid
 *  PSLERROR_QUEUE_LOCK : attempt to lock the queue to perform
 *                        post operation failed
 *  PSLERROR_QUEUE_UNLOCK : attempt to unlock the queue after
 *                          post operation failed
 *  PSLERROR_QUEUE_ONWAIT : OnWait function failed
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgGet(
                            PSLMSGQUEUE     mqID,
                            PSLMSG**        ppMsg);

/*
 *  Method will return a message from the queue without actually
 *  removing it from the queue. Caller of this function wil pass a
 *  pointer to a message structure and the function will copy the
 *  message from the queue on to the message buffer. This is done
 *  to avoid race conditions. Caller of this function should not call
 *  PSLMsgFree. Caller can also pass PSLNULL for the pMsg parameter
 *  to check if a message is available in the queue
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  PSLERROR_INVALID_PARAMETER : mqID is invalid
 *  PSLERROR_QUEUE_LOCK : attempt to lock the queue to perform
 *                        post operation failed
 *  PSLERROR_QUEUE_UNLOCK : attempt to unlock the queue after
 *                          post operation failed
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgPeek(
                            PSLMSGQUEUE     mqID,
                            PSLMSG*         pMsg);

/*
 *  This function is used to set a callback function which the newly
 *  set OnPost OnWait and OnDestroy functions will call.
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  PSLERROR_INVALID_PARAMETER : indicates
 *                              (a) mqDispatcher is equal to
 *                                  PSLNULL value
 *                              (b) pvParam is equal to PSLNULL
 *                              (c) pfnOnPost or pfnOnWait is PSLNULL
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgQueueSetCallbacks(
                            PSLMSGQUEUE     mqDispatcher,
                            PSLVOID*        pvParam,
                            PFNONPOST       pfnOnPost,
                            PFNONWAIT       pfnOnWait,
                            PFNONDESTROY    pfnOnDestroy);


/*
 *  This function enumerates the messages in the queue.  For each message
 *  the callback function provided is called and depending on the result
 *  of the callback action is taken.  If the callback returns PSLSUCCESS the
 *  message is left in the queue.  Returning PSLSUCCESS_FALSE from the
 *  callback causes the message to be removed from the queue.  Returning
 *  PSLSUCCESS_CANCEL causes enumeration to stop and the enumeration function
 *  to report success.  Returning any error from the callback causes enumeration
 *  to stop and the error to be returned.
 *
 */

typedef PSLSTATUS (PSL_API *PFNMSGENUM)(
                            PSL_CONST PSLMSG* pMsg,
                            PSLPARAM aParam);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMsgEnum(
                            PSLMSGQUEUE     mqEnum,
                            PFNMSGENUM      pfnMsgEnum,
                            PSLPARAM        aParam);


/*
 *  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#define SAFE_PSLMSGQUEUEDESTROY(_mq) \
if (PSLNULL != _mq) \
{ \
    PSLMsgQueueDestroy(_mq); \
    _mq = PSLNULL; \
}

#endif  /*_PSLMSGQUEUE_H_*/
