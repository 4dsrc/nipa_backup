/*
 *  PSLString.h
 *
 *  Contains declarations of the PSL string functions.  Use of these
 *  functions is strongly encouraged as they are designed to guard
 *  against buffer overruns.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLSTRING_H_
#define _PSLSTRING_H_

#define PSLSTRING_MAX_CCH       0x10000

/*
 *  String Length
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLStringLenA(
                            PSLCSTR szSrc,
                            PSLUINT32 cchMaxSrc,
                            PSLUINT32* pcchSrc);

PSL_EXTERN_C PSLSTATUS PSL_API PSLStringLen(
                            PSLCWSTR szSrc,
                            PSLUINT32 cchMaxSrc,
                            PSLUINT32* pcchSrc);

/*
 *  String Copies
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLStringCopyA(
                            PSLCHAR* szDest,
                            PSLUINT32 cchMaxDest,
                            PSLCSTR szSrc);

PSL_EXTERN_C PSLSTATUS PSL_API PSLStringCopy(
                            PSLWCHAR* szDest,
                            PSLUINT32 cchMaxDest,
                            PSLCWSTR szSrc);

PSL_EXTERN_C PSLSTATUS PSL_API PSLStringCopyLenA(
                            PSLCHAR* szDest,
                            PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest,
                            PSLCSTR szSrc,
                            PSLUINT32 cchSrc);

PSL_EXTERN_C PSLSTATUS PSL_API PSLStringCopyLen(
                            PSLWCHAR* szDest,
                            PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest,
                            PSLCWSTR szSrc,
                            PSLUINT32 cchSrc);

PSL_EXTERN_C PSLSTATUS PSL_API PSLStringCatA(
                            PSLCHAR* szDest,
                            PSLUINT32 cchMaxDest,
                            PSLCSTR szSrc);

PSL_EXTERN_C PSLSTATUS PSL_API PSLStringCat(
                            PSLWCHAR* szDest,
                            PSLUINT32 cchMaxDest,
                            PSLCWSTR szSrc);

PSL_EXTERN_C PSLSTATUS PSL_API PSLStringCatLenA(
                            PSLCHAR* szDest,
                            PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest,
                            PSLCSTR szSrc,
                            PSLUINT32 cchSrc);

PSL_EXTERN_C PSLSTATUS PSL_API PSLStringCatLen(
                            PSLWCHAR* szDest,
                            PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest,
                            PSLCWSTR szSrc,
                            PSLUINT32 cchSrc);

/*
 *  String Comparison
 */

PSL_EXTERN_C PSLINT32 PSL_API PSLStringCmpA(
                            PSLCSTR szStr1,
                            PSLCSTR szStr2);

PSL_EXTERN_C PSLINT32 PSL_API PSLStringICmpA(
                            PSLCSTR szStr1,
                            PSLCSTR szStr2);

PSL_EXTERN_C PSLINT32 PSL_API PSLStringCmp(
                            PSLCWSTR szStr1,
                            PSLCWSTR szStr2);

PSL_EXTERN_C PSLINT32 PSL_API PSLStringICmp(
                            PSLCWSTR szStr1,
                            PSLCWSTR szStr2);

PSL_EXTERN_C PSLINT32 PSL_API PSLStringCmpLenA(
                            PSLCSTR szStr1,
                            PSLCSTR szStr2,
                            PSLUINT32 cchCmp);

PSL_EXTERN_C PSLINT32 PSL_API PSLStringICmpLenA(
                            PSLCSTR szStr1,
                            PSLCSTR szStr2,
                            PSLUINT32 cchCmp);

PSL_EXTERN_C PSLINT32 PSL_API PSLStringCmpLen(
                            PSLCWSTR szStr1,
                            PSLCWSTR szStr2,
                            PSLUINT32 cchCmp);

PSL_EXTERN_C PSLINT32 PSL_API PSLStringICmpLen(
                            PSLCWSTR szStr1,
                            PSLCWSTR szStr2,
                            PSLUINT32 cchCmp);

#endif  /* _PSLSTRING_H_ */

