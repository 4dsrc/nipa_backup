/*
 *  MTPTransport.h
 *
 *  Contains declaration of MTP Transport related functionality.  This
 *  functionality is agnostic of the specific transport in use.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPTRANSPORT_H_
#define _MTPTRANSPORT_H_

/*  Transport Identifiers
 */

#define MTPTRANSPORTPARAM_UNKNOWN       0


/*  MTP Transport Events
 *
 *  The following events may be received by registering a transport monitor.
 */

enum
{
    MTPTRANSPORTEVENT_UNKNOWN =     MAKE_PSL_MSGID(MTP_TRANSPORTEVENT, 0, 0),
    MTPTRANSPORTEVENT_BOUND =       MAKE_PSL_MSGID(MTP_TRANSPORTEVENT, 1, 0),
    MTPTRANSPORTEVENT_CLOSED =      MAKE_PSL_MSGID(MTP_TRANSPORTEVENT, 2, 0),
};


/*  MTP Transport Monitor
 *
 *  API's for registering and unregistering transport monitors.  Transport
 *  monitor callbacks may be called on any thread so appropriate thread
 *  synchronization must be handled.
 *
 */

typedef PSLVOID (PSL_API* PFNMTPTRANSPORTMONITOR)(
                    PSLMSGID eventID,
                    PSLPARAM aParamTransport);

PSL_EXTERN_C PSLSTATUS PSL_API MTPTransportMonitorRegister(
                    PFNMTPTRANSPORTMONITOR pfnMTPTransportMonitor);

PSL_EXTERN_C PSLSTATUS PSL_API MTPTransportMonitorUnregister(
                    PFNMTPTRANSPORTMONITOR pfnMTPTransportMonitor);

#endif  /* _MTPTRANSPORT_H_ */

