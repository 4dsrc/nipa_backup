/*
 *  MTPInitialize.h
 *
 *  Contains the declaration of system initialize function
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPINITIALIZE_H_
#define _MTPINITIALIZE_H_

/*
 *  MTP Runtime Options Flags
 */

#define MTPINITFLAG_RESPONDER       0x80000000
#define MTPINITFLAG_INITIATOR       0x40000000
#define MTPINITFLAG_MODE_MASK       0xf0000000

#define MTPINITFLAG_USB_TRANSPORT   0x00000001
#define MTPINITFLAG_IP_TRANSPORT    0x00000002
#define MTPINITFLAG_BT_TRANSPORT    0x00000004
#define MTPINITFLAG_TRANSPORT_MASK  0x000000ff

/*
 *  MTPInitialize
 *
 *  This function will be called by the MTP system at application
 *  start up to initialize the core command handler and realted
 *  resources that are required to start processing MTP commands
 *  once the traport is ready
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  values less than zero indicates failure
 *  values greater than zero indicate success with more information
 *  for the user of the function
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPInitialize(PSLUINT32 dwInitFlags);

/*
 *  MTPUninitialize
 *
 *  This function will be called by the MTP system at application
 *  shutdown to uninitialize the command handlers and related
 *  resources.
 *
 *  Return Values
 *  PSLSUCCESS : indicates success
 *  values less than zero indicates failure
 *  values greater than zero indicate success with more information
 *  for the user of the function
 *
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPUninitialize();

#endif /*_MTPINITIALIZE_H_*/
