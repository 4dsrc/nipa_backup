/*
 *  MTPBTSockets.h
 *
 *  MTP Bluetooth socket wrapper.  This interface is designed to maintain
 *  consistency with standard BSD Socket operations while allowing platforms
 *  without Bluetooth L2CAP socket support to still support the MTP
 *  Bluetooth transport.
 *
 *  This interface is patterned on the L2CAP socket interface defined by
 *  the Netgraph documentation on FreeBSD (from man pages only- see
 *  http://www.freebsd.org/cgi/man.cgi?query=ng_btsocket&sektion=4&apropos=0&
 *  manpath=FreeBSD+6.3-RELEASE).  Only functionality necessary for MTP
 *  Bluetooth has been replicated here.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPBTSOCKETS_H_
#define _MTPBTSOCKETS_H_

/*  MTP Bluetooth Sockets are intended to match the behavior of the existing
 *  PSLSocket implemnetation- inlcuding the underlying standardized BSD
 *  Socket implementation.  In environments where this is not possible, it
 *  is the responsiblity of the BSP implementor to provide socket style
 *  behavior on top of Bluetooth L2CAP functionality.
 */

#include "PSLSockets.h"

/*  Generic types
 */

/*  Determine whether native Bluetooth socket support is available
 */

#ifdef MTP_NATIVE_BTSOCKETS

/*  Bluetooth Socket Defines
 */

#define MTPBT_AF_BLUETOOTH          AF_BLUETOOTH

#define MTPBT_PROTO_L2CAP           BLUETOOTH_PROTO_L2CAP

#define MTPBT_SOCK_SEQPACKET        SOCK_SEQPACKET

#define MTPBT_SO_L2CAP_IMTU         SO_L2CAP_IMTU
#define MTPBT_SO_L2CAP_OMTU         SO_L2CAP_OMTU

/*  Bluetooth Socket Types
 */

typedef struct bdaddr_t mtpbtbdaddr_t;
typedef struct sockaddr_l2cap mtpbtsockaddr_l2cap;

/*  Standard BSD Socket Functions
 */

#define MTPBTSocketCreate           socket
#define MTPBTSocketAccept           accept
#define MTPBTSocketBind             bind
#define MTPBTSocketConnect          connect
#define MTPBTSocketListen           listen
#define MTPBTSocketRecv             recv
#define MTPBTSocketSend             send
#define MTPBTSocketGetSockOpt       getsockopt
#define MTPBTSocketSetSockOpt       setsockopt
#define MTPBTSocketIoctlSocket      ioctlsocket
#define MTPBTSocketGetSockName      getsockname
#define MTPBTSocketGetPeerName      getpeername

/*  PSL Socket Functions
 */

#define MTPBTSocketInitialize       PSLSocketInitialize
#define MTPBTSocketUninitialize     PSLSocketUninitialize
#define MTPBTSocketMsgToString      PSLSocketMsgToString
#define MTPBTSocketErrorToPSLStatus PSLSocketErrorToPSLStatus
#define MTPBTSocketMsgSelect        PSLSocketMsgSelect
#define MTPBTSocketMsgSelectClose   PSLSocketMsgSelectClose
#define MTPBTSocketMsgReset         PSLSocketMsgReset
#define MTPBTSocketClose            PSLSocketClose

#else   /* !MTP_NATIVE_BTSOCKETS */

/*  Bluetooth Socket Defines
 */

#define MTPBT_AF_BLUETOOTH          0x01

#define MTPBT_PROTO_L2CAP           0x0001

#define MTPBT_SOCK_SEQPACKET        0x0001

#define MTPBT_SO_L2CAP_IMTU         0x0001
#define MTPBT_SO_L2CAP_OMTU         0x0002

/*  Bluetooth Socket Types
 */

#include "PSLPackPush1.h"

typedef struct _mtpbtbdaddr_t
{
    PSLUINT8        bdaddr[6];
} mtpbtbdaddr_t;

typedef struct _mtpbtsockaddr_l2cap
{
    PSLUINT8        l2cap_len;    /* total length */
    PSLUINT8        l2cap_family; /* address family */
    PSLUINT16       l2cap_psm;    /* Protocol/Service Multiplexor */
    mtpbtbdaddr_t   l2cap_bdaddr; /* address */
} mtpbtsockaddr_l2cap;

#include "PSLPackPop.h"

/*  Standard BSD Socket Functions
 */

PSL_EXTERN_C SOCKET BSDSOCKETS_API MTPBTSocketCreate(
                    int af,
                    int type,
                    int protocol);

PSL_EXTERN_C int BSDSOCKETS_API MTPBTSocketClose(
                    SOCKET s);

PSL_EXTERN_C SOCKET BSDSOCKETS_API MTPBTSocketAccept(
                    SOCKET s,
                    struct sockaddr* addr,
                    int* addrlen);

PSL_EXTERN_C int BSDSOCKETS_API MTPBTSocketBind(
                    SOCKET s,
                    PSL_CONST struct sockaddr* addr,
                    int namelen);

PSL_EXTERN_C int BSDSOCKETS_API MTPBTSocketConnect(
                    SOCKET s,
                    PSL_CONST struct sockaddr* name,
                    int namelen);

PSL_EXTERN_C int BSDSOCKETS_API MTPBTSocketListen(
                    SOCKET s,
                    int backlog);

PSL_EXTERN_C int BSDSOCKETS_API MTPBTSocketRecv(
                    SOCKET s,
                    char* buf,
                    int len,
                    int flags);

PSL_EXTERN_C int BSDSOCKETS_API MTPBTSocketSend(
                    SOCKET s,
                    PSL_CONST char* buf,
                    int len,
                    int flags);

PSL_EXTERN_C int BSDSOCKETS_API MTPBTSocketGetSockOpt(
                    SOCKET s,
                    int level,
                    int optname,
                    char* optval,
                    int* optlen);

PSL_EXTERN_C int BSDSOCKETS_API MTPBTSocketSetSockOpt(
                    SOCKET s,
                    int level,
                    int optname,
                    PSL_CONST char* optval,
                    int optlen);

PSL_EXTERN_C int BSDSOCKETS_API MTPBTSocketIOCTL(
                    SOCKET s,
                    long cmd,
                    u_long* argp);

PSL_EXTERN_C int BSDSOCKETS_API MTPBTSocketGetSockName(
                    SOCKET s,
                    struct sockaddr* name,
                    int* namelen);

PSL_EXTERN_C int BSDSOCKETS_API MTPBTSocketGetPeerName(
                    SOCKET s,
                    struct sockaddr* name,
                    int* namelen);


/*  PSL Socket Functions
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTSocketInitialize(PSLVOID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTSocketUninitialize(PSLVOID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTSocketErrorToPSLStatus(
                    SOCKET socket,
                    PSLINT32 errSock);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTSocketMsgSelect(
                    SOCKET sockSource,
                    PSLMSGQUEUE mqDest,
                    PSLMSGPOOL mpDest,
                    PSLPARAM pvParam,
                    PSLUINT32 dwSelectFlags);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTSocketMsgSelectClose(
                    SOCKET sockSource);

PSL_EXTERN_C PSLSTATUS PSL_API MTPBTSocketMsgReset(
                    SOCKET sockSource,
                    PSLBOOL fResetMask,
                    PSLUINT32 dwData);

#endif  /* MTP_NATIVE_BTSOCKETS */

/*  Generic Defines
 */

#define MTPBT_AF_BLUETOOTH_L2CAP \
    (PSLUINT16)((MTPBT_AF_BLUETOOTH << 8) | sizeof(mtpbtsockaddr_l2cap))

#define MTPBT_L2CAP_MAX_PACKET_SIZE 0xfe00

/*  Safe Helpers
 */

#define SAFE_MTPBTSOCKETCLOSE(_sock) \
if (INVALID_SOCKET != _sock) \
{ \
    MTPBTSocketClose(_sock); \
    _sock = INVALID_SOCKET; \
}


#endif  /* _MTPBTSOCKETS_H_ */



