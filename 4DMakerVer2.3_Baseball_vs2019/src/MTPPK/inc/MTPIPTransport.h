/*
 *  MTPIPTransport.h
 *
 *  Contains declarations of the MTP/IP transport.  This transport
 *  uses the PSL Socket abstraction and should be capable of running
 *  on any system with a IPv6/IPv4 capable BSD socket implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPIPTRANSPORT_H_
#define _MTPIPTRANSPORT_H_

#include "BSDSockets.h"

#define MTPIP_MAX_NAME_LENGTH           40
#define MTPIP_UDN_LENGTH                41

#ifndef MTPIP_MAX_COMMAND_PACKET
#define MTPIP_MAX_COMMAND_PACKET        0x00010000
#endif  /* MTPIP_MAX_COMMAND_PACKET */

#ifndef MTPIP_MAX_EVENT_PACKET
#define MTPIP_MAX_EVENT_PACKET          0x00004000
#endif  /* MTPIP_MAX_EVENT_PACKET */

/*  MTPIPBindInitiator
 *
 *  Binds a socket connection from an initiator to the MTP/IP
 *  Transport.  The socket may be either the command/data socket or
 *  the event socket.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPBindInitiator(
                    SOCKET sockInitiator,
                    PSL_CONST struct sockaddr* psaInitiator,
                    PSLUINT32 cbAddr,
                    PSLPARAM aParamTransport);

/*  MTPIPBindResponder
 *
 *  Binds the MTP/IP transport to the specifed responder.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPBindResponder(
                    PSLCWSTR szInitiatorName,
                    PSLMSGQUEUE mqEventHandler,
                    PSLLOOKUPTABLE hHandlerTable,
                    PSL_CONST struct sockaddr* psaResponder,
                    PSLUINT32 cbResponderAddr,
                    PSLUINT32 dwMSTimeout,
                    MTPCONTEXT** ppmtpctx);

/*  MTPIPUDNToMACAddressStr
 *
 *  Converts a UPnP UDN for an MTP/IP endpoint to a MAC address
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPUDNToMACAddress(
                    PSLCWSTR szUDN,
                    PSLWCHAR* rgchMACDest,
                    PSLUINT32 cchMACDest,
                    PSLUINT32* pcchMACUsed);


/*  MTPIPMACAddressStrToUDN
 *
 *  Converts a MAC address to the correct format for an MTP/IP fully
 *  qualified UDN.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPIPMACAddressStrToUDN(
                    PSLCWSTR szMAC,
                    PSLWCHAR* rgchUDNDest,
                    PSLUINT32 cchUDNDest,
                    PSLUINT32* pcchUDNUsed);

#endif  /* _MTPIPTRANSPORT_H_ */

