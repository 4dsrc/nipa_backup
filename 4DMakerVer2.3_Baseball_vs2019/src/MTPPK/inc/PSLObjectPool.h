/*
 *  PSLObjectPool.h
 *
 *  Declaration of the Object Pool support.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLOBJECTPOOL_H_
#define _PSLOBJECTPOOL_H_

typedef PSLHANDLE PSLOBJPOOL;

/*  Object pools can be used to avoid constantly asking the memory subsystem
 *  to allocate and free memory for each object.  Depending on the flag
 *  objects will either be allocated if the number of objects in the pool is
 *  exhausted or the request will fail.
 */

/*  Flags
 */

#define PSLOBJPOOLFLAG_ALLOW_ALLOC      0x00000001
#define PSLOBJPOOLFLAG_ZERO_MEMORY      0x00000002

/*  Creation and Destruction
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLObjectPoolCreate(
                PSLUINT32 cbObjectSize,
                PSLUINT32 cObjects,
                PSLUINT32 dwFlags,
                PSLOBJPOOL* pobjPool);

PSL_EXTERN_C PSLSTATUS PSL_API PSLObjectPoolDestroy(
                PSLOBJPOOL objPool);


/*  Allocate and Release Objects
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLObjectPoolAlloc(
                PSLOBJPOOL objPool,
                PSLVOID** ppobjNew);

PSL_EXTERN_C PSLSTATUS PSL_API PSLObjectPoolFree(
                PSLVOID* pobjDestroy);

/*  Safe Helpers
 */

#define SAFE_PSLOBJECTPOOLDESTROY(_p) \
if (PSLNULL != (_p)) \
{ \
    PSLObjectPoolDestroy(_p); \
    (_p) = PSLNULL; \
}

#define SAFE_PSLOBJECTPOOLFREE(_p) \
if (PSLNULL != (_p)) \
{ \
    PSLObjectPoolFree(_p); \
    (_p) = PSLNULL; \
}

#endif  /* _PSLOBJECTPOOL_H_ */

