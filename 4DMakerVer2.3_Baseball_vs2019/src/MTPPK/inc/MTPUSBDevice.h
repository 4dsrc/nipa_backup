/*
 *  MTPUSBDevice.h
 *
 *  Defines the MTP/USB Device interface
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPUSBDEVICE_H_
#define _MTPUSBDEVICE_H_

typedef PSLHANDLE           MTPUSBDEVICE;

/*  Force tight packing
 */

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPush1.h"
#endif

/*
 *  Standard USB Device Request
 *
 */

struct _MTPUSB_DEVICE_REQUEST
{
    PSLBYTE                 bmRequestType;
    PSLBYTE                 bRequest;
    PSLUINT16               wValue;
    PSLUINT16               wIndex;
    PSLUINT16               wLength;
} PSL_PACK1;

typedef struct _MTPUSB_DEVICE_REQUEST MTPUSB_DEVICE_REQUEST;

typedef struct _MTPUSB_DEVICE_REQUEST PSL_UNALIGNED* PXMTPUSB_DEVICE_REQUEST;
typedef PSL_CONST struct _MTPUSB_DEVICE_REQUEST PSL_UNALIGNED*
                            PCXMTPUSB_DEVICE_REQUEST;

#define MTPUSBDEVICEREQ_HOST_TO_DEVICE          0x00
#define MTPUSBDEVICEREQ_DEVICE_TO_HOST          0x10

#define MTPUSBDEVICEREQ_STANDARD                0x00
#define MTPUSBDEVICEREQ_CLASS                   0x20
#define MTPUSBDEVICEREQ_VENDOR                  0x40
#define MTPUSBDEVICEREQ_RESERVED                0x60

#define MTPUSBDEVICEREQ_FOR_DEVICE              0x00
#define MTPUSBDEVICEREQ_FOR_INTERFACE           0x01
#define MTPUSBDEVICEREQ_FOR_ENDPOINT            0x02
#define MTPUSBDEVICEREQ_FOR_OTHER               0x03

#define MTPUSBDEVICEREQ_CHECK_FLAGS(_pmdr, _f) \
        ((_f) == ((_f) & (_pmdr)->bmRequestType))

/*
 *  Recover default packing
 */

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPop.h"
#endif


/*
 *  MTP USB Device Information
 *
 */

typedef struct _MTPUSBDEVICEINFO
{
    PSLUINT32               cbPacketCommand;
    PSLUINT32               cbPacketData;
    PSLUINT32               cbMaxData;
    PSLUINT32               cbPacketResponse;
    PSLUINT32               cbPacketEvent;
    PSLUINT32               cbPacketControl;
} MTPUSBDEVICEINFO;


/*
 *  MTP/USB Buffers
 *
 */

enum
{
    MTPUSBBUFFER_CONTROL = MTPBUFFER_START_TRANSPORT_SPECIFIC,
    MTPUSBBUFFER_TRANSMIT_SHORT_DATA
};


/*
 *  MTP/USB Transport Messages
 *
 */

enum
{
    MTPUSBMSG_UNKNOWN =             MAKE_PSL_MSGID(MTP_TRANSPORT, 0, 0),
    MTPUSBMSG_DEVICE_RESET =        MAKE_PSL_MSGID(MTP_TRANSPORT, 1, 0),
    MTPUSBMSG_DEVICE_DISCONNECT =   MAKE_PSL_MSGID(MTP_TRANSPORT, 2, 0),
    MTPUSBMSG_BUFFER_AVAILABLE =    MAKE_PSL_MSGID(MTP_TRANSPORT, 3, MTPF_B),
    MTPUSBMSG_BUFFER_SENT =         MAKE_PSL_MSGID(MTP_TRANSPORT, 4, MTPF_PB),
    MTPUSBMSG_BUFFER_RECEIVED =     MAKE_PSL_MSGID(MTP_TRANSPORT, 5, MTPF_PB),
    MTPUSBMSG_SEND_FAILED =         MAKE_PSL_MSGID(MTP_TRANSPORT, 6, MTPF_PB),
    MTPUSBMSG_RECEIVE_FAILED =      MAKE_PSL_MSGID(MTP_TRANSPORT, 7, MTPF_PB),
    MTPUSBMSG_SEND_CANCELLED =      MAKE_PSL_MSGID(MTP_TRANSPORT, 8, MTPF_PB),
    MTPUSBMSG_RECEIVE_CANCELLED =   MAKE_PSL_MSGID(MTP_TRANSPORT, 9, MTPF_PB),
    MTPUSBMSG_SEND_INCOMPLETE =     MAKE_PSL_MSGID(MTP_TRANSPORT, 10, MTPF_PB),
    MTPUSBMSG_RECEIVE_INCOMPLETE =  MAKE_PSL_MSGID(MTP_TRANSPORT, 11, MTPF_PB),
    MTPUSBMSG_NOT_RESPONDING =      MAKE_PSL_MSGID(MTP_TRANSPORT, 12, MTPF_PB),
    MTPUSBMSG_BUFFER_ERROR =        MAKE_PSL_MSGID(MTP_TRANSPORT, 13, MTPF_PB),
};


/*
 *  PFNMTPUSBDEVICEHANDLECLASSREQUEST
 *
 *  MTP USB Device class request callback.  This function is called directly
 *  from the device whenever a class request needs to be processed.  Because
 *  of the USB timing requirements of control pipe handling, this request
 *  must be processed immediately rather than allowing it to be routed via
 *  the standard device messages.  Note that the callback may happen on the
 *  thread context of the device so care must be taken to handle any
 *  synchronization required.
 *
 */

typedef PSLSTATUS (PSL_API* PFNMTPUSBDEVICEHANDLECLASSREQUEST)(
                            PSLPARAM aParamClassReq,
                            PSLVOID* pvRequest,
                            PSLUINT64 cbRequest);

/*
 *  MTPUSBDeviceBind
 *
 *  Binds a queue to the USB device to receive notifications of events
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceBind(
                            MTPUSBDEVICE hUSBDeviceSrc,
                            PSLMSGQUEUE mqDest,
                            PFNMTPUSBDEVICEHANDLECLASSREQUEST pfnHandleClassReq,
                            PSLPARAM aParamClassReq);


/*
 *  MTPUSBDeviceUnbind
 *
 *  Removes a monitor from the USB device
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceUnbind(
                            MTPUSBDEVICE hUSBDeviceSrc,
                            PSLMSGQUEUE mqDest);


/*
 *  MTPUSBDeviceGetInfo
 *
 *  Retrieves information about the USB device specified
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceGetInfo(
                            MTPUSBDEVICE hUSBDevice,
                            MTPUSBDEVICEINFO* pusbdI);


/*
 *  MTPUSBDeviceReset
 *
 *  Resets the device cancelling all pending transfers as well as releasing
 *  any pending buffer requests.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceReset(
                            MTPUSBDEVICE hUSBDevice);


/*
 *  MTPUSBDeviceRequestBuffer
 *
 *  Requests a buffer of the specified type from the USB device
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceRequestBuffer(
                            MTPUSBDEVICE hUSBDevice,
                            PSLUINT32 dwBufferType,
                            PSLUINT32 cbRequested,
                            PSLPARAM aParam,
                            PSLVOID** ppvBuffer,
                            PSLUINT32* cbActual);


/*
 *  MTPUSBDeviceReleaseBuffer
 *
 *  Releases the specified buffer back to the USB device
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceReleaseBuffer(
                            MTPUSBDEVICE hUSBDevice,
                            PSLVOID* pvBuffer);


/*
 *  MTPUSBDeviceReceiveBuffer
 *
 *  Requests that the specified buffer type be recieved from the appropriate
 *  USB pipe
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceReceiveBuffer(
                            MTPUSBDEVICE hUSBDevice,
                            PSLVOID* pvBuffer,
                            PSLPARAM aParam,
                            PSLPARAM* paCookie);


/*
 *  MTPUSBDeviceSendBuffer
 *
 *  Requests that the specified buffer provided be sent over the USB
 *  connection
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceSendBuffer(
                            MTPUSBDEVICE hUSBDevice,
                            PSLUINT32 dwPacketFlag,
                            PSLVOID* pvBuffer,
                            PSLUINT32 cbSend,
                            PSLPARAM aParam,
                            PSLPARAM* paCookie);


/*
 *  MTPUSBDeviceCancelRequest
 *
 *  Cancels a pending send or receive request
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceCancelRequest(
                            MTPUSBDEVICE hUSBDevice,
                            PSLPARAM aCookie);


/*
 *  MTPUSBDeviceStallPipe
 *
 *  Stalls the pipe associated with the specified buffer type
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceStallPipe(
                            MTPUSBDEVICE hUSBDevice,
                            PSLUINT32 dwBufferType);


/*
 *  MTPUSBDeviceClearPipeStall
 *
 *  Clears a stall on the pipe associated with the specified buffer type
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceClearPipeStall(
                            MTPUSBDEVICE hUSBDevice,
                            PSLUINT32 dwBufferType);


/*
 *  MTPUSBDeviceSendControlHandshake
 *
 *  Sends the appropriate control handshake to indicate the end of a
 *  control transaction.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBDeviceSendControlHandshake(
                            MTPUSBDEVICE hUSBDevice);

#endif  /* _MTPUSBDEVICE_H_ */

