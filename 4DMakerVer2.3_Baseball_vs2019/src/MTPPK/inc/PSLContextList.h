/*
 *  PSLContextList.h
 *
 *  Contains the definition for the context list.  This list is valuable
 *  when you need to enumerate a particular state of the list (it's context)
 *  while still allowing the list to be dynamically updated.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLCONTEXTLIST_H_
#define _PSLCONTEXTLIST_H_

typedef PSLHANDLE PSLCTXLIST;

/*
 *  PSLCtxListCreate
 *
 *  Create a context list
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLCtxListCreate(
                    PSLCTXLIST* pclist);

/*
 *  PSLCtxListDestroy
 *
 *  Closes a context list
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLCtxListDestroy(
                    PSLCTXLIST clist);

/*
 *  PSLCtxListRegister
 *
 *  Registers an object with the list
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLCtxListRegister(
                    PSLCTXLIST clist,
                    PSLPARAM aParam);

/*
 *  PSLCtxListUnregister
 *
 *  Removes the specified object from the list
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLCtxListUnregister(
                    PSLCTXLIST clist,
                    PSLPARAM aParam);


/*
 *  PSLCtxListEnum
 *
 *  Calls the enumeration proc for each element in the list at the time
 *  PSLCtxListEnum is called.  If an element is removed from the list while
 *  PSLCtxListEnum is operating and it has yet to be forwarded to the
 *  enumeration proc it will be removed from the list and not forwarded.  If
 *  an item is added to the list while PSLCtxListEnum is operating it will
 *  not be forwarded to the enum proc as it does not represent the context of
 *  the list at the time PSLCtxListEnum was called.
 *
 */

typedef PSLSTATUS (PSL_API* PFNENUMCTXLISTPROC)(
                    PSLPARAM aParam,
                    PSLPARAM aEnumParam);

PSL_EXTERN_C PSLSTATUS PSL_API PSLCtxListEnum(
                    PSLCTXLIST clist,
                    PFNENUMCTXLISTPROC pfnEnumCtxListProc,
                    PSLPARAM aEnumParam);


/*
 *  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#define SAFE_PSLCTXLISTDESTROY(_cl) \
if (PSLNULL != (_cl)) \
{ \
    PSLCtxListDestroy(_cl); \
    (_cl) = PSLNULL; \
}


#endif  /* _PSLCONTEXTLIST_H_ */

