/*
 *  PSLErrors.h
 *
 *  Contains PSL error codes.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLERRORS_H_
#define _PSLERRORS_H_

#define MAKE_PSLERROR_CODE(_ec)     (PSL_FAILED(_ec) ? _ec : -1 * (_ec))
#define MAKE_PSLSUCCESS_CODE(_sc)   (PSL_SUCCEEDED(_sc) ? _sc : -1 * (_sc))

/*
 *  PSL User Status
 *
 *  This macro defines the starting range for errors that are "assign" for
 *  definition by users of the PSL system
 *
 */

#define PSL_USER_STATUS             32768

/*
 *  PSL Error Codes
 *
 */

enum
{
    /*
     *  General Errors (1 to 1023)
     *
     *  Error codes in this range can be used as a general error code
     *  returned by any component
     *
     */

    PSLERROR_INVALID_PARAMETER =            MAKE_PSLERROR_CODE(1),
    PSLERROR_NOT_IMPLEMENTED =              MAKE_PSLERROR_CODE(2),
    PSLERROR_OUT_OF_MEMORY =                MAKE_PSLERROR_CODE(3),
    PSLERROR_UNEXPECTED =                   MAKE_PSLERROR_CODE(4),
    PSLERROR_PLATFORM_FAILURE =             MAKE_PSLERROR_CODE(5),
    PSLERROR_NOT_AVAILABLE =                MAKE_PSLERROR_CODE(6),
    PSLERROR_SHUTTING_DOWN =                MAKE_PSLERROR_CODE(7),
    PSLERROR_IN_USE =                       MAKE_PSLERROR_CODE(8),
    PSLERROR_INVALID_OPERATION =            MAKE_PSLERROR_CODE(9),
    PSLERROR_INVALID_RANGE =                MAKE_PSLERROR_CODE(10),
    PSLERROR_NOT_INITIALIZED =              MAKE_PSLERROR_CODE(11),
    PSLERROR_LIBRARY_NOT_FOUND =            MAKE_PSLERROR_CODE(12),
    PSLERROR_ACCESS_DENIED =                MAKE_PSLERROR_CODE(13),
    PSLERROR_FAILURE =                      MAKE_PSLERROR_CODE(14),

    /* Buffer Errors */

    PSLERROR_INSUFFICIENT_BUFFER =          MAKE_PSLERROR_CODE(50),
    PSLERROR_INVALID_BUFFER =               MAKE_PSLERROR_CODE(51),
    PSLERROR_UNSUPPORTED_VERSION =          MAKE_PSLERROR_CODE(53),
    PSLERROR_INVALID_PACKET =               MAKE_PSLERROR_CODE(54),
    PSLERROR_INVALID_DATA =                 MAKE_PSLERROR_CODE(55),

    /* Network Errors */

    PSLERROR_NETWORK_FAILURE =              MAKE_PSLERROR_CODE(100),
    PSLERROR_CONNECTION_CLOSED =            MAKE_PSLERROR_CODE(101),
    PSLERROR_CONNECTION_NOT_FOUND =         MAKE_PSLERROR_CODE(102),
    PSLERROR_INVALID_ADDRESS =              MAKE_PSLERROR_CODE(103),
    PSLERROR_DATA_CORRUPT =                 MAKE_PSLERROR_CODE(104),
    PSLERROR_CONNECTION_CORRUPT =           MAKE_PSLERROR_CODE(105),

    /* USB Errors */

    PSLERROR_USB_FAILURE =                  MAKE_PSLERROR_CODE(120),

    /*
     *  Queue Errors (1024 to 1152)
     *
     */

    PSLERROR_QUEUE_CREATE =                 MAKE_PSLERROR_CODE(1024),
    PSLERROR_QUEUE_DESTROY =                MAKE_PSLERROR_CODE(1025),
    PSLERROR_QUEUE_LOCK =                   MAKE_PSLERROR_CODE(1026),
    PSLERROR_QUEUE_UNLOCK =                 MAKE_PSLERROR_CODE(1027),
    PSLERROR_QUEUE_ONWAIT =                 MAKE_PSLERROR_CODE(1028),
    PSLERROR_QUEUE_ONPOST =                 MAKE_PSLERROR_CODE(1029),
};

/*
 *  PSL Success Codes
 *
 */

enum
{
    PSLSUCCESS =                            MAKE_PSLSUCCESS_CODE(0),
    PSLSUCCESS_FALSE =                      MAKE_PSLSUCCESS_CODE(1),
    PSLSUCCESS_CANCEL =                     MAKE_PSLSUCCESS_CODE(2),
    PSLSUCCESS_EXISTS =                     MAKE_PSLSUCCESS_CODE(3),
    PSLSUCCESS_WOULD_BLOCK =                MAKE_PSLSUCCESS_CODE(4),
    PSLSUCCESS_BLOCKING =                   MAKE_PSLSUCCESS_CODE(5),
    PSLSUCCESS_TIMEOUT =                    MAKE_PSLSUCCESS_CODE(6),
    PSLSUCCESS_NOT_FOUND =                  MAKE_PSLSUCCESS_CODE(7),
    PSLSUCCESS_NOT_AVAILABLE =              MAKE_PSLSUCCESS_CODE(8),
    PSLSUCCESS_SINGLE_THREADED =            MAKE_PSLSUCCESS_CODE(9),
    PSLSUCCESS_DATA_AVAILABLE =             MAKE_PSLSUCCESS_CODE(10),
};


#endif  /* _PSLERRORS_H_ */

