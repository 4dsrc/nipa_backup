/*
 *  MTPSerialize.h
 *
 *  Root implementation of the MTP Serialization utilities.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MTPSERIALIZE_H_
#define _MTPSERIALIZE_H_

typedef PSLHANDLE MTPSERIALIZE;

/*  Base MTP Serialization Structure
 *
 *  The Base MTP Serialization helper object is designed to allow flexible
 *  encoding of MTP-compatible data buffers in a generic way.  The
 *  functionality of the base engine is driven by the table provided to it
 *  which defines the set of data to be encoded into the buffer.  As buffer
 *  space allows, the engine will place the data into the provided buffer
 *  in the proper format.  The engine will correctly handle double-buffering
 *  only that portion of data which must span a buffer boundary to limit
 *  memory copy overhead while still enabling clients of the serialization
 *  object to deal with data in atomic units.
 *
 *  The serializer must be provided with a template to define the data that
 *  is to be put into the destination buffer.  The data in this template
 *  may be stored by value, by reference, as an offset from the provided
 *  context, or supplied by a function call.  Not all MTP types support all
 *  forms of serialization encodings:
 *
 *              BYVALUE     BYREFERENCE     BYOFFSETVAL BYOFFSETREF BYFUNCTION
 *  UINT8       *           *               *           *           *
 *  UINT16      *           *               *           *           *
 *  UINT32      *           *               *           *           *
 *  UINT64                  *               *           *           *
 *  UINT128                 *               *           *           *
 *  All Arrays              *               *           *           *
 *  Strings                 *               *           *           *
 *
 *  In general the aValue field always contains the value itself.  The
 *  encoding of the value determines what form the data takes:
 *
 *      BYVALUE:        aValue contains the value itself.  aValue will be
 *                      casted to the appropriate type and then stored in the
 *                      serialization buffer.
 *      BYREFERENCE:    aValue contains a pointer to the value.  aValue will
 *                      be casted to the appropriate pointer type and then the
 *                      contents at that location stored in the serialization
 *                      buffer.  If an array type is specified the pointer is
 *                      to the first item in the array.
 *      BYOFFSETVAL:    aValue contains a byte offset to the value from
 *                      the pointer specified as pvItemOffset.  To retrieve
 *                      the value, the value of pvItemOffset will be cast
 *                      to a PSLBYTE* location, the offset in aValue added, and
 *                      then the contents at that location stored in the
 *                      serialization buffer.  If an array type is specified
 *                      the pointer is to the first item in the array.
 *      BYOFFSETREF:    aValue containds a byte offset.  The offset in aValue
 *                      specifies an offset from pvItemOffset  to a pointer to
 *                      the actual value.  If the flag MTPSERIALIZEFLAG_REFOFFSET
 *                      is set (not supported for arrays) an additional byte
 *                      offset is in aValueContext and is added to the
 *                      secondary pointer to generate the final pointer to the
 *                      value.  This value is read from the memory location
 *                      specified by the pointer and stored in the serialization
 *                      buffer.  If an array type is specified the final
 *                      reference is to the first item in the array.
 *      BYFUNCTION:     aValue contains a pointer to either a function of
 *                      type PFNMTPSERIALIZEVALUE or PFNMTPSERIALIZEARRAY.
 *                      For simple types, the function will be called directly
 *                      to encode the value into the buffer with aValueContext
 *                      passed in addition to aRootContext.  For array types,
 *                      the function will be called multiple times and for
 *                      different purposes.  If MTPSERIALIZEARRAYOP_GET_COUNT
 *                      is specified as the operation the function is to return
 *                      the number of items in the array.  For each item in
 *                      the array the function will be called with the operation
 *                      MTPSERIALIZEARRAYOP_GET_ITEM specified and the index
 *                      of the item being retrieved.  For array types both
 *                      the aRootContext and the aValue context will be passed
 *                      to the callback function.
 *
 *  aValueContext Usage
 *
 *  The aValueContext is used to provide additional information about the
 *  context of the value being serialized.  In some cases the information
 *  is used directly by the Serializer while in others it is handed off:
 *      Arrays:         If an array is specified as being encoded as
 *                      either BYREFERENCE or BYOFFSET, the number of elements
 *                      in the array pointed to by the aValue parameter is
 *                      placed in the aValueContext field.
 *      Strings:        If a string is specified as being encoded as either
 *                      BYREFERENCE or BYOFFSET, the number of *characters* in
 *                      the string not including the NULL terminator may be
 *                      specified in the aValueContext field.  If the length
 *                      of the string is to be calculated set the aValueContext
 *                      field to 0.
 *      BYREFERENCE     Additional operations may be used to further modify
 *      BYOFFSETVAL     the standard behavior of these operations.  The use
 *      BYOFFSETREF     of the MTPSERIALIZEFLAG_VALOFFSET and
 *                      MTPSERIALIZEFLAG_REFOFFSET cause an additional byte
 *                      offset to be added to the location determined by the
 *                      typical BYREFERENCE/BYOFFSETVAL/BYOFFSETREF behavior
 *                      before the data is transferred.  When
 *                      MTPSERIALIZEFLAG_VALOFFSET is specified, the additional
 *                      offset is added to the standard behavior offsetting
 *                      the source for the value by the offset specified. If
 *                      MTPSERIALIZEFLAG_REFOFFSET is specified, the additional
 *                      offset is added to the standard behavior and then the
 *                      contents of memory at the specified location are used
 *                      to determine the source of the serialization data.
 *
 *  MTP_DATATYPE_UNKNOWN Handling
 *
 *  MTP_DATATYPE_UNKNOWN is a special type that is used to handle custom cases
 *  and child templates.  The following behaviors are supported:
 *
 *      BYFUNCTION:         The function specified in aValue is called directly
 *                          to retrieve the serialized value.  No additional
 *                          support is provided.  This is the recommended way to
 *                          handle "do it myself" serialization as opposed to
 *                          using MTP_DATATYPE_AUINT8 (this will always caused
 *                          the length of the blob to be serialized first which
 *                          may break the serialization structure).
 *      BYTEMPLATE:         Specifies that a new child template is to be used.
 *                          Serialization of the current template will continue
 *                          upon completion of the child template.  Child
 *                          template and context information is retrieved using
 *                          the pointer to an MTPSERIALIZETEMPLATEVTBL specified
 *                          in aValue.
 *      BYTEMPLATEREPEAT:   Specifies that a new child template is to be used.
 *                          This template will be repeated zero or more times.
 *                          Serialization of the current template continues
 *                          following completion of the final repeat of the
 *                          child template.  Child template, count, and
 *                          per-repeat context information is retrieved via
 *                          the pointer to a MTPSERIALIZETEMPLATEREPEATVTBL
 *                          specified in aValue.
 */


#define MTPSERIALIZEFLAG_BYVALUE            0x0001
#define MTPSERIALIZEFLAG_BYREFERENCE        0x0002
#define MTPSERIALIZEFLAG_BYOFFSETVAL        0x0004
#define MTPSERIALIZEFLAG_BYOFFSETREF        0x0008
#define MTPSERIALIZEFLAG_BYFUNCTION         0x0010
#define MTPSERIALIZEFLAG_BYTEMPLATE         0x0020
#define MTPSERIALIZEFLAG_BYTEMPLATEREPEAT   0x0040
#define MTPSERIALIZEFLAG_ENCODING_MASK      0x007f

#define MTPSERIALIZEFLAG_VALOFFSET          0x0100
#define MTPSERIALIZEFLAG_REFOFFSET          0x0200
#define MTPSERIALIZEFLAG_CONTEXTOFFSET_MASK 0x0300

#define MTPSERIALIZEFLAG_PSLGUID            0x0400
#define MTPSERIALIZEFLAG_PSLPKEY            0x0800

#define MTPSERIALIZE_INFINITE_REPEAT        0xffffffff

typedef struct _MTPSERIALIZEINFO
{
    PSLUINT16               wType;
    PSLUINT16               wFlags;
    PSLPARAM                aValue;
    PSLPARAM                aValueContext;
} MTPSERIALIZEINFO;

typedef PSLVOID (PSL_API* PFNONMTPSERIALIZEDESTROY)(
                    PSL_CONST MTPSERIALIZEINFO* rgmsiTemplate,
                    PSLPARAM aContext,
                    PSL_CONST PSLVOID* pvOffset);

typedef PSLSTATUS (PSL_API* PFNMTPSERIALIZEGETCOUNT)(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT32* pcItems);

typedef PSLSTATUS (PSL_API* PFNMTPSERIALIZEGETITEM)(
                    PSLUINT32 idxItem,
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

typedef PSLSTATUS (PSL_API* PFNMTPSERIALIZEVALUE)(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLUINT16 wType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

typedef PSLSTATUS (PSL_API* PFNMTPSERIALIZEGETTEMPLATE)(
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSL_CONST MTPSERIALIZEINFO** prgmsiTemplate,
                    PSLUINT32* pcItems,
                    PFNONMTPSERIALIZEDESTROY* ppfnOnDestroy);

typedef PSLSTATUS (PSL_API* PFNMTPSERIALIZEGETCONTEXT)(
                    PSLUINT32 idxItem,
                    PSLPARAM aContext,
                    PSLPARAM aValueContext,
                    PSLPARAM* paItemContext,
                    PSL_CONST PSLVOID** ppvItemOffset);

typedef struct _MTPSERIALIZEARRAYVTBL
{
    PFNMTPSERIALIZEGETCOUNT     pfnMTPSerializeGetCount;
    PFNMTPSERIALIZEGETITEM      pfnMTPSerializeGetItem;
} MTPSERIALIZEARRAYVTBL;

typedef struct _MTPSERIALIZETEMPLATEVTBL
{
    PFNMTPSERIALIZEGETTEMPLATE  pfnMTPSerializeGetTemplate;
    PFNMTPSERIALIZEGETCONTEXT   pfnMTPSerializeGetContext;
} MTPSERIALIZETEMPLATEVTBL;

typedef struct _MTPSERIALIZETEMPLATEREPEATVTBL
{
    MTPSERIALIZETEMPLATEVTBL    vtblTemplate;
    PFNMTPSERIALIZEGETCOUNT     pfnMTPSerializeGetCount;
} MTPSERIALIZETEMPLATEREPEATVTBL;

/*  Creation/Destruction
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPSerializeCreate(
                    PSL_CONST MTPSERIALIZEINFO* rgmsiTemplate,
                    PSLUINT32 cItems,
                    PSLPARAM aRootContext,
                    PSL_CONST PSLVOID* pvRootOffset,
                    PFNONMTPSERIALIZEDESTROY pfnOnDestroy,
                    MTPSERIALIZE* pmso);

PSL_EXTERN_C PSLSTATUS PSL_API MTPSerializeDestroy(
                    MTPSERIALIZE mso);

/* Serialization
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPSerialize(
                    MTPSERIALIZE mso,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);


/*  Safe Helpers
 */

#define SAFE_MTPSERIALIZEDESTROY(_p) \
if (PSLNULL != (_p)) \
{ \
    MTPSerializeDestroy(_p); \
    (_p) = PSLNULL; \
}

#endif  /* _MTPSERIALIZE_H_ */
