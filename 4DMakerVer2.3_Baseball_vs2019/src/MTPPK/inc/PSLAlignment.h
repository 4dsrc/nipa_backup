/*
 *  PSLAlignment.h
 *
 *  Collection of helper macros and functions to deal with alignment fixups
 *  on CPUs with alignment sensitivity.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLALIGNMENT_H_
#define _PSLALIGNMENT_H_

#ifdef PSL_ALIGNMENT_INTRINSIC

#define PSLLoadAlignUInt16(_ps)         (*(_ps))
#define PSLStoreAlignUInt16(_pd, _w)    (*(_pd) = _w)
#define PSLCopyAlignUInt16(_pd, _ps)    (*(_pd) = *(_ps))

#define PSLLoadAlignUInt32(_ps)         (*(_ps))
#define PSLStoreAlignUInt32(_pd, _dw)   (*(_pd) = _dw)
#define PSLCopyAlignUInt32(_pd, _ps)    (*(_pd) = *(_ps))

#define PSLLoadAlignUInt64(_ps)         (*(_ps))
#define PSLStoreAlignUInt64(_pd, _qw)   (*(_pd) = _qw)
#define PSLCopyAlignUInt64(_pd, _ps)    (*(_pd) = *(_ps))

#define PSLLoadAlignDouble(_ps)         (*(_ps))
#define PSLStoreAlignDouble(_pd, _d)    (*(_pd) = _d)
#define PSLCopyAlignDouble(_pd, _ps)    (*(_pd) = *(_ps))

#else   /* !PSL_ALIGNMENT_INTRINSIC */

PSL_EXTERN_C PSLUINT16 PSL_API PSLLoadAlignUInt16(
                            PSL_CONST PSLUINT16 PSL_UNALIGNED* pSrc);

PSL_EXTERN_C PSLUINT16 PSL_API PSLStoreAlignUInt16(
                            PSLUINT16 PSL_UNALIGNED* pDest,
                            PSLUINT16 wValue);

PSL_EXTERN_C PSLVOID PSL_API PSLCopyAlignUInt16(
                            PSLUINT16 PSL_UNALIGNED* pDest,
                            PSL_CONST PSLUINT16 PSL_UNALIGNED* pSrc);

PSL_EXTERN_C PSLUINT32 PSL_API PSLLoadAlignUInt32(
                            PSL_CONST PSLUINT32 PSL_UNALIGNED* pSrc);

PSL_EXTERN_C PSLUINT32 PSL_API PSLStoreAlignUInt32(
                            PSLUINT32 PSL_UNALIGNED* pDest,
                            PSLUINT32 dwValue);

PSL_EXTERN_C PSLVOID PSL_API PSLCopyAlignUInt32(
                            PSLUINT32 PSL_UNALIGNED* pDest,
                            PSL_CONST PSLUINT32 PSL_UNALIGNED* pSrc);

PSL_EXTERN_C PSLUINT64 PSL_API PSLLoadAlignUInt64(
                            PSL_CONST PSLUINT64 PSL_UNALIGNED* pSrc);

PSL_EXTERN_C PSLUINT64 PSL_API PSLStoreAlignUInt64(
                            PSLUINT64 PSL_UNALIGNED* pDest,
                            PSLUINT64 qwValue);

PSL_EXTERN_C PSLVOID PSL_API PSLCopyAlignUInt64(
                            PSLUINT64 PSL_UNALIGNED* pDest,
                            PSL_CONST PSLUINT64 PSL_UNALIGNED* pSrc);

PSL_EXTERN_C PSLDOUBLE PSL_API PSLLoadAlignDouble(
                            PSL_CONST PSLDOUBLE PSL_UNALIGNED* pSrc);

PSL_EXTERN_C PSLDOUBLE PSL_API PSLStoreAlignDouble(
                            PSLDOUBLE PSL_UNALIGNED* pDest,
                            PSLDOUBLE dValue);

PSL_EXTERN_C PSLVOID PSL_API PSLCopyAlignDouble(
                            PSLDOUBLE PSL_UNALIGNED* pDest,
                            PSL_CONST PSLDOUBLE PSL_UNALIGNED* pSrc);

#endif  /* PSL_ALIGNMENT_INTRINSIC */

#define PSLLoadAlignBool(_ps)           PSLLoadAlignUInt32(_ps)
#define PSLStoreAlignBool(_pd, _b)      PSLStoreAlignUInt32(_pd, _b)
#define PSLCopyAlignBool(_pd, _ps)      PSLCopyAlignUInt32(_pd, _ps)

#define PSLLoadAlignWChar(_ps)          PSLLoadAlignUInt16(_ps)
#define PSLStoreAlignWChar(_pd, _b)     PSLStoreAlignUInt16(_pd, _b)
#define PSLCopyAlignWChar(_pd, _ps)     PSLCopyAlignUInt16(_pd, _ps)

#define PSLLoadAlignUInt128(_pd, _ps)   PSLCopyAlignUInt128(_pd, _ps)
#define PSLStoreAlignUInt128(_pd, _ps)  PSLCopyAlignUInt128(_pd, _ps)

PSL_EXTERN_C PSLVOID PSL_API PSLCopyAlignUInt128(
                            PSLUINT128 PSL_UNALIGNED* pDest,
                            PSL_CONST PSLUINT128 PSL_UNALIGNED* pSrc);

#endif  /* _PSLALIGNMENT_H_ */

