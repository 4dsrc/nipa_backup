/*
 *  MTPStructs.h
 *
 *  Contains delcarations of MTP structures.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPSTRUCTS_
#define _MTPSTRUCTS_

#define MAX_MTP_STRING_LEN 255

/* Maximum number of paramters that can be contained in
 * response buffer
 */
#define MTPRESPONSE_NUMPARAMS_MAX 5

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPush1.h"
#endif

/*
 * The MTP string structure contains Unicode string (16-bit)
 * that is limited to MAX_MTP_STRING_LEN in length.
 * The default string length is defined as
 * 255 including the NULL terminating character.
 *
 * NOTE: The default type "MTPSTRING" is not defined due to the
 * frequent source of alignment issues that it causes in other
 * code.
 */
struct _MTPSTRING
{
    PSLBYTE     cchLen;
    PSLWCHAR    rgchStr[MAX_MTP_STRING_LEN];
} PSL_PACK1;

typedef struct _MTPSTRING MTPSTRING;
typedef struct _MTPSTRING PSL_UNALIGNED* PXMTPSTRING;
typedef PSL_CONST struct _MTPSTRING PSL_UNALIGNED* PCXMTPSTRING;

/*  Following data set will be used when handling operation request.
 *  When a parameter is not used, it will be set to 0x00000000.
 *  When a parameter occupies less than 32 bits,
 *  the least-significant bits shall be used,
 *  with the unused bits being set to 0.
 */

struct _MTPOPERATION
{
    PSLUINT16   wOpCode;
    PSLUINT32   dwTransactionID;
    PSLUINT32   dwParam1;
    PSLUINT32   dwParam2;
    PSLUINT32   dwParam3;
    PSLUINT32   dwParam4;
    PSLUINT32   dwParam5;
} PSL_PACK1;

typedef struct _MTPOPERATION MTPOPERATION;

typedef struct _MTPOPERATION PSL_UNALIGNED* PXMTPOPERATION;
typedef PSL_CONST struct _MTPOPERATION PSL_UNALIGNED* PCXMTPOPERATION;

#define MTPOPERATION_MIN_SIZE \
        PSLOFFSETOF(MTPOPERATION, dwParam1)

#define MTPOPERATION_SIZE(_cp) \
        (MTPOPERATION_MIN_SIZE + (sizeof(PSLUINT32) * (_cp)))

/*  Following data set will be used when sending response
 *  When a parameter is not used, it will be set to 0x00000000.
 *  When a parameter occupies less than 32 bits,
 *  the least-significant bits shall be used,
 *  with the unused bits being set to 0.
 */

struct _MTPRESPONSE
{
    PSLUINT16   wRespCode;
    PSLUINT32   dwTransactionID;
    PSLUINT32   dwParam1;
    PSLUINT32   dwParam2;
    PSLUINT32   dwParam3;
    PSLUINT32   dwParam4;
    PSLUINT32   dwParam5;
} PSL_PACK1;

typedef struct _MTPRESPONSE MTPRESPONSE;

typedef struct _MTPRESPONSE PSL_UNALIGNED* PXMTPRESPONSE;
typedef PSL_CONST struct _MTPRESPONSE PSL_UNALIGNED* PCXMTPRESPONSE;

#define MTPRESPONSE_MIN_SIZE \
        PSLOFFSETOF(MTPRESPONSE, dwParam1)

/*  Following data set will be used when sending events
 *  Events may have at most three parameters, and all must
 *  occupy 32 bits. If a parameter occupies less than 32 bits,
 *  it should be aligned to the least significant portion
 *  of this field. The interpretation of event parameters
 *  depends upon the event with which they are being sent.
 */

struct _MTPEVENT
{
    PSLUINT16   wEventCode;
    PSLUINT32   dwTransactionID;
    PSLUINT32   dwParam1;
    PSLUINT32   dwParam2;
    PSLUINT32   dwParam3;
} PSL_PACK1;

typedef struct _MTPEVENT MTPEVENT;

typedef struct _MTPEVENT PSL_UNALIGNED* PXMTPEVENT;
typedef PSL_CONST struct _MTPEVENT PSL_UNALIGNED* PCXMTPEVENT;

#define MTPEVENT_MIN_SIZE \
        PSLOFFSETOF(MTPEVENT, dwParam1)

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPop.h"
#endif

#endif /*_MTPSTRUCTS_*/
