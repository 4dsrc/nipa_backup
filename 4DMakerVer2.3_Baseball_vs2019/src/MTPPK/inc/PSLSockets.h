/*
 *  PSLSockets.h
 *
 *  Contains declarations of the PSL extensions to the standard BSD
 *  Sockets API.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  BSD Socket Support
 *
 *  While not technically part of the ANSI C foundation, the near
 *  ubiqutous adoption of the Berkeley Socket API in modern operating
 *  systems means that for default networking operations we will
 *  not attempt to "duplicate" the BSD Socket API simply to have
 *  a "PSL abstraction" for networking.  It is assumed that somewhere
 *  on the include path a file "BSDSockets.h" exists which can be
 *  used to get the declarations of the standard BSD socket API.
 *
 *  While BSD Sockets is the de-facto standard, it is limited by the
 *  fact that it does not gracefully handle asynchronous operation.
 *  The APIs provided here are designed to add asynchronous a
 *  consistent model for asynchronous behavior while maintaining
 *  compatibility with the traditional BSD Socket API.
 *
 */

#ifndef _PSLSOCKETS_H_
#define _PSLSOCKETS_H_

#include <BSDSockets.h>

/*
 *  Network Messages
 *
 *  Each of these messages places the socket sending attached to the
 *  message as an argument in dwParam1.
 */

enum
{
    PSLSOCKETMSG_UNKNOWN =              MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 0),
    PSLSOCKETMSG_DATA_AVAILABLE =       MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 1),
    PSLSOCKETMSG_READY_TO_SEND =        MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 2),
    PSLSOCKETMSG_OUT_OF_BAND =          MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 3),
    PSLSOCKETMSG_ACCEPT =               MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 4),
    PSLSOCKETMSG_CONNECT =              MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 5),
    PSLSOCKETMSG_CLOSE =                MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 6),
    PSLSOCKETMSG_RESET =                MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 7),
    PSLSOCKETMSG_QOS =                  MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 8),
    PSLSOCKETMSG_GROUP_QOS =            MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 9),
    PSLSOCKETMSG_ROUTE_IFACE_CHANGE =   MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 10),
    PSLSOCKETMSG_ADDRESS_LIST_CHANGE =  MAKE_PSL_NORMAL_MSGID(PSL_SOCKET, 11),
};


/*
 *  PSLSocketInitialize
 *
 *  Called to initialize the PSL socket support
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLSocketInitialize(PSLVOID);


/*
 *  PSLSocketUninitialize
 *
 *  Called to uninitialize the PSL socket support
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLSocketUninitialize(PSLVOID);

/*
 *  PSLSocketErrorToPSLStatus
 *
 *  Converts the last socket error to a PSLSTATUS result.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLSocketErrorToPSLStatus(
                            SOCKET socket,
                            PSLINT32 errSock);


/*
 *  PSLSocketMsgSelect
 *
 *  When a network event from the specified bitmask of BSD Socket
 *  events (see BSD Socket select API) occurs a network message is
 *  sent to the specified message queue.  The socket will be set to
 *  non-blocking mode when this call is issued.
 *
 *  Setting the select flags to 0 will disable sending of messages.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLSocketMsgSelect(
                            SOCKET sockSource,
                            PSLMSGQUEUE mqDest,
                            PSLMSGPOOL mpDest,
                            PSLPARAM pvParam,
                            PSLUINT32 dwSelectFlags);


/*
 *  PSLSocketMsgSelectClose
 *
 *  Unbinds the specified socket from the message based eventing mechanism.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLSocketMsgSelectClose(
                            SOCKET sockSource);


/*
 *  PSLSocketMsgReset
 *
 *  Once a socket event message has been sent, PSLSocketMsgSelect will
 *  hold all future notifications of the same event until this function
 *  has been called to reset the message state.  Note that messages
 *  will not be generated for events that have already occurred
 *  (e.g. a data available event will not be sent if data is currently
 *  available on the socket) before the message was reset.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLSocketMsgReset(
                            SOCKET sockSource,
                            PSLBOOL fResetMask,
                            PSLUINT32 dwData);

/*
 *  PSLSocketClose
 *
 *  Closes a socket.  Use of this call guarantees that any sockets registered
 *  with PSLSocketMsgSelect are removed from the list before the socket
 *  is completely closed.
 *
 *  NOTE: The signature of this function is designed to match the standard
 *  closesocket() signature from BSD Sockets rather than being a PSL_API
 *  version.
 *
 */

PSL_EXTERN_C int BSDSOCKETS_API PSLSocketClose(
                            SOCKET sock);


/*
 *  PSLSocketMsgToString
 *
 *  Converts the specified socket message to a string for debugging purposes
 *
 */

PSL_EXTERN_C PSLCSTR PSL_API PSLSocketMsgToString(
                            PSLMSGID msgID);

/*
 *  Safe Helpers
 *
 */

#define SAFE_PSLSOCKETCLOSE(_sock) \
if (INVALID_SOCKET != _sock) \
{ \
    PSLSocketClose(_sock); \
    _sock = INVALID_SOCKET; \
}

#endif  /* _PSLSOCKETS_H_ */

