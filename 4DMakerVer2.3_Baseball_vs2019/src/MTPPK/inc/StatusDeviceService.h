/*
 *  StatusDeviceService.h
 *
 *  Contains definitions of the Status Device Service
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The service definition specified in this file has been designed for use
 *  with Microsoft Windows.  To request additions to this file, including new
 *  service properties, object formats, object properties, methods, and events,
 *  please contact Microsoft at askmtp@microsoft.com with your request to
 *  maintain interoperability.
 *
 *  Custom service properties, object formats, object properties, methods,
 *  and events may also be added by generating new format, method, or event
 *  GUIDs or by creating new PKeys in a privately generated and managed
 *  namespace.
 *
 */

#ifndef _STATUSDEVICESERVICE_H_
#define _STATUSDEVICESERVICE_H_

#include <DeviceServices.h>

/*****************************************************************************/
/*  Status Service Info                                                      */
/*****************************************************************************/

DEFINE_DEVSVCGUID(SERVICE_Status,
    0x0B9F1048, 0xB94B, 0xDC9A, 0x4e, 0xd7, 0xfe, 0x4f, 0xed, 0x3a, 0x0d, 0xeb);

#define NAME_StatusSvc                      L"Status"
#define TYPE_StatusSvc                      DEVSVCTYPE_DEFAULT

/*****************************************************************************/
/*  Status Service Property Keys                                             */
/*****************************************************************************/

DEFINE_DEVSVCGUID(NAMESPACE_StatusSvc,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13);


/*  PKEY_StatusSvc_SignalStrength
 *
 *  Signal strength in "bars" from 0 (no signal) to 4 (excellent signal)
 *
 *  Type: UInt8
 *  Form: Range
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_SignalStrength,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    2);

#define NAME_StatusSvc_SignalStrength       L"SignalStrength"

#define RANGEMIN_StatusSvc_SignalStrength   0
#define RANGEMAX_StatusSvc_SignalStrength   4
#define RANGESTEP_StatusSvc_SignalStrength  1

/*  PKEY_StatusSvc_TextMessages
 *
 *  Number of unread text messages (255 max)
 *
 *  Type: UInt8
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_TextMessages,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    3);

#define NAME_StatusSvc_TextMessages         L"TextMessages"
#define RANGEMAX_StatusSvc_TextMessages     255

/*  PKEY_StatusSvc_NewPictures
 *
 *  Number of "new" pictures on the device (65535 max)
 *
 *  Type: UInt16
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_NewPictures,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    4);

#define NAME_StatusSvc_NewPictures          L"NewPictures"
#define RANGEMAX_StatusSvc_NewPictures      65535

/*  PKEY_StatusSvc_MissedCalls
 *
 *  Number of missed calls on the device (255 max)
 *
 *  Type: UInt8
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_MissedCalls,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    5);

#define NAME_StatusSvc_MissedCalls          L"MissedCalls"
#define RANGEMAX_StatusSvc_MissedCalls      255


/*  PKEY_StatusSvc_VoiceMail
 *
 *  Number of "available" voice mail messages (255 max)
 *
 *  Type: UInt8
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_VoiceMail,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    6);

#define NAME_StatusSvc_VoiceMail            L"VoiceMail"
#define RANGEMAX_StatusSvc_VoiceMail        255


/*  PKEY_StatusSvc_NetworkName
 *
 *  Network provider network name
 *
 *  Type: String
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_NetworkName,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    7);

#define NAME_StatusSvc_NetworkName          L"NetworkName"


/*  PKEY_StatusSvc_NetworkType
 *
 *  Network "type" (e.g. GPRS, EDGE, UMTS, 1xRTT, EVDO, or operator branded)
 *
 *  Type: String
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_NetworkType,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    8);

#define NAME_StatusSvc_NetworkType          L"NetworkType"


/*  PKEY_StatusSvc_Roaming
 *
 *  Current network roaming state
 *
 *  Type: UInt8
 *  Form: Enum
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_Roaming,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    9);

#define NAME_StatusSvc_Roaming              L"Roaming"

#define ENUM_StatusSvc_RoamingInactive      0x00
#define ENUM_StatusSvc_RoamingActive        0x01
#define ENUM_StatusSvc_RoamingUnknown       0x02


/*  PKEY_StatusSvc_BatteryLife
 *
 *  Remaining battery life on the device as a percentage between 100 and 0.
 *
 *  Type: UInt8
 *  Form: Range
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_BatteryLife,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    10);

#define NAME_StatusSvc_BatteryLife          L"BatteryLife"

#define RANGEMIN_StatusSvc_BatteryLife      0
#define RANGEMAX_StatusSvc_BatteryLife      100
#define RANGESTEP_StatusSvc_BatteryLife     1


/*  PKEY_StatusSvc_ChargingState
 *
 *  Current charging state of the device
 *
 *  Type: UInt8
 *  Form: Enum
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_ChargingState,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    11);

#define NAME_StatusSvc_ChargingState        L"ChargingState"

#define ENUM_StatusSvc_ChargingInactive     0x00
#define ENUM_StatusSvc_ChargingActive       0x01
#define ENUM_StatusSvc_ChargingUnknown      0x02


/*  PKEY_StatusSvc_StorageCapacity
 *
 *  Total storage capacity on the device (across all storages)
 *
 *  Type: UInt64
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_StorageCapacity,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    12);

#define NAME_StatusSvc_StorageCapacity      L"StorageCapacity"


/*  PKEY_StatusSvc_StorageFreeSpace
 *
 *  Total free storage capacity on the device (across all storages)
 *
 *  Type: UInt64
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_StatusSvc_StorageFreeSpace,
    0x49cd1f76, 0x5626, 0x4b17, 0xa4, 0xe8, 0x18, 0xb4, 0xaa, 0x1a, 0x22, 0x13,
    13);

#define NAME_StatusSvc_StorageFreeSpace     L"StorageFreeSpace"

#endif  /* _STATUSDEVICESERVICE_H_ */

