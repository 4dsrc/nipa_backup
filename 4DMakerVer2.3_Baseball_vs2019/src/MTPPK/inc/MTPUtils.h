/*
 *  MTPUtils.h
 *
 *  Collection of helpful utilities within the MTP environment.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPUTILS_H_
#define _MTPUTILS_H_

/*  Byte Swapping and Alignment Support
 */

#ifdef PSL_LITTLE_ENDIAN

#define MTPSwapUInt16(_val)         (_val)
#define MTPSwapUInt32(_val)         (_val)

#elif defined(PSL_BIG_ENDIAN)

#define MTPSwapUInt16(_val) \
    (((_val) >> 8) | (((_val) & 0xff) << 8))

#define MTPSwapUInt32(_val) \
    (((((_val) >> 8) | (((_val) & 0x00ff0000) << 8)) >> 16) | \
     ((((_val) & 0x0000ff00 >> 8) | (((_val) & 0x000000ff) << 8)) << 16))

#else

#error PSL_LITTLE_ENDIAN or PSL_BIG_ENDIAN not defined

#endif  /* Byte Order */

#ifdef MTP_STORAGE_INLINE

#ifdef PSL_LITTLE_ENDIAN

#define MTPStoreUInt64(_pd, _ps)    PSLCopyAlignUInt64(_pd, _ps)
#define MTPCopyUInt64(_pd, _ps)     PSLCopyAlignUInt64(_pd, _ps)

#define MTPStoreUInt128(_pd, _ps)   PSLCopyAlignUInt128(_pd, _ps)
#define MTPCopyUInt128(_pd, _ps)    PSLCopyAlignUInt128(_pd, _ps)

#else

#define MTPStoreUInt64(_pd, _ps) \
        PSLASSERT((PSLNULL != (_pd)) && (PSLNULL != (_ps))); \
        MTPStoreUInt32((PSLUINT32*)((PSLBYTE*)_pd + sizeof(PSLUINT32)), \
                            *((PSLUINT32*)(_ps))); \
        MTPStoreUInt32((PSLUINT32*)(_pd), \
                            *((PSLUINT32*)((PSLBYTE*)_ps + sizeof(PSLUINT32))))

#define MTPCopyUInt64(_pd, _ps) \
        PSLASSERT((PSLNULL != (_pd)) && (PSLNULL != (_ps))); \
        MTPCopyUInt32((PSLUINT32*)((PSLBYTE*)_pd + sizeof(PSLUINT32)), \
                            ((PSLUINT32*)(_ps))); \
        MTPCopyUInt32((PSLUINT32*)(_pd), \
                            ((PSLUINT32*)((PSLBYTE*)_ps + sizeof(PSLUINT32))))

#define MTPStoreUInt128(_pd, _ps) \
        PSLASSERT((PSLNULL != (_pd)) && (PSLNULL != (_ps))); \
        MTPStoreUInt64((PSLUINT64*)((PSLBYTE*)_pd + sizeof(PSLUINT64)), \
                            *((PSLUINT64*)(_ps))); \
        MTPStoreUInt64((PSLUINT64*)(_pd), \
                            *((PSLUINT64*)((PSLBYTE*)_ps + sizeof(PSLUINT64))))

#define MTPCopyUInt128(_pd, _ps) \
        PSLASSERT((PSLNULL != (_pd)) && (PSLNULL != (_ps))); \
        MTPCopyUInt64((PSLUINT64*)((PSLBYTE*)_pd + sizeof(PSLUINT64)), \
                            ((PSLUINT64*)(_ps))); \
        MTPCopyUInt64((PSLUINT64*)(_pd), \
                            ((PSLUINT64*)((PSLBYTE*)_ps + sizeof(PSLUINT64))))

#endif  /* Byte Order */

#define MTPLoadUInt16(_ps)      MTPSwapUInt16(PSLLoadAlignUInt16(_ps))
#define MTPLoadUInt32(_ps)      MTPSwapUInt32(PSLLoadAlignUInt32(_ps))

#define MTPStoreUInt16(_pd, _v) PSLStoreAlignUInt16(_pd, MTPSwapUInt16(_v))
#define MTPStoreUInt32(_pd, _v) PSLStoreAlignUInt32(_pd, MTPSwapUInt32(_v))

#define MTPCopyUInt16(_pd, _ps) PSLStoreAlignUInt16(_pd, MTPLoadUInt16(_ps))
#define MTPCopyUInt32(_pd, _ps) PSLStoreAlignUInt32(_pd, MTPLoadUInt32(_ps))

#else   /* !MTP_STORAGE_INLINE */

PSL_EXTERN_C PSLUINT16 PSL_API MTPLoadUInt16(
                            PSL_CONST PSLUINT16 PSL_UNALIGNED* pwSrc);

PSL_EXTERN_C PSLVOID PSL_API MTPStoreUInt16(
                            PSLUINT16 PSL_UNALIGNED* pwDest,
                            PSLUINT16 wSrc);

PSL_EXTERN_C PSLVOID PSL_API MTPCopyUInt16(
                            PSLUINT16 PSL_UNALIGNED* pwDest,
                            PSL_CONST PSLUINT16 PSL_UNALIGNED* pwSrc);

PSL_EXTERN_C PSLUINT32 PSL_API MTPLoadUInt32(
                            PSL_CONST PSLUINT32 PSL_UNALIGNED* pdwSrc);

PSL_EXTERN_C PSLVOID PSL_API MTPStoreUInt32(
                            PSLUINT32 PSL_UNALIGNED* pdwDest,
                            PSLUINT32 dwSrc);

PSL_EXTERN_C PSLVOID PSL_API MTPCopyUInt32(
                            PSLUINT32 PSL_UNALIGNED* pdwDest,
                            PSL_CONST PSLUINT32 PSL_UNALIGNED * pdwSrc);

PSL_EXTERN_C PSLVOID PSL_API MTPStoreUInt64(
                            PSLUINT64 PSL_UNALIGNED* pqwDest,
                            PSL_CONST PSLUINT64* pqwSrc);

PSL_EXTERN_C PSLVOID PSL_API MTPCopyUInt64(
                            PSLUINT64 PSL_UNALIGNED* pqwDest,
                            PSL_CONST PSLUINT64 PSL_UNALIGNED* pqwSrc);

PSL_EXTERN_C PSLVOID PSL_API MTPStoreUInt128(
                            PSLUINT128 PSL_UNALIGNED* pdqwDest,
                            PSL_CONST PSLUINT128* pdqwSrc);

PSL_EXTERN_C PSLVOID PSL_API MTPCopyUInt128(
                            PSLUINT128 PSL_UNALIGNED* pdqwDest,
                            PSL_CONST PSLUINT128 PSL_UNALIGNED* pdqwSrc);

#endif  /* MTP_STORAGE_INLINE */

PSL_EXTERN_C PSLUINT64 PSL_API MTPLoadUInt64(
                            PSL_CONST PSLUINT64 PSL_UNALIGNED* pqwSrc);

#define MTPLoadUInt128(_pd, _ps)    MTPCopyUInt128(_pd, _ps)

PSL_EXTERN_C PSLVOID PSL_API MTPLoadUIntArray(
                            PSLVOID* pvDest,
                            PSL_CONST PSLVOID* pvSrc,
                            PSLUINT32 cItems,
                            PSLUINT32 cbItem);

PSL_EXTERN_C PSLVOID PSL_API MTPStoreUIntArray(
                            PSLVOID* pvDest,
                            PSL_CONST PSLVOID* pvSrc,
                            PSLUINT32 cItems,
                            PSLUINT32 cbItem);

#define MTPLoadGUID(_pd, _ps) \
        MTPCopyUInt32(&((_pd)->Data1), &((_ps)->Data1)); \
        MTPCopyUInt16(&((_pd)->Data2), &((_ps)->Data2)); \
        MTPCopyUInt16(&((_pd)->Data3), &((_ps)->Data3)); \
        PSLCopyMemory(&((_pd)->Data4), &((_ps)->Data4), sizeof((_pd)->Data4))

#define MTPCopyGUID(_pd, _ps)       MTPLoadGUID(_pd, _ps)

#define MTPStoreGUID(_pd, _ps) \
        MTPStoreUInt32(&((_pd)->Data1), (_ps)->Data1); \
        MTPStoreUInt16(&((_pd)->Data2), (_ps)->Data2); \
        MTPStoreUInt16(&((_pd)->Data3), (_ps)->Data3); \
        PSLCopyMemory(&((_pd)->Data4), &((_ps)->Data4), sizeof((_pd)->Data4))

#define MTPLoadPKey(_pd, _ps) \
        MTPLoadGUID(&((_pd)->nsid), &((_ps)->nsid)); \
        ((_pd)->pid) = MTPLoadUInt32(&((_ps)->pid))

#define MTPCopyPKey(_pd, _ps)       MTPLoadPKey(_pd, _ps)

#define MTPStorePKey(_pd, _ps) \
        MTPStoreGUID(&((_pd)->nsid), &((_ps)->nsid)); \
        MTPStoreUInt32(&((_pd)->pid), (_ps)->pid)

/*  Data Driven Load and Store Functions
 *
 *  These functions will load and store data of the specified size between the
 *  source and destination values
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPLoadValue(
                            PSLVOID* pvDest,
                            PSLUINT32 cbDest,
                            PSLUINT16 wDataType,
                            PSL_CONST PSLVOID* pvSrc,
                            PSLUINT32 cbSrc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStoreValue(
                            PSLVOID* pvDest,
                            PSLUINT32 cbDest,
                            PSLUINT16 wDataType,
                            PSL_CONST PSLVOID* pvSrc);

/*  MTP String Support
 *
 *  The APIs below assume that information is being loaded out of a UTF-16LE
 *  string per MTP standard behavior and converting it into the appropriate
 *  UTF-16 format for the platform.  Both basic and MTPSTRING support is
 *  provided.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUTF16StringLen(
                            PSL_CONST PSLWCHAR PSL_UNALIGNED* szSrc,
                            PSLUINT32 cchMaxSrc,
                            PSLUINT32* pcchSrc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPLoadUTF16String(
                            PSLWCHAR* szDest,
                            PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest,
                            PSL_CONST PSLWCHAR PSL_UNALIGNED* szSrc,
                            PSLUINT32 cchSrc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStoreUTF16String(
                            PSLWCHAR PSL_UNALIGNED* szDest,
                            PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest,
                            PSLCWSTR szSrc,
                            PSLUINT32 cchSrc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPLoadMTPString(
                            PSLWCHAR* szDest,
                            PSLUINT32 cchMaxDest,
                            PSLUINT32* pcchDest,
                            PCXMTPSTRING pmstrSrc,
                            PSLUINT32 cbSrc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStoreMTPString(
                            PXMTPSTRING pmstrDest,
                            PSLUINT32 cbDest,
                            PSLUINT32* cbWritten,
                            PSLCWSTR szSrc,
                            PSLUINT32 cchSrc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPStringCopy(
                            PXMTPSTRING pmstrDest,
                            PSLUINT32 cbDest,
                            PCXMTPSTRING pmstrSrc);

PSL_EXTERN_C PSLINT32 PSL_API MTPStringCmp(
                            PCXMTPSTRING pmstr1,
                            PCXMTPSTRING pmstr2);

PSL_EXTERN_C PSLINT32 PSL_API MTPStringICmp(
                            PCXMTPSTRING pmstr1,
                            PCXMTPSTRING pmstr2);

#endif  /* _MTPUTILS_H_ */

