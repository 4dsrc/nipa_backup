/*
 *  MTPRouter.h
 *
 *  Contains delcarations for routing operations supported by MTP
 *  responder implementations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPROUTER_H_
#define _MTPROUTER_H_

/*
 *  MTPROUTEUPDATE
 *
 *  Declaration of the route update structure.  This structure is
 *  provided to the router when an route is ended so that the route
 *  state may be updated.
 *
 */

typedef struct
{
    PSLLOOKUPTABLE          hHandlerTableNew;
} MTPROUTEUPDATE;


/*
 *  Route creation and destruction
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPRouteCreate(
                            PSLMSGQUEUE mqTransport,
                            MTPCONTEXT** ppmtpctx);

PSL_EXTERN_C PSLSTATUS PSL_API MTPRouteDestroy(
                            MTPCONTEXT* pmtpctx);


/*
 *  Route Management Functions
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPRouteBegin(
                            MTPCONTEXT* pmtpctx,
                            PSLMSGPOOL mpTransport,
                            PCXMTPOPERATION pmtpOp,
                            PSLUINT32 cmtpOp);

PSL_EXTERN_C PSLSTATUS PSL_API MTPRouteEnd(
                            MTPCONTEXT* pmtpctx,
                            MTPROUTEUPDATE* pmtpru);

/*
 *  Routing Functions
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPRouteMsgToTransport(
                            MTPCONTEXT* pmtpctx,
                            PSLMSG* pMsg);

PSL_EXTERN_C PSLSTATUS PSL_API MTPRouteMsgToHandler(
                            MTPCONTEXT* pmtpctx,
                            PSLMSG* pMsg);

PSL_EXTERN_C PSLSTATUS PSL_API MTPRouteTransportError(
                            MTPCONTEXT* pmtpctx,
                            PSLMSGPOOL mpSrc,
                            PSLMSGID msgID,
                            PSLUINT32 dwSequenceID,
                            PSLSTATUS psErr);

/*
 *  Safe Functions
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#ifndef SAFE_MTPROUTEDESTROY
#define SAFE_MTPROUTEDESTROY(_r) \
if (PSLNULL != (_r)) \
{ \
    MTPRouteDestroy(_r); \
    (_r) = PSLNULL; \
}
#endif  /* SAFE_MTPROUTEDESTROY */

#endif  /* _MTPROUTER_H_ */

