/*
 *  MTPContext.h
 *
 *  Contains declaration of the MTP context structure.  An MTP context is
 *  associated with each active transport to help manage the current state
 *  of the MTP protocol.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPCONTEXT_H_
#define _MTPCONTEXT_H_

/*
 *  MTPHANDLERSTATE
 *
 *  Handlers can be in different states during an MTP operation.
 *
 */

enum
{
    MTPHANDLERSTATE_UNKNOWN = 0,
    MTPHANDLERSTATE_INITIALIZED,
    MTPHANDLERSTATE_DATA_IN,
    MTPHANDLERSTATE_DATA_OUT,
    MTPHANDLERSTATE_RESPONSE
};


/*
 *  MTPCONTEXT
 *
 *  Declaration of the MTP route structure holds state about the current
 *  participants in an MTP communication.  Contexts apply to both router
 *  (responder) and proxy (initiator) connections.  Each transport endpoint
 *  should be associated with an MTPCONTEXT.
 *
 *  Given the threaded nature of the MTP system architecture the MTPCONTEXT
 *  structure is the only place to store context specific information.  All
 *  MTP defined messages include the current MTPCONTEXT as the first parameter
 *  in the message.
 *
 *  To maintain thread safety write access to members of the MTPCONTEXT
 *  structure is restricted by policy to the owners of the data indicated.
 *  In order to support read safety it is recommended that access to the
 *  MTPCONTEXT structure use the MTPAtomic* function set to provide atomic
 *  access to the members of the structure.
 *
 */

typedef struct
{
    /*
     *  Context State
     *
     *  Context state is managed by the router (responder) or the proxy
     *  (initiator).
     *
     */

    struct
    {
        PSLLOOKUPTABLE      hHandlerTable;
        PSLMSGQUEUE         mqTransport;
        PSLMSGQUEUE         mqHandler;
        PSLMSGQUEUE         mqEventHandler;
    } mtpContextState;

    /*
     *  Transport State
     *
     *  Transport state is managed by the transport.  Since transports
     *  may have different requirements on the amount of data needed to
     *  store state the size of the data is not fixed by the structure.
     *  All transports will be queried for the amount of data storage
     *  space needed and a buffer large enough to satisfy the maximum
     *  request will be allocated.
     *
     */

    struct
    {
        PSLBYTE*            pbTransportData;
        PSLUINT32           cbTransportData;
    } mtpTransportState;

    /*
     *  Handler State
     *
     *  Handler state is managed by the handler.  In the case of the router
     *  (responder) this is a command handler.  In the case of the proxy
     *  (initiator) this is the event handler or proxy handler.
     *
     *  Like transports, handlers may have different requirements on the
     *  amount of memory required to store state.  The MTPCONTEXT will
     *  always contain enough space to fulfill the maximum size requested.
     *
     */

    struct
    {
        PSLUINT32           dwHandlerState;
        PSLBYTE*            pbHandlerData;
        PSLUINT32           cbHandlerData;
    } mtpHandlerState;
} MTPCONTEXT;


/*
 *  MTPContextReserveTransportBytes
 *
 *  Specifies the number of bytes to be reserved in the context for the
 *  transport.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPContextReserveTransportBytes(
                            PSLUINT32 cbReserve);

/*
 *  MTPContextGetTransportReserve
 *
 *  Returns the number of bytes reserved by the transports for use in the
 *  context.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPContextGetTransportReserve(
                            PSLUINT32* pcbReserve);


/*
 *  MTPContextReserveHandlerBytes
 *
 *  Specifies the number of bytes to be reserved in the context for the
 *  handler.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPContextReserveHandlerBytes(
                            PSLUINT32 cbReserve);

/*
 *  MTPContextGetHandlerReserve
 *
 *  Returns the number of bytes reserved by the handlers for use in the
 *  context.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPContextGetHandlerReserve(
                            PSLUINT32* pcbReserve);

#endif  /* _MTPCONTEXT_H_ */

