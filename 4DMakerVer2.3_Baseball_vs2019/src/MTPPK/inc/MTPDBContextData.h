/*
 *  MTPDBContextData.h
 *
 *  Contains declaration of the MTP DB Context Data base class implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPDBCONTEXTDATA_H_
#define _MTPDBCONTEXTDATA_H_

/*  MTP DB Context Data Base Implementation
 *
 *  This implementation may be used as a starting place for other
 *  MTPDBCONTEXTDATA implementations.  It provides basic support for
 *  serialization and deserialization through the MTP Serializer
 *  functionality.
 */

typedef PSLHANDLE MTPDBCONTEXTBASE;

/*  Standard DB Context Virtual Functions
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDataBaseClose(
                    MTPDBCONTEXTDATA mdbcd);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDataBaseCancel(
                    MTPDBCONTEXTDATA mdbcd);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDataBaseSerialize(
                    MTPDBCONTEXTDATA mdbcd,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDataBaseStartDeserialize(
                    MTPDBCONTEXTDATA mdbcd,
                    PSLUINT64 cbData);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDataBaseDeserialize(
                    MTPDBCONTEXTDATA mdbcd,
                    PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLMSGQUEUE mqHandler,
                    MTPCONTEXT* pmtpctx,
                    PSLPARAM aParam,
                    PSLUINT32 dwMsgFlags);

/*  Extended Virtual Operations
 */

/*  pfnMTPDBContextBaseGetItem - PURE
 *
 *  Used by the DBContextData implementations to get information about
 *  specific items in a DBContext
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTBASEGETITEM)(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32 idxItem,
                    PSLUINT32 dwFormat,
                    PSLUINT32* pdwObjectID,
                    PSLPARAM* paParamItem,
                    PSL_CONST MTPFORMATINFO** ppfiObject);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextBaseGetItemBase(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32 idxItem,
                    PSLUINT32 dwFormat,
                    PSLUINT32* pdwObjectID,
                    PSLPARAM* paParamItem,
                    PSL_CONST MTPFORMATINFO** ppfiObject);


/*  pfnMTPDBContextBaseCloseItem
 *
 *  Closes an item opened with a call to pfnMTPDBContextBaseGetItem
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTBASECLOSEITEM)(
                    MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextBaseCloseItemBase(
                    MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem);


/*  pfnMTPDBContextBaseCommitItem
 *
 *  Commits changes to the specified item
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTBASECOMMITITEM)(
                    MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextBaseCommitItemBase(
                    MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem);


/*  pfnMTPDBContextBaseIsPropSupported
 *
 *  Used by the DBContextData implementations to get information about
 *  specific items in a DBContext.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTBASEISPROPSUPPORTED)(
                    MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem,
                    PSLUINT16 wPropCode,
                    PSLPARAM aContextProp);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextBaseIsPropSupportedBase(
                    MTPDBCONTEXT mdbc,
                    PSLPARAM aParamItem,
                    PSLUINT16 wPropCode,
                    PSLPARAM aContextProp);


/*  MTPDBCONTEXTBASEVTBL
 *
 *  Virtual Table definition for the base DBContext implementation
 */

typedef struct _MTPDBCONTEXTBASEVTBL
{
    MTPDBCONTEXTVTBL                    vtblDBC;
    PFNMTPDBCONTEXTBASEGETITEM          pfnMTPDBContextBaseGetItem;
    PFNMTPDBCONTEXTBASECLOSEITEM        pfnMTPDBContextBaseCloseItem;
    PFNMTPDBCONTEXTBASECOMMITITEM       pfnMTPDBContextBaseCommitItem;
    PFNMTPDBCONTEXTBASEISPROPSUPPORTED  pfnMTPDBContextBaseIsPropSupported;
} MTPDBCONTEXTBASEVTBL;


/*  MTPDBCONTEXTBASEOBJ
 *
 *  Base object for the MTPDBCONTEXTBASEOBJ class
 */

typedef struct _MTPDBCONTEXTBASEOBJ
{
    MTPDBCONTEXTOBJ                 mtpDBCBO;
#ifdef PSL_ASSERTS
    PSLUINT32                       dwCookie;
#endif  /* PSL_ASSERTS */
    PSLUINT32                       dwCtxObjectID;
    PSLUINT32                       dwCtxParentID;
    PSLUINT32                       dwCtxDepth;
    PSLUINT16                       wCtxFormat;
} MTPDBCONTEXTBASEOBJ;

typedef PSL_CONST MTPDBCONTEXTBASEOBJ* PMTPDBCONTEXTBASEOBJ;

/*  MTP DB Service Base Virtual Function Helpers
 */

#define GET_MTPDBCONTEXTBASEVTBL(_po) \
    ((MTPDBCONTEXTBASEVTBL*)(((PMTPDBCONTEXTBASEOBJ)_po)->mtpDBCBO.vtbl))

#define MTPDBContextBaseGetItem(_po, _i, _dwF, _pdw, _pa, _ppfi) \
    GET_MTPDBCONTEXTBASEVTBL(_po)->pfnMTPDBContextBaseGetItem(_po, _i, _dwF, _pdw, _pa, _ppfi)

#define MTPDBContextBaseCloseItem(_po, _aP) \
    GET_MTPDBCONTEXTBASEVTBL(_po)->pfnMTPDBContextBaseCloseItem(_po, _aP)

#define MTPDBContextBaseCommitItem(_po, _aP) \
    GET_MTPDBCONTEXTBASEVTBL(_po)->pfnMTPDBContextBaseCommitItem(_po, _aP)

#define MTPDBContextBaseIsPropSupported(_po, _aP, _w, _aC) \
    GET_MTPDBCONTEXTBASEVTBL(_po)->pfnMTPDBContextBaseIsPropSupported(_po, _aP, _w, _aC)


/*  Callback Function
 *
 *  The base implementation will provide a callback notification whenever the
 *  object is closed.  If you desire this notification you must register a
 *  callback function.
 */

typedef PSLVOID (PSL_API* PFNONMTPDBCONTEXTDATABASECLOSE)(
                    PSLPARAM aParam,
                    PSL_CONST PSLVOID* pvData);

/*  MTPDBCONTEXTDATABASEOBJ
 *
 *  This is the core object for the DB Context Data Base implementation.  To
 *  inherit from this object you must include this as the first item in your
 *  structure.
 */

typedef struct _MTPDBCONTEXTDATABASEOBJ
{
    MTPDBCONTEXTDATAOBJ             mdbcdObj;
#ifdef PSL_ASSERTS
    PSLUINT32                       dwCookie;
#endif  /*  PSL_ASSERTS */
    MTPSERIALIZE                    msSource;
    PSLPARAM                        aParam;
    PSL_CONST PSLVOID*              pvData;
    PFNONMTPDBCONTEXTDATABASECLOSE  pfnOnClose;
} MTPDBCONTEXTDATABASEOBJ;


/*  MTPDBContextDataBaseCreate
 *
 *  Creates an instance of the base object leaving all values empty with the
 *  exception of setting the VTable to point to the MTPDBContextDataBase
 *  implementation.  Specify the total size of the subclass to reserve
 *  additional space for your object.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDataBaseCreate(
                    PSLUINT32 cbObject,
                    MTPDBCONTEXTDATA* pmdcd);


/*  Storage Info Context Data
 *
 *  Handles serialization of the standard Storage Info dataset
 */

typedef struct _MTPSTORAGEINFO
{
    PSLUINT16               wStorageType;
    PSLUINT16               wFileSystemType;
    PSLUINT16               wAccessCapability;
    PSLUINT64               qwMaxCapacity;
    PSLCWSTR                szStorageDescription;
    PSLCWSTR                szVolumeIdentifier;
} MTPSTORAGEINFO;

PSL_EXTERN_C PSLSTATUS PSL_API MTPStorageInfoDBContextDataCreate(
                    PSL_CONST MTPSTORAGEINFO* pStorageInfo,
                    PSLUINT32 dwFormat,
                    PSLUINT64 qwFreeSpace,
                    PSLUINT32 dwFreeObjects,
                    MTPDBCONTEXTDATA* pmdbcd);


/*  Format Capabilities Context Data
 *
 *  Creates a context data for handling the MTP GetFormatCapabilities dataset
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPFormatCapabilitiesContextDataCreate(
                    PSL_CONST MTPFORMATINFO* pFormatInfo,
                    PSLUINT32 cFormats,
                    PSLUINT32 dwFormat,
                    PSLUINT16 wFormat,
                    PSLPARAM aParam,
                    PFNONMTPDBCONTEXTDATABASECLOSE pfnOnClose,
                    MTPDBCONTEXTDATA* pmdbcd);


/*  Object Handles Context Data
 *
 *  Handles serialization of the standard Object Handles dataset
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPObjectHandlesContextDataCreate(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32 dwFormat,
                    MTPDBCONTEXTDATA* pmdbcd);


/*  Object Prop List Context Data
 *
 *  Handles serialization of the standard Object Prop List dataset
 */

PSLSTATUS PSL_API MTPObjectPropListContextDataCreate(
                    MTPDBCONTEXTBASE mdbc,
                    PSLUINT32 dwFormat,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    MTPDBCONTEXTDATA* pmdbcd);

/*  Object Binary Context Data
 *
 *  Handles serialization of the object binary data
 */

PSLSTATUS PSL_API MTPObjectBinaryContextDataCreate(
                    MTPDBCONTEXTBASE mdbc,
                    PSLUINT32 dwFormat,
                    PSLUINT32 idxObj,
                    MTPDBCONTEXTDATA* pmdbcd);

#endif  /* _MTPDBCONTEXTDATA_H_ */

