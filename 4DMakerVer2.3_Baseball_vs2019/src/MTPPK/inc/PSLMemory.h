/*
 *  PSLMemory.h
 *
 *  Contains definitions for memory tracking and management routines that
 *  are used as part of the core platform implementation.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLMEMORY_H_
#define _PSLMEMORY_H_

/*  Memory Types
 */

typedef PSLHANDLE PSLHMEM;
typedef PSLHANDLE PSLHEAP;

/*  Memory Subsystem Shutdown
 *
 *  Should be called during process shutdown to handle any memory subsystem
 *  shutdown that needs tha handle.  If memory tracing is enabled this call
 *  will perform leak detection.
 */

PSL_EXTERN_C PSLVOID PSL_API PSLMemShutdown(PSLVOID);


/*  Out of Memory Handler
 *
 *  The PSL memory architecture defines a global callback to be called when
 *  an out of memory condition is reached.  Default behavior is to do nothing.
 *  Based on the result of the callback the approriate action is taken by
 *  the memory system.
 */


/*  Custom status codes that may be returned by the OOM handler
 */

enum
{
    PSLOOM_FAIL =                   MAKE_PSLSUCCESS_CODE(PSL_USER_STATUS),
    PSLOOM_RETRY =                  MAKE_PSLSUCCESS_CODE(PSL_USER_STATUS + 1),
    PSLOOM_COMPACT_FAIL =           MAKE_PSLSUCCESS_CODE(PSL_USER_STATUS + 2),
    PSLOOM_COMPACT_RETRY =          MAKE_PSLSUCCESS_CODE(PSL_USER_STATUS + 3)
};

typedef PSLSTATUS (PSL_API* PFNPSLOOMHANDLER)(
                            PSLUINT32 idxAttempt,
                            PSLUINT32 cbDesired);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMemRegisterOOMHandler(
                            PFNPSLOOMHANDLER pfnHandler);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMemUnregisterOOMHandler(
                            PFNPSLOOMHANDLER pfnHandler);


/*  Default Heap APIs
 *
 *  The following APIs all retrieve and return memory to the default heap
 *  associated with the process.
 */

#define PSLMEM_FIXED        0x00000000
#define PSLMEM_RELOCATABLE  0x00000001
#define PSLMEM_TYPE_MASK    0x0000000f

#define PSLMEM_ZERO_INIT    0x80000000

#define PSLMEM_PTR          (PSLMEM_FIXED | PSLMEM_ZERO_INIT)

#ifdef PSL_MEMORY_TRACK

/*  Management APIs for debug behavior
 *
 */

PSL_EXTERN_C PSLVOID PSL_API PSLHaltOnMemErrors(
                            PSLBOOL fHalt);

/*  Memory Tracking
 *
 *  The memory management functions below provide information to support
 *  leak tracking, sentinal checking, and other memory validation functions
 *  that may be appropriate for the particular platform.
 */

PSL_EXTERN_C PSLHMEM PSL_API PSLMemTrackedAlloc(
                            PSLUINT32 dwFlags,
                            PSLUINT32 cbRequested,
                            PSLCSTR szFile,
                            PSLINT32 nLine);

PSL_EXTERN_C PSLHMEM PSL_API PSLMemTrackedReAlloc(
                            PSLHMEM hMem,
                            PSLUINT32 cbRequested,
                            PSLUINT32 dwFlags,
                            PSLCSTR szFile,
                            PSLINT32 nLine);

#define PSLMemAlloc(_uF, _uB)           \
        PSLMemTrackedAlloc(_uF, _uB, __FILE__, __LINE__)

#define PSLMemReAlloc(_hM, _uB, _uF)    \
        PSLMemTrackedReAlloc(_hM, _uB, _uF, __FILE__, __LINE__)

/*  Validate sentinals on the specified memory
 */

PSL_EXTERN_C PSLBOOL PSL_API PSLMemCheckSentinals(PSLHMEM hMem);

#else   /* !PSL_MEMORY_TRACK */

/*  Memory Management API
 *
 *  Default memory management API.  This version does not support memory
 *  tracking.
 */

PSL_EXTERN_C PSLHMEM PSL_API PSLMemAlloc(
                            PSLUINT32 dwFlags,
                            PSLUINT32 cbRequested);

PSL_EXTERN_C PSLHMEM PSL_API PSLMemReAlloc(
                            PSLHMEM hMem,
                            PSLUINT32 cbRequested,
                            PSLUINT32 dwFlags);

#endif  /* PSL_MEMORY_TRACK */

PSL_EXTERN_C PSLHMEM PSL_API PSLMemFree(
                            PSLHMEM hMem);

PSL_EXTERN_C PSLUINT32 PSL_API PSLMemSize(
                            PSLHMEM hMem);


#ifdef PSL_OVERRIDE_CRT_MEMORY

/*  Override the CRT to use PSL memory calls
 */

#undef malloc
#undef realloc
#undef free

#define malloc(_uB)         PSLMemAlloc(PSLMEM_FIXED, _uB)
#define realloc(_pB, _uB)   PSLMemReAlloc(_pB, _uB, 0)
#define free(_pb)           PSLMemFree(_pb)

#endif  /* PSL_OVERRIDE_CRT_MEMORY */

#ifdef __cplusplus

/*
 *  C++ new/delete operators
 *
 *  Enables support for C++ environments to default to using the memory
 *  management APIs defined here.  In order to enable memory tracking with
 *  C++ new operations the user must replace references to 'new' in the
 *  source code with the macro 'NEW'.  This macro will correctly resolve
 *  to the appropriate memory allocation operator if tracing is enabled.
 *
 */

void* operator new(size_t);
void* operator new[](size_t);

void operator delete(void*);
void operator delete[](void*);

#ifdef PSL_MEMORY_TRACK

PSL_EXTERN_C PSLVOID PSL_API PSLTrackCPPArrayAllocators(
                            PSLBOOL fHalt);

void* operator new(size_t, PSLCSTR, PSLINT32);
void* operator new[](size_t, PSLCSTR, PSLINT32);

#define NEW     new(__FILE__, __LINE__)

#else   /* PSL_MEMORY_TRACK */

#define NEW     new

#endif  /* PSL_MEMORY_TRACK */

#endif  /* __cplusplus */


/*  Heap Memory Routines
 *
 *  The routines below enables creation and management of non-default
 *  memory heaps.
 */


#define PSLHEAP_CREATE_FLAGS_MASK      0x000000ff
#define PSLHEAP_DEFAULT                0x00000001
#define PSLHEAP_SHARED                 0x00000002
#define PSLHEAP_CACHE                  0x00000004

PSL_EXTERN_C PSLSTATUS PSL_API PSLHeapCreate(
                            PSLUINT32 dwFlags,
                            PSLUINT32 cbMinSize,
                            PSLUINT32 cbMaxSize,
                            PSLHEAP* phHeap);

PSL_EXTERN_C PSLSTATUS PSL_API PSLHeapDestroy(
                            PSLHEAP hHeap);

#ifdef PSL_MEMORY_TRACK

/*  Heap Memory Tracking
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLHeapTrackedAlloc(
                            PSLHEAP hHeap,
                            PSLUINT32 dwFlags,
                            PSLUINT32 cbRequested,
                            PSLVOID** ppvMem,
                            PSLCSTR szFile,
                            PSLINT32 nLine);

PSL_EXTERN_C PSLSTATUS PSL_API PSLHeapTrackedReAlloc(
                            PSLHEAP hHeap,
                            PSLUINT32 dwFlags,
                            PSLVOID* pvMem,
                            PSLUINT32 cbRequested,
                            PSLVOID** ppvMem,
                            PSLCSTR szFile,
                            PSLINT32 nLine);

#define PSLHeapAlloc(_h, _dwF, _cbR, _ppvM)         \
        PSLHeapTrackedAlloc(_h, _dwF, _cbR, _ppvM, __FILE__, __LINE__)

#define PSLHeapReAlloc(_h, _dwF, _pM, _cbR, _ppvM)  \
        PSLHeapTrackedReAlloc(_h, _dwF, _pM, _cbR, _ppvM, __FILE__, __LINE__)

/*  Sentinal Checking
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLHeapCheckSentinals(
                            PSLHEAP hHeap,
                            PSL_CONST PSLVOID* pvMem);


#else   /* !PSL_MEMORY_TRACK */

/*  Heap Memory Management API
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLHeapAlloc(
                            PSLHEAP hHeap,
                            PSLUINT32 dwFlags,
                            PSLUINT32 cbRequested,
                            PSLVOID** ppvMem);


PSL_EXTERN_C PSLSTATUS PSL_API PSLHeapReAlloc(
                            PSLHEAP hHeap,
                            PSLUINT32 dwFlags,
                            PSLVOID* pvMem,
                            PSLUINT32 cbRequested,
                            PSLVOID** ppvMem);

#endif  /* PSL_MEMORY_TRACK */

/*  Additional Heap Functionality
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLHeapFree(
                            PSLHEAP hHeap,
                            PSLUINT32 dwFlags,
                            PSLVOID* pvMem);

PSL_EXTERN_C PSLSTATUS PSL_API PSLHeapSize(
                            PSLHEAP hHeap,
                            PSLUINT32 dwFlags,
                            PSL_CONST PSLVOID* pvMem,
                            PSLUINT32* pcbSize);

PSL_EXTERN_C PSLSTATUS PSL_API PSLHeapCompact(
                            PSLHEAP hHeap,
                            PSLUINT32 dwFlags,
                            PSLUINT32* pcbLargest);

PSL_EXTERN_C PSLVOID PSL_API PSLHeapCompactAll(PSLVOID);


/*  Information API
 *
 *  The following APIs return information about the underlying memory
 *  subsystem as indicated
 */


/*  Return the default alignment for the platform
 */

PSL_EXTERN_C PSLUINT32 PSL_API PSLGetAlignment(PSLVOID);

/*  Return the default page size for the platform
 */

PSL_EXTERN_C PSLUINT32 PSL_API PSLGetPageSize(PSLVOID);

/*  Return the amount of memory currently available to the process- this may
 *  or may not be the total amount of virtual memory available on the platform
 */

PSL_EXTERN_C PSLUINT32 PSL_API PSLGetAvailableMemory(PSLVOID);

/*  Return the total amount of memory available to the process- this may or
 *  may not be the total amount of memory available on the platform
 */

PSL_EXTERN_C PSLUINT32 PSL_API PSLGetTotalMemory(PSLVOID);

/*  Return the total amount of memory that is currently free on the system.
 *  This entire amount number may or may not be available to the process
 *  at this point.
 */

PSL_EXTERN_C PSLUINT32 PSL_API PSLGetFreeMemory(PSLVOID);

/*  Align the specified allocation size to a page boundary
 */

PSL_EXTERN_C PSLUINT32 PSL_API PSLAlignAllocSizeToPage(PSLUINT32 cbSize);


/*  Do a cryptographically secure cleanup on memory
 */

PSL_EXTERN_C PSLVOID PSL_API PSLCryptClearMemory(
                            PSLVOID* pvData,
                            PSLUINT32 cbData);

/*  Memory Alignment Helper
 */

PSL_EXTERN_C PSLBYTE* PSL_API PSLMemAlign(
                            PSLBYTE* pb,
                            PSLUINT32 dwBoundary);

/*  Memory Allocation Alignment Helper
 */

#define PSLAllocAlign(_cb, _dwBoundary)                                     \
    (PSLUINT32)PSLMemAlign((PSLBYTE*)_cb, _dwBoundary);


/*  Memory Operations
 */

PSL_EXTERN_C PSLVOID PSL_API PSLSetMemory(
                            PSLVOID* pvDest,
                            PSLBYTE bData,
                            PSLUINT32 cbDest);

#define PSLZeroMemory(_pv, _cb) PSLSetMemory(_pv, 0, _cb);

PSL_EXTERN_C PSLVOID PSL_API PSLCopyMemory(
                            PSLVOID* pvDest,
                            PSL_CONST PSLVOID* pvSrc,
                            PSLUINT32 cbCopy);

PSL_EXTERN_C PSLVOID PSL_API PSLMoveMemory(
                            PSLVOID* pvDest,
                            PSL_CONST PSLVOID* pvSrc,
                            PSLUINT32 cbMove);

PSL_EXTERN_C PSLINT32 PSL_API PSLCompareMemory(
                            PSL_CONST PSLVOID* pvMem1,
                            PSL_CONST PSLVOID* pvMem2,
                            PSLUINT32 cbCompare);

/*  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 */

#define SAFE_PSLMEMFREE(_pv) \
if (PSLNULL != _pv) \
{ \
    PSLMemFree(_pv); \
    _pv = PSLNULL; \
}

#define SAFE_PSLHEAPDESTROY(_hh) \
if (PSLNULL != _hh) \
{ \
    PSLHeapDestroy(_hh); \
    _hh = PSLNULL; \
}

#define SAFE_PSLHEAPFREE(_hh, _dw, _pv) \
if (PSLNULL != _pv) \
{ \
    PSLHeapFree(_hh, _dw, _pv); \
    _pv = PSLNULL; \
}

#endif  /* _PSLMEMORY_H_ */

