/*
 *  PSLAtomic.h
 *
 *  Contains declaration of PSL atomic operations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLATOMIC_H_
#define _PSLATOMIC_H_

/*
 *  Atomic Functions
 *
 *  Each of the operations indicated below is assumed to perform its work in
 *  an atomic fashion to guarantee thread safety.  Whereever possible these
 *  operations should be implemented using the most efficient method possible
 *  and without the use of heavy synchronization objects.
 *
 */

PSL_EXTERN_C PSLUINT32 PSL_API PSLAtomicIncrement(
                            PSLUINT32* pui32);

PSL_EXTERN_C PSLUINT32 PSL_API PSLAtomicDecrement(
                            PSLUINT32* pui32);

PSL_EXTERN_C PSLUINT32 PSL_API PSLAtomicAdd(
                            PSLUINT32* pui32,
                            PSLUINT32 ui32Addend);

PSL_EXTERN_C PSLUINT32 PSL_API PSLAtomicSubtract(
                            PSLUINT32* pui32,
                            PSLUINT32 ui32Operand);

PSL_EXTERN_C PSLUINT32 PSL_API PSLAtomicExchange(
                            PSLUINT32* pui32Dest,
                            PSLUINT32 ui32Value);

PSL_EXTERN_C PSLUINT32 PSL_API PSLAtomicCompareExchange(
                            PSLUINT32* pui32Dest,
                            PSLUINT32 ui32Exchange,
                            PSLUINT32 ui32Comperand);

PSL_EXTERN_C PSLVOID* PSL_API PSLAtomicExchangePointer(
                            PSLVOID** ppvDest,
                            PSLVOID* pvValue);

PSL_EXTERN_C PSLVOID* PSL_API PSLAtomicCompareExchangePointer(
                            PSLVOID** ppvDest,
                            PSLVOID* pvExchange,
                            PSL_CONST PSLVOID* pvComperand);

#endif  /* _PSLATOMIC_H_ */

