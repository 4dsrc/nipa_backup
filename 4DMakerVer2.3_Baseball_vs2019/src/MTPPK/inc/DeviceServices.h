/*
 *  DeviceServices.h
 *
 *  Contains definitions for the core Device Services platform
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The service definition specified in this file has been designed for use
 *  with Microsoft Windows.  To request additions to this file, including new
 *  service properties, object formats, object properties, methods, and events,
 *  please contact Microsoft at askmtp@microsoft.com with your request to
 *  maintain interoperability.
 *
 *  Custom service properties, object formats, object properties, methods,
 *  and events may also be added by generating new format, method, or event
 *  GUIDs or by creating new PKeys in a privately generated and managed
 *  namespace.
 *
 */

#ifndef _DEVICESERVICES_H_
#define _DEVICESERVICES_H_

#include "BridgeDeviceService.h"

/*****************************************************************************/
/*  Service Info                                                             */
/*****************************************************************************/

/*  Service Info Version
 */

#define DEVSVC_SERVICEINFO_VERSION      0x00000064

/*  Service Flags
 */

#define DEVSVCTYPE_DEFAULT              0x00000000
#define DEVSVCTYPE_ABSTRACT             0x00000001

/*****************************************************************************/
/*  Common Service Properties                                                */
/*****************************************************************************/

DEFINE_DEVSVCGUID(NAMESPACE_Services,
    0x14fa7268, 0x0b6c, 0x4214, 0x94, 0x87, 0x43, 0x5b, 0x48, 0x0a, 0x8c, 0x4f);


/*  PKEY_Services_ServiceDisplayName
 *
 *  Type: String
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_Services_ServiceDisplayName,
    0x14fa7268, 0x0b6c, 0x4214, 0x94, 0x87, 0x43, 0x5b, 0x48, 0x0a, 0x8c, 0x4f,
    2);

#define NAME_Services_ServiceDisplayName    L"ServiceDisplayName"


/*  PKEY_Services_ServiceIcon
 *
 *  Type: AUInt8
 *  Form: ByteArray
 */

DEFINE_DEVSVCPROPKEY(PKEY_Services_ServiceIcon,
    0x14fa7268, 0x0b6c, 0x4214, 0x94, 0x87, 0x43, 0x5b, 0x48, 0x0a, 0x8c, 0x4f,
    3);

#define NAME_Services_ServiceIcon           L"ServiceIcon"


/*  PKEY_Services_ServiceLocale
 *
 *  Contains the RFC4646 compliant language string for data in this service
 *
 *  Type: String
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_Services_ServiceLocale,
    0x14fa7268, 0x0b6c, 0x4214, 0x94, 0x87, 0x43, 0x5b, 0x48, 0x0a, 0x8c, 0x4f,
    4);

#define NAME_Services_ServiceLocale         L"ServiceLocale"

#endif  /* _DEVICESERVICES_H_ */

