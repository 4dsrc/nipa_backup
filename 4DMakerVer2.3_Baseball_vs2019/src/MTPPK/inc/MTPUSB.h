/*
 *  MTPUSB.h
 *
 *  Contains important definitions for MTP/USB transport functionality
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPUSB_H_
#define _MTPUSB_H_

/*  Force tight packing
 */

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPush1.h"
#endif

/*
 *  MTP USB Class Defines
 */

#define MTPUSB_DEVICE_CLASS_IMAGE               0x06
#define MTPUSB_DEVICE_SUBCLASS_IMAGE_CAPTURE    0x01
#define MTPUSB_DEVICE_PROTOCOL_PIMA15740        0x01

/*
 *  MTP USB Class Specific Requests
 *
 */

#define MTPUSB_CANCEL_REQUEST                   0x64

struct _MTPUSB_CANCEL_REQUEST_DATA
{
    PSLUINT16               wCancellationCode;
    PSLUINT32               dwTransactionID;
} PSL_PACK1;

typedef struct _MTPUSB_CANCEL_REQUEST_DATA MTPUSB_CANCEL_REQUEST_DATA;

typedef struct _MTPUSB_CANCEL_REQUEST_DATA PSL_UNALIGNED*
                            PXMTPUSB_CANCEL_REQUEST_DATA;
typedef PSL_CONST struct _MTPUSB_CANCEL_REQUEST_DATA PSL_UNALIGNED*
                            PCXMTPUSB_CANCEL_REQUEST_DATA;

#define MTPUSB_GET_EXTENDED_EVENT_DATA_REQUEST  0x65

struct _MTPUSB_EXTENDED_EVENT_HEADER
{
    PSLUINT16               wEventCode;
    PSLUINT32               dwTransactionID;
    PSLUINT16               wParamCount;
} PSL_PACK1;

typedef struct _MTPUSB_EXTENDED_EVENT_HEADER MTPUSB_EXTENDED_EVENT_HEADER;

typedef struct _MTPUSB_EXTENDED_EVENT_HEADER PSL_UNALIGNED*
                            PXMTPUSB_EXTENDED_EVENT_HEADER;
typedef PSL_CONST struct _MTPUSB_EXTENDED_EVENT_HEADER PSL_UNALIGNED*
                            PCXMTPUSB_EXTENDED_EVENT_HEADER;

typedef struct _MTPUSB_EXTENDED_EVENT_PARAM
{
    PSLUINT16               wParamSize;
} PSL_PACK1;

typedef struct _MTPUSB_EXTENDED_EVENT_PARAM MTPUSB_EXTENDED_EVENT_PARAM;

typedef struct _MTPUSB_EXTENDED_EVENT_PARAM PSL_UNALIGNED*
                            PXMTPUSB_EXTENDED_EVENT_PARAM;
typedef PSL_CONST struct _MTPUSB_EXTENDED_EVENT_PARAM PSL_UNALIGNED*
                            PCXMTPUSB_EXTENDED_EVENT_PARAM;

#define MTPUSB_DEVICE_RESET_REQUEST             0x66

#define MTPUSB_DEVICE_STATUS_REQUEST            0x67

typedef struct _MTPUSB_DEVICE_STATUS_HEADER
{
    PSLUINT16               wLength;
    PSLUINT16               wStatusCode;
} PSL_PACK1;

typedef struct _MTPUSB_DEVICE_STATUS_HEADER MTPUSB_DEVICE_STATUS_HEADER;

typedef struct _MTPUSB_DEVICE_STATUS_HEADER PSL_UNALIGNED*
                            PXMTPUSB_DEVICE_STATUS_HEADER;
typedef PSL_CONST struct _MTPUSB_DEVICE_STATUS_HEADER PSL_UNALIGNED*
                            PCXMTPUSB_DEVICE_STATUS_HEADER;


/*
 *  MTP USB Container Types
 */

#define MTPUSBCONTAINER_UNDEFINED       0
#define MTPUSBCONTAINER_COMMAND         1
#define MTPUSBCONTAINER_DATA            2
#define MTPUSBCONTAINER_RESPONSE        3
#define MTPUSBCONTAINER_EVENT           4

/*
 *  Standard Header
 */

struct _MTPUSBPACKET_HEADER
{
    PSLUINT32               cbPacket;
    PSLUINT16               wContainerType;
} PSL_PACK1;

typedef struct _MTPUSBPACKET_HEADER MTPUSBPACKET_HEADER;

typedef struct _MTPUSBPACKET_HEADER PSL_UNALIGNED*
                            PXMTPUSBPACKET_HEADER;
typedef PSL_CONST struct _MTPUSBPACKET_HEADER PSL_UNALIGNED*
                            PCXMTPUSBPACKET_HEADER;

/*
 *  Operation Request
 */

struct _MTPUSBPACKET_OPERATION_REQUEST
{
    MTPUSBPACKET_HEADER     header;
    MTPOPERATION            mtpOp;
} PSL_PACK1;

typedef struct _MTPUSBPACKET_OPERATION_REQUEST MTPUSBPACKET_OPERATION_REQUEST;

typedef struct _MTPUSBPACKET_OPERATION_REQUEST PSL_UNALIGNED*
                            PXMTPUSBPACKET_OPERATION_REQUEST;
typedef PSL_CONST struct _MTPUSBPACKET_OPERATION_REQUEST PSL_UNALIGNED*
                            PCXMTPUSBPACKET_OPERATION_REQUEST;

/*
 *  Operation Response
 */

struct _MTPUSBPACKET_OPERATION_RESPONSE
{
    MTPUSBPACKET_HEADER     header;
    MTPRESPONSE             mtpResponse;
} PSL_PACK1;

typedef struct _MTPUSBPACKET_OPERATION_RESPONSE MTPUSBPACKET_OPERATION_RESPONSE;

typedef struct _MTPUSBPACKET_OPERATION_RESPONSE PSL_UNALIGNED*
                            PXMTPUSBPACKET_OPERATION_RESPONSE;
typedef PSL_CONST struct _MTPUSBPACKET_OPERATION_RESPONSE PSL_UNALIGNED*
                            PCXMTPUSBPACKET_OPERATION_RESPONSE;

/*
 *  Event
 */

struct _MTPUSBPACKET_EVENT
{
    MTPUSBPACKET_HEADER     header;
    MTPEVENT                mtpEvent;
} PSL_PACK1;

typedef struct _MTPUSBPACKET_EVENT MTPUSBPACKET_EVENT;

typedef struct _MTPUSBPACKET_EVENT PSL_UNALIGNED* PXMTPUSBPACKET_EVENT;
typedef PSL_CONST struct _MTPUSBPACKET_EVENT PSL_UNALIGNED*
                            PCXMTPUSBPACKET_EVENT;

/*
 *  Data
 */

struct _MTPUSBPACKET_DATA_HEADER
{
    MTPUSBPACKET_HEADER     header;
    PSLUINT16               wOpCode;
    PSLUINT32               dwTransactionID;
} PSL_PACK1;

typedef struct _MTPUSBPACKET_DATA_HEADER MTPUSBPACKET_DATA_HEADER;

typedef struct _MTPUSBPACKET_DATA_HEADER PSL_UNALIGNED*
                            PXMTPUSBPACKET_DATA_HEADER;
typedef PSL_CONST struct _MTPUSBPACKET_DATA_HEADER PSL_UNALIGNED*
                            PCXMTPUSBPACKET_DATA_HEADER;

/*
 *  Recover default packing
 */

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPop.h"
#endif

#endif  /* _MTPUSB_H_ */

