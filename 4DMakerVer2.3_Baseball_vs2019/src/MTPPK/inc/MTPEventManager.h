/*
 *  MTPEventManager.h
 *
 *  Contains declarations for the MTP event manager.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPEVENTMANAGER_H_
#define _MTPEVENTMANAGER_H_

typedef PSLHANDLE MTPEVENTOBJECT;

/*  MTPEventObject
 *
 *  In order to optimize event handling, events are managed as objects
 *  instead of simple structures.  This enables the "one to many" nature of
 *  events to be better handled through internal mechanisms which avoid
 *  creating multiple copies.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPEventObjectCreate(
                    PSLUINT16 wEventCode,
                    PSLUINT32 dwTransactionID,
                    PSLUINT32 dwParam1,
                    PSLUINT32 dwParam2,
                    PSLUINT32 dwParam3,
                    MTPEVENTOBJECT* pevObj);

PSL_EXTERN_C PSLSTATUS PSL_API MTPEventObjectDestroy(
                    MTPEVENTOBJECT evObj);

PSL_EXTERN_C PSLSTATUS PSL_API MTPEventObjectStore(
                    MTPEVENTOBJECT evObj,
                    PXMTPEVENT pmtpEvent,
                    PSLUINT32 cbEvent,
                    PSLUINT32* pcbUsed);

PSL_EXTERN_C PSLUINT16 PSL_API MTPEventObjectGetEventCode(
                    MTPEVENTOBJECT evObj);

PSL_EXTERN_C PSLSTATUS PSL_API MTPEventObjectGetParams(
                    MTPEVENTOBJECT evObj,
                    PSLUINT32* pdwTransactionID,
                    PSLUINT32* pdwParam1,
                    PSLUINT32* pdwParam2,
                    PSLUINT32* pdwParam3);

PSL_EXTERN_C PSLSTATUS PSL_API MTPEventObjectGetBroadcastMode(
                    MTPEVENTOBJECT evObj,
                    PSLUINT32* pdwBroadcastMode,
                    MTPCONTEXT** ppmtpctx);


/*  MTP Event Manager
 *
 *  The MTP Event Manager is presponsible for posting and broadcasting events
 *  throught the MTP arhcitecture.  One of the core clients of the MTP Event
 *  Manager is a per-MTP Session handler which handles sending events at the
 *  transport level.  Other message queues may be registered to receive MTP
 *  Event broadcasts for their own needs- this is especially convenient to
 *  allow different object stores to listen to events internally without
 *  taking up additional platform resources to create their own internal
 *  platform event handlers.
 */

/*  MTPEventManagerAddMonitor
 *
 *  Called to register a message queue to monitor MTP event notfications.
 *  All events will be broadcast to this message queue using the MTPMSG_EVENT
 *  message in addition to the standard MTP Router handler.  The MTPEVENTOBJECT
 *  will be passed in aParam0 of the message and must be destroyed via call
 *  to MTPEventDestroy when the receiving handler is finished with the object.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPEventManagerAddMonitor(
                    PSLMSGQUEUE mqNotify);

/*  MTPEventManagerRemoveMonitor
 *
 *  Called to remove a message queue from the list of queues currently
 *  listening for MTP events.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPEventManagerRemoveMonitor(
                    PSLMSGQUEUE mqNotify);

/*  MTPEventManagerBroadcastEvent
 *
 *  Called to broadcast an event to all listening parties.  Also provides
 *  specific information about the intended event destination.
 */

enum
{
    MTPEVENTBROADCASTMODE_UNKNOWN = 0,
    MTPEVENTBROADCASTMODE_ALL,
    MTPEVENTBROADCASTMODE_EXCLUDE_MTPSESSION,
    MTPEVENTBROADCASTMODE_ONLY_MTPSESSION,
};

PSL_EXTERN_C PSLSTATUS PSL_API MTPEventManagerBroadcastEvent(
                    MTPEVENTOBJECT evObj,
                    PSLUINT32 dwBroadcastMode,
                    MTPCONTEXT* pmtpctx);


/*  Safe Helpers
 */

#define SAFE_MTPEVENTOBJECTDESTROY(_ev) \
if (PSLNULL != (_ev)) \
{ \
    MTPEventObjectDestroy(_ev); \
    (_ev) = PSLNULL; \
}

#endif  /* _MTPEVENTMANAGER_H_ */
