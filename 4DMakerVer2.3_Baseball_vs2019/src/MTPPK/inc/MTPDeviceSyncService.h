/*
 *  MTPDeviceSyncService.h
 *
 *  Declares different forms of initialization for the MTP Sync Services.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPDEVICESYNCSERVICES_H_
#define _MTPDEVICESYNCSERVICES_H_

#include "FullEnumSyncDeviceService.h"
#include "MTPServiceContext.h"

#define FULLENUMSYNCSVC_MIN_SERVICE_PROPERTIES  1
#define FULLENUMSYNCSVC_MAX_SERVICE_PROPERTIES  100
#define FULLENUMSYNCSVC_MIN_SERVICE_FORMATS     0xb002
#define FULLENUMSYNCSVC_MAX_SERVICE_FORMATS     0xb003

#define FULLENUMSYNCSVC_MIN_OBJECT_PROPERTIES \
                MTP_OBJECTPROPCODE_VENDOREXTENSION_FIRST
#define FULLENUMSYNCSVC_MAX_OBJECT_PROPERTIES \
                (FULLENUMSYNCSVC_MIN_OBJECT_PROPERTIES + 100)

DEFINE_PSLGUID(SVCPUID_MTPFullEnumSyncService,
    0x29592125, 0x7e45, 0x4768, 0x87, 0xfa, 0xac, 0x4b, 0x77, 0xbd, 0xe9, 0x76);

/*  Full Enumeration Sync Service Support
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceFullEnumSyncServiceInit(
                    MTPCONTEXT* pmtpSession,
                    PSLUINT32 dwServiceID);

/*  Full Enumeration Sync Service Property Support
 */

enum
{
    FULLENUMSYNCPROP_FORMAT = FULLENUMSYNCSVC_MIN_SERVICE_PROPERTIES,
    FULLENUMSYNCPROP_VERSION_PROPS,
    FULLENUMSYNCPROP_LOCAL_ONLY_DELETE,
    FULLENUMSYNCPROP_SYNC_FILTER,
    FULLENUMSYNCPROP_REPLICA_ID,
    FULLENUMSYNCPROP_KNOWLEDGE_OBJECT_ID,
    FULLENUMSYNCPROP_LAST_SYNC_PROXY_ID,
    FULLENUMSYNCPROP_SYNC_OBJECT_REFERENCES,
};

PSL_EXTERN_C PSLSTATUS PSL_API MTPFullEnumSyncSvcGetServicePropInfo(
                        PSLUINT16 wPropCode,
                        PSL_CONST MTPSERVICEPROPERTYINFO** ppServProps,
                        PSLUINT32* pcServProps);

/*  Full Enumeration Sync Service Format Support
 */

#define MTP_OBJECTPROPCODE_LASTAUTHORPROXYID \
                            FULLENUMSYNCSVC_MIN_OBJECT_PROPERTIES

enum
{
    FULLENUMSYNCFORMAT_KNOWLEDGE = FULLENUMSYNCSVC_MIN_SERVICE_FORMATS
};

PSL_EXTERN_C PSLSTATUS PSL_API MTPFullEnumSyncSvcGetFormatInfo(
                        PSLUINT16 wFormatCode,
                        PSL_CONST MTPSERVICEFORMATINFO** ppServFormats,
                        PSLUINT32* pcServFormats);

#endif  /* _MTPDEVICESYNCSERVICES_H_ */

