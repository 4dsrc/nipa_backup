/*
 *  PSLDemandLoader.h
 *
 *  Provides a set of macros that can be used to support on-demand loading of
 *  shared libraries.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLDEMANDLOADER_H_
#define _PSLDEMANDLOADER_H_

/*
 *  PSLLOADER_*
 *
 *  While most modern shared library system support some form of "late binding"
 *  either through "weak linking" or other demand load schemes, many of them
 *  lack proper error handling if the share library fails to load.  This will
 *  often lead to an access violation when an attempt is made to actually
 *  executed the routine imported from the shared libray.  Use of these macros
 *  enable both on-demand loading of the shared library as well as gracefully
 *  handling the error condition if the library is not loaded.
 *
 *  To use the macros a header file is created for the shared library being
 *  loaded.  In the header file all functions that are to be loaded from the
 *  library are specified in the file using a PSLLOADER_* macro.  In addition
 *  a define is created which defines the name of the function (_name) as
 *  PSLLOADER_IMPORT(_name).
 *
 *  In platform specific code (in order to deal with the differences between
 *  various shared library systems), the implementor is responsible for
 *  providing a PSLLoaderDemandLoad[_lib] function that will locate the
 *  shared library and import all specified imports from that library.  Note
 *  that in order to work correctly (and save an additional level of
 *  indirection) the loader function should only update the function pointer
 *  if the import was succesfully located.  That way the next call will get
 *  the correct failure behavior.  In addition the file should have
 *  PSLLOADER_IMPLEMENT_LOADER_FUNCTIONS defined prior to including this file
 *  so that the necessary wrappers are generated.
 *
 */

#define PSLLOADER_LOAD(_lib)                                                \
        PSLLoaderDemandLoad##_lib

#define PSLLOADER_IMPORT(_name)                                             \
        PSLLOADERVAR_##_name

#define PSLLOADER_TYPE(_name)                                               \
        PSLLOADERTYP_##_name

#define PSLLOADER_FUNC(_name)                                               \
        PSLLOADERFUNC_##_name

#define PSLLOADER_STUB(_ret, _cconv, _name, _args1)                         \
        typedef _ret (_cconv* PSLLOADER_TYPE(_name)) _args1;                \
        PSL_EXTERN_C PSLLOADER_TYPE(_name) PSLLOADER_IMPORT(_name)

#define PSLLOADER_STUB_VOID(_cconv, _name, _args1)                          \
        typedef void (_cconv* PSLLOADER_TYPE(_name)) _args1;                \
        PSL_EXTERN_C PSLLOADER_TYPE(_name) PSLLOADER_IMPORT(_name)

#ifdef PSLLOADER_IMPLEMENT_LOADER_FUNCTIONS

#undef PSLLOADER_FUNCTION
#undef PSLLOADER_FUNCTION_VOID

#define PSLLOADER_FUNCTION(_ret, _cconv, _name, _args1, _args2, _err, _lib) \
        PSLLOADER_STUB(_ret, _cconv, _name, _args1);                        \
        PSL_EXTERN_C _ret _cconv PSLLOADER_FUNC(_name) _args1;              \
        _ret PSLLOADER_FUNC(_name) _args1                                   \
        {                                                                   \
            PSLLOADER_LOAD(_lib)();                                         \
            if (PSLLOADER_IMPORT(_name) == PSLLOADER_FUNC(_name))           \
            {                                                               \
                return _err;                                                \
            }                                                               \
            return PSLLOADER_IMPORT(_name) _args2;                          \
        }                                                                   \
        PSLLOADER_TYPE(_name) PSLLOADER_IMPORT(_name) = PSLLOADER_FUNC(_name)

#define PSLLOADER_FUNCTION_VOID(_cconv, _name, _args1, _args2, _err, _lib)  \
        PSLLOADER_STUB_VOID(_cconv, _name, _args1);                         \
        PSL_EXTERN_C void _cconv PSLLOADER_FUNC(_name) _args1;              \
        void PSLLOADER_FUNC(_name) _args1                                   \
        {                                                                   \
            PSLLOADER_LOAD(_lib)();                                         \
            if (PSLLOADER_IMPORT(_name) != PSLLOADER_FUNC(_name))           \
            {                                                               \
                PSLLOADER_IMPORT(_name) _args2;                             \
            }                                                               \
            else                                                            \
            {                                                               \
                _err;                                                       \
            }                                                               \
            return;                                                         \
        }                                                                   \
        PSLLOADER_TYPE(_name) PSLLOADER_IMPORT(_name) = PSLLOADER_FUNC(_name)

#else  // !PSLLOADER_IMPLEMENT_LOADER_FUNCTIONS

#define PSLLOADER_FUNCTION(_ret, _cconv, _name, _args1, _args2, _err, _lib) \
        PSLLOADER_STUB(_ret, _cconv, _name, _args1)

#define PSLLOADER_FUNCTION_VOID(_cconv, _name, _args1, _args2, _err, _lib)  \
        PSLLOADER_STUB_VOID(_cconv, _name, _args1)

#endif  // PSLLOADER_IMPLEMENT_LOADER_FUNCTIONS

#define PSLLOADER_BIND_RESET(_imp)                                          \
        PSLLOADER_IMPORT(_imp) = PSLLOADER_FUNC(_imp)

#define PSLLOADER_BIND_IMPORT(_imp, _pfn, _onErr)                           \
        if (PSLNULL != _pfn)                                                \
        {                                                                   \
            PSLLOADER_IMPORT(_imp) = (PSLLOADER_TYPE(_imp))_pfn;            \
        }                                                                   \
        else                                                                \
        {                                                                   \
            PSLLOADER_BIND_RESET(_imp);                                     \
            _onErr;                                                         \
        }

#endif  /* _PSLDEMANDLOADER_H_ */

