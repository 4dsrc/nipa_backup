/*
 *  PSLIPHelper.h
 *
 *  Collection of helper routines for supporting TCP/IP connections
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLIPHELPER_H_
#define _PSLIPHELPER_H_

#define SAFE_FREERESOLVEDIPPORTLIST     SAFE_PSLMEMFREE
#define SAFE_FREERESOVLEDMACADDRESS     SAFE_PSLMEMFREE

/*
 *  PSLIsIPAddressAny
 *
 *  Returns S_OK if the specified address is an 'any' address
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLIsIPAddressAny(
                            PSL_CONST SOCKADDR* psaAddr,
                            PSLUINT32 cbAddr);


/*
 *  PSLIsEqualIPAddress
 *
 *  Returns S_OK if the specified addresses are the same, S_FALSE if not
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLIsEqualIPAddress(
                            PSL_CONST SOCKADDR* psaAddr1,
                            PSLUINT32 cbAddr1,
                            PSL_CONST SOCKADDR* psaAddr2,
                            PSLUINT32 cbAddr2,
                            PSLBOOL fMatchingPorts);

/*
 *  PSLResolveIPAddress
 *
 *  Resolves the specified port to a list of fully qualified port(s).
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLResolveIPAddress(
                            PSL_CONST SOCKADDR* psaAddr,
                            PSLUINT32 cbAddr,
                            PSL_CONST PSLUINT32* rgdwIFList,
                            PSLUINT32 cIFList,
                            SOCKADDR_STORAGE** ppsasResolved,
                            PSLUINT32* pcResolved);


/*
 *  PSLResolveMACAddress
 *
 *  Given a socket address locates the MAC address for the associated
 *  adapter.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLResolveMACAddress(
                            PSL_CONST SOCKADDR* psaAddr,
                            PSLUINT32 cbAddr,
                            PSLBYTE** ppbMAC,
                            PSLUINT32* pcbMac);

#endif  // _PSLIPHELPER_H_

