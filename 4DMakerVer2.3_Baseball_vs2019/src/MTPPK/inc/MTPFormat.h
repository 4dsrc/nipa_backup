/*
 *  MTPFormat.h
 *
 *  Contains definitions and helpers for managing MTP Formats
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MTPFORMAT_H_
#define _MTPFORMAT_H_

/*  MTP Format Info
 *
 *  Defines the basic information about an MTP Format
 */

#define FORMATFLAGS_TYPE_OBJECT         0x0002
#define FORMATFLAGS_TYPE_SERVICEOBJECT  0x0008
#define FORMATFLAGS_TYPE_METHODOBJECT   0x0010

#define FORMATFLAGS_TYPE_MASK           0x00ff

#define GET_FORMATTYPE(_wF)             ((_wF) & FORMATFLAGS_TYPE_MASK)

typedef PSLSTATUS (PSL_API* PFNMTPGETOBJECTBINARY)(
                    PSLPARAM aContextParam,
                    PSLUINT16 wFormat,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);

typedef PSLSTATUS (PSL_API* PFNMTPSETOBJECTBINARY)(
                    PSLPARAM aContextParam,
                    PSLUINT16 wFormat,
                    PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf);

typedef struct _MTPFORMATREC
{
    PSLUINT16                       wFlags;
} MTPFORMATREC;

typedef struct _MTPFORMATINFO
{
    PSL_CONST MTPFORMATREC*         precFormat;
    PSLUINT16                       wFormatCode;
    PSL_CONST MTPOBJECTPROPINFO*    rgopiObjectProps;
    PSLUINT32                       cObjectProps;
    PFNMTPGETOBJECTBINARY           pfnMTPGetObjectBinary;
    PFNMTPSETOBJECTBINARY           pfnMTPSetObjectBinary;
} MTPFORMATINFO;


/*  MTPFormatInfoFromList
 *
 *  Extracts the MTPFORMATINFO dataset from a list of format info's based on
 *  the type of formats stored in the list.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPFormatInfoFromList(
                    PSL_CONST PSLVOID* rgvFormatList,
                    PSLUINT32 cFormats,
                    PSLUINT32 idxFormat,
                    PSLUINT16 wFlags,
                    PSL_CONST MTPFORMATINFO** ppfiResult);

PSL_EXTERN_C PSL_CONST MTPFORMATINFO* PSL_API MTPFormatInfoNext(
                    PSL_CONST MTPFORMATINFO* pfiCur,
                    PSLUINT16 wFlags);

#endif  /* _MTPFORMAT_H_ */

