/*
 *  MTPDeviceHintsService.h
 *
 *  Declares support for the MTP Device Hints Service
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPDEVICEHINTSSERVICE_H_
#define _MTPDEVICEHINTSSERVICE_H_

DEFINE_PSLGUID(SERVICE_HintsPersistentID,
    0xbecf6fe8, 0xed5b, 0x4e53, 0xa3, 0x69, 0x10, 0xa5, 0x21, 0x5b, 0x5b, 0xa2);

/*  MTPDeviceHintsServiceInit
 *
 *  Initialization function for the device hints service
 */
PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceHintsServiceInit(
                    MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwServiceID);

/*  MTPDeviceHintsServiceGetHints
 *
 *  Gets the Hints data for the device hints service
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceHintsServiceGetHints(
                    MTPCONTEXT* pmtpctx,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten);

#endif  /* _MTPDEVICEHINTSSERVICE_H_ */

