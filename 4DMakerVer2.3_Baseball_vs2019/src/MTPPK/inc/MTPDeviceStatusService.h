/*
 *  MTPDeviceStatusService.h
 *
 *  Declares support for the MTP Device Status Service
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPDEVICESTATUSSERVICE_H_
#define _MTPDEVICESTATUSSERVICE_H_

#include "MTPMsgQueue.h"
#include "StatusDeviceService.h"

DEFINE_PSLGUID(SERVICE_StatusPersistentID,
    0x0feaa9b7, 0x4075, 0x429c, 0x8d, 0x59, 0x41, 0x36, 0xd8, 0x5e, 0xe9, 0x4d);


/*  MTPDeviceStatusServiceInit
 *
 *  Initializes an instance of the device status service.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceStatusServiceInit(
                    MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwServiceID);


/*  MTPStatusServiceNotify
 *
 *  Notifies the status service of a property change
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStatusServiceNotifyChange(
                    MTPSERVICE mtpsStatus,
                    PSLPARAM aParamNotify);


/*  MTPStatusServiceAddWatch
 *
 *  Adds a watch to the specified status variable
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStatusServiceAddWatch(
                    MTPSERVICE mtpsStatus,
                    PSLPARAM aParamNotify,
                    PSL_CONST PSLPKEY* pkeyProp,
                    PSLHANDLE* phPropNotifier);


/*  MTPStatusServuceRemoveWatch
 *
 *  Removes a watch from the specified status variable
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStatusServiceRemoveWatch(
                    MTPSERVICE mtpsStatus,
                    PSLPARAM aParamNotify,
                    PSLHANDLE hPropNotifier);


/*  MTPStatusServiceSerializeServiceProp
 *
 *  Serializes a specific service property
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPStatusPropSerializeValue(
                    PSLHANDLE hPropNotifier,
                    PSLUINT16 wPropType,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

#endif  /* _MTPDEVICESTATUSSERVICE_H_ */

