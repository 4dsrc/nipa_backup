/*
 *  MTPDeviceMetadataService.h
 *
 *  Contains declarations for the MTP Metadata Service functionality
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MTPDEVICEMETADATASERVICE_H_
#define _MTPDEVICEMETADATASERVICE_H_

#ifndef MTPMETADATASERVICE_FORMATCODE_METADATA
#define MTPMETADATASERVICE_FORMATCODE_METADATA  0xb001
#endif  /* MTPMETADATASERVICE_FORMATCODE_METADATA */

#define MTPMETADATASERVICEFLAGS_DEFAULT         0x00000001

#define MTPMETADATASERVICE_CABCONTEXT_UNKNOWN   0

DEFINE_PSLGUID(SVCPUID_MTPDeviceMetadata,
    0xca75c35c, 0xb5ad, 0x4f22, 0xa7, 0x90, 0xc4, 0x99, 0xd8, 0x86, 0x9d, 0x6a);


typedef struct _MTPDEVICEMETADATACABINFO
{
    PSLUINT32                       dwFlags;
    PSLUINT64                       cbCABLength;
    PSL_CONST PSLGUID*              pguidContentID;
    PSLCWSTR                        szLocale;
} MTPDEVICEMETADATACABINFO;

/*  MTPDeviceMetadataServiceInit
 *
 *  Creates an instance of the device metadata service and associates it with
 *  the specified context.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceMetadataServiceInit(
                    MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwServiceID,
                    PSLUINT32 dwStorageID);


/*  MTPDeviceMetadataGetCABCount
 *
 *  Callback function implemented in the platform code that returns the
 *  count of metadata objects.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceMetadataGetCABCount(
                    PSLUINT32* pcItems);


/*  MTPDeviceMetadataGetCABInfo
 *
 *  Retrieve information about a particular device metadata CAB
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceMetadataGetCABInfo(
                    PSLUINT32 idxItem,
                    PSL_CONST MTPDEVICEMETADATACABINFO** ppCABinfo);


/*  MTPDeviceMetadataOpenCABObject
 *
 *  Opens a context for the specified CAB object
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceMetadataOpenCABObject(
                    PSLUINT32 idxItem,
                    PSLPARAM* paCABContext);

/*  MTPDeviceMetadataReadCABObject
 *
 *  Retrieve the actual conents of a metadata object
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceMetadataReadCABObject(
                    PSLPARAM aCABContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);


/*  MTPDeviceMetadataCloseCABObject
 *
 *  Closes a the specified CAB context
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDeviceMetadataCloseCABObject(
                    PSLPARAM aCABContext);


/*  Safe Helpers
 */

#define SAFE_MTPDEVICEMETADATACLOSECABOBJECT(_a) \
if (MTPMETADATASERVICE_CABCONTEXT_UNKNOWN != (_a)) \
{ \
    MTPDeviceMetadataCloseCABObject(_a); \
    (_a) = MTPMETADATASERVICE_CABCONTEXT_UNKNOWN; \
}

#endif  /* _MTPDEVICEMETADATASERVICE_H_ */

