/*
 *  MTPDatabase.h
 *
 *  Contains declaration of the MTP database API functions
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPDATABASE_H_
#define _MTPDATABASE_H_

typedef PSLHANDLE   MTPDBSESSION;
typedef PSLHANDLE   MTPDBCONTEXT;
typedef PSLHANDLE   MTPDBCONTEXTDATA;

/*  Storage ID and Object ID Packing
 */

#define PACK_OBJDBID(_x) \
    ((_x) & 0x00ffffff)

#define UNPACK_OBJDBID(_objh)      PACK_OBJDBID(_objh)

#define PACK_STORAGEINDEX(_idx) \
    (((_idx) & 0x000000ff) << 24)

#define UNPACK_STORAGEINDEX(_objh) \
    (((_objh) & 0xff000000) >> 24)

#define MAKE_OBJHANDLE( _dbId, _idx ) \
    (PACK_OBJDBID(_dbId) | PACK_STORAGEINDEX(_idx))

#define MAKE_STORAGEID(_idx) \
    ((((_idx) & 0x000000FF) << 16) | 0x1)

#define GET_STORAGEINDEX(_storageID) \
    (((_storageID) & MTP_STORAGEID_PHYSICAL) >> 16)

/*  Object property information
 */

enum
{
    MTPJOINTYPE_AND = 0,
    MTPJOINTYPE_OR
};

typedef union _MTPDBVPARAM
{
    PSLUINT8            valUInt8;
    PSLINT8             valInt8;
    PSLUINT16           valUInt16;
    PSLINT16            valInt16;
    PSLUINT32           valUInt32;
    PSLINT32            valInt32;
    PSLUINT64           valUInt64;
    PSLINT64            valInt64;
    PSLUINT128          valUInt128;
    PSLINT128           valInt128;
    struct
    {
        PSLCWSTR                sz;
        PSLUINT32               cch;
    } valString;
    struct
    {
        PSLVOID*                rgItems;
        PSLUINT32               cItems;
    } valArray;
} MTPDBVPARAM;

typedef struct _MTPDBVARIANTPROP
{
    PSLUINT16               wPropCode;
    PSLUINT16               wDataType;
    MTPDBVPARAM             vParam;
} MTPDBVARIANTPROP;

typedef struct _MTPDBVARIANTPROP* PMTPDBVARIANTPROP;
typedef PSL_CONST struct _MTPDBVARIANTPROP* PCMTPDBVARIANTPROP;

/*  Object property codes
 */

typedef enum MTP_OBJECTPROPCODE_DATABASEEXTENSION
{
 /* MTP_OBJECTPROPCODE_VENDOREXTENSION_LAST             = 0xDBFF  */
    MTP_OBJECTPROPCODE_OBJECTHANDLE                     = 0xDF00,
    MTP_OBJECTPROPCODE_PROPCODE                         = 0xDF01,
    MTP_OBJECTPROPCODE_GROUPCODE                        = 0xDF02,
    MTP_OBJECTPROPCODE_DEPTH                            = 0xDF03,
    MTP_OBJECTPROPCODE_OBJECTREFERENCES                 = 0xDF04,
    MTP_OBJECTPROPCODE_DATAOFFSET                       = 0xDF05,
    MTP_OBJECTPROPCODE_DATALENGTH                       = 0xDF06,
    MTP_OBJECTPROPCODE_WMPOBJECTMETADATAROUNDTRIP       = 0xDF07,
    MTP_OBJECTPROPCODE_WMPOBJECTACQUIRED                = 0xDF08,
    MTP_OBJECTPROPCODE_SERVICEID                        = 0xDF09,
} MTP_OBJECTPROPCODE_DATABASEEXTENSION;

/*  Service property codes
 */

typedef enum MTP_SERVICEPROPCODE_EXTENSION
{
    MTP_SERVICEPROPCODE_PROPCODE                         = 0xF001,
} MTP_SERVICEPROPCODE_EXTENSION;

/*  Data Formats
 */

enum
{
    MTPDBCONTEXTDATAFORMAT_UNKNOWN = 0,
    MTPDBCONTEXTDATAFORMAT_STORAGE,
    MTPDBCONTEXTDATAFORMAT_STORAGEID,
    MTPDBCONTEXTDATAFORMAT_FORMATID,
    MTPDBCONTEXTDATAFORMAT_DEVICEPROPDESC,
    MTPDBCONTEXTDATAFORMAT_DEVICEPROPLIST,
    MTPDBCONTEXTDATAFORMAT_OBJECTHANDLE,
    MTPDBCONTEXTDATAFORMAT_OBJECTINFO,
    MTPDBCONTEXTDATAFORMAT_OBJECTPROPLIST,
    MTPDBCONTEXTDATAFORMAT_OBJECTPROTECTION,
    MTPDBCONTEXTDATAFORMAT_OBJECTPROPARRAY,
    MTPDBCONTEXTDATAFORMAT_OBJECTBINARYDATA,
    MTPDBCONTEXTDATAFORMAT_OBJECTTHUMBDATA,
    MTPDBCONTEXTDATAFORMAT_OBJECTPROPSSUPPORTED,
    MTPDBCONTEXTDATAFORMAT_OBJECTPROPDESC,
    MTPDBCONTEXTDATAFORMAT_FORMATCAPABILITIES,
    MTPDBCONTEXTDATAFORMAT_SERVICEID,
    MTPDBCONTEXTDATAFORMAT_SERVICEINFO,
    MTPDBCONTEXTDATAFORMAT_SERVICECAPABILITIES,
    MTPDBCONTEXTDATAFORMAT_SERVICEPROPLIST,
    MTPDBCONTEXTDATAFORMAT_SERVICEPROPERTYDESC,
    MTPDBCONTEXTDATAFORMAT_MAX_FORMAT = 0xffff
};

#define MTPDBCONTEXTDATA_QUERY                  0x0000
#define MTPDBCONTEXTDATA_INSERT                 0x0001
#define MTPDBCONTEXTDATA_UPDATE                 0x0002
#define MTPDBCONTEXTDATA_DELETE                 0x0004

#define MTPDBCONTEXTDATA_FORMAT_MASK            0xffff0000
#define MTPDBCONTEXTDATA_FLAGS_MASK             0x0000ffff


#define MAKE_MTPDBCONTEXTDATADESC(_flags, _format) \
        (PSLUINT32)(((0xffff & (_format)) << 16) | \
         (MTPDBCONTEXTDATA_FLAGS_MASK & (_flags)))

#define MTPDBCONTEXTDATADESC_FORMAT(_dwDataDesc) \
        (PSLUINT16)((MTPDBCONTEXTDATA_FORMAT_MASK & (_dwDataDesc)) >> 16)

#define MTPDBCONTEXTDATADESC_FLAGS(_dwDataDesc) \
        (PSLUINT16)(MTPDBCONTEXTDATA_FLAGS_MASK & (_dwDataDesc))


/*  MTP Database Session Initialization
 *
 *  Provides an opportunity to the MTP DB Session sub-system to perform any
 *  initialization based on the formats being supported.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBSessionInitialize(
                    PCXPSLUINT16 pwFormatCodes,
                    PSLUINT32 cdwFormatCodes);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBSessionUninitialize(PSLVOID);


/*  MTP Database Session Object
 *
 *  The MTP Database Session object is the root object for the "legacy" MTP
 *  object store.  it must be implemented by the platform to provide access
 *  to this store.
 */

/*  pfnMTPDBSessionClose - PURE
 *
 *  Closes the specified MTPDBSESSION instance
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBSESSIONCLOSE)(
                    MTPDBSESSION hDBSession);


/*  pfnMTPDBSessionDBContextOpen - PURE
 *
 *  Opens a DBContext (the "where" portion of the query) in the current session
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBSESSIONDBCONTEXTOPEN)(
                    MTPDBSESSION hDBSession,
                    PSLUINT32 dwJoinType,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    MTPDBCONTEXT* pmdbc);


/*  MTPDBSESSIONVTBL
 *
 *  Virtual function table for the MTP DBSession object
 */

typedef struct _MTPDBSESSIONVTBL
{
    PFNMTPDBSESSIONCLOSE            pfnMTPDBSessionClose;
    PFNMTPDBSESSIONDBCONTEXTOPEN    pfnMTPDBSessionDBContextOpen;
} MTPDBSESSIONVTBL;


/*  MTPDBSESSIONOBJ
 *
 *  Contains all of the public state necessary for the MTP Database Session
 *  object.  The MTPDBSESSION handle MUST point to an object which contains
 *  an MTPDBSESSIONOBJ at the specified address.
 */

typedef struct _MTPDBSESSIONOBJ
{
    PSL_CONST MTPDBSESSIONVTBL*     vtbl;
} MTPDBSESSIONOBJ;

typedef PSL_CONST MTPDBSESSIONOBJ* PMTPDBSESSIONOBJ;

/*
 *  MTPDBSessionOpen
 *
 *  This function is used to open or start a DB session.  It is typically
 *  called during service initialization and used to register the legacy
 *  service.
 *
 *  Arguments:
 *      MTPDBSESSION*   phDBSession     Variable to return opened DB
 *                                          Session handle
 *
 *  Returns:
 *      PSLSTATUS       PSLSUCCESS on success
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBSessionOpen(
                    MTPDBSESSION* phDBSession);


/*  MTP Database Session Virtual Function Helper Macros
 */

#define GET_MTPDBSESSIONVTBL(_po)   (((PMTPDBSESSIONOBJ)(_po))->vtbl)

#define MTPDBSessionClose(_po) \
    GET_MTPDBSESSIONVTBL(_po)->pfnMTPDBSessionClose(_po)

#define MTPDBSessionDBContextOpen(_po, _dwJT, _pmq, _cmq, _pdbc) \
    GET_MTPDBSESSIONVTBL(_po)->pfnMTPDBSessionDBContextOpen(_po, _dwJT, _pmq, _cmq, _pdbc)


/*  MTP Database Context Object
 *
 *  The MTP Database Context Object represents the context for a particular
 *  database operation.  In SQL query terminology it would be considered the
 *  "where" portion of the query.  It defines the set of objects that will
 *  be operated upon and may be persisted for more than one call.
 */


/*  pfnMTPDBContextClose - PURE
 *
 *  Closes the context set up by MTPDBContextOpen
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTCLOSE)(
                    MTPDBCONTEXT mdbc);


/*  pfnMTPDBContextCancel - TRUE
 *
 *  Called when an in progress operation is cancelled.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTCANCEL)(
                    MTPDBCONTEXT mdbc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextCancelBase(
                    MTPDBCONTEXT mdbc);


/*  pfnMTPDBContextGetCount - PURE
 *
 *  Returns the count of items in the current context
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTGETCOUNT)(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32* pcItems);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextGetCountBase(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32* pcItems);


/*  pfnMTPDBContextGetHandle - PURE
 *
 *  Returns one of the object handles associated with the context.  By default
 *  this function will not run a query against the database to retrieve these
 *  unless it is fResolveContext paramter is set to PSLTRUE. By doing that
 *  caller is forcing the function to resolve the context and in turn run a
 *  query to gather the details.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTGETHANDLE)(
                    MTPDBCONTEXT mdbc,
                    PSLBOOL fResolveContext,
                    PSLUINT32 idxItem,
                    PSLUINT32* pdwObjectHandle,
                    PSLUINT32* pdwParentHandle,
                    PSLUINT32* pdwStorageID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextGetHandleBase(
                    MTPDBCONTEXT mdbc,
                    PSLBOOL fResolveContext,
                    PSLUINT32 idxItem,
                    PSLUINT32* pdwObjectHandle,
                    PSLUINT32* pdwParentHandle,
                    PSLUINT32* pdwStorageID);


/*  pfnMTPDBContextCommit - PURE
 *
 *  This function when called to commit all the data that may have been
 *  cached within this context to the database. This doesn't mean that
 *  database cannot peform commits outside of this call. It will
 *  do so when required but this is the way the handler indicate
 *  it is time for it to commit. This function would help handlers to set
 *  multiple properties of same object and perform data base commit in one
 *  shot
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTCOMMIT)(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32 dwObjectID);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextCommitBase(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32 dwObjectID);


/*  pfnMTPDBContextCopy - PURE
 *
 *  Copys the contents of the source DB Context to the destination DB Context.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTCOPY)(
                    MTPDBCONTEXT mdbcDest,
                    MTPDBCONTEXT mdbcSrc);


PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextCopyBase(
                    MTPDBCONTEXT mdbcDest,
                    MTPDBCONTEXT mdbcSrc);


/*  pfnMTPDBContextMove - PURE
 *
 *  Moves the contents of the source DB Context to the destination DB Context.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTMOVE)(
                    MTPDBCONTEXT mdbcDest,
                    MTPDBCONTEXT mdbcSrc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextMoveBase(
                    MTPDBCONTEXT mdbcDest,
                    MTPDBCONTEXT mdbcSrc);


/*  pfnMTPDBContextDelete - PURE
 *
 *  Deletes the contents of the specified DB Context.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTDELETE)(
                    MTPDBCONTEXT mdbc);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDeleteBase(
                    MTPDBCONTEXT mdbc);


/*  pfnMTPDBContextFormat - PURE
 *
 *  Formats the stores in the specified DB Context
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTFORMAT)(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32 dwFSFormat);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextFormatBase(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32 dwFSFormat);


/*  pfnMTPDBContextDataOpen - PURE
 *
 *  Opens a DB Context Data for this object.  The context data is the "what"
 *  portion of the query.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTDATAOPEN)(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32 dwDataFormat,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    PSLUINT64* pcbTotalSize,
                    MTPDBCONTEXTDATA* pmdbcd);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDataOpenBase(
                    MTPDBCONTEXT mdbc,
                    PSLUINT32 dwDataFormat,
                    PSL_CONST MTPDBVARIANTPROP* rgmdbv,
                    PSLUINT32 cmdbv,
                    PSLUINT64* pcbTotalSize,
                    MTPDBCONTEXTDATA* pmdbcd);

/*  MTPDBCONTEXTVTBL
 *
 *  Virtual function table for the MTP DB Context
 */

typedef struct _MTPDBCONTEXTVTBL
{
    PFNMTPDBCONTEXTCLOSE            pfnMTPDBContextClose;
    PFNMTPDBCONTEXTCANCEL           pfnMTPDBContextCancel;
    PFNMTPDBCONTEXTGETCOUNT         pfnMTPDBContextGetCount;
    PFNMTPDBCONTEXTGETHANDLE        pfnMTPDBContextGetHandle;
    PFNMTPDBCONTEXTCOMMIT           pfnMTPDBContextCommit;
    PFNMTPDBCONTEXTCOPY             pfnMTPDBContextCopy;
    PFNMTPDBCONTEXTMOVE             pfnMTPDBContextMove;
    PFNMTPDBCONTEXTDELETE           pfnMTPDBContextDelete;
    PFNMTPDBCONTEXTFORMAT           pfnMTPDBContextFormat;
    PFNMTPDBCONTEXTDATAOPEN         pfnMTPDBContextDataOpen;
} MTPDBCONTEXTVTBL;


/*  MTPDBCONTEXTOBJ
 *
 *  Contains all of the public state necessary for the MTP Database Context
 *  object.  The MTPDBCONTEXT handle must point to a memory location that
 *  contains this information.
 */

typedef struct _MTPDBCONTEXTOBJ
{
    PSL_CONST MTPDBCONTEXTVTBL*     vtbl;
} MTPDBCONTEXTOBJ;

typedef PSL_CONST MTPDBCONTEXTOBJ* PMTPDBCONTEXTOBJ;

/*  MTP Database Context Virtual Function Helper Macros
 */

#define GET_MTPDBCONTEXTVTBL(_po)   (((PMTPDBCONTEXTOBJ)(_po))->vtbl)

#define MTPDBContextClose(_po) \
    GET_MTPDBCONTEXTVTBL(_po)->pfnMTPDBContextClose(_po)

#define MTPDBContextCancel(_po) \
    GET_MTPDBCONTEXTVTBL(_po)->pfnMTPDBContextCancel(_po)

#define MTPDBContextGetCount(_po, _pc) \
    GET_MTPDBCONTEXTVTBL(_po)->pfnMTPDBContextGetCount(_po, _pc)

#define MTPDBContextGetHandle(_po, _f, _idx, _pO, _pP, _pS) \
    GET_MTPDBCONTEXTVTBL(_po)->pfnMTPDBContextGetHandle(_po, _f, _idx, _pO, _pP, _pS)

#define MTPDBContextCommit(_po, _id) \
    GET_MTPDBCONTEXTVTBL(_po)->pfnMTPDBContextCommit(_po, _id)

#define MTPDBContextCopy(_pd, _ps) \
    GET_MTPDBCONTEXTVTBL(_pd)->pfnMTPDBContextCopy(_pd, _ps)

#define MTPDBContextMove(_pd, _ps) \
    GET_MTPDBCONTEXTVTBL(_pd)->pfnMTPDBContextMove(_pd, _ps)

#define MTPDBContextDelete(_po) \
    GET_MTPDBCONTEXTVTBL(_po)->pfnMTPDBContextDelete(_po)

#define MTPDBContextFormat(_po, _fsf) \
    GET_MTPDBCONTEXTVTBL(_po)->pfnMTPDBContextFormat(_po, _fsf)

#define MTPDBContextDataOpen(_po, _dwF, _pmq, _cmq, _pcb, _pcd) \
    GET_MTPDBCONTEXTVTBL(_po)->pfnMTPDBContextDataOpen(_po, _dwF, _pmq, _cmq, _pcb, _pcd)


/*  MTP Database Context Data
 *
 *  The MTP DB Context Data abstraction manages the actual data being
 *  serialized on or off the MTP protocol.  In SQL terminology it corresponds
 *  to the "what" portion of a query.
 */

/*  pfnMTPDBContextDataClose - PURE
 *
 *  Closes the DB Context Data object.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTDATACLOSE)(
                    MTPDBCONTEXTDATA mdbcd);


/*  pfnMTPDBContextDataCancel - TRUE
 *
 *  Called when an in progress operation is cancelled.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTDATACANCEL)(
                    MTPDBCONTEXTDATA mdbcd);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDataCancelBase(
                    MTPDBCONTEXTDATA mdbcd);


/*  pfnMTPDBContextDataSerialize - PURE
 *
 *  Serializes the "what" portion of the query into the provided MTP buffer
 *  for transmission.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTDATASERIALIZE)(
                    MTPDBCONTEXTDATA mdbcd,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDataSerializeBase(
                    MTPDBCONTEXTDATA mdbcd,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbWritten,
                    PSLUINT64* pcbLeft);


/*  pfnMTPDBContextDataStartDeserialize - PURE
 *
 *  Called to inform the context data object that deserialization is about
 *  to begin.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTDATASTARTDESERIALIZE)(
                    MTPDBCONTEXTDATA mdbcd,
                    PSLUINT64 cbData);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDataStartDeserializeBase(
                    MTPDBCONTEXTDATA mdbcd,
                    PSLUINT64 cbData);

/*  pfnMTPDBContextDataDeserialize - PURE
 *
 *  Called to deserialize the incmoing MTP data buffers.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDBCONTEXTDATADESERIALIZE)(
                    MTPDBCONTEXTDATA mdbcd,
                    PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLMSGQUEUE mqHandler,
                    MTPCONTEXT* pmtpctx,
                    PSLPARAM aParam,
                    PSLUINT32 dwMsgFlags);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBContextDataDeserializeBase(
                    MTPDBCONTEXTDATA mdbcd,
                    PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLMSGQUEUE mqHandler,
                    MTPCONTEXT* pmtpctx,
                    PSLPARAM aParam,
                    PSLUINT32 dwMsgFlags);


/*  MTPDBCONTEXTDATAVTBL
 *
 *  Virtual function table associated with the MTP DB Context Data object.
 */

typedef struct _MTPDBCONTEXTDATAVTBL
{
    PFNMTPDBCONTEXTDATACLOSE            pfnMTPDBContextDataClose;
    PFNMTPDBCONTEXTDATACANCEL           pfnMTPDBContextDataCancel;
    PFNMTPDBCONTEXTDATASERIALIZE        pfnMTPDBContextDataSerialize;
    PFNMTPDBCONTEXTDATASTARTDESERIALIZE pfnMTPDBContextDataStartDeserialize;
    PFNMTPDBCONTEXTDATADESERIALIZE      pfnMTPDBContextDataDeserialize;
} MTPDBCONTEXTDATAVTBL;


/*  MTPDBCONTEXTDATAOBJ
 *
 *  Contains all of the public state associated with the MTP DB Context Data
 *  object.  The value pointed to by an MTPDBCONTEXTDATA handle should be
 *  an MTPDBCONTEXTDATAOBJ.
 */

typedef struct _MTPDBCONTEXTDATAOBJ
{
    PSL_CONST MTPDBCONTEXTDATAVTBL*     vtbl;
} MTPDBCONTEXTDATAOBJ;

typedef PSL_CONST MTPDBCONTEXTDATAOBJ* PMTPDBCONTEXTDATAOBJ;

/*  MTP Database Context Data Virtual Function Helper Macros
 */

#define GET_MTPDBCONTEXTDATAVTBL(_po)   (((PMTPDBCONTEXTDATAOBJ)(_po))->vtbl)

#define MTPDBContextDataClose(_po) \
    GET_MTPDBCONTEXTDATAVTBL(_po)->pfnMTPDBContextDataClose(_po)

#define MTPDBContextDataCancel(_po) \
    GET_MTPDBCONTEXTDATAVTBL(_po)->pfnMTPDBContextDataCancel(_po)

#define MTPDBContextDataSerialize(_po, _pv, _cb, _pcbW, _pcbL) \
    GET_MTPDBCONTEXTDATAVTBL(_po)->pfnMTPDBContextDataSerialize(_po, _pv, _cb, _pcbW, _pcbL)

#define MTPDBContextDataStartDeserialize(_po, _cb) \
    GET_MTPDBCONTEXTDATAVTBL(_po)->pfnMTPDBContextDataStartDeserialize(_po, _cb)

#define MTPDBContextDataDeserialize(_po, _pv, _cb, _mq, _pctx, _aP, _dwM) \
    GET_MTPDBCONTEXTDATAVTBL(_po)->pfnMTPDBContextDataDeserialize(_po, _pv, _cb, _mq, _pctx, _aP, _dwM)


/*  MTPDBTerminate
 *
 *  Helper function for terminating all pending MTP Database operations.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBTerminate(
                    MTPDBCONTEXT mdbc,
                    MTPDBCONTEXTDATA mdbcd);


/*  MTPDBResolveObjectHandleToStorageID
 *
 *  Maps an MTP object handle to the service or storage that it lives in.
 *  Returns PSL_SUCCESS if mapping is valid, PSLSUCCESS_NOT_FOUND if storage ID
 *  is not located and item should be forwarded to the legacy service.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDBResolveObjectHandleToStorageID(
                    MTPCONTEXT* pmtpctx,
                    PSLUINT32 dwObjectHandle,
                    PSLUINT32* pdwStorageID);


/*  Safe Helpers
  */

#define SAFE_MTPDBSESSIONCLOSE(_pv) \
if (PSLNULL != (_pv)) \
{ \
    MTPDBSessionClose(_pv); \
    (_pv) = PSLNULL; \
}

#define SAFE_MTPDBCONTEXTCLOSE(_pv) \
if (PSLNULL != (_pv)) \
{ \
    MTPDBContextClose(_pv); \
    (_pv) = PSLNULL; \
}

#define SAFE_MTPDBCONTEXTDATACLOSE(_pv) \
if (PSLNULL != (_pv)) \
{ \
    MTPDBContextDataClose(_pv); \
    (_pv) = PSLNULL; \
}

#endif  /* _MTPDATABASE_H_ */
