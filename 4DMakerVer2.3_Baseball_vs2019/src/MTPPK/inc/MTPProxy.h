/*
 *  MTPProxy.h
 *
 *  Contains definitions of the MTP Proxy API
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPPROXY_H_
#define _MTPPROXY_H_

/*
 *  MTPPROXYUPDATE
 *
 *  Declaration of the route proxy structure.  This structure is
 *  provided to the router when an route is ended so that the route
 *  state may be updated.
 *
 */

typedef struct
{
    PSLLOOKUPTABLE          hHandlerTableNew;
} MTPPROXYUPDATE;


/*
 *  Proxy creation and destruction
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPProxyCreate(
                            PSLMSGQUEUE mqTransport,
                            PSLMSGQUEUE mqEventHandler,
                            PSLLOOKUPTABLE hHandlerTable,
                            MTPCONTEXT** ppmtpctx);

PSL_EXTERN_C PSLSTATUS PSL_API MTPProxyDestroy(
                            MTPCONTEXT* pmtpctx);


/*
 *  Proxy Management Functions
 *
 */

typedef PSLSTATUS (PSL_API* PFNMTPPROXYBEGINOP)(
                            MTPCONTEXT* pmtpctx,
                            PSLUINT16 wOpCode,
                            PSLVOID* pvOpData);

PSL_EXTERN_C PSLSTATUS PSL_API MTPProxyBegin(
                            MTPCONTEXT* pmtpctx,
                            PSLUINT16 wOpCode,
                            PSLVOID* pvOpData);

PSL_EXTERN_C PSLSTATUS PSL_API MTPProxyEnd(
                            MTPCONTEXT* pmtpctx,
                            MTPPROXYUPDATE* pmtppu);

/*
 *  Proxy Functions
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPProxyMsgToTransport(
                            MTPCONTEXT* pmtpctx,
                            PSLMSG* pmsg);

PSL_EXTERN_C PSLSTATUS PSL_API MTPProxyMsgToHandler(
                            MTPCONTEXT* pmtpctx,
                            PSLMSG* pmsg);

PSL_EXTERN_C PSLSTATUS PSL_API MTPProxyTransportError(
                            MTPCONTEXT* pmtpctx,
                            PSLMSGPOOL mpSrc,
                            PSLMSGID msgID,
                            PSLUINT32 dwSequenceID,
                            PSLSTATUS psErr);

/*
 *  Safe Functions
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#ifndef SAFE_MTPPROXYDESTROY
#define SAFE_MTPPROXYDESTROY(_r) \
if (PSLNULL != (_r)) \
{ \
    MTPProxyDestroy(_r); \
    (_r) = PSLNULL; \
}
#endif  /* SAFE_MTPPROXYDESTROY */


#endif  /* _MTPPROXY_H_ */

