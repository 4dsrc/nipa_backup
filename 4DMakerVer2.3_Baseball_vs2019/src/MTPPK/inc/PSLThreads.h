/*
 *  PSLThreads.h
 *
 *  Contains PSL abstractions for thread operations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLTHREADS_H_
#define _PSLTHREADS_H_

#define PSLTHREAD_DEFAULT_STACK_SIZE        0xffffffff
#define PSLTHREAD_INFINITE                  0xffffffff

enum
{
    PSLTHREADPRIORITY_UNKNOWN = 0,
    PSLTHREADPRIORITY_TIME_CRITICAL,
    PSLTHREADPRIORITY_HIGHEST,
    PSLTHREADPRIORITY_ABOVE_NORMAL,
    PSLTHREADPRIORITY_NORMAL,
    PSLTHREADPRIORITY_BELOW_NORMAL,
    PSLTHREADPRIORITY_LOWEST,
    PSLTHREADPRIORITY_ABOVE_IDLE,
    PSLTHREADPRIORITY_IDLE,
};


/*  Thread Procedure
 */

typedef PSLUINT32 (PSL_API* PFNPSLTHREADFUNC)(
                            PSLVOID* pvData);

/*  Thread Create and Close Routines
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLThreadCreate(
                            PFNPSLTHREADFUNC pfnThreadFunc,
                            PSLVOID* pvData,
                            PSLBOOL fCreateSuspended,
                            PSLUINT32 cbStack,
                            PSLHANDLE* phThread);

PSL_EXTERN_C PSLSTATUS PSL_API PSLThreadClose(
                            PSLHANDLE hThread);

PSL_EXTERN_C PSLSTATUS PSL_API PSLThreadTerminate(
                            PSLHANDLE hThread,
                            PSLUINT32 dwResult);

/*  Retrieving Thread state
 */

PSL_EXTERN_C PSLUINT32 PSL_API PSLThreadGetCurrentThreadID(
                            PSLVOID);

PSL_EXTERN_C PSLSTATUS PSL_API PSLThreadGetResult(
                            PSLHANDLE hThread,
                            PSLUINT32 dwMSTimeout,
                            PSLUINT32* pdwResult);

/*  Controlling Thread State and Priority
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLSleep(
                            PSLUINT32 dwMSSleep);

PSL_EXTERN_C PSLSTATUS PSL_API PSLThreadSetPriority(
                            PSLHANDLE hThread,
                            PSLUINT32 dwPriority);

PSL_EXTERN_C PSLSTATUS PSL_API PSLThreadGetPriority(
                            PSLHANDLE hThread,
                            PSLUINT32* pdwPriority);

PSL_EXTERN_C PSLSTATUS PSL_API PSLThreadSuspend(
                            PSLHANDLE hThread);

PSL_EXTERN_C PSLSTATUS PSL_API PSLThreadResume(
                            PSLHANDLE hThread);


/*  Synchronization Objects
 *
 *  The objects indicated below may be used to create generic synchronization
 *  objects to support atomic operations in multithreaded programming.  For
 *  simplicity only two type of objects are defined- a mutual exclusion object
 *  and an event (signalling) object.
 */


/*  PSL Mutex Support
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMutexOpen(
                            PSLUINT32 dwWaitToAcquire,
                            PSLCWSTR szName,
                            PSLHANDLE* phPSLMutex);

#ifdef PSL_MUTEX_TRACE

PSL_EXTERN_C PSLSTATUS PSL_API PSLMutexTraceStart(
                            PSLHANDLE hPSLMutex,
                            PSLCSTR szFile,
                            PSLUINT32 dwLine);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMutexTraceMutexOpen(
                            PSLUINT32 dwWaitToAcquire,
                            PSLCWSTR szName,
                            PSLHANDLE* hPSLMutex,
                            PSLCSTR szFile,
                            PSLUINT32 dwLine);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMutexTraceClose(
                            PSLHANDLE hPSLMutex,
                            PSLCSTR szFile,
                            PSLUINT32 dwLine);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMutexTraceAcquire(
                            PSLHANDLE hPSLMutex,
                            PSLUINT32 dwMSTimeout,
                            PSLCSTR szFile,
                            PSLUINT32 dwLine);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMutexTraceRelease(
                            PSLHANDLE hPSLMutex,
                            PSLCSTR szFile,
                            PSLUINT32 dwLine);

#define PSLMutexTrace(_hMutex) \
        PSLMutexTraceStart(_hMutex, __FILE__, __LINE__)

#define PSLMutexTraceOpen(_hMutex, _szName, _phMutex) \
        PSLMutexTraceMutexOpen(_hMutex, _szName, _phMutex, __FILE__, __LINE__)

#define PSLMutexClose(_hMutex) \
        PSLMutexTraceClose(_hMutex, __FILE__, __LINE__)

#define PSLMutexAcquire(_hMutex, _dwWait) \
        PSLMutexTraceAcquire(_hMutex, _dwWait, __FILE__, __LINE__)

#define PSLMutexRelease(_hMutex) \
        PSLMutexTraceRelease(_hMutex, __FILE__, __LINE__)

#else   /* !PSL_MUTEX_TRACE */

PSL_EXTERN_C PSLSTATUS PSL_API PSLMutexClose(
                            PSLHANDLE hPSLMutex);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMutexAcquire(
                            PSLHANDLE hPSLMutex,
                            PSLUINT32 dwMSTimeout);

PSL_EXTERN_C PSLSTATUS PSL_API PSLMutexRelease(
                            PSLHANDLE hPSLMutex);

#endif  /* PSL_MUTEX_TRACE */


/*  PSL Signal Support
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLSignalOpen(
                                PSLBOOL fSignalled,
                                PSLBOOL fManualReset,
                                PSLCWSTR szName,
                                PSLHANDLE* phPSLSignal);

PSL_EXTERN_C PSLSTATUS PSL_API PSLSignalClose(
                                PSLHANDLE hPSLSignal);

PSL_EXTERN_C PSLSTATUS PSL_API PSLSignalSet(
                                PSLHANDLE hPSLSignal);

PSL_EXTERN_C PSLSTATUS PSL_API PSLSignalReset(
                                PSLHANDLE hPSLSignal);

PSL_EXTERN_C PSLSTATUS PSL_API PSLSignalWait(
                                PSLHANDLE hPSLSignal,
                                PSLUINT32 dwMSTimeout);


/*  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 */

#define SAFE_PSLTHREADCLOSE(_ht) \
if (PSLNULL != _ht) \
{ \
    PSLThreadClose(_ht); \
    _ht = PSLNULL; \
}

#define SAFE_PSLMUTEXCLOSE(_hm) \
if (PSLNULL != _hm) \
{ \
    PSLMutexClose(_hm); \
    _hm = PSLNULL; \
}

#define SAFE_PSLMUTEXRELEASE(_hm) \
if (PSLNULL != _hm) \
{ \
    PSLMutexRelease(_hm); \
    _hm = PSLNULL; \
}

#define SAFE_PSLMUTEXRELEASECLOSE(_hm) \
if (PSLNULL != _hm) \
{ \
    PSLMutexRelease(_hm); \
    PSLMutexClose(_hm); \
    _hm = PSLNULL; \
}

#define SAFE_PSLSIGNALCLOSE(_hs) \
if (PSLNULL != _hs) \
{ \
    PSLSignalClose(_hs); \
    _hs = PSLNULL; \
}

#endif  /* _PSLTHREADS_H_ */

