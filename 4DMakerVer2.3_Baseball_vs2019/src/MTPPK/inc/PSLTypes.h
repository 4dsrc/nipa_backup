/*
 *  PSLTypes.h
 *
 *  Declares standard types used in the PSL implementation in a platform
 *  agnostic fashion.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLTYPES_H_
#define _PSLTYPES_H_


/*
 *  Precompiler Macros
 *
 */

#ifndef PSL_EXTERN
#define PSL_EXTERN extern
#endif  /* PSL_EXTERN */

#ifndef PSL_EXTERN_C
#ifdef __cplusplus
#define PSL_EXTERN_C PSL_EXTERN "C"
#else
#define PSL_EXTERN_C PSL_EXTERN
#endif
#endif

#ifndef PSL_API
#define PSL_API
#endif

#ifndef PSL_CONST
#define PSL_CONST const
#endif

#ifndef PSL_UNALIGNED
#define PSL_UNALIGNED
#endif

#ifndef PSL_PACK1
#define PSL_PACK1
#endif

#ifndef PSL_PACK2
#define PSL_PACK2
#endif

#ifndef PSL_PACK4
#define PSL_PACK4
#endif

#ifndef PSL_PACK8
#define PSL_PACK8
#endif

#ifndef PSL_PACK16
#define PSL_PACK16
#endif

/*
 *  Basic PSL Variable Types
 *
 */

typedef signed char PSLINT8;
typedef unsigned char PSLUINT8;

typedef PSLUINT8 PSLBYTE;

typedef short PSLINT16;

typedef unsigned short PSLUINT16;
typedef PSLUINT16 PSL_UNALIGNED* PXPSLUINT16;
typedef PSL_CONST PSLUINT16 PSL_UNALIGNED* PCXPSLUINT16;

typedef long PSLINT32;

typedef unsigned long PSLUINT32;
typedef PSLUINT32 PSL_UNALIGNED* PXPSLUINT32;
typedef PSL_CONST PSLUINT32 PSL_UNALIGNED* PCXPSLUINT32;

typedef long long PSLINT64;

typedef unsigned long long PSLUINT64;
typedef PSLUINT64 PSL_UNALIGNED* PXPSLUINT64;
typedef PSL_CONST PSLUINT64 PSL_UNALIGNED* PCXPSLUINT64;

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPush1.h"
#endif

struct _PSLINT128
{
#ifdef PSL_LITTLE_ENDIAN
    PSLUINT64       ulLowPart;
    PSLINT64        llHighPart;
#else /* !PSL_LITTLE_ENDIAN */
    PSLINT64        llHighPart;
    PSLUINT64       ulLowPart;
#endif  /* PSL_LITTLE_ENDIAN */
} PSL_PACK1;

typedef struct _PSLINT128 PSLINT128;

struct _PSLUINT128
{
#ifdef PSL_LITTLE_ENDIAN
    PSLUINT64       ulLowPart;
    PSLUINT64       ulHighPart;
#else /* !PSL_LITTLE_ENDIAN */
    PSLUINT64       ulHighPart;
    PSLUINT64       ulLowPart;
#endif  /* PSL_LITTLE_ENDIAN */
} PSL_PACK1;

typedef struct _PSLUINT128 PSLUINT128;
typedef PSLUINT128 PSL_UNALIGNED* PXPSLUINT128;
typedef PSL_CONST PSLUINT128 PSL_UNALIGNED* PCXPSLUINT128;

#ifdef PSL_PACK_PRAGMA
#include "PSLPackPop.h"
#endif

typedef PSLUINT32 PSLBOOL;

typedef void PSLVOID;
typedef PSLVOID* PSLHANDLE;

typedef double PSLDOUBLE;
typedef PSLDOUBLE PSL_UNALIGNED* PXPSLDOUBLE;
typedef PSL_CONST PSLDOUBLE PSL_UNALIGNED* PCXPSLDOUBLE;

typedef char PSLCHAR;
typedef PSLCHAR* PSLSTR;
typedef PSL_CONST PSLCHAR* PSLCSTR;

typedef PSLUINT16 PSLWCHAR;
typedef PSLWCHAR* PSLWSTR;
typedef PSL_CONST PSLWCHAR* PSLCWSTR;
typedef PSLWCHAR PSL_UNALIGNED* PXPSLWSTR;
typedef PSL_CONST PSLWCHAR PSL_UNALIGNED* PCXPSLWSTR;

#ifndef PSLSIZET_FORWARD_DEFINED
#define PSLSIZET_FORWARD_DEFINED
#ifdef PSL_64BIT_PORTABILITY
typedef PSLUINT64 PSLSIZET;
#else
typedef PSLUINT32 PSLSIZET;
#endif  /* PSL_64BIT_PORTABILITY */
#endif  /* PSLSIZET_FORWARD_DEFINED */

typedef PSLSIZET PSLPARAM;
typedef PSLUINT64 PSLLPARAM;


/*
 *  PSL Compatibility Types
 *
 *  The types defined here are for compatibility with well-known
 *  types used in Windows development.
 *
 */

typedef struct _PSLGUID
{
    PSLUINT32               Data1;
    PSLUINT16               Data2;
    PSLUINT16               Data3;
    PSLBYTE                 Data4[8];
} PSLGUID;

#ifdef PSL_INITGUID

#define DEFINE_PSLGUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
        PSL_EXTERN_C PSL_CONST PSLGUID name \
                = { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }

#else   /* !PSL_INITGUID */

#define DEFINE_PSLGUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
        PSL_EXTERN_C PSL_CONST PSLGUID name

#endif  /* PSL INITGUID */

typedef PSLGUID PSL_UNALIGNED* PXPSLGUID;
typedef PSL_CONST PSLGUID PSL_UNALIGNED* PCXPSLGUID;

typedef struct _PSLPKEY
{
    PSLGUID     nsid;
    PSLUINT32   pid;
} PSLPKEY;

#ifdef PSL_INITGUID

#define DEFINE_PSLPROPERTYKEY(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8, \
                              ID) \
        PSL_EXTERN_C PSL_CONST PSLPKEY name \
                = { { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }, \
                    ID }

#else   /* !PSL_INITGUID */

#define DEFINE_PSLPROPERTYKEY(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8, \
                              ID) \
        PSL_EXTERN_C PSL_CONST PSLPKEY name

#endif  /* PSL INITGUID */

typedef PSLPKEY PSL_UNALIGNED* PXPSLPKEY;
typedef PSL_CONST PSLPKEY PSL_UNALIGNED* PCXPSLPKEY;

/*
 *  PSL Error Handling
 *
 *  All PSL functions return a result of type PSLSTATUS.  PSL status codes
 *  are positive or negative values.  Positive values indicate success,
 *  negative values indicate failure.
 *
 */

typedef PSLINT32 PSLSTATUS;

#define PSL_SUCCEEDED(_ps)  (0 <= (_ps))
#define PSL_FAILED(_ps)     (0 > (_ps))

#define PSLTRUE  1
#define PSLFALSE 0
#define PSLNULL  0

#endif  /* _PSLTYPES_H_ */

