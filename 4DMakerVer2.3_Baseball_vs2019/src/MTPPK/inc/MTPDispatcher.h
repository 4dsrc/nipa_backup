/*
 *  MTPDispatcher.h
 *
 *  Contains declaration for the MTP Dispatcher.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPDISPATCHER_H_
#define _MTPDISPATCHER_H_

typedef PSLHANDLE MTPDISPATCHER;

/*
 *  PFNMTPMSGDISPATCH
 *
 *  Callback function used by the message dispatcher to transfer
 *  processing of messages to the correct handler.
 *
 */

typedef PSLSTATUS (PSL_API* PFNMTPMSGDISPATCH)(
                            MTPDISPATCHER md,
                            PSLMSG* pmsg);

typedef PSLSTATUS (PSL_API* PFNMTPONCMDDISPATCH)(
                            PSLMSG* pmsg);

/*
 *  MTPMSGDISPATCHER
 *
 *  For handlers which use the MTP dispatcher to dispatch messages, the
 *  dispatcher implementation assumes that the first entry in the
 *  handlers private data is the message dispatcher structure.
 *  When declaring handler context data structures the following
 *  pattern is recommended:
 *
 *  typedef struct
 *  {
 *      MTPMSGDISPATCHER    msgDispatch;
 *      ...
 *      Handler specific entries
 *      ...
 *  } HANDLERFOOCONTEXT;
 *
 */

typedef struct
{
    PFNMTPMSGDISPATCH       pfnMsgDispatch;
} MTPMSGDISPATCHER;

/*
 *  Dispatcher Registration Functions
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDispatcherCreate(
                            PSLUINT32 cdwMsgs,
                            PFNMTPONCMDDISPATCH pfnOnCmd,
                            MTPDISPATCHER* pmd);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDispatcherDestroy(
                            MTPDISPATCHER md);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDispatcherGetMsgQueue(
                            MTPDISPATCHER md,
                            PSLMSGQUEUE* pmqDispatch);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDispatcherGetMsgPool(
                            MTPDISPATCHER md,
                            PSLMSGPOOL* pmpDispatch);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDispatcherRouteEnd(
                            MTPDISPATCHER md,
                            MTPCONTEXT* pmtpctx);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDispatcherRouteTerminate(
                            MTPDISPATCHER md,
                            MTPCONTEXT* pmtpctx,
                            PSLMSGID msgIDTerminate);

/*
 *  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#define SAFE_MTPDISPATCHERDESTROY(_md) \
if (PSLNULL != _md) \
{ \
    MTPDispatcherDestroy(_md); \
    _md = PSLNULL; \
}

#endif  /* _MTPDISPATCHER_H_ */

