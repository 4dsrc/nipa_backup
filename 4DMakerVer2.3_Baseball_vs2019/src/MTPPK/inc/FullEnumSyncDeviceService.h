/*
 *  FullEnumSyncDeviceService.h
 *
 *  Contains definitions for the Full Enumeration Sync Device Service
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The service definition specified in this file has been designed for use
 *  with Microsoft Windows.  To request additions to this file, including new
 *  service properties, object formats, object properties, methods, and events,
 *  please contact Microsoft at askmtp@microsoft.com with your request to
 *  maintain interoperability.
 *
 *  Custom service properties, object formats, object properties, methods,
 *  and events may also be added by generating new format, method, or event
 *  GUIDs or by creating new PKeys in a privately generated and managed
 *  namespace.
 *
 */

#ifndef _FULLENUMSYNCSERVICE_H_
#define _FULLENUMSYNCSERVICE_H_

#include <DeviceServices.h>
#include <SyncDeviceService.h>

/*****************************************************************************/
/*  Full Enumeration Sync Service Info                                       */
/*****************************************************************************/

DEFINE_DEVSVCGUID(SERVICE_FullEnumSync,
    0x28d3aac9, 0xc075, 0x44be, 0x88, 0x81, 0x65, 0xf3, 0x8d, 0x30, 0x59, 0x09);

#define NAME_FullEnumSyncSvc                L"FullEnumSync"
#define TYPE_FullEnumSyncSvc                DEVSVCTYPE_ABSTRACT

/*****************************************************************************/
/*  Full Enumeration Sync Service Properties                                 */
/*****************************************************************************/

DEFINE_DEVSVCGUID(NAMESPACE_FullEnumSyncSvc,
    0x63b10e6c, 0x4f3a, 0x456d, 0x95, 0xcb, 0x98, 0x94, 0xed, 0xec, 0x9f, 0xa5);

/*  PKEY_FullEnumSyncSvc_VersionProps
 *
 *  Provides information about change units and version properties.  The
 *  format for the dataset is
 *
 *      UINT32      Number of change units
 *      UINT128     Namespace GUID for first change unit property key
 *      UINT32      Namespace ID for the first change unit property key
 *      UINT32      Number of properties associated with this change unit
 *      UINT128     Namespace GUID for first property key in change unit
 *      UINT32      Namespace ID for first property key in change unit
 *                  ...  Repeat for number of property keys
 *                  ...  Repeat for number of change units
 *
 *  NOTE: If all change units use the same property key specify a namespace
 *  GUID of GUID_NULL (all 0's) and a namespace ID of 0.
 *
 *  Type: UInt8
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_FullEnumSyncSvc_VersionProps,
    0x63b10e6c, 0x4f3a, 0x456d, 0x95, 0xcb, 0x98, 0x94, 0xed, 0xec, 0x9f, 0xa5,
    3);

#define NAME_FullEnumSyncSvc_VersionProps       L"FullEnumVersionProps"


/*  PKEY_FullEnumSyncSvc_ReplicaID
 *
 *  Contains the GUID representing this replica in the sync community.
 *
 *  Type: UInt128
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_FullEnumSyncSvc_ReplicaID,
    0x63b10e6c, 0x4f3a, 0x456d, 0x95, 0xcb, 0x98, 0x94, 0xed, 0xec, 0x9f, 0xa5,
    4);

#define NAME_FullEnumSyncSvc_ReplicaID          L"FullEnumReplicaID"


/*  PKEY_FullEnumSyncSvc_KnowledgeObjectID
 *
 *  Object ID to be used for the knowledge object
 *
 *  Type: UInt32
 *  Form: ObjectID
 */

DEFINE_DEVSVCPROPKEY(PKEY_FullEnumSyncSvc_KnowledgeObjectID,
    0x63b10e6c, 0x4f3a, 0x456d, 0x95, 0xcb, 0x98, 0x94, 0xed, 0xec, 0x9f, 0xa5,
    7);

#define NAME_FullEnumSyncSvc_KnowledgeObjectID  L"FullEnumKnowledgeObjectID"


/*  PKEY_FullEnumSyncSvc_LastSyncProxyID
 *
 *  Contains a GUID indicating the last sync proxy to perform a sync operation
 *
 *  Type: UInt128
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_FullEnumSyncSvc_LastSyncProxyID,
    0x63b10e6c, 0x4f3a, 0x456d, 0x95, 0xcb, 0x98, 0x94, 0xed, 0xec, 0x9f, 0xa5,
    8);

#define NAME_FullEnumSyncSvc_LastSyncProxyID    L"FullEnumLastSyncProxyID"


/*  PKEY_FullEnumSyncSvc_ProviderVersion
 *
 *  Contains a device defined value giving the version of the provider
 *  currently in use on the device.  This version must be incremented whenever
 *  new properties are added to the device implementation so that they will
 *  be recognized and managed as part of synchronization.  0 is reserved.
 *
 *  Type: UInt16
 *  Form: None
 */

DEFINE_DEVSVCPROPKEY(PKEY_FullEnumSyncSvc_ProviderVersion,
    0x63b10e6c, 0x4f3a, 0x456d, 0x95, 0xcb, 0x98, 0x94, 0xed, 0xec, 0x9f, 0xa5,
    9);

#define NAME_FullEnumSyncSvc_ProviderVersion    L"FullEnumProviderVersion"


/*  PKEY_FullEnumSyncSvc_SyncFormat
 *
 *  Indicates the format GUID for the object format that is to be used in the
 *  sync operation.
 *
 *  Type: UInt128
 *  Form: None
 */

#define PKEY_FullEnumSyncSvc_SyncFormat         PKEY_SyncSvc_SyncFormat
#define NAME_FullEnumSyncSvc_SyncFormat         NAME_SyncSvc_SyncFormat

/*  PKEY_FullEnumSyncSvc_LocalOnlyDelete
 *
 *  Boolean flag indicating whether deletes of objects on the service host
 *  should be treated as "local only" and not propogated to other sync
 *  participants.  The alternative is "true sync" in which deletes on the
 *  service host are propogated to all other sync participants.
 *
 *  Type: UInt8
 *  Form: None
 */

#define PKEY_FullEnumSyncSvc_LocalOnlyDelete    PKEY_SyncSvc_LocalOnlyDelete
#define NAME_FullEnumSyncSvc_LocalOnlyDelete    NAME_SyncSvc_LocalOnlyDelete


/*  PKEY_FullEnumSyncSvc_FilterType
 *
 *  Type: UInt8
 *  Form: None
 */

#define PKEY_FullEnumSyncSvc_FilterType         PKEY_SyncSvc_FilterType
#define NAME_FullEnumSyncSvc_FilterType         NAME_SyncSvc_FilterType


/*****************************************************************************/
/*  Full Enumeration Sync Service Object Formats                             */
/*****************************************************************************/

/*  FORMAT_FullEnumSyncKnowledge
 *
 *  Knowledge object format
 */

DEFINE_DEVSVCGUID(FORMAT_FullEnumSyncKnowledge,
    0x221bce32, 0x221b, 0x4f45, 0xb4, 0x8b, 0x80, 0xde, 0x9a, 0x93, 0xa4, 0x4a);

#define NAME_FullEnumSyncKnowledge          L"FullEnumSyncKnowledge"


/*****************************************************************************/
/*  Full Enumeration Sync Service Methods                                    */
/*****************************************************************************/

/*  Inherited methods
 */

#define METHOD_FullEnumSyncSvc_BeginSync    METHOD_SyncSvc_BeginSync
#define NAME_FullEnumSyncSvc_BeginSync      NAME_SyncSvc_BeginSync

#define METHOD_FullEnumSyncSvc_EndSync      METHOD_SyncSvc_EndSync
#define NAME_FullEnumSyncSvc_EndSync        NAME_SyncSvc_EndSync

#endif  /* _FULLENUMSYNCSERVICE_H_ */

