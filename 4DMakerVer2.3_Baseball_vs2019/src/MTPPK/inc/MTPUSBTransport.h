/*
 *  MTPUSBTransport.h
 *
 *  Contains declarations of the MTP/USB Transport support
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPUSBTRANSPORT_H_
#define _MTPUSBTRANSPORT_H_

#include "MTPUSBDevice.h"

/*
 *  MTPUSBBindInitiator
 *
 *  Binds a connection from an initiator to the MTP/USB Transport.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBBindInitiator(
                            MTPUSBDEVICE mtpusbdInitiator);

/*
 *  MTPUSBBindResponder
 *
 *  Binds the MTP/USB transport to the specifed responder.
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPUSBBindResponder(
                            MTPUSBDEVICE mtpusbdResponder);

#endif  /* _MTPUSBTRANSPORT_H_ */

