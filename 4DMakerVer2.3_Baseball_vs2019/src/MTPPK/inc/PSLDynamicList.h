/*
 *  PSLDynamicList.h
 *
 *  Helper "object" to provide dynamic list functionality.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLDYNAMICLIST_H_
#define _PSLDYNAMICLIST_H_

typedef PSLHANDLE PSLDYNLIST;

#define PSLDYNLIST_MAX_ITEMS        0x80000000

#define PSLDYNLIST_INVALID_INDEX    0xffffffff

#define PSLDYNLIST_GROW_DEFAULT     0xfffffffe
#define PSLDYNLIST_GROW_UNCHANGED   0xffffffff

/*  PSLDynListCreate
 *
 *  Creates a dynamic list
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListCreate(
                            PSLUINT32 cItems,
                            PSLBOOL fReserve,
                            PSLUINT32 dwGrowBy,
                            PSLDYNLIST* pdl);

/*  PSLDynListDestroy
 *
 *  Destroys a dynamic list
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListDestroy(
                            PSLDYNLIST dlDestroy);


/*  PSLDynListGetSize
 *
 *  Returns the current size of the list
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListGetSize(
                            PSLDYNLIST dl,
                            PSLUINT32* pcItems);

/*  PSLDynListSetSize
 *
 *  Sets the size of the list and optionally modifies the the grow by
 *  size.
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListSetSize(
                            PSLDYNLIST dl,
                            PSLUINT32 cItems,
                            PSLUINT32 dwGrowBy);


/*  PSLDynListReserveSize
 *
 *  Reserves size for the specified number of items in the list but does
 *  not include those items in the current size of the list.  Reserve size
 *  may be specified in terms of the total size of the list (fTotal = PSLTRUE)
 *  or as the sum of the current count of items in the list and the reserve
 *  size (fTotal = PSLFALSE)
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListReserveSize(
                            PSLDYNLIST dl,
                            PSLUINT32 cReserve,
                            PSLBOOL fTotal);


/*  PSLDynListGetItem
 *
 *  Retrieves an item from the list
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListGetItem(
                    PSLDYNLIST dl,
                    PSLUINT32 idxItem,
                    PSLPARAM* paItem);


/*  PSLDynListAddItem
 *
 *  Adds the specified item at the end of the list
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListAddItem(
                    PSLDYNLIST dl,
                    PSLPARAM aItem,
                    PSLUINT32* idxItem);


/*  PSLDynListSetItem
 *
 *  Sets the specified item
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListSetItem(
                    PSLDYNLIST dl,
                    PSLUINT32 idxItem,
                    PSLPARAM aItem);


/*  PSLDynListInsertItem
 *
 *  Inserts an item into the list
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListInsertItem(
                    PSLDYNLIST dl,
                    PSLUINT32 idxItem,
                    PSLPARAM aItem);


/*  PSLDynListRemoveItem
 *
 *  Removes items from the list
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListRemoveItem(
                    PSLDYNLIST dl,
                    PSLUINT32 idxItem,
                    PSLUINT32 cItems);


/*  PSL Dynamic List Comparison Callback
 *
 *  Callback for handling custom comparison of PSL Dynamic List objects.
 *  Passing PSLNULL for the function will cause the default comparison based
 *  to be used.
 *
 *  The return of the function is:
 *          < 0 : aKey < *paItem
 *          = 0 : aKey == *paItem
 *          > 0 : aKey > *paItem
 */

typedef PSLINT32 (PSL_API *PFNDYNLISTCOMPARE)(
                    PSLPARAM aKey,
                    PSL_CONST PSLPARAM* paItem);


/*  PSLDynListFindItem
 *
 *  Performs a linear search on the list for the specified item
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListFindItem(
                    PSLDYNLIST dl,
                    PSLUINT32 idxStart,
                    PSLPARAM aKey,
                    PFNDYNLISTCOMPARE pfnCompare,
                    PSLUINT32* pidxItem);


/*  PSLDynListBSearch
 *
 *  Performas a binary search on the items in the list
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLDynListBSearch(
                    PSLDYNLIST dl,
                    PSLPARAM aKey,
                    PFNDYNLISTCOMPARE pfnCompare,
                    PSLUINT32* pidxItem);


/*  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 */

#define SAFE_PSLDYNLISTDESTROY(_dl) \
if (PSLNULL != (_dl)) \
{ \
    PSLDynListDestroy(_dl); \
    (_dl) = PSLNULL; \
}

#endif  /* _PSLDYNAMICLIST_H_ */
