/*
 *  MTPIdentity.h
 *
 *  Provides routines for determining the identity of an MTP device.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPIDENTITY_H_
#define _MTPIDENTITY_H_

enum
{
    MTPIDENTITY_MANUFACTURER_NAME = 0,
    MTPIDENTITY_MANUFACTURER_URL,
    MTPIDENTITY_PRODUCT_NAME,
    MTPIDENTITY_PRODUCT_VERSION,
    MTPIDENTITY_PRODUCT_DESCRIPTION,
    MTPIDENTITY_FRIENDLY_NAME,
    MTPIDENTITY_SERIAL_NUMBER,
    MTPIDENTITY_DEVICE_CONTAINER_ID
};


/*  MTPIdentityGetValue
 *
 *  Returns the requested Identity value
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPIdentityGetValue(
                            PSLUINT32 idxValue,
                            PSLUINT16 wDataType,
                            PSLVOID* pvValue,
                            PSLUINT32 cbValue,
                            PSLUINT32* pcbUsed);

#endif  /* _MTPIDENTITY_H_ */

