/*
 *  MTPDeviceProperty.h
 *
 *  Contains declaration for functions that support
 *  MTP device property operations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPDEVICEPROPERTY_H_
#define _MTPDEVICEPROPERTY_H_

typedef PSLHANDLE MTPDEVICEPROPCONTEXT;

/*  Device Property Initialization
 *
 *  This function will perform device property related
 *  initializations.
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPDevicePropInitialize(
                    PSLLOOKUPTABLE ltDevProps,
                    PCXPSLUINT16 pwCodes,
                    PSLUINT32  cCodes);

PSL_EXTERN_C PSLSTATUS PSL_API MTPDevicePropUninititialize(PSLVOID);


/*  MTP Device Property Context
 *
 *  The Device Property Context is used to provide access to serialization/
 *  deserialization functionality for device properties.  Currently the
 *  Device Property Context matches the behavior of the standard DB
 *  Context Data
 */

/*  PFNMTPDEVICEPROPCONTEXTOPEN
 *
 *  The open context function is registered in the device property lookup
 *  table during the call to MTPDevicePropInitialize for each device property
 *  supported.
 */

typedef PSLSTATUS (PSL_API* PFNMTPDEVICEPROPCONTEXTOPEN)(
                    PSLUINT32 dwPropCode,
                    PSLUINT32 dwDataFormat,
                    PSLPARAM aData,
                    MTPDEVICEPROPCONTEXT* pmdpc);


/*  pfnMTPDevicePropContextReset - PURE
 *
 *  Resets the changes currently pending in the context
 */

typedef PSLSTATUS (PSL_API* PFNMTPDEVICEPROPCONTEXTCOMMIT)(
                    MTPDEVICEPROPCONTEXT mdpc);


/*  pfnMTPDevicePropContextReset
 *
 *  Resets the device property to it's default state
 */

typedef PSLSTATUS (PSL_API* PFNMTPDEVICEPROPCONTEXTRESET)(
                    MTPDEVICEPROPCONTEXT mdpc);


/*  MTP Device Property Context Virtual Function Table
 *
 *  Contains the VTable details for the MTP Device Property Context.  Note
 *  that the context inherits from the standard MTPDBContextData for the
 *  core elements.
 */

typedef struct _MTPDEVICEPROPCONTEXTVTBL
{
    MTPDBCONTEXTDATAVTBL            vtblCD;
    PFNMTPDEVICEPROPCONTEXTCOMMIT   pfnMTPDevicePropContextCommit;
    PFNMTPDEVICEPROPCONTEXTRESET    pfnMTPDevicePropContextReset;
} MTPDEVICEPROPCONTEXTVTBL;


/*  MTP Device Property Context Object
 *
 *  Core object for the MTP Device Property Context
 */

typedef struct _MTPDEVICEPROPCONTEXTOBJ
{
    PSL_CONST MTPDEVICEPROPCONTEXTVTBL*     vtbl;
} MTPDEVICEPROPCONTEXTOBJ;

typedef PSL_CONST MTPDEVICEPROPCONTEXTOBJ* PMTPDEVICEPROPCONTEXTOBJ;

/*  MTP Device Property Context Helper Macros
 */

#define MTPDevicePropContextClose(_po) \
    MTPDBContextDataClose(_po)

#define MTPDevicePropContextCancel(_po) \
    MTPDBContextDataCancel(_po)

#define MTPDevicePropContextSerialize(_po, _pv, _cb, _pcbW, _pcbL) \
    MTPDBContextDataSerialize(_po, _pv, _cb, _pcbW, _pcbL)

#define MTPDevicePropContextStartDeserialize(_po, _cb) \
    MTPDBContextDataStartDeserialize(_po, _cb)

#define MTPDevicePropContextDeserialize(_po, _pv, _cb, _mq, _pctx, _aP, _dwM) \
    MTPDBContextDataDeserialize(_po, _pv, _cb, _mq, _pctx, _aP, _dwM)


#define GET_MTPDEVICEPROPCONTEXTVTBL(_po) \
                    (((PMTPDEVICEPROPCONTEXTOBJ)(_po))->vtbl)

#define MTPDevicePropContextCommit(_po) \
    GET_MTPDEVICEPROPCONTEXTVTBL(_po)->pfnMTPDevicePropContextCommit(_po)

#define MTPDevicePropContextReset(_po) \
    GET_MTPDEVICEPROPCONTEXTVTBL(_po)->pfnMTPDevicePropContextReset(_po)


/*  Safe Helpers
 */

#define SAFE_MTPDEVICEPROPCONTEXTCLOSE(_mdpc) SAFE_MTPDBCONTEXTDATACLOSE(_mdpc)

#endif /*_MTPDEVICEPROPERTY_H_*/
