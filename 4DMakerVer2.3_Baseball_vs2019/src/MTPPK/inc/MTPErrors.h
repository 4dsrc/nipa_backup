/*
 *  MTPErrors.h
 *
 *  Contains errors specific to the MTP subsystem
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPERRORS_H_
#define _MTPERRORS_H_

#define MAKE_MTPERROR_CODE(_ec)     MAKE_PSLERROR_CODE(PSL_USER_STATUS + _ec)
#define MAKE_MTPSUCCESS_CODE(_sc)   MAKE_PSLSUCCESS_CODE(PSL_USER_STATUS + _sc)

/*  MTP User Status
 *
 *  Defines the beginning range for error codes that belong to the MTP
 *  platform user
 */

#define MTP_USER_STATUS             (PSL_USER_STATUS + 32768)

/*  MTP Errors
 */

enum
{
    /*  Database Errors (PSL_USER_STATUS - (PSL_USER_STATUS + 128))
     */

    MTPERROR_DATABASE_INVALID_HANDLE            =  MAKE_MTPERROR_CODE(0),
    MTPERROR_DATABASE_INVALID_STORAGEID         =  MAKE_MTPERROR_CODE(1),
    MTPERROR_DATABASE_INVALID_PARENT            =  MAKE_MTPERROR_CODE(2),
    MTPERROR_DATABASE_INVALID_FORMATCODE        =  MAKE_MTPERROR_CODE(3),
    MTPERROR_DATABASE_INVALID_OBJECTPROPCODE    =  MAKE_MTPERROR_CODE(4),
    MTPERROR_DATABASE_INVALID_OBJECTGROUPCODE   =  MAKE_MTPERROR_CODE(5),
    MTPERROR_DATABASE_SPEC_BY_GROUP_UNSUPPORTED =  MAKE_MTPERROR_CODE(6),
    MTPERROR_DATABASE_SPEC_BY_DEPTH_UNSUPPORTED =  MAKE_MTPERROR_CODE(7),
    MTPERROR_DATABASE_WRITEPROTECTED_STORE      =  MAKE_MTPERROR_CODE(8),
    MTPERROR_DATABASE_WRITEPROTECTED_OBJECT     =  MAKE_MTPERROR_CODE(9),
    MTPERROR_DATABASE_PARTIAL_DELETION          =  MAKE_MTPERROR_CODE(10),
    MTPERROR_DATABASE_STORAGEFULL               =  MAKE_MTPERROR_CODE(11),
    MTPERROR_DATABASE_INVALIDOBJECTPROPVALUE    =  MAKE_MTPERROR_CODE(12),

    /*  Property Errors (PSL_USER_STATUS + 129 - (PSL_USER_STATUS + 256))
     */

    MTPERROR_PROPERTY_READONLY                  = MAKE_MTPERROR_CODE(129),
    MTPERROR_PROPERTY_FORMATCODE_UNSUPPORTED    = MAKE_MTPERROR_CODE(130),
    MTPERROR_PROPERTY_INVALID_OBJECTPROPCODE    = MAKE_MTPERROR_CODE(131),
    MTPERROR_PROPERTY_INVALID_DEVICEPROPCODE    = MAKE_MTPERROR_CODE(132),
    MTPERROR_PROPERTY_INVALID_PROPVALUE         = MAKE_MTPERROR_CODE(133),

    /*  Service Errors (PSL_USER_STATUS + 257 - (PSL_USER_STATUS + 384))
     */

    MTPERROR_SERVICE_INVALID_SERVICEID          = MAKE_MTPERROR_CODE(257),
    MTPERROR_SERVICE_INVALID_PROPCODE           = MAKE_MTPERROR_CODE(258),

};

#endif  /* _MTPERRORS_H_ */

