/*
 *  MTPProperty.h
 *
 *  Contains declaration for the fucntions that support
 *  property related MTP operations.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPPROPERTY_H_
#define _MTPPROPERTY_H_

#define MTP_LONGSTRING_DEFAULT          0x4000

/*  Generic Property Support
 *
 *  MTP at its core is an object property system.  Some of those properties
 *  are defined as part of the core MTP standard while others are specified
 *  as part of extensions or service definitions.  The core property system
 *  is built around the MTPPROPERTYREC structure which defines certain native
 *  behaviors for each property.
 */

#define PROPFLAGS_TYPE_DEVICE           0x0001
#define PROPFLAGS_TYPE_OBJECT           0x0002
#define PROPFLAGS_TYPE_SERVICE          0x0004
#define PROPFLAGS_TYPE_SERVICEOBJECT    0x0008
#define PROPFLAGS_TYPE_METHODOBJECT     0x0010

#define PROPFLAGS_TYPE_MASK             0x00ff

#define GET_PROPTYPE(_wF)              ((_wF) & PROPFLAGS_TYPE_MASK)

/*  Form Details
 *
 *  Each property record contains information about the default form for
 *  the property.  The data in the pvForm field is specific to the type of
 *  form specified:
 *      MTP_FORMFLAG_RANGE          pvForm points to an array of 3 values of
 *                                  the appropriate size for the datatype.
 *                                  The first value is the minimum, second is
 *                                  the maximum, and third is the step size.
 *                                  The default value is the minimum value.
 *      MTP_FORMFLAG_ENUM           pvForm points to a structure of type
 *                                  MTPENUMFORM.  It includes a UINT16 count
 *                                  of the number of values in the enumeration
 *                                  followed by a PSLVOID* to an array of
 *                                  the values in the enumeration of the
 *                                  appropriate data size.  The defaukt value
 *                                  is the first value in the enumeration.
 *      MTP_FORMFLAG_FIXEDARRAY     The value of pvForm is cast to a UINT32
 *                                  and returned as the maximum length of
 *                                  the fixed length array.
 *      MTP_FORMFLAG_REGEXP         pvForm points to a PSLCWSTR containing
 *                                  the regular expression.
 *      MTP_FORMFLAG_BYTEARRAY      The value of pvForm is cast to a UINT32 and
 *                                  returned as the maximum length of the byte
 *                                  array.
 *      MTP_FORMFLAG_LONGSTRING     The value of pvForm is cast to a UINT32 and
 *                                  returned as the maximun length of the
 *                                  string.
 */

typedef struct _MTPENUMFORM
{
    PSLUINT16                   cValues;
    PSL_CONST PSLVOID*          rgValues;
} MTPENUMFORM;

typedef struct _MTPPROPERTYREC
{
    PSLUINT16                   wFlags;
    PSLUINT16                   wDataType;
    PSLBYTE                     bForm;
    PSL_CONST PSLVOID*          pvForm;
} MTPPROPERTYREC;


typedef PSLSTATUS (PSL_API* PFNMTPPROPGETVALUE)(
                    PSLUINT16 wPropCode,
                    PSLUINT16 wPropType,
                    PSLPARAM aContext,
                    PSLPARAM aContextProp,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

typedef PSLSTATUS (PSL_API* PFNMTPPROPSETVALUE)(
                    PSLUINT16 wPropCode,
                    PSLUINT16 wPropType,
                    PSLPARAM aContext,
                    PSLPARAM aContextProp,
                    PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

typedef PSLSTATUS (PSL_API* PFNMTPPROPDELETEVALUE)(
                    PSLUINT16 wPropCode,
                    PSLPARAM aContext,
                    PSLPARAM aContextProp);


typedef struct _MTPPROPERTYINFO
{
    PSL_CONST MTPPROPERTYREC*   precProp;
    PSLUINT16                   wPropCode;
    PSLUINT8                    bGetSet;
    PFNMTPPROPGETVALUE          pfnMTPPropGetValue;
    PFNMTPPROPGETVALUE          pfnMTPPropGetDefaultValue;
    PFNMTPPROPSETVALUE          pfnMTPPropSetValue;
    PFNMTPPROPDELETEVALUE       pfnMTPPropDeleteValue;
    PSLPARAM                    aContextProp;
} MTPPROPERTYINFO;


/*  MTP Device Property Info
 *
 *  Defines information about the supported device properties.
 */

typedef PSLSTATUS (PSL_API* PFNDEVICEPROPRESETVALUE)(
                    PSLUINT16 wPropCode);

typedef PSLSTATUS (PSL_API* PFNDEVICEPROPCOMMIT)(
                    PSLUINT16 wPropCode);

typedef struct _MTPDEVICEPROPINFO
{
    MTPPROPERTYINFO                 infoProp;
    PFNDEVICEPROPRESETVALUE         pfnResetValue;
    PFNDEVICEPROPCOMMIT             pfnCommit;
} MTPDEVICEPROPINFO;


/*  MTP Object Property Info
 *
 *  Defines the object and service object property details
 */

typedef struct _MTPOBJECTPROPINFO
{
    MTPPROPERTYINFO             infoProp;
    PSLUINT32                   dwGroupCode;
} MTPOBJECTPROPINFO;


/*  MTP Property System
 *
 *  The MTP property system is designed to provide access to information about
 *  properties documented in the MTP specification.
 *
 *  Common Helper Functions
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPPropertyGetDefaultRec(
                    PSLUINT16 wPropCode,
                    PSLUINT16 wPropFlags,
                    PSL_CONST MTPPROPERTYREC** pprecProp);

PSL_EXTERN_C PSLSTATUS PSL_API MTPPropertyGetMinSizeFromType(
                    PSLUINT16 wDataType,
                    PSLUINT32* pbSize);

PSL_EXTERN_C PSLSTATUS PSL_API MTPPropertyReadSize(
                    PSLUINT16 wDataType,
                    PSL_CONST PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbSize);

PSL_EXTERN_C PSLSTATUS PSL_API MTPPropertySerializeForm(
                    PSL_CONST MTPPROPERTYREC* precProp,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

PSL_EXTERN_C PSLSTATUS PSL_API MTPPropertySerializeDefaultValue(
                    PSL_CONST MTPPROPERTYREC* precProp,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbUsed);

/*  Property Description Serialization
 *
 *  Common Helper Functions
 */

PSL_EXTERN_C PSLSTATUS PSL_API MTPPropertyDescSerializeCreate(
                    PSL_CONST MTPPROPERTYINFO* pinfoProp,
                    PSLUINT16 wPropFlags,
                    PSLPARAM aContext,
                    MTPSERIALIZE* pmso);

PSL_EXTERN_C PSLSTATUS PSL_API MTPPropertyDescSerialize(
                    PSL_CONST MTPPROPERTYINFO* pinfoProp,
                    PSLUINT16 wPropFlags,
                    PSLPARAM aContext,
                    PSLVOID* pvBuf,
                    PSLUINT32 cbBuf,
                    PSLUINT32* pcbBuf);

#endif  /* _MTPPROPERTY_H_ */

