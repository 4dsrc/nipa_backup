/*
 *  MTPPerfLogger.h
 *
 *  Header file for MTP Performance Logger. defines compiler
 *  switch to turn on Perf Logger functionality, defines performance
 *  event code, declares function prototype, etc.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _MTPPERFLOGGER_H_
#define _MTPPERFLOGGER_H_

/* Define the MACRO to turn on perf logger functionality
 */
#if 1
#define MTP_PERFORFANCE_TRACE
#endif

#define MTPPERF_MAX_PAYLOAD_LENGTH          512

#define MPF_ROUTER                      0x80
#define MPF_TRANSPORT                   0x40
#define MPF_HANDLER                     0x20
#define MPF_DEBUG                       0x10

#define MAKE_MTPPERF_EVENTCODE(_layer, _code) \
    (  (PSLUINT16)(((0xFf & (_layer)) << 8 )| \
        (0xFf  & (_code))))

typedef struct _MTPPERF_LOG_FILTER
{
    PSLUINT8    chFlag;
    PSLUINT8    chLevel;
} MTPPERF_LOG_FILTER;

#define MPF_FILTER_ALL      (MPF_ROUTER | MPF_TRANSPORT |MPF_HANDLER |MPF_DEBUG)

#define MFL_FILTER_ADMIN                1
#define MFL_FILTER_OPERATION            2
#define MFL_FILTER_ANALYSIS             3
#define MFL_FILTER_DEBUG                4

/* MTP Perf event code definition
  */
typedef enum
{
    /* transport router layer code
      */
    MTPPERF_OPERATION  = MAKE_MTPPERF_EVENTCODE(MPF_ROUTER, 1),
    MTPPERF_COMMAND    = MAKE_MTPPERF_EVENTCODE(MPF_ROUTER, 2) ,
    MTPPERF_DATA       = MAKE_MTPPERF_EVENTCODE(MPF_ROUTER, 3),
    MTPPERF_RESPONSE   = MAKE_MTPPERF_EVENTCODE(MPF_ROUTER, 4),

    /* transprot driver layer code
      */
    MTPPERF_TRANSPORT  = MAKE_MTPPERF_EVENTCODE(MPF_TRANSPORT, 1),

    /* handler layer code
      */
    MTPPERF_ACCESSOBJECT  = MAKE_MTPPERF_EVENTCODE(MPF_HANDLER, 1),

    /* debug layer
     */
   MTPPERF_STRINGMSG      = MAKE_MTPPERF_EVENTCODE(MPF_DEBUG, 1),
} MTPPERFEVENTCODE;

#define MP_TYPE_UNDEFINED               0x00

#define MP_TYPE_ELAPSETIME_BEGIN        0x01
#define MP_TYPE_ELAPSETIME_END          0x02

#define MP_TYPE_RECEIVEUSB_BEGIN        0x03
#define MP_TYPE_RECEIVEUSB_END          0x04
#define MP_TYPE_RECEIVEUSB_CANCEL       0x05

#define MP_TYPE_SENDUSB_BEGIN           0x06
#define MP_TYPE_SENDUSB_END             0x07
#define MP_TYPE_SENDUSB_CANCEL          0x08

#define MP_TYPE_READMTPOBJECT_BEGIN     0x09
#define MP_TYPE_READMTPOBJECT_END       0x0a

#define MP_TYPE_WRITEMTPOBJECT_BEGIN    0x0b
#define MP_TYPE_WRITEMTPOBJECT_END      0x0c

#define MP_TYPE_SENDDATA_BEGIN          0x0d
#define MP_TYPE_SENDDATA_END            0x0e

#define MP_TYPE_RECEIVEDATA_BEGIN       0x0f
#define MP_TYPE_RECEIVEDATA_END         0x10

#ifdef MTP_PERFORFANCE_TRACE

PSL_EXTERN_C PSLVOID PSL_API MTPPerfInitLogger();
PSL_EXTERN_C PSLVOID PSL_API MTPPerfDeinitLogger();
PSL_EXTERN_C PSLVOID PSL_API MTPPerfStartLog();
PSL_EXTERN_C PSLVOID PSL_API MTPPerfStopLog();
PSL_EXTERN_C PSLVOID PSL_API MTPPerfSetLogObject(PSLCSTR LogFile);
PSL_EXTERN_C PSLVOID PSL_API MTPPerfWriteLogRecord(
                             MTPPERFEVENTCODE EventCode,
                             PSLUINT8 EventType,
                             PSLVOID *pPayload,
                             PSLUINT16 PayloadLen);
PSL_EXTERN_C PSLVOID PSL_API MTPPerfSetMTPContext(PSLUINT32 dwTransactionID,
                                                  PSLUINT16 wOpcode);
PSL_EXTERN_C PSLVOID PSL_API MTPPerfSetLogFilter(PSLUINT8 FilterFlag,
                                                 PSLUINT8 FilterLevel);
PSL_EXTERN_C PSLSTATUS PSL_API MTPPerfIsLogEnabled(PSLUINT8 FilterFlag,
                                                   PSLUINT8 FilterLevel);

#define MP_TRACE_ENABLE(Flags, Level) (PSLSUCCESS == \
            MTPPerfIsLogEnabled((PSLUINT8)Flags, (PSLUINT8)Level))

#define MTPPERFLOG_TRANSPORT( \
    _eventtype, \
    _endpoint,  \
    _transfersize) \
{  \
    if (MP_TRACE_ENABLE(MPF_TRANSPORT, MFL_FILTER_OPERATION)) \
    { \
        PSLUINT8 TracePayload[5]; \
        \
        PSLStoreAlignUInt32((PSLUINT32 *)&TracePayload[0], \
                    MTPSwapUInt32(_transfersize)); \
        TracePayload[4] = (PSLUINT8)_endpoint; \
        \
        MTPPerfWriteLogRecord(MTPPERF_TRANSPORT, (PSLUINT8)_eventtype, \
                              TracePayload, sizeof(TracePayload)); \
     } \
}

#ifdef PSL_LITTLE_ENDIAN

#define MTPPerfSwapUInt64(_pval)

#elif defined(PSL_BIG_ENDIAN)

#define MTPPerfSwapUInt64(_pval) \
{ \
    PSLUINT8 *pbBegin = (PSLUINT8 *)_pval; \
    PSLUINT8 *pbEnd = pbBegin + 8; \
    PSLUINT8 bTemp; \
    for (; pbBegin < pbEnd; pbBegin++, pbEnd--) \
    { \
        bTemp = *pbBegin; \
        *pbBegin = *pbEnd; \
        *pbEnd = bTemp; \
    } \
}

#endif

#define MTPPERFLOG_DATAPHASE( \
    _eventtype, \
    _datasize) \
{  \
    if (MP_TRACE_ENABLE(MPF_ROUTER, MFL_FILTER_OPERATION)) \
    { \
        PSLUINT64 TracePayload; \
        PSLStoreAlignUInt64(&TracePayload,(PSLUINT64)_datasize); \
        MTPPerfSwapUInt64(&TracePayload); \
        MTPPerfWriteLogRecord(MTPPERF_DATA, (PSLUINT8)_eventtype, \
                              &TracePayload, sizeof(TracePayload)); \
    } \
}

#define MTPPERFLOG_COMMANDPHASE(_eventtype) \
{  \
    if (MP_TRACE_ENABLE(MPF_ROUTER, MFL_FILTER_OPERATION)) \
    { \
        MTPPerfWriteLogRecord(MTPPERF_COMMAND, (PSLUINT8)_eventtype, \
                              PSLNULL, 0); \
    } \
}

#define MTPPERFLOG_RESPONSEPHASE( \
    _eventtype, \
    _responsecode) \
{  \
    if (MP_TRACE_ENABLE(MPF_ROUTER, MFL_FILTER_OPERATION)) \
    { \
        PSLUINT16 TracePaylaod; \
        TracePaylaod = MTPSwapUInt16((PSLUINT16)_responsecode); \
        MTPPerfWriteLogRecord(MTPPERF_RESPONSE, (PSLUINT8)_eventtype, \
                              &TracePaylaod, sizeof(TracePaylaod)); \
    } \
}

#define MTPPERFLOG_OPERATION( \
    _eventtype, \
    _pmtpopparam, \
    _cmtpopparam) \
{  \
    if (MP_TRACE_ENABLE(MPF_ROUTER, MFL_FILTER_ADMIN)) \
    { \
        PSLUINT32 TracePayload[5] = {0}; \
        PSLUINT32 MTPOpPaaramLen = 0; \
        \
        if (_cmtpopparam != MTPOpPaaramLen) \
        { \
            PSLCopyMemory(TracePayload, \
                          (const PSLVOID*)_pmtpopparam, \
                          (PSLUINT32)_cmtpopparam); \
        } \
        \
        MTPPerfWriteLogRecord(MTPPERF_OPERATION, (PSLUINT8)_eventtype, \
                              TracePayload, sizeof(TracePayload)); \
    }\
}

#define MTPPERFLOG_ACCESSOBJECT( \
    _eventtype, \
    _datasize, \
    _status) \
{ \
    if (MP_TRACE_ENABLE(MPF_HANDLER, MFL_FILTER_OPERATION)) \
    { \
        PSLUINT32 TracePayload[2]; \
        \
        TracePayload[0] = MTPSwapUInt32(_datasize); \
        TracePayload[1] = MTPSwapUInt32(_status); \
        \
        MTPPerfWriteLogRecord(MTPPERF_ACCESSOBJECT, (PSLUINT8)_eventtype, \
                              TracePayload, sizeof(TracePayload)); \
    } \
}

#define MTPPERFLOG_STRINGMSG(_msg) \
{ \
    if (MP_TRACE_ENABLE(MPF_ROUTER, MFL_FILTER_DEBUG)) \
    { \
        PSLUINT32   StrLen; \
        PSLStringLenA((PSLCSTR)_msg, MTPPERF_MAX_PAYLOAD_LENGTH, &StrLen); \
        \
        if (0 < StrLen) \
        { \
            MTPPerfWriteLogRecord(MTPPERF_STRINGMSG, 0, (PSLVOID*)_msg, \
                                 (PSLUINT16)(StrLen + 1)); \
        } \
    } \
}

#else //MTP_PERFORFANCE_TRACE

#define MTPPerfInitLogger()
#define MTPPerfDeinitLogger()
#define MTPPerfStartLog()
#define MTPPerfStopLog()
#define MTPPerfSetLogObject(_lo)
#define MTPPerfSetMTPContext(_tid, _op)
#define MTPPerfSetLogFilter(Flags, Level)

#define MTPPERFLOG_TRANSPORT(_eventtype, _endpoint, _transfersize)

#define MTPPERFLOG_DATAPHASE(_eventtype, _datasize)

#define MTPPERFLOG_COMMANDPHASE(_eventtype)

#define MTPPERFLOG_RESPONSEPHASE(_eventtype, _responsecode)

#define MTPPERFLOG_OPERATION(_eventtype, _dwParams1, _dwParams2, \
                             _dwParams3, _dwParams4, _dwParams5)

#define MTPPERFLOG_ACCESSOBJECT(_eventtype, _datasize, _status)

#define MTPPERFLOG_STRINGMSG(_msg)

#endif //MTP_PERFORFANCE_TRACE

#endif //_MTPPERFLOGGER_H_
