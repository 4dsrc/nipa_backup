/*
 *  PSLNotify.h
 *
 *  PSL Notification engine for broadcasting notifications to interested
 *  receivers.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLNOTIFY_H_
#define _PSLNOTIFY_H_

typedef PSLHANDLE PSLNOTIFY;

/*  PFNNOTIFYENUM
 *
 *  Callback function that will be called prior to posting or sending the
 *  message.  Returning PSLSUCCESS_FALSE will skip posting the message, an
 *  error will halt any additional messages from being sent.
 */

typedef PSLSTATUS (PSL_API* PFNNOTIFYENUM)(
                    PSLMSG* pMsg);

/*  PSLNotifyCreate
 *
 *  Create a notifier
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLNotifyCreate(
                    PSLNOTIFY* pnotify);

/*  PSLNotifyDestroy
 *
 *  Closes a notifier
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLNotifyDestroy(
                    PSLNOTIFY notify);

/*  PSLNotifyRegister
 *
 *  Registers a message queue to receive broadcast notifications
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLNotifyRegister(
                    PSLNOTIFY notify,
                    PSLMSGQUEUE mqDest);

/*  PSLNotifyUnregister
 *
 *  Removes the specified queue from the broadcast notification list
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLNotifyUnregister(
                    PSLNOTIFY notify,
                    PSLMSGQUEUE mqDest);

/*  PSLNotifyMsgPost
 *
 *  Posts a message to all of the queues represented in the context
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLNotifyMsgPost(
                    PSLNOTIFY notify,
                    PFNNOTIFYENUM pfnNotifyEnum,
                    PSLMSGID msgID,
                    PSLPARAM aParam0,
                    PSLPARAM aParam1,
                    PSLPARAM aParam2,
                    PSLPARAM aParam3,
                    PSLLPARAM alParam);

/*  PSLNotifyMsgSend
 *
 *  Sends a message to all of the queues represented in the context
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLNotifyMsgSend(
                    PSLNOTIFY notify,
                    PFNNOTIFYENUM pfnNotifyEnum,
                    PSLMSGID msgID,
                    PSLPARAM aParam0,
                    PSLPARAM aParam1,
                    PSLPARAM aParam2,
                    PSLPARAM aParam3,
                    PSLLPARAM alParam,
                    PSLUINT32 dwTimeOut);


/*
 *  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#define SAFE_PSLNOTIFYDESTROY(_n) \
if (PSLNULL != (_n)) \
{ \
    PSLNotifyDestroy(_n); \
    (_n) = PSLNULL; \
}

#endif  /* _PSLNOTIFY_H_ */

