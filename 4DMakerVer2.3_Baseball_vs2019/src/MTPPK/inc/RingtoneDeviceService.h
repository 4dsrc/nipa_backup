/*
 *  RingtoneDeviceService.h
 *
 *  Contains declarations for the Ringtone Device Service
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The service definition specified in this file has been designed for use
 *  with Microsoft Windows.  To request additions to this file, including new
 *  service properties, object formats, object properties, methods, and events,
 *  please contact Microsoft at askmtp@microsoft.com with your request to
 *  maintain interoperability.
 *
 *  Custom service properties, object formats, object properties, methods,
 *  and events may also be added by generating new format, method, or event
 *  GUIDs or by creating new PKeys in a privately generated and managed
 *  namespace.
 *
 */

#ifndef _RINGTONEDEVICESERVICE_H_
#define _RINGTONEDEVICESERVICE_H_

#include <DeviceServices.h>
#include <MessageDeviceService.h>


/*****************************************************************************/
/*  Ringtone Service Info                                                    */
/*****************************************************************************/

DEFINE_DEVSVCGUID(SERVICE_Ringtones,
    0xd0eace0e, 0x707d, 0x4106, 0x8d, 0x38, 0x4f, 0x56, 0xe, 0x6a, 0x9f, 0x8e);

#define NAME_RingtonesSvc                   L"Ringtones"
#define TYPE_RingtonesSvc                   DEVSVCTYPE_DEFAULT


/*****************************************************************************/
/*  Ringtone Service Properties                                              */
/*****************************************************************************/

DEFINE_DEVSVCGUID(NAMESPACE_RingtonesSvc,
    0x7d05d925, 0x32e6, 0x4790, 0x92, 0x05, 0x54, 0x76, 0x4b, 0xb3, 0xcb, 0x74);

/*  PKEY_RingtonesSvc_DefaultRingtone
 *
 *  Indicates the objectID of the default ringtone for incoming calls
 *
 *  Type: UInt32
 *  Form: ObjectID
 */

DEFINE_DEVSVCPROPKEY(PKEY_RingtonesSvc_DefaultRingtone,
    0x7d05d925, 0x32e6, 0x4790, 0x92, 0x05, 0x54, 0x76, 0x4b, 0xb3, 0xcb, 0x74,
    2);

#define NAME_RingtonesSvc_DefaultRingtone   L"DefaultRingtone"


/*****************************************************************************/
/*  Ringtone Service Object Properties                                       */
/*****************************************************************************/

DEFINE_DEVSVCGUID(NAMESPACE_RingtonesObj,
    0x8d943c78, 0x2c7d, 0x4c74, 0x94, 0x5a, 0x42, 0xd8, 0x3c, 0xb5, 0x8b, 0x5a);

#endif  /* _RINGTONEDEVICESERVICE_H_ */


