/*
 *  PSLBucketHash.h
 *
 *  Helper utility providing basic bucket hash functionality.
 *
 *  Copyright (c) 2009 Microsoft Corporation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the Microsoft Corporation nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY MICROSOFT CORPORATION ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL MICROSOFT CORPORATION BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _PSLBKTHASH_H_
#define _PSLBKTHASH_H_

typedef PSLHANDLE PSLBKTHASH;

typedef PSLUINT32 (PSL_API* PFNPSLBKTHASHFN)(
                            PSLPARAM aKey,
                            PSLUINT32 cTableEntries,
                            PSLPARAM aHashParam);


/*
 *  PSLBktHashCreate
 *
 *  Creates a bucket hash object
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLBktHashCreate(
                            PSLUINT32 cTableEntries,
                            PFNPSLBKTHASHFN pfnHash,
                            PSLUINT32 aHashParam,
                            PSLBKTHASH* pbh);

/*
 *  PSLBktHashDestroy
 *
 *  Destroys the bucket hash object
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLBktHashDestroy(
                            PSLBKTHASH bh);


/*
 *  PSLBktHashGetSize
 *
 *  Returns the number of items in the hash
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLBktHashGetSize(
                            PSLBKTHASH bh,
                            PSLUINT32* pcEntries);

/*
 *  PSLBktHashInsert
 *
 *  Inserts an entry into the hash object
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLBktHashInsert(
                            PSLBKTHASH bh,
                            PSLPARAM aKey,
                            PSLPARAM aValue);


/*
 *  PSLBktHashFind
 *
 *  Locates a value in the hash
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLBktHashFind(
                            PSLBKTHASH bh,
                            PSLPARAM aKey,
                            PSLPARAM* paValue);


/*
 *  PSLBktHashRemove
 *
 *  Removes an entry from the hash
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLBktHashRemove(
                            PSLBKTHASH bh,
                            PSLPARAM aKey,
                            PSLPARAM* paValue);

/*
 *  PSLBktHashGetFirstEntry
 *
 *  Returns the first entry in the hash
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLBktHashGetFirstEntry(
                            PSLBKTHASH bh,
                            PSLPARAM* paKey,
                            PSLPARAM* paValue);

/*
 *  PSLBktHashGetNextEntry
 *
 *  Returns the next entry in the hash
 *
 */

PSL_EXTERN_C PSLSTATUS PSL_API PSLBktHashGetNextEntry(
                            PSLBKTHASH bh,
                            PSLPARAM* paKey,
                            PSLPARAM* paValue);


/*
 *  Safe Helpers
 *
 *  Collection of macros designed to make the code more readable by
 *  collapsing the process of checking an object to see if it has been
 *  used and destroying it if it has.
 *
 */

#define SAFE_PSLBKTHASHDESTROY(_bh) \
if (PSLNULL != (_bh)) \
{ \
    PSLBktHashDestroy(_bh); \
    (_bh) = PSLNULL; \
}

#endif  /* _PSLBKTHASH_H_ */

