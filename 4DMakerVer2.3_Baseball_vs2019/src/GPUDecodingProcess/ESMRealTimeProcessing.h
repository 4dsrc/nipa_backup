#pragma once
#include "Index.h"

using namespace std;

class CESMRealTimeProcessing
{
public: 
		CESMRealTimeProcessing();
		~CESMRealTimeProcessing();

		void Run(stMakeMovieInfo* MakeMovieInfo);
		void DoGpuDecode(CString strPath,int nGPUIdx,vector<cv::Mat>*ArrFrameInfo);
		void DoReadAdj();

		static unsigned WINAPI DoGPUAdjust(LPVOID param);
		void SplitImage(BYTE* pImg,stDecodeInfo* decodinginfo);
		void DoAdjust(cv::Mat Y,cv::Mat U,cv::Mat V,stDecodeInfo* decodinginfo);
		BOOL DoEncode(CString strSavePath);
		BOOL DoEncode2Way(CString strPath,CString strSavePath);
		BOOL CheckIni(bool bType);

		void DoFinishConnect(BOOL bFinish);
		void DoGPUAdjust();

		int m_nMakingOpt;
		void SetMakingOpt(int nMakeOpt){m_nMakingOpt = nMakeOpt;}
		int  GetMakingOpt(){return m_nMakingOpt;}

		int m_nDeviceNum;
		int m_nFrameCnt;
		int m_nInterFlag;
		int m_nYWidth;
		int m_nYHeight;
		int m_nUVWidth;
		int m_nUVHeight;
		vector<stDecodeInfo>*m_pArrDecodeInfo;
		cv::Mat m_ArrYAdj;//6
		cv::Size m_szYre;//2
		cv::Point m_ptYstr;//2
		cv::Point m_ptYdst;//2

		cv::Mat m_ArrUVAdj;//6
		cv::Size m_szUVre;//2
		cv::Point m_ptUVstr;//2
		cv::Point m_ptUVdst;//2

		cv::cuda::GpuMat gpuYImage,gpuUImage,gpuVImage;
		cv::cuda::GpuMat gpuYRotImage,gpuURotImage,gpuVRotImage;
		cv::cuda::GpuMat gpuYReImage,gpuUReImage,gpuVReImage;

		static unsigned WINAPI DoGpuDecode(LPVOID param);

		int m_nWidth;
		int m_nHeight;
		int m_nStartIdx;
		int m_nEndIdx;
};
struct stThreadDecodingInfo
{
	stThreadDecodingInfo()
	{
		MakeMovieInfo = NULL;
		pParent = NULL;
	}
	stMakeMovieInfo* MakeMovieInfo;
	CESMRealTimeProcessing* pParent;
};
struct stThreadMovieInfo
{
	stThreadMovieInfo()
	{
		pRealTimeProcessing = NULL;
		MakeMovieInfo = NULL;
		ArrDecodeInfo = NULL;
	}
	CESMRealTimeProcessing* pRealTimeProcessing;
	stMakeMovieInfo* MakeMovieInfo;
	vector<stDecodeInfo>*    ArrDecodeInfo;
};