////////////////////////////////////////////////////////////////////////////////
//
//	ESMFileOperation.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-06-20
//
////////////////////////////////////////////////////////////////////////////////
#include "atlstr.h"


#pragma once

enum
{
	PATH_ERROR		= -1,
	PATH_NOT_FOUND	,
	PATH_IS_FILE	,
	PATH_IS_FOLDER	,
};

class CFExeption
{
public:
	CFExeption(DWORD dwErrCode);
	CFExeption(CString sErrText);
	CString GetErrorText() {return m_sError;}
	DWORD GetErrorCode() {return m_dwError;}

private:
	CString m_sError;
	DWORD m_dwError;
};


//--***************************************************************************************************

class CESMMovieFileOperation
{
public:
	CESMMovieFileOperation(); // constructor
	BOOL Delete(CString sPathName, BOOL bRemoveFolder = TRUE); // delete file or folder
	BOOL Copy(CString sSource, CString sDest, BOOL bCreateFolder = TRUE); // copy file or folder
	BOOL Replace(CString sSource, CString sDest); // move file or folder
	BOOL Rename(CString sSource, CString sDest); // rename file or folder

	//-- 2012-06-20 hongsu@esmlab.com
	//-- Get File with include string
	CString SearchFile(CString strFolder, CString strFile, CString strInclude);
	CString SearchLine(CString strFile, CString strInclude);	
	//-- 2013-04-22 hongsu.jung
	//-- move to "..\\"
	CString GetPreviousFolder(CString strFolder);
	char* GetFileVersionInfo(CString strFullPath, CString strFileName);
	//-- 2013-10-18 hongsu@esmlab.com
	//-- File Count in Folder
	int GetFileCount(CString strFolder, CString strExtention = _T("*.jpg"));
	
	
	CString GetErrorString() {return m_sError;} // return error description
	DWORD GetErrorCode() {return m_dwError;} // return error code
	void ShowError()	{MessageBox(NULL, m_sError, _T("Error"), MB_OK | MB_ICONERROR);}// show error message
	
	void SetAskIfReadOnly(BOOL bAsk = TRUE) {m_bAskIfReadOnly = bAsk;}// sets behavior with readonly files(folders)
	BOOL IsAskIfReadOnly()	{return m_bAskIfReadOnly;}	// return current behavior with readonly files(folders)
	
	BOOL CanDelete(CString sPathName); // check attributes
	void SetOverwriteMode(BOOL bOverwrite = FALSE) {m_bOverwriteMode = bOverwrite;}	// sets overwrite mode on/off
	BOOL IsOverwriteMode() {return m_bOverwriteMode;} // return current overwrite mode
	
	int CheckPath(CString sPath);
	BOOL IsAborted() {return m_bAborted;}
	void CreateFolder(CString strDestDir);
	BOOL IsFileExist(CString sPathName);
	BOOL IsFileFolder(CString sPath);	//-- 2012-04-12 hongsu.jung

	
public:
	void DoFileCopy(CString sSourceFile, CString sDestFile, BOOL bDelteAfterCopy = FALSE);
	void DoFolderCopy(CString sSourceFolder, CString sDestFolder, BOOL bDelteAfterCopy = FALSE, BOOL bCreateFolder = TRUE);

protected:
	void DoDelete(CString sPathName, BOOL bRemoveFolder = TRUE);
	void DoCopy(CString sSource, CString sDest, BOOL bDelteAfterCopy = FALSE, BOOL bCreateFolder = TRUE);
	void DoRename(CString sSource, CString sDest);
	void PreparePath(CString &sPath);
	void Initialize();
	void CheckSelfRecursion(CString sSource, CString sDest);
	BOOL CheckSelfCopy(CString sSource, CString sDest);
	CString ChangeFileName(CString sFileName);
	CString ParseFolderName(CString sPathName);

private:
	CString m_sError;
	DWORD m_dwError;
	BOOL m_bAskIfReadOnly;
	BOOL m_bOverwriteMode;
	BOOL m_bAborted;
	int m_iRecursionLimit;
};


//--***************************************************************************************************

