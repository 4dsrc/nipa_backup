#include "ESMRealTimeProcessing.h"
#include "ESMGpuDecode.h"


CESMGpuDecode * g_pDecoder = NULL;

CESMRealTimeProcessing::CESMRealTimeProcessing()
{
	m_ArrYAdj.create(2,3,CV_64FC1);
	m_ArrUVAdj.create(2,3,CV_64FC1);
	m_nYWidth = -1;
	m_nYHeight = -1;
	m_nUVWidth = -1;
	m_nUVHeight = -1;
	m_nDeviceNum = -1;
	m_nMakingOpt = -1;
	m_nFrameCnt	 = -1;
	m_nInterFlag = cv::INTER_LINEAR;
	m_pArrDecodeInfo = NULL;
	g_pDecoder = new CESMGpuDecode;
}
CESMRealTimeProcessing::~CESMRealTimeProcessing()
{
	if(g_pDecoder)
	{
		delete g_pDecoder;
		g_pDecoder = NULL;
	}
}
void CESMRealTimeProcessing::Run(stMakeMovieInfo* MakeMovieInfo)
{
	CString strPath = MakeMovieInfo->strPath;
	CString strSavePath = MakeMovieInfo->strSavePath;
	m_nDeviceNum = MakeMovieInfo->nGPUIdx;

	CT2CA pszConvertedAnsiString (strPath);
	string strDst(pszConvertedAnsiString);
	int nFrameCnt = MakeMovieInfo->nMovieCount;
	m_nFrameCnt = nFrameCnt;

	if(GetMakingOpt()==4)
	{
		m_nStartIdx = MakeMovieInfo->nStartIdx;
		m_nEndIdx = MakeMovieInfo->nEndIdx;
		//m_nWidth = MakeMovieInfo->nWidth;
		//m_nHeight = MakeMovieInfo->nHeight;
	}

	m_nWidth = MakeMovieInfo->nWidth;
	m_nHeight = MakeMovieInfo->nHeight;

	BOOL bFinish = FALSE;
	
	m_pArrDecodeInfo = new vector<stDecodeInfo>(nFrameCnt);

	for(int i = 0 ; i < m_pArrDecodeInfo->size(); i++)
	{
		if(GetMakingOpt() == 4)
		{
			m_pArrDecodeInfo->at(i).nFrameIndex = i + m_nStartIdx;
		}
		else
			m_pArrDecodeInfo->at(i).nFrameIndex = i;
		
	}
	//GPUDecoding

	int nStart = GetTickCount();
	//g_pDecoder = new CESMGpuDecode(strSavePath);
	//g_pDecoder->SetGPUIdx(m_nDeviceNum);
	//g_pDecoder->GPUDecoding(strPath,0,nFrameCnt-1,1,m_pArrDecodeInfo,nFrameCnt);
	stThreadDecodingInfo* pDecodeInfo = new stThreadDecodingInfo;
	pDecodeInfo->MakeMovieInfo = MakeMovieInfo;
	pDecodeInfo->pParent	   = this;
	
	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, DoGpuDecode, (void *)pDecodeInfo, 0, NULL);
	SetThreadPriority(hSyncTime,THREAD_PRIORITY_TIME_CRITICAL);
	CloseHandle(hSyncTime); 

	int nEnd = GetTickCount();

	cout <<endl<<"Decoding: " <<nEnd - nStart <<endl;
	
	if(GetMakingOpt() == 5)
	{
		CString strSecondPath = MakeMovieInfo->strSecondSavePath;
		DoEncode2Way(strSavePath,strSecondPath);
	}
	else
		BOOL bEncode = DoEncode(strSavePath);

	if(g_pDecoder)
	{
		delete g_pDecoder;
		g_pDecoder = NULL;
	}
//	DoFinishConnect(TRUE);
//	if(TRUE)
//	{
//#if 0
//		DoGPUAdjust();
//#else
//		/*//Decoding 후 Adjust Thread 생성
//		stThreadMovieInfo* pMovieInfo = new stThreadMovieInfo;
//		pMovieInfo->MakeMovieInfo = MakeMovieInfo;
//		pMovieInfo->pRealTimeProcessing = this;
//		pMovieInfo->ArrDecodeInfo   = m_pArrDecodeInfo;
//
//		HANDLE hSyncTime = NULL;
//		hSyncTime = (HANDLE) _beginthreadex(NULL, 0, DoGPUAdjust, (void *)pMovieInfo, 0, NULL);
//		CloseHandle(hSyncTime); */
//#endif
//		//Encoding 대기
//		BOOL bEncode = DoEncode(strSavePath);
//
//		DoFinishConnect(TRUE);
//	}
//	else
//	{
//		DoFinishConnect(FALSE);
//	}
}
void CESMRealTimeProcessing::DoReadAdj()
{	
	FILE* file;
	char strData[100];
	
	vector<string>arr;
	if((file =fopen("M:\\AdjData.txt","r")) != NULL)
	{
		while(fgets(strData, 100, file) != NULL)
		{
			arr.push_back(strData);
		}
	}

	for(int i = 0 ; i < 6; i++)
	{
		m_ArrYAdj.at<double>(i) = stod(arr.at(i).c_str());
	}
	m_szYre.width  = atoi(arr.at(6).c_str());
	m_szYre.height = atoi(arr.at(7).c_str());

	m_ptYstr.x	   = atoi(arr.at(8).c_str());
	m_ptYstr.y	   = atoi(arr.at(9).c_str());

	m_ptYdst.x	   = atoi(arr.at(10).c_str());
	m_ptYdst.y	   = atoi(arr.at(11).c_str());

	m_nYWidth  = atoi(arr.at(10).c_str());
	m_nYHeight = atoi(arr.at(11).c_str());

	for(int i = 12 ; i < 18; i++)
	{
		m_ArrUVAdj.at<double>(i-12) = stod(arr.at(i).c_str());
	}
	m_ArrUVAdj.at<double>(2) = stod(arr.at(2).c_str())/2;

	m_ArrUVAdj.at<double>(5) = stod(arr.at(5).c_str())/2;

	m_szUVre.width  = atoi(arr.at(18).c_str());
	m_szUVre.height = atoi(arr.at(19).c_str());

	m_ptUVstr.x	   = atoi(arr.at(20).c_str());
	m_ptUVstr.y	   = atoi(arr.at(21).c_str());

	m_ptUVdst.x	   = atoi(arr.at(22).c_str());
	m_ptUVdst.y	   = atoi(arr.at(23).c_str());

	m_nUVWidth  = atoi(arr.at(22).c_str());
	m_nUVHeight = atoi(arr.at(23).c_str());


	//g_pDecoder
}
void CESMRealTimeProcessing::DoGPUAdjust()
{
	int nStart = GetTickCount();
	for(int i = 0 ; i < m_pArrDecodeInfo->size(); i++)
	{
		stDecodeInfo* decodinginfo = &(m_pArrDecodeInfo->at(i));
		SplitImage(m_pArrDecodeInfo->at(i).pYUV,decodinginfo);
	}
	int nEnd = GetTickCount();

	cout << "DIP : " <<nEnd - nStart <<endl;
}
unsigned WINAPI CESMRealTimeProcessing::DoGPUAdjust(LPVOID param)
{
	stThreadMovieInfo* pDst = (stThreadMovieInfo*) param;
	
	CESMRealTimeProcessing* pMgr = (CESMRealTimeProcessing*)pDst->pRealTimeProcessing;
	stMakeMovieInfo* pMovieInfo = pDst->MakeMovieInfo;
	vector<stDecodeInfo>*ArrDecodeInfo = pDst->ArrDecodeInfo;

	cout << "DIP Start" << endl;
	int nStart = GetTickCount();
	for(int i = 0 ; i < ArrDecodeInfo->size(); i++)
	{
		stDecodeInfo* decodinginfo = &(ArrDecodeInfo->at(i));
		
		while(1)
		{
			if(decodinginfo->bComplete == TRUE)
				break;

			Sleep(10);
		}
		
		pMgr->SplitImage(ArrDecodeInfo->at(i).pYUV,decodinginfo);
	}
	int nEnd = GetTickCount();

	cout << "DIP : " <<nEnd - nStart <<endl;

	delete pDst;
	
	return TRUE;
}
void CESMRealTimeProcessing::SplitImage(BYTE* pImg,stDecodeInfo* decodinginfo)
{
	int nDataSizeY = m_nYWidth * m_nYHeight * sizeof(BYTE);
	int nDataSizeU = m_nUVWidth * m_nUVHeight * sizeof(BYTE);

	cv::Mat Y(m_nYHeight,m_nYWidth,CV_8UC1,pImg);
	cv::Mat U(m_nUVHeight,m_nUVWidth,CV_8UC1,pImg+nDataSizeY);
	cv::Mat V(m_nUVHeight,m_nUVWidth,CV_8UC1,pImg+nDataSizeY+nDataSizeU);

	DoAdjust(Y,U,V,decodinginfo);
}
void CESMRealTimeProcessing::DoAdjust(cv::Mat Y,cv::Mat U,cv::Mat V,stDecodeInfo* decodinginfo)
{
	cv::cuda::setDevice(1);

	gpuYImage.upload(Y);
	gpuUImage.upload(U);
	gpuVImage.upload(V);

	cv::cuda::warpAffine(gpuYImage,gpuYRotImage,m_ArrYAdj ,gpuYImage.size(),m_nInterFlag);
	cv::cuda::warpAffine(gpuUImage,gpuURotImage,m_ArrUVAdj,gpuUImage.size(),m_nInterFlag);
	cv::cuda::warpAffine(gpuVImage,gpuVRotImage,m_ArrUVAdj,gpuVImage.size(),m_nInterFlag);

	cv::cuda::resize(gpuYRotImage,gpuYReImage,m_szYre ,0,0,m_nInterFlag);
	cv::cuda::resize(gpuURotImage,gpuUReImage,m_szUVre,0,0,m_nInterFlag);
	cv::cuda::resize(gpuVRotImage,gpuVReImage,m_szUVre,0,0,m_nInterFlag);

	cv::cuda::GpuMat gpuYResultImage = (gpuYReImage)(cv::Rect( m_ptYstr.x ,m_ptYstr.y ,m_nYWidth  ,m_nYHeight));
	cv::cuda::GpuMat gpuUResultImage = (gpuUReImage)(cv::Rect( m_ptUVstr.x,m_ptUVstr.y,m_nUVWidth ,m_nUVHeight));
	cv::cuda::GpuMat gpuVResultImage = (gpuVReImage)(cv::Rect( m_ptUVstr.x,m_ptUVstr.y,m_nUVWidth ,m_nUVHeight));

	gpuYResultImage.download(decodinginfo->Y);
	gpuUResultImage.download(decodinginfo->U);
	gpuVResultImage.download(decodinginfo->V);

	decodinginfo->nComplete = 1;
/*
	gpuYResultImage.download(Y);
	gpuUResultImage.download(U);
	gpuVResultImage.download(V);*/
}
BOOL CESMRealTimeProcessing::DoEncode2Way(CString strPath,CString strSavePath)
{
	FFMpegMgr ffmpeg,ffmpegSecond;
	int nFrameSpeed = 30;
	int nGOP	= 6;
	//3840,2160,1280,720
	BOOL bEncode	= ffmpeg.InitEncode(strPath,nFrameSpeed,AV_CODEC_ID_MPEG2VIDEO,nGOP,1920,1080);
	BOOL bSecEncode = ffmpegSecond.InitEncode(strSavePath,nFrameSpeed,AV_CODEC_ID_MPEG2VIDEO,nGOP,1280,720);

	if(bEncode && bSecEncode)
	{
		stDecodeInfo* pEndDecodeInfo = NULL;
		for(int i = 0 ; i < m_pArrDecodeInfo->size() ; i++)
		{
			stDecodeInfo* decodeInfo = &m_pArrDecodeInfo->at(i);
			while(1)
			{
				if(decodeInfo->nComplete == 1 || decodeInfo->nComplete == -100)
					break;

				Sleep(1);
			}
			printf("%d Finish!\n",decodeInfo->nFrameIndex);

			if(decodeInfo->nComplete == -100)
			{
				printf("Encoding Fail : %d",decodeInfo->nComplete);
				continue;
			}


			ffmpeg.EncodePushImage(decodeInfo->Y,decodeInfo->U,decodeInfo->V,
				3840,2160);	

			ffmpegSecond.EncodePushImage(decodeInfo->Y2,decodeInfo->U2,decodeInfo->V2,
				1280,720);	

			pEndDecodeInfo = decodeInfo;
		}

		if(pEndDecodeInfo)
		{
			ffmpeg.EncodePushImage(pEndDecodeInfo->Y,pEndDecodeInfo->U,pEndDecodeInfo->V,
			3840,2160);
			ffmpegSecond.EncodePushImage(pEndDecodeInfo->Y2,pEndDecodeInfo->U2,pEndDecodeInfo->V2,
				1280,720);	
		}
	}
	else
	{
		printf("Init Error\n");
		for(int i = 0 ; i < m_pArrDecodeInfo->size() ; i++)
		{
			stDecodeInfo* decodeInfo = &m_pArrDecodeInfo->at(i);

			while(1)
			{
				if(decodeInfo->nComplete == 1 || decodeInfo->nComplete == -100)
					break;

				Sleep(1);
			}
		}
	}
	for(int i = 0 ; i < m_pArrDecodeInfo->size(); i++)
	{
		stDecodeInfo* decodeInfo = &m_pArrDecodeInfo->at(i);

		if(decodeInfo->pYUV)
		{
			delete decodeInfo->pYUV;
			decodeInfo->pYUV = NULL;

			decodeInfo->Y.release();
			decodeInfo->U.release();
			decodeInfo->V.release();

			decodeInfo->Y2.release();
			decodeInfo->U2.release();
			decodeInfo->V2.release();
		}
	}
	m_pArrDecodeInfo->clear();
	delete m_pArrDecodeInfo;
	m_pArrDecodeInfo = NULL;

	return TRUE;
}
BOOL CESMRealTimeProcessing::DoEncode(CString strSavePath)
{
	FFMpegMgr ffmpeg;

	int nFrameSpeed = 30;
	int nWidth = m_nWidth;//m_ptYdst.x;
	int nHeight = m_nHeight;//m_ptYdst.y;
	int nGOP	= 6;

	if(GetMakingOpt() == 1)//60p
	{
		nWidth = 1920;
		nHeight = 1080;
		nFrameSpeed = 60;
	}
	
	if(GetMakingOpt() == 2)//Transcode
	{
		nWidth  = m_nWidth;//960;//1280;
		nHeight = m_nHeight;//540;//720;
		nGOP	= 1;
	}

	if(GetMakingOpt() == 4)
	{
		nWidth = m_nWidth;
		nHeight = m_nHeight;
	}

	BOOL bEncode = ffmpeg.InitEncode(strSavePath,nFrameSpeed,AV_CODEC_ID_MPEG2VIDEO,nGOP,nWidth,nHeight);

	cout << nWidth << endl << nHeight << endl;

	if(bEncode)
	{
		//if(CheckIni(TRUE))
		{
			stDecodeInfo* pEndDecodeInfo = NULL;
			for(int i = 0 ; i < m_pArrDecodeInfo->size() ; i++)
			{
				stDecodeInfo* decodeInfo = &m_pArrDecodeInfo->at(i);
				while(1)
				{
					if(decodeInfo->nComplete == 1 || decodeInfo->nComplete == -100)
						break;

					Sleep(1);
				}
				printf("%d Finish!\n",decodeInfo->nFrameIndex);

				if(decodeInfo->nComplete == -100)
				{
					printf("Encoding Fail : %d",decodeInfo->nComplete);
					continue;
				}

			
				ffmpeg.EncodePushImage(decodeInfo->Y,decodeInfo->U,decodeInfo->V,
					nWidth,nHeight);	
				
				pEndDecodeInfo = decodeInfo;
			}
			
			if(pEndDecodeInfo)
				ffmpeg.EncodePushImage(pEndDecodeInfo->Y,pEndDecodeInfo->U,pEndDecodeInfo->V,
									nWidth,nHeight);
		}

		//CheckIni(FALSE);

		if(GetMakingOpt() == 2)
			ffmpeg.CloseEncode(TRUE,m_nFrameCnt);
		else
			ffmpeg.CloseEncode(TRUE);


		for(int i = 0 ; i < m_pArrDecodeInfo->size(); i++)
		{
			stDecodeInfo* decodeInfo = &m_pArrDecodeInfo->at(i);

			if(decodeInfo->pYUV)
			{
				delete decodeInfo->pYUV;
				decodeInfo->pYUV = NULL;

				decodeInfo->Y.release();
				decodeInfo->U.release();
				decodeInfo->V.release();
			}
		}
		m_pArrDecodeInfo->clear();
		delete m_pArrDecodeInfo;
		m_pArrDecodeInfo = NULL;
		
		return TRUE;
	}
	else
	{
		printf("Init Fail..\n");
		for(int i = 0 ; i < m_pArrDecodeInfo->size() ; i++)
		{
			stDecodeInfo* decodeInfo = &m_pArrDecodeInfo->at(i);

			while(1)
			{
				if(decodeInfo->nComplete == 1 || decodeInfo->nComplete == -100)
					break;

				Sleep(1);
			}
			if(decodeInfo->pYUV)
			{
				delete decodeInfo->pYUV;
				decodeInfo->pYUV = NULL;

				decodeInfo->Y.release();
				decodeInfo->U.release();
				decodeInfo->V.release();
			}
		}
		m_pArrDecodeInfo->clear();
		delete m_pArrDecodeInfo;
		m_pArrDecodeInfo = NULL;

		return FALSE;
	}
}
#define    DATA_LEN    1024
void CESMRealTimeProcessing::DoFinishConnect(BOOL bFinish)
{
	//Processor Communication

	LPSTR    lpMapping;
	BOOL    bEnd = FALSE;
	char    szData[DATA_LEN];
	char    szNo[8];
	HANDLE    hMemMap = NULL;

	if(bFinish)//Processing success
	{
		
	}
	else//Fail...
	{

	}
}
unsigned WINAPI CESMRealTimeProcessing::DoGpuDecode(LPVOID param)
{
	stThreadDecodingInfo* pInfo		= (stThreadDecodingInfo*) param;
	stMakeMovieInfo* pMovieInfo		= pInfo->MakeMovieInfo; 
	CESMRealTimeProcessing* pParent = pInfo->pParent; 

	delete pInfo;

	int nDeviceIdx = pParent->m_nDeviceNum;
	/*if(pParent->m_nDeviceNum == 0)
		nDeviceIdx = 1;
	else
		nDeviceIdx = 0;*/
	pParent->m_nStartIdx;
	pParent->m_nEndIdx;



	g_pDecoder->m_nMakingOpt = pParent->GetMakingOpt();

	if(pParent->GetMakingOpt() == 2 || pParent->GetMakingOpt() == 4)
		g_pDecoder->m_bTransCode = TRUE;

	g_pDecoder->SetOutputWidth(pMovieInfo->nWidth,pMovieInfo->nHeight);

	if(pParent->GetMakingOpt() == 4)
		g_pDecoder->SetOutputWidth(pMovieInfo->nWidth,pMovieInfo->nHeight);
	else if(pParent->GetMakingOpt() == 2)
		g_pDecoder->SetOutputWidth(pMovieInfo->nWidth,pMovieInfo->nHeight);
	else if(pParent->GetMakingOpt() == 5)
		g_pDecoder->SetOutputWidth(1920,1080,1280,720);

	g_pDecoder->SetGPUIdx(0);
	g_pDecoder->GPUDecoding(pMovieInfo->strPath,pParent->m_nStartIdx,pParent->m_nEndIdx,1,
		pParent->m_pArrDecodeInfo,pMovieInfo->nMovieCount);

	/*g_pDecoder->GPUDecoding(pMovieInfo->strPath,0,pMovieInfo->nMovieCount-1,1,
		pParent->m_pArrDecodeInfo,pMovieInfo->nMovieCount);*/

	_endthreadex(1);
	return TRUE;
}

BOOL CESMRealTimeProcessing::CheckIni(bool bType)
{
	BOOL bCheck = FALSE;
	int nState = -1;

	CString strValue;
	strValue.Format(_T("%d"),m_nDeviceNum);
	
	if(bType)
	{
		nState = ::GetPrivateProfileInt( STR_STATE, STR_USE, 9, ESM_4DMAKER_REALTIME );

		if (nState == 9)
		{
			::WritePrivateProfileString( STR_STATE, STR_USE, strValue, ESM_4DMAKER_REALTIME );
			return true;
		}
		else
		{
			while (1)
			{
				nState = ::GetPrivateProfileInt( STR_STATE, STR_USE, 0, ESM_4DMAKER_REALTIME );
				if (nState == 9)
				{
					::WritePrivateProfileString( STR_STATE, STR_USE, strValue, ESM_4DMAKER_REALTIME );
					return true;
				}
				Sleep(20);
			}
		}
	}else
	{
		strValue.Format(_T("9"));
		::WritePrivateProfileString( STR_STATE, STR_USE, strValue, ESM_4DMAKER_REALTIME );
	}
	return true;
}