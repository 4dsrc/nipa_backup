#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#define WINDOWS_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>
#endif

// CUDA Header includes
#include "dynlink_nvcuvid.h"  // <nvcuvid.h>
#include "dynlink_cuda.h"     // <cuda.h>
#include "dynlink_cudaD3D11.h" // <cudaD3D11.h>
#include "dynlink_builtin_types.h"	  // <builtin_types.h>

// CUDA utilities and system includes
#include "helper_functions.h"
#include "helper_cuda_drvapi.h"

// cudaDecodeD3D11 related helper functions
#include "FrameQueue.h"
#include "VideoSource.h"
#include "VideoParser.h"
#include "VideoDecoder.h"
#include "ImageDX.h"

#include "cudaProcessFrame.h"
#include "cudaModuleMgr.h"

// Include files
#include "Index.h"
#include "ESMRealTimeProcessing.h"
#include "FFMpegMgr.h"
typedef struct _ESMFAMEARRAY 
{
	_ESMFAMEARRAY()
	{
		bError = -100;
	}
	BYTE* pYUV;

	BYTE* pImg;
	BOOL bLoad;
	int bError;
	int nResizeX;
	int nResizeY;
	int nWidth;
	int nHeight;
}ESMFrameArray;
struct EffectInfo
{
	EffectInfo()
	{
		nPosX=  0;
		nPosY = 0;
		bZoom = FALSE;
		nZoomRatio = 0;
		bBlur = FALSE;
		nBlurSize = 0;
		nBlurWeight = 0;
	}
	int		nPosX;
	int		nPosY;

	BOOL	bZoom;
	int		nZoomRatio;	
	BOOL	bBlur;
	int		nBlurSize;
	int		nBlurWeight;	
};
struct MakeFrameInfo 
{
	MakeFrameInfo()
	{
		memset(strFramePath, 0, MAX_PATH);
		bComplete	= FALSE;
		nFrameSpeed = 0;
		nImageSize	= 0;
		nDscIndex	= 0;
		nPriIndex	= 0;
		nFrameIndex = 0;
		nObjectIndex = 0;
		bColorRevision	= 0;
		nSameFrameIdx = -1;	// cygil 2015-11-18 동일Frame Not Decoding
		Image		= NULL;
		memset(strCameraID, 0, MAX_PATH);
		nSecIdx		=	0;
		pYUVImg		=	NULL;
		bCompleteGPU = 0;
		nWidth		= 0;
		nHeight		= 0;
		bGPUPlay	= FALSE;
		pParent		= NULL;
		bReverse	= FALSE;
		bInsertWB   = FALSE;
		bPrnColorInfo = FALSE;
		memset(strTime,0,MAX_PATH);
		memset(strLocal, 0, MAX_PATH);
		nSameFrameArrIdx = -1;

		b3DLogo = FALSE;
		bLogoZoom = FALSE;
	}
	TCHAR	strFramePath[MAX_PATH];
	int		nFrameIndex;
	int		nObjectIndex;
	int		nDscIndex;
	BOOL	b3DLogo;
	BOOL	bLogoZoom;
	int		nPriIndex;
	int		nPriValue;
	int		nPriValue2;
	int		nPrinumIndex;
	BOOL bColorRevision;
	BOOL	bComplete;
	BOOL	bCompleteGPU;
	int		nFrameSpeed;
	int		nImageSize;
	int		nSameFrameIdx;	// cygil 2015-11-18 동일Frame Not Decoding
	TCHAR	strCameraID[MAX_PATH];
	int		nSecIdx;		//2016-11-29 SecIdx 추가
	BYTE*	Image;
	BYTE*	pYUVImg;
	BOOL	bValid;
	BOOL	bPlay;
	BOOL	bSensorSync;
	BOOL	bTickSync;
	TCHAR	strDSC_ID[MAX_PATH];
	TCHAR	strDSC_IP[MAX_PATH];
	int		nWidth;
	int		nHeight;
	BOOL	bGPUPlay;
	void*	pParent;
	BOOL	bReverse;
	BOOL	bInsertWB;
	//170210 hjcho
	TCHAR strTime[MAX_PATH];
	TCHAR strLocal[MAX_PATH];
	BOOL	bPrnColorInfo;
	int		nSameFrameArrIdx;
};
struct ThreadFrameData
{
	ThreadFrameData()
	{
		pFrameInfo = NULL;
		pDecodingInfo = NULL;
		bSaveImg = FALSE;
		bReverse = FALSE;
		strPath = _T("");
		//pEffectData = NULL;
	}
	MakeFrameInfo* pFrameInfo;
	stDecodeInfo* pDecodingInfo;
	//EffectInfo* pEffectData;
	BOOL bSaveImg;
	BOOL bReverse;
	CString strPath;
};
using namespace std;
class CESMGpuDecode
{
public:
	CESMGpuDecode(void);
	CESMGpuDecode(CString strSavePath);
	~CESMGpuDecode(void);


public:
	CString m_strWindowName;
	vector<ESMFrameArray*> m_pImgArr;
public:
	void initValue();

	BOOL GPUDecoding(CString strPath, int nStartIndex, int nEndIndex, int nType, vector<MakeFrameInfo>* pArrFrameInfo = NULL,int nFrameSize = 30,BOOL bIsLoad = FALSE);
	void renderVideoFrame(HWND hWnd, bool bUseInterop, int nType, vector<MakeFrameInfo>* pArrFrameInfo = NULL);
	bool copyDecodedFrameToTexture(unsigned int &nRepeats, int bUseInterop, int *pbIsProgressive, int nType, ThreadFrameData* pFrameData = NULL);

	void renderVideoFrame(HWND hWnd, bool bUseInterop,vector<ESMFrameArray*> pImgArr, int nType, vector<MakeFrameInfo>* pArrFrameInfo = NULL);
	bool copyDecodedFrameToTexture(unsigned int &nRepeats, int bUseInterop, int *pbIsProgressive, vector<ESMFrameArray*> pImgArr, int nType, ThreadFrameData* pFrameData = NULL);

	void parseCommand(CString strPath);
	bool loadVideoSource(const char *video_file,unsigned int &width    , unsigned int &height,	unsigned int &dispWidth, unsigned int &dispHeight);
	bool initD3D11(HWND hWnd,int *pbTCC);
	HRESULT initCudaResources(int bUseInterop, int bTCC,BOOL bIsLoad = FALSE);
	BOOL initCudaVideo();
	HRESULT initD3D11Surface(unsigned int nWidth, unsigned int nHeight);
	
	
	void cudaPostProcessFrame(CUdeviceptr *ppDecodedFrame, size_t nDecodedPitch,	CUdeviceptr *ppTextureData,  size_t nTexturePitch,CUmodule cuModNV12toARGB,	CUfunction fpCudaKernel, CUstream streamID);
	void cudaPostProcessFrame(CUdeviceptr *ppDecodedFrame, size_t nDecodedPitch,	CUarray array,CUmodule cuModNV12toARGB,CUfunction fpCudaKernel, CUstream streamID);
	void SaveFrameAsYUV(unsigned char *pdst,const unsigned char *psrc,int width, int height, int pitch);
	HRESULT reinitCudaResources();
	HRESULT cleanup(bool bDestroyContext);
	void freeCudaResources(bool bDestroyContext);
	HRESULT CESMGpuDecode::drawScene(int field_num);
	void computeFPS(HWND hWnd, bool bUseInterop);
	void printStatistics();
	void yuv420_to_rgb( unsigned char *in, unsigned char *out, int w, int h );
	void yuv420_to_rgb( BYTE* in, ESMFrameArray* out, int w, int h );
	void imageAdjust(ThreadFrameData* pFrameData);
	static unsigned WINAPI  imageAdjust(LPVOID param);
	void YUV_lookup_table();
	
	void ConvertYUV2RGB(uint32 *srcImage,     size_t nSourcePitch, uint32 *dstImage,     size_t nDestPitch,	uint32 width,         uint32 height);
	void Passthru_drvapi(uint32 *srcImage,   size_t nSourcePitch,	uint32 *dstImage,   size_t nDestPitch,	uint32 width,       uint32 height);
	uint32 RGBAPACK_10bit(float red, float green, float blue, uint32 alpha);
	uint32 RGBAPACK_8bit(float red, float green, float blue, uint32 alpha);
	void YUV2RGB(uint32 *yuvi, float *red, float *green, float *blue);

	static unsigned WINAPI  ConvertColor(LPVOID param);
	BOOL GPUDecoding(CString strPath, int nStartIndex, int nEndIndex, int nType, vector<stDecodeInfo>*ArrFrameInfo,int nFrameSize,BOOL bIsLoad = FALSE);
	void renderVideoFrame(HWND hWnd, bool bUseInterop, int nType, vector<stDecodeInfo>* pArrFrameInfo = NULL);
	int m_nSelGPU;
	void SetGPUIdx(int nIdx){m_nSelGPU = nIdx;}
	void SplitImage(BYTE* pImg, stDecodeInfo* decodingInfo);
	void DoAdjust(cv::Mat Y,cv::Mat U,cv::Mat V,stDecodeInfo* decodinginfo);
	void DoReadAdj();
	cv::Mat m_ArrYAdj;//6
	cv::Size m_szYre;//2
	cv::Point m_ptYstr;//2
	cv::Point m_ptYdst;//2

	cv::Mat m_ArrUVAdj;//6
	cv::Size m_szUVre;//2
	cv::Point m_ptUVstr;//2
	cv::Point m_ptUVdst;//2
	cv::cuda::GpuMat gpuYImage,gpuUImage,gpuVImage;
	cv::cuda::GpuMat gpuYRotImage,gpuURotImage,gpuVRotImage;
	cv::cuda::GpuMat gpuYReImage,gpuUReImage,gpuVReImage;

	BOOL m_bTransCode;
	int  m_nMakingOpt;
	int m_nYWidth,m_nUVWidth,m_nYHeight,m_nUVHeight;

	//FFMpegMgr *m_Mgr;
	int m_nStart;
	int m_nOutputWidth;
	int m_nOutputHeight;
	int m_nSecondOutputWidth;
	int m_nSecondOutputHeight;
	void SetOutputWidth(int nWidth,int nHeight)
		{m_nOutputWidth = nWidth;m_nOutputHeight = nHeight;}
	void SetOutputWidth(int nWidth,int nHeight,int nSecondWidth,int nSecondHeight)
	{
		m_nOutputWidth = nWidth;
		m_nOutputHeight = nHeight;
		m_nSecondOutputWidth = nSecondWidth;
		m_nSecondOutputHeight = nSecondHeight;
	}
	static unsigned WINAPI ImageThread(LPVOID param);
};		
struct ThreadImageProcessing{
	ThreadImageProcessing()
	{
		decodingInfo = NULL;
		pParent		 = NULL;
	}
	stDecodeInfo* decodingInfo;
	CESMGpuDecode* pParent;
};