#ifndef A_H
#include "Index.h"
#endif

#include "ESMRealTimeProcessing.h"

int main(int argc,char* argv[])
{
#if 0
	int nDSCID = 1;//atoi(argv[1]);

	vector<int> nArrSec;
	
	int nSecIdx = 0;
	while(1)
	{
		if(CheckIni(nSecIdx))
		{

			nSecIdx++;
		}
		Sleep(50);

		continue;
	}
#else
	//Option
	//0: UHD 30p , adapt adjust
	//1: FHD 60p , no adjust, only transcode
	//2: HD Transcoding
	//3: Making Logic
	//4: Muxing Logic
	//5: 2-way Transcoding
	stMakeMovieInfo *MovieInfo = new stMakeMovieInfo;
	//
#if 0
	int nMakeOpt = 2;
	MovieInfo->strPath     = _T("M:\\Movie\\1.mp4");//argv[1];
	MovieInfo->strSavePath = _T("M:\\Movie\\EncodeTest.mp4");;//argv[2];
	MovieInfo->strSecondSavePath = _T("M:\\Movie\\EncodeSecondTest.mp4");;//argv[2];
	MovieInfo->nGPUIdx	   = 0;//atoi(argv[3]);
	MovieInfo->nStartIdx   = 0;//atoi(argv[5]);
	MovieInfo->nEndIdx     = 60;//atoi(argv[6]);
	MovieInfo->nWidth	   = 1920;//atoi(argv[7]);
	MovieInfo->nHeight	   = 1080;//atoi(argv[8]);	
	MovieInfo->nMovieCount  = 60;//MovieInfo->nEndIdx - MovieInfo->nStartIdx + 1;
#else
	int nMakeOpt = atoi(argv[1]);

	if(nMakeOpt == 4)
	{
		MovieInfo->strPath     = argv[2];
		MovieInfo->strSavePath = argv[3];
		MovieInfo->nGPUIdx	   = atoi(argv[4]);
		MovieInfo->nStartIdx   = atoi(argv[5]);
		MovieInfo->nEndIdx     = atoi(argv[6]);
		MovieInfo->nWidth	   = atoi(argv[7]);
		MovieInfo->nHeight	   = atoi(argv[8]);
		MovieInfo->nMovieCount = MovieInfo->nEndIdx - MovieInfo->nStartIdx + 1;//atoi(argv[9]);
	}
	else if(nMakeOpt == 5)
	{
		printf("MakeOpt: %d\n",5);
		printf("%s %s %s\n",argv[2],argv[3],argv[4]);
		MovieInfo->strPath					= argv[2];//_T("M:\\Movie\\10102_0.mp4");argv[1];
		MovieInfo->strSavePath				= argv[3];//_T("F:\\10102__0.mp4");;//argv[2];
		MovieInfo->strSecondSavePath		= argv[4];
		MovieInfo->nGPUIdx					= atoi(argv[5]);//0;//atoi(argv[3]);
		MovieInfo->nMovieCount				= atoi(argv[6]);//30;//atoi(argv[5]);
	}
	else
	{
		MovieInfo->strPath     = argv[2];//_T("M:\\Movie\\10102_0.mp4");argv[1];
		MovieInfo->strSavePath = argv[3];//_T("F:\\10102__0.mp4");;//argv[2];
		MovieInfo->nGPUIdx	   = atoi(argv[4]);//0;//atoi(argv[3]);
		MovieInfo->nMovieCount = atoi(argv[5]);//30;//atoi(argv[5]);
		MovieInfo->nWidth	   = atoi(argv[6]);
		MovieInfo->nHeight	   = atoi(argv[7]);
	}
	int nSleepTime;
	/*if(nMakeOpt == 0)
	{
		nSleepTime = atoi(argv[6]);
	}*/
#endif

	BOOL bFinish = FALSE;
	int nStart = GetTickCount();

	//printf("main Start [%d]\n",nStart);
	//MyLogic....
	//HANDLE hSyncTime = NULL;
	//hSyncTime = (HANDLE) _beginthreadex(NULL, 0, Proc, &bFinish, 0, NULL);
	//CloseHandle(hSyncTime);

	CESMRealTimeProcessing RTProcessing;
	RTProcessing.SetMakingOpt(nMakeOpt);
	
	if(nMakeOpt != 1 && nMakeOpt != 2 && nMakeOpt != 5)
		RTProcessing.DoReadAdj();

	RTProcessing.Run(MovieInfo);

	int nEnd = GetTickCount();

	int nTime = nEnd - nStart;

	/*if(nMakeOpt == 0)
	{
	while(1)
	{
	if(GetTickCount() - nStart > 2000 + nSleepTime)
	break;

	Sleep(1);
	}
	}*/

	cout << "Processing Time :" << nTime << endl;

	//while(1)
	//{
	//	if(bFinish)
	//		break;

	//	Sleep(10);
	//}

	/*cv::VideoCapture vc("M:\\Movie\\EncodeTest.mp4");
	cv::namedWindow("AAA",CV_WINDOW_FREERATIO);
	if(vc.isOpened())
	{
	int nCnt = 0;
	while(1)
	{
	cv::Mat frame;
	vc>>frame;
	if(frame.empty())
	break;
	cv::imshow("AAA",frame);
	cv::waitKey(0);
	nCnt++;
	}
	cout << nCnt << endl;
	}*/

	if(MovieInfo)
	{
		delete MovieInfo;
		MovieInfo = NULL;
	}
#endif
	return 1;
}