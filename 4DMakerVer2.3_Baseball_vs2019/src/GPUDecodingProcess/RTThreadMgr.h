#include "opencv2/core.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"
#include "opencv/cv.h"

#include "atlstr.h"

//using namespace cv;


class CRTThreadMgr
{
public:
	CRTThreadMgr(void);
	~CRTThreadMgr(void);
	
	static unsigned WINAPI RTMakeGPUFrame(LPVOID param);
	static unsigned WINAPI RTMakeGPUAdjust(LPVOID param);
	static unsigned WINAPI RTMakeEncoding(LPVOID param);
	static unsigned WINAPI RunThread(LPVOID param);

	void Run();
	CString GetDSCIDFromPath(CString strPath);
	CString GetSecIdxFromPath(CString strPath);
	
	CString m_strDSCID;
	void SetDSCID(CString strDSCID){m_strDSCID = strDSCID;}

	CString m_ArrstrPath[100];
	int m_Index;
	BOOL m_bFinish[2];
	BOOL m_bDestroy;
};