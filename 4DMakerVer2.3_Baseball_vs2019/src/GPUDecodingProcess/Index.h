#ifndef A_H
#define A_H

#include <Windows.h>
#include <math.h>
#include <memory>
#include <iostream>
#include <cassert>
#include <vector>
#include <stdio.h>
#include <atlstr.h>
#include <crtdbg.h>
#include "atlstr.h"

#include "opencv2/core.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"
#include "opencv/cv.h"
#include "opencv2/cudawarping.hpp"

#define ESM_4DMAKER_REALTIME	_T("M:\\Movie\\RealTime.ini")
#define STR_SECTION_INPUT		_T("INPUT")
#define STR_SECTION_OUTPUT		_T("OUTPUT")
#define STR_INDEX				_T("Index")
#define STR_PATH				_T("PATH")
#define STR_STATE				_T("State")
#define STR_USE					_T("Use")

typedef struct stMakeMovieInfo{
	CString strPath;
	CString strSavePath;
	CString strSecondSavePath;
	int nGPUIdx;
	int nStartIdx;
	int nEndIdx;
	int nMovieCount;
	int nWidth;
	int nHeight;
	//int nFPS;
	//int	nEncodeQuality;
};

typedef struct stDecodeInfo{
	stDecodeInfo()
	{
		pYUV = NULL;
		nComplete = -1;
		nFrameIndex = -1;
		bComplete = FALSE;

	}
	BYTE* pYUV;
	cv::Mat Y;
	cv::Mat U;
	cv::Mat V;
	BOOL bComplete;
	int nComplete;
	int nFrameIndex;
	cv::Mat Y2;
	cv::Mat U2;
	cv::Mat V2;
};
#endif