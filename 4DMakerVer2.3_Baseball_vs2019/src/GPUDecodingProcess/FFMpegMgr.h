#include "Index.h"
#include "ESMRealTimeProcessing.h"

extern"C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/mem.h>
#include <libavutil/opt.h>

#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
};

#define FFMPEGFPS	30

class FFMpegMgr
{
public:
	FFMpegMgr(void);
	~FFMpegMgr(void);
	BOOL InitEncode(CString strFileName, int nFrmSpeed, int codec_id, int nGopSize, int nWidth, int nHeight);
	void EncodePushImage(cv::Mat Y,cv::Mat U,cv::Mat V,int nWidth,int nHeight);
	void PushEncodeData(AVFrame *pFrame);
	void CloseEncode(BOOL bClose,int nFrameCnt = -1);

	AVCodecContext *m_pCodecCtx;
	int m_nEncodeIndex;
	FILE *m_pFile;
	AVFrame *m_pFrame;
	AVBufferRef *m_pAvBufferRef;
	int m_nFrmCountToSpeed;	
	int m_nFrmCount;
	int m_nFrmFinishCount;
	int m_nGopSize;
	int m_bClose;
	int m_nStartFrame;
	int m_nEndFrame;
};