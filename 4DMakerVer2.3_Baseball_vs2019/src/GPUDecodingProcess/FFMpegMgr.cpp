#pragma once
#include "FFMpegMgr.h"

FFMpegMgr::FFMpegMgr(void)
{
	m_pCodecCtx = NULL;
	m_nEncodeIndex = 0;
	m_pFile = NULL;
	m_pFrame = NULL;
	m_nFrmCountToSpeed = 0;
	m_nFrmCount = 0;
	m_nGopSize = 15;
	m_nFrmFinishCount = 0;
	m_bClose = FALSE;
	m_pAvBufferRef = NULL;
	av_register_all();
	avcodec_register_all();
}
FFMpegMgr::~FFMpegMgr(void)
{

}
BOOL FFMpegMgr::InitEncode(CString strFileName, int nFrmSpeed, int codec_id, int nGopSize, int nWidth, int nHeight)
{
	char filename[MAX_PATH];
	sprintf(filename,"%s",strFileName);

	m_nGopSize = nGopSize;
	m_nFrmCount = 0;
	m_nEncodeIndex = 0;
	m_nFrmFinishCount = 0;
	m_nFrmCountToSpeed = nFrmSpeed + int(1000.0 /FFMPEGFPS/2);
	if(m_nFrmCountToSpeed < 1000.0 /FFMPEGFPS)
		m_nFrmCountToSpeed = 1000.0 /FFMPEGFPS + 1;
	m_nFrmCountToSpeed = int((double)m_nFrmCountToSpeed / ((double)1000 /(double)FFMPEGFPS));
	
	AVCodec *pCodec;
	int nRet = 0;

	pCodec = avcodec_find_encoder((AVCodecID)codec_id);
	if(!pCodec)
	{
		printf("Not found codec : %d\n",codec_id);
		return FALSE;
	}

	m_pCodecCtx = avcodec_alloc_context3(pCodec);
	if(!m_pCodecCtx)
	{
		printf("Could not find codecCtx\n");
		return FALSE;
	}
	m_pCodecCtx->height = nHeight;
	m_pCodecCtx->width  = nWidth;

	m_pCodecCtx->time_base.num = 1;
	m_pCodecCtx->time_base.den = FFMPEGFPS;

	m_pCodecCtx->gop_size = nGopSize; /* emit one intra frame every ten frames */
	m_pCodecCtx->max_b_frames=1;
	m_pCodecCtx->pix_fmt = AV_PIX_FMT_YUV420P;
	m_pCodecCtx->thread_type = FF_THREAD_SLICE;
	m_pCodecCtx->thread_count = 16;
	m_pCodecCtx->codec_type = AVMEDIA_TYPE_VIDEO;
	
	
	m_pCodecCtx->bit_rate = 2147483647;
	//int nBitRate = (1024*1024) * 20;
	//m_pCodecCtx->bit_rate = nBitRate;//(1024*1024) * 20;m_AVIMOV_BPS;			//Bits Per Second 
	//m_pCodecCtx->rc_min_rate = nBitRate;//m_AVIMOV_BPS;// / (1024*1024) * 18;
	//m_pCodecCtx->rc_max_rate = nBitRate;//m_AVIMOV_BPS;// / (1024*1024) * 23;	
	//m_pCodecCtx->bit_rate_tolerance = nBitRate;//m_AVIMOV_BPS;
	//m_pCodecCtx->rc_buffer_size = nBitRate;//m_AVIMOV_BPS;
	//m_pCodecCtx->rc_initial_buffer_occupancy = m_pCodecCtx->rc_buffer_size * 3 / 4;
	//m_pCodecCtx->rc_buffer_aggressivity = (float) 1.0;
	//m_pCodecCtx->rc_initial_cplx = 0.5;

	if (avcodec_open2(m_pCodecCtx, pCodec, NULL) < 0) 
	{
		printf("Could not open codec\n");
		delete[] filename;
		return FALSE;
	}

	m_pFile = fopen(filename, "wb");
	
	if (!m_pFile)
	{	
		printf("Could not open  %s", filename);
		return FALSE;
	}

	m_pFrame = av_frame_alloc();

	if (!m_pFrame) 
	{
		//ESMLog(0, _T("Could not allocate video frame"));
		return -1;
	}
	m_pFrame->format = m_pCodecCtx->pix_fmt;
	m_pFrame->width  = m_pCodecCtx->width;
	m_pFrame->height = m_pCodecCtx->height;
	m_pFrame->quality = 1;

	nRet = av_image_alloc(m_pFrame->data, m_pFrame->linesize, m_pCodecCtx->width, m_pCodecCtx->height, m_pCodecCtx->pix_fmt, 32);
	if (nRet < 0) 
	{
		printf("Could not allocate raw picture buffer");

		return FALSE;
	}
}
void FFMpegMgr::EncodePushImage(cv::Mat Y,cv::Mat U,cv::Mat V,int nWidth,int nHeight)
{
	memcpy(m_pFrame->data[0],Y.data,nWidth*nHeight);
	memcpy(m_pFrame->data[1],U.data,nWidth*nHeight/4);
	memcpy(m_pFrame->data[2],V.data,nWidth*nHeight/4);

	PushEncodeData(m_pFrame);
}
void FFMpegMgr::PushEncodeData(AVFrame *pFrame)
{
	AVPacket Packet;
	int nRet = 0, nGotOutput = 0;
	av_init_packet(&Packet);
	Packet.data = NULL;    // packet data will be allocated by the encoder
	Packet.size = 0;
	fflush(stdout);
	pFrame->pts = m_nEncodeIndex++;
	pFrame->quality = 1000;
	
	/* encode the image */
	nRet = avcodec_encode_video2(m_pCodecCtx, &Packet, pFrame, &nGotOutput);
	if (nRet < 0)
	{
		exit(1);
	}

	if (nGotOutput)
	{
		fwrite(Packet.data, 1, Packet.size, m_pFile);
		av_free_packet(&Packet);
	}
	m_nFrmFinishCount = m_nEncodeIndex;
}
void FFMpegMgr::CloseEncode(BOOL bClose,int nFrameCnt)
{
	m_bClose = TRUE;

	uint8_t endcode[] = { 0, 0, 1, 0xb7 };
	/* add sequence end code to have a real mpeg file */
	if( m_pFile)
	{
		fwrite(endcode, 1, sizeof(endcode), m_pFile);
		
		if(nFrameCnt != -1)
		{
			char buffer[2047] = {0,};
			sprintf(buffer, "%d", nFrameCnt);
			fwrite (buffer , 1 , sizeof(buffer) , m_pFile );
		}

		fclose(m_pFile);
	}

	avcodec_close(m_pCodecCtx);
	av_free(m_pCodecCtx);

	if( bClose )
	{
		av_freep(&m_pFrame->data[0]);
		av_frame_free(&m_pFrame);
	}
}