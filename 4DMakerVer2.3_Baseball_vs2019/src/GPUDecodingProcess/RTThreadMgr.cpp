#include "RTThreadMgr.h"
#include "ESMGpuDecode.h"
#include "stdio.h"

CRTThreadMgr::CRTThreadMgr(void)
{
	m_Index = 0;
	
	for(int i = 0; i < 2; i++)
		m_bFinish[i] = FALSE;

	m_bDestroy = FALSE;
}
CRTThreadMgr::~CRTThreadMgr(void)
{

}
void CRTThreadMgr::Run()
{
	HANDLE hSyncTime1 = NULL;
	hSyncTime1 = (HANDLE) _beginthreadex(NULL, 0, RunThread, (void *)this, 0, NULL);
	CloseHandle(hSyncTime1);

	HANDLE hSyncTime = NULL;
	hSyncTime = (HANDLE) _beginthreadex(NULL, 0, RTMakeGPUFrame, (void *)this, 0, NULL);
	CloseHandle(hSyncTime);
}
unsigned WINAPI CRTThreadMgr::RunThread(LPVOID param)
{
	CRTThreadMgr* pMgr = (CRTThreadMgr*) param;
/*
	HANDLE hSyncTime1 = NULL;
	hSyncTime1 = (HANDLE) _beginthreadex(NULL, 0, RTMakeGPUAdjust, (void *)pMgr, 0, NULL);
	CloseHandle(hSyncTime1);*/

	int _nPrevIdx = 0;
	//CString strPath;
	CString strDSCID = pMgr->m_strDSCID;
	UINT nCnt = 0;
	//fstream file;

	char strPath[100];

	BOOL m_bOpenClose = FALSE;

	ifstream file;
	while(1)
	{
		if(pMgr->m_bDestroy)
		{
			pMgr->m_bFinish[0] = TRUE;
			break;
		}
			
		sprintf(strPath,"M:\\Movie\\%s_%d.mp4",strDSCID,_nPrevIdx);
		//FILE* file = fopen(strPath,"r");

		//cv::VideoCapture vc(strPath);
		file.open(strPath);

		if(file.is_open() == FALSE)
		{
			Sleep(10);
			if(++nCnt > 300)	//Sleep 10 기준 75번 반복 = 7.5초 GAP
			{
				if(m_bOpenClose == TRUE)
				{
					pMgr->m_bDestroy = TRUE;
					
					m_bOpenClose = FALSE;
					printf("Close\n");
					_nPrevIdx = 0;
					cout << "Destroy [0]" << endl;
					pMgr->m_bFinish[0] = TRUE;
					break;
				}
			}
		}
		else
		{
			Sleep(10);
			//cout <<strPath <<endl;

			pMgr->m_ArrstrPath[pMgr->m_Index++].Format(_T("%s"),strPath);
			_nPrevIdx++;

			if(pMgr->m_Index > 99)
			{
				pMgr->m_Index = 0;
			}
			nCnt = 0;

			if(m_bOpenClose == FALSE)
			{
				m_bOpenClose = TRUE;

				cout << "Open" << endl;
			}
			
		}

		file.close();

	}


	return TRUE;
}
unsigned WINAPI CRTThreadMgr::RTMakeGPUFrame(LPVOID param)
{
	CRTThreadMgr* pMgr = (CRTThreadMgr*) param;
	//pMgr->m_bFinish[1] = TRUE;
	//return 0;
	int nPos = 0;
	int dec=0;
	vector<MakeFrameInfo>*pArrFrameInfo;
	//MakeFrameInfo pArrFrameInfo[30];
	CString strDSCID = pMgr->m_strDSCID;
	int nSec;
	while(1)
	{
		if(pMgr->m_bDestroy)
		{
			cout << "Destroy [1]" << endl;
			pMgr->m_bFinish[1] = TRUE;
			break;
		}
		if(pMgr->m_ArrstrPath[nPos].GetLength())
		{
			//strPath = pMgr->m_ArrstrPath[nPos];//유니코드
			pArrFrameInfo = new vector<MakeFrameInfo>;

			for(int i =0; i < 30; i++)
			{
				MakeFrameInfo stMakeFrameInfo;
				int n = sizeof(MakeFrameInfo);
				_stprintf(stMakeFrameInfo.strFramePath, pMgr->m_ArrstrPath[nPos]);//유니코드
				stMakeFrameInfo.nFrameIndex = i;
				stMakeFrameInfo.nFrameSpeed = 33;
				stMakeFrameInfo.bGPUPlay = TRUE;
				CString strCamID = pMgr->GetDSCIDFromPath(pMgr->m_ArrstrPath[nPos]);
				CString strSecIdx = pMgr->GetSecIdxFromPath(pMgr->m_ArrstrPath[nPos]);
				
				_stprintf(stMakeFrameInfo.strDSC_ID, _T("%s"), strCamID);//유니코드
				_stprintf(stMakeFrameInfo.strCameraID, _T("%s"), strCamID);
				stMakeFrameInfo.nSecIdx = _ttol(strSecIdx);			//jhhan 16-11-29 SecIdx
				
				nSec = _ttol(strSecIdx);

				stMakeFrameInfo.pYUVImg = NULL;
				stMakeFrameInfo.Image = NULL;
				//pArrFrameInfo[i] = stMakeFrameInfo;
				//pArrFrameInfo->push_back(stMakeFrameInfo);
			}
		}
		else
		{
			Sleep(5);
			dec++;
			if(dec>1000)
			{
				dec = 0;
				continue;
			}
			continue;
		}

		int start = GetTickCount();

		char strPathchar[100];
		CString strPath = pMgr->m_ArrstrPath[nPos];
		CT2CA pszConvertedAnsiString (strPath);
		string strDst(pszConvertedAnsiString);

		sprintf(strPathchar,"%s",strPath);
		cout << strDst<<endl;
		cv::VideoCapture vc(strDst);
		if(!vc.isOpened())
		{
			Sleep(50);
			vc.open(strDst);
			if(!vc.isOpened())
			{
				Sleep(100);
				vc.open(strDst);
				if(!vc.isOpened())	
				{
					printf("Cannot Open File\n");
					pMgr->m_ArrstrPath[nPos++] = _T("");
					if(nPos > 99)
						nPos = 0;

					continue;
				}
			}
		}
		int nMaxFrameCount = vc.get(cv::CAP_PROP_FRAME_COUNT);
		CESMGpuDecode pGPU;
		pGPU.GPUDecoding(strPath,0,30,1,pArrFrameInfo,30,FALSE);
		
		vc.release();
		//int nMaxFrameCount = 30;

		for(int i = 0 ; i < 0; i++)
		{
			MakeFrameInfo frameInfo = pArrFrameInfo->at(i);
			/*if(i==0 && frameInfo.pYUVImg)
			{
				char strSavePath[50];
				sprintf(strSavePath,"M:\\Movie\\%s_%d.jpg",strDSCID,nSec);
				cv::Mat frame(frameInfo.nHeight*1.5,frameInfo.nWidth,CV_8UC1,frameInfo.pYUVImg);
				cv::Mat BGR;
				cvtColor(frame,BGR,cv::COLOR_YUV2BGR_I420);
				imwrite(strSavePath,BGR);
			}*/
			if(frameInfo.pYUVImg)
			{
				delete frameInfo.pYUVImg;
				frameInfo.pYUVImg = NULL;
			}

			if(frameInfo.Image)
			{
				delete frameInfo.Image;
				frameInfo.Image = NULL;
			}

			if(frameInfo.pParent)
			{
				delete frameInfo.pParent;
				frameInfo.pParent = NULL;
			}
			memset(frameInfo.strFramePath,'\0',sizeof(TCHAR) * MAX_PATH);
			//ZeroMemory(&frameInfo.strFramePath,sizeof(TCHAR) * MAX_PATH);
		}
		
		pArrFrameInfo->clear();
		delete pArrFrameInfo;

		pArrFrameInfo = NULL;
		remove(strPathchar);

		

		pMgr->m_ArrstrPath[nPos++] = _T("");
		if(nPos > 99)
		{
			nPos = 0;
		}

	}


	return TRUE;
}
unsigned WINAPI CRTThreadMgr::RTMakeGPUAdjust(LPVOID param)
{
	CRTThreadMgr* pMgr = (CRTThreadMgr*) param;

	return TRUE;
}
CString CRTThreadMgr::GetDSCIDFromPath(CString strPath)
{
	int nFind = strPath.ReverseFind('\\') + 1;
	CString csFile = strPath.Right(strPath.GetLength()-nFind);
	csFile.Trim();
	//csFile.Trim(_T("\\"));
	nFind = csFile.Find('.');
	CString csCam = csFile.Left(nFind);
	csCam.Trim();
	nFind = csCam.Find('_');

	CString strCamID = csCam.Mid(0, nFind);

	return strCamID;
}

CString CRTThreadMgr::GetSecIdxFromPath(CString strPath)
{
	int nFind = strPath.ReverseFind('\\') + 1;
	CString csFile = strPath.Right(strPath.GetLength()-nFind);
	csFile.Trim();
	//csFile.Trim(_T("\\"));
	nFind = csFile.Find('.');
	CString csCam = csFile.Left(nFind);
	csCam.Trim();
	nFind = csCam.ReverseFind('_') + 1;

	CString strSecIdx = csCam.Right(csCam.GetLength()-nFind);

	return strSecIdx;
}