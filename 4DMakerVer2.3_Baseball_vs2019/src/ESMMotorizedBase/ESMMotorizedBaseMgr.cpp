////////////////////////////////////////////////////////////////////////////////
//
//	ESMMotorizedBaseMgr.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-22
//
////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "ESMMotorizedBaseMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CESMMotirizedBaseMgr, CWinThread)

//------------------------------------------------------------------------------ 
//! @brief		CESMMotirizedBaseMgr(HWND hParentFrm)
//! @date		2014-05-24
//! @author		hongsu.jung
//! @note	 	constructor
//------------------------------------------------------------------------------
CESMMotirizedBaseMgr::CESMMotirizedBaseMgr(HWND hParentFrm)
{
	InitializeCriticalSection (&m_CS);	
	m_bThreadRun = FALSE;
	m_bThreadWork = FALSE;
	m_hParentWnd = hParentFrm;
}


//------------------------------------------------------------------------------ 
//! @brief		~CESMMotirizedBaseMgr()
//! @date		2014-05-24
//! @author		hongsu.jung
//! @note	 	destructor
//------------------------------------------------------------------------------
CESMMotirizedBaseMgr::~CESMMotirizedBaseMgr(void)
{
	ExitInstance();
	DeleteCriticalSection (&m_CS);
}

BOOL CESMMotirizedBaseMgr::InitInstance()
{
	m_bThreadWork = TRUE;
	return TRUE;
}

int CESMMotirizedBaseMgr::ExitInstance()
{
	StopThread();	
	return CWinThread::ExitInstance();
}

//------------------------------------------------------------------------------ 
//! @brief		StopThread
//! @date		2010-06-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CESMMotirizedBaseMgr::StopThread()
{
	if(!m_bThreadRun)
		return;

	m_bThreadWork = FALSE;
	while(m_bThreadRun)
		Sleep(1);

	RemoveAll();
	//-- 2013-12-01 hongsu@esmlab.com
	//-- Confirm Stop Thread
	WaitForSingleObject(this->m_hThread, INFINITE);
}

BEGIN_MESSAGE_MAP(CESMMotirizedBaseMgr, CWinThread)
END_MESSAGE_MAP()


void CESMMotirizedBaseMgr::AddEvent(ESMMotorMsg* pMsg)		
{ 
	//-- 2014-05-26 hongsu@esmlab.com
	//-- Don't Get New Message After Stop Thread	
	m_arMsg.Add((CObject*)pMsg);
}

//------------------------------------------------------------------------------ 
//! @brief		RemoveAll()
//! @date		2010-05-24
//! @author	hongsu.jung
//! @note	 	delete all array
//------------------------------------------------------------------------------
void CESMMotirizedBaseMgr::RemoveAll()
{
	//-- Remove Message
	int nAll = (int)m_arMsg.GetSize();
	while(nAll--)
	{
		ESMMotorMsg* pMsg = (ESMMotorMsg*)m_arMsg.GetAt(nAll);
		if(pMsg)
		{
			delete pMsg;
			pMsg = NULL;
			m_arMsg.RemoveAt(nAll);
		}		
		else
		{
			m_arMsg.RemoveAll();
			nAll = 0;
		}
	}
}

//--------------------------------------------------------------------------
//!@brief	RUN THREAD
//!@param   
//!@return	
//--------------------------------------------------------------------------
int CESMMotirizedBaseMgr::Run(void)
{
	int nMsgIndex;
	ESMMotorMsg* pImgMsg = NULL;	

	m_bThreadRun = TRUE;
	//-- Delete Thread by User
	m_bAutoDelete = FALSE;

	while(1)
	{
		//-- 2013-03-01 honsgu.jung
		//-- For CPU
		Sleep(1);

		//-- 2014-05-26 hongsu@esmlab.com
		//-- Get Message 
		nMsgIndex = (int)m_arMsg.GetSize();
		while(nMsgIndex--)
		{
			//-- For CPU
			Sleep(1);

			//-- Get First Message
			pImgMsg = (ESMMotorMsg*)m_arMsg.GetAt(0);
			if(!pImgMsg)
			{
				m_arMsg.RemoveAt(0);
				continue;
			}

			//-- Critical Section 
			EnterCS();			

			//-- 2014-06-27 hongsu@esmlab.com
			//-- Do Something 

			//-- Critical Section 
			LeaveCS();
			
			//-- Delete Message
			//-- 2014-05-29 KCD
			m_arMsg.RemoveAt(0);			
		}		

		//-- 2014-05-26 hongsu@esmlab.com
		//-- Check Until Finish Thread
		if(!m_bThreadWork)
			break;
	}	

	m_bThreadRun = FALSE;
	return 0;
}

//--------------------------------------------------------------------------
//!@brief	CreateEvent
//!@param   
//!@return	
//--------------------------------------------------------------------------
BOOL CESMMotirizedBaseMgr::ActionEvent(ESMMotorMsg* pMsg)
{
	switch(pMsg->nMsg)
	{	
	default:
		return FALSE;
	}
	return TRUE;
}
