////////////////////////////////////////////////////////////////////////////////
//
//	ESMMotorizedBaseMgr.h : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2014-05-22
//
////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <Afxtempl.h>
#include "ESMMotorizedBase.h"
#include "ESMMotorizedDefine.h"


class AFX_EXT_CLASS CESMMotirizedBaseMgr : public CWinThread
{
	DECLARE_DYNCREATE(CESMMotirizedBaseMgr)
public:
	CESMMotirizedBaseMgr(void) {};
	CESMMotirizedBaseMgr(HWND hParentFrm);
	~CESMMotirizedBaseMgr(void);

public:	
	virtual int Run(void);	

protected:
	DECLARE_MESSAGE_MAP()

	virtual BOOL InitInstance();
	virtual int ExitInstance();	
	void StopThread();
	void EnterCS()	{ EnterCriticalSection (&m_CS);	}
	void LeaveCS()	{ LeaveCriticalSection (&m_CS);	}

	void RemoveAll();

private:
	CRITICAL_SECTION m_CS;
	HWND	m_hParentWnd;
	BOOL	m_bThreadWork;	//-- Is Working?
	BOOL	m_bThreadRun;	//-- Is Finish?
	CObArray	m_arMsg;		

public:
	void AddEvent(ESMMotorMsg* pMsg);	
	BOOL ActionEvent(ESMMotorMsg* pMsg);
};
