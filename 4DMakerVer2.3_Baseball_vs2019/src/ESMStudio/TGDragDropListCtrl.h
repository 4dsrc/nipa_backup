//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : TGDragDropListCtrl.h
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------

#pragma once

/////////////////////////////////////////////////////////////////////////////
// CTGDragDropListCtrl window

class CTGDragDropListCtrl : public CListCtrl
{
protected:
	CDWordArray			m_anDragIndexes;
	int					m_nDropIndex;
	CImageList*			m_pDragImage;
	int					m_nPrevDropIndex;
	UINT				m_uPrevDropState;
	DWORD				m_dwStyle;

	enum EScrollDirection
	{
		scrollNull,
		scrollUp,
		scrollDown
	};
	EScrollDirection	m_ScrollDirection;
	UINT				m_uScrollTimer;

// Construction
public:
	CTGDragDropListCtrl();
	bool IsDragging() const { return m_pDragImage != NULL; }

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTGDragDropListCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTGDragDropListCtrl();

protected:
	void DropItem();
	void RestorePrevDropItemState();
	void UpdateSelection(int nDropIndex);
	void SetScrollTimer(EScrollDirection ScrollDirection);
	void KillScrollTimer();
	CImageList* CreateDragImageEx(LPPOINT lpPoint);
	
// Generated message map functions
protected:
	//{{AFX_MSG(CTGDragDropListCtrl)
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
