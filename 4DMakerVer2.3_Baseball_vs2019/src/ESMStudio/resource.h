//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ESMStudio.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDD_SDISAMPLE_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDD_LIVEVIEW                    129
#define IDD_PROPERTY                    130
#define IDD_CONNECTLIST                 131
#define IDC_LIVEVIEW                    1000
#define IDC_CHK_ALL                     1001
#define IDCB_NEARER                     1010
#define IDCB_NEAR                       1011
#define IDCB_FAR                        1012
#define IDCB_FARTHER                    1013
#define IDCS_STATUS                     1014
#define IDCS_FOCUS                      1015
#define IDCS_LIVEVIEW                   1016
#define IDC_COMBO_ISO                   1100
#define IDC_COMBO_SS                    1101
#define IDC_COMBO_MODE                  1102
#define IDC_COMBO_SIZE                  1103
#define IDC_COMBO_QUALITY               1104
#define IDC_COMBO_WB                    1105
#define IDC_COMBO_PW                    1106
#define IDC_EDIT_PATH                   1110
#define IDC_EDIT_FPS                    1111
#define IDC_BTN_PATH                    1120
#define IDC_BTN_APPLY                   1121
#define IDC_BTN_MAKE                    1122
#define IDC_BTN_REC                     1123
#define IDC_BTN_CAP_S1                  1124
#define IDC_BTN_CAP_S2                  1125
#define IDC_CHK_3D                      1130
#define IDC_CHK_AF                      1131
#define IDC_LIST                        1150

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
