//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : ESMStudioDlg.h
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------

#pragma once
// Header File Import

#include "SdiSdk.h"

#include "LiveviewDlg.h"
//-- 2013-02-05 hongsu.jung
#include "ConnectListDlg.h"


// CESMStudioDlg dialog
class CESMStudioDlg : public CDialogEx
{
// Construction
public:
	CESMStudioDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CESMStudioDlg();
	
public:
	CLiveviewDlg* m_pDlgLiveview;
	//-- 2013-02-05 hongsu.jung
	//-- Connection List Dialog
	CConnectListDlg* m_pDlgConnect;	

// Dialog Data
	enum { IDD = IDD_SDISAMPLE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnClose();	
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()	

private:


public:
	LRESULT OnChangeConnect(WPARAM wParam, LPARAM lParam);
	LRESULT OnDialogMsg(WPARAM wParam, LPARAM lParam);
	LRESULT OnSDIMsg(WPARAM wParam, LPARAM lParam);
	
	
public:
	SdiResult CheckConnect(void);
	void SetLiveview(BOOL bLiveview);
	
	void OnCapture(void);	
	void OnCaptureDone(SdiMessage* pMsg);	
	void OnSaveDone(SdiMessage* pMsg);
	void OnGetFocusStatus(SdiMessage* pMsg);

	
	void CheckProperty(SdiMessage* pMsg);
	void OnSetFocus(int nFocusType);		
};
