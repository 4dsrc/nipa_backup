//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : ConnectListDlg.h
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------

#pragma once
#include "afxwin.h"
#include "DevInfoManager.h"
#include "TGDragDropListCtrl.h"
#include "TGCreateMovie.h"

#include "SdiSingleMgr.h"


enum DSC_STATUS
{
	status_unknown	= 0	,
	status_focus		,
	status_focussed		,
	status_focusfail	,
	status_focussuccess	,
	status_af			,
	status_mf			
};

// CConnectListDlg dialog
class CConnectListDlg : public CDialog
{
	DECLARE_DYNAMIC(CConnectListDlg)

public:
	CConnectListDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CConnectListDlg();

// Dialog Data
	enum { IDD = IDD_CONNECTLIST };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	DECLARE_MESSAGE_MAP()
	
private:
	CTGDragDropListCtrl	m_List;

public:
	CTGCreateMovie*	m_pMovie;
	UINT m_nFps;
	BOOL m_b3D;
	BOOL m_bAF;
	BOOL m_bCamIDSort;
	CString m_strPath;

public:
	//-- 2013-02-06 hongsu.jung
	//-- Add Connected Device
	void AddDevice(CSdiSingleMgr* pSDIMg);
	CSdiSingleMgr* GetSelectedDevice();
	
	BOOL IsExist(CString srtVendor);
	int GetSelectedItem();
	void SetSelectedItem(int nIndex);
	
	void SyncConnection(SdiStrArray *pArList);
	void RemoveList(int nIndex);
	void RemoveListAll();

	void UpdateStatus(CSdiSingleMgr* pSDIMg, int nStatus);

	//-- 2013-04-03 yongmmin.lee
	BOOL SortItems(CTGDragDropListCtrl *pListCtrl, int nCol, BOOL bAscending, int low, int high);

	afx_msg void OnBnClickedBtnRec();
	afx_msg void OnBnClickedBtnRelease1();
	afx_msg void OnBnClickedBtnRelease2();
	afx_msg void OnBnClickedBtnApply();	
	afx_msg void OnBnClickedBtnPath();
	afx_msg void OnLvnItemchangedList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnColumnclick(NMHDR *pNMHDR, LRESULT *pResult);

private:
	int GetIntervalTime();
	void SaveDSCList(CStringArray* pArray);	

public:
	afx_msg void OnBnClickedChkAf();
};