////////////////////////////////////////////////////////////////////////////////
//
//	TGDirRead.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-06-20
//
////////////////////////////////////////////////////////////////////////////////

/**********************************************************************

  Sample use:
  
	#include "TGDirRead.h"
	...
	CTGDirRead dr;
	dr.Recurse() = bScanSub;    // scan subdirs ?
	dr.ClearDirs();         // start clean
	dr.GetDirs("c:\\temp"); // get all folders under c:\temp

	// dump the directory list to the debug trace window:

	// get the dir array
	CTGDirRead::SADirVector &dirs = dr.Dirs();

	// loop over it
	for (CTGDirRead::SADirVector::const_iterator dit = dirs.begin(); dit!=dirs.end(); dit++)
	{
		TRACE("%s\n", (*dit).m_sName);
	}
	dr.ClearFiles();        // start clean
	dr.GetFiles("*.jpg");   // get all *.JPG files in c:\temp and below
	// get the file array
	CTGDirRead::FileVector &files = dr.Files();   

	// dump it...
	for (CTGDirRead::FileVector::const_iterator fit = files.begin(); fit!=files.end(); fit++)
	{
		TRACE("%s\n", (*fit).m_sName);
	}
		dr.ClearFiles();		// start clean, again
		dr.GetFiles("*.txt");	// get all *.txt files in c:\temp and below
	// dump them, too
	for (fit = files.begin(); fit!=files.end(); fit++)
	{
		TRACE("%s : %d bytes\n", (*fit).m_sName, (*fit).size);
	}
								
**********************************************************************/


#include "stdafx.h"
#include "TGDirRead.h"
#include <io.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>

#define STR_SIZE	4089

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// some helper functions we'll use here
bool asc_alpha_dir_sort (const CTGDirRead::CDirEntry  &a, const CTGDirRead::CDirEntry  &b);
bool asc_date_file_sort (const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b);
bool asc_size_file_sort (const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b);
bool asc_alpha_file_sort(const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b);
bool dsc_date_file_sort (const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b);
bool dsc_size_file_sort (const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b);
bool dsc_alpha_file_sort(const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CTGDirRead::CTGDirRead(){}
//////////////////////////////////////////////////////////////////////
CTGDirRead::~CTGDirRead(){}
//////////////////////////////////////////////////////////////////////

bool CTGDirRead::GetDirs(CString strFolder, bool bRecurse)
{
	m_sSourceDir = strFolder;
	FormatPath(m_sSourceDir);
	// find tree starting from pDirPath
	if (bRecurse)
	{
		GetSubDirs(m_dirs, m_sSourceDir);
	}
	
	// add the main directory. 
	m_dirs.push_back(CDirEntry(m_sSourceDir));
	return true;	
}

//////////////////////////////////////////////////////////////////////

bool CTGDirRead::GetSubDirs(vector<CTGDirRead::CDirEntry> &dir_array, const CString &path)
{
	CString newPath;
	
	CString searchString;
	searchString = path;
	searchString+= "\\*.*";
	
	try 
	{
#ifndef USE_WIN32_FINDFILE
		struct _finddata_t  c_file;
		long fhandle;

		char* pchCommand = new char[STR_SIZE];
		if(!pchCommand)
			return false;
		memset(pchCommand, 0, STR_SIZE);
#ifdef UNICODE
		size_t CharCinverted = 0;
		//wcstombs(pchCommand, searchString, _tcslen(searchString)); 
		wcstombs_s(&CharCinverted, pchCommand, STR_SIZE, searchString, _tcslen(searchString));
#else
		memcpy(pchCommand, searchString, _tcslen(searchString)); 
#endif
		if ((fhandle=(long)_findfirst( pchCommand, &c_file ))!=-1) 
		{
			// we only care about subdirs
			if ((c_file.attrib & _A_SUBDIR)==_A_SUBDIR) 
			{
				// add c_file.name to the string array
				
				// we'll handle parents on our own
				if ((strcmp(c_file.name, ".")!=0) && (strcmp(c_file.name, "..")!=0)) 
				{
					newPath = path;
					newPath+= "\\";
					newPath+= c_file.name;
					GetSubDirs(dir_array, newPath);               
					dir_array.push_back(newPath);
				}
			}
			
			// find the rest of them	
			while(_findnext( fhandle, &c_file ) == 0 ) 
			{            
				if ((c_file.attrib & _A_SUBDIR)==_A_SUBDIR) 
				{
					// we'll handle parents on our own
					if ((strcmp(c_file.name, ".")!=0) && (strcmp(c_file.name, "..")!=0)) 
					{
						newPath = path;
						newPath+= "\\";
						newPath+= c_file.name;
						GetSubDirs(dir_array, newPath);
						dir_array.push_back(newPath);
					}
				}
			}
			_findclose(fhandle);
		}

		if(pchCommand)
		{
			delete []pchCommand;
			pchCommand = NULL;
		}
#else
		WIN32_FIND_DATA FindFileData;
		HANDLE hFind;
		if ((hFind = FindFirstFile(searchString, &FindFileData))!=INVALID_HANDLE_VALUE)
		{
			// we only care about files, not subdirs
			if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)==FILE_ATTRIBUTE_DIRECTORY) 
			{
				// we'll handle parents on our own
				if ((strcmp(FindFileData.cFileName, ".")!=0) && (strcmp(FindFileData.cFileName, "..")!=0)) 
				{
					newPath = path;
					newPath+= "\\";
					newPath+=FindFileData.cFileName;
					GetSubDirs(dir_array, newPath);               
					dir_array.push_back(newPath);
				}
			}
			
			// find the rest of them	
			while(FindNextFile( hFind, &FindFileData )) 
			{
				// we only care about files, not subdirs
				if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)==FILE_ATTRIBUTE_DIRECTORY) 
				{
					// we'll handle parents on our own
					if ((strcmp(FindFileData.cFileName, ".")!=0) && (strcmp(FindFileData.cFileName, "..")!=0)) 
					{
						newPath = path;
						newPath+= "\\";
						newPath+=FindFileData.cFileName;
						GetSubDirs(dir_array, newPath);                  
						dir_array.push_back(newPath);
					}               
				}
			}
		}
		FindClose(hFind);
#endif
	} 
	catch (...) 
	{
		return false;
	}   
	return true;
}

//////////////////////////////////////////////////////////////////////

bool CTGDirRead::ClearFiles()
{
	m_files.clear();
	return true;
}

//////////////////////////////////////////////////////////////////////

bool CTGDirRead::ClearDirs()
{
	m_dirs.clear();
	return true;
}

//////////////////////////////////////////////////////////////////////

bool CTGDirRead::GetFiles(CString strFileMask, bool bIncludeFilesInFileList, bool bIncludeFoldersInFileList)
{
	// get the files in each of our directories
	for (vector<CDirEntry>::iterator it = m_dirs.begin();it!=m_dirs.end(); it++) 
	{
		CString curDir = (*it).m_sName;
		// sanity check
		if (curDir.IsEmpty())
			continue;

		if (!FindFiles(curDir, strFileMask, bIncludeFilesInFileList, bIncludeFoldersInFileList))
			return false;
	}
	return true;
}	

//////////////////////////////////////////////////////////////////////

bool CTGDirRead::SortFiles(int iSortStyle, bool bReverse)
{
	switch (iSortStyle)
	{
	default:
		break;
	case eSortAlpha:
		std::sort(m_files.begin(), m_files.end(), (bReverse ? dsc_alpha_file_sort : asc_alpha_file_sort));
		break;
	case eSortWriteDate:
		std::sort(m_files.begin(), m_files.end(), (bReverse ? dsc_date_file_sort : asc_date_file_sort));
		break;
	case eSortSize:
		std::sort(m_files.begin(), m_files.end(), (bReverse ? dsc_size_file_sort : asc_size_file_sort));
		break;
	}
	
	return true;
}

//////////////////////////////////////////////////////////////////////

// chop off trailing "\"
void CTGDirRead::FormatPath(CString &path)
{
	CString inPath = path;
	inPath.TrimRight();
	CString tmp;
	
	int iLastSlashPos = inPath.ReverseFind('\\');
	if (iLastSlashPos == -1)
	{
		iLastSlashPos = inPath.ReverseFind('/');
	}
	
	if (iLastSlashPos!=-1) 
	{
		if (iLastSlashPos==inPath.GetLength()-1) 
		{
			path = inPath.Left(iLastSlashPos);
			
			FormatPath(path); // in case the incoming path is "C:\temp\\\..."
		}
	} 
}

//////////////////////////////////////////////////////////////////////

UINT CTGDirRead::FindFiles(const CString & dir, const CString & filter, bool bIncludeFilesInFileList, bool bIncludeFoldersInFileList)
{
	// make sure the path ends in a single "\"
	CString baseName = dir;
	FormatPath(baseName);
	baseName+='\\';
	
	CString fullPath = baseName;
	fullPath += filter;
	
	CString fileName;
	
	// find first file in current directory
#ifndef USE_WIN32_FINDFILE
	struct _finddata_t  c_file;
	long fhandle;
	
	try 
	{

		char* pfullPath = new char[STR_SIZE];
		if(!pfullPath)
			return false;
		memset(pfullPath, 0, STR_SIZE);
#ifdef UNICODE
		size_t CharCinverted = 0;
		//wcstombs(pfullPath, fullPath, _tcslen(fullPath));  
		wcstombs_s(&CharCinverted, pfullPath, STR_SIZE, fullPath, _tcslen(fullPath));
#else
		memcpy(pfullPath, fullPath, _tcslen(fullPath)); 
#endif
		

		if ((fhandle=(long)_findfirst( pfullPath, &c_file ))!=-1) 
		{
			bool bIsFolder = (c_file.attrib & _A_SUBDIR)==_A_SUBDIR;			
			bool bAddThisOne = (bIsFolder && bIncludeFoldersInFileList) || (!bIsFolder && bIncludeFilesInFileList);
			
			// skip . and ..
			if (bIsFolder && (strcmp(c_file.name, ".")==0) || (strcmp(c_file.name, "..")==0))
			{
				bAddThisOne = false;
			}
			
			if (bAddThisOne) 
			{
				fileName = baseName;
				fileName += c_file.name;
				
				CFileEntry t;
				t.bIsFolder = bIsFolder;
				t.attrib = c_file.attrib;
				t.m_sName = fileName;
				t.time_write = c_file.time_write;
				t.time_create = c_file.time_create;
				t.size = c_file.size;
				m_files.push_back(t);
			}
			
			// find the rest of them	
			while(_findnext( fhandle, &c_file ) == 0 ) 
			{
				bool bIsFolder = (c_file.attrib & _A_SUBDIR)==_A_SUBDIR;
				bool bAddThisOne = (bIsFolder && bIncludeFoldersInFileList) || (!bIsFolder && bIncludeFilesInFileList);
				
				// skip . and ..
				if (bIsFolder && (strcmp(c_file.name, ".")==0) || (strcmp(c_file.name, "..")==0))
				{
					bAddThisOne = false;
				}
				
				if (bAddThisOne) 
				{
					fileName=baseName;
					fileName += c_file.name;
					
					CFileEntry t;
					t.bIsFolder = bIsFolder;
					t.attrib = c_file.attrib;
					t.m_sName = fileName;
					t.time_write = c_file.time_write;
					t.time_create = c_file.time_create;
					t.size = c_file.size;
					m_files.push_back(t);
				}
			}
			_findclose(fhandle);
		}

		if(pfullPath)
		{
			delete []pfullPath;
			pfullPath = NULL;
		}
	} 
	catch (...) 
	{
		return false;
	}
#else
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	try 
	{
		if ((hFind = FindFirstFile(fullPath, &FindFileData))!=INVALID_HANDLE_VALUE)
		{
			bool bIsFolder = (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)==FILE_ATTRIBUTE_DIRECTORY;
			
			bool bAddThisOne = (bIsFolder && bIncludeFoldersInFileList) || ((!bIsFolder) && bIncludeFilesInFileList);
			
			// skip . and ..
			if (bIsFolder && (strcmp(FindFileData.cFileName, ".")==0) || (strcmp(FindFileData.cFileName, "..")==0))
			{
				bAddThisOne = false;
			}
			
			if (bAddThisOne) 
			{
				fileName = baseName;
				fileName += FindFileData.cFileName;
				
				CFileEntry t;
				t.m_sName = fileName;
				
				t.bIsFolder = bIsFolder;
				t.attrib = FindFileData.dwFileAttributes;
				
				ASSERT(sizeof(FindFileData.ftLastWriteTime)== sizeof(_FILETIME));
				memcpy(&t.time_write, &FindFileData.ftLastWriteTime, sizeof(_FILETIME));
				
				ASSERT(sizeof(FindFileData.ftLastWriteTime)== sizeof(_FILETIME));
				memcpy(&t.time_create, &FindFileData.ftCreationTime, sizeof(_FILETIME));
				
				t.size = ((unsigned __int64)FindFileData.nFileSizeHigh * ((unsigned __int64)MAXDWORD+1)) + (unsigned __int64)FindFileData.nFileSizeLow;
				m_files.push_back(t);
			}
			
			// find the rest of them	
			while (FindNextFile(hFind, &FindFileData))
			{
				bool bIsFolder = (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)==FILE_ATTRIBUTE_DIRECTORY;
				
				bool bAddThisOne = (bIsFolder && bIncludeFoldersInFileList) || ((!bIsFolder) && bIncludeFilesInFileList);
				
				// skip . and ..
				if (bIsFolder && (strcmp(FindFileData.cFileName, ".")==0) || (strcmp(FindFileData.cFileName, "..")==0))
				{
					bAddThisOne = false;
				}
				
				if (bAddThisOne) 
				{
					fileName = baseName;
					fileName += FindFileData.cFileName;
					
					CFileEntry t;
					t.m_sName = fileName;
					
					t.bIsFolder = bIsFolder;
					t.attrib = FindFileData.dwFileAttributes;
					
					ASSERT(sizeof(FindFileData.ftLastWriteTime)== sizeof(_FILETIME));
					memcpy(&t.time_write, &FindFileData.ftLastWriteTime, sizeof(_FILETIME));
					
					ASSERT(sizeof(FindFileData.ftLastWriteTime)== sizeof(_FILETIME));
					memcpy(&t.time_create, &FindFileData.ftCreationTime, sizeof(_FILETIME));
					
					t.size = ((unsigned __int64)FindFileData.nFileSizeHigh * ((unsigned __int64)MAXDWORD+1)) + (unsigned __int64)FindFileData.nFileSizeLow;
					m_files.push_back(t);
				}
			}
			FindClose(hFind);
		}
	} 
	catch (...) 
	{
		return false;
	}
#endif
	
	return true;
}

//////////////////////////////////////////////////////////////////////

bool asc_alpha_dir_sort(const CTGDirRead::CDirEntry &a, const CTGDirRead::CDirEntry &b)
{
	if(a.m_sName == b.m_sName)
		return true;
	return false;
	//return (stricmp(a.m_sName, b.m_sName) < 0);
}

//////////////////////////////////////////////////////////////////////

bool asc_alpha_file_sort(const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b)
{
	if(a.m_sName == b.m_sName)
		return true;
	return false;
	//return (stricmp(a.m_sName, b.m_sName) < 0);
}

//////////////////////////////////////////////////////////////////////

bool asc_date_file_sort(const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b)
{
#ifndef USE_WIN32_FINDFILE
	return (difftime(a.time_write, b.time_write) < 0);
#else
	return (a.time_write < b.time_write);
#endif
}

//////////////////////////////////////////////////////////////////////

bool asc_size_file_sort(const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b)
{
	return (a.size < b.size);
}

//////////////////////////////////////////////////////////////////////

bool dsc_alpha_file_sort(const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b)
{
	return (!asc_alpha_file_sort(a,b));
}

//////////////////////////////////////////////////////////////////////

bool dsc_date_file_sort(const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b)
{
	return (!asc_date_file_sort(a,b));
}

//////////////////////////////////////////////////////////////////////

bool dsc_size_file_sort(const CTGDirRead::CFileEntry &a, const CTGDirRead::CFileEntry &b)
{
	return (!asc_size_file_sort(a,b));
}
