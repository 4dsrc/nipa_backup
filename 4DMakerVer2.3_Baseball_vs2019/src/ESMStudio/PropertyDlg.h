//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : PropertyDlg.h
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------

#pragma once
#include "afxwin.h"
#include "DevInfoManager.h"

// CPropertyDlg dialog
class CPropertyDlg : public CDialog
{
	DECLARE_DYNAMIC(CPropertyDlg)

public:
	CPropertyDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPropertyDlg();

// Dialog Data
	enum { IDD = IDD_PROPERTY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	afx_msg void OnCbnSelchangeComboMode();
	afx_msg void OnCbnSelchangeComboSize();
	afx_msg void OnCbnSelchangeComboQuality();
	afx_msg void OnCbnSelchangeComboWB();
	afx_msg void OnCbnSelchangeComboPW();
	afx_msg void OnCbnSelchangeComboISO();
	afx_msg void OnCbnSelchangeComboSS();

public:
	CDevInfoManager* m_pDevMgr;

	CComboBox m_ctrlMode;
	CComboBox m_ctrlPhotosize;
	CComboBox m_ctrlQuality;	
	CComboBox m_ctrlWB;	
	CComboBox m_ctrlPW;	
	CComboBox m_ctrlISO;
	CComboBox m_ctrlSS;

public:
	void UpdateCombo(int nPTPCode);
	void AddDevInfo(IDeviceInfo *pDevInfo);
	CDevInfoManager* GetDevMgr()	{return m_pDevMgr;}

	CString GetPropIndexToPropValueStr(int nPTPCode);//, int nPropIndex);
};