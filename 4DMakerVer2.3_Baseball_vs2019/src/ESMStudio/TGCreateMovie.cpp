//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : TGCreateMovie.cpp
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------

#include "stdafx.h"
#include "ESMStudio.h"
#include "TGCreateMovie.h"
#include "SdiSdk.h"

//-- time measure
#include <sys/timeb.h>
#include <time.h>

#include "TGFileOperation.h"

#define TIMER_WAIT_SAVE_PICTURE 1500

IMPLEMENT_DYNCREATE(CTGCreateMovie, CWinThread)

//------------------------------------------------------------------------------ 
//! @brief		GetTime
//! @date		2011-7-18
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString GetDate()
{
	CString strDate = _T("");
	CTime time  = CTime::GetCurrentTime();
	int nYear = time.GetYear();
	int nMonth = time.GetMonth();
	int nDay = time.GetDay();

	strDate.Format(_T("%d%02d%02d"), nYear, nMonth, nDay);
	return strDate;
}

//------------------------------------------------------------------------------ 
//! @brief		GetTime
//! @date		2011-7-18
//! @author		Lee JungTaek
//! @note	 	
//------------------------------------------------------------------------------ 
CString GetTime()
{
	struct _timeb milli_time;
	_ftime( &milli_time );	

	CString strTime = _T("");
	CTime time  = CTime::GetCurrentTime();
	int nHour = time.GetHour();
	int nMin = time.GetMinute();
	int nSec = time.GetSecond();
	int nMilliSec = milli_time.millitm;

	strTime.Format(_T("%02d%02d%02d.%03d"), nHour, nMin, nSec, nMilliSec);

	return strTime;
}


// CTGCreateMovie
CTGCreateMovie::CTGCreateMovie(CString strPath, double nFps, CStringArray* pArray, BOOL b3D)
{
	//-- 2013-02-05 hongsu.jung
	//-- Set Movie File Name
	m_strPath = strPath;
	m_strFilePath.Format(_T("%s\\%s_%s.avi"), strPath, GetDate(), GetTime());
	//-- Set Movie File Fps
	m_nFps	 = nFps;	
	m_bWrite = TRUE;
	m_bRun	 = FALSE;
	m_b3D	 = b3D;

	//-- 2013-02-05 hongsu.jung
	//-- Full HD 1920 X 1080 (wide 2.1M)
	m_MovieSize = cvSize(1920,1080);

	if(pArray)
	{
		m_arDSC.Copy(*pArray);
		pArray->RemoveAll();
		delete pArray;	
	}
}

CTGCreateMovie::~CTGCreateMovie()
{
	m_arSavedPicture.RemoveAll();
	m_arDSC.RemoveAll();
}

BOOL CTGCreateMovie::InitInstance() { return TRUE; }

int CTGCreateMovie::ExitInstance()				
{
	return CWinThread::ExitInstance();
}

//------------------------------------------------------------------------------ 
//! @brief		StopThread
//! @date		2010-06-29
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CTGCreateMovie::StopThread()
{
	while(m_bRun)
		Sleep(1);
	TRACE(_T("====== Stop Movie Thread ======\n"));
}

void CTGCreateMovie::AddPicture(CString strFile)
{
	TRACE(_T("> Save Picture [%s]\n"),strFile);
	m_arSavedPicture.Add(strFile);	
}

//------------------------------------------------------------------------------ 
//! @brief		Run
//! @date		2010-05-30
//! @author	hongsu.jung
//! @note	 	get message
//------------------------------------------------------------------------------ 
int CTGCreateMovie::Run(void)
{
	m_bRun = TRUE;

	//-- 2013-04-15 hongsu.jung
	//-- Check file Size
	if(!CheckFinish())
	{
		AfxMessageBox(_T("Not Enough DSC Count"));
		return 0;
	}


	CStringA ansiStrMovie(m_strFilePath);
	//CvVideoWriter* writer = cvCreateVideoWriter(ansiStrMovie, CV_FOURCC('M', 'J', 'P', 'G'), m_nFps, size);
	CvVideoWriter* writer = cvCreateVideoWriter(ansiStrMovie, 1, m_nFps, m_MovieSize);	

	int nTimer = TIMER_WAIT_SAVE_PICTURE * 10;
	while(m_bWrite)
	{
		//-- yield thread
		Sleep(1);
		int nAll = m_arSavedPicture.GetCount();
		if(!nAll)
		{			
			nTimer--;
			if(!nTimer)
				m_bWrite = FALSE;
			continue;
		}

		nTimer = TIMER_WAIT_SAVE_PICTURE* 5;
		//-- Make 3D Movie
		if(m_b3D)
		{
			Sleep(1000);
			//-- 2013-02-20 hongsu.jung
			//-- Get File on sequence
			if(!Save3DMovie(writer))
				continue;			
		}
		//-- Just Movie
		else
		{
			if(!SaveMovie(writer))
				continue;			
		}		
	}	
	//-- releasing the Video writer	which is the same as we release the image..or any other structure
	cvReleaseVideoWriter( &writer );		

	TRACE(_T("====== Finish Create Movie ======\n"));
	TRACE(_T("====== Remind Picture [%d] ======\n"),m_arDSC.GetCount());

	m_bRun = FALSE;

	//-- Remove Self
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_REMOVE_MOVIEMAKER;
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESMSTUDIO, (WPARAM)pMsg->message, (LPARAM)pMsg);	
	return 0;
}

BOOL CTGCreateMovie::SaveMovie(CvVideoWriter* writer)
{
	int nAll = m_arDSC.GetCount();	
	if(nAll < 1)
	{
		m_bWrite = FALSE;
		return FALSE;
	}

	CString strPicture;	
	//-- Check Order
	//-- 2013-04-07 hongsu.jung
	strPicture = m_arDSC.GetAt(0);
	if(!CheckNextFile(strPicture))
		return FALSE;

	CStringA ansiStrPicture(strPicture);
	//-- 2013-02-05 hongsu.jung		
	IplImage * image = cvLoadImage(ansiStrPicture);
	if(!image)
		return FALSE;

	IplImage * temp = cvCreateImage(m_MovieSize, image->depth, image->nChannels);
	cvResize( image, temp );		
	cvWriteFrame( writer, temp);
	//-- Release Image
	cvReleaseImage( &image );		
	cvReleaseImage( &temp );

	TRACE(_T("Load File %s\n"),strPicture);	
	m_arDSC.RemoveAt(0);
	return TRUE;
}

BOOL CTGCreateMovie::Save3DMovie(CvVideoWriter* writer)
{
	int nAll = m_arDSC.GetCount();	
	if(nAll < 2)
	{
		m_bWrite = FALSE;
		return FALSE;
	}

	CString strPicture1;
	CString strPicture2;

	//-- Check Order
	strPicture1 = m_arDSC.GetAt(0);
	strPicture2 = m_arDSC.GetAt(1);
	if(!CheckNextFile(strPicture1,strPicture2))
		return FALSE;

	//-- 2013-02-05 hongsu.jung		
	IplImage * image = Create3DPicture(strPicture1,strPicture2);
	if(!image)
		return FALSE;

	IplImage * temp = cvCreateImage(m_MovieSize, image->depth, image->nChannels);
	cvResize( image, temp );		
	cvWriteFrame( writer, temp);
	//-- Release Image
	cvReleaseImage( &image );		
	cvReleaseImage( &temp );

	TRACE(_T("Load File %s + %s \n"),strPicture1, strPicture2);	
	m_arDSC.RemoveAt(0);
	return TRUE;
}

IplImage* CTGCreateMovie::Create3DPicture(CString strPicture1, CString strPicture2)
{
	//-- 2013-02-05 hongsu.jung		
	//-- Make Left 50%, Right 50%
	CStringA ansiStrPicture1(strPicture1);
	CStringA ansiStrPicture2(strPicture2);

	IplImage * image1 = cvLoadImage(ansiStrPicture1);
	if(!image1)
		return NULL;
	
	IplImage * image2 = cvLoadImage(ansiStrPicture2);
	if(!image2)
		return NULL;

	//-- 2013-02-05 hongsu.jung
	//-- Full HD 1920 X 540 (1080/2)
	CvSize size_half = cvSize(1920/2,1080);

	IplImage * temp1	= cvCreateImage(size_half	, image1->depth, image1->nChannels);
	cvResize( image1, temp1 );		
	
	IplImage * temp2	= cvCreateImage(size_half	, image2->depth, image2->nChannels);
	cvResize( image2, temp2 );
	
	
	//-- 3D Image
	IplImage * temp3D	= cvCreateImage (m_MovieSize	, image1->depth, image1->nChannels);

	cvSetImageROI (temp3D, cvRect(	0			, 0				,
									temp1->width, temp1->height));
	cvCopy(temp1, temp3D);
	cvSetImageROI (temp3D, cvRect(	temp1->width, 0				,
									temp2->width, temp2->height));
	cvCopy(temp2, temp3D);

	cvResetImageROI(temp3D);

	//-- Release Image
	cvReleaseImage( &image1 );		
	cvReleaseImage( &image2 );		
	cvReleaseImage( &temp1 );	
	cvReleaseImage( &temp2 );

	return temp3D;
}

BOOL CTGCreateMovie::CheckNextFile(CString strPicture)
{
	TRACE(_T("Check Next File [%s]\n"),strPicture);

	int nAll = m_arSavedPicture.GetCount();
	CString strSaved;
	for(int i = 0 ; i < nAll ; i ++)
	{
		strSaved = m_arSavedPicture.GetAt(i);
		if(strSaved == strPicture)
		{
			m_arSavedPicture.RemoveAt(i);
			TRACE(_T("TRUE: Check Next File [%s]\n"),strPicture);
			return TRUE;
		}
	}

	TRACE(_T("FAIL: Check Next File [%s]\n"),strPicture);
	return FALSE;
}

BOOL CTGCreateMovie::CheckNextFile(CString strPicture1, CString strPicture2)
{
	int nAll = m_arSavedPicture.GetCount();
	CString strSaved;
	for(int i = 0 ; i < nAll ; i ++)
	{
		strSaved = m_arSavedPicture.GetAt(i);
		if(strSaved == strPicture1)
		{
			//-- 2013-02-20 hongsu.jung
			//-- Find Next File
			nAll = m_arSavedPicture.GetCount();
			for(int j = 0; j < nAll ; j++)
			{
				strSaved = m_arSavedPicture.GetAt(j);
				if(strSaved == strPicture2)
				{
					//m_arSavedPicture.RemoveAt(i);			
					return TRUE;
				}
			}
			return FALSE;
		}
	}
	return FALSE;
}

BOOL CTGCreateMovie::CheckFinish(BOOL b3D)
{
	int nAll = m_arDSC.GetCount();
	//-- 3D
	if(b3D)
	{
		if(nAll < 2)
		{
			m_bWrite = FALSE;
			return FALSE;
		}
	}
	else
	{
		if(nAll < 1)
		{
			m_bWrite = FALSE;
			return FALSE;
		}
	}
	return TRUE;
}