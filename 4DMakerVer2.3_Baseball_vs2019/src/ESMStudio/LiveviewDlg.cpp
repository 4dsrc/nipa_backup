//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : LiveviewDlg.cpp
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------

#include "stdafx.h"
#include "ESMStudio.h"
#include "ESMStudioDlg.h"
#include "LiveviewDlg.h"
#include "afxdialogex.h"
#include "SdiSdk.h"

// CLiveviewDlg dialog

IMPLEMENT_DYNAMIC(CLiveviewDlg, CDialog)

CLiveviewDlg::CLiveviewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLiveviewDlg::IDD, pParent)
	, m_pLiveview	(NULL)	
	, m_pLiveSDI	(NULL)
{	
}

CLiveviewDlg::~CLiveviewDlg()
{
	if(m_pLiveview)
	{
		delete m_pLiveview;
		m_pLiveview = NULL;
	}
}

void CLiveviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDCS_FOCUS		, m_staticFocus	);
	DDX_Control(pDX, IDCB_NEARER	, m_btnNearer	);
	DDX_Control(pDX, IDCB_NEAR		, m_btnNear		);
	DDX_Control(pDX, IDCB_FAR		, m_btnFar		);
	DDX_Control(pDX, IDCB_FARTHER	, m_btnFarther	);	
}


BEGIN_MESSAGE_MAP(CLiveviewDlg, CDialog)
	ON_BN_CLICKED(IDCB_FAR		, OnBnClickedFar	)
    ON_BN_CLICKED(IDCB_FARTHER	, OnBnClickedFarther)
    ON_BN_CLICKED(IDCB_NEAR		, OnBnClickedNear	)
    ON_BN_CLICKED(IDCB_NEARER	, OnBnClickedNearer	)
END_MESSAGE_MAP()

BOOL CLiveviewDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	//-- Set Position 
	SetPosition();

	//-- Create Liveview
	m_pLiveview = new CLiveview();
	m_pLiveview->Create(NULL,_T(""),WS_VISIBLE|WS_CHILD,CRect(CPoint(5,5),CSize(600,424)),this,IDC_LIVEVIEW);

	return TRUE;
}

void CLiveviewDlg::SetBuffer(PVOID pBuffer)
{
	if(m_pLiveview)
		m_pLiveview->SetBuffer(pBuffer);
}

void CLiveviewDlg::SetPosition()
{
	m_staticFocus.	MoveWindow(400,444,60,20);
	m_btnNearer.	MoveWindow(450,435,35,30);	
	m_btnNear.		MoveWindow(490,435,35,30);	
	m_btnFar.		MoveWindow(530,435,35,30);	
	m_btnFarther.	MoveWindow(570,435,35,30);
}

void CLiveviewDlg::Connection(CSdiSingleMgr* pLiveSDI)
{
	if(!pLiveSDI)
	{
		m_pLiveSDI->SetLiveview(FALSE);
		return;
	}

	if(m_pLiveSDI)
	{
		if(pLiveSDI != m_pLiveSDI)
			m_pLiveSDI->SetLiveview(FALSE);
			// pSdiSDK->SdiLiveviewSet(bLiveview);
	}

	m_pLiveSDI = pLiveSDI;
	m_pLiveSDI->SetLiveview(TRUE);
	m_pLiveview->SetLiveview(TRUE);

	UpdateData(FALSE);
}

void CLiveviewDlg::Disconnection(CSdiSingleMgr* pLiveSDI)
{
	if(m_pLiveSDI == pLiveSDI)
	{
		m_pLiveSDI->SetLiveview(FALSE);
		//-- 2013-02-13 hongsu.jung
		//-- DRAW NONE
		m_pLiveview->SetLiveview(FALSE);
		m_pLiveview->DrawLiveview();
		m_pLiveSDI = NULL;
		return;
	}
	UpdateData(FALSE);
}

void CLiveviewDlg::OnBnClickedFar()
{
    SendFocusMsg(SDI_FOCUS_FAR);
}

void CLiveviewDlg::OnBnClickedFarther()
{
    SendFocusMsg(SDI_FOCUS_FARTHER);
}

void CLiveviewDlg::OnBnClickedNear()
{
    SendFocusMsg(SDI_FOCUS_NEAR);
}

void CLiveviewDlg::OnBnClickedNearer()
{
    SendFocusMsg(SDI_FOCUS_NEARER);
}

void CLiveviewDlg::SendFocusMsg(ULONG nFocusType)
{
    SdiMessage* pMsg = new SdiMessage();
    pMsg->message = WM_SAMPLE_OP_SET_FOCUSPOSITION;
    pMsg->nParam1 = nFocusType;
    ::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESMSTUDIO, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

// CLiveviewDlg message handlers
void CLiveviewDlg::OnBnClickedCapture()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_CAPTURE;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_ESMSTUDIO, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

