/*
 * Project : Samsung Digital Imaging Software Development Kit
  *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file        SdiManager.cpp
 \brief      Define specific structure and value defined PTP Spec.
 \author 
 \date	2011-07-08
 \version 1.0
 */

#pragma once

#define RS_NA	-1
#define RS_NG	0
#define RS_OK	1
#define RS_STOP	2
#define RS_DISCONNECT	3
#define RS_ERR_RequireLabel -2


enum EPTP_STAT
{
    PTP_STAT_DISCONNECT = 0,
    PTP_STAT_CONNECT = 1
};