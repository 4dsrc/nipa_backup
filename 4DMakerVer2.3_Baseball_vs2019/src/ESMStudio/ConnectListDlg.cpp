//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : ConnectListDlg.cpp
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------


#include "stdafx.h"
#include "ESMStudio.h"
#include "ConnectListDlg.h"
#include "afxdialogex.h"
#include "SdiSdk.h"
#include "ESMStudioDlg.h"

//TimeTrace
#include <sys/timeb.h>
#include <time.h>

// CConnectListDlg dialog
IMPLEMENT_DYNAMIC(CConnectListDlg, CDialog)

CConnectListDlg::CConnectListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConnectListDlg::IDD, pParent)
	, m_nFps(0)
	, m_pMovie(NULL)	
	, m_strPath(_T("c:\\movie"))
	, m_b3D(FALSE)
	, m_bAF(TRUE)
	, m_bCamIDSort(FALSE)
{	
}

CConnectListDlg::~CConnectListDlg()
{
	if(m_pMovie)
	{
		delete m_pMovie;
		m_pMovie = NULL;
	}

	//-- 2013-02-17 hongsu.jung
	RemoveListAll();
}

void CConnectListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);	
	DDX_Control	(pDX, IDC_LIST		, m_List);	
	DDX_Text	(pDX, IDC_EDIT_FPS	, m_nFps);
	DDX_Text	(pDX, IDC_EDIT_PATH	, m_strPath);
	DDX_Check	(pDX, IDC_CHK_3D	, m_b3D);
	DDX_Check	(pDX, IDC_CHK_AF	, m_bAF);
	DDV_MinMaxUInt(pDX, m_nFps, 0, 60);		
}

BEGIN_MESSAGE_MAP(CConnectListDlg, CDialog)		
	ON_BN_CLICKED	(IDC_BTN_REC	, OnBnClickedBtnRec)
	ON_BN_CLICKED	(IDC_BTN_CAP_S1	, OnBnClickedBtnRelease1)
	ON_BN_CLICKED	(IDC_BTN_CAP_S2	, OnBnClickedBtnRelease2)
	ON_BN_CLICKED	(IDC_BTN_APPLY	, OnBnClickedBtnApply)	
	ON_BN_CLICKED	(IDC_BTN_PATH	, OnBnClickedBtnPath)
	ON_BN_CLICKED	(IDC_CHK_AF		, OnBnClickedChkAf)
	ON_NOTIFY		(LVN_ITEMCHANGED, IDC_LIST, OnLvnItemchangedList)
	ON_NOTIFY		(LVN_COLUMNCLICK, IDC_LIST, OnColumnclick)
END_MESSAGE_MAP()


BOOL CConnectListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_List.SetExtendedStyle(m_List.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);

	// Initialize list control with some data.
	m_List.InsertColumn(0, _T("Model"), LVCFMT_LEFT, 75);
	m_List.InsertColumn(1, _T("#"), LVCFMT_LEFT, 60);
	m_List.InsertColumn(2, _T("Status"), LVCFMT_LEFT, 70);
	m_List.InsertColumn(3, _T("Interval"), LVCFMT_LEFT, 65);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

BOOL CConnectListDlg::IsExist(CString srtVendor)
{
	int nIndex = m_List.GetItemCount();	
	CSdiSingleMgr* pSDIMgr = NULL;
	while (nIndex--)
	{
		pSDIMgr = (CSdiSingleMgr*)m_List.GetItemData(nIndex);
		if(pSDIMgr->GetDeviceVendor() == srtVendor)
			return TRUE;
	}
	return FALSE;
}


void CConnectListDlg::SetSelectedItem(int nIndex)
{
	m_List.SetFocus();
	//m_List.SetCurSel(nIndex);

	m_List.SetSelectionMark(nIndex);
	m_List.SetItemState(nIndex, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED);	
	m_List.UpdateData(FALSE);
}

int CConnectListDlg::GetSelectedItem()
{
	int nIndex	= m_List.GetItemCount();
	for (int i = 0; i < nIndex	; i++)
	{
		if(m_List.GetItemState(i, LVIS_SELECTED)!=0)
			return i;
	}
	return -1;
}


void CConnectListDlg::SyncConnection(SdiStrArray *pArList)
{
	int nAllList	= pArList->GetCount();
	int nIndex		= m_List.GetItemCount();	
	int nSelected	= GetSelectedItem();

	if(!nIndex)
		return;

	CString srtVendor;
	CSdiSingleMgr* pSDIMgr = NULL;
	while(nIndex--)
	{
		pSDIMgr = (CSdiSingleMgr*)m_List.GetItemData(nIndex);
		srtVendor = pSDIMgr->GetDeviceVendor();

		//-- 2013-02-12 hongsu.jung
		//-- Is Exist
		for(int i = 0 ;i < nAllList ; i ++)
		{
			if(srtVendor == pArList->GetAt(i))
				goto bEXIST;
		}

		//-- 2013-02-13 hongsu.jung
		//-- Remove List
		RemoveList(nIndex);
		//-- Check Selected Item
		if(nSelected == nIndex)
			if(m_List.GetItemCount())
				SetSelectedItem(0);

bEXIST:
		;
	}
}


void CConnectListDlg::RemoveListAll()
{
	int nAll = m_List.GetItemCount();
	CSdiSingleMgr* pSDIMgr = NULL;

	while(nAll--)
	{
		pSDIMgr = (CSdiSingleMgr*)m_List.GetItemData(nAll);
		if(pSDIMgr)
		{
			//-- 2013-02-13 hongsu.jung
			//-- Stop Liveview
			CESMStudioDlg* pMain = (CESMStudioDlg*)GetParent();
			if(pMain)
				pMain->m_pDlgLiveview->Disconnection(pSDIMgr);
		
			//-- 2013-02-13 hongsu.jung
			//-- Remove Manager
			pSDIMgr->StopThread();
		}
	}

	m_List.DeleteAllItems();
}

void CConnectListDlg::RemoveList(int nIndex)
{
	int nAll = m_List.GetItemCount();
	if(nAll < nIndex)
		return;
	
	CSdiSingleMgr* pSDIMgr = NULL;
	pSDIMgr = (CSdiSingleMgr*)m_List.GetItemData(nIndex);
	if(pSDIMgr)
	{
		//-- 2013-02-13 hongsu.jung
		//-- Stop Liveview
		CESMStudioDlg* pMain = (CESMStudioDlg*)GetParent();
		if(pMain)
			pMain->m_pDlgLiveview->Disconnection(pSDIMgr);
		
		//-- 2013-02-13 hongsu.jung
		//-- Remove Manager (Delete Pointer)
		pSDIMgr->StopThread();
	}	

	m_List.DeleteItem(nIndex);	
}

//-- 2013-02-06 hongsu.jung
//-- ADD Connection List
void CConnectListDlg::AddDevice(CSdiSingleMgr* pSDIMgr)
{
	if(!pSDIMgr)
		return;
	int nIndex = m_List.GetItemCount();	

	//-- 2013-02-12 hongsu.jung
	//-- Model Name
	m_List.InsertItem(nIndex, pSDIMgr->GetDeviceModel());
	m_List.SetItemData(nIndex, reinterpret_cast<DWORD_PTR>((CSdiSingleMgr*)pSDIMgr));

	//-- 2013-02-12 hongsu.jung
	//-- Unique Name
	CString strUnique;
	strUnique.Format(_T("%s"),pSDIMgr->GetDeviceUniqueID().Right(5));
	m_List.SetItemText(nIndex, 1, strUnique);//pSDIMgr->GetDeviceUniqueID());

	m_List.SetItemText(nIndex, 2, _T("Unknown"));

	//-- 2013-02-24 hongsu.jung
	//-- Show Status
	m_List.SetItemText(nIndex, 3, pSDIMgr->GetDeviceUniqueID());	

	//-- 2013-02-20 hongsu.jung
	//-- Calculate Delay Time
	CString strInterval;
	strInterval.Format(_T("%d"),GetIntervalTime());	
	m_List.SetItemText(nIndex, 3, strInterval);

	//-- Selected Item
	SetSelectedItem(nIndex);

	//-- 2013-02-13 hongsu.jung
	//-- Set Change Manual Focus
	SdiMessage *pMsg = NULL;

	//-- Set Change Program Mode
	/*
	pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = PTP_CODE_FUNCTIONALMODE;
	pMsg->nParam2 = PTP_VALUE_UINT_16;
	pMsg->pParam = (LPARAM) eUCS_UI_MODE_PROGRAM;
	pSDIMgr->SdiAddMsg(pMsg);
	*/

	//-- 2013-02-20 hongsu.jung
	//-- Check Mode (Program)
	pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_GET_PROPERTY_DESC;
	pMsg->nParam1 = PTP_CODE_FUNCTIONALMODE;
	pSDIMgr->SdiAddMsg(pMsg);
	
	//-- Set Change Manual Focus Or Auto Focus
	pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = PTP_CODE_FOCUSMODE;
	pMsg->nParam2 = PTP_VALUE_UINT_16;
	if(m_bAF)
		pMsg->pParam = (LPARAM) eUCS_CAP_AF_MODE_SAF;
	else
		pMsg->pParam = (LPARAM) eUCS_CAP_AF_MODE_MF;	
	pSDIMgr->SdiAddMsg(pMsg);		

	//-- Set Change 1920X1080
	pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = PTP_CODE_IMAGESIZE;
	pMsg->nParam2 = PTP_VALUE_STRING;
	pMsg->pParam = (LPARAM) eUCS_CAP_PHOTO_SIZE_2M_WIDE;
	pSDIMgr->SdiAddMsg(pMsg);

}

//-- 2013-02-05 hongsu.jung
//-- Set Save Path
void CConnectListDlg::OnBnClickedBtnPath()
{
	// Set Save Movie File Path	
}

//-- 2013-02-05 hongsu.jung
//-- Calculate Interval (All Connected Movie)
void CConnectListDlg::OnBnClickedBtnApply()
{
	CString strInterval;
	strInterval.Format(_T("%d"),GetIntervalTime());


	//-- 2013-02-15 hongsu.jung
	//-- Set Change Interval
	int nAll = m_List.GetItemCount();
	for(int i = 0 ; i < nAll ; i ++)
		m_List.SetItemText(i,2,strInterval);

	m_List.UpdateData(FALSE);
	UpdateData(FALSE);
}

void CConnectListDlg::OnBnClickedBtnRelease1()
{
	BOOL bCapture = TRUE;
	int nIndex = m_List.GetItemCount();
	if(!nIndex)
	{
		AfxMessageBox(_T("Non Connected NX Camera"));
		return;
	}

	CSdiSingleMgr *pSDIMgr = NULL;
	RSEvent* pEvent = NULL;

	if(m_bAF)
	{
		for(int i=0; i < nIndex ; i ++)
		{
			pSDIMgr = (CSdiSingleMgr*)m_List.GetItemData(i);
			if(!pSDIMgr)
				continue;

			//-- S1 Press			
			pEvent = new RSEvent();
			pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4;
			pSDIMgr->AddMsg((CObject*)pEvent);

			//-- S1 Press
			pSDIMgr->SetAFDetect(SDI_AF_NULL);
			UpdateStatus(pSDIMgr, status_focus);			
			pEvent = new RSEvent();
			pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_1;	
			pSDIMgr->AddMsg((CObject*)pEvent);
			
		}

		//-- 2013-02-24 hongsu.jung
		//-- Focus Waiting Time		
		for(int i=0; i < nIndex ; i ++)
		{
			pSDIMgr = (CSdiSingleMgr*)m_List.GetItemData(i);
			if(!pSDIMgr)
				continue;
			//-- Wait Until Focusing
			int nWait = 3000;
			while(nWait--)
			{
				if(pSDIMgr->GetAFDetect() == SDI_AF_DETECT)
					break;	
				Sleep(1);
			}
			if(!nWait)
				bCapture = FALSE;
		}
	}
}

void CConnectListDlg::OnBnClickedBtnRelease2()
{
	BOOL bCapture = TRUE;
	int nIndex = m_List.GetItemCount();
	if(!nIndex)
	{
		AfxMessageBox(_T("Non Connected NX Camera"));
		return;
	}

	CSdiSingleMgr *pSDIMgr = NULL;
	RSEvent* pEvent = NULL;

	for(int i=0; i < nIndex ; i ++)
	{
		pSDIMgr = (CSdiSingleMgr*)m_List.GetItemData(i);
		if(!pSDIMgr)
			continue;
		
		//-- 2013-02-13 hongsu.jung
		//-- MANUAL CAPTURE
		pEvent = new RSEvent();
		pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_2;	
		pSDIMgr->AddMsg((CObject*)pEvent);

		pEvent = new RSEvent();
		pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_3;	
		pSDIMgr->AddMsg((CObject*)pEvent);

		pEvent = new RSEvent();
		pEvent->message = WM_RS_MC_CAPTURE_IMAGE_REQUIRE_4;	
		pSDIMgr->AddMsg((CObject*)pEvent);

		/*
		//-- Check Time
		struct _timeb timebuffer;
		_ftime( &timebuffer );
		TRACE(_T("[%03hu] Send Message \n"),timebuffer.millitm);
		*/
		
		//-- 2013-02-15 hongsu.jung
		//-- Get Interval
		int nInterval = _ttoi(m_List.GetItemText(i,2));
		TRACE(_T("Interval Capture %d\n"),nInterval);
		if(nInterval > 0 && nInterval < 6000)
			Sleep(nInterval);
	}
}

//-- 2013-02-05 hongsu.jung
//-- Record Movie
void CConnectListDlg::OnBnClickedBtnRec()
{
	//-- 2013-02-05 hongsu.jung
	//-- Create Movie Thread
	UpdateData(TRUE);

	//-- Waiting Until Finish Previous Making Movie
	while(m_pMovie)
		Sleep(10);

	//-- Check frame rate 0
	if(m_nFps < 1 || m_nFps > 30)
		m_nFps = 1;

	//-- 2013-02-20 hongsu.jung
	//-- Add Create Array Of String
	CStringArray* pArray = new CStringArray();
	SaveDSCList(pArray);

	m_pMovie = new CTGCreateMovie(m_strPath, m_nFps, pArray, m_b3D);
	if(!m_pMovie)
		return;
	m_pMovie->CreateThread();

	
	//-- 2013-02-15 hongsu.jung
	//-- Capture Each Camera
	OnBnClickedBtnRelease2();
}

CSdiSingleMgr* CConnectListDlg::GetSelectedDevice()
{
	//-- Selected Item
	int nAll		= m_List.GetItemCount();
	int nSelected	= GetSelectedItem();
	if(nSelected < nAll && nSelected > -1)
		return (CSdiSingleMgr*)m_List.GetItemData(nSelected);
	return NULL;
}

void CConnectListDlg::OnLvnItemchangedList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	if(pNMLV ->uNewState == 3)
	{
		CSdiSingleMgr* pSDIMgr = NULL;
		pSDIMgr = (CSdiSingleMgr*)m_List.GetItemData(pNMLV ->iItem);
		if(pSDIMgr)
		{
			//-- 2013-02-13 hongsu.jung
			//-- Stop Liveview
			CESMStudioDlg* pMain = (CESMStudioDlg*)GetParent();
			if(pMain)
				pMain->m_pDlgLiveview->Connection(pSDIMgr);
		}
	}
	*pResult = 0;
}

int CConnectListDlg::GetIntervalTime()
{
	int nInterval = 0;
	UpdateData(TRUE);

	if(m_nFps == 0)
		nInterval = 0;
	else
		nInterval = 1000/m_nFps;
	return nInterval;
}

void CConnectListDlg::SaveDSCList(CStringArray* pArray)
{
	int nIndex = m_List.GetItemCount();
	CSdiSingleMgr *pSDIMgr = NULL;
	CString strFile;
	
	for(int i=0; i < nIndex ; i ++)
	{
		pSDIMgr = (CSdiSingleMgr*)m_List.GetItemData(i);
		if(!pSDIMgr)
			continue;
		CString strUnique;
		strUnique = pSDIMgr->GetDeviceUniqueID().Right(5);
		strFile.Format(_T("%s\\%s.jpg"), m_strPath, strUnique);
		pArray->Add(strFile);
	}
}

void CConnectListDlg::OnBnClickedChkAf()
{
	UpdateData(TRUE);

	//-- 2013-02-13 hongsu.jung
	//-- Set Change Manual Focus
	SdiMessage *pMsg = NULL;
	
	int nIndex = m_List.GetItemCount();
	CSdiSingleMgr *pSDIMgr = NULL;

	for(int i=0; i < nIndex ; i ++)
	{
		pSDIMgr = (CSdiSingleMgr*)m_List.GetItemData(i);
		if(!pSDIMgr)
			continue;

		//-- Set Change Manual Focus Or Auto Focus
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
		pMsg->nParam1 = PTP_CODE_FOCUSMODE;
		pMsg->nParam2 = PTP_VALUE_UINT_16;
		if(m_bAF)
		{
			pMsg->pParam = (LPARAM) eUCS_CAP_AF_MODE_SAF;
			UpdateStatus(pSDIMgr, status_af);
		}
		else
		{
			pMsg->pParam = (LPARAM) eUCS_CAP_AF_MODE_MF;
			UpdateStatus(pSDIMgr, status_mf);
		}
		pSDIMgr->SdiAddMsg(pMsg);		
	}
}

void CConnectListDlg::UpdateStatus(CSdiSingleMgr* pSDIMg, int nStatus)
{
	int nIndex = m_List.GetItemCount();
	CSdiSingleMgr *pExist = NULL;

	for(int i=0; i < nIndex ; i ++)
	{
		pExist = (CSdiSingleMgr*)m_List.GetItemData(i);
		if(pExist == pSDIMg)
		{
			switch(nStatus)
			{
			
			case status_focus		:	m_List.SetItemText(i,2,_T("Wait"));		break;
			case status_focussed	:	m_List.SetItemText(i,2,_T("Focussed"));	break;
			case status_af			:	m_List.SetItemText(i,2,_T("AF"));		break;
			case status_mf			:	m_List.SetItemText(i,2,_T("MF"));		break;
			case status_focussuccess:	m_List.SetItemText(i,2,_T("Success"));	break;
			case status_unknown		:	
			default:					m_List.SetItemText(i,2,_T("Unknown"));	break;
			}
			return;
		}
	}
}

//-- 2013-04-03 yongmmin.lee
//-- Column Click Event 
void CConnectListDlg::OnColumnclick(NMHDR *pNMHDR, LRESULT *pResult)
 {
     NM_LISTVIEW* pNMListView = (NM_LISTVIEW*) pNMHDR;
     int iColumn = pNMListView->iSubItem;
 
     switch(iColumn)
	 {
	 case 1: //Camera ID Sort
		 if(m_bCamIDSort)
		 {
			SortItems(&m_List, iColumn, FALSE, 0, -1);
			m_bCamIDSort = FALSE;
		 }else
		 {
			SortItems(&m_List, iColumn, TRUE, 0, -1);
			m_bCamIDSort = TRUE;
		 }
		 break;
	 }
}

//-- 2013-04-03 yongmmin.lee
//-- SortItems
BOOL CConnectListDlg::SortItems(CTGDragDropListCtrl *pListCtrl, int nCol, BOOL bAscending, int low, int high)
{
    if(nCol >= ((CHeaderCtrl *)pListCtrl->GetDlgItem(0))->GetItemCount()) return FALSE;
    if(high == -1) high=pListCtrl->GetItemCount()-1;

    int lo=low, hi=high;
    CString midItem;
    if(hi <= lo) return FALSE;
    midItem=pListCtrl->GetItemText((lo+hi)/2, nCol);
    while(lo <= hi)
    {
        CStringArray rowText;
        if(bAscending) {while((lo<high) && (pListCtrl->GetItemText(lo, nCol)<midItem)) ++lo;}
        else {while((lo<high) && (pListCtrl->GetItemText(lo, nCol)>midItem)) ++lo;}

        if(bAscending) {while((hi>low) && (pListCtrl->GetItemText(hi, nCol)>midItem)) --hi;}
        else {while((hi>low) && (pListCtrl->GetItemText(hi, nCol)<midItem)) --hi;}

        if(lo <= hi)
        {
            if(pListCtrl->GetItemText(lo, nCol) != pListCtrl->GetItemText(hi, nCol))
            {
                LV_ITEM lvitemlo, lvitemhi;
                int nColCount=((CHeaderCtrl*)pListCtrl->GetDlgItem(0))->GetItemCount();
                rowText.SetSize(nColCount);

                for(int i=0; i<nColCount; i++) rowText[i]=pListCtrl->GetItemText(lo, i);
                lvitemlo.mask=LVIF_IMAGE | LVIF_PARAM | LVIF_STATE;
                lvitemlo.iItem=lo;
                lvitemlo.iSubItem=0;
                lvitemlo.stateMask=LVIS_CUT | LVIS_DROPHILITED | LVIS_FOCUSED |  LVIS_SELECTED | LVIS_OVERLAYMASK | LVIS_STATEIMAGEMASK;

                lvitemhi=lvitemlo;
                lvitemhi.iItem=hi;
                pListCtrl->GetItem(&lvitemlo);
                pListCtrl->GetItem(&lvitemhi);

                for(int i=0; i<nColCount; i++) pListCtrl->SetItemText(lo, i, pListCtrl->GetItemText(hi, i));
                lvitemhi.iItem=lo;
                pListCtrl->SetItem(&lvitemhi);

                for(int i=0; i<nColCount; i++) pListCtrl->SetItemText(hi, i, rowText[i]);
                lvitemlo.iItem = hi;
                pListCtrl->SetItem(&lvitemlo);
            }
            ++lo; --hi;
        }
    }

    if(low < hi) CConnectListDlg::SortItems(pListCtrl, nCol, bAscending , low, hi);
    if(lo < high) CConnectListDlg::SortItems(pListCtrl, nCol, bAscending , lo, high);

    return TRUE;
}