//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : Liveview.cpp
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------


#include "stdafx.h"
#include "Liveview.h"


IMPLEMENT_DYNCREATE(CLiveview, CView)

CLiveview::CLiveview()
:m_pLiveviewBuffer (NULL)
{	
}
CLiveview::~CLiveview(){}

BEGIN_MESSAGE_MAP(CLiveview, CView)	
	ON_WM_PAINT()	
END_MESSAGE_MAP()

// CLiveview diagnostics
#ifdef _DEBUG
void CLiveview::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CLiveview::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG

void CLiveview::OnPaint()
{	
	DrawLiveview();
	CView::OnPaint();
}


//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-19
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveview::DrawLiveview()
{
	//-- GetRect
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);	
	
	//-- Get Liveview	
	if(!m_pLiveviewBuffer || !m_bLiveview)
	{		
		//-- Black Background	
		dc.FillRect(rect, &CBrush(RGB(0,0,0)));		
		return;
	}

	CvvImage cImage;	
	cImage.CopyOf(m_pLiveviewBuffer,8);	
	cImage.Show(dc, rect.left, rect.top, rect.Width(), rect.Height() );
	cImage.Destroy();
}