//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : TGCreateMovie.h
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------

#pragma once

//-- OpenCV
#include "cv.h"
#include "highgui.h"
#include "conio.h"
#include "stdio.h"

// TGCreateMovie

class CTGCreateMovie : public CWinThread
{
	DECLARE_DYNCREATE(CTGCreateMovie)

private:
	CStringArray	m_arSavedPicture;
	CStringArray	m_arDSC;
	BOOL			m_bWrite;			
	BOOL			m_bRun;	
	CvSize			m_MovieSize;

public:
	CTGCreateMovie(void) {};
	CTGCreateMovie(CString strPath, double nFps, CStringArray* pArray, BOOL b3D = FALSE);
	virtual ~CTGCreateMovie();

protected:
	void Init();
	virtual BOOL InitInstance();	
	virtual int Run(void);

private:
	CString m_strFilePath;
	CString m_strPath;
	double  m_nFps;	
	BOOL	m_b3D;

public:
	void StopThread();
	virtual int ExitInstance();

	void AddPicture(CString strFile);
	BOOL CheckFinish(BOOL b3D = FALSE);
	BOOL CheckNextFile(CString strPicture);
	BOOL CheckNextFile(CString strPicture1, CString strPicture2);
	

	BOOL SaveMovie(CvVideoWriter* writer);
	BOOL Save3DMovie(CvVideoWriter* writer);
	IplImage* Create3DPicture(CString strPicture1, CString strPicture2);
};


