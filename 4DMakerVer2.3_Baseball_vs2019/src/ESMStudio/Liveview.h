//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : Liveview.h
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------

#pragma once

#include "cv.h"
#include "highgui.h"

// CLiveview view

class CLiveview : public CView
{
	DECLARE_DYNCREATE(CLiveview)

public:
	CLiveview();	
	virtual ~CLiveview();

public:
	virtual void OnDraw(CDC* pDC) {};      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	
protected:
	DECLARE_MESSAGE_MAP()

public:
	void SetLiveview(BOOL b)			{ m_bLiveview = b; Invalidate(FALSE);	}
	void SetBuffer(PVOID pBuffer)		{ m_pLiveviewBuffer = (IplImage*)pBuffer;	}
	
		
private:	
	BOOL m_bLiveview;		//-- Show or Not		
	IplImage* m_pLiveviewBuffer;	
	afx_msg void OnPaint();	

public:
	//-- 2011-05-16	
	void DrawLiveview();	
};

