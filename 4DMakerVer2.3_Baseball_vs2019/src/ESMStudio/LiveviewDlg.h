//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : LiveviewDlg.h
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------

#pragma once
#include "afxwin.h"

#include "Liveview.h"
#include "SdiSingleMgr.h"


// CLiveviewDlg dialog

class CLiveviewDlg : public CDialog
{
	DECLARE_DYNAMIC(CLiveviewDlg)

public:
	CLiveviewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLiveviewDlg();

enum
{
	LIVEVIEW_ON = 0,
	LIVEVIEW_OFF
};

// Dialog Data
	enum { IDD = IDD_LIVEVIEW};

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()

	void SetPosition();
	CLiveview* m_pLiveview;
	CSdiSingleMgr* m_pLiveSDI;

	CStatic m_staticFocus;
	CButton m_btnNearer;
    CButton m_btnNear;
    CButton m_btnFar;
    CButton m_btnFarther;

public:
	afx_msg void OnBnClickedCapture();	
	afx_msg void OnBnClickedFar();
    afx_msg void OnBnClickedFarther();
    afx_msg void OnBnClickedNear();
    afx_msg void OnBnClickedNearer();

	//-- 2013-02-13 hongsu.jung
	//-- Set Liveview
	void Connection(CSdiSingleMgr* pLiveSDI);
	void Disconnection(CSdiSingleMgr* pLiveSDI);

	CLiveview* GetLiveview()	{ return m_pLiveview; }
	void SetBuffer(PVOID pBuffer); 	

private:
    void SendFocusMsg(ULONG nFocusType);
};
