//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : PropertyDlg.cpp
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------

#include "stdafx.h"
#include "ESMStudio.h"
#include "PropertyDlg.h"
//#include "afxdialogex.h"
#include "SdiSdk.h"
#include "ESMStudioDlg.h"

// CPropertyDlg dialog

IMPLEMENT_DYNAMIC(CPropertyDlg, CDialog)

CPropertyDlg::CPropertyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPropertyDlg::IDD, pParent)
{
	m_pDevMgr = new CDevInfoManager();
}

CPropertyDlg::~CPropertyDlg()
{
	if(m_pDevMgr)
	{
		delete m_pDevMgr;
		m_pDevMgr = NULL;
	}
}

void CPropertyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_SIZE, m_ctrlPhotosize);
	DDX_Control(pDX, IDC_COMBO_QUALITY, m_ctrlQuality);
	DDX_Control(pDX, IDC_COMBO_WB, m_ctrlWB);
	DDX_Control(pDX, IDC_COMBO_PW, m_ctrlPW);
	DDX_Control(pDX, IDC_COMBO_ISO, m_ctrlISO);
	DDX_Control(pDX, IDC_COMBO_SS, m_ctrlSS);
	DDX_Control(pDX, IDC_COMBO_MODE, m_ctrlMode);
}

BEGIN_MESSAGE_MAP(CPropertyDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO_MODE, OnCbnSelchangeComboMode)
	ON_CBN_SELCHANGE(IDC_COMBO_SIZE,	OnCbnSelchangeComboSize)
	ON_CBN_SELCHANGE(IDC_COMBO_QUALITY, OnCbnSelchangeComboQuality)
	ON_CBN_SELCHANGE(IDC_COMBO_WB, OnCbnSelchangeComboWB)
	ON_CBN_SELCHANGE(IDC_COMBO_PW, OnCbnSelchangeComboPW)
	ON_CBN_SELCHANGE(IDC_COMBO_ISO, OnCbnSelchangeComboISO)
	ON_CBN_SELCHANGE(IDC_COMBO_SS, OnCbnSelchangeComboSS)	
END_MESSAGE_MAP()


// CPropertyDlg message handlers
//-- Add Device Infomation
void CPropertyDlg::AddDevInfo(IDeviceInfo *pDevInfo)
{
	m_pDevMgr->SetDevInfo(pDevInfo);
}

//-- Update Combo Values
void CPropertyDlg::UpdateCombo(int nPTPCode)
{
	IDeviceInfo* pDevInfo = m_pDevMgr->GetDevInfo(nPTPCode);

	int nEnumValue = 0;
	CString strValue = _T("");
	int nIndex = 0;
	int nPropCode = pDevInfo->GetPropCode();
	switch(nPropCode)
	{
	//-- Production Mode
	case SDI_DPC_FUNCTIONALMODE:
		{
			CString strEnumValue = _T("");
			m_ctrlMode.ResetContent();
			if(pDevInfo->GetPropValueCntEnum())
			{
				for(int i=0; i<pDevInfo->GetPropValueCntEnum(); i++)
				{
					nEnumValue = pDevInfo->GetPropValueListEnum()->GetAt(i);	
					strEnumValue.Format(_T("%x"),nEnumValue);
					m_ctrlMode.AddString(strEnumValue);
				}			
			}
			else
			{
				nEnumValue = pDevInfo->GetCurrentEnum();
				strEnumValue.Format(_T("%x"),nEnumValue);				
				m_ctrlMode.AddString(strEnumValue);
			}
			nIndex = m_pDevMgr->GetCurrentValueIndex(nPTPCode);
			m_ctrlMode.SetCurSel(nIndex);	
		}
		break;

	//-- Imagesize
	case SDI_DPC_IMAGESIZE:
		m_ctrlPhotosize.ResetContent();

		if(pDevInfo->GetPropValueCntStr())
		{
			for(int i=0; i<pDevInfo->GetPropValueCntStr(); i++)
			{
				strValue = pDevInfo->GetPropValueListStr()->GetAt(i);
				//-- 2013-02-05 hongsu.jung
				//-- CString strDesc = GetPropIndexToPropValueStr(SDI_DPC_IMAGESIZE);
				m_ctrlPhotosize.AddString(strValue);
			}
		}
		else
		{
			strValue = pDevInfo->GetCurrentStr();
			m_ctrlPhotosize.AddString(strValue);
		}
		nIndex = m_pDevMgr->GetCurrentValueIndex(nPTPCode);
		m_ctrlPhotosize.SetCurSel(nIndex);
		break;

	//-- Quality
	case SDI_DPC_COMPRESSIONSETTING:
		{
			CString strEnumValue = _T("");
			m_ctrlQuality.ResetContent();
			if(pDevInfo->GetPropValueCntEnum())
			{
				for(int i=0; i<pDevInfo->GetPropValueCntEnum(); i++)
				{
					nEnumValue = pDevInfo->GetPropValueListEnum()->GetAt(i);	
					strEnumValue.Format(_T("%x"),nEnumValue);
					m_ctrlQuality.AddString(strEnumValue);
				}			
			}
			else
			{
				nEnumValue = pDevInfo->GetCurrentEnum();
				strEnumValue.Format(_T("%x"),nEnumValue);				
				m_ctrlQuality.AddString(strEnumValue);
			}
			nIndex = m_pDevMgr->GetCurrentValueIndex(nPTPCode);
			m_ctrlQuality.SetCurSel(nIndex);	
		}			
 		break;

	//-- White Balance
	case SDI_DPC_WHITEBALANCE:
		{
			CString strEnumValue = _T("");
			m_ctrlWB.ResetContent();
			if(pDevInfo->GetPropValueCntEnum())
			{
				for(int i=0; i<pDevInfo->GetPropValueCntEnum(); i++)
				{
					nEnumValue = pDevInfo->GetPropValueListEnum()->GetAt(i);	
					strEnumValue.Format(_T("%x"),nEnumValue);
					m_ctrlWB.AddString(strEnumValue);
				}			
			}
			else
			{
				nEnumValue = pDevInfo->GetCurrentEnum();
				strEnumValue.Format(_T("%x"),nEnumValue);				
				m_ctrlWB.AddString(strEnumValue);
			}
			nIndex = m_pDevMgr->GetCurrentValueIndex(nPTPCode);
			m_ctrlWB.SetCurSel(nIndex);	
		}	
		break;

	//-- Picture Wizard
	case SDI_DPC_SAMSUNG_PICTUREWIZARD:
		{
			CString strEnumValue = _T("");
			m_ctrlPW.ResetContent();
			if(pDevInfo->GetPropValueCntEnum())
			{
				for(int i=0; i<pDevInfo->GetPropValueCntEnum(); i++)
				{
					nEnumValue = pDevInfo->GetPropValueListEnum()->GetAt(i);	
					strEnumValue.Format(_T("%x"),nEnumValue);
					m_ctrlPW.AddString(strEnumValue);
				}			
			}
			else
			{
				nEnumValue = pDevInfo->GetCurrentEnum();
				strEnumValue.Format(_T("%x"),nEnumValue);				
				m_ctrlPW.AddString(strEnumValue);
			}
			nIndex = m_pDevMgr->GetCurrentValueIndex(nPTPCode);
			m_ctrlPW.SetCurSel(nIndex);	
		}	
		break;

	//-- ISO
	case SDI_DPC_EXPOSUREINDEX:
		{
			CString strEnumValue = _T("");
			m_ctrlISO.ResetContent();
			if(pDevInfo->GetPropValueCntEnum())
			{
				for(int i=0; i<pDevInfo->GetPropValueCntEnum(); i++)
				{
					nEnumValue = pDevInfo->GetPropValueListEnum()->GetAt(i);	
					strEnumValue.Format(_T("%x"),nEnumValue);
					m_ctrlISO.AddString(strEnumValue);
				}			
			}
			else
			{
				nEnumValue = pDevInfo->GetCurrentEnum();
				strEnumValue.Format(_T("%x"),nEnumValue);				
				m_ctrlISO.AddString(strEnumValue);
			}
			nIndex = m_pDevMgr->GetCurrentValueIndex(nPTPCode);
			m_ctrlISO.SetCurSel(nIndex);	
		}
		break;

	//-- Shutter Speed
	case SDI_DPC_SAMSUNG_SHUTTERSPEED:
		{
			CString strEnumValue = _T("");
			m_ctrlSS.ResetContent();
			if(pDevInfo->GetPropValueCntEnum())
			{
				for(int i=0; i<pDevInfo->GetPropValueCntEnum(); i++)
				{
					nEnumValue = pDevInfo->GetPropValueListEnum()->GetAt(i);	
					strEnumValue.Format(_T("%x"),nEnumValue);
					m_ctrlSS.AddString(strEnumValue);
				}			
			}
			else
			{
				nEnumValue = pDevInfo->GetCurrentEnum();
				strEnumValue.Format(_T("%x"),nEnumValue);				
				m_ctrlSS.AddString(strEnumValue);
			}
			nIndex = m_pDevMgr->GetCurrentValueIndex(nPTPCode);
			m_ctrlSS.SetCurSel(nIndex);	
		}
		break;
	default:
		TRACE("What's That? [%d]\n",nPropCode);
		break;
	}	
}

//--Photosize Event Handler
void CPropertyDlg::OnCbnSelchangeComboSize()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_IMAGESIZE;
	pMsg->nParam2 = m_ctrlPhotosize.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

	
//-- Quality Event Handler
void CPropertyDlg::OnCbnSelchangeComboQuality()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_COMPRESSIONSETTING;
	pMsg->nParam2 = m_ctrlQuality.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

//-- White Balance Event Handler
void CPropertyDlg::OnCbnSelchangeComboWB()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_WHITEBALANCE;
	pMsg->nParam2 = m_ctrlWB.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

//-- Picture Wizard
void CPropertyDlg::OnCbnSelchangeComboPW()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_SAMSUNG_PICTUREWIZARD;
	pMsg->nParam2 = m_ctrlPW.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

//--ISO
void CPropertyDlg::OnCbnSelchangeComboISO()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_EXPOSUREINDEX;
	pMsg->nParam2 = m_ctrlISO.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

//--Shutter Speed
void CPropertyDlg::OnCbnSelchangeComboSS()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_SAMSUNG_SHUTTERSPEED;
	pMsg->nParam2 = m_ctrlSS.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

//-- Production Mode
void CPropertyDlg::OnCbnSelchangeComboMode()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_FUNCTIONALMODE;
	pMsg->nParam2 = m_ctrlMode.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

//-- Proprety Index To Value String
CString CPropertyDlg::GetPropIndexToPropValueStr(int nPTPCode)//, int nPropIndex)
{
	IDeviceInfo* pDevInfo = m_pDevMgr->GetDevInfo(nPTPCode);
	CString strDesc = _T("");
	switch(nPTPCode)
	{
	case SDI_DPC_IMAGESIZE:
		for(int i=0; i<pDevInfo->GetPropValueCntStr(); i++)
		{
			if(pDevInfo->GetPropValueListStr()->GetAt(i)==_T("5742x3648"))
				strDesc = _T("20M");
			else if(pDevInfo->GetPropValueListStr()->GetAt(i)==_T("4896x3264"))
				strDesc = _T("16M");		
			//-- 2013-02-05 hongsu.jung
			//-- NotYet
		}		
		break;
	}
	
	return strDesc;
}

 
