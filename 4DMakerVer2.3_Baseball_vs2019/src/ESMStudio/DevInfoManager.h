//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : DevInfoManager.h
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------

#pragma once
#include "SdiDefines.h"

// DevInfoManager

class CDevInfoManager
{
public:
	CDevInfoManager();
	virtual ~CDevInfoManager();

private:
	CObArray m_arPropInfo;	

public:
	void RemoveAll();
	void SetDevInfo(IDeviceInfo* devInfo);
	IDeviceInfo* GetDevInfo(int index);
	int GetCurrentValueIndex(int nPTPCode);
	int GetCurrentValueEnum(int nPTPCode, int nPropValueIndex);
};


