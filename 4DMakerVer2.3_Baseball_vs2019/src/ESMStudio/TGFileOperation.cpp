////////////////////////////////////////////////////////////////////////////////
//
//	TGFileOperation.cpp : implementation of the TestGuarantee Function Class.
//
//  ESMLab, Inc. PROPRIETARY INFORMATION.
//  The following contains information proprietary to ESMLab, Inc. and may not be copied
//  nor disclosed except upon written agreement by ESMLab, Inc.
//
//  Copyright (C) 2012 ESMLab, Inc. All rights reserved.
//
// @author	Hongsu Jung (hongsu@esmlab.com)
// @Date	2012-04-04
//
////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h" 
#include "TGFileOperation.h" 
#include "TGDirRead.h"

#pragma warning(disable:4996)


//--**********************************************************************************************************
CFExeption::CFExeption(DWORD dwErrCode)
{
	LPVOID lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			      NULL, dwErrCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
	m_sError = (LPTSTR)lpMsgBuf;
	LocalFree(lpMsgBuf);
	m_dwError = dwErrCode;
}


CFExeption::CFExeption(CString sErrText)
{
	m_sError = sErrText;
	m_dwError = 0;
}


//--**********************************************************************************************************

CTGFileOperation::CTGFileOperation()
{
	Initialize();
}


void CTGFileOperation::Initialize()
{
	m_sError = _T("No error");
	m_dwError = 0;
	m_bAskIfReadOnly = TRUE;
	m_bOverwriteMode = FALSE;
	m_bAborted = FALSE;
	m_iRecursionLimit = -1;
}


void CTGFileOperation::DoDelete(CString sPathName)
{
	CFileFind ff;
	CString sPath = sPathName;

	if (CheckPath(sPath) == PATH_IS_FILE)
	{
		if (!CanDelete(sPath)) 
		{
			m_bAborted = TRUE;
			return;
		}
		if (!DeleteFile(sPath)) 
			throw new CFExeption(GetLastError());
		return;
	}

	PreparePath(sPath);
	sPath += "*.*";

	BOOL bRes = ff.FindFile(sPath);
	while(bRes)
	{
		bRes = ff.FindNextFile();
		if (ff.IsDots()) 
			continue;
		if (ff.IsDirectory())
		{
			sPath = ff.GetFilePath();
			DoDelete(sPath);
		}
		else
			DoDelete(ff.GetFilePath());
	}
	ff.Close();
	if (!RemoveDirectory(sPathName) && !m_bAborted) 
		throw new CFExeption(GetLastError());
}


void CTGFileOperation::DoFolderCopy(CString sSourceFolder, CString sDestFolder, BOOL bDelteAfterCopy, BOOL bCreateFolder)
{
	CFileFind ff;
	CString sPathSource = sSourceFolder;
	BOOL bRes = ff.FindFile(sPathSource);
	while (bRes)
	{
		bRes = ff.FindNextFile();
		if (ff.IsDots()) continue;
		if (ff.IsDirectory()) // source is a folder
		{
			if (m_iRecursionLimit == 0) 
				continue;
			sPathSource = ff.GetFilePath() + CString("\\") + CString("*.*");
			CString sPathDest = sDestFolder + ff.GetFileName() + CString("\\");
			
			//-- 2008-07-25
			if(bCreateFolder)
			{
				if (CheckPath(sPathDest) == PATH_NOT_FOUND) 
				{
					if (!CreateDirectory(sPathDest, NULL))
					{
						ff.Close();
						throw new CFExeption(GetLastError());
					}
				}
			}

			if (m_iRecursionLimit > 0) 
				m_iRecursionLimit --;			
			DoFolderCopy(sPathSource, sPathDest, bDelteAfterCopy);
		}
		else // source is a file
		{
			CString sNewFileName = sDestFolder + ff.GetFileName();
			DoFileCopy(ff.GetFilePath(), sNewFileName, bDelteAfterCopy);
		}
	}
	ff.Close();
}


BOOL CTGFileOperation::Delete(CString sPathName)
{
	try
	{
		DoDelete(sPathName);
	}
	catch(CFExeption* e)
	{
		m_sError = e->GetErrorText();
		m_dwError = e->GetErrorCode();
		delete e;
		if (m_dwError == 0) return TRUE;
		return FALSE;
	}
	return TRUE;
}


BOOL CTGFileOperation::Rename(CString sSource, CString sDest)
{
	try
	{
		DoRename(sSource, sDest);
	}
	catch(CFExeption* e)
	{
		m_sError = e->GetErrorText();
		m_dwError = e->GetErrorCode();
		delete e;
		return FALSE;
	}
	return TRUE;
}


void CTGFileOperation::DoRename(CString sSource, CString sDest)
{
	if (!MoveFile(sSource, sDest)) 
		throw new CFExeption(GetLastError());
}

void CTGFileOperation::CreateFolder(CString strDestDir)
{
	CStringArray arParse;
	CString strNowDir, strCreateDir;
	strNowDir = strDestDir;
	DWORD dwAttr;

	while(1)
	{
		dwAttr = GetFileAttributes(strNowDir);
		if (dwAttr == 0xffffffff) 
		{
			CString sFolderName = strNowDir;
			int pos = sFolderName.ReverseFind('\\');
			if (pos != -1) 
			{
				sFolderName = sFolderName.Right(sFolderName.GetLength() - pos - 1);
				strNowDir .Delete(pos, strNowDir .GetLength() - pos);
			}							
			arParse.Add(sFolderName);			
		}
		else
			break;
	}

	//-- CREATE NEW DIRECTORY
	int nAll = (int)arParse.GetCount ();
	while(nAll--)
	{
		strCreateDir = arParse.GetAt(nAll);
		strNowDir.Format(_T("%s\\%s"),strNowDir, strCreateDir);
		::CreateDirectory (strNowDir, NULL);
	}

	//-- 2012-05-22 hongsu
	nAll = (int)arParse.GetSize();
	while(nAll--)
		arParse.RemoveAt(nAll);
	arParse.RemoveAll();
}

void CTGFileOperation::DoCopy(CString sSource, CString sDest, BOOL bDelteAfterCopy, BOOL bCreateFolder)
{
	CheckSelfRecursion(sSource, sDest);
	// source not found
	if (CheckPath(sSource) == PATH_NOT_FOUND)
	{
		CString sError = sSource + CString(" not found");
		throw new CFExeption(sError);
	}
	// dest not found
	if (CheckPath(sDest) == PATH_NOT_FOUND)
	{
		CString sError = sDest + CString(" not found");
		//-- 2008-06-19
		//throw new CFExeption(sError);
		//-- 2008-07-24
		
		//-- 2008-08-11
		//-- CHECK FILE OR FOLDER
		CString strTemp;
		strTemp = sDest;
		int nPath = strTemp.ReverseFind ('\\');
		int nFile = strTemp.ReverseFind ('.');
		if(nPath > nFile)
			CreateFolder(sDest);
	}
	// folder to file
	if (CheckPath(sSource) == PATH_IS_FOLDER && CheckPath(sDest) == PATH_IS_FILE) 
	{
		throw new CFExeption(_T("Wrong operation"));
	}
	// folder to folder
	if (CheckPath(sSource) == PATH_IS_FOLDER && CheckPath(sDest) == PATH_IS_FOLDER) 
	{
		CFileFind ff;
		CString sError = sSource + CString(" not found");
		PreparePath(sSource);
		PreparePath(sDest);
		sSource += "*.*";
		if (!ff.FindFile(sSource)) 
		{
			ff.Close();
			throw new CFExeption(sError);
		}
		if (!ff.FindNextFile()) 
		{
			ff.Close();
			throw new CFExeption(sError);
		}
		//-- 2008-07-25
		if(bCreateFolder)
		{
			CString sFolderName = ParseFolderName(sSource);		
			if (!sFolderName.IsEmpty()) // the source is not drive
			{
				sDest += sFolderName;
				PreparePath(sDest);
				if (!CreateDirectory(sDest, NULL))
				{
					DWORD dwErr = GetLastError();
					if (dwErr != 183)
					{
						ff.Close();
						throw new CFExeption(dwErr);
					}
				}
			}
		}

		ff.Close();
		DoFolderCopy(sSource, sDest, bDelteAfterCopy, bCreateFolder);
	}
	// file to file
	//int nTest =  CheckPath(sDest);
	//int nTest2 = CheckPath(sSource);

	if (CheckPath(sSource) == PATH_IS_FILE && 
		( CheckPath(sDest) == PATH_IS_FILE || CheckPath(sDest) == PATH_NOT_FOUND) ) 
	{
		DoFileCopy(sSource, sDest);
	}
	// file to folder
	if (CheckPath(sSource) == PATH_IS_FILE &&  CheckPath(sDest) == PATH_IS_FOLDER ) 
	{
		PreparePath(sDest);
		//char drive[MAX_PATH], dir[MAX_PATH], name[MAX_PATH], ext[MAX_PATH];
		//_splitpath(sSource, drive, dir, name, ext);
		TCHAR drive[MAX_PATH], dir[MAX_PATH], name[MAX_PATH], ext[MAX_PATH];
		_tsplitpath(sSource, drive, dir, name, ext);
		sDest = sDest + CString(name) + CString(ext);
		DoFileCopy(sSource, sDest);
	}
}


void CTGFileOperation::DoFileCopy(CString sSourceFile, CString sDestFile, BOOL bDelteAfterCopy)
{
	BOOL bOvrwriteFails = FALSE;
	if (!m_bOverwriteMode)
	{
		while (IsFileExist(sDestFile)) 
		{
			sDestFile = ChangeFileName(sDestFile);
		}
		bOvrwriteFails = TRUE;
	}
	
	if (!CopyFile(sSourceFile, sDestFile, bOvrwriteFails)) 
		throw new CFExeption(GetLastError());

	if (bDelteAfterCopy)
	{
		DoDelete(sSourceFile);
	}
}


BOOL CTGFileOperation::Copy(CString sSource, CString sDest, BOOL bCreateFolder)
{
	if (CheckSelfCopy(sSource, sDest)) return TRUE;
	BOOL bRes;
	try
	{
		DoCopy(sSource, sDest, FALSE, bCreateFolder);
		bRes = TRUE;
	}
	catch(CFExeption* e)
	{
		m_sError = e->GetErrorText();
		m_dwError = e->GetErrorCode();
		delete e;
		if (m_dwError == 0) bRes = TRUE;
		bRes = FALSE;
	}
	m_iRecursionLimit = -1;
	return bRes;
}


BOOL CTGFileOperation::Replace(CString sSource, CString sDest)
{
	if (CheckSelfCopy(sSource, sDest)) return TRUE;
	BOOL bRes;
	try
	{
		BOOL b = m_bAskIfReadOnly;
		m_bAskIfReadOnly = FALSE;
		DoCopy(sSource, sDest, TRUE);
		DoDelete(sSource);
		m_bAskIfReadOnly = b;
		bRes = TRUE;
	}
	catch(CFExeption* e)
	{
		m_sError = e->GetErrorText();
		m_dwError = e->GetErrorCode();
		delete e;
		if (m_dwError == 0) bRes = TRUE;
		bRes = FALSE;
	}
	m_iRecursionLimit = -1;
	return bRes;
}


CString CTGFileOperation::ChangeFileName(CString sFileName)
{
	CString sName, sNewName, sResult;
	//char drive[MAX_PATH];
	//char dir  [MAX_PATH];
	//char name [MAX_PATH];
	//char ext  [MAX_PATH];
	TCHAR drive[MAX_PATH];
	TCHAR dir  [MAX_PATH];
	TCHAR name [MAX_PATH];
	TCHAR ext  [MAX_PATH];

	_tsplitpath((LPCTSTR)sFileName, drive, dir, name, ext);
	sName = name;

	int pos = sName.Find(_T("Copy "));
	if (pos == -1)
	{
		sNewName = CString("Copy of ") + sName + CString(ext);
	}
	else
	{
		int pos1 = sName.Find('(');
		if (pos1 == -1)
		{
			sNewName = sName;
			sNewName.Delete(0, 8);
			sNewName = CString("Copy (1) of ") + sNewName + CString(ext);
		}
		else
		{
			CString sCount;
			int pos2 = sName.Find(')');
			if (pos2 == -1)
			{
				sNewName = CString("Copy of ") + sNewName + CString(ext);
			}
			else
			{
				sCount = sName.Mid(pos1 + 1, pos2 - pos1 - 1);
				sName.Delete(0, pos2 + 5);
				int iCount = _ttoi((LPCTSTR)sCount);
				iCount ++;
				sNewName.Format(_T("%s%d%s%s%s"), "Copy (", iCount, ") of ", (LPCTSTR)sName, ext);
			}
		}
	}

	sResult = CString(drive) + CString(dir) + sNewName;

	return sResult;
}


BOOL CTGFileOperation::IsFileExist(CString sPathName)
{
	HANDLE hFile;
	hFile = CreateFile(sPathName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, NULL, NULL);
	if (hFile == INVALID_HANDLE_VALUE) 
		return FALSE;
	CloseHandle(hFile);
	return TRUE;
}

BOOL CTGFileOperation::IsFileFolder(CString sPath)
{
	DWORD dwAttr = GetFileAttributes(sPath);
	if (dwAttr == 0xffffffff) 
		return FALSE;
	if (dwAttr & FILE_ATTRIBUTE_DIRECTORY)
		return TRUE;
	return FALSE;
}


int CTGFileOperation::CheckPath(CString sPath)
{
	DWORD dwAttr = GetFileAttributes(sPath);
	if (dwAttr == 0xffffffff) 
	{
		if (GetLastError() == ERROR_FILE_NOT_FOUND || GetLastError() == ERROR_PATH_NOT_FOUND) 
			return PATH_NOT_FOUND;
		return PATH_ERROR;
	}
	if (dwAttr & FILE_ATTRIBUTE_DIRECTORY)
		return PATH_IS_FOLDER;
	return PATH_IS_FILE;
}


void CTGFileOperation::PreparePath(CString &sPath)
{
	if(sPath.Right(1) != "\\") sPath += "\\";
}


BOOL CTGFileOperation::CanDelete(CString sPathName)
{
	DWORD dwAttr = GetFileAttributes(sPathName);
	if (dwAttr == -1) 
		return FALSE;
	if (dwAttr & FILE_ATTRIBUTE_READONLY)
	{
		if (m_bAskIfReadOnly)
		{
			CString sTmp = sPathName;
			int pos = sTmp.ReverseFind('\\');
			if (pos != -1) sTmp.Delete(0, pos + 1);
			CString sText = sTmp + CString(" is read only. Do you want delete it?");
			int iRes = MessageBox(NULL, sText, _T("Warning"), MB_YESNOCANCEL | MB_ICONQUESTION);
			switch (iRes)
			{
				case IDYES:
				{
					if (!SetFileAttributes(sPathName, FILE_ATTRIBUTE_NORMAL)) return FALSE;
					return TRUE;
				}
				case IDNO:
				{
					return FALSE;
				}
				case IDCANCEL:
				{
					m_bAborted = TRUE;
					throw new CFExeption(0);
					return FALSE;
				}
			}
		}
		else
		{
			if (!SetFileAttributes(sPathName, FILE_ATTRIBUTE_NORMAL)) return FALSE;
			return TRUE;
		}
	}
	return TRUE;
}


CString CTGFileOperation::ParseFolderName(CString sPathName)
{
	CString sFolderName = sPathName;
	int pos = sFolderName.ReverseFind('\\');
	if (pos != -1) 
		sFolderName.Delete(pos, sFolderName.GetLength() - pos);
	pos = sFolderName.ReverseFind('\\');
	if (pos != -1) 
		sFolderName = sFolderName.Right(sFolderName.GetLength() - pos - 1);
	else sFolderName.Empty();
	return sFolderName;
}


void CTGFileOperation::CheckSelfRecursion(CString sSource, CString sDest)
{
	if (sDest.Find(sSource) != -1)
	{
		int i = 0, count1 = 0, count2 = 0;
		for(i = 0; i < sSource.GetLength(); i ++)	if (sSource[i] == '\\') count1 ++;
		for(i = 0; i < sDest.GetLength(); i ++)	if (sDest[i] == '\\') count2 ++;
		if (count2 >= count1) m_iRecursionLimit = count2 - count1;
	}
}


BOOL CTGFileOperation::CheckSelfCopy(CString sSource, CString sDest)
{
	BOOL bRes = FALSE;
	if (CheckPath(sSource) == PATH_IS_FOLDER)
	{
		CString sTmp = sSource;
		int pos = sTmp.ReverseFind('\\');
		if (pos != -1)
		{
			sTmp.Delete(pos, sTmp.GetLength() - pos);
			if (sTmp.CompareNoCase(sDest) == 0) bRes = TRUE;
		}
	}
	return bRes;
}

//-- 2012-06-20 hongsu@esmlab.com
//-- Get File with include string
CString CTGFileOperation::SearchFile(CString strFolder, CString strFile, CString strInclude)
{
	CString strFileName;

	CTGDirRead dr;
	//dr.Recurse() = true;		// scan subdirs ?
	dr.ClearDirs();				// start clean
	dr.GetDirs(strFolder, true);	// get all folders under c:\temp

	// get the dir array
	CTGDirRead::DirVector &dirs = dr.Dirs();

	dr.ClearFiles();        // start clean
	dr.GetFiles(strFile);   // get all *.JPG files in c:\temp and below
	// get the file array
	CTGDirRead::FileVector &files = dr.Files();   

	// dump it...
	CString strLine;
	for (CTGDirRead::FileVector::const_iterator fit = files.begin(); fit!=files.end(); fit++)
	{
		strFileName.Format(_T("%s"),fit->m_sName);

		//-- Search Line
		strLine = SearchLine(strFileName,strInclude);
		if(strLine.GetLength())
			break;
	}

	dirs.clear();
	files.clear();	

	return strFileName;
}

CString CTGFileOperation::SearchLine(CString strFile, CString strInclude)
{
	CString strLine;
	CStdioFile file;

	if(!file.Open(strFile, CFile::modeRead))
		return _T("");

	while(file.ReadString(strLine) != NULL)
	{
		if(strLine.Find(strInclude) == 0)
			return strLine;
	}

	return _T("");
}


//-- 2013-02-15 hongsu.jung
//-- Get File List By File Date
int CTGFileOperation::GetFileList(CString strFolder, CStringArray* pFileList)
{
	CString strFileName;
	if(!pFileList)
		return -1;
	pFileList->RemoveAll();

	CTGDirRead dr;
	//dr.Recurse() = true;		// scan subdirs ?
	dr.ClearDirs();				// start clean
	dr.GetDirs(strFolder, true);	// get all folders under c:\temp

	// get the dir array
	CTGDirRead::DirVector &dirs = dr.Dirs();

	dr.ClearFiles();			// start clean
	dr.GetFiles(_T("*.JPG"));		// get all *.JPG files in c:\temp and below
	dr.SortFiles(CTGDirRead::eSortWriteDate,false);
	// get the file array
	CTGDirRead::FileVector &files = dr.Files();   

	for (CTGDirRead::FileVector::const_iterator fit = files.begin(); fit!=files.end(); fit++)
	{
		strFileName.Format(_T("%s"),fit->m_sName);
		pFileList->Add(strFileName);
	}

	dirs.clear();
	files.clear();	

	return pFileList->GetCount();
}