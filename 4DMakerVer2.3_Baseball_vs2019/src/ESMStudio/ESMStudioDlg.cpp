//----------------------------------------------------------------------------
//--
//-- Project : ESM Studio
//--
//-- Copyright 2013 by ESMLab, Inc.
//-- All rights reserved.
//-- 
//-- Project Description :
//--		Make Matrix Movie File using NX Camera 
//--
//--
//-- file : ESMStudioDlg.cpp
//-- author : hongsu.jung (hongsu@esmlab.com)
//-- date : 2013-02-05
//-- version 1.0
//--
//----------------------------------------------------------------------------


#include "stdafx.h"
#include "ESMStudio.h"
#include "ESMStudioDlg.h"
#include "AfxDialogEX.h"
#include "SdiSdk.h"
#include "SdiSingleMgr.h"

//TimeTrace
#include <sys/timeb.h>
#include <time.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{

}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CESMStudioDlg dialog

CESMStudioDlg::CESMStudioDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CESMStudioDlg::IDD, pParent)
	,	m_pDlgLiveview	(NULL)	
	,	m_pDlgConnect	(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);	
}

CESMStudioDlg::~CESMStudioDlg(){}
void CESMStudioDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CESMStudioDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_SDI			, OnSDIMsg)
	ON_MESSAGE(WM_ESMSTUDIO		, OnDialogMsg)	
	ON_MESSAGE(WM_DEVICECHANGE	, OnChangeConnect)
END_MESSAGE_MAP()



// CESMStudioDlg message handlers
void CESMStudioDlg::OnClose()
{
	if(m_pDlgConnect)
	{
		delete m_pDlgConnect;
		m_pDlgConnect = NULL;
	}

	if(m_pDlgLiveview)
	{
		delete m_pDlgLiveview;
		m_pDlgLiveview = NULL;
	}
	CDialogEx::OnClose();
}

BOOL CESMStudioDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	this->MoveWindow(0,0,890,500);

	m_pDlgLiveview = new CLiveviewDlg();
	m_pDlgLiveview->Create(CLiveviewDlg::IDD, this);
	m_pDlgLiveview->MoveWindow(0,0,620,500);

	m_pDlgConnect = new CConnectListDlg();
	m_pDlgConnect->Create(CConnectListDlg::IDD, this);
	m_pDlgConnect->MoveWindow(620,0,270,530);

	CheckConnect();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CESMStudioDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
void CESMStudioDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CESMStudioDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


LRESULT CESMStudioDlg::OnChangeConnect(WPARAM wParam, LPARAM lParam)
{	
	return CheckConnect();
}


SdiResult CESMStudioDlg::CheckConnect()
{
	if(!m_pDlgConnect)
		return SDI_ERR_OK;

	//-- 2013-02-12 hongsu.jung
	//-- Get Connection List
	SdiStrArray arList;
	CSdiSdk::SdiGetDeviceHandleList(&arList);
	
	//-- 2013-02-12 hongsu.jung
	//-- Sync Connection With Connection List
	m_pDlgConnect->SyncConnection(&arList);

	SdiString strConnect;
	//-- 2013-02-06 hongsu.jung
	//-- Get Connected List
	int nAll = arList.GetSize();
	if(!nAll)
		return SDI_ERR_OK;

	//-- 2013-02-06 hongsu.jung
	//-- Single Manager
	CSdiSingleMgr* pSDIMgr = NULL;
	
	while(nAll--)	
	{
		//-- 2013-02-13 hongsu.jung
		//-- Wait Create Thread
		Sleep(1);

		strConnect = arList.GetAt(nAll);

		//-- 2013-02-12 hongsu.jung
		//-- Check Exist
		if(m_pDlgConnect->IsExist(strConnect))
			continue;

		pSDIMgr = new CSdiSingleMgr(nAll, strConnect, _T(""));
		pSDIMgr->SdiSetReceiveHandler(m_hWnd);
		if(!pSDIMgr)
			return NULL;	

		//-- 2011-05-24 hongsu.jung
		//-- Init Session
		int nRet = pSDIMgr->PTP_InitSession(strConnect);
		if(nRet < 1)
		{
			AfxMessageBox(_T("PTP_INITIAL ERROR"));
			return NULL;
		}
	
		//-- 2011-05-24 hongsu.jung
		//-- Open Session
		TRACE(_T("[MTP] PTP_OpenSession \n"));
		pSDIMgr->PTP_OpenSession();

		//-- 2011-06-25 hongsu.jung
		//-- Get Unique ID
		TRACE(_T("[PTP] PTP_GetDevUniqueID \n"));
		if(pSDIMgr->PTP_GetDevUniqueID() < 0)
		{
			TRACE(_T("[ERROR]\n"));
			continue;
		}

		//-- 2011-05-24 hongsu.jung
		//-- Create Thread
		pSDIMgr->CreateThread();

		//-- 2013-02-06 hongsu.jung
		//-- Add To Connect List with All ID		
		m_pDlgConnect->AddDevice(pSDIMgr);
	}	

	arList.RemoveAll();
	return SDI_ERR_OK;
}

LRESULT CESMStudioDlg::OnDialogMsg(WPARAM wParam, LPARAM lParam)
{
	SdiMessage* pMsg = NULL;
	pMsg = (SdiMessage*)lParam;
	switch(int(wParam))
	{
	case WM_SAMPLE_OP_CAPTURE:
		OnCapture();
		break;
    case WM_SAMPLE_OP_SET_FOCUSPOSITION:
        OnSetFocus(pMsg->nParam1);
        break;
	case WM_SAMPLE_OP_REMOVE_MOVIEMAKER:
		m_pDlgConnect->m_pMovie = NULL;
		TRACE(_T("====== MOVIE THREAD NULL ======\n"));
		break;
	default:
		break;
	}

	return 0;
}

// SDK Command
LRESULT CESMStudioDlg::OnSDIMsg(WPARAM wParam, LPARAM lParam)
{
	/*
	{
		//-- TEST	
		struct _timeb timebuffer;
		_ftime( &timebuffer );
		TRACE(_T("[%03hu] Get Message [%d]\n"),timebuffer.millitm,int(wParam));
	}
	*/	
	
	SdiMessage* pMsg = NULL;
	SdiMessage* pMessage = NULL;
	pMsg = (SdiMessage*)lParam;
	switch(int(wParam))
	{
	case WM_SDI_EVENT_CAPTURE_DONE:
		OnCaptureDone(pMsg);
		break;
	case WM_SDI_EVENT_SAVE_DONE:
		OnSaveDone(pMsg);
		break;
	case WM_SDI_RESULT_GET_PROPERTY_DESC:
		CheckProperty(pMsg);
		break;
	case WM_SDI_EVENT_AF_DETECT:
		OnGetFocusStatus(pMsg);
		break;
	case WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED:
		switch((int)pMsg->nParam2)
		{
		case 0x5002:
		case 0x5003:
		case 0x5004:
		case 0x5005:
		case 0x500F:
		case 0xD80F:
		case 0xD815:
			break;
		default:
			break;
		}			
		break;
	case WM_SDI_EVENT_LIVEVIEW_RECEIVED:
		//-- Set Buffer
		if(m_pDlgLiveview)
		{
			m_pDlgLiveview->SetBuffer((PVOID)pMsg->pParam);
			m_pDlgLiveview->GetLiveview()->Invalidate(FALSE);
		}	
		break;
	case WM_SDI_EVENT_EVENT_ERROR_OCCURED:
		break;
	case WM_SDI_EVENT_LIVEVIEW_ERROR_OCCURED:
		break;	
	default:
		break;
	}
	return 0;
}

//Take Picture Function
void CESMStudioDlg::OnCapture(void)
{
	/*		
	if(m_pSdiSDK!=NULL)
	{		
		SdiMessage *pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_1;
		m_pSdiSDK->SdiAddMsg(pMsg);
		Sleep(1000);
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_2;
		m_pSdiSDK->SdiAddMsg(pMsg);
		Sleep(1000);
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_3;
		m_pSdiSDK->SdiAddMsg(pMsg);
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_4;
		m_pSdiSDK->SdiAddMsg(pMsg);


		//SdiMessage *pMsg = new SdiMessage();
		//pMsg = new SdiMessage();
		//pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_ONCE;
		//m_pSdiSDK->SdiAddMsg(pMsg);		
	}
	*/
}



//Save Picture Function
void CESMStudioDlg::OnCaptureDone(SdiMessage* pMsg)
{
	CSdiSingleMgr* pSdiMgr = NULL;
	pSdiMgr = (CSdiSingleMgr*)pMsg->pParam;

	//-- 2013-02-15 hongsu.jung
	//-- Get SDI Manager
	if(pSdiMgr)
	{
		CString strPath = m_pDlgConnect->m_strPath;
		//-- 2013-04-07 hongsu.jung
		CString strUnique = pSdiMgr->GetDeviceUniqueID().Right(5);
		strPath.Format(_T("%s\\%s.jpg"),strPath,strUnique);
		//-- Save Capture Image
		TCHAR* cPath = new TCHAR[1+strPath.GetLength()];
		_tcscpy(cPath, strPath);

		SdiMessage* pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_RECEIVE;
		pMsg->pParam = (LPARAM)cPath;
		pSdiMgr->SdiAddMsg(pMsg);
	}	
}

//-- Check Property
void CESMStudioDlg::CheckProperty(SdiMessage* pMsg)
{
	IDeviceInfo* pDevInfo = (IDeviceInfo *)pMsg->pParam;
	if(!pDevInfo)
		return;

	int nCode = pDevInfo->GetPropCode();
	int nValue;
	switch(pDevInfo->GetPropCode())
	{
	case PTP_CODE_FUNCTIONALMODE:
		//-- 2013-02-20 hongsu.jung
		//-- Get Value
		nValue = pDevInfo->GetCurrentEnum();
		//-- Check Program Model
		if(nValue != eUCS_UI_MODE_PROGRAM)
		{
			TRACE(_T("\nXXXXXXXXXXXXXXXX Check Mode : Not Program Mode Detection XXXXXXXXXXXXXXXXXXXX\n\n"));
			//MessageBox(_T("Not Program Mode Detection"));
		}
		break;
	default:
		;
		break;
	}
}


//Save Picture Function
void CESMStudioDlg::OnSaveDone(SdiMessage* pMsg)
{
	CSdiSingleMgr* pSdiMgr = NULL;
	pSdiMgr = (CSdiSingleMgr*)pMsg->pParam;

	//-- 2013-02-15 hongsu.jung
	//-- Get SDI Manager
	if(pSdiMgr)
	{
		if(m_pDlgConnect->m_pMovie)
		{
			CString strPath = m_pDlgConnect->m_strPath;
			CString strUnique = pSdiMgr->GetDeviceUniqueID().Right(5);
			strPath.Format(_T("%s\\%s.jpg"),strPath,strUnique);		
			m_pDlgConnect->m_pMovie->AddPicture(strPath);
		}		
	}
}

//-- Get Focus Status
void CESMStudioDlg::OnGetFocusStatus(SdiMessage* pMsg)
{
	CSdiSingleMgr* pSdiMgr = NULL;
	pSdiMgr = (CSdiSingleMgr*)pMsg->pParam;
	int nStatus = status_unknown;
	//-- 2013-02-15 hongsu.jung
	//-- Get SDI Manager
	if(pSdiMgr)
	{
		switch(pMsg->nParam2)
		{
		case SDI_AF_NULL	: nStatus = status_unknown	;	break;
		case SDI_AF_DETECT	: nStatus = status_focussed	;	break;
		case SDI_AF_FAIL	: nStatus = status_focusfail;	break;
		default				: nStatus = status_focussuccess;break;
		}

		m_pDlgConnect->UpdateStatus(pSdiMgr, nStatus);
	}
}

void CESMStudioDlg::SetLiveview(BOOL bLiveview)
{
	//-- 2013-02-06 hongsu.jung
	//-- Get Selected Device
	CSdiSdk* pSdiSDK = NULL;
	if(pSdiSDK)
		pSdiSDK->SdiLiveviewSet(bLiveview);
}

void CESMStudioDlg::OnSetFocus(int nFocusType)
{
	CSdiSingleMgr* pSdiMgr = NULL;
	pSdiMgr = m_pDlgConnect->GetSelectedDevice();

	if(pSdiMgr)
	{	
		SdiMessage *pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_SET_FOCUS;
		pMsg->nParam1 = nFocusType;    
		pSdiMgr->SdiAddMsg(pMsg);
	}
}
