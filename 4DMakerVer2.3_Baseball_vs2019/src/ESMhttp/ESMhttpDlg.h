
// ESMhttpDlg.h : header file
//

#pragma once
#include "ESMServer.h"
#include "ESMClient.h"
#include "ESMUtil.h"
#include "ESMFunc.h"
#include "ESMIndex.h"


// CESMhttpDlg dialog
class CESMhttpDlg : public CDialogEx
{
// Construction
public:
	CESMhttpDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ESMHTTP_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	virtual BOOL DestroyWindow();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CESMServer m_server;	//서버 모듈
	void StartServer();		//서버 시작 스레드
	CWinThread * m_pThread;
	static UINT ThreadServer(LPVOID _method);	//서버 리스너 쓰레드

	CESMClient m_client;	//클라이언트 모듈
	
	CString m_csServer;		//서버주소
	CString m_csClient;		//클라이언트 주소

	CString m_csSetup[ESM_SETUP_COUNT];	//환경설정
	void SaveSetup();				//환결설정 저장
	BOOL LoadSetup();				//환경설정 로드
	
	void SaveINI();				//환결설정 저장
	BOOL LoadINI();				//환경설정 로드

	LRESULT OnESMServerMsg(WPARAM w, LPARAM l);	//jhhan 16-10-19	//4DMaker -> ESMhttp -> Client 정보 전송


	CString m_cs4DMaker;
	afx_msg void OnBnClickedBtnReady();			//테스트 접속
	afx_msg void OnBnClickedBtnStart();			//테스트 녹화시작
	afx_msg void OnBnClickedBtnStop();			//테스트 녹화중지
	afx_msg void OnBnClickedBtnFinish();		//테스트 카메라OFF //접속해제

	void CreateLog();							//로그 생성

	void Dectect4DMaker();						//4DMaker 실행 확인 및 실행

	BOOL m_bTrayIcon;
	void RegisterTrayIcon();
	LRESULT TrayIconMsg(WPARAM wParam, LPARAM lParam);
	afx_msg void OnMenuOpen();
	afx_msg void OnMenuClose();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	BOOL ConnectMySQL();
	CString GetSqlQuery();
	int m_nCommand;

	afx_msg void OnBnClickedCommand(UINT msg);
	void OnRecieveCmd(int nCmd);
	
};
