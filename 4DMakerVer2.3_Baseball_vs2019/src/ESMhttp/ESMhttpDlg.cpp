
// ESMhttpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ESMhttp.h"
#include "ESMhttpDlg.h"
#include "afxdialogex.h"

#include "ESMIni.h"
#include "ESMFileOperation.h"

#define _DEF_CMD_READY		_T("0")
#define _DEF_CMD_START		_T("1")
#define _DEF_CMD_STOP		_T("2")
#define _DEF_CMD_FINISH		_T("3")

#define _DEF_PATH_BIN		_T("C:\\Program Files\\ESMLab\\4DMaker\\bin")
#define _DEF_PATH_LOG		_T("C:\\Program Files\\ESMLab\\4DMaker\\log")
#define _DEF_PATH_SETUP		_T("C:\\Program Files\\ESMLab\\4DMaker\\Setup")

#define  WM_TRAYICON_MSG WM_USER + 1

#define _DEF_4DS_ADDR		_T("192.168.18.18")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CESMhttpDlg dialog



CESMhttpDlg::CESMhttpDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_ESMHTTP_DIALOG, pParent)
	, m_csServer(_T(""))
	, m_csClient(_T(""))
	, m_cs4DMaker(_T(""))
	, m_nCommand(-1)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_pThread = NULL;
}

void CESMhttpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_SERVER_ADDR, m_csServer);
	DDX_Text(pDX, IDC_EDIT_CLIENT_ADDR, m_csClient);
	DDX_Text(pDX, IDC_EDIT_4DMAKER, m_cs4DMaker);
	DDX_Radio(pDX, IDC_RADIO_START, m_nCommand);
}

BEGIN_MESSAGE_MAP(CESMhttpDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_ESM_SERVER, OnESMServerMsg)
	ON_BN_CLICKED(IDC_BTN_READY, &CESMhttpDlg::OnBnClickedBtnReady)
	ON_BN_CLICKED(IDC_BTN_START, &CESMhttpDlg::OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, &CESMhttpDlg::OnBnClickedBtnStop)
	ON_BN_CLICKED(IDC_BTN_FINISH, &CESMhttpDlg::OnBnClickedBtnFinish)
	ON_MESSAGE(WM_TRAYICON_MSG, &CESMhttpDlg::TrayIconMsg)
	ON_COMMAND(ID_MENU_OPEN, &CESMhttpDlg::OnMenuOpen)
	ON_COMMAND(ID_MENU_CLOSE, &CESMhttpDlg::OnMenuClose)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO_START, IDC_RADIO_END, &CESMhttpDlg::OnBnClickedCommand)
END_MESSAGE_MAP()


// CESMhttpDlg message handlers

BOOL CESMhttpDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CreateLog();

	RegisterTrayIcon();

	//ConnectMySQL();
	//Dectect4DMaker();

	if (LoadINI()/*LoadSetup()*/ == FALSE)
	{
		AfxMessageBox(_T("Not found - httpSetup.ini"));
		AMLOGDEVINFO(_T("Not found - httpSetup.ini"));

		//SaveSetup();	//설정파일이 없을때 기본값으로 파일 생성
		return FALSE;
	}

	LPTSTR lpCmdLine = theApp.m_lpCmdLine;
	if (strlen((const char *)lpCmdLine) > 0)
	{
		m_server.SetEanbleLog(TRUE);
	}

	/*m_server.Set4DMaker(m_csSetup[ESM_4DMAKER]);
	m_server.SetHttpIp(m_csSetup[ESM_SERVER_IP]);
	m_server.SetHttpPort(m_csSetup[ESM_SERVER_PORT]);
	m_server.SetURL(m_csSetup[ESM_SERVER_URL]);

	m_client.SetHttpIp(m_csSetup[ESM_CLIENT_IP]);
	m_client.SetHttpPort(m_csSetup[ESM_CLIENT_PORT]);
	m_client.SetURL(m_csSetup[ESM_CLIENT_URL]);*/

	//m_cs4DMaker = m_csSetup[ESM_4DMAKER];
	//m_csServer.Format(_T("http://%s:%s/%s"), m_csSetup[ESM_SERVER_IP], m_csSetup[ESM_SERVER_PORT], m_csSetup[ESM_SERVER_URL]);
	
	//m_csServer = m_csSetup[ESM_CLIENT_UPLUS_01];
	//m_csClient = m_csSetup[ESM_CLIENT_UPLUS_02];
	//m_csClient.Format(_T("http://%s:%s/%s"), m_csSetup[ESM_CLIENT_IP], m_csSetup[ESM_CLIENT_PORT], m_csSetup[ESM_CLIENT_URL]);
	
	UpdateData(FALSE);

	
	//m_client.SetURL(m_csServer);
#ifdef _USE_UPLUS
	if (m_csServer.IsEmpty() != TRUE)
		m_client.OnGetList(m_csServer);
	if (m_csServer.IsEmpty() != TRUE)
		m_client.OnGetList(m_csClient);
#else
	m_client.OnAddTestUrl(_DEF_4DS_ADDR);
	GetDlgItem(IDC_STATIC_PROC)->SetWindowText(_DEF_4DS_ADDR);
#endif

	//m_client.OnSendTest();

	/*if (m_pThread == NULL)
	{
		m_pThread = AfxBeginThread(ThreadServer, this);
	}*/

	/*m_server.GetVodInfo();

	m_server.GetVodInfo_ALL();*/

	//SendMessage(WM_ESM_SERVER, WM_ESM_NOTIFY_DB, (int)100);

#if _DEBUG
	GetDlgItem(IDC_BTN_READY)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BTN_FINISH)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BTN_START)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BTN_STOP)->ShowWindow(SW_SHOW);
#endif

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CESMhttpDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else if (nID == SC_CLOSE)
	{
		ShowWindow(SW_HIDE);
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CESMhttpDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CESMhttpDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CESMhttpDlg::StartServer()
{
	m_server.StartServer();
}

UINT CESMhttpDlg::ThreadServer(LPVOID _method)
{
	CESMhttpDlg * pDlg = (CESMhttpDlg *)_method;
	
	pDlg->StartServer();

	return 0;
}

BOOL CESMhttpDlg::DestroyWindow()
{
	m_server.StopServer();

	if (m_pThread != NULL)
	{
		m_pThread->SuspendThread();
		DWORD dwResult;
		::GetExitCodeProcess(m_pThread->m_hThread, &dwResult);

		/*delete m_pThread;
		m_pThread = NULL;*/
	}
	return CDialogEx::DestroyWindow();
}

void CESMhttpDlg::SaveSetup()
{
	CFile file;
	CString strFilePath, WriteData;
	strFilePath.Format(_T("%s\\HttpSetup.csv"), _DEF_PATH_SETUP);

	char* pstrData = NULL;
	if (file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite))
	{
		WriteData.Format(_T("%s, %s, %s, %s, %s\r\n"), _T("4DMakerCaption"), _T("ServerIP"), _T("ServerPort"), _T("ClientIP"), _T("ClientPort"));
		pstrData = ESMUtil::CStringToChar(WriteData);
		file.Write(pstrData, (int)strlen(pstrData));

		WriteData.Format(_T("%s, %s, %s, %s, %s\r\n"), _T("4DMaker")/*m_csSetup[ESM_4DMAKER]*/, ESMUtil::GetLocalIPAddress()/*m_csSetup[ESM_SERVER_IP]*/,_T("1223")/*m_csSetup[ESM_SERVER_PORT]*/, ESMUtil::GetLocalIPAddress()/*m_csSetup[ESM_CLIENT_IP]*/, _T("1223")/*m_csSetup[ESM_CLIENT_PORT]*/);
		pstrData = ESMUtil::CStringToChar(WriteData);
		file.Write(pstrData, (int)strlen(pstrData));
		delete[] pstrData;
		pstrData = NULL;

		file.Close();

		AMLOGDEVINFO(_T("Default - Setup.csv / Create"));
		AfxMessageBox(_T("Edit : ")+strFilePath);
	}


}

BOOL CESMhttpDlg::LoadSetup()
{
	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\HttpSetup.csv"), _DEF_PATH_SETUP);

	if (!ReadFile.Open(strInputData, CFile::modeRead))
		return FALSE;

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if (iLength == 0)
		return FALSE;

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength - 1] = '\0';
	int len = 0;
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for (int i = 0; i < len; i++)
	{
		if (buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	int five = sBuffer.GetLength();
	int iPos = 0, i = 0;
	CString sLine;
	//sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

	while (1)
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);
		sLine.Trim();
		if (sLine == _T(""))
			break;

		for (int i = 0; i < ESM_SETUP_COUNT; i++)
		{
			AfxExtractSubString(m_csSetup[i], sLine, i, ',');
			m_csSetup[i].Trim();
		}
	}
	return TRUE;
}

void CESMhttpDlg::SaveINI()
{
	CString strFile;
	strFile.Format(_T("%s\\httpSetup.ini"), _DEF_PATH_SETUP);

	CESMIni ini;
	if (ini.SetIniFilename(strFile))
	{
		ini.WriteString(_T("Common"), _T("App"), m_csSetup[ESM_4DMAKER]);

		ini.WriteString(_T("Server"), _T("IP"), m_csSetup[ESM_SERVER_IP]);
		ini.WriteString(_T("Server"), _T("Port"), m_csSetup[ESM_SERVER_PORT]);
		ini.WriteString(_T("Server"), _T("URL"), m_csSetup[ESM_SERVER_URL]);

		ini.WriteString(_T("Client"), _T("IP"), m_csSetup[ESM_CLIENT_IP]);
		ini.WriteString(_T("Client"), _T("Port"), m_csSetup[ESM_CLIENT_PORT]);
		ini.WriteString(_T("Client"), _T("URL"), m_csSetup[ESM_CLIENT_URL]);

		ini.WriteString(_T("UPlus"), _T("URL1"), m_csSetup[ESM_CLIENT_UPLUS_01]);
		ini.WriteString(_T("UPlus"), _T("URL2"), m_csSetup[ESM_CLIENT_UPLUS_02]);
	}
	
}

BOOL CESMhttpDlg::LoadINI()
{
	CFile  file;
	CESMFileOperation fo;
	CString strFile;
	strFile.Format(_T("%s\\httpSetup.ini"), _DEF_PATH_SETUP);
	if (!fo.IsFileExist(strFile))
	{
		if (file.Open(strFile, CFile::modeCreate | CFile::modeReadWrite))
		{
			AMLOGDEVINFO(_T("Default - httpSetup.ini / Create Success"));
			file.Close();
		}
		else
		{
			AMLOGDEVINFO(_T("Default - httpSetup.ini / Create Fail"));
			return FALSE;
		}
	}

	CESMIni ini;
	if (ini.SetIniFilename(strFile))
	{
		m_csSetup[ESM_4DMAKER] = ini.GetString(_T("Common"), _T("App"), _T("4DMaker"));

		m_csSetup[ESM_SERVER_IP] = ini.GetString(_T("Server"), _T("IP"), ESMUtil::GetLocalIPAddress());
		m_csSetup[ESM_SERVER_PORT] = ini.GetString(_T("Server"), _T("Port"), _T("1223"));
		m_csSetup[ESM_SERVER_URL] = ini.GetString(_T("Server"), _T("URL"), _T("camera"));


		m_csSetup[ESM_CLIENT_IP] = ini.GetString(_T("Client"), _T("IP"), _T("127.0.0.1"));
		m_csSetup[ESM_CLIENT_PORT] = ini.GetString(_T("Client"), _T("Port"), _T("1223"));
		m_csSetup[ESM_CLIENT_URL] = ini.GetString(_T("Client"), _T("URL"), _T("its-api/notify/camState"));

		m_csSetup[ESM_CLIENT_UPLUS_01] = ini.GetString(_T("UPlus"), _T("URL1"), _T("http://211.170.95.150/service/server/ss"));
		m_csSetup[ESM_CLIENT_UPLUS_02] = ini.GetString(_T("UPlus"), _T("URL2"), _T("http://211.170.95.150/service/server/ss"));
	}
	else
	{
		return FALSE;
	}
	
	SaveINI();

	return TRUE;
}


LRESULT CESMhttpDlg::OnESMServerMsg(WPARAM wParam, LPARAM lParam)
{
	switch ((int)wParam)
	{
	case WM_ESM_GET_INFO:		break;
	case WM_ESM_GET_STATUS:		break;;
	case WM_ESM_GET_COMMAND:	break;
	case WM_ESM_NOTIFY:
	{
		m_client.OnSendCommand((int)lParam);
		OnRecieveCmd((int)lParam);
		
		//m_client.SendCamState((int)lParam);
	}
		break;
	case WM_ESM_NOTIFY_SYNC:
		m_client.OnSendSync((int)lParam);
		break;
	case WM_ESM_NOTIFY_DB:
		//(int)lParam -> Push
		m_client.SendNotifyUpdate((int)lParam);
		break;
	default:
		TRACE(_T("[ESM] Unknown ESM Message[0x%X]"), (int)wParam);
		AMLOGDEVINFO(_T("[ESM] Unknown ESM Message[0x%X]"), (int)wParam);
		break;
	}
	return 0;
}

void CESMhttpDlg::OnBnClickedBtnReady()
{
	m_client.OnTestUPlus();
	//m_client.SendCamState((int)1);
	//준비	Connect
	//m_server.GetResponse(_T("command"), _DEF_CMD_READY);
}


void CESMhttpDlg::OnBnClickedBtnStart()
{
	//m_client.SendCamState((int)3);
	//시작 Recoding
	m_server.GetResponse(_T("command"), _DEF_CMD_START);
}


void CESMhttpDlg::OnBnClickedBtnStop()
{
	//m_client.SendCamState((int)4);
	//중지 Stop
	m_server.GetResponse(_T("command"), _DEF_CMD_STOP);
	//m_client.SendNotifyUpdate(146);
}


void CESMhttpDlg::OnBnClickedBtnFinish()
{
	//m_client.SendCamState((int)0);
	//종료 Camera Off
	m_server.GetResponse(_T("command"), _DEF_CMD_FINISH);
}

void CESMhttpDlg::CreateLog()
{
	/*CMapStringToString map;
	CString strValue = _T("Tx");
	map.SetAt(_T("1"),_T("Rx"));	//input
	map.Lookup(_T(""), strValue);	//output

	POSITION pos = map.GetStartPosition();	//search
	while (pos != NULL)
	{
		CString csKey, csValue;
		map.GetNextAssoc(pos, csKey, csValue);

	}*/

	CString strDate = _T("");
	CString strTime = _T("");
	CTime time = CTime::GetCurrentTime();
	//time = CTime::GetCurrentTime();
	int nYear = time.GetYear();
	int nMonth = time.GetMonth();
	int nDay = time.GetDay();

	strDate.Format(_T("%04d-%02d-%02d"), nYear, nMonth, nDay);
	strTime.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), nYear, nMonth, nDay, time.GetHour(), time.GetMinute(), time.GetSecond());
	//strTime.Format(_T("%02d_%02d_%02d"), time.GetHour(), time.GetMinute(), time.GetSecond());

	CString csLog, csPath;
	csPath.Format(_T("%s\\%s"), _DEF_PATH_LOG, strDate);
	AMLOG_SETPATH(csPath);

	csLog.Format(_T("%s\\%s\\%s_http.log"), _DEF_PATH_LOG, strDate, strTime);

	AMLOG_SETFILENAME((TCHAR*)(LPCTSTR)csLog);
	AMLOG_SETLOGLEVEL_DEVELOPERINFO;
	AMLOGDEVINFO(_T("%s\\%s\\%s_http.log"), _DEF_PATH_LOG, strDate, strTime);
	AMLOGDEVINFO(_T("Running ESM http Server & Client"));
}

void CESMhttpDlg::Dectect4DMaker()
{
	HWND _hWnd = NULL;
	DWORD dwPid = 0;

#if _DEBUG
	dwPid = ESMUtil::GetProcessPID(_T("4DMakerD.exe"));
	if (dwPid == 0)
	{
		dwPid = ESMUtil::GetProcessPID(_T("4DMaker.exe"));
	}
#else
	dwPid = ESMUtil::GetProcessPID(_T("4DMaker.exe"));
#endif
	if (dwPid != 0)
	{
		_hWnd = ESMUtil::GetWndHandle(dwPid);
	}

	if (_hWnd == NULL)
	{
		//AfxMessageBox(_T("Not found - 4DMaker"));
		AMLOGDEVINFO(_T("Not found - 4DMaker"));
#if _DEBUG
		if (ESMUtil::GetIsFile(_DEF_PATH_BIN, _T("4DMakerD.exe")))
		{
			if (ESMUtil::ExecuteProcess(_DEF_PATH_BIN, _T("4DMakerD.exe")))
			{
				AMLOGDEVINFO(_T("Execute - 4DMaker"));
			}
		}
#else
		if (ESMUtil::GetIsFile(_DEF_PATH_BIN, _T("4DMaker.exe")))
		{
			if (ESMUtil::ExecuteProcess(_DEF_PATH_BIN, _T("4DMaker.exe")))
			{
				AMLOGDEVINFO(_T("Execute - 4DMaker"));
			}
		}
#endif
	}
}

void CESMhttpDlg::RegisterTrayIcon()
{
	NOTIFYICONDATA nid;
	nid.cbSize = sizeof(nid);

	nid.hWnd = m_hWnd;
	nid.uID = IDR_MAINFRAME;
	nid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
	nid.uCallbackMessage = WM_TRAYICON_MSG;
	nid.hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	//char strTitle[MAX_PATH];
	//GetWindowText(strTitle, sizeof(strTitle));
	lstrcpy(nid.szTip, _T("ESMhttp"));
	Shell_NotifyIcon(NIM_ADD, &nid);
	SendMessage(WM_SETICON, (WPARAM)TRUE, (LPARAM)nid.hIcon);
	m_bTrayIcon = TRUE;

}

LRESULT CESMhttpDlg::TrayIconMsg(WPARAM wParam, LPARAM lParam)
{
	switch (lParam)
	{
	case WM_LBUTTONDBLCLK:
		ShowWindow(SW_SHOW);
		break;
	case WM_RBUTTONUP:
	{
		CPoint ptMouse;
		::GetCursorPos(&ptMouse);
		CMenu menu;
		menu.LoadMenu(IDR_MENU);
		CMenu *pMenu = menu.GetSubMenu(0);

		HWND _hwnd = GetSafeHwnd();
		::SetForegroundWindow(_hwnd);
		pMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptMouse.x, ptMouse.y, AfxGetMainWnd());
	}
	break;
	default:
		break;
	}

	return 0;
}

void CESMhttpDlg::OnMenuOpen()
{
	ShowWindow(SW_SHOW);
}


void CESMhttpDlg::OnMenuClose()
{
	SendMessage(WM_CLOSE, NULL, NULL);
}


BOOL CESMhttpDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			ShowWindow(SW_HIDE);
			return TRUE;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

BOOL CESMhttpDlg::ConnectMySQL()
{
	CString strDatabase, strId, strPwd, strName;
	strDatabase = DB_ADDRESS;
	strId = DB_ID;
	strPwd = DB_PASS;
	strName = DB_NAME;

	char* pDbAddrIp = ESMUtil::CStringToChar(strDatabase);
	char* pDbID = ESMUtil::CStringToChar(strId);
	char* pDbPW = ESMUtil::CStringToChar(strPwd);
	char* pDbName = ESMUtil::CStringToChar(strName);

	mysql_init(&mysql);

	if (!mysql_real_connect(&mysql, pDbAddrIp, pDbID, pDbPW, pDbName, 3306, 0, 0))
	{
		//char errMsg[MAX_PATH];
		const char * errM = mysql_error(&mysql);
		CString strMsg;
		strMsg = ESMUtil::CharToCString((char *)errM);
		AfxMessageBox(strMsg, MB_OK);
		return FALSE;
	}
	mysql_query(&mysql, "set Names EucKR");

	CString strQuery;
	strQuery.Format(_T("use "), DB_NAME);
	char * pQuery = NULL;
	pQuery = ESMUtil::CStringToChar(strQuery);
	//mysql_query(&mysql, "use esm");
	//mysql_query(&mysql, pQuery);
	delete[] pQuery;


	delete[] pDbAddrIp;
	delete[] pDbID;
	delete[] pDbPW;
	delete[] pDbName;

	MYSQL_ROW row;
	MYSQL_RES *m_res = NULL;

	char* sQuery = new char[1000];
	//strcpy(sQuery, "select Idx, Name from dbTest");
	//strcpy(sQuery, "SELECT * FROM world.city WHERE CountryCode = 'KOR' AND Population > 1000000 ORDER BY Population DESC");
	strcpy(sQuery, "SELECT 4d_live.`num`, 4d_live.`hit`, 4d_live.`mode`, 4d_live.`sub_l`, 4d_live.`sub_m`, 4d_live.`sub_s`, \
		4d_live.`date`, 4d_live.`time`, 4d_live.`v_count`, 4d_live.`h_count`, 4d_live.`object`, 4d_live.`explain` FROM ESM.4D_LIVE; ");



	if (mysql_query(&mysql, sQuery))
	{
		return FALSE;
	}
	
	if ((m_res = mysql_store_result(&mysql)) == NULL)
	{
		return FALSE;
	}

	my_ulonglong nRow = 0;
	nRow = mysql_num_rows(m_res);

	web::json::value data;
	data[U("rstCode")] = web::json::value::string(U("000"));			//결과 코드
	data[U("errMsg")] = web::json::value::string(U("OK"));				//에러 메시지

	
	UINT nFiled = 0;
	nFiled = mysql_num_fields(m_res);

	std::vector<web::json::value> objArr;
	
	while ((row = mysql_fetch_row(m_res)) != NULL)
	{
		CString strMsg;
		

		CString strNum, strHit, strMode, strSubL, strSubM, strSubS, \
			strDate, strTime, strVCount, strHCount, strObject, strExplain;
		
		
		strNum = row[0];
		strHit = row[1];
		strMode = row[2];
		strSubL = row[3];
		strSubM = row[4];
		strSubS = row[5];
		strDate = row[6];
		strTime = row[7];
		strVCount = row[8];
		strHCount = row[9];
		strObject = row[10];
		strExplain = row[11];
		
		strMsg.Format(_T("%s  %s, %s\r\n%s, %s, %s\r\n%s, %s\r\n%s, %s\r\n%s, %s"), strNum, strHit, strMode, strSubL, strSubM, strSubS, strDate, strTime, strVCount, strHCount, strObject, strExplain);
		AfxMessageBox(strMsg);

		//CString strLog1, strLog2;
		//strLog1 = row[0];
		//strLog2 = row[1];
		//strMsg.Format(_T("%s, %s"), strLog1, strLog2);
		//AfxMessageBox(strMsg);
		web::json::value objData;
		//objData[U("Idx")] = web::json::value::string(strLog1.operator LPCWSTR());				//카메라 ID   - CamId
		//objData[U("Name")] = web::json::value::string(strLog2.operator LPCWSTR());				//카메라 명칭 - IPGroup+CamId
		
		objData[U("num")] = web::json::value::string(strNum.operator LPCWSTR());
		objData[U("hit")] = web::json::value::string(strHit.operator LPCWSTR());
		objData[U("mode")] = web::json::value::string(strMode.operator LPCWSTR());

		objData[U("sub_l")] = web::json::value::string(strSubL.operator LPCWSTR());
		objData[U("sub_m")] = web::json::value::string(strSubM.operator LPCWSTR());
		objData[U("sub_s")] = web::json::value::string(strSubS.operator LPCWSTR());

		objData[U("date")] = web::json::value::string(strDate.operator LPCWSTR());
		objData[U("time")] = web::json::value::string(strTime.operator LPCWSTR());

		objData[U("v_count")] = web::json::value::string(strVCount.operator LPCWSTR());
		objData[U("h_count")] = web::json::value::string(strHCount.operator LPCWSTR());

		objData[U("object")] = web::json::value::string(strObject.operator LPCWSTR());
		objData[U("explain")] = web::json::value::string(strExplain.operator LPCWSTR());

		objArr.push_back(objData);
	}

	if (objArr.size() > 0)
	{
		data[U("Data")] = web::json::value::array(objArr);
	}

	mysql_free_result(m_res);

	mysql_close(&mysql);
	

	return TRUE;
}


CString CESMhttpDlg::GetSqlQuery()
{
	CString strQuery;

	//SELECT -> ALL
	strQuery = _T("SELECT * FROM esm.`4d_live`");
	//SELECT -> hit == 0
	strQuery = _T("SELECT * FROM esm.`4d_live` WHERE hit = 0");
	//select -> hit > 0
	strQuery = _T("SELECT * FROM esm.`4d_live` WHERE hit > 0");
	//select -> MAX(num)
	strQuery = _T("SELECT num FROM esm.`4d_live` ORDER BY num DESC LIMIT 1");
	//select -> MAX(num)
	strQuery = _T("SELECT MAX(num) FROM esm.`4d_live`");

	//UPDATE -> hit == 0, hit++
	strQuery = _T("UPDATE esm.`4d_live` SET hit = hit + 1 WHERE hit = 0");
	//UPDATE -> num < ? && hit == 0, hit++
	strQuery = _T("UPDATE esm.`4d_live` SET hit = hit + 1 WHERE hit = 0 AND num <= %d");
	//UPDATE -> num <= MAX(num) && hit == 0, hit++
	strQuery = _T("UPDATE esm.`4d_live` SET hit = hit + 1 WHERE hit = 0 AND num <= (SELECT * FROM (SELECT num FROM esm.`4d_live` ORDER BY num DESC LIMIT 1) AS A)");
	//UPDATE -> hit == 0, select Lock, hit++
	strQuery = _T("UPDATE esm.`4d_live` SET hit = hit + 1 WHERE hit =  (SELECT * FROM (SELECT hit FROM esm.`4d_live` WHERE hit = 0) AS A LIMIT 1)");


	return strQuery;
}

void CESMhttpDlg::OnBnClickedCommand(UINT msg)
{
	UpdateData(TRUE);

	CString strLog;
	strLog.Format(_T("Clicked : %d"), m_nCommand);
	AMLOGDEVINFO((TCHAR*)(LPCTSTR)strLog);

	switch (m_nCommand)
	{
	case 0:		//START
		SendMessage(WM_ESM_SERVER, WM_ESM_NOTIFY, 1);
		break;
	case 1:		//PAUSE
		SendMessage(WM_ESM_SERVER, WM_ESM_NOTIFY, 9);
		break;
	case 2:		//RESUME
		SendMessage(WM_ESM_SERVER, WM_ESM_NOTIFY, 11);
		break;
	case 3:		//END
		SendMessage(WM_ESM_SERVER, WM_ESM_NOTIFY, 19);
		break;
	default:
		break;
	}
}

void CESMhttpDlg::OnRecieveCmd(int nCmd)
{
	switch (nCmd)
	{
	case 1:
		m_nCommand = 0;
		break;
	case 9:
		m_nCommand = 3;
		break;
	case 11:
		m_nCommand = 2;
		break;
	case 19:
		m_nCommand = 1;
		break;
	default:
		m_nCommand = -1;
		break;
	}
	
	UpdateData(FALSE);
}