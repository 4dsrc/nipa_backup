#pragma once

#include <iostream>
#include <tchar.h>

//#include <atlstr.h>
using namespace std;


#include <cpprest/http_client.h>
#include <cpprest/json.h>
#include <cpprest/filestream.h>

using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams

typedef struct _TRHEADEVENTMSG
{
	_TRHEADEVENTMSG()
	{
		pParam = NULL;
		pParent = NULL;
	}
	LPARAM	pParam;
	LPARAM	pParent;
} TRHEADEvent;

class CESMClient
{
public:
	CESMClient();
	~CESMClient();

	void GetTest();
	void SendCamState(int nState = 1);

private:
	CString m_csClientIp;
	CString m_csClientPort;

	CString m_csURL;

	CStringArray m_arrClient;

	int m_nCommand;
	int m_nIdx;
public:
	void SetHttpIp(CString csIp);
	CString GetHttpIp();
	void SetHttpPort(CString csPort);
	CString GetHttpPort();

	void SetURL(CString csUrl);
	CString GetURL();

	void SendNotifyUpdate(int nNum);

	void OnTestUPlus();

	void OnGetList();

	void OnGetList(CString csUrl);

	void OnSendCommand(int nParam);

	void SetCommand(int nParam) { m_nCommand = nParam; }
	int GetCommand() { return m_nCommand; }

	void OnCommand();

	static unsigned WINAPI SendCommandThread(LPVOID param);
	void OnSendSync(int nParam);
	void SetCameraIdx(int nParam) { m_nIdx = nParam; }
	int GetCameraIdx() { return m_nIdx; }

	void OnSync();
	static unsigned WINAPI SendSyncThread(LPVOID param);

	static unsigned WINAPI RunCommandThread(LPVOID param);
	void OnSendTest();

	void OnAddTestUrl(CString strUrl);
};

