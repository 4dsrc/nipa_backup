#include "stdafx.h"
#include "ESMServer.h"
#include "ESMIni.h"
#include <afxmt.h>

static CESMServer * g_Main = NULL;
CMutex g_mutex(FALSE, NULL);

void GetHandler(http_request request) {

	string_t uri = U("");

	ucout << U("GetHandler") << std::endl;
	ucout << U("\t[request uri] ") << std::endl << request.request_uri().to_string() << std::endl;
	
	//Request uri 구분 동작 구분
	uri = request.relative_uri().to_string();
	
	//AMLOGDEVINFO((TCHAR*)(LPCTSTR)uri.c_str());

	g_mutex.Lock();
	CString ul = uri.c_str();
	
	CString csData;
	std::wstring wsTmp = ul.operator LPCWSTR();
	std::wstring wsValue = uri::decode(wsTmp);
	csData = wsValue.c_str();
	

	CString csTemp, csState, csValue;
	//AfxExtractSubString(csTemp, ul, 0, '/');
	AfxExtractSubString(csState, csData, 1, '/');
	AfxExtractSubString(csValue, csData, 2, '/');

	AMLOGDEVINFO((TCHAR*)(LPCTSTR)csData);
	web::json::value answer;

#if _SINGLE_VALUE
	web::json::value answer = g_Main->GetResponse(csState, csValue);
#else	//KT 4D & ESM 4DApp = DUAL
	CString csSearch[3];
	if (csValue.IsEmpty() != true)
	{
		CString csTemp, csTmp;
		AfxExtractSubString(csTemp, csValue, 0, '?');
		if (csTemp.CompareNoCase(_T("search")) == 0)
		{
			AfxExtractSubString(csTemp, csValue, 1, '?');

			AfxExtractSubString(csSearch[0], csTemp, 0, '&');

			AfxExtractSubString(csTmp, csSearch[0], 0, '=');
			if (csTmp.CompareNoCase(_T("category")) == 0)
			{
				//search Category API
				CString csData;
				AfxExtractSubString(csData, csSearch[0], 1, '=');

				answer = g_Main->GetCategory(csData);
			}
			else
			{
				AfxExtractSubString(csSearch[1], csTemp, 1, '&');
				AfxExtractSubString(csSearch[2], csTemp, 2, '&');
				//search API
				answer = g_Main->GetSearch(csTemp);
			}
		}
		else if (csState.CompareNoCase(_T("contents")) == 0)
		{
			//contents Hits
			answer = g_Main->GetHits(csValue);
		}
		else 
		{
			//KT
			answer = g_Main->GetResponse(csState, csValue);
		}
	}
	else
	{
		if (csState.CompareNoCase(_T("contents")) == 0)
		{
			//contents API
			answer = g_Main->GetContentsAll();
		}
		else
		{
			//KT
			answer = g_Main->GetResponse(csState, csValue);
		}
	}
#endif

	http_response response;
	response.set_status_code(status_codes::OK);
	response.set_body(answer);

	request.reply(response);

	g_mutex.Unlock();

	return;
}

void PostHandler(http_request request) {

	string_t uri = U("");

	ucout << U("PostHandler") << std::endl;
	ucout << U("\t[request uri] ") << std::endl << request.request_uri().to_string() << std::endl;
	
	task<std::wstring> extractVectorTask = request.extract_string();
	extractVectorTask.wait();
	//std::wstring val = extractVectorTask.get();
	
	uri = request.relative_uri().to_string();
	AMLOGDEVINFO((TCHAR*)(LPCTSTR)uri.c_str());

	CString ul = uri.c_str();

	CString csData;
	std::wstring wsTmp = ul.operator LPCWSTR();
	std::wstring wsValue = uri::decode(wsTmp);
	csData = wsValue.c_str();

	CString csTemp, csState, csValue;
	AfxExtractSubString(csTemp, csData, 0, '/');
	AfxExtractSubString(csState, csData, 1, '/');
	AfxExtractSubString(csValue, csData, 2, '/');

	AMLOGDEVINFO((TCHAR*)(LPCTSTR)csData);

	
	web::json::value answer;

	if (csTemp.CompareNoCase(_T("camera")) == 0)
	{
		answer = g_Main->GetResponse(csState, csValue);
	}
	else if (csTemp.CompareNoCase(_T("app")) == 0)
	{
		answer = g_Main->GetDBResponse(csState, csValue);
	}

	http_response response;
	response.set_status_code(status_codes::OK);
	response.set_body(answer);

	request.reply(response);

	/*web::json::value answer = g_Main->GetResponse(csState, csValue);

	http_response response;
	response.set_status_code(status_codes::OK);
	response.set_body(answer);

	request.reply(response);*/
	return;
}

CESMServer::CESMServer()
{
	g_Main = this;
	
	m_cs4DMaker = _T("");
	m_csServerPort = _T("");
	m_bLog = FALSE;
	m_csURL = _T("");
}

CESMServer::~CESMServer()
{
}

void CESMServer::StartServer()
{
	CString csUri;
	csUri.Format(_T("http://%s:%s/%s"), GetHttpIp(), GetHttpPort(), GetURL());
	//csUri.Format(_T("http://%s:%s/camera"), GetHttpIp(), GetHttpPort());
	//csUri.Format(_T("http://%s:%s/camera"), _T("esmlab.com"), GetHttpPort());
	AMLOGDEVINFO((TCHAR*)(LPCTSTR)csUri);

	uri u;
	u = csUri.operator LPCWSTR();

	http_listener_config config;
	utility::seconds timeout(10);
	config.set_timeout(timeout);
	
	http_listener listener(u, config);
	
	listener.support(web::http::methods::GET, GetHandler);
	listener.support(web::http::methods::POST, PostHandler);
	
	task<void> openTask = listener.open();
	task<void> afterOpenTask = openTask.then([&listener]() {
		//ucout << U("listening [") << listener.uri().to_string() << U("]") << std::endl;
		
		while (true) {
			Sleep(1);
		}

	});

	try
	{
		afterOpenTask.wait();
		openTask.wait();
		listener.close().wait();
	}
	catch (const std::exception& e)
	{
		TRACE(e.what());
	}
}
void CESMServer::StopServer()
{
}

BOOL CESMServer::SetResponse(CString csState, CString csValue/* = _T("")*/)
{
	HWND _hWnd = NULL;
	DWORD dwPid = 0;

	//_hWnd = ::FindWindow(NULL, Get4DMaker());
#if _DEBUG
	dwPid = ESMUtil::GetProcessPID(_T("4DMakerD.exe"));
	if (dwPid == 0)
	{
		dwPid = ESMUtil::GetProcessPID(_T("4DMaker.exe"));
	}
#else
	dwPid = ESMUtil::GetProcessPID(_T("4DMaker.exe"));
#endif
	if (dwPid != 0)
	{
		_hWnd = ESMUtil::GetWndHandle(dwPid);
	}

	if (_hWnd == NULL)
	{
		//AfxMessageBox(_T("Not found - 4DMaker"));
		AMLOGDEVINFO(_T("Not found - 4DMaker"));
		return FALSE;
	}

	CString cshWnd;
	TCHAR buffer[256];
	GetWindowText(_hWnd, buffer, sizeof(buffer));
	cshWnd.Format(_T("FindWindow : %s"), buffer);
	AMLOGDEVINFO((TCHAR*)(LPCTSTR)cshWnd);
	
	AMLOGDEVINFO(_T("SetResponse : %s - %s"), csState, csValue);

	int nValue = _ttoi(csValue);
	LRESULT nResult = S_FALSE;

	if (csState == _T("info"))	//정보
	{
		nResult = ::SendMessage(_hWnd, WM_ESM_SERVER, (WPARAM)WM_ESM_GET_INFO, (LPARAM)nValue);		//4DMaker CameraList.csv 참조
	}
	else if (csState == _T("state"))	//상태
	{
		nResult = ::SendMessage(_hWnd, WM_ESM_SERVER, (WPARAM)WM_ESM_GET_STATUS, (LPARAM)nValue);		//4DMaker 파일 기록 CameraState.csv 참조
	}
	else if (csState == _T("command"))	//제어
	{
		nResult = ::SendMessage(_hWnd, WM_ESM_SERVER, (WPARAM)WM_ESM_GET_COMMAND, (LPARAM)nValue);
	}
	if (nResult != S_OK)
		return FALSE;

	return TRUE;
}

web::json::value CESMServer::GetResponse(CString csState, CString csValue/* = _T("")*/)
{
	web::json::value response;

	//4DMaker와의 통신을 통해 아래와 같은 정보를 얻은 후 JSON 포맷으로 리턴
	BOOL bRet = SetResponse(csState, csValue);
	if (bRet == FALSE)
	{
		response[U("rstCode")] = web::json::value::string(U("100"));			//결과 코드
		response[U("errMsg")] = web::json::value::string(U("4DMaker 오류"));				//에러 메시지
		return response;
	}

	if (csState == _T("info"))	//정보
	{
		response = GetInfo(csValue);			//CameraList.csv

		if (m_bLog)
		{
			if (response.has_field(U("rstCode")))
			{
				AMLOGDEVINFO(_T("rstCode : %s"), response.at(U("rstCode")).as_string().c_str());
			}
			if (response.has_field(U("errMsg")))
			{
				AMLOGDEVINFO(_T("errMsg : %s"), response.at(U("errMsg")).as_string().c_str());
			}
			if (response.has_field(U("camList")))
			{
				AMLOGDEVINFO(_T("camList : %d"), response.at(U("camList")).as_array().size());

				for (int i = 0; i < response.at(U("camList")).as_array().size(); i++)
				{
					web::json::value v = response.at(U("camList")).at(i);
					if (v.has_field(U("camId")))
					{
						AMLOGDEVINFO(_T("camId : %s"), v.at(U("camId")).as_string().c_str());
					}
					if (v.has_field(U("camNm")))
					{
						AMLOGDEVINFO(_T("camNm : %s"), v.at(U("camNm")).as_string().c_str());
					}
					if (v.has_field(U("camLoc")))
					{
						AMLOGDEVINFO(_T("camLoc : %s"), v.at(U("camLoc")).as_string().c_str());
					}
				}
			}
			else
			{
				if (response.has_field(U("camNm")))
				{
					AMLOGDEVINFO(_T("camNm : %s"), response.at(U("camNm")).as_string().c_str());
				}
				if (response.has_field(U("camLoc")))
				{
					AMLOGDEVINFO(_T("camLoc : %s"), response.at(U("camLoc")).as_string().c_str());
				}

			}
		}
		
	}
	else if (csState == _T("state"))	//상태
	{
		response = GetStatus(csValue);			//CameraState.csv

		if (m_bLog)
		{
			if (response.has_field(U("rstCode")))
			{
				AMLOGDEVINFO(_T("rstCode : %s"), response.at(U("rstCode")).as_string().c_str());
			}
			if (response.has_field(U("errMsg")))
			{
				AMLOGDEVINFO(_T("errMsg : %s"), response.at(U("errMsg")).as_string().c_str());
			}
			if (response.has_field(U("camList")))
			{
				AMLOGDEVINFO(_T("camList : %d"), response.at(U("camList")).as_array().size());

				for (int i = 0; i < response.at(U("camList")).as_array().size(); i++)
				{
					web::json::value v = response.at(U("camList")).at(i);
					if (v.has_field(U("camId")))
					{
						AMLOGDEVINFO(_T("camId : %s"), v.at(U("camId")).as_string().c_str());
					}
					if (v.has_field(U("camState")))
					{
						AMLOGDEVINFO(_T("camState : %s"), v.at(U("camState")).as_string().c_str());
					}
				}
			}
			else
			{
				if (response.has_field(U("camState")))
				{
					AMLOGDEVINFO(_T("camState : %s"), response.at(U("camState")).as_string().c_str());
				}

			}
		}

	}
	else if (csState == _T("command"))	//제어
	{
		response = GetCommand();

		if (m_bLog)
		{
			if (response.has_field(U("rstCode")))
			{
				AMLOGDEVINFO(_T("rstCode : %s"), response.at(U("rstCode")).as_string().c_str());
			}
			if (response.has_field(U("errMsg")))
			{
				AMLOGDEVINFO(_T("errMsg : %s"), response.at(U("errMsg")).as_string().c_str());
			}
		}

	}
	
	return response;
}

web::json::value CESMServer::GetInfo(CString csCamId/* = _T("")*/)
{
	web::json::value data;

	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\Setup\\CameraList.csv"), _T("C:\\Program Files\\ESMLab\\4DMaker"));

	AMLOGDEVINFO(_T("GetInfo : %s"), strInputData);

	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		data[U("rstCode")] = web::json::value::string(U("100"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("NOT_FOUND"));	//에러 메시지
		return data;
	}

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if (iLength == 0)
	{
		data[U("rstCode")] = web::json::value::string(U("100"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("EMPTY"));	//에러 메시지
		return data;
	}

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength - 1] = '\0';
	int len = 0;
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for (int i = 0; i < len; i++)
	{
		if (buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	data[U("rstCode")] = web::json::value::string(U("000"));			//결과 코드
	data[U("errMsg")] = web::json::value::string(U("OK"));				//에러 메시지

	int iPos = 0;
	CString sLine;

	CString camId, camNm, camLoc;
	CString matY[6], resizeY[2], ptY[2], imageY[2];
	CString matUV[6], resizeUV[2], ptUV[2], imageUV[2];
	std::vector<web::json::value> objArr;
		
	CString strNetPath,str4DPSendIP;
	strNetPath.Format(_T("%s\\config\\4DMakerEx.net"), _T("C:\\Program Files\\ESMLab\\4DMaker"));
	
	BOOL bNetExLoad = TRUE;
	CESMIni ini;
	if (!ini.SetIniFilename(strNetPath))
		bNetExLoad = FALSE;

	int nIdx = 0;
	while (1)
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if (sLine == _T(""))
			break;

		AfxExtractSubString(camId, sLine, LIST_CAMID, ',');
		AfxExtractSubString(camLoc, sLine, LIST_ADDR, ',');
		AfxExtractSubString(camNm, sLine, LIST_NAME, ',');

		if (bNetExLoad && camId != _T(""))
		{
			CString str4DPIP;
			str4DPIP = ini.GetString(_T("4DA"), camId);
			
			if (str4DPIP != _T(""))
			{
				str4DPSendIP = ini.GetString(str4DPIP, _T("SendIP"));//KT 전송 IP

				int nFind = str4DPSendIP.ReverseFind('.');
				CString strIP1 = str4DPSendIP.Left(/*strIP.GetLength()-*/nFind);
				CString strRealIP;
				strRealIP.Format(_T("%s.%d"), strIP1, 5);//KT 전송 IP 2->5로 변경

				camLoc = strRealIP;
			}
		}

		AfxExtractSubString(matY[0], sLine, LIST_ADJ_MAT_Y0, ',');
		AfxExtractSubString(matY[1], sLine, LIST_ADJ_MAT_Y1, ',');
		AfxExtractSubString(matY[2], sLine, LIST_ADJ_MAT_Y2, ',');
		AfxExtractSubString(matY[3], sLine, LIST_ADJ_MAT_Y3, ',');
		AfxExtractSubString(matY[4], sLine, LIST_ADJ_MAT_Y4, ',');
		AfxExtractSubString(matY[5], sLine, LIST_ADJ_MAT_Y5, ',');
		AfxExtractSubString(resizeY[0], sLine, LIST_ADJ_RESIZE_YW, ',');
		AfxExtractSubString(resizeY[1], sLine, LIST_ADJ_RESIZE_YH, ',');
		AfxExtractSubString(ptY[0], sLine, LIST_ADJ_PT_YX, ',');
		AfxExtractSubString(ptY[1], sLine, LIST_ADJ_PT_YY, ',');
		AfxExtractSubString(imageY[0], sLine, LIST_ADJ_IMG_YW, ',');
		AfxExtractSubString(imageY[1], sLine, LIST_ADJ_IMG_YH, ',');

		AfxExtractSubString(matUV[0], sLine, LIST_ADJ_MAT_UV0, ',');
		AfxExtractSubString(matUV[1], sLine, LIST_ADJ_MAT_UV1, ',');
		AfxExtractSubString(matUV[2], sLine, LIST_ADJ_MAT_UV2, ',');
		AfxExtractSubString(matUV[3], sLine, LIST_ADJ_MAT_UV3, ',');
		AfxExtractSubString(matUV[4], sLine, LIST_ADJ_MAT_UV4, ',');
		AfxExtractSubString(matUV[5], sLine, LIST_ADJ_MAT_UV5, ',');
		AfxExtractSubString(resizeUV[0], sLine, LIST_ADJ_RESIZE_UVW, ',');
		AfxExtractSubString(resizeUV[1], sLine, LIST_ADJ_RESIZE_UVH, ',');
		AfxExtractSubString(ptUV[0], sLine, LIST_ADJ_PT_UVX, ',');
		AfxExtractSubString(ptUV[1], sLine, LIST_ADJ_PT_UVY, ',');
		AfxExtractSubString(imageUV[0], sLine, LIST_ADJ_IMG_UVW, ',');
		AfxExtractSubString(imageUV[1], sLine, LIST_ADJ_IMG_UVH, ',');
		
		CString camIdx;
		camIdx.Format(_T("%d"), ++nIdx);

		camId.Trim();
		if (camId != _T(""))
		{
			if (csCamId.IsEmpty())
			{
				web::json::value objData;
				objData[U("camIdx")] = web::json::value::string(camIdx.operator LPCWSTR());				//카메라 INDEX   - CamIdx
				objData[U("camId")] = web::json::value::string(camId.operator LPCWSTR());				//카메라 ID   - CamId
				objData[U("camNm")] = web::json::value::string(camNm.operator LPCWSTR());				//카메라 명칭 - IPGroup+CamId
				objData[U("camLoc")] = web::json::value::string(camLoc.operator LPCWSTR());				//카메라 위치 - 4DA IP

				objData[U("matY_0")] = web::json::value::string(matY[0].operator LPCWSTR());
				objData[U("matY_1")] = web::json::value::string(matY[1].operator LPCWSTR());
				objData[U("matY_2")] = web::json::value::string(matY[2].operator LPCWSTR());
				objData[U("matY_3")] = web::json::value::string(matY[3].operator LPCWSTR());
				objData[U("matY_4")] = web::json::value::string(matY[4].operator LPCWSTR());
				objData[U("matY_5")] = web::json::value::string(matY[5].operator LPCWSTR());
				objData[U("resizeY_W")] = web::json::value::string(resizeY[0].operator LPCWSTR());
				objData[U("resizeY_H")] = web::json::value::string(resizeY[1].operator LPCWSTR());
				objData[U("ptY_X")] = web::json::value::string(ptY[0].operator LPCWSTR());
				objData[U("ptY_Y")] = web::json::value::string(ptY[1].operator LPCWSTR());
				objData[U("imgY_W")] = web::json::value::string(imageY[0].operator LPCWSTR());
				objData[U("imgY_H")] = web::json::value::string(imageY[1].operator LPCWSTR());
				
				objData[U("matUV_0")] = web::json::value::string(matUV[0].operator LPCWSTR());
				objData[U("matUV_1")] = web::json::value::string(matUV[1].operator LPCWSTR());
				objData[U("matUV_2")] = web::json::value::string(matUV[2].operator LPCWSTR());
				objData[U("matUV_3")] = web::json::value::string(matUV[3].operator LPCWSTR());
				objData[U("matUV_4")] = web::json::value::string(matUV[4].operator LPCWSTR());
				objData[U("matUV_5")] = web::json::value::string(matUV[5].operator LPCWSTR());
				objData[U("resizeUV_W")] = web::json::value::string(resizeUV[0].operator LPCWSTR());
				objData[U("resizeUV_H")] = web::json::value::string(resizeUV[1].operator LPCWSTR());
				objData[U("ptUV_X")] = web::json::value::string(ptUV[0].operator LPCWSTR());
				objData[U("ptUV_Y")] = web::json::value::string(ptUV[1].operator LPCWSTR());
				objData[U("imgUV_W")] = web::json::value::string(imageUV[0].operator LPCWSTR());
				objData[U("imgUV_H")] = web::json::value::string(imageUV[1].operator LPCWSTR());

				//180104 hjcho
				//objData[U("4DPIP")] = web::json::value::string(str4DPSendIP.operator LPCWSTR());

				objArr.push_back(objData);
			}
			else
			{
				if (csCamId == camId)
				{
					data[U("camIdx")] = web::json::value::string(camIdx.operator LPCWSTR());				//카메라 INDEX   - CamIdx
					data[U("camId")] = web::json::value::string(camId.operator LPCWSTR());				//카메라 ID   - CamId
					data[U("camNm")] = web::json::value::string(camNm.operator LPCWSTR());					//카메라 명칭
					data[U("camLoc")] = web::json::value::string(camLoc.operator LPCWSTR());				//카메라 위치

					data[U("matY_0")] = web::json::value::string(matY[0].operator LPCWSTR());
					data[U("matY_1")] = web::json::value::string(matY[1].operator LPCWSTR());
					data[U("matY_2")] = web::json::value::string(matY[2].operator LPCWSTR());
					data[U("matY_3")] = web::json::value::string(matY[3].operator LPCWSTR());
					data[U("matY_4")] = web::json::value::string(matY[4].operator LPCWSTR());
					data[U("matY_5")] = web::json::value::string(matY[5].operator LPCWSTR());
					data[U("resizeY_W")] = web::json::value::string(resizeY[0].operator LPCWSTR());
					data[U("resizeY_H")] = web::json::value::string(resizeY[1].operator LPCWSTR());
					data[U("ptY_X")] = web::json::value::string(ptY[0].operator LPCWSTR());
					data[U("ptY_Y")] = web::json::value::string(ptY[1].operator LPCWSTR());
					data[U("imgY_W")] = web::json::value::string(imageY[0].operator LPCWSTR());
					data[U("imgY_H")] = web::json::value::string(imageY[1].operator LPCWSTR());

					data[U("matUV_0")] = web::json::value::string(matUV[0].operator LPCWSTR());
					data[U("matUV_1")] = web::json::value::string(matUV[1].operator LPCWSTR());
					data[U("matUV_2")] = web::json::value::string(matUV[2].operator LPCWSTR());
					data[U("matUV_3")] = web::json::value::string(matUV[3].operator LPCWSTR());
					data[U("matUV_4")] = web::json::value::string(matUV[4].operator LPCWSTR());
					data[U("matUV_5")] = web::json::value::string(matUV[5].operator LPCWSTR());
					data[U("resizeUV_W")] = web::json::value::string(resizeUV[0].operator LPCWSTR());
					data[U("resizeUV_H")] = web::json::value::string(resizeUV[1].operator LPCWSTR());
					data[U("ptUV_X")] = web::json::value::string(ptUV[0].operator LPCWSTR());
					data[U("ptUV_Y")] = web::json::value::string(ptUV[1].operator LPCWSTR());
					data[U("imgUV_W")] = web::json::value::string(imageUV[0].operator LPCWSTR());
					data[U("imgUV_H")] = web::json::value::string(imageUV[1].operator LPCWSTR());

					//180104 hjcho
					//data[U("4DPIP")] = web::json::value::string(str4DPSendIP.operator LPCWSTR());

					break;
				}

			}
		}
		else
			break;
	}

	if (objArr.size() > 0)
	{
		data[U("camList")] = web::json::value::array(objArr);											//카메라 목록
	}


	return data;
}
web::json::value CESMServer::GetStatus(CString csCamId/* = _T("")*/)
{
	web::json::value data;

	CFile ReadFile;
	CString strInputData;
	strInputData.Format(_T("%s\\Setup\\CameraState.csv"), _T("C:\\Program Files\\ESMLab\\4DMaker"));

	AMLOGDEVINFO(_T("GetStatus : %s"), strInputData);

	if (!ReadFile.Open(strInputData, CFile::modeRead))
	{
		data[U("rstCode")] = web::json::value::string(U("100"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("NOT_FOUND"));	//에러 메시지
		return data;
	}
		

	CString sBuffer;
	INT iLength = (INT)(ReadFile.GetLength());

	if (iLength == 0)
	{
		data[U("rstCode")] = web::json::value::string(U("100"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("EMPTY"));	//에러 메시지
		return data;
	}
		

	char* pBuffer = new char[iLength];
	ReadFile.Read(pBuffer, iLength);

	pBuffer[iLength - 1] = '\0';
	int len = 0;
	BSTR buf;
	len = MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, NULL, NULL);
	buf = SysAllocStringLen(NULL, len);
	MultiByteToWideChar(CP_ACP, 0, pBuffer, iLength, buf, len);
	for (int i = 0; i < len; i++)
	{
		if (buf[i] == 0)
			buf[i] = ' ';
	}
	sBuffer.Format(_T("%s"), buf);
	delete[] pBuffer;
	pBuffer = NULL;
	ReadFile.Close();

	data[U("rstCode")] = web::json::value::string(U("000"));			//결과 코드
	data[U("errMsg")] = web::json::value::string(U("OK"));				//에러 메시지

	int iPos = 0;
	CString sLine;

	CString camId, camState;

	CString strNetPath, str4DPSendIP;
	strNetPath.Format(_T("%s\\config\\4DMakerEx.net"), _T("C:\\Program Files\\ESMLab\\4DMaker"));

	BOOL bNetExLoad = TRUE;
	CESMIni ini;
	if (!ini.SetIniFilename(strNetPath))
		bNetExLoad = FALSE;

	std::vector<web::json::value> objArr;
	int nIdx = 0;
	while (1)
	{
		sLine = sBuffer.Tokenize(_T("\r\n"), iPos);

		if (sLine == _T(""))
			break;

		AfxExtractSubString(camId, sLine, 0, ',');
		AfxExtractSubString(camState, sLine, 1, ',');
		
		if (bNetExLoad && camId != _T(""))
		{
			CString str4DPIP;
			str4DPIP = ini.GetString(_T("4DA"), camId);

			if (str4DPIP != _T(""))
			{
				str4DPSendIP = ini.GetString(str4DPIP, _T("SendIP"));//KT 전송 IP

				int nFind = str4DPSendIP.ReverseFind('.');
				CString strIP1 = str4DPSendIP.Left(/*strIP.GetLength()-*/nFind);
				CString strRealIP;
				strRealIP.Format(_T("%s.%d"), strIP1, 5);//KT 전송 IP 2->5로 변경

				//camLoc = strRealIP;
			}
		}
		camId.Trim();
		camState.Trim();
		
		CString camIdx;
		camIdx.Format(_T("%d"), ++nIdx);

		if (camId != _T(""))
		{
			CString csCode;
			csCode = GetStateCode(camState);

			if (csCamId.IsEmpty())
			{
				web::json::value objData;
				objData[U("camIdx")] = web::json::value::string(camIdx.operator LPCWSTR());				//카메라 INDEX   - CamIdx
				objData[U("camId")] = web::json::value::string(camId.operator LPCWSTR());					//카메라 ID
				objData[U("camState")] = web::json::value::string(csCode.operator LPCWSTR());				//카메라 상태
				objArr.push_back(objData);
			}
			else
			{
				if (csCamId == camId)
				{
					data[U("camIdx")] = web::json::value::string(camIdx.operator LPCWSTR());				//카메라 INDEX   - CamIdx
					data[U("camId")] = web::json::value::string(camId.operator LPCWSTR());					//카메라 ID
					data[U("camState")] = web::json::value::string(csCode.operator LPCWSTR());				//카메라 상태
					break;
				}
				
			}
		}
		else
			break;
	}

	if (objArr.size() > 0)
	{
		data[U("camList")] = web::json::value::array(objArr);												//카메라 목록
	}

	return data;
}
web::json::value CESMServer::GetCommand()
{
	web::json::value data;

	data[U("rstCode")] = web::json::value::string(U("000"));			//결과 코드
	data[U("errMsg")] = web::json::value::string(U("OK"));				//에러 메시지

	return data;
}

CString CESMServer::GetStateCode(CString csState)
{
	/*1:시작, 2:종료, 9:장애발생*/
	CString csCode;

	if (csState == _T("Recording ..."))
	{
		csCode = _T("1");
	}
	else if (csState == _T("Ready to Edit"))
	{
		csCode = _T("2");
	}
	else if (csState == _T("Connect"))
	{
		csCode = _T("3");
	}
	else if (csState == _T("Disconnected"))
	{
		csCode = _T("4");
	}
	/*else if (csState == _T("Unknown"))
	{
		csCode = _T("5");
	}*/
	else
	{
		csCode = _T("9");	//Error
	}

	return csCode;
}

void CESMServer::Set4DMaker(CString csCaption)
{
	m_cs4DMaker = csCaption;
}
CString CESMServer::Get4DMaker()
{
	return m_cs4DMaker;
}
void CESMServer::SetHttpIp(CString csIp)
{
	m_csServerIp = csIp;
}
CString CESMServer::GetHttpIp()
{
	return m_csServerIp;
}
void CESMServer::SetHttpPort(CString csPort)
{
	m_csServerPort = csPort;
}
CString CESMServer::GetHttpPort()
{
	return m_csServerPort;
}
void CESMServer::SetEanbleLog(BOOL bLog)
{
	m_bLog = bLog;
}

void CESMServer::SetURL(CString csUrl)
{
	m_csURL = csUrl;
}
CString CESMServer::GetURL()
{
	return m_csURL;
}


web::json::value CESMServer::GetDBResponse(CString csState, CString csValue/* = _T("")*/)
{
	web::json::value response;

	if (csState == _T("info"))	//정보
	{
		response = GetVodInfo();			//CameraList.csv

		if (m_bLog)
		{
			if (response.has_field(U("rstCode")))
			{
				AMLOGDEVINFO(_T("rstCode : %s"), response.at(U("rstCode")).as_string().c_str());
			}
			if (response.has_field(U("errMsg")))
			{
				AMLOGDEVINFO(_T("errMsg : %s"), response.at(U("errMsg")).as_string().c_str());
			}
			/*if (response.has_field(U("camList")))
			{
				AMLOGDEVINFO(_T("camList : %d"), response.at(U("camList")).as_array().size());

				for (int i = 0; i < response.at(U("camList")).as_array().size(); i++)
				{
					web::json::value v = response.at(U("camList")).at(i);
					if (v.has_field(U("camId")))
					{
						AMLOGDEVINFO(_T("camId : %s"), v.at(U("camId")).as_string().c_str());
					}
					if (v.has_field(U("camNm")))
					{
						AMLOGDEVINFO(_T("camNm : %s"), v.at(U("camNm")).as_string().c_str());
					}
					if (v.has_field(U("camLoc")))
					{
						AMLOGDEVINFO(_T("camLoc : %s"), v.at(U("camLoc")).as_string().c_str());
					}
				}
			}
			else
			{
				if (response.has_field(U("camNm")))
				{
					AMLOGDEVINFO(_T("camNm : %s"), response.at(U("camNm")).as_string().c_str());
				}
				if (response.has_field(U("camLoc")))
				{
					AMLOGDEVINFO(_T("camLoc : %s"), response.at(U("camLoc")).as_string().c_str());
				}

			}*/
		}

	}
	

	return response;
}

BOOL CESMServer::ConnectMySQL()
{
	CString strDatabase, strId, strPwd, strName;
	strDatabase = DB_ADDRESS;
	strId = DB_ID;
	strPwd = DB_PASS;
	strName = DB_NAME;

	char* pDbAddrIp = ESMUtil::CStringToChar(strDatabase);
	char* pDbID = ESMUtil::CStringToChar(strId);
	char* pDbPW = ESMUtil::CStringToChar(strPwd);
	char* pDbName = ESMUtil::CStringToChar(strName);

	mysql_init(&mysql);

	if (!mysql_real_connect(&mysql, pDbAddrIp, pDbID, pDbPW, pDbName, DB_PORT, 0, 0))
	{
		//char errMsg[MAX_PATH];
		const char * errM = mysql_error(&mysql);
		CString strMsg;
		strMsg = ESMUtil::CharToCString((char *)errM);
		AMLOGERROR((TCHAR*)(LPCTSTR)strMsg);
		return FALSE;
	}
	mysql_query(&mysql, "set Names EucKR");
	
	CString strQuery;
	strQuery.Format(_T("use "), DB_NAME);
	char * pQuery = NULL;
	pQuery = ESMUtil::CStringToChar(strQuery);
	//mysql_query(&mysql, "use esm");
	mysql_query(&mysql, pQuery);
	delete[] pQuery;


	delete[] pDbAddrIp;
	delete[] pDbID;
	delete[] pDbPW;
	delete[] pDbName;

	return TRUE;
}
void CESMServer::DisconnectMySQL()
{
	mysql_close(&mysql);
}

web::json::value CESMServer::GetVodInfo()
{
	web::json::value data;
	
	int nSize = -1;
	nSize = data.size();

	CString strDatabase, strId, strPwd, strName;
	strDatabase = DB_ADDRESS;
	strId = DB_ID;
	strPwd = DB_PASS;
	strName = DB_NAME;

	char* pDbAddrIp = ESMUtil::CStringToChar(strDatabase);
	char* pDbID = ESMUtil::CStringToChar(strId);
	char* pDbPW = ESMUtil::CStringToChar(strPwd);
	char* pDbName = ESMUtil::CStringToChar(strName);

	mysql_init(&mysql);

	if (!mysql_real_connect(&mysql, pDbAddrIp, pDbID, pDbPW, pDbName, DB_PORT, 0, 0))
	{
		//char errMsg[MAX_PATH];
		const char * errM = mysql_error(&mysql);
		CString strMsg;
		strMsg = ESMUtil::CharToCString((char *)errM);
		AMLOGERROR((TCHAR*)(LPCTSTR)strMsg);
		return data;
	}
	mysql_query(&mysql, "set Names EucKR");

	CString strQuery;
	strQuery.Format(_T("use %s"), strName);
	char * pQuery = NULL;
	pQuery = ESMUtil::CStringToChar(strQuery);
	//mysql_query(&mysql, "use esm");
	//mysql_query(&mysql, pQuery);
	delete[] pQuery;
	pQuery = NULL;

	delete[] pDbAddrIp;
	delete[] pDbID;
	delete[] pDbPW;
	delete[] pDbName;
	
	MYSQL_ROW row;
	MYSQL_RES *m_res = NULL;

	int nMax = 0, nNum = 0;
	//CString strQuery;
	strQuery.Format(_T("SELECT 4d_live.`num`, 4d_live.`hit`, 4d_live.`mode`, 4d_live.`sub_l`, 4d_live.`sub_m`, 4d_live.`sub_s`, \
		4d_live.`date`, 4d_live.`time`, 4d_live.`v_count`, 4d_live.`h_count`, 4d_live.`object`, 4d_live.`explain`, 4d_live.`playback` FROM %s.4d_live WHERE 4d_live.`hit` = 0"), strName);
	pQuery = ESMUtil::CStringToChar(strQuery); 
	//AMLOGDEVINFO((TCHAR*)(LPCTSTR)strQuery);
	
	if (mysql_query(&mysql, pQuery))
	{
		delete[] pQuery;

		data[U("rstCode")] = web::json::value::string(U("005"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("FAIL"));			//에러 메시지

		return data;
	}
	delete[] pQuery;

	if ((m_res = mysql_store_result(&mysql)) == NULL)
	{
		data[U("rstCode")] = web::json::value::string(U("010"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("EMPTY"));			//에러 메시지

		return data;
	}

	my_ulonglong nRow = 0;
	nRow = mysql_num_rows(m_res);

	data[U("rstCode")] = web::json::value::string(U("000"));			//결과 코드
	data[U("errMsg")] = web::json::value::string(U("OK"));				//에러 메시지

	UINT nFiled = 0;
	nFiled = mysql_num_fields(m_res);

	std::vector<web::json::value> objArr;

	while ((row = mysql_fetch_row(m_res)) != NULL)
	{
		CString strMsg;


		CString strNum, strHit, strMode, strSubL, strSubM, strSubS, \
			strDate, strTime, strVCount, strHCount, strObject, strExplain, strPlayback;


		strNum = row[0];
		strHit = row[1];
		strMode = row[2];
		strSubL = row[3];
		strSubM = row[4];
		strSubS = row[5];
		strDate = row[6];
		strTime = row[7];
		strVCount = row[8];
		strHCount = row[9];
		strObject = row[10];
		strExplain = row[11];
		strPlayback = row[12];

		nNum = _ttoi(strNum);

		if (nMax < nNum)
		{
			nMax = nNum;
		}

		/*strMsg.Format(_T("%s  %s, %s\r\n%s, %s, %s\r\n%s, %s\r\n%s, %s\r\n%s, %s"), strNum, strHit, strMode, strSubL, strSubM, strSubS, strDate, strTime, strVCount, strHCount, strObject, strExplain);
		AfxMessageBox(strMsg);*/
				
		web::json::value objData;
		
		objData[U("num")] = web::json::value::string(strNum.operator LPCWSTR());
		objData[U("hit")] = web::json::value::string(strHit.operator LPCWSTR());
		objData[U("mode")] = web::json::value::string(strMode.operator LPCWSTR());

		objData[U("sub_l")] = web::json::value::string(strSubL.operator LPCWSTR());
		objData[U("sub_m")] = web::json::value::string(strSubM.operator LPCWSTR());
		objData[U("sub_s")] = web::json::value::string(strSubS.operator LPCWSTR());

		objData[U("date")] = web::json::value::string(strDate.operator LPCWSTR());
		objData[U("time")] = web::json::value::string(strTime.operator LPCWSTR());

		objData[U("v_count")] = web::json::value::string(strVCount.operator LPCWSTR());
		objData[U("h_count")] = web::json::value::string(strHCount.operator LPCWSTR());

		objData[U("object")] = web::json::value::string(strObject.operator LPCWSTR());
		objData[U("explain")] = web::json::value::string(strExplain.operator LPCWSTR());

		objData[U("playback")] = web::json::value::string(strPlayback.operator LPCWSTR());

		objArr.push_back(objData);
	}

	mysql_free_result(m_res);

	if (objArr.size() > 0)
	{
		data[U("Data")] = web::json::value::array(objArr);

		strQuery.Format(_T("UPDATE %s.`4d_live` SET hit = hit + 1 WHERE hit = 0 AND num <= %d"), strName, nMax);

		pQuery = ESMUtil::CStringToChar(strQuery);
		//AMLOGDEVINFO((TCHAR*)(LPCTSTR)strQuery);
		if (mysql_query(&mysql, pQuery))
		{
			delete[] pQuery;

			data[U("rstCode")] = web::json::value::string(U("015"));			//결과 코드
			data[U("errMsg")] = web::json::value::string(U("FAIL"));			//에러 메시지

			return data;
		}
		delete[] pQuery;
	}

	nSize = data.size();

	mysql_close(&mysql);

	return data;
}

web::json::value CESMServer::GetVodInfo_ALL()
{
	web::json::value data;

	int nSize = -1;
	nSize = data.size();

	CString strDatabase, strId, strPwd, strName;
	strDatabase = DB_ADDRESS;
	strId = DB_ID;
	strPwd = DB_PASS;
	strName = DB_NAME;

	char* pDbAddrIp = ESMUtil::CStringToChar(strDatabase);
	char* pDbID = ESMUtil::CStringToChar(strId);
	char* pDbPW = ESMUtil::CStringToChar(strPwd);
	char* pDbName = ESMUtil::CStringToChar(strName);

	mysql_init(&mysql);

	if (!mysql_real_connect(&mysql, pDbAddrIp, pDbID, pDbPW, pDbName, DB_PORT, 0, 0))
	{
		const char * errM = mysql_error(&mysql);
		CString strMsg;
		strMsg = ESMUtil::CharToCString((char *)errM);
		AMLOGERROR((TCHAR*)(LPCTSTR)strMsg);
		return data;
	}
	mysql_query(&mysql, "set Names EucKR");

	CString strQuery;
	strQuery.Format(_T("use %s"), strName);
	char * pQuery = NULL;
	pQuery = ESMUtil::CStringToChar(strQuery);
	mysql_query(&mysql, pQuery);
	delete[] pQuery;
	pQuery = NULL;

	delete[] pDbAddrIp;
	delete[] pDbID;
	delete[] pDbPW;
	delete[] pDbName; 

	MYSQL_ROW row;
	MYSQL_RES *m_res = NULL;

	int nMax = 0, nNum = 0;
	//CString strQuery;
	strQuery.Format(_T("SELECT 4d_live.`num`, 4d_live.`hit`, 4d_live.`mode`, 4d_live.`sub_l`, 4d_live.`sub_m`, 4d_live.`sub_s`, \
		4d_live.`date`, 4d_live.`time`, 4d_live.`v_count`, 4d_live.`h_count`, 4d_live.`object`, 4d_live.`explain`, 4d_live.`playback` FROM %s.4d_live"), strName);
	pQuery = ESMUtil::CStringToChar(strQuery);
	//AMLOGDEVINFO((TCHAR*)(LPCTSTR)strQuery);

	if (mysql_query(&mysql, pQuery))
	{
		delete[] pQuery;

		data[U("rstCode")] = web::json::value::string(U("005"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("FAIL"));			//에러 메시지

		return data;
	}
	delete[] pQuery;

	if ((m_res = mysql_store_result(&mysql)) == NULL)
	{
		data[U("rstCode")] = web::json::value::string(U("010"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("EMPTY"));			//에러 메시지

		return data;
	}

	my_ulonglong nRow = 0;
	nRow = mysql_num_rows(m_res);

	data[U("rstCode")] = web::json::value::string(U("000"));			//결과 코드
	data[U("errMsg")] = web::json::value::string(U("OK"));				//에러 메시지

	UINT nFiled = 0;
	nFiled = mysql_num_fields(m_res);

	std::vector<web::json::value> objArr;

	int nCnt = 0;
	while ((row = mysql_fetch_row(m_res)) != NULL)
	{
		CString strMsg;


		CString strNum, strHit, strMode, strSubL, strSubM, strSubS, \
			strDate, strTime, strVCount, strHCount, strObject, strExplain, strPlayback;


		strNum = row[0];
		strHit = row[1];
		strMode = row[2];
		strSubL = row[3];
		strSubM = row[4];
		strSubS = row[5];
		strDate = row[6];
		strTime = row[7];
		strVCount = row[8];
		strHCount = row[9];
		strObject = row[10];
		strExplain = row[11];
		strPlayback = row[12];

		nNum = _ttoi(strNum);

		if (nMax < nNum)
		{
			nMax = nNum;
		}

		/*strMsg.Format(_T("[%d] %s  %s, %s\r\n%s, %s, %s\r\n%s, %s\r\n%s, %s\r\n%s, %s"), ++nCnt, strNum, strHit, strMode, strSubL, strSubM, strSubS, strDate, strTime, strVCount, strHCount, strObject, strExplain);
		AfxMessageBox(strMsg);*/

		web::json::value objData;


		objData[U("num")] = web::json::value::string(strNum.operator LPCWSTR());
		objData[U("hit")] = web::json::value::string(strHit.operator LPCWSTR());
		objData[U("mode")] = web::json::value::string(strMode.operator LPCWSTR());

		objData[U("sub_l")] = web::json::value::string(strSubL.operator LPCWSTR());
		objData[U("sub_m")] = web::json::value::string(strSubM.operator LPCWSTR());
		objData[U("sub_s")] = web::json::value::string(strSubS.operator LPCWSTR());

		objData[U("date")] = web::json::value::string(strDate.operator LPCWSTR());
		objData[U("time")] = web::json::value::string(strTime.operator LPCWSTR());

		objData[U("v_count")] = web::json::value::string(strVCount.operator LPCWSTR());
		objData[U("h_count")] = web::json::value::string(strHCount.operator LPCWSTR());

		objData[U("object")] = web::json::value::string(strObject.operator LPCWSTR());
		objData[U("explain")] = web::json::value::string(strExplain.operator LPCWSTR());

		objData[U("playback")] = web::json::value::string(strPlayback.operator LPCWSTR());

		objArr.push_back(objData);
		objArr.push_back(objData);
	}

	mysql_free_result(m_res);

	if (objArr.size() > 0)
	{
		data[U("Data")] = web::json::value::array(objArr);
	}
	
	mysql_close(&mysql);

	return data;
}

web::json::value CESMServer::GetContentsAll()
{
	web::json::value data;

	int nSize = -1;
	nSize = data.size();

	CString strDatabase, strId, strPwd, strName;
	strDatabase = DB_ADDRESS;
	strId = DB_ID;
	strPwd = DB_PASS;
	strName = DB_NAME;

	char* pDbAddrIp = ESMUtil::CStringToChar(strDatabase);
	char* pDbID = ESMUtil::CStringToChar(strId);
	char* pDbPW = ESMUtil::CStringToChar(strPwd);
	char* pDbName = ESMUtil::CStringToChar(strName);

	mysql_init(&mysql);

	if (!mysql_real_connect(&mysql, pDbAddrIp, pDbID, pDbPW, pDbName, DB_PORT, 0, 0))
	{
		//char errMsg[MAX_PATH];
		const char * errM = mysql_error(&mysql);
		CString strMsg;
		strMsg = ESMUtil::CharToCString((char *)errM);
		AMLOGERROR((TCHAR*)(LPCTSTR)strMsg);
		return data;
	}
	mysql_query(&mysql, "set Names EucKR");

	CString strQuery;
	strQuery.Format(_T("use %s"), strName);
	char * pQuery = NULL;
	pQuery = ESMUtil::CStringToChar(strQuery);
	mysql_query(&mysql, pQuery);
	delete[] pQuery;
	pQuery = NULL;

	delete[] pDbAddrIp;
	delete[] pDbID;
	delete[] pDbPW;
	delete[] pDbName;

	MYSQL_ROW row;
	MYSQL_RES *m_res = NULL;

	int nMax = 0, nNum = 0;
	//CString strQuery;
	strQuery.Format(_T("SELECT 4d_live.`num`, 4d_live.`hit`, 4d_live.`mode`, 4d_live.`sub_l`, 4d_live.`sub_m`, 4d_live.`sub_s`, \
		4d_live.`date`, 4d_live.`time`, 4d_live.`v_count`, 4d_live.`h_count`, 4d_live.`object`, 4d_live.`explain`, 4d_live.`playback` FROM %s.4d_live"), strName);
	pQuery = ESMUtil::CStringToChar(strQuery);
	//AMLOGDEVINFO((TCHAR*)(LPCTSTR)strQuery);

	if (mysql_query(&mysql, pQuery))
	{
		delete[] pQuery;

		data[U("rstCode")] = web::json::value::string(U("005"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("FAIL"));			//에러 메시지

		return data;
	}
	delete[] pQuery;

	if ((m_res = mysql_store_result(&mysql)) == NULL)
	{
		data[U("rstCode")] = web::json::value::string(U("010"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("EMPTY"));			//에러 메시지

		return data;
	}

	my_ulonglong nRow = 0;
	nRow = mysql_num_rows(m_res);
	if (nRow == 0)
	{
		data[U("rstCode")] = web::json::value::string(U("010"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("EMPTY"));			//에러 메시지

		return data;
	}

	data[U("rstCode")] = web::json::value::string(U("000"));			//결과 코드
	data[U("errMsg")] = web::json::value::string(U("OK"));				//에러 메시지

	UINT nFiled = 0;
	nFiled = mysql_num_fields(m_res);

	std::vector<web::json::value> objArr;

	int nCnt = 0;
	while ((row = mysql_fetch_row(m_res)) != NULL)
	{
		CString strMsg;


		CString strNum, strHit, strMode, strSubL, strSubM, strSubS, \
			strDate, strTime, strVCount, strHCount, strObject, strExplain, strPlayback;

		strNum = row[0];
		strHit = row[1];
		strMode = row[2];
		strSubL = row[3];
		strSubM = row[4];
		strSubS = row[5];
		strDate = row[6];
		strTime = row[7];
		strVCount = row[8];
		strHCount = row[9];
		strObject = row[10];
		strExplain = row[11];
		strPlayback = row[12];

		nNum = _ttoi(strNum);

		if (nMax < nNum)
		{
			nMax = nNum;
		}

		/*strMsg.Format(_T("[%d] %s  %s, %s\r\n%s, %s, %s\r\n%s, %s\r\n%s, %s\r\n%s, %s"), ++nCnt, strNum, strHit, strMode, strSubL, strSubM, strSubS, strDate, strTime, strVCount, strHCount, strObject, strExplain);
		AfxMessageBox(strMsg);*/

		web::json::value objData;


		objData[U("num")] = web::json::value::string(strNum.operator LPCWSTR());
		objData[U("hit")] = web::json::value::string(strHit.operator LPCWSTR());
		objData[U("mode")] = web::json::value::string(strMode.operator LPCWSTR());

		objData[U("sub_l")] = web::json::value::string(strSubL.operator LPCWSTR());
		objData[U("sub_m")] = web::json::value::string(strSubM.operator LPCWSTR());
		objData[U("sub_s")] = web::json::value::string(strSubS.operator LPCWSTR());

		objData[U("date")] = web::json::value::string(strDate.operator LPCWSTR());
		objData[U("time")] = web::json::value::string(strTime.operator LPCWSTR());

		objData[U("v_count")] = web::json::value::string(strVCount.operator LPCWSTR());
		objData[U("h_count")] = web::json::value::string(strHCount.operator LPCWSTR());

		objData[U("object")] = web::json::value::string(strObject.operator LPCWSTR());
		objData[U("explain")] = web::json::value::string(strExplain.operator LPCWSTR());

		objData[U("playback")] = web::json::value::string(strPlayback.operator LPCWSTR());

		objArr.push_back(objData);
		//objArr.push_back(objData);
	}

	mysql_free_result(m_res);

	if (objArr.size() > 0)
	{
		data[U("arrList")] = web::json::value::array(objArr);
	}

	mysql_close(&mysql);

	return data;
}

web::json::value CESMServer::GetCategory(CString csData)
{
	web::json::value data;
	if (csData.IsEmpty())
	{
		data[U("rstCode")] = web::json::value::string(U("015"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("VALUE EMPTY"));
		return data;
	}

	int nSize = -1;
	nSize = data.size();

	CString strDatabase, strId, strPwd, strName;
	strDatabase = DB_ADDRESS;
	strId = DB_ID;
	strPwd = DB_PASS;
	strName = DB_NAME;

	char* pDbAddrIp = ESMUtil::CStringToChar(strDatabase);
	char* pDbID = ESMUtil::CStringToChar(strId);
	char* pDbPW = ESMUtil::CStringToChar(strPwd);
	char* pDbName = ESMUtil::CStringToChar(strName);

	mysql_init(&mysql);

	if (!mysql_real_connect(&mysql, pDbAddrIp, pDbID, pDbPW, pDbName, DB_PORT, 0, 0))
	{
		//char errMsg[MAX_PATH];
		const char * errM = mysql_error(&mysql);
		CString strMsg;
		strMsg = ESMUtil::CharToCString((char *)errM);
		AMLOGERROR((TCHAR*)(LPCTSTR)strMsg);
		return data;
	}
	mysql_query(&mysql, "set Names EucKR");

	CString strQuery;
	strQuery.Format(_T("use %s"), strName);
	char * pQuery = NULL;
	pQuery = ESMUtil::CStringToChar(strQuery);
	mysql_query(&mysql, pQuery);
	delete[] pQuery;
	pQuery = NULL;

	delete[] pDbAddrIp;
	delete[] pDbID;
	delete[] pDbPW;
	delete[] pDbName;

	MYSQL_ROW row;
	MYSQL_RES *m_res = NULL;

	int nMax = 0, nNum = 0;
	//CString strQuery;
	strQuery.Format(_T("SELECT DISTINCT 4d_live.`%s` FROM %s.4d_live"), csData, strName);
	pQuery = ESMUtil::CStringToChar(strQuery);
	//AMLOGDEVINFO((TCHAR*)(LPCTSTR)strQuery);

	if (mysql_query(&mysql, pQuery))
	{
		delete[] pQuery;

		data[U("rstCode")] = web::json::value::string(U("005"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("FAIL"));			//에러 메시지

		return data;
	}
	delete[] pQuery;

	if ((m_res = mysql_store_result(&mysql)) == NULL)
	{
		data[U("rstCode")] = web::json::value::string(U("010"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("EMPTY"));			//에러 메시지

		return data;
	}

	my_ulonglong nRow = 0;
	nRow = mysql_num_rows(m_res);
	if (nRow == 0)
	{
		data[U("rstCode")] = web::json::value::string(U("010"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("EMPTY"));			//에러 메시지

		return data;
	}

	data[U("rstCode")] = web::json::value::string(U("000"));			//결과 코드
	data[U("errMsg")] = web::json::value::string(U("OK"));				//에러 메시지

	UINT nFiled = 0;
	nFiled = mysql_num_fields(m_res);

	std::vector<web::json::value> objArr;

	int nCnt = 0;
	while ((row = mysql_fetch_row(m_res)) != NULL)
	{
		CString strMsg;
		CString strCategory;
		
		strCategory = row[0];
		
		web::json::value objData;


		objData[U("category")] = web::json::value::string(strCategory.operator LPCWSTR());
		
		objArr.push_back(objData);
	}

	mysql_free_result(m_res);

	if (objArr.size() > 0)
	{
		data[U("arrList")] = web::json::value::array(objArr);
	}

	mysql_close(&mysql);

	return data;
}

web::json::value CESMServer::GetSearch(CString csData)
{
	setlocale(LC_ALL, "");

	web::json::value data;
	if (csData.IsEmpty())
	{
		data[U("rstCode")] = web::json::value::string(U("015"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("VALUE EMPTY"));
		return data;
	}

	CString csSearch[3];
	CString csCategory[3];
	CString csValue[3];

	CString csWhere = _T("WHERE");
	
	AfxExtractSubString(csSearch[0], csData, 0, '&');
	AfxExtractSubString(csCategory[0], csSearch[0], 0, '=');
	AfxExtractSubString(csValue[0], csSearch[0], 1, '=');
	if (csSearch[0].IsEmpty() != true)
	{
		/*CString csData;
		std::wstring wsTmp = csValue[0].operator LPCWSTR();
		std::wstring wsValue = uri::decode(wsTmp);
		csData = wsValue.c_str();*/

		if (csCategory[0].IsEmpty() || csValue[0].IsEmpty())
		{
			data[U("rstCode")] = web::json::value::string(U("015"));			//결과 코드
			data[U("errMsg")] = web::json::value::string(U("VALUE EMPTY"));
			return data;
		}

		CString csTemp;
		csTemp.Format(_T(" 4d_live.`%s` LIKE '%%%s%%'"), csCategory[0], csValue[0]);
		csWhere.Append(csTemp);
	}

	AfxExtractSubString(csSearch[1], csData, 1, '&');
	AfxExtractSubString(csCategory[1], csSearch[1], 0, '=');
	AfxExtractSubString(csValue[1], csSearch[1], 1, '=');
	if (csSearch[1].IsEmpty() != true)
	{
		/*CString csData;
		std::wstring wsTmp = csValue[1].operator LPCWSTR();
		std::wstring wsValue = uri::decode(wsTmp);
		csData = wsValue.c_str();*/

		if (csCategory[1].IsEmpty() || csValue[1].IsEmpty())
		{
			data[U("rstCode")] = web::json::value::string(U("015"));			//결과 코드
			data[U("errMsg")] = web::json::value::string(U("VALUE EMPTY"));
			return data;
		}
		
		CString csTemp;
		csTemp.Format(_T(" AND 4d_live.`%s` LIKE '%%%s%%'"), csCategory[1], csValue[1]);
		csWhere.Append(csTemp);
	}

	AfxExtractSubString(csSearch[2], csData, 2, '&');
	AfxExtractSubString(csCategory[2], csSearch[2], 0, '=');
	AfxExtractSubString(csValue[2], csSearch[2], 1, '=');
	if (csSearch[2].IsEmpty() != true)
	{
		/*CString csData;
		std::wstring wsTmp = csValue[2].operator LPCWSTR();
		std::wstring wsValue = uri::decode(wsTmp);
		csData = wsValue.c_str();*/

		if (csCategory[2].IsEmpty() || csValue[2].IsEmpty())
		{
			data[U("rstCode")] = web::json::value::string(U("015"));			//결과 코드
			data[U("errMsg")] = web::json::value::string(U("VALUE EMPTY"));
			return data;
		}

		CString csTemp;
		csTemp.Format(_T(" AND 4d_live.`%s` LIKE '%%%s%%'"), csCategory[2], csValue[2]);
		csWhere.Append(csTemp);
	}

	if (csCategory[0].CompareNoCase(_T("search")) == 0)
	{
		csWhere.Format(_T("WHERE 4d_live.`%s` LIKE '%%%s%%' OR 4d_live.`%s` LIKE '%%%s%%' OR 4d_live.`%s` LIKE '%%%s%%'"),_T("sub_s"), csValue[0], _T("object"), csValue[0], _T("explain"), csValue[0]);
	}


	int nSize = -1;
	nSize = data.size();

	CString strDatabase, strId, strPwd, strName;
	strDatabase = DB_ADDRESS;
	strId = DB_ID;
	strPwd = DB_PASS;
	strName = DB_NAME;

	char* pDbAddrIp = ESMUtil::CStringToChar(strDatabase);
	char* pDbID = ESMUtil::CStringToChar(strId);
	char* pDbPW = ESMUtil::CStringToChar(strPwd);
	char* pDbName = ESMUtil::CStringToChar(strName);

	mysql_init(&mysql);

	if (!mysql_real_connect(&mysql, pDbAddrIp, pDbID, pDbPW, pDbName, DB_PORT, 0, 0))
	{
		//char errMsg[MAX_PATH];
		const char * errM = mysql_error(&mysql);
		CString strMsg = _T("");
		strMsg = ESMUtil::CharToCString((char *)errM);
		AMLOGERROR((TCHAR*)(LPCTSTR)strMsg);

		data[U("rstCode")] = web::json::value::string(U("001"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("FAIL"));

		return data;
	}
	mysql_query(&mysql, "set Names EucKR");

	CString strQuery = _T("");
	strQuery.Format(_T("use %s"), strName);
	char * pQuery = NULL;
	pQuery = ESMUtil::CStringToChar(strQuery);
	mysql_query(&mysql, pQuery);
	delete[] pQuery;
	pQuery = NULL;

	delete[] pDbAddrIp;
	delete[] pDbID;
	delete[] pDbPW;
	delete[] pDbName;

	MYSQL_ROW row;
	MYSQL_RES *m_res = NULL;

	int nMax = 0, nNum = 0;
	//CString strQuery;
	strQuery.Format(_T("SELECT 4d_live.`num`, 4d_live.`hit`, 4d_live.`mode`, 4d_live.`sub_l`, 4d_live.`sub_m`, 4d_live.`sub_s`, \
		4d_live.`date`, 4d_live.`time`, 4d_live.`v_count`, 4d_live.`h_count`, 4d_live.`object`, 4d_live.`explain`, 4d_live.`playback` FROM %s.4d_live %s"), strName, csWhere);
	pQuery = ESMUtil::CStringToChar(strQuery);
	//AMLOGDEVINFO((TCHAR*)(LPCTSTR)strQuery);

	if (mysql_query(&mysql, pQuery))
	{
		delete[] pQuery;

		data[U("rstCode")] = web::json::value::string(U("005"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("FAIL"));			//에러 메시지

		return data;
	}
	delete[] pQuery;

	if ((m_res = mysql_store_result(&mysql)) == NULL)
	{
		data[U("rstCode")] = web::json::value::string(U("010"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("EMPTY"));			//에러 메시지

		return data;
	}

	my_ulonglong nRow = 0;
	nRow = mysql_num_rows(m_res);
	if (nRow == 0)
	{
		data[U("rstCode")] = web::json::value::string(U("010"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("EMPTY"));			//에러 메시지

		return data;
	}

	data[U("rstCode")] = web::json::value::string(U("000"));			//결과 코드
	data[U("errMsg")] = web::json::value::string(U("OK"));				//에러 메시지

	UINT nFiled = 0;
	nFiled = mysql_num_fields(m_res);

	std::vector<web::json::value> objArr;

	int nCnt = 0;
	while ((row = mysql_fetch_row(m_res)) != NULL)
	{
		CString strMsg;


		CString strNum, strHit, strMode, strSubL, strSubM, strSubS, \
			strDate, strTime, strVCount, strHCount, strObject, strExplain, strPlayback;


		strNum = row[0];
		strHit = row[1];
		strMode = row[2];
		strSubL = row[3];
		strSubM = row[4];
		strSubS = row[5];
		strDate = row[6];
		strTime = row[7];
		strVCount = row[8];
		strHCount = row[9];
		strObject = row[10];
		strExplain = row[11];
		strPlayback = row[12];

		nNum = _ttoi(strNum);

		if (nMax < nNum)
		{
			nMax = nNum;
		}

		/*strMsg.Format(_T("[%d] %s  %s, %s\r\n%s, %s, %s\r\n%s, %s\r\n%s, %s\r\n%s, %s"), ++nCnt, strNum, strHit, strMode, strSubL, strSubM, strSubS, strDate, strTime, strVCount, strHCount, strObject, strExplain);
		AfxMessageBox(strMsg);*/

		web::json::value objData;


		objData[U("num")] = web::json::value::string(strNum.operator LPCWSTR());
		objData[U("hit")] = web::json::value::string(strHit.operator LPCWSTR());
		objData[U("mode")] = web::json::value::string(strMode.operator LPCWSTR());

		objData[U("sub_l")] = web::json::value::string(strSubL.operator LPCWSTR());
		objData[U("sub_m")] = web::json::value::string(strSubM.operator LPCWSTR());
		objData[U("sub_s")] = web::json::value::string(strSubS.operator LPCWSTR());

		objData[U("date")] = web::json::value::string(strDate.operator LPCWSTR());
		objData[U("time")] = web::json::value::string(strTime.operator LPCWSTR());

		objData[U("v_count")] = web::json::value::string(strVCount.operator LPCWSTR());
		objData[U("h_count")] = web::json::value::string(strHCount.operator LPCWSTR());

		objData[U("object")] = web::json::value::string(strObject.operator LPCWSTR());
		objData[U("explain")] = web::json::value::string(strExplain.operator LPCWSTR());

		objData[U("playback")] = web::json::value::string(strPlayback.operator LPCWSTR());

		objArr.push_back(objData);
		//objArr.push_back(objData);
	}

	mysql_free_result(m_res);

	if (objArr.size() > 0)
	{
		data[U("arrList")] = web::json::value::array(objArr);
	}

	mysql_close(&mysql);

	return data;
}

web::json::value CESMServer::GetHits(CString csData)
{
	setlocale(LC_ALL, "");

	web::json::value data;
	if (csData.IsEmpty())
	{
		data[U("rstCode")] = web::json::value::string(U("015"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("VALUE EMPTY"));
		return data;
	}

	int nSize = -1;
	nSize = data.size();

	CString strDatabase, strId, strPwd, strName;
	strDatabase = DB_ADDRESS;
	strId = DB_ID;
	strPwd = DB_PASS;
	strName = DB_NAME;

	char* pDbAddrIp = ESMUtil::CStringToChar(strDatabase);
	char* pDbID = ESMUtil::CStringToChar(strId);
	char* pDbPW = ESMUtil::CStringToChar(strPwd);
	char* pDbName = ESMUtil::CStringToChar(strName);

	mysql_init(&mysql);

	if (!mysql_real_connect(&mysql, pDbAddrIp, pDbID, pDbPW, pDbName, DB_PORT, 0, 0))
	{
		//char errMsg[MAX_PATH];
		const char * errM = mysql_error(&mysql);
		CString strMsg;
		strMsg = ESMUtil::CharToCString((char *)errM);
		AMLOGERROR((TCHAR*)(LPCTSTR)strMsg);
		return data;
	}
	mysql_query(&mysql, "set Names EucKR");

	CString strQuery;
	strQuery.Format(_T("use %s"), strName);
	char * pQuery = NULL;
	pQuery = ESMUtil::CStringToChar(strQuery);
	mysql_query(&mysql, pQuery);
	delete[] pQuery;
	pQuery = NULL;

	delete[] pDbAddrIp;
	delete[] pDbID;
	delete[] pDbPW;
	delete[] pDbName;

	MYSQL_ROW row;
	
	//CString strQuery;
	strQuery.Format(_T("UPDATE %s.`4d_live` SET hit = hit + 1 WHERE num = %s"), strName, csData);
	pQuery = ESMUtil::CStringToChar(strQuery);
	//AMLOGDEVINFO((TCHAR*)(LPCTSTR)strQuery);

	if (mysql_query(&mysql, pQuery))
	{
		delete[] pQuery;

		data[U("rstCode")] = web::json::value::string(U("005"));			//결과 코드
		data[U("errMsg")] = web::json::value::string(U("FAIL"));			//에러 메시지

		return data;
	}
	delete[] pQuery;


	
	data[U("rstCode")] = web::json::value::string(U("000"));			//결과 코드
	data[U("errMsg")] = web::json::value::string(U("OK"));				//에러 메시지
			
	mysql_close(&mysql);

	return data;
}