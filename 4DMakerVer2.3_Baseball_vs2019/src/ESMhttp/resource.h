//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ESMhttp.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_ESMHTTP_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       130
#define IDR_MENU                        130
#define IDC_EDIT_SERVER_ADDR            1000
#define IDC_EDIT_CLIENT_ADDR            1001
#define IDC_EDIT_SERVER_ADDR2           1002
#define IDC_EDIT_4DMAKER                1002
#define IDC_BTN_READY                   1003
#define IDC_BTN_START                   1004
#define IDC_BTN_STOP                    1005
#define IDC_BTN_FINISH                  1006
#define IDC_RADIO_START                 1007
#define IDC_RADIO_PAUSE                 1008
#define IDC_RADIO_RESUME                1009
#define IDC_RADIO_END                   1010
#define IDC_STATIC_PROC                 1011
#define ID_MENU_OPEN                    32771
#define ID_MENU_CLOSE                   32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
