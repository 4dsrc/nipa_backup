#pragma once
#include <windows.h>
#include <iostream>
#include <string>
#include <conio.h>
#include <strsafe.h>

#include "cpprest/http_listener.h"

#include "ESMUtil.h"

using namespace utility;
using namespace web::http;
using namespace web::http::experimental::listener;
using namespace Concurrency;

#define WM_ESM_SERVER							0x1010
#define WM_ESM_GET_INFO							0x1011
#define WM_ESM_GET_STATUS						0x1012
#define WM_ESM_GET_COMMAND						0x1013
#define WM_ESM_NOTIFY							0x1014
#define WM_ESM_NOTIFY_SYNC						0x1015
#define WM_ESM_NOTIFY_DB						0x1016

enum LISTColumn
{
	LIST_CAMID = 0,
	LIST_ADDR,
	LIST_USE,
	LIST_MAKE,
	LIST_NAME,
	LIST_ADJ_MAT_Y0,
	LIST_ADJ_MAT_Y1,
	LIST_ADJ_MAT_Y2,
	LIST_ADJ_MAT_Y3,
	LIST_ADJ_MAT_Y4,
	LIST_ADJ_MAT_Y5,
	LIST_ADJ_RESIZE_YW,
	LIST_ADJ_RESIZE_YH,
	LIST_ADJ_PT_YX,
	LIST_ADJ_PT_YY,
	LIST_ADJ_IMG_YW,
	LIST_ADJ_IMG_YH,
	LIST_ADJ_MAT_UV0,
	LIST_ADJ_MAT_UV1,
	LIST_ADJ_MAT_UV2,
	LIST_ADJ_MAT_UV3,
	LIST_ADJ_MAT_UV4,
	LIST_ADJ_MAT_UV5,
	LIST_ADJ_RESIZE_UVW,
	LIST_ADJ_RESIZE_UVH,
	LIST_ADJ_PT_UVX,
	LIST_ADJ_PT_UVY,
	LIST_ADJ_IMG_UVW,
	LIST_ADJ_IMG_UVH,
	LIST_COLUMN_COUNT
};

class CESMServer
{
public:
	CESMServer();
	~CESMServer();

	void StartServer();
	void StopServer();

	BOOL SetResponse(CString csState, CString csValue = _T(""));
	web::json::value GetResponse(CString csState, CString csValue = _T(""));
	web::json::value GetInfo(CString csCamId = _T(""));
	web::json::value GetStatus(CString csCamId = _T(""));
	web::json::value GetCommand();
	CString GetStateCode(CString csState);
	
private:
	CString m_cs4DMaker;
	CString m_csServerIp;
	CString m_csServerPort;
	BOOL m_bLog;
	CString m_csURL;

public:
	void Set4DMaker(CString csCaption);
	CString Get4DMaker();
	void SetHttpIp(CString csIp);
	CString GetHttpIp();
	void SetHttpPort(CString csPort);
	CString GetHttpPort();

	void SetURL(CString csUrl);
	CString GetURL();

	void SetEanbleLog(BOOL bLog);
	
protected:
	MYSQL mysql;
	BOOL ConnectMySQL();
	void DisconnectMySQL();
	
public:
	web::json::value GetDBResponse(CString csState, CString csValue = _T(""));
	web::json::value GetVodInfo();
	web::json::value GetVodInfo_ALL();

	web::json::value GetContentsAll();				//Contents ALL
	web::json::value GetCategory(CString csData);	//Category
	web::json::value GetSearch(CString csData);		//Search
	web::json::value GetHits(CString csData);		//Hits

};

