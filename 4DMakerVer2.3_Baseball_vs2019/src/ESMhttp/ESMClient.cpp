#include "stdafx.h"
#include "ESMClient.h"

web::uri_builder GetBuilder(std::wstring p_sQueryPath,
	std::vector<std::pair<std::wstring, std::wstring>>* p_pvQuery)
{
	web::uri_builder builder;
	if (!p_sQueryPath.empty())
	{
		builder.set_path(p_sQueryPath);
		if (!p_pvQuery->empty())
		{
			for (std::pair<std::wstring, std::wstring> pQuery : (*p_pvQuery))
			{
				builder.append_query(pQuery.first, pQuery.second);
			}
		}
	}

	return builder;
}
web::json::value GetJson(std::wstring p_sUrl,
	std::wstring p_sQueryPath = U(""),
	std::vector<std::pair<std::wstring, std::wstring>>* p_pvQuery = nullptr)
{
	web::json::value vJson;
	web::http::client::http_client client(p_sUrl);
	web::uri_builder builder = GetBuilder(p_sQueryPath, p_pvQuery);

	std::wstring wsBuilder = builder.to_string();
	//CString csLog = wsBuilder.c_str();
	AMLOGDEVINFO(_T("GetJson : %s"), wsBuilder.c_str());

	pplx::task<void> requestTask = client.request(web::http::methods::GET, builder.to_string())
		.then([&](web::http::http_response response) {

		return response.extract_json();
	})
		.then([&](pplx::task<web::json::value> previousTask) {
		try
		{
			vJson = previousTask.get();

		}
		catch (const web::http::http_exception& e)
		{
			AMLOGDEVINFO(_T("GetJson : Error http_exception : %s"), (CString)e.what());
		}
	});

	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		AMLOGDEVINFO(_T("GetJson : Error exception : %s"), (CString)e.what());
	}

	return vJson;
}

std::wstring GetString(std::wstring p_sUrl,
	std::wstring p_sQueryPath = U(""),
	std::vector<std::pair<std::wstring, std::wstring>>* p_pvQuery = nullptr)
{
	std::wstring vString;
	web::http::client::http_client client(p_sUrl);
	web::uri_builder builder = GetBuilder(p_sQueryPath, p_pvQuery);

	std::wstring wsBuilder = builder.to_string();
	//CString csLog = wsBuilder.c_str();
	AMLOGDEVINFO(_T("GetString : %s"), wsBuilder.c_str());

	pplx::task<void> requestTask = client.request(web::http::methods::GET, builder.to_string())
		.then([&](web::http::http_response response) {

		return response.extract_string();
	})
		.then([&](utility::string_t str) {
		try
		{
			vString = str;
		}
		catch (const web::http::http_exception& e)
		{
			AMLOGDEVINFO(_T("GetString : Error http_exception : %s"), (CString)e.what());
		}
	});

	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		AMLOGDEVINFO(_T("GetString : Error exception : %s"), (CString)e.what());
	}

	return vString;
}

web::json::value PostJson(std::wstring p_sUrl,
	std::wstring p_sQueryPath = U(""),
	std::vector<std::pair<std::wstring, std::wstring>>* p_pvQuery = nullptr)
{
	web::json::value vJson;
	web::http::client::http_client client(p_sUrl);
	web::uri_builder builder = GetBuilder(p_sQueryPath, p_pvQuery);

	std::wstring wsBuilder = builder.to_string();
	//CString csLog = wsBuilder.c_str();
	//AMLOGDEVINFO(_T("PostJson : %s"), wsBuilder.c_str());

	pplx::task<void> requestTask = client.request(web::http::methods::POST, builder.to_string())
		.then([&](web::http::http_response response) {

		return response.extract_json();
	})
		.then([&](pplx::task<web::json::value> previousTask) {
		try
		{
			vJson = previousTask.get();

		}
		catch (const web::http::http_exception& e)
		{
			CString csLog;
			csLog = (CString)e.what();
			csLog.Trim();
			AMLOGERROR(_T("PostJson : Error http_exception : %s"), csLog);
		}
	});

	try
	{
		//requestTask.wait();
		pplx::wait(2000);
	}
	catch (const std::exception &e)
	{
		CString csLog;
		csLog = (CString)e.what();
		csLog.Trim();
		AMLOGERROR(_T("PostJson : Error exception : %s"), csLog);
	}

	return vJson;
}

std::wstring PostString(std::wstring p_sUrl,
	std::wstring p_sQueryPath = U(""),
	std::vector<std::pair<std::wstring, std::wstring>>* p_pvQuery = nullptr)
{
	std::wstring vString;
	web::http::client::http_client client(p_sUrl);
	web::uri_builder builder = GetBuilder(p_sQueryPath, p_pvQuery);

	std::wstring wsBuilder = builder.to_string();
	//CString csLog = wsBuilder.c_str();
	AMLOGDEVINFO(_T("PostString : %s"), wsBuilder.c_str());

	pplx::task<void> requestTask = client.request(web::http::methods::POST, builder.to_string())
		.then([&](web::http::http_response response) {

		AMLOGDEVINFO(_T("PostString : %s"), response.extract_string());
		return response.extract_string();
	})
		.then([&](utility::string_t str) {
		try
		{
			vString = str;

		}
		catch (const web::http::http_exception& e)
		{
			AMLOGDEVINFO(_T("PostString : Error http_exception : %s"), (CString)e.what());
		}
	});

	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		AMLOGDEVINFO(_T("PostString : Error exception : %s"), (CString)e.what());
	}

	return vString;
}

CESMClient::CESMClient()
{
	m_csClientIp = _T("");
	m_csClientPort = _T("");
	m_csURL = _T("");
}


CESMClient::~CESMClient()
{
}

void CESMClient::GetTest()
{
	CString csUri;
	csUri.Format(_T("http://%s:%s/camera/info"), m_csClientIp, m_csClientPort);
	std::wstring wsUri;

	wsUri = csUri.operator LPCWSTR();
	web::json::value J = GetJson(wsUri);

	if (!J.is_null())
	{
		//CString csMsg;
		std::wstring rstCode, errMsg, camId, camState;
		
		if (J.has_field(U("rstCode")))
			rstCode = J.at(U("rstCode")).as_string();
		if (J.has_field(U("errMsg")))
			errMsg = J.at(U("errMsg")).as_string();
		
		if (J.has_field(U("camId")))
			camId = J.at(U("camId")).as_string();
		if (J.has_field(U("camState")))
			camState = J.at(U("camState")).as_string();

		//csMsg.Format(L"rstCode : %s, errMsg : %s, camNm : %s, camLoc : %s", rstCode.c_str(), errMsg.c_str(), camId.c_str(), camState.c_str());
		TRACE(_T("rstCode : %s, errMsg : %s, camNm : %s, camLoc : %s\r\n"), rstCode.c_str(), errMsg.c_str(), camId.c_str(), camState.c_str());
		//AfxMessageBox(csMsg);

		//for (auto iter = J.as_object().begin(); iter != J.as_object().end(); ++iter)
		//{
		//	// Make sure to get the value as const reference otherwise you will end up copying
		//	// the whole JSON value recursively which can be expensive if it is a nested object.
		//	/*const json::value &str = iter->first;
		//	const json::value &v = iter->second;*/

		//	utility::string_t str = iter->first;
		//	const json::value &v = iter->second;
		//	CString csValue;
		//	if (J.at(str).is_array())
		//	{
		//		for (int i = 0; i < J.at(str).as_array().size(); i++)
		//		{
		//			std::wstring camId, camNm, camLoc;
		//			camId = J.at(str).at(i).at(U("camId")).as_string();
		//			camNm = J.at(str).at(i).at(U("camNm")).as_string();
		//			camLoc = J.at(str).at(i).at(U("camLoc")).as_string();
		//			csValue.Format(_T("[%d] camId : %s,  camNm : %s, camLoc : %s"), i+1, camId.c_str(), camNm.c_str(), camLoc.c_str());
		//			AfxMessageBox(csValue);
		//		}
		//		
		//	}
		//	else
		//	{
		//		csValue.Format(_T("%s : %s"), str.c_str(), v.as_string().c_str());
		//		AfxMessageBox(csValue);
		//	}
		//	// Perform actions here to process each string and value in the JSON object...
		//	//std::wcout << L"String: " << str.as_string() << L", Value: " << v.to_string() << endl;
		//}
	}

	csUri.Format(_T("http://%s:%s/camera/state"), m_csClientIp, m_csClientPort);
	wsUri = csUri.operator LPCWSTR();
	J = GetJson(wsUri);

}

void CESMClient::SendCamState(int nState/* = 1*/)
{
	CString csUri;
	//csUri.Format(_T("http://%s:%s/its-api/notify/camState/%d"), GetHttpIp(), GetHttpPort(), nState);
	csUri.Format(_T("http://%s:%s/%s/%d"), GetHttpIp(), GetHttpPort(), GetURL(),nState);

	AMLOGDEVINFO((TCHAR*)(LPCTSTR)csUri);

	std::wstring wsUri;
	wsUri = csUri.operator LPCWSTR();

	/*
	std::vector<std::pair<std::wstring, std::wstring>> _vData;
	_vData.push_back(std::make_pair(U("1"), U("Test")));
	web::json::value J = PostJson(wsUri, U("Code"), &_vData);
	*/

	web::json::value J = PostJson(wsUri);

	if (!J.is_null())
	{
		std::wstring rstCode, errMsg;

		if (J.has_field(U("rstCode")))
			rstCode = J.at(U("rstCode")).as_string();
		if (J.has_field(U("errMsg")))
			errMsg = J.at(U("errMsg")).as_string();

		AMLOGDEVINFO(_T("rstCode : %s, errMsg : %s\r\n"), rstCode.c_str(), errMsg.c_str());
		TRACE(_T("rstCode : %s, errMsg : %s\r\n"), rstCode.c_str(), errMsg.c_str());
	}
}

void CESMClient::SetHttpIp(CString csIp)
{
	m_csClientIp = csIp;
}
CString CESMClient::GetHttpIp()
{
	return m_csClientIp;
}
void CESMClient::SetHttpPort(CString csPort)
{
	m_csClientPort = csPort;
}
CString CESMClient::GetHttpPort()
{
	return m_csClientPort;
}
void CESMClient::SetURL(CString csUrl)
{
	m_csURL = csUrl;
}
CString CESMClient::GetURL()
{
	return m_csURL;
}

void CESMClient::SendNotifyUpdate(int nNum)
{
	CString csUri;
	csUri.Format(_T("http://%s:%s/%s"), GetHttpIp(), GetHttpPort(), GetURL());

	//AMLOGDEVINFO((TCHAR*)(LPCTSTR)csUri);

	std::wstring wsUri;
	wsUri = csUri.operator LPCWSTR();

	CString csNum = _T("");
	csNum.Format(_T("%d"), nNum);

	std::wstring wsNum;
	wsNum = csNum.operator LPCWSTR();

	std::vector<std::pair<std::wstring, std::wstring>> _vData;
	_vData.push_back(std::make_pair(U("num"), wsNum));
	

	//web::json::value J = PostJson(wsUri, U("fcm"), &_vData);
	web::json::value J = GetJson(wsUri, U("fcm"), &_vData);
	if (!J.is_null())
	{
		std::wstring rstCode, errMsg;

		if (J.has_field(U("rstCode")))
		{
			if (J.at(U("rstCode")).is_integer())
			{
				CString rst;
				rst.Format(_T("%d"), J.at(U("rstCode")).as_integer());
				rstCode = rst.operator LPCWSTR();
			}
			else
			{
				rstCode = J.at(U("rstCode")).as_string();
			}
			
		}
				
		if (J.has_field(U("errMsg")))
			errMsg = J.at(U("errMsg")).as_string();

		AMLOGDEVINFO(_T("rstCode : %s, errMsg : %s\r\n"), rstCode.c_str(), errMsg.c_str());
		TRACE(_T("rstCode : %s, errMsg : %s\r\n"), rstCode.c_str(), errMsg.c_str());
	}
}

void CESMClient::OnTestUPlus()
{
	CString csUri;
	//csUri.Format(_T("http://%s:%s/%s"), _T("211.170.95.150"), _T("80"), _T("service/server/ss"));
	csUri.Format(_T("http://211.170.95.150/service/server/ss"));

	//AMLOGDEVINFO((TCHAR*)(LPCTSTR)csUri);

	std::wstring wsUri;
	wsUri = csUri.operator LPCWSTR();

	web::json::value J = GetJson(wsUri);
	if (!J.is_null())
	{
		/*if (J.has_field(U("serverList")))
		{
			if (J.at(U("serverList")).is_array())
			{
				int nCount = J.at(U("serverList")).as_array().size();
				for (int i = 0; i < nCount; i++)
				{
					std::wstring serverId, serverIp;
					serverId = J.at(U("serverList")).at(i).at(U("name")).as_string();
					serverIp = J.at(U("serverList")).at(i).at(U("ip")).as_string();
					CString csValue;
					csValue.Format(_T("[%d/%d] name : %s,  ip : %s"), i + 1, nCount, serverId.c_str(), serverIp.c_str());
					AMLOGDEVINFO((TCHAR*)(LPCTSTR)csValue);
					AfxMessageBox(csValue);
				}
			}
		}*/
				
				
		for (auto iter = J.as_object().begin(); iter != J.as_object().end(); ++iter)
		{
			// Make sure to get the value as const reference otherwise you will end up copying
			// the whole JSON value recursively which can be expensive if it is a nested object.
			/*const json::value &str = iter->first;
			const json::value &v = iter->second;*/


			utility::string_t str = iter->first;
			const json::value &v = iter->second;
			
			if (J.has_field(str))
			{
				if (J.at(str).is_array())
				{
					const json::value &arry = J.at(str);

					int nCount = arry.as_array().size();
					for (int i = 0; i < nCount; i++)
					{
						const json::value &data = arry.at(i);
						
						/*for (auto itData = data.as_object().begin(); itData != data.as_object().end(); itData++)
						{
							utility::string_t key = itData->first;
							const json::value &value = itData->second;

							std::wstring strText;
							strText = value.as_string();
						}*/
						
						std::wstring serverId, serverIp;
						serverId = data.at(U("name")).as_string();
						serverIp = data.at(U("ip")).as_string();

						CString csValue;
						csValue.Format(_T("[%d/%d] name : %s,  ip : %s"), i + 1, nCount, serverId.c_str(), serverIp.c_str());
						AMLOGDEVINFO((TCHAR*)(LPCTSTR)csValue);
						AfxMessageBox(csValue);
					}

					/*int nCount = J.at(str).as_array().size();
					for (int i = 0; i < nCount; i++)
					{
						std::wstring serverId, serverIp;
						serverId = J.at(str).at(i).at(U("name")).as_string();
						serverIp = J.at(str).at(i).at(U("ip")).as_string();

						CString csValue;
						csValue.Format(_T("[%d] name : %s,  ip : %s"), i + 1, serverId.c_str(), serverIp.c_str());
						AMLOGDEVINFO((TCHAR*)(LPCTSTR)csValue);
						AfxMessageBox(csValue);
					}*/

				}
				else
				{
					CString csValue;
					csValue.Format(_T("%s : %s"), str.c_str(), v.as_string().c_str());
					AfxMessageBox(csValue);
				}
			}
			
			// Perform actions here to process each string and value in the JSON object...
			//std::wcout << L"String: " << str.as_string() << L", Value: " << v.to_string() << endl;
		}
	}
}

void CESMClient::OnGetList()
{
	m_arrClient.RemoveAll();

	CString csUri;
	csUri = GetURL();

	std::wstring wsUri;
	wsUri = csUri.operator LPCWSTR();

	web::json::value J = GetJson(wsUri);
	if (!J.is_null())
	{
		//for (auto iter = J.as_object().begin(); iter != J.as_object().end(); ++iter)
		//{
		//	utility::string_t str = iter->first;
		//	const json::value &v = iter->second;

		//	if (J.has_field(str))
		//	{
		//		if (J.at(str).is_array())
		//		{
		//			const json::value &arry = J.at(str);

		//			int nCount = arry.as_array().size();
		//			for (int i = 0; i < nCount; i++)
		//			{
		//				const json::value &data = arry.at(i);

		//				std::wstring serverId, serverIp;
		//				serverId = data.at(U("name")).as_string();
		//				serverIp = data.at(U("ip")).as_string();

		//				CString csValue;
		//				csValue.Format(_T("[%d/%d] name : %s,  ip : %s"), i + 1, nCount, serverId.c_str(), serverIp.c_str());
		//				AMLOGDEVINFO((TCHAR*)(LPCTSTR)csValue);
		//				//AfxMessageBox(csValue);
		//				if (!serverIp.empty())
		//				{
		//					m_arrClient.Add(serverIp.c_str());
		//				}
		//			}
		//		}
		//		else
		//		{
		//			CString csValue;
		//			csValue.Format(_T("%s : %s"), str.c_str(), v.as_string().c_str());
		//			AMLOGDEVINFO((TCHAR*)(LPCTSTR)csValue);
		//		}
		//	}
		//}

		if (J.has_field(U("serverList")))
		{
			if (J.at(U("serverList")).is_array())
			{
				int nCount = J.at(U("serverList")).as_array().size();
				for (int i = 0; i < nCount; i++)
				{
					std::wstring serverId, serverIp;
					serverId = J.at(U("serverList")).at(i).at(U("name")).as_string();
					serverIp = J.at(U("serverList")).at(i).at(U("ip")).as_string();
					CString csValue;
					csValue.Format(_T("[%d/%d] name : %s,  ip : %s"), i + 1, nCount, serverId.c_str(), serverIp.c_str());
					AMLOGDEVINFO((TCHAR*)(LPCTSTR)csValue);
					//AfxMessageBox(csValue);
					if (!serverIp.empty())
					{
						m_arrClient.Add(serverIp.c_str());
					}
				}
			}
		}
	}
}

void CESMClient::OnSendCommand(int nParam)
{
	SetCommand(nParam);

	HANDLE hCmd = NULL;
	hCmd = (HANDLE)_beginthreadex(NULL, 0, SendCommandThread, this, 0, NULL);
	CloseHandle(hCmd);
}

void CESMClient::OnGetList(CString csUrl)
{
	std::wstring wsUri;
	wsUri = csUrl.operator LPCWSTR();

	web::json::value J = GetJson(wsUri);
	if (!J.is_null())
	{
		if (J.has_field(U("serverList")))
		{
			if (J.at(U("serverList")).is_array())
			{
				int nCount = J.at(U("serverList")).as_array().size();
				for (int i = 0; i < nCount; i++)
				{
					std::wstring serverId, serverIp;
					serverId = J.at(U("serverList")).at(i).at(U("name")).as_string();
					serverIp = J.at(U("serverList")).at(i).at(U("ip")).as_string();
					CString csValue;
					csValue.Format(_T("[%d/%d] name : %s,  ip : %s"), i + 1, nCount, serverId.c_str(), serverIp.c_str());
					AMLOGDEVINFO((TCHAR*)(LPCTSTR)csValue);
					//AfxMessageBox(csValue);
					if (!serverIp.empty())
					{
						m_arrClient.Add(serverIp.c_str());
					}
				}
			}
		}
	}
}

void CESMClient::OnCommand()
{
	int nCommand = 0;
	CString csParam;

	nCommand = GetCommand();

	switch (nCommand)
	{
	case 1:
		csParam = _T("start");
		break;
	case 9:
		csParam = _T("end");
		break;
	case 11:
		csParam = _T("resume");
		break;
	case 19:
		csParam = _T("pause");
		break;
	default:
		break;
	}

	//AMLOGDEVINFO((TCHAR*)(LPCTSTR)csParam);

	if (csParam.IsEmpty() != TRUE)
	{
		for (int i = 0; i < m_arrClient.GetCount(); i++)
		{
			CString csUri;
			csUri.Format(_T("http://%s:%d/4dapp/movie/status/%s"), m_arrClient.GetAt(i), 7070, csParam);
			//AMLOGDEVINFO((TCHAR*)(LPCTSTR)csUri);
			
			TRHEADEvent *pData = NULL;
			pData = new TRHEADEvent();
			pData->pParent = (LPARAM)this;
			TCHAR * cPath = new TCHAR[csUri.GetLength() + 1];
			_tcscpy(cPath, csUri);
			pData->pParam = (LPARAM)cPath;

			HANDLE hCmd = NULL;
			hCmd = (HANDLE)_beginthreadex(NULL, 0, RunCommandThread, pData, 0, NULL);
			CloseHandle(hCmd);

			/*std::wstring wsUri;
			wsUri = csUri.operator LPCWSTR();

			web::json::value J = PostJson(wsUri);
			if (!J.is_null())
			{
				if (J.has_field(U("result")))
				{
					std::wstring result;
					result = J.at(U("result")).as_string();

					AMLOGDEVINFO(_T("%s / %s"), wsUri.c_str(), result.c_str());
				}
			}
			else
			{
				AMLOGDEVINFO((TCHAR*)(LPCTSTR)csUri);
			}*/
		}
	}
}

unsigned WINAPI CESMClient::SendCommandThread(LPVOID param)
{

	CESMClient * pParent = (CESMClient *)param;

	pParent->OnCommand();

	return 0;
}

void CESMClient::OnSendSync(int nParam)
{
	SetCameraIdx(nParam);

	HANDLE hCmd = NULL;
	hCmd = (HANDLE)_beginthreadex(NULL, 0, SendSyncThread, this, 0, NULL);
	CloseHandle(hCmd);
}

void CESMClient::OnSync()
{
	int nSync = -1;
	//CString csParam;

	nSync = GetCameraIdx();

	//AMLOGDEVINFO((TCHAR*)(LPCTSTR)csParam);

	if (nSync != -1)
	{
		/*std::vector<std::pair<std::wstring, std::wstring>> _vData;
		_vData.push_back(std::make_pair(U("adminId"), U("shpark")));
		_vData.push_back(std::make_pair(U("adminPw"), U("shpark1234")));*/

		for (int i = 0; i < m_arrClient.GetCount(); i++)
		{
			CString csUri;
			csUri.Format(_T("http://%s:%d/4dapp/movie/inactive/%d"), m_arrClient.GetAt(i), 7070, nSync);
			//AMLOGDEVINFO((TCHAR*)(LPCTSTR)csUri);
			std::wstring wsUri;
			wsUri = csUri.operator LPCWSTR();

			int nRetry = 3;
			while (nRetry)
			{
				web::json::value J = PostJson(wsUri);
				if (!J.is_null())
				{
					if (J.has_field(U("result")))
					{
						std::wstring result;
						result = J.at(U("result")).as_string();

						AMLOGDEVINFO(_T("%s / %s"), wsUri.c_str(), result.c_str());

						int nFind = 0;
						nFind = result.find(U("200"));
						if (nFind >= 0)
							break;
					}
				}
				else
				{
					AMLOGDEVINFO((TCHAR*)(LPCTSTR)csUri);
				}
				--nRetry;
			}
		}
	}
}

unsigned WINAPI CESMClient::SendSyncThread(LPVOID param)
{

	CESMClient * pParent = (CESMClient *)param;

	pParent->OnSync();

	return 0;
}

unsigned WINAPI CESMClient::RunCommandThread(LPVOID param)
{
	TRHEADEvent *pData = NULL;
	pData = (TRHEADEvent*)param;
	TCHAR *cUrl = (TCHAR*)pData->pParam;
	CString csUri;
	csUri.Format(_T("%s"), cUrl);

	std::wstring wsUri;
	wsUri = csUri.operator LPCWSTR();

	int nRetry = 3;
	while (nRetry)
	{
		web::json::value J = PostJson(wsUri);
		if (!J.is_null())
		{
			if (J.has_field(U("result")))
			{
				std::wstring result;
				result = J.at(U("result")).as_string();
				
				AMLOGDEVINFO(_T("%s / %s"), wsUri.c_str(), result.c_str());

				int nFind = 0;
				nFind = result.find(U("200"));
				if (nFind >= 0)
					break;
			}
		}
		else
		{
			AMLOGERROR((TCHAR*)(LPCTSTR)csUri);
		}

		--nRetry;
	}
	
	
	if (pData)
	{
		delete pData;
		pData = NULL;
	}

	return 0;
}


void CESMClient::OnSendTest()
{
	CString csUri;
	csUri.Format(_T("http://192.168.0.26:7070/4dapp/movie/status/start"));
	{
		TRHEADEvent *pData = NULL;
		pData = new TRHEADEvent();
		pData->pParent = (LPARAM)this;
		TCHAR * cPath = new TCHAR[csUri.GetLength() + 1];
		_tcscpy(cPath, csUri);
		pData->pParam = (LPARAM)cPath;

		HANDLE hCmd = NULL;
		hCmd = (HANDLE)_beginthreadex(NULL, 0, RunCommandThread, pData, 0, NULL);
		CloseHandle(hCmd);
	}
	//csUri.Format(_T("http://192.168.0.26:7070/4dapp/movie/status/pause"));
	//{
	//	TRHEADEvent *pData = NULL;
	//	pData = new TRHEADEvent();
	//	pData->pParent = (LPARAM)this;
	//	TCHAR * cPath = new TCHAR[csUri.GetLength() + 1];
	//	_tcscpy(cPath, csUri);
	//	pData->pParam = (LPARAM)cPath;

	//	HANDLE hCmd = NULL;
	//	hCmd = (HANDLE)_beginthreadex(NULL, 0, RunCommandThread, pData, 0, NULL);
	//	CloseHandle(hCmd);
	//}

	//csUri.Format(_T("http://192.168.0.26:7070/4dapp/movie/status/resume"));
	//{
	//	TRHEADEvent *pData = NULL;
	//	pData = new TRHEADEvent();
	//	pData->pParent = (LPARAM)this;
	//	TCHAR * cPath = new TCHAR[csUri.GetLength() + 1];
	//	_tcscpy(cPath, csUri);
	//	pData->pParam = (LPARAM)cPath;

	//	HANDLE hCmd = NULL;
	//	hCmd = (HANDLE)_beginthreadex(NULL, 0, RunCommandThread, pData, 0, NULL);
	//	CloseHandle(hCmd);
	//}

	//csUri.Format(_T("http://192.168.0.26:7070/4dapp/movie/status/end"));
	//{
	//	TRHEADEvent *pData = NULL;
	//	pData = new TRHEADEvent();
	//	pData->pParent = (LPARAM)this;
	//	TCHAR * cPath = new TCHAR[csUri.GetLength() + 1];
	//	_tcscpy(cPath, csUri);
	//	pData->pParam = (LPARAM)cPath;

	//	HANDLE hCmd = NULL;
	//	hCmd = (HANDLE)_beginthreadex(NULL, 0, RunCommandThread, pData, 0, NULL);
	//	CloseHandle(hCmd);
	//}
	
}

void CESMClient::OnAddTestUrl(CString strUrl)
{
	m_arrClient.Add(strUrl);
}