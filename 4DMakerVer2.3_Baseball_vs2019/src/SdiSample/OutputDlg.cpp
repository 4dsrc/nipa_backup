// OutputDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "OutputDlg.h"
#include "afxdialogex.h"


// COutputDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(COutputDlg, CDialog)

COutputDlg::COutputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COutputDlg::IDD, pParent)
{

}

COutputDlg::~COutputDlg()
{
}

void COutputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(COutputDlg, CDialog)
END_MESSAGE_MAP()


// COutputDlg 메시지 처리기입니다.
