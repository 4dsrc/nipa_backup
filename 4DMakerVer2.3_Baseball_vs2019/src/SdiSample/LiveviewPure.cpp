/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */



#include "stdafx.h"
#ifndef _NonOpenCV
#include "Liveview.h"
#else
#include "LiveviewPure.h"
#endif

IMPLEMENT_DYNCREATE(CLiveview, CView)

const int NX1_LIVE_WIDTH  = 720;
const int NX1_LIVE_HEIGHT = 480;

const int NX1_EVF_LIVE_WIDTH  = 1024;
const int NX1_EVF_LIVE_HEIGHT = 684;

CLiveview::CLiveview()
{	
	m_pFrameData = NULL;
	m_bEVF = FALSE;
	m_bPreLive = FALSE;
}
CLiveview::~CLiveview(){}

BEGIN_MESSAGE_MAP(CLiveview, CView)	
	ON_WM_PAINT()	
END_MESSAGE_MAP()

// CLiveview diagnostics
#ifdef _DEBUG
void CLiveview::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CLiveview::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG

void CLiveview::OnPaint()
{	
	DrawLiveview();
	CView::OnPaint();
}


//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-19
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveview::DrawLiveview()
{;
	BYTE* pLiveConvert;
	//-- GetRect
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	
	//-- Get Liveview	
	if(!m_pFrameData || !m_bLiveview)
	{		
		//-- Black Background	
		dc.FillRect(rect, &CBrush(RGB(0,0,0)));		
		return;
	}

	BITMAPINFOHEADER bih;
	int nChenel = 3;
	int nDepth = 8;
	memset(&bih, 0, sizeof(BITMAPINFOHEADER));    
	bih.biSize      = sizeof(BITMAPINFOHEADER);
	bih.biWidth      = m_pFrameData->nWidth;  
	bih.biHeight   = m_pFrameData->nHeight; 
	bih.biWidth      = m_pFrameData->nWidth;  
	bih.biHeight   = m_pFrameData->nHeight; 
	bih.biPlanes   = 1;
	bih.biBitCount  = nChenel * nDepth;
	bih.biSizeImage  = m_pFrameData->nWidth*m_pFrameData->nHeight * nChenel;
	bih.biCompression = BI_RGB;
	dc.SetStretchBltMode(COLORONCOLOR);

	::StretchDIBits(dc,	// hDC
		rect.TopLeft().x,					// XDest
		rect.TopLeft().y,					// YDest
		rect.Width(),					// nDestWidth
		rect.Height(),					// nDestHeight
		0,					// XSrc
		m_pFrameData->nHeight,					// YSrc
		m_pFrameData->nWidth,			// nSrcWidth
		-m_pFrameData->nHeight,			// nSrcHeight
		(BYTE*)m_pFrameData->nFrameData,			// lpBits
		(BITMAPINFO*)&bih,				// lpBitsInfo
		DIB_RGB_COLORS,		// wUsage
		SRCCOPY);			// dwROP

}

//-- 2013-03-08 hongsu.jung
void CLiveview::DrawBlank()
{
	SetLiveview(FALSE);
	SetBuffer(NULL);
	DrawLiveview();
} 