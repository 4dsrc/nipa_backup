/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */


#include "stdafx.h"
#include "SdiSample.h"
#include "DevInfoManager.h"
#include "PropertyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CDevInfoManager

//IMPLEMENT_DYNAMIC(CDevInfoManager, CWnd)

CDevInfoManager::CDevInfoManager()
{

}

CDevInfoManager::~CDevInfoManager()
{
	RemoveAll();
}

void CDevInfoManager::RemoveAll()
{
	m_arPropInfo.RemoveAll();
}

void CDevInfoManager::SetDevInfo(IDeviceInfo* devInfo)
{
	//-- Check Exist Property
	BOOL bExist = FALSE;
	int nCnt = m_arPropInfo.GetCount();

	for(int i = 0; i < nCnt; i++)
	{
		//-- Get Exist Device Info
		IDeviceInfo *pExist = (IDeviceInfo*)m_arPropInfo.GetAt(i);

		int nDevCode = devInfo->GetPropCode();
		int nExistCode = pExist->GetPropCode();

		if(nDevCode == nExistCode)
		{
			m_arPropInfo.SetAt(i, (CObject *)devInfo);
			bExist = TRUE;
			break;
		}
	}

	if(!bExist)
		m_arPropInfo.Add((CObject*)devInfo);
}

IDeviceInfo* CDevInfoManager::GetDevInfo(int nPropCode)
{
	IDeviceInfo* pPropInfo = NULL;
	for(int i=0;i<m_arPropInfo.GetCount();i++){
		pPropInfo = ((IDeviceInfo*)m_arPropInfo.GetAt(i));
		if( pPropInfo->GetPropCode() == nPropCode) return pPropInfo;
	}
	
	return NULL;
}

//-- Get CurrentValue by PTP Code, PropValueIndex
int CDevInfoManager::GetCurrentValueEnum(int nPTPCode, int nPropValueIndex)
{
	for(int i = 0; m_arPropInfo.GetCount(); i++)
	{
		//-- Get Exist Device Info
		IDeviceInfo *pExist = (IDeviceInfo*)m_arPropInfo.GetAt(i);
		if(nPTPCode == pExist->GetPropCode())
		{
			return pExist->GetPropValueListEnum()->GetAt(nPropValueIndex);			
		}
	}
	return -1;
}

//-- Get CurrentValueIndex by PTP Code, PropValue 
int CDevInfoManager::GetCurrentValueIndex(int propCode)
{
	int nCnt = m_arPropInfo.GetCount();
	for(int i = 0; i < nCnt; i++)
	{
		//-- Get Exist Device Info
		IDeviceInfo *pExist = (IDeviceInfo*)m_arPropInfo.GetAt(i);

		if(propCode == pExist->GetPropCode())
		{
			switch(pExist->GetType())
			{
			case SDI_TYPE_INT_8:
			case SDI_TYPE_UINT_8:
			case SDI_TYPE_UINT_16:
			case SDI_TYPE_UINT_32:
				for(int j=0;j<pExist->GetPropValueCntEnum();j++)
				{
					int enumValue = pExist->GetPropValueListEnum()->GetAt(j);
					if(pExist->GetCurrentEnum() == enumValue) 
						return j;
				}
				break;
			case SDI_TYPE_STRING:
				for(int j=0;j<pExist->GetPropValueCntStr();j++)
				{
					CString strValue = pExist->GetPropValueListStr()->GetAt(j);
					if(pExist->GetCurrentStr() == strValue) 
						return j;
				}
				break;
			default:
				break;
			}			
		}
	}

	return 0;
}

