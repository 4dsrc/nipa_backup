#pragma once


// CCustomListBox

class CCustomListBox : public CListBox
{
	DECLARE_DYNAMIC(CCustomListBox)

public:
	CCustomListBox();
	virtual ~CCustomListBox();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};


