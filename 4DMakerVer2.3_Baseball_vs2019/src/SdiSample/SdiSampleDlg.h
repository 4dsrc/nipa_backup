/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */


#pragma once
// Header File Import

#include "PropertyDlg.h"
#include "LiveviewDlg.h"
#include "OutputDlg.h"
#include "SdiSdk.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "../4DMakerWatcher/CommThread.h"
#include "CustomListBox.h"

#define WM_SDI_SAMPLE									WM_USER+0x8000
#define WM_SAMPLE_OP_CAPTURE							WM_USER+0x8100
#define WM_SAMPLE_OP_REC								WM_USER+0x8101
#define WM_SAMPLE_OP_REC_STOP							WM_USER+0x8102
#define WM_SAMPLE_OP_GET_PROPERTY_DESC					WM_USER+0x8110
#define WM_SAMPLE_OP_SET_PROPERTY_VALUE					WM_USER+0x8111
#define WM_SAMPLE_OP_GET_CUSTOM_PROPERTY_DESC			WM_USER+0x8112
#define WM_SAMPLE_OP_SET_CUSTOM_PROPERTY_VALUE			WM_USER+0x8113
#define WM_SAMPLE_OP_SET_FOCUSTYPE						WM_USER+0x8114
#define WM_SAMPLE_OP_SET_FOCUSVALUE 					WM_USER+0x8115
#define WM_SAMPLE_OP_GET_FOCUSVALUE 					WM_USER+0x8116
#define WM_SAMPLE_OP_CHANGE_CAMERA 						WM_USER+0x8117
#define WM_SAMPLE_OP_GET_RECORD_STATUS					WM_USER+0x8118
#define WM_SAMPLE_OP_FW_UPDATE							WM_USER+0x8119
#define WM_SAMPLE_OP_SET_ENLARGE						WM_USER+0x811a
#define WM_SAMPLE_OP_SET_RECORD_PAUSE					WM_USER+0x811b
#define WM_SAMPLE_OP_SET_RECORD_RESUME					WM_USER+0x8120
#define WM_SAMPLE_OP_SET_LIVEVIEW						WM_USER+0x8121
#define WM_SAMPLE_OP_CONNECT_WIFI						WM_USER+0x8122
#define WM_SAMPLE_OP_CONNECT_WIFI						WM_USER+0x8123
#define WM_SAMPLE_OP_DEVICE_REBOOT						WM_USER+0x8124
#define WM_SAMPLE_OP_DEVICE_SHUT_DOWN					WM_USER+0x8125
#define WM_SAMPLE_OP_FW_DOWNLOAD_N_UPDATE				WM_USER+0x8126
#define WM_SAMPLE_OP_GET_UUID							WM_USER+0x8127
#define WM_SAMPLE_OP_SET_UUID							WM_USER+0x8128
#define WM_SAMPLE_OP_BANK_SAVE							WM_USER+0x8129
#define WM_SAMPLE_OP_EXPORT_LOG							WM_USER+0x812a
#define WM_SAMPLE_OP_GET_TICK							WM_USER+0x812b
#define WM_SAMPLE_OP_WRITE_LOG							WM_USER+0x812c
#define WM_SAMPLE_OP_WRITE_LOG_VERTICAL_FRAME			WM_USER+0x812d
#define WM_SAMPLE_OP_WRITE_LOG_VERTICAL_END				WM_USER+0x812e
#define WM_SAMPLE_OP_HALF_SHUTTER						WM_USER+0x812f
#define WM_SAMPLE_OP_GET_FOCUSVALUE_ALL					WM_USER+0x8130
#define WM_SAMPLE_OP_FOCUS_LOCATE_SAVE					WM_USER+0x8131
#define WM_SAMPLE_OP_SET_FOCUS_FRAME					WM_USER+0x8132
#define WM_SAMPLE_OP_GET_FOCUS_FRAME					WM_USER+0x8133
#define WM_SAMPLE_OP_SET_POE							WM_USER+0x8134
#define WM_SAMPLE_OP_SET_CONNECT						WM_USER+0x8135
#define WM_SAMPLE_OP_SENSOR_CLEANING					WM_USER+0x8136
#define WM_SAMPLE_OP_BANK_SAVE_ALL						WM_USER+0x8137




#include "StopWatch.h"
#include <vector>

using namespace std;

enum USBCONTROLERTYPE
{	USB_CONTROLER_KOREA,
	USB_CONTROLER_CHINA,
	USB_CONTROLER_V30
};

// CSdiSampleDlg dialog
class CSdiSampleDlg : public CDialogEx
{
// Construction

public:
	CSdiSampleDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CSdiSampleDlg();
	
public:
	int m_nPCTime;
	int m_nDSCTime;
	CStopWatch m_Clock;
	CLiveviewDlg* m_pDlgLiveview;
	CPropertyDlg* m_pDlgProperty;
	COutputDlg* m_pDlgOutput;
	int m_nModel;

 	CCommThread m_pSerial;
 	int m_nControlerType;
	BOOL m_bPowerFlag;
	BOOL m_bConnectFlag;

// Dialog Data
	enum { IDD = IDD_SDISAMPLE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	CBrush m_brush; 
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();	
	afx_msg void OnClose();	
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()	

public:
	CStringArray m_arrLog;
	int m_nAvgTime;
	int m_nSyncCount;
	CString m_strDate;

	CString m_strModel;
	CSdiSdk* m_pSdiSDK;
	vector<CSdiSdk*> m_pArrSdiSDK;
	int m_nFocus;
	BOOL m_bFileTrans;
	int m_nMovieCount;

	LRESULT OnChangeConnect(WPARAM wParam, LPARAM lParam);
	LRESULT OnDialogMsg(WPARAM wParam, LPARAM lParam);
	LRESULT OnSDIMsg(WPARAM wParam, LPARAM lParam);
	static unsigned WINAPI RecordThreadGH5(LPVOID param);
	static unsigned WINAPI RecordThread(LPVOID param);
	static unsigned WINAPI CaptureThread(LPVOID param);
	
public:	
	void ConnectCam();
	void DisconnectCam();
	SdiResult CheckConnect(void);
	void CloseSDK();				//-- 2013-12-01 hongsu.jung
	void OnCapture(void);	
	void OnGetUUID(void);	
	void OnSetUUID(void);	
	void OnReconnect();
	void OnGH5Initialize(void);
	void OnGetTick(void);	
	void OnSetTick(int nTime);	
	void OnRecord(void);	
	void OnRecordStop(void);	
	void OnCaptureDone(int nFileNum, int nClose);	
	//void OnMovieDone(void);	
	void OnMovieDone(int nfilenum, int nClose);	 // nClose 0 = contine, 1 = end
	void OnGetCustomProperty(int nPTPCode, int nAllFlag = 1);
	void OnSetCustomProperty(int nPTPCode, int nValue);
	void OnSetProperty(int nPTPCode, int index);
    void OnSetFocus(int nFocusType);
	void OnSetFocusValue(int nFocusValue);	
	void OnGetFocusValue();
	void OnGetFocusValueAll();
	void OnGetRecordStatus();
	void OnSetEnLarge(int nLarge);
	void OnSetRecordPause(int nPause);
	void OnSetRecordResume(int nSkip);
	void OnFileTransfer(int nfilenum, int nmdat, int nOffset, void* pDest);
	void GetPropertyAll();
	void GetPTPProperty(int nPTPCode);
	void GetLiveviewInfo();
	void SetDevPropertyStr(IDeviceInfo* pDevInfo, int nPropIndex);
	void SetDevPropertyEnum(IDeviceInfo* pDevInfo, int nPropIndex);
	void OnUpdateCombo(int nPTPCode);
	void SetLiveview(BOOL bLiveview);
	void OnChangeCamera();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	void OnHalfShutter(int nRelease);
	void OnSetFocusFrame(int nX, int nY, int nMagnification);
	void OnGetFocusFrame();
	void OnFWUpdate();
	void OnReboot();
	void OnSensorCleaning();
	void OnBankSave();
	void OnBankSaveAll();
	void OnExportLog();
	void OnFocusLocateSave();
	void OnLiveView(int nCommand);
	void SetEVFLive(BOOL bEVF);
	void CloseWindow();
	void SetDevPropertyValue(IDeviceInfo* pDevInfo, int nPropIndex);
	void OnUpdateValue(int nPTPCode);
	void OnConnectWifi();
	void OnDeviceReboot();
	void OnDeviceShutDown();
	void OnFW_Download_N_Update();
	void OnSetFrameRate();
	int ConvertTime(int nTime);
	void SaveLogFile(CString strPath);
	char* CStringToChar(CString str);

	CFile* m_pFile;
	//CCustomListBox m_ctrlLog;
	CListBox m_ctrlLog;
	void WriteLog(LPCTSTR lpszFormat, ...);
	CTabCtrl m_ctrlTab;
	afx_msg void OnSelchangeTabControl(NMHDR *pNMHDR, LRESULT *pResult);
	CListBox m_ctrDSCList;
	//CCustomListBox m_ctrDSCList;
	afx_msg void OnSelchangeDscList();
	afx_msg void OnSetfocusDscList();
	afx_msg void OnBnClickedBtnExportLog();

private:
	CString m_strSerialName;

public:
	BOOL GetPortName(CString& strPort);
	BOOL CheckPower();
	BOOL ONPower();
	BOOL OFFPower();
	void CheckModel();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnBnClickedBtnAdmin();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
