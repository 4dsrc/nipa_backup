/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */


#include "stdafx.h"
#include "NXInfo.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNXInfo::CNXInfo()
{
	m_strIniFilename.Empty();
}

CNXInfo::CNXInfo(LPCTSTR TGIniFilename)
{
	if(!SetIniFilename( TGIniFilename ))
	{
		//TGLog(0,_T("Ini File Name"));
	}
}

CNXInfo::~CNXInfo()
{
   // Flush .ini file
   ::WritePrivateProfileString( NULL, NULL, NULL, m_strIniFilename );
}


//////////////////////////////////////////////////////////////////////
// Methods
//////////////////////////////////////////////////////////////////////

#define MAX_INI_BUFFER 1048576   // Defines the maximum number of chars we can
								// read from the ini file 

BOOL CNXInfo::SetIniFilename(LPCTSTR strFile)
{
	ASSERT(AfxIsValidString(strFile));
	m_strIniFilename = strFile;
	if( m_strIniFilename.IsEmpty() ) 
	   return FALSE;

	//-- Check File Exist
	HANDLE hFile;
	hFile = CreateFile(strFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, NULL, NULL);
	if (hFile == INVALID_HANDLE_VALUE) 
		return FALSE;
	CloseHandle(hFile);

	return TRUE;
};

CString CNXInfo::GetString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszDefault)
{
   ASSERT(AfxIsValidString(lpszSection));
   ASSERT(AfxIsValidString(lpszEntry));
   if( m_strIniFilename.IsEmpty() ) return CString();
   TCHAR pBuffer[2048] = {0};
   long ret = ::GetPrivateProfileString( lpszSection, lpszEntry, lpszDefault, pBuffer, 2047, m_strIniFilename );
   CString s = pBuffer;

   if( ret==0 ) return CString(lpszDefault);
   return s;
};
