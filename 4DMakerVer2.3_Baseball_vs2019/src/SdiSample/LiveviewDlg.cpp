/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */

#include "stdafx.h"
#include "SdiSample.h"
#include "SdiSampleDlg.h"
#include "LiveviewDlg.h"
#include "afxdialogex.h"
#include "SdiSdk.h"

#ifndef VERTICAL
#else
#include "opencv2\imgproc.hpp"
#include "opencv2\highgui.hpp"
#include "opencv2\core.hpp"
#include "opencv2\videoio.hpp"
using namespace cv;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const int default_liveview_width		= 400;		// NX1000 width
static const int default_liveview_height	= 300;		// NX1000 height
static const int default_liveview_margin_x	= 5;
static const int default_liveview_margin_y	= 35;

//Test Code
#define TIMER_START_RECORD	WM_USER+0x0010
#define TIMER_END_RECORD	WM_USER+0x0011
#define TIMER_MAKEMOVIE		WM_USER+0x0012
#define TIMER_EXPORTLOG		WM_USER+0x0013
//#define TEST_TIMER

#define TIMER_POWER_ON					WM_USER+0x0014
#define TIMER_ENABLE_CAM_CONNECT		WM_USER+0x0015
#define TIMER_REC_WAIT_FLAG		WM_USER+0x0016

IMPLEMENT_DYNAMIC(CLiveviewDlg, CDialog)

CLiveviewDlg::CLiveviewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLiveviewDlg::IDD, pParent)
	, m_strConnect(_T("Disconnect"))	
	, m_pLiveview (NULL)
	, m_nLiveview (LIVEVIEW_ON)
	, m_nFocusMin	(0)
	, m_nFocusCur	(0)
	, m_nFocusMax	(0)
	, m_bTest(FALSE)
{
	m_bPowerOnWaitFlag = FALSE;
	m_bCamConnectWaitFlag = FALSE;
	m_bRecStartStopWaitFlag = FALSE;
	m_bRecStartFlag = FALSE;
}

CLiveviewDlg::~CLiveviewDlg()
{
	//-- 2013-11-29 hongsu.jung
	//-- Disconnet
	Disconnection();

	if(m_pLiveview)
	{
		delete m_pLiveview;
		m_pLiveview = NULL;
	}
}

void CLiveviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text	(pDX	, IDCS_STATUS	, m_strConnect);
	DDX_Radio	(pDX	, IDCR_ON		, (int&)m_nLiveview);
	DDX_Control	(pDX	, IDC_UUID		, m_ctrlUUID);	
	DDX_Control	(pDX	, IDC_TEXT_SECOND, m_ctrlTextSecond);		
	DDX_Control	(pDX	, IDCB_GET_TICK	, m_btnGetTick);
	DDX_Control	(pDX	, IDCB_SET_ENLARGE, m_btnSetEnLarge);
	DDX_Control	(pDX	, IDCB_GET_UUID	, m_btnGetUUID);
	DDX_Control	(pDX	, IDCB_SET_UUID	, m_btnSetUUID);
	DDX_Control	(pDX	, IDCB_CAPTURE	, m_btnCapture);	
	DDX_Control	(pDX	, IDCB_REC		, m_btnRec);		
	DDX_Control	(pDX	, IDCB_REC_STOP	, m_btnRecStop);		
	DDX_Control	(pDX	, IDCB_REC_VERTICAL, m_btnRecVertical);		
	DDX_Control	(pDX	, IDCR_ON		, m_btnOn);
	DDX_Control	(pDX	, IDCR_OFF		, m_btnOff);
	DDX_Control	(pDX	, IDCS_CONNECT	, m_staticConnect);
	DDX_Control	(pDX	, IDCS_STATUS	, m_staticStatus);
	DDX_Control	(pDX	, IDCS_FOCUS	, m_staticFocus);
	DDX_Control	(pDX	, IDCS_LIVEVIEW	, m_staticLiveview);
	DDX_Control	(pDX	, IDCB_NEARER	, m_btnNearer);
	DDX_Control	(pDX	, IDCB_NEAR		, m_btnNear);
	DDX_Control	(pDX	, IDCB_FAR		, m_btnFar);
	DDX_Control	(pDX	, IDCB_FARTHER	, m_btnFarther);	
	DDX_Control	(pDX	, IDCB_SET_FOCUS, m_btnSetFocus);	
	DDX_Control	(pDX	, IDCB_RECORD_TEST, m_btnRecordTest);	
	DDX_Control	(pDX	, IDCB_GET_FOCUS, m_btnGetFocus);	
	DDX_Control	(pDX	, IDCS_FOCUS_MIN, m_staticStrFocusMin);
	DDX_Control	(pDX	, IDC_FOCUS_MIN	, m_staticFocusMin);
	DDX_Text	(pDX	, IDC_FOCUS_MIN	, m_nFocusMin);
	DDX_Control	(pDX	, IDCS_FOCUS_CUR, m_staticStrFocusCur);
	DDX_Control	(pDX	, IDC_FOCUS_SET	, m_staticFocusSet);
	DDX_Control	(pDX	, IDC_FOCUS_CUR	, m_staticFocusCur);
	DDX_Text	(pDX	, IDC_FOCUS_CUR	, m_nFocusCur);
	DDX_Control	(pDX	, IDCS_FOCUS_MAX, m_staticStrFocusMax);
	DDX_Control	(pDX	, IDC_FOCUS_MAX	, m_staticFocusMax);
	DDX_Text	(pDX	, IDC_FOCUS_MAX	, m_nFocusMax);
	DDX_Control	(pDX	, IDC_COMBO_ENLARGE		, m_ctrlEnLarge);

	DDX_Control	(pDX	, IDC_BTN_HALF_PRESS, m_btnHalfPress);	
	DDX_Control	(pDX	, IDC_BTN_HALF_RELEASE, m_btnHalfRelease);	
	DDX_Control	(pDX	, IDC_BTN_HALF_ONE_SHOT, m_btnHalfOneShot);
	DDX_Control(pDX, IDC_STATIC_FOCUS_FRAME, m_staticFocusFrame);
	DDX_Control(pDX, IDC_STATIC_FOCUS_FRAME2, m_staticFocusFrameP1);
	DDX_Control(pDX, IDC_STATIC_FOCUS_FRAME3, m_staticFocusFrameP2);
	DDX_Control(pDX, IDC_STATIC_FOCUS_FRAME4, m_staticFocusFrameP3);
	DDX_Control(pDX, IDC_FOCUS_FRAME_1, m_ctrlFocusFrameP1);
	DDX_Control(pDX, IDC_FOCUS_FRAME_2, m_ctrlFocusFrameP2);
	DDX_Control(pDX, IDC_FOCUS_FRAME_3, m_ctrlFocusFrameP3);
	DDX_Control(pDX, IDC_BTN_SET_FOCUS_FRAME, m_btnSetFocusFrame);
	DDX_Control(pDX, IDC_BTN_GET_FOCUS_FRAME, m_btnGetFocusFrame);
}


BEGIN_MESSAGE_MAP(CLiveviewDlg, CDialog)
	ON_BN_CLICKED(IDCB_CAPTURE	, OnBnClickedCapture)
	ON_BN_CLICKED(IDCB_GET_TICK	, OnBnClickedGetTick)	
	ON_BN_CLICKED(IDCB_SET_ENLARGE	, OnBnClickedSetEnLarge)	
	ON_BN_CLICKED(IDCB_GET_UUID	, OnBnClickedGetUUID)	
	ON_BN_CLICKED(IDCB_SET_UUID	, OnBnClickedSetUUID)	
	ON_BN_CLICKED(IDCB_REC		, OnBnClickedRec)	
	ON_BN_CLICKED(IDCB_REC_STOP	, OnBnClickedRecStop)	
#ifndef VERTICAL
#else
	ON_BN_CLICKED(IDCB_REC_VERTICAL, OnBnClickedRecVertical)	
#endif
	ON_BN_CLICKED(IDCR_ON		, OnBnClickedLiveview)
	ON_BN_CLICKED(IDCR_OFF		, OnBnClickedLiveview)
	ON_BN_CLICKED(IDCS_CONNECT	, OnBnClickedConnect)
    ON_BN_CLICKED(IDCB_FAR		, OnBnClickedFar)
    ON_BN_CLICKED(IDCB_FARTHER	, OnBnClickedFarther)
    ON_BN_CLICKED(IDCB_NEAR		, OnBnClickedNear)
    ON_BN_CLICKED(IDCB_NEARER	, OnBnClickedNearer)	
	ON_BN_CLICKED(IDCB_SET_FOCUS, OnBnClickedSetFocus)		
	ON_BN_CLICKED(IDCB_RECORD_TEST, OnBnClickedRecordTest)		
	ON_BN_CLICKED(IDC_BTN_HALF_PRESS, &CLiveviewDlg::OnBnClickedBtnHalfPress)
	ON_BN_CLICKED(IDC_BTN_HALF_RELEASE, &CLiveviewDlg::OnBnClickedBtnHalfRelease)
	ON_BN_CLICKED(IDCB_GET_FOCUS, &CLiveviewDlg::OnBnClickedGetFocus)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_HALF_ONE_SHOT, &CLiveviewDlg::OnBnClickedBtnHalfOneShot)
	ON_BN_CLICKED(IDC_BTN_SET_FOCUS_FRAME, &CLiveviewDlg::OnBnClickedBtnSetFocusFrame)
	ON_BN_CLICKED(IDC_BTN_GET_FOCUS_FRAME, &CLiveviewDlg::OnBnClickedBtnGetFocusFrame)
	ON_BN_CLICKED(IDC_BTN_ON, &CLiveviewDlg::OnBnClickedBtnOn)
	ON_BN_CLICKED(IDC_BTN_OFF, &CLiveviewDlg::OnBnClickedBtnOff)
	ON_BN_CLICKED(IDC_BTN_CONNECT, &CLiveviewDlg::OnBnClickedBtnConnect)
	ON_BN_CLICKED(IDC_BTN_DISCONNECT, &CLiveviewDlg::OnBnClickedBtnDisconnect)
	ON_WM_CTLCOLOR()
	ON_WM_DRAWITEM()
END_MESSAGE_MAP()

BOOL CLiveviewDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	m_brush.CreateSolidBrush(RGB(52, 51, 51));

#ifndef VERTICAL
	GetDlgItem(IDCB_REC_VERTICAL)->ShowWindow(SW_HIDE);	
	//GetDlgItem(IDCB_SET_ENLARGE)->ShowWindow(SW_HIDE);	
	GetDlgItem(IDC_TEXT_SECOND)->ShowWindow(SW_HIDE);
#else	
#endif

	//-- Set Position 
	SetPosition();
	//-- Create Liveview
	m_pLiveview = new CLiveview();
	m_pLiveview->Create(NULL,_T(""),WS_VISIBLE|WS_CHILD,
		CRect(
			CPoint(	default_liveview_margin_x,
					default_liveview_margin_y),
			CSize(	default_liveview_width,
					default_liveview_height)
			),this,IDC_LIVEVIEW);

	m_ctrlEnLarge.AddString(_T("x1"));
	m_ctrlEnLarge.AddString(_T("x3"));
	m_ctrlEnLarge.AddString(_T("x4"));
	m_ctrlEnLarge.AddString(_T("x5"));
	m_ctrlEnLarge.AddString(_T("x6"));
	m_ctrlEnLarge.AddString(_T("x7"));
	m_ctrlEnLarge.AddString(_T("x8"));
	m_ctrlEnLarge.AddString(_T("x9"));
	m_ctrlEnLarge.AddString(_T("x10"));
	m_ctrlEnLarge.SetCurSel(0);

	m_ctrlTextSecond.SetWindowText(_T("0"));

	m_ctrlFocusFrameP1.SetWindowText(_T("0"));
	m_ctrlFocusFrameP2.SetWindowText(_T("0"));
	m_ctrlFocusFrameP3.SetWindowText(_T("0"));


	return TRUE;
}

void CLiveviewDlg::SetBuffer(PVOID pBuffer)
{
	if(m_pLiveview)
		m_pLiveview->SetBuffer(pBuffer);
}

void CLiveviewDlg::SetPosition()
{
	return;
	m_staticConnect		.MoveWindow( 10, 14, 80, 20);
	m_staticStatus		.MoveWindow( 90, 14,100, 20);
	m_staticLiveview	.MoveWindow(330-120, 14, 55, 20);
	m_btnOn				.MoveWindow(390-120, 10, 35, 20);
	m_btnOff			.MoveWindow(430-120, 10, 50, 20);
	m_btnCapture		.MoveWindow(480-120,  5, 60, 25);
	m_btnRec			.MoveWindow(545-120,  5, 60, 25);
	m_btnRecStop		.MoveWindow(610-120,  5, 60, 25);
	m_btnRecVertical	.MoveWindow(260-120,  5, 25, 25);
	m_ctrlTextSecond	.MoveWindow(290-120,  5, 25, 25);
	m_ctrlUUID			.MoveWindow(545-120,  30, 125, 25);
	m_btnGetUUID		.MoveWindow(545-120,  60, 60, 25);
	m_btnSetUUID		.MoveWindow(610-120,  60, 60, 25);
	m_btnGetTick		.MoveWindow(545-120,  90, 60, 25);
	m_btnSetEnLarge		.MoveWindow(545-120,  120, 60, 25);
	m_ctrlEnLarge		.MoveWindow(610-120,  123, 60, 25);

	m_btnHalfPress		.MoveWindow(545-120,  180, 60, 25);
	m_btnHalfRelease	.MoveWindow(610-120,  180, 60, 25);
	m_btnHalfOneShot	.MoveWindow(545-120,  210, 60, 25);

	m_staticFocusFrame	.MoveWindow(545-120,  250, 70, 25);
	m_staticFocusFrameP1.MoveWindow(620-120,  245, 15, 25);
	m_ctrlFocusFrameP1	.MoveWindow(635-120,  240, 40, 25);
	m_staticFocusFrameP2.MoveWindow(545-120,  270, 15, 25);
	m_ctrlFocusFrameP2	.MoveWindow(560-120,  265, 40, 25);
	m_staticFocusFrameP3.MoveWindow(620-120,  270, 15, 25);
	m_ctrlFocusFrameP3	.MoveWindow(635-120,  265, 40, 25);
	m_btnSetFocusFrame	.MoveWindow(545-120,  300, 60, 25);
	m_btnGetFocusFrame	.MoveWindow(610-120,  300, 60, 25);

	//-- Focus
	m_staticFocus		.MoveWindow(130-120,472-130, 40, 20);
	m_btnNearer			.MoveWindow(175-120,467-130, 25, 25);	
	m_btnNear			.MoveWindow(205-120,467-130, 25, 25);	
	m_btnFar			.MoveWindow(235-120,467-130, 25, 25);	
	m_btnFarther		.MoveWindow(265-120,467-130, 25, 25);

	m_staticStrFocusMin	.MoveWindow(310-130,472-130, 20, 25);	
	m_staticFocusMin	.MoveWindow(335-130,467-130, 50, 25);
	m_staticFocusMin	.EnableWindow(FALSE);
	m_staticStrFocusCur	.MoveWindow(390-130,472-130, 40, 25);
	m_staticFocusCur	.MoveWindow(435-130,467-130, 50, 25);
	m_staticStrFocusMax	.MoveWindow(490-130,472-130, 25, 25);
	m_staticFocusMax	.MoveWindow(520-130,467-130, 50, 25);
	m_staticFocusMax	.EnableWindow(FALSE);
	m_btnGetFocus		.MoveWindow(575-130,467-130, 60, 25);	
	m_btnSetFocus		.MoveWindow(575-130,467-100, 60, 25);	
	m_btnRecordTest		.MoveWindow(575-270,467-100, 60, 25);	
	m_staticFocusSet	.MoveWindow(520-130,467-100, 50, 25);	

//#ifdef VERTICAL
//	m_btnRecordTest.ShowWindow(SW_SHOW);
//#endif
//	m_btnRecordTest.ShowWindow(SW_HIDE);
}

// CLiveviewDlg message handlers
void CLiveviewDlg::OnBnClickedCapture()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_CAPTURE;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

// CLiveviewDlg message handlers
void CLiveviewDlg::OnBnClickedRec()
{
	CSdiSampleDlg* pDlg = (CSdiSampleDlg*)GetParent();
	if (m_bRecStartStopWaitFlag)
	{
		pDlg->WriteLog(_T("RecStartStop Wait."));
		return;
	}

	if (m_bRecStartFlag)
		return;

	m_bRecStartFlag = TRUE;
	m_bRecStartStopWaitFlag = TRUE;
	
	pDlg->WriteLog(_T("Clicked REC"));
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_REC;

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);

	SetTimer(TIMER_REC_WAIT_FLAG, 5000, NULL);
}

void CLiveviewDlg::OnBnClickedGetTick()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_GET_TICK;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

void CLiveviewDlg::OnBnClickedSetEnLarge()
{
	CString strValue;
	int nIndex = m_ctrlEnLarge.GetCurSel();
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_ENLARGE;
	
	
	switch(nIndex)
	{
	case 0:
		pMsg->nParam1 = 0;
		break;
	default:
		pMsg->nParam1 = nIndex + 2;
		break;
	}
	//pMsg->nParam1 = 	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

void CLiveviewDlg::OnBnClickedGetUUID()
{
	/*m_btnOn.SetCheck(FALSE);
	m_btnOff.SetCheck(TRUE);
	UpdateData(TRUE);
	SetLiveview(FALSE);
	UpdateData(FALSE);
	Sleep(100);*/

	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_GET_UUID;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);

	/*m_btnOn.SetCheck(TRUE);
	m_btnOff.SetCheck(FALSE);
	UpdateData(TRUE);
	SetLiveview(TRUE);
	UpdateData(FALSE);
	Sleep(1000);*/
}

void CLiveviewDlg::OnBnClickedSetUUID()
{
	/*m_btnOn.SetCheck(FALSE);
	m_btnOff.SetCheck(TRUE);
	UpdateData(TRUE);
	SetLiveview(FALSE);
	UpdateData(FALSE);
	Sleep(100);*/

	// 소문자X, 특수문자X
	TCHAR* tchr;
	CString str = _T("");
	int i;
	BOOL bChecked = TRUE;

	CString strTest;
	m_ctrlUUID.GetWindowText(strTest);

	if (strTest.GetLength() < 5)
		bChecked = FALSE;

	for(i = 0 ; i < strTest.GetLength() ; i++) 
	{
		str = strTest.Mid(i,1);
		tchr = (TCHAR*)(LPCTSTR)str;

		if(!IsCharAlphaNumericA(*tchr))
		{
			//AfxMessageBox(_T("Either alphabet or number"));
			bChecked = FALSE;
			//strTest.Remove(*tchr);
			//m_ctrlUUID.SetWindowTextW(strTest.MakeUpper());
		}
	}

	if (bChecked == FALSE)
	{
		CSdiSampleDlg* pDlg = (CSdiSampleDlg*)GetParent();
		pDlg->WriteLog(_T("Please checked UUID  ex) A1234, 12345, 123AB"));
		return;
	}

	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_UUID;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);

	/*m_btnOn.SetCheck(TRUE);
	m_btnOff.SetCheck(FALSE);
	UpdateData(TRUE);
	SetLiveview(TRUE);
	UpdateData(FALSE);
	Sleep(1000);*/
}


void CLiveviewDlg::OnBnClickedRecStop()
{
	CSdiSampleDlg* pDlg = (CSdiSampleDlg*)GetParent();

	if (m_bRecStartStopWaitFlag)
	{
		pDlg->WriteLog(_T("RecStartStop Wait."));
		return;
	}

	if (m_bRecStartFlag == FALSE)
		return;

	m_bRecStartFlag = FALSE;

	m_bRecStartStopWaitFlag = TRUE;

	pDlg->WriteLog(_T("Clicked REC Stop"));
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_REC_STOP;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);

	SetTimer(TIMER_REC_WAIT_FLAG, 5000, NULL);
}

#ifndef VERTICAL
#else
void CLiveviewDlg::OnBnClickedRecVertical()
{	
	CSdiSampleDlg* pParent = (CSdiSampleDlg*)GetParent();
	CString tmpCstring;
	tmpCstring.Format(_T("%s\\%s\\"), pParent->m_pDlgProperty->m_strPath, pParent->m_strDate);
	
	
	
	//string strFilePath = "C:\\picture\\";
	string strFilePath;

	CT2CA pszConvertedAnsiString(tmpCstring);
	strFilePath = pszConvertedAnsiString;

	CString strPath(strFilePath.c_str());

	CString strSecond;
	GetTextSecond(strSecond);
	SetFileNameInFolder(strPath,strSecond);
	
	vector<string> strFileName;
	vector<string> strInputFile;
	vector<VideoCapture> vc;

	for(int i = 0; i < m_arrFileName.size(); i++)
	{		
		CT2CA pszConvertedAnsiString (m_arrFileName[i] + ".mp4");
		std::string s(pszConvertedAnsiString);  

		strFileName.push_back(s);
		
		strInputFile.push_back(strFilePath + strFileName[i]);
		
		VideoCapture vc_(strInputFile[i]);
		vc.push_back(vc_);		
	}

	for(int i = 0; i < vc.size(); i++)
	{		
		if(!vc[i].isOpened())
		{
			vc.erase(vc.begin() + i);
			strFileName.erase(strFileName.begin() + i);
			strInputFile.erase(strInputFile.begin() + i);
		}		
	}

	CT2CA ansiStrSecond (strSecond + ".mp4");
	std::string s(ansiStrSecond);  

	string strOutputName = "Vertical_" + s;	
		
	Mat frame;
	vector<Mat> arrFrame;

	if(vc.empty())
		return;

	for(int i = 0; i < vc.size(); i++)
	{
		vc[i] >> frame;	
		arrFrame.push_back(frame.clone());

		SendVerticalLogMessage(i);	
	}

	VideoWriter writer; 
	if (!writer.isOpened()) 						
		writer.open(strFilePath + strOutputName, VideoWriter::fourcc('m','p','e','g'), 30, frame.size());
	
	for(int j = 0; j < 5; j++)
	{
		for(int i = 0; i < arrFrame.size(); i++)
		{			
			CString temp = m_arrFileName[i];

			int retval= 0;
			retval = temp.Find('_');
			temp = temp.Left(retval);

			CT2CA pszConvertedAnsiString (temp);
			std::string strCameraId(pszConvertedAnsiString);

			CvFont Font;
			cvInitFont(&Font, CV_FONT_HERSHEY_SIMPLEX, 5, 5, 0, 10, 8);
			rectangle(arrFrame[i], Point(100, 10), Point(720, 210), Scalar(0, 0, 0), CV_FILLED );
			putText(arrFrame[i],strCameraId,cv::Point(100,150),CV_FONT_HERSHEY_SIMPLEX,5,Scalar(255,255,255),10,8);

			writer << arrFrame[i]; 
		}
	}

	writer.release();  
	m_arrFileName.clear();
	SendVerticalEncodingEndLog();
}

void CLiveviewDlg::SetFileNameInFolder(CString strPath, CString strSecond)
{	
	CString tpath = strPath + _T("*.*");
		
	CFileFind finder;	
	BOOL bWorking = finder.FindFile(tpath);

	CString fileName;
	CString DirName;

	while (bWorking)
	{		
		bWorking = finder.FindNextFile();
	
		if (finder.IsArchived())
		{	
			CString _fileName =  finder.GetFileName();
		
			if( _fileName == _T(".") || 
				_fileName == _T("..")|| 
				_fileName == _T("Thumbs.db") ) continue;

			CString temp = finder.GetFileTitle();

			int retval= 0;
			retval = temp.Find('_');
			
			CString strParseSecond = temp.Mid(retval+1);
			CString strParseCameraId = temp.Left(retval);
			
			if(strParseSecond.CompareNoCase(strSecond))
				continue;
			if(!strParseCameraId.CompareNoCase(_T("Vertical")))
				continue;

			m_arrFileName.push_back(finder.GetFileTitle());
		}		
	}	
}
#endif

void CLiveviewDlg::OnBnClickedLiveview()
{
	UpdateData(TRUE);
	switch(m_nLiveview)
	{
	case LIVEVIEW_OFF	:	SetLiveview(FALSE);		break;
	case LIVEVIEW_ON	:	SetLiveview(TRUE);		break;
	}
	UpdateData(FALSE);
}

void CLiveviewDlg::Disconnection()
{
	m_strConnect.Format(_T("Disconnect"));	
	SetLiveview(FALSE);
	m_nLiveview = LIVEVIEW_OFF;
	UpdateData(FALSE);
}

void CLiveviewDlg::Connection()
{
	m_strConnect.Format(_T("Connect"));
	SetLiveview(TRUE);
	m_nLiveview = LIVEVIEW_OFF;
	m_nLiveview = LIVEVIEW_ON;
	UpdateData(FALSE);
}

void CLiveviewDlg::SetLiveview(BOOL bLiveview)
{
	m_pLiveview->SetLiveview(bLiveview);
	//-- Remove Previous Image
	if(!bLiveview)
		m_pLiveview->DrawBlank();

	CSdiSampleDlg* pMain = (CSdiSampleDlg*)GetParent();
	if(!pMain)
		return;
	pMain->SetLiveview(bLiveview);	
}

void CLiveviewDlg::OnBnClickedFar()
{
#if 0
    SendFocusMsg(SDI_FOCUS_FAR);
#else
	SendFocusMsg(GH5_FOCUS_FAR_SLOW);
#endif
}

void CLiveviewDlg::OnBnClickedFarther()
{
#if 0
	SendFocusMsg(SDI_FOCUS_FARTHER);
#else
	SendFocusMsg(GH5_FOCUS_FAR_FAST);
#endif
}

void CLiveviewDlg::OnBnClickedNear()
{
#if 0
	SendFocusMsg(SDI_FOCUS_NEAR);
#else
	SendFocusMsg(GH5_FOCUS_NEAR_SLOW);
#endif
}

void CLiveviewDlg::OnBnClickedNearer()
{
#if 0
	SendFocusMsg(SDI_FOCUS_NEARER);
#else
	SendFocusMsg(GH5_FOCUS_NEAR_FAST);
#endif
}

void CLiveviewDlg::SendFocusMsg(ULONG nFocusType)
{	
#if 0
    SdiMessage* pMsg = new SdiMessage();
    pMsg->message = WM_SAMPLE_OP_SET_FOCUSTYPE;
    pMsg->nParam1 = nFocusType;
    ::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
#else
	// GH5
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_CUSTOM_PROPERTY_VALUE;
	pMsg->nParam1 = PTP_OC_SAMSUNG_SetFocusPosition;
	pMsg->nParam2 = nFocusType;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);

	SdiMessage* pMsg1 = new SdiMessage();
	pMsg1->message = WM_SAMPLE_OP_FOCUS_LOCATE_SAVE;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg1->message, (LPARAM)pMsg1);


	BankSave();

#endif

}

void CLiveviewDlg::OnBnClickedRecordTest()
{
	if(m_bTest)
		m_bTest = FALSE;
	else
		m_bTest = TRUE;

	if(m_bTest)
	{
		SetTimer(TIMER_START_RECORD, 1000, NULL);
		m_btnRecordTest.SetWindowText(_T("Stop"));
	}
	else
	{
		m_btnRecordTest.SetWindowText(_T("Test"));
		OnBnClickedRecStop();
		KillTimer(TIMER_START_RECORD);
		KillTimer(TIMER_END_RECORD);
		KillTimer(TIMER_MAKEMOVIE);
		KillTimer(TIMER_EXPORTLOG);
	}
}

void CLiveviewDlg::OnBnClickedSetFocus()
{
	UpdateData(TRUE);
#if 0
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_FOCUSVALUE;
	pMsg->nParam1 = m_nFocusCur;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
#else
	// GH5
	SetFocus();
#endif
}

void CLiveviewDlg::SetFocus()
{
	CString strCurrentValue;
	m_staticFocusCur.GetWindowText(strCurrentValue);

	CString strSetValue;
	m_staticFocusSet.GetWindowText(strSetValue);

	if (strCurrentValue.IsEmpty() || strSetValue.IsEmpty())
		return;

	int nLength = strSetValue.GetLength();

	for(int i=0 ; i<nLength ; i++)
	{
		char ch = strSetValue.GetAt(i);

		if( !((ch >= '0') && (ch <= '9') ) )
			return;
	}

	int nCurFocusValue = _ttoi(strCurrentValue);
	int nSetFocusValue = _ttoi(strSetValue);
	
	if (nCurFocusValue == nSetFocusValue)
		return;

	int nGap = nCurFocusValue - nSetFocusValue;
	int nAbsGap = abs(nGap);

	if (nAbsGap < STEP4/2 + 1)
		return;

	int nStep170 = 0;
	int nStep17 = 0;
	double dStep17 = 0;

	nStep170 = nAbsGap / STEP170;

	dStep17 = (double)(nAbsGap % STEP170) / (double)STEP4;
	nStep17 = floor(dStep17+0.5);
		
	for (int i = 0; i < nStep170 ; i++)
	{
		if (nGap > 0)
			OnBnClickedFarther();
		else
			OnBnClickedNearer();
	}

	for (int i = 0; i < nStep17 ; i++)
	{
		if (nGap > 0)
			OnBnClickedFar();
		else
			OnBnClickedNear();
	}
}

void CLiveviewDlg::GetFocus()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_GET_FOCUSVALUE;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

void CLiveviewDlg::GetFocusValue(int nMin, int nCurr, int nMax)
{
	m_nFocusMin = nMin;
	m_nFocusCur= nCurr;
	m_nFocusMax	= nMax;
	UpdateData(FALSE);
}

////
void CLiveviewDlg::OnBnClickedConnect()
{
	UpdateData(TRUE);
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_CONNECT_WIFI;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
	UpdateData(FALSE);
}

#ifndef VERTICAL
#else
void CLiveviewDlg::SendVerticalLogMessage(int nFrame)
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_WRITE_LOG_VERTICAL_FRAME;
	pMsg->nParam1 = nFrame;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

void CLiveviewDlg::SendVerticalEncodingEndLog()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_WRITE_LOG_VERTICAL_END;

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}
#endif


void CLiveviewDlg::BankSave()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_BANK_SAVE;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

void CLiveviewDlg::SetFocusValue( int nValue )
{
	CString str;
	str.Format(_T("%d"), nValue); 

	m_staticFocusCur.SetWindowText(str);
}



void CLiveviewDlg::OnBnClickedBtnHalfPress()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_HALF_SHUTTER;	
	pMsg->nParam1 = 0;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


void CLiveviewDlg::OnBnClickedBtnHalfRelease()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_HALF_SHUTTER;	
	pMsg->nParam1 = 1;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


void CLiveviewDlg::OnBnClickedBtnHalfOneShot()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_HALF_SHUTTER;	
	pMsg->nParam1 = 2;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


void CLiveviewDlg::OnBnClickedGetFocus()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_GET_CUSTOM_PROPERTY_DESC;
	pMsg->nParam1 = PTP_OC_SAMSUNG_GetFocusPosition;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


void CLiveviewDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	switch(nIDEvent)
	{
	case TIMER_START_RECORD:
		OnBnClickedRec();
		KillTimer(TIMER_START_RECORD);
		SetTimer(TIMER_END_RECORD, 17000, NULL);
		break;
	case TIMER_END_RECORD:
		OnBnClickedRecStop();
		GetFocusValueAll();
		KillTimer(TIMER_END_RECORD);
		SetTimer(TIMER_MAKEMOVIE, 8000, NULL);
		break;
	case TIMER_MAKEMOVIE:
#ifdef VERTICAL
		OnBnClickedRecVertical();
#endif
		KillTimer(TIMER_MAKEMOVIE);
		SetTimer(TIMER_EXPORTLOG, 8000, NULL);
		break;
	case TIMER_EXPORTLOG:
		{
			KillTimer(TIMER_EXPORTLOG);
			CString strPath;
			
			CSdiSampleDlg* pParent = (CSdiSampleDlg*)GetParent();
			strPath.Format(_T("%s\\%s\\Log.txt"), pParent->m_pDlgProperty->m_strPath, pParent->m_strDate);
			pParent->SaveLogFile(strPath);

			for(int i = 0 ; i < pParent->m_ctrlLog.GetCount(); i++)
				pParent->m_ctrlLog.DeleteString(0);

			SetTimer(TIMER_START_RECORD, 8000, NULL);
		}
		break;
	case TIMER_POWER_ON:
		{
			m_bPowerOnWaitFlag = FALSE;
			KillTimer(TIMER_POWER_ON);
		}
	case TIMER_ENABLE_CAM_CONNECT:
		{
			m_bCamConnectWaitFlag = FALSE;
			KillTimer(TIMER_ENABLE_CAM_CONNECT);
		}
		break;
	case TIMER_REC_WAIT_FLAG:
		{
			m_bRecStartStopWaitFlag = FALSE;
			KillTimer(TIMER_REC_WAIT_FLAG);
		}
		break;
	}

	CDialog::OnTimer(nIDEvent);
}

void CLiveviewDlg::GetFocusValueAll()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_GET_FOCUSVALUE_ALL;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


void CLiveviewDlg::SetAdminMode( BOOL b )
{
	GetDlgItem(IDC_UUID)->ShowWindow(b);
	GetDlgItem(IDCB_GET_UUID)->ShowWindow(b);
	GetDlgItem(IDCB_SET_UUID)->ShowWindow(b);
	GetDlgItem(IDCB_GET_TICK)->ShowWindow(b);
	GetDlgItem(IDCB_RECORD_TEST)->ShowWindow(b);
	GetDlgItem(IDCB_REC_VERTICAL)->ShowWindow(b);
	GetDlgItem(IDC_TEXT_SECOND)->ShowWindow(b);

}

void CLiveviewDlg::OnBnClickedBtnSetFocusFrame()
{
	CString strX;
	m_ctrlFocusFrameP1.GetWindowText(strX);
	CString strY;
	m_ctrlFocusFrameP2.GetWindowText(strY);
	CString strMag;
	m_ctrlFocusFrameP3.GetWindowText(strMag);

	int nX = 0;
	int nY = 0;
	int nMagnification = 0;

	nX = _ttoi(strX);
	nY = _ttoi(strY);
	nMagnification = _ttoi(strMag);

	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_FOCUS_FRAME;	
	pMsg->nParam1 = nX;
	pMsg->nParam2 = nY;
	pMsg->nParam3 = nMagnification;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


void CLiveviewDlg::OnBnClickedBtnGetFocusFrame()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_GET_FOCUS_FRAME;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}



void CLiveviewDlg::OnBnClickedBtnOn()
{	
	m_bPowerOnWaitFlag = TRUE;
	m_bCamConnectWaitFlag = FALSE;
	
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_POE;
	pMsg->nParam1 = 1;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);

	SetTimer(TIMER_POWER_ON, 1000*2, NULL);
}


void CLiveviewDlg::OnBnClickedBtnOff()
{
	m_bPowerOnWaitFlag = FALSE;
	m_bCamConnectWaitFlag = FALSE;

	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_POE;
	pMsg->nParam1 = 0;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

void CLiveviewDlg::OnBnClickedBtnConnect()
{
	if (m_bPowerOnWaitFlag)
	{
		CSdiSampleDlg* pDlg = (CSdiSampleDlg*)GetParent();
		pDlg->WriteLog(_T("Please wait. Waiting for the camera to turn on.."));
		return;
	}

	if (m_bCamConnectWaitFlag)
		return;

	m_bCamConnectWaitFlag = TRUE;

	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_CONNECT;
	pMsg->nParam1 = 0;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	

	SetTimer(TIMER_ENABLE_CAM_CONNECT, 1000*3, NULL);
}


void CLiveviewDlg::OnBnClickedBtnDisconnect()
{
	m_bCamConnectWaitFlag = FALSE;

	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_CONNECT;
	pMsg->nParam1 = 1;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


HBRUSH CLiveviewDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	COLORREF textRGB = RGB(255,255,255);

	//if (nCtlColor == CTLCOLOR_STATIC)  
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(textRGB);
	}

	return m_brush;
}


void CLiveviewDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	COLORREF btnRGB = RGB(87,87,86);
	COLORREF textRGB = RGB(255,255,255);
	COLORREF selectBtnRGB = RGB(36,36,34);
	CDC dc;
	RECT rect;
	dc.Attach(lpDrawItemStruct->hDC);	//Get the Button DC to CDC
	rect = lpDrawItemStruct->rcItem	;	//Store the Button rect to local rect
	
	//dc.Draw3dRect(&rect, textRGB, testrgb1);
	//dc.FillSolidRect(&rect, testrgb1);

	UINT state = lpDrawItemStruct->itemState;
	if((state & ODS_SELECTED))
	{
		dc.FillSolidRect(&rect, selectBtnRGB);
		dc.SetBkColor(selectBtnRGB);		
	}
	else
	{
		dc.FillSolidRect(&rect, btnRGB);
		dc.SetBkColor(btnRGB);
	}


// 	if((state & ODS_SELECTED))
// 		dc.DrawEdge(&rect, EDGE_SUNKEN, BF_RECT);
// 	else
// 		dc.DrawEdge(&rect, EDGE_RAISED, BF_RECT);

	//Draw Color Text
	dc.SetTextColor(textRGB);		//Setting the Text Color

	TCHAR buffer[MAX_PATH];
	ZeroMemory(buffer, MAX_PATH);
	::GetWindowText(lpDrawItemStruct->hwndItem, buffer, MAX_PATH);

	UINT nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
	RECT textRect = rect;
	if (nIDCtl == IDC_BTN_HALF_ONE_SHOT)
	{
		textRect.left = rect.left;
		textRect.right = rect.right;
		textRect.top = (rect.top+rect.bottom)/2-15;
		textRect.bottom = rect.bottom;
		nFormat = DT_CENTER|DT_VCENTER;		
	}

	dc.DrawText(buffer, &textRect, nFormat);

	dc.Detach();		

	//CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

