/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */



#pragma once
#include "afxwin.h"
#include "DevInfoManager.h"

// CPropertyDlg dialog

class CPropertyDlg : public CDialog
{
	DECLARE_DYNAMIC(CPropertyDlg)

public:
	CPropertyDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPropertyDlg();
//-- joonho.kim Temp
	int m_nLarge;
	BOOL m_bPause;
// Dialog Data
	enum { IDD = IDD_PROPERTY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	afx_msg void OnCbnSelchangeComboMode	();
	afx_msg void OnCbnSelchangeComboSize	();
	afx_msg void OnCbnSelchangeComboQuality	();
	afx_msg void OnCbnSelchangeComboWB		();
	afx_msg void OnCbnSelchangeComboMovieSize ();
	afx_msg void OnCbnSelchangeComboISO		();
	afx_msg void OnCbnSelchangeComboSS		();
	afx_msg void OnCbnSelchangeComboAFMode	();
	afx_msg void OnCbnSelchangeComboFNumber	();
	afx_msg void OnCbnSelchangeComboPW();
	afx_msg void OnSelchangeComboLensPower();	
	afx_msg void OnSelchangeComboLcd();
	afx_msg void OnSelchangeComboPeaking();	
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CBrush m_brush; 

public:
	CDevInfoManager* m_pDevMgr;
	//-- 2013-02-12 hongsu.jung
	//-- Model Name
	BOOL m_bTimerOn;
	CString m_strModel;

	CComboBox m_ctrlMode;
	CComboBox m_ctrlPhotosize;
	CComboBox m_ctrlQuality;	
	CComboBox m_ctrlWB;	
	CComboBox m_ctrlMoviesize;	
	CComboBox m_ctrlISO;
	CComboBox m_ctrlSS;
	CComboBox m_ctrlAfMode;
	CComboBox m_ctrlFNumber;
	CComboBox m_ctrlPW;	
	CComboBox m_ctrlLensPower;
	CComboBox m_ctrlLCD;
	CComboBox m_ctrlPeaking;
	CString	  m_strPath;	

	//CMiLRe 2015129 �� ��Ʈ��
	CEdit			m_editZoom;
	CEdit			m_editZoomRange;
	int m_nIndex;
	int m_nInterval;
	//CMiLRe 2015129 �� ��Ʈ��
	int m_nZoom;

public:

	void UpdateCombo(int nPTPCode);
	//CMiLRe 2015129 �� ��Ʈ��
	void OnUpdateValue(int nPTPCode);	
	void Clean();
	void UpdatePropertyStr	(int nPropCode, IDeviceInfo* pDevInfo, CComboBox* pCombo);
	void UpdatePropertyEnum	(int nPropCode, IDeviceInfo* pDevInfo, CComboBox* pCombo);	

	void DisConnect();
	void CaptureOnce();
	void AddDevInfo(IDeviceInfo *pDevInfo);
	CDevInfoManager* GetDevMgr()	{return m_pDevMgr;}
	BOOL IsExist(CComboBox* pCtrlCombo, CString strValue);

	CString LoadInfoStringToDesc(int nPTPCode, CString strValue);
	CString LoadInfoToDesc(int nPTPCode, int nValue);
	CString LoadInfoIntToDesc(int nPTPCode, int nValue);
	CString LoadInfoHexToDesc(int nPTPCode, int nValue);

	//CMiLRe 2015129 �� ��Ʈ��
	void UpdatePropertyZoom(IDeviceInfo* pDevInfo);

	afx_msg void OnBnClickedDirOpen();
	afx_msg void OnBnClickedDirOpen2();
	afx_msg void OnBnClickedInterval();
	//20150128 CMiLRe NX3000
	afx_msg void OnBnClickedReboot();
	afx_msg void OnBnClickedShutDown();
	afx_msg void OnBnClickedFWDownLoad_N_Updawn();
	//CMiLRe 2015129 �� ��Ʈ��
	afx_msg void OnBnClickedZoom();		

	
	CEdit m_ctrlPropertyCode;
	CEdit m_ctrlPropertyValue;
	afx_msg void OnBnClickedBtnGetProperty();
	afx_msg void OnBnClickedBtnSetProperty();
	CString ConvertToHex(CString data);
	afx_msg void OnBnClickedBtnBankSave();
	afx_msg void OnBnClickedBtnExportLog();
	CButton m_ctrlAllFlag;
	afx_msg void OnBnClickedBtnFocusLocateSave();
	afx_msg void OnBnClickedBtnSensorCleaning();
	afx_msg void OnBnClickedBtnVersion();	
	
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);

	void SetAdminMode(BOOL b);
	afx_msg void OnBnClickedBtnBankSaveAll();
};