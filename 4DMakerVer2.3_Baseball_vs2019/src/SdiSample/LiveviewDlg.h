/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */

#pragma once

#define VERTICAL

#ifndef _NonOpenCV
#include "Liveview.h"
#else
#include "LiveviewPure.h"
#endif

#include "afxwin.h"
#include <vector>
using namespace std;

// CLiveviewDlg dialog

class CLiveviewDlg : public CDialog
{
	DECLARE_DYNAMIC(CLiveviewDlg)

public:
	CLiveviewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLiveviewDlg();

	void BankSave();
	void SetFocusValue(int nValue);

enum
{
	LIVEVIEW_ON = 0,
	LIVEVIEW_OFF
};

// Dialog Data
	enum { IDD = IDD_LIVEVIEW};

	enum FOCUS_STEP
	{
		STEP4 = 4,
		STEP170 = 170
	};

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()

	void SetPosition();

	CBrush m_brush; 

	CString m_strConnect;
	CLiveview* m_pLiveview;

	UINT	m_nLiveview;
	
    CStatic m_staticLiveview;
    CStatic m_staticConnect;
    CStatic m_staticStatus;
	CStatic m_staticFocus;	
	CStatic m_staticFocusMin;
	CStatic m_staticFocusCur;
	CStatic m_staticFocusSet;
	CStatic m_staticFocusMax;
	CStatic m_staticStrFocusMin;
	CStatic m_staticStrFocusCur;
	CStatic m_staticStrFocusMax;
	CEdit m_ctrlUUID;
	CEdit m_ctrlTextSecond;
	CButton m_btnCapture;
	CButton m_btnRec;
	CButton m_btnRecStop;
	CButton m_btnRecVertical;
	CButton	m_btnGetTick;	
	CButton	m_btnSetEnLarge;
	CButton	m_btnGetUUID;	
	CButton	m_btnSetUUID;	
	CButton m_btnNearer;
    CButton m_btnNear;
    CButton m_btnFar;
    CButton m_btnFarther;
	CButton m_btnSetFocus;
	CButton m_btnRecordTest;
	CComboBox m_ctrlEnLarge;

	BOOL m_bTest;

	CButton m_btnOn;
    CButton m_btnOff;
	CButton m_btnConnect;

	//-- 2013-12-05 hongsu@esmlab.com
	//-- Set Focus
	int m_nFocusMin;
	int m_nFocusCur;
	int m_nFocusMax;

	CButton m_btnHalfPress;
	CButton m_btnHalfRelease;
	CButton m_btnHalfOneShot;

	CButton m_btnGetFocus;

	CStatic m_staticFocusFrame;
	CStatic m_staticFocusFrameP1;
	CStatic m_staticFocusFrameP2;
	CStatic m_staticFocusFrameP3;
	CEdit m_ctrlFocusFrameP1;
	CEdit m_ctrlFocusFrameP2;
	CEdit m_ctrlFocusFrameP3;
	CButton m_btnSetFocusFrame;
	CButton m_btnGetFocusFrame;

	// cam connect wait flag
	BOOL m_bPowerOnWaitFlag;
	BOOL m_bCamConnectWaitFlag;

	// rec start/stop flag
	BOOL m_bRecStartStopWaitFlag;
	BOOL m_bRecStartFlag;

public:
	afx_msg void OnBnClickedSetEnLarge();
	afx_msg void OnBnClickedGetTick();
	afx_msg void OnBnClickedSetUUID();
	afx_msg void OnBnClickedGetUUID();
	afx_msg void OnBnClickedCapture();	
	afx_msg void OnBnClickedRec();	
	afx_msg void OnBnClickedRecStop();	
#ifndef VERTICAL
#else
	afx_msg void OnBnClickedRecVertical();
#endif
	afx_msg void OnBnClickedLiveview();	
	afx_msg void OnBnClickedConnect();	
	
    afx_msg void OnBnClickedFar();
    afx_msg void OnBnClickedFarther();
    afx_msg void OnBnClickedNear();
    afx_msg void OnBnClickedNearer();
	afx_msg void OnBnClickedSetFocus();
	afx_msg void OnBnClickedRecordTest();
	//wgkim
#ifndef VERTICAL
#else
	void SetFileNameInFolder(CString strPath, CString strSecond);
	void SendVerticalLogMessage(int nFrame);
	void SendVerticalEncodingEndLog();
	vector<CString> m_arrFileName;
#endif

	void Disconnection();
	void Connection();
	CLiveview* GetLiveview()	{ return m_pLiveview; }
	void SetBuffer(PVOID pBuffer); 	
	void SetLiveview(BOOL bLiveview);
	void SetFocus();
	void GetFocus();
	void GetFocusValue(int nMin, int nCur, int nMax);		
	void SetUUID(CString strUUID) { m_ctrlUUID.SetWindowText(strUUID);};
	void GetUUID(CString& strUUID) { m_ctrlUUID.GetWindowText(strUUID);};
	void GetTextSecond(CString& strTextSecond) { m_ctrlTextSecond.GetWindowText(strTextSecond);};
	void GetFocusValueAll();
	void SetAdminMode(BOOL b);

private:
    void SendFocusMsg(ULONG nFocusType);
public:
	afx_msg void OnBnClickedBtnHalfPress();
	afx_msg void OnBnClickedBtnHalfRelease();
	afx_msg void OnBnClickedBtnHalfOneShot();
	afx_msg void OnBnClickedGetFocus();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedBtnSetFocusFrame();
	afx_msg void OnBnClickedBtnGetFocusFrame();	
	afx_msg void OnBnClickedBtnPowerOn();
	afx_msg void OnBnClickedBtnPowerOff();
	afx_msg void OnBnClickedBtnOn();
	afx_msg void OnBnClickedBtnOff();
	afx_msg void OnBnClickedBtnConnect();
	afx_msg void OnBnClickedBtnDisconnect();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);	
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
};
