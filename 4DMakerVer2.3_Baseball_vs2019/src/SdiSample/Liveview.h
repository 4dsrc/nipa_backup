/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */


#pragma once
#include "cv.h"
#include "highgui.h"

// CLiveview view

class CLiveview : public CView
{
	DECLARE_DYNCREATE(CLiveview)

public:
	CLiveview();	
	virtual ~CLiveview();

public:
	virtual void OnDraw(CDC* pDC) {};      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	
protected:
	DECLARE_MESSAGE_MAP()

public:
	void SetLiveview(BOOL b)			{ m_bLiveview = b;		Invalidate(FALSE);	}
	void SetBuffer(PVOID pBuffer)		{ m_pLiveviewBuffer = (IplImage*)pBuffer;	}
	void DrawBlank();
	void SetEVFLive(BOOL bEVF)			{ m_bEVF = bEVF; }
	
		
private:	
	BOOL m_bLiveview;		//-- Show or Not		
	IplImage* m_pLiveviewBuffer;
	BOOL m_bEVF;
	BOOL m_bPreLive;

	afx_msg void OnPaint();	
	//-- 2011-05-16	
	void DrawLiveview();	
};

