/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	2.1
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 13-12-05	hongsu@esmlab.com	for SDK ver.2.1
 */

#include "stdafx.h"
#include "SdiSample.h"
#include "SdiSampleDlg.h"
#include "afxdialogex.h"
#include "SdiSdk.h"

#define	ESMLOG_LINE_SIZE		4096
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


enum FRAME_RATE
{
	MOVIE_FPS_UHD_25P = 0,
	MOVIE_FPS_UHD_30P,
	MOVIE_FPS_UHD_60P,
	MOVIE_FPS_UHD_50P,
	MOVIE_FPS_FHD_60P,
	MOVIE_FPS_FHD_50P,
	MOVIE_FPS_FHD_30P,
	MOVIE_FPS_FHD_25P,
	MOVIE_FPS_UNKNOWN,
};
// CSdiSampleDlg dialog

CSdiSampleDlg::CSdiSampleDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSdiSampleDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pSdiSDK = NULL;
	m_pDlgLiveview = NULL;
	m_pDlgProperty = NULL;

	m_nMovieCount = 0;
	m_nFocus = 0;
	m_pFile = NULL;
	m_nControlerType = USB_CONTROLER_KOREA;
	m_bPowerFlag = FALSE;
	m_bConnectFlag = FALSE;
}

CSdiSampleDlg::~CSdiSampleDlg()
{
	m_pSerial.ClosePort();
}

void CSdiSampleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOG, m_ctrlLog);
	DDX_Control(pDX, IDC_TAB_CONTROL, m_ctrlTab);
	DDX_Control(pDX, IDC_DSC_LIST, m_ctrDSCList);

	//m_pSerial.ClosePort();
}

BEGIN_MESSAGE_MAP(CSdiSampleDlg, CDialogEx)
	ON_WM_CLOSE()
	ON_MESSAGE(WM_SDI,OnSDIMsg)
	ON_MESSAGE(WM_SDI_SAMPLE,OnDialogMsg)	
	ON_MESSAGE(WM_DEVICECHANGE,OnChangeConnect)
	ON_WM_TIMER()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_CONTROL, &CSdiSampleDlg::OnSelchangeTabControl)
	ON_LBN_SELCHANGE(IDC_DSC_LIST, &CSdiSampleDlg::OnSelchangeDscList)
	ON_LBN_SETFOCUS(IDC_DSC_LIST, &CSdiSampleDlg::OnSetfocusDscList)
	ON_BN_CLICKED(IDC_BTN_EXPORT_LOG, &CSdiSampleDlg::OnBnClickedBtnExportLog)
	ON_WM_CTLCOLOR()
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(IDC_BTN_ADMIN, &CSdiSampleDlg::OnBnClickedBtnAdmin)
END_MESSAGE_MAP()

//------------------------------------------------------------------------------
//! @date		2013-12-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Close Message
//------------------------------------------------------------------------------
void CSdiSampleDlg::OnClose()
{
	ShowWindow(SW_HIDE);
	//-- Close/Disconnect SDK
	CloseSDK();

	//-- Remove Dialogs
	if(m_pDlgProperty)
	{
		delete m_pDlgProperty;
		m_pDlgProperty = NULL;
	}

	if(m_pDlgLiveview)
	{
		delete m_pDlgLiveview;
		m_pDlgLiveview = NULL;
	}
	CDialogEx::OnClose();
}


//------------------------------------------------------------------------------
//! @date		2013-12-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Close SDK
//------------------------------------------------------------------------------
void CSdiSampleDlg::CloseSDK()
{
	if(m_pDlgLiveview)
		m_pDlgLiveview->Disconnection();
	//-- 2013-03-01 hongsu.jung
	if(m_pDlgProperty)
		m_pDlgProperty->DisConnect();
	//-- 2013-11-29 Clear SDK
	if(m_pSdiSDK)
	{
		delete m_pSdiSDK;
		m_pSdiSDK = NULL;
	}
}

char* CStringToChar(CString str)
{
	wchar_t* wchar_str;     //첫번째 단계(CString to wchar_t*)를 위한 변수 
	char*    char_str;      //char* 형의 변수 
	int      char_str_len;  //char* 형 변수의 길이를 위한 변수   
	//1. CString to wchar_t* conversion 
	wchar_str = str.GetBuffer(str.GetLength());   //2. wchar_t* to char* conversion //char* 형에 대한길이를 구함 
	char_str_len = WideCharToMultiByte(CP_ACP, 0, wchar_str, -1, NULL, 0, NULL, NULL); 
	char_str = new char[char_str_len];  //메모리 할당 //wchar_t* to char* conversion 
	WideCharToMultiByte(CP_ACP, 0, wchar_str, -1, char_str, char_str_len, 0,0);
	str.ReleaseBuffer();
	return char_str;
}

void CSdiSampleDlg::WriteLog(LPCTSTR lpszFormat, ...)
{
	

	if(NULL == lpszFormat)	
		return;

	TCHAR pszMsg[ESMLOG_LINE_SIZE] = {0};

	va_list args;
	va_start(args, lpszFormat);
	_vstprintf(pszMsg, lpszFormat, args);
	va_end(args);

	int nLength = (int)_tcslen(pszMsg);
	if(!nLength)
		return;

	CString strMsg;

	strMsg.Format(_T("[%u] %s"),GetTickCount(), pszMsg );
	while(m_ctrlLog.GetCount() > 3000)
	{
		m_ctrlLog.DeleteString(0);
	}

	m_ctrlLog.InsertString(-1,strMsg);
	m_ctrlLog.SetTopIndex(m_ctrlLog.GetCount()-1);

	m_arrLog.Add(strMsg);
#if 0 // Test Log
	strMsg.Append(_T("\r\n"));
	if(!m_pFile)
	{
		m_pFile = new CFile;
		m_pFile->Open(_T("C:\\picture\\test.csv"), CFile::modeCreate | CFile::modeReadWrite);
	}

	if(m_pFile)
	{
		m_pFile->SeekToEnd();
		char* pstrData = NULL;
		pstrData = CStringToChar(strMsg);
		m_pFile->Write(pstrData,strlen(pstrData));
		delete[] pstrData;
		pstrData = NULL;
	}
#endif
}
//------------------------------------------------------------------------------
//! @date		2013-12-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Init Dialog
//------------------------------------------------------------------------------
BOOL CSdiSampleDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_brush.CreateSolidBrush(RGB(52, 51, 51));

	m_ctrlTab.DeleteAllItems();
	m_ctrlTab.InsertItem(0, _T("LiveView"));
	m_ctrlTab.InsertItem(1, _T("Property"));
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			
	SetIcon(m_hIcon, FALSE);		

	this->MoveWindow(0,0,770,630);

	m_pDlgLiveview = new CLiveviewDlg();
	m_pDlgLiveview->Create(CLiveviewDlg::IDD, this);
	m_pDlgLiveview->MoveWindow(1,23,567,408);
	m_pDlgLiveview->ShowWindow(TRUE);

	m_pDlgProperty = new CPropertyDlg();
	m_pDlgProperty->Create(CPropertyDlg::IDD, this);
	m_pDlgProperty->MoveWindow(1,23,567,408);
	m_pDlgProperty->ShowWindow(FALSE);
	
	//-- Create Save Folder
	CreateDirectory(m_pDlgProperty->m_strPath, NULL);
	
	//-- Set Start Tick Count
	m_Clock.Start();

	m_ctrlLog.InitStorage(10000,10);
	//-- Auto Connect Camera
	//for(int i = 0; i < 100; i++)
// 	WriteLog(_T("Check Connect"));
//	CheckConnect();

	CheckModel();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

//------------------------------------------------------------------------------
//! @date		2013-12-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Get Message From Changing USB Connection
//------------------------------------------------------------------------------
LRESULT CSdiSampleDlg::OnChangeConnect(WPARAM wParam, LPARAM lParam)
{	
	//WriteLog(_T("OnChangeConnect %d, bConnect: %d, m_bConnectFlag: %d"), (int)wParam, m_bPowerFlag, m_bConnectFlag);

	if (!m_bPowerFlag)
		return TRUE;

	if (!m_bConnectFlag)
		return TRUE;

	return CheckConnect();
}

void CSdiSampleDlg::ConnectCam()
{
	if (!m_bPowerFlag)
		return;

	if (m_bConnectFlag)
	{
		DisconnectCam();
	}

	m_bConnectFlag = TRUE;

	CheckConnect();
}

void CSdiSampleDlg::DisconnectCam()
{
	m_bConnectFlag = FALSE;

	for(int i = 0; i < m_ctrDSCList.GetCount(); i++)
		m_ctrDSCList.ResetContent();
	//m_ctrDSCList.DeleteString(i);

	m_pArrSdiSDK.clear();

	SdiStrArray* pArList = new SdiStrArray;
	CSdiSdk::SdiGetDeviceHandleList(pArList);
	int nAll = pArList->GetSize();		


	CloseSDK();

	if(pArList)
	{
		nAll = pArList->GetSize();
		while(nAll--)
			pArList->RemoveAt(nAll);
		delete pArList;
	}
}

//------------------------------------------------------------------------------
//! @date		2013-12-06
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Check NX Camera Connection
//------------------------------------------------------------------------------
SdiResult CSdiSampleDlg::CheckConnect()
{

	for(int i = 0; i < m_ctrDSCList.GetCount(); i++)
		m_ctrDSCList.DeleteString(i);

	m_pArrSdiSDK.clear();


	char buff[500];
	SdiString strConnect;
	int nDSCCount = 0;
	
	
	//-- Get NX Camera list
	SdiStrArray* pArList = new SdiStrArray;
	CSdiSdk::SdiGetDeviceHandleList(pArList);
	int nAll = pArList->GetSize();		

	//-- Clean Existed Connection
	if(!nAll && m_pSdiSDK)
		CloseSDK();

	for(int i = 0; i < nAll; i++)
	//while(nAll--)	
	{
		
		//-- Connect PTP just One
		/*if(m_pSdiSDK)
			break;*/
		CSdiSdk* sdi;
		strConnect = pArList->GetAt(i);
		WriteLog(_T("PTP Get List : %s"), strConnect);
		//-- Create SDK
		//m_pSdiSDK = new CSdiSdk(0, strConnect, NULL);
		sdi = new CSdiSdk(0, strConnect, NULL,m_Clock.m_swFreq, m_Clock.m_swStart);
		sdi->SdiSetParents();
		m_pArrSdiSDK.push_back(sdi);
		if(!sdi)
			continue;

		m_strModel = sdi->SdiGetDeviceModel();
		m_nModel = (unsigned long)sdi->SdiGetModel();
		

		WriteLog(_T("Connect Model : [%s]"), sdi->SdiGetDeviceModel());
		//-- Set Receive Handle to MainDialog
		sdi->SdiSetReceiveHandler(this->GetSafeHwnd());

		//-- Set DSC from USB List (Samsung NX Camera (NX200, NX1000, NX2000, NX1)
		strConnect = pArList->GetAt(i);
		sprintf(buff, "%S", strConnect);
		
		//-----------------------------------------------------------
		//-- Init Session
		//-- No more need sending OPEN_SESSION Message
		//-----------------------------------------------------------
		
		if(SDI_ERR_OK != sdi->SdiInitSession(buff))
		{
			WriteLog(_T("[ERROR] OpenSession Fail"));
			break;
		}
		else
			WriteLog(_T("OpenSession Success"));
	
		m_pDlgProperty->m_strModel = m_strModel;
		nDSCCount++;

		
		CString strDSC;
		//strDSC.Format(_T("CAM %d"), nDSCCount);
		char buff[13];
		CString strUUID;
		int nRet = sdi->SdiGetDeviceID(buff);
		strUUID = CString(buff,13);
		strDSC.Format(_T("%s"),strUUID);
		m_ctrDSCList.AddString(strDSC);
	}	
	

	if(m_pArrSdiSDK.size() > 0)
	{
		m_ctrDSCList.SetCurSel(0);
		OnSelchangeDscList();
	}
	
	//-- Remove Array List
	if(pArList)
	{
		nAll = pArList->GetSize();
		while(nAll--)
			pArList->RemoveAt(nAll);
		delete pArList;
	}
	

	return SDI_ERR_OK;
}

unsigned WINAPI CSdiSampleDlg::RecordThreadGH5(LPVOID param)
{
	CSdiSampleDlg *pMain = (CSdiSampleDlg*)param;
	pMain->m_nDSCTime = 0;
	pMain->SetLiveview(FALSE);

	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		pMain->m_pSdiSDK->SdiInit();		
		pMain->OnGH5Initialize();
	}

	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		while(1)
		{
			if((BOOL)pMain->m_pSdiSDK->SdiGetInitPreRec() == TRUE)
			{
				break;
			}
		}
	}

	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		pMain->OnGetTick();
	}

	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		while(1)
		{
			if((unsigned int)pMain->m_pSdiSDK->SdiGetDSCTime() > 0)
			{

				break;
			}
		}
	}

	int nRecordTime = 0;
	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		if( i == 0)
			nRecordTime = pMain->m_pSdiSDK->SdiGetPCTime() + 35000;

		pMain->OnSetTick(nRecordTime);
	}

	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		pMain->m_pSdiSDK->m_nFrameCount = 0;
		pMain->OnGetRecordStatus();
		pMain->OnRecord();
	}

	return 0;
}

unsigned WINAPI CSdiSampleDlg::RecordThread(LPVOID param)
{
	CSdiSampleDlg *pMain = (CSdiSampleDlg*)param;
	pMain->m_nDSCTime = 0;
	pMain->SetLiveview(FALSE);

	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		pMain->m_pSdiSDK->SdiInit();		
		pMain->OnGetTick();
	}
	
	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		while(1)
		{
			if((unsigned int)pMain->m_pSdiSDK->SdiGetDSCTime() > 0)
			{
				
				break;
			}
		}
	}

	int nRecordTime = 0;
	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		if( i == 0)
			nRecordTime = pMain->m_pSdiSDK->SdiGetPCTime() + 35000;

		pMain->OnSetTick(nRecordTime);
	}
	
	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		pMain->m_pSdiSDK->m_nFrameCount = 0;
		pMain->OnGetRecordStatus();
		pMain->OnRecord();
	}
	
	return 0;
}

unsigned WINAPI CSdiSampleDlg::CaptureThread(LPVOID param)
{
	CSdiSampleDlg *pMain = (CSdiSampleDlg*)param;
	pMain->m_nDSCTime = 0;
	pMain->SetLiveview(FALSE);


	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		pMain->m_pSdiSDK->SdiInit();
		pMain->OnGetTick();
	}

	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		while(1)
		{
			if((unsigned int)pMain->m_pSdiSDK->SdiGetDSCTime() > 0)
			{

				break;
			}
		}
	}

	int nRecordTime = 0;
	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		if( i == 0)
			nRecordTime = pMain->m_pSdiSDK->SdiGetPCTime() + 35000;

		pMain->OnSetTick(nRecordTime);
	}

	for(int i = 0; i < pMain->m_pArrSdiSDK.size(); i++)
	{
		pMain->m_pSdiSDK = pMain->m_pArrSdiSDK.at(i);
		pMain->OnCapture();
	}

/*	CSdiSampleDlg *pMain = (CSdiSampleDlg*)param;
	pMain->m_nDSCTime = 0;
	pMain->OnGetTick();
	while(1)
	{
		if(pMain->m_nDSCTime > 0)
		{
			pMain->OnSetTick();
			break;
		}
	}

	pMain->OnCapture();*/
	return 0;
}

//------------------------------------------------------------------------------
//! @date		2013-12-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Message From Inside Dialog
//------------------------------------------------------------------------------
LRESULT CSdiSampleDlg::OnDialogMsg(WPARAM wParam, LPARAM lParam)
{
	SdiMessage* pMsg = NULL;
	pMsg = (SdiMessage*)lParam;
	switch(int(wParam))
	{
	case WM_SAMPLE_OP_FOCUS_LOCATE_SAVE:
		OnFocusLocateSave();
		break;
	case WM_SAMPLE_OP_EXPORT_LOG:
		OnExportLog();
		break;
	case WM_SAMPLE_OP_SENSOR_CLEANING:
		OnSensorCleaning();
		break;
	case WM_SAMPLE_OP_BANK_SAVE:
		OnBankSave();
		break;
	case WM_SAMPLE_OP_BANK_SAVE_ALL:
		OnBankSaveAll();
		break;
	case WM_SAMPLE_OP_DEVICE_REBOOT:
		OnReboot();
		break;
	case WM_SAMPLE_OP_FW_DOWNLOAD_N_UPDATE:
		OnFWUpdate();
		break;
	//-- 2014-07-21 joonho.kim
	case WM_SAMPLE_OP_SET_LIVEVIEW:
		OnLiveView(pMsg->nParam1);
		break;
	case WM_SAMPLE_OP_GET_RECORD_STATUS:
		OnGetRecordStatus();					//-- Get Record Status
		break;
	case WM_SAMPLE_OP_SET_ENLARGE:
		OnSetEnLarge(pMsg->nParam1);
		break;
	case WM_SAMPLE_OP_SET_RECORD_PAUSE:
		OnSetRecordPause(pMsg->nParam1);
		break;
	case WM_SAMPLE_OP_SET_RECORD_RESUME:
		OnSetRecordResume(pMsg->nParam1);
		break;
	case WM_SAMPLE_OP_CAPTURE:
		{
			HANDLE hSyncTime = NULL;
			hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, CaptureThread, this, 0, NULL);
			CloseHandle(hSyncTime);
		}
		break;
	case WM_SAMPLE_OP_GET_TICK:
		//for(int n = 0; n <10000; n++)
			OnGetTick();
		break;
	case WM_SAMPLE_OP_GET_UUID:
		OnGetUUID();
		break;
	case WM_SAMPLE_OP_SET_UUID:
		OnSetUUID();
		break;
	case WM_SAMPLE_OP_REC:
		{		
			CString strPath;
			CTime time = CTime::GetCurrentTime();
			m_strDate.Format(_T("%02d_%02d_%02d_%02d_%02d"), time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());
			strPath.Format(_T("%s\\%s"), m_pDlgProperty->m_strPath, m_strDate);
			CreateDirectory(strPath, NULL);

			m_nSyncCount = 0;
			m_nAvgTime = 0;

			if(m_nModel == SDI_MODEL_GH5)
			{
				HANDLE hSyncTime = NULL;
				hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, RecordThreadGH5, this, 0, NULL);
				CloseHandle(hSyncTime);

				//OnGH5Initialize();
			}
			else
			{
				HANDLE hSyncTime = NULL;
				hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, RecordThread, this, 0, NULL);
				CloseHandle(hSyncTime);
			}
		}
		break;	
	case WM_SAMPLE_OP_REC_STOP:
		OnGetRecordStatus();
		OnRecordStop();
		break;
	case WM_SAMPLE_OP_GET_FOCUSVALUE_ALL:
		OnGetFocusValueAll();
		break;
	case WM_SAMPLE_OP_GET_CUSTOM_PROPERTY_DESC:
		OnGetCustomProperty(pMsg->nParam1, pMsg->nParam2);
		break;
	case WM_SAMPLE_OP_SET_CUSTOM_PROPERTY_VALUE:
		OnSetCustomProperty(pMsg->nParam1, pMsg->nParam2);
		break;
	case WM_SAMPLE_OP_WRITE_LOG:
		{
			CString strLog;
			strLog.Format(_T("[GET PROPERTY] code = 0x%X, value = 0x%X"), pMsg->nParam1, pMsg->nParam2);
			WriteLog(strLog);
		}
		break;
	case WM_SAMPLE_OP_WRITE_LOG_VERTICAL_FRAME:
		{			
			CString strLog;
			strLog.Format(_T("[VERTICAL FRAME] %d"), pMsg->nParam1);
			WriteLog(strLog);
		}
		break;
	case WM_SAMPLE_OP_WRITE_LOG_VERTICAL_END:
		{			
			CString strLog;
			strLog.Format(_T("[VERTICAL FRAME] Encoding Finish"));
			WriteLog(strLog);
		}
		break;
	case WM_SAMPLE_OP_SET_PROPERTY_VALUE:
		OnSetProperty(pMsg->nParam1, pMsg->nParam2);
		break;
	case WM_SAMPLE_OP_SET_FOCUSTYPE:			//-- << < > <<
		OnSetFocus(pMsg->nParam1);
		break;
	case WM_SAMPLE_OP_SET_FOCUSVALUE:			//-- Set Focus Value
		OnSetFocusValue(pMsg->nParam1);
		break;
	case WM_SAMPLE_OP_GET_FOCUSVALUE:			//-- Get Focus Value
		OnGetFocusValue();
		break;
	case WM_SAMPLE_OP_CHANGE_CAMERA:
		OnChangeCamera();
		break;
	//CMiLRe NX2000 FirmWare Update CMD 추가
	case WM_SAMPLE_OP_FW_UPDATE:
		OnFWUpdate();
		break;
	case WM_SAMPLE_OP_HALF_SHUTTER:		
		OnHalfShutter(pMsg->nParam1);
		break;
	case WM_SAMPLE_OP_SET_FOCUS_FRAME:
		OnSetFocusFrame(pMsg->nParam1, pMsg->nParam2, pMsg->nParam3);
		break;
	case WM_SAMPLE_OP_GET_FOCUS_FRAME:
		OnGetFocusFrame();
		break;
	case WM_SAMPLE_OP_SET_POE:
		{
			if (pMsg->nParam1 == 0)
			{
				DisconnectCam();
				OFFPower();
			}
			else
				ONPower();
		}		
		break;
	case WM_SAMPLE_OP_SET_CONNECT:
		{
			if (pMsg->nParam1 == 0)
				ConnectCam();
			else
				DisconnectCam();				
		}
		break;
	default:
		break;
	}
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
	

	return 0;
}

//------------------------------------------------------------------------------
//! @date		2013-12-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Message From SDK
//------------------------------------------------------------------------------
LRESULT CSdiSampleDlg::OnSDIMsg(WPARAM wParam, LPARAM lParam)
{
	SdiMessage* pMsg = NULL;
	SdiMessage* pMessage = NULL;
	pMsg = (SdiMessage*)lParam;
	IDeviceInfo *pDevInfo;	
	switch(int(wParam))
	{
	case WM_SDI_EVENT_CAPTURE_DONE:
		{
			//TRACE("OnCaptureDone(%d, %d)", pMsg->nParam2, pMsg->nParam3);
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			WriteLog(_T("[%s] [EVENT] PTP_EC_CUSTOM_RequestObjectTransfer p1 = %d, p2 = %d, p3 = %d"), pSdi->m_strDSCID,pMsg->nParam2, pMsg->nParam3, (int)pMsg->pParam);
			if(m_nModel == SDI_MODEL_GH5)
			{
				for(int i = 0; i < m_pArrSdiSDK.size(); i++)
				{
					CSdiSdk* pArrSdi = m_pArrSdiSDK.at(i);
					if (pArrSdi->m_strDSCID != pSdi->m_strDSCID)
						continue;

					m_pSdiSDK = pArrSdi;
				}

				OnCaptureDone(0, (int)pMsg->pParam);
			}
			else
				OnCaptureDone(pMsg->nParam2, pMsg->nParam3);
		}
		break;
	case WM_SDI_EVENT_MOVIE_DONE:
		OnMovieDone(pMsg->nParam2, pMsg->nParam3);
		break;
	case WM_SDI_EVENT_DEVICE_PROPERTY_CHANGED:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;

			WriteLog(_T("[%s] [EVENT] PTP_EC_CUSTOM_DevicePropChanged param1 = 0x%X"), pSdi->m_strDSCID, pMsg->nParam2);
			pMessage = new SdiMessage();
			pMessage->message = WM_SDI_OP_GET_PROPERTY_DESC;
			pMessage->nParam1 = pMsg->nParam2;
			m_pSdiSDK->SdiAddMsg(pMessage);
			
			/*switch((int)pMsg->nParam2)
			{
			
			case SDI_DPC_FUNCTIONALMODE			:	// 0x5002	Production Mode	
			case SDI_DPC_IMAGESIZE				:	// 0x5003	Image size
			case SDI_DPC_COMPRESSIONSETTING		:	// 0x5004	Quality
			case SDI_DPC_WHITEBALANCE			:	// 0x5005	White Balance
			case SDI_DPC_F_NUMBER				:	// 0X5007	F-Number
			case SDI_DPC_FOCUSMODE				:	// 0X500A	AF Mode	
			case SDI_DPC_EXPOSUREINDEX			:	// 0x500F	ISO
			case SDI_DPC_MOVIESIZE				:	// 0x5012	Movie Size
			case SDI_DPC_SAMSUNG_SHUTTERSPEED	:	// 0xD815	ShutterSpeed
			case SDI_DPC_SAMSUNG_VIDEO_OUT		:	// 0xD909	Video out
				pMessage = new SdiMessage();
				pMessage->message = WM_SDI_OP_GET_PROPERTY_DESC;
				pMessage->nParam1 = pMsg->nParam2;
				m_pSdiSDK->SdiAddMsg(pMessage);
				break;
			default:
				break;
			}	*/
		}
		break;
	case WM_SDI_EVENT_ERR_MSG_GH5:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			WriteLog(_T("[%s] [EVENT] PTP_EC_CUSTOM_ErrorMsg p1 = %u, p2 = %u, p3 = %u"), pSdi->m_strDSCID, pMsg->nParam1, pMsg->nParam2, pMsg->nParam3);
			
#if 0
			// shut down program
			if (pMsg->nParam1 == 900 || pMsg->nParam1 == 901)
			{
				SetTimer(1, 35*1000, NULL);
			}
#endif

		}
		break;
	case WM_SDI_EVENT_ERR_MSG:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			WriteLog(_T("[%s] [EVENT] PTP_EC_CUSTOM_ErrorMsg p1 = %u, p2 = %u, p3 = %u"), pSdi->m_strDSCID, pMsg->nParam1, pMsg->nParam2, pMsg->nParam3);
		}
		break;
	case WM_SDI_EVENT_RECORD_STOP:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			WriteLog(_T("[%s] [EVENT] PTP_EC_CUSTOM_RecordingEnd"), pSdi->m_strDSCID);
			OnGetRecordStatus();
		}
		break;
	case WM_SDI_EVENT_EVENT_BUFFER_FULL:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			WriteLog(_T("[%s] [EVENT] PTP_EC_CUSTOM_EventBufferFull"),  pSdi->m_strDSCID);
		}
		break;
	case WM_SDI_EVENT_SENSOR_TIME:
		{
			int nStartTime;
			CString* pstr = (CString*)pMsg->pParam;
			if(m_nModel == SDI_MODEL_GH5)
			{
				WriteLog(_T("[%s] [EVENT] PTP_EC_CUSTOM_StartTime t1 = %u, t2 = %u"), *pstr , pMsg->nParam2, pMsg->nParam3);
				nStartTime = pMsg->nParam3;
			}
			else
			{
				WriteLog(_T("[%s] [EVENT] PTP_EC_CUSTOM_StartTime t1 = %u, t2 = %u"), *pstr , pMsg->nParam3, pMsg->nParam2);
				nStartTime = pMsg->nParam2;
			}

			for(int i = 0; i < m_pArrSdiSDK.size(); i++)
			{
				CSdiSdk* psdi = m_pArrSdiSDK.at(i);
				if(psdi->m_strDSCID.CompareNoCase(*pstr) == 0)
				{
					psdi->m_nStartTime = nStartTime;
					m_nSyncCount++;

					if(m_nModel == SDI_MODEL_GH5)
						m_nAvgTime+= (psdi->m_nSensorStart - psdi->m_nStartTime);
					else
						m_nAvgTime+= (psdi->m_nStartTime - psdi->m_nSensorStart);

					break;
				}
			}

			int nAvgTime = 0;
			int nTime = 2; //200us
			if(m_nModel == SDI_MODEL_GH5)
				nTime =  ConvertTime(nTime);

			if(m_nSyncCount == m_pArrSdiSDK.size())
			{
				nAvgTime = m_nAvgTime/m_pArrSdiSDK.size();
				int nGap;
				for(int i = 0; i < m_pArrSdiSDK.size(); i++)
				{
					CSdiSdk* psdi = m_pArrSdiSDK.at(i);

					if(m_nModel == SDI_MODEL_GH5)
						nGap = psdi->m_nSensorStart - psdi->m_nStartTime;
					else
						nGap = psdi->m_nStartTime - psdi->m_nSensorStart;
				
					if( abs(nAvgTime - nGap) > nTime )
						WriteLog(_T("[%s] [SYNC RESULT][NG]  SensorTime[%u], StartTime[%u], Gap[%d], Avg[%d], Avg Gap[%d]"),psdi->m_strDSCID, psdi->m_nSensorStart, psdi->m_nStartTime, nGap, nAvgTime, nAvgTime - nGap);
					else
						WriteLog(_T("[%s] [SYNC RESULT][PASS]  SensorTime[%u], StartTime[%u], Gap[%d], Avg[%d], Avg Gap[%d]"),psdi->m_strDSCID, psdi->m_nSensorStart, psdi->m_nStartTime, nGap, nAvgTime, nAvgTime - nGap);
				}
			}
			
			if(pstr)
			{
				delete pstr;
				pMsg->pParam = NULL;
			}
		}
		
		break;
	case WM_SDI_RESULT_LIVEVIEW_INFO:
		{
			SdiLiveviewInfo* pLiveviewInfo = (SdiLiveviewInfo*)pMsg->pParam;
			//SdiLiveviewInfo* pLiveviewInfo = (SdiLiveviewInfo*)pMsg->pParam;
			if(pLiveviewInfo)
			{
				TRACE("Liveview Size [Height:%d | Width:%d]\n",pLiveviewInfo->imageSizeHeight, pLiveviewInfo->imageSizeWidth);
				delete pLiveviewInfo;
			}
		}
		break;
	case WM_SDI_EVENT_LIVEVIEW_RECEIVED:
		//-- Get Liveview Buffer
		if(m_pDlgLiveview)
		{
			m_pDlgLiveview->SetBuffer((PVOID)pMsg->pParam);
			m_pDlgLiveview->GetLiveview()->Invalidate(FALSE);
		}
		break;
	case WM_SDI_RESULT_SET_PROPERTY_VALUE:
		{
			int nPropCode = (int)pMsg->pParam;
			if(nPropCode == PTP_OC_SAMSUNG_SetFocusPosition)
			{
				WriteLog(_T("[SET PROPERTY RESULT] code = 0x%X, value = 0x%X"),nPropCode);

				SdiMessage* _pMsg = new SdiMessage();
				_pMsg->message = WM_SAMPLE_OP_GET_CUSTOM_PROPERTY_DESC;
				_pMsg->nParam1 = PTP_OC_SAMSUNG_GetFocusPosition;
				::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)_pMsg->message, (LPARAM)_pMsg);
			}
		}
		break;
	case WM_SDI_RESULT_GET_PROPERTY_DESC:
		{
			pDevInfo = (IDeviceInfo *)pMsg->pParam;
			if(pDevInfo)
			{
				CSdiSdk* pSdi	= (CSdiSdk*)pMsg->pParent;
				
				WriteLog(_T("[%s] [GET PROPERTY] code = 0x%X, value = 0x%X (%d)"),pSdi->m_strDSCID, pDevInfo->GetPropCode(), pDevInfo->GetCurrentEnum(), pDevInfo->GetCurrentEnum());
				m_pDlgProperty->AddDevInfo(pDevInfo);
				OnUpdateCombo(pDevInfo->GetPropCode());

				int nPropCode = pDevInfo->GetPropCode();
				if (nPropCode == PTP_OC_SAMSUNG_GetFocusPosition)
				{
					int nValue = pDevInfo->GetCurrentEnum();
					m_pDlgLiveview->SetFocusValue(nValue);
				}
			}
		}		
		break;
	//-- 2013-12-05 Get Focus Info
	case WM_SDI_RESULT_FOCUS:	
		{
			SdiFocusPosition* pFocus = (SdiFocusPosition*)pMsg->pParam;
			if(!pFocus)
				break;
			m_pDlgLiveview->GetFocusValue(pFocus->min, pFocus->current, pFocus->max);

			TRACE("Focus Data %d", pFocus->current);
			/*if(m_nFocus)
			{
				if(m_nFocus != pFocus->current)
					OnSetFocusValue(m_nFocus);
			}*/

			if(pFocus)
				delete pFocus;
		}
		break;
	case WM_SDI_EVENT_SET_LIVE_STATUS:
		SetEVFLive(pMsg->nParam1);
		break;
	//-- Indicator
	case WM_SDI_EVENT_SAVE_DONE					:	TRACE("Save Done		: 0x%08X\n", pMsg->nParam2); break;
	case WM_SDI_RESULT_CAPTURE_IMAGE_RECEIVE	:	TRACE("Receive File		: 0x%08X\n", pMsg->nParam2); break;
	case WM_SDI_RESULT_CAPTURE_IMAGE_ONCE		:	TRACE("Capture Once		: 0x%08X\n", pMsg->nParam2); break;
	case WM_SDI_EVENT_AF_DETECT					:	TRACE("AF Detect		: 0x%08X\n", pMsg->nParam2); break;
	case WM_SDI_EVENT_LIVEVIEW_ERROR_OCCURED	:	
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			WriteLog(_T("[%s] [EVENT] Liveview Error"),pSdi->m_strDSCID);
		}
		break;
	case WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_5	:	TRACE("Recording Button [Press]\n");				 break;
	case WM_SDI_RESULT_CAPTURE_IMAGE_REQUIRE_6	:	TRACE("Recording Button [Release]\n");				 break;		
	case WM_SDI_EVENT_FILE_TRANSFER				:	
		{
			//TRACE("Time Test	%d %d \n",pMsg->nParam2,(int)pMsg->pParam);
			//TRACE("Time Test	%d %d\n",pMsg->nParam2,(int)pMsg->pParam);
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			WriteLog(_T("[%s] [EVENT] PTP_EC_CUSTOM_FileTransfer p1 = %u, p2 = %u, p3 = %u"),pSdi->m_strDSCID, pMsg->nParam2,pMsg->nParam3,(int)pMsg->pParam);
			OnFileTransfer(pMsg->nParam2, pMsg->nParam3, (int)pMsg->pParam, (void*)pMsg->pParent);

			/*HANDLE hSyncTime = NULL;
			hSyncTime = (HANDLE)  _beginthreadex(NULL, 0, CaptureThread, this, 0, NULL);
			CloseHandle(hSyncTime);*/
		}
		break;
	case WM_SDI_RESULT_GET_RECORD_STATUS:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			if(pMsg->nParam1 == 0)
			{
				WriteLog(_T("[%s] [__RESULT RECORD STATUS] PTP_OC_CUSTOM_GetRecordStatus Recording [ %d ]"), pSdi->m_strDSCID, pMsg->nParam1);
			}
			else
			{
				WriteLog(_T("[%s] [__RESULT RECORD STATUS] PTP_OC_CUSTOM_GetRecordStatus Live [ %d ]"), pSdi->m_strDSCID, pMsg->nParam1);
			}
		}
		break;
	case WM_SDI_RESULT_FILE_TRANSFER_2:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			int nResult = (int)pMsg->nParam1;

			if(nResult==SDI_ERR_OK)
				nResult = 0x2001;

			WriteLog(_T("[%s] [__RESULT FILE TRANSFER] PTP_OC_CUSTOM_FileTransfer Result: 0x%X "), pSdi->m_strDSCID, nResult);
		}
		break;
	case WM_SDI_RESULT_CHECK_EVENT:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			int nResult = (int)pMsg->nParam1;			
			int nEventExist = (int)pMsg->nParam2;	
			SdiUIntArray* pArList = (SdiUIntArray*)pMsg->pParam;
			int nParam2 = pArList->GetAt(0);
			int nParam3 = pArList->GetAt(1);

			if(nResult==SDI_ERR_OK)
				nResult = 0x2001;
			
			WriteLog(_T("[%s] [__RESULT CHECK EVENT] PTP_OC_CUSTOM_CheckEvent Result:[0x%X], Event:[%d], P2:[0x%X], P3:[0x%X]"),
				pSdi->m_strDSCID, nResult, nEventExist, nParam2, nParam3);
		}
		break;
	case WM_SDI_RESULT_GET_EVENT:
		{			 
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			int nResult = (int)pMsg->nParam1;
			SdiUIntArray* pArList = (SdiUIntArray*)pMsg->pParam;
			
			SdiUInt16 nEventId = pArList->GetAt(0);
			SdiUInt32 nPTPCode = pArList->GetAt(1);
			SdiUInt32 nParam1 = pArList->GetAt(2);
			SdiUInt32 nParam2 = pArList->GetAt(3);
			SdiUInt32 nParam3 = pArList->GetAt(4);
			

			if(nResult==SDI_ERR_OK)
				nResult = 0x2001;

			WriteLog(_T("[%s] [__RESULT GET EVENT] PTP_OC_CUSTOM_GetEvent Result: [0x%X] , EventID: [0x%X(%d)] , PTPCode: [0x%X(%d)], param1: [%u], param2: [%u], param3: [%u]"), 
				pSdi->m_strDSCID, nResult, nEventId, nEventId, nPTPCode, nPTPCode, nParam1, nParam2, nParam3);
		}
		break;
	case WM_SDI_RESULT_CAPTURE_IMAGE_RECORD:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			int nResult = (int)pMsg->nParam1;
			int nResultStatus = (int)pMsg->nParam2;

			CString strStatus;
			if (nResultStatus == 0)
				strStatus = _T("Recording");
			else
				strStatus = _T("Standby");

			if (nResult == SDI_ERR_OK)
			{
				nResult = 0x2001; //PTP_RC_OK
				WriteLog(_T("[%s] [__RESULT RECORD START] PTP_OC_CUSTOM_Record GetRecordStatus: [%s], Result: [0x%X]"), pSdi->m_strDSCID, strStatus, nResult);
			}
			else
				WriteLog(_T("[%s] [ERROR __RESULT RECORD START] PTP_OC_CUSTOM_Record GetRecordStatus: [%s], Result: [0x%X]"), pSdi->m_strDSCID, strStatus, nResult);
			
		}
		break;
	case WM_SDI_RESULT_CAPTURE_IMAGE_RECORD_STOP:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			int nResult = (int)pMsg->nParam1;
			int nResultStatus = (int)pMsg->nParam2;

			CString strStatus;
			if (nResultStatus == 0)
				strStatus = _T("Recording");
			else
				strStatus = _T("Standby");

			if (nResult == SDI_ERR_OK)
			{
				nResult = 0x2001; //PTP_RC_OK	
				WriteLog(_T("[%s] [__RESULT RECORD STOP] PTP_OC_CUSTOM_RecordStop Result: [0x%X], GetRecordStatus: [%s]"), pSdi->m_strDSCID, nResult, strStatus);
			}
			else
				WriteLog(_T("[%s] [ERROR __RESULT RECORD STOP] PTP_OC_CUSTOM_RecordStop Result: [0x%X], GetRecordStatus: [%s]"), pSdi->m_strDSCID, nResult, strStatus);
		}
		break;
	case WM_SDI_RESULT_GH5_INITIALIZE:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			int nResult = (int)pMsg->nParam1;

			if (nResult == SDI_ERR_OK)
			{
				nResult = 0x2001; //PTP_RC_OK
				WriteLog(_T("[%s] [__RESULT GH5 INIT] PTP_OC_CUSTOM_LumixInitialize result value = 0x%X"), pSdi->m_strDSCID, nResult);
			}
			else
			{
				pSdi->SdiSetInitPreRec(FALSE);
				OnRecordStop();
				WriteLog(_T("[%s] [ERROR __RESULT GH5 INIT] PTP_OC_CUSTOM_LumixInitialize result value = 0x%X"), pSdi->m_strDSCID, nResult);
				break;
			}

			if (nResult == 0x2001)
			{
				pSdi->SdiSetInitPreRec(TRUE);
//  				SdiMessage* pMsg = new SdiMessage();
//  				pMsg->message = WM_SAMPLE_OP_REC_GH5;
//  				::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
			}
			
		}
		break;
	case WM_SDI_RESULT_GET_TICK_2				:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			int nResult = (int)pMsg->nParam1;

			if (nResult == SDI_ERR_OK)
			{
				nResult = 0x2001; //PTP_RC_OK
				WriteLog(_T("[%s] [__RESULT GET TICK_2] PTP_OC_CUSTOM_GetTick result value = 0x%X"), pSdi->m_strDSCID, nResult);
			}
			else
				WriteLog(_T("[%s] [ERROR __RESULT GET TICK_2] PTP_OC_CUSTOM_GetTick result value = 0x%X"), pSdi->m_strDSCID, nResult);

			
		}
		break;
	case WM_SDI_RESULT_GET_TICK					:	
		{
			CString* pDSCID = (CString*)pMsg->pDest;
			CString strID = pDSCID->GetString();
			//WriteLog(_T("[GET TICK] pc = %d us, dsc = %d, check time = %d us, check count = %d"), pMsg->nParam1 *100, pMsg->nParam2, pMsg->nParam3*100, (int)pMsg->pParam);
			WriteLog(_T("[%s] [__RESULT GET TICK] pc = %u us, dsc = %u, Gap = %u us, check count = %d"), strID,pMsg->nParam1 *100, pMsg->nParam2, pMsg->nParam3*100, (int)pMsg->pParam);
			m_nPCTime = pMsg->nParam1;
			m_nDSCTime = pMsg->nParam2;
		}		
		break;
	case WM_SDI_RESULT_SET_TICK					:	
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			int nResult = (int)pMsg->nParam1;
			UINT nResponseTick = (UINT)pMsg->nParam2;
			UINT nResponseCode = (UINT)pMsg->nParam3;

			if (nResult == SDI_ERR_OK)
			{
				nResult = 0x2001; //PTP_RC_OK
				WriteLog(_T("[%s] [__RESULT SET TICK] result code = 0x%X ,  Response Tick = %u,  Response code = 0x%X"), pSdi->m_strDSCID, nResult, nResponseTick, nResponseCode);
			}			
			else
				WriteLog(_T("[%s] [ERROR __RESULT SET TICK] result code = 0x%X ,  Response Tick = %u,  Response code = 0x%X"), pSdi->m_strDSCID, nResult, nResponseTick, nResponseCode);
		}
		break;
	case WM_SDI_RESULT_SET_FOCUS_FRAME:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			int nResult = (int)pMsg->nParam1;

			if (nResult == SDI_ERR_OK)
			{
				nResult = 0x2001; //PTP_RC_OK
				WriteLog(_T("[%s] [__RESULT SET FOCUS FRAME] PTP_OC_CUSTOM_SetFocusFrame result code = 0x%X "), pSdi->m_strDSCID, nResult);
			}
			else
				WriteLog(_T("[%s] [ERROR __RESULT SET FOCUS FRAME] PTP_OC_CUSTOM_SetFocusFrame result code = 0x%X "), pSdi->m_strDSCID, nResult);			

			
		}
		break;
	case WM_SDI_RESULT_GET_FOCUS_FRAME:
		{
			CSdiSdk* pSdi = (CSdiSdk*)pMsg->pParent;
			int nResult = (int)pMsg->nParam1;

			SdiUIntArray* pArList = (SdiUIntArray*)pMsg->pParam;

			UINT32 nX = pArList->GetAt(0);
			UINT32 nY = pArList->GetAt(1);
			UINT32 nW = pArList->GetAt(2);
			UINT32 nH = pArList->GetAt(3);
			UINT32 nMag = pArList->GetAt(4);

			if (nResult == SDI_ERR_OK)
			{
				nResult = 0x2001; //PTP_RC_OK
				WriteLog(_T("[%s] [__RESULT GET FOCUS FRAME] PTP_OC_CUSTOM_GetFocusFrame result=0x%X,X:%d,Y:%d,W:%d,H:%d,Mag:%d"), pSdi->m_strDSCID, nResult, nX, nY, nW, nH, nMag);
			}
			else
				WriteLog(_T("[%s] [ERROR __RESULT GET FOCUS FRAME] PTP_OC_CUSTOM_GetFocusFrame result=0x%X,X:%d,Y:%d,W:%d,H:%d,Mag:%d"), pSdi->m_strDSCID, nResult, nX, nY, nW, nH, nMag);				

			

			if (pArList != NULL)
			{
				pArList->RemoveAll();
				delete pArList;
			}
		}
		break;
	default:
		// Event From SDK
		//TRACE("[%x] Event : 0x%08X\n", (int)wParam, pMsg->nParam2);
		break;
	}

	//-- Memory Release
	if(pMsg)
	{
		delete pMsg;
		pMsg = NULL;
	}
	return 0;
}

void CSdiSampleDlg::OnGetUUID(void)
{
	if (!m_pSdiSDK)
		return;

	char buff[13];
	CString strUUID;
	int nRet = m_pSdiSDK->SdiGetDeviceID(buff);
	strUUID = CString(buff,13);
	
	m_pDlgLiveview->SetUUID(strUUID);
	WriteLog(_T("[GET UUID] %s"), strUUID);
}

void CSdiSampleDlg::OnSetUUID(void)
{
	if (!m_pSdiSDK)
		return;

	char buff[500];
	CString strUUID;
	m_pDlgLiveview->GetUUID(strUUID);
	if(!strUUID.IsEmpty())
	{
		int nRet = m_pSdiSDK->SdiSetDeviceID(strUUID);
		WriteLog(_T("[SET UUID] %s"), strUUID);
	}
	else
		WriteLog(_T("[ERROR][SET UUID] Empty!"));
		
	
}

//------------------------------------------------------------------------------
//! @date		2013-12-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Take Picture Function
//! @return		
//! @revision	2.1.0.0
//------------------------------------------------------------------------------
void CSdiSampleDlg::OnCapture(void)
{
	//SetTimer(0,8000,NULL);
	//return;

	SdiMessage *pMsg = NULL;

	if(m_pSdiSDK!=NULL)
	{		
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_ONCE;
		pMsg->nParam1 = m_nModel;
		m_pSdiSDK->SdiAddMsg(pMsg);				
	}
}

void CSdiSampleDlg::OnGH5Initialize(void)
{
	SdiMessage *pMsg = NULL;

	if(m_pSdiSDK!=NULL)
	{		
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_GH5_INITIALIZE;
		m_pSdiSDK->SdiAddMsg(pMsg);	
	}
}

void CSdiSampleDlg::OnGetTick(void)
{
	SdiMessage *pMsg = NULL;

	if(m_pSdiSDK!=NULL)
	{		
		//-- MOVIE REC BUTTON PRESS
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_GET_TICK;
		m_pSdiSDK->SdiAddMsg(pMsg);	
	}
}

int CSdiSampleDlg::ConvertTime(int nTime)
{
	//int nRet = (double)nTime / (double)0.0948;//9.48us
	int nRet = (double)nTime / (256.0 / 27.0 / 100.0);		//9.481481481481... us
	return nRet;
}

void CSdiSampleDlg::OnSetTick(int nTime)
{
	SdiMessage *pMsg = NULL;

	if(m_pSdiSDK!=NULL)
	{	
		
		unsigned int nGap = nTime - m_pSdiSDK->SdiGetPCTime();
		//-- MOVIE REC BUTTON PRESS
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_SET_TICK;
		//pMsg->nParam1 = m_nDSCTime + 35000;

		if(m_nModel == SDI_MODEL_GH5)
		{
			pMsg->nParam1 = m_pSdiSDK->SdiGetDSCTime() - ConvertTime(nGap);
			pMsg->nParam2 = ConvertTime(10000);

			//WriteLog(_T("[SET TICK] On Time = %u, Start Frame Time = %u"), pMsg->nParam1 + pMsg->nParam2, pMsg->nParam1);
		}
		else
		{
			pMsg->nParam1 = m_pSdiSDK->SdiGetDSCTime() + nGap;
			pMsg->nParam2 = 10000;

			//WriteLog(_T("[SET TICK] On Time = %u, Start Frame Time = %u"), pMsg->nParam1 - pMsg->nParam2, pMsg->nParam1);
		}
		char buff[13];
		CString strUUID;
		int nRet = m_pSdiSDK->SdiGetDeviceID(buff);
		strUUID = CString(buff,13);

		WriteLog(_T("[%s] [SET TICK] param1 = %u, param2 = %u"), strUUID, pMsg->nParam1, pMsg->nParam2);

		for(int i = 0; i < m_pArrSdiSDK.size(); i++)
		{
			CSdiSdk* psdi = m_pArrSdiSDK.at(i);
			if(psdi->m_strDSCID.CompareNoCase(strUUID) == 0)
			{
				psdi->m_nSensorStart = pMsg->nParam1;
				break;
			}
		}
		

		m_pSdiSDK->SdiAddMsg(pMsg);	
	}
}

//------------------------------------------------------------------------------
//! @date		2013-12-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Recording Function
//! @return		
//! @revision	2.1.0.0
//------------------------------------------------------------------------------
void CSdiSampleDlg::OnRecord(void)
{
	m_nMovieCount = 0;

	SdiMessage *pMsg = NULL;

	CString strDSC;
	char strDSCID[13];
	m_pSdiSDK->SdiGetDeviceID(strDSCID);
	strDSC = CString(strDSCID,13);
	WriteLog(_T("[%s] [RECORD START]"), strDSC);


	if(m_pSdiSDK!=NULL)
	{		
		if(m_nModel == SDI_MODEL_GH5)
		{
			pMsg = new SdiMessage();
			pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_RECORD;
			m_pSdiSDK->SdiAddMsg(pMsg);	
		}
		else
		{
			//-- MOVIE REC BUTTON PRESS
			pMsg = new SdiMessage();
			pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_5;
			m_pSdiSDK->SdiAddMsg(pMsg);	
			Sleep(100);
			pMsg = new SdiMessage();
			pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_6;
			m_pSdiSDK->SdiAddMsg(pMsg);	
		}
	}
}

void CSdiSampleDlg::OnRecordStop(void)
{
	CString strDSC;
	char strDSCID[13];
	m_pSdiSDK->SdiGetDeviceID(strDSCID);
	strDSC = CString(strDSCID,13);
	WriteLog(_T("[%s] [RECORD STOP]"), strDSC);

	m_nMovieCount = 0;

	SdiMessage *pMsg = NULL;

	for(int i = 0; i < m_pArrSdiSDK.size(); i++)
	{
		m_pSdiSDK = m_pArrSdiSDK.at(i);
		if(m_pSdiSDK!=NULL)
		{		
			if(m_nModel == SDI_MODEL_GH5)
			{
				pMsg = new SdiMessage();
				pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_RECORD_STOP;
				m_pSdiSDK->SdiAddMsg(pMsg);	
			}
			else
			{
				//-- MOVIE REC BUTTON PRESS
				pMsg = new SdiMessage();
				pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_5;
				m_pSdiSDK->SdiAddMsg(pMsg);	
				Sleep(100);
				pMsg = new SdiMessage();
				pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_6;
				m_pSdiSDK->SdiAddMsg(pMsg);	
			}
		}
	}
}

//------------------------------------------------------------------------------
//! @date		2014-07-21
//! @owner		joonho.kim (joonho@esmlab.com)
//! @brief		Recording Status Function
//! @return		
//! @revision	1.0.0.0
//------------------------------------------------------------------------------
void CSdiSampleDlg::OnGetRecordStatus(void)
{
	SdiMessage *pMsg = NULL;

	if(m_pSdiSDK!=NULL)
	{		
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_GET_RECORD_STATUS;
		m_pSdiSDK->SdiAddMsg(pMsg);		
	}
}

//------------------------------------------------------------------------------
//! @date		2014-07-21
//! @owner		joonho.kim (joonho@esmlab.com)
//! @brief		Set EnLarge
//! @return		
//! @revision	1.0.0.0
//------------------------------------------------------------------------------
void CSdiSampleDlg::OnSetEnLarge(int nLarge)
{
	if (m_pSdiSDK == NULL)
		return;

	char strDSCID[13];
	CString	strDSC;
	m_pSdiSDK->SdiGetDeviceID(strDSCID);
	strDSC = CString(strDSCID,13);
	WriteLog(_T("[%s] [EnLarge] Value: [%d]"), strDSC, nLarge);

	SdiMessage *pMsg = NULL;

	if(m_pSdiSDK!=NULL)
	{		
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_SET_ENLARGE;
		pMsg->nParam1 = nLarge;
		m_pSdiSDK->SdiAddMsg(pMsg);		
	}
}

void CSdiSampleDlg::OnLiveView(int nCommand)
{
	SdiMessage *pMsg = NULL;

	if(m_pSdiSDK!=NULL)
	{		
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_SET_LIVEVIEW;
		pMsg->nParam1 = nCommand;
		m_pSdiSDK->SdiAddMsg(pMsg);		
	}	
}

//------------------------------------------------------------------------------
//! @date		2014-07-21
//! @owner		joonho.kim (joonho@esmlab.com)
//! @brief		Set RecordPause
//! @return		
//! @revision	1.0.0.0
//------------------------------------------------------------------------------
void CSdiSampleDlg::OnSetRecordPause(int nPause)
{
	SdiMessage *pMsg = NULL;

	if(m_pSdiSDK!=NULL)
	{		
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_SET_RECORD_PAUSE;
		pMsg->nParam1 = nPause;
		m_pSdiSDK->SdiAddMsg(pMsg);		
	}
}

//------------------------------------------------------------------------------
//! @date		2014-07-21
//! @owner		joonho.kim (joonho@esmlab.com)
//! @brief		Set RecordResume
//! @return		
//! @revision	1.0.0.0
//------------------------------------------------------------------------------
void CSdiSampleDlg::OnSetRecordResume(int nSkip)
{
	SdiMessage *pMsg = NULL;

	if(m_pSdiSDK!=NULL)
	{		
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_SET_RECORD_RESUME;
		pMsg->nParam1 = nSkip;
		m_pSdiSDK->SdiAddMsg(pMsg);		
	}
}


void CSdiSampleDlg::OnSetFrameRate()
{
	SdiMessage *pMsg = NULL;
	int nRate = 5;
	CString strValue;
	m_pDlgProperty->m_ctrlMoviesize.GetWindowText(strValue);

	if(m_nModel == SDI_MODEL_GH5)
	{
		if(strValue.Find(_T("4K/60p")) != -1)
			nRate = MOVIE_FPS_UHD_60P;
		else if(strValue.Find(_T("4K/30p")) != -1)
			nRate = MOVIE_FPS_UHD_30P;
		else if(strValue.Find(_T("FHD/60p")) != -1)
			nRate = MOVIE_FPS_FHD_60P;
		else if(strValue.Find(_T("FHD/30p")) != -1)
			nRate = MOVIE_FPS_FHD_30P;
		else if(strValue.Find(_T("4K/50p")) != -1)
			nRate = MOVIE_FPS_UHD_50P;
		else if(strValue.Find(_T("4K/25p")) != -1)
			nRate = MOVIE_FPS_UHD_25P;
		else if(strValue.Find(_T("FHD/50p")) != -1)
			nRate = MOVIE_FPS_FHD_50P;
		else if(strValue.Find(_T("FHD/25p")) != -1)
			nRate = MOVIE_FPS_FHD_25P;
	}
	else //NX Series
	{
		if(strValue.Find(_T("UHD 25P")) != -1)
			nRate = MOVIE_FPS_UHD_25P;
		else if(strValue.Find(_T("FHD 50P")) != -1)
			nRate = MOVIE_FPS_FHD_50P;
		else if(strValue.Find(_T("FHD 30P")) != -1)
			nRate = MOVIE_FPS_FHD_30P;
		else if(strValue.Find(_T("UHD 30P")) != -1)
			nRate = MOVIE_FPS_UHD_30P;
		else if(strValue.Find(_T("FHD 25P")) != -1)
			nRate = MOVIE_FPS_FHD_25P;
	}
	
	

	pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_SET_FRAMERATE;
	pMsg->nParam1 = nRate;
	m_pSdiSDK->SdiAddMsg(pMsg);	
}
//------------------------------------------------------------------------------
//! @date		2014-07-21
//! @owner		joonho.kim (joonho@esmlab.com)
//! @brief		Recoding File Transfer
//! @return		
//! @revision	1.0.0.0
//------------------------------------------------------------------------------
void CSdiSampleDlg::OnFileTransfer(int nfilenum, int nmdat, int nOffset, void* pDest)
{
	SdiMessage *pMsg = NULL;
	m_pSdiSDK = (CSdiSdk*)pDest;
	if(m_pSdiSDK!=NULL)
	{	
		CString strDSC;
		/*char strDSCID[13];
		
		m_pSdiSDK->SdiGetDeviceID(strDSCID);
		strDSC = CString(strDSCID,13);*/
		
		strDSC = m_pSdiSDK->m_strDSCID;
		TCHAR* strPath = NULL;
		strPath = new TCHAR[MAX_PATH];
		wsprintf(strPath, _T("%s\\%s\\%s_%d.mp4"), m_pDlgProperty->m_strPath, m_strDate, strDSC.Right(6), m_pSdiSDK->m_nFrameCount);
		m_pSdiSDK->m_nFrameCount++;
		WriteLog(_T("[%s] [SAVE FILE] %s"),m_pSdiSDK->m_strDSCID, strPath);
		/*if(m_pSdiSDK->m_nFrameCount > 11)
			Sleep(5000);*/

		//17.09.08 joonho.kim SetFrameRate
		OnSetFrameRate();
		
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_FILE_TRANSFER;
		pMsg->nParam1 = nfilenum;
		pMsg->nParam2 = nmdat;
		pMsg->nParam3 = nOffset;
		pMsg->pParam = (LPARAM)strPath;
		m_pSdiSDK->SdiAddMsg(pMsg);	
		CString strLog;
		strLog.Format(_T("pMsg->nParam1[ %d ], pMsg->nParam2[ %d ], pMsg->nParam3[ %d ]\n"), nfilenum, nmdat, nOffset);
		TRACE(strLog);
		if( nmdat > 0)
			m_nMovieCount++;
	}
}

//------------------------------------------------------------------------------
//! @date		2013-12-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Save Picture File
//! @return		
//! @revision	2.1.0.0
//------------------------------------------------------------------------------
void CSdiSampleDlg::OnCaptureDone(int nFileNum, int nClose)
{
	CString strFile;
	CString strName;
	//-- Set Save File Name
	CTime time  = CTime::GetCurrentTime();	
	/*strName.Format(_T("%04d%02d%02d_%02d%02d%02d"),	time.GetYear(),
													time.GetMonth(),
													time.GetDay(),
													time.GetHour(),
													time.GetMinute(),
													time.GetSecond());*/

	strName.Format(_T("%s_%02d%02d%02d"), m_pSdiSDK->m_strDSCID,	time.GetHour(),	time.GetMinute(),time.GetSecond());

	//-- Add File Extension name on SdiMgr
	//strFile.Format(_T("%s\\%s.jpg"),m_pDlgProperty->m_strPath, strName);
	strFile.Format(_T("%s\\%s"),m_pDlgProperty->m_strPath, strName);

	//-- Save Capture Image
	TCHAR* cSaveFile = new TCHAR[strFile.GetLength()+1];
	_tcscpy(cSaveFile, strFile);

	if(m_pSdiSDK!=NULL)
	{
		SdiMessage* pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_RECEIVE;		
		pMsg->pParam = (LPARAM)cSaveFile;	
		pMsg->nParam1 = TRUE;				//-- Set Full File Name
		//pMsg->nParam1 = FALSE;			//-- Set Just Folder Name

		//-- 2014-9-19 joonho.kim
		pMsg->nParam2 = nFileNum;
		pMsg->nParam3 = nClose;

		m_pSdiSDK->SdiAddMsg(pMsg);
	}	
}

//------------------------------------------------------------------------------
//! @date		2013-12-05
//! @owner		Hongsu Jung (hongsu@esmlab.com)
//! @brief		Save Movie File
//! @return		
//! @revision	2.1.0.0
//------------------------------------------------------------------------------
/*void CSdiSampleDlg::OnMovieDone(void)
{
	CString strFile;
	CString strName;
	//-- Set Save File Name
	CTime time  = CTime::GetCurrentTime();	
	strName.Format(_T("%04d%02d%02d_%02d%02d%02d"),	time.GetYear(),
		time.GetMonth(),
		time.GetDay(),
		time.GetHour(),
		time.GetMinute(),
		time.GetSecond());
	//-- Add File Extension name on SdiMgr
	//strFile.Format(_T("%s\\%s.mp4"),m_pDlgProperty->m_strPath, strName);
	strFile.Format(_T("%s\\%s"),m_pDlgProperty->m_strPath, strName);

	//-- Save Capture Image
	TCHAR* cPath = new TCHAR[strFile.GetLength()+1];
	_tcscpy(cPath, strFile);

	if(m_pSdiSDK!=NULL)
	{
		SdiMessage* pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_MOVIE_IMAGE_RECEIVE;
		pMsg->pParam = (LPARAM)cPath;
		pMsg->nParam1 = TRUE;				//-- Set Full File Name
		//pMsg->nParam1 = FALSE;			//-- Set Just Folder Name
		m_pSdiSDK->SdiAddMsg(pMsg);
	}	
}*/

void CSdiSampleDlg::OnMovieDone(int nfilenum, int nClose)
{
	CString strFile;
	CString strName;
	//-- Set Save File Name
	CTime time  = CTime::GetCurrentTime();	
	strName.Format(_T("%04d%02d%02d_%02d%02d%02d"),	time.GetYear(),
		time.GetMonth(),
		time.GetDay(),
		time.GetHour(),
		time.GetMinute(),
		time.GetSecond());
	//-- Add File Extension name on SdiMgr
	//strFile.Format(_T("%s\\%s.mp4"),m_pDlgProperty->m_strPath, strName);
	strFile.Format(_T("%s\\%s"),m_pDlgProperty->m_strPath, strName);

	//-- Save Capture Image
	TCHAR* cPath = new TCHAR[strFile.GetLength()+1];
	_tcscpy(cPath, strFile);

	SdiMessage *pMsg = NULL;

	if(m_pSdiSDK!=NULL)
	{		
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_MOVIE_IMAGE_RECEIVE;
		pMsg->nParam1 = nfilenum;
		pMsg->nParam2 = nClose;
		pMsg->pParam = (LPARAM)cPath;
		m_pSdiSDK->SdiAddMsg(pMsg);		
	}
}

void CSdiSampleDlg::GetPropertyAll()
{
	//-- 2013-02-05 hongsu.jung
	//-- Load Model Info
	if(m_strModel.CompareNoCase(_T("GH5")) == 0)
	{
		GetPTPProperty(SDI_DPC_FUNCTIONALMODE		); // 0x5002	Production Mode	
		GetPTPProperty(SDI_DPC_IMAGESIZE			); // 0x5003	Image size
		GetPTPProperty(SDI_DPC_COMPRESSIONSETTING	); // 0x5004	Quality
		GetPTPProperty(SDI_DPC_WHITEBALANCE			); // 0x5005	White Balance
		GetPTPProperty(SDI_DPC_F_NUMBER				); // 0X5007	F-Number
		GetPTPProperty(SDI_DPC_FOCUSMODE			); // 0X500A	AF Mode
		GetPTPProperty(SDI_DPC_EXPOSUREINDEX		); // 0x500F	ISO
		GetPTPProperty(SDI_DPC_MOVIESIZE			); // 0xd84b	Movie Size
		GetPTPProperty(SDI_DPC_SAMSUNG_SHUTTERSPEED	); // 0xd815	ShutterSpeed
		GetPTPProperty(PTP_OC_SAMSUNG_GetFocusPosition	); // 0x9009	Focus Value
		GetPTPProperty(SDI_DPC_LENS_POWER			); // 0xd981	Lens Power control				
		GetPTPProperty(SDI_DPC_LCD					); // 0xE504	LCD	
		GetPTPProperty(SDI_DPC_PEAKING				); // 0xE505	Peaking
	}
	else
	{
		GetPTPProperty(SDI_DPC_FUNCTIONALMODE		); // 0x5002	Production Mode	
		GetPTPProperty(SDI_DPC_IMAGESIZE			); // 0x5003	Image size
		GetPTPProperty(SDI_DPC_COMPRESSIONSETTING	); // 0x5004	Quality
		GetPTPProperty(SDI_DPC_WHITEBALANCE			); // 0x5005	White Balance
		GetPTPProperty(SDI_DPC_F_NUMBER				); // 0X5007	F-Number
		GetPTPProperty(SDI_DPC_FOCUSMODE			); // 0X500A	AF Mode
		GetPTPProperty(SDI_DPC_EXPOSUREINDEX		); // 0x500F	ISO
		GetPTPProperty(SDI_DPC_MOVIESIZE			); // 0xd84b	Movie Size
		GetPTPProperty(SDI_DPC_SAMSUNG_SHUTTERSPEED	); // 0xD815	ShutterSpeed
		GetPTPProperty(SDI_DPC_SAMSUNG_VIDEO_OUT	); // 0xd909	Video Out
	}	
}

void CSdiSampleDlg::GetPTPProperty(int propCode)
{
	SdiMessage* pMsg = new SdiMessage;
	pMsg->message = WM_SDI_OP_GET_PROPERTY_DESC;
	pMsg->nParam1 = propCode;	
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::GetLiveviewInfo()
{
	SdiMessage* pMsg = new SdiMessage;
	pMsg->message = WM_SDI_OP_LIVEVIEW_INFO;
	m_pSdiSDK->SdiAddMsg(pMsg);
}


// Get Property Function
void CSdiSampleDlg::OnSetProperty(int propCode, int index)
{
	int nType;
	IDeviceInfo *pDevInfo = (m_pDlgProperty->GetDevMgr())->GetDevInfo(propCode);

	if(pDevInfo== NULL) 
		return;
	nType = pDevInfo->GetType();

	switch(nType)
	{
	case SDI_TYPE_INT_8:
	case SDI_TYPE_UINT_8:
	case SDI_TYPE_UINT_16:
	case SDI_TYPE_UINT_32:
		SetDevPropertyEnum(pDevInfo, index);
		break;
	case SDI_TYPE_STRING:
		SetDevPropertyStr(pDevInfo, index);
		break;
	}
	
}

void CSdiSampleDlg::OnGetCustomProperty(int propCode, int nAllFlag )
{
	SdiMessage *pMsg = new SdiMessage();
	pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_GET_PROPERTY_DESC;
	pMsg->nParam1 = propCode;
	pMsg->nParam2 = nAllFlag;
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnSetCustomProperty(int propCode, int nValue)
{
	WriteLog(_T("[SET PROPERTY] code = 0x%X , value = 0x%X"), propCode, nValue);
	

	if(m_strModel.CompareNoCase(_T("GH5")) == 0)
	{
		SdiMessage *pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
		pMsg->nParam1 = propCode;
		pMsg->nParam2 = 0x0004; //PTP_VALUE_UINT_32
		pMsg->pParam = (LPARAM) nValue;
		m_pSdiSDK->SdiAddMsg(pMsg);
	}
	else
	{
		int nType;
		IDeviceInfo *pDevInfo = (m_pDlgProperty->GetDevMgr())->GetDevInfo(propCode);
		if(pDevInfo== NULL) 
			return;
		nType = pDevInfo->GetType();

		SdiMessage *pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
		pMsg->nParam1 = propCode;
		pMsg->nParam2 = nType;
		pMsg->pParam = (LPARAM) nValue;
		m_pSdiSDK->SdiAddMsg(pMsg);

		
	}
	
}

void CSdiSampleDlg::OnUpdateCombo(int nPTPCode)
{
	m_pDlgProperty->UpdateCombo(nPTPCode);
}

void CSdiSampleDlg::SetDevPropertyEnum(IDeviceInfo* pDevInfo, int nPropIndex)
{
	int propCode = pDevInfo->GetPropCode();
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = propCode;
	pMsg->nParam2 = pDevInfo->GetType();
	pMsg->pParam = (LPARAM) pDevInfo->GetPropValueListEnum()->GetAt(nPropIndex);
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::SetDevPropertyStr(IDeviceInfo* pDevInfo, int nPropIndex)
{
	int propCode = pDevInfo->GetPropCode();
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = propCode;
	pMsg->nParam2 = pDevInfo->GetType();
	pMsg->pParam = (LPARAM) (LPCTSTR) pDevInfo->GetPropValueListStr()->GetAt(nPropIndex);
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::SetLiveview(BOOL bLiveview)
{
	if(m_pSdiSDK!=NULL)
		m_pSdiSDK->SdiLiveviewSet(bLiveview);
}

void CSdiSampleDlg::OnSetFocus(int nFocusType)
{
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_SET_FOCUS;
	pMsg->nParam1 = nFocusType;    
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnSetFocusValue(int nFocusValue)
{
	m_nFocus = nFocusValue;
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_SET_FOCUS_VALUE;
	pMsg->nParam1 = nFocusValue;    
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnGetFocusValue()
{
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_GET_FOCUS_VALUE;
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnGetFocusValueAll()
{
	for (int i = 0; i < m_pArrSdiSDK.size() ; i++)
	{
		CSdiSdk* pSdiSdk = m_pArrSdiSDK.at(i);
		if (!pSdiSdk)
			continue;

		SdiMessage *pMsg = new SdiMessage();
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_GET_PROPERTY_DESC;
		pMsg->nParam1 = 0x9009;
		pSdiSdk->SdiAddMsg(pMsg);
	}
}

void CSdiSampleDlg::OnChangeCamera()
{

}

void CSdiSampleDlg::OnHalfShutter(int nRelease)
{
	if (nRelease == 0) 
		WriteLog(_T("[HALF SHUTTER RELEASE]"));
	else if (nRelease == 1) 
		WriteLog(_T("[HALF SHUTTER PRESS]"));
	else if (nRelease == 2) 
		WriteLog(_T("[ONE SHOT AF]"));

	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_HALF_SHUTTER;
	pMsg->nParam1 = nRelease;
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnSetFocusFrame( int nX, int nY, int nMagnification )
{
	WriteLog(_T("[SET FOCUS FRAME] X: %d  ,  Y: %d  ,  Magnification: %d"), nX, nY, nMagnification);

	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_SET_FOCUS_FRAME;
	pMsg->nParam1 = nX;
	pMsg->nParam2 = nY;
	pMsg->nParam3 = nMagnification;
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnGetFocusFrame()
{
	WriteLog(_T("[GET FOCUS FRAME]"));
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_GET_FOCUS_FRAME;
	m_pSdiSDK->SdiAddMsg(pMsg);
}

//CMiLRe NX2000 FirmWare Update CMD 추가
void CSdiSampleDlg::OnFWUpdate()
{
	WriteLog(_T("[FW UPDATE]"));
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_FW_UPDATE;
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnReboot()
{
	WriteLog(_T("[REBOOT]"));
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_DEVICE_REBOOT_GH5;
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnSensorCleaning()
{
	WriteLog(_T("[SENSOR CLEANING]"));
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_SENSORCLEANING;
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnBankSave()
{
	if (m_pSdiSDK == NULL)
		return;

	WriteLog(_T("[BANK SAVE]"));
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
	pMsg->nParam1 = 1;
	pMsg->pParam = (int)1;		//0 : Disable, 1: Use
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnBankSaveAll()
{
	WriteLog(_T("[BANK SAVE ALL]"));

	for(int i = 0; i < m_pArrSdiSDK.size(); i++)
	{
		m_pSdiSDK = m_pArrSdiSDK.at(i);

		SdiMessage *pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_HIDDEN_COMMAND;
		pMsg->nParam1 = 1;
		pMsg->pParam = (int)1;		//0 : Disable, 1: Use

		if (m_pSdiSDK == NULL)
		{
			delete pMsg;
			pMsg == NULL;
			continue;
		}

		m_pSdiSDK->SdiAddMsg(pMsg);
	}	
}

void CSdiSampleDlg::OnExportLog()
{
	WriteLog(_T("[EXPORT LOG]"));
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_EXPORT_LOG;
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnFocusLocateSave()
{
	WriteLog(_T("[FOCUS LOCATE SAVE]"));
	SdiMessage *pMsg = new SdiMessage();
	pMsg->message = WM_SDI_OP_FOCUS_LOCATE_SAVE;
	m_pSdiSDK->SdiAddMsg(pMsg);
}

void CSdiSampleDlg::OnTimer(UINT_PTR nIDEvent)
{
	/*SdiMessage *pMsg = NULL;

	if(m_pSdiSDK!=NULL)
	{		
		//-- MOVIE REC BUTTON PRESS
		pMsg = new SdiMessage();
		pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_REQUIRE_5;
		m_pSdiSDK->SdiAddMsg(pMsg);	
	}

	CDialogEx::OnTimer(nIDEvent);*/


	switch(nIDEvent)
	{
	case	0:
		{
			SdiMessage *pMsg = NULL;

			if(m_pSdiSDK!=NULL)
			{		
				pMsg = new SdiMessage();
				pMsg->message = WM_SDI_OP_CAPTURE_IMAGE_ONCE;
				m_pSdiSDK->SdiAddMsg(pMsg);				
			}
		}
		break;
	case	1:
		{
			KillTimer(1);
			AfxGetMainWnd()->PostMessage(WM_CLOSE);
		}
		break;
	default:
		break;
	}
	CDialog::OnTimer(nIDEvent);
}

void CSdiSampleDlg::SetEVFLive(BOOL bEVF)
{
	m_pDlgLiveview->GetLiveview()->SetEVFLive(bEVF);
}

void CSdiSampleDlg::OnSelchangeTabControl(NMHDR *pNMHDR, LRESULT *pResult)
{
	
	int nIndex = m_ctrlTab.GetCurSel();
	switch(nIndex)
	{
	case 0:
		m_pDlgLiveview->ShowWindow(TRUE);
		m_pDlgProperty->ShowWindow(FALSE);
		break;
	case 1:
		m_pDlgLiveview->ShowWindow(FALSE);
		m_pDlgProperty->ShowWindow(TRUE);
		break;
	}
	*pResult = 0;
}


void CSdiSampleDlg::OnSelchangeDscList()
{
	int nSelect = m_ctrDSCList.GetCurSel();

	if(m_pArrSdiSDK.size() > 0 || nSelect >= 0)
	{
		SetLiveview(FALSE);

		m_pSdiSDK = m_pArrSdiSDK.at(nSelect);
		
		CString strDSC;
		/*char strDSCID[13];
		m_pSdiSDK->SdiGetDeviceID(strDSCID);
		strDSC = CString(strDSCID,13);*/
		strDSC = m_pSdiSDK->m_strDSCID;

		WriteLog(_T("[SELECT DSC] %s"), strDSC);
		//-- Get Properties
		GetPropertyAll();

		if(m_strModel.CompareNoCase(_T("GH5")) != 0)
		{
			//-- Get Focus
			m_pDlgLiveview->GetFocus();
		}

		//-- 2014-04-13 low PC need
		Sleep(1000);
		//-- Get Liveview Info
		GetLiveviewInfo();
		//-- 2014-04-13 low PC need
		Sleep(1000);
		//-- Start Liveview
		m_pDlgLiveview->Connection();	
		//-- Finish Create/Connect SDK
		//break;
		SetLiveview(TRUE);
	}
}


void CSdiSampleDlg::OnSetfocusDscList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CSdiSampleDlg::OnBnClickedBtnExportLog()
{
	CString strRecordFolder;
	

	CString szFilter = _T("Log File (*.log)|*.log|");
	CString strTitle = _T("Log File Save");
	const BOOL bVistaStyle = TRUE;  

	CFileDialog dlg(FALSE, NULL, NULL, OFN_NOCHANGEDIR | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter, NULL, 0, bVistaStyle );  
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = _T("F:\\picture");	

	//-- 2013-10-02 hongsu@esmlab.com
	//-- Default Save Name
	CTime time = CTime::GetCurrentTime();
	CString strFile;
	strFile.Format(_T("%04d_%02d_%02d_%02d_%02d_%02d"), time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),time.GetSecond());

	int length = 1024;
	LPWSTR pwsz = strFile.GetBuffer(length);
	dlg.m_ofn.lpstrFile = pwsz;
	strFile.ReleaseBuffer();

	if( dlg.DoModal() == IDOK )
	{
		strFile.Format(_T("%s.log"), strFile);
		SaveLogFile(strFile);
	}
}

char* CSdiSampleDlg::CStringToChar(CString str)
{
	wchar_t* wchar_str;     //첫번째 단계(CString to wchar_t*)를 위한 변수 
	char*    char_str;      //char* 형의 변수 
	int      char_str_len;  //char* 형 변수의 길이를 위한 변수   
	//1. CString to wchar_t* conversion 
	wchar_str = str.GetBuffer(str.GetLength());   //2. wchar_t* to char* conversion //char* 형에 대한길이를 구함 
	char_str_len = WideCharToMultiByte(CP_ACP, 0, wchar_str, -1, NULL, 0, NULL, NULL); 
	char_str = new char[char_str_len];  //메모리 할당 //wchar_t* to char* conversion 
	WideCharToMultiByte(CP_ACP, 0, wchar_str, -1, char_str, char_str_len, 0,0);
	str.ReleaseBuffer();
	return char_str;
}

void CSdiSampleDlg::SaveLogFile(CString strPath)
{
	CString strData;
	CFile file;
	char* pstrData = NULL;
	if(file.Open(strPath, CFile::modeCreate | CFile::modeReadWrite))
	{
		for(int i = 0; i < m_arrLog.GetCount(); i++)
		{
			strData = m_arrLog.GetAt(i);
			strData.Append(_T("\r\n"));
			pstrData = CStringToChar(strData);
			file.Write(pstrData, strlen(pstrData));
		}
		file.Close();

		/*for(int i = 0; i < m_ctrlLog.GetCount(); i++)
		{
			m_ctrlLog.GetText(i,strData);
			strData.Append(_T("\r\n"));
			pstrData = CStringToChar(strData);
			file.Write(pstrData, strlen(pstrData));
		}
		file.Close();*/
	}
	WriteLog(_T("[LOG] Save Complete [%s]"), strPath);
}

BOOL CSdiSampleDlg::GetPortName( CString& strPort )
{
	HKEY  hSerialCom;
	TCHAR buffer[_MAX_PATH], data[_MAX_PATH];
	DWORD len, type, dataSize;
	long  i;

	//CMiLRe 20160202 Watcher 레지스트리 읽기 옵션 변경
	if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE, REG_ADDRESS_SERIAL, 0, KEY_QUERY_VALUE, &hSerialCom) == ERROR_SUCCESS)
	{
		for (i=0, len=dataSize=_MAX_PATH; ::RegEnumValue(hSerialCom, i, buffer, &len, NULL, &type, (unsigned char*)data, &dataSize)==ERROR_SUCCESS; i++, len=dataSize=_MAX_PATH)
		{
			//CString strSerialName =(LPCTSTR)buffer;
			m_strSerialName =(LPCTSTR)buffer;

			if(m_strSerialName == SERIAL_PORT_NAME || m_strSerialName == SERIAL_PORT_NAME_4DA3)
			{
				strPort = (LPCTSTR)data;
				return TRUE;
			}
		}

		::RegCloseKey(hSerialCom);
	}

	return FALSE;
}

BOOL CSdiSampleDlg::CheckPower()
{
	//CCommThread pSerial;
	if( m_nControlerType == USB_CONTROLER_KOREA)
	{
		DWORD nRead = 1;
		/*
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;

		if( !pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		*/
		char pBuf[8];
		memset(pBuf,0,8);
		while (nRead)
		{
			//nRead = pSerial.ReadComm((BYTE *)&pBuf ,8);
			nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,8);
		}

		pBuf[0] = 0x55;
		pBuf[1] = 0x01;
		pBuf[2] = 0x00;
		pBuf[3] = 0x01;
		pBuf[4] = 0x00;
		pBuf[5] = 0x00;
		pBuf[6] = 0x00;
		pBuf[7] = 0x57;

		//pSerial.WriteComm((BYTE *)&pBuf,8);
		m_pSerial.WriteComm((BYTE *)&pBuf,8);

		Sleep(200);
		//nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,4);
		nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,4);
		//pSerial.ClosePort();

		if ( pBuf[3] == 0x02 )
			return TRUE;
		else
			return FALSE;
	}
	else if( m_nControlerType == USB_CONTROLER_CHINA )
	{
		DWORD nRead = 1;
		/*
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;
		
		if( !pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		*/
		char pBuf[17];		  
		memset(pBuf,0,17);		  
		while (nRead)
		{
		  	//nRead = pSerial.ReadComm((BYTE *)&pBuf ,8);
			nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,8);
		}
		  
		// OUT1 ON
		pBuf[0] = 0x3A;
		pBuf[1] = 0x46;
		pBuf[2] = 0x45;
		pBuf[3] = 0x30;
		pBuf[4] = 0x31;
		pBuf[5] = 0x30;
		pBuf[6] = 0x30;
		  
		pBuf[7] = 0x30;
		pBuf[8] = 0x30;
		pBuf[9] = 0x30;
		pBuf[10] = 0x30;
		pBuf[11] = 0x30;
		  
		pBuf[12] = 0x31;
		pBuf[13] = 0x30;
		pBuf[14] = 0x30;
		pBuf[15] = 0x0D;
		pBuf[16] = 0x0A;
		  
		//pSerial.WriteComm((BYTE *)&pBuf,17);
		m_pSerial.WriteComm((BYTE *)&pBuf,17);
		  
		Sleep(200);
		//nRead = pSerial.ReadComm((BYTE *)&pBuf ,13);
		nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,13);
		//pSerial.ClosePort();
		  
		if ( pBuf[8] == 0x31 )
		  	return TRUE;
		else
		  	return FALSE;
	}

	return TRUE;
}

BOOL CSdiSampleDlg::ONPower()
{	
	m_bPowerFlag = TRUE;

	DWORD dwRet = 0;
	//CCommThread pSerial;
	if( m_nControlerType == USB_CONTROLER_KOREA)
	{
		char pBuf[8];
		memset(pBuf,0,8);
		/*
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;

		if( !pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		*/
		// OUT1 ON
		pBuf[0] = 0x55;
		pBuf[1] = 0x01;
		pBuf[2] = 0x01;
		pBuf[3] = 0x02;
		pBuf[4] = 0x00;
		pBuf[5] = 0x00;
		pBuf[6] = 0x00;
		pBuf[7] = 0x59;

		//pSerial.WriteComm((BYTE *)pBuf,8);
		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,8);
	} 
	else if( m_nControlerType == USB_CONTROLER_CHINA)
	{
		char pBuf[17];
		memset(pBuf,0,17);
		
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;
				
		if( !m_pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		
 		// OUT1 ON
 		pBuf[0] = 0x3A;
 		pBuf[1] = 0x46;
 		pBuf[2] = 0x45;
 		pBuf[3] = 0x30;
 		pBuf[4] = 0x35;
 		pBuf[5] = 0x30;
 		pBuf[6] = 0x30;
 		pBuf[7] = 0x30;
 		pBuf[8] = 0x30;
 		pBuf[9] = 0x46;
 		pBuf[10] = 0x46;
 		pBuf[11] = 0x30;
 		pBuf[12] = 0x30;
 		pBuf[13] = 0x46;
 		pBuf[14] = 0x45;
 		pBuf[15] = 0x0D;
 		pBuf[16] = 0x0A;
 
 		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,17);
		
		m_pSerial.ClosePort();
	}
	else if(m_nControlerType == USB_CONTROLER_V30)
	{
		char pBuf[17];
		memset(pBuf,0,17);
		
 		// OUT1 ON
 		pBuf[0] = 0x3A;
 		pBuf[1] = 0x46;
 		pBuf[2] = 0x45;
 		pBuf[3] = 0x30;
 		pBuf[4] = 0x35;
 		pBuf[5] = 0x30;
 		pBuf[6] = 0x30;
 		pBuf[7] = 0x30;
 		pBuf[8] = 0x30;
 		pBuf[9] = 0x46;
 		pBuf[10] = 0x46;
 		pBuf[11] = 0x30;
 		pBuf[12] = 0x30;
 		pBuf[13] = 0x46;
 		pBuf[14] = 0x45;
 		pBuf[15] = 0x0D;
 		pBuf[16] = 0x0A;
 
 		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,17);
		
	}
 	//pSerial.ClosePort();
	WriteLog(_T("ONPower[%d] : %d"), m_nControlerType, dwRet);	

	if (dwRet == 0)
		WriteLog(_T("Please check 4DMakerWatcher program is running"));

 	return TRUE;
}

BOOL CSdiSampleDlg::OFFPower()
{
	m_bPowerFlag = FALSE;

	DWORD dwRet = 0;
	//CCommThread pSerial;
	if( m_nControlerType == USB_CONTROLER_KOREA)
	{
		char pBuf[8];
		memset(pBuf,0,8);
		/*
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;

		if( !pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		*/
		// OUT1 OFF
		pBuf[0] = 0x55;
		pBuf[1] = 0x01;
		pBuf[2] = 0x01;
		pBuf[3] = 0x01;
		pBuf[4] = 0x00;
		pBuf[5] = 0x00;
		pBuf[6] = 0x00;
		pBuf[7] = 0x58;

		//pSerial.WriteComm((BYTE *)pBuf,8);
		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,8);
	}
	else if( m_nControlerType == USB_CONTROLER_CHINA)
	{
		char pBuf[17];
		memset(pBuf,0,17);
		
		CString strCOM;
		if (GetPortName(strCOM) == FALSE)
			return FALSE;

		if( !m_pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
			return FALSE;
		
		pBuf[0] = 0x3A;
		pBuf[1] = 0x46;
		pBuf[2] = 0x45;
		pBuf[3] = 0x30;
		pBuf[4] = 0x35;
		pBuf[5] = 0x30;
		pBuf[6] = 0x30;
		pBuf[7] = 0x30;
		pBuf[8] = 0x30;
		pBuf[9] = 0x30;
		pBuf[10] = 0x30;
		pBuf[11] = 0x30;
		pBuf[12] = 0x30;
		pBuf[13] = 0x46;
		pBuf[14] = 0x44;
		pBuf[15] = 0x0D;
		pBuf[16] = 0x0A;
		
		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,17);
		m_pSerial.ClosePort();
	}
	else if( m_nControlerType == USB_CONTROLER_V30)
	{
		char pBuf[17];
		memset(pBuf,0,17);
		
		pBuf[0] = 0x3A;
		pBuf[1] = 0x46;
		pBuf[2] = 0x45;
		pBuf[3] = 0x30;
		pBuf[4] = 0x35;
		pBuf[5] = 0x30;
		pBuf[6] = 0x30;
		pBuf[7] = 0x30;
		pBuf[8] = 0x30;
		pBuf[9] = 0x30;
		pBuf[10] = 0x30;
		pBuf[11] = 0x30;
		pBuf[12] = 0x30;
		pBuf[13] = 0x46;
		pBuf[14] = 0x44;
		pBuf[15] = 0x0D;
		pBuf[16] = 0x0A;
				
		dwRet = m_pSerial.WriteComm((BYTE *)pBuf,17);
	}
	WriteLog(_T("OFFPower[%d] : %d"), m_nControlerType, dwRet);
	//pSerial.ClosePort();

	if (dwRet == 0)
		WriteLog(_T("Please check 4DMakerWatcher program is running"));

	return TRUE;
}

void CSdiSampleDlg::CheckModel()
{
	DWORD nRead = 1;

	//CCommThread pSerial;
	CString strCOM;
	if (GetPortName(strCOM) == FALSE)
		return ;

	if(m_strSerialName == SERIAL_PORT_NAME_4DA3)
	{
		/*m_nControlerType = USB_CONTROLER_CHINA;*/
		m_nControlerType = USB_CONTROLER_V30;
		//jhhan 180421
		m_pSerial.OpenPort(strCOM, 9600, 8, 0, 0);		//4DA 3.0의 경우 초기 OPEN 이후 최종 종료전 CLOSE 수행
		return;
	}

	//if( !pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
	if( !m_pSerial.OpenPort(strCOM, 9600, 8, 0, 0))
		return ;

	char pBuf[8];

	memset(pBuf,0,8);

	while (nRead)
	{
		//nRead = pSerial.ReadComm((BYTE *)&pBuf ,8);
		nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,8);
	}

	pBuf[0] = 0x55;
	pBuf[1] = 0x01;
	pBuf[2] = 0x00;
	pBuf[3] = 0x01;
	pBuf[4] = 0x00;
	pBuf[5] = 0x00;
	pBuf[6] = 0x00;
	pBuf[7] = 0x57;

	//pSerial.WriteComm((BYTE *)&pBuf,8);
	m_pSerial.WriteComm((BYTE *)&pBuf,8);

	Sleep(200);
	//nRead = pSerial.ReadComm((BYTE *)&pBuf ,4);
	nRead = m_pSerial.ReadComm((BYTE *)&pBuf ,4);
	//pSerial.ClosePort();
	if(pBuf[0] == 34)
		m_nControlerType = USB_CONTROLER_KOREA;
	else
		m_nControlerType = USB_CONTROLER_CHINA;
	//jhhan 180421
	m_pSerial.ClosePort();		//4DA 2.0의 경우 OPEN & CLOSE 수행
}


HBRUSH CSdiSampleDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF textRGB = RGB(255,255,255);

 	if ( pWnd->GetDlgCtrlID() == IDC_LOG  || pWnd->GetDlgCtrlID() == IDC_DSC_LIST)
	{
 		pDC->SetTextColor(textRGB);
		pDC->SetBkColor(RGB(52, 51, 51));
	}

	return m_brush;
}


void CSdiSampleDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	COLORREF btnRGB = RGB(87,87,86);
	COLORREF textRGB = RGB(255,255,255);
	COLORREF selectBtnRGB = RGB(69,68,66);
	CDC dc;
	RECT rect;
	dc.Attach(lpDrawItemStruct->hDC);	//Get the Button DC to CDC
	rect = lpDrawItemStruct->rcItem	;	//Store the Button rect to local rect

	//dc.Draw3dRect(&rect, btnRGB, btnRGB);
	//dc.FillSolidRect(&rect, btnRGB);

	UINT state = lpDrawItemStruct->itemState;
	if((state & ODS_SELECTED))
	{
		dc.FillSolidRect(&rect, selectBtnRGB);
		dc.SetBkColor(selectBtnRGB);
	}
	else
	{
		dc.FillSolidRect(&rect, btnRGB);
		dc.SetBkColor(btnRGB);
	}

	//Draw Color Text
	dc.SetTextColor(textRGB);		//Setting the Text Color

	TCHAR buffer[MAX_PATH];
	ZeroMemory(buffer, MAX_PATH);
	::GetWindowText(lpDrawItemStruct->hwndItem, buffer, MAX_PATH);

	UINT nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
	RECT textRect = rect;
	dc.DrawText(buffer, &textRect, nFormat);

	dc.Detach();		

	//CDialogEx::OnDrawItem(nIDCtl, lpDrawItemStruct);
}


void CSdiSampleDlg::OnBnClickedBtnAdmin()
{
	CString strTemp;
	GetDlgItem(IDC_EDIT_ADMIN)->GetWindowText(strTemp);

	if (strTemp != _T("esmlab"))
		return;

	m_pDlgLiveview->SetAdminMode(TRUE);
	m_pDlgProperty->SetAdminMode(TRUE);
}


BOOL CSdiSampleDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->wParam == VK_ESCAPE) 
	{
		return TRUE;
	}

	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{     
		if (GetDlgItem(IDC_EDIT_ADMIN) == GetFocus())
		{
			OnBnClickedBtnAdmin();
		}
	}

	return FALSE;
	//return CDialogEx::PreTranslateMessage(pMsg);
}
