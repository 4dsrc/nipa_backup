// CustomListBox.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CustomListBox.h"


// CCustomListBox

IMPLEMENT_DYNAMIC(CCustomListBox, CListBox)

CCustomListBox::CCustomListBox()
{
	
}

CCustomListBox::~CCustomListBox()
{
}


BEGIN_MESSAGE_MAP(CCustomListBox, CListBox)
	ON_WM_PAINT()
END_MESSAGE_MAP()



// CCustomListBox 메시지 처리기입니다.




void CCustomListBox::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	
	CBrush brush;
	//brush.CreateSolidBrush(RGB(32, 32, 31));
	brush.CreateSolidBrush(RGB(255,255,255));

	RECT rect;
	GetClientRect( &rect );
	dc.FrameRect(&rect, &brush);
}
