﻿/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */



#include "stdafx.h"
#include "SdiSample.h"
#include "PropertyDlg.h"
#include "afxdialogex.h"
#include "SdiSdk.h"
#include "SdiSampleDlg.h"
#include "NXInfo.h"

enum PTP_VALUE
{
	PTP_VALUE_NONE = 0,
	PTP_VALUE_INT_8,
	PTP_VALUE_UINT_8,
	PTP_VALUE_UINT_16,
	PTP_VALUE_UINT_32,
	PTP_VALUE_STRING,
	PTP_VALUE_MODE,
};


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


static const int timer_start_Interval_capture	= WM_USER + 0x0001;
#define MIM_INTERVAL 3000
// CLiveviewDlg dialog

//-- CALLBACK
static int CALLBACK BrowseCallbackProc(
									   HWND hWnd, 
									   UINT uMsg, 
									   LPARAM lParam, 
									   LPARAM lpData
									   )
{
	TCHAR szPath[ MAX_PATH ] = _T("");
	switch (uMsg) 
	{
	case BFFM_INITIALIZED:
		if( lpData != NULL )
			::SendMessage( 
			hWnd, 
			BFFM_SETSELECTION, 
			WPARAM(TRUE), 
			LPARAM(lpData)
			);
		break;
	case BFFM_SELCHANGED:
		::SHGetPathFromIDList( LPITEMIDLIST(lParam), szPath );
		::SendMessage(
			hWnd, 
			BFFM_SETSTATUSTEXT, 
			WPARAM(NULL), 
			LPARAM(szPath)
			);
		break;
	}
	return 0;
}

// CPropertyDlg dialog

IMPLEMENT_DYNAMIC(CPropertyDlg, CDialog)

CPropertyDlg::CPropertyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPropertyDlg::IDD, pParent)
	, m_nInterval(MIM_INTERVAL)
	, m_nIndex(1)
{
	m_pDevMgr = new CDevInfoManager();
	m_strPath = _T("f:\\picture");
	m_strModel = _T("NX1000");
	m_bTimerOn  = FALSE;
//-- joonho.kim Temp
	m_nLarge = 0;
	m_bPause = 0;
}

CPropertyDlg::~CPropertyDlg()
{
	//-- 2013-11-29 hongsu.jung
	DisConnect();

	if(m_pDevMgr)
	{
		m_pDevMgr->RemoveAll();
		delete m_pDevMgr;
		m_pDevMgr = NULL;
	}
}

void CPropertyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control (pDX, IDC_COMBO_MOVIE_SIZE , m_ctrlMoviesize);
	DDX_Control	(pDX, IDC_COMBO_SIZE	, m_ctrlPhotosize);
	DDX_Control	(pDX, IDC_COMBO_QUALITY	, m_ctrlQuality);
	DDX_Control	(pDX, IDC_COMBO_WB		, m_ctrlWB);
	DDX_Control	(pDX, IDC_COMBO_ISO		, m_ctrlISO);
	DDX_Control	(pDX, IDC_COMBO_SS		, m_ctrlSS);
	DDX_Control	(pDX, IDC_COMBO_MODE	, m_ctrlMode);
	DDX_Control	(pDX, IDC_COMBO_AFMODE	, m_ctrlAfMode);
	DDX_Control	(pDX, IDC_COMBO_FNUMBER	, m_ctrlFNumber);
	DDX_Control	(pDX, IDC_COMBO_PW		, m_ctrlPW);
	//CMiLRe 2015129 줌 컨트롤
	DDX_Control	(pDX, IDC_ZOOM_CUR		, m_editZoom);	
	DDX_Control	(pDX, IDC_ZOOM_CUR_RANGE, m_editZoomRange);	

	DDX_Text	(pDX, IDC_INDEX, m_nIndex);
	DDX_Text	(pDX, IDC_DIR_PATH		, m_strPath);
	DDX_Text	(pDX, IDC_INTERVAL, m_nInterval);
	//CMiLRe 2015129 줌 컨트롤
	DDX_Text	(pDX, IDC_ZOOM_CUR, m_nZoom);	
	DDV_MinMaxInt(pDX, m_nInterval, MIM_INTERVAL, 10000000);

	DDX_Control(pDX, IDC_PROPERTY_CODE, m_ctrlPropertyCode);
	DDX_Control(pDX, IDC_PROPERTY_VALUE, m_ctrlPropertyValue);
	DDX_Control(pDX, IDC_CHK_ALL_FLAG, m_ctrlAllFlag);
	DDX_Control(pDX, IDC_COMBO_LENS_POWER, m_ctrlLensPower);
	DDX_Control(pDX, IDC_COMBO_LCD, m_ctrlLCD);
	DDX_Control(pDX, IDC_COMBO_PEAKING, m_ctrlPeaking);
}

BEGIN_MESSAGE_MAP(CPropertyDlg, CDialog)
	ON_WM_TIMER()	
	ON_CBN_SELCHANGE(IDC_COMBO_MODE		, OnCbnSelchangeComboMode)
	ON_CBN_SELCHANGE(IDC_COMBO_SIZE		, OnCbnSelchangeComboSize)
	ON_CBN_SELCHANGE(IDC_COMBO_QUALITY	, OnCbnSelchangeComboQuality)
	ON_CBN_SELCHANGE(IDC_COMBO_WB		, OnCbnSelchangeComboWB)
	ON_CBN_SELCHANGE(IDC_COMBO_MOVIE_SIZE	, OnCbnSelchangeComboMovieSize)
	ON_CBN_SELCHANGE(IDC_COMBO_ISO		, OnCbnSelchangeComboISO)
	ON_CBN_SELCHANGE(IDC_COMBO_SS		, OnCbnSelchangeComboSS)	
	ON_CBN_SELCHANGE(IDC_COMBO_AFMODE	, OnCbnSelchangeComboAFMode)	
	ON_CBN_SELCHANGE(IDC_COMBO_FNUMBER	, OnCbnSelchangeComboFNumber)		
	ON_CBN_SELCHANGE(IDC_COMBO_PW		, OnCbnSelchangeComboPW)
	ON_BN_CLICKED	(IDC_DIR_OPEN		, OnBnClickedDirOpen)
	ON_BN_CLICKED	(IDC_DIR_OPEN2		, OnBnClickedDirOpen2)
	ON_BN_CLICKED	(IDC_DIR_INTERVAL	, OnBnClickedInterval)
	ON_BN_CLICKED	(IDCB_REBOOT	, OnBnClickedReboot)
	ON_BN_CLICKED	(IDCB_SHUT_DOWN	, OnBnClickedShutDown)
	ON_BN_CLICKED	(IDCB_FW_DN_N_UP	, OnBnClickedFWDownLoad_N_Updawn)
	//CMiLRe 2015129 줌 컨트롤
	ON_BN_CLICKED	(IDCB_ZOOM	, OnBnClickedZoom)
	
	ON_BN_CLICKED(IDC_BTN_GET_PROPERTY, &CPropertyDlg::OnBnClickedBtnGetProperty)
	ON_BN_CLICKED(IDC_BTN_SET_PROPERTY, &CPropertyDlg::OnBnClickedBtnSetProperty)
	ON_BN_CLICKED(IDC_BTN_BANK_SAVE, &CPropertyDlg::OnBnClickedBtnBankSave)
	ON_BN_CLICKED(IDC_BTN_EXPORT_LOG, &CPropertyDlg::OnBnClickedBtnExportLog)
	ON_BN_CLICKED(IDC_BTN_FOCUS_LOCATE_SAVE, &CPropertyDlg::OnBnClickedBtnFocusLocateSave)
	ON_BN_CLICKED(IDC_BTN_SENSOR_CLEANING, &CPropertyDlg::OnBnClickedBtnSensorCleaning)
	ON_BN_CLICKED(IDC_BTN_VERSION, &CPropertyDlg::OnBnClickedBtnVersion)
	ON_CBN_SELCHANGE(IDC_COMBO_LENS_POWER, &CPropertyDlg::OnSelchangeComboLensPower)
	ON_CBN_SELCHANGE(IDC_COMBO_LCD, &CPropertyDlg::OnSelchangeComboLcd)
	ON_CBN_SELCHANGE(IDC_COMBO_PEAKING, &CPropertyDlg::OnSelchangeComboPeaking)
	ON_WM_CTLCOLOR()
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(IDC_BTN_BANK_SAVE_ALL, &CPropertyDlg::OnBnClickedBtnBankSaveAll)
END_MESSAGE_MAP()


void CPropertyDlg::DisConnect()
{
	if(m_pDevMgr)
		m_pDevMgr->RemoveAll();

	//-- 2013-11-29 hongsu.jung
	Clean();
}


// CPropertyDlg message handlers
//-- Add Device Information
void CPropertyDlg::AddDevInfo(IDeviceInfo *pDevInfo)
{
	if(m_pDevMgr)
		m_pDevMgr->SetDevInfo(pDevInfo);
}

//-- Update Combo Values
void CPropertyDlg::UpdateCombo(int nPTPCode)
{
	IDeviceInfo* pDevInfo = m_pDevMgr->GetDevInfo(nPTPCode);
	int nPropCode = pDevInfo->GetPropCode();

	CString strCode;
	m_ctrlPropertyCode.GetWindowText(strCode);
	int nCode = _tcstol(strCode, NULL, 16);

	if(nCode == nPropCode)
	{
		CString strValue;
		if(pDevInfo->GetType() == PTP_VALUE_STRING)
		{
			m_ctrlPropertyValue.SetWindowText(pDevInfo->GetCurrentStr());
		}
		else
		{
			strValue.Format(_T("0x%X"),pDevInfo->GetCurrentEnum());
			m_ctrlPropertyValue.SetWindowText(strValue);

			if(m_ctrlAllFlag.GetCheck())
			{
				for(int i=0; i<pDevInfo->GetPropValueCntEnum(); i++)
				{
					int nEnumValue = pDevInfo->GetPropValueListEnum()->GetAt(i);

					if(nEnumValue == pDevInfo->GetCurrentEnum())
						continue;

					SdiMessage* pMsg = new SdiMessage();
					pMsg->message = WM_SAMPLE_OP_WRITE_LOG;
					pMsg->nParam1 = nPropCode;
					pMsg->nParam2 = nEnumValue;
					::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
				}
			}
		}
	}

	switch(nPropCode)
	{
	//-- Image size
	case SDI_DPC_FUNCTIONALMODE			:	UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlMode		);	
											//-- disable Change Mode
											m_ctrlMode.EnableWindow(FALSE);									break;	//-- Production Mode	
	case SDI_DPC_MOVIESIZE				:	UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlMoviesize	);	break;	//-- Movie Size
	case SDI_DPC_IMAGESIZE				:	
		{
			if(pDevInfo->GetType() == PTP_VALUE_STRING)
				UpdatePropertyStr	(nPropCode, pDevInfo, &m_ctrlPhotosize	);	
			else
				UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlPhotosize	);	
				
			break;	//-- Photo Size
		}
	case SDI_DPC_COMPRESSIONSETTING		:	UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlQuality	);	break;	//-- Quality
	case SDI_DPC_WHITEBALANCE			:	UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlWB			);	break;	//-- White Balance		
	case SDI_DPC_EXPOSUREINDEX			:	UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlISO		);	break;	//-- ISO
	case SDI_DPC_SAMSUNG_SHUTTERSPEED	:	UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlSS			);	break;	//-- Shutter Speed
	case SDI_DPC_FOCUSMODE				:	UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlAfMode		);	break;	//-- Focus Mode		
	case SDI_DPC_SAMSUNG_PICTUREWIZARD				:	UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlPW		);	break;	//-- PW
	case SDI_DPC_LENS_POWER				:	UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlLensPower	);	break;	//-- Lens power ctrl
	case SDI_DPC_LCD					:	UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlLCD		);	break;	//-- LCD ctrl
	case SDI_DPC_PEAKING				:	UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlPeaking	);	break;	//-- Peaking		
	case SDI_DPC_F_NUMBER				:
		{
			if(m_strModel.CompareNoCase(_T("GH5")) == 0)
			{
				UpdatePropertyEnum	(nPropCode, pDevInfo, &m_ctrlFNumber		);	break;	//-- F
			}
			else
			{
				CString strEnumValue = _T("");
				m_ctrlFNumber.ResetContent();
				if(pDevInfo->GetPropValueCntEnum())
				{
					for(int i=0; i<pDevInfo->GetPropValueCntEnum(); i++)
					{
						int nEnumValue = pDevInfo->GetPropValueListEnum()->GetAt(i);
						//-- 2013-02-24 hongsu.jung
						//-- Specific Value
						if(nEnumValue % 100)
							strEnumValue.Format(_T("F%.1f"), (float)nEnumValue / 100);
						else
						{
							if(nEnumValue >= 1000)	strEnumValue.Format(_T("F%.0f"), (float)nEnumValue / 100);
							else					strEnumValue.Format(_T("F%.1f"), (float)nEnumValue / 100);
						}										
						m_ctrlFNumber.AddString(strEnumValue);
					}			
				}			
				int nIndex = m_pDevMgr->GetCurrentValueIndex(nPTPCode);
				m_ctrlFNumber.SetCurSel(nIndex);
			}
			
		}
		break;
	default:
		TRACE("Unknow Property [%d]\n",nPropCode);
		break;
	}	
}

//CMiLRe 2015129 줌 컨트롤
void CPropertyDlg::OnUpdateValue(int nPTPCode)
{
	IDeviceInfo* pDevInfo = m_pDevMgr->GetDevInfo(nPTPCode);
	int nPropCode = pDevInfo->GetPropCode();
	switch(nPropCode)
	{	
	case SDI_DPC_SAMSUNG_ZOOM_CTRL_GET				:	UpdatePropertyZoom	(pDevInfo );	break;	//-- Photo Size
	case SDI_DPC_SAMSUNG_WHITE_BALANCE_DETAIL_COUSTOM_RGB:	UpdatePropertyZoom	(pDevInfo );	break;	//-- Photo Size
		break;
	default:
		TRACE("Unknow Property [%d]\n",nPropCode);
		break;
	}	
}

//--Photosize Event Handler
void CPropertyDlg::UpdatePropertyStr(int nPropCode, IDeviceInfo* pDevInfo, CComboBox* pCombo)
{
	CString strValue = _T("");	
	pCombo->ResetContent();
	if(pDevInfo->GetPropValueCntStr())
	{
		for(int i=0; i<pDevInfo->GetPropValueCntStr(); i++)
		{
			strValue = pDevInfo->GetPropValueListStr()->GetAt(i);						
			CString strDesc = LoadInfoStringToDesc(nPropCode,strValue);
			m_ctrlPhotosize.AddString(strDesc);
		}
	}
	else
	{
		strValue = pDevInfo->GetCurrentStr();
		m_ctrlPhotosize.AddString(strValue);
	}

	int nIndex = m_pDevMgr->GetCurrentValueIndex(nPropCode);
	m_ctrlPhotosize.SetCurSel(nIndex);
}

//--Photosize Event Handler
void CPropertyDlg::UpdatePropertyEnum(int nPropCode, IDeviceInfo* pDevInfo, CComboBox* pCombo)
{
	int nEnumValue = 0;
	CString strEnumValue = _T("");
	pCombo->ResetContent();

	int nPropValueCount = pDevInfo->GetPropValueCntEnum();
	if(nPropValueCount)
	{
		for(int i = 0 ; i < nPropValueCount ; i++)
		{
			nEnumValue = pDevInfo->GetPropValueListEnum()->GetAt(i);
			CString strDesc = LoadInfoToDesc(nPropCode,nEnumValue);
			pCombo->AddString(strDesc);
		}
		//-- 2013-03-08 hongsu.jung
		pCombo->EnableWindow(TRUE);
	}
	else
	{
		if(m_strModel.CompareNoCase(_T("GH5")) == 0)
		{
			nEnumValue = pDevInfo->GetCurrentEnum();
			CString strDesc = LoadInfoToDesc(nPropCode,nEnumValue);
			pCombo->AddString(strDesc);
			pCombo->EnableWindow(TRUE);
		}
		else
		{
			//-- 2013-03-08 hongsu.jung
			//-- Enable ComboBox			
			pCombo->AddString(_T("Disable"));
			pCombo->EnableWindow(FALSE);
		}
		
	}

	int nIndex = m_pDevMgr->GetCurrentValueIndex(nPropCode);
	pCombo->SetCurSel(nIndex);
}

//--Photosize Event Handler
void CPropertyDlg::OnCbnSelchangeComboSize()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_IMAGESIZE;
	pMsg->nParam2 = m_ctrlPhotosize.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


//-- Quality Event Handler
void CPropertyDlg::OnCbnSelchangeComboQuality()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_COMPRESSIONSETTING;
	pMsg->nParam2 = m_ctrlQuality.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

//-- White Balance Event Handler
void CPropertyDlg::OnCbnSelchangeComboWB()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_WHITEBALANCE;
	pMsg->nParam2 = m_ctrlWB.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

//-- Movie Size Event Handler
void CPropertyDlg::OnCbnSelchangeComboMovieSize()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_SAMSUNG_VIDEO_OUT;
	if(m_ctrlMoviesize.GetCurSel() < 14)
		pMsg->nParam2 = 0;
	else
		pMsg->nParam2 = 1;

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
	
	SdiMessage* pMsg1 = new SdiMessage();
	pMsg1->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg1->nParam1 = SDI_DPC_MOVIESIZE;
	pMsg1->nParam2 = m_ctrlMoviesize.GetCurSel();
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg1);	
	
}

void CPropertyDlg::OnCbnSelchangeComboPW()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_SAMSUNG_PICTUREWIZARD;
	pMsg->nParam2 = m_ctrlPW.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

//--ISO
void CPropertyDlg::OnCbnSelchangeComboISO()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_EXPOSUREINDEX;
	pMsg->nParam2 = m_ctrlISO.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

//--Shutter Speed
void CPropertyDlg::OnCbnSelchangeComboSS()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_SAMSUNG_SHUTTERSPEED;
	pMsg->nParam2 = m_ctrlSS.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}

//-- Production Mode
void CPropertyDlg::OnCbnSelchangeComboMode()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_FUNCTIONALMODE;
	pMsg->nParam2 = m_ctrlMode.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
}
 
//--Photo size Event Handler
void CPropertyDlg::OnCbnSelchangeComboAFMode()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_FOCUSMODE;
	pMsg->nParam2 = m_ctrlAfMode.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

//-- 2013-02-24 hongsu.jung
//-- F-Number
void CPropertyDlg::OnCbnSelchangeComboFNumber()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_F_NUMBER;
	pMsg->nParam2 = m_ctrlFNumber.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

//-- NXInfo Property String To String
CString CPropertyDlg::LoadInfoStringToDesc(int nPTPCode, CString strValue)
{
	CString strDesc = _T("");
	CString strSection = _T("");
	CString strFilePath = _T("");
	CString strInfoFile;
	CNXInfo ini;
	strInfoFile.Format(_T("\\..\\..\\..\\config\\%s.Info"),m_strModel);
	while(1)
	{		
		TCHAR CurrentDirectory[MAX_PATH];  
		GetCurrentDirectory(MAX_PATH, CurrentDirectory);
		strFilePath = CurrentDirectory;
		strFilePath.Append(strInfoFile);
//		strFilePath.Format(_T("D:\\Project\\SRS_SDK\\config\\NX1.info"));
		if(ini.SetIniFilename (strFilePath))
			break;

		int nCount = strInfoFile.GetLength();
		if(nCount < 15)
			break;
		strInfoFile = strInfoFile.Mid(3,nCount);//.RemoveAt(0);
	}

	strSection.Format(_T("%x"),nPTPCode);
	if(strSection.CompareNoCase(_T("d84b")) == 0)
		TRACE(_T("ddd"));
	strDesc = ini.GetString(strSection, strValue);

	return strDesc;
}

CString CPropertyDlg::LoadInfoToDesc(int nPTPCode, int nValue)
{
	CString strRet = _T("");

	if(m_strModel.CompareNoCase(_T("GH5")) == 0)
	{
		strRet = LoadInfoHexToDesc(nPTPCode, nValue);
	}
	else
	{
		switch(nPTPCode)
		{
		case SDI_DPC_FUNCTIONALMODE			:	
		case SDI_DPC_COMPRESSIONSETTING		:
		case SDI_DPC_WHITEBALANCE			:
		case SDI_DPC_MOVIESIZE				:
		case SDI_DPC_SAMSUNG_PICTUREWIZARD	:	
		case SDI_DPC_SAMSUNG_SHUTTERSPEED	:
		case SDI_DPC_FOCUSMODE				:	strRet = LoadInfoHexToDesc(nPTPCode, nValue);	break;
		case SDI_DPC_EXPOSUREINDEX			:	strRet = LoadInfoIntToDesc(nPTPCode, nValue);	break;	
		case SDI_DPC_F_NUMBER				:
		case SDI_DPC_IMAGESIZE				:
		default								:	break;
		}
	}
	
	return strRet;
}

//-- NXInfo Property Int To String
CString CPropertyDlg::LoadInfoIntToDesc(int nPTPCode, int nValue)
{
	CString strValue = _T("");
	strValue.Format(_T("%d"),nValue);	
	return LoadInfoStringToDesc(nPTPCode, strValue);
}

//-- NXInfo Property hex To String
CString CPropertyDlg::LoadInfoHexToDesc(int nPTPCode, int nValue)
{
	CString strValue = _T("");
	strValue.Format(_T("%x"),nValue);	
	return LoadInfoStringToDesc(nPTPCode, strValue);
}


static int CALLBACK FolderSelectDialogCallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	switch(uMsg)
	{
	case BFFM_INITIALIZED:
		{
			SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData);
		}
		break;
	}
	return 0;
}
void CPropertyDlg::OnBnClickedDirOpen()
{	
	UpdateData(TRUE);
	ITEMIDLIST* pIIDList;
	TCHAR tstrPathName[MAX_PATH] = _T("");
	BROWSEINFO bInfo;
	memset(&bInfo, 0 , sizeof(bInfo));
	bInfo.hwndOwner = AfxGetMainWnd()->m_hWnd;
	bInfo.pidlRoot = NULL;
	bInfo.pszDisplayName = tstrPathName;
	bInfo.lpszTitle = _T("Select Directory");
	bInfo.ulFlags = BIF_RETURNONLYFSDIRS | BIF_VALIDATE;
	bInfo.lpfn = FolderSelectDialogCallbackProc;

	if(m_strPath.GetLength() <= 0)
	{
		TCHAR tstrCurrentDir[MAX_PATH] = _T("");
		GetCurrentDirectory( MAX_PATH, tstrCurrentDir);
		m_strPath = tstrCurrentDir;
	}

	TCHAR tstrDefaultOpenFolder[MAX_PATH] = _T("");
	_stprintf(tstrDefaultOpenFolder, _T("%s"), m_strPath);
	bInfo.lParam = (LPARAM)(LPCTSTR)tstrDefaultOpenFolder;
	pIIDList = ::SHBrowseForFolder(&bInfo);
	if(pIIDList)
	{
		SHGetPathFromIDList(pIIDList, tstrPathName);
		m_strPath = tstrPathName;
	}
	else
	{
		return;
	}
	UpdateData(FALSE);
	/*UpdateData(TRUE);
	BROWSEINFO bi;
	memset(&bi, 0, sizeof(bi));
	CString strPath;

	bi.hwndOwner	= this->GetSafeHwnd();
	bi.lpszTitle	= _T("Select Folder");
	bi.ulFlags		= BIF_NEWDIALOGSTYLE;
	bi.lParam		= (LPARAM)(LPCTSTR)m_strPath;
	bi.lpfn			= BrowseCallbackProc;
	
	::OleInitialize(NULL);
	LPITEMIDLIST pIDL = ::SHBrowseForFolder(&bi);

	if ( pIDL )
	{
		TCHAR buffer[_MAX_PATH] = {0};
		if (::SHGetPathFromIDList(pIDL, (LPTSTR)buffer) != 0)
		{
			strPath.Format(_T("%s"), buffer);
		}
		::OleUninitialize();
		m_strPath = strPath;
	}	
	UpdateData(FALSE);*/

	
}



void CPropertyDlg::OnBnClickedDirOpen2()
{
	UpdateData(TRUE);
	ShellExecute(NULL ,_T("open"), m_strPath, NULL, NULL, SW_SHOWNORMAL);
}


//-- 2013-02-24 hongsu.jung
//-- Erase same Option
BOOL CPropertyDlg::IsExist(CComboBox* pCtrlCombo, CString strValue)
{
	int nAll = pCtrlCombo->GetCount();
	CString strExist;
	while(nAll--)
	{
		pCtrlCombo->GetLBText(nAll,strExist);
		if(strValue == strExist)
			return TRUE;
	}
	return FALSE;
}

//-- Update Combo Values
void CPropertyDlg::Clean()
{
	m_ctrlMode		.ResetContent();
	m_ctrlPhotosize	.ResetContent();
	m_ctrlQuality	.ResetContent();
	m_ctrlWB		.ResetContent();
	m_ctrlISO		.ResetContent();
	m_ctrlSS		.ResetContent();
	m_ctrlAfMode	.ResetContent();
	m_ctrlFNumber	.ResetContent();
	m_ctrlPW	.ResetContent();

}

// CLiveviewDlg message handlers
void CPropertyDlg::OnBnClickedInterval()
{
	if(!m_bTimerOn)
	{
		UpdateData(TRUE);
		if(m_nInterval < MIM_INTERVAL)
			m_nInterval = MIM_INTERVAL;
		CaptureOnce();
		SetTimer(timer_start_Interval_capture, m_nInterval, NULL);
		m_bTimerOn = TRUE;
		UpdateData(FALSE);
	}
	else
	{
		KillTimer(timer_start_Interval_capture);
		m_bTimerOn = FALSE;
	}
}

void CPropertyDlg::OnBnClickedReboot()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_DEVICE_REBOOT;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}
void CPropertyDlg::OnBnClickedShutDown()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_DEVICE_SHUT_DOWN;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

void CPropertyDlg::OnBnClickedFWDownLoad_N_Updawn()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_FW_DOWNLOAD_N_UPDATE;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

//CMiLRe 2015129 줌 컨트롤
void CPropertyDlg::UpdatePropertyZoom(IDeviceInfo* pDevInfo)
{
	CString zoom, zoomrange;
	m_nZoom = (pDevInfo->GetCurrentEnum()&0xFF00)>>8;
	zoom.Format(_T("%d"),  m_nZoom);
	zoomrange.Format(_T("%d"),  pDevInfo->GetCurrentEnum()&0x00FF);
	m_editZoom.SetWindowTextW(zoom);
	m_editZoomRange.SetWindowTextW(zoomrange);
}

//CMiLRe 2015129 줌 컨트롤
void CPropertyDlg::OnBnClickedZoom()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_SAMSUNG_ZOOM_CTRL_GET;
	CString strZoom;
	m_editZoom.GetWindowTextW(strZoom);
	m_nZoom = _ttoi(strZoom);	
	m_nZoom = m_nZoom<<8;
	pMsg->nParam2 = m_nZoom;

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);;	
}

//------------------------------------------------------------------------------ 
//! @brief		OnTimer
//! @date		2011-6-16
//------------------------------------------------------------------------------ 
void CPropertyDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
	case timer_start_Interval_capture:
		CaptureOnce();
		break;
	}	
	CDialog::OnTimer(nIDEvent);
}

void CPropertyDlg::CaptureOnce()
{
	//-- Add Index
	UpdateData(FALSE);
	m_nIndex++;

	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_CAPTURE;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


void CPropertyDlg::OnBnClickedBtnGetProperty()
{
	CString strCode;
	m_ctrlPropertyCode.GetWindowText(strCode);
	int nCode = _tcstol(strCode, NULL, 16);
	

	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_GET_CUSTOM_PROPERTY_DESC;
	pMsg->nParam1 = nCode;
	pMsg->nParam2 = m_ctrlAllFlag.GetCheck();
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


void CPropertyDlg::OnBnClickedBtnSetProperty()
{
	CString strCode, strValue;
	m_ctrlPropertyCode.GetWindowText(strCode);
	m_ctrlPropertyValue.GetWindowText(strValue);
	
	int nCode = _tcstol(strCode, NULL, 16);
	unsigned int nValue = _tcstoul(strValue, NULL, 16);


	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_CUSTOM_PROPERTY_VALUE;
	pMsg->nParam1 = nCode;
	pMsg->nParam2 = nValue;
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

CString CPropertyDlg::ConvertToHex(CString data)
{
	CString returnvalue;
	for (int x = 0; x < data.GetLength(); x++)
	{
		CString temporary;
		int value = (int)(data[x]);
		returnvalue.Format(_T("%02X"), value);
		returnvalue += temporary;
	}
	return returnvalue;
}




void CPropertyDlg::OnBnClickedBtnBankSave()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_BANK_SAVE;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


void CPropertyDlg::OnBnClickedBtnExportLog()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_EXPORT_LOG;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}

void CPropertyDlg::OnBnClickedBtnFocusLocateSave()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_FOCUS_LOCATE_SAVE;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);

	SdiMessage* pMsg1 = new SdiMessage();
	pMsg1->message = WM_SAMPLE_OP_BANK_SAVE;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg1->message, (LPARAM)pMsg1);
}


void CPropertyDlg::OnBnClickedBtnSensorCleaning()
{
	if(m_strModel.CompareNoCase(_T("GH5")) == 0)
	{		
		SdiMessage* pMsg = new SdiMessage();
		pMsg->message = WM_SAMPLE_OP_SET_CUSTOM_PROPERTY_VALUE;
		pMsg->nParam1 = 0xd95a;		// sensor cleaning

		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
	}
}


void CPropertyDlg::OnBnClickedBtnVersion()
{
	if(m_strModel.CompareNoCase(_T("GH5")) == 0)
	{		
		SdiMessage* pMsg = new SdiMessage();
		pMsg->message = WM_SAMPLE_OP_GET_CUSTOM_PROPERTY_DESC;
		pMsg->nParam1 = 0xD991;				//PTP_DPC_SAMSUNG_FW_VERSION;  FW version

		::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);	
	}
}


void CPropertyDlg::OnSelchangeComboLensPower()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_LENS_POWER;		
	pMsg->nParam2 = m_ctrlLensPower.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


void CPropertyDlg::OnSelchangeComboLcd()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_LCD;		
	pMsg->nParam2 = m_ctrlLCD.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


void CPropertyDlg::OnSelchangeComboPeaking()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_SET_PROPERTY_VALUE;
	pMsg->nParam1 = SDI_DPC_PEAKING;		
	pMsg->nParam2 = m_ctrlPeaking.GetCurSel();

	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}


HBRUSH CPropertyDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	COLORREF textRGB = RGB(255,255,255);

	//if (nCtlColor == CTLCOLOR_STATIC)  )
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(textRGB);
	}

	return m_brush;
}


BOOL CPropertyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_brush.CreateSolidBrush(RGB(52, 51, 51));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPropertyDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	COLORREF btnRGB = RGB(87,87,86);
	COLORREF textRGB = RGB(255,255,255);
	COLORREF selectBtnRGB = RGB(69,68,66);
	CDC dc;
	RECT rect;
	dc.Attach(lpDrawItemStruct->hDC);	//Get the Button DC to CDC
	rect = lpDrawItemStruct->rcItem	;	//Store the Button rect to local rect

	//dc.Draw3dRect(&rect, btnRGB, btnRGB);
	//dc.FillSolidRect(&rect, btnRGB);

	UINT state = lpDrawItemStruct->itemState;
	if((state & ODS_SELECTED))
	{
		dc.FillSolidRect(&rect, selectBtnRGB);
		dc.SetBkColor(selectBtnRGB);
	}
	else
	{
		dc.FillSolidRect(&rect, btnRGB);
		dc.SetBkColor(btnRGB);
	}

	//Draw Color Text
	dc.SetBkColor(btnRGB);		//Setting the Text Background Color
	dc.SetTextColor(textRGB);		//Setting the Text Color

	TCHAR buffer[MAX_PATH];
	ZeroMemory(buffer, MAX_PATH);
	::GetWindowText(lpDrawItemStruct->hwndItem, buffer, MAX_PATH);

	UINT nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE;
	RECT textRect = rect;
	if (nIDCtl == IDC_BTN_SENSOR_CLEANING)
	{
		textRect.left = rect.left;
		textRect.right = rect.right;
		textRect.top = (rect.top+rect.bottom)/2-15;
		textRect.bottom = rect.bottom;
		nFormat = DT_CENTER|DT_VCENTER;
	}

	dc.DrawText(buffer, &textRect, nFormat);

	dc.Detach();		

	//CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CPropertyDlg::SetAdminMode( BOOL b )
{
	GetDlgItem(IDC_STATIC_1)->ShowWindow(b);
	GetDlgItem(IDC_BTN_FOCUS_LOCATE_SAVE)->ShowWindow(b);
	GetDlgItem(IDC_BTN_EXPORT_LOG)->ShowWindow(b);
	GetDlgItem(IDCB_REBOOT)->ShowWindow(b);
	GetDlgItem(IDCB_FW_DN_N_UP)->ShowWindow(b);

	GetDlgItem(IDC_STATIC_2)->ShowWindow(b);
	GetDlgItem(IDC_STATIC_3)->ShowWindow(b);
	GetDlgItem(IDC_PROPERTY_CODE)->ShowWindow(b);
	GetDlgItem(IDC_PROPERTY_VALUE)->ShowWindow(b);
	GetDlgItem(IDC_CHK_ALL_FLAG)->ShowWindow(b);
	GetDlgItem(IDC_BTN_GET_PROPERTY)->ShowWindow(b);
	GetDlgItem(IDC_BTN_SET_PROPERTY)->ShowWindow(b);
}


void CPropertyDlg::OnBnClickedBtnBankSaveAll()
{
	SdiMessage* pMsg = new SdiMessage();
	pMsg->message = WM_SAMPLE_OP_BANK_SAVE_ALL;	
	::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SDI_SAMPLE, (WPARAM)pMsg->message, (LPARAM)pMsg);
}
