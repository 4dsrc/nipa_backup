/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */



#include "stdafx.h"
#ifndef _NonOpenCV
#include "Liveview.h"
#else
#include "LiveviewPure.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CLiveview, CView)

CLiveview::CLiveview()
:m_pLiveviewBuffer (NULL)
{
	m_bEVF = FALSE;
	m_bPreLive = FALSE;
}
CLiveview::~CLiveview(){}

BEGIN_MESSAGE_MAP(CLiveview, CView)	
	ON_WM_PAINT()	
END_MESSAGE_MAP()

// CLiveview diagnostics
#ifdef _DEBUG
void CLiveview::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CLiveview::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG

void CLiveview::OnPaint()
{	
	DrawLiveview();
	CView::OnPaint();
}


//------------------------------------------------------------------------------ 
//! @brief		DrawLiveview
//! @date		2010-05-19
//! @author	hongsu.jung
//------------------------------------------------------------------------------ 
void CLiveview::DrawLiveview()
{
	//-- GetRect
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);

	//TRACE(_T("width = %d, height = %d\n"), rect.Width(), rect.Height());
	
	//-- Get Liveview	
	if(!m_pLiveviewBuffer || !m_bLiveview)	
	{		
		//-- Black Background	
		dc.FillRect(rect, &CBrush(RGB(0,0,0)));		
		return;
	}

	//-- EVF 1Frame Skip
	if(m_bPreLive != m_bEVF)
	{
		m_bPreLive = m_bEVF;
		return;
	}

	//-- EVF ON/OFF view
	if(!m_bEVF)
		cvSetImageROI(m_pLiveviewBuffer, cvRect( 0, 0, 720, 480 ));
	else
		cvSetImageROI(m_pLiveviewBuffer, cvRect( 0, 0, m_pLiveviewBuffer->width, m_pLiveviewBuffer->height ));

	

	CvvImage cImage;
	IplImage *pImgSrc		= m_pLiveviewBuffer;
	IplImage *pImgResize	= NULL;
	pImgResize = cvCreateImage( cvSize(rect.Width(),rect.Height()),pImgSrc->depth, pImgSrc->nChannels );



	cvResize(pImgSrc, pImgResize);
	//TRACE(_T("width = %d, height = %d\n"), pImgResize->width, pImgResize->height);

	cImage.CopyOf(pImgResize,8);	
	cImage.Show(dc, rect.left, rect.top, rect.Width(), rect.Height() );
	cImage.Destroy();

	//-- Release Buffer
	cvReleaseImage(&pImgResize);
}

//-- 2013-03-08 hongsu.jung
void CLiveview::DrawBlank()
{
	SetLiveview(FALSE);
	SetBuffer(NULL);
	DrawLiveview();
}