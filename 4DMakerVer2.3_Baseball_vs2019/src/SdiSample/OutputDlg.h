#include "resource.h"

#pragma once


// COutputDlg 대화 상자입니다.

class COutputDlg : public CDialog
{
	DECLARE_DYNAMIC(COutputDlg)

public:
	COutputDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COutputDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_EX_OUTPUTDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
};
