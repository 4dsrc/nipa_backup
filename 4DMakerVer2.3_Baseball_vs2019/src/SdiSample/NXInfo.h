/*
 * Project : Samsung Digital Imaging Software Development Kit
 *
 * Copyright 2011 by Samsung Electronics, Inc.
 * All rights reserved.
 *
 * Project Description:
 *   Open API for Control Samsung Digital Imaging Camera
 *
 * Written and developed by ESMLab (http://esmlab.com)
 * Tel		: +82-31-718-8281
 * E-mail	: contact@esmlab.com
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File Comment
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 \file		SdiSample.cpp
 \brief		Sample Project
 \author	Hongsu Jung (hongsu@esmlab.com)
 \date		2013-02-06
 \version	1.0
 \Update Information:                                                  
 13-02-06	hongsu@esmlab.com	create first version.
 */


#pragma once

class CNXInfo : public CObject  
{
public:
   CNXInfo();
   CNXInfo( LPCTSTR TGIniFilename );
   virtual ~CNXInfo();

// Methods
public:
   // Sets the current NXInfo-file to use.
   BOOL SetIniFilename(LPCTSTR strFile);
   // Reads a string from the ini-file.
   CString GetString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszDefault=NULL);

// Variables
protected:
   CString m_strIniFilename; // The current ini-file used.
};
